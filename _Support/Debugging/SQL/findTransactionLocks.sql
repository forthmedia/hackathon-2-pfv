/*
WAB 2016-07-08
A query I got from somewhere and have had in my own folders for some time

This query shows locked objects
It can be used to kill very old processeds by setting @killProcessesOlderThan_Minutes 
*/

declare @killProcessesOlderThan_Minutes  int

SELECT  L.request_session_id AS SPID, 
        DB_NAME(L.resource_database_id) AS DatabaseName,
        O.Name AS LockedObjectName, 
        P.object_id AS LockedObjectId, 
        L.resource_type AS LockedResource, 
        L.request_mode AS LockType,
        datediff (n, transaction_begin_time, getdate()) runtime_min,
        ST.text AS SqlStatementText,        
        ES.login_name AS LoginName,
        ES.host_name AS HostName,
        TST.is_user_transaction as IsUserTransaction,
        AT.name as TransactionName,
        CN.auth_scheme as AuthenticationMethod, 
        transaction_begin_time
-- select distinct  L.request_session_id
FROM    sys.dm_tran_locks L
        JOIN sys.partitions P ON P.hobt_id = L.resource_associated_entity_id
        JOIN sys.objects O ON O.object_id = P.object_id
        JOIN sys.dm_exec_sessions ES ON ES.session_id = L.request_session_id
        JOIN sys.dm_tran_session_transactions TST ON ES.session_id = TST.session_id
        JOIN sys.dm_tran_active_transactions AT ON TST.transaction_id = AT.transaction_id
        JOIN sys.dm_exec_connections CN ON CN.session_id = ES.session_id
        CROSS APPLY sys.dm_exec_sql_text(CN.most_recent_sql_handle) AS ST
--WHERE   resource_database_id = db_id()
ORDER BY transaction_begin_time, L.request_session_id


IF @killProcessesOlderThan_Minutes is not null and @killProcessesOlderThan_Minutes > 30
BEGIN
	declare myCursor cursor for 
	SELECT  distinct L.request_session_id AS SPID 
	FROM    sys.dm_tran_locks L
			JOIN sys.partitions P ON P.hobt_id = L.resource_associated_entity_id
			JOIN sys.objects O ON O.object_id = P.object_id
			JOIN sys.dm_exec_sessions ES ON ES.session_id = L.request_session_id
			JOIN sys.dm_tran_session_transactions TST ON ES.session_id = TST.session_id
			JOIN sys.dm_tran_active_transactions AT ON TST.transaction_id = AT.transaction_id
			JOIN sys.dm_exec_connections CN ON CN.session_id = ES.session_id
			CROSS APPLY sys.dm_exec_sql_text(CN.most_recent_sql_handle) AS ST
	WHERE   datediff (n, transaction_begin_time, getdate()) > 15

	declare @spid int, @sql nvarchar(max)

	OPEN myCursor;
		FETCH NEXT FROM myCursor INTO @spid
		WHILE @@FETCH_STATUS = 0
		BEGIN   
			   set @sql = 'kill ' + convert(varchar, @spid ) 
				print @sql
			   exec (@sql)
			   FETCH NEXT FROM myCursor INTO @spid
		END

	CLOSE myCursor;
	DEALLOCATE myCursor;

END


select * from ProcessLock
 
exec sp_whoisactive

