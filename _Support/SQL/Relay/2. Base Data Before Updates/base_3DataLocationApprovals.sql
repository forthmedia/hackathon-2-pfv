
-------------------------------------------------------------------------------
-- Create new view to support getting the primary contacts to use in emails
-------------------------------------------------------------------------------
--CreatePlaceHOlderObject 'view', 'vPrimaryContactByLocation'
--GO
create view vPrimaryContactByLocation
AS
select 
	PrimaryContact.personid as PrimaryContactID,
	PrimaryContact.email,
	l.locationid,
	'Location' as PrimaryContactType
from 
	Person PrimaryContact
		inner join
	integerMultipleFlagData ifd on PrimaryContact.personid = ifd.data and ifd.flagid = 2663
		inner join
	location l on l.locationid = ifd.entityid
where 
	dbo.getsettingValue('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') is null or dbo.getsettingValue('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') =1

UNION

select 
	PrimaryContact.personid,
	PrimaryContact.email,
	l.locationid,
	'Organisation'
from 
	Person PrimaryContact
		inner join
	integerFlagData ifd on PrimaryContact.personid = ifd.data and ifd.flagid = 1750
		inner join
	location l on l.organisationid = ifd.entityid
where 
	dbo.getsettingValue('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') = 0

	
GO
--------------------------------------------------------------------------------------
-- update existing workflows to show the full work 'Registration' rather than 'Reg'
--------------------------------------------------------------------------------------
update processheader set processdescription = 'Person Registration Approvals' where processid = 121
update processheader set processdescription = 'Account Registration Approvals' where processid = 122

-----------------------------------------------------------------------
-- Create the new workflow for the location level registration process
-----------------------------------------------------------------------
SET IDENTITY_INSERT processHeader ON

insert into processHeader (processid, processdescription)
values (114, 'Account Registration Process')

SET IDENTITY_INSERT processHeader OFF

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114,10,5,'not isdefined("frmPersonID")','value','frmPersonID','#frmcurrentEntityID#',0,NULL,NULL,NULL,NULL,1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114,10,10,'TRUE','sql','getLocApprovalStatus','select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''LocApprovalStatus'' and b.entityid in (select LocationID from person where personId=#frmPersonID#)'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	20	,	'getLocApprovalStatus.recordcount eq 0'	,	'value'	,	'session.LocStatus'	,	'New'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	30	,	'(getLocApprovalStatus.recordcount eq 0) and (not request.relaycurrentuser.isinternal)'	,	'newstep'	,	NULL	,	NULL	,	40	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	40	,	'(getLocApprovalStatus.recordcount eq 0) and (request.relaycurrentuser.isinternal)'	,	'newstep'	,	NULL	,	NULL	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

/*insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	20	,	'location.locApprovalStatus EQ ""'	,	'value'	,	'session.LocStatus'	,	'New'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	30	,	'(location.locApprovalStatus EQ "") and (not request.relaycurrentuser.isinternal)'	,	'newstep'	,	NULL	,	NULL	,	40	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	40	,	'(location.locApprovalStatus EQ "") and (request.relaycurrentuser.isinternal)'	,	'newstep'	,	NULL	,	NULL	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	50	,	'getLocApprovalStatus.value eq "LocRejected"'	,	'newstep'	,	NULL	,	NULL	,	20	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	60	,	'getLocApprovalStatus.value eq "LocApproved"'	,	'value'	,	'session.LocStatus'	,	'Approved'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	70	,	'getLocApprovalStatus.value eq "LocApplied"'	,	'value'	,	'session.LocStatus'	,	'Pending'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	50	,	'location.locApprovalStatus EQ "LocRejected"'	,	'newstep'	,	NULL	,	NULL	,	20	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	60	,	'location.locApprovalStatus EQ "locApproved"'	,	'value'	,	'session.LocStatus'	,	'Approved'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	70	,	'location.locApprovalStatus EQ "LocApplied"'	,	'value'	,	'session.LocStatus'	,	'Pending'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	10	,	80	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	30	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	20	,	10	,	'TRUE'	,	'setFlag'	,	'PerRejected'	,	'TRUE'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	20	,	20	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	90	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	10	,	'TRUE'	,	'sql'	,	'GetPersonApprovalStatus'	,	'select f.flagTextId as Status, datediff(mi,b.LastUpdated,getdate()) as StatusSet from flag f inner join booleanflagdata b on f.flagid=b.flagid where f.flaggroupid in (select flaggroupid from flaggroup where flaggrouptextid=''PerApprovalStatus'') and b.entityid=#frmpersonID#'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	20	,	'GetPersonApprovalStatus.Status eq "PerRejected"'	,	'newstep'	,	NULL	,	NULL	,	100	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	30	,	'GetPersonApprovalStatus.Status eq "PerApplied"'	,	'newstep'	,	NULL	,	NULL	,	80	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	40	,	'GetPersonApprovalStatus.Status eq "PerApproved"'	,	'newstep'	,	NULL	,	NULL	,	70	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	50	,	'GetPersonApprovalStatus.recordcount eq 0'	,	'value'	,	'session.PersonStatus'	,	'New'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	20	,	'person.perApprovalStatus EQ "perRejected"'	,	'newstep'	,	NULL	,	NULL	,	100	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	30	,	'person.perApprovalStatus EQ "perApplied"'	,	'newstep'	,	NULL	,	NULL	,	80	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	40	,	'person.perApprovalStatus EQ "perApproved"'	,	'newstep'	,	NULL	,	NULL	,	70	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	50	,	'person.perApprovalStatus EQ ""'	,	'value'	,	'session.PersonStatus'	,	'New'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	60	,	'request.relaycurrentuser.isinternal'	,	'newstep'	,	NULL	,	NULL	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	70	,	'GetPersonApprovalStatus.recordcount eq 0'	,	'newstep'	,	NULL	,	NULL	,	50	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	70	,	'person.perApprovalStatus EQ ""'	,	'newstep'	,	NULL	,	NULL	,	50	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	30	,	80	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	110	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	40	,	10	,	'structKeyExists(session,"UserInvited") and session.UserInvited'	,	'screen'	,	NULL	,	'LocRegistrationScreen'	,	50	,	NULL	,	NULL	,	NULL	,	'HIDERIGHTBORDER=yes'	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	40	,	15	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	50	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	50	,	10	,	'structKeyExists(session,"UserInvited") and session.UserInvited'	,	'screen'	,	NULL	,	'PerRegistrationScreen'	,	60	,	NULL	,	NULL	,	NULL	,	'HIDERIGHTBORDER=yes'	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	50	,	15	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	5	,	'TRUE'	,	'sql'	,	'isPrimaryContactValid'	,	'select email as value from person where personid in (select distinct i.data from integermultipleflagdata i join person p on p.locationid = i.entityid where i.flagid = 2663 and p.personid = #frmPersonID#) and personID in (select b.entityID from booleanflagdata b where b.flagID = 1695) and locationid in (select b.entityID from booleanflagdata b where b.flagID = 2233)'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	10	,	'TRUE'	,	'include'	,	'/relay/approvals/RegistrationProcess.cfm'	,	null	,	0	,	NULL	,	'Set PC & HQ & Approval Flags, Username & Password (where relevant)'	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	20	,	'isdefined("session.LocStatus") and session.LocStatus eq "New"'	,	'sendemail'	,	NULL	,	'RegistrationNewOrgNotification'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	23	,	NULL	,	'value'	,	'session.LocStatus'	,	'Completed'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	26	,	NULL	,	'newstep'	,	NULL	,	NULL	,	30	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	30	,	'isdefined("session.PersonStatus") and session.PersonStatus eq "New" and isPrimaryContactValid.recordcount eq 0'	,	'sendemail'	,	NULL	,	'RegistrationNewPersonNotification'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	30	,	'isdefined("session.PersonStatus") and session.PersonStatus eq "New" and location.primarycontact eq ""'	,	'sendemail'	,	NULL	,	'RegistrationNewPersonNotification'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	35	,	NULL	,	'value'	,	'session.PersonStatus'	,	'Completed'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	37	,	'isdefined("session.PersonStatus") and session.PersonStatus eq "New" and isPrimaryContactValid.recordcount neq 0'	,	'sendemail'	,	NULL	,	'RegistrationNotificationForPrimaryContact'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	37	,	'isdefined("session.PersonStatus") and session.PersonStatus eq "New" and location.primarycontact neq ""'	,	'sendemail'	,	NULL	,	'RegistrationNotificationForPrimaryContact'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	40	,	NULL	,	'value'	,	'session.PersonStatus'	,	'Completed'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	60	,	45	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	30	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	70	,	10	,	'not request.relaycurrentuser.isinternal'	,	'sendemail'	,	NULL	,	'RegistrationApprovedEmail'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	70	,	20	,	NULL	,	'goToElement'	,	'ApprovedPerson'	,	'ApprovedPerson'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	70	,	30	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	-1	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	80	,	10	,	'not request.relaycurrentuser.isinternal'	,	'sendemail'	,	NULL	,	'RegistrationConfirmation'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	80	,	20	,	NULL	,	'goToElement'	,	'AppliedPerson'	,	'AppliedPerson'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	80	,	30	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	-1	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	90	,	10	,	'not request.relaycurrentuser.isinternal'	,	'goToElement'	,	'RejectedOrg'	,	'RejectedOrg'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	90	,	20	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	-1	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	100	,	10	,	'not request.relaycurrentuser.isinternal'	,	'goToElement'	,	'ExistingRejPerNonRejOrg'	,	'ExistingRejPerNonRejOrg'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	100	,	20	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	-1	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	110	,	10	,	'not request.relaycurrentuser.isinternal'	,	'goToElement'	,	'ExistingPerson'	,	'ExistingPerson'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (114	,	110	,	20	,	'TRUE'	,	'newstep'	,	NULL	,	NULL	,	-1	,	NULL	,	NULL	,	NULL	,	NULL	,	1)


-----------------------------------------------------------
-- Update workflow 111 to accommodate approval level check
-----------------------------------------------------------
update processactions
set StepID = 15
where StepID = 10 and ProcessID = 111


-------------------------------
-- Add in approval level check
-------------------------------
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	10	,	'TRUE'	,	'sql'	,	'CheckApprovalLevel'	,	'select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'''	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	20	,	'CheckApprovalLevel.recordcount eq 0'	,	'newprocess'	,	'0'	,	'114'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	25	,	'CheckApprovalLevel.value eq ''1'''	,	'newprocess'	,	'0'	,	'114'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	30	,	'CheckApprovalLevel.value eq ''0'''	,	'newstep'	,	''	,	''	,	15	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	25	,	'getSetting("PLO.USELOCATIONASPRIMARYPARTNERACCOUNT") EQ 1'	,	'newprocess'	,	'0'	,	'114'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (111	,	10	,	30	,	'getSetting("PLO.USELOCATIONASPRIMARYPARTNERACCOUNT") EQ 0'	,	'newstep'	,	''	,	''	,	15	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/


---------------------------------------------
-- add Approval check to approval workflow
---------------------------------------------
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	10	,	'TRUE'	,	'sql'	,	'CheckApprovalLevel'	,	'select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'''	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	20	,	'(isDefined("frmLocationID")) and (CheckApprovalLevel.recordcount eq 0)'	,	'newstep'	,	''	,	''	,	10	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	25	,	'(isDefined("frmLocationID")) and (CheckApprovalLevel.value eq ''1'')'	,	'newstep'	,	''	,	''	,	10	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	30	,	'(isDefined("frmOrganisationID")) and (CheckApprovalLevel.value eq ''0'')'	,	'newstep'	,	''	,	''	,	10	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	35	,	'isDefined("frmOrganisationID")'	,	'message'	,	''	,	'phr_ApprovalsLocLevel'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	1	,	40	,	'isDefined("frmLocationID")'	,	'message'	,	''	,	'phr_ApprovalsOrgLevel'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
/*
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	10	,	'getSetting(''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'') is 1'	,	'newstep'	,	''	,	''	,	55	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	20	,	'getSetting(''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'') is 0'	,	'newstep'	,	NULL	,	NULL	,	50	,	NULL	,	NULL	,	NULL	,	NULL	,	1)
*/
insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	10	,	'TRUE'	,	'sql'	,	'CheckApprovalLevel'	,	'select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'''	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	20	,	'CheckApprovalLevel.recordcount eq 0'	,	'newstep'	,	''	,	''	,	55	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	25	,	'CheckApprovalLevel.value eq ''1'''	,	'newstep'	,	''	,	''	,	55	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	45	,	30	,	'CheckApprovalLevel.value eq ''0'''	,	'newstep'	,	''	,	''	,	50	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	55	,	10	,	'TRUE'	,	'sql'	,	'getApprovedUsers'	, 'select b.entityid from booleanflagdata b inner join flag f on f.flagid=b.flagid where flagtextid=''PerApproved'' and entityid in 	(select personid from person where locationid in (#frmlocationid#))'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	55	,	25	,	'getApprovedUsers.recordcount neq 0'	,	'screen'	,	''	,	'ApprovalChoosePrimaryContactAccount'	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (122	,	55	,	30	,	'TRUE'	,	'newstep'	,	''	,	''	,	60	,	NULL	,	NULL	,	NULL	,	NULL	,	1)


update processactions
set jumpexpression = '(isdefined("form.AccountApprovalStatus")) and (form.AccountApprovalStatus contains "Approv")'
where processid = 122 and stepid = 40 and sequenceid = 10

update processactions
set nextStepID = 45
where processid = 122 and stepid = 40 and sequenceid = 10

update processactions
set jumpexpression = 'isdefined("session.accountApprovalMsg")', jumpvalue = '#session.accountApprovalMsg#'
where processid = 122 and stepid = 60 and sequenceid = 10


-------------------------------------------
-- Make example reseller and relayware locations approved
-------------------------------------------
insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby, lastupdatedbyperson)
values (5006,(select flagid from flag where flagtextid = 'locapproved') ,getdate(), 102, getdate(), 102, 3)

insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby, lastupdatedbyperson)
values (1046,(select flagid from flag where flagtextid = 'locapproved') ,getdate(), 102, getdate(), 102, 3)

insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby, lastupdatedbyperson)
values (5072,(select flagid from flag where flagtextid = 'locapproved') ,getdate(), 102, getdate(), 102, 3)



--------------------------------------------
-- Rename organisation registration screen
--------------------------------------------
update screens 
set screentextid = 'LocRegistrationScreen', screenname = 'Account Registration', suite = 0
where screentextid = 'organizationRegistration'


---------------------
--Reg and approval screen tidy up
---------------------
--Org
Delete from screendefinition where fieldtextid = 'AddressedIndustry' and screenid in (select screenid from screens where screentextid = 'OrgRegistrationScreen')
Delete from screendefinition where fieldtextid = 'OrgProfile' and screenid in (select screenid from screens where screentextid = 'OrgRegistrationScreen')
Delete from screendefinition where screenid in (select screenid from screens where screentextid = 'OrgProfile')
-- Delete from screens where screentextid = 'OrgProfile' This screen is used in the Company Profile page

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'OrgRegistrationScreen'),0,0,1,'HTML','<p>Organisation Level</p><p>Registration questions go here</p>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 21 2016 11:02:32:837AM',0,0)


--Loc
Delete from screendefinition where fieldtextid in ('LocServices','LocAddressedIndustry','LocBusinessHours','LocBusinessHoursWeekends','LocNumberOfEmployees') and screenid in (select screenid from screens where screentextid = 'LocRegistrationScreen')

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'LocRegistrationScreen'),0,0,1,'HTML','<p>Location Level</p><p>Registration questions go here</p>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 21 2016 11:19:17:327AM',0,0)

update screens set entitytype = 'Location', Screenname = 'Approval - Manage People Approval Status', entitytypeid = 1
where screentextid = 'ManagePeopleApprovalStatus'

delete from screendefinition where fieldsource = 'HTML' and screenid in (select screenid from screens where screentextid = 'ManagePeopleApprovalStatus')

update screendefinition set condition = null where screenid in (select screenid from screens where screentextid = 'ManagePeopleApprovalStatus')


--------------------------------------------
-- Add in new screens here
-- Account Approvals Review, Account Approval, Approvals Primary Contact Screen & Account Approval - Manage People Approval Status
--------------------------------------------
-- Account Approvals Review
INSERT INTO [screens] 
([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[LastUpdatedBy],[LastUpdated])
VALUES((select max(screenid)+1 from screens),'AccountApprovalsReview','Account Approvals Review',1,0,'location',0,0,'ShowScreen','edit','',102,'Sep 15 2016  6:26:04:000AM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0,1,0,'',102,'Sep 15 2016  6:53:44:000AM')

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),0,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 21 2016 11:35:08:243AM',0,0)

/*INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),3,0,1,'HTML','<p>Review partner application form before approving.</p>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','request.relaycurrentuser.isinternal',102,'Sep 22 2016  9:35:24:993AM',0,0)
*/

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),5,0,1,'Screen','LocRegistrationScreen','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')) and not request.relaycurrentuser.isinternal',102,'Sep 22 2016  9:35:24:993AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),10,0,1,'Screen','OrgRegistrationScreen','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.value eq ''0'') and (not request.relaycurrentuser.isinternal))',102,'Sep 22 2016 10:02:59:277AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),15,0,1,'Screen','LocRegistrationScreen','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')) and request.relaycurrentuser.isinternal',102,'Sep 22 2016  9:36:50:820AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApprovalsReview'),20,0,1,'Screen','OrgRegistrationScreen','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.value eq ''0'') and (request.relaycurrentuser.isinternal))',102,'Sep 22 2016 10:02:59:277AM',0,0)


-- Account Approval
INSERT INTO [screens] 
([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[LastUpdatedBy],[LastUpdated])
VALUES((select max(screenid)+1 from screens),'AccountApproval','Account Approval',1,0,'location',0,0,'ShowScreen','edit','',102,'Sep 15 2016  6:32:58:000AM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0,1,0,'',102,'Sep 15 2016  6:33:08:000AM')

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),0,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 22 2016 11:14:03:590AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),5,0,1,'HTML','<p>Organisation Approvals</p>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','CheckApprovalLevel.value eq ''0''',102,'Sep 22 2016 11:16:12:270AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),10,0,1,'flagGroup','OrgApprovalStatus','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','CheckApprovalLevel.value eq ''0''',102,'Sep 22 2016 11:16:24:620AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),15,0,1,'HTML','<p>Location Approvals</p>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','(CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')',102,'Sep 22 2016 11:16:52:797AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),20,0,1,'flagGroup','locApprovalStatus','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','(CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')',102,'Sep 22 2016 11:16:44:390AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),25,0,1,'flagGroup','AllocationSalesTechnical','','',0,0,'edit',1,0,NULL,NULL,'ignoreFlagRightsForBackwardsCompatibility=true',0,0,'','',NULL,'','',438,'Dec 15 2010 11:56:12:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),30,0,1,'flagGroup','Tier1Tier2Distributor','','',0,0,'edit',1,0,NULL,NULL,'SHOWNULL=false,ALLOWCURRENTVALUE=false,DISPLAYAS=Select',1,0,'','',NULL,'','',438,'Dec 15 2010 11:57:26:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'AccountApproval'),35,0,1,'flagGroup','AssignDistributor','','',0,0,'edit',1,0,NULL,NULL,'DISPLAYAS=Select',1,0,'','',NULL,'','',438,'Dec 15 2010 11:59:46:000AM',0,0)


-- Approvals Primary Contact Screen
INSERT INTO [screens] 
([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[LastUpdatedBy],[LastUpdated])
VALUES((select max(screenid)+1 from screens),'ApprovalChoosePrimaryContactAccount','Approvals Primary Contact Screen',1,1,'location',0,0,'ShowScreen','edit','',102,'Sep 20 2016 11:31:14:000AM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0,1,0,'',102,'Sep 26 2016 10:20:37:000AM')

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'ApprovalChoosePrimaryContactAccount'),15,0,1,'HTML','<p>Please select Primary Contacts from the list below:</p>','','',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','application.com.flag.getFlagData(flagId="PrimaryContacts", entityid=#frmLocationID#).data eq ""',102,'Sep 26 2016 10:41:14:453AM',0,1)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'ApprovalChoosePrimaryContactAccount'),20,0,1,'Flag','PrimaryContacts','','',0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',102,'Sep 26 2016 10:21:22:100AM',0,0)


/*--Account Approval - Manage People Approval Status
INSERT INTO [screens] 
([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[LastUpdatedBy],[LastUpdated])
VALUES((select max(screenid)+1 from screens),'LocManagePeopleApprovalStatus','Account Approval - Manage People Approval Status',1,0,'Location',0,0,'ShowScreen','edit','',432,'May 30 2008  6:20:06:000PM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'approvalTask',NULL,'',0,2,0,'',0,'Apr 30 2014  3:34:43:173PM')

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'LocManagePeopleApprovalStatus'),4,0,1,'IncludeRelayFile','approvals/RegistrationApprovalAccountUserListIframe.cfm','','',0,0,'edit',1,0,NULL,NULL,'CHANGABLESTATUSES="PerApplied,PerApproved,PerRejected,PerInactive"',0,0,'','',NULL,'','isdefined("frmLocationID")',432,'Jul  2 2008 11:20:46:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES((select screenid from screens where screentextid = 'LocManagePeopleApprovalStatus'),6,0,1,'HTML','<p>LocationID not found</p>','','',0,0,'edit',1,0,NULL,NULL,'',0,0,'','',NULL,'','not isdefined("frmLocationID")',432,'May 30 2008  6:21:14:000PM',0,0)
*/


----------------------------------
-- Set unused screens to inactive
----------------------------------
 update screens
 set active = 0
 where screentextid in ('OrgApprovalReviewScreen')


--------------------------------------------------
-- Update screens in account reg approval workflow
--------------------------------------------------
update processactions
set jumpvalue = 'AccountApprovalsReview'
where jumpvalue = 'OrgApprovalReviewScreen'
and processid = 122

update processactions
set jumpvalue = 'AccountApproval'
where jumpvalue = 'OrgApprovalScreen'
and processid = 122


------------------------------------------
-- update org profile summay tidy up
------------------------------------------
update screendefinition set active = 0
where screenid = 430
and fieldtextid in ('PartnerLevel','NumbEmps','AddressedIndustry')

update screendefinition set sortorder = 55
where screenid = 430
and fieldtextid in ('LeadAndOppCheckBoxes')

update screendefinition set sortorder = 2
where screenid = 430
and fieldtextid in ('Tier1Tier2Distributor')


INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(430,0,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 21 2016 11:35:08:243AM',0,0)

update screendefinition
set condition = 'CheckApprovalLevel.value eq ''0''', sortorder = 1
where screenid = 430 
and fieldtextid = 'OrgApprovalStatus'


--------------------------------------
-- update location profile summary
--------------------------------------
INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(518,0,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Sep 21 2016 11:35:08:243AM',0,0)

update screendefinition
set condition = '(CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')', method = 'view', sortorder = 10
where screenid = 518 
and fieldtextid = 'LocApprovalStatus'


-------------------------------------------------------------
-- Update request a login page to include new reg screen
-------------------------------------------------------------
update phrases set phrasetext = '<RELAY_PARTNERREGISTRATIONFORM frmnext="returnprocessid" frmnextpage="thisPage" initialiseuser="true" personScreenID=400 organisationscreenid=AccountApprovalsReview ProcessID=111 showCols=insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID,insLocEmail showPersonCols=insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone ShowLocationCols=insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax>'
where phraseid = (select phraseid from phraselist where entitytypeid = 10 and entityid = 464 and phrasetextid = 'detail')



---------------------------------------
-- Email changes
-- Update mergefield to show account approval status for the emails 'RegistrationNotificationForPrimaryContact' and 'RegistrationNewPersonNotification'
-- Update 'To' address in 'RegistrationNotificationForPrimaryContact' email
---------------------------------------
update phrases
set phrasetext = '
<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>New Registration</title>
</head>
<body style="background: rgb(247, 243, 243);">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-top: 10px; padding-right: 20px; padding-left: 20px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color: rgb(17, 137, 202);">New Person Notification</span></h3>

						<p style="line-height: 20px;">A new Partner Portal application has been received with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[firstname]] [[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>User Status:</b></td>
									<td class="rColumn">[[PerApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account&nbsp;Status:</b></td>
									<td class="rColumn">[[accountApprovalStatus]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
where phraseid in (select phraseid from phraselist where phrasetextid = 'body' and entitytypeid = 200 and entityid in (select emaildefid from emaildef where emailtextid = 'RegistrationNewPersonNotification'))
and languageid = 1 and countryid = 0



update phrases
set phrasetext = 
'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background: rgb(247, 243, 243);">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-top: 10px; padding-right: 20px; padding-left: 20px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color: rgb(17, 137, 202);">New Person Notification</span></h3>

						<p style="line-height: 20px;">A new Partner Portal application has been received with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[firstname]] [[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>User Status:</b></td>
									<td class="rColumn">[[PerApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lcolumn"><b>Account Status:</b></td>
									<td class="rcolumn">[[accountApprovalStatus]]</td>
								</tr>
							</tbody>
						</table>

						<p>Once you''ve reviewed the above, please log into the Portal''s ''Manage My Colleagues'' section to approve this user.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
where phraseid in (select phraseid from phraselist where phrasetextid = 'body' and entitytypeid = 200 and entityid in (select emaildefid from emaildef where emailtextid = 'RegistrationNotificationForPrimaryContact'))
and languageid = 1 and countryid = 0


Update emaildef
set toAddress = 'select email from vPrimaryContactByLocation where locationid in (select locationid from person where personid = #personid#)'
where emailtextid = 'RegistrationNotificationForPrimaryContact'

update emaildef set name = 'Registration Account Notification' where emailtextid = 'RegistrationNewOrgNotification'
update emaildef set name = 'Registration Rejected Account' where emailtextid = 'RegistrationRejectedEmail'


--Phrase updates
update phrases set phrasetext = 'Email Domain - Automatically added to email address when creating a new person' where phraseid in (select phraseid from phraselist where phrasetextid = 'Sys_DefaultEmailDomain')
and languageid in (0,1)


 /*
 select * from screens where screentextid in (
 select jumpvalue from processactions where jumpaction = 'screen')

 select * from phrases where phrasetext like '%screenid%'

 select * from screens where screenid in (542,501,433,436)
 union
 select * from screens where screentextid in ('primarycontact','unsubscribehandler','baselocationrecord','OrgProfile','OrderingSetupQuestions')
 union
  select * from screens where screentextid in (
 select jumpvalue from processactions where jumpaction = 'screen')


 select screenid, screenname, * from screens where entitytypeid = 2 and active = 1 and screenid not in (17,400,424,427,432,433,436,442,444,449,450,452,501,504,532,542,543,544) and suite = 1


 select * from screens where screenid in (442,504,449,427)
 */

 -----------------------------------------------------------------------
 -- Update the OOTB security profile to look at the locapproval status - PROD2016-2157
  -----------------------------------------------------------------------
 update securityprofile
 set needflagids = '(perApproved AND LocApproved) AND (NOT Tier1Tier2DistributorEndCustom)'
 where securityprofileid = 1

 
------------------------------------------------------------------------------
-- Update screen 'New Card Add Screen' to include new reg questions screen
-- Part of the internal registration
------------------------------------------------------------------------------
update screendefinition
set fieldlabel = '<h2>Account Questions</h2>'
where screenid = 446 and fieldlabel = '<h2>Organisation Questions</h2>'

update screendefinition
set fieldtextid = 'AccountApprovalsReview'
where screenid = 446 and fieldtextid = 'OrgRegistrationScreen'
 

-------------------------------------------
-- Internal registration changes
-------------------------------------------
SET IDENTITY_INSERT processHeader ON

insert into processHeader (processid, processdescription)
values (115, 'Internal Registration Process')

SET IDENTITY_INSERT processHeader OFF


insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	10	,	10	,	'TRUE'	,	'include'	,	'/relay/approvals/RegistrationProcess.cfm'	, NULL, 	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	10	,	15	,	'TRUE'	,	'sql'	,	'CheckApprovalLevel'	,	'select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT'''	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	10	,	20	,	'CheckApprovalLevel.recordcount eq 0'	,	'newprocess'	,	NULL	,	NULL	,	20	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	10	,	25	,	'CheckApprovalLevel.value eq ''1'''	,	'newprocess'	,	NULL	,	NULL	,	20	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	10	,	30	,	'CheckApprovalLevel.value eq ''0'''	,	'newstep'	,	NULL	,	NULL	,	30	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115,	20,		10,		'TRUE','sql','getLocApprovalStatus','select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''LocApprovalStatus'' and b.entityid in (select LocationID from person where personId=#frmPersonID#)'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	20	,	15	,	'(getLocApprovalStatus.value eq "LocApproved") or (getLocApprovalStatus.value eq "LocApplied")'	,	'newstep'	,	NULL	,	NULL	,	40	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	30	,	10	,	'true'	,	'sql'	,	'getOrgApprovalStatus'	,	'select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''OrgApprovalStatus'' and b.entityid in (select OrganisationID from person where personId=#frmPersonID#)'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	30	,	15	,	'(getOrgApprovalStatus.value eq "OrgApproved") or (getOrgApprovalStatus.value eq "OrgApplied")'	,	NULL	,	NULL	,	NULL	,	40	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	40	,	10	,	'true'	,	'sql'	,	'GetPerApprovalStatus'	,	'select f.flagTextId as Status, datediff(mi,b.LastUpdated,getdate()) as StatusSet from flag f inner join booleanflagdata b on f.flagid=b.flagid where f.flaggroupid in (select flaggroupid from flaggroup where flaggrouptextid=''PerApprovalStatus'') and b.entityid=#frmpersonID#'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	40	,	15	,	'(getPerApprovalStatus.status eq "PerApproved") and (getPerApprovalStatus.StatusSet lt 10)'	,	'sendemail'	,	NULL	,	'RegistrationApprovedEmail'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)

insert into ProcessActions (ProcessID,StepID,SequenceID,JumpExpression,JumpAction,jumpname, Jumpvalue,NextStepID,NextProcessStepTextID,comment,processStepTextID, parameters, active)
values (115	,	40	,	20	,	'getPerApprovalStatus.recordcount eq 1'	,	'sendemail'	,	NULL	,	'RegistrationNewPersonNotification'	,	0	,	NULL	,	NULL	,	NULL	,	NULL	,	1)


------------------------------------------------------------------------------
-- Update screen 'Process Registration' to include new reg questions screen
-- Part of the internal registration
------------------------------------------------------------------------------
--  Add Person 2nd Screen
DELETE from screendefinition where screenid = 447

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,1,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 15 2016  8:25:59:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,2,0,1,'Query','GetPersonApprovalStatus','"select f.flagTextId as Status, datediff(mi,b.LastUpdated,getdate()) as StatusSet from flag f inner join booleanflagdata b on f.flagid=b.flagid where f.flaggroupid in (select flaggroupid from flaggroup where flaggrouptextid=''PerApprovalStatus'') and b.entityid=#frmpersonID#"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 15 2016  8:26:09:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,3,0,1,'Query','GetLocationApprovalStatus','"select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''LocApprovalStatus'' and b.entityid in (select LocationID from person where personId=#frmPersonID#)"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1''))',102,'Nov 15 2016  8:26:18:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,4,0,1,'Query','GetOrgApprovalStatus','"select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''OrgApprovalStatus'' and b.entityid in (select organisationid from person where personId=#frmPersonID#)"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','CheckApprovalLevel.value eq ''0''',102,'Nov 15 2016  8:26:28:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,10,0,1,'Label','-','','<h1>Click on save to submit the registration</h1>',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','GetPersonApprovalStatus.recordcount eq 0',102,'Nov 15 2016  8:32:08:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,15,0,1,'Label','-','','<h2>Person Questions.....</h2>',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:50:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,20,0,1,'Screen','PerRegistrationScreen','','',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:41:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,50,0,1,'flagGroup','locApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')) and (GetLocationApprovalStatus.recordcount gt 0)',102,'Nov 15 2016  8:28:37:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,55,0,1,'flagGroup','OrgApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','(CheckApprovalLevel.value eq ''0'') and (GetOrgApprovalStatus.recordcount gt 0)',102,'Nov 15 2016  8:28:28:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,60,0,1,'flagGroup','PerApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','GetPersonApprovalStatus.recordcount gt 0',102,'Nov 15 2016  8:28:15:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(447,100,0,1,'ScreenPostProcess','115','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 10 2016 10:07:49:767AM',0,0)


-- New Card Add Screen
DELETE from screenDefinition where screenid = 446

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,1,0,1,'Query','CheckApprovalLevel','"select value as value from settings where name = ''PLO.USELOCATIONASPRIMARYPARTNERACCOUNT''"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 15 2016  8:25:59:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,2,0,1,'Query','GetPersonApprovalStatus','"select f.flagTextId as Status, datediff(mi,b.LastUpdated,getdate()) as StatusSet from flag f inner join booleanflagdata b on f.flagid=b.flagid where f.flaggroupid in (select flaggroupid from flaggroup where flaggrouptextid=''PerApprovalStatus'') and b.entityid=#frmpersonID#"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 15 2016  8:26:09:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,3,0,1,'Query','GetLocationApprovalStatus','"select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''LocApprovalStatus'' and b.entityid in (select LocationID from person where personId=#frmPersonID#)"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1''))',102,'Nov 15 2016  8:26:18:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,4,0,1,'Query','GetOrgApprovalStatus','"select f.flagtextid as value from booleanflagdata b inner join flag f on b.flagid=f.flagid inner join flaggroup fg on f.flaggroupid=fg.flaggroupid where flaggrouptextid=''OrgApprovalStatus'' and b.entityid in (select organisationid from person where personId=#frmPersonID#)"','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','CheckApprovalLevel.value eq ''0''',102,'Nov 15 2016  8:26:28:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,10,0,1,'Label','-','','<h1>Click on save to submit the registration</h1>',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','GetPersonApprovalStatus.recordcount eq 0',102,'Nov 15 2016  8:32:08:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,15,0,1,'Label','-','','<h2>Person Questions.....</h2>',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:50:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,20,0,1,'Screen','PerRegistrationScreen','','',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:41:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,25,0,1,'Label','-','','<h2>Account Questions</h2>',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:28:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,30,0,1,'Screen','AccountApprovalsReview','','',0,0,'edit',1,0,NULL,NULL,'',0,0,'screen','screen',NULL,'','',102,'Nov 15 2016  8:29:18:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,50,0,1,'flagGroup','locApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','((CheckApprovalLevel.recordcount eq 0) or (CheckApprovalLevel.value eq ''1'')) and (GetLocationApprovalStatus.recordcount gt 0)',102,'Nov 15 2016  8:28:37:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,55,0,1,'flagGroup','OrgApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','(CheckApprovalLevel.value eq ''0'') and (GetOrgApprovalStatus.recordcount gt 0)',102,'Nov 15 2016  8:28:28:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,60,0,1,'flagGroup','PerApprovalStatus','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','GetPersonApprovalStatus.recordcount gt 0',102,'Nov 15 2016  8:28:15:000AM',0,0)

INSERT INTO [screendefinition] 
([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
VALUES(446,100,0,1,'ScreenPostProcess','115','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',102,'Nov 10 2016 10:07:49:767AM',0,0)


--PerRegistrationScreen
update screendefinition
set condition = 'not request.relaycurrentuser.isinternal'
where screenid = 400 and fieldsource = 'IncludeUserFile' and sortorder = '100'

update screendefinition
set active = 0
where screenid = 400 and fieldtextid in ('PreferredMethodOfContact','Unsubscribe')


------------------------------------------------------------------------------------------
-- Update settings to use 'Add Person 2nd Screen' as the default 'Per Post Add Screen ID'
------------------------------------------------------------------------------------------
insert into settings (name, value, lastupdated, lastupdatedbyperson, lastupdatedby)
values ('SCREENS.PERPOSTADDSCREENID','perPostAddScreen', getdate(), 3, 102)