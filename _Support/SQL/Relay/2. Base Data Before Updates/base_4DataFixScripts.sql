/* 

	Scripts run after base_2Data

*/

/* CORE-839 add usergroupcountry data for new internal users to prevent this issue from occuring with the population of action owner valid value */
insert into usergroupcountry
select ug.usergroupID,c.countryID,100,getdate(),100,getdate()
from usergroup as ug 
left outer join usergroupcountry ugc on ug.usergroupID = ugc.usergroupID
inner join country c on 1=1 and c.ISOcode is not null
where ug.personID is not null
and ugc.usergroupID is  null



/* NJH 2015/08/05 JIRA Fifteen-302 - remove some unwanted profiles */
delete from BooleanFlagData where FlagID in (Select FlagID from vFlagDef where FlagGroupTextID in ('LocProducts','Products'))
delete from ScreenDefinition where FieldTextID in ('LocProducts','Products')
delete from flag where FlagID in (Select FlagID from vFlagDef where FlagGroupTextID in ('LocProducts','Products'))
delete from FlagGroup where FlagGroupTextID in ('LocProducts','Products')

/* BF-10 NJH 2015/11/18 - remove connector flags that exist in the OOTB - there were 149 records that were flagged, where the records didn't actually exist */
delete from booleanFlagData where flagID in (select flagID from vbooleanFlagData where flagGroupTextID like '%connectorSynchStatus') 

/* remove items from screen definition where they refer to a country that doesn't exist */
delete from screenDefinition where countryID not in (select countryID from country) and countryId != 0

GO

---------------------------------
--Configuration changes
---------------------------------
--------------------------------------------------------------------------------------
-- update HQ valid value query as does not show complete data - fixed on Micro Focus
--------------------------------------------------------------------------------------
update validfieldvalues
set datavalue = 'select l.locationid as thisValidValue, l.locationid as dataValue, case when len(l.sitename) > 20 then left(l.sitename,20)+''..'' else l.sitename end +'' (''+ case when len(l.address1) > 15 then left(l.address1,15)+''..'' else isNull(l.address1,'''') end + '', '' + case when len(l.address4) > 10 then left(l.address4,10)+''..'' else isNull(l.address4,'''') end +'', ''+ case when len(l.address5) > 2 then left(l.address5,2)+''..'' else isNull(l.address5,'''') end +'' ''+ case when len(l.postalcode) > 10 then left(l.postalcode,10)+''..'' else isNull(l.postalcode,'''') end +'')'' as displayValue from location l where organisationid=#entityid#'
where fieldname = 'flag.HQ'


GO

-----------------------------------------------------------------------
-- update Jamies userrights
-----------------------------------------------------------------------
insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
select (select usergroupid from usergroup where name = 'Jaime Hutchins'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
from rights where usergroupid = 438

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, 550, 102, getdate(), 102, getdate() from person where username = 'Jaime Hutchins'

insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
select personid, usergroupid, 102, getdate(), 102, getdate() from usergroup where name = 'Jaime Hutchins'


GO

-----------------------------------
--Site map changes
-----------------------------------
update element set parentid = 1143 where id = 1146

update element set elementtextid = 'SalesOld' where id = 453 

update element set elementtextid = 'MarketingOld' where id = 506 

update element set elementtextid = 'EventsOld' where id = 1077 


GO

-----------------------------
-- Add in my companies leads page
-----------------------------
if not exists (select id from element where headline = 'My Companies Leads')
begin
	insert into element (elementtypeid, parentid, statusID, sortOrder, headline, isLive, Showonmenu, 
	createdby, created, lastupdatedby, lastupdated, lastupdatedbyPerson)
	values (42,1163,4,53,'My Companies Leads', 1, 1, 102, getdate(), 102, getdate(), 3)
	
	insert into phraselist (phraseid, entitytypeid, entityid, phrasetextID, createdby, created)
	select (select max(phraseid) + 1 from phraselist), 10, ID, 'HEADLINE', 3, getdate()
	from element where headline = 'My Companies Leads'
	
	insert into phraselist (phraseid, entitytypeid, entityid, phrasetextID, createdby, created)
	select (select max(phraseid) + 1 from phraselist), 10, id, 'DETAIL', 3, getdate()
	from element where headline = 'My Companies Leads'
	
	insert into phrases (languageid, phraseid, countryid, phrasetext, createdby, created, lastupdatedby, lastupdated, defaultforthiscountry)
	select 1, phraseid, 0, 'My Companies Leads', 3, getdate(), 3, getdate(), 1
	from phraselist where phrasetextID = 'HEADLINE' and entityid = (select id from element where headline = 'My Companies Leads')
	
	insert into phrases (languageid, phraseid, countryid, phrasetext, createdby, created, lastupdatedby, lastupdated, defaultforthiscountry)
	select 1, phraseid, 0, '<RELAY_SHOWPARTNERLEADS>', 3, getdate(), 3, getdate(), 1
	from phraselist where phrasetextID = 'DETAIL' and entityid = (select id from element where headline = 'My Companies Leads')
end

GO

----------------------------------------------------------
-- Insert locCRMID into defualt downloadformat defintion
----------------------------------------------------------
insert into downloadformatdef (downloadformatid, sortorder, tablesource, fieldsource, headinglabel)
values (9,101, 'location', 'crmlocid', 'CRMLocID')

select * from downloadformatdef where downloadformatid = 9

-------------------------------------------------------------
-- Update phrases that have all of the languages set as default for 'All Countries' - causes issues with anyone set with the default language
-------------------------------------------------------------
update phrases
set defaultforthiscountry = 1
where phraseid in (select phraseid
from phrases
where defaultforthiscountry = 1
group by phraseid, countryid
having count(defaultforthiscountry) > 1)
and languageid = 1


GO

----------------------------------------------------------
-- Add in new OOTB users
----------------------------------------------------------
update person
set firstname = 'Duncan', lastname = 'Mollison', username = 'Duncan Mollison', password = 'E2B0D9EBFA4C22B8CE84F3074CA67C8B', email = 'Duncan.Mollison@relayware.com', matchname = 'Duncan�Mollison�Duncan.Mollison@relayware.com', 
publicprofilename = 'Duncan Mollison', publicProfileAbout = 'Project Manager'
where username = 'Steven Nettleingham'

update usergroup
set name = 'Duncan Mollison'
where name = 'Steven Nettleingham'

update person
set firstname = 'Grace', lastname = 'Grant-Adamson', username = 'Grace GrantAdamson', password = '9F88754A7D08D04FC8EAC11C21C779C7', email = 'Grace.Grant-Adamson@relayware.com', 
matchname = 'Grace�Grant Adamson�Grace.Grant Adamson@relayware.com'
where username = 'Vicky Hume'

update usergroup
set name = 'Grant-Adamson'
where name = 'Vicky Hume'

update person
set Salutation = 'Mr.', firstname = 'Paul', lastname = 'Ellery', username = 'Paul Ellery', password = 'BCFE67DA1EAD004C45DCF52CA9F7C97B', email = 'Paul.Ellery@relayware.com', 
matchname = 'Paul�Ellery�Paul.Ellery@relayware.com'
where username = 'Tikhoze Banda'

update usergroup
set name = 'Paul Ellery'
where name = 'Tikhoze Banda'

GO

-- Tim Rice (Front End Developer)
if not exists (select personid from person where FirstName = 'Tim' and LastName = 'Rice') 
begin
	insert into person (personid, salutation, firstname, lastname, username, password, passworddate, loginexpires, firsttimeuser, email, emailstatusold, 
	language, locationid, organisationid, active, createdby, created, lastupdatedby, lastupdated, emailstatus)
	values ((select max(personid) + 1 from person), 'Mr.', 'Tim', 'Rice', 'Tim Rice', '62630B260659595CC57792B7BB0CCA0D', DATEADD(mm,-1,getdate()), '2050-08-06 00:10:16.000',1,'tim.rice@relayware.com', 0,
	'English', 1046, 1, 1, 102, getdate(), 102, getdate(), 0)
	
	
	-- Add in approved flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1695, getdate(), 102, getdate(), 102
	from person
	where username = 'Tim Rice'
	
	-- Add in T's & C's flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1681, getdate(), 102, getdate(), 102
	from person
	where username = 'Tim Rice'
	
	-- Add in usergroup
	insert into usergroup (usergroupid, name, personid, createdby, created, lastupdatedby, lastupdated, usergrouptype)
	select (select max(usergroupid) + 1 from usergroup), 'Tim Rice', personid, 102, getdate(), 102, getdate(), 'Personal'
	from person where username = 'Tim Rice'
	
	-- Add into rightsgroup
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select (select personid from person where username = 'Tim Rice'), usergroupid, 102, getdate(), 102, getdate()
	from usergroup where name = 'Tim Rice'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Tim Rice'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Tim Rice'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Tim Rice'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Tim Rice'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Tim Rice'
	
	-- insert into rights
	insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
	select (select usergroupid from usergroup where name = 'Tim Rice'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
	from rights where usergroupid = 438

end

GO

-- Ryan added in Arsam (03.08.2016)
-- Arsam Sarabi(Front End Developer)
if not exists (select personid from person where FirstName = 'Arsam' and LastName = 'Sarabi')
begin
	insert into person (personid, salutation, firstname, lastname, username, password, passworddate, loginexpires, firsttimeuser, email, emailstatusold, 
	language, locationid, organisationid, active, createdby, created, lastupdatedby, lastupdated, emailstatus)
	values ((select max(personid) + 1 from person), 'Mr.', 'Arsam', 'Sarabi', 'Arsam Sarabi', 'E58172B6F669EE90823949B38416116B', DATEADD(mm,-1,getdate()), '2050-08-06 00:10:16.000',1,'arsam.sarabi@relayware.com', 0,
	'English', 1046, 1, 1, 102, getdate(), 102, getdate(), 0)
	
	
	-- Add in approved flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1695, getdate(), 102, getdate(), 102
	from person
	where username = 'Arsam Sarabi'
	
	-- Add in T's & C's flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1681, getdate(), 102, getdate(), 102
	from person
	where username = 'Arsam Sarabi'
	
	-- Add in usergroup
	insert into usergroup (usergroupid, name, personid, createdby, created, lastupdatedby, lastupdated, usergrouptype)
	select (select max(usergroupid) + 1 from usergroup), 'Arsam Sarabi', personid, 102, getdate(), 102, getdate(), 'Personal'
	from person where username = 'Arsam Sarabi'
	
	-- Add into rightsgroup
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select (select personid from person where username = 'Arsam Sarabi'), usergroupid, 102, getdate(), 102, getdate()
	from usergroup where name = 'Arsam Sarabi'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Arsam Sarabi'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Arsam Sarabi'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Arsam Sarabi'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Arsam Sarabi'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Arsam Sarabi'
	
	-- insert into rights
	insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
	select (select usergroupid from usergroup where name = 'Arsam Sarabi'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
	from rights where usergroupid = 438

end

GO

-- Ryan added in Martin Elgie (03.10.2016)
-- Martin Elgie(Dev)
if not exists (select personid from person where FirstName = 'Martin' and LastName = 'Elgie')
begin
	insert into person (personid, salutation, firstname, lastname, username, password, passworddate, loginexpires, firsttimeuser, email, emailstatusold, 
	language, locationid, organisationid, active, createdby, created, lastupdatedby, lastupdated, emailstatus)
	values ((select max(personid) + 1 from person), 'Mr.', 'Martin', 'Elgie', 'Martin Elgie', 'CD15A999ED69424A61B323E390DD6001', DATEADD(mm,-1,getdate()) , '2050-08-06 00:10:16.000',1,'Martin.Elgie@relayware.com', 0,
	'English', 1046, 1, 1, 102, getdate(), 102, getdate(), 0)
	
	
	-- Add in approved flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1695, getdate(), 102, getdate(), 102
	from person
	where username = 'Martin Elgie'
	
	-- Add in T's & C's flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1681, getdate(), 102, getdate(), 102
	from person
	where username = 'Martin Elgie'
	
	-- Add in usergroup
	insert into usergroup (usergroupid, name, personid, createdby, created, lastupdatedby, lastupdated, usergrouptype)
	select (select max(usergroupid) + 1 from usergroup), 'Martin Elgie', personid, 102, getdate(), 102, getdate(), 'Personal'
	from person where username = 'Martin Elgie'
	
	-- Add into rightsgroup
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select (select personid from person where username = 'Martin Elgie'), usergroupid, 102, getdate(), 102, getdate()
	from usergroup where name = 'Martin Elgie'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Martin Elgie'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Martin Elgie'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Martin Elgie'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Martin Elgie'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Martin Elgie'
	
	-- insert into rights
	insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
	select (select usergroupid from usergroup where name = 'Martin Elgie'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
	from rights where usergroupid = 438

end

-- Ryan added in Akash Payne (05.09.2016)
-- Akash Payne(BA)
if not exists (select personid from person where FirstName = 'Akash' and LastName = 'Payne')
begin
	insert into person (personid, salutation, firstname, lastname, username, password, passworddate, loginexpires, firsttimeuser, email, emailstatusold, 
	language, locationid, organisationid, active, createdby, created, lastupdatedby, lastupdated, emailstatus)
	values ((select max(personid) + 1 from person), 'Mr.', 'Akash', 'Payne', 'Akash Payne', '6084B91BD619FCC36BB5602DB81C5D6E', DATEADD(mm,-1,getdate()), '2050-08-06 00:10:16.000',1,'Akash.Payne@relayware.com', 0,
	'English', 1046, 1, 1, 102, getdate(), 102, getdate(), 0)
	
	
	-- Add in approved flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1695, getdate(), 102, getdate(), 102
	from person
	where username = 'Akash Payne'
	
	-- Add in T's & C's flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1681, getdate(), 102, getdate(), 102
	from person
	where username = 'Akash Payne'
	
	-- Add in usergroup
	insert into usergroup (usergroupid, name, personid, createdby, created, lastupdatedby, lastupdated, usergrouptype)
	select (select max(usergroupid) + 1 from usergroup), 'Akash Payne', personid, 102, getdate(), 102, getdate(), 'Personal'
	from person where username = 'Akash Payne'
	
	-- Add into rightsgroup
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select (select personid from person where username = 'Akash Payne'), usergroupid, 102, getdate(), 102, getdate()
	from usergroup where name = 'Akash Payne'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Akash Payne'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Akash Payne'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Akash Payne'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Akash Payne'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Akash Payne'
	
	-- insert into rights
	insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
	select (select usergroupid from usergroup where name = 'Akash Payne'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
	from rights where usergroupid = 438

end

GO

-- Ryan added in Akash Payne (05.09.2016)
-- Lukas(BA)
if not exists (select personid from person where FirstName = 'Lukas' and LastName = 'Obuchowski')
begin
	insert into person (personid, salutation, firstname, lastname, username, password, passworddate, loginexpires, firsttimeuser, email, emailstatusold, 
	language, locationid, organisationid, active, createdby, created, lastupdatedby, lastupdated, emailstatus)
	values ((select max(personid) + 1 from person), 'Mr.', 'Lukas', 'Obuchowski', 'Lukas Obuchowski', '28FE7CE83BE4CF08C7D51E8F650A9C16', DATEADD(mm,-1,getdate()), '2050-08-06 00:10:16.000',1,'Lukas.Obuchowski@relayware.com', 0,
	'English', 1046, 1, 1, 102, getdate(), 102, getdate(), 0)
	
	
	-- Add in approved flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1695, getdate(), 102, getdate(), 102
	from person
	where username = 'Lukas Obuchowski'
	
	-- Add in T's & C's flag
	insert into booleanflagdata (entityid, flagid, created, createdby, lastupdated, lastupdatedby)
	select personid, 1681, getdate(), 102, getdate(), 102
	from person
	where username = 'Lukas Obuchowski'
	
	-- Add in usergroup
	insert into usergroup (usergroupid, name, personid, createdby, created, lastupdatedby, lastupdated, usergrouptype)
	select (select max(usergroupid) + 1 from usergroup), 'Lukas Obuchowski', personid, 102, getdate(), 102, getdate(), 'Personal'
	from person where username = 'Lukas Obuchowski'
	
	-- Add into rightsgroup
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select (select personid from person where username = 'Lukas Obuchowski'), usergroupid, 102, getdate(), 102, getdate()
	from usergroup where name = 'Lukas Obuchowski'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 4, 102, getdate(), 102, getdate() from person where username = 'Lukas Obuchowski'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 24, 102, getdate(), 102, getdate() from person where username = 'Lukas Obuchowski'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 472, 102, getdate(), 102, getdate() from person where username = 'Lukas Obuchowski'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 515, 102, getdate(), 102, getdate() from person where username = 'Lukas Obuchowski'
	
	insert into rightsgroup (personid, usergroupid, createdby, created, lastupdatedby, lastupdated)
	select personid, 516, 102, getdate(), 102, getdate() from person where username = 'Lukas Obuchowski'
	
	-- insert into rights
	insert into rights (usergroupid, Securitytypeid, countryid, menuid, permission, created, lastupdated)
	select (select usergroupid from usergroup where name = 'Lukas Obuchowski'), securitytypeid, countryid, menuid, permission, getdate(), getdate()
	from rights where usergroupid = 438

end

GO

-----------------------------------------------------
-- update relayware users with language editing rights
-----------------------------------------------------
--English
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 385, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 385) and loginexpires > getdate()

--French
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 353, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 353) and loginexpires > getdate()

--Spanish
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 386, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 386) and loginexpires > getdate()

--German
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 388, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 388) and loginexpires > getdate()

--Portuguese
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 389, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 389) and loginexpires > getdate()

--Add new phrase
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 440, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 440) and loginexpires > getdate()

--Japanese
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 1791, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 1791) and loginexpires > getdate()

--Italian
insert into booleanflagdata (flagid, entityid, created, createdby, lastupdated, lastupdatedby)
select 390, personid, getdate(), 102, getdate(), 102
from person where organisationid = 1 and personid not in (select entityid from booleanflagdata where flagid = 390) and loginexpires > getdate()

GO

--------------------------------------------
-- Update Mustafa so that he is not expired
--------------------------------------------
update person
set passworddate = DATEADD(mm,-1,getdate()), password = 'B88F89FA1022453090DEC766167A5869'
where username = 'Mustafa Shairani'


--sql updates

--Phrases

update Phrases set PhraseText = 'Quantity' where Phraseid in (select phraseid from phraselist where PhraseTextID = 'incentive_Claim_quantity')



-------------------------------------------
-- Add in India screendef info
-------------------------------------------
--Add in new country into valid value list so that the correct country appears
--in the dropdown list in the standard address format screen. (1 row)
IF NOT EXISTS (select * from usergroupcountry where usergroupid = 404 and countryid = 275)
				BEGIN
					insert into usergroupcountry (usergroupid, countryid)
					values (404,275)
				END

--updateScreenDefinitionScope (360 rows)
insert into countryscope (entityid,entity,countryid)
			select distinct entityID, 'screendefinition', 275
			from countryscope 
			where entityid in (select itemid from screendefinition where countryid = 0)
			and entity = 'screendefinition' and entityid not in (
				select entityid from countryscope where countryid = 275 and entity = 'screendefinition'
			)

/* NJH 2016/11/17 - some more configuration changes */
update person
set firstname = 'Michael', lastname = 'Turnbull', username = 'Michael Turnbull', password = '31EC33114F7362E93503A46CC7967141', email = 'Michael.Turnbull@relayware.com', 
matchname = 'Michael�Turnbull�Michael.Turnbull@relayware.com'
where username = 'Mikron Licud'

update usergroup
set name = 'Michael Turnbull'
where name = 'Mikron Licud'

update person
set firstname = 'Chris', lastname = 'Taniguchi', username = 'Chris Taniguchi', password = 'A180E29690FA6533F5BF0DD67457A514', email = 'Chris.Taniguchi@relayware.com', 
matchname = 'Chris�Taniguchi�Chris.Taniguchi@relayware.com'
where username = 'Richard Tingle'

update usergroup
set name = 'Chris Taniguchi'
where name = 'Richard Tingle'


----------------------
-- PROD2016-2666
----------------------
update phrases
set phrasetext = ''
where phraseid in 
       (select pl.phraseid 
              from phraselist pl join element e on e.id = pl.entityid
              where e.id = '999' and pl.phrasetextid = 'detail' and entitytypeid = 10)


/* NJH 2016/11/24 JIRA PROD2016-2218 (lead screens) - set the default lead screen to 2 */
if not exists(select 1 from settings where name='versions.leadScreen')
	insert into settings (name,value) values ('versions.leadScreen',2)


-- Jira ticket - PRS-99 -Country Group Changes
-- Add Guernsey 'All countries'
insert into countrygroup (Countrygroupid, countrymemberid)
values (37, 128)

-- update EMEA countryGroup
-- Remove countries Antigua And Barbuda, Aruba, Brunei Darussalam, French Guiana, French Polynesia, Guadeloupe, Lao People's Democratic Republic (Laos), Maldives,
-- Marshall Islands, Mayotte, Mexico, New Caledonia, Saint Vincent And The Grenadines, Samoa, Turks And Caicos Islands, United States Minor Outlying Islands
delete from countrygroup where countrygroupid = 443 and countrymemberid in (284,286,299,328,329,336,354,362,364,367,368,378,400,401,421,424)

--  Add to EMEA CountryGroup
-- Central African Republic, Eritrea, Faroe Islands, Guernsey, Guinea, Guinea-Bissau, Iran, Jersey, Lesotho, Malawi, Mali, Montenegro, Senegal, Somalia , Sudan,
-- Swaziland, Togo, Uganda, Vatican City, Western Sahara, Zambia
insert into countrygroup (countrygroupid, countrymemberid)
select 443, countryid 
from country
where countryid in (305,323,326,128,339,340,347,127,355,363,372,404,408,60,413,417,423,344,432,433)


-- remove some fields from modEntityDef where they no longer exist on the location table
delete from modEntityDef where tablename='location' and fieldname='matchSitename'
delete from modEntityDef where tablename='location' and fieldname='ReceivedFromSSG'
delete from modEntityDef where tablename='location' and fieldname='SentToSSG'
delete from modEntityDef where tablename='location' and fieldname='SiteType'

/* NJH 2016/12/13 JIRA PROD2016-2953 */
declare @oppSourceID int
select @oppSourceID = max(OpportunitySourceID) from oppSource

insert into oppSource (OpportunitySourceID,opportunitySource)
select ROW_NUMBER()  over (order by lookupID)+@oppSourceID, itemText from lookupList ll
	left join oppSource s on ll.itemText = s.opportunitySource
where fieldname='leadSourceID'
	and s.opportunitySource is null

