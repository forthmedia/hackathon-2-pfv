/* 
	WAB/SB 2015-10-21 Update all email phrases to include a header and a footer phrase  

*/

update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Order Confirmation</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Order Confirmed</span></h3>

						<p style="line-height: 20px">Your order has been confirmed and will now be processed. Please review the information below and contact us immediately if there are any inconsistencies. In the unlikely event that your order cannot be processed, we will contact you directly to discuss a solution.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Order Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Order From Opportunity:</b></td>
									<td class="rColumn">[[opportunity.Detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Customer Contact:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Customer Organization:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.organisationName]]</td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Items Ordered</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Items:</b></td>
									<td class="rColumn"><relay_loop query=opportunity.opportunityproducts></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Description:</b></td>
									<td class="rColumn">[[opportunity.opportunityproducts.description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Quantity:</b></td>
									<td class="rColumn">[[opportunity.opportunityproducts.quantity]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Unit Price:</b></td>
									<td class="rColumn">[[opportunity.opportunityproducts.unitprice]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Discount:</b></td>
									<td class="rColumn">[[opportunity.opportunityproducts.discount]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Sub total:</b></td>
									<td class="rColumn">[[opportunity.opportunityproducts.subtotal]]</relay_loop></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Grand Total:</b></td>
									<td class="rColumn">[[opportunity.currency]][[opportunity.grandTotal]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any queries regarding this order, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33335



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Opportunity Created</span></h3>

						<p style="line-height: 20px">A new opportunity has been created with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[opportunity.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity ID:</b></td>
									<td class="rColumn">[[opportunity.opportunityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[opportunity.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[opportunity.partner.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[opportunity.partner.OrganisationName]]</td>
								</tr>
							</tbody>
						</table>

						<p>To access this opportunity, please <a href="[[getInternalSiteURL()]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">Click Here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33359



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Opportunity Updated</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Opportunity Updated</span></h3>

						<p style="line-height: 20px">An opportunity under your supervision has been updated. As the account manager, you may wish to view the progress of this opportunity below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[opportunity.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[opportunity.partner.Organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[opportunity.stage]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this email, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33361



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Registration Approval</span></h3>

						<p style="line-height: 20px">A new deal has been submitted by [[opportunity.partner.OrganisationName]]. Please review the following information and set its approval status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[opportunity.partner.FirstName]] [[opportunity.partner.Lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[opportunity.partner.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[Detail]]</td>
								</tr>
							</tbody>
						</table>

						<p>In order to set this deal''s approval status, please <a href="[[func.getInternalSiteURL(1)]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">click here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33363



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Action Created</span></h3>

						<p style="line-height: 20px">[[owner]] has given you shared rights to the following action:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Action:</b></td>
									<td class="rColumn">[[ActionWith]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Action ID:</b></td>
									<td class="rColumn">[[ActionID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Action Description:</b></td>
									<td class="rColumn">[[description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Due Date:</b></td>
									<td class="rColumn">[[DueDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33501



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3></h3>

						<p style="line-height: 20px">You have created a new opportunity with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[opportunity.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity ID:</b></td>
									<td class="rColumn">[[opportunity.opportunityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[opportunity.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Phone:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.directLine]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Email:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.email]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding the above, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
phr_email_footer
			</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 33502



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px; width=600px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Forgotten Your Password?</span></h3>

						<p style="line-height: 20px">To create a new Internal [[getSiteName()]] password for request.currentsite.domain]], click the link below. If you didn''t request this email, please disregard the following contents.</p>

						<p>Remember, a password must contain 6 characters and a strong password should include:</p>

						<ul>
							<li>Letters</li>
							<li>Numbers</li>
							<li>Symbols</li>
						</ul>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="600px">
							<tbody>
								<tr>
									<td class="lColumn"><b>Username:</b></td>
									<td class="rColumn">[[username]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Password:</b></td>
									<td class="rColumn"><a href="[[getSiteURL()]]/resendPW.cfm?spl=[[request.emailmerge.generatePasswordLink(personID=personID)]]">Click Here</a> to create a new password.</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33504



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>New Registration</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Person Notification</span></h3>

						<p style="line-height: 20px">A new Partner Portal application has been received with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[firstname]] [[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>User Status:</b></td>
									<td class="rColumn">[[PerApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization Status:</b></td>
									<td class="rColumn">[[OrgApprovalStatus]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33506



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Partner Application</span></h3>

						<p style="line-height: 20px">A new partner has applied to join the Partner Portal. Please review their application below and modify their approval status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Applicant Name:</b></td>
									<td class="rColumn">[[firstname]] [[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Submitted Organization:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Address:</b></td>
									<td class="rColumn">[[Address1]]<br />
									[[Address4]]<br />
									[[State]]<br />
									[[postalCode]]<br />
									[[Country]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Phone:</b></td>
									<td class="rColumn">[[DirectLine]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Email:</b></td>
									<td class="rColumn">[[Email]]</td>
								</tr>
							</tbody>
						</table>

						<p>An email has also been sent notifying the partner that their application is being evaluated.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33507



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Registration Pending</span></h3>

						<p>Your details have been uploaded to our system and will be verified shortly.</p>

						<p>We''ll let you know when this process is complete.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33508



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear Colleague,</h2>

						<h3><span style="color:#1189ca;">Partner Portal Invitation</span></h3>

						<p>You''ve been invited to join the [[getSiteName()]] Partner Portal.</p>

						<p style="line-height: 20px">To get started, <a href="[[func.getExternalSiteURL(2)]]/et.cfm?etid=InvitedReg&amp;pnumber=[[magicnumber]]">Click Here</a>. After the registration process is complete, you''ll have access to our company account.</p>

						<p>If you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33509



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Application Unsuccessful</span></h3>

						<p style="line-height: 20px">Unfortunately the application for your organization''s membership to join the [[getSiteName()]] Partner Portal has been unsuccessful.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33510



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px; width=100%;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Forgotten Your Password?</span></h3>

						<p style="line-height: 20px">To create a new Portal password, click the link below. If you didn''t request this email, please disregard the following contents.</p>

						<p>Remember, a password must contain 6 characters. A strong password should include:</p>

						<ul>
							<li>Letters</li>
							<li>Numbers</li>
							<li>Symbols</li>
						</ul>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="600px">
							<tbody>
								<tr>
									<td class="lColumn"><b>Username:</b></td>
									<td class="rColumn">[[username]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Password:</b></td>
									<td class="rColumn"><a href="[[func.getExternalSiteURL(2)]]/index.cfm?eid=resend_password&amp;spl=[[request.emailmerge.generatePasswordLink(personID=personID)]]">Click Here</a> to create a new password.</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33511



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Application Unsuccessful</span></h3>

						<p style="line-height: 20px">Unfortunately the application for your organization''s membership to join the [[getSiteName()]] Partner Portal has been unsuccessful. In consequence, your user application has also been unsuccessful.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33512



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Person Notification</span></h3>

						<p style="line-height: 20px">A new Partner Portal application has been received with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[firstname]] [[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>User Status:</b></td>
									<td class="rColumn">[[PerApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lcolumn"><b>Organization Status:</b></td>
									<td class="rcolumn">[[OrgApprovalStatus]]</td>
								</tr>
							</tbody>
						</table>

						<p>Once you''ve reviewed the above, please log into the Portal''s ''Manage My Colleagues'' section to approve this user.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33514



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Specialization Expired</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 100%; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Specialization Expired</span></h3>

						<p style="line-height: 20px">Unfortunately your specialization has expired. The details of this can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable">
							<tbody>
								<tr>
									<td class="lColumn"><b>Specialization:</b></td>
									<td class="rColumn">[[specialisation]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Expire Date:</b></td>
									<td class="rColumn">[[expiredDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33515



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Specialization Achieved</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 100%; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:600px;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Specialization Achieved!</span></h3>

						<p style="line-height: 20px">Congratulations! Your achievement can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="600px">
							<tbody>
								<tr>
									<td class="lColumn"><b>Specialization:</b></td>
									<td class="rColumn">[[specialisation]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Pass Date:</b></td>
									<td class="rColumn">[[passDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33516



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Certification Rules Added</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Important Certification Update</span></h3>

						<p style="line-height: 20px">The rules regarding [[Certification]] have been modified. You are advised to review this certification as soon as possible.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33517



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Update Required</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Certification Update Required</span></h3>

						<p style="line-height: 20px">The rules regarding [[Certification]] have been modified. As a result, you are now required to complete the additional modules to pass. <a href="[[func.getExternalSiteURL(2)]]/et.cfm?eid=modules&amp;certificationid=[[CertificationID]]">Click Here</a> to access and complete these additional modules.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33518



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Certification Passed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Congratulations!</span></h3>

						<p style="line-height: 20px">You have passed the following certification:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[Certification]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Pass Date:</b></td>
									<td class="rColumn">[[PassDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33520



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Certification Expired</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Certification Expired</span></h3>

						<p style="line-height: 20px">Unfortunately a certification has expired. The details of this can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Certification:</b></td>
									<td class="rColumn">[[Certification]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Expired on:</b></td>
									<td class="rColumn">[[ExpireDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33521



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Certification Cancelled</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Certification Cancelled</span></h3>

						<p style="line-height: 20px">[[certification]] was successfully cancelled on [[CancellationDate]].</p>

						<p>If you have any queries regarding this email, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33522



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Training Order Placed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 100%; height:20px;}
table.header{ padding-bottom:5px; width: 100%; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Training Order Placed</span></h3>

						<p style="line-height: 20px">Thanks for placing an order for Training Materials. We will now process your order and notify you when this is complete. Please review your order details and inform us of any inconsistencies. In the unlikely event that your order cannot be processed, we will contact you directly to discuss a solution.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center; width: 100%;"><b>Order Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Order Number:</b></td>
									<td class="rColumn">[[orderItems.recordCount]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Number of Items:</b></td>
									<td class="rColumn">[[orderItems.recordCount]]</td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td><b>Items Ordered</b></td>
									<td><relay_look query=merge.orderItems></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Product:</b></td>
									<td class="rColumn">[[orderItems.sku]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Description:</b></td>
									<td class="rColumn">[[orderItems.description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Quantity:</b></td>
									<td class="rColumn">[[orderItems.quantity]]</relay_loop></td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Shipping Address</b></td>
									<td>[[delcontact]]<br />
									[[delsitename]]<br />
									[[delAddress1]]<br />
									[[delPostalCode]]<br />
									[[delCountryName]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this order, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33523



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Quiz Passed With Rewards</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 100%; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:600px;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Congratulations!</span></h3>

						<p style="line-height: 20px">You''ve passed [[QuizName]] with rewards! The details of your score can be found below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Pass Mark:</b></td>
									<td class="rColumn">[[passMark]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Score:</b></td>
									<td class="rColumn">[[score]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Points Earned:</b></td>
									<td class="rColumn">[[pointsEarned]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33524



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Quiz Failed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Quiz Failed</span></h3>

						<p style="line-height: 20px">Unfortunately you have not passed your recent attempt at [[QuizName]].</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33525



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Quiz Passed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 100%; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Congratulations!</span></h3>

						<p style="line-height: 20px">You''ve passed [[QuizName]]! The details of your score can be found below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Pass Mark:</b></td>
									<td class="rColumn">[[passMark]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Score:</b></td>
									<td class="rColumn">[[score]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33526



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Deal Approved</span></h3>

						<p style="line-height: 20px">Congratualtions, your deal has been reviewed and approved. The details of this deal are as follows:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Address:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.Address1]]</td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Opportunity Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity ID:</b></td>
									<td class="rColumn">[[opportunity.OpportunityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity Status:</b></td>
									<td class="rColumn">[[opportunity.Status]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity Detail:</b></td>
									<td class="rColumn">[[opportunity.Detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[opportunity.Stage]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Expected Close Date:</b></td>
									<td class="rColumn">[[opportunity.ExpectedCloseDate]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Current Situation:</b></td>
									<td class="rColumn">[[opportunity.CurrentSituation]]</td>
								</tr>
							</tbody>
						</table>

						<p>The partner representative for this deal is: [[opportunity.Partner.FullName]]</p>

						<p>If you have any questions regarding this information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33527



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Deal Unsuccessful</span></h3>

						<p style="line-height: 20px">Unfortunately the following deal has been declined and no further action will be taken.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Address:</b></td>
									<td class="rColumn">[[opportunity.EndCustomer.Address1]]</td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Opportunity Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity ID:</b></td>
									<td class="rColumn">[[opportunity.OpportunityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity Status:</b></td>
									<td class="rColumn">[[opportunity.Status]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity Detail:</b></td>
									<td class="rColumn">[[opportunity.Detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[opportunity.Stage]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Expected Close Date:</b></td>
									<td class="rColumn">[[opportunity.ExpectedCloseDate]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Current Situation:</b></td>
									<td class="rColumn">[[opportunity.CurrentSituation]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33528



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">One Last Thing...</span></h3>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Purchase Order number:</b></td>
									<td class="rColumn">[[fundRequestActivityID]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">Thanks for submitting the Proof of Performance (PoP) for your [[getSiteName()]] marketing activity.</p>

						<p style="line-height: 20px">Before you can receive your funds, you must provide the following additional documentation:<br />
						[[Documentation Required]]</p>

						<p style="line-height: 20px">To guarantee you''ll get your Marketing Development Funds, make sure you submit the relevant documentation as soon as possible and before the end of this quarter. PoP must be submitted in full via [[func.getExternalSiteURL(2)]].</p>

						<p style="line-height: 20px">Once this is done, your funds will be paid to your nominated bank account within 30 days of the above documentation being approved.</p>

						<p style="line-height: 20px">If you''d like more information on this, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33529



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Marketing Development Funds</span>*</h3>

						<p>The following email contains the details of your available MDF for this quarter.</p>
						

						<table border="0" cellpadding="0" cellspacing="0" class="budget" width="100%">
							<tbody>
								<tr>
									<td>Quarter:<br />
									[[BudgetQuarter]]
									<p style="margin: 0px; line-height: 5px;"></p>
									</td>
									<td>Start date:<br />
									[[BudgetStartDate]]
									<p style="margin: 0px; line-height: 5px;"></p>
									</td>
									<td>End date:<br />
									[[BudgetEndDate]]
									<p style="margin: 0px; line-height: 5px;"></p>
									</td>
								</tr>
							</tbody>
						</table>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Customer Name:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[firstname]]&nbsp;[[lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Email:</b></td>
									<td class="rColumn">[[email]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Telephone:</b></td>
									<td class="rColumn">[[telephone]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Issue Date:</b></td>
									<td class="rColumn">[[issueDate]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>[[budgetholder]]<br />
									MDF available:</b></td>
									<td class="rColumn">$[[approvedamount]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">To apply for these funds, log into [[func.getExternalSiteURL(2)]] and click on the <b>''Programs &amp; MDF Application Form''</b> link.</p>

						<p style="font-size:8px; line-height: 15px">*These discretionary Marketing Development Funds are available for agreed joint marketing activities. An agreed plan must be in place from the beginning of the quarter and satisfactory evidence of activity must be received before the rebate is made. This evidence must be received within 2 weeks from the end of the quarter in which the activity is made. All MDF rebates will be made via a credit into your trading account.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33530



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Application Cancelled</span></h3>

						<p style="line-height: 20px">We have cancelled your MDF application as requested.</p>

						<p>If you''d like more information on this, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33531



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Funds Expired</span></h3>

						<p style="line-height: 20px">Unfortunately our records show that [[organisationname]] are yet to submit a marketing plan for this quarter. Because of this, your marketing funds for this quarter have expired</p>

						<p style="line-height: 20px">In order to qualify for Marketing Development Funds per quarter, you are required to submit a marketing plan before the end of the first month within that quarter. You may still be entitled to funds for the next quarter and, if you are, we''ll let you know.</p>

						<p>If you have any questions, or would like more information on this, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33532



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Funds Expired</span></h3>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="600px">
							<tbody>
								<tr>
									<td class="lColumn"><b>Purchase Order Number:</b></td>
									<td class="rColumn">[[fundRequestActivityID]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">Unfortunately our records show that no Proof of Performance (PoP) has been received by [[organisationname]] for the above application. In order to qualify for Marketing Development Funds, it is required that PoP documentation is submitted before the end of the first month after the related quarter.</p>

						<p>You may still be entitled to funds for the next quarter and, if so, we''ll let you know.</p>

						<p>If you''d like more information on this, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33533



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Funds Paid</span></h3>

						<p style="line-height: 20px">We''re delighted to confirm that the sum of $[[approvedAmount]] has been transferred into your nominated bank account.</p>

						<p style="line-height: 20px">If you''d like to review the summary of this transaction, and further details of your application, please log into [[getSiteName()]] Partner Portal and navigate to Marketing Development Funds.</p>

						<p>If the transaction does not show in your account within ten working days, feel free to contact us</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33534



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">All Documentation Received</span></h3>

						<p style="line-height: 20px">We''ve now received all supporting documentation for the following claim:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Company Name:</b></td>
									<td class="rColumn">[[companyName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Country</b></td>
									<td class="rColumn">[[partnerCountry]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Fund Activity ID</b></td>
									<td class="rColumn">[[fundRequestActivityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Approved Sum:</b></td>
									<td class="rColumn">$[[approvedAmount]]</td>
								</tr>
							</tbody>
						</table>

						<p>Please review this information and proceed to make the payment to this Partner.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33535



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Funds Paid</span></h3>

						<p style="line-height: 20px">We''re delighted to confirm that the sum of $[[approvedAmount]] has been transferred into your nominated bank account.</p>

						<p style="line-height: 20px">If you''d like to review the summary of this transaction, and further details of your application, please log into [[getSiteName()]] Partner Portal and navigate to Marketing Development Funds.</p>

						<p>If the transaction does not show in your account within ten working days, feel free to contact us</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33536



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Proof of Purchase Submitted</span></h3>

						<p style="line-height: 20px">Thanks! Your Proof of Purcahse has now been submitted for your marketing plan, Purchase Order Number [[fundRequestActivityID]], and will now be processed.</p>

						<p>Until then, if you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33537



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">2 Week Reminder</span></h3>

						<p style="line-height: 20px">It''s been 2 weeks since you''ve submitted your marketing plan, Purchase Order Number [[fundRequestActivityID]], and we''ve noticed you''re yet to submit your Proof of Performance documentation.</p>

						<p>In order to be eligible for funding, you must provide the following documentation:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="rColumn">[[popRequired]]</td>
								</tr>
								<tr>
									<td class="rColumn">[[comments]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33538



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Funding Request Approved</span></h3>

						<p style="line-height: 20px">We''re delighted to inform you that your Purchase Order Number [[fundRequestActivityID]] has been approved and the details can be found below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Activity Type:</b></td>
									<td class="rColumn">[[fundRequestType]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Total Approved:</b></td>
									<td class="rColumn">[[approvedAmount]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">Payment will be made into your nominated account within 30 days of the above documentation being processed.</p>

						<p style="line-height: 20px">Remember, Proof of Performance <b>must</b> be submitted and received within two weeks after the respective quarter ends. If this documentation hasn''t been received, [[getSiteName()]] reserves the right to withdraw approval of this funding.</p>

						<p style="line-height: 20px">If you have any questions regarding this information, feel free to contact us quoting your Purchase Order Number.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33539



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Fund Request Approval</span></h3>

						<p style="line-height: 20px">[[companyName]], [[PartnerCountry]] have submitted an application for the following activity:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Type:</b></td>
									<td class="rColumn">[[fundRequestType]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Description:</b></td>
									<td class="rColumn">[[fundclaimdescription]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Total:</b></td>
									<td class="rColumn">[[fundclaimamount]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">Please review the above information and, if you approve of the application,<a href="[[getSiteURL()]]/index.cfm?fundRequestActivityid=[[fundRequestActivityID]]%26mode=edit&amp;st=012"> Click Here</a></p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33540



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Fund Request Final Approval</span></h3>

						<p style="line-height: 20px">[[companyName]], [[PartnerCountry]] have submitted an application for the following activity:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Type:</b></td>
									<td class="rColumn">[[fundRequestType]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Description:</b></td>
									<td class="rColumn">[[fundclaimdescription]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Total:</b></td>
									<td class="rColumn">[[fundclaimamount]]</td>
								</tr>
							</tbody>
						</table>

						<p style="line-height: 20px">This application has been approved by the <b>Admin Team</b> and a <b>Level 2 Approver</b>. If you approve of this application, please <a href="[[getSiteURL()]]/index.cfm?fundRequestActivityid=[[fundRequestActivityID]]%26mode=edit&amp;st=012">Click Here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33541



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Fund Request Incomplete</span></h3>

						<p style="line-height: 20px">Thanks for submitting your MDF Program application. Unfortunately your application is missing required criteria and, in consequence, we are unable to proceed. In order to rectify this, please review the following areas of your application:</p>

						<p>[[comments]]</p>

						<p style="line-height: 20px">To amend your application, log into your respective portal and follow the Marketing Development Fund links and, If you have any questions regarding this email, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33542



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Claim Submitted</span></h3>

						<p style="line-height: 20px">[[companyName]], [[PartnerCountry]] have submitted a new claim for review. Please <a href="[[getSiteURL()]]/index.cfm?fundRequestActivityid=[[fundRequestActivityID]]%26mode=edit&amp;st=012">Click Here</a> to examine the application and action it accordingly.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33543



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;"><style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Application Submitted</span></h3>

						<p style="line-height: 20px">Thanks for submitting your MDF Program application. Your proposal has been received and is currently being reviewed. Once this process is complete, you will be notified whether your application has been successful.</p>

						<p>Bare in mind, this email is not a confirmation, nor guarantee, that funds will be made available</p>

						<p>If you have any queries, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33544



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">MDF Application Unsuccessful</span></h3>

						<p style="line-height: 20px">Thanks for submitting an MDF Program application. Unfortunately your application has been unsuccessful and will no longer be processed.</p>

						<p>The reasons for this unsuccessful application are:<br />
						[[comments]]</p>

						<p>If you have any questions regarding the above information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33545



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Order Approval</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Order Awaiting Approval</span></h3>

						<p style="line-height: 20px">The below order is currently awaiting approval.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Customer Contact:</b></td>
									<td class="rColumn">[[EndCustomer.FirstName]] [[EndCustomer.LastName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Customer Organization:</b></td>
									<td class="rColumn">[[EndCustomer.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Order Detail:</b></td>
									<td class="rColumn">[[Detail]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 33553



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Lead Available</span></h3>

						<p style="line-height: 20px">A new Lead has become available through the [[getSiteName()]] Locator. The details of this Lead can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>Lead Information</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[LocatorFirstName]] [[LocatorLastName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[LocatorOrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Email:</b></td>
									<td class="rColumn">[[LocatorContactEmail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Phone:</b></td>
									<td class="rColumn">[[LocatorPhoneNumber]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Address:</b></td>
									<td class="rColumn">[[locatoraddress1]]<br />
									[[locatoraddress2]]<br />
									[[locatorcity]]<br />
									[[locatorpostalcode]]<br />
									[[Countryname]]</td>
								</tr>
								<tr>
									<td class="lColumn">Comments:</td>
									<td class="rColumn">[[locatorInterestproductName]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this email, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 34370



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Incentive Registration Complete</span></h3>

						<p style="line-height: 20px">Thanks for registering with the Incentive Program. Once your details have been reviewed you will be notified and your new login details will be sent, by email, to this address.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 34376



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Sales Opportunity Assigned</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Opportunity Reassigned</span></h3>

						<p style="line-height: 20px">The following opportunity has been reassigned to you.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[opportunity.partner.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[opportunity.partner.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[opportunity.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>OpportunityID:</b></td>
									<td class="rColumn">[[opportunity.opportunityID]]</td>
								</tr>
							</tbody>
						</table>

						<p>In order to access this opportunity, please <a href="[[getInternalSiteURL()]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">Click Here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 35722



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Enquiry</span></h3>

						<p style="line-height: 20px">Hey there, [[firstname]] [[lastname]] from [[organizationName]] has attempted to contact your company through the [[getSiteName()]] Locator. Their enquiry details are listed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Email:</b></td>
									<td class="rColumn">[[email]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Phone:</b></td>
									<td class="rColumn">[[person.officePhone]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Enquiry:</b></td>
									<td class="rColumn">[[enquiry]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Current Situation:</b></td>
									<td class="rColumn">[[CurrentSituation]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you''d like more details on the above information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 35788



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Opportunities Reassigned</span></h3>

						<p style="line-height: 20px">The following opportunities have been reassigned to you.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>List of Opportunities:</b></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center">[[listOfOpportunities]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 35802



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Opportunities Reassigned</span></h3>

						<p style="line-height: 20px">The following opportunities have been reassigned to different account managers:</p>

						<p>[[listOfOpportunities]]</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 35804



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">One More Step...</span></h3>

						<p style="line-height: 20px">Thanks for registering with the [[getSiteName()]] Partner Portal. Before you can access our Reseller Portal, you need to create a memorable password. Passwords should always contain 6 characters. The most secure passwords always contain a mix of letters, numbers, and symbols.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Username:</b></td>
									<td class="rColumn">[[username]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Create Your Password:</b></td>
									<td class="rColumn"><a href="[[func.getExternalSiteURL(2)]]/index.cfm?eid=resend_password&amp;spl=[[request.emailmerge.generatePasswordLink(personID=personID)]]">Click Here</a></td>
								</tr>
							</tbody>
						</table>

						<p>If you have any queries on the above content, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 35806



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Registration Approved</span></h3>

						<p style="line-height: 20px">Welcome to the [[getSiteName()]] Incentive Program. Your new login details are listed below. Please click on the link blow to create a new password. If you have any questions regarding the Incentives Program, feel free to contact us.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Username:</b></td>
									<td class="rColumn">[[username]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Password:</b></td>
									<td class="rColumn"><a href="[[func.getExternalSiteURL(2)]]/index.cfm?eid=resend_password&amp;spl=[[request.emailmerge.generatePasswordLink(personID=personID)]]">Click Here</a></td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>


</body>
</html>'
 where ident = 35808



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Password Recovery</span></h3>

						<p style="line-height: 20px">To reset your password, please click the link below. If you didn''t request this email, please disregard its contents.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Username:</b></td>
									<td class="rColumn">[[username]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Password:</b></td>
									<td class="rColumn"><a href="[[func.getExternalSiteURL(2)]]/index.cfm?eid=resend_password&amp;spl=[[request.emailmerge.generatePasswordLink(personID=personID)]]">Click Here</a></td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 35810



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Approval</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Business Plan Approval</span></h3>

						<p style="line-height: 20px">A new business plan has been submitted and is now ready for approval.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Contact:</b></td>
									<td class="rColumn">[[person.Firstname]] [[person.lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
							</tbody>
						</table>

						<p>Please <a href="[[func.getInternalSiteURL(1)]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">Click Here</a> to review this plan and set its approval status accordingly.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36012



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Submitted</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Business Plan Submitted</span></h3>

						<p style="line-height: 20px">Thanks for submitting your latest business plan, [[businessplanname]], for review. We''ll let you know when this process is complete.</p>

						<p>If you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36283



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Review</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Business Plan</span></h3>

						<p style="line-height: 20px">Your business plan ''[[businessPlan.PlanTitle]]'' needs revisions. Please review the approval status of your business plan within the business plan editor.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Sales plan status:</b></td>
									<td class="rColumn">[[businessPlan.SalesApproval]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Marketing plan status:</b></td>
									<td class="rColumn">[[businessPlan.MarketingApproval]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Training plan status:</b></td>
									<td class="rColumn">[[businessPlan.trainingApproval]]</td>
								</tr>
							</tbody>
						</table>

						<p>The approvers will be made once your business plan is re-submitted.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36287



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Approval</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3>Business Plan Approved</h3>

						<p>Your business plan, [[businessPlan.PlanTitle]], has been approved. You may now review the plan, and make any changes, from within the partner portal.</p>

						<p>Kind regards,<br />
						[[getSiteName()]]</p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 36291



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Rejected</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Business Plan Rejected</span></h3>

						<p style="line-height: 20px">We regret to inform you that you business plan, [[businessPlan.planTitle]], has been unsuccessful and the application will be archived.</p>

						<p style="line-height: 20px">If you have any questions regarding your unsuccessful application, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36295



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Updated</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Business Plan Updated</span></h3>

						<p style="line-height: 20px">Your business plan, [[businessplanname]], has been updated and submitted for review. We''ll let you know when this process is complete.</p>

						<p>If you have any further questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36299



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Updated</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Business Plan Updated</span></h3>

						<p style="line-height: 20px">The business plan, [[businessplanname]], has been updated by [[organisationname]]. Please review their changes and set the approval status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Organization Name:</b></td>
									<td class="rColumn">[[organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Business Plan Name:</b></td>
									<td class="rColumn">[[BusinessplanName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Updated On:</b></td>
									<td class="rColumn">[[UpdateDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36303



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Submitted</span></h3>

						<p style="line-height: 20px">Your support case has been submitted and will be reviewed shortly. Meanwhile, your case details are listed below and, if you have any queries, feel free to contact us.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Case ID:</b></td>
									<td class="rColumn">[[SupportCase.SupportCaseID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Case Subject:</b></td>
									<td class="rColumn">
									<p>[[showProfileAttributeValue(ProfileAttributeID=2372)]]</p>

									<p>[[showProfileValue(ProfileID=''Case_Status'')]]</p>
									</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Created On:</b></td>
									<td class="rColumn">[[SupportCase.Created]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 36308



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Review</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Review</span></h3>

						<p style="line-height: 20px">A new support case has been submitted by [[organisationName]] for review. Please process this case and set its status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Organization Name:</b></td>
									<td class="rColumn">[[organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Support Case ID:</b></td>
									<td class="rColumn">[[SupportCase.SupportCaseID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Submit Date:</b></td>
									<td class="rColumn">[[SupportCase.Created]]</td>
								</tr>
							</tbody>
						</table>

						<p>The partner has been notified that their support case is being processed.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36312



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Status</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Status</span></h3>

						<p style="line-height: 20px">Your support case, [[SupportCase.SupportCaseID]], status has been modified to: [[showProfileValue(ProfileID=''Case_Status'')]].</p>

						<p style="line-height: 20px">If you have any questions regarding this email, or if you need further assistance with your support case, feel free to contact us quoting your case ID, [[SupportCase.SupportCaseID]].</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36316



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Updated</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>


<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Updated</span></h3>

						<p style="line-height: 20px">Your support case, [[case.ID]], has been updated and submitted for review. You will be notified once this process is complete.</p>

						<p>If you have any further questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 36320



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Closed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Closed</span></h3>

						<p style="line-height: 20px">Your support case, [[caseID]], has been closed and archived.</p>

						<p>If this was a mistake, or you have any further questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36324



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Priority Changed</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Priority Changed</span></h3>

						<p>We''ve changed the priority of your support case, [[CaseID]], with the subject of: [[showProfileValue(ProfileID=''Case_Subject'')]].</p>

						<p style="line-height: 20px">If you have any questions regarding this email, or if you need further assistance with your support case, feel free to contact us quoting your case ID, [[CaseID]].</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36328



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ width: 600px;  }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<p>You have been given shared rights on the following action by [[owner]]:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable">
							<tbody>
								<tr>
									<td class="lColumn"><b>Action With:</b>&nbsp;</td>
									<td class="rColumn">[[actionWith]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Action ID:</b> &nbsp;</td>
									<td class="rColumn">[[actionID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Action Description:</b> &nbsp;</td>
									<td class="rColumn">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vulputate vulputate lectus ac aliquam. Etiam laoreet placerat tincidunt. Vivamus urna justo, molestie sed suscipit sed, auctor at diam.</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Due Date:</b> &nbsp;</td>
									<td class="rColumn">[[dueDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions, feel free to <a href="mailto:relayhelpcc@relayware.com">contact us</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36332



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Approved</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Approved</span></h3>

						<p style="line-height: 20px">Your lead has been reviewed and approved. The details of this lead are as follows:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[lead.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[lead.Company]]</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Lead Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.LeadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Status:</b></td>
									<td class="rColumn">[[lead.ApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Detail:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[lead.Progress]]</td>
								</tr>
							</tbody>
						</table>

						<p>The partner representative for this lead is: [[lead.partner.fullname]]</p>

						<p>If you have any questions regarding this information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36598



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Declined</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Declined</span></h3>

						<p style="line-height: 20px">Unfortunately the following lead has been declined and no further action will be taken.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[lead.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[lead.Company]]</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Lead Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.LeadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Status:</b></td>
									<td class="rColumn">[[lead.ApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Detail:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[lead.Progress]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36602



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Approval</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">New Lead Approval</span></h3>

						<p style="line-height: 20px">A new lead has been submitted by [[lead.partner.OrganisationName]]. Please review the following information and set its approval status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[lead.partner.FirstName]] [[lead.partner.Lastname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[lead.partner.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[Detail]]</td>
								</tr>
							</tbody>
						</table>

						<p>In order to set this lead''s approval status, please <a href="[[func.getInternalSiteURL(1)]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">click here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36606



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Leads Assigned</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Leads Reassigned</span></h3>

						<p style="line-height: 20px">The following Leads have been reassigned to you.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center"><b>List of Opportunities:</b></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center">[[listOfLeads]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36610



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Leads Reassigned</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Leads Reassigned</span></h3>

						<p style="line-height: 20px">The following leads have been reassigned to different account managers:</p>

						<p>[[listOfLeads]]</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36614



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Sales Lead Assigned</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Reassigned</span></h3>

						<p style="line-height: 20px">The following Lead has been reassigned to you.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[lead.partner.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[lead.partner.OrganisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead:</b></td>
									<td class="rColumn">[lLead.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>OpportunityID:</b></td>
									<td class="rColumn">[[opportunity.opportunityID]]</td>
								</tr>
							</tbody>
						</table>

						<p>In order to access this lead, please <a href="[[getInternalSiteURL()]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;opportunityID=[[opportunity.opportunityID]]">Click Here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36618



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center; border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */ background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */ background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */ background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */ width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Registered</span></h3>

						<p style="line-height: 20px">You have successfully registered a new lead with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.leadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[lead.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager:</b></td>
									<td class="rColumn">[[lead.AccountManager.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Phone:</b></td>
									<td class="rColumn">[[lead.AccountManager.directLine]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Email:</b></td>
									<td class="rColumn">[[lead.AccountManager.email]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36622



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Created</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Created</span></h3>

						<p style="line-height: 20px">A new Lead has been created with the following details:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Lead Description:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.leadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[lead.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Contact:</b></td>
									<td class="rColumn">[[lead.partner.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Partner Organization:</b></td>
									<td class="rColumn">[[lead.partner.OrganisationName]]</td>
								</tr>
							</tbody>
						</table>

						<p>To access this lead, please <a href="[[getInternalSiteURL()]]/remote.cfm?p=[[magicnumber]]&amp;a=i&amp;st=010&amp;leadID=[[lead.leadID]]">Click Here</a>.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36626



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Updated</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Updated</span></h3>

						<p style="line-height: 20px">A lead under your supervision has been updated. As the account manager, you may wish to view the progress of this lead below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Lead Description:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Organization:</b></td>
									<td class="rColumn">[[lead.partner.Organisationname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[lead.stage]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this email, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36630



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Converted</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Converted</span></h3>

						<p style="line-height: 20px">The following lead has been converted into an opportunity.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Converted From (Lead)</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Description:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.leadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[lead.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager:</b></td>
									<td class="rColumn">[[lead.AccountManager.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Phone:</b></td>
									<td class="rColumn">[[lead.AccountManager.directLine]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Email:</b></td>
									<td class="rColumn">[[lead.AccountManager.email]]</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Converted To (Opportunity)</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity:</b></td>
									<td class="rColumn">[[opportunity.detail]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Opportunity ID:</b></td>
									<td class="rColumn">[[opportunity.OpportunityID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Name:</b></td>
									<td class="rColumn">[[lead.partner.organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.fullname]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Phone:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.directLine]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Account Manager Email:</b></td>
									<td class="rColumn">[[opportunity.AccountManager.email]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding this information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>

</body>
</html>'
 where ident = 36634



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Accepted</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Accepted</span></h3>

						<p style="line-height: 20px">Congratulations, your lead has been accepted and the details can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[lead.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[lead.Company]]</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Lead Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.LeadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Status:</b></td>
									<td class="rColumn">[[lead.ApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Detail:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[lead.Progress]]</td>
								</tr>
							</tbody>
						</table>

						<p>The partner representative for this lead is: [[lead.partner.fullname]]</p>

						<p>If you have any questions regarding this information, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36638



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Lead Rejected</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Lead Rejected</span></h3>

						<p style="line-height: 20px">Unfortunately you were unsuccessful in your recent lead application. The details of this lead can be reviewed below.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Contact Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Name:</b></td>
									<td class="rColumn">[[lead.FullName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Contact Organization:</b></td>
									<td class="rColumn">[[lead.Company]]</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align: center;"><b>Lead Details</b></td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead ID:</b></td>
									<td class="rColumn">[[lead.LeadID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Status:</b></td>
									<td class="rColumn">[[lead.ApprovalStatus]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Lead Detail:</b></td>
									<td class="rColumn">[[lead.Description]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Stage:</b></td>
									<td class="rColumn">[[lead.Progress]]</td>
								</tr>
							</tbody>
						</table>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36642



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Support Case Review</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Support Case Updated</span></h3>

						<p style="line-height: 20px">The following support case has been updated by [[organisationname]] for review. Please process this case and set its status accordingly.</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Organization Name:</b></td>
									<td class="rColumn">[[organisationName]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Support Case ID:</b></td>
									<td class="rColumn">[[case.ID]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Submit Date:</b></td>
									<td class="rColumn">[[case.UpdateDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>The partner has been notified that their support case is being processed.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36677



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Thank You for Registering</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0; }
table#colTable td.odd{background:#549bd5;}
table#colTable td.even{background:#90b807;}
table#colTable td{border:solid 2px #fff; text-align:center;color:#fff;}


table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px; }

.list_check {
margin: 0 0 0 10px;
}
.list_check li {background: url(http://aspire.staging.portal.relayware.com/Code/Borders/relayware2010/css/images/tick.png) no-repeat 0 6px;padding: 10px 0 10px 30px;list-style: none;overflow: hidden;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Don''t Forget</span></h3>

						<p style="line-height: 20px">Relayware''s Partner Portal gives you the power for:</p>

						<ul class="list_check">
							<li>Registration &amp; Approvals</li>
							<li>Recruitment &amp; On-boarding</li>
							<li>Dynamic Content Library</li>
							<li>Mobile Optimization</li>
							<li>Self-Administration</li>
							<li>Up to 500 Partner Logins</li>
						</ul>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="50%">
							<tbody>
								<tr><!--td class="odd">
										Registration & Approvals
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td class="even">
										Recruitment & On-boarding
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td class="odd">
										Dynamic Content Library
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td class="even">
										Mobile Optimization
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td class="odd">
										Self-Administration
									</td>
									<td>
									</td>
								</tr>	
								<tr>
									<td class="even">
										Up to 500 Partner Logins
									</td>
									<td>
									</td-->
								</tr>
							</tbody>
						</table>

						<p>If you have any questions, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36760



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Submitted</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;  border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */  background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */  width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">A business plan has been submitted for approval</span></h3>

						<p style="line-height: 20px">Please <a href="[[encryptedApprovalLink]]&amp;p=[[magicnumber]]">click here</a> to review it.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 36884



update phrases set phraseText = 
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Business Plan Submitted</title>
</head>
<body style="background: rgb(247, 243, 243);">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;  border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */  background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */  width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-top: 10px; padding-right: 20px; padding-left: 20px;">
						<h2>Dear [[firstname]],</h2>

						<p style="line-height: 20px;">Welcome to the incentive program.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>'
 where ident = 37429

update phrases set phraseText =
N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Registration Successful</title>
</head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}z
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;	border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th {	background: #0177b7; /* Old browsers */	background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */	background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */	background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */	background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */	background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */	width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>
<table align="center" class="main">
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:100%;">
				<tbody>
					<tr>
						<td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
						<h2>Dear [[firstname]] [[lastname]],</h2>

						<h3><span style="color:#1189ca;">Registration Successful</span></h3>

						<p style="line-height: 20px">You are now registered for the following certification:</p>

						<table border="0" cellpadding="6" cellspacing="0" class="content" id="colTable" width="100%">
							<tbody>
								<tr>
									<td class="lColumn"><b>Name:</b></td>
									<td class="rColumn">[[Certification]]</td>
								</tr>
								<tr>
									<td class="lColumn"><b>Registration Date:</b></td>
									<td class="rColumn">[[RegistrationDate]]</td>
								</tr>
							</tbody>
						</table>

						<p>If you have any questions regarding your new registration, feel free to contact us.</p>

						<p>Kind regards,<br />
						<span class="sig">[[getSiteName()]]</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>
'
where ident = 33519 


update phrases set phraseText =
'
<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>A licence renewal opportunity is will soon be due.</title>
	<style type="text/css">.content {font-family:Verdana; font-size: 14px; padding-left: 15px;}

.content h2{
font-size: 16px;
}

.loginDetails{
font-family:Verdana; font-size: 14px;
}

.header{
font-family:Verdana;
padding-bottom: 10px;
width: 632px;
}

.header h1{
font-family:Verdana;
font-weight: normal;
text-align: right;
font-size: 22px;
}

.footer{
color: #000000;
font-family:Verdana;
font-size: 10px;
text-align: center;
}
	</style>
</head>
<body style="text-align: center; text-align: centre">
<table>
	<tbody>
		<tr>
			<td>phr_email_header
			<table style="width:632;">
				<tbody>
					<tr class="content">
						<td colspan="2" style="padding-left:15px">
						<p>Dear [[salutation]] [[lastname]],</p>

						<p>A license renewal opportunity is will soon be due.</p>

						<p>Here are the details of this opportunity:<br />
						<br />
						<b>Detail:</b> [[opportunity.Detail]]<br />
						<b>Customer Contact:</b> [[opportunity.EndCustomer.fullname]]<br />
						<b>Renewal Date:</b> [[assetEndDate]]</p>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr class="footer">
						<td></td>
					</tr>
				</tbody>
			</table>
			phr_email_footer</td>
		</tr>
	</tbody>
</table>
</body>
</html>
'
where ident = 33551 

update phrases set phraseText =
N'<div class="row">
   <!-- Col 1 -->  
   <div class="col-xs-12 col-sm-12 col-md-8">
      <!-- Banner Ad -->  
      <div id="bannerRotator">
         <ul>
            <li>
               <div class="col-xs-12 banner" id="banner1">
                  <h1>The #1 Partnering Automation Platform</h1>
                  <h3>The future of partner management has arrived.</h3>
               </div>
            </li>
            <li>
               <div class="col-xs-12 banner" id="banner2">
                  <h1>It’s Time to Partner Up.</h1>
                  <h3>Improve your partnerability with our vast library of resources.</h3>
               </div>
            </li>
         </ul>
      </div>
      <!-- End Banner Ad -->    
      <h1>Welcome to Relayware''s Partner Cloud Configuration Portal</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
      <p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
   </div>
   <!-- Col 2 -->    
   <div class="col-xs-12 col-sm-12 col-md-4">
      <RELAY_ACTIVITY_STREAM filterTypes=All,Following,Flagged>
   </div>
</div>
<div class="row">
   <div class="col-xs-12 col-sm-6 col-md-3 cta"><a class="cta-link" href="/"><span class="title">Lorum Ipsum</span><span class="image"><img class="cta-image" src="/code/borders/images/feature-video.png" /></span><span class="text">Aliquam sit amet libero ultrices, convallis lectus in, dapibus eros.</span> </a></div>
   <div class="col-xs-12 col-sm-6 col-md-3 cta"><a class="cta-link" href="/"><span class="title">Lorum Ipsum</span><span class="image"><img class="cta-image" src="/code/borders/images/feature-video.png" /></span><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </span> </a></div>
   <div class="col-xs-12 col-sm-6 col-md-3 cta"><a class="cta-link" href="/"><span class="title">Lorum Ipsum</span><span class="image"><img class="cta-image" src="/code/borders/images/feature-video.png" /></span><span class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span> </a></div>
   <div class="col-xs-12 col-sm-6 col-md-3 cta"><a class="cta-link" href="/"><span class="title">Lorum Ipsum</span><span class="image"><img class="cta-image" src="/code/borders/images/feature-video.png" /></span><span class="text">Aliquam sit amet libero ultrices, convallis lectus in, dapibus eros.</span> </a></div>
</div>
'
where ident = 29254


/* PROD2016-797 WAB/SB Remove formatting from captcha text */
update phrases set phraseText = N'For added security, please enter the verification code hidden in the image'
where phraseID = (select phraseid from phraselist where phraseTextID = 'Ext_Captcha_instructions')
and phraseText = N'<div class="requestLeftAlign">For added security, please enter the verification code hidden in the image.</div>'

