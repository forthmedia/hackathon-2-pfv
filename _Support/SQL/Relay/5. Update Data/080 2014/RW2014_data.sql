
/* populate OOTB leadSource table */
if not exists (select 1 from lookupList where fieldname='leadSourceID')
begin
	insert into lookupList (fieldname,itemText,lookupTextID,extraInfo,isParent,isDefault,isLive,locked)
	select 'leadSourceID',b.source, REPLACE(replace(b.source,' ',''),'-','') as lookupTextID,0,0,0,1,0 from (
		select 'Campaign' as source
		union
		select 'Cold Call' as source
		union
		select 'Conference' as source
		union
		select 'Direct Mail' as source
		union
		select 'Email' as source
		union
		select 'Employee' as source
		union
		select 'Existing Customer' as source
		union
		select 'LinkedIn' as source
		union
		select 'Locator' as source
		union
		select 'Other' as source
		union
		select 'Partner' as source
		union
		select 'Unknown' as source
		union
		select 'Public Relations' as source
		union
		select 'Self-generated' as source
		union
		select 'Trade Show' as source
		union
		select 'Web' as source
		union
		select 'Word of mouth' as source
		union
		select 'Marketing Response' as source
		union
		select 'Advert' as source	
	)
	b
	left join lookupList l on replace(b.source,' ','') = l.lookupTextID and l.fieldname='leadSourceID'
end


/* populate OOTB leadType values */
if not exists (select 1 from lookupList where fieldname='leadTypeID')
begin
	insert into lookupList (fieldname,itemText,lookupTextID,extraInfo,isParent,isDefault,isLive,locked,sortOrder)
	select 'leadTypeID', b.leadType, replace(b.leadType,' ','') as leadTypeTextID,0,0,0,1,0,b.sortOrder from (
		select 'Partner' as leadType,3 as sortOrder
		union
		select 'Lead' as leadType, 1 as sortOrder
		union
		select 'Distributed Lead' as leadType, 2 as sortOrder
	)
	b
	left join lookupList l on replace(b.leadType,' ','') = l.lookupTextID and l.fieldname='leadTypeID'
	
	update lookUpList set isDefault=1 where lookupTextID = 'Lead' and fieldname='leadTypeID'
end

/* populate OOTB lead approval values */
if not exists (select 1 from lookupList where fieldname='leadApprovalStatusID')
begin
	insert into lookupList (fieldname,itemText,lookupTextID,extraInfo,isParent,isDefault,isLive,locked,sortOrder)
	select 'leadApprovalStatusID', b.leadApproval, replace(b.leadApproval,' ','') as leadApprovalTextID,0,0,0,1,0,b.sortOrder from (
		select 'Pending' as leadApproval, 1 as sortOrder
		union
		select 'Rejected' as leadApproval, 2 as sortOrder
		union
		select 'Approved' as leadApproval, 3 as sortOrder
		union
		select 'Converted' as leadApproval, 4 as sortOrder
	)
	b
	left join lookupList l on replace(b.leadApproval,' ','') = l.lookupTextID and l.fieldname='leadApprovalStatusID'
end


/* populate OOTB lead progress values */
--if not exists (select 1 from lookupList where fieldname='leadProgressID')
--begin
	insert into lookupList (fieldname,itemText,lookupTextID,extraInfo,isParent,isDefault,isLive,locked,sortOrder)
	select 'leadProgressID', b.leadProgress, replace(replace(b.leadProgress,' ',''),'-','') as leadProgressTextID,0,0,0,1,0, b.sortOrder from (
		select 'Open - Not Contacted' as leadProgress, 1 as sortOrder
		union
		select 'Working - Contacted' as leadProgress, 2 as sortOrder
		union
		select 'Closed - Converted' as leadProgress, 3 as sortOrder
		union
		select 'Closed - Not Converted' as leadProgress, 4 as sortOrder
	)
	b
	left join lookupList l on replace(replace(b.leadProgress,' ',''),'-','') = l.lookupTextID and l.fieldname='leadProgressID'
	where l.lookUpId is null
--end

/* populate lead enquiry type values */
/* 2014-09-16	RPW	Add Lead Screen on Portal - Change Enquiry Type to use lookUpList */

IF NOT EXISTS (SELECT * FROM LookupList WHERE FieldName = 'leadEnquiryTypeID')
	BEGIN
INSERT INTO lookupList
(fieldname,itemText,lookupTextID,extraInfo,isParent,isDefault,isLive,locked,sortOrder)
VALUES
('leadEnquiryTypeID','Sales Request','SalesRequest',0,0,0,1,0,1),
('leadEnquiryTypeID','Services Request','ServicesRequest',0,0,0,1,0,2),
('leadEnquiryTypeID','Partnering Enquiry','PartneringEnquiry',0,0,0,1,0,3)
	END

/* update screen name */
update screens set screenName = 'Opportunities' where screenName='Leads' and screenTextID='LeadAndOppList'

/*** OOTB Currency Values ***/

IF NOT EXISTS (SELECT * FROM FlagType WHERE Name = 'Financial')
	BEGIN
		INSERT INTO FlagType
		(Name,DataTable,DataTableFullName)
		VALUES
		('Financial','financial','financialFlagData')
	END

IF NOT EXISTS (SELECT * FROM Currency WHERE currencyISOCode = 'MXN')
	BEGIN
		INSERT INTO Currency (currencyName,currencyISOCode,currencySign) VALUES ('Mexican Peso','MXN',N'$')
	END

IF NOT EXISTS (SELECT * FROM Currency WHERE currencyISOCode = 'JPY')
	BEGIN
		INSERT INTO Currency (currencyName,currencyISOCode,currencySign) VALUES ('Japanese Yen','JPY',N'¥')
	END
	
/* add salutation Lead valid values */
IF NOT EXISTS (SELECT * FROM [ValidFieldValues] WHERE [FieldName] = 'lead.salutation') 
INSERT INTO [dbo].[ValidFieldValues]
           ([FieldName]
           ,[CountryID]
           ,[ValidValue]
           ,[DefaultFieldCase]
           ,[DefaultFieldValue]
           ,[sortorder]
           ,[datavalue]
           ,[LanguageID]
           ,[InAnalysis]
           ,[score])
     VALUES
          ('lead.salutation'
           ,0
           ,'*lookup'
           ,'T'
           ,0
           ,1
           ,'SELECT  validValue as dataValue, validValue as displayValue, sortOrder from validfieldvalues where fieldname = ''person.salutation'' order by sortOrder'
           ,NULL
           ,0
           ,null)
GO

/* OOTB Valid Values for Lead Enquiry Type */
IF NOT EXISTS (SELECT * FROM [ValidFieldValues] WHERE [FieldName] = 'lead.enquiryType') 
BEGIN
	INSERT INTO [dbo].[ValidFieldValues] ([FieldName], [CountryID], [ValidValue], [DefaultFieldCase], [DefaultFieldValue], [sortorder], [datavalue], [LanguageID], [InAnalysis], [score]) VALUES ('lead.enquiryType', 0, N'Sales Request', 'T', 0, 1, N'', 1, 0, NULL)
	INSERT INTO [dbo].[ValidFieldValues] ([FieldName], [CountryID], [ValidValue], [DefaultFieldCase], [DefaultFieldValue], [sortorder], [datavalue], [LanguageID], [InAnalysis], [score]) VALUES ('lead.enquiryType', 0, N'Services Request', 'T', 0, 2, N'', 1, 0, NULL)
	INSERT INTO [dbo].[ValidFieldValues] ([FieldName], [CountryID], [ValidValue], [DefaultFieldCase], [DefaultFieldValue], [sortorder], [datavalue], [LanguageID], [InAnalysis], [score]) VALUES ('lead.enquiryType', 0, N'Partnering Enquiry', 'T', 0, 3, N'', 1, 0, NULL)
END 

/* remove the CJM module */
delete from relayModule where moduleTextID='CJM'

/* add accounType to selections */
if not exists (select 1 from selectionField where selectionField='frmAccountTypeID')
insert into selectionField (selectionField,criteriaMask) values ('frmAccountTypeID','IN (FieldData)')



-- 2014/09/10	YMA	Add out of office delegation to approval engine
declare @flagGroupID int
declare @entitytypeid int
declare @entityid int

if not exists(select 1 from flaggroup where flaggrouptextID in ('outOfOffice','fg_outOfOffice'))
begin
--insert flaggroup
	insert into flaggroup (ParentFlagGroupID,FlagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,Description,NamePhraseTextID,DescriptionPhraseTextID,Notes,OrderingIndex,Expiry,Scope,Viewing,Edit,Search,Download,ViewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,PublicAccess,PartnerAccess,CreatedBy,Created,LastUpdatedBy,LastUpdated,UseInSearchScreen,FlagRegionEntity,EntityMemberID,EntityMemberLive,formattingParameters,createdByPerson,lastUpdatedByPerson)
	select 0,2,0,
		1,'fg_outOfOffice','Out of office delegation','','flagGroup_outOfOffice',null,'',1,
		'2100-01-01 00:00:00.000',0,1,1,0,0,0,0,0,0,0,0,100,getdate(),100,getdate(),0,null,null,0,null,0,0
--insert flags
	select @flagGroupID = flaggroupID from flaggroup where FlagGroupTextID = 'fg_outOfOffice'
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Out of office','',1,1,100,getdate(),100,getdate(),'outOfOffice','flag_outOfOffice',null,0,0,null,0,null,null,0,null,0,null,null,null,null,null,null,0,0,0,0
	
--insert phrases
	select @flagGroupID = 0
	select @entitytypeid = 0
	select @entityid = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flagGroup_outOfOffice', @entitytypeid = @entitytypeid, @entityid=@flagGroupID, @phraseText= 'Out of office delegation', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_outOfOffice', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Out of office', @updateMode = 0
	
--insert screenitem
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,breakln,tdclass,specialformatting,lastupdatedby,lastupdated)
	values ((select screenID from screens where screentextID = 'PerProfileSummary'),2,'flagGroup','fg_outOfOffice','','edit',1,1,'screentd','noflaggroupname=1',100,getdate())
	
end
if not exists(select 1 from flaggroup where flaggrouptextID in ('outOfOfficeDelegateTo','fg_outOfOfficeDelegateTo'))
begin
--insert flaggroup
	insert into flaggroup (ParentFlagGroupID,FlagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,Description,NamePhraseTextID,DescriptionPhraseTextID,Notes,OrderingIndex,Expiry,Scope,Viewing,Edit,Search,Download,ViewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,PublicAccess,PartnerAccess,CreatedBy,Created,LastUpdatedBy,LastUpdated,UseInSearchScreen,FlagRegionEntity,EntityMemberID,EntityMemberLive,formattingParameters,createdByPerson,lastUpdatedByPerson)
	select 0,6,0,
		1,'fg_outOfOfficeDelegateTo','Out of office delegation','','flagGroup_outOfOfficeDelegateTo',null,'',1,
		'2100-01-01 00:00:00.000',0,1,1,0,0,0,0,0,0,0,0,100,getdate(),100,getdate(),0,null,null,0,null,0,0
--insert flags
	select @flagGroupID = flaggroupID from flaggroup where FlagGroupTextID = 'fg_outOfOfficeDelegateTo'
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Delegate to','',1,1,100,getdate(),100,getdate(),'outOfOfficeDelegateTo','flag_outOfOfficeDelegateTo',null,1,1,null,0,null,null,0,null,0,null,null,null,null,0,null,0,0,0,0
	
--insert phrases
	select @flagGroupID = 0
	select @entitytypeid = 0
	select @entityid = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flagGroup_outOfOfficeDelegateTo', @entitytypeid = @entitytypeid, @entityid=@flagGroupID, @phraseText= 'Out of office delegation', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_outOfOfficeDelegateTo', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Delegate to', @updateMode = 0
	
--insert validfieldvalues
	insert into validfieldvalues (FieldName,CountryID,ValidValue,DefaultFieldCase,DefaultFieldValue,sortorder,datavalue,LanguageID,InAnalysis,score)
	select 'flag.outOfOfficeDelegateTo',0,'*lookup','T',0,1,'SELECT  personid as dataValue, firstName + '' '' + lastname as displayValue  from person where OrganisationID in (select OrganisationID from person where PersonID = #entityID#)',1,0,''
--insert screenitem
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,tdclass,specialformatting,lastupdatedby,lastupdated)
	values ((select screenID from screens where screentextID = 'PerProfileSummary'),3,'flagGroup','fg_outOfOfficeDelegateTo','','edit',1,'screentd','noflaggroupname=1',100,getdate())
	
end
GO



/* 
WAB 2015-10-07 Add if statement and dynamic SQL to protect against non existence of relayserver or non existence of particular table
*/

IF Exists (select 1 from sys.databases where name = 'relayserver')
BEGIN	

	declare @tableExists bit, 
			@checkForTableSQL nvarchar(max) = 'select @tableExists = count(1) from relayserver.information_schema.tables where table_name = ''processJumpAction'' '

	exec sp_executeSQL @checkForTableSQL,N'@tableExists bit OUTPUT', @tableExists = @tableExists OUTPUT
		
	IF @tableExists <> 0
	BEGIN
       	declare @fullSQL nvarchar(max) = '
					/* update processJumpAction table */
					insert into processJumpAction (jumpAction,jumpActionDesc)
					select r.jumpAction,r.jumpActionDesc
					from relayServer.dbo.processJumpAction r
						left join processJumpAction pja on pja.jumpAction = r.jumpAction
					where pja.jumpAction is null
						and r.jumpAction !=''predefinedSql''
					
					
					/* update processJumpAction table */
					insert into processJumpExpression (jumpExpression,jumpExpressionDesc)
					select r.jumpExpression,r.jumpExpressionDesc
					from relayServer.dbo.processJumpExpression r
						left join processJumpExpression pje on pje.jumpExpression = r.jumpExpression
					where pje.jumpExpression is null
    		'

		exec (@fullSQL)
	
	END


	set	@checkForTableSQL = 'select @tableExists = count(1) from relayserver.information_schema.tables where table_name = ''dataloadCol'' '

	exec sp_executeSQL @checkForTableSQL,N'@tableExists bit OUTPUT', @tableExists = @tableExists OUTPUT
		
	IF @tableExists <> 0
	BEGIN
       	set @fullSQL  = '
					/* update dataloadCol table */
					insert into dataloadCol (dataloadColumnName,alternativeColumnName,loadThisColumn,controlColumn,relayColumn,columnDataType,loadTypeGroup)
					select r.dataloadColumnName,r.alternativeColumnName,r.loadThisColumn,r.controlColumn,r.relayColumn,r.columnDataType,r.loadTypeGroup
					from relayServer.dbo.dataloadCol r
						left join dataloadCol dc on dc.dataloadColumnName = r.dataloadColumnName
					where dc.dataloadColumnName is null
					
					/* update dataLoadFunctions table */
					insert into dataLoadFunctions (functionName,functionTextID,URL,description,dataLoadType,Created,functionResultText,sortOrder,functionType,openInTab)
					select r.functionName,r.functionTextID,r.URL,r.description,r.dataLoadType,r.Created,r.functionResultText,r.sortOrder,r.functionType,r.openInTab
					from relayServer.dbo.dataLoadFunctions r
						left join dataLoadFunctions df on df.functionTextID = r.functionTextID
					where df.functionTextID is null
					
					/* update dataLoadType table */
					insert into dataLoadType (dataloadTypeTextID,Name,loadTypeGroup)
					select r.dataloadTypeTextID,r.Name,r.loadTypeGroup
					from relayServer.dbo.dataLoadType r
						left join dataLoadType dt on dt.dataloadTypeTextID = r.dataloadTypeTextID
					where dt.dataloadTypeTextID is null
    		'

		exec (@fullSQL)

	END
        	
END              




/* Remove entries from modregister from the activityType refreshing */
update modEntityDef set fieldMonitor=0 where fieldname='lastRefreshed' and tablename='activityType' and fieldMonitor = 1

delete from modRegister where action='ATM'
and modentityID = (select modEntityID from modEntityDef where fieldname='lastRefreshed' and tablename='activityType')


/* re-instate the isSalesManager flag as that seems to have disappeared */
if not exists (select 1 from flag where flagTextID='isSalesManager')
begin
	insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
	select distinct flaggroupID,'Sales Manager',1,1,'isSalesManager','flag_isSalesManager',404,getdate(),404,getdate() from vFlagDef where flagGroupTextID='AccountResources'

	exec addNewPhraseAndTranslation @phraseTextId='flag_isSalesManager',@phraseText='Sales Manager'
end


-- populate the leads table from the opportunity table - may need to add more columns.. These are just the minimum.
insert into lead
	(salutation,firstname,lastname,email,officePhone,mobilePhone,
		company,address1,address2,address4,address5,postalCode,countryID,website,
		description,distiLocationID,partnerLocationID,partnerSalesPersonID,currency,vendorAccountManagerPersonID,campaignId,
		convertedPersonId,convertedLocationID,
		progressID,leadTypeID,
		lastUpdatedBy,lastUpdated,lastUpdatedByPerson,created,createdBy,active
	)
select
	p.salutation,p.firstname,p.lastname,p.email,p.officePhone,p.mobilePhone,
	l.sitename,l.address1,l.address2,l.address4,l.address5,l.postalCode,l.countryID,l.specificUrl,
	opp.currentSituation,opp.distiLocationID,opp.partnerLocationID,opp.partnerSalesPersonID,opp.currency,opp.vendorAccountManagerPersonID,opp.campaignID,
	p.personID,l.locationID,
	(select lookUpId from lookupList where fieldname='leadprogressID' and lookupTextID='OpenNotContacted'),
	(select lookUpId from lookupList where fieldname='leadTypeId' and lookupTextID='Lead'),
	opp.lastUpdatedBy,opp.lastUpdated,opp.lastUpdatedByPerson,opp.created,opp.createdBy,1
from
	opportunity opp
		inner join person p on p.personID = opp.contactPersonID
		inner join location l on p.locationID = l.locationId
		inner join oppType on oppType.oppTypeID = opp.oppTypeID
		left join lead on lead.created = opp.created and lead.createdBy = opp.createdBy -- a tenuous link - don't insert duplicate leads - may need to add to this join
	where oppType.oppTypeTextID = 'Leads'
		and expectedCloseDate > getDate()
		and lead.leadID is null
		
/* 2014-10-28	YMA	CORE-906 ensure screens have a viewer set */
update Screens
set viewer = 'ShowScreen'
where viewer is null or viewer = ''


/* NJH 2014/12/10 set the siteStylesheet */
update siteDef set stylesheet='/styles/global.css,/javascript/bootstrap/css/bootstrap.min.css,/javascript/fancybox/jquery.fancybox.css,/code/styles/brand.css,/code/styles/mobile.css,/code/styles/tablet.css,/code/styles/desktop-medium.css,/code/styles/desktop-large.css'
where isInternal=0
and isNull(stylesheet,'') not like '%,%'