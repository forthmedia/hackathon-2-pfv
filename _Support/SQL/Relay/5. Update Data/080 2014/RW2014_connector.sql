exec generate_mraudit @tablename='textMultipleFlagData',@mnemonic='TMF'
exec generate_mraudit @tablename='decimalFlagData',@mnemonic='DF'

/* add a connector module */
if not exists (select 1 from relayModule where moduleID = 59)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (59,'Connector','Connector',4,4,0)
GO

if not exists(select 1 from SecurityType where shortname='connectorTask')
insert into SecurityType (ShortName,Description,CountrySpecific,relayWareModule) values ('connectorTask', 'Connector Module', 0, NULL)

/* give connectorTask to RW Admin usergroup */
insert into rights (userGroupID,securityTypeID,permission,createdBy,lastUpdatedBy)
select distinct u.userGroupID,s.securityTypeID,7,4,4
from userGroup u
	left join securityType s on 1=1
	left join rights r on r.userGroupID = u.userGroupId and s.securityTypeID = r.securityTypeID
where 
	u.name = 'RW Admin'
	and s.shortName = 'connectorTask' 
	and r.userGroupID is null


/* populate the new matchAddress field on the location table*/
update location set matchAddress=matchname where matchAddress is null and matchname is not null

/* the functionality has now been moved into a trigger  - probably should get rid of this sp*/
update fieldTrigger set active=0 where triggername='sp_fieldTrigger_oppAcMgrModified' and active=1
update fieldTrigger set active=0 where triggername='sp_fieldTrigger_oppSalesMgrModified' and active=1
update fieldTrigger set active=0 where triggername='sp_fieldTrigger_oppPartnerModified' and active=1

/* clean up some old opportunity data - set values to null where they were 0 */
update opportunity set vendorAccountManagerPersonID=null where vendorAccountManagerPersonID = 0

/* create new connector synching status flags */
declare @entityName varchar(50),@entityTypeID int

declare entity_cursor cursor for
    select distinct s.entityName,s.entityTypeID
		from schemaTable s
	where s.entityName in (select object from connectorObject where relayware=1 and object !='organisation')
	
open entity_cursor
fetch next from entity_cursor into @entityName,@entityTypeID

while @@FETCH_STATUS = 0
begin
	declare @flagGroupTextID varchar(50) = @entityName+'ConnectorSynchStatus'
	declare @flagGroupPhraseTextID varchar(50) = 'flagGroup_'+@entityName+'ConnectorSynch'
	declare @flagTextID varchar(50)
	declare @flagPhraseTextID varchar(50)
	
	if not exists(select 1 from flaggroup where flagGroupTextID=@flagGroupTextID)
	begin
		insert into flaggroup (flagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,NamePhraseTextID,OrderingIndex,viewing,edit,search,download,viewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,CreatedBy,Created,LastUpdatedBy,LastUpdated,scope,entitymemberID,parentFlagGroupID,createdByPerson,lastUpdatedByPerson) values (3,@entityTypeID,1,@flagGroupTextID,'Connector Synch Status',@flagGroupPhraseTextID,147,1,0,0,0,0,0,0,0,404,getdate(),404,getdate(),0,0,0,0,0)
		exec addNewPhraseAndTranslation @phraseTextId=@flagGroupPhraseTextID,@phraseText='Connector Synch Status'
	end

	set @flagTextID = @entityName+'Exported'
	set @flagPhraseTextID = 'flag_'+@entityName+'Exported'
	if not exists(select 1 from flag where flagTextID=@flagTextID)
	begin
		insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Exported',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
		exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Exported'
	end

	set @flagTextID = @entityName+'Imported'
	set @flagPhraseTextID = 'flag_'+@entityName+'Imported'
	if not exists(select 1 from flag where flagTextID=@flagTextID)
	begin
		insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Imported',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
		exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Imported'
	end
	
	set @flagTextID = @entityName+'Synching'
	set @flagPhraseTextID = 'flag_'+@entityName+'Synching'
	if not exists(select 1 from flag where flagTextID=@flagTextID)
	begin
		insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Synching',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
		exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Synching'
	end
	
	set @flagTextID = @entityName+'Suspended'
	set @flagPhraseTextID = 'flag_'+@entityName+'Suspended'
	if not exists(select 1 from flag where flagTextID=@flagTextID)
	begin
		insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Suspended',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
		exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Suspended'
	end
	
	declare @viewName varchar(100)
	set @viewname='v'+@entityName
	exec createFlagView @entityName=@entityName, @viewname=@viewname
	
	fetch next from entity_cursor into @entityName,@entityTypeID
end

close entity_cursor;
deallocate entity_cursor;