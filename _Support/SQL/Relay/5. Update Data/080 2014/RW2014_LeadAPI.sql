--Set these fields in the Leads table as being accessable from the API 
UPDATE ModEntityDef
SET ApiActive=1
WHERE TableName='Lead'
AND
(
FieldName='PostalCode' OR
FieldName='CountryID' OR
FieldName='Email' OR
FieldName='created' OR
FieldName='createdby' OR
FieldName='lastUpdatedBy' OR
FieldName='lastUpdatedByPerson' OR
FieldName='lastUpdated' OR
FieldName='crmLeadID' OR
FieldName='convertedLocationID' OR
FieldName='convertedPersonID' OR
FieldName='convertedOpportunityID' OR
FieldName='description' OR
FieldName='officePhone' OR
FieldName='mobilePhone' OR
FieldName='website' OR
FieldName='partnerLocationID' OR
FieldName='officePhone' OR
FieldName='partnerLocationID' OR
FieldName='partnerSalesPersonID' OR
FieldName='partnerSalesManagerPersonID' OR
FieldName='distiLocationID' OR
FieldName='vendorAccountManagerPersonID' OR
FieldName='approvalStatusID' OR
FieldName='acceptedByPartner' OR
FieldName='acceptedByPartnerDate' OR
FieldName='viewedByPartnerDate' OR
FieldName='currency' OR
FieldName='rejectionReason' OR
FieldName='progressID' OR
FieldName='sourceID' OR
FieldName='campaignID' OR
FieldName='vendorSalesManagerPersonID' OR
FieldName='sponsorPersonID' OR
FieldName='annualRevenue' OR
FieldName='sponsorFirstname' OR
FieldName='LeadID' OR
FieldName='leadTypeID' OR
FieldName='leadTypeStatusID' OR
FieldName='salutation' OR
FieldName='LastName' OR
FieldName='Company' OR
FieldName='Address1' OR
FieldName='Address2' OR
FieldName='Address3' OR
FieldName='Address4' OR
FieldName='Address5' OR
FieldName='active' OR
FieldName='distiContactPersonID' OR
FieldName='sponsorLastname' OR
FieldName='enquiryType');

--Give the fields which don't already have API Friendly name's  the same name as the Field. This is neccessary due to a bug where fields without apiNames can't be used in filters
UPDATE ModEntityDef
SET APIName=FieldName
WHERE TableName='Lead'
AND APIName IS NULL
AND
(
FieldName='PostalCode' OR
FieldName='CountryID' OR
FieldName='Email' OR
FieldName='created' OR
FieldName='createdby' OR
FieldName='lastUpdatedBy' OR
FieldName='lastUpdatedByPerson' OR
FieldName='lastUpdated' OR
FieldName='crmLeadID' OR
FieldName='convertedLocationID' OR
FieldName='convertedPersonID' OR
FieldName='convertedOpportunityID' OR
FieldName='description' OR
FieldName='officePhone' OR
FieldName='mobilePhone' OR
FieldName='website' OR
FieldName='partnerLocationID' OR
FieldName='officePhone' OR
FieldName='partnerLocationID' OR
FieldName='partnerSalesPersonID' OR
FieldName='partnerSalesManagerPersonID' OR
FieldName='distiLocationID' OR
FieldName='vendorAccountManagerPersonID' OR
FieldName='approvalStatusID' OR
FieldName='acceptedByPartner' OR
FieldName='acceptedByPartnerDate' OR
FieldName='viewedByPartnerDate' OR
FieldName='currency' OR
FieldName='rejectionReason' OR
FieldName='progressID' OR
FieldName='sourceID' OR
FieldName='campaignID' OR
FieldName='vendorSalesManagerPersonID' OR
FieldName='sponsorPersonID' OR
FieldName='annualRevenue' OR
FieldName='sponsorFirstname' OR
FieldName='LeadID' OR
FieldName='leadTypeID' OR
FieldName='leadTypeStatusID' OR
FieldName='salutation' OR
FieldName='LastName' OR
FieldName='Company' OR
FieldName='Address1' OR
FieldName='Address2' OR
FieldName='Address3' OR
FieldName='Address4' OR
FieldName='Address5' OR
FieldName='active' OR
FieldName='distiContactPersonID' OR
FieldName='sponsorLastname' OR
FieldName='enquiryType');



