/* set the properties on various tables as being lookup tables */
exec setExtendedProperty @level1Object='table', @level1name='lookupList',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppType',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppSource',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppStatus',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppStage',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppReason',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppCustomerType',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppPricingStatus',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppPaymentTerms',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='organisationType',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='currency',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='trngPersonCertificationStatus',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='campaign',@level2Object=null,@level2name=null,@property='isLookup',@value='true'

/*select dbo.getExtendedProperty('table','lookupList',null,null,'isLookup')*/

/* set the name expression to be the name fullname column */
update schemaTable set nameExpression='fullname' where entityName='person' and nameExpression !='fullname'
update schemaTable set nameExpression='opportunitySource' where entityName='oppSource' and isNull(nameExpression,'') != 'opportunitySource' 
update schemaTable set nameExpression='oppType' where entityName='oppType' and isNull(nameExpression,'') != 'oppType' 
update schemaTable set nameExpression='status' where entityName='oppStatus' and isNull(nameExpression,'') != 'status' 
update schemaTable set nameExpression='oppReason' where entityName='oppReason' and isNull(nameExpression,'') != 'oppReason' 
update schemaTable set nameExpression='customerType' where entityName='oppCustomerType' and isNull(nameExpression,'') != 'customerType' 
update schemaTable set nameExpression='typeTextID' where entityName='organisationType' and isNull(nameExpression,'') != 'typeTextID'
update schemaTable set nameExpression='opportunityStage' where entityName='oppStage' and isNull(nameExpression,'') != 'opportunityStage'
update schemaTable set nameExpression='itemText' where entityName='lookupList' and isNull(nameExpression,'') != 'itemText'
update schemaTable set nameExpression='title_defaultTranslation' where entityName='trngCertification' and isNull(nameExpression,'') != 'title_defaultTranslation'
update schemaTable set nameExpression='currencyISOCode' where entityName='currency' and nameExpression is null

/* get the name expression from the extended property */
exec setExtendedProperty @level1Object='table', @level1name='oppPricingStatus',@level2Object=null,@level2name=null,@property='nameField',@value='oppPricingStatus'
exec setExtendedProperty @level1Object='table', @level1name='oppPaymentTerms',@level2Object=null,@level2name=null,@property='nameField',@value='paymentTerm'
exec setExtendedProperty @level1Object='table', @level1name='oppCustomerType',@level2Object=null,@level2name=null,@property='nameField',@value='customerType' -- noticed that oppCustomerType not always in schemaTable, so this is quick fix...

/* NJH set org/account type as a lookup field */
exec setExtendedProperty @level1Object='table', @level1name='organisationType',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='organisationType',@level2Object=null,@level2name=null,@property='nameField',@value='typeTextID'

/* initialise/populate the customMappingDefinition table */
insert into connectorCustomMapping (connectorType,direction,entityName,columnName,customised,selectStatement,joinStatement,selectDependencyStatement) 
select b.connectorType,b.direction,b.entityName,b.columnName,0,b.selectStatement,b.joinStatement,b.selectDependencyStatement from
(
	select 'salesforce' as connectorType,'E' as direction, 'opportunity' as entityName,'entityID' as columnName,'endCustLoc.crmLocID' as selectStatement,'left join (location endCustLoc inner join person endCustPer on endCustPer.locationID = endCustLoc.locationID) on endCustLoc.organisationID = opportunity.entityID and endCustPer.personID = opportunity.contactPersonID' as joinStatement,'endCustLoc.locationID as [export'+char(172)+'accountID'+char(172)+'location]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'opportunity' as entityName,'pricebookID' as columnName,'dbo.listGetAt(op.crmPricebookID,1,''_'')' as selectStatement,'left join (select distinct opportunityId,currency,countryId,crmPricebookID from opportunityProduct op inner join pricebookEntry pbe on op.pricebookEntryID = pbe.pricebookEntryID inner join pricebook p on p.pricebookId = pbe.pricebookID) op on op.opportunityID = opportunity.opportunityID and opportunity.currency = op.currency and opportunity.countryID = op.countryID' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'person' as entityName,'accManager' as columnName,'accManager.crmPerID' as selectStatement,'left join (integerFlagData ifd inner join person accManager on accManager.personID = ifd.data inner join vFlagDef f on f.flagID = ifd.flagID and f.flagTextID=''locAccManager'') on ifd.entityID = account.locationID' as joinStatement,'accManager.personID as [exportUser' +char(172)+ 'ownerID' +char(172)+'personID]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'person' as entityName,'Country' as columnName,'c.r1CountryID' as selectStatement,'inner join country c on c.countryID = account.countryID' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'person' as entityName,'address5' as columnName,'account.address5' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'person' as entityName,'accountTypeID' as columnName,'dbo.replaceConnectorMappingValue(''person'',account.accountTypeID,''E'','';'','','')' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'location' as entityName,'organisationID' as columnName,'isNull(isNull(isNull(HQLocation.organisationID,parent.organisationID),salesforce_account.OrganisationID),location.organisationID)' as selectStatement,'left join location HQLocation on HQLocation.crmLocID = salesforce_account.parentID and len(salesforce_account.parentID) != 0 left join connector_salesforce_account parent on parent.ID = salesforce_account.parentID and len(salesforce_account.parentID) != 0' as joinStatement,'salesforce_account.parentID as [import'+char(172)+'organisationID'+char(172)+'account]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'opportunity' as entityName,'entityID' as columnName,'endCustLoc.organisationID' as selectStatement,'left join location endCustLoc on endCustLoc.crmLocID = salesforce_opportunity.accountID' as joinStatement,'salesforce_opportunity.accountID as [import'+char(172)+'entityID'+char(172)+'account]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'opportunity' as entityName,'countryID' as columnName,'isNull(partnerLocation.countryID,endCustLoc.countryID)' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'opportunityProduct' as entityName,'productID' as columnName,'pricebookEntry.productID' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'pricebook' as entityName,'currency' as columnName,'pbe.currencyIsoCode' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'pricebook' as entityName,'countryID' as columnName,'c.countryID' as selectStatement,'left join country c on Salesforce_pricebook2.name like ''%(%''+c.isoCode+''%)%''' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'pricebookEntry' as entityName,'pricebookID' as columnName,'pricebook.pricebookID' as selectStatement,'left join pricebook on pricebook.crmPricebookID = salesforce_pricebookEntry.pricebook2ID and pricebook.currency = salesforce_pricebookEntry.currencyISOCode' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'product' as entityName,'productGroupID' as columnName,'isNull(pg.productGroupID,isNull(dbo.getSettingValue(''connector.salesforce.defaults.import.product.productGroup''),3))' as selectStatement,'left join productGroup pg on pg.description_defaultTranslation = Salesforce_product2.Family and len(Salesforce_product2.Family) > 0' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'asset' as entityName,'entityID' as columnName,'endCustLoc.organisationID' as selectStatement,'left join location endCustLoc on endCustLoc.crmLocID = salesforce_asset.accountID' as joinStatement,'salesforce_asset.accountID as [import'+char(172)+'entityID'+char(172)+'account]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'location' as entityName,'HQ' as columnName,'case when location.is_HQ = 1 then '''' else HQLoc.crmLocID end' as selectStatement,'left join (location HQLoc inner join integerFlagData orgFlag on orgFlag.entityID = HQLoc.organisationId and orgFlag.data = HQloc.locationId inner join flag f on f.flagTextID=''HQ'' and f.flagID = orgFlag.flagID) on HQLoc.organisationID = location.organisationID' as joinStatement,'HQLoc.locationID as [export'+char(172)+'parentID'+char(172)+'location]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'person' as entityName,'ActualLocation' as columnName,'p.locationID' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'pricebookEntry' as entityName,'currency' as columnName,'Salesforce_pricebookEntry.currencyIsoCode' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'location' as entityName,'HQ' as columnName,'case when isNull(salesforce_account.parentID,'''') = '''' then 1 else 0 end' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'organisation' as entityName,'organisationID' as columnName,'case when ifd.data is not null then location.OrganisationID else salesforce_account.organisationID end' as selectStatement,'left join (integerFlagData ifd inner join flag f on f.flagId = ifd.flagID and f.flagTextID=''HQ'') on ifd.data = location.locationID' as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'relatedFile' as entityName,'body' as columnName,'salesforce_attachment.body' as selectStatement,null as joinStatement,null as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'relatedFile' as entityName,'body' as columnName,'connector_fileBody.body' as selectStatement,'left join connector_fileBody on connector_fileBody.fileID = relatedFile.fileID' as joinStatement,null as selectDependencyStatement
	union
	/* PROD2016-1183 - changed from inner to left join on connectorObject to handle deletions */
	select 'salesforce' as connectorType,'I' as direction, 'relatedFile' as entityName,'entityID' as columnName,'vEntityName.entityID' as selectStatement,'left join connectorObject co on salesforce_attachment.parentId like co.crmKeyPrefix+''%'' and relayware=0 left join vEntityName on vEntityName.crmID = salesforce_attachment.parentId  left join relatedFileCategory on relatedFileCategory.fileCategoryId = case when co.object=''contact'' then 51 else null end' as joinStatement,'parentID+char(172)+co.object as [import¬entityID¬object]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'E' as direction, 'relatedFile' as entityName,'entityID' as columnName,'vEntityName.crmID' as selectStatement,'left join relatedFileCategory on relatedFileCategory.fileCategoryId = relatedFile.fileCategoryID left join vEntityName on vEntityName.entityID = relatedFile.entityID and vEntityName.entityTypeID = relatedFileCategory.entityTypeID' as joinStatement,'cast(relatedFile.entityID as varchar)+char(172)+relatedFileCategory.fileCategoryEntity as [export¬parentID¬object]' as selectDependencyStatement
	union
	select 'salesforce' as connectorType,'I' as direction, 'relatedFile' as entityName,'fileCategoryID' as columnName,'relatedFileCategory.fileCategoryId' as selectStatement,null as joinStatement,null as selectDependencyStatement

) b
	left join connectorCustomMapping ccm on b.connectorType = ccm.connectorType and ccm.entityName = b.entityName and ccm.columnName = b.columnName and ccm.direction=b.direction
where
	ccm.ID is null

/* NJH 2015/04/15 - update the join for existing customers to only join on rows that have values*/	
update connectorCustomMapping set joinStatement='left join location HQLocation on HQLocation.crmLocID = salesforce_account.parentID and len(salesforce_account.parentID) != 0 left join connector_salesforce_account parent on parent.ID = salesforce_account.parentID and len(salesforce_account.parentID) != 0' where joinStatement='left join location HQLocation on HQLocation.crmLocID = salesforce_account.parentID left join connector_salesforce_account parent on parent.ID = salesforce_account.parentID'
	
/* turn off the tracking for the lastSuccessfulSynch column */
update modEntityDef set fieldMonitor=0 where tablename='connectorObject' and fieldname='lastSuccessfulSynch' and fieldMonitor!=0


/* 2015-02-12 PPB P-KAS049 add 'DocumentationUploaded' fundApprovalStatus   */
IF	NOT EXISTS	(SELECT * FROM fundApprovalStatus WHERE fundStatusTextID = 'DocumentationUploaded' )
BEGIN
INSERT INTO fundApprovalStatus (fundApprovalStatus,sortOrder,fundStatusMethod,fundStatusTextID,nextAvailableStatus,StatusGroup,levelAvailabilty,emailTextID1,emailTextID2,isApproved,isFullyApproved)
VALUES ('Documentation Uploaded',1,'','DocumentationUploaded','13,10,12','Approved','1','','',1,1)
END


/* WAB 2015-02-25 The new OOTB border sets seem to rely on elementTextID being set for PortalUtilities Page */
update element set elementTextID = 'PortalUtilities' where headline = 'Portal Utilities' and isNull(elementTextID,'') = ''

/* Jira Fifteen-269 - add scheduleType record */
if not exists (select 1 from ScheduleType)
insert into ScheduleType (ScheduleTypeTextID,ScheduleType,LogHistory,ScheduleTable) values ('Email','Email',1,'scheduleEmail')


/* work done to change the column delimiters from underscore to bent pipe 
	NJH 2017/02/23 RT-255 - This only affects SF custom mappings. Dynamics was added after this change was made so filter them out.
*/
declare @delimiter char(1)
set @delimiter = ' ' -- the delimiter for the selectDependencyStatement which is 'column as column'
update connectorCustomMapping
	set selectDependencyStatement = dbo.listGetAt(selectDependencyStatement,1,@delimiter)+' '+dbo.listGetAt(selectDependencyStatement,2,@delimiter)+' ['+replace(dbo.listGetAt(selectDependencyStatement,3,@delimiter),'_',char(172))+']'
	from connectorCustomMapping
where
	dbo.listGetAt(selectDependencyStatement,3,@delimiter) like '%[_]%'
	and connectorType='Salesforce'
	
	
/* NJH 2015/06/10 insert currencies into the currency table as now currency is a foreign key on the lead and opportunity table. I don't have the currency sign yet, but these can be updated as and when
	It also may be that some of the currencies are invalid, in which case, we can clean them up as and when as well.
 */
insert into currency(currencyIsoCode,currencyName,currencySign)
select currenciesToImport.currencyIsoCode,currenciesToImport.currencyName,'' from
(values
('AED','United Arab Emirates dirham'),
('AFN','Afghani'),
('ALL','Lek'),
('AMD','Armenian Dram'),
('ANG','Netherlands Antillian Guilder'),
('AOA','Kwanza'),
('ARS','Argentine Peso'),
('AUD','Australian Dollar'),
('AWG','Aruban Guilder'),
('AZN','Azerbaijanian Manat'),
('BBD','Barbados Dollar'),
('BDT','Bangladeshi Taka'),
('BGN','Bulgarian Lev'),
('BHD','Bahraini Dinar'),
('BIF','Burundian Franc'),
('BMD','Bermudian Dollar'),
('BND','Brunei Dollar'),
('BOB','Boliviano'),
('BRL','Brazilian Real'),
('BSD','Bahamian Dollar'),
('BTN','Ngultrum'),
('BWP','Pula'),
('BYR','Belarussian Ruble'),
('BZD','Belize Dollar'),
('CAD','Canadian Dollar'),
('CDF','Franc Congolais'),
('CHF','Swiss Franc'),
('CLP','Chilean Peso'),
('CNY','Yuan Renminbi'),
('COP','Colombian Peso'),
('COU','Unidad de Valor Real'),
('CRC','Costa Rican Colon'),
('CUP','Cuban Peso'),
('CVE','Cape Verde Escudo'),
('CYP','Cyprus Pound'),
('CZK','Czech Koruna'),
('DJF','Djibouti Franc'),
('DKK','Danish Krone'),
('DOP','Dominican Peso'),
('DZD','Algerian Dinar'),
('EEK','Kroon'),
('EGP','Egyptian Pound'),
('ERN','Nakfa'),
('ETB','Ethiopian Birr'),
('EUR','Euro'),
('FJD','Fiji Dollar'),
('FKP','Falkland Islands Pound'),
('GBP','Pound Sterling'),
('GEL','Lari'),
('GHS','Cedi'),
('GIP','Gibraltar pound'),
('GMD','Dalasi'),
('GNF','Guinea Franc'),
('GTQ','Quetzal'),
('GYD','Guyana Dollar'),
('HKD','Hong Kong Dollar'),
('HNL','Lempira'),
('HRK','Croatian Kuna'),
('HTG','Haiti Gourde'),
('HUF','Forint'),
('IDR','Rupiah'),
('ILS','New Israeli Shekel'),
('INR','Indian Rupee'),
('IQD','Iraqi Dinar'),
('IRR','Iranian Rial'),
('ISK','Iceland Krona'),
('JMD','Jamaican Dollar'),
('JOD','Jordanian Dinar'),
('JPY','Japanese yen'),
('KES','Kenyan Shilling'),
('KGS','Som'),
('KHR','Riel'),
('KMF','Comoro Franc'),
('KPW','North Korean Won'),
('KRW','South Korean Won'),
('KWD','Kuwaiti Dinar'),
('KYD','Cayman Islands Dollar'),
('KZT','Tenge'),
('LAK','Kip'),
('LBP','Lebanese Pound'),
('LKR','Sri Lanka Rupee'),
('LRD','Liberian Dollar'),
('LSL','Loti'),
('LTL','Lithuanian Litas'),
('LVL','Latvian Lats'),
('LYD','Libyan Dinar'),
('MAD','Moroccan Dirham'),
('MDL','Moldovan Leu'),
('MGA','Malagasy Ariary'),
('MKD','Denar'),
('MMK','Kyat'),
('MNT','Tugrik'),
('MOP','Pataca'),
('MRO','Ouguiya'),
('MTL','Maltese Lira'),
('MUR','Mauritius Rupee'),
('MVR','Rufiyaa'),
('MWK','Kwacha'),
('MXN','Mexican Peso'),
('MYR','Malaysian Ringgit'),
('MZN','Metical'),
('NAD','Namibian Dollar'),
('NGN','Naira'),
('NIO','Cordoba Oro'),
('NOK','Norwegian Krone'),
('NPR','Nepalese Rupee'),
('NZD','New Zealand Dollar'),
('OMR','Rial Omani'),
('PAB','Balboa'),
('PEN','Nuevo Sol'),
('PGK','Kina'),
('PHP','Philippine Peso'),
('PKR','Pakistan Rupee'),
('PLN','Zloty'),
('PYG','Guarani'),
('QAR','Qatari Rial'),
('RON','Romanian New Leu'),
('RSD','Serbian Dinar'),
('RUB','Russian Ruble'),
('RWF','Rwanda Franc'),
('SAR','Saudi Riyal'),
('SBD','Solomon Islands Dollar'),
('SCR','Seychelles Rupee'),
('SDG','Sudanese Pound'),
('SEK','Swedish Krona'),
('SGD','Singapore Dollar'),
('SHP','Saint Helena Pound'),
('SKK','Slovak Koruna'),
('SLL','Leone'),
('SOS','Somali Shilling'),
('SRD','Surinam Dollar'),
('STD','Dobra'),
('SYP','Syrian Pound'),
('SZL','Lilangeni'),
('THB','Baht'),
('TJS','Somoni'),
('TMM','Manat'),
('TND','Tunisian Dinar'),
('TOP','Pa''anga'),
('TRY','New Turkish Lira'),
('TTD','Trinidad and Tobago Dollar'),
('TWD','New Taiwan Dollar'),
('TZS','Tanzanian Shilling'),
('UAH','Hryvnia'),
('UGX','Uganda Shilling'),
('USD','US Dollar'),
('UYU','Peso Uruguayo'),
('UZS','Uzbekistan Som'),
('VEB','Venezuelan bolívar'),
('VND','Vietnamese đồng'),
('VUV','Vatu'),
('WST','Samoan Tala'),
('XCD','East Caribbean Dollar'),
('YER','Yemeni Rial'),
('ZAR','South African Rand'),
('ZMK','Kwacha'),
('ZWD','Zimbabwe Dollar'))
as currenciesToImport (currencyIsoCode,currencyName)
	left join currency c on c.currencyIsoCode = currenciesToImport.currencyIsoCode
where c.currencyName is null

/* the functionality has been moved into a trigger so this SP not needed - probably should get rid of this sp*/
update fieldTrigger set active=0 where triggername='sp_fieldTrigger_oppDistiModified' and active=1

/* create any missing indexes on crmIds for tables referenced in connector object. Do not include pricebook */
declare @colName varchar(50),@tablename varchar(50)
declare crmID_cursor cursor for select distinct column_name, table_name from information_schema.columns c inner join connectorObject o on c.table_name = o.object and o.relayware=1 where column_name like 'crm%ID' and o.object !='pricebook'
open crmID_cursor
fetch next from crmID_cursor into @colName,@tablename
while @@FETCH_STATUS = 0
begin
    declare @indexSql nvarchar(max)
    IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].['+@tablename+']') AND name = N''+@colName+'_unique')
    begin
        select @indexSql = 'update ' + @tablename + ' set ' + @colname + ' = null where ' + @colname + ' = ''''; '
        select @indexSql += ' CREATE unique INDEX '+ @colName +'_unique ON ' + @tablename +' ('+@colName+') WHERE '+@colName+' IS NOT NULL;'
        print @indexSql
        exec(@indexSql)
    end
    IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].['+@tablename+'Del]') AND name = N''+@colName+'_idx')
    begin
        set @indexSql='CREATE INDEX '+@colName+'_idx ON '+@tablename+'Del ('+@colName+');'
        exec(@indexSql)
    end
    fetch next from crmID_cursor into @colName,@tablename
end

close crmID_cursor
deallocate crmID_cursor

GO

/* remove the pricebookID unique index if it was created */
IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[pricebook]') AND name = N'crmPricebookID_unique')
drop index pricebook.crmPricebookID_unique

 /* fancybox moved to 'lib' directory */
 update siteDef set stylesheet=replace(stylesheet,'/javascript/fancybox/','/javascript/lib/fancybox/') where stylesheet like '%/javascript/fancybox/%'
 
/* MDC 2015-10-21 New email incentiveUserNameLinkEmail*/
IF NOT EXISTS (select 1 from emailDef where emailTextID = 'incentiveUserNameLinkEmail') 
BEGIN	 
	 declare @emailDefID int
	INSERT INTO [emaildef] ([eMailTextID],[type],[addressType],[toAddress],[ccAddress],[bccAddress],[includeTemplate],[parameters],[created],[createdBy],[fromAddress],[commid],[active],[messageTitle_defaultTranslation],[messageText_defaultTranslation],[messageUrl],[messageImage],[sendMessage],[moduleTextID],[Name],[lastUpdated],[lastUpdatedBy],[lastUpdatedByPerson])
	VALUES('incentiveUserNameLinkEmail',NULL,'literal','','','',NULL,NULL,'Oct 21 2015 12:20:34:000PM',422,'#application.emailFrom#@#application.mailfromdomain#',NULL,1,'','','',NULL,0,'Incentive','Incentive Program Account Created','Oct 21 2015 12:22:07:000PM',0,0)
	 select @emailDefID = scope_identity()
	 
	exec addNewPhraseAndTranslation @phraseTextID = 'subject', @entitytypeid = 200, @entityid=@emailDefID, @updateMode = 0, @languageID = 1, @phraseText= 'Welcome to the Incentive Program' 
	 
	exec addNewPhraseAndTranslation @phraseTextID = 'body', @entitytypeid = 200, @entityid=@emailDefID, @updateMode = 0, @languageID = 1, @phraseText= 
		'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
		<html>
		<head>
			<title>Business Plan Submitted</title>
		</head>
		<body style="background: rgb(247, 243, 243);">
		<style type="text/css">body { background:#f7f3f3;}
		table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;  border:2px solid #ffffff; border-collapse: collapse; }
		table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */  background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */ background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */  width: 600px; height:20px;}
		table.header{ padding-bottom:5px; width: 600px; }
		table td {padding:0; margin:0;}
		
		table#colTable td { padding:10px 0;line-height: 20px;}
		p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
		p.sig{ padding-bottom: 15px; }
		p.footer{ }
		td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
		td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
		td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
		#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
		#footerContainer p{ line-height:16px; font-size:11px;}
		</style>
		<table align="center" class="main">
			<tbody>
				<tr>
					<td>
					<table style="width: 100%;">
						<tbody>
							<tr>
								<td colspan="2" style="padding-top: 10px; padding-right: 20px; padding-left: 20px;">
								<h2>Dear [[firstname]],</h2>
		
								<p style="line-height: 20px;">Welcome to the incentive program.</p>
		
								<p>Kind regards,<br />
								<span class="sig">[[getSiteName()]]</span></p>
								</td>
							</tr>
						</tbody>
					</table>
					</td>
				</tr>
			</tbody>
		</table>
		</body>
		</html>'


	 
END

UPDATE PHRASES SET PHRASETEXT = '<b>Partner Type</b>' WHERE PHRASETEXT = '<b>Partner Type<b>' /* 2015-11-23 ACPK    Fixed missing bold end tag in phr_PartnerType */

/* NJH 2015/11/19 noticed that some flags were not marked as 'isSystem' where they should be */
update flag set isSystem=1 where flagTextID in ('PortalLeadManager','KeyContactsPrimary','PartnerOrderingManager') and isSystem=0