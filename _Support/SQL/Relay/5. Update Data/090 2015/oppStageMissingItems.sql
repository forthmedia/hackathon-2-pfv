/*
WAB 2016-02-2015
After upgrading TATA to 2015 from 2013 (1) we found that these items were not in the oppStage table
They were only being created from base_2Data (ie in OOTB), so check for them here as well 
*/


merge into oppStage as target
using
(VALUES 
   ( 'Prospecting', 0, 1, 'Prospecting', 0, 'Prospecting', NULL, 1, 1, NULL, 0, 0) 
 , ( 'Qualification', 0, 2, 'Qualification', 0, 'Qualification', NULL, 1, 1, NULL, 0, 0)
 , ( 'Proposal/Price Quote', 0, 7, 'Proposal/Price Quote', 0, 'Proposal/PriceQuote', NULL, 1, 1, NULL, 0, 0)
 , ( 'Value Proposition', 0, 4, 'Value Proposition', 0, 'ValueProposition', NULL, 1, 1, NULL, 0, 0)
 , ( 'Closed Lost', 0, 10, 'Closed Lost', 0, 'ClosedLost', NULL, 1, 1, NULL, 0, 0)
 , ( 'Closed Won', 0, 9, 'Closed Won', 0, 'ClosedWon', NULL, 1, 1, NULL, 0, 0)
 , ( 'Id. Decision Makers', 0, 5, 'Id. Decision Makers', 0, 'Id.DecisionMakers', NULL, 1, 1, NULL, 0, 0)
 , ( 'Needs Analysis', 0, 3, 'Needs Analysis', 0, 'NeedsAnalysis', NULL, 1, 1, NULL, 0, 0)
 , ( 'Negotiation/Review', 0, 8, 'Negotiation/Review', 0, 'Negotiation/Review', NULL, 1, 1, NULL, 0, 0)
 , ( 'Perception Analysis', 0, 6, 'Perception Analysis', 0, 'PerceptionAnalysis', NULL, 1, 1, NULL, 0, 0)
 , ( 'LEAD - lead that we have identified', 0, 0, 'LEAD - lead that we have identified', 0, 'LEAD-leadthatwehaveidentified', NULL, 0, 1, NULL, 0, 0)
)
as 
source ( [OpportunityStage], [DefaultValue], [SortOrder], [status], [probability], [stageTextID], [statusSortOrder], [liveStatus], [onlyShowInQuarter], [oppTypeID], [isSystem], [isWon])
ON target.stageTextID = source.stageTextID 
WHEN NOT MATCHED BY TARGET THEN
INSERT ( [OpportunityStage], [DefaultValue], [SortOrder], [status], [probability], [stageTextID], [statusSortOrder], [liveStatus], [onlyShowInQuarter], [oppTypeID], [isSystem], [isWon]) 
VALUES ( [OpportunityStage], [DefaultValue], [SortOrder], [status], [probability], [stageTextID], [statusSortOrder], [liveStatus], [onlyShowInQuarter], [oppTypeID], [isSystem], [isWon]);


