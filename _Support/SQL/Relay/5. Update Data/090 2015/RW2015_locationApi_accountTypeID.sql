/*
RJT & GCC 2015-07-14
Found in case 445237 but in all customers, creating a location pulls in accountTypeID 
and if it doesn't have an api name the create fails

*/
update ModEntityDef set apiname = 'accountTypeID' where TableName = 'location' and FieldName = 'accountTypeID'
and apiname is null