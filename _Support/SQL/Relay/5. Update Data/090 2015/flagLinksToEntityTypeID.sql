/*
WAB 2015-11-17 CASE 445999 
One particular flags in the OOTB data need to have linksToEntityTypeID set to 0 (person) 
The flags are used to allocated people to usergroups and the queries will not work without these values set
(I have tried to run this query on most databases already) 

WAB 2015-11-30 One of these flags extFundManager turned out to not be a flag which links to a person, actually links to fundCompanyAccount  

*/

update 
	flag 
set 
	linksToEntityTypeID = 0 
from 
	flag f
		inner join
	vflagdef fd on f.flagid = fd.flagid		
where 
	f.flagTextid in ('KeycontactsTraining') 
	and dataTable like 'integer%'
	and f.linksToEntityTypeID is null
	

update 
	flag 
set 
	linksToEntityTypeID = (select entityTypeID from schemaTable where entityname like 'fundcompanyaccount') 
from 
	flag f
		inner join
	vflagdef fd on f.flagid = fd.flagid		
where 
	f.flagTextid in ('extFundManager') 
	and dataTable like 'integer%'
	and (f.linksToEntityTypeID is null or f.linksToEntityTypeID = 0)  
	


	
		 