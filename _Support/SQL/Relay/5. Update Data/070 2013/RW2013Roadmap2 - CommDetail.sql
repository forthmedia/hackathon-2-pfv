/*
2013-11-21	WAB	2013RoadMap2 Item 25 Contact History 
Major Changes to commdetail/contact history
	Delete all web visit records
		This information will now be derived from the entityTracking table and stored as an activity
	Create new CommTypeIDs for System Emails and Ad Hoc Emails
		Update all existing data	
	Move subject/body of system and adhoc emails and contact records into commDetailContent table
*/


		declare @rows int, @msg varchar (100), @startTime dateTime, @rowsToDelete int  = 0, @totalRows int = 0 
		select @rowsToDelete = count(*) from commdetail where feedback = 'web visit'

		set @rows = 1		
		SET ROWCOUNT 10000
		SET nocount on
		while @rows <> 0 
		BEGIN
			select @startTime = getdate()
				delete from commdetail where feedback = 'web visit' and commTypeID = 11
			
			set @rows = @@rowcount	
			set @totalRows = @totalRows  + @rows
			select @msg = convert(varchar, @totalRows) + '/' + convert(varchar, @rowsToDelete) + ' web visit rows deleted  ' + convert(varchar,datediff(s,@starttime,getdate())) + 's'
			RAISERROR(@msg ,0,1) WITH NOWAIT
		END
		SET ROWCOUNT 0

GO

/* Create new commDetailTypes, chosen some IDs above any existing ones (I hope!) */
IF not exists (Select 1 from commDetailType where commTypeID = 49)
	insert into commDetailType (commTypeID, name, description, showincommhistory, showinAddcomm,createdby,created,lastupdatedby,lastupdated)
	values (49,'Feedback Form', 'Feedback Form', 1,0,0,getdate(),0,getdate())

IF not exists (Select 1 from commDetailType where commTypeID = 50)
	insert into commDetailType (commTypeID, name, description, showincommhistory, showinAddcomm,createdby,created,lastupdatedby,lastupdated)
	values (50,'Standard Email', 'Standard  Email', 1,0,0,getdate(),0,getdate())

IF not exists (Select 1 from commDetailType where commTypeID = 51)
	insert into commDetailType (commTypeID, name, description, showincommhistory, showinAddcomm,createdby,created,lastupdatedby,lastupdated)
	values (51,'Email', 'Email', 1,0,0,getdate(),0,getdate())

IF not exists (Select 1 from commDetailType where commTypeID = 52)
	insert into commDetailType (commTypeID, name, description, showincommhistory, showinAddcomm,createdby,created,lastupdatedby,lastupdated)
	values (52,'Message', 'Message', 1,0,0,getdate(),0,getdate())


/* Rename email to communication */
update CommdetailType set Name = 'Communication', description = 'Communication' where commTypeID = 2 and name = 'Email'
update CommdetailType set Name = 'Web Visit', description = 'Web Visit' where commTypeID = 11 and name = 'Web Site Visit'

update commDetailType set showInCommHistory = 0 where name = 'fax'

GO

/*
Update all adHoc Emails to new commTypeID
*/

disable trigger commDetail_UTrig ON commdetail
		declare @rows int, @msg varchar (100), @startTime dateTime, @rowsToDelete int  = 0, @totalRows int = 0 , @adhocCommID int

		select @adhocCommID = commid from communication where title = 'AdHocComms'

		select @rowsToDelete = count(*) from  commdetail where commid = @adhocCommID and commTypeID =2 

		set @rows = 1		
		SET ROWCOUNT 10
		SET nocount on
		while @rows <> 0 
		BEGIN
			select @startTime = getdate()
			
			update commdetail set commTypeID = 51 
			where commid = @adhocCommID and commTypeID =2 

			set @rows = @@rowcount	
			set @totalRows = @totalRows  + @rows
			select @msg = convert(varchar, @totalRows) + '/' + convert(varchar, @rowsToDelete) + ' AddHocComms - commTypeID set to 51  ' + convert(varchar,datediff(s,@starttime,getdate())) + 's'
			RAISERROR(@msg ,0,1) WITH NOWAIT
		END
		SET ROWCOUNT 0
		SET nocount off
GO
enable trigger commDetail_UTrig ON commdetail


/*
Update all system Emails to new commTypeID
*/

		
		declare @rows int, @msg varchar (100), @startTime dateTime, @rowsToDelete int  = 0, @totalRows int = 0 
		
		 select @rowsToDelete = count(*) from commdetail cd  where commTypeID = 11

		set @rows = 1		
		SET ROWCOUNT 10000
		SET nocount on
		while @rows <> 0 
		BEGIN
			select @startTime = getdate()
				delete commdetail from commdetail cd  where commTypeID = 11
			
			set @rows = @@rowcount	
			set @totalRows = @totalRows  + @rows
			select @msg = convert(varchar, @totalRows) + '/' + convert(varchar, @rowsToDelete) + ' web visit rows deleted  ' + convert(varchar,datediff(s,@starttime,getdate())) + 's'
			RAISERROR(@msg ,0,1) WITH NOWAIT
		END
		SET ROWCOUNT 0
		
		
/*
Any items left of CommTypeID 11, will I believe be items added from either the relaytags FeedbackEmailForm.cfm or Suggestions.cfm		
*/
			update 
				commdetail 
			set 
				commTypeID = 49 
			where 
					Feedback <> 'Web Visit' 
				and commTypeID = 11 
				
				
				
				
/*
Move content of feedback column (for Adhoc Emails and contact Records) to the new commDetailContent.Body field
*/


insert into commDetailContent
(commdetailID, subject,body)
select 
	cd.commdetailid,
	left(feedback, charindex(char(172),feedback)-1),
	right(feedback, len(feedback) - charindex(char(172),feedback))
from 
	commdetail cd
		left join 
	commdetailContent cdc ON cd.commdetailid = cdc.commdetailid
where 
	commtypeid = 51 
	and feedback like '%'+char(172)+'%'
	and cdc.commdetailid is null

update 
	commdetail 
set 
	feedback = null
from 
	commdetail cd
		inner join 
	commdetailContent cdc ON cd.commdetailid = cdc.commdetailid
where 
	commtypeid = 51 
	and feedback like '%'+char(172)+'%'


declare @adhocCommID int
select @adhocCommID = commid from communication where title = 'AdHocComms'

insert into commDetailContent
(commdetailID, subject,body)
select 
	cd.commdetailid,
	'',
	feedback
from 
	commdetail cd
		left join 
	commdetailContent cdc ON cd.commdetailid = cdc.commdetailid
where 
	commid = @adhocCommID 
	and isnull(feedback,'') <> ''
	and cdc.commdetailid is null

update 
	commdetail 
set 
	feedback = null
from 
	commdetail cd
		inner join 
	commdetailContent cdc ON cd.commdetailid = cdc.commdetailid
where 
	commid = @adhocCommID 
	and isnull(feedback,'') <> ''