/* NJH  - add SalesForceProxyUser usergroup */
declare @SalesForceProxyUserID int
select @SalesForceProxyUserID=min(usergroupID)+1 from usergroup where usergroupID+1 not in (select usergroupID from usergroup)

if not exists (select 1 from userGroup where name='SalesForceProxyUser')
insert into userGroup (userGroupID,name,createdBy,lastUpdatedBy,userGroupType) values (@SalesForceProxyUserID,'SalesForceProxyUser',4,4,'Other')
GO

/* Add import synched flags */
update flag set flagTextID=flagTextID+'_export', name='Synched to SalesForce', namePhraseTextID=namePhraseTextID+'_export' where flagTextID like '%SalesForceSynched'

if not exists(select 1 from flag where flagTextID='orgSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'orgSalesForceSynched_import','flag_orgSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='orgSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='locSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'locSalesForceSynched_import','flag_locSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='locSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='perSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'perSalesForceSynched_import','flag_perSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='perSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='oppSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'oppSalesForceSynched_import','flag_oppSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='oppSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='pricebookSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'pricebookSalesForceSynched_import','flag_pricebookSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='pricebookSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='productSalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'productSalesForceSynched_import','flag_productSalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='productSalesForceSynch'
GO

if not exists(select 1 from flag where flagTextID='pricebookEntrySalesForceSynched_import')
insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated) select flaggroupID,'Synched from SalesForce',4,1,'pricebookEntrySalesForceSynched_import','flag_pricebookEntrySalesForceSynched_import',404,getdate(),404,getdate() from flaggroup where flagGroupTextID='pricebookEntrySalesForceSynch'
GO

/* set some columns to null to clean them up */
/*
update salesForceError set objectID=null where objectID='0' or objectID=''
update salesForceError set entityID=null where entityID=0
*/

update schematable set nameExpression='Name' where entityName='pricebook' and entityname !='Name'
update schematable set nameExpression='title_defaultTranslation' where entityName='product' and entityname !='title_defaultTranslation'

/*
update salesForceSynchPending set direction='I' where (entityID is null or entityID=0 and objectId is not null) and direction is null
update salesForceSynchPending set direction='E' where (entityID is not null and entityID != 0) and direction is null
*/