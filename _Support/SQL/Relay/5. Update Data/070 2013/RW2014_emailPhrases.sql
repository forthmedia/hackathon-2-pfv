/*
WAB 2014-09-29 CASE 439422
Add new patterns to the emailPhrases table

Create a temporary table with all the new phrases and then merge it in to the existing table 

Note sortOrder = 8 which puts this test before the tests which just look for 'Mail Delivery Failure'
*/

declare @newPhrases Table (phrase nvarchar(255), action varchar(100), commStatus int, feedback varchar(255), sortOrder int)

	
insert into @newPhrases 
(phrase , action , commStatus , feedback , sortorder )
select ' 554 Denied','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554 Email rejected due to security policies','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554 mail server permanently rejected message','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554% blacklisted','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554% access denied','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' 554 %spam','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' 554 %Sender denied','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554% high spam score','updatecommstatus',-21,'Detected as Spam',8
union 
select 	' Reason: 554% banned content','updatecommstatus',-21,'Detected as Spam',8



insert into emailPhrases
	(phrase , action , commStatus , feedback , sortorder )
select 
	n.phrase , n.action , n.commStatus , n.feedback , n.sortorder
from 
	@newPhrases n
		left join 
	emailPhrases ep on n.phrase = ep.phrase
where 
	ep.phraseid is null
