/* NJH 2014/09/23 in an attempt to get the build to work, I've re-add the column  - the script later on drops it again */
if not exists (select 1 from information_schema.columns where table_name='Communication' and column_name='fromDisplayName')
alter table communication add FromDisplayName varchar(1)
GO

if exists (select 1 from information_schema.columns where table_name='Communication' and column_name='fromDisplayName')
begin
declare @sql nvarchar(max)
declare @name varchar(500);

select @sql = 'insert into [commFromDisplayName] (displayName) select fromDisplayName from Communication where FromDisplayName is not null and FromDisplayName != '''' group by fromDisplayName'

select @sql = @sql+' update Communication set commFromDisplayNameID = (select commFromDisplayNameID from commFromDisplayName dm where Communication.fromDisplayName = dm.displayName)'


declare db_cursor CURSOR FOR  
select distinct fromdisplayname from Communication
where FromDisplayName is not null and FromDisplayName != '';

select @sql = ''

OPEN db_cursor;
FETCH NEXT FROM db_cursor INTO @name   
WHILE @@FETCH_STATUS = 0
BEGIN   
       select @sql += ' update commFromDisplayName set createdby = (select top 1 createdby from communication where fromdisplayname = ''' + @name + ''' order by created desc) where DisplayName = ''' + @name + '''';
       FETCH NEXT FROM db_cursor INTO @name;
END

CLOSE db_cursor;
DEALLOCATE db_cursor;

exec sp_executesql @sql

ALTER TABLE communication DROP COLUMN [fromDisplayName]

end


/*
WAB 2013-09-04
For some reason which I haven't quite worked out (but not really important) these passKeys settings were not getting created
Better this way since this ensures that we will have unique passkeys per database
*/

if not exists(select 1 from settings where name='security.encryptionmethods.standard.passkey')
BEGIN
	insert into settings (Name, Value) 
	values ('security.encryptionmethods.standard.passkey',left (replace(newid(),'-',''),10))
END

if not exists(select 1 from settings where name='security.encryptionmethods.rwsso.passkey')
BEGIN
	insert into settings (Name, Value) 
	values ('security.encryptionmethods.rwsso.passkey',left (replace(newid(),'-',''),16))
END

/* NJH set some modules for some standard email defs - still a few more to do - order is important!!!! */
update emailDef set moduleTextID='Funds' where emailTextID like 'MDF%' and moduleTextID is null
update emailDef set moduleTextID='incentive' where emailTextID like 'incentive%' and moduleTextID is null
update emailDef set moduleTextID='eLearning' where (emailTextID like 'trng%' or emailTextID like 'elearning%') and moduleTextID is null
update emailDef set moduleTextID='LeadManager' where (emailTextID like '%opp%' or emailTextID like 'deal%' or emailTextID like 'asset%') and moduleTextID is null
update emailDef set moduleTextID='Content' where emailTextID like 'Locator%' and moduleTextID is null
update emailDef set moduleTextID='Orders' where emailTextID like '%order%' and moduleTextID is null
update emailDef set moduleTextID='DataTools' where (emailTextID like '%registration%' or emailTextID like '%password%' or emailTextID in ('PerRejectedEmail','sendToFriendEmail','ActionsEmail')) and moduleTextID is null

update schematablebase set mnemonic='F' where entityname='Files' and isNull(mnemonic,'') !='F' 

update schematablebase set entityname='broadcastAccount',uniqueKey='broadcastAccountID',mnemonic='BCA' where entityname='twitterAccount'



