/* delete old entity tracking for 404 user */
declare @x int = 1, @total int=0
declare @message varchar(100)
set rowCount 50000
while (@x!=0)
begin
	delete from EntityTrackingBase where personID = 404 and visitID is null
	set @x = @@rowcount
	set @total = @total+@x
	set @message = cast(@total as varchar)+' rows deleted in total'+ convert(varchar, getdate(),9)
	RAISERROR (@message, 10, 1) WITH NOWAIT
end
GO
set rowCount 0
GO

if exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='fromLoc')
	and exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='toLoc')
begin
declare @sql nvarchar(max)

/* replace occurrences of &#38; with & in entityTracking */
select @sql = 'update entityTrackingBase
	set fromLoc = replace(fromLoc,''&##38;'',''&''), toLoc = replace(toLoc,''&##38;'',''&'')
where fromLoc like ''%&#38;%'' or toLoc like ''%&#38;%'''

exec (@sql)
end
GO

-- if we still have the fromLoc and toLoc columns on the entityTracking table, then create a temp table to do the normalising work
if exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='fromLoc')
	and exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='toLoc')
begin
	declare @sql nvarchar(max)

	select @sql = 'select distinct * into entityTrackingUrlTemp from
	(select distinct fromLoc as loc,cast(null as nvarchar(max)) as newLoc from entityTrackingBase
		union
	select distinct toLoc as loc,cast(null as nvarchar(max)) as newLoc from entityTrackingBase) as base'
	
	exec (@sql)
end

GO

/* create temp function to remove items from query string */
CREATE function [dbo].[removeSingleItemFromQueryString](@queryString nvarchar(max),@parameterName varchar(100)) RETURNS nVARCHAR(max)
AS
BEGIN

	Declare @ParameterNamePosition INT, 
			@ParameterValuePosition INT, 
			@FirstCharacter varchar(1), 
			@EndDelimiterCharacter varchar(2), 
			@EndDelimiterPosition INT,
			@delimiter char(1) = '&'
	Declare @Result VARCHAR(1000)
	
	set @result = @queryString
	-- find position of @parameterName in the string
	SELECT @ParameterNamePosition = charindex(upper(@parameterName) + '=',upper(@queryString))

	IF @ParameterNamePosition <> 0 AND substring (@queryString,@ParameterNamePosition-1,1) IN ('?','&') 
		-- @parameterName Found
		BEGIN
			SELECT @ParameterValuePosition  = @ParameterNamePosition + LEN(@parameterName) + 1
			-- now find end character	
			SELECT @EndDelimiterPosition = CHARINDEX(@delimiter,@queryString,@ParameterValuePosition) 

			IF @EndDelimiterPosition = 0
				BEGIN
					SELECT @EndDelimiterPosition = len (@queryString)+1
					SELECT @ParameterNamePosition = @ParameterNamePosition - 1
				END	
			ELSE
				BEGIN
					SELECT @EndDelimiterPosition = @EndDelimiterPosition + 1
				END	

			select @result = stuff (@queryString,@ParameterNamePosition, @EndDelimiterPosition - @ParameterNamePosition,'')	

		END
	
	RETURN @Result
	
END
GO

/* temp function to remove list of unwanted url variables */
create function [dbo].[removeListOfItemsFromQueryString](@queryString nvarchar(max)) RETURNS nVARCHAR(max)
AS
BEGIN
	Declare @Result nVARCHAR(max)
	
	declare @unwantedParams varchar(2000), @param varchar(20)
	set @unwantedParams = 'rwsessionToken,formState,_cf_noDebug,spl,courseSortOrder,moduleSortOrder,jsessionid,flush'

	set @result = @queryString
	while len(@unwantedParams) > 0
	begin
	  set @param = left(@unwantedParams, charindex(',', @unwantedParams+',')-1)
	  select @result = dbo.removeSingleItemFromQueryString(@result,@param)
	  set @unwantedParams = stuff(@unwantedParams, 1, charindex(',', @unwantedParams+','), '')
	end
	
	set @result = ltrim(rtrim(@result))
	if (right(@result,1) = '?')
		set @result = substring(@result,1,len(@result)-1)
	return @result
End
GO

/* if the entityTrackingUrlTemp table exists, then we want to normalise the enitytTracking table */
if exists (select 1 from information_schema.tables where table_name='entityTrackingUrlTemp')
begin

	update entityTrackingURlTemp
		set newLoc = dbo.removeListOfItemsFromQueryString(ltrim(rtrim(loc))) 
	where newLoc is null

	--insert the sorted/trimmed versions of the url
	insert into entityTrackingURL(url)
	select distinct newLoc from entityTrackingURlTemp t
		left join entityTrackingURL u on t.newLoc = u.url
	where u.url is null
		and t.newLoc is not null
		and len(rtrim(ltrim(t.newLoc))) > 0
	
	-- update the entityTracking table
	declare @x int = 1, @total int=0
	declare @message varchar(100)
	set rowCount 200000
	while (@x!=0)
	begin
		update entityTrackingBase set fromUrlID = entityTrackingUrlID
		from 
			entityTrackingBase et 
				inner join entityTrackingUrlTemp ett on et.fromLoc = ett.loc
				inner join entityTrackingURL etu on etu.url = ett.newLoc
		where et.fromUrlID is null
			and et.fromLoc is not null 
--	WAB Removed this line, seemed to cause the query to be very very much slower and there don't seem to be many blank items in ett
--		and len(rtrim(ltrim(et.fromLoc))) != 0
		
		set @x = @@rowcount
		set @total = @total+@x
		set @message = cast(@total as varchar)+' rows updated in total ' + convert(varchar, getdate(),9)
		RAISERROR (@message, 10, 1) WITH NOWAIT
	end
	
	set @x=1
	set @message = ''
	set @total = 0
	
	while (@x!=0)
	begin
		update entityTrackingBase set toUrlID = entityTrackingUrlID
		from 
			entityTrackingBase et 
				inner join entityTrackingUrlTemp ett on et.toLoc = ett.loc
				inner join entityTrackingURL etu on etu.url = ett.newLoc
		where et.toUrlID is null
			and et.toLoc is not null 
			--and len(rtrim(ltrim(et.toLoc))) != 0
		
		set @x = @@rowcount
		set @total = @total+@x
		set @message = cast(@total as varchar)+' rows updated in total' + convert(varchar, getdate(),9)
		RAISERROR (@message, 10, 1) WITH NOWAIT
	end
end
GO

-- remove the temporary functions
drop function removeListOfItemsFromQueryString
drop function removeSingleItemFromQueryString

if exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='fromLoc')
alter table entityTrackingBase drop column fromLoc
if exists (select 1 from information_schema.columns where table_name='entityTrackingBase' and column_name='toLoc')
alter table entityTrackingBase drop column toLoc
GO

/* if we have normalised the data, do some clean up */
if exists (select 1 from information_schema.tables where table_name='entityTrackingUrlTemp')
begin
	/* to clean up unused space - move to a new table, delete data from original table, and then stick data back in */
	select * into entityTrackingBaseTemp from entityTrackingBase
	truncate table entityTrackingBase
	
	/* disable the indexes to speed up the insert */
	alter index idx_entityTypeID_personID_created_include_visitID on entityTrackingBase disable
	alter index IX_Compound_EntityID_EntityTypeID on entityTrackingBase disable
	alter index IX_EntityTracking_CommID on entityTrackingBase disable
	alter index IX_PersonID on entityTrackingBase disable
	alter index IX_PersonID2 on entityTrackingBase disable
	alter index PK_EntityTracking on entityTrackingBase disable
	
	/* put data back into the original table */
	insert into entityTrackingBase(entityTypeID,entityId,visitId,commID,fromurlId,tourlID,personID,created)
	select entityTypeID,entityId,visitId,commID,fromurlId,tourlID,personID,created from entityTrackingBaseTemp
	
	alter index idx_entityTypeID_personID_created_include_visitID on entityTrackingBase rebuild
	alter index IX_Compound_EntityID_EntityTypeID on entityTrackingBase rebuild
	alter index IX_EntityTracking_CommID on entityTrackingBase rebuild
	alter index IX_PersonID on entityTrackingBase rebuild
	alter index IX_PersonID2 on entityTrackingBase rebuild
	alter index PK_EntityTracking on entityTrackingBase rebuild
	
	drop table entityTrackingUrlTemp
end

set rowcount 0


/* 
2013-11-21	WAB			2013RoadMap-25
Back populate the visit column in the entityTracking table to allow historical pageVisit activity to be created, 
PageVisit activity  is now used to create the web visit item in contact history
My not be completely accurate (for example if someone visited more than one site in a day, but at least as accurate as existing data
Had to write with sp_executesql because when building OOTB the site column does not exist and the SQL will not compile
*/
IF EXISTS (SELECT 1 from information_schema.columns where table_name = 'visit' and column_name = 'site')
BEGIN
	
	declare @daysInPast int = 999, @itemsToProcess int   /* change this value to determine how far back in time we go to do the back population, can be adjusted if there are performance problems */
	declare @SQL nvarchar(max)
	
	select @itemsToProcess = count(*) from entityTracking where visitid is null and personid <> 404 and entitytypeid = 10 and clickthru = 0 and created > getdate() - @daysInPast 
	
	print convert(varchar, @itemsToProcess) + ' entityTracking Items to process'
	
	IF (@itemsToProcess <> 0)
	BEGIN
	
		/* Make an extract of the visit table, one record per person per day*/
		IF exists (select 1 from tempdb..sysobjects where id = object_id('tempdb.dbo.#tempVisitDate'))
			drop table #tempVisitDate
		
		create table #tempVisitDate
		(personid int, thedate datetime, site varchar(200) , visitid int)
		

		set rowcount 10
		select @sql = 
		'
			insert into 
				#tempVisitDate
			(	personid, thedate, site , visitid)

			select personid, convert(date,visitstarteddate) as thedate, site, min(visitid) as visitid
			from visit v
						inner join sitedefdomain sdd  on v.siteDefDomainid = sdd.id
						inner join sitedef sd on sdd.sitedefid = sd.sitedefid  and isinternal = 0
			group by personid, convert(date,visitstarteddate), site
		'
		
		exec sp_executesql @sql
		
		
		/* now add visits which straddle more than one day */
		select @sql = 
		'
			insert into 
				#tempVisitDate
			(	personid, thedate, site, visitid)
			select personid, convert(date,sessionendeddate) as thedate, site, min(visitid) as visitid
			from visit v
						inner join sitedefdomain sdd  on v.siteDefDomainid = sdd.id
						inner join sitedef sd on sdd.sitedefid = sd.sitedefid  and isinternal = 0
			where convert(date,sessionendeddate) <> convert(date,visitstarteddate)
			group by personid, convert(date,sessionendeddate), site
		'	
	
		exec sp_executesql @sql
		
		/* index should help performance */
		create index tempVisitDate on #tempVisitDate
		(
			[PersonID] ASC,
			thedate
		)
		INCLUDE ( [visitID], site)
		
		
		
		/* now populate the visitid */
		
		/* update is quicker if these indexes are disabled */
		alter index idx_entityTypeID_personID_created_include_visitID ON entityTrackingBase  disable
		
		declare 
		@rows int = 1, 
		@totalrows int = 0 , 
		@msg varchar(100)
	
		set rowcount 1000
		set nocount on
		while @rows <> 0
			BEGIN
				update entityTrackingbase set visitid = v.visitid
				 --select top 1 * 
				 from 
				entityTrackingbase et
					inner join
				entityTrackingURL etu on et.toURLid = 	etu.entityTrackingURLid 
					inner join 
				#tempVisitDate v on et.personid = v.personid and convert(date,created) = thedate and patindex (etu.url, v.site) <> 0
				where created > getdate() - @daysInPast and et.visitid is null and et.personid <> 404 and et.entityTypeID =10 
		
				set @rows = @@rowcount
				set @totalRows = @totalRows + @rows
				set @msg = convert( varchar, @totalrows) + '/' + convert( varchar, @itemsToProcess) +  ' entityTracking Items updated ' + convert(varchar,getdate(),14)
				RAISERROR(@msg ,0,1) WITH NOWAIT
		        --WAITFOR DELAY '00:00:03'
			END
		set rowcount 0
		
		/* re-enable the indexes */
		alter index idx_entityTypeID_personID_created_include_visitID ON entityTrackingBase  rebuild
		
		drop table #tempVisitDate
	
	END

END	