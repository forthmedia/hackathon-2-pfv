/* NJH 2015/01/28 - moved here from another 2013Roadmap2 file */
update userGroup set name = 'RW Admin' where name = 'FNL Admin'
update securityType set shortName = 'RWAdminTask',description='Relayware Admin Task' where shortName = 'FNLAdminTask'

/* Sprint 1 User Guides */ 
if not exists (select 1 from relayModule where moduleTextID ='About')
insert into relayModule (moduleID,name,moduleTextId,active,createdBy,lastUpdatedBy) values (58,'About Relayware','About',1,4,4)
GO

declare @fileTypeID int
declare @fileTypeGroupID int
select @fileTypeGroupID=fileTypeGroupID from fileTypeGroup where heading='User Guides'

if (@fileTypeGroupID is null)
insert into fileTypeGroup (heading,intro,lastUpdatedBy,createdBy,portalSearch)
values ('User Guides','Relayware User Guides',4,4,0)

select @fileTypeGroupID=fileTypeGroupID from fileTypeGroup where heading='User Guides'

if not exists (select 1 from fileType where fileTypeGroupID=@fileTypeGroupID and type='User Guides')
insert into fileType (fileTypeGroupID,[type],[path],secure,autounzip,lastUpdatedBy,lastUpdatedByPerson,createdBy,allowedFiles)
values (@fileTypeGroupID,'User Guides','UserGuides',0,0,4,2,4,'pdf')

select @fileTypeID = fileTypeID from fileType where fileTypeGroupID=@fileTypeGroupID and type='User Guides'
if not exists (select 1 from files where fileTypeID = @fileTypeID and name='System Utilities User Guide')

insert into files (fileTypeID,filename,revision,personID,name,languageID,lastUpdatedByPerson,lastUpdatedBy,createdBy,triggerType)
select @fileTypeID,filename,getDate(),5024,name,1,5024,528,528,0 from 
(
select 'Communications - User Guide v8.2.pdf' as filename,'Communication User Guide' as name
union
select 'Contact History and Actions User Guide v2.pdf' as filename,'Contact History and Actions User Guide' as name
union
select 'Custom Entities User Guide v1.pdf' as filename,'Custom Entities User Guide' as name
union
select 'Discussions User Guide v2.pdf' as filename,'Discussions User Guide' as name
union
select 'Event Manager - User Guide v4.pdf' as filename,'Event Manager User Guide' as name
union
select 'Relayware Quick Guide to MDF set up � with multiple approval levels v2.pdf' as filename,'MDF with Multiple Approval Levels User Guide' as name
union
select 'Relayware User''s Guide to MDF v3.pdf' as filename,'MDF User Guide' as name
union
select 'Incentives - User Guide v1.pdf' as filename,'Incentives User Guide' as name
union
select 'Media Library User Guide v3.pdf' as filename,'Media Library User Guide' as name
union
select 'Relayware Mobile App User Guide v2.pdf' as filename,'Mobile App User Guide' as name
union
select 'Relayware Portal User Guide v6.pdf' as filename,'Portal User Guide' as name
union
select 'Products User Guide v3.pdf' as filename,'Products User Guide' as name
union
select 'Relayware Getting Started Guide v3.pdf' as filename,'Getting Started User Guide' as name
union
select 'Sales Collaboration - User Guide v3.pdf' as filename,'Sales Collaboration User Guide' as name
union
select 'Sales Force Connector v3.pdf' as filename,'Salesforce Connector User Guide' as name
union
select 'Support Portal - User Guide v2.pdf' as filename,'Support Portal User Guide' as name
union
select 'Relayware Training User Guide v2.pdf' as filename,'Training User Guide' as name
union
select 'Social Relayware User Guide v2.pdf' as filename,'Social Relayware User Guide' as name
union
select 'Administration User Guide v3.pdf' as filename,'Administration User Guide' as name
union
select 'Relayware Report Designer User Guide v4.pdf' as filename,'Report Designer User Guide' as name
) b
where not exists (select 1 from files where fileTypeID=@fileTypeID and name= b.name)

GO

declare @rwAdmin int,@fileTypeGroupID int

select @rwAdmin = userGroupId from userGroup where name='RW Admin'
select @fileTypeGroupID = fileTypeGroupId from fileTypeGroup where heading='User Guides'

if not exists (select 1 from recordRights where entity='fileTypeGroup' and userGroupID=@rwAdmin and permission=2 and recordID=@fileTypeGroupID)
insert into recordRights (userGroupID,entity,recordID,permission,lastUpdatedBy,lastUpdatedByPerson,lastUpdated)
values (@rwAdmin,'fileTypeGroup',@fileTypeGroupID,2,4,2,getDate())

GO

/* Sprint 1 Communication Stylesheets */

if not exists (select 1 from fileTypeGroup where heading='Stylesheets')
insert into fileTypeGroup (heading,intro,lastUpdatedBy,createdBy,portalSearch)
values ('Stylesheets','Communication Stylesheets',4,4,0)

declare @fileTypeGroupID int
select @fileTypeGroupID=fileTypeGroupID from fileTypeGroup where heading='Stylesheets'

if not exists (select 1 from fileType where fileTypeGroupID=@fileTypeGroupID and type='Communications')
insert into fileType (fileTypeGroupID,[type],[path],secure,autounzip,lastUpdatedBy,lastUpdatedByPerson,createdBy,allowedFiles)
values (@fileTypeGroupID,'Communications','styles/emailStyles',0,0,4,2,4,'css')
GO

declare @rwAdmin int,@fileTypeGroupID int

select @rwAdmin = userGroupId from userGroup where name='RW Admin'
select @fileTypeGroupID = fileTypeGroupId from fileTypeGroup where heading='Stylesheets'

if not exists (select 1 from recordRights where entity='fileTypeGroup' and userGroupID=@rwAdmin and permission=2 and recordID=@fileTypeGroupID)
insert into recordRights (userGroupID,entity,recordID,permission,lastUpdatedBy,lastUpdatedByPerson,lastUpdated)
values (@rwAdmin,'fileTypeGroup',@fileTypeGroupID,2,4,2,getDate())


/* Change the phraseTextID for the 'help' phrases to that the textID starts with 'help'
WAB 2014-10-21 modified to prevent error if destination phrases already exist
*/
update phraseList set phraseTextID = 'help_'+replace(phraseTextID,'_help','') 
from phraseList pl
where phraseTextID like '%[_]help[_]%' and 
(phraseTextID like 'fileType%' or phraseTextID like 'emailDef%' )
and not exists (Select 1 from phraseList where phrasetextid = 'help_'+replace(pl.phraseTextID,'_help','') )



/* new Portal search */
declare @clientName varchar(50)
set @clientName = DB_NAME()

SET IDENTITY_INSERT portalSearch ON

if not exists(select 1 from portalSearch where DisplayName = 'Pages')
	INSERT INTO "portalSearch" ("portalSearchID", "displayname", "searchMethodName", "indexMethodName", "path", "lastIndexed", "sortOrder", "created", "createdBy", "lastUpdated", "lastUpdatedBy", "lastUpdatedByPerson", "active", "languageid", "collectionName") 
	VALUES (1, 'Pages', 'getPages', NULL, '/et.cfm?eid=', NULL, 1, NULL, 438, '2013-10-01 14:58:07', 438, 1032, 'True', NULL, NULL)

if not exists(select 1 from portalSearch where DisplayName = 'Resources')
	INSERT INTO "portalSearch" ("portalSearchID", "displayname", "searchMethodName", "indexMethodName", "path", "lastIndexed", "sortOrder", "created", "createdBy", "lastUpdated", "lastUpdatedBy", "lastUpdatedByPerson", "active", "languageid", "collectionName") 
	VALUES (2, 'Resources', 'getFiles', 'updateFilesSearchCollection', NULL, '2013-10-03 16:51:05', 2, NULL, 438, '2013-10-02 10:41:54', 438, 1032, 'True', 1, @clientName + 'Files')

if not exists(select 1 from portalSearch where DisplayName = 'Products')
	INSERT INTO "portalSearch" ("portalSearchID", "displayname", "searchMethodName", "indexMethodName", "path", "lastIndexed", "sortOrder", "created", "createdBy", "lastUpdated", "lastUpdatedBy", "lastUpdatedByPerson", "active", "languageid", "collectionName") 
	VALUES 	(3, 'Products', 'getProducts', NULL, '/et.cfm?eid=', NULL, 3, NULL, 438, '2013-09-30 16:14:09', 438, 1032, 'True', NULL, NULL)

if not exists(select 1 from portalSearch where DisplayName = 'Courses')
	INSERT INTO "portalSearch" ("portalSearchID", "displayname", "searchMethodName", "indexMethodName", "path", "lastIndexed", "sortOrder", "created", "createdBy", "lastUpdated", "lastUpdatedBy", "lastUpdatedByPerson", "active", "languageid", "collectionName") 
	VALUES 	(4, 'Courses', 'getCourses', NULL, '/et.cfm?eid=displayCourseWare&courseID=', NULL, 4, NULL, 438, '2013-10-01 14:21:27', 438, 1032, 'True', NULL, NULL)


SET IDENTITY_INSERT portalSearch OFF
GO

delete from settings where name = 'versions.textsearch'
GO

/* insert the activityType data */
if not exists (select 1 from activityType where activityType = 'PageSharedOnSocialSite')
insert into activityType (activityType,description,entityTypeID) values ('PageSharedOnSocialSite','Page Shared On Social Site',(select entityTypeID from schemaTable where entityname='element'))

if not exists (select 1 from activityType where activityType = 'CommClickThru')
insert into activityType (activityType,description,entityTypeID) values ('CommClickThru','Communication Click Thru',(select entityTypeID from schemaTable where entityname='communication'))

if not exists (select 1 from activityType where activityType = 'CommRead')
insert into activityType (activityType,description,entityTypeID) values ('CommRead','Communication Read',(select entityTypeID from schemaTable where entityname='communication'))

if not exists (select 1 from activityType where activityType = 'CommUnsubscribed')
insert into activityType (activityType,description,entityTypeID) values ('CommUnsubscribed','Communication Unsubscribed',(select entityTypeID from schemaTable where entityname='communication'))

if not exists (select 1 from activityType where activityType = 'FileDownloaded')
insert into activityType (activityType,description,entityTypeID) values ('FileDownloaded','File Downloaded',(select entityTypeID from schemaTable where entityname='files'))

if not exists (select 1 from activityType where activityType = 'SocialAccountConnected')
insert into activityType (activityType,description,entityTypeID) values ('SocialAccountConnected','Social Account Connected',(select entityTypeID from schemaTable where entityname='person'))

if not exists (select 1 from activityType where activityType = 'SpecialisationAwarded')
insert into activityType (activityType,description,entityTypeID) values ('SpecialisationAwarded','Specialisation Awarded',(select entityTypeID from schemaTable where entityname='specialisation'))

if not exists (select 1 from activityType where activityType = 'SpecialisationExpired')
insert into activityType (activityType,description,entityTypeID) values ('SpecialisationExpired','Specialisation Expired',(select entityTypeID from schemaTable where entityname='specialisation'))

if not exists (select 1 from activityType where activityType = 'PersonApproved')
insert into activityType (activityType,description,entityTypeID) values ('PersonApproved','Person Approved',(select entityTypeID from schemaTable where entityname='person'))

if not exists (select 1 from activityType where activityType = 'PersonRejected')
insert into activityType (activityType,description,entityTypeID) values ('PersonRejected','Person Rejected',(select entityTypeID from schemaTable where entityname='person'))

if not exists (select 1 from activityType where activityType = 'OrganisationApproved')
insert into activityType (activityType,description,entityTypeID) values ('OrganisationApproved','Organisation Approved',(select entityTypeID from schemaTable where entityname='organisation'))

if not exists (select 1 from activityType where activityType = 'OrganisationRejected')
insert into activityType (activityType,description,entityTypeID) values ('OrganisationRejected','Organisation Rejected',(select entityTypeID from schemaTable where entityname='organisation'))

if not exists (select 1 from activityType where activityType = 'PagesVisited')
insert into activityType (activityType,description,entityTypeID) values ('PagesVisited','Pages Visited',(select entityTypeID from schemaTable where entityname='siteDefDomain'))

if not exists (select 1 from activityType where activityType = 'PortalVisited')
insert into activityType (activityType,description,entityTypeID) values ('PortalVisited','Portal Visited',(select entityTypeID from schemaTable where entityname='siteDefDomain'))

if not exists (select 1 from activityType where activityType = 'LoggedIn')
insert into activityType (activityType,description,entityTypeID) values ('LoggedIn','Logged In',(select entityTypeID from schemaTable where entityname='siteDefDomain'))

if not exists (select 1 from activityType where activityType = 'DiscussionStarted')
insert into activityType (activityType,description,entityTypeID) values ('DiscussionStarted','Discussion Started',(select entityTypeID from schemaTable where entityname='discussionMessage'))

if not exists (select 1 from activityType where activityType = 'DiscussionLiked')
insert into activityType (activityType,description,entityTypeID) values ('DiscussionLiked','Discussion Liked',(select entityTypeID from schemaTable where entityname='discussionMessage'))

if not exists (select 1 from activityType where activityType = 'DiscussionCommentedOn')
insert into activityType (activityType,description,entityTypeID) values ('DiscussionCommentedOn','Discussion Commented On',(select entityTypeID from schemaTable where entityname='discussionMessage'))

if not exists (select 1 from activityType where activityType = 'ProfileUpdated')
insert into activityType (activityType,description,entityTypeID) values ('ProfileUpdated','Profile Updated',(select entityTypeID from schemaTable where entityname='flag'))

if not exists (select 1 from activityType where activityType = 'ModuleStarted')
insert into activityType (activityType,description,entityTypeID) values ('ModuleStarted','Module Started',(select entityTypeID from schemaTable where entityname='trngModule'))

if not exists (select 1 from activityType where activityType = 'ModulePassed')
insert into activityType (activityType,description,entityTypeID) values ('ModulePassed','Module Passed',(select entityTypeID from schemaTable where entityname='trngModule'))

if not exists (select 1 from activityType where activityType = 'CertificationRegistered')
insert into activityType (activityType,description,entityTypeID) values ('CertificationRegistered','Certification Registered',(select entityTypeID from schemaTable where entityname='trngCertification'))

if not exists (select 1 from activityType where activityType = 'CertificationPassed')
insert into activityType (activityType,description,entityTypeID) values ('CertificationPassed','Certification Passed',(select entityTypeID from schemaTable where entityname='trngCertification'))

if not exists (select 1 from activityType where activityType = 'CertificationUpdatePassed')
insert into activityType (activityType,description,entityTypeID) values ('CertificationUpdatePassed','Certification Update Passed',(select entityTypeID from schemaTable where entityname='trngCertification'))

if not exists (select 1 from activityType where activityType = 'CertificationExpired')
insert into activityType (activityType,description,entityTypeID) values ('CertificationExpired','Certification Expired',(select entityTypeID from schemaTable where entityname='trngCertification'))

if not exists (select 1 from activityType where activityType = 'EventRegistered')
insert into activityType (activityType,description,entityTypeID) values ('EventRegistered','Event Registered',(select entityTypeID from schemaTable where entityname='eventDetail'))

if not exists (select 1 from activityType where activityType = 'EventAttended')
insert into activityType (activityType,description,entityTypeID) values ('EventAttended','Event Attended',(select entityTypeID from schemaTable where entityname='eventDetail'))

if not exists (select 1 from activityType where activityType = 'EventRegistrationShared')
insert into activityType (activityType,description,entityTypeID) values ('EventRegistrationShared','Event Registration Shared on Social Site',(select entityTypeID from schemaTable where entityname='eventDetail'))

if not exists (select 1 from activityType where activityType = 'EventAttendanceShared')
insert into activityType (activityType,description,entityTypeID) values ('EventAttendanceShared','Event Attendance Shared on Social Site',(select entityTypeID from schemaTable where entityname='eventDetail'))

if not exists (select 1 from activityType where activityType = 'FundRequestSubmitted')
insert into activityType (activityType,description,entityTypeID) values ('FundRequestSubmitted','Fund Request Submitted',(select entityTypeID from schemaTable where entityname='fundRequestActivity'))

if not exists (select 1 from activityType where activityType = 'FundRequestApproved')
insert into activityType (activityType,description,entityTypeID) values ('FundRequestApproved','Fund Request Approved',(select entityTypeID from schemaTable where entityname='fundRequestActivity'))

if not exists (select 1 from activityType where activityType = 'FundRequestRejected')
insert into activityType (activityType,description,entityTypeID) values ('FundRequestRejected','Fund Request Rejected',(select entityTypeID from schemaTable where entityname='fundRequestActivity'))

if not exists (select 1 from activityType where activityType = 'CertificationPassShared')
insert into activityType (activityType,description,entityTypeID) values ('CertificationPassShared','Certification Pass Shared on Social Site',(select entityTypeID from schemaTable where entityname='trngCertification'))

if not exists (select 1 from activityType where activityType = 'ModulePassShared')
insert into activityType (activityType,description,entityTypeID) values ('ModulePassShared','Module Pass Shared on Social Site',(select entityTypeID from schemaTable where entityname='trngModule'))

if not exists (select 1 from activityType where activityType = 'DealRegistered')
insert into activityType (activityType,description,entityTypeID) values ('DealRegistered','Deal Registered',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'DealRegistrationRequested')
insert into activityType (activityType,description,entityTypeID) values ('DealRegistrationRequested','Deal Registration Requested',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'DealRegistrationApproved')
insert into activityType (activityType,description,entityTypeID) values ('DealRegistrationApproved','Deal Registration Approved',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'DealClosed')
insert into activityType (activityType,description,entityTypeID) values ('DealClosed','Deal Closed',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'LeadAssigned')
insert into activityType (activityType,description,entityTypeID) values ('LeadAssigned','Lead Assigned',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'LeadReceivedViaLocator')
insert into activityType (activityType,description,entityTypeID) values ('LeadReceivedViaLocator','Lead Received via Locator',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'LeadUpdated')
insert into activityType (activityType,description,entityTypeID) values ('LeadUpdated','Lead Updated',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'LeadClosed')
insert into activityType (activityType,description,entityTypeID) values ('LeadClosed','Lead Closed',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'RenewalAssigned')
insert into activityType (activityType,description,entityTypeID) values ('RenewalAssigned','Renewal Assigned',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'RenewalClosed')
insert into activityType (activityType,description,entityTypeID) values ('RenewalClosed','Renewal Closed',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'OpportunityLost')
insert into activityType (activityType,description,entityTypeID) values ('OpportunityLost','Opportunity Lost',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'SpecialPricingRequested')
insert into activityType (activityType,description,entityTypeID) values ('SpecialPricingRequested','Special Pricing Requested',(select entityTypeID from schemaTable where entityname='opportunity'))

if not exists (select 1 from activityType where activityType = 'RegisteredForIncentives')
insert into activityType (activityType,description,entityTypeID) values ('RegisteredForIncentives','Registered For Incentives',(select entityTypeID from schemaTable where entityname='rwPersonAccount'))

if not exists (select 1 from activityType where activityType = 'ClaimsRegistered')
insert into activityType (activityType,description,entityTypeID) values ('ClaimsRegistered','Incentive Claims Registered',(select entityTypeID from schemaTable where entityname='rwTransaction'))

if not exists (select 1 from activityType where activityType = 'RewardsRedeemed')
insert into activityType (activityType,description,entityTypeID) values ('RewardsRedeemed','Incentive Rewards Redeemed',(select entityTypeID from schemaTable where entityname='rwTransaction'))

/* 2013-06-25	NJH add screen for activity type */
declare @screenID int
declare @ItemID int

select @screenID = max(screenID) + 1 from screens

if not exists (select 1 from screens where screentextID = 'activityTypeEdit')
begin
	insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
	values(@screenID,'activityTypeEdit','Activity Type Edit',1,0,'activityType',(select entityTypeID from schemaTable where entityname='activityType'),0,0,'ShowScreen','edit',101,getdate())

	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenID,0,'ActivityType','ActivityType','Activity Type Text ID','view',0,'screentd','',100,getdate())
	
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,BreakLn,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenID,1,'ActivityType','Description','Activity Type Description','edit',0,1,'screentd','',100,getdate())
	
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenID,2,'HTML','This is how the activity type will display in reports','','edit',0,'screentd','',100,getdate())
	
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenID,3,'HTML','<p class="noBottomMargin"><b>Activity Type profile data</b> (Define default targets against the activity type)</p>','','edit',1,'screentd','',100,getdate())
end	
GO

/* 
	2013/12/19	YMA	Case 438444 screens now use description of the entityType in the screens listing.
	2016-04-05  WAB BF-676 Knock on effect of PROD2016-878
				Remove this update.  ScreensListing now gets entityType directly from schemaTable.label and this update was causing confusion between organization/organisation
				Really screen.entityType should be deprecated

	update screens
		set entitytype = f.description
	from screens
		join flagentitytype f on screens.entitytypeID = f.entitytypeID
	where 	
		screens.entityType != f.description
*/


-- 2013/12/18	YMA	Case 438444 create out of the box activities flag
declare @flagGroupID int
declare @entitytypeid int
declare @entityid int

if not exists(select 1 from flaggroup where flaggrouptextID = 'activityTarget')
begin
--insert flaggroup
	insert into flaggroup (ParentFlagGroupID,FlagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,Description,NamePhraseTextID,DescriptionPhraseTextID,Notes,OrderingIndex,Expiry,Scope,Viewing,Edit,Search,Download,ViewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,PublicAccess,PartnerAccess,CreatedBy,Created,LastUpdatedBy,LastUpdated,UseInSearchScreen,FlagRegionEntity,EntityMemberID,EntityMemberLive,formattingParameters,createdByPerson,lastUpdatedByPerson)
	select 0,6,(select entitytypeID from schematable where entityname = 'activityType'),
		1,'activityTarget','Activity Target','','flagGroup_activityTarget',null,'',1,
		'2100-01-01 00:00:00.000',0,1,1,0,0,0,0,0,0,0,0,100,getdate(),100,getdate(),0,null,null,0,null,0,0
--insert flags
	select @flagGroupID = flaggroupID from flaggroup where FlagGroupTextID = 'activityTarget'
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Target for Gold Partners','',42,1,100,getdate(),100,getdate(),'activityTargetGold','flag_activityTargetGold',null,0,0,null,0,null,null,0,null,0,null,null,null,null,null,null,0,0,0,0
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Target for Silver Partners','',3,1,100,getdate(),100,getdate(),'activityTargetSilver','flag_activityTargetSilver',null,0,0,null,0,null,null,0,null,0,null,null,null,null,null,null,0,0,0,0
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Target for Bronze Partners','',2,1,100,getdate(),100,getdate(),'activityTargetBronze','flag_activityTargetBronze',null,0,0,null,0,null,null,0,null,0,null,null,null,null,null,null,0,0,0,0
	insert into flag (flagGroupID,Name,Description,OrderingIndex,Active,CreatedBy,Created,LastUpdatedBy,LastUpdated,FlagTextID,NamePhraseTextID,DescriptionPhraseTextID,useValidValues,lookup,Score,Protected,subEntityTypeID,WDDXStruct,UseInSearchScreen,FlagRegionEntity,HasMultipleInstances,CurrentCount,currentCountDate,FlagRegionEntityTypeId,Expression,LinksToEntityTypeID,formattingParameters,IsSystem,showInConditionalEmails,createdByPerson,lastUpdatedByPerson)
	select @flagGroupID,'Monthly Target','',1,1,100,getdate(),100,getdate(),'MonthlyPartnerActivityTarget','flag_MonthlyPartnerActivityTarget',null,0,0,null,0,null,null,0,null,0,null,null,null,null,null,null,0,0,0,0

--insert phrases
	select @flagGroupID = 0
	select @entitytypeid = 0
	select @entityid = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flagGroup_activityTarget', @entitytypeid = @entitytypeid, @entityid=@flagGroupID, @phraseText= 'Activity Target', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_activityTargetGold', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Target for Gold Partners', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_activityTargetSilver', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Target for Silver Partners', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_activityTargetBronze', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Target for Bronze Partners', @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'flag_MonthlyPartnerActivityTarget', @entitytypeid = @entitytypeid, @entityid=@entityid, @phraseText= 'Monthly Target', @updateMode = 0

--insert screenitem
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,tdclass,specialformatting,lastupdatedby,lastupdated)
	values ((select screenID from screens where screentextID = 'activityTypeEdit'),4,'flagGroup','activityTarget','','edit',1,'screentd','noflaggroupname=1',100,getdate())
	
end
GO

/* drop the file and fileRelation table which are not being used */
if exists (select 1 from information_schema.tables where table_name='file')
drop table [file]

if exists (select 1 from information_schema.tables where table_name='fileRelation')
drop table fileRelation

if exists (select 1 from information_schema.tables where table_name='faxLog')
drop table faxLog

if exists (select 1 from information_schema.tables where table_name='faxLogFiles')
drop table faxLogFiles

/* add an oppSource of Locator - used for activities */
if not exists (select 1 from oppSource where opportunitySource='Locator')
insert into oppSource (opportunitySourceID,opportunitySource) values ((select max(opportunitySourceID)+1 from oppSource),'Locator')


/* Case 438521 Add LocatorPartnerDirectMail  email template */
if not exists(select 1 from emailDef where emailTextID = 'LocatorPartnerDirectMail')
begin
	declare
	@emailTextID nvarchar(max),
	@emailName nvarchar(max),
	@subject nvarchar(max),
	@body nvarchar(max),
	@messageTitle nvarchar(max),
	@messageText nvarchar(max),
	@messageURL varchar(100),
	@moduletextID varchar(100),
	@emailDefID int;
	
	set @emailTextID = 'LocatorPartnerDirectMail'
	set @emailName = 'Locator Partner Direct Mail'
	set @subject = N'[[getSiteName()]] Locator: [[Enquiry]]'
	set @body = N'<html><body><p>[[Firstname]] [[Lastname]] from [[organisationname]] has chosen to contact your company through the [[getSiteName()]] locator.</p><p><b>Email:</b> [[email]]</p><p><b>Phone:</b> [[person.officePhone]]</p><p><b>Enquiry:</b> [[enquiry]]</br><b>Current Situation:</b> [[CurrentSituation]]</p></body></html>'
	set @messageTitle = N'[[getSiteName()]] Locator: [[Enquiry]]'
	set @messageText = N'[[Firstname]] [[Lastname]] from [[organisationname]] has chosen to contact your company through the [[getSiteName()]] locator.

Email: [[email]]
Phone: [[person.officePhone]]

Enquiry: [[enquiry]]
Current Situation: [[CurrentSituation]]'
	set @messageURL = ''
	set @moduletextID = 'Content'
	
	INSERT INTO emaildef (emailtextID,name,addresstype,bccaddress,created,createdby,fromaddress,active,subject_defaulttranslation,body_defaulttranslation,messagetitle_defaulttranslation,messagetext_defaulttranslation,messageURL,sendmessage,moduletextID) values(@emailTextID,@emailName,'personid','relayhelpcc@relayware.com',getdate(),100,'#application.emailFrom#@#application.mailfromdomain#',1,@subject,@body,@messageTitle,@messageText,@messageURL,1,@moduletextID);
	
	select @emailDefID = emailDefID from emaildef where emailtextID = @emailTextID
	exec addNewPhraseAndTranslation @phraseTextID = 'Subject', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @subject, @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'Body', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @body, @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'MESSAGETITLE', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @messageTitle, @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'MESSAGETEXT', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @messageText, @updateMode = 0

	
end
GO

if not exists (select 1 from relayModule where moduleTextID ='Files')
insert into relayModule (moduleID,name,moduleTextId,active,createdBy,lastUpdatedBy) values (662,'Media Library','Files',1,4,4)
GO


/* 
WAB Scripted many moons ago and finally committed 2013-10-13!
create a mostRecentBounceback profile attribute
 */
declare @entityTypeID int, @flagTypeID  int
select @entityTypeID = entityTypeID from schemaTable where entityName='person'

select @flagTypeID = flagTypeID  from flagType where name = 'text'

/* create the flag group */
if not exists(select 1 from flaggroup where flagGroupTextID='mostRecentBounceback')
	insert into flaggroup (flagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,NamePhraseTextID,OrderingIndex,viewing,edit,search,download,viewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,CreatedBy,Created,LastUpdatedBy,LastUpdated,scope,entitymemberID,parentFlagGroupID,expiry,createdByPerson,lastUpdatedByPerson) 
	values (@flagTypeID,@entityTypeID,1,'mostRecentBounceback','mostRecentBounceback','',0,1,0,1,1,0,0,0,0,4,getdate(),4,getdate(),0,0,0,'2030-12-31',0,0)
GO


/* add the  flag */
if not exists(select 1 from flag where flagTextID='mostRecentBounceback')
	insert into flag (flagGroupID,Description,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,useValidValues,createdByPerson,lastUpdatedByPerson) 
	select flaggroupID,'','Most Recent Bounceback',1,1,'mostRecentBounceback','',4,getdate(),4,getdate(),1,0,0,0 from flaggroup 
	where flagGroupTextID='mostRecentBounceback'
GO


