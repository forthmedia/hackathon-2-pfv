/* NJH 2012/11/27 - set the fileFamilyID to be the fileID where null or 0 */
--update [files] set fileFamilyID=fileID where fileFamilyID is null or fileFamilyID = 0

/* NJH 2012/11/28 - added two new flagTypes */
set identity_insert flagType on
if not exists(select 1 from flagType where dataTable='decimal')
insert into flagType (flagTypeID,name,datatable,datatableFullName) values (12,'Decimal','decimal','decimalFlagData')

if not exists(select 1 from flagType where dataTable='textMultiple')
insert into flagType (flagTypeID,name,datatable,datatableFullName) values (13,'TextMultiple','textMultiple','textMultipleFlagData')

set identity_insert flagType off

GO

/* create a keywords profile */
declare @entityTypeID int
select @entityTypeID = entityTypeID from schemaTable where entityName='files'
if not exists(select 1 from flaggroup where flagGroupTextID='fileskeywords')
insert into flaggroup (flagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,NamePhraseTextID,OrderingIndex,viewing,edit,search,download,viewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,CreatedBy,Created,LastUpdatedBy,LastUpdated,scope,entitymemberID,parentFlagGroupID,expiry,createdByPerson,lastUpdatedByPerson) values (13,@entityTypeID,1,'fileskeywords','fileskeywords','flaggroup_fileskeywords',148,1,1,0,0,0,0,0,0,404,getdate(),404,getdate(),0,0,0,'2020-12-31',0,0)
GO

/* add a flag for product keywords */
if not exists(select 1 from flag where flagTextID='filesKeywords')
insert into flag (flagGroupID,Description,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,useValidValues,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Keywords','Keywords',1,1,'filesKeywords','flag_filesKeywords',404,getdate(),404,getdate(),1,0,0,0 from flaggroup where flagGroupTextID='fileskeywords'
GO

if not exists(select 1 from flag where flagTextID='keywordsProducts')
insert into flag (flagGroupID,Description,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,useValidValues,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Product Keywords','Product Keywords',1,1,'keywordsProducts','flag_keywordsProducts',404,getdate(),404,getdate(),1,1,0,0 from flaggroup where flagGroupTextID='fileskeywords'
GO

/* add validValues for product keyword flag */
if not exists(select 1 from validFieldValues where fieldName='flag.keywordsProducts')
insert into validFieldValues (fieldname,countryID,validValue,datavalue,defaultFieldCase) values ('flag.keywordsProducts',0,'func','relayProduct.getProductKeywords()','T')

/* update the active field where it's null */
update product set active=1 where active is null
update productGroup set active=1 where active is null
update productCategory set active=1 where active is null

/* add default tranlsation columns, and move decription to be a translation and add title */
exec migrateToEntityPhrases @tablename='product',@phraseTextid ='longDescription',@columnname=''
GO
exec migrateToEntityPhrases @tablename='product',@phraseTextid ='description',@columnname='description', @columnContainsPhraseTextID = 0
GO
exec migrateToEntityPhrases @tablename='product',@phraseTextid ='title',@columnname='description', @columnContainsPhraseTextID = 0
GO

/* setting the product group name field so that we have a title where possible*/
update productGroup set productGroupName = description where productGroupName is null and len(description) <=50
GO

exec migrateToEntityPhrases @tablename='productgroup',@phraseTextid ='title',@columnname='productGroupName', @columnContainsPhraseTextID = 0
GO
exec migrateToEntityPhrases @tablename='productGroup',@phraseTextid='description',@columnname='description', @columnContainsPhraseTextID = 0
GO

exec migrateToEntityPhrases @tablename='productCategory',@phraseTextid ='title',@columnname='productCategoryName', @columnContainsPhraseTextID = 0
GO

/* STCR 2013-04-09 CASE 432194 Translation of Module Set Title */
exec migrateToEntityPhrases @tablename='trngModuleSet',@phraseTextid='description',@columnname='description', @columnContainsPhraseTextID = 0
GO


/* 2013/01/03	YMA		Content Deletion */

/* Create Archive Status */
if not exists (select 1 from elementstatus where status='Archive')
	insert into elementstatus values (6,'Archive','Archive',401,1,0,null,null)
GO

if exists (select 1 from elementstatus where status='Retired' and description != 'Retired')
	update elementstatus set description = 'Retired' where status='Retired'
GO

if not exists (select 1 from information_schema.columns where table_name = 'Elementdel' and column_name = 'share')
BEGIN
ALTER TABLE dbo.Elementdel ADD
	share bit NULL
End
GO


/* NJH drop the view as it is not used */
if exists (select 1 from sysobjects where name = 'vLeadListingCalculatedBudgetV3')
DROP VIEW  vLeadListingCalculatedBudgetV3


/* YMA 2013/01/21 Cleaning up as per Wlls request.  Remove unused GetElementBranch Sp's */
if object_id('GetElementBranch') is not null
drop procedure GetElementBranch
GO
if object_id('GetElementBranchFULL') is not null
drop procedure GetElementBranchFULL
GO
if object_id('GetElementBranchPhrases') is not null
drop procedure GetElementBranchPhrases
GO
if object_id('GetElementBranchV3') is not null
drop procedure GetElementBranchV3
GO

/* Populate discussionGroup table */
SET IDENTITY_INSERT [discussionGroup] ON
if not exists(select 1 from discussionGroup where title='Technical Support Group')
INSERT INTO [dbo].[discussionGroup]
	(discussionGroupID, title, [description], createdBy)
VALUES
	(1, 'Technical Support Group', 'The quickest way to get answers to your technical support queries. A place where the whole community can share knowledge about our products.', 1032)
GO

if not exists(select 1 from discussionGroup where title='Sales Group')
INSERT INTO [dbo].[discussionGroup]
	(discussionGroupID, title, [description], createdBy)
VALUES
	(2, 'Sales Group', 'Sales Group discsussions.', 1032)
GO

if not exists(select 1 from discussionGroup where title='Social Marketing Group')
INSERT INTO [dbo].[discussionGroup]
	(discussionGroupID, title, [description], createdBy)
VALUES
	(3, 'Social Marketing Group', 'Social Marketing Group discussions.', 1032)
GO

SET IDENTITY_INSERT [discussionGroup] OFF	
	
/* 2013/01/30	YMA		Person Social Profile */
declare @screenID int
select @screenID = max(screenID) + 1 from screens
if not exists (select 1 from screens where screentextID = 'personSocialProfile')
insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
values(@screenID,'personSocialProfile','Social Profile',1,1,'person',0,0,0,'/social/personPublicProfile.cfm',
	'edit',101,getdate())
GO

/* NJH Item 13 - add a row for twitter */
if not exists (select 1 from service where serviceTextID='twitter')
insert into service (serviceTextId,name,consumerKey,consumerSecret,createdBy,lastUpdatedBy)
values ('Twitter','Twitter','','',4,4)
GO


/* WAB 2014-02-19 Removed code here referencing verityCollection
	No longer used and was causing a SQL error
*/





/* 2013/02/26	YMA		119 public profiles Organisation */
declare @screenID int
select @screenID = max(screenID) + 1 from screens
if not exists (select 1 from screens where screentextID = 'CompanyLogo')
insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
values(@screenID,'CompanyLogo','Company Logo',1,1,'Organisation',2,0,0,'/social/organisationPictureURL.cfm',
	'edit',101,getdate())
GO

/* YMA 2013/03/07 case 433524 - add valid value for frmOwnerPersonID */
if not exists (select 1 from validfieldvalues where fieldname = 'action.OwnerPersonID')
insert into validfieldvalues (fieldname,countryID,validvalue,defaultfieldcase,defaultfieldvalue,sortorder,datavalue,languageID,inanalysis,score)
values('action.OwnerPersonID',0,'*lookup','T',0,1,'SELECT  p.personid as dataValue, firstName + '' '' + lastname  + '' ('' + ISOCode + '')'' as displayValue  from person p   inner join location l on p.locationid = l.locationid   inner join country c ON c.countryID = l.countryID  inner join userGroup ug on ug.personid = p.personid  where l.countryid in (select countryid from usergroupCountry where usergroupID = #request.relaycurrentuser.usergroupid#)  and '',#application.actionUserOrgIDs#,'' like  ''%,'' + cast(p.organisationID as varchar(100)) + '',%''  and p.loginExpires > getDate()  order by firstname',null,0,'')
GO


/* NJH 2013/03/26 update some social phrases to take advantage of new merge fields*/
update vphrases set phraseText=replace(phraseText,'[[certification]]','[[trngCertification.title]]') where phraseTextID like 'share[_]%' and phrasetext like '%[[][[]certification]]%'

update vphrases set phraseText=replace(phraseText,'[[module]]','[[trngModule.title]]') where phraseTextID like 'share[_]%' and phrasetext like '%[[][[]module]]%'

update vphrases set phraseTExt=replace(phraseTExt,'[[event]]','[[event.title]]') where phraseTextID like 'share[_]%' and phrasetext like '%[[][[]event]]%'

/* NJH 2013/04/08 - moved from phrases file..update another phrase that needed updating */
update vphrases set phraseText='Expecting the following fields in comma-separated format, with the first row reserved for headers: [[fieldList]]' where phraseTextId='eLearning_Bulk_CSVIncorrectColumns'

/* Attempt to normalise the userlink table*/
-- create a table with the distinct links
if exists (select 1 from information_schema.columns where table_name='userLink' and column_name='personID') and not exists(select 1 from entityUserLink)
begin
 select distinct (ROW_NUMBER( ) OVER (order by linkID)) as linkId, linkLocation,linkDescription,linkAttribute1,linkTypeID,moduleTextID
 into userLinkDistinct
 from userLink
 
-- backup the existing link table
select * into userLinkBackup from userLink
 
-- put user links into entityUserLink table
declare @sql nvarchar(max)
select @sql = 
'insert into entityUserLink (linkId,entityID,entityTypeID,createdBy,lastUpdatedBy)
select distinct l.linkID,ul.personID,0,4,4
from userLinkDistinct l inner join userLink ul on
      l.linkLocation = ul.linkLocation 
      and l.linkDescription = ul.linkDescription
      and isNull(l.linkAttribute1,'') = isNull(ul.linkAttribute1,'''') 
      and l.linkTypeID = ul.linkTypeID
      and isNull(l.moduleTextID,'') = isNull(ul.moduleTextID,'''')
'

exec sp_executesql @sql

-- drop the foreign key on the userLink table
IF EXISTS (SELECT 1 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.FK_UserLink_Person')
   AND parent_object_id = OBJECT_ID(N'dbo.userLink')
)
  ALTER TABLE [userLink] DROP CONSTRAINT [FK_UserLink_Person]

if exists(select 1 from information_schema.columns where table_name='userLink' and column_name='personID')
alter table dbo.userLink drop column personID

delete from userLink --resetting the userLink table

set identity_insert dbo.userLink on

insert into userLink (linkId, linkLocation,linkDescription,linkAttribute1,linkTypeID,moduleTextID)
select linkId, linkLocation,linkDescription,linkAttribute1,linkTypeID,moduleTextID
from userLinkDistinct

set identity_insert dbo.userLink off

end

/* NJH populate the new file family table - we're grabbing the name of the files, ordering by language and then created */
if not exists (select * from fileFamily)
begin
set identity_insert dbo.fileFamily on

insert into fileFamily (fileFamilyId,title,createdBy,lastUpdatedBy,lastUpdatedByPerson)
select fileFamilyId,(select top 1 name from files f1 where f1.fileFamilyID = f.fileFamilyID order by languageID,created desc),4,4,5
from files f where isNull(fileFamilyID,0) != 0
group by fileFamilyId

set identity_insert dbo.fileFamily off
	
end

/* NJH set this flag as a system flag as all it's associated flags are sys flags */
update flag set isSystem=1 where flagTextId='OrgInactive'


/* WAB 2013 Comms 
Add a status for people filterd out of comms because inactive
2013-06-19 corrected - I had failed to insert the specific commstatusID (so I now have to delete the item that I accidentally inserted with a random commstatusid)  
*/
if not exists (select 1 from commdetailstatus where commstatusid = -17)
BEGIN
	delete from commdetailstatus where significance = 44   -- this is the item I accidentally inserted when the script was wrong 
	set identity_Insert commdetailstatus ON
	insert into commdetailstatus
	(commStatusID,name, description, statsReport,statsreportOrder,statusGroup, significance,created,lastupdated)
	values 
	(-17, 'Not Active','Not Active','Inactive People', 44,'NotSent',44,getdate(),getdate())
	set identity_Insert commdetailstatus OFF
END


update vphrases set phraseText='Review Functions' where phraseTextID='Sys_FilesForReviewFunctions' and countryID=0 and languageID=1

/* 2013-05-28 STCR CASE 434612 */
update vphrases set phraseText='Add Matching Pairs' where phraseTextID='elearning_AddNewOption' and countryID=0 and languageID=1
update vphrases set phraseText='Column 1' where phraseTextID='elearning_LHS_Option' and countryID=0 and languageID=1
update vphrases set phraseText='Column 2' where phraseTextID='elearning_RHS_Option' and countryID=0 and languageID=1
update vphrases set phraseText='Column 2 text is required' where phraseTextID='elearning_RHS_Option_Required' and countryID=0 and languageID=1
update vphrases set phraseText='None - use as Column 2' where phraseTextID='elearning_Matching_Answer_None' and countryID=0 and languageID=1
update vphrases set phraseText='Note: It is possible to match entries in Column 2 with more than one entry in Column 1, or with none.' where phraseTextID='elearning_MatchingAnswerSetupIntro' and countryID=0 and languageID=1
update vphrases set phraseText='Matching Column 2 pair' where phraseTextID='elearning_MatchingAnswerColumnTitle' and countryID=0 and languageID=1
update vphrases set phraseText='Choose matching Column 2 pair' where phraseTextID='elearning_AddMatchingAnswerColumnTitle' and countryID=0 and languageID=1

update filetypegroup set portalSearch = 0 where filetypegroupid in (13,4,3,6,29,30)
GO

if((select count(*) 
from phraseList 
inner join phrases on phraselist.phraseid = phrases.phraseid
where entityTypeID = 10 and phraseTextid = 'detail'
and phrasetext LIKE '%RELAY_ACTIVECONTENT%') > 0 and (select count(*) from relayTag where relayTag = 'RELAY_ACTIVECONTENT') = 0)
begin
	print 'Enable RELAY_ACTIVECONTENT on this site.'
	insert into relayTag values('RELAY_ACTIVECONTENT',1)
end
GO
-- contains(phrasetext,'RELAY_ACTIVECONTENT')



/* Update some of the api fields to use the new translated columns */
exec generate_mrAudit @tablename='product', @mnemonic='PROD'
exec generate_mrAudit @tablename='productGroup', @mnemonic='PRODG'

update modEntityDef set apiName='Title', apiActive=1 where tablename in ('product','productGroup') and fieldname = 'title_defaultTranslation' and apiName is null
update modEntityDef set apiName='Description', apiActive=1 where tablename in ('product','productGroup') and fieldname = 'description_defaultTranslation' and apiName is null

update modEntityDef set apiActive=0 where tablename in ('product','productGroup') and fieldname='description' and apiActive=1

/* set the products and myFavourites modules on */
if not exists (select 1 from relayModule where moduleTextID='Products')
insert into relayModule (moduleID,moduleTextId,name,active,created,createdBy,lastUpdated,lastUpdatedBy)
values (46,'Products','Product Manager',1,getDate(),4,getDate(),4)
GO

if not exists (select 1 from relayModule where moduleTextID='MyFavorites')
insert into relayModule (moduleID,moduleTextId,name,active,created,createdBy,lastUpdated,lastUpdatedBy)
values (57,'MyFavorites','My Favorites',1,getDate(),4,getDate(),4)

/* 2013-06-12	YMA	Case 435692 Messages at the end of registration approvals */
declare @stepID int
declare @sequenceID int

select @stepID =  max(stepID) from processactions where processID = 121
select @sequenceID =  max(sequenceID) from processactions where processID = 121 and stepID = @stepID

if (@stepID is not null)
begin

	if exists(select * from processactions where processID = 121 and stepID = 30 and sequenceID = 10 and jumpname = '/relay/approvals/RegistrationApprovalPer.cfm' and nextstepID = -1)

	update processactions
	set nextstepID = 0, jumpvalue = null 
	where processID = 121 and stepID = @stepID and sequenceID = @sequenceID

	insert into processactions
	values (121,@stepID, @sequenceID + 5, 'true', 'newstep',null,null,@stepID + 10,null,null,null,null,1)

	insert into processactions
	values (121,@stepID + 10, 10, 'structKeyExists(session,"PerApprovalMsg")', 'message',null,'#session.PerApprovalMsg#',0,null,null,null,null,1)

	insert into processactions
	values (121,@stepID + 10, 15, 'true', 'newstep',null,null,-1,null,null,null,null,1)
end

GO

if exists(select * from phrases p join phraselist pl on p.phraseID = pl.phraseID and pl.phrasetextID = 'ClickHereToCloseWindow')
update p
set p.phrasetext = replace(replace(p.phrasetext,'<p>',''),'</p>','')
from phrases p 
join phraselist pl on p.phraseID = pl.phraseID and pl.phrasetextID = 'ClickHereToCloseWindow'
GO


/* create modRegister for settings */
exec generate_mrAudit @tablename='settings',@mnemonic='SET'


/* 2013-06-25	YMA	Case 435926 fix incorrect config on person certifications screen */
declare @screenID int
select @screenID = screenID from screens where screentextID = 'personcertifications'
declare @ItemID int
if exists (select * from screens where screentextID = 'personcertifications')
begin
	if exists (select 1 from screens where screenID = @screenID and viewer != 'ShowScreen' )
		update screens set viewer = 'ShowScreen' where screenID = @screenID
	
	if exists (select 1 from screendefinition where screenID = @screenID and fieldtextID = '/eLearning/personCertifications.cfm')
		select @itemID = itemID from screendefinition where screenID = @screenID and fieldtextID = '/eLearning/personCertifications.cfm'
		update screendefinition
		set active = 1
		where itemID = @itemID
	
	if not exists (select 1 from screendefinition where screenID = @screenID and fieldtextID = '/eLearning/personCertifications.cfm')
		insert into screendefinition (screenID, fieldSource, fieldTextID, method, allcolumns,specialformatting,lastupdatedby,lastupdated)
		values (@screenID,'IncludeRelayFile','/eLearning/personCertifications.cfm','edit',1,'returnpersondata=false,allowupdate=true,sortorder=Description,returnorgdata=false',100,getdate())
end	
GO

/* WAB 2013-02-18 removed identity insert from this query - no guarantee that the ID no already used */
if not exists (select 1 from [dbo].[SecurityType] where ShortName='SSOTask')
	BEGIN
		INSERT INTO [dbo].[SecurityType] ([ShortName], [Description], [CountrySpecific], [relayWareModule]) VALUES ('SSOTask', 'SSOTask', 0, NULL)
	END
GO

-- Remove references to merge structure
update Phrases set PhraseText = replace(phraseText,'[[merge.','[[') where phraseText like '%[[]merge.%'

/* NJH 2013/09/18 - set the button column to 0 where it is null */
update eventDetail set hideUnRegisterForEventBtn = 0 where hideUnRegisterForEventBtn is null
update eventDetail set hideRegisterForEventBtn = 0 where hideRegisterForEventBtn is null

GO
/* WAB 2013-10-22 CASE 437111
	We no longer support ColdFusion expressions for generating download column values - not sure exactly when this happened, probably accidental and probably not much missed.
	No longer any in OOTB, delete any which remain in big wide world
*/
Delete from DownloadFormatDef
where fieldSource like '%#%'


/* WAB 2013-11-04 CASE 431749 
A number of emailDefs in the OOTB do not have their associated communications (must have been deleted from the scripts
This script recreates them
*/
SET identity_insert communication ON

INSERT INTO 
	communication
		(commid, title,commtypelid, projectRef,description)

SELECT 
	e.commid, emailTextID, 98,'','' 
FROM 
	emailDef e
		left join
	communication c on e.commid = c.commid
WHERE
	c.commid is null 
	and e.commid is not null	

SET identity_insert communication OFF


GO
/* 
WAB 2014-03-17
Rather a late addition to the 2013 release!
To prevent the new recent searches showing  "frmFieldname=searchString"
Do a replace
*/

update 
	tracksearch
set 
	searchString = substring (searchString,charindex('=',searchString) + 1, len (searchString))
from 
	tracksearch 
where 
	searchType = 'D' and searchString like 'frm%=%'
	