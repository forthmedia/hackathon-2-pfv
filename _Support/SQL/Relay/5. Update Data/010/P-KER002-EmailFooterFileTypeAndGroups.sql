if not exists (SELECT * FROM filetypegroup WHERE heading = 'Email Footers')  
	INSERT INTO filetypegroup
	(heading, intro, screenID)
	VALUES
	('Email Footers', 'Email Footers', 0)
GO


if exists (SELECT * FROM filetypegroup WHERE heading = 'Email Footers')  

DECLARE
@UseFileGroupID	INT

SELECT @UseFileGroupID = filetypegroupid FROM filetypegroup WHERE heading = 'Email Footers'

	if not exists (SELECT * FROM filetype WHERE filetypegroupid = @UseFileGroupID)

		if not exists (select * from information_schema.columns where table_name='filetype' and column_name='DefaultDeliveryMethod')  
		
			INSERT INTO filetype
			(filetypegroupid, [type], [path], secure, autounzip)
			VALUES
			(@UseFileGroupID, 'Images', 'emailfooters/images', 0, 0)
		GO

		if exists (select * from information_schema.columns where table_name='filetype' and column_name='DefaultDeliveryMethod')  
		
			DECLARE
			@UseFileGroupID	INT

			SELECT @UseFileGroupID = filetypegroupid FROM filetypegroup WHERE heading = 'Email Footers'
		
			INSERT INTO filetype
			(filetypegroupid, [type], [path], secure, autounzip, DefaultDeliveryMethod)
			VALUES
			(@UseFileGroupID, 'Images', 'emailfooters/images', 0, 0, 'Local')
		GO

	GO
GO

