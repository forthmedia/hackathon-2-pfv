/* 17/02/2016 DAN PROD2015-503 - add email templates for person certification expiry notice and module expiry notice */

if not exists(select 1 from emailDef where emailTextID = 'PersonCertificationExpiryNotice')
begin
	declare
	@emailTextID nvarchar(max),
	@emailName nvarchar(max),
	@subject nvarchar(max),
	@body nvarchar(max),
	@addresstype varchar(100),
	@toAddress varchar(100),
	@ccAddress varchar(100),
	@moduletextID varchar(100),
	@emailDefID int;
	
	set @emailTextID = 'PersonCertificationExpiryNotice'
	set @emailName = 'Person Certification Expiry Notice'
	set @subject = N'[[certificationTitle]] certification - Expires in [[noticePeriod]] Days'
	set @body = N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title>Person Certification Expiry Notice</title></head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;    border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */    background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */   background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */    width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
    <tbody>
        <tr>
            <td>phr_email_header
            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
                        <h2>Dear [[firstname]] [[lastname]],</h2>
                        <h3><span style="color:#1189ca;">Person Certification Expiry Notice</span></h3>
                        <p style="line-height: 20px">Your&nbsp;[[certificationTitle]]&nbsp;certification&nbsp;will expire in&nbsp;[[noticePeriod]]&nbsp;days. Please upgrade your certification.</p>
                        <p>Kind regards,<br />
                        <span class="sig">[[getSiteName()]]</span></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            phr_email_footer</td>
        </tr>
    </tbody>
</table>
</body>
</html>'
	set @addresstype = 'personid'
	set @toAddress = ''
	set @ccAddress = ''
	set @moduletextID = 'eLearning'
	
	INSERT INTO emaildef (emailtextID,name,addresstype,toAddress,ccAddress,created,createdby,fromaddress,active,subject_defaulttranslation,body_defaulttranslation,sendmessage,moduletextID) values(@emailTextID,@emailName,@addresstype,@toAddress,@ccAddress,getdate(),100,'#application.emailFrom#@#application.mailfromdomain#',1,@subject,@body,0,@moduletextID);
	
	select @emailDefID = emailDefID from emaildef where emailtextID = @emailTextID
	exec addNewPhraseAndTranslation @phraseTextID = 'Subject', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @subject, @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'Body', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @body, @updateMode = 0
	
end
GO

if not exists(select 1 from emailDef where emailTextID = 'PersonCertificationExpired')
begin
    declare
    @emailTextID nvarchar(max),
    @emailName nvarchar(max),
    @subject nvarchar(max),
    @body nvarchar(max),
    @addresstype varchar(100),
    @toAddress varchar(100),
    @ccAddress varchar(100),
    @moduletextID varchar(100),
    @emailDefID int;
    
    set @emailTextID = 'PersonCertificationExpired'
    set @emailName = 'Person Certification Expired'
    set @subject = N'[[certificationTitle]] certification - Expired'
    set @body = N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title>Person Certification Expired</title></head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;    border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */    background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */   background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */    width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
    <tbody>
        <tr>
            <td>phr_email_header
            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
                        <h2>Dear [[firstname]] [[lastname]],</h2>
                        <h3><span style="color:#1189ca;">Person Certification Expired</span></h3>
                        <p style="line-height: 20px">Your&nbsp;[[certificationTitle]]&nbsp;certification&nbsp;has expired.</p>
                        <p>Kind regards,<br />
                        <span class="sig">[[getSiteName()]]</span></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            phr_email_footer</td>
        </tr>
    </tbody>
</table>
</body>
</html>'     
    set @addresstype = 'personid'
    set @toAddress = ''
    set @ccAddress = ''
    set @moduletextID = 'eLearning'
    
    INSERT INTO emaildef (emailtextID,name,addresstype,toAddress,ccAddress,created,createdby,fromaddress,active,subject_defaulttranslation,body_defaulttranslation,sendmessage,moduletextID) values(@emailTextID,@emailName,@addresstype,@toAddress,@ccAddress,getdate(),100,'#application.emailFrom#@#application.mailfromdomain#',1,@subject,@body,0,@moduletextID);
    
    select @emailDefID = emailDefID from emaildef where emailtextID = @emailTextID
    exec addNewPhraseAndTranslation @phraseTextID = 'Subject', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @subject, @updateMode = 0
    exec addNewPhraseAndTranslation @phraseTextID = 'Body', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @body, @updateMode = 0
    
end
GO

if not exists(select 1 from emailDef where emailTextID = 'ModuleExpiryNotice')
begin
	declare
	@emailTextID nvarchar(max),
	@emailName nvarchar(max),
	@subject nvarchar(max),
	@body nvarchar(max),
	@addresstype varchar(100),
	@toAddress varchar(100),
	@ccAddress varchar(100),
	@moduletextID varchar(100),
	@emailDefID int;
	
	set @emailTextID = 'ModuleExpiryNotice'
	set @emailName = 'Module Expiry Notice'
	set @subject = N'Module [[title]] will expire in [[noticePeriod]] days'
	set @body = N'<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head><title>Module Expiry Notice</title></head>
<body style="background:#f7f3f3;">
<style type="text/css">body { background:#f7f3f3;}
table.main{ background:#fff; -webkit-box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 2px 10px rgba(51, 51, 51, 0.5); box-shadow:0 -5px 5px -5px rgba(0, 0, 0, 0.3); width: 600px; align: center;    border:2px solid #ffffff; border-collapse: collapse; }
table.gradientHead th { background: #0177b7; /* Old browsers */ background: -moz-linear-gradient(top,  #0177b7 0%, #0689d0 100%); /* FF3.6+ */  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#0177b7), color-stop(100%,#0689d0)); /* Chrome,Safari4+ */    background: -webkit-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Chrome10+,Safari5.1+ */  background: -o-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* Opera 11.10+ */   background: -ms-linear-gradient(top,  #0177b7 0%,#0689d0 100%); /* IE10+ */ background: linear-gradient(to bottom,  #0177b7 0%,#0689d0 100%); /* W3C */ filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#0177b7'', endColorstr=''#0689d0'',GradientType=0 ); /* IE6-9 */    width: 600px; height:20px;}
table.header{ padding-bottom:5px; width: 600px; }
table td {padding:0; margin:0;}

table#colTable td { padding:10px 0;line-height: 20px;}
p{ line-height: 30px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
p.sig{ padding-bottom: 15px; }
p.footer{ }
td{ font-family: Arial, Helvetica, sans-serif; font-size: 14px; }
td.lColumn{ width: 30%; text-align: right; vertical-align: top; }
td.rColumn{ width: 70%; max-width: 400px; text-align: left; text-indent: 10px; vertical-align: top; }
#footerContainer { text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff; }
#footerContainer p{ line-height:16px; font-size:11px;}
</style>

<table align="center" class="main">
    <tbody>
        <tr>
            <td>phr_email_header
            <table style="width:100%;">
                <tbody>
                    <tr>
                        <td colspan="2" style="padding-left:20px; padding-right:20px; padding-top:10px;">
                        <h2>Dear LMS Admin,</h2>
                        <h3><span style="color:#1189ca;">Module Expiry Notice</span></h3>
                        <p style="line-height: 20px">Module&nbsp;[[title]]&nbsp;is due to expire in&nbsp;[[noticePeriod]]&nbsp;days. Please update the module in order for it to remain active.</p>
                        <p>Kind regards,<br />
                        <span class="sig">[[getSiteName()]]</span></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            phr_email_footer</td>
        </tr>
    </tbody>
</table>
</body>
</html>'
	set @addresstype = 'literal'
	set @toAddress = 'LMSAdmin'
	set @ccAddress = ''
	set @moduletextID = 'eLearning'
	
	INSERT INTO emaildef (emailtextID,name,addresstype,toAddress,ccAddress,created,createdby,fromaddress,active,subject_defaulttranslation,body_defaulttranslation,sendmessage,moduletextID) values(@emailTextID,@emailName,@addresstype,@toAddress,@ccAddress,getdate(),100,'#application.emailFrom#@#application.mailfromdomain#',1,@subject,@body,0,@moduletextID);
	
	select @emailDefID = emailDefID from emaildef where emailtextID = @emailTextID
	exec addNewPhraseAndTranslation @phraseTextID = 'Subject', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @subject, @updateMode = 0
	exec addNewPhraseAndTranslation @phraseTextID = 'Body', @entitytypeid = 200, @entityid=@emailDefID, @phraseText= @body, @updateMode = 0
	
end
GO


/* show expired and expiry dates in conditional emails for trngPersonCertification */ 
update modEntityDef set showInConditionalEmails=1 where TableName = 'trngPersonCertification' and FieldName in ('expiredDate','expiryDate');
