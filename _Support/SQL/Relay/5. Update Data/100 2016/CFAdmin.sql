/*  
	WAB 2016-01-12
	add standard CFAdmin passwords to db 
	very slightly obfuscated
*/

declare 
	@livePW binary (10)  = 0x313973417475524E3236,
	@devPW binary (10) = 0x383773417475524E3236

insert into settings (name, value, testsite)
select newsettings.name, convert (varchar,newsettings.value), newsettings.testsite
from
(values 
		('relaywareinc.cfadminpassword', @livePW , 0),
		('relaywareinc.cfadminpassword', @livePW , 1),
		('relaywareinc.cfadminpassword', @devPW, 2),		
		('relaywareinc.cfadminpassword', @devPW, 3)				
) as newSettings (name, value, testsite)		
 left join
settings  on newsettings.name = settings.name and newsettings.testsite = settings.testsite
where settings.name is null

