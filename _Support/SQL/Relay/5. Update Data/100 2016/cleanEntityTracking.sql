/*  NJH 2016/01/19 JIRA BF-32 update the entityTracking table; then delete any unused entityTrackingUrls 
	WAB 2016-04-06 rewritten to be more performant (and to be relatively quick when run for a second time 
*/


/*	@unwantedURLVarsRexExp is the same regular expression as is used in procedure entityTracking_removeUnwantedUrlVariables 
	I am going to check that the the regexp has not been changed in the sp, and throw and error if it has
*/
declare  @unwantedURLVarsRexExp nvarchar(max) = 'rwsessionToken|formState|_cf_noDebug|spl|courseSortOrder|moduleSortOrder|jsessionid|flush|returnURL|urlrequested|debug|p|pid|es|pp|requestDump|oo'

IF NOT EXISTS (select * from syscomments where id = object_id ('entityTracking_removeUnwantedUrlVariables') and text like '%' + @unwantedURLVarsRexExp+ '%')
BEGIN
	RAISERROR('The value of @unwantedURLVarsRexExp is not the same as in sp entityTracking_removeUnwantedUrlVariables',16,16) 
END

/* Get all the URLs which need to be updated, and what they need to */
select 
	entityTrackingURLID, url, dbo.entityTracking_removeUnwantedUrlVariables(url) as updatedURL
into 
	#entityTrackingURLIDsToUpdate
from 
	entityTrackingURL
where 
	dbo.regExIsMatch ('(?<=(\?|&))('+@unwantedURLVarsRexExp+')=.*?(&|$)', URL, 1) <> 0


/* Add any newly generated URLs to the entityTrackingURL table */
insert into 
	entityTrackingURL (url)
select 
	distinct updatedURL 
from
	#entityTrackingURLIDsToUpdate oldIDs 
		left join
	entityTrackingURL  newIDs on newIDs.url = oldIDS.updatedurl 
where 
	newIDs.entityTrackingURLID is null

/* update toURLID and fromURLID with new values */
print 'Updating EntityTrackingBase toURLID'
set rowcount  10000
set nocount on
declare @rows int = -1, @msg nvarchar(max), @totalrows int = 0, @rowsToUpdate int
while @rows <> 0
begin

	update entityTrackingBase 
		set ToUrlId = newIDs.entityTrackingURLID
	from 
		entityTrackingBase etb
		inner join
		#entityTrackingURLIDsToUpdate oldIDs on etb.ToUrlId = oldIDs.entityTrackingURLID
		inner join
		entityTrackingURL  newIDs on newIDs.url = oldIDS.updatedurl 
		
	set @rows = @@rowcount
	set @totalrows += @rows
	set @msg = convert(varchar,@totalrows) + ' rows done'
	
	RAISERROR(@msg ,0,1) WITH NOWAIT	
end


set @rows = -1
set @totalrows = 0 
print 'Updating EntityTrackingBase fromURLID'
while @rows <> 0
begin

	update entityTrackingBase 
		set fromUrlId = newIDs.entityTrackingURLID
	from 
		entityTrackingBase etb
		inner join
		#entityTrackingURLIDsToUpdate oldIDs on etb.fromUrlId = oldIDs.entityTrackingURLID
		inner join
		entityTrackingURL  newIDs on newIDs.url = oldIDS.updatedurl 
		
	set @rows = @@rowcount
	set @totalrows += @rows
	set @msg = convert(varchar,@totalrows) + ' rows done'
	
	RAISERROR(@msg ,0,1) WITH NOWAIT	
end
set rowcount  0


/* Now remove any URLs which are no longer in use */
select 
	entityTrackingURLID 
into #entityTrackingURLIDsToDelete
from 
	entityTrackingURL
		left join 
	entitytrackingBase etb_t on entityTrackingURLID = etb_t.tourlid
		left join 
	entitytrackingBase etb_f on entityTrackingURLID = etb_f.fromurlid	
where 	
	etb_t.tourlid is null and etb_f.fromurlid is null		


print convert (varchar,@@rowcount)+ ' unused items to delete from entityTrackingURL'
set @rows = -1
set @totalrows = 0 

while @rows <> 0
begin
set rowcount  1000
	delete 
		entityTrackingURL
	from 
		entityTrackingURL etu
		inner join
		#entityTrackingURLIDsToDelete	etud on etu.entityTrackingURLID = etud.entityTrackingURLID 
		

	set @rows = @@rowcount
	set @totalrows += @rows
	set @msg = convert(varchar,@totalrows) + ' rows done'
	
	RAISERROR(@msg ,0,1) WITH NOWAIT	
end

set rowcount  0
set nocount off

drop table #entityTrackingURLIDsToUpdate
drop table #entityTrackingURLIDsToDelete