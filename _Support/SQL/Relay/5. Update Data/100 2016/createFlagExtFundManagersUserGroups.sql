-- atrunov/SoftServe 11/10/2016
-- Flag creation regarding ticket https://relayware.atlassian.net/browse/SSPS-48, this changes should go to Core product
DECLARE @FlagGroupID int
DECLARE @FlagID int
DECLARE @FlagTypeID int
DECLARE @EntityTypeID int
DECLARE @FlagGroupTextID varchar(MAX)
DECLARE @FlagGroupNamePhraseTextID varchar(MAX)
DECLARE @FlagtextID varchar(MAX)
DECLARE @FlagNamePhraseTextID varchar(MAX)


SET @EntityTypeID = (SELECT entitytypeid FROM schemaTable where entityName = 'fundCompanyAccount')
SET @FlagTypeID = (SELECT FlagTypeID FROM FlagType WHERE Name = 'IntegerMultiple')
SET @FlagGroupTextID = 'ExtFundManagers'
SET @FlagGroupNamePhraseTextID = 'flagGroup_' + @FlagGroupTextID
SET @FlagTextID = 'ExtFundManagersUserGroups'
SET @FlagNamePhraseTextID = 'flag_' + @FlagTextID


if not exists (select * from FlagGroup fg where fg.FlagGroupTextID = @FlagGroupTextID and fg.FlagTypeID = @FlagTypeID)
BEGIN
	INSERT [dbo].[FlagGroup] ([ParentFlagGroupID], [FlagTypeID], [EntityTypeID], [Active], [FlagGroupTextID], [Name], [Description], [NamePhraseTextID], [DescriptionPhraseTextID], [Notes], [OrderingIndex], [Expiry], [Scope], [Viewing], [Edit], [Search], [Download], [ViewingAccessRights], [EditAccessRights], [SearchAccessRights], [DownloadAccessRights], [PublicAccess], [PartnerAccess], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [UseInSearchScreen], [FlagRegionEntity], [EntityMemberID], [EntityMemberLive], [formattingParameters], [helpText], [FlagTypeCurrency], [FlagTypeDecimalPlaces],createdByPerson,lastUpdatedByPerson)
	VALUES (0, @FlagTypeID, @EntityTypeID, 1, @FlagGroupTextID, @FlagGroupTextID, N'', @FlagGroupNamePhraseTextID, NULL, N'', 263, CAST(0x0000AC4D00000000 AS DateTime), 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 601, CAST(0x0000A69800789AD8 AS DateTime), 601, CAST(0x0000A6980078FDA9 AS DateTime), 0, NULL, 0, 0, N'', N'', NULL, NULL,0,0)
END

if not exists (select * from Flag f where f.FlagTextID = @FlagTextID)
BEGIN
	SET @FlagGroupID = (SELECT FlagGroupID FROM FlagGroup WHERE FlagGroupTextID = @FlagGroupTextID)
	SET @FlagID = (SELECT MAX(FlagID) FROM Flag) + 1

    INSERT [dbo].[flag] ([FlagGroupID], [Name], [Description], [OrderingIndex], [Active], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [FlagTextID], [NamePhraseTextID], [DescriptionPhraseTextID], [useValidValues], [lookup], [Score], [Protected], [subEntityTypeID], [WDDXStruct], [UseInSearchScreen], [FlagRegionEntity], [HasMultipleInstances], [CurrentCount], [currentCountDate], [FlagRegionEntityTypeId], [Expression], [LinksToEntityTypeID], [formattingParameters], [IsSystem], [showInConditionalEmails], [helpText],createdByPerson,lastUpdatedByPerson)
	VALUES (@FlagGroupID, @FlagTextID, N'', 1, 1, 601, CAST(0x0000A6980078FDA9 AS DateTime), 601, CAST(0x0000A6980078FDA9 AS DateTime), @FlagTextID, @FlagNamePhraseTextID, NULL, 0, 0, CAST(0.000 AS Numeric(9, 3)), 0, NULL, N'', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, N'', 0, 0, N'',0,0)
END