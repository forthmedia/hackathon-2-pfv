/*
Script for migrating Country.R1CountryID to the CountryAlternatives.
RMP 2017-02-14  PROD2016-2981 Connector: Country Name Variations in Accordance to the Country Alternatives Table.
				As the r1countryId field is used for the OOTB mappings and that is effectively being removed, we will want to write
				a release script to populate the countryAlternatives table with any r1CountryID value that does not already yet exist
				in the countryDescription , isocode or localCountryDescription fields (and of course that is not in the countryAlternatives table either).
*/


begin transaction

insert into countryAlternatives(CountryID,alternativeCountryDescription,created,lastupdated,createdByPerson,lastUpdatedByPerson)
select Country.CountryID as CountryID, Country.R1CountryID as alternativeCountryDescription, 0, 0, 0, 0
from Country
left join countryAlternatives on Country.CountryID = countryAlternatives.countryID and Country.R1CountryID = countryAlternatives.alternativeCountryDescription
where R1CountryID <> CountryDescription
and R1CountryID <> LocalCountryDescription
and R1CountryID <> ISOCode
and ISOCode is not null
and R1CountryID is not null
and countryAlternatives.countryID is null

commit transaction