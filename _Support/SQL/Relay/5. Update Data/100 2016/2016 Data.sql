

/* NJH 2015/11/09 move generic settings out of salesforce specific settings */
update settings set name=replace(name,'salesforce.','') where name in ('CONNECTOR.SALESFORCE.ADMINEMAIL','CONNECTOR.SALESFORCE.ALLOWDATATRUNCATION','CONNECTOR.SALESFORCE.MAXSYNCHATTEMPTS','CONNECTOR.SALESFORCE.DEBUGLEVEL','CONNECTOR.SALESFORCE.SCHEDULEDTASK.PAUSE','CONNECTOR.SALESFORCE.SCHEDULEDTASK.WAITINMINUTESBEFORENEXTRUN')

/* Domain matching flag */
declare @flagGroupTextID varchar(50) = 'AuthorizedDomain'
declare @flagGroupPhraseTextID varchar(50) = 'flagGroup_AuthorizedDomain'
declare @flagTextID varchar(50) = 'AuthorizedDomain'
declare @flagPhraseTextID varchar(50) = 'flag_AuthorizedDomain'

if not exists(select 1 from flaggroup where flagGroupTextID=@flagGroupTextID)
begin
	insert into flaggroup (flagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,NamePhraseTextID,OrderingIndex,viewing,edit,search,download,viewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,CreatedBy,Created,LastUpdatedBy,LastUpdated,scope,entitymemberID,parentFlagGroupID,expiry,createdByPerson,lastUpdatedByPerson) values (5,1,1,@flagGroupTextID,'Authorized Domain',@flagGroupPhraseTextID,150,1,1,0,1,0,0,0,0,404,getdate(),404,getdate(),0,0,0,'2100-01-01 00:00:00.000',0,0)
	exec addNewPhraseAndTranslation @phraseTextId=@flagGroupPhraseTextID,@phraseText='Authorized Domain'
end

if not exists(select 1 from flag where flagTextID=@flagTextID)
begin
	insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,helpText,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Authorized Domain',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,'Enter a comma-delimited list of domains to associate with this account.',0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
	exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Authorized Domain'
end
GO


/* NJH 2016/01/07 - inactivate the countryID field mapping on the product table BF-144 */
update connectorMapping set active=0 where ID in (select ID from vConnectorMapping where column_relayware='countryID' and object_relayware='product') and active!=0

/* NJH 2016/01/18 - rename the flagGroupTextIDs for the delete flaggroups as they happen to be the same as the flagTextIDs, which then causes problems in upsertEntity */
update flagGroup set flagGroupTextID=replace(flagGroupTextID,'delete','delete_')
where flagGroupID in (select flagGroupId from flag where flagTextID in ('deletePerson','deleteLocation','deleteOrganisation'))
and flagGroupTextID not like 'delete[_]%'


/* NJH 2016/01/25 add currency mapping for pricebook as it's now needed */
if not exists (select 1 from vConnectorMapping where object_relayware='pricebook' and column_relayware='currency')
insert into connectorMapping ([connectorObjectID_relayware], [connectorObjectID_remote],[column_relayware],[column_remote],mappingType,direction,required,[mappedRelaywareColumn],isRemoteID,active,canEdit,emptyStringAsNull,lastUpdatedByPerson,createdBy,lastUpdatedBy)	
select rwo.ID as [connectorObjectID_relayware],o.ID as [connectorObjectID_remote],'currency' as [column_relayware],'' as [column_remote],'Field' as mappingType,'I' as direction,'0' as required,null as [mappedRelaywareColumn],0 as isRemoteID,1 as active,1 as canEdit,0 as emptyStringAsNull,2 as lastUpdatedByPerson,101 as createdBy,101 as lastUpdatedBy
	from connectorObject o   join connectorObject rwo  on o.relayware=0 and o.object = 'pricebook2'and rwo.relayware=1 and rwo.object = 'pricebook' and rwo.connectorType = 'Salesforce' and o.connectorType = 'Salesforce'
	
/* NJH 2016/01/27 add productOrGroupID as well */
if not exists (select 1 from vConnectorMapping where object_relayware='opportunityProduct' and column_relayware='productOrGroupID')
insert into connectorMapping ([connectorObjectID_relayware], [connectorObjectID_remote],[column_relayware],[column_remote],mappingType,direction,required,[mappedRelaywareColumn],isRemoteID,active,canEdit,emptyStringAsNull,lastUpdatedByPerson,createdBy,lastUpdatedBy)	
select rwo.ID as [connectorObjectID_relayware],o.ID as [connectorObjectID_remote],'productOrGroupID' as [column_relayware],'' as [column_remote],'Field' as mappingType,'I' as direction,'0' as required,null as [mappedRelaywareColumn],0 as isRemoteID,1 as active,1 as canEdit,0 as emptyStringAsNull,2 as lastUpdatedByPerson,101 as createdBy,101 as lastUpdatedBy
	from connectorObject o   join connectorObject rwo  on o.relayware=0 and o.object = 'opportunityLineItem'and rwo.relayware=1 and rwo.object = 'opportunityProduct' and rwo.connectorType = 'Salesforce' and o.connectorType = 'Salesforce'

update connectorCustomMapping set columnName='productOrGroupId' where columnName='productId' and entityname='opportunityProduct'

/* NJH 2016/02/03 dealing with HQ/org problems - check the parent.organisationID first as the location may have changed organisation */
update connectorCustomMapping set selectStatement = 'isNull(isNull(isNull(parent.organisationID,HQLocation.organisationID),salesforce_account.OrganisationID),location.organisationID)' where selectStatement='isNull(isNull(isNull(HQLocation.organisationID,parent.organisationID),salesforce_account.OrganisationID),location.organisationID)'

/*	WAB 2016-02-08 BF-409 get rid of email addresses which contain a single space due to a dodgy default
	Should have done update at same point as updating the default, but due to dodgy trigger can only do it after trigger has been fixed
*/
	update person set email = '' where email = ' '

/* JIRA PROD2015-566 NJH 2016/02/10  - moved campaign setting */
update settings set name='campaigns.displayCampaigns' where name='actions.displayCampaign'


/* NJH 2016/02/27 insert authorised domain as screenitem */
declare @screenId int
select @screenId = screenID from screens where screentextID = 'LocProfileSummary'

if @screenId is not null and not exists(select 1 from screenDefinition where screenId=@screenId and fieldTextID='AuthorizedDomain')
Begin
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,breakln,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenId,30,'Flag','AuthorizedDomain','','edit',1,1,'','',101,getdate())
END

/*RJT 2016/02/22 Correction of phrase text ID misspelling in existing phrases; enityTypeScreen to entityTypeScreen. The joining back onto itself is to avoid issues where the entity version already exists */
update phraseList1 set PhraseTextID=REPLACE(phraseList1.PhraseTextID,'enityTypeScreen','entityTypeScreen') 
from PhraseList phraseList1
left join PhraseList phraseList2 on phraseList2.PhraseTextID=REPLACE(phraseList1.PhraseTextID,'enityTypeScreen','entityTypeScreen') and phraseList2.PhraseTextID like '%entityTypeScreen%'
where phraseList1.PhraseTextID like '%enityTypeScreen%' and phraseList2.PhraseTextID is null;

/* NJH 2016/03/06 JIRA PROD2016-472 - synch attachments */
update schemaTableBase set nameExpression='fileName' where entityname='RelatedFile' and isNull(nameExpression,'') !='fileName'
update connectorObject set crmKeyPrefix = '006' where object='opportunity' and crmKeyPrefix is null and relayware=0
update connectorObject set crmKeyPrefix = '001' where object='account' and crmKeyPrefix is null and relayware=0
update connectorObject set crmKeyPrefix = '003' where object='contact' and crmKeyPrefix is null and relayware=0
update connectorObject set crmKeyPrefix = '00k' where object='opportunityLineItem' and crmKeyPrefix is null and relayware=0
update connectorObject set crmKeyPrefix = '00Q' where object='lead' and crmKeyPrefix is null and relayware=0

/* WAB 2016-03-09 Screens Layout Issues */

/*
BF-545
The myProfile Page will not work in stacked layout if Social Media is switched on (because it includes a hard coded horizontal layout)
Therefore set to horizontal layout if social media switched on and divlayout not already set
*/
IF exists (select screenid from screendefinition where fieldtextid like '%linkedInProfile%')
BEGIN
	IF (dbo.getSettingValue ('SOCIALMEDIA.ENABLESOCIALMEDIA')  is null OR dbo.getSettingValue ('SOCIALMEDIA.ENABLESOCIALMEDIA') =1 )
		AND 
		not exists (select screenid from screens where screentextID= 'myprofile' and parameters like '%divLayout%')
	BEGIN
		update 
			screens 
		set 
			parameters = isNull(parameters,'') + case when isNull(parameters,'') <> '' then ',' else '' end + 'divlayout=horizontal' 
		where 
			screenid in (select screenid from screendefinition where fieldtextid like '%linkedInProfile%')
			and parameters not like '%divlayout%'
	
	END	
			
END	

/*
BF-534  
Manage Colleagues Page can include any screens and need to be divlayout=horizontal
Get ScreenIDs from the RELAY_PEOPLELIST tag in phrases table using a various convoluted functions! 
*/

update 
	screens 
set 
	parameters = isNull(parameters,'') + case when isNull(parameters,'') <> '' then ',' else '' end + 'divlayout=horizontal'
from	
	screens 
		inner join
	dbo.csvToTable (
		(select 
		dbo.concatenate(rtrim(replace(replace(dbo.regexMatch ('screenids=(.*?)[ >]',phraseText,1),'screenids=',''),'>','')) + ',')
		from phrases where contains (phraseText,'"*RELAY_PEOPLELIST*"')
		and phraseText like '%screenid%')
	) as Screenids  on (screenids.value = screens.screenTextID OR screenids.value = convert(varchar,screens.screenID))
where parameters not like '%divlayout%'

/* set flagsExist to 1 for all entities in connectorObject */
update schemaTableBase set flagsExist=1 where entityName in (select object from connectorObject where relayware=1) and flagsExist=0
/* set in_vEntityName to 1 for all entities in connectorObject */
update schemaTableBase set in_vEntityName=1 where entityName in (select object from connectorObject where relayware=1) and isNull(in_vEntityName,0)=0
/* JIRA 2016/02/22 - add table to hold relationship between relay modules and entityTypes */
declare @textTable table (moduleTextID nvarchar(max),entityType nvarchar(max))

insert into @textTable (moduleTextID,entityType) values 
 ('Files','files')
 ,('Content','element')
 ,('Content','siteDefDomain')
,('Events','EventDetail')
,('Products','Product')
,('Products','ProductGroup')
,('Discussion','discussionMessage')
,('Discussion','discussionGroup')
,('LeadManager','lead')
,('LeadManager','opportunity')
,('LeadManager','opportunityProduct')
,('LeadManagerNoProducts','Opportunity')
,('Funds','fundCompanyAccount')
,('Funds','fundRequestActivity')
,('Funds','FundRequest')
,('Communicate','Commdetail')
,('Communicate','Communication')
,('LeadManager','businessPlan')
,('eLearning','specialisation')
,('eLearning','trngModule')
,('eLearning','trngCertification')
,('Incentive','RWPersonAccount')
,('Incentive','RWTransaction')
,('Incentive','RWPersonAccount')
,('TrainingManager','QuizTaken')

insert into relayModuleEntityType (relayModuleID,entityTypeID) 
select distinct rm.moduleID,s.entityTypeID
from @textTable t inner join 
	relayModule rm on t.moduleTextID=rm.moduleTextID inner join
	schemaTable s on s.entityName = t.entityType left join
	relayModuleEntityType rme on rme.relayModuleID = rm.moduleId and rme.entityTypeID = s.entitytypeID
where
	rme.ID is null


	

IF NOT EXISTS (Select * from [dbo].[SecurityType]  where ShortName='scoreTask') 
Begin
	
	Declare @scoreTaskNewSecurityTypeID	int;
	
	select @scoreTaskNewSecurityTypeID= max(SecurityTypeID)+1 from SecurityType	
	
	SET IDENTITY_INSERT [dbo].[SecurityType] ON	
	INSERT INTO [dbo].[SecurityType] ([SecurityTypeID], [ShortName], [Description], [CountrySpecific], [relayWareModule]) VALUES (@scoreTaskNewSecurityTypeID, 'scoreTask', 'Control of Activity Scoring', 0, N'relayWare')
	SET IDENTITY_INSERT [dbo].[SecurityType] OFF

	INSERT INTO [dbo].[Rights] ([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated]) VALUES (4, @scoreTaskNewSecurityTypeID, 0, 0, 7, 0, '2009-03-27 17:09:39.343', 0, '2009-03-27 17:09:39.343')

end



IF NOT EXISTS (Select * from [dbo].[Screens]  where screentextID='personScoreReport') 
Begin
	select @screenID = max(screenID) + 1 from screens
	insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
		values(@screenID,'personScoreReport','Person Score Report',1,1,'person',(select entityTypeID from schemaTable where entityname='person'),0,0,'/score/screens/personScoreReport.cfm','edit',101,getdate())
End

IF NOT EXISTS (Select * from [dbo].[Screens]  where screentextID='locationScoreReport') 
Begin
	select @screenID = max(screenID) + 1 from screens
	insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
		values(@screenID,'locationScoreReport','Location Score Report',1,1,'location',(select entityTypeID from schemaTable where entityname='location'),0,0,'/score/screens/locationScoreReport.cfm','edit',101,getdate())
End

IF NOT EXISTS (Select * from [dbo].[Screens]  where screentextID='organisationScoreReport') 
Begin
	select @screenID = max(screenID) + 1 from screens
	insert into screens (screenID,screentextID, screenname, active,suite,entitytype,entitytypeID,sortorder,scope,viewer,viewedit,createdby,created)
		values(@screenID,'organisationScoreReport','Organization Score Report',1,1,'organisation',(select entityTypeID from schemaTable where entityname='organisation'),0,0,'/score/screens/organisationScoreReport.cfm','edit',101,getdate())
End

update ActivityType set availableForScoring = 0 where ActivityType='PagesVisited' --this is an agregate type and does not support scoring (as its constantbly being updated rather than being transactional)
update ActivityType set availableForScoring = 0 where ActivityType='PortalVisited' --The unique key for the siteDefDomain entity doesn't conform to standards (i.e. isn't numeric) so breaks the system
update ActivityType set availableForScoring = 0 where ActivityType='LoggedIn' --The unique key for the siteDefDomain entity doesn't conform to standards (i.e. isn't numeric) so breaks the system


--update that these activities are only available on the org level
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='FundRequestApproved'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='FundRequestRejected'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='FundRequestSubmitted'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='OrganisationApproved'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='OrganisationRejected'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='SpecialisationAwarded'
update ActivityType set minimumEntityTypeIDWithData = 2 where activityType='SpecialisationExpired'

/* JIRA PROD2016-817 NJH 2016/05/19 */
update emailDef set name='Certification Expired' where emailTextID='TrngCertificationExpired' and name='Accreditation Expired'

/*JIRA PROD2016-350 - NJH 2016/05/19 renamed setting*/
update settings set name='plo.useLocationAsPrimaryPartnerAccount' where name='plo.useOrganizationAsParentAccountModel'


--Single sign on control override
if not exists (select 1 from Flag where FlagTextID='AllowNonSSOLogin')
begin
	INSERT INTO [dbo].[FlagGroup] ( [ParentFlagGroupID], [FlagTypeID], [EntityTypeID], [Active], [FlagGroupTextID], [Name], [Description], [NamePhraseTextID], [DescriptionPhraseTextID], [Notes], [OrderingIndex], [Expiry], [Scope], [Viewing], [Edit], [Search], [Download], [ViewingAccessRights], [EditAccessRights], [SearchAccessRights], [DownloadAccessRights], [PublicAccess], [PartnerAccess], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [UseInSearchScreen], [FlagRegionEntity], [EntityMemberID], [EntityMemberLive], [formattingParameters], [helpText], [FlagTypeCurrency], [FlagTypeDecimalPlaces],createdByPerson,lastUpdatedByPerson) VALUES (0, 2, 2, 1, 'SingleSignOnOverrides', N'Single Sign On Overrides', N'', NULL, NULL, N'', 1, '2100-01-01 00:00:00.000', 0, 1, 1, 1, 1, 0, 0, 0, 0, NULL, NULL, 102, '2002-12-05 15:36:30.000', 438, '2008-07-01 16:43:25.000', 0, NULL, 0, 0, '', NULL, NULL, NULL,0,0)
	
	Declare @SingleSignOnOverridesFlagGroupID int;
	set @SingleSignOnOverridesFlagGroupID=(select FlagGroupID from FlagGroup where flagGroupTextID='SingleSignOnOverrides');
	
	INSERT INTO [dbo].[flag] ( [FlagGroupID], [Name], [Description], [OrderingIndex], [Active], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [FlagTextID], [NamePhraseTextID], [DescriptionPhraseTextID], [useValidValues], [lookup], [Score], [Protected], [subEntityTypeID], [WDDXStruct], [UseInSearchScreen], [FlagRegionEntity], [HasMultipleInstances], [CurrentCount], [currentCountDate], [FlagRegionEntityTypeId], [Expression], [LinksToEntityTypeID], [formattingParameters], [IsSystem], [showInConditionalEmails], [helpText],createdByPerson,lastUpdatedByPerson) VALUES (@SingleSignOnOverridesFlagGroupID, N'Allow Relayware password based login even when site forbids password login', '', 1, 1, 102, '2002-12-05 15:36:54.000', 438, '2008-07-01 16:43:25.000', 'AllowNonSSOLogin', 'flag_AllowNonSSOLogin', NULL, 0, 0, 0.000, 0, NULL, '', 0, NULL, 0, 34, '2014-06-24 15:35:07.070', NULL, NULL, NULL, N'', 1, 0, NULL,0,0)

	Declare @AllowNonSSOLoginsFlagID int;
	set @AllowNonSSOLoginsFlagID=(select FlagID from Flag where flagTextID='AllowNonSSOLogin');
	
	INSERT INTO [dbo].[BooleanFlagData] ([EntityID], [FlagID], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [lastUpdatedByPerson]) VALUES (1, @AllowNonSSOLoginsFlagID, 100, '2001-01-21 14:31:40.000', 100, '2001-01-21 14:31:40.000', NULL)
	
end

/* NJH 2016/09/21 JIRA PROD-379 */
update emailDef set toAddress = replace(toAddress,'#mergeStruct.businessplanID#','#businessPlan.businessPlanID#') where toAddress like '%#mergeStruct.businessplanID#%'
-- associate business plans with the lead manager module
update emailDef set moduleTextID='LeadManager' where len(isNull(moduleTextID,'')) = 0 and (name like 'BusinessPlan%' or name like 'Business Plan%')


/* PROD2016-1310 NJH - update the modRegister and schemaTable where connectorObject entitytypeID is 500 and not 508 */
if exists (select 1 from schemaTable where entityTypeID=500 and entityName='connectorObject')
begin
	update modRegister set entityTypeID=508 where entityTypeID=500
	update schemaTable set entityTypeID=508 where entityName='connectorObject'
end


/* NJH 2016/07/28 JIRA PROD2016-1504 - remove cookie.user from validvalues queries and replace with request.relayCurrentUser.userGroupID */
update validFieldValues set dataValue = replace(dataValue,'#listGetAt(cookie.USER, 2, ''-'')#','#request.relayCurrentUser.userGroupID#') where dataValue like '%#listGetAt(cookie.USER, 2, ''-'')#%'

/* Build the security token used as an additional security measure with internal webservices */

if not exists(select 1 from settings where name = 'security.internalWebserviceToken')
BEGIN
	--create a secure random token, used for websevice call authentication
	declare @binaryAPIToken varbinary(max);
	declare @APIToken nVarChar(Max);
	set @binaryAPIToken=CRYPT_GEN_RANDOM(50);
	set @APIToken=master.dbo.fn_varbintohexstr (@binaryAPIToken)

	insert into settings(name,value)
	values('security.internalWebserviceToken',@APIToken)
END

/* NJH 2016/07/20 JIRA PROD2016-599 - create location level primary contact flag. Create valid values for it and add it to the locatin Profile Summary screen */
declare @flagGroupTextID varchar(50) = 'LocationPrimaryContacts';
declare @flagGroupPhraseTextID varchar(50) = 'flagGroup_'+@flagGroupTextID;
declare @flagTextID varchar(50) = 'PrimaryContacts';
declare @flagPhraseTextID varchar(50) = 'flag_'+@flagTextID;

if not exists(select 1 from flaggroup where flagGroupTextID=@flagGroupTextID)
begin
	insert into flaggroup (flagTypeID,EntityTypeID,Active,FlagGroupTextID,Name,NamePhraseTextID,OrderingIndex,viewing,edit,search,download,viewingAccessRights,EditAccessRights,SearchAccessRights,DownloadAccessRights,CreatedBy,Created,LastUpdatedBy,LastUpdated,scope,entitymemberID,parentFlagGroupID,expiry,createdByPerson,lastUpdatedByPerson) values (8,1,1,@flagGroupTextID,'Location Primary Contacts',@flagGroupPhraseTextID,0,1,1,0,1,0,0,0,0,404,getdate(),404,getdate(),0,0,0,'2100-01-01 00:00:00.000',0,0)
	exec addNewPhraseAndTranslation @phraseTextId=@flagGroupPhraseTextID,@phraseText='Location Primary Contacts'
end

if not exists(select 1 from flag where flagTextID=@flagTextID)
begin
	insert into flag (flagGroupID,Name,OrderingIndex,Active,FlagTextID,NamePhraseTextID,CreatedBy,Created,LastUpdatedBy,LastUpdated,isSystem,useValidValues,linksToEntityTypeID,createdByPerson,lastUpdatedByPerson) select flaggroupID,'Primary Contacts',1,1,@flagTextID,@flagPhraseTextID,404,getdate(),404,getdate(),1,1,0,0,0 from flaggroup where flagGroupTextID=@flagGroupTextID
	exec addNewPhraseAndTranslation @phraseTextId=@flagPhraseTextID,@phraseText='Primary Contacts'
end
GO

-- create the valid values for the location primary contact
if not exists(select 1 from validFieldValues where fieldname='flag.PrimaryContacts')
begin
	insert into validFieldValues (fieldname,countryID,validValue,defaultFieldCase,defaultFieldValue,sortOrder,dataValue,languageID)
	values
	('flag.PrimaryContacts',0,'*lookup','T',0,1,'SELECT personid as dataValue,fullname as displayValue 
	FROM person p 
	left join vBooleanFlagData b on p.personid=b.entityid and b.flagTextID=''perApproved''
	left join (integerMultipleFlagData i inner join flag f on i.flagID = f.flagID and f.flagTextID=''PrimaryContacts'') on i.data = p.personID and i.entityID=locationID
	where (b.entityId is not null
		or i.entityID is not null
	) and locationID=#entityID#
	group by personid, fullname
	order by fullname',1)
end

/* insert primary contacts as screenitem */
declare @screenId int
select @screenId = screenID from screens where screentextID = 'LocProfileSummary'

if @screenId is not null and not exists(select 1 from screenDefinition where screenId=@screenId and fieldTextID='PrimaryContacts')
Begin
	insert into screenDefinition (screenID, sortOrder, fieldSource, fieldTextID, fieldLabel, method, allcolumns,breakln,tdclass,specialformatting,lastupdatedby,lastupdated)
	values (@screenId,22,'Flag','PrimaryContacts','','edit',1,1,'','',101,getdate())
End

/* setting the primary contact user group to be based on location flag */
if exists(select 1 from information_schema.tables where table_name='rightsflaggroup')
begin
	declare @locPriContactFlagID int
	
	select @locPriContactFlagID = flagID from flag where flagTextID='PrimaryContacts'
	
	update rightsflaggroup set flagId = @locPriContactFlagID
	from rightsflaggroup r
		inner join flag f on f.flagID = r.flagID where f.flagTextID='KeyContactsPrimary'
end
GO

/* NJH 2016/09/06 JIRA PROD2016-1313 - renamed the org registration file to be more generic */
update processActions set jumpName=replace(jumpName,'RegistrationApprovalOrg.cfm','RegistrationApprovalAccount.cfm') where jumpName like '%RegistrationApprovalOrg.cfm%'
update screenDefinition set FieldTextID = replace(FieldTextID,'RegistrationApprovalOrg','RegistrationApprovalAccount') where FieldTextID like '%registrationApprovalOrg%'

/* NJH JIRA PROD2016-2383 - remove expiry dates */
update flagGroup set expiry=null where expiry is not null
GO


update Flag set Protected=0  where FlagTextID='HQ' and Protected=1 -- prevented deletions of organisations and no one could come up with a good reason why it should be protected RJT

/* NJH 2016/10/10 JIRA PROD2016-2489 move all org primary contacts to loction level for any users who are location level but still use org primary contacts
	WAB 2017-02-06  Upgrade issues because some of the accountManager personIDs no longer existed and there is now some referential integrity on the integerMultipleFlagData table
					So did a join to vEntityName to check
*/
declare @locPriContactflagId int
declare @orgPriContactFlagId int

select @locPriContactflagId = flagId from flag where flagTextID='primaryContacts'
select @orgPriContactflagId = flagId from flag where flagTextID='keyContactsPrimary'

if exists (select 1 from integerFlagData where flagID=@orgPriContactflagId) and not exists (select 1 from integerMultipleFlagData where flagID=@locPriContactflagId) and isNull(dbo.getSettingValue('plo.useLocationAsPrimaryPartnerAccount'),1) = 1
begin
      insert into integerMultipleFlagData (flagId,entityId,data,createdBy,created,lastupdatedBy,lastUpdated)
      select @locPriContactflagId,l.locationID,ifd.data,ifd.createdBy,ifd.created,ifd.lastupdatedBy,ifd.lastUpdated
      from
            integerFlagData ifd
                  inner join organisation o on o.organisationID = ifd.entityID
                  inner join location l on l.organisationID = o.organisationID
				  inner join ventityName personcheck on ifd.data = personcheck.entityid and entityTypeID = 0
            where ifd.flagID =@orgPriContactflagId
end


/* JIRA PROD2016-1303 - update flaggroup for any groups that contain actual 'bit' fields as based on connector mappings */
update flagGroup set formattingParameters =
	case
		when len(ltrim(rtrim(isNull(fg.formattingParameters,'')))) = 0 then 'isBitField=true'
		else fg.formattingParameters+',isBitField=true'
	end
from
flagGroup fg
	inner join flag f on fg.flagGroupID = f.flagGroupID
	inner join vConnectorMapping m on m.column_relayware = f.flagTextId and m.entityTypeID = fg.entityTypeID
where fg.flagTypeID in (2,3)
	and fg.formattingParameters not like '%isBitField=true%'


/* WAB 2016-10-31	PROD2016-2614 countryGroups should not have a self-referencing record in the countryGroup table, but these have been being added by the country insert trigger (now fixed)
					Remove any instances of countryGroups in their own countryGroup
*/
delete countryGroup
from
	countryGroup cg
		inner join
	country  c on cg.countryGroupID = c.countryid
where
		c.isocode is null	/* ie is a countryGroup not a country */
	and cg.countryGroupID = cg.countryMemberID


/* PROD2016-2389 Triggered Emails - Add Lead Entity into Triggered Emails */
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'created'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'leadTypeStatusID'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'progressID'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'lastUpdated'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'acceptedByPartnerDate'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'CountryID'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'leadTypeID'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'enquiryType'
update modEntityDef set showInConditionalEmails = 1 where TableName = 'lead' and FieldName = 'sourceID'

if exists (select 1 from information_schema.tables where table_name='assignedEntityIDs')
drop table assignedEntityIDs

/* NJH 2016/11/18 JIRA PROD2016-2218 - some work for both screensV2 and also lead screens */
/* Set the textID column for lookup tables */
exec setExtendedProperty @level1Object='table', @level1name='lookupList',@level2Object=null,@level2name=null,@property='textIDColumn',@value='lookupTextID'
exec setExtendedProperty @level1Object='table', @level1name='oppType',@level2Object=null,@level2name=null,@property='textIDColumn',@value='oppTypeTextID'
--exec setExtendedProperty @level1Object='table', @level1name='oppSource',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppStatus',@level2Object=null,@level2name=null,@property='textIDColumn',@value='statusTextID'
exec setExtendedProperty @level1Object='table', @level1name='oppStage',@level2Object=null,@level2name=null,@property='textIDColumn',@value='stageTextID'
--exec setExtendedProperty @level1Object='table', @level1name='oppReason',@level2Object=null,@level2name=null,@property='isLookup',@value='true'
exec setExtendedProperty @level1Object='table', @level1name='oppCustomerType',@level2Object=null,@level2name=null,@property='textIDColumn',@value='customerTypeTextID'
exec setExtendedProperty @level1Object='table', @level1name='oppPricingStatus',@level2Object=null,@level2name=null,@property='textIDColumn',@value='statusTextID'
--exec setExtendedProperty @level1Object='table', @level1name='oppPaymentTerms',@level2Object=null,@level2name=null,@property='isLookup',@value='true'

/* internal lead screen */
if not exists(select 1 from screens where screenTextId='leadInternalScreen')
begin
	declare @screenid int
	select @screenid = max(screenID)+1 from screens
	INSERT INTO [screens] 
	([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[lastUpdatedBy],[lastUpdated])
	VALUES(@screenid,'leadInternalScreen','Lead Internal Screen',1,0,'lead',0,0,'formRenderer','lead.isConverted()','',563,'Sep 14 2016 11:18:28:000AM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0,16,0,'',563,'Sep 14 2016 11:18:28:000AM') 

	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,1,0,1,'lead','salutation','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',563,'Sep 22 2016 10:46:53:207AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,5,0,1,'lead','FirstName','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 11 2016  4:02:38:243PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,10,0,1,'lead','LastName','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov  3 2016  5:12:00:750PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,15,0,1,'lead','Company','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:15:18:280PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,18,0,1,'lead','CountryID','','',0,0,'edit',0,0,NULL,NULL,'defaultvalue=[[user.person.location.countryID]]',0,0,'','',NULL,'','',101,'Nov 11 2016  5:05:17:153PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,20,0,1,'lead','Email','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:15:52:533PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,25,0,1,'lead','officePhone','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:16:39:947PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,30,0,1,'lead','website','',NULL,0,0,'edit',0,0,NULL,NULL,null,0,0,'','',NULL,'','',410,'Nov  2 2016  6:11:24:093PM',0,0)
	 INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,35,0,1,'lead','progressID','',NULL,0,0,'edit',0,0,NULL,NULL,'defaultValue=20624',0,0,'','',NULL,'','',563,'Sep 28 2016  1:52:09:553PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,37,0,1,'lead','leadTypeID','','',0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',101,'Nov 17 2016  9:32:01:290AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,40,0,1,'lead','enquiryType','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',563,'Sep 21 2016  4:33:09:907PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,45,0,1,'lead','description','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:33:31:950PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,50,0,1,'lead','sourceID','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',101,'Nov  3 2016  5:18:04:103PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,55,0,1,'lead','annualRevenue','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:35:37:783PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,60,0,1,'lead','currency','','',0,0,'edit',0,0,NULL,NULL,'display=currencyName,value=currencyISO,bindfunction=cfc:relay.webservices.relaycountries.getCurrenciesForCountry(countryID={lead_countryid})',0,0,'','',NULL,'','',101,'Nov 17 2016  9:41:15:873AM',0,0)
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,65,0,1,'lead','approvalStatusID','','',0,1,'edit',0,0,NULL,NULL,'data-showOtherFields={"rejected":"lead_rejectionreason"},defaultValue=20628',1,0,'','',NULL,'','',101,'Nov 17 2016  9:20:00:797AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,70,0,1,'lead','rejectionReason','','',0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 17 2016  9:21:02:357AM',0,0)
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,80,0,1,'lead','acceptedByPartner','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'',NULL,101,'Nov 11 2016 12:06:24:190PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,85,0,1,'HTML','<h2>phr_lead_section_contacts</h2>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,'',101,'Dec  9 2016  1:30:31:133PM',0,0)
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,87,0,1,'lead','partnerLocationID','','',0,0,'edit',0,0,NULL,NULL,'display=sitename,value=locationID,bindfunction=cfc:relay.webservices.relayOpportunityWS.getOpportunityPartners(countryID={lead_countryid})',0,0,'','',NULL,'','',101,'Nov 11 2016  5:17:56:007PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,90,0,1,'lead','partnerSalesPersonID','','',0,0,'edit',0,0,NULL,NULL,'value=personid,bindfunction=cfc:relay.webservices.relayOpportunityWS.getOppPartnerSalesPerson({lead_partnerlocationid}),bindonload=false,display=fullname',0,0,'','',NULL,'','',101,'Nov 17 2016  9:42:02:907AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,91,0,1,'lead','partnerSalesManagerPersonID','','',0,0,'edit',0,0,NULL,NULL,'value=personID,bindfunction=cfc:relay.webservices.relayOpportunityWS.getOppPartnerSalesPerson({lead_partnerlocationid}),bindonload=false,display=fullname',0,0,'','',NULL,'','',101,'Nov 17 2016 10:00:00:107AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,92,0,1,'lead','distiLocationID','','',0,0,'edit',0,0,NULL,NULL,'value=ID,bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName=''relayOpportunityWS'',methodName=''getOppDistiLocations'',countryID={lead_countryid},locationID={lead_partnerlocationid})",display=name',0,0,'','',NULL,'','',101,'Nov 17 2016  9:44:51:410AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,93,0,1,'lead','distiContactPersonID','','',0,0,'edit',0,0,NULL,NULL,'value=personID,bindfunction=cfc:relay.webservices.relayOpportunityWS.getDistiSalesPerson(locationID={lead_distilocationid}),bindonload=false,display=itemtext',0,0,'','',NULL,'','',101,'Nov 17 2016  9:46:14:793AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,94,0,1,'lead','vendorAccountManagerPersonID','','',0,0,'edit',0,0,NULL,NULL,'value=personId,display=personName,validValues=func:com.opportunity.getOppVendorAccountManagers()',0,0,'','',NULL,'','',101,'Nov 17 2016  9:51:21:377AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,96,0,1,'HTML','<h2>phr_lead_section_updateHistory</h2>','','',0,0,'edit',0,0,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,'',101,'Dec  9 2016  1:30:31:133PM',0,0)
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,97,0,1,'lead','viewedByPartnerDate','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 17 2016  9:52:54:587AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,98,0,1,'lead','acceptedByPartnerDate','','',0,0,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 17 2016  9:53:11:420AM',0,0) 
	/*INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,97,0,1,'lead','convertedOpportunityID','','',0,0,'edit',0,0,NULL,NULL,'displayAs=text',0,0,'','',NULL,'','lead.isConverted()',101,'Nov 18 2016  1:26:34:000PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,98,0,1,'lead','convertedLocationID','','',0,0,'edit',0,0,NULL,NULL,'displayAs=text',0,0,'','',NULL,'','lead.isConverted()',101,'Nov 18 2016  1:26:24:000PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,99,0,1,'lead','convertedPersonID','','',0,0,'edit',0,0,NULL,NULL,'displayAs=text',0,0,'','',NULL,'','lead.isConverted()',101,'Nov 18 2016  1:25:56:000PM',0,0) 
	*/
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,100,0,1,'formWidget','convertLead','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 17 2016  9:50:16:353AM',0,0)
end
GO


/* portal lead content screen */
if not exists(select 1 from screens where screenTextId='LeadPortalScreen')
begin
	declare @screenid int
	select @screenid = max(screenID)+1 from screens
	INSERT INTO [screens] ([ScreenID],[screentextid],[ScreenName],[active],[suite],[entitytype],[sortorder],[scope],[viewer],[ViewEdit],[preInclude],[createdBy],[created],[appName],[Purpose],[Live],[DefaultLanguageID],[DefaultCountryID],[UseScreenDef],[path],[frmNextPage],[UpdateMessage],[SendeMail],[ShowSecurePage],[allowNewLocs],[PeopleMaintScrn],[exitpage],[processid],[startstepid],[AppParameter1],[securityTask],[userCookie],[parameters],[tablewidth],[entityTypeID],[predefined],[postinclude],[lastUpdatedBy],[lastUpdated])
	VALUES(@screenid,'LeadPortalScreen','Lead Portal Screen',1,0,'lead',0,0,'formRenderer','lead.isConverted()','',563,'Sep 14 2016 11:18:28:000AM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,'',0,16,0,'',563,'Sep 14 2016 11:18:28:000AM') 

	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,1,0,1,'lead','salutation','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',563,'Sep 22 2016 10:46:53:207AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,5,0,1,'lead','FirstName','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:14:35:653PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,10,0,1,'lead','LastName','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov  3 2016  5:12:00:750PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,15,0,1,'lead','Company','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:15:18:280PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,18,0,1,'lead','CountryID','','',0,1,'edit',0,0,NULL,NULL,'defaultvalue=[[user.person.location.countryID]]',0,0,'','',NULL,'','',101,'Nov 16 2016  1:16:23:360PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,20,0,1,'lead','Email','',NULL,0,1,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:15:52:533PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,25,0,1,'lead','officePhone','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:16:39:947PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,30,0,1,'lead','website','',NULL,0,0,'edit',0,0,NULL,NULL,NULL,0,0,'','',NULL,'','',410,'Nov  2 2016  6:11:24:093PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,35,0,1,'lead','progressID','','',0,1,'edit',0,0,NULL,NULL,'defaultValue=20624',1,0,'','',NULL,'','',101,'Nov 15 2016 11:37:44:037AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,37,0,1,'lead','leadTypeID','','',0,0,'edit',0,0,NULL,NULL,'data-showotherfields={"20619":"lead_acceptedbypartner"},defaultValue=20620,displayAs=hidden',1,0,'','',NULL,'','',101,'Nov 15 2016  2:55:26:707PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,40,0,1,'lead','enquiryType','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',563,'Sep 21 2016  4:33:09:907PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,45,0,1,'lead','description','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:33:31:950PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,50,0,1,'lead','sourceID','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','',101,'Nov  3 2016  5:18:04:103PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,55,0,1,'lead','annualRevenue','',NULL,0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',563,'Sep 21 2016  4:35:37:783PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,60,0,1,'lead','currency','','',0,0,'edit',0,0,NULL,NULL,'bindonload=true,value=currencyISO,bindfunction=cfc:relay.webservices.relaycountries.getCurrenciesForCountry(countryID={lead_countryid}),display=CurrencyName',0,0,'','',NULL,'','',410,'Nov 15 2016  5:23:26:340PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,65,0,1,'lead','approvalStatusID','','',0,0,'view',0,0,NULL,NULL,'data-showotherfields={"rejected":"lead_rejectionreason"},defaultValue=20628',1,0,'','',NULL,'','',101,'Nov 16 2016  5:03:42:067PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,70,0,1,'lead','rejectionReason','','',0,1,'view',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 15 2016  3:51:13:653PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,85,0,1,'lead','acceptedByPartner','',NULL,0,0,'edit',0,0,NULL,NULL,'',1,0,'','',NULL,'','!lead.isNewRecord()',101,'Nov 11 2016 12:06:24:190PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,90,0,1,'lead','partnerLocationID','','',0,0,'1',0,0,NULL,NULL,'defaultvalue=[[user.person.locationID]],displayas=Hidden',0,0,'','',NULL,'','',101,'Nov 16 2016  2:26:41:253PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,95,0,1,'lead','partnerSalesPersonID','','',0,0,'edit',0,0,NULL,NULL,'defaultvalue=[[user.person.personID]],displayas=[[user.person.isPrimaryContact()?''select'':''hidden'']],value=personid,bindfunction=cfc:relay.webservices.relayOpportunityWS.getOppPartnerSalesPerson({lead_partnerlocationid}),display=fullname',0,0,'','',NULL,'','',101,'Nov 16 2016  2:26:55:807PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,97,0,1,'lead','distiLocationID','','',0,0,'edit',0,0,NULL,NULL,'value=ID,bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName=''relayOpportunityWS'',methodname=''getOppDistiLocations'',countryid={lead_countryid},locationid={lead_partnerlocationid})",display=name',0,0,'','',NULL,'','',410,'Nov 15 2016  5:36:51:883PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,98,0,1,'lead','distiContactPersonID','','',0,0,'edit',0,0,NULL,NULL,'value=personid,bindfunction=cfc:relay.webservices.relayOpportunityWS.getDistiSalesPerson(locationID={lead_distilocationid}),display=itemtext',0,0,'','',NULL,'','',410,'Nov 15 2016  5:36:06:873PM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,99,0,1,'lead','vendorAccountManagerPersonID','','',0,0,'view',0,0,NULL,NULL,'validValues=func:com.opportunity.getOppVendorAccountManagers(),defaultvalue=[[user.person.getAccountManagerPersonId()]],value=personID,display=personName',0,0,'','',NULL,'','',101,'Nov 17 2016 10:27:28:987AM',0,0) 
	INSERT INTO [screendefinition] 
	([ScreenID],[SortOrder],[CountryID],[active],[FieldSource],[FieldTextID],[evalstring],[FieldLabel],[TranslateLabel],[Required],[Method],[AllColumns],[BreakLn],[Maxlength],[Size],[specialformatting],[UseValidValues],[lookup],[TRClass],[TDClass],[eventList],[jsVerify],[condition],[LastUpdatedBy],[LastUpdated],[IsCollapsable],[IsVisible])
	VALUES(@screenid,100,0,1,'formWidget','convertLead','','',0,0,'edit',0,0,NULL,NULL,'',0,0,'','',NULL,'','',101,'Nov 15 2016 10:38:51:647AM',0,0)
end
GO

/* NJH 2016/11/24 JIRA PROD2016-2218 (lead screens) - set the setting for any existing customers */
if not exists(select 1 from settings where name='versions.leadScreen')
	insert into settings (name,value) values ('versions.leadScreen',1)
	
/* set some readonly fields for leads */
update modEntityDef set readonly=1 where tablename='lead' and (fieldname in ('leadID','acceptedByPartnerDate','viewedByPartnerDate','convertedPersonID','convertedLocationID','convertedOpportunityID') or fieldname like 'created%' or fieldname like 'lastUpdated%') and readonly=0
/* set filter on country field for lead */
update modentitydef set cfformParameters = 'filter=isocode is not null' where fieldname = 'countryid' and tablename = 'lead'

/* YMA 2016/02/03 PROD2016-2377 Connector will not synch more than 1 user per object */
update connectorCustomMapping set selectDependencyStatement = 'accManager.personID as [exportUser' +char(172)+ 'ownerID' +char(172)+'personID]' where selectDependencyStatement = 'accManager.personID as [exportUser' +char(172)+ 'personID]'

/* NJH 2017/01/04 PROD2016-3070 - update wrong configuration. Opportunity merge fields existed in lead email */
declare @emailDefID int
select @emailDefID = emailDefId from emailDef where emailTextID='LeadAccountManagerNotification'

update phrases
	set phraseText = replace(phraseText,'&amp;opportunityID=[[opportunity.opportunityID]]"','&amp;leadID=[[lead.leadID]]"')
from phrases p
	inner join phraseList pl on pl.phraseID = p.phraseID
	and pl.entityID=@emailDefID
	and pl.entityTypeID=200
	and pl.phraseTextId = 'body'
	and p.phraseText like '%&amp;opportunityID=\[\[opportunity.opportunityID]]"%' escape '\'
GO

-- NJH 2017/01/30 JIRA PROD2016-3317 create the valid values for the lead acceptedByPartner field
if not exists(select 1 from validFieldValues where fieldname='lead.acceptedByPartner')
begin
	insert into validFieldValues (fieldname,countryID,validValue,defaultFieldCase,defaultFieldValue,sortOrder,dataValue,languageID)
	values ('lead.acceptedByPartner',0,'Accepted','T',0,1,'1',1)
	insert into validFieldValues (fieldname,countryID,validValue,defaultFieldCase,defaultFieldValue,sortOrder,dataValue,languageID)
	values ('lead.acceptedByPartner',0,'Rejected','T',0,1,'0',1)
end

/* AHL 2017/01/07 PROD2016-3307 Adding Banner into media groups */

IF NOT EXISTS(SELECT 1 FROM [filetypegroup] WHERE [heading]='Banner')
BEGIN
	INSERT INTO [dbo].[filetypegroup] ([heading], [intro], [screenID], [created], [createdBy], [lastUpdatedBy], [lastUpdated], [portalSearch]) 
		VALUES (N'Banner', N'Banner', 0, '2017-02-07 12:00:00', 0, 0 , '2017-02-07 12:00:00', 0 )
	
	IF NOT EXISTS(SELECT 1 FROM [filetype] t INNER JOIN [filetypegroup] g ON t.[filetypegroupid] = g.[filetypegroupid] AND g.[heading]='Banner')
	BEGIN
		DECLARE @filetypegroupid INT 
		SELECT @filetypegroupid = [filetypegroupid] FROM [dbo].[filetypegroup] WHERE heading = 'Banner'
	
		INSERT INTO [dbo].[filetype] ([filetypegroupid],[type],[path],[secure],[autounzip],[DefaultDeliveryMethod],[created],[createdBy],[allowedFiles],[lastUpdated],[lastUpdatedBy],[lastUpdatedByPerson])
			VALUES ( @filetypegroupid, N'Banner', N'Banners', 1, 0, 'Local', '2017-02-07 12:00:00', 535, NULL, NULL, 535, 5054 )

	END
END


/* WAB 2017-02-08 remove person.emailStatus flag.  Its name duplicates a core field, and nobody seems to use it anyway (but I will do a check in any case)*/
declare @flagGroupID int 
 select @flagGroupID = flagGroupID from flagGroup where flagGroupTextID = 'emailStatus'
IF  @flagGroupID  is not null
BEGIN
	IF NOT EXISTS (select 1 from booleanflagdata where flagid in (select flagid from flag where flaggroupid = @flaggroupid) )
	BEGIN
		delete from flag where flagGroupID = @flagGroupID
		delete from flaggroup where flagGroupID = @flagGroupID
	END
	ELSE
	BEGIN
		Print 'Flag EmailStatus Could not be deleted because it is in use'
	END
	
END

/* ONB-393-right-to-left-styling Adding Hebrew language into CORE */

BEGIN TRAN

DECLARE @maxLanguageID int;
SELECT @maxLanguageID = COALESCE(MAX(languageid),0) + 1 FROM [dbo].[language];

INSERT INTO [dbo].[language] (Language,languageid,buttonid,ISOCode,localLanguageName) VALUES ('Hebrew',@maxLanguageID,'he','HE',N'עברית')

COMMIT TRAN

BEGIN TRAN

DECLARE @flagroupid int;
SELECT @flagroupid = g.flaggroupID FROM [dbo].[flaggroup] g WHERE g.flaggrouptextid LIKE 'LanguageRights'

INSERT INTO [dbo].[Flag]
	 (FlagGroupID,Name,Active,CreatedBy,Created,lastUpdatedBy,LastUpdated,flagTextID,NamePhraseTextID,score,createdByPerson,lastUpdatedByPerson,Description,OrderingIndex,WDDXStruct,currentCount,currentCountDate,formattingParameters)
	 VALUES
	 (@flagroupid,'Hebrew',1,438,GETDATE(),438,GETDATE(),'Hebrew','flag_Hebrew',0,1032,1032,'',25,'',0,GETDATE(),'')

COMMIT TRAN


