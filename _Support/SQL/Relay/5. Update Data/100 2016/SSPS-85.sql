IF EXISTS(SELECT * FROM sys.views WHERE name = 'vFundRequestList_Base')
  DROP VIEW dbo.vFundRequestList_Base;
GO

CREATE VIEW dbo.vFundRequestList_Base
AS
  SELECT
    fr.fundRequestID,
    fca.AccountID,
    bp.budgetPeriodID,
    fr.fundRequestID AS Request_ID,
    bp.description AS period,
    dbo.organisation.OrganisationName AS Account,
    dbo.organisation.countryID,
    c.CountryDescription AS country,
    bh.description AS BudgetGroup,
    bh.BudgetGroupID,
    b.budgetID,
    fca.OrganisationID,
    CASE WHEN b.entityTypeID = 2
              AND bpa.authorised = 1
      THEN bpa.amount
    ELSE NULL
    END AS orgBudgetAllocation,
    bpa.budgetPeriodAllocationID,
    fra.createdBy,
    p.LocationID,
    fra.fundRequestActivityID,
    ISNULL(fra.requestedAmount,0) as Sum_requested,
    fraa.fundActivityApprovalID,
    ISNULL(fraa.amount, 0) as Sum_approved
  FROM
    dbo.FundCompanyAccount AS fca
    INNER JOIN dbo.organisation ON fca.OrganisationID = dbo.organisation.OrganisationID
    INNER JOIN dbo.Country AS c ON c.CountryID = dbo.organisation.countryID
    INNER JOIN dbo.FundAccountBudgetAccess AS faba ON faba.accountID = fca.AccountID AND faba.budgetID = fca.budgetID
    INNER JOIN dbo.Budget AS b ON b.budgetID = faba.budgetID
    INNER JOIN dbo.BudgetGroup AS bh ON b.BudgetGroupID = bh.BudgetGroupID
    INNER JOIN dbo.BudgetPeriodAllocation AS bpa ON bpa.budgetID = b.budgetID
    INNER JOIN dbo.BudgetPeriod AS bp ON bp.budgetPeriodID = bpa.budgetPeriodID
    LEFT OUTER JOIN dbo.fundRequest AS fr ON fca.AccountID = fr.accountID AND bp.budgetPeriodID = fr.budgetPeriodID
    LEFT OUTER JOIN dbo.fundRequestActivity fra ON fra.fundRequestID = fr.fundRequestID
    LEFT OUTER JOIN dbo.Person p ON p.PersonID = fra.createdBy
    LEFT OUTER JOIN dbo.fundRequestActivityApproval fraa on fraa.fundRequestActivityID = fra.fundRequestActivityID

GO


IF EXISTS(SELECT * FROM sys.views WHERE name = 'vFundRequestList')
  DROP VIEW dbo.vFundRequestList;
GO

CREATE VIEW dbo.vFundRequestList
AS
SELECT
	vfrlb.fundRequestID,
	vfrlb.AccountID,
	vfrlb.budgetPeriodID,
	vfrlb.fundRequestID AS Request_ID,
	vfrlb.period,
	vfrlb.Account,
	vfrlb.countryID,
	vfrlb.country,
	vfrlb.BudgetGroup,
	vfrlb.BudgetGroupID,
	vfrlb.budgetID,
	SUM(vfrlb.Sum_approved) as Sum_approved,
	SUM(vfrlb.Sum_requested) as Sum_requested,
	COUNT(vfrlb.fundRequestActivityID) as count_requests,
	vfrlb.OrganisationID,
	vfrlb.orgBudgetAllocation,
	vfrlb.budgetPeriodAllocationID

 FROM vFundRequestList_Base vfrlb
 GROUP BY vfrlb.fundRequestID,
			vfrlb.AccountID,
			vfrlb.budgetPeriodID,
			vfrlb.fundRequestID,
			vfrlb.period,
			vfrlb.Account,
			vfrlb.countryID,
			vfrlb.country,
			vfrlb.BudgetGroup,
			vfrlb.BudgetGroupID,
			vfrlb.budgetID,
			vfrlb.OrganisationID,
			vfrlb.orgBudgetAllocation,
			vfrlb.budgetPeriodAllocationID

GO