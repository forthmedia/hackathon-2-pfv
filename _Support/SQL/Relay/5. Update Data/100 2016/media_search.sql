update schemaTableBase
set ScreensExist = 1,
FlagsExist = 1
where entityName = 'files'

if not exists(select 1 from screens s 
              where s.entityTypeID = 47 and entitytype = 'files' and screentextid = 'FilesEdit_Custom') 
begin
    insert into screens (screenID,screentextid, ScreenName, active, entitytype, viewer, ViewEdit,entityTypeID,sortorder,createdby,lastupdatedby)
	values ((select max(screenID+1) from screens),'FilesEdit_Custom','Files Screen',1,'files','ShowScreen','edit',47,0,0,0)
end
GO


exec createflagview @entityName='files',@viewName='vfileswithboolean',@includeBooleans=1