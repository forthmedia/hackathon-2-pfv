

/*
Script for migrating from use of rightsFlagGroup table to userGroup.conditionSQL
WAB 2016-06-13 Improvements to deal with usergroups based on an OR between more than one flag
*/

IF exists (select 1 from information_schema.tables where table_name = 'rightsFlagGroup') 
BEGIN

		update
			usergroup
		set conditionSQL = '1=1 \* set for all people *\'
		from
			usergroup ug
				inner join
			rightsflaggroup rfg on ug.usergroupid = rfg.usergroupid
		where 
			isnull(ug.conditionSQL,'') = ''
			and 
			flagid = 0	  


		update 
			usergroup
		set 

			conditionSQL  = new.condition,
			conditionvalidated = 1
		from 
			usergroup ug
				inner join
			(
				/* 
				WAB 2016-06-13
				Have now realised that I can deal with usergroups based on an OR between more than one flag by using a concatenate
				*/
				Select 
						usergroupid
						, replace (dbo.concatenate (fc.condition + '*'), '*', char(10) + 'OR' + char(10)) as condition  /* The replace construction just replaces the single character delimiter with an OR statement */
				from 
					rightsflaggroup rfg 
				inner join 
					vFlagConditions fc on fc.flagid = rfg.flagid

				group by usergroupid
			) as new on ug.usergroupid = new.usergroupid  


		where 
			isnull(ug.conditionSQL,'') = ''



	
	exec sp_rename 'rightsFlagGroup','rightsFlagGroup_notused'		

	/* need some warning to be raised during release if some of the records have not been upgraded correctly */
	select 
		'These Flags in RightsFlagGroup table have not been upgraded correctly', 
		ug.name, 
		rfg.flagid 
	from usergroup ug
			inner join 
		rightsflaggroup_notused  rfg on ug.usergroupid = rfg.usergroupid 
	where 
		isnull(ug.conditionsql,'') = ''

	IF @@rowcount <> 0
	BEGIN
		RAISERROR ('Some Flags in RightsFlagGroup table have not been upgraded correctly.  Please refer to Wiki for more information',16,16)
	END

END

GO

	/* migrate entitynames to match schemaTable and allow use of entityTypeID: 
		screen to screens	
		event to eventDetail (WAB 2016-01-12 BF-226) 
	*/
	update recordrights set entity = 'screens' where entity = 'screen'

	update recordrights set entity = 'eventdetail' where entity = 'event'
	
	update recordrights set entity = 'opportunity' where entity = 'opportunityManagement'

	/* this will populate the entityTypeID field (using the trigger on the column */
	update recordrights set entity = entity where entityTypeID is null

	select  'These are entities without an entityTypeID', entity, permission, count(1) from recordrights where entityTypeID is null
	group by entity, permission

GO
	declare @entityName sysname
	select distinct @entityName = entity from recordrights where entityTypeID is null
	IF @entityName is not null
	BEGIN
		declare @msg varchar(max) = 'There are records relating to ' + @entityName + ' in the recordrights table.  These need to be updated to refer to actual entities ' 
		RAISERROR (@msg, 16, 16)
	END

GO


/*  
	Populate the hasRecordRightsBitMask column by activating trigger
	This query generates the SQL to do this on all tables with hasRecordRightsBitMask column 
*/

 declare @sql nvarchar (max) = ''
 select 
	@sql +=
	'
		IF Exists (select 1 from ' + table_name + ' where hasRecordRightsBitMask is null)
	update recordRights
	set
		permission = permission 
	where 
				entity = ''' + table_name + ''';
	'
 from 
	information_schema.columns 
 where 
	column_Name = 'hasRecordRightsBitMask' and table_name not like '%del'

exec (@sql)




/*  WAB 2016-12-20 PROD2016-3041 Problems occuring when flagGroups are changed to isBitField=true, need to update the userGroup conditions.  This should correct problems
	Possibly need to make this a function and call it on a trigger 
 */
declare @rows int = -1

WHILE @rows <> 0 
BEGIN

	update usergroup
		set 
			conditionSQL = replace (conditionSQL, oldCondition, condition),
			conditionValidated = 1
		--select * 
	from 
		(
		select 
			'dbo.listfind( ' + f.entityTable + '.' + f.flagGroupTextID + ' , ''' + flagtextID + ''' ) <> 0' as OldCondition
			,fc.condition

		from 
			vflagdef f 
				inner join 
			flagGroup fg on fg.flagGroupID = f.flagGroupID
				inner join
			vflagConditions fc 	on	fc.flagid = f.flagid
		where 
			dbo.getParameterValue (fg.formattingParameters,'isBitField',',') = 'true' and f.entityTypeID in (0,1,2)
	) as x
			inner join
		usergroup ug on conditionSQL like '%' + x.oldCondition+ '%'	
			
	set @rows = @@rowcount
END
