/*
WAB CASE 431028, implement rotating passkeys for formstate variable. Used for OOTB data
*/
IF not exists (select 1 from settings where name = 'security.formstatepasskeys.01')
BEGIN
	insert into settings (name, value, lastupdated) values('security.formstatepasskeys.01',left(replace(newid(),'-',''),32),GETDATE())
END
