/* 
	WAB/SB 2015-10-21 Add a header and a footer phrase for emails

	*/



exec addNewPhraseAndTranslation @phraseTextID = 'email_header', @entitytypeid = 0, @entityid=0, @updateMode = 0, @languageID = 1, @phraseText= 
		'<table class="header" style="width:100%;">    <tbody>      <tr>        <td align="left" style="padding-left:10px; padding-top:4px;"><img src="[[func.getExternalSiteURL(2)]]/content/miscImages/images/personal%20(5116)/relaywareLogo.png" style="padding:0px; margin:0;px" /></td>        <td align="right" class="emailType" style="padding-right:10px;">[[organisationname]]</td>      </tr>    </tbody>  </table>  <table class="gradientHead" style="width:100%; height:20px;">    <tbody>      <tr>        <th> </th>      </tr>    </tbody>  </table>'
	 
exec addNewPhraseAndTranslation @phraseTextID = 'email_footer', @entitytypeid = 0, @entityid=0, @updateMode = 0, @languageID = 1, @phraseText= 
		'<table class="footer" style="width:100%;">    <tr>        <td style="padding-top:20px;"></td>      </tr>      <tr>        <td id="footerContainer" style="text-align:center; padding:10px 0; background: #e9e9e9;  border-top:2px solid #fff;">        <p style="line-height:16px; font-size:11px;">Trademark used under licence by Relayware Inc<br />        ©2014 Relayware Inc</p>        </td>      </tr>  </table>'
