/* 
WAB 2015-02-25
I have removed the very large scripts which add items to the countryScope and screenIndex tables
I have also altered the update trigger so that just doing an update on the countryID table will update the countryScope and screenIndex tables
So this simple update will populate these tables
Takes about 3 minutes on the OOTB build
*/

update screenDefinition set countryID = countryID
