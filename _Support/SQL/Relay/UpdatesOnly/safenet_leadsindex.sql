/*GCC/DCC 2016-01-04 many leads on internal homepage fix*/
IF NOT EXISTS(
			SELECT * 
			FROM sys.indexes 
			WHERE name='IX_ActiveLeads' AND object_id = OBJECT_ID('Lead')
) BEGIN 

	CREATE NONCLUSTERED INDEX [IX_ActiveLeads]
	ON [dbo].[Lead] ([active],[convertedOpportunityID])
	INCLUDE ([leadTypeID],[CountryID])
	
END
