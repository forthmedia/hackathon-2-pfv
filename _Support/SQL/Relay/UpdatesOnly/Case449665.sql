/* Case 449665 - change stageTextId for LOST oppStage to match current value referenced in the core (due to recent renaming - see BF-571) */
if exists (select * from oppStage where status='LOST' and stageTextID='ClosedLost')
update oppStage set stageTextID='OppStageLost' where status='LOST' and stageTextID='ClosedLost'
GO
