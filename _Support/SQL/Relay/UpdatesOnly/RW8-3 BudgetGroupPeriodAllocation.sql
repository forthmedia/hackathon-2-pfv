/*
This script adds a large BudgetGroupPeriodAllocation for every BudgetGroup/Period combination so that existing users will not be restricted by the MIGRATION
it is recommeded that the client edit each current BudgetGroupPeriodAllocation and reduce it to a more realistic amount (if they wish to use this facility)
*/

INSERT INTO [dbo].[BudgetGroupPeriodAllocation]
           ([budgetGroupID]
           ,[budgetPeriodID]
           ,[amount]
           ,[authorised]
           ,[created]
           ,[createdBy]
           ,[lastUpdated]
           ,[lastUpdatedBy])
SELECT bg.budgetGroupID, bp.budgetPeriodID, 99999999, 1, getdate(), 0,getdate(),0
FROM dbo.budgetGroup bg, dbo.budgetPeriod bp 
WHERE NOT EXISTS(SELECT 1 FROM dbo.BudgetGroupPeriodAllocation WHERE budgetGroupID = bg.budgetGroupID AND budgetPeriodID = bp.budgetPeriodID )
GO


