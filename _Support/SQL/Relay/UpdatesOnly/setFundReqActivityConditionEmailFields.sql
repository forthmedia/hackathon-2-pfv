--17/08/2015    DAN    GE MDF P2 - setting fields of FundRequestActivity table to appear in conditional emails

update modEntityDef set showInConditionalEmails=1 where TableName = 'fundRequestActivity' and FieldName in ('createdBy','roi','currentApprovalLevel','authorised','fundApprovalStatusID','currentApprovalLevel','startDate','endDate','updated','startDate','endDate');
