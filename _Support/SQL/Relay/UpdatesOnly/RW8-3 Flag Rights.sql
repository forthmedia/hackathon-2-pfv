/* Scripts to prevent enforcement of flag rights on existing screens for purposes of migration */

/*select 
      sd.fieldsource,sd.fieldtextid,
      viewing,edit,viewingAccessRights,EditAccessRights,
      fg.flaggroupid , */
update screendefinition set specialFormatting = 
isNull(specialFormatting,'') +  case when isNull(specialFormatting,'') = '' then '' else ',' end + 'ignoreFlagRightsForBackwardsCompatibility=true'
from 
      screendefinition sd
      left join (
      flag f 
      inner join flagGroup fg on f.flaggroupid = fg.flagGroupid)
      on (f.flagTextid = sd.fieldTextid  or convert(varchar,flagid) = sd.fieldTextid   )
      
where 
      sd.fieldsource = 'flag' 
      and (isNull(edit,0) = 0 or isNull(viewing,0) = 0)
      and isnull(specialFormatting,'') not like 'ignoreFlagRightsForBackwardsCompatibility=%'
      
      
/* select 
      sd.fieldsource,sd.fieldtextid,
      viewing,edit,viewingAccessRights,EditAccessRights,
      fg.flaggroupid , */
update screendefinition set specialFormatting = 
isNull(specialFormatting,'') +  case when isNull(specialFormatting,'') = '' then '' else ',' end + 'ignoreFlagRightsForBackwardsCompatibility=true'
from 
      screendefinition sd
      left join flagGroup fg 
      on (fg.flagGroupTextid = sd.fieldTextid or convert(varchar,fg.flagGroupid) = sd.fieldTextid)
      
where 
      sd.fieldsource = 'flaggroup'  
      and (isNull(edit,0) = 0 or isNull(viewing,0) = 0)
      and isnull(specialFormatting,'') not like 'ignoreFlagRightsForBackwardsCompatibility=%'   
      
