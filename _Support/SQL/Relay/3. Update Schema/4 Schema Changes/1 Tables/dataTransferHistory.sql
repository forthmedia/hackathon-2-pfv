
if not exists (select * from sysobjects where name='DataTransferHistory')
begin
CREATE TABLE [dbo].[DataTransferHistory](
	[DthID] [int] IDENTITY(1,1) NOT NULL,
	[DtdID] [int] NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[Rows] [int] NOT NULL CONSTRAINT [DF__DataTransf__Rows__627B613F]  DEFAULT (0),
	[MaxModRegisterID] [int] NOT NULL,
	[GenerateFile] [bit] NOT NULL CONSTRAINT [DF__DataTrans__Gener__636F8578]  DEFAULT (0),
	[filename] [varchar](100) NULL,
	[scheduledTaskLogID] [int] NULL,
	[isOK] [bit] NULL DEFAULT (1),
 CONSTRAINT [PK_DataTransferHistory] PRIMARY KEY CLUSTERED 
(
	[DthID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
End
GO

-- add scheduledTaskLogId
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'dataTransferHistory' AND column_name='scheduledTaskLogID') 
ALTER TABLE dataTransferHistory add scheduledTaskLogID int NULL
GO

-- add filename which holds file processed
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'dataTransferHistory' AND column_name='Filename') 
ALTER TABLE dataTransferHistory add filename varchar(100) NULL

GO

-- add isOK which holds whether the file processed successfully or not
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'dataTransferHistory' AND column_name='isOK') 
ALTER TABLE dataTransferHistory add isOK bit default(1)

GO

update dataTransferHistory set isOK = 1 where isOK is null


if not exists (select 1 from information_schema.columns where table_name='dataTransferHistory' and column_name='trace')
begin
	alter table dataTransferHistory add trace text null
end

GO

if exists (select * from sysindexes where name='IX_DataTransferHistory_dtdID')
drop index DataTransferHistory.IX_DataTransferHistory_dtdID
GO

/****** Object:  Index [IX_DataTransferHistory_dtdID]    Script Date: 02/27/2012 09:38:09 ******/
CREATE NONCLUSTERED INDEX [IX_DataTransferHistory_dtdID] ON [dbo].[DataTransferHistory] 
(
	[DtdID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

