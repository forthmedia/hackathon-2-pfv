/* P-SOP019*/
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'eventflagdata' AND column_name='AttendedDate') 
ALTER TABLE eventflagdata add AttendedDate datetime NULL



-- this trigger was used for the Triathlon project but is no longer needed (LID4831)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FullEventSwitch]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[FullEventSwitch]
GO
