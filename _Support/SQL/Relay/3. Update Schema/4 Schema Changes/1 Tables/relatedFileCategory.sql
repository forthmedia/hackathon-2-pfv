
/* NJH 2016/03/10 - JIRA PROD2016-472 Synching Attachments */
if not exists (select 1 from information_schema.columns where table_name='relatedFileCategory' and column_name='entityTypeID')
alter table relatedFileCategory add entityTypeId int null
GO

update relatedFileCategory set entityTypeID = s.entitytypeID
from relatedFileCategory rfc
	inner join schemaTable s on rfc.fileCategoryEntity = s.entityName
where rfc.entityTypeID is null