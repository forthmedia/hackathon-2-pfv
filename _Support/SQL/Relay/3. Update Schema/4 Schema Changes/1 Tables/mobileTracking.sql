/****** Object:  Table [dbo].[apiRequest]    Script Date: 03/22/2012 16:15:27 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mobileTracking]') AND type in (N'U'))

CREATE TABLE [dbo].[mobileTracking](
	[mobileTrackingID] [int] IDENTITY(1,1) NOT NULL,
	[visitID] [int] NOT NULL,
	[viewTextID] [varchar](50) NOT NULL,
	[entityID] [int] NULL,
	[entityTypeID] [int] NULL,
	[created] [datetime] NOT NULL,
 CONSTRAINT [PK_mobileTracking] PRIMARY KEY CLUSTERED 
(
	[mobileTrackingID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_mobileTracking_created]') AND type = 'D')
ALTER TABLE [dbo].[mobileTracking] ADD  CONSTRAINT [DF_mobileTracking_created]  DEFAULT (getdate()) FOR [created]
GO