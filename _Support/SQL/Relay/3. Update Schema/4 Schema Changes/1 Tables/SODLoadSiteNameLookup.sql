if not exists (select 1 from sysobjects where name = 'SODloadSiteNameLookup')
BEGIN

CREATE TABLE [dbo].[SODloadSiteNameLookup]
(
[thirdPartySiteName] [nvarchar] (50) NOT NULL,
[locationID] [int] NOT NULL,
[created] [datetime] NOT NULL DEFAULT (getdate()),
[createdBy] [int] NOT NULL DEFAULT (0),
[lastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
[lastUpdatedBy] [int] NOT NULL DEFAULT (0)
)

ALTER TABLE [dbo].[SODloadSiteNameLookup] ADD CONSTRAINT [PK_SODloadSiteNameLookup] PRIMARY KEY CLUSTERED  ([thirdPartySiteName])
END
