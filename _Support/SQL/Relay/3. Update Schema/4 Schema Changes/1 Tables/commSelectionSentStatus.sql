/*  
WAB 2013-03-04
Added a new table to make it more explicit what the different values in commselection.sent mean 
*/

IF NOT EXISTS (select 1 from Sysobjects where name = 'commSelectionSentStatus')
BEGIN
	CREATE TABLE commSelectionSentStatus	
		(
		sent int NOT NULL,
		sentTextID varchar(20) NOT NULL
		)  ON [PRIMARY]
	
	ALTER TABLE dbo.commSelectionSentStatus	 ADD CONSTRAINT
		PK_commSelectionSentStatus	 PRIMARY KEY CLUSTERED 
		(
		sent
		) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	insert into commSelectionSentStatus	
		select 0,'Unsent'
		UNION
		select 1,'Sent'
		UNION
		select 2,'Scheduled'
		UNION
		select 3,'PartSent'

END
