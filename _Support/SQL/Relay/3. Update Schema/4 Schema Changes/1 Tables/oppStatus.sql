IF NOT EXISTS (select * from ModEntityDef where TableName='oppStatus')
BEGIN
 	exec generate_MRAudit @TableName = 'oppStatus'
END

IF EXISTS (select * from ModEntityDef where TableName='oppStatus')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='status' where TableName ='oppStatus' and FieldName = 'Status' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStatus')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='statusID' where TableName ='oppStatus' and FieldName = 'statusID' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStatus')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='statusTextID' where TableName ='oppStatus' and FieldName = 'statusTextID' and ApiActive=0
END
GO