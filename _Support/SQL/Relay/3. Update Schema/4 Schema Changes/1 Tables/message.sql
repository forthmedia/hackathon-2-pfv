

if not exists (select 1 from sysobjects where name='message' and type='u')
begin

CREATE TABLE [dbo].[message](
	[messageID] [int] IDENTITY(1,1) NOT NULL,
	[title_defaultTranslation] [nvarchar](100) NULL,
	[text_defaultTranslation] [nvarchar](250) NULL,
	[url] [nvarchar](150) NULL,
	[image] [varchar](100) NULL,
	[sendToEntityType] [varchar](10) NOT NULL,
	[sendToEntityID] [int] NOT NULL,
	[sendDate] [datetime] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_message] PRIMARY KEY CLUSTERED 
(
	[messageID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[message] ADD  CONSTRAINT [DF_message_created]  DEFAULT (getdate()) FOR [created]

ALTER TABLE [dbo].[message] ADD  CONSTRAINT [DF_message_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]

End
GO

alter table [dbo].[message] alter column sendToEntityType varchar(10) null
alter table [dbo].[message] alter column sendToEntityID int null

GO

if not exists (select 1 from information_schema.columns where table_name = 'message' and column_name = 'campaignID')
	alter table message add campaignID int null
GO