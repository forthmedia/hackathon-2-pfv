if not exists(select 1 from INFORMATION_SCHEMA.tables where TABLE_NAME = 'commFromDisplayName')
BEGIN
	CREATE TABLE [dbo].[commFromDisplayName](
		[commFromDisplayNameID] [smallint] primary key not null identity,
		[displayName] [nvarchar] (255) not null,
		[active] [bit] not null default 1,
		[CreatedBy] [int] null,
		[Created] [datetime] null,
		[LastUpdatedBy] [int] null,
		[LastUpdated] [datetime] null,
		[lastupdatedbypersonid] [int] null
	)
END