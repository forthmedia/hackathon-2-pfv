IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[approvalEngineApprover]') AND type in (N'U'))

	CREATE TABLE dbo.approvalEngineApprover
		(
		approvalEngineApproverID int NOT NULL 
			IDENTITY (1, 1) 
			CONSTRAINT [PK_approvalEngineApproverID] PRIMARY KEY CLUSTERED,
		approvalEngineID int 
			FOREIGN KEY REFERENCES [dbo].[approvalEngine] ([approvalEngineID]),
		approverEntityID int NOT NULL,
		approverLevel int NOT NULL,
		entityTypeID int NOT NULL 
			FOREIGN KEY REFERENCES [dbo].[schemaTableBase] ([entityTypeID]),
		[Created] [datetime] NOT NULL DEFAULT (getdate()),
		[CreatedBy] [int] NULL,
		[LastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
		[LastUpdatedBy] [int] NULL,
		[lastUpdatedByPerson] [int] NULL
		)  ON [PRIMARY]

GO


IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=511) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'approvalEngineApprover') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'AEA') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
	VALUES (511, 'approvalEngineApprover', 'approvalEngineApproverID', 0, 'AEA', 0);
GO