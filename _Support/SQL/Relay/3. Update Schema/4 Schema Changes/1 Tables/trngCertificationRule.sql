

/* add sortOrder to trngCertificationRule */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationRule' AND column_name='certificationRuleOrder') 
ALTER TABLE trngCertificationRule add certificationRuleOrder int NULL
GO


/* WAB 2016-10-18 PROD2016-2546 Change certificationRuleOrder to be not null with a default of 0 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationRule' AND column_name='certificationRuleOrder' and is_Nullable = 'Yes')
BEGIN
	update trngCertificationRule set certificationRuleOrder = 0 where certificationRuleOrder is null
	ALTER TABLE trngCertificationRule alter column certificationRuleOrder int NOT NULL
	ALTER TABLE trngCertificationRule ADD  CONSTRAINT DF_trngCertificationRule_certificationRuleOrder  DEFAULT 0 FOR certificationRuleOrder
END	
