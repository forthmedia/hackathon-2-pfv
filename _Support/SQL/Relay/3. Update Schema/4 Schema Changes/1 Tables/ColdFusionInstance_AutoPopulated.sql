/*
Create Tables to record Instances
*/

if not exists (select 1 from sysobjects where name = 'ColdFusionInstance_AutoPopulated') 
BEGIN
CREATE TABLE [dbo].[ColdFusionInstance_AutoPopulated] (
 [id] [int] IDENTITY (1, 1) NOT NULL ,
 [DatabaseIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [CFAppname] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [instanceIPAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [instancePort] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [instanceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [LastUpdated] [datetime] NULL ,
 [testsite] [int] NULL ,
 [jsessionid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 [active] [int] NOT NULL CONSTRAINT [DF_ColdFusionInstances_AutoPopulated_active] DEFAULT (1),
 [servername] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
 CONSTRAINT [PK_ColdFusionInstances_AutoPopulated] PRIMARY KEY  CLUSTERED 
 (
  [id]
 ) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
 	
END


GO
if not exists (select 1 from information_schema.columns where table_name = 'coldfusioninstance_autopopulated' and column_name = 'relaywareVersion') 
BEGIN
	Alter table coldfusioninstance_autopopulated add  relaywareVersion  varchar(100)
END

GO
if not exists (select 1 from information_schema.columns where table_name = 'coldfusioninstance_autopopulated' and column_name = 'databaseid') 
BEGIN
	Alter table coldfusioninstance_autopopulated add  databaseid int null
END
GO

if not exists (select 1 from information_schema.columns where table_name = 'coldfusioninstance_autopopulated' and column_name = 'allIPAddresses') 
BEGIN
	Alter table coldfusioninstance_autopopulated add  allIPAddresses varchar(100)
END
GO

/* 2014-04-01 GCC Increase size of column to allow longer instance names on production*/
alter table ColdFusionInstance_AutoPopulated alter column jsessionid varchar(200)

/* NJH 2015/07/29 - change length of allIPAddresses to accomodate a long list of IP addresses as was in the case for Mellanox that had a large number of vanity urls */
if exists (select 1 from information_schema.columns where table_name = 'coldfusioninstance_autopopulated' and column_name = 'allIPAddresses' and character_maximum_Length <> -1) 
	ALTER TABLE ColdfusionInstance_AutoPopulated
 		ALTER COLUMN allIPAddresses varchar(max)

