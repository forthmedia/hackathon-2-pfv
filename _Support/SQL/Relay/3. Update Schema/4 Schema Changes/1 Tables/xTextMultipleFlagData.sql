if not exists (select 1 from sysobjects where name='xTextMultipleFlagData' and type='u')
CREATE TABLE [dbo].[xTextMultipleFlagData](
	[rowID] [int] IDENTITY(1,1) NOT NULL,
	[flagID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[data] [nvarchar](100) NOT NULL,
	[sortOrder] [int] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
	[ArchivedBy] [int] NOT NULL,
	[Archived] [datetime] NOT NULL,
 CONSTRAINT [PK_xTextMultipleFlagData] PRIMARY KEY CLUSTERED 
(
	[flagID] ASC,
	[entityID] ASC,
	[data] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
