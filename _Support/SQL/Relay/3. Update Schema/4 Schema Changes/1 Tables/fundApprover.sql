if not exists (select * from information_schema.KEY_COLUMN_USAGE where constraint_name='UC_fundApprover_PersonCountryLevel') 
ALTER TABLE fundApprover
ADD CONSTRAINT UC_fundApprover_PersonCountryLevel UNIQUE (personId,countryId,fundApprovalLevelID)

