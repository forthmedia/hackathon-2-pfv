if not exists (select 1 from sysobjects where name='xDecimalFlagData' and type='u')
CREATE TABLE [dbo].[xDecimalFlagData](
	[entityID] [int] NOT NULL,
	[flagID] [int] NOT NULL,
	[data] [decimal](18, 2) NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
	[ArchivedBy] [int] NOT NULL,
	[Archived] [datetime] NOT NULL,
 CONSTRAINT [PK_xDecimalFlagData] PRIMARY KEY CLUSTERED 
(
	[entityID] ASC,
	[flagID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO