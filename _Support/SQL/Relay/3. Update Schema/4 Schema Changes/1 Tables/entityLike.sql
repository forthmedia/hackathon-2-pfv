if not exists (select 1 from sysobjects where name='entityLike' and type='u')
begin

CREATE TABLE [dbo].[entityLike](
	[entityTypeID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[personID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL
PRIMARY KEY CLUSTERED 
(
	[entityTypeID] ASC,
	[entityID] ASC,
	[personID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[entityLike] ADD  CONSTRAINT [DF_entityLike_created]  DEFAULT (getdate()) FOR [created]

ALTER TABLE [dbo].[entityLike] ADD  DEFAULT ((0)) FOR [createdBy]

end

GO