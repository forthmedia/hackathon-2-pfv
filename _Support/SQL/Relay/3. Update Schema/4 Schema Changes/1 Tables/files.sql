
/* make filename nvarchar */
IF EXISTS (select 1 from information_schema.COLUMNS where table_name= 'files'  and COLUMN_name = 'filename' and (character_Maximum_length  <200 or DATA_TYPE <> 'nvarchar'))
BEGIN
alter table files alter column filename nvarchar(200)
END

/* 
	2011/11/22	RMB		Added DeliveryMethod defalt set to 'Local' 
 	2011/12/09	RMB		Added CreatedBy 
 	2011/12/09	RMB		Added LastUpdatedBy
	2012/04/01	IH 		Added Index
*/

/* WAB 2014-12-02 wrapped this alter in an IF and in code to drop/recreate the fullText Index which depends upon it */
IF exists (select 1 from information_schema.columns where table_name = 'files' and column_name = 'name' and character_maximum_length  < 200) 
BEGIN
		exec dropAndRecreateDependentObjects 'files.name', 'dropfulltextindexes'
	begin tran
		exec dropAndRecreateDependentObjects 'files.name', 'drop'

			ALTER TABLE dbo.files alter column name nvarchar(200)
	
		exec dropAndRecreateDependentObjects 'files.name', 'recreate'
	commit
		exec dropAndRecreateDependentObjects 'files.name', 'recreatefulltextindexes'
END



IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
          WHERE TABLE_NAME = 'files' 
          AND  COLUMN_NAME = 'DeliveryMethod')

begin

ALTER TABLE dbo.files 
ADD DeliveryMethod nvarchar(50) NULL DEFAULT 'Local'

end
GO

UPDATE dbo.files SET DeliveryMethod = 'Local'
WHERE (DeliveryMethod IS NULL OR LTRIM(RTRIM(DeliveryMethod)) = '')
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
          WHERE TABLE_NAME = 'files' 
          AND  COLUMN_NAME = 'CreatedBy')

begin

ALTER TABLE dbo.files 
ADD CreatedBy int NULL

end
GO

UPDATE dbo.files SET CreatedBy = (SELECT UserGroupID FROM UserGroup WHERE PersonID = files.PersonID) 
WHERE (CreatedBy IS NULL OR LTRIM(RTRIM(CreatedBy)) = '')
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
          WHERE TABLE_NAME = 'files' 
          AND  COLUMN_NAME = 'LastUpdatedBy')

begin

ALTER TABLE dbo.files 
ADD LastUpdatedBy int NULL

end
GO

UPDATE dbo.files SET LastUpdatedBy = (SELECT UserGroupID FROM UserGroup WHERE PersonID = files.PersonID) 
WHERE (LastUpdatedBy IS NULL OR LTRIM(RTRIM(LastUpdatedBy)) = '')
GO

if not exists (SELECT 1 FROM sysindexes WHERE name = 'languageid')
CREATE NONCLUSTERED INDEX [languageid]
ON [dbo].[files] ([languageid],[fileFamilyID])
INCLUDE ([fileid]);
GO

/* added for Case 427049*/
IF  EXISTS (SELECT 1 FROM sysindexes WHERE name = N'Idx_FileFamilyID')
	drop index files.Idx_FileFamilyID 
	
CREATE NONCLUSTERED INDEX [Idx_FileFamilyID] ON [dbo].[files] 
([fileFamilyID])
INCLUDE ([fileid])
GO

/* NJH 2012/11/27 - remove the not used column 
if exists(select 1 from information_schema.columns where table_name='files' and column_name='notused')
alter table [files] drop column notused
GO*/

if exists(select 1 from information_schema.columns where table_name='files' and column_name='description')
	/* 2017-03-06 PPB RT-325 extend length of description */
	/* wrapped this alter in an IF and in code to drop/recreate the fullText Index which depends upon it */
	BEGIN
		exec dropAndRecreateDependentObjects 'files.description', 'dropfulltextindexes'
		begin tran
			exec dropAndRecreateDependentObjects 'files.description', 'drop'
	
				ALTER TABLE dbo.files alter column description nvarchar(max)
		
			exec dropAndRecreateDependentObjects 'files.description', 'recreate'
		commit
			exec dropAndRecreateDependentObjects 'files.description', 'recreatefulltextindexes'
	END
else
	BEGIN
		alter table [dbo].[files] add description nvarchar(max) null
	END
GO

if not exists(select 1 from information_schema.columns where table_name='files' and column_name='publishDate')
alter table [dbo].[files] add publishDate datetime null default(getDate())
GO




/* 2016-02-24		WAB 	PROD2015-291  Media Library Visibility.
 get rid of the hasRecordRights_x columns and replace with a single hasRecordRightsBitMask field and populate it

 */
if not exists (select * from information_schema.columns where table_name = 'files' and column_Name = 'hasRecordRightsBitMask')
BEGIN
	alter table files add hasRecordRightsBitMask int default 0  
	declare @SQL nvarchar(max)
	
	IF exists (select 1 from information_schema.tables where table_name = 'filesdel')
	BEGIN
		set @SQL  = 'alter table filesdel add hasRecordRightsBitMask int'
		exec (@SQL)
	END	

	set @SQL  = '
		update files set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
		alter table files alter column hasRecordRightsBitMask int not null
	'	
	exec (@SQL)

	IF exists (select 1 from information_schema.tables where table_name = 'filesdel')
	BEGIN
		set @SQL  = '
			update filesdel set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
			alter table filesdel alter column hasRecordRightsBitMask int not null
		'	
		exec (@SQL)
	END	



END	

if exists (select * from information_schema.columns where table_name = 'files' and column_Name = 'hasRecordRights_1')
	exec sp_dropcolumn 'files', 'hasRecordRights_1'
if exists (select * from information_schema.columns where table_name = 'files' and column_Name = 'hasRecordRights_2')
	exec sp_dropcolumn 'files', 'hasRecordRights_2'
if exists (select * from information_schema.columns where table_name = 'files' and column_Name = 'hasRecordRights_10')
	exec sp_dropcolumn 'files', 'hasRecordRights_10'



if not exists (select * from information_schema.columns where table_name = 'files' and column_Name = 'hasCountryScope_1')
	alter table files add hasCountryScope_1 bit default 0  
GO
update files set hasCountryScope_1 = 0 where hasCountryScope_1  is null
GO

/*  New columns are kept up to date by trigger, but need initial population with existing data */
update files set hasCountryScope_1 = case when cs.entityid is not null then 1 else 0 end 
from 
	files e 
		left join
	CountryScope cs on e.fileid = cs.entityid and entity = 'files' and ((cs.permission & 1) <> 0 )



if not exists (select 1 from information_schema.columns where table_name = 'files' and column_name = 'campaignID')
	alter table [files] add campaignID int null
GO

/* NJH changing field types from datetime to date as they are date fields */
if exists (select 1 from information_schema.columns where table_name='files' and column_name='triggerDate' and data_type='datetime')
alter table files alter column triggerDate date null


if exists (select 1 from information_schema.columns where table_name='files' and column_name='publishDate' and data_type='datetime')
begin
	begin tran
		exec dropAndRecreateDependentObjects 'files.publishDate', 'drop'
		alter table files alter column publishDate date null
		exec dropAndRecreateDependentObjects 'files.publishDate', 'recreate'
	commit
end


/*  WAB 2013-12-09 create auditing here rather than later, so that we can make an index on the Del table  */
exec generate_mrAudit @tablename='files',@mnemonic='F'

IF EXISTS (select * from ModEntityDef where TableName='files')
BEGIN
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'fileid'and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'filetypeid' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'filename' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'personid' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'name' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'languageid' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'fileFamilyID' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'fileSize' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'triggerType' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'sortOrder' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'familySortOrder' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'DeliveryMethod' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'CreatedBy' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'LastUpdatedBy' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'lastUpdatedByPerson' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'description' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='files' and FieldName = 'campaignID' and ApiActive=0

	update ModEntityDef set ApiName='fileid' where TableName ='files' and FieldName = 'fileid'and ApiName is null
	update ModEntityDef set ApiName='filetypeid' where TableName ='files' and FieldName = 'filetypeid' and ApiName is null
	update ModEntityDef set ApiName='filename' where TableName ='files' and FieldName = 'filename' and ApiName is null
	update ModEntityDef set ApiName='personid' where TableName ='files' and FieldName = 'personid' and ApiName is null
	update ModEntityDef set ApiName='name' where TableName ='files' and FieldName = 'name' and ApiName is null
	update ModEntityDef set ApiName='languageid' where TableName ='files' and FieldName = 'languageid' and ApiName is null
	update ModEntityDef set ApiName='fileFamilyID' where TableName ='files' and FieldName = 'fileFamilyID' and ApiName is null
	update ModEntityDef set ApiName='fileSize' where TableName ='files' and FieldName = 'fileSize' and ApiName is null
	update ModEntityDef set ApiName='triggerType' where TableName ='files' and FieldName = 'triggerType' and ApiName is null
	update ModEntityDef set ApiName='sortOrder' where TableName ='files' and FieldName = 'sortOrder' and ApiName is null
	update ModEntityDef set ApiName='familySortOrder' where TableName ='files' and FieldName = 'familySortOrder' and ApiName is null
	update ModEntityDef set ApiName='DeliveryMethod' where TableName ='files' and FieldName = 'DeliveryMethod' and ApiName is null
	update ModEntityDef set ApiName='CreatedBy' where TableName ='files' and FieldName = 'CreatedBy' and ApiName is null
	update ModEntityDef set ApiName='LastUpdatedBy' where TableName ='files' and FieldName = 'LastUpdatedBy' and ApiName is null
	update ModEntityDef set ApiName='lastUpdatedByPerson' where TableName ='files' and FieldName = 'lastUpdatedByPerson' and ApiName is null
	update ModEntityDef set ApiName='description' where TableName ='files' and FieldName = 'description' and ApiName is null
	update ModEntityDef set ApiName='campaignID' where TableName ='files' and FieldName = 'campaignID' and ApiName is null
	update ModEntityDef set ApiName = 'languageID' where FieldName='languageid' and ApiName is null
END


