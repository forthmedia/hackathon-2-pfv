if not exists (select 1 from information_schema.tables where table_name = 'SpecialisationRuleCompulsoryModule')
CREATE TABLE [dbo].[SpecialisationRuleCompulsoryModule](
	[SpecialisationRuleID] [int] NOT NULL,
	[moduleID] [int] NOT NULL,
	[created] [datetime] NOT NULL CONSTRAINT [DF_SpecialisationRuleCompulsoryModule_created]  DEFAULT (getdate()),
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL CONSTRAINT [DF_SpecialisationRuleCompulsoryModule_lastUpdated]  DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_SpecialisationRuleCompulsoryModule] PRIMARY KEY CLUSTERED 
(
	[SpecialisationRuleID] ASC,
	[moduleID] ASC
) 
) 

