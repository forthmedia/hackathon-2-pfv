/*
WAB 2012-01  Sprint 15&42 Comms
A table to store the result of sending a communication
*/

IF not exists (select 1 from sysobjects where name = 'commsendresult')
BEGIN

create table CommSendResult	(
	commSendResultID int identity(1,1), 
	commid int, 
	result nvarchar(max), 
	numberSent int, 
	numberNotSent int, 
	OK bit, 
	test bit, 
	createdby int, 
	created datetime default getdate(), 
	completed datetime )

END