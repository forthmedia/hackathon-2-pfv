if not exists (select * FROM  sysobjects where name='locatorMapType' and xtype='U') 
	begin
		CREATE TABLE [dbo].[locatorMapType](
			[mapTypeID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
			[mapTypeDescription] [varchar](50) NOT NULL
		)
	end

GO
	
if not exists (select top 1 1 FROM  locatorMapType) 
	insert into locatorMapType (mapTypeDescription) values ('Google Map - Single Address - PopUp')
