if not exists (select 1 from information_schema.columns where table_name = 'dateFlagData' and column_name = 'lastUpdatedByPerson') 
	alter table dateFlagData add lastUpdatedByPerson int 
GO

if not exists (select 1 from information_schema.columns where table_name='dateFlagData' and column_name='rowID')
alter table dateFlagData add rowID int identity not null
GO