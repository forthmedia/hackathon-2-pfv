/*

WAB 2015-12-01 PROD2015-290 Visibility.  Adding support for ANDs and NOTs
			
*/
IF NOT EXISTS (select 1 from sysobjects where name = 'recordRightsOperator')
BEGIN
	create table recordRightsOperator
	(
		  entityTypeID int not null 
		, recordid int not null
		, groupAnd Int not null default 0 
		, groupNot Int not null default 0
	)
END


IF NOT EXISTS (select 1 from sysindexes where name = 'recordRightsOperator_entityTypeID_recordid')
BEGIN
	create clustered index recordRightsOperator_entityTypeID_recordid on recordRightsOperator (entityTypeID, recordid)
END	

