/* CREATE TrainingProgram table */
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trngTrainingProgram]') AND type in (N'U'))
   BEGIN
	CREATE TABLE [dbo].[trngTrainingProgram] (
	[TrainingProgramID] int IDENTITY(1,1) NOT NULL  
	, [TrainingProgramTextID] varchar(100) NULL
	, [Created] datetime NULL
	, [CreatedBy] int NULL  
	, [LastUpdated] datetime NULL  
	, [LastUpdatedBy] int NULL  
	)


ALTER TABLE [dbo].[trngTrainingProgram] ADD CONSTRAINT [trngTrainingProgram_PK] PRIMARY KEY CLUSTERED (
[TrainingProgramID]
)
END
GO