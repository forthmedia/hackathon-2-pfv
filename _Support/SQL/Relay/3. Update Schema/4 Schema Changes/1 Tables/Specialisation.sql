/* Specialisation */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'Specialisation') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'Specialisation' AND column_name='TrainingProgramID') 
ALTER TABLE Specialisation add TrainingProgramID int NULL
GO

/* migrate specialisations */
exec migrateToEntityPhrases @phrasetextID = 'Title', @tablename = 'specialisation', @columnname = 'titlePhraseTextID'
GO
exec migrateToEntityPhrases @phrasetextID = 'Description', @tablename = 'specialisation', @columnname = 'descriptionPhraseTextID'
GO

exec generate_MRAudit @TableName = 'specialisation'