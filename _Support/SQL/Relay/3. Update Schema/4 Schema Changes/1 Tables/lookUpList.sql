
if not exists (select 1 from information_schema.columns where table_name='lookupList' and column_name='lookUpTextID')
alter table lookupList add lookUpTextID varchar(50) null
GO

update lookupList set lookupTextID = replace(replace(itemText,'-',''),' ','') where lookupTextID is null

alter table lookupList alter column lookUpTextID varchar(50) not null

begin tran
	exec dropAndRecreateDependentObjects 'lookupList.fieldname', 'drop'
		alter table lookupList alter column fieldname varchar(50) not null
	exec dropAndRecreateDependentObjects 'lookupList.fieldname', 'recreate'
commit


/* changing the lookupId column to be an identity 
WAB 2014-12-02  Changed the ordering of this file.  
	The rebuilding of the table loses the indexes
*/
if 0 = (select columnproperty(object_id('lookupList'),'lookupID','IsIdentity'))
begin
	CREATE TABLE [dbo].[tmp_LookupList](
		[LookupID] [int] identity (1,1) NOT NULL,
		[lookUpTextID] [varchar](50) NOT NULL,
		[FieldName] [varchar](50) NULL,
		[ItemText] [varchar](50) NULL,
		[ExtraInfo] [bit] NOT NULL,
		[Description] [varchar](50) NULL,
		[IsParent] [bit] NOT NULL,
		[IsDefault] [bit] NOT NULL,
		[InputType] [varchar](50) NULL,
		[IsLive] [bit] NOT NULL,
		[Locked] [bit] NOT NULL,
		[CreatedBy] [int] NULL,
		[Created] [datetime] NULL,
		[LastUpdated] [datetime] NULL,
		[LastUpdatedBy] [int] NULL

	 CONSTRAINT [LookupList_PK] PRIMARY KEY CLUSTERED 
	(
		[LookupID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	
	SET IDENTITY_INSERT dbo.tmp_LookupList ON


	IF EXISTS ( SELECT  1 FROM dbo.LookupList ) 
		INSERT  INTO dbo.tmp_LookupList (lookUpID,fieldName,itemText,extraInfo,description,isParent,isDefault,inputType,isLive,locked,createdBy,created,lastUpdated,lastUpdatedBy,lookupTextID)
				SELECT  lookUpID,fieldName,itemText,extraInfo,description,isParent,isDefault,inputType,isLive,locked,createdBy,created,lastUpdated,lastUpdatedBy,lookupTextID
				FROM    dbo.LookupList TABLOCKX


	SET IDENTITY_INSERT dbo.tmp_LookupList OFF


	DROP TABLE dbo.LookupList


	Exec sp_rename 'tmp_LookupList', 'LookupList'
end



IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LookupList]') AND name = N'FieldName')
BEGIN

	CREATE NONCLUSTERED INDEX [FieldName] ON [dbo].[LookupList] 
	(
		[FieldName] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LookupList]') AND name = N'IX_LookupList_fieldName_lookupTextID')
Begin
	CREATE UNIQUE NONCLUSTERED INDEX [IX_LookupList_fieldName_lookupTextID] ON [dbo].[LookupList] 
	(
		[FieldName] ASC,
		[lookUpTextID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO
	



IF NOT EXISTS (SELECT 1 FROM information_schema.columns where table_name='LookupList' and column_name='sortOrder')
BEGIN
	ALTER TABLE LookupList add sortOrder tinyint null
	ALTER TABLE [dbo].[LookupList] ADD  CONSTRAINT [DF_sort_order]  DEFAULT ((0)) FOR [sortOrder]
END
GO