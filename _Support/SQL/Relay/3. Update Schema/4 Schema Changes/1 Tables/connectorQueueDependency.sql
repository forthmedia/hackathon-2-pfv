
If not exists (select 1 from sysobjects where name='connectorQueueDependency' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorQueueDependency](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorQueueID] [int] NOT NULL,
		[dependentOnConnectorQueueID] [int] NOT NULL,
		[created] [datetime] NOT NULL,
	 CONSTRAINT [PK_connectorRelatedQueue] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorQueueDependency] ADD  CONSTRAINT [DF_connectorRelatedQueue_created]  DEFAULT (getdate()) FOR [created]
END