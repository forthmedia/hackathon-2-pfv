
alter table securityType alter column shortName varchar(20)
GO


/*
Get rid of menuid column 
WAB 2013/06/19 altered to try and make runnable more than once
*/

if exists (select 1 from information_schema.columns where table_name = 'securityType' and column_name = 'menuid')
BEGIN
	drop index securityType.MenuID
	
	EXEC sp_unbindefault "securityType.MenuID"

	alter table securityType drop column MenuID
END
	