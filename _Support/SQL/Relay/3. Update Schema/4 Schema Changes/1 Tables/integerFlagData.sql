if not exists (select 1 from information_schema.columns where table_name = 'integerFlagData' and column_name = 'lastUpdatedByPerson') 
	alter table integerFlagData add lastUpdatedByPerson int 
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IntegerFlagData]') AND name = N'FlagID')
DROP INDEX [FlagID] ON [dbo].[IntegerFlagData] 
GO
CREATE NONCLUSTERED INDEX [FlagID] ON [dbo].[IntegerFlagData] ([FlagID]) INCLUDE ([Data], [EntityID])
GO

if not exists (select 1 from information_schema.columns where table_name='integerFlagData' and column_name='rowID')
alter table integerFlagData add rowID int identity not null
GO
