/*
WAB 2016-10-20
This OOTB Custom entity was scripted with a del table but not with its auditing
And some systems don't have it at all
*/
IF exists (select 1 from sysobjects where name = 'businessPlan')
BEGIN
	exec generate_mrAudit @tablename='businessPlan'
END	
