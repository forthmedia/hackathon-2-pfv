IF object_id('SAMLIdentityProviderUsergroupControl') is null  
BEGIN
	Create Table [dbo].[SAMLIdentityProviderUsergroupControl](
		SAMLIdentityProviderId int NOT NULL FOREIGN KEY REFERENCES [dbo].[SAMLIdentityProvider] ([SAMLIdentityProviderId]) on delete cascade,
		usergroupID int NOT NULL FOREIGN KEY REFERENCES [dbo].[usergroup] ([usergroupID]) on delete cascade,
		permitsControl bit not null default 0,
		setAtPersonCreation bit not null default 0
		)
END