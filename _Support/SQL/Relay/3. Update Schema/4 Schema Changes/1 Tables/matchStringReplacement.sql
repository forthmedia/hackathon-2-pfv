/*
	NJH 2016/08/16 JIRA PROD2016-1190 - rename table so that it's a generic matching replacement table
	and populate it with values from charReplacement table as well.
*/

IF  NOT EXISTS (select 1 from information_schema.tables where table_name='MatchStringReplacement')
begin
	CREATE TABLE [dbo].[MatchStringReplacement](
	[ID] INT IDENTITY(1,1) NOT NULL,
	[stringToReplace] nvarchar(100) NOT NULL,
	[replacementString] nvarchar(100) NULL DEFAULT(''),
	[matchType] varchar(20) NULL,
	CONSTRAINT [PK_MatchStringReplacement] PRIMARY KEY CLUSTERED
		([ID] ASC)
	)
end
GO

if exists (select 1 from information_schema.tables where table_name='MatchNameReplacementOrg')
begin
	insert into matchStringReplacement (stringToReplace,replacementString,matchType)
	select o.stringToReplace,o.replacementString,'name'
	from matchNameReplacementOrg o
		left join matchStringReplacement m on o.stringToReplace = m.stringToReplace and o.replacementString = m.replacementString
	where
		m.ID is null
		
	drop table matchnameReplacementOrg
end

if exists (select 1 from information_schema.tables where table_name='charReplacement')
begin
	insert into matchStringReplacement (stringToReplace,replacementString,matchType)
	select charToReplace,replacementChar,'address'
	from charReplacement c
		left join matchStringReplacement m on c.charToReplace = m.stringToReplace and c.replacementChar = m.replacementString
	where
		m.ID is null
		
	drop table charReplacement
end