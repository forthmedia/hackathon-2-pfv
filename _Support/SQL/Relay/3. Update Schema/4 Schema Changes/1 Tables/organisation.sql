
IF EXISTS (select 1 from information_schema.COLUMNS where table_name= 'organisation'  and COLUMN_name = 'orgurl' and (character_Maximum_length  <500 OR DATA_TYPE <> 'nvarchar'))
BEGIN
alter table organisation alter column OrgURL nvarchar(500) null
alter table organisationDel alter column OrgURL nvarchar(500) null
END



/****** Object:  Index [crmOrgID]    Script Date: 03/02/2012 11:28:13 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organisation]') AND name = N'crmOrgID')
DROP INDEX [crmOrgID] ON [dbo].[Organisation] 
GO
/****** Object:  Index [crmOrgID]    Script Date: 03/02/2012 11:28:14 ******/
/*
CREATE NONCLUSTERED INDEX [crmOrgID] ON [dbo].[Organisation] 
(
	[crmOrgID] ASC
) ON [PRIMARY]
GO*/

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Organisation]') AND name = N'crmOrgID_unique')
BEGIN
	update Organisation set crmOrgID = null where crmOrgID = ''
	CREATE unique INDEX crmOrgID_unique ON Organisation (crmOrgID) WHERE crmOrgID IS NOT NULL ;
END	
GO

if not exists (select * from information_schema.columns where table_name='organisation' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].organisation add [lastUpdatedByPerson] int NULL

GO

if not exists (select * from information_schema.columns where table_name='organisation' and column_name='ParentOrgID')  
      alter TABLE [dbo].organisation add [ParentOrgID] int NULL

GO

if not exists(select 1 from information_schema.columns where table_name='organisation' and column_name='pictureURL')
alter table organisation add pictureURL varchar(250)
GO

/* unbind the organisationID default */
if exists (select 1 from sys.columns where object_id = OBJECT_ID('organisation') AND name = 'organisationID' and OBJECT_NAME([default_object_id]) = 'UW_ZeroDefault')
EXEC sp_unbindefault 'organisation.organisationID'


/* WAB 2014-01-28 CASE 438734
Create an index on matchname
Greatly improves performance on inserting new records 
*/ 
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[organisation]') AND name = N'organisation_matchname')
	CREATE INDEX organisation_matchname ON organisation
	(matchname)
GO

/* NJH 2016/0322 sugar 446863 - increase length of org name to 255 */
if not exists (select 1 from information_schema.columns where table_name='organisation' and column_name='organisationname' and character_maximum_length=255)
begin
	exec dropAndRecreateDependentObjects 'organisation.organisationname', 'dropfulltextindexes'
	begin tran
        exec dropAndRecreateDependentObjects 'organisation.organisationname', 'drop'
        alter table organisation alter column organisationname nvarchar(255) not null

        exec dropAndRecreateDependentObjects 'organisation.organisationname', 'recreate'
    commit
    exec dropAndRecreateDependentObjects 'organisation.organisationname', 'recreatefulltextindexes'
    exec generate_mrAuditDel @tablename='organisation'
end


/* NJH 2016/0322 sugar 446863 - increase length of org name to 255 */
if not exists (select 1 from information_schema.columns where table_name='organisation' and column_name='matchname' and character_maximum_length=255)
begin
	alter table organisation alter column matchname nvarchar(255) null
	exec generate_mrAuditDel @tablename='organisation'
end


/* NJH 2016/08/25 JIRA PROD2016-125 if organisationId is not an identity column */
declare @orgIDisIdentity int = 0
select  @orgIDisIdentity = columnproperty(object_id('organisation'),'organisationID','IsIdentity')
if (@orgIDisIdentity = 0)
begin
	exec convertBoundDefaultsToConstraints 'organisation'
	exec convertColumnToIdentity 'organisation', 'organisationid'
end