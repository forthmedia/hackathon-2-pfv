if not exists (select 1 from information_schema.columns where table_name = 'booleanFlagData' and column_name = 'lastUpdatedByPerson') 
	alter table booleanFlagData add lastUpdatedByPerson int 
GO

IF NOT EXISTS (select * from sysindexes where id=object_id('BooleanFlagData') and name='idx_flagid')
CREATE NONCLUSTERED INDEX [idx_flagid]
ON [dbo].[BooleanFlagData] ([FlagID])
INCLUDE ([EntityID],[Created])
GO

if not exists (select 1 from information_schema.columns where table_name='booleanFlagData' and column_name='rowID')
alter table booleanFlagData add rowID int identity not null
GO

IF NOT EXISTS (select 1 from sysindexes where id=object_id('BooleanFlagData') and name='idx_booleanFlagData_flagId_lastUpdated_include_entityID')
CREATE NONCLUSTERED INDEX [idx_booleanFlagData_flagId_lastUpdated_include_entityID]
ON [dbo].[BooleanFlagData] ([FlagID],[LastUpdated])
INCLUDE ([EntityID])
GO

/* Case 449722 - performance fix - adding missing index on BooleanFlagData for just entityID column */
IF NOT EXISTS (select 1 from sysindexes where id=object_id('BooleanFlagData') and name='idx_booleanFlagData_entityID')
CREATE NONCLUSTERED INDEX [idx_booleanFlagData_entityID] ON [dbo].[BooleanFlagData]
(
    [EntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO