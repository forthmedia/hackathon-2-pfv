IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[apiSession]') AND type in (N'U'))
CREATE TABLE [dbo].[apiSession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NOT NULL,
	[sessionID] [varchar](50) NOT NULL,
	[created] [datetime] NOT NULL,
	[expired] [datetime] NULL,
 CONSTRAINT [PK_apiSession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_apiSession_created]') AND type = 'D')
ALTER TABLE [dbo].[apiSession] ADD  CONSTRAINT [DF_apiSession_created]  DEFAULT (getdate()) FOR [created]
GO


