/* NJH 2016/03/10 - JIRA PROD2016-472 Synching Attachments */
/* table used by the connector to store the binary copy of the related file. Ideally, want to upgrade to Sql Server 2012 which then has fileTables which we could take advantage of,
	but have gone down this route for now */
	
if not exists (select 1 from information_schema.tables where table_name='connector_fileBody')
begin
	CREATE TABLE [dbo].[connector_fileBody](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[fileID] [int] NOT NULL,
		[body] [nvarchar](max) NOT NULL,
	 CONSTRAINT [PK_connector_fileBody] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
end

GO