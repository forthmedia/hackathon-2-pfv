/* Add whole table for customers without existing CourseCounts table */
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseCounts]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[CourseCounts](
		[courseCountID] [int] IDENTITY(1,1) NOT NULL,
		[personID] [int] NOT NULL,
		[ipAdd] [varchar](35) NULL,
		[moduleLaunchedID] [int] NOT NULL,
		[visitId] [int] NOT NULL,
		[sessionID] [varchar](75) NULL,
		[dateCreated] [datetime] NOT NULL,
		[browserLanguage] [nvarchar](15) NULL,
		[fileID] [int] NULL,
		[languageID] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[courseCountID] ASC
	)
	) ON [PRIMARY]
END

/* Courseware access tracking lifted from PS code with new columns added according to Veronica's requirements */
/* Add new column for customers with old CourseCounts table */
IF 	NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'CourseCounts' AND column_name='fileID') 
BEGIN
	ALTER TABLE CourseCounts add fileID int NULL
END

/* Add new column for customers with old CourseCounts table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'CourseCounts' AND column_name='languageID') 
BEGIN	
	ALTER TABLE CourseCounts add languageID int NULL
END

