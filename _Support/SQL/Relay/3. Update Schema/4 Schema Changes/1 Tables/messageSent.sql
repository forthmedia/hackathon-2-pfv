if not exists (select 1 from sysobjects where name='messageSent' and type='u')
begin

CREATE TABLE [dbo].[messageSent](
	[messageSentID] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[text] [nvarchar](250) NULL,
	[url] [nvarchar](150) NULL,
	[image] [varchar](100) NULL,
	[personID] [int] NOT NULL,
	[created] [datetime] NOT NULL
 CONSTRAINT [PK_messageSent] PRIMARY KEY CLUSTERED 
(
	[messageSentID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[messageSent] ADD  CONSTRAINT [DF_messageSent_created]  DEFAULT (getdate()) FOR [created]

End
GO


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[messageSent]') AND name = N'messageSent_personID_idx')
CREATE NONCLUSTERED INDEX [messageSent_personID_idx] ON [dbo].[messageSent] 
(
	[personID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* message URL can be fairly large due to merge fields e.g. in business plan approval.*/
ALTER TABLE [dbo].messageSent
ALTER COLUMN url [nvarchar](1000) NULL

/*2016-08-05 GCC Issue with mail sent body being bigger than 250 characters for dealRegApproved email sent in SF opp post processing*/
ALTER TABLE [dbo].messageSent 
ALTER COLUMN [text] nvarchar(max)

