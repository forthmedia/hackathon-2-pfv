if not exists (select 1 from information_schema.tables where table_name = 'ProductModel')
CREATE TABLE [dbo].[ProductModel](
	[ProductModelID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_ProductModel] PRIMARY KEY CLUSTERED 
(
	[ProductModelID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO