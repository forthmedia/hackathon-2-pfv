
if not exists (select 1 from information_schema.tables where table_name='EntityName')
Begin
	CREATE TABLE [dbo].[EntityName](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[entityID] [int] NOT NULL,
		[entityTypeID] [int] NOT NULL,
		[name] [nvarchar](500) NULL,
		[crmID] [varchar](50) NULL,
		[deleted] [bit] NOT NULL,
	 CONSTRAINT [PK_EntityName] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
End
GO

IF  NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntityName_schemaTableBase]') AND parent_object_id = OBJECT_ID(N'[dbo].[EntityName]'))
BEGIN
ALTER TABLE [dbo].[EntityName]  WITH CHECK ADD  CONSTRAINT [FK_EntityName_schemaTableBase] FOREIGN KEY([entityTypeID])
REFERENCES [dbo].[schemaTableBase] ([entityTypeID])
END
GO

ALTER TABLE [dbo].[EntityName] CHECK CONSTRAINT [FK_EntityName_schemaTableBase]
GO


IF  NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[entityName]') AND name = N'idx_entityName_unique_entityTypeID_entityID')
CREATE UNIQUE NONCLUSTERED INDEX [idx_entityName_unique_entityTypeID_entityID] ON [dbo].[EntityName] 
(
	[entityID] ASC,
	[entityTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF  NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[entityName]') AND name = N'idx_EntityName_entityTypeID_crmID_inc_entityID_name')
CREATE NONCLUSTERED INDEX [idx_EntityName_entityTypeID_crmID_inc_entityID_name]
ON [dbo].[EntityName] ([entityTypeID],[crmID])
INCLUDE ([entityID],[name])
GO

/* we should in theory have a unique index on the crmId column
	However, the pricebook table does not have a unique index, as a crmPricebookId can be across multiple pricebooks, as it can be for multiple countries...
	Not sure what to do with this....
*/
declare @entityTypeID int
select @entityTypeID = entityTypeID from schemaTable where entityName='pricebook'
declare @sqlCmd nvarchar(max)
set @sqlCmd='CREATE unique INDEX idx_entityName_unique_crmID ON entityName (crmID) WHERE crmID IS NOT NULL and entityTypeID!='+cast(@entityTypeID as varchar(10))

IF  NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[entityName]') AND name = N'idx_entityName_unique_crmID')
exec(@sqlCmd)