if not exists (select * FROM  sysobjects where name='LocatorDef' and xtype='U') 
	begin
		CREATE TABLE dbo.LocatorDef
			(
				LocatorID int NOT NULL IDENTITY (1, 1),
				[Name] varchar(100) NOT NULL default(''),
				CountryGroupID int NOT NULL,
				RequiredFlagList varchar(500) NULL Default('locator_yes'),
				DisplayFlagList varchar(500) NULL,
				PrimaryFlagList varchar(500) NULL,
				PartnerFlagList varchar(500) NULL,
				PartnerFlagGroupList varchar(500) NULL,
				GroupPartnerFlagGroupList varchar(500) NULL,
				OrderbyFlagOverride varchar(255) NULL,
				NumResultsList varchar(50) NOT NULL Default('10,20,50,100'),
				DistanceLengths varchar(50) NOT NULL Default('10,20,50,100'),
				ContactReseller bit NOT NULL Default(0),
				ClientContactEmail varchar(50) NULL,
				UseCompany bit NOT NULL Default(0),
				UseHasWebsite bit NOT NULL Default(0),
				UseProducts bit NOT NULL Default(0),
				MapTypeID int NOT NULL Default(0),
				Live bit NOT NULL Default(1),
				CreatedBy Int NOT NULL Default(0),
				Created datetime NOT NULL DEFAULT (getdate()),
				LastUpdatedBy Int NOT NULL Default(0),
				LastUpdated datetime NOT NULL DEFAULT (getdate())

			)  ON [PRIMARY]
	end
else
	begin
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='RequiredFlagList')  
		      alter TABLE [dbo].locatorDef add requiredFlagList varchar(500) NULL
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='PrimaryFlagList')  
		      alter TABLE [dbo].locatorDef add primaryFlagList varchar(500) NULL
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='OrderByFlagOverride')  
		      alter TABLE [dbo].locatorDef add OrderbyFlagOverride varchar(500) NULL
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='DisplayFlagList')  
		      alter TABLE [dbo].locatorDef add DisplayFlagList varchar(500) NULL
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseHasWebsite')  
		      alter TABLE [dbo].locatorDef add UseHasWebsite Int NOT NULL Default(0)
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='GroupPartnerFlagGroupList')  
		      alter TABLE [dbo].locatorDef add GroupPartnerFlagGroupList varchar(500) NULL
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='NumResultsList')  
		      alter TABLE [dbo].locatorDef add NumResultsList varchar(50) NOT NULL Default('10,20,50,100')

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='DistanceLengths')  
		      alter TABLE [dbo].locatorDef add DistanceLengths varchar(50) NOT NULL Default('10,20,50,100')

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='PartnerFlagList' and data_type='varchar' and character_maximum_length=500)
			alter TABLE [dbo].locatorDef ALTER COLUMN PartnerFlagList varchar(500) NULL
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='PartnerFlagGroupList' and data_type='varchar' and character_maximum_length=500)
			alter TABLE [dbo].locatorDef ALTER COLUMN PartnerFlagGroupList varchar(500) NULL
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='DisplayFlagList' and data_type='varchar' and character_maximum_length=500)
			alter TABLE [dbo].locatorDef ALTER COLUMN DisplayFlagList varchar(500) NULL

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='ContactReseller')
			alter TABLE [dbo].locatorDef ADD ContactReseller bit NOT NULL Default(0)

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='CreatedBy')  
		      alter TABLE [dbo].locatorDef add CreatedBy Int NOT NULL Default(0)
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='Created')  
		      alter TABLE [dbo].locatorDef add Created datetime NOT NULL DEFAULT (getdate())
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='LastUpdatedBy')  
		      alter TABLE [dbo].locatorDef add LastUpdatedBy Int NOT NULL Default(0)
		
		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='LastUpdated')  
		      alter TABLE [dbo].locatorDef add LastUpdated datetime NOT NULL DEFAULT (getdate())

		if not exists (select * from information_schema.columns where table_name like 'locatordef' and column_name='Name')
			alter table locatordef add [Name] varchar (100) NOT NULL default('')

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseCompany')  
		      alter TABLE [dbo].locatorDef add UseCompany bit NOT NULL Default(0)

		if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='Live')  
		      alter TABLE [dbo].locatorDef add Live bit NOT NULL Default(1)
		
		--if regression fields exist make them nullable to enable xmleditor to work for new defs.
		if exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseCounty' and IS_NULLABLE!='YES')  
			alter table locatordef alter column UseCounty bit null
		if exists (select * from information_schema.columns where table_name='locatorDef' and column_name='Feedback' and IS_NULLABLE!='YES')  
			alter table locatordef alter column Feedback bit null
		if exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseTown' and IS_NULLABLE!='YES')  
			alter table locatordef alter column UseTown bit null
		if exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseZip' and IS_NULLABLE!='YES')  
			alter table locatordef alter column UseZip bit null
	end

/* START: 2011-10-14 NYB LHID7283 - added: */
if not exists (select c.COLUMN_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,INFORMATION_SCHEMA.KEY_COLUMN_USAGE c where pk.TABLE_NAME = 'LocatorDef' and	CONSTRAINT_TYPE = 'PRIMARY KEY' and	c.TABLE_NAME = pk.TABLE_NAME and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME)
	ALTER TABLE LocatorDef ADD PRIMARY KEY (LocatorID); 
/* END: 2011-10-14 NYB LHID7283 */


GO

if exists (select * from information_schema.columns where table_name like 'locatordef' and column_name='CountryGroupID')
	begin
		if exists (select * from information_schema.columns where table_name like 'locatordef' and column_name='CountryGroupID' and IS_NULLABLE='NO')
			begin
				ALTER TABLE locatordef ALTER COLUMN CountryGroupID int NULL
				if exists (select * from locatordef where [Name] is NULL)
					begin
						update locatordef set [Name]=c.countrydescription from locatordef l inner join country c on l.countrygroupid=c.countryid where [Name] is NULL
					end
			end
	end

GO

if not exists (select top 1 1 from locatordef)
	begin
		insert into locatordef ([Name],RequiredFlagList,NumResultsList,DistanceLengths) values ('Default','locator_yes','10,20,50,100','10,20,50,100')

		insert into CountryScope (countryid,entity,permission,entitytypeid,entityid)
		select countryid,'LocatorDef',1,
		(select entitytypeid from schematable where entityName='LOCATORDEF') as entitytypeid,
		(select top 1 locatorid from locatordef order by locatorid) as entityid
		from country where isocode is not null 
			
	end

GO

if exists (select * from locatordef where [Name] is NULL or [Name] = '')
	update locatordef set [Name]='LocatorDefition'+cast(LocatorID as varchar(100)) where [Name] is NULL or [Name] = ''
	
/* NJH 2013/04/03  - add new locator fields for locator V5.. the idea is that we get rid of the other ones once everyone is using V5*/
if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='UseGeo')  
	alter table [dbo].locatorDef add useGeo bit not null Default(1)
	
if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='active')  
	alter table [dbo].locatorDef add active bit not null Default(1)
	
if not exists (select * from information_schema.columns where table_name='locatorDef' and column_name='searchCriteriaList')  
	alter table [dbo].locatorDef add searchCriteriaList varchar(1000) null

/* add a default to the live column if a default does not currently exist */	
IF  NOT EXISTS (
select c.name from sys.all_columns a 
	inner join sys.tables b on a.object_id = b.object_id 
	inner join sys.default_constraints c on a.default_object_id = c.object_id
where b.name='locatordef'
and a.name = 'live')
ALTER TABLE [dbo].[locatorDef] ADD  CONSTRAINT [DF_locatorDef_live]  DEFAULT (1) FOR [live]
GO