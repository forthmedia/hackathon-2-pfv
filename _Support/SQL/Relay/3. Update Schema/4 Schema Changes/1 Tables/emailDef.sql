
/* NJH 2013/06/11 - add message title, text, image and url */
if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'messageTitle_defaultTranslation') 
	alter table emailDef add messageTitle_defaultTranslation nvarchar(200) 
ELSE
	alter table emailDef ALTER column messageTitle_defaultTranslation nvarchar(200) 
GO

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'messageText_defaultTranslation') 
	alter table emailDef add messageText_defaultTranslation nvarchar(500) 
ELSE
	alter table emailDef ALTER column messageText_defaultTranslation nvarchar(500) 
GO

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'messageUrl') 
	alter table emailDef add messageUrl nvarchar(150) 
GO

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'messageImage') 
	alter table emailDef add messageImage varchar(100) 
GO

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'sendMessage') 
	alter table emailDef add sendMessage bit default 0 not null 
GO

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'moduleTextID') 
	alter table emailDef add moduleTextID varchar(50) null 
GO

/* 
	WAB 2013-10-21 (During CASE 432365) Tidy up emailDef table by removing columns subjectPhraseTextID and bodyPhraseTextID which are no longer used
	First delete all phrases associated with these columns 
*/

IF EXISTS (select 1 from information_schema.columns where table_name = 'emailDef' and column_Name = 'subjectPhraseTextID')
	BEGIN

		-- WAB 2013-11-06 run migrateToEntityPhrases again mainly to make sure that the _defaultTranslation columns get created, for some reason weren't there even after re-baselining
		exec migrateToEntityPhrases @phrasetextID = 'Subject', @tablename = 'emailDef', @columnname = 'subjectPhraseTextID'
		exec migrateToEntityPhrases @phrasetextID = 'Body', @tablename = 'emailDef', @columnname = 'bodyPhraseTextID'


		/* WAB 2014-10-13 converted to use dynamic SQL so does not throw an (admittedly non fatal) error during upgrade when run for the second time (when columns no longer exist)*/
		declare @sql nvarchar(max) 
		SET @sql = '

		SELECT phraseID 
 		INTO #tempDeletePhrases
		FROM		
			phraselist pl 
				inner join 
			emaildef on  (subjectPhraseTextID = pl.phraseTextID or bodyPhraseTextID = pl.phraseTextID) and pl.entityTypeID = 0 
		
	
		DELETE 
			phrases
		FROM 
			phrases p
				inner join
			#tempDeletePhrases del on p.phraseID = del.phraseID
		

		DELETE 
			phraseList
		FROM 
			phraseList p
				inner join
			#tempDeletePhrases del on p.phraseID = del.phraseID
	
		DROP TABLE #tempDeletePhrases
	
		ALTER TABLE emailDEF DROP COLUMN subjectPhraseTextID
		ALTER TABLE emailDEF DROP COLUMN bodyPhraseTextID
		'
		
		exec (@sql)
		
	END 

GO

/*
WAB 2013-12-18 changed this field to be called Name rather than emailTitle (before release so no update required
Modified query for updating the null Names
Awaiting some standard names 
*/

if not exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'Name') 	
	alter table emailDef add Name nvarchar(100) null	

GO

update emailDef set name='Certification Cancelled' where emailTextID='TrngCertificationCancelled' and name is null
update emailDef set name='Certification Expired' where emailTextID='TrngCertificationExpired' and name is null
update emailDef set name='Certification Passed' where emailTextID='TrngCertificationPassed' and name is null
update emailDef set name='Certification Registration' where emailTextID='TrngCertificationRegistered' and name is null
update emailDef set name='Certification Rules Added' where emailTextID='TrngNewCertificationRuleAdded' and name is null
update emailDef set name='Certification Update Required' where emailTextID='TrngCertificationUpdateRequired' and name is null
update emailDef set name='Deal Registration Approved' where emailTextID='dealApprovedEmail' and name is null
update emailDef set name='Deal Registration Approved Notify Account Manager' where emailTextID='dealAccountManagerApproval' and name is null
update emailDef set name='Deal Registration Declined' where emailTextID='dealDeclinedEmail' and name is null
update emailDef set name='Fund Budget Details' where emailTextID='MDFBudgetAuthorised' and name is null
update emailDef set name='Fund Claim Cancelled' where emailTextID='MDFClaimCancelled' and name is null
update emailDef set name='Fund Expired' where emailTextID='MDFFundsExpired' and name is null
update emailDef set name='Fund Expired - No PoP Received' where emailTextID='MDFFundsExpiredProofsNR' and name is null
update emailDef set name='Fund Info Required' where emailTextID='MDFAddInfoRequired' and name is null
update emailDef set name='Fund Paid Tier 1' where emailTextID='MDFPaidTier1' and name is null
update emailDef set name='Fund Payment Approved' where emailTextID='MDFPaymentApproved_Admin' and name is null
update emailDef set name='Fund Payment Confirmation' where emailTextID='MDFPaymentConf' and name is null
update emailDef set name='Fund PoP Initial Reminder' where emailTextID='MDFProofsReminderInitial' and name is null
update emailDef set name='Fund PoP Received' where emailTextID='MDFPOPReceived' and name is null
update emailDef set name='Fund Request Approved' where emailTextID='MDFReqApproved' and name is null
update emailDef set name='Fund Request Approved Level 1' where emailTextID='MDFReqPartApprovedLevelOne' and name is null
update emailDef set name='Fund Request Approved Level 2' where emailTextID='MDFReqPartApprovedLevelTwo' and name is null
update emailDef set name='Fund Request Confirmation' where emailTextID='MDFReqRctConf' and name is null
update emailDef set name='Fund Request Incomplete' where emailTextID='MDFReqQueried' and name is null
update emailDef set name='Fund Request Notification' where emailTextID='MDFReqRctAlert' and name is null
update emailDef set name='Fund Request Rejected' where emailTextID='MDFReqRejected' and name is null
update emailDef set name='Incentive Registration' where emailTextID='incentiveRegistrationEmail' and name is null
update emailDef set name='Incentive User Name' where emailTextID='incentiveUserNameEmail' and name is null
update emailDef set name='Incentive User Name Link' where emailTextID='incentiveUserNameLinkEmail' and name is null
update emailDef set name='Invite Colleague' where emailTextID='sendToFriendEmail' and name is null
update emailDef set name='Locator New Lead Notification' where emailTextID='LocatorPartnerLead' and name is null
update emailDef set name='Action Created' where emailTextID='ActionsEmail' and name is null
update emailDef set name='Opportunity Created' where emailTextID='OpportunityEmail' and name is null
update emailDef set name='Opportunity Created Notify Account Manager' where emailTextID='oppAccountManagerNotification' and name is null
update emailDef set name='Opportunity Renewal Created' where emailTextID='AssetRenewal' and name is null
update emailDef set name='Opportunity Updated Notify Account Manager' where emailTextID='emailAccMngrOppStageChange' and name is null
update emailDef set name='Order Approval Notify Partner' where emailTextID='partnerOrderApproval' and name is null
update emailDef set name='Order Confirmation' where emailTextID='OrderConfirmationEmail' and name is null
update emailDef set name='Quiz Failed' where emailTextID='eLearningQuizFailed' and name is null
update emailDef set name='Quiz Passed' where emailTextID='eLearningQuizPassed' and name is null
update emailDef set name='Quiz Passed with Rewards' where emailTextID='eLearningPassEmailRewards' and name is null
update emailDef set name='Registration Confirmed' where emailTextID='RegistrationConfirmation' and name is null
update emailDef set name='Registration New Person' where emailTextID='RegistrationNewPersonNotification' and name is null
update emailDef set name='Registration Notify Primary Contact' where emailTextID='RegistrationNotificationForPrimaryContact' and name is null
update emailDef set name='Registration Organization Notification' where emailTextID='RegistrationNewOrgNotification' and name is null
update emailDef set name='Registration Rejected Organization' where emailTextID='RegistrationRejectedEmail' and name is null
update emailDef set name='Registration Rejected Person' where emailTextID='PerRejectedEmail' and name is null
update emailDef set name='Resend Portal Password' where emailTextID='ResendPasswordEmail' and name is null
update emailDef set name='Resend Relayware Password' where emailTextID='SendInternalUserPassword' and name is null
update emailDef set name='Specialization Achieved' where emailTextID='TrngSpecialisationAchieved' and name is null
update emailDef set name='Specialization Expired' where emailTextID='TrngSpecialisationExpired' and name is null
update emailDef set name='Training Order Placed Confirmation' where emailTextID='elearningOrderPlaced' and name is null
	
	/*
	Need to add the default names of standard emails
	This is a placeholder script
	*/
		declare @temp AS Table (emailTextID varchar(100), name nvarchar (100))
		
		insert into @temp 

		select 'Test','This is a Test Email'
		Union  
		select 'TestEmail','This is another Test Email'
		
		update 
			emailDef
		set 
			name = t.name
		from 
			emailDef e
				inner join
			@temp t on t.emailtextID = e.emailTextID
		where e.name is null



 	declare @length int = 100
 
 	update emailDef  		
 	set Name = 	
 		
 	emailTextID  +  case when phraseText is not null and len (emailTextID + convert(varchar(max),phraseText)) <@length - 3 then ' (' + convert(varchar(max),phraseText) + ')' else '' end
	from 
		emailDef e 
			left join 
		vphrases p on p.entityid = e.emailDefID and p.phraseTextID = 'subject' and p.entityTypeID = (select entityTypeID from schemaTable where entityname = 'emaildef') and countryid = 0 and defaultforthiscountry = 1 
	where e.name is null
GO

-- 13/10/2015 DAN PROD2015-160 - unique index for emailTextID
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[emailDef]') AND name = N'emailTextID_unique')
BEGIN
    CREATE unique INDEX emailTextID_unique ON emailDef (emailTextID) WHERE emailTextID IS NOT NULL ;
END 
GO

-- allow for more email body content
if exists (select 1 from information_schema.columns where table_name = 'emailDef' and column_name = 'Body_defaultTranslation') 
    alter table emailDef ALTER column Body_defaultTranslation nvarchar(max) 
GO
