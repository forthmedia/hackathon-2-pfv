/*
NJH 2015/12/18 - renamed to opportunityProduct - oppProducts now a view

if not exists (select * from information_schema.columns where table_name='oppProducts' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].oppProducts add [lastUpdatedByPerson] int NULL

/* add default for productOrGroup column*/
IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_oppProducts_ProductORGroup]') AND type = 'D')
	ALTER TABLE [dbo].[oppProducts] ADD  CONSTRAINT [DF_oppProducts_ProductORGroup]  DEFAULT ('P') FOR [ProductORGroup]
*/