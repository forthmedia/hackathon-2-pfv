IF object_id('OAuth2Server') is null  
BEGIN
	CREATE TABLE OAuth2Server (
	serverName nvarchar(256) NOT NULL,
	clientIdentifier nvarchar(256) CONSTRAINT PK_OAuth2Server PRIMARY KEY NOT NULL,
	hashedClientSecret nvarchar(512) NOT NULL,
	authoriseURL nvarchar(256) NOT NULL,
	tokenURL nvarchar(256) NOT NULL,
	dataURL nvarchar(256) NOT NULL,
	scope nvarchar(256) NOT NULL,
	tokenPrefix nvarchar(256) NOT NULL,
	serverUID nvarchar(256) NOT NULL,
	relaywareUID nvarchar(256) NOT NULL,
	authorisedSites nvarchar(256) NOT NULL,
	created date CONSTRAINT DF_OAuth2ServerCreated DEFAULT getDate(),
	active bit NOT NULL,

	autocreate bit NOT NULL,
	autoregister bit NOT NULL,
	selectLocOrg bit NOT NULL,
	autocreate_selectOrgId int null,
	autocreate_selectLocId int null,
	detectedLoc_RelaywareField nvarchar(max),
	detectedLoc_ForeignField nvarchar(max),

	onCreationPortalPersonApprovalStatus int DEFAULT 0,
	usergroupCountriesAtPersonCreation nvarchar(max),
	usergroupsAtPersonCreation nvarchar(max),
	permittedToSetUsergroups bit NOT NULL,
	permittedToSetCountryRights bit NOT NULL,
	assertionAttributeToControlUsergroups nvarchar(256),
	usergroupsPermittedToControl nvarchar(256),
	assertionAttributeToControlCountryRights nvarchar(256)

);
END

/* Selecting all usergroups surpasses original size of 512 */
DECLARE @usergroupsLength int = 
(
SELECT 
	Character_Maximum_Length 
FROM 
	INFORMATION_SCHEMA.COLUMNS 
WHERE
	table_name='OAuth2Server'
	AND
	column_name='usergroupCountriesAtPersonCreation'
)
IF @usergroupsLength != -1
BEGIN
	ALTER TABLE 
		OAuth2Server 
	ALTER COLUMN 
		usergroupCountriesAtPersonCreation nvarchar(max)
END