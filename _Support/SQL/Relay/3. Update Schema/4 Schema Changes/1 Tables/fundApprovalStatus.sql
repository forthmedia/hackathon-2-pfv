
-- isApproved should be true for all statuses of ApprovalLevel1 or above (typically all except Pending & Rejected) 
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundApprovalStatus' AND column_name='isApproved') 
ALTER TABLE fundApprovalStatus add isApproved bit default 0 NOT NULL
  
-- isFullyApproved should be true for all statuses of Fully Approved or above (typically Approved,ClaimApproved,FinanceApproved)
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundApprovalStatus' AND column_name='isFullyApproved') 
ALTER TABLE fundApprovalStatus add isFullyApproved bit default 0 NOT NULL

-- if we add a new status this should be nullable; it is an old column but is retained until MDF V1 is dumped
ALTER TABLE fundApprovalStatus alter column fundApprovalStatus varchar(50) NULL

declare @fundApprovalStatusTempVar as int
set @fundApprovalStatusTempVar = 0

if exists (select * from fundApprovalStatus where fundStatusTextID is null)
	begin
		update fundApprovalStatus set fundStatusTextID=replace(fundApprovalStatus,' ','') 
		set @fundApprovalStatusTempVar = 1
	end

if @fundApprovalStatusTempVar = 1
	alter table fundApprovalStatus alter column fundStatusTextID varchar(50) not null

/* NJH 2013/03/06 case 433562 - rename columns so that they follow standard naming convention 
WAB 2014-1013 script wasn't running well on an upgrade - especially if lastUpdatedBy/lastupdated columns already existed but not populated.  
Uncommented a bit of code which had been commented out and made use of new sp_dropcolumn to drop a column with a default and used some dynamic sql to prevent compile errors when run for a second time  
*/
if exists(select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='updatedBy')
and exists (select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='lastupdatedBy')
begin
	declare @sql nvarchar(max) = 'update fundApprovalStatus set lastUpdatedBy = updatedBy where lastUpdatedBy is null'
	exec (@sql)
	exec sp_dropcolumn 'fundApprovalStatus','updatedBy'
end
else 
if exists(select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='updatedBy')
begin
	exec sp_RENAME 'fundApprovalStatus.updatedBy' , 'lastUpdatedBy', 'COLUMN';
end
GO

if exists(select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='updated')
and exists (select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='lastupdated')
begin
	declare @sql nvarchar(max) = 'update fundApprovalStatus set lastUpdated = updated where lastUpdated is null'
	exec (@sql)	
	exec sp_dropcolumn 'fundApprovalStatus','updated'
end
else
if exists(select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='updated')
begin
exec sp_RENAME 'fundApprovalStatus.updated' , 'lastUpdated', 'COLUMN';
end
GO

-- the values in here are now in phrases...
if exists (select 1 from information_schema.columns where table_name='fundApprovalStatus' and column_name='externalStatusText')
alter table fundApprovalStatus drop column externalStatusText