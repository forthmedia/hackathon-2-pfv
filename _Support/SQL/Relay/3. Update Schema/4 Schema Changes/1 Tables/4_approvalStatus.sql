IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[approvalStatus]') AND type in (N'U'))

	CREATE TABLE dbo.approvalStatus
		(
		approvalStatusID int NOT NULL 
			IDENTITY (1, 1) 
			CONSTRAINT [PK_approvalStatusID] PRIMARY KEY CLUSTERED,
		approvalEngineID int NOT NULL 
			FOREIGN KEY REFERENCES [dbo].[approvalEngine] ([approvalEngineID]),
		currentLevel int NULL,
		approvalStatusTypeID int NOT NULL 
			FOREIGN KEY REFERENCES [dbo].[approvalStatusType] ([approvalStatusTypeID]),
		entityID int NOT NULL,
		approvalEngineApproverID int NOT NULL,
		reason nvarchar(500) NULL,
		[Created] [datetime] NOT NULL DEFAULT (getdate())
		)  ON [PRIMARY]

GO