
/* add pricebook Entry table */
if not exists (select 1 from information_schema.tables where table_name = 'PricebookEntry')
CREATE TABLE [dbo].[PricebookEntry](
	[pricebookEntryID] [int] IDENTITY(1,1) NOT NULL,
	[pricebookID] [int] NOT NULL,
	[productID] [int] NOT NULL,
	[unitPrice] [decimal](18, 2) NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_PricebookEntry_active]  DEFAULT (1),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_PricebookEntry_deleted]  DEFAULT (0),
	[created] [datetime] NOT NULL CONSTRAINT [DF_PricebookEntry_created]  DEFAULT (getdate()),
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL CONSTRAINT [DF_PricebookEntry_lastUpdated]  DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL,
	[crmPricebookEntryID] [varchar](50) NULL,
 CONSTRAINT [PK_PricebookEntry] PRIMARY KEY CLUSTERED 
(
	[pricebookEntryID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PricebookEntry_PricebookEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[PricebookEntry]'))
ALTER TABLE [dbo].[PricebookEntry] DROP CONSTRAINT [FK_PricebookEntry_PricebookEntry]
GO

if not exists (select 1 from sysconstraints WHERE constid = OBJECT_ID (N'[FK_PricebookEntry_Pricebook]'))
ALTER TABLE [dbo].[PricebookEntry]  WITH NOCHECK ADD  CONSTRAINT [FK_PricebookEntry_Pricebook] FOREIGN KEY([pricebookID])
REFERENCES [dbo].[Pricebook] ([pricebookID])
GO
ALTER TABLE [dbo].[PricebookEntry] CHECK CONSTRAINT [FK_PricebookEntry_Pricebook]
GO



if not exists (select * from information_schema.columns where table_name='pricebookEntry' and column_name='lastUpdatedByPerson')  
alter TABLE [dbo].[pricebookEntry] add [lastUpdatedByPerson] int NULL
IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PricebookEntry_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[PricebookEntry]'))
ALTER TABLE [dbo].[PricebookEntry] DROP CONSTRAINT [FK_PricebookEntry_Product]
GO

ALTER TABLE [dbo].[PricebookEntry]  WITH CHECK ADD  CONSTRAINT [FK_PricebookEntry_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([ProductID])
GO

ALTER TABLE [dbo].[PricebookEntry] CHECK CONSTRAINT [FK_PricebookEntry_Product]
GO

/* have to run this here so that vEntityname picks up pricebookEntryDel table */
exec generate_mrAudit @tablename='pricebookEntry',@mnemonic='PBE'

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PricebookEntry]') AND name = N'crmPricebookEntryID_unique')
BEGIN
	update PricebookEntry set crmPricebookEntryID = null where crmPricebookEntryID = ''
	CREATE unique INDEX crmPricebookEntryID_unique ON PricebookEntry (crmPricebookEntryID) WHERE crmPricebookEntryID IS NOT NULL ;
END	
GO