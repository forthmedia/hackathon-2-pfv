/* CREATE trngCertificatePrints table */
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trngCertificatePrints]') AND type in (N'U'))
   BEGIN

	CREATE TABLE [dbo].[trngCertificatePrints](
		[cpid] [int] IDENTITY(1,1) NOT NULL,
		[personID] [int] NULL,
		[quizTakenID] [int] NULL,
		[generatedOn] [datetime] NULL,
		[visitid] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[cpid] ASC
		)
	)
	
	ALTER TABLE [dbo].[trngCertificatePrints] ADD  DEFAULT (getdate()) FOR [generatedOn]

END
GO