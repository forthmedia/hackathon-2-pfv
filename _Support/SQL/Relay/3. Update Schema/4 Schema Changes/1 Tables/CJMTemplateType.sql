
if not exists (select 1 from CJMTemplateType where CJMTemplateType = 'Brochure')
begin
	insert into CJMTemplateType values ('Brochure','Brochure')
end
GO
if not exists (select 1 from CJMTemplateType where CJMTemplateType = 'Email Template')
begin
	insert into CJMTemplateType values ('Email Template','Email Template')
end
GO
if not exists (select 1 from CJMTemplateType where CJMTemplateType = 'Datasheet')
begin
	insert into CJMTemplateType values ('Datasheet','Product Datasheet')
end
GO