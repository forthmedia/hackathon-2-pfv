IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[filterCriteria]') AND type in (N'U'))

	CREATE TABLE dbo.filterCriteria
		(
		filterCriteriaID int NOT NULL 
			IDENTITY (1, 1) 
			CONSTRAINT [PK_filterCriteriaID] PRIMARY KEY CLUSTERED,
		filterEntityTypeID int NOT NULL,
		Field nvarchar(100) not null,
		Operator nvarchar(50) not null,
		Comparison nvarchar(max) not null,
		compiledSQL nvarchar(max) not null,
		entityID int NOT NULL,
		entityTypeID int NOT NULL 
			FOREIGN KEY REFERENCES [dbo].[schemaTableBase] ([entityTypeID]),
		)  ON [PRIMARY]

GO