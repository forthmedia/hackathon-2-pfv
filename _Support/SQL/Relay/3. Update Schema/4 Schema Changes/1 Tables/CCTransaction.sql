if not exists (select 1 from information_schema.tables where table_name = 'CCTransaction')
CREATE TABLE [dbo].CCTransaction(
	[transactionID] [int] IDENTITY(1,1) NOT NULL,
	[opportunityID] [int] NOT NULL,
	[orderValue] [numeric](18,2) NOT NULL,
	[currency] [char](3) NOT NULL,
	[created] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [int] NOT NULL,
 CONSTRAINT [PK_CCTransaction] PRIMARY KEY CLUSTERED 
(
	[transactionID] ASC
)) 
GO
