/*********************
2012/03/27 RMB added mobileCompatible
2016-05-12 atrunov  PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules (added hasRecordRightsBitMask)
**********************/

IF		NOT EXISTS 
		(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE	TABLE_NAME	= 'trngModule' 
AND		COLUMN_NAME = 'publishedDate')
BEGIN
	ALTER TABLE trngModule
	ADD			publishedDate DATETIME NULL
END

GO

/*2011/09/06 PPB LID7679 country scope modules*/
if not exists (SELECT * FROM information_schema.columns where table_name='trngModule' and column_name='CountryID')
	ALTER TABLE trngModule ADD CountryID int NULL 

GO

if not exists(select 1 from information_schema.columns where table_name='trngModule' and column_name = 'mobileCompatible')
-- RMB added the mobileCompatible column to the trngModule table
alter table trngModule add mobileCompatible bit not null default 0
GO

/* trngModule */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='TrainingProgramID') 
ALTER TABLE trngModule add TrainingProgramID int NULL default(0)
GO

/* WAB 2012-07-07, although this column is added automatically by a script later on in the build, we now need it earlier to allow a view to be built on it*/
if not exists (select 1 from information_schema.columns WHERE table_name= 'trngModule' AND column_name='title_defaultTranslation')
alter table trngModule add title_defaultTranslation nvarchar(400)

if not exists (select 1 from information_schema.columns WHERE table_name= 'trngModule' AND column_name='description_defaultTranslation')
alter table trngModule add description_defaultTranslation nvarchar(400)

/* 2012-08-29 Training module changes */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='isSCORM') 
	ALTER TABLE trngModule add isSCORM bit NOT NULL DEFAULT 0
GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='passDecidedBy') 
	ALTER TABLE trngModule add passDecidedBy varchar(50) NOT NULL DEFAULT 'Quiz'
GO

/* Social Changes 2012/10/23 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='share') 
	ALTER TABLE trngModule add share bit NOT NULL DEFAULT 1
GO

/* 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='coursewareTypeID') 
	ALTER TABLE trngModule add coursewareTypeID int NULL
GO

declare @lookupId int
select @lookupId = lookupID from lookupList where fieldname='coursewareTypeID' and itemText='Courseware File'
if (@lookupId is null)
begin
	insert into lookupList (fieldname,lookUpTextID,itemText,extraInfo,isParent,isDefault,isLive,locked)
	values ('coursewareTypeID','CoursewareFile','Courseware File',0,0,1,1,0)
end
GO

/* 2015/11/04 PYW P-TAT006 BRD 31 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModule' AND column_name='expiryDate') 
BEGIN
	ALTER TABLE trngModule
	ADD expiryDate datetime NULL
END
GO

/* This script adds a hasRecordRightsBitMask column to a table and populates it */
DECLARE @tableName varchar(MAX)
DECLARE @SQL varchar(MAX)
SET @tableName ='trngModule'
if not exists (select * from information_schema.columns where table_name = @tableName and column_Name = 'hasRecordRightsBitMask')
BEGIN

    set @SQL  = 'alter table ' + @tableName + ' add hasRecordRightsBitMask int default 0  '
    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = 'alter table ' + @tableName + 'del add hasRecordRightsBitMask int'
        exec (@SQL)
    END

    set @SQL  = '
        update ' + @tableName + ' set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
        alter table ' + @tableName + ' alter column hasRecordRightsBitMask int not null
        update recordRights set permission = permission where entity = ''' + @tableName  + '''
        '

    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = '
            update ' + @tableName + 'del set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
            alter table ' + @tableName + 'del alter column hasRecordRightsBitMask int not null
        '
        exec (@SQL)
    END

END
GO


update modEntityDef set ApiActive=1 where tableName='trngModule'
update modEntityDef set APIName=FieldName where tableName='trngModule' and APIName is null