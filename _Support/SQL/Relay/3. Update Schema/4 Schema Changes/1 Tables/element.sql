
/* Social Changes 2012/10/23 */
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'element' AND column_name='share')
BEGIN 
	ALTER TABLE element add share bit NOT NULL DEFAULT 1
	ALTER TABLE elementDel add share bit NOT NULL DEFAULT 1
END
GO

ALTER TABLE element ALTER COLUMN keywords nvarchar(max)
GO

/* 2013-09-10	YMA	Case 436153 Embedly off by default */
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'element' AND column_name='embedly')
Begin
	ALTER TABLE element add embedly bit NOT NULL DEFAULT 0
	ALTER TABLE elementdel add embedly bit NOT NULL DEFAULT 0
END
GO

if not exists (select 1 from information_schema.columns where table_name = 'element' and column_name = 'campaignID')
Begin
	alter table element add campaignID int null
	alter table elementdel add campaignID int null
End
GO


/* WAB 2015-12-01 Visibility Project - column to replace hasRecordRights */
if not exists (select 1 from information_schema.columns where table_name = 'element' and column_name = 'hasRecordRightsBitMask')
Begin
	alter table element add hasRecordRightsBitMask Int default 0 not null
	alter table elementdel add hasRecordRightsBitMask Int default 0 not null
End
GO

/* WAB 2015-12-01 Visibility Project - column to replace hasRecordRights */
if exists (select 1 from information_schema.columns where table_name = 'element' and column_name = 'hasRecordRights')
Begin
	exec sp_dropcolumn 'element','hasRecordRights'
	exec sp_dropcolumn 'elementdel','hasRecordRights'
End
GO



