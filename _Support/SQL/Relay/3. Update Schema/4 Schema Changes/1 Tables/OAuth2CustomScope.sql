
IF object_id('OAuth2CustomScope') is null  
BEGIN
	CREATE TABLE dbo.OAuth2CustomScope
	(
	scopeID INT IDENTITY(1,1),
	scope nvarchar(256) UNIQUE,
	personFields nvarchar(max),
	locationFields nvarchar(max),
	organisationFields nvarchar(max)
	);
End

