/****** Object:  Table [dbo].[ProcessLock]    Script Date: 05/13/2011 12:24:57 ******/
if not exists (select 1 from sysobjects where name='processLock' and type='u')

CREATE TABLE [dbo].[ProcessLock](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[lockName] [varchar](50) NOT NULL,
	[lockDate] [datetime] NOT NULL,
	[instanceID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
 CONSTRAINT [PK_ProcessLock] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ProcessLock_lockDate]'))
ALTER TABLE [dbo].[ProcessLock] ADD  CONSTRAINT [DF_ProcessLock_lockDate]  DEFAULT (getdate()) FOR [lockDate]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ProcessLock_created]'))
ALTER TABLE [dbo].[ProcessLock] ADD  CONSTRAINT [DF_ProcessLock_created]  DEFAULT (getdate()) FOR [created]
GO


if not exists (select 1 from sysindexes where name='IX_ProcessLockName')

/****** Object:  Index [IX_ProcessLockName]    Script Date: 05/13/2011 12:34:28 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ProcessLockName] ON [dbo].[ProcessLock] 
(
	[lockName] ASC
) ON [PRIMARY]
GO

/*
WAB 2012-09-20 Expansion of Process Lock
Adding lastupdatedby column will allow locks to 'renew themselves' in very long running loops around 
*/
if not exists(select 1 from information_schema.columns where table_name='processLock' and column_name='lastupdated')
alter table processLock add lastupdated datetime

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ProcessLock_lastupdated]'))
ALTER TABLE [dbo].[ProcessLock] ADD  CONSTRAINT [DF_ProcessLock_lastupdated]  DEFAULT (getdate()) FOR [lastupdated]
GO

/* 
Adding a metadata/status column allows a long running process to communicate information back to other pages
WAB 2012-10-31 changed column name from Status to MetaData (just seemed nicer and column hasn't been used in anger yet)
*/
if exists(select 1 from information_schema.columns where table_name='processLock' and column_name='status')
	exec sp_rename 'processlock.status', 'metadata', 'column'

if not exists(select 1 from information_schema.columns where table_name='processLock' and column_name='metadata')
	alter table processLock add metadata varchar (max)


/* WAB 2013-01 	Sprint 15&42 Comms.  Increased size of lockname field. */
alter table processLock
	alter column lockname varchar(100)

/* WAB 2015-07-02 Add maximumExpectedLockMinutes */
if not exists(select 1 from information_schema.columns where table_name='processLock' and column_name='maximumExpectedLockMinutes')
	alter table processLock add maximumExpectedLockMinutes int
	
/* WAB 2015-09-14 added a spid column - to help debugging */
if not exists(select 1 from information_schema.columns where table_name='processLock' and column_name='spid')
	alter table processlock add spid int

	