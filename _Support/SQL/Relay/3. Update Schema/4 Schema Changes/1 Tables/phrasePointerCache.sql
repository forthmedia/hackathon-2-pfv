/*
Code for new database level phrase caching
Table: PhrasePointerCache 
Function: getTablePrimaryKey
Views: dbo.schemaTable  - updated to use getTablePrimaryKey
		dbo.flagentitytype  - updated to refer to dbo.schemaTable

SP: translatePhrases
function getTablePrimaryKey
SP updatePhrasePointerCacheForEntityType
Trigger trigger_phrase_updatePhrasePointerCache

*/

if not exists (select 1 from sysobjects where name = 'phrasePointerCache')
BEGIN
CREATE TABLE [dbo].[phrasePointerCache](
	[phraseid] [int] NOT NULL,
	[languageid] [int] NOT NULL,
	[countryid] [int] NOT NULL,
	[phrase_Ident] [int] NULL,
 CONSTRAINT [phrasePointerCache_PK] PRIMARY KEY CLUSTERED 
(
	[phraseid] ASC,
	[languageid] ASC,
	[countryid] ASC
)
) ON [PRIMARY]
END
GO


/* WAB 2011/05/09 
Performance changes
*/
if not exists (select 1 from information_schema.columns where table_name = 'phrasePointerCache' and column_name = 'dirty')
BEGIN
alter table phrasePointerCache add dirty bit default 1 not null
END


/*	WAB/NJH 2017-01-12 
	improve performance when doing a full text search by language
	Eg this query 
			SELECT pl.entityid
             FROM PhraseList pl
                  INNER JOIN phrases p ON pl.phraseid = p.phraseid
                  INNER JOIN phrasepointercache pp on pp.phrase_ident = p.ident
                  INNER JOIN vPhrasesSearch ps on ps.ident = p.ident
                 -- inner join elementTreeCacheData c on pl.entityID = c.elementID
				WHERE pl.entityTypeID = 10
                  and CONTAINS(ps.phraseText, 'internet' )
                  and pp.languageid = 1
                  and pp.countryid = 130

*/
if not exists (select 1 from sysindexes where name = 'phrasePointerCache_languagueid_countryid_phraseident')
BEGIN	
	CREATE NONCLUSTERED INDEX [phrasePointerCache_languagueid_countryid_phraseident]
	ON [dbo].[phrasePointerCache] ([languageid],[countryid],[phrase_Ident])
END