if not exists(select 1 from information_schema.columns where table_name='modEntityDef' and column_name='showInConditionalEmails')
alter table modEntityDef add showInConditionalEmails bit default 0
GO

update modEntityDef set showInConditionalEmails=0 where showInConditionalEmails is null
GO

alter table modEntityDef alter column showInConditionalEmails bit not null
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ModEntityDef]') AND name = N'ModEntityID_Idx')
DROP INDEX [dbo].[ModEntityDef].[ModEntityID_Idx]
GO

CREATE NONCLUSTERED INDEX [ModEntityID_Idx] ON [dbo].[ModEntityDef] 
(
	[ModEntityID] ASC,
	[TableName] ASC,
	[FieldName] ASC
) ON [PRIMARY]
GO


if not exists(select 1 from information_schema.columns where table_name='modEntityDef' and column_name='ApiName')
	alter table modEntityDef add ApiName varchar(30)
GO

if not exists(select 1 from information_schema.columns where table_name='modEntityDef' and column_name='ApiActive')
	alter table modEntityDef add ApiActive bit default(0)
GO

update modEntityDef set ApiActive = 0 where ApiActive is null

alter table modEntityDef alter column ApiActive bit not null
GO

/* NJH 2013/01/16  - lengthened the fieldName column*/
alter table modEntityDef alter column fieldname varchar(50) not null
GO

alter table modEntityDef alter column ApiName varchar(50)
GO


/* WAB 2014-03-26 
Add some unique indexes to the table
*/

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ModEntityDef]') AND name = N'modentityDef_modentityid') 
	create unique index modentityDef_modentityid on modentitydef (modentityid)

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ModEntityDef]') AND name = N'modentityDef_tableName_Fieldname')
	create unique index modentityDef_tableName_Fieldname on modentitydef (tablename, fieldname)

/* NJH 2016/11/29 - screen re-work - create a readonly attribute for fields */
if not exists(select 1 from information_schema.columns where table_name='modEntityDef' and column_name='readOnly')
	alter table modEntityDef add readOnly bit not null constraint DF_modEntityDef_readOnly default 0