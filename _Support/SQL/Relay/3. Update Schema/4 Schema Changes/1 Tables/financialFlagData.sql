IF  NOT EXISTS (SELECT 1 FROM sysobjects WHERE name='financialFlagData' AND type='u')
CREATE TABLE [dbo].[financialFlagData](
	[entityID] [int] NOT NULL,
	[flagID] [int] NOT NULL,
	[data] [decimal](28, 10) NOT NULL,
	[currencyISOCode] NCHAR(3) NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
	[rowID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_financialFlagData] PRIMARY KEY CLUSTERED 
(
	[entityID] ASC,
	[flagID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_financialFlagData_created]') AND type = 'D')
ALTER TABLE [dbo].[financialFlagData] ADD  CONSTRAINT [DF_financialFlagData_created]  DEFAULT (getdate()) FOR [created]
GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_financialFlagData_lastUpdated]') AND type = 'D')
ALTER TABLE [dbo].[financialFlagData] ADD  CONSTRAINT [DF_financialFlagData_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO


