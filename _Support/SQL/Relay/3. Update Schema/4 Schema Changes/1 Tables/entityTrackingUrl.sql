if not exists (select 1 from sysobjects where name='entityTrackingUrl' and type='u')
BEGIN
CREATE TABLE [dbo].[entityTrackingUrl](
	[entityTrackingUrlID] [int] IDENTITY(1,1) NOT NULL,
	[url] [nvarchar] (1000)
	PRIMARY KEY (entityTrackingUrlID) 
) 
END
GO

if not exists (select 1 from sysindexes where name='idx_url')
CREATE UNIQUE NONCLUSTERED INDEX [idx_url] ON [dbo].[entityTrackingUrl] 
(
	[url] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO