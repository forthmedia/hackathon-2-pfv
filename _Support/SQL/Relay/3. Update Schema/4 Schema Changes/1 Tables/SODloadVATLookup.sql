

if not exists (select 1 from sysobjects where name = 'SODloadVATLookup')
BEGIN
CREATE TABLE [dbo].[SODloadVATLookup]
(
[thirdPartyVATnumber] [varchar] (20) NOT NULL,
[locationID] [int] NOT NULL,
[created] [datetime] NOT NULL DEFAULT (getdate()),
[createdBy] [int] NOT NULL DEFAULT (0),
[lastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
[lastUpdatedBy] [int] NOT NULL DEFAULT (0)
)

ALTER TABLE [dbo].[SODloadVATLookup] ADD CONSTRAINT [PK_SODloadVATLookup] PRIMARY KEY CLUSTERED  ([thirdPartyVATnumber])
END
