/* LID 4314 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='commDetail' AND column_name='CommCampaignID') 
BEGIN	
	ALTER TABLE commDetail
	ADD CommCampaignID  int null
END	

/* WAB 2012-10-10 This index has been superceded by later ones, so I will delete if it exists
/* IH 2012-04 CASE 424774 */
CREATE NONCLUSTERED INDEX [CommID_Test_Internal] ON [dbo].[commdetail] 
(
	[CommID] ASC,
	[Test] ASC,
	[Internal] ASC
)
INCLUDE ( [PersonID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
*/

IF EXISTS (select * from sysindexes where id=object_id('commdetail') and name='CommID_Test_Internal')
DROP Index commdetail.CommID_Test_Internal


	
/* WAB 2012-10-10 This index is being replaced by a very similar one with an extra column.  Note that this index never got released to some critical boxes
/* IH 2012-06 CASE 427512*/
CREATE NONCLUSTERED INDEX 
	[idx_CommID_Test_Internal_mostSignificantCommStatusID_CommTypeId]
ON [dbo].[commdetail] 
	([CommID],[Test],[Internal],[mostSignificantCommStatusID],[CommTypeId])
INCLUDE 
	([PersonID])
*/

IF EXISTS (select * from sysindexes	where id=object_id('commdetail') and name='idx_CommID_Test_Internal_mostSignificantCommStatusID_CommTypeId')
DROP INDEX commdetail.idx_CommID_Test_Internal_mostSignificantCommStatusID_CommTypeId



/* WAB 2012-10-10 CASE 426507 This index is a minor improvement on idx_CommID_Test_Internal_mostSignificantCommStatusID_CommTypeId above */
IF NOT EXISTS (select * from sysindexes where id=object_id('commdetail') and name='idx_CommID_Test_Internal_Include_Personid_mostSignificantCommStatusID_CommTypeId_commstatusid')
CREATE NONCLUSTERED INDEX 
	[idx_CommID_Test_Internal_Include_Personid_mostSignificantCommStatusID_CommTypeId_commstatusid]
ON [dbo].[commdetail] 
	([CommID],[Test],[Internal],[mostSignificantCommStatusID])
INCLUDE 
	([PersonID], [CommTypeId],CommStatusID)


/* WAB 2012-10-10 Remove this index, it is a subset of the above one
/* 
PJP 2012/08/03 426507
NJH 2012/08/06 - removed where clause - not supported in pre-2008 servers */
IF NOT EXISTS (SELECT * FROM sysindexes WHERE name = N'FI_mostsignificantcommstatusid')
	CREATE NONCLUSTERED INDEX [FI_mostsignificantcommstatusid] ON [dbo].[commdetail] 
	(
		[CommId] asc
	)
	INCLUDE ( [commstatusid],[mostsignificantcommstatusid]) 
	--WHERE ([commstatusid] IS NOT NULL AND [mostsignificantcommstatusid] IS NULL)
*/

IF EXISTS (SELECT * FROM sysindexes WHERE name = N'FI_mostsignificantcommstatusid')
	DROP INDEX commdetail.FI_mostsignificantcommstatusid


/* WAB 2012-10-10, don't see much point in this index, given the ones above
CREATE NONCLUSTERED INDEX [CommID]ON [dbo].[commdetail] 
([CommID] ASC)
*/
IF EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='CommID')
	DROP INDEX commdetail.commid
	
	
/* WAB 2012-10-10 Don�t see much point in this index either
CREATE NONCLUSTERED INDEX [TypeLID] ON [dbo].[commdetail] 
([CommFormLID] ASC)
*/
IF EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='TypeLID')
	DROP INDEX commdetail.typelid


/*
WAB 2012-10-16 Add datesent as an include on the locationID index (now renamed LocationID_datesent)
*/
IF EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='LocationID')
	DROP INDEX commdetail.locationID


IF NOT EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='LocationID_dateSent')
BEGIN	
	CREATE NONCLUSTERED INDEX [LocationID_DateSent] ON [dbo].[commdetail] 
	(
		[LocationID] ASC
	)
		include (datesent)
END


/*
WAB 2012-10-16 Add datesent as an include on the PersonID index (now renamed personID_datesent)
*/
IF EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='PersonID')
	DROP INDEX commdetail.personID


IF NOT EXISTS (SELECT * FROM sysindexes WHERE id=object_id('commdetail') and name='PersonID_dateSent')
BEGIN	
	CREATE NONCLUSTERED INDEX [PersonID_DateSent] ON [dbo].[commdetail] 
	(
		[PersonID] ASC
	)
		include (datesent)
END


/*
WAB 2013-11-21 convert feeback to nvarchar(max)
Although generally contains very short text, can be used for large ad hoc emails etc.
Currently there is code to split very long items across more that one record.
We need to join together previously split records 

*/

IF EXISTS (select * from information_schema.columns where table_name = 'commdetail' and column_name = 'feedback' and character_maximum_Length <> -1)
	BEGIN
		alter table commdetail alter column feedback nvarchar(max)
		alter table commdetailHistory alter column feedback nvarchar(max)
		
			
			/* Now update the data */
			declare @adhocCommsID int, @commid int, @commdetailid1 int, @commdetailid2 int, @feedback1 nvarchar(max), @feedback2 nvarchar(max), @len1 int, @len2 int
			
			select @adhocCommsID = commid from communication where title = 'adhoccomms'
			
			/* Get all items which were split over more than one record 
				The will have exactly same lastUpdated date 
			*/
			declare myCursor cursor for
				select 
					cd1.commid,cd1.commdetailid as commdetailid1, cd2.commdetailid as commdetailid2, cd1.feedback as feedback1, cd2.feedback as feedback2, len(cd1.feedback) as len1, len(cd2.feedback) as len2
				from 
					commdetail cd1
						inner join
					commdetail cd2 on cd1.commid = cd2.commid and cd1.personid = cd2.personid and cd1.lastupdated = cd2.lastupdated  and cd1.commdetailid > cd2.commdetailid and cd1.commdetailid < cd2.commdetailid + 10   
			
				where 
					1=1
					and cd1.commid = @adhocCommsID
					and cd1.feedback <> cd2.feedback
					and (len(cd1.feedback) > 3990 OR  len(cd1.feedback) BETWEEN  240 AND 256)
				order by 
					cd1.commdetailid asc
			
			
			open myCursor
			fetch next from myCursor into 
				@commid , @commdetailid1 , @commdetailid2 , @feedback1 , @feedback2 , @len1 , @len2 
			
			while @@FETCH_STATUS = 0
			begin
				update 
					commdetail 
				set 
					feedback = feedback + (SELECT FEEDBACK FROM COMMDETAIL WHERE COMMDETAILID = @commdetailid2)
				where 
					commdetailid = @commdetailid1
				
				delete from commdetail where commdetailid = @commdetailid2	
			
			
				fetch next from myCursor into 
					@commid , @commdetailid1 , @commdetailid2 , @feedback1 , @feedback2 , @len1 , @len2 
					
			end
			
			close myCursor;
			deallocate myCursor;
		
	END	
	
	