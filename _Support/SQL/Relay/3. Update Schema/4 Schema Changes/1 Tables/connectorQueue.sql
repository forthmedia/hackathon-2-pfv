
IF not exists (select 1 from sysobjects where name='connectorQueue' and type='U')
BEGIN
	/****** Object:  Table [dbo].[connectorQueue]    Script Date: 05/15/2014 10:30:55 ******/
	CREATE TABLE [dbo].[connectorQueue](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorObjectID] [int] NOT NULL,
		[objectID] [varchar](20) NOT NULL,
		[modifiedDateUTC] [datetime] NOT NULL,
		[synchAttempt] [int] NOT NULL,
		[isDeleted] [bit] NOT NULL,
		[errored] [bit] NOT NULL,
		[connectorResponseID] [int] NULL,
		[created] [datetime] NOT NULL,
	 CONSTRAINT [PK_connectorQueue] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorQueue] ADD  CONSTRAINT [DF_connectorQueue_synchAttempt]  DEFAULT ((0)) FOR [synchAttempt]

	ALTER TABLE [dbo].[connectorQueue] ADD  CONSTRAINT [DF_connectorQueue_isDeleted]  DEFAULT ((0)) FOR [isDeleted]

	ALTER TABLE [dbo].[connectorQueue] ADD  CONSTRAINT [DF_connectorQueue_errored]  DEFAULT ((0)) FOR [errored]

	ALTER TABLE [dbo].[connectorQueue] ADD  CONSTRAINT [DF_connectorQueue_created]  DEFAULT (getdate()) FOR [created]
END

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[connectorQueue]') AND name = N'idx_connectorQueue_connectorObjectID_errored_synchAttempt_inc_ID_objectID')
CREATE NONCLUSTERED INDEX [idx_connectorQueue_connectorObjectID_errored_synchAttempt_inc_ID_objectID]
ON [dbo].[connectorQueue] ([connectorObjectID],[errored],[synchAttempt])
INCLUDE ([ID],[objectID])
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[connectorQueue]') AND name = N'IX_connectorQueue_object_objectID')
CREATE UNIQUE NONCLUSTERED INDEX [IX_connectorQueue_object_objectID] ON [dbo].[connectorQueue] 
(
	[connectorObjectID] ASC,
	[objectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[connectorQueue]') AND name = N'idx_connectorQueue_connectorObjectID_isDeleted_errored_synchAttempt_inc_ID_objectID')
CREATE NONCLUSTERED INDEX [idx_connectorQueue_connectorObjectID_isDeleted_errored_synchAttempt_inc_ID_objectID] ON [dbo].[connectorQueue] 
(
	[connectorObjectID] ASC,
	[isDeleted] ASC,
	[errored] ASC,
	[synchAttempt] ASC
)
INCLUDE ( [ID],
[objectID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* add dataExists column that is set to one when data for a given record is not found... generally a deleted record
	that came over as a dependency. This is used primarily for reporting purposes */
if not exists (select 1 from information_schema.columns where table_name='connectorQueue' and column_name='dataExists')
alter table connectorQueue add dataExists bit not null constraint DF_connectorQueue_dataExists default 1

/*************** increase size of crm IDs to varchar(50) **************************/
ALTER TABLE [connectorQueue] ALTER COLUMN [objectID] varchar(50)
GO

/* JIRA PROD2016-953 - NJH 2016/05/26 - column to hold the data response. The connectorResponseID column now 
holds the ID of the record that brought the data into the queue. The dataConnectorResponseID holds the connectorResponse
of the call that got the data */
if not exists (select 1 from information_schema.columns where table_name='connectorQueue' and column_name='dataConnectorResponseID')
alter table connectorQueue add dataConnectorResponseID int null

/* Case 451735/PROD2016-2336 - add process column so we can mark the records to export/import per iteration */
if not exists (select 1 from information_schema.columns where table_name='connectorQueue' and column_name='process')
alter table connectorQueue add process bit not null constraint DF_connectorQueue_process default 0

/* JIRA PROD2016-1336 */
if not exists(select 1 from sys.indexes where object_id = OBJECT_ID(N'[dbo].[connectorQueue]') AND name = N'idx_connectorQueue_connectorResponseID')
CREATE NONCLUSTERED INDEX [idx_connectorQueue_connectorResponseID] ON [dbo].[connectorQueue] ([connectorResponseID])
