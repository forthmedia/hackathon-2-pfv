if not exists (select 1 from information_schema.tables where table_name='trngTrainingPathPerson')
BEGIN
	CREATE TABLE [dbo].[trngTrainingPathPerson](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[trainingPathID] [int] NOT NULL,
		[personID] [int] NOT NULL,
		[active] [bit] NOT NULL,
		[Created] [datetime] NOT NULL,
		[CreatedBy] [int] NOT NULL,
		[LastUpdated] [datetime] NOT NULL,
		[LastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_trainingPathPerson_ID] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[trngTrainingPathPerson] ADD CONSTRAINT [DF_trngTrainingPathPerson_created]  DEFAULT (getdate()) FOR [Created]

	ALTER TABLE [dbo].[trngTrainingPathPerson] ADD CONSTRAINT [DF_trngTrainingPathPerson_lastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]

	ALTER TABLE [dbo].[trngTrainingPathPerson] ADD CONSTRAINT [DF_trngTrainingPathPerson_active]  DEFAULT (1) FOR [Active]
END
GO