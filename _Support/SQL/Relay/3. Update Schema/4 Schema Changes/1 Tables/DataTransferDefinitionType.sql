
if not exists (select * from sysobjects where name='DataTransferDefinitionType')
begin
CREATE TABLE [dbo].[DataTransferDefinitionType](
	[DataTransferTypeID] [int] NOT NULL,
	[DataTransferTypeName] [varchar](255) NULL,
 CONSTRAINT [PK_DataTransferDefinitionType] PRIMARY KEY CLUSTERED 
(
	[DataTransferTypeID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
End
GO
