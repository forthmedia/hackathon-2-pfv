IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizDetailQuestionPool]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[QuizDetailQuestionPool] (
	[QuizDetailQuestionPoolID] int IDENTITY(1,1) NOT NULL  
	, [QuizDetailID] int  NOT NULL  
	, [QuestionPoolID] int  NOT NULL  
	, [sortOrder] int  NULL  
	)


ALTER TABLE [dbo].[QuizDetailQuestionPool] ADD CONSTRAINT [QuizDetailQuestionPool_PK] PRIMARY KEY CLUSTERED (
[QuizDetailQuestionPoolID]
)
END
GO


/* NJH 2014/06/23 Task Core-81 add numOfQuestions */
if not exists (select 1 from information_schema.columns where table_name='QuizDetailQuestionPool' and column_name='numOfQuestions')
alter table QuizDetailQuestionPool add numOfQuestions int null