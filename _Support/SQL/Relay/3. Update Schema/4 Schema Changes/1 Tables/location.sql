alter table location alter column SpecificURL nvarchar(500) null
alter table locationDel alter column SpecificURL nvarchar(500) null


-- Create the HistoricalLocationIDlist field in the location and del table
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'Location' AND column_name='HistoricalLocationIDlist')
ALTER TABLE Location ADD HistoricalLocationIDlist varchar(2000)  null default('') /* WAB 2010/11/1 changed from default(@@identity + 1) which didn't work */
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'LocationDel' AND column_name='HistoricalLocationIDlist')
ALTER TABLE LocationDel ADD HistoricalLocationIDlist varchar(2000) not null default('')
GO
-- set default values for the column in both tables
--2011/06/17 GCC added where clause to stop overwriting data once implemented
UPDATE Location
set HistoricalLocationIDlist = locationID
where HistoricalLocationIDlist is null or HistoricalLocationIDlist = ''
GO
--2011/06/17 GCC added where clause to stop overwriting data once implemented
UPDATE LocationDel
set HistoricalLocationIDlist = locationID
where HistoricalLocationIDlist is null or HistoricalLocationIDlist = ''
GO
-- set the column to not null in the location table
ALTER TABLE Location ALTER COLUMN HistoricalLocationIDlist varchar(2000) not null
GO


if not exists (select 1 from information_schema.columns where table_name='location' and column_name='Latitude')  
      alter TABLE [dbo].location add Latitude float NULL

if not exists (select 1 from information_schema.columns where table_name='location' and column_name='Longitude')  
      alter TABLE [dbo].location add Longitude float NULL

if not exists (select 1 from information_schema.columns where table_name='location' and column_name='LatLongSource')  
      alter TABLE [dbo].location add [LatLongSource] varchar(50) NULL

GO
    
/****** Object:  Index [crmLocID]    Script Date: 03/02/2012 11:28:13 ******/
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'crmLocID')
DROP INDEX [crmLocID] ON [dbo].[Location] 
GO
/****** Object:  Index [crmLocID]    Script Date: 03/02/2012 11:28:14 ******/
/*CREATE NONCLUSTERED INDEX [crmLocID] ON [dbo].[Location] 
(
	[crmLocID] ASC
) ON [PRIMARY]
*/

if not exists (select 1 from information_schema.columns where table_name='location' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].location add [lastUpdatedByPerson] int NULL
GO

if not exists (select * from information_schema.columns where table_name='location' and column_name='matchSiteName')  
	alter TABLE [dbo].location add matchSiteName varchar(160) NULL
GO

if not exists (select * from information_schema.columns where table_name='location' and column_name='matchAddress')  
	alter TABLE [dbo].location add matchAddress varchar(500) NULL
GO

if exists (select 1 from information_schema.columns where table_name='location' and column_name='siteType')
	alter table dbo.location drop column siteType
GO

if not exists (select 1 from information_schema.columns where table_name='location' and column_name='accountTypeID')  
	alter TABLE [dbo].location add [accountTypeID] int NULL
GO

IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_accountTypeID_organisationType_organisationTypeID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_Location_accountTypeID_organisationType_organisationTypeID]
GO

ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_Location_accountTypeID_organisationType_organisationTypeID] FOREIGN KEY([accountTypeID])
REFERENCES [dbo].[organisationType] ([organisationTypeID])
GO

ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_Location_accountTypeID_organisationType_organisationTypeID]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'crmLocID_unique')
BEGIN
	/* WAB 2015-02-12 null out any blank crmLocIDs so that unique index can be created */
	update location set crmlocid = null where crmLocID = ''
	CREATE unique INDEX crmLocID_unique ON location (crmlocid) WHERE crmlocid IS NOT NULL ;
END	
GO

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'idx_location_matchSitename')
/****** Object:  Index [idx_matchSitename]    Script Date: 05/01/2014 14:41:46 ******/
CREATE NONCLUSTERED INDEX [idx_location_matchSitename] ON [dbo].[Location] 
(
	[matchSiteName] ASC
)
GO


IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'idx_location_matchAddress')
/****** Object:  Index [idx_matchSitename]    Script Date: 05/01/2014 14:41:46 ******/
CREATE NONCLUSTERED INDEX [idx_location_matchAddress] ON [dbo].[Location] 
(
	[matchAddress] ASC
)
GO


/* WAB 2014-01-28 CASE 438734 
Create an index on matchname
Greatly improves performance on inserting new records 
*/ 
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'location_matchname')
	CREATE INDEX location_matchname ON location
	(matchname) INCLUDE (countryid)
	
GO


if not exists (select 1 from information_schema.columns where table_name='location' and column_name='convertedFromLeadID')
	BEGIN
		alter table dbo.location add convertedFromLeadID int null
		alter table dbo.locationDEL add convertedFromLeadID int null
	END
GO



/*		WAB 2014-10-22  CASE 442259 Added Areas for use in locator  
		WAB 2014-11-12  CASE 442259 changed the names of these fields to not have underscores in them - causes problems withupdate data
						Script to change the one or two dbs where fields with _ were created
*/

/* First to deal with the one or two dbs where the fields were created with _s */
if exists (select 1 from information_schema.columns where table_name='location' and column_name='administrative_Area_Level_1')
	BEGIN
		exec sp_rename 'location.administrative_Area_Level_1', 'administrativeAreaLevel1', 'column'
		exec sp_rename 'locationdel.administrative_Area_Level_1', 'administrativeAreaLevel1', 'column'
		exec sp_rename 'location.administrative_Area_Level_1', 'administrativeAreaLevel1', 'index'
	END
if exists (select 1 from information_schema.columns where table_name='location' and column_name='administrative_Area_Level_2')
	BEGIN
		exec sp_rename 'location.administrative_Area_Level_2', 'administrativeAreaLevel2', 'column'
		exec sp_rename 'locationdel.administrative_Area_Level_2', 'administrativeAreaLevel2', 'column'
	END

if not exists (select 1 from information_schema.columns where table_name='location' and column_name='administrativeAreaLevel1')
	BEGIN
		alter table location add administrativeAreaLevel1 nvarchar(200)
		alter table locationdel add administrativeAreaLevel1 nvarchar(200)
	END
if not exists (select 1 from information_schema.columns where table_name='location' and column_name='administrativeAreaLevel2')
	BEGIN
		alter table location add administrativeAreaLevel2 nvarchar(200)
		alter table locationdel add administrativeAreaLevel2 nvarchar(200)
	END


IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'administrativeAreaLevel1')
	create Index administrativeAreaLevel1 on Location (administrativeAreaLevel1)

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'administrativeAreaLevel2')
	create Index administrativeAreaLevel2 on Location (administrativeAreaLevel2)

update ModEntityDef set ApiActive=1 where TableName ='location' and FieldName = 'crmLocID' --BF256 RJT

/* NJH 2016/0322 sugar 446863 - increase length of org name to 255 */
if not exists (select 1 from information_schema.columns where table_name='location' and column_name='sitename' and character_maximum_length =255)
begin
	exec dropAndRecreateDependentObjects 'location.sitename', 'dropfulltextindexes'
	begin tran
        exec dropAndRecreateDependentObjects 'location.sitename', 'drop'
        alter table location alter column sitename nvarchar(255) not null

        exec dropAndRecreateDependentObjects 'location.sitename', 'recreate'
    commit
    exec dropAndRecreateDependentObjects 'location.sitename', 'recreatefulltextindexes'
    exec generate_mrAuditDel @tablename='location'
end

/* NJH 2016/0322 sugar 446863 - increase length of org name to 255 
if not exists (select 1 from information_schema.columns where table_name='location' and column_name='matchSiteName' and character_maximum_length =255)
begin
	begin tran
        exec dropAndRecreateDependentObjects 'location.matchSiteName', 'drop'
        alter table location alter column matchSiteName nvarchar(255) null

        exec dropAndRecreateDependentObjects 'location.matchSiteName', 'recreate'
    commit
	exec generate_mrAuditDel @tablename='location'
end*/

-- set match address to be nvarchar (BF-663) NJH 2016/04/26
if not exists(select 1 from information_schema.columns where table_name='location' and column_name='matchAddress' and data_type ='nvarchar')
begin
	begin tran
        exec dbo.dropAndRecreateDependentObjects 'location.matchAddress', 'drop'
        alter table dbo.location alter column matchAddress nvarchar(500) null

        exec dbo.dropAndRecreateDependentObjects 'location.matchAddress', 'recreate'
    commit
	exec dbo.generate_mrAuditDel @tablename='location'
end

/* unbind the locationID default */
if exists (select 1 from sys.columns where object_id = OBJECT_ID('location') AND name = 'locationID' and OBJECT_NAME([default_object_id]) = 'UW_ZeroDefault')
EXEC sp_unbindefault 'location.locationID'

/* NJH 2016/08/25 JIRA PROD2016-125 if locationId is not an identity column */
declare @locIDisIdentity int = 0
select  @locIDisIdentity = columnproperty(object_id('location'),'locationID','IsIdentity')
if (@locIDisIdentity = 0)
begin
	exec convertBoundDefaultsToConstraints 'location'
	exec convertColumnToIdentity 'location', 'locationid'
end

/* NJH 2016/12/05 - JIRA PROD2016-1190 - remove matchsitename and use matchname again */
if exists (select 1 from information_schema.columns where table_name='location' and column_name='matchSiteName')
begin
	if exists (select 1 from sysindexes where name = 'idx_location_matchSitename')
	begin
		drop index location.idx_location_matchSitename
	end
    alter table location drop column matchSiteName
    
    /* NJH 2017/02/20  - build fix - drop the trigger here as later this trigger is no longer correct with the above change and in dropping and 
		recreating the organisation table, we fail because this trigger tries to get rebuilt as it references the organisation table. 
		This trigger is no longer valid references the matchsitename column. It assumes the trigger is recreated later*/
    IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Location_UTrig]'))
	DROP TRIGGER [dbo].[Location_UTrig]
	
	IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Location_IUTrig]'))
	DROP TRIGGER [dbo].[Location_IUTrig]
end