if not exists (select 1 from information_schema.columns where table_name='productFamily' and column_name='createdByPerson')
begin
	alter table productFamily add createdByPerson int null
end

if not exists (select 1 from information_schema.columns where table_name='productFamily' and column_name='lastUpdatedByPerson')
begin
	delete from ModEntityDef where TableName='productFamily'
	alter table productFamily add lastUpdatedByPerson int null
end

IF NOT EXISTS (select * from ModEntityDef where TableName='productFamily')
BEGIN
 	exec generate_MRAudit @TableName = 'productFamily'
END

IF EXISTS (select * from ModEntityDef where TableName='productFamily')
BEGIN
 	update ModEntityDef set ApiActive=1 where TableName ='productFamily' and FieldName = 'PRODUCTFAMILYID' and ApiActive=0
 	update ModEntityDef set ApiActive=1 where TableName ='productFamily' and FieldName = 'NAME' and ApiActive=0
 	update ModEntityDef set ApiActive=1 where TableName ='productFamily' and FieldName = 'CREATEDBY' and ApiActive=0
 	update ModEntityDef set ApiActive=1 where TableName ='productFamily' and FieldName = 'LASTUPDATEDBY' and ApiActive=0

 	update ModEntityDef set ApiName='PRODUCTFAMILYID' where TableName ='productFamily' and FieldName = 'PRODUCTFAMILYID' and ApiName is null
 	update ModEntityDef set ApiName='NAME' where TableName ='productFamily' and FieldName = 'NAME' and ApiName is null
 	update ModEntityDef set ApiName='CREATEDBY' where TableName ='productFamily' and FieldName = 'CREATEDBY' and ApiName is null
 	update ModEntityDef set ApiName='LASTUPDATEDBY' where TableName ='productFamily' and FieldName = 'LASTUPDATEDBY' and ApiName is null
END

