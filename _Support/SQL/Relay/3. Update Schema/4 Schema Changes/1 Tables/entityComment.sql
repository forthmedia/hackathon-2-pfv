if not exists (select 1 from sysobjects where name='entityComment' and type='u')
begin
CREATE TABLE [dbo].[entityComment](
	[entityCommentID] [int] IDENTITY(1,1) NOT NULL,
	[entityTypeID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[comment] [nvarchar](max) NOT NULL,
	[personID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[entityCommentID] ASC
) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[entityComment] ADD  DEFAULT (getdate()) FOR [created]

ALTER TABLE [dbo].[entityComment] ADD  DEFAULT ((0)) FOR [createdBy]

ALTER TABLE [dbo].[entityComment] ADD  DEFAULT (getdate()) FOR [lastUpdated]

ALTER TABLE [dbo].[entityComment] ADD  DEFAULT ((0)) FOR [lastUpdatedBy]

ALTER TABLE [dbo].[entityComment] ADD  DEFAULT ((0)) FOR [lastUpdatedByPerson]

END
GO

if not exists (select 1 from information_schema.columns where table_name = 'entityComment' and column_name = 'active')
	BEGIN
	ALTER TABLE dbo.entityComment ADD
		active bit NOT NULL default ((1))
	End
GO


