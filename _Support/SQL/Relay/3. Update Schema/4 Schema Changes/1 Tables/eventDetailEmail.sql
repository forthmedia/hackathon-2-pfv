/* WAB 2011/04/27 add nVarchar support */
alter table eventDetailEmail alter column subject nvarchar (255)
GO

/* Very nasty construct to change text to nText.  Can't use alter column.
*/

if exists (select 1 from information_schema.columns where table_name = 'eventDetailEmail' and column_name = 'messageText' and data_type <> 'nText')
BEGIN
	BEGIN TRANSACTION
	CREATE TABLE dbo.Tmp_EventDetailEmail
		(
		FlagID int NOT NULL,
		EventDetailEmailType varchar(50) NOT NULL,
		subject nvarchar(255) NULL,
		messagetext ntext NULL
		)  
		 
	
	IF EXISTS(SELECT * FROM dbo.EventDetailEmail)
		 EXEC('INSERT INTO dbo.Tmp_EventDetailEmail (FlagID, EventDetailEmailType, subject, messagetext)
			SELECT FlagID, EventDetailEmailType, subject, CONVERT(text, messagetext) FROM dbo.EventDetailEmail WITH (HOLDLOCK TABLOCKX)')

	DROP TABLE dbo.EventDetailEmail

	EXECUTE sp_rename N'dbo.Tmp_EventDetailEmail', N'EventDetailEmail', 'OBJECT' 

	ALTER TABLE dbo.EventDetailEmail ADD CONSTRAINT
		PK_EventDetailEmail PRIMARY KEY NONCLUSTERED 
		(
		FlagID,
		EventDetailEmailType
		)  ON [PRIMARY]
	

	COMMIT
END




if not exists (select 1 from information_schema.columns where table_name = 'EventDetailEmail' and column_name = 'SuppressEmail') 
	alter table EventDetailEmail add SuppressEmail bit default(0) not null
GO
