/* create scheduleEmail table */

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleEmail')
BEGIN
CREATE TABLE [dbo].[ScheduleEmail](
	[ScheduleEmailID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[emailDefID] [int] NULL,
 CONSTRAINT [PK_ScheduleEmail] PRIMARY KEY CLUSTERED 
(
	[ScheduleEmailID] ASC,
	[ScheduleID] ASC
)) ON [PRIMARY]
END
GO
