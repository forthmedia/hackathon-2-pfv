
/* NJH 2012/11/19 CASE 432093 - add some mod register columns */
if not exists(select 1 from information_schema.columns where table_name='eligibilityRule' and column_name='lastUpdated')
alter table eligibilityRule add lastUpdated datetime not null default(getDate())

if not exists(select 1 from information_schema.columns where table_name='eligibilityRule' and column_name='lastUpdatedBy')
alter table eligibilityRule add lastUpdatedBy int null

if not exists(select 1 from information_schema.columns where table_name='eligibilityRule' and column_name='lastUpdatedByPerson')
alter table eligibilityRule add lastUpdatedByPerson int null