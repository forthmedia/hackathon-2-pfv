IF object_id('SAMLIdentityProviderMapping') is null  
BEGIN
	Create Table [dbo].[SAMLIdentityProviderMapping](
		SAMLIdentityProviderId int NOT NULL FOREIGN KEY REFERENCES [dbo].[SAMLIdentityProvider] ([SAMLIdentityProviderId]) on delete cascade,
		relaywareField nVarChar(500) NOT NULL,
		useAssertion bit not null,
		foreignValue nVarChar(500) not null default 0
		)
END