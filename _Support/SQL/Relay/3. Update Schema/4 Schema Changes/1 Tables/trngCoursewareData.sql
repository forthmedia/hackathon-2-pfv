/* 2012-08-06 STCR P-REL109 Phase 2 */ 
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trngCoursewareData]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[trngCoursewareData](
		[CoursewareDataID] [int] IDENTITY(1,1) NOT NULL,
		[FileID] [int] NOT NULL,
		[PersonID] [int] NOT NULL,
		[ElementName] [varchar](100) NOT NULL,
		[ElementValue] [ntext] NULL,
		[ModuleID] [int] NULL,
		[Created] [datetime] NULL,
		[LastUpdated] [datetime] NULL,
	 	CONSTRAINT [PK_trngCoursewareData] PRIMARY KEY CLUSTERED (
			[CoursewareDataID] ASC
		)
	)

END
GO


/* WAB 2015-06-09 Add an index, gives fractionally better performance to relayScormWS.SCORMSetValue webservice call, although doesn't solve main problem which is just the general page response time */
IF NOT EXISTS (select * from sysindexes where id=object_id('trngCoursewareData') and name='trngCoursewareData_ModuleID_PersonID_ElementName')
	create index trngCoursewareData_ModuleID_PersonID_ElementName	on trngCoursewareData (ModuleID,personid,elementName)
