

exec generate_mrAuditDel @tablename='lead'
GO

/* NJH 2015/07/16 - for vEntityname */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LeadDel]') AND name = N'crmLeadID_idx')
BEGIN
	CREATE INDEX crmLeadID_idx ON LeadDel (crmLeadID);
END	
GO