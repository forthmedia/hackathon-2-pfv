

/* dropping the old version of the banner table - basing it on whether the column bannerText1 exists */
if exists (select 1 from information_schema.columns where table_name='banner' and column_name='bannerText1')
drop table banner

if not exists(select 1 from information_schema.tables where table_name='banner')
CREATE TABLE [dbo].[banner](
	[bannerID] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[targetUrl] [nvarchar](max) NULL,
	[heading_defaultTranslation] [nvarchar](255) NULL,
	[paragraph_defaultTranslation] [nvarchar](255) NULL,
	[button_defaultTranslation] [nvarchar](255) NULL,
	[cssClasses] [nvarchar](max) NULL,
	[backgroundFileID] [int] NULL,
	[containerPadding] [int] NULL,
	[headingFontColor] [nvarchar](7) NULL,
	[paragraphFontColor] [nvarchar](7) NULL,
	[hasRecordRightsBitMask] [int] DEFAULT 0,
	[lastUpdatedByPerson] [int] NOT NULL,
	[createdByPerson] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[Created] [datetime] NOT NULL CONSTRAINT [DF_Banner_Created]  DEFAULT (getdate()),
	[LastUpdatedBy] [int] NOT NULL,
	[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Banner_LastUpdated]  DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[bannerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



	DELETE FROM schematable WHERE entityName like 'banner'
	GO
	
	INSERT INTO schematablebase (
	    entityTypeID,
	    entityName,
	    NameExpression,
	    uniqueKey,
	    FlagsExist,
	    mnemonic,
	    ScreensExist,
	    CustomEntity,
	    in_vEntityName,
	    label
	  )
	  VALUES (
	    563,
	    'banner',
	    'title',
	    'bannerID',
	    1,
	    'BNR',
	    1,
	    0,
	    1,
	    'Banner'
	  )
	GO

