
if not exists(select 1 from sysobjects where name='relayActivity' and type='u')
CREATE TABLE [dbo].[relayActivity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[siteID] [int] NOT NULL,
	[databaseID] [int] NOT NULL,
	[action] [varchar](50) NOT NULL,
	[shareText] [nvarchar](250) NULL,
	[performedOnEntityID] [int] NOT NULL,
	[performedOnEntityTypeID] [int] NOT NULL,
	[performedByEntityID] [int] NOT NULL,
	[performedByEntityTypeID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_relayActivity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_relayActivity_created]'))
ALTER TABLE [dbo].[relayActivity] ADD  CONSTRAINT [DF_relayActivity_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_relayActivity_lastUpdated]'))
ALTER TABLE [dbo].[relayActivity] ADD  CONSTRAINT [DF_relayActivity_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

if not exists (select * from information_schema.columns where table_name='relayActivity' and column_name='url')  
	alter TABLE [dbo].relayActivity add [url] varchar(200) NULL
GO

if not exists (select * from information_schema.columns where table_name='relayActivity' and column_name='share')  
	alter TABLE [dbo].relayActivity add share bit not null default(1)
GO

if not exists (select * from information_schema.columns where table_name='relayActivity' and column_name='urlTitle')  
	alter TABLE [dbo].relayActivity add urlTitle nvarchar (150) null
GO