

/* create ScheduleEmailLogStatus table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleEmailLogStatus')
BEGIN
CREATE TABLE [dbo].[ScheduleEmailLogStatus](
	[ScheduleEmailLogStatusID] [nchar](10) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ScheduleEmailLogStatus] PRIMARY KEY CLUSTERED 
(
	[ScheduleEmailLogStatusID] ASC
)) ON [PRIMARY]
END
GO
