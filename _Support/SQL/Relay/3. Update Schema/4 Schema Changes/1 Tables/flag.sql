/* 
LID 5369 Make Flag and Flag Group Names/Desriptions nVarchar
WAB 2016-05-12 Added some if statements to prevent build failures
*/
if exists (select * from information_schema.columns where table_name = 'flag' and column_name = 'name' and data_type <> 'nvarchar')
BEGIN
	alter table flag alter column name nvarchar (250)
END	
if exists (select * from information_schema.columns where table_name = 'flag' and column_name = 'description' and data_type <> 'nvarchar')
BEGIN
	alter table flag alter column description nvarchar (160)
END	

/*
Also copied to base_RW831configurationTidyUp1.sql to allow flag records to be added before update scripts are run
*/
if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='showInConditionalEmails')
alter table flag add showInConditionalEmails bit default 0
GO

update flag set showInConditionalEmails=0 where showInConditionalEmails is null

alter table flag alter column showInConditionalEmails bit not null

if exists (select * from information_schema.columns where table_name = 'flag' and column_name = 'description' and character_Maximum_length < 160)
BEGIN
	alter table flag alter column [description] nvarchar(160)
END

/* required longer column length for flagTextID's.  Repeated PS change bringing into core.*/
if exists (select * from information_schema.columns where table_name = 'flag' and column_name = 'flagtextID' and character_Maximum_length < 160)
BEGIN
	alter table flag alter column flagtextID varchar(250)
END


/* change description to help text */
if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='helpText')
BEGIN
	alter table flag add helpText nvarchar(160) null
	alter table xFlag add helpText nvarchar(160) null
END
GO

if not exists (select 1 from flag where helpText is not null)
begin
	update flag set helpText = description where len(isNull(description,'')) > 0
end

/* WAB 2015-10-20 
allow for infinitely long formatting parameters (for PYW Tata)
*/
IF (select character_Maximum_Length from information_schema.columns where table_name = 'flag' and column_name = 'formattingParameters') <> - 1
BEGIN
	ALTER TABLE [dbo].[flag]
	ALTER COLUMN [formattingParameters] nvarchar(max)
END

if not exists (select 1 from information_schema.columns where table_name='flag' and column_name='name' and character_maximum_length =250 and is_nullable='no')
begin
	begin tran
        exec dropAndRecreateDependentObjects 'flag.name', 'drop'
		alter table flag alter column name nvarchar (250) not null

        exec dropAndRecreateDependentObjects 'flag.name', 'recreate'
    commit
end

if not exists (select 1 from information_schema.columns where table_name='flag' and column_name='flagGroupID' and is_nullable='no')
begin
	begin tran
        exec dropAndRecreateDependentObjects 'flag.flagGroupID', 'drop'
		alter table flag alter column flagGroupID int not null

        exec dropAndRecreateDependentObjects 'flag.flagGroupID', 'recreate'
    commit
end

alter table flag alter column created datetime not null
IF OBJECT_ID('DF_flag_created', 'D') IS NULL
alter table flag add constraint DF_flag_created default getDate() for created
alter table flag alter column lastUpdated datetime not null
IF OBJECT_ID('DF_flag_lastUpdated', 'D') IS NULL
alter table flag add constraint DF_flag_lastUpdated default getDate() for lastUpdated

if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='createdByPerson')
	alter table flag add createdByPerson int null
GO

update flag 
	set createdByPerson=u.personID
from flag f 
	inner join userGroup u on f.createdBy = u.userGroupID
where f.createdByPerson is null
 
update flag set createdByPerson=0 where createdByPerson is null

if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='createdByPerson' and is_nullable='No')
alter table flag alter column createdByPerson int not null

IF OBJECT_ID('DF_flag_createdByPerson', 'D') IS NULL
alter table flag add constraint DF_flag_createdByPerson default 0 for createdByPerson


if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='lastUpdatedByPerson')
	alter table flag add lastUpdatedByPerson int null
GO

update flag 
	set lastUpdatedByPerson=u.personID
from flag f 
	inner join userGroup u on f.lastUpdatedBy = u.userGroupID
where f.lastUpdatedByPerson is null

update flag set lastUpdatedByPerson=0 where lastUpdatedByPerson is null

if not exists(select 1 from information_schema.columns where table_name='flag' and column_name='lastUpdatedByPerson' and is_nullable='No')
alter table flag alter column lastUpdatedByPerson int not null

IF OBJECT_ID('DF_flag_lastUpdatedByPerson', 'D') IS NULL
alter table flag add constraint DF_flag_lastUpdatedByPerson default 0 for lastUpdatedByPerson