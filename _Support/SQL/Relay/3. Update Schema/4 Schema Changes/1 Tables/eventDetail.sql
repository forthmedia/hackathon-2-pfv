alter table eventdetail alter column title nvarchar(255)

/* Social Changes 2012/10/23 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'eventDetail') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'eventDetail' AND column_name='share') 
	ALTER TABLE eventDetail add share bit NOT NULL DEFAULT 1
GO

-- 2013-08-01 	MS		P-KAS026  - Allow internal users to hide or show register / unregister buttons on the portal.
IF	NOT EXISTS 	(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE	TABLE_NAME	= 'EventDetail' AND	COLUMN_NAME = 'hideRegisterForEventBtn')
BEGIN
	ALTER TABLE EventDetail ADD hideRegisterForEventBtn bit default(0)
END
GO

IF	NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE	TABLE_NAME	= 'EventDetail' AND	COLUMN_NAME = 'hideUnRegisterForEventBtn')
BEGIN
	ALTER TABLE EventDetail ADD hideUnRegisterForEventBtn bit default(0)
END
GO

if not exists (select 1 from information_schema.columns where table_name = 'EventDetail' and column_name = 'campaignID')
	alter table EventDetail add campaignID int null
GO

if exists (select 1 from information_schema.columns where table_name='EventDetail' and column_name='Agenda' and data_type!='nvarchar')
ALTER TABLE EventDetail ALTER COLUMN [Agenda] [nvarchar](max)
GO

exec generate_MRAudit @TableName = 'EventDetail'