
/*  WAB 2013-12-09 added index so that filesDel can be searched quickly in vEntityName view 
	Won't actually be built OOTB because filesDel does not exisr
*/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[filesDel]') AND name = N'filesID_Idx')

	CREATE NONCLUSTERED INDEX [filesID_Idx] ON [dbo].[filesDel]
	(
		[fileID] ASC
	) 
	INCLUDE (name)
	ON [PRIMARY]

GO