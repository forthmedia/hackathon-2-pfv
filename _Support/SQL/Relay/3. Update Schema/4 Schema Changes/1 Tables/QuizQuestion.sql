IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizQuestion]') AND type in (N'U'))
   BEGIN
	CREATE TABLE [dbo].[QuizQuestion] (
	[QuestionID] int IDENTITY(1,1) NOT NULL  
	, [QuestionTypeID] int  NOT NULL  
	, [QuestionPoolID] int  NOT NULL  /* Question Pool CR */
	, [QuestionRef] nvarchar(100)  NULL  
	/* , [QuestionPhraseTextID] varchar(100)  NULL  */
	, [title_defaultTranslation] nvarchar (200) NULL
	, [Created] datetime  NULL  
	, [CreatedBy] int  NULL  
	, [LastUpdated] datetime  NULL  
	, [LastUpdatedBy] int  NULL  
	)


ALTER TABLE [dbo].[QuizQuestion] ADD CONSTRAINT [QuizQuestion_PK] PRIMARY KEY CLUSTERED (
[QuestionID]
)
END
GO

IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'QuizQuestion') AND
NOT EXISTS (select 1 from information_schema.columns WHERE table_name= 'QuizQuestion' AND column_name='sortOrder')
ALTER TABLE QuizQuestion add sortOrder int NOT NULL default 0
GO