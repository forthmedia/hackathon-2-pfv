IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fileFamily]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[fileFamily](
	[fileFamilyID] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_fileFamily] PRIMARY KEY CLUSTERED 
(
	[fileFamilyID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[fileFamily] ADD  CONSTRAINT [DF_fileFamily_created]  DEFAULT (getdate()) FOR [created]

ALTER TABLE [dbo].[fileFamily] ADD  CONSTRAINT [DF_fileFamily_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
end
GO
