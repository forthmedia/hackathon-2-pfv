/*select 'select rwo.ID as connectorRWObjectID,o.ID as connectorObjectID,'''+m.entityColumn+''' as entityColumn,'''+m.objectColumn+''' as objectColumn,'''+m.mappingType+''' as mappingType,'''+isNull(m.direction,null)+''' as direction,'''+cast(m.[required] as varchar)+''' as required,'''+isNull(m.mappedEntityColumn,null) +''' as mappedEntityColumn,'+cast(m.isRemoteID as varchar)+' as isRemoteID,'+cast(m.active as varchar)+' as active,'+cast(m.canEdit as varchar)+' as canEdit,'+cast(m.emptyStringAsNull as varchar)+' as emptyStringAsNull
from connectorObject o
	join connectorObject rwo
on o.direction=''I'' and o.object = ''' + object +'''and rwo.direction=''E'' and rwo.object = '''+entityName+'''
union'
from vconnectorMapping v
inner join connectorMapping m on v.ID = m.ID*/

/****** Object:  Table [dbo].[connectorMapping]    Script Date: 05/15/2014 10:12:29 ******/
IF not exists (select 1 from sysobjects where name='connectorMapping' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorMapping](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorObjectID_relayware] [int] NULL,
		[connectorObjectID_remote] [int] NULL,
		[column_relayware] [varchar](50) NOT NULL,
		[column_remote] [varchar](50) NOT NULL,
		[direction] [char](1) NULL,
		[mappedRelaywareColumn] [varchar](50) NULL,
		[mappingType] [varchar](20) NOT NULL,
		[required] [bit] NOT NULL,
		[isRemoteID] [bit] NOT NULL,
		[active] [bit] NOT NULL,
		[emptyStringAsNull] [bit] NOT NULL,
		--[foreignKeyMappingID] [int] NULL,
		[canEdit] [bit] NOT NULL,
		[notes] [nvarchar](max) NULL,
		[created] [datetime] NOT NULL,
		[createdBy] [int] NOT NULL,
		[lastUpdated] [datetime] NOT NULL,
		[lastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_connectorMapping] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_mappingType]  DEFAULT ('Field') FOR [mappingType]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_required]  DEFAULT ((0)) FOR [required]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_isRemoteID]  DEFAULT ((0)) FOR [isRemoteID]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_active]  DEFAULT ((1)) FOR [active]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_emptyStringAsNull]  DEFAULT ((0)) FOR [emptyStringAsNull]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_canEdit]  DEFAULT ((1)) FOR [canEdit]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_created]  DEFAULT (getdate()) FOR [created]

	ALTER TABLE [dbo].[connectorMapping] ADD  CONSTRAINT [DF_connectorMapping_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
END

/*
ALTER TABLE [dbo].[connectorMapping]  WITH CHECK ADD  CONSTRAINT [FK_connectorMapping_connectorMapping] FOREIGN KEY([foreignKeyMappingID])
REFERENCES [dbo].[connectorMapping] ([ID])

ALTER TABLE [dbo].[connectorMapping] CHECK CONSTRAINT [FK_connectorMapping_connectorMapping]
*/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[connectorMapping]') AND name = N'IX_connectorMapping_unique_entity_object_entityColumn_objectColumn')
DROP INDEX [IX_connectorMapping_unique_entity_object_entityColumn_objectColumn] ON [dbo].[connectorMapping] WITH ( ONLINE = OFF )
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_connectorMapping_unique_entity_object_entityColumn_objectColumn] ON [dbo].[connectorMapping] 
(
	[connectorObjectID_remote] ASC,
	[connectorObjectID_relayware] ASC,
	[column_relayware] ASC,
	[column_remote] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

alter table connectorMapping alter column column_remote varchar(100)

if not exists (select 1 from information_schema.columns where table_name='connectorMapping' and column_name='column_relayware' and character_maximum_length=250)
alter table connectorMapping alter column column_relayware varchar(250) not null
GO

/* PROD2016-2651 Connector: Option to set a default value for fields when value is null */
if not exists (select 1 from information_schema.columns where table_name='connectorMapping' and column_name='defaultImportValue')
alter table connectorMapping add defaultImportValue nvarchar(500)
GO

if not exists (select 1 from information_schema.columns where table_name='connectorMapping' and column_name='defaultExportValue')
alter table connectorMapping add defaultExportValue nvarchar(500)

exec Generate_MRAuditDel @tablename = 'connectorMapping'