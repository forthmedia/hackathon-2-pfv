IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[approvalStatusType]') AND type in (N'U'))

	CREATE TABLE dbo.approvalStatusType
		(
		approvalStatusTypeID int NOT NULL 
			IDENTITY (1, 1)
			CONSTRAINT [PK_approvalStatusType] PRIMARY KEY CLUSTERED,
		approvalStatusTextID nvarchar(50) NOT NULL
		)  ON [PRIMARY]

GO

if not exists(select approvalStatusTextID from approvalStatusType where approvalStatusTextID = 'partialyApproved')
	insert into approvalStatusType
	select 'partialyApproved'
go
if not exists(select approvalStatusTextID from approvalStatusType where approvalStatusTextID = 'approved')
	insert into approvalStatusType
	select 'approved'
go
if not exists(select approvalStatusTextID from approvalStatusType where approvalStatusTextID = 'rejected')
	insert into approvalStatusType
	select 'rejected'
go
if not exists(select approvalStatusTextID from approvalStatusType where approvalStatusTextID = 'revisionNeeded')
	insert into approvalStatusType
	select 'revisionNeeded'
go
if not exists(select approvalStatusTextID from approvalStatusType where approvalStatusTextID = 'approvalRequested')
	insert into approvalStatusType
	select 'approvalRequested'
go