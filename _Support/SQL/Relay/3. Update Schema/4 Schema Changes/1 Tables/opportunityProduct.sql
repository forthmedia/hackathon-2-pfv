
if not exists (select 1 from sysObjects where name='opportunityProduct' and type='U')
BEGIN
/*     This procedure has to be run in a transaction.  
        This means that if anything goes wrong you are not left in Limbo 
		Deal with vEntityname and the unique crmID index
    */
    begin tran

		/*if exists (select 1 from sysobjects where name = 'vEntityName' and xtype= 'U')
			drop view vEntityName*/
			
		IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[oppproducts]') AND name = N'crmOppProductID_unique')
			drop INDEX oppproducts.crmOppProductID_unique
			
		if exists (select 1 from sysobjects where name = 'oppProducts_MRAudit' and xtype= 'TR')
			drop trigger oppProducts_MRAudit	
		
		update schematable set entityname='opportunityProduct' where entityname='oppProducts'
		
        /*     Now do what you need to do */    
        exec sp_rename 'oppProducts', 'opportunityProduct'
        exec sp_rename 'opportunityProduct.oppProductID', 'opportunityProductID'
        exec sp_rename 'opportunityProduct.crmOppProductID', 'crmOpportunityProductID'
        
        exec sp_rename 'oppProductsDel', 'opportunityProductDel'
        exec sp_rename 'opportunityProductDel.oppProductID', 'opportunityProductID'
        exec sp_rename 'opportunityProductDel.crmOppProductID', 'crmOpportunityProductID'
        
        
			
            
        update modEntityDef set tablename='opportunityProduct' where tablename='oppProducts'
        update modEntityDef set FieldName='opportunityProductID' where tablename='opportunityProduct' and FieldName='oppProductID';
        update modEntityDef set FieldName='crmOpportunityProductID' where tablename='opportunityProduct' and FieldName='crmOppProductID';        
        
        exec generate_mrAudit @tablename='opportunityProduct'
        
			
        /*if exists (select 1 from sysobjects where name = 'generate_vEntityName' and xtype= 'P')
			exec generate_vEntityName*/
			
		CREATE unique INDEX crmOpportunityProductID_unique ON opportunityProduct(crmOpportunityProductID) WHERE crmOpportunityProductID IS NOT NULL

		/* NJH 2015/12/18 - renamed oppProducts to opportunityProduct - deal with change in connector */
		update connectorObject set object='opportunityProduct' where object='oppProducts'
		update connectorMapping set column_relayware='opportunityProductID' where column_relayware='oppProductID'
		update connectorMapping set column_relayware='crmOpportunityProductID' where column_relayware='crmOppProductID'
		update flagGroup set flagGroupTextID='OpportunityProductConnectorSynchStatus' where flagGroupTextID='OppProductsConnectorSynchStatus'
		update flag set flagTextID=replace(flagTextID,'oppProducts','opportunityProduct') where flagTextID like 'oppProducts%'
			and flagGroupID = (select flagGroupId from flagGroup where flagGroupTextID='OpportunityProductConnectorSynchStatus')
			
		if exists (select 1 from information_schema.tables where table_name='vConnector_salesforce_imp_oppProducts_opportunityLineItem')
			drop view vConnector_salesforce_imp_oppProducts_opportunityLineItem

		if exists (select 1 from information_schema.tables where table_name='vConnector_salesforce_exp_oppProducts_opportunityLineItem')
			drop view vConnector_salesforce_exp_oppProducts_opportunityLineItem
    commit
END

/* WAB 2016-05-11 
Fixing upgrade failures.
There seem to be some live systems which have had oppportunityProduct created but not had these three upgrade statements (which are copied from above) run
This is manifesting itself with an error in RW2015_data.sql where unique CRMID indexes are created against tables mentioned in the connectorObjectTable.
Tries to create unique index against oppProducts, which is actually a view
*/
IF EXISTS (select 1 from connectorObject where object='oppProducts')
BEGIN
		update connectorObject set object='opportunityProduct' where object='oppProducts'
		update connectorMapping set column_relayware='opportunityProductID' where column_relayware='oppProductID'
		update connectorMapping set column_relayware='crmOpportunityProductID' where column_relayware='crmOppProductID'
END		

/* sugar 448757 - performance enhancements */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[opportunityProduct]') AND name = N'idx_opportunityProduct_opportunityID')
CREATE NONCLUSTERED INDEX [idx_opportunityProduct_opportunityID]
ON [dbo].[opportunityProduct] ([OpportunityID])