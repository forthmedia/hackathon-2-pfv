if not exists (select 1 from information_schema.columns where table_name = 'integerMultipleFlagData' and column_name = 'lastUpdatedByPerson') 
	alter table integerMultipleFlagData add lastUpdatedByPerson int 
GO


/* 2012-09-10 PPB Case 430540 START make audit columns NOT NULLable because a trigger now tries to write the value back to the main entity table */

if exists (select * from dbo.sysobjects where id = object_id(N'integerMultipleFlagData_MRAudit') and OBJECTPROPERTY(id, N'IsTrigger') = 1) 
DISABLE TRIGGER integerMultipleFlagData_MRAudit ON integerMultipleFlagData
GO
update integerMultipleFlagData set created = '1/1/1900'	where created IS NULL
update integerMultipleFlagData set createdBy = 0 where createdBy IS NULL
update integerMultipleFlagData set lastUpdated = created where lastUpdated IS NULL
update integerMultipleFlagData set lastUpdatedBy = 0 where lastUpdatedBy IS NULL
update integerMultipleFlagData set lastUpdatedByPerson = 0 where lastUpdatedByPerson IS NULL
GO
if exists (select * from dbo.sysobjects where id = object_id(N'integerMultipleFlagData_MRAudit') and OBJECTPROPERTY(id, N'IsTrigger') = 1) 
ENABLE TRIGGER integerMultipleFlagData_MRAudit ON integerMultipleFlagData
GO

ALTER TABLE integerMultipleFlagData alter column created datetime NOT NULL
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF__integerMultipleFlagData__created') AND xtype = 'D')
	ALTER TABLE [dbo].integerMultipleFlagData ADD  CONSTRAINT [DF__integerMultipleFlagData__created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE integerMultipleFlagData alter column createdBy int NOT NULL
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF__integerMultipleFlagData__createdBy') AND xtype = 'D')
	ALTER TABLE [dbo].integerMultipleFlagData ADD  CONSTRAINT [DF__integerMultipleFlagData__createdBy]  DEFAULT ((0)) FOR [createdBy]
GO

ALTER TABLE integerMultipleFlagData alter column lastUpdated datetime NOT NULL
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF__integerMultipleFlagData__lastUpdated') AND xtype = 'D')
	ALTER TABLE [dbo].integerMultipleFlagData ADD  CONSTRAINT [DF__integerMultipleFlagData__lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

ALTER TABLE integerMultipleFlagData alter column lastUpdatedBy int NOT NULL
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF__integerMultipleFlagData__lastUpdatedBy') AND xtype = 'D')
	ALTER TABLE [dbo].integerMultipleFlagData ADD  CONSTRAINT [DF__integerMultipleFlagData__lastUpdatedBy]  DEFAULT ((0)) FOR [lastUpdatedBy]
GO

ALTER TABLE integerMultipleFlagData alter column lastUpdatedByPerson int NOT NULL
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF__integerMultipleFlagData__lastUpdatedByPerson') AND xtype = 'D')
	ALTER TABLE [dbo].integerMultipleFlagData ADD  CONSTRAINT [DF__integerMultipleFlagData__lastUpdatedByPerson]  DEFAULT ((0)) FOR [lastUpdatedByPerson]
GO
/* 2012-09-10 PPB Case 430540 END */ 

