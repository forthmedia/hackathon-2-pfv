
if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='crmLeadID')
alter table lead add crmLeadID varchar(50) null

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='fax')
alter table lead drop column fax

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='transactionNo')
alter table lead drop column transactionNo

alter table lead alter column countryID int not null

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='address7')
alter table lead drop column address7

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='address8')
alter table lead drop column address8

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='address9')
alter table lead drop column address9

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='town')
alter table lead drop column town

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='telephone')
alter table lead drop column telephone

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='prefPartner')
alter table lead drop column prefPartner

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='personID')
alter table lead drop column personID

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='custom2')
alter table lead drop column custom2

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='custom3')
alter table lead drop column custom3

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='custom4')
alter table lead drop column custom4

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='source')
alter table lead drop column source

GO


/* WAB 2014-12-02  wrapped some alter statements in IFs */

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='sponsorPersonID')
	EXEC sp_dropColumn 'lead', 'sponsorPersonID'

GO

/*
if not exists (select 1 from information_schema.columns where table_name='leadDel' and column_name='crmLeadID')
	alter table leadDel add crmLeadID varchar(20) null
*/

IF EXISTS (select 1 from information_schema.columns where table_name = 'lead' and column_name = 'lastname' and (character_maximum_length < 100 or data_type <> 'nvarchar'))
	alter table lead alter column lastname nvarchar(100) not null


IF EXISTS (select 1 from information_schema.columns where table_name = 'lead' and column_name = 'email' and (character_maximum_length < 100 or data_type <> 'nvarchar'))
	alter table lead alter column email nvarchar(100) not null


/* WAB 2014-12-02  wrapped alter statements in IF and code to deal with dependent view (vEntityName)  */
IF EXISTS (select 1 from information_schema.columns where table_name = 'lead' and column_name = 'company' and (character_maximum_length < 100 or data_type <> 'nvarchar'))
BEGIN
	begin tran
		exec dropAndRecreateDependentObjects 'lead.company', 'drop'
			alter table lead alter column company nvarchar(100) not null
		exec dropAndRecreateDependentObjects 'lead.company', 'recreate'
	commit
END

GO
/* add the source column - set existing records to be a source of 'locator' */
if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='sourceID')
alter table lead add sourceID int null
GO

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='convertedLocationID')
alter table lead add convertedLocationID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='convertedPersonID')
alter table lead add convertedPersonID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='convertedOpportunityID')
alter table lead add convertedOpportunityID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='description')
alter table lead add description nvarchar(2000) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='officePhone')
alter table lead add officePhone varchar(20) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='mobilePhone')
alter table lead add mobilePhone varchar(20) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='website')
alter table lead add website varchar(100) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='partnerLocationID')
alter table lead add partnerLocationID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='partnerSalesPersonID')
alter table lead add partnerSalesPersonID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='partnerSalesManagerPersonID')
alter table lead add partnerSalesManagerPersonID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='distiLocationID')
alter table lead add distiLocationID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='vendorAccountManagerPersonID')
alter table lead add vendorAccountManagerPersonID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='vendorSalesManagerPersonID')
alter table lead add vendorSalesManagerPersonID int null

/*if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='sponsorPersonID')
alter table lead add sponsorPersonID int null*/

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='approvalStatusID')
alter table lead add approvalStatusID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='acceptedByPartner')
alter table lead add acceptedByPartner bit null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='acceptedByPartnerDate')
alter table lead add acceptedByPartnerDate datetime null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='viewedByPartnerDate')
alter table lead add viewedByPartnerDate datetime null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='currency')
alter table lead add currency varchar(3) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='annualRevenue')
alter table lead add annualRevenue decimal(10,2) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='rejectionReason')
alter table lead add rejectionReason nvarchar(2000) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='progressID')
alter table lead add progressID int null
GO

if exists (select 1 from lead where progressID is null)
begin
	declare @lookupId int
	select @lookupId = lookupID from lookupList where fieldname='leadProgressID' and itemText='Open - Not Contacted'
	select @lookupID
	if (@lookupId is null)
	begin
		select @lookupID = max(lookupID)+1 from lookupList
		insert into lookupList (lookupID,fieldname,itemText,extraInfo,isParent,isDefault,isLive,locked)
		values (@lookupID,'leadProgressID','Open - Not Contacted',0,0,0,1,0)
	end

	update lead set progressID=@lookupID
end
GO

alter table lead alter column progressID int not null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='campaignID')
alter table lead add campaignID int null


if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='lastUpdated')
alter table lead add lastUpdated datetime not null default(getDate())

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='lastUpdatedBy')
alter table lead add lastUpdatedBy int null
GO

if exists (select 1 from lead where lastUpdatedBy is null)
update lead set lastUpdatedBy=404 where lastUpdatedBy is null
GO
	
alter table lead alter column lastUpdatedBy int not null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='lastUpdatedByPerson')
alter table lead add lastUpdatedByPerson int null
GO

if exists (select 1 from lead where lastUpdatedByPerson is null)
update lead set lastUpdatedByPerson=0
GO
	
alter table lead alter column lastUpdatedByPerson int not null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='sponsorFirstname')
alter table lead add sponsorFirstname varchar(50) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='sponsorLastname')
alter table lead add sponsorLastname varchar(50) null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='enquiryType')
alter table lead add enquiryType varchar(50) null

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='lead' AND column_name='active')
BEGIN
	ALTER TABLE [dbo].[lead] ADD active BIT NULL 
END

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='distiContactPersonID')
alter table lead add distiContactPersonID int null

if not exists (select 1 from information_schema.columns where table_name='lead' and column_name='createdByPerson')
alter table lead add createdByPerson int null
GO

/* NJH 2015/09/17  - increase the field by 2 extra characters
	WAb 2015-11-23 added if statement
 */
if exists (select * from information_schema.columns where table_name = 'lead' and column_name = 'annualRevenue' and numeric_precision < 12)
BEGIN
	alter table lead alter column annualRevenue decimal(12,2)
END	

exec generate_mrAudit @tablename='lead'
GO

/* NJH 2015/06/17 */
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Lead]') AND name = N'crmLeadID_unique')
DROP INDEX [dbo].[Lead].[crmLeadID_unique]
GO

if exists (select 1 from information_schema.columns where table_name='lead' and column_name='crmLeadID')
 begin tran
   exec dropAndRecreateDependentObjects 'lead.crmLeadID', 'drop'
   alter table lead alter column crmLeadID varchar(50) null
   exec dropAndRecreateDependentObjects 'lead.crmLeadID', 'recreate'
 commit
GO

/****** Object:  Index [crmLocID_unique]    Script Date: 06/17/2015 16:29:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [crmLeadID_unique] ON [dbo].[Lead] 
(
	[crmLeadID] ASC
)
WHERE ([crmLeadID] IS NOT NULL)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO