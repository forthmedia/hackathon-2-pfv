

if not exists(select 1 from information_schema.columns where table_name='selection' and column_name='followEntityTypeID')
BEGIN
alter table selection add followEntityTypeID int null
END

if not exists(select 1 from information_schema.columns where table_name='selection' and column_name='followEntityID')
BEGIN
alter table selection add followEntityID int null
END