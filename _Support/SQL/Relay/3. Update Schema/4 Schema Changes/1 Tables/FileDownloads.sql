IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='FileDownloads' AND column_name='referringEntityTypeID') 	
Begin
	ALTER TABLE FileDownloads
	ADD referringEntityTypeID int null
End
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='FileDownloads' AND column_name='referringEntityID') 	
Begin
	ALTER TABLE FileDownloads
	ADD referringEntityID int null
End
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='FileDownloads' AND column_name='referringEID') 	
Begin
	ALTER TABLE FileDownloads
	ADD referringEID int null
End
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='FileDownloads' AND column_name='visitid') 	
Begin
	ALTER TABLE FileDownloads
	ADD visitid int null
End
GO