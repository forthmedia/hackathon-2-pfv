

if not exists (select 1 from UserLinkType where linkType = 'crossTabReport')
	Insert into UserLinkType (linkTypeID,linkType) values (3,'crossTabReport')

if not exists (select 1 from UserLinkType where linkType = 'jasperReport')
	Insert into UserLinkType (linkTypeID,linkType) values (4,'jasperReport')