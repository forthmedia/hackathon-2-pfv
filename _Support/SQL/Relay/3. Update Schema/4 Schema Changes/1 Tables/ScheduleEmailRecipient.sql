
/* create ScheduleEmailRecipient table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleEmailRecipient')
BEGIN
CREATE TABLE [dbo].[ScheduleEmailRecipient](
	[ScheduleEmailRecipientID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [int] NULL,
	[RecipientValue] [nvarchar](50) NULL,
 CONSTRAINT [PK_ScheduleEmailRecipient] PRIMARY KEY CLUSTERED 
(
	[ScheduleEmailRecipientID] ASC
)) ON [PRIMARY]
END
GO