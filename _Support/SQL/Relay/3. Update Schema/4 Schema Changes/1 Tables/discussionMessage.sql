IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[discussionMessage]') AND type in (N'U'))

CREATE TABLE [dbo].[discussionMessage] (	
	[discussionMessageID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[discussionGroupID] [int] NOT NULL,
	[title] [nvarchar] (255) NOT NULL,
	[message] [nvarchar] (max) NOT NULL,	
	[personID] [int] NOT NULL,
	[created] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [int] NOT NULL DEFAULT (0),
	[lastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL DEFAULT (0)
)
GO

if not exists (select 1 from information_schema.columns where table_schema = 'dbo' and table_name = 'discussionMessage' and column_name = 'active') 
	alter table [dbo].[discussionMessage] add active bit not null default 1
GO