
if not exists (select * from sysobjects where name='DataTransferDirectionType')
begin
CREATE TABLE [dbo].[DataTransferDirectionType](
	[directionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DataTransferDirectionType] PRIMARY KEY CLUSTERED 
(
	[directionTypeID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
End
GO