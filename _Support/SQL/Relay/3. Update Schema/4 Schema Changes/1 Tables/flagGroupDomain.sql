if not exists (select 1 from sysobjects where name='flagGroupDomain' and type='u')
Begin
/****** Object:  Table [dbo].[flagGroupDomain]    Script Date: 02/09/2011 17:24:49 ******/
CREATE TABLE [dbo].[flagGroupDomain](
	[flagGroupID] [int] NOT NULL,
	[domainTextID] [varchar](100) NOT NULL,
	[domainTableID] [varchar](100) NOT NULL,
	[created] [datetime] NULL,
	[createdBy] [int] NULL,
	[lastUpdated] [datetime] NULL,
	[lastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_flagGroupDomain] PRIMARY KEY CLUSTERED 
(
	[flagGroupID] ASC,
	[domainTextID] ASC,
	[domainTableID] ASC
)--WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[flagGroupDomain] ADD  CONSTRAINT [DF_flagGroupDomain_created]  DEFAULT (getdate()) FOR [created]
ALTER TABLE [dbo].[flagGroupDomain] ADD  CONSTRAINT [DF_flagGroupDomain_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]

END
