if not exists(select 1 from information_schema.tables where table_name='scoreManualAdjustment')
begin
	CREATE TABLE [dbo].[scoreManualAdjustment](
		[scoreAdjustmentID] [int] IDENTITY(1,1) NOT NULL,
		[scoreRuleGroupingID] [int],
		[score] [int]  NOT NULL,
		[comment] nvarchar(max),
		[effectiveDate] [datetime] NOT NULL, --when the adjustment is considered to have happened (even if it happened at a different time
		[personID] [int] NULL,
		[locationID] [int] NULL,
		[organisationID] [int] NULL,
		[created] [datetime] NOT NULL DEFAULT getdate(),
		[createdBy] [int] NOT NULL,
		[lastUpdated] [datetime] NOT NULL  DEFAULT getdate(),
		[lastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_scoreManualAdjustments] PRIMARY KEY CLUSTERED 
	(
		[scoreAdjustmentID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
			 VALUES (552, 'scoreManualAdjustment', 'scoreAdjustmentID', 0, 'SCMAA', 0);
	
end
GO

IF not EXISTS (SELECT 1 FROM sys.indexes  WHERE name='scoreManualAdjustment_organisationID_index' AND object_id = OBJECT_ID('[dbo].[scoreManualAdjustment]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX scoreManualAdjustment_organisationID_index
	ON dbo.scoreManualAdjustment (organisationID)
End


IF not EXISTS (SELECT 1 FROM sys.indexes  WHERE name='scoreManualAdjustment_scoreRuleGroupingID_index' AND object_id = OBJECT_ID('[dbo].[scoreManualAdjustment]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX scoreManualAdjustment_scoreRuleGroupingID_index
	ON dbo.scoreManualAdjustment (scoreRuleGroupingID)
End


