/*  
	WAB 2014-09-29, During CASE 439422
	get Rid of the emailPhrases table in relayserver database 
	table already exists in main db, but was not being used!
	also convert phrase column to nvarChar 
*/

IF exists (select * from information_schema.columns where table_name = 'emailPhrases' and column_name = 'phrase' and data_Type = 'varchar')
BEGIN
	alter table emailPhrases alter column phrase nvarchar(255)
END

IF Exists (select 1 from sys.databases where name = 'relayserver')
begin
	if exists (select * from relayserver.information_schema.tables where table_name = 'emailphrases')
	BEGIN
		insert into emailPhrases (phrase,subjectlineonly,action,commstatus,flagTextID,sortorder,active,feedback)
		select rs.phrase,rs.subjectlineonly,rs.action,rs.commstatus,rs.flagTextID,rs.sortorder,rs.active,rs.feedback
		from relayserver.dbo.emailPhrases  rs
			left join 
			emailPhrases   p
			
			on rs.phrase = p.phrase
	
		where p.phrase is null
	
	
	END
end

/* regExp column not being used, so delete */
exec sp_dropColumn @tablename='emailPhrases', @columnName = 'regExp'


