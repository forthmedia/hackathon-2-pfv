IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundRequestActivityApproval' AND column_name='CoFundingAmount') 
alter table fundRequestActivityApproval add CoFundingAmount decimal(18,2) 
GO
