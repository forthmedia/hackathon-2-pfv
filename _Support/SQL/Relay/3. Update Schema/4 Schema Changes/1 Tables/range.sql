IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Range]') AND type in (N'U'))

CREATE TABLE [dbo].[range](
	[rangeID] [int] IDENTITY(1,1) NOT NULL,
	[rangeValue] [int] NOT NULL,
	[range]  AS ((CONVERT([varchar],[lowerRange],(0))+case when [upperRange] IS NULL then '+' else '-' end)+isnull(CONVERT([varchar],[upperRange],0),'')),
	[lowerRange] [int] NOT NULL,
	[upperRange] [int] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_Range] PRIMARY KEY CLUSTERED 
(
	[rangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists(select 1 from sysconstraints where constid=OBJECT_ID(N'[dbo].[DF_Range_created]'))
ALTER TABLE [dbo].[Range] ADD  CONSTRAINT [DF_Range_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists(select 1 from sysconstraints where constid=OBJECT_ID(N'[dbo].[DF_Range_lastUpdated]'))
ALTER TABLE [dbo].[Range] ADD  CONSTRAINT [DF_Range_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

if exists (select 1 from sys.computed_columns where name='range' and object_id=object_id('range') and definition like '%((CONVERT([[]varchar],[[]lowerRange],(0))+case when [[]upperRange] IS NULL then ''+'' else ''-'' end)+isnull(CONVERT([[]varchar],[[]upperRange],(0)),''''))%')
begin
	alter table range drop column [range]
	alter table range add range as ((CONVERT([varchar],REPLACE(CONVERT(VARCHAR,CONVERT(MONEY,lowerRange),1), '.00',''))+case when [upperRange] IS NULL then '+' else '-' end)+isnull(CONVERT([varchar],REPLACE(CONVERT(VARCHAR,CONVERT(MONEY,upperRange),1), '.00',''),(0)),''))
end