IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizTakenAnswer]') AND type in (N'U'))
   BEGIN
	CREATE TABLE [dbo].[QuizTakenAnswer] (
	[QuizTakenAnswerID] int IDENTITY(1,1) NOT NULL  
	, [QuizTakenQuestionID] int  NULL  
	--, [QuizTakenID] int  NULL  
	, [AnswerID] int  NOT NULL  
	, [MatchingAnswerID] int  NULL  
	, [AnsweredAt] datetime  NULL  
	, [ScoreGiven] numeric(9,3)  NOT NULL  
	, [AnswerTextGiven] nvarchar(4000)  NULL  
	, [MatchingAnswerTextGiven] nvarchar(4000)  NULL  
	)

ALTER TABLE [dbo].[QuizTakenAnswer] ADD CONSTRAINT [QuizTakenAnswer_PK] PRIMARY KEY CLUSTERED (
[QuizTakenAnswerID]
)
END
GO

IF NOT EXISTS (select * from sysindexes where id=object_id('QuizTakenAnswer') and name='idx_QuizTakenQuestionID_AnswerID')
	CREATE NONCLUSTERED INDEX [idx_QuizTakenQuestionID_AnswerID]
		ON [dbo].[QuizTakenAnswer] ([QuizTakenQuestionID],[AnswerID])
		INCLUDE ([QuizTakenAnswerID],[ScoreGiven])
GO
