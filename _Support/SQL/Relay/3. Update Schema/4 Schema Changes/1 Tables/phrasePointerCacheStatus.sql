/* WAB 2011/01/17
	Nat Discovered a bug in my code
	Needed to add an extra column to the phrasePointerCacheStatus table
	easiest way to do this was drop the table if the column did not exist 

*/
if exists (select 1 from sysobjects where name = 'phrasePointerCacheStatus'  ) and not exists (select 1 from information_schema.columns where table_name ='phrasePointerCacheStatus' and column_Name = 'phraseTextID')
BEGIN
	drop table phrasePointerCacheStatus
END

GO
if not exists (select 1 from sysobjects where name = 'phrasePointerCacheStatus')
BEGIN
CREATE TABLE [dbo].[phrasePointerCacheStatus](
	[entityTypeId] [int] NOT NULL,
	[phraseTextID] varchar(100) NOT NULL,
	[languageid] [int] NOT NULL,
	[countryid] [int] NOT NULL,
 CONSTRAINT [phrasePointerCacheStatus_PK] PRIMARY KEY CLUSTERED 
(
	[entityTypeId] ASC,
	[phraseTextID] ASC ,
	[languageid] ASC,
	[countryid] ASC
)
) ON [PRIMARY]
END
GO


