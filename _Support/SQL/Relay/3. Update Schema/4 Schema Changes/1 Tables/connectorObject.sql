
/*
select 'insert into connectorObject ([connectorType],[object],[direction],[active],[allowDeletions],[lastSuccessfulSynch],[whereClause],[parentObjectID],[sortOrder]) VALUES ('''+connectorType+''','''+object+''','''+direction+''','+cast(active as varchar)+','+cast(allowDeletions as varchar)+',getDate(),'+isNull(whereClause,'null')+','+isNull(cast(parentObjectID as varchar),'null') +','+isNull(cast(sortOrder as varchar),'null')+')'
from connectorObject*/

/****** Object:  Table [dbo].[connectorObject]    Script Date: 05/15/2014 10:29:54 ******/
IF not exists (select 1 from sysobjects where name='connectorObject' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorObject](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorType] [varchar](10) NOT NULL,
		[object] [varchar](30) NOT NULL,
		[relayware] [bit] NOT NULL,
		[active] [bit] NOT NULL,
		[synch] [bit] NOT NULL,
		[allowDeletions] [bit] NOT NULL,
		[lastSuccessfulSynch] [datetime] NULL,
		[whereClause] [varchar](max) NULL,
		[parentObjectID] [int] NULL,
		[sortOrder] [int] NULL,
		[synchErrorID] [int] NULL,
		[notes] [nvarchar](max) NULL,
		[lastUpdated] [datetime] NOT NULL,
		[lastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_connectorObject] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_synch]  DEFAULT ((1)) FOR [synch]

	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_active]  DEFAULT ((1)) FOR [active]
	
	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_relayware]  DEFAULT ((1)) FOR [relayware]

	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_allowDeletions]  DEFAULT ((0)) FOR [allowDeletions]

	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_lastSuccessfulRun]  DEFAULT (getdate()) FOR [lastSuccessfulSynch]
	
	ALTER TABLE [dbo].[connectorObject] ADD  CONSTRAINT [DF_connectorObject_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
END


/* WAB 2014-12-09 Some versions of this table were created without the synch, notes and lastUpdated... columns */
if not exists (select 1 from information_schema.COLUMNS where column_name = 'synch' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add	[synch] [bit] not null default 1
	alter table connectorObjectDel add	[synch] [bit] not null default 1	
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'notes' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [notes] [nvarchar](max) NULL
	alter table connectorObjectDel add [notes] [nvarchar](max) NULL
	
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdated' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [lastUpdated] [datetime] NOT NULL default 0
	alter table connectorObjectDel add [lastUpdated] [datetime] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdated'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdated'
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdatedBy' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [lastUpdatedBy] [int] NOT NULL default 0
	alter table connectorObjectDel add [lastUpdatedBy] [int] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdatedBy'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdatedBy'
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdatedByPerson' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add	[lastUpdatedByPerson] [int] NOT NULL default 0
	alter table connectorObjectDel add	[lastUpdatedByPerson] [int] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdatedByPerson'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdatedByPerson'

END

		
/* store the post process ID against the connector object, rather than using naming convention */
if not exists(select 1 from information_schema.columns where table_name='connectorObject' and column_name='postProcessID')
alter table connectorObject add postProcessID int null


IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[connectorObject]') AND name = N'IX_connectorObject_object_relayware_connectorType_unique')
BEGIN
CREATE UNIQUE NONCLUSTERED INDEX [IX_connectorObject_object_relayware_connectorType_unique] ON [dbo].[connectorObject] 
(
	[object] ASC,
	[relayware] ASC,
	[connectorType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END

GO

/* Would like to work this out dynamically at some point, but for now store it. Hopefully we can get rid of it later. it stores the field on the current object
	that is the foreign key to the parent object */
if not exists (select 1 from information_schema.columns where table_name='connectorObject' and column_name='parentKeyColumn')
	alter table connectorObject add parentKeyColumn varchar(50) null
	
alter table connectorObject alter column [object] varchar(50) not null


/* WAB 2014-12-09 Some versions of this table were created without the synch, notes and lastUpdated... columns */
if not exists (select 1 from information_schema.COLUMNS where column_name = 'synch' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add	[synch] [bit] not null default 1
	alter table connectorObjectDel add	[synch] [bit] not null default 1	
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'notes' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [notes] [nvarchar](max) NULL
	alter table connectorObjectDel add [notes] [nvarchar](max) NULL
	
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdated' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [lastUpdated] [datetime] NOT NULL default 0
	alter table connectorObjectDel add [lastUpdated] [datetime] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdated'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdated'
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdatedBy' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add [lastUpdatedBy] [int] NOT NULL default 0
	alter table connectorObjectDel add [lastUpdatedBy] [int] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdatedBy'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdatedBy'
END

if not exists (select 1 from information_schema.COLUMNS where column_name = 'lastUpdatedByPerson' and TABLE_NAME = 'connectorObject')
BEGIN
	alter table connectorObject add	[lastUpdatedByPerson] [int] NOT NULL default 0
	alter table connectorObjectDel add	[lastUpdatedByPerson] [int] NOT NULL default 0
	exec sp_dropColumnConstraints @tablename = 'connectorObject', @columnName = 'LastUpdatedByPerson'
	exec sp_dropColumnConstraints @tablename = 'connectorObjectDel', @columnName = 'LastUpdatedByPerson'

END

/* NJH 2016/03/02 JIRA PROD2016-472 connector - synching attachments */
if not exists (select 1 from information_schema.columns where table_name='connectorObject' and column_name='crmKeyPrefix')
	alter table connectorObject add crmKeyPrefix varchar(5)
GO
		
exec Generate_MRAuditDel @tablename = 'connectorObject'
		