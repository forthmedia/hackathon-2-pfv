

IF object_id('token') is null   
BEGIN
	CREATE TABLE token(
	tokenID int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	tokenType nVarChar(56) NOT NULL,
	token nVarChar(400) NOT NULL,
	personID int Null,
	metadata nvarchar(MAX) null,
	created dateTime NOT NULL,
	expiryDate dateTime NULL);
	
	CREATE INDEX tokenIndex
	ON token (token);
	
	CREATE INDEX tokenTypeIndex
	ON token (tokenType);
END
