/* 	WAB 2016-03-17	PROD2016-118  Housekeeping Improvements
	New design of housekeeping log
	Want to keep old table for the time being to prevent backwards compatibility problems on dev sites, so will clear it out and create a separate table for new code
	In fullness of time might get rid of old one and rename the new one back to houseKeepingLog
*/
delete from houseKeepingLog


IF not exists (select 1 from information_schema.tables where table_name = 'housekeepingLog_')
BEGIN
	create table houseKeepingLog_
	(
		housekeepingLogID int identity (1,1),
		housekeepingid int,
		coldfusionInstanceID int,
		lastRunTime  datetime,
		nextRunTime datetime,
		isOK bit default 0,
		metadata nvarchar(max),
	)
END

