
if not exists(select 1 from sysobjects where name='relayActivityShare' and type='u')
	CREATE TABLE [dbo].[relayActivityShare](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[relayActivityID] [int] NOT NULL,
		[serviceID] [int] NOT NULL,
		[shared] [bit] NOT NULL,
		[created] [datetime] NOT NULL,
		[createdBy] [int] NOT NULL,
	 CONSTRAINT [PK_relayActivityShare] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	) ON [PRIMARY]
	) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[FK_relayActivityShare_relayActivity]'))
ALTER TABLE [dbo].[relayActivityShare]  WITH CHECK ADD  CONSTRAINT [FK_relayActivityShare_relayActivity] FOREIGN KEY([relayActivityID])
REFERENCES [dbo].[relayActivity] ([ID])
GO

ALTER TABLE [dbo].[relayActivityShare] CHECK CONSTRAINT [FK_relayActivityShare_relayActivity]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_relayActivityShare_created]'))
ALTER TABLE [dbo].[relayActivityShare] ADD  CONSTRAINT [DF_relayActivityShare_created]  DEFAULT (getdate()) FOR [created]
GO
