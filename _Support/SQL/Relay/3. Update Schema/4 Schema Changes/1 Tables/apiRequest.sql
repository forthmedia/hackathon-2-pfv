/****** Object:  Table [dbo].[apiRequest]    Script Date: 03/22/2012 16:15:27 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[apiRequest]') AND type in (N'U'))

CREATE TABLE [dbo].[apiRequest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NULL,
	[component] [varchar](50) NOT NULL,
	[method] [varchar](10) NOT NULL,
	[arguments] [nvarchar](4000) NULL,
	[startTime] [datetime] NOT NULL,
	[endTime] [datetime] NULL,
	[statusCode] [int] NULL,
	[IP] [varchar](50) NULL,
	[data] [nvarchar](max) NULL,
	[sessionID] [varchar](50) NULL,
 CONSTRAINT [PK_apiRequest] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO