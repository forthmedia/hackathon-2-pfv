if not exists (select 1 from information_schema.tables where table_name='xFinancialFlagData')
CREATE TABLE [dbo].[xFinancialFlagData](
	[entityID] [int] NOT NULL,
	[flagID] [int] NOT NULL,
	[data] [decimal](18, 2) NOT NULL,
	[currencyISOCode] NCHAR(3) NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
	[ArchivedBy] [int] NOT NULL,
	[Archived] [datetime] NOT NULL,
 CONSTRAINT [PK_xFinancialFlagData] PRIMARY KEY CLUSTERED 
(
	[entityID] ASC,
	[flagID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
