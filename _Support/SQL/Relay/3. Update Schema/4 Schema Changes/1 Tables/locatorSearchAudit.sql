if exists (select * FROM  sysobjects where name='locatorSearchAudit' and xtype='U') 
BEGIN
DROP TABLE locatorSearchAudit
END

CREATE TABLE dbo.locatorSearchAudit
(
searchID int NOT NULL IDENTITY,
hitID int,
searchTime datetime not null,
searchCriteriaString nvarchar(250),
searchSiteName nvarchar(250),
flagSelectionList nvarchar(500),
searchRadius tinyint,
locatorCountryID int
)