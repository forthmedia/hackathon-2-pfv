IF object_id('SAMLIdentityProviderUsergroupCountryControl') is null  
BEGIN
	Create Table [dbo].[SAMLIdentityProviderUsergroupCountryControl](
		SAMLIdentityProviderId int NOT NULL FOREIGN KEY REFERENCES [dbo].[SAMLIdentityProvider] ([SAMLIdentityProviderId]) on delete cascade,
		countryID int NOT NULL FOREIGN KEY REFERENCES [dbo].[Country] ([CountryID]) on delete cascade,
		permitsControl bit not null default 0,
		setAtPersonCreation bit not null default 0
		)
END