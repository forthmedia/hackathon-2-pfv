if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'active') 
	alter table productGroup add active bit default(1)
GO

if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'productCategoryID') 
	alter table productGroup add productCategoryID int
GO

/* NJH 2013 Roadmap - added created/createdBy */
if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'created') 
	alter table productGroup add created datetime default getDate()
GO

if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'createdBy') 
	alter table productGroup add createdBy int
GO

/* NJH 2012/12/13 - make description nullable as it's going to be translated. So, field no longer required */
if exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'description') 
	alter table productGroup alter column description nvarchar(255) null
GO

/* make productGroupName an nvarchar */
alter table productGroup alter column ProductGroupName nvarchar(50) null

if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'title_defaultTranslation') 
	alter table productGroup add title_defaultTranslation nvarchar(400)
GO

if not exists (select 1 from information_schema.columns where table_name = 'productGroup' and column_name = 'description_defaultTranslation') 
	alter table productGroup add description_defaultTranslation nvarchar(400)
GO
