IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[entitySpecialisationRuleStatus]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[entitySpecialisationRuleStatus](
		[entitySpecialisationID] [int] IDENTITY(1,1) NOT NULL,
		[entityID] [int] NOT NULL,
		[entityTypeID] [int] NOT NULL,
		[numOfCertifications] [int] NOT NULL,
		[specialisationRuleID] [int] NOT NULL,
		[created] [datetime] NOT NULL DEFAULT GETDATE(),
		[createdBy] [int] NOT NULL DEFAULT (0),
		[lastUpdated] [datetime] NOT NULL DEFAULT GETDATE(),
		[lastUpdatedBy] [int] NOT NULL DEFAULT (0)
	)

	ALTER TABLE [dbo].[entitySpecialisationRuleStatus] ADD CONSTRAINT [entitySpecialisationRuleStatus_PK] PRIMARY KEY CLUSTERED (
	[entitySpecialisationID]
	)

END
GO