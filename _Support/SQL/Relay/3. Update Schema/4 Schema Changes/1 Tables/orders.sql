/* 2011/05/12 PPB LID6574 */
if not exists (select 1 from information_schema.columns where table_name = 'orders' and column_name = 'approverNotes')
BEGIN
ALTER TABLE orders Add approverNotes nvarchar(4000) NULL
END
