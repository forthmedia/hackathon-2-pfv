/* Create Schedule Table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'Schedule')
BEGIN
CREATE TABLE [dbo].[Schedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleName] [varchar](50) NULL,
	[Description] [varchar](2000) NULL,
	[ScheduleTypeID] [int] NOT NULL,
	[Tablename] [varchar](50) NULL,
	[StartDate] [datetime] NULL CONSTRAINT [DF_Table_1_Recurance]  DEFAULT (1),
	[EndDate] [datetime] NULL,
	[StartTime] [int] NULL,
	[EndTime] [int] NOT NULL,
	[DailyHours] [int] NULL,
	[DailyMon] [bit] NULL,
	[DailyTue] [bit] NULL,
	[DailyWed] [bit] NULL,
	[DailyThur] [bit] NULL,
	[DailyFri] [bit] NULL,
	[DailySat] [bit] NULL,
	[DailySun] [bit] NULL,
	[scheduleQuery] [ntext] NULL,
	[created] [datetime] NULL,
	[createdby] [int] NULL,
	[lastUpdated] [datetime] NULL,
	[lastUpdatedBy] [int] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
