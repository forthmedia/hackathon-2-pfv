if not exists (select 1 from information_schema.tables where table_name='ActivityType')
Begin
	CREATE TABLE [dbo].[activityType](
		[activityTypeID] [int] IDENTITY(1,1) NOT NULL,
		[activityType] [varchar](50) NOT NULL,
		[description] [varchar](250) NOT NULL,
		[lastRefreshed] [datetime] NOT NULL DEFAULT GetDate(),
		[entityTypeID] [int] NOT NULL,
		[lastUpdated] [datetime] NULL,
		[lastUpdatedBy] [int] NULL,
		[lastupdatedbyPerson] [int] NULL
	 CONSTRAINT [PK_activityType] PRIMARY KEY CLUSTERED 
	(
		[activityTypeID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
End

GO

IF COL_LENGTH('ActivityType','availableForScoring') IS NULL
BEGIN
	ALTER TABLE ActivityType
	ADD availableForScoring  bit NOT NULL Default 1
END

IF COL_LENGTH('ActivityType','minimumEntityTypeIDWithData') IS NULL --some activitities only have data on the org level etc, this holds the entityTypeID where data begins
BEGIN
	ALTER TABLE ActivityType
	ADD minimumEntityTypeIDWithData int NOT NULL Default 0 --default of data on all levels (0 is the person TypeID)
END


