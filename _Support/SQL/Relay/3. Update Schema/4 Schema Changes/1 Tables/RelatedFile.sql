
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'RelatedFile' AND column_name='UploadUUID') 
ALTER TABLE RelatedFile ADD UploadUUID UNIQUEIDENTIFIER NULL
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'RelatedFile' AND column_name='UploadedFileName') 
ALTER TABLE RelatedFile ADD UploadedFileName NVARCHAR(255) NULL
GO

/* make several columns not nullable */
alter table relatedFile alter column fileName nvarchar(255) not null
alter table relatedFile alter column entityId int not null
alter table relatedFile alter column fileCategoryID int not null

/* NJH 2016/03/10 JIRA PROD2016-472 connector - synching attachments*/
/* add crmId to relatedFile table */
if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='crmRelatedFileID')
alter table relatedFile add crmRelatedFileID varchar(50)
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[relatedFile]') AND name = N'crmRelatedFileID_unique')
CREATE unique INDEX crmRelatedFileID_unique ON relatedFile (crmRelatedFileID) WHERE crmRelatedFileID IS NOT NULL ;
GO

/* NJH 2016/03/22 JIRA PROD2016-472 track changes to relatedFiles - run this before adding createdBy and lastUpdatedBy so that we know that the del table exists */
exec generate_mrAudit @tablename='relatedFile'

/* add created/by fields */
if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='created')
begin
	alter table relatedFile add created datetime constraint DF_relatedFile_created default (getDate()) not null
	alter table relatedFiledel add created datetime 

	declare @sql nvarchar (max) = 
		'update relatedFile set created = uploaded
		update relatedFiledel set created = uploaded'
	exec (@sql)	

end	
GO

if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='lastUpdated')
begin
	alter table relatedFile add lastUpdated datetime constraint DF_relatedFile_lastUpdated default (getDate()) not null
	alter table relatedFiledel add lastUpdated datetime 
	declare @sql nvarchar (max) = 
		'update relatedFile set lastUpdated = uploaded
		update relatedFiledel set lastUpdated = uploaded'
	exec (@sql)	

end
GO


if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='createdBy')
begin
	alter table relatedFile add createdBy int null
	alter table relatedFileDel add createdBy int null

	declare @sql nvarchar (max) = '
			update relatedFile set createdBy=uploadedBy where createdBy is null
			update relatedFiledel set createdBy=uploadedBy where createdBy is null
			alter table relatedFile alter column createdBy int not null
		'
	exec (@sql)	



end
GO

if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='lastUpdatedBy')
begin
	alter table relatedFile add lastUpdatedBy int null
	alter table relatedFileDel add lastUpdatedBy int null
	declare @sql nvarchar (max) = '
		update relatedFile set lastUpdatedBy= uploadedBy where lastUpdatedBy is null
		update relatedFiledel set lastUpdatedBy= uploadedBy where lastUpdatedBy is null
		alter table relatedFile alter column lastUpdatedBy int not null
		'
	exec (@sql)	

	
end
GO

/* case 449530 ESZ Unable to attach file to MDF request*/
if not exists(select 1 from information_schema.columns where table_name='relatedFile' and column_name='lastUpdatedByPerson')
begin
    alter table relatedFile add lastUpdatedByPerson int null
    alter table relatedFileDel add lastUpdatedByPerson int null
    
end
