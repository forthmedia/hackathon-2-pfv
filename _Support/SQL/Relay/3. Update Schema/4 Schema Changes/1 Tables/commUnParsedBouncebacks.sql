
/*  
  	WAB 2011/02
	Create CommUnParsedBouncebacks table
	Currently no interface written to deal with these!
	But hopefully will happen in future
	At the moment allows nulls in all fields, could  conceivably be used to store bouncebacks where we haven't worked out the commid/personid yet
*/
if not exists (select 1 from sysObjects where name = 'CommUnParsedBouncebacks') 
BEGIN

CREATE TABLE [dbo].[CommUnParsedBouncebacks](
	[commid] [int] NULL,
	[personid] [int] NULL,
	[subject] [nvarchar](500) NULL,
	[body] [ntext] NULL,
	[fromAddress] [nvarchar](100) NULL,
	[toAddress] [nvarchar](100) NULL,
	[received] [datetime] NULL
) 

CREATE NONCLUSTERED INDEX [IX_CommUnParsedBouncebacks] ON [dbo].[CommUnParsedBouncebacks] 
(
	[commid] ASC,
	[personid] ASC
) 


END


alter table dbo.commUnParsedBouncebacks
alter column fromAddress nvarchar(255)

alter table dbo.commUnParsedBouncebacks
alter column toAddress nvarchar(255)