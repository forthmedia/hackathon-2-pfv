
IF object_id('OAuth2ScopePermission') is null  
BEGIN
	CREATE TABLE dbo.OAuth2ScopePermission
	(
		clientIdentifier nVarChar(256) NOT NULL FOREIGN KEY REFERENCES OAuth2Client(clientIdentifier) ON DELETE CASCADE,
		scope nVarChar(256) NOT NULL --note can include built it scopes
	);
End

