/* 
	WAB 2013-12-03 2013RoadMap2 Item 25 Contact History
	RelatedEntities Table
	Can store a relationship between any entities (but not much detail about that relationship)
	First created to store information about which entityID a system email might have been sent about
*/
if not exists (select 1 from information_schema.tables where table_name='relatedEntity')
begin
CREATE TABLE [dbo].[relatedEntity](
	[entityTypeID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[relatedEntityTypeID] [int] NOT NULL,
	[relatedEntityID] [int] NOT NULL
) ON [PRIMARY]


CREATE UNIQUE NONCLUSTERED INDEX [idx_relatedEntity] ON [dbo].[relatedEntity] 
(
	[entityTypeID] ASC,
	[entityID] ASC,
	[relatedEntityTypeID] ASC,
	[relatedEntityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_relatedEntity_relatedEntityID] ON [dbo].[relatedEntity] 
(
	[relatedEntityTypeID] ASC,
	[relatedEntityID] ASC
)
INCLUDE ( [entityTypeID],
[entityID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


end
GO