/*  WAB 2013-12-09 added includes to this index so that LocationDel can be searched quickly in vEntityName view */
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganisationDel]') AND name = N'OrganisationID_Idx')
DROP INDEX [dbo].[OrganisationDel].[OrganisationID_Idx]
GO

CREATE NONCLUSTERED INDEX [OrganisationID_Idx] ON [dbo].[OrganisationDel] 
(
	[OrganisationID] ASC
) 
INCLUDE 
(OrganisationName) 
ON [PRIMARY]
GO

if not exists (select * from information_schema.columns where table_name='organisationDel' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].organisationDel add [lastUpdatedByPerson] int NULL

GO

if not exists (select * from information_schema.columns where table_name='organisationDel' and column_name='ParentOrgID')  
      alter TABLE [dbo].organisationDel add [ParentOrgID] int NULL

GO

if not exists(select 1 from information_schema.columns where table_name='organisationDel' and column_name='pictureURL')
	alter table organisationDel add pictureURL varchar(250)
GO

/* NJH 2015/06/17 created index to speed up vEntityName - recommended by query analyzer */
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrganisationDel]') AND name = N'Idx_organisationDel_crmOrgId_inc_organisationID_organisationName')
DROP INDEX [dbo].[OrganisationDel].[Idx_organisationDel_crmOrgId_inc_organisationID_organisationName]
GO

CREATE NONCLUSTERED INDEX [Idx_organisationDel_crmOrgId_inc_organisationID_organisationName]
ON [dbo].[OrganisationDel] ([crmOrgID])
INCLUDE ([OrganisationID],[OrganisationName])