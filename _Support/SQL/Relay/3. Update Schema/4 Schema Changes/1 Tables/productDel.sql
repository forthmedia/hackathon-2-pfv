/*  WAB 2013-12-09 added index so that productDel can be searched quickly in vEntityName view (actually does not seem to have any records in it anyway but added for consistency */
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[productDel]') AND name = N'productID_Idx')
	
	CREATE NONCLUSTERED INDEX [productID_Idx] ON [dbo].[productDel]
	(
		[productID] ASC
	) 
	INCLUDE (title_defaultTranslation)
	ON [PRIMARY]
	
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[productDel]') AND name = N'crmProductID_idx')
BEGIN
	CREATE INDEX crmProductID_idx ON ProductDel (crmProductID);
END	
GO
