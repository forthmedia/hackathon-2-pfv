IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundRequestActivity' AND column_name='proposedPartnerFunding') 
alter table fundRequestActivity add proposedPartnerFunding decimal(18,2) 
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundRequestActivity' AND column_name='requestedCoFunding') 
alter table fundRequestActivity add requestedCoFunding decimal(18,2) 
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'fundRequestActivity' AND column_name='CoFunder') 
alter table fundRequestActivity add coFunder int 
GO

alter table fundRequestActivity alter column roi nvarchar(1000) null

alter table fundRequestActivity alter column typeDescription nvarchar(1000) null


--RJT As the totalCost is the requestedAmount plus the Proposed Partner Funding it must fit the sum of the maximum of both
alter table fundRequestActivity alter column totalCost decimal(19,2) not null
