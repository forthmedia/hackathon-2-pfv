
if not exists(select 1 from information_schema.columns where table_name='person' and column_name='pictureURL')
alter table person add pictureURL varchar(250)
GO

/* extend r2PerId column */
alter table person alter column r2PerID varchar(20)


/****** Object:  Index [crmPerID]    Script Date: 03/02/2012 11:28:13 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND name = N'crmPerID')
DROP INDEX [crmPerID] ON [dbo].[Person] 
GO
/****** Object:  Index [crmPerID]    Script Date: 03/02/2012 11:28:14 ******/
/*
CREATE NONCLUSTERED INDEX [crmPerID] ON [dbo].[Person] 
(
	[crmPerID] ASC
) ON [PRIMARY]
GO*/

if not exists (select * from information_schema.columns where table_name='person' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].person add [lastUpdatedByPerson] int NULL

GO


/* YMA 2013/01/24 Sprint 2 Item 118 - Person Public Profiles */
if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'publicProfileName')
BEGIN
ALTER TABLE dbo.Person ADD
	publicProfileName nvarchar(400) NULL
End
GO
if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'publicProfileAbout')
BEGIN
ALTER TABLE dbo.Person ADD
	publicProfileAbout nvarchar(1000) NULL
End
GO
if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'publicProfileJobDesc')
BEGIN
ALTER TABLE dbo.Person ADD
	publicProfileJobDesc nvarchar(1000) NULL
End
GO
if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'publicProfileLocation')
BEGIN
ALTER TABLE dbo.Person ADD
	publicProfileLocation nvarchar(400) NULL
End
GO


/* 	Make the organisationID column null and then add a trigger to update it on a insert/update
 	WAB 2015-01-21 Add an IF clause so that does not run unless needed and add dropAndRecreateDependentObjects so that will work if run 
*/
if exists (select 1 from INFORMATION_SCHEMA.COLUMNS where table_name = 'person' and column_name = 'organisationid' and is_nullable = 'no')
BEGIN
		
		exec dropAndRecreateDependentObjects 'person.organisationid', 'dropfulltextindexes'
	begin tran
		exec dropAndRecreateDependentObjects 'person.organisationid', 'drop'
		alter table person alter column organisationID int null
	
		exec dropAndRecreateDependentObjects 'person.organisationid', 'recreate'
	commit
		exec dropAndRecreateDependentObjects 'person.organisationid', 'recreatefulltextindexes'

END	

-- default for active
IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Person_Active]') AND type = 'D')
ALTER TABLE [dbo].[Person] ADD  CONSTRAINT [DF_Person_Active]  DEFAULT ((1)) FOR [Active]
GO

-- default for firstTimeUser
IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Person_FirstTimeUser]') AND type = 'D')
ALTER TABLE [dbo].[Person] ADD  CONSTRAINT [DF_Person_FirstTimeUser]  DEFAULT ((1)) FOR [FirstTimeUser]
GO

-- default for passwordDate
IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Person_PasswordDate]') AND type = 'D')
ALTER TABLE [dbo].[Person] ADD  CONSTRAINT [DF_Person_PasswordDate]  DEFAULT (getdate()) FOR [PasswordDate]
GO

if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'convertedFromLeadID')
	ALTER TABLE dbo.Person ADD convertedFromLeadID int NULL
GO

if not exists (select 1 from information_schema.columns where table_name = 'person' and column_name = 'fullname')
	alter table person add fullname as (firstname+' '+lastname)

alter table person alter column homePhone nvarchar(50)
alter table person alter column homeFax nvarchar(50)
alter table person alter column officePhone nvarchar(50)
alter table person alter column mobilePhone nvarchar(50)
alter table person alter column faxPhone nvarchar(50)
alter table person alter column jobDesc nvarchar(200)

exec generate_mrAudit @tablename='person'


IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND name = N'crmPerID_unique')
BEGIN
	/* WAB 2015-02-12 null out any blank crmLocIDs so that unique index can be created */
	update person set crmPerID = null where crmPerID = ''
	CREATE unique INDEX crmPerID_unique ON person (crmPerID) WHERE crmPerID IS NOT NULL ;
END	
GO


/*
WAB 2016-02-08
BF-409
Some columns on the person table have defaults of ' '
This is a bit odd and was causing pages to think that data had changed when it hadn't (since trailing spaces tend to get trimmed)
Change default to '' and update old data
Cannot update email field here because of a trigger which considers blank email addresses as duplicates of each other.  Update those after trigger is fixed

*/

declare 
	@sql  nvarchar(max)
	, @table_name sysname
	, @column_name sysname
	, @constraint_name sysname

declare myCursor cursor for
select  OBJECT_NAME(sc.id), sc.name , so.name from 
				syscolumns  sc 
				   INNER JOIN
			   sysobjects as so on sc.cdefault = so.id
				   INNER JOIN
			   syscomments as sm on sc.cdefault = sm.id
			where 
				 OBJECTPROPERTY(so.id, N'IsDefaultCnst') = 1
				 and sm.text = '('' '')'
				 and OBJECT_NAME(sc.id) = 'person'


open myCursor
fetch next from myCursor into @table_name , @column_name, @constraint_name
	while @@FETCH_STATUS = 0
	begin
		IF @column_name <> 'email'
		BEGIN
			set @sql = '  update ' + @table_name + ' set ' + @column_name + ' = '''' where ' + @column_name + ' = '' ''  '
			exec (@sql)

		END		
		
		set @sql = '
			ALTER TABLE ' + @table_name + '  DROP CONSTRAINT '+ @constraint_name + '
			ALTER TABLE ' + @table_name + ' ADD CONSTRAINT ' + @constraint_name+ ' DEFAULT ('''') FOR [' + @column_name + ']
		'
		exec (@sql)
		
		fetch next from myCursor into @table_name , @column_name, @constraint_name
	end

close myCursor
deallocate myCursor


/* NJH 2016/08/25 JIRA PROD2016-125 if personId is not an identity column */
declare @perIDisIdentity int = 0
select  @perIDisIdentity = columnproperty(object_id('person'),'personID','IsIdentity')
if (@perIDisIdentity = 0)
begin
declare @userGroupsToDelete table (usergroupid int)

	/* Procedure fails because there are invalid personids in the usergroup table, and this gets picked up when we recreate the foreign key constraint, so delete the invalid usergroups
		Also need to delete from usergroupcountry because there is no cascading delete 
	 */
	insert into @userGroupsToDelete
		(usergroupid)
	select 
		usergroupid	
	from 
		usergroup ug 
			left join 
		person p on ug.personid = p.personid 
	where 
		ug.personid is not null and p.personid is null

	delete from usergroupcountry where usergroupid in (select usergroupid from @userGroupsToDelete)

	delete from usergroup where usergroupid in (select usergroupid from @userGroupsToDelete)

	/* now do the conversion */
	exec convertBoundDefaultsToConstraints 'person'
	exec convertColumnToIdentity 'person', 'personid'
end


