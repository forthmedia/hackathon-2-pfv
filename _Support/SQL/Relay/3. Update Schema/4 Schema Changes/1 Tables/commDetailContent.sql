/* 
	WAB 2013-12-03 2013RoadMap2 Item 25 Contact History

	Decided to start storing the content of system emails
	Didn't want to use the commdetail.feedback column (rather, I plan to move a lot of guff out of that field into this new table)
	I didn't want to add large columns to the commdetail table (imagined performance problems), so I have created a table which sits alongside commdetail 	
	Haven't put crud columns on it because record is created alongside a commdetail records.  Perhaps would be needed if we started to do updates
*/


IF NOT EXISTS (SELECT 1 from sysobjects where name = 'commDetailContent')
BEGIN
		CREATE TABLE [dbo].[commDetailContent](
		[commDetailID] [int] NOT NULL,
		[Subject] [nvarchar](max) NULL,
		[Body] [nvarchar](max) NULL,
		[MetaData] [nvarchar](max) NULL,
	 CONSTRAINT [PK_commDetailContent] PRIMARY KEY CLUSTERED 
	(
		[commDetailID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	
	
	ALTER TABLE [dbo].[commDetailContent]  WITH CHECK ADD  CONSTRAINT [FK_commDetailContent_commdetailID] FOREIGN KEY([commDetailID])
	REFERENCES [dbo].[commdetail] ([CommDetailID])
	
	
	ALTER TABLE [dbo].[commDetailContent] CHECK CONSTRAINT [FK_commDetailContent_commdetailID]
	
END





