
/* extend status column */
alter table oppStage alter column status varchar(40)

if not exists (select * from information_schema.columns where table_name='oppStage' and column_name='isWon')  
      alter TABLE [dbo].oppStage add [isWon] bit NULL default 0
GO
      
update oppStage set isWon=0 where isWon is null
GO

alter TABLE [dbo].oppStage alter column [isWon] bit NOT NULL
GO

IF NOT EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	exec generate_MRAudit @TableName = 'oppStage'
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='status' where TableName ='oppStage' and FieldName = 'status' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='opportunityStageID' where TableName ='oppStage' and FieldName = 'opportunityStageID' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='opportunityStage' where TableName ='oppStage' and FieldName = 'opportunityStage' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='defaultValue' where TableName ='oppStage' and FieldName = 'defaultValue' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='sortOrder' where TableName ='oppStage' and FieldName = 'sortOrder' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='probability' where TableName ='oppStage' and FieldName = 'probability' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='statusSortOrder' where TableName ='oppStage' and FieldName = 'statusSortOrder' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='liveStatus' where TableName ='oppStage' and FieldName = 'liveStatus' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='onlyShowInQuarter' where TableName ='oppStage' and FieldName = 'onlyShowInQuarter' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='oppTypeID' where TableName ='oppStage' and FieldName = 'oppTypeID' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='isSystem' where TableName ='oppStage' and FieldName = 'isSystem' and ApiActive=0
END

IF EXISTS (select * from ModEntityDef where TableName='oppStage')
BEGIN
 	update ModEntityDef set ApiActive=1, ApiName='isWon' where TableName ='oppStage' and FieldName = 'isWon' and ApiActive=0
END
GO

/* 
PROD2016-3487  WAB/AHL Cannot delete anything from this table because auditing has been added without system columns being added
Add system columns
*/
exec addSystemColumns @tablename = 'oppstage'

