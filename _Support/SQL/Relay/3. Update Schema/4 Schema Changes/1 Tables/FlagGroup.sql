/* 
LID 5369 Make Flag and Flag Group Names/Desriptions nVarchar
*/
alter table flaggroup alter column name nvarchar (500)
alter table flaggroup alter column description nvarchar (2000)
alter table flaggroup alter column notes nvarchar (200) -- was 50 but seemed a bit useless
/* RPW added currency types to flag group data */
if not exists (select 1 from information_schema.columns where table_name='FlagGroup' and column_name='FlagTypeCurrency')
ALTER TABLE FlagGroup ADD FlagTypeCurrency NVARCHAR(3) NULL

if not exists (select 1 from information_schema.columns where table_name='FlagGroup' and column_name='FlagTypeDecimalPlaces')
ALTER TABLE FlagGroup ADD FlagTypeDecimalPlaces TINYINT NULL

IF NOT EXISTS (select * from sysindexes
where id=object_id('FlagGroup') and name='idx_ParentFlagGroupID_EntityTypeID_Active_Expiry')
CREATE NONCLUSTERED INDEX [idx_ParentFlagGroupID_EntityTypeID_Active_Expiry]
ON [dbo].[FlagGroup] ([ParentFlagGroupID],[EntityTypeID],[Active],[Expiry])
GO

--alter table flaggroup alter column flaggroupTextID varchar(50)

/*
WAB/GCC 2013-03-18.  CASE 431321 
As recommended by Query Analyser
 */

IF NOT EXISTS (select * from sysindexes where id=object_id('FlagGroup') and name='IX_flagGroup_flagGroupTextID')
BEGIN
	CREATE NONCLUSTERED INDEX IX_flagGroup_flagGroupTextID
	ON [dbo].[FlagGroup] ([FlagGroupTextID])
END

if not exists(select 1 from information_schema.columns where table_name='flagGroup' and column_name='helpText')
BEGIN
	alter table flagGroup add helpText nvarchar(160) null
	alter table xFlagGroup add helpText nvarchar(160) null
END
GO

if not exists (select 1 from flagGroup where helpText is not null)
begin
	update flagGroup set helpText = description where len(isNull(description,'')) > 0
end

alter table flagGroup alter column namePhrasetextID varchar(100) null -- make to be the same as textID in phraseList. Connector generating longer phraseTextID

alter table flagGroup alter column flagGroupTextID varchar(250) null -- make to be the same as flagTextID

/* WAB 2015-10-20 
allow for infinitely long formatting parameters (for PYW Tata)
*/
IF (select character_Maximum_Length from information_schema.columns where table_name = 'flagGroup' and column_name = 'formattingParameters') <> - 1
BEGIN
	ALTER TABLE [dbo].[flagGroup]
	ALTER COLUMN [formattingParameters] nvarchar(max)
END



if not exists(select 1 from information_schema.columns where table_name='flagGroup' and column_name='createdByPerson')
	alter table flagGroup add createdByPerson int null
GO

update flagGroup 
	set createdByPerson=u.personID
from flagGroup fg
	inner join userGroup u on fg.createdBy = u.userGroupID
where fg.createdByPerson is null

update flagGroup set createdByPerson=0 where createdByPerson is null

if not exists(select 1 from information_schema.columns where table_name='flagGroup' and column_name='createdByPerson' and is_nullable='No')
alter table flagGroup alter column createdByPerson int not null

IF OBJECT_ID('DF_flagGroup_createdByPerson', 'D') IS NULL
alter table flagGroup add constraint DF_flagGroup_createdByPerson default 0 for createdByPerson


if not exists(select 1 from information_schema.columns where table_name='flagGroup' and column_name='lastUpdatedByPerson')
	alter table flagGroup add lastUpdatedByPerson int null
GO

update flagGroup 
	set lastUpdatedByPerson=u.personID
from flagGroup fg
	inner join userGroup u on fg.lastUpdatedBy = u.userGroupID
where fg.lastUpdatedByPerson is null

update flagGroup set lastUpdatedByPerson=0 where lastUpdatedByPerson is null

if not exists(select 1 from information_schema.columns where table_name='flagGroup' and column_name='lastUpdatedByPerson' and is_nullable='No')
alter table flagGroup alter column lastUpdatedByPerson int not null

IF OBJECT_ID('DF_flagGroup_lastUpdatedByPerson', 'D') IS NULL
alter table flagGroup add constraint DF_flagGroup_lastUpdatedByPerson default 0 for lastUpdatedByPerson