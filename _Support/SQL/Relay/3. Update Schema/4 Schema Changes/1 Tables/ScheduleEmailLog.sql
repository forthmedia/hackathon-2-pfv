/* create ScheduleEmailLog table */

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleEmailLog')
BEGIN
CREATE TABLE [dbo].[ScheduleEmailLog](
	[scheduleEmailLogID] [int] IDENTITY(1,1) NOT NULL,
	[scheduleID] [int] NULL,
	[entityID] [int] NULL,
	[personID] [int] NULL,
	[recipientValue] [nvarchar](50) NULL,
	[created] [datetime] NULL CONSTRAINT [DF_ScheduleEmailLog_created]  DEFAULT (getdate()),
	[emailDefID] [int] NULL,
	[scheduleEmailLogStatusID] [int] NULL,
	[actionDate] [datetime] NULL,
 CONSTRAINT [PK_ScheduleEmailLog] PRIMARY KEY CLUSTERED 
(
	[scheduleEmailLogID] ASC
)) ON [PRIMARY]
END
GO
