
if not exists (select 1 from sysobjects where name = 'ColdFusionInstance_Host_AutoPopulated') 
BEGIN
CREATE TABLE [dbo].[ColdFusionInstance_Host_AutoPopulated](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[coldFusionInstance_AutoPopulatedID] [int] NULL,
	[hostname] [varchar](50) NULL,
	[lastupdated] [datetime] NULL
) ON [PRIMARY]
END


/* increase length of hostname field to 100 equal to sitedefdomain.domain */
alter table ColdFusionInstance_Host_AutoPopulated alter column hostname varchar(100)