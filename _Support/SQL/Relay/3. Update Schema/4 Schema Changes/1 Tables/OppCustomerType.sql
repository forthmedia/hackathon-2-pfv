/*FILE LHID8224 should be run after this*/
if not exists (select * from information_schema.columns where table_name='OppCustomerType' and column_name='CustomerTypeTextID')
	ALTER TABLE OppCustomerType ADD CustomerTypeTextID [varchar](50) NULL 
GO
if exists (select * from information_schema.columns where table_name='OppCustomerType' and column_name='CustomerTypeTextID')
	begin	
		if exists (select * from OppCustomerType where CustomerTypeTextID is null)
			update OppCustomerType set CustomerTypeTextID=replace(replace(replace(CustomerType,'-',''),'&',''),' ','') 
	end

