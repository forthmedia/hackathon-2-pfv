if object_id('tempdb..#CommDetailStatus') is not null 
drop table #CommDetailStatus

CREATE TABLE [#CommDetailStatus](
	[CommStatusID] [int]  NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[StatsReport] [varchar](50) NULL,
	[StatsReportOrder] [int] NULL,
	[statusGroup] [varchar](20) NULL,
	[significance] [int] NULL
)

/* sp_generate_Inserts 
	@table_name ='commdetailstatus' ,
	@target_table ='#commdetailstatus',
	@cols_to_exclude = '''created'',''createdby'',''lastupdatedby'',''lastupdated'''
*/

INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-31,'Error during faxing','Error during faxing',NULL,0,'NotReceived',NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-30,'Bad Fax Number','Bad Fax Number','Faxes failed during sending',31,'NotReceived',31)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-21,'SPAM','SPAM','Emails rejected as SPAM',39,'NotReceived',39)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-20,'Email Failed','Email Failed','Emails returned, undelivered',22,'NotReceived',22)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-16,'Not Opted In','Not Opted In','people who have not Opted In',43,'NotSent',43)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-15,'Duplicate Email Address','Duplicate Email Address','Duplicate Email Address',25,'NotSent',25)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-14,'Too Many Bouncebacks','Too Many Bouncebacks','Email addresses which have failed too many times',24,'NotSent',24)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-13,'No Faxes','No Faxes','partner who do not / prefer not to receive faxes',42,'NotSent',42)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-12,'No Fmail','No Fmail','partner who do not / prefer not to receive emails',41,'NotSent',41)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-11,'Unsubscribed','Unsubscribed','people who have previously unsubscribed',40,'NotSent',40)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-10,'Excluded','Excluded','Excluded',38,'NotSent',38)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-3,'Invalid Fax Number','Invalid Fax Number','Locations with invalid/blank fax numbers',32,'NotSent',32)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(-2,'Invalid Email Address','Invalid Email Address','People with invalid/blank email addresses',23,'NotSent',23)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(0,'Items being Processed','Items being Processed','Items being Processed',19,'Not Sent',0)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(2,'Email Sent (no confirmation)','Email Sent (no confirmation)','Emails sent (receipt unconfirmed)',21,'Sent',2)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3,'Fax Queued','Fax Queued','Faxes still in queue',33,'Sent',3)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(4,'Downloaded','Downloaded','Downloaded',NULL,'Sent',NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(20,'Email Receipt Received','Email Receipt Received','Emails sent (receipt confirmed)',20,'Received',8)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(21,'Email Receipt Received (Not Read)','Email Receipt Received (Not Read)','Emails received but deleted',6,'Received',6)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(25,'Email read','Recipient read the email','Emails Read',0,'Received',10)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(27,'Click thru','Recipient clicked through to read some content','Click Through Recorded to Relay Site',0,'Received',NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(28,'Clicked thru to Remote URL','Clicked thru to Remote URL','Clicked thru to Remote URL',14,'Received',14)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(30,'Fax Received OK','Fax Received OK','Faxes confirmed sent',30,'Received',30)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(40,'Email Marketing Response','This person responded to an email campiagn','Response',NULL,'Received',NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(41,'Unsubscribe Response','This person requested to unsubscribe','Unsubscribed',NULL,'Received',20)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(50,'Email Read Receipt Received','Email has been read','Email read',NULL,'Received',11)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(60,'Downloaded file','Downloaded file','Downloaded file',18,'Received',18)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(65,'Clicked thru to ElementID','Clicked thru to ElementID','Clicked thru to ElementID',16,'Received',16)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(66,'Sent to a friend','Sent to a friend','Sent to a friend',17,'Received',17)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(501,'Closed','','Closed Call',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(502,'Open','','Open Call',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(503,'Call Back','','Call Back',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(504,'Customer Info','','Customer Info',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(506,'Success','','Success',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(507,'No Answer','','No Answer',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(508,'Other','','Other',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(544,'Untried','This record has not been phoned','Untried',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(545,'Engaged','','Engaged',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(546,'Number unobtainable','','Number unobtainable',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(1042,'Success','','Success',NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3000,'Web Visit Unrelated','A web visit not related to any communications',NULL,NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3001,'Web Visit with partner logon','Web Visit with partner logon',NULL,NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3002,'Web Visit Feedback','A web visit with feedback',NULL,NULL,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3003,'Fax (manual)','Fax','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3004,'E-Mail (manual)','E-Mail','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3005,'Make Telephone Call','Make Telephone Call','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3006,'Event','Event','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3007,'Meeting','Meeting','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3008,'Project','Project','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3009,'Info','Info','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3010,'Training','Training','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3011,'Letter','Letter','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3012,'Warranty','Warranty','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3013,'Offer','Offer','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3014,'Receive Telephone Call','Receive Telephone Call','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3015,'Test','Test','',0,NULL,NULL)
INSERT INTO [#commdetailstatus] ([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance])VALUES(3016,'Visit','Visit','',0,NULL,NULL)


/* Insert any new statuses */
set identity_insert commdetailstatus on 
insert into commdetailstatus
([CommStatusID],[Name],[Description],[StatsReport],[StatsReportOrder],[statusGroup],[significance],created,lastupdated)
select t.CommStatusID,t.Name,t.Description,t.StatsReport,t.StatsReportOrder,t.statusGroup,t.significance,getdate(),getdate()
from 
#commdetailstatus t
left join 
commdetailstatus cds on t.commstatusid = cds.commstatusid
where cds.commstatusid is null
set identity_insert commdetailstatus off

/* Update the significance field if it is null */
--select t.commstatusid, t.significance,cds.significance 
update commdetailstatus set significance = t.significance
from
#commdetailstatus t
inner join 
commdetailstatus cds on t.commstatusid = cds.commstatusid
where cds.significance is null -- or t.significance <> cds.significance

/* Update the statusGroup field if it is null */
update commdetailstatus set statusGroup = t.statusGroup
from
#commdetailstatus t
inner join 
commdetailstatus cds on t.commstatusid = cds.commstatusid
where cds.statusGroup is null -- or t.significance <> cds.significance

/* Update the statsReportOrder field if it is null */
-- select t.commstatusid, cds.statsReportOrder,t.statsReportOrder 
update commdetailstatus set statsReportOrder = t.statsReportOrder 
from
#commdetailstatus t
inner join 
commdetailstatus cds on t.commstatusid = cds.commstatusid
where cds.statsReportOrder  is null  -- or cds.statsReportOrder <> t.statsReportOrder 


drop table #commdetailstatus
