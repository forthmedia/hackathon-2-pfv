if not exists (select 1 from information_schema.columns where table_name = 'modregister' and column_name = 'entityTypeID')
BEGIN
ALTER TABLE ModRegister Add EntityTypeID int
END


GO

/* WAB 2011/05/04 LID 6469 discovered that modregister did not support nvarchar!
Can take a bit long to do this change, so do a test first
 */
if not exists (select 1 from information_schema.columns where table_name = 'modregister' and column_name = 'newVal' and data_Type = 'nvarchar')
BEGIN
ALTER TABLE ModRegister alter column newVal nvarchar(4000)
ALTER TABLE ModRegister alter column oldVal nvarchar(4000)
END

GO

/*
WAB 2013-04-17  Commented out all this indexing and replaced with code below which completely re-builds modregister with new indexing
		/* start 2012/07/03 IH Case 429045 */
		IF NOT EXISTS (select * from sysindexes where id=object_id('ModRegister') and name='idx_id_action_flagid')
		CREATE NONCLUSTERED INDEX [idx_id_action_flagid]
		ON [dbo].[ModRegister] ([ID],[Action],[FlagID])
		INCLUDE ([ModEntityID],[RecordID])
		
		IF NOT EXISTS (select * from sysindexes where id=object_id('ModRegister') and name='idx_id')
		CREATE NONCLUSTERED INDEX [idx_id]
		ON [dbo].[ModRegister] ([ID])
		INCLUDE ([ModEntityID],[Action],[RecordID],[FlagID])
		/* end 2012/07/03 IH Case 429045 */
		
		
		CREATE NONCLUSTERED INDEX [modentity_idx] ON [dbo].[ModRegister] 
		(
			[ModEntityID] ASC
		)
		
		
		
		CREATE NONCLUSTERED INDEX [recordid_idx] ON [dbo].[ModRegister] 
		(
			[RecordID] ASC
		)
		
		
		ALTER TABLE [dbo].[ModRegister] ADD  CONSTRAINT [UQ_ModRegister_2__15] UNIQUE NONCLUSTERED 
		(
			[ID] ASC
		)

*/


/* WAB 2013-03-21  CASE 434808 
	Complete Rebuild of ModRegister with new indexing 
	Takes a while (maybe 20 mins on a Lenovo db), but can be run on a live site 
*/

IF not EXISTS (select 1 from SYSindexes where name = 'IX_ModRegister_Moddate_Clustered')
BEGIN

	-- clear up from previous failed attempts
	if exists (select 1 from sysobjects where name = 'modRegister_Temp')
		BEGIN
			drop table modRegister_Temp
		END	

	/*	copy existing modregister into a temporary table
		have tested and does not cause locks on modRegister, so can be used on a live system
	*/	
	select 
		id,moddate,entityTypeID,recordid,action,modentityid,flagid,convert(nvarchar(max),oldval) as oldVal,convert(nvarchar(max),newval) as newVal,actionbycf,actionbyuser,actionbyperson
	into 
		modRegister_Temp
	from 
		modRegister with (nolock)
	/* 	13 mins - Lenovo 
		 for comparison creating the clustered index on current table takes 34 mins and locks the table 
	*/


	/* Create an index clustered on ModDate */
	CREATE CLUSTERED INDEX [IX_ModRegister_Moddate_Clustered] ON [dbo].[ModRegister_Temp] 
	(
		[ModDate] ASC
	)
	/* 4 mins */


	/* Make ID a unique column */
	ALTER TABLE [dbo].[ModRegister_Temp] ADD  CONSTRAINT [PK_ModRegister_ID] PRIMARY KEY NONCLUSTERED 
	(
		[ID] ASC
	)
	/* 1 min  */


	/* This is an index which was created for Case 429045
	There was another (similar) one, but I can't see the sense in it
	*/
	CREATE NONCLUSTERED INDEX [ix_ModRegister_ID]
	ON [dbo].[ModRegister_Temp] ([ID])
	INCLUDE ([ModEntityID],[Action],[RecordID],[FlagID])


	/* There was an existing index just on RecordID
	Seems sensible to add in entityTypeID 
	NB MIGHT BE WORTH INCLUDING SOME EXTRA COLUMNS
	*/
	CREATE NONCLUSTERED INDEX [idx_modregister_Recordid_EntityTypeID] ON [dbo].[ModRegister_Temp] 
	(
		[RecordID] ASC, entityTypeID asc
	)


	begin tran
		/* Now check for any items recently added to modRegister - may take a couple of minutes*/
		declare @maxid int
		select @maxid = (select Max(id) from modRegister_Temp)
	
				set identity_insert modRegister_temp ON
	
				insert into modRegister_Temp
				(moddate,id,action,modentityid,entityTypeID,recordid,flagid,oldval,newval,actionbycf,actionbyuser,actionbyperson)
				select mr.moddate,mr.id,mr.action,mr.modentityid,mr.entityTypeID,mr.recordid,mr.flagid,convert(varchar(max),mr.oldval) ,convert(varchar(max),mr.newval) ,mr.actionbycf,mr.actionbyuser,mr.actionbyperson
				from modRegister mr  with (nolock)
				left join 
				modRegister_Temp mrt with (nolock) on mr.id = mrt.id
				where 
					1=1
					and mr.id > @maxid - 100
				and	mrt.id is null
			
				set identity_insert modRegister_Temp OFF
	
		-- rename object and trigger
		exec sp_rename 'modRegister', 'modRegister_Old'
		exec sp_rename 'modRegisterFieldTrigger', 'modRegister_OldFieldTrigger'
		exec sp_rename 'modregister_temp','modregister'
	
	commit

	Print 'You now need to reapply the trigger [ModRegisterFieldTrigger]'
	Print 'Note that stored procedure [modRegisterTriggerAction] needs updating at same time'

END

	/* to revert 
	exec sp_rename 'modregister','modregister_temp'
	drop trigger modRegisterFieldTrigger
	exec sp_rename 'modRegister_old', 'modRegister'

	exec sp_rename 'modRegister_oldFieldTrigger', 'modRegisterFieldTrigger'

	*/
	
/* End of Reindexing Code */

/* NJH 2016/05/10 - index on entityTypeID and recordId, including modDate and field... used initially to speed up some connector queries... This was done for kaspersky, where the query to reset the synch attempt for changed records was taking a long time
	dropping the original index which was similiar, but not quite had all what we needed */
if not exists(select 1 from SYSindexes where name = 'idx_modregister_entityTypeId_recordId_inc_modDate_modEntityID_flagID')
begin
	CREATE NONCLUSTERED INDEX [idx_modregister_entityTypeId_recordId_inc_modDate_modEntityID_flagID]
	ON [dbo].[modregister] (entityTypeId,recordId)
	INCLUDE (moddate,modEntityID,flagID)
end

if exists(select 1 from SYSindexes where name='idx_modregister_Recordid_EntityTypeID')
drop index modregister.idx_modregister_Recordid_EntityTypeID