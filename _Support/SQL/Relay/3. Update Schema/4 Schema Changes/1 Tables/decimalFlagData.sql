
if not exists (select 1 from sysobjects where name='decimalFlagData' and type='u')
CREATE TABLE [dbo].[decimalFlagData](
	[entityID] [int] NOT NULL,
	[flagID] [int] NOT NULL,
	[data] [decimal](18, 2) NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_decimalFlagData] PRIMARY KEY CLUSTERED 
(
	[entityID] ASC,
	[flagID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_decimalFlagData_created]') AND type = 'D')
ALTER TABLE [dbo].[decimalFlagData] ADD  CONSTRAINT [DF_decimalFlagData_created]  DEFAULT (getdate()) FOR [created]
GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_decimalFlagData_lastUpdated]') AND type = 'D')
ALTER TABLE [dbo].[decimalFlagData] ADD  CONSTRAINT [DF_decimalFlagData_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

if not exists (select 1 from information_schema.columns where table_name='decimalFlagData' and column_name='rowID')
alter table decimalFlagData add rowID int identity not null
GO