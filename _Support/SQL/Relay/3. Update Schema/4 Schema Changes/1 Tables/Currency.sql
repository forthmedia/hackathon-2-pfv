if not exists (select * from information_schema.columns where table_name='currency' and column_name='currencyid')
	alter table currency add currencyid INT IDENTITY

GO

if not exists(select * from information_schema.columns where table_name like 'Currency' and column_name='formatCountryID')
	ALTER TABLE Currency ADD formatCountryID int NULL	

GO

declare @ctryID int
if exists(select * from Currency where currencyISOCode='AUD' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='AU'
		update Currency set formatCountryID=@ctryID where currencyISOCode='AUD'	
	end

if exists(select * from Currency where currencyISOCode='BRL' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='BR'
		update Currency set formatCountryID=@ctryID where currencyISOCode='BRL'	
	end

if exists(select * from Currency where currencyISOCode='CHF' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='LI'
		update Currency set formatCountryID=@ctryID where currencyISOCode='CHF'	
	end

if exists(select * from Currency where currencyISOCode='DKK' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='DK'
		update Currency set formatCountryID=@ctryID where currencyISOCode='DKK'	
	end

if exists(select * from Currency where currencyISOCode='EUR' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='FR'
		update Currency set formatCountryID=@ctryID where currencyISOCode='EUR'	
	end

if exists(select * from Currency where currencyISOCode='GBP' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='GB'
		update Currency set formatCountryID=@ctryID where currencyISOCode='GBP'	
	end

if exists(select * from Currency where currencyISOCode='ILS' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='IL'
		update Currency set formatCountryID=@ctryID where currencyISOCode='ILS'	
	end

if exists(select * from Currency where currencyISOCode='JPY' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='JP'
		update Currency set formatCountryID=@ctryID where currencyISOCode='JPY'	
	end

if exists(select * from Currency where currencyISOCode='NOK' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='NO'
		update Currency set formatCountryID=@ctryID where currencyISOCode='NOK'	
	end

if exists(select * from Currency where currencyISOCode='SEK' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='SE'
		update Currency set formatCountryID=@ctryID where currencyISOCode='SEK'	
	end

if exists(select * from Currency where currencyISOCode='USD' and formatCountryID is null)
	begin
		select @ctryID=countryid from country where isocode='US'
		update Currency set formatCountryID=@ctryID where currencyISOCode='USD'	
	end


IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'r1CurrencyID' AND Object_ID = Object_ID(N'currency'))
BEGIN
    ALTER TABLE currency ADD r1CurrencyID varchar(50);
END