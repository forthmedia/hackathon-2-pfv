/* 2014-01-27 WAB/NJH CASE 438718 Performance improvements to field triggers, add column to record whether or not fieldTrigger takes #ModifiedEntityIDs Table  */
if not exists (select 1 from information_schema.columns where table_name='fieldTrigger' and column_name='takesModifiedEntityIDsTable')
alter table fieldTrigger add takesModifiedEntityIDsTable bit null

