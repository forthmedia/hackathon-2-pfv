/* Add some columns to the visit table so that I can control sessions remotely 
	Need to know which server each visit is on
*/ 
if not exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'coldFusionInstanceid') 
BEGIN
	Alter table visit add  coldFusionInstanceid int
END

if not exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'sessionEndedDate') 
BEGIN
	Alter table visit add  sessionEndedDate datetime
END

if not exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'metaData') 
BEGIN
	Alter table visit add  metaData nvarchar(1000)
END

/* 
This changes the jessionid column to varchar if it was previously binary.  All old info will be lost (well garbled) but this is not a problem
*/
alter table visit alter column jsessionid varchar (255)

if not exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'siteDefDomainID') 
BEGIN
	Alter table visit add  siteDefDomainID int
END

GO

/* populate the new siteDefDomainID column */
if exists (select 1 from visit where siteDefDomainID is null) and exists (select 1 from information_schema.columns where table_name='visit' and column_name='site')
begin
	/* WAB 2013-11-21
		 I don't think that we should do this, if it isn't a current siteDefDomain then it can't be that important!
	insert into siteDefDomain(siteDefID,testSite,domain,mainDomain,active)
	select distinct 0,0, rtrim(ltrim(v.site)),0,0 from visit v
		left join siteDefDomain sdd on ltrim(rtrim(sdd.domain)) = rtrim(ltrim(v.site))
	where sdd.id is null
	*/	
	
	
	/* WAB 2015-06-23 rearranged this code a bit, was erroring when upgrading a very old system */
	declare @sql nvarchar(max)

	IF EXISTS (select * from sysindexes where id=object_id('visit') and name='idx_personID_visitStartedDate_include_visitId_siteDefDomainID')
	BEGIN
		select @sql = 'alter index idx_personID_visitStartedDate_include_visitId_siteDefDomainID on visit disable'
		exec (@sql)
	END
	
		select @sql = '
			declare @rows int, @msg varchar (100), @startTime dateTime 
			set @rows = 1		
			SET ROWCOUNT 10000
			SET nocount on
			while @rows <> 0 
			BEGIN
				select @startTime = getdate()
				update visit set siteDefDomainID = sdd.ID
					from siteDefDomain sdd inner join visit v on v.site like sdd.domain + ''%''  /* WAB 2013-11-21 sometimes v.site has a port added to it, hence the like statement */
				where 	siteDefDomainID is null
				
				set @rows = @@rowcount	
				select @msg = convert(varchar, @rows) + '' rows updated '' + convert(varchar,datediff(s,@starttime,getdate())) + ''s''
				RAISERROR(@msg ,0,1) WITH NOWAIT
			END
			SET ROWCOUNT 0
		'

		exec (@sql)		



	IF EXISTS (select * from sysindexes where id=object_id('visit') and name='idx_personID_visitStartedDate_include_visitId_siteDefDomainID')
		alter index idx_personID_visitStartedDate_include_visitId_siteDefDomainID on visit rebuild
	

end

/* do not drop the column on dev sites for backwards compatibility issues */
if not exists (select 1 from visit where siteDefDomainID is null) and not exists(select 1 from coldfusionInstance_autoPopulated where testSite = 2)
and exists (select 1 from information_schema.columns where table_name='visit' and column_name='site')
alter table visit drop column site

/* NJH 2013/10/18 activities */
IF NOT EXISTS (select * from sysindexes where id=object_id('visit') and name='idx_personID_visitStartedDate_include_visitId_siteDefDomainID')
create nonclustered index [idx_personID_visitStartedDate_include_visitId_siteDefDomainID] on dbo.visit (personId,visitStartedDate) include (visitID,siteDefDomainId)


/* 2014-02-26 WAB remove cfid and cftoken from the visit table.  We don't use them and in CF10 they sometimes seem to no longer be of type int */ 
if exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'cfid')
	alter table visit drop column cfid

if exists (select 1 from information_schema.columns where table_name = 'visit' and column_name = 'cftoken')
	alter table visit drop column cftoken
	

/* 2015-05-19 	WAB CASE 444754 Rotate Session Code 
	Add an index to jsessionid column.  
	Needed to be able to update the jSessionID during the session rotation
	Might take quite a long time on a large system! 
*/	
if not exists (select 1 from sysindexes where name = 'visit_jsessionid')
	create index visit_jsessionid on visit (jsessionid)
	