/* derived from the twitterAccount table */
if exists (select 1 from information_schema.TABLES where TABLE_NAME='twitterAccount')
	exec sp_rename 'twitterAccount' ,'broadcastAccount'

if exists (select 1 from information_schema.columns where table_name = 'broadcastAccount' and column_name='twitterAccountID')
	exec sp_rename 'broadcastAccount.twitterAccountID' ,'broadcastAccountID'
GO
	
if not exists (select 1 from sysobjects where name='broadcastAccount' and type='u')
CREATE TABLE [dbo].[broadcastAccount](
	[broadcastAccountID] [int] IDENTITY(1,1) NOT NULL,
	[account] [nvarchar](50) NOT NULL,
	[countryID] [int] NOT NULL,
	[languageID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_broadcastAccount] PRIMARY KEY CLUSTERED 
(
	[broadcastAccountID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/*
Add default constraints
for ease of coding drop and recreate any which have twitterAccount names
*/


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_twitterAccount_countryID]') AND type = 'D')
ALTER TABLE broadcastaccount DROP CONSTRAINT [DF_twitterAccount_countryID]    

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_broadcastAccount_countryID]') AND type = 'D')
ALTER TABLE [dbo].[broadcastAccount] ADD  CONSTRAINT [DF_broadcastAccount_countryID]  DEFAULT ((0)) FOR [countryID]

GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_twitterAccount_languageID]') AND type = 'D')
ALTER TABLE broadcastaccount DROP CONSTRAINT [DF_twitterAccount_languageID]    

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_broadcastAccount_languageID]') AND type = 'D')
ALTER TABLE [dbo].[broadcastAccount] ADD  CONSTRAINT [DF_broadcastAccount_languageID]  DEFAULT ((1)) FOR [languageID]
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_twitterAccount_created]') AND type = 'D')
ALTER TABLE broadcastaccount DROP CONSTRAINT [DF_twitterAccount_created]    

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_broadcastAccount_created]') AND type = 'D')
ALTER TABLE [dbo].[broadcastAccount] ADD  CONSTRAINT [DF_broadcastAccount_created]  DEFAULT (getdate()) FOR [created]
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_twitterAccount_lastUpdated]') AND type = 'D')
ALTER TABLE broadcastaccount DROP CONSTRAINT [DF_twitterAccount_lastUpdated]    

IF  NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_broadcastAccount_lastUpdated]') AND type = 'D')
ALTER TABLE [dbo].[broadcastAccount] ADD  CONSTRAINT [DF_broadcastAccount_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]

GO

if not exists (select 1 from information_schema.columns where table_name = 'broadcastAccount' and column_name='serviceID')
alter table broadcastAccount add serviceID int
GO

declare @sql nvarchar(max)
select @sql = 'update broadcastAccount set serviceID = (select serviceID from service where serviceTextID=''Twitter'') where serviceID is null'
if exists (select 1 from broadcastAccount where serviceID is null)
exec sp_executesql @sql

alter table broadcastAccount alter column serviceID int not null

if not exists (select 1 from information_schema.columns where table_name = 'broadcastAccount' and column_name='accessToken')
alter table broadcastAccount add accessToken varchar(250)

if not exists (select 1 from information_schema.columns where table_name = 'broadcastAccount' and column_name='serviceAccountID')
alter table broadcastAccount add serviceAccountID varchar(50)

GO