/* LID 4314 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='Campaign' AND column_name='active')
	Alter table Campaign Add active bit default 0
GO

Update Campaign	Set active=1 where active is null

if exists (select 1 from information_schema.columns where table_name='campaign' and column_name='startDate' and data_type='datetime')
begin
	begin tran
		exec dropAndRecreateDependentObjects 'campaign.startDate', 'drop'
		alter table campaign alter column startDate date null
		exec dropAndRecreateDependentObjects 'campaign.startDate', 'recreate'
	commit
end

if exists (select 1 from information_schema.columns where table_name='campaign' and column_name='endDate' and data_type='datetime')
begin
	begin tran
		exec dropAndRecreateDependentObjects 'campaign.endDate', 'drop'
		alter table campaign alter column endDate date null
		exec dropAndRecreateDependentObjects 'campaign.endDate', 'recreate'
	commit
end