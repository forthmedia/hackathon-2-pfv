/* add the relayModule table that holds the active modules for a given client */
If not exists (select 1 from dbo.sysobjects where name = 'relayModule' and xtype='u')
CREATE TABLE [dbo].[relayModule](
	[moduleID] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_relayModule] PRIMARY KEY CLUSTERED 
(
	[moduleID] ASC
)
-- WAB 2010/11/09 removed the with statement, not 2000 compatible WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
ON [PRIMARY]
) ON [PRIMARY]



IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF_relayModule_active') AND xtype = 'D')
ALTER TABLE [dbo].[relayModule] ADD  CONSTRAINT [DF_relayModule_active]  DEFAULT ((1)) FOR [active]
GO

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF_relayModule_created') AND xtype = 'D')
ALTER TABLE [dbo].[relayModule] ADD  CONSTRAINT [DF_relayModule_created]  DEFAULT (getdate()) FOR [created]
GO

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'DF_relayModule_lastUpdated') AND xtype = 'D')
ALTER TABLE [dbo].[relayModule] ADD  CONSTRAINT [DF_relayModule_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO


if not exists (select 1 from information_schema.columns where table_name='relayModule' and column_name='moduleTextID')
alter table relayModule add moduleTextID varchar(50)
GO

if not exists (select 1 from information_schema.columns where table_name='relayModule' and column_name='name')
alter table relayModule add name varchar(50)

if not exists (select 1 from information_schema.columns where table_name='relayModule' and column_name='description')
alter table relayModule add description varchar(250)
GO

if not exists (select 1 from relayModule where moduleID = 21)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (21,'Sales Out Data','SalesOutData',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 661)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (661,'Content & Portal Manager','Content',4,4,0)
GO

/* approvals used to be 42 - changed to 22 as it conflicted with Profile Manager */
if not exists (select 1 from relayModule where moduleID = 22)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (22,'Approvals','Approvals',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 30)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (30,'Application','Application',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 31)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (31,'Event Manager','Events',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 32)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (32,'Incentive Manager','Incentive',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 33)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (33,'eService','eService',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 34)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (34,'eMarketing','Communicate',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 35)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (35,'eLearning','eLearning',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 36)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (36,'Locator','Locator',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 37)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (37,'Fund Manager','Funds',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 38)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (38,'eCommerce','Orders',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 39)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (39,'Lead & Opportunity','LeadManager',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 41)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (41,'Forecasting','Forecasting',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 42)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (42,'Profile Manager','ProfileManager',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 43)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (43,'Home Page','Home',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 44)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (44,'Customised Joint Marketing','CJM',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 45)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (45,'Training Manager','TrainingManager',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 46)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (46,'Product Manager','Products',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 47)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (47,'Projects','Projects',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 48)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (48,'Scorecard','Scorecard',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 49)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (49,'Lead & Opportunity (no products)','LeadManagerNoProducts',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 50)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (50,'Cashback','Cashback',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 51)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (51,'SalesForce','SalesForce',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 52)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (52,'Report Manager','ReportManager',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 53)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (53,'Configure','Config',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 54)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (54,'Social','Social',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 55)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (55,'Plug-ins','Plugins',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 56)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (56,'Discussions','Discussions',4,4,0)
GO

if not exists (select 1 from relayModule where moduleID = 57)
insert into relayModule (moduleID,name,moduleTextID,createdBy,lastUpdatedBy,active) values (57,'My Favorites','MyFavorites',4,4,0)
GO


/* 
WAB 2015-10-07 Add if statement and dynamic SQL to protect against non existence of relayserver or non existence of particular table
*/

IF Exists (select 1 from sys.databases where name = 'relayserver')
BEGIN	

	declare @tableExists bit, 
			@checkForTableSQL nvarchar(max) = 'select @tableExists = count(1) from relayserver.information_schema.tables where table_name = ''module'' '

	exec sp_executeSQL @checkForTableSQL,N'@tableExists bit OUTPUT', @tableExists = @tableExists OUTPUT
		
	IF @tableExists <> 0
	BEGIN
       	declare @fullSQL nvarchar(max) = '
				update relayModule set name=moduleName,description=moduleDescription,moduleTextID=m.moduleTextID from
					relayModule rm inner join relayserver.dbo.module m
					on rm.moduleId = m.moduleId
    		'
		exec (@fullSQL)

	END
        	
END              


	
alter table relayModule alter column moduleTextID varchar(50) not null
alter table relayModule alter column name varchar(50) not null
GO

IF NOT EXISTS(SELECT * FROM sysindexes WHERE id = OBJECT_ID(N'[dbo].[relayModule]') AND name = N'IX_relayModule_ModuleTextID')
CREATE UNIQUE NONCLUSTERED INDEX [IX_relayModule_ModuleTextID] ON [dbo].[relayModule] 
(
	[ModuleTextID] ASC
) ON [PRIMARY]
GO