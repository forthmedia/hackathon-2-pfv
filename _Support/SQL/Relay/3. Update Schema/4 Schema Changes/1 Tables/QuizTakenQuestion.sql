IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizTakenQuestion]') AND type in (N'U'))
   BEGIN
	CREATE TABLE [dbo].[QuizTakenQuestion] (
	[QuizTakenQuestionID] int IDENTITY(1,1) NOT NULL  
	, [QuizTakenID] int  NOT NULL  
	, [QuestionID] int  NOT NULL  
	, [Viewed] bit  NULL  
	, [ViewedAt] Datetime  NULL  
	, [QuestionTextGiven] nvarchar(4000)  NULL  
	)


ALTER TABLE [dbo].[QuizTakenQuestion] ADD CONSTRAINT [QuizTakenQuestion_PK] PRIMARY KEY CLUSTERED (
[QuizTakenQuestionID]
)
END
GO

/* BEGIN STCR Phase 2 SCORM SQL */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion' AND column_name='ScormInteractionID') 
	ALTER TABLE QuizTakenQuestion add ScormInteractionID varchar(4000) NULL
GO
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion' AND column_name='ScormResultState') 
	ALTER TABLE QuizTakenQuestion add ScormResultState varchar(32) NULL
GO
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTakenQuestion' AND column_name='ScormResultScore') 
	ALTER TABLE QuizTakenQuestion add ScormResultScore real NULL
GO
/* END STCR Phase 2 SCORM SQL */

/* 2014-09-22 PPB Case 439473 */
if not exists (SELECT 1 FROM information_schema.columns where table_name='QuizTakenQuestion' and column_name='InteractionType')
	alter table QuizTakenQuestion add InteractionType nvarchar(20) NULL
GO

/* 2014-09-22 PPB Case 439473 */
if not exists (SELECT 1 FROM information_schema.columns where table_name='QuizTakenQuestion' and column_name='CorrectResponses')
	alter table QuizTakenQuestion add CorrectResponses nvarchar(max) NULL
GO



IF NOT EXISTS (select * from sysindexes where id=object_id('QuizTakenQuestion') and name='idx_QuizTakenID')
	CREATE NONCLUSTERED INDEX [idx_QuizTakenID]
		ON [dbo].[QuizTakenQuestion] ([QuizTakenID])
		INCLUDE ([QuizTakenQuestionID],[QuestionID])
GO
