
IF object_id('SAMLIdentityProvider') is null  
BEGIN
	Create Table [dbo].[SAMLIdentityProvider](
		SAMLIdentityProviderId int identity(1,1)  NOT NULL,
		identityProviderName nVarChar(max)  NOT NULL,
		identityProviderEntityID nVarChar(256) NOT NULL,  --cant be nVarCharMax as indexed
		authnRequestURL nVarChar(max)  NOT NULL,
		nameIDField_Relayware nVarChar(256) NOT NULL, --the field on RW that is used as the unique identifier
		nameIDField_Foreign nVarChar(256) NULL, --should be null (which indicates the assertion nameID is the nameID), but can be annother field from the attributes 
		rawIDPMetadata nVarChar(max) NOT NULL,
		active bit NOT NULL default 1,
		allowAutoCreate bit NOT NULL default 0,
		autoCreate_SelectLocOrg bit NOT NULL default 1,
		autoCreate_selectedOrganisationID int null,
		autoCreate_selectedLocationID int null,
		autoCreate_detectedLoc_RelaywareField nVarChar(max),
		autoCreate_detectedLoc_ForeignField nVarChar(max),
		permittedToSetUsergroups bit default 0,
		assertionAttributeToControlUsergroups nVarChar(max) default '',
		permittedToControlCountryRights bit default 0,
		assertionAttributeToControlCountryRights nVarChar(max) default '',
		createdByPerson int,
		createdby int,
		created date,
		LastUpdatedBy int,
		lastUpdatedByPerson int,
		lastUpdated date,
		certificateToUse nVarChar(256),
		certificateWarningSent bit default 0
		 CONSTRAINT SAMLIdentityProvider_SAMLIdentityProviderId PRIMARY KEY CLUSTERED 
		(
			SAMLIdentityProviderId ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]);
		

	
	
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
		 VALUES (554, 'SAMLIdentityProvider', 'SAMLIdentityProviderId', 0, 'SAIP', 0);
	
	CREATE INDEX identityProviderIdentifierIndex
	ON SAMLIdentityProvider (identityProviderEntityID);	 
	
	exec generate_MRAudit @TableName = 'SAMLIdentityProvider', @mnemonic = 'SAIP'
	
END

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'SAMLIdentityProvider' AND column_name='isPermittedToInitiateSAML') 
BEGIN
	ALTER TABLE SAMLIdentityProvider ADD isPermittedToInitiateSAML BIT DEFAULT 0 NOT NULL
	exec generate_MRAudit @TableName = 'SAMLIdentityProvider', @mnemonic = 'SAIP'
END
 
IF COL_LENGTH('SAMLIdentityProvider','onCreationPortalPersonApprovalStatus') IS NULL
BEGIN
	ALTER TABLE dbo.SAMLIdentityProvider ADD onCreationPortalPersonApprovalStatus nVARCHAR(200) NULL
	exec generate_MRAudit @TableName = 'SAMLIdentityProvider', @mnemonic = 'SAIP'
	
END