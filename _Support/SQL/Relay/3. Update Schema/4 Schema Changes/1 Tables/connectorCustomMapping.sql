if not exists (select 1 from information_schema.tables where table_name='ConnectorCustomMapping')
Begin
	CREATE TABLE [dbo].[ConnectorCustomMapping](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorType] [varchar](20) NOT NULL,
		[direction] [char](1) NOT NULL,
		[entityName] [varchar](50) NOT NULL,
		[columnName] [varchar](50) NOT NULL,
		[customised] [bit] NOT NULL CONSTRAINT [DF_ConnectorCustomMapping_custom]  DEFAULT ((1)),
		[selectStatement] [varchar](max) NOT NULL,
		[joinStatement] [varchar](max) NULL,
		[selectDependencyStatement] [varchar](100) NULL,
		[created] [datetime] NOT NULL CONSTRAINT [DF_ConnectorCustomMapping_created] DEFAULT (getdate()),
	 CONSTRAINT [PK_ConnectorCustomMapping] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

End

if not exists (select 1 from information_schema.columns where table_name='ConnectorCustomMapping' and column_name='active')
	alter table ConnectorCustomMapping add active bit not null default(1)
	
alter table ConnectorCustomMapping alter column selectStatement varchar(max) NOT NULL

alter table ConnectorCustomMapping alter column selectDependencyStatement varchar(max) NULL