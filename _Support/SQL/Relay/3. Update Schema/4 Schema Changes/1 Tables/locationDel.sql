if not exists (select * from information_schema.columns where table_name='locationDel' and column_name='Latitude')  
      alter TABLE [dbo].locationDel add Latitude float NULL

if not exists (select * from information_schema.columns where table_name='locationDel' and column_name='Longitude')  
      alter TABLE [dbo].locationDel add Longitude float NULL

if not exists (select * from information_schema.columns where table_name='locationDel' and column_name='LatLongSource')  
      alter TABLE [dbo].locationDel add [LatLongSource] varchar(50) NULL
      

/*  WAB 2013-12-09 added includes to this index so that LocationDel can be searched quickly in vEntityName view */
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LocationDel]') AND name = N'LocationID_Idx')
DROP INDEX [dbo].[LocationDel].[LocationID_Idx]
GO

CREATE NONCLUSTERED INDEX [LocationID_Idx] ON [dbo].[LocationDel]
(
	[LocationID] ASC
) 
INCLUDE (sitename, CRMLocID)
ON [PRIMARY]
GO

if not exists (select * from information_schema.columns where table_name='locationDel' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].locationDel add [lastUpdatedByPerson] int NULL

GO

/* NJH 2015/07/16 - for vEntityname */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[LocationDel]') AND name = N'crmLocID_idx')
BEGIN
	CREATE INDEX crmLocID_idx ON locationDel (crmlocid);
END	
GO