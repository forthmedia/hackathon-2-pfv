/* change to entityTrackingBase as entityTracking will now be a view */
if not exists (select 1 from information_schema.TABLES where TABLE_NAME='EntityTrackingBase')
	exec sp_rename 'EntityTracking' ,'EntityTrackingBase'
	
/* LHID5321*/
--alter table EntityTrackingBase alter column toLoc nvarchar(350)

/****** Object:  Index [IX_PersonID2]    Script Date: 04/19/2012 10:32:32 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityTrackingBase]') AND name = 'IX_PersonID2')
DROP INDEX [IX_PersonID2] ON [dbo].[EntityTrackingBase] 
GO
CREATE NONCLUSTERED INDEX [IX_PersonID2] ON [dbo].[EntityTrackingBase] 
(
	[PersonID] ASC
)
INCLUDE ( [EntityTrackingID],
[EntityID],
[EntityTypeID],
[commid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


if not exists (select 1 from information_schema.columns where table_name='EntityTrackingBase' and column_name='visitID')
alter table EntityTrackingBase add visitID int
GO

if not exists (select 1 from information_schema.columns where table_name='EntityTrackingBase' and column_name='fromUrlID')
alter table EntityTrackingBase add fromUrlID int

if not exists (select 1 from information_schema.columns where table_name='EntityTrackingBase' and column_name='toUrlID')
alter table EntityTrackingBase add toUrlID int

if exists (select 1 from information_schema.columns where table_name='EntityTrackingBase' and column_name='userGroupID')
alter table EntityTrackingBase drop column userGroupID

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EntityTrackingBase]') AND name = 'idx_entityTypeID_personID_created_include_visitID')
DROP INDEX [idx_entityTypeID_personID_created_include_visitID] ON [dbo].[EntityTrackingBase] 
GO
create nonclustered index [idx_entityTypeID_personID_created_include_visitID] on dbo.EntityTrackingBase (entitytypeID,personID,created) include (visitID)
GO

IF  EXISTS (SELECT 1 FROM sysindexes WHERE name = N'IX_toLoc')
	drop index entitytrackingBase.IX_toLoc
GO


if not exists (select 1 from information_schema.columns where table_name='EntityTrackingBase' and column_name='clickThru')
begin
	alter table EntityTrackingBase add clickThru bit default 0
end

GO

update EntityTrackingBase set clickThru = 0 where clickThru is null and fromUrlID is not null
update entityTrackingBase set clickThru=1 where fromUrlID is null and isNull(clickThru,0) != 1