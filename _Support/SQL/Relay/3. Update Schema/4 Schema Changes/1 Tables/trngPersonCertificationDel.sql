/* 2015/12/20 PYW P-TAT006 BRD 31 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngPersonCertificationDel') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngPersonCertificationDel' AND column_name='expiryDate') 
BEGIN
	ALTER TABLE trngPersonCertificationDel
	ADD expiryDate datetime NULL
END
GO