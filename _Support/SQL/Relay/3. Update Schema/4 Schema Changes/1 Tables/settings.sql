if not exists (select 1 from information_schema.tables where table_name = 'settings')
BEGIN
CREATE TABLE [dbo].[settings] (
 [id] [int] IDENTITY (1, 1) NOT NULL ,
 [name] [varchar] (200) NULL ,
 [value] [varchar] (200) NULL ,
 [testSite] [varchar] (20) NULL ,
 [condition] [varchar] (100) NULL ,
 [sitedefID] [varchar] (20) NULL ,
  UNIQUE  NONCLUSTERED 
 (
  [id]
 )  ON [PRIMARY] 
) ON [PRIMARY]
END

GO
if not exists (select 1 from information_schema.columns where table_name = 'settings' and column_name = 'lastupdated')
alter table settings add  lastupdated datetime
GO
if not exists (select 1 from information_schema.columns where table_name = 'settings' and column_name = 'lastupdatedbypersonid') and not exists (select 1 from information_schema.columns where table_name = 'settings' and column_name = 'lastupdatedbyperson')
alter table settings add lastupdatedbypersonid int

GO

-- this is just to deal with a change of table name during dev.  
-- Only needed to run once on each dev box which has 8.3 at the moment
if exists (select 1 from sysobjects where name = 'relayvarv2')
BEGIN
	insert into settings
	(name, value)
	select relayVArName, relayVarValue
	from 
	relayvarv2

	drop table relayVarV2
END	
	
GO

-- move over the one variable which is required to get everthing under way
/* NJH removed as relayVar no longer exists and script no longer needed
if not exists(select 1 from settings where name='security.encryptionmethods.standard.passkey')
	insert into settings (Name, Value) 
	select 'security.encryptionmethods.standard.passkey',relayvarvalue
	from relayvar where relayvarname = 'EncryptionParams.standard.passkey'


if exists (select 1 from relayvar where relayvarname='encryptionMethod.relayPassword' and relayvarvalue <>'passwordHash')
	insert into settings (Name, Value) 
	select 'security.encryptionmethods.relayPassword.method',relayvarvalue
	from relayvar where relayvarname = 'encryptionMethod.relayPassword'
*/



/* WAB 2011/04/27 give settings a primary key so it is ready for doing auditing*/
if not exists (select 1 from sysObjects where name = 'PK_settings' )
BEGIN
	ALTER TABLE dbo.settings ADD CONSTRAINT
		PK_settings PRIMARY KEY CLUSTERED 
		(
		id
		) ON [PRIMARY]
END

/* Personal/UserGroup/Country Settings (initially for Report Designer) */
if not exists(select 1 from information_schema.columns where table_name='settings' and column_name='entityTypeID')
alter table settings add entityTypeID integer
GO

if not exists(select 1 from information_schema.columns where table_name='settings' and column_name='entityID')
alter table settings add entityID integer
GO

if not exists(select 1 from information_schema.columns where table_name='settings' and column_name='countryID')
alter table settings add countryID integer
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME	= 'Settings' AND COLUMN_NAME = 'value')
ALTER TABLE Settings ALTER COLUMN value nvarchar(max)
GO

if not exists(select 1 from information_schema.columns where table_name='settings' and column_name='lastUpdatedBy')
alter table settings add lastUpdatedBy integer
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Settings' AND COLUMN_NAME = 'lastUpdatedByPersonId')
AND NOT Exists (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Settings' AND COLUMN_NAME = 'lastUpdatedByPerson')
exec sp_rename 'settings.lastUpdatedByPersonId', 'lastUpdatedByPerson','column'
GO


