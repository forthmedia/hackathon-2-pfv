IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[discussionGroup]') AND type in (N'U'))

CREATE TABLE [dbo].[discussionGroup] (	
	[discussionGroupID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,	
	[title] [nvarchar] (255) NOT NULL,
	[description] [nvarchar] (max) NOT NULL,		
	[created] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [int] NOT NULL DEFAULT (0),
	[lastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL DEFAULT (0),
	[lastUpdatedByPerson] [int] NOT NULL DEFAULT (0)
)
GO

if not exists (select 1 from information_schema.columns where table_schema = 'dbo' and table_name = 'discussionGroup' and column_name = 'active') 
	alter table [dbo].[discussionGroup] add active bit not null default 1
GO
