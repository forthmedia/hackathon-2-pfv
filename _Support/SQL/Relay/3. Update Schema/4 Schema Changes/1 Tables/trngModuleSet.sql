/* trngModuleSet */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModuleSet') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModuleSet' AND column_name='TrainingProgramID') 
ALTER TABLE trngModuleSet add TrainingProgramID int NULL
GO


/* NJH 2012/12/12 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModuleSet' AND column_name='lastUpdatedByPerson') 
ALTER TABLE trngModuleSet add lastUpdatedByPerson int NULL
GO

/* STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModuleSet' AND column_name='description') 
ALTER TABLE trngModuleSet ALTER COLUMN description nvarchar(100) NULL
GO

/* IH 2013/04/12 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngModuleSet' AND column_name='description_defaultTranslation') 
ALTER TABLE trngModuleSet add description_defaultTranslation nvarchar(100) NULL
GO