/*
WAB 2016-10-20
This OOTB Custom entity was scripted with a del table but not with its auditing
*/
if exists (select 1 from information_schema.tables where table_name='businessObjective')
exec generate_mrAudit @tablename='businessObjective'