
IF not exists (select 1 from sysobjects where name='connectorQueueError' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorQueueError](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorQueueID] [int] NOT NULL,
		[message] [nvarchar](500) NOT NULL,
		[field] [varchar](20) NULL,
		[value] [nvarchar](250) NULL,
		[statusCode] [varchar](50) NULL,
		[connectorResponseID] [int] NULL,
		[created] [datetime] NULL,
	 CONSTRAINT [PK_connectorQueueError] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[connectorQueueError] ADD  CONSTRAINT [DF_connectorQueueError_created]  DEFAULT (getdate()) FOR [created]
END
GO

if exists(select 1 from information_schema.columns where table_name='connectorQueueError' and column_name='field' and character_maximum_length != 50)
alter table connectorQueueError alter column field varchar(50)

/* JIRA PROD2016-1336 */
if not exists(select 1 from sys.indexes where object_id = OBJECT_ID(N'[dbo].[connectorQueueError]') AND name = N'idx_connectorQueueError_connectorResponseID')
CREATE NONCLUSTERED INDEX [idx_connectorQueueError_connectorResponseID] ON [dbo].[connectorQueueError] ([connectorResponseID])
