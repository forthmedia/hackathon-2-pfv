/* Very nasty construct to change text to nText.  Can't use alter column.
 Then had to use to ifs and a GO because won't compile when column temp does not exist*/
if exists (select 1 from information_schema.columns where table_name = 'eventGroupEmail' and column_name = 'messageText' and data_type <> 'nText')
	alter table eventGroupEmail add temp ntext 
GO
if exists (select 1 from information_schema.columns where table_name = 'eventGroupEmail' and column_name = 'messageText' and data_type <> 'nText')
BEGIN
	BEGIN TRANSACTION
	
	CREATE TABLE dbo.Tmp_EventGroupEmail
		(
		EventGroupID int NOT NULL,
		EventGroupEmailType varchar(50) NOT NULL,
		subject nvarchar(255) NULL,
		messagetext ntext NULL,
		eventGroupEmailID int NOT NULL IDENTITY (1, 1)
		)  
	
		SET IDENTITY_INSERT dbo.Tmp_EventGroupEmail ON
		IF EXISTS(SELECT * FROM dbo.EventGroupEmail)
			 EXEC('INSERT INTO dbo.Tmp_EventGroupEmail (EventGroupID, EventGroupEmailType, subject, messagetext, eventGroupEmailID)
				SELECT EventGroupID, EventGroupEmailType, subject, CONVERT(ntext, messagetext), eventGroupEmailID FROM dbo.EventGroupEmail WITH (HOLDLOCK TABLOCKX)')
		SET IDENTITY_INSERT dbo.Tmp_EventGroupEmail OFF
	
		DROP TABLE dbo.EventGroupEmail
	
		EXECUTE sp_rename N'dbo.Tmp_EventGroupEmail', N'EventGroupEmail', 'OBJECT' 
		ALTER TABLE dbo.EventGroupEmail ADD CONSTRAINT
			PK_EventGroupEmail PRIMARY KEY NONCLUSTERED 
			(
			eventGroupEmailID
			) ON [PRIMARY]
	
		COMMIT
END

/* WAB 2011/04/27 add nVarchar support, although it doesn't seem as if we ever do an update! */
alter table eventGroupEmail alter column subject nvarchar (255)