/* trngCertification */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification' AND column_name='TrainingProgramID') 
ALTER TABLE trngCertification add TrainingProgramID int NULL
GO

/* Social Changes 2012/10/23 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification' AND column_name='share') 
	ALTER TABLE trngCertification add share bit NOT NULL DEFAULT 1
GO

/* migrate trngCertifications */
exec migrateToEntityPhrases @phrasetextID = 'Title', @tablename = 'trngCertification', @columnname = 'titlePhraseTextID'
GO
exec migrateToEntityPhrases @phrasetextID = 'Description', @tablename = 'trngCertification', @columnname = 'descriptionPhraseTextID'
GO

/* 2015/12/20 PYW P-TAT006 BRD 31 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertification' AND column_name='validity') 
BEGIN
	ALTER TABLE trngCertification
	ADD validity integer NULL
END
GO