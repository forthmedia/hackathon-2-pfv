-- P-CYM006 Deal Registration changes
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunity' AND column_name='oppCustom1') 
ALTER TABLE opportunity add oppCustom1 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunity' AND column_name='oppCustom2') 
ALTER TABLE opportunity add oppCustom2 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunity' AND column_name='oppCustom3') 
ALTER TABLE opportunity add oppCustom3 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunity' AND column_name='oppCustom4') 
ALTER TABLE opportunity add oppCustom4 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunityDel' AND column_name='oppCustom1') 
ALTER TABLE opportunityDel add oppCustom1 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunityDel' AND column_name='oppCustom2') 
ALTER TABLE opportunityDel add oppCustom2 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunityDel' AND column_name='oppCustom3') 
ALTER TABLE opportunityDel add oppCustom3 nvarchar(250) NULL

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'opportunityDel' AND column_name='oppCustom4') 
ALTER TABLE opportunityDel add oppCustom4 nvarchar(250) NULL


if not exists (select 1 from information_schema.columns where table_name='opportunity' and column_name='campaignID')
BEGIN
alter table opportunity add campaignID int null
END
GO
if not exists (select 1 from information_schema.columns where table_name='opportunityDel' and column_name='campaignID')
BEGIN
alter table opportunityDel add campaignID int null
END
GO

/* NJH removing the default countryID of 9 and setting it to 0 */
if exists (select top 1 * from sysconstraints where constid=object_id('DF_opportunity_countryID'))
begin
	declare @defaultValue varchar(20)
	SELECT @defaultValue = object_definition(default_object_id) FROM sys.columns WHERE  name ='countryID' AND object_id = object_id('opportunity')

	if (@defaultValue = '((9))')
		alter table dbo.opportunity drop constraint DF_opportunity_countryID
end

GO

if not exists (select top 1 * from sysconstraints where constid=object_id('DF_opportunity_countryID'))
ALTER TABLE [dbo].[opportunity] ADD  CONSTRAINT [DF_opportunity_countryID]  DEFAULT (0) FOR [countryID]
GO

/****** Object:  Index [crmOppID]    Script Date: 03/02/2012 11:28:13 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Opportunity]') AND name = N'crmOppID')
DROP INDEX [crmOppID] ON [dbo].[Opportunity] 
GO

/****** Object:  Index [crmOppID]    Script Date: 03/02/2012 11:28:14 ******/
/*CREATE NONCLUSTERED INDEX [crmOppID] ON [dbo].[Opportunity] 
(
	[crmOppID] ASC
) ON [PRIMARY]
GO*/

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Opportunity]') AND name = N'crmOppID_unique')
BEGIN
	update opportunity set crmOppID = null where crmOppID = ''
	CREATE unique INDEX crmOppID_unique ON opportunity (crmOppId) WHERE crmOppId IS NOT NULL ;
END	
GO

if not exists (select * from information_schema.columns where table_name='opportunity' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].opportunity add [lastUpdatedByPerson] int NULL

GO

/* MOBILE */
if not exists (select * from information_schema.columns where table_name='opportunity' and column_name='estimatedBudget')  
      alter TABLE [dbo].opportunity add [estimatedBudget] int NULL

GO
/* 2013/02/05	YMA		104 Forecast Opportunity */
if not exists (select 1 from information_schema.columns where table_name = 'opportunity' and column_name = 'forecast')
ALTER TABLE [dbo].opportunity ADD	[forecast] bit NULL
GO

/* 02/06/2014 - Connector - DROP Default contraints */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_contactPersonID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_contactPersonID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_partnerSalesManagerPersonID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_partnerSalesManagerPersonID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_partnerSalesPersonID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_partnerSalesPersonID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_sponsorPersonid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_sponsorPersonid]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_vendorAccountManagerPersonID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_vendorAccountManagerPersonID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_vendorSalesManagerPersonID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_vendorSalesManagerPersonID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_partnerLocationID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_partnerLocationID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_distiLocationID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_distiLocationID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_statusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_statusID]
END

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_opportunity_countryID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [DF_opportunity_countryID]
END


alter TABLE [dbo].opportunity alter column statusID int NULL

GO




/*  2014-06-05 PPB Reduce the length from 200 to 120 because SF only allows 120; NB vEntityName has to be dropped before doing this (see dropViewsWithSCHEMABINDING.sql) 
	2015-01-21 WAB Add an if statement and plumb in dropAndRecreateDependentObjects - saves getting an error during upgrades 
*/
if exists (select 1 from INFORMATION_SCHEMA.COLUMNS where table_name = 'opportunity' and column_name = 'detail' and character_maximum_length <> 120)
BEGIN
		exec dropAndRecreateDependentObjects 'opportunity.detail', 'dropfulltextindexes'
	begin tran
		exec dropAndRecreateDependentObjects 'opportunity.detail', 'drop'

		ALTER table opportunity ALTER COLUMN detail nvarchar(120)
	
		exec dropAndRecreateDependentObjects 'opportunity.detail', 'recreate'
	commit
		exec dropAndRecreateDependentObjects 'opportunity.detail', 'recreatefulltextindexes'

END	


GO

if not exists (select * from information_schema.columns where table_name='opportunity' and column_name='convertedFromLeadID')  
alter TABLE [dbo].opportunity add [convertedFromLeadID] int NULL

GO

/*
   2015-03-17 AHL Case 444049 Opp Inphi Corporation- WSA EP -400 has synch error but can't resolve it?
    I would like to see the field increased to match the SFDC standard of 4,000 chars
*/
IF EXISTS (select 1 from information_schema.columns where table_name = 'Opportunity' and column_name = 'currentSituation' and character_maximum_Length <> -1)
BEGIN
   ALTER TABLE dbo.Opportunity         ALTER    COLUMN currentSituation varchar(4000)
   ALTER TABLE dbo.OpportunityDel     ALTER    COLUMN currentSituation varchar(4000) 
END

GO

/* NJH 2015/06/04 - added this field which we can hopefully start using. */
if not exists (select 1 from information_schema.columns where table_name='opportunity' and column_name='endCustomerLocationID')
begin 
      alter TABLE [dbo].opportunity add [endCustomerLocationID] int NULL
      --exec generate_mrAudit @tablename='opportunity'
end
GO

/* NJH 2015/12/17 - alter expectedClosedate to date field from datetime */
alter table opportunity alter column expectedCloseDate date null

/* NJH 2016/04/20 Sugar 449186 - make currentSituation nvarchar(max) */
alter table opportunity alter column currentSituation nvarchar(max)

/* sugar 448757 - performance enhancements */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[opportunity]') AND name = N'idx_opportunity_partnerLocationID')
CREATE NONCLUSTERED INDEX [idx_opportunity_partnerLocationID]
ON [dbo].[opportunity] ([partnerLocationID])

exec generate_mrAudit @tablename='opportunity'