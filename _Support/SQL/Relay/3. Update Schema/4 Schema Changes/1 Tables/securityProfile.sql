

/****** NJH 2016/06/16 JIRA PROD2016-595 ******/
IF  NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'PK_securityProfile') AND type in (N'PK'))
BEGIN
	ALTER TABLE [dbo].[securityProfile] ADD  CONSTRAINT [PK_securityProfile] PRIMARY KEY CLUSTERED 
	(
		[securityProfileID] ASC
	) ON [PRIMARY]
END
GO
