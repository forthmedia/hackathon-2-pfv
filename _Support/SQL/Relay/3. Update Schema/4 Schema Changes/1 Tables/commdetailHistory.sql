/*
CASE 428866 
WAB 2012-09-14
Attempt to improve speed of deduping by adding personid and locationid indexes
*/

IF NOT EXISTS (select * from sysindexes where id=object_id('commdetailHistory') and name='personid')
	CREATE NONCLUSTERED INDEX [PersonID] ON [dbo].[commdetailHistory] 
	(
		[PersonID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]



IF NOT EXISTS (select * from sysindexes where id=object_id('commdetailHistory') and name='locationid')
	CREATE NONCLUSTERED INDEX [LocationID] ON [dbo].[commdetailHistory] 
	(
		[LocationID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]



/*
WAB 2012-10-10 CASE 426507
Improve Comm Statistics Speed
WAB 2012-11-22
There was an error in this script which created the index with the wrong name [commDetailHistory.idx_commid_internal_test_commstatusID_include_personid] rather than [idx_commid_internal_test_commstatusID_include_personid] 
If the wrong one is present then rename it
*/
if exists (select * from sysindexes where id=object_id('commdetailHistory') and name='commDetailHistory.idx_commid_internal_test_commstatusID_include_personid')
	BEGIN
		if exists (select * from sysindexes where id=object_id('commdetailHistory') and name='idx_commid_internal_test_commstatusID_include_personid')
			drop index commDetailHistory.[commDetailHistory.idx_commid_internal_test_commstatusID_include_personid]
		ELSE
			exec sp_rename 'commDetailHistory.[commDetailHistory.idx_commid_internal_test_commstatusID_include_personid]','idx_commid_internal_test_commstatusID_include_personid','index'	
	END
	


IF NOT EXISTS (select * from sysindexes where id=object_id('commdetailHistory') and name='idx_commid_internal_test_commstatusID_include_personid')
CREATE NONCLUSTERED INDEX [idx_commid_internal_test_commstatusID_include_personid] ON [dbo].[commDetailHistory] 
(
	[commid] ASC,
	[internal] ASC,
	[test] ASC,
	[CommStatusID] ASC
)
INCLUDE ( [PersonID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/* 
WAB 2012-10-10 CASE 426507
Remove this index, because superceded by the more comprehensive one above
*/
IF EXISTS (select * from sysindexes where id=object_id('commdetailHistory') and name='IX_commDetailHistory_CommID')
DROP INDEX commdetailHistory.IX_commDetailHistory_CommID

/* NJH 2013/10/18 - activities */
IF NOT EXISTS (select * from sysindexes where id=object_id('commdetailHistory') and name='idx_datesent_include_personid_commStatusID_commID')
create nonclustered index [idx_datesent_include_personid_commStatusID_commID] on dbo.commDetailHistory (dateSent) include (personID,commStatusId,commID)
