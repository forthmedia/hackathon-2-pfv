/* PPB Case 438147 add VisitId to rating */

if not exists (SELECT * FROM information_schema.columns where table_name='Rating' and column_name='VisitId')
	alter table Rating add VisitId int null
