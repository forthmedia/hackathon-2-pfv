

if not exists (select 1 from sysobjects where name='connectorResponse' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorResponse](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[sendXML] [xml] NULL,
		[responseXML] [xml] NOT NULL,
		[connectorObjectID] [int] NULL,
		[method] [varchar](20) NULL,
		[created] [datetime] NOT NULL,
	 CONSTRAINT [PK_connectorResponse] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorResponse] ADD  CONSTRAINT [DF_connectorResponse_created]  DEFAULT (getdate()) FOR [created]
END