IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizAnswer]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[QuizAnswer] (
	[AnswerID] int IDENTITY(1,1) NOT NULL  
	, [QuestionID] int  NOT NULL  
	, [MatchingAnswerID] int  NULL  
	, [AnswerRef] nvarchar(100)  NOT NULL
	, [MatchingAnswerRef] nvarchar(100)  NULL
	, [Score] numeric(9,3)  NOT NULL  
	, [sortOrder] int  NULL
	, [title_defaultTranslation] nvarchar (200) NULL
	)

ALTER TABLE [dbo].[QuizAnswer] ADD CONSTRAINT [QuizAnswer_PK] PRIMARY KEY CLUSTERED (
[AnswerID]
)
END
GO

/* ALTER existing QuizAnswer table to add AnswerRef column for environments where P-REL109 Phase 1 was released prior to Bulk Upload CR */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizAnswer') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizAnswer' AND column_name='AnswerRef') 
ALTER TABLE QuizAnswer add AnswerRef nvarchar(100) NULL
GO
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizAnswer') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizAnswer' AND column_name='MatchingAnswerRef') 
ALTER TABLE QuizAnswer add MatchingAnswerRef nvarchar(100) NULL
GO