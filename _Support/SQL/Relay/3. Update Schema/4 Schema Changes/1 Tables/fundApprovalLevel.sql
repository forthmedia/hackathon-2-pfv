
if exists(select 1 from information_schema.columns where table_name='fundApprovalLevel' and column_name='approvalLevelPhraseTextID')
begin
exec sp_RENAME 'fundApprovalLevel.approvalLevelPhraseTextID' , 'approvalLevelPhrase', 'COLUMN';
end
GO