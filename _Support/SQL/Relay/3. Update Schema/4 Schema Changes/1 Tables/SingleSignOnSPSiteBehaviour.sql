
if not exists(select 1 from information_schema.tables where table_name='SingleSignOnSPSiteBehaviour')
begin
	CREATE TABLE [dbo].[SingleSignOnSPSiteBehaviour](
		[singleSignOnSPSiteBehaviourID] [int] IDENTITY(1,1) NOT NULL,
		[siteDefID] [int] FOREIGN KEY REFERENCES SiteDef(siteDefID) on DELETE CASCADE,
		[defaultAllowRegularLogin] bit,
		[defaultAllowSingleSignOn] bit,
		[autostartSingleSignOn] bit
	 CONSTRAINT [PK_SingleSignOnSPSiteBehaviour] PRIMARY KEY CLUSTERED 
	(
		[SingleSignOnSPSiteBehaviourID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE INDEX index_siteDefID ON SingleSignOnSPSiteBehaviour (siteDefID)

	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
	 VALUES (553, 'singleSignOnSPSiteBehaviour', 'singleSignOnSPSiteBehaviourID', 0, 'SSOSB', 0);

	exec generate_MRAudit @TableName = 'singleSignOnSPSiteBehaviour', @mnemonic = 'SSOSB'

end
GO
