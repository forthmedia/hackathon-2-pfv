IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload')
CREATE TABLE [dbo].[QuizQuestionPoolBulkUpload](
	[QuestionPoolID] [int] NULL,
	[CountryDescription] [nvarchar](50) NULL,
	[CountryID] [int] NULL,
	[LanguageName] [nvarchar](20) NULL,
	[LanguageID] [int] NULL,
	[QuestionRef] [nvarchar](100) NULL,
	[QuestionID] [int] NULL,
	[QuestionTypeDescription] [nvarchar](100) NULL,
	[QuestionTypeTextID] [varchar](32) NULL,
	[QuestionTypeID] [int] NULL,
	[QuestionText] [nvarchar](4000) NULL,
	[AnswerRef] [nvarchar](100) NULL,
	[AnswerID] [int] NULL,
	[AnswerText] [nvarchar](4000) NULL,
	[AnswerScore] [numeric](9, 3) NULL,
	[SortOrder] [int] NULL,
	[MatchingAnswerRef] [nvarchar](100) NULL,
	[MatchingAnswerID] [int] NULL,
	[created] [datetime] NULL,
	[createdBy] [int] NULL,
	[lastUpdated] [datetime] NULL,
	[lastUpdatedBy] [int] NULL
)

GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload' AND column_name='MatchingAnswerRef') 
ALTER TABLE QuizQuestionPoolBulkUpload add MatchingAnswerRef nvarchar(100) NULL
GO
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload' AND column_name='MatchingAnswerID') 
ALTER TABLE QuizQuestionPoolBulkUpload add MatchingAnswerID int NULL
GO
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload') AND
NOT EXISTS (select 1 from information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload' AND column_name='QuestionSortOrder')
ALTER TABLE QuizQuestionPoolBulkUpload add QuestionSortOrder int NULL
GO
/* Add new column AnswerSortOrder rather than renaming existing SortOrder column, as we may be able to preserve backward compatiblity for customers using previous version of csv template */
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload') AND
NOT EXISTS (select 1 from information_schema.columns WHERE table_name= 'QuizQuestionPoolBulkUpload' AND column_name='AnswerSortOrder')
ALTER TABLE QuizQuestionPoolBulkUpload add AnswerSortOrder int NULL
GO