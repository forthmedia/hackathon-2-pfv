/****** Ticket number 429052 Spring 2012 ******/
/****** Object:  Table [dbo].[trngModuleCourse]    Script Date: 07/10/2012 10:27:25 Phil Porter ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[trngModuleCourse]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[trngModuleCourse](
	[trngModuleCourseID] [int] IDENTITY(1,1) NOT NULL,
	[moduleID] [int] NOT NULL,
	[courseID] [int] NOT NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastUpdatedBy] [int] NULL,
	[lastUpdatedByPerson] [int] NULL,
 CONSTRAINT [PK_trngModuleCourse] PRIMARY KEY CLUSTERED 
(
	[trngModuleCourseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[trngModuleCourse]  WITH CHECK ADD  CONSTRAINT [FK_trngModuleCourse_ModuleID] FOREIGN KEY([moduleID])
REFERENCES [dbo].[trngModule] ([moduleID])

ALTER TABLE [dbo].[trngModuleCourse] CHECK CONSTRAINT [FK_trngModuleCourse_ModuleID]
End
GO