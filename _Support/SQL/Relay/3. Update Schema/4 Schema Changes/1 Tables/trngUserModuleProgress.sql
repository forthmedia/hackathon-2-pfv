IF NOT EXISTS (select * from sysindexes where id=object_id('trngUserModuleProgress') and name='Ix_moduleStatus')
CREATE NONCLUSTERED INDEX [Ix_moduleStatus]
ON [dbo].[trngUserModuleProgress] ([moduleStatus])
INCLUDE ([personID],[moduleID])
GO


update modEntityDef set ApiActive=1 where tableName='Trngusermoduleprogress'
update modEntityDef set APIName=FieldName where tableName='Trngusermoduleprogress' and APIName is null


--For PROD2016-1194 RJT as far as I know the column countryID is always null on Trngusermoduleprogress. It however triggers autocountry scoping which messes up the API (no one has rights for country null)

IF (COL_LENGTH('trngUserModuleProgress','countryID') is not NULL)
begin
	--Dynamic SQL as the column countryID may not exist
	DECLARE @sqlCommand varchar(4000)
	set @sqlCommand='
	if not exists (select 1 from trngUserModuleProgress where countryID is not null)
		begin
			ALTER TABLE trngUserModuleProgress
			DROP COLUMN countryID
			
			delete from ModEntityDef where TableName=''trngUserModuleProgress'' and FieldName=''countryID''
			exec generate_MRAudit @TableName = ''trngUserModuleProgress'', @mnemonic = ''TUMP''
			
		end
	else
		begin
			RAISERROR(''Column countryID in trngUserModuleProgress had data so was not dropped. This was believed to always be null and unused'',16, 1)
		end
	'
	EXEC (@sqlCommand)
	
	
	
end