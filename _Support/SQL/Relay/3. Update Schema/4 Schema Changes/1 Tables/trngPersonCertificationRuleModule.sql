
if not exists(select 1 from sysobjects where name='trngPersonCertificationRuleModule' and type='u')
CREATE TABLE [dbo].[trngPersonCertificationRuleModule](
	[personCertificationRuleModuleID] [int] IDENTITY(1,1) NOT NULL,
	[personCertificationRuleID] [int] NOT NULL,
	[moduleID] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_trngPersonCertificationRuleModule] PRIMARY KEY CLUSTERED 
(
	[personCertificationRuleModuleID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[FK_trngPersonCertificationRuleModule_trngModule]'))
ALTER TABLE [dbo].[trngPersonCertificationRuleModule]  WITH CHECK ADD  CONSTRAINT [FK_trngPersonCertificationRuleModule_trngModule] FOREIGN KEY([moduleID])
REFERENCES [dbo].[trngModule] ([moduleID])
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[FK_trngPersonCertificationRuleModule_trngPersonCertificationRule]'))
ALTER TABLE [dbo].[trngPersonCertificationRuleModule]  WITH CHECK ADD  CONSTRAINT [FK_trngPersonCertificationRuleModule_trngPersonCertificationRule] FOREIGN KEY([personCertificationRuleID])
REFERENCES [dbo].[trngPersonCertificationRule] ([personCertificationRuleID])
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[DF_trngPersonCertificationRuleModule_created]'))
ALTER TABLE [dbo].[trngPersonCertificationRuleModule] ADD  CONSTRAINT [DF_trngPersonCertificationRuleModule_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[DF_trngPersonCertificationRuleModule_lastUpdated]'))
ALTER TABLE [dbo].[trngPersonCertificationRuleModule] ADD  CONSTRAINT [DF_trngPersonCertificationRuleModule_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO