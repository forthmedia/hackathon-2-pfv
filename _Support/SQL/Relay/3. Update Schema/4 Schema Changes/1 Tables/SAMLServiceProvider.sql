

IF object_id('SAMLServiceProvider') is null  
BEGIN
	Create Table dbo.SAMLServiceProvider(
		ServiceProviderId int identity(1,1)  NOT NULL,
		ServiceProviderName nVarChar(256)  NOT NULL,
		ServiceProviderIdentifier nVarChar(256) NOT NULL, 
		assertionConsumerURL nVarChar(512)  NOT NULL,
		nameIDField nVarChar(256) NOT NULL,
		personAttributes nvarchar(max),
		locationAttributes nvarchar(max),
		organisationAttributes nvarchar(max),
		createdByPerson int,
		createdby int,
		created date,
		LastUpdatedBy int,
		lastUpdatedByPerson int,
		lastUpdated date,
		certificateToUse nVarChar(256),
		certificateWarningSent bit NOT NULL
		 CONSTRAINT ServiceProviderId PRIMARY KEY CLUSTERED 
		(
			ServiceProviderId ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]);
		

	
	
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
		 VALUES (538, 'SAMLServiceProvider', 'ServiceProviderId', 0, 'SASP', 0);
	
	CREATE INDEX ServiceProviderIdentifierIndex
	ON SAMLServiceProvider (ServiceProviderIdentifier);	 
	
	exec generate_MRAudit @TableName = 'SAMLServiceProvider', @mnemonic = 'SASP'
	
END