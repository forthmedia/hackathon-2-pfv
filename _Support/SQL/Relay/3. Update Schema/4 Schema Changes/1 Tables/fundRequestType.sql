if not exists (select * from information_schema.columns where table_name='fundRequestType' and column_name='startdate')
BEGIN
alter table fundRequestType Add startdate datetime null
END

GO

if not exists (select * from information_schema.columns where table_name='fundRequestType' and column_name='enddate')
BEGIN
alter table fundRequestType Add enddate datetime null
END

alter table fundRequestType alter column fundRequestType varchar(50) null

/* NJH 2013/03/06 case 433562 - rename columns so that they follow standard naming convention 
	WAB 2014-10-13 some changes to get to run nicely during upgrades (see fundApprovalStatus.sql)
*/
if exists(select 1 from information_schema.columns where table_name='fundRequestType' and column_name='updatedBy')
and exists (select 1 from information_schema.columns where table_name='fundRequestType' and column_name='lastupdatedBy')
begin
	declare @sql nvarchar(max) = 'update fundRequestType set lastUpdatedBy = updatedBy where lastUpdatedBy is null'
	exec (@sql)
	exec sp_dropcolumn 'fundRequestType','updatedBy'
end
else 
if exists(select 1 from information_schema.columns where table_name='fundRequestType' and column_name='updatedBy')
begin
exec sp_RENAME 'fundRequestType.updatedBy' , 'lastUpdatedBy', 'COLUMN';
end
GO

if exists(select 1 from information_schema.columns where table_name='fundRequestType' and column_name='updated')
and exists (select 1 from information_schema.columns where table_name='fundRequestType' and column_name='lastupdated')
begin
	declare @sql nvarchar(max) = 'update fundRequestType set lastUpdated = updated where lastUpdated is null'
	exec (@sql)	
	exec sp_dropcolumn 'fundRequestType','updated'
end
else
if exists(select 1 from information_schema.columns where table_name='fundRequestType' and column_name='updated')
begin
exec sp_RENAME 'fundRequestType.updated' , 'lastUpdated', 'COLUMN';
end
GO