
if not exists (select 1 from information_schema.columns where table_name = 'recordRights' and column_name = 'lastUpdated')
alter table recordRights add lastUpdated datetime default getDate()

if not exists (select 1 from information_schema.columns where table_name = 'recordRights' and column_name = 'lastUpdatedBy')
alter table recordRights add lastUpdatedBy int

if not exists (select 1 from information_schema.columns where table_name = 'recordRights' and column_name = 'lastUpdatedByPerson')
alter table recordRights add lastUpdatedByPerson int

/* WAB 2015-12-01 PROD2015-290 Visibility Project. 
Add entitityTypeID column for better indexing
*/
if not exists (select 1 from information_schema.columns where table_name = 'recordRights' and column_name = 'entityTypeID')
BEGIN
	alter table recordRights
	add entityTypeID int

	create index recordRights_entityTypeID_recordID
	on recordRights (entityTypeID,recordID)
	include (usergroupid,permission)

	/*	WAB 2016-03-08 Problems with old version of recordRights_Updatehasrecordrights not being compatible with new hasRecordRightsBitMask columns
		So we will drop it here knowing that it will be recreated later
	*/
	IF EXISTS (select 1 from sysobjects where name = 'recordRights_Updatehasrecordrights')
	BEGIN
		declare @sql nvarchar(max) = 
			'drop trigger recordRights_Updatehasrecordrights'
		exec (@sql)	
	END
	

END

/* add a constraint. recordRights.userGroupID  to usergroup.usergroupid */
IF NOT EXISTS (select * from information_schema.table_constraints where constraint_name='FK_RecordRights_UsergroupID')
BEGIN
	/* get rid of any items in recordrights where usergroupid does not exist */ 
	delete from recordrights where usergroupid not in (select usergroupid from usergroup)

	ALTER TABLE [dbo].[RecordRights]  WITH CHECK ADD  CONSTRAINT [FK_RecordRights_UsergroupID] FOREIGN KEY([UsergroupID])
	REFERENCES [dbo].[Usergroup] ([UsergroupID])
	ON UPDATE CASCADE
	ON DELETE CASCADE

END






