
if not exists(select 1 from sysobjects where name='ServiceToken' and type='u')
CREATE TABLE [dbo].[ServiceToken](
	[serviceTokenID] [int] IDENTITY(1,1) NOT NULL,
	[serviceID] [int] NOT NULL,
	[personID] [int] NOT NULL,
	[accessKey] [varchar](500) NOT NULL,
	[accessSecret] [varchar](250) NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_ServiceToken] PRIMARY KEY CLUSTERED 
(
	[serviceTokenID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ServiceToken_created]'))
ALTER TABLE [dbo].[ServiceToken] ADD  CONSTRAINT [DF_ServiceToken_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ServiceToken_lastUpdated]'))
ALTER TABLE [dbo].[ServiceToken] ADD  CONSTRAINT [DF_ServiceToken_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO


if exists(select 1 from information_schema.columns where table_name='serviceToken' and column_name='personID')
begin
exec sp_RENAME 'serviceToken.personID' , 'entityID', 'COLUMN';
end
GO

if not exists (select 1 from information_schema.columns where table_name='serviceToken' and column_name='entityTypeID')
alter table serviceToken add entityTypeID int default 0 not null

GO

/* 2013-11-19 Case 437503 PPB */
if exists (select 1 from information_schema.columns where table_name='serviceToken' and column_name='accessKey')
alter table serviceToken alter column accessKey varchar(1000)
GO

