IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizQuestionPool]') AND type in (N'U'))
   BEGIN
	CREATE TABLE [dbo].[QuizQuestionPool] (
	[QuestionPoolID] int IDENTITY(1,1) NOT NULL  
	, [QuestionPoolRef] nvarchar(100)  NULL  
	, [Created] datetime NULL
	, [CreatedBy] int NULL  
	, [LastUpdated] datetime NULL  
	, [LastUpdatedBy] int NULL 
	)


ALTER TABLE [dbo].[QuizQuestionPool] ADD CONSTRAINT [QuizQuestionPool_PK] PRIMARY KEY CLUSTERED (
[QuestionPoolID]
)
END
GO