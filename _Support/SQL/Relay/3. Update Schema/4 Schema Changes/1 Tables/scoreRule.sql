if not exists(select 1 from information_schema.tables where table_name='scoreRule')
	begin
		CREATE TABLE [dbo].[scoreRule](
			[scoreRuleID] [int] IDENTITY(1,1) NOT NULL,
			[scoreRuleGroupingID] [int], --not foreign key due to build order, add in contstrains
			[name_defaultTranslation] [nVarChar](4000), --name is a translation
			[created] [datetime] NOT NULL DEFAULT getdate(),
			[createdBy] [int] NOT NULL,
			[lastUpdated] [datetime] NOT NULL  DEFAULT getdate(),
			[lastUpdatedBy] [int] NOT NULL,
			[lastUpdatedByPerson] [int] NOT NULL,
		 CONSTRAINT [PK_scoreRule] PRIMARY KEY CLUSTERED 
		(
			[scoreRuleID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
			 VALUES (546, 'scoreRule', 'scoreRuleID', 0, 'SCRL', 0);
		
	end
	GO

	
IF not EXISTS (SELECT 1  FROM sys.indexes  WHERE name='scoreRule_scoreRuleGroupingID_index' AND object_id = OBJECT_ID('[dbo].[scoreRule]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX scoreRule_scoreRuleGroupingID_index
	ON dbo.scoreRule (scoreRuleGroupingID)
End
		