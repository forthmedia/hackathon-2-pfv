
if not exists (select 1 from sysobjects where name='textMultipleFlagData' and type='u')
CREATE TABLE [dbo].[textMultipleFlagData](
	[rowID] [int] IDENTITY(1,1) NOT NULL,
	[flagID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[data] [nvarchar](100) NOT NULL,
	[sortOrder] [int] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_textMultipleFlagData] PRIMARY KEY CLUSTERED 
(
	[flagID] ASC,
	[entityID] ASC,
	[data] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_textMultipleFlagData_sortOrder]') AND type = 'D')
ALTER TABLE [dbo].[textMultipleFlagData] ADD  CONSTRAINT [DF_textMultipleFlagData_sortOrder]  DEFAULT ((0)) FOR [sortOrder]
GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_textMultipleFlagData_created]') AND type = 'D')
ALTER TABLE [dbo].[textMultipleFlagData] ADD  CONSTRAINT [DF_textMultipleFlagData_created]  DEFAULT (getdate()) FOR [created]
GO

IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_textMultipleFlagData_lastUpdated]') AND type = 'D')
ALTER TABLE [dbo].[textMultipleFlagData] ADD  CONSTRAINT [DF_textMultipleFlagData_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO
