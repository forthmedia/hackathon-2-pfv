IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'SODMaster' AND column_name='R1DistiID') 
	alter table SODMaster add R1DistiID VARCHAR(50)
GO


if not exists (select 1 from information_schema.columns where table_name='SODMaster' and column_name='createdByPerson')
begin
	alter table SODMaster add createdByPerson int null
end

if not exists (select 1 from information_schema.columns where table_name='SODMaster' and column_name='lastUpdatedByPerson')
begin
	alter table SODMaster add lastUpdatedByPerson int null
end

if not exists (select 1 from information_schema.columns where table_name='SODMaster' and column_name='sodMasterID')
begin
	--drop foreign keys referencing old PK
	ALTER TABLE SODFailure drop constraint FK_SODFailure_SODMaster
	ALTER TABLE SODClaim drop constraint FK_SODclaim_SODMaster
	--drop old PK
	ALTER TABLE SODMaster drop constraint PK__SODMaster__3CFFC3CD
	--create new PK
	ALTER TABLE SODMaster ADD sodMasterID INT PRIMARY KEY IDENTITY(1,1)
	update schemaTableBase set uniquekey='sodMasterID' where entityName='SODMaster'
	
	--create a uniqueness constraint equivalent to the old pk
	ALTER TABLE SODMaster ADD CONSTRAINT unique_SODMaster_sourceID_batchRowID_created UNIQUE (sourceID, batchRowID, created)
	--put the FKs back (now referencing the uniqueness constraint but otherwise identical
	ALTER TABLE SODFailure ADD CONSTRAINT FK_SODFailure_SODMaster FOREIGN KEY (sourceID, batchRowID, batchDate) REFERENCES SODMaster(sourceID, batchRowID, created)
	ALTER TABLE SODClaim ADD CONSTRAINT FK_SODclaim_SODMaster FOREIGN KEY (sourceID, batchRowID, batchDate) REFERENCES SODMaster(sourceID, batchRowID, created)
	
end

exec generate_MRAudit @TableName = 'SODMaster'

update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'batchRowID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'country' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'countryID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'createdBy' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'createdByPerson' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'EndUserCompanyName' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'foreignTransactionID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'invoiceDate' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'invoiceNumber' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'lastUpdatedBy' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'lastUpdatedByPerson' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'locationID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'noOfUsers' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'orderType' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'orderValue' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'points' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'productCode' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'productID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'quantity' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'R1DistiID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'ResellerName' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'RWCompanyAccountID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'RWPersonAccountID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'rwTransactionItemsID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'sodMasterID' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'ThirdPartyForeignKey' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'VATNumber' and ApiActive=0
update ModEntityDef set ApiActive=1 where TableName ='SODMaster' and FieldName = 'sourceID' and ApiActive=0


update ModEntityDef set ApiName='batchRowID' where TableName ='SODMaster' and FieldName = 'batchRowID' and ApiName is null
update ModEntityDef set ApiName='country' where TableName ='SODMaster' and FieldName = 'country' and ApiName is null
update ModEntityDef set ApiName='countryID' where TableName ='SODMaster' and FieldName = 'countryID' and ApiName is null
update ModEntityDef set ApiName='createdBy' where TableName ='SODMaster' and FieldName = 'createdBy' and ApiName is null
update ModEntityDef set ApiName='createdByPerson' where TableName ='SODMaster' and FieldName = 'createdByPerson' and ApiName is null
update ModEntityDef set ApiName='EndUserCompanyName' where TableName ='SODMaster' and FieldName = 'EndUserCompanyName' and ApiName is null
update ModEntityDef set ApiName='foreignTransactionID' where TableName ='SODMaster' and FieldName = 'foreignTransactionID' and ApiName is null
update ModEntityDef set ApiName='invoiceDate' where TableName ='SODMaster' and FieldName = 'invoiceDate' and ApiName is null
update ModEntityDef set ApiName='invoiceNumber' where TableName ='SODMaster' and FieldName = 'invoiceNumber' and ApiName is null
update ModEntityDef set ApiName='lastUpdatedBy' where TableName ='SODMaster' and FieldName = 'lastUpdatedBy' and ApiName is null
update ModEntityDef set ApiName='lastUpdatedByPerson' where TableName ='SODMaster' and FieldName = 'lastUpdatedByPerson' and ApiName is null
update ModEntityDef set ApiName='locationID' where TableName ='SODMaster' and FieldName = 'locationID' and ApiName is null
update ModEntityDef set ApiName='noOfUsers' where TableName ='SODMaster' and FieldName = 'noOfUsers' and ApiName is null
update ModEntityDef set ApiName='orderType' where TableName ='SODMaster' and FieldName = 'orderType' and ApiName is null
update ModEntityDef set ApiName='orderValue' where TableName ='SODMaster' and FieldName = 'orderValue' and ApiName is null
update ModEntityDef set ApiName='points' where TableName ='SODMaster' and FieldName = 'points' and ApiName is null
update ModEntityDef set ApiName='productCode' where TableName ='SODMaster' and FieldName = 'productCode' and ApiName is null
update ModEntityDef set ApiName='productID' where TableName ='SODMaster' and FieldName = 'productID' and ApiName is null
update ModEntityDef set ApiName='quantity' where TableName ='SODMaster' and FieldName = 'quantity' and ApiName is null
update ModEntityDef set ApiName='R1DistiID' where TableName ='SODMaster' and FieldName = 'R1DistiID' and ApiName is null
update ModEntityDef set ApiName='ResellerName' where TableName ='SODMaster' and FieldName = 'ResellerName' and ApiName is null
update ModEntityDef set ApiName='RWCompanyAccountID' where TableName ='SODMaster' and FieldName = 'RWCompanyAccountID' and ApiName is null
update ModEntityDef set ApiName='RWPersonAccountID' where TableName ='SODMaster' and FieldName = 'RWPersonAccountID' and ApiName is null
update ModEntityDef set ApiName='rwTransactionItemsID' where TableName ='SODMaster' and FieldName = 'rwTransactionItemsID' and ApiName is null
update ModEntityDef set ApiName='sodMasterID' where TableName ='SODMaster' and FieldName = 'sodMasterID' and ApiName is null
update ModEntityDef set ApiName='ThirdPartyForeignKey' where TableName ='SODMaster' and FieldName = 'ThirdPartyForeignKey' and ApiName is null
update ModEntityDef set ApiName='VATNumber' where TableName ='SODMaster' and FieldName = 'VATNumber' and ApiName is null
update ModEntityDef set ApiName='sourceID' where TableName ='SODMaster' and FieldName = 'sourceID' and ApiName is null



/* 2012/07/23 IH commented out because it looks like OrganisationID is not in core. (put into Lenovo specific file instead) */

--IF NOT EXISTS (select * from sysindexes where id=object_id('SODMaster') and name='idx_countryID')
--CREATE NONCLUSTERED INDEX [idx_countryID]
--ON [dbo].[SODMaster] ([countryID])
--INCLUDE ([sourceID],[invoiceNumber],[invoiceDate],[productCode],[OrganisationID],[invoiceLineNumber])
--GO



