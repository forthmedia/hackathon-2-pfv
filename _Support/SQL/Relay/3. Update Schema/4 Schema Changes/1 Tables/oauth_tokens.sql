IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[oauth_tokens]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[oauth_tokens](
		[tkey] [nvarchar](50) NOT NULL,
		[consumer_ID] [int] NOT NULL,
		[type] [varchar](10) NULL,
		[tsecret] [nvarchar](50) NULL,
		[nonce] [nvarchar](50) NULL,
		[time_stamp] [datetime] NULL,
		[callbackURL] [nvarchar](max) NULL,
		[PersonID] [int] NULL,
	 CONSTRAINT [PK_oauth_tokens] PRIMARY KEY CLUSTERED 
	(
		[tkey] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[oauth_tokens]  WITH CHECK ADD  CONSTRAINT [FK_oautc_oautt] FOREIGN KEY([consumer_ID])
	REFERENCES [dbo].[oauth_consumers] ([consumer_ID])
	ON UPDATE CASCADE
	ON DELETE CASCADE

	ALTER TABLE [dbo].[oauth_tokens] CHECK CONSTRAINT [FK_oautc_oautt]

END

/* WAB 2015-02-24 CASE 443944 Add this index to speed up response on Pembridge system, will also add housekeeping task to clear out old tokens */
IF  NOT EXISTS (SELECT * FROM sysindexes WHERE name = N'oauth_tokens_nonce')
BEGIN
	CREATE NONCLUSTERED INDEX [oauth_tokens_nonce]
	ON [dbo].[oauth_tokens] ([nonce])
END