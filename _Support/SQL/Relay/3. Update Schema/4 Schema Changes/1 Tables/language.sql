IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_Name = 'aaaaaLanguage_PK')
Begin
	--swap language and languageID as the primary keys
	alter table Language DROP CONSTRAINT aaaaaLanguage_PK	
	ALTER TABLE Language ALTER COLUMN LanguageID INTEGER NOT NULL
	alter table Language add constraint Language_PK_LanguageID primary key (LanguageID)
	alter table Language add constraint Language_unique_Language UNIQUE (Language)
	update schemaTableBase set uniquekey='LanguageID' where EntityName='Language'
	
End