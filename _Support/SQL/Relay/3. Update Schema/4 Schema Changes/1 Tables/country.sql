/*** START: move LocatorCountryDef contents to country table ***/
	GO
	if not exists (select * from information_schema.columns where table_name='country' and column_name='Latitude')
		alter table country add Latitude float NOT NULL default(0)
	GO
	if not exists (select * from information_schema.columns where table_name='country' and column_name='Longitude')
		alter table country add Longitude float NOT NULL default(0)
	GO
	if not exists (select * from information_schema.columns where table_name='country' and column_name='Zoom')
		alter table country add Zoom int NOT NULL default(0)
	GO
	/*** START: populate new columns ***/
		if exists (select * from country where isocode='BE')
		 begin
		  if exists (select * from country where isocode='BE' and latitude=0 and zoom=0)
		   update country set latitude=50.5039,longitude=4.46994,zoom=7 where isocode='BE'
		 end
		
		if exists (select * from country where isocode='BG')
		 begin
		  if exists (select * from country where isocode='BG' and latitude=0 and zoom=0)
		   update country set latitude=42.7339,longitude=25.4858,zoom=6 where isocode='BG'
		 end
		
		if exists (select * from country where isocode='CH')
		 begin
		  if exists (select * from country where isocode='CH' and latitude=0 and zoom=0)
		   update country set latitude=46.8182,longitude=8.22751,zoom=7 where isocode='CH'
		 end
		
		if exists (select * from country where isocode='DE')
		 begin
		  if exists (select * from country where isocode='DE' and latitude=0 and zoom=0)
		   update country set latitude=51.1657,longitude=10.4515,zoom=5 where isocode='DE'
		 end
		
		if exists (select * from country where isocode='DK')
		 begin
		  if exists (select * from country where isocode='DK' and latitude=0 and zoom=0)
		   update country set latitude=56.2639,longitude=9.50178,zoom=6 where isocode='DK'
		 end
		
		if exists (select * from country where isocode='ES')
		 begin
		  if exists (select * from country where isocode='ES' and latitude=0 and zoom=0)
		   update country set latitude=40.4637,longitude=-3.74922,zoom=5 where isocode='ES'
		 end
		
		if exists (select * from country where isocode='FI')
		 begin
		  if exists (select * from country where isocode='FI' and latitude=0 and zoom=0)
		   update country set latitude=65.2762,longitude=26.4173,zoom=4 where isocode='FI'
		 end
		
		if exists (select * from country where isocode='FR')
		 begin
		  if exists (select * from country where isocode='FR' and latitude=0 and zoom=0)
		   update country set latitude=46.2276,longitude=2.21375,zoom=5 where isocode='FR'
		 end
		
		if exists (select * from country where isocode='GB')
		 begin
		  if exists (select * from country where isocode='GB' and latitude=0 and zoom=0)
		   update country set latitude=55,longitude=-3.43597,zoom=5 where isocode='GB'
		 end
		
		if exists (select * from country where isocode='IE')
		 begin
		  if exists (select * from country where isocode='IE' and latitude=0 and zoom=0)
		   update country set latitude=53.4129,longitude=-8.24389,zoom=6 where isocode='IE'
		 end
		
		if exists (select * from country where isocode='IT')
		 begin
		  if exists (select * from country where isocode='IT' and latitude=0 and zoom=0)
		   update country set latitude=41.8719,longitude=12.5674,zoom=5 where isocode='IT'
		 end
		
		if exists (select * from country where isocode='LU')
		 begin
		  if exists (select * from country where isocode='LU' and latitude=0 and zoom=0)
		   update country set latitude=49.8153,longitude=6.12958,zoom=8 where isocode='LU'
		 end
		
		if exists (select * from country where isocode='NL')
		 begin
		  if exists (select * from country where isocode='NL' and latitude=0 and zoom=0)
		   update country set latitude=52.1326,longitude=5.29127,zoom=6 where isocode='NL'
		 end
		
		if exists (select * from country where isocode='NO')
		 begin
		  if exists (select * from country where isocode='NO' and latitude=0 and zoom=0)
		   update country set latitude=65.0012,longitude=13.5853,zoom=4 where isocode='NO'
		 end
		
		if exists (select * from country where isocode='PT')
		 begin
		  if exists (select * from country where isocode='PT' and latitude=0 and zoom=0)
		   update country set latitude=39.3999,longitude=-8.22445,zoom=6 where isocode='PT'
		 end
		
		if exists (select * from country where isocode='SE')
		 begin
		  if exists (select * from country where isocode='SE' and latitude=0 and zoom=0)
		   update country set latitude=63.4517,longitude=16.3978,zoom=4 where isocode='SE'
		 end
		
		if exists (select * from country where isocode='US')
		 begin
		  if exists (select * from country where isocode='US' and latitude=0 and zoom=0)
		   update country set latitude=37.0902,longitude=-95.7129,zoom=2 where isocode='US'
		 end
		
		if exists (select * from country where isocode='AT')
		 begin
		  if exists (select * from country where isocode='AT' and latitude=0 and zoom=0)
		   update country set latitude=47.5162,longitude=14.5501,zoom=6 where isocode='AT'
		 end
		
		if exists (select * from country where isocode='CZ')
		 begin
		  if exists (select * from country where isocode='CZ' and latitude=0 and zoom=0)
		   update country set latitude=49.8175,longitude=15.473,zoom=6 where isocode='CZ'
		 end
		
		if exists (select * from country where isocode='GR')
		 begin
		  if exists (select * from country where isocode='GR' and latitude=0 and zoom=0)
		   update country set latitude=39.0742,longitude=21.8243,zoom=6 where isocode='GR'
		 end
		
		if exists (select * from country where isocode='HR')
		 begin
		  if exists (select * from country where isocode='HR' and latitude=0 and zoom=0)
		   update country set latitude=45.1,longitude=15.2,zoom=6 where isocode='HR'
		 end
		
		if exists (select * from country where isocode='HU')
		 begin
		  if exists (select * from country where isocode='HU' and latitude=0 and zoom=0)
		   update country set latitude=47.1625,longitude=19.5033,zoom=6 where isocode='HU'
		 end
		
		if exists (select * from country where isocode='IL')
		 begin
		  if exists (select * from country where isocode='IL' and latitude=0 and zoom=0)
		   update country set latitude=31.0461,longitude=34.8516,zoom=6 where isocode='IL'
		 end
		
		if exists (select * from country where isocode='IS')
		 begin
		  if exists (select * from country where isocode='IS' and latitude=0 and zoom=0)
		   update country set latitude=64.9631,longitude=-19.0208,zoom=5 where isocode='IS'
		 end
		
		if exists (select * from country where isocode='PL')
		 begin
		  if exists (select * from country where isocode='PL' and latitude=0 and zoom=0)
		   update country set latitude=51.9194,longitude=19.1451,zoom=5 where isocode='PL'
		 end
		
		if exists (select * from country where isocode='RO')
		 begin
		  if exists (select * from country where isocode='RO' and latitude=0 and zoom=0)
		   update country set latitude=45.9432,longitude=24.9668,zoom=6 where isocode='RO'
		 end
		
		if exists (select * from country where isocode='RU')
		 begin
		  if exists (select * from country where isocode='RU' and latitude=0 and zoom=0)
		   update country set latitude=61.524,longitude=105.319,zoom=2 where isocode='RU'
		 end
		
		if exists (select * from country where isocode='SI')
		 begin
		  if exists (select * from country where isocode='SI' and latitude=0 and zoom=0)
		   update country set latitude=46.1512,longitude=14.9955,zoom=7 where isocode='SI'
		 end
		
		if exists (select * from country where isocode='SK')
		 begin
		  if exists (select * from country where isocode='SK' and latitude=0 and zoom=0)
		   update country set latitude=48.669,longitude=19.699,zoom=6 where isocode='SK'
		 end
		
		if exists (select * from country where isocode='TR')
		 begin
		  if exists (select * from country where isocode='TR' and latitude=0 and zoom=0)
		   update country set latitude=38.9637,longitude=35.2433,zoom=5 where isocode='TR'
		 end
		
		if exists (select * from country where isocode='ZA')
		 begin
		  if exists (select * from country where isocode='ZA' and latitude=0 and zoom=0)
		   update country set latitude=-30.5595,longitude=22.9375,zoom=5 where isocode='ZA'
		 end
		
		if exists (select * from country where isocode='LV')
		 begin
		  if exists (select * from country where isocode='LV' and latitude=0 and zoom=0)
		   update country set latitude=56.8796,longitude=24.6032,zoom=6 where isocode='LV'
		 end
		
		if exists (select * from country where isocode='NG')
		 begin
		  if exists (select * from country where isocode='NG' and latitude=0 and zoom=0)
		   update country set latitude=9.082,longitude=8.67528,zoom=5 where isocode='NG'
		 end
		
		if exists (select * from country where isocode='MT')
		 begin
		  if exists (select * from country where isocode='MT' and latitude=0 and zoom=0)
		   update country set latitude=35.9375,longitude=14.3754,zoom=10 where isocode='MT'
		 end
		
		if exists (select * from country where isocode='CY')
		 begin
		  if exists (select * from country where isocode='CY' and latitude=0 and zoom=0)
		   update country set latitude=35.1264,longitude=33.4299,zoom=8 where isocode='CY'
		 end
		
		if exists (select * from country where isocode='AE')
		 begin
		  if exists (select * from country where isocode='AE' and latitude=0 and zoom=0)
		   update country set latitude=23.4241,longitude=53.8478,zoom=6 where isocode='AE'
		 end
		
		if exists (select * from country where isocode='BH')
		 begin
		  if exists (select * from country where isocode='BH' and latitude=0 and zoom=0)
		   update country set latitude=25.9304,longitude=50.6378,zoom=9 where isocode='BH'
		 end
		
		if exists (select * from country where isocode='EG')
		 begin
		  if exists (select * from country where isocode='EG' and latitude=0 and zoom=0)
		   update country set latitude=26.8206,longitude=30.8025,zoom=5 where isocode='EG'
		 end
		
		if exists (select * from country where isocode='JO')
		 begin
		  if exists (select * from country where isocode='JO' and latitude=0 and zoom=0)
		   update country set latitude=30.5852,longitude=36.2384,zoom=6 where isocode='JO'
		 end
		
		if exists (select * from country where isocode='KE')
		 begin
		  if exists (select * from country where isocode='KE' and latitude=0 and zoom=0)
		   update country set latitude=-0.023559,longitude=37.9062,zoom=5 where isocode='KE'
		 end
		
		if exists (select * from country where isocode='KW')
		 begin
		  if exists (select * from country where isocode='KW' and latitude=0 and zoom=0)
		   update country set latitude=29.3117,longitude=47.4818,zoom=8 where isocode='KW'
		 end
		
		if exists (select * from country where isocode='OM')
		 begin
		  if exists (select * from country where isocode='OM' and latitude=0 and zoom=0)
		   update country set latitude=21.5126,longitude=55.9233,zoom=5 where isocode='OM'
		 end
		
		if exists (select * from country where isocode='QA')
		 begin
		  if exists (select * from country where isocode='QA' and latitude=0 and zoom=0)
		   update country set latitude=25.3548,longitude=51.1839,zoom=8 where isocode='QA'
		 end
		
		if exists (select * from country where isocode='SA')
		 begin
		  if exists (select * from country where isocode='SA' and latitude=0 and zoom=0)
		   update country set latitude=23.8859,longitude=45.0792,zoom=6 where isocode='SA'
		 end
		
		if exists (select * from country where isocode='TN')
		 begin
		  if exists (select * from country where isocode='TN' and latitude=0 and zoom=0)
		   update country set latitude=33.8869,longitude=9.5375,zoom=6 where isocode='TN'
		 end
		
		if exists (select * from country where isocode='ZW')
		 begin
		  if exists (select * from country where isocode='ZW' and latitude=0 and zoom=0)
		   update country set latitude=-19.0154,longitude=29.1549,zoom=6 where isocode='ZW'
		 end
		
		if exists (select * from country where isocode='LB')
		 begin
		  if exists (select * from country where isocode='LB' and latitude=0 and zoom=0)
		   update country set latitude=33.8547,longitude=35.8623,zoom=8 where isocode='LB'
		 end
		
		if exists (select * from country where isocode='MA')
		 begin
		  if exists (select * from country where isocode='MA' and latitude=0 and zoom=0)
		   update country set latitude=29.0917,longitude=-8.39262,zoom=5 where isocode='MA'
		 end
		
		if exists (select * from country where isocode='EE')
		 begin
		  if exists (select * from country where isocode='EE' and latitude=0 and zoom=0)
		   update country set latitude=58.5953,longitude=25.0136,zoom=6 where isocode='EE'
		 end
		
		if exists (select * from country where isocode='MD')
		 begin
		  if exists (select * from country where isocode='MD' and latitude=0 and zoom=0)
		   update country set latitude=47,longitude=28.3699,zoom=7 where isocode='MD'
		 end
		
		if exists (select * from country where isocode='BY')
		 begin
		  if exists (select * from country where isocode='BY' and latitude=0 and zoom=0)
		   update country set latitude=53.7098,longitude=27.9534,zoom=6 where isocode='BY'
		 end
		
		if exists (select * from country where isocode='PK')
		 begin
		  if exists (select * from country where isocode='PK' and latitude=0 and zoom=0)
		   update country set latitude=30.3753,longitude=69.3451,zoom=5 where isocode='PK'
		 end
		
		if exists (select * from country where isocode='SD')
		 begin
		  if exists (select * from country where isocode='SD' and latitude=0 and zoom=0)
		   update country set latitude=15,longitude=30.2176,zoom=5 where isocode='SD'
		 end
		
		if exists (select * from country where isocode='YE')
		 begin
		  if exists (select * from country where isocode='YE' and latitude=0 and zoom=0)
		   update country set latitude=15.5527,longitude=48.5164,zoom=5 where isocode='YE'
		 end
		
		if exists (select * from country where isocode='AL')
		 begin
		  if exists (select * from country where isocode='AL' and latitude=0 and zoom=0)
		   update country set latitude=41.1533,longitude=20.1683,zoom=7 where isocode='AL'
		 end
		
		if exists (select * from country where isocode='LT')
		 begin
		  if exists (select * from country where isocode='LT' and latitude=0 and zoom=0)
		   update country set latitude=55.1694,longitude=23.8813,zoom=6 where isocode='LT'
		 end
		
		if exists (select * from country where isocode='MK')
		 begin
		  if exists (select * from country where isocode='MK' and latitude=0 and zoom=0)
		   update country set latitude=41.6086,longitude=21.7453,zoom=8 where isocode='MK'
		 end
		
		if exists (select * from country where isocode='BA')
		 begin
		  if exists (select * from country where isocode='BA' and latitude=0 and zoom=0)
		   update country set latitude=44.0314,longitude=17.8344,zoom=7 where isocode='BA'
		 end
		
		if exists (select * from country where isocode='DZ')
		 begin
		  if exists (select * from country where isocode='DZ' and latitude=0 and zoom=0)
		   update country set latitude=28.0339,longitude=1.65963,zoom=4 where isocode='DZ'
		 end
		
		if exists (select * from country where isocode='MU')
		 begin
		  if exists (select * from country where isocode='MU' and latitude=0 and zoom=0)
		   update country set latitude=-20.3484,longitude=57.5522,zoom=9 where isocode='MU'
		 end
		
		if exists (select * from country where isocode='UA')
		 begin
		  if exists (select * from country where isocode='UA' and latitude=0 and zoom=0)
		   update country set latitude=48.3794,longitude=31.1656,zoom=5 where isocode='UA'
		 end
		
		if exists (select * from country where isocode='NR')
		 begin
		  if exists (select * from country where isocode='NR' and latitude=0 and zoom=0)
		   update country set latitude=54.7877,longitude=-6.49231,zoom=7 where isocode='NR'
		 end
		
		if exists (select * from country where isocode='CI')
		 begin
		  if exists (select * from country where isocode='CI' and latitude=0 and zoom=0)
		   update country set latitude=7.53999,longitude=-5.54708,zoom=6 where isocode='CI'
		 end
		
		if exists (select * from country where isocode='NA')
		 begin
		  if exists (select * from country where isocode='NA' and latitude=0 and zoom=0)
		   update country set latitude=-22.9576,longitude=18.4904,zoom=5 where isocode='NA'
		 end
		
		if exists (select * from country where isocode='MZ')
		 begin
		  if exists (select * from country where isocode='MZ' and latitude=0 and zoom=0)
		   update country set latitude=-18.6657,longitude=35.5296,zoom=5 where isocode='MZ'
		 end
		
		if exists (select * from country where isocode='BW')
		 begin
		  if exists (select * from country where isocode='BW' and latitude=0 and zoom=0)
		   update country set latitude=-22.3285,longitude=24.6849,zoom=5 where isocode='BW'
		 end
		
		if exists (select * from country where isocode='GH')
		 begin
		  if exists (select * from country where isocode='GH' and latitude=0 and zoom=0)
		   update country set latitude=7.94653,longitude=-1.02319,zoom=6 where isocode='GH'
		 end
		
		if exists (select * from country where isocode='GA')
		 begin
		  if exists (select * from country where isocode='GA' and latitude=0 and zoom=0)
		   update country set latitude=-0.803689,longitude=11.6094,zoom=6 where isocode='GA'
		 end
		
		if exists (select * from country where isocode='JP')
		 begin
		  if exists (select * from country where isocode='JP' and latitude=0 and zoom=0)
		   update country set latitude=36.2048,longitude=138.253,zoom=4 where isocode='JP'
		 end
		
		if exists (select * from country where isocode='NZ')
		 begin
		  if exists (select * from country where isocode='NZ' and latitude=0 and zoom=0)
		   update country set latitude=-40.9006,longitude=174.886,zoom=5 where isocode='NZ'
		 end
		
		if exists (select * from country where isocode='TW')
		 begin
		  if exists (select * from country where isocode='TW' and latitude=0 and zoom=0)
		   update country set latitude=23.6978,longitude=120.961,zoom=7 where isocode='TW'
		 end
		
		if exists (select * from country where isocode='PH')
		 begin
		  if exists (select * from country where isocode='PH' and latitude=0 and zoom=0)
		   update country set latitude=12.8797,longitude=121.774,zoom=5 where isocode='PH'
		 end
		
		if exists (select * from country where isocode='MY')
		 begin
		  if exists (select * from country where isocode='MY' and latitude=0 and zoom=0)
		   update country set latitude=4.21048,longitude=101.976,zoom=5 where isocode='MY'
		 end
		
		if exists (select * from country where isocode='HK')
		 begin
		  if exists (select * from country where isocode='HK' and latitude=0 and zoom=0)
		   update country set latitude=22.3964,longitude=114.109,zoom=10 where isocode='HK'
		 end
		
		if exists (select * from country where isocode='TH')
		 begin
		  if exists (select * from country where isocode='TH' and latitude=0 and zoom=0)
		   update country set latitude=14,longitude=100.993,zoom=5 where isocode='TH'
		 end
		
		if exists (select * from country where isocode='SG')
		 begin
		  if exists (select * from country where isocode='SG' and latitude=0 and zoom=0)
		   update country set latitude=1.35208,longitude=103.82,zoom=10 where isocode='SG'
		 end
		
		if exists (select * from country where isocode='AU')
		 begin
		  if exists (select * from country where isocode='AU' and latitude=0 and zoom=0)
		   update country set latitude=-25.2744,longitude=133.775,zoom=3 where isocode='AU'
		 end
		
		if exists (select * from country where isocode='CN')
		 begin
		  if exists (select * from country where isocode='CN' and latitude=0 and zoom=0)
		   update country set latitude=35.8617,longitude=104.195,zoom=3 where isocode='CN'
		 end
		
		if exists (select * from country where isocode='KR')
		 begin
		  if exists (select * from country where isocode='KR' and latitude=0 and zoom=0)
		   update country set latitude=35.9078,longitude=127.767,zoom=6 where isocode='KR'
		 end
		
		if exists (select * from country where isocode='UZ')
		 begin
		  if exists (select * from country where isocode='UZ' and latitude=0 and zoom=0)
		   update country set latitude=41.3775,longitude=64.5853,zoom=5 where isocode='UZ'
		 end
		
		if exists (select * from country where isocode='CM')
		 begin
		  if exists (select * from country where isocode='CM' and latitude=0 and zoom=0)
		   update country set latitude=7.36972,longitude=12.3547,zoom=5 where isocode='CM'
		 end
		
		if exists (select * from country where isocode='JE')
		 begin
		  if exists (select * from country where isocode='JE' and latitude=0 and zoom=0)
		   update country set latitude=49.2144,longitude=-2.13125,zoom=11 where isocode='JE'
		 end
		
		if exists (select * from country where isocode='GG')
		 begin
		  if exists (select * from country where isocode='GG' and latitude=0 and zoom=0)
		   update country set latitude=49.4657,longitude=-2.58528,zoom=10 where isocode='GG'
		 end
		
		if exists (select * from country where isocode='SY')
		 begin
		  if exists (select * from country where isocode='SY' and latitude=0 and zoom=0)
		   update country set latitude=34.8021,longitude=38.9968,zoom=6 where isocode='SY'
		 end
		
		if exists (select * from country where isocode='IN')
		 begin
		  if exists (select * from country where isocode='IN' and latitude=0 and zoom=0)
		   update country set latitude=20.5937,longitude=78.9629,zoom=4 where isocode='IN'
		 end
		
		if exists (select * from country where isocode='AF')
		 begin
		  if exists (select * from country where isocode='AF' and latitude=0 and zoom=0)
		   update country set latitude=33.9391,longitude=67.71,zoom=5 where isocode='AF'
		 end
		
		if exists (select * from country where isocode='AX')
		 begin
		  if exists (select * from country where isocode='AX' and latitude=0 and zoom=0)
		   update country set latitude=60.177,longitude=19.915,zoom=8 where isocode='AX'
		 end
		
		if exists (select * from country where isocode='AS')
		 begin
		  if exists (select * from country where isocode='AS' and latitude=0 and zoom=0)
		   update country set latitude=-14.2963,longitude=189.311,zoom=11 where isocode='AS'
		 end
		
		if exists (select * from country where isocode='AD')
		 begin
		  if exists (select * from country where isocode='AD' and latitude=0 and zoom=0)
		   update country set latitude=42.5462,longitude=1.60155,zoom=10 where isocode='AD'
		 end
		
		if exists (select * from country where isocode='AO')
		 begin
		  if exists (select * from country where isocode='AO' and latitude=0 and zoom=0)
		   update country set latitude=-11.2027,longitude=17.8739,zoom=5 where isocode='AO'
		 end
		
		if exists (select * from country where isocode='AI')
		 begin
		  if exists (select * from country where isocode='AI' and latitude=0 and zoom=0)
		   update country set latitude=18.2206,longitude=-63.0686,zoom=10 where isocode='AI'
		 end
		
		if exists (select * from country where isocode='AQ')
		 begin
		  if exists (select * from country where isocode='AQ' and latitude=0 and zoom=0)
		   update country set latitude=-75.251,longitude=-0.071389,zoom=1 where isocode='AQ'
		 end
		
		if exists (select * from country where isocode='AG')
		 begin
		  if exists (select * from country where isocode='AG' and latitude=0 and zoom=0)
		   update country set latitude=17.3743,longitude=-61.7567,zoom=9 where isocode='AG'
		 end
		
		if exists (select * from country where isocode='AM')
		 begin
		  if exists (select * from country where isocode='AM' and latitude=0 and zoom=0)
		   update country set latitude=40.0691,longitude=45.0382,zoom=7 where isocode='AM'
		 end
		
		if exists (select * from country where isocode='AW')
		 begin
		  if exists (select * from country where isocode='AW' and latitude=0 and zoom=0)
		   update country set latitude=12.5211,longitude=-69.9683,zoom=11 where isocode='AW'
		 end
		
		if exists (select * from country where isocode='AZ')
		 begin
		  if exists (select * from country where isocode='AZ' and latitude=0 and zoom=0)
		   update country set latitude=40.1431,longitude=47.5769,zoom=6 where isocode='AZ'
		 end
		
		if exists (select * from country where isocode='BS')
		 begin
		  if exists (select * from country where isocode='BS' and latitude=0 and zoom=0)
		   update country set latitude=25.0343,longitude=-77.3963,zoom=6 where isocode='BS'
		 end
		
		if exists (select * from country where isocode='BD')
		 begin
		  if exists (select * from country where isocode='BD' and latitude=0 and zoom=0)
		   update country set latitude=23.685,longitude=90.3563,zoom=6 where isocode='BD'
		 end
		
		if exists (select * from country where isocode='BB')
		 begin
		  if exists (select * from country where isocode='BB' and latitude=0 and zoom=0)
		   update country set latitude=13.1939,longitude=-59.5432,zoom=10 where isocode='BB'
		 end
		
		if exists (select * from country where isocode='BZ')
		 begin
		  if exists (select * from country where isocode='BZ' and latitude=0 and zoom=0)
		   update country set latitude=17.1899,longitude=-88.4976,zoom=7 where isocode='BZ'
		 end
		
		if exists (select * from country where isocode='BJ')
		 begin
		  if exists (select * from country where isocode='BJ' and latitude=0 and zoom=0)
		   update country set latitude=9.30769,longitude=2.31583,zoom=6 where isocode='BJ'
		 end
		
		if exists (select * from country where isocode='BM')
		 begin
		  if exists (select * from country where isocode='BM' and latitude=0 and zoom=0)
		   update country set latitude=32.3214,longitude=-64.7574,zoom=11 where isocode='BM'
		 end
		
		if exists (select * from country where isocode='BT')
		 begin
		  if exists (select * from country where isocode='BT' and latitude=0 and zoom=0)
		   update country set latitude=27.5142,longitude=90.4336,zoom=7 where isocode='BT'
		 end
		
		if exists (select * from country where isocode='BO')
		 begin
		  if exists (select * from country where isocode='BO' and latitude=0 and zoom=0)
		   update country set latitude=-16.2902,longitude=-63.5887,zoom=5 where isocode='BO'
		 end
		
		if exists (select * from country where isocode='BV')
		 begin
		  if exists (select * from country where isocode='BV' and latitude=0 and zoom=0)
		   update country set latitude=-54.4232,longitude=3.41319,zoom=11 where isocode='BV'
		 end
		
		if exists (select * from country where isocode='BR')
		 begin
		  if exists (select * from country where isocode='BR' and latitude=0 and zoom=0)
		   update country set latitude=-14.235,longitude=-51.9253,zoom=3 where isocode='BR'
		 end
		
		if exists (select * from country where isocode='IO')
		 begin
		  if exists (select * from country where isocode='IO' and latitude=0 and zoom=0)
		   update country set latitude=-6.34319,longitude=71.8765,zoom=7 where isocode='IO'
		 end
		
		if exists (select * from country where isocode='BN')
		 begin
		  if exists (select * from country where isocode='BN' and latitude=0 and zoom=0)
		   update country set latitude=4.53528,longitude=114.728,zoom=9 where isocode='BN'
		 end
		
		if exists (select * from country where isocode='BF')
		 begin
		  if exists (select * from country where isocode='BF' and latitude=0 and zoom=0)
		   update country set latitude=12.2383,longitude=-1.56159,zoom=6 where isocode='BF'
		 end
		
		if exists (select * from country where isocode='BI')
		 begin
		  if exists (select * from country where isocode='BI' and latitude=0 and zoom=0)
		   update country set latitude=-3.37306,longitude=29.9189,zoom=8 where isocode='BI'
		 end
		
		if exists (select * from country where isocode='KH')
		 begin
		  if exists (select * from country where isocode='KH' and latitude=0 and zoom=0)
		   update country set latitude=12.5657,longitude=104.991,zoom=6 where isocode='KH'
		 end
		
		if exists (select * from country where isocode='CA')
		 begin
		  if exists (select * from country where isocode='CA' and latitude=0 and zoom=0)
		   update country set latitude=72.8347,longitude=-86.7451,zoom=2 where isocode='CA'
		 end
		
		if exists (select * from country where isocode='CV')
		 begin
		  if exists (select * from country where isocode='CV' and latitude=0 and zoom=0)
		   update country set latitude=16.0021,longitude=-24.0132,zoom=7 where isocode='CV'
		 end
		
		if exists (select * from country where isocode='KY')
		 begin
		  if exists (select * from country where isocode='KY' and latitude=0 and zoom=0)
		   update country set latitude=19.5135,longitude=-80.567,zoom=8 where isocode='KY'
		 end
		
		if exists (select * from country where isocode='CF')
		 begin
		  if exists (select * from country where isocode='CF' and latitude=0 and zoom=0)
		   update country set latitude=6.61111,longitude=20.9394,zoom=5 where isocode='CF'
		 end
		
		if exists (select * from country where isocode='TD')
		 begin
		  if exists (select * from country where isocode='TD' and latitude=0 and zoom=0)
		   update country set latitude=15.4542,longitude=18.7322,zoom=5 where isocode='TD'
		 end
		
		if exists (select * from country where isocode='CL')
		 begin
		  if exists (select * from country where isocode='CL' and latitude=0 and zoom=0)
		   update country set latitude=-35.6751,longitude=-71.543,zoom=3 where isocode='CL'
		 end
		
		if exists (select * from country where isocode='CX')
		 begin
		  if exists (select * from country where isocode='CX' and latitude=0 and zoom=0)
		   update country set latitude=-10.49,longitude=105.632,zoom=11 where isocode='CX'
		 end
		
		if exists (select * from country where isocode='CC')
		 begin
		  if exists (select * from country where isocode='CC' and latitude=0 and zoom=0)
		   update country set latitude=-12.1642,longitude=96.871,zoom=11 where isocode='CC'
		 end
		
		if exists (select * from country where isocode='CO')
		 begin
		  if exists (select * from country where isocode='CO' and latitude=0 and zoom=0)
		   update country set latitude=4.57087,longitude=-74.2973,zoom=5 where isocode='CO'
		 end
		
		if exists (select * from country where isocode='KM')
		 begin
		  if exists (select * from country where isocode='KM' and latitude=0 and zoom=0)
		   update country set latitude=-12.2621,longitude=44.3822,zoom=8 where isocode='KM'
		 end
		
		if exists (select * from country where isocode='CG')
		 begin
		  if exists (select * from country where isocode='CG' and latitude=0 and zoom=0)
		   update country set latitude=-0.728021,longitude=15,zoom=6 where isocode='CG'
		 end
		
		if exists (select * from country where isocode='CD')
		 begin
		  if exists (select * from country where isocode='CD' and latitude=0 and zoom=0)
		   update country set latitude=-3.34063,longitude=21.8726,zoom=4 where isocode='CD'
		 end
		
		if exists (select * from country where isocode='CK')
		 begin
		  if exists (select * from country where isocode='CK' and latitude=0 and zoom=0)
		   update country set latitude=-21.2367,longitude=-159.778,zoom=4 where isocode='CK'
		 end
		
		if exists (select * from country where isocode='CR')
		 begin
		  if exists (select * from country where isocode='CR' and latitude=0 and zoom=0)
		   update country set latitude=9.74892,longitude=-83.7534,zoom=7 where isocode='CR'
		 end
		
		if exists (select * from country where isocode='CU')
		 begin
		  if exists (select * from country where isocode='CU' and latitude=0 and zoom=0)
		   update country set latitude=21.5218,longitude=-79.5,zoom=6 where isocode='CU'
		 end
		
		if exists (select * from country where isocode='DJ')
		 begin
		  if exists (select * from country where isocode='DJ' and latitude=0 and zoom=0)
		   update country set latitude=11.8251,longitude=42.5903,zoom=8 where isocode='DJ'
		 end
		
		if exists (select * from country where isocode='DM')
		 begin
		  if exists (select * from country where isocode='DM' and latitude=0 and zoom=0)
		   update country set latitude=15.415,longitude=-61.371,zoom=10 where isocode='DM'
		 end
		
		if exists (select * from country where isocode='DO')
		 begin
		  if exists (select * from country where isocode='DO' and latitude=0 and zoom=0)
		   update country set latitude=18.7357,longitude=-70.1627,zoom=7 where isocode='DO'
		 end
		
		if exists (select * from country where isocode='EC')
		 begin
		  if exists (select * from country where isocode='EC' and latitude=0 and zoom=0)
		   update country set latitude=-1.83124,longitude=-78.1834,zoom=5 where isocode='EC'
		 end
		
		if exists (select * from country where isocode='SV')
		 begin
		  if exists (select * from country where isocode='SV' and latitude=0 and zoom=0)
		   update country set latitude=13.7942,longitude=-88.8965,zoom=8 where isocode='SV'
		 end
		
		if exists (select * from country where isocode='GQ')
		 begin
		  if exists (select * from country where isocode='GQ' and latitude=0 and zoom=0)
		   update country set latitude=1.6508,longitude=10.2679,zoom=8 where isocode='GQ'
		 end
		
		if exists (select * from country where isocode='ER')
		 begin
		  if exists (select * from country where isocode='ER' and latitude=0 and zoom=0)
		   update country set latitude=15.1794,longitude=39.7823,zoom=6 where isocode='ER'
		 end
		
		if exists (select * from country where isocode='ET')
		 begin
		  if exists (select * from country where isocode='ET' and latitude=0 and zoom=0)
		   update country set latitude=9.145,longitude=40.4897,zoom=5 where isocode='ET'
		 end
		
		if exists (select * from country where isocode='FK')
		 begin
		  if exists (select * from country where isocode='FK' and latitude=0 and zoom=0)
		   update country set latitude=-51.7963,longitude=-59.5236,zoom=7 where isocode='FK'
		 end
		
		if exists (select * from country where isocode='FO')
		 begin
		  if exists (select * from country where isocode='FO' and latitude=0 and zoom=0)
		   update country set latitude=61.8926,longitude=-6.91181,zoom=8 where isocode='FO'
		 end
		
		if exists (select * from country where isocode='FJ')
		 begin
		  if exists (select * from country where isocode='FJ' and latitude=0 and zoom=0)
		   update country set latitude=-17.7134,longitude=178.065,zoom=6 where isocode='FJ'
		 end
		
		if exists (select * from country where isocode='GF')
		 begin
		  if exists (select * from country where isocode='GF' and latitude=0 and zoom=0)
		   update country set latitude=3.93389,longitude=-53.1258,zoom=7 where isocode='GF'
		 end
		
		if exists (select * from country where isocode='PF')
		 begin
		  if exists (select * from country where isocode='PF' and latitude=0 and zoom=0)
		   update country set latitude=-17.6797,longitude=-149.407,zoom=3 where isocode='PF'
		 end
		
		if exists (select * from country where isocode='TF')
		 begin
		  if exists (select * from country where isocode='TF' and latitude=0 and zoom=0)
		   update country set latitude=-49.2804,longitude=69.3486,zoom=8 where isocode='TF'
		 end
		
		if exists (select * from country where isocode='GM')
		 begin
		  if exists (select * from country where isocode='GM' and latitude=0 and zoom=0)
		   update country set latitude=13.4432,longitude=-15.3101,zoom=7 where isocode='GM'
		 end
		
		if exists (select * from country where isocode='GE')
		 begin
		  if exists (select * from country where isocode='GE' and latitude=0 and zoom=0)
		   update country set latitude=42.3154,longitude=43.3569,zoom=6 where isocode='GE'
		 end
		
		if exists (select * from country where isocode='GI')
		 begin
		  if exists (select * from country where isocode='GI' and latitude=0 and zoom=0)
		   update country set latitude=36.13,longitude=-5.34537,zoom=13 where isocode='GI'
		 end
		
		if exists (select * from country where isocode='GL')
		 begin
		  if exists (select * from country where isocode='GL' and latitude=0 and zoom=0)
		   update country set latitude=70.5167,longitude=-26.25,zoom=2 where isocode='GL'
		 end
		
		if exists (select * from country where isocode='GD')
		 begin
		  if exists (select * from country where isocode='GD' and latitude=0 and zoom=0)
		   update country set latitude=12.2628,longitude=-61.6042,zoom=10 where isocode='GD'
		 end
		
		if exists (select * from country where isocode='GP')
		 begin
		  if exists (select * from country where isocode='GP' and latitude=0 and zoom=0)
		   update country set latitude=16.265,longitude=-61.551,zoom=9 where isocode='GP'
		 end
		
		if exists (select * from country where isocode='GU')
		 begin
		  if exists (select * from country where isocode='GU' and latitude=0 and zoom=0)
		   update country set latitude=13.4443,longitude=144.794,zoom=9 where isocode='GU'
		 end
		
		if exists (select * from country where isocode='GT')
		 begin
		  if exists (select * from country where isocode='GT' and latitude=0 and zoom=0)
		   update country set latitude=15.7835,longitude=-90.2308,zoom=7 where isocode='GT'
		 end
		
		if exists (select * from country where isocode='GN')
		 begin
		  if exists (select * from country where isocode='GN' and latitude=0 and zoom=0)
		   update country set latitude=9.94559,longitude=-9.69665,zoom=6 where isocode='GN'
		 end
		
		if exists (select * from country where isocode='GW')
		 begin
		  if exists (select * from country where isocode='GW' and latitude=0 and zoom=0)
		   update country set latitude=11.8037,longitude=-15.1804,zoom=7 where isocode='GW'
		 end
		
		if exists (select * from country where isocode='GY')
		 begin
		  if exists (select * from country where isocode='GY' and latitude=0 and zoom=0)
		   update country set latitude=4.86042,longitude=-58.9302,zoom=6 where isocode='GY'
		 end
		
		if exists (select * from country where isocode='HT')
		 begin
		  if exists (select * from country where isocode='HT' and latitude=0 and zoom=0)
		   update country set latitude=18.9712,longitude=-72.2852,zoom=7 where isocode='HT'
		 end
		
		if exists (select * from country where isocode='HM')
		 begin
		  if exists (select * from country where isocode='HM' and latitude=0 and zoom=0)
		   update country set latitude=-53.0818,longitude=73.5042,zoom=10 where isocode='HM'
		 end
		
		if exists (select * from country where isocode='VA')
		 begin
		  if exists (select * from country where isocode='VA' and latitude=0 and zoom=0)
		   update country set latitude=41.9039,longitude=12.4527,zoom=15 where isocode='VA'
		 end
		
		if exists (select * from country where isocode='HN')
		 begin
		  if exists (select * from country where isocode='HN' and latitude=0 and zoom=0)
		   update country set latitude=15.2,longitude=-86.2419,zoom=6 where isocode='HN'
		 end
		
		if exists (select * from country where isocode='ID')
		 begin
		  if exists (select * from country where isocode='ID' and latitude=0 and zoom=0)
		   update country set latitude=-0.789275,longitude=113.921,zoom=3 where isocode='ID'
		 end
		
		if exists (select * from country where isocode='IR')
		 begin
		  if exists (select * from country where isocode='IR' and latitude=0 and zoom=0)
		   update country set latitude=32.4279,longitude=53.688,zoom=5 where isocode='IR'
		 end
		
		if exists (select * from country where isocode='IQ')
		 begin
		  if exists (select * from country where isocode='IQ' and latitude=0 and zoom=0)
		   update country set latitude=33.2232,longitude=43.6793,zoom=5 where isocode='IQ'
		 end
		
		if exists (select * from country where isocode='IM')
		 begin
		  if exists (select * from country where isocode='IM' and latitude=0 and zoom=0)
		   update country set latitude=54.2361,longitude=-4.54806,zoom=9 where isocode='IM'
		 end
		
		if exists (select * from country where isocode='JM')
		 begin
		  if exists (select * from country where isocode='JM' and latitude=0 and zoom=0)
		   update country set latitude=18.1096,longitude=-77.2975,zoom=8 where isocode='JM'
		 end
		
		if exists (select * from country where isocode='KZ')
		 begin
		  if exists (select * from country where isocode='KZ' and latitude=0 and zoom=0)
		   update country set latitude=48.0196,longitude=66.9237,zoom=4 where isocode='KZ'
		 end
		
		if exists (select * from country where isocode='KI')
		 begin
		  if exists (select * from country where isocode='KI' and latitude=0 and zoom=0)
		   update country set latitude=-3.37042,longitude=-168.734,zoom=4 where isocode='KI'
		 end
		
		if exists (select * from country where isocode='KG')
		 begin
		  if exists (select * from country where isocode='KG' and latitude=0 and zoom=0)
		   update country set latitude=41.2044,longitude=74.7661,zoom=6 where isocode='KG'
		 end
		
		if exists (select * from country where isocode='LA')
		 begin
		  if exists (select * from country where isocode='LA' and latitude=0 and zoom=0)
		   update country set latitude=18.4563,longitude=104.895,zoom=6 where isocode='LA'
		 end
		
		if exists (select * from country where isocode='LS')
		 begin
		  if exists (select * from country where isocode='LS' and latitude=0 and zoom=0)
		   update country set latitude=-29.61,longitude=28.2336,zoom=7 where isocode='LS'
		 end
		
		if exists (select * from country where isocode='LR')
		 begin
		  if exists (select * from country where isocode='LR' and latitude=0 and zoom=0)
		   update country set latitude=6.42805,longitude=-9.4295,zoom=7 where isocode='LR'
		 end
		
		if exists (select * from country where isocode='LY')
		 begin
		  if exists (select * from country where isocode='LY' and latitude=0 and zoom=0)
		   update country set latitude=26.3351,longitude=17.2283,zoom=5 where isocode='LY'
		 end
		
		if exists (select * from country where isocode='LI')
		 begin
		  if exists (select * from country where isocode='LI' and latitude=0 and zoom=0)
		   update country set latitude=47.166,longitude=9.55537,zoom=10 where isocode='LI'
		 end
		
		if exists (select * from country where isocode='MO')
		 begin
		  if exists (select * from country where isocode='MO' and latitude=0 and zoom=0)
		   update country set latitude=22.17,longitude=113.58,zoom=12 where isocode='MO'
		 end
		
		if exists (select * from country where isocode='MG')
		 begin
		  if exists (select * from country where isocode='MG' and latitude=0 and zoom=0)
		   update country set latitude=-18.7669,longitude=46.8691,zoom=5 where isocode='MG'
		 end
		
		if exists (select * from country where isocode='MW')
		 begin
		  if exists (select * from country where isocode='MW' and latitude=0 and zoom=0)
		   update country set latitude=-13.2543,longitude=34.3015,zoom=6 where isocode='MW'
		 end
		
		if exists (select * from country where isocode='MV')
		 begin
		  if exists (select * from country where isocode='MV' and latitude=0 and zoom=0)
		   update country set latitude=3.20278,longitude=73.2207,zoom=5 where isocode='MV'
		 end
		
		if exists (select * from country where isocode='ML')
		 begin
		  if exists (select * from country where isocode='ML' and latitude=0 and zoom=0)
		   update country set latitude=17.5707,longitude=-3.99617,zoom=5 where isocode='ML'
		 end
		
		if exists (select * from country where isocode='MH')
		 begin
		  if exists (select * from country where isocode='MH' and latitude=0 and zoom=0)
		   update country set latitude=7.13147,longitude=171.184,zoom=4 where isocode='MH'
		 end
		
		if exists (select * from country where isocode='MQ')
		 begin
		  if exists (select * from country where isocode='MQ' and latitude=0 and zoom=0)
		   update country set latitude=14.6415,longitude=-61.0242,zoom=10 where isocode='MQ'
		 end
		
		if exists (select * from country where isocode='MR')
		 begin
		  if exists (select * from country where isocode='MR' and latitude=0 and zoom=0)
		   update country set latitude=21.0079,longitude=-10.9408,zoom=5 where isocode='MR'
		 end
		
		if exists (select * from country where isocode='YT')
		 begin
		  if exists (select * from country where isocode='YT' and latitude=0 and zoom=0)
		   update country set latitude=-12.8204,longitude=45.1686,zoom=10 where isocode='YT'
		 end
		
		if exists (select * from country where isocode='MX')
		 begin
		  if exists (select * from country where isocode='MX' and latitude=0 and zoom=0)
		   update country set latitude=23.6345,longitude=-102.553,zoom=4 where isocode='MX'
		 end
		
		if exists (select * from country where isocode='FM')
		 begin
		  if exists (select * from country where isocode='FM' and latitude=0 and zoom=0)
		   update country set latitude=7.42555,longitude=150.551,zoom=4 where isocode='FM'
		 end
		
		if exists (select * from country where isocode='MC')
		 begin
		  if exists (select * from country where isocode='MC' and latitude=0 and zoom=0)
		   update country set latitude=43.7384,longitude=7.42462,zoom=13 where isocode='MC'
		 end
		
		if exists (select * from country where isocode='MN')
		 begin
		  if exists (select * from country where isocode='MN' and latitude=0 and zoom=0)
		   update country set latitude=46.8625,longitude=103.847,zoom=4 where isocode='MN'
		 end
		
		if exists (select * from country where isocode='ME')
		 begin
		  if exists (select * from country where isocode='ME' and latitude=0 and zoom=0)
		   update country set latitude=42.7087,longitude=19.3744,zoom=7 where isocode='ME'
		 end
		
		if exists (select * from country where isocode='MS')
		 begin
		  if exists (select * from country where isocode='MS' and latitude=0 and zoom=0)
		   update country set latitude=16.7514,longitude=-62.185,zoom=11 where isocode='MS'
		 end
		
		if exists (select * from country where isocode='MM')
		 begin
		  if exists (select * from country where isocode='MM' and latitude=0 and zoom=0)
		   update country set latitude=21.914,longitude=95.9562,zoom=5 where isocode='MM'
		 end
		
		if exists (select * from country where isocode='NP')
		 begin
		  if exists (select * from country where isocode='NP' and latitude=0 and zoom=0)
		   update country set latitude=28.3949,longitude=84.124,zoom=6 where isocode='NP'
		 end
		
		if exists (select * from country where isocode='AN')
		 begin
		  if exists (select * from country where isocode='AN' and latitude=0 and zoom=0)
		   update country set latitude=12.2261,longitude=-69.0601,zoom=5 where isocode='AN'
		 end
		
		if exists (select * from country where isocode='NC')
		 begin
		  if exists (select * from country where isocode='NC' and latitude=0 and zoom=0)
		   update country set latitude=-20.9043,longitude=165.618,zoom=6 where isocode='NC'
		 end
		
		if exists (select * from country where isocode='NI')
		 begin
		  if exists (select * from country where isocode='NI' and latitude=0 and zoom=0)
		   update country set latitude=12.8654,longitude=-85.2072,zoom=6 where isocode='NI'
		 end
		
		if exists (select * from country where isocode='NE')
		 begin
		  if exists (select * from country where isocode='NE' and latitude=0 and zoom=0)
		   update country set latitude=17.6078,longitude=8.08167,zoom=5 where isocode='NE'
		 end
		
		if exists (select * from country where isocode='NU')
		 begin
		  if exists (select * from country where isocode='NU' and latitude=0 and zoom=0)
		   update country set latitude=-19.0544,longitude=-169.867,zoom=11 where isocode='NU'
		 end
		
		if exists (select * from country where isocode='NF')
		 begin
		  if exists (select * from country where isocode='NF' and latitude=0 and zoom=0)
		   update country set latitude=-29.0408,longitude=167.955,zoom=11 where isocode='NF'
		 end
		
		if exists (select * from country where isocode='MP')
		 begin
		  if exists (select * from country where isocode='MP' and latitude=0 and zoom=0)
		   update country set latitude=15,longitude=145.385,zoom=7 where isocode='MP'
		 end
		
		if exists (select * from country where isocode='PW')
		 begin
		  if exists (select * from country where isocode='PW' and latitude=0 and zoom=0)
		   update country set latitude=7.31667,longitude=134.483,zoom=9 where isocode='PW'
		 end
		
		if exists (select * from country where isocode='PS')
		 begin
		  if exists (select * from country where isocode='PS' and latitude=0 and zoom=0)
		   update country set latitude=31.9522,longitude=35.2332,zoom=8 where isocode='PS'
		 end
		
		if exists (select * from country where isocode='PA')
		 begin
		  if exists (select * from country where isocode='PA' and latitude=0 and zoom=0)
		   update country set latitude=8.53798,longitude=-80.7821,zoom=6 where isocode='PA'
		 end
		
		if exists (select * from country where isocode='PG')
		 begin
		  if exists (select * from country where isocode='PG' and latitude=0 and zoom=0)
		   update country set latitude=-6.31499,longitude=143.956,zoom=5 where isocode='PG'
		 end
		
		if exists (select * from country where isocode='PY')
		 begin
		  if exists (select * from country where isocode='PY' and latitude=0 and zoom=0)
		   update country set latitude=-23.4425,longitude=-58.4438,zoom=6 where isocode='PY'
		 end
		
		if exists (select * from country where isocode='PE')
		 begin
		  if exists (select * from country where isocode='PE' and latitude=0 and zoom=0)
		   update country set latitude=-9.18997,longitude=-75.0152,zoom=4 where isocode='PE'
		 end
		
		if exists (select * from country where isocode='PN')
		 begin
		  if exists (select * from country where isocode='PN' and latitude=0 and zoom=0)
		   update country set latitude=-24.38,longitude=-128.333,zoom=12 where isocode='PN'
		 end
		
		if exists (select * from country where isocode='PR')
		 begin
		  if exists (select * from country where isocode='PR' and latitude=0 and zoom=0)
		   update country set latitude=18.2208,longitude=-66.5901,zoom=8 where isocode='PR'
		 end
		
		if exists (select * from country where isocode='RE')
		 begin
		  if exists (select * from country where isocode='RE' and latitude=0 and zoom=0)
		   update country set latitude=-21.1151,longitude=55.5364,zoom=9 where isocode='RE'
		 end
		
		if exists (select * from country where isocode='RW')
		 begin
		  if exists (select * from country where isocode='RW' and latitude=0 and zoom=0)
		   update country set latitude=-1.94028,longitude=29.8739,zoom=8 where isocode='RW'
		 end
		
		if exists (select * from country where isocode='KN')
		 begin
		  if exists (select * from country where isocode='KN' and latitude=0 and zoom=0)
		   update country set latitude=17.3578,longitude=-62.783,zoom=10 where isocode='KN'
		 end
		
		if exists (select * from country where isocode='LC')
		 begin
		  if exists (select * from country where isocode='LC' and latitude=0 and zoom=0)
		   update country set latitude=13.9094,longitude=-60.9789,zoom=10 where isocode='LC'
		 end
		
		if exists (select * from country where isocode='VC')
		 begin
		  if exists (select * from country where isocode='VC' and latitude=0 and zoom=0)
		   update country set latitude=12.9843,longitude=-61.2872,zoom=9 where isocode='VC'
		 end
		
		if exists (select * from country where isocode='WS')
		 begin
		  if exists (select * from country where isocode='WS' and latitude=0 and zoom=0)
		   update country set latitude=-13.759,longitude=-172.105,zoom=9 where isocode='WS'
		 end
		
		if exists (select * from country where isocode='SM')
		 begin
		  if exists (select * from country where isocode='SM' and latitude=0 and zoom=0)
		   update country set latitude=43.9424,longitude=12.4578,zoom=12 where isocode='SM'
		 end
		
		if exists (select * from country where isocode='ST')
		 begin
		  if exists (select * from country where isocode='ST' and latitude=0 and zoom=0)
		   update country set latitude=0.18636,longitude=6.61308,zoom=9 where isocode='ST'
		 end
		
		if exists (select * from country where isocode='SN')
		 begin
		  if exists (select * from country where isocode='SN' and latitude=0 and zoom=0)
		   update country set latitude=14.4974,longitude=-14.4524,zoom=6 where isocode='SN'
		 end
		
		if exists (select * from country where isocode='RS')
		 begin
		  if exists (select * from country where isocode='RS' and latitude=0 and zoom=0)
		   update country set latitude=22.0655,longitude=-80.2929,zoom=8 where isocode='RS'
		 end
		
		if exists (select * from country where isocode='RS')
		 begin
		  if exists (select * from country where isocode='RS' and latitude=0 and zoom=0)
		   update country set latitude=44.0165,longitude=21.0059,zoom=6 where isocode='RS'
		 end
		
		if exists (select * from country where isocode='SC')
		 begin
		  if exists (select * from country where isocode='SC' and latitude=0 and zoom=0)
		   update country set latitude=-4.67957,longitude=55.492,zoom=9 where isocode='SC'
		 end
		
		if exists (select * from country where isocode='SL')
		 begin
		  if exists (select * from country where isocode='SL' and latitude=0 and zoom=0)
		   update country set latitude=8.46055,longitude=-11.7799,zoom=7 where isocode='SL'
		 end
		
		if exists (select * from country where isocode='SB')
		 begin
		  if exists (select * from country where isocode='SB' and latitude=0 and zoom=0)
		   update country set latitude=-9.64571,longitude=160.156,zoom=4 where isocode='SB'
		 end
		
		if exists (select * from country where isocode='SO')
		 begin
		  if exists (select * from country where isocode='SO' and latitude=0 and zoom=0)
		   update country set latitude=5.15215,longitude=46.1996,zoom=5 where isocode='SO'
		 end
		
		if exists (select * from country where isocode='GS')
		 begin
		  if exists (select * from country where isocode='GS' and latitude=0 and zoom=0)
		   update country set latitude=-54.4138,longitude=-36.5827,zoom=3 where isocode='GS'
		 end
		
		if exists (select * from country where isocode='LK')
		 begin
		  if exists (select * from country where isocode='LK' and latitude=0 and zoom=0)
		   update country set latitude=7.87305,longitude=80.7718,zoom=7 where isocode='LK'
		 end
		
		if exists (select * from country where isocode='SH')
		 begin
		  if exists (select * from country where isocode='SH' and latitude=0 and zoom=0)
		   update country set latitude=-15.9645,longitude=-5.705,zoom=12 where isocode='SH'
		 end
		
		if exists (select * from country where isocode='PM')
		 begin
		  if exists (select * from country where isocode='PM' and latitude=0 and zoom=0)
		   update country set latitude=46.9419,longitude=-56.2711,zoom=9 where isocode='PM'
		 end
		
		if exists (select * from country where isocode='SR')
		 begin
		  if exists (select * from country where isocode='SR' and latitude=0 and zoom=0)
		   update country set latitude=3.91931,longitude=-56.0278,zoom=7 where isocode='SR'
		 end
		
		if exists (select * from country where isocode='SJ')
		 begin
		  if exists (select * from country where isocode='SJ' and latitude=0 and zoom=0)
		   update country set latitude=77.5536,longitude=23.6703,zoom=4 where isocode='SJ'
		 end
		
		if exists (select * from country where isocode='SZ')
		 begin
		  if exists (select * from country where isocode='SZ' and latitude=0 and zoom=0)
		   update country set latitude=-26.5225,longitude=31.4659,zoom=8 where isocode='SZ'
		 end
		
		if exists (select * from country where isocode='TJ')
		 begin
		  if exists (select * from country where isocode='TJ' and latitude=0 and zoom=0)
		   update country set latitude=38.861,longitude=71.2761,zoom=6 where isocode='TJ'
		 end
		
		if exists (select * from country where isocode='TZ')
		 begin
		  if exists (select * from country where isocode='TZ' and latitude=0 and zoom=0)
		   update country set latitude=-6.36903,longitude=34.8888,zoom=5 where isocode='TZ'
		 end
		
		if exists (select * from country where isocode='TL')
		 begin
		  if exists (select * from country where isocode='TL' and latitude=0 and zoom=0)
		   update country set latitude=-8.87422,longitude=126,zoom=8 where isocode='TL'
		 end
		
		if exists (select * from country where isocode='TG')
		 begin
		  if exists (select * from country where isocode='TG' and latitude=0 and zoom=0)
		   update country set latitude=8.61954,longitude=0.824782,zoom=6 where isocode='TG'
		 end
		
		if exists (select * from country where isocode='TK')
		 begin
		  if exists (select * from country where isocode='TK' and latitude=0 and zoom=0)
		   update country set latitude=-8.96736,longitude=-171.856,zoom=9 where isocode='TK'
		 end
		
		if exists (select * from country where isocode='TO')
		 begin
		  if exists (select * from country where isocode='TO' and latitude=0 and zoom=0)
		   update country set latitude=-21.179,longitude=-175.198,zoom=9 where isocode='TO'
		 end
		
		if exists (select * from country where isocode='TT')
		 begin
		  if exists (select * from country where isocode='TT' and latitude=0 and zoom=0)
		   update country set latitude=10.6918,longitude=-61.2225,zoom=8 where isocode='TT'
		 end
		
		if exists (select * from country where isocode='TM')
		 begin
		  if exists (select * from country where isocode='TM' and latitude=0 and zoom=0)
		   update country set latitude=38.9697,longitude=59.5563,zoom=5 where isocode='TM'
		 end
		
		if exists (select * from country where isocode='TC')
		 begin
		  if exists (select * from country where isocode='TC' and latitude=0 and zoom=0)
		   update country set latitude=21.694,longitude=-71.7979,zoom=9 where isocode='TC'
		 end
		
		if exists (select * from country where isocode='TV')
		 begin
		  if exists (select * from country where isocode='TV' and latitude=0 and zoom=0)
		   update country set latitude=-7.10954,longitude=177.649,zoom=4 where isocode='TV'
		 end
		
		if exists (select * from country where isocode='UG')
		 begin
		  if exists (select * from country where isocode='UG' and latitude=0 and zoom=0)
		   update country set latitude=1.37333,longitude=32.2903,zoom=6 where isocode='UG'
		 end
		
		if exists (select * from country where isocode='UM')
		 begin
		  if exists (select * from country where isocode='UM' and latitude=0 and zoom=0)
		   update country set latitude=14.0045,longitude=-134.203,zoom=3 where isocode='UM'
		 end
		
		if exists (select * from country where isocode='UY')
		 begin
		  if exists (select * from country where isocode='UY' and latitude=0 and zoom=0)
		   update country set latitude=-32.5228,longitude=-55.7658,zoom=6 where isocode='UY'
		 end
		
		if exists (select * from country where isocode='VU')
		 begin
		  if exists (select * from country where isocode='VU' and latitude=0 and zoom=0)
		   update country set latitude=-15.3767,longitude=166.959,zoom=5 where isocode='VU'
		 end
		
		if exists (select * from country where isocode='VE')
		 begin
		  if exists (select * from country where isocode='VE' and latitude=0 and zoom=0)
		   update country set latitude=6.42375,longitude=-66.5897,zoom=5 where isocode='VE'
		 end
		
		if exists (select * from country where isocode='VN')
		 begin
		  if exists (select * from country where isocode='VN' and latitude=0 and zoom=0)
		   update country set latitude=14.0583,longitude=108.277,zoom=5 where isocode='VN'
		 end
		
		if exists (select * from country where isocode='VG')
		 begin
		  if exists (select * from country where isocode='VG' and latitude=0 and zoom=0)
		   update country set latitude=18.4207,longitude=-64.64,zoom=10 where isocode='VG'
		 end
		
		if exists (select * from country where isocode='VI')
		 begin
		  if exists (select * from country where isocode='VI' and latitude=0 and zoom=0)
		   update country set latitude=18.3358,longitude=-64.8963,zoom=9 where isocode='VI'
		 end
		
		if exists (select * from country where isocode='WF')
		 begin
		  if exists (select * from country where isocode='WF' and latitude=0 and zoom=0)
		   update country set latitude=-14.3,longitude=-178.1,zoom=11 where isocode='WF'
		 end
		
		if exists (select * from country where isocode='EH')
		 begin
		  if exists (select * from country where isocode='EH' and latitude=0 and zoom=0)
		   update country set latitude=24.2155,longitude=-12.8858,zoom=6 where isocode='EH'
		 end
		
		if exists (select * from country where isocode='ZM')
		 begin
		  if exists (select * from country where isocode='ZM' and latitude=0 and zoom=0)
		   update country set latitude=-13.1339,longitude=27.8493,zoom=5 where isocode='ZM'
		 end
		
		if exists (select * from country where isocode='AR')
		 begin
		  if exists (select * from country where isocode='AR' and latitude=0 and zoom=0)
		   update country set latitude=-38.4161,longitude=-63.6167,zoom=3 where isocode='AR'
		 end
		
		if exists (select * from country where isocode='XK')
		 begin
		  if exists (select * from country where isocode='XK' and latitude=0 and zoom=0)
		   update country set latitude=42.6026,longitude=20.903,zoom=8 where isocode='XK'
		 end

	/*** END: populate new columns ***/
/*** END: move LocatorCountryDef contents to country table ***/

/*** Case 428806 set default locale for US and Canada ***/
update country set defaultlocale= 'en_US' where isoCode = 'US'
update country set defaultlocale= 'en_CA' where isoCode = 'CA'
/*** END Case 428806 set default locale for US and Canada ***/


/*** START: 2013-03-20 NYB Case 431040 Update missing CountryCurrency ***/
	update country set CountryCurrency=qry.CurrencyCode
	from country ctry,
	(select 'Albania' as CountryName,'Lek' as CurrencyName,'ALL' as CurrencyCode,N'Lek' as CurrencySymbol
	union select 'Afghanistan' as CountryName,'Afghani' as CurrencyName,'AFN' as CurrencyCode,N'؋' as CurrencySymbol
	union select 'Argentina' as CountryName,'Peso' as CurrencyName,'ARS' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Aruba' as CountryName,'Guilder' as CurrencyName,'AWG' as CurrencyCode,N'ƒ' as CurrencySymbol
	union select 'Australia' as CountryName,'Dollar' as CurrencyName,'AUD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Azerbaijan New' as CountryName,'Manat' as CurrencyName,'AZN' as CurrencyCode,N'ман' as CurrencySymbol
	union select 'Bahamas' as CountryName,'Dollar' as CurrencyName,'BSD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Barbados' as CountryName,'Dollar' as CurrencyName,'BBD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Belarus' as CountryName,'Ruble' as CurrencyName,'BYR' as CurrencyCode,N'p.' as CurrencySymbol
	union select 'Belize' as CountryName,'Dollar' as CurrencyName,'BZD' as CurrencyCode,N'BZ$' as CurrencySymbol
	union select 'Bermuda' as CountryName,'Dollar' as CurrencyName,'BMD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Bolivia' as CountryName,'Boliviano' as CurrencyName,'BOB' as CurrencyCode,N'$b' as CurrencySymbol
	union select 'Bosnia and Herzegovina' as CountryName,'Convertible Marka' as CurrencyName,'BAM' as CurrencyCode,N'KM' as CurrencySymbol
	union select 'Botswana' as CountryName,'Pula' as CurrencyName,'BWP' as CurrencyCode,N'P' as CurrencySymbol
	union select 'Bulgaria' as CountryName,'Lev' as CurrencyName,'BGN' as CurrencyCode,N'лв' as CurrencySymbol
	union select 'Brazil' as CountryName,'Real' as CurrencyName,'BRL' as CurrencyCode,N'R$' as CurrencySymbol
	union select 'Brunei Darussalam' as CountryName,'Dollar' as CurrencyName,'BND' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Cambodia' as CountryName,'Riel' as CurrencyName,'KHR' as CurrencyCode,N'៛' as CurrencySymbol
	union select 'Canada' as CountryName,'Dollar' as CurrencyName,'CAD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Cayman Islands' as CountryName,'Dollar' as CurrencyName,'KYD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Chile' as CountryName,'Peso' as CurrencyName,'CLP' as CurrencyCode,N'$' as CurrencySymbol
	union select 'China Yuan' as CountryName,'Renminbi' as CurrencyName,'CNY' as CurrencyCode,N'¥' as CurrencySymbol
	union select 'Colombia' as CountryName,'Peso' as CurrencyName,'COP' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Costa Rica' as CountryName,'Colon' as CurrencyName,'CRC' as CurrencyCode,N'₡' as CurrencySymbol
	union select 'Croatia' as CountryName,'Kuna' as CurrencyName,'HRK' as CurrencyCode,N'kn' as CurrencySymbol
	union select 'Cuba' as CountryName,'Peso' as CurrencyName,'CUP' as CurrencyCode,N'₱' as CurrencySymbol
	union select 'Czech Republic' as CountryName,'Koruna' as CurrencyName,'CZK' as CurrencyCode,N'Kč' as CurrencySymbol
	union select 'Denmark' as CountryName,'Krone' as CurrencyName,'DKK' as CurrencyCode,N'kr' as CurrencySymbol
	union select 'Dominican Republic' as CountryName,'Peso' as CurrencyName,'DOP' as CurrencyCode,N'RD$' as CurrencySymbol
	union select 'East Caribbean' as CountryName,'Dollar' as CurrencyName,'XCD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Egypt' as CountryName,'Pound' as CurrencyName,'EGP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'El Salvador' as CountryName,'Colon' as CurrencyName,'SVC' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Estonia' as CountryName,'Kroon' as CurrencyName,'EEK' as CurrencyCode,N'kr' as CurrencySymbol
	union select 'Euro Member' as CountryName,'Countries' as CurrencyName,'EUR' as CurrencyCode,N'€' as CurrencySymbol
	union select 'Falkland Islands (Malvinas)' as CountryName,'Pound' as CurrencyName,'FKP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Fiji' as CountryName,'Dollar' as CurrencyName,'FJD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Ghana' as CountryName,'Cedis' as CurrencyName,'GHC' as CurrencyCode,N'¢' as CurrencySymbol
	union select 'Gibraltar' as CountryName,'Pound' as CurrencyName,'GIP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Guatemala' as CountryName,'Quetzal' as CurrencyName,'GTQ' as CurrencyCode,N'Q' as CurrencySymbol
	union select 'Guernsey' as CountryName,'Pound' as CurrencyName,'GGP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Guyana' as CountryName,'Dollar' as CurrencyName,'GYD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Honduras' as CountryName,'Lempira' as CurrencyName,'HNL' as CurrencyCode,N'L' as CurrencySymbol
	union select 'Hong Kong' as CountryName,'Dollar' as CurrencyName,'HKD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Hungary' as CountryName,'Forint' as CurrencyName,'HUF' as CurrencyCode,N'Ft' as CurrencySymbol
	union select 'Iceland' as CountryName,'Krona' as CurrencyName,'ISK' as CurrencyCode,N'kr' as CurrencySymbol
	union select 'India' as CountryName,'Rupee' as CurrencyName,'INR' as CurrencyCode,N'' as CurrencySymbol
	union select 'Indonesia' as CountryName,'Rupiah' as CurrencyName,'IDR' as CurrencyCode,N'Rp' as CurrencySymbol
	union select 'Iran' as CountryName,'Rial' as CurrencyName,'IRR' as CurrencyCode,N'﷼' as CurrencySymbol
	union select 'Isle of Man' as CountryName,'Pound' as CurrencyName,'IMP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Israel' as CountryName,'Shekel' as CurrencyName,'ILS' as CurrencyCode,N'₪' as CurrencySymbol
	union select 'Jamaica' as CountryName,'Dollar' as CurrencyName,'JMD' as CurrencyCode,N'J$' as CurrencySymbol
	union select 'Japan' as CountryName,'Yen' as CurrencyName,'JPY' as CurrencyCode,N'¥' as CurrencySymbol
	union select 'Jersey' as CountryName,'Pound' as CurrencyName,'JEP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Kazakhstan' as CountryName,'Tenge' as CurrencyName,'KZT' as CurrencyCode,N'лв' as CurrencySymbol
	union select 'Korea (North)' as CountryName,'Won' as CurrencyName,'KPW' as CurrencyCode,N'₩' as CurrencySymbol
	union select 'Korea (South)' as CountryName,'Won' as CurrencyName,'KRW' as CurrencyCode,N'₩' as CurrencySymbol
	union select 'Kyrgyzstan' as CountryName,'Som' as CurrencyName,'KGS' as CurrencyCode,N'лв' as CurrencySymbol
	union select 'Laos' as CountryName,'Kip' as CurrencyName,'LAK' as CurrencyCode,N'₭' as CurrencySymbol
	union select 'Latvia' as CountryName,'Lat' as CurrencyName,'LVL' as CurrencyCode,N'Ls' as CurrencySymbol
	union select 'Lebanon' as CountryName,'Pound' as CurrencyName,'LBP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Liberia' as CountryName,'Dollar' as CurrencyName,'LRD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Lithuania' as CountryName,'Litas' as CurrencyName,'LTL' as CurrencyCode,N'Lt' as CurrencySymbol
	union select 'Macedonia' as CountryName,'Denar' as CurrencyName,'MKD' as CurrencyCode,N'ден' as CurrencySymbol
	union select 'Malaysia' as CountryName,'Ringgit' as CurrencyName,'MYR' as CurrencyCode,N'RM' as CurrencySymbol
	union select 'Mauritius' as CountryName,'Rupee' as CurrencyName,'MUR' as CurrencyCode,N'₨' as CurrencySymbol
	union select 'Mexico' as CountryName,'Peso' as CurrencyName,'MXN' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Mongolia' as CountryName,'Tughrik' as CurrencyName,'MNT' as CurrencyCode,N'₮' as CurrencySymbol
	union select 'Mozambique' as CountryName,'Metical' as CurrencyName,'MZN' as CurrencyCode,N'MT' as CurrencySymbol
	union select 'Namibia' as CountryName,'Dollar' as CurrencyName,'NAD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Nepal' as CountryName,'Rupee' as CurrencyName,'NPR' as CurrencyCode,N'₨' as CurrencySymbol
	union select 'Netherlands Antilles' as CountryName,'Guilder' as CurrencyName,'ANG' as CurrencyCode,N'ƒ' as CurrencySymbol
	union select 'New Zealand' as CountryName,'Dollar' as CurrencyName,'NZD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Nicaragua' as CountryName,'Cordoba' as CurrencyName,'NIO' as CurrencyCode,N'C$' as CurrencySymbol
	union select 'Nigeria' as CountryName,'Naira' as CurrencyName,'NGN' as CurrencyCode,N'₦' as CurrencySymbol
	union select 'Korea (North)' as CountryName,'Won' as CurrencyName,'KPW' as CurrencyCode,N'₩' as CurrencySymbol
	union select 'Norway' as CountryName,'Krone' as CurrencyName,'NOK' as CurrencyCode,N'kr' as CurrencySymbol
	union select 'Oman' as CountryName,'Rial' as CurrencyName,'OMR' as CurrencyCode,N'﷼' as CurrencySymbol
	union select 'Pakistan' as CountryName,'Rupee' as CurrencyName,'PKR' as CurrencyCode,N'₨' as CurrencySymbol
	union select 'Panama' as CountryName,'Balboa' as CurrencyName,'PAB' as CurrencyCode,N'B/.' as CurrencySymbol
	union select 'Paraguay' as CountryName,'Guarani' as CurrencyName,'PYG' as CurrencyCode,N'Gs' as CurrencySymbol
	union select 'Peru Nuevo' as CountryName,'Sol' as CurrencyName,'PEN' as CurrencyCode,N'S/.' as CurrencySymbol
	union select 'Philippines' as CountryName,'Peso' as CurrencyName,'PHP' as CurrencyCode,N'₱' as CurrencySymbol
	union select 'Poland' as CountryName,'Zloty' as CurrencyName,'PLN' as CurrencyCode,N'zł' as CurrencySymbol
	union select 'Qatar' as CountryName,'Riyal' as CurrencyName,'QAR' as CurrencyCode,N'﷼' as CurrencySymbol
	union select 'Romania New' as CountryName,'Leu' as CurrencyName,'RON' as CurrencyCode,N'lei' as CurrencySymbol
	union select 'Russia' as CountryName,'Ruble' as CurrencyName,'RUB' as CurrencyCode,N'руб' as CurrencySymbol
	union select 'Saint Helena' as CountryName,'Pound' as CurrencyName,'SHP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Saudi Arabia' as CountryName,'Riyal' as CurrencyName,'SAR' as CurrencyCode,N'﷼' as CurrencySymbol
	union select 'Serbia' as CountryName,'Dinar' as CurrencyName,'RSD' as CurrencyCode,N'Дин.' as CurrencySymbol
	union select 'Seychelles' as CountryName,'Rupee' as CurrencyName,'SCR' as CurrencyCode,N'₨' as CurrencySymbol
	union select 'Singapore' as CountryName,'Dollar' as CurrencyName,'SGD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Solomon Islands' as CountryName,'Dollar' as CurrencyName,'SBD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Somalia' as CountryName,'Shilling' as CurrencyName,'SOS' as CurrencyCode,N'S' as CurrencySymbol
	union select 'South Africa' as CountryName,'Rand' as CurrencyName,'ZAR' as CurrencyCode,N'R' as CurrencySymbol
	union select 'Korea (South)' as CountryName,'Won' as CurrencyName,'KRW' as CurrencyCode,N'₩' as CurrencySymbol
	union select 'Sri Lanka' as CountryName,'Rupee' as CurrencyName,'LKR' as CurrencyCode,N'₨' as CurrencySymbol
	union select 'Sweden' as CountryName,'Krona' as CurrencyName,'SEK' as CurrencyCode,N'kr' as CurrencySymbol
	union select 'Switzerland' as CountryName,'Franc' as CurrencyName,'CHF' as CurrencyCode,N'CHF' as CurrencySymbol
	union select 'Suriname' as CountryName,'Dollar' as CurrencyName,'SRD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Syria' as CountryName,'Pound' as CurrencyName,'SYP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'Taiwan New' as CountryName,'Dollar' as CurrencyName,'TWD' as CurrencyCode,N'NT$' as CurrencySymbol
	union select 'Thailand' as CountryName,'Baht' as CurrencyName,'THB' as CurrencyCode,N'฿' as CurrencySymbol
	union select 'Trinidad and Tobago' as CountryName,'Dollar' as CurrencyName,'TTD' as CurrencyCode,N'TT$' as CurrencySymbol
	union select 'Turkey' as CountryName,'Lira' as CurrencyName,'TRY' as CurrencyCode,N'' as CurrencySymbol
	union select 'Turkey' as CountryName,'Lira' as CurrencyName,'TRL' as CurrencyCode,N'₤' as CurrencySymbol
	union select 'Tuvalu' as CountryName,'Dollar' as CurrencyName,'TVD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Ukraine' as CountryName,'Hryvna' as CurrencyName,'UAH' as CurrencyCode,N'₴' as CurrencySymbol
	union select 'United Kingdom' as CountryName,'Pound' as CurrencyName,'GBP' as CurrencyCode,N'£' as CurrencySymbol
	union select 'United States' as CountryName,'Dollar' as CurrencyName,'USD' as CurrencyCode,N'$' as CurrencySymbol
	union select 'Uruguay' as CountryName,'Peso' as CurrencyName,'UYU' as CurrencyCode,N'$U' as CurrencySymbol
	union select 'Uzbekistan' as CountryName,'Som' as CurrencyName,'UZS' as CurrencyCode,N'лв' as CurrencySymbol
	union select 'Venezuela' as CountryName,'Bolivar' as CurrencyName,'VEF' as CurrencyCode,N'Bs' as CurrencySymbol
	union select 'Viet Nam' as CountryName,'Dong' as CurrencyName,'VND' as CurrencyCode,N'₫' as CurrencySymbol
	union select 'Yemen' as CountryName,'Rial' as CurrencyName,'YER' as CurrencyCode,N'﷼' as CurrencySymbol
	union select 'Zimbabwe' as CountryName,'Dollar' as CurrencyName,'ZWD' as CurrencyCode,N'Z$' as CurrencySymbol
	) as qry 
	where ctry.CountryCurrency is null and ctry.CountryDescription=qry.CountryName 
/*** END: 2013-03-20 NYB Case 431040 Update missing CountryCurrency ***/
	

GO
	
	/* 2013-10-23 WAB CASE 437445 
		Set defaultLanguageID for Japan
		(it is one that can't be guessed from the browser )
	 */
	update country 
	set defaultlanguageid = (select languageid from language where language = 'japanese')
	where isocode = 'JP' and isnull(defaultlanguageid,'') = ''
	 
/*** START: 2014-02-06 NYB Case 438603 add postal code format ***/
if not exists (select * from information_schema.columns where table_name='country' and column_name='PostalCodeFormat')
	alter table country add PostalCodeFormat [varchar](200) NULL
GO
if exists (select * from country where ISOCode='AU' and PostalCodeFormat is null)
	update country set PostalCodeFormat='^[0-9]{4}$' where ISOCode='AU'
if exists (select * from country where ISOCode='GB' and PostalCodeFormat is null)
	update country set PostalCodeFormat='^[A-Za-z]{2}[0-9]{2}[A-Za-z]{2}$|^[A-Za-z]{2}[0-9]{3}[A-Za-z]{2}$|^[A-Za-z]{2}[0-9]{1}[A-Za-z]{1}[0-9]{1}[A-Za-z]{2}$' where ISOCode='GB'
if exists (select * from country where ISOCode='IT' and PostalCodeFormat is null)
	update country set PostalCodeFormat='^[0-9]{5}$' where ISOCode='IT'
if exists (select * from country where ISOCode='US' and PostalCodeFormat is null)
	update country set PostalCodeFormat='^[0-9]{5}[-][0-9]{4}$' where ISOCode='US'
/*** END: 2014-02-06 NYB Case 438603 add postal code mask ***/

/* NJH 2015/01/23 insert schema record for country if it doesn't exist */
if not exists (select 1 from schematableBase where entityTypeID=507)
INSERT INTO [dbo].[schemaTableBase] ([entityTypeID], [entityName], [NameExpression], [UniqueKey], [Description], [FlagsExist], [mnemonic], [ScreensExist], [CustomEntity]) VALUES (507, 'currency', NULL, N'currencyISOCode', NULL, 0, null, 0, 0)
