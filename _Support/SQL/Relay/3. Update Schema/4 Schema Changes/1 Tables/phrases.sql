/*  WAB 2011/05/11 performance improvements */

if exists (select * from sysindexes where name = 'pk___1__10')
	alter table phrases drop constraint pk___1__10

GO
if not exists (select * from sysindexes where name = 'pk_phrases')
	ALTER TABLE [dbo].[Phrases] ADD  CONSTRAINT [PK_Phrases] PRIMARY KEY CLUSTERED 
	(
		[Phraseid] ASC,
		[Languageid] ASC,
		[CountryID] ASC
	) 


if not exists (select * from sysindexes where name = 'ix_phrases_ident')
CREATE UNIQUE NONCLUSTERED INDEX IX_Phrases_Ident ON dbo.Phrases
	(
	Ident
	) 
GO


if not exists (select 1 from information_schema.columns where table_name='phrases' and column_name='phraseText' and data_type='nvarchar' and character_maximum_length = -1)
begin
alter table phrases alter column phraseText nvarchar(max)
alter table xphrases alter column phraseText nvarchar(max)
end
GO