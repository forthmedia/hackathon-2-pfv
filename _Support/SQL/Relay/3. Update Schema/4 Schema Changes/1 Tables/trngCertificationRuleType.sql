/* add sortOrder to trngCertificationRule */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationRule' AND column_name='certificationRuleOrder') 
ALTER TABLE trngCertificationRule add certificationRuleOrder int NULL
GO
/* 2015/12/20 PYW P-TAT006 BRD 31 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationRuleType')
BEGIN
	IF (SELECT COUNT (*) FROM trngCertificationRuleType WHERE ruleTypeTextID = 'Retest') = 0
	BEGIN
		INSERT INTO trngCertificationRuleType
		(Description,ruleTypeTextID,created,createdBy,lastUpdated,lastUpdatedBy)
		VALUES
		('Retest','Retest',GETDATE(),4,GETDATE(),4)
	END
END
GO