
if not exists (select 1 from sysobjects where name='broadcast' and type='u')
Begin
CREATE TABLE [dbo].[broadcast](
	[broadcastID] [int] IDENTITY(1,1) NOT NULL,
	[accountID] [int] NOT NULL,
	[text] [nvarchar](140) NOT NULL,
	[sendDate] [datetime] NULL,
	[sentDate] [datetime] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_broadcast] PRIMARY KEY CLUSTERED 
(
	[broadcastID] ASC
) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[broadcast] ADD  CONSTRAINT [DF_broadcast_created]  DEFAULT (getdate()) FOR [created]

ALTER TABLE [dbo].[broadcast] ADD  CONSTRAINT [DF_broadcast_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]

End
GO


if exists (select 1 from information_schema.columns where table_name='broadcast' and column_name='accountID')
exec sp_rename 'broadcast.accountID','broadcastAccountID'

if not exists (select 1 from information_schema.columns where table_name='broadcast' and column_name='messageID')
alter table broadcast add messageID int null

if exists (select 1 from information_schema.columns where table_name='broadcast' and column_name='text')
alter table broadcast alter column [text] nvarchar(140) null

if not exists (select 1 from information_schema.columns where table_name = 'broadcast' and column_name = 'campaignID')
	alter table broadcast add campaignID int null
GO