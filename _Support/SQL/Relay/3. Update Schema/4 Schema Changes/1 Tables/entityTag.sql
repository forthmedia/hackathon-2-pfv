if not exists (select 1 from sysobjects where name='entityTag' and type='u')
BEGIN
CREATE TABLE [dbo].[entityTag](
	[entityTypeID] [int] NOT NULL,
	[entityID] [int] NOT NULL,
	[personID] [int] NOT NULL,
	[created] [datetime] default getDate(),
	PRIMARY KEY (entityTypeID,entityID,personID) 
) 
END
GO