if not exists (select 1 from sysobjects where name='dataLoadFunctions' and type='u')
begin
CREATE TABLE [dbo].[dataLoadFunctions](
	[dataLoadFunctionID] [int] IDENTITY(1,1) NOT NULL,
	[functionName] [varchar](80) NOT NULL,
	[functionTextID] [varchar](25) NOT NULL,
	[URL] [varchar](250) NULL,
	[description] [varchar](750) NOT NULL,
	[dataLoadType] [varchar](20) NULL,
	[Created] [datetime] NOT NULL,
	[functionResultText] [varchar](30) NULL,
	[sortOrder] [int] NULL,
	[functionType] [nvarchar](50) NULL,
	[openInTab] [bit] NULL,
 CONSTRAINT [PK_dataLoadFunctionID] PRIMARY KEY CLUSTERED 
(
	[dataLoadFunctionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [functionTextID_unique] UNIQUE NONCLUSTERED 
(
	[functionTextID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[dataLoadFunctions] ADD  DEFAULT (getdate()) FOR [Created]

END
GO