if not exists (select 1 from sysobjects where name='processJumpAction' and type='u')
begin
CREATE TABLE [dbo].[processJumpAction](
	[processJumpActionID] [int] IDENTITY(1,1) NOT NULL,
	[JumpAction] [varchar](20) NULL,
	[JumpActionDesc] [varchar](80) NULL,
	[DefaultJumpExpression] [varchar](50) NULL,
	[DefaultJumpValue] [varchar](100) NULL,
 CONSTRAINT [PK__processJumpAction] PRIMARY KEY CLUSTERED 
(
	[processJumpActionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End

