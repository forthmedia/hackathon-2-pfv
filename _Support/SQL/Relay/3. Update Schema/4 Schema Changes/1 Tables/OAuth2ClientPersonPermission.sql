
IF object_id('OAuth2ClientPersonPermission') is null  
BEGIN
	CREATE TABLE dbo.OAuth2ClientPersonPermission
	(
		clientIdentifier nVarChar(256) NOT NULL FOREIGN KEY REFERENCES OAuth2Client(clientIdentifier) ON DELETE CASCADE,
		personID int NOT NULL FOREIGN KEY REFERENCES person (personID) ON DELETE CASCADE
	);
End