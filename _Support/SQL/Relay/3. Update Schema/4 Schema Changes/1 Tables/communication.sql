/* LID 4918 */
if not exists (SELECT * FROM information_schema.columns where table_name='Communication' and column_name='CommEmailFormatID')
	ALTER TABLE Communication ADD CommEmailFormatID int NULL 

/* 2011/05/18 PPB LID6620 */
/* 2011/07/04 NYB P-LEN024 */
/* 2012/03/06 RMB P-LEN044 - CR-069 */
ALTER TABLE communication alter column description nvarchar(200) NULL
if not exists (select top 1 1 from information_schema.columns where table_name='Communication' and column_name='TrackEmailWithOmniture')
	ALTER TABLE communication add TrackEmailWithOmniture [bit] NOT NULL default(0)

if not exists (SELECT * FROM information_schema.columns where column_name='LinksReviewed' and table_name='communication')
	alter table communication add LinksReviewed bit not null default(0)
if not exists (SELECT * FROM information_schema.columns where column_name='PreviewReviewed' and table_name='communication')
	alter table communication add PreviewReviewed bit not null default(0)

/* start:  2011/06/27 NYB LID7003 */
if exists (select top 1 1 from information_schema.columns where table_name='communication' and data_type in ('varchar'))
begin
	declare @locSql nvarchar(4000),@columnName varchar(300),@constraintName varchar(300),@named_constraint varchar(300),@column_default nvarchar(4000),@character_maximum_length varchar(300),@data_type varchar(300),@is_nullable as varchar(10),@test bit,@tableName varchar(200);
	set @locSql = '';
	set @test=0;
	set @tableName = 'communication'
	declare c CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
		select cols.name as columnName,constraintObj.name as constraintName,
		case when column_default is null or left(ltrim(column_default),1)='(' then 0 else 1 end as named_constraint,
		column_default,character_maximum_length,
		colsV.data_type,colsV.is_nullable 
		from sysobjects tableObj 
		inner join syscolumns cols on cols.id=tableObj.id 
		inner join information_schema.columns colsV on cols.name=colsV.column_name and tableObj.name=colsV.table_name 
		left join sysobjects constraintObj on cols.cdefault=constraintObj.id 
		where tableObj.name=@tableName and data_type in ('varchar')
	open c
	fetch c into @columnName,@constraintName,@named_constraint,@column_default,@character_maximum_length ,@data_type,@is_nullable
	while (@@fetch_status <> -1)
	begin
	    if (@@fetch_status <> -2) 
			begin 
				if @constraintName is not null
					begin
						if @named_constraint = 1
							begin 
								set @locSql = 'EXEC sp_unbindefault ''['+@tableName+'].['+@columnName+']''';
								if @test = 0
									exec sp_executesql @stmt= @locSql
								else
									print '/*1*/ '+@locSql
							end
						else
							begin
								set @locSql = 'alter table '+@tableName+' drop CONSTRAINT '+@constraintName
								if @test = 0
									exec sp_executesql @stmt= @locSql
								else
									print '/*2*/ '+@locSql
							end
					end
				set @locSql = 'alter table '+@tableName+' alter column '+@columnName+' n'+@data_type+'('+@character_maximum_length+')'
				if @is_nullable ='NO'
					set @locSql = @locSql+' NOT'
				set @locSql = @locSql+' NULL'
				if @test = 0
					exec sp_executesql @stmt= @locSql
				else
					print '/*3*/ '+@locSql
				if @constraintName is not null and @named_constraint = 0
					begin
						set @locSql = 'alter table '+@tableName+' add default'+@column_default+' for '+@columnName
						if @test = 0
							exec sp_executesql @stmt= @locSql
						else
							print '/*4*/ '+@locSql
					end
				if @constraintName is not null and @named_constraint = 1
					begin
						set @locSql = 'EXEC sp_bindefault '''+@constraintName+''', '''+@tableName+'.'+@columnName+''';'
						if @test = 0
							exec sp_executesql @stmt= @locSql
						else
							print '/*5*/ '+@locSql
					end
			end
	    fetch c INTO @columnName,@constraintName,@named_constraint,@column_default,@character_maximum_length,@data_type,@is_nullable
	end
	close c
end
/* end:  2011/06/27 NYB LID7003 */

/* start:  2012/03/06 RMB P-LEN044 - CR-069 */
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
          WHERE TABLE_NAME = 'Communication' 
          AND  COLUMN_NAME = 'CharacterSet')

begin

	ALTER TABLE [dbo].[Communication]
	ADD
		[CharacterSet] [nvarchar](10) NULL

	ALTER TABLE [dbo].[Communication] ADD CONSTRAINT
		[DF_Communication_CharacterSet]  DEFAULT (N'UTF-8') FOR [CharacterSet]
	
end
GO

UPDATE [dbo].[Communication] SET [CharacterSet] = N'UTF-8'
WHERE ([CharacterSet] IS NULL OR LTRIM(RTRIM([CharacterSet])) = '')
GO
/* end:  2012/03/06 RMB P-LEN044 - CR-069 */

/* start 2012/05/18 IH Case 426819 */
IF NOT EXISTS (select * from sysindexes
where id=object_id('Communication') and name='idx_commtypelid_commformlid')
CREATE NONCLUSTERED INDEX [idx_commtypelid_commformlid] 
ON [dbo].[Communication] ([CommTypeLID],[CommFormLID])
INCLUDE ([CommID],[Title],[countryIDList])

IF NOT EXISTS (select * from sysindexes
where id=object_id('CommSelection') and name='idx_commselectlid_sent')
CREATE NONCLUSTERED INDEX [idx_commselectlid_sent]
ON [dbo].[CommSelection] ([CommSelectLID],[sent])
INCLUDE ([CommID],[sentDate])
/* end 2012/05/18 IH Case 426819 */
/* start:  2012/11/19 YMA CASE:432115 */
ALTER TABLE [dbo].[Communication]
ALTER COLUMN [Linktositeurl] [nvarchar](100)
GO
/* end:  2012/11/19 YMA CASE:432115 */

/* NJH 2013Roadmap */
if exists(select * from information_schema.columns where column_name='upsize_ts' and table_name='Communication')
alter table Communication drop column upsize_ts
GO

if not exists (select top 1 1 from information_schema.columns where table_name='Communication' and column_name='sendMessage')
BEGIN
	ALTER TABLE communication add sendMessage [bit] NOT NULL default(1)
	/* 
		WAB CASE 439039  - all existing comms will not have messages so must have sendMessage set back to 0
		Need to do as an exec otherwise SQL compiler complains that sendMessage does not exist yet  
	 */
	declare @sql nvarchar(max) = 'UPDATE communication set sendMessage = 0'
	exec (@SQL)	
END	
GO
if not exists (select 1 from information_schema.columns where table_name='Communication' and column_name='styleSheet')
	ALTER TABLE communication add [styleSheet] [nvarchar](255) NULL
GO
if not exists (select 1 from information_schema.columns where table_name='Communication' and column_name='commFromDisplayNameID')
	ALTER TABLE communication add [commFromDisplayNameID] [smallint] NULL
GO

if exists (select 1 from information_schema.columns where table_name='communication' and column_name='Notes' and data_type!='nvarchar')
ALTER TABLE communication ALTER COLUMN [Notes] [nvarchar](max)
GO

exec generate_MRAudit @TableName = 'Communication'