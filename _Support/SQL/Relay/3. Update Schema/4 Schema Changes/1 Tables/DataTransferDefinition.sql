if not exists (select * from sysobjects where name='DataTransferDefinition')
begin
CREATE TABLE [dbo].[DataTransferDefinition](
	[DtdID] [int] IDENTITY(1,1) NOT NULL,
	[DataTransferType] [int] NOT NULL,
	[DirectionTypeID] [int] NOT NULL,
	[FunctionName] [varchar](50) NOT NULL,
	[Filename] [varchar](50) NOT NULL,
	[FilenameSuffix] [varchar](50) NULL,
	[DateMask] [varchar](20) NULL,
	[StartPoint] [varchar](50) NULL,
	[HistoricFileQty] [int] NOT NULL CONSTRAINT [DF__DataTrans__Histo__5EAAD05B]  DEFAULT (0),
	[GenerateOkFile] [bit] NOT NULL CONSTRAINT [DF__DataTrans__Gener__5F9EF494]  DEFAULT (0),
	[executionInterval] [int] NOT NULL,
	[RecordsPerFile] [int] NULL,
	[ParentID] [int] NULL,
	[fileextension] [varchar](10) NULL CONSTRAINT [DF__DataTrans__filee__74EF1735]  DEFAULT ('csv'),
	[OKFileType] [varchar](50) NOT NULL CONSTRAINT [DF__DataTrans__OKFil__1CFD088F]  DEFAULT ('bytes'),
	[moveProcessedFile] [bit] NULL DEFAULT (0),
 CONSTRAINT [PK_DataTransferDefinition] PRIMARY KEY CLUSTERED 
(
	[DtdID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
end
GO


-- add new column to dataTransferDefinition
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'dataTransferDefinition' AND column_name='moveProcessedFile') 
ALTER TABLE dataTransferDefinition add moveProcessedFile bit default 0
GO
