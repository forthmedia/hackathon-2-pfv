IF NOT EXISTS (select * from sysindexes where id=object_id('SelectionTag') and name='IX_SelectionTag_ID_Status')
BEGIN
	CREATE NONCLUSTERED INDEX IX_SelectionTag_ID_Status
	ON [dbo].[SelectionTag] ([SelectionID],[Status])
	INCLUDE ([EntityID])
END

if not exists(select 1 from information_schema.columns where table_name='selectionTag' and column_name='lastUpdatedByPerson')
BEGIN
alter table selectionTag add lastUpdatedByPerson int null
END