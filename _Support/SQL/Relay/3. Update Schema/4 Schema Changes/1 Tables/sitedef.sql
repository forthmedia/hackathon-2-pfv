/* 2012-12-14 PPB Case 430230 increase size of column livelanguageids to 200 (if it is currently less)*/
if exists (select * from information_schema.columns where table_name='sitedef' and column_name='livelanguageids' and character_maximum_length < 200)
	alter table sitedef alter column livelanguageids varchar(200)
	
alter table siteDef alter column stylesheet varchar(max)


	
/* WAB 2014-12-16 CASE 443133 add siteOffLineAllowedIPs column 
Note that when this column is added the editing of the sitedef table might fail.  Can be resolved with a structDelete(application.tableDefinition,"siteDef") to clear cache
*/
IF not exists (select * from information_schema.columns where table_name='sitedef' and column_name='siteOffLineAllowedIPs')
BEGIN
	alter table sitedef add siteOffLineAllowedIPs varchar(max)
END	


/* WAB 2014-12-16 Get rid of hese columns which were only there for an experiment */
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'testOfAStructure_Key2_subkey2'
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'testOfAStructure_Key2_subkey1'
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'testOfAStructure_key1'
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'egEmailFromAddress'
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'egEmailFromName'
exec sp_dropcolumn @tablename = 'sitedef', @columnName = 'egSupportEmail'

