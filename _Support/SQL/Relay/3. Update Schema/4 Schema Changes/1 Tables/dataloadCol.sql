if not exists (select 1 from sysobjects where name='dataLoadCol' and type='u')
begin
CREATE TABLE [dbo].[dataLoadCol](
	[dataLoadColID] [int] IDENTITY(1,1) NOT NULL,
	[dataLoadColumnName] [varchar](50) NOT NULL,
	[alternativeColumnName] [varchar](1000) NULL,
	[loadThisColumn] [bit] NULL,
	[relayColumn] [varchar](50) NULL,
	[columnDataType] [varchar](50) NULL,
	[ControlColumn] [bit] NOT NULL,
	[loadTypeGroup] [varchar](50) NULL,
 CONSTRAINT [PK_dataLoadCol] PRIMARY KEY CLUSTERED 
(
	[dataLoadColID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[dataLoadCol] ADD  DEFAULT ((0)) FOR [loadThisColumn]

ALTER TABLE [dbo].[dataLoadCol] ADD  DEFAULT ((0)) FOR [ControlColumn]

END
GO