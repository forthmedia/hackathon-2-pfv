/* 2014/11/25	YMA	Modify CJM data tables */


if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMTemplate')
begin
	alter table CJMTemplate drop column CJMTemplate
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMPrintWidth')
begin
	alter table CJMTemplate drop column CJMPrintWidth
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMPrintHeight')
begin
	alter table CJMTemplate drop column CJMPrintHeight
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMPrintDPI')
begin
	alter table CJMTemplate drop column CJMPrintDPI
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMHeight')
begin
	alter table CJMTemplate drop column CJMHeight
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMWidth')
begin
	alter table CJMTemplate drop column CJMWidth
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='Approval')
begin
	alter table CJMTemplate drop column Approval
end
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMHTMLWrapper')
begin
	alter table CJMTemplate drop constraint DF_CJMTemplate_CJMHTMLwrapper

	alter table CJMTemplate drop column CJMHTMLWrapper
end
GO

if not exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='Webable')
begin
	alter table CJMTemplate add Webable bit
end
GO

if not exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='jsonContent')
begin
	alter table CJMTemplate add jsonContent nvarchar(max)
end
GO

if not exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='jsonStoredFiles')
begin
	alter table CJMTemplate add jsonStoredFiles nvarchar(max)
end
GO

if not exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='hasRecordRights_1')
begin
	alter table CJMTemplate add hasRecordRights_1 bit default 0
end
GO

if not exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='hasCountryScope_1')
begin
	alter table CJMTemplate add hasCountryScope_1 bit default 0
end
GO

DECLARE @SQL NVARCHAR(MAX) = N'';

SELECT @SQL += N'
ALTER TABLE ' + OBJECT_NAME(PARENT_OBJECT_ID) + ' DROP CONSTRAINT ' + OBJECT_NAME(OBJECT_ID) + ';' 
FROM SYS.OBJECTS
WHERE TYPE_DESC LIKE 'DEFAULT_CONSTRAINT' AND OBJECT_NAME(PARENT_OBJECT_ID) = 'CJMTemplate' AND NAME != 'DF_CJMTemplate_lastUpdated';

EXECUTE sp_executesql @sql
GO

ALTER TABLE [dbo].[CJMTemplate] ADD  CONSTRAINT [DF_CJMTemplate_Printable]  DEFAULT ((1)) FOR [Printable]
GO
ALTER TABLE [dbo].[CJMTemplate] ADD  CONSTRAINT [DF_CJMTemplate_Active]  DEFAULT ((0)) FOR [Active]
GO
ALTER TABLE [dbo].[CJMTemplate] ADD  CONSTRAINT [DF_CJMTemplate_PDFable]  DEFAULT ((1)) FOR [PDFable]
GO
ALTER TABLE [dbo].[CJMTemplate] ADD  CONSTRAINT [DF_CJMTemplate_Webable]  DEFAULT ((1)) FOR [Webable]
GO

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMTemplateLabel')
begin
	alter table CJMTemplate drop column CJMTemplateLabel
end
if not exists (select 1 from information_schema.columns where table_name = 'CJMTemplate' and column_name = 'title_defaultTranslation') 
begin
	alter table CJMTemplate add title_defaultTranslation nVarChar(50)
end

if exists (select 1 from information_schema.columns where table_name='CJMTemplate' and column_name='CJMTemplateDesc')
begin
	alter table CJMTemplate drop column CJMTemplateDesc
end
if not exists (select 1 from information_schema.columns where table_name = 'CJMTemplate' and column_name = 'description_defaultTranslation') 
begin
	alter table CJMTemplate add description_defaultTranslation nVarChar(50)
end

/* run this after the changes have been made */
exec generate_MRAuditDel @tablename = 'CJMTemplate'