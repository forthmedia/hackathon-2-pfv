/* 2011/11/22	RMB		Added DefaultDeliveryMethod defalt set to 'Local' */

ALTER TABLE fileType ALTER COLUMN type nvarchar(50) NULL
GO

ALTER TABLE fileType ALTER COLUMN path nvarchar(50) NULL
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
          WHERE TABLE_NAME = 'filetype' 
          AND  COLUMN_NAME = 'DefaultDeliveryMethod')

begin

ALTER TABLE dbo.filetype 
ADD DefaultDeliveryMethod VARCHAR(50) NULL DEFAULT 'Local'

end
GO

UPDATE dbo.filetype SET DefaultDeliveryMethod = 'Local'
WHERE (DefaultDeliveryMethod IS NULL OR LTRIM(RTRIM(DefaultDeliveryMethod)) = '')
GO

if not exists(select 1 from information_schema.columns where table_name='fileType' and column_name='created')
alter table dbo.fileType add created datetime not null default(getDate())
GO

if not exists(select 1 from information_schema.columns where table_name='fileType' and column_name='createdBy')
alter table dbo.fileType add createdBy int not null default(0)
GO




/* 2016-02-24		WAB 	PROD2015-291  Media Library Visibility.
 get rid of the hasRecordRights_x columns and replace with a single hasRecordRightsBitMask field and populate it

 */

if exists (select * from information_schema.columns where table_name = 'fileType' and column_Name = 'hasRecordRights_1')
	exec sp_dropcolumn 'fileType', 'hasRecordRights_1'
if exists (select * from information_schema.columns where table_name = 'fileType' and column_Name = 'hasRecordRights_2')
	exec sp_dropcolumn 'fileType', 'hasRecordRights_2'

if not exists (select * from information_schema.columns where table_name = 'fileType' and column_Name = 'hasRecordRightsBitMask')
BEGIN
	alter table fileType add hasRecordRightsBitMask int default 0  

	declare @SQL nvarchar(max) = '
		IF exists (select 1 from information_schema.tables where table_name = ''fileTypeDel'')
			alter table fileTypeDel add hasRecordRightsBitMask int 
	'		
	exec (@SQL)

	Set  @SQL  = '
		update fileType set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
		alter table fileType alter column hasRecordRightsBitMask int not null
	'	
	exec (@SQL)
END	


if not exists (select * from information_schema.columns where table_name = 'fileType' and column_Name = 'allowedFiles')
	alter table fileType add allowedFiles varchar(50) default null
GO


/* WAB 2016-03-01 PROD2016-509 Added addWatermark */
if not exists (select * from information_schema.columns where table_name = 'fileType' and column_Name = 'addWatermark')
	alter table fileType add addWaterMark bit not null default 0

/*BF-521 RJT Allow a longer list of allowed file types */	
ALTER TABLE filetype ALTER COLUMN allowedFiles varchar(500)


