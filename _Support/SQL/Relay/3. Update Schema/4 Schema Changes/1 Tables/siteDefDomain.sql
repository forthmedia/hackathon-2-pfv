if not exists (select 1 from sysobjects where name = 'siteDefDomain')
BEGIN
	CREATE TABLE [dbo].[siteDefDomain](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[siteDefID] [int] NOT NULL,
		[testsite] [smallint] NOT NULL,
		[domain] [varchar](100) NOT NULL,
		[maindomain] [bit] NOT NULL,
		[reciprocalid] [int] NULL,
	CONSTRAINT [PK_siteDefDomain] PRIMARY KEY CLUSTERED 
	(
		[domain] ASC
	)
	) ON [PRIMARY]

	
	ALTER TABLE [dbo].[siteDefDomain] ADD  CONSTRAINT [DF_siteDefDomains_maindomain]  DEFAULT ((0)) FOR [maindomain]
	
END

GO

if not exists(select 1 from information_schema.columns where table_name='siteDefDomain' and column_name='active')
alter table siteDefDomain add active bit not null default(1)
GO





