
-- add additionalReference field
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'cashbackClaimItem' AND column_name='additionalReference') 
alter table cashbackClaimItem add additionalReference varchar(40) null
GO