
/* NJH 2012/11/19 CASE 432093  - move the primary key from the compound and create a new identity column. Had to do this for modRegister*/
if not exists (select 1 from information_schema.columns where table_name='trngModuleSetModule' and column_name='trngModuleSetModuleID')  
alter table TrngModuleSetModule add trngModuleSetModuleID int not null identity

/* drop the existing primary key where the key is made up of modulesetId and moduleID */
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME and ku.table_name='trngModuleSetModule' where column_name='trngModuleSetModuleID')
IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[trngModuleSetModule]') AND name = N'PK_trngModuleSetModule')
ALTER TABLE [dbo].[trngModuleSetModule] DROP CONSTRAINT [PK_trngModuleSetModule]
GO

/* create a new primary key */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[trngModuleSetModule]') AND name = N'PK_trngModuleSetModule')
ALTER TABLE [dbo].[trngModuleSetModule] ADD  CONSTRAINT [PK_trngModuleSetModule] PRIMARY KEY CLUSTERED 
(
	[trngModuleSetModuleID] ASC
) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name='IX_trngModuleSetModule_moduleSet_module' AND object_id = OBJECT_ID('trngModuleSetModule'))
CREATE UNIQUE NONCLUSTERED INDEX [IX_trngModuleSetModule_moduleSet_module] ON [dbo].[trngModuleSetModule] 
(
	[moduleSetID] ASC,
	[moduleID] ASC
) ON [PRIMARY]
GO