/*********************
2012/03/27 RMB added mobileCompatible
**********************/

if not exists(select 1 from information_schema.columns where table_name='trngsolutionArea' and column_name = 'mobileCompatible')
-- RMB added the mobileCompatible column to the trngsolutionArea table
alter table trngsolutionArea add mobileCompatible bit not null default 0
GO