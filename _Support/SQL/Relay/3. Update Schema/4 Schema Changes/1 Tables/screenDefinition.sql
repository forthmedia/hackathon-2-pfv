-- increase the size of evalString column as in some cases we may need to store large number of characters (i.e. SQL)
ALTER TABLE ScreenDefinition ALTER COLUMN EVALSTRING NVARCHAR (4000)

if (not exists (select 1 from information_schema.columns where column_name='method' and table_name='ScreenDefinition' and character_maximum_length = 250))
begin
	declare @defaultConstName sysname = null

	select @defaultConstName = d.name
		  from sys.all_columns c
		  join sys.tables t on t.object_id = c.object_id
		  join sys.schemas s on s.schema_id = t.schema_id
		  join sys.default_constraints d on c.default_object_id = d.object_id
		where t.name = 'screenDefinition'
		  and c.name = 'method'
		  and s.name = 'dbo'
    
    declare @sql nvarchar(max)
    set @sql = 
		'alter table ScreenDefinition drop constraint '+@defaultConstName+'
		alter table ScreenDefinition alter column method nvarchar(250)
		alter table ScreenDefinition add constraint [df_screenDefinition_method] default (''view'') for method'

	if (@defaultConstName is not null)
	begin
		exec (@sql)
	end
end

if not exists(select 1 from information_schema.columns where table_name='screenDefinition' and column_name='notes')
alter table screenDefinition add notes nvarchar(max)

if not exists(select 1 from information_schema.columns where table_name='screenDefinition' and column_name='lastUpdatedByPerson')
alter table screenDefinition add lastUpdatedByPerson int null

/* NJH 2016/12/01 - added auditing */
exec generate_MRAudit @TableName = 'screenDefinition'