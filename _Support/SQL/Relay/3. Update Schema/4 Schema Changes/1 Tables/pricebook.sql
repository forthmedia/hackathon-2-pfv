
/* add pricebook table */
if not exists (select 1 from information_schema.tables where table_name = 'Pricebook')
CREATE TABLE [dbo].[Pricebook](
	[pricebookID] [int] IDENTITY(1,1) NOT NULL,
	[organisationID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[countryID] [int] NOT NULL,
	[currency] [char](3) NOT NULL,
	[startDate] [datetime] NOT NULL,
	[endDate] [datetime] NULL,
	[campaignID] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_Pricebook_active]  DEFAULT (1),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_Pricebook_deleted]  DEFAULT (0),
	[created] [datetime] NOT NULL CONSTRAINT [DF_Pricebook_created]  DEFAULT (getdate()),
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Pricebook_lastUpdated]  DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL,
	[crmPricebookID] [varchar](50) NULL,
 CONSTRAINT [PK_Pricebook] PRIMARY KEY CLUSTERED 
(
	[pricebookID] ASC
) ON [PRIMARY]
) ON [PRIMARY]


if exists (select 1 from information_schema.columns where table_name = 'pricebook' and column_name = 'Name' and (data_Type <> 'nvarchar' OR character_maximum_length < 100))
	alter table pricebook alter column name nvarchar(100)

if not exists (select * from information_schema.columns where table_name='pricebook' and column_name='lastUpdatedByPerson')  
	alter TABLE [dbo].[pricebook] add [lastUpdatedByPerson] int NULL

/* generate mr audit is here so that the scripts to create an index on the pricebookDel table can run */
exec generate_mrAudit @tablename='pricebook',@mnemonic='PB'
IF  NOT EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pricebook_startDate]') AND type = 'D')
ALTER TABLE [dbo].[Pricebook] ADD  CONSTRAINT [DF_Pricebook_startDate]  DEFAULT (getdate()) FOR [startDate]
GO

/* NJH 2016/02/01 - change datatype from datetime to date */
if exists (select 1 from information_schema.columns where table_name='pricebook' and column_name='startDate' and data_type='datetime')
begin
	begin tran
		exec dropAndRecreateDependentObjects 'pricebook.startDate', 'drop'
		alter table pricebook alter column startDate date not null
		exec dropAndRecreateDependentObjects 'pricebook.startDate', 'recreate'
	commit
end

if exists (select 1 from information_schema.columns where table_name='pricebook' and column_name='endDate' and data_type='datetime')
begin
	begin tran
		exec dropAndRecreateDependentObjects 'pricebook.endDate', 'drop'
		alter table pricebook alter column endDate date null
		exec dropAndRecreateDependentObjects 'pricebook.endDate', 'recreate'
	commit
end
