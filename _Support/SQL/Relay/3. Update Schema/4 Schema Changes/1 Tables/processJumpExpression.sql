
if not exists (select 1 from information_schema.tables where table_name='processJumpExpression')
Begin
CREATE TABLE [dbo].[processJumpExpression](
	[processJumpExpressionID] [int] IDENTITY(1,1) NOT NULL,
	[JumpExpression] [varchar](250) NULL,
	[JumpExpressionDesc] [varchar](80) NULL,
 CONSTRAINT [PK__processJumpExpression] PRIMARY KEY CLUSTERED 
(
	[processJumpExpressionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END