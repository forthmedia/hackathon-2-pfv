If not exists (select 1 from sysobjects where name='portalSearch' and type='U')
	CREATE TABLE [dbo].[portalSearch](
		[portalSearchID] [int] IDENTITY(1,1) NOT NULL,
		[displayname] [nvarchar](255) NOT NULL,
		[searchMethodName] [nvarchar](255) NOT NULL,
		[indexMethodName] [nvarchar](255) NULL,
		[path] [nvarchar](255) NULL,
		[lastIndexed] [datetime] NULL,
		[sortOrder] [tinyint] NOT NULL default 1,
		[created] [datetime] NULL,
		[createdBy] [int] NULL,
		[lastUpdated] [datetime] NULL,
		[lastUpdatedBy] [int] NULL,
		[lastUpdatedByPerson] [int] NULL,
		[active] [bit] NOT NULL default 1,
		[languageid] [int] NULL,
		[collectionName] [nvarchar](255) NULL,
	PRIMARY KEY CLUSTERED 
		([portalSearchID] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]
GO