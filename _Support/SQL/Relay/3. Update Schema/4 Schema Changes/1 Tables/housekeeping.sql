/*
WAB 2016-03-17	PROD2016-118  Housekeeping Improvements
New Table
*/

IF not exists (select 1 from information_schema.tables where table_name = 'housekeeping')
BEGIN

create table housekeeping
(
	housekeepingID	int identity (1,1),
	methodname sysname,
	runOnEach sysname
)

END

GO

	