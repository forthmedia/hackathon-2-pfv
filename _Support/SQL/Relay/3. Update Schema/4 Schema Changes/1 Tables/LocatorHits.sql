if exists (select * FROM  sysobjects where name='LocatorHits' and xtype='U') 
BEGIN
	IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='LocatorHits' AND column_name='visitID')
	BEGIN
		ALTER TABLE [dbo].[LocatorHits] ADD [visitID] [int] NULL
	END
	IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='LocatorHits' AND column_name='sessionID')
	BEGIN
		ALTER TABLE [dbo].[LocatorHits] ADD [sessionID] [nvarchar] (500) NULL
	END
	IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='LocatorHits' AND column_name='browserLanguage')
	BEGIN
		ALTER TABLE [dbo].[LocatorHits] ADD [browserLanguage] [nvarchar] (25) NULL
	END

	IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='LocatorHits' AND column_name='RemoteAddy')
	BEGIN
		ALTER TABLE [dbo].[LocatorHits] DROP COLUMN [RemoteAddy]
	END
	IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name='LocatorHits' AND column_name='islive')
	BEGIN
		ALTER TABLE [dbo].[LocatorHits] DROP COLUMN [islive]
	END

	ALTER TABLE [dbo].[LocatorHits] ALTER COLUMN [LocatorID] [smallint] NULL
END
ELSE
--not sure this is needed as should exist everywhere
BEGIN
	CREATE TABLE dbo.LocatorHits
	(
	HitID int NOT NULL IDENTITY,
	LocatorID smallint,
	HitTime datetime not null,
	visitID int,
	sessionID nvarchar(500),
	browserLanguage nvarchar(25)
	)
END