/****** Object:  Table [dbo].[cachedQuery]    Script Date: 04/30/2012 22:33:27 ******/
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cachedQuery]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[cachedQuery](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[tablename] [varchar](250) NOT NULL,
	[queryString] [ntext] NOT NULL,
	[sessionID] [varchar](100) NOT NULL,
	[pageSize] [int] NOT NULL,
	[recordCount] [int] NOT NULL,
	[lastPageAccessed] [int] NOT NULL,
	[lastAccessed] [datetime] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL
 CONSTRAINT [PK_cachedQuery] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]



ALTER TABLE [dbo].[cachedQuery] ADD  CONSTRAINT [DF_cachedQuery_lastAccessed]  DEFAULT (getdate()) FOR [lastAccessed]


ALTER TABLE [dbo].[cachedQuery] ADD  CONSTRAINT [DF_cachedQuery_created]  DEFAULT (getdate()) FOR [created]


ALTER TABLE [dbo].[cachedQuery] ADD  CONSTRAINT [DF_cachedQuery_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]

End
GO


alter table cachedQuery alter column queryString nvarchar(max) not null

/* GCC 2015-02-12 
Add index to improve performance of query cleanUpCachedQueriesForSession
*/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cachedQuery]') AND name = N'IX_SessionID')
DROP INDEX [IX_SessionID] ON [dbo].[cachedQuery] WITH ( ONLINE = OFF )

GO

CREATE NONCLUSTERED INDEX [IX_SessionID] ON [dbo].[cachedQuery] 
(
	[sessionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO