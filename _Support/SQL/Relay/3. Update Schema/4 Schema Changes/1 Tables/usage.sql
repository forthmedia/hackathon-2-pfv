
if not exists(select 1 from information_schema.columns where table_name='usage' and column_name='source')
alter table usage add source varchar(256)
GO

if not exists(select 1 from information_schema.columns where table_name='usage' and column_name='siteDefDomainID')
alter table usage add siteDefDomainID int
GO

if not exists(select 1 from information_schema.columns where table_name='usage' and column_name='visitID')
alter table usage add visitID int
GO

/* populate the new siteDefDomainID column */
if exists (select 1 from usage where siteDefDomainID is null) and exists (select 1 from information_schema.columns where table_name='usage' and column_name='app') 
begin
	declare @sql nvarchar(max)
	select @sql = 'insert into siteDefDomain(siteDefID,testSite,domain,mainDomain,active)
	select distinct 0,0, rtrim(ltrim(replace(u.app,''Login'',''''))),0,0 from usage u
		left join siteDefDomain sdd on ltrim(rtrim(sdd.domain)) = rtrim(ltrim(replace(u.app,''Login'','''')))
	where sdd.id is null
	
	update usage set siteDefDomainID = sdd.ID
		from siteDefDomain sdd inner join usage u on ltrim(rtrim(sdd.domain)) = rtrim(ltrim(replace(u.app,''Login'','''')))'
	exec (@sql)
end

/* do not drop the column on dev sites for backwards compatibility issues */
if not exists (select 1 from usage where siteDefDomainID is null) and not exists(select 1 from coldfusionInstance_autoPopulated where testSite = 2)
and exists (select 1 from information_schema.columns where table_name='usage' and column_name='app')
alter table usage drop column app

/* NJH 2013/10/18 activities */
IF NOT EXISTS (select * from sysindexes where id=object_id('usage') and name='idx_impersonatedByPersonID_loginDate_include_personId_source_siteDefDomainID')
create nonclustered index [idx_impersonatedByPersonID_loginDate_include_personId_source_siteDefDomainID] on dbo.usage (impersonatedByPersonID,loginDate) include (personID,source,siteDefDomainId)

/* MRE 2017-01-19 PROD2016-3225 Original 'source' column length was too short for MS Azure */
DECLARE @sourceLength int = 
(SELECT Character_Maximum_Length FROM INFORMATION_SCHEMA.COLUMNS 
	WHERE table_name='Usage' AND column_name='source')
IF (@sourceLength < 256 AND @sourceLength != -1)
BEGIN
	ALTER TABLE Usage ALTER COLUMN source varchar(256)
END