
if not exists(select 1 from information_schema.tables where table_name='scoreRuleVersion')
	begin
		CREATE TABLE [dbo].scoreRuleVersion(
			[scoreRuleVersionID] [int] IDENTITY(1,1) NOT NULL,
			[scoreRuleID] [int] NOT NULL FOREIGN KEY REFERENCES scoreRule(scoreRuleID),
			[scoreValue] [int],	
			[scoreCap] [int],
			[nonStandardMethod] [nvarchar](MAX),
			[nonStandardParameters] [nvarchar](MAX),
			[filterDefinition] [nvarchar](MAX),
			[activityTypeID][int] NOT NULL FOREIGN KEY REFERENCES activityType(activityTypeID),
			[validFrom] [datetime] NOT NULL,
			[validTo] [datetime],  
			[active] [bit], 
			[created] [datetime] NOT NULL DEFAULT getdate(),
			[createdBy] [int] NOT NULL,
			[lastUpdated] [datetime] NOT NULL  DEFAULT getdate(),
			[lastUpdatedBy] [int] NOT NULL,
			[lastUpdatedByPerson] [int] NOT NULL,
		 CONSTRAINT [PK_scoreRuleVersion] PRIMARY KEY CLUSTERED 
		(
			[scoreRuleVersionID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
			 VALUES (547, 'scoreRuleVersion', 'scoreRuleVersionID', 0, 'SCRLV', 0);
	end		 
	GO
	
	
IF not EXISTS (SELECT *  FROM sys.indexes  WHERE name='scoreRuleVersion_scoreRuleID_index' AND object_id = OBJECT_ID('[dbo].[scoreRuleVersion]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX scoreRuleVersion_scoreRuleID_index
	ON dbo.scoreRuleVersion (scoreRuleID)
End
	