IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[oauth_consumers]') AND type in (N'U'))

CREATE TABLE [dbo].[oauth_consumers](
	[consumer_ID] [int] NOT NULL,
	[editor_ID] [int] NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[fullname] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[ckey] [nvarchar](50) NOT NULL,
	[csecret] [nvarchar](50) NOT NULL,
	[datecreated] [datetime] NOT NULL,
 CONSTRAINT [PK_oauth_consumers] PRIMARY KEY CLUSTERED 
(
	[consumer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


