/* Create ScheduleType table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleType')
BEGIN
CREATE TABLE [dbo].[ScheduleType](
	[ScheduleTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleTypeTextID] [nvarchar](50) NOT NULL,
	[ScheduleType] [nvarchar](50) NULL,
	[LogHistory] [bit] NULL,
	[ScheduleTable] [nvarchar](50) NULL,
 CONSTRAINT [PK_ScheduleType] PRIMARY KEY CLUSTERED 
(
	[ScheduleTypeID] ASC
)
) ON [PRIMARY]
END
GO