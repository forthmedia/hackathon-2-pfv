/*********************
2012/03/27 RMB added mobileCompatible
2016-05-12 atrunov  PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules (added hasRecordRightsBitMask)
**********************/

if not exists(select 1 from information_schema.columns where table_name='trngCourse' and column_name = 'active')
-- add the active column to the trngCourse table
alter table trngCourse add active bit null
GO

if not exists(select 1 from information_schema.columns where table_name='trngCourse' and column_name = 'mobileCompatible')
-- RMB added the mobileCompatible column to the trngCourse table
alter table trngCourse add mobileCompatible bit not null default 0
GO

IF		NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE	TABLE_NAME	= 'trngCourse' AND COLUMN_NAME = 'publishedDate')
BEGIN
	ALTER TABLE trngCourse
	ADD			publishedDate DATETIME NULL
END

GO

if exists (select * from information_schema.columns where table_name ='trngCourse' and column_name='active')
	begin
		if exists (select * from information_schema.columns where table_name ='trngCourse' and column_name='active' and column_default is null)
			begin 
				alter table trngCourse add constraint DF_trngCourse_active DEFAULT(1) for active
				if exists (select * from trngCourse WHERE active is NULL)
					update trngCourse set active=1 where active is null
				alter table trngCourse alter column active bit NOT NULL 
			end
	end


GO

/* WAB 2012-07-16, although this column is added automatically by a script later on in the build, we now need it earlier to allow a view to be built on it*/
if not exists (select 1 from information_schema.columns WHERE table_name= 'trngCourse' AND column_name='title_defaultTranslation')
alter table trngCourse add title_defaultTranslation nvarchar(400)
GO

/* trngCourse */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCourse') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCourse' AND column_name='TrainingProgramID') 
ALTER TABLE trngCourse add TrainingProgramID int NULL
GO

/* NJH 2013/01/08 Case 433201 */
if not exists (select 1 from information_schema.columns WHERE table_name= 'trngCourse' AND column_name='lastUpdatedByPerson')
alter table trngCourse add lastUpdatedByPerson int not null default(0)
GO

/* This script adds a hasRecordRightsBitMask column to a table and populates it */
DECLARE @tableName varchar(MAX)
DECLARE @SQL varchar(MAX)
SET @tableName ='trngCourse'
if not exists (select * from information_schema.columns where table_name = @tableName and column_Name = 'hasRecordRightsBitMask')
BEGIN

    set @SQL  = 'alter table ' + @tableName + ' add hasRecordRightsBitMask int default 0  '
    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = 'alter table ' + @tableName + 'del add hasRecordRightsBitMask int'
        exec (@SQL)
    END

    set @SQL  = '
        update ' + @tableName + ' set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
        alter table ' + @tableName + ' alter column hasRecordRightsBitMask int not null
        update recordRights set permission = permission where entity = ''' + @tableName  + '''
        '

    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = '
            update ' + @tableName + 'del set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
            alter table ' + @tableName + 'del alter column hasRecordRightsBitMask int not null
        '
        exec (@SQL)
    END

END
