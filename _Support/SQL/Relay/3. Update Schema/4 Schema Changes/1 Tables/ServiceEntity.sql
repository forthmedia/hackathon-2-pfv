
if not exists(select 1 from sysobjects where name='ServiceEntity' and type='u')

CREATE TABLE [dbo].[ServiceEntity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[entityID] [int] NOT NULL,
	[entityTypeID] [int] NOT NULL,
	[serviceID] [int] NOT NULL,
	[serviceEntityID] [varchar](50) NOT NULL,
	[publicProfileURL] [varchar](500) NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_ServiceEntity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ServiceEntity_created]'))
ALTER TABLE [dbo].[ServiceEntity] ADD  CONSTRAINT [DF_ServiceEntity_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_ServiceEntity_lastUpdated]'))
ALTER TABLE [dbo].[ServiceEntity] ADD  CONSTRAINT [DF_ServiceEntity_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO
