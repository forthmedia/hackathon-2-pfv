/*
2013-09-14 IH Case 437335 create a single column primary key
*/

DECLARE @PrimaryKeyName AS VARCHAR(200)
SELECT @PrimaryKeyName = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'exchangeRate' AND TABLE_SCHEMA ='dbo' AND CONSTRAINT_NAME != 'PK_exchangeRate_exchangeRateID'
IF @PrimaryKeyName IS NOT NULL
	EXEC ('ALTER TABLE dbo.exchangeRate DROP CONSTRAINT ' + @PrimaryKeyName)

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='exchangeRate' AND column_name='exchangeRateID') 	
Begin
	ALTER TABLE exchangeRate
	ADD exchangeRateID int identity not null;
End
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'exchangeRate' AND TABLE_SCHEMA ='dbo' AND CONSTRAINT_NAME = 'PK_exchangeRate_exchangeRateID')
	ALTER TABLE dbo.exchangeRate
	ADD CONSTRAINT PK_exchangeRate_exchangeRateID PRIMARY KEY (exchangeRateID)
GO

if exists (select 1 from information_schema.columns where table_name='exchangeRate' and column_name='updatedBy')
exec sp_rename 'exchangeRate.updatedBy','lastUpdatedBy'

if exists (select 1 from information_schema.columns where table_name='exchangeRate' and column_name='updated')
exec sp_rename 'exchangeRate.updated','lastUpdated'
