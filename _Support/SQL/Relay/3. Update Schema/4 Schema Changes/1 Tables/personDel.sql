IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PersonDel]') AND name = N'PersonID_Idx')
DROP INDEX [dbo].[PersonDel].[PersonID_Idx]
GO

CREATE NONCLUSTERED INDEX [PersonID_Idx] ON [dbo].[PersonDel] 
(
	[PersonID] ASC
) ON [PRIMARY]
GO

if not exists(select 1 from information_schema.columns where table_name='personDel' and column_name='pictureURL')
alter table personDel add pictureURL varchar(250)
GO

if not exists (select * from information_schema.columns where table_name='personDel' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].personDel add [lastUpdatedByPerson] int NULL

GO


/*  WAB 2013-12-09 add this index so that personDel can be searched quickly in vEntityName view */
IF  not EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PersonDel]') AND name = N'personDel_personid')

	CREATE NONCLUSTERED INDEX [personDel_personid] ON [dbo].[PersonDel] 
	(
		[personid] ASC


	) 
		include (firstname, lastname, crmPerid)
	ON [PRIMARY]

/* NJH 2015/07/16 - for vEntityname */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PersonDel]') AND name = N'crmPerID_idx')
BEGIN
	CREATE INDEX crmPerID_idx ON PersonDel (crmPerID);
END	
GO