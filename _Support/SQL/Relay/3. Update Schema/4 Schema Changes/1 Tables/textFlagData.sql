if not exists (select 1 from information_schema.columns where table_name = 'textFlagData' and column_name = 'lastUpdatedByPerson') 
	alter table textFlagData add lastUpdatedByPerson int 
GO
/* WAB 2013-11-21 convert data to nvarchar(max) 
		Russ wanted to store lots of data (html).
 */
IF EXISTS (select * from information_schema.columns where table_name = 'textflagdata' and column_name = 'data' and character_maximum_Length <> -1)
	BEGIN
		alter table textFlagData 	alter column data nvarchar(max)
		IF EXISTS (select * from sysObjects where name = 'textFlagDataDel')
			alter table textFlagDataDel alter column data nvarchar(max)
	END	
	

if not exists (select 1 from information_schema.columns where table_name='textFlagData' and column_name='rowID')
alter table textFlagData add rowID int identity not null
GO