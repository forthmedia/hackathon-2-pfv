if not exists (select 1 from sysobjects where name='dataLoadType' and type='u')
begin
CREATE TABLE [dbo].[dataLoadType](
	[dataloadTypeID] [int] IDENTITY(1,1) NOT NULL,
	[dataloadTypeTextID] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[loadTypeGroup] [varchar](10) NULL,
 CONSTRAINT [PK_dataLoadType] PRIMARY KEY CLUSTERED 
(
	[dataloadTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

GO