if not exists (SELECT * FROM information_schema.columns where table_name='quiztaken' and column_name='QuestionsViewed')
	alter table quiztaken add QuestionsViewed varchar(500) null
if not exists (SELECT * FROM information_schema.columns where table_name='quiztaken' and column_name='forceCompletion')
	alter table quiztaken add forceCompletion bit default(0) not null

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTaken') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizTaken' AND column_name='LanguageID') 
ALTER TABLE QuizTaken add LanguageID int NULL
GO

if not exists (select * from sys.indexes where name='idx_created_createdby_personid' and object_id = OBJECT_ID('QuizTaken'))
	CREATE NONCLUSTERED INDEX [idx_created_createdby_personid]
	ON [dbo].[QuizTaken] ([Created],[CreatedBy],[PersonID])

/* NJH 2012/09/11 part of training manager phase 2. */ 
if not exists (SELECT 1 FROM information_schema.columns where table_name='quiztaken' and column_name='quizStatus')
	alter table quiztaken add quizStatus varchar(4)
GO

if not exists (SELECT 1 FROM information_schema.columns where table_name='quiztaken' and column_name='passmark')
	alter table quiztaken add passmark float
GO
/* 2014-09-22 PPB Case 439473 */
if not exists (SELECT 1 FROM information_schema.columns where table_name='quiztaken' and column_name='ScoreMin')
	alter table quiztaken add ScoreMin numeric(9, 3) NULL 
GO

/* 2014-09-22 PPB Case 439473 also add to the del table */
if exists (SELECT 1 FROM information_schema.columns WHERE table_name= 'QuizTakenDel') AND not exists (SELECT 1 FROM information_schema.columns where table_name='quiztakendel' and column_name='ScoreMin')
	alter table quiztakendel add ScoreMin numeric(9, 3) NULL 
GO

/* 2014-09-22 PPB Case 439473 */
if not exists (SELECT 1 FROM information_schema.columns where table_name='quiztaken' and column_name='ScoreMax')
	alter table quiztaken add ScoreMax numeric(9, 3) NULL 
GO

/* 2014-09-22 PPB Case 439473  also add to the del table */
if exists (SELECT 1 FROM information_schema.columns WHERE table_name= 'QuizTakenDel') AND not exists (SELECT 1 FROM information_schema.columns where table_name='quiztakendel' and column_name='ScoreMax')
	alter table quiztakendel add ScoreMax numeric(9, 3) NULL 
GO
