
if not exists(select 1 from sysobjects where name='trngPersonCertificationRule' and type='u')
CREATE TABLE [dbo].[trngPersonCertificationRule](
	[personCertificationRuleID] [int] IDENTITY(1,1) NOT NULL,
	[personCertificationID] [int] NOT NULL,
	[certificationRuleID] [int] NOT NULL,
	[numModulesToPass] [int] NOT NULL,
	[moduleSetID] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[studyPeriod] [int] NULL,
	[certificationRuleTypeID] [int] NULL,
	[activationDate] [datetime] NULL,
	[certificationRuleOrder] [int] NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_trngPersonCertificationRule] PRIMARY KEY CLUSTERED 
(
	[personCertificationRuleID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[FK_trngPersonCertificationRule_trngCertificationRule]'))
ALTER TABLE [dbo].[trngPersonCertificationRule]  WITH CHECK ADD  CONSTRAINT [FK_trngPersonCertificationRule_trngCertificationRule] FOREIGN KEY([certificationRuleID])
REFERENCES [dbo].[trngCertificationRule] ([certificationRuleID])
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[FK_trngPersonCertificationRule_trngModuleSet]'))
ALTER TABLE [dbo].[trngPersonCertificationRule]  WITH CHECK ADD  CONSTRAINT [FK_trngPersonCertificationRule_trngModuleSet] FOREIGN KEY([moduleSetID])
REFERENCES [dbo].[trngModuleSet] ([moduleSetID])
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[FK_trngPersonCertificationRule_trngPersonCertification]'))
ALTER TABLE [dbo].[trngPersonCertificationRule]  WITH CHECK ADD  CONSTRAINT [FK_trngPersonCertificationRule_trngPersonCertification] FOREIGN KEY([personCertificationID])
REFERENCES [dbo].[trngPersonCertification] ([personCertificationID])
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[DF_trngPersonCertificationRule_created]'))
ALTER TABLE [dbo].[trngPersonCertificationRule] ADD  CONSTRAINT [DF_trngPersonCertificationRule_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select 1 from sysconstraints where constID=OBJECT_ID(N'[DF_trngPersonCertificationRule_lastUpdated]'))
ALTER TABLE [dbo].[trngPersonCertificationRule] ADD  CONSTRAINT [DF_trngPersonCertificationRule_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO