IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[approvalEngine]') AND type in (N'U'))

	CREATE TABLE dbo.approvalEngine
		(
		approvalEngineID int NOT NULL 
			IDENTITY (1, 1) 
			CONSTRAINT [PK_approvalEngineID] PRIMARY KEY CLUSTERED,
		entityTypeID int NOT NULL 
			FOREIGN KEY REFERENCES [dbo].[schemaTableBase] ([entityTypeID]),
		rejectedWorkflowID int NULL,
		approvedWorkflowID int NULL,
		approvalRequestEmailID int NOT NULL,
		approvalCopyRequestEmailID int NULL,
		approvalReviewScreenID int NULL,
		title nvarchar(500) NULL,
		[Created] [datetime] NOT NULL DEFAULT (getdate()),
		[CreatedBy] [int] NULL,
		[LastUpdated] [datetime] NOT NULL DEFAULT (getdate()),
		[LastUpdatedBy] [int] NULL,
		[lastUpdatedByPerson] [int] NULL
		)  ON [PRIMARY]

GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=510) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'approvalEngine') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'AE') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
	VALUES (510, 'approvalEngine', 'approvalEngineID', 0, 'AE', 0);
GO

if not exists (SELECT * FROM information_schema.columns where table_name='approvalEngine' and column_name='revisionWorkflowID')
	ALTER TABLE approvalEngine ADD revisionWorkflowID int NULL 
GO