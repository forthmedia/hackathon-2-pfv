/*				select 'select ID,'''+c.mappingType+''','''+importValue+''','''+exportValue+''') from vconnectormapping where entityname=''opportunity'' and entityColumn = '''+entityCOlumn+''''
from connectorColumnValueMapping c
inner join vConnectorMapping m on c.connectorMappingID = m.ID
where c.mappingType='C'*/

/****** Object:  Table [dbo].[connectorColumnValueMapping]    Script Date: 05/15/2014 10:08:29 ******/
If not exists (select 1 from sysobjects where name='connectorColumnValueMapping' and type='U')
BEGIN
	CREATE TABLE [dbo].[connectorColumnValueMapping](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[connectorMappingID] [int] NOT NULL,
		[mappingType] [char](1) NOT NULL,
		[value_relayware] [nvarchar](50) NOT NULL,
		[value_remote] [nvarchar](50) NOT NULL,
		[created] [datetime] NOT NULL CONSTRAINT [DF_connectorColumnValueMapping_created] DEFAULT (getdate()),
		[createdBy] [int] NOT NULL,
		[lastUpdated] [datetime] NOT NULL CONSTRAINT [DF_connectorColumnValueMapping_lastUpdated] DEFAULT (getdate()),
		[lastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_connectorColumnValueMapping] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[connectorColumnValueMapping] ADD  CONSTRAINT [DF_connectorColumnValueMapping_mappingType]  DEFAULT ('V') FOR [mappingType]

END


/* 2014-12-02 WAB 
Unfortunately some systems got this table in a 'pre-release' form, for these we need to retrospectively add the CRUD columns
Because they are NOT NULL with no default, we are going to create with a default (to populate existing records) and the drop the default contraint
*/
if not exists (select 1 from information_schema.columns where table_name = 'connectorColumnValueMapping' and column_name = 'lastUpdatedByPerson') 
BEGIN
	alter table connectorColumnValueMapping add lastUpdatedByPerson int not Null default 0
	exec sp_dropcolumnconstraints 'connectorColumnValueMapping', 'lastUpdatedByPerson'
END

if not exists (select 1 from information_schema.columns where table_name = 'connectorColumnValueMapping' and column_name = 'lastUpdatedBy') 
BEGIN
	alter table connectorColumnValueMapping add lastUpdatedBy int NOT Null default 0
	exec sp_dropcolumnconstraints 'connectorColumnValueMapping', 'lastUpdatedBy'
END

 

if not exists (select 1 from information_schema.columns where table_name = 'connectorColumnValueMapping' and column_name = 'createdBy') 
BEGIN
	alter table connectorColumnValueMapping add createdBy int NOT Null default 0
	exec sp_dropcolumnconstraints 'connectorColumnValueMapping', 'createdBy'
END

if not exists (select 1 from information_schema.columns where table_name = 'connectorColumnValueMapping' and column_name = 'created') 
BEGIN
	alter table connectorColumnValueMapping add created [datetime] NOT Null CONSTRAINT [DF_connectorColumnValueMapping_created] DEFAULT (getdate())
END

if not exists (select 1 from information_schema.columns where table_name = 'connectorColumnValueMapping' and column_name = 'lastUpdated') 
BEGIN
	alter table connectorColumnValueMapping add lastUpdated [datetime] NOT Null CONSTRAINT [DF_connectorColumnValueMapping_lastUpdated] DEFAULT (getdate())
END

/* 2016-04-22 WAB 
Seems as if some versions of this table do not have the primary key
Probably should have fixed this a long time ago.  I kept ignoring this error when Generate_MRaudit was run:
	'This table has a composite key. Create schematable entry with unique key and try again'
*/
if not exists (select 1 from sysconstraints where object_name (id) = 'connectorColumnValueMapping' and colid = 0 )
BEGIN
	ALTER TABLE [connectorColumnValueMapping]
	ADD CONSTRAINT [PK_connectorColumnValueMapping] PRIMARY KEY (ID)
END	


alter table connectorColumnValueMapping alter column [value_relayware] nvarchar(250)
alter table connectorColumnValueMapping alter column [value_remote] nvarchar(250)

exec Generate_MRAuditDel @tablename = 'connectorColumnValueMapping'