/****** Object:  Table [dbo].[Service]    Script Date: 12/22/2011 16:27:36 ******/

if not exists(select 1 from sysobjects where name='Service' and type='u')

CREATE TABLE [dbo].[Service](
	[serviceID] [int] IDENTITY(1,1) NOT NULL,
	[serviceTextID] [varchar](20) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[consumerKey] [varchar](20) NOT NULL,
	[consumerSecret] [varchar](50) NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[serviceID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_Service_created]'))
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_created]  DEFAULT (getdate()) FOR [created]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[DF_Service_lastUpdated]'))
ALTER TABLE [dbo].[Service] ADD  CONSTRAINT [DF_Service_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

/*NJH 2013/02/21 - set the consumer key to be 50 characters long */
alter table service alter column consumerKey varchar(50) not null
GO