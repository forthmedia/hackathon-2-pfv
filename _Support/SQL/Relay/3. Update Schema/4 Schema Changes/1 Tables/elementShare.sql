if not exists (select 1 from information_schema.tables where table_name='elementShare')
Begin
	CREATE TABLE [dbo].[elementShare](
		[elementShareID] [int] IDENTITY(1,1) NOT NULL,
		[personID] [varchar](50) NOT NULL,
		[elementID] [int] NOT NULL,
		[entityTrackingUrlID] [int] NOT NULL,
		[serviceID] [int] NOT NULL,
		[created] [datetime] NOT NULL DEFAULT GetDate(),
	 CONSTRAINT [PK_elementShare] PRIMARY KEY CLUSTERED 
	(
		[elementShareID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
End

GO