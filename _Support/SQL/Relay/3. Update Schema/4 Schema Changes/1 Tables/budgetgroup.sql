update dbo.budgetgroup set accountmanagerapproval=0 where accountmanagerapproval IS NULL

ALTER TABLE dbo.budgetgroup alter column accountmanagerapproval bit NOT NULL


if not exists (select column_name from INFORMATION_SCHEMA.columns where table_schema='dbo' and table_name = 'budgetgroup' and column_name = 'currency') 
ALTER TABLE dbo.budgetgroup add currency varchar(3) NULL

--adding this field caused a problem in relayFundManager.getFundAccountsAvailableForActivity - make sure the fix of 20110419 prefixing countryID is in place
if not exists (select column_name from INFORMATION_SCHEMA.columns where table_schema='dbo' and table_name = 'budgetgroup' and column_name = 'countryID') 
ALTER TABLE dbo.budgetgroup add countryID int NOT NULL DEFAULT(0)



