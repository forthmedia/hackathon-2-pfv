

IF object_id('OAuth2Client') is null  
BEGIN
	Create Table dbo.OAuth2Client(
	clientName nVarChar(256)  NOT NULL,
	clientIdentifier nVarChar(256) PRIMARY KEY NOT NULL, 
	hashedClientSecret nVarChar(512)  NOT NULL,
	redirectURL nVarChar(512)  NOT NULL,
	created date NOT NULL,
	active bit NOT NULL);
END
