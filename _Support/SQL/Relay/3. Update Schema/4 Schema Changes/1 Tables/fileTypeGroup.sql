

alter table fileTypeGroup alter column heading nvarchar(50) null
GO

alter table fileTypeGroup alter column intro nvarchar(255) null
GO

if not exists(select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='created')
alter table dbo.fileTypeGroup add created datetime not null default(getDate())
GO

if not exists(select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='createdBy')
alter table dbo.fileTypeGroup add createdBy int not null default(0)
GO

if not exists(select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='lastUpdatedBy')
alter table dbo.fileTypeGroup add lastUpdatedBy int not null default(0)
GO

if not exists(select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='lastUpdated')
alter table dbo.fileTypeGroup add lastUpdated datetime not null default(getDate())
GO

/* Case 435206 add ability to exclude certain media types from portal search */
if not exists(select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='showOnPortal')
and not exists (select 1 from information_schema.columns where table_name='fileTypeGroup' and column_name='portalSearch')
  	BEGIN
	  	alter table filetypegroup add showOnPortal bit not null default 1
	 	
		/*  A few standard file groups which we don't want on the portal	
			Email template
			Various Standard Template
			Misc Image
			ELearning_CourseWare
			Email Footers
			Locator Images
		
			rather convoluted updated because sql gets update if tries to compile a query before the showOnPortal column has been added 
		*/
	 	
	 	declare @sql nvarchar(1000)
	  	select @sql = 'update filetypegroup set showOnPortal = 0 where filetypegroupid in (13,4,3,6,29,30)'
	  	exec sp_executesql @sql
	END

GO


/* 2016-02-24		WAB 	PROD2015-291  Media Library Visibility.
 get rid of the hasRecordRights_x columns and replace with a single hasRecordRightsBitMask field and populate it

 */


if exists (select * from information_schema.columns where table_name = 'fileTypeGroup' and column_Name = 'hasRecordRights_1')
	exec sp_dropcolumn 'fileTypeGroup', 'hasRecordRights_1'
if exists (select * from information_schema.columns where table_name = 'fileTypeGroup' and column_Name = 'hasRecordRights_2')
	exec sp_dropcolumn 'fileTypeGroup', 'hasRecordRights_2'

if not exists (select * from information_schema.columns where table_name = 'fileTypeGroup' and column_Name = 'hasRecordRightsBitMask')
BEGIN
	alter table fileTypeGroup add hasRecordRightsBitMask int default 0  

	declare @SQL nvarchar(max) = '
		update fileTypeGroup set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
		alter table fileTypeGroup alter column hasRecordRightsBitMask int not null
	'	
	exec (@SQL)
END	


if exists (select * from information_schema.columns where table_name = 'fileTypeGroup' and column_Name = 'showOnPortal')
exec sp_rename 'fileTypeGroup.showOnPortal','portalSearch','COLUMN'
GO



