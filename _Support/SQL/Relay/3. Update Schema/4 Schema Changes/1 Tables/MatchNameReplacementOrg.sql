/*
WAB 2011/11/23 alter to not use sys. objects which appears to no longer be supported
*/

IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[MatchNameReplacementOrg]') AND type in (N'U'))
CREATE TABLE [dbo].[MatchNameReplacementOrg](
	[StringToReplace] nvarchar(100) NOT NULL,
	[ReplacementString] nvarchar(100) NULL DEFAULT('')) ON [PRIMARY]
GO


if not exists (select 1 from information_schema.columns where table_name='MatchNameReplacementOrg' and column_name='ID')
alter table MatchNameReplacementOrg add ID INT IDENTITY(1,1) NOT NULL


IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[MatchNameReplacementOrg]') AND name = N'PK_MatchNameReplacementOrg')
ALTER TABLE [dbo].[MatchNameReplacementOrg] ADD  CONSTRAINT [PK_MatchNameReplacementOrg] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

