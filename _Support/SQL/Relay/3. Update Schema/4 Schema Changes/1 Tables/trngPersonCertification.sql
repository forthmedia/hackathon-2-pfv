if not exists (select 1 from information_schema.columns where table_name = 'trngPersonCertification' and column_name = 'lastUpdatedByPerson') 
	alter table trngPersonCertification add lastUpdatedByPerson int 
GO


/* 2015/12/20 PYW P-TAT006 BRD 31 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngPersonCertification') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngPersonCertification' AND column_name='expiryDate') 
BEGIN
	ALTER TABLE trngPersonCertification
	ADD expiryDate datetime NULL
END
GO