/*  WAB 2013-12-09 added index so that priceBookDel can be searched quickly in vEntityName view (actually does not seem to have any records in it anyway but added for consistency */
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[priceBookDel]') AND name = N'priceBookID_Idx')
	
	CREATE NONCLUSTERED INDEX [priceBookID_Idx] ON [dbo].[priceBookDel]
	(
		[priceBookID] ASC
	) 
	INCLUDE (name)
	ON [PRIMARY]
