IF object_id('SAMLServiceProviderPersonPermission') is null  
BEGIN
	CREATE TABLE dbo.SAMLServiceProviderPersonPermission
	(
		SAMLServiceProviderPersonPermissionID int identity(1,1)  NOT NULL,
		ServiceProviderId int NOT NULL FOREIGN KEY REFERENCES SAMLServiceProvider(ServiceProviderId) ON DELETE CASCADE,
		personID int NOT NULL FOREIGN KEY REFERENCES person (personID) ON DELETE CASCADE,
		createdByPerson int,
		createdby int,
		created date,
		LastUpdatedBy int,
		lastUpdatedByPerson int,
		lastUpdated date,
		
	 CONSTRAINT SAMLServiceProviderPersonPermissionID PRIMARY KEY CLUSTERED 
		(
			SAMLServiceProviderPersonPermissionID ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 85) ON [PRIMARY]);
			
	
	
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
		 VALUES (539, 'SAMLServiceProviderPersonPermission', 'SAMLServiceProviderPersonPermissionID', 0, 'SASPP', 0);
	
	CREATE INDEX SAMLServiceProviderPersonPermissionIndex
	ON SAMLServiceProviderPersonPermission (ServiceProviderId);	 
	CREATE INDEX SAMLServiceProviderPersonPermissionPersonIDIndex
	ON SAMLServiceProviderPersonPermission (personID);	
	

	
End