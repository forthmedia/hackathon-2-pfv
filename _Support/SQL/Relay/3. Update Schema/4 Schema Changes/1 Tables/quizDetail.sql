ALTER TABLE quizDetail ALTER COLUMN possibleQuestions varchar(4000) NULL

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='title_defaultTranslation')
alter table QuizDetail add title_defaultTranslation nvarchar(400)
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='header_defaultTranslation')
alter table QuizDetail add header_defaultTranslation nvarchar(400)
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='footer_defaultTranslation')
alter table QuizDetail add footer_defaultTranslation nvarchar(400)
GO
/* header_defaultTranslation and footer_defaultTranslation will be added by a migration script */

alter table quizDetail alter column moduleId int null
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='active')
alter table QuizDetail add active bit not null default(1)
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='lastUpdated')
alter table QuizDetail add lastUpdated datetime not null default(getDate())
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='lastUpdatedBy')
alter table QuizDetail add lastUpdatedBy int not null default(0)
GO

if not exists (select 1 from information_schema.columns WHERE table_name= 'QuizDetail' AND column_name='lastUpdatedByPerson')
alter table QuizDetail add lastUpdatedByPerson int not null default(0)
GO

if not exists (select 1 from information_schema.columns where table_name = 'QuizDetail' and column_name = 'title_defaultTranslation') 
	alter table QuizDetail add title_defaultTranslation nvarchar(400)
GO