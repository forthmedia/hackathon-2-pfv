if not exists (select 1 from information_schema.tables where table_name='trngTrainingPath')
BEGIN
	CREATE TABLE [dbo].[trngTrainingPath](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[trainingPathTextID] [varchar](50) NOT NULL,
		[title_defaultTranslation] [nvarchar](400) NULL,
		[ignoreAssignmentRules] bit NOT NULL,
		[active] bit NOT NULL,
		[hasRecordRights_1] bit NOT NULL,
		[hasCountryScope_1] bit NOT NULL,
		[Created] [datetime] NOT NULL,
		[CreatedBy] [int] NOT NULL,
		[LastUpdated] [datetime] NOT NULL,
		[LastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL
	 CONSTRAINT [PK_trainingPath_ID] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_hasRecordRights_1]  DEFAULT (0) FOR [hasRecordRights_1]
	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_hasCountryScope_1]  DEFAULT (0) FOR [hasCountryScope_1]
	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_created]  DEFAULT (getdate()) FOR [created]
	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_ignoreAssignmentRules]  DEFAULT (0) FOR [ignoreAssignmentRules]
	ALTER TABLE [dbo].[trngTrainingPath] ADD  CONSTRAINT [DF_trngTrainingPath_active]  DEFAULT (1) FOR [active]
END
GO


/*
2016-05-12	WAB During PROD2016-778 
			Changed this table to use hasRecordRightsBitMask rather than hasRecordRights_1 column
*/

declare		@sql nvarchar(max)
			,@tablename sysname = 'trngTrainingPath'
if not exists (select * from information_schema.columns where table_name = @tableName and column_Name = 'hasRecordRightsBitMask')
BEGIN

    set @SQL  = 'alter table ' + @tableName + ' add hasRecordRightsBitMask int default 0  '
    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = 'alter table ' + @tableName + 'del add hasRecordRightsBitMask int'
        exec (@SQL)
    END    

    set @SQL  = '
        update ' + @tableName + ' set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
        alter table ' + @tableName + ' alter column hasRecordRightsBitMask int not null
        update recordRights set permission = permission where entity = ''' + @tableName  + '''
        '

    exec (@SQL)

    IF exists (select 1 from information_schema.tables where table_name = @tableName + 'del')
    BEGIN
        set @SQL  = '
            update ' + @tableName + 'del set hasRecordRightsBitMask = 0 where hasRecordRightsBitMask is null
            alter table ' + @tableName + 'del alter column hasRecordRightsBitMask int not null
        '    
        exec (@SQL)
    END    

    exec sp_dropcolumn 'trngTrainingPath', 'hasRecordRights_1'
    exec sp_dropcolumn 'trngTrainingPathDel', 'hasRecordRights_1'


    
END

