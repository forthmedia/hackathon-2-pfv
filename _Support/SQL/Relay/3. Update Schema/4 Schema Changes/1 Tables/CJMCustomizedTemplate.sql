/* 2014/12/10	YMA	Store customized templates 
    2015-11-10  ACPK    PROD2015-24 Add canvasBody, customTitle, and customDescription to table
*/

if not exists (select 1 from information_schema.tables where table_name='CJMCustomizedTemplate')
begin
	CREATE TABLE [dbo].[CJMCustomizedTemplate](
		[cjmCustomizedTemplateID] [int] IDENTITY(1,1) NOT NULL,
		[cjmTemplateID] [int] NOT null,
		[customizedJsonContent] nvarchar(max) null,
		[personID] [int] NOT NULL,
		[createdBy] [int] NOT NULL DEFAULT (0),
		[created] [datetime] NOT NULL DEFAULT (getdate()),
		[lastUpdatedBy] [int] NOT NULL DEFAULT (0),
		[lastUpdated] [datetime] NOT NULL DEFAULT (getdate())
	)
end

if not exists (select 1 from INFORMATION_SCHEMA.COLUMNS where table_name='CJMCustomizedTemplate' and COLUMN_NAME='canvasBody')
begin
    alter table CJMCustomizedTemplate
    ADD canvasBody nvarchar(max)
end

if not exists (select 1 from INFORMATION_SCHEMA.COLUMNS where table_name='CJMCustomizedTemplate' and COLUMN_NAME='customTitle')
begin
    alter table CJMCustomizedTemplate
    ADD customTitle nvarchar(50)
end

if not exists (select 1 from INFORMATION_SCHEMA.COLUMNS where table_name='CJMCustomizedTemplate' and COLUMN_NAME='customDescription')
begin
    alter table CJMCustomizedTemplate
    ADD customDescription nvarchar(max)
end