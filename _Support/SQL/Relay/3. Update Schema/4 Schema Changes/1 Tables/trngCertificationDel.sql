
/* NJH 2013/07/24 - added share to del table.. couldn't add it via generate_MRAuditDel script
	because share was a non nullable field, so the script fell over when trying to insert data from the existing del 
	table into the temp_del table (which is a copy of the main table)... so, that needs to be looked at at some point */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationDel') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationDel' AND column_name='share') 
	ALTER TABLE trngCertificationDel add share bit NOT NULL DEFAULT 1
GO

/* 2015/12/20 PYW P-TAT006 BRD 31 */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationDel') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'trngCertificationDel' AND column_name='validity') 
BEGIN
	ALTER TABLE trngCertificationDel
	ADD validity integer NULL
END
GO