

IF  EXISTS (select * from dbo.sysobjects where xtype='F' and name='FK_BudgetGroupPeriodAllocation_BudgetGroup')
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetGroup]
GO

IF  EXISTS (select * from dbo.sysobjects where xtype='F' and name='FK_BudgetGroupPeriodAllocation_BudgetGroup')
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetPeriod]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BudgetGroupPeriodAllocation_authorised]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [DF_BudgetGroupPeriodAllocation_authorised]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BudgetGroupPeriodAllocation_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [DF_BudgetGroupPeriodAllocation_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BudgetGroupPeriodAllocation_createdBy]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [DF_BudgetGroupPeriodAllocation_createdBy]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BudgetGroupPeriodAllocation_updated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [DF_BudgetGroupPeriodAllocation_updated]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BudgetGroupPeriodAllocation_updatedBy]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] DROP CONSTRAINT [DF_BudgetGroupPeriodAllocation_updatedBy]
END

GO

/*
WAB 2011/11/23 alter to not use sys. objects which appears to no longer be supported
*/

/****** Object:  Table [dbo].[BudgetGroupPeriodAllocation]    Script Date: 04/12/2011 16:24:30 ******/
IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[BudgetGroupPeriodAllocation]') AND type in (N'U'))
CREATE TABLE [dbo].[BudgetGroupPeriodAllocation](
	[budgetGroupPeriodAllocationID] [int] IDENTITY(1,1) NOT NULL,
	[budgetGroupID] [int] NOT NULL,
	[budgetPeriodID] [int] NOT NULL,
	[amount] [decimal](18, 2) NOT NULL,
	[authorised] [bit] NOT NULL,
	[created] [datetime] NOT NULL,
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL,
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_BudgetGroupPeriodAllocation] PRIMARY KEY CLUSTERED 
(
	[budgetGroupPeriodAllocationID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetGroup] FOREIGN KEY([budgetGroupID])
REFERENCES [dbo].[BudgetGroup] ([BudgetGroupID])
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] CHECK CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetGroup]
GO

if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[FK_BudgetGroupPeriodAllocation_BudgetPeriod]'))
ALTER TABLE [dbo].[BudgetGroupPeriodAllocation]  WITH NOCHECK ADD  CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetPeriod] FOREIGN KEY([budgetPeriodID])
REFERENCES [dbo].[BudgetPeriod] ([budgetPeriodID])
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] CHECK CONSTRAINT [FK_BudgetGroupPeriodAllocation_BudgetPeriod]
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] ADD  CONSTRAINT [DF_BudgetGroupPeriodAllocation_authorised]  DEFAULT ((0)) FOR [authorised]
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] ADD  CONSTRAINT [DF_BudgetGroupPeriodAllocation_created]  DEFAULT (getdate()) FOR [created]
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] ADD  CONSTRAINT [DF_BudgetGroupPeriodAllocation_createdBy]  DEFAULT ((0)) FOR [createdBy]
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] ADD  CONSTRAINT [DF_BudgetGroupPeriodAllocation_updated]  DEFAULT (getdate()) FOR [lastUpdated]
GO

ALTER TABLE [dbo].[BudgetGroupPeriodAllocation] ADD  CONSTRAINT [DF_BudgetGroupPeriodAllocation_updatedBy]  DEFAULT ((0)) FOR [lastUpdatedBy]
GO


