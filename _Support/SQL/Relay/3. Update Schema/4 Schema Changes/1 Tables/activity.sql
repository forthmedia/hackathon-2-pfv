if not exists (select 1 from information_schema.tables where table_name='Activity')
Begin

	CREATE TABLE [dbo].[Activity](
		[activityID] [int] IDENTITY(1,1) NOT NULL,
		[activityTypeID] [int] NOT NULL,
		[entityID] [int] NULL,
		[activityDate] [date] NOT NULL,
		[activityTime] [datetime] NULL,
		[personID] [int] NULL,
		[locationID] [int] NULL,
		[organisationTypeID] [int] NULL,
		[organisationID] [int] NULL,
		[countryID] [int] NOT NULL,
		[numActivities] [int] NULL,
		[data] [nvarchar](max) NULL
		CONSTRAINT [PK_activity] PRIMARY KEY CLUSTERED 
	(
		[activityID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[Activity] ADD  CONSTRAINT [DF_Activity_activityDate]  DEFAULT (getdate()) FOR [activityDate]
End

GO

IF not EXISTS (SELECT *  FROM sys.indexes  WHERE name='Activity_organisationID_index' AND object_id = OBJECT_ID('[dbo].[Activity]'))
Begin
	-- required for the aqctivity score widget which heavily gets organisation data
	CREATE INDEX Activity_organisationID_index
	ON dbo.Activity (organisationID)
End