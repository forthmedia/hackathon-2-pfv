
if not exists (select 1 from sysObjects where name = 'countryAlternatives')
BEGIN
CREATE TABLE [dbo].[countryAlternatives]
(
[countryAlternativesID] [int] NOT NULL IDENTITY(1, 1),
[countryID] [int] NULL,
[relayCountryDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[alternativeCountryDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)

-- Add 66 rows to [dbo].[countryAlternatives]
SET IDENTITY_INSERT [dbo].[countryAlternatives] ON
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (1, 'Poland', 'Polska')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (2, 'United Kingdom', 'UK')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (3, 'United Kingdom', 'Unitd kingdom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (4, 'United Kingdom', 'United  Kingdom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (5, 'Belarus', 'Belorussia')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (6, 'Croatia', 'Hrvatska')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (7, 'NetherLands', 'The NetherLands')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (8, 'USA', 'US')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (9, 'United Kingdom', 'UUnited Kingdom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (10, 'United Kingdom', 'United Kingom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (11, 'United Kingdom', 'United Kingdon')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (12, 'Ireland', 'Republic of Ireland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (13, 'USA', 'America')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (14, 'Belgium', 'Belguim')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (15, 'Germany', 'Deutschland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (16, 'Hong kong', 'Hongkong')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (17, 'Portugal', 'Madeira')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (18, 'United Kingdom', 'England')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (19, 'United Kingdom', 'British')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (20, 'United Kingdom', 'Britain')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (21, 'United Kingdom', 'Endland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (22, 'United Kingdom', 'Eng;land')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (23, 'United Kingdom', 'ENGALND')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (24, 'United Kingdom', 'Engfland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (25, 'United Kingdom', 'England (UK)')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (26, 'United Kingdom', 'England, UK')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (27, 'United Kingdom', 'England.')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (28, 'United Kingdom', 'ENGLAND9')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (29, 'United Kingdom', 'Englans')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (30, 'United Kingdom', 'English')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (31, 'United Kingdom', 'G B')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (32, 'United Kingdom', 'GB')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (33, 'United Kingdom', 'Great Britain')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (34, 'United Kingdom', 'great britian')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (35, 'United Kingdom', 'Gt Britain')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (36, 'United Kingdom', 'Wales')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (37, 'United Kingdom', 'Wales UK')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (38, 'United Kingdom', 'Wales/UK')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (39, 'United Kingdom', 'United Kindgom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (40, 'United Kingdom', 'United Kingodm')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (41, 'United Kingdom', 'Unitied Kingdom')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (42, 'Ireland', 'Rebulic Of Ireland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (43, 'USA', 'United States')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (44, 'USA', 'United States of America')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (45, 'USA', 'u.s.a.')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (46, 'United Kingdom', 'U K')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (47, 'United Kingdom', 'U. K.')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (48, 'United Kingdom', 'U.K')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (49, 'United Kingdom', 'U.K.')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (50, 'Ireland', 'Eire')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (51, 'United Kingdom', 'Isle of Man')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (52, 'Northern Ireland', 'N.Ireland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (53, 'Netherlands', 'Netherland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (54, 'United Kingdom', 'Scotland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (55, 'United Kingdom', 'Scotland, UK')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (56, 'South Africa', 'SouthbAfrica')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (57, 'Netherlands', 'Holland')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (58, 'Slovakia', 'Slovak Republic')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (59, 'Trinidad & Tobago', 'Trinidad and Tobago')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (60, 'MACAO ', 'MACAU')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (61, 'Bosnia & Herzegovina', 'BOSNIA AND HERZEGOVINA')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (62, 'Mauritania ', 'MAURETANIA')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (63, 'United Arab Emirates', 'UTD. ARAB EMIR.')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (64, 'Réunion', 'REUNION')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (65, 'Luxembourg', 'LUXEMBURG')
INSERT INTO [dbo].[countryAlternatives] ([countryAlternativesID], [relayCountryDescription], [alternativeCountryDescription]) VALUES (66, 'Faroe Islands', 'FAEROE')
SET IDENTITY_INSERT [dbo].[countryAlternatives] OFF

END
GO



/****** Object:  Index [PK_countryAlternatives]    Script Date: 09/18/2014 11:21:01 ******/
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[countryAlternatives]') AND name = N'PK_countryAlternatives')
ALTER TABLE [dbo].[countryAlternatives] ADD  CONSTRAINT [PK_countryAlternatives] PRIMARY KEY CLUSTERED 
(
	[countryAlternativesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


exec addSystemColumns @tablename = 'countryAlternatives', 
                 @excludeCreatedByLastUpdatedBy = 1,
                @allowNullCreatedByPersonLastUpdatedByPerson = 0

IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'countryAlternatives' AND COLUMN_NAME = 'countryID' AND IS_NULLABLE = 'YES')
BEGIN
	ALTER TABLE countryAlternatives
	ALTER COLUMN [countryID] [int] NOT NULL
END
GO

IF NOT EXISTS (SELECT TOP 1 * FROM information_schema.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'FK_countryAlternatives_Country' AND CONSTRAINT_TYPE = 'FOREIGN KEY')
BEGIN
	ALTER TABLE countryAlternatives    
	ADD CONSTRAINT FK_countryAlternatives_Country FOREIGN KEY (countryID)     
		REFERENCES Country (CountryID)
END
GO

IF NOT EXISTS (SELECT TOP 1 * FROM information_schema.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'UC_countryAlternatives_alternativeCountryDescription' AND CONSTRAINT_TYPE = 'UNIQUE')
BEGIN
	/* WAB 2017-03-01 delete any duplicate data before adding the unique index */
	delete from countryAlternatives 
	where countryAlternativesID in 
	(select countryAlternativesID from 
		(select row_number () over(partition by  alternativecountrydescription order by countryid ) as row, alternativecountrydescription, countryAlternativesID
		from countryalternatives ) _
		where row <> 1
	)	

	ALTER TABLE countryAlternatives 
	ADD CONSTRAINT UC_countryAlternatives_alternativeCountryDescription UNIQUE (alternativeCountryDescription)		
END
GO

IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'countryAlternatives' AND COLUMN_NAME = 'relayCountryDescription')
BEGIN
	ALTER TABLE countryAlternatives 
	DROP COLUMN relayCountryDescription	
END
GO