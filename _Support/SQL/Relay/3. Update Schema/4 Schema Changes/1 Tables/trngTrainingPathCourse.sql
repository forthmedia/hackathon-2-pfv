if not exists (select 1 from information_schema.tables where table_name='trngTrainingPathCourse')
BEGIN
	CREATE TABLE [dbo].[trngTrainingPathCourse](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[trainingPathID] [int] NOT NULL,
		[courseID] [int] NOT NULL,
		[Created] [datetime] NOT NULL,
		[CreatedBy] [int] NOT NULL,
		[LastUpdated] [datetime] NOT NULL,
		[LastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL
	 CONSTRAINT [PK_trainingPathCourse_ID] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
	
	ALTER TABLE [dbo].[trngTrainingPathCourse] ADD  CONSTRAINT [DF_trngTrainingPathCourse_created]  DEFAULT (getdate()) FOR [created]
	ALTER TABLE [dbo].[trngTrainingPathCourse] ADD  CONSTRAINT [DF_trngTrainingPathCourse_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
END
GO