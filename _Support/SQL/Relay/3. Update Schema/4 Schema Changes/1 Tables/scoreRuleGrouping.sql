if not exists(select 1 from information_schema.tables where table_name='scoreRuleGrouping')
begin
	CREATE TABLE [dbo].scoreRuleGrouping(
		[scoreRuleGroupingID] [int] IDENTITY(1,1) NOT NULL,
		[title_defaultTranslation] nVarChar(max),
		[description_defaultTranslation] nVarChar(max),
		[entityTypeID] [int] NOT NULL  FOREIGN KEY REFERENCES schemaTableBase(entityTypeID),
		[cappingPeriodTypeTextID] nVarChar(200) NOT NULL,
		[cappingPeriodResetMonthOfYear] [int] DEFAULT 1,
		[cappingPeriodResetDayOfMonth] [int] DEFAULT 1,
		[created] [datetime] NOT NULL DEFAULT getdate(),
		[createdBy] [int] NOT NULL,
		[lastUpdated] [datetime] NOT NULL DEFAULT getdate(),
		[lastUpdatedBy] [int] NOT NULL,
		[lastUpdatedByPerson] [int] NOT NULL,
	 CONSTRAINT [PK_scoreRuleGrouping] PRIMARY KEY CLUSTERED 
	(
		[scoreRuleGroupingID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	INSERT INTO schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) 
			 VALUES (545, 'scoreRuleGrouping', 'scoreRuleGroupingID', 0, 'SCRLG', 0);

end
GO

IF COL_LENGTH('scoreRuleGrouping','active') IS NULL
BEGIN
	ALTER TABLE scoreRuleGrouping
	ADD active  bit NOT NULL Default 1
END

