if not exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'active') 
	alter table productCategory add active bit not null default(1)
GO

if not exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'created') 
	alter table productCategory add created datetime default getDate()
GO

if not exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'createdBy') 
	alter table productCategory add createdBy int
GO

/* NJH 2012/12/13 - make productCategoryName nullable as it's going to be translated. So, field no longer required */
if exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'productCategoryName') 
	alter table productCategory alter column productCategoryName varchar(40) null
GO

/* NJH 2012/12/13 - make productCategoryFactor nullable... field no longer required */
if exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'productCategoryFactor') 
	alter table productCategory alter column productCategoryFactor int null
GO

if not exists (select 1 from information_schema.columns where table_name = 'productCategory' and column_name = 'title_defaultTranslation') 
	alter table productCategory add title_defaultTranslation nvarchar(400)
GO

IF NOT EXISTS (select * from ModEntityDef where TableName='productCategory')
BEGIN
 	exec generate_MRAudit @TableName = 'productCategory'
END

IF EXISTS (select * from ModEntityDef where TableName='productCategory')
BEGIN
 	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'active' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'createdBy' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'createdByPerson' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'lastUpdatedBy' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'productCategoryFactor' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'productCategoryID' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'productCategoryName' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'productFamilyID' and ApiActive=0
	update ModEntityDef set ApiActive=1 where TableName ='productCategory' and FieldName = 'title_defaultTranslation' and ApiActive=0

	update ModEntityDef set ApiName='active' where TableName ='productCategory' and FieldName = 'active' and ApiName is null
	update ModEntityDef set ApiName='createdBy' where TableName ='productCategory' and FieldName = 'createdBy' and ApiName is null
	update ModEntityDef set ApiName='createdByPerson' where TableName ='productCategory' and FieldName = 'createdByPerson' and ApiName is null
	update ModEntityDef set ApiName='lastUpdatedBy' where TableName ='productCategory' and FieldName = 'lastUpdatedBy' and ApiName is null
	update ModEntityDef set ApiName='productCategoryFactor' where TableName ='productCategory' and FieldName = 'productCategoryFactor' and ApiName is null
	update ModEntityDef set ApiName='productCategoryID' where TableName ='productCategory' and FieldName = 'productCategoryID' and ApiName is null
	update ModEntityDef set ApiName='productCategoryName' where TableName ='productCategory' and FieldName = 'productCategoryName' and ApiName is null
	update ModEntityDef set ApiName='productFamilyID' where TableName ='productCategory' and FieldName = 'productFamilyID' and ApiName is null
	update ModEntityDef set ApiName='title_defaultTranslation' where TableName ='productCategory' and FieldName = 'title_defaultTranslation' and ApiName is null
END