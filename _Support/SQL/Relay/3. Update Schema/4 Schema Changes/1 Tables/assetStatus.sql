
if not exists(select top 1 * from information_schema.tables where table_name='AssetStatus')
	begin
		CREATE TABLE [dbo].[AssetStatus](
			[assetStatusID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
			[name] [varchar](50) NOT NULL,
			[created] [datetime] NOT NULL CONSTRAINT [DF_AssetStatus_created]  DEFAULT (getdate()),
			[createdBy] [int] NOT NULL
		)
	end
GO
