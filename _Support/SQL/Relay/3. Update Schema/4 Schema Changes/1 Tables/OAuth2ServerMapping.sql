IF object_id('OAuth2ServerMapping') is null  
BEGIN
	Create Table [dbo].[OAuth2ServerMapping](
		OAuth2ServerId nvarchar(256) NOT NULL FOREIGN KEY REFERENCES [dbo].[OAuth2Server] ([clientIdentifier]) on delete cascade,
		relaywareField nVarChar(512) NOT NULL,
		useAssertion bit not null,
		foreignValue nVarChar(512) not null default 0
		)
END