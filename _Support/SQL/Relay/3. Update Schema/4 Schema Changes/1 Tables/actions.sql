/* LID 4314 */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name='actions' AND column_name='actionCampaignID') 	
Begin
	ALTER TABLE actions
	ADD actionCampaignID  int null
End
GO

/* 2012-09-11 WAB Increase size of column to reflect MaxLength on Form (after error on Kerio)*/
alter table actions alter column requestedby nvarchar(100)


/* 2014-11-24 WAB 	Change notes from nText to nVarchar
					Then recreate MRAudit trigger which was hard coded to exclude the nText field (and so wasn't picking up any new fields added to the table 
*/
IF Exists (select 1 from information_schema.columns where table_name = 'actions' and column_name = 'notes' and data_Type like  '%text')
BEGIN
	alter table actions alter column notes nvarchar(max)
	alter table actionsdel alter column notes nvarchar(max)
END	

IF Exists (select 1 from sys.triggers where name = 'Actions_MRAudit' and OBJECT_DEFINITION(object_id) like '%as notes%')  /* the check for 'as notes' is a hack way of identifying the original trigger */ 
BEGIN
	Drop trigger Actions_MRAudit
	exec generate_MRAudit @TableName = 'actions', @mnemonic = 'ACT'

END