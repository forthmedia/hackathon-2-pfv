if not exists (select 1 from sysobjects where name='entitySelection' and type='u')
BEGIN
CREATE TABLE [dbo].[entitySelection](	
	[personID] [int] NOT NULL,
	[selectionID] [int] NOT NULL,
	PRIMARY KEY (personID,selectionID) 
) 
END
