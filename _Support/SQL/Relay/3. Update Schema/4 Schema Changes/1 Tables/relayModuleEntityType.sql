
if not exists (select 1 from information_schema.tables where table_name='relayModuleEntityType')
begin
	CREATE TABLE [dbo].[relayModuleEntityType](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[relayModuleID] [int] NOT NULL,
		[entityTypeID] [int] NOT NULL,
	 CONSTRAINT [PK_relayModuleEntityType] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	
	ALTER TABLE [dbo].[relayModuleEntityType]  WITH CHECK ADD  CONSTRAINT [FK_relayModuleEntityType_relayModule] FOREIGN KEY([relayModuleID])
	REFERENCES [dbo].[relayModule] ([moduleID])
	
	
	ALTER TABLE [dbo].[relayModuleEntityType] CHECK CONSTRAINT [FK_relayModuleEntityType_relayModule]
	
	ALTER TABLE [dbo].[relayModuleEntityType]  WITH CHECK ADD  CONSTRAINT [FK_relayModuleEntityType_schemaTable] FOREIGN KEY([entityTypeID])
	REFERENCES [dbo].[schemaTableBase] ([entityTypeID])
	
	ALTER TABLE [dbo].[relayModuleEntityType] CHECK CONSTRAINT [FK_relayModuleEntityType_schemaTable]
	
end

