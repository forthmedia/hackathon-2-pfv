/*  2015-12-01 WAB PROD2015-26 Visibility Project. 
   Add new columns to define the link between usergroups and flags
*/

IF not exists (select 1 from information_schema.columns where table_name = 'usergroup' and column_name = 'conditionSQL') 
BEGIN
	alter table usergroup add conditionSQL nvarchar (max)
	
END



IF not exists (select 1 from information_schema.columns where table_name = 'userGroup' and column_name = 'conditionValidated') 
BEGIN
	alter table usergroup add conditionValidated bit default 0
	declare @sqlCMD nvarchar(max) = '
		update usergroup set conditionValidated = 0
	'
	exec (@sqlCMD)
	
END
