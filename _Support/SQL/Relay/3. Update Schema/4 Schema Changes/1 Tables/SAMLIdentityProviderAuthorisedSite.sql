IF object_id('SAMLIdentityProviderAuthorisedSite') is null  
BEGIN
	Create Table [dbo].[SAMLIdentityProviderAuthorisedSite](
		SAMLIdentityProviderId int NOT NULL FOREIGN KEY REFERENCES [dbo].[SAMLIdentityProvider] ([SAMLIdentityProviderId]) on delete cascade,
		siteDefID int NOT NULL FOREIGN KEY REFERENCES [dbo].[siteDef] ([siteDefID]) on delete cascade
		)
		
	--prior to the creation of this table "internal" was the default assumed authorisation. We now must make this explicit
	insert into SAMLIdentityProviderAuthorisedSite(SAMLIdentityProviderID,siteDefId)
	select SAMLIdentityProviderID,siteDefId from siteDef 
	join SAMLIdentityProvider on active=1 --intentionally no real join condition, want the combination of site X provider
	where siteDef.isInternal=1 

END
