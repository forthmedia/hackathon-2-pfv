/* add asset table */
if not exists (select 1 from information_schema.tables where table_name = 'Asset')
CREATE TABLE [dbo].[Asset](
	[assetID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50)  NOT NULL,
	[price] [decimal](18, 2) NULL,
	[productID] [int] NOT NULL,
	[entityID] [int] NULL,
	[contactPersonID] [int] NULL,
	[partnerEntityID] [int] NOT NULL,
	[partnerEntityTypeID] [int] NOT NULL,
	[distiEntityID] [int] NULL,
	[distiEntityTypeID] [int] NULL,
	[countryID] [int] NULL,
	[currencyISOCode] [varchar](3)  NULL,
	[statusID] [int] NULL,
	[quantity] [int] NOT NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NOT NULL,
	[renewable] [bit] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_Asset_active]  DEFAULT (1),
	[crmAssetID] [varchar](50)  NULL,
	[renewalOppID] [int] NULL,
	[created] [datetime] NOT NULL CONSTRAINT [DF_Asset_created]  DEFAULT (getdate()),
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL CONSTRAINT [DF_Asset_lastUpdated]  DEFAULT (getdate()),
	[lastUpdatedBy] [int] NOT NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[assetID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO


IF  EXISTS (SELECT * FROM sysindexes WHERE name = N'assetPartnerEntityIDX')
	drop index asset.assetPartnerEntityIDX 
	
CREATE NONCLUSTERED INDEX [assetPartnerEntityIDX] ON [dbo].[Asset] 
(
	[partnerEntityID] ASC,
	[partnerEntityTypeID] ASC
) ON [PRIMARY]
GO

alter table asset alter column partnerEntityID int null
if not exists (select 1 from sysobjects where xtype = 'D' AND name = 'DF_partnerEntityTypeID')
alter table asset add constraint DF_partnerEntityTypeID default 2 for partnerEntityTypeID

alter table asset alter column renewable bit null


/* track changes on the endDate field of the asset */
exec generate_mrAudit @tablename='Asset', @mnemonic='Ass'
GO

update ModEntityDef set ApiActive=1 where TableName ='Asset' and FieldName = 'crmAssetID' --BF256 RJT
