/* Case 451230 - cannot delete profile group - update column types on xFlagGroup to match the FlagGroup table */

IF exists (select 1 from information_schema.columns where table_name = 'xFlagGroup' and column_name = 'Name' and data_Type='varchar')
BEGIN
    ALTER TABLE xFlagGroup ALTER COLUMN Name nvarchar(500) null
END

/* need to unbind default on Description before altering the column */
EXEC sp_unbindefault N'[dbo].[xFlagGroup].[Description]'
GO

IF exists (select 1 from information_schema.columns where table_name = 'xFlagGroup' and column_name = 'Description' and data_Type='varchar')
BEGIN
    ALTER TABLE xFlagGroup ALTER COLUMN Description nvarchar(2000) null
END

/* bind it again */
EXEC sp_bindefault N'[dbo].[xFlagGroup_Description_D]', N'[dbo].[xFlagGroup].[Description]'
GO

IF exists (select 1 from information_schema.columns where table_name = 'xFlagGroup' and column_name = 'Notes' and data_Type='varchar')
BEGIN
    ALTER TABLE xFlagGroup ALTER COLUMN Notes nvarchar(200) null
END

/* Already nvarchar but need to increase the character_maximum_length */
IF exists (select 1 from information_schema.columns where table_name = 'xFlagGroup' and column_name = 'helpText' and data_Type like '%varchar%' and character_maximum_length=100)
BEGIN
    ALTER TABLE xFlagGroup ALTER COLUMN helpText nvarchar(160) null
END
