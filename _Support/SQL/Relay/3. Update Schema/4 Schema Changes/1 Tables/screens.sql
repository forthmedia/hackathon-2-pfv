if not exists (select * from information_schema.columns where table_name='screens' and column_name='LastUpdatedBy')  
      alter TABLE [dbo].screens add LastUpdatedBy Int NOT NULL Default(0)

if not exists (select * from information_schema.columns where table_name='screens' and column_name='LastUpdated')  
      alter TABLE [dbo].screens add LastUpdated datetime NOT NULL DEFAULT (getdate())

if exists (select * from information_schema.columns where table_name='screens' and column_name='UpdateMessage' and data_type='text')  
      ALTER TABLE [dbo].screens ALTER COLUMN UpdateMessage NVARCHAR(4000) NULL	--I set to 4000 not MAX for pre-2005 compatibility; I beleive the column is redundant so it shouldn't matter
		

/* WAB 2014-04-01 Xirrus needed parameters column longer */
alter table screens alter column parameters nVarchar(max)

alter table screens alter column viewEdit varchar(100)

if not exists(select 1 from information_schema.columns where table_name='screens' and column_name='lastUpdatedByPerson')
alter table screens add lastUpdatedByPerson int not null constraint DF_Screens_LastUpdatedByPerson default (0) 

/* NJH 2016/12/01 - added auditing */
exec generate_MRAudit @TableName = 'screens'
