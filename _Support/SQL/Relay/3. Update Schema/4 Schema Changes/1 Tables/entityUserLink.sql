if not exists (select 1 from sysobjects where name='entityUserLink' and type='u')
begin
	CREATE TABLE [dbo].[entityUserLink](
		[entityUserLinkID] [int] IDENTITY(1,1) NOT NULL,
		[linkID] [int] NOT NULL,
		[entityTypeID] [int] NOT NULL,
		[entityID] [int] NOT NULL,
		[active] [bit] NOT NULL,
		[created] [datetime] NOT NULL,
		[createdBy] [int] NOT NULL,
		[lastUpdated] [datetime] NOT NULL,
		[lastUpdatedBy] [int] NOT NULL,
	 CONSTRAINT [PK_entityUserLink] PRIMARY KEY CLUSTERED 
	(
		[entityUserLinkID] ASC
	) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[entityUserLink] ADD  CONSTRAINT [DF_entityUserLink_entityTypeID]  DEFAULT ((0)) FOR [entityTypeID]

	ALTER TABLE [dbo].[entityUserLink] ADD  CONSTRAINT [DF_entityUserLink_active]  DEFAULT ((1)) FOR [active]

	ALTER TABLE [dbo].[entityUserLink] ADD  CONSTRAINT [DF_entityUserLink_created]  DEFAULT (getdate()) FOR [created]

	ALTER TABLE [dbo].[entityUserLink] ADD  CONSTRAINT [DF_entityUserLink_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
end