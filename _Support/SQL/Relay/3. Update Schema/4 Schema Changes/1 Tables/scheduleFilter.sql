/* create ScheduleFilter table */
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name=  'ScheduleFilter')
BEGIN
CREATE TABLE [dbo].[ScheduleFilter](
	[ScheduleFilterID] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [int] NULL,
	[ColumnName] [varchar](50) NULL,
	[Operator] [varchar](50) NULL,
	[FilterValue] [nvarchar](50) NULL,
 CONSTRAINT [PK_ScheduleFilter] PRIMARY KEY CLUSTERED 
(
	[ScheduleFilterID] ASC
)) ON [PRIMARY]
END
GO

ALTER TABLE dbo.schedulefilter alter column FilterValue nvarchar(MAX)	--increased from 50 to cope with long lists of comma delimited ids from dropdowns
