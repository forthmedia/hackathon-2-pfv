if not exists(select 1 from information_schema.tables where table_name='score')
begin
CREATE TABLE [dbo].[score](
	[scoreID] [int] IDENTITY(1,1) NOT NULL,
	[triggeringScoreRuleVersionID] [int], --can be null for manual entries
	[triggeringActivityID] [int],--can be null for manual entries
	[score] [int]  NOT NULL,
	[created] [datetime] NOT NULL DEFAULT getdate(),
	[createdBy] [int] NOT NULL,
	[lastUpdated] [datetime] NOT NULL  DEFAULT getdate(),
	[lastUpdatedBy] [int] NOT NULL,
	[lastUpdatedByPerson] [int] NOT NULL,
 CONSTRAINT [PK_score] PRIMARY KEY CLUSTERED 
(
	[scoreID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE INDEX index_score_created ON score (created)

end
GO

IF not EXISTS (SELECT *  FROM sys.indexes  WHERE name='score_triggeringScoreRuleVersionID_index' AND object_id = OBJECT_ID('[dbo].[score]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX score_triggeringScoreRuleVersionID_index
	ON dbo.score (triggeringScoreRuleVersionID)
End

IF not EXISTS (SELECT *  FROM sys.indexes  WHERE name='score_triggeringActivityID_index' AND object_id = OBJECT_ID('[dbo].[score]'))
Begin
	-- required for the activity score widget which heavily gets organisation data
	CREATE INDEX score_triggeringActivityID_index
	ON dbo.score (triggeringActivityID)
End
