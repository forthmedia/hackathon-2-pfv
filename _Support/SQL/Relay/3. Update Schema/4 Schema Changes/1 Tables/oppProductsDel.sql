/*
NJH 2015/12/18 - renamed to opportunityProduct (oppProducts is now a view)
if not exists (select * from information_schema.columns where table_name='oppProductsDel' and column_name='lastUpdatedByPerson')  
      alter TABLE [dbo].oppProductsDel add [lastUpdatedByPerson] int NULL

/* NJH 2015/07/16 - for vEntityname */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[oppProductsDel]') AND name = N'crmOppProductID_idx')
BEGIN
	CREATE INDEX crmOppProductID_idx ON oppProductsDel (crmOppProductID);
END	
*/