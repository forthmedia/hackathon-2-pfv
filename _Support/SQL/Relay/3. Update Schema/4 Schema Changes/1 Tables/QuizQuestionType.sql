IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuizQuestionType]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[QuizQuestionType] (
	[QuestionTypeID] int IDENTITY(1,1) NOT NULL  
	, [QuestionTypeTextID] varchar(32)  NULL  
	)


ALTER TABLE [dbo].[QuizQuestionType] ADD CONSTRAINT [QuizQuestionType_PK] PRIMARY KEY CLUSTERED (
[QuestionTypeID]
)
END
GO

/* STCR Phase 2 SCORM SQL */
IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionType') AND 
NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name= 'QuizQuestionType' AND column_name='relayQuizCompatible') 
	ALTER TABLE QuizQuestionType add relayQuizCompatible bit NOT NULL DEFAULT 1 --assumes that existing rows are relayQuiz types 'checkbox' and 'radio'
GO