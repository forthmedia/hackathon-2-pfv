

/* add a default of 0 for the countryID - added for the api as countryId is not a required field 
	WAB 2016-03-15 removed this code - countryID column has been removed
	if not exists(select c.name from sys.all_columns a inner join 
			sys.tables b on a.object_id = b.object_id inner join
			sys.default_constraints c on a.default_object_id = c.object_id
		where b.name='product'
		and a.name = 'countryID')
	ALTER TABLE Product ADD  DEFAULT ((0)) FOR CountryID
*/

/* WAB 2012-11-06 431755 Make Description NVarchar) */
alter table product alter column description nvarchar(4000)
GO


/*  WAB 2013-12-09 create auditing here rather than later, so that we can make an index on the Del table  */
exec generate_mrAudit @tablename='product',@mnemonic='PROD'
GO

if not exists (select 1 from information_schema.columns where table_name = 'product' and column_name = 'active') 
	alter table product add active bit not null default 1

/* WAB 2014-02-18 this active column needs to be added to the del table as well */
if not exists (select 1 from information_schema.columns where table_name = 'productdel' and column_name = 'active') 
	alter table productdel add active bit not null default 1

GO

/* create index on crmProductID column */
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND name = N'idx_crmProductID')
DROP INDEX [idx_crmProductID] ON [dbo].[Product] WITH ( ONLINE = OFF )
GO

/*
CREATE NONCLUSTERED INDEX [idx_crmProductID] ON [dbo].[Product] 
(
	[crmProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO*/

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND name = N'crmProductID_unique')
BEGIN
	update Product set crmProductID = null where crmProductID = ''
	CREATE unique INDEX crmProductID_unique ON Product (crmProductID) WHERE crmProductID IS NOT NULL ;
END	
GO


/*  WAB 2013-12-09 Also add the title_defaultTranslation field so that this field can be included in vEntityName (would otherwise be created later) */
if not exists (select 1 from information_schema.columns where table_name = 'product' and column_name = 'title_defaultTranslation') 
	BEGIN
		alter table product add title_defaultTranslation nVarChar(400)
		alter table productdel add title_defaultTranslation nVarChar(400)
	END	 


/* /02/06/2014 - Connector */	
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_product_productGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[product]'))
ALTER TABLE [dbo].[product] DROP CONSTRAINT [FK_product_productGroup]
GO

update product set productGroupID = null where productGroupID = 0

ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_productGroup] FOREIGN KEY([productGroupID])
REFERENCES [dbo].[productGroup] ([productGroupID])
GO

ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_productGroup]
GO

-- PROD2015-364 - products country restrictions
-- add hasCountryScope column
if not exists (select * from information_schema.columns where table_name = 'product' and column_Name = 'hasCountryScope')
BEGIN
    alter table product add hasCountryScope bit default 0
    alter table productdel add hasCountryScope bit default 0
END
GO

-- set hasCountryScope to 0 if NULL and make it not null
update product set hasCountryScope = 0 where hasCountryScope is null
update productdel set hasCountryScope = 0 where hasCountryScope is null

GO
	ALTER TABLE [dbo].[product] ALTER COLUMN [hasCountryScope] bit NOT NULL
	ALTER TABLE [dbo].[productdel] ALTER COLUMN [hasCountryScope] bit NOT NULL

-- insert country scope for appropriate products
if exists(select 1 from information_schema.columns where table_name='product' and column_name='countryID')
BEGIN
	declare @SQL nvarchar(max) =
		'
		insert into countryScope (entity, entityid, countryid, entityTypeId, permission)
		select ''product'', p.productId, p.countryId, 24, 1  
		from 
			product p 
		where 
			p.countryId is not null and p.countryId != 0 and p.countryId != 37
			and not exists (select cs.entityId from countryScope cs where cs.entity = ''product'' and cs.entityID = p.productid and cs.countryId=p.countryId)
		'	
	exec (@SQL)
END
GO

-- rename countryId column so its no longer used
if exists(select 1 from information_schema.columns where table_name='product' and column_name='countryID')
begin
	exec sp_RENAME 'product.countryID' , 'countryID_notused', 'COLUMN';
end
GO

