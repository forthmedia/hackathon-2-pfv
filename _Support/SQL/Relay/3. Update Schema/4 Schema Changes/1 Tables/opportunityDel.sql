/* 2013/02/05	YMA		104 Forecast Opportunity */
if not exists (select 1 from information_schema.columns where table_name = 'opportunityDel' and column_name = 'forecast')
ALTER TABLE [dbo].opportunityDel ADD	[forecast] bit NULL
GO

/*  WAB 2013-12-09 added index so that opportunityDel can be searched quickly in vEntityName view */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OpportunityDel]') AND name = N'OpportunityID_Idx')

	CREATE NONCLUSTERED INDEX [OpportunityID_Idx] ON [dbo].[OpportunityDel]
	(
		[OpportunityID] ASC
	) 
	INCLUDE (detail, CRMOppID)
	ON [PRIMARY]

GO


exec generate_mrAuditDel @tablename='opportunity'

/* NJH 2015/07/16 - for vEntityname */
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[opportunityDel]') AND name = N'crmOppID_idx')
BEGIN
	CREATE INDEX crmOppID_idx ON OpportunityDel (crmOppID);
END	
GO