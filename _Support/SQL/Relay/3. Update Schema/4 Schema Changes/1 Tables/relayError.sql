
/*
Add dataStruct and other column to relayerror
*/
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'datastruct') 
BEGIN
	Alter table relayerror add  dataStruct Text
END
GO
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'serverName') 
BEGIN
	Alter table relayerror add  serverName varchar(50)
END
GO
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'instanceName') 
BEGIN
	Alter table relayerror add  instanceName  varchar(50)
END
GO
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'remoteErrorID') 
BEGIN
	Alter table relayerror add  remoteErrorID int null 
END

if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'source') 
BEGIN
	Alter table relayerror add  source  varchar(50)
END
GO
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'type') 
BEGIN
	Alter table relayerror add  type  varchar(100)
END
GO
if not exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name = 'relayVersion') 
BEGIN
	Alter table relayerror add  relayVersion  varchar(50)
END
	Alter table relayerror alter column relayVersion  varchar(100)


/* WAB 2011/04/27 add indexes to the relayError tables */
if not exists (select 1 from sysIndexes where name = 'IX_RelayError_FileNameLineNumber' )
BEGIN
	CREATE NONCLUSTERED INDEX IX_RelayError_FileNameLineNumber ON dbo.RelayError
		(
		filename,
		linenumber
		)  ON [PRIMARY]
END

GO

if not exists (select 1 from sysIndexes where name = 'IX_RelayError_ErrorDateTime' )
BEGIN
CREATE NONCLUSTERED INDEX IX_RelayError_ErrorDateTime ON dbo.RelayError
	(
	ErrorDateTime
	) ON [PRIMARY]
END
GO

/*
WAB 2012-09-27 add a TTL column - TimeToLive
*/
if not exists(select 1 from information_schema.columns where table_name='relayError' and column_name='TTL')
alter table relayError	 add  TTL Integer
GO
/* and now update all existing relayErrors to have a non null TTL 
	 
  */
if exists (select 1 from relayError where TTL is null)
BEGIN

	Update relayError
	SET TTL = 
		CASE 
			 WHEN source is null then 1  -- anything pre-8.3 will have source = null and can be deleted immediately
			 WHEN source = 'javascript' THEN 90
			 WHEN source = 'coldfusion' THEN 400
			 WHEN source = 'information' THEN 45
			 WHEN source = 'warning' THEN 90
			 WHEN source = 'error' THEN 120
			 WHEN type = 'Missing File' THEN 30
			 WHEN type = 'Flag Rights Overriden' THEN 30
			 ELSE null
		END	 

END

/*  Now add a not null constraint on the ttl column */
ALTER TABLE dbo.RelayError alter column ttl int not null
if not exists (select 1 from sys.all_columns a inner join sys.tables b on a.object_id = b.object_id
	inner join sys.default_constraints c on a.default_object_id = c.object_id 
	where b.name='relayerror' and a.name = 'TTL')
alter table relayerror ADD  DEFAULT (60) FOR TTL


/* WAB 2016-01-13 add visitID to errors */
if not exists(select 1 from information_schema.columns where table_name='relayError' and column_name='visitid')
	alter table relayerror add visitid int null

/* WAB 2016-04-06 make datastruct nvarchar - amazing hadn't been done before */
if exists (select 1 from information_schema.columns where table_name = 'relayerror' and column_name='dataStruct' and data_type = 'text')
	alter table relayerror alter column datastruct nvarchar(max)
