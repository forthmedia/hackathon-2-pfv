

/* RJT 2015-10-30 In discussions with WAB this table (which doesn't conform to the other Flagdata tables was causing issues with PROD2015-272)
 * is no longer used, so should be dropped  */

delete from flagType where DataTableFullName='TextSubEntityflagData';

IF exists (select 1 from sysObjects where name = 'textSubentityFlagdata ')
	DROP table textSubentityFlagdata;
