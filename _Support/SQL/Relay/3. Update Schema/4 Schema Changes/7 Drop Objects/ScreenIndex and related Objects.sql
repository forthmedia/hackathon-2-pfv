/* WAB 2015-02-25 no longer need screenIndex table and associated functions  */
IF exists (select 1 from sysObjects where name = 'ScreenIndex')
	DROP table screenIndex
	
IF exists (select 1 from sysObjects where name = 'updateScreenIndex')
	DROP PROCEDURE updateScreenIndex 

IF exists (select 1 from sysObjects where name = 'updateScreenIndexForChild')
	DROP PROCEDURE updateScreenIndexForChild

IF exists (select 1 from sysObjects where name = 'updateScreenIndexTree')
	DROP PROCEDURE updateScreenIndexTree

/* These triggers only called updateScreenIndexForChild, so can be deleted */
IF exists (select 1 from sysObjects where name = 'screens_ITrig')
	DROP TRIGGER screens_ITrig

IF exists (select 1 from sysObjects where name = 'screens_UTrig')
	DROP TRIGGER screens_UTrig

IF exists (select 1 from sysObjects where name = 'screens_DTrig')
	DROP TRIGGER screens_DTrig
