/* 
WAB 2015-11-18 get rid of this function, replaced with better named getCountryScopeList.sql
*/

if (object_id('fn_getCountryRightsV2') is not null)
BEGIN
	drop function fn_getCountryRightsV2
END 

if (object_id('fn_getCountryRights') is not null)
BEGIN
	drop function fn_getCountryRights
END 
