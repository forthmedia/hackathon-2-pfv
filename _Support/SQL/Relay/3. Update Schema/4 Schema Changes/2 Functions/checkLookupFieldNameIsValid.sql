/*
WAB 2016-12-12	Altered checkLookupFieldNameIsValid so that fieldname could be more than 20 characters
				In order to change, had to implement a way to recreate constraints which get dropped
*/

-- drop existing check constraints that are using the function
    SELECT
	      'ALTER TABLE ' + RTRIM(o.name) + ' drop constraint ' +  cc.name AS dropConstraintSql
	    , 'ALTER TABLE ' + RTRIM(o.name) + ' add constraint ' +  cc.name + ' check ' + cc.Definition AS addConstraintSql
	INTO 
		##temp_Constraints
    FROM
       sys.check_constraints cc
	inner join sys.objects o on o.object_id = cc.parent_object_id
	inner join sys.Columns c on  o.object_id = c.object_id and c.column_id = cc.parent_column_id
	where cc.type = 'C'
	and cc.Definition like '%ChecklookupFieldNameIsValid%'
        

declare @constraintSql nvarchar(2000)

DECLARE constraintCursor CURSOR LOCAL STATIC FORWARD_ONLY FOR
	select dropConstraintSql from ##temp_Constraints
        
OPEN constraintCursor
fetch next from constraintCursor into @constraintSql

while @@FETCH_STATUS = 0
begin
	exec sp_executesql @statement=@constraintSql
	fetch next from constraintCursor into @constraintSql
end

CLOSE constraintCursor
DEALLOCATE constraintCursor


IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[ChecklookupFieldNameIsValid]') AND type = N'FN')
DROP FUNCTION [dbo].[ChecklookupFieldNameIsValid]
GO

CREATE FUNCTION [dbo].[ChecklookupFieldNameIsValid] (@fieldvalue int, @fieldname sysname)
RETURNS BIT AS
BEGIN
    IF (EXISTS (SELECT 1 FROM lookupList WHERE fieldname = @fieldname and lookupId = @fieldvalue) or @fieldvalue is null)
    BEGIN
        RETURN 1;
    END

    RETURN 0;

END

GO

declare @constraintSql nvarchar(2000)

DECLARE constraintCursor CURSOR LOCAL STATIC FORWARD_ONLY FOR
	select addConstraintSql from ##temp_Constraints
        
OPEN constraintCursor
fetch next from constraintCursor into @constraintSql

while @@FETCH_STATUS = 0
begin
	exec sp_executesql @statement=@constraintSql
	fetch next from constraintCursor into @constraintSql
end

CLOSE constraintCursor
DEALLOCATE constraintCursor

drop table ##temp_Constraints

