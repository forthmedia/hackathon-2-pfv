createPlaceHolderObject @objectName = 'fn_urlencode', @objectType = 'FUNCTION'

GO

ALTER FUNCTION [dbo].[fn_urlencode] (@string nvarchar(max))
RETURNS nvarchar(max)
AS

/*
ACPK	2015-10-26

Convert a string into a URL-encoded format
Source: http://sqlsunday.com/2013/04/07/url-encoding-function/

WAB 2016-10-17	PROD2016-2544 Alter this procedure to support nVarchar 
				Replace dbo.fn_char2hex which couldn't support nVarchar with a new function dbo.fn_char2percentEncoded()

*/

BEGIN;
    DECLARE @offset int, @char nchar(1);
    
     --- Replace % with %25, so that we can skip replacing the
    --- % character once we're looping through the string
    SET @string=REPLACE(@string, '%', dbo.fn_char2percentEncoded('%'));
    
    --- Loop through the @string variable, using PATINDEX() to look
    --- for non-standard characters using a wildcard. When no more
    --- occurrences are found, PATINDEX() will return 0, and the WHILE
    --- loop will end
    SET @offset=PATINDEX('%[^A-Z0-9.\-\%]%', @string);
    WHILE (@offset!=0) BEGIN;
        SET @char=SUBSTRING(@string, @offset, 1);
        
        --- Replace the non-standard characters with URL encoded
        --- equivalents:
        SET @string=REPLACE(@string, @char, dbo.fn_char2percentEncoded(@char));

        --- Find the next occurrence, if any:
        SET @offset=PATINDEX('%[^A-Z0-9.\-\%]%', @string);
    END

    --- Done
    RETURN @string;
END;

GO