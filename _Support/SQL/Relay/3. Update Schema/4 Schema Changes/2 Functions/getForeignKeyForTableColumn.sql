/****** Object:  UserDefinedFunction [dbo].[getForeignKeyForTableColumn]    Script Date: 12/04/2014 10:11:29 ******/
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getForeignKeyForTableColumn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[getForeignKeyForTableColumn]
GO

/****** Object:  UserDefinedFunction [dbo].[getForeignKeyForTableColumn]    Script Date: 12/04/2014 10:11:29 ******/
CREATE FUNCTION [dbo].[getForeignKeyForTableColumn] (@table as varchar(250), @column as varchar(250))
RETURNS varchar(250)
AS
BEGIN
	declare @result varchar(250)
	
    select top 1 @result = fkName from 
		(	
		select 
			referencedTable + '.' + referencedColumn fkName,
			sortIdx 
		from 
			dbo.getForeignKeysForTable(@table)
		where
			parentColumn = @column	
		) fkeys
		order by sortIdx
			
	return @result

END
GO