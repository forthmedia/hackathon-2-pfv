/* a function to get the the textId column for a given table.
 */
IF EXISTS (select 1 from sysobjects where name = 'getTableTextIDColumn')
	DROP FUNCTION getTableTextIDColumn
GO

CREATE FUNCTION [dbo].[getTableTextIDColumn] (@tablename as varchar(250))
RETURNS varchar(250)
AS
BEGIN

	return dbo.getExtendedProperty('table',@tablename,null,null,'textIDColumn')
END

GO