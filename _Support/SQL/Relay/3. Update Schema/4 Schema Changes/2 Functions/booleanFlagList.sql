ALTER   FUNCTION [dbo].[BooleanFlagList] (@flagGroupID int,@EntityID int,@returnValue varchar(20)='name')  
RETURNS nvarchar(max) AS  
BEGIN 

/*
WAB 2014-05-07 Deal with null/blank flagTextIDs, return the flagID instead
AHL 2014-12-19 Case 442967 Phrases not being Resolved in Report Designer 
WAB 2016-06-20 PROD2016-1295 Get rid of cursor.  Improves performance by at least a factor of 2 and resolved a lock/blockage problem in connector
				Test to prove that does same as old version at /_support/tests/SQL/flagStoredProcedures/BooleanFlagList-ProveNewVersionSameAsOld.sql
*/



DECLARE @returnString nvarchar(max) = ''
DECLARE @flagName nvarchar(200)
DECLARE @n int


if (@returnValue = 'name')
begin
	SELECT 
		@returnString += 
			case when @returnString <> '' then ',' else '' end
			+ CASE WHEN isNull(f.NamePhraseTextID,'') = '' 
					THEN f.Name 
					ELSE 'phr_'+f.NamePhraseTextID 
				END
	FROM booleanflagdata b 
		INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
	WHERE b.entityID = @EntityID
	ORDER BY f.orderingIndex
end
/* START  2014-12-19 AHL Case 442967 Phrases not being Resolved in Report Designer */
else if (@returnValue = 'flagName')
begin
	SELECT 
		@returnString += 
			case when @returnString <> '' then ',' else '' end
			+ f.Name 
		FROM booleanflagdata b 
		INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
		WHERE b.entityID = @EntityID
		ORDER BY f.orderingIndex
end
/* END 2014-12-19 AHL Case 442967 Phrases not being Resolved in Report Designer */
else
begin
	SELECT 
		@returnString += 
			case when @returnString <> '' then ',' else '' end
			+ case when isNull(f.flagTextID,'') = '' then convert(varchar,f.flagID) else f.flagTextID end  
	FROM booleanflagdata b 
		INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
	WHERE b.entityID = @EntityID
	ORDER BY f.orderingIndex
end

Return @returnString

END