/* WAB/NJH 2015-12-16 - a function to return a UTC datetime to server time */
IF Exists (select 1 from sysobjects where name = 'convertUTCISO8601DateTimeToServerDateTime' and xtype= 'FN')
	DROP FUNCTION convertUTCISO8601DateTimeToServerDateTime
GO

CREATE FUNCTION convertUTCISO8601DateTimeToServerDateTime (@datetime varchar(30)) RETURNS dateTime
AS
BEGIN
	/* the replacing of T with T is to fix issue when the incoming T is in lower case. It then can't convert to datetime */
	return CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,replace(replace(@datetime,'Z',''),'T','T')),DATENAME(TzOffset, SYSDATETIMEOFFSET())))
END
