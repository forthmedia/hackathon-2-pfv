-- drop existing default constraints that are using the function
declare @constraintSql nvarchar(2000)

DECLARE constraintCursor CURSOR LOCAL STATIC FORWARD_ONLY FOR
    SELECT
    'ALTER TABLE ' + RTRIM(o.name) + ' drop constraint ' +  dc.name AS constraintSql
    --cc.Definition as constraintText,c.name as columnName
    FROM
       sys.default_constraints dc
	inner join sys.objects o on o.object_id = dc.parent_object_id
	inner join sys.Columns c on  o.object_id = c.object_id and c.column_id = dc.parent_column_id
	where dc.type = 'D'
	and dc.Definition like '%getDefaultLookupValue%'
        
OPEN constraintCursor
fetch next from constraintCursor into @constraintSql

while @@FETCH_STATUS = 0
begin
	exec sp_executesql @statement=@constraintSql
	fetch next from constraintCursor into @constraintSql
end

CLOSE constraintCursor
DEALLOCATE constraintCursor

/*
Create a new function getDefaultLookupValue
*/
IF EXISTS (select 1 from sysobjects where name = 'getDefaultLookupValue')
	DROP FUNCTION getDefaultLookupValue
GO

CREATE
		 Function [dbo].[getDefaultLookupValue] (
			@fieldName varchar(50)
		) 
		returns int as

BEGIN

	return (select lookupID from lookupList where isDefault=1 and fieldName = @fieldName)

END
