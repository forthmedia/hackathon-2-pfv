/*
WAB 2011/11/23 alter to not use sys. objects which appears to no longer be supported
*/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[dateAtMidnight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[dateAtMidnight]
GO

CREATE FUNCTION [dbo].[dateAtMidnight] (@datetimeIn datetime)  
/*
Function: dateAtMidnight
Author: PPB  
Date:  Oct 2010
Purpose:  return the date with a time of 00:00:00
*/

returns datetime AS

begin

declare @result datetime

select @result = CAST(FLOOR(CAST(@datetimeIn AS FLOAT)) AS DATETIME)

return @result
end


GO


