createPlaceHolderObject @objectName = 'XMLFormat', @objectType = 'FUNCTION'

GO

ALTER FUNCTION dbo.XMLFormat (@string Nvarchar(max))
RETURNS nVarchar(max)
AS
/*
XMLFormat ()
WAB/NJB 2015-01-19 
Escapes characters invalid in XML text and Attributes  ( < > & ")

*/

BEGIN
	/*	note that the clever 
				SELECT @string FOR XML PATH('') 
		encodes < > and & but not ".
		This is probably because " is allowed in the XML Text.
		We wanted to deal with attributes as well, where " is not allowed
		Therefore we have added an replace statement which deals with "
	*/
		
	SELECT @string = replace (CONVERT(NVARCHAR(MAX), (SELECT @string FOR XML PATH(''))),'"','&quot;')
	RETURN @string
END




