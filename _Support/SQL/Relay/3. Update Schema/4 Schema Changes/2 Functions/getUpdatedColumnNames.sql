createPlaceholderObject 'inlinedtablefunction', 'getUpdatedColumnNames'
GO

alter function [dbo].[getUpdatedColumnNames] (@tablename sysname, @columns_Updated varbinary(50)) returns
TABLE
AS

/*
WAB 2016-03-08
Extracts Names of updated columns from @columns_updated
Could be used in modRegisterTriggerAction and EntityUpsert

*/

RETURN (

	select
		distinct name  /* computed columns can cause duplicates */
	from 
		(
		select 
			colid
			,name
			,byte = convert(int, (colid - 1)/8) + 1 
			,bit = (colid -1) % 8 + 1
		from 
			(
			select 
				colid
				,name
			from syscolumns where id = object_id (@tablename)

			union
		
			select
				referenced_minor_id	
				,c.name
			from
				syscolumns c 
					inner join
				sys.sql_expression_dependencies d on d.referencing_minor_id = c.colid AND d.referenced_id = c.id and isComputed = 1		
			where 
				c.id = object_id(@tablename)
			) as cols
		) as colsAndStuff
	where 
		case when substring(@columns_Updated,byte, 1) & power (2, bit - 1)   > 0  then 1 else 0 end = 1
)

