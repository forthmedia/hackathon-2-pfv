/****** Object:  UserDefinedFunction [dbo].[listGetAt]    Script Date: 03/14/2014 09:19:06 ******/
IF  EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[listGetAt]') AND type = N'FN')
	DROP FUNCTION [dbo].[listGetAt]
GO

CREATE FUNCTION [dbo].[listGetAt](@list as varchar(8000), @pos as int, @delim as varchar(10))
RETURNS varchar(255)
AS
BEGIN
        declare @retval varchar(255)
        if @pos > dbo.listLen(@list, @delim) OR @pos < 1
               set @retval = NULL
        else
        begin
                declare @myPos int
                set @myPos = 1

                while @myPos < @pos
                begin
                        set @myPos = @myPos + 1
                        set @list = right(@list, len(@list) - charindex(@delim, @list))
                end
                set @retval = @list
                if charindex(@delim, @retval) > 0
					set @retval = left(@retval, charindex(@delim, @retval) - 1)
        end

        return @retval
END


GO