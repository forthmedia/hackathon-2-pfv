
if  exists (select 1 from sysobjects where name='dateWithoutMilliseconds' and type='FN')
drop function dbo.dateWithoutMilliseconds
GO

CREATE FUNCTION [dbo].[dateWithoutMilliseconds] (@datetimeIn datetime)  
/*
Function: dateWithoutMilliseconds
Author: NJH  
Date:  FEB 2013
Purpose:  return the date without any milliseconds
*/

returns datetime AS

begin

declare @result datetime

select @result = DATEADD(ms, -DATEPART(ms, @datetimeIn), @datetimeIn)

return @result
end

GO