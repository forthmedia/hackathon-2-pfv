/* a function to get the some extended properties for various objects */
IF EXISTS (select 1 from sysobjects where name = 'getExtendedProperty')
	DROP FUNCTION getExtendedProperty
GO

CREATE FUNCTION [dbo].[getExtendedProperty] (@level1Object as varchar(250) = 'table', @level1name as varchar(250), @level2Object as varchar(250), @level2name as varchar(250), @property as varchar(250))
RETURNS varchar(250)
AS
BEGIN

	declare @result varchar(250)
	
    select @result = cast(value as varchar(250)) from sys.extended_properties p
		left join sys.columns c on major_id = object_ID and column_id = minor_id and c.name=@level2Name and @level2Object='Column'
	where object_id(@level1name) = major_id and p.name= @property
		and isNull(c.column_id,0) = minor_id
			
	return @result

END

GO