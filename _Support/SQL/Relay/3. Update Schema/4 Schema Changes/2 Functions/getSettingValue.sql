/*
Create a new function getSettingValue
*/
IF EXISTS (select 1 from sysobjects where name = 'getSettingValue')
	DROP FUNCTION getSettingValue
GO
CREATE
		 Function [dbo].[getSettingValue] (
			@Name nvarchar(200)
		) 
		returns nvarchar(4000) as

BEGIN

/* 
WAB 2009/05/13
Gets value of a setting from the db  
	doesn't deal with 
	conditions, 
	items which change on a sitebysitebasis 
	items where there is a default set in XML and no value in table
WAB 2010/10/19 Converted to use settings Table from relayvarTable
*/
	declare @Value nvarchar(4000)
	select @Value = Value from settings where Name = @Name

	return @Value


END


