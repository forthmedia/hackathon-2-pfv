/* a function to get the sql used for picklist values for a table.column 
	NJH 2015/06/25  - Get top 1 from validvalues... I now remember that there can be multiple validvalues per field based on country (I think).. Not sure how to best deal
					with this, so get top 1 for now.
	NJH 2015/07/02	- Implemented Will's solution for above problem
	NJH 2016/06/20	JIRA PROD1292 - be consistent in what we return. Am using dataValue and displayValue rather than value/display
	NJH 2016/08/10	JIRA PROD2016-2171 - remove whitespace from valid values
	NJH 2016/10/17	JIRA PROD2016-2538 - only select rows from validvalues where something exists in dataValue column
	NJH 2016/11/14	Part of lead screen rework - change the validFieldValue sql. Keep the last order by rather than removing all of them to preserve ordering.
	NJH 2017/01/20	JIRA PROD2016-3317 - if we have a bit field that is nullable, then output the field with a valid values as we want to be able to offer 3 choices. The null is handled by the code layer.
*/
IF EXISTS (select 1 from sysobjects where name = 'getPickListSqlForTableColumn')
	DROP FUNCTION getPickListSqlForTableColumn
GO

CREATE FUNCTION [dbo].[getPickListSqlForTableColumn] (@tablename as varchar(50), @columnName as varchar(50))
RETURNS nvarchar(max)
AS
BEGIN
	
    declare @sqlString nvarchar(max)
    declare @foreignKeyTable nvarchar(max) = isNull(dbo.listGetAt(dbo.getForeignKeyForTableColumn(@tablename,@columnName),1,'.'),'')
    
    select @sqlString = 
    case 
		when exists (select 1 from validFieldValues where fieldname = @tablename+'.'+@columnName and validValue='*lookup') then (select dbo.RegExReplace(STUFF((select  ' UNION ' + dbo.RegExReplace(dataValue,'\s',' ') FROM validfieldvalues where fieldname = @tableName+'.'+@columnName and validValue = '*lookup' and len(dataValue) > 0 FOR XML PATH('')),1,6,''),' order by .* union ',' union '))
		when exists (select 1 from validFieldValues where fieldname = @tablename+'.'+@columnName and validValue!='*lookup') then 'select case when len(dataValue) > 0 then dataValue else validValue end as dataValue, validValue as displayValue from validFieldValues where fieldname = '''+@tableName+'.'+@columnName+''''
		when exists (select 1 from lookupList where fieldname in (@columnName,@tablename+@columnName)) then 'select lookupID as dataValue, itemText as displayValue, lookUpTextId as textId from lookupList where isNull(isLive,1) =1 and fieldname in ('''+@columnName+''','''+@tableName+@columnName+''')'
		when @foreignKeyTable != '' and (select dbo.getExtendedProperty('table',@foreignKeyTable,null,null,'isLookup')) is not null then 'select '+ (SELECT column_name FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 AND table_name = @foreignKeyTable) + ' as dataValue, '+dbo.getTableNameExpression(@foreignKeyTable) +' as displayValue from '+@foreignKeyTable +' order by '+dbo.getTableNameExpression(@foreignKeyTable)
		when exists (select 1 from vFlagDef where entityTable=@tablename and (flagGroupTextID=@columnName or cast(flagGroupID as varchar) = @columnName) and datatable='boolean') then 'select case when len(flagTextID) > 0 then flagTextID else cast(flagID as varchar(10)) end as dataValue,flag as displayValue from vFlagDef where active=1 and (flagGroupTextID='''+@columnName+''' or cast(flagGroupID as varchar) ='''+ @columnName +''') and entityTable='''+@tablename+''''
		when exists (select 1 from information_schema.columns where table_name=@tablename and column_name=@columnName and data_type='bit' and IS_NULLABLE = 'YES') then 'select ''1'' as dataValue, ''phr_Yes'' as displayValue union select ''0'' as dataValue, ''phr_No'' as displayValue'
		else null
	end
	
	return @sqlString

END

GO