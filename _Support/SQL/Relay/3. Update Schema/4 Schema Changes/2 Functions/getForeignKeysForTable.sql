IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getForeignKeysForTable]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[getForeignKeysForTable]
GO

CREATE FUNCTION [dbo].[getForeignKeysForTable] (@table as varchar(250))
RETURNS TABLE
AS
return (

select 
	OBJECT_NAME(fc.referenced_object_id) as referencedTable, 
	COL_NAME(f.parent_object_id,fc.parent_column_id) as parentColumn, 
	COL_NAME(f.referenced_object_id,fc.referenced_column_id) as referencedColumn,
	OBJECT_NAME(fc.constraint_object_id) fkName,
	0 as sortIdx
from 
	sys.foreign_keys AS f INNER JOIN sys.foreign_key_columns AS fc 
		ON f.OBJECT_ID = fc.constraint_object_id 
where OBJECT_NAME(f.parent_object_id) = @table
union
select 
	dbo.listGetAt(name,4,'_') as referencedTable, 
	dbo.listGetAt(name,3,'_') as parentColumn, 
	dbo.listGetAt(name,5,'_') as referencedColumn,
	name fkName,
	1 as orderIdx
from 
	sys.check_constraints 
where name like 'fk_'+ @table +'_%'
union
select 
	s.entityName as referencedTable, 
	v.flagTextID as parentColumn,
	s.uniqueKey as referencedColumn,
	'' as fkName,
	2 as orderIdx
from 
	vFlagDef v inner join schemaTable s
		on v.linksToEntityTypeID = s.entityTypeID
where v.entityTable = @table

)
GO