exec createPlaceHolderObject 'function', 'hasColumnBeenUpdated'

GO

/* 

	WAB 2016/03/08 
	Pulled repeated code out of modRegisterTriggerAction
	
	Works out whether a given column (or any of a list of columns) has been updated by a query (for use in a trigger)

*/

alter FUNCTION hasColumnBeenUpdated
	(
		  @tableName sysname				-- name of the table
		, @columnNames varchar(max)			-- a columName or a list of columnNames (function returns true if any column in list has been updated)	
		, @columns_Updated varbinary(50)    -- comes from the columns_updated() function
	)
returns bit AS



BEGIN

declare   @result bit 

select 
	/* we do a count of the columns because there may be more than column involved if we are looking at a computed column */
	@result = count(1)
from
	(	
	/*
	Works out in which byte position within @columns_Updated varbinary we will find the information for the given column
	and then the bit for the column within that byte
	*/

	select 
		bit = ((colid - 1 )% 8 + 1 ) ,
		byte = ((colid  - 1) / 8) + 1  
	from	
		(	
			/*
			This sub query gets the colids of the columns defined in @columnNames, 
			but if that column is a computed column then it gives the colID(s) of the columns which it depends upon
			*/
			select 
				isNull (referenced_minor_id, c.colid) as colid
			from 
				syscolumns c 
					inner join
				dbo.csvtotable (@columnNames) as colNames on colnames.value = c.name
					left join
				sys.sql_expression_dependencies d on d.referencing_minor_id = c.colid AND d.referenced_id = c.id and isComputed = 1
			where 
				c.id = object_id (@tablename)
		) as getColIDs
	) as getBitMasksAndCharPos
where 
	/* this gets the columns which have been updated */
	case when substring(@columns_Updated,byte, 1) & power(2,bit -1) > 0  then 1 else 0 end = 1

return @result	
	
END	

