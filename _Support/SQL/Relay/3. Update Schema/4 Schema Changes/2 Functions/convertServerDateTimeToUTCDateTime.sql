/* WAB/NJH 2015-12-16 - a function to convert datetime in server time to UTC */
IF Exists (select 1 from sysobjects where name = 'convertServerDateTimeToUTCDateTime' and xtype= 'FN')
	DROP FUNCTION convertServerDateTimeToUTCDateTime
GO

CREATE FUNCTION dbo.convertServerDateTimeToUTCDateTime (@datetime datetime) RETURNS dateTime
AS
BEGIN
	return dateadd(n,- DATEPART(TZOFFSET, SYSDATETIMEOFFSET()), @datetime)
END