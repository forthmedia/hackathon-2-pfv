/* NJH 2016/03/22 - function to get synch attempts from settings. Used in vConnectorRecords and improves performance considerably when done this way */
createPlaceHolderObject @objectName = 'getConnectorMaxSynchAttempts', @objectType = 'INLINEDTABLEFUNCTION'
GO

ALTER FUNCTION getConnectorMaxSynchAttempts() RETURNS TABLE AS 
	RETURN (select value from settings where name = 'connector.maxSynchAttempts')