createPlaceHolderObject 'function', 'fn_char2PercentEncoded'
GO
alter function fn_char2PercentEncoded (@character nvarchar(1)) returns varchar(16)
AS

/* WAB 2016-10-13	PROD2016-2544 
		Out existing code to URLencode a string will not deal with nvarchar characters
		Remarkably I could not find any SQL code which even attempted to URLEncode a unicode string 
		Eventually I came across the algorithm for converting unicode Code points to UTF8 Bytes
		This code implements the algorithm is SQL, it is probably slightly long winded but it seems to work
		
		From http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=IWS-AppendixA
		
		If U <= U+007F, then
			C1 = U
		Else if U+0080 <= U <= U+07FF, then
			C1 = U 64 + 192
			C2 = U mod 64 + 128
		Else if U+0800 <= U <= U+D7FF, or if U+E000 <= U <= U+FFFF, then
			C1 = U 4,096 + 224
			C2 = (U mod 4,096) 64 + 128
			C3 = U mod 64 + 128
		Else
			C1 = U 262,144 + 240
			C2 = (U mod 262,144) 4,096 + 128
			C3 = (U mod 4,096) 64 + 128
			C4 = U mod 64 + 128
		End if

		*/


begin

	declare @U int = unicode (@character)

	declare @c1 bigint, @c2 bigint, @c3 bigint, @c4 bigint, @result varchar(16)

	If @U <= 0x7F 
	BEGIN
		set @C1 = @U	
		set @result = replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c1)),'0x','%')
	END	
	Else if @U <= 0x07FF 
	BEGIN
		SET @C1 = (@U / 64) + 192
		set @C2 =  (@U % 64) + 128
		set @result = replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c1)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c2)),'0x','%')
	END
	Else if (0x0800 <= @U  and @U <= 0xD7FF)  or (0xE000 <= @U  and @u <= 0xFFFF)
	BEGIN
		SET @C1 = @U/4096 + 224
		SET @C2 = (@U % 4096) / 64 + 128
		SET @C3 = @U % 64 + 128
		set @result = replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c1)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c2)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c3)),'0x','%')
	END
	ELSE 
	BEGIN
		SET @C1 = @U /262144 + 240
		SET @C2 = (@U % 262144) / 4096 + 128
		SET @C3 = (@U % 4096) / 64 + 128
		SET @C4 = @U % 64 + 128
		set @result = replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c1)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c2)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c3)),'0x','%') + replace (sys.fn_varbintohexstr(CONVERT(BINARY(1), @c4)),'0x','%') 
	END
		

return upper(@result)

end

GO

