ALTER function [dbo].[getParameterValue](@nameValuePairString varchar(1000),@parameterName varchar(100),@delimiter char(1) =',') RETURNS VARCHAR(1000)

AS
/*
NOTE THAT THIS FUNCTION IS ALSO IN THE JASPERSERVER DB

WAB 2009-02-25	A function for pulling parameter values out of name value pair strings
WAB 2012-04-11	Added support for quoted name value pairs - (just doubleQuotes)
WAB 2012-06-25	Added support for a different delimiter (defaults to , but might want to use space)
				Also noted that there was almost support for escaped double quotes - but needed to finish by replacing "" with "
*/

BEGIN


	Declare @ParameterNamePosition INT, 
			@ParameterValuePosition INT, 
			@FirstCharacter varchar(1), 
			@EndDelimiterCharacter varchar(2), 
			@EndDelimiterPosition INT
	Declare @Result VARCHAR(1000)
	
	set @EndDelimiterCharacter = @delimiter
	set @nameValuePairString = rtrim(@nameValuePairString)  + @delimiter   -- a bit nasty but much easier to shove a delimiter on the end
	-- find position of @parameterName in the string
	SELECT @ParameterNamePosition = charindex(upper(@parameterName) + '=',upper(@nameValuePairString))
	IF @ParameterNamePosition <> 0 
		-- @parameterName Found
		BEGIN
			SELECT @ParameterValuePosition  = @ParameterNamePosition + LEN(@parameterName) + 1
			-- get first character, if it is a double quote then change what we search for at the end to " + delimiter
			SELECT @FirstCharacter = SUBSTRING(@nameValuePairString,@ParameterValuePosition,1)
			IF @FirstCharacter = '"'
				select @EndDelimiterCharacter = @FirstCharacter + @EndDelimiterCharacter, @ParameterValuePosition = @ParameterValuePosition + 1
			SELECT @EndDelimiterPosition = CHARINDEX(@EndDelimiterCharacter,@nameValuePairString,@ParameterValuePosition)
	  		SELECT @Result = SUBSTRING(@nameValuePairString,@ParameterValuePosition,@EndDelimiterPosition-@ParameterValuePosition)
			-- if quote qualified, then unescape any escaped double quotes
	  		IF @FirstCharacter = '"'
	  			select @Result = REPLACE (@Result,'""','"')
		END
	ELSE
		BEGIN
			SELECT @Result = NULL
		END


	RETURN @Result
	
END



