IF Exists (select * from sysobjects where name = 'columnListToBitmask' and xtype= 'FN')
	DROP FUNCTION columnListToBitmask

GO
CREATE  FUNCTION [dbo].[columnListToBitmask] (@tableName sysname,@columnList varchar(max))  
/*
WAB 2014-03-27
Function for creating bitmasks as used by @@columns_updated
Pass in the name of the table or view and a comma separated list of columns

*/

RETURNS varbinary(12) AS  
BEGIN 

	/* convert the list of columns to a temporary table variable */
	declare @columnTable table (item sysname)

	insert into @columnTable
	select  item from
	(SELECT cast('<l>'+replace(replace(@columnList,';',','),',','</l><l>')+'</l>' as xml) as ListAsXML) as _
        CROSS APPLY 
   (SELECT l.value('.','varchar(100)') as item FROM ListAsXML.nodes('l') as f(l)) as items


	/* create a temporary table variable with info on the columns updated
		two versions to deal with temporary tables
	*/		
	declare @SchemaTable table (bit int, bitvalue int, byte int, column_name sysname)

	IF LEFT(@tablename,1) <> '#'
	BEGIN
		insert into @SchemaTable
		select 
			bit = ordinal_position % 8 , 
			bitvalue = power(2,((ordinal_position -1) % 8 )),      
			byte = (ordinal_position - 1) / 8,   
			column_name

		from 
			information_schema.columns 
			inner join
			@columnTable ON column_name = item
		where 
		table_name = @tablename	
	END
	ELSE
	BEGIN
		
		insert into @SchemaTable
		select 
			bit = ordinal_position % 8 , 
			bitvalue = power(2,((ordinal_position -1) % 8 )),      
			byte = (ordinal_position - 1) / 8,   
			column_name

		from 
			tempdb.information_schema.columns 
			inner join
			@columnTable ON column_name = item
		where 
		table_name = (select name from tempdb.sys.objects where object_id = object_id('tempdb..' + @tablename))
	
	END

	/*  now generate the bitmask
		uses a funky bit of concatenation in a variable
		The select top 10 is just a hack way of getting a list of numbers
	*/

	declare @bitmask varbinary(100)	= 0x	

	SELECT  
		@bitmask = @bitmask + 	convert(varbinary(1),bitvalue)
	FROM
		(
		select 
			numbers.n, isnull(sum(bitvalue),0) as bitvalue		
		from
			@SchemaTable as X
				right join
			(SELECT TOP (20) n = ROW_NUMBER() OVER (ORDER BY [object_id]) -1 FROM sys.all_objects ) as numbers
				ON numbers.n = x.byte
			group by numbers.n

			) as _
	order by n asc
	
	RETURN @bitmask
END		

