/* NJH 2015/01/23 - added objectColumn as duplicate value mappings meant that the correct value was not being returned 
	NJH 2015/03/05 - use bent pipe as delimiter as commas could be present in the value
	NJH 2015/03/30 - add new parameter @inputDelimiter, to tell us what the delimiter is for the incoming string. @delimiter was changed to @outputDelimiter
*/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[replaceConnectorMappingValue]') AND type = N'FN')
DROP FUNCTION [dbo].[replaceConnectorMappingValue]
GO

CREATE FUNCTION [dbo].[replaceConnectorMappingValue]
(
	@entityName VARCHAR(50),
	@objectColumn varchar(100),
	@value NVARCHAR(max),
	@direction CHAR(1) = 'I',
	@outputDelimiter CHAR(1) = ',',
	@inputDelimiter CHAR(1) = ';',
	@connectorType varchar(50) = 'Salesforce'
)
RETURNS varchar(500)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result varchar(500)

	/* speed up the performance. Don't run query below if nothing passed in */
	if (isNull(@value,'') = '')
		return ''
		
	select @result = isNull(@result + @outputDelimiter,'') + isNull(case when @direction = 'I' then cm.value_relayware else cm.value_remote end,x.val)
	from 
		(SELECT cast('<origVal>'+replace(dbo.XMLFormat(replace(@value,@inputDelimiter,char(172))),char(172),'</origVal><origVal>')+'</origVal>' as xml) as originalValueListAsXml) as _
			CROSS APPLY 
		(SELECT origVal.value('./text()[1]','varchar(100)') as val FROM originalValueListAsXml.nodes('origVal') as v(origVal)) as x
			LEFT JOIN  
		(connectorColumnValueMapping cm	INNER JOIN vConnectorMapping c on c.connectorType=@connectorType and c.ID = cm.connectorMappingID and c.object_relayware = @entityName
			and @objectColumn = case when @direction = 'I' then c.column_remote else c.column_relayware end) 
			ON x.val = case when @direction = 'I' then cm.value_remote else cm.value_relayware end and cm.mappingType = 'V'
		
	RETURN isNull(@result,@value)

END
GO