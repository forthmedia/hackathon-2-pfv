createPlaceHolderObject @objectName = 'evaluateRecordRights', @objectType = 'function'

GO

/*
WAB 2015-12-01 PROD2015-290 Visibility Project
This function is used exclusively within the getRecordRightsTableByUserGroup function
It encapsulates some logic which is repeated


*/


ALTER FUNCTION [dbo].evaluateRecordRights (@numberOfUserGroupsInGroup int, @numberOfUserGroupsBelongedTo  int, @ANDGroup bit, @notGroup  bit, @ifNoUserGroupsInGroupReturn bit = null)  

returns bit AS

BEGIN

	declare @result bit 

	IF isNull(@numberOfUserGroupsInGroup,0) = 0 
	BEGIN
		set @result = @ifNoUserGroupsInGroupReturn
	END
	ELSE
	BEGIN
		set @result = case 
						when isNull(@ANDGroup,0) = 1 and isnull(@numberOfUserGroupsInGroup,0) = isnull(@numberOfUserGroupsBelongedTo,0) then 1
						when isNull(@ANDGroup,0) = 0 and isNull(@numberOfUserGroupsBelongedTo,0) > 0 then 1
						else 0 
					  end
	
		IF isnull(@notGroup,0) = 1
		BEGIN
			set @result = ~ @result
		END	
	END
	
	return @result
end

