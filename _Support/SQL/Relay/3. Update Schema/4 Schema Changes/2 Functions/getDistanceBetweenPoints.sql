createPlaceHolderObject @objectName = 'getDistanceBetweenPoints', @objectType = 'function'

GO

ALTER FUNCTION [dbo].[getDistanceBetweenPoints] (@lat1 float(24),@long1  float(24), @Lat2 float(24), @long2  float(24))  

returns float AS

begin


/*

Function GetDistanceBetweenPoints

Author WAB  

Date  2014-09-01

Purpose   To find the distance between two lat/longs
		  Returns distance in KM	
		  First used in locator, CASE 442259	
		  Calculation taken from CF code in Locator.cfc and then replaced with call to this function	

1/5/2016 ESZ 446804 Partner Locator - Error Message	- reduced precision of float to prevent floating point error in certain circumstances
*/


declare @result float(24), @x float

select @result = 

(
	(
		(
			acos(
				sin((@lat1*pi()/180)) 
				* 
				sin((@lat2*pi()/180)) 
				+ 
				cos((@lat1*pi()/180)) 
				* 
				cos((@lat2*pi()/180)) 
				* 
				cos(((@long1 - @long2)*pi()/180))
				)
		)*180/pi()
	)*60*1.8424
)

return @result
end

