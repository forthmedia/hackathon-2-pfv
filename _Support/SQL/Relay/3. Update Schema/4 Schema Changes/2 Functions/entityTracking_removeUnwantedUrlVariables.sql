createPlaceHolderObject @objectName = 'entityTracking_removeUnwantedUrlVariables', @objectType = 'FUNCTION'
GO

ALTER FUNCTION dbo.entityTracking_removeUnwantedUrlVariables (@urlString Nvarchar(max))
RETURNS nVarchar(max)
AS
/*
entityTracking_removeUnwantedUrlVariables ()
NJH 2016/01/19
Removes unwanted url variables from the entity tracking url

1. remove index.cfm
2. replace /? with ?
3. then replace all unwanted url variables
4. Remove any trailing ? or &
5. WAB 2016-01-28 Special code for seid links
*/
BEGIN
	declare @unwantedURLVarsRexExp varchar(max)
	set @unwantedURLVarsRexExp = 'rwsessionToken|formState|_cf_noDebug|spl|courseSortOrder|moduleSortOrder|jsessionid|flush|returnURL|urlrequested|debug|p|pid|es|pp|requestDump|oo|hl|pNumber'
	select @urlString = ltrim(rtrim(dbo.RegExReplace(@urlString,'[\/]index.cfm','')))
	select @urlString = replace(@urlString,'/?','?')
	select @urlString = dbo.RegExReplace(@urlString,'(?<=(\?|&))('+@unwantedURLVarsRexExp+')=.*?(&|$)','')
	select @urlString = dbo.RegExReplace(@urlString,'(\?|&)*$','')
	/*  WAB 2016-01-28 special code for seid links of form seid=pageID-tr-xxxxxxxxx  . 
		Want to keep the page name but drop the personal bit 
		Replace with a plain eid link
		Also has to deal with - being encoded as %2d
	*/
	select @urlString = dbo.RegExReplace(@urlString,'(?<=(\?|&))seid=(.*?)(-|%2d)(.*?)(&|$)','eid=$2')

	RETURN @urlString
END