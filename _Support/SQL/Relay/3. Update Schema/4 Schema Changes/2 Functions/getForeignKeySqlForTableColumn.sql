/* 
	NJH 2016/11/08 a function to get the sql used for foreign key values for a table.column 
*/
IF EXISTS (select 1 from sysobjects where name = 'getForeignKeySqlForTableColumn')
	DROP FUNCTION getForeignKeySqlForTableColumn
GO

CREATE FUNCTION [dbo].[getForeignKeySqlForTableColumn] (@tablename as varchar(50), @columnName as varchar(50))
RETURNS nvarchar(max)
AS
BEGIN
    declare @sqlString nvarchar(max)
    declare @foreignKey nvarchar(max) = dbo.getForeignKeyForTableColumn(@tablename,@columnName);
    declare @foreignKeyTable nvarchar(max) = isNull(dbo.listGetAt(@foreignKey,1,'.'),'');
    declare @foreignKeyColumn nvarchar(max) = isNull(dbo.listGetAt(@foreignKey,2,'.'),'');
    declare @filter nvarchar(max) = '1=1';
    declare @foreignKeyTableNameExpression nvarchar(max) = dbo.getTableNameExpression(@foreignKeyTable)
    
    select @filter = isNull(dbo.getParameterValue(cfFormParameters,'filter',','),'1=1') from modEntityDef where tablename= @tablename and fieldname=@columnName
    if (len(@filter) = 0)
		select @filter = isNull(dbo.getParameterValue(f.formattingParameters,'filter',','),'1=1') from vFlagDef v inner join flag f on v.flagID = f.flagId where entityTable= @tablename and v.flagTextID=@columnName 
	if (len(@filter) = 0)
		select @filter = '1=1'
		
    select @sqlString = 
    case
		when @foreignKeyTable != '' and (select dbo.getExtendedProperty('table',@foreignKeyTable,null,null,'isLookup')) is null then 'select '+ @foreignKeyColumn + ' as dataValue, '+ @foreignKeyTableNameExpression +' as displayValue from '+@foreignKeyTable +' where ' +@filter + ' order by '+@foreignKeyTableNameExpression
		else null
	end
	
	return @sqlString

END

GO