/* a function to get the the name column for a given table. It looks at schema table or at the nameField extended property.
	The name expression can be held in a number of ways at the moment to allow custom tables to have name properties. It also
	means that not every table in the db needs to be in schemaTable. But it may be that that is the way we decide to go.
 */
IF EXISTS (select 1 from sysobjects where name = 'getTableNameExpression')
	DROP FUNCTION getTableNameExpression
GO

CREATE FUNCTION [dbo].[getTableNameExpression] (@tablename as varchar(250))
RETURNS varchar(250)
AS
BEGIN

	declare @result varchar(250)
	
    select @result = isNull(nameExpression,dbo.getExtendedProperty('table',t.table_name,null,null,'nameField'))
		from information_schema.tables t 
			left join schemaTable s on t.table_name=s.entityName 
		where t.table_name=@tablename
			
	return @result

END

GO