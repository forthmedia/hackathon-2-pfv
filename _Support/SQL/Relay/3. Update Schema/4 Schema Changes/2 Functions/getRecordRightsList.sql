createPlaceHolderObject 'function', 'getRecordRightsList' , '	@entityID int,	@EntityType sysname, 	@level int =1,	@returnUserGroupNames int = 1'

GO

/*
9/11/04 	WAB added a permission parameter and had to call it V2 for backeards compatability
4/7/5   	WAB corrected a bug and added a parameter to allow return of names or ids
2015-12-01	WAB PROD2015-290 Visibility Project.
			Renamed function and altered to return ANDs, ORs, NOTs.  Also adds Tabs and line breaks - which may be a bit naughty
*/

ALTER       FUNCTION [dbo].[getRecordRightsList] 
	(@entityID int,
	@EntityType sysname,
	@Level int = 1,
	@returnUserGroupNames bit = 1)
RETURNS nvarchar(4000) AS  
BEGIN 


DECLARE    @returnString nvarchar(4000)
		 , @operatorString nvarchar(20)
		 , @notString varchar(20)  
		 , @andOrString varchar(20) -- note must be varchar (or you need to divide datalength by 2 below)
		 , @singleItem bit
		 , @entityTypeID int
	
	Select @entityTypeID = case when isnumeric(@entityType) =1 then @entityType else (select entityTypeID from schemaTable where entityname = @entityType) end

	select @notString = case when groupNot & power(2, @level-1) <> 0 then 'NOT ' + char(10) else '' end,
		   @andOrString = 	case when groupAnd & power(2, @level-1) <> 0 then ' AND ' else char(10) + char(9) + 'OR ' end
	from 
		recordRightsOperator
	where
			entityTypeID = @EntityTypeID
		AND recordid = @entityID

	SELECT @returnString = SUBSTRING(
	(
		SELECT 
			@andOrString + case when @returnUserGroupNames = 1 then UserGroup.Name else convert(varchar,usergroup.usergroupid) end 
		FROM 
			RecordRights 
				INNER JOIN 
			UserGroup ON RecordRights.UserGroupID = UserGroup.UserGroupID 
		WHERE 
				RecordRights.entityTypeID = @EntityTypeID
			AND recordRights.recordid = @entityID
			AND permission & power(2, @level-1) <> 0
		ORDER BY UserGroup.Name
	FOR XML PATH('')),datalength(@andOrString) + 1,200000) 

	select @singleItem = case when @returnString not like '%' + @andOrString + '%' then 1 else 0 end

	IF @returnString <> '' 
	BEGIN
		SET @returnString = @notString + case when @singleItem = 1 then @returnString else '( ' +char(10) + char(9)  + @returnString + char(10) + ' )'  end 
	END	


	IF @level = 1
	BEGIN
		declare 
			@level11 nvarchar(max), @not bit = 0
		
		select 	
			@level11 =  dbo.getRecordRightsList (@entityID,@entityTypeID, 11,@returnUserGroupNames)
		
		select
			@not = case when left (@level11,4) = 'NOT ' then 1 else 0 end  
			
	
		select 	@returnString =
			@returnString + case when @returnString <> '' and @level11 <> '' then char(10) + case when @not = 0 then ' AND ' + char(10) else ' BUT ' end  + @level11  else '' end
	
	END



Return @returnString 

END











GO


