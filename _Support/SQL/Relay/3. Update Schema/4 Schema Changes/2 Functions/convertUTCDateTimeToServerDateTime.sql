/* WAB/NJH 2015-12-16 - a function to return a UTC datetime to server time */
IF Exists (select 1 from sysobjects where name = 'convertUTCDateTimeToServerDateTime' and xtype= 'FN')
	DROP FUNCTION dbo.convertUTCDateTimeToServerDateTime
GO

CREATE FUNCTION dbo.convertUTCDateTimeToServerDateTime (@datetime datetime) RETURNS dateTime
AS
BEGIN
	return CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,@datetime),DATENAME(TzOffset, SYSDATETIMEOFFSET())))
END