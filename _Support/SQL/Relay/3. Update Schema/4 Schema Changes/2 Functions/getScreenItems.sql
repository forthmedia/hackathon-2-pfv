createPlaceHolderObject @objectName = 'getScreenItems', @objectType = 'InlinedTableFunction'

GO


alter function getScreenItems (@screenid int, @countryid int) returns TABLE

AS


/*
WAB 2015-02-25
	Build up the items in a screen by recursing through the screendefinition table (to pick up screens included within screens)

	country specific items override region specific ones at the same sort position
	note that if a country is in two regions and lines are specified for each of these regions, then each will appear on screen

	Replaces much of the code in screens.cfc getScreenAndDefinition()
	and removes the requirement for the screenIndex table and all associated triggers and procedures

	Note that result must be ordered by sortIndex (since you can't do an order by within this function)

WAB 2016-09-11 For ScreensV2 - do not want evalString automatically populated when null, so had to create another aliased column  evalString2
	
*/


RETURN
(


					WITH screenDef (
						itemid,
						CountryID ,
						FieldSource,
						FieldTextID,
						FieldLabel,
						TranslateLabel ,
						AllColumns,
						IsCollapsable,
						IsVisible,
						BreakLn ,
						Maxlength,
						Size   ,
						specialformatting,
						UseValidValues ,
						lookup ,
						TRClass ,
						TDClass ,
						jsVerify,
						eventList,
						EvalString ,
						EvalString2 ,
						required,
						method, 
						readOnly,
						thisLineCondition,
						level, 
						sortIndex
					) AS
					(
				    SELECT 
						0,
						0,
						convert(varchar(40),'screen'),
						convert(varchar(1100),@screenid),
						convert(varchar(250),''),
						CONVERT(bit,0),
						CONVERT(bit,0),
						0,
						1,
						CONVERT(bit,0),
						CONVERT(smallInt,0),
						CONVERT(smallInt,0),
						convert(varchar(2000),''),  -- specialFormatting
						CONVERT(bit,0),
						CONVERT(bit,0),
						convert(varchar(20),''),
						convert(varchar(20),''),
						convert(varchar(255),''),
						convert(varchar(255),''),
						convert(varchar(4000),''),  -- evalString
						convert(varchar(4000),''),  -- evalString2
						0,
						convert(varchar(max),''), -- method
						convert(varchar(max),''), -- readonly
						convert(varchar(1000),''),
						 0,	
						convert(varchar(max),'')
						from 
						screens where ScreenID = @screenid

				    UNION ALL

				    SELECT 			
						sd.itemid,
						sd.CountryID ,
						sd.FieldSource,
						sd.FieldTextID,
						sd.FieldLabel,
						sd.TranslateLabel ,
						sd.AllColumns,
						0 as IsCollapsable,
						1 as IsVisible,
						sd.BreakLn ,
						sd.Maxlength,
						sd.Size   ,
						sd.specialformatting,
						sd.UseValidValues ,
						sd.lookup ,
						sd.TRClass ,
						sd.TDClass ,
						sd.jsVerify,
						sd.eventList,
						convert(varchar(4000),CASE when isnull(sd.evalstring,'') = '' THEN sd.fieldSource + '.' + sd.fieldtextid ELSE sd.evalstring END) as EvalString ,
						convert(varchar(4000),sd.evalString) as EvalString2 ,						
						/* if an included screen is marked as required then all items within it are required */
						CASE WHEN d.required > sd.required THEN d.required ELSE sd.required END as required,
						/* if an included screen has method=view then all items within it are view */
							convert(varchar(max),case when s.viewedit = 'view' OR d.method = 'view' then 'view' else sd.method end),  
						 convert(varchar(max),
							case when d.readOnly <> '' then d.readOnly + ' OR ' else '' end +
							case when sd.method in  ('edit','hidden') then 'false' when sd.method in  ('view') then 'true' else sd.method  end 
						),						
						/* if an included screen has a condition, then that condition applies to all child items */
						convert(varchar(1000),CASE WHEN isNull(d.thisLineCondition,'') = '' THEN sd.condition
							 WHEN isNull(sd.condition,'') = '' THEN d.thisLineCondition
							ELSE '(' + d.thisLineCondition +') AND (' + sd.condition + ')' END) as thisLineCondition,
					     level+1, 
						/* Items must be sorted - uses an alpha sort with 0's padding */
					    d.sortIndex + right('000' + CONVERT(varchar (max), sd.sortorder),4)
				    FROM 
						screenDef AS d
							inner join
						screens as s ON d.fieldSource = 'screen' and (s.screenTextID = d.fieldTextID OR convert(varchar, s.screenid) = d.fieldTextID)
							inner join
						screendefinitionscoped AS sd ON s.screenid = sd.screenid and country = @countryid 
					WHERE 
						sd.active = 1	

					)

					SELECT 	
						itemid,
						CountryID ,
						FieldSource,
						FieldTextID,
						FieldLabel,
						TranslateLabel ,
						AllColumns,
						IsCollapsable,
						IsVisible,
						BreakLn ,
						Maxlength,
						Size   ,
						specialformatting,
						UseValidValues ,
						lookup ,
						TRClass ,
						TDClass ,
						jsVerify,
						eventList,
						EvalString ,
						EvalString2 ,
						required,
						method, 
						readonly ,
						thisLineCondition,
						 level, 
						 sortIndex

					FROM screendef 
					where fieldSource not in ('screen','blank')


)