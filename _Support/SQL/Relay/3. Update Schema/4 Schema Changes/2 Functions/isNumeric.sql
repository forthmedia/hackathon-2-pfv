createplaceholderobject 'function', 'isNumeric'

GO

/*
WAB 2016-05-26
For BF-947
For use in entityUpsert
A custom isNumericFunction which checks that a string is both numeric and can be cast to a numeric type
In particular it checks for commas which the built in isNumeric function ignores but which trip up casts to int/float/decimal
Note that casting to Money is less problematic because it seems ignore commas entirely
Also note that this can't be done with a simple regExp searching for anything not 0-9-. because the built in isNumeric does deal with things like exponent format - so eg 1.23e3 is valid numeric
*/

ALTER FUNCTION dbo.ISNUMERIC (
	@string nvarchar(MAX)
	) RETURNS bit
AS
BEGIN

	declare @result BIT = 1
	
	SELECT @result = 
		CASE WHEN ISNUMERIC(@string) = 0 THEN 0
			WHEN @string LIKE '%,%' THEN 0
			ELSE 1 END

RETURN @result
END
