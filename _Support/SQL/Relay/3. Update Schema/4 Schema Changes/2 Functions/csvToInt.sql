createPlaceHolderObject @objectName = 'CsvToInt', @objectType = 'TableFunction'  

GO
	/*
	WAB 2017-03-10 Altered to accept varchar(max)
	*/

		alter Function [dbo].[CsvToInt] ( @Array varchar(max)) 
		returns @IntTable table 
			(IntValue int)
		AS
		begin

			declare @separator char(1)
			set @separator = ','

			declare @separator_position int 
			declare @array_value varchar(1000) 
			
			set @array = @array + ','
			
			while patindex('%,%' , @array) <> 0 
			begin
			
			  select @separator_position =  patindex('%,%' , @array)
			  select @array_value = left(@array, @separator_position - 1)
			
				Insert @IntTable
				Values (Cast(@array_value as int))

			  select @array = stuff(@array, 1, @separator_position, '')
			end

			return
		end
