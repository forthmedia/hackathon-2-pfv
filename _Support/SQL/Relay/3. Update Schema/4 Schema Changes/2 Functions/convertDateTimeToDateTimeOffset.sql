/* WAB/NJH 2015-12-16 - a function to return the offset between server time and UTC */
IF Exists (select 1 from sysobjects where name = 'convertDateTimeToDateTimeOffset' and xtype= 'FN')
	DROP FUNCTION dbo.convertDateTimeToDateTimeOffset
GO

CREATE FUNCTION dbo.convertDateTimeToDateTimeOffset (@datetime datetime) RETURNS dateTimeOffset
AS
BEGIN
	return todateTimeOffset(@datetime,DATEPART(TZOFFSET, SYSDATETIMEOFFSET()))	
END