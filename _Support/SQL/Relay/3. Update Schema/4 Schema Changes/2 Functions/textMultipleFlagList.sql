IF Exists (select * from sysobjects where name = 'TextMultipleFlagList' and xtype= 'FN')
	DROP FUNCTION TextMultipleFlagList

GO


CREATE FUNCTION [dbo].[TextMultipleFlagList] (@flagID int, @entityid int)  

RETURNS nvarchar(Max) AS  
BEGIN 

	DECLARE @result nVARCHAR(max)
	
	SELECT @result = case when @result is null then '' else @result + ',' end + data from textMultipleflagdata fd where fd.flagid = @flagid and fd.entityid = @entityID


	RETURN @result

END

