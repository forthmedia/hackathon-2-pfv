createPlaceHolderObject 'TableFunction', 'getNonUniqueEmails'

GO

ALTER function [dbo].[getNonUniqueEmails](@personEmailTable personEmailtableType readonly) RETURNS @nonUniqueEmails table
(
	personID int,
	email varchar(250),
	accountTypeID int
)
AS
/*
	get any emails that may not be unique. Currently, this is used in conjunction with the isEmailUnique function.
	This function was written so that we could actually see which emails were the problem emails.
	End Customers are not included in email validation.
	
	NJH 2015/07/20	also now check emails within the incoming table as well as the db. If they are not unique within non-end customer account types, then return them as well
		(ie. this will flag up if you're trying to insert/update two records with the same email address at the same time even if that email address does not exist in RW.)
	NJH 2015/07/24 - re-wrote the query to do a union rather than a left join to speed up performance..
	WAB 2016-02-08	BF-409 Exclude blank emails from the validation (wasn't doing this on first part of union)
	WAB 2016-07-06 PROD2016-1367 Connector Performance. Put result of getSetting() into a variable before using, 
			Considered changing from listFind to use a regular expression
			declare @excludedEmailsRegExp nvarchar(max) = '^(' + replace (@excludedEmails,',','|') + ')$' 
			and dbo.RegExIsMatch(@excludedEmailsRegExp,p.email,1) = 0
	WAB 2017-03-03  RT-128 Put excluded emails into a table, so don't have to call listfind lots of times - which is somewhat inefficient
*/

BEGIN

	declare @excludedEmails nvarchar(max) = dbo.getSettingValue('plo.excludedEmailsFromUniqueValidation')

	declare @excludedEmailsTable as table (excludedEmail nvarchar(max))
	insert into @excludedEmailsTable (excludedEmail)
	select value from dbo.csvtotable (@excludedEmails) 

	insert @nonUniqueEmails
	select distinct  pt.personID,pt.email,pt.accountTypeID 
	from @personEmailTable pt
		inner join person p on len(isNull(p.email,'')) > 0 and ltrim(rtrim(pt.email)) = ltrim(rtrim(p.email)) and pt.personID != p.personID
		left join @excludedEmailsTable excludedEmails on excludedEmails.excludedEmail = pt.email		
		inner join location pl on pl.locationID = p.locationID and pt.accountTypeID = isNull(pl.accountTypeID,1)
		inner join organisationType ot on ot.organisationTypeID = pt.accountTypeID and ot.typeTextID != 'EndCustomer'
		left outer join (booleanflagdata bfd inner join flag f ON f.flagID = bfd.flagID and f.flagTextID='DeletePerson') on bfd.entityID = p.personId
	where
		bfd.flagID is null
		and excludedEmails.excludedEmail is null
		and isNull(pt.email,'') != ''
			
			
			
		 --or (dupIncomingEmail.email is not null)
	union
		select  pt.personID,pt.email,pt.accountTypeID
		from @personEmailTable pt inner join
			(select email, isNull(ptd.accountTypeID,1) as accountTypeId 
				from @personEmailTable ptd
					inner join organisationType ot on ot.organisationTypeID = ptd.accountTypeID 
					left outer join (booleanflagdata bfd inner join flag f ON f.flagID = bfd.flagID and f.flagTextID='DeletePerson') on bfd.entityID = ptd.personId
					left join @excludedEmailsTable excludedEmails on excludedEmails.excludedEmail = ptd.email
				where 1=1
					and bfd.flagID is null
					and excludedEmails.excludedEmail is null
					and ot.typeTextID != 'EndCustomer' 
					and isNull(ptd.email,'') != ''
				group by ptd.email,isNull(ptd.accountTypeID,1)
				having count(ptd.email) > 1) as dupIncomingEmail on dupIncomingEmail.email = pt.email and dupIncomingEmail.accountTypeID = pt.accountTypeID
	return
	
END		

GO
