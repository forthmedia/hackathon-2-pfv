
IF  EXISTS (SELECT 1 FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[isEmailUnique]') AND type = N'FN')
	DROP FUNCTION [dbo].[isEmailUnique]
GO

CREATE function [dbo].[isEmailUnique](@personEmailTable personEmailtableType readonly) RETURNS bit

AS
/*
	Function to determine unique email validation
*/

BEGIN
	Declare @result bit

	select @result = case when count(1) = 0 then 1 else 0 end from dbo.getNonUniqueEmails(@personEmailTable)

	return @result
	
END		

GO