IF Exists (select * from sysobjects where name = 'IntegerMultipleFlagList' and xtype= 'FN')
	DROP FUNCTION IntegerMultipleFlagList

GO

CREATE FUNCTION [dbo].[IntegerMultipleFlagList] (@flagID int, @entityid int,@returnValue varchar(20)='data')

RETURNS nvarchar(Max) 

AS

BEGIN
	DECLARE @result nVARCHAR(max)
	
	IF @returnValue = 'name'
	BEGIN
		SELECT @result = case when @result is null then '' else @result + ',' end + isnull(e.name,convert(varchar,fd.data)) from integermultipleflagdata fd inner join flag f ON f.flagid = fd.flagid left join vEntityName e on e.entityID = data and e.entityTypeID = f.linksToEntityTypeID
		where fd.flagid = @flagid and fd.entityid = @entityID
		
	END
	ELSE
	BEGIN
		SELECT @result = case when @result is null then '' else @result + ',' end + convert(varchar,data) from integermultipleflagdata fd where fd.flagid = @flagid and fd.entityid = @entityID
	END
	
	RETURN @result
	
END