createPlaceHolderObject 'function', 'getCountryScopeList' , '	@entityID int,	@EntityType sysname, 	@level int =1,	@returnCountryNames int = 1'

GO

ALTER function [dbo].[getCountryScopeList] (
	@entityID int,
	@EntityType sysname, 
	@level int =1,
	@returnCountryNames int = 1      -- 0 gives countryids, 1 gives names, 2 gives isocode (+ names for regions)
)  

/*  


Changes
WAB 25/10/2006  made to work! (was hard coded for elements despite taking @EntityType parameter
WAB 25/10/2006 to be able to return list of isocodes or names
NJH 2013/05/31 - return null if nothing returned instead of an empty string, so that we can do an isNull
WAB 2015-12-01	PROD2015-290 Visibility Project.
				Changed name of function and got rid of cursor
				Add support for level11 being an exclude
				Return string with ORs and NOTs in it and some LF and TAB for improved display 
WAB 2016-03-22	BF-594 Should not use ORs when just returning countryids.  These lists are sometimes parsed by other code (such as in fileManager.cfc) which expects nicely formatted lists of countries.
						Note that such code will still fail if the NOT functionality is implemented on files.
*/

RETURNS nvarchar(4000) AS  
BEGIN 


	DECLARE   @returnString nvarchar(max)  
			, @or varchar(5) = char(9)
			, @entityTypeID int
			, @count int = 0
	
	Select @entityTypeID = case when isnumeric(@entityType) =1 then @entityType else (select entityTypeID from schemaTable where entityname = @entityType) end
	
	select 
		@returnString = isnull(@returnString,'') + @or + case  
										when @returnCountryNames = 1 then countryDescription 
										when @returnCountryNames = 2 then isNull(isocode,countryDescription)  
										else convert (varchar,c.countryid) 
									end  
		, @or = case when @returnCountryNames = 0 then ',' else char(10) + char(9) + 'OR ' end  /* when returning countryIDs just separate with commas otherwise, when returning text, use OR */
		, @count += 1
	from countryscope csc 
			inner join
		country c on c.countryid = csc.countryid 
	where 
			csc.entityTypeID = @entityTypeID
		and csc.entityid = @entityID
		AND permission & power(2, @level-1) <> 0
	order by 
		countryDescription 

	IF @count > 2 and @returnCountryNames <> 0
	BEGIN
		/* add brackets if more than 1 item and not returning countryIDs */
		SET @returnString = '(' + char(10) + @returnString + char(10) + ')'
	END
	

	/* for level1 we also need to check for excludes defined in level11 */
	IF (@level = 1)
	BEGIN
		declare @level11 nvarchar(max) 
		select @level11 = [dbo].[getCountryScopeList] (@entityID, @entityTypeID , 11, @returnCountryNames)
		IF @level11 is not null
		BEGIN
			select @returnString = isnull(@returnString,'') + ' BUT NOT ' + @level11
		END
	END
	
	
	Return @returnString

END

