ALTER  TRIGGER [dbo].[quizTaken_IUTrig] ON [dbo].[QuizTaken] FOR INSERT,Update AS
/* =============================================
 Author:		NJH
 Create date: 2008/09/16
 Description:	Updates the quizPassed and quizPassed date in the user module progress if the quiz was passed

 Modifications:
 2011-10-14		NYB  LHID7420 - userModuleFulfilled not updating

 ============================================= */
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON

	update trngUserModuleProgress set 
		quizPassed = 1, 
		quizPassedDate = i.completed, 
		userModuleFulfilled=i.completed, 
		moduleStatus = (select statusID from trngUserModuleStatus where statusTextID='Passed') 
	from inserted i 
	inner join trngUserModuleProgress tump
		on i.userModuleProgressID = tump.userModuleProgressId 
		and i.personid = tump.personid 
		and tump.quizPassed != 1 
		and moduleStatus not in (select statusID from trngUserModuleStatus where statusTextID='Passed')
	inner join quizDetail qd
		on qd.quizDetailID = i.quizDetailID
	where i.score >= qd.passMark

END