
if exists (select 1 from sysobjects where name='UserGroup_DTrig' and type='tr')
drop trigger UserGroup_DTrig

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nathaniel Hogeboom
-- Create date: 2011/02/24
-- Description:	Delete related usergroup records when a usergroup is deleted
-- =============================================
CREATE TRIGGER [dbo].[UserGroup_DTrig] ON [dbo].[userGroup]
FOR DELETE 
AS

Set Nocount On

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
	delete rights from rights r inner join deleted d
		on r.userGroupID = d.userGroupID
		
	/*delete rightsGroup from rightsGroup r inner join deleted d
		on r.userGroupID = d.userGroupID*/
END
GO
