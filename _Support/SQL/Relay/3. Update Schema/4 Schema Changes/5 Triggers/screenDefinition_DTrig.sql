ALTER    TRIGGER [dbo].[ScreenDefinition_DTrig] ON [dbo].[ScreenDefinition] 
FOR Delete AS


/*
Author  WAB

Date ???


Purpose

When a row is removed from this table, need to update the countryscope table


Mods:
WAB 13/6/01	At last got around to making this work with multiple row deletes - by using a cursor

WAB 2015-02-25 Removed calls to update the , now removed,  screen Index table
*/



Declare @Itemid int,  @oldScreenid int , @oldsortOrder int

declare sdDeleteCursor cursor for
	select 	
		itemid,
		screenid, 
		sortorder
	from 
		deleted


open sdDeleteCursor

fetch next from sdDeleteCursor 
into @itemid, @oldScreenid, @oldSortOrder


WHILE @@FETCH_STATUS = 0
	BEGIN

		exec updateScreenDefinitionScope @oldScreenid,  @oldsortOrder , @itemid

		fetch next from sdDeleteCursor 
		into @itemid, @oldScreenid, @oldSortOrder
	END

close sdDeleteCursor 
deallocate sdDeleteCursor 

