/* NJH 2016/01/19 JIRA BF-32 functionality moved to insert/update trigger */
IF exists (select 1 from sysobjects where name = 'EntityTracking_Insert')
	DROP TRIGGER EntityTracking_Insert
GO