createPlaceHolderObject @objectName = 'recordRights.recordRights_Updatehasrecordrights', @objectType = 'TRIGGER'

GO

ALTER TRIGGER [dbo].[recordRights_Updatehasrecordrights] 
   ON  [dbo].[recordrights]
   for INSERT,DELETE,UPDATE
AS 

BEGIN
/*
WAB 2010/10/18 
	This procedure keeps the hasRecordRights field on the element table up to date
	we used to do it in Cold Fusion but it kept getting out of synch.
	The hasRecordRights bit is set if there is a record right at permission 1 
	
WAB 2013/09/09
	Extended to keep track of permissions at any level on any parent table
	Any field on the parent table with a name of the form
		hasRecordRights_n   (Where n is the numeric level)
		[for backwards compatibility we support hasRecordRights which is the equivalent of hasRecordRights_1]
	NOTE 2015-11-19 now have better way of doing this just using a single column hasRecordRightsBitMask

WAB 2015-11-03
	Introduced the recordRightsOperator table which keeps track of AND/OR and NOT logic within a set of usergroups
	Also keeps track of number of userGroups attached to any record - used for doing the AND logic
	The recordRightsOperator table should always be updated through the view
	
	Add hasRecordRightsBitMask column instead of hasRecordRights initially for elements but can be used anywhere
	WAB 2016-02-03 I had mucked up the trigger when updating old style   hasrecordRights_x fields
	WAB 2016-02-24 Another problem with trigger, I wasn't filtering the hasRecordRightsBitMask update query by entityTypeID, so when doing a mass update or recordrights table (to initialise hasRecordRightsBitMask) there was all sorts of cross contamination
*/

SET NOCOUNT ON

	/* put both inserted and deleted records into a single temp table 
		saves replicating queries and can be passed into sp_execute sql
	*/	
	
	select entityTypeID, recordid, entity 
	into #id
		FROM
		(
		select * from inserted 
			union
		select * from deleted
		) As X

	IF @@rowcount = 0
		GOTO THEEND


	/* also update #id.entityTypeID table - need entityTypeID in later queries */
	update 
		#id
	set entityTypeID = s.entityTypeID
	from 
		#id i
			inner join
		schemaTable s on i.entity = s.entityName
	where i.entityTypeID is null	


	/* WAB 2015-11-03 Added an entityTypeID column, so for backwards compatibility with old code need to update it update it*/
	IF update (entity) 
	BEGIN
		update 
			recordRights
		set entityTypeID = s.entityTypeID
		from 
			inserted i
				inner join
			recordRights rr ON i.recordid = rr.recordid and i.usergroupid = rr.usergroupid and i.entity = rr.entity
				inner join
 			schemaTable s on rr.entity = s.entityName

	END
	
	IF update (entityTypeID) 
	BEGIN
		update 
			recordRights
		set entity = s.entityName
		from 
			inserted i
				inner join
			recordRights rr ON i.recordid = rr.recordid and i.usergroupid = rr.usergroupid and i.entityTypeID = rr.entityTypeID
				inner join
 			schemaTable s on rr.entityTypeID = s.entityTypeID
	
	END


	declare @sql  nvarchar(4000)
	Declare @entityTypeID int ,@tableName sysname, @columnName sysname, @Level int, @uniqueKey sysname 
	

	declare myCursor CURSOR FOR
		SELECT distinct 
			s.entityTypeID, 
			table_name, 
			column_Name, 
			case when column_name = 'hasrecordRights' then 1 else convert(int,right(column_name,len(column_name) - charindex('_',column_name,0))) end as level, 
			uniqueKey
			 
		FROM
			#id as 	 i
				inner join
			schemaTable s on (i.entityTypeID = s.entityTypeID) 
				inner join
			information_schema.columns on s.entityname = table_name and (column_name = 'hasrecordRights' or column_name like 'hasrecordRights[_]%')
		

		OPEN myCursor

		FETCH NEXT FROM myCursor
		INTO @entityTypeID,@tableName, @columnName, @Level, @uniqueKey

		WHILE @@FETCH_STATUS = 0
		BEGIN

			/* level1 rights can be generated from a combination of Level1 and Level11 so we need to look a levels 1 and 11 when deciding whether to update the hasRecordrights column) */ 
			/* wab converted to get the number of usergroups associated with a record - which might allows AND style permissions */
			set @sql = 
			' update ' + @tablename +
			' set ' + @columnName + ' = theCount
			from ' +
			@tablename + ' AS e
				inner join 
				(select id.recordid, count( distinct rr.usergroupid) as theCount from 
				#id AS id 
				left join
				recordRights rr on id.recordid = rr.recordid and (id.entityTypeID = rr.entityTypeID) and ((rr.permission & power (2,@level -1) <> 0)  ' + case when @level = 1 then ' OR (rr.permission & power (2,11 -1) <> 0)' else '' end + ')  
				where id.entityTypeID =  @entityTypeID
				group by id.recordid
				) as x on e. ' + @uniqueKey + ' = x.recordid 
			'	
		
			exec sp_executeSQL @sql, N'@entityTypeID int, @level int', @entityTypeID = @entityTypeID, @level= @level

			FETCH NEXT FROM myCursor
			INTO @entityTypeID,@tableName, @columnName, @Level, @uniqueKey

		END
		
		CLOSE myCursor
		DEALLOCATE myCursor

/* hasRecordRightsBitMask */

	declare myCursor CURSOR FOR
		SELECT distinct 
			s.entityTypeID, 
			table_name, 
			uniqueKey
			 
		FROM
			#id as 	 i
				inner join
			schemaTable s on (i.entityTypeID = s.entityTypeID) 
				inner join
			information_schema.columns on s.entityname = table_name and column_name like 'hasRecordRightsBitMask'
		

		OPEN myCursor

		FETCH NEXT FROM myCursor
		INTO @entityTypeID,@tableName, @uniqueKey

		WHILE @@FETCH_STATUS = 0
		BEGIN

			/* level1 rights are generated from a combination of Level1 and Level11 rights so we need to look a levels 11 and 11 when deciding whether to update the hasRecordrights column) */ 

			set @sql = 
			' update ' + @tablename +
			' set hasRecordRightsBitMask = isNull(
											level1|level11 
											+ level2 * power(2,2-1)
											+ level3 * power(2,3-1)
											+ level4 * power(2,4-1)
											+ level5 * power(2,5-1)
											+ level11 * power(2,11-1)
							,0)				

			from ' +
			@tablename + ' AS e
				inner join 
					( select 
							id.recordid
						,	convert(bit, sum(permission & power (2,1-1)) ) as level1
						,	convert(bit, sum(permission & power (2,2-1)) ) as level2 
						,	convert(bit, sum(permission & power (2,3-1)) ) as level3 
						,	convert(bit, sum(permission & power (2,4-1)) ) as level4
						,	convert(bit, sum(permission & power (2,5-1)) ) as level5
						,	convert(bit, sum(permission & power (2,11-1)) ) as level11
					from
						#id AS id 
							left join
						recordrights rr on id.recordid = rr.recordid and id.entityTypeID = rr.entityTypeID
						where id.entityTypeID = @entityTypeID
					group by id.entityTypeID, id.recordID
					) as rr
						 on e. ' + @uniqueKey + ' = rr.recordid 
			'	

			exec sp_executeSQL @sql, N'@entityTypeID int', @entityTypeID = @entityTypeID


			FETCH NEXT FROM myCursor
			INTO @entityTypeID,@tableName, @uniqueKey

		END
		
		CLOSE myCursor
		DEALLOCATE myCursor


		-- update recordRightsOperator
		-- check that there is an item in the table
		insert into recordRightsOperator (recordid, entityTypeID)
		select distinct i.recordID, i.entityTypeID
		from 
			#id i
				left join 
			recordRightsOperator	rro on i.recordid = rro.recordid and i.entityTypeID = rro.entityTypeID
		where
			rro.recordid is null	
			and i.entityTypeID is not null  -- can't do for entities which don't have an entityTypeID (backwards compatibility)


		/* delete any recordRightsOperator records where there are no corresponding recordrights records */
		if not exists (select 1 from inserted)
		BEGIN
			delete
				recordRightsOperator
			from 
				recordRightsOperator rro
					inner join
				deleted d  on d.recordid = rro.recordid and d.entityTypeId = rro.entityTypeId
					left join 
				recordRights rr on d.recordid = rr.recordid and d.entityTypeId = rr.entityTypeId
			where 	
				rr.recordid is null
		END

THEEND:	


END





		
