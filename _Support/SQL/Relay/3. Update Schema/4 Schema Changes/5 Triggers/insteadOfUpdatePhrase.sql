if exists (select 1 from sysobjects where name = 'InsteadOfUpdatePhrases')
drop trigger InsteadOfUpdatePhrases

GO

CREATE  trigger [dbo].[InsteadOfUpdatePhrases] 
on [dbo].[Phrases]
instead of update

as
set nocount on 

/*   
Trigger InsteadOfUpdatePhrases 
Author WAB November 2004
Purpose: To keep track of changes to phrases

Background:  This can't be done by the standard modregister because the phrasetextfield is 
		an ntext field and so isn't accessible to regular triggers

Requirements:  A table called xphrases
		You'll also need the trigger InsteadOfDeletePhrases	

Future Mods:  It may be that in the future this could be made into a more generic stored procedure if the need arises

WAB 2010/09/28 Added code to clear the phrasePointerCache if a language or country is changed
WAB 2011/05/09 changed to use new dirty field on table rather than deleting rows
WAB 2013-02-19 PhraseTest has been changed from nText to nVarchar(max).  Had to change a NOT LIKE to <>   (not sure why)
			This could now be changed into a normal trigger (only required because of the ntext problems) 
*/


	-- insert the current values into the xPhrases Table if the phrasetext has changed
	-- note that it won't catch changes of case
	-- the modified date and modified by are taken from the new record
	insert into xphrases (ident,languageid,phraseid,countryid,phrasetext,modifiedby,modified)
	select 
	d.ident,d.languageid,d.phraseid,d.countryid,d.phrasetext,i.lastupdatedby,i.lastupdated
	from deleted d inner join inserted i on i.ident = d.ident and i.phrasetext <> d.phrasetext


-- now do the actual update
update phrases
set languageid = i.languageid,
	phraseid = i.phraseid,
	countryid = i.countryid,
	defaultforthiscountry = i.defaultforthiscountry,
	phrasetext = i.phrasetext,
	createdby = i.createdby,
	created = i.created,
	lastupdated = i.lastupdated,
	lastupdatedby = i.lastupdatedby
	-- ,tempActualLanguageID = 	I.tempActualLanguageID  -- this field just here for migration


from phrases p inner join inserted i on i.ident = p.ident 


/*

This bit makes sure that there is only ever one (or no) defaultLanguage for a given Country 
and that there is always a defaultLanguage for CountryID 0 

*/

update phrases
set defaultforthiscountry = 1
--select * 
from phrases inner join inserted i on phrases.ident= i.ident
where 
i.countryid = 0
and not exists (select 1 from phrases p where p.phraseid = i.phraseid and p.countryid = i.countryid and defaultforthiscountry = 1)


-- this make sure that if a phrase has defaultforthiscountry set to 1 then all other phrases for this country have defaultforthiscountry set to 0

update phrases
set defaultforthiscountry = 0
from inserted i inner join phrases on phrases.phraseid = i.phraseid and phrases.countryid = i.countryid
where 
 i.defaultforthiscountry = 1
 and phrases.defaultforthiscountry = 1
 and  phrases.ident <> i.ident 

/* If language, country or defaultForThisCountry have changed then need to update the phrasePointerCache */

	/* update any current entries in the phrasePointerCache table to show dirty*/
	update phrasePointerCache
	set dirty = 1	
	from 
		phrasePointerCache pp	
			inner join 
		deleted d on pp.phraseid = d.phraseid
			inner join 
		inserted i on i.ident = d.ident 
	where d.countryid <> i.countryid or d.languageid <> i.languageid or d.defaultforthiscountry <> i.defaultforthiscountry

	/* tell the cache that it will need to be updated */
	delete phrasePointerCacheStatus	
	from 
		phrasePointerCacheStatus pps
			inner join 
		phraselist pl on pps.entityTypeID = pl.entityTypeID and pps.phrasetextid = pl.phraseTextID
			inner join 
		deleted d on pl.phraseid = d.phraseid
			inner join 
		inserted i on i.ident = d.ident 
	where d.countryid <> i.countryid or d.languageid <> i.languageid or d.defaultforthiscountry <> i.defaultforthiscountry

