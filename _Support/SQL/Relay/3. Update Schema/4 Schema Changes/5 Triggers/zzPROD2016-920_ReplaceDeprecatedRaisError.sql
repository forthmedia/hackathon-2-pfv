/*
WAB 2016-05-10 PROD2016-920 Alter a group of triggers which use deprecated RAISERROR syntax which is not compatible with SQL 2012
	
*/
 declare @SQL nvarchar(max), @objectname sysname, @msg nvarchar(max)
 
 declare aCursor cursor  for
 
 select 
	 object_name(id)
	,dbo.RegExReplace  (dbo.RegExReplace (text,'RAISERROR 44444 (.*)','RAISERROR ($1, 16 , 1)'), 'CREATE\s*TRIGGER', 'ALTER TRIGGER') 
 from 
	syscomments 
where text like '%raiserror 4444%'
 
 
OPEN aCursor;
FETCH NEXT FROM aCursor INTO @objectname, @SQL
WHILE @@FETCH_STATUS = 0
BEGIN   
	IF (len (@sql) > 4000)
	BEGIN
		set @msg = ' Script for ' + @objectname + ' is too long (the regExp library only does nvarchar(4000)'
		print @sql
		raisError ( @msg ,16,1)
	END
	ELSE
	BEGIN
		BEGIN TRY
			exec (@sql)
		END TRY
		BEGIN CATCH
			set @msg = @objectname + ' failed'
			raisError ( @msg ,16,1)
			print cast (@sql as nText)
		END CATCH
	END
	
	FETCH NEXT FROM aCursor INTO @objectname,@SQL
END

CLOSE aCursor;
DEALLOCATE aCursor;

