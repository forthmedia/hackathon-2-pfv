ALTER TRIGGER [dbo].[Location_HistorialLocationIDList] 
   ON  [dbo].[Location]
   AFTER INSERT
AS 
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      
      SET NOCOUNT ON;
      UPDATE location
      set HistoricalLocationIDList = locationID
      where locationID IN (select locationID from inserted)
      and HistoricalLocationIDList is null
    -- Insert statements for trigger here

END

GO