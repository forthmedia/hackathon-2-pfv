if exists (select 1 from sysobjects where name = 'UpdateOpportunityAmountTrigger')
drop trigger UpdateOpportunityAmountTrigger

GO

CREATE TRIGGER UpdateOpportunityAmountTrigger 
   ON  opportunityProduct 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select distinct case when abs(datediff(s,lastUpdated,getdate())) < 10 then lastUpdatedby end as lastupdatedby, opportunityID 
	into #changedRecords 
	from (
		select * from inserted where productOrGroup = 'P'
		union 
		select * from deleted where productOrGroup = 'P'
			and not exists (select 1 from inserted where productOrGroup = 'P')
	 ) as x

	update opportunity set budget=p.total,lastUpdated = case when c.lastUpdatedBy is not null then GetDate() else o.lastUpdated end, lastUpdatedBy = case when c.lastUpdatedBy is not null then c.lastUpdatedBy else o.lastUpdatedBy end
		from opportunity o
			inner join #changedRecords c on o.opportunityID = c.opportunityID
			inner join (select sum(subTotal) as total,opportunityID from opportunityProduct where productOrGroup='P' group by opportunityID) p
				on p.opportunityID = o.opportunityID

END
GO