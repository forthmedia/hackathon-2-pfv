if exists (select * from sysobjects where xtype='TR' and name='flagGroup_createFlagViewTrigger')
	drop trigger flagGroup_createFlagViewTrigger

GO

CREATE TRIGGER [dbo].[flagGroup_createFlagViewTrigger]
   ON  [dbo].[flagGroup]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN

-- =============================================
-- Author:        WAB
-- Create date: 27/04/2015
-- Description:  Almost a copy of flag_createFlagViewTrigger
--				 Used to update the flagView when a boolean group is added				
--   	  		 Was required when flag_createFlagViewTrigger was altered to not recreate the views when boolean flags were added and removed
--	
--	WAB 2017-01-24 PROD2016-3299 Altered so that views are updated if isBitField is changed
-- =============================================

      SET NOCOUNT ON;
	  /* has isBitField been set/unset */
	declare @isBitFieldChanged bit
	select @isBitFieldChanged = case when exists
		 (select 1 
				  from inserted i inner join deleted d on i.flagGroupID = d.flagGroupID
				  where 
					(i.formattingParameters like '%isBitField=true%' and d.formattingParameters not like '%isBitField=true%')
					OR
					(d.formattingParameters like '%isBitField=true%' and i.formattingParameters not like '%isBitField=true%')	  
				)	
		then 1 else 0 end


      IF update(flagGroupTextID) OR	@isBitFieldChanged = 1
      BEGIN
            DECLARE @tablename sysname, @viewName sysname
            DECLARE tables insensitive cursor for

            SELECT distinct tablename
            FROM
                  inserted fg
                        inner join
                  FlagEntityType fet ON fet.entityTypeID = fg.entityTypeID    
                        inner join
                  FlagType ft  ON fg.flagTypeID = ft.flagTypeID

			WHERE
				ft.name in ('radio','checkbox')		
      
                  OPEN tables
            
                  FETCH next from tables into @tablename
            
                  WHILE @@fetch_status = 0 
                  BEGIN
                        
                        print @tablename
                        set @viewName = 'v' + @tablename
                        exec createFlagView @entityName = @tablename, @viewName = @viewName
                  
                                    FETCH next from tables into @tablename
                  END   
                  
                  CLOSE tables
                  DEALLOCATE tables

      
      END


END

