if exists (select 1 from sysobjects where name = 'trigger_phrase_updatePhrasePointercache')
BEGIN
drop trigger trigger_phrase_updatePhrasePointerCache
END

GO

create trigger [trigger_phrase_updatePhrasePointerCache]
on [dbo].[Phrases] for insert, delete

as

/*
WAB 2010/09/14
This trigger automatically updates the phrasePointercache table when ever an entity translation is inserted or deleted
(similar also needs to be done if a phrase is updated and the language, country or defaultForThisCountry fields are updated

WAB 2011/05/09 changed to use new dirty field on table rather than deleting rows
WAB 2014/02/20	Altered so that it can deal with groups of status type phrases (such as oppStage_xxxxx) where entityTypeID is 0.  They will be stored in the phrasePointerCacheStatus table with a wildcard (such as oppStage[_]%) (the _ has to be escaped since it is a wildcard itself)
*/
set nocount on 

declare  @sql nvarchar(500),@languageid int, @countryid int

		/* create a temporary table 
			does a union to deal with either insert or delete */
			
			select x.phraseid, phrasetextid, entitytypeid, entityid, cast(null as varchar(50)) as entitytype, cast (null as int) as phrase_ident 				
				into 
				#TempItems
			from 
				(select phraseid from  inserted 
				union 
				select phraseid from  deleted) as x 
					inner join
				phraselist pl on x.phraseid = pl.phraseid
	
		
		/* update any current entries in the phrasePointerCache table */
		update phrasePointerCache
		set dirty = 1	
		from 
		phrasePointerCache pp	
			inner join 
		#tempitems t on t.phraseid = pp.phraseid
		
		/* tell the cache that it will need to be updated */
		delete phrasePointerCacheStatus
		from 
			phrasePointerCacheStatus pps
		inner join 
			#tempitems t on  t.entitytypeid = pps.entitytypeid and t.phrasetextID like pps.phraseTextID   /* WAB 2013/02/20	changed = to like */

		drop table #tempitems

