if exists (select * from sysobjects where name = 'countryscope_UTrig')
BEGIN
drop trigger  [dbo].[countryscope_UTrig]
END

GO



CREATE TRIGGER countryscope_UTrig ON dbo.countryScope 
FOR update
AS

/*
We have added entityTypeID to this table to speed up indexing
However in order to accept old code I have out a trigger to keep it up to date
*/
update countryScope
set entityTypeID = s.entityTypeID
from
countryScope cs
inner join 
inserted as i
	on cs.countryscopeid = i.countryscopeid 
inner join 
deleted as d
	on d.countryscopeid = i.countryscopeid 
inner join
schemaTable s
on s.entityName = i.entity
where d.entity <> i.entity

GO