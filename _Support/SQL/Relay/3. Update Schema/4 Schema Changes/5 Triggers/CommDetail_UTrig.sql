SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON
GO
ALTER     TRIGGER [dbo].[CommDetail_UTrig] ON [dbo].[commdetail]
FOR UPDATE,INSERT
AS

/*
Mods

WAB 2008/07/10 Added Trigger for mostSignificantCommStatusID
WAB 2009/01/27 Altered and improved Trigger
WAb 2009/11/26  Improved performance during an insert
*/


IF UPDATE (PersonId) OR
   UPDATE (CommFileId) OR
   UPDATE (LocationId) OR
   UPDATE (Feedback) OR
   UPDATE (DateSent) OR
   UPDATE (CommTypeId) OR
   UPDATE (CommReasonId) OR
   UPDATE (CommStatusId)

	INSERT INTO CommDetailHistory (CommDetailId
				      ,commid
	                              ,PersonId
	                              ,CommFileId
	                              ,LocationId
	                              --,Fax
	                              ,Feedback
	                              ,ContactDetails
	                              ,DateSent
	                              ,CommTypeId
	                              ,CommReasonId
        	                      ,CommStatusId
	                              ,lastUpdated
	                              ,lastUpdatedBy
				      ,test
					, internal	)

	SELECT i.CommDetailId
	      ,i.commid
	      ,i.PersonId
	      ,i.CommFileId
	      ,i.LocationId
	      --,i.Fax
	      ,i.Feedback
	      ,i.ContactDetails
	      ,i.DateSent
	      ,i.CommTypeId
	      ,i.CommReasonId
	      ,i.CommStatusId
	      ,i.lastUpdated
	      ,i.lastUpdatedBy
	      ,i.test
	, i.internal	
	 FROM inserted i 
	 JOIN CommDetail cod ON i.CommDetailId = cod.CommDetailId 
        WHERE i.commStatusId IS NOT NULL       



/*

WAB 2008/07/10 trying a new approach on most significant status
Have a calculated field which is updated everytime the commstatusid is updated
WAB 2009/01/27 modify to do less work
Check whether current status is more or less significant than previous rather than trawling through whole of the commdetailhistory table
Can still do a complete update of mostSignificantCommStatusID by setting its value to null (or just updating it)

WAB 2009/11/26 discovered that this code was very inefficient during an insert, therefore have added in a specific section for dealing with inserts

*/

IF  (select count(1) from deleted) = 0  -- ie an insert
BEGIN
	-- this is an insert so mostSignificantCommStatusID will always just be CommStatusId
 update commdetail
 set mostSignificantCommStatusID = I.CommStatusId
	FROM
commdetail cd
		inner join
	inserted i on cd.commdetailid = i.commdetailid

END 
ELSE IF   UPDATE (CommStatusId) 
BEGIN
		
	update commdetail
	set mostSignificantCommStatusID = 
	    case 
		-- no change in status so no change in significance	     
		when i.commstatusid = d.commstatusid then d.mostSignificantCommStatusID
		-- new significance greater than old, so use new status (deals with null d. table (insert)
		when newStatus.significance > isNull(oldStatus.significance,-9999) then i.commstatusid
		-- new significance less than old, so leave old 
		when newStatus.significance < oldStatus.significance then d.mostSignificantCommStatusID
		else d.mostSignificantCommStatusID
	    end	
	from
	commdetail cd
		inner join
	inserted i on cd.commdetailid = i.commdetailid
		left join 
	deleted d on i.commdetailid = d.commdetailid 		
	left join commdetailstatus oldStatus on oldStatus.commstatusid = d.mostSignificantCommStatusID
	left join commdetailstatus newStatus on newStatus.commstatusid = i.commstatusid

END

else IF UPDATE (mostSignificantCommStatusID) -- allows us to update mostSignificantCommStatusID by setting it to null and allowing the trigger to fire and it won't add a commdetailhistory record
BEGIN
-- select commdetail.commdetailid, isnull(commdetail.commstatusid,cds1.commstatusid),commdetail.commstatusid,cds1.commstatusid
update commdetail
set mostSignificantCommStatusID = isnull(cds1.commstatusid, commdetail.commstatusid)
from commdetail
inner join 
(
select i.commdetailid, max(cds.significance) as significance from inserted i
 left join 
 (commdetailhistory cdh inner join commdetailstatus cds on cdh.commstatusid = cds.commstatusid ) 
 on i.commdetailid = cdh.commdetailid 
group by i.commdetailid
) as x 
on commdetail.commdetailid = x.commdetailid
left join 
commdetailstatus cds1 on x.significance = cds1.significance 

END
Return
