IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Location_IUTrig]'))
DROP TRIGGER [dbo].[Location_IUTrig]
GO

CREATE TRIGGER [dbo].[Location_IUTrig] ON [dbo].[Location] FOR INSERT,UPDATE AS

set nocount on

/* check whether this is the first instance of the trigger to be called, as it could be recursive with an update happening on the lead table */
if (TRIGGER_NESTLEVEL(object_id('Location_IUTrig')) = 1)
begin
	/* update the convertedFromLocationID column on the lead table */
	if update(convertedFromLeadID)
	begin
		update lead
			set convertedLocationID = i.locationID
		from
			inserted i
				inner join lead l on l.leadID = i.convertedFromLeadID
				left join deleted d on d.locationID = i.locationID
		where
			l.convertedLocationID is null
			and d.convertedFromLeadID is null 
			and i.convertedFromLeadID is not null
	end
	
	/* NJH 2015/04/20 set the location as HQ for the org if one doesn't already exist */
	if not exists (select 1 from deleted)
	begin
		declare @hqFlagId int
		select @hqFlagId = flagId from flag where flagTextID='HQ'
		
		insert into integerFlagData (flagId,entityId,data,createdBy,lastUpdatedBy,lastUpdatedByPerson)
		select @hqFlagId,i.organisationID,hqLoc.locationID,i.createdBy,i.lastUpdatedBy,i.lastUpdatedByPerson
			from inserted i
				inner join (select organisationID, min(locationID) as locationID from inserted group by organisationID) hqLoc on hqLoc.locationID = i.locationID
				left join integerFlagData ifd on ifd.entityID = i.organisationID and ifd.flagID = @hqFlagId
		where
			ifd.entityID is null
	end
end

GO
/* WAB 2016-03-16 make this trigger run last because inserting an integerflag triggers a foreign key check which relies on vEntityName being up to date.  And this is done in an earlier trigger */
sp_settriggerorder @triggername='Location_IUTrig', @order ='last', @stmttype = 'insert'