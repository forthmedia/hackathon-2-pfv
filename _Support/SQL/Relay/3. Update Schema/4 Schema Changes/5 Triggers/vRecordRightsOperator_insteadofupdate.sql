createPlaceHolderObject @objectName = 'vRecordRightsOperator.vRecordRightsOperator_insteadofupdate', @objectType = 'insteadoftrigger'

GO

alter trigger vRecordRightsOperator_insteadofupdate 
on vRecordRightsOperator
instead of update
AS

update recordRightsOperator
set		
	  entityTypeId = i.entityTypeId
	, recordID = i.recordID
	, groupAnd = isnull(i.groupAnd_Level1,0) 
				+ (power(2,2-1) * isnull(i.groupAnd_Level2,0))
				+ (power(2,3-1) * isnull(i.groupAnd_Level3,0))
				+ (power(2,4-1) * isnull(i.groupAnd_Level4,0))
				+ (power(2,5-1) * isnull(i.groupAnd_Level5,0))
				+ (power(2,11-1) * isnull(i.groupAnd_Level11,0))
	
	, groupNot = isnull(i.groupNot_Level1,0)
				+ (power(2,2-1) * isnull(i.groupNot_Level2,0))
				+ (power(2,3-1) * isnull(i.groupNot_Level3,0))
				+ (power(2,4-1) * isnull(i.groupNot_Level4,0))
				+ (power(2,5-1) * isnull(i.groupNot_Level5,0))
				+ (power(2,11-1) * isnull(i.groupNot_Level11,0))
from
	inserted i	
		inner join 
	recordRightsOperator rro on i.recordid = rro.recordid and i.entityTypeId = rro.entityTypeId


/* could be an argument for reseting the AND and NOT fields if there are no recordrights left at that level */
