CREATEPLACEHOLDEROBJECT 'Trigger',  'IntegerFlagData.IntegerFlagData_ForeignKey'

GO

alter TRIGGER [dbo].[IntegerFlagData_ForeignKey] ON [dbo].[IntegerFlagData] FOR INSERT,UPDATE AS 	

/*
WAB 2016-01-05	New trigger to enforce some level of foreign key constraint when integer flags are storing entityIDs
				Note that it does allow the linked entity to be in a 'del' table
*/

declare 
	  @flagid int
	, @entityid int
	, @data int
	, @linkedEntityType sysname

select 
	  @flagid = i.flagid
	, @entityID = i.entityID 
	, @data = i.data
	, @linkedEntityType = st.entityName
from 
	inserted i
		inner join
	flag f on f.flagID = i.flagid
		inner join
	schemaTable st on f.linksToEntityTypeID = st.entityTypeID
		left  join
	ventityName e on e.entityID = i.data and e.entityTypeID = f.linksToEntityTypeID
where
		st.in_vEntityName = 1
	and f.linksToEntityTypeID is not null		
	and e.entityID is null
		


IF (@flagid is not null)
    BEGIN
		declare @msg varchar(100) = 'IntegerFlagData Foreign Key Error.  FlagID: ' + convert(varchar,@flagID) + '. EntityID: ' + convert(varchar,@entityID) + '. ' + @linkedEntityType + ' ' + convert(varchar,@Data) + ' does not exist'
        RAISERROR (@msg,16,1)
        ROLLBACK TRANSACTION
    END

