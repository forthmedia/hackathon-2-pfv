/* 
WAB 2015-02-12
This trigger relies on the existence of vLead
This will not exist automatically if there are no flags on the lead table.  
So check for and create it here just in case  (otherwise some code in RW2014_data.sql which copies opportunities to leads fails)
*/
if not exists (select 1 from sysobjects where name = 'vlead')
BEGIN
	exec createFlagView @entityName= 'lead', @viewName = 'vLead'
END


/*
Now create the trigger
*/

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Lead_IUTrig]'))
DROP TRIGGER [dbo].[Lead_IUTrig]
GO

CREATE TRIGGER [dbo].[Lead_IUTrig] ON [dbo].[Lead] FOR INSERT,UPDATE AS

set nocount on
/* check whether this is the first instance of the trigger to be called, as it could be recursive with an update trigger happening on the person and location tables */
if (TRIGGER_NESTLEVEL(object_id('Lead_IUTrig')) = 1)
begin

	if (update(convertedPersonID) or update(convertedLocationID) or  update(convertedOpportunityID))
		select l.* into #convertedLeads from inserted i inner join vLead l on l.leadID = i.leadID

	/* update the convertedFromLeadID column on the person table */
	if update(convertedPersonID) 
	begin
		update person
			set convertedFromLeadID = i.leadID
		from
			inserted i
				inner join person p on p.personID = i.convertedPersonID
				left join deleted d on d.leadID = i.leadID
		where
			p.convertedFromLeadID is null
			and d.convertedPersonID is null 
			and i.convertedPersonID is not null
			
		if (@@rowCount > 0)
		begin	
			exec convertLeadProfiles @convertedEntityname='person'
		end
	end

	/* update the convertedFromLeadID column on the location table */
	if update(convertedLocationID)
	begin
		update location
			set convertedFromLeadID = i.leadID
		from
			inserted i
				inner join location l on l.locationID = i.convertedLocationID
				left join deleted d on d.leadID = i.leadID
		where
			l.convertedFromLeadID is null
			and d.convertedLocationID is null 
			and i.convertedLocationID is not null
		
		if (@@rowCount > 0)
		begin
			exec convertLeadProfiles @convertedEntityname='location'
			exec convertLeadProfiles @convertedEntityname='organisation'
		end
	end
	
	
	/* update the convertedFromLeadID column on the opportunity table */
	if update(convertedOpportunityID)
	begin
		update opportunity
			set convertedFromLeadID = i.leadID
		from
			inserted i
				inner join opportunity o on o.opportunityID = i.convertedOpportunityID
				left join deleted d on d.leadID = i.leadID
		where
			o.convertedFromLeadID is null
			and d.convertedOpportunityID is null 
			and i.convertedOpportunityID is not null
		
		if (@@rowCount > 0)
		begin
			exec convertLeadProfiles @convertedEntityname='opportunity'
		end
	end
end

if update(acceptedByPartner)
begin
	update lead set acceptedByPartnerDate  = getDate()
	from
		lead l
		inner join inserted i on i.leadId = l.leadID
		left join deleted d on d.leadID = i.leadID
	where
		i.acceptedByPartner = 1
		and isNull(d.acceptedByPartner,0) = 0
		and i.acceptedByPartnerDate is null
end

/* if the partner has changed, reset the viewedByPartnerDate */
if update(partnerLocationID)
begin
	update lead 
		set viewedByPartnerDate = null
	from
		lead l
		inner join inserted i on i.leadId = l.leadID
		inner join deleted d on d.leadID = i.leadID and i.partnerLocationID <> d.partnerLocationID
	where
		l.viewedByPartnerDate is not null
end