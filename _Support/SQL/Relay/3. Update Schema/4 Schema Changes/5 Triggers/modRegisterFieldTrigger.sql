IF exists (select 1 from sysobjects where name = 'ModRegisterFieldTrigger')
	DROP TRIGGER ModRegisterFieldTrigger
GO


CREATE                TRIGGER [dbo].[ModRegisterFieldTrigger] ON [dbo].[ModRegister] 
FOR INSERT
AS

/***************************************************************************************
** Name   : ModRegisterFieldTrigger
** Created: 20/09/05
** By     : WAB
**
** Purpose: 
**
** A trigger on the ModRegister Table
** To detect changes to individual flags (or fields) which require a special trigger to fire
** as defined in the fieldtrigger table 
** Arguments:
**      None
**
** The triggers which fire are stored procedures which have to be able to accept the following parameters:
** Pre May 2011 
** For Flags
**    @flagid
**    @entityID
**    @modaction
**    @triggerInitiatedBy
**    @RecordUpdatesAgainstID
**
** For other tables  (eg person/location  etc.    - must be being tracked by modregister)
**    @modEntityID
**    @entityID
**    @modaction
**    @triggerInitiatedBy
**    @RecordUpdatesAgainstID
**
** Post May 2011
** To improve performance @entityid is no longer passed
**    instead a temporary table called #modifiedEntityIDs is passed to each stored procedure  
**
** Details FieldTriggerTable table
** FieldType - can be Flag, flaggroup, modentity
** FieldID  - a flagid, flaggroupid or modentityid (from modentitydef table)
** Ordering index - order the triggers fire if more than one
** Active 1/0
** TriggerName  - name of the stored procedure to run
** 
**
** 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00		20/09/05  	WAB  Original
**          2008/03/18  WAB   Added code so that whole record inserts automatically fire all the triggers on that table
**        	06/06/08  	WAB/JDT    Above code wasn't correct and was firing all fields on all modifications, fixed 
**          2008/06/11	WAB    added isNUll to isnull(i.actionByCF,0).  This is because latest modRegister code leaves actionByCF null if the lastupdated field is not being updated
**          20??              At some point this code was extended to work when records were inserted, previously it had only been running on updates.  But this code did not get widely circulated 
** 			2011/02/17  WAB after a change to mod register so that flags were given their own modEntityID there was a conflict with the code which did whole record inserts
**2.00      2011/05/11  WAB Major change to improve performance.  No longer pass a single @entityID, but a temporary table (#modifiedEntityIDs) with all affected entityIDs.
                             Also pass the id of person doing the update (used to just be the usergroup) 
**			2013-05-15 	PPB Case 435168 added brackets to SQL statement 
**			2014-01-27 	WAB/NJH	CASE 438718 Deal with performance problems related to checking whether fieldtrigger takes #modifiedEntityIDs table
**								Added a column takesModifiedEntityIDsTable to the fieldtrigger table and populate it automatically
**			2014-01-30	WAB	At end of trigger, update the modDate to reflect the current time, not the time at the beginning of the request
**										 
** Enhancements required
** 
**
****************************************************************************************/


if @@rowcount = 0
      return

set nocount on

declare @this_flagid int, @this_modentityid int, @this_entityid int, 
@triggerName varchar(100), @this_Modaction varchar(10), @this_orderingIndex int,
@this_ActionByCF int, @This_RecordUpdatesAgainstID int, @this_ActionByPerson int,
@this_PassOldNewValues int,@this_ActionedFieldID int,
@OldValue varchar(4000),@NewValue varchar(4000),
@this_FieldMonitor int, @debug bit, @debugrows int, @takesModifiedEntityIDsTable bit


set @debug = 0
if (@debug = 1) 
      BEGIN
      select @debugrows = count(1) from inserted
      print convert(varchar, getdate(),109) + ' ModRegisterFieldTrigger Start. ' + convert(varchar,@debugrows) + ' rows inserted into modregister ' 
      END
      

-- this cursor brings back any rows inserted into the modregister table 
-- which have an entry in the FieldTrigger table
-- The entry in the FieldTrigger table may be linked to a flag, flaggroup or an individual field
-- Now created a temporary table and then create a cursor(s) on bits of the table

select 
      triggerName ,
      isnull(i.flagid,0) as flagid, 
      isnull(i.actionByCF,0) as actionbycf, 
      modentityid,recordid,action, ft.orderingindex, 
      case when isNull(ft.RecordUpdatesAgainstID,0) = 0 then isnull(i.actionByCF,0) else isNull(ft.RecordUpdatesAgainstID,0) end as RecordUpdatesAgainstID,
      actionByPerson,
      takesModifiedEntityIDsTable
into #triggers

from 
      inserted i 
            inner join 
      fieldtrigger ft on (i.flagid is not null and ft.fieldType = 'flag' and fieldid = i.flagid)  -- flags
                              or (i.flagid is null and ft.fieldType = 'modentity' and fieldid = i.modentityid ) -- fields
where 
      ft.active = 1


union

-- flaggroups
select 
      triggerName,
      i.flagid, 
      isnull(i.actionByCF,0), 
      modentityid,recordid,action, ft.orderingindex, 
      case when isNull(ft.RecordUpdatesAgainstID,0) = 0 then isnull(i.actionByCF,0) else isNull(ft.RecordUpdatesAgainstID,0) end,
      actionByPerson,
      takesModifiedEntityIDsTable
from 
      inserted i 
            inner join 
      flag f on i.flagid is not null  and i.flagid = f.flagid
            inner join 
      fieldtrigger ft on (ft.fieldType = 'flaggroup' and fieldid = f.flaggroupid)
where 
      ft.active = 1



union

-- when a whole record is inserted we want to fire any triggers for individual fields on that table
-- modified 2011/02/17 because when we added the AnyFlag fieldname the query went wrong
select 
      triggerName ,
      isnull(i.flagid,0),
      isnull(i.actionByCF,0), 
      med2.modentityid,recordid,action, ft.orderingindex, 
      case when isNull(ft.RecordUpdatesAgainstID,0) = 0 then isnull(i.actionByCF,0) else isNull(ft.RecordUpdatesAgainstID,0) end, 
      actionByPerson,
      takesModifiedEntityIDsTable
from 
      inserted i 
            inner join 
      modentitydef med on med.modentityid = i.modentityid and med.fieldname = 'Whole Record' and i.flagid is null
            inner join 
      modentitydef med2 on med.tablename  = med2.tablename  and med2.fieldname  <> 'AnyFlag'
            inner join 
      fieldtrigger ft 
            on (ft.fieldType = 'modentity' and ft.fieldid = med2.modentityId)
where 
      ft.active = 1
      and   (right(i.Action,1)='A' or right(i.Action,1)='D')            /* 2013-05-15 PPB Case 435168 added brackets */

order by 
      orderingindex

set @debugrows = @@rowCount

      IF (@debug = 1) print convert(varchar,@debugrows ) + ' rows of Field Triggers'

DECLARE Trigger_cursor CURSOR LOCAL STATIC FOR
select distinct triggername, flagid, actionbycf, modentityid,action,recordupdatesAgainstid ,actionByPerson, takesModifiedEntityIDsTable
from #triggers

--Select @OLdvalue=oldval from inserted
--Select @Newvalue=newval from inserted

OPEN Trigger_cursor
-- Perform the first fetch.
FETCH NEXT FROM Trigger_cursor into @triggerName, @this_flagid, @this_actionByCF, @this_modentityid, @this_modaction, @this_RecordUpdatesAgainstID,@this_actionByPerson,@takesModifiedEntityIDsTable
WHILE @@FETCH_STATUS = 0

BEGIN

IF (@debug = 1) print 'In modRegisterFieldTrigger: ' + @triggername + '  Flag: ' + convert(varchar,isnull(@this_flagid,0)) + ' EntityTypeID: ' + convert(varchar,@this_modentityid)


/*
print 'FlagID: ' + convert(varchar,@this_flagid) 
print 'FieldID: ' + convert(varchar,@this_ActionedFieldID)
print 'Trigger Name: ' +@triggerName
*/    

      IF @triggerName = ''
            print 'blank trigger field'   
      ELSE
        BEGIN
        	/*  IF takesModifiedEntityIDsTable is null then we need to look at the sysComments table to work out whether the trigger is of Version1 or Version2 type 
        		We update the takesModifiedEntityIDsTable field on the fieldtrigger table so we never have to do this query again
        	*/
            IF @takesModifiedEntityIDsTable is null
                  BEGIN
                        UPDATE 
                        fieldTrigger
                        SET takesModifiedEntityIDsTable = case when EXISTS (
                        	SELECT 1 
                        	FROM 
                              sysobjects o 
                                    inner join
                              syscomments c on c.id = o.id
                        	WHERE 
                        		o.name = triggerName
                              and text like '%#modifiedEntityIDs%') then 1 else 0 end
						WHERE triggerName = @triggername
						
                              
                        select      @takesModifiedEntityIDsTable = takesModifiedEntityIDsTable from fieldTrigger where triggerName = @triggername
                  
                  END


                  /* does trigger accept a temporary table */
                  IF @takesModifiedEntityIDsTable = 1
                  BEGIN


                        /* does accept a temporary table */
                        select recordid as entityid
                        into #modifiedEntityIDs
                        from #triggers
                        where triggername = @triggername
                                    and flagid = @this_flagid
                                    and modentityid =  @this_modentityid 
                        
                        IF (@debug = 1) print convert(varchar, getdate(),109) + 'Call Trigger ' + @triggerName + ' by temporary table with '+ convert(varchar,@@rowcount) +  ' rows ' 
                        if @this_flagid <> 0 
                              exec @triggerName @flagid = @this_flagid , @modaction = @this_modaction, @RecordUpdatesAgainstID = @this_RecordUpdatesAgainstID, @triggerInitiatedBy = @this_ActionByCF, @triggerInitiatedByPersonID = @this_actionByPerson
                        else
                              exec @triggerName @modentityid = @this_modentityid ,  @modaction = @this_modaction, @RecordUpdatesAgainstID = @this_RecordUpdatesAgainstID, @triggerInitiatedBy = @this_ActionByCF, @triggerInitiatedByPersonID = @this_actionByPerson

                        drop table #modifiedEntityIDs
                        IF (@debug = 1) print convert(varchar, getdate(),109) + 'Call Trigger Done '

                  END
                  ELSE
                  BEGIN

                        /* doesn't accept an entityID table, have to cursor through each item and make individual call */

                        DECLARE entityID_cursor CURSOR LOCAL STATIC FOR
                        select distinct recordID
                        from #triggers
                        where triggername = @triggerName
                                    and flagid = @this_flagid
                                    and modentityid =  @this_modentityid 

                        OPEN entityID_cursor
                        FETCH NEXT FROM entityID_cursor into @this_entityid

                        WHILE @@FETCH_STATUS = 0
                              BEGIN

                                    if (@debug = 1) print convert(varchar, getdate(),109) + 'Call trigger ' + @triggerName + ' for entityID ' + convert(varchar,@this_entityid)
                                    if @this_flagid <> 0
                                          exec @triggerName @flagid = @this_flagid , @entityid  = @this_entityid  , @modaction = @this_modaction, @RecordUpdatesAgainstID = @this_RecordUpdatesAgainstID, @triggerInitiatedBy = @this_ActionByCF
                                    else
                                          exec @triggerName @modentityid = @this_modentityid , @entityid  = @this_entityid  , @modaction = @this_modaction, @RecordUpdatesAgainstID = @this_RecordUpdatesAgainstID, @triggerInitiatedBy = @this_ActionByCF


                              FETCH NEXT FROM entityID_cursor into @this_entityid
                              END
                        CLOSE entityID_cursor
                        DEALLOCATE entityID_cursor
                  END

        END
        
FETCH NEXT FROM Trigger_cursor into @triggerName, @this_flagid, @this_actionByCF, @this_modentityid, @this_modaction, @this_RecordUpdatesAgainstID,@this_actionByPerson,@takesModifiedEntityIDsTable
END     


/* WAB 2014-01-30 
	To prevent problems when we come to synch from modregister the moddate needs to be the date at the end of the request not the beginning.
	This is particularly a problem if a fieldTrigger takes a long time to run.
	So we will update moddate to the current date.  
	If we ever need to order by start of transaction then use the ID columns 
*/

	update modRegister
	set moddate = getdate()
	from inserted i inner join modregister mr  on mr.ID = i.ID
	

if (@debug = 1) print convert(varchar, getdate(),109) +  ' ModRegisterFieldTrigger End '


