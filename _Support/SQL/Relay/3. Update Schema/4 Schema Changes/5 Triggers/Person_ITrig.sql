/*
	NJH 2014/11/03 - have commented this out as there is now a single insert/update trigger
	
	
	
--*
 * Mods 
 *
 * WAB 04.10.05   added select statement to bring back inserted records
 *--


ALTER  TRIGGER [dbo].[Person_ITrig] ON [dbo].[Person] FOR INSERT AS

--*
 * VALIDATION RULE FOR FIELD 'PersonID'
 *--
IF (SELECT Count(*) FROM inserted WHERE NOT (PersonID>0)) > 0
    BEGIN
        RAISERROR(778256, 16, 1)
        ROLLBACK TRANSACTION
    END

-- NJH 2012/04/24 - if the organisationID and locationId aren't synched, then throw an error
if exists (select 1 from inserted i
	inner join location l on l.locationID = i.locationID and l.organisationID <> i.organisationID)
begin
	raiserror('Persons location and organisation ids are not in synch',16,1)
	rollback transaction
end

-- Added WAB 04/10/05
-- since personid is not an identity column we can't use @identity_inserted so instead return the inserted ids
SELECT personID from INSERTED

*/

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Person_ITrig]'))
DROP TRIGGER [dbo].[Person_ITrig]
GO