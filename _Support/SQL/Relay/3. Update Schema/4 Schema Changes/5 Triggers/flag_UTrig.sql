ALTER     TRIGGER [dbo].[Flag_UTrig] ON [dbo].[flag] FOR UPDATE AS

/*
Mods

1.1	2/4/01  WAB added code to make flagtextids unique and valid
1.2	27/4/01  WAB added query to bring back the updated record
1.21	16/6/01	 WAB corrected bug caused by update and insert triggers both having cursors called myCursor
WAB 2015-04-13  remove code which returns the whole udpated record
WAB 2016-05-10 PROD2016-920 Alter deprecated RAISERROR syntax which is not compatible with SQL 2012
*/



Set noCount On
/* NJH 2016/12/12 moved to IU trigger as common to both
/*
 * PREVENT NULL VALUES IN 'FlagGroupID'
 */
IF (SELECT Count(*) FROM inserted WHERE FlagGroupID IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''FlagGroupID'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'Name'
 */
IF (SELECT Count(*) FROM inserted WHERE Name IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''Name'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'CreatedBy'
 */
IF (SELECT Count(*) FROM inserted WHERE CreatedBy IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''CreatedBy'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'Created'
 */
IF (SELECT Count(*) FROM inserted WHERE Created IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''Created'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'LastUpdatedBy'
 */
IF (SELECT Count(*) FROM inserted WHERE LastUpdatedBy IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''LastUpdatedBy'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'LastUpdated'
 */
IF (SELECT Count(*) FROM inserted WHERE LastUpdated IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''LastUpdated'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END



/*
*  Make sure that flagTextID is valid
Added by WAB 2/4/01
*/

	declare @FlagID int
	declare @FlagTextID varchar(100)
	declare @newFlagTextID varchar(100)


	Declare flagUCursor Cursor
	For
	select 	i.flagTextID, i.flagid
	from 	inserted i inner join deleted d 
		on i.flagid = d.flagid
	where 	i.flagTextID <> d.flagTextID

	Open flagUCursor

	Fetch Next From flagUCursor
	INTO @FlagTextID, @flagid

	While @@Fetch_Status = 0
	BEGIN
		exec stripNonCFVariableCharacters @inString = @FlagTextID, @outString = @newFlagTextID output
		if @newFlagTextID <> @FlagTextID
			update flag set flagTextID = @newFlagTextID
			where flagID = @flagid

	Fetch Next From flagUCursor
	INTO @FlagTextID,  @flagid
			
	END

	close flagUCursor
	Deallocate flagUCursor

/*
Make sure that flagtextids are unique (or blank)
Added by WAB 2/4/01
*/

IF  exists
	(select 
		f.flagtextID , count(1)
	from 
		flag f1
			inner join
		flag f 
			on f.flagtextid = f1.flagtextid
			and isNull(f.flagtextid,'') <> ''
			inner join 
		inserted i
			on f.flagid = i.flagid
	group by 
		f.flagtextID 
	having count(1) > 1) 

    BEGIN
        RAISERROR ('FlagTextIDs must be unique (or blank)',16,1)
        ROLLBACK TRANSACTION
    END
*/

/*
 * CASCADE UPDATES TO 'BooleanFlagData'
 */
IF UPDATE(FlagID)
    BEGIN
       UPDATE BooleanFlagData
       SET BooleanFlagData.FlagID = inserted.FlagID
       FROM BooleanFlagData, deleted, inserted
       WHERE deleted.FlagID = BooleanFlagData.FlagID
    END

/*
 * PREVENT UPDATES IF DEPENDENT RECORDS IN 'TextFlagData'
 */
IF UPDATE(FlagID)
    BEGIN
        IF (SELECT COUNT(*) FROM deleted, TextFlagData WHERE (deleted.FlagID = TextFlagData.FlagID)) > 0
            BEGIN
        RAISERROR(778250, 16, 1)
                ROLLBACK TRANSACTION
            END
    END

/*
 * PREVENT UPDATES IF DEPENDENT RECORDS IN 'DateFlagData'
 */
IF UPDATE(FlagID)
    BEGIN
        IF (SELECT COUNT(*) FROM deleted, DateFlagData WHERE (deleted.FlagID = DateFlagData.FlagID)) > 0
            BEGIN
        RAISERROR(778253, 16, 1)
                ROLLBACK TRANSACTION
            END
    END

/*
 * PREVENT UPDATES IF DEPENDENT RECORDS IN 'IntegerFlagData'
 */
IF UPDATE(FlagID)
    BEGIN
        IF (SELECT COUNT(*) FROM deleted, IntegerFlagData WHERE (deleted.FlagID = IntegerFlagData.FlagID)) > 0
            BEGIN
        RAISERROR(778256, 16, 1)
                ROLLBACK TRANSACTION
            END
    END		