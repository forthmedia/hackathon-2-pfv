
/*
WAB 2016-10-31	PROD2016-2614 When inserting a countryGroup (null ISOCode) do not add a self-referencing record to countryGroup
*/

ALTER  TRIGGER [dbo].[Country_ITrig] ON [dbo].[Country] FOR INSERT AS


 /* 
	A country must have its own self-referencing record in the countryGroup Table,
	countrygroups do not
 */	

 insert countrygroup(CountryGroupID, CountryMemberID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
 select CountryId,CountryId,CreatedBy, Created, LastUpdatedBy, LastUpdated
   from inserted
  where inserted.isocode is not null 

/* 
	Add a record to the All Countries Group
	WAB 2016 - we have gradually tried to remove places where this is referred to in code.  
	Modern convention is that if something is to be visible to all countries then no countryScoping at all is applied
*/	
insert countrygroup(CountryGroupID, CountryMemberID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
 select 37,CountryId,CreatedBy, Created, LastUpdatedBy, LastUpdated
   from inserted 
   where inserted.isocode is not null

