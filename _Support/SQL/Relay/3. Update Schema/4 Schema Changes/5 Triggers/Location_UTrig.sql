IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Location_UTrig]'))
DROP TRIGGER [dbo].[Location_UTrig]
GO

CREATE TRIGGER [dbo].[Location_UTrig] ON [dbo].[Location] FOR UPDATE AS

/*
 * VALIDATION RULE FOR FIELD 'LocationID'
 */
IF (SELECT Count(*) FROM inserted WHERE NOT (LocationID>0)) > 0
    BEGIN
        RAISERROR(778254, 16, 1)
        ROLLBACK TRANSACTION
    END

/*
 * VALIDATION RULE FOR FIELD 'FaxStatus'
 */
IF (SELECT Count(*) FROM inserted WHERE NOT (FaxStatus>=-1)) > 0
    BEGIN
        RAISERROR(778255, 16, 1)
        ROLLBACK TRANSACTION
    END
/*
 * PREVENT UPDATES IF DEPENDENT RECORDS IN 'LocationDataSource'
 */
IF UPDATE(LocationID)
    BEGIN
        IF EXISTS(SELECT * FROM deleted d
					inner join LocationDataSource  ld on ld.LocationID = d.LocationID)
        BEGIN
			RAISERROR(778259, 16, 1)
			ROLLBACK TRANSACTION
        END
    END

/*
 * PREVENT UPDATES IF DEPENDENT RECORDS IN 'Person'
 */
IF UPDATE(LocationID)
    BEGIN
        IF EXISTS(SELECT * FROM deleted d
					inner join Person p on p.LocationID = d.LocationID)
        BEGIN
			RAISERROR(778267, 16, 1)
            ROLLBACK TRANSACTION
        END
    END

--Reset FaxStatus:
if update(fax)
Begin
Update L
   Set FaxStatus=0 
  from Location l
	 inner join Inserted I on I.LocationID = l.LocationID
	 inner Join Deleted  D on D.LocationID = l.LocationID
Where ISNULL(I.Fax, '')<>ISNULL(D.Fax, '')
End

if (update(sitename) or update(address1) or update(address2) or update(address3) or update(address4) or update(address5) or update(postalCode) or update(countryID))
begin
Update Location 
   Set latlongSource='GOOGLE UPDATE REQD' 
  from Location 
	 inner join Inserted I on I.LocationID = Location.LocationID
	 inner Join Deleted  D on D.LocationID = Location.LocationID
Where 
(ISNULL(I.Address1, '')!=ISNULL(D.Address1, '')
or ISNULL(I.Address2, '')!=ISNULL(D.Address2, '')
or ISNULL(I.Address3, '')!=ISNULL(D.Address3, '')
or ISNULL(I.Address4, '')!=ISNULL(D.Address4, '')
or ISNULL(I.Address5, '')!=ISNULL(D.Address5, '')
or ISNULL(I.PostalCode, '')!=ISNULL(D.PostalCode, '')
or I.CountryID!=D.CountryID) 
AND Location.Latitude is not NULL 
AND Location.Longitude is not NULL

	
	/* NJH 2013/09/12 RW2013Roadmap 2 - update matchname where core fields have changed */
	/* NJH 2016/12/05 JIRA PROD2016-1190 - matchname now just holds the sitename. This is dealt with below
	update location set matchname=null
	from location
		inner join inserted i on i.locationID = location.locationID
		inner join deleted d on d.locationID = location.locationID
	where ltrim(rtrim(isNull(i.sitename,''))) <> ltrim(rtrim(isNull(d.sitename,''))) or 
		ltrim(rtrim(isNull(i.address1,''))) <> ltrim(rtrim(isNull(d.address1,''))) or 
		ltrim(rtrim(isNull(i.address2,''))) <> ltrim(rtrim(isNull(d.address2,''))) or 
		ltrim(rtrim(isNull(i.address3,''))) <> ltrim(rtrim(isNull(d.address3,''))) or 
		ltrim(rtrim(isNull(i.address4,''))) <> ltrim(rtrim(isNull(d.address4,''))) or 
		ltrim(rtrim(isNull(i.address5,''))) <> ltrim(rtrim(isNull(d.address5,''))) or 
		ltrim(rtrim(isNull(i.postalCode,''))) <> ltrim(rtrim(isNull(d.postalCode,''))) or 
		ltrim(rtrim(isNull(i.countryID,''))) <> ltrim(rtrim(isNull(d.countryID,'')))*/
end

-- NJH 2012/04/24 if the organisationID has been updated, then update the organisationID on all people at the location
if update(organisationID)
begin
update person
	set organisationID = i.organisationID,
		lastUpdated = getDate(),
		lastUpdatedBy = i.lastUpdatedBy
from
	person p 
		inner join inserted i on i.locationID = p.locationId and p.organisationID <> i.organisationID
			
	-- delete the HQ flag if the location that moved organisation is the org HQ.		
	delete integerFlagData
	from
		deleted d 
			inner join integerFlagData ifd on ifd.entityID = d.organisationID and ifd.data = d.locationID
			inner join flag f on f.flagID = ifd.flagID and f.flagTextID='HQ'
			inner join inserted i on i.locationID = d.locationID
		where d.organisationID != i.organisationID
end

/* NJH 2014/04/17 - update the new matchname fields - can eventually get rid of the query above setting the matchname to null */
if update(address1) or update(address2) or update(address3) or update(address4)
Begin
	update location set matchAddress=null
	from location
		inner join inserted i on i.locationID = location.locationID
		inner join deleted d on d.locationID = location.locationID
	where
		ltrim(rtrim(isNull(i.address1,''))) <> ltrim(rtrim(isNull(d.address1,''))) or 
		ltrim(rtrim(isNull(i.address2,''))) <> ltrim(rtrim(isNull(d.address2,''))) or 
		ltrim(rtrim(isNull(i.address3,''))) <> ltrim(rtrim(isNull(d.address3,''))) or
		ltrim(rtrim(isNull(i.address4,''))) <> ltrim(rtrim(isNull(d.address4,'')))
	and location.matchAddress is not null
End
	
if update(sitename)
Begin
	update location set matchName=null
	from location
		inner join inserted i on i.locationID = location.locationID
		inner join deleted d on d.locationID = location.locationID
	where ltrim(rtrim(isNull(i.sitename,''))) <> ltrim(rtrim(isNull(d.sitename,'')))
		and location.matchName is not null
End

-- if any HQ locations have had their account type changed, then update the organisations account type to reflect the HQ's account type
if update(accountTypeID)
Begin
	update organisation
		set organisationTypeID = l.accountTypeID,
			lastUpdated = getDate(),
			lastUpdatedBy = l.lastUpdatedBy,
			lastUpdatedByPerson = l.lastUpdatedByPerson
	from 
		inserted i
			inner join location l on l.locationID = i.locationID
			inner join organisation o on o.organisationID = l.organisationID
			inner join integerFlagData orgHQ on orgHQ.entityID = o.organisationID and orgHQ.data = l.locationID
			inner join flag f on f.flagID = orgHQ.flagID and f.flagTextID='HQ'
	where
		o.organisationTypeID != l.accountTypeID
		and l.accountTypeID is not null
End
GO
