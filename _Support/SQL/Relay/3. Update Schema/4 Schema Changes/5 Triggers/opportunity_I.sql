/****** Object:  Trigger [opportunity_I]    Script Date: 05/14/2014 10:47:32 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[opportunity_I]'))
DROP TRIGGER [dbo].[opportunity_I]
GO


/****** Object:  Trigger [dbo].[opportunity_I]    Script Date: 05/14/2014 10:47:32 ******/
/*

CREATE TRIGGER [dbo].[opportunity_I] ON [dbo].[opportunity] 
FOR INSERT
AS

select opportunityID,  opportunityID as insertedID from inserted

*/
