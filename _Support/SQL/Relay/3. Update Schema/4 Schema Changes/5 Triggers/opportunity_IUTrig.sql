/****** Object:  Trigger [opportunity_I]    Script Date: 05/14/2014 10:31:13 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[opportunity_IUTrig]'))
DROP TRIGGER [dbo].[opportunity_IUTrig]
GO


/****** Object:  Trigger [dbo].[opportunity_I]    Script Date: 05/14/2014 10:31:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[opportunity_IUTrig] ON [dbo].[opportunity] 
FOR INSERT,UPDATE
AS

/*
NJH 2015-06-04	Added some distincts so that these queries would work when doing bulk updates 
NJH	2015/06/16	Delete any flags in the distiLocFlag radio group before inserting a new one.
*/


SET NOCOUNT ON
-- set the account manager flag for any people that have been assigned as account managers
if update(vendorAccountManagerPersonID)
BEGIN
	/*if a user is allocated as the vendorAccountManagerPersonID - opp field -  they will be flagged with [person boolean flag] 'isAccountManager'  */
	declare @perFlagID int = 0

	select @perFlagID=flagid from flag where flagtextid= 'isAccountManager';
	
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select distinct i.vendorAccountManagerPersonID,@perFlagID,i.lastupdatedBy,i.lastupdated,i.lastupdatedBy,i.lastupdated 
	from inserted i
		left join booleanflagdata bfd on bfd.flagid=@perFlagID and bfd.entityid=i.vendorAccountManagerPersonID
	where
		bfd.flagId is null
		and i.vendorAccountManagerPersonID is not null
END

-- set the sales manager flag for any people that have been assigned as vendor sales managers
if update(vendorSalesManagerPersonID)
BEGIN
	/*if a user is allocated as the vendorSalesManagerPersonID - opp field -  they will be flagged with [person boolean flag] 'isSalesManager'  */
	declare @perSalesManagerFlagID int = 0

	select @perSalesManagerFlagID=flagid from flag where flagtextid= 'isSalesManager';
	
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select distinct i.vendorSalesManagerPersonID,@perSalesManagerFlagID,i.lastupdatedBy,i.lastupdated,i.lastupdatedBy,i.lastupdated 
	from inserted i
		left join booleanflagdata bfd on bfd.flagid=@perFlagID and bfd.entityid=i.vendorSalesManagerPersonID
	where
		bfd.flagId is null
		and i.vendorSalesManagerPersonID is not null
		and isNull(@perSalesManagerFlagID,0) != 0
END



/*if a location is allocated as the partnerLocationID - opp field -  their organisation will be flagged with [org boolean flag] 'OppPartner'  */
if update(partnerLocationID)
BEGIN
	declare @partnerFlagID int = 0

	select @partnerFlagID=flagid from flag where flagTextID= 'OppPartner';
	
	insert into booleanFlagData (entityID,flagID,createdBy,created,lastUpdatedBy,lastUpdated) 
	select distinct organisationID,@partnerFlagID,i.lastupdatedBy,i.lastupdated,i.lastupdatedBy,i.lastupdated 
	from 
		inserted i 
		inner join location l on i.partnerLocationID = l.locationID
		left join booleanFlagData bfd on bfd.entityid = l.organisationID and bfd.flagID=@partnerFlagID
	where bfd.entityID is null
END



/* NJH 2014/06/24 Task Core-195 if a location is allocated as the distiLocationID - opp field -  their location will be flagged with what has been defined in the settings*/
if (update(distiLocationID) and dbo.getSettingValue('leadManager.distiLocFlagList') != '')
BEGIN
	declare @distiFlagID int = 0

	select @distiFlagID=flagid from flag where flagTextID= dbo.getSettingValue('leadManager.distiLocFlagList')
	
	delete booleanFlagData
	from
		inserted i
		inner join location l on i.distiLocationID = l.locationID
		inner join booleanFlagdata bfd on bfd.entityID = l.locationID
		inner join flag f on f.flagID = @distiFlagID
		inner join vFlagDef v on v.flagID = bfd.flagID and v.flagGroupID = f.flagGroupID
	where bfd.flagId != @distiFlagID

	insert into booleanFlagData (entityID,flagID,createdBy,created,lastUpdatedBy,lastUpdated) 
	select distinct locationID,@distiFlagID,i.lastupdatedBy,i.lastupdated,i.lastupdatedBy,i.lastupdated 
	from 
		inserted i 
		inner join location l on i.distiLocationID = l.locationID
		left join booleanFlagData bfd on bfd.entityid = l.locationID and bfd.flagID=@distiFlagID
	where bfd.entityID is null
END

/* NJH 2015/01/20 set the currency for an opportunity if the country has been set. Base it off the country table */
if update(countryID)
BEGIN
	update opportunity set currency=dbo.listGetAt(countryCurrency,1,',')
	from inserted i
		inner join opportunity opp on i.opportunityID = opp.opportunityID
		inner join country c on c.countryID = i.countryID
		inner join currency cur on cur.currencyISOCode = dbo.listGetAt(countryCurrency,1,',')
	where
		i.currency is null and i.countryID is not null
END

/* NJH 2015/02/05 if the partnerSalesPerson or the end customer contact has changed. This was previously in code, (in relayEntity).
	But any opportunities coming in from the connector were not going through this code. So, set it here as a trigger.  */
if update(partnerSalesPersonID) or update(contactPersonID)
BEGIN
	declare @flagID int
	
	select @flagID = flagId from vFlagDef where flagTextID='EndCustomerOppContacts'
	
	insert into IntegerMultipleFlagData (flagId,entityID,data,created,lastupdated,createdBy,lastUpdatedBy)
	select distinct @flagId,ec.personID,pc.personID,getDate(),getDate(),i.lastUpdatedBy,i.lastUpdatedBy
	from inserted i
		inner join opportunity opp on i.opportunityID = opp.opportunityID
		inner join person ec on ec.personID = i.contactPersonID
		inner join person pc on pc.personID = i.partnerSalesPersonID
		left join IntegerMultipleFlagData imfd on imfd.entityID = ec.personID and imfd.data=pc.personID and imfd.flagID=@flagID
	where
		imfd.rowID is null
END

GO