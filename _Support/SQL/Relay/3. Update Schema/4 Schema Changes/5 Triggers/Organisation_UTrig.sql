/****** Object:  Trigger [Organisation_UTrig]    Script Date: 09/12/2013 16:11:18 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Organisation_UTrig]'))
DROP TRIGGER [dbo].[Organisation_UTrig]
GO

/****** Object:  Trigger [dbo].[Organisation_UTrig]    Script Date: 09/12/2013 16:11:18 ******/
CREATE   TRIGGER [dbo].[Organisation_UTrig] ON [dbo].[organisation] FOR UPDATE AS

/*
 * VALIDATION RULE FOR FIELD 'OrganisationID'
 */
IF (SELECT Count(*) FROM inserted WHERE NOT (OrganisationID>0)) > 0
BEGIN
    RAISERROR(778253, 16, 1)
    ROLLBACK TRANSACTION
END

declare @replaceString nvarchar(4000)
declare @sql nvarchar(4000)
declare @status int
set @replaceString = 'not set yet'

/* NJH 2013/09/12 RW2013Roadmap 2 / Case 438724 - update matchname where core fields have changed */
update organisation set matchname=null
from organisation
	inner join inserted i on i.organisationID = organisation.organisationID
	inner join deleted d on d.organisationID = organisation.organisationID
	inner join country c on c.countryID = organisation.countryID
where case when c.vatRequired = 1 then ltrim(rtrim(isNull(d.vatNumber,''))) else ltrim(rtrim(isNull(i.vatNumber,''))) end <> ltrim(rtrim(isNull(i.vatNumber,'')))
	or ltrim(rtrim(i.organisationName)) <> ltrim(rtrim(d.organisationName))
	or ltrim(rtrim(i.orgURL)) <> ltrim(rtrim(d.orgURL))
	or ltrim(rtrim(i.countryID)) <> ltrim(rtrim(d.countryID))
GO