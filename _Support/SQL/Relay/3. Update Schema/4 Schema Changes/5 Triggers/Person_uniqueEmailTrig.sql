if exists (select 1 from sysobjects where name = 'Person_uniqueEmailTrig' and type='TR')
drop trigger Person_uniqueEmailTrig
GO

CREATE TRIGGER [dbo].[Person_uniqueEmailTrig] ON [dbo].[Person] FOR INSERT,UPDATE AS

set nocount on
declare @uniqueEmail_Error varchar(max)

/* NJH 2015/06/19 if account type is null, then look at the organisation type */
if update(email) and isNull(dbo.getSettingValue('plo.uniqueEmailValidation'),1) = 1
begin
	declare @personEmails as personEmailTableType

	insert into @personEmails (personID, email,accountTypeID)
	select distinct i.personID,i.email,isNull(l.accountTypeID,o.organisationTypeID)
		from inserted i 
			inner join location l on i.locationID = l.locationID
			inner join organisation o on l.organisationID = o.organisationId

	if (dbo.isEmailUnique(@personEmails) != 1)
	begin
		--select * from dbo.getNonUniqueEmails(@personEmails)
		set @uniqueEmail_Error = 'ERROR: email is not unique';

		RAISERROR (@uniqueEmail_Error,16,1)
		ROLLBACK TRANSACTION
	end
end