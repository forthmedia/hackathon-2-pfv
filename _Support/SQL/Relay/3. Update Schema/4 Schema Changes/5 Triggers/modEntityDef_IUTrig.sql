IF EXISTS (select 1 from sysobjects where name = 'modEntityDef_IUTrig' and XTYPE = 'TR')
	BEGIN
		DROP TRIGGER modEntityDef_IUTrig
	END	

GO

CREATE TRIGGER modEntityDef_IUTrig
   ON  ModEntityDef
   FOR INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

/*
WAB 2011/03/15

Make sure that modentityIDs are unique


*/

IF Exists (
	select * from 
		inserted i
			inner join 
		modentityDef  med on i.modentityID = med.modentityID and i.id <> med.id
	where 
		i.modentityID is not null )
		
		
BEGIN

	RAISERROR('ModEntityID must be unique', 16, 1)
            ROLLBACK TRANSACTION

END


END
GO




