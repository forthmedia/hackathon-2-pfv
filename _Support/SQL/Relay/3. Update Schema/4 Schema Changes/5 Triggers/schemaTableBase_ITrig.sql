

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[SchemaTableBase_ITrig]'))
DROP TRIGGER [dbo].[SchemaTableBase_ITrig]
GO

/*
CREATE TRIGGER [dbo].[SchemaTableBase_ITrig] ON [dbo].[SchemaTableBase] FOR INSERT AS

exec generate_vEntityName
*/