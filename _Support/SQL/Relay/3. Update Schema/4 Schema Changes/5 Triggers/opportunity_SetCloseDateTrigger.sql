if exists (select 1 from sysobjects where name = 'opportunity_SetCloseDateTrigger')
drop trigger opportunity_SetCloseDateTrigger

GO
-- =============================================
-- Author:		Nathaniel
-- Create date: 2012/08/14
-- Description:	Update the actual close date for an opportunity
-- =============================================
CREATE TRIGGER [opportunity_SetCloseDateTrigger] ON [dbo].[opportunity]
FOR INSERT,UPDATE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update opportunity set actualCloseDate=GetDate()
	from 
	opportunity o inner join
		(select i.opportunityID from deleted d
			inner join inserted i on i.opportunityID = d.opportunityID and d.stageID != i.stageID
			inner join oppStage osi on i.stageID = osi.opportunityStageID and osi.isWon = 1) won
		on o.opportunityID = won.opportunityID

END
GO