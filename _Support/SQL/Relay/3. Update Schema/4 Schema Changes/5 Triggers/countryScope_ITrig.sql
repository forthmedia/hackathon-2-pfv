if exists (select * from sysobjects where name = 'countryscope_ITrig')
BEGIN
drop trigger  [dbo].[countryscope_ITrig]
END

GO


CREATE  TRIGGER countryscope_ITrig ON dbo.countryScope 
FOR INSERT
AS

/*
We have added entityTypeID to this table to speed up indexing
However in order to accept old code I have out a trigger to keep it up to date
*/
update countryScope
set entityTypeID = s.entityTypeID
from
countryScope cs
inner join 
inserted as i
	on cs.countryscopeid = i.countryscopeid
	and isnull(i.entityTypeID,0) =0
inner join
schemaTable s
on s.entityName = cs.entity

GO