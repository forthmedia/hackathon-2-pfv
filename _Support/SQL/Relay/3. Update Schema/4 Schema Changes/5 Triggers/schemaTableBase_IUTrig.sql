

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[SchemaTableBase_IUTrig]'))
DROP TRIGGER [dbo].[SchemaTableBase_IUTrig]
GO

/*
WAB 2017-01-13	Add support for synchEntityName taking @refresh.  
So will do a complete refresh of entityName table if either in_vEntityName or nameExpression is changed 
*/

CREATE TRIGGER [dbo].[SchemaTableBase_IUTrig] ON [dbo].[SchemaTableBase] FOR INSERT,UPDATE AS

IF update (in_vEntityName) OR update(nameExpression)
BEGIN
	
	declare @tablename sysname
	declare @entityNameRefresh bit = 0
	
	declare entity_cursor cursor for
		select 
    		distinct sTable.table_name 
		from inserted i
			inner join information_schema.tables sTable on i.entityName = sTable.table_name
			inner join information_schema.tables deltable on deltable.table_name = i.entityName+ 'del'
	    where 
			i.in_vEntityName = 1
			
	open entity_cursor
	fetch next from entity_cursor into @tablename

		while @@FETCH_STATUS = 0
		begin

			exec synchEntityName @tablename=@tablename, @refresh = 1
			fetch next from entity_cursor into @tablename
		end
	close entity_cursor;
	deallocate entity_cursor;
END	