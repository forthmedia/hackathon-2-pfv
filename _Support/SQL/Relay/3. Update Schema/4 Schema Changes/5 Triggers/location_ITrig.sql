/*
 * Mods 
 *
 * WAB 04.10.05   added select statement to bring back inserted records
 */

/*
ALTER      TRIGGER [dbo].[Location_ITrig] ON [dbo].[Location] FOR INSERT AS

declare @countryid int
/*
if update(countryid)
begin
	declare cc insensitive cursor for
		select distinct countryid from inserted
	open cc 

	fetch next from cc into @countryid
	while @@fetch_status = 0
	begin
		if not exists(select * from  organisation o 
					inner join  countrygroup cg on cg.countrygroupid = o.countryid  
					inner join inserted i on i.countryid = cg.countrymemberid
								and i.organisationid = o.organisationid
				where i.countryid=@countryid)
		BEGIN
			RAISERROR('The country cannot be inserted outside the country or region of the organisation', 16, 1)
			ROLLBACK TRANSACTION
		END
		fetch next from cc into @countryid
	end
deallocate cc
end
*/
-- VALIDATION RULE FOR FIELD 'LocationID'

IF EXISTS(SELECT * FROM inserted WHERE NOT (LocationID>0)) 
    BEGIN
        RAISERROR(778254, 16, 1)
        ROLLBACK TRANSACTION
    END

/*
 * VALIDATION RULE FOR FIELD 'FaxStatus'
 */
IF EXISTS(SELECT * FROM inserted WHERE NOT (FaxStatus>=-1))
    BEGIN
        RAISERROR(778255, 16, 1)
        ROLLBACK TRANSACTION
    END


-- Added WAB 04/10/05
-- since locationid is not an identity column we can't use @identity_inserted so instead return the inserted ids
SELECT LOCATIONID from INSERTED
*/

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Location_ITrig]'))
DROP TRIGGER [dbo].[Location_ITrig]
GO
