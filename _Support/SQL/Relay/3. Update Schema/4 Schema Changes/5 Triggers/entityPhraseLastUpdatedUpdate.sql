/*
NJH 2013/03/19
Create Trigger for keeping a the lastUpdated column in the base table up to date. Only fires the update
	if the entity table has the lastUpdated,lastUpdatedBy and lastUpdatedByPerson columns
*/

IF exists (select 1 from sysobjects where name = 'entityPhraseLastUpdatedUpdate')
	DROP TRIGGER entityPhraseLastUpdatedUpdate
GO

create trigger [dbo].[entityPhraseLastUpdatedUpdate] 
on [dbo].[Phrases]
for update, insert

AS

if exists (
	select 1 from 
		inserted i 
			inner join 
		phrases p  on p.ident = i.ident
			inner join	
		phraseList pl on p.phraseid = pl.phraseid and pl.entityTypeID <> 0

)


BEGIN

	/* Get distinct values of entityTypeID and phraseTextID */
	
	select phraseid,ident,phraseText,lastUpdatedBy,lastUpdated into #inserted from inserted 	/* can't refer to inserted in exec sp_execuresql, just need the phraseid and can't use the phrasetext anyway  */
	select phraseid,ident,phraseText,lastUpdatedBy,lastUpdated into #deleted from deleted

	DECLARE @tablename nvarchar(200), @uniqueKey nvarchar(200), @entityTypeID int
	DECLARE @SQL nvarchar (4000)

	DECLARE n_cursor CURSOR FOR
		select distinct sc.table_name, uniqueKey, s.entityTypeID
			from 
				inserted i 
					inner join	
				phraseList pl on i.phraseid = pl.phraseid and pl.entityTypeID <> 0
					inner join 
				schematable s on s.entitytypeid = pl.entitytypeid
					inner join 
				information_schema.columns sc on sc.table_name = s.entityname and sc.column_name in ('lastUpdated','lastUpdatedBy','lastUpdatedByPerson')
			where isnull(uniqueKey,'') <> '' 	
			group by sc.table_name, uniqueKey, s.entityTypeID
			having count(sc.column_name) = 3
	OPEN n_cursor
	
	FETCH NEXT FROM n_cursor
		INTO @tablename ,@uniqueKey, @entityTypeID

		WHILE @@FETCH_STATUS = 0
			BEGIN
				select @sql = '
					update ' + @tablename + ' set lastUpdated=i.lastUpdated,lastUpdatedBy=isNull(u.userGroupID,0),lastUpdatedByPerson = i.lastUpdatedBy 
					from ' + @tablename + ' ent
					inner join vphrases p	
						on ent.' + @uniqueKey + ' = p.entityid  
							and p.entityTypeID  = ' + convert(varchar,@entityTypeID) + '
					inner join #inserted	i
						on i.ident = p.ident
					left join #deleted	d
						on d.ident = p.ident
					left outer join usergroup u
						on u.personID = i.lastUpdatedBy
					where isNull(d.phraseText,'''') != i.phraseText'						

				exec sp_executesql @sql

				FETCH NEXT FROM n_cursor INTO @tablename ,@uniqueKey, @entityTypeID

			END

	CLOSE n_cursor
	DEALLOCATE n_cursor
	
END
