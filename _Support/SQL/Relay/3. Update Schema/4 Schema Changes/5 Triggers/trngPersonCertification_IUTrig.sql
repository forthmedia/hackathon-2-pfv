if exists (select * from sysobjects where name = 'trngPersonCertification_IUTrig')
BEGIN
drop trigger  [dbo].[trngPersonCertification_IUTrig]
END
GO

/****** Object:  Trigger [dbo].[trngPersonCertification_IUTrig]    Script Date: 09/30/2013 13:23:09 ******/
CREATE TRIGGER [dbo].[trngPersonCertification_IUTrig] ON [dbo].[trngPersonCertification] FOR INSERT,Update AS

declare @effectedOrg int;

select @effectedOrg = p.organisationID
from inserted as tpc
inner join person p on tpc.personID = p.personID
;

/*** update the certification count for organisations where the certification count has changed ****/
update entitySpecialisationRuleStatus set numOfCertifications = base.numOfCertificationsByOrg,lastUpdatedBy=4,lastUpdated=GetDate()
from entitySpecialisationRuleStatus esrs inner join (
select count(tpc.certificationID) as numOfCertificationsByOrg,p.organisationID,sr.specialisationRuleID from
trngPersonCertification tpc
	inner join person p on tpc.personID = p.personID and p.OrganisationID = @effectedOrg
	inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
	inner join specialisationRule sr on tpc.certificationID = sr.certificationID
	inner join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
group by organisationID,tpc.certificationID,sr.specialisationRuleID)
base on esrs.entityID = base.organisationID and esrs.specialisationRuleID = base.specialisationRuleID and entityTypeID=2 and esrs.numOfCertifications <> base.numOfCertificationsByOrg


/**** insert the certification count for organisations that don't yet have records in the entitySpecialistionRuleStatus table ****/
insert into entitySpecialisationRuleStatus (entityID,entityTypeID,numOfCertifications,specialisationRuleID,created,createdBy,lastUpdated,lastUpdatedBy)
select p.organisationID,2,count(tpc.certificationID),sr.specialisationRuleID,GetDate(),4,GetDate(),4
from trngPersonCertification tpc
	inner join person p on tpc.personID = p.personID and p.OrganisationID = @effectedOrg
	inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID and tpcs.statusTextID in ('Passed','Pending')
	inner join specialisationRule sr on tpc.certificationID = sr.certificationID
	left join entitySpecialisationRuleStatus esrs on esrs.specialisationRuleID = sr.specialisationRuleID and entityID=p.organisationID and entityTypeID=2
where esrs.entityID is null
group by organisationID,tpc.certificationID,sr.specialisationRuleID

/* NJH 2013/09/30 - moved to a trigger, as it would appear that certifications can get passed in multiple ways */
if (update(statusID))
begin

	delete from trngPersonCertificationRuleModule
	--select *
		from trngPersonCertificationRuleModule tpcrm with (NOLOCK)
			inner join trngPersonCertificationRule tpcr on tpcrm.personCertificationRuleID = tpcr.personCertificationRuleID
			inner join inserted tpc on tpc.personCertificationID = tpcr.personCertificationID
			inner join trngCertificationRule tcr with (NOLOCK) on tcr.certificationID = tpc.certificationID
			inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID
			left join deleted d on d.personCertificationID = tpc.personCertificationID
		where tcr.active = 1
			and tpcs.statusTextID  = 'Passed'
			and d.statusID <> tpc.statusID

	delete from trngPersonCertificationRule
	--select *
		from trngPersonCertificationRule tpcr
			inner join inserted tpc on tpc.personCertificationID = tpcr.personCertificationID
			inner join trngCertificationRule tcr with (NOLOCK) on tcr.certificationID = tpc.certificationID
			inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID
			left join deleted d on d.personCertificationID = tpc.personCertificationID
		where tcr.active = 1
			and tpcs.statusTextID  = 'Passed'
			and d.statusID <> tpc.statusID

	insert into trngPersonCertificationRule (personCertificationID,certificationRuleID,numModulesToPass,moduleSetID,active,studyPeriod,certificationRuleTypeID,activationDate,certificationRuleOrder,createdBy,lastUpdatedBy,lastUpdatedByPerson)
	select distinct tpc.personCertificationID,tcr.certificationRuleID,tcr.numModulesToPass,tcr.moduleSetID,tcr.active,tcr.studyPeriod,tcr.certificationRuleTypeID,tcr.activationDate,tcr.certificationRuleOrder,tpc.createdBy,tpc.lastUpdatedBy,tpc.lastUpdatedByPerson 
		from trngCertificationRule tcr with (NOLOCK)
			inner join inserted tpc on tcr.certificationID = tpc.certificationID
			inner join trngPersonCertificationStatus tpcs on tpcs.statusID = tpc.statusID
			left join deleted d on d.personCertificationID = tpc.personCertificationID
			left join trngPersonCertificationRule tpcr with (NOLOCK) on tpcr.personCertificationID = tpc.personCertificationID and tpcr.certificationRuleID = tcr.certificationRuleID
	where tpcr.certificationRuleID is null
		and tcr.active = 1
		and tpcs.statusTextID  = 'Passed'
		and d.statusID <> tpc.statusID
		
		
	insert into trngPersonCertificationRuleModule (personCertificationRuleID,moduleID,createdBy,lastUpdatedBy,lastUpdatedByPerson)
	select distinct tpcr.personCertificationRuleID,tump.moduleID,tpc.createdBy,tpc.lastUpdatedBy,tpc.lastUpdatedByPerson
		from trngPersonCertificationRule tpcr with (NOLOCK)
			inner join inserted tpc on tpcr.personCertificationID = tpc.personCertificationID
			inner join trngCertificationRule tcr with (NOLOCK)on tpcr.certificationRuleID = tcr.certificationRuleID
			inner join trngModuleSetModule tmsm with (NOLOCK)on tcr.moduleSetID = tmsm.moduleSetID
			inner join trngUserModuleProgress tump with (NOLOCK)on tump.moduleId = tmsm.moduleID and tump.userModuleFulfilled is not null and tpc.personid = tump.personid
			left join trngPersonCertificationRuleModule tpcrm with (NOLOCK)on tpcrm.personCertificationRuleID = tpcr.personCertificationRuleID and tpcrm.moduleID = tump.moduleID
	where tpcrm.moduleID is null
end