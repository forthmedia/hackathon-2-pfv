/*   Update to the Flag Insert Trigger */



ALTER     TRIGGER [dbo].[Flag_ITrig] ON [dbo].[flag] FOR INSERT AS

/*


Mods:

1.1	2/4/01  WAB added code to make flagtextids unique and valid
1.2	27/4/01  WAB added code to return the inserted records
1.21	16/6/01	 WAB corrected bug caused by update and insert triggers both having cursors called myCursor
	note 22/6/01  WAB found that 'INSERT INTO SelectionField  ...	 ' had been commented out, this left a random select statement floating in the middle and caused problems
  WAB 2010/10/04 problem with selectionfields when inserting integer and integer multiple flags
WAB 2015-04-13  remove code which returns the whole inserted record
WAB 2016-05-10 PROD2016-920 Alter deprecated RAISERROR syntax which is not compatible with SQL 2012
*/


/* NJH 2016/12/12 moved to IU trigger as common to both
/*
 * PREVENT NULL VALUES IN 'FlagGroupID'
 */
IF (SELECT Count(*) FROM inserted WHERE FlagGroupID IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''FlagGroupID'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'Name'
 */
IF (SELECT Count(*) FROM inserted WHERE Name IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''Name'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'CreatedBy'
 */
IF (SELECT Count(*) FROM inserted WHERE CreatedBy IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''CreatedBy'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'Created'
 */
IF (SELECT Count(*) FROM inserted WHERE Created IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''Created'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'LastUpdatedBy'
 */
IF (SELECT Count(*) FROM inserted WHERE LastUpdatedBy IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''LastUpdatedBy'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END
ELSE 
/*
 * PREVENT NULL VALUES IN 'LastUpdated'
 */
IF (SELECT Count(*) FROM inserted WHERE LastUpdated IS NULL) > 0
    BEGIN
        RAISERROR ('Field ''LastUpdated'' cannot contain a null value.', 16 , 1)
        ROLLBACK TRANSACTION
    END



/*
*  Make sure that flagTextID is valid
Added by WAB 2/4/01
*/

	declare @FlagID int
	declare @FlagTextID varchar(100)
	declare @newFlagTextID varchar(100)


	Declare flagICursor Cursor
	For
	select 	i.flagTextID, i.flagid
	from 	inserted i 

	Open flagICursor

	Fetch Next From flagICursor
	INTO @FlagTextID, @flagid

	While @@Fetch_Status = 0
	BEGIN
		exec stripNonCFVariableCharacters @inString = @FlagTextID, @outString = @newFlagTextID output
		if @newFlagTextID <> @FlagTextID
			update flag set flagTextID = @newFlagTextID
			where flagID = @flagid

	Fetch Next From flagICursor
	INTO @FlagTextID,  @flagid
			
	END

	close flagICursor
	Deallocate flagICursor


/*
Make sure that flagtextids are unique (or blank)
Added by WAB 2/4/01
*/

IF  exists
	(select 
		f.flagtextID , count(1)
	from 
		flag f1
			inner join
		flag f 
			on f.flagtextid = f1.flagtextid
			and isNull(f.flagtextid,'') <> ''
			inner join 
		inserted i
			on f.flagid = i.flagid
	group by 
		f.flagtextID 
	having count(1) > 1) 

    BEGIN
        RAISERROR ('FlagTextIDs must be unique (or blank)',16,1)
        ROLLBACK TRANSACTION
    END

*/


/*
*	Create search stuff  - populate the selectionFieldTable
*/

Set noCount On

Declare @flagTypeID int, @flagGroupID int, @flagTypeName varchar(20)
declare @FlagID int

	 Select @flagID = flagID,  @flagGroupID = flagGroupID from Inserted

	 Select 	@flagTypeName=ft.Name, @flagTypeID=ft.flagTypeID
	From 	FlagGroup as fg, FlagType as ft
	where	fg.flagTypeid = ft.flagTypeid
	and	fg.flagGroupID = @flagGroupID


	IF ( @flagTypeID <> 2 and @flagTypeID <> 3 )
		BEGIN
			INSERT INTO SelectionField	(SelectionField, CriteriaMask)
			select 	'frmFlag_' + @flagTypeName
					+ '_' + convert(varchar(6),@flagID)			
					+ CASE WHEN charindex('_', selectionField) <>  0 THEN '_'+substring(selectionfield, charindex('_', selectionField)+1, 100) ELSE '' END ,
					CriteriaMask
					from selectionfield	
			WHERE SelectionField =  'frmFlag' + @flagTypeName  /*  WAB 2010/10/04 remove the like and %.   Integer was pickingup integer and integermultiple and erroring if unique index set on selectionfield table.  as it happens this field is no longer used! */
		END
GO
