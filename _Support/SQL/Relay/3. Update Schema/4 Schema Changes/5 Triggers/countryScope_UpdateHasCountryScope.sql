if exists (select * from sysobjects where name = 'countryScope_UpdateHasCountryScope')
BEGIN
drop trigger  [dbo].[countryScope_UpdateHasCountryScope]
END

GO

CREATE TRIGGER [dbo].[countryScope_UpdateHasCountryScope] 
   ON  [dbo].[countryScope]
   for INSERT,DELETE,UPDATE
AS 

BEGIN
/*
WAB 2010/10/18 This procedure keeps the hasCountryScope field on the element table up to date
	we used to do it in Cold Fusion but it kept getting out of synch.
	The hasCountryScope bit is set if there is a countryscope at permission 1 
	Could be extended for permission 2^10 which deals with country exclusion , but the getElementTree code would need checked/modified

WAB 2013/09/09
	Extended to keep track of permissions at any level on any parent table
	Any field on the parent table with a name of the form
		hasCountryScope_n   (Where n is the numeric level)
		[for backwards compatibility we support hasCountryScope which is the equivalent of hasCountryScope_1]

WAB 2015-11-10
	Altered to deal with Level11 (an exclude from level1). If anything set at level11 then hasCountryScope_Level1 is set to 1
*/

	SET NOCOUNT ON;

	/* put both inserted and deleted records into a single temp table 
		saves replicating queries and can be passed into sp_execute sql
	*/	

	select * into #id
		FROM
		(
		select * from inserted 
			union
		select * from deleted
		) As X

	IF @@rowcount = 0
		GOTO THEEND

	declare @sql  nvarchar(4000)
	Declare @entityTypeID int ,@tableName sysname, @columnName sysname, @Level int, @uniqueKey sysname

	declare myCursor CURSOR FOR
		SELECT distinct s.entityTypeID, table_name, column_Name, case when column_name = 'hasCountryScope' then 1 else convert(int,right(column_name,1)) end as level, uniqueKey 
		FROM
			#id as 	 i
				inner join
			schemaTable s on isNull(i.entityTypeID,0) = s.entityTypeID OR i.entity = s.entityName 
				inner join
			information_schema.columns on s.entityname = table_name and column_name like 'hasCountryScope%'
		

		OPEN myCursor

		FETCH NEXT FROM myCursor
		INTO @entityTypeID,@tableName, @columnName, @Level, @uniqueKey

		WHILE @@FETCH_STATUS = 0
		BEGIN


			set @sql = 
			' update ' + @tablename +
			' set ' + @columnName + ' = case when cs.entityid is not null then 1 else 0 end 
			from ' +
			@tablename + ' AS e
				inner join 
				#id AS id on e. ' + @uniqueKey + ' = id.entityid and id.entity =  @tablename 
				left join
				CountryScope cs on id.entityid = cs.entityid 
									and id.entity = cs.entity 
									and (
										(cs.permission & power (2,@level -1) <> 0 ) 
											OR 
										(@level = 1 AND cs.permission & power (2,@level - 11) <> 0  ) /* special case for level 11 - which is defined as an exclude for level1 */
									)
			'	

			
			exec sp_executeSQL @sql, N'@tablename sysname,@level int', @tablename = @tablename , @level= @level

			FETCH NEXT FROM myCursor
			INTO @entityTypeID,@tableName, @columnName, @Level, @uniqueKey

		END
		
		CLOSE myCursor
		DEALLOCATE myCursor

THEEND:	

END

