ALTER TRIGGER [dbo].[ScreenDefinition_ITrig] ON [dbo].[ScreenDefinition] 
FOR Insert AS


/*
Author  WAB

Date ???


Purpose

When a row is added to this table, need to update the countryscope table


Mods:
WAB 13/6/01	At last got around to making this work with multiple row inserts - by using a cursor

WAB 2015-02-25 Removed calls to update the, now removed,  screen Index table 

*/


Declare  @newScreenid int , @newsortOrder int


declare sdInsertCursor cursor for
	select 	
		screenid, 
		sortorder
	from 
		inserted


open sdInsertCursor

fetch next from sdInsertCursor 
into  @newScreenid, @newSortOrder


WHILE @@FETCH_STATUS = 0
	BEGIN

		exec updateScreenDefinitionScope @newScreenid,  @newsortOrder 

		fetch next from sdInsertCursor 
		into @newScreenid, @newSortOrder


	END 	

close sdInsertCursor
deallocate sdInsertCursor

