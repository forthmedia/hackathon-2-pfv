

IF EXISTS (SELECT 1 FROM sysobjects where name = 'Location_HistorialLocationIDList')
DROP TRIGGER Location_HistorialLocationIDList

GO
/*
WAB 2010/11/1 Added script for this trigger since it is critical but was not being created 
*/
GO
CREATE TRIGGER [dbo].[Location_HistorialLocationIDList] 
   ON  [dbo].[Location]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON
	UPDATE location
	set HistoricalLocationIDList = locationID
	where locationID IN (select locationID from inserted)
    -- Insert statements for trigger here

END

GO
