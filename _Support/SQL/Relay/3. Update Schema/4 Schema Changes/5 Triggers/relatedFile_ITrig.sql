IF exists (select 1 from sysobjects where name = 'relatedFile_ITrig')
	DROP TRIGGER relatedFile_ITrig
GO

CREATE  TRIGGER [dbo].[relatedFile_ITrig] ON [dbo].[relatedFile] FOR INSERT AS
/* =============================================
 Author:		PPB
 Create date:	2015-02-11
 Description:	P-KAS049 Initial version is to set a fundRequestActivity to have Status 'DocumentationUploaded' if it is currently 'Approved' when any MDF file is uploaded against it

 Modifications:
2015-03-02 PPB P-KAS049 changed to trigger when status='ApprovedPendingDocumentation' instead of 'Approved'

 ============================================= */
BEGIN
	SET NOCOUNT ON		/* added to prevent extra result sets from interfering with SELECT statements */

	DECLARE @statusId_DocumentationUploaded int, @levelAvailabilty_DocumentationUploaded int
	
	/* is the new relatedFile an MDF file ? */
	IF EXISTS (	SELECT i.FileId 
				FROM inserted i 
				INNER JOIN relatedFileCategory rfc ON rfc.FileCategoryId = i.FileCategoryId AND rfc.FileCategoryEntity = 'fundRequestActivity') 
	BEGIN
		/* is the related fundRequestActivity at Status=ApprovedPendingDocumentation ? */
		IF EXISTS (	SELECT i.FileId 
					FROM inserted i 
					INNER JOIN fundRequestActivity fra ON fra.fundRequestActivityId = i.entityId
					INNER JOIN fundApprovalStatus fas ON fas.fundApprovalStatusId = fra.fundApprovalStatusId AND fas.fundStatusTextId = 'ApprovedPendingDocumentation') 
		BEGIN
			/* check we have a status called 'DocumentationUploaded' and use it; we don't want to set the activity status to NULL if it didn't exist */
			SELECT @statusId_DocumentationUploaded=fundApprovalStatusId, @levelAvailabilty_DocumentationUploaded=CAST(levelAvailabilty AS int) FROM fundApprovalStatus WHERE fundStatusTextID='DocumentationUploaded'	

			IF @statusId_DocumentationUploaded IS NOT NULL
			BEGIN
				UPDATE fundRequestActivity 
				SET fundApprovalStatusID = @statusId_DocumentationUploaded
					,currentApprovalLevel =  @levelAvailabilty_DocumentationUploaded
					,lastUpdated = GETDATE()
					,lastUpdatedBy = 4
				FROM inserted i 
				INNER JOIN fundRequestActivity fra ON fra.fundRequestActivityID=i.entityId
			END
		END
	END

END