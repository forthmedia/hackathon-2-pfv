ALTER  TRIGGER [dbo].[trngUserModuleProgress_IUTrig] ON [dbo].[trngUserModuleProgress] FOR INSERT,Update 
AS
/* 
 Modifications:
	NJH P-SNY047 added else statement
	NYB:  Altered as part of Sophos Stargate 2 1/1/09 (est) 
	NYB 2009-07-31 Sony Bug (not in LH) - userModuleFulfilled not updating
	NYB 2011-07-31 Sony Bug (not in LH) - userModuleFulfilled not updating
	NYB 2011-10-14 LHID7420 - userModuleFulfilled not updating

*/
update trngUserModuleProgress 
	set userModuleFulfilled = 
	CASE WHEN i.moduleStatus in (select statusID from trngUserModuleStatus where statusTextID='Passed') 
			AND i.userModuleFulfilled IS NULL 
	THEN
		GetDate()
	ELSE
		i.userModuleFulfilled
	END
from trngUserModuleProgress tump 
inner join inserted i on tump.userModuleProgressID = i.userModuleProgressID

