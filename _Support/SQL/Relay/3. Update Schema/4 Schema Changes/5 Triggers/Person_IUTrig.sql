/* NJH 2014/11/03 - combine the insert and update triggers into a single trigger as they pretty much did the same thing.
	This prevents the update trigger firing if an update was made in the insert trigger
*/

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Person_IUTrig]'))
DROP TRIGGER [dbo].[Person_IUTrig]
GO

CREATE TRIGGER [dbo].[Person_IUTrig] ON [dbo].[Person] FOR INSERT,UPDATE AS

/*
 * VALIDATION RULE FOR FIELD 'PersonID'
 */
IF (SELECT Count(*) FROM inserted WHERE NOT (PersonID>0)) > 0
    BEGIN
        RAISERROR(778256, 16, 1)
        ROLLBACK TRANSACTION
    END


--if an update has been done
if exists (select 1 from deleted)
begin
	--Reset EmailStatus:
	Update  p
		 Set EmailStatus=0 
	From Person p
		inner join Inserted I On I.PersonID=P.PersonID
		inner join Deleted D On D.PersonID=P.PersonID
	 Where ISNULL(I.Email, '')<>ISNULL(D.Email, '')
	 
	/* NJH 2013/09/12 RW2013Roadmap 2 - update matchname where core fields have changed */
	update p set matchname=null
	from person p
		inner join inserted i on i.personID = p.personID
		inner join deleted d on d.personID = p.personID
	where ltrim(rtrim(isNull(i.firstname,''))) <> ltrim(rtrim(isNull(d.firstname,''))) or 
		ltrim(rtrim(isNull(i.lastname,''))) <> ltrim(rtrim(isNull(d.lastname,''))) or 
		ltrim(rtrim(isNull(i.email,''))) <> ltrim(rtrim(isNull(d.email,'')))
end
/*
 NJH 2012/04/24  - if we update the location field, the update the person's organisation field if it needs updating
 WAB 2012-11-26 CASE 432014, came to test this code and discovered that it wasn't working correctly
						also removed the IF update(organisationid) because this ended up performing a hidden rollback - better to throw an error if someone is trying to update organisationid without updating locationid
*/  
if not exists (select 1 from deleted) or UPDATE(locationID) or UPDATE(organisationID)
begin
	update person
		set organisationID = l.organisationID
	from person p
			inner join 
		inserted i on i.personID = p.personId
			inner join 
		location l on i.locationID = l.locationID  and  l.organisationID <> isNull(i.organisationID,0)
end

/*
 NJH 2012/04/24 - if the organisationID and locationId aren't synched, then throw an error
WAB 2012-11-26 CASE 432014 moved this code, was previously before the code to do an automatic synch
*/ 
/*
if exists (
	select 1 
	from 
		inserted i
			inner join 
		person p on i.personID = p.personId
			inner join 
		location l on l.locationID = p.locationID and l.organisationID <> p.organisationID)
begin
	raiserror('Persons location and organisation ids are not in synch',16,1)
	rollback transaction
end
*/

