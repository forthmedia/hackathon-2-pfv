ALTER    TRIGGER [dbo].[ScreenDefinition_UTrig] ON [dbo].[ScreenDefinition] 
FOR UPDATE AS


/*
Author  WAB

Date 2000


Purpose

When a row is updated, may need to update the countryscope table


Mods:
WAB 13/6/01	At last got around to making this work with multiple row inserts - by using a cursor

WAB 2015-02-25 Altered so that update is always done is countryID is updated (regardless of whether changed or not)
				This allows us to refresh the countryScope easily by doing an Update screenDefinition set countryID = countryID	
				This helps during the build process and can help when new countries are added.
				Have removed need for the screen Index table, so remove calls to procedures to update it

*/


Declare @newScreenid int, @oldScreenid int, @newsortOrder int, @oldsortOrder int

declare sdUpdateCursor cursor for
select 	i.screenid, 
	i.sortorder, 
	d.screenid, 
	d.sortorder  
from 	inserted i 
		inner join 
	deleted d on i.itemid = d.itemid
where
	update (countryid)
	or i.screenid <> d.screenid
	or i.sortorder <> d.sortorder
	or i.active <> d.active

-- need to update the scope in the following instances
-- countryid changed
-- sort order changed
-- active changed
-- screenid changed (unlikely but could)


open sdUpdateCursor
fetch next from sdUpdateCursor
into @newScreenid, @newSortOrder , @oldScreenid  , @oldSortOrder 

WHILE @@FETCH_STATUS = 0
	BEGIN

		-- update countryScope 
		exec updateScreenDefinitionScope @newScreenid,  @newsortOrder 
		
		-- if screenid or sortorder has changed (ie not just countryid and active) then need to update old ones as well
		IF (@oldScreenid <> @newScreenid) or (@oldsortOrder <> @newsortOrder)
			BEGIN
				exec updateScreenDefinitionScope @oldScreenid,  @oldsortOrder 
			END


		fetch next from sdUpdateCursor
		into @newScreenid, @newSortOrder ,  @oldScreenid  , @oldSortOrder  

		
	END


close sdUpdateCursor
deallocate sdUpdateCursor

