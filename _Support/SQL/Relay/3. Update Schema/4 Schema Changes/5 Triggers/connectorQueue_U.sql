/****** Object:  Trigger [dbo].[ConnectorQueue_IUTrig]    Script Date: 09/16/2014 14:35:58 ******/
IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ConnectorQueue_UTrig]'))
DROP TRIGGER [dbo].[ConnectorQueue_UTrig]
GO

CREATE TRIGGER [dbo].[ConnectorQueue_UTrig] ON [dbo].[connectorQueue] FOR UPDATE AS

/* 
	TESTING: 		see _support\tests\SQL\connector\test_ConnectorQueue_UTrig.sql

	NJH				Only run below if synch attempt has been updated
	
	WAB 2015-10-12  CASE 446216 Queries seemed to be causing some blocking issues
					Rewrote to use cursor which only processes one object type at a time, which got rid of a rather nasty union involving vEntityName
					In reality we should normally only be updating a single object type at a time, so only one loop of the cursor
					Altered to use setRadioFlag/deleteBooleanFlag sps rather than manually crafted queries
					Added trigger_nestlevel() <2  protects against cascading due to fieldTrigger on salesforce synch flags.  Became apparent because cursor tried to run twice 	(actually this seemed to be a legacy problem but no harm having it)				
					*NOTE* requires updates to setRadioFlag/deleteBooleanFlag sps


*/

SET NOCOUNT ON

IF UPDATE(synchAttempt) and trigger_nestlevel() < 2  
BEGIN

	declare
		@entityTypeID int
		,@connectorObjectID int
		,@maxSynch int
		,@isrelayWare int
		,@flagGroupID int
		,@suspendedFlagID int
		,@rowcount int

	declare objectTypeCursor cursor  for
		select distinct 
			  s.entityTypeID
			, i.connectorObjectID  
			, isNull(dbo.getSettingValue('connector.maxSynchAttempts'),5)
			, o.relayware
			, fg.flagGroupID
			, f.flagid
		from
			inserted i
			inner join 	connectorObject o on o.ID = i.connectorObjectID
			inner join vConnectorMapping m on isRemoteID=1 and case when o.relayware=0 then m.object_remote else m.object_relayware end = o.object
			inner join schemaTable s on s.entityName = m.object_relayware 
			inner join flagGroup fg on fg.entityTypeID = s.entityTypeID and fg.flagGroupTextID like '%ConnectorSynchStatus'
			inner join flag f on f.flagGroupID = fg.flagGroupID and f.flagTextID like '%Suspended'

	OPEN objectTypeCursor

		FETCH NEXT FROM objectTypeCursor 
		INTO @entityTypeID , @connectorObjectID, @maxSynch , @isrelayWare, @flagGroupID, @suspendedFlagID 

			WHILE @@FETCH_STATUS = 0
			BEGIN

				declare @sysTaskPersonID int
				select @sysTaskPersonID = personID from person where firstname='System' and lastname='Task'

				-- delete the suspended flag for any records that have their synch attempt reset	
				select 
					case when @isRelayware = 0 then v.entityID else i.objectid end as entityid
				into #deleteFlag
				from inserted i
						left join
					vEntityName v on @isrelayWare = 0 and v.entityTypeID = @entityTypeID and  i.objectID = v.crmid
						left join 
					deleted d on d.ID = i.ID
				where
					i.connectorObjectID = @connectorObjectID
					and i.synchAttempt = 0
					and isNull(d.synchAttempt,0) != 0

				IF @@rowCount <> 0
				BEGIN
					exec deleteBooleanFlag @flagid = @suspendedFlagID, @entityID = null, @userid = 0, @updatedByPersonID = @sysTaskPersonID, @tablename = '#deleteFlag'
				END
				drop table #deleteFlag

				-- set the suspended flag for any records where we have reached the maximum attempts
				select 
					case when @isRelayware = 0 then v.entityID else i.objectid end as entityid
				into #setFlag
				from
					inserted i
						left join
					vEntityName v on @isrelayWare = 0 and v.entityTypeID = @entityTypeID and  i.objectID = v.crmid
						left join 
					deleted d on d.ID = i.ID
				where
					i.connectorObjectID = @connectorObjectID
					and case when @isRelayware = 0 then v.entityID else i.objectid end is not null
					and i.synchAttempt >= @maxSynch
					and isNull(d.synchAttempt,0) < @maxSynch

				IF @@rowCount <> 0
				BEGIN
					exec setRadioFlag @flagid = @suspendedFlagID, @entityID = null, @userid = 0, @updatedByPersonID = @sysTaskPersonID
				END

				drop table #setflag

			FETCH NEXT FROM objectTypeCursor 
			INTO @entityTypeID , @connectorObjectID, @maxSynch , @isrelayWare, @flagGroupID, @suspendedFlagID 

		END
		
		CLOSE objectTypeCursor
		DEALLOCATE objectTypeCursor

END