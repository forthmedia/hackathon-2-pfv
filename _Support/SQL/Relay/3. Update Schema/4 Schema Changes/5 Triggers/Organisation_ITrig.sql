/*
 * Mods 
 *
 * WAB 04.10.05   added select statement to bring back inserted records
 */


/*
ALTER  TRIGGER [dbo].[Organisation_ITrig] ON [dbo].[organisation] FOR INSERT AS
/*
 * VALIDATION RULE FOR FIELD 'OrganisationID'
 */
IF (SELECT Count(*) FROM inserted WHERE NOT (OrganisationID>0)) > 0
    BEGIN
        RAISERROR(778253, 16, 1)
        ROLLBACK TRANSACTION
    END

-- Added WAB 04/10/05
-- since organisationid is not an identity column we can't use @identity_inserted so instead return the inserted ids
select organisationid from inserted
*/


IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Organisation_ITrig]'))
DROP TRIGGER [dbo].[Organisation_ITrig]
GO