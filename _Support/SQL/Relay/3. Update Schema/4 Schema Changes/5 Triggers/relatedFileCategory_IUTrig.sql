/* NJH 2015/08/07 JIRA PROD2016-472 Synching Attachments- update the entityTypeId column if entitytype field changes */

IF  EXISTS (SELECT 1 FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[relatedFileCategory_IUTrig]'))
DROP TRIGGER [dbo].[relatedFileCategory_IUTrig]
GO

CREATE TRIGGER [dbo].[relatedFileCategory_IUTrig] ON [dbo].[relatedFileCategory] FOR INSERT,UPDATE AS

IF UPDATE(fileCategoryEntity)
Begin
	update relatedFileCategory set entityTypeID = s.entitytypeID
	from relatedFileCategory rfc
		inner join inserted i on i.fileCategoryID = rfc.fileCategoryID
		inner join schemaTable s on rfc.fileCategoryEntity = s.entityName
	where rfc.entityTypeID is null
End