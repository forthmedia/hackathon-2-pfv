if exists (select * from sysobjects where xtype='TR' and name='flag_createFlagViewTrigger')
	drop trigger flag_createFlagViewTrigger

GO

CREATE TRIGGER [dbo].[flag_createFlagViewTrigger]
   ON  [dbo].[flag]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN

/*	=============================================
	Author:        WAB
	Create date: 29/04/2014
	Description:   
	2015-04-15  	WAB modified so that does not update the view if only adding boolean flags.  
				Boolean flags are not included as individual columns in the vEntity view 
				(although in future we could have views with booleans in them)
				I have removed them because we were getting transaction problems when creating flags within the salesforce connector
	2016-05-18	During BF-699.  Trigger update of flag view for linked entities
	2016-10-13	WAB If there is an existing withBooleans view then update it.  Hope doesn't cause problem of 2015-04-15 again
	============================================= */

      SET NOCOUNT ON;

      IF update(flagTextID)
      BEGIN
            DECLARE @tablename sysname, @viewName sysname, @includeBooleans bit
            DECLARE tables insensitive cursor for

            SELECT distinct tablename, 'v' + tablename as viewName, 0 as includeBooleans
            FROM
                  inserted f
                        inner join
                  FlagGroup fg  ON f.flagGroupID      = fg.flagGroupID
                        inner join
                  FlagEntityType fet ON fet.entityTypeID = fg.entityTypeID    
                        inner join
                  FlagType ft  ON fg.flagTypeID = ft.flagTypeID
			WHERE
				ft.name not in ('radio','checkbox')		
				

			UNION
			
			/* If there is an existing 'withBoolean' view then recreate it */
            SELECT distinct tablename, s.name as viewName, 1 as includeBooleans
            FROM
                  inserted f
                        inner join
                  FlagGroup fg  ON f.flagGroupID      = fg.flagGroupID
                        inner join
                  FlagEntityType fet ON fet.entityTypeID = fg.entityTypeID    
                        inner join
                  FlagType ft  ON fg.flagTypeID = ft.flagTypeID
						inner join
				  sysObjects s on	s.name like 'v' + tablename + 'withBoolean' and xType = 'v'
				

			UNION

            SELECT distinct entityname, 'v' + entityname as viewName, 0 as includeBooleans
            FROM
                  inserted f
                        inner join
					schemaTable st  ON st.entityTypeID = f.linksToEntityTypeID
			WHERE
				f.linksToEntityTypeID is not null -- actually implicit in the inner join, but this makes it more understandable

      
                  OPEN tables
            
                  FETCH next from tables into @tablename, @viewName, @includeBooleans
            
                  WHILE @@fetch_status = 0 
                  BEGIN
                        
                        print @tablename
                        exec createFlagView @entityName = @tablename, @viewName = @viewName, @includeBooleans = @includeBooleans
                  
                                    FETCH next from tables into @tablename, @viewName, @includeBooleans
                  END   
                  
                  CLOSE tables
                  DEALLOCATE tables

      
      END


END

GO

