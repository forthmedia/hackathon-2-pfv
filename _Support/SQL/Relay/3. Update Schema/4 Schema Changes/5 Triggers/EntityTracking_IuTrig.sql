/* NJH 2016/01/19 JIRA BF-32*/
IF exists (select 1 from sysobjects where name = 'EntityTracking_IUTrig')
	DROP TRIGGER EntityTracking_IUTrig
GO

create trigger EntityTracking_IUTrig on EntityTracking
instead of insert,update
AS
BEGIN
	set nocount on
	
	/* Get fromLocID and toLocID */
	insert into entityTrackingURL (url)
	select distinct rtrim(ltrim(left(fromLoc,450))) --RJT 450 because only up to 900 bytes can be indexed
	from inserted i
			  left join 
		 entityTrackingURL fromUrl on rtrim(ltrim(left(i.fromLoc,450))) = rtrim(ltrim(fromUrl.url))
	where i.fromLoc is not null and fromUrl.entityTrackingUrlID is null

	insert into entityTrackingURL (url)
	select distinct rtrim(ltrim(left(toLoc,450)))
	from inserted  i 
			  left join 
		entityTrackingURL toUrl on rtrim(ltrim(left(i.toLoc,450))) = rtrim(ltrim(toUrl.url))
	where i.toLoc is not null and toUrl.entityTrackingUrlID is null

	if exists (select 1 from deleted)
	/* updates */
	begin
		update entityTrackingBase 
		set entityid = i.entityID
			,entityTypeID = i.entityTypeID
			,fromUrlID = fromUrl.entityTrackingUrlID
			,toUrlID = toUrl.entityTrackingUrlID
			,visitID = i.visitID
			,commID = i.commID
			,personID = i.personID
			,created = i.created
			,clickThru = i.clickThru
		from inserted i
			inner join entityTrackingBase e on e.entityTrackingID = i.entityTrackingID
			left join entityTrackingURL toUrl   on rtrim(ltrim(left(i.toLoc,450))) = rtrim(ltrim(toUrl.url))
			left join entityTrackingURL fromUrl on rtrim(ltrim(left(i.fromLoc,450))) = rtrim(ltrim(fromUrl.url))
	end
	else
	begin
		/* inserts */
		insert into 
		entityTrackingBase (entityid, entityTypeID, fromUrlID, toUrlID,visitID,commID,personID,created,clickThru)
		select entityid, entityTypeID, fromUrl.entityTrackingUrlID, toUrl.entityTrackingUrlID,visitID,commID,personID,created,clickThru
		from inserted
			left join entityTrackingURL toUrl on rtrim(ltrim(left(inserted.toLoc,450))) = rtrim(ltrim(toUrl.url))
			left join entityTrackingURL fromUrl on rtrim(ltrim(left(inserted.fromLoc,450))) = rtrim(ltrim(fromUrl.url))
	end
END
