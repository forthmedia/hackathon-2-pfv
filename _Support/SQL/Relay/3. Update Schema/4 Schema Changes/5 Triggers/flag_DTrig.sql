if exists (select * from sysobjects where xtype='TR' and name='Flag_DTrig')
	drop trigger Flag_DTrig

GO

/****** Object:  Trigger dbo.Flag_DTrig    Script Date: 8/6/99 3:38:17 PM ******/
/****** Object:  Trigger dbo.Flag_DTrig    Script Date: 11/03/99 17:38:12 ******/
CREATE TRIGGER [dbo].[Flag_DTrig] ON [dbo].[flag] FOR DELETE AS
/* Altered By NYB on: 11/03/99 17:38:12 */
/*
 * PREVENT DELETES IF DEPENDENT RECORDS IN 'BooleanFlagData'
 */
IF (SELECT COUNT(*) FROM deleted, BooleanFlagData WHERE (deleted.FlagID = BooleanFlagData.FlagID)) > 0
    BEGIN
        RAISERROR(778247, 16, 1)
        ROLLBACK TRANSACTION
    END

/*
 * CASCADE DELETES TO 'TextFlagData'
 */
DELETE TextFlagData FROM deleted, TextFlagData WHERE deleted.FlagID = TextFlagData.FlagID

/*
 * CASCADE DELETES TO 'DateFlagData'
 */
DELETE DateFlagData FROM deleted, DateFlagData WHERE deleted.FlagID = DateFlagData.FlagID

/*
 * CASCADE DELETES TO 'IntegerFlagData'
 */
DELETE IntegerFlagData FROM deleted, IntegerFlagData WHERE deleted.FlagID = IntegerFlagData.FlagID


