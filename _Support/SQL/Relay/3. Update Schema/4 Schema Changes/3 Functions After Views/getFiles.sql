if exists (select 1 from sysobjects where name = 'getFiles' and type = 'IF')
	drop function dbo.getFiles

GO

/*
This function assumes the following entityTypeIDs
files : 47
fileType : 217
fileTypeGroup : 218
*/
IF 
	NOT EXISTS (select 1 from schemaTable where entityName = 'files' and entityTypeID = 47)
	OR  NOT EXISTS (select 1 from schemaTable where entityName = 'fileType' and entityTypeID = 217)
	OR  NOT EXISTS (select 1 from schemaTable where entityName = 'fileTypeGroup' and entityTypeID = 218)
BEGIN
	RAISERROR  ('This Function Requires the Following EntityTypeIDs. Files:47, FileType:217, FileTypeGroup:218',16,16)
END

GO

create function getFiles (@personid int, @countryid int, @applyRights bit ) returns TABLE
AS


/*
function getFiles

Returns (as a table) files and associated rights for a given person/countryid

Can either filter the result based on rights, or return all files with a hasViewRights column

@personid  - personID to calculate rights for
@countryid	- countryID of the person.  Can pass in null and returns a record for each country which can then be filtered by programmer  (for example with an IN statement)
@applyRights (bit) whether or not to filter the results by rights 

WAB 2016-09-27  PROD2016-2393 Added file.name and fileType.path to the result
YMA 2016-11-08  PROD2016-2461 Added f.name as title to the result

*/


RETURN
(
select

	f.fileid  
	,f.name
	,f.name as title
	,f.fileTypeID
	,ft.fileTypeGroupID
	,ft.path
	,CASE 
		WHEN
			(ftg.hasRecordRightsBitMask & 1 = 0 OR rftg.level1 <> 0)
			AND
			(ft.hasRecordRightsBitMask & 1 = 0 OR rft.level1 <> 0)
			AND	
			(f.hasRecordRightsBitMask & 1 = 0 OR rf.level1 <> 0)
			AND
			(f.hasCountryScope_1 = 0 OR cs.countryid is not null)
			THEN 1 
		ELSE 
			0 
		END 
	AS hasViewRights

	,CASE 
		WHEN
			f.personid = @personid THEN 1	
		WHEN
			f.hasRecordRightsBitMask & 2 <> 0 
			THEN rf.level2 
		WHEN
			ft.hasRecordRightsBitMask & 2 <> 0 
			THEN rft.level2 
		WHEN
			ftg.hasRecordRightsBitMask & 2 <> 0 
			THEN rftg.level2 
		ELSE
			1
		END
	AS hasEditRights
	
	, case when	f.personid = @personid THEN 1 ELSE 0 END as isOwner
	, countryid

	/* These columns useful for debugging */
	,rf.level1 as file_level1
	,rf.level2 as file_level2
	,rft.level1 as fileType_level1
	,rft.level2 as fileType_level2
	,rftg.level1 as fileTypeGroup_level1
	,rftg.level2 as fileTypeGroup_level2
	,ftg.hasRecordRightsBitMask & 1 As fileTypeGroup_hasRecordRights
	,ft.hasRecordRightsBitMask & 1As fileType_hasRecordRights
	,f.hasRecordRightsBitMask & 1 As file_hasRecordRights
	,f.hasCountryScope_1
	,(f.hasRecordRightsBitMask & 2)/2 As file_hasEditRights
	,(ft.hasRecordRightsBitMask & 2)/2 As fileType_EditRights
	,(ftg.hasRecordRightsBitMask & 2)/2 As fileTypeGroup_EditRights
	

from 
	fileTypeGroup ftg
		inner join 
	fileType ft ON ftg.fileTypeGroupID = ft.fileTypeGroupID
		inner join 
	files f ON ft.fileTypeID = f.fileTypeID
		left join 
	/* rights on fileTypeGroup */	
	dbo.getRecordRightsTableByPerson (218,@personid) as rftg on rftg.recordid = ftg.fileTypeGroupid
		left join 
	/* rights on fileType */
	dbo.getRecordRightsTableByPerson (217,@personid) as rft on rft.recordid = ft.fileTypeid
		left join
	/* rights on file */		
	dbo.getRecordRightsTableByPerson (47,@personid) as rf on rf.recordid = f.fileid
		left join
	/* country scope on file */
	vcountryScope cs on f.hasCountryScope_1 = 1 AND cs.entityid = f.fileid and cs.entity = 'files' and (cs.countryid = @countryid or @countryid is null)


where 
	/* rights on fileTypeGroup */
	@applyRights = 0
		OR
	(
		/* NOTE: this section is an exact copy of the section that selects hasViewRights */
		CASE 
		WHEN
			(ftg.hasRecordRightsBitMask & 1 = 0 OR rftg.level1 <> 0)
			AND
			(ft.hasRecordRightsBitMask & 1 = 0 OR rft.level1 <> 0)
			AND	
			(f.hasRecordRightsBitMask & 1 = 0 OR rf.level1 <> 0)
			AND
			(f.hasCountryScope_1 = 0 OR cs.countryid is not null)
			THEN 1 
		ELSE 
			0 
		END 
		= 1		
	)

)
