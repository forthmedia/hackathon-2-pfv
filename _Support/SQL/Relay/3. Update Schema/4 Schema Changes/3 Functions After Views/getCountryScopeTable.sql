createPlaceholderObject 'inlinedtablefunction', 'getCountryScopeTable'
GO
alter function [dbo].[getCountryScopeTable] (@entityTypeID int, @CountryIDList varchar(max)) returns
TABLE
AS



RETURN (


/*  
	WAB 2015-11 PROD2015-364.  
		Created this function to encapsulate some of the country scoping logic
		In particular to deal with level11 being used as an exclude 

*/


 select 
			entityID 
			,entity 
			,entityTypeID
			-- ,countryid
			,case when level1Exists = 1 then level1 else 1 end & ~ level11 as level1
			, level11
			,scopeInheritedFromCountryGroup

from (
		SELECT 
			entityID 
			,entity 
			,entityTypeID
--			,countryMemberID as countryid
			,cast (SUM(case when cg.countrymemberID is not null then PERMISSION else 0 end & 1) as bit) as level1
			,cast (sum(case when cg.countrymemberID is not null then PERMISSION else 0 end & power(2,11-1)) as bit) as level11
			,cast (sum(permission & 1) as bit) as level1Exists
			,cast (sum(permission & power(2,11-1)) as bit) as level11Exists
		 	, min(case when cg.countryGroupID = cg.countryMemberID then 1 else 0 end) as scopeInheritedFromCountryGroup
			
		FROM 
			countryscope cs
				left join
			( countryGroup cg	 
				inner join 
			dbo.csvToTable (@CountryIDList) AS c 	on countryMemberID = c.value)
			on cg.countryGroupID = cs.countryID   
		WHERE
			1=1
			and cs.entityTypeID = @entityTypeid 
			
		GROUP BY
			entityID
			,entity
			,entityTypeID
 			-- ,countryMemberID
 	) as x


)


