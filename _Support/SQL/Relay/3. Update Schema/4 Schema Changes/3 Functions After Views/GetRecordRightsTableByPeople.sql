
createPlaceholderObject 'inlinedtablefunction', 'getRecordRightsTableByPeople'
GO
alter function [dbo].[getRecordRightsTableByPeople] (
														  @entityTypeID int					
														, @personIDs varchar(max)			-- a list of personIDs
														, @recordIDs varchar(max) = '-1')   -- a list of recordIDs, or leave as default (-1) for all records
														
returns TABLE
AS

/*
WAB 2016-06-22 
	This function, similar to getRecordRightsTableByPerson allows a list of personIDs and a list of recordIDs to be passed in
	Mainly created so that can be called by dbo.checkRecordRights ()


******************************
******************************
IMPORTANT IMPORTANT IMPORTANT 
IMPORTANT IMPORTANT IMPORTANT 
IMPORTANT IMPORTANT IMPORTANT 

The code in this file is duplicated (almost)
	getRecordRightsTableByPerson.sql
	getRecordRightsTableByUserGroup.sql
	getRecordRightsTableByPeople.sql	
	 
Any change to one file must be made to all files

While I like to keep things DRY (and did initially) 
I think that the performance of these queries is important enough to justify a repeat of the code

******************************
******************************

*/

RETURN (
select 
	  isNull (level1,0)  
	+ isNull (level2,0) * power (2,2-1)
	+ isNull (level3,0) * power (2,3-1)
	+ isNull (level4,0) * power (2,4-1)
	+ isNull (level5,0) * power (2,5-1)

	+ isNull (level11,0) * power (2,11-1)
	as permission,  /* Recreate a permission bitmask - in some cases this is easier to deal with than individual columns */
	*
from
	(select 
		 entityTypeID
		, personid
		, recordid
		, case 
				when level1 is null and level11 is null then null
				else isnull (level1,1) & isnull(level11,1)
		  end as level1
		,level2
		,level3
		,level4
		,level5
		
		,level11
			
	FROM	
		(SELECT  
			
			 rr.entityTypeID
			, p.Value as personid 
			, rr.recordid
			,dbo.evaluateRecordRights (count(case when PERMISSION & 1 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 1)      ,groupAnd_level1,groupNot_Level1,default) AS level1
			,dbo.evaluateRecordRights (count(case when PERMISSION & 2 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 2)/2    ,groupAnd_level2,groupNot_Level2,default) AS level2 
			,dbo.evaluateRecordRights (count(case when PERMISSION & 4 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 4)/4    ,groupAnd_level3,groupNot_Level3,default) AS level3 
			,dbo.evaluateRecordRights (count(case when PERMISSION & 8 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 8)/8    ,groupAnd_level4,groupNot_Level4,default) AS level4 
			,dbo.evaluateRecordRights (count(case when PERMISSION & 16 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 16)/16    ,groupAnd_level5,groupNot_Level5,default) AS level5

			,dbo.evaluateRecordRights (count(case when PERMISSION & 1024 <> 0 then 1 end), SUM(case when rg.personid is not null then PERMISSION end & 1024)/1024, groupAnd_level11, groupNot_Level11,default) AS level11
	
		FROM
			dbo.csvToTable (@personIDs) AS p 	
				inner join
			vRecordRightsOperator rro  on 1=1		
					inner join
			dbo.csvToTable (@recordIDs ) AS records on @recordIDs = '-1' or records.Value = rro.recordid 	
					inner join
			RecordRights rr on rro.entityTypeID = rr.entityTypeID and rro.recordid = rr.recordid	
					left join
			rightsGroup rg on rg.UserGroupID = rr.usergroupid and rg.personid = p.value
	
		where rro.entityTypeID = @entityTypeID
			
		GROUP BY p.value,rr.entityTypeID, rr.recordid
			, groupAnd_level1, groupNot_Level1
			, groupAnd_level2, groupNot_Level2
			, groupAnd_level3, groupNot_Level3
			, groupAnd_level4, groupNot_Level4
			, groupAnd_level5, groupNot_Level5
			, groupAnd_level11, groupNot_Level11
		) AS X 
	) as Y
)


