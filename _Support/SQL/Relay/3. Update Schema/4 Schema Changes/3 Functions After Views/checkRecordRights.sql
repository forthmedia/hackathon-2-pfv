createPlaceHolderObject 'function','checkRecordRights'
GO
alter  function checkRecordRights (
									  @entityTypeID int
									, @personid int
									, @entityid int
									, @level int = 1) 
returns bit as

/*
WAB 2016-06-22 
	test record rights for an individual person/record


*/

BEGIN
	Declare @result bit
	
	IF @level = 1
		Select @result = level1 from dbo.getRecordRightsTableByPeople (@entityTypeID, @personid, @entityid)	as x
	ELSE IF @level = 2
		Select @result = level2 from dbo.getRecordRightsTableByPeople (@entityTypeID, @personid, @entityid)	as x	
	ELSE IF @level = 3
		Select @result = level3 from dbo.getRecordRightsTableByPeople (@entityTypeID, @personid, @entityid)	as x	

	return  @result

END
