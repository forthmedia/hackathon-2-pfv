/*created for Case 436226 */
if not exists (SELECT * FROM sys.table_types where name='UndeleteLocationsTableType')
	begin
		CREATE TYPE UndeleteLocationsTableType AS TABLE
		 (                     
			   [LocationID] int NOT NULL,                
			   [DoNotRecoverPersons] [int] NOT NULL Default(0)                
		 )
	end



