if not exists (select 1 from sys.table_types where name='personEmailTableType')
begin
	create type personEmailTableType as table
	( 
		personID int,
		email varchar(250),
		accountTypeID int
	)
end

GO