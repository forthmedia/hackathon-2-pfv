IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorColumnValueMapping_connectorMapping]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorColumnValueMapping]'))
ALTER TABLE [dbo].[connectorColumnValueMapping] DROP CONSTRAINT [FK_connectorColumnValueMapping_connectorMapping]
GO

ALTER TABLE [dbo].[connectorColumnValueMapping]  WITH CHECK ADD  CONSTRAINT [FK_connectorColumnValueMapping_connectorMapping] FOREIGN KEY([connectorMappingID])
REFERENCES [dbo].[connectorMapping] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[connectorColumnValueMapping] CHECK CONSTRAINT [FK_connectorColumnValueMapping_connectorMapping]
GO