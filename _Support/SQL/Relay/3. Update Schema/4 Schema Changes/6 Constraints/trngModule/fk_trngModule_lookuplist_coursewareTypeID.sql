/* 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration */
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_trngModule_lookuplist_coursewareTypeID]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngModule]'))
ALTER TABLE [dbo].[trngModule]  WITH CHECK ADD  CONSTRAINT [FK_trngModule_lookuplist_coursewareTypeID] FOREIGN KEY([coursewareTypeID])
REFERENCES [dbo].[lookUpList] ([lookupID])
GO