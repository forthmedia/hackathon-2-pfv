/* personID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_trngPersonCertification_personID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngPersonCertification]'))
BEGIN
	ALTER TABLE [dbo].[trngPersonCertification]  WITH NOCHECK ADD CONSTRAINT [fk_trngPersonCertification_personID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([personID],(0))=(1)))
END

ALTER TABLE [dbo].[trngPersonCertification] CHECK CONSTRAINT [fk_trngPersonCertification_personID_person_personID]
GO