/* statusID column */
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_trngPersonCertification_trngPersonCertificationStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngPersonCertification]'))
ALTER TABLE [dbo].[trngPersonCertification]  WITH NOCHECK ADD  CONSTRAINT [FK_trngPersonCertification_trngPersonCertificationStatus] FOREIGN KEY([statusID])
REFERENCES [dbo].[trngPersonCertificationStatus] ([statusID])
GO