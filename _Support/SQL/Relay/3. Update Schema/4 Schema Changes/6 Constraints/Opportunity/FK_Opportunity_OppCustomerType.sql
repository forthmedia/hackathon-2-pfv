IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Opportunity_OppCustomerType]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_Opportunity_OppCustomerType]
GO

update opportunity set oppCustomerTypeID = null where oppCustomerTypeID = 0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_Opportunity_OppCustomerType] FOREIGN KEY([oppCustomerTypeID])
REFERENCES [dbo].[oppCustomerType] ([OppCustomerTypeID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_Opportunity_OppCustomerType]
GO