IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunity_campaign]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_opportunity_campaign]
GO

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_opportunity_campaign] FOREIGN KEY([campaignID])
REFERENCES [dbo].[campaign] ([campaignID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_opportunity_campaign]
GO