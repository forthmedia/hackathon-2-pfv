/* distiLocationID column */
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_opportunity_distiLocationID_location_locationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [fk_opportunity_distiLocationID_location_locationID]
GO

update opportunity set distiLocationID = null where distiLocationID = 0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [fk_opportunity_distiLocationID_location_locationID] CHECK  (([dbo].[checkEntityIDExists]([distiLocationID],(1))=(1)))
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [fk_opportunity_distiLocationID_location_locationID]
GO