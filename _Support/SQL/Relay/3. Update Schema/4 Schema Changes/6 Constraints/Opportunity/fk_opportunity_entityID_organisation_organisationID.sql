/* entityID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_opportunity_entityID_organisation_organisationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
BEGIN
	update opportunity set entityID=null where entityID = 0
	
	ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [fk_opportunity_entityID_organisation_organisationID] CHECK  (([dbo].[checkEntityIDExists]([entityID],(2))=(1)))
END

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [fk_opportunity_entityID_organisation_organisationID]
GO