/* partnerSalesManagerPersonID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_opportunity_partnerSalesManagerPersonID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
BEGIN
	update opportunity set partnerSalesManagerPersonID=null where partnerSalesManagerPersonID = 0
	
	ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [fk_opportunity_partnerSalesManagerPersonID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([partnerSalesManagerPersonID],(0))=(1)))
END

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [fk_opportunity_partnerSalesManagerPersonID_person_personID]
GO