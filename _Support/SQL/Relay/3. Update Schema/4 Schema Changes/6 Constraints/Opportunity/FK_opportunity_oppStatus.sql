IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunity_oppStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_opportunity_oppStatus]
GO

update opportunity set statusID=null where statusID=0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_opportunity_oppStatus] FOREIGN KEY([statusID])
REFERENCES [dbo].[OppStatus] ([StatusID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_opportunity_oppStatus]
GO