IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunity_Country]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_opportunity_Country]
GO

update opportunity set countryID = null where countryID = 0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_opportunity_Country] FOREIGN KEY([countryID])
REFERENCES [dbo].[Country] ([countryID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_opportunity_Country]
GO