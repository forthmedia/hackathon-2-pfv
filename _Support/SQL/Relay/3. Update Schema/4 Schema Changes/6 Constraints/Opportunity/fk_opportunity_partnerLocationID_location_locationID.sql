/* partnerLocationID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_opportunity_partnerLocationID_location_locationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
BEGIN
	update opportunity set partnerLocationID=null where partnerLocationID = 0
	
	ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [fk_opportunity_partnerLocationID_location_locationID] CHECK  (([dbo].[checkEntityIDExists]([partnerLocationID],(1))=(1)))
END

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [fk_opportunity_partnerLocationID_location_locationID]
GO