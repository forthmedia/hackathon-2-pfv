
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunity_OppStage]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_opportunity_OppStage]
GO

update opportunity set stageID = null where stageID = 0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_opportunity_OppStage] FOREIGN KEY([stageID])
REFERENCES [dbo].[OppStage] ([OpportunityStageID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_opportunity_OppStage]
GO