IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunity_OppSource]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunity]'))
ALTER TABLE [dbo].[opportunity] DROP CONSTRAINT [FK_opportunity_OppSource]
GO

update opportunity set sourceID = null where sourceID = 0

ALTER TABLE [dbo].[opportunity]  WITH NOCHECK ADD  CONSTRAINT [FK_opportunity_OppSource] FOREIGN KEY([sourceID])
REFERENCES [dbo].[OppSource] ([OpportunitySourceID])
GO

ALTER TABLE [dbo].[opportunity] CHECK CONSTRAINT [FK_opportunity_OppSource]
GO