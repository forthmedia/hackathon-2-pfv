IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueueError_connectorQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueueError]'))
ALTER TABLE [dbo].[connectorQueueError]  WITH CHECK ADD  CONSTRAINT [FK_connectorQueueError_connectorQueue] FOREIGN KEY([connectorQueueID])
REFERENCES [dbo].[connectorQueue] ([ID])
ON DELETE CASCADE


ALTER TABLE [dbo].[connectorQueueError] CHECK CONSTRAINT [FK_connectorQueueError_connectorQueue]
GO