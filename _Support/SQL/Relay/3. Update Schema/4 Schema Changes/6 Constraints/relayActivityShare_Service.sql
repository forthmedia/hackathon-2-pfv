if not exists (select * from sysconstraints where constID=OBJECT_ID(N'[FK_relayActivityShare_Service]'))
ALTER TABLE [dbo].[relayActivityShare]  WITH CHECK ADD  CONSTRAINT [FK_relayActivityShare_Service] FOREIGN KEY([serviceID])
REFERENCES [dbo].[Service] ([serviceID])
GO

ALTER TABLE [dbo].[relayActivityShare] CHECK CONSTRAINT [FK_relayActivityShare_Service]
GO