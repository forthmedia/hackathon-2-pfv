/* convertedFromLeadID column 
	NJH 2015/06/18 - changed to set null on delete as this
		a) makes it easier to delete from the backend if needed (still have the opportunityID on the lead)
		b) seems that SF allows people to delete converted leads in their utility and so need to handle this, rather than falling over with a FK constraint error
*/
IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_location_lead]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_location_lead]
GO

ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_location_lead] FOREIGN KEY([convertedFromLeadID])
REFERENCES [dbo].[Lead] ([LeadID])
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_location_lead]
GO
