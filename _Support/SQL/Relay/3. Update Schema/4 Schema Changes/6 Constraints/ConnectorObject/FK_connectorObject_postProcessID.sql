IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorObject_postProcessID]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorObject]'))
BEGIN
	ALTER TABLE [dbo].[connectorObject]  WITH CHECK ADD  CONSTRAINT [FK_connectorObject_postProcessID] FOREIGN KEY([postProcessID])
	REFERENCES [dbo].[processHeader] ([processID])
END