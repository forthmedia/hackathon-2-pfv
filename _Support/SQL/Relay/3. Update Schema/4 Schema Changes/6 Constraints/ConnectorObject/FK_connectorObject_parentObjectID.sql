IF  NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorObject_parentObjectID]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorObject]'))
BEGIN
	ALTER TABLE [dbo].[connectorObject]  WITH CHECK ADD  CONSTRAINT [FK_connectorObject_parentObjectID] FOREIGN KEY([parentObjectID])
	REFERENCES [dbo].[connectorObject] ([ID])
END