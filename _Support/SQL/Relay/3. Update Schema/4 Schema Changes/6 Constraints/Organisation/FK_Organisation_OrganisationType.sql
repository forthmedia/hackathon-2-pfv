/* add organisationType foreign key constraint */
IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_organisation_organisationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[organisation]'))
BEGIN
	ALTER TABLE [dbo].[organisation]  WITH CHECK ADD  CONSTRAINT [FK_organisation_organisationType] FOREIGN KEY([organisationTypeID])
	REFERENCES [dbo].[organisationType] ([organisationTypeID])
END
GO