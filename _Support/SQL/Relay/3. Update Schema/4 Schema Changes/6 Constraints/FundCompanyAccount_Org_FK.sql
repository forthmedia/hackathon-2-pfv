if exists (select * from sys.foreign_keys where name='FundCompanyAccount_FK2')
	ALTER TABLE FundCompanyAccount DROP CONSTRAINT FundCompanyAccount_FK2 

GO

if not exists (select 1 from sysobjects where name = 'FundCompanyAccount_Org_FK')
	ALTER TABLE FundCompanyAccount WITH NOCHECK ADD CONSTRAINT FundCompanyAccount_Org_FK CHECK (dbo.CheckEntityIDExists(organisationid,2) = 1)