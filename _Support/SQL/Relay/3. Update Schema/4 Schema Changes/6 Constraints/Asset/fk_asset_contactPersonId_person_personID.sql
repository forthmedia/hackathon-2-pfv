
/* contactPersonID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_asset_contactPersonID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[asset]'))
ALTER TABLE [dbo].[asset]  WITH CHECK ADD  CONSTRAINT [fk_asset_contactPersonID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([contactPersonID],(0))=(1)))