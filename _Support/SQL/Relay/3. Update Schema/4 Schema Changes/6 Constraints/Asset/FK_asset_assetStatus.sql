IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_asset_assetStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[asset]'))
ALTER TABLE [dbo].[asset]  WITH CHECK ADD  CONSTRAINT [FK_asset_assetStatus] FOREIGN KEY([statusID])
REFERENCES [dbo].[assetStatus] ([assetStatusID])
GO