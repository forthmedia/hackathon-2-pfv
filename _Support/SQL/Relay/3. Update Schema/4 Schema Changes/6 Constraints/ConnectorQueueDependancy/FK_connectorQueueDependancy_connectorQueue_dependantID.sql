IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueueDependancy_connectorQueue_dependantID]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueueDependency]'))
BEGIN
	ALTER TABLE [dbo].[connectorQueueDependency]  WITH CHECK ADD  CONSTRAINT [FK_connectorQueueDependancy_connectorQueue_dependantID] FOREIGN KEY([dependentOnConnectorQueueID])
	REFERENCES [dbo].[connectorQueue] ([ID])
	ON UPDATE CASCADE
	ON DELETE CASCADE

	ALTER TABLE [dbo].[connectorQueueDependency] CHECK CONSTRAINT [FK_connectorQueueDependancy_connectorQueue_dependantID]
END