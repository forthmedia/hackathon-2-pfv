IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueueDependancy_connectorQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueueDependency]'))
BEGIN
	ALTER TABLE [dbo].[connectorQueueDependency]  WITH CHECK ADD  CONSTRAINT [FK_connectorQueueDependancy_connectorQueue] FOREIGN KEY([connectorQueueID])
	REFERENCES [dbo].[connectorQueue] ([ID])

	ALTER TABLE [dbo].[connectorQueueDependency] CHECK CONSTRAINT [FK_connectorQueueDependancy_connectorQueue]
END