/* add foreign key constraint to the pricebookEntry table */
if exists (select 1 from information_schema.tables where table_name='oppProducts' and table_type='Table')
BEGIN
	IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_oppProducts_PricebookEntry]') AND parent_object_id = OBJECT_ID(N'[dbo].[oppProducts]'))
	BEGIN
		ALTER TABLE [dbo].[oppProducts]  WITH CHECK ADD  CONSTRAINT [FK_oppProducts_PricebookEntry] FOREIGN KEY([pricebookEntryID])
		REFERENCES [dbo].[PricebookEntry] ([pricebookEntryID])
	END

	ALTER TABLE [dbo].[oppProducts] CHECK CONSTRAINT [FK_oppProducts_PricebookEntry]
END
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_oppProducts_PricebookEntry]'))
	exec sp_rename 'FK_oppProducts_PricebookEntry', 'FK_opportunityProduct_PricebookEntry'