/* add foreign key constraint to the opportunity table */
if exists (select 1 from information_schema.tables where table_name='oppProducts' and table_type='Table')
BEGIN
	IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OPProducts_opportunity]') AND parent_object_id = OBJECT_ID(N'[dbo].[oppProducts]'))
	BEGIN
		ALTER TABLE [dbo].[oppProducts]  WITH CHECK ADD  CONSTRAINT [FK_OPProducts_opportunity] FOREIGN KEY([opportunityID])
		REFERENCES [dbo].[opportunity] ([opportunityID])
	END

	ALTER TABLE [dbo].[oppProducts] CHECK CONSTRAINT [FK_OPProducts_opportunity]
END
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OPProducts_opportunity]'))
	exec sp_rename 'FK_OPProducts_opportunity', 'FK_opportunityProduct_Opportunity'

/* PROD2016-1146 NJH 2016/05/18 - problem with connector deleting opportunties... Add a cascade delete on the foreign key constraint */	
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_opportunityProduct_Opportunity]') AND parent_object_id = OBJECT_ID(N'[dbo].[opportunityProduct]'))
ALTER TABLE [dbo].[opportunityProduct] DROP CONSTRAINT [FK_opportunityProduct_Opportunity]
GO

ALTER TABLE [dbo].[opportunityProduct]  WITH CHECK ADD CONSTRAINT [FK_opportunityProduct_Opportunity] FOREIGN KEY([opportunityID])
REFERENCES [dbo].[opportunity] ([opportunityID]) ON DELETE CASCADE;