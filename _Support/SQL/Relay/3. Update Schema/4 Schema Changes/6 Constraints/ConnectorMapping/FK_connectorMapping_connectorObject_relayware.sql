

IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorMapping_connectorObject_relayware]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorMapping]'))
BEGIN
	--ALTER TABLE [dbo].[connectorMapping] DROP CONSTRAINT [FK_connectorMapping_connectorObject_rw]

	ALTER TABLE [dbo].[connectorMapping]  WITH CHECK ADD  CONSTRAINT [FK_connectorMapping_connectorObject_relayware] FOREIGN KEY([connectorObjectID_relayware])
	REFERENCES [dbo].[connectorObject] ([ID])
END

ALTER TABLE [dbo].[connectorMapping] CHECK CONSTRAINT [FK_connectorMapping_connectorObject_relayware]
GO