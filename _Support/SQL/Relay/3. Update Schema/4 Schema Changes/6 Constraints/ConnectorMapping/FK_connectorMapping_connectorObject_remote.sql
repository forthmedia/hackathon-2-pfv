IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorMapping_connectorObject_remote]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorMapping]'))
BEGIN
	--ALTER TABLE [dbo].[connectorMapping] DROP CONSTRAINT [FK_connectorMapping_connectorObject]
	
	ALTER TABLE [dbo].[connectorMapping]  WITH CHECK ADD  CONSTRAINT [FK_connectorMapping_connectorObject_remote] FOREIGN KEY([connectorObjectID_remote])
	REFERENCES [dbo].[connectorObject] ([ID])
END
GO

ALTER TABLE [dbo].[connectorMapping] CHECK CONSTRAINT [FK_connectorMapping_connectorObject_remote]
GO