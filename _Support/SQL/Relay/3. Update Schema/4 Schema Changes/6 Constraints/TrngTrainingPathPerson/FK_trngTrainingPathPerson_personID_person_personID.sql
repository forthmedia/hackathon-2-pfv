
/* personID column */
IF  NOT EXISTS (SELECT 1 FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_trngTrainingPathPerson_personID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngTrainingPathPerson]'))
BEGIN
	ALTER TABLE [dbo].[trngTrainingPathPerson]  WITH NOCHECK ADD CONSTRAINT [fk_trngTrainingPathPerson_personID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([personID],(0))=(1)))
END

ALTER TABLE [dbo].[trngTrainingPathPerson] CHECK CONSTRAINT [fk_trngTrainingPathPerson_personID_person_personID]
GO