IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_trngTrainingPathPerson_trngTrainingPath]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngTrainingPathPerson]'))
BEGIN
	ALTER TABLE [dbo].[trngTrainingPathPerson]  WITH CHECK ADD  CONSTRAINT [FK_trngTrainingPathPerson_trngTrainingPath] FOREIGN KEY([trainingPathID])
	REFERENCES [dbo].[trngTrainingPath] ([ID])

	ALTER TABLE [dbo].[trngTrainingPathPerson] CHECK CONSTRAINT [FK_trngTrainingPathPerson_trngTrainingPath]
END