IF  NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueue_ConnectorResponse_dataConnectorResponseID]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueue]'))
BEGIN
	ALTER TABLE [dbo].[connectorQueue]  WITH CHECK ADD CONSTRAINT [FK_connectorQueue_ConnectorResponse_dataConnectorResponseID] FOREIGN KEY([dataConnectorResponseID])
	REFERENCES [dbo].[connectorResponse] ([ID])
END
GO

ALTER TABLE [dbo].[connectorQueue] CHECK CONSTRAINT [FK_connectorQueue_ConnectorResponse_dataConnectorResponseID]
GO