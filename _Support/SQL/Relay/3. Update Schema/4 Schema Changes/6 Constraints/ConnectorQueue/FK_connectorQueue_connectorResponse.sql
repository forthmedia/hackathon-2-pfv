IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueue_connectorResponse]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueue]'))
BEGIN
	--ALTER TABLE [dbo].[connectorQueue] DROP CONSTRAINT [FK_connectorQueue_connectorResponse]

	ALTER TABLE [dbo].[connectorQueue]  WITH CHECK ADD  CONSTRAINT [FK_connectorQueue_connectorResponse] FOREIGN KEY([connectorResponseID])
	REFERENCES [dbo].[connectorResponse] ([ID])
END

ALTER TABLE [dbo].[connectorQueue] CHECK CONSTRAINT [FK_connectorQueue_connectorResponse]
GO