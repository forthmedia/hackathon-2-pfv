IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_connectorQueue_connectorObject]') AND parent_object_id = OBJECT_ID(N'[dbo].[connectorQueue]'))
BEGIN
--ALTER TABLE [dbo].[connectorQueue] DROP CONSTRAINT [FK_connectorQueue_connectorObject]

	ALTER TABLE [dbo].[connectorQueue]  WITH CHECK ADD  CONSTRAINT [FK_connectorQueue_connectorObject] FOREIGN KEY([connectorObjectID])
	REFERENCES [dbo].[connectorObject] ([ID])
END

ALTER TABLE [dbo].[connectorQueue] CHECK CONSTRAINT [FK_connectorQueue_connectorObject]
GO