IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EntityTrackingBase_EntityTrackingUrl_To]') AND parent_object_id = OBJECT_ID(N'[dbo].[entityTrackingBase]'))
ALTER TABLE [dbo].[entityTrackingBase] DROP CONSTRAINT [FK_EntityTrackingBase_EntityTrackingUrl_To]
GO

ALTER TABLE [dbo].[entityTrackingBase]  WITH NOCHECK ADD CONSTRAINT [FK_EntityTrackingBase_EntityTrackingUrl_To] FOREIGN KEY([toUrlID])
REFERENCES [dbo].[entityTrackingUrl] ([entityTrackingUrlID])
GO

ALTER TABLE [dbo].[entityTrackingBase] CHECK CONSTRAINT [FK_EntityTrackingBase_EntityTrackingUrl_To]
GO