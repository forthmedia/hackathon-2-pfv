IF NOT EXISTS (select * from information_schema.table_constraints where constraint_name='FK_Schedule_ScheduleType')
BEGIN
ALTER TABLE [dbo].[Schedule]  WITH NOCHECK ADD  CONSTRAINT [FK_Schedule_ScheduleType] FOREIGN KEY([ScheduleTypeID])
REFERENCES [dbo].[ScheduleType] ([ScheduleTypeID])
ON UPDATE CASCADE
ON DELETE CASCADE

END

GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_ScheduleType]

GO