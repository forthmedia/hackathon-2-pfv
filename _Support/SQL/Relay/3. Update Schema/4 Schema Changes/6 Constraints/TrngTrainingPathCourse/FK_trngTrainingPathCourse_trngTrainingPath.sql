IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_trngTrainingPathCourse_trngTrainingPath]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngTrainingPathCourse]'))
ALTER TABLE [dbo].[trngTrainingPathCourse]  WITH NOCHECK ADD  CONSTRAINT [FK_trngTrainingPathCourse_trngTrainingPath] FOREIGN KEY([trainingPathID])
REFERENCES [dbo].[trngTrainingPath] ([ID])
GO