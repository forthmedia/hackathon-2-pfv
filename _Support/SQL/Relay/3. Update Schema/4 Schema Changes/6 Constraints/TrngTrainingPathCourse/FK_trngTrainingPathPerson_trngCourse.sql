IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_trngTrainingPathCourse_trngCourse]') AND parent_object_id = OBJECT_ID(N'[dbo].[trngTrainingPathCourse]'))
ALTER TABLE [dbo].[trngTrainingPathCourse]  WITH NOCHECK ADD  CONSTRAINT [FK_trngTrainingPathCourse_trngCourse] FOREIGN KEY([courseID])
REFERENCES [dbo].[trngCourse] ([courseID])
GO