
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_lead_lookupList_source]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [FK_lead_lookupList_source] FOREIGN KEY([sourceID])
REFERENCES [dbo].[lookUpList] ([lookupID])
GO