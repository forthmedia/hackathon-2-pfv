/* lead progressID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[lead_progressID_lookupList_fieldname]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
BEGIN
	ALTER TABLE [dbo].[lead]  WITH NOCHECK ADD CONSTRAINT [lead_progressID_lookupList_fieldname] CHECK  (([dbo].[checkLookupFieldNameIsValid]([progressID],'leadProgressID')=(1)))
END

ALTER TABLE [dbo].[lead] CHECK CONSTRAINT [lead_progressID_lookupList_fieldname]
GO