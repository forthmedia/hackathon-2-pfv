IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_lead_currency]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead] DROP CONSTRAINT [FK_lead_currency]
GO

ALTER TABLE [dbo].[lead]  WITH NOCHECK ADD  CONSTRAINT [FK_lead_currency] FOREIGN KEY([currency])
REFERENCES [dbo].[currency] ([currencyIsoCode])
GO

ALTER TABLE [dbo].[lead] CHECK CONSTRAINT [FK_lead_currency]
GO