/* distiLocationID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_lead_distiLocationID_location_locationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [fk_lead_distiLocationID_location_locationID] CHECK  (([dbo].[checkEntityIDExists]([distiLocationID],(1))=(1)))
