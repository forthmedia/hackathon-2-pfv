
/* NJH 2015/06/21 changed the constraint to one where the FK is set to null on a delete so that we
	can delete opportunities without a problem in the connector and through the db... */
IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_lead_opportunity]') AND parent_object_id = OBJECT_ID(N'[dbo].[Lead]'))
ALTER TABLE [dbo].[Lead] DROP CONSTRAINT [FK_lead_opportunity]
GO

ALTER TABLE [dbo].[Lead]  WITH CHECK ADD  CONSTRAINT [FK_lead_opportunity] FOREIGN KEY([convertedOpportunityID])
REFERENCES [dbo].[opportunity] ([opportunityID])
ON DELETE SET NULL
GO