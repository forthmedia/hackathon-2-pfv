if exists (select 1 from sys.default_constraints where name='df_lead_leadTypeID' and type='D' and definition !='([dbo].[getDefaultLookupValue](''leadTypeID''))')
alter table lead drop constraint df_lead_leadTypeID

if not exists (select 1 from sys.default_constraints where name='df_lead_leadTypeID' and type='D')
alter table lead add constraint df_lead_leadTypeID default ([dbo].[getDefaultLookupValue]('leadTypeID')) for leadTypeID