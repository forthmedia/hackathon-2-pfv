IF EXISTS (SELECT 1 FROM sys.default_constraints WHERE name='DF_lead_active' and type='D' and definition !='((1))')
BEGIN
	ALTER TABLE [dbo].[lead]
	DROP CONSTRAINT [DF_lead_active]
END

IF NOT EXISTS (SELECT 1 FROM sys.default_constraints WHERE name='DF_lead_active' and type='D')
BEGIN	
	/* Table name */
	ALTER TABLE [dbo].[lead] 
	/* constraint name */
	ADD CONSTRAINT [DF_lead_active] 
	/* default value for column_name */
	DEFAULT (1) FOR active
END