/* convertedPersonID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_lead_convertedPersonID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [fk_lead_convertedPersonID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([convertedPersonID],(0))=(1)))