/* partnerLocationID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_lead_partnerLocationID_location_locationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [fk_lead_partnerLocationID_location_locationID] CHECK  (([dbo].[checkEntityIDExists]([partnerLocationID],(1))=(1)))
