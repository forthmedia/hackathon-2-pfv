
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_lead_country]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [FK_lead_country] FOREIGN KEY([countryID])
REFERENCES [dbo].[country] ([countryID])
GO