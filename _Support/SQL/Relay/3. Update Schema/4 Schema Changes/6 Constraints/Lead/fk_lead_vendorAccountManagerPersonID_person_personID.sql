/* vendorAccountManagerPersonID column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[fk_lead_vendorAccountManagerPersonID_person_personID]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead]  WITH CHECK ADD  CONSTRAINT [fk_lead_vendorAccountManagerPersonID_person_personID] CHECK  (([dbo].[checkEntityIDExists]([vendorAccountManagerPersonID],(0))=(1)))
