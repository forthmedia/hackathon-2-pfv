/* lead Approval Status column */
IF  NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[lead_approvalStatusID_lookupList_fieldname]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
BEGIN
	ALTER TABLE [dbo].[lead]  WITH NOCHECK ADD CONSTRAINT [lead_approvalStatusID_lookupList_fieldname] CHECK  (([dbo].[checkLookupFieldNameIsValid]([approvalStatusID],'leadApprovalStatusID')=(1)))
END

ALTER TABLE [dbo].[lead] CHECK CONSTRAINT [lead_approvalStatusID_lookupList_fieldname]
GO