IF  EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_lead_campaign]') AND parent_object_id = OBJECT_ID(N'[dbo].[lead]'))
ALTER TABLE [dbo].[lead] DROP CONSTRAINT [FK_lead_campaign]
GO

ALTER TABLE [dbo].[lead]  WITH NOCHECK ADD  CONSTRAINT [FK_lead_campaign] FOREIGN KEY([campaignID])
REFERENCES [dbo].[campaign] ([campaignID])
GO

ALTER TABLE [dbo].[lead] CHECK CONSTRAINT [FK_lead_campaign]
GO