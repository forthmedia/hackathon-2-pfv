IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vMDFStatus]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[vMDFStatus]
GO

CREATE VIEW [dbo].[vMDFStatus]
AS
SELECT     fas.fundApprovalStatus AS Status,
                          (SELECT     MAX(created) AS created
                            FROM          dbo.fundRequestActivityHistory AS fah
                            WHERE      (fundRequestActivityID = fra.fundRequestActivityID)) AS Date_Of_Status_Change, o.OrganisationName AS organisation_Name, 
                      c.CountryDescription AS Country, b.description AS Budget, fraa.amount AS Amount_Approved, fraa.created AS Approval_Date, 
                      bh.description AS product_category, fas.StatusGroup AS status_group, frt.fundRequestType AS Activity_Type,
                      fra.fundRequestActivityID AS Request_Activity_ID
FROM         dbo.fundRequestActivity AS fra INNER JOIN
                      dbo.fundRequest AS fr ON fra.fundRequestID = fr.fundRequestID INNER JOIN
                      dbo.FundCompanyAccount AS fca ON fca.AccountID = fr.accountID INNER JOIN
                      dbo.organisation AS o ON o.OrganisationID = fca.OrganisationID INNER JOIN
                      dbo.Country AS c ON c.CountryID = o.countryID INNER JOIN
                      dbo.fundRequestType AS frt ON frt.fundRequestTypeID = fra.fundRequestTypeID INNER JOIN
                      dbo.fundRequestActivityApproval AS fraa ON fraa.fundRequestActivityID = fra.fundRequestActivityID INNER JOIN
                      dbo.fundApprovalStatus AS fas ON fas.fundApprovalStatusID = fra.fundApprovalStatusID INNER JOIN
                      dbo.Budget AS b ON fca.budgetID = b.budgetID INNER JOIN
                      dbo.BudgetGroup AS bh ON b.BudgetGroupID = bh.BudgetGroupID

GO