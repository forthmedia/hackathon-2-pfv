If exists(select 1 from sysobjects where name='vOrgFlagData' and type='v')
drop view vOrgFlagData
GO

CREATE  view  [dbo].[vOrgFlagData]
AS

/*
WAB 13/06/07 trying out a concept
idea would be that this view is created by an sp, so that can be updated if new types come along

*/

select
	e.entityid,
	f.flaggrouptextid, 
	f.flaggroupid, 
	f.flaggroup, 
	f.flagid, 
	f.flagtextid, 
	f.flag, 
	f.flagTypeid, 
	case 	when flagtypeid in (2,3) and bfd.flagid is not null then '1'
		when flagtypeid in (6) then convert(varchar,ifd.data)
		when flagtypeid in (4) then convert(varchar,dfd.data)
		when flagtypeid in (5) then tfd.data
		when flagtypeid in (8) then convert(varchar,imfd.data)
	end as data	,
	case 	when flagtypeid in (2,3) then bfd.created 
		when flagtypeid in (6) then ifd.created
		when flagtypeid in (4) then dfd.created
		when flagtypeid in (5) then tfd.created
		when flagtypeid in (8) then imfd.created
	end as created,
	case 	when flagtypeid in (2,3) then bfd.lastupdated 
		when flagtypeid in (6) then ifd.lastupdated
		when flagtypeid in (4) then dfd.lastupdated
		when flagtypeid in (5) then tfd.lastupdated
		when flagtypeid in (8) then imfd.lastupdated
	end as lastupdated

from 
 (select organisationid as entityid from organisation) as e join 
 vflagdef f on f.pl = 'o'
	left join booleanflagdata bfd on flagtypeid in (2,3) and bfd.flagid = f.flagid and bfd.entityid = e.entityid
	left join integerflagdata ifd on flagtypeid in (6) and ifd.flagid = f.flagid and ifd.entityid = e.entityid
	left join dateflagdata dfd on flagtypeid in (4) and dfd.flagid = f.flagid and dfd.entityid = e.entityid

	left join integermultipleflagdata imfd on flagtypeid in (8) and imfd.flagid = f.flagid and imfd.entityid = e.entityid
	left join textflagdata tfd on flagtypeid in (5) and tfd.flagid = f.flagid and tfd.entityid = e.entityid

GO

