/* 2013-04-11 STCR CASE 432194 - use trngModuleSet.description_defaultTranslation */
ALTER VIEW [dbo].[vTrngCertificationRules] AS   
SELECT tcr.certificationID, tcr.certificationRuleID, SUM(tm.Credits) AS numCredits, tms.description_defaultTranslation AS description, 
tcr.moduleSetID, tcr.numModulesToPass,   tcr.active AS ruleActive, tms.active AS moduleSetActive, 
tcr.studyPeriod, tcr.certificationRuleTypeID,   tcrt.Description AS certificationRuleType, 
tcr.activationDate,tcr.certificationRuleOrder
FROM dbo.trngCertificationRule AS tcr WITH (noLock) 
INNER JOIN dbo.trngCertificationRuleType AS tcrt WITH (noLock) ON tcrt.certificationRuleTypeID = tcr.certificationRuleTypeID 
LEFT OUTER JOIN dbo.trngModuleSet AS tms   WITH (noLock) ON tcr.moduleSetID = tms.moduleSetID 
LEFT OUTER JOIN dbo.trngModuleSetModule AS tmsm WITH (noLock) ON tmsm.moduleSetID = tms.moduleSetID 
LEFT OUTER JOIN dbo.trngModule AS tm WITH (noLock) ON tm.moduleID = tmsm.moduleID 
GROUP BY tcr.certificationRuleID, tms.description_defaultTranslation, tcr.created, tcr.numModulesToPass, tcr.moduleSetID, tcr.certificationID, tcr.active, 
tms.active, tcr.studyPeriod, tcr.certificationRuleTypeID, tcrt.Description, tcr.activationDate,tcr.certificationRuleOrder    


GO