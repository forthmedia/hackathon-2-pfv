
if exists (select 1 from information_schema.views where table_name = 'vOppSpecialDealFlag')
drop view vOppSpecialDealFlag
GO

CREATE VIEW [dbo].[vOppSpecialDealFlag]
AS
SELECT     p.SKU, op.quantity, op.subtotal AS VALUE, org.OrganisationName AS ACCOUNT, o.detail, org.OrganisationID AS ENTITYID, 
                      per.FirstName + ' ' + per.LastName AS ACCOUNT_MANAGER, c.CountryDescription AS COUNTRY, os.status AS OPPORTUNITY_STATUS, 
                      op.probability AS PRODUCT_PROBABILITY, op.forecastShipDate AS FORECAST_SHIP_DATE, org.countryID, o.opportunityID AS OPPORTUNITY_ID, DATENAME(month, 
                      op.forecastShipDate) AS Month_due, pg.Description AS Product_Group, o.currency
FROM         dbo.opportunity AS o INNER JOIN
                      dbo.opportunityProduct AS op ON op.OpportunityID = o.opportunityID INNER JOIN
                      dbo.Product AS p ON p.ProductID = op.ProductORGroupID INNER JOIN
                      dbo.ProductGroup AS pg ON pg.ProductGroupID = p.ProductGroupID INNER JOIN
                      dbo.organisation AS org ON org.OrganisationID = o.entityid INNER JOIN
                      dbo.Country AS c ON c.CountryID = org.countryID INNER JOIN
                      dbo.OppStage AS os ON os.OpportunityStageID = o.stageID INNER JOIN
                      dbo.oppPricingStatus ON o.OppPricingStatusID = dbo.oppPricingStatus.oppPricingStatusID LEFT OUTER JOIN
                      dbo.Person AS per ON per.PersonID = o.vendorAccountManagerPersonID
WHERE     (dbo.oppPricingStatus.statusTextID = 'SPQFullyApproved')

