If exists (select 1 from sysobjects where name = 'vContactHistory')
	drop view vContactHistory
GO
	
create view vContactHistory as

		/*
		2013-11-21	WAB	2013RoadMap2 Item 25 Contact History
					Combine contact history from various tables.
					Note that records which come from tables other than commDetail still have a commTypeID assigned to them (for easier filtering)
		2013-12-03 	WAB Started storing subject/body of system and adhoc emails in commDetailContent table 
		2013-12-17  WAB Added the metadata column (tells me when a system email has been sent to someone other than the personid, and also who a system email is sent from)
		2013-12-18	WAB	changed emailDef.emailTitle to emailDef.Name and gave priority over merged subject line
		*/

		SELECT 
			'commdetail' as tablename,
			cd.commDetailID as tableIdentity,
			
			cd.commTypeID,
			cdt.name + case when c.sendmessage = 1 then ' & Message' else '' end as commForm,
			cds.Name AS status, 
			case 
				when isNull(ed.Name,'') <> '' Then ed.Name  
				when cdc.commDetailID is not null then 
						/* commDetailContent is populated, get either the subject or failing that the body */ 		
						case 
							when isnull(cdc.subject,'') <> '' Then left(cdc.subject,50) + case when len(cdc.subject) > 50 then ' ...' else '' end  
							else left(cdc.body,50) + case when len(cdc.body) > 50 then ' ...' else '' end 
						end
						 
				else c.Title end 
				 + case when c.sendmessage = 1 and title.phraseText is not null then '<BR>' + title.phraseText else '' 
			end as Title,
			c.CommID, 
			c.CommTypeLID,
			p.FirstName + ' ' + p.LastName AS PersonName, 
			cd.fax as Address,
			cd.DateSent As Date, 
			isNUll(cd.lastupdated,cd.DateSent) as lastupdated, 
			cd.LastUpdatedBy,
			case 
				when ed.commid is not null then case when ed.fromAddress not like '%#%' then ed.fromAddress else '' end
				when cfdn.commfromdisplaynameid is not null then cfdn.displayName 
				when sender.personid is not null then sender.firstname + ' ' + sender.lastname
				/* This complicated CASE statement pulls the from address of a System Email*/
				when isNull(metadata,'') like '%"FROM":"%' then  
					substring (
						isNull(metadata,''),
						(charindex('"FROM":"',isNull(metadata,'')) + LEN('"FROM":"')),
						charindex ('"',isNull(metadata,''),charindex('"FROM":"',isNull(metadata,'')) + LEN('"FROM":"')+1) - (charindex('"FROM":"',isNull(metadata,'')) + LEN('"FROM":"'))
						)
				else u.name 
			end AS agent, 
			cd.personid,	
			p.locationid,	
			p.organisationid,	
			cd.commdetailid,  -- required if need to join flags
			case 
				when commCamp.campaignName is null 	then commDetCamp.campaignName
				else commCamp.campaignName 
			end as campaignName,
			null as visitID,
			cdc.metadata
			
			FROM CommDetailStatus cds 
		INNER JOIN commdetail cd ON cds.CommStatusID = cd.CommStatusID and test = 0
		left JOIN commdetailContent cdc ON cd.CommDetailID = cdc.commDetailID
		RIGHT OUTER JOIN CommDetailType cdt ON cdt.CommTypeID = cd.CommTypeID and showInCommHistory = 1
		INNER JOIN Communication c ON c.CommID = cd.CommID
		LEFT JOIN emailDef ed ON ed.emailTextID = c.title
		LEFT JOIN commFromDisplayName cfdn on cfdn.commfromdisplaynameid = c.commfromdisplaynameid
		left join vPhrases title on c.sendMessage = 1  AND title.entityID = c.commID and title.entityTypeID=61 and title.phraseTextID='messageTitle'
		
		LEFT OUTER JOIN Campaign AS commCamp ON c.CampaignID = commCamp.campaignID 
		LEFT OUTER JOIN Campaign AS commDetCamp ON cd.CommCampaignID = commDetCamp.campaignID 
		LEFT OUTER JOIN person p ON cd.PersonID = p.PersonID
		LEFT OUTER JOIN usergroup u ON u.usergroupid = cd.lastupdatedby		
		LEFT JOIN person sender on cd.commsenderid = sender.personid

		WHERE
		NOT (cd.commTypeID =2 and cd.commStatusID < 0) /* communications with commStatusID less than zero have not been sent */


	UNION
			/* Direct Messages, sent either to people or selections */
			select 
			'message' as tablename,
			m.messageid as tableIdentity,
			52 as commTypeID,  -- HardCoded! 
			'Direct Message' as commForm,
			'Sent' AS status, 
			title_defaultTranslation as title,
			0 as CommID, 
			0 as CommTypeLID,
			p.FirstName + ' ' + p.LastName AS PersonName, 
			null as Address,
			isNull(m.sendDate,m.created) As Date, 
			m.lastupdated, 
			m.LastUpdatedBy,
			u.name AS agent, 
			p.personid,
			p.locationid,	
			p.organisationid,	
			null as commdetailID,
			null as campaignName,
			null as visitID,
			null as metadata
			from message m
				left join usergroup u ON u.usergroupid = m.lastupdatedby
				left join (selection s inner join selectionTag st
							on s.selectionID = st.selectionID) on m.sendToEntityID = s.selectionID and m.sendToEntityType = 'selection' and st.status != 0
				left join person p on (p.personID = m.sendToEntityID and m.sendToEntityType = 'person') or (st.entityID = p.personID and m.sendToEntityType = 'selection')
			where 
				isNull(m.sendDate,m.created) < getDate()


	UNION
			/* Website Visits from the activities table */
			select 
			'activity' as tablename,
			activityID as tableIdentity,
			11 as commTypeID, -- hardCoded!
			'Website Visit' as commForm,
			convert(varchar,numActivities) + ' pages visited'  as status, 
			sdd.domain  as Title,
			0 as CommID, 
			0 as CommTypeLID,
			p.FirstName + ' ' + p.LastName AS PersonName, 
			null as Address,
			i.ActivityTime As Date, 
			i.ActivityTime, 
			NULL,
			null AS agent, 
			p.personid,
			p.locationid,	
			p.organisationid,	
			null as commdetailID,
			null  as campaignName,
			data as visitID,
			null as metadata

			from activityType it
					inner join
				 activity i ON it.activityType = 'PagesVisited' and it.activityTypeID = i.activityTypeID
					left join 
				sitedefdomain sdd on sdd.id = i.entityid
					left join
				person p on p.personid = i.personid		 	
