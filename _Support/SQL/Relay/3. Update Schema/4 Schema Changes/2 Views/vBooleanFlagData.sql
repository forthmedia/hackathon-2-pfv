/*
WAB 2012-10-01 alter this view so that does not use 'select *'
Also added in more metadata (such as flagGroupTextid)
NJH 2015/07/06 - added entityTypeID
*/
ALTER VIEW [dbo].[vBooleanFlagData]
AS
SELECT     f.entityTypeID,bfd.EntityID, bfd.FlagID, 
f.FlagtextID, f.FlagGroupID, f.FlagGrouptextID, f.Flag as name,
bfd.CreatedBy, bfd.Created, bfd.LastUpdatedBy, bfd.LastUpdated, bfd.lastUpdatedByPerson 

FROM         BooleanFlagData bfd 
INNER JOIN
              vflagdef f ON         bfd.FlagID = f.FlagID