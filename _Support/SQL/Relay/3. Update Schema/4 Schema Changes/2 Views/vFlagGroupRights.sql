
alter view vFlagGroupRights as
select p.personID, fg.flagGroupID,fg.entityTypeID, fg.flagGroupTextID, fg.flagTypeID,
max(case when fg.viewing = 1 or fg.ViewingAccessRights = 1 and fgr.Viewing = 1 then 1 else 0 end) as viewing,
max(case when fg.viewing = 1 or fg.ViewingAccessRights = 1 and fgr.Viewing = 1 then 1 else 0 end) as [view],
max(case when fg.edit = 1 or fg.editAccessRights = 1 and fgr.edit = 1 then 1 else 0 end) as edit,
max(case when fg.search = 1 or fg.searchAccessRights = 1 and fgr.search = 1 then 1 else 0 end) as search,
max(case when fg.download = 1 or fg.downloadAccessRights = 1 and fgr.download = 1 then 1 else 0 end) as download
from 
Person p inner join	flagGroup fg on 1 = 1 
	left join (FlagGroupRights fgr 
			inner join RightsGroup rg on rg.UserGroupID = fgr.userid) 
		on fg.flaggroupid = fgr.flaggroupid and p.PersonID = rg.PersonID
where fg.EntityTypeID  <> 7 -- filter out quizzes
group by p.PersonID, fg.FlagGroupID,fg.EntityTypeID, fg.flagGrouptextid, fg.flagTypeID

GO

