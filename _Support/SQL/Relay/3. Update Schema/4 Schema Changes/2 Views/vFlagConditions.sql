createPlaceHolderObject 'view', 'vFlagConditions'

GO

ALTER VIEW vFlagConditions
AS

/* 	WAB 2015-11   Visibility Project  
	This view is used to build conditions for putting people into usergroups
	It has been written as a view because I need to use it twice, once in a SQL upgrade script and once in CF
	In practice we only use flags of entityType 0,1,2 but the view actually returns all types
	I have only coded conditions for radio, checkbox and integer flags which link to other entities.  
		Other conditions are a bit esoteric and can always be entered manually
	Integer flags which link to Person/Location/Organisation records are dealt with slightly differently to other flags.  
		In this case we are looking in the data column of other entities flags for a reference back to this person 
		This logic is dealt with in the flagviews with columns of the form IS_myflag 
		(sorry that probably does not make sense, but at least you know what there is something odd and you can investigate further)

	WAB 2016-06-13	Fix issue with boolean Flags which do not have flagTextIDs.  The value returned in the flagGroup column is just the flagID not flag_#flagID#
					(whereas the name of a flagcolumn where the flagtextid is blank is indeed flag_#flagID#)
	 WAB 2016-12-20	PROD2016-3041 Add support for isBitField=true  (also see upgrade script in visibilityProject.sql)
*/

select	
			flagGroup,
			flag,
			flagid,
			f.entityTypeID,
			case
				-- when dataType in  ('checkbox') then entityTable + '.' + case when isnull(flagTextID,'') = '' then 'flag_' + convert(varchar,f.flagid) else flagtextid end + ' = 1 ' 
				when dataType in  ('bit') then  entityTable + '.' + flagTextID + ' = 1'
				when dataType in  ('checkbox') then 'dbo.listfind( ' + entityTable + '.' + flagGroupTextID + ' , ''' + flagtextid + ''' ) <> 0'
				when dataType in  ('radio') then entityTable + '.' + flagGroupTextID  + ' = ''' + flagTextID + ''''
				when dataType in ('integer','integermultiple') and linksToEntityTypeID in (0,1,2) then linkedEntity.entityname + '.IS_' + flagTextID + ' = 1'
				when dataType in ('integer','integermultiple') and linksToEntityTypeID not in (0,1,2) then entityTable + '.' + flagTextID   + ' is not null '

				else '' 
			end as Condition,
			case
				when dataType in  ('bit') then  flag + ' is set'
				when dataType in  ('checkbox','radio') then flag + ' is set'
				when dataType in ('integer','integermultiple') and linksToEntityTypeID in (0,1,2) then upper(left (linkedEntity.entityname,1)) + right(linkedEntity.entityname,len(linkedEntity.entityname)-1) + ' is a ' + flag 
				when dataType in ('integer','integermultiple') and linksToEntityTypeID not in (0,1,2) then flag + ' has a value '
				else '' 
			end as description
			
		from 
				(select 
					flagid
					, flag
					, flagGroup
					, case when isnull(f.flagGroupTextID,'') = '' then 'flaggroup_' + convert(varchar,f.flaggroupid) else f.flagGrouptextid end as flagGroupTextID
					/* 
						explanation of:  when dataType not in  ('checkbox','radio') ..
						flagTextID is used for two things: for non booleans it is the name of the column in the flag views, for booleans it is the value in the flagGroup column 
						if there is no flagTextID then the columnNames become flag_#flagID# whereas the values in the flagGroupColumn are just #flagid#
					*/ 
					, case when isnull(flagTextID,'') = '' then case when dataType not in  ('checkbox','radio') then 'flag_' else '' end + convert(varchar,flagid) else flagtextid end as flagtextid
					, case when dbo.getParameterValue (fg.formattingParameters,'isBitField',',') = 'true' then 'bit' else datatype end as dataType
					, entitytable
					, f.entityTypeID
					, linkstoentityTypeID
				from 
					vflagdef f
						inner join
					flagGroup fg on f.flagGroupID = fg.flagGroupID
				) as f 
				
				left join
			schemaTable linkedEntity on	linkedEntity.entityTypeID = f.linksToEntityTypeID


