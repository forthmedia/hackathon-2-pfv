alter  view [dbo].[vFlagGroupsUsedInSelections]
AS
/*
WAB 2007/02/28 Modified to handle integerMultiple and Text
WAB 2011/04/10 Problem with the query when running on some systems - only occurred when filtering by entityTypeID.  
			   Couldn't work out exact reason (possibly something to do with SQL Server 10) 
			   Restructured second UNION to use a sub-query rather than a join with lots of ORs
NJH 2013/05/15 - Added support for textMultiple and decimal flags
*/



select distinct selectionfield, 0 as flagID, fg.name, flagGroupid , flagTypeID , parentFlagGroupID, entityTypeID, sc.createdby
from selectionCriteria sc
inner join flagGroup fg on 
	(SelectionField  like 'frmFlag_radio%' and fg.flagGroupID = replace(selectionfield,'frmFlag_radio_',''))
		oR
	(SelectionField  like 'frmFlag_checkbox%' and fg.flagGroupID = replace(selectionfield,'frmFlag_checkbox_',''))
where 
sc.created > dateadd(dd,-300,getDate())


union

select distinct sc.selectionfield, f.flagID, f.name, f.flagGroupID, flagTypeID ,parentFlagGroupID, entityTypeID, sc.createdby
from 

	(select replace(replace(replace(replace(replace(selectionfield,'frmFlag_Text_',''),'frmFlag_integer_',''),'frmFlag_integerMultiple_',''),'frmFlag_textMultiple_',''),'frmFlag_decimal_','') as flagid, selectionfield, createdby, created
		from selectioncriteria
		where selectionfield like 'frmFlag_integerMultiple%'
			or SelectionField like 'frmFlag_integer[_]%'
			or SelectionField like 'frmFlag_textMultiple%'
			or SelectionField like 'frmFlag_text%'
			or SelectionField like 'frmFlag_decimal%') as sc

inner join flag f on 
sc.flagid = f.flagid
inner join flagGroup fg on f.flagGroupID = fg.flagGroupID
where 
sc.created > dateadd(dd,-300,getDate())



