/* 
2015-12-22	PYW		P-TAT006 BRD 31 add trngPersonCertification.expiryDate
*/

ALTER VIEW [dbo].[vTrngCertificationSummary] AS

SELECT DISTINCT 
	v.code, v.SortOrder, v.title, v.description, tpc.registrationDate AS registration_date, tpc.passDate AS pass_date, 
	DATEADD(mm, v.duration, tpc.passDate) AS expiry_date, tpcs.description AS status, 
		--SUM(v.numModulesToPass) AS num_modules_to_pass, CASE 432093 NJH 2013/04/04 replace with below to get correct value 
      /* 2013-04-30 YMA Enhance performance of the view. */
      isnull(case when tpcs.statusTextId = 'passed' then num_modules_to_pass.numModulesToPass else SUM(v.numModulesToPass) end,0) as num_modules_to_pass,		/* 2013/10/01 PPB Case 437223 use status rather than passdate */
      isnull(moduleProgress.numModulesPassed,0) as Modules_Passed,
      /* (select sum(numModulesToPass) from (
            select distinct certificationRUleID,certificationid,numModulesToPass from vTrngPersonCertifications with(nolock)
            WHERE certificationID = v.certificationID and
		personID = tpc.personID and ruleactive=1) numMods
            group by certificationid
      ) as num_modules_to_pass,
	(
	SELECT  COUNT(DISTINCT moduleID) AS Expr1
      FROM    dbo.vTrngPersonCertifications AS vtpc with(nolock)
	WHERE   certificationID = v.certificationID AND userModuleStatus = 'Passed' 
			AND personID = tpc.personID and ruleactive=1
      ) AS Modules_Passed, */
	/* 2015-12-22 PYW P-TAT008 BRD 31 add trngPersonCertification.expiryDate */
	v.certificationID, tpc.personID, tpc.personCertificationID, tpc.expiredDate AS expired_date, tpc.expiryDate AS personCertificationExpiryDate,
	v.certificationTypeID, tpcs.statusTextID
FROM dbo.vTrngCertifications AS v with(nolock) INNER JOIN
     dbo.trngPersonCertification AS tpc WITH (noLock) ON tpc.certificationID = v.certificationID INNER JOIN
     dbo.trngPersonCertificationStatus AS tpcs WITH (noLock) ON tpcs.statusID = tpc.statusID LEFT OUTER JOIN
      /* 2013-04-30 YMA Enhance performance of the view. */
      (select tpcx.personCertificationID, sum(tcrx.numModulesToPass) as numModulesToPass  /* 2013-12-09 PPB Case 438196 issue1 change tpcrx.numModulesToPass to tcrx.numModulesToPass  */
            from trngPersonCertification tpcx with (noLock)
            join trngPersonCertificationRule tpcrx with (noLock) on tpcrx.personCertificationID = tpcx.personCertificationID
 			inner join trngCertificationRule tcrx WITH (NOLOCK) ON tcrx.CertificationRuleID = tpcrx.CertificationRuleID	 	/* 2013-09-02 PPB Case 436827 added join to trngCertificationRule to use active from there rather than trngPersonCertificationRule */
  			/* 2013/09/30 PPB Case 437223 join to trngCertificationRule on CertificationRuleID rather than CertificationID to avoid dup rows */
            where  tcrx.active=1
				/* 2013-05-14 STCR Case 432193 respect the activationDate at the time when the rule content was taken */
				and GETDATE() >= dbo.dateAtMidnight(tpcrx.activationDate)		/* 2013-12-09 PPB Case 438196 issue1 changed tpcrx.lastUpdated to GETDATE */
            group by tpcx.personCertificationID
            ) as num_modules_to_pass on num_modules_to_pass.personCertificationID = tpc.personCertificationID LEFT OUTER JOIN
      (select tump.personID, tc.certificationID, count(statustextID) numModulesPassed from 
            dbo.trngCertification AS tc WITH (noLock)
            LEFT OUTER JOIN dbo.trngCertificationRule AS tcr WITH (noLock) ON tc.certificationID = tcr.certificationID 
            LEFT OUTER JOIN dbo.trngModuleSet AS tms WITH (noLock) ON tcr.moduleSetID = tms.moduleSetID 
            LEFT OUTER JOIN dbo.trngModuleSetModule AS tmsm WITH (noLock) ON tms.moduleSetID = tmsm.moduleSetID 
            LEFT OUTER JOIN dbo.trngModule AS tm WITH (noLock) ON tmsm.moduleID = tm.moduleID 
            LEFT OUTER JOIN dbo.trngUserModuleProgress AS tump WITH (noLock) 
            INNER JOIN dbo.trngUserModuleStatus AS tums WITH (noLock) ON tump.moduleStatus = tums.statusID ON tump.moduleID = tm.moduleID
            where statustextID = 'passed'
            and tcr.active = 1 
            and tms.active = 1 			/* 2013-12-09 PPB Case 438196 issue2 added extra filter */
            AND getDate() >= dbo.dateAtMidnight(tcr.activationDate)
            group by tump.personID, tc.certificationID) as moduleProgress on tpc.personID = moduleProgress.personID and tpc.certificationID = moduleProgress.certificationID
            /* 2013-05-14 STCR Case 432193 num_modules_to_pass and numModulesPassed should consider Certification Rule activation date */
WHERE (v.ruleActive = 1 AND getDate() >= dbo.dateAtMidnight(v.ruleActivationDate)) 
/* 2015-12-22 PYW P-TAT008 BRD 31 add trngPersonCertification.expiryDate */
GROUP BY v.code, v.SortOrder, v.title, v.description, tpc.registrationDate, tpc.passDate, tpcs.description, v.certificationID, tpc.personID, v.duration, tpc.expiredDate, tpc.expiryDate,
                      tpc.personCertificationID, v.certificationTypeID, tpcs.statusTextID,num_modules_to_pass.numModulesToPass,moduleProgress.numModulesPassed /* 2013-04-30 YMA Enhance performance of the view. */

