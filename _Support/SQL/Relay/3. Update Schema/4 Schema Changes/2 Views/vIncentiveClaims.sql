ALTER              view [dbo].[vIncentiveClaims] 

AS
select p.personid as PersonID, p.firstname + ' ' + p.lastname as Name, 
l.sitename as Company, l.locationid as LocationID, rwti.rwtransactionID as ClaimID, 
prod.description as Product, pg.description as ProductGroup, rwti.quantity as Quantity,
rwti.pointstype as pointstype, rwti.endusercompanyname as EndUser, o.organisationname as Disti,  
c.countryid, rwti.points as Points, pc.productcategoryname as Category, rwti.invoiceNumber as Invoice,
c.countrydescription as Country, rwt.created as Created, rwt.accountID as AccountID, 
CASE WHEN rwt.RWTransactionTypeID = 'AC' THEN 'Yes - Rewards'
     WHEN rwt.RWTransactionTypeID = 'CL' THEN 'No - Rewards'
     WHEN rwt.RWTransactionTypeID = 'LA' THEN 'Yes - League'
     WHEN rwt.RWTransactionTypeID = 'LS' THEN 'No - League'
END as verified,(dbo.BooleanFlagList (743,p.organisationID,default)) as Flagdata, f.name as Flag 
from rwtransaction rwt
inner join person p on p.personid = rwt.personid
inner join location l on l.locationid = p.locationid
inner join country c on c.countryid = l.countryid
inner join rwtransactionitems rwti on rwti.rwtransactionid = rwt.rwtransactionid
inner join product prod on prod.productid = rwti.productID
inner join productgroup pg on pg.productgroupid = prod.productgroupid
left join productCategory pc ON pc.productCategoryID = pg.productCategoryID
left outer join organisation o on o.organisationid = rwti.distiorganisationid
left outer join productflag pf on pf.productid = prod.productid
left outer join flag f on f.flagid = pf.flagid
where rwt.RWTransactionTypeID in ('AC','CL','LA','LS')
and p.personid in (select distinct personid from rwpersonaccount where testaccount = 0 and accountdeleted = 0)
and p.organisationID not in (select organisationID from vOrgsToSupressFromReports) 
