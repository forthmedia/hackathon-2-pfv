/****** Object:  View [dbo].[vTrngCourseSummary]    Script Date: 07/12/2012 19:03:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vTrngCourseSummary] AS
SELECT DISTINCT 'phr_title_TrngCourse_' + CONVERT(VarChar, dbo.trngCourse.courseId) as AICCCourseTitle,
                      ISNULL
                          ((SELECT     COUNT( distinct UserModuleProgressID)
                              FROM         trngUserModuleProgress inner join trngModule on
                                   trngUserModuleProgress.ModuleID = trngModule.ModuleID inner join 
												trngModuleCourse on trngModule.ModuleID=trngModuleCourse.moduleID and trngModuleCourse.CourseID = trngCourse.CourseID
										where 
                                                    trngUserModuleProgress.userModuleFulfilled is not null), 0) AS totalCompletedModules, 
						ISNULL
                          ((SELECT     COUNT(distinct UserModuleProgressID)
                              FROM         trngUserModuleProgress inner join trngModule on
                                   trngUserModuleProgress.ModuleID = trngModule.ModuleID inner join 
												trngModuleCourse on trngModule.ModuleID=trngModuleCourse.moduleID and trngModuleCourse.CourseID = trngCourse.CourseID
										where 
                                                    trngUserModuleProgress.userModuleFulfilled is null), 0) AS totalStartedModules,
                          (SELECT     COUNT(distinct m.moduleID)
                            FROM          trngmodule m
                            inner join trngModuleCourse tmc on tmc.moduleId = m.moduleID
                            WHERE      m.moduleID NOT IN
                                                       (SELECT     moduleID
                                                         FROM          trngUserModuleProgress) AND tmc.courseID = trngCourse.CourseID) AS totalNonStartedModules, 
						ISNULL
                          ((SELECT     SUM(cast(QuizTimeSpent AS integer))
                              FROM     trngUserModuleProgress inner join trngModule on
                                   trngUserModuleProgress.ModuleID = trngModule.ModuleID inner join 
												trngModuleCourse on trngModule.ModuleID=trngModuleCourse.moduleID and trngModuleCourse.CourseID = trngCourse.CourseID), 0) AS totalTimeQuiz 
FROM         dbo.trngModule INNER JOIN
					dbo.trngModuleCourse tm on tm.moduleID = dbo.trngModule.moduleID INNER JOIN
                      dbo.trngCourse ON tm.courseID = dbo.trngCourse.courseId INNER JOIN
                      dbo.trngUserModuleProgress ON dbo.trngModule.moduleID = dbo.trngUserModuleProgress.moduleID
--WHERE     (dbo.trngUserModuleProgress.CountryID <> ' ')


GO


