/****** Object:  View [dbo].[vConnectorQueueError]    Script Date: 04/11/2014 14:08:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorQueueError]'))
DROP VIEW [dbo].[vConnectorQueueError]
GO

/****** Object:  View [dbo].[vConnectorQueueError]    Script Date: 04/11/2014 14:08:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vConnectorQueueError]
AS
SELECT     dbo.connectorQueueError.ID, dbo.connectorQueue.ID AS queueID, dbo.connectorObject.connectorType, dbo.connectorObject.object, case when dbo.connectorObject.relayware = 1 then 'E' else 'I' end as direction, dbo.connectorQueue.objectID, 
                      dbo.connectorQueue.modifiedDateUTC, dbo.connectorQueue.synchAttempt, dbo.connectorQueue.isDeleted, dbo.connectorQueueError.message, dbo.connectorQueueError.field, 
                      dbo.connectorQueueError.value, dbo.connectorQueueError.statusCode, dbo.connectorQueueError.created,  dbo.connectorQueueError.connectorResponseID
FROM         dbo.connectorQueue INNER JOIN
                      dbo.connectorObject ON dbo.connectorQueue.connectorObjectID = dbo.connectorObject.ID INNER JOIN
                      dbo.connectorQueueError ON dbo.connectorQueueError.connectorQueueID = dbo.connectorQueue.ID
GO