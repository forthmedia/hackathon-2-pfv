createplaceholderobject 'view', 'vProductDetails'

GO

ALTER VIEW [dbo].[vProductDetails] AS
SELECT  Product.SKU, 
        isNull(Product.DiscountPrice,0) as DiscountPrice, 
        Product.ProductID, 
        Product.DistiDiscount,
        Product.ResellerDiscount,
        promotion.PromoID, 
        Product.Description, 
        Product.CountryID_notused, 
        Product.CampaignID, 
        Product.Deleted, 
        isNull(Product.ListPrice,0) as ListPrice, 
        Product.SKUGroup, 
        Product.PriceISOCode,
        Product.hasCountryScope
FROM Product LEFT OUTER JOIN
    promotion ON 
    Product.CampaignID = promotion.CampaignID
WHERE (Product.Deleted = 0) AND (Product.SKU IS NOT NULL)


GO