if exists (select 1 from information_schema.views where table_name = 'vRWPromotionProducts')
drop view vRWPromotionProducts
GO

CREATE VIEW [dbo].[vRWPromotionProducts]
AS
SELECT     dbo.RWPromotionScope.RWPromotionID, dbo.Product.ProductID, dbo.Product.SKU
FROM         dbo.RWPromotionScope INNER JOIN
                      dbo.productGroupCategory ON dbo.RWPromotionScope.EntityID = dbo.productGroupCategory.productCategoryID INNER JOIN
                      dbo.Product ON dbo.productGroupCategory.productGroupID = dbo.Product.ProductGroupID
WHERE     (dbo.RWPromotionScope.Entity = N'ProductCategoryID')
UNION
SELECT     RWPromotionScope_2.RWPromotionID, Product_2.ProductID, Product_2.SKU
FROM         dbo.RWPromotionScope AS RWPromotionScope_2 INNER JOIN
                      dbo.Product AS Product_2 ON RWPromotionScope_2.EntityID = Product_2.ProductGroupID
WHERE     (RWPromotionScope_2.Entity = N'ProductGroupID')
UNION
SELECT     RWPromotionScope_1.RWPromotionID, Product_1.ProductID, Product_1.SKU
FROM         dbo.RWPromotionScope AS RWPromotionScope_1 INNER JOIN
                      dbo.Product AS Product_1 ON RWPromotionScope_1.EntityID = Product_1.ProductID
WHERE     (RWPromotionScope_1.Entity = N'ProductID')
GO
