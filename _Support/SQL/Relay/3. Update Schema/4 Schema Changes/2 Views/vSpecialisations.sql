DROP VIEW vSpecialisations
GO

CREATE VIEW vSpecialisations
AS
SELECT     s.SpecialisationID,
                          (SELECT     COUNT(SpecialisationRuleID) AS Expr1
                            FROM          dbo.SpecialisationRule AS sr
                            WHERE      (specialisationID = s.SpecialisationID)) AS numRules,
                          (SELECT     COUNT(SpecialisationRuleID) AS Expr1
                            FROM          dbo.SpecialisationRule AS sr
                            WHERE      (specialisationID = s.SpecialisationID) AND (active = 1)) AS numRulesToPass, s.active, s.Code, dbo.Country.CountryDescription AS Country, s.countryID, 
                      s.TrainingProgramID
FROM         dbo.Specialisation AS s WITH (noLock) INNER JOIN
                      dbo.Country ON s.countryID = dbo.Country.CountryID

GO