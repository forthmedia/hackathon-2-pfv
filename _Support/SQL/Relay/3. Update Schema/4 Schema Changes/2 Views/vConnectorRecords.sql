/* NJH/WAB 2016/03/22 - a number of changes to the view to improve performance. Moved 'Admin' queries from join to sub-selects. Create connector settings as a table
	value function. Speeds it up dramatically */

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorRecords]'))
DROP VIEW [dbo].[vConnectorRecords]
GO

/* don't synch any records where the following are true:
	1. Max synch attempts reached
	2. Record has errored
	3. There are dependancies in the queue 
	4. It is an admin person/company
	5. The record has been flag as suspended
*/
CREATE VIEW [dbo].[vConnectorRecords]
AS
SELECT     distinct q.ID as queueID, q.isDeleted, q.object, q.objectID, q.connectorType, q.connectorResponseID,
	name, 
	q.synchAttempt,q.process,q.direction,q.entityTypeID, 
	relaywareID,
	remoteID,
	q.modifiedDateUTC as lastModified,
	case when error.ID is not null then error.created else q.created end as queueDateTime,
	case 
		when q.flagID is not null then 'Suspended'
		when q.synchAttempt >= isNull(synchAttemptSetting.value,5) then 'MaxSynchAttemptsReached'
		when not (q.errored = 0) then 'Errored'
		when not (q.dataExists = 1) then 'DeletedData'
		when queueDependency.dependentOnConnectorQueueID IS NOT NULL then 'HasDependency'
		when isNull((
					select typetextid from organisationtype where organisationtypeid in (
							case 
								when q.object = 'person'  then (select accounttypeid from location l inner join person p on p.locationid = l.locationid where p.personid = relaywareid) 
								when q.object = 'location'  then (select accounttypeid from location l where l.locationid = relaywareid) 
								when q.object = 'organisation'  then (select organisationtypeid from organisation o where o.organisationid = relaywareid) 
							else null end)
					
					),'') = 'AdminCompany' then 'IsAdminCompany'
		else 'Synching'
	end as queueStatus,
	case when error.ID is not null then 1 else 0 end as hasError,
	case when error.ID is not null then error.message end as errorMessage                
FROM
	vConnectorQueueResolvedEntityIDs q
		left join dbo.getConnectorMaxSynchAttempts() synchAttemptSetting on 1=1
		left join connectorQueueDependency AS queueDependency ON queueDependency.connectorQueueID = q.ID
		left join connectorQueueError as error on error.connectorQueueID = q.ID
GO
