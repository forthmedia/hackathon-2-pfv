

if exists (select 1 from sysobjects where name = 'vDataLoadMappingAll' and type = 'v')
drop view  vDataLoadMappingAll 

GO

CREATE  view [dbo].[vDataLoadMappingAll] as
SELECT     

dl.datasourceID, dl.load_Table, LT.column_name AS loadTableColumnName, dataloadmappingid, LT.data_type, 
                      CASE WHEN dlm.MappingType IS NOT NULL 
                      THEN dlm.mappingType WHEN rc.loadThisColumn = 1 THEN 'POL-default' WHEN controlcolumn = 1 THEN 'Control Column' ELSE 'UnMapped' END AS mappingtype_,
                       dlm.LoadData, rc.dataLoadColumnName, dlm.mappedto, rc.relayColumn, 
			relayTableSchema.is_nullable as relayColumn_IsNullable,
			rc.loadThisColumn, rc.ControlColumn, automapped, loadeddate
FROM         dbo.vDataLoads dl INNER JOIN
                      information_SCHEMA.columns LT ON dl.load_table = LT.table_name LEFT OUTER JOIN
                      dbo.vdataLoadMappingDetail dlm ON dlm.dataloadid = dl.datasourceID AND LT.column_name = dlm.LoadTableColumn LEFT OUTER JOIN
                      dataLoadCol rc ON ISNULL(dlm.mappedto, LT.column_name) = rc.dataLoadColumnName
		left join information_SCHEMA.columns relayTableSchema on  relaycolumn is not null and relayTableSchema.table_name = substring(relaycolumn,1,charindex('.',relaycolumn)-1) and relayTableSchema.column_name = substring(relaycolumn,charindex('.',relaycolumn)+1,100)  

GO