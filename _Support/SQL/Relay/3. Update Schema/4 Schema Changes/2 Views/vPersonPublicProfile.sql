/****** Object:  View [dbo].[vPersonPublicProfile]    YMA	2013/01/24	Sprint 2 Item 118 - Person Public Profiles ******/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vPersonPublicProfile]'))
DROP VIEW [dbo].[vPersonPublicProfile]
GO

CREATE VIEW [dbo].[vPersonPublicProfile]
AS
SELECT     p.personID,
					  CASE WHEN p.publicProfileName IS NULL OR p.publicProfileName = '' THEN p.firstname + ' ' + p.lastname ELSE p.publicProfileName END AS publicProfileName, p.publicProfileAbout, 
                      CASE WHEN p.publicProfileJobDesc IS NULL THEN p.jobDesc ELSE p.publicProfileJobDesc END AS publicProfileJobDesc, CASE WHEN p.publicProfileLocation IS NULL
                      	THEN CASE WHEN l.address5 != '' AND l.address5 IS NOT NULL 
                      THEN l.address5 + ', ' + c.localcountrydescription ELSE c.localcountrydescription END ELSE p.publicProfileLocation END AS publicProfileLocation, 
                      isNull(p.pictureURL,'/images/social/icon_no_photo_80x80.png') as pictureURL
FROM         dbo.Person AS p INNER JOIN
                      dbo.Location AS l ON p.LocationID = l.LocationID INNER JOIN
                      dbo.Country AS c ON l.CountryID = c.CountryID
GO