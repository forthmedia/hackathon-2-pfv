/* 2016/03/23			NJH			Sugar 448762 - added created as a date filter */
ALTER VIEW [dbo].[vLeadListingCalculatedBudget]
AS
SELECT     dbo.organisation.OrganisationName AS Account, opp.lastupdated AS last_Updated, dbo.OppStage.OpportunityStage AS Stage, opp.stageID, 
                      'phr_OppStage_' + dbo.OppStage.stageTextID AS StagePhraseTextID,
                          (SELECT     SUM(subtotal) AS Expr1
                            FROM          dbo.opportunityProduct
                            WHERE      (OpportunityID = opp.opportunityID)) AS calculated_budget,
                          (SELECT     SUM(unitPrice * quantity) AS Expr1
                            FROM          dbo.opportunityProduct AS opportunityProduct_3
                            WHERE      (OpportunityID = opp.opportunityID)) AS calculated_budget_fullPrice,
                          (SELECT     SUM(subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS opportunityProduct_2
                            WHERE      (OpportunityID = opp.opportunityID) AND (DATEPART(q, opp.expectedCloseDate) = DATEPART(q, GETDATE())) AND (DATEPART(yy, 
                                                   opp.expectedCloseDate) = DATEPART(yy, GETDATE()))) AS calculated_budget_this_quarter, opp.budget * 0.05 AS rebate, opp.OppTypeID,
                          (SELECT     COUNT(1) AS Expr1
                            FROM          dbo.opportunityProduct AS opportunityProduct_1
                            WHERE      (OpportunityID = opp.opportunityID) AND (specialPrice > 0) AND (specialPriceApproverPersonID IS NULL)) AS unapproved_special_price, 
                      opp.budget as base_budget, opp.estimatedBudget AS overall_customer_budget, opp.forecast, opp.probability, opp.currency, p1.FirstName + ' ' + p1.LastName AS Account_Manager, 
                      p2.FirstName + ' ' + p2.LastName AS partner_Sales_Person, opp.distiLocationID, 
                      CASE WHEN oppPrice.oppPricingStatus = 'SPQ Fully Approved' THEN opp.SPEXPIRYDATE ELSE NULL END AS SPEXPIRYDATE,
                          (SELECT     COUNT(1) AS Expr1
                            FROM          dbo.actions
                            WHERE      (entityTypeID = 23) AND (entityID = opp.opportunityID)) AS Number_of_Actions, opp.entityid, opp.sponsorPersonid, opp.sourceID, opp.campaignID, 
                      opp.detail, opp.statusID AS status_ID, dbo.OppStage.status, dbo.OppStage.OpportunityStageID AS OppStageID, dbo.OppStage.statusSortOrder, 
                      dbo.OppStage.liveStatus, dbo.OppStage.onlyShowInQuarter, dbo.OppStage.probability AS OppStageProbability, partnerlocation.SiteName AS Partner_Name, 
                      opp.flagid, opp.opportunityID AS Opportunity_ID, opp.currentSituation, CONVERT(varchar(3), RTRIM(DATENAME(m, opp.expectedCloseDate))) 
                      + '-' + SUBSTRING(CONVERT(varchar(4), DATEPART(yy, opp.expectedCloseDate)), 3, 2) AS month_expected_close, 'Q' + CONVERT(varchar(3), RTRIM(DATENAME(q, 
                      opp.expectedCloseDate))) + '-' + SUBSTRING(CONVERT(varchar(4), DATEPART(yy, opp.expectedCloseDate)), 3, 2) AS Quarter_expected_close, 
                      partnerlocation.LocationID AS PartnerLocationID, opp.expectedCloseDate, opp.vendorSalesManagerPersonID, opp.vendorAccountManagerPersonID, 
                      opp.partnerSalesManagerPersonID, opp.partnerSalesPersonID, dbo.organisation.OrganisationID AS AccountLocationID,
                          (SELECT     TOP (1) c2.CountryDescription
                            FROM          dbo.Country AS c1 INNER JOIN
                                                   dbo.CountryGroup AS cg ON cg.CountryMemberID = c1.CountryID INNER JOIN
                                                   dbo.Country AS c2 ON c2.CountryID = cg.CountryGroupID
                            WHERE      (c2.ISOCode IS NULL) AND (c1.CountryID = opp.countryID) AND (c2.CountryDescription NOT IN ('all countries', 'europe'))) AS Region, opp.countryID, 
                      dbo.Country.ISOCode, dbo.Country.CountryDescription AS country, oppPrice.oppPricingStatus AS pricing_status, opp.OppPricingStatusID, opp.SPApproverPersonIDs, 
                      opp.SPRequiredApprovalLevel, opp.SPCurrentApprovalLevel, opp.SPApprovalNumber, l.SiteName AS Distributor, MAX(oph.created) AS SPQ_Last_Updated, 
                      p1.Email,
                      /* P-PSI001 - RMB - 2013-06-21 - Added CASE 'OppStatus' AND CASE 'OppType' */
                      CASE
						WHEN OPS.statusTextID IS NULL THEN 'phr_notAppliedForDealReg'
						ELSE 'phr_opp_' + OPS.statusTextID END AS 'OppStatus',
                      CASE
						WHEN OPT.oppTypeTextID IS NULL THEN 'phr_OppTypeNotKnown'
						ELSE 'phr_opp_' + OPT.oppTypeTextID END AS 'OppType',
					ops.statusTextID as oppStatusTextID, opt.oppTypeTextID,
					opp.created
FROM
                      dbo.opportunity opp 
                      INNER JOIN dbo.organisation ON dbo.organisation.OrganisationID = opp.entityid		/* 2013-01-10 PPB Case 438445 changed to inner join and moved the ON statement up from below */
                      INNER JOIN dbo.Country ON opp.countryID = dbo.Country.CountryID
                      INNER JOIN dbo.oppPricingStatus AS oppPrice ON oppPrice.oppPricingStatusID = opp.OppPricingStatusID
                      INNER JOIN dbo.OppStage ON opp.stageID = dbo.OppStage.OpportunityStageID
					  LEFT OUTER JOIN dbo.Person p1 ON p1.PersonID = opp.vendorAccountManagerPersonID	/* 2013-01-10 PPB Case 438445 changed to a LEFT join (from a RIGHT one) and moved the ON statement up from below (aethestic only) */
                      LEFT OUTER JOIN OppStatus AS OPS ON OPS.StatusID = ISNULL(opp.StatusID, 0)
                      LEFT OUTER JOIN OppType AS OPT ON OPT.OppTypeID = opp.OppTypeID
                      LEFT OUTER JOIN dbo.Location AS l ON l.LocationID = opp.distiLocationID
                      LEFT OUTER JOIN dbo.oppPricingStatusHistory AS oph ON oph.opportunityID = opp.opportunityID
                      LEFT OUTER JOIN dbo.Person AS p3 ON opp.sponsorPersonid = p3.PersonID 
                      LEFT OUTER JOIN dbo.Person AS p2 ON opp.partnerSalesPersonID = p2.PersonID
                      LEFT OUTER JOIN dbo.location AS partnerlocation ON partnerlocation.LocationID = opp.partnerLocationID
WHERE     (dbo.OppStage.status <> 'delete')
GROUP BY dbo.organisation.OrganisationName, opp.lastupdated, dbo.OppStage.OpportunityStage, opp.stageID, opp.opportunityID, opp.budget, opp.probability, opp.currency, 
                      p1.FirstName, p1.LastName, p2.FirstName, p2.LastName, opp.distiLocationID, opp.SPExpiryDate, opp.entityid, opp.sponsorPersonid, opp.sourceID, opp.campaignID, 
                      opp.detail, opp.statusID, dbo.OppStage.status, dbo.OppStage.OpportunityStageID, dbo.OppStage.statusSortOrder, dbo.OppStage.liveStatus, 
                      dbo.OppStage.onlyShowInQuarter, partnerlocation.SiteName, opp.flagid, opp.currentSituation, opp.expectedCloseDate, partnerlocation.LocationID, 
                      opp.vendorSalesManagerPersonID, opp.vendorAccountManagerPersonID, opp.partnerSalesManagerPersonID, opp.partnerSalesPersonID, 
                      dbo.organisation.OrganisationID, opp.countryID, dbo.Country.ISOCode, dbo.Country.CountryDescription, oppPrice.oppPricingStatus, opp.OppPricingStatusID, 
                      opp.SPApproverPersonIDs, opp.SPRequiredApprovalLevel, opp.SPCurrentApprovalLevel, opp.SPApprovalNumber, l.SiteName, opp.OppTypeID, p1.Email, 
                      dbo.OppStage.probability, dbo.OppStage.stageTextID, opp.estimatedBudget, opp.forecast,
                      /* P-PSI001 - RMB - 2013-06-21 - Added CASE 'OppStatus' AND CASE 'OppType' */
                      CASE
						WHEN OPS.statusTextID IS NULL THEN 'phr_notAppliedForDealReg'
						ELSE 'phr_opp_' + OPS.statusTextID END ,
                      CASE
						WHEN OPT.oppTypeTextID IS NULL THEN 'phr_OppTypeNotKnown'
						ELSE 'phr_opp_' + OPT.oppTypeTextID END,
						ops.statusTextID,opt.oppTypeTextID,
						opp.created
GO