/****** Object:  View [dbo].[vBudgetAllocations]    Script Date: 04/18/2011 12:50:04 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vBudgetAllocations]'))
DROP VIEW [dbo].[vBudgetAllocations]
GO
/****** Object:  View [dbo].[vBudgetAllocations]    Script Date: 04/18/2011 12:50:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBudgetAllocations]
AS
SELECT     bpa.budgetPeriodAllocationID, bpa.budgetID, bpa.budgetPeriodID, bpa.amount, bpa.created, bpa.createdBy, bpa.lastUpdated, bp.description AS budgetPeriod, 
                      b.description AS budget, bh.description AS budgetgroup, bh.CountryID, 'phr_Edit' AS Edit, 
                      CASE WHEN b.entityTypeID = 2 THEN 'Organisation' WHEN b.entityTypeID = 3 THEN 'Country' WHEN entityTypeID = 4 THEN 'Region' END AS budgetType, 
                      CASE WHEN b.entityTypeID = 2 THEN OrganisationName WHEN b.entityTypeID IN (3, 4) THEN Countrydescription END AS entityName, bpa.amount - ISNULL
                          ((SELECT     SUM(amount) AS Expr1
                              FROM         dbo.fundRequestActivityApproval
                              WHERE     (budgetPeriodAllocationID = bpa.budgetPeriodAllocationID)), 0) AS budgetRemaining, ISNULL
                          ((SELECT     SUM(amount) AS Expr1
                              FROM         dbo.fundRequestActivityApproval AS fundRequestActivityApproval_1
                              WHERE     (budgetPeriodAllocationID = bpa.budgetPeriodAllocationID)), 0) AS budgetApproved, 
                      CASE WHEN bpa.authorised = 1 THEN 'Yes' ELSE 'No' END AS allocationAuthorised, b.BudgetGroupID, 
                      dbo.Person.FirstName + ' ' + dbo.Person.LastName AS LastUpdatedByFullName, bpa.lastUpdatedBy
FROM         dbo.BudgetPeriodAllocation AS bpa INNER JOIN
                      dbo.BudgetPeriod AS bp ON bpa.budgetPeriodID = bp.budgetPeriodID INNER JOIN
                      dbo.Budget AS b ON b.budgetID = bpa.budgetID LEFT OUTER JOIN
                      dbo.Country ON b.entityTypeID IN (3, 4) AND b.entityID = dbo.Country.CountryID LEFT OUTER JOIN
                      dbo.organisation ON b.entityTypeID = 2 AND b.entityID = dbo.organisation.OrganisationID INNER JOIN
                      dbo.BudgetGroup AS bh ON b.BudgetGroupID = bh.BudgetGroupID LEFT OUTER JOIN
                      dbo.Person ON bpa.lastUpdatedBy = dbo.Person.PersonID
GO
