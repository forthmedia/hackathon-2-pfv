
/* this view is a candidate for removal. it is used for the old locator which should be phased out by now. I'm just not sure it currently is, so have left it in for now */
if exists (select 1 from information_schema.views where table_name='vLocatorResellers')
drop view vLocatorResellers
GO

CREATE VIEW [dbo].[vLocatorResellers]
AS
SELECT DISTINCT 
                      l.LocationID, l.Address1, l.Address2, l.Address3, l.Address4, l.Address5, l.PostalCode, l.CountryID, o.OrganisationID, l.SiteName, l.Telephone, l.Fax, 
                      l.FaxStatus, l.SpecificURL, l.Direct, l.Active, l.MatchName, l.MatchTel, l.Note, l.CreatedBy, l.Created, l.LastUpdatedBy, l.LastUpdated, l.DupeChuck, 
                      l.DupeGroup, l.ActualCountry, l.ssgCompanyID, l.EmailDomain, l.R1LocID, l.R1ParentID, l.address7, l.address8, l.address9, l.nSiteName, l.nAddress1, 
                      l.nAddress2, l.nAddress3, l.nAddress4, l.nAddress5, l.CompanyLevel, l.accountTypeID, l.R2LocID, l.R2OrgID, l.Latitude, l.Longitude, l.address6, 
                      o.OrganisationName, o.OrgURL, o.AKA, o.orgemail, prov.Name AS provinceName, 
                      CASE WHEN partnerLevel.flagID = 1852 THEN 1 WHEN partnerLevel.flagID = 1851 THEN 2 ELSE 3 END AS partnerLevelOrder, 
                      inLocator.FlagID AS locatorFlagID
FROM         dbo.Location AS l INNER JOIN
                      dbo.organisation AS o ON l.OrganisationID = o.OrganisationID INNER JOIN
                      dbo.BooleanFlagData AS inLocator ON l.LocationID = inLocator.EntityID INNER JOIN
                      dbo.BooleanFlagData AS partnerLevel ON o.OrganisationID = partnerLevel.EntityID 
					  LEFT OUTER JOIN dbo.Province AS prov ON prov.Abbreviation = l.Address5
