alter view
vMatchingLocationByMatchNameByCountryID
as
/* WAB LID 5849 2011/03/08 added locationid to the match key and made corresponding change to the join in the dedupeMatchMethod table */

SELECT DISTINCT CountryID, OrganisationID, CONVERT(varchar, OrganisationID) + char(172) + MatchName AS Matchkey,
                          (SELECT     COUNT(LocationID) AS Expr1
                            FROM          dbo.Location
                            WHERE      (MatchName = l.MatchName) AND (OrganisationID = l.OrganisationID) AND (CountryID = l.CountryID)) AS countduplicates
FROM         dbo.Location AS l
WHERE     (LTRIM(RTRIM(MatchName)) <> '') AND (LTRIM(RTRIM(MatchName)) IS NOT NULL)
GROUP BY CountryID, OrganisationID, MatchName
HAVING      ((SELECT     COUNT(LocationID) AS Expr1
                         FROM         dbo.Location AS Location_1
                         WHERE     (MatchName = l.MatchName) AND (OrganisationID = l.OrganisationID) AND (CountryID = l.CountryID)) > 1)
                         
