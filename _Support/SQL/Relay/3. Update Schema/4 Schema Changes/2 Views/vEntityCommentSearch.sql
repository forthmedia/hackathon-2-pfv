/* only added the view for consistency */
IF EXISTS(SELECT * FROM sys.views WHERE name = 'vEntityCommentSearch') 
	DROP VIEW dbo.vEntityCommentSearch;
GO
CREATE VIEW dbo.vEntityCommentSearch WITH SCHEMABINDING 
AS 
SELECT entityCommentID, comment  as searchableString
FROM dbo.entityComment 
WHERE active = 1
GO
CREATE UNIQUE CLUSTERED INDEX vEntityCommentSearch_PK ON vEntityCommentSearch (entityCommentID);
GO