createPlaceHolderObject @objectName = 'vRecordRightsOperator', @objectType = 'VIEW'

GO

alter view vRecordRightsOperator
AS

/*
WAB 2015-11-03

*/

select 
	entityTypeID
	, recordid

	, convert(bit,groupAnd & power(2,1-1)) as  groupAnd_level1
	, convert(bit,groupNot & power(2,1-1)) as  groupNot_level1

	, convert(bit,groupAnd & power(2,2-1)) as  groupAnd_level2
	, convert(bit,groupNot & power(2,2-1)) as  groupNot_level2

	, convert(bit,groupAnd & power(2,3-1)) as  groupAnd_level3
	, convert(bit,groupNot & power(2,3-1)) as  groupNot_level3

	, convert(bit,groupAnd & power(2,4-1)) as  groupAnd_level4
	, convert(bit,groupNot & power(2,4-1)) as  groupNot_level4

	, convert(bit,groupAnd & power(2,5-1)) as  groupAnd_level5
	, convert(bit,groupNot & power(2,5-1)) as  groupNot_level5


	, convert(bit,groupAnd & power(2,11-1)) as  groupAnd_level11
	, convert(bit,groupNot & power(2,11-1)) as  groupNot_level11

			
from 
	recordRightsOperator

