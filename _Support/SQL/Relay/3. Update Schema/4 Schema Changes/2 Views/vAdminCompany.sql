if exists (select 1 from sysObjects where name = 'vAdminCompany')
	DROP view vAdminCompany

GO

create view [dbo].[vAdminCompany] 
as


select 
	organisationid, organisationname 
from 
	dbo.organisation  o 
		inner join 
	dbo.organisationType ot on o.organisationTypeID = ot.organisationTypeID
where 
TypeTextID = 'AdminCompany'
