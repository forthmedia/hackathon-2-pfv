

ALTER       VIEW [dbo].[vDistiList]
AS
SELECT     OrganisationID, c.CountryDescription, 
                      o.CountryID, web.data as specificURL, OrganisationName, phone.data as telephone, fax.data as fax
FROM         
	organisation o
		INNER JOIN
	organisationType ot on ot.organisationTypeID = o.organisationTypeID
		INNER JOIN
	country c
		on c.countryid = o.countryid
		left outer join
	textflagdata web
		on o.organisationid = web.entityid and web.flagid = 1298
		left outer join
	textflagdata phone
		on o.organisationid = phone.entityid and phone.flagid = 1299
		left outer join
	textflagdata fax
		on o.organisationid = fax.entityid and fax.flagid = 1300
where 
	ot.typeTextid='Distributor'
union
select 0,'All Countries',9,null,'Other',null,null	
