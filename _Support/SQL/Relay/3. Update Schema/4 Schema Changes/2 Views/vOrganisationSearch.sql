-- Create a full text indexed view on organisation
IF EXISTS(SELECT * FROM sys.views WHERE name = 'vOrganisationSearch') 
	DROP VIEW dbo.vOrganisationSearch;
GO

CREATE VIEW dbo.vOrganisationSearch WITH SCHEMABINDING 
AS 
SELECT OrganisationID as EntityID, cast(OrganisationID as nVarchar(16)) + cast(' ' as nVarchar(1)) + isNull(OrganisationName,'') + cast(' ' as nVarchar(1)) + isNull(OrgURL,'') + cast(' ' as nVarchar(1)) + isNull(AKA,'') + cast(' ' as nVarchar(1)) + isNull(VATNumber,'') + cast(' ' as nVarchar(1)) + isNull(crmOrgID,'') as searchableString
FROM dbo.organisation;
GO

CREATE UNIQUE CLUSTERED INDEX vOrganisationSearch_PK ON [dbo].vOrganisationSearch (EntityID);
GO