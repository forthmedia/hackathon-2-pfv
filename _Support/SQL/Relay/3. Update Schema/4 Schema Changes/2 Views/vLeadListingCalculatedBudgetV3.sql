if exists (select 1 from sysobjects where name = 'vLeadListingCalculatedBudgetV3')
DROP VIEW  vLeadListingCalculatedBudgetV3

GO

CREATE  VIEW [dbo].[vLeadListingCalculatedBudgetV3]
AS
SELECT     dbo.organisation.OrganisationName AS Account, opp.lastupdated AS last_Updated, dbo.OppStage.OpportunityStage AS Stage, opp.stageID,
                          BUDGET AS calculated_budget,
                          (SELECT     SUM(unitprice * quantity)
                            FROM          opportunityProduct
                            WHERE      opportunityID = opp.opportunityID) AS calculated_budget_fullPrice,
                          (SELECT     SUM(subtotal)
                            FROM          opportunityProduct
                            WHERE      opportunityID = opp.opportunityID AND datepart(q, forecastShipDate) = datepart(q, getDate()) AND datepart(yy, forecastShipDate) 
                                                   = datepart(yy, getDate())) AS calculated_budget_this_quarter, (budget * 0.05) AS rebate,
					opp.oppTypeID,
                          (SELECT     count(*)
                            FROM          opportunityProduct
                            WHERE      opportunityID = opp.opportunityID AND specialPrice > 0 AND specialPriceApproverPersonID IS NULL) AS unapproved_special_price, 
                      budget AS overall_customer_budget, opp.probability, opp.currency, p1.FirstName + ' ' + p1.LastName AS Account_Manager, 
                      p2.FirstName + ' ' + p2.LastName AS partner_Sales_Person, opp.distiLocationID, 
                      /*show expiry date only for deals which will expire*/ SPEXPIRYDATE = CASE WHEN oppPrice.oppPricingStatus = 'SPQ Fully Approved' /*and OppStage.opportunitystageid <> 5*/ THEN
                       opp.SPEXPIRYDATE ELSE NULL END,
                          (SELECT     COUNT(*)
                            FROM          actions
                            WHERE      entityTypeID = 23 AND entityID = opp.opportunityID) AS Number_of_Actions, opp.entityid, opp.sponsorPersonid, opp.sourceID, opp.detail, 
                      opp.statusID AS status_ID, dbo.OppStage.status, dbo.OppStage.opportunitystageid AS OppStageID, dbo.OppStage.statusSortOrder, 
                      dbo.OppStage.liveStatus, dbo.OppStage.onlyShowInQuarter, partnerlocation.SiteName AS Partner_Name, opp.flagid, 
                      opp.opportunityID AS Opportunity_ID, opp.currentSituation, CONVERT(varchar(3), rtrim(datename(m, opp.expectedCloseDate))) 
                      + '-' + substring(CONVERT(varchar(4), datepart(yy, opp.expectedCloseDate)), 3, 2) AS month_expected_close, 'Q' + CONVERT(varchar(3), 
                      rtrim(datename(q, opp.expectedCloseDate))) + '-' + substring(CONVERT(varchar(4), datepart(yy, opp.expectedCloseDate)), 3, 2) 
                      AS Quarter_expected_close, partnerlocation.LocationID AS PartnerLocationID, opp.expectedCloseDate, opp.vendorSalesManagerPersonID, 
                      opp.vendorAccountManagerPersonID, opp.partnerSalesManagerPersonID, opp.partnerSalesPersonID, 
                      dbo.organisation.OrganisationID AS AccountLocationID,
                          (SELECT     TOP 1 c2.countrydescription
                            FROM          country c1 JOIN
                                                   countrygroup cg ON cg.countrymemberid = c1.countryid JOIN
                                                   country c2 ON c2.countryid = cg.countrygroupid
                            WHERE      c2.isocode IS NULL AND c1.countryid = opp.countryid AND c2.countrydescription NOT IN ('all countries', 'europe')) AS Region, 
                      opp.countryID AS countryID, dbo.Country.ISOCode, dbo.Country.CountryDescription AS country, oppPrice.oppPricingStatus AS pricing_status, 
                      opp.oppPricingStatusID, opp.SPApproverPersonIDs AS SPApproverPersonIDs, SPRequiredApprovalLevel, SPCurrentApprovalLevel, 
                      opp.SPApprovalNumber AS SPApprovalNumber, l.sitename AS Distributor, max(oph.created) AS SPQ_Last_Updated
FROM         dbo.Person p1 RIGHT OUTER JOIN
                      dbo.organisation RIGHT OUTER JOIN
                      dbo.opportunity opp INNER JOIN
                      dbo.oppPricingStatus oppPrice ON oppPrice.oppPricingStatusID = opp.oppPricingStatusID LEFT OUTER JOIN
                      location l ON l.locationid = opp.distilocationid INNER JOIN
                      dbo.OppStage ON opp.stageID = dbo.OppStage.OpportunityStageID LEFT OUTER JOIN
                      oppPricingStatusHistory oph ON oph.opportunityid = opp.opportunityid INNER JOIN
                      dbo.Country ON opp.countryID = dbo.Country.CountryID ON dbo.organisation.OrganisationID = opp.entityid ON 
                      p1.PersonID = opp.vendorAccountManagerPersonID LEFT OUTER JOIN
                      dbo.Person p3 ON opp.sponsorPersonid = p3.PersonID LEFT OUTER JOIN
                      dbo.Person p2 ON opp.partnerSalesPersonID = p2.PersonID LEFT OUTER JOIN
                          (SELECT     Location.CountryID, Location.LocationID, Location.SiteName, Country.CountryDescription, Country.ISOCode
                            FROM          Country INNER JOIN
                                                   Location ON Country.CountryID = Location.CountryID) partnerlocation ON partnerlocation.LocationID = opp.partnerLocationID
WHERE     (dbo.OppStage.status <> 'delete')
GROUP BY dbo.organisation.OrganisationName, opp.lastupdated, dbo.OppStage.OpportunityStage, opp.stageID, opp.opportunityID, opp.budget, opp.probability, 
                      opp.currency, p1.FirstName, p1.LastName, p2.FirstName, p2.LastName, opp.distiLocationID, opp.SPExpiryDate, opp.entityid, opp.sponsorPersonid, 
                      opp.sourceID, opp.detail, opp.statusID, dbo.OppStage.status, dbo.OppStage.OpportunityStageID, dbo.OppStage.statusSortOrder, 
                      dbo.OppStage.liveStatus, dbo.OppStage.onlyShowInQuarter, partnerlocation.SiteName, opp.flagid, opp.currentSituation, opp.expectedCloseDate, 
                      partnerlocation.LocationID, opp.vendorSalesManagerPersonID, opp.vendorAccountManagerPersonID, opp.partnerSalesManagerPersonID, 
                      opp.partnerSalesPersonID, dbo.organisation.OrganisationID, opp.countryID, dbo.Country.ISOCode, dbo.Country.CountryDescription, 
                      oppPrice.oppPricingStatus, opp.oppPricingStatusID, opp.SPApproverPersonIDs, opp.SPRequiredApprovalLevel, opp.SPCurrentApprovalLevel, 
                      opp.SPApprovalNumber, l.SiteName, opp.oppTypeID


