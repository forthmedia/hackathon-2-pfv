/* NJH 2015/07/06 - filter by active, synching objects 
JIRA PROD2016-953 NJH 2016/05/26 - add dataConnectorResponseID to hold ID of data response record
*/

/****** Object:  View [dbo].[vConnectorQueue]    Script Date: 04/11/2014 14:07:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorQueue]'))
DROP VIEW [dbo].[vConnectorQueue]
GO

/****** Object:  View [dbo].[vConnectorQueue]    Script Date: 04/11/2014 14:07:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vConnectorQueue]
AS
SELECT     dbo.connectorQueue.ID, dbo.connectorObject.connectorType, dbo.connectorObject.object, case when dbo.connectorObject.relayware = 1 then 'E' else 'I' end as direction, dbo.connectorQueue.objectID, dbo.connectorQueue.modifiedDateUTC, 
                      dbo.connectorQueue.synchAttempt, dbo.connectorQueue.isDeleted, dbo.connectorQueue.created, dbo.connectorQueue.errored, dbo.connectorQueue.connectorResponseID, dbo.connectorQueue.dataExists, dbo.connectorQueue.dataConnectorResponseID, dbo.connectorQueue.process 
FROM         dbo.connectorQueue INNER JOIN
                      dbo.connectorObject ON dbo.connectorQueue.connectorObjectID = dbo.connectorObject.ID
WHERE
			dbo.connectorObject.active = 1 and dbo.connectorObject.synch = 1
GO