/**********************************************************
	Bugzilla 2070
**********************************************************/	

ALTER VIEW [dbo].[vTrngPeopleEligibleForCertificationByModules] AS  
/*
	NYB 2009-04-01 Sophos
	NYB 2009-06-04 SOP010 - should not be taking inactive rules into account
	DAN 2016-11-15 PROD2016-2739 - add also latest module fulfilled date so it can be used to set pass date on a housekeeping task
*/
SELECT     TOP 100 PERCENT personCertificationID, certificationID, personID, sysAccessDate, maxModuleFulfilledDate
FROM         
	(SELECT     personID, certificationID, certificationRuleID, numModulesToPass, sysAccessDate, personCertificationID, max(userModuleFulfilled) as maxModuleFulfilledDate, 
	       CASE WHEN
				  (SELECT     COUNT(DISTINCT moduleID)
					FROM          vtrngPersonCertifications
					WHERE      userModuleFulfilled IS NOT NULL AND personCertificationID = v.personCertificationID AND 
					certificationRuleID = v.certificationRuleID
					) >= numModulesToPass THEN 1 ELSE 0 END AS rulePassed
		   FROM dbo.vTrngPersonCertifications AS v
		   WHERE (personCertificationStatus in ('Registered','Certified Pending Update')) /* NYB 2009-04-01 Sophos - added 'Certified Pending Update' to list */ 
			AND ruleActive=1 /* NYB 2009-06-04 SOP010 - should not be taking inactive rules into account */
			AND getDate() >= dbo.dateAtMidnight(activationDate) /* 2013-05-14 CASE 432193 respect rule activationDate */ 
		   GROUP BY certificationID, personID, certificationRuleID, numModulesToPass, personCertificationID, sysAccessDate
	) AS base
GROUP BY personCertificationID, certificationID, personID, sysAccessDate, maxModuleFulfilledDate
HAVING      (COUNT(DISTINCT certificationRuleID) <= SUM(rulePassed))
ORDER BY sysAccessDate
GO
