--define entity grouping column and grouping name
select 'CountryID' as Col, 'Country' as name
into #entitygrouping
UNION
select 'OrganisationID', 'Organisation'
UNION
select 'PersonID', 'Person' 

--defined date grouping name and date grouping SQL
select 'Year' as Name, 'DATEADD(yy, DATEDIFF(yy,0,timeOfActivity), 0)' as SQL
into #dategrouping
UNION
select 'Month' as Name, 'DATEADD(month, DATEDIFF(month, 0, timeOfActivity),0)' as SQL
UNION
select 'Quarter' , 'RIGHT(CAST(DATEPART(YEAR, DATEADD(MONTH, - 3, timeOfActivity)) AS VARCHAR(4)), 4) + '' - '' + RIGHT(CAST(DATEPART(YEAR, DATEADD(YEAR, 1, DATEADD(MONTH, - 3, timeOfActivity))) AS VARCHAR(4)), 4) + '' Q'' + CAST(DATEPART(QUARTER, DATEADD(MONTH, - 3, timeOfActivity)) AS VARCHAR(1))' 
UNION
select 'Day', 'DATEADD(dd, DATEDIFF(dd, 0, timeOfActivity), 0)'


--Loop through temporary tables and build views
DECLARE	@currentViewEntity nvarchar(100),
		@currentViewEntityName nvarchar(100),
		@currentViewSQL nvarchar(1000),
		@currentViewDateName nvarchar(100),
		@rsqlcmd nvarchar(2000)

DECLARE entityCurser CURSOR FOR  
SELECT col, name 
FROM #entitygrouping 

OPEN entityCurser  
FETCH NEXT FROM entityCurser INTO @currentViewEntity, @currentViewEntityName  

WHILE @@FETCH_STATUS = 0   
BEGIN
	PRINT @currentViewEntity
	PRINT @currentViewEntityName
	
	DECLARE dateCurser CURSOR FOR  
	SELECT name, sql
	FROM #dategrouping 
	
	OPEN dateCurser
	FETCH NEXT FROM dateCurser INTO @currentViewDateName, @currentViewSQL  

	WHILE @@FETCH_STATUS = 0   
	BEGIN
		PRINT @currentViewDateName
		PRINT @currentViewSQL
		
		select @rsqlcmd = N'
			if exists (select 1 from information_schema.views where table_name=''vActivities' + @currentViewEntityName + 'by' + @currentViewDateName + ''')
			drop view vActivities' + @currentViewEntityName + 'by' + @currentViewDateName
		print  (@rsqlcmd)
		exec  (@rsqlcmd)
		
		select @rsqlcmd = N'
			Create view vActivities' + @currentViewEntityName + 'by' + @currentViewDateName + ' as
			
			select ' + @currentViewSQL + ' as timeOfActivity,activityTypeID, ' + @currentViewEntity + ', sum(numActivities) as numActivities
			from vActivities
			group by ' + @currentViewSQL + ', activityTypeID, ' + @currentViewEntity
		print  (@rsqlcmd)
		exec  (@rsqlcmd)
		
		
	FETCH NEXT FROM dateCurser INTO @currentViewDateName, @currentViewSQL  
	END
	
	CLOSE dateCurser   
	DEALLOCATE dateCurser
	
FETCH NEXT FROM entityCurser INTO @currentViewEntity, @currentViewEntityName  
END

CLOSE entityCurser   
DEALLOCATE entityCurser

--remove temporary tables
drop table #entitygrouping
drop table #dategrouping
GO