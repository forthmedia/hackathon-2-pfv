/*
2013-10-22 PPB Case 436900 use RegDate as Registered instead of ConfirmationSent
*/

ALTER VIEW [dbo].[vEventRegPastWeek] AS
	select p.firstname + ' ' + p.lastname as Name, o.organisationname as Company, 
	e.RegDate as Registered, eg.eventgroupid as EventGroupID, eg.eventname as Event_Group, 
	ed.title as Event, ed.PreferredStartdate as Event_Date, o.organisationid as OrgID, p.personid,
	p.jobdesc as Job_Description, RegStatus as Reg_Status, c.countrydescription as Event_Country,
	ed.location as Event_Location
	From eventflagdata e
	join person p on p.personid = e.entityid
	join organisation o on o.organisationid = p.organisationid
	join eventdetail ed on ed.flagid = e.flagid
	join eventgroup eg on eg.eventgroupid = ed.eventgroupid
	join country c on c.countryid = ed.countryid
	
GO

