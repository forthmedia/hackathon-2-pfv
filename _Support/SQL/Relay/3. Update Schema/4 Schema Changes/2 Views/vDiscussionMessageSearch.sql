IF EXISTS(SELECT * FROM sys.views WHERE name = 'vDiscussionMessageSearch') 
	DROP VIEW dbo.vDiscussionMessageSearch;
GO
CREATE VIEW dbo.vDiscussionMessageSearch WITH SCHEMABINDING 
AS 
SELECT discussionMessageID, title +  cast(' ' as nVarchar(1)) + message  as searchableString
FROM dbo.discussionMessage 
WHERE active = 1
GO
CREATE UNIQUE CLUSTERED INDEX vDiscussionMessageSearch_PK ON vDiscussionMessageSearch (discussionMessageID);
GO