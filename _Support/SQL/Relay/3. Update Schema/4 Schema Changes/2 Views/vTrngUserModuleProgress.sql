/****** Object:  View [dbo].[vTrngUserModuleProgress]    Script Date: 07/12/2012 19:21:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vTrngUserModuleProgress] AS
/*MODS: 
	NYB 2009-09-30 LHID2686
*/
SELECT distinct dbo.trngUserModuleProgress.UserModuleProgressID,
sp.FirstName + ' ' + sp.LastName AS Trainee, 
dbo.trngsolutionArea.solutionAreaName AS Solution_Area, 
dbo.trngCourseSeries.courseSeriesTitle AS Series_Title, 
so.OrganisationName AS Organisation_Name, 
ll.Address1 + '<br>' + ll.Address4 + '<br> ' + ll.Address5 + '<br>' + ll.PostalCode AS Postal_Address, /*NYB 2009-09-30 LHID2686 - added*/
dbo.trngCourse.AICCCourseID AS Course_Code, 
dbo.trngCourse.title_defaultTranslation AS Course_Title, 
dbo.trngModule.title_defaultTranslation AS Module_Title, 
dbo.trngUserModuleProgress.quizAttempts AS Attempts, 
dbo.trngUserModuleProgress.quizTimeSpent AS Time_Spent, 
dbo.trngUserModuleProgress.quizPassed AS quiz_Passed, 
dbo.trngUserModuleProgress.quizPassedDate AS completion_date, 
dbo.trngUserModuleProgress.AICCScore AS Score, 
dbo.trngUserModuleProgress.userModuleFulfilled, /*NYB 2009-09-30 LHID2686 - added*/
cc.CountryDescription AS Country, 
dbo.trngUserModuleStatus.description AS Module_Status, /*NYB 2009-09-30 LHID2686 - changed from statusID to description*/
dbo.trngUserModuleStatus.StatusTextID AS Module_StatusTextID, /*NYB 2009-09-30 LHID2686 - added*/
dbo.trngUserModuleProgress.personID, cc.CountryID, sp.OrganisationID,
dbo.trngUserModuleProgress.moduleID
FROM dbo.trngUserModuleProgress with (nolock)  
INNER JOIN dbo.trngUserModuleStatus with (nolock) ON dbo.trngUserModuleProgress.moduleStatus = dbo.trngUserModuleStatus.statusID 
INNER JOIN dbo.trngModule with (nolock) ON dbo.trngUserModuleProgress.moduleID = dbo.trngModule.moduleID 
INNER JOIN dbo.trngModuleCourse with (nolock) ON dbo.trngModule.moduleID = dbo.trngModuleCourse.moduleID 
INNER JOIN dbo.trngCourse with (nolock) ON dbo.trngModuleCourse.courseID = dbo.trngCourse.courseId 
INNER JOIN dbo.Person AS sp with (nolock) ON sp.PersonID = dbo.trngUserModuleProgress.personID 
INNER JOIN dbo.Location AS ll with (nolock) ON ll.LocationID = sp.LocationID 
INNER JOIN dbo.Country AS cc with (nolock) ON cc.CountryID = ll.CountryID 
INNER JOIN dbo.organisation AS so with (nolock) ON sp.OrganisationID = so.OrganisationID 
INNER JOIN dbo.trngsolutionArea with (nolock) ON dbo.trngCourse.solutionAreaID = dbo.trngsolutionArea.solutionAreaId 
LEFT OUTER JOIN dbo.trngCourseSeries with (nolock) ON dbo.trngCourse.courseSeriesID = dbo.trngCourseSeries.courseSeriesID 
/*NYB 2009-09-30 LHID2686 - removed: 
WHERE (NOT (sp.OrganisationID IN (SELECT OrganisationID FROM dbo.vOrgsToSupressFromReports)))
*/


GO


