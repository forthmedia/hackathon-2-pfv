IF EXISTS(SELECT * FROM sys.views WHERE name = 'vActivityScore') 
	DROP VIEW dbo.vActivityScore
GO

CREATE VIEW [dbo].[vActivityScore]
AS 
select 
		unionedScores.score,
		unionedScores.triggeringActivityID, 
		unionedScores.triggeringScoreRuleVersionID,
		unionedScores.scoreRuleGroupingID,
		unionedScores.comment,
		unionedScores.personID,
		unionedScores.locationID,
		unionedScores.organisationID,
		unionedScores.scoreRuleName,
		unionedScores.scoreRuleID,
		unionedScores.scoreRuleNameTranslation,
		unionedScores.scorePerActivity,
		unionedScores.scoreCap,
		unionedScores.wasManuallyApplied,
		scoreRuleGrouping.title_defaultTranslation as schemeTitle,
		scoreRuleGrouping.description_defaultTranslation as schemeDescription,
		scoreRuleGrouping.cappingPeriodTypeTextID,
		scoreRuleGrouping.cappingPeriodResetMonthOfYear,
		scoreRuleGrouping.cappingPeriodResetDayOfMonth,
		activityType.activityType,
		activityType.description as activityTypeDescription,
		activityType.activityTypeID,
		unionedScores.activityDate,
		unionedScores.activityTime,
		activity.countryID as activityCountry,
		activity.entityID as activityEntityID, --the entity that "was" the activity or the activity "was about"
		activityType.entityTypeID as activityEntityTypeID, --the type of the entity that the activity "was" or the activity "was about"
		activityEntitySchemaTable.entityname as activityEntityTypeName,
		activityEntitySchemaTable.label as activityEntityTypeLabel,
		collectionEntitySchemaTable.entitytypeid as collectingEntityTypeID,
		collectionEntitySchemaTable.entityname as collectingEntityTypeName,
		collectionEntitySchemaTable.label as collectingEntityTypeLabel
from ( 
		--grab the score table relevant bits, null out bits that only apply to manualScoreAdjustments 
		Select 
			score,
			triggeringActivityID, 
			triggeringScoreRuleVersionID,
			scoreRuleGroupingID,
			scoreRule.scoreRuleID as scoreRuleID,
			scoreRule.name_defaultTranslation as scoreRuleName,
			'phr_name_scoreRule_' + cast(scoreRule.scoreRuleID as nVarChar(30)) as scoreRuleNameTranslation,
			scoreRuleVersion.scoreValue as scorePerActivity,
			scoreRuleVersion.scoreCap as scoreCap,
			null as comment,
			personID,
			locationID,
			organisationID ,
			Activity.activityTime as activityTime,
			Activity.activityDate as activityDate,
			0 as wasManuallyApplied
		from
			Score
			join scoreRuleVersion on scoreRuleVersion.scoreRuleVersionID=Score.triggeringScoreRuleVersionID
			join scoreRule on scoreRuleVersion.scoreRuleID=scoreRule.scoreRuleID
			join Activity on Score.triggeringActivityID=Activity.activityID
	
	union all
		--grab the manual score adjustment data
		Select 
			score,
			null as triggeringActivityID,
			null as triggeringScoreRuleVersionID,
			scoreRuleGroupingID,
			null as scoreRuleName, 
			null as scoreRuleNameTranslation,
			null as scoreRuleID,
			null as scorePerActivity,
			null as scoreCap,
			comment,
			personID,
			locationID,
			organisationID ,
			CONVERT(date, effectiveDate) as activityDate,
			effectiveDate as activityTime,
			1 as wasManuallyApplied
		from
			scoreManualAdjustment
	) as unionedScores
	--jeft join as scoreManualAdjustment will have null for activity data
	left join activity on unionedScores.triggeringActivityID=activity.activityID
	left join activityType on activity.activityTypeID=activityType.activityTypeID
	left join schemaTable activityEntitySchemaTable on activityType.entityTypeID=activityEntitySchemaTable.entitytypeid
	join scoreRuleGrouping on scoreRuleGrouping.scoreRuleGroupingID=unionedScores.scoreRuleGroupingID
	join schemaTable collectionEntitySchemaTable on scoreRuleGrouping.entityTypeID=collectionEntitySchemaTable.entitytypeid

GO


