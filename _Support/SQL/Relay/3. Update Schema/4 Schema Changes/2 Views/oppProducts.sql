
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[oppProducts]'))
DROP VIEW [dbo].[oppProducts]
GO

create view oppProducts as 
select OpportunityID,ProductORGroupID,ProductORGroup,quantity,createdby,lastupdated,lastupdatedby,created,unitPrice,forecastShipDate,
	opportunityProductID as oppProductID,specialPrice,specialPriceApproverPersonID,productPromotionID,probability,stockLocation,SPManualEdit,
	productsShipped,shippingComplete,originalUnitPrice,ApprovedPrice,demo,promotionPrice,crmOpportunityProductID as crmOppProductID,pricebookEntryID,
	lastUpdatedByPerson,subtotal
from opportunityProduct