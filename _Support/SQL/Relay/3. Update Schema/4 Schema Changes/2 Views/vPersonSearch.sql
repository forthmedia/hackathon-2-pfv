IF EXISTS(SELECT * FROM sys.views WHERE name = 'vPersonSearch') 
	DROP VIEW dbo.vPersonSearch;
GO
CREATE VIEW dbo.vPersonSearch WITH SCHEMABINDING 
	AS 
SELECT p.personID as EntityID, cast(p.personID as nVarchar(16)) + cast(' ' as nVarchar(1)) + isNull(p.FirstName,'') + cast(' ' as nVarchar(1)) + isNull(p.LastName,'') + cast(' ' as nVarchar(1)) + p.Email 
	+ cast(' ' as nVarchar(1)) + isNull(Address1,'') + cast(' ' as nVarchar(1)) + isNull(Address2,'') + cast(' ' as nVarchar(1)) + isNull(Address3,'') + cast(' ' as nVarchar(1)) + isNull(Address4,'') + cast(' ' as nVarchar(1)) + isNull(Address5,'') + cast(' ' as nVarchar(1)) + isNull(PostalCode,'') + cast(' ' as nVarchar(1)) + isNull(SiteName,'') + cast(' ' as nVarchar(1)) + isNull(Telephone,'')
	+ cast(' ' as nVarchar(1)) + isNull(OrganisationName,'') + cast(' ' as nVarchar(1)) + isNull(AKA,'') + cast(' ' as nVarchar(1)) + isNull(ORGURL,'') + cast(' ' as nVarchar(1)) + isNull(crmPerID,'') + cast(' ' as nVarchar(1)) + isNull(crmLocID,'') + cast(' ' as nVarchar(1)) + isNull(crmOrgID,'') 
	as searchableString
	FROM dbo.person p
	INNER JOIN dbo.location l ON p.locationID = l.locationID
	INNER JOIN dbo.organisation o ON p.organisationID = o.organisationID
GO
CREATE UNIQUE CLUSTERED INDEX vPersonSearch_PK ON vPersonSearch (EntityID);
GO