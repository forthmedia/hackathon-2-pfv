ALTER VIEW [dbo].[vLeadAMPipeline]
AS
SELECT  distinct l.countryID,OppS.status AS Project_Stage,
	P.PersonID as PersonID, P.Firstname + ' ' + p.lastname AS Account_Manager, 
	OppS.OpportunityStage as Opportunity_Stage,
	OppS.OpportunityStageID as OpportunityStageID, 
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		and op.probability = 25
		) AS TwentyFive_Percent
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		and op.probability = 50
		) AS Fifty_Percent
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		and op.probability = 75
		) AS SeventyFive_Percent
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		and op.probability = 90
		) AS Ninety_Percent
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		and op.probability = 100
		) AS OneHundred_Percent,
(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,getDate())
		and datepart(yy, forecastShipDate) = datepart(yy,getDate())
		) AS calculated_budget_this_quarter,
(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
		and op.probability = 25
		) AS TwentyFive_PercentN
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
		and op.probability = 50
		) AS Fifty_PercentN
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
		and op.probability = 75
		) AS SeventyFive_PercentN
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
		and op.probability = 90
		) AS Ninety_PercentN
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
		and op.probability = 100
		) AS OneHundred_PercentN,
(SELECT     SUM(op.subtotal)
                            FROM          opportunityProduct op
				INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
                            WHERE      p.personid = o.vendoraccountmanagerpersonid
				and o.StageID = OppS.OpportunityStageID
				and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 1, getDate()))
				and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 1,getDate()))
			) AS calculated_budget_next_quarter,
(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
		and op.probability = 25
		) AS TwentyFive_PercentN2
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
		and op.probability = 50
		) AS Fifty_PercentN2
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
		and op.probability = 75
		) AS SeventyFive_PercentN2
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
		and op.probability = 90
		) AS Ninety_PercentN2
,
	(SELECT     SUM(op.subtotal)
	FROM    opportunityProduct op
		INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
	WHERE   p.personid = o.vendoraccountmanagerpersonid
		and o.StageID = OppS.OpportunityStageID
		and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
		and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
		and op.probability = 100
		) AS OneHundred_PercentN2,
(SELECT     SUM(op.subtotal)
                            FROM          opportunityProduct op
				INNER JOIN opportunity o ON o.opportunityID = op.opportunityID
                            WHERE      p.personid = o.vendoraccountmanagerpersonid
				and o.StageID = OppS.OpportunityStageID
				and datepart(q, forecastShipDate) = datepart(q,DATEADD ( q , 2, getDate()))
				and datepart(yy, forecastShipDate) = datepart(yy,DATEADD ( q , 2,getDate()))
			) AS calculated_budget_quarter_plus2


FROM    Person p 
inner join location l on p.locationid = l.locationid 
join Opportunity o on o.vendoraccountmanagerpersonid = p.personid
Cross join OppStage OppS