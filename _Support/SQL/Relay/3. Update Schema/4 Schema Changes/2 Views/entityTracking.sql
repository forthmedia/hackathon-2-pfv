/* Note: any columns added in here means that the entityTracking trigger needs to be updated!! */

IF EXISTS(SELECT * FROM sys.views WHERE name = 'EntityTracking') 
	DROP VIEW dbo.EntityTracking
GO

CREATE VIEW [dbo].[EntityTracking]
AS 
SELECT e.entityTrackingID,e.entityID,e.entityTypeID,e.personID,fromURl.url as fromLoc,toUrl.url as toLoc,e.created,e.commId,e.visitID,e.clickThru
	from entityTrackingBase e
		left join entityTrackingUrl fromUrl on fromUrl.entityTrackingUrlId = e.fromUrlID
		left join entityTrackingUrl toUrl on toUrl.entityTrackingUrlId = e.toUrlID
GO