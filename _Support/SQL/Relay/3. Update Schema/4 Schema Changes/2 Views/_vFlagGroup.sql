/*
WAB 2014-03-26 created this view, long overdue
Doesn't have every field in it, but any others can be added if required
NJH 2016/09/27	JIRA PROD2016-1303 - added formatting parameters to view as need to work out whether a booleanFlagGroup should be treated as a bit field
*/

IF EXISTS(SELECT * FROM sys.views WHERE name = 'vflagGroup') 
	DROP VIEW dbo.vflagGroup
GO

create view vflagGroup
AS
SELECT 
	fg.flagGroupID,fg.flagGroupTextID,fg.parentFlagGroupID,fg.flagTypeID,fg.entityTypeID,fg.active,fg.description,fg.namePhraseTextID,fg.DescriptionPhraseTextID,
	fet.tablename as entityTable,
	ft.name as dataType,
	ft.dataTableFullName,
	fg.formattingParameters,
	fg.name
FROM 
	flagGroup fg
		inner join
	flagEntityType fet	on fg.entityTypeID = fet.entityTypeID
		inner join
	flagType ft on ft.flagTypeID = 	fg.flagTypeID 

	