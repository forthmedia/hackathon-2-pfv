/* NJH 2014/12/03
	a view returning field meta data... in it's early stages and is open for modification. If changed, please let NJH know so that we can look at how this affects
	the connector configuration screen 
	
	If 'displayAs=memo', then treat as a textArea	

	WAB 2016-09-21 	PROD2016-1303 Performance improvements.  Split into nested queries so that  getPickListSqlForTableColumn() does not have to be called so often and is not called unless required
					Also filter out event flags and individual boolean flags (booleans appear as their groups)
					
					Still unsure what dataType a list of boolean flags should be given.  'Text' does not feel quite right
	NJH 2016/09/27	PROD2016-1303 - continue with changes. Removed displayType column. Added max/min. Look for boolean flags with isBitField attribute in formatting paramters
					and treat it as a bit field
	NJH 2017/01/20	JIRA PROD2016-3317 - if we have a bit field that is nullable, then output the field as a radio as we want to be able to offer 3 choices. The null is handled by the code layer.
*/
	

createplaceholderobject 'view', 'vFieldMetaData'

GO

ALTER view [dbo].[vFieldMetaData]
AS
	select 
		  tablename
		, name
		, fullfieldname
		, label
		, readonly
		, dataType
		, MaxLength
		, [min]
		, [max]
		, isUniqueKey as isPrimaryKey
		, foreignKey
		, [parameters]
		, apiName
		, IsNullable
		, defaultValue
		, isIdentity
		, isComputed
		, validate
		/*, displayType = 
		  case when displayType is null then
			case when isUniqueKey = 1 then 'id' when pickListValuesSql is not null then 'picklist' when dbo.getForeignKeyForTableColumn(tablename,name) is not null then 'reference' when datatype='bit' then 'boolean' when MaxLength > 255 then 'textArea' when datatype like '%date%' then 'date' when datatype='numeric' then 'numeric' else 'text' end 
		  else
			displayType
		  end*/
		, displayAs = 
		  case 
			when displayAs is null then
				case when MaxLength > 255 then 'textArea' 
				when datatype like '%date%' then 'date' when datatype = 'bit' and IsNullable = 0 then 'checkbox' when datatype = 'bit' and IsNullable = 1 then 'radio' when pickListValuesSql is not null or foreignKey is not null then 'select' else 'text' end 
		  else
			displayAs
		  end
		, pickListValuesSql
		,foreignKeyValuesSql
		, source
		, sourceid
	from
		(select 
			*
			,dbo.getPickListSqlForTableColumn(tablename,name) as pickListValuesSql
			,dbo.getForeignKeySqlForTableColumn(tablename,name) as foreignKeyValuesSql
			,dbo.getForeignKeyForTableColumn(tablename,name) as foreignKey
		from
			(select
				  c.table_name as tablename
				, c.column_name as name
				, c.table_name +  '.' + c.column_name as fullfieldname
				, case when character_maximum_length is not null then 'text' when numeric_precision is not null then 'numeric' when datetime_precision is not null then 'date' else c.data_Type end as dataType
				, case
						when c.numeric_precision is not null then
							c.numeric_precision +
							case when data_Type = 'tinyint' then 0 else 1 end +   /* add one for the negative sign (except tinyint) */
							case when c.numeric_Scale = 0 then 0 else 1 end			/* add one for the decimal point */
						else
							c.character_Maximum_Length
						end
					as MaxLength
				,case
						when data_Type like 'tinyint' then
							'0'
						when data_Type like '%int' then
							CONVERT(VARCHAR (40), - (power (cast(2 as decimal(38)),(8*st.length)-1) ) )
						when numeric_precision is not null then
							CONVERT(VARCHAR (40), -(POWER (cast(10 as decimal(38)) , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (cast(10 as decimal(38)),NUMERIC_SCALE) -1) ELSE '' END

						ELSE
							NULL

						end
					as [min]
				,case
						when data_Type like 'tinyint' then
							CONVERT(VARCHAR (40), power (cast(2 as decimal(38)),(8*st.length)) -1 )
						when data_Type like '%int' then
							CONVERT(VARCHAR (40), power (cast(2 as decimal(38)),(8*st.length)-1) -1 )
						when numeric_precision is not null then
							CONVERT(VARCHAR (40), (POWER (cast(10 as decimal(38)) , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (cast(10 as decimal(38)),NUMERIC_SCALE) -1) ELSE '' END
						end
					as [max]
				, cfformParameters as [parameters]
				, isNull(apiName,c.column_name) as apiName
				, case when IS_NULLABLE='No' then 0 else 1 end as IsNullable
				--, case when COLUMN_DEFAULT like '%getDate()%' then 'getDate()' else replace(replace(replace(COLUMN_DEFAULT,'(',''),')',''),'''','') end as defaultValue
				/* This regExp deals with 'old style' defaults which look like CREATE DEFAULT xxxx AS '' as defaultValue */
				, dbo.regExmatch('(?<=As ).*', column_default , 1) as defaultValue
				, COLUMNPROPERTY(object_id(c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') as isIdentity
				, COLUMNPROPERTY(object_id(c.TABLE_NAME), c.COLUMN_NAME, 'IsComputed') as isComputed
				, case when numeric_precision is not null then 'numeric' when c.column_name='email' then 'email' when (c.column_name like '%phone' or c.column_name like '%fax') then 'telephone' end as validate
				, case when c.column_name = s.uniqueKey then 1 else 0 end as isUniqueKey
				--, null as displayType
				, null as displayAs
				, 'core' as source
				, modentityId as sourceID
				, case when p.phraseID is not null then  'phr_'+ p.phraseTextID else c.column_name end as label
				, f.readonly
			from
				information_schema.columns  c 
					inner join 
				sysTypes st on c.data_type = st.name
					inner join
				schemaTable s on c.table_name = s.entityName
					left Join 
				modentitydef f on f.tablename= c.table_name and f.fieldname = c.column_name
					left join
				phraseList p on p.phraseTextID = c.table_name+'_'+c.column_name	and p.entityTypeID = 0

			union

			/* boolean flags - the group is the field */
			select
				  s.entityName as tablename
				, case when len(fg.flagGroupTextID) = 0 then 'flagGroup_'+cast(fg.flagGroupId as nvarchar(max)) else fg.flagGroupTextID end as name
				, entityName +  '.' + case when len(fg.flagGroupTextID) = 0 then 'flagGroup_'+cast(fg.flagGroupId as nvarchar(max)) else fg.flagGroupTextID end as fullfieldname
				, 'text' as dataType
				, null as MaxLength
				, null as [min]
				, null as [max]
				, formattingParameters as [parameters]
				, fg.flagGroupTextID as apiName
				, 1 as IsNullable
				, null as defaultValue
				, 0 as isIdentity
				, 0 as isComputed
				, null as validate
				, 0 as isUniqueKey
				--, case when dataType='checkbox' then 'multipicklist' else 'picklist' end as displayType
				, case when ft.name='checkbox' then 'multiselect' else 'select' end as displayAs
				, 'flagGroup'
				, flagGroupID
				, case when fg.namePhraseTextID is not null then 'phr_'+ fg.namePhraseTextID else fg.name end as label
				, case when edit = 0 and editAccessRights = 0 then 1 else 0 end as readonly
			from 
				flagGroup fg
					inner join flagType ft on ft.flagTypeID = fg.flagTypeID 
					inner join schemaTable s on s.entityTypeID = fg.entityTypeID
			where 
				dataTableFullName = 'booleanFlagData'
				and isNull(dbo.getParameterValue(fg.formattingParameters,'isBitField',','),'0') in ('0','false')

			union


			select
				  v.entityTable as tablename
				, case when len(v.flagTextID) = 0 then 'flag_'+cast(v.flagId as nvarchar(max)) else v.flagTextID end as name
				, v.entityTable +  '.' + case when len(v.flagTextID) = 0 then 'flag_'+cast(v.flagId as nvarchar(max)) else v.flagTextID end as fullfieldname
				, case when dataType = 'integer' then 'numeric' when dataTable='boolean' then 'bit' else dataType end as dataType
				, case when dataType = 'integer' then 11 when dataTable != 'boolean' then isNull(dbo.getParameterValue(f.formattingParameters,'maxLength',','),4000) else null end as MaxLength
				, null as [min]
				, null as [max]
				, f.formattingParameters as [parameters]
				, v.flagTextID as apiName
				, 1 as IsNullable
				, null as defaultValue
				, 0 as isIdentity
				, 0 as isComputed
				, null as validate	
				, 0 as isUniqueKey
				--, case when dataType = 'IntegerMultiple' then 'multipicklist' when dataType='textMultiple' or dbo.getParameterValue(f.formattingParameters,'displayAs',',')= 'memo' then 'textArea' when v.linksToEntityTypeID is not null then 'reference' else dataType end as displayType
				, case when dataType = 'IntegerMultiple' then 'multiselect' when dataType='textMultiple' or dbo.getParameterValue(f.formattingParameters,'displayAs',',')= 'memo' then 'textArea' when v.linksToEntityTypeID is not null then 'select' else dataType end as displayAs
				, 'flag'
				, v.flagID
				, case when f.namePhraseTextID is not null then 'phr_'+ f.namePhraseTextID else f.name end as label
				, case when edit = 0 and editAccessRights = 0 then 1 else 0 end as readonly
			from 
				vFlagDef v
					inner join
				flag f on f.flagid = v.flagid
					inner join 
				flagGroup fg on fg.flagGroupID = f.flagGroupId
			where 
				dataTable not in ('event','boolean') 
					or (datatable = 'boolean' and isNull(dbo.getParameterValue(fg.formattingParameters,'isBitField',','),'0') in ('1','true'))
			
			) as dummy
		) as dummy	




