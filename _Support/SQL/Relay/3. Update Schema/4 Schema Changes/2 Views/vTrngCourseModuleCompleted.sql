ALTER VIEW [dbo].[vTrngCourseModuleCompleted] AS 
/*
2009/03/11  NYB Altered to use new phrase technique
*/
SELECT sp.Salutation + ' ' + sp.FirstName + ' ' + sp.LastName AS Trainee, 
so.OrganisationName AS Organisation_Name, ll.Address1 + '<br>' + ll.Address4 + '<br> ' + ll.Address5 + '<br>' + ll.PostalCode AS Postal_Address, 
dbo.trngCourse.courseid,dbo.trngCourse.title_defaultTranslation AS Course_Title,
dbo.trngModule.moduleid, 
'phr_title_TrngModule_' + convert(VarChar,dbo.trngModule.moduleID) AS Module_Title, 
dbo.trngUserModuleProgress.quizAttempts AS Attempts, dbo.trngUserModuleProgress.quizTimeSpent AS Time_Spent, 
dbo.trngUserModuleProgress.quizPassed AS quiz_Passed, dbo.trngUserModuleProgress.AICCScore AS Score, 
dbo.trngUserModuleProgress.userModuleFulfilled, cc.CountryDescription AS Country, 
dbo.trngUserModuleProgress.moduleStatus AS Module_Status, dbo.trngUserModuleProgress.personID, 
dbo.trngUserModuleProgress.userModuleProgressID, cc.CountryID, sp.OrganisationID, 
dbo.trngCourseSeries.courseSeriesID, dbo.trngCourseSeries.courseSeriesShortName 
FROM dbo.trngUserModuleProgress 
INNER JOIN dbo.trngModule ON dbo.trngUserModuleProgress.moduleID = dbo.trngModule.moduleID 
INNER JOIN dbo.trngModuleCourse ON dbo.trngModuleCourse.moduleID = dbo.trngModule.moduleID
INNER JOIN dbo.trngCourse ON dbo.trngModuleCourse.courseID = dbo.trngCourse.courseId 
LEFT OUTER JOIN dbo.trngCourseSeries ON dbo.trngCourse.courseSeriesID = dbo.trngCourseSeries.courseSeriesID 
INNER JOIN dbo.Person AS sp ON sp.PersonID = dbo.trngUserModuleProgress.personID 
INNER JOIN dbo.Location AS ll ON ll.LocationID = sp.LocationID 
INNER JOIN dbo.Country AS cc ON cc.CountryID = ll.CountryID 
INNER JOIN dbo.organisation AS so ON sp.OrganisationID = so.OrganisationID 
WHERE (NOT (sp.OrganisationID IN (SELECT OrganisationID FROM dbo.vOrgsToSupressFromReports)))