createplaceholderobject 'view', 'vCountryScope'

GO

ALTER VIEW
vCountryScope AS

/*  WAB 2012-09-09 Joins countryScope to the countrygroup table to resolve countrygroups
	WAB 2015-11-12 PROD2015-364.  Added a group by to prevent duplicate records (which occur if a country ends up in more than one of the groups specified on a record)
								Added a column to indicate whether entity has been explicity scoped with this country (as opposed to the country being a member of a countryGroup (region) which is scoped to the entity)
*/

SELECT 
	entityID, 
	entity, 
	entityTypeID,
	countryMemberID as countryid,
	max(case when cg.countryGroupID = cg.countryMemberID then 1 else 0 end) as explicityScopedToThisCountry
FROM 
	countryscope cs
		inner join
	countryGroup cg	 on cg.countryGroupID = cs.countryID
group by 
	entityID, 
	entity, 
	entityTypeID,
	countryMemberID


