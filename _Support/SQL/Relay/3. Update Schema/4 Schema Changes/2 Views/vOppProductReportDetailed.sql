if exists (select 1 from sysObjects where name = 'vOppProductReportDetailed')
	DROP view vOppProductReportDetailed

GO


/* NJH 2009/11/03 - added productID column */
CREATE VIEW [dbo].[vOppProductReportDetailed]
AS
SELECT     p.SKU, op.quantity, op.subtotal AS opp_VALUE, org.OrganisationName AS ACCOUNT, o.detail, org.OrganisationID AS ENTITYID, 
                      per.FirstName + ' ' + per.LastName AS ACCOUNT_MANAGER, o.vendorAccountManagerPersonID, c.CountryDescription AS COUNTRY, 
                      os.status AS OPPORTUNITY_STATUS, os.OpportunityStageID AS opp_StatusID, op.probability AS PRODUCT_PROBABILITY, 
                      op.forecastShipDate AS FORECAST_SHIP_DATE, org.countryID, o.opportunityID AS OPPORTUNITY_ID, DATENAME(month, op.forecastShipDate) AS Month_due, 
                      pg.Description AS Product_Group, o.lastupdated AS LAST_UPDATED, p.ProductID
FROM         dbo.opportunity AS o WITH (NOLOCK) INNER JOIN
                      dbo.opportunityProduct AS op WITH (NOLOCK) ON op.OpportunityID = o.opportunityID INNER JOIN
                      dbo.Product AS p WITH (NOLOCK) ON p.ProductID = op.ProductORGroupID INNER JOIN
                      dbo.ProductGroup AS pg WITH (NOLOCK) ON pg.ProductGroupID = p.ProductGroupID INNER JOIN
                      dbo.organisation AS org WITH (NOLOCK) ON org.OrganisationID = o.entityid INNER JOIN
                      dbo.Country AS c WITH (NOLOCK) ON c.CountryID = org.countryID INNER JOIN
                      dbo.OppStage AS os WITH (NOLOCK) ON os.OpportunityStageID = o.stageID LEFT OUTER JOIN
                      dbo.Person AS per WITH (NOLOCK) ON per.PersonID = o.vendorAccountManagerPersonID
WHERE     (os.OpportunityStageID IN (1, 2, 3, 4))
GO
