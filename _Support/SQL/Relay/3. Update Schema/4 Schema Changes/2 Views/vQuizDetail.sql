/*********************
Need to run /translation/bulkEntityPhraseInsert.cfm?entityname=quizDetail&entityIDColumn=quizDetailID&phrasetextID=title&columnToTranslate=QuizName
on any environment this is released to to ensure default translations are created for the quiz names
**********************/

ALTER  VIEW [dbo].[vQuizDetail]
AS
/* 
	NYB 2009-08-03 SNY047 replaced qd.QuizName with: 'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) 
	 2010/11/17	NAS		LID4784: CR : Quiz - Setup Quiz Area - Add Column for Associated Module 
	2012-07-06 WAB removed the phr_ stuff and instead use _defaultTranslation columns  (prevents timeouts doing cf_translateQueryColumn) 
*/
SELECT     
	qd.QuizDetailID, 
	/*'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) AS Quiz_Name,*/
	qd.title_defaultTranslation as quiz_name, 
	NumToChoose AS Questions_To_Choose, 
	TimeAllowed AS Time_Allowed, 
	QuestionsPerPage AS Questions_Per_Page, 
	NumberOfAttemptsAllowed AS Attempts_allowed, 
	/*	'phr_title_TrngModule_' + convert(VarChar,tm.moduleID) AS Associated_Module */
	tm.title_defaultTranslation AS Associated_Module,
	qd.active
FROM         dbo.QuizDetail AS qd left outer join trngmodule tm on qd.quizDetailID = tm.quizDetailID

GO