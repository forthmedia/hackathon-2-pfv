/*
 Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016-05-12			    atrunov  PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules (added hasRecordRightsBitMask)


 */

DROP VIEW vTrngCourses
GO


CREATE VIEW vTrngCourses
AS
SELECT     tcs.courseSeriesShortName AS Series, tc.courseId, 'phr_title_TrngCourse_' + CONVERT(VarChar, tc.courseId) AS Title_of_Course, tul.userLevelName AS [LEVEL], 
                      tsa.solutionAreaId, tsa.solutionAreaName AS Solution_Area, tc.AICCCourseID AS Course_Code, 'phr_description_TrngCourse_' + CONVERT(VarChar, tc.courseId) 
                      AS Description, tc.sortOrder, tc.active, tc.publishedDate, tc.mobileCompatible, tc.TrainingProgramID, tc.hasRecordRightsBitMask
FROM         dbo.trngCourse AS tc LEFT OUTER JOIN
                      dbo.trngCourseSeries AS tcs ON tc.courseSeriesID = tcs.courseSeriesID INNER JOIN
                      dbo.trngsolutionArea AS tsa ON tc.solutionAreaID = tsa.solutionAreaId INNER JOIN
                      dbo.trnguserLevel AS tul ON tc.userLevelID = tul.userLevelId

GO