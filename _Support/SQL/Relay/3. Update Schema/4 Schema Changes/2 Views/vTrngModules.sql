DROP VIEW vTrngModules
GO

/*********************
Need to run /translation/bulkEntityPhraseInsert.cfm?entityname=quizDetail&entityIDColumn=quizDetailID&phrasetextID=title&columnToTranslate=QuizName
on any environment this is released to to ensure default translations are created for the quiz names

Amendments
2011/09/06 PPB LID7679 country scope modules
2012/03/27 RMB added mobileCompatible
2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration Return new field 'coursewareTypeID'
2016-05-18	WAB BF-712 Add ExpiryDate
2016-05-12  atrunov  PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules (added hasRecordRightsBitMask)

**********************/
CREATE VIEW vTrngModules
AS
SELECT     tc.title_defaultTranslation AS Title_of_Course, tm.AICCFileName AS FileName, 'phr_title_trngModule_' + CONVERT(VarChar, tm.moduleID) AS Title_of_Module, 
                      'phr_description_trngModule_' + CONVERT(VarChar, tm.moduleID) AS Description_of_Module, tm.AICCType AS Module_Type, tm.AICCMaxScore AS Maximum_Score, 
                      tm.AICCMaxTimeAllowed AS Maximum_Time_Allowed, tm.moduleID, tm.moduleCode AS Module_Code, tm.moduleDuration AS Duration, tm.quizDetailID, tm.CountryID, 
                      dbo.QuizDetail.title_defaultTranslation AS Associated_Quiz, tm.fileID, tm.SortOrder, tm.publishedDate, tc.courseId, tm.points, tc.active, tm.availability, tsa.solutionAreaId, 
                      tsa.solutionAreaName AS Solution_Area, tm.mobileCompatible, tm.TrainingProgramID, tm.isSCORM, tm.passDecidedBy, tc.TrainingProgramID as courseTrainingProgramID, tc.title_defaultTranslation as courseTitle_defaultTranslation,
                      tm.title_defaultTranslation as moduleTitle_defaultTranslation, tm.coursewareTypeID, tm.expiryDate, tm.hasRecordRightsBitMask as moduleHasRecordRightsBitMask, tc.hasRecordRightsBitMask as courseHasRecordRightsBitMask
FROM         dbo.trngModule AS tm INNER JOIN
                      dbo.trngModuleCourse AS tmc ON tm.moduleID = tmc.moduleID INNER JOIN
                      dbo.trngCourse AS tc ON tmc.courseID = tc.courseId INNER JOIN
                      dbo.trngsolutionArea AS tsa ON tc.solutionAreaID = tsa.solutionAreaId LEFT OUTER JOIN
                      dbo.QuizDetail ON tm.quizDetailId = dbo.QuizDetail.quizDetailId

GO

