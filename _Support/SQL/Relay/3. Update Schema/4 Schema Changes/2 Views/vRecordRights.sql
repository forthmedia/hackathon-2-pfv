IF EXISTS (select 1 from sysobjects where name = 'vRecordRights')
	DROP view vRecordRights
	
GO

Create view [dbo].[vRecordRights]
AS
/*
WAB 2012-07-03 a view which adds together all the recordrights a person has from different user groups
*/

SELECT  
	personid, entity, recordid,

	CONVERT(bit, SUM(PERMISSION & 1)) * 1 +
	CONVERT(bit, SUM(PERMISSION & 2)) * 2 + 
	CONVERT(bit, SUM(PERMISSION & 4)) * 4 +
	CONVERT(bit, SUM(PERMISSION & 8)) * 8  +
	CONVERT(bit, SUM(PERMISSION & 16)) * 16  +

	CONVERT(bit, SUM(PERMISSION & 512)) * 512  +
	CONVERT(bit, SUM(PERMISSION & 1024)) * 1024
	as permission,

	CONVERT(bit, SUM(PERMISSION & 1)) AS level1,
	CONVERT(bit, SUM(PERMISSION & 2)) AS level2, 
	CONVERT(bit, SUM(PERMISSION & 4)) AS level3, 
	CONVERT(bit, SUM(PERMISSION & 8)) AS level4,
	CONVERT(bit, SUM(PERMISSION & 16)) AS level5,

	CONVERT(bit, SUM(PERMISSION & 512)) AS level10,
	CONVERT(bit, SUM(PERMISSION & 1024)) AS level11


FROM
	RightsGroup AS rg 
			inner join
	RecordRights rr 	on rr.UserGroupID = rG.UserGroupID
	
GROUP BY PersonID, entity,recordid

