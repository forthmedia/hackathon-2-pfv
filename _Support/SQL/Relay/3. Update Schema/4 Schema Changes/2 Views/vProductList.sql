createplaceholderobject 'view', 'vProductList'

GO

ALTER VIEW [dbo].[vProductList]
AS
SELECT promotion.promoName, promotion.MaxOrderNo, 
    promotion.PromoID, promotion.AskQuestions, 
    promotion.InventoryID, Product.ProductID, Product.Description_defaultTranslation as description,
    Product.title_defaultTranslation as title, 
    Product.SKU, Product.DiscountPrice, Product.ListPrice, 
    Product.countryID_notused, Product.ProductGroupID, Product.Deleted, 
    Product.SKUGroup, Product.PriceISOCode, Product.SortOrder, 
    Product.includeImage, Product.maxQuantity,
    Product.hasCountryScope,
    productGroup.title_defaultTranslation as productGroup,
    productGroup.Description_defaultTranslation as productGroupDescription,
        (SELECT NumberInStock - isnull(NumberAllocated, 0)
      FROM Inventory
      WHERE Product.SKU = Inventory.SKU AND 
           promotion.inventoryid = inventory.inventoryid) 
    AS NumberAvailable, Product.CampaignID, product.active
FROM Product INNER JOIN
    promotion ON 
    Product.CampaignID = promotion.CampaignID
    inner join productGroup on Product.ProductGroupID = ProductGroup.ProductGroupID
WHERE (Product.SKU IS NOT NULL) AND (Product.Deleted = 0)


GO