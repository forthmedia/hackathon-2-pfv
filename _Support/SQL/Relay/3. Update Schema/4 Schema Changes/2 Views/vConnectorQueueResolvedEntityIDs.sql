
IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorQueueResolvedEntityIDs]'))
DROP VIEW [dbo].[vConnectorQueueResolvedEntityIDs]
GO

create view [dbo].[vConnectorQueueResolvedEntityIDs] as
select q.*, m.entityTypeID, v.name, v.entityid as relaywareID, v.crmid as remoteID, bfd.flagID
FROM 
	dbo.vConnectorQueue AS q
	inner join vConnectorMapping m on m.isRemoteID=1 and isNumeric(q.objectId)=1 and q.object = m.object_relayware and q.direction='E'
    inner join vEntityName v on v.entityTypeID = m.entityTypeID and v.entityid = q.objectID
	left join vBooleanFlagData bfd on bfd.name='Suspended' and bfd.entityTypeID = m.entityTypeID and bfd.entityID = q.objectid 
union all
select q.*, m.entityTypeID, v.name, v.entityID as relaywareID, q.objectID as remoteID, bfd.flagID
FROM						
	dbo.vConnectorQueue AS q
	inner join vConnectorMapping m on m.isRemoteID=1 and q.object = m.object_remote and q.direction='I'
    left join vEntityName v on v.entityTypeID = m.entityTypeID and v.crmID = q.objectID
	left join vBooleanFlagData bfd on bfd.name='Suspended' and bfd.entityTypeID = m.entityTypeID and bfd.entityID = v.entityID 
GO
