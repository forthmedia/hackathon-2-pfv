ALTER view [dbo].[vLogins] as
select firstname + ' '+ lastname as fullname, o.organisationName,
username, c.ISOCode, p.personid, remoteIP, sdd.domain as app, browser, loginDate
from usage u 
inner join person p on p.personid = u.personid
inner join organisation o ON o.organisationID = p.organisationID
inner join location l ON l.locationID = p.locationID
inner join country c ON c.countryID = l.countryID
inner join siteDefDomain sdd on sdd.ID = u.siteDefDomainID

GO
