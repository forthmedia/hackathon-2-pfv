/* 	WAB 2016-03-17	PROD2016-118  Housekeeping Improvements
					New View
*/
exec createPlaceHolderObject 'view', 'vHouseKeepingLog'

GO
	ALTER  VIEW
	vHouseKeepingLog
	
	AS

		select
			 hk.methodname
			, case when pl.id is not null then 1 else 0 end as locked
			, rank() over (partition by hk.methodname + case when hk.runoneach = 'instance' then '_' + convert(varchar,cf.id ) else '' end order by isNull(lastRunTime,cf.id) desc) as FilterOnThisColumnToRemoveDuplicateRunOnEachDatabaseItems
			, hk.runoneach
			, cf.id as coldfusioninstanceid
			, servername + '.' + instancename as instance
			, hkl.lastRunTime
			, hkl.nextRunTime
			, isNull(hkl.isOK,0) as isOK
			, hkl.metadata
			, cf.instancename
			, cf.active as instanceActive
		from
			housekeeping	hk
				inner join
			coldFusionInstance_autopopulated cf on cf.active = 1
				left join
			housekeepingLog_ hkl on hk.housekeepingid = hkl.housekeepingid  and hkl.coldfusioninstanceid = cf.id
				left join
			processLock	pl on lockname = methodname + case when hk.runoneach = 'instance' then '_' + convert(varchar, hkl.coldfusioninstanceid ) else '' end

GO
