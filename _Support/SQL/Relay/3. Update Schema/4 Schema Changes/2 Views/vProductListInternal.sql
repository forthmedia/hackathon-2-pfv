createplaceholderobject 'view', 'vProductListInternal'

GO

ALTER  VIEW [dbo].[vProductListInternal]
AS
SELECT  pm.promoName as promotion,
    pm.MaxOrderNo as max_order_no,
    pm.PromoID,
    pm.AskQuestions as askQuestions,
    P.ProductID as productID,
    left(P.Description,40) as description,
    P.SKU,
    P.Status,
    p.remoteProductID,
    p.DiscountPrice as discount_price,
    p.DiscountPrice as points_value,
    P.ListPrice as list_Price,
    P.countryID_notused,
    p.ProductGroupID,
    CASE WHEN p.deleted = 1 THEN 'Supressed' ELSE 'Show' END as supressed,
    p.lastUpdated as last_updated,
    P.SKUGroup as SKU_Group,
    p.PriceISOCode,
    LEFT( UPPER( SKU ), 1 ) as alphabeticalIndex,
    -- CASE WHEN countryid = 0 THEN 'ALL' ELSE (select CountryDescription from country where countryid = p.countryid) END as country,
    p.SortOrder,
    P.includeImage,
    p.maxQuantity,
    pg.description as Product_Group,
    p.CampaignID,
    p.hasCountryScope
FROM Product p
   left outer join productGroup pg  on Pg.ProductGroupID = P.ProductGroupID
    INNER JOIN promotion pm     ON P.CampaignID = pm.CampaignID
GO