/****** Object:  View [dbo].[vTrngPersonCertifications]    Script Date: 08/23/2012 10:55:55 ******/
/* 
2012-09-17 IH Case 430705 get default certification title if titlePhraseTextID is null 
2012-09-27 IH Add tcr.certificationRuleOrder
2012/11/19 NJH CASE 432093 - added a union as we get completed trngPersonCertifications from the snapshot or history tables, rather than the current tables
2013-04-11 STCR CASE 432194 - use trngModuleSet.description_defaultTranslation
2013-05-22 STCR CASE 435401 - include module publishedDate so that portal webservice can filter out future modules
2013-05-14 NYB Case 434909 added "with(nolock)"s
2013-05-22 PPB Case 435262 added moduleSortOrder
2013-11-05 WAB CASE 437839 enforce maximum of 9999 on tc.duration  (if set too large causes a date overflow elsewhere)
2014-05-08 DCC CR P-LEN139 - removed the where clause on the first select statement so that we get all modules that are against the certification even if a user has passed the certification.
           This allows users on the portal to take additional modules should they wish to.
2015-12-20 PYW P-TAT006 BRD 31 - add trngPersonCertification.expiryDate as personCertificationExpiryDate and to get expiry date when person has passed a certification
           and trngPersonCertification.validity
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vTrngPersonCertifications]  AS  
SELECT 
	tpc.personID, tc.certificationID, tc.code AS certificationCode, 
	tc.active AS certificationActive, tc.certificationTypeID, 
	tpc.registrationDate AS personCertificationRegistrationDate, 
	tpc.passDate AS personCertificationPassDate, 
	tpc.expiredDate AS personCertificationExpiredDate, 
	tpc.cancellationDate AS personCertificationCancellationDate, 
	tpc.accessDate AS sysAccessDate, tc.Title_defaultTranslation AS certificationTitle, 
	tc.Description_defaultTranslation AS certificationDescription,
	dbo.minimumValue(tc.duration,9999) AS certificationDuration, 
	DATEADD(mm, tc.duration, tpc.passDate) AS expiryDate, 
	tpc.expiryDate AS personCertificationExpiryDate,
	tc.countryID AS certificationCountryID, tcr.certificationRuleID, 
	tcr.numModulesToPass, tcr.active AS ruleActive, 
	tcr.certificationRuleOrder, tms.moduleSetID, 
	tms.active AS moduleSetActive, 
	tms.description_defaultTranslation AS moduleSetDescription, tmsm.moduleID, 
	tm.AICCTitle, tm.ModuleCode, tm.AICCDescription, tm.publishedDate AS modulePublishedDate,
	tm.Credits, tump.userModuleFulfilled, 
	tpcs.description AS personCertificationStatus, 
	tpcs.statusTextID AS personCertificationStatusTextID, 
	tpc.personCertificationID, tm.fileID, 
	tcrt.ruleTypeTextID AS certificationRuleType, 
	tcr.studyPeriod, ISNULL(tcr.activationDate, tcr.created) AS activationDate, 
	tm.AICCType, tm.AICCFileName, tums.statusTextID AS userModuleStatus, 
	tump.userModuleProgressID, tm.quizDetailID, tm.SortOrder AS moduleSortOrder,
	tc.validity
FROM dbo.trngPersonCertification AS tpc WITH (noLock) 
	INNER JOIN dbo.trngPersonCertificationStatus AS tpcs WITH (noLock) ON tpc.statusID = tpcs.statusID 
	INNER JOIN dbo.trngCertification AS tc WITH (noLock) ON tc.certificationID = tpc.certificationID 
	LEFT OUTER JOIN dbo.trngCertificationRule AS tcr WITH (noLock) 
		INNER JOIN dbo.trngCertificationRuleType AS tcrt WITH (noLock) ON tcr.certificationRuleTypeID = tcrt.certificationRuleTypeID 
		ON tc.certificationID = tcr.certificationID 
	LEFT OUTER JOIN dbo.trngModuleSet AS tms WITH (noLock) ON tcr.moduleSetID = tms.moduleSetID 
	LEFT OUTER JOIN dbo.trngModuleSetModule AS tmsm WITH (noLock) ON tms.moduleSetID = tmsm.moduleSetID 
	LEFT OUTER JOIN dbo.trngModule AS tm WITH (noLock) ON tmsm.moduleID = tm.moduleID 
	LEFT OUTER JOIN dbo.trngUserModuleProgress AS tump WITH (noLock) 
	INNER JOIN dbo.trngUserModuleStatus AS tums WITH (noLock) ON tump.moduleStatus = tums.statusID ON tump.personID = tpc.personID 
		AND tump.moduleID = tm.moduleID 
		AND tump.userModuleProgressID IN (
			SELECT     TOP 1 userModuleProgressID AS Expr1 FROM dbo.trngUserModuleProgress AS tump1 WITH (noLock)
			WHERE (moduleID = tm.moduleID) AND (personID = tpc.personID) 
			ORDER BY startedDateTime DESC
		)
/*WHERE 
	tpc.passDate is null or (tpc.passDate is not null and tpcs.statusTextID = 'pending' and tcrt.ruleTypeTextID='Update') --taken out to show all modules*/
UNION
SELECT 
	tpc.personID, tc.certificationID, tc.code AS certificationCode, 
	tc.active AS certificationActive, tc.certificationTypeID, 
	tpc.registrationDate AS personCertificationRegistrationDate, 
	tpc.passDate AS personCertificationPassDate, 
	tpc.expiredDate AS personCertificationExpiredDate, 
	tpc.cancellationDate AS personCertificationCancellationDate, 
	tpc.accessDate AS sysAccessDate, tc.Title_defaultTranslation AS certificationTitle, 
	tc.Description_defaultTranslation AS certificationDescription, 
	tc.duration AS certificationDuration, 
	DATEADD(mm, tc.duration, tpc.passDate) AS expiryDate,
	tpc.expiryDate AS personCertificationExpiryDate,
	tc.countryID AS certificationCountryID, tpcr.certificationRuleID, 
	tpcr.numModulesToPass, tpcr.active as ruleActive, 
	tpcr.certificationRuleOrder, tms.moduleSetID, 
	tms.active AS moduleSetActive, 
	tms.description_defaultTranslation AS moduleSetDescription, tpcmr.moduleID, 
	tm.AICCTitle, tm.ModuleCode, tm.AICCDescription, tm.publishedDate AS modulePublishedDate,
	tm.Credits, tump.userModuleFulfilled, 
	tpcs.description AS personCertificationStatus, 
	tpcs.statusTextID AS personCertificationStatusTextID, 
	tpc.personCertificationID, tm.fileID, 
	tcrt.ruleTypeTextID as certificationRuleType, 
	tpcr.studyPeriod, tpcr.activationDate, 
	tm.AICCType, tm.AICCFileName, tums.statusTextID AS userModuleStatus, 
	tump.userModuleProgressID, tm.quizDetailID, tm.SortOrder AS moduleSortOrder,
	tc.validity 
FROM dbo.trngPersonCertification AS tpc WITH (noLock) 
	INNER JOIN dbo.trngPersonCertificationStatus AS tpcs WITH (noLock)  ON tpc.statusID = tpcs.statusID 
	INNER JOIN dbo.trngCertification AS tc WITH (noLock)  ON tc.certificationID = tpc.certificationID 
	INNER JOIN dbo.trngPersonCertificationRule AS tpcr WITH (noLock) ON tpc.personCertificationID = tpcr.personCertificationID
	INNER JOIN dbo.trngCertificationRuleType AS tcrt WITH (noLock) ON tpcr.certificationRuleTypeID = tcrt.certificationRuleTypeID 
	INNER JOIN dbo.trngModuleSet AS tms WITH (noLock)  ON tpcr.moduleSetID = tms.moduleSetID 
	INNER JOIN dbo.trngPersonCertificationRuleModule AS tpcmr WITH (noLock)  ON tpcmr.personCertificationRuleID = tpcr.personCertificationRuleID
	INNER JOIN dbo.trngModule AS tm WITH (noLock)  ON tpcmr.moduleID = tm.moduleID 
	INNER JOIN dbo.trngUserModuleProgress AS tump WITH (noLock) 
		INNER JOIN dbo.trngUserModuleStatus AS tums  WITH (noLock) ON tump.moduleStatus = tums.statusID ON tump.personID = tpc.personID 
			AND tump.moduleID = tm.moduleID 
			AND tump.userModuleProgressID IN (
				SELECT     TOP 1 userModuleProgressID FROM dbo.trngUserModuleProgress AS tump1 WITH (noLock) 
				WHERE (moduleID = tm.moduleID) AND (personID = tpc.personID) 
				ORDER BY startedDateTime DESC
			)
WHERE 
	tpc.passDate is not null

GO