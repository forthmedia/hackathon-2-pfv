if exists (select 1 from information_schema.views where table_name = 'vOpportunityProductBySPQ')
drop view vOpportunityProductBySPQ
GO

CREATE VIEW [dbo].[vOpportunityProductBySPQ]
AS
SELECT     TOP 100 PERCENT dbo.Product.SKU, dbo.Country.CountryDescription AS Country_description, dbo.opportunityProduct.specialPrice, dbo.opportunityProduct.unitPrice AS ListPrice, 
                      dbo.opportunityProduct.specialPrice * dbo.opportunityProduct.quantity AS TOTAL_SPQ_PRICE, 
                      CASE WHEN dbo.opportunityProduct.unitPrice = 0 THEN 0 ELSE 100 - ((dbo.opportunityProduct.specialPrice / dbo.opportunityProduct.unitPrice) * 100) END AS AverageDiscount, 
                      SUM(dbo.opportunityProduct.quantity) AS SKUQuantity, dbo.opportunity.opportunityID, dbo.opportunity.detail AS project, 
                      per.FirstName + ' ' + per.LastName AS ACCOUNT_MANAGER, dbo.opportunityProduct.forecastShipDate AS FORECAST_SHIP_DATE, dbo.opportunityProduct.created, 
                      dbo.Country.CountryID, dbo.opportunity.SPApprovalNumber AS SPQ_Approved_ID, dbo.opportunity.currency, dbo.Product.CampaignID
FROM         dbo.Product INNER JOIN
                      dbo.OppStage INNER JOIN
                      dbo.opportunity ON dbo.OppStage.OpportunityStageID = dbo.opportunity.stageID AND dbo.OppStage.OpportunityStageID <> 10 INNER JOIN
                      dbo.opportunityProduct ON dbo.opportunity.opportunityID = dbo.opportunityProduct.OpportunityID ON dbo.Product.ProductID = dbo.opportunityProduct.ProductORGroupID INNER JOIN
                      dbo.Country ON dbo.opportunity.countryID = dbo.Country.CountryID LEFT OUTER JOIN
                      dbo.Person AS per ON per.PersonID = dbo.opportunity.vendorAccountManagerPersonID
WHERE     (dbo.opportunityProduct.ProductORGroup = 'P') AND (dbo.opportunityProduct.specialPrice IS NOT NULL)
GROUP BY dbo.Product.SKU, dbo.Country.CountryDescription, dbo.Product.CampaignID, dbo.Product.Deleted, dbo.opportunityProduct.unitPrice, dbo.opportunityProduct.specialPrice, 
                      dbo.opportunity.opportunityID, dbo.opportunity.detail, per.FirstName + ' ' + per.LastName, dbo.opportunityProduct.forecastShipDate, dbo.opportunityProduct.quantity, 
                      dbo.opportunityProduct.created, dbo.Country.CountryID, dbo.opportunity.SPApprovalNumber, dbo.opportunity.currency
HAVING      (dbo.Product.Deleted = 0)

