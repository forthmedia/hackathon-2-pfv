IF EXISTS(SELECT * FROM sys.views WHERE name = 'vPhrasesSearch') 
	DROP VIEW dbo.vPhrasesSearch;
GO
CREATE VIEW dbo.vPhrasesSearch WITH SCHEMABINDING 
	AS 
	SELECT ident, phraseText
	FROM dbo.phrases 
GO
CREATE UNIQUE CLUSTERED INDEX vPhrasesSearch_PK ON vPhrasesSearch (ident);
GO