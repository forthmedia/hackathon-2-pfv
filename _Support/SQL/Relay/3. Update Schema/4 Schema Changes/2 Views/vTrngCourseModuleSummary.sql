ALTER VIEW [dbo].[vTrngCourseModuleSummary] AS
/*
2009-03-11  NYB Altered to use new phrase technique
2009-09-24  NYB LHID2667
2012-07-12  PJP Case#429382 added titleTranslation Colums for trngModule and trngCourse
2016-7-18	RJT Prod2016-1194	Removed countryID from view as its always null so have been removed from the base table
*/
SELECT DISTINCT  dbo.trngModule.title_defaultTranslation AS AICCTitleDefault, dbo.trngCourse.title_defaultTranslation AS AICCCourseTitleDefault,dbo.trngCourse.courseid,trngModule.ModuleID,
'phr_title_TrngCourse_' + convert(VarChar,dbo.trngCourse.courseid) AS AICCCourseTitle, 
'phr_title_TrngModule_' + convert(VarChar,dbo.trngModule.moduleID) AS AICCTitle, 
/* LHID2667 - removed:
ISNULL ((SELECT round(AVG(cast(cast(QuizTimeSpent AS integer) + cast(AICCtime 
AS integer) AS decimal(8, 2))), 0) FROM trngUserModuleProgress 
WHERE trngUserModuleProgress.ModuleID = trngModule.ModuleID), 0) AS AvgTimeModule,
ISNULL ((SELECT SUM(cast(QuizTimeSpent AS integer)) + SUM(cast(AICCtime AS integer)) 
FROM trngUserModuleProgress WHERE trngUserModuleProgress.ModuleID = trngModule.ModuleID), 0) AS SumTimeModule, 
*/
/* LHID2667 - REPLACED:
ISNULL ((SELECT AVG(AICCScore) FROM trngUserModuleProgress 
WHERE trngUserModuleProgress.ModuleID = trngModule.ModuleID AND quizattempts > 0), 0) AS AvgQuizScore, 
 WITH:
*/
ISNULL ((SELECT AVG(qt.Score) FROM quiztaken qt INNER JOIN quizDetail qd on qt.quizDetailID=qd.quizDetailID and qd.quizDetailID=trngModule.quizDetailID),0) AS AvgQuizScore, 
ISNULL ((SELECT round(AVG(QuizAttempts), 0) FROM trngUserModuleProgress 
WHERE trngUserModuleProgress.ModuleID = trngModule.ModuleID AND quizattempts > 0), 0) AS 
/*renamed AvgNumberModule to AvgNumberQuizAttempts*/
AvgNumberQuizAttempts
FROM dbo.trngCourse 
INNER JOIN dbo.trngModuleCourse ON dbo.trngCourse.courseId = dbo.trngModuleCourse.courseID
INNER JOIN dbo.trngModule ON dbo.trngModuleCourse.moduleID = dbo.trngModule.moduleID 
INNER JOIN dbo.trngUserModuleProgress ON dbo.trngModule.moduleID = dbo.trngUserModuleProgress.moduleID 
--WHERE (dbo.trngUserModuleProgress.CountryID <> N' ')  /* 2009-09-24  NYB - removed, countryid seems to never get populated */


GO
