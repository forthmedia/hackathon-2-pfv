if exists (select 1 from information_schema.views where table_name='vActivities')
drop view vActivities
GO

Create view dbo.vActivities as
select i.activityId,isNull(i.activityTime,activityDate) as timeOfActivity,
	i.countryID, c.countryDescription as country, ot.typeTextID as organisationType, o.organisationName as organisation,
	case when p.personID is not null then isNull(p.firstname,'')+' '+isNull(p.lastname,'') else null end as person, i.activityTypeID, it.activityType, 
	it.description as activity,
	case 
		when s.entityName = 'discussionMessage' then dm.title
		when s.entityName = 'flag' then f.name
		when s.entityName = 'element' then e.headline
		when s.entityName = 'communication' then comm.title
		when s.entityName = 'files' then fi.filename
		when s.entityName = 'person' then case when pe.personID is not null then isNull(pe.firstname,'')+' '+isNull(pe.lastname,'') when pd.personID is not null then isNull(pd.firstname,'')+' '+isNull(pd.lastName,'') end
		when s.entityName = 'organisation' then case when oe.organisationID is not null then oe. organisationName else od.organisationName end
		when s.entityName = 'trngModule' then m.title_defaultTranslation
		when s.entityName = 'trngCertification' then tc.title_defaultTranslation
		when s.entityName = 'specialisation' then sp.title_defaultTranslation
		when s.entityName = 'eventDetail' then ed.title
		when s.entityName = 'opportunity' then opp.detail
		when s.entityName = 'siteDefDomain' then sdd.domain
		else cast(i.entityID as varchar) end
		 as name,
	isNull(i.numActivities,1) as numActivities,
	i.data,
	i.organisationID,i.locationID,i.personID,i.entityID,it.entityTypeID
from activity i
	inner join activityType it on i.activityTypeID = it.activityTypeID
	inner join country c on c.countryID = i.countryID
	inner join schemaTable s on s.entityTypeID = it.entityTypeID
	left join organisationType ot on i.organisationTypeID = ot.organisationTypeID
	left join organisation o on i.organisationID = o.organisationID
	left join person p on p.personID = i.personID
	left join person pe on pe.personID = i.entityID and s.entityName='person'
	left join personDel pd on pd.personID = i.personID and s.entityName='person'
	left join organisation oe on oe.organisationID = i.entityID and s.entityName='organisation'
	left join organisationDel od on od.organisationID = i.personID and s.entityName='organisation'
	left join discussionMessage dm on dm.discussionMessageID = i.entityID and s.entityName='discussionMessage'
	left join flag f on f.flagID = i.entityID and s.entityName='flag'
	left join communication comm on comm.commID = i.entityID and s.entityName='communication'
	left join files fi on fi.fileID = i.entityID and s.entityName='files'
	left join element e on e.ID = i.entityID and s.entityName='element'
	left join trngModule m on m.moduleID = i.entityID and s.entityName='trngModule'
	left join trngCertification tc on tc.certificationID= i.entityID and s.entityName='trngCertification'
	left join specialisation sp on sp.specialisationID = i.entityID and s.entityName='specialisation'
	left join eventDetail ed on ed.flagID = i.entityID and s.entityName='eventDetail'
	left join opportunity opp on opp.opportunityID = i.entityID and s.entityName='opportunity'
	left join siteDefDomain sdd on sdd.ID = i.entityID and s.entityName='siteDefDomain'
GO