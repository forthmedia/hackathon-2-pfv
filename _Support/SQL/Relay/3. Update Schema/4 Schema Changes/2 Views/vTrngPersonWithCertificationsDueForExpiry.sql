/****** Object:  View [dbo].[vTrngPeopleWithCertificationsDueForExpiry]    Script Date: 23/12/2015 10:28:24 ******/
/* 
2012-12-23	PYW		P-TAT006 BRD 31. Add new expiry condition when person certification has an expiry date
*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vTrngPeopleWithCertificationsDueForExpiry] AS

SELECT DISTINCT 
	TOP 100 PERCENT personCertificationID, certificationID, personID, sysAccessDate,
	CASE
		WHEN (personCertificationStatus = 'Pending') AND (certificationRuleType = 'Update') AND (ruleActive = 1) AND (DATEADD(mm, studyPeriod, activationDate) < GETDATE()) 
			THEN 'CertUpdateStudyPeriodExpired'
		WHEN (personCertificationStatus = 'Registered') AND (ruleActive = 1) AND (DATEADD(mm, studyPeriod, personCertificationRegistrationDate) < GETDATE())
			THEN 'CertStudyPeriodExpired'
		WHEN (personCertificationStatus = 'Registered') AND (certificationActive = 1) AND (DATEADD(mm, certificationDuration, personCertificationRegistrationDate) < GETDATE())
			THEN 'CertDurationExpired' 
		WHEN (personCertificationStatus = 'Passed') AND (DATEDIFF(d,GETDATE(),personCertificationExpiryDate) = 0)
			THEN 'CertificationExpired'
		ELSE '' 
	END
		AS expiryReason
FROM
	dbo.vTrngPersonCertifications AS v
WHERE
	(personCertificationStatus = 'Pending') AND (certificationRuleType = 'Update') AND (ruleActive = 1) AND (DATEADD(mm, studyPeriod, activationDate) < GETDATE()) OR
	(personCertificationStatus = 'Registered') AND (ruleActive = 1) AND (DATEADD(mm, studyPeriod, personCertificationRegistrationDate) < GETDATE()) OR
	(personCertificationStatus = 'Registered') AND (certificationActive = 1) AND (DATEADD(mm, certificationDuration, personCertificationRegistrationDate) < GETDATE()) OR
	(personCertificationStatus = 'Passed') AND (DATEDIFF(d,GETDATE(),personCertificationExpiryDate) = 0)
ORDER BY
	sysAccessDate

GO