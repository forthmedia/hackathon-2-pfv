IF EXISTS (select 1 from sysobjects where name = 'vRights')
	DROP view vRights
	
GO

Create view [dbo].[vRights]
AS
/*
WAB 2012-07-03 a view which adds together all the rights a person has from different user groups
*/

SELECT  
	personid, s.securityTypeID,shortname,countryid, 

	CONVERT(bit, SUM(PERMISSION & 1)) * 1 +
	CONVERT(bit, SUM(PERMISSION & 2)) * 2 + 
	CONVERT(bit, SUM(PERMISSION & 4)) * 4 +
	CONVERT(bit, SUM(PERMISSION & 8)) * 8  +
	CONVERT(bit, SUM(PERMISSION & 16)) * 16 +
	CONVERT(bit, SUM(PERMISSION & 32)) * 32  +
	CONVERT(bit, SUM(PERMISSION & 64)) * 64  	
	as permission,

	CONVERT(bit, SUM(PERMISSION & 1)) AS level1,
	CONVERT(bit, SUM(PERMISSION & 2)) AS level2, 
	CONVERT(bit, SUM(PERMISSION & 4)) AS level3, 
	CONVERT(bit, SUM(PERMISSION & 8)) AS level4,
	CONVERT(bit, SUM(PERMISSION & 16)) AS level5,
	CONVERT(bit, SUM(PERMISSION & 32)) AS level6,
	CONVERT(bit, SUM(PERMISSION & 64)) AS level7

FROM
	RightsGroup AS rg 
			inner join
	Rights r 	on r.UserGroupID = rG.UserGroupID
			inner join
	securityType s on s.securityTypeID = r.securityTypeID
			 
	
	
GROUP BY PersonID, s.securityTypeID,shortname, COUNTRYID	

