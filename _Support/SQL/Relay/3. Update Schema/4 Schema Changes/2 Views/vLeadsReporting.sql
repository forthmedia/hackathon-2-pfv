/*
YMA 2014/10/29 Create vLeadsReporting view.
YMA 2014/10/31 moved to end of release after ventityname is created.
*/

IF EXISTS(SELECT * FROM sys.views WHERE name = 'vLeadsReporting') 
	DROP VIEW dbo.vLeadsReporting
GO

create view vLeadsReporting
AS
SELECT     dbo.Lead.LeadID, 
			dbo.Lead.leadTypeID, 
			leadType.itemText as leadType, 
			dbo.Lead.leadTypeStatusID, 
			leadTypeStatus.itemText as leadTypeStatus, 
			dbo.Lead.salutation, 
			dbo.Lead.FirstName, 
			dbo.Lead.LastName, 
			dbo.Lead.Company, 
			dbo.Lead.Address1, 
			dbo.Lead.Address2, 
			dbo.Lead.Address3, 
			dbo.Lead.Address4, 
			dbo.Lead.Address5, 
			dbo.Lead.PostalCode, 
			dbo.Lead.CountryID, 
			dbo.Country.CountryDescription,
			dbo.Lead.Email, 
			dbo.Lead.custom1, 
			dbo.Lead.created, 
			dbo.Lead.createdby, 
			dbo.Lead.lastUpdatedBy, 
			dbo.Lead.lastUpdatedByPerson, 
			dbo.Lead.lastUpdated, 
			dbo.Lead.crmLeadID, 
			dbo.Lead.convertedLocationID, 
			dbo.Lead.convertedPersonID, 
			dbo.Lead.convertedOpportunityID, 
			dbo.Lead.description, 
			dbo.Lead.officePhone, 
			dbo.Lead.mobilePhone, 
			dbo.Lead.website, 
			dbo.Lead.partnerLocationID, 
			partnerLocation.name as partnerLocation, 
			dbo.Lead.partnerSalesPersonID, 
			partnerSalesPerson.name as partnerSalesPerson, 
			dbo.Lead.partnerSalesManagerPersonID, 
			partnerSalesManagerPerson.name as partnerSalesManagerPerson, 
			dbo.Lead.distiLocationID, 
			distiLocation.name as distiLocation, 
			dbo.Lead.vendorAccountManagerPersonID, 
			vendorAccountManagerPerson.name as vendorAccountManagerPerson, 
			dbo.Lead.approvalStatusID, 
			approvalStatus.itemText as approvalStatus, 
			dbo.Lead.acceptedByPartner, 
			dbo.Lead.acceptedByPartnerDate, 
			dbo.Lead.viewedByPartnerDate, 
			dbo.Lead.currency, 
			dbo.Lead.rejectionReason, 
			dbo.Lead.progressID, 
			progress.ItemText as progress, 
			dbo.Lead.sourceID, 
			source.ItemText as source, 
			dbo.Lead.campaignID, 
			dbo.Lead.vendorSalesManagerPersonID, 
			vendorSalesManagerPerson.name as vendorSalesManagerPerson, 
			dbo.Lead.annualRevenue, 
			dbo.Lead.sponsorFirstname, 
			dbo.Lead.sponsorLastname, 
			dbo.Lead.enquiryType, 
			dbo.Lead.active
FROM         dbo.Lead LEFT OUTER JOIN
              dbo.Country ON dbo.Lead.CountryID = dbo.Country.CountryID LEFT OUTER JOIN
              LookupList as source on source.lookupID = dbo.Lead.sourceID LEFT OUTER JOIN
              LookupList as progress on progress.lookupID = dbo.Lead.progressID LEFT OUTER JOIN
              LookupList as approvalStatus on approvalStatus.lookupID = dbo.Lead.approvalStatusID LEFT OUTER JOIN
              LookupList as leadTypeStatus on leadTypeStatus.lookupID = dbo.Lead.leadTypeStatusID LEFT OUTER JOIN
              LookupList as leadType on leadType.lookupID = dbo.Lead.leadTypeID LEFT OUTER JOIN
              
              ventityname as partnerLocation on partnerLocation.entityID = dbo.Lead.partnerLocationID and partnerLocation.entityTypeID = 1 LEFT OUTER JOIN
              ventityname as partnerSalesPerson on partnerSalesPerson.entityID = dbo.Lead.partnerSalesPersonID and partnerSalesPerson.entityTypeID = 0 LEFT OUTER JOIN
              ventityname as partnerSalesManagerPerson on partnerSalesManagerPerson.entityID = dbo.Lead.partnerSalesManagerPersonID and partnerSalesManagerPerson.entityTypeID = 0 LEFT OUTER JOIN
              ventityname as distiLocation on distiLocation.entityID = dbo.Lead.distiLocationID and distiLocation.entityTypeID = 1 LEFT OUTER JOIN
              ventityname as vendorAccountManagerPerson on vendorAccountManagerPerson.entityID = dbo.Lead.vendorAccountManagerPersonID and vendorAccountManagerPerson.entityTypeID = 0 LEFT OUTER JOIN
              ventityname as vendorSalesManagerPerson on vendorSalesManagerPerson.entityID = dbo.Lead.vendorSalesManagerPersonID and vendorSalesManagerPerson.entityTypeID = 0        
GO