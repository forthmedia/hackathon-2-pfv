If exists(select 1 from sysobjects where name='vLocFlagData' and type='v')
drop view vLocFlagData
GO

create view  [dbo].[vLocFlagData]
AS

/*
WAB 13/06/07 trying out a concept
idea would be that this view is created by an sp, so that can be updated if new types come along

*/

select
	e.entityid,
	f.flag, 
	f.flagId,
	f.flagTypeid, 
	case 	when flagtypeid in (2,3) and bfd.flagid is not null then '1'
		when flagtypeid in (6) then convert(varchar,ifd.data)
		when flagtypeid in (4) then convert(varchar,dfd.data)
		when flagtypeid in (5) then tfd.data
		when flagtypeid in (8) then convert(varchar,imfd.data)
	end as data	

from 
(select locationid as entityid from location) as e join 
 vflagdef f on f.pl = 'l'
	left join booleanflagdata bfd on flagtypeid in (2,3) and bfd.flagid = f.flagid and bfd.entityid = e.entityid
	left join integerflagdata ifd on flagtypeid in (6) and ifd.flagid = f.flagid and ifd.entityid = e.entityid
	left join dateflagdata dfd on flagtypeid in (4) and dfd.flagid = f.flagid and dfd.entityid = e.entityid

	left join integermultipleflagdata imfd on flagtypeid in (8) and imfd.flagid = f.flagid and imfd.entityid = e.entityid
	left join textflagdata tfd on flagtypeid in (5) and tfd.flagid = f.flagid and tfd.entityid = e.entityid


GO
