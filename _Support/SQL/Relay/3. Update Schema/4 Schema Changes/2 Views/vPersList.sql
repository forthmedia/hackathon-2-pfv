ALTER  VIEW [dbo].[vPersList]
AS

/*
Modifications
WAB 2009/01/20  Added hasContactHistory column
		To speed up this access to this view the cfc was changed to not always bring back the lastcontactdate column
		In these cases we bring back a quicker hasContactHistory column 

2013-02-28 PPB Case 432634 consider a person deleted if their location is deleted (the same principle also applies for active/inactive)
*/
SELECT     P.PersonID, P.Salutation, LTRIM(RTRIM(P.FirstName)) AS firstname, LTRIM(RTRIM(P.LastName)) AS lastname, P.OfficePhone, LTRIM(RTRIM(P.Email)) AS email, 
                      P.MobilePhone, P.LastUpdated, ug.Name AS LastUpdatedBy,
                          (SELECT     MAX(DateSent) AS Expr1
                            FROM          commdetail WITH (nolock)
                            WHERE      (PersonID = P.PersonID) AND (CommTypeId IN
                                                       (SELECT     CommTypeID
                                                         FROM          CommDetailType WITH (nolock)
                                                         WHERE      (showInCommHistory = 1)))) AS lastContactDate,
		(SELECT     top 1 1
                            FROM          commdetail WITH (nolock)
                            WHERE      (PersonID = P.PersonID) AND (CommTypeId IN
                                                       (SELECT     CommTypeID
                                                         FROM          CommDetailType WITH (nolock)
                                                         WHERE      (showInCommHistory = 1)))) AS hasContactHistory,
                          
                          (SELECT     COUNT(*) AS Expr1
                            FROM          actions AS a WITH (nolock)
                            WHERE      (entityID = P.PersonID) AND (actionStatusID <> 3)) AS actions, P.LocationID, P.JobDesc, P.OrganisationID, L.SiteName, L.Address4, L.Address3, 
                      L.Address5, L.Telephone, L.PostalCode, C.ISOCode
 					  ,	(CASE WHEN (bfd_locdel.entityId IS NOT NULL OR bfd_perdel.entityId IS NOT NULL) THEN 1 ELSE 0 END) AS deleted
					  ,	(CASE WHEN (L.Active=1 AND P.Active=1) THEN 1 ELSE 0 END)  AS active
FROM Person AS P WITH (nolock) 
	INNER JOIN Location AS L WITH (nolock) ON P.LocationID = L.LocationID 
	INNER JOIN Country AS C WITH (nolock) ON L.CountryID = C.CountryID 
	LEFT OUTER JOIN UserGroup AS ug WITH (nolock) ON P.LastUpdatedBy = ug.UserGroupID
	LEFT OUTER JOIN (BooleanFlagData bfd_locdel INNER JOIN Flag f_locdel ON bfd_locdel.flagId=f_locdel.flagId AND f_locdel.flagTextId='DeleteLocation') on bfd_locdel.entityID = l.locationID
	LEFT OUTER JOIN (BooleanFlagData bfd_perdel INNER JOIN Flag f_perdel ON bfd_perdel.flagId=f_perdel.flagId AND f_perdel.flagTextId='DeletePerson') on bfd_perdel.entityID = p.personID

GO
