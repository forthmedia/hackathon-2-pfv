createPlaceHolderObject 'view', 'vRadioFlagGroupData'
GO
alter  view vRadioFlagGroupData with schemabinding

AS

/*
WAB 2016-11-15	New indexed view to improve performance of flagViews when retrieving radio group data
*/

select fg.flagGroupID, bfd.entityID, f.flagTextID, f.flagID, f.name 
from
	dbo.flagGroup fg
		inner join 
	dbo.flag f on fg.flagGroupID = f.flagGroupID  
		inner join 
	dbo.booleanFlagData bfd on f.flagID = bfd.flagid	
where 
	flagTypeID = 3
	

GO

CREATE UNIQUE CLUSTERED INDEX vRadioFlagGroupData
    ON vRadioFlagGroupData (flagGroupID, entityid);	

