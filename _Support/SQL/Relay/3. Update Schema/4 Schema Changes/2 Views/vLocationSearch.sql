IF EXISTS(SELECT * FROM sys.views WHERE name = 'vLocationSearch') 
	DROP VIEW dbo.vLocationSearch;
GO

CREATE VIEW dbo.vLocationSearch WITH SCHEMABINDING 
AS 
SELECT LocationID as EntityID, isNull(Address1,'') + cast(' ' as nVarchar(1)) + isNull(Address2,'') + cast(' ' as nVarchar(1)) + isNull(Address3,'') + cast(' ' as nVarchar(1)) + isNull(Address4,'') + cast(' ' as nVarchar(1)) + isNull(PostalCode,'') + cast(' ' as nVarchar(1)) + isNull(SiteName,'') + cast(' ' as nVarchar(1)) + isNull(Telephone,'') + cast(' ' as nVarchar(1)) + isNull(crmLocID,'') as searchableString
FROM dbo.location;
GO

CREATE UNIQUE CLUSTERED INDEX vLocationSearch_PK ON [dbo].vLocationSearch (EntityID);
GO