IF EXISTS (select 1 from sysObjects where name = 'vFinancialYear')
	DROP view vFinancialYear

GO

CREATE View
[dbo].[vFinancialYear]
AS

/*
vFinancialYear
WAB 2012-05-23 after an seed sown by MS
This is a view which gives details of the current,next and previous financial years
For use when needing to filter financial reports by year
Has various columns which can be used depending upn whether you are dealing with dates or year/month columns
Start Month of financial year is a setting reports.FinancialYearStartMonth ( defaults to 1)

Originally startDate was of data Type date, but then discovered that this was not supported in SQL 2005 

*/


select 
	RelativeYearName,
	convert(varchar,startYear) + case when startMonth <> 1 then '_' + right (convert(varchar,startYear + 1),2) else '' end as YearName,
	CONVERT(datetime, convert(varchar,startYear) + '-' +convert(varchar,startMonth) + '-01') as startDate,
	dateAdd(d,-1,CONVERT(datetime, convert(varchar,startYear + 1) + '-' +convert(varchar,startMonth) + '-01')) as endDate, -- this is time 00:00 on the last day of the period (so can't be used in a BETWEEN Statement)
	CONVERT(datetime, convert(varchar,startYear) + '-' +convert(varchar,startMonth) + '-01') as startDateTime,
	dateAdd(ms,-2,CONVERT(datetime, convert(varchar,startYear + 1) + '-' +convert(varchar,startMonth) + '-01')) as endDateTime,  -- This is 23:59:59.9998 on the last day of the period (so can be used in a BETWEEN Statement)  2 ms is the smallest number that can be subtracted

	startMonth,	
	startYear,
	endMonth,	
	startYear + 1 as endYear
	
	
from
(
	select  
		*,
		case when startMonth-1 = 0 then 12 else startMonth-1 end as endMonth,
		year(getdate()) - case when month(GETDATE()) >= startMonth then 0 else 1 end + yearDelta as startYear
	
	from 
		(
		select 
			RelativeDescription.*,
			case when settings.name is null then 1 else convert(int,value) end as startMonth
		from 
			(
				select -1 as yearDelta,'LastYear' as RelativeYearName
				Union
				select 0 as yearDelta,'ThisYear'
				Union
				select 1 as yearDelta,'NextYear'
			) as RelativeDescription  
				left join
				settings on settings.name = 'reports.financialYearStartMonth'	
		) as Dummy1

) as Dummy2

GO