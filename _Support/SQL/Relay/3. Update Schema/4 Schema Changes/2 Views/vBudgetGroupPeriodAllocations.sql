
/****** Object:  View [dbo].[vBudgetGroupPeriodAllocations]    Script Date: 04/18/2011 12:45:40 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vBudgetGroupPeriodAllocations]'))
DROP VIEW [dbo].[vBudgetGroupPeriodAllocations]
GO

/****** Object:  View [dbo].[vBudgetGroupPeriodAllocations]    Script Date: 04/18/2011 12:45:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBudgetGroupPeriodAllocations]
AS
SELECT     bgpa.budgetGroupPeriodAllocationID, bgpa.budgetGroupID, bgpa.budgetPeriodID, bgpa.amount, bgpa.authorised, bg.description AS budgetGroup, bg.countryId, 
                      bg.BudgetGroupTypeID, bgt.description AS BudgetGroupType, bp.description AS budgetPeriod, ISNULL
                          ((SELECT     SUM(dbo.BudgetPeriodAllocation.amount) AS Expr1
                              FROM         dbo.BudgetPeriodAllocation INNER JOIN
                                                    dbo.Budget ON dbo.BudgetPeriodAllocation.budgetID = dbo.Budget.budgetID
                              WHERE     (dbo.Budget.BudgetGroupID = bg.BudgetGroupID) AND (dbo.BudgetPeriodAllocation.budgetPeriodID = bp.budgetPeriodID) AND 
                                                    (dbo.BudgetPeriodAllocation.authorised = 1)), 0) AS amountAuthorised, ISNULL
                          ((SELECT     SUM(BudgetPeriodAllocation_1.amount) AS Expr1
                              FROM         dbo.BudgetPeriodAllocation AS BudgetPeriodAllocation_1 INNER JOIN
                                                    dbo.Budget AS Budget_1 ON BudgetPeriodAllocation_1.budgetID = Budget_1.budgetID
                              WHERE     (Budget_1.BudgetGroupID = bg.BudgetGroupID) AND (BudgetPeriodAllocation_1.budgetPeriodID = bp.budgetPeriodID) AND 
                                                    (BudgetPeriodAllocation_1.authorised = 0)), 0) AS amountNotAuthorised, bgpa.lastUpdated, 
                      dbo.Person.FirstName + ' ' + dbo.Person.LastName AS LastUpdatedByFullName, bgpa.lastUpdatedBy
FROM         dbo.BudgetGroupPeriodAllocation AS bgpa INNER JOIN
                      dbo.BudgetPeriod AS bp ON bgpa.budgetPeriodID = bp.budgetPeriodID INNER JOIN
                      dbo.BudgetGroup AS bg ON bgpa.budgetGroupID = bg.BudgetGroupID INNER JOIN
                      dbo.BudgetGroupType AS bgt ON bg.BudgetGroupTypeID = bgt.budgetTypeID LEFT OUTER JOIN
                      dbo.Person ON bgpa.lastUpdatedBy = dbo.Person.PersonID
GO

