

if exists (select 1 from sysobjects where name = 'vDataLoads' and type = 'v')
drop view  vDataLoads 

GO
CREATE VIEW vDataLoads AS
select description, countryDescription as default_country, 
	loadtable as load_table, datasourceID, dateReceived as Received, 
	dateLoaded as loaded, dlt.name as data_type, dlt.loadTypeGroup,  ISOCode, TypeTextID as organisation_Type
	from 
	dataSource ds 
		Left join dataLoadType dlt on ds.dataloadTypeID = dlt.dataloadTypeID
	left outer join organisationtype on ds.organisationtypeID = organisationtype.organisationtypeID
	left outer join country on defaultCountryID = countryID