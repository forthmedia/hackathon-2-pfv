ALTER VIEW [dbo].[vSpecialisationSummary] AS
/*
2009/03/13  NYB Altered 
2009/08/20  NYB LHID2539
2010/09/15  WAB 8.3 Testing.  This view seemed to have lost columns num_modules_passed and num_modules_to_pass when previous bug fixed
*/
SELECT     s.SpecialisationID, 
				CASE WHEN es.expiredDate IS NOT NULL THEN 'Expired' else Case WHEN es.passDate IS NOT NULL THEN 'Passed' END END AS status, 
				es.passDate AS pass_date, 
				es.expiredDate AS expired_date, 
				(select sum(numOfCertifications) from SpecialisationRule sr where sr.active=1 and sr.SpecialisationID=s.SpecialisationID) AS num_Certifications_To_Pass,
				es.entityID, es.entityTypeID, 
				count(distinct(v2.personCertificationID)) as num_Certifications_Passed,
				es.entitySpecialisationID, 
				s.Code,
				'phr_title_specialisation_'+cast(s.specialisationID as varchar(50)) AS Title,
				'phr_description_specialisation_'+cast(s.specialisationID as varchar(50)) AS Description,
				--'phr_' + s.titlePhraseTextID as Title, 
				--'phr_' + s.descriptionPhraseTextID as Description, 
				s.active, 
				s.countryID,
				SUM(v2.Modules_Passed) AS num_modules_passed, 
				SUM(v2.num_modules_to_pass) AS num_modules_to_pass
	FROM        dbo.Specialisation s WITH (noLock) 
	INNER JOIN	dbo.entitySpecialisation AS es WITH (noLock) ON s.SpecialisationID = es.specialisationID 
	LEFT JOIN	SpecialisationRule sr ON sr.SpecialisationID=s.SpecialisationID AND sr.active=1
	LEFT JOIN	(person p2 
		inner join vTrngCertificationSummary v2 on p2.personid=v2.personid and v2.status='Passed'
		inner join SpecialisationRule sr2 on v2.certificationid=sr2.certificationid and sr2.active!=0 
		) on p2.organisationid=es.entityID and sr2.specialisationid=s.SpecialisationID
	WHERE		s.active=1
GROUP BY s.SpecialisationID, es.passDate, es.expiredDate, es.entityID, es.entityTypeID, es.entitySpecialisationID, s.Code, 'phr_' + s.titlePhraseTextID	, 'phr_' + s.descriptionPhraseTextID, s.countryID, s.active

