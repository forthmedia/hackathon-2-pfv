ALTER
 view [dbo].[vIncentiveApprovals]
AS
select p.firstname + ' ' + p.lastname as Name, l.sitename as Company, 
c.countrydescription as Country, b.created as Reg_Date,
p.organisationid as OrgID, p.personID as PersonID, (
dbo.BooleanFlagList (777,p.personID,default)) as FlagData
from person p
join location l on l.locationid = p.locationid
join country c on c.countryid = l.countryid
join booleanflagdata b on b.entityid = p.personid
join flag f on f.flagid = b.flagid
where f.flagtextid in ('incetiveRegistration')
and p.personid not in (select personid from rwpersonaccount)