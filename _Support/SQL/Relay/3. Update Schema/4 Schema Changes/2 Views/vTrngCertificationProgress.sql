ALTER view  [dbo].[vTrngCertificationProgress]
as
/* NYB 2009-08-03 SNY047 replaced qd.QuizName with: 'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) */
SELECT     'phr_title_trngCourse_'+cast(tmc.courseId as varchar(50)) AS Title_of_Course, --tc.AICCCourseTitle AS Title_of_Course, 
			tm.AICCFileName AS FileName, 
			'phr_title_trngModule_'+cast(tm.moduleId as varchar(50)) AS Title_of_Module,--tm.AICCTitle AS Title_of_Module, 
			tm.AICCType AS Module_Type, 
           tm.AICCMaxScore AS Maximum_Score, tm.AICCMaxTimeAllowed AS Maximum_Time_Allowed, tc.courseID,
		   tm.moduleID, tm.moduleCode as Module_Code, tm.moduleDuration as Duration, tcc.completedDate, 
           'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) AS Associated_Quiz, qd.QuizDetailID, tcc.personid as student_personID
FROM       dbo.trngModule AS tm 
		INNER JOIN trngModuleCourse AS tmc ON tm.moduleID = tmc.moduleID
		INNER JOIN trngCourse AS tc ON tmc.courseID = tc.courseId 
		LEFT OUTER JOIN QuizDetail qd ON tm.quizDetailID = qd.QuizDetailID
		LEFT OUTER JOIN trngCourseCertification tcc ON tmc.courseID = tcc.courseID

GO