IF EXISTS (select 1 from sysobjects where name = 'vCountryRights')
	DROP view vCountryRights

GO


CREATE view [dbo].[vCountryRights]
AS
/* WAB 2012-07-03 a view onto the vRights view giving all the countries a person has rights to */

SELECT  
	personid,CountryID,permission, level1, level2, level3, level4, level5 
FROM
	vRights
WHERE
	shortName = 'RecordTask'
