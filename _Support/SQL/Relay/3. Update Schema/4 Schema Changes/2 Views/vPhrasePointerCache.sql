if exists (select 1 from sysobjects where name = 'vphrasePointerCache')
drop view [dbo].[vphrasePointerCache]
GO
CREATE VIEW [dbo].[vphrasePointerCache]
	AS
/*
WAB 2013-03-20 Added p.lastupdated for use in verity views
*/

	select phrasetextid, entityid, entityTypeid, pl.phraseid, phrase_ident, phrasetext, pp.countryid, pp.languageid, p.lastupdated
	
	from 
		phraselist pl 
			inner join 
		phrasePointerCache pp on pl.phraseid = pp.phraseid
			inner join
		phrases p on pp.phrase_ident = p.ident
GO
