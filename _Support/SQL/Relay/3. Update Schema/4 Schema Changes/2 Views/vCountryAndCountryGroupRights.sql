/* This view provides all countries and country group rights a person may have. A country group rights are the MOST rights a person has 
 * in any country in that group
 *  
 * Note that this works without unions because all countries are also country groups containing only themselves
*/


IF EXISTS(SELECT * FROM sys.views WHERE name = 'vCountryAndCountryGroupRights') 
	DROP VIEW dbo.vCountryAndCountryGroupRights;
GO

Create view [dbo].[vCountryAndCountryGroupRights]
as
select distinct personid,CountryGroup.CountryGroupID as CountryID, Max(cast (level1 as tinyInt)) as level1, Max(cast (level2 as tinyInt)) as level2, Max(cast (level3 as tinyInt)) as level3,MAX(cast (level4 as tinyInt)) as level4
from vCountryRights
inner join country on vCountryRights.CountryID=country.countryID
inner join CountryGroup on country.CountryID=CountryGroup.CountryMemberID
group by personid,CountryGroup.CountryGroupID
GO