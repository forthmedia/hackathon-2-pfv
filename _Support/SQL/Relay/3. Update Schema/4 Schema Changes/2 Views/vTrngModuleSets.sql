/* 2013-04-11 STCR CASE 432194 - use trngModuleSet.description_defaultTranslation */
/* 2013-08-28 YMA CASE 436710 - trngModuleSet.description_defaultTranslation should be referenced as description to avoid breaking anything which was looking at description e.g. report designer domains. */

IF EXISTS (select 1 from sysobjects where name = 'vTrngModuleSets')
	DROP VIEW vTrngModuleSets	
GO

CREATE VIEW vTrngModuleSets
AS
SELECT     tcr.certificationRuleID, tms.moduleSetID, tms.description_defaultTranslation as description, tms.active,
                          (SELECT     COUNT(moduleID) AS Expr1
                            FROM          dbo.trngModuleSetModule
                            WHERE      (moduleSetID = tms.moduleSetID)) AS numModulesInModuleSet, tms.TrainingProgramID
FROM         dbo.trngModuleSet AS tms LEFT OUTER JOIN
                      dbo.trngCertificationRule AS tcr ON tms.moduleSetID = tcr.moduleSetID
GROUP BY tms.moduleSetID, tms.description_defaultTranslation, tms.active, tcr.certificationRuleID, tms.TrainingProgramID

GO