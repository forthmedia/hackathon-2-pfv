/* update view to use currency of each opp */
if exists (select 1 from information_schema.views where table_name = 'vOpportunityProductPipeline')
drop view vOpportunityProductPipeline
GO

CREATE VIEW [dbo].[vOpportunityProductPipeline]
AS
SELECT     o.opportunityID AS OPPORTUNITY_ID, o.opportunityID, op.subtotal, org.OrganisationName AS ACCOUNT, o.detail, org.OrganisationID AS ENTITYID, 
                      per.FirstName + ' ' + per.LastName AS ACCOUNT_MANAGER, preper.FirstName + ' ' + preper.LastName AS Sales_MANAGER, 
                      c.CountryDescription AS COUNTRY, os.status AS OPPORTUNITY_STATUS, os.OpportunityStageID AS opp_StatusID, org.countryID, 
                      op.forecastShipDate AS FORECAST_SHIP_DATE, o.lastupdated AS LAST_UPDATED, o.currency as CURRENCY, o.expectedCloseDate as expected_close_date
FROM         dbo.opportunityProduct AS op INNER JOIN
                      dbo.opportunity AS o ON o.opportunityID = op.OpportunityID INNER JOIN
                      dbo.organisation AS org ON org.OrganisationID = o.entityid INNER JOIN
                      dbo.Country AS c ON c.CountryID = org.countryID INNER JOIN
                      dbo.OppStage AS os ON os.OpportunityStageID = o.stageID LEFT OUTER JOIN
                      dbo.Person AS per ON per.PersonID = o.vendorAccountManagerPersonID LEFT OUTER JOIN
                      dbo.Person AS preper ON preper.PersonID = o.vendorSalesManagerPersonID
GROUP BY op.subtotal, org.OrganisationName, o.detail, org.OrganisationID, per.FirstName, per.LastName, preper.FirstName, preper.LastName, os.status, 
                      os.OpportunityStageID, op.probability, op.forecastShipDate, org.countryID, o.opportunityID, c.CountryDescription, op.OpportunityID, 
                      op.forecastShipDate, o.lastupdated, o.currency, o.expectedCloseDate
GO
