if exists (select 1 from sysObjects where name = 'flagEntityType')
	DROP view flagEntityType

GO
/****** Object:  View dbo.flagEntityType    Script Date: 22/10/02 17:42:16 ******/

CREATE    VIEW dbo.flagEntityType
AS
SELECT entitytypeid, tablename=entityname, isnull(label,entityname) as description, NameExpression, UniqueKey
FROM dbo.schemaTable
WHERE (FlagsExist = 1)

GO