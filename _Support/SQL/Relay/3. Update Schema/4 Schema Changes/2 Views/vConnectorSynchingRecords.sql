/****** Object:  View [dbo].[vConnectorSynchingRecords]    Script Date: 04/11/2014 14:08:57 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorSynchingRecords]'))
DROP VIEW [dbo].[vConnectorSynchingRecords]
GO

/****** Object:  View [dbo].[vConnectorSynchingRecords]    Script Date: 04/11/2014 14:08:57 ******/
/* SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


don't synch any records where the following are true:
	1. Max synch attempts reached
	2. Record has errored
	3. There are dependancies in the queue 
	4. It is an admin person/company
	5. Another reason why a record may not synch, which is currently not in here, is the suspended flag. Would have to do too many joins to work this out and am worried this will
		impede performance. For this reason, this is done in the actual import/export views.
		
	Please note: If changing one case statement below, update the other!

CREATE VIEW [dbo].[vConnectorSynchingRecords]
AS
SELECT     q.isDeleted, q.object, q.objectID, q.connectorType, q.connectorResponseID,
			case 
				when not (q.synchAttempt < ISNULL(dbo.getSettingValue(N'connector.'+q.connectorType+'.maxSynchAttempts'), 5)) then 'Max Synch Attempts Reached'
				when not (q.errored = 0) then 'Errored'
				when not (queueDependency.dependentOnConnectorQueueID IS NULL) then 'Has Dependency'
				when not (isNull(ot.typeTextID,'') != 'AdminCompany') then 'Is Admin Company'
				else null
			end as message,
			case 
				when not (q.synchAttempt < ISNULL(dbo.getSettingValue(N'connector.'+q.connectorType+'.maxSynchAttempts'), 5)) then 0
				when not (q.errored = 0) then 0
				when not (queueDependency.dependentOnConnectorQueueID IS NULL) then 0
				when not (isNull(ot.typeTextID,'') != 'AdminCompany') then 0
				else 1
			end as canSynch,
			case when error.ID is not null then 1 else 0 end as hasError
FROM         dbo.vConnectorQueue AS q 
					left join connectorQueueDependency AS queueDependency ON queueDependency.connectorQueueID = q.ID
					left join connectorQueueError as error on error.connectorQueueID = q.ID
					left join (person p inner join location pl on p.locationID = pl.locationID) on cast(p.personID as varchar) = q.objectID and q.object='person'
					left join location l on cast(l.locationID as varchar) = q.objectID and q.object='location'
					left join organisation o on cast(o.organisationID as varchar) = q.objectID and q.object='organisation'
					left join organisationType ot on ot.organisationTypeID in (pl.accountTypeId,l.accountTypeId,o.organisationTypeID)
*/