/*
WAB 2013-02-26
Created a view on ModRegister to join to lots of useful tables
Long overdue!
Please add any fields which might be of use

2013-03-10 WAB 	Add resolving of fullname
				Hide passwords 
2013-09-25	WAB CASE 437028 Mods to FieldName and OldVal/NewVal returned when dealing with boolean flags (to mimic code which was in modifications.cfm) FieldName is the FlagGroup Name and the Old or New Value is the Flag Name 
2016-07-05	WAB	Added a FieldName column (there was already a Field column).  FieldName is the TextID whereas Field contains the actual names of flags and flagGroups
*/

createPlaceHolderObject 'view', 'vModRegister'

GO


ALTER VIEW vModRegister
AS
SELECT 
	moddate, 
	mr.entityTypeID , 
	recordid, 
	action,  
	mr.modentityid,
	mr.flagid,
	CASE WHEN mr.flagid is NOT null THEN CASE WHEN  f.datatable = 'boolean' THEN f.flagGroup else f.flagGroup + ' - ' + f.flag END ELSE fieldname end as field, 
	CASE WHEN mr.flagid is NOT null THEN CASE WHEN  f.datatable = 'boolean' THEN case when f.flagGroupTextID <> '' then f.flagGroupTextID else 'flagGroup_' + convert(varchar,f.flagGroupID) end else case when f.flagTextID <> '' then f.flagTextID else 'flag_' + convert(varchar,f.flagID) end  END ELSE fieldname end as fieldName, 
	CASE WHEN med.fieldname = 'password' THEN '****' WHEN f.datatable = 'boolean' and action = 'FD' THEN f.flag ELSE oldval END as oldval, 
	CASE WHEN med.fieldname = 'password' THEN '****' WHEN f.datatable = 'boolean' and action = 'FA' THEN f.flag ELSE newval END as newval, 

	case 
		when p.entityid is not null then p.name
		else ug.name end 
		as fullname, 
	entityName as EntityType, 
	f.flagGroupid,
	f.dataType,
	actionByCF, 
	ActionByUser, 
	ActionByPerson, 
	recordid as entityid,
	mr.id
FROM
	modregister mr
		inner join 
	modentitydef med on mr.modentityid = med.modentityid
		left join 
	vflagDef f on mr.flagid = f.flagid
		inner join 
	schemaTable st on st.entityTypeID = mr.entityTypeID
		left join 
	usergroup ug on ug.usergroupid = actionbyCF
		left join 
	vEntityName p on p.entityTypeID = 0 and p.entityid = actionByPerson
	

GO

