IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vFundApprovers]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[vFundApprovers]

GO
CREATE VIEW [dbo].[vFundApprovers]
AS
SELECT     o.fundApproverID, p.FirstName + ' ' + p.LastName AS Approver, fl.approvalLevel as approverlevel,fl.approvalLevelPhrase, p.PersonID, p.Email, c.CountryDescription, fl.lowerApprovalThreshold, fl.upperApprovalThreshold
FROM         dbo.fundApprover AS o INNER JOIN
                      dbo.Person AS p ON p.PersonID = o.personID INNER JOIN
                      dbo.Country AS c ON c.CountryID = o.countryID INNER JOIN
                      dbo.fundApprovalLevel AS fl ON o.fundApprovalLevelID = fl.fundApprovalLevelID
UNION
SELECT     o.fundApproverID, p.FirstName + ' ' + p.LastName AS Approver, fl.approvalLevel as approverlevel,fl.approvalLevelPhrase, p.PersonID, p.Email, 'All Countries' AS countrydescription, fl.lowerApprovalThreshold, fl.upperApprovalThreshold
FROM         dbo.fundApprover AS o INNER JOIN
                      dbo.Person AS p ON p.PersonID = o.personID INNER JOIN
                      dbo.fundApprovalLevel AS fl ON o.fundApprovalLevelID = fl.fundApprovalLevelID
WHERE     (o.countryID = 0)


GO