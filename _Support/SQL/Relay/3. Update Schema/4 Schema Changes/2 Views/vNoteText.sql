if exists (select TABLE_NAME from INFORMATION_SCHEMA.VIEWS 
		where TABLE_NAME = 'vNoteText') 
	DROP VIEW vNoteText
GO

	
CREATE VIEW vNoteText
	AS
	select entityID as orgID, cast (noteText as varchar(2000)) as note_Text, null as personid, 
		null as PERSON, 'Organisation' as note_type 
	from entityNote with(nolock)
		where entityTypeID = 2
	UNION
	select organisationID as orgID, cast (noteText as varchar(2000)) as note_Text, personid, 
		firstname +' ' +lastname as PERSON, 'Person' as note_type 
	from entityNote with(nolock)
		INNER JOIN person with(nolock) ON personID = entityID
		where entityTypeID = 0
	UNION
	/* NJH 2012/06/08 - removed the commDetail union as it didn't look useful and it was bringing the system to its knees
	select organisationID as orgID, cast (cd.feedBack as varchar(2000)) as note_Text,  
		p.personid, firstname +' ' +lastname as PERSON, 'Contact' as note_type 
	from commdetail cd with(nolock)
		INNER JOIN person p with(nolock) ON p. personID = cd.personid
		where cd.commTypeID > 7
	UNION*/
	select entityID as orgID, cast (notes as varchar(2000)) as note_Text,  
		null as personid, null as PERSON, 'Action' as note_type 
	from actions with(nolock)
		where entityTypeID = 2
	UNION
	select p.organisationID as orgID, cast (a.notes as varchar(2000)) as note_Text,  
		a.entityID as personid, firstname +' ' +lastname as PERSON, 'Action note' as note_type  
	from actions a with(nolock)
		INNER JOIN person p with(nolock) ON p.personID = a.entityID
		where a.entityTypeID = 0 

GO