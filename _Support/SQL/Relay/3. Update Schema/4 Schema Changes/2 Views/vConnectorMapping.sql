
/****** Object:  View [dbo].[vConnectorMapping]    Script Date: 04/11/2014 14:06:35 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vConnectorMapping]'))
DROP VIEW [dbo].[vConnectorMapping]
GO

/****** Object:  View [dbo].[vConnectorMapping]    Script Date: 04/11/2014 14:06:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vConnectorMapping]
AS
SELECT     m.ID, s.entitytypeid, rwObj.object AS object_relayware, rwobj.ID as objectID_relayware, obj.object as object_remote, obj.ID as objectID_remote, m.column_relayware, m.mappedRelaywareColumn, m.column_remote, m.isRemoteID,
		case when rwObj.active = 1 and obj.active = 1 and m.active = 1 then 1 else 0 end as active, rwObj.connectorType, 
                      CASE WHEN isNull(m.direction, 'E') = 'E' AND rwObj.synch = 1 THEN 1 ELSE 0 END AS export, CASE WHEN isNull(m.direction, 'I') = 'I' AND obj.synch = 1 THEN 1 ELSE 0 END AS import, 
                      m.emptyStringAsNull, m.mappingType, m.required, m.canEdit, m.defaultImportValue, m.defaultExportValue
FROM         dbo.connectorMapping AS m INNER JOIN
                      dbo.connectorObject AS rwObj ON rwObj.ID = m.connectorObjectID_relayware INNER JOIN
                      dbo.connectorObject AS obj ON m.connectorObjectID_remote = obj.ID INNER JOIN
                      dbo.schemaTable AS s ON s.entityName = rwObj.object

GO