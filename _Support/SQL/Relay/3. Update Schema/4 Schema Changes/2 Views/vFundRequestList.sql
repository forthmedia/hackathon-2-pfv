ALTER VIEW [dbo].[vFundRequestList]
AS
SELECT     fr1.fundRequestID, fca.AccountID, bp.budgetPeriodID, fr1.fundRequestID AS Request_ID, bp.description AS period, dbo.organisation.OrganisationName AS Account, 
                      dbo.organisation.countryID, c.CountryDescription AS country, bh.description AS BudgetGroup, bh.BudgetGroupID, b.budgetID, ISNULL
                          ((SELECT     SUM(fraa.amount) AS Expr1
                              FROM         dbo.fundRequestActivityApproval AS fraa INNER JOIN
                                                    dbo.fundRequestActivity AS fra ON fraa.fundRequestActivityID = fra.fundRequestActivityID INNER JOIN
                                                    dbo.fundRequest AS fr ON fr.fundRequestID = fra.fundRequestID
                              WHERE     (fr.fundRequestID = fr1.fundRequestID)), 0) AS Sum_approved,
                          (SELECT     SUM(requestedAmount) AS Expr1
                            FROM          dbo.fundRequestActivity AS fundRequestActivity_1
                            WHERE      (fundRequestID = fr1.fundRequestID)) AS Sum_requested,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.fundRequestActivity AS fundRequestActivity_1
                            WHERE      (fundRequestID = fr1.fundRequestID)) AS count_requests, fca.OrganisationID, CASE WHEN b.entityTypeID = 2 AND 
                      bpa.authorised = 1 THEN bpa.amount ELSE NULL END AS orgBudgetAllocation, bpa.budgetPeriodAllocationID
FROM         dbo.FundCompanyAccount AS fca INNER JOIN
                      dbo.organisation ON fca.OrganisationID = dbo.organisation.OrganisationID INNER JOIN
                      dbo.Country AS c ON c.CountryID = dbo.organisation.countryID INNER JOIN
                      dbo.FundAccountBudgetAccess AS faba ON faba.accountID = fca.AccountID AND faba.budgetID = fca.budgetID INNER JOIN
                      dbo.Budget AS b ON b.budgetID = faba.budgetID INNER JOIN
                      dbo.BudgetGroup AS bh ON b.BudgetGroupID = bh.BudgetGroupID INNER JOIN
                      dbo.BudgetPeriodAllocation AS bpa ON bpa.budgetID = b.budgetID INNER JOIN
                      dbo.BudgetPeriod AS bp ON bp.budgetPeriodID = bpa.budgetPeriodID LEFT OUTER JOIN
                      dbo.fundRequest AS fr1 ON fca.AccountID = fr1.accountID AND bp.budgetPeriodID = fr1.budgetPeriodID

GO