
/****** Object:  View [dbo].[vFundRequestActivities]    Script Date: 05/23/2011 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vFundRequestActivities]'))
DROP VIEW [dbo].[vFundRequestActivities]
GO

/****** Object:  View [dbo].[vFundRequestActivities]    Script Date: 05/23/2011 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vFundRequestActivities]
AS
SELECT     fa.AccountID, fa.OrganisationID, fa.Account, fa.country, a.partnerOrganisationID, a.fundRequestActivityID, a.requestedAmount, a.totalCost, ISNULL(aa.amount, 0) 
                      AS approvedAmount, a.fundRequestID, a.fundRequestTypeID, a.fundApprovalStatusID, a.typeDescription, a.startDate, a.endDate, a.createdBy, a.created, a.updatedBy, 
                      a.updated, 'phr_title_fundApprovalStatus_' + convert(VarChar,fas.fundApprovalStatusID) AS status, 'phr_title_fundRequestType_' + convert(VarChar,frt.fundRequestTypeID) AS requestType, a.fundRequestTypeID AS Expr1, 
                      CASE WHEN (fas.canEditActivity = 1 and fca.AccountClosed = 0) THEN 'phr_Edit' ELSE 'phr_View' END AS Edit_View
FROM         dbo.fundRequestActivity AS a INNER JOIN
                      dbo.fundApprovalStatus AS fas ON a.fundApprovalStatusID = fas.fundApprovalStatusID INNER JOIN
                      dbo.fundRequestType AS frt ON frt.fundRequestTypeID = a.fundRequestTypeID LEFT OUTER JOIN
                      dbo.fundRequestActivityApproval AS aa ON aa.fundRequestActivityID = a.fundRequestActivityID INNER JOIN
                      dbo.fundRequest AS fr ON fr.fundRequestID = a.fundRequestID INNER JOIN
                      dbo.vFundAccounts AS fa ON fa.AccountID = fr.accountID INNER JOIN
                      dbo.FundCompanyAccount AS fca ON fca.AccountID = fr.accountID				  
