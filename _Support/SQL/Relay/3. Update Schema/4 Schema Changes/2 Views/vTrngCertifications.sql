IF EXISTS (select 1 from sysobjects where name = 'vTrngCertifications')
	DROP VIEW vTrngCertifications
GO

CREATE VIEW vTrngCertifications
AS

/*
2012-11-27 PPB  Case 432253 extended the join into trngCertificationRule so that only ACTIVE modulesets are included
2013-05-14 STCR Case 432193 include trngCertificationRule.activationDate so that vTrngCertificationSummary can filter on it 
*/

SELECT     tc.certificationID, tc.SortOrder,
                          (SELECT     COUNT(certificationRuleID) AS Expr1
                            FROM          dbo.trngCertificationRule AS tcr1 WITH (noLock)
                            WHERE      (certificationID = tc.certificationID)) AS numRules,
                          (SELECT     COUNT(certificationRuleID) AS Expr1
                            FROM          dbo.trngCertificationRule AS tcr2 WITH (noLock)
                            WHERE      (certificationID = tc.certificationID) AND (active = 1)) AS numRulesToPass, 'phr_title_trngCertification_' + CONVERT(varchar, tc.certificationID) AS title, 
                      'phr_description_trngCertification_' + + CONVERT(varchar, tc.certificationID) AS description, tc.duration, tc.countryID, tc.code, tcr.numModulesToPass, 
                      tcr.active AS ruleActive, tcr.activationDate AS ruleActivationDate, c.CountryDescription AS country, tc.active,
                          (SELECT     COUNT(tcr3.certificationRuleID) AS Expr1
                            FROM          dbo.trngCertificationRule AS tcr3 WITH (noLock) INNER JOIN
                                                   dbo.trngCertificationRuleType AS tcrt WITH (noLock) ON tcr3.certificationRuleTypeID = tcrt.certificationRuleTypeID AND tcrt.Description = 'Update'
                            WHERE      (tcr3.certificationID = tc.certificationID)) AS numUpdateRules, tct.description AS certificationType, tc.certificationTypeID, tc.TrainingProgramID
FROM         dbo.trngCertification AS tc WITH (noLock) INNER JOIN
                      dbo.Country AS c WITH (noLock) ON tc.countryID = c.CountryID INNER JOIN
                      dbo.trngCertificationType AS tct WITH (noLOck) ON tct.certificationTypeID = tc.certificationTypeID 
                      LEFT OUTER JOIN (dbo.trngCertificationRule tcr  WITH (noLock) INNER JOIN dbo.trngModuleSet tms WITH (noLock) ON tms.ModuleSetId=tcr.ModuleSetId AND tms.active<>0) ON tcr.certificationID = tc.certificationID
GROUP BY tc.certificationID, tc.SortOrder, tc.descriptionPhraseTextID, tc.titlePhraseTextID, tc.duration, tc.countryID, tc.code, tcr.numModulesToPass, tcr.active, tcr.activationDate,
                      c.CountryDescription, tc.active, tct.description, tc.certificationTypeID, tcr.certificationRuleID, tc.TrainingProgramID

GO