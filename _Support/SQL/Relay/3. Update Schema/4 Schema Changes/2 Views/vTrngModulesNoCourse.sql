/*********************
2016-05-26 atrunov  PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules (added hasRecordRightsBitMask)
**********************/
IF EXISTS (select 1 from sysObjects where name = 'vTrngModulesNoCourse' and xType='v')
	DROP view vTrngModulesNoCourse

GO

CREATE VIEW vTrngModulesNoCourse
AS
      
SELECT     'phr_elearning_noCourse' AS Title_of_Course, tm.AICCFileName AS FileName, 'phr_title_trngModule_' + CONVERT(VarChar, tm.moduleID) AS Title_of_Module, 
                      'phr_description_trngModule_' + CONVERT(VarChar, tm.moduleID) AS Description_of_Module, tm.AICCType AS Module_Type, tm.AICCMaxScore AS Maximum_Score, 
                      tm.AICCMaxTimeAllowed AS Maximum_Time_Allowed, tm.moduleID, tm.moduleCode AS Module_Code, tm.moduleDuration AS Duration, tm.quizDetailID, tm.CountryID, 
                      dbo.QuizDetail.title_defaultTranslation AS Associated_Quiz, tm.fileID, tm.SortOrder, tm.publishedDate, tc.courseId, tm.points, ISNULL(tc.active, 1) AS Active, tm.availability, 0 as solutionAreaId, 
                      '' AS Solution_Area, tm.mobileCompatible, tm.TrainingProgramID, tm.isSCORM, tc.TrainingProgramID as courseTrainingProgramID, tm.coursewareTypeID, tm.hasRecordRightsBitMask as moduleHasRecordRightsBitMask, tc.hasRecordRightsBitMask as courseHasRecordRightsBitMask
FROM         dbo.trngModule AS tm LEFT OUTER JOIN
			 dbo.trngModuleCourse AS tmc ON tm.moduleID = tmc.moduleID
			 LEft JOIN
                      dbo.trngCourse AS tc ON tmc.courseID = tc.courseId  
                      LEFT OUTER JOIN
                      dbo.QuizDetail ON tm.quizDetailID = dbo.QuizDetail.quizDetailID
				where tmc.courseId is null

GO
