
IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vEntityName]'))
DROP VIEW [dbo].[vEntityName]
GO

create view [dbo].[vEntityName] as 
select entityId, name,entityTypeID,deleted,crmID from dbo.entityName