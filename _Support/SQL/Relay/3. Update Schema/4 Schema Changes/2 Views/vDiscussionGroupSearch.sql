IF EXISTS(SELECT * FROM sys.views WHERE name = 'vDiscussionGroupSearch') 
	DROP VIEW dbo.vDiscussionGroupSearch;
GO
CREATE VIEW dbo.vDiscussionGroupSearch WITH SCHEMABINDING 
AS 
SELECT g.discussionGroupID, g.title +  cast(' ' as nVarchar(1)) + g.description  as searchableString
FROM dbo.discussionGroup as g
WHERE g.active = 1
GO
CREATE UNIQUE CLUSTERED INDEX vDiscussionGroupSearch_PK ON vDiscussionGroupSearch (discussionGroupID);
GO