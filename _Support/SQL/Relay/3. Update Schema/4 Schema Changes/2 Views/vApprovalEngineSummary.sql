IF EXISTS(SELECT * FROM sys.views WHERE name = 'vApprovalEngineSummary') 
	DROP VIEW vApprovalEngineSummary;
GO

CREATE VIEW vApprovalEngineSummary
AS
	SELECT		dbo.approvalEngine.approvalEngineID,
				dbo.approvalEngine.title,
				approvalEngineEntity.entityTypeID as approvalEngineEntityTypeID,
				approvalEngineEntity.entityname as approvalEngineEntityType,
				rejectedWorkflow.processID as approvalEngineRejectedWorkflowID,
				rejectedWorkflow.processDescription as approvalEngineRejectedWorkflow,
				approvedWorkflow.processID as approvalEngineApprovedWorkflowID,
				approvedWorkflow.processDescription AS approvalEngineApprovedWorkflow,
				revisionWorkflow.processID as approvalEngineRevisionWorkflowID,
				revisionWorkflow.processDescription AS approvalEngineRevisionWorkflow,
				dbo.eMailDef.emailDefID as approvalRequestEmailID,
				dbo.eMailDef.eMailTextID as approvalRequestEmail,
				dbo.screens.screenID as approvalReviewScreenID,
				dbo.screens.screenTextID as approvalReviewScreenTextID,
				dbo.screens.screenname as approvalReviewScreen,
				dbo.approvalEngine.Created as approvalEngineCreated,
				dbo.approvalEngine.CreatedBy as approvalEngineCreatedBy,
				approvalStatusApproversCount.numApprovers as numApprovers,
				isnull(max(approvalEngineApprover.approverLevel),0) as maxLevel,
				approvalStatusApprovalCount.numApprovals as numRequestsMade
	FROM         dbo.approvalEngine LEFT OUTER JOIN
						  dbo.approvalEngineApprover ON dbo.approvalEngine.approvalEngineID = dbo.approvalEngineApprover.approvalEngineID LEFT OUTER JOIN
						  (select approvalengineID, count(approvalEngineApproverID) as numApprovers 
							from approvalEngineApprover
							group by approvalengineID) as approvalStatusApproversCount on dbo.approvalEngine.approvalEngineID = approvalStatusApproversCount.approvalEngineID LEFT OUTER JOIN
						  dbo.approvalStatus ON dbo.approvalEngine.approvalEngineID = dbo.approvalStatus.approvalEngineID LEFT OUTER JOIN
						  (select approvalengineID, count(entityid) as numApprovals
							from (select distinct approvalengineID, entityid as entityid
							from approvalStatus) as aSt
							group by approvalengineID) as approvalStatusApprovalCount on dbo.approvalEngine.approvalEngineID = approvalStatusApprovalCount.approvalEngineID LEFT OUTER JOIN
						  dbo.schemaTable AS approvalEngineEntity ON dbo.approvalEngine.entityTypeID = approvalEngineEntity.entitytypeid LEFT OUTER JOIN
						  dbo.processHeader AS rejectedWorkflow ON dbo.approvalEngine.rejectedWorkflowID = rejectedWorkflow.processID LEFT OUTER JOIN
						  dbo.processHeader AS approvedWorkflow ON dbo.approvalEngine.approvedWorkflowID = approvedWorkflow.processID LEFT OUTER JOIN
						  dbo.processHeader AS revisionWorkflow ON dbo.approvalEngine.revisionWorkflowID = revisionWorkflow.processID LEFT OUTER JOIN
						  dbo.eMailDef ON dbo.approvalEngine.approvalRequestEmailID = dbo.eMailDef.emailDefID LEFT OUTER JOIN
						  dbo.schemaTable AS approvalEngineApproverEntity ON dbo.approvalEngineApprover.entityTypeID = approvalEngineApproverEntity.entitytypeid LEFT OUTER JOIN
						  dbo.screens ON dbo.approvalEngine.approvalReviewScreenID = dbo.screens.screenID
						   LEFT OUTER JOIN dbo.usergroup on dbo.usergroup.usergroupID = dbo.approvalEngine.CreatedBy
	group by dbo.approvalEngine.title,dbo.approvalEngine.approvalEngineID,approvalEngineEntity.entityname,rejectedWorkflow.processDescription
	,approvedWorkflow.processDescription, dbo.eMailDef.eMailTextID, dbo.approvalEngine.Created, dbo.approvalEngine.CreatedBy,
	approvalEngineEntity.entityTypeID, rejectedWorkflow.processID, approvedWorkflow.processID, dbo.eMailDef.emailDefID,dbo.screens.screenID,dbo.screens.screenTextID,dbo.screens.screenName
	,revisionWorkflow.processID ,revisionWorkflow.processDescription, approvalStatusApprovalCount.numApprovals, approvalStatusApproversCount.numApprovers
GO