
/* NJH 2010/11/24 LID 4925 - add SKU_ParentSKUGroup to vOrderItems 
	NJH 2012/09/26 - removed the orderItem.* and replaced with column names
*/
ALTER  VIEW [dbo].[vOrderItems]
AS
SELECT     dbo.orderItem.Itemid, dbo.orderItem.OrderID,	dbo.orderItem.SKU,dbo.orderItem.Quantity,
			dbo.orderItem.UnitDiscountPrice,dbo.orderItem.TotalDiscountPrice,dbo.orderItem.Tax,
			dbo.orderItem.Createdby,dbo.orderItem.created,dbo.orderItem.LastUpdatedby,
			dbo.orderItem.LastUpdated,dbo.orderItem.QuantityShipped,dbo.orderItem.SortOrder,
			dbo.orderItem.Activeold,dbo.orderItem.Active,dbo.orderItem.unitlistprice,
			dbo.orderItem.totallistprice,dbo.orderItem.ProductID,dbo.orderItem.PriceISOCode,
			dbo.orderItem.ShippedDate,dbo.orderItem.DistiDiscount,dbo.orderItem.ResellerDiscount,
			dbo.orderItem.Shipper,dbo.orderItem.ShipperRef,dbo.orderItem.orderItemStatusID,
			dbo.orderItem.voucherCode,dbo.orderItem.voucherValue,dbo.orderItem.parentSKUGroup,
			dbo.OrderItem.SKU + '_' + dbo.OrderItem.parentSKUGroup AS SKU_ParentSKUGroup, dbo.Product.Description_defaultTranslation AS Description, 
                      dbo.Product.ShippingMethod AS ShippingMethod, dbo.Product.ProdWeight AS ProdWeight, dbo.Product.screenID AS ScreenID, 
                      dbo.Product.ProductGroupID
FROM         dbo.OrderItem INNER JOIN
                      dbo.Product ON dbo.OrderItem.ProductID = dbo.Product.ProductID LEFT OUTER JOIN
                      dbo.ProductGroup PG ON dbo.Product.ProductGroupID = PG.ProductGroupID
WHERE     (dbo.Product.SKU <> 'Conditional')


GO