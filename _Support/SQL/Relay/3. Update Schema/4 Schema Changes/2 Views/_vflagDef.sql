if exists (select 1 from sysObjects where name = 'vFlagDef')
	DROP view vFlagDef

GO

CREATE VIEW [dbo].[vFlagDef]
AS
/* 2011/03/16 Add a modentityID field to vflagdef */
SELECT     F.FlagGroupID, FG.FlagGroupTextID, F.FlagID, F.FlagTextID, SUBSTRING(FT.DataTable, 1, 8) AS DataTable, UPPER(SUBSTRING(SC.entityName, 1, 1)) AS PL, 
                      FG.Name AS FlagGroup, F.Name AS Flag, SC.entitytypeid, UPPER(SUBSTRING(SC.entityName, 1, 1)) + ' : ' + FG.Name + ': ' + F.Name + '(' + FT.Name + ')' AS Description,
                       FT.Name AS DataType, F.Protected, FT.DataTableFullName, FT.FlagTypeID, SC.entityName AS EntityTable, 
                       isNull(MED.modentityID,0) as modentityid, F.LinksToEntityTypeID,f.Score,sc.uniqueKey, f.isSystem, case when isNull(f.active,1) = 1 and isNull(fg.active,1) = 1 then 1 else 0 end as active
FROM         dbo.flag AS F INNER JOIN
                      dbo.FlagGroup AS FG ON FG.FlagGroupID = F.FlagGroupID INNER JOIN
                      dbo.schemaTable AS SC ON SC.entitytypeid = FG.EntityTypeID INNER JOIN
                      dbo.FlagType AS FT ON FG.FlagTypeID = FT.FlagTypeID LEFT OUTER JOIN
                      dbo.ModEntityDef MED ON SC.entityName = MED.TableName and MED.fieldname = 'AnyFlag'
