IF EXISTS(SELECT * FROM sys.views WHERE name = 'vApprovalEngine') 
	DROP VIEW vApprovalEngine;
GO

CREATE VIEW vApprovalEngine
AS
	SELECT		dbo.approvalEngine.approvalEngineID, 
				dbo.approvalEngine.title, 
				dbo.approvalEngine.entityTypeID as approvalEngineEntityTypeID, 
				approvalEngineEntity.label as approvalEngineEntityType, 
				dbo.approvalEngine.rejectedWorkflowID, 
				rejectedWorkflow.processDescription as approvalEngineRejectedWorkflow, 
				dbo.approvalEngine.approvedWorkflowID, 
				approvedWorkflow.processDescription AS approvalEngineApprovedWorkflow,
				dbo.approvalEngine.revisionWorkflowID, 
				revisionWorkflow.processDescription AS approvalEngineRevisionWorkflow,
				dbo.approvalEngine.approvalRequestEmailID, 
				dbo.eMailDef.eMailTextID as approvalRequestEmail, 
				dbo.approvalEngine.approvalCopyRequestEmailID, 
				copyEmail.eMailTextID as approvalCopyRequestEmail, 
				dbo.approvalEngine.Created as approvalEngineCreated, 
				dbo.approvalEngine.CreatedBy as approvalEngineCreatedBy, 
				dbo.approvalEngine.LastUpdated as approvalEngineLastUpdated, 
				dbo.approvalEngine.LastUpdatedBy as approvalEngineLastUpdatedBy, 
				dbo.approvalEngine.lastUpdatedByPerson as approvalEnginelastUpdatedByPerson,  
				dbo.approvalStatus.approvalStatusID, 
				dbo.approvalStatus.currentLevel, 
				dbo.approvalStatus.approvalStatusTypeID, 
				dbo.approvalStatus.reason,
				dbo.approvalStatus.created as approvedStatusCreated, 
				dbo.approvalStatusType.approvalStatusTextID as approvalStatusTypeTextID, 
				dbo.approvalStatus.entityID as approvalStatusRelatedEntityID, 
				dbo.approvalEngineApprover.approvalEngineApproverID as approverID, 
				dbo.approvalEngineApprover.approverLevel, 
				dbo.approvalEngineApprover.entityTypeID AS approverEntityTypeID,
				case 
					when approvalEngineApproverEntity.entityName = 'person' 
						then (select FirstName + ' ' + Lastname from person where personID = dbo.approvalEngineApprover.approverEntityID)
					when approvalEngineApproverEntity.entityName = 'usergroup' 
						then (select name from usergroup where usergroupID = dbo.approvalEngineApprover.approverEntityID)
					when approvalEngineApproverEntity.entityName in ('flag','integerFlagData') 
						then (select f.name from flag f where f.flagID = dbo.approvalEngineApprover.approverEntityID) 
				end as approverEntity,
				approvalEngineApproverEntity.label AS approverEntityType, 
				dbo.approvalEngineApprover.approverEntityID, 
				dbo.approvalEngineApprover.Created AS approverCreated, 
				dbo.approvalEngineApprover.CreatedBy AS approverCreatedBy, 
				dbo.approvalEngineApprover.LastUpdated AS approverLastUpdated, 
				dbo.approvalEngineApprover.LastUpdatedBy AS approverLastUpdatedBy, 
				dbo.approvalEngineApprover.lastUpdatedByPerson AS approverlastUpdatedByPerson
	FROM         dbo.approvalEngine LEFT OUTER JOIN
						  dbo.approvalEngineApprover ON dbo.approvalEngine.approvalEngineID = dbo.approvalEngineApprover.approvalEngineID LEFT OUTER JOIN
						  dbo.approvalStatus ON dbo.approvalEngine.approvalEngineID = dbo.approvalStatus.approvalEngineID and dbo.approvalEngineApprover.approvalEngineApproverID = dbo.approvalStatus.approvalEngineApproverID LEFT OUTER JOIN
						  dbo.approvalStatusType ON dbo.approvalStatus.approvalStatusTypeID = dbo.approvalStatusType.approvalStatusTypeID LEFT OUTER JOIN
						  dbo.schemaTable AS approvalEngineEntity ON dbo.approvalEngine.entityTypeID = approvalEngineEntity.entitytypeid LEFT OUTER JOIN
						  dbo.processHeader AS rejectedWorkflow ON dbo.approvalEngine.rejectedWorkflowID = rejectedWorkflow.processID LEFT OUTER JOIN
						  dbo.processHeader AS approvedWorkflow ON dbo.approvalEngine.approvedWorkflowID = approvedWorkflow.processID LEFT OUTER JOIN
						  dbo.processHeader AS revisionWorkflow ON dbo.approvalEngine.revisionWorkflowID = revisionWorkflow.processID LEFT OUTER JOIN
						  dbo.eMailDef ON dbo.approvalEngine.approvalRequestEmailID = dbo.eMailDef.emailDefID LEFT OUTER JOIN
						  dbo.eMailDef as copyEmail ON dbo.approvalEngine.approvalCopyRequestEmailID = copyEmail.emailDefID LEFT OUTER JOIN
						  dbo.schemaTable AS approvalEngineApproverEntity ON dbo.approvalEngineApprover.entityTypeID = approvalEngineApproverEntity.entitytypeid

GO