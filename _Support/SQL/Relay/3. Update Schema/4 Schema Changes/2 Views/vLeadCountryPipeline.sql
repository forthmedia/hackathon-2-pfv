if exists (select 1 from information_schema.views where table_name = 'vLeadCountryPipeline')
drop view vLeadCountryPipeline
GO

CREATE VIEW [dbo].[vLeadCountryPipeline]
AS
SELECT     C.CountryID, C.CountryDescription AS Country, OppS.status AS Project_Stage, OppS.OpportunityStage AS Opportunity_Stage, OppS.OpportunityStageID,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE())) AND (op.probability = 25)) AS TwentyFive_Percent,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE())) AND (op.probability = 50)) AS Fifty_Percent,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE())) AND (op.probability = 75)) AS SeventyFive_Percent,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE())) AND (op.probability = 90)) AS Ninety_Percent,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE())) AND (op.probability = 100)) AS OneHundred_Percent,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, GETDATE())) AND 
                                                   (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, GETDATE()))) AS calculated_budget_this_quarter,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE()))) AND (op.probability = 25)) AS TwentyFive_PercentN,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE()))) AND (op.probability = 50)) AS Fifty_PercentN,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE()))) AND (op.probability = 75)) 
                      AS SeventyFive_PercentN,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE()))) AND (op.probability = 90)) AS Ninety_PercentN,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE()))) AND (op.probability = 100)) 
                      AS OneHundred_PercentN,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 1, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 1, GETDATE())))) AS calculated_budget_next_quarter,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE()))) AND (op.probability = 25)) 
                      AS TwentyFive_PercentN2,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE()))) AND (op.probability = 50)) AS Fifty_PercentN2,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE()))) AND (op.probability = 75)) 
                      AS SeventyFive_PercentN2,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE()))) AND (op.probability = 90)) AS Ninety_PercentN2,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE()))) AND (op.probability = 100)) 
                      AS OneHundred_PercentN2,
                          (SELECT     SUM(op.subtotal) AS Expr1
                            FROM          dbo.opportunityProduct AS op INNER JOIN
                                                   dbo.opportunity AS o ON o.opportunityID = op.OpportunityID
                            WHERE      (o.countryID = C.CountryID) AND (o.stageID = OppS.OpportunityStageID) AND (DATEPART(q, op.forecastShipDate) = DATEPART(q, DATEADD(q, 2, 
                                                   GETDATE()))) AND (DATEPART(yy, op.forecastShipDate) = DATEPART(yy, DATEADD(q, 2, GETDATE())))) AS calculated_budget_quarter_plus2
FROM         dbo.Country AS C INNER JOIN
                      dbo.CountryGroup AS cg ON cg.CountryMemberID = C.CountryID CROSS JOIN
                      dbo.OppStage AS OppS
WHERE     (cg.CountryGroupID = ISNULL
                          ((SELECT     CountryID
                              FROM         dbo.Country
                              WHERE     (CountryDescription LIKE 'Active Lead Countries')), cg.CountryGroupID))
GO
