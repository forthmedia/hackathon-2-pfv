ALTER PROCEDURE [dbo].[ModRegisterTriggerAction]
			@procid	int,
			@columns_Updated binary (20)
			

AS

/*
WAB 8/11/04 modified to deal with complex flag tables
WAB 16/11/04 modifed to deal with flag tables such as text/date correctly - was putting an insert and a delete record in modregister rather than a single record
WAB 22/3/05 changed reference to schema table to relayserver database
WAB 01/06/05 added the isNull to the query getting the identity column - sometime the column is null and soemtimes it is blank
WAB 20/09/06 made modification so that a query which updates a flagid will be detected and recorded as an insert and a delete

[WAB 17/10/07 wanted a change of flagid or entityid to show an insert and delete - specifically for changing radios by changing the flagid]
Seemed to do the above change twice in different ways on different dbs - now consolidated		



WAB 26/06/07 added convert varchar(4000) to the queries - needed an explicit conversion for a money field
WAB 2008/01/23  don't record changes to lastupdated/updatedby/created/createdby in flags - this means that these lastupdated/updatedby can be altered just before a delete to get the correct personid into modregister, without a record appearing in modregister
	           2008/03/05 noticed a problem with this code which may have meant that deletes were not being picked up at all
WAB 2008/01/23 merged together various versions of this code
WAB 2008/03/05 added code so that lastupdatedby is not used unless it has been updated
		Added code to allow use of a lastupdatedbyperson column on a table and record it in the modregister
WAB 2008/04/11  fixed bug in above code when doing insert
WAB 2008/04/14  corrected columns_updated() bitmask code to make sure that it dealt with tables with more than 8 fields
GCC 2008/10/03 As part of SQL 2005 upgrade columns_updated() only available to the trigger and not this sp which the trigger calls - must be passed in
	also changed ordinal_position because it cannot be relied upon in 2005, 2008 to COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME),COLUMN_NAME, 'ColumnID')

WAB 2008/11/24  Problems with GCC code above:
	@columns_updated needed a length long enough to fit all the columns in the table (8 cols per byte)
	Note also need a change to every trigger which calls this code, @columns_updated needs a length
	COLUMNPROPERTY 'ColumnID' was not backwards compatible and brings back null on old SQL, so have used an isNull
	also found problems in existing code - things went odd on colums 8,16,24,32 etc

WAB  2009/02/11 Added check for no entry in ModTrigger
WAB 2011/02/04 Added a special modentitydef to signify an update to a flags for a particular table (requires vflagdef to be updated)
WAB 2011/03/11 Added a new column entityTypeID to the modregister column and now populate it - this is slight denormalisation but might make some queries easier
	also no longer essential to have an entity entry in the tableSchema table
WAB 2011/05/04 LID 6469 Amazingly the oldVal and newVal columns were not nvarchar, fixed
NJH 2012/05/17  set the lastupdated fields on the core table when updating flags 
WAB 2012-09-26 moved NJH code of 2012/05/17 into flag section.  Dealt with problems when lastupdated by was not set ansd code was trying to insert null into baseTable.lastupdatedby.  Now insert lastupdatedby = 0 if not known  
WAB 2012-10-01 Problems when uniquekeyColumn was longer than 20 characters - changed to sysname 
PPB 2012-12-10 Case 432473 added a day datediff check to avoid it erroring when evaluating the seconds datediff check when the dates are very far apart (WAB advised)
2013-08-16 NYB Case 436316 added []'s around entityTable - to stop it erroring when entityTable was called a sql recognised name - like 'File' 
WAB	2013-03-21 CASE 434808 Removed all references to R1Share, R2Share etc, now removed from db.
IH 2013-09-17 Case 436984 item 1 increase the size of @TriggerName from 32 to 100 chars to accomodate longer trigger names 
NJH 2013/11/25 Case 437316 provide support for tracking changes when one updates a flagID or entityID on a boolean (or other non-mulitple type) flag. Change
	the unique join to be on rowID and if a flagID or entityID has been modified, then treat it as a delete and then insert
WAB 2013-12-06	Performance tuning (from a salesforce project).  Got rid of an unneeded IF Exists before doing an insert into modRegister
WAB 2014-04-28 Updates to flags where being recorded even if data had not changed 
WAB 2014-05-23 change @TriggerName to sysname, even better then varchar(100)
WAB	2015-01-28	Performance improvements prompted by entityUpsert.  Exit quickly if either no rows in #i/#d or only columns updated are CRUD
NJH 2016/03/10	Populate/synch the entityName table if new records are added, or if the nameExpression column or crmID column have changed.
NJH 2016/04/05	JIRA BF-624 Found out that queries to populate/update entityname were running unnecessarily. (for updates when inserts only would have sufficed and also when no columns were getting updated)
NJH	2016/08/01	JIRA PROD2016-1507. If object has 'deleted' column, then check that to find out if object is deleted when populating vEntityName, rather than del table (such as product)
NJH 2016/10/06	Sugar 451406/JIRA PROD2016-2466  - when updating a crmID, if the value is an empty string, then null it, both in the base table and the entityName table. We don't want crmID columns to have empty strings.
WAB 2017-02-28	PROD2016-3487 Remove dependency on existence of lastupdatedBy Column when doing a delete
*/


set nocount on
/* The action can be switched on and off
   by setting the 'IsOn' column in Modtrigger (1 on 0 off) against the appropriate
   action. All ModRegister recording can be switched off by setting ANY to 0
 */
/*-----------------------------------------------------------------------------------*/
/* Initialisations                                                                   */
/*-----------------------------------------------------------------------------------*/
Declare	@TriggerName sysname,
	@EID		int,
	@ColName	sysname,
	@Colid		int,
	@mod8		int,
	@Rem8		int,
	@user		nvarchar(30),
	@ModAction	nvarchar(5),		-- Database action: I(nsert), U(pdate), D(elete)
	@ActionMnemonic	nvarchar(10),		-- Mnemonic used to describe the action in ModRegister
	@sqlcmd		nvarchar(2000),
	@ModStamp	datetime,
	@Table		sysname,
	@FlagTable	tinyint,		-- Decimal representation of bitmap to determine table type:
						-- 0 = Non-flag table; 
						-- bit1 (value 1)= Flag table; 
						-- bit2 (value 2)= Multi-value flag table; 
						-- bit3 (value 4) = multi-column/complex flagtable
						-- ie.  1= regular flag table
						--	3= flag table with multiple values
						--	5 = complex flag table
						--	7 = complex flagtable with multiple values
							
	@drows		int,
	@irows		int,
	@status		int,
	@sqlerror	int,
	@OldValName	sysname,
	@NewValName	sysname,
	@NewDataColName	sysname,
	@OldDataColName	sysname,
	@UniqueColName	sysname,
	@uniqueColumnJoin varchar (100),
	@recordidColumn sysname,
	@flagidColumn sysname ,
	@doInsertsAndDeletesAsWell bit	,
	@bitmask int,
	@char int,
	@LastUpdatedByColID int,
	@lastUpdatedByColumn varchar (200),					-- PPB 2012-12-10 Case 432473 increase length from 100
	@LastUpdatedByPersonColID int,
	@lastUpdatedByPersonColumn varchar (200),			-- PPB 2012-12-10 Case 432473 increase length from 100
	@debug bit,
	@entityTypeID int,
	@entityTable sysname,
	@uniquekeyColumn sysname,
	@flagIDEntityIDModified bit,
	@crmIDCol sysname,
	@nameExpressionCol sysname,
	@inEntityName bit,
	@hasDeletedColumn bit,
	@deletedColumn sysname = 'deleted',
	@startTime datetime = getdate()
	
	
	select @debug = 0   -- use for debugging

	select @drows = count(*) from #D
	select @irows = count(*) from #I

	/* WAB 2015-01-28 if no rows in tables we will exit immediately */
	IF @irows + @drows = 0
	BEGIN
		RETURN 0
	END

	if @debug = 1 
	Begin
		select * from #I
		select * from #D
	end


/*-- Get necessary details, including the name of the table to which the trigger belongs ---*/
	Select	@ModStamp	= getdate(),
			@Table		= o2.name,
			@user		= user_name(),
			@TriggerName	= o1.name,
			@crmIDCol = isNull(c.column_name,''),
			@nameExpressionCol = isNull(s.nameExpression,''),
			@entityTypeID = s.entityTypeID,
			@uniquekeyColumn = s.uniqueKey,
			@inEntityName = isNull(s.in_vEntityName,0),
			@hasDeletedColumn = case when dc.column_name is not null then 1 else 0 end
	from	sysobjects	o1
	  		join sysobjects 	o2 	  on	o1.parent_obj = o2.id
	  		left join schemaTable s on s.entityName = o2.name and o2.xType='U' and s.in_vEntityName = 1
	  		left join information_schema.columns c on c.table_name = o2.name and c.column_name like 'crm%ID'
	  		left join information_schema.columns dc on dc.table_name = o2.name and dc.column_name = @deletedColumn
	 where	o1.id	= @procid


	/* 	WAB 2015-01-28 if update is just CRUD columns (ie just before a delete) we will exit immediately 
		This test is for all the lastupdated.. columns.  Would be nice to do subset, but need bitwise operators on binary dataType which I don't have
		NJH 2016/04/19 - had to add @columns_Updated != 0 in the below check to deal with deletions.
	*/

	/*declare @crudCols binary (50 )= dbo.columnListToBitmask (@Table,'lastupdated,lastupdatedby,lastupdatedbyperson')
	IF @crudCols = @columns_Updated */
	IF not exists (select 1 from dbo.getUpdatedColumnNames(@table,@columns_updated) where name not in ('lastupdated','lastupdatedby','lastupdatedbyperson')) and @columns_Updated != 0
	BEGIN
		RETURN 0
	END

	declare @entityNameUpdateColList varchar(50) = @crmIDCol+','+@nameExpressionCol
	declare @entityNameUpdateCols binary (50 )= dbo.columnListToBitmask (@Table,@entityNameUpdateColList)
	
	/* if we've updated the crmId on a base table, then effectively we want to get rid of the crmID on the deleted object, as it means that the record has re-created itself
		and we don't want to have duplicate crmIDs in this table */
	if (@inEntityName = 1)
	begin
		/* NJH 2016/10/06 if a crmID has been modified or on insert, then null the CRMID column if the value is empty string. */
		IF ( @crmIDCol != '' and (dbo.hasColumnBeenUpdated(@Table,@crmIDCol,@columns_Updated)= 1 or @drows = 0))
		BEGIN
			set @sqlcmd = 'update ' + @Table + 
				' set ' + @crmIDCol + ' = null
				from '  + @Table + ' e
					inner join #i i on i.'+@uniquekeyColumn+' = e. '+@uniquekeyColumn+ 
				' where i.'+ @crmIDCol + ' = '''' '
			exec(@sqlcmd)
		END
		
		/* if new entityName record is inserted and contains a crmID already in entityName (which is deleted), remove the crmID from the deleted row */
		if (dbo.hasColumnBeenUpdated(@Table,@crmIDCol,@columns_Updated)= 1 and @crmIDCol != '')
		begin
			set @sqlcmd = 'update entityName 
				set crmID = null
				from entityName e 
					inner join #i i on i.'+@crmIDCol+' = e.crmID and e.entityTypeID='+cast(@entityTypeID as char(5))+' and e.deleted=1'
			exec(@sqlcmd)
		end

		/* run on insert */
		IF (@drows = 0)
		BEGIN
			set @sqlcmd = 'insert into entityName (entityId,entityTypeID,name,crmID,deleted)
			select distinct i.'+@uniquekeyColumn+', '+cast(@entityTypeID as char(5))+','+ case when @nameExpressionCol = '' then 'null' else 'i.'+@nameExpressionCol end+','
				+ case when @crmIDCol = '' then 'null' else 
					'case when '+@crmIDCol +' = '''' then null else i.'+@crmIDCol+' end' end+','+ 
					case when @hasDeletedCOlumn = 1 then 'i.'+@deletedColumn else '0' end+'
				from #i i
					left join entityName e on i.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+cast(@entityTypeID as char(5))+'
				where e.entityID is null'
			exec(@sqlcmd)
		END

		/* if crmID,deleted or nameExpressions columns have been updated AND we're dealing with an update OR if we're dealing with a delete */
		IF (((dbo.hasColumnBeenUpdated(@Table,@crmIDCol,@columns_Updated)= 1 or dbo.hasColumnBeenUpdated(@Table,@nameExpressionCol,@columns_Updated) = 1 or dbo.hasColumnBeenUpdated(@Table,@deletedColumn,@columns_Updated)= 1) and @drows = @irows) or @irows=0)
		begin
			set @sqlcmd = 'update entityName 
				set name = '+case when @nameExpressionCol = '' then 'null' when @irows =0 then 'd.'+@nameExpressionCol else 'i.'+@nameExpressionCol end+',
					crmID = ' + case when @crmIDCol = '' then ' null ' else 
						+ ' case when ' + case  when @irows =0 then 'd.'+@crmIDCol else +'i.'+@crmIDCol end +
							' = '''' then null else '
						+case when @irows =0 then 'd.'+@crmIDCol else +'i.'+@crmIDCol end+
							' end ' end + ', 
					deleted = '+ case when @hasDeletedColumn=1 then 'isNull(i.'+@deletedColumn+',1)' else 'case when i.'+@uniquekeyColumn+' is not null then 0 else 1 end' end+'
				from entityName e 
					left join #i i on i.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+cast(@entityTypeID as char(5))+'
					left join #d d on d.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+cast(@entityTypeID as char(5))+'
				where
					i.'+@uniquekeyColumn+' is not null or d.'+@uniquekeyColumn+' is not null'

			exec(@sqlcmd)
		end

	end

/*-- Validate trigger name format --*/
if 	@TriggerName not like '%_MRAudit'
begin
	   RAISERROR ('Error : Trigger %s is an invalid name for ModRegister updates',
	      16, 1, @TriggerName)
	   RETURN 1
end

/*-- determine the 'action' --*/
select @ModAction =
	CASE
		when @irows = @drows	Then 'U'
		when @irows > 0		Then 'I'
		when @drows > 0		Then 'D'
	END

select @doInsertsAndDeletesAsWell = 0     

if @debug = 1 print @ModAction


/*-- check whether this 'action' has the ModRegister Audit switched off --*/
if exists(Select * from ModTrigger
		where (ModAction = 'ANY' and IsOn=0)					-- all triggers switched off
		   or (ModAction = @ModAction and IsOn=0 and TableName = @Table))	-- particular trigger switched off
	begin	
		 print 'trigger off'	
		return -- do nothing as trigger is switched off
	end

/*-- Get Action mnemonic and table type --*/
select 	@ActionMnemonic = Name, @FlagTable = FlagTable
from 	ModTrigger mt
 where 	mt.TableName = @Table
 		and 	mt.ModAction = @ModAction

/* WAB  2009/02/11 added error checking.  This condition will occur if the trigger has been created by scripting rather than from generate_MRAudit and no entry has been added in ModTrigger */
IF @@ROWCOUNT = 0 
BEGIN
	   RAISERROR ('Error : No entry in ModTrigger Table for Table %s.  Try Running Generate_MRAudit ',
      16, 1, @Table)
	   RETURN 
END



if @debug = 1 print 'Columns_Updated'
if @debug = 1 print @columns_Updated



/* 
For an update, determine whether the lastupdatedby column has been updated.
	 if it hasn't we are not going to use the id in lastupdatedby , but rather use 0 (this is dealt with in the insert queries)
For a delete, determine whether the lastupdated date is been updated in the last x seconds
	if it is then we assume that the lastupdatedby has also been updated and we can therefore use value of d.lastupdatedby
*/


/* get colid of the lastupdatedby column
  which bit it is (in groups of 8), eg col 10 = bit 2 (of 2nd byte )
  convert bit to mask (eg make bit 2   00000010)
 which byte it is in eg col 3 is in byte 1, col 11 is in byte 2
*/
	
	/*select @LastUpdatedByColID = colid from syscolumns c join sysobjects o on c.id = o.id where c.name in ('lastupdatedby') and o.name= @table
	select @bitmask = (@LastUpdatedByColID - 1 )% 8 + 1  
	select @bitmask = power(2,@bitmask  - 1)  
	select @char = ((@LastUpdatedByColID - 1) / 8) + 1  */
	
	if @ModAction = 'D' and exists (select 1 from syscolumns where name = 'lastupdatedby' and id = object_id(@table))
		/* 2012-12-10 PPB Case 432473 added the day datediff check to avoid it erroring when evaluating the seconds datediff check when the dates are very far apart  */
		select @lastUpdatedByColumn = ' case when datediff(d,d.lastupdated,getdate()) < 1  AND datediff(s,d.lastupdated,getdate()) < 2 then d.lastupdatedby else 0 end '
	else if dbo.hasColumnBeenUpdated(@table,'lastupdatedby',@columns_Updated) = 1
		select @lastUpdatedByColumn = ' i.lastupdatedby '
	else 
		select @lastUpdatedByColumn = ' 0 '
	
	
	
/*  
Does the table we are dealing with have a lastupdatedbyPerson column?
*/
	select @LastUpdatedByPersonColID = colid from syscolumns c join sysobjects o on c.id = o.id where c.name in ('lastupdatedbyPerson') and o.name= @table
	IF @LastUpdatedByPersonColID <> 0 
	BEGIN
		/*select @bitmask = (@LastUpdatedByPersonColID - 1 )% 8 + 1  
		select @bitmask = power(2,@bitmask  - 1)  
		select @char = ((@LastUpdatedByPersonColID - 1) / 8) + 1  */
	
		if @ModAction = 'D' 
			/* 2012-12-10 PPB Case 432473 added the day datediff check to avoid it erroring when evaluating the seconds datediff check when the dates are very far apart  */
			select @lastUpdatedByPersonColumn = ' case when datediff(d,d.lastupdated,getdate()) < 1  AND datediff(s,d.lastupdated,getdate()) < 2 then d.lastupdatedbyperson else 0 end '	
		else if dbo.hasColumnBeenUpdated(@table,'lastupdatedbyPerson',@columns_Updated) = 1
			select @lastUpdatedByPersonColumn = ' i.lastupdatedbyperson '
		else 
			select @lastUpdatedByPersonColumn = ' 0 '
	
	
	END
	ELSE
		select @lastUpdatedByPersonColumn =  ' 0 '

/*=============================================================================================================*/
/* END OF WORKING OUT LASTUPDATEDBY COLUMNS																		*/
/*=============================================================================================================*/



/*=============================================================================================================*/
/* BEGIN AUDIT PROCESSING                                                                                      */
/*=============================================================================================================*/

/*-----------------------------------------------------------------------------------*/
/* Processing for non-Flag data tables                                               */
/* or complex flag tables											*/
/*-----------------------------------------------------------------------------------*/

if @debug =1 print '@FlagTable =  ' + convert(varchar,@flagTable) 

IF @FlagTable & 5 = 5  or @FlagTable  = 0 -- Either normal table or a flag table with multiple columns 
BEGIN

	-- get the table unique id  (WAB changed to point to relayserver version)
	-- 01/06/05 WAB added the isNull 
		select @UniqueColName = isNull(UniqueKey,'')
		from dbo.schematable
		where EntityName = @Table


	  -- a flag table with single values will not have a unique key so we join on flagid and entityid
	  	IF @UniqueColName = '' and @FlagTable & 4 <> 0
		BEGIN
			select @uniqueColumnJoin = ' d.entityid = i.entityid and d.flagid = i.flagid '
			select @recordidColumn = case when @ModAction = 'U' then 'i.' else '' end  + 'entityid'	
			select @flagidColumn = case when @ModAction = 'U' then 'i.' else '' end  + 'flagid'	
		END
	  	ELSE
	    BEGIN 
	  		IF isnull(@UniqueColName,'') = ''
	  		BEGIN
	  			select @UniqueColName  = dbo.getTablePrimaryKey (@table)
	  		END
	  
			IF @UniqueColName <> ''
			BEGIN
					select @uniqueColumnJoin = 'd.'+@UniqueColName+'=i.'+@UniqueColName
					select @recordidColumn = case when @ModAction = 'U' then 'i.' else '' end  +@UniqueColName	
					select @flagidColumn = 'null'
			END
			ELSE	
			BEGIN
					print 'ModRegisterTriggerAction: No unique key or flag'
					print @flagTable
					print @Table
					return
			END
				
		END
	
	

	/*-----------------------------------------------------------------------------------*/
	/* Updates                                                                           */
 	/*-----------------------------------------------------------------------------------*/
	IF @ModAction = 'U' 
    BEGIN
		declare mr  cursor static local for    -- 21/09/05 WAB added local and had to change Insensitive to Static
		Select FieldName, ModEntityID, c.colid,st.entityTypeID
		From ModEntityDef
			inner join sysobjects s on s.name=@Table
						and type ='U'
			inner join syscolumns c on s.id =c.id
						and c.name=Fieldname
			left join Schematable st on st.entityName = tablename			
		Where TableName=@Table and FieldMonitor=1		

			/*
			WAB 2008/11/24 added this test rather than bringing back all the fields and testing each individually after the fetch
			also this code works correctly, whereas there was previously a problem with fields where colid was 8,16,32 
			*/
			and SUBSTRING(@columns_Updated,((isNull(COLUMNPROPERTY(s.id,c.name, 'ColumnID'),c.colid)-1)/8)+1,1) & power(2,(isnull(COLUMNPROPERTY(s.id,c.name,'ColumnID'),c.colid)-1)%8) <> 0
	



		OPEN mr

			select @sqlerror = @@error

			IF @sqlerror <> 0
			BEGIN
				   RAISERROR ('SQL Error : %d accessing ModEntity table for Entity : %s',
				      16, 1, @sqlerror,@Table)
				   RETURN 1
			END	

			fetch next from mr into 
				@ColName,@EID,@colid,@entityTypeID

	
		WHILE @@fetch_status = 0 
		BEGIN
			if @debug =1 print 'Column Updated: ' + @colname

			select @sqlcmd	= 
					/* WAB 2013-12-06 
						remove the if exists from this query 
						this check is already in the where clause
						seems to improve performance by about 20% when removed

					N'if exists(select * from #I i'
					+ ' inner join #D d on ' + @uniqueColumnJoin
					+ ' Where ISNULL(convert(nvarchar(4000),i.'
					+ @ColName+'), '''')<>ISNULL(convert(nvarchar(4000),d.'
					+ @ColName+'), ''''))'
					+  */

					 N'Insert Into ModRegister(ModDate,ModEntityID,'
					+ 'Action,RecordID,flagid,entityTypeID,ActionByCF,ActionByPerson,ActionByUser,OldVal,'
					+ 'NewVal)'
					+ ' Select @ModStamp,'
					+ '@EID,'
					+ '@ActionMnemonic,'
					+  @recordidColumn +','  
					+  @flagidColumn + ',' 
					+  '@entityTypeID ,'
					+ @lastupdatedByColumn + ',' 
					+ @lastupdatedByPersonColumn + ',' 
					+ '@user,'
					+ 'convert(nvarchar(4000),d.'+ @ColName+'),'
					+ 'convert(nvarchar(4000),i.'+ @ColName+')'
					+ ' From #I i'
					+ ' inner join #D d on ' + @uniqueColumnJoin
					+ ' Where ISNULL(convert(nvarchar(4000),i.'
					+ @ColName+'), '''')<>ISNULL(convert(nvarchar(4000),d.'
					+ @ColName+'), '''') '
			
			if @debug =1 print @sqlcmd
			
			exec @status = sp_executesql 
				@sqlcmd,
				N'@ModStamp DATETIME,@EID INT, @ActionMnemonic nvarchar(5), @user nvarchar(30),@entityTypeID int',
				@ModStamp,
				@EID,
				@ActionMnemonic,
				@user,
				@entityTypeID 
				
			select @sqlerror = @@error


			IF @sqlerror <> 0 or @status <> 0
			BEGIN
					if @debug =1 print 'Error ' + convert(varchar,@sqlerror)
				   RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert ModRegister %s action data',
				      16, 1, @sqlerror,@status,@ActionMnemonic)
				   RETURN 1
			END	

	
			fetch next from mr into 
			@ColName,@EID,@colid,@entityTypeID

		END
	
		close mr
		deallocate mr
    END /* End of Updates for normal tables */


   	/*-----------------------------------------------------------------------------------*/
   	/* Inserts or Deletes                                                                */
   	/*-----------------------------------------------------------------------------------*/
	ELSE 
    BEGIN
		Select @EID=ModEntityID, @entityTypeID = entityTypeID
		From ModEntityDef
		left join Schematable st on st.entityName = tablename 
		Where fieldName='Whole Record' and Tablename=@Table
	

		select @sqlcmd	= N'Insert Into ModRegister(ModDate, ModEntityID, Action, RecordID, flagid,entityTypeID,ActionByCF, ActionByPerson,ActionByUser)'
			+ ' Select @ModStamp, @EID, @ActionMnemonic, '+@recordIDcolumn + ',' +@flagidcolumn +', @entityTypeID,' 
			+ @lastupdatedByColumn +','
			+ @lastupdatedByPersonColumn + ',' 
			+ '@user'
			+ ' From #'+ @ModAction + ' as ' + @ModAction

		exec @status=sp_executesql 
			@sqlcmd,
			N'@ModStamp DATETIME,@EID INT, @ActionMnemonic nvarchar(5), @user nvarchar(30),@entityTypeID int',
			@ModStamp,
			@EID,
			@ActionMnemonic,
			@user,
			@entityTypeID

		select @sqlerror = @@error

		IF @sqlerror <> 0 or @status <> 0
 	 	BEGIN
		   RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert ModRegister %s action data',
		      16, 1, @sqlerror,@status,@ActionMnemonic)
		   RETURN 1
	 	END	

        /*--- Deletions require a copy of the deleted row to be saved in the relevant ...Del table, if one exists ---*/
		IF @ModAction = 'D'
	 	BEGIN
			select @sqlcmd = N'If exists(select 1 from sysobjects where name = ''' + @Table + 'Del'') ' + 
				' Insert '+@Table+'Del select * from #D'
			exec @status=sp_executesql 
				@stmt	= @sqlcmd

			IF @sqlerror <> 0 or @status <> 0
		 	BEGIN
			   RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert %sDel data',
			      16, 1, @sqlerror,@status,@Table)
			   RETURN 1
		 	END	

	 	END

    END  /* End of Insert Normal Tables */
 
 END  /* End of Normal Tables */


/*-----------------------------------------------------------------------------------*/
/* Processing for Flag data tables   (not complex flags)                                                 */
/*-----------------------------------------------------------------------------------*/
ELSE -- This is a Flag table trigger
BEGIN
	if @debug =1 print '@ActionMnemonic ' + @ActionMnemonic
    -- For flags we only need FD,FM,FA as ActionName
    if len(@ActionMnemonic)>2
		select @ActionMnemonic = right(@ActionMnemonic,2)

    -- Set column names as required, depending on whether flag table has a data column or not
    IF exists(select 1 from syscolumns c join sysobjects o on c.id = o.id where c.name = 'Data' and o.name= @table)
    BEGIN
		set @NewDataColName= 'i.Data, '
		set @OldDataColName= 'd.Data, '
		set @OldValName = 'OldVal, '
		set @NewValName = 'NewVal, '
    END
    ELSE
    BEGIN
		set @NewDataColName= ''
		set @OldDataColName= ''
		set @OldValName = ''
		set @NewValName = ''
    END

	/*
	2008-04-14
	Changed my previous code so that it dealt correctly with tables of more than 8 columns
	Test whether fields other than lastupdated have been updated
	Now all done within a single query

	power(2,(ordinal_position-1)%8) is the Bit mask of each column  [the % 8 splits it into single bytes]
	((ordinal_position-1)/8)+1,1) gets which byte of columns_updated() each column appears in)
	*/


	-- only record mod if a field of interest has been changed
	-- or if this is a delete in which case @irows = 0 WAB: 2008/03/05 
   	IF @debug =1 
	BEGIN
		print 'table ' + @table
		
		select c.name
		from 
			sysobjects s  
				inner join 
			syscolumns c on s.id =c.id
		where s.name=@Table
		and c.name not in ('lastupdated','lastupdatedby','created','createdby','lastUpdatedByPerson')
		and  SUBSTRING(@columns_Updated,((isNull(COLUMNPROPERTY(s.id,c.name, 'ColumnID'),c.colid)-1)/8)+1,1) & power(2,(isnull(COLUMNPROPERTY(s.id,c.name,'ColumnID'),c.colid)-1)%8) <> 0

	END



	IF exists 
		(
			/*
			WAB 2008/11/24
			replaced with code below, decided to go back to using sys tables, because could use s.id as a parameter in the columnproperty function
			select 1 
			from information_schema.columns 
			where table_name = @table
			and column_name not in ('lastupdated','lastupdatedby','created','createdby')
			and SUBSTRING(@columns_Updated,((COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME),COLUMN_NAME, 'ColumnID')-1)/8)+1,1) & power(2,(COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME),COLUMN_NAME, 'ColumnID')-1)%8) <> 0
			*/

		select 1
		from 
			sysobjects s 
				inner join 
			syscolumns c on s.id =c.id
		where s.name=@Table
		and c.name not in ('lastupdated','lastupdatedby','created','createdby','lastUpdatedByPerson')
		and  SUBSTRING(@columns_Updated,((isNull(COLUMNPROPERTY(s.id,c.name, 'ColumnID'),c.colid)-1)/8)+1,1) & power(2,(isnull(COLUMNPROPERTY(s.id,c.name,'ColumnID'),c.colid)-1)%8) <> 0

		)
		or (@irows = 0)
	BEGIN

	   -- join between i and d - only suitable for single value flags
	   --select @uniqueColumnJoin = ' d.entityid = i.entityid and d.flagid = i.flagid '
	   select @uniqueColumnJoin = ' d.rowID = i.rowID '
	   


		/*
		WAB 20/9/05 discovered that this code wouldn't detect a flagid being updated
		(eg update booleanflagdata set flagid = 21 where flagid = 22) wouldn't get detected because the
		number of rows in the inserted and deleted tables would be the same and the @modaction would
		be set to update - however the inner join on flagid would return zero rows.  
		The only way to get round this is to run all three queries.  

		(This of course assumes that it is a valid action to change a flagid (which it may not be!))
	
		*/
		if @debug =1 print 'Modaction ' + @modAction
	   /*-----------------------------------------------------------------------------------*/
   		/* Flag Updates                                                          */
	   /*-----------------------------------------------------------------------------------*/
	   
	   /* NJH if a flagID or entityID has been modified, then skip the modregister insert here and treat it as a delete/insert */
	   select @flagIDEntityIDModified = 0
	   
		IF @ModAction in('U')
		BEGIN
	
			if exists (
				select 1
					from 
						sysobjects s 
							inner join 
						syscolumns c on s.id =c.id
					where s.name=@Table
					and c.name in ('flagID','entityID')
					and  SUBSTRING(@columns_Updated,((isNull(COLUMNPROPERTY(s.id,c.name, 'ColumnID'),c.colid)-1)/8)+1,1) & power(2,(isnull(COLUMNPROPERTY(s.id,c.name,'ColumnID'),c.colid)-1)%8) <> 0
			)
			select @flagIDEntityIDModified = 1
			
			if (@flagIDEntityIDModified !=1)
			BEGIN
			select @sqlcmd	= N'Insert Into ModRegister(ModDate, ModEntityID, Action, RecordID, ActionByCF, ActionByPerson,ActionByUser, ' + @OldValName + @NewValName + ' FlagId,entityTypeID)'
				+ ' Select @ModStamp, f.modentityid, @ActionMnemonic, i.EntityID,'
				+ @lastupdatedByColumn +','
				+ @lastupdatedByPersonColumn + ',' 
				+ '@user, ' + @OldDataColName + @NewDataColName + '  i.FlagId, f.entityTypeID'
				+ ' From #D d inner join #I i on' + @uniqueColumnJoin 
				+ ' inner join vflagdef f on i.flagid = f.flagid  '
				
				/* WAB 2014-04-28 Discovered that this line was missing so all updates - even those that did not change the data - were being reported
					however not if this is a boolean flag because there is no data column
					unfortunately these variables include a comma, which needs to be removed
 				*/
				IF 	@NewDataColName <> ''
					select @sqlcmd	= @sqlcmd +  ' Where '  + replace(@NewDataColName,',','') + ' <> ' + replace(@OldDataColName,',','')
				
				

			exec @status=sp_executesql 
				@sqlcmd,
				N'@ModStamp DATETIME, @ActionMnemonic nvarchar(5), @user nvarchar(30)',
				@ModStamp,
				@ActionMnemonic,
				@user
			END

			if (@irows <> @@rowcount) or (@flagIDEntityIDModified = 1) 
	  			/* if number of rows inserted into the modregister table is not the same as the 
	  			  	number of rows in the inserted table, then there is a possibility that a flagid (or an entityid) has been updated
	  			 	in which case we need to record an insert and a delete
	  			 	set a variable so that we do the delete and insert queries below */
	  			select @doInsertsAndDeletesAsWell = 1	

				select @sqlerror = @@error

			IF @sqlerror <> 0 or @status <> 0
		 	BEGIN
				   RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert ModRegister %s action data',
		      	16, 1, @sqlerror,@status,@ActionMnemonic)
		   		RETURN 1
		 	END	
		END
	
	   	/*-----------------------------------------------------------------------------------*/
   		/* Flag Deletes                                                               */
	   	/*-----------------------------------------------------------------------------------*/
   		IF @ModAction in('D') or @doInsertsAndDeletesAsWell = 1
	    BEGIN
	
			IF @modAction = 'U'		
				set @ActionMnemonic = 'FD'   -- this is the case of a flag having its flagid or entityid changed, needs recording as delete/add 

				select @sqlcmd	= N'Insert Into ModRegister(ModDate, ModEntityID, Action, RecordID, ActionByCF,ActionByPerson,ActionByUser, ' + @OldValName + ' FlagId,entityTypeID)'
					+ ' Select @ModStamp, f.modentityid, @ActionMnemonic, d.EntityID,'
					+ @lastupdatedByColumn +','
					+ @lastupdatedByPersonColumn + ',' 
					+ ' @user, ' + @OldDataColName + '  d.FlagId, f.entityTypeID'
					+ ' From #D d left join #I i on' + @uniqueColumnJoin 
					+ ' inner join vflagdef f on d.flagid = f.flagid  '
					+ ' where i.flagid is null'
					+ ' OR (isNull(i.flagid,0) <> d.flagid) OR (isNull(i.entityID,0) <> d.entityid)'
			
				exec @status=sp_executesql 
				@sqlcmd,
				N'@ModStamp DATETIME, @ActionMnemonic nvarchar(5), @user nvarchar(30)',
				@ModStamp,
				@ActionMnemonic,
				@user
	
				select @sqlerror = @@error
	
				if @sqlerror <> 0 or @status <> 0
		 		BEGIN
			   		RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert ModRegister %s action data',
			      	16, 1, @sqlerror,@status,@ActionMnemonic)
			   		RETURN 1
		 		END
	
	   	END
	
   		/*-----------------------------------------------------------------------------------*/
		/* Flag Inserts                                                               */
   		/*-----------------------------------------------------------------------------------*/
		IF @ModAction in('I') or @doInsertsAndDeletesAsWell = 1
    	BEGIN

			IF @modAction = 'U'		
				set @ActionMnemonic = 'FA'   -- this is the case of a flag having its flagid or entityid changed, needs recording as delete/add 

				select @sqlcmd	= N'Insert Into ModRegister(ModDate, ModEntityID, Action, RecordID, ActionByCF, ActionByPerson,ActionByUser, ' + @NewValName + ' FlagId,entityTypeID)'
					+ ' Select @ModStamp, f.modentityid, @ActionMnemonic, i.EntityID, '
					+ @lastupdatedByColumn +','
					+ @lastupdatedByPersonColumn + ',' 
					+ ' @user, ' + @NewDataColName + ' i.FlagId,f.entityTypeID'
					+ ' From #I i left join #d d on' + @uniqueColumnJoin 
					+ ' inner join vflagdef f on i.flagid = f.flagid  '
					+ ' where d.flagid is null'
					+ ' OR (isNull(d.flagid,0) <> i.flagid) OR (isNull(d.entityID,0) <> i.entityid)'
			
				exec @status=sp_executesql 
						@sqlcmd,
						N'@ModStamp DATETIME, @ActionMnemonic nvarchar(5), @user nvarchar(30)',
						@ModStamp,
						@ActionMnemonic,
						@user
	
				select @sqlerror = @@error
	
				IF @sqlerror <> 0 or @status <> 0
	 			BEGIN
		   			RAISERROR ('SQL Error : %d calling sp_ExecuteSQL (returned %d) to insert ModRegister %s action data',
		      				16, 1, @sqlerror,@status,@ActionMnemonic)
		   			RETURN 1
	 			END	
	    		
   		END
	    
		/* NJH 2012/05/17 - set the lastupdated fields on the core table when updating flags */
		/* only done on insert/update.. as the delete action does not give us who did the deleting.. and we don't want to just
			update the date without updating who did the update */
				
		IF @ModAction not in('D')
		BEGIN
			select @sqlcmd = 'select distinct top 1 @entityTable = entityTable, @uniquekeyColumn = uniqueKey  from vFlagDef v
				inner join #I i on i.flagId = v.flagID'
			exec sp_executesql @statement=@sqlcmd,@params=N'@entityTable sysname output, @uniquekeyColumn sysname output',@entityTable=@entityTable output,@uniquekeyColumn=@uniquekeyColumn output
			/* 2013-08-16 NYB Case 436316 added []'s around entityTable - to stop it erroring when entityTable was called a sql recognised name - like 'File': */
			select @sqlcmd = 'update ['+@entityTable+'] set lastUpdated=getDate(), '+'lastupdatedby='+@lastupdatedByColumn+', '+'lastupdatedbyPerson='+@lastUpdatedByPersonColumn+'
				from ['+@entityTable+'] e inner join #I i on i.entityID = e.'+@uniquekeyColumn
			exec (@sqlcmd)
		END
		ELSE
		BEGIN
			select @sqlcmd = 'select distinct top 1 @entityTable = entityTable, @uniquekeyColumn = uniqueKey  from vFlagDef v
				inner join #d d on d.flagId = v.flagID'
			exec sp_executesql @statement=@sqlcmd,@params=N'@entityTable sysname output, @uniquekeyColumn sysname output',@entityTable=@entityTable output,@uniquekeyColumn=@uniquekeyColumn output
			
			/* 2013-08-16 NYB Case 436316 added []'s around entityTable - to stop it erroring when entityTable was called a sql recognised name - like 'File': */
			select @sqlcmd = 'update ['+@entityTable+'] set lastUpdated=getDate(), '+'lastupdatedby='+@lastupdatedByColumn+', '+'lastupdatedbyPerson='+@lastUpdatedByPersonColumn+'
				from ['+@entityTable+'] e inner join #D d on d.entityID = e.'+@uniquekeyColumn
			exec (@sqlcmd)
		
		
		END		


	END  /* end of if 'useful' fields in flags updated */

	print 'ModRegister Time Taken: ' + convert(varchar,datediff (ms,@startTime, getdate()))

 END 
