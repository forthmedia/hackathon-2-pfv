createPlaceHolderObject 'procedure', 'GetElementTree'

/****** Object:  StoredProcedure [dbo].[GetElementTree]    Script Date: 10/06/2015 13:25:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
ALTER PROCEDURE [dbo].[GetElementTree]

	-- tried building the main query more cleverly - leaving out any joins not required for the set of parameters passed, thought might be quicker not having to join to all the record rights tables etc.
	-- problems encountered with passing table type to sp_executesql, performance didn't seem much better!
	


	-- note that the defaults will bring back the whole of a live tree, regardless of user and country rights
	-- however when coding, it is best to always set a personid - even if it is -1.
(	@ElementID int,				-- ElementID of the Node at the top of the Branch
	@PersonID int = -1,			-- ID of user for which to retrieve elements. 
						-- if -1 brings back all records, 
						-- 0 would bring back all records with no rights
	@statusid int = 4,
--	@Status nvarchar(100) = '',		-- Filter on Branch Status - defaults to only show live.  other statuses give a tree with all items between that status and live (eg status id 2 returns tree with statuses 2,3,4).  Statusid -1 gives all statuses
	@Depth int =0	,			-- Filter on Branch Depth Optional - Defaulting to show full depth
	@countryid varchar(400) = null,			-- if null then calculated from personid.  0 bring back all records without countryscope	. -1 brings back all record regardless of countryscope
	@isLoggedin int = -1 ,			-- 1 logged in , 0 not logged in, -1 bring back all records
	@usergrouplist  varchar(max) = null,	-- can pass in a comma list of usergroupids instead of a personid. 
						-- if -1 brings back all records regardless of rights, 
						-- 0 brings back all records with no rights

	@returnViewRightsLists bit = 0   ,	 -- decided that getting rights list for each record was probably a bit slow and not always needed,so parameterised
	@returnEditRightsForPersonID int = 0   ,	-- works out whether user has edit rights to this item in the tree, again it is optional
	@justReturnNodes bit = 0	,		-- just returns the list of ids without all the other columns
	@Date datetime = 0,     -- this should really be defaulting to getdate() but I couldn't get it to work, so defaults to 0 and later if @date is 0 then is set to getdate
	@cacheName varchar(255) = ''	,
	@useTempRightsTable int = 1,	   -- testing using temptable of all elements person has rights to
	@branchList varchar(100) = '',
	@returnBranchSiblings int = 0		)


as


/*

WAB 15/3/5   added checking for login
WAB 21/3/5   can pass in usergroupid list instead of a personid.  Note that needs function dbo.listfind
WAB 13/05/05 altered my sortorder stuff to use a much simpler concept than my previous thing
	added support for new values of showOnMenu
	0 - don't show
	1 - do show
	-1  - only show when logged out
	2   -	only show when logged in 
	these values are inherited to some extent as shown on this table, 
	
	parent	0	1	-1	2
	child
	0	0	0	0	0	
	1	0	1	-1	2
	-1	0	-1	-1	0
	2	0	2	0	2
WAB 13/05/05 added support for content expiration
WAB 21/06/05 changed name to getElementTree at same time as work in relatElementTree.cfc
WAB  05/07/05   added support for getting edit rights back and also get the viewrights as a list of usergroupids
WAB	12/09/05 brings back lastupdated date in query
WAB 03/10/05 added support for a list of countryids being passed in.  
	Designed for showing trees to editors which contain all countries which they have an interest in
	03/10/05  Changed the way that @usergrouptable is used (now join to the recordrights table rather than join an IN  - makes it similar to how countries now done)
YMA	13/01/21 2013 Roadmap Content Deletion.  Added support for statusID 6, (archived).  These nodes are not returned by the standard calls, only when called with @statusid = 6

April 2006 added support for showonsecondary navigation
April 2006 added support for loginrequired being 0,1 or 2
October 2006  if not getting full depth of tree, still populate hasChildren field of last generation
November 2006 major rework to try and improve efficiency
	rewrote main query to use sp_executesql.  This allows me to build the query slightly differently for different input parameters.  Thought this might improve efficiency by eg not doing any joins when no person/usergroups passed in
	also allowed me to have switches to use different versions of the query (eg one version which works out the elementrights first (good for large trees) and one which does it all in one query (good for small trees)
	raninto problems because couldn't pass table type variables into sp_executesql
	Added a table to hold a cache of the ids in a tree
		
added parameters @branchList and @returnBranchSiblings which allow particular bits of a tree to be returned

added hasChildrenNotInResultSet

29/01/07  - confirmed as latest version!
18/12/2007  - WAB added some more nolocks
2013-11-05 WAB Added column TextIDList - which gives parentage in terms of textIDs.  Note that, unlike parentIDList, it includes the current element  

2015-11		WAB	Major changes to support AND/OR/NOT ing of user groups.  Use dbo.getRecordRightsTableByUserGroup() function
			Added use of dbo.getCountryScopeTable () - this deals with support for Level11 being used as an exclude
2016-01-07 	WAB Revert a change which I made during previous changes, see comment of this date
2016-06-15	NJH PROD2016-1256 - changed instances of @@identity with SCOPE_IDENTITY();
*/




set nocount on 

declare 
	  @sql  nvarchar(4000)
	, @sqlwhere nvarchar(4000)
	, @sqlforbranch  nvarchar(4000)
	, @rc int   -- rowcount
	, @generation int = 1
	, @entityTypeID int = 10

IF @personid =-1 and @countryid is null 				-- for complete tree
BEGIN
	set @countryid  = -1
END

IF @date = 0 
BEGIN
	set @date = getDate()    -- this is a hack because I could not get @date to default to getdate()  - see comment in parameter list
END


IF @countryid is null   -- work out a countryid if not supplied
BEGIN
	select @countryid = countryid from location inner join person on location.locationid = person.locationid where personid = @personid
END

	
IF @usergrouplist is null
BEGIN
	-- if no usergroup list is supplied then we generate one from the personid
	SELECT @usergrouplist = SUBSTRING(
			(SELECT ',' + CONVERT(VARCHAR,userGroupid) 
				FROM RightsGroup 
				WHERE personid = @personid
			FOR XML PATH('')),2,200000)
 

END
ELSE IF @usergrouplist = '0'
BEGIN
		-- get tree for person with no rights	
		select @personid = 0
END
ELSE IF @usergrouplist = '-1'
BEGIN
		-- get tree for person with all rights
		select @personid = -1
END
ELSE
BEGIN
		-- if passed @usergrouplist then we need to ignore anything passed as @personid
		select @personid = 0
END


IF @useTempRightsTable  = 1 
BEGIN

		create table #TempElementsWithRights (id int)
		
		insert into #TempElementsWithRights 
		select distinct 
		id 
		from
		
		element e WITH (NOLOCK)
		
		left join
		
			dbo.[getRecordRightsTableByUserGroup](@entityTypeID, @usergrouplist)  rr on e.hasRecordRightsBitMask & 1 = 1 AND rr.recordid = e.id 
		
		left join 	
			
			dbo.getCountryScopeTable (@entityTypeID, @countryid) cs on hascountryscope = 1 and cs.entityid = e.id 
		
		where

			(e.hasRecordRightsBitMask & 1 = 0 OR rr.level1 = 1 OR @personid = -1) 

		and (hascountryscope = 0 or cs.level1 = 1  or @countryid = '-1')    


END




/* 
	create the temporary table to hold the tree in 
	and put in the top element
	this ought to be built from the same code as the query in the loop below - but isn't yet, as a consequence it does not deal with excludes (but this isn't a biggie)!
*/

select distinct		
		0 as grandparentid, 
		id as parentid, 
		id as id , 
		case when getChildrenfromparentid <> 0 then getChildrenfromparentid ELSE id END as getChildrenOfID  , case when isnull(getContentFromID,0) = 0 then id ELSE getContentFromID END as getContentFromID, 
		@generation as generation, 
		-- power(100,4-@generation) * sortorder as sortorder,
		convert(varchar(100),'I')  as SortOrder,
		1 as showOnMenu,   -- I have decided to always set this to 1 can't really explain why, but if you are asking for a tree you would be suprised if nothin showed on the menu!
		loginrequired as loginrequired,
		showonSecondaryNav as showonSecondaryNav,
		hideChildrenFromMenu as hideChildrenFromMenu,	
		0 as hasChildren, 
		0 as hasChildrenNotInResultSet, 
		borderset as inheritedBorderSet, 
		convert(varchar(200),'0') as parentidlist,
		convert(varchar(max),null) as textidlist,
		case when @returnEditRightsForPersonID = 0 then 0 
			when not exists (select 1 from 
						recordrights rr where	rr.entity='element'  and rr.recordid = e.id and rr.permission & power(2,2-1) <> 0				
					) then 1
			when exists (select 1 from 
						recordrights rr
							inner join
					rightsgroup rg on rr.entity='element' and rr.usergroupid = rg.usergroupid and rr.recordid = e.id and permission & power(2,2-1) <> 0 and rg.personid = @returnEditRightsForPersonID
					) then 1
			ELSE 0 END   as editRights
into 
	#TempTable
from 
	element e WITH (NOLOCK)
		left join 
	dbo.[getRecordRightsTableByUserGroup](10,@usergrouplist) rr on rr.recordID = e.id
		left join 	
	dbo.getCountryScopeTable (@entityTypeID, @countryid) cs on hascountryscope = 1 and cs.entityid = e.id 

where 
	id = @elementid
	and (hascountryscope = 0 or cs.level1  = 1 or @countryid = '-1')
	and (hasRecordRightsBitMask & 1 = 0 or rr.level1 = 1 or @personid = -1)
	and (
			(@statusid = -1 and statusid != 6) -- YMA	2013/01/21	If statusID is set to -1 return all pages except statusID 6 ones.
			or (@statusid <> 5 and statusid >= @statusid and statusid <=4)
			or (@statusid = 5 and statusid in (4,5))
			or (@statusid = 6)		-- YMA	2013/01/21	If statusID is set to 6 return all pages.
		)
	and (
			(@isLoggedIn = -1 ) 
			or (@isLoggedIn = 1 and loginRequired <> -1) 
			or (@isLoggedIn = 0 and loginRequired <= 0)  
		)
	and (@Date is Null or expiresdate is null or @date < expiresdate)  -- date passed in must be before expiresdate
	and (@Date is Null  or date is null or @date > date)     -- date passed in must be after livedate

/* build SQL for doing the loop */

select @sqlwhere = ''
select @sql = '
		insert into #TempTable
			(grandparentid,parentid,id,getChildrenOfID,getContentFromID,generation,sortorder,showonMenu,loginrequired,showonSecondaryNav,hideChildrenFromMenu,haschildren,hasChildrenNotInResultSet,inheritedBorderSet,parentidlist,TextIdlist,editrights)

		select distinct 
			ei.parentid,
			ei.id, 
			e.id ,
			case when e.getChildrenfromparentid <> 0 then e.getChildrenfromparentid ELSE e.id END  ,
			case when isnull(e.getContentFromID,0) = 0  then e.id ELSE e.getContentFromID END ,
			 @generation , 
			ei.SortOrder + right(''00''+convert (varchar(3),e.sortorder %1000),3) 	+ right(''00''+convert (varchar(3),e.id %1000),3),
							/* --made up of the sort order followed by the elementid incase of duplicate sort orders (formatted to 3 digits)
							WAB 2015-11 tried to remove elementID from the sortOrder, because it prevents having items at the same position being sorted alphabetically
							WAB 2016-01-07	But actually it causes all sort of other problems because children appear in the wrong order.  I wonder if there is another way 
							*/   
			case 	
				when ei.hideChildrenFromMenu = 1 then 0		
				when ei.showOnMenu = 0 or e.showonMenu = 0 then 0
				when ei.showOnMenu = 1 then e.showOnMenu 
				when ei.showOnMenu = 2 and e.showOnMenu in (1,2) then 2 			
				when ei.showOnMenu = -1 and e.showOnMenu in (1,-1) then -1 			
				ELSE 0  
			END, 
			case 
				when ei.loginrequired > e.loginrequired then ei.loginrequired  
				ELSE e.loginrequired 
			END,
			e.showOnSecondaryNav,
			e.hideChildrenFromMenu,	
			0,
			0,
			case 
				when isnull(borderset,'''') <> '''' then borderset 
				ELSE inheritedBorderSet 
			END,
			ei.parentidlist + '','' + convert(varchar,ei.id),
			case 
				when ei.Textidlist is null then '''' 
				ELSE ei.Textidlist + '','' 
			END + 
			case 
				when isNull(e.elementTextid,'''') = '''' then convert(varchar,e.id) 
				ELSE e.elementTextid END,
		'

if @returnEditRightsForPersonID = 0
	select @sql +=  ' 0 as editRights'
ELSE
	/* TBD tidy this up */
	select @sql +=  '
			case 
			when not exists (select 1 from 
						recordrights rr where
							rr.entity=''element'' and rr.recordid = e.id and rr.permission & power(2,2-1) <> 0
					) then ei.editrights
			when exists (select 1 from 
						recordrights rr
							inner join
					rightsgroup rg on rr.entity=''element'' and rr.usergroupid = rg.usergroupid and rr.recordid = e.id and rr.permission & power(2,2-1) <> 0 and   rg.personid = @returnEditRightsForPersonID
					) then 1
			ELSE 0 END   as editRights
			'

	select @sql +=  '
		from
			#TempTable ei 
				inner join 
			element e on ei.generation = @generation - 1 and e.parentid = ei.getChildrenOfID 
	'


if @useTempRightsTable = 0 
BEGIN

	IF @countryid <> '-1' 
	BEGIN
		select @sql = @sql + '	
			left join 
			dbo.getCountryScopeTable (@entityTypeID, @countryid) cs on hascountryscope = 1 and cs.entityid = e.id 		'

		select @sqlwhere = @sqlwhere + '	
			and (hascountryscope = 0 or cs.level1 = 1 )    
		'

	END


	IF @personid = 0
	BEGIN
		select @sqlwhere +=  	' 
			and (hasRecordRightsBitMask & 1 = 0 ) '
	END		

	ELSE IF @personid <> -1 
	BEGIN

		select @sql +=  '	
			left join dbo.[getRecordRightsTableByUserGroup](@entityTypeID, @usergrouplist)  rr on e.hasRecordRightsBitMask & 1 = 1 AND rr.recordid = e.id 
			'

		select @sqlwhere += '	
			and (e.hasRecordRightsBitMask & 1 = 0 or rr.level1 = 1)
		'

	END

END
ELSE
BEGIN
	-- using the temporary table of element rights
--	IF @countryid <> '-1' or @personid not in (0,-1)
--	BEGIN
		-- using temporary rights table
		select @sql = @sql + '	
			left join #TempElementsWithRights et on (hasRecordRightsBitMask & 1 = 1 or hascountryscope = 1) and e.id = et.id	
		'

		select @sqlwhere = @sqlwhere + '	
			and ((e.hasRecordRightsBitMask & 1 = 0 and e.hascountryscope = 0) or et.id is not null)
		'
--	END

END



IF @statusid = 5 
	select @sqlwhere = @sqlwhere + ' and statusid in (4,5) '	
ELSE IF @statusid <> -1 and @statusid <> 6		-- ie @statusid is 0,1,2,3,4. In this case we get all the items with status greater than the requested status.  So if you ask for status 0 (draft), the tree includes status 1 (Approval), 2 (Translation), 3 (Pre-Live), 4 (Live) )
	select @sqlwhere = @sqlwhere + ' and (statusid >= @statusid and statusid <=4)'
ELSE IF @statusid = -1		-- YMA	2013/01/21	For @statusid -1 we return everything except archived (6)
	select @sqlwhere = @sqlwhere + ' and (statusid != 6)'

IF @isLoggedIn = 1
	select @sqlwhere = @sqlwhere + ' and e.loginRequired <> -1 '
ELSE IF @isLoggedIn = 0
	select @sqlwhere = @sqlwhere + ' and e.loginRequired <= 0 '


IF @Date is not Null 
	BEGIN	
		select @sqlwhere = @sqlwhere + '
		and (expiresdate is null or @date < expiresdate) '  -- date passed in must be before expiresdate
		select @sqlwhere = @sqlwhere + '
		and (date is null or @date > date)'     -- date passed in must be after livedate
	END



/* if dealing with a branch then add the branch items first */
if @branchList <> '' 
BEGIN

	select @sqlforbranch  = @sql + 'where (e.id in (' + @branchList +') '
	
	if @returnBranchSiblings = 1
		 select @sqlforbranch  = @sqlforbranch + ' or  (ei.id in (' + @branchList +')' + @sqlwhere + ')'


	select @sqlforbranch  = @sqlforbranch + ')'

	select @rc=1

	WHILE @rc <>0 
	BEGIN	
		select @generation = @generation + 1
		
		exec sp_executesql @stmt = @sqlforbranch , @params = N'@statusid int ,@isloggedIn int,@date datetime,@personid int,@generation int,@returnEditRightsForPersonID int,@branchList varchar (max)', @statusid = @statusid, @isloggedIn = @isloggedIn,@date = @date ,@personid = @personid, @generation = @generation, @returnEditRightsForPersonID = @returnEditRightsForPersonID,@branchList = @branchList--YMA	2013/01/21	Included new param into call.
		select @rc = @@rowcount	

		--print 'rows added to branch: ' + convert(varchar,@rc)
	END
		
	IF @depth <> 0 
		select @depth = @depth + @generation  -- depth of tree is counted from END of branch, so need to adjust to take this into account

	select @generation = @generation - 1   -- generation is now one more than it should be

END
/* END of dealing with branches */




/* now do main loop adding all item to the tree */
select @sql = @sql + ' where 1=1 ' + @sqlwhere 
select @rc=1

WHILE (@generation < @depth  or @depth=0)  and @rc <> 0 
BEGIN	
	select @generation = @generation + 1
	exec sp_executesql @stmt = @sql, @params = N'@statusid int, @isloggedIn int,@date datetime,@personid int,@generation int,@returnEditRightsForPersonID int,@branchList varchar, @entityTypeId int, @usergrouplist varchar(Max), @countryID varchar(max) ', @statusid = @statusid ,@isloggedIn = @isloggedIn,@date = @date ,@personid = @personid, @generation = @generation, @returnEditRightsForPersonID = @returnEditRightsForPersonID,@branchList = @branchList, @entityTypeId = @entityTypeId, @usergrouplist=@usergrouplist, @countryID = @countryID 
	select @rc = @@rowcount	
	
	-- print 'rows added: ' + convert(varchar,@rc)
	-- note that @@rowcount variable from this query is used to stop the loop 

END



update 
	#TempTable
set 
	haschildren = 1
from 
	#TempTable  
		inner join 
	#TempTable b on #TempTable.id = b.parentid 
					and b.id <> @elementid   -- WAB 21/11/60 this bit prevents the top element joining back to itself (because for topelement parentid is set to same as elementid).  On noticeable when tree requested for a node which has no children


-- if we haven't got the whole depth of a tree we might want to know whether an item has children or not
-- added WAB 09/10/06
if @depth <> 0 and @depth <> 99 
Begin
	update #TempTable
	set hasChildrenNotInResultSet = 1
	from #TempTable  inner join element b on #TempTable.id = b.parentid and generation  = @generation
End

-- or if we have been getting a branch we will need to mark children of all the uncles down the branch
if @branchList <> ''
Begin
	update #TempTable
	set hasChildrenNotInResultSet = 1
-- 	select * 
	from #TempTable  inner join element b on #TempTable.id = b.parentid 
	where haschildren = 0
	-- and generation  = @generation
End


/*
Update the elementTreeCache table if required
*/

IF @cacheName  <> '' 
BEGIN

	declare	@elementTreeCacheID int

	select @elementTreeCacheID = elementTreeCacheID from elementTreeCache where cachename =  @cacheName
	
	if @elementTreeCacheID is null
	 BEGIN

		insert into elementTreeCache
		(topelementid, cachename)
		values (@elementid, @cacheName)

		select @elementTreeCacheID = SCOPE_IDENTITY()
	 END
	ELSE
	  BEGIN
		update elementTreeCache set lastupdated = getdate() where elementTreeCacheID = @elementTreeCacheID
	  END

	/* 
	decided to do separate updates, insert and deletes rather than a delete and an insert.  
	Hope it is quicker and also might prevent issues if no locking
	*/
	update
		elementTreecachedata
		set sortorder = elementTreecachedata.sortorder
	from elementTreecachedata  inner join #TempTable ei on elementTreecachedata.elementtreecacheid = @elementTreeCacheID and elementTreecachedata.elementid = ei.id
	where elementTreecachedata.sortorder <> ei.sortOrder

	insert into elementTreecacheData 
	(elementTreeCacheID, elementid, sortorder)
	select @elementTreeCacheID, ei.id, ei.sortorder
	from #TempTable ei WITH (NOLOCK) left join elementTreecachedata ecd WITH (NOLOCK) on ecd.elementtreecacheid = @elementTreeCacheID and ecd.elementid = ei.id
	where ecd.elementid is null

	delete elementTreecachedata 		
	from elementTreecachedata  ecd WITH (NOLOCK) left join #TempTable ei WITH (NOLOCK) on ecd.elementid = ei.id
	where ecd.elementtreecacheid = @elementTreeCacheID  and ei.id is null


END
/* End of elementTreeCache stuff*/



IF @justReturnNodes = 1
BEGIN
	select 	
		ei.id as node
	from 
		#TempTable ei 
	order by ei.sortorder, ei.id
END
-- this query may need modifying to get the correct values when the content comes from another element
-- currently it just give a contentid and the element headline phrase is correct
-- use content.
ELSE
  Begin


	SELECT 
		ei.sortorder,


		CASE 
			WHEN ei.parentID IS NULL THEN 0
			WHEN ei.parentid = @ElementID  THEN ei.id
			WHEN ei.grandparentid = @ElementID  THEN ei.parentID
			ELSE ei.grandparentid 
		END AS sectionEID,    -- WAB not sure exactly what this does!


--		was:
--		CASE 
--			WHEN element.parentID IS NULL THEN 0
--			WHEN element.parentid = ' + CONVERT(varchar,@ElementID) + ' THEN #temp1.id

--			WHEN p.parentid = ' + CONVERT(varchar,@ElementID) + ' THEN element.parentID
--			ELSE p.parentid 
--		END AS sectionEID, 
		ei.parentid as parent ,
		e.id as node,
		case when content.id is null then e.ID ELSE content.id END  as ContentID,
		case when content.id is null then convert(varchar,e.ID) ELSE convert(varchar,content.id) END  as ContentIDAsString,
		e.headline as name,
		ei.loginrequired,
		ei.showOnMenu as showOnMenu,
		case when ei.showOnmenu in (1,2) then 1 ELSE 0 END   as ShowOnMenuLoggedIn,
		case when ei.showOnmenu in (1,-1) then 1 ELSE 0 END   as ShowOnMenuLoggedOut,
		ei.showOnSecondaryNav,
		ei.hideChildrenFromMenu,	
		ei.inheritedborderset,
		ei.parentidlist,
		ei.textidlist,
		ei.editrights,
		haschildren,
		hasChildrenNotInResultSet,	
		'phr_headline_element_'+convert(varchar,e.id) as headline,
		e.headline as UnTranslatedTitle,
		p.headline as ParentHeadline,
		ei.parentid,
		e.parentid as actualParentid,
		generation,
		e.statusid,
		es.status,
		e.showSummary,
		e.showLongSummary,
		e.stylesheets,
		E.SortOrder,
--		E.showOnMenu,
		E.Date, 
		E.Date as livedate, 
		E.URL, 
		E.ElementTypeID, 
		E.Keywords, 
		E.isExternalFile,
		isnull(E.elementTextID,'') as elementTextID,  -- remove null, 'cause queryofQueries doesn't like nulls in CF
		E.isLive, 
		E.ElementTypeID,
		E.LongSummary,
		E.Image,
		E.hidden,
		E.ExpiresDate,
		E.includeScreen,
		E.SummaryInContents,
		E.rate,
		E.hasCountryScope,
		E.parameters,
		E.hasRecordRightsBitMask & 1 as hasRecordRights,
		case when @returnViewRightsLists = 0 then '' when E.hasRecordRightsBitMask & 1 = 1 then dbo.getRecordRightsList(e.id,@entityTypeID,1,0) ELSE '' END as recordRightsIds ,
		case when @returnViewRightsLists = 0 then '' when E.hasRecordRightsBitMask & 1 = 1 then dbo.getRecordRightsList(e.id,@entityTypeID,1,1) ELSE '' END as recordRights ,
		case when @returnViewRightsLists = 0 then '' when E.hascountryscope = 1 then dbo.getCountryScopeList(e.id,@entityTypeID,1,1) ELSE '' END as countryScope ,
		e.lastupdated		

	frOM #TempTable ei WITH (NOLOCK)
		inner join 
		element e WITH (NOLOCK) on e.id = ei.id
		inner join elementStatus es WITH (NOLOCK)
		on es.statusid = e.statusid
		left join element p WITH (NOLOCK) --parent
--		on p.id = e.parentid
		on p.id = ei.parentid
		inner join  element content
		on ei.getContentFromID = content.id


	order by ei.sortorder, e.id


END

		
