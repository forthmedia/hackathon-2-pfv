/****** Object:  StoredProcedure [dbo].[sp_fieldTrigger_locHQAccManagerModified]    Script Date: 02/17/2014 15:33:44 ******/
if exists (select * from sysobjects where name = 'sp_fieldTrigger_locHQAccManagerModified' and XTYPE = 'P')
	DROP Procedure sp_fieldTrigger_locHQAccManagerModified

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fieldTrigger_locHQAccManagerModified] 
	@flagid int, 
    @modaction varchar(10),
    @triggerInitiatedBy int, 
    @RecordUpdatesAgainstID int,
    @triggerInitiatedByPersonID int = null

AS
Set nocount on


IF @modaction in ('FA','FM') 
BEGIN
	declare @orgAccManagerFlagID int
    select @orgAccManagerFlagID=flagID from flag where flagTextID='accManager'
    
    -- get current account managers for existing HQ locations and any associated organisation account managers for those locations
    select distinct @orgAccManagerFlagID as orgAccManagerFlagID,l.organisationID,locAccManager.data as locAccManagerPersonID,orgAccManager.data as orgAccManagerPersonID
		into #LocHQAccManagers
	from location l
		inner join #modifiedEntityIDs e on l.locationID = e.entityid
		inner join integerFlagData locAccManager on locAccManager.entityID = l.locationID and locAccManager.flagID = @flagID
		inner join integerFlagData orgHQ on orgHQ.entityID = l.organisationID and orgHQ.data = l.locationID
		inner join flag f on f.flagID = orgHQ.flagID and f.flagTextID='HQ'
		left join integerFlagData orgAccManager on orgAccManager.entityID = l.organisationID and orgAccManager.flagID = @orgAccManagerFlagID
		
    -- insert account managers for organisations that don't already have one set
    insert into integerFlagData(flagID,entityID,data,createdBy,lastUpdatedBy)
    select orgAccManagerFlagID,l.organisationID,l.locAccManagerPersonID,@RecordUpdatesAgainstID,@RecordUpdatesAgainstID
	from 
		#LocHQAccManagers l
	where
		l.orgAccManagerPersonID is null
	
	-- update the account managers for organisations where they are not the same as the location HQ	
	update integerFlagData
		set data = locAccManagerPersonID
	from
		#LocHQAccManagers l
		inner join integerFlagData ifd on ifd.entityID = l.organisationID and ifd.flagID = @orgAccManagerFlagID
	where 
		l.orgAccManagerPersonID is not null and l.orgAccManagerPersonID ! = l.locAccManagerPersonID
   
END
Set nocount off