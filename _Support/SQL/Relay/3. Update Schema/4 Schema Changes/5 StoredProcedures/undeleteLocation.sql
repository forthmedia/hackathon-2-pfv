/*
2016/09/01 NJH JIRA PROD2016-125 - turn identity insert on/off
*/

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'UndeleteLocation' and Type = 'P')
	DROP PROCEDURE UndeleteLocation
	
GO
	
CREATE    proc [dbo].[UndeleteLocation]
		@SiteName	varchar(64)	= null,
		@Country	varchar(64)	= null,
		@LocationID	int		= null,
		@DoNotRecoverPersons int = 0
as
declare @status int,
	@sqlerror int,
	@rcount int,
	@countryid int,
	@moddate datetime,
	@ActionByCF int,
	@delLocationID int
set nocount on
if @SiteName is null 
 and @Country is null
 and @LocationID is null
begin
	   RAISERROR ('Error Location Id or Sitename & country must be supplied',
	      16, 1, @sqlerror)
	   RETURN 1
end
if @LocationID is null
begin
	select @countryid = countryid
	  from country
	 where CountryDescription = @Country
	if @countryid is null
	begin
	   RAISERROR ('Error Invalid country : %s',
	      16, 1, @country)
	   RETURN 1
	end
	select @LocationID = locationid
	  from LocationDel
	 where CountryId = @CountryId
	   and Sitename = @SiteName
	if @locationid is null
	begin
	   RAISERROR ('Error Sitename : %s and country : %s not found in Deleted Set',
	      16, 1, @sitename,@country)
	   RETURN 1
	end
end
else
begin
	select @delLocationID = locationid
	  from LocationDel
	 where locationId = @LocationID
	if @delLocationID is null
	begin
	   RAISERROR ('Error Location : %d not found in Deleted Set',
	      16, 1, @locationId)
	   RETURN 1
	end
end
begin transaction
if not exists(select * from organisation o
		inner join locationdel d on d.organisationid = o.organisationid
					and d.locationid = @locationid)
begin
	set identity_insert dbo.organisation on
	insert organisation(OrganisationID, OrganisationName, AKA, DefaultDomainName, OrgURL, Notes, MatchName, Confidence, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, Dupe, CountryID, AccountMngrID, AccountNumber, R1OrgID, R2OrgID)
		select od.OrganisationID, od.OrganisationName, od.AKA, od.DefaultDomainName, od.OrgURL, od.Notes, od.MatchName, od.Confidence, od.Active, od.CreatedBy, od.Created, od.LastUpdatedBy, od.LastUpdated, od.Dupe, od.CountryID, od.AccountMngrID, od.AccountNumber, od.R1OrgID, od.R2OrgID
		from OrganisationDel od
			inner join locationdel d on d.organisationid = od.organisationid
					and d.locationid = @locationid
	set identity_insert dbo.organisation off
	
	select	@sqlerror = @@error,
		@rcount   = @@rowcount
		
	if @sqlerror <> 0
	 or @rcount <> 1
	begin 
	   RAISERROR ('Error : %d inserting organisation for Location %d ',
	      16, 1, @sqlerror,@locationid)
	   rollback transaction
	   RETURN 1
	end
end

set identity_insert dbo.location on			
insert location
(LocationID, Address1, Address2, Address3, Address4, Address5, PostalCode, CountryID, OrganisationID, SiteName, Telephone, Fax, FaxStatus, SpecificURL, Direct, Active, MatchName, MatchTel, Note, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualCountry, EmailDomain, R1LocID, R1ParentID, address7, address8, address9, nSiteName, nAddress1, nAddress2, nAddress3, nAddress4, nAddress5, CompanyLevel, R2LocID, R2OrgID, Latitude, Longitude)
select distinct LocationID, Address1, Address2, Address3, Address4, Address5, PostalCode, CountryID, OrganisationID, SiteName, Telephone, Fax, FaxStatus, SpecificURL, Direct, Active, MatchName, MatchTel, Note, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualCountry, EmailDomain, R1LocID, R1ParentID, address7, address8, address9, nSiteName, nAddress1, nAddress2, nAddress3, nAddress4, nAddress5, CompanyLevel, R2LocID, R2OrgID, Latitude, Longitude
from locationdel
where locationid = @locationid
set identity_insert dbo.location off
select	@sqlerror = @@error,
	@rcount   = @@rowcount
	
if @sqlerror <> 0
 or @rcount <> 1
begin 
   RAISERROR ('Error : %d inserting Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
delete locationdel
where locationid = @locationid
select	@sqlerror = @@error
	
if @sqlerror <> 0
 
begin 
   RAISERROR ('Error : %d deleting delete record for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
select	@moddate = moddate,
	@ActionByCF = ActionByCF
  from modregister m
	where action='LD'
	and recordid = @locationid
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d selecting ModRegister details for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
select distinct personid into #pundel
from PersonDel p
 inner join modregister r on r.recordid=p.personid
				and r.moddate between dateadd(hh,-12,@moddate)
						  and dateadd(hh,12,@moddate)
				and r.action = 'PD'
				-- and r.ActionByCF = @ActionByCF
 where p.locationid = @locationid
and 0= @DoNotRecoverPersons
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting person details into temporary table for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end

set identity_insert dbo.person on	
insert Person(
       PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, EmailStatus, JobDesc, Language, Notes, LocationID, OrganisationID, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualLocation, Title, Department, R1PerID, R1LocID, sex, initials, SecondName, nFirstName, nLastName, nTitle, emailStatusOld, R2PerId, R2LocId)
select distinct PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, EmailStatus, JobDesc, Language, Notes, p.LocationID, OrganisationID, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualLocation, Title, Department, R1PerID, R1LocID, sex, initials, SecondName, nFirstName, nLastName, nTitle, emailStatusOld, R2PerId, R2LocId 
from PersonDel p
	where personid in(select * from #pundel)
set identity_insert dbo.person off
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting person rows for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
delete PersonDel 
 where personid in(select * from #pundel)
select	@sqlerror = @@error
	
if @sqlerror <> 0
 
begin 
   RAISERROR ('Error : %d deleting person delete record for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
select distinct flagid into #f from  modregister r 
				where r.moddate between dateadd(mi,-1,@moddate)
						  and dateadd(mi,1,@moddate)
				and r.action = 'FD'
				-- and ActionByCF=@ActionByCF
 and( recordid in(select * from #pundel)
	or recordid = @locationid)
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting flag rows into temporary table for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end


select * from #f
insert BooleanFlagData(EntityID,FlagID)
select distinct recordid,flagid from  modregister r 
				where r.moddate between dateadd(mi,-1,@moddate)
						  and dateadd(mi,1,@moddate)
				and r.action = 'FD'
				-- and ActionByCF=@ActionByCF
 and( recordid in(select * from #pundel)
  	or recordid = @locationid)
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'boolean'
  and flagtextID not in ('DeleteLocation','DeletePerson'))
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting boolean flag rows for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
insert TextFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,oldval from  modregister r 
				where r.moddate between dateadd(mi,-1,@moddate)
						  and dateadd(mi,1,@moddate)
				and r.action = 'FD'
				-- and ActionByCF=@ActionByCF
 and( recordid in(select * from #pundel)
   OR (recordid = @locationid and flagid not in (select flagid from TextFlagData where EntityID=@locationid) )
   )
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'text')
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting text flag rows for Location %d ',
      16, 1, @sqlerror,@locationid)

   rollback transaction
   RETURN 1
end
insert IntegerFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,convert(int,oldval) from  modregister r 
				where r.moddate between dateadd(mi,-1,@moddate)
						  and dateadd(mi,1,@moddate)
				and r.action = 'FD'
				-- and ActionByCF=@ActionByCF
 and( recordid in(select * from #pundel)
  	or recordid = @locationid)
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'integer')
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting integer flag rows for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
insert DateFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,convert(datetime,oldval) from  modregister r 
				where r.moddate between dateadd(mi,-1,@moddate)
						  and dateadd(mi,1,@moddate)
				and r.action = 'FD'
				-- and ActionByCF=@ActionByCF
 and( recordid in(select * from #pundel)
  	or recordid = @locationid)
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'date')
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting date flag rows for Location %d ',
      16, 1, @sqlerror,@locationid)
   rollback transaction
   RETURN 1
end
insert locationdatasource (locationid,datasourceid,RemoteDataSourceID)
 select @locationid,1,' '


commit transaction
return 0

GO