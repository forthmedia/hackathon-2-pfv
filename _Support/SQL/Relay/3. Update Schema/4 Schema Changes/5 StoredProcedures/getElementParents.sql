
ALTER             PROCEDURE [dbo].[getElementParents]
	@ElementID int,				-- ElementID of the Node 
	@TopID int = 0,				-- ElementID of the Node which may be on tree
	@PersonID int = null,			-- ID of user for which to retrieve elements. 
						-- if -1 brings back all records, 
						-- 0 would bring back all records with no rights
	@statusid int = 4,
--	@Status nvarchar(100) = '',		-- Filter on Branch Status - defaults to only show live
	@countryid int = null,			-- if null then calculated from personid.  0 bring back all records without countryscope	. -1 brings back all record regardless of countryscope
	@permission int = 1			-- 1 view, 2 edit, 3 super edit

as


/*

WAB 26/4/5   added isTreeRoot to the query being returned
WAB 21/12/2005 added check for @countryID being passed in as ''

WAB mar 2006 added showonmenu,hidechildrenfrommenu,elementTypeid
		added support for excluding a country - note need fn_getcountryrightsV2
WAB 2015-11-18 PROD2015-290 Altered to use getRecordRightsList(), getCountryScopeList() and getRecordRightsTableByPerson() and hasRecordRightsBitMask
					
*/


set nocount on

declare @generation int
declare @maxgeneration int
declare @finishloop bit
declare @topidfound bit
declare @multitree bit
declare @thisRowCount int

select @generation =0
select @finishloop =0
select @topidfound =0
select @multitree  =0


IF @personid =-1		-- for complete tree the personid is set to -1 and we basically ignore country
	set @countryid  = -1


-- work out a countryid if not supplied
 IF isnull(@countryid,'') = ''     -- WAB 21/12/05 slightly odd testing an integer against '', but I want to trap @countryid being passed in as '' (which oddly seems to be allowed)
	select @countryid = countryid from location inner join person on location.locationid = person.locationid where personid = @personid



select @countryid = isnull(@countryid,0)


/* Create and seed the #tempTable.  This record isn't returned */
select 
	@elementid  as parentid
	, 0 as elementid 
	, @generation as generation
	, statusid
	, 1 as hasRecordRights
	, 1 as hasCountryScope
	, 1 as recordrightsOK
	, 1 as countryscopeOK
	, convert(varchar(max),'') as headline
	, convert(varchar(max),'') as recordrights
	, convert(varchar(max),'') as countryscope
	, borderset
	, loginrequired
	, isTreeRoot
	, showOnMenu
	, hideChildrenFromMenu
	, elementTypeid
into #TempTable
from 
	element 
where id = @elementid


-- this loop goes back up the tree to see what the rights are of its parents
-- I decided to all the information in the table mainly for debugging purposes
-- at the end, if there are any lines in the table which don't have recordrightsOK and countryscopeOK set then all set to 1 
-- then there user doesn't have rights to the element at the given permission level
-- note that we aren't actually worried about countryscope for permissions other than 1 (viewing)

-- stop when we hit the top id, or come to the end of the tree (ie the top)

WHILE @finishloop <> 1
  BEGIN	

if @topid <> 0 
-- this bit deals with multiple parentage - puts an extra copy of the element into the temporary table if necessary
-- only done if there is a topid set - ie we know where we are going
	begin
		insert into #TempTable
			(parentid
			,elementid
			,generation
			,statusid
			,hasRecordRights
			,hasCountryScope
			,recordrightsOK
			,countryscopeOK
			,headline
			,recordrights
			,countryscope
			,borderset
			,loginrequired
			,isTreeRoot
			,showOnMenu
			,hideChildrenFromMenu
			,elementTypeid)
		select 
			otherparents.id
			,ei.elementid
			,ei.generation
			,ei.statusid
			,ei.hasRecordRights
			,ei.hasCountryScope
			,ei.recordrightsOK
			,ei.countryscopeOK
			,ei.headline
			,ei.recordrights
			,ei.countryscope
			,ei.borderset
			,ei.loginrequired
			,ei.isTreeRoot
			,ei.showOnMenu
			,ei.hideChildrenFromMenu
			,ei.elementTypeid
		from #TempTable ei
			inner join
		     element otherparents on ei.parentid = otherparents.getChildrenfromparentid and ei.parentid <>0
		where generation = @generation 
	end


select @generation = @generation + 1


-- this bit gets the parents of the previous generation with all the rights
insert into #TempTable
	(
	parentid
	,elementid
	,generation
	,statusid
	,hasRecordRights
	,hasCountryScope
	,recordrightsOK
	,countryscopeOK
	,headline
	,recordrights
	,countryscope
	,borderset
	,loginrequired
	,isTreeRoot
	,showOnMenu
	,hideChildrenFromMenu
	,elementTypeid
	)
select distinct 
	e.parentid, 
	e.id,
	@generation,
	e.statusid,
	convert(bit,hasRecordRightsBitMask & power(2,@permission -1)),
	e.hascountryscope,
	case 
		when hasRecordRightsBitMask & power(2,@permission -1) = 0 then 1 
		when isNull(rr.permission,0) & power(2,@permission-1) <> 0 then 1 else 0 
	end as recordrightsOK,
	case 
		when e.hascountryscope = 0 then 1 
		else csc.level1
	end as countryscopeOK,
	e.headline,
	dbo.getRecordRightsList(e.id,10,@permission,1) , 
	dbo.getCountryScopeList(e.id,'element',1,1) ,
	e.borderset,
	e.loginrequired,
	e.isTreeRoot,
	e.showOnMenu,
	e.hideChildrenFromMenu,
	e.elementTypeid
from
	#TempTable ei 
		inner join 
	element e  on ei.generation = @generation - 1 and ei.parentid = e.iD 
		left join 	
	dbo.[getRecordRightsTableByPerson](10, @personid)  rr on e.hasRecordRightsBitMask & power(2,@permission -1) <> 0 AND rr.recordid = e.id 
		
		left join 	
	dbo.[getCountryScopeTable]	(10, @countryid) csc on e.hascountryscope = 1 and csc.entityid = e.id 
where 
	1=1


-- have we reached the end of the line?
select @thisRowCount = @@rowcount

If @thisRowCount = 0
   select @finishloop = 1

If @thisRowCount > 1
   select @multitree = 1

If @topid <> 0  
    select @finishloop = 1, @topidfound = 1 from #TempTable where elementid = @topid


END

select @maxgeneration = @generation


-- have we ended up with more than one tree 
if @multitree =1 and @topidfound =1 
   begin
	-- delete the item at the top of the tree which isn't the one we want
	delete from #TempTable where (generation = @generation and elementid <> @topid) or (generation  <> @generation and parentid = 0) 

	-- loop down the tree deleting the ones which don't now have a parent
	WHILE @generation <>0
	  BEGIN	
		select @generation = @generation -1  


		delete #TempTable  from #TempTable left join #TempTable parent on #TempTable.parentid = parent.elementid 
		where #TempTable.generation = @generation  and parent.elementid is null

	  end

  end


select  
	parentid,
	elementid,
	@maxgeneration - generation  as generation,  -- reverse generation so that they are consistent with getelementbranch
	-- generation as orig,
	statusid,
	hasRecordRights,
	hasCountryScope,	
	recordrightsOK,
	countryscopeOK,
	loginrequired,
	headline,
	recordrights,
	countryscope,
	isnull(borderset,'') as borderset,
	isTreeRoot,
	showOnMenu,
	hideChildrenFromMenu,
	elementTypeid
	 
from 
	#TempTable 
where 
	generation <> 0 
order by 
	generation 
