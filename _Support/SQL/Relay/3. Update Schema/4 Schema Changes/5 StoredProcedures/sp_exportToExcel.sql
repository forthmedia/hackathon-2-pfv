/****** 2013-04-30 NYB Case 343151 a stored procedure to export to Excel ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_exportToExcel]'))
	DROP PROCEDURE [dbo].[sp_exportToExcel]
GO

CREATE Procedure [dbo].[sp_exportToExcel] @tableName VARCHAR(100),@filePath as varchar(2000),@tempTable bit = 0

AS

DECLARE  
    @query VARCHAR(max)
    ,@bcpquery VARCHAR(8000)
    ,@bcpconn VARCHAR(max);

		DECLARE @SQLString VARCHAR(1000);
		DECLARE @CSVString VARCHAR(4000);
		DECLARE @headings VARCHAR(4000);
		DECLARE @headingsAliased VARCHAR(4000);
		DECLARE @column_name VARCHAR(200);
		DECLARE @data_type VARCHAR(200);
		DECLARE @firstRun bit;
		DECLARE @dbName VARCHAR(100);
		SELECT @dbName = DB_NAME();
		SET @firstRun = 1;

		if @tempTable = 1 
			set @SQLString = 'DECLARE column_Cursor CURSOR FOR select c.name as COLUMN_NAME,t.name as data_type from tempdb.sys.columns c inner join tempdb.sys.types t on c.user_type_id=t.user_type_id where object_id=object_id(''tempdb..' + @tableName + ''') order by column_id'
		else 
			set @SQLString = 'DECLARE column_Cursor CURSOR FOR select COLUMN_NAME,data_type from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @tableName + ''' order by ORDINAL_POSITION'
		EXEC(@SQLString)

		OPEN column_Cursor;
		fetch column_Cursor into @column_name,@data_type
		WHILE (@@fetch_status <> -1)
		   BEGIN
				if @firstRun = 1
					begin
						set @firstRun = 0;
						set @headings = @column_name;
						set @headingsAliased = ''''+@column_name+''' as '+@column_name;
						set @CSVString = 'CAST('+@column_name+' as nvarchar(4000)) as '+@column_name;
					end
				else
					begin
						set @headings = @headings+','+@column_name;
						set @headingsAliased = @headingsAliased+','''+@column_name+''' as '+@column_name;
						set @CSVString = @CSVString + ',CAST('+@column_name+' as nvarchar(4000)) as '+@column_name;
					end
				fetch column_Cursor into @column_name,@data_type
		   END;
		CLOSE column_Cursor;
		DEALLOCATE column_Cursor;

		SET @query      = 'select '+@headings+' from (select 1 as sorter,'+@headingsAliased+' union select 2 as sorter, '+@CSVString+' from '+@dbName+'.dbo.'+@tableName+') as dt order by sorter'
		
		SET @bcpconn    = '-T' -- Trusted
		--SET @bcpconn    = '-U <username> -P <password>' -- SQL authentication

		SET @bcpquery = 'bcp "' + replace(@query, char(10), '') + '" QUERYOUT "' + @filePath + '" -w ' + @bcpconn + ' -S ' + @@servername 

		EXEC master..xp_cmdshell @bcpquery  		




