/**
* Updates
*
* 11/01/2016 DAN Case 447205 - fixing duplicate key error on Salesforce import (violation of Primary Key constraint on inserting into BooleanFlagData)
* 28/06/2016 DAN move it out of core as it turns out it's customer specific (Aerohive), it will need to be recreated on their site after the upgrade
* 
**/

if exists (select 1 from sysobjects where name='sp_fieldTrigger_roleFlagsModified' and xType='P')
if not exists (select 1 from fieldtrigger where triggername='sp_fieldTrigger_roleFlagsModified')
drop procedure sp_fieldTrigger_roleFlagsModified

GO
