IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'UndeleteOrganisation' and Type = 'P')
	DROP PROCEDURE UndeleteOrganisation
	
GO
	

/*
UndeleteOrganisation

Author:  WAB    2/11/2005

Hacked out of undeleteLocation procedure

Made some changes to logic of original code 
	- the flagdeletions in modregister do not have to have the same ActionByCF as the organisation delete   (the mod register 
	- the flags which are restored must have been deleted within a minute of the organisation deletion (was 12 hours?)

Does a basic undelete of organisation and basic flags

MODIFICATIONS
2011-04-13 NYB	fixed issue where there was a boolean flag that had a fieldtrigger that set a dateflag,  
				which system tried to set further on.  Have add 'if flag not already set' to flag areas
2011-04-13 NYB	LHID6552: Rewrote - falls over when organisation hadn't been deleted properly at the start
2016/09/01 NJH JIRA PROD2016-125 - turn identity insert on/off
*/


CREATE  proc [dbo].[UndeleteOrganisation]
		@OrganisationID	int		= null
as
declare @status int,
	@sqlerror int,
	@rcount int,
	@countryid int,
	@moddate datetime,
	@ActionByCF int /*delete?*/
set nocount on

begin transaction
if not exists(select * from organisation o where organisationid = @organisationid)
begin
	set identity_insert dbo.organisation on
	
	declare @cols varchar(max) = ''
	declare @sqlcmd varchar(max) = ''
	select @cols = @cols+case when @cols = '' then '' else ','end +name from sys.columns where is_computed = 0 AND object_id = OBJECT_ID('organisation')
	select @sqlcmd = 'insert into organisation('+@cols+') select top 1 '+@cols+' from  from OrganisationDel where organisationid = '+@organisationid +' order by lastupdated desc'
	exec (@sqlcmd)
	/*	NJH 2016/09/01 - can't do below after organisaitonID has become an IDENTITY column
	insert into organisation 
		select top 1 * from OrganisationDel where organisationid = @organisationid 
		order by lastupdated desc*/
	set identity_insert dbo.organisation off

	select	@sqlerror = @@error,
		@rcount   = @@rowcount
		
	if @sqlerror <> 0
	 or @rcount <> 1
	begin 
	   RAISERROR ('Error : %d inserting organisation  %d ',
	      16, 1, @sqlerror,@Organisationid)
	   rollback transaction
	   RETURN 1
	end

	delete organisationdel where organisationid = @organisationid
end


select	@moddate = moddate,@ActionByCF = ActionByCF
  from modregister m where action='OD' and recordid = @organisationid

print @@rowcount
select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d selecting ModRegister details for Organisation %d ',
      16, 1, @sqlerror,@organisationid)
   rollback transaction
   RETURN 1
end

print @moddate

/*  get all the last unique flags deleted in the 5 minutes surounding the deletion of this organisation */
select max(id) as id,r.flagid,v.flaggroupid,v.DataTableFullName,v.DataType into #f 
	from modregister r 
	inner join vflagdef as v on r.flagid=v.flagid and v.flagtextid not in ('DeleteOrganisation') and v.EntityTable='Organisation'
	where r.moddate between dateadd(mi,-5,@moddate) and dateadd(mi,+5,@moddate) and 
	r.action = 'FD' and recordid = @Organisationid
group by r.flagid,v.flaggroupid,v.DataTableFullName,v.DataType


/*  get the data for the flags found above */
select r.id,r.flagid,f.flaggroupid,r.recordid,r.oldval,f.DataTableFullName,f.DataType into #flagData 
	from modregister r 
	inner join #f as f on r.id=f.id 


/*  remove all siblings in a radio group - except the last added */
delete from #flagData where id in (
	SELECT f1.id  
	FROM #flagData f1 
	left join 
	(SELECT max(id) as id,flaggroupid FROM #flagData where DataType='Radio' 
	group by flaggroupid
	) as f2 on f1.id=f2.id 
	where DataType='Radio' and f2.id is null
)


select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting flag rows into temporary table for organisation %d ',
      16, 1, @sqlerror,@Organisationid)
   rollback transaction
   RETURN 1
end


/*  insert data into BooleanFlagData     */
	insert into BooleanFlagData (EntityID,FlagID) 
		select recordid,flagid from #flagData 
			where DataTableFullName='booleanflagdata'
			and flagid not in (select flagid from BooleanFlagData with(nolock) where recordid=@Organisationid) 

	select	@sqlerror = @@error

	IF @sqlerror <> 0
	begin 
	   RAISERROR ('Error : %d inserting boolean flag rows for Organisation %d ', 16, 1, @sqlerror,@Organisationid)
	   rollback transaction
	   RETURN 1
	end


/*  insert data into TextFlagData     */
	insert into TextFlagData (EntityID,FlagID,data)
		select recordid,flagid,oldval from #flagData 
			where DataTableFullName='TextFlagData'
			and flagid not in (select flagid from TextFlagData with(nolock) where recordid=@Organisationid) 

	select	@sqlerror = @@error
	if @sqlerror <> 0
	begin 
	   RAISERROR ('Error : %d inserting text flag rows for Organisation %d ',
		  16, 1, @sqlerror,@organisationid)

	   rollback transaction
	   RETURN 1
	end


/*  insert data into IntegerFlagData     */
	insert into IntegerFlagData (EntityID,FlagID,data)
		select recordid,flagid,convert(int,oldval) from #flagData 
			where DataTableFullName='IntegerFlagData'
			and flagid not in (select flagid from IntegerFlagData with(nolock) where recordid=@Organisationid) 

	select	@sqlerror = @@error
	if @sqlerror <> 0
	begin 
	   RAISERROR ('Error : %d inserting integer flag rows for Organisation %d ',
		  16, 1, @sqlerror,@Organisationid)
	   rollback transaction
	   RETURN 1
	end


/*  insert data into DateFlagData     */
	insert into DateFlagData (EntityID,FlagID,data)
		select recordid,flagid,convert(datetime,oldval) from #flagData 
			where DataTableFullName='DateFlagData'
			and flagid not in (select flagid from DateFlagData with(nolock) where recordid=@Organisationid) 

	select	@sqlerror = @@error
	if @sqlerror <> 0
	begin 
	   RAISERROR ('Error : %d inserting date flag rows for Organisation %d ',
		  16, 1, @sqlerror,@Organisationid)
	   rollback transaction
	   RETURN 1
	end


/*
	WAB don't think that organisationdatasource is used
 insert Organisationdatasource (Organisationid,datasourceid,RemoteDataSourceID)
 select @Organisationid,1,' '

*/


commit transaction 



return 0



