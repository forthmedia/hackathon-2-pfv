/* a stored procedure to set extended properties on an object */
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'setExtendedProperty' and Type = 'P')
BEGIN
	DROP PROCEDURE setExtendedProperty
END

GO

CREATE PROCEDURE [dbo].[setExtendedProperty](@level1Object as varchar(250) = 'table', @level1name as varchar(250), @level2Object as varchar(250), @level2name as varchar(250), @property as varchar(250), @value as varchar(250))
WITH EXECUTE AS OWNER
AS
	declare @extendedProperty varchar(250)
	
	begin
		select @extendedProperty = dbo.getExtendedProperty(@level1Object,@level1name,@level2Object,@level2name,@property)
	    
		/* delete existing if different from incoming - could use sp_updateextendedproperty as well */
		if ((@extendedProperty is not null and @extendedProperty != @value) or @value is null)
		begin	
			EXEC sp_dropextendedproperty 
			@name = @property,
			@level0type = N'Schema', @level0name = 'dbo',
			@level1type = @level1Object,  @level1name = @level1name,
			@level2type = @level2Object,  @level2name = @level2name;
		end
	    
		if ((@extendedProperty is null or @extendedProperty != @value) and @value is not null)
		begin
			EXEC sp_addextendedproperty 
			@name = @property,
			@value = @value,
			@level0type = N'Schema', @level0name = 'dbo',
			@level1type = @level1Object,  @level1name = @level1name,
			@level2type = @level2Object,  @level2name = @level2name;
		end
	end


GO