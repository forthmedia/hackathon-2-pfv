/****** Object:  StoredProcedure [dbo].[DeleteFlaggedPeople]    Script Date: 06/04/2013 17:32:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DeleteFlaggedPeople' and Type = 'P')
	DROP PROCEDURE DeleteFlaggedPeople
	
GO
	
CREATE     procedure [dbo].[DeleteFlaggedPeople]
@TimeLimit int=1 ,   -- number of hours to wait from flagging until deletion can happen
@Test bit = 0 ,		-- when set to 1, no deletions take place
@overRideDatestring char(30) = null  
/* @overRideDatestringthis is passed as a string because of problems getting dates in with cfprocparam and also allows the milliseconds to be retained 
*/
AS


/*

2007/11/19  WAB   incorporated separate procedures for checking for protected flags etc. and for deleting flags
2016/01/14	NJH		added lastUpdatedByPerson to deleteEntity temp table for tidiness sake
*/

Declare @overrideCountCheck datetime
Select @overrideCountCheck=CONVERT(datetime,@overrideDateString) 

-- 


Set nocount on
If @TimeLimit=999 Set @TimeLimit=0
Declare @StartDel datetime
Declare @StartDelWithLimit datetime
Declare @PerToBeDeleted int
Declare @PerDoNotDelete int
Declare @DeleteFlagID int
declare @maxrows int
set @maxrows  = 25
Declare @pwddays int
Select @pwddays=30
Declare @DelGo Varchar(5), @UserSet Varchar(30)

-- Exec("setuser '"+@UserSet+"'")
Select @StartDel=getdate()
Select @timelimit=abs(@timelimit)*-1
select @StartDelWithLimit=DATEADD(hour, @TimeLimit, @StartDel)
select @DeleteFlagID = flagid from flag where flagtextid = 'deleteperson'
-- create a table to keep track of people Not Deleted
Create table #NotDeletable(EntityID int,Reason varchar(50),lastupdated  datetime, extendedInfo varchar(max))


Create table #countCheck (delcount int, lastupdated datetime, lastupdatedby int)
if @overrideCountCheck is null 
  begin 

	insert into #countcheck (delcount,lastupdated,lastupdatedby)
	Select count(*),LastUpdated,LastUpdatedBy 
	  from BooleanFlagData
	Where FlagID=@DeleteFlagID	 
	group by LastUpdated,LastUpdatedBy having count(*) > @maxrows
  end
else
  begin

	insert into #countcheck (delcount,lastupdated,lastupdatedby)
	Select count(*),LastUpdated,LastUpdatedby 
	  from BooleanFlagData
	Where FlagID=@DeleteFlagID 
	AND lastupdated <> @overrideCountCheck
	group by LastUpdated,LastUpdatedBy having count(*) > @maxrows

  end



-- record people excluded due to too many people being deleted at a time
insert into #NotDeletable (entityid, reason,lastupdated)
select entityid, 'Too Many People in Request', b.lastupdated  from booleanflagdata b, #countcheck c
where b.FlagID=@DeleteFlagID and c.LastUpdatedBy = b.LastUpdatedBy and c.LastUpdated = b.LastUpdated

--IDs of Persons to be deleted:(FlagID=92)
Select EntityID as entityID, LastUpdatedBy as DeletedBy, b.lastUpdatedByPerson as deletedByPerson
into #deleteEntityIDTable
From BooleanFlagData b Where FlagID=@DeleteFlagID and LastUpdated<@StartDelWithLimit
 and not exists(select * from #countcheck c where c.LastUpdatedBy = b.LastUpdatedBy 
						and c.LastUpdated = b.LastUpdated)

Select @PerToBeDeleted = @@rowcount

-- record people excluded because marked for deletion too recently
insert into #NotDeletable (entityid, reason)
Select EntityID, 'Marked for deletion within last ' + convert(varchar,abs(@Timelimit)) + ' hours'
From BooleanFlagData b Where FlagID=@DeleteFlagID and LastUpdated>=@StartDelWithLimit



print 'pertobedeleted = ' + convert (varchar,@PerToBeDeleted )


If @PerToBeDeleted = 0 GOTO TheEnd


exec deleteentitycheck @entityTypeID = 0


Delete From #deleteEntityIDTable
Where entityID IN (Select entityid From #notDeletable)





/*** Now Start Deleting the People records... ***/
Select @PerToBeDeleted=count(*) from #deleteEntityIDTable


IF @PerToBeDeleted = 0  or @Test = 1  GOTO TheEnd

--For safety, if the number to be deleted is very large then the delete
-- will not proceed and the DBA will have to intervene:
Begin Tran

print 'deleting'
exec deleteEntityFlags @entityTypeID = 0

-- delete user Links  - NJH 2007/03/16
Delete From entityuserlink
where 
	entityTypeID = 0 
	AND EntityID IN (Select entityID from #deleteEntityIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End

--Person PDS:
Delete
From PersonDataSource
Where PersonID IN (Select entityID from #deleteEntityIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
--Person PDS:
Delete
From rightsgroup
Where PersonID IN (Select entityID from #deleteEntityIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
--Person Records:
Delete
From Person
Where PersonID IN (Select entityID from #deleteEntityIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
--This will ensure that the user who originally set the delete flag
---will be recorded in the modRegister as the user who did the deletion:
Update M
Set M.ActionByCF=P.DeletedBy
From ModRegister as M Join #deleteEntityIDTable as P
On M.RecordID=P.entityID
Where (Action='PD' and ModDate>=@StartDel)

IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
Commit Tran
TheEnd:


-- return query of all people deleted (or which will be deleted in case of a test)
if @test = 0
	select 'Deleted', p.personid, firstname, lastname  from persondel p inner join #deleteEntityIDTable ptd on p.personid = ptd.entityid
else
	select 'Will be deleted', p.personid, firstname, lastname  from person p inner join #deleteEntityIDTable ptd on p.personid = ptd.entityid



-- return query of all people not deleted for some reason
if @test = 0
  Begin
	select reason, extendedinfo, p.personid, firstname, lastname , nd.lastupdated
	from person p inner join #notdeletable nd on p.personid = nd.entityid

	union 

	-- these are any items which were in ToDeletePer but for some reason are still in the person table
	select 'Unknown reason', null, p.personid, firstname, lastname , null
	from person p inner join #deleteEntityIDTable td on p.personid = td.entityid
  end
else
  begin
	select reason, extendedinfo, p.personid, firstname, lastname , nd.lastupdated
	from person p inner join #notdeletable nd on p.personid = nd.entityid
  end


-- return query of any suspicious mass deletes
select PeopleDeleted=delcount,DeletedBy=left(u.name,25),
WhenDeleted=d.LastUpdated 

from #countcheck d inner join usergroup u on u.usergroupid=d.LastUpdatedBy

GO


