/* 
	NJH 2016/06/15	PROD2016-1256 - changed instances of @@identity with SCOPE_IDENTITY();
*/

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'RWCreateTransaction' and Type = 'P')
	DROP PROCEDURE RWCreateTransaction
	
GO
	
CREATE    procedure
	RWCreateTransaction
	@OrganisationID		int,			-- ID of the organisation which owns the account.
	@TxTypeID  		char(2),		-- Transaction Code (e.g. AL = Allocation, CN = Cancellation)
	@CashAmount   		numeric(18,2) = NULL,	-- Monetary Value of the transaction	) Mutually
	@PointsAmount		int = NULL,		-- Points value of the transaction	) exclusive
	@PriceISOCode		char(3) = NULL,		-- Currency Code for this transaction
	@CurrencyPerPoint	numeric(18,6) = NULL,	-- Conversion factor used by preceding balance checking, if appropriate
							-- (ensures consistency between validation of balance and debiting of account)
	@OrderID		int = NULL,			-- ID of the Purchase Order to which the transaction relates, if appropriate.
	@PersonID		int = NULL,			-- ID of user
	@ReasonID		int = NULL,		-- Reason code (pre-existing reason)
	@ReasonText		nvarchar(500) = NULL	-- Reason text (new reason)

as
set nocount on
/***************************************************************************************
** Name   : RWCreateTransaction
** Created: Dec/2000
** By     : JVH
**
** Purpose: 
** Create transactions against a reward points account, and perform necessary updates.
**
** Transaction behaviour is dictated by parameters stored in RWTransactionType.
**
** 
** Arguments:
**      See above
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who	Comments
** -------  -----------  ---	----------------------------------------------------------
** 1.00	    Dec/2000     JVH	Original
** 1.01     Jan/2001     JVH	Mods to use new transaction type control parm 'NeedsOrderID'.
**									Also removed references to 'CashOrPoints', which is redundant
**								Now accommodates 'RA' (Refund of allocated points) transaction.
** 			2011-03-14   NYB	LHID5528
**
****************************************************************************************/

/*===================================================================================*/
/* Initialisations                                                                   */
/*===================================================================================*/
Declare	@ErrCode int,	-- Error return code
	@CreditSwitch bit,	-- Switch to activate Credit side of a transaction
	@DebitSwitch bit,	-- Switch to activate debit side of a transaction
	@OrigTxID int,		-- Original Transaction ID (e.g for a Cancellation, the Tx ID
				-- of the original Allocation)
	@NewTxID int,		-- Transaction ID of new transaction being created
	@Outstanding int,	-- Points amount of this transaction yet to be debited from 
				-- the partner's account
	@PtsAccID int,		-- Holds Account ID for the 'pot' being debited/credited
	@PointsPool int,	-- Holds the number of points available to this transaction
				-- for the 'pot' being debited/credited
	@SplitAmount int,	-- Amount for a transaction to be debited from/credited to a
				-- specific 'pot'
	@PostHoc bit,		-- Flag to indicate whether the transaction is a 'follow-up' type
				-- (i.e. Spend of Allocated points, Refund or Cancellation). THese
				-- all make adjustments based on an existing OrderID
	@NeedsOrderID bit,	-- Flag to indicate that OrderID must be specified
--	@CashOrPoints char(1),	-- Flag to indicate whether the transaction deals in cash or directly in points
	@AccountID int		-- AccountID to be debited/credited

SET @ErrCode = 0

/*===================================================================================*/
/* Validations                                                                       */
/*===================================================================================*/
-- Exclude Accrual and Expiry transactions fromn this process
IF @TxTypeID in('AC','EX')
 BEGIN
   RAISERROR ('Error : Transaction type %s cannot be handled by this procedure',
	      16, 1, @TxTypeID)

   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Check the Organisation ID is supplied and exists
IF @OrganisationID is NULL
 BEGIN
   RAISERROR ('Error : OrganisationID is required but was not supplied',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

IF NOT EXISTS(SELECT 1 FROM RWCompanyAccount WHERE OrganisationID = @OrganisationID)
 BEGIN
   RAISERROR ('Error : Account cannot be found for organisation %d',
	      16, 1, @OrganisationID)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Check Transaction Type is provided
IF @TxTypeID is NULL
 BEGIN
   RAISERROR ('Error : Transaction type is required but was not supplied',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Check the specified Transaction Type is valid
IF NOT EXISTS(SELECT 1 FROM RWTransactionType WHERE RWTransactionTypeID = @TxTypeID)
 BEGIN
   RAISERROR ('Error : Transaction type %s is not valid',
	      16, 1, @TxTypeID)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Get control variables for the transaction type
SELECT @CreditSwitch = CreditSwitch,
	@DebitSwitch = DebitSwitch,
	@PostHoc = PostHoc,
	@NeedsOrderID = NeedsOrderID
FROM	RWTransactionType
WHERE   RWTransactionTypeID = @TxTypeID


/*-----------------------------------------------------------------------------------*/
/* Check points/cash amount and associated parms                                     */
/*-----------------------------------------------------------------------------------*/
-- Post-Hoc transactions don't need a points or cash amount. Others do.
IF @PostHoc = 0
 BEGIN
   IF @CashAmount is NULL AND @PointsAmount is NULL
    BEGIN
       RAISERROR ('Error : Cash or Points value required for transaction type %s',
	          16, 1, @TxTypeID)
       SET @ErrCode = -1
       GOTO ERRORTRAP
    END

   -- Check that only a points OR a cash value has been specified
   IF @CashAmount is not NULL AND @PointsAmount is not NULL
    BEGIN
      RAISERROR ('Error : Only ONE of Cash Value or Points Value should be specified',
	         16, 1)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

   -- Cash transactions must have the currency code. Conversion rate is optional as can be retrieved from
   -- RWPointsConversion
   IF @CashAmount is NOT NULL 
    BEGIN
      IF @PriceISOCode is  NULL
       BEGIN
          RAISERROR ('Error : Cash transactions must specify currency',
   	          16, 1)
          SET @ErrCode = -1
          GOTO ERRORTRAP
       END

      -- Check that the specified currency code exists
      IF NOT EXISTS(SELECT 1 FROM RWPointsConversion WHERE PriceISOCode = @PriceISOCode)
       BEGIN
          RAISERROR ('Error : Currency Code %s is not valid',
   	         16, 1, @PriceISOCode)
          SET @ErrCode = -1
          GOTO ERRORTRAP
       END
    END
 END


/*-----------------------------------------------------------------------------------*/
-- Check OrderID supplied and correct if required
/*-----------------------------------------------------------------------------------*/
IF @NeedsOrderID = 1
 BEGIN
   IF @OrderID is NULL 
    BEGIN
      RAISERROR ('Error : %s transactions require an Order ID, which was not supplied',
	        16, 1, @TxTypeID)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

   IF NOT EXISTS(SELECT 1 FROM RWTransaction WHERE OrderID = @OrderID
						AND RWTransactionTypeID = 'AL')
   AND @PostHoc = 1
    BEGIN
      RAISERROR ('Error : OrderID %d cannot be found',
	         16, 1, @OrderID)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END
 END

/*-----------------------------------------------------------------------------------*/
-- Check that only reason id or reason text is specified
/*-----------------------------------------------------------------------------------*/
IF @ReasonID is not NULL and @ReasonText is not NULL
 BEGIN
   RAISERROR ('Error : Only ONE of ReasonID or Reason Text should be specified',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

/*===================================================================================*/
/* Set working variables                                                             */
/*===================================================================================*/
-- Get the AccountID from the RWCompanyAccount table
SELECT @AccountID = AccountID FROM RWCompanyAccount WHERE OrganisationID = @OrganisationID

-- Get the corresponding 'original' (AL) transaction id if one exists -
-- This is used by post-hoc transactions to create corresponding entries in
-- RWTransactionSplit and by RA transactions to identify 'pots' for refunding
IF @OrderID is not NULL
   SELECT @OrigTxID = RWTransactionID 
   FROM	  RWTRansaction 
   WHERE  OrderID = @OrderID
   AND	  AccountID = @AccountID
   AND	  RWTransactionTypeID = 'AL'

/*-----------------------------------------------------------------------------------*/
-- Get Points Amount
/*-----------------------------------------------------------------------------------*/
-- Convert the cash amount (if specified) to its points equivalent for 'new' transactions
-- (Round up fractional points)
IF @PostHoc = 0
 BEGIN
   IF @CashAmount is NOT NULL
    BEGIN
      IF @CurrencyPerPoint is NULL
        SELECT @CurrencyPerPoint = CurrencyPerPoint FROM RWPointsConversion WHERE PriceISOCode = @PriceISOCode

      SET @PointsAmount = case 
			when (@CashAmount / @CurrencyPerPoint) - convert(int,(@CashAmount / @CurrencyPerPoint)) > 0 
				then convert(int, (@CashAmount / @CurrencyPerPoint)) + 1
		    	else convert(int,(@CashAmount / @CurrencyPerPoint))
		       end
    END
 END
--** 'Post-hoc' transactions should look in RWTransaction for their 'original' transactions 
--** to get the points amount
ELSE -- i.e. SA, RF or CN
   SELECT @PointsAmount = DebitAmount
   FROM RWTransaction RWtr
   WHERE RWtr.RWTransactionID = @OrigTxID

/*-----------------------------------------------------------------------------------*/
-- If a new reason is supplied, insert into RWTransactionReason and get the new ID
/*-----------------------------------------------------------------------------------*/
IF @ReasonID is NULL and @ReasonText is NOT NULL
 AND NOT EXISTS(SELECT 1 FROM RWTransactionReason WHERE ReasonText = @REasonText)
 BEGIN
   INSERT into RWTransactionReason (reasonText,createdBy,created,updatedBy,updated) VALUES(@ReasonText, @PersonID, getdate(), @PersonID, getdate())

   IF @@Error <> 0
    BEGIN
      RAISERROR ('Error : Unable to save new reason record',
	         16, 1)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

   SET @ReasonID = SCOPE_IDENTITY()

 END

/*-----------------------------------------------------------------------------------*/
-- Set Person ID if NULL
/*-----------------------------------------------------------------------------------*/
IF @PersonID is NULL SET @PersonID = user_id()

/*===================================================================================*/
/* Create database entries                                                           */
/*===================================================================================*/
BEGIN TRANSACTION

/*-----------------------------------------------------------------------------------*/
/* RWTransaction                                                                     */
/*-----------------------------------------------------------------------------------*/

--*** ALL TRANSACTIONS MUST CREATE AN ENTRY IN RWTRansaction ***--
INSERT RWTransaction(RWTransactionTypeID, 
			AccountID, 
			OrderID, 
			CreditAmount, 
			DebitAmount, 
			PriceISOCode, 
			CurrencyPerPoint, 
			RWTransactionReasonID,
			Created, 
			CreatedBy,
			personid)
VALUES( @TxTypeID,
	  @AccountID,
	  @OrderID,
	  @PointsAmount * @CreditSwitch ,
	  @PointsAmount * @DebitSwitch ,
	  @PriceISOCode,
	  @CurrencyPerPoint,
	  @ReasonID,
	  GetDate(),
	  @PersonID,
	  @PersonID )

--** Get the TransactionID just created **--
SET @NewTxID = SCOPE_IDENTITY()

/*-----------------------------------------------------------------------------------*/
/* RWTransactionSplit                                                                */
/*-----------------------------------------------------------------------------------*/
--** 'Post-hoc' transactions need to create entries in RWTransactionSplit matching their **--
--** originals **--
IF @PostHoc = 1 -- i.e. SA, RF or CN
 BEGIN
   INSERT RWTransactionSplit
   SELECT @NewTxID, RWts.PtsAccID, RWts.DebitAmount * @CreditSwitch, RWts.DebitAmount * @DebitSwitch--,@PersonID,@PersonID,GETDATE()
   FROM   RWTransactionSplit RWts
   WHERE  RWts.RWTransactionID = @OrigTxID
 END

--** 'New' transactions need to create entries in RWTransactionSplit indicating where the points are **--
--** spent from or refunded to                                                                       **--
-- If this is a 'points spending' transaction, create a cursor to step through the various 
-- 'pots' of points, oldest first, until enough points have been debited to pay for the transaction
--
-- If this is a 'Refund of Allocated Points' ('RA') transaction, step through the pots youngest first to 
-- ensure points are refunded to non-expired pots as far as possible

ELSE
 BEGIN
   -- Create 'oldest->youngest' cursor for transactions which spend points
   IF @TxTypeID <> 'RA'
      DECLARE Points_csr CURSOR
      FOR
      SELECT PtsAccID,
	    Balance
      FROM	 RWPointsAccrued
      WHERE  Balance > 0
      AND 	 AccountID = @AccountID
      AND   ActivationDate <= GetDate()
      ORDER BY ExpiryDate

   -- Create 'youngest->oldest' cursor for RA transactions which (partially) refund points
   ELSE
      DECLARE Points_csr CURSOR
      FOR
      SELECT ts.PtsAccID,
	     ts.DebitAmount
      FROM   RWTransactionSplit ts
      JOIN   RWPointsAccrued pa	on ts.PtsAccID = pa.PtsAccID
      WHERE  ts.RWTransactionID = @OrigTxID
      AND    pa.AccountID = @AccountID
      ORDER BY ExpiryDate DESC

   OPEN Points_Csr

   -- Loop through the cursor and create entries in RWTransactionSplit, until sufficient points
   -- have been debited or refunded to cover the transaction
   SET @Outstanding = @PointsAmount

   WHILE @Outstanding > 0
    BEGIN
      FETCH NEXT 
      FROM Points_csr 
      INTO @PtsAccID,
	   @PointsPool

      IF @@FETCH_STATUS = 0 -- We have a row
       BEGIN
	 SET @SplitAmount = CASE
				WHEN @PointsPool >= @Outstanding THEN @Outstanding
		 		WHEN @PointsPool <  @Outstanding THEN @PointsPool
			    END

         INSERT RWTransactionSplit
         SELECT @NewTxID, @PtsAccID, @SplitAmount * RWtr.CreditSwitch, @SplitAmount * RWtr.DebitSwitch--,@PersonID,@PersonID,GETDATE()
         FROM   RWTransactionType RWtr
         WHERE  RWtr.RWTransactionTypeID = @TxTypeID

  	 SET @Outstanding = @Outstanding - @SplitAmount

       END
      ELSE IF @@FETCH_STATUS = -1 BREAK  -- We have reached the end of the cursor
--      ELSE 
--       BEGIN
         -- Do nothing - the row is no longer eligible for the cursor
--       END

    END   

   CLOSE Points_Csr
   DEALLOCATE Points_Csr

   -- After the cursor loop, check whether the amount outstanding has reached zero
   IF @Outstanding > 0 
    BEGIN
      RAISERROR ('Error : Outstanding amount of %d points cannot be accommodated',
	         16, 1, @Outstanding)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

 END


/*-----------------------------------------------------------------------------------*/
/* RWPointsAccrued                                                                   */
/*-----------------------------------------------------------------------------------*/
--** All transactions must update the balance in RWPointsAccrued, using the details in RWTransactionSplit **--
--** If the 'pot' in RWPointsAccrued has expired, don't adjust the balance, which will have been set to zero **--
UPDATE RWpa
SET	RWpa.Balance = RWpa.Balance + RWts.CreditAmount - RWts.DebitAmount
FROM	RWPointsAccrued RWpa
JOIN	RWTransactionSplit RWts
ON	RWts.PtsAccID = RWpa.PtsAccID
WHERE	RWts.RWTransactionID = @NewTxID
AND	RWpa.ExpiredDate is NULL

/*-----------------------------------------------------------------------------------*/
/* Finally update the running balance in RWTransaction                               */
/*-----------------------------------------------------------------------------------*/
UPDATE 	RWTr
SET	RWtr.SnapshotBalance = (SELECT isnull(SUM(isnull(RWpa.Balance,0)),0)
			   FROM	RWPointsAccrued RWpa
			   WHERE RWtr.AccountID = RWpa.AccountID)
FROM 	RWTransaction RWtr
WHERE  RWtr.RWTransactionID = @NewTxID

COMMIT TRANSACTION

Return @NewTxID 


/*===================================================================================*/
/* Common exit point                                                                 */
/*===================================================================================*/
EXITPOINT:

Return @ErrCode

/*===================================================================================*/
/* Handle any errors                                                                 */
/*===================================================================================*/
ERRORTRAP:

IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION
GOTO EXITPOINT

