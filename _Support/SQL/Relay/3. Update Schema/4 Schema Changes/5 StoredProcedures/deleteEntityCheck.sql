
/****** Object:  StoredProcedure [dbo].[DeleteEntityCheck]    Script Date: 03/29/2012 14:36:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteEntityCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[DeleteEntityCheck]
GO

/****** Object:  StoredProcedure [dbo].[DeleteEntityCheck]    Script Date: 03/29/2012 14:36:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   procedure [dbo].[DeleteEntityCheck]
@EntityTypeID int ,   
@EntityID varchar(10)  = null

AS
/*
WAB trying to have a generic sp for checking for protected flags ets, rather than duplicating the code all over the place
also to allow the app to check whether an entity can be deleted before actually trying to delete it.

need to pass in either @entityID or create a temporary table called #deleteentityidtable
if you want to be able to access the result set then create a temporary table called 	Create table #NotDeletable(EntityID int,Reason varchar(50),,extendedInfo varchar(max))

2008/04/15 Added rudimentary check for rewards accounts - need to perhaps deal with balances
2009/03/16 NYB wasn�t working for linked flags � bug - fixed
2009/05/11 WAB reversed out NYB change
2012/03/29 NJH/RNB CASE 427080 - check foreign key constraints, and throw put affected records into the not deletable result.
2015-11-20	WAB Change names of cursors to unique names to aid debugging
*/

Declare @sql varchar(400)
Declare @flagid int
Declare @returnRecordSet bit     -- if #NotDeletable table is passed in the assume that results are returned in it, otherwise return the recordset
set @returnRecordSet = 0


if @entityID is not null
  begin
	select @entityid as entityid into #deleteentityIDTable
  end


IF OBJECT_ID('tempdb..#NotDeletable') IS NULL
	BEGIN 
		Create table #NotDeletable(EntityID int,Reason varchar(50),extendedInfo varchar(max))
		set @returnRecordSet = 1
	END


If @EntityTypeID = 0 
   BEGIN
	insert into 
	#NotDeletable (entityID, reason)
	Select p.PersonID, 'isValidUser'
	
	From #deleteEntityIDTable as e 
		inner Join Person as P On e.entityID=P.PersonID
		inner join usergroup u on u.PersonID=P.PersonID
	Where ISNULL(P.Username, '')<>''
	and ISNULL(P.Password, '')<>''
	and P.LoginExpires>=getdate()
	
	-- record people excluded due to owning an opportunity
	insert into #NotDeletable (entityid, reason)
	select p.personID, 'hasOpportunity'
	From #deleteEntityIDTable as e 
	inner join person p on e.entityID = p.personID
	inner join opportunity o on p.personID = o.partnerSalesPersonID
	
   END

/* 
   Exclude Organisations which have rewards accounts 
   should perhaps just be rewards accounts with balances
*/

If @EntityTypeID = 2
   BEGIN
	insert into 
	#NotDeletable (entityID, reason)
	Select distinct entityID, 'hasRewardsAccount'
	
	From #deleteEntityIDTable as e 
		inner Join rwcompanyaccount as rw On e.entityID=rw.organisationid and rw.accountDeleted <> 1

	-- NJH 2008/06/04
	insert into 
	#NotDeletable (entityID, reason)
	Select distinct entityID, 'hasFundAccount'
	
	From #deleteEntityIDTable as e 
		inner Join fundCompanyAccount as fca On e.entityID=fca.organisationid and fca.accountClosed <> 1

   END



-- Exclude Entity records that happen to have any Protected flags:
DECLARE ProtectedFlags_Cursor CURSOR LOCAL FOR
Select 'Select fd.Entityid , ''hasProtectedFlag'' , convert(varchar,flagid) from '+ft.datatable+'flagdata fd inner join #deleteEntityIDTable e on fd.entityid = e.entityid '+' where FlagID='
+rtrim(convert(varchar(5),F.FlagID)), f.flagid
From (Flag as F Inner Join FlagGroup as FG
On F.FlagGroupID=FG.FlagGroupID)
Inner Join FlagType as FT
On FG.FlagTypeID=FT.FlagTypeID
Where FG.EntityTypeID=@entityTypeID and F.Protected=1



OPEN ProtectedFlags_Cursor
-- Perform the first fetch.
FETCH NEXT FROM ProtectedFlags_Cursor into @sql, @flagid
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
-- This is executed as long as the previous fetch succeeds.
	print 'Testing for Protected Flag ' + convert(varchar,@flagid)
	--print @sql

	Insert into #NotDeletable (entityid, reason, extendedInfo) 
	Exec(@sql)

	FETCH NEXT FROM ProtectedFlags_Cursor into @sql, @flagid
END
  
CLOSE ProtectedFlags_Cursor
DEALLOCATE ProtectedFlags_Cursor



-- Exclude Entity records linked to flags (linkToEntityTypeID not null) where protect Linked Entities:
DECLARE ProtectedLinkedEntityFlags_cursor CURSOR LOCAL FOR
Select 'Select distinct fd.data , ''hasProtectedLinkedEntityFlag'' , convert(varchar,flagid) from '+ft.datatable+'flagdata fd inner join #deleteEntityIDTable e on fd.data = e.entityid '+' where FlagID='
+rtrim(convert(varchar(5),F.FlagID)), f.flagid
From (Flag as F Inner Join FlagGroup as FG
On F.FlagGroupID=FG.FlagGroupID)
Inner Join FlagType as FT
On FG.FlagTypeID=FT.FlagTypeID
Where F.linksToEntityTypeID=@entityTypeID 
and (F.formattingParameters like '%protectedLinkedEntity=1%' 
	-- WAB removed 2009/05/11, No Protected and protectedLinkedEntity are different 
	--	or F.Protected=1  -- NYB added 2009/03/18
)



OPEN ProtectedLinkedEntityFlags_cursor
-- Perform the first fetch.
FETCH NEXT FROM ProtectedLinkedEntityFlags_cursor into @sql, @flagid
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
-- This is executed as long as the previous fetch succeeds.
	print 'Testing for Protected Flag (linked entity)' + convert(varchar,@flagid)
	--print @sql
	-- select * from #notdeletable

	Insert into #NotDeletable (entityid, reason, extendedInfo) 
	Exec(@sql)

	FETCH NEXT FROM ProtectedLinkedEntityFlags_cursor into @sql, @flagid
END
  
CLOSE ProtectedLinkedEntityFlags_cursor
DEALLOCATE ProtectedLinkedEntityFlags_cursor

DECLARE
@Referencing_Table	nvarchar(100),
@Referencing_Column	nvarchar(100),
@Referenced_Table	nvarchar(100),
@Referenced_Column	nvarchar(100)

/* NJH/RNB 2012/03/29 Looking for foreign key constraints that will cause the delete to fall over. We exclude serveral tables
	that may have foreign keys as we handle in them in the delete stored procedures */
DECLARE ForeignKeyConstraints_cursor CURSOR LOCAL FOR
	SELECT
		rcu.TABLE_NAME 'Referencing_Table',
		rcu.COLUMN_NAME 'Referencing_Column',
		rcu1.TABLE_NAME 'Referenced_Table',
		rcu1.COLUMN_NAME 'Referenced_Column',
		'SELECT DISTINCT e.entityid, ''hasTableConstraint'' ,''' + rcu.COLUMN_NAME + '/' + rcu.TABLE_NAME +
		''' FROM '+ rcu.TABLE_NAME +' M1 INNER JOIN ' + rcu1.TABLE_NAME + ' J1 ON J1.' + rcu1.COLUMN_NAME + ' = M1.' + rcu.COLUMN_NAME +
		--' AND J1.' + SCH.uniquekey + ' = ' + @EntityID
        ' INNER JOIN #deleteEntityIDTable e ON e.entityid = J1.' + SCH.uniquekey

	FROM
		INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE rcu ON rc.CONSTRAINT_CATALOG = rcu.CONSTRAINT_CATALOG 
																		AND rc.CONSTRAINT_NAME = rcu.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE rcu1 ON rc.UNIQUE_CONSTRAINT_CATALOG = rcu1.CONSTRAINT_CATALOG 
																		AND rc.UNIQUE_CONSTRAINT_NAME = rcu1.CONSTRAINT_NAME
		INNER JOIN schemaTable sch ON sch.entityName = rcu1.TABLE_NAME
	WHERE
		sch.entitytypeid = @EntityTypeID AND
		--rcu1.TABLE_NAME = @UseEntityTable AND
		rc.delete_rule = 'NO ACTION' AND
		rcu.TABLE_NAME NOT IN ('rwcompanyaccount',
								'fundCompanyAccount',
								'PersonDataSource',
								'LocationDataSource',
								'organisationDataSource',
								'rightsgroup',
								'userGroup',
								'Person',
								'Location',
								'Organisation'
								)

OPEN ForeignKeyConstraints_cursor
-- Perform the first fetch.
FETCH NEXT FROM ForeignKeyConstraints_cursor into @Referencing_Table, @Referencing_Column, @Referenced_Table, @Referenced_Column, @sql
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
	
	print 'Testing for Table Constraint (linked entity) Referencing Table ' + @Referencing_Table +
			' Column ' + @Referencing_Column + ' with Referenced Table ' + @Referenced_Table +
			' Column ' + @Referenced_Column
	--print @sql 
	--print 'Referencing_Table' + @Referencing_Table
	--print '' + @Referencing_Column
	--print '' + @Referenced_Table
	--print '' + @Referenced_Column

	INSERT INTO #NotDeletable (entityid, reason, extendedInfo)
	EXEC(@sql)

FETCH NEXT FROM ForeignKeyConstraints_cursor into @Referencing_Table, @Referencing_Column, @Referenced_Table, @Referenced_Column, @sql
END
  
CLOSE ForeignKeyConstraints_cursor
DEALLOCATE ForeignKeyConstraints_cursor

if @returnRecordSet =1 
	select * from #NotDeletable
GO