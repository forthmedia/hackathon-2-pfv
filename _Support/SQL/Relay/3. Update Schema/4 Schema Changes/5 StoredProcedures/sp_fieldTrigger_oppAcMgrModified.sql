if exists (select 1 from sysobjects where name='sp_fieldTrigger_oppAcMgrModified' and xType='P')
drop procedure sp_fieldTrigger_oppAcMgrModified

/****** Object:  StoredProcedure [dbo].[sp_fieldTrigger_oppAcMgrModified]    Script Date: 09/25/2014 10:00:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppAcMgrModified] 
	@modEntityid int, 
    @modaction varchar(10),
    @triggerInitiatedBy int, 
    @RecordUpdatesAgainstID int,
    @triggerInitiatedByPersonID int = null
AS
Set nocount on

/*if a user is allocated as the vendorAccountManagerPersonID - opp field -  
they will be flagged with [person boolean flag] as defined in the settings  */
declare @accManagerFlagID int
select @accManagerFlagID = flagID from flag where flagTextID = isNull(dbo.getSettingValue('leadManager.vendorAcMngrFlagTextIDs'),'isAccountManager')

IF (@modaction in ('oppA','oppM') and isNull(@accManagerFlagID,0) != 0)
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated,lastUpdatedByPerson) 
	select vendorSalesManagerPersonID,@accManagerFlagID,@RecordUpdatesAgainstID,getDate(),@RecordUpdatesAgainstID,getDate(),@triggerInitiatedByPersonID
	from #modifiedEntityIDs m
		inner join opportunity opp on opp.opportunityID = m.entityID
		left join booleanFlagData bfd on bfd.entityID = opp.vendorSalesManagerPersonID and bfd.flagid=@accManagerFlagID
	where
		bfd.entityID is null
END

Set nocount off