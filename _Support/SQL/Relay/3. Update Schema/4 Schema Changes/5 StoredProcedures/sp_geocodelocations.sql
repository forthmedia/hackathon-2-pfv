/* change sp_GeoCodeLocations stored procedure */
ALTER PROCEDURE [dbo].[sp_GeoCodeLocations] AS
DECLARE @nCount int 
DECLARE @nLoop1 int
DECLARE @nLoop2 int
DECLARE @nLocFieldName varchar(50)
DECLARE @nGeoFieldName varchar(50)
DECLARE @SQLstring nvarchar(1000)
/*
	See if any Locations need doing or are wonky
*/
SELECT 
	@nCount = count(LocationID) 
FROM
	Location
WHERE
	Latitude IS NULL
	AND
	Longitude IS NULL
if (SELECT @nCount) = 0
	begin
	PRINT 'There are no Locations where both Latitude and Longitude are required'
	RETURN
	end
SELECT 
	@nCount = count(LocationID) 
FROM
	Location
WHERE
	Latitude IS NULL
	OR
	Longitude IS NULL
if (SELECT @nCount) = 0
	begin
	PRINT 'There are ' +cast(@nCount AS varchar (5)) + ' Locations which are missing a Latitude or Longitude'
	PRINT 'Their LocationIDs are...'
	SELECT 
		LocationID
	FROM
		Location
	WHERE
		Latitude IS NULL
		OR
		Longitude IS NULL
	RETURN
	end
/*
	Try Postalcode Precise Match
*/
SELECT 
	@nCount = count(DISTINCT LocationID) 
FROM
	Geodata g INNER JOIN Location l
	ON g.postalcode = l.postalcode
WHERE 
	g.Postalcode IS NOT NULL
	AND 
	l.postalcode IS NOT NULL
	AND
	l.Latitude IS NULL
	AND
	l.Longitude IS NULL
if (SELECT @nCount) > 0
	begin
	UPDATE
		l
	SET
		l.Latitude = g.DecimalLatitude,
		l.Longitude = g.DecimalLongitude
	FROM
		Geodata g INNER JOIN Location l
		ON g.postalcode = l.postalcode
	WHERE
	l.Latitude IS NULL
	AND
	l.Longitude IS NULL
	PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to direct post code matches'
	end
/*
	Try Postalcode First Part Match English Only
*/
SELECT 
	@nCount = count(DISTINCT LocationID) 
FROM
	Geodata g, Location l
WHERE 
	g.Postalcode IS NOT NULL
	AND 
	l.postalcode IS NOT NULL
	AND 
	Substring(l.Postalcode,1,5) = g.postalcode
	AND
	g.isocode = 'ENG'
	AND
	l.Latitude IS NULL
	AND
	l.Longitude IS NULL
if (SELECT @nCount) > 0
	begin
	UPDATE
		l
	SET
		l.Latitude = g.DecimalLatitude,
		l.Longitude = g.DecimalLongitude
	FROM
		Geodata g,  Location l
	WHERE
		g.Postalcode IS NOT NULL
		AND 
		l.postalcode IS NOT NULL
		AND 
		Substring(l.Postalcode,1,5) = g.postalcode
		AND
		g.isocode = 'ENG'
		AND
		l.Latitude IS NULL
		AND
		l.Longitude IS NULL
	PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to UK first half post code matches'
	end
/*
	Try Address3 and Address4  Precise Match
*/
SELECT 
	@nCount = count(DISTINCT LocationID) 
FROM
	location l INNER JOIN
    		Country c INNER JOIN
    			GeoCountry gc INNER JOIN
    			GeoData g ON g.ISOCode = gc.ISOCode3 
		ON c.ISOCode = gc.ISOCode2 
	ON l.CountryID = c.CountryID 
		AND 
    		l.Address3 = g.Place 
		AND 
		l.Address4 = g.Province
WHERE 
	l.address3 IS NOT NULL
	AND
	l.address4 IS NOT NULL
	AND
	l.Latitude IS NULL
	AND
	l.Longitude IS NULL
if (SELECT @nCount) > 0
	begin
	UPDATE
		l
	SET
		l.Latitude = g.DecimalLatitude,
		l.Longitude = g.DecimalLongitude
	FROM
		location l INNER JOIN
    			Country c INNER JOIN
    				GeoCountry gc INNER JOIN
    				GeoData g ON g.ISOCode = gc.ISOCode3 
			ON c.ISOCode = gc.ISOCode2 
	ON l.CountryID = c.CountryID 
		AND 
    		l.Address3 = g.Place 
		AND 
		l.Address4 = g.Province
	WHERE 
		l.address3 IS NOT NULL
		AND
		l.address4 IS NOT NULL
		AND
		l.Latitude IS NULL
		AND
		l.Longitude IS NULL
	PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to ad3 and ad4 direct matches'
	end
/*
	Try country Address4 and Address5  Precise Match
*/
SELECT 
	@nCount = count(DISTINCT LocationID) 
FROM
	location l INNER JOIN
    		Country c INNER JOIN
    			GeoCountry gc INNER JOIN
    			GeoData g ON g.ISOCode = gc.ISOCode3 
		ON c.ISOCode = gc.ISOCode2 
	ON l.CountryID = c.CountryID 
		AND 
    		l.Address4 = g.Place 
		AND 
		l.Address5 = g.Province
WHERE 
	l.address4 IS NOT NULL
	AND
	l.address5 IS NOT NULL
	AND
	l.Latitude IS NULL
	AND
	l.Longitude IS NULL
if (SELECT @nCount) > 0
	begin
	UPDATE
		l
	SET
		l.Latitude = g.DecimalLatitude,
		l.Longitude = g.DecimalLongitude
	FROM
		location l INNER JOIN
    			Country c INNER JOIN
    				GeoCountry gc INNER JOIN
    				GeoData g ON g.ISOCode = gc.ISOCode3 
			ON c.ISOCode = gc.ISOCode2 
		ON l.CountryID = c.CountryID 
			AND 
    			l.Address4 = g.Place 
			AND 
			l.Address5 = g.Province
	WHERE 
		l.address4 IS NOT NULL
		AND
		l.address5 IS NOT NULL
		AND
		l.Latitude IS NULL
		AND
		l.Longitude IS NULL
	PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to ad3 and ad4 direct matches'
	end
/*
	Try Single field match
	This runs a cyle through Address5,4,3,2 against first place, then provence
*/
SET @nLoop1 = 1
WHILE @nLoop1 <= (SELECT count(ID) FROM GeoAddressFields WHERE TableName ='geodata')
	begin
	SET @nLoop2 = 1
	WHILE @nLoop2 <= (SELECT count(ID) FROM GeoAddressFields WHERE TableName ='location')
		begin
		SELECT @nLocFieldName = (SELECT FieldName FROM GeoAddressFields WHERE ID = @nLoop2)
		SELECT @nGeoFieldName = (SELECT FieldName FROM GeoAddressFields WHERE ID = (4 + @nLoop1))
		Print @nLocFieldName
		Print @nGeoFieldName
		
		SET @SQLstring =('
		SELECT 
			DISTINCT LocationID as p
		FROM
			location l INNER JOIN
		    		Country c INNER JOIN
		    			GeoCountry gc INNER JOIN GeoData g 
					ON g.ISOCode = gc.ISOCode3 
				ON c.ISOCode = gc.ISOCode2 
			ON l.CountryID = c.CountryID 
			AND 
    			l.' + @nLocFieldName +' = g.' + @nGeoFieldName + 
		' WHERE 
			l.' + @nLocFieldName +' IS NOT NULL
			AND
			l.Latitude IS NULL
			AND
			l.Longitude IS NULL
		')
		Print @SQLstring
		EXEC (@SQLstring)
		SET @nCount = @@rowcount
		if (SELECT @nCount) > 0
			begin
			SET @SQLstring =
			('
			UPDATE
				l
			SET
				l.Latitude = g.DecimalLatitude,
				l.Longitude = g.DecimalLongitude
			FROM
				location l INNER JOIN
			    		Country c INNER JOIN
			    			GeoCountry gc INNER JOIN GeoData g 
						ON g.ISOCode = gc.ISOCode3 
					ON c.ISOCode = gc.ISOCode2 
				ON l.CountryID = c.CountryID 
				AND 
    				l.' + @nLocFieldName +' = g.' + @nGeoFieldName + 
			' WHERE 
				l.' + @nLocFieldName +' IS NOT NULL
				AND
				l.Latitude IS NULL
				AND
				l.Longitude IS NULL
			')
			EXEC (@SQLstring)
			PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to ' + @nLocFieldName +' and ' + @nGeoFieldName +' direct matches'
			end
		SET @nLoop2 = @nLoop2 + 1
		end  ---End of Loop 2
	SET @nLoop1 = @nLoop1 + 1
end --End of Loop 1
/*
	Try Single field match
	This runs a cyle through Address5,4,3,2 against first place, then provence with LIKE
*/
SET @nLoop1 = 1
WHILE @nLoop1 <= (SELECT count(ID) FROM GeoAddressFields WHERE TableName ='geodata')
	begin
	SET @nLoop2 = 1
	WHILE @nLoop2 <= (SELECT count(ID) FROM GeoAddressFields WHERE TableName ='Location')
		begin
		SELECT @nLocFieldName = (SELECT FieldName FROM GeoAddressFields WHERE ID = @nLoop2)
		SELECT @nGeoFieldName = (SELECT FieldName FROM GeoAddressFields WHERE ID = (4 + @nLoop1))
		Print @nLocFieldName
		Print @nGeoFieldName
		
		SET @SQLstring =('
		SELECT 
			DISTINCT LocationID as p
		FROM
			location l INNER JOIN
		    		Country c INNER JOIN
		    			GeoCountry gc INNER JOIN GeoData g 
					ON g.ISOCode = gc.ISOCode3 
				ON c.ISOCode = gc.ISOCode2 
			ON l.CountryID = c.CountryID 
			AND 
    			l.' + @nLocFieldName +' LIKE g.' + @nGeoFieldName + 
		' WHERE 
			l.' + @nLocFieldName +' IS NOT NULL
			AND
			l.Latitude IS NULL
			AND
			l.Longitude IS NULL
		')
		Print @SQLstring
		EXEC (@SQLstring)
		SET @nCount = @@rowcount
		if (SELECT @nCount) > 0
			begin
			SET @SQLstring =
			('
			UPDATE
				l
			SET
				l.Latitude = g.DecimalLatitude,
				l.Longitude = g.DecimalLongitude
			FROM
				location l INNER JOIN
			    		Country c INNER JOIN
			    			GeoCountry gc INNER JOIN GeoData g 
						ON g.ISOCode = gc.ISOCode3 
					ON c.ISOCode = gc.ISOCode2 
				ON l.CountryID = c.CountryID 
				AND 
    				l.' + @nLocFieldName +' LIKE g.' + @nGeoFieldName + 
			' WHERE 
				l.' + @nLocFieldName +' IS NOT NULL
				AND
				l.Latitude IS NULL
				AND
				l.Longitude IS NULL
			')
			EXEC (@SQLstring)
			PRINT cast(@nCount AS varchar (5)) + ' Locations had Latitude and Longitude values inserted due to ' + @nLocFieldName +' and ' + @nGeoFieldName +' direct matches'
			end
		SET @nLoop2 = @nLoop2 + 1
		end  ---End of Loop 2
	SET @nLoop1 = @nLoop1 + 1
end --End of Loop 1
/*
	Report On Null Records 
*/
SELECT 
	@nCount = count(DISTINCT LocationID)
FROM
	Location
WHERE
	Latitude IS NULL
	AND	
	Longitude IS NULL
PRINT cast(@nCount AS varchar (5)) + ' of all Locations have no Latitude or Longitude.'


GO
