set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

ALTER          proc [dbo].[dedupeLocation]
			@RemoveLocationId	int,
			@RetainLocationId	int,
			@createdByPersonID	int = 0,
			@AllowDedupeAcrossOrganisations varchar(5) = 'No',
			@Run			varchar(5) = 'No'
as

/*
WAB: 10/26/05

This procedure written after errors occurred when some locations in different organisations
were deduped and the organisationid in the person table was not updated.
(This is generally prevented by the front end which is why it hasn't been a big issue - but i think that it occurred during auto deduping)
By default this procedure prevents merging of locations across organisations.  
However I haev added a switch which will allow this - although I can't say that it does it rigourously 
eg. there could be problems with rewards accounts.  

I would suggest that we need a procedure which will do a rigorous location move , and this could be plumbed in here if necessary


Otherwise much like dedupePerson and dedupeOrganisation

Calls dedupe entity, but with checks on 


WAB 2007/07/17 added the error handling/rollback which is in dedupeperson

AJC 2010/06/08 P-SNY096 Reseller Stock and Sales Metadata - Append the locationID begin removed to the historicalLocationIDList column
*/

	declare
	@ErrCode 	int,
	@StartedTran 	bit,		-- Indicates whether a transaction was started inside this proc.
	@historicalLocationIDList varchar(2000)

	set @ErrCode = 0


	If @Run = 'Yes' and @@TranCount = 0 
	 begin
	   set @StartedTran = 1
	   Begin Transaction
	 end



	if not exists 
		(
		select 1 
			from 
		location l1 
			inner join 
		location l2 on l1.locationid = @RemoveLocationId and l2.locationid = @RetainLocationId 
			where l1.countryid = l2.countryid
		)


		begin	
		   RAISERROR ('Error. Locations must be in same Country to be merged', 16, 1)
		   Return
		End



if @AllowDedupeAcrossOrganisations <> 'yes'
	-- check that locations are in same organisation
begin
	if not exists (
		select 1 
			from 
		location l1 
			inner join 
		location l2 on l1.organisationid = l2.organisationid
		where l1.locationid = @RemoveLocationId and l2.locationid = @RetainLocationId 
	)

		begin	
		   RAISERROR ('Error. Locations must be in same organisation to be merged, unless @AllowDedupeAcrossOrganisations is set to Yes.', 16, 1)
		   Return
		End

End

if @Run <> 'yes'
	print 'The Following rows will have Locationid updated'


/*===================================================================================*/
-- Perform generic dedupe
/*===================================================================================*/
select @historicalLocationIDList = historicalLocationIDList from location where locationID = @RemoveLocationId

exec @ErrCode = dedupeEntity
		@RemoveEntityId	= @RemoveLocationId,
		@RetainEntityId	= @RetainLocationId,
		@Entity		= 'Location',
		@createdByPersonID	= @createdByPersonID,
@Run		= @Run

-- If there was a problem, rollback the whole dedupe
If @ErrCode <> 0 GOTO ErrorTrap

--AJC 2010/06/08 P-SNY096 Reseller Stock and Sales Metadata - Append the locationID begin removed to the historicalLocationIDList column
update location
set historicalLocationIDList = historicalLocationIDList+','+@historicalLocationIDList
where locationID=@RetainLocationId

if @AllowDedupeAcrossOrganisations = 'yes'
  Begin
	-- need to update person records so that organisationid is updated to same one as new location
	-- there is an argument that locations should only be merged if they are already in the same organisation
	-- note that mergeing locations in different organisations could leave people's reward accounts in limbo
	if @Run = 'yes'
	   Begin	
		update person 
		set organisationid = l.organisationid
		from person p inner join location l on p.locationid = l.locationid
		where l.locationid = @RetainLocationId and p.organisationid <> l.organisationid
	   End	
	else
	  begin
		Print 'The following people will have their organisationid updated'

		select personid
		from person p inner join location l on p.locationid = l.locationid
		where l.locationid = @RemoveLocationId

   	  end

End


-- If there was a problem, rollback the whole dedupe
If @ErrCode <> 0 GOTO ErrorTrap

/*===================================================================================*/
-- Exit Point
/*===================================================================================*/
If @Run = 'Yes' and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
RETURN @ErrCode

/*===================================================================================*/
-- Error Trap
/*===================================================================================*/
ErrorTrap:
--print 'Error in DedupeEntity detected by DedupeLocation'
If @Run = 'Yes' and @StartedTran = 1 and @@TranCount > 0
   ROLLBACK TRANSACTION

	raiserror ('Location Dedupe Failed',16,1)

GOTO ExitPoint
