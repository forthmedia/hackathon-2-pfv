IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'setBooleanFlag' and Type = 'P')
	DROP PROCEDURE setBooleanFlag
	
GO
	
CREATE     PROCEDURE [dbo].[setBooleanFlag]
	  @entityid int 					-- the entityID or null if using temporary table
	, @flagid varchar(100) 				-- flagID or flagTextID or null if using temporary table with flagID column
	, @userid int 
	, @updatedByPersonID int  = null
	, @tablename sysname = '#setFlag'   -- must be a temporary table.  Requires entityID column.  Optional flagid and Data columns

AS

/*
SetBooleanFlag

Sets a checkbox type boolean flag if not already set  (will actually also set a radio flag - but will [correctly] fall over if there is already a radio flag in the group)
If used with temporary table with data column can be used to delete a flag as well

TEST SCRIPT: See _support\tests\sql\flagStoredProcedures\test_setBooleanFlag.sql
Note that code structure is very similar to setRadioFlag and bugs in one may exist in the other

WAB 2012/04/11   Made to work like setRadioFlag (which was modified last year)
	Modified so that it can work in 3 different ways
	1. Pass in @flagID and @entityId to set a single flag (original version) 
	2. Pass in @flagID  and a temporary table called #setFlag with a column entityID
	3. Pass in 	temporary table called #setFlag with a columns entityID and flagID

WAB 2015-10-13
	While modifiying deleteBooleanFlag to run off a temporary table
	Changed to run with dynamic SQL because ran into problems with flagid sometimes existing on temp table
	At this point decided that might as well add @tableName parameter
	Added support for a data column 
*/

Set nocount on

declare 
	  @ErrorMessage nVarchar (200)
	, @tableID int
	, @sqlCmd nVarChar (max)
	, @hasNonBooleanFlag  bit
	, @hasDataColumn  bit
	, @tempFlagID int
	, @tempFlagGroupID int
	, @tempFlagTypeID int



select @userid = isNull(@userID,0)  -- createdBy and LastUpdated by can't be null, but do default to zero

/*
First check that the flagids are of radio or boolean type
*/ 

IF @flagID is not null
BEGIN -- get numeric flagid and check is a boolean 

	select @tempFlagID = flagid, @tempFlagGroupID = fg.flaggroupid, @tempFlagTypeID = flagTypeid 
	from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid 
	where (convert(varchar,flagid) = @flagid OR flagTextID = @flagid )

	IF @tempFlagTypeID not in  (2,3) or @tempFlagTypeID is null
	BEGIN
		SET @ErrorMessage = @FlagID + ': Not a boolean flag'
	END

	
END
ELSE
BEGIN
	select @tableID = object_id('tempdb..' + @tablename)

	IF @flagID is null and exists (select * from tempdb..syscolumns where id = @tableID and name = 'flagid')
	BEGIN
		set @sqlCmd = 'select @hasNonBooleanFlag = 1 
						from ' + @tableName + ' ef
						inner join vflagdef f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
						and flagTypeID not in (2,3)'
		exec sp_executeSQL @sqlCmd,N'@hasNonBooleanFlag bit OUTPUT', @hasNonBooleanFlag = @hasNonBooleanFlag OUTPUT
		IF @hasNonBooleanFlag = 1
		  BEGIN
			set @ErrorMessage =  @tableName + ' Table contains a non-boolean flag'
		  END
	END	  
	ELSE IF @flagID is null
	BEGIN
			set @ErrorMessage =  '@fladID null and no flagID column on ' + @tableName + ' table'
	END

	select @hasDataColumn = count(1) from tempdb..syscolumns where id = @tableID and name = 'data'

END


IF @ErrorMessage is not null 
BEGIN

	RAISERROR (@ErrorMessage, 16, 16)
	Return
END

Set nocount off
/*
Now do the update
*/
IF @entityID is null
	BEGIN -- using a temporary table
		
		IF @flagID is not null
			BEGIN  --- @flagID supplied 
				SET @sqlCmd = 			
						'insert into booleanflagdata
							(entityid,flagid,created,createdby,lastupdated,lastupdatedby,lastupdatedByPerson)
						select
							ef.entityid,@tempFlagID,getdate(),@userid,getdate(),@userid,@updatedByPersonid
						from ' +
							@tablename  + ' ef
								left join 
							booleanFlagData bfd on ef.entityid = bfd.entityid and bfd.flagid = @tempFlagID
						where bfd.flagid is null	
						'
						+ case when @hasDataColumn = 1 then ' and ef.data = 1' else '' end
					exec sp_executeSQL @sqlCmd, N'@tempflagid int, @userid int, @updatedByPersonID int', @tempflagid = @tempflagid, @userid = @userid , @updatedByPersonID = @updatedByPersonID 
				
				
			END
		ELSE
			BEGIN  --- flagID on temporary table
				SET @sqlCmd = 			
						'insert into booleanflagdata
							(entityid,flagid,created,createdby,lastupdated,lastupdatedby,lastupdatedByPerson)
						select
							ef.entityid,f.flagid,getdate(),@userid,getdate(),@userid,@updatedByPersonid
						from 
							' + @tableName + ' ef
								inner join
							flag f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
								left join 
							booleanFlagData bfd on ef.entityid = bfd.entityid and bfd.flagid = f.flagid
						where bfd.flagid is null	
						'
						+ case when @hasDataColumn = 1 then ' and ef.data = 1' else '' end
				
					exec sp_executeSQL @sqlCmd, N'@userid int, @updatedByPersonID int', @userid = @userid , @updatedByPersonID = @updatedByPersonID 
			
			END
			
			IF @hasDataColumn = 1 
			BEGIN
				create table #deleteFlags (entityID int)
				declare @fieldList varchar (100) = 'entityid'
				
				IF @flagID is null
				BEGIN
					alter table #deleteFlags add flagid varchar(100)
					set @fieldList += ',flagid'
				END
					
				SET @sqlCmd = '
					insert into #deleteFlags (' + @fieldList + ' )
					select ' + @fieldList + '
					from ' + @tableName + '
					where data = 0
				'	
				print @sqlCmd
				exec (@sqlCmd)				
				
				exec deleteBooleanFlag
						@entityid = @entityid
						, @flagid = @flagid 
						, @userid = @userid 
						, @updatedByPersonID = @updatedByPersonID 
						, @tableName = '#deleteFlags'

				
			END
	END
ELSE	
	BEGIN -- @flagID and @entityID supplied

			-- is it already set?	
			IF not exists (select 1 from booleanflagdata bfd where  entityid = @entityid and flagid = @tempFlagID)
				BEGIN
					insert into booleanflagdata (entityID,flagID, created, createdby,lastupdated,lastupdatedby,lastupdatedbyperson )
					values (@entityid, @tempflagid, getdate(),@userid,getdate(),@userid,@updatedByPersonID)
	
				END

	END
