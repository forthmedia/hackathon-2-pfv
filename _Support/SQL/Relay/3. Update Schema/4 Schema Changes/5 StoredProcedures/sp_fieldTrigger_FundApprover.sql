/* ==========================================================================================
  	Author:			<YAN ANDERSON>
  	Create date:		<2013/05/01>
  	Description:		<This trigger will delete records from the 'fundApprover' table if they 
					have had the 'FundApprover' flag removed.>
  
 	Mods
	WAB 2013-05-01	Recode to use the #modifiedEntityIDs table	
  ========================================================================================== */

declare @flagID int
select @flagID  = flagID from flag where flagtextID = 'FundApprover'


/* add the field trigger */
if not exists (select * from fieldtrigger where triggerName = 'sp_fieldTrigger_FundApprover')
	begin
		insert into fieldTrigger (fieldType,FieldID,orderingIndex,active,triggerName,RecordUpdatesAgainstID)
		values ('flag',@flagID ,1,1,'sp_fieldTrigger_FundApprover',null)
	end


if exists (select * from sysobjects where name = 'sp_fieldTrigger_FundApprover' and XTYPE = 'P')
	DROP Procedure sp_fieldTrigger_FundApprover

GO
	
/* add the stored procedure */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[sp_fieldTrigger_FundApprover]
	@flagID int, 
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@triggerInitiatedByPersonID int, 
	@RecordUpdatesAgainstID int

AS
Set nocount on


-- Delete records from fundApprover if there is no longer a fundapprover flag set
delete fundapprover 
from #modifiedEntityIDs ent
	LEFT JOIN
	 booleanflagdata bfd ON flagID = @flagID and bfd.entityID = ent.entityid
	 INNER JOIN
	 fundApprover fa ON fa.personID = ent.entityid
WHERE
	bfd.flagid is null

