
ALTER Procedure [dbo].[DeleteEntityFlags]
@EntityTypeID int ,   
@EntityID varchar(10)  = null,
@deletedbyperson int  = null,
@test bit = 0,
@deletedby int  = null

AS


/*
WAB 2007/11/18 
a generic sp for deleting all flags associated with an entity
to replace code which appears in each delete sp and which doesn't actually deal with every flag type

need to pass in either @entityID or create a temporary table called #deleteEntityIdTable before calling

error handling stolen from the dedupe code

WAB 2015-09-29 Altered to takes @deletedbyperson (a personid) rather than @deletedby (a usergroupid), but still backwards compatible
WAB 2015-11-20 Fixed problem whereby of there was an error during the deletion the cursor was left hanging around.  
				Haven't identified actual error yet	
WAB 2015-12-01	Discovered a case when this code was called recursively.  Can't think why, but by making the cursor local I have been able to fix the problem
				Also fixed error when adding deletedByPerson column to table (doing the add and update in the same sql caused an error)

*/
SET NOCOUNT ON

Declare @sql varchar(max),
	@dataTableName sysname,
	@baseSQL nVarchar(max),
	@flagtypeid int,
	@StartedTran bit,		-- Indicates whether a transaction was started inside this proc
	@StartDel datetime,
	@ErrCode int = 0			-- Error code


if @deletedby is not null and @deletedbyperson is null
begin
	select @deletedbyperson = personid from usergroup where usergroupid = @deletedby
end

if @entityID is not null 
  begin
	select @entityid as entityid, @deletedbyperson as deletedbyPerson into #deleteEntityIdTable
  end

/* if there isn't a deletedByPerson column on the table then add it (backwards compatibility */
IF not exists (select 1 from tempdb..syscolumns where id = object_id('tempdb..#deleteEntityIdTable') and name = 'deletedByPerson')
BEGIN

	set @sql = '
		alter table #deleteEntityIdTable add deletedByPerson int
	'
	
	exec (@sql)

	set @sql = '		
		update #deleteEntityIdTable 
			set deletedByPerson = personid
		from
			#deleteEntityIdTable 
				inner join
			usergroup ug on ug.usergroupid = deletedBy	
	'

	exec (@sql)

END


set @startedTran = 0
Select @StartDel=getdate()

-- If this proc wasn't called from inside a transaction, start a new one
If @@TranCount = 0
 begin
   set @StartedTran = 1
   BEGIN TRANSACTION
 end



-- use a cursor to build up the delete queries for each flag type:
DECLARE flagTable_cursor CURSOR LOCAL 
FOR
Select 
	ft.datatablefullname, 
	' from ' + ft.datatablefullname + ' fd inner join vflagdef f on f.flagid = fd.flagid inner join #deleteEntityIDTable d on  d.entityID = fd.entityid where entityTypeid = ' + convert(varchar,@entityTypeID) + ' and flagTypeID = ' + convert(varchar,flagTypeid) as baseSQLCommand, 
	flagTypeID

From FlagType as FT
inner join sysobjects so on ft.datatablefullname = so.name
Where exists (select 1 from flaggroup where entityTypeID = @entityTypeID and flaggroup .flagTypeid = ft.flagTypeID) 


OPEN flagTable_cursor
FETCH NEXT FROM flagTable_cursor into @dataTableName, @basesql, @flagtypeid
WHILE @@FETCH_STATUS = 0
BEGIN

	IF (@Test = 1)
	BEGIN
		set @sql = 'Select fd.Entityid ' + @baseSQL
	END
	ELSE
	BEGIN
		set @sql = 'update ' + @dataTableName + ' set lastUpdated = getdate(), lastUpdatedByPerson = d.deletedByPerson, lastupdatedby = isnull((select usergroupid from usergroup where usergroup.personid = d.deletedbyperson),0) '  + @baseSQL
		set @sql += char(10) + 'delete ' + @dataTableName + @baseSQL
	END
	
	Exec(@sql)
	Set @ErrCode = @@Error
	
	If @ErrCode <> 0 GOTO ErrorTrap

	FETCH NEXT FROM flagTable_cursor into @dataTableName, @basesql, @flagtypeid
END
  
CLOSE flagTable_cursor
DEALLOCATE flagTable_cursor





/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
RETURN @ErrCode


/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:

Print 'In Error Trap'
IF (CURSOR_STATUS('local', 'flagTable_cursor') > -2)
BEGIN
	CLOSE flagTable_cursor
   	DEALLOCATE flagTable_cursor
END	
   

If @@TranCount > 0 and @StartedTran = 1
BEGIN	
   ROLLBACK TRANSACTION
END

GOTO ExitPoint






