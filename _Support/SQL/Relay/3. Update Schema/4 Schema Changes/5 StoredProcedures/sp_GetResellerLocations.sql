IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'sp_GetResellerLocations' and Type = 'P')
	DROP PROCEDURE sp_GetResellerLocations
	
GO
	
CREATE     PROCEDURE [dbo].[sp_GetResellerLocations] 

@NumResults 		char(5) = 5,   	/* Number of search results required INTEGER*/
@Latitude 		char(16),	/* User's Latitude position FLOAT*/
@Longitude 		char(16),	/* User's Longitude position FLOAT */
@PartnerFlag		int = 0,--char(5),		/* NO LONGER USED kept in for backwards compatability */
@OrgFilterList		varchar(255) = '',	/* List of org Flag IDs to select results from / order by - eg. Focus Gold, Focus Bronze */
@locFilterList		varchar(255) = '',	/* List of loc Flag IDs to select results from / order by - eg. Focus Gold, Focus Bronze */
@CountryIDs		varchar(500),	/* List of IDs of countries in which to search */
@SelectionOrder	bit = 0,		/* If 0, then choose nearest results, if 1, then choose nearest results in FilterList order */
@OrgApprovalFlagTextID	varchar(100) = 'OrgApproved',  /* orgApprovalFlagTextID - Get locations only in organisations that have been approved*/
@OrgRequiredList		varchar(255) = '',	/* List of Flags IDs required (NYB-bug 1384) */
@LocRequiredList		varchar(255) = ''	/* List of Flags IDs required (NYB-bug 1384) */


AS

/*
Modifications
2013-02-28 PPB Case 432691 don't return inactive locations
2013-10-02 NYB Case 435669 though still called @OrgApprovalFlagTextID this can now handle location, organisation or country flags 
*/

DECLARE @SQL varchar(4000)
DECLARE @deleteSQL varchar(2000)
declare @flagid varchar(50)

SET NOCOUNT ON

if @PartnerFlag != 0
	begin
		if exists(select * from vflagdef where flagid=@PartnerFlag)
			begin
				if exists(select * from vflagdef where flagid=@PartnerFlag and EntityTable='Location')
					begin
						SET @LocRequiredList = REPLACE (@LocRequiredList,cast(@PartnerFlag as varchar(20)),'')
						SET @LocRequiredList = REPLACE (@LocRequiredList,',,',',')
						if @LocRequiredList = ''
							SET @LocRequiredList = cast(@PartnerFlag as varchar(20))
						else
							SET @LocRequiredList = @LocRequiredList+','+cast(@PartnerFlag as varchar(20))
					end
				else
					begin
						SET @OrgRequiredList = REPLACE (@OrgRequiredList,cast(@PartnerFlag as varchar(20)),'')
						SET @OrgRequiredList = REPLACE (@OrgRequiredList,',,',',')
						if @OrgRequiredList = ''
							SET @OrgRequiredList = cast(@PartnerFlag as varchar(20))
						else
							SET @OrgRequiredList = @OrgRequiredList+','+cast(@PartnerFlag as varchar(20))
					end
			end
	end

SELECT TOP 0 LOCATIONID INTO #tempPartnerTable FROM Location L with(nolock) 

/* START: 2013-10-02 NYB Case 435669 replaced:
set @SQL = 'INSERT INTO #tempPartnerTable SELECT DISTINCT L.LocationID FROM Location L with(nolock) 
			inner join (
				organisation o with(nolock) inner join (
					booleanFlagData appBFD with(nolock) 
					inner join flag appFlag with(nolock) on 
					appBFD.flagid=appFlag.flagid and appFlag.flagtextid in (' + @OrgApprovalFlagTextID + '))
					on o.organisationid=appBFD.entityid 
			) on L.organisationID=o.organisationID 
				and o.organisationID not in (select organisationID from vOrgsToSupressFromReports with(nolock)) ' 
Case 435669 with: */

IF NOT EXISTS (select * from vFlagDef where FlagTextID=@OrgApprovalFlagTextID)
	BEGIN 
		SET @OrgApprovalFlagTextID=REPLACE(@OrgApprovalFlagTextID,'''','')
	END

declare @approvalFlagid varchar(50), @DataTableFullName varchar(50),@uniqueKey varchar(50)
select @approvalFlagid=CAST(flagid as varchar(50)), @DataTableFullName=DataTableFullName,@uniqueKey=uniqueKey from vFlagDef where FlagTextID=@OrgApprovalFlagTextID

set @SQL = 'INSERT INTO #tempPartnerTable SELECT DISTINCT L.LocationID FROM Location L with(nolock) 
		INNER JOIN organisation o ON L.organisationID=o.organisationID 
			and o.organisationID not in (select organisationID from vOrgsToSupressFromReports with(nolock))
		INNER JOIN '+@DataTableFullName+' d with(nolock) on L.'+@uniqueKey+'=d.entityid and d.flagid='+@approvalFlagid

/* END: 2013-10-02 NYB Case 435669 */

if @OrgRequiredList <> ''
	begin
		declare c CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
			Select cast(IntValue as varchar(50)) as flagid from dbo.CsvToInt(@OrgRequiredList)
		open c
		fetch c into @flagid
		while (@@fetch_status <> -1)
		begin
			if (@@fetch_status <> -2) 
				begin
					set @SQL = @SQL+'inner join booleanFlagData as flag_'+@flagid+' with(nolock) 
								on flag_'+@flagid+'.entityID = L.organisationID 
								and flag_'+@flagid+'.flagID='+@flagid

				end
			fetch c INTO @flagid
		end
		close c
		deallocate c
	end

if @LocRequiredList <> ''
	begin
		declare c CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
			Select cast(IntValue as varchar(50)) as flagid from dbo.CsvToInt(@LocRequiredList)
		open c
		fetch c into @flagid
		while (@@fetch_status <> -1)
		begin
			if (@@fetch_status <> -2) 
				begin
					set @SQL = @SQL+'inner join booleanFlagData as flag_'+@flagid+' with(nolock) 
								on flag_'+@flagid+'.entityID = L.locationid  
								and flag_'+@flagid+'.flagID='+@flagid

				end
			fetch c INTO @flagid
		end
		close c
		deallocate c
	end
EXEC(@SQL)

/* Get only locations that are in organisations that have the orgFlagID */
if @OrgFilterList <> ''
	begin
		select @deleteSQL='delete from #tempPartnerTable where locationID not in
		(select locationID from location l inner join booleanFlagData bfd on bfd.entityID = l.organisationID
		 where bfd.flagID in ('+@OrgFilterList+'))'
		EXEC(@deleteSQL)
	end 


CREATE TABLE #tempLocator (LocationID int, CountryDescription varchar(40), /*FlagID int,*/ Dist float(8),latitude float(8),longitude float(8))--, FilterID int)

if @locFilterList <> ''
	begin
		SELECT @SQL =  'INSERT #tempLocator
			SELECT DISTINCT /*TOP  ' + RTRIM(@NumResults) + '*/  
			L.LocationID, C.CountryDescription, /*F.FlagID,*/ 
			(((acos(sin(('+RTRIM(@Latitude)+'*pi()/180)) * sin((l.latitude*pi()/180)) + cos(('+RTRIM(@Latitude)+'*pi()/180)) * cos((l.latitude*pi()/180)) * cos((('+RTRIM(@Longitude)+' - l.longitude)*pi()/180))))*180/pi())*60*1.1515) dist, 

			L.latitude, L.longitude--, TL.FilterID

			FROM		Location L, Country C, BooleanFlagdata F, /*#tempFilterTable TL,*/ #tempPartnerTable TP

			WHERE	/*CONVERT(INT,TL.Filter) = F.FlagID AND*/
					L.CountryID = C.CountryID
			AND		C.CountryID IN (' + RTRIM(@CountryIDs) + ')

			AND		F.EntityID = L.LocationID
			AND		L.LocationID = TP.LocationID
			AND		F.FlagID IN (' + RTRIM(@locFilterList) + ')

			AND		L.Latitude  is not NULL
			AND		L.Longitude is not NULL
			AND		L.Active=1	
		'
	end
else
	begin
		SELECT @SQL =  'INSERT #tempLocator
			SELECT DISTINCT /*TOP  ' + RTRIM(@NumResults) + '*/  
			L.LocationID, C.CountryDescription,
			(((acos(sin(('+RTRIM(@Latitude)+'*pi()/180)) * sin((l.latitude*pi()/180)) + cos(('+RTRIM(@Latitude)+'*pi()/180)) * cos((l.latitude*pi()/180)) * cos((('+RTRIM(@Longitude)+' - l.longitude)*pi()/180))))*180/pi())*60*1.1515) dist, 

		L.latitude, L.longitude

			FROM		Location L, Country C, #tempPartnerTable TP

			WHERE	
					L.CountryID = C.CountryID
			AND		C.CountryID IN (' + RTRIM(@CountryIDs) + ')
			AND		L.LocationID = TP.LocationID

			AND		L.Latitude  is not NULL
			AND		L.Longitude is not NULL
			AND		L.Active=1	
		'
	end


if @SelectionOrder = 1
	begin
		SELECT @SQL = @SQL + ' ORDER BY dist /*filterID, dist*/'
	end
else
	begin
		SELECT @SQL = @SQL + ' ORDER BY dist'
	end
EXEC(@SQL)


SET NOCOUNT OFF

SELECT TL.* , L.*,o.orgUrl, o.orgEmail, o.AKA
FROM #tempLocator TL, Location L, Organisation O
WHERE 	TL.LocationID = L.LocationID
AND l.organisationID = o.organisationID
ORDER BY dist, sitename, TL.locationID /*FilterID, dist*/


/**** SCRIPT END****/
