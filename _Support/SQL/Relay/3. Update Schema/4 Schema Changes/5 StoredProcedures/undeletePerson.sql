IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'UndeletePerson' and Type = 'P')
	DROP PROCEDURE UndeletePerson
	
GO
	
CREATE           proc [dbo].[UndeletePerson]
  @PersonID int 
as
declare @status int,
 @sqlerror int,
 @rcount int,
 @countryid int,
 @moddate datetime,
 @ActionByCF int
 

/*
 
2008-01-21   WAB this procedure can't have ever worked properly! It was looking in Modregister against 'LD', not 'PD'
  also added dateofbirth to person insert
2008/11/18 WAB changed undelete of integerflagdata so that convert is to bigInt, not int
2011-02-28 NYB LHID 5761 changed undelete of booleanFlagdata so it doesn't fall over when trying to set radio flags 
  when radio flags changed just before a person was deleted
2013-04-24 YMA Case 434607 updated to check organisationID from location in case the org was merged since the user was deleted.
2016/09/01 NJH JIRA PROD2016-125 - turn identity insert on/off
TODO   Not all flag types are dealt with here
 
*/
set nocount on
if not exists(select * from PersonDel where Personid = @PersonId)
begin
    RAISERROR ('Error Person Id %d supplied is not in the deleted set',
       16, 1, @personid)
    RETURN 1
end
select @moddate = moddate,
 @ActionByCF = ActionByCF
  from modregister m
 where action='PD'
 and recordid = @personid
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d selecting ModRegister details for Person %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end
begin transaction
set identity_insert dbo.person on
insert Person(
       PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, EmailStatus, JobDesc, Language, Notes, LocationID, OrganisationID, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualLocation,  Title, Department, R1PerID, R1LocID, sex, initials, SecondName, nFirstName, nLastName, nTitle, emailStatusOld, R2PerId, R2LocId,dateofbirth)
select p.PersonID, p.Salutation, p.FirstName, p.LastName, p.Username, p.Password, p.PasswordDate, p.LoginExpires, p.FirstTimeUser, p.HomePhone, p.OfficePhone, p.MobilePhone, p.FaxPhone, p.Email, p.EmailStatus, p.JobDesc, p.Language, p.Notes, p.LocationID, l.OrganisationID, p.Active, p.CreatedBy, p.Created, p.LastUpdatedBy, p.LastUpdated, p.DupeChuck, p.DupeGroup, p.ActualLocation, p.Title, p.Department, p.R1PerID, p.R1LocID, p.sex, p.initials, p.SecondName, p.nFirstName, p.nLastName, p.nTitle, p.emailStatusOld, p.R2PerId, p.R2LocId, p.dateofbirth
from PersonDel p
 inner join location l on  p.locationID = l.locationID
 where personid = @personid
 set identity_insert dbo.person off
 
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting person rows for Personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end
delete PersonDel 
 where personid = @personid
select @sqlerror = @@error
 
if @sqlerror <> 0
 
begin 
   RAISERROR ('Error : %d deleting person delete record for Personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end
select distinct flagid into #f from  modregister r 
    where r.moddate between dateadd(hh,-12,@moddate)
        and dateadd(hh,12,@moddate)
    and r.action = 'FD'
    and ActionByCF=@ActionByCF
 and recordid = @personid
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting flag rows into temporary table for Personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end


/**********************************************************   
	insert BooleanFlagData 
**********************************************************/
insert BooleanFlagData(EntityID,FlagID)
/*NYB 2011-02-28 LHID 5761 replaced:*/
/*
select distinct recordid,flagid from  modregister r 
    where r.moddate between dateadd(hh,-12,@moddate)
        and dateadd(hh,12,@moddate)
    and r.action = 'FD'
    and ActionByCF=@ActionByCF
and recordid = @personid
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'boolean'
  and flagtextID <> 'DeletePerson')
*/
/*NYB 2011-02-28 LHID 5761 with:*/
select distinct r.recordid,v.flagid 
from  modregister r 
inner join vflagdef v on r.flagid=v.flagid 
inner join 
(
	select max(modDate) as maxModDate,r.recordid,flaggroupid 
	from  modregister r 
	inner join vflagdef v on r.flagid=v.flagid 
	where r.moddate between dateadd(hh,-12,@moddate)
		and dateadd(hh,12,@moddate) and r.action = 'FD'
		and ActionByCF=@ActionByCF and recordid = @personid
		and DataTable = 'boolean' and flagtextID <> 'DeletePerson'
	group by r.recordid,flaggroupid 
) as d on d.maxModDate=r.moddate and v.flaggroupid=d.flaggroupid and r.recordid=d.recordid 
where r.moddate between dateadd(hh,-12,@moddate)
	and dateadd(hh,12,@moddate)
    and r.action = 'FD' and ActionByCF=@ActionByCF
	and r.recordid = @personid and DataTable = 'boolean' 
	and v.flagtextID <> 'DeletePerson' 
/*NYB 2011-02-28 LHID 5761 END:*/

select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting boolean flag rows for personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end


/**********************************************************   
	insert TextFlagData 
**********************************************************/
insert TextFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,oldval from  modregister r 
    where r.moddate between dateadd(hh,-12,@moddate)
        and dateadd(hh,12,@moddate)
    and r.action = 'FD'
    and ActionByCF=@ActionByCF
and recordid = @personid
and flagid in (
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'text')
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting text flag rows for Personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end


/**********************************************************   
	insert IntegerFlagData 
**********************************************************/
insert IntegerFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,convert(bigint,oldval) from  modregister r    -- 2008/11/18 WAB change to bigint
    where r.moddate between dateadd(hh,-12,@moddate)
        and dateadd(hh,12,@moddate)
    and r.action = 'FD'
    and ActionByCF=@ActionByCF
and recordid = @personid
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'integer')
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting integer flag rows for personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end


/**********************************************************   
	insert DateFlagData 
**********************************************************/
insert DateFlagData(EntityID,FlagID,data)
select distinct recordid,flagid,convert(datetime,oldval) from  modregister r 
    where r.moddate between dateadd(hh,-12,@moddate)
        and dateadd(hh,12,@moddate)
    and r.action = 'FD'
    and ActionByCF=@ActionByCF
and recordid = @personid
and flagid in(
select f.flagid from #f f
 inner join  vflagdef v on f.flagid=v.flagid
where DataTable = 'date')
select @sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting date flag rows for personid %d ',
      16, 1, @sqlerror,@personid)
   rollback transaction
   RETURN 1
end


commit transaction
return 0