IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'dedupeEntity' and Type = 'P')
	DROP PROCEDURE dedupeEntity

GO

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[dedupeEntity]
			@RemoveEntityId	int,
			@RetainEntityId	int,
			@Entity		varchar(32), 		-- Organisation, Location or Person
			@Run		varchar(5) = 'No',
			@Trace 		bit = 0,		-- Switch to enable/disable printing of dynamic SQL commands
			@createdByPersonID	int = 0		-- Store the person who has created the dedupe.
as

/***************************************************************************************
** Name   : dedupeEntity
** Created: Dec/2000
** By     : GP
**
** Purpose: 
** Deduplicate IDs for any entity
** 
** Arguments:
**      See above
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00	    Dec/2000     GP   Original
** 1.01     02/03/2001   WAB  Corrected a couple of bugs.  Also altered so that it will 
**                            dedupe on any column with name of the form %EntityID  
**                            eg: DistiOrganisationID	
** 1.02     09/03/2001   WAB  Also altered so that does not touch table @entityDel  - 
**                            OK there shouldn't be an entry, but I wanted to do some 
**                            retrospective clear up of previous dedupes
** 1.03	    05/04/2001   JVH  Take account of potential duplicate rows (flag and non-flag data) when merging. 
**                            Prevent duplicate/multiple radio button flags.
**                            Ignore any ...Del tables containing a %EntityID FK column -
**                               leave these with the original FK value.
**                            Exclude 'x...FlagData' tables.
** 1.03a    04/May/2001  JVH  Modify duplicate detection code to look only at UNIQUE or
**                            PRIMARY KEY constraints.  If Duplicates found in non-Flag data tables,
**                            abort the merge pending further investigation.
** 1.04     14/Jun/2001  JVH  Ticket 5590:
**                            o Fix bug printing 'delete' command for duplicate flag data in non-run mode.
**                            o Remove the exclusion on RW... tables
** 1.05     18/Jun/2001  JVH  Ticket 5590:
**                            o Enhance handling of RI violations caused by deletion of potential
**                              duplicates or updates to columns which are part of a primary key.
**                            o Enhanced transaction management.
**                            o Detect potential duplicates in Category 3 tables.
** 2.00     21/Jun/2001  JVH  Ticket 5434:
**                            o Restructured code to make more modular and less monolithic.
** 2.01     25/Sep/2001  JVH  Ticket 6323:
**                            o Check existence of explicitly-named tables before trying to process them.
** 2.02     29/Oct/2001  JVH  Ticket 5178:
**                            o Add processing to handle IntersectionEntity.
**
** 		12/07/05 WAB  o problem with deduping in selectiontag when not doing person dedupe.  
**				Did work to DedupeEntityAmendChild, but also prevented dedupe of tempselectiontag unless @tablename = 'person'
**
**		 11/12/06 	WAB  need to add tables which have an entityID column but which are not flags
**		 30/01/07 	WAB  discovered a bug in the above change  	
**		18/06/2007 	NM  adding createdByPersonID as parameter. used to track who does a dedupe.
		2009/04/22 	WAB CR Lex 588 Merge Protection.  Checks for protected flags before doing delete
	 	2009/10/18 	WAB LID 2773 noticed that transaction being started if @run = 'No', didn't seem sensible
	 	2009/11/02 	WAB	LID 2821 Added check for tables being owned by dbo.  Later code fails if table not dbo owned
		2010/04/22	WAB	added support for entityID columns of form xxx_entityID	
`		2011/03/08  WAB LID 5849 Added another check for tables being owned by dbo.  This time it was for tables with flagid and entityid columns
		2011-03-25	NYB	LHID5511 - added update and delete sql for integer tables where entity is a linkedEntity 
  		2011/06/06 	WAB	LID6816 - changed @tablename to sysname type 
		2011/06/07  WAB LHID5511 rejigged NYB code because it was not dealing with both integerFlag and integerMultipleFlag tables.  Also not taknig accoutn of @run variable
		2013-08-27	YMA	Case 436694 Merge was not working.  dedupeEntity not specific enough about tables it is targeting.
		2015-10-21	NJH	Removed computed columns from the merge
		2016-02-29	NJH	Case 448044 Set ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER   to ON
****************************************************************************************/


/*===========================================================================================*/
-- Initialisations
/*===========================================================================================*/
declare @vsqlcmd nvarchar(2000),
	@rsqlcmd nvarchar(2000),
	@coresqlcmd nvarchar(2000),
	@coresqlcmd2 nvarchar(2000),
	@rsqlcmd2 nvarchar(2000),	-- Added for 1.03
	@vsqlcmd2 nvarchar(2000),	-- Added for 1.03
	@Tablename sysname,   -- WAB 2011/06/07 LID6816 changed from varchar(32) 
	@ColumnName sysname,

	@RemoveAccountId int,
	@RetainAccountId int,
	@EntityTypeId int,
	@KeyColumnName sysname,	-- Name of key column in flag table: Added 05/04/2001 v1.03
	@ConstraintName sysname,
	@ErrCode int,
	@LastTableCount int,	-- Number of tables processed in last pass through the 'Category 1' loop
	@ConstraintCount int,	-- Counter to control construction of SQL commands
	@AndOrText nvarchar(10),	-- Contains variable text for insertion in SQL commands
	@RemainderTable sysname,	-- Name of unprocessed Category 1 table
	@StartedTran bit,		-- Indicates whether a transaction was started inside this proc
	@IsInPK bit,			-- Indicates whether the column being updated is part of the Primary Key
					-- in the table to which it belongs
	@HasIdentityCol bit		-- Indicates whether the table has an IDENTITY column
--	@DupeCount int

Set @ErrCode = 0
Set @StartedTran = 0
Set @IsInPK = 0

/*===========================================================================================*/
-- The tables that refer to Organisations, Locations and Persons fall into a number of categories
-- 	1. Tables that have an NameId column    (eg LocationID)
--	2. Flag tables that have an EntityId column
--	3. Other tables that have EntityId column ( and usually an entityTypeID column
--	4. Tables that have a Recordid column (currently only ModRegister which is not changed)
/*===========================================================================================*/

-- Get the entitytypeid for the selected entity
select @EntityTypeId = EntityTypeId
  from SchemaTable 
 where entityName = @Entity

-- If this proc wasn't called from inside a transaction, start a new one
-- WAB 2009/10/18 noticed that if this procedure was entered with @run=No then a transaction was created and not comitted, best not to ALTER  transaction (as per dedupe person)
If @@TranCount = 0 
 begin
   set @StartedTran = 1
   BEGIN TRANSACTION
 end

SET NOCOUNT ON

/*   
WAB 2009/04/22
check for items which cannot be merged because of merge protection flags

*/
CREATE  table #DedupeEntityCheckProtectionResult (Type varchar(20) ,dataTable varchar(20),flagGroupID int,flagGroupName varchar(100),flagID int,flagName varchar(100),entityID int,data varchar(2000) )
exec dedupeentitycheckprotection @entityTypeID = @EntityTypeId, @entityid = @RemoveEntityId

if exists (select 1 from #DedupeEntityCheckProtectionResult)
BEGIN
	if @run = 'Yes'
		begin	
		   RAISERROR ('Error. MergeProtected. Entity is Protected From Merging. Must be retained record', 16, 1)
   	 	Goto ErrorTrap	
		End
	Else
		begin		
			print 'Error. Entity Protected From Merging. Must be retained record'
		end
END



/*===========================================================================================*/
--                            		CATEGORY 1
-- Create a cursor for the tables/columns where the primary key of the entity being deduped
-- is used as a foreign key.
--
-- Loop through the cursor and update these foreign keys.
/*===========================================================================================*/
if @Run <> 'yes'
begin
	select @vsqlcmd = N'The Following rows will have '+@Entity+N'Id updated'
	
end
/*
 This query rewritten so that it can handle column names of the form xyzentityid
Now brings back table name and columnname which are then used in the following queries 

select @rsqlcmd = 'declare cursorTablesToProcess insensitive cursor for select name, from sysobjects where type = ''U'' and id in('
		+ 'select id from syscolumns where name ='''+@Entity+'Id'')'
		+ ' and name <> '''+@Entity+''''
		+ ' and not name like ''RW%'''
		+ ' order by name'
*/

-- 1.05: CReate temp table to keep a list of the tables successfully processed
CREATE TABLE #Cat1TablesProcessed
	(TableName sysname NOT NULL)

-- 1.05: Initialise control variable to enable table processing loop to start
SET @LastTableCount = -1

/*-------------------------------------------------------------------------------------------*/
-- 1.05: Loop until no new tables have been added to the list of tables processed
/*-------------------------------------------------------------------------------------------*/
WHILE (SELECT count(*) from #Cat1TablesProcessed) > @LastTableCount
 BEGIN

   select @LastTableCount = count(*) from #Cat1TablesProcessed

   -- construct cursor to get list of tablenames and columns 
   select @rsqlcmd = N'declare cursorTablesToProcess insensitive cursor for select 

			t.name, c.name '
		+ N'from sysobjects t inner join syscolumns c on t.id = c.id inner join sysusers su on su.uid = t.uid '
		+ N'where t.type = ''U'' and su.name = ''dbo'' '
		+ N' and t.name not in(select TableName from #Cat1TablesProcessed) ' 	-- 1.05
		+ N' and t.name not like ''%dataload%'' '
		+ N'and ((c.name ='''+@Entity+N'Id'''
		+ N' and t.name <> '''+@Entity+N''''
--		+ N' and t.name <> '''+@Entity+'Del'''
		+ N' and t.name not like''%Del'''			-- 1.03 Ignore all ...Del tables
		+ N' and t.name not like''X%'')'			-- 1.03 Ignore all X... tables
--		+ N' and not t.name like ''RW%'')'			-- 1.04 Enable dedupes on RW... tables
		+ N' or (c.name  <> '''+@Entity+N'Id'' and c.name like  ''%'+@Entity+N'Id'' and c.name not like ''%AllocationID''))'
		+ N' and c.isComputed != 1' -- ignore computed columns

-- WAB 11/12/06 need to add tables which have an entityID column but which are not flags
-- 2013-08-27	YMA	Case 436694 Merge was not working.  dedupeEntity not specific enough about tables it is targeting.
+ N' union select t.name, c1.name from sysobjects t left join syscolumns c1 on t.id = c1.id and c1.name like ''%entityID''
join (SELECT column_name, table_name
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), ''IsPrimaryKey'') = 1) as primaryKey 
	on c1.name != primaryKey.column_name and t.name = primaryKey.table_name 
inner join (select column_name, table_name from INFORMATION_SCHEMA.COLUMNS 
	where data_type = ''int'') as dataType
	on c1.name = primaryKey.column_name and t.name = primaryKey.table_name
left join syscolumns c2 on t.id = c2.id and c2.name = ''flagID''
where t.type = ''u'' and c1.name is not null and c2.name is null
and t.name not like ''%dataload%''
and t.name not in(select TableName from #Cat1TablesProcessed) 
'
		+ N' order by t.name'


EXEC sp_executesql @rsqlcmd

   open cursorTablesToProcess

   fetch next from cursorTablesToProcess into @TableName, @ColumnName
   while @@fetch_status = 0
    begin
    if @trace = 1 print 'Calling sp DedupeEntityAmendChild for table ' + @tablename + ' column name '  + @ColumnName
	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@TableName      = @TableName,
			@ColumnName     = @ColumnName,
			@FlagData 	= 0,
			@Run            = @Run,
			@Trace 		= @Trace

	-- If the amend failed, take appropriate action:
	-- ErrorCode =  2: General failure - abort this dedupe
	--              1: Update or delete failed - try again on next iteration
	--              0: Amend OK - add to list of tables processed
	
	If @ErrCode = 0
	   Insert #Cat1TablesProcessed(TableName) Values(@TableName)
	Else if @ErrCode = 1
	   Print 'Leaving table ' + @TableName + ' for next iteration'
	Else 
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
		Close cursorTablesToProcess
		Deallocate cursorTablesToProcess
	 	Goto ErrorTrap
	 End

	fetch next from cursorTablesToProcess into @TableName, @ColumnName

    end		-- of loop through entity cursor
   close cursorTablesToProcess
   deallocate cursorTablesToProcess

 END	-- of outermost WHILE... loop

-- Check whether any tables weren't processed. If so, flag an error and exit

-- use exactly the same cursor command - since it excludes all items already processed and put into #Cat1TablesProcessed, it should return zero rows

/* select @rsqlcmd = N'declare remainder insensitive cursor for select distinct t.name'
		+ N' from sysobjects t inner join syscolumns c on t.id = c.id '
		+ N' where t.type = ''U'' '
		+ N' and t.name not in(select TableName from #Cat1TablesProcessed) ' 	
		+ N' and ((c.name ='''+@Entity+N'Id'''
		+ N' and t.name <> '''+@Entity+N''''
		+ N' and t.name not like''%Del'''			
		+ N' and t.name not like''X%'')'			
		+ N' or (c.name  <> '''+@Entity+N'Id'' and c.name like  ''%'+@Entity+N'Id''))'
*/
--print @rsqlcmd
EXEC (@rsqlcmd)

open cursorTablesToProcess
If @@cursor_rows > 0 
 begin
	print '*********************************************************************************'
	print '*** Dedupe abandoned because of RI violations caused by duplicate rows in'

	while 1=1
     	 begin
		fetch next from cursorTablesToProcess into @RemainderTable, @ColumnName
		if @@fetch_status <> 0 BREAK
		print '*** ' + @RemainderTable
	 end

	close cursorTablesToProcess
	deallocate cursorTablesToProcess

	print '*********************************************************************************'
	Set @ErrCode = 1
	GOTO ErrorTrap
 end

close cursorTablesToProcess
deallocate cursorTablesToProcess

/*===========================================================================================*/
--                            		CATEGORY 2
-- Process flag data: update the entityid for any flag data currently belonging to the entity being
-- removed.
--
-- 1.03 This section modified extensively to ensure that no non-unique rows result where they
--      are not allowed. Also modified to ensure that we don't end up with >1 value for 'radio button'
--      flags.
/*===========================================================================================*/

/*-------------------------------------------------------------------------------------------*/
-- Create and loop through a cursor of tables which contain a flagid column
/*-------------------------------------------------------------------------------------------*/
declare ent insensitive cursor for
	select t.name from sysobjects t
		inner join sysusers su on su.uid = t.uid 
		
	where type = 'U' 
	and su.name = 'dbo'
	and substring(t.name,1,1) <> 'x' 			-- 1.03 Exclude 'x...FlagData' tables
	and t.id in(
		select s.id from syscolumns  s
			inner join syscolumns c on c.id=s.id
		where s.name ='entityid'
		  and c.name ='flagid')
open ent
fetch next from ent into @TableName
while @@fetch_status = 0
begin

	print '***============ ' + @Tablename + ' ============***'

	------ 1.03 Remove any radio button flags  belonging to the removed entity if there is already a -----
	------ corresponding flag for the retained entity                                                -----
	If @TableName like'%booleanFlagData%'			-- Only do this for ..FlagData tables   WAB 13/06/2007 - this actually only needed for booleanflagdata / radio
	 begin
	   select @vsqlcmd	= N'select ''duplicateRadio'', t.* '
				+ N' from [' + @TableName + N'] t '
				+ N' join vFlagDef f on f.FlagID = t.FlagID  and f.EntityTable = ''' + @Entity + N''''
				+ N' where t.EntityID = @RemoveEntityID'
				+ N' and f.FlagGroupID in(select f.FlagGroupID from [' + @TableName + N'] fd '
				+ N' join vFlagDef f on fd.FlagID = f.FlagID '
				+ N' and f.DataType = ''Radio'' and f.EntityTable = ''' + @Entity + N''''
				+ N' where fd.EntityID in(@RetainEntityID,@RemoveEntityID) '
				+ N' group by f.FlagGroupID having count(f.FlagID) > 1) ',
		  @rsqlcmd	= N'delete t'
				+ N' from [' + @TableName + N'] t '
				+ N' join vFlagDef f on f.FlagID = t.FlagID  and f.EntityTable = ''' + @Entity + N''''
				+ N' where t.EntityID = @RemoveEntityID'
				+ N' and f.FlagGroupID in(select f.FlagGroupID from [' + @TableName + N'] fd '
				+ N' join vFlagDef f on fd.FlagID = f.FlagID '
				+ N' and f.DataType = ''Radio'' and f.EntityTable = ''' + @Entity + N''''
				+ N' where fd.EntityID in(@RetainEntityID,@RemoveEntityID) '
				+ N' group by f.FlagGroupID having count(f.FlagID) > 1) '


  	   if @Run = 'Yes'
	    begin
		print '>>> Delete duplicate radio buttons'
		if @trace = 1 print N'    ' + @rsqlcmd
		EXEC sp_executesql @rsqlcmd,
			N'@RetainEntityId INT, @RemoveEntityId INT',
			@RetainEntityId,
			@RemoveEntityId
	    end
	   else
	    begin
	   	print '>>> The following duplicate radio button flags will be deleted:'
		if @trace = 1 print N'    ' + @vsqlcmd
		EXEC sp_executesql @vsqlcmd,
			N'@RetainEntityId INT, @RemoveEntityId INT',
			@RetainEntityId,
			@RemoveEntityId
	    end
	 end

	/*-------------------------------------------------------------------------------------------*/
	-- Transfer remaining flag data from the removed entity to the retained entity
	/*-------------------------------------------------------------------------------------------*/

	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@TableName      = @TableName,
			@ColumnName     = 'EntityID',
			@Run            = @Run,
			@FlagData 	= 1,
			@Trace 		= @Trace

	-- If the amend failed, take appropriate action:
	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
		Close ent
		Deallocate ent

	 	Goto ErrorTrap
	 End

	-- Get the next ..FlagData table name
	fetch next from ent into @TableName
end

close ent
deallocate ent



/*-------------------------------------------------------------------------------------------*
  START update and delete entries in integer tables this entity is a linkedEntity for
  2011-03-25 NYB LHID5511 
*-------------------------------------------------------------------------------------------*/
declare ent insensitive cursor for
	select t.name from sysobjects t
		inner join sysusers su on su.uid = t.uid 
	where type = 'U' 
	and su.name = 'dbo'
	and substring(t.name,1,1) <> 'x' 			
	and t.id in(
		select s.id from syscolumns  s
			inner join syscolumns c on c.id=s.id
		where s.name ='entityid'
		  and c.name ='flagid') 
	and t.name like '%integer%' 
open ent
fetch next from ent into @TableName
while @@fetch_status = 0
begin

	print '***============ ' + @Tablename + ' Linked Entities ============***'

		select @coresqlcmd = 
					N' from ' + @tablename  
				+ ' inner join vflagdef f on ' + @tablename + '.flagid = f.flagid '
				+ ' left join ' + @tablename + ' fd2 on  fd2.flagid = f.flagid '
				+ ' and ' + @tablename + '.entityid = fd2.entityid and fd2.data = @RetainEntityid '
				+ ' where linksToEntityTypeID = @EntityTypeId '
				+ ' and ' + @tablename + '.data = @RemoveEntityID'
				+ ' and fd2.flagid is null ', /*ensures that we don�t do the update if there is already a record with this data value*/
			@coresqlcmd2 = 
					N' from '+@TableName+' t ' 
					+ ' inner join vflagdef on t.flagid = vflagdef.flagid ' 
					+ ' where linksToEntityTypeID = @EntityTypeId and t.data = @RemoveEntityID'
		
	
		select @rsqlcmd = 
					N'update '+@TableName 
					+ ' set data = @RetainEntityid '  
					+ @coresqlcmd ,
				@vsqlcmd = 
						'select ''update rows'', ''' + @tablename+ ''',' + @tablename+ '.flagid,' + @tablename+ '.entityid,' + @tablename+ '.data'
				 	+ @coresqlcmd ,
				@rsqlcmd2 = 
					N'delete t '
					+ @coresqlcmd2,
				@vsqlcmd2 = 
					'select ''delete rows'', ''' + @tablename+ ''',t.flagid,t.entityid,t.data'
					+ @coresqlcmd2


	   	
		IF @run = 'YES'
		BEGIN	
			EXEC sp_executesql @rsqlcmd,
				N'@RetainEntityId INT, @RemoveEntityId INT,@EntityTypeId INT',
				@RetainEntityId,@RemoveEntityId,@EntityTypeId
	
			EXEC sp_executesql @rsqlcmd2,
				N'@RetainEntityId INT, @RemoveEntityId INT,@EntityTypeId INT',
				@RetainEntityId,@RemoveEntityId,@EntityTypeId

			-- If the amend failed, take appropriate action:
			If @ErrCode <> 0
			 Begin
				print '*********************************************************************************'
				print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
				print '*********************************************************************************'
				Close ent
				Deallocate ent
			 End
		

		END
		ELSE
		BEGIN
			print '>>> The following actions will be performed - in this order - on this integer flag data tables :'
				if @trace = 1 print N'    ' + @vsqlcmd
				if @trace = 1 print N'    ' + @rsqlcmd
				if @trace = 1 print N'    ' + @rsqlcmd2
	

				EXEC sp_executesql @vsqlcmd,
				N'@RetainEntityId INT, @RemoveEntityId INT,@EntityTypeId INT',
				@RetainEntityId,@RemoveEntityId,@EntityTypeId
	
		END



	-- Get the next ..FlagData table name
	fetch next from ent into @TableName
end

close ent
deallocate ent

/*-------------------------------------------------------------------------------------------*
  END update and delete entries in integer tables this entity is a linkedEntity for
  2011-03-25 NYB LHID5511 
*-------------------------------------------------------------------------------------------*/



/*===========================================================================================*/
--                            		CATEGORY 3
-- Process tables with an EntityID column which can't be covered by the generic processes above
/*===========================================================================================*/


/* WAB removed all these 11/12/2006 dealt with by change in cursor in category 1
/*-------------------------------------------------------------------------------------------*/
-- EntityTracking
/*-------------------------------------------------------------------------------------------*/
If exists(select 1 from sysobjects where type = 'U' and name = 'EntityTracking')
 Begin
	print '***============ EntityTracking ============***'
	Set @TableName = 'EntityTracking'
			
	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@TableName      = @TableName,
			@ColumnName     = 'EntityID',
			@Run            = @Run,
			@FlagData 	= 0,
			@Trace		= @Trace

	-- If the amend failed, take appropriate action:
	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
	 	Goto ErrorTrap
	 End
 End

/*-------------------------------------------------------------------------------------------*/
-- PhraseList
/*-------------------------------------------------------------------------------------------*/
If exists(select 1 from sysobjects where type = 'U' and name = 'PhraseList')
 Begin
	print '***============ PhraseList ============***'
	Set @TableName= 'PhraseList'

	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@TableName      = @TableName,
			@ColumnName     = 'EntityID',
			@Run            = @Run,
			@FlagData 	= 0,
			@Trace		= @Trace

	-- If the amend failed, take appropriate action:
	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
	 	Goto ErrorTrap
	 End
 End

/*-------------------------------------------------------------------------------------------*/
-- SelectionTag
/*-------------------------------------------------------------------------------------------*/
If exists(select 1 from sysobjects where type = 'U' and name = 'SelectionTag')
 Begin
	print '***============ SelectionTag ============***'
	Set @TableName = 'SelectionTag'

	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,

			@TableName      = @TableName,
			@ColumnName     = 'EntityID',
			@Run            = @Run,
			@FlagData 	= 0,
			@Trace		= @Trace

	-- If the amend failed, take appropriate action:
	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
	 	Goto ErrorTrap
	 End
 End

/*-------------------------------------------------------------------------------------------*/
-- TempSelectionTag
-- WAB 12/7/05 not sure whether tempselectiontag should be done - we can't tell whether it is storing personid, locationids or whatever (although at the time of writing we only do person selections), but certainly currently should not run if table is not person.
--			probably not really necessary to dedupe this table at all in fact
-- 
/*-------------------------------------------------------------------------------------------*/
If @tablename = 'person'
Begin
If exists(select 1 from sysobjects where type = 'U' and name = 'TempSelectionTag')
 Begin
	print '***============ TempSelectionTag ============***'
	Set @TableName = 'TempSelectionTag'

	Exec @ErrCode = DedupeEntityAmendChild
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@TableName      = @TableName,
			@ColumnName     = 'EntityID',
			@Run            = @Run,
			@FlagData 	= 0,
			@Trace		= @Trace

	-- If the amend failed, take appropriate action:

	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
	 	Goto ErrorTrap
	 End
 End
End
/*-------------------------------------------------------------------------------------------*/
-- IntersectionEntity
/*-------------------------------------------------------------------------------------------*/
If exists(select 1 from sysobjects where type = 'U' and name = 'IntersectionEntity')
 Begin
	print '***============ IntersectionEntity ============***'
	Set @TableName = 'IntersectionEntity'

	Exec @ErrCode = DedupeIntersectionEntity
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID,
			@Entity 	= @Entity,
			@Run            = @Run,
			@Trace		= @Trace

	-- If the amend failed, take appropriate action:
	If @ErrCode <> 0
	 Begin
		print '*********************************************************************************'
		print '*** Dedupe abandoned because of fatal errors processing ' + @TableName
		print '*********************************************************************************'
	 	Goto ErrorTrap
	 End
 End
*/

/*===========================================================================================*/
--                            		CATEGORY 4
-- Tables with a RecordID column.
/*===========================================================================================*/

--***************** NO PROCESSING **********************************************************


/*===========================================================================================*/
-- Finally, remove the record to be deduped
/*===========================================================================================*/
print '***============ '+@Entity+' ============***'
if @Run = 'yes'
begin
-- print @RemoveEntityId
	select @rsqlcmd = N'delete ['+@Entity+
			N'] where ['+@Entity+N'Id] =  @RemoveEntityId'
	if @trace = 1 print N'    ' + @rsqlcmd
	EXEC sp_executesql @rsqlcmd,
		N'@RemoveEntityId INT',
		 @RemoveEntityId
	
	insert dedupeaudit(ArchivedID, NewID, RecordType ,Created,createdByPersonID)
	 	select @RemoveEntityId,@RetainEntityId,''+@Entity+'',getdate(),@createdByPersonID
end

/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION
ExitPoint:
SET NOCOUNT OFF
RETURN @ErrCode

/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:

If @@TranCount > 0 and @StartedTran = 1
   ROLLBACK TRANSACTION

GOTO ExitPoint