IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DedupeEntityCascadeDelete' and Type = 'P')
	DROP PROCEDURE DedupeEntityCascadeDelete

	
GO

CREATE     
proc [dbo].[DedupeEntityCascadeDelete]
			@TableName	sysname, 	-- Name of table from which rows are to be deleted
			@KeyListTable 	sysname,	-- Name of temp table holding key column values of rows to be deleted
			@Depth		int = 0,	-- Counter to track the level of nested calls to this proc
			@IsInPK		bit = 0,	-- Indicates whether the dedupe column is part of the PK in this table
							-- (only relevant for the first call from DedupeEntityAmendChild)
			@Run		varchar(5) = 'No',
			@Trace 		bit = 0		-- Switch to enable/disable printing of dynamic SQL commands

as
/***************************************************************************************
** Name   : DedupeEntityCascadeDelete
** Created: 21/Jun/2001
** By     : JVH
**
** Purpose: 
** Performs cascading deletes to tables where deletions are required in the top-level table
** in order to avoid duplicate rows which would otherwise be caused by deduping
**
** Arguments:
**      See above
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00	    21/Jun/2001  JVH  Ticket 5434:
**                            o Original
**
	 WAB 2009/10/18 problem with transaction being started if @run = 'No'

****************************************************************************************/

/*===========================================================================================*/
-- Initialisations
/*===========================================================================================*/
declare @vsqlcmd1 nvarchar(2000),	-- Dynamic SQL to show rows to be deleted
	@rsqlcmd nvarchar(2000),	-- Holds dynamic SQL as required
	@rsqlcmd0 nvarchar(2000),	-- Holds dynamic SQL fragments as required
	@rsqlcmd1 nvarchar(2000),	-- Dynamic SQL to perform the deletions from the table
	@rsqlcmd2 nvarchar(2000),	-- Dynamic SQL to define join between child table and keylist table
	@rsqlcmd3 nvarchar(2000),	-- Dynamic SQL to select key columns from child table
	@countcmd nvarchar(2000),	-- Command to count number of unprocessed child tables
	@indent nvarchar(100),		-- String used to indent printed output 


	@KeyColumnName sysname,	-- Name of key column in flag table
	@ChildTableName sysname, -- Name of child table to try deleting from
	@ChildKeyList sysname,	-- Name of temp table to hold keys of rows to be deleted from child table
	@ChildColName sysname,	-- Name of column from 'child' table
	@ParentColName sysname,	-- Name of column from 'parent' table (i.e. the table being deleted)
	@ChildCount int,	-- Number of unprocessed child tables
	@KeyCount int,		-- Number of rows in the Key List table
	@ChildList sysname,	-- Name of temp table holding list of child tables
	@ErrCode int,
	@DelCode int,		-- Use to capture the error code returned from the delete command
	@ColCount int,		-- Counter to control construction of SQL commands
	@StartedTran bit,		-- Indicates whether a transaction was started inside this proc
	@i int			-- Loop control variable

Set @ErrCode = 0
Set @StartedTran = 0
Set @Depth = @Depth + 1
Set @indent = N''

-- Set up the indent variable
set @i = 0
While @i < @depth-1
 begin
	Set @indent = isnull(@indent,N'') + '     '
	set @i = @i + 1
 end

-- If the depth of nesting has exceeded the number of user tables in the database, then we must have a
-- circular join path, in which case abort the process
If @Depth > (select count(id) from sysobjects where type = 'U')
 begin
   Set @ErrCode = 1
   Goto ErrorTrap
 end

-- If this proc wasn't called from inside a transaction, start a new one
-- WAB 2009/10/18 noticed that if this procedure was entered with @run=No then a transaction was created, best not to create transaction (as per dedupe person)
If @@TranCount = 0 and @run='Yes'
 begin
   set @StartedTran = 1
   BEGIN TRANSACTION
 end

SET NOCOUNT ON

/*===========================================================================================*/
-- Process the table
/*===========================================================================================*/
--print ''
if @depth > 1 print @indent + '***============ ' + @Tablename + ' ============***'

-- Check whether the keylist table actually contains any rows - if not, skip the rest of the proc
set @rsqlcmd = N'select @keycount = count(*) from ' + @keylisttable
exec sp_executesql @rsqlcmd, N'@keycount int output', @keycount = @keycount output

If @keycount = 0
 begin
	Print @indent + '>>>> NO ROWS TO DELETE <<<<'
	select @rsqlcmd = N'select 1 as ' + @tablename + '_keyList , * from ' + @keylisttable
	exec sp_executesql @rsqlcmd 
	Goto ErrorTrap
 end

set @rsqlcmd = ''

/*-------------------------------------------------------------------------------------------*/
-- Construct a command to perform the deletion, and the basis of one to generate a list of keys for the 
-- rows to be deleted from each child table
/*-------------------------------------------------------------------------------------------*/
Set @rsqlcmd0 =  N' from ' + @TableName + N' data '
		+ N' join ' + @KeyListTable + N' kl on '


-- Create a cursor to step through the Primary Key column
DECLARE PK_csr insensitive cursor
FOR	
	SELECT distinct kcu.Column_Name
	FROM	INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
	JOIN 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on kcu.Table_Name = tc.Table_Name
							and kcu.Constraint_Name = tc.Constraint_Name
							and tc.Constraint_Type ='PRIMARY KEY'
	WHERE 	kcu.Table_Name = @TableName

OPEN PK_csr

-- Loop through the PK cursor
WHILE 1=1
 BEGIN
	Fetch 	Next
	From	PK_csr
	Into	@KeyColumnName

	If @@Fetch_Status <> 0  BREAK

	Set @ColCount = isnull(@ColCount,0) + 1

	Set @rsqlcmd0 =	@rsqlcmd0 + 
			case when @ColCount > 1 then N' and ' else N'' end + 
			N'data.' + @KeyColumnName + N' = ' +
			N'kl.' + @KeyColumnName
 END

close PK_csr
deallocate PK_csr

Set @rsqlcmd1 = N'Delete data ' + @rsqlcmd0
Set @vsqlcmd1 = N'Select ''' + @tablename  +  ''' as dedupetablename, ''to be deleted'' as dedupeaction, data.* ' + @rsqlcmd0     -- WAB added 'to be deleted 'for debuggin 13/06/2007

if @Run <> 'Yes'
 begin
	print @indent + '>>> The following rows will be deleted:'
	If @Trace = 1 print @indent + @vsqlcmd1
	exec sp_executesql @vsqlcmd1

	if @@rowcount <> 0 and @Trace = 1
		begin
			set @vsqlcmd1 = 'select * from ' + @KeyListTable			
			exec sp_executesql @vsqlcmd1
		end	
 end

/*-------------------------------------------------------------------------------------------*/
-- Execute the delete command. If it fails with an RI violation, loop through any tables which 
-- have an FK constraint pointing at this one and do a cascade delete on them
/*-------------------------------------------------------------------------------------------*/
If @Run = 'Yes'
 begin
	print @indent + '>>> Deleting rows'
	If @Trace = 1 print @indent + @rsqlcmd1
	Exec sp_executesql @rsqlcmd1
	Set @DelCode = @@Error
--	Print @DelCode
 end

IF @DelCode = 547	-- RI violation
 or @Run <> 'Yes'
 BEGIN
	/*-------------------------------------------------------------------------------------------*/
	-- If it's a live run and the 'dedupe' column is in the PK, go no further - the child records
	-- may have been cleared by the time we come back to this table again
	/*-------------------------------------------------------------------------------------------*/
	If @IsInPK = 1 and @Run = 'Yes'
	 Begin
		Set @ErrCode = @DelCode
		Goto ErrorTrap
	 End

	/*-------------------------------------------------------------------------------------------*/
	-- Otherwise go ahead with the cascade delete
	/*-------------------------------------------------------------------------------------------*/
	Else
	 Begin
		/*-------------------------------------------------------------------------------------------*/
		-- Create and loop through a list of tables - don't use a cursor because this procedure calls itself
		-- recursively and if a cursor is used it'll cause a problem because the cursor will still exist when 
		-- the next instance of the proc tries to create its own copy.
		/*-------------------------------------------------------------------------------------------*/
		Set @ChildList = '##' + convert(varchar, @@spid) + @TableName + N'_Children'
		Set @rsqlcmd = 	N'Select distinct o1.name as TableName into ' + @ChildList  + 
				N' from	sysobjects o1 ' +
				N' join 	sysforeignkeys sfk on o1.id = sfk.fkeyid ' +
				N' join 	sysobjects o2 on sfk.rkeyid = o2.id ' +
				N' where o2.name = ''' +  @TableName + N''''
	
		exec sp_executesql @rsqlcmd			-- Create the table
	

		-- Count the number of rows
		set @countcmd = N'select @childcount = count(*) from '  + @ChildList
		exec sp_executesql @countcmd, N'@childcount int output', @childcount = @childcount output 

		While @childcount > 0 
		 begin

			-- Get each child table in turn
		 	set @rsqlcmd = N'set rowcount 1 select @ChildTableName = TableName from ' + @ChildList + N' set rowcount 0'
			exec sp_executesql @rsqlcmd, N'@ChildTableName sysname output', @ChildTableName = @ChildTableName output 

			set @rsqlcmd2 =  '' 

			/*-----------------------------------------------------------------------------------*/
			-- Construct & loop through a cursor of FK columns in the child table
			-- Use this to construct SQL to generate the list of PKs of child rows to be deleted
			/*-----------------------------------------------------------------------------------*/
			Declare ChildFKCol_csr	insensitive cursor
			 For
				select c1.name, c2.name		-- c1 is 'child', c2 is 'parent'
				from sysobjects o1
				join sysforeignkeys sfk on o1.id = sfk.fkeyid
				join syscolumns c1 	on o1.id = c1.id 
							and sfk.fkey = c1.colid
				join sysobjects o2 	on sfk.rkeyid = o2.id
				join syscolumns c2 	on o2.id = c2.id 
							and sfk.rkey = c2.colid
				where o2.name = @TableName
				and   o1.Name = @ChildTableName

			Open ChildFKCol_csr

			Set @ColCount = 0 
			While 1=1 
			 Begin
	
				-- Loop through the columns in the cursor and construct the join 
				-- between the parent and child tables
				Fetch next from ChildFKCol_csr into @ChildColName, @ParentColName
				If @@Fetch_Status <> 0 BREAK

				set @ColCount = @ColCount + 1	-- Keep track of how many columns have been found

				set @rsqlcmd2 = @rsqlcmd2 + 
						case when @ColCount > 1 then N' and ' else N'' end + 
						N'kl.' + @ParentColName + N' = ' +
						N'child.' + @ChildColName
		
			 End -- Loop through FK columns

			CLOSE ChildFKCol_csr
			DEALLOCATE ChildFKCol_csr

			/*-----------------------------------------------------------------------------------*/
			-- Construct & loop through a cursor of PK columns in the child table
			/*-----------------------------------------------------------------------------------*/
			Declare ChildPKCol_csr	insensitive cursor
			 For
				SELECT distinct kcu.Column_Name
				FROM	INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
				JOIN 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on kcu.Table_Name = tc.Table_Name
										and kcu.Constraint_Name = tc.Constraint_Name
										and tc.Constraint_Type ='PRIMARY KEY'
				WHERE 	kcu.Table_Name = @ChildTableName
	
			Open ChildPKCol_csr

			Set @rsqlcmd3 = N'Select '

			Set @ColCount = 0 
			While 1=1 
			 Begin

				-- Loop through the columns in the cursor and construct the join 
				-- between the parent and child tables
				Fetch next from ChildPKCol_csr into @ChildColName
				If @@Fetch_Status <> 0 BREAK

				set @ColCount = @ColCount + 1	-- Keep track of how many columns have been found

				set @rsqlcmd3 = @rsqlcmd3 + 
						case when @ColCount > 1 then N', ' else N'' end + 
						'child.' + @ChildColName
		
			 End -- Loop through PK columns

			CLOSE ChildPKCol_csr
			DEALLOCATE ChildPKCol_csr

			Set @ChildKeyList = N'##' + convert(varchar,@@spid) + N'_' + @ChildTableName + N'_KeyList'
			Set @rsqlcmd3 = @rsqlcmd3 + N' into ' + @ChildKeyList + N' from ' + @ChildTableName + N' child ' +
						    N' join ' + @KeyListTable + N' kl on '

			/*-----------------------------------------------------------------------------------*/
			-- Execute the command to populate the Child PK List table
			/*-----------------------------------------------------------------------------------*/
			Set @rsqlcmd = @rsqlcmd3 + @rsqlcmd2 
			Exec sp_executesql @rsqlcmd

			/*-----------------------------------------------------------------------------------*/
			-- Call DedupeEntityCascadeDelete to try deletions on the child table
			/*-----------------------------------------------------------------------------------*/
			print @indent + N'>>> Cascading Deletes to ' + @ChildTableName

			Exec @ErrCode = DedupeEntityCascadeDelete
							@TableName 	= @ChildTableName,
							@KeyListTable 	= @ChildKeyList,
							@Depth 		= @Depth,
							@Run 		= @Run,
							@Trace		= @Trace

			If @ErrCode <> 0
			 Begin
				-- Get rid of child list
				Set @rsqlcmd = N'Drop table ' + @ChildList 
				exec (@rsqlcmd)
			 	Goto ErrorTrap
			 end

			/*-----------------------------------------------------------------------------------*/
			-- Drop the Child Key List table
			/*-----------------------------------------------------------------------------------*/
			Set @Rsqlcmd = N'Drop table ' + @ChildKeyList
			exec sp_executesql @rsqlcmd		

			/*-----------------------------------------------------------------------------------*/
			-- Remove the table just processed from the list
			/*-----------------------------------------------------------------------------------*/
			-- Delete the table just processed from the list of children		
		 	set @rsqlcmd = N'Delete ' + @ChildList + N' where TableName = ''' + @ChildTableName + N''''
			exec sp_executesql @rsqlcmd

			-- Get the count of rows in the list of children
			exec sp_executesql @countcmd, N'@childcount int output', @childcount = @childcount output

		 End -- Loop through child tables

		/*-----------------------------------------------------------------------------------*/
		-- Tidy up
		/*-----------------------------------------------------------------------------------*/
		Set @rsqlcmd = N'Drop table ' + @ChildList 
		exec (@rsqlcmd)

		/*-----------------------------------------------------------------------------------*/
		-- Attempt the original delete again
		/*-----------------------------------------------------------------------------------*/
		If @Run = 'Yes'
		 begin
			print @indent + '>>> Deleting rows'
			Exec sp_executesql @rsqlcmd1
			Set @DelCode = @@Error

			-- If it still fails, abort the cascade delete
			If @DelCode <> 0 
			 begin
				print @indent + '****** Failed to cascade deletes in ' + @TableName + ' ************'
				Set @ErrCode = 1
				Goto ErrorTrap
			 end
		 end
	 End
 END


/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
SET NOCOUNT OFF
RETURN @ErrCode

/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:

If @@TranCount > 0 and @StartedTran = 1
   ROLLBACK TRANSACTION

GOTO ExitPoint
