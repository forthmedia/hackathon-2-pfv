/*
	2016/09/01 NJH JIRA PROD2016-125 - turn identity insert on/off
*/

if exists (select 1 from sysobjects where name = 'UndeleteLocationsFromTable')
	drop procedure UndeleteLocationsFromTable

GO

Create PROCEDURE [dbo].[UndeleteLocationsFromTable]
(
      @UndeleteLocationsTable UndeleteLocationsTableType READONLY,
      @UserGroupID int = 0,
	  @PersonID int = 0
)

AS

print 'Started'

declare @sqlerror int,
		@rcount int,
		@sql nvarchar(4000)
	
begin transaction

print 'Create temp tables for processing'

Select locationid,DoNotRecoverPersons into #locIdsNotInDelSet from @UndeleteLocationsTable

Select tl.locationid,tl.DoNotRecoverPersons,max(isnull(mr.moddate,getdate()-(365*50))) as moddate into #locIdsInDelSet 
	from #locIdsNotInDelSet tl 
	inner join locationdel ld on tl.locationid=ld.locationid
	inner join organisation o on ld.organisationid=o.organisationid
	left join modregister mr on tl.locationid=mr.recordid and action='LD' and mr.entitytypeid=1
	group by tl.locationid,tl.DoNotRecoverPersons


delete from #locIdsNotInDelSet where locationid in (select locationid from #locIdsInDelSet)

print 'Insert locationids in Deleted Set into live location table'

set identity_insert dbo.location on
insert location
(LocationID, Address1, Address2, Address3, Address4, Address5, PostalCode, CountryID, OrganisationID, SiteName, Telephone, Fax, FaxStatus, SpecificURL, Direct, Active, MatchName, MatchTel, Note, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualCountry, EmailDomain, R1LocID, R1ParentID, address7, address8, address9, nSiteName, nAddress1, nAddress2, nAddress3, nAddress4, nAddress5, CompanyLevel, SiteType, R2LocID, R2OrgID, Latitude, Longitude)
select distinct ld.LocationID, Address1, Address2, Address3, Address4, Address5, PostalCode, CountryID, OrganisationID, SiteName, Telephone, Fax, FaxStatus, SpecificURL, Direct, Active, MatchName, MatchTel, Note, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualCountry, EmailDomain, R1LocID, R1ParentID, address7, address8, address9, nSiteName, nAddress1, nAddress2, nAddress3, nAddress4, nAddress5, CompanyLevel, SiteType, R2LocID, R2OrgID, Latitude, Longitude
from locationdel ld 
inner join #locIdsInDelSet temp on ld.locationid=temp.locationid 
set identity_insert dbo.location off

select	@sqlerror = @@error,
	@rcount   = @@rowcount
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting Locations %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Delete locationids in Deleted Set from locationdel table'

delete locationdel where locationid in (select locationid from #locIdsInDelSet)

select	@sqlerror = @@error
	
if @sqlerror <> 0
 
begin 
   RAISERROR ('Error : %d deleting delete record for Location %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Create temp table of people to restore'

select personid,max(r.moddate) as moddate
into #pundel
from PersonDel p
inner join #locIdsInDelSet l on  p.locationid = l.locationid and l.DoNotRecoverPersons=0
inner join modregister lmr on lmr.recordid=l.locationid
				and lmr.action = 'LD' and lmr.entitytypeid=1
inner join modregister r on r.recordid=p.personid
				and r.moddate between dateadd(hh,-12,lmr.moddate)
						  and dateadd(hh,12,lmr.moddate)
				and r.action = 'PD' and r.entitytypeid=0
				-- and r.ActionByCF = @ActionByCF
group by personid


select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting person details into temporary table for Location %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Insert restored people'

set identity_insert dbo.person on
insert Person (PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, EmailStatus, JobDesc, Language, Notes, LocationID, OrganisationID, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualLocation, Title, Department, R1PerID, R1LocID, sex, initials, SecondName, nFirstName, nLastName, nTitle, emailStatusOld, R2PerId, R2LocId)
select distinct p.PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, EmailStatus, JobDesc, Language, Notes, p.LocationID, OrganisationID, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, DupeChuck, DupeGroup, ActualLocation, Title, Department, R1PerID, R1LocID, sex, initials, SecondName, nFirstName, nLastName, nTitle, emailStatusOld, R2PerId, R2LocId 
from PersonDel p 
inner join #pundel temp on p.personid=temp.personid
set identity_insert dbo.person off

select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting person rows for Location %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Delete restored people from PersonDel'

delete PersonDel where personid in(select personid from #pundel)
select	@sqlerror = @@error
	
if @sqlerror <> 0
 
begin 
   RAISERROR ('Error : %d deleting person delete record for Location %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Create temp table of Location Flags to restore'

select v.flagid,v.datatablefullname,r.recordid,v.entitytable,oldVal into #f 
	from #locIdsInDelSet l 
	inner join modregister r on r.recordid=l.locationid and r.action='FD' and r.moddate between dateadd(hh,-12,l.moddate) and dateadd(hh,12,l.moddate)
	inner join  vflagdef v on r.flagid=v.flagid and v.entitytable='location' and v.flagtextid != 'DeleteLocation'
	group by v.flagid,v.datatablefullname,r.recordid,v.entitytable,oldVal

print 'Add to temp table Person Flags to restore'

insert into #f 
	select v.flagid,v.datatablefullname,r.recordid,v.entitytable,oldVal 
	from #pundel p 
	inner join modregister r on r.recordid=p.Personid and r.action='FD' and r.moddate between dateadd(hh,-12,p.moddate) and dateadd(hh,12,p.moddate)
	inner join  vflagdef v on r.flagid=v.flagid and v.entitytable='person' and v.flagtextid != 'DeletePerson'
	group by v.flagid,v.datatablefullname,r.recordid,v.entitytable,oldVal


select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting flag rows into temporary table %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Restore flags based on temp table'

declare @datatable nvarchar(50), @datatype nvarchar(50), @created datetime;
select @created=getdate()
declare c CURSOR LOCAL FORWARD_ONLY OPTIMISTIC FOR
    select distinct datatablefullname from #f
open c
FETCH NEXT FROM c into @datatable 
	WHILE @@FETCH_STATUS = 0
	begin
		if @@FETCH_STATUS = 0
		begin
			if @datatable != 'booleanflagdata'
				begin
					select @datatype=data_type from information_schema.columns where table_name=@datatable and column_name='Data'
					if exists (select * from information_schema.columns where table_name=@datatable and column_name='Data' and character_maximum_length is not null)
						select @datatype=data_type+'('+cast(character_maximum_length as varchar(15))+')' from information_schema.columns where table_name=@datatable and column_name='Data'
				end
			set @sql = 'insert into '+@datatable+ ' (flagid,entityid,created,createdby,lastupdated,lastupdatedby'
			if @datatable != 'booleanflagdata'
				set @sql = @sql+',data'
			set @sql = @sql+') ' 
			set @sql = @sql+'select flagid,recordid,'''+convert(varchar(50),@created,21)+''','+cast(@usergroupid as varchar(50))+','''+convert(varchar(50),@created,21)+''','+cast(@usergroupid as varchar(50)) 
			if @datatable != 'booleanflagdata'
				set @sql = @sql+',cast(oldVal as '+@datatype+')'
			set @sql = @sql+' from #f where datatablefullname='''+@datatable+'''' 
			execute sp_executesql @sql
			FETCH NEXT FROM c into @datatable
		end
	end
close c
deallocate c


select	@sqlerror = @@error
if @sqlerror <> 0
begin 
   RAISERROR ('Error : %d inserting flag rows %d ',
      16, 1, @sqlerror)
   rollback transaction
   RETURN 1
end

print 'Insert locationdatasource'

insert locationdatasource (locationid,datasourceid,RemoteDataSourceID) 
select locationid,1,' ' from #locIdsInDelSet

select locationid,'Successful' as Result from #locIdsInDelSet
union
select locationid,'Unsuccessful' from #locIdsNotInDelSet

commit transaction
return 0
	
print 'Finished'
	

GO

