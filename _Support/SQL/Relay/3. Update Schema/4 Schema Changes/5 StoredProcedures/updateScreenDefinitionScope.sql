alter PROCEDURE [dbo].[updateScreenDefinitionScope] 
				@screenid int, 
				@sortorder int,
				@itemid int = 0  -- only needed for deletions 
AS

	/* 
		This procedure populates the countryScopeTable for a given line of 
		screenDefinition.  (A line in this case being any items which share screenid and sortorder)

		WAB 2015-02-25  Attempt at improving performance by using a temporary table
						Don't delete all and reinsert
	*/
	
	SET NOCOUNT ON

	/* temporary table to collect all the values */       
	DECLARE @tempCountryScope AS TABLE (
		entityID INT
		,countryid INT
	)
	
	
	-- insert lines into countryscope table where a country is defined in the scope field
	INSERT INTO @tempCountryScope (
		entityid
		,countryid
		)
	SELECT sd.itemid
		,sd.countryid
	FROM screendefinition AS sd
		,COUNTRY AS c
	WHERE screenid = @screenid
		AND sortorder = @sortorder
		AND active = 1
		AND sd.countryid = c.countryid
		--	and c.isocode <> ''   wab removed 18/12/00, so now also adds any regionids (countrygroupids), not often used but could be an issue with organisations with countryids set to regionIDs
	

 	/*
 	insert lines into countryscope table where a region is defined in the scope field
 	and there isn't already an entry for this country
	*/

	INSERT INTO @tempCountryScope (
		entityid
		,countryid
		)
	SELECT sd.itemid
		,countrymemberid
	FROM screendefinition AS sd
		,countrygroup AS cg
	WHERE sd.countryid = cg.countrygroupid
		AND screenid = @screenid
		AND sortorder = @sortorder
		AND active = 1
		AND countrymemberid NOT IN (
			SELECT CountryID
			FROM @tempCountryScope
			)

 	/*
 	 insert lines into countryscope table for all remaining countries where there is a 
	 0 in the scope field
	 also add a 0 for good measure (actually gets added automatically if countrygroup has a 0 - 0 record)
	*/ 

	INSERT INTO @tempcountryscope (
		entityid
		,countryid
		)
	SELECT sd.itemid
		,c.countryid
	FROM screendefinition AS sd
		,country AS c
	WHERE
		--	c.isocode <>''	and   WAB removed, now adds on regionids, (countrygroupids)
		screenid = @screenid
		AND sortorder = @sortorder
		AND active = 1
		AND sd.countryid = 0
		AND c.countryid NOT IN (
			SELECT CountryID
			FROM @tempCountryScope
			)
	
	UNION
	
	SELECT sd.itemid
		,0
	FROM screendefinition AS sd
	WHERE screenid = @screenid
		AND sortorder = @sortorder
		AND sd.countryid = 0
		AND NOT EXISTS (
			SELECT 1
			FROM @tempCountryScope
			WHERE entityid = sd.itemid
			)


	/* Delete all existing items which are not required */
	DELETE countryscope
	FROM countryscope cs
	LEFT JOIN @tempCountryScope t ON cs.entityid = t.entityID
		AND cs.countryid = t.countryid
	WHERE cs.entity = 'screendefinition'
		AND (
			cs.entityid IN (
				SELECT itemid
				FROM screendefinition
				WHERE screenid = @screenid
					AND sortorder = @sortorder
				)
			OR cs.entityID = @itemid
			)
		AND t.countryid IS NULL
		
		
	/* insert any missing items */
	MERGE INTO countryscope AS target
	USING @tempCountryScope AS source
		ON target.entity = 'screendefinition'
			AND target.entityid = source.entityid
			AND target.countryid = source.countryid
	WHEN NOT MATCHED BY target
		THEN
			INSERT (
				entity
				,entityid
				,countryid
				,entityTypeID
				)
			VALUES (
				'screendefinition'
				,entityid
				,countryid
				,(
					SELECT entityTypeID
					FROM schemaTable
					WHERE entityName = 'screenDefinition'
					)
				)

;	







