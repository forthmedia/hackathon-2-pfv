createPlaceHolderObject 'procedure', 'setRecordRights'

GO

ALTER     PROCEDURE [dbo].[setRecordRights]
	  @entity sysname
	, @recordid int
	, @level int
	, @userGroupList varchar(max) = null   -- if left null then this procedure can be used to just update the AND and NOT
	, @groupAnd bit = null
	, @groupNot bit = null
	, @previoususerGroupList varchar(max) = '' -- optional, allows code to only remove items in this list rather than all items against this record
	, @userGroupsAdded varchar(max) = '' OUTPUT 
	, @userGroupsRemoved varchar(max) = '' OUTPUT 
	, @UpdatedByPerson int 
AS


BEGIN
	declare 
		@entityTypeID int,
			@comma char(1) = ''
	
	select 
		@entityTypeID = case 
							when isNumeric(@entity) = 1 then @entity 
							else (select entityTypeID from schemaTable where entityName = @entity)
						end	
	
	declare @result as table (action varchar(10), entityid int, usergroupid int, level int)

	IF @userGroupList is not null
	BEGIN
	
		MERGE recordRights As rr
		USING 	
			(select 
				@entityTypeID, @recordid, power(2,@level-1), ug.usergroupid
			from
				dbo.csvToTable(@userGroupList) as users	
					inner join
				usergroup ug on ug.usergroupid = users.value
			) as source (entityTypeID, recordID, permission, usergroupid)
		ON 	rr.entityTypeID = @entityTypeID and rr.recordID = @recordID	and source.usergroupid = rr.usergroupid	

		WHEN NOT MATCHED and source.usergroupid is not null
			THEN INSERT (entityTypeID, recordid, permission, usergroupid, lastupdated, lastUpdatedByPerson)
				 VALUES (@entityTypeID, @recordid, power(2,@level-1), source.usergroupid, getdate(), @UpdatedByPerson)

		WHEN NOT MATCHED BY SOURCE	AND rr.entityTypeID = @entityTypeID 
									and rr.recordID = @recordID	
									and rr.permission = power(2,@level-1) 
									and (@previoususerGroupList = '' or dbo.listFind(@previoususerGroupList,rr.userGroupID) <> 0)
			THEN DELETE
			
		WHEN NOT MATCHED BY SOURCE	AND rr.entityTypeID = @entityTypeID 
									and rr.recordID = @recordID	
									and rr.permission & power(2,@level-1) <> 0 
									and (@previoususerGroupList = '' or dbo.listFind(@previoususerGroupList,rr.userGroupID) <> 0)
			THEN UPDATE 
				SET 
					permission = rr.permission - power(2,@level-1)
					,lastupdated = getdate ()
					,lastUpdatedByPerson = @UpdatedByPerson

		WHEN MATCHED AND rr.permission & power(2,@level-1) = 0 
			THEN UPDATE 
				SET 
					permission = rr.permission + source.permission
					,lastupdated = getdate ()
					,lastUpdatedByPerson = @UpdatedByPerson

		
		OUTPUT case when $action = 'update' then case when deleted.permission & power(2,@level-1) = 0 then 'insert' else 'delete' end else $action end, isNull(inserted.recordID, deleted.recordID), isNull(inserted.usergroupid, deleted.usergroupid), @level  into @result
		;


		select 
			@userGroupsAdded = isNull (@userGroupsAdded,'') + @comma + convert(varchar,usergroupid)
			, @comma = ','
		from 
			@result
		where
			action = 'insert'	

		set @comma = ''			
		select 
			@userGroupsRemoved = isNull (@userGroupsRemoved,'') + @comma + convert(varchar,usergroupid)
			, @comma = ','
		from 
			@result
		where
			action = 'delete'	
						

	END

	IF @groupAnd is not null or @groupNot is not null
	BEGIN
		update RecordRightsOperator
		set		
			  groupAnd +=  case 
								when @groupAnd = 1 and groupAnd & power (2,@level -1) = 0 then power (2,@level -1)
								when @groupAnd = 0 and groupAnd & power (2,@level -1) <> 0 then - power (2,@level -1)
								else 0
							end		
			, groupNot +=  case 
								when @groupNot = 1 and groupNot & power (2,@level -1) = 0 then power (2,@level -1)
								when @groupNot = 0 and groupNot & power (2,@level -1) <> 0 then - power (2,@level -1)
								else 0
							end		
		where 
				entityTypeID= @entityTypeID
			and recordid = @recordID
	END
	
END	
	
