IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'addNewPhraseAndTranslation' and Type = 'P')
	DROP PROCEDURE addNewPhraseAndTranslation

GO

/* WAB 2015-10-22 quoted_identifier must be on when creating this procedure because of indexed views on the phrase table*/	
SET QUOTED_IDENTIFIER ON

GO


	
CREATE     Procedure [dbo].[addNewPhraseAndTranslation]
		@phraseTextID		varchar(100) ,
		@entitytypeid		int = 0,
		@entityid			int = 0,
		@LanguageID			int = 1,
		@countryID			int = 0,
		@phraseText			nvarchar(max), 
		@updateMode			bit = 1,
		@onlyUpdateUnedited bit = 0,
		@updateTime dateTime = null,
		@lastUpdatedBy		int = 0
		
as
set nocount on


/*
Version History
1.0

WAB 2016-01-14 	PROD2015-513  Add @onlyUpdateUnedited feature.  
				This is for use when updating system phrases so that we only update those system phrases which have not be specifically edited on the given site.
				TBD - Not exactly sure what to do about dates.  Considering making it so that all system phrases are given same creation date - say 2000-01-01    

*/


	declare 
		@phraseExists int = 1, 
		@phraseAdded int = 0, 
		@translationExists bit , 
		@translationAddedUpdated int = 0, 
		@thisphraseID int,
		@ident int,				
		@translationIsUnchanged bit,
		@notUpdatedBecauseEdited bit = 0

		IF @updateTime is null
		BEGIN
			IF @onlyUpdateUnedited = 1
				set @updateTime = '2000-01-01'
			ELSE
				set @updateTime = getdate()
		
		END


	/* look for phrase in phraseList? */
		SELECT @thisPhraseID = phraseID 
				FROM phraseList 
					WHERE phraseTextID =  @phraseTextID 
					AND entitytypeid = @entitytypeid 
					AND entityid = @entityid 


	IF @@rowcount = 0   /* ie no entry in phraseList */
	
		BEGIN
			select @phraseExists = 0
				/*  then add one */
			SELECT @thisPhraseID=max(PHRASEID)+1 from phraseList
			
			INSERT INTO PhraseList
				(Phraseid, PhraseTextID, createdby, created, entitytypeid, entityid)
			VALUES (@thisPhraseID, @phraseTextID, @lastUpdatedBy, 					
				@updateTime, @entitytypeid, @entityid)

			select @phraseAdded = 1
		
		END


	/* check for translation */
	SELECT 
		@ident = p.ident,
		@translationIsUnchanged = case when p.created = p.lastupdated then 1 else 0 end
		
	from 
		PhraseList pl 
			INNER JOIN 
		phrases p ON pl.phraseID = p.phraseID
	where 
		pl.phraseid = @thisphraseid
		and p.languageid = @LanguageID
		and p.countryid = @countryID


	set @translationExists = isnull (@ident,0)

	/*  if we don't have a translation in phrases add one */
	IF @translationExists = 0 
		BEGIN
			  /* do an insert */
			INSERT INTO Phrases
			(Phraseid, PhraseText, LanguageID, CountryID, createdby, created, lastupdatedby, lastUpdated)
			VALUES (@thisPhraseID, @phraseText, @LanguageID, @countryID, 
				@lastUpdatedBy, @updateTime, @lastUpdatedBy, @updateTime)
			
			select 
				@ident = scope_identity(),
				@translationAddedUpdated = 1	

		END
	ELSE
		BEGIN
		
			IF(@updateMode = 1 and (@onlyUpdateUnedited = 0 OR @translationIsUnchanged = 1))
				BEGIN
					
					UPDATE phrases 
						SET 
							PhraseText =  @phraseText ,
							/* If we are only updating unchanged items 
								(which we detect by the created being the same as the lastudpdated) 
								then we need to keep the created and lastupdated in synch so that future updates will work */
							created = created,
							createdby = createdby ,
							lastUpdatedBy = @lastUpdatedBy, 
							lastUpdated = case when @onlyUpdateUnedited = 1 then lastupdated else @updateTime end
						WHERE 
							ident = @ident
							and phraseText  <> @phraseText  COLLATE SQL_Latin1_General_Cp1_CS_AS
							
					select @translationAddedUpdated = @@rowcount					

				END
			ELSE IF (@updateMode = 1 and @onlyUpdateUnedited = 1 AND @translationIsUnchanged = 0)	
			BEGIN
				set @notUpdatedBecauseEdited = 1
			END
			
		END
	
	select  
		@phraseExists  as phraseExists, 
		@phraseAdded as phraseAdded, 
		@translationExists as translationExists, 
		@translationAddedUpdated as translationAddedUpdated,
		@notUpdatedBecauseEdited as notUpdatedBecauseEdited,
		ident as ident,
		defaultForThisCountry = defaultForThisCountry 
	from 
		phrases
	where 
		ident = @ident 


