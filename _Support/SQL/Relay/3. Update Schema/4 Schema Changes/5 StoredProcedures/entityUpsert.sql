createplaceholderobject 'Procedure', 'entityUpsert'

GO

/* BF-947 introduced a dependency */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'isnumeric' AND xType='FN')
BEGIN
	RAISERROR ('entityUpsert is dependent upon dbo.isnumeric Function.  Please create this first', 16, 16)
END	

GO
ALTER PROCEDURE [dbo].[entityUpsert]
	@upsertTableName sysname  ,
	@entityName sysname,					/* name of the table to be updated */
	@lastUpdatedByPerson int = null,		/* when used for updating a view, the lastupdatedbyperson column must be SET in the update query.  otherwise pass in a value here */ 
	@columns_Updated varbinary (50) = -1,
	@validate bit = 0,
	@processNulls  varbinary (50) = -1,  	/* Whether nulls are processed. A bitmask.  -1 == all columns, 0 == no columns, use dbo.columnListToBitmask() to generate column specific bitmask */
	@truncate  varbinary (50) = 0,  		/* Whether varchar fields are automatically truncated. A bitmask.  -1 == all columns, 0 == no columns, use dbo.columnListToBitmask to generate column specific bitmask */
	@whereClause nVarchar(200) = '',  		/* a where clause to restrict the rows to be processed.  Note that the where clause cannot include 'identityColumn is not null' */
	@useTemporaryTable bit = 0 ,  			/* when processing flags I do a cross join to get all the columns of the same type into a single column. I usually put the results into a temp table for better performance (particularly on boolean groups), but when debugging it can be easier to be able to run a single query without the temp tables */
	@numberOfFlagColsToProcessSimultaneously int = 50,
	@returnTableName sysname = '',			/* if specified then the result is returned in a table rather than as a select statement.  Table can exist (must have correct columns, probably in correct order) or you can leave it to be created */
	@debug int = 0, 						/* 1 (limited debug) or 2 (query strings output) */
	@rows int = null,
	@suppressResult bit = 1,				/* suppress the output given by this sp unless specifically asked not to, to avoid problems in triggers, etc.*/
	@debugPerformance bit =   @debug ,  
	@triggerInsert bit = null
AS 

	/*   
	A stored procedure for 
		either	updating/inserting into views made up of a mixture of tables and flags
		or		upserting data from a dataload table/view
		
	The names of the columns are the base column names or the flagTextIDs or the flagGroupIDs


	Author WAB  2014

	NOTE:  IF YOU UPDATE THIS SP, PLEASE TEST BY RUNNING \tests\sql\entityUpsert\entityUpsert_TestHarness.sql

	Mods
		2014-06-19 	WAB Fix problem when lastupdatedbyperson was not an internal user (ie no matching record in the usergrouptable). Ended up trying to put null values into createdBy/updatedBy fields   
		2014-06-30	WAB Was demanding that createBy/lastudpatedBy columns were provided for an insert.  Also a typo fix
		2014-09-30  WAB Deal with cartesian join 
		2014-11-11	WAB	Major refactoring (but unit tested!).  
						Started when add support for blank fields being used to null out date or numeric fields						
						Got rid of a cursor or two, build SQL snippets in the #updatedColumns table and then concatenate
						Combined dataType validation into single cursor
						Support for flags and flagGroups without textIDs
						Foreign Key validation does not fall over if also has dataType Error (slightly unusual case, but appeared in unit test)
		2014-11-17	WAB	Fix 2 problems relating to fix of 2014-09-30 (when generating identities for POL tables)				
		2015-01-08	WAB	Add support for boolean columns containing True,false,Yes,no			
						Problem with inserting boolean flagGroup fields when field is blank	and some of the flags in the group did not have flagtextids					
		2015-01-19	WAB	Problem if a boolean flag name with an & in it.  Added calles to be dbo.XMLformat function.
		2015-01-28	WAB	Performance improvements - in particular don't get foreign key information unless doing validation
		2015-03-24	WAB	Problem with modRegister not showing lastUpdatedBy correctly on flag deletes.  Was caused by use of a static date rather than getdate().  Removed @theDate variable
		2015-04-13	WAB Add support for conversion of Exponent format to decimalFlags.  And some other changes in the same area.
		2015-04-14	WAB	Add support for multiple data type checks per column (specifically for testing numeric data type and length). Involved a major reworking of that section of the code
						Fixed validation issue with integerMultiple - didn't like blank column
		2015-04-15	WAB	Validation of unique indexes was not filtered by the columns in the upsert table	
		2015-04-27	WAB	Validation of unique indexes did not take into account indexes containing filters
		2015-04-28	WAB Validation of integer flags was not working
		2015-05-11	WAB Add validation step to look for duplicate records (ie duplicate entityids)
		2015-05-12	WAB When opening a transaction SET XACT_ABORT ON, so that transaction will rollback if query times out or is aborted
		2015-05-12	WAB	Alter handing of Radio Groups (use slightly different code than checkbox code, because do not need to use XML to convert list to separate rows)
		2015-06-08	WAB	Maxing out at 96 columns in incoming query due to length of @binaryTrue variable.  Have increased to deal with 1024 columns.  Can't work out a way to make it dynamic
		2015-09-15	WAB Test for integer/numeric fields being too large failed when incoming data was nvarchar (but worked with varchar).  Recrafted test into a Case statement to ensure short circuiting in cases when incoming data was not even numeric.
		2015-09-20	WAB	While working on performance improvements also I made the following changes.  
						I am committing them in advance of the performance changes so that direct performance comparisons can be made.
						Add test for @lastUpdatedByPersonID being a valid personid
						Add validation of integerMultipleFlags (both testing numeric and testing for foreign keys)
						Altered all debug statements to deal with SQL commands longer than 4000 characters
						Adding some debugging specifically for doing performance testing
						Dealt with problem where flags and flagGroups have the same textIDs.  
		2015-09-30	WAB	Perfomance improvements.  Process all flags of the same type in a single query by using an UNPIVOT approach (actually using cross apply)
		2015-10-05	WAB	Fix issues in above changes - 
						Error updating numeric/date flags when incoming field of numeric/date type
						Code was not deleting individual instances of multiple flags
		2015-11-23	WAB	Fix error when updating integer flags where incoming columns are a mixture of char and numeric fields.  Need to do the updates separately
		2015-12-18 	WAB BF-87 When checking inserts for NOT NULL columns which are not in the incoming table, return a row for each item to be inserted rather than a single row
		2016-01-15	WAB BF-295 Updating of linkedEntity using a IS_xxxxx field was not working.  
							a) I have managed to stop the code running - introduced during performance improvements
							b) It has never worked properly. It could set and unset but not do an update
		2016-05-25	WAB	BF-946 Problem updating decimal field from decimal column.  Losing decimal places.  (only decimal on base table not decimal flag.  does not affect updating decimal from a varchar field.  Was doing a cast with no precision/scale
		2016-05-25	WAB	BF-947 Numbers containing commas did not fail validation.  Have had to implement own isNumeric function
		2016-07-08	WAB PROD2016-1367  (RingCentral).  Problem inserting new organisation where organisationid column is varchar: "Data type varchar of receiving variable is not equal to the data type int of column 'organisationID'"
		2016-07-05	WAB	PROD2016-2427 Validation was not picking up invalid boolean flags (has been a problem since performance improvements of 09/2015)
						Add validation of boolean flags during an update
		2016-07-05	WAB	PROD2016-2428 Do not attempt to update a computed column
		2016-07-05	WAB	PROD2016-2458 Fixed a problem with linkedEntity flags and IS_xxxxx field.  Make sure that id of the linked entity exists in the incoming view
		2016-10-17	WAB During PROD2016-1303 Support for isBitField in views.  More dealing with cases where flagGroupTextID and flagID are the same (which can be a problem on a boolean).  If the incoming column is a bit then it must match the flag not the flagGroup
		2016-11-13	WAB	Deal with identity Inserts
		2016-11-14	WAB	Alter debugging so that has 2 levels.  1: just messages, 2:include SQL statements.
						When debug:2, do not delete temporary tables
		2016-12-13	WAB	PROD2016-2957 More code to deal with flagGroups and flags with same textids - in particular booleans with isBitField=true
		2016-12-13	WAB	Deal with flag/flagGroup tables getting a lastUpdatedBy column - added some aliasing
		2016-12-13	WAB Fix longstanding (but unnoticed in the wider world) error when validating integerMultiple data (a required temporary table was not there)
		2016-12-13	WAB	Added Support for createdByPerson (all entity tables must now have it)
		2017-02-01	RMP	PROD2016-2981 Changed error message to standardise errors and status code
	*/ 
	
	SET nocount on 

	DECLARE	
		@ColName	varchar(80),    /* column name (in cursor) */ 
		@tableType varchar(30),		/* base, flaggroup or flag (in cursor) */ 
		@flagOrFlagGroupId int,		/* either a flagID or flagGroupID (depending upon @tableType) (in cursor) */ 
		@hasSetToNullColumn bit,	/* indicates whether a column has a related column name in the form _setToNull.  Used to force fields to null - usually nulls in columns are ignored (in cursor) */
		@flagid varchar(10),
		@flaggroupid varchar(10),
		@flagtype varchar(30),
		@flagtable varchar(30),
		@requiredForInsert	bit,
		@notNullable	bit,
		@defaultValue	nvarchar(max),
		@processNull bit,
		@truncatedLength int,
		@castColumn nvarchar(max),
		@processWhen nvarchar(max),
		@columnBeforeCasting nvarchar(max),
		@dataTypeCheck nvarchar(max),
		@dataTypeErrorMessage varchar(30),
		@dataTypeCheckAnderrorMessage nvarchar(max),
		@listAsXML XML,
		@fieldname sysname,
		@rest varchar(80),
		@basesqlcmd		nvarchar(max),
		@basesqlcmdInsert		nvarchar(max),
		@basesqlcmdDelete		nvarchar(max),
		@joinON		nvarchar(max),
		@deleteWhen		nvarchar(max),
		@updateWhen		nvarchar(max),
		@insertWhen		nvarchar(max),	
		@sqlcmd		nvarchar(max),
		@pointer1 int,
		@pointer2 int,
		@updateWhereClause nVarChar(max) = '',
		@setClause nVarChar(max) = '',
		@requiredValidateClause nVarChar(max) = '',
		@colList nVarChar(max) = '',
		@colListWithAlias nVarChar(max) = '',
		@isView bit = 0,	
		@errorCode int ,		
		@entityTypeID int ,	
		@CR  char(1) = char(10),
		@updatedByPersonClause nVarChar(max) ,
		@updatedByClause nVarChar(max) = '',	
		@setCRUDClause nVarChar(max),
		@insertCRUDClause nVarChar(max),
		@insertCRUDCols nVarChar(max),
		@entityIDColumn nVarChar(100),
		@tempTableName sysname,
		@upsertUniqueIDColumnName sysname,
		@crossApplyStatement nVarChar(max), 
		@joinFlagToCrossApply nVarChar(max),
		@count int,
		@startTime datetime = getdate(),
		@flagOrFlagGroup sysname,
		@commaSeparator char (5),
		@ORSeparator char(5),
		@isList bit = 0,
		@invalidFlags nvarchar(max) = '',
		@msg nvarchar(max) = ''

	IF (@debug > 0  or @debugPerformance > 1)	print @cr+@cr+@cr+ 'Start entityUpsert'
	
	SELECT 
		@entityIDColumn=uniqueKey, @entityTypeID = entityTypeID  FROM schemaTable WHERE entityName=@entityName
	
	/*  In general entities need to be in the schema table, 
		but for unit testing it is useful to be able to use tables outside of the relayware environment (although won't deal with flags)
		So if not found in the schema table we can SET the entityIDColumn by convention.  Won't be able to do flags
		TBD Actually could query for identityColumn
		
	*/
	IF @entityIDColumn is null
		SET @entityIDColumn = @entityName + 'id'
	
	DECLARE 
		@entityIDColumnClause nVarChar(100) =  'i.' + @entityIDColumn,
		@baseEntityIDColumnClause nVarChar(100) =  'baseentity.' + @entityIDColumn
		
	DECLARE 	
		@BaseEntityON nVarChar(max) = @baseEntityIDColumnClause + ' = ' + @entityIDColumnClause

	DECLARE 	
		@JoinToBaseEntity nVarChar(max) =  ' inner join ' +  @entityName +  ' baseentity on ' + @BaseEntityON
	
	DECLARE 
		@filteredUpsert	nvarchar(max) = @upsertTableName


	/* Create a temporay table for results, unless it already exists */	
	IF object_id('tempdb..#results') is null
		create table #results (action sysname, entityID int, upsertUniqueID varchar(50))  /* although we use an integer for the upsertUniqueID which we put in the upsertTable, IF  this column is already present it may be varChar (eg a salesforceid) */

	/* When running validation we put validation results into another temporary table, so create that */	
	IF  @validate = 1
		create table #validateResult (entityID int, upsertUniqueID varchar(50), field sysname, message nvarchar(max), value nvarchar(max))


	/* If a where clause has been passed in then we prepare a clause to select the correct records */
	IF @whereClause	<> '' OR @rows is not null
	BEGIN
		SET @filteredUpsert = '(Select '  
									+ CASE WHEN @rows is not null then ' top ' + convert(varchar,@rows) else '' end + 
								' * 
									from ' + @upsertTableName 
									+ case when @whereClause	<> '' then '  where ' +  @whereClause  else '' end
									+	') '
	END

	/*  If we are not already inside a transaction then open one
		WAB 2015-05-12 The SET XACT_ABORT ON makes sure that if the process times out (or is cancelled by say query analyser) then the transaction is rolled back
	 */
	DECLARE @TransactionCountOnEntry int = @@TranCount
	IF @TransactionCountOnEntry = 0 AND @validate = 0
	BEGIN
		BEGIN TRANSACTION
		SET XACT_ABORT ON
	END	

	BEGIN TRY
		

	/* check number of rows to process, can exit quickly if none */
	SET @sqlCMD = 'Select @count = count(1)  from ' + @filteredUpsert + ' as x'
	EXEC sp_executeSQL @sqlCMD, N'@count int OUTPUT',@count OUTPUT

	IF @count = 0
		GOTO TIDYUPANDRETURN


	IF (@debug > 0)	print '@filteredUpsert: ' + @filteredUpsert

	/* A bit of a hack to deal with -1 being passed in to varBinary parameters.  
			@truncate
			@processNulls
			@columns_Updated
		-1 will be converted to 0xFFFFFFFF
		But we really need it to be a much longer binary, eg 0xFFFFFFFFFFFFFFFFFFFFFFFF,  to deal with the maximum number of columns that there mightbe in a table 
		2015-06-08	WAB	needs to be longer (was on 24 bytes long) increased to 128 bytes == 1024 columns
	*/
	DECLARE @binaryTrue varbinary(64) = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF  -- this is 16 bytes long = 128 columns
	select @binaryTrue += @binaryTrue, @binaryTrue += @binaryTrue, @binaryTrue += @binaryTrue  -- this line quadruples the length to 128 bytes = 1024 columns (perhaps neater than counting out 128 Fs)

	IF @truncate = -1
		SET @truncate = @binaryTrue
	IF @processNulls = -1
		SET @processNulls = @binaryTrue
	IF @columns_Updated = -1
		SET @columns_Updated = @binaryTrue

	/* Lastupdatedbyperson can come either from the upsertTable, or from the @lastUpdatedByPerson parameter deal with these two instances 
		2014-06-19 WAB Failing if lastUpdatedByPerson was not also a user in the usergroup table (ending up with a null value to be put into the lastupdated column).  
		2015-09-28  WAB Raise and error if lastUpdatedByPerson is not a valid personID 
	*/
	
	IF @lastUpdatedByPerson is null
	BEGIN
		SELECT	@updatedByPersonClause = ' i.lastupdatedbyperson ',
				@updatedByClause = ' isnull((select userGroup.usergroupID from userGroup  where personid = i.lastupdatedbyperson),0) '
	END
	ELSE
	BEGIN
		SELECT	@updatedByPersonClause = convert(varchar,@lastUpdatedByPerson),
				@updatedByClause = convert(varchar, isnull(usergroupID,0)) 
		FROM
			 person p 
				left join 
			userGroup ug on p.personid = ug.personid
		WHERE
			 p.personid = @lastUpdatedByPerson	

		IF @updatedByPersonClause is null 
		BEGIN
			RAISERROR ('Invalid Value for @lastUpdatedByPerson',16,16)
			RETURN 1
		END	

	END


	/* prepare some statements for doing updates and inserts of CRUD columns */
	SELECT	
		@setCRUDClause = 'lastupdated = getDate() ,lastupdatedbyperson = ' + @updatedByPersonClause + ' ,lastupdatedby = ' + @updatedByClause ,
		@insertCRUDCols =	'created,createdby,createdbyPerson,lastupdated,lastupdatedby,lastupdatedbyperson',
		@insertCRUDClause =  'getDate() , ' + @updatedByClause + ' , ' + @updatedByPersonClause + ' , getDate() , ' + @updatedByClause + ' , ' + @updatedByPersonClause 

	

	/* work out whether the upsert is a view or not, bit awkward because it may or may not be a temporary table */
	IF left(@upsertTableName,1) = '#' 
	BEGIN
		SELECT @isView = CASE WHEN exists (select 1 from tempdb.sys.objects  where object_id = object_id('tempdb..' + @upsertTableName) and type = 'V') then 1 else 0 end
	END
	ELSE
	BEGIN
		SELECT @isView = CASE WHEN exists (select 1 from sys.objects  where object_id = object_id(@upsertTableName) and type = 'V') then 1 else 0 end
	END



		/* 
			If we have been passed a view then copy into temporary table
			Improves performance and allows me to add the upsertUniqueID column for inserts
			However I can't use a real temporary table because if I create it in exec() then it is in the wrong scope
		 */
		IF @isView = 1 
		BEGIN
			SELECT
				 @tempTableName = '##entityUpsert_filteredData' + replace(newID(),'-','')
			
			SELECT 
				@sqlCMD = 'Select * into '  + @tempTableName + ' from ' + @filteredUpsert + ' as x',
				@upsertTableName = @tempTableName,
				@filteredUpsert = @tempTableName
			
			EXEC (@sqlCMD)
			
		END



		/*  add an upsertUniqueID column to the incoming table 
			originally only added when inserts were required, but seems quite useful anyway (may reconsider)
			Note this code also exists in upsertEntity_handler
		*/
		SELECT 
			@upsertUniqueIDColumnName = name 
		FROM
			(select name from sys.columns c where object_id = object_id( @upsertTableName) and (name = 'upsertUniqueID' or is_identity = 1)
				union
			select name from tempdb.sys.columns c where object_id = object_id( 'tempdb..' + @upsertTableName) and (name = 'upsertUniqueID' or is_identity = 1)	
			) as x
			
		
		IF @upsertUniqueIDColumnName is null 
		BEGIN
		  SET @sqlCmd = 'alter Table ' +  @upsertTableName+ '  add  upsertUniqueID  int identity(1,1)'
		  EXEC sp_executeSQL @sqlCmd 
		  SET @upsertUniqueIDColumnName  = 'upsertUniqueID'	
		END

		SET @upsertUniqueIDColumnName  = 'i.' + @upsertUniqueIDColumnName
		

		/* 
			Create a table variable containing all the columns in the upsert table 
			Again, awkward because of differences between temporary and real tables
		*/
		DECLARE @viewColumns table (colid int, name sysname,dataType sysname, length int, isNumericOrDateType bit)
		IF left(@upsertTableName,1) = '#' 
		BEGIN
					insert into @viewColumns (colid, name, dataType, length,isNumericOrDateType)
					SELECT 
						column_id as colid,
						c.name,
						t.name as dataType,
						c.max_length,
						case when t.precision <> 0 then 1 else 0 end
					FROM 
						tempdb.sys.columns c
							inner  join
						sys.Types t on t.system_type_id = c.system_type_id and t.name <> 'sysname'
					where object_id = object_id('tempdb..' + @upsertTableName)

				
		END
		ELSE
		BEGIN
					insert into @viewColumns (colid, name, dataType, length,isNumericOrDateType)
					SELECT 
						column_id as colid,
						c.name,
						t.name as dataType,
						case when t.name IN ('nvarchar','nchar') and c.max_length <> -1 then c.max_length/2 else c.max_length end as length,
						case when t.precision <> 0 then 1 else 0 end
					FROM 
						sys.columns c
							inner join
						sys.Types t on t.system_type_id = c.system_type_id and t.name <> 'sysname'
					where object_id = object_id(@upsertTableName)
		
		
		END

		/* 
			Create a temporary table containing all the colums which have been updated 
			based on the value of @columns_UPDATED 
			Also collects together lots of metadata about those columns
		*/

		SELECT 
			DISTINCT  /* can get duplicate rows from checking constraints  */
			col.name, 
			col.dataType as upsert_dataType,
			col.length as upsert_length,
			col.isNumericOrDateType,
			t.character_maximum_length ,
			CASE WHEN SUBSTRING(@processNulls,byte,1) & power(2,bit-1) > 0 THEN 1 ELSE 0 END  as processNull,
			CASE WHEN SUBSTRING(@truncate,byte,1) & power(2,bit-1) > 0 AND t.data_type like '%char%' AND t.character_maximum_length > 0 THEN t.character_maximum_length ELSE 0 END  as truncatedLength,

			/* WAB 2015-02-04 	Change way casting is generated so that can support multiple casting (which is needed to get from string to integer.  
								Now create whole cast expression using FIELDNAME as a placeholder to be replaced 
				WAB 2015-04-13  Add conversion for decimal flags (couldn't handle exponent format).  
								Also added support for all 'int' types and decimal and money.
								And the numeric was truncating to integer (although a separate case had been added for numeric I had not removed it from the earlier case  
				WAB 2015-09-20  Deal with situations where flagGroupTextID and flagTextID are the same (which, although not ideal, can happen)
								For non boolean flags the flagTextID is king (you can't actually update a flagGroup of these types)
								For boolean flags it actually makes more sense for flagGroupTextID to be king - this is the field which will appear in an entity view
								
			*/


			case 
				when t.table_name is not null then 
							case 
								when t.data_Type like '%char' then
									null  /* t.data_Type + '(' + case when t.character_maximum_length = -1 then 'max' else convert(varchar,t.character_maximum_length) end + ')'   need to deal with varchar(max) */
								when col.dataType like '%char' and t.data_Type in ('int','tinyint','smallint','bigint') then
									'CAST (CAST (FIELDNAME as float) as ' + t.data_Type + ')'   /* SQL cannot convert straight from nvarchar to int (especially if there is a decimal point or an exponent) but seems to be able to if we pass through float */
								when col.dataType like '%char' and t.data_Type in ('numeric','decimal','money') then
									'CAST (FIELDNAME as float)'   /* SQL cannot convert straight from nvarchar to numeric (especially if there is a decimal point) but seems to be able to if we pass through float */
								else	
									'CAST (FIELDNAME as ' + t.data_Type + CASE WHEN t.numeric_precision IS NOT NULL AND t.numeric_scale <> 0 THEN '(' + CONVERT(VARCHAR,t.numeric_precision) + ',' + CONVERT(VARCHAR,t.numeric_scale) + ')' ELSE '' END + ')'
							end	
				when f.flagid is not null then 
					case 
						when col.dataType like '%char' and f.dataType = 'integer' then 
							'CAST (CAST (FIELDNAME as float) as int)' 
						when col.dataType like '%char' and f.dataType in ('decimal') then 
							'CAST (FIELDNAME as float) ' 
						when f.dataType in ('checkbox','radio') and (fg.flaggroupid is null OR isNull(dbo.getParameterValue (fg.formattingParameters,'isBitField',default),'') = 'true') and col.isNumericOrDateType = 0 then 
							'case when FIELDNAME in (''1'',''true'',''Yes'') then 1 when FIELDNAME in (''0'',''false'',''no'') then 0 else 0 end' 
						else 
							'FIELDNAME' 
					end
				when fg.flaggroupid is not null then 
					'FIELDNAME' 
			end as castExpression, 
			case 
				when t.table_name is not null then 
					t.numeric_precision - t.numeric_scale 
				when ft.table_name is not null then 	
					ft.numeric_precision - ft.numeric_scale 			
				else null
			end	as lengthBeforeDecimalPoint,
			case 
				when (t.numeric_precision is not null or t.datetime_precision is not null or f.FlagID is not null) and col.isNumericOrDateType = 0 then 
					1 
				else 
					0 
			end as blankToNull,
			case 
				when t.table_name is not null then 
					t.data_Type  
				when fg.flaggroupid is not null and fg.flagTypeID in (2,3) and (col.isNumericOrDateType = 0 OR f.flagid is null) and not isNull(dbo.getParameterValue (fg.formattingParameters,'isBitField',default),'') = 'true' then -- if matches a flag as well and the dataType is a Numeric/Bit then don't match the flagGroup 
					'flagGroup' 
				when f.flagid is not null then 
					f.dataType 
			end as table_dataType, 
			case 
				when t.table_name is not null then 
					t.character_maximum_length 
				when f.flagid is not null then 
					0 
				when fg.flaggroupid is not null then 
					0 
			end as table_Length, 
			case 
				when t.table_name is not null then 
					'Base' 
				when fg.flaggroupid is not null and fg.flagTypeID in (2,3) and (col.isNumericOrDateType = 0 OR f.flagid is null) and not isNull(dbo.getParameterValue (fg.formattingParameters,'isBitField',default),'') = 'true'  then   -- if matches a flag as well and the dataType is a Numeric/Bit then don't match the flagGroup 
					'flagGroup'  
				when f.flagid is not null then 
					'Flag' 
				when linkedFlag.flagid is not null then 
					'LinkedFlag' 
			end as tableType, 
			case 
				when f.flagid is not null then f.dataType when fg.flagTypeID = 2 then 
					'checkbox' 
				when fg.flagTypeID = 3 then 
					'radio' 
				when linkedFlag.flagid is not null then 
					linkedFlag.dataType 
				else 
					'' 
			end as flagDataType,
			case 
				when f.flagid is not null then 
					f.flagid 
				when fg.flagTypeID is not null then 
					fg.flagGroupID 
				when linkedFlag.flagid is not null Then 
					linkedFlag.flagid 
				else 
					0 
			end as flagOrflagGroupID,
			f.linksToEntityTypeID,
			case when f.dataType in ('IntegerMultiple','TextMultiple') then 1 else 0 end isList,
			case when setToNull.name is not null then 1 else 0 end as hasSetToNull,
			case when t.table_name is not null AND t.is_Nullable = 'NO' and t.column_default is null and t.column_name not in ('created','createdby','lastupdated','lastupdatedby','lastupdatedbyperson','createdbyperson') then 1 else 0 end AS requiredForInsert,
			case when t.table_name is not null AND t.is_Nullable = 'NO' then 1 else 0 end AS notNullable,
			case when t.table_name is not null and t.column_default is not null and t.column_default  not like '%/*%' and t.column_name not in ('created','createdby','lastupdated','lastupdatedby','lastupdatedbyperson','createdbyperson') then t.column_default else null end AS defaultValue,
			/* These columns are populated later by an update statement */
			convert(nvarchar(max),'') as columnBeforeCasting,
			convert(nvarchar(max),'') as castColumn,
			convert(nvarchar(max),'') as updateWhere,
			convert(nvarchar(max),'') as setStatement,
			convert(nvarchar(max),'') as processWhen,
			/* These columns are only used during validation */
			convert(nvarchar(max),'')  as foreignKeyTable,
			convert(nvarchar(max),'')  as foreignKeyColumn,
			convert(nvarchar(max),'') as dataTypeCheckAnderrorMessage

		INTO #updatedColumns
		FROM 
			(SELECT 
				colid, 
				name,
				dataType,
				length,
				isNumericOrDateType,
				((colid - 1) / 8) + 1 as byte,  /* refers to which byte in columns_UPDATED */
				(colid - 1 )% 8 + 1 as bit /* refers to which bit in the byte */							
			FROM
				@viewColumns as col1
			) as col	
			/* left joins to the flag, flaggroup and schema tables to get information on each column */
			left join 
				vflagDef f ON entityTypeID = @entityTypeID and (col.name = f.flagTextID OR col.name = 'flag_' + convert(varchar,f.flagID)) 
			left join 
				flagGroup fg ON fg.entityTypeID = @entityTypeID and (col.name = fg.flagGroupTextID OR col.name = 'flagGroup_' + convert(varchar,fg.flagGroupID) )
			left join 
				information_schema.columns t ON t.table_name = @entityName and t.column_name = col.name
			left join 
				information_schema.columns ft ON f.flagid is not null and ft.table_name = f.DataTable + 'flagData' and ft.column_name = 'data'
			left join 
				@viewColumns setToNull ON setToNull.name = col.name + '_setToNull'
			left join
				vflagDef linkedFlag on 'is_' + linkedFlag.flagTextID = col.name and linkedFlag.linksToEntityTypeID = @entityTypeID

		where 
			
			SUBSTRING(@columns_UPDATED,byte,1) & power(2,bit-1) > 0 
			and col.name <> @entityIDColumn
			and col.name not like  '%_setToNull'
			and col.name not in ('lastUpdated','lastupdatedby','lastupdatedbyperson','created','createdby','createdbyPerson')
			and isnull(COLUMNPROPERTY(OBJECT_ID(@entityname),col.name,'IsComputed'),0) = 0


		/* 
			Use some of the metadata to populate some more columns with SQL snippets
				columnBeforeCasting
				processWhen
				castColumn
				updatewhere
				setStatement
				
			See descriptions below
		*/

		update #updatedColumns
		SET 
			/* In date and numeric columns we consider blank to be equivalent to setting the value to null 
				except it is not a null when it comes to deciding whether to process the value or not
			*/
			@columnBeforeCasting = 
			columnBeforeCasting = 
				CASE when BlankToNull =1 OR  hasSetToNull = 1 THEN 
					'case ' 
						+ CASE WHEN BlankToNull =1  THEN 
							 ' when i.' + name + '= '''' then null ' 
							  WHEN hasSetToNull = 1 THEN
							  ' when(isnull(i.[' + name + '_setToNull],0) = 1 then null'
							END
					+ ' else i.' + name + ' end '		
				ELSE
					'i.' + name
				END	
			
			/* an value item needs processing if
				either
						it is not null
					OR	processNull == 1 
					OR	there is a _setToNull column with a value 1
				of course null values are ignored during data type checks, but they	are important for not null fields
				
				This is used both to control which values are validated and which sets/updates are done
			*/		
			,@processWhen = 
			processWhen = 
					'('
				+	'i.' + name + ' is not null'
				+	CASE WHEN processNull = 1 THEN ' OR 1 = 1' ELSE '' END
				+	CASE WHEN hasSetToNull = 1 THEN  ' OR (isnull(i.[' + name + '_setToNull],0) = 1' ELSE '' END
				+	')'

			/* WAB 2015-02-04 change way castColumn is generated - now down by a replace */
			,@castColumn = 
			castColumn =
					CASE when truncatedLength <> 0 THEN 'left (' ELSE '' END	
				+	CASE WHEN castExpression is not null THEN replace(castExpression,'FIELDNAME',@columnBeforeCasting) ELSE  @columnBeforeCasting END
				+	CASE when truncatedLength <> 0 THEN ',' + convert(varchar,truncatedLength) + ')'	ELSE ''	END	
			
			
			,updateWhere = 
						'('
					+	@processWhen     -- this test for a non null value (or processNull = 1)
					+	 ' AND (' + @CR
					+				@castColumn + case when table_dataType like '%varchar%' then ' COLLATE Latin1_General_CS_AS ' ELSE '' END + ' <> baseEntity.' + name + @CR	   -- value has changed
					+	'		OR (baseEntity.' + name + ' is null		and ' + @castColumn + ' is not null) ' + @CR  -- null to not null
					+	'		OR (baseEntity.' + name + ' is not null and ' + @castColumn + ' is null) '	+ @CR	 -- not null to null (but remember that the @processWhen clause will prevent null values in the upsert table being processed inappropriately
					+	'))' + @CR

			,setStatement = 
					name + ' =  case when NOT ' + @processWhen + '	then baseEntity.' + name +	' else ' + @castColumn + ' end '



		IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': Columns Analysed' 

		/*
		Start of Basic Validation
			Data Types, Required Fields, Foreign Keys, Unique Indexes
			(Some flag validation is done in the flag section later) 
		*/
		IF @validate = 1
		BEGIN

			/* 	For performance reasons we just get the foreign key information when in validate mode
				So we might as well do the dataTypeCheck stuff here aswell rather than in the section above
			 */

			update #updatedColumns
			SET 		
				foreignKeyTable = case when tableType = 'base' then ref.table_Name when linkedEntity.entityTypeID is not null then linkedEntity.entityName end  
				,foreignKeyColumn = case when tableType = 'base' then ref.column_Name when linkedEntity.entityTypeID is not null then linkedEntity.uniquekey end 

			/*
				2015-04-14	WAB major changes in this area as detailed below

				dataTypeCheckAndErrorMessage 
				
				This specifies the data type check(s) to do on a column and the message to return if the check fails
				The check is coded as a WHERE statement which will return records which are correct (the validation code later negates it to return the items which fail)
				Originally I had separate columns for the check and the message and could only support a single check per column
				It was then necessary to support multiple checks per column (In particular to test numeric fields for a) being numeric and b) not causing an arithmetic overflow)
				My solution for Multiple Checks was to create a list of checks and to parse the list using an XML function
				At this point I decided to do away with the separate columns for the check and the message and instead ended up with a list of lists.  
				This actually makes some things neater (we don't have to repeat the CASE statements)
				The check and the message are separated by a | delimiter
				Multiple checks are separated by a � delimiter. This is replace by char(172) here as � can get corrupted

				WAB 2015-09-28 Changed the validation code to alias the column being validated as [__ColumnToCheck]. (see comment in validation section)
				
			*/

			, dataTypeCheckAndErrorMessage	=	
					CASE 
						WHEN table_dataType in ('int','decimal', 'float', 'numeric','integer','integerMultiple') and upsert_dataType not in ('int','decimal','float', 'numeric') THEN
							'(dbo.isNumeric (__ColumnToCheck) = 1 )'
							+ '|' 
							+ 'NotNumeric'
						WHEN table_dataType in ('datetime','smallDateTime','date') and upsert_dataType not in ('datetime','smallDateTime','date') THEN
							'(isDate (__ColumnToCheck) = 1  )'
							+ '|'
							+ 'NotValidDate'  
						WHEN table_dataType like '%char%' and character_maximum_length <> -1 	and (character_maximum_length < upsert_length OR upsert_length = -1) and truncatedLength = 0 THEN
							'len (__ColumnToCheck ) <=  ' + convert(varchar,character_maximum_length)
							+ '|'
							+ 'DataTooLong'
						ELSE	
							''												
						END	
				
					+
					/* an extra check for the length of numeric fields
						work out length by taking the log10 of the number
						have to do an ABS first and exclude 0
						Need to convert 0 to float so that implicit conversion works in all circumstances
						WAB 2015-09-15 had to recraft this test as a case statement so that it short-circuits.  Oddly the old version worked with varchar fields but not nvarchar
					
					 */
					CASE	
						WHEN table_dataType in ('int','decimal', 'float', 'numeric','integer') and upsert_dataType not in ('int','decimal','float','numeric') THEN
							char(172) 
									+ ' case when (dbo.isNumeric (__ColumnToCheck) = 0 ) then 1 when __ColumnToCheck = convert(float,0) then 1 when convert (int,log10 (abs(convert(float,__ColumnToCheck)))) < ' + convert(varchar,lengthBeforeDecimalPoint) + ' then 1 else 0 end = 1'
							+ '|' 
							+ 'NumberTooLarge'
						ELSE	
							''												
						END	


			from
				#updatedColumns
			left join
				schemaTable linkedEntity on linksToEntityTypeID = linkedEntity.entityTypeID

			left join
				(
				information_schema.CONSTRAINT_COLUMN_USAGE ccu
					inner join
				information_schema.REFERENTIAL_CONSTRAINTS rc on rc.constraint_name = ccu.constraint_name 
					inner join
				information_schema.CONSTRAINT_COLUMN_USAGE ref	on ref.constraint_name = rc.unique_constraint_name
					) ON ccu.table_name = @entityName And ccu.COLUMN_NAME = name
			


			/*  Data Type Validation 
				2015-04-14	WAB major changes in this area (see other comments of this date!)
				WAB 2015-09-28 Changed validation code to deal with integerMultiple flags (ie @isList = 1)
								Column being validated now aliased as [__ColumnToCheck].
			*/


			DECLARE validateCols cursor local static for
			SELECT name, processWhen, dataTypeCheckAndErrorMessage, isList
			FROM #updatedColumns
			WHERE dataTypeCheckAndErrorMessage <> ''
			
			OPEN validateCols
				
				FETCH next from validateCols into 
					@ColName,@processWhen, @dataTypeCheckAndErrorMessage, @isList 

				WHILE @@fetch_status = 0 
				BEGIN

					/* @dataTypeCheckAndErrorMessage is a list of lists!
						Delimited by | and �
						break it down using XML
					*/

					set @listAsXML = '<listitem><check>' + replace(replace(replace(replace(@dataTypeCheckAndErrorMessage,'>','&gt;'),'<','&lt;'),'|','</check><message>'),char(172),'</message></listitem><listitem><check>') + '</message></listitem>'

					DECLARE validateCol cursor local static for
					select 
						dataNode.value('check[1]','varchar(max)') as dataTypeCheck ,
						dataNode.value('message[1]','varchar(max)') as dataTypeErrorMessage 
						from @listAsXML .nodes('/listitem') as d (dataNode)
			
					OPEN validateCol
				
						FETCH next from validateCol into 
							 @dataTypeCheck,@dataTypeErrorMessage


						WHILE @@fetch_status = 0 
						BEGIN
									
							if (@debug > 0) print 'Validating Data Type OR Length ' + @colname + @CR + @dataTypeCheck

							SET @sqlCmd =	
								' 	insert into #validateResult (entityID, upsertUniqueID, field, message,value)
									select ' +  @cr 
								+		@entityIDColumn + ',' + @upsertUniqueIDColumnName + ',''' + @colname + ''',''' + @dataTypeErrorMessage + ''', convert(varchar(max), __ColumnToCheck )  
									from  
										(
											select i.* ' + @cr  
												/* when dealing with a list we have to do an XML conversion and a CROSS APPLY, otherwise just select the column aliased as __ColumnToCheck*/
								+				case when @isList = 1 then 
													', cast(''<dataitem>''+ replace(dbo.xmlformat(replace( i.[' + @ColName + '] ,'';'','','')),'','',''</dataitem><dataitem>'')+''</dataitem>'' as xml)  as dataAsXML '
												else 
													', i.[' + @ColName + '] as __ColumnToCheck '
												end 
								+			 ' from ' 
								+				 @filteredUpsert + ' as i 
										 ) i ' +  @cr 
								+		     case when @islist = 1 then 
												 'CROSS APPLY 
														(SELECT aaa.bbb.value(''.'',''varchar(100)'') as __ColumnToCheck FROM dataAsXML.nodes(''dataitem'') as aaa(bbb)) as individualDataValues'
											else '' end 
								+ ' where ' + @cr  
								+	 	@processWhen + @cr  
								+			 ' and ' + @ColName + ' is not null ' + @cr  
								+			 ' and ' + @ColName + ' <> '''''  + @cr 
								+			 ' and NOT (' + @dataTypeCheck + ')'
									
									if (@debug = 1)	print CAST (@sqlcmd as nText)
									EXEC (@sqlCmd)

							FETCH next from validateCol into 
								 @dataTypeCheck,@dataTypeErrorMessage
						END
					CLOSE validateCol
					DEALLOCATE validateCol




					FETCH next from validateCols into 
						@ColName,@processWhen,@dataTypeCheckAndErrorMessage, @isList
				END  
		
			CLOSE validateCols
			DEALLOCATE validateCols


			/*  
				WAB 2015-05-11
				Look for Duplicate Items (same entityID)
			*/

			SET @sqlCmd =	
					' insert into #validateResult (entityID, upsertUniqueID, field, message,value) ' + @CR +
					' select i.' + @entityIDColumn + ', ' + @upsertUniqueIDColumnName + ','''',''DuplicateEntityID'', null  ' + @CR +
					' from ' + @CR +
							'(select ' + @entityIDColumn  + @CR +
							'  from ' + @filteredUpsert + ' x  ' + @CR +
							'	where ' + @entityIDColumn + ' is not null  ' + @CR +
							'	group by ' + @entityIDColumn +  @CR +
							'	having count (1) > 1) as dupes ' + @CR +
						' left join ' + @filteredUpsert + ' i '+ @CR +
						'	 on i.' + @entityIDColumn + ' = dupes.'+ @entityIDColumn  + @CR
			
			IF  (@debug > 1)	print CAST (@sqlcmd as nText)

			EXEC (@sqlCmd)
					

			/*  Foreign Key Constraint Validation	
				WAB 2015-09-28 Added support for integerMultiple flags
			*/

			DECLARE @foreignKeyTable sysname, @foreignKeyColumn sysname

			DECLARE validateCols cursor local static for
			SELECT name, columnBeforeCasting, processWhen, foreignKeyTable, foreignKeyColumn, isList
			FROM #updatedColumns
			WHERE 
				foreignKeyColumn is not null
			
			
			OPEN validateCols
				
				FETCH next from validateCols into 
					@ColName,@columnBeforeCasting, @processWhen, @foreignKeyTable, @foreignKeyColumn, @isList 

				WHILE @@fetch_status = 0 
				BEGIN
					IF  (@debug = 1) print 'validating foreign Key field ' + @colname + ' --> ' + @foreignKeyTable + '.' + @foreignKeyColumn 

					SET @sqlCmd =	
										' insert into #validateResult (entityID, upsertUniqueID, field, message,value) ' + @CR
									+	' select i.' + @entityIDColumn + ',' + @upsertUniqueIDColumnName + ',''' + @colname + ''',''InvalidForeignKey'', convert(varchar(max), __ColumnToCheck ) ' + @CR
									+	' from ' + @CR
									+			'(select i.*' + 
													case when @isList = 1 then 
														', cast(''<dataitem>''+ replace(dbo.xmlformat(replace( i.' + @ColName + ' ,'';'','','')),'','',''</dataitem><dataitem>'')+''</dataitem>'' as xml)  as dataAsXML '
													else 
														', i.' + @ColName + ' as __ColumnToCheck '
													 end
									
									+				'from ' + @filteredUpsert + ' i left join #validateresult v on ' + @upsertUniqueIDColumnName + ' = v.upsertuniqueid where v.upsertUniqueID is null) as i ' + @CR
									+     case when @isList =1 then 
												 'CROSS APPLY 
														(SELECT aaa.bbb.value(''.'',''varchar(100)'') as __ColumnToCheck FROM dataAsXML.nodes(''dataitem'') as aaa(bbb)) as individualDataValues'
											else '' end
									+	'	left join ' + @foreignKeyTable  + ' ON cast(__ColumnToCheck as varchar) = cast(' + @foreignKeyTable + '.' + @foreignKeyColumn  + ' as varchar) ' + @CR
									+	' where ' + @CR
									+				 @processWhen  + @CR
									+	'	and ' + @columnBeforeCasting + ' is not null ' + @CR
									+	'	and ' + @foreignKeyTable + '.' +@foreignKeyColumn + ' is null'


					IF  ( @debug > 1)	print CAST (@sqlcmd as nText)

					EXEC (@sqlCmd)

					FETCH next from validateCols into 
						@ColName,@columnBeforeCasting,@processWhen,@foreignKeyTable, @foreignKeyColumn, @isList
				END  
		
			CLOSE validateCols
			DEALLOCATE validateCols


			/*  Unique Index Validation	
				2015-04-15	WAB	added join to #updatedColumns, otherwise tried to validate columns which were not there 	
			*/
			declare @indexFilter_definition nvarchar(max)

			DECLARE validateCols cursor local static for

				SELECT 
					 col.name, filter_definition
				FROM 
					 sys.indexes ind 
				INNER JOIN 
					 sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
				INNER JOIN 
					 sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id 
				INNER JOIN 
					 sys.tables t ON ind.object_id = t.object_id 
				INNER JOIN
					#updatedColumns  ON #updatedColumns.name = col.name
				WHERE 
					 ind.is_primary_key = 0 
					 AND ind.is_unique = 1 
					 AND ind.is_unique_constraint = 0 
					 AND t.is_ms_shipped = 0 
					 AND t.name = @entityName
				ORDER BY 
					 t.name, ind.name, ind.index_id, ic.index_column_id 

			
			OPEN validateCols
				
				FETCH next from validateCols into 
					@ColName, @indexFilter_definition

				WHILE @@fetch_status = 0 
				BEGIN
					IF  (@debug > 0 ) print 'validating unique Index ' + @colname 

					set @indexFilter_definition = REPLACE (@indexFilter_definition,'[' + @colName + ']', 'i.[' + @colName + ']')

					SET @sqlCmd =	'
									insert into #validateResult (entityID, upsertUniqueID, field, message,value)
									select i.' + @entityIDColumn + ',' + @upsertUniqueIDColumnName + ',''' + @colname + ''',''UniqueIndexConflict'', convert(varchar(max), i.' + @colName + ' )  
									from ' + @filteredUpsert + ' i 
									inner join ' + @entityName + ' baseEntity ON i.' + @ColName + ' = baseEntity.' + @ColName 
									+ ' where '
										+ @baseEntityIDColumnClause + '<> isNull(' + @EntityIDColumnClause + ',0)'+ @CR
					
									+ case when @indexFilter_definition is not null then 
										' and ' + @indexFilter_definition
										else '' end
			
									
									
					IF  (@debug > 1)	print CAST (@sqlcmd as nText)

					EXEC (@sqlCmd)

					FETCH next from validateCols into 
						@ColName, @indexFilter_definition
				END  
		
			CLOSE validateCols
			DEALLOCATE validateCols


			/*  NonNullable Column Validation
			*/
			
			DECLARE validateCols cursor local static for
			SELECT name , columnBeforeCasting, processWhen
			FROM #updatedColumns  
			WHERE 
					notNullable = 1
					and (processNull = 1 or blankToNull = 1 ) 				/* In the case of a date or numeric value passed in as a text field, a blank value is equivalent to actually setting a null value*/
	
			
			OPEN validateCols
				
				FETCH next from validateCols into 
					@ColName, @columnBeforeCasting, @processWhen

				WHILE @@fetch_status = 0 
				BEGIN
					IF  (@debug > 0) print 'Non Nullable Columns ' + @colname

					SET @sqlCmd =	'
									insert into #validateResult (entityID, upsertUniqueID, field, message)
									select ' + @entityIDColumn + ',' + @upsertUniqueIDColumnName + ',''' + @colname + ''',''REQUIRED_DATA_MISSING'' 
									from ' + @filteredUpsert + ' i 
									where ' 
										+	@processWhen 
										+	' and ' + @columnBeforeCasting + ' is null'
										+	' and ' + @entityIDColumn + ' is not null'  -- when @entityIDColumn is null then we are dealing with an insert - the required will be dealt with by a later validation (which takes defaults into account)
									
					IF  (@debug > 1)	print CAST (@sqlcmd as nText)

					EXEC (@sqlCmd)

					FETCH next from validateCols into 
						@ColName,@columnBeforeCasting, @processWhen 
				END  
		
			CLOSE validateCols
			DEALLOCATE validateCols

		END
		/* End of Basic Validation */


		/*  
			Start actually processing the upserts
			Start with the base table, 
			They can all be done in one update, but we need to create lists for the SET and WHERE clause
		*/
		DECLARE @updatewhere nvarchar(max), @setStatement nvarchar(max)
		
		/* concatenate all the individual pieces of SQL into long statements */	
		SELECT 
			@setClause			+=  case when @setClause = '' then '' else ','				end + @CR + setStatement,
			@updateWhereClause +=  case when @updateWhereClause = '' then '' else ' OR '	end + @CR + updateWhere,
			@colList			+=  case when @colList = '' then '' else  ','				end + @CR + name,						
			@colListWithAlias	+=	case when @colListWithAlias = '' then '' else  ','		end + @CR + CASE WHEN defaultValue is not null then 'isNull(' + castColumn + ',' + defaultValue + ')' else castColumn end
		FROM #updatedColumns
		WHERE tableType = 'base' and name not like '%_defaultTranslation'


		/* We now have the SET and UPDATE clauses to run the base table updates/insert
			 If the setClause has anything in it then we need to do an update (or an insert)
		*/ 
		IF @setClause <> '' 
		BEGIN		

			/*	Work out whether we have updates, inserts or both 
				For Inserts the entityIDs may already be populated or not, but to start with we just need to do a left join to decide whether there are any records to process
			*/
			DECLARE @totalItems int, @itemsToInsert int, @idsToCreate int 
			SET @sqlcmd = 'select 
								@totalItems = count(1), 
								@itemsToInsert = sum (case when ' + @baseEntityIDColumnClause + ' is null then 1 else 0 end) , 
								@idsToCreate = sum (case when ' + @entityIDColumnClause + ' is null then 1 else 0 end) ' 
						+ ' from ' + @filteredUpsert + ' i '
						+ replace (@JoinToBaseEntity,'inner join ', 'left join ')

			IF  (@debug > 1)	print CAST (@sqlcmd as nText)
			
			EXEC sp_executesql @sqlcmd,N'@itemsToInsert int OUTPUT,@idsToCreate int OUTPUT,@totalItems int OUTPUT',@itemsToInsert OUTPUT,@idsToCreate OUTPUT, @totalItems OUTPUT
			
			IF  (@debug > 0)	print 'Items To Insert: '+ convert(	varchar, @itemsToInsert) + ' totalItems: ' 	+ convert(	varchar, @totalItems ) + ' @idsToCreate: ' + convert(	varchar, @idsToCreate )

			IF @itemsToInsert <> 0
			/* Inserts to be done */
			BEGIN
				
				IF @validate = 1
				BEGIN
				/* Validation of Inserts */
					IF  (@debug > 0)	print 'Doing Insert Validation'

					/*  Need to check for required (NOT NULL) columns which are not in the incoming table and which don't have defaults
						WAB 2015-12-18 BF-87 Return a row for each item to be inserted rather than a single row
					*/
					SET @sqlCmd =	'
							insert into #validateResult (entityID, upsertUniqueID, field, message)
							select ' + @entityIDColumn + ',' + @upsertUniqueIDColumnName +  ', column_name, ''missingRequiredField'' 
							from 
								information_schema.columns 
									inner join
								' + @filteredUpsert + ' i on ' + @entityIDColumn + ' is null 
							where 
								table_name = @entityName 
								and is_Nullable = ''NO'' and column_default is null
								and column_name not in (''created'',''createdby'',''lastupdated'',''lastupdatedby'',''lastupdatedbyperson'') 
								and column_name not in (select name from #updatedColumns)
								and COLUMN_NAME <>  @entityIDColumn'
					

					IF  (@debug > 1)	print CAST (@sqlcmd as nText)
					EXEC sp_executesql @sqlCmd, N'@entityName sysname, @entityIDColumn sysname', @entityName = @entityName,  @entityIDColumn = @entityIDColumn
					
					
					/*  Need to check that any required columns do not have null values */
					DECLARE requiredCols cursor local static for
					SELECT name, columnBeforeCasting, requiredForInsert
					FROM #updatedColumns
					WHERE tableType = 'base' and requiredForInsert = 1
					
					OPEN requiredCols
						
						FETCH next from requiredCols into 
							@ColName,@columnBeforeCasting, @requiredForInsert

						WHILE @@fetch_status = 0 
						BEGIN

							SET @sqlCmd =	'
											insert into #validateResult (entityID, upsertUniqueID, field, message)
											select ' + @entityIDColumn + ',' + @upsertUniqueIDColumnName + ',''' + @colname + ''',''REQUIRED_DATA_MISSING'' 
											from ' + @filteredUpsert + ' i 
											where ' 
												+	@upsertUniqueIDColumnName + ' not in (select upsertUniqueID from #validateResult) '
												+	'and ' + @columnBeforeCasting + ' is null'

							IF  (@debug > 1)	print CAST (@sqlcmd as nText)

							EXEC (@sqlCmd)

							FETCH next from requiredCols into 
								@ColName,@columnBeforeCasting, @requiredForInsert
						END  
				
					CLOSE requiredCols
					DEALLOCATE requiredCols
			
				END
				ELSE /* @validate = 0 */
				BEGIN
					DECLARE @seed int, @hasIdentity bit = 1, @identityInsert bit = 0
					-- print '@idsToCreate : ' + convert(varchar,@idsToCreate)

					/* TBD bit of a hack all round here to deal with the POL tables which do not have identity column 
					/* NJH 2016/09/01 - JIRA PROD2016-125 - removed assignedEntityIds for POL data
					IF @entityName in ('person','location','organisation')
						SET @hasIdentity = 0
					*/	

					IF isnull(@idsToCreate,0) <> 0
					/* ids to be created */
					BEGIN
						
						/*
						
						*/

						/* for POL tables we need to supply the values in the ID column, 
							for other tables they have identity columns so can just do an insert
							Not dealing with identities being supplied for identity columns (ie would need to use SET identity_insert ON)
							@hasIdentity
						 */
						/* get the seed for the ids */
						
						
						IF @entityName in ('person','location','organisation')
						BEGIN	
							EXEC getNextEntityID @entityName,@idsToCreate,@seed OUTPUT, @hasIdentity OUTPUT
							
							IF  (@debug = 1) print '@hasIdentity: ' + convert(varchar,@hasIdentity)
							SET @seed = @seed - 1  /* need to subtract 1 because 1 is added on every record including the first */
					
							/* WAB 2016-07-08 2016 PROD2016-1367  (RingCentral)
								Receiving Error Data type varchar of receiving variable is not equal to the data type int of column 'organisationID'
								This occurred when incoming 'identity' column was varchar.  
								Alter table so that column is integer
								No idea why this error suddenly started happening
							*/	

							SET @sqlcmd = 'alter table ' + @upsertTableName + ' alter column ' + replace(@entityIDColumnClause,'i.','') +  ' int'
							exec (@sqlcmd)

							/*  and update the upsert table 
								2014-09-30  WAB Deal with cartesian join, needed to reference upsertTable in the from clause
								2014-11-17	WAB	Fix 2 problems with above fix (which can't have ever worked). Missing a space and aliasing of the upsertTable did not work 
							*/

							SET @sqlcmd =  'update ' + @upsertTableName + ' SET @seed = '  
											+ replace(@entityIDColumnClause,'i.','') + ' = @seed + 1 ' 
											+ ' from ' 
											+ @upsertTableName + '  inner join '
											+ @filteredUpsert + ' i on ' + @upsertTableName + '.upsertUniqueID = ' + @upsertUniqueIDColumnName
											+ replace (@JoinToBaseEntity,'inner join ', 'left join ')
											+ ' where ' 
											+  @baseEntityIDColumnClause + ' is null'			
											+ ' and ' + @entityIDColumnClause + ' is null'			

								IF  (@debug = 1)	print CAST (@sqlcmd as nText)

								EXEC sp_executesql @sqlcmd , N'@seed int',@seed = @seed
						END	

					END	*/	

				END	
						
			END

			IF @validate = 0
			BEGIN
		
					DECLARE @colListWithCRUD nvarchar(max ) = @colList + ',' + @insertCRUDCols
					DECLARE @colListWithAliasAndCRUD nvarchar(max ) = @colListWithAlias + ',' + @insertCRUDClause
					

					IF @hasIdentity = 1 and	@idsToCreate = 0
						set @identityInsert = 1

					/* IF we are inserting items to tables without an identity on the entityID column then need to added the entityID column to the insert */
					IF  (@itemsToInsert <> 0 AND @hasIdentity = 0) OR @identityInsert = 1
					BEGIN
						SET @colListWithCRUD = @entityIDColumn + ',' + @colListWithCRUD
						SET @colListWithAliasAndCRUD = 'i.' + @entityIDColumn + ',' + @colListWithAliasAndCRUD
						
					END	

					
					IF  (@debug > 0)	print 'update : ' + @entityName + '. Fields: ' + @setClause 

					SET @sqlcmd = ' MERGE ' + @entityName + ' baseEntity ' + @CR
						+ ' USING ' +	@filteredUpsert + ' i ' + @CR
						+ ' ON ' + @baseEntityIDColumnClause + '=' + @EntityIDColumnClause + @CR
						+ ' WHEN MATCHED AND (' + @updateWhereClause + ')' + @CR									
						+ '		THEN UPDATE SET ' + @setClause + ',' + @CR + @setCRUDClause + @CR

					IF  @itemsToInsert <> 0
					BEGIN
						SET @sqlcmd = @sqlcmd 
								+ ' WHEN NOT MATCHED BY TARGET ' 									
								+ '		THEN INSERT ' 
								+ '					(' + @colListWithCRUD + ') '
								+ '			VALUES	(' + @colListWithAliasAndCRUD + ')'
					END
					
					SET @sqlcmd = @sqlcmd 						
							+ ' OUTPUT $action, isNull(inserted.' + @entityIDColumn + ',deleted.' + @entityIDColumn + '),' + @upsertUniqueIDColumnName +' INTO #results;'

					IF @identityInsert = 1
					BEGIN
						set @sqlcmd =	'Begin tran ' + char(10) + 
										'Set identity_insert ' + @entityName + ' ON ' + char(10) +
										@sqlcmd +
										'Set identity_insert ' + @entityName + ' OFF ' + char(10) +
										'Commit'
					END
					
					IF  (@debug > 0) print (@colListWithAliasAndCRUD)
					IF  (@debug > 1) print CAST (@sqlcmd as nText)
					IF  (@debug > 0)print 'Start Merge '  + convert(varchar,getdate(),114)
					

					EXEC (@sqlcmd)
		
					IF  (@debug > 0)	print 'END Merge ' + convert(varchar,getdate(),114)
			
					/* Now we need to update the temporary table with the identities which have been inserted (so that we can update the associated flags)*/
					IF  @itemsToInsert <> 0
					BEGIN

						SET @sqlcmd =	' UPDATE ' + @upsertTableName
									+	' SET ' + @entityIDColumn + ' = r.entityID '
									+	' FROM ' + @upsertTableName + ' i ' + @CR
									+	' INNER JOIN #results r ON ' + @upsertUniqueIDColumnName + ' = r.upsertUniqueID'
						
						IF  (@debug > 1)	print CAST (@sqlcmd as nText)
						
						EXEC (@sqlcmd)			


					END

			END


		END
		
		IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': Base Table Done'


		IF @isView = 0 and ( @whereClause <> '' OR @rows is not null) AND @useTemporaryTable = 1 
		BEGIN
			SELECT
				 @tempTableName = '##entityUpsert_filteredData' + replace(newID(),'-','')
			
			SELECT 
				@sqlCMD = 'Select * into '  + @tempTableName + ' from ' + @filteredUpsert + ' as x2',
				@upsertTableName = @tempTableName,
				@filteredUpsert = @tempTableName
			
			EXEC (@sqlCMD)
			
		END

		/* now do _defaultTranslation columns */
		IF @validate = 0
		BEGIN
			DECLARE defaultTranslations cursor local static for
				SELECT name
				FROM #updatedColumns
				WHERE tableType = 'base' and name like '%_defaultTranslation'
		
		
			DECLARE @phraseTextID sysname
			OPEN defaultTranslations
			
				FETCH next from defaultTranslations into 
				@ColName
				
				WHILE @@fetch_status = 0 
				BEGIN
				
					SET @phraseTextID = replace (@colName,'_defaultTranslation','')
					
					SET @sqLCMD = '
						INSERT INTO
							PHRASELIST
								(phraseid, phraseTextID, entityid, entityTypeID, created, createdby) 
							SELECT 
							(select max(phraseid) from phraselist) +  Row_Number() OVER (order by ' + @upsertUniqueIDColumnName + '),''' +@phraseTextID + ''',i.' + @entityIDColumn + ',' + convert(varchar,@entityTypeID) + ',getdate(),' + @updatedByPersonClause
						+ '	 FROM  '
						+		 @filteredUpsert  + ' i  
									inner join '
						+		@entityName + ' e on i.' + @entityIDColumn + ' = e.' + @entityIDColumn 
						+ '			LEFT JOIN
								PhraseLIST 	
									 ON phraselist.entityTypeID = ' + convert(varchar,@entityTypeID) + ' and phraselist.entityid = i.' + @entityIDColumn + ' and phraseTextID = ''' +@phraseTextID + '''  
							WHERE '
						+	'i.' + @phraseTextID + '_defaultTranslation is not null and phraselist.phraseid is null '

						IF  (@debug > 1) print CAST (@sqlcmd as nText)
						
						EXEC (@sqlcmd)			

					SELECT @baseSQLCMD = '
							MERGE phrases 
							 USING ' + @filteredUpsert + ' i 
								inner join '
						+			@entityName + ' e on i.' + @entityIDColumn + '= e.' + @entityIDColumn 
						+	'	inner join
									phraselist pl on pl.entityTypeID = ' + convert(varchar,@entityTypeID) + ' and pl.entityid = i.' + @entityIDColumn + ' and pl.phraseTextID = ''' + @phraseTextID + '''  
							 ON phrases.phraseid=pl.phraseID '
							 
					SET @sqLCMD = @baseSQLCMD +		 
							 ' WHEN MATCHED and e. ' + @phraseTextID + '_defaultTranslation = phrases.phraseText and i.' + @phraseTextID + '_defaultTranslation <> phrases.phraseText
									THEN UPDATE SET phrases.phraseText = i.' + @phraseTextID + '_defaultTranslation, lastupdated = getdate(), lastupdatedby = ' + @updatedByPersonClause 
							+ '	 OUTPUT ''UPDATE'', i.' + @entityIDColumn + ',' + @upsertUniqueIDColumnName +   ' INTO #results    
							;'		
							
						IF  (@debug > 1) print CAST (@sqlcmd as nText)
						
						EXEC (@sqlcmd)			

					SET @sqLCMD = @baseSQLCMD +		 
							 ' 	WHEN NOT MATCHED 
									THEN INSERT 	
										(phraseID,languageid,countryid,phraseText,created,createdby,lastupdated,lastupdatedby)
										values (pl.phraseid,0,0,i.' + @phraseTextID + '_defaultTranslation ,getdate(),' + @updatedByPersonClause + ',getdate(),' + @updatedByPersonClause + ')
								 OUTPUT ''UPDATE'', i.' + @entityIDColumn + ',' + @upsertUniqueIDColumnName + ' INTO #results    
							;'		
							
						IF  (@debug > 1)  print CAST (@sqlcmd as nText)
						
						EXEC (@sqlcmd)			
					
				
				
				
					FETCH next from defaultTranslations into 
					@ColName
				END

			CLOSE defaultTranslations
			DEALLOCATE defaultTranslations
		END





		 /* START OF PROCESSING FLAGS */


		 /* 2015-02-05 WAB Implement processing of all flags of same type in single query
		  */

		BEGIN

			declare @pivotFieldsWithAlias nvarchar(max), 
					@crossJoinedSubQuery  nvarchar(max), 
					@uncastdataCaseStatement nvarchar(max),  
					@dataCaseStatement nvarchar(max), 
					@columnNameCaseStatement nvarchar(max),  
					@processNullsCaseStatement nvarchar(max),  
					@crossJoinValues nvarchar(max), 
					@castAndAliased nvarchar(max),
					@columnlist nvarchar(max),
					@aliaslist nvarchar(max),
					@isNumericOrDateType bit,
					@castexpression nvarchar(max),
					@numberofColsOfType int,
					@colsProcessedCount int = 0,
					@colsProcessedCountPrevious int = 0,
					@crossJoinedTableName sysName,
					@crossJoinedTable varchar (max),
					@crossApplyTableName sysname


			/* 	We can process all flags of the same type in one go
				except we can't mix character and numeric type incoming fields because of type casting issues
			*/

			DECLARE flagTypes cursor local FAST_FORWARD for
				SELECT flagDataType , tabletype as flagOrFlagGroup, castexpression , isNumericOrDateType, count(1)
				 FROM #updatedColumns
				WHERE flagDataType <> '' 
				  and tableType in  ( 'flagGroup' , 'flag')
				GROUP BY flagDataType, tableType, castexpression , isNumericOrDateType


			OPEN flagTypes

				FETCH next from flagTypes into 
					  @flagType /* checkbox, radio, integer etc. */
					, @flagOrFlagGroup /* flag or flagGroup  (flagGroup when we are updating a complete boolean group */
					, @castexpression   /* expression to cast the column data.  Includes casting boolean Yes/No to 1/0, casting character strings to numeric/datetime */
					, @isNumericOrDateType
					, @numberofColsOfType
					
				WHILE @@fetch_status = 0 
				BEGIN

					IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': ' + @flagOrFlagGroup  + '. ' + @flagType + '. ' + convert(varchar,@numberofColsOfType) + ' Cols. ' + @castexpression  

					select  @colsProcessedCount = 0

					WHILE @colsProcessedCount < @numberofColsOfType
					BEGIN

						-- blank out some variables
						select  
							  @pivotFieldsWithAlias = ''
							, @dataCaseStatement = ''
							, @uncastdataCaseStatement = ''
							, @processNullsCaseStatement = ''
							, @crossJoinValues = ''
							, @castAndAliased = ''
							, @columnlist = ''
							, @aliaslist  = ''
							, @columnNameCaseStatement = ''
							, @commaSeparator = ''
							, @ORSeparator = ''
							, @isList = case when @flagOrFlagGroup = 'flagGroup' or @flagType = 'integerMultiple' then 1 else 0 end
							, @colsProcessedCountPrevious = @colsProcessedCount
							, @crossApplyTableName = null
							, @crossJoinedTableName = null



						/* 
							create variables used to convert all the columns of a given flag type into a single data column
							may need to deal with nulls if processNulls = 1 by setting to blank in the pivot 
						*/
						select 
							 @pivotFieldsWithAlias +=		@commaSeparator + castColumn + ' as [' + convert(varchar,flagOrFlagGroupID) +']' 
							,@dataCaseStatement +=			' when '+ convert(varchar,flagOrFlagGroupID) + ' then _.[' + convert(varchar,flagOrFlagGroupID) +']'
							,@uncastdataCaseStatement +=	' when '+ convert(varchar,flagOrFlagGroupID) + ' then _.[' + name + ']'
							,@processNullsCaseStatement +=	' when '+ convert(varchar,flagOrFlagGroupID) + ' then ' + convert(varchar,processNull)  
							,@columnNameCaseStatement +=		' when '+ convert(varchar,flagOrFlagGroupID) + ' then ''' + name + ''''
							,@castAndAliased +=				@commaSeparator + '[' + name + '] as [' + convert(varchar,flagOrFlagGroupID) + ']'
							,@columnlist +=					@commaSeparator + '[' + name + ']'
							,@aliaslist +=					@commaSeparator + '[' + convert(varchar,flagOrFlagGroupID) + ']'
							,@crossJoinValues +=			@commaSeparator + '(''' + convert(varchar,flagOrFlagGroupID)  + ''')'
							,@commaSeparator = ',' + @CR
							,@ORSeparator = ' OR ' + @CR
							,@colsProcessedCount += 1

						from 
							(	select 
									*, 
									ROW_NUMBER() OVER (ORDER BY flagOrFlagGroupID) AS RowNumber 
								from 
									#updatedColumns
								where 
										flagDataType = @flagType
									and	tableType = @flagOrFlagGroup
									and isNumericOrDateType = @isNumericOrDateType
							) as x
					 		where rowNumber BETWEEN  @colsProcessedCount + 1 AND @colsProcessedCount + @numberOfFlagColsToProcessSimultaneously


						IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': Processing cols ' + convert(varchar,@colsProcessedCountPrevious) + ' to ' + convert(varchar,@colsProcessedCount) + ': '  + replace (@columnlist,@cr,'')


						/*	this is the query which takes all columns of the same flag type and converts them into a table with a single data column
							All the types of flag/flaggroup base can use this same base query
							I could have used, but one runs into problem UNPIVOT
						 */
						SET @crossJoinedSubQuery = 	
							
							'   /* start cross join */
								SELECT 
									entityID 
									,upsertUniqueID
									,columnID as ' + @flagOrFlagGroup + 'id 
									,_.lastUpdatedByPerson 
									,data = case columns.columnID ' + @dataCaseStatement + ' end  
									,uncastdata = 
									case columns.columnID ' + @uncastdataCaseStatement + ' end
									,columnName = 
									case columns.columnID ' + @columnNameCaseStatement + ' end '
									+ case when  @flagOrFlagGroup = 'flag' then @cr+ ',
									flagGroupID = f.flagGroupID' else '' end + 
							'	FROM 
								   (
									SELECT ' 
										+ @entityIDColumn + ' as entityID, ' 
										+ @upsertUniqueIDColumnName + ' as upsertUniqueID, ' 
										+ @updatedByPersonClause + ' as lastUpdatedByPerson,	'							
										+ @pivotFieldsWithAlias + ', '
										+ @columnList + '
									FROM ' + @filteredUpsert + ' as i
									) AS _
								cross join
									(values ' + @crossJoinValues + '
									) 
									
									columns (columnID) '
							+ case when @flagOrFlagGroup = 'flag' then 
							'
								inner join flag f on f.flagid = columns.columnID
							'
							else '' end
							+		
							'		
								where  
									(
										case columns.columnID ' + @uncastdataCaseStatement + ' end  is not null	
											OR
										case columns.columnID ' + @processNullsCaseStatement + ' end  = 1
									)	
										
								 
								/* end cross join */
							'

						IF @Validate = 0 OR @flagOrFlagGroup = ('flagGroup') OR @flagType in ('IntegerMultiple') /* this cross join required for update and for validating booleans */
						BEGIN
		select	@crossJoinedTableName = '##entityUpsert_crossJoinedTable' + replace(newID(),'-','')
		select	@crossJoinedTable = @crossJoinedTableName

		select @columnList  = 'entityid,lastupdatedbyperson,uncastdata,flaggroupid,upsertuniqueid,columnName' 
								+ case when @flagOrFlagGroup = 'flag' then ',flagid' else '' end
								+ case when 1=0 and @isList = 1 then ',dataAsXML' else ',data' end

		set @sqlcmd = 'select ' + @cr + @columnList + @cr + ' into ' + @crossJoinedTableName + @cr + ' from (' + @cr + @crossJoinedSubQuery + @cr + ') as x'
		IF (@debug > 1 ) print CAST (@sqlcmd as nText) 

		exec (@sqlcmd)
		IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': crossjoinedTable created'
	END


	/* TBD @processNull needs to be set elsewhere since it can be column specific */
	set @processNull = 0 

						IF @flagOrFlagGroup = ('flagGroup')
						BEGIN 

							/* supports lists of comma separated flagIDs or flagtextIDs.  the flagtextIDs can be semi-colon separated (for salesforce)
							 delete items in deleted but not in inserted
							 convert list to XML and process
							 don't bother about items where data is blank
							*/
							SET @crossApplyTableName = '##entityUpsert_crossApplyTable' + replace(newID(),'-','')

							select 
								@crossApplyStatement = 
										'/* start cross apply */' + @CR + 
										+  '(select *, cast(''<dataitem>''+ replace(dbo.xmlformat(replace( data ,'';'','','')),'','',''</dataitem><dataitem>'')+''</dataitem>'' as xml)  as dataAsXML from ' + @crossJoinedTable + ' where isNull(data,'''') <> '''' ) as _ 
										 CROSS APPLY 
									 (SELECT f.flag.value(''.'',''varchar(100)'') as flagID FROM dataAsXML.nodes(''dataitem'') as f(flag)) as individualFlagIDs
										INNER JOIN  
									 flag f ON ((isnumeric (individualFlagIDs.flagid) = 1 AND convert(varchar,f.flagid) = individualFlagIDs.flagid  ) OR (individualFlagIDs.flagid  <> '''' and f.flagtextid = individualFlagIDs.flagid ) OR (f.name = individualFlagIDs.flagid )) AND f.flagGroupID = _.flagGroupID'
									 + @CR + '/* end cross apply */' 

														 
							/* Whether we are validating or not we need to check that the flags are all valid */
							/* basic test is this */	
								SET @sqlcmd = 'from ' 
											  + replace (@crossApplyStatement,'inner join', 'left join')
											+ ' where uncastData <> '''' and f.flagID is null'  

							IF @validate = 1 
							BEGIN
								/* check that the flags in the column actually exist */
								SET @sqlcmd = 'insert into #validateResult (entityID, upsertUniqueID, field, message,value)
											  select entityID , upsertUniqueID, columnName,''NotValidFlag'', individualFlagIDs.flagid '
											  + @sqlcmd
											    
								IF  (@debug > 0) print 'Boolean. Validate FlagIDs'
								IF  (@debug > 1) print CAST (@sqlcmd as nText)
								EXEC (@sqlcmd)
							END	
							ELSE  /* Validate for update, throw error if fails */
							BEGIN

								SET @sqlcmd = 'select @invalidFlags += @commaSeparator + columnName + '':''+ individualFlagIDs.flagid, @commaSeparator = '','' '
											  + @sqlcmd
								exec sp_executeSQL @sqlcmd, N'@invalidFlags nvarchar(max) OUTPUT, @commaSeparator char(1)',@invalidFlags = @invalidFlags OUTPUT, @commaSeparator = ''
								
								IF @invalidFlags <> ''
								BEGIN
									SET @msg = 'Invalid Value(s) ' + @invalidFlags
									RAISERROR (@msg,16,16)
									RETURN 1

								END
							
							END


							IF @validate = 0
							BEGIN


								select @columnList  = 'entityid,_.lastupdatedbyperson,uncastdata,_.flaggroupid,f.flagid,columnName' 

									set @sqlcmd = 'select ' + @columnList + ' into ' + @crossApplyTableName + ' from ' + @crossApplyStatement + ''
									IF  (@debug > 1) print CAST (@sqlcmd as nText) 
									exec (@sqlcmd)

								IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': Resolved Table Created'

								SET @basesqlcmddelete = ' 	
										FROM 
											' + @crossJoinedTable + '	AS i 
												inner join 
											flag f1 ON  f1.flagGroupID = i.flagGroupID
												inner join 
											booleanflagdata fd on  f1.flagid = fd.flagid and fd.entityid = i.entityid 
												left join 
											 ' + @crossApplyTableName + ' as _ ON  fd.flagid = _.flagid and fd.entityid = _.entityID  
													
										WHERE 
											_.flagid is null '  

								/* update the CRUD before a delete */
								SET @sqlcmd = '
											update booleanflagdata  SET lastupdatedbyPerson = ' + @updatedByPersonClause + ',lastupdatedby = ' + @updatedByClause + ',lastupdated = getdate()' + @CR
											+ @basesqlcmddelete
								IF  (@debug > 0)  print 'Boolean. Update Before Delete'
								IF  (@debug > 1)  print CAST (@sqlcmd as nText) 
								EXEC (@sqlcmd)
								IF (@debugPerformance = 1 )print convert(varchar,datediff(ms,@starttime,getdate())) + ': pre-Del update done'

								SET @sqlcmd = '
											delete booleanflagdata  
											 OUTPUT ''UPDATE'', deleted.entityid, i.upsertUniqueID INTO #results '
											+ @basesqlcmddelete
								IF  (@debug > 0)  print 'Boolean. Delete'
								IF  (@debug > 1)  print CAST (@sqlcmd as nText) 
								EXEC (@sqlcmd)
								IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': delete done'

								/* insert items not already in the table (i.e in the inserted but  not in deleted)
									Don't actually need to use merge, but only way to get at _.upsertUniqueID in the output statement
								*/
								SET @sqlcmd = 'MERGE booleanflagdata fd ' 
											+ ' USING ' + @filteredUpsert + '	AS i inner join ' + @crossApplyStatement + ' ON i.' + @entityIDColumn + ' = _.entityid '
											+ ' ON fd.entityid = _.entityid and fd.flagid = f.flagid '
											+ ' WHEN NOT MATCHED BY TARGET '
											+ ' THEN INSERT (entityid,flagid, ' +  @insertCRUDCols + ') VALUES (_.entityid, f.flagid,  ' + @insertCRUDClause + ')'
											+ ' OUTPUT ''UPDATE'', inserted.entityid,_.upsertUniqueID INTO #results ;'
								
								IF  (@debug > 0) print 'Boolean. Insert'
								IF  (@debug > 1) print CAST (@sqlcmd as nText) 
								
								EXEC (@sqlcmd)
								IF (@debugPerformance = 1 ) print convert(varchar,datediff(ms,@starttime,getdate())) + ': Merge done'


							END

						END 
						/* END OF FLAG GROUPS */


						ELSE IF @flagtype in ('integerMultiple','textMultiple')
						BEGIN
							
							select @flagTable = @flagType + 'flagdata'
							/* supports lists of comma separated values.  
							*/



							select 
								@crossApplyStatement = 
										'/* start cross apply */' + @CR + 
										'(select *, cast(''<dataitem>''+ replace(dbo.xmlformat(replace( data ,'';'','','')),'','',''</dataitem><dataitem>'')+''</dataitem>'' as xml)  as dataAsXML from ' + @crossJoinedTable + ' where isNull(data,'''') <> '''' ) as _ 
										 CROSS APPLY 
									 (SELECT aaa.bbb.value(''.'',''varchar(100)'') as data FROM dataAsXML.nodes(''dataitem'') as aaa(bbb)) as individualDataValues'
									 + @CR + '/* end cross apply */' 

							IF @validate = 1
							BEGIN
								/* if integer then check for integers 
								   and if has a linked entity then check that entity exists
								*/
								IF @flagtype in ('integerMultiple')		
								BEGIN
										SET @sqlCmd =	'
													insert into #validateResult (entityID, upsertUniqueID, field, message,value)
													select entityid,upsertUniqueID,columnName,''NotNumeric'', individualDataValues.data
													from ' + @crossApplyStatement + '  
													where individualDataValues.data <> '''' and isNumeric ( individualDataValues.data ) = 0 '

									IF  (@debug > 0) print 'Integer Multiple. Validate Numeric'
									IF  (@debug > 1) print CAST (@sqlcmd as nText) 
									EXEC (@sqlCmd)


								
								
								END
								/*  TBD Foreign Key Validation */
							
							END


							IF @validate = 0
							BEGIN
								/* Note that nulls which are not to be processed have already been filtered out */
								SET @basesqlcmddelete = ' 	
										FROM 
											' + @flagTable + ' fd 
												inner join 
											' + @crossJoinedTable + '	AS i 
													ON fd.entityid = i.entityid and fd.flagid = i.flagid
												left join 
											( ' + @crossApplyStatement + ')  
										ON  fd.entityid = _.entityid and fd.flagid = _.flagid and fd.data = individualDataValues.data 
										WHERE 
											  _.flagid is null       
									'  
									
								/* update the CRUD before a delete */
								SET @sqlcmd = '
											update ' + @flagTable + '  SET lastupdatedbyperson = ' + @updatedByPersonClause + ',lastupdated = getdate() '  + @CR
											+ @basesqlcmddelete
								IF  (@debug > 0) print 'Multiple. Update before Delete'
								IF  (@debug > 1) print CAST (@sqlcmd as nText)
								EXEC (@sqlcmd)

								SET @sqlcmd = '
											delete ' + @flagTable + '  
											 OUTPUT ''UPDATE'', deleted.entityid,i.upsertUniqueID INTO #results '
											+ @basesqlcmddelete
								IF  (@debug > 0) print 'Multiple. Delete'; 
								IF  (@debug > 1) print CAST (@sqlcmd as nText)
									EXEC (@sqlcmd)


								/* insert items not already in the table (i.e in the inserted but not in deleted)
									Don't actually need to use merge, but only way to get at _.upsertUniqueID in the output statement
								*/
								SET @sqlcmd = 'MERGE ' + @flagTable + ' fd ' 
											+ ' USING ' + @crossApplyStatement 
											+ ' ON fd.entityid = _.entityid and fd.data = individualDataValues.data and fd.flagid = _.flagid'  + @CR
											+ ' WHEN NOT MATCHED BY TARGET and individualDataValues.data <> '''' ' + @CR
											+ ' THEN INSERT (entityid,flagid,data,lastupdatedbyPerson,lastupdatedby,lastupdated,createdby,created )' + @CR 
											+ '	VALUES (_.entityid, _.flagid, individualDataValues.data  ,_.lastupdatedbyPerson,0,getdate(),0,getdate())' + @CR 
											+ ' OUTPUT ''UPDATE'', inserted.entityid,_.upsertUniqueID INTO #results ;'
											
								IF  (@debug > 0)  print 'Multiple. Merge'
								IF  (@debug > 1)  print CAST (@sqlcmd as nText)
									EXEC (@sqlcmd)




							END


						
						END


						ELSE IF @flagOrFlagGroup = ('flag')
						BEGIN
						
							IF @validate = 0
							BEGIN

								SET @joinON =  ' fd.entityid = i.entityid and fd.flagid = i.flagID ' 

								IF @flagType IN ('checkbox') 
								BEGIN

									SET @deleteWhen = ' AND ( ( i.data is null ) OR (i.data  = 0 ) )'
									SET @insertWhen = ' AND i.data  = 1 '
									SET @updateWhen = ' AND 1 = 0 '

									select @flagTable = 'booleanflagdata'			

								END
								ELSE
								IF @flagType IN ('radio') 
								BEGIN
									SET	@joinON = ' fd.entityid = i.entityid and fd.flagid IN (select flagID from flag where flag.flagGroupID =  i.flaggroupid) '
									SET @deleteWhen = ' AND (  ( i.data is null ) OR (i.data  = 0 and  fd.flagid = i.flagID) )'
									SET @insertWhen = ' AND i.data  = 1 '
									SET @updateWhen = ' AND i.data = 1 and fd.flagid <> i.flagID '

									select @flagTable = 'booleanflagdata'			

								END
/*
								ELSE
								IF @flagType IN ('integermultiple') 
								BEGIN
								set @debug = 1
								
									SET @crossJoinedSubQuery = 
															' select * from
															(' + @crossJoinedSubQuery + ') as _
														 CROSS APPLY 
											(SELECT aaa.bbb.value(''.'',''varchar(100)'') as data FROM dataAsXML.nodes(''dataitem'') as aaa(bbb)) as individualDataValues '

									SET @deleteWhen = ' AND ( /* first section only active is processNulls true */ ( ' + convert(varchar,@processNull) + ' = 1  AND i.data is null ) OR (i.uncastdata  = '''' ) )'
			--						SET @updateWhen = ' AND ' + @castColumn + ' <> fd.data AND isNull(convert(varchar,' + @castColumn + '),'''') <> ''''  '
									SET @updateWhen = ' AND i.data <> fd.data and uncastData <> ''''  '
									SET @insertWhen = ' AND uncastData <> '''' '


									select @flagTable = @flagType + 'flagdata'				

								END
*/
								ELSE
								BEGIN
									select @flagTable = @flagType + 'flagdata'			

									SET @deleteWhen = ' AND (  (  i.data is null ) '  + case when @isNumericOrDateType <> 1 then  'OR (i.uncastdata  = '''' )' else '' end + ')'  /* when incoming data is already of a numeric/date type then we can't compare to '' */
									--	SET @updateWhen = ' AND ' + @castColumn + ' <> fd.data AND isNull(convert(varchar,' + @castColumn + '),'''') <> ''''  '
									SET @updateWhen = ' AND i.data <> fd.data and uncastData ' + case when @isNumericOrDateType <> 1 then  ' <> '''' ' else 'is not null' end    
									SET @insertWhen = ' AND uncastData ' + case when @isNumericOrDateType <> 1 then  ' <> '''' ' else 'is not null' end    
				
								END
				
								-- TBD deal with updating nulls properly						

									/* SET last updated before deletes */
									SET @sqlcmd = 'UPDATE '
									+ @flagtable
									+ ' SET ' + @setCRUDClause + @CR
									+ ' FROM ' + @flagtable + ' fd' + @CR
									+ ' INNER JOIN (' + @crossJoinedSubQuery + ') AS I' + @CR
									+ ' ON ' + @joinON + @CR
									+ ' WHERE 1=1 ' +  @deleteWhen + @CR
									IF  (@debug > 0)  print @flagTable + '. Update before Delete';   
									IF  (@debug > 1) print CAST (@sqlcmd as nText) 
		
									EXEC (@sqlcmd)
							
									SET @sqlcmd = 'MERGE '
									+ @flagtable + ' AS FD ' + @CR
									+  ' USING (' + @crossJoinedSubQuery + ')  AS I' + @CR
									+ ' ON '+ @joinON + @CR
									+ ' WHEN MATCHED  ' + @deleteWhen + @CR
									+ '		THEN DELETE ' + @CR
									
									+ 
									CASE WHEN @flagType IN ('checkbox','radio') THEN 

									  ' WHEN NOT MATCHED BY TARGET ' + @insertWhen + @CR
									+ '		THEN INSERT  (entityid,flagid, ' +  @insertCRUDCols + ') ' + @CR
									+ '		VALUES (i.entityID, i.flagid, ' +  @insertCRUDClause + ')' + @CR

									+	CASE WHEN @flagType IN ('radio') THEN 	
										  ' WHEN MATCHED  ' + @updateWhen + @CR
										+ '		THEN UPDATE SET  flagid = i.flagid, ' + @setCRUDClause + @CR
									
										ELSE '' END
									
									ELSE

									
									  ' WHEN MATCHED  ' + @updateWhen + @CR
									+ '		THEN UPDATE SET  data = i.data, ' + @setCRUDClause + @CR
									+ ' WHEN NOT MATCHED BY TARGET ' + @insertWhen + @CR
									+ '		THEN INSERT  (entityid,flagid, data,' +  @insertCRUDCols + ') ' + @CR
									+ '		VALUES (i.entityID, i.flagid, i.data,' +  @insertCRUDClause + ')' + @CR
									
									END
									
									+ ' OUTPUT ''UPDATE'', isNull(inserted.entityid,deleted.entityid), i.upsertUniqueID INTO #results'    /* Note always returns an action = update */
									+ ';'
								
								
									
									IF  (@debug > 0)	print @flagTable + '. Merge';    
									IF  (@debug > 1)	print CAST (@sqlcmd as nText)
										EXEC (@sqlcmd)

							END

						END

						IF @crossJoinedTableName is not null and object_id('tempdb..' + @crossJoinedTableName) is not null
						BEGIN
							SET @sqlCmd = 'drop table ' + @crossJoinedTableName
							if @debug <= 1
							BEGIN
								exec (@sqlCmd)
							END
							ELSE
							BEGIN
								Print '/* Temporary Cross Join Table Not Deleted*/' + @sqlCmd
							END
						END

						IF @crossApplyTableName is not null and object_id('tempdb..' +@crossApplyTableName) is not null
						BEGIN
							SET @sqlCmd = 'drop table ' + @crossApplyTableName
							if @debug <= 1
							BEGIN
								exec (@sqlCmd)
							END
							ELSE
							BEGIN
								Print '/* Temporary crossApply Table Not Deleted*/' + @sqlCmd
							END

						END

					END /* end of loop @colsProcessedCount < @numberofColsOfType */

				FETCH next from flagTypes into 
				@flagType, @flagOrFlagGroup, @castexpression , @isNumericOrDateType, @numberofColsOfType

				END


			CLOSE flagTypes
			DEALLOCATE flagTypes
		END

		IF (@debugPerformance = 1 )print convert(varchar,datediff(ms,@starttime,getdate())) + ': Flags done'

		/* now do remaining flag tables which have not been converted to the UNPIVOT code - just linked flags at the moment 
			2016-01-15	WAB Performance changes of 2015-09 had managed to disable this section completely
						and the linked entity code had never worked properly - could not do an update
		*/
		DECLARE cols cursor local FAST_FORWARD for
			SELECT name, tabletype, flagDataType, flagOrFlagGroupID, hasSetToNull,  processNull, castColumn
			 FROM #updatedColumns
			WHERE tableType <> 'base'  and  (flagDataType IN ('AnyFlagTypeNotDealtWithAbove') or tableType in ('linkedFlag'))
										
		OPEN cols

			FETCH next from cols into 
			@ColName,@TableType,@flagType,@flagOrFlagGroupID,@hasSetToNullColumn, @processNull, @castColumn

			WHILE @@fetch_status = 0 
			BEGIN

				IF  (@debug > 0)
				BEGIN
					print 'ColName: ' + @colname
					print 'TableType: ' + @tableType
					print 'FlagType: ' + @FlagType
					print '@flagOrFlagGroupID: ' + convert(varchar,@flagOrFlagGroupID)
				END


				IF @tableType = 'LinkedFlag'
		  		BEGIN  

					IF @validate = 0
					BEGIN
							
						IF  (@debug > 0)
						BEGIN
							print 'Experimental code.  This is a linked flag.  Will only work (at the moment) if the id of the linked entity is on the supplied table'
							print 'TBD ought to only do this if there it is a BIT column'
							print @flagOrFlagGroupID
						END

						DECLARE @linkedEntity sysname, @linkedEntityID sysname
						select @linkedEntity = entityTable from vflagDef where flagid = @flagOrFlagGroupID
						select @linkedEntityID =  @linkedEntity + 'ID'

						IF exists (select 1 from @viewColumns where Name = @linkedEntityID)
						BEGIN
							
						select 
							@fieldname = @entityIDColumn, 
							@flagID = @flagOrFlagGroupID,
							@flagTable = @flagType + 'flagdata'			

						
									SET @joinON =  ' fd.entityid = i.' + @linkedEntity + 'id and fd.flagid = ' + @flagID
									SET @deleteWhen = ' AND (i.[' + @colname + '] is null  OR isNull(convert(varchar,i.[' + @colname + ']),'''') = 0  ) AND data = i.[' + @fieldname + ']'    /* TBD implement ignore null logic.  The convert varchar is a bit odd but otherwise seems to get problems with isNull(0,'') = '' returning true */
									SET @updateWhen = ' AND i.[' + @colname + '] = 1 AND data <> i.[' + @fieldname + '] AND isNull(convert(varchar,i.[' + @colname + ']),'''') <> 0  '
									SET @insertWhen = ' AND isNull(convert(varchar,i.[' + @colname + ']),'''') <> 0 '
									IF  (@debug > 0) print 	@flagtable
									/* SET last updated before deletes */
									SET @sqlcmd = 'UPDATE '
									+ @flagtable
									+ ' SET ' + @setCRUDClause
									+ ' FROM ' + @flagtable + ' fd'
									+ ' INNER JOIN ' + @filteredUpsert + ' AS I'
									+ ' ON ' + @joinON
									+ ' WHERE 1=1 ' +  @deleteWhen
									
									IF  (@debug > 1)
										print CAST (@sqlcmd as nText)
									EXEC (@sqlcmd)
									
									/*  Have ended up doing a delete followed by an insert here.  Otherwise was finding that the merge was trying to update the same row more than once - which is not allowed*/
							
									SET @sqlcmd = 'MERGE '
									+ @flagtable + ' AS FD '
									+  ' USING ' + @filteredUpsert + ' AS I'
									+ ' ON '+ @joinON
									+ ' WHEN MATCHED  ' + @deleteWhen
									+ '		THEN DELETE '
									+ ' OUTPUT ''UPDATE'', isNull(inserted.data,deleted.data),null INTO #results'    /* Note always returns an action = update */
									+ ';'
								
								
									IF  (@debug > 1)
										print CAST (@sqlcmd as nText)
									EXEC (@sqlcmd)


									SET @sqlcmd = 'MERGE '
									+ @flagtable + ' AS FD '
									+  ' USING ' + @filteredUpsert + ' AS I'
									+ ' ON '+ @joinON
									+ ' WHEN NOT MATCHED BY TARGET ' + @insertWhen
									+ '		THEN INSERT  (entityid,flagid,data,' +  @insertCRUDCols + ') '
									+ '		VALUES (i.' + @linkedEntity + 'id, ' + @flagid + ', i.[' + @fieldname + '],' +  @insertCRUDClause + ')'
									+ ' WHEN MATCHED  ' + @updateWhen
									+ '		THEN UPDATE SET data = i.[' + @fieldname + '], ' +  @setCRUDClause
									+ ' OUTPUT ''UPDATE'', isNull(inserted.data,deleted.data),null INTO #results'    /* Note always returns an action = update */
									+ ';'
								
								
									IF  (@debug > 1)
										print CAST (@sqlcmd as nText)
									EXEC (@sqlcmd)

					END
					END
					
				END

			IF (@debugPerformance = 1 ) print 'Flag Done ' + case when @validate = 1 then 'Validate ' else 'update ' end + @TableType + ' ' + @flagType + ' ' +  convert(varchar,@flagOrFlagGroupID) + ' ' + convert(varchar,datediff(ms,@starttime,getdate()))

			fetch next from cols into 
				@ColName,@TableType,@flagType,@flagOrFlagGroupID,@hasSetToNullColumn, @processNull, @castColumn
		END  /* end of while loop */
		
		CLOSE cols
		DEALLOCATE cols

/* WAB 2015-02-04 Added this label so can exit easily and tidily when no incoming records */
TIDYUPANDRETURN:


		DECLARE @returnTableExists bit = 0
		IF 	@returnTableName <> ''
		BEGIN
			IF object_id('tempdb..' + @returnTableName ) is not null  OR object_id(@returnTableName ) is not null
				SET @returnTableExists = 1
		END

		IF  @validate = 1
		BEGIN
			SET @sqlcmd =	' select * from  #validateResult '
			IF 	@returnTableName <> ''
			BEGIN
				IF @returnTableExists = 0
				BEGIN
					SET @sqlcmd = replace(@sqlcmd,' from ', ' into ' + @returnTableName + ' from ')
				END	
				ELSE	
				BEGIN
					SET @sqlcmd = ' insert into ' + @returnTableName  + @sqlcmd
				END	
				
			END	

			EXEC (@sqlcmd)
					
			drop table #validateResult 
		END	
		ELSE
		BEGIN
		
			/* think that this will return a single row per entity (or into a table) */
			SET @sqlcmd =				' select 
											distinct r1.action , r1.entityID , r1.upsertUniqueID '
			IF 	@returnTableName <> ''
			BEGIN
				IF @returnTableExists = 0
				BEGIN
					SET @sqlcmd = @sqlcmd  + ' into ' + @returnTableName
				END	
				ELSE
				BEGIN	
					SET @sqlcmd = ' insert into ' + @returnTableName + ' (action,entityID ,upsertUniqueID) '  + @sqlcmd
				END	
				
			END	

			SET @sqlcmd += 	'	from 
									#results	r1
										left join
									#results	r2	on r1.entityID = r2.entityID and r2.action = ''insert''
								where 
									r1.action = ''insert'' or 	r2.action is null
									
									
								union
								
								Select 
								''UNCHANGED'', ' + @entityIDColumn + ',' + isNull(@upsertUniqueIDColumnName,'''''')   /* @upsertUniqueIDColumnName might be null if no rows in incoming table so no processing has been done */
								+ ' from  ' + @filteredUpsert + ' i '
								+ ' left join #results r on r.entityID = i.' + @entityIDColumn
								+ ' where r.entityID is null '	
			
			-- IF  (@debug > 1) print CAST (@sqlcmd as nText)
			
			/* only execute the command if we're putting data into the return table or if suppress output is set to false */
			if (@suppressResult=0 or @returnTableName <> '')
			begin
				EXEC (@sqlcmd)
			end
		
		
		END



		IF  @tempTableName is not null
		BEGIN
			/* Drop temporary table created earlier*/
			SET @sqlcmd = 'drop table  '+ @tempTableName
			EXEC (@sqlcmd)

		END

		IF @debug > 1 and @upsertTableName = '#I'
			BEGIN
				set @tempTableName = '##i_' +  replace(newID(),'-','')
				SET @sqlcmd = 'select * into ' + @tempTableName + ' from #i'
				EXEC (@sqlcmd)
				Print 'A copy of the Inserted Table has been created to aid debugging queries'
				Print @tempTableName
			END

	 IF @TransactionCountOnEntry = 0 AND @validate = 0
	 COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		--print 'Error Caught.  Last SQL Statement'
		--print CAST (@sqlcmd as nText)
		IF @TransactionCountOnEntry = 0 AND @validate = 0
		ROLLBACK TRANSACTION
		IF @validate = 1
		BEGIN
			print 'Possible SQL Command which caused error'
			print CAST (@sqlcmd as nText) 
			print ''
			print ''
			select * from #validateResult
		END
		
		DECLARE 
			@errorMessage nvarchar(max) = error_message() + ' Line:' + convert(varchar,error_line()) + '. ' + convert(varchar, getdate(),114),
			@errorSeverity int = error_severity(),
			@errorState int = error_state()
			
		
		RAISERROR (@errorMessage,@errorSeverity,@errorState)
		RETURN -1
	END CATCH

return 0