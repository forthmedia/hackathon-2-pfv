IF OBJECT_ID ( 'RWInsertPromotion', 'P' ) IS NOT NULL 
    DROP PROCEDURE RWInsertPromotion;
GO

CREATE PROCEDURE RWInsertPromotion
			@RWPromotionCode 	varchar(20),	
			@RWPromotionDescription	nvarchar(100),	
			@RWPromotionTypeID 	int,		
			@OwnerPersonID		int,
			@campaignID			int
AS

DECLARE @ErrCode int

SET @ErrCode = 0

If exists(select 1 from RWPromotion where RWPromotionCode = @RWPromotionCode)
 begin
   RAISERROR ('Error : Promotion Code %s already exists',
	      16, 1, @RWPromotionCode)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

INSERT INTO RWPromotion
	(RWPromotionCode, 
	RWPromotionDescription, 
	RWPromotionTypeID, 
	OwnerPersonID,
	campaignID)
VALUES
	(@RWPromotionCode, 
	@RWPromotionDescription, 
	@RWPromotionTypeID, 
	@OwnerPersonID,
	@campaignID)

If @@Error <> 0 
 begin
   RAISERROR ('Error : Creation of promotion %s failed',
	      16, 1, @RWPromotionCode)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

EXITPOINT:
Return @ErrCode

ERRORTRAP:
GOTO EXITPOINT

