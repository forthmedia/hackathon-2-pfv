if exists (select 1 from sysobjects where name = 'ExpiringLoyaltyProgram')
	drop procedure ExpiringLoyaltyProgram

GO
CREATE PROCEDURE ExpiringLoyaltyProgram
-- =============================================
-- Author:  Nathaniel Hogeboom
-- Create date: 2009/11/26
-- Description: Sets the ExpiringLoyaltyPA flag if the loyalty flag is unchecked
-- =============================================
 @flagid int, 
 @entityid int,
 @modaction varchar(10),
 @RecordUpdatesAgainstID int, 
 @triggerInitiatedBy int
AS
Set nocount on

/*declare @tempFlagID int
declare @tempFlagGroupID int
declare @personEntity int

select @personEntity =  dbo.integerflagdata.data
from dbo.integerflagdata inner join
dbo.flag ON dbo.integerflagdata.FlagID = dbo.flag.FlagID and dbo.flag.flagtextID = 'LoyaltyPointsAdministratorID'
where EntityID = @entityid

if @personEntity <> ''
BEGIN
	if exists (select 1 from RWCompanyAccount where organisationID = @entityid)
	BEGIN
		if @modaction = 'FD' 
		BEGIN
			if @flagID = 2236 
			  BEGIN
				exec setBooleanFlag @flagid = 'ExpiringLoyaltyPA', @entityid = @personEntity , @userid = @RecordUpdatesAgainstID
			  END
		END

		if @modaction = 'FA' 
		BEGIN
			if @flagID = 2236
			  BEGIN
				exec deleteBooleanFlag @flagid = 'ExpiringLoyaltyPA', @entityid = @personEntity , @userid = @RecordUpdatesAgainstID
			  END
		END
	END
END
ELSE
BEGIN

		update RWCompanyAccount
			set AccountDeleted = 1
			where organisationID = @entityid
END
*/

declare @pointsAdminPerson int
declare @accountID int

select @pointsAdminPerson =  dbo.integerflagdata.data from dbo.integerflagdata inner join
	dbo.flag ON dbo.integerflagdata.FlagID = dbo.flag.FlagID and dbo.flag.flagtextID = 'LoyaltyPointsAdministratorID'
where EntityID = @entityid

if (@RecordUpdatesAgainstID is null)
set @RecordUpdatesAgainstID = 0

if (@pointsAdminPerson <> '')
	Begin
		if (@modAction = 'FD')	
			Begin
				exec setBooleanFlag @flagid = 'ExpiringLoyaltyPA', @entityid = @pointsAdminPerson, @userid = @RecordUpdatesAgainstID
			End
		else if (@modAction = 'FA')
		Begin
			exec deleteBooleanFlag @flagid = 'ExpiringLoyaltyPA', @entityid = @pointsAdminPerson , @userid = @RecordUpdatesAgainstID
			/*  PJP CASE 430120:  Added in other flags that need to be deleted */
			exec deleteIntegerFlag @flagid	= 'LoyaltyPointsAdministratorID', @entityid = @entityid , @userid = @RecordUpdatesAgainstID
			exec deleteBooleanFlag @flagid	= 'LoyaltyAccept', @entityid = @entityid , @userid = @RecordUpdatesAgainstID
			exec deleteTextFlag @flagid		= 'LoyaltyTsCsSignature', @entityid = @entityid , @userid = @RecordUpdatesAgainstID			
		End
	End
Else
	Begin
		update RWCompanyAccount set AccountDeleted=1, updated=Getdate(),updatedBy=@RecordUpdatesAgainstID where organisationID = @entityid
	End
