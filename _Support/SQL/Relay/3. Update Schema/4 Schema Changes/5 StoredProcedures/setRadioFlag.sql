IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'setRadioFlag' and Type = 'P')
	DROP PROCEDURE setRadioFlag
	
GO
	
CREATE     PROCEDURE [dbo].[setRadioFlag]
	  @flagid varchar(100)				-- the entityID or null if using temporary table
	, @entityid int 					-- the entityID or null if using temporary table
	, @userid int
	, @updatedByPersonID int  = null
	, @tablename sysname = '#setFlag'  -- must be a temporary table.  Requires entityID column.  Optional flagid and Data columns
AS


/*
SetRadioFlag

Sets a radio type boolean flag if not already set
If used with temporary table with data column can be used to delete a flag as well

TEST SCRIPT: See _support\tests\sql\flagStoredProcedures\test_setRadioFlag.sql
Note that code structure is very similar to setRadioFlag and bugs in one may exist in the other

WAB 2011/05/12 Modified so that it can work in 3 different ways
	1. Pass in @flagID and @entityId to set a single flag (original version) 
	2. Pass in @flagID  and a temporary table called #setFlag with a column entityID
	3. Pass in 	temporary table called #setFlag with a columns entityID and flagID

WAB 2011/10/05  LID 7935 code fell over if for some reason @flagid was passed in but was null, so added a test for the non existence of the #setflag temporary table

WAB 2015-10-13
	While modifiying deleteBooleanFlag to run off a temporary table
	Changed to run with dynamic SQL because ran into problems with flagid sometimes existing on temp table
	At this point decided that might as well add @tableName parameter
	Added support for a data column

WAB 2015-11-04 
	Fix error if radio updated using a tempTable with flagtextID

*/

Set nocount on

declare 
	  @ErrorMessage nVarchar (200)
	, @tableID int
	, @baseSqlCmd nVarChar (max)
	, @sqlCmd nVarChar (max)
	, @hasNonBooleanFlag bit 
	, @hasDataColumn  bit
	, @tempFlagID int
	, @tempFlagGroupID int
	, @tempFlagTypeID int


	
select @userid = isNull(@userID,0)  -- createdBy and LastUpdated by can't be null, but do default to zero

/*
First check that the flagids are of radio type
*/ 

IF @flagID is not null
	BEGIN -- get numeric flagid and check is a radio 

		select @tempFlagID = flagid, @tempFlagGroupID = fg.flaggroupid, @tempFlagTypeID = flagTypeid 
		from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid 
		where (convert(varchar,flagid) = @flagid OR flagTextID = @flagid )
		
		
		IF @tempFlagTypeID not in  (3) or @tempFlagTypeID is null
		BEGIN
			SET @ErrorMessage = @FlagID + ': Not a radio flag'
		END
	
	END
ELSE
BEGIN

	select @tableID = object_id('tempdb..' + @tablename)

	IF @flagID is null and exists (select * from tempdb..syscolumns where id = @tableID and name = 'flagid')
	BEGIN
		set @sqlCmd = 'select @hasNonBooleanFlag = 1 
						from ' + @tableName + ' ef
						inner join vflagdef f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
						and flagTypeID not in (2,3)'
		exec sp_executeSQL @sqlCmd,N'@hasNonBooleanFlag bit OUTPUT', @hasNonBooleanFlag = @hasNonBooleanFlag OUTPUT
		IF @hasNonBooleanFlag = 1
		  BEGIN
			set @ErrorMessage =  @tableName + ' Table contains a non-boolean flag'
		  END
	END	  
	ELSE IF @flagID is null
	BEGIN
			set @ErrorMessage =  '@fladID null and no flagID column on ' + @tableName + ' table'
	END

	select @hasDataColumn = count(1) from tempdb..syscolumns where id = @tableID and name = 'data'

END	


IF @ErrorMessage is not null 
BEGIN

	RAISERROR (@ErrorMessage, 16, 16)
	Return
END

Set nocount off
/*
Now do the update
*/
IF @entityID is null
	BEGIN -- using a temporary table
		
		IF @flagID is not null
			BEGIN  --- @flagID supplied 
				
				set @sqlCmd = ' 
					update booleanflagdata
					set 
						flagid = @tempFlagid,
						lastupdatedBy = @userid,
						lastupdatedByPerson = @updatedByPersonID,
						lastupdated = getdate(),
						createdBy =@userid,
						created = getdate()
		
					from booleanflagdata bfd1
						inner join flag f1	on bfd1.flagid = f1.flagid
						inner join flag f2  on f1.flaggroupid = f2.flaggroupid   and f1.flagid <> f2.flagid 
						inner join ' + @tablename + ' ef on ef.entityid = bfd1.entityid and f2.flagid = @tempFlagid			


					insert into booleanflagdata
						(entityid,flagid,created,createdby,lastupdated,lastupdatedby,lastupdatedByPerson)
					select
						ef.entityid,@tempFlagid,getdate(),@userid,getdate(),@userid,@updatedByPersonid
					from 
						' + @tablename + ' ef
							left join 
						booleanFlagData bfd on ef.entityid = bfd.entityid and bfd.flagid = @tempFlagid
					where bfd.flagid is null	
				'		
						+ case when @hasDataColumn = 1 then ' and ef.data = 1' else '' end
				exec sp_executeSQL @sqlCmd, N'@tempflagid int, @userid int, @updatedByPersonID int', @tempflagid = @tempflagid, @userid = @userid , @updatedByPersonID = @updatedByPersonID 				
				
			END
		ELSE
			BEGIN  --- flagID on temporary table

					/* any other radio buttons in the group can be updated to be this radio button, 
					(otherwise would have to do delete followed by insert)
					but need to change the created date and by (added WAB 2007/01/23
					2015-11-04 WAB fix error if radio updated using a tempTable with flagtextID - I was trying to set flagID to the value in the temp table rather than from table f2 
					*/
						
						set @sqlCmd = ' 
							update booleanflagdata
							set 
								flagid = f2.flagid,
								lastupdatedBy = @userid,
								lastupdatedByPerson = @updatedByPersonID,
								lastupdated = getdate(),
								createdBy =@userid,
								created = getdate()
				
							from booleanflagdata bfd1
								inner join flag f1	on bfd1.flagid = f1.flagid
								inner join flag f2  on f1.flaggroupid = f2.flaggroupid   and f1.flagid <> f2.flagid 
								inner join ' + @tablename + ' ef on ef.entityid = bfd1.entityid and (f2.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f2.flagID) = convert(varchar(200),ef.flagid))
				
							
							insert into booleanflagdata
								(entityid,flagid,created,createdby,lastupdated,lastupdatedby,lastupdatedByPerson)
							select
								ef.entityid,f.flagid,getdate(),@userid,getdate(),@userid,@updatedByPersonid
							from 
							' + @tableName + ' ef
								inner join
							flag f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
									left join 
							booleanFlagData bfd on ef.entityid = bfd.entityid and bfd.flagid = f.flagid
							where bfd.flagid is null	
						'	
						+ case when @hasDataColumn = 1 then ' and ef.data = 1' else '' end
	
					exec sp_executeSQL @sqlCmd, N'@userid int, @updatedByPersonID int', @userid = @userid , @updatedByPersonID = @updatedByPersonID 				
				
			END
			
			IF @hasDataColumn = 1 
			BEGIN
				create table #deleteFlags (entityID int)
				declare @fieldList varchar (100) = 'entityid'
				
				IF @flagID is null
				BEGIN
					alter table #deleteFlags add flagid varchar(100)
					set @fieldList += ',flagid'
				END
					
				SET @sqlCmd = '
					insert into #deleteFlags (' + @fieldList + ' )
					select ' + @fieldList + '
					from ' + @tableName + '
					where data = 0
				'	
				print @sqlCmd
				exec (@sqlCmd)				
				
				exec deleteBooleanFlag
						@entityid = @entityid
						, @flagid = @flagid 
						, @userid = @userid 
						, @updatedByPersonID = @updatedByPersonID 
						, @tableName = '#deleteFlags'

			
			END
	END
ELSE	
	BEGIN -- @flagID and @entityID supplied

			-- is it already set?	
			IF not exists (select 1 from booleanflagdata bfd where  entityid = @entityid and flagid = @tempFlagID)
				BEGIN
					-- delete any other radios in group, but update lastupdatedby to current user so that modtriggers get correct info
					update booleanflagdata set lastupdatedby = @userid, lastupdated = getdate(),lastupdatedbyperson = @updatedByPersonID from booleanflagdata inner join flag on booleanflagdata.flagid = flag.flagid where booleanflagdata.flagid = flag.flagid and flag.flaggroupid = @tempflaggroupid and entityid = @entityid 
					delete booleanflagdata from booleanflagdata inner join flag on booleanflagdata.flagid = flag.flagid where booleanflagdata.flagid = flag.flagid and flag.flaggroupid = @tempflaggroupid and entityid = @entityid 
	
					insert into booleanflagdata (entityID,flagID, created, createdby,lastupdated,lastupdatedby,lastupdatedbyperson )
					values (@entityid, @tempflagid, getdate(),@userid,getdate(),@userid,@updatedByPersonID)
	
				END

	END

