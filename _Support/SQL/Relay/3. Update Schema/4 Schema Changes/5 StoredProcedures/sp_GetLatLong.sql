ALTER PROCEDURE [dbo].[sp_GetLatLong] 

@Postalcode 	nvarchar(20),
@Place 	nvarchar(50),
@Province 	nvarchar(50),
@CountryIDs 	nvarchar(500) 

AS

/*
WAB 2010/11/29 
Made changes so that don't use @place or @province to look for matches if they are blank
Also don't match on postalcode when postalcode in the db is blank

*/

	
DECLARE @CMD nvarchar(4000)
DECLARE @c int
SET @c=0

/* Remove any spaces from post code */

--SELECT @Postalcode 	= REPLACE(@PostalCode,' ','')
SELECT @Place = REPLACE(@Place,'''','')   /* remove single quotes from place */
SELECT @Province = REPLACE(@Province,'''','')   /* remove single quotes from province */

CREATE TABLE #tempISO (ISOCode3 varchar(10))

SELECT @CMD='INSERT
		INTO		#tempISO
		(ISOCode3)
		SELECT 	ISOCode3
		FROM		geoCountry G, Country C
		WHERE	
				C.IsoCode = G.ISOCode2
		AND		C.CountryID IN (' + @CountryIDs + ')'
EXEC (@CMD)


---40
/* select records from geodata that match postcode, place and province, and are in a required country */

CREATE TABLE #tempLatLong
	(
	DecimalLatitude float,
	DecimalLongitude float,
	Place varchar(50),
	PostalCode varchar(25),
	Province varchar(50),
	CountryDescription varchar(50)
	)

-- perfect postcode match
if (@Postalcode <>'')
	begin
	INSERT INTO  
	#tempLatLong
	SELECT DISTINCT 
		G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
	from geodata G, #tempISO I, Country C, geoCountry GC
	where 		(
			(G.postalcode = @PostalCode )
			)
	AND		G.ISOCode = I.ISOCode3
	AND 		G.ISOCode = GC.ISOCode3
	AND		C.ISOCode = GC.ISOCode2
	SET @c=@@rowcount
	print 'perfect postcode - ' + convert(varchar(5),@c)

	if (@c = 0)
		begin
		INSERT INTO  
		#tempLatLong
		SELECT DISTINCT 
			G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
		from geodata G, #tempISO I, Country C, geoCountry GC
		where 		(
				(replace(G.postalcode,' ','') = @PostalCode )
				)
		AND		G.ISOCode = I.ISOCode3
		AND 		G.ISOCode = GC.ISOCode3
		AND		C.ISOCode = GC.ISOCode2
		SET @c=@@rowcount
		print 'perfect postcode - ' + convert(varchar(5),@c)
	end

	-- NJH 2007/12/04 added partial postal code only
	if ((@c = 0) and (@place = '') and (@province = '') and @postalcode <> '')
		Begin
		INSERT INTO  
		#tempLatLong
		SELECT DISTINCT 
			G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
		from geodata G, #tempISO I, Country C, geoCountry GC
		where 	
			G.postalcode <> ''    /* WAB Added 2010/11/29  */
		AND	
			(
			(REPLACE(G.postalcode,' ' ,'') like @PostalCode + '%'
			OR		
			@PostalCode like  REPLACE(G.postalcode,' ','') + '%'	))
		AND		G.ISOCode = I.ISOCode3
		AND 		G.ISOCode = GC.ISOCode3
		AND		C.ISOCode = GC.ISOCode2
		SET @c=@@rowcount
		print 'partial postcodes - ' + convert(varchar(5),@c)
	end

 	if ((@c = 0) and (@place <> '') and (@PostalCode <> ''))
		Begin
		INSERT INTO  
		#tempLatLong
		SELECT DISTINCT 
			G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
		from geodata G, #tempISO I, Country C, geoCountry GC
		where 		(
			(REPLACE(G.postalcode,' ' ,'') like @PostalCode + '%'
			OR		
			@PostalCode like  REPLACE(G.postalcode,' ','') + '%'	))
		AND g.place = @place
		AND		G.ISOCode = I.ISOCode3
		AND 		G.ISOCode = GC.ISOCode3
		AND		C.ISOCode = GC.ISOCode2
		SET @c=@@rowcount
		print 'partial postcodes and perfect place - ' + convert(varchar(5),@c)
		end
	end

---90

--- perfect place and province match
if (@c = 0 and (@place <> '') and (@Province <> '')) 
	begin
	print 'in'
	if (@c = 0)
		Begin

		INSERT INTO  
		#tempLatLong
		SELECT DISTINCT 
			G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
		from geodata G, #tempISO I, Country C, geoCountry GC

		where 		(
				(G.place = @Place)
			AND	(G.province = @Province 
				or G.province in (select name from province where countryid = c.countryid and abbreviation = @province)

				)

				)

		AND		G.ISOCode = I.ISOCode3
		AND 		G.ISOCode = GC.ISOCode3
		AND		C.ISOCode = GC.ISOCode2
		SET @c=@@rowcount
		print 'perfect place and province' + convert(varchar(5),@c)
		end
	end

-- place match
if (@c = 0 and @place <> '') 
	begin
	print 'in'
	if (@c = 0)
		Begin

		INSERT INTO  
		#tempLatLong
		SELECT DISTINCT 
			G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
		from geodata G, #tempISO I, Country C, geoCountry GC

		where (G.place = @Place)

		AND		G.ISOCode = I.ISOCode3
		AND 		G.ISOCode = GC.ISOCode3
		AND		C.ISOCode = GC.ISOCode2
		SET @c=@@rowcount
		print 'perfect place ' + convert(varchar(5),@c)
		end
	end

if (@c = 0 and @place <> '' and @province <> '')
	Begin

	INSERT INTO  
	#tempLatLong
	SELECT DISTINCT 
		G.DecimalLatitude, G.DecimalLongitude, G.Place, G.PostalCode, G.Province, C.CountryDescription
	from geodata G, #tempISO I, Country C, geoCountry GC

	where 		(
			(REPLACE(G.place,'''','') like '%' +  @Place + '%'
			OR	
			'%' + @Place + '%' like REPLACE(G.Place,'''',''))

		AND	
			(REPLACE(G.province,'''','') like '%' + @Province + '%'
			OR
			'%' + @Province + '%' like REPLACE(G.province,'''',''))
			)

	AND		G.ISOCode = I.ISOCode3
	AND 		G.ISOCode = GC.ISOCode3
	AND		C.ISOCode = GC.ISOCode2
	SET @c=@@rowcount
	print 'partial place or province' + convert(varchar(8),@c)
	end

/* See what results have been returned. If all results have the smae Lat/Long, then no point refining location,
	so just return one record. If there are different Lat/Longs, then return the complete list */

DECLARE @LatLongCount int

SELECT @LatLongCount =  COUNT (DISTINCT CONVERT(varchar, DecimalLatitude) + CONVERT(varchar, DecimalLongitude) + CONVERT(varchar, PostalCode))
FROM #tempLatLong

if @LatLongCount = 1
begin
SELECT TOP 1  convert(varchar,DecimalLatitude) as DecimalLatitude,
		convert(varchar,DecimalLongitude) as DecimalLongitude,
		Place,
		PostalCode ,
		Province,
		CountryDescription     
FROM #tempLatLong
end
else
begin
SELECT 	 convert(varchar,DecimalLatitude) as DecimalLatitude,
		convert(varchar,DecimalLongitude) as DecimalLongitude,
		Place,
		PostalCode ,
		Province,
		CountryDescription     
FROM #tempLatLong
ORDER BY CountryDescription, Province, Place, Postalcode
end
GO
