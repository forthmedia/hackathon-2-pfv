IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DedupeEntityAmendChild' and Type = 'P')
	DROP PROCEDURE DedupeEntityAmendChild

GO

CREATE proc [dbo].[DedupeEntityAmendChild]
			@RemoveEntityId	int,
			@RetainEntityId	int,
			@Entity		varchar(32),	-- Name of entity being deduped (eg Person)
			@TableName	sysname, 	-- Name of table in which data needs to be updated
			@ColumnName	sysname,	-- Name of column in which data needs to be updated
			@FlagData	bit = 0,	-- Is the table a flag data table?
			@Run		varchar(5) = 'No',
			@Trace 		bit = 0		-- Switch to enable/disable printing of dynamic SQL commands
as

/***************************************************************************************
** Name   : DedupeEntityAmendChild
** Created: 21/Jun/2001
** By     : JVH
**
** Purpose: 
** Performs updates to tables where necessary as part of dedupe processing
** Return
** Arguments:
**      See above
**
** s: 0 = OK
**          1 = Updates/deletes failed; try again on next iteration
**          2 = Fatal error occurred in updates or deletes - abort
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00	    21/Jun/2001  JVH  Ticket 5434:
**                            o Original, created by extracting code from DedupeEntity
** 1.01     28/Aug/2001  JVH  Ticket 6323:
**                            o Bug fix: Need to look at indexes, not constraints.
** 1.02     24/Oct/2001  JVH  Ticket 6323:
**                            o Bug fix: Need to examine IndexDescription to determine if a PK,
**                              not IndexKeys
**                            o Bug fix: Code to include updated column in 'where...' clause being skipped
**
** 1.03	    12/7/05	 WAB  o Bug Fix: when deduping tables with entityid column, there was nothing to check what type of entity the entityid.
**					bug was discovered on selectiontag table, but affects all other tables of a similar type.  
**					It is necessary to link to either link to entityType table or filter on entity or entityname column or in the case of selectiontag link up to the selection table.
** 	11/12/06 	WAB 	corrected a major bug 
**  	2009/10/18 	WAB 	Added handling of related files.  However note that it does not deal with related file categories which should only have a single file (so could end up with two files linked to a single entity)
				Noticed that transaction was created when @run = 'no', corrected

	2010/04/22	WAB	added support for entityID columns of form xxx_entityID
****************************************************************************************/


/*===========================================================================================*/
-- Initialisations
/*===========================================================================================*/
declare @vsqlcmd nvarchar(2000),	-- Holds SQL command to show rows to delete/update
	@vsqlcmd2 nvarchar(2000),	-- Holds SQL command to show rows to delete/update
	@rsqlcmd nvarchar(2000),	-- Holds SQL command to update rows
	@rsqlcmd2 nvarchar(2000),	-- Holds SQL command to select PKs of rows for deletion
	@rsqlcmd3 nvarchar(2000),	-- Holds SQL command to delete rows
	@sqlupdate nvarchar(200),	-- Holds SQL 'Update' fragment
	@sqlselect nvarchar(200),	-- Holds SQL 'Select' fragment
	@sqldelete nvarchar(200),	-- Holds SQL 'Delete' fragment
	@sqlfrom   nvarchar(200),	-- Holds SQL 'From' fragment
	@sqljoin   nvarchar(200),	-- Holds SQL 'Join' fragment
	@sqlwhere  nvarchar(200),	-- Holds SQL 'Where' fragment
	@sqlexists nvarchar(200),	-- Holds SQL 'Exists' fragment
	@sqlnotexists nvarchar(200),	-- Holds SQL 'Not Exists fragment
	@sqlselectinto nvarchar(200),	-- Holds SQL 'Select ... Into' fragment
	@sqlsubquery nvarchar(2000),	-- Holds SQL subquery fragment to identify potential duplicate rows
	@sqlsubquery0 nvarchar(2000),	-- Holds SQL initial subquery fragment to identify potential duplicate rows
	@sqlsubquery1a nvarchar(2000),	-- Holds SQL subquery fragment for identification of deletions
	@sqlsubquery1b nvarchar(2000),	-- Holds SQL subquery fragment for identification of updates
--	@Tablename varchar(32),
--	@ColumnName sysname,

	@EntityTypeId int,
	@KeyColumnName sysname,		-- Name of key column in flag table
	@IndexName sysname,
	@KeyList sysname,		-- Name of the temp table to hold key values of records to be deleted
	@ErrCode int,
	@IndexCount int,		-- Counter to control construction of SQL commands
	@ColumnCount int,		-- Counter to control construction of SQL commands
	@AndOrText nvarchar(10),	-- Contains variable text for insertion in SQL commands
	@StartedTran bit,		-- Indicates whether a transaction was started inside this proc
	@IsInPK bit,			-- Indicates whether the column being updated is part of the Primary Key
					-- in the table to which it belongs
	@HasIdentityCol bit,		-- Indicates whether the table has an IDENTITY column
	@HasPK		bit,		-- Indicates whether the table has a Primary Key
	@FoundColumns 	bit,		-- Indicates whether any columns were found in table constraints
	@IndexKeys 	nvarchar(2078),	-- Holds list of columns in an index
	@IndexDescription nvarchar(210),-- Holds description of an index
	@SplitPos	int,		-- Used for parsing list of index columns
	@Remainder	nvarchar(2078),	-- Holds copy of index column list
	@joinToEntityTable bit ,         -- set if the table has an entitytypeid column which needs to be resolved to the tablename
	@EntityNameColumn varchar(40) ,  -- set if the table has an entityid column and another column which contains the name of the entityType - column might be entity or entityname
	@EntityTypeIDColumn varchar(50)  -- usually just entityTypeID but can be xxx_entityTypeID

Set @ErrCode = 0
Set @StartedTran = 0
Set @IsInPK = 0
Set @HasPK = 0
Set @FoundColumns = 0
set @joinToEntityTable = 0
set @EntityNameColumn = ''
set @EntityTypeIDColumn = replace(@columnName,'entityID','') + 'entityTypeID'   -- sometimes this will end up as garbage, but when @columnName is of form xxxEntityID it will be xxxEntityTypeID   (where xxx might be nothing)


/*------------------*/
-- this section added by WAB 12/7/05 to correct a bug which was causing personids in the selectiontag table to be deduped as if they were entityids
-- If the table isn't a flag table and has an entityid column, may need to link to entityTypeid
/*------------------*/
If  @FlagData <> 1 and @ColumnName like '%entityID'
Begin
  if exists (select * from information_schema.columns where TABLE_NAME = @tablename and COLUMN_NAME = @EntityTypeIDColumn /*'entityTypeID'*/)	
		select @joinToEntityTable = 1 
  else if exists (select * from information_schema.columns where TABLE_NAME = @tablename and COLUMN_NAME = 'entityType')	
		select @EntityNameColumn = 'entityType'

  else if exists (select * from information_schema.columns where TABLE_NAME = @tablename and COLUMN_NAME = 'entity')	
		select @EntityNameColumn = 'entity'

  else if @tablename in ('selectionTag','relatedFile')
	print ' '		-- special case OK

  else
	begin
		print @TableName + ' No column for entityType'
		return 0 
	end

End



-- Initialise the sql command fragments
-- WAB 11/12/06 corrected a major bug in here (which basically stopped it working! - the join to the schemaTable was being done on @tablename rather then @entity)
Set 	@sqlupdate	= N'update data  set data.['+@ColumnName+N'] = @RetainEntityId'
Set 	@sqlfrom	= N' from ['+@TableName + N'] data '
Set 	@sqljoin	= case  when @tablename = 'selectionTag' then 'inner join selection s on data.selectionid = s.selectionid and tablename = ''' + @Entity + ''''
				when @tablename = 'relatedFile' then 'inner join relatedFileCategory rfc on data.FileCategoryid = rfc.FileCategoryid and fileCategoryEntity = ''' + @Entity + ''''
				when @joinToEntityTable = 1 then ' join schemaTable as entityType on entityType.entityTypeid = data.' + @EntityTypeIDColumn + ' and entityType.entityName = ''' + @Entity + ''''
				when @FlagData = 0 then N''
			       else N' join [vFlagDef] v on data.[FlagID] = v.[FlagID] and v.[EntityTable] = ''' + @Entity + N''''
					
			  end
Set 	@sqlselect	= N'select ''' + @tablename    + ''' as tableName, data.* '    -- WAB 13/06/07 added name of table for ease of reading results
Set	@sqldelete	= N'Delete data '
Set 	@sqlwhere	= N' where data.['+ case when @FlagData = 0 then @ColumnName else N'EntityID' end +N'] = @RemoveEntityId' 
				+  case when @EntityNameColumn <> '' then ' and ' + @EntityNameColumn + ' = ''' + @entity + '''' else N'' end
Set	@sqlexists	= N' exists '
Set	@sqlnotexists 	= N' not exists '
Set	@sqlselectinto	= N'Select '
Set	@sqlsubquery0 	= N' (select 1 from [' + @TableName + N'] dat1 where dat1.[' + @ColumnName + N'] = @RetainEntityID' 
Set	@sqlsubquery1a 	= N''
Set	@sqlsubquery1b 	= N''
Set	@KeyList 	= N'##' + convert(nvarchar, @@spid) + N'_' + @TableName

select	@vsqlcmd	= '',
	@rsqlcmd	= '',
	@vsqlcmd2 	= '',
	@rsqlcmd2 	= '',
	@rsqlcmd3 	= ''

set @IsInPK = 0

if @trace = 1 print 'WAB: ' + @Tablename
if @trace = 1 print 'WAB: ' + @sqljoin
if @trace = 1 print 'WAB: ' + @sqlwhere


-- If this proc wasn't called from inside a transaction, start a new one
-- WAB 2009/10/18 noticed that if this procedure was entered with @run=No then a transaction was created and not comitted, best not to create transaction (as per dedupePerson)
If @@TranCount = 0 and @run= 'Yes'
 begin
   if @trace = 1 print 'Starting Tran'
   set @StartedTran = 1
   BEGIN TRANSACTION
 end


/*===========================================================================================*/
-- Process the table
/*===========================================================================================*/
print '***============ ' + @Tablename + ' (' + @ColumnName + ') ============***'








/*-------------------------------------------------------------------------------------------*/
-- ALTER  and populate a temp table to hold details of indexes on this table
/*-------------------------------------------------------------------------------------------*/
Create table #IndexInfo(IndexName sysname, IndexDescription nvarchar(210), IndexKeys nvarchar(2078))

insert #IndexInfo
 exec sp_helpindex @TableName

-- Modify the index column list so that it's bounded and delimited by exclamation marks - makes 
-- identifying the columns easier
update 	#IndexInfo
set 	IndexKeys = '!' + replace(IndexKeys,', ','!') + '!'
			
/*-------------------------------------------------------------------------------------------*/
-- Create and loop through a cursor to get the constraints applying to this column, then loop through each constraint to get the
-- names of the other columns.
--
-- Use this to create a command to identify/display any rows which can be updated and any which would violate uniqueness
-- if updated.
/*-------------------------------------------------------------------------------------------*/

--
--      If the column to be updated is part of the key, or belongs to a unique constraint, check whether the dedupe
--      will violate the uniqueness. If so, try to delete any offending rows.
--
--      If deletion of rows which would violate uniqueness fails because of an RI violation, this can have two possible resolutions:
--      1. If the column to be updated is part of the Primary Key, then it should also appear in the 'child' table. If that happens,
--         the offending 'child' table may be yet to be processed, after which the deletes could work. In order to give the deletions
--         another chance, the table can be skipped (and not added to the list of tables processed). This will mean it gets
--         tried again the next time round, by which time the 'child' table may have been processed.
--      2. If the column to be updated is not part of the Primary Key, then the potential orphans in the offending child table will
--         either need to be deleted themselves, or transferred to a new parent
--
--      If the column is part of the PK but will not violate uniqueness if it's updated, and the update fails because of an RI violation, 
--      then it's necessary to create a new row in the table to act as the new parent for the offending child records.  
--      If this isn't done, when the dedupe process tries to update the column in the child table it'll also fail because of RI.
--


DECLARE Index_csr INSENSITIVE cursor
FOR
	SELECT 	distinct IndexName, IndexDescription, IndexKeys
	FROM 	#IndexInfo
	WHERE 	(IndexDescription like'%unique%' or
		IndexDescription like'%primary key%')
	AND 	IndexKeys like'%!' + @ColumnName + '!%'

OPEN Index_csr

/*-------------------------------------------------------------------------------------------*/
-- Loop through the cursor to get each index in turn
/*-------------------------------------------------------------------------------------------*/
Set @IndexCount = 0

WHILE 1=1
 BEGIN
	FETCH next
	FROM  Index_csr
	INTO  @IndexName, @IndexDescription, @IndexKeys

	IF @@Fetch_Status <> 0 BREAK

	-- Set a flag if the index is a PK
	If @IndexDescription like'%primary key%'
	   set @IsInPK = 1

	Set @IndexCount = @IndexCount + 1
	Set @AndOrText = case when @IndexCount = 1 then ' and (' else ' or ' end

	-- Prime the subquery fragments with the details relating to the column being updated
	SET @sqlsubquery1a	= @sqlsubquery1a + 
				  case when @IndexCount = 1 then ' and (' else ' or ' end + 
				  @sqlexists  + 
				  @sqlsubquery0 
	SET @sqlsubquery1b	= @sqlsubquery1b + 
				  case when @IndexCount = 1 then ' and (' else ' and ' end + 
				  @sqlnotexists + 
				  @sqlsubquery0 

	/*-------------------------------------------------------------------------------------------*/
	-- Loop through the columns in the index. If the table is a flagdata
	-- table, exclude any identity column that may exist because this is irrelevant
	/*-------------------------------------------------------------------------------------------*/
	Set @SplitPos = 1
	Set @Remainder = @IndexKeys
	
--	Set @ColumnCount = 0

	While @SplitPos < len(@Remainder)
	 Begin
		Set @Remainder = substring(@Remainder, @SplitPos + 1, len(@Remainder) - @SplitPos)
		Set @SplitPos = charindex('!', @Remainder)
		Set @KeyColumnName = substring(@Remainder, 1, @SplitPos-1)

		/*-------------------------------------------------------------------------------------------*/
		-- Skip the column if it's the column we're updating, or if the table is a flag data table and the column is
 		-- an IDENTITY column
		/*-------------------------------------------------------------------------------------------*/
		If  (@KeyColumnName = @ColumnName or
	   	  	(@FlagData = 1 and (select sc.status & 128 
					from  syscolumns sc 
					join sysobjects so on sc.id = so.id
							   and so.name = @TableName
							   and sc.name = @KeyColumnName) = 128))	-- Exclude IDENTITY column
		 CONTINUE

		/*-------------------------------------------------------------------------------------------*/
		-- Otherwise use the column to extend the SQL command 
		/*-------------------------------------------------------------------------------------------*/
		Set @FoundColumns = 1
		Set @ColumnCount = @ColumnCount + 1
	
		-- If this is the first 'valid' column found in the index, add in @sqlsubquery0
--		If @ColumnCount = 1
--		 Begin
--		 End

		-- Append the join clause involving the current index column
		SET @sqlsubquery1a  = @sqlsubquery1a  + N' and dat1.[' + @KeyColumnName + N'] = data.[' + @KeyColumnName + N']'
		SET @sqlsubquery1b  = @sqlsubquery1b  + N' and dat1.[' + @KeyColumnName + N'] = data.[' + @KeyColumnName + N']'

	 End	-- Loop through columns in the index

	/*-------------------------------------------------------------------------------------------*/
	-- Add the closing bracket for columns in this index, if any columns were found
	/*-------------------------------------------------------------------------------------------*/
--	If @ColumnCount > 0
--	 Begin
		SET @sqlsubquery1a  = @sqlsubquery1a  + N')'
		SET @sqlsubquery1b  = @sqlsubquery1b  + N')'
--	 End

 END	-- Loop through indexes

/*-------------------------------------------------------------------------------------------*/
-- Add final closing brackets, etc
/*-------------------------------------------------------------------------------------------*/
--If @FoundColumns = 1
If @IndexCount > 0
 begin
	SET @sqlsubquery1a  = @sqlsubquery1a  + N')'
	SET @sqlsubquery1b  = @sqlsubquery1b  + N')'
 end
Else
	SET @sqlsubquery1a  = N' and 1=0'

-- Tidy up the cursor	
CLOSE Index_csr
DEALLOCATE Index_csr

/*-------------------------------------------------------------------------------------------*/
-- Create and loop through a cursor to get the columns in the Primary Key
/*-------------------------------------------------------------------------------------------*/
DECLARE PK_csr insensitive cursor
FOR	
	SELECT distinct kcu.Column_Name
	FROM	INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
	JOIN 	INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on kcu.Table_Name = tc.Table_Name
							and kcu.Constraint_Name = tc.Constraint_Name
							and tc.Constraint_Type ='PRIMARY KEY'
	WHERE 	kcu.Table_Name = @TableName

OPEN PK_csr

-- Loop through the PK cursor
Set @ColumnCount = 0

WHILE 1=1
 BEGIN
	Fetch 	Next
	From	PK_csr
	Into	@KeyColumnName

	If @@Fetch_Status <> 0  BREAK

	Set @ColumnCount = isnull(@ColumnCount,0) + 1
	Set @HasPK = 1

	Set @sqlselectinto =	
			@sqlselectinto + 
			case when @ColumnCount > 1 then N', data.' else N' data.' end + 
			@KeyColumnName 
 END

close PK_csr
deallocate PK_csr

SET @sqlselectinto  = @sqlselectinto + N' into '

/*-------------------------------------------------------------------------------------------*/
-- Construct the final SQL commands
/*-------------------------------------------------------------------------------------------*/
Set @rsqlcmd 	= @sqlupdate + @sqlfrom + @sqljoin + @sqlwhere + @sqlsubquery1b			-- Command to update rows
Set @rsqlcmd2 	= @sqlselectinto + @keylist + @sqlfrom + @sqljoin + @sqlwhere + @sqlsubquery1a	-- Command to insert PKs of rows for deletion 
												-- into temp table
Set @vsqlcmd	= @sqlselect + ', ''To Update'' ' + @sqlfrom + @sqljoin + @sqlwhere + @sqlsubquery1b			-- Command to list rows to be updated
Set @vsqlcmd2	= @sqlselect + ', ''To Delete'' ' +  @sqlfrom + @sqljoin + @sqlwhere + @sqlsubquery1a			-- Command to list rows to be deleted 
Set @rsqlcmd3 	= @sqldelete + @sqlfrom + @sqljoin + @sqlwhere + @sqlsubquery1a			-- Command to delete rows


if @trace = 1 print '@sqlsubquery1b: ' +  @sqlsubquery1b
if @trace = 1 print '@rsqlcmd: ' +  @rsqlcmd
if @trace = 1 print '@rsqlcmd2: ' +  @rsqlcmd2
if @trace = 1 print '@rsqlcmd3: ' + @rsqlcmd3
if @trace = 1 print '@vsqlcmd: ' + @vsqlcmd
if @trace = 1 print '@vsqlcmd2: ' + @vsqlcmd2

/*-------------------------------------------------------------------------------------------*/
-- If the table has a PK, populate the temp table with the PKs of rows to be deleted
/*-------------------------------------------------------------------------------------------*/
If @HasPK = 1
	exec sp_executesql @rsqlcmd2,
			N'@RetainEntityID int, @RemoveEntityID int',
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID

/*-------------------------------------------------------------------------------------------*/
-- If this is a live run, try to do the deletes. If they work (or they don't but the column is in the PK), do the updates.
-- If it's not a live run, just list the deletes and updates.
/*-------------------------------------------------------------------------------------------*/
-- Do/show deletes
If @HasPK = 1
   Begin
	print ' calling DedupeEntityCascadeDelete.  Keylist =  ' + @keylist + ' tablename = ' + @tablename

	EXEC @ErrCode = DedupeEntityCascadeDelete 
				@TableName 	= @TableName,
				@KeyListTable 	= @KeyList,
				@IsInPK 	= @IsInPK,
				@Run 		= @Run,
				@Trace 		= @Trace
  End
Else
 Begin
	If @Run = 'Yes'
	 Begin
		print '>>> Deleting rows'
		exec sp_executesql @rsqlcmd3,
			N'@RetainEntityID int, @RemoveEntityID int',
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID
		Set @ErrCode = @@Error
	 End
	Else
	 Begin
		print '>>> The following rows will be deleted: ' + @TableName
		if @trace = 1 print N'    ' + @vsqlcmd
		exec sp_executesql @vsqlcmd2,
			N'@RetainEntityID int, @RemoveEntityID int',
			@RetainEntityID = @RetainEntityID,
			@RemoveEntityID = @RemoveEntityID
	 End

 End		

If @Run = 'yes'
 Begin
    IF @ErrCode = 0 or 				-- i.e. successful
	(@ErrCode = 547 and  @IsInPK = 1)	-- i.e. failed with RI violation & is in PK
     Begin
    	print '>>> Update rows'
    	if @trace = 1 print N'    ' + isnull(@rsqlcmd, 'NONE')
	EXEC sp_executesql @rsqlcmd,
			N'@RemoveEntityId INT, @RetainEntityID INT',
			@RemoveEntityId,
			@RetainEntityID

	Set @ErrCOde = @@Error

	-- If the update fails and the column is in the PK, it indicates that there are children for which
	-- a new parent row is required. Call ..CreateCopy to insert these parent rows (which will be copies of the row 
	-- being removed - the original rows will then be treated as duplicates the next time round, by when the 
	-- children will have been updated, thus allowing these 'duplicates' to be deleted).
	If @ErrCode <> 0
	 Begin
		If @IsInPK = 1
		 Begin
	 		exec @ErrCode = DedupeEntityCreateCopy
					@TableName 	= @TableName,
					@ColumnName 	= @ColumnName,
					@RetainEntityID = @RetainEntityID,
					@RemoveEntityID = @RemoveEntityID,
					@Run 		= @Run,
					@Trace		= @Trace,
					@SqlSubquery 	= @sqlsubquery1b

			If @ErrCode <> 0 -- Fatal Error
			 Begin
				Set @ErrCode = 2
			  	GOTO ErrorTrap 
			 End
			Else		-- Can try again next time round
			 Begin
			  	Set @ErrCode = 1
			 End
		 End
		Else	-- Fatal Error
		 Begin
			Set @ErrCode = 2
	 		Goto ErrorTrap
		 End
	 End
     End
    Else	-- Fatal error
     Begin
	Set @ErrCode = 2
	Goto ErrorTrap
     End		
 End
Else
 Begin
	print '>>> Rows to be updated :'
	if @trace = 1 print N'    ' + @vsqlcmd
	EXEC sp_executesql @vsqlcmd,
		N'@RemoveEntityId INT, @RetainEntityID INT',
		@RemoveEntityId,
		@RetainEntityID
	print @@rowcount
 End

if @trace = 1 print 'done ' + @TableName 
/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
-- Get rid of the Key List Table (if one was created)
If @HasPK = 1
 Begin
	Set @rsqlcmd = N'Drop table ' + @KeyList
	exec sp_executesql @rsqlcmd
 End

RETURN @ErrCode

/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:

If @@TranCount > 0 and @StartedTran = 1
   ROLLBACK TRANSACTION

GOTO ExitPoint

