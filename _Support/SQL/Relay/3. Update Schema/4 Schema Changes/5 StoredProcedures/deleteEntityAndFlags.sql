createPlaceHolderObject 'procedure','deleteEntityAndFlags'
GO

ALTER PROCEDURE [dbo].[deleteEntityAndFlags] 
	(
		@EntityTypeID int ,   
		@EntityID varchar(10)  = null,  
		@deletedbyperson int  = null,
		@calledFromDeleteTrigger bit = 0
	)

	/* 
		WAB 2015-10-22  
		A procedure to tie together deleteEntityCheck and DeleteEntityFlags into a single procedure 
		which checks for protected flags, deletes flags and then deletes from table
		
		Requires @entityTypeID but multiple entityIDs can be passed in via a temporaryTable
		#deleteEntityIdTable (entityid, deletedByPerson)
	*/

AS
	SET NOCOUNT ON

	declare 
		@table sysname, 
		@uniqueKey sysname, 
		@sqlCmd nvarchar(max), 
		@baseSqlCmd nvarchar(max),
		@StartedTran bit		-- Indicates whether a transaction was started inside this proc

	Select
		@Table	= s.entityname, @uniqueKey = uniquekey
	from	
  		schemaTable	s 
	 where	s.entityTypeid	= @entityTypeID


	-- If this proc wasn't called from inside a transaction, start a new one
	IF @@TranCount = 0 
	 BEGIN
	   set @StartedTran = 1
	   BEGIN TRANSACTION
	 END


	IF @entityID is not null
	  begin
		select @entityid as entityid, @deletedbyperson as deletedbyPerson into #deleteEntityIdTable
	  END
	
	
	/* create a table to take the return from deleteEntityCheck and then run */
	create table #NotDeletable(EntityID int,Reason varchar(50),extendedInfo varchar(max))

	exec deleteEntityCheck @entityTypeID = @entityTypeID, @entityid = null

	/* if any items in the #NotDeletable table then we have to abort */
	IF exists (select 1 from #NotDeletable) 
	BEGIN	
		declare @msg nvarchar(max)
		select @msg = 'Can''t Delete EntityID ' + convert(varchar,entityID) + '. ' + reason + '. ' + isNull (extendedInfo, '') from #NotDeletable
		RAISERROR (@msg ,16,16)
		If @@TranCount > 0 and @StartedTran = 1
   			ROLLBACK TRANSACTION
		RETURN 1
	END	
	ELSE
	BEGIN
		exec DeleteEntityFlags @entityTypeID = @entityTypeID
	END

	/*  Now delete the records themselves, after a quick update to get the deleted by person set 
		This isn't actually necessary when this sp has been called from a delete trigger on the entity table so I have added a parameter to test for that.
		However, when this sp is called directly we do end up with a bit of recursion at this point because the delete fires the trigger which runs this sp again, so the checkEntity and deleteEntityFlags get run twice.  
		I haven't worked out a way of detecting this condition yet
	*/
	IF @calledFromDeleteTrigger = 0
	BEGIN
		set @BaseSQLCmd = ' from ' + @table + ' e inner join #deleteEntityIdTable d on e.' + @uniqueKey + ' = d.entityid'
		set @sqlCmd = 'update ' + @table + ' set lastUpdatedByPerson = d.deletedByPerson, lastUpdated = getdate() ' + @BaseSQLCmd
		exec (@sqlCmd)

		set @sqlCmd = 'delete ' + @table  + @BaseSQLCmd
		exec (@sqlCmd)
	END

	IF @StartedTran = 1
	 BEGIN
	   COMMIT TRANSACTION
	 END


	return 0	