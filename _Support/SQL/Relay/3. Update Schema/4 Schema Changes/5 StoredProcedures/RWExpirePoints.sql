IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'RWExpirePoints' and Type = 'P')
	DROP PROCEDURE RWExpirePoints
	
GO
	
CREATE    procedure
	[dbo].[RWExpirePoints]
	@PersonID	int = NULL,	/*ID of user*/
	@ExpiryDate datetime = NULL /*NJH 2009/11/26 P-LEX041 expiry date of points to be expired*/
as

/*===================================================================================
 Modifications                                                                     
 18-Jul-05	GCC		changed to ensure it only expires points for the personid
 2009/11/26 NJH		P-LEX041 - changed procedure so that an expiry date can be passed in, so that we can expire all points now    
 2011-02-14	NYB		LHID5496 replaced RWTransaction.RWTransactionID with RWTransactionSplit.PtsAccID in RWTransaction EX entry insert 
					as it was inserted the balance multiple times on RWPointsAccrued that had corresponding Debit entries 
					in RWTransaction that hadn't brought the balance back to 0
===================================================================================*/


set nocount on
/*===================================================================================*/
/* Initialisations                                                                   */
/*===================================================================================*/
Declare	@ErrCode int	-- Error return code
Declare	@CreatedDate datetime -- NJH 2009/11/26

-- NJH 2009/11/26 P-LEX041 - pass in expire date as a parameter, so that we can expire in the future. Default to getDate() below
--@ExpiredDate datetime	-- Date/time points were expired

SET @ErrCode = 0
SET @CreatedDate = getdate()

--SET @ExpiredDate = getdate()

-- Set ExpiryDate if NULL
IF @ExpiryDate is NULL SET @ExpiryDate = getDate()

-- Set Person ID if NULL
IF @PersonID is NULL SET @PersonID = user_id()

/*===================================================================================*/
/* Create database entries                                                           */
/*===================================================================================*/
BEGIN TRANSACTION
/*-----------------------------------------------------------------------------------*/
/* RWTransaction                                                                     */
/*-----------------------------------------------------------------------------------*/
INSERT RWTransaction(RWTransactionTypeID, 
			AccountID, 
			OrderID, 
			CreditAmount, 
			DebitAmount, 
			PriceISOCode, 
			CurrencyPerPoint, 
			RWTransactionReasonID,
			PersonID,
			Created, 
			CreatedBy)
SELECT     	'EX',
		AccountID, 
		NULL, 
		0, 
		SUM(Balance), 
		NULL, 
		NULL,
 		NULL, 
		personid, 
		@CreatedDate, -- NJH 2009/11/26 - was @ExpiredDate
		4
FROM         (SELECT DISTINCT RWPointsAccrued.AccountID, RWPointsAccrued.Balance, RWTransaction.personid, 
			/*<--NYB 2011-02-14 LHID5496 replaced-->RWTransaction.RWTransactionID<--with:-->*/ RWTransactionSplit.PtsAccID 
                       FROM RWPointsAccrued 
						INNER JOIN RWTransactionSplit ON RWPointsAccrued.PtsAccID = RWTransactionSplit.PtsAccID 
						INNER JOIN RWTransaction ON RWTransactionSplit.RWTransactionID = RWTransaction.RWTransactionID 
                       WHERE      
		RWTransaction.personid = @personID 
		AND (RWPointsAccrued.Points > 0) 
		AND RWPointsAccrued.expireddate IS NULL
		AND RWPointsAccrued.ExpiryDate <= @ExpiryDate) baseQuery
GROUP BY AccountID, personid

/*-----------------------------------------------------------------------------------*/
/* RWTransactionSplit                                                                */
/*-----------------------------------------------------------------------------------*/
INSERT RWTransactionSplit(RWTransactionID, PtsAccID, CreditAmount, DebitAmount)
SELECT RWtr.RWTransactionID, 
	RWpa.PtsAccID,
	0,
	RWpa.Balance
FROM	RWTransaction RWtr
JOIN	RWPointsAccrued RWpa
ON	RWpa.AccountID = RWtr.AccountID
WHERE	RWpa.ExpiryDate <= @ExpiryDate
AND	RWpa.ExpiredDate is NULL
AND	RWtr.Created = @CreatedDate -- NJH 2009/11/26 - was @ExpiredDate
AND	RWtr.RWTransactionTypeID = 'EX'

/*-----------------------------------------------------------------------------------*/
/* RWPointsAccrued                                                                   */
/*-----------------------------------------------------------------------------------*/
UPDATE RWpa
SET	RWpa.Balance = RWpa.Balance + RWts.CreditAmount - RWts.DebitAmount,
	RWpa.ExpiredDate = @CreatedDate -- NJH 2009/11/26 - was @ExpiredDate
FROM	RWPointsAccrued RWpa
JOIN	RWTransactionSplit RWts
ON	RWts.PtsAccID = RWpa.PtsAccID
JOIN	RWTransaction RWtr
ON	RWts.RWTransactionID = RWtr.RWTransactionID
WHERE	RWtr.RWTransactionTypeID = 'EX'
AND	RWtr.Created = @CreatedDate -- NJH 2009/11/26 - was @ExpiredDate
AND 	RWpa.ExpiredDate is NULL

/*-----------------------------------------------------------------------------------*/
/* Finally update the running balance in RWTransaction                               */
/*-----------------------------------------------------------------------------------*/
UPDATE 	RWTr
SET	RWTr.SnapshotBalance = (SELECT isnull(SUM(isnull(RWpa.Balance,0)),0)
			   FROM	RWPointsAccrued RWpa
			   WHERE RWtr.AccountID = RWpa.AccountID)
FROM 	RWTransaction RWTr
WHERE  RWtr.Created = @CreatedDate -- NJH 2009/11/26 - was @ExpiredDate
AND    RWtr.RWTransactionTypeID = 'EX'
COMMIT TRANSACTION

/*===================================================================================*/
/* Common exit point                                                                 */
/*===================================================================================*/
EXITPOINT:
Return @ErrCode
/*===================================================================================*/
/* Handle any errors                                                                 */
/*===================================================================================*/
ERRORTRAP:
IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION
GOTO EXITPOINT
