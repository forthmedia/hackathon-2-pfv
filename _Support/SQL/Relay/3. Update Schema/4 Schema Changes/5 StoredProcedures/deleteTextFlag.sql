if exists (select 1 from sysobjects where name = 'deleteTextFlag')
	drop procedure deleteTextFlag

GO
create procedure deleteTextFlag
	@entityid int, 
	@flagid varchar(100), 
	@userid int

AS

/*
SetTextFlag

Deletes a Text flag 


*/

Set nocount on

declare @tempFlagID int
declare @tempFlagTypeID int

-- get flagid and flaggroupid
IF isnumeric (@flagid)  = 1
	select @tempFlagID = flagid, @tempFlagTypeID = flagTypeid from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid where flagid = convert(int,@flagid)
ELSE
	select @tempFlagID = flagid, @tempFlagTypeID = flagTypeid from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid where flagtextid = @flagid


	IF @tempFlagTypeID not in  (2,3) or @tempFlagTypeID is null
	  BEGIN
		PRINT @FlagID + ': Not a Text flag. '

	  END
	ELSE
	  BEGIN
		-- is it already set?	
		IF exists (select 1 from Textflagdata bfd where  entityid = @entityid and flagid = @tempFlagID)
			BEGIN
				update Textflagdata set lastupdatedby = @userid, lastupdated = getdate() from Textflagdata where Textflagdata.flagid = @tempflagid and entityid = @entityid 
				delete Textflagdata from Textflagdata where Textflagdata.flagid = @tempflagid and entityid = @entityid 

			END
	   END


