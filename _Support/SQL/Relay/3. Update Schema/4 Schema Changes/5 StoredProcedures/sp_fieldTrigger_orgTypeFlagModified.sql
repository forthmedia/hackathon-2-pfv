if exists (select 1 from sysobjects where name='sp_fieldTrigger_orgTypeFlagModified' and xType='P')
drop procedure sp_fieldTrigger_orgTypeFlagModified

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[sp_fieldTrigger_orgTypeFlagModified] 
                @flagid int, 
                @modaction varchar(10),
                @triggerInitiatedBy int, 
                @RecordUpdatesAgainstID int,
                @triggerInitiatedByPersonID int = null
AS
Set nocount on


/*
2011/05/11 WAB Major Modification to support new temporary Table #modifiedEntityIDs passed by the modRegisterTrigger
*/


IF @modaction = 'FA' 
BEGIN
				declare @organisationTypeID int
                select @organisationTypeID=organisationTypeID from organisationType where dbo.listfind(mapToFlagIDList, @flagid) = 1

                update organisation set organisationTypeId = @organisationTypeID
                from organisation inner join #modifiedEntityIDs e on organisationid = e.entityid
                where organisationTypeID <> @organisationTypeID
END


Set nocount off
