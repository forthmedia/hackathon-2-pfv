IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'dedupePerson' and Type = 'P')
    DROP PROCEDURE dedupePerson

GO

SET ANSI_NULLS ON
GO

SET ANSI_PADDING ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE     proc [dbo].[dedupePerson]
			@RemovePersonId	int,
			@RetainPersonId	int,
			@Run			varchar(5) = 'No',
			@createdByPersonID	int =0		-- Store the person who has created the dedupe.

as

/***************************************************************************************
** Name   : dedupePerson
** Created: 13/Jun/2001
** By     : JVH
**
** Purpose: 
** Perform custom activities required when deduping Person records
**
** 
** Arguments:
**      See below
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00	    13/Jun/2001  JVH  Original
** 1.01	    19/Jan/2004	 SWJ  Added action merge section
**			25/Apr/2007	 NJH  Set username and password to be nvarchar rather than varchar
**          06/Jun/2016  DAN  Case 449803 - unable to merge people - enable QUOTED_IDENTIFIER before creating the procedure
**
****************************************************************************************/

/*===================================================================================*/
/* Initialisations                                                                   */
/*===================================================================================*/
declare @UserName1 nvarchar(50),		-- UserName of Person being retained
	@UserName2 nvarchar(50),		-- UserName of Person being deduped
	@Password1 nvarchar(50),		-- Password of Person being retained
	@Password2 nvarchar(50),		-- Password of Person being deduped
	@ErrCode 	int,
	@StartedTran 	bit		-- Indicates whether a transaction was started inside this proc.

set @ErrCode = 0

/*===================================================================================*/
-- Ensure username & password details are not lost
/*===================================================================================*/
Select 	@Username1 = Username,	
	@Password1 = Password
from	Person
where	PersonID = @RetainPersonID

Select 	@Username2 = Username,	
	@Password2 = Password
from	Person
where	PersonID = @RemovePersonID

If  (isnull(len(@UserName1),0) = 0 or isnull(len(@Password1),0) = 0)
and (isnull(len(@UserName2),0) > 0 and isnull(len(@Password2),0) > 0)
 Begin

    If @Run <> 'Yes'
     begin
	Print 'The following UserName and Password will be transferred from PersonID ' + convert(varchar, @RemovePersonID) + 
		' to PersonID ' + convert(varchar, @RetainPersonID) + ':'
	select @UserName2 as 'UserName', @Password2 as 'Password'
     end
    Else
     begin
	If @@TranCount = 0 
	 begin
	   set @StartedTran = 1
	   Begin Transaction
	 end

   	Update Person 
   	Set    UserName = @UserName2,
	       Password = @Password2
	Where  PersonID = @RetainPersonID

     end
 end
/*===================================================================================*/
-- Perform merge on action
/*===================================================================================*/

update actions
   set ownerPersonID = @RetainPersonId
 where ownerPersonID = @RemovePersonId

update actions
   set entityID = @RetainPersonId
 where entityID = @RemovePersonId
and entityTypeID = 0

/*===================================================================================*/
-- Perform merge on notes
/*===================================================================================*/

update entityNote
   set entityID = @RetainPersonId
 where entityID = @RemovePersonId
and entityTypeID = 0

/*===================================================================================*/
-- Perform generic dedupe
/*===================================================================================*/
exec @ErrCode = dedupeEntity
		@RemoveEntityId	= @RemovePersonId,
		@RetainEntityId	= @RetainPersonId,
		@Entity		= 'Person',
		@Run		= @Run,
		@createdByPersonID = @createdByPersonID

-- If there was a problem, rollback the whole dedupe
If @ErrCode <> 0 GOTO ErrorTrap

/*===================================================================================*/
-- Exit Point
/*===================================================================================*/
If @Run = 'Yes' and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
RETURN @ErrCode

/*===================================================================================*/
-- Error Trap
/*===================================================================================*/
ErrorTrap:
--print 'Error in DedupeEntity detected by DedupePerson'
If @Run = 'Yes' and @StartedTran = 1 and @@TranCount > 0
   ROLLBACK TRANSACTION

GOTO ExitPoint



GO