if exists (select 1 from sysobjects where name = 'deleteIntegerFlag')
	drop procedure deleteIntegerFlag

GO
create procedure deleteIntegerFlag

	@entityid int, 
	@flagid varchar(100), 
	@userid int

AS

/*
SetIntegerFlag

Deletes a Integer flag 


*/

Set nocount on

declare @tempFlagID int
declare @tempFlagTypeID int

-- get flagid and flaggroupid
IF isnumeric (@flagid)  = 1
	select @tempFlagID = flagid, @tempFlagTypeID = flagTypeid from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid where flagid = convert(int,@flagid)
ELSE
	select @tempFlagID = flagid, @tempFlagTypeID = flagTypeid from flag f inner join flaggroup fg on fg.flaggroupid = f.flaggroupid where flagtextid = @flagid


	IF @tempFlagTypeID not in  (2,3) or @tempFlagTypeID is null
	  BEGIN
		PRINT @FlagID + ': Not a Integer flag. '

	  END
	ELSE
	  BEGIN
		-- is it already set?	
		IF exists (select 1 from Integerflagdata bfd where  entityid = @entityid and flagid = @tempFlagID)
			BEGIN
				update Integerflagdata set lastupdatedby = @userid, lastupdated = getdate() from Integerflagdata where Integerflagdata.flagid = @tempflagid and entityid = @entityid 
				delete Integerflagdata from Integerflagdata where Integerflagdata.flagid = @tempflagid and entityid = @entityid 

			END
	   END


