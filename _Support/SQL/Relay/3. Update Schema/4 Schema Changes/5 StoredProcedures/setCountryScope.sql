createPlaceHolderObject 'procedure', 'setCountryScope'

GO

ALTER     PROCEDURE [dbo].[setCountryScope]
	  @entity sysname
	, @entityid int
	, @level int
	, @countryIDList varchar(max) = null   
	, @previousCountryIDList varchar(max) = '' -- optional, allows code to only remove items in this list rather than all items against this record
	, @countriesAdded varchar(max) = '' OUTPUT 
	, @countriesRemoved varchar(max) = '' OUTPUT 
	
AS

BEGIN
	declare 
		@comma char(1) = ''
	
	select 
		@entity = case 
							when isNumeric(@entity) = 0 then @entity 
							else (select entityName from schemaTable where entityTypeID = @entity)
						end	
	
	declare @result as table (action varchar(10), entityid int, countryid int, level int)

	IF @countryIDList is not null
	BEGIN
	
		MERGE countryScope As cs
		USING 	
			(select 
				@entity, @entityid, power(2,@level-1), c.countryid
			from
				dbo.csvToTable(@countryIDList) as users	
					inner join
				country c on c.countryid = users.value
			) as source (entity, entityID, permission, countryID)
		ON 	cs.entity = @entity and cs.entityID = @entityid	and source.countryID = cs.countryID	

		WHEN NOT MATCHED and source.countryID is not null
			THEN INSERT (entity, entityID, permission, countryID)
				 VALUES (@entity, @entityid, power(2,@level-1), source.countryID)

		WHEN NOT MATCHED BY SOURCE	AND cs.entity = @entity 
									and cs.entityID = @entityid	
									and cs.permission = power(2,@level-1) 
									and (@previousCountryIDList = '' or dbo.listFind(@previousCountryIDList,cs.countryID) <> 0)
			THEN DELETE
			
		WHEN NOT MATCHED BY SOURCE	AND cs.entity = @entity 
									and cs.entityID = @entityid	
									and cs.permission & power(2,@level-1) <> 0 
									and (@previousCountryIDList = '' or dbo.listFind(@previousCountryIDList,cs.countryID) <> 0)
			THEN UPDATE 
				SET permission = cs.permission - power(2,@level-1)

		WHEN MATCHED AND cs.permission & power(2,@level-1) = 0 
			THEN UPDATE 
				SET permission = cs.permission + source.permission
		
		OUTPUT case when $action = 'update' then case when deleted.permission & power(2,@level-1) = 0 then 'insert' else 'delete' end else $action end, isNull(inserted.entityID, deleted.entityID), isNull(inserted.countryID, deleted.countryID), @level  into @result
		;


		select 
			@countriesAdded = isNull (@countriesAdded,'') + @comma + convert(varchar,countryID)
			, @comma = ','
		from 
			@result
		where
			action = 'insert'	

		set @comma = ''			
		select 
			@countriesRemoved = isNull (@countriesRemoved,'') + @comma + convert(varchar,countryID)
			, @comma = ','
		from 
			@result
		where
			action = 'delete'	

	END



END	
	