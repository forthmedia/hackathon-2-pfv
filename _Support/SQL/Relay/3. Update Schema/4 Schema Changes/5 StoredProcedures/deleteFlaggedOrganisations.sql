 

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
 

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'deleteFlaggedOrganisations' and Type = 'P')
	DROP PROCEDURE deleteFlaggedOrganisations
	
GO
	
CREATE       Procedure [dbo].[deleteFlaggedOrganisations]
@TimeLimit int=1 ,   -- number of hours to wait from flagging until deletion can happen
@Test bit = 0 ,  -- when set to 1, no deletions take place
@overRideDatestring char(30) = null,
@overRide int=0
/* @overRideDatestringthis is passed as a string because of problems getting dates in with cfprocparam and also allows the milliseconds to be retained 
*/
AS
 
 
 
/*** Deletion of organisations ***/
/** Created by swj 10-Feb-03  - modified by NJH 2007/03/01
USAGE exec DeleteOrganisationHourLimit 1
 
2007/11/14  WAB   added protection of location flags and some better feedback, still work to be done to bring the three deletion sps into line
2009/04/15  WAB  wasn't putting recently deleted items into the #NotDeletableOrg  table (so wan't deleting them, but not giving feedback)
2010/06/30 LID 3627 WAB Organisations not due to be deleted because "too many in a request" were being put into the wrong table.  They were (correctly) not being deleted, but this was not being reported  
2011/01/20ish   WAB modified the returned queries to bring back the name of the person who did the delete
2016/01/14		NJH	BF-138, add the deletedByperson column to the deletedEntity temp table. it was getting created in deleteEntityFlags if it didn't exist anyways.
**/
 
Declare @overrideCountCheck datetime
Select @overrideCountCheck=CONVERT(datetime,@overrideDateString) 
 

Set nocount on
If @TimeLimit=999 Set @TimeLimit=0
 
Declare @StartDel datetime,
@StartDelWithLimit datetime,
@LocToBeDeleted int, @OrgToBeDeleted int,
@DeleteFlagID int,
@maxrows int,
@StartedTran bit,  -- Indicates whether a transaction was started inside this proc
@ErrCode int   -- Error code
 

select @maxrows=25
 
Declare @DelGo Varchar(5), @UserSet Varchar(30)
Select @Delgo='delok', @UserSet='dbo'
 
If @Delgo <> 'delok'
BEGIN
Print 'You must supply the argument "delok" to get the delete to work'
--return(0)
END
 
--Exec("setuser '"+@UserSet+"'")
if @overRide <> 1
begin
 Select @StartDel=getdate()
 Select @timelimit=abs(@timelimit)*-1
 Select @StartDelWithLimit=DATEADD(hour, @TimeLimit, @StartDel)
end
else
 Select @StartDelWithLimit=GetDate()
 

select @DeleteFlagID = flagid from flag where flagtextid = 'DeleteOrganisation'
 

Create table #NotDeletable(entityID int,Reason varchar(100),lastupdated  datetime,extendedInfo varchar(max),lastupdatedby INT) -- this table is passed to the deleteentitycheck function and passed back with the result
Create table #NotDeletableOrg(entityID int,Reason varchar(100),lastupdated  datetime,extendedInfo varchar(max), lastupdatedby int)
Create table #NotDeletableLoc(entityID int,Reason varchar(100),lastupdated  datetime,extendedInfo varchar(max), lastupdatedby INT)
 
Create table #countCheck (delcount int, lastupdated datetime, lastupdatedby int)
 
-- get the organisations that have been done in a large batch whose count exceeds the maximum allowed
if @overrideCountCheck is null 
  begin 
 
 insert into #countcheck (delcount,lastupdated,lastupdatedby)
 Select count(*),LastUpdated,LastUpdatedBy 
   from BooleanFlagData
 Where FlagID=@DeleteFlagID  
 group by LastUpdated,LastUpdatedBy having count(*) > @maxrows
  end
else
  begin
 
 insert into #countcheck (delcount,lastupdated,lastupdatedby)
 Select count(*),LastUpdated,LastUpdatedby 
   from BooleanFlagData
 Where FlagID=@DeleteFlagID 
 AND lastupdated <> @overrideCountCheck
 group by LastUpdated,LastUpdatedBy having count(*) > @maxrows
 
  end
 

-- record organisations excluded due to too many organisations being deleted at a time
-- fix here 2010/06/30
insert into #NotDeletableOrg (entityid, reason,lastupdated,lastupdatedby)
select entityid, 'Too Many Organisations in Request', b.lastupdated, b.lastupdatedby  
from booleanflagdata b, #countcheck c
where b.FlagID=@DeleteFlagID and c.LastUpdatedBy = b.LastUpdatedBy and c.LastUpdated = b.LastUpdated
 

-- get organisations that can't be deleted due to having people in them still
insert into #NotDeletableOrg (entityid, reason,lastupdated, lastupdatedby)
select distinct bfd.entityID, 'Organisation has people in locations', bfd.lastUpdated, bfd.lastupdatedby 
from booleanFlagData bfd inner join person p
on p.organisationID = bfd.entityID
where flagID=@DeleteFlagID
 
-- get organisations that can't be deleted because deleted too recently
insert into #NotDeletableOrg (entityid, reason,lastupdated, lastupdatedby)
select distinct bfd.entityID, 'Marked for deletion within last ' + convert(varchar,abs(@Timelimit)) + ' hours', bfd.lastUpdated , bfd.lastupdatedby
From BooleanFlagData as Bfd Left Join person p On Bfd.EntityID=p.organisationID
left join flag f ON f.flagID = bfd.flagID
Where f.FlagID=@DeleteFlagID 
and Bfd.LastUpdated>=@StartDelWithLimit 
 

--IDs of organisations to be deleted:(FlagID=90)
--****This WILL exclude orgs with people:****
Select B.EntityID as entityid, B.LastUpdated as DeletedDate, B.LastUpdatedBy as DeletedBy, b.lastUpdatedByPerson as deletedByPerson
into #deleteOrgIDTable
From BooleanFlagData as B Left Join person p On B.EntityID=p.organisationID
left join flag f ON f.flagID = b.flagID
Where f.FlagID=@DeleteFlagID and p.organisationID is null
and B.LastUpdated<@StartDelWithLimit 
and not exists(select * from #countcheck c where c.LastUpdatedBy = b.LastUpdatedBy and c.LastUpdated = b.LastUpdated)
 
Select @orgToBeDeleted = @@rowcount
 
print 'orgToBeDeleted = ' + convert (varchar,@orgToBeDeleted )
 
IF @orgToBeDeleted=0 GOTO TheEnd
 

-- this procedure checks for protected flags, requires #deleteEntityIDTable and #notDeletable to exist
select entityid , deletedby, deletedByPerson into #deleteEntityIDTable from #deleteOrgIDTable
exec deleteentitycheck @entityTypeID = 2
insert into #NotDeletableOrg  select * from #NotDeletable 
 

-- Exclude Location records that happen to have any Protected flags:
-- because my deleteEntityCheck procedure expects a particular table name, I have to do some jiggery pokery with the organisation tables
 
delete  from  #NotDeletable
-- put locations which need to be deleted into a temp table
Select locationid as entityid, DeletedDate, DeletedBy,deletedByPerson into #deleteLocationIDTable 
from #deleteOrgIDTable o inner join location l on l.organisationid = o.entityid
 
-- clear out the entityID table and pop the locationids into it
delete  from  #deleteEntityIDTable 
insert into #deleteEntityIDTable select entityid, deletedby,deletedByPerson from #deleteLocationIDTable

exec deleteentitycheck @entityTypeID = 1
 

-- add details of protected locations to the notdeletable list
insert into #NotDeletableOrg (entityID,reason,extendedinfo)
select o.organisationid, 'Location ' + convert(varchar,nd.entityid) + ' ' + reason , extendedinfo
from organisation o inner join location l on l.organisationid = o.organisationid  
inner join #NotDeletable nd on nd.entityid = l.locationid
 
-- remove protected locations from list to delete
Delete From #deleteLocationIDTable
Where entityID IN (Select entityID From #NotDeletable)
 

-- Exclude any Protected organisations from the deletion:
Delete From #deleteOrgIDTable
Where entityID IN (Select entityID From #NotDeletableOrg )
 
 
 
/*** Now Start Deleting the organisations... ***/
 
Select @orgToBeDeleted=count(*) from #deleteOrgIDTable
IF @orgToBeDeleted=0 GOTO TheEnd
 
--For safety, if the number to be deleted is very large then the delete
-- will not proceed and the DBA will have to intervene:
--IF @orgToBeDeleted>@maxrows GOTO TheEnd
 
/* NJH 2008/06/02 this is currently incorrect behaviour.
The behaviour has been changed so that should a batch of orgs flagged for deleted have a count greater than maxRows then prevent only
those organisations from being deleted.
 
IF @overRide <> 1 and @orgToBeDeleted > @maxRows
Begin
 insert into #NotDeletableOrg (entityid, reason,lastupdated)
 select entityID, 'Too Many Organisations in Request', deletedDate  from #deleteOrgIDTable
 
 delete from #deleteOrgIDTable
 Print convert(varchar,@orgToBeDeleted)+' orgs are flagged for deletion. This is greater than the maximum allowed of '+convert(varchar,@maxRows)+'. Exiting....'
 Goto TheEnd
End
*/
 
IF @Test = 1 GOTO TheEnd
 
Begin Tran
 

-- start deleting any empty locations
-- need to put entityids in deleteEntityIDTable so that can be used by deleteEntityFlags
-- clear out the entityID table and pop the locationids into it
delete  from  #deleteEntityIDTable 
insert into #deleteEntityIDTable select entityid , deletedby, deletedByPerson from #deleteLocationIDTable
 
-- now delete flags
exec deleteentityflags @entityTypeID = 1
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
 
 
--Location LDS:
Delete
From LocationDataSource
Where LocationID IN (Select entityid from #deleteLocationIDTable)
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
 
 
--Location Records:
Delete
From Location
Where LocationID IN (Select entityID from #deleteLocationIDTable)
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
--This will ensure that the user who originally set the delete flag
---will be recorded in the modRegister as the user who did the deletion:
Update M
Set M.ActionByCF=L.DeletedBy
From ModRegister as M Join #deleteLocationIDTable as L
On M.RecordID=L.EntityID
Where (Action='LD' and ModDate>=@StartDel)
 
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
 
 
 
 
-- now do the organisation stuff, need to rename #deleteOrgIDTable so that can be used by deleteEntityFlags

--org Flags:
delete  from  #deleteEntityIDTable 
insert into #deleteEntityIDTable select entityid, deletedby, deletedByPerson from #deleteOrgIDTable
 
exec deleteentityflags @entityTypeID = 2
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
--organisation ODS:
Delete
From organisationDataSource
Where organisationID IN (Select entityID from #deleteOrgIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
 
--organisation Records:
 
Delete
From organisation
Where organisationID IN (Select entityID from #deleteOrgIDTable)
IF @@error<>0
Begin
Rollback Tran
GOTO TheEnd
End
 
 
 
--This will ensure that the user who originally set the delete flag
---will be recorded in the modRegister as the user who did the deletion:
Update M
Set M.ActionByCF=O.DeletedBy
From ModRegister as M Join #deleteOrgIDTable as O
On M.RecordID=O.EntityID
Where (Action='OD' and ModDate>=@StartDel)
 
Set @ErrCode = @@Error  
If @ErrCode <> 0 GOTO ErrorTrap
 
 
 

Commit Tran
 
TheEND:
 

-- return query of all organisations and locations deleted (or which will be deleted in case of a test)
if @test = 0
 Begin
  select 'Locations Deleted' as LocationDeleteAction, l.locationid, sitename  from locationDel l inner join #deleteOrgIDTable  otd on l.organisationid = otd.entityid
  select 'Organisations Deleted' as OrganisationDeleteAction, o.organisationid, organisationName  from organisationdel o inner join #deleteOrgIDTable otd on o.organisationid = otd.entityid
 End
else
 Begin
  select 'Locations will be deleted' as LocationDeleteAction, l.locationid, sitename  from location l inner join #deleteOrgIDTable  otd on l.organisationid = otd.entityid
  select 'Organisations will be deleted' as OrganisationDeleteAction, o.organisationid, organisationName  from organisation o inner join #deleteOrgIDTable otd on o.organisationid = otd.entityid
 End
 
 
 
-- return query of all organisations not deleted for some reason
if @test = 0
  Begin
 select reason, o.organisationid, organisationname, nd.lastupdated, nd.lastupdatedby,ug.name as deletedbyname
 from organisation o inner join #notdeletableorg nd on o.organisationid = nd.entityid
	left join usergroup ug on ug.usergroupid = nd.lastupdatedby
 
 union 
 
 -- these are any items which were in ToDeleteOrg but for some reason are still in the organisation table
 select 'Unknown reason', o.organisationid, organisationname, null, td.deletedby,ug.name as deletedbyname
 from organisation o inner join #deleteOrgIDTable  td on o.organisationid = td.entityid
	left join usergroup ug on ug.usergroupid = td.deletedby

	order by organisationname,organisationid
  end
else
  begin
 select reason, o.organisationid, organisationname, nd.lastupdated, nd.lastupdatedby,ug.name as deletedbyname
 from organisation o inner join #notdeletableorg nd on o.organisationid = nd.entityid
  left join usergroup ug on ug.usergroupid = nd.lastupdatedby
  end
 
 
 
 
 
 
 
/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION
 
ExitPoint:
RETURN @ErrCode
 
/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:
 
If @@TranCount > 0 and @StartedTran = 1
   ROLLBACK TRANSACTION
 
GOTO ExitPoint
 
 
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
