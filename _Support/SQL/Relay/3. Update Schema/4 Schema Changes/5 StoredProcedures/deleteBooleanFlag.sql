
ALTER PROCEDURE [dbo].[deleteBooleanFlag]
	  @entityid int
	, @flagid varchar(100)
	, @userid int
	, @updatedByPersonID int  = null
	, @tableName  sysname = '#setFlag'

AS

/*
SetBooleanFlag

Deletes a boolean flag (either radio or checkbox)


WAB 2015/10/13   Made to work like setBooleanFlag (which was modified in 2011!)
	Modified so that it can work in 3 different ways
	1. Pass in @flagID and @entityId to set a single flag (original version) 
	2. Pass in @flagID  and a temporary table, by default called #setFlag, with a column entityID
	3. Pass in 	temporary table, by default called #setFlag, with a columns entityID and flagID

	Changed to run with dynamic SQL because ran into problems with flagid sometimes existing on temp table
	At this point decided that might as well make add @tableName parameter

*/

Set nocount on

declare	  @tempFlagID int
		, @tempFlagTypeID int
		, @ErrorMessage nVarchar (200)
		, @sqlCmd nVarChar (max)
		, @baseSqlCmd nVarChar (max)
		, @hasNonBooleanFlag  bit
		, @tableID int


IF @flagID is not null
BEGIN	
	-- get numeric flagid and flagTypeid and check that it is boolean
	select 
		@tempFlagID = flagid, 
		@tempFlagTypeID = flagTypeid 
	from 
		flag f 
			inner join 
		flaggroup fg on fg.flaggroupid = f.flaggroupid 
	where 
		(convert(varchar,flagid) = @flagid OR flagTextID = @flagid )
	
	IF @tempFlagTypeID not in  (2,3) or @tempFlagTypeID is null
	  BEGIN
		set @ErrorMessage = @FlagID + ': Not a boolean flag. '
	  END
		   
END

IF @EntityID is Null 
BEGIN
	select @tableID = object_id('tempdb..' + @tablename)
END

IF @EntityID is Null and (@tableID is null OR not exists (select 1 from tempdb..syscolumns where id = @tableID and name = 'entityid'))
BEGIN
	set @ErrorMessage =  '@EntityID is null and no ' + @tablename + ' table passed'
END
ELSE IF @EntityID is Null and @tableID is not null 
BEGIN
	IF @flagID is null and exists (select * from tempdb..syscolumns where id = @tableID and name = 'flagid')
	BEGIN
		set @sqlCmd = 'select @hasNonBooleanFlag = 1 
						from ' + @tablename + '  ef
						inner join vflagdef f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
						and flagTypeID not in (2,3)'
		exec sp_executeSQL @sqlCmd,N'@hasNonBooleanFlag bit OUTPUT', @hasNonBooleanFlag = @hasNonBooleanFlag OUTPUT
		IF @hasNonBooleanFlag = 1
		  BEGIN
			set @ErrorMessage =  @tablename + ' Table contains a non-boolean flag'
		  END
	END	  
	ELSE IF @flagID is null
	BEGIN
			set @ErrorMessage =  '@fladID null and no flagID column on ' + @tablename + 'table'
	END
END
ELSE IF @entityID is not null and @flagID is null
BEGIN
	set @ErrorMessage =  '@fladID null'
END


IF @ErrorMessage is not null 
BEGIN

	RAISERROR (@ErrorMessage, 16, 16)
	Return
END




IF @entityID is null  -- using temporary table
BEGIN
	IF @flagID is not null -- temporary table does not have flagid column
	BEGIN
		Set nocount off		
		SET @baseSQLCMD = '
			from 
				booleanflagdata bfd
					inner join
				' + @tablename + ' ef on bfd.entityid = ef.entityid
			where 
				bfd.flagid = @tempflagid 
		'

		set @sqlCMD = '
			update 
				booleanflagdata 
			set 
				lastupdatedby = @userid
				, lastupdatedbyPerson = @updatedByPersonID
				, lastupdated = getdate() 
		'
			+ @baseSQLCmd
			
		exec sp_executeSQL @sqlCMD, N'@tempflagid int, @userid int, @updatedByPersonID int', @tempflagid = @tempflagid, @userid = @userid , @updatedByPersonID = @updatedByPersonID 
		
		IF @@rowcount <> 0
		BEGIN
			Set nocount on
			set @sqlCMD = '
				DELETE 
					booleanflagdata 
			' + @baseSQLCMD	
			exec sp_executeSQL @sqlCMD, N'@tempflagid int', @tempflagid = @tempflagid

		
		END	

	END
	ELSE	-- temporary table has have flagid column
	BEGIN
		Set nocount off
		SET @baseSQLCMD = '
			from 
				' + @tablename + '  ef 
					inner join
				flag f on (f.flagtextID = convert(varchar(200),ef.flagid) OR convert(varchar(200),f.flagID) = convert(varchar(200),ef.flagid))	
					inner join
				booleanflagdata bfd on bfd.entityid = ef.entityid and bfd.flagid = f.flagid
		'

		set @sqlCMD = '
			update 
				booleanflagdata 
			set 
				lastupdatedby = @userid
				, lastupdatedbyPerson = @updatedByPersonID
				, lastupdated = getdate() 
			' + @baseSQLCMD	

		exec sp_executeSQL @sqlCMD, N'@userid int, @updatedByPersonID int', @userid = @userid , @updatedByPersonID = @updatedByPersonID 

		IF @@rowcount <> 0
		BEGIN
			Set nocount on
			set @sqlCMD = '
				DELETE 
					booleanflagdata 
			' + @baseSQLCMD
		
			exec sp_executeSQL @sqlCMD

		END	
	END
END
ELSE  -- both @flagid and @entityid set
BEGIN
	-- Set the lastupdated by before deletion and then do deletion if any rows exist
	Set nocount off
	update 
		booleanflagdata 
	set 
		lastupdatedby = @userid
		, lastupdatedbyPerson = @updatedByPersonID
		, lastupdated = getdate() 
	from 
		booleanflagdata 
	where 
		booleanflagdata.flagid = @tempflagid 
		and entityid = @entityid 
	
	IF @@rowcount <> 0
	BEGIN
		Set nocount on
		delete booleanflagdata from booleanflagdata where booleanflagdata.flagid = @tempflagid and entityid = @entityid 
	END	
END
	
