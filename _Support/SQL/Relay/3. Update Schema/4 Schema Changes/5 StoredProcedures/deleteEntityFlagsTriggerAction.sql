createPlaceHolderObject 'procedure','deleteEntityFlagsTriggerAction'
GO

ALTER PROCEDURE [dbo].[deleteEntityFlagsTriggerAction] 
	(@procID int)

AS

	/*
	WAB 2015-10-23
	A procedure which is called by an mrAudit trigger when any entity is deleted 
	It deletes all associated flags after checking for flag protection.

	Heavy lifting is done by calling deleteEntityAndFlags 
	Which itself calls deleteEntityCheck and deleteEntityFlags and deletes the entity (although deleting the entity isn't strictly necessary since already deleted by the original trigger)

	*/

	SET NOCOUNT ON

	declare @table sysname, @entityTypeID int, @uniqueKey sysname, @sqlCmd nvarchar(max), @status int = 0

	Select	@Table	= o2.name, @entityTypeID = entityTypeID, @uniqueKey = uniquekey
			
	from	sysobjects	o1
	  			join 
	  		sysobjects 	o2 	  on	o1.parent_obj = o2.id
	  			join
	  		schemaTable	s on o2.name = s.entityname
	  		
	 where	o1.id	= @procid

	
	/* 	Check that lastUpdateByPerson has been set
		Won't throw a full scale error for backwards compatibility reasons 	
		WAB 2017-02-27 PROD2016-3487.  If lastUpdated is a very long way in past then seconds will overflow, so add a test with days which will 'shortcircuit'
	*/	
	IF (datediff(d,(select min (lastupdated) from #d),getdate()) ) > 1  OR  (datediff(s,(select min (lastupdated) from #d),getdate()) )> 2 
	BEGIN
		RAISERROR ('Must Set LastUpdatedBy and LastUpdated',1,1)
	END
	
								
	/* put entityIDs into a #deleteentityidtable table, needs to use dynamic dql to get the unique key, 
	but deleted table is not available to exec, so have to copy to temp table */
	
	create table #deleteEntityIdTable (EntityID int, deletedByPerson int)
	
	set @sqlCMD = '
					insert 	into 
						#deleteentityidtable
					select ' + 
						@uniqueKey + ' as entityID 
						,lastupdatedByPerson as deletedByPerson
					from 
						#d
					'									
	exec (@sqlCMD)				
	
	exec @status = deleteEntityAndFlags @entityTypeID = @entityTypeID, @entityid = null, @deletedByPerson = null, @calledFromDeleteTrigger = 1

	return @status

	