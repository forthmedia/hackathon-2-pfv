createPlaceHolderObject @objectName = 'flagConditionToUserGroup', @objectType = 'PROCEDURE'

GO

alter procedure flagConditionToUserGroup @personid int , @changed bit = 0 OUTPUT
AS

	declare 
		  @userGroupID int
		, @conditionSQL nvarchar(max)
		, @result bit = 0
	
	set nocount on	
		select 
			usergroupid ,
			conditionSQL as condition,
			null as conditionResult
		into #tempCondition
		from 
			usergroup
		where 	
			conditionSQL <> ''
		 	and conditionvalidated = 1
				
	exec testConditions_POL  @personid = @personid 

	/* insert new items and remove old items */
	insert into rightsGroup
		(usergroupid, personid, createdby)
	select 
		distinct ug.usergroupid, @personid, -1
	from 
		#tempCondition ug
	left join 
		rightsgroup rg on rg.personid = @personid and rg.usergroupid = ug.usergroupid
	where 
		ug.conditionResult = 1
		and rg.usergroupid is null

	set @changed = @@rowcount

	delete 
		rightsGroup
	from 
		rightsGroup rg
			left join 
		#tempCondition ug on ug.conditionResult = 1 and rg.usergroupid = ug.usergroupid
	where 
			rg.personid = @personid and rg.createdby = -1
		and ug.usergroupid is null

	set @changed = @@rowcount | @changed
	

	/* Any conditions which have caused an error will have conditionResult set to null, we should set validated to 0 */
	update usergroup
	set conditionvalidated = 0
	from usergroup ug
			inner join
		 #tempCondition c on ug.conditionSQL = c.condition
	where conditionResult is null	 
			
	
