IF Exists (select * from sysobjects where name = 'createFlagView' and xtype= 'P')
	DROP procedure createFlagView

GO
CREATE PROCEDURE dbo.createFlagView
/*  =============================================
	Author:  William Bibby
	Create date: 2014-02-26
	Description: Creates flag views


	WAB 2015-04-14 	Transaction problems when called from inside the SalesForce Connector.
					Don't begin/commit transaction if already inside one
	WAB 2015-10		Added support for creating a view with a subset of fields					
	WAB 2015-10-22	Added support for deletion of records via the view 
					Deletion of flags and checking of protection is handled by a trigger on the base table
	WAB 2015-11-25	Move away from using vEntityName for getting name of linked objects
	WAB 2016-02-23	CASE 448111. Ensure that view is created in dbo schema
	WAB 2016-05-12	BF-699 Fields of the form IS_xxxx (ie linked Entities) was not implemented for integerMultiple Flags
	WAB 2016-10-40	Add SET NOCOUNT ON Trigger
	WAB 2016-10-04	PROD2016-2428 After an insert into a view, implement a hack so that @@identityInsert is correctly populated.  Note that this hack does not deal with scope_identity()
	WAB 2016-10-17	PROD2016-1303 SupportFor isBitField attribute on flagGroups.  All checkboxes in group are exposed as individual flags and there is no flagGroup Field
					Datatype for individual radio/checkboxes in updateable views was wrong - should be bit
					Also, linkedEntityNameExpressionAppeared to be wrong
	WAB 2016-11-15	PROD2016-2791 Use new indexed view vRadioFlagGroupData to improve performance when retrieving radio group data
	WAB 2016-11		Add support for ForeignKey TextIDs such as lookuplist
	WAB 2016-11-30	PROD2016-2895 _Update view losing decimal places
	NJH 2017/01/04  PROD2016-3061 - decimal places not getting saved on update views
	WAb 2017-01-24	During PROD2016-3299.  Altered so that only recreates if view has actually changed
	WAb 2017-03-04	Modified so no longer creates backups so does create/alter as appropriate
 	============================================= */
 @entityName sysname,
 @viewName sysname,
 @includeBooleans bit = 0,
 @fieldList varchar(max) = null,
 @debug bit = 0


AS

declare @flagID varchar(10),
		@flagGroupID varchar(10),
		@flagTextID sysname,
		@flagGroupTextID sysname,
		@flagType sysname,
		@dataTableFullName sysname,
		@linksToEntityTypeID int,
		@linkedEntity sysname,
		@linkedEntityUniqueKey sysname,
		@linkedEntityNameExpression nvarchar(max),
		@selects nVarchar(max) = '',
		@updateableSelects nVarchar(max) = '',
		@joins nVarchar(max) ='',
		@flagtableAlias sysname,
		@entityNameTableAlias sysname,
		@flagJoin nVarchar(max),
		@flagSelect nvarchar(max),
		@columnName sysname,
		@casting nvarchar(max),
		@fieldAlias sysname,
		@UniqueKey sysname,
		@qualifiedUniqueKey sysname, 
		@sql nvarchar(max),
		@CR varchar(1) = char(10),
		@comma varchar(1),
		@errorid int,
		@entityTypeID  int,
		@schema sysname = 'dbo',
		@schemaAndName sysname,
		@join nvarchar(max),
		@select	nvarchar(max)
		

declare @aliasesUsed as table (alias sysname)

	select 
		@qualifiedUniqueKey = @entityName + '.' + uniqueKey,
		@UniqueKey = uniqueKey,
		@entityTypeID = entityTypeID
		 from schemaTable where entityName=@entityName

Set nocount on

/* NJH 2014/11/19 Add some lastUpdated columns if they don't already exist, as they should be standard, particular if a table has flags associated with it
declare @sqlcmd varchar(max)
select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@entityName+''' and column_name=''lastUpdated'')
	begin
		alter TABLE [dbo].['+@entityName+'] add [lastUpdated] datetime NULL
		ALTER TABLE [dbo].['+@entityName+'] ADD  CONSTRAINT [DF_'+@entityName+'_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
	end'
exec (@sqlcmd)

select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@entityName+''' and column_name=''lastUpdatedBy'')  
  alter TABLE [dbo].['+@entityName+'] add [lastUpdatedBy] int NULL'
exec (@sqlcmd)

select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@entityName+''' and column_name=''lastUpdatedByPerson'')  
  alter TABLE [dbo].['+@entityName+'] add [lastUpdatedByPerson] int NULL'
exec (@sqlcmd)

/* regenerate the del table should columns have been added */
exec Generate_MRAuditDel @tablename = @entityName
*/		

/* get All fields in the table */
DECLARE tableCursor CURSOR LOCAL FOR  
	select column_Name ,
			case 
				when t.data_Type like '%char' then
					t.data_Type + '(' + case when t.character_maximum_length = -1 then 'max' else convert(varchar,character_maximum_length) end + ')'  -- need to deal with varchar(max)
				when t.data_Type = 'decimal' then
					t.data_Type + '(' + cast(t.numeric_precision as varchar)+','+cast(t.numeric_scale as varchar) + ')' 
				else	
					t.data_Type
			end	as casting 
	
	from information_schema.columns t
			left join 
		dbo.csvToTable (@fieldList) fields on fields.value = column_name
		
	where 
		table_name = @entityName
		and (@fieldList is null or fields.value is not null or t.column_name in ('created','createdby','lastudpated','lastupdatedby','lastupdatedbyperson') or t.column_name = @uniqueKey)

OPEN tableCursor
FETCH NEXT FROM tableCursor INTO @columnName, @casting

	WHILE @@FETCH_STATUS = 0   
	BEGIN
			Select @selects = @selects + case when @selects = '' then '' else ',' end + @CR + @entityName + '.' + @columnName
			Select @updateableSelects = @updateableSelects + case when @updateableSelects = '' then '' else ',' end + @CR + case when @entityName + '.' + @columnName = @qualifiedUniqueKey  THEN @qualifiedUniqueKey  ELSE ' convert(' + @casting +',null) ' END + ' AS ' + @columnName
			
			insert into @aliasesUsed values (@columnName)
	FETCH NEXT FROM tableCursor INTO @columnName, @casting
	END

CLOSE tableCursor   
DEALLOCATE tableCursor

/* get non checkbox flags (or checkboxes exposed as bits or if @includeBooleans=1 */
DECLARE flagCursor CURSOR FOR  
	select flagID, flagTextID, dataTableFullName, dataType, casting, linksToEntityTypeID, linkedEntity, linkedEntityUniqueKey, linkedEntityNameExpression  
	from 
	(	select 
			convert(varchar,flagID) as flagID, case when isNull(flagTextID,'') = '' then 'flag_' + convert(varchar,flagID)  ELSE flagTextID END as flagTextID, dataTableFullName, 
		dataType,
		case when dataType in ('checkbox','radio') then 'bit'  when dataType = 'integer' then 'int' when dataType = 'decimal' then 'decimal (18,2)' when dataType = 'date' then 'datetime' else 'nvarchar(max)' end as casting, 
		linksToEntityTypeID,
		f.flagGroupTextID,
		s.entityName as linkedEntity,
		s.uniqueKey as linkedEntityUniqueKey,
		s.nameExpression as linkedEntityNameExpression
		from 
			vflagdef f
				inner join
			flagGroup fg on fg.flagGroupID = f.flagGroupID	
				left join
			schemaTable s on s.entityTypeID = linksToEntityTypeID 	
			
		where entityTable = @entityname 
				and (datatype not in ('checkbox','radio') OR @includeBooleans = 1 OR isNull(dbo.getParameterValue (fg.Formattingparameters, 'isBitField',' '),'') = 'true') 
				and datatype <> 'event' 
	) as x
		left join 
			dbo.csvToTable (@fieldList) fields on (fields.value = x.flagtextID or fields.value = x.flagGroupTextID)
	where (@fieldList is null or fields.value is not null)


OPEN flagCursor
FETCH NEXT FROM flagCursor INTO @flagID, @flagTextID, @dataTableFullName, @flagType, @casting, @linksToEntityTypeID, @linkedEntity, @linkedEntityUniqueKey ,@linkedEntityNameExpression 

	WHILE @@FETCH_STATUS = 0   
	BEGIN
		Select @fieldAlias = rtrim(@flagTextID)

		IF NOT exists (select 1 from @aliasesUsed where alias = @fieldAlias)
		BEGIN


			select @flagTableAlias = 'flag_' + @flagid
			select @flagJoin = ' LEFT JOIN ' + @dataTableFullName + ' AS ' + @flagTableAlias + ' ON ' + @qualifiedUniqueKey + '= ' + @flagTableAlias + '.entityid AND ' + @flagTableAlias + '.flagID = ' + @flagid				
			
			IF  @flagType IN ('radio','checkbox')
			BEGIN
						select @flagSelect = 'case when '+ @flagTableAlias +'.flagid is null then 0 else 1 end AS ['+ @fieldAlias+']'
			END
			ELSE IF @flagType IN ('IntegerMultiple')
			BEGIN
						select @flagJoin = ''
						select  @flagSelect = 'dbo.integerMultipleFlagList(' + @flagID + ',' + @qualifiedUniqueKey + ',default) AS ['+ @fieldAlias+']'
						/* for integer flags which are entityIDs, this column will give the entity names */
						select  @flagSelect = @flagSelect + ',dbo.integerMultipleFlagList(' + @flagID + ',' + @qualifiedUniqueKey + ',''name'') AS ['+ @fieldAlias+'_name]'
						insert into @aliasesUsed values (@fieldAlias+'_name')
						
			END
			ELSE IF @flagType IN ('TextMultiple')
			BEGIN
						select @flagJoin = ''
						select  @flagSelect = 'dbo.textMultipleFlagList(' + @flagID + ',' + @qualifiedUniqueKey + ') AS ['+ @fieldAlias+']'
						
			END
			ELSE
			BEGIN
						select  @flagSelect = @flagTableAlias +'.data AS ['+ @fieldAlias+']'
						
						IF @flagType = 'Integer' and @linksToEntityTypeID is not null
						BEGIN
							select @entityNameTableAlias = @flagTableAlias + '_name'
							select @flagJoin = @flagJoin + ' LEFT JOIN ' + @linkedEntity + ' as ' + @entityNameTableAlias + ' on ' + @flagTableAlias + '.data = ' + @entityNameTableAlias + '.' + @linkedEntityUniqueKey   				
							select  @flagSelect = @flagSelect + ',' + @entityNameTableAlias +'.' + @linkedEntityNameExpression + ' AS ['+ @fieldAlias+'_name]'
							insert into @aliasesUsed values (@fieldAlias+'_name')
						END 
			END
			
			Select @joins = @joins + ' ' + @flagJoin + @CR
			Select @selects = @selects + ',' + @CR + @flagSelect	
			Select @updateableSelects = @updateableSelects + ',' + @CR + ' convert( ' + @casting + ',null) as ['+ @fieldAlias+']' 
			insert into @aliasesUsed values (@fieldAlias)
		
		END
		ELSE
			Print 'Field ' + @fieldAlias + ' already used'	


		FETCH NEXT FROM flagCursor INTO @flagID, @flagTextID, @dataTableFullName, @flagType, @casting, @linksToEntityTypeID, @linkedEntity, @linkedEntityUniqueKey ,@linkedEntityNameExpression 
	END

	CLOSE flagCursor   
	DEALLOCATE flagCursor


	/* get checkbox flags (as groups) */
	DECLARE flagGroupCursor CURSOR LOCAL FOR  
	select flagGroupID, flagGroupTextID, dataType 
	 from (
		select  
			convert(varchar,flagGroupID) as flagGroupID, case when isNull(flagGroupTextID,'') = '' THEN 'flagGroup_' + convert(varchar,flagGroupID) ELSE  flagGroupTextID END as flagGroupTextID, dataType 
		from
			vflagGroup fg
		where 
			entityTable = @entityname and datatype in  ('checkbox', 'radio')
			and isNull(dbo.getParameterValue (fg.Formattingparameters, 'isBitField',' '),'') <> 'true'
	) as x
			left join 
		dbo.csvToTable (@fieldList) fields on fields.value = x.flagGroupTextID
	where 
		(@fieldList is null or fields.value is not null)
		


	OPEN flagGroupCursor
	FETCH NEXT FROM flagGroupCursor INTO @flagGroupID, @flagGroupTextID, @flagType

	WHILE @@FETCH_STATUS = 0   
	BEGIN

		Select @fieldAlias = rtrim(@flagGroupTextID)
		IF NOT exists (select 1 from @aliasesUsed where alias = @fieldAlias)
		BEGIN

			IF @flagType = 'checkbox'
			BEGIN
				select 
						  @flagSelect = 'dbo.BooleanFlagList('+ @flagGroupID + ',' +  @qualifiedUniqueKey +',default) AS ['+ @fieldAlias+'_name], ' +
								'dbo.BooleanFlagList('+ @flagGroupID + ',' +  @qualifiedUniqueKey +',''textID'') AS ['+ @fieldAlias+'], ' +
								'dbo.BooleanFlagNumericList('+ @flagGroupID + ',' +  @qualifiedUniqueKey +') AS ['+ @fieldAlias+'_id]' 
						,@flagJoin = ''
				insert into @aliasesUsed values (@fieldAlias+'_name')
				insert into @aliasesUsed values (@fieldAlias+'_ID')
			
			END
			ELSE
			BEGIN

				select 
						  @flagTableAlias = 'flagGroupData_' + @flagGroupID 
						, @flagJoin = ' LEFT JOIN vRadioFlagGroupData ' + @flagTableAlias + ' WITH (NOEXPAND) ON ' + @flagTableAlias + '.flagGroupID = ' + @flagGroupid	+ ' and ' + @qualifiedUniqueKey + '= ' + @flagTableAlias + '.entityid '
						, @flagSelect = @flagTableAlias + '.name AS ['+ @fieldAlias+'_name], ' +
										@flagTableAlias + '.flagtextID AS ['+ @fieldAlias+'], ' +
										@flagTableAlias + '.flagID  AS ['+ @fieldAlias+'_id]' 
				insert into @aliasesUsed values (@fieldAlias+'_name')
				insert into @aliasesUsed values (@fieldAlias+'_ID')


			
			END

			Select @joins = @joins + ' ' + @flagJoin + @CR			 
			Select @selects = @selects + ',' + @CR + @flagSelect	
			Select @updateableSelects = @updateableSelects + ',' + @CR + ' convert(varchar(max),null) as ['+ @fieldAlias+']'

			insert into @aliasesUsed values  (@fieldAlias)

		END
		ELSE
			Print 'Field ' + @fieldAlias + ' already used'	
			
		FETCH NEXT FROM flagGroupCursor INTO @flagGroupID, @flagGroupTextID, @flagType
	END

	CLOSE flagGroupCursor   
	DEALLOCATE flagGroupCursor

	
/* deal with linked entity Flags*/
	DECLARE linkedFlagsCursor CURSOR LOCAL FOR  
	select flagid, flagTextID, dataTableFullName, dataType  from (
		select 
			convert(varchar,flagID) as flagID, case when isNull(flagTextID,'') = '' then 'flag_' + convert(varchar,flagID)  ELSE flagTextID END as flagTextID, dataTableFullName, dataType 
		from vflagdef f
		where 
			linksToEntityTypeID = (select entityTypeID from schemaTable where entityName = @entityname) 
			/* WAB 2016-05-12	BF-699 Added support for IntegerMultiples. Note that for some odd reason this dataTable column uses IntegerM not IntegerMultiple */
			and dataTable in ('Integer', 'IntegerM') 
	) as x
			left join 
		dbo.csvToTable (@fieldList) fields on fields.value = x.flagTextID
	where 
		(@fieldList is null or fields.value is not null)

	
	OPEN linkedFlagsCursor
	FETCH NEXT FROM linkedFlagsCursor INTO @flagID, @flagTextID, @dataTableFullName, @flagType
	
		WHILE @@FETCH_STATUS = 0   
		BEGIN

			Select @fieldAlias = 'IS_' + rtrim(@flagTextID)

	
			IF NOT exists (select 1 from @aliasesUsed where alias = @fieldAlias)
			BEGIN
	
	
				select @flagTableAlias = 'flag_' + @flagid
				select @flagJoin = ''				
				select @flagSelect = ' case when exists (select 1 from ' + @dataTableFullName + ' as ' + @flagTableAlias + ' where ' + @qualifiedUniqueKey + '= ' + @flagTableAlias + '.data AND ' + @flagTableAlias + '.flagID = ' + @flagid + ') then 1 else 0 end as ['+ @fieldAlias+']'
				Select @joins = @joins + ' ' + @flagJoin + @CR
				Select @selects = @selects + ',' + @CR + @flagSelect	
				Select @updateableSelects = @updateableSelects + ',' + @CR + ' convert(bit,null) as [' + @fieldAlias + ']' 
				insert into @aliasesUsed values (@fieldAlias)
			END


			FETCH NEXT FROM linkedFlagsCursor INTO @flagID, @flagTextID, @dataTableFullName, @flagType
		END

	CLOSE linkedFlagsCursor   
	DEALLOCATE linkedFlagsCursor


	/* Look for foreign keys to tables such as lookupList, we can bring in the textID and _name fields */
	DECLARE lookuplistCursor CURSOR LOCAL FOR  
	select
		  join_ =	'left join ' + foreignObject + ' as ' + foreignObjectAlias + ' ON ' + @entityName + '.' + localColumn + ' = ' + foreignObjectAlias + '.' + foreignColumn
		, select_ =	 case 
						when textIDColumn.name is not null then foreignObjectAlias + '.' + textIDColumn.name
						when dbo.getTableNameExpression (foreignObject) <> '' then foreignObjectAlias + '.' + dbo.getTableNameExpression (foreignObject) 
					   end 
		, alias =	case 
						when textIDColumn.name is not null then  metadata.name + '_textID' 
						when dbo.getTableNameExpression (foreignObject) <> '' then metadata.name + '_Name' 
					   end 
	from 
				(
					select				
						 case when right (name,2) = 'id' then left(name,len(name)-2) else name + '_' end as name  /* name of foreign object is column name with ID stripped off */
						, name as localColumn
						, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) as foreignObject
						, dbo.regExMatch ('^.*(?=\.)',foreignKey,1) + '_' + case when right (name,2) = 'id' then left(name,len(name)-2) else name + '_' end as foreignObjectAlias
						, dbo.regExMatch ('(?<=\.).*$',foreignKey,1) as foreignColumn
						
					FROM
						vFieldMetaData
					where
						tablename = @entityName 
							and foreignKey is not null														
							and source = 'core'
				) as metaData
				left join syscolumns textIDColumn on textIDColumn.id = object_id(foreignObject) and textIDColumn.name like '%textid'
				
	where 
			textIDColumn.name is not null or dbo.getTableNameExpression (foreignObject) <> ''

	
	OPEN lookuplistCursor
	FETCH NEXT FROM lookuplistCursor INTO @join, @select, @fieldAlias
	
		WHILE @@FETCH_STATUS = 0   
		BEGIN

			IF NOT exists (select 1 from @aliasesUsed where alias = @fieldAlias)
			BEGIN
	
				Select @joins = @joins + ' ' + @Join + @CR
				Select @selects = @selects + ',' + @CR + @Select + ' as [' + @fieldAlias + ']' 
				Select @updateableSelects = @updateableSelects + ',' + @CR + ' convert(bit,null) as [' + @fieldAlias + ']' 
				insert into @aliasesUsed values (@fieldAlias)
			END
			ELSE
			BEGIN
				Print 'Field ' + @fieldAlias + ' already used'	
			END


			FETCH NEXT FROM lookuplistCursor INTO @join, @select, @fieldAlias
		END

	CLOSE lookuplistCursor   
	DEALLOCATE lookuplistCursor

	declare 
		@createViewName sysname,
		@viewSQL nvarchar(max),
		@triggerSQL nvarchar(max),
		@currentViewSQL nvarchar(max),
		@currenttriggerSQL nvarchar(max),
		@createOrAlter nvarchar(max),
		@viewChanged bit,
		@triggerChanged bit
	
	declare @loop int = 1
	
	WHILE @loop <= 2
	BEGIN
		IF @loop = 1 /* This does the main view */
		BEGIN
			select @viewSQL = 
					' SELECT	' 
					+ @selects + @CR
					+ ' from ' + @entityName
					+ @joins
			select @createViewName = @viewName
		END
		ELSE IF @loop = 2 /* This does the updateable view */
		BEGIN
			select @viewSQL = 
					' SELECT	' 
					+ @updateableSelects + @CR
					+ ' from ' + @entityName
			select @createViewName = @viewName + '_update'
		END

		if @debug =1 
			print cast (@viewSQL as nText)
			
		declare @triggerName sysname = @createViewName + '_InsteadOf'

		/*	Get current SQL to check whether there have been changes, 
			and also can tell whether object exists
		*/
		SELECT 
				@currentViewSQL = object_definition (object_id(@createViewName))
			,	@currentTriggerSQL = object_definition (object_id(@triggerName))

		
		select @createOrAlter  = case when @currentViewSQL is null then 'CREATE' else 'ALTER' end
		Select @viewSQL =  @createOrAlter +  ' view  ' + @schema + '.' + @createViewName + ' AS ' + char(10) + ' /* AutoCreatedBy createFlagView */ ' + char(10) +  @viewSQL
		select @viewChanged = case when @currentViewSQL is null then 1 when replace (@currentViewSQL, 'create view', @createOrAlter + ' view') <> @viewSQL then 1 else 0 end

		/* now define an update trigger */
		select @createOrAlter  = case when @currentTriggerSQL is null then 'CREATE' else 'ALTER' end
		SELECT @triggerSQL = 
			@createOrAlter +
			' TRIGGER dbo.' + @triggerName + ' on ' + @schema + '.' + @createViewName + ' instead of update,insert,delete
			AS 

				set nocount ON

				IF exists (select 1 from inserted)
				BEGIN

					select * into #I from inserted
				
					create table #results (action sysname, entityID int, upsertUniqueID varchar(50))
					
					declare @columns_Updated varbinary(50) = columns_updated()
					
					exec entityUpsert 
						@upsertTableName = ''#I'', 
						@columns_Updated = @columns_Updated , 
						@entityName = '''+@entityName+''',
						@lastUpdatedByPerson = default,
						@validate = 0,
						@processNulls = -1,								
						@truncate = 0,	
						@whereClause = default,
						@returnTableName = ''#results'',
						@debug = default

						/* This code give @@identity a value */
						IF Exists (select 1 from #results where action = ''insert'')
						BEGIN
							create table #temp (id int identity)
							set identity_insert #temp on
							insert into #temp (id) select max (entityID) from #results where action = ''insert''
							drop table #temp 

						END


				END		
				ELSE IF exists (select 1 from deleted)
				BEGIN
						/* do a delete, there is now a trigger on the entity table which will delete all the flags and enforce protection */
						Set nocount on
						delete ' + @entityName +' from ' + @entityName + '
						inner join deleted on deleted.' + @UniqueKey + ' = ' + @entityName + '.' + @UniqueKey + '
				
				END
				'		

		select @triggerChanged = case when @currentTriggerSQL is null then 1 when replace (@currentTriggerSQL, 'create trigger', @createOrAlter + ' trigger') <> @triggerSQL then 1 else 0 end

			IF (@triggerChanged = 1 OR @viewchanged = 1)
			BEGIN

				/* WAB 2015-04-14 	Don't begin/commit transaction if already inside one */
				DECLARE @TransactionCountOnEntry int
				Select @TransactionCountOnEntry = @@TranCount
			
				IF @TransactionCountOnEntry = 0
				BEGIN
					begin tran
				END
			
					exec (@viewSQL)
					
					IF @triggerChanged = 1
					BEGIN
						exec sp_executeSQL @triggerSQL
					END	
		
				/* WAB 2015-04-14 	Don't begin/commit transaction if already inside one */
				IF @TransactionCountOnEntry = 0
				BEGIN
					commit	
				END	
				Print @createViewName + ' updated'
			END
			ELSE
			BEGIN
				Print 'No Changes to view ' + @createViewName
			END
	
		select @loop = @loop + 1	
	
	END
	
	


RETURN


ErrorTrap:
	print cast (@SQL as nText)


GO
