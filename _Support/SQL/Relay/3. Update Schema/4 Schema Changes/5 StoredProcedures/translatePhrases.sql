IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'translatePhrases' and Type = 'P')
	DROP PROCEDURE translatePhrases
	
GO
	
CREATE     Procedure [dbo].[translatePhrases]
		@tableName                  varchar(100) ,
		@RequiredCountry  	varchar(3)  = '0',  -- not needed for exact match , supplied in table
		@RequiredLanguage 	varchar(3)  = '1',  -- not needed for exact match, supplied in table
		@FallBackLanguage	varchar(3)   = '1',   -- not needed for exact match
		@ExactMatch		int  =0,
		@NullText		varchar(100)  = 'No Translation',
		@doAnalysis		varchar(3) = 'No',
		@onNullShowPhraseTextID	int = 1,
		@logRequests	int = 0,
		@appIdentifier	varchar(100) = '',
		@pageIdentifier	varchar(100) = '',
		@returnResults	int = 1     -- when 0 doesn't return a query, just updates the phrasePointerCache .


as
set nocount on


declare @cmd nvarchar(max)

/*
Version History
1.0
1.01	16/3/01  	WAB	Altered queries to use sp_executeSql
1.02	1/4/01		WAB    Corrected a bug - bring back required language with countryid = 0
1.03	5/4/01		WAB	Corrected bug - problem if translation exisited, but none were suitable
1.04	10/4/01 ish 	WAB	added parameter @onNullShowPhraseTextID
1.05	18/5/01		WAB	added parameters to allow for recording of all translation requests
1.06	25/6/01		WAB 	was bringing back incorrect results when country groups were being used.  Seemed OK on dev but not on 3Contact.  Have replaced query   where x in (select countrygroupid from countrygroup where countrymemberid = @RequiredCountry) with where x in (@countryGroupList).  @countryGroupList is a string which is created once at beginnning
		08/06/05	WAB   problem if @countryGroupList is blank - corrected



2.0 	June 2005	WAB	Altered to handle the concept of "default item for this country"
						Will still handle languageid 0 though

3.0 September 2010  WAB Altered to handle the phrasePointerCache
		2010/09/28		WAB Bug fix to above
		2011/02/17		WAB deal with looking for column names in temporary tables. see comment in code
		2011/05/09		WAB	Performance Improvements.
						Added a dirty column to the phrasePointerCache table
						Reorganised all the code to try and prevent locking between processes
						Copied incoming table into my own table - made all the queries easier - did not have to use so much dynamic SQL
						Main difference actually came from a new index!
					
LHID8224 2011/11/24	NYB	Control Content->Record Translations was not working:  
						Increased the varchar size of @tableName

*/



/*
When a best match is required:  (@exactMatch = 0)
Takes a table with columns phrasetextid, entityid, entitytypeid, entityType and get translations in the required language
entityTypeid can be set to 0 and will be derived from entityType

When an exact match is required (@exactMatch = 1) 
then takes a similar table with additional columns languageid and countryid


suggested index: 	CREATE  CLUSTERED  INDEX IX_#tableName# ON #tableName# (entityTypeId, entityid, phrasetextid) 

*/
		


	declare @RowsInTable int,@rowsdirty int,@rowsWhichDoNotExist int, @debug bit
	
	set @debug = 0

if (@debug = 1) PRINT 'Start'	 + convert(varchar,getdate(),109)


	/*  update entityTypeIDs from entityTypes in the table  */
	select @cmd = 'update ' + 
		@tableName + ' 
	set entityTypeID = s.entityTypeID 
	from ' +
		@tableName + ' t inner join 
		schemaTable s on t.entityType = s.entityName'

	 exec(@cmd)

	 

IF @exactMatch = 0  

		   BEGIN	

				/*  Copy the incoming table into another temporary table
					makes all queries much easier having a known table name, less need for dynamic SQL
					wish there was an easier way though
				*/
				create table #TranslatePhrases (phrasetextid varchar(100),entityType varchar(100),entityTypeID int,entityID int,phrase_ident int,phraseid int ,isClean bit default 0,hasCacheRecord bit default 0)
				set @cmd = '
							insert into #TranslatePhrases (phrasetextid,entityType,entityTypeID,entityID) 
							select phrasetextid,entityType,entityTypeID,entityID
							 from 
							' + @tableName
				exec (@cmd)
				

				/* make a list of  country groups that this country is in 
				 later query seems to prefer this to running the query each time (brought back wrong results)
				*/
				declare @countryGroupList varchar(200)
				declare @acountryGroup int
			
				declare countryGroups cursor local for 
				select countrygroupid from countrygroup where countrymemberid = @RequiredCountry
				
				OPEN countryGroups
			
					select @countryGroupList = ''
			
				FETCH NEXT FROM countryGroups
				INTO @acountryGroup
			
				WHILE @@FETCH_STATUS = 0
					BEGIN
						
						select @countryGroupList = @countryGroupList + case when @countryGroupList <> '' then ',' else '' end +  convert(varchar,@aCountryGroup)  
			
						FETCH NEXT FROM countryGroups
						INTO @acountryGroup
			
					END
			
					if @countryGroupList = ''
						select @countryGroupList = '-1'		-- goes wrong for if this list is blank, so add a non existent country 		
			
			
				CLOSE countryGroups
				DEALLOCATE countryGroups
				-- we now have a variable @countryGroupList	

	
					/* 
					update all the items in temporary table with
					phraseID (from join to phraseList table),
					phrase_Ident from the phrasePointerCache table (if it exists)
					isClean 
					hasCacheRecord 
					*/
					
					 update #translatePhrases 
							set phraseid = pl.phraseid,
								phrase_ident = pp.phrase_ident,
								isclean = ~ isnull(pp.dirty,1),
								hasCacheRecord = case when pp.phraseid is null then 0 else 1 end
							from #translatePhrases  c 
								inner join 
								phraselist pl on 
													pl.phrasetextid = c.phrasetextid
													and pl.entityid = c.entityid
													and pl.entityTypeID = c.entityTypeID 
								left join
								phrasepointercache pp 
													on pl.phraseid = pp.phraseid
													and  pp.countryid =  @requiredcountry 
													and pp.languageid = @requiredlanguage 
													and pp.dirty = 0
							where c.phrase_ident is null 					

if (@debug = 1) 			select * from #translatePhrases


					/*
					Work out whether any of the phrases need updating in the cache
					*/
		
					select 
						@rowsdirty = count(1), 
						@rowsWhichDoNotExist = sum( case when hasCacheRecord = 0 then 1 else 0 end) 
					from #translatePhrases where isclean = 0    


if (@debug = 1) 	print 'Dirty Rows: ' + convert(varchar,@rowsdirty) + '. rowsWhichDoNotExist=' + convert(varchar,@rowsWhichDoNotExist) 

				if @rowsdirty <> 0
					BEGIN


						IF @rowsWhichDoNotExist <> 0 
						BEGIN

if (@debug = 1) 						PRINT  convert(varchar,getdate(),109) + '  Start putting blank rows in cache'

							/*
							Now add an item to the cache for any items where there isn't already an item in the cache
							I am doing this now so that the query at the end can just be a simple update
							Hope might lead to less conflicts 
							*/		
							insert into 
								phrasePointerCache
										(phraseid , languageid , countryid , phrase_Ident )
									select distinct c.phraseid, convert(int,@requiredlanguage), convert(int,@requiredcountry) , pp.phrase_Ident 
								from #translatePhrases c
								left join 	
								phrasePointerCache pp 
												on c.phraseid = pp.phraseid
												and  pp.countryid =  @requiredcountry 
												and pp.languageid = @requiredlanguage 
								where c.hasCacheRecord = 0 and pp.phraseid is null and c.phraseid is not null
								 								
if (@debug = 1)							 print convert(varchar,@@rowcount ) + ' Rows Inserted'

						END		-- end of adding rows to cache	

					
if (@debug = 1) 				PRINT convert(varchar,getdate(),109) + '  Start doing algorithm'	 
				
							/*
							Main algorithim, updates values of phrase_ident in the temporary table, will update the cache table in a separate step
							*/								
								select @cmd = 	
								'
								update #translatePhrases
								set phrase_Ident = isnull(p.ident,0)
								 from 
									#translatePhrases as c
									left outer join 	
									phrases as p
									on p.phraseid = c.phraseid
									and (countryid = 0 or countryid in ( ' + @countryGroupList + '))
									and (languageid in (0,@RequiredLanguage,@FallbackLanguage) or defaultForThisCountry = 1)
								where 
									c.phraseid is not null
									and 
									(c.phrase_ident is null or c.isclean = 0)
									and 
									(	
										p.ident = (select top 1 ident from phrases as p1 left join country cty on p1.countryid = cty.countryid
											where p1.phraseid = p.phraseid
												and (p1.countryid = 0 or p1.countryid in ( ' + @countryGroupList + '))
												and (languageid in (0,@RequiredLanguage,@FallbackLanguage) or defaultForThisCountry = 1)

											order by
											case 
												when p1.countryid = @RequiredCountry and languageid = @RequiredLanguage then 1
												when p1.countryid in ( ' + @countryGroupList + ') and languageid = @RequiredLanguage then 2
												when p1.countryid = @RequiredCountry   and defaultForThisCountry = 1 then 3
												when p1.countryid = @RequiredCountry   and languageid = 0 then 4  -- this line redundant when languageid 0 is removed

												when p1.countryid in ( ' + @countryGroupList + ') and defaultForThisCountry = 1 then 5
												when p1.countryid in ( ' + @countryGroupList + ') and languageid = 0 then 6 -- this line redundant when languageid 0 is removed
												when p1.countryid = 0 and languageid = @RequiredLanguage then 7

												when p1.countryid = 0 and defaultForThisCountry = 1 then 9
												when p1.countryid = 0 and languageid = 0 then 10  -- this line redundant when languageid 0 is removed

												when languageid = @FallbackLanguage and p1.countryid = @RequiredCountry  then 13
												when p1.countryid in ( ' + @countryGroupList + ') and languageid =  @FallbackLanguage   then 14
												when languageid = @FallbackLanguage and p1.countryid = 0 then 15
												else 20
												End
											)			

										)'

								/*
										--		when countryid = @RequiredCountry   and languageid = 0 then 3
										--		when countryid = @RequiredCountry   and languageid = 0 then 3
										-- 		when countryid = 0 and languageid = 0 then 6
										-- 		when languageid = @FallbackLanguage and countryid = 0 then 9

								*/
											/* logic of this is open to debate, eg if there is a phrase for the required country but wrong language, does this take precedence over the correct language for country 0
											Here is my explanation in order of best match
											1) Matching Language and Country
											2) Matching Language and Region
											3) Default Language (0) for this country
											4) Default Language (0) for this region

											5 or 9)!  Default Language (0) Default Country (0)

											[ not sure about how to use fall back language now that language 0 has been added
											  may just be needed for backwards compatability
												6) Fallback Language for this country
												7) Fallback Language for this region
												8) Fallback Language for country 0 
											]

											*/

					
if (@debug = 1) 					print @cmd

								exec	sp_executesql  @cmd ,  N'@requiredCountry int, @requiredLanguage int, @nullText varchar(100), @FallbackLanguage int,@onNullShowPhraseTextID int',	@requiredCountry = @requiredCountry , @requiredLanguage = @requiredLanguage , @nullText = @nullText, @FallbackLanguage = @FallbackLanguage , @onNullShowPhraseTextID = @onNullShowPhraseTextID

if (@debug = 1)						select * from #translatePhrases
if (@debug = 1)						PRINT 	 convert(varchar,getdate(),109) + ' Start updating cache'

						/* now update the phrase_ident column for all the items just translated */											
							 update PhrasePointerCache
								set phrase_Ident = tpp.phrase_Ident, dirty = 0
								from 
									phrasePointerCache PP 
										inner join 
									#translatePhrases tpp  on pp.phraseid = tpp.phraseid and pp.languageid = convert(int,@requiredlanguage) and pp.countryid = convert(int,@requiredcountry) 
									where  tpp.isClean = 0 
									
							
								if (@debug = 1) print convert(varchar,@@rowcount ) + ' Rows updated'

					
					END  -- end of @rowsdirty <> 0  - updating phrasepointer cache for where dirty or not exist


						if (@debug = 1) PRINT  convert(varchar,getdate(),109) + '  Start returning results'
					
					/*
					Now return the finished query if required
					*/
					IF @returnResults	= 1
					BEGIN
						select 
						C.phraseid,
						c.phraseTextid,
						c.entityid,
						c.entityTypeID,
						p.languageid,
						p.countryid,
						c.phraseTextid + case when c.entityType <> '' then '_' + c.EntityType + '_' +convert(varchar, c.entityid)  else '' end  as Identifier, 
						c.phraseTextid  as QuickIdentifier, 
						c.phraseTextid +'_' + convert(varchar,c.EntityTypeID) + '_' + convert(varchar,c.entityid) + '_' + convert(varchar,p.LanguageID) + '_' + convert(varchar,p.countryid) as UpdateIdentifier, 
						c.phraseTextid +'_' + convert(varchar,c.EntityTypeID) + '_' + convert(varchar,c.entityid) + '_'  as UpdateIdentifierStub, 
						isNull(PhraseText,case when @onNullShowPhraseTextID = 1 then c.phraseTextid else '' end + @NullText ) as PhraseText,
						Case 
							when P.phraseID is Null Then 
							 ' <A HREF=javascript:doNewTranslation(''' + c.phraseTextID + '_' + convert(varchar,c.EntityTypeID) + '_' + convert(varchar,c.entityid)  + ''')'
							Else
							 ' <A HREF=javascript:doTranslation(' + convert(varchar,P.phraseID) + ')'
							End
				
						as TranslationLink
						
							from #translatePhrases c 
								left join 
								phrases p on isnull(c.phrase_ident,-1) = p.ident
						/* added the isnull(c.phrase_ident,-1) because sometimes c.phrase_ident is null and query took ages even if only 1 record in c.  Adding the isnull did the trick  and there isn't one with id -1*/
						
					
					END	-- end of @returnResults = 1

		    END	-- end of @exactMatch = 0

	ELSE  
	
	BEGIN
	
			-- does the table have a languageid  in it?
			-- surely it is easier than all this code!
			-- need to look in both tempdb and actual db
			/* WAB 2011/02/17 changed the way I tested the temp table to use the object_id (previous method could pick up temp table from another connection).
				problem manifested itself as an error column phrase_ident does not exist */
						  
			declare @hasLanguageID int 
			select @hasLanguageID = 0
			
			select @hasLanguageID = _exists from 
				(select 1 as _exists from information_Schema.columns where table_name like @tablename and column_name = 'languageid'
					union 
				 select 1 as _exists from tempdb..syscolumns where id = object_id ('tempdb..' + @tablename) and name = 'languageid'
				) as x
		

		IF (@exactMatch = 1   and @hasLanguageID = 0) -- exact match without languageid in the table

		BEGIN			
		
					select @cmd = 'select 
					p.phraseid,
					c.phraseTextid,
					c.entityid,
					p.entityTypeID,
					p.countryid,
					p.languageid,
					isNull(p.defaultForThisCountry,0) as defaultForThisCountry,
					 c.phraseTextid + case when c.entityType <> '''' then ''_'' + c.EntityType + ''_'' +convert(varchar, c.entityid)  else '''' end  as Identifier, 
					 c.phraseTextid  as QuickIdentifier, 
					 c.phraseTextid +''_'' + convert(varchar,c.EntityTypeID) + ''_'' + convert(varchar,c.entityid) + ''_'' + convert(varchar, @RequiredLanguage ) + ''_'' + convert(varchar, @RequiredCountry ) as UpdateIdentifier, 
					 c.phraseTextid +''_'' + convert(varchar,c.EntityTypeID) + ''_'' + convert(varchar,c.entityid) + ''_'' as UpdateIdentifierStub, 
					isNull(PhraseText,@NullText) as PhraseText,
					Case 
						when p.phraseID is Null Then 
							 '' <A HREF=javascript:doNewTranslation('''''' + c.phraseTextID + ''_'' + convert(varchar,c.EntityTypeID) + ''_'' + convert(varchar,c.entityid)  + '''''')''
						Else
							 '' <A HREF=javascript:doTranslation('' + convert(varchar,p.phraseID) + '')''
						End
				
					as TranslationLink
				
				from 
					' + @tableName  + ' as c
					left outer join 	
					vphrases as p
					on p.phrasetextid = c.phrasetextid
					and convert(varchar,p.entityid) = c.entityid
					and p.entityTypeID = c.EntityTypeID	
					and p.countryid =  @RequiredCountry 
					and p.languageid =  @requiredLanguage'			
		

					exec	sp_executesql  @cmd ,  N'@requiredCountry int, @requiredLanguage int, @nullText varchar(100), @FallbackLanguage int,@onNullShowPhraseTextID int ',	@requiredCountry = @requiredCountry , @requiredLanguage = @requiredLanguage , @nullText = @nullText, @FallbackLanguage = @FallbackLanguage , @onNullShowPhraseTextID = @onNullShowPhraseTextID

		END  -- end of exact match without languageid in the table


		ELSE -- exact match 

	 BEGIN	-- this is an exact match with countryid in table

		select @cmd = 
			'select 
				p.phraseid,
				c.phraseTextid,
				c.entityid,
				c.EntityTypeID ,
				c.LanguageID,
				c.countryid,
				isNull(p.defaultForThisCountry,0) as defaultForThisCountry,  -- WAB added 2010/10/05 
				 c.phraseTextid + ''_'' + convert(varchar,c.LanguageID) + ''_'' + convert(varchar,c.CountryID) as QuickIdentifier, 
				 c.phraseTextid + case when c.EntityType <> '''' then ''_'' + c.EntityType + ''_'' + convert(varchar,c.entityid) else '''' end + ''_'' + convert(varchar,c.LanguageID) + ''_'' + convert(varchar,c.CountryID) as Identifier, 
				 c.phraseTextid +''_'' + convert(varchar,c.EntityTypeID) + ''_'' + convert(varchar,c.entityid )+ ''_'' + convert(varchar,c.LanguageID) + ''_'' + convert(varchar,c.CountryID) as UpdateIdentifier, 
				 c.phraseTextid +''_'' + convert(varchar,c.EntityTypeID) + ''_'' + convert(varchar,c.entityid) + ''_'' as UpdateIdentifierStub, 
				isNull(PhraseText,'''+ @NullText + ''') as PhraseText,
				Case 
					when p.phraseID is Null Then 
						 ''<A HREF=javascript:doNewTranslation('''' '' + c.phraseTextID + ''_''  + c.EntityType + ''_'' + convert(varchar,c.entityid) +  '' '''')''
					Else
						 '' <A HREF=javascript:doTranslation('' + convert(varchar,p.phraseID) + '')''
	
					End
			
				as TranslationLink
	
	
			from 
				' + @tableName +' as c
				left outer join 	
				vphrases as p
				on p.phrasetextid = c.phrasetextid
				and p.entityid = c.entityid
				and p.LanguageID = c.languageid
				and p.entityTypeID = c.EntityTypeID	
	
				and p.CountryID = c.CountryID'	
	
			exec (@cmd)
		
	END

 END

if (@debug = 1) PRINT 'End'	 + convert(varchar,getdate(),109)



/* The code for logging requests and doing analysis was removed to a separate
procedure so that it could be called in isolation from this code (in particular when memory resident phrases are bing used
*/
IF @logRequests = 1 or @doAnalysis = 'Yes'
BEGIN
		exec translatePhrases_Analysis @tableName = @tableName, @doAnalysis = @doAnalysis, @logRequests = @logRequests, @appIdentifier = @appIdentifier, @pageIdentifier = @pageIdentifier
END


