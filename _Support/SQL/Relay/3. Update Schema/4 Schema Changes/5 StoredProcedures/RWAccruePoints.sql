
/* NJH 2016/06/09 BF-972 - return scope_identity() rather than @@identity. Problems with FK constraints when adding record in rwTransactionSplit */

ALTER       procedure
	[dbo].[RWAccruePoints]
	@AccountID 		int,			-- ID of the account to be credited.
	@PtsAccID		int = NULL,		-- ID of the 'pot' to be Credited (if null, a new one will be created)
	@RWTransactionTypeID	varchar(2) = 'AC',	-- ID of the transaction type (CL - claimed, AC - Accrued)
	@AccruedDate		datetime = NULL,	-- Accrual date for the points
	@PointsAmount		int = NULL,		-- Points to be credited
	@CashAmount		decimal(18,6) = NULL,	-- Cash amount to be credited
	@PriceISOCode		varchar(3) = NULL,	-- Currency code (required if cash amount specified)
	@CurrencyPerPoint	decimal(18,6) = NULL,	-- Currency conversion rate
	@PersonID		int = NULL,		-- ID of user
	@ReasonID		int = NULL,		-- Reason code (pre-existing reason)
	@ReasonText		nvarchar(500) = NULL,	-- Reason text (new reason)
	@Type			varchar(2) = NULL,	-- Type of points (required if PtsAccID is null)
	@PPType			varchar(20) = NULL,	-- Points Promotion Type 
	@DistiOrgID		int = NULL,		-- Org ID of distributor involved
	@TransactionID		int = NULL,		-- Transaction ID if transaction already exists
	@CreatedPersonID	int = NULL,		-- ID of the administrator adding the points - defaults to personID	
	@ExpiryDate		datetime = NULL     -- points expiry date - defaults to a year from when the points were accrued

as
set nocount on

/***************************************************************************************
** Name   : RWAccruePoints
** Created: Dec/2000
** By     : JVH
**
** Purpose: 
** Accrue points to a specific account or 'pot'  
** 
** Arguments:
**      See above
**
** Syntax example: 
**
**
****************************************************************************************
** Modifications
** 
** Version  Date         Who  Comments
** -------  -----------  ---  ----------------------------------------------------------
** 1.00	    Dec/2000     JVH  Original
** 1.01     26/Jan/2001  JVH  Added @DistiOrgID as an optional parameter 
** 1.02     30/Jan/2001  JVH  Allow cash amount and accrued date to be specified. 
** 1.03	    02/Mar/2001  WAB  Column DistiOrgID in rwPointsAccrued was changed to DistiOrganisationID to aid with deduping
** 1.04		30/Mar/2007	 NJH  Added points expiry date as a param. If not passed in, it's set to be a year from day accrued
**
**
****************************************************************************************/


/*===================================================================================*/
/* Initialisations                                                                   */
/*===================================================================================*/
Declare	@ErrCode int,	-- Error return code
	@NewRWTransactionID int,	-- Transaction ID of new transaction being created
	@RWPromotionID int,	-- Points Promotion integer id
	@ActivationDate datetime,	-- Points ActivationDate
	@WeekEndingDate datetime 	-- Week Ending date for current week
	--@ExpiryDate datetime		-- Expiry Date (2 quarters + 5 weeks after week ending)

SET @ErrCode = 0

/*===================================================================================*/
/* Validations                                                                       */
/*===================================================================================*/
-- Check the Account ID is supplied and exists
IF @AccountID is NULL
 BEGIN
   RAISERROR ('Error : AccountID is required but was not supplied',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

IF NOT EXISTS(SELECT 1 FROM RWCompanyAccount WHERE AccountID = @AccountID)
 BEGIN
   RAISERROR ('Error : AccountID %d cannot be found',
	      16, 1, @AccountID)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Check that the 'Pot' ID is specified and is valid if specified. If not specified, check other
-- required parameters are supplied
IF @PtsAccID is not NULL 
 BEGIN 
   IF NOT EXISTS(SELECT 1 FROM RWPointsAccrued WHERE PtsAccID = @PtsAccID)
    BEGIN
      RAISERROR ('Error : PtsAccID %d cannot be found',
	        16, 1, @PtsAccID)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END
 END
ELSE
 BEGIN
   IF @Type IS NULL
    BEGIN
      RAISERROR ('Error : Points Type is required but was not supplied',
	         16, 1)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END
 END

/* Allow NULL PersonID - will default to user_id()
-- Check PersonID is provided
IF @PersonID is NULL
 BEGIN
   RAISERROR ('Error : PersonID is required but was not supplied',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END
*/

-- Check that Points or cash amount has been specified and is valid
IF @CashAmount is NULL AND @PointsAmount is NULL
 BEGIN
    RAISERROR ('Error : Cash or Points value is required',
        16, 1)
    SET @ErrCode = -1
    GOTO ERRORTRAP
 END

-- Check that only a points OR a cash value has been specified
IF @CashAmount is not NULL AND @PointsAmount is not NULL
 BEGIN
   RAISERROR ('Error : Only ONE of Cash Value or Points Value should be specified',
       16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Cash transactions must have the currency code. Conversion rate is optional as can be retrieved from
-- RWPointsConversion
IF @CashAmount is NOT NULL 
 BEGIN
   IF @PriceISOCode is  NULL
    BEGIN
       RAISERROR ('Error : Cash transactions must specify currency',
	          16, 1)
       SET @ErrCode = -1
       GOTO ERRORTRAP
    END

   -- Check that the specified currency code exists
   IF NOT EXISTS(SELECT 1 FROM RWPointsConversion WHERE PriceISOCode = @PriceISOCode)
    BEGIN
       RAISERROR ('Error : Currency Code %s is not valid',
	         16, 1, @PriceISOCode)
       SET @ErrCode = -1
       GOTO ERRORTRAP
    END
 END

/* Allow negative accruals
IF @PointsAmount < 0
 BEGIN
   RAISERROR ('Error : Points amount (%d) is less than zero',
	      16, 1, @PointsAmount)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END
*/

-- Validate ReasonID and text (not required; should not both be supplied)
IF @ReasonID is not NULL and @ReasonText is not NULL
 BEGIN
   RAISERROR ('Error : Only ONE of ReasonID or Reason Text should be specified',
	      16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Validate the Points Promotion ID and get the integer id
IF @PPType is NOT NULL
 BEGIN
   IF NOT EXISTS(SELECT 1 FROM RWPromotion WHERE RWPromotionCode = @PPType)
    BEGIN
      RAISERROR ('Error : Promotion Type %s cannot be found',
	        16, 1, @PPType)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

   SELECT @RWPromotionID = RWPromotionID FROM RWPromotion WHERE RWPromotionCode = @PPType
 END


-- Validate the Disti Org ID if supplied
IF @DistiOrgID is NOT NULL
 BEGIN
   IF NOT EXISTS(SELECT 1 FROM Organisation WHERE OrganisationID = @DistiOrgID)
    BEGIN
      RAISERROR ('Error : Distributor ID %d cannot be found',
	        16, 1, @DistiOrgID)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

 END

-- Check that Accrued Date is not in the future, if entered
IF @AccruedDate is not NULL and @AccruedDate > GetDate()
 BEGIN
   RAISERROR ('Error : Accrued Dates in the future are not allowed',
	       16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END


-- Check that Expiry Date is not in the past, if entered
IF @ExpiryDate is not NULL and @ExpiryDate < GetDate()
 BEGIN
   RAISERROR ('Error : Expiry Dates in the past are not allowed',
	       16, 1)
   SET @ErrCode = -1
   GOTO ERRORTRAP
 END

-- Set the expiry date to be a year from now if not passed in
IF @ExpiryDate is NULL SET @ExpiryDate = dateadd(yyyy,1,GetDate())


/*-----------------------------------------------------------------------------------------*/
/* If a new reason is supplied, insert into RWTransactionReason and get the new ID */
/*-----------------------------------------------------------------------------------------*/
IF @ReasonText is not NULL
 AND NOT EXISTS(SELECT 1 FROM RWTransactionReason WHERE ReasonText = @ReasonText)
 BEGIN
   INSERT RWTransactionReason(ReasonText, CreatedBy, CReated, UpdatedBy, Updated)
   VALUES(@ReasonText, @PersonID, getdate(), @PersonID, getdate())

   IF @@Error <> 0
    BEGIN
      RAISERROR ('Error : Unable to save new reason record',
	         16, 1)
      SET @ErrCode = -1
      GOTO ERRORTRAP
    END

   SET @ReasonID = SCOPE_IDENTITY()

 END

-- Set Person ID to current DB user if NULL
IF @PersonID is NULL SET @PersonID = user_id()

/*===================================================================================*/
/* Set working variables                                                             */
/*===================================================================================*/
-- For cash transactions, get conversion rate if necessary, and calculate points amount
IF @CashAmount is Not NULL
 BEGIN

   IF @CurrencyPerPoint is NULL
      SELECT 	@CurrencyPerPoint = CurrencyPerPoint 
      FROM 	RWPointsConversion
      WHERE 	PriceISOCode = @PriceISOCode

   SET @PointsAmount = @CashAmount / @CurrencyPerPoint

 END 

-- Calculate the necessary dates for a new Points Accrued Entry. If an accrual date is
-- specified, work out the w/e date for that week. Otherwise get the current w/e date.

SET @WeekEndingDate = 
   CASE
	WHEN @AccruedDate is NULL THEN dateadd(d, 7-datepart(dw, GetDate()), GetDate())
	ELSE dateadd(d, 7-datepart(dw, @AccruedDate), @AccruedDate)
   END

SET @ActivationDate = isnull(@AccruedDate, GetDate())
--SET @ExpiryDate = dateadd(wk, 5, dateadd(q, 2, @WeekEndingDate))

/*===================================================================================*/
/* Create database entries                                                           */
/*===================================================================================*/
BEGIN TRANSACTION

/*-----------------------------------------------------------------------------------*/
/* RWTransaction                                                                     */
/*-----------------------------------------------------------------------------------*/
IF @TransactionID IS NULL
BEGIN
INSERT RWTransaction(RWTransactionTypeID, 
			AccountID, 
			OrderID, 
			CreditAmount, 
			DebitAmount, 
			PriceISOCode, 
			CurrencyPerPoint, 
			RWTransactionReasonID,
			Created, 
			CreatedBy,
			personid)
VALUES( @RWTransactionTypeID,
	  @AccountID,
	  NULL,
	  @PointsAmount,
	  0 ,
	  NULL,
	  NULL,
	  @ReasonID,
	  GetDate(),
	  isnull(@CreatedPersonID,@PersonID), 
	  @PersonID )

--** Get the TransactionID just created **--
SET @NewRWTransactionID = SCOPE_IDENTITY()
END

IF @TransactionID IS NOT NULL
BEGIN
SET @NewRWTransactionID = @TransactionID
END

/*-----------------------------------------------------------------------------------*/
/* RWPointsAccrued                                                                   */
/*-----------------------------------------------------------------------------------*/
IF @PtsAccID is NOT NULL
 BEGIN
   UPDATE RWpa
   SET	RWpa.Balance = RWpa.Balance + RWtr.CreditAmount - RWtr.DebitAmount
   FROM	RWPointsAccrued RWpa
   JOIN	RWTransaction RWtr	
   ON	RWtr.AccountID = RWpa.AccountID
   WHERE RWtr.RWTransactionID = @NewRWTransactionID
   AND  RWpa.PtsAccID = @PtsAccID
 END
ELSE
 BEGIN

   INSERT RWPointsAccrued(AccountID, Points, Type, WeekEndingDate, ActivationDate, ExpiryDate, CReated, CReatedBy, RWPromotionID, Balance, DistiOrganisationID)
   VALUES(@AccountID, @PointsAmount, @Type, @WeekEndingDate, @ActivationDate, @ExpiryDate, GetDate(), 0, @RWPromotionID, @PointsAmount, @DistiOrgID)

   SET @PtsAccID = SCOPE_IDENTITY()
 END


/*-----------------------------------------------------------------------------------*/
/* RWTransactionSplit                                                                */
/*-----------------------------------------------------------------------------------*/
INSERT RWTransactionSplit
SELECT @NewRWTransactionID, @PtsAccID, @PointsAmount, 0


/*-----------------------------------------------------------------------------------*/
/* Finally update the running balance in RWTransaction                               */
/*-----------------------------------------------------------------------------------*/

/*OLD VERSION*/
/*UPDATE 	RWtr
SET	RWtr.SnapshotBalance = (SELECT isnull(SUM(isnull(RWpa.Balance,0)),0)
			   FROM	RWPointsAccrued RWpa
			   WHERE RWtr.AccountID = RWpa.AccountID
)
FROM  RWTransaction RWtr
WHERE  RWtr.RWTransactionID = @NewRWTransactionID*/

/*NEW VERSION*/
UPDATE 	RWtr
SET	RWtr.SnapshotBalance = (SELECT sum(creditAmount)-sum(debitAmount)
			   FROM	RWTransaction RWtr1
			   WHERE RWtr.AccountID = RWtr1.AccountID)
FROM  RWTransaction RWtr
WHERE  RWtr.RWTransactionID = @NewRWTransactionID

COMMIT TRANSACTION

/*===================================================================================*/
/* Common exit point                                                                 */
/*===================================================================================*/
EXITPOINT:

Return @ErrCode

/*===================================================================================*/
/* Handle any errors                                                                 */
/*===================================================================================*/
ERRORTRAP:

IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION
GOTO EXITPOINT

RETURN @NewRWTransactionID


GO