if exists (select 1 from sysobjects where name='sp_fieldTrigger_oppSalesMgrModified' and xType='P')
drop procedure sp_fieldTrigger_oppSalesMgrModified

/****** Object:  StoredProcedure [dbo].[sp_fieldTrigger_oppSalesMgrModified]    Script Date: 09/25/2014 09:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppSalesMgrModified] 
	@modEntityid int, 
    @modaction varchar(10),
    @triggerInitiatedBy int, 
    @RecordUpdatesAgainstID int,
    @triggerInitiatedByPersonID int = null
AS
Set nocount on

declare @salesManagerFlagID int
select @salesManagerFlagID = flagID from flag where flagTextID = dbo.getSettingValue('leadManager.vendorSalesManagerFlagTextIDs')

IF (@modaction in ('oppA','oppM') and isNull(@salesManagerFlagID,0) != 0)
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated,lastUpdatedByPerson) 
	select vendorSalesManagerPersonID,@salesManagerFlagID,@RecordUpdatesAgainstID,getDate(),@RecordUpdatesAgainstID,getDate(),@triggerInitiatedByPersonID
	from #modifiedEntityIDs m
		inner join opportunity opp on opp.opportunityID = m.entityID
		left join booleanFlagData bfd on bfd.entityID = opp.vendorSalesManagerPersonID and bfd.flagid=@salesManagerFlagID
	where
		bfd.entityID is null
END

Set nocount off