/****** Object:  StoredProcedure [dbo].[sp_fieldTrigger_oppPartnerModified]    Script Date: 09/24/2014 16:38:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_fieldTrigger_oppPartnerModified] 
	@modEntityid int, 
    @modaction varchar(10),
    @triggerInitiatedBy int, 
    @RecordUpdatesAgainstID int,
    @triggerInitiatedByPersonID int = null
AS
Set nocount on

/*if a location is allocated as the partnerLocationID - opp field -  
their organisation will be flagged with [org boolean flag] 'OppPartner'  */
declare @partnerFlagID int
set @partnerFlagID = 0
if exists(select flagid from flag where flagtextid= 'OppPartner')
	select @partnerFlagID=flagid from flag where flagtextid= 'OppPartner';

IF @modaction in ('oppA','oppM') and @partnerFlagID > 0
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select distinct l.organisationID,@partnerFlagID,@RecordUpdatesAgainstID,getDate(),@RecordUpdatesAgainstID,@triggerInitiatedByPersonID
	from #modifiedEntityIDs as m
		inner join opportunity opp on opp.opportunityID = m.entityID
		inner join location l on opp.partnerLocationID = l.locationID
		left join booleanFlagData bfd on bfd.entityid = l.organisationID and bfd.flagID=@partnerFlagID
	where bfd.entityID is null
END
Set nocount off