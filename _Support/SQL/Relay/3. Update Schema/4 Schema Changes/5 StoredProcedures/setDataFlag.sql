if exists (select 1 from sysobjects where name = 'setDataFlag')
BEGIN
drop procedure setDataFlag
END

GO
CREATE  Procedure [dbo].[setDataFlag]
	@tablename varchar(100),
	@columnname varchar(100) = 'data',
	@flagId int,
	@userGroupID int,
	@personId int
AS
declare @flagDataTable varchar(20)
declare @sql nvarchar(4000)
declare @baseWhereClause nvarchar(4000)
declare @sqlerror int

set @sql = N'select @flagDataTable=ft.dataTableFullName from flagType ft inner join flagGroup fg
	on ft.flagTypeId = fg.flagTypeID inner join flag f
	on f.flagGroupId = fg.flagGroupId
	where f.flagID = '+cast(@flagId as varchar)
exec sp_executesql @statement=@sql,@params=N'@flagDataTable varchar(20) output',@flagDataTable=@flagDataTable output

-- update
set @baseWhereClause = ' from '+ @tablename+' t
			inner join '+@flagDataTable+' d on t.entityID = d.entityID
			inner join flag f on d.flagID = f.flagid and f.flagID = '+cast(@flagId as varchar)+'
		where t.'+@columnName+' is not null
			and t.'+@columnName+' != d.data'
		
set @sql = 'update '+@flagDataTable+' set data = t.'+@columnName+',lastUpdated=getDate(),lastUpdatedBy='+cast(@userGroupID as varchar)+',lastUpdatedByPerson='+cast(@personID as varchar)
	+@baseWhereClause + ' and ltrim(rtrim(t.'+@columnName+')) != '''''
exec (@sql)

--delete		
set @sql = 'update '+@flagDataTable+' set lastUpdated=getDate(),lastUpdatedBy='+cast(@userGroupID as varchar)+',lastUpdatedByPerson='+cast(@personID as varchar)
	+@baseWhereClause + ' and ltrim(rtrim(t.'+@columnName+')) = '''''
exec (@sql)
set @sql = 'delete '+@flagDataTable+@baseWhereClause + ' and ltrim(rtrim(t.'+@columnName+')) = '''''
exec (@sql)

-- insert new values
set @sql = N'insert into '+@flagDataTable+' (entityID,flagid,data,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
	select t.entityID, f.flagID,t.'+@columnName+', getDate(), '+cast(@userGroupID as varchar)+', getDate(), '+cast(@userGroupID as varchar)+','+ cast(@personID as varchar) +'
	from 
		flag f inner join '+ @tablename+' t on 1=1
		left join '+@flagDataTable+' d on d.entityID = t.entityID and d.flagid = f.flagid
		where
			f.flagID = '+cast(@flagId as varchar)+'
			and d.FlagID is null
			and t.'+@columnName+' is not null and ltrim(rtrim(t.'+@columnName+')) != '''''
exec (@sql)