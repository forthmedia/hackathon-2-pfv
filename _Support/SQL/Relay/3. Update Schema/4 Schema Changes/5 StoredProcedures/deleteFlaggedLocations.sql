/* 
2013-06-05 WAB CASE 435612 QUOTED_IDENTIFIER Must be ON for deletion of locations because they have an indexed view
*/

SET QUOTED_IDENTIFIER ON 
GO

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DeleteFlaggedLocations' and Type = 'P')
	DROP PROCEDURE DeleteFlaggedLocations
	
GO
	
CREATE  Procedure [dbo].[DeleteFlaggedLocations]
@TimeLimit int=1 ,   -- number of hours to wait from flagging until deletion can happen
@Test bit = 0 		-- when set to 1, no deletions take place




AS




/*** Deletion of Locations and Persons therein 
	Updated by pmcc 28 Feb 00  

2007-11-14 WAB created this version (named deleteFlaggedLocations) which contains feedback - still under development
2007/11/19  WAB   incorporated separate procedures for checking for protected flags etc. and for deleting flags
2013-11-11	WAB 	CASE 437918 removed setUser
2016/01/14	NJH		added lastUpdatedByPerson to deleteEntity temp table for tidiness sake
**/



Set nocount on
If @TimeLimit=999 Set @TimeLimit=0
Select @TimeLimit=0   --<< This overrides the time limit.
Declare @StartDel datetime, @StartDelWithLimit datetime,
@LocToBeDeleted int, @OrgToBeDeleted int,
@DelGo Varchar(5), @UserSet Varchar(30),
@StartedTran bit,		-- Indicates whether a transaction was started inside this proc
@ErrCode int			-- Error code
Declare @DeleteFlagID int

set @startedTran = 0

select @DeleteFlagID = flagid from flag where flagtextid = 'deletelocation'
Select @Delgo='delok', @UserSet='dbo'


If @delgo<>'delok'
BEGIN
Print 'You must supply the argument ''delok'' to get the delete to work'
--return(0)
END
/*2013-11-11 WAB removed setUser.  Deprecated, sometimes causes an error, already removed from deleteFlaggedPeople/Organisations and does not seem to have a particular purpose
Exec('setuser '''+@UserSet+'''')
*/  
Select @StartDel=getdate()
Select @timelimit=abs(@timelimit)*-1
Select @StartDelWithLimit=DATEADD(hour, @TimeLimit, @StartDel)
--IDs of Locations to be deleted:(FlagID=91 @DeleteFlagID)
--****This WILL exclude Locs with people:****

-- create a table to keep track of people Not Deleted
Create table #NotDeletable(EntityID int,Reason varchar(50),lastupdated  datetime, extendedInfo varchar(max))


insert into #NotDeletable (entityid, reason)
Select EntityID, 
	case when B.LastUpdated > @StartDelWithLimit then
		'Marked for deletion recently'
	when P.LocationID is not Null then
		'Contains People ' 
	else 
		'Unknown Reason'
	end
From BooleanFlagData as B Left Join Person as P On B.EntityID=P.LocationID
Where B.FlagID=@DeleteFlagID and 
(B.LastUpdated>=@StartDelWithLimit or P.LocationID is not Null)


Select B.EntityID as entityid, B.EntityID as LocationID, B.LastUpdatedBy as DeletedBy, b.lastUpdatedByPerson as deletedByPerson
into #deleteEntityIDTable
From BooleanFlagData as B Left Join Person as P On B.EntityID=P.LocationID
Where B.FlagID=@DeleteFlagID and B.LastUpdated<@StartDelWithLimit and P.LocationID is Null
Select @LocToBeDeleted = @@rowcount

print 'LocToBeDeleted = ' + convert (varchar,@LocToBeDeleted )

IF @LocToBeDeleted=0 GOTO TheEnd

	-- Exclude Location records that happen to have any Protected flags:
	-- need to pass #deleteentityIDTable into deleteEntityCheck, 
	-- need to have created #notDeletable table which gets updated with items which can't be deleted

	
	exec DeleteEntityCheck @entityTypeID = 1


Delete From #deleteEntityIDTable
Where LocationID IN (Select EntityID From #NotDeletable)


/*** Now Start Deleting the Locations... ***/
Select @LocToBeDeleted=count(*) from #deleteEntityIDTable



IF @LocToBeDeleted=0 GOTO TheEnd
IF @test=1 GOTO TheEnd

--For safety, if the number to be deleted is very large then the delete
-- will not proceed and the DBA will have to intervene:
---IF @LocToBeDeleted>300 GOTO TheEnd
-- Getting Rid of this above line overrides the limit on the number
--    of records that are allowed to be deleted at once.

-- If this proc wasn't called from inside a transaction, start a new one
If @@TranCount = 0
 begin
   set @StartedTran = 1
   BEGIN TRANSACTION
 end


exec deleteentityflags @entityTypeID = 1
Set @ErrCode = @@Error		
If @ErrCode <> 0 GOTO ErrorTrap



--Location LDS:
Delete
From LocationDataSource
Where LocationID IN (Select LocationID from #deleteEntityIDTable)
Set @ErrCode = @@Error		
If @ErrCode <> 0 GOTO ErrorTrap



--Location Records:
Delete
From Location
Where LocationID IN (Select entityID from #deleteEntityIDTable)
Set @ErrCode = @@Error		
If @ErrCode <> 0 GOTO ErrorTrap



--This will ensure that the user who originally set the delete flag
---will be recorded in the modRegister as the user who did the deletion:
Update M
Set M.ActionByCF=L.DeletedBy
From ModRegister as M Join #deleteEntityIDTable as L
On M.RecordID=L.EntityID
Where (Action='LD' and ModDate>=@StartDel)

Set @ErrCode = @@Error		
If @ErrCode <> 0 GOTO ErrorTrap



TheEND:


-- return query of all locations deleted (or which will be deleted in case of a test)
if @test = 0
	select 'Deleted' as DeleteAction, l.locationid, l.sitename  from locationdel l inner join #deleteEntityIDTable ltd on l.locationid = ltd.locationid
else
	select 'Will be deleted' as DeleteAction, l.locationid, l.sitename   from location l inner join #deleteEntityIDTable ltd on l.locationid = ltd.locationid



-- return query of all people not deleted for some reason
if @test = 0
  Begin
	select reason, l.locationid, l.sitename   , nd.lastupdated, extendedinfo
	from location l inner join #notdeletable nd on l.locationid = nd.entityid

	union 

	-- these are any items which were in ToDeleteLoc but for some reason are still in the location table
	select 'Unknown reason', l.locationid, l.sitename   , null ,null
	from location l inner join #deleteEntityIDTable td on l.locationid = td.locationid
  end
else
  begin
	select reason, l.locationid, l.sitename   , nd.lastupdated, extendedinfo
	from location l inner join #notdeletable nd on l.locationid = nd.entityid
  end





/*===========================================================================================*/
-- Exit
/*===========================================================================================*/
If @@TranCount > 0 and @StartedTran = 1
   COMMIT TRANSACTION

ExitPoint:
RETURN @ErrCode

/*===========================================================================================*/
-- Error Trap
/*===========================================================================================*/
ErrorTrap:

If @@TranCount > 0 and @StartedTran = 1
   ROLLBACK TRANSACTION

GOTO ExitPoint
