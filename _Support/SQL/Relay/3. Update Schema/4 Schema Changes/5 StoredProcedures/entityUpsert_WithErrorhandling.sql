IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'entityUpsert_withErrorHandling' and Type = 'P')
	DROP PROCEDURE entityUpsert_withErrorHandling
	
GO

CREATE PROCEDURE [dbo].[entityUpsert_withErrorHandling]
	/* for documentation of these parameters see entityUpsert */
	@upsertTableName sysname  ,
	@entityName sysname,					
	@lastUpdatedByPerson int = null,		
	@columns_Updated varbinary (50) = -1,
	@processNulls  varbinary (50) = -1,  	
	@truncate  varbinary (50) = 0,  		
	@whereClause nVarchar(max) = '',  		
	@orderBy nVarchar(100) = '',  		
	@returnTableName sysname = '',			-- can return results as a recordset or, if a name is supplied here, in a table
	@debug bit = 0,							-- passed through to entityUpsert
	@skipValidation bit = 0,
	@maxIterations int = 30,   				/* note that number of iterations does not equate exactly to the number of errors */
	@maxBatchSize int = 5000,  				/* max number to be processed per iteration */
	@waitBetweenBatchesSeconds int = 2,   	/* seconds wait between batches - will break if > 60 but only imagine being set in range 1 - 10 */
	@AdditionalErrorMessages nvarchar (max) = '' OUTPUT  /* this variable returns any error messages which are not returned in the return table. These will usually be errors which only occur when updating multiple records and which disappear when single records are updated */
AS 


/*
entityUpsert_withErrorHandling
Author:		WAB
Date:		2014/05/09
Purpose:	This is a wrapper around entityUpsert which combines validation, dataUpdate and errorHandling into a single procedure
			It allows a single call to be made which will update as many records as possible while returning details of errors on any rows which fail


Returns:	A query (or table) indicating which rows have been inserted/updated or have failed validation or have failed during update.
			There will always be one row per input record
			ACTION, ENTITYID, UPSERTUNIQUEID, FIELD, MESSAGE, VALUE
			

Debugging:  @debug=1 will print some debugging information - in particular all queries being run by entityUpsert
			Whether debugging is on or off a temporary debug table is created for the duration of the run.
			This will show what errors are being picked up
			This can be accessed using this code:
						declare @tablename sysname
						select @tablename = name from tempdb..sysobjects where name like '##ups%' order by crdate asc
						if @tablename is not null
						BEGIN
							declare @sql nvarchar (max) = 'select * from ' + @tablename + '  order by id'
							exec (@sql)
						END
 


WAB 2014-09-29  Deal with possible infinite (or long running) loop if error occurred in very last record
WAB 2015-01-14	Deal with infinite loop if error occurred when zero records is the incoming table
				Added an debugging table which exists while the procedure is running and records details of errors.  
				Useful for debugging errors which cause the process to run 1 row at a time  
WAB 2015-01-15	If error occurred when there was a single record in the incoming table, the return table was not being populated
WAB 2015-02-12	Error if @returnTableName was not a temporary table.  Query to check for existence of upsertUniqueID was referencing the tempdb!
WAB 2015-04-13	Add dbname, upsertTableName and spid to @debugTableName  
WAB 2015-05-12	If passed a view, convert to a temp table here rather than leaving it to entityUpsert
WAB 2015-06-22	Add support for @maxIterations and pass back @AdditionalErrorMessages 
				Removed support for @maxConsecutiveErrors
				Added support for an @orderBy parameter (allowed us to put items which were known to have errored previously to be done last)
WAB 2015-09-21	Added code to try to improve performance and prevent blocking when doing very large updates
				Add a @maxBatchSize parameter
				Add a @waitBetweenBatchesSeconds parameter
WAB 2015-12-18  BF-87 Items being inserted which gave validation errors were not being excluded from subsequent processing (because a NOT IN was using entityID rather
*/


declare 
	@rowsLeft int,  /* number left to process */
	@rowsToProcess int = null,  /* number to be processed on this run */
	@continue bit = 1,  /* loop controller */
	@newwhereclause varchar(max) = '',  /* as rows are excluded from successive iterations we make a new where clause here */
	@sqlcmd nvarchar(max),
	@entityIDColumn nVarChar(100),
	@rowcount int,					/* to hols @@rowCount after a successful upsert */
	@successCount int = 1,  /* used to decide whether we need to reduce or increase the number of items in a batch (to either home in on an error or speed up processing) */
	@debugMessage  nvarchar(max),
	@debugSQL nvarchar(max),
	@debugTableName sysname, 
	@isView bit ,					/* whether incoming table is actually a view */
	@tempTableName sysname,			/* when incoming table is a view we generate a temporary table - this is its name */
	@rowsprocessed int =0,			/* keeps track of how many rows have been processed - for debug and messaging purposes */
	@numberOfIterations int = 0,     /* used to break out if greater than @maxIterations */
	@errorMessage nvarchar(max) ,
	@errorSeverity int ,
	@errorState int ,
	@waitString varchar (8) = '00:00:' + right('0' + convert(varchar,11),@waitBetweenBatchesSeconds)
			

SET NOCOUNT ON
	BEGIN TRY

		/* create a table to store the results of both validation and update */
		CREATE TABLE  #itemsdone 
			(action sysname, entityid int, upsertuniqueid varchar(100), field sysname null, message nvarchar(max), value nvarchar(max))

		create index itemsdone_upsertuniqueid on #itemsDone (upsertuniqueid)

		/* create a table to pass to upsertEntity in which results are returned (by passing a temp table we get round problems of creating a table with a unique name) */
		create table #tmpUpsertResult (action sysname, entityID int, upsertUniqueID varchar(50))

		/* create a table to hold error messages so that we can detect error messages which occur but do not get logged (usually caused by triggers which work on single rows but not multiple rows) */
		create table #errorMessages (message varchar(max),startRow int, numberOfRows int)

		/* work out the entityID column */
		select 	@entityIDColumn=uniqueKey from schemaTable where entityName=@entityName
		
		/* 
			work out whether the upsert is a view or not, bit awkward because it may or may not be a temporary table 
			also indirectly checks whether incoming table exists
		*/
		IF left(@upsertTableName,1) = '#' 
		BEGIN
			SELECT @isView = CASE WHEN type = 'V' then 1 else 0 end
			from tempdb.sys.objects  where object_id = object_id('tempdb..' + @upsertTableName) 
		END
		ELSE
		BEGIN
			SELECT @isView = CASE WHEN type = 'V' then 1 else 0 end
			from sys.objects  where object_id = object_id(@upsertTableName) 
		END


		IF @isView is null
		BEGIN
			set @errorMessage  = 'Table ' + @upsertTableName + ' does not exist'
				RAISERROR (@errorMessage,16,0)
				GOTO THEEND
		
		END
		

		/* 
			WAB 2015-05-12
			If we have been passed a view then copy into temporary table
			This would happen anyway in entityUpsert, but by doing it here we improve performance if we end up with errors and go into a long loop
		 */
		IF @isView = 1 OR @orderBy <> ''
		BEGIN
			SELECT
				 @tempTableName = '##' + replace(@upsertTableName,'#','') + replace(newID(),'-','')
			
			SELECT 
				@sqlCMD = 'Select * into '  + @tempTableName + ' from ' + @upsertTableName + ' as x ' + case when @orderBy <> '' then 'order by ' + @orderBy else '' end,
				@upsertTableName = @tempTableName
			
			EXEC (@sqlCMD)
			
		END


		/*	add upsertUniqueID column if it does not already exist
			Note this code is a duplicate of that in entityUpsert
			WAB 2015-02-12 fixed error for no temp table
		*/
			
		IF 
			(left(@upsertTableName,1) = '#' AND NOT exists (select 1	FROM 	tempdb.sys.columns c where object_id = object_id('tempdb..' + @upsertTableName) and name = 'upsertUniqueID'))
			OR
			(left(@upsertTableName,1) <> '#' AND NOT exists (select 1	FROM 	sys.columns c where object_id = object_id( @upsertTableName) and name = 'upsertUniqueID'))
		
		BEGIN
		  set @sqlCmd = 'alter Table ' +  @upsertTableName+ '  add  upsertUniqueID  int identity(1,1)'
		  exec sp_executeSQL @sqlCmd 
		END

		/* create a debug table for debugging while the process is running, deleted at end of run */
		SET @debugTableName = '##upsertDebug_' + db_name() + '_spid' + convert(varchar,@@SPID) + '_' + @upsertTableName + '_' + replace(convert(varchar(50),newID()),'-','')
		print 'Debug Table: ' + @debugTableName
		SET @sqlCmd = 'create table ' + @debugTableName + '(id int identity(1,1), message nvarchar(max), created datetime default getdate())'
		exec sp_executeSQL @sqlCmd
		
		/* this statement used to insert into the debug table */
		SET @debugSQL = 'insert into ' + @debugTableName + ' (message) values (@debugMessage)'

		exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@upsertTableName
		exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@entityName
		if @whereClause is not null
		BEGIN
			set @debugMessage = 'WhereClause: ' + @whereClause
			exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage 
		END	
	
	

			/*  In some instances the validation step will have already been run, so we can skip this step */
			IF @skipValidation = 0
			BEGIN
				/* First run a validation step, record all items with validation errors, 
					so that they are excluded from the update attempt 
				*/
				create table #tmpValidateResult (entityID int, upsertUniqueID varchar(50), field sysname, message nvarchar(max), value nvarchar(max))

				exec 
					entityUpsert
					@upsertTableName = @upsertTableName,
					@entityName = @entityName,
					@lastupdatedbyperson = @lastupdatedbyperson,
					@debug = @debug,
					@validate = 1,
					@whereclause = @whereclause,
					@returnTableName = '#tmpValidateResult'   /* TBD need a unique name for the return table */

				/* Any validation errors need to be inserted into #itemsdone table 
					Only want to end up with one row per item, hence the odd insert and then an update
					WAB 2015-12-18 BF-87 No longer need to worry about global errors (where entityID column is null) - I don't return any (so remove entityID is not null condition on next query)
										Update query needs to join on upsertuniqueid, not entityid, since entityid will be null for inserts 			
				*/
				
				INSERT INTO
						#itemsdone 
						(action , entityid , upsertuniqueid)
				SELECT  DISTINCT
						'VALIDATIONERROR' , entityid , upsertuniqueid
				FROM	
						#tmpValidateResult
						
				UPDATE
						#itemsdone				
				SET
						field = v.field,
						message = v.message,
						value = v.value
							
				FROM
						#itemsdone	d			
							INNER JOIN
						#tmpValidateResult v ON 	d.upsertuniqueid = v.upsertuniqueid

				drop table #tmpValidateResult	 	

			END

			
			/* set a new where clause which combines the passed in where clause and a clause to exclude items already processed */
			select @newwhereclause = case when @whereclause <> '' THEN '(' + @whereClause + ') AND ' ELSE ' ' END  +  ' upsertuniqueid not in (select upsertuniqueid from #itemsdone)'

	END TRY


	BEGIN CATCH

		SELECT
			@errorMessage  = error_message() + ' Line:' + convert(varchar,error_line()) + '. ' + convert(varchar, getdate(),114),
			@errorSeverity = error_severity(),
			@errorState  = error_state()
			
		RAISERROR (@errorMessage,@errorSeverity,@errorState)
		RETURN -1


	END CATCH


WHILE @continue = 1
BEGIN

	/* work out total number of rows left to do */
	select @sqlcmd = 'select @rowsLeft = count(*) from ' + @upsertTableName + case when @newWhereClause <> '' then ' WHERE ' + @newWhereClause  ELSE '' END 
	exec sp_executesql @sqlcmd,N'@rowsLeft int OUTPUT',@rowsLeft OUTPUT

	truncate table #tmpUpsertResult
	
	BEGIN TRY

		/* run the update for the given number of rows */
		
		set @debugMessage = 'Rows left: ' + convert(varchar,@rowsLeft) + '. Rows to process: ' + isNull (convert(varchar,@rowsToProcess),'ALL')
		print @debugMessage + ' ' + convert(varchar,getdate(),114)
		exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage

		/* If the number of rows to process is larger than the maximum batch size, then reduce the rows to process accordingly */
		IF isNull(@rowsToProcess,@rowsLeft) > @maxBatchSize 
			BEGIN
				set @rowsToProcess = @maxBatchSize 
				set @debugMessage = 'Maximum Batch Size Used ' + convert(varchar,@rowsToProcess)
				exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage
			END	

		exec 
			entityUpsert
			@upsertTableName = @upsertTableName,
			@entityName = @entityName,
			@lastupdatedbyperson = @lastupdatedbyperson,
			@columns_Updated = @columns_Updated,
			@processNulls  = @processNulls,
			@truncate  = @truncate  ,
			@debug = @debug,
			@whereclause = @newWhereclause,
			@returnTableName = '#tmpUpsertResult',  
			@rows = @rowsToProcess

			/* copy the results into the #itemsdone table */
			INSERT INTO #itemsdone 
				(action , entityid , upsertuniqueid )
			SELECT 
				action , entityid , upsertuniqueid 
			FROM 
				#tmpUpsertResult	  
			
			select @rowcount = @@rowcount
			
	 		set @debugMessage =  'Processed ' + convert(varchar,@rowcount) + ' rows, starting at row ' + convert(varchar,@rowsprocessed +1) + ' - success'
			print @debugMessage + ' ' + convert(varchar,getdate(),114)
			exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage
			

			select 
				@successCount = @successCount + 1,
				@rowsprocessed = @rowsprocessed + @rowCount

			/* don't need to continue if
					@rowsToProcess is null (ie all rows have been processed)
				 or	@rowsleft = 0  - there are no rows left to process anyway (probably could only happen if there was an error in the very last record)
				 or	@rowcount = @rowsLeft the number actually processed was the same as the number left to process*/

			if @rowsToProcess is null or @rowsLeft = 0 or @rowcount = @rowsLeft
				select @continue = 0
			
			/*  After two successful runs then we can double the number of rows processed in the next batch 
				will have no effect if @rowsToProcess is null, but that is fine because in that case we have had no errors and are not going to be looping anyway
				Can't double after a single success because we might still be working down looking for an error
				Sometimes errors disappear when the number of rows is reduced.  If we are looking for an error and we get two successful runs in succession then we can deduce this has happened and can happily carry on
			*/
			
			IF  @successCount >= 2 
				set @rowsToProcess = @rowsToProcess * 2
			

			/* If we are not on the last iteration then we can put in a delay to help prevent locking */
			IF @continue = 1 and @waitBetweenBatchesSeconds > 0 and @rowsToProcess >= @maxBatchSize
				WAITFOR DELAY @waitString 
			
	END TRY


	BEGIN CATCH
		/* we want to count the number of iterations, but only want to count each error once, not multiple times as we halve the number of rows processed, 
			so only count after a success (which also logging an error on a single row)*/
		IF @successCount > 0 
			set @numberOfIterations = @numberOfIterations + 1

		IF @rowsToProcess = 1 OR @rowsLeft = 1   
		BEGIN

			/* 	If @rowsToProcess = 1 then we can identify which record has caused the problem, record the problem and move on
				WAB 2015-01-15 If there is only 1 row in the incoming table and an error occurs then @rowsToProcess will be null (ALL), so we also need to test for @rowsLeft = 1
			*/

 			set @debugMessage =  'Process 1 row (' + convert(varchar,@rowsprocessed +1) +')- Failed.  Error recorded: ' + error_message()
			print @debugMessage + ' ' + convert(varchar,getdate(),114)
			exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage = @debugMessage

			/* debug log id of failed item */
			select @sqlCMD = 
					'insert into ' + @debugTableName + ' (message) 
					select top 1 ''ID: '' + convert(varchar,upsertuniqueid) 
				from '
			+ @upsertTableName +
			' where upsertUniqueID not in (select upsertUniqueID from #itemsdone)'
			exec (@sqlcmd)


			select @sqlCMD = 
				'insert into #itemsdone (action , entityid , upsertuniqueid,message )
					select top 1 ''FAILED'',' + @entityIDColumn + ', upsertuniqueid, error_message()
				from '
			+ @upsertTableName +
			' where upsertUniqueID not in (select upsertUniqueID from #itemsdone)'
			exec (@sqlcmd)

			/*   
				the procedure will now continue by processing a single row, once that is successful then then next run will do 2 rows and so on
				alternative strategy is to go back and try processing all rows again
				Which is best depends on the likely number of failures, but if there are likely to be multiple failures then we are better off building up slowly
				-- select @rowsToProcess = null  
			*/
			select 
				@rowsprocessed = @rowsprocessed + 1,
				@successCount = 1  /* this may seem slightly odd to reset @successCount after recording an error, but it allows us to speed up more quickly.  
									If left at 0 then we will do two lots of single row updates before starting to double
									It is also used to decide when to increment @numberOfIterations
									 */

		END
		ELSE
		BEGIN

			/* @rowsToProcess <> 1, therefore we need to continue halving the number of rows processed */
 			set @debugMessage =  'Process ' + isNull(convert(varchar,@rowsToProcess),'All') + ' rows starting at row ' +  convert(varchar,@rowsprocessed +1) + ' - failed. ' + error_message()
			print @debugMessage + ' ' + convert(varchar,getdate(),114)
			exec sp_executeSQL @debugSQL, N'@debugMessage nvarchar(max)',@debugMessage

			insert into #errorMessages (message, startRow, NumberOfRows)
			values (error_message(),@rowsprocessed +1,@rowsToProcess)

			select 
				@rowsToProcess = round((isnull(@rowsToProcess,@rowsLeft) +1 )/2,0),
				@successCount = 0

			IF @rowsLeft = 0
			BEGIN
				SET @continue = 0	
			END

		END
		
		/* 	special check for an error on the very last row, in which case do not continue 
			WAB 2015-01-14 - moved this test and made it <=1 so that catches case of no rows in the incoming table and the insert errored which ended up in infinite loop with @rowsToProcess = 0
					It is a bit odd a) processing zero rows, and b) an error occurring when now rows are being processed - but it did happen!	  
		*/ 
		IF @rowsLeft <= 1
			SET @continue = 0


		IF @numberOfIterations  >= @maxIterations
		BEGIN
			
			select @sqlCMD = 
				'INSERT INTO
					#itemsdone 
					(action , entityid , upsertuniqueid, Message)
			SELECT  DISTINCT
					''NOTPROCESSED'' , ' + @entityIDColumn + ' , upsertuniqueid, ''TooManyErrorsInBatch''
			FROM	 '
			+ @upsertTableName +
			' where upsertuniqueid not in (select upsertuniqueid from #itemsdone)'
			exec (@sqlcmd)
		
		
			SET @continue = 0
		END
		
		
	END CATCH		

END

/*
Now generate the @AdditionalErrorMessages return value
*/
/*	First delete any error messages which actually appear in the #itemsDone.message column 
	We are then left with any 'unlogged' errors which we are going to return in a string
*/
delete 
#errorMessages
from 
	#errorMessages e
		inner join
	#itemsDone i on e.message = i.message


IF exists (select 1 from #errorMessages)
BEGIN
	IF @AdditionalErrorMessages is Null
		set @AdditionalErrorMessages = ''

	/*	Return each distinct error occurrence 
		Namely one error for each 'startrow'.  We take the message generated with the smallest numberOfRows, 
		which should allow easier pinpointing of the problem rows
	*/
	
	/*	I need to join back to the upsert table by row number, 
		I had problems with my query concatentating when using row_number() so I am creating a temporary table 
		Which is good actually, because I need to join twice
	*/	
	create table #upsertRowIDs (upsertUniqueID varchar (40), entityid int, rowNumber int)
	select @sqlCMD = '
		insert into #upsertRowIDs (upsertUniqueID , entityid , rowNumber )
		select upsertUniqueID  ,' + @entityIDColumn + ' , row_number() over (order by (select null)) as rownumber
		from ' + @upsertTableName 

	exec (@sqlcmd)

	select @sqlcmd = '
		select 
			@AdditionalErrorMessages += char(10) + 
				''Rows '' + convert(varchar, e.startRow) + ''-'' + convert(varchar, e.startRow + e.numberOfRows -1) + ''. UniqueIDs: ''
				+ convert(varchar, startItem.upsertUniqueID ) + '' & '' + convert(varchar, endItem.upsertUniqueID )  + 
				'', EntityIDs: '' + convert(varchar,startItem.entityID) + '' & '' + convert(varchar,endItem.entityID) + ''. ''
				+ message
		 from #errorMessages e
					inner join 
				(select 
					startRow, min(numberOfRows) as numberOfRows
					from #errorMessages 
					group by startRow
				) as distinctErrors  on e.startRow = distinctErrors.startRow and  e.numberOfRows = distinctErrors.numberOfRows 
					inner join
			#upsertRowIDs startItem on startItem.rownumber = e.startrow 	
					inner join
			#upsertRowIDs endItem on endItem.rownumber = e.startrow + e.numberOfRows -1	
		 order by e.startrow asc, e.numberOfRows desc
	'

	exec sp_executeSQL @sqlcmd, N'@AdditionalErrorMessages nVarchar(max) OUTPUT', @AdditionalErrorMessages = @AdditionalErrorMessages OUTPUT

END


/* Either select from #itemsDone or put items into the return table*/
	declare @returnTableExists bit = 0
	IF 	@returnTableName <> ''
	BEGIN
		IF object_id('tempdb..' + @returnTableName ) is not null  OR object_id(@returnTableName ) is not null
			SET @returnTableExists = 1
	END


	IF @returnTableName <> ''
	BEGIN
		set @sqlCMD = ' select * from #itemsdone'

		IF @returnTableExists = 0
		BEGIN
			SELECT @sqlcmd = replace(@sqlcmd,' from ', ' into ' + @returnTableName + ' from ')
		END	
		ELSE	
		BEGIN
			SELECT @sqlcmd = ' insert into ' + @returnTableName  + @sqlcmd
		END	
		
		exec (@sqlCMD)
	END
	ELSE
	BEGIN
		select * from #itemsdone
	END		

		drop table #itemsdone	

	/* drop debug table */
	SET @sqlCmd = 'drop table ' + @debugTableName 
	exec sp_executeSQL @sqlCmd


	/* if we have created a temporary table (either because incoming object is a view or we have an order by clause) then delete it */
	IF @tempTableName is not null   
	BEGIN
		SET @sqlCmd = 'drop table ' + @tempTableName 
		exec sp_executeSQL @sqlCmd
	END
	



THEEND:		