IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'CloneElementBranch' and Type = 'P')
	DROP PROCEDURE CloneElementBranch
	
GO
	
CREATE  PROCEDURE [dbo].[CloneElementBranch]
      @ElementID int,                     -- ElementID of the Node at the top of the Branch
      @PersonID int,                      -- ID of user for which to retrieve elements. 		                                   -- use #ListFirst(Cookie.USER,"-")# when calling from ColdFusion
      @targetElementID int,  -- NJH 2006/10/25
	@includeTopElement bit = 1,     -- when set to 0 the children of this node get copied and made children of the target node
	@copyRecordRights bit = 1,		
	@copyCountryScopes bit = 1,
	@copyPhrases bit = 1,
	@numberOfGenerations int= 99,

      @FNLErrorParam nVarchar (1000) output 	    -- OUTPUT Parm for bad arguments

 
AS
 

/*

WAB 2007-10-15 some rejigging done.  
	Made sure that countries and rights copies properly. 
	Got rid of some temporary tables
	Changed way new IDs were allocated (ie don't use identityInsert on, but do the inserts one by one get the inserted values and save in the temporary table)
	I note that it will only copy content which the user has rights to SEE - which doesn't make sense!   Query needs re-done!  Now done 2007-10-17

	Added extra parameters @includeTopElement @copyRecordRights @copyCountryScopes @copyPhrases @numberOfGenerations

WAB 2007-11-28  rejigged the copying of phrases because old code truncated anything over 4000 characters (was storing nText data in an nVarchar temporary variable), notw does a direct copy ina single query
NJH 2009/11/05	LID 2289 - set the createdBy and lastUpdatedBy fields for new element using the personID that is passed in
WAB 2010/01/12 LID 2288 hasRecordRights and hasCountryRights fields needed to be set to 0 if rights not being copied
WAB 2015-11-19 Convert to use hasRecordRightsBitMask
*/
 
 
 
      DECLARE @Depth int                        -- Filter on Branch Depth Optional - Defaulting to show full depth
      SET @Depth = 0
 
 
/*-----------------------------------------------------------------------------------*/
/* Initialisations                                                                   */
/*-----------------------------------------------------------------------------------*/
 
/* These are used to make the copy of the Element Tree */
 
DECLARE @ChildrenLeft int           -- 
DECLARE @CountBefore int              -- Used to determine when there are no children left for the branch
DECLARE @CountAfter int       --
DECLARE @gendepth int         -- Used to contain the current generation depth from the given sibling to the TopNode
      SET @gendepth = 1
DECLARE @EntityTypeID int
DECLARE @NewElementID int
DECLARE @Child int
DECLARE @id int
declare @rc int           -- stores row count of insert query 
	set @rc =1
DECLARE @rootElementID int   -- NJH 2006/10/25
declare @insertedIdentity int
DECLARE @userGroupID int -- NJH 2009/11/05 LID 2289
 
/* These are used to make the copy of the Phrases */
 
DECLARE @NewPhraseListID int
DECLARE @NewPhraseID int
 
DECLARE @PL_PhraseID int
DECLARE @PL_EntityTypeID int 
DECLARE @PL_EntityID int
DECLARE @PL_PhraseTextID nvarchar(1000)
 
DECLARE @P_EntityID int
DECLARE @P_PhraseID int
DECLARE @P_PhraseTextID nvarchar(1000)
DECLARE @P_CountryID int
DECLARE @P_LanguageID int
DECLARE @P_PhraseText nvarchar(4000)
 
 
/* These are used for error handlinge */
DECLARE @FNLErrorCode int           -- Integer return code
SET   @FNLErrorCode = 0
 
/*-----------------------------------------------------------------------------------*/
/* Check validity of input parameters                                                */
/*-----------------------------------------------------------------------------------*/
IF NOT EXISTS(SELECT 1 FROM Element WHERE ID = @ElementID) and @ElementID <> 0
 BEGIN
   SET @FNLErrorCode = 2000   -- Error number for invalid ElementID
 
   SET @FNLErrorParam = convert(nvarchar, @ElementID)
   GOTO ExitPoint
 END
 
 
--- NJH 2006/10/25
IF NOT EXISTS(SELECT 1 FROM Element WHERE ID = @targetElementID) and @targetElementID <> 0
 BEGIN
   SET @FNLErrorCode = 2000   -- Error number for invalid ElementID
 

   SET @FNLErrorParam = convert(nvarchar, @targetElementID)
   GOTO ExitPoint
 END
 
IF NOT EXISTS(SELECT 1 FROM Person WHERE PersonID = @PersonID) AND @PersonID > 0
 BEGIN
   SET @FNLErrorCode = 1001   -- Error number for invalid Person ID
   SET @FNLErrorParam = convert(nvarchar,@PersonID)
   GOTO ExitPoint
 END

-- NJH 2009/11/05 LID 2289 - get userGroupID for person
SELECT @userGroupID = isNull(userGroupID,0) from userGroup where personID = @PersonID
IF (@userGroupID = 0)
	BEGIN
		SET @FNLErrorCode = 1001   -- Error number for invalid Person ID (ie. must be internal)
		SET @FNLErrorParam = convert(nvarchar,@PersonID)
		GOTO ExitPoint
	END
 
 
 
/*-----------------------------------------------------------------------------------*/
/* Create temporary tables                                             */
/*-----------------------------------------------------------------------------------*/
 
/* Create a table to contain all the element IDs in the branch */
 
CREATE TABLE #temp1
      (
      id int,
      gendepth int,
      NewElementID int
      )
 
 
/*-----------------------------------------------------------------------------------*/
/* Copy the Element Tree                                           */
/*-----------------------------------------------------------------------------------*/
 
 
 
 
/* Put the first Node into the table */
 
      INSERT INTO 
            #temp1 
            (id, gendepth, NewElementID) 
      VALUES 
            (@ElementID, @gendepth, 0)

/*Initialise the Loop variable */
SET @ChildrenLeft = 1
 
/*-----------------------------------------------------------------------------------
 
      Loop through each generation until we come to one that has no children.
      This is achieved by looping through all elements with a given Parent ID
 
      We can recognise the generation without children by observing when the count of
      records found so far does not increase after an insert.
 
-----------------------------------------------------------------------------------*/
 
WHILE  @rc > 0 AND @gendepth < @numberOfGenerations 
      begin
      
      SET @CountBefore = (SELECT count(ID) FROM #temp1)
      SET @gendepth = @gendepth + 1
 
 --      DECLARE Element_Cursor CURSOR FOR
          INSERT INTO #temp1   (
                              id,
                              gendepth,
                               NewElementID)
      SELECT DISTINCT
            Child.id, @gendepth, 0
      FROM 
		#temp1 e
			inner join 
		Element AS Child ON Child.parentid = e.ID



	WHERE
			e.gendepth=@gendepth - 1
		and 	child.statusid <> 5 

		select @rc = @@rowcount	
		
/*
WAB removed, ignore country and view rights doesn't really make sense, although some could be reintroduced, but if so copy code from getElementBranch
              (SELECT recordid, 
                  usergroupid 
             FROM       recordrights 
             WHERE      entity = 'ELEMENT') AS rr 
      
              RIGHT JOIN Element AS Child
              ON rr.recordid = Child.id
      
              LEFT JOIN UserGroup 
                  ON rr.usergroupid = UserGroup.UserGroupID
----------------------------------------------------------------------- line 200
            LEFT JOIN rightsgroup 
            ON UserGroup.UserGroupID = rightsgroup.UserGroupID
            INNER JOIN (SELECT ID FROM #temp1 where gendepth=@gendepth - 1) e 
            ON Child.parentid = e.ID
      WHERE       --120
            (                             
                  Child.islive=1 
                  AND rr.usergroupid Is Null
            ) 
            OR
            (                             
                  Child.islive=1 
                  AND rightsgroup.PersonID=@personid
            ) 
      
 
 
      SET @CountAfter = (SELECT count(ID) FROM #temp1)
      if (@Depth > 0) AND (@Depth = @gendepth)
            begin
            SET @ChildrenLeft = 0
            end
      else
            begin
            SET @ChildrenLeft = @CountAfter-@CountBefore
            end -----------------------------------------------------------------line 250
 
*/


 end
 



/*  
WAB If we are not copying the top element then delete it from the temporary table
*/
IF @includeTopElement = 0 
	BEGIN
		delete from #temp1 where gendepth = 1
	END








/* 
Loop over the temporary table inserting the elements one at a time and then updating the new id in the temp table
*/

      DECLARE InsertElement_cursor CURSOR local FOR
      SELECT 
            id
	from #temp1
	order by genDepth

      OPEN InsertElement_cursor
 
      FETCH NEXT FROM InsertElement_cursor
            INTO @id
 
      WHILE @@FETCH_STATUS = 0
      BEGIN

/* insert elements*/
INSERT INTO Element
(
      ParentID,
      SortOrder,
      Headline,
      elementTextID,
      Date,
      StatusID,
      Summary,
      URL,
      ElementTypeID,
      Keywords,
      isExternalFile,
      isLive,
      LongSummary,
      Detail,
      Image,
      ExpiresDate,
      includeScreen,
      SummaryInContents,
      rate,
      hasCountryScope,
      parameters,
      hasRecordRightsBitMask,
      showonmenu,
      LoginRequired,
      menuimage,
      GetContentFromID,
      stylesheets,
      showSummary,
      showLongSummary,
      hideChildrenFromMenu,
      showonSecondaryNav,
	  createdBy,
	  lastUpdatedBy
)


SELECT 
      CASE WHEN tt.NewElementID IS NULL THEN @targetElementID ELSE tt.NewElementID END as ParentID,
      Element.SortOrder,
      CASE WHEN tt.NewElementID IS NULL THEN 'Clone Of ' + Element.Headline ELSE Element.Headline END,
      CASE WHEN Rtrim(isNull(Element.elementTextID,'')) = '' THEN Element.elementTextID ELSE 'Clone Of' + Element.elementTextID END as elementTextID,
      Element.Date, 
      Element.StatusID,
      Element.Summary, 
      Element.URL, 
      Element.ElementTypeID, 
      Element.Keywords, 
      Element.isExternalFile,
      Element.isLive, 
      Element.LongSummary,
      Element.Detail,
      Element.Image,
      Element.ExpiresDate,
      Element.includeScreen,
      Element.SummaryInContents,
      Element.rate,
	/* WAB 2010/01/12 LID 2288
	if record/country rights are not copied then set the hasRecord/CountryRights field to 0, otherwise just get original value
	*/ 		
      case when @copyCountryScopes = 1 then Element.hasCountryScope else 0 end,
      Element.parameters,
      case when @copyRecordRights = 1 then Element.hasRecordRightsBitMask else 0 end,
      Element.showonmenu,
      Element.LoginRequired,
      Element.menuimage,
      Element.GetContentFromID,
      Element.stylesheets,
      Element.showSummary,
      Element.showLongSummary,
      Element.hideChildrenFromMenu,
      Element.showonSecondaryNav,
	  @userGroupID,
	  @userGroupID

FROM #temp1 t INNER JOIN Element ON Element.ID=t.id
      Left JOIN #temp1 tt on element.parentid = tt.id
 where
	t.id = @id
 
	set @newElementID = SCOPE_IDENTITY ()

	update #temp1 set newElementID = @newElementID where id = @id
	print 'New/Old ElementID ' + convert(varchar,@newElementID) + '  ' +  convert(varchar,@id)


      FETCH NEXT FROM InsertElement_cursor
            INTO @id
END

close InsertElement_cursor
deallocate InsertElement_cursor


select @rootElementID = newElementID from #temp1 where gendepth = 1


IF @copyPhrases = 1 
BEGIN



/*-----------------------------------------------------------------------------------*/
/* Copy the Phrases                                           */
/*-----------------------------------------------------------------------------------*/
 
 
/*                Copy the phraselists */
 
	-- copy all the ids of the phrases which need copying into a temp table
          select        PhraseID as originalPhraseID,e.id as originalEntityID, e.newElementID as newEntityID, 0 as newPhraseID
		into #tempPhraseID 
            from #Temp1 e
                  join  phraselist pl on e.ID = pl.EntityID
                  and pl.EntityTypeID = 10

      -- Declare a cursor for copying the records with new IDs
      DECLARE PhraseList_Cursor CURSOR local FOR
            SELECT      
                  originalPhraseID
            from #tempPhraseID 

 
      -- Roll through the cursor and copy each row
      OPEN PhraseList_Cursor
 
      FETCH NEXT FROM PhraseList_Cursor
            INTO @PL_PhraseID
 
      WHILE @@FETCH_STATUS = 0
      BEGIN
            INSERT INTO PhraseList    
			(
                        PhraseID,
                        EntityTypeID,
                        EntityID,
                        PhraseTextID
            ) 

	select 		
		(select max(phraseID)+ 1 from phraseList),
                        pl.entityTypeID,
			tp.newEntityID,                        
                        pl.PhraseTextID
            from phraseList Pl inner join #tempPhraseID tp on pl.phraseid = tp.originalPhraseID
		where pl.phraseid = @PL_PhraseID


		set @insertedIdentity = SCOPE_IDENTITY ()
		select @newPhraseid =	phraseid from phraselist where ident = @insertedIdentity
		update #tempPhraseID
		set newPhraseID = @newPhraseid where originalphraseid = @PL_PhraseID

		print 'New Phrase'
		print @insertedIdentity
		print @newPhraseid
		print @PL_PhraseID


            FETCH NEXT FROM PhraseList_Cursor
                  INTO @PL_PhraseID
      END
 
      CLOSE PhraseList_Cursor
      DEALLOCATE PhraseList_Cursor
 
 
 
/*-----------------------------------------------------------------------------------*/
 
/*                Copy the actual phrases */
 
	insert into phrases
	(phraseid,languageid,countryid,phraseText,defaultForThisCountry)
	select 
	tp.newphraseid,p.languageid,p.countryid,p.phraseText,p.defaultForThisCountry
	from #tempPhraseID tp inner join phrases p on tp.originalPhraseID = p.phraseid
 

END 
 
 
 
SELECT @EntityTypeID = EntityTypeID FROM SchemaTable WHERE EntityName = 'Element'
 

 
/*-----------------------------------------------------------------------------------*/
/* Update The Database                                                                                                       */
/*-----------------------------------------------------------------------------------*/
 
 
/* Prepare   */
 
DECLARE @Insert_Copies bit
SET @Insert_Copies = 1
IF @Insert_Copies = 1 --Provides a switch to control whether to inset the copies or just look at them for debugging
BEGIN
 
DECLARE @T_ERROR int
SET @T_ERROR = @@ERROR
IF @T_ERROR > 0 
      BEGIN
            SET @FNLErrorCode = 3000      -- Error occurred before Insert
 
      SET @FNLErrorParam = 'Error occurred before Insert - Error Code = ' + convert(varchar(20),@@ERROR)
            GOTO Exit_Rollback
      END
 
 
BEGIN TRANSACTION
 
-- NJH 2006/10/25
IF @targetElementID <> 0
      update 
  element 
 set 
  -- parentID = @targetElementID,  now done elsewhere
  statusid = 0      -- WAB 2007-10-10 make top element draft so that does not show immediately
  where 
  ID = @rootElementID
 



SET IDENTITY_INSERT Element OFF
IF @@ERROR > @T_ERROR
      BEGIN
            SET @FNLErrorCode = 3001      -- Error occurred while inserting Elements copy
 
      SET @FNLErrorParam = 'Element Insert Failed - Error Code = ' + convert(varchar(20),@@ERROR)
            GOTO Exit_Rollback
      END
 


 



IF @copyRecordRights = 1 
BEGIN 
/*-----------------------------------------------------------------------------------*/
/*                RecordRights */
 
--
 
INSERT INTO 
      RecordRights 
(
      recordid,
      entity,
      usergroupid,
 permission
)

/*
WAB 2007-10-15 removed, no need to go through the #newRights table
SELECT 
      NewElementID,

      usergroupid,
 permission
FROM 
      #NewRights
*/

SELECT 		
	  NewElementID,
	      entity,
           usergroupid,
	  permission
             FROM       #temp1 t inner join recordrights r   
                              on t.id = r.recordid 
             WHERE      entity = 'ELEMENT'
END 


IF @copyCountryScopes = 1 
BEGIN
/*-----------------------------------------------------------------------------------
                Country Scope 
-----------------------------------------------------------------------------------*/
INSERT INTO 
      CountryScope
(
      entityid,
      entity,
      entityTypeID,
      countryid,
 	permission
)


SELECT 		
	  NewElementID,
      entity,
      entityTypeID,
           countryid,
	  permission
             FROM       #temp1 t inner join CountryScope cs
                              on t.id = cs.entityid 
             WHERE      entity = 'ELEMENT'




 
IF @@ERROR > @T_ERROR
      BEGIN
            SET @FNLErrorCode = 3005      -- Error occurred while inserting CountryScope copy
 
      SET @FNLErrorParam = 'CountryScope Insert Failed - Error Code = ' + convert(varchar(20),@@ERROR)
            GOTO Exit_Rollback
      END
 
END


select * from #temp1



/*-----------------------------------------------------------------------------------*/
 
/*                Commit the Insert Transaction */
 
COMMIT TRANSACTION
GOTO ExitPoint
/*-----------------------------------------------------------------------------------*/
 
/*                Rollback the Insert Transaction */
 
Exit_Rollback:
 
ROLLBACK TRANSACTION
GOTO ExitPoint
 
END
 
 
ExitPoint:
PRINT convert(varchar(255),@FNLErrorCode)
PRINT @FNLErrorParam
 
--RETURN @FNLErrorCode
 
      DROP TABLE #temp1
      DROP TABLE #tempPhraseID
GO
/* LID 2288/2289 end */