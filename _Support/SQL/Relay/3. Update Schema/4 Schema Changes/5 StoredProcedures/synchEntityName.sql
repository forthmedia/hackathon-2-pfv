/* 
	NJH 2016
	This SP will keep the entityName table in synch with the data that contributes towards it.  
	Only run on an occasional basis (such as after upgrades) - live synching done in modRegisterTiggerAction

	WAB 2016-06-06	Had problem when table had no real name field and the identity column was used for the name expression.
					We were trying to fire off a dummy update to get modregister to do the synching (and ended up trying to update an identity field)
					Have changed so that does not use Modregister.  Which Nat and I think is what the intention was (after initially planning to re-use the modRegister functionality)
	NJH	2016/08/01	JIRA PROD2016-1507. If object has 'deleted' column, then check that to find out if object is deleted rather than del table (such as product)
	WAB 2016-12-27  Procedure failing if entity did not have a CRMID  (due to @crmIDCol being null)
	WAB 2017-01-13	Alter so can do a complete refresh (useful if name expression changes - usually from blank)
*/

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'synchEntityName' and Type = 'P')
	DROP PROCEDURE synchEntityName
GO

CREATE procedure [dbo].[synchEntityName]
@tablename sysname = null,
@refresh bit = 0
AS
	set nocount on
	
	declare @table sysname, @nameExpressionCol sysname, @crmIDCol sysname, @uniquekeyColumn sysname
	declare @sqlcmd nvarchar(max)
	declare @entityTypeID char(5)
	declare @hasDeletedCol bit
	
	declare tablenameCursor cursor for 
		select s.entityName as tablename,
			isNull(c.column_name,'') as crmCol,
			isNull(s.nameExpression,'') as nameExpression,
			s.uniqueKey,
			s.entityTypeId,
			case when dc.column_name is not null then 1 else 0 end as hasDeletedCol
		from schemaTable s 
			left join dbo.CsvToTable(@tablename) t on s.entityName = t.value
			left join information_schema.columns c on c.table_name = s.entityName and c.column_name like 'crm%ID'
			left join information_schema.columns dc on dc.table_name = s.entityName and dc.column_name = 'deleted'
		where s.in_vEntityName=1 and (t.value is not null or @tablename is null)
			and s.entityName not like '%del'
		
	open tablenameCursor
	fetch next from tablenameCursor into @table,@crmIDCol,@nameExpressionCol,@uniquekeyColumn,@entityTypeID,@hasDeletedCol
	
	while @@fetch_status = 0
	begin
		Print 'Synching : ' + @table

			if @refresh =1
			BEGIN
				set @sqlcmd = 'Delete from entityName where entityTypeID='+@entityTypeID
				exec(@sqlcmd)
			END	
			
			if exists (select 1 from information_schema.columns where table_name = @table+'del')
			begin
				/* update entityname from del table */
				set @sqlcmd = 'update entityName 
					set name = '+case when @nameExpressionCol = '' then 'null' else 'd.'+@nameExpressionCol end+',
						crmID = null,
						deleted = 1
					from entityName e 
						inner join '+@table+'del d on d.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+@entityTypeID+'
					where e.name != '+case when @nameExpressionCol = '' then 'null' else 'd.'+@nameExpressionCol end+'
						or e.crmID is not null'
				exec(@sqlcmd)
					
				/* select crmID as null when populating entityName from deleted table */
				set @sqlcmd = 'insert into entityName (entityId,entityTypeID,name,crmID,deleted)
				select distinct d.'+@uniquekeyColumn+', '+@entityTypeID +','+ case when @nameExpressionCol = '' then 'null' else 'd.'+@nameExpressionCol end+',
					null,1
					from '+@table+'del d
						left join entityName e on d.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+@entityTypeID+'
					where e.entityID is null'
				exec(@sqlcmd)
			end
			
			/* update entityName from base table */
			set @sqlcmd= 'update entityName 
							set name = '+case when @nameExpressionCol = '' then 'null' else 't.'+@nameExpressionCol end+',
							crmID = '+case when @crmIDCol = '' then 'null' else 't.'+@crmIDCol end+',
						deleted = '+ case when @hasDeletedCol=1 then ' t.deleted' else '0' end +'
				from entityName e 
						join 
					 ' + @table+' t on t.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+@entityTypeID+'
				where
					e.deleted = 1 ' -- This would pick up an item which had been undeleted and not updated in entityName (OK fairly unlikely but I needed something before my ORs)
			if (@nameExpressionCol != '')
				set @sqlcmd = @sqlcmd + ' or isNull(e.name,'''') != isNull(t.'+@nameExpressionCol+','''')'
			if (@crmIDCol != '')
				set @sqlcmd = @sqlcmd + ' or isNull(e.crmID,'''') != isNull(t.'+@crmIDCol+','''')'

			exec(@sqlcmd)
			
			/* new data into entityName from base table */
			set @sqlcmd = 'insert into entityName (entityId,entityTypeID,name,crmID,deleted)
			select distinct i.'+@uniquekeyColumn+', '+@entityTypeID +','+ case when @nameExpressionCol = '' then 'null' else 'i.'+@nameExpressionCol end+','+
				case when @crmIDCol = '' then 'null' else 'i.'+@crmIDCol end+','+case when @hasDeletedCol=1 then ' i.deleted' else '0' end+'
				from '+@table+' i
					left join entityName e on i.'+@uniquekeyColumn+' = e.entityID and e.entityTypeID='+@entityTypeID+'
				where e.entityID is null'

			exec(@sqlcmd)

		fetch next from tablenameCursor into @table,@crmIDCol,@nameExpressionCol,@uniquekeyColumn,@entityTypeID,@hasDeletedCol
	end
	
	close tablenameCursor
	deallocate tablenameCursor
	
return


