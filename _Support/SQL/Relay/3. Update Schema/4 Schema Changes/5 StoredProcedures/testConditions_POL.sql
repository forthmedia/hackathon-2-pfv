createPlaceHolderObject @objectName = 'testConditions_POL', @objectType = 'procedure'

GO

/*
WAB 2015-11-03
Purpose:
Evaluates conditions expressed as SQL WHERE statements against a Person's Person/Location/Organisation record.
Original use is for allocating people to visibility groups based on a SQL expression.

Parameters
@personid int - the ID of the person to test
#tempCondition - a temporary table (
					condition nvarchar(max), 
					conditionResult bit)
				contains all the conditions to be evaluated and a column to receive a boolean result (or null if the SQL fails)


Desgin:
Dynamic SQL is used to build a query including the WHERE clause (note that there is an inherent SQL injection risk here)
The query uses views which include every single flag on the POL tables.  These views (which involve a PIVOT) are quite efficient for a single record - which is all we need
Original idea was to have a procedure which evaluated a single condition and return a true/false answer.  
However when evaluating multiple conditions, speed becomes an issue and I found that the views weren't so efficient when joined together.
Therefore I moved to a model of 
a) reading the POL records into 3 temporary tables before joining them together and applying the where statement
b) passing lots of conditions in using a temporary table and updating a bit column on that table with the result

*/


alter procedure testConditions_POL @personid int
AS

BEGIN

	SET NOCOUNT ON
	
	declare 
		@sqlCMD  nvarchar(max)
		, @condition nvarchar(max)
		, @result bit
		, @locationid int
		, @organisationid int
		, @errorid int

	/* select the POL records into temp tables */
	
	select @locationid = locationid,  @organisationid = organisationid from person where personid = @personid
	select * into #person from vPerson where personid = @personid
	select * into #location from vLocation where locationid = @locationid
	select * into #organisation from vorganisation where organisationid = @organisationid

	/* make a cursor of all the conditions and loop through them, updating the ConditionResult column as we go */
	declare entity_cursor cursor for
    select 
		distinct condition
	from 
		#tempCondition

	
	open entity_cursor
	fetch next from entity_cursor into @condition
	
		while @@FETCH_STATUS = 0
		begin

			set @sqlCMD = '		select @result = count(1) 
						from 
							#person person 
								inner join 
							#location location on location.locationid = person.locationid 
								inner join 
							#organisation organisation on organisation.organisationid = person.organisationid 
						where 
							
						 (' + @condition + ')'
							
				BEGIN TRY
					exec @errorid = sp_executeSQL @sqlCMD, N'@personid int, @result int OUTPUT', @personid = @personid, @result = @result OUTPUT


					IF @errorid = 0
					BEGIN
						update #tempCondition set ConditionResult = @result where condition = @condition
					END	 

				END TRY
				BEGIN CATCH
					if exists (select 1 from tempdb..syscolumns where id = object_id('tempdb..#tempCondition') and name = 'validateMessage')
					begin
						declare @errorMessage nvarchar(max) = error_message()
						set @sqlCMD = 'update #tempCondition set validateMessage = @errorMessage'
						exec sp_executeSQL @sqlcmd, N'@errorMessage nvarchar(max)', @errorMessage = @errorMessage
					end
					
				END CATCH

			fetch next from entity_cursor into @condition
		end
		
	close entity_cursor
	deallocate entity_cursor

		

	
	END


GO



