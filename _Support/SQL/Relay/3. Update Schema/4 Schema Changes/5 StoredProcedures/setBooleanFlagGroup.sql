if exists (select 1 from sysobjects where name = 'setBooleanFlagGroup')
BEGIN
drop procedure setBooleanFlagGroup
END

GO
CREATE  Procedure [dbo].[setBooleanFlagGroup]
	@tablename varchar(100),
	@columnname varchar(100) = 'data',
	@flagGroupId int,
	@userGroupID int,
	@personId int
AS
declare @flagType varchar(10)
declare @sql nvarchar(4000)
declare @whereClause nvarchar(4000)
declare @sqlerror int

set @sql = N'select @flagType=ft.name from flagType ft inner join flagGroup fg
	on ft.flagTypeId = fg.flagTypeID
	where fg.flagGroupID = '+cast(@flagGroupId as varchar)
exec sp_executesql @statement=@sql,@params=N'@flagType varchar(10) output',@flagType=@flagType output


/* if it's a radio flag, then check that we're only setting one value. Throw an error if trying to set two flags */
if (@flagType = 'Radio')
begin

	set @sql = N'declare @radioFlagCount table (entityId int);
		insert @radioFlagCount select t.entityid from
		flag f inner join '+@tablename+' t on dbo.ListFind('+@columnName+',flagtextid) <> 0 or dbo.ListFind('+@columnName+',name) <> 0 or dbo.ListFind('+@columnName+',convert(varchar,f.flagid)) <> 0
		where f.flaggroupid = '+cast(@flagGroupId as varchar)+'
		group by t.entityid
		having count(1) > 1;
				
		if exists(select 1 from @radioFlagCount)
		begin
			select t.* from @radioFlagCount r inner join '+@tablename+' t on t.entityId = r.entityId
			RAISERROR (''Only one value for a radio flag is allowed.'',16,1)
			return
		end'
		
	exec sp_executesql @statement=@sql
	select @sqlerror = @@error
	if @sqlerror <> 0
		return
end

-- ones to delete
set @whereClause = ' from '+ @tablename+' t
			inner join booleanFlagData bfd on t.entityID = bfd.entityID
			inner join flag f on bfd.flagID = f.flagid and dbo.ListFind('+@columnName+',flagtextid) = 0 and dbo.ListFind('+@columnName+',name) = 0 and dbo.ListFind('+@columnName+',convert(varchar,f.flagid)) = 0
		where flaggroupid = '+cast(@flagGroupId as varchar)+'
			and t.'+@columnName+' is not null'
		
set @sql = 'update booleanFlagData set lastUpdated=getDate(),lastUpdatedBy='+cast(@userGroupID as varchar)+',lastUpdatedByPerson='+cast(@personID as varchar)+@whereClause
exec (@sql)
set @sql = 'delete from booleanFlagData'+@whereClause
exec (@sql)


-- insert new values
set @sql = N'insert into booleanFlagData (entityID,flagid,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
	select t.entityID, f.flagID, getDate(), '+cast(@userGroupID as varchar)+', getDate(), '+cast(@userGroupID as varchar)+','+ cast(@personID as varchar) +'
	from 
		flag f inner join '+ @tablename+' t on dbo.ListFind('+@columnName+',flagtextid) <> 0 or dbo.ListFind('+@columnName+',name) <> 0 or dbo.ListFind('+@columnName+',convert(varchar,f.flagid)) <> 0
				left join booleanFlagData bfd on bfd.flagid = f.flagid and bfd.entityID = t.entityID
		where
			f.flaggroupid = '+cast(@flagGroupId as varchar)+'
			and bfd.FlagID is null
			and t.'+@columnName+' is not null'
exec (@sql)