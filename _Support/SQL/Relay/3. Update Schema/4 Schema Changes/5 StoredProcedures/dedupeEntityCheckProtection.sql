IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'DedupeEntityCheckProtection' and Type = 'P')
	DROP PROCEDURE DedupeEntityCheckProtection

	
GO

CREATE    procedure [dbo].[DedupeEntityCheckProtection]
@EntityTypeID int ,   
@EntityID int
 
 

AS
 
/*
WAB 2009/04/30  Created for CR Lex 588
 Procedure to check whether an entity has any flags with merge protection set
 Works in a simlar way to deleteEntityCheck - ie you can create a temporary table in the calling procedure which gets returned populated, or it will just return a recordset (but this can't be used in an sp)

Currently checks the Protected field and the protectedLinkedEntity parameter
There is an argument that mergeProtection and deleteProtection are actually different [ie deleteProtection should not automatically imply mergeProtection].  If this turns out the be the case then it is possible to add appropriate switches - one is already coded

*/

/****************************************************************************************
**	Modifications
** 
**	Version		Date			Who		Comments
**	-------		-----------		---		----------------------------------------------------------
**	1.00	    2009/04/30		WAB		Original
	1.01		2012/09/05		PJP		Added in 'DEFAULT' to the called function getParameterValue
****************************************************************************************/
 
 
Declare @sql varchar(3000)
Declare @returnRecordSet bit     -- if #NotDeletable table is passed in the assume that results are returned in it, otherwise return the recordset
set @returnRecordSet = 0
 
IF OBJECT_ID('tempdb..#DedupeEntityCheckProtectionResult') IS NULL
 BEGIN 
  create table #DedupeEntityCheckProtectionResult (Type varchar(20) ,dataTable varchar(20),flagGroupID int,flagGroupName varchar(100),flagID int,flagName varchar(100),entityID int,data varchar(2000) )
  set @returnRecordSet = 1
 END
 
 
 
-- check for records that have Merge Protected flags:
DECLARE protected_cursor CURSOR LOCAL FOR
Select 
' Select 
  ''MergeProtected'',
  ''' + ft.datatablefullname + ''',
  fg.flaggroupid,
  fg.name,
  f.flagid,
  f.name,
  fd.Entityid , 
  ' + case when ft.datatablefullname <> 'booleanflagdata' then 'fd.data'  else 'null' end + '
 from 
  flagGroup fg
   inner join
  flag f  on fg.flaggroupid = f.flaggroupid 
   inner join
  ' + ft.datatablefullname + ' fd 
   on fd.flagid = f.flagid 
 Where 
  
   FG.EntityTypeID=' + convert(varchar,@entityTypeID) + '
  and  fg.flagTypeID = ' + convert(varchar,ft.flagTypeID) +'
  and fd.entityid = ' + convert(varchar,@entityid) + ' 
  and 
   -- This is a possible way of doing it if mergeProtection and Protection need to be distinguished (dbo.getParameterValue(f.formattingParameters,''MergeProtected'') =1 or dbo.getParameterValue(fg.formattingParameters,''MergeProtected'') =1)
	f.protected = 1
'
 
From flagType ft
where ft.flagtypeid in (2,3,4,5,6,8)  -- booleans,text,integer,date, integermultiple
 
 
 
OPEN protected_cursor
-- Perform the first fetch.
FETCH NEXT FROM protected_cursor into @sql
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
-- This is executed as long as the previous fetch succeeds.
   insert into #DedupeEntityCheckProtectionResult(Type ,dataTable ,flagGroupID , flagGroupName,flagID ,flagName,EntityID ,data  )
 Exec(@sql)
 
 FETCH NEXT FROM protected_cursor into @sql
END
  
CLOSE protected_cursor
DEALLOCATE protected_cursor
 
 
 
-- check for records that have Linked Entity Protected flags:
-- PJP 2012/09/05  Added in 'DEFAULT' to the called function getParameterValue
DECLARE protected_cursor CURSOR LOCAL FOR
Select 
' Select distinct
  ''PrLinkedEntity'',
  ''' + ft.datatablefullname + ''',
  fg.flaggroupid,
  fg.name,
  f.flagid,
  f.name,
  null, -- fd.Entityid , 
  ' + case when ft.datatablefullname <> 'booleanflagdata' then 'fd.data'  else 'null' end + '
 from 
  flagGroup fg
   inner join
  flag f  on fg.flaggroupid = f.flaggroupid 
   inner join
  ' + ft.datatablefullname + ' fd 
   on fd.flagid = f.flagid 
 Where 
  
   linkstoEntityTypeID=' + convert(varchar,@entityTypeID) + '
  and  fg.flagTypeID = ' + convert(varchar,ft.flagTypeID) +'
  and fd.data = ' + convert(varchar,@entityid) + ' 
  and 
   (dbo.getParameterValue(f.formattingParameters,''ProtectedLinkedEntity'',DEFAULT) =1 )
'
 
From flagType ft
where ft.flagtypeid in (6,8)  -- booleans,text,integer,date, integermultiple
 
 
 
OPEN protected_cursor
-- Perform the first fetch.
FETCH NEXT FROM protected_cursor into @sql
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
 
print @sql
-- This is executed as long as the previous fetch succeeds.
   insert into #DedupeEntityCheckProtectionResult(Type ,dataTable ,flagGroupID , flagGroupName,flagID ,flagName,EntityID ,data  )
 Exec(@sql)
 
 FETCH NEXT FROM protected_cursor into @sql
END
  
CLOSE protected_cursor
DEALLOCATE protected_cursor
 
 
 

if @returnRecordSet = 1
 select * from #DedupeEntityCheckProtectionresult
 
