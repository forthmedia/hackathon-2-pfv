if exists (select 1 from sysobjects where name = 'updatePhrasePointerCacheForEntityType')
BEGIN
drop procedure updatePhrasePointerCacheForEntityType
END

GO
CREATE  Procedure [dbo].[updatePhrasePointerCacheForEntityType]
		@entityType                 varchar(300) ,
		@phraseTextID               varchar(50) ,
--		@entityTypeFilter           varchar(300) = '' ,
		@CountryID          int ,
		@LanguageID         int ,
		@FallBackLanguageid int = 1

AS

/*
WAB 2010/09/13 Updates the phrasePointerCache table for an entity / phraseTextID
WAB 2010/09/28 Bug Fix
WAB 2011/05/10 Altered to use new 'dirty' column on the phrasePointerCache table
WAB 2014/02/20 Altered so that it can deal with groups of status type phrases (such as oppStage_xxxxx) which are stored in the phrasePointerCacheStatus table with a wildcard (such as oppStage[_]%) (the _ has to be escaped since it is a wildcard itself

*/

set nocount on

	declare 
		@sql  nvarchar(1000), 
		@sqlFrom  nvarchar(1000), 
		@entityTypeID int, @rc int, @temp int, 
		@tablename  nvarchar(100),
		@checkUpdateRequired  INT

	IF @entityType = ''   /*  2014/02/20 blank entityType indicates non entity phrase (which are confusingly stored as entityTypeID = 0 in the phraseList Table) */
		BEGIN
			SELECT @entityTypeID = 0, @entityType = ''
		END	
	ELSE
		BEGIN
			/* Get the entityTypeID and UniqueKey column*/
			select 
				@entityTypeID = entityTypeID
			from 
				schemaTable where entityname = @entityType
		END
	
	/* Check in the phrasePointerCacheStatus table to see if cache is up to date for this entityType */
	
	if exists (select 1 from phrasePointerCacheStatus where entityTypeID = @entitytypeID and countryid  = @countryID and languageid =@languageid and phraseTextID like @phraseTextID)
		return

	/* Build the guts of a query to bring back all entities which don't have an entry in the cache table for this language and country combination*/
	SELECT 
		@sqlFrom = ' 
		from phraselist pl
			left join 
				phrasePointerCache  pp on  pl.phraseid = pp.phraseid and pp.countryid =  ' + convert(varchar,@countryID) + ' and pp.languageid = ' + convert(varchar,@languageid)	+ ' 
		where 
			pl.entitytypeid = '  + convert(varchar,@entityTypeID) + ' and pl.phraseTextID like ''' + @phraseTextID + '''    /* 2014/02/20 WAB change = to like */
			and
			(pp.phraseid is null or pp.Dirty =1 )' 

	/* Add on a filter if required 
	removed because probably not needed and can't say that cache is up to date after a filter has been applied
	IF isnull(@entityTypeFilter,'') <> ''
		SELECT @sqlFrom= @sqlFrom + ' AND ' + @entityTypeFilter
	*/

	/* check whether this query returns any rows */
	SELECT @sql = 'select top 1 @checkUpdateRequired =  1 ' +  @sqlFrom
	exec sp_executesql @sql, N'@checkUpdateRequired int OUTPUT ', @checkUpdateRequired OUTPUT

	IF 	@checkUpdateRequired	is  not null
	BEGIN
		/* put all the items which need updating into a temporary table */
		select @tablename = '##_' + replace(newID(),'-','')

		select @sql = 'select pl.phraseTextID as phrasetextid, ' + 
				convert(varchar,@entitytypeid) + ' as entitytypeid, ''' + 
				@entityType + ''' as entitytype, pl.entityid , cast (null as int) as phrase_ident
				into ' +
			@tablename 

		
		+  @sqlFrom

		exec sp_executesql @sql

			 
		exec translatePhrases @tablename = @tablename,@requiredcountry = @countryid, @requiredLanguage = @LanguageID , @returnResults = 0

	exec (N'drop table ' + @tablename)

	END

	/*
	Update the phrasePointerCacheStatus table, bearing in mind that another process might have inserted a record in the mean time
	*/
	 select @sql = 'insert into phrasePointerCacheStatus (entityTypeID,countryid, languageid,phraseTextID ) 
					select ' + convert(varchar,@entitytypeID) + ',' + convert(varchar,@countryID )+ ','  + convert(varchar,@languageid) + ',''' + @phraseTextID + ''' 
					where not exists (select 1 from phrasePointerCacheStatus where entityTypeID = ' + convert(varchar,@entitytypeID) + ' and countryid = ' + convert(varchar,@countryID ) +' and languageid = ' + convert(varchar,@languageid) +' and phraseTextID = ''' + @phraseTextID + ''')'
	exec (@sql)

	
	