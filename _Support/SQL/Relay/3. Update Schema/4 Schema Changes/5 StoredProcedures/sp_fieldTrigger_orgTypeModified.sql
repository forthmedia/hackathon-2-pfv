if exists (select 1 from sysobjects where name='sp_fieldTrigger_orgTypeModified' and xType='P')
drop procedure sp_fieldTrigger_orgTypeModified

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[sp_fieldTrigger_orgTypeModified] 
                @modEntityID int, 
                @modaction varchar(10),
                @triggerInitiatedBy int, 
                @RecordUpdatesAgainstID int,
                @triggerInitiatedByPersonID int = null
AS

/*
2011/05/11 WAB Major Modification to support new temporary Table #modifiedEntityIDs passed by the modRegisterTrigger
				and use setRadioFlag sp with a temporary table

*/

Set nocount on




IF @modaction <> 'OD'  -- not required for organisation delete
BEGIN
/* get the flagids which need inserting for each record and pop into a temporary table */
	 declare @debug int
	 set @debug = 0
	
	 if (@debug =1 ) print convert(varchar, getdate(),109) + ' sp_fieldTrigger_orgTypeModified start'
	
	select distinct fInsert.flagId,entityID
	into #setFlag
		from 
		#modifiedEntityIDs e
		inner join
		organisation o on e.entityid = o.organisationid
	                   inner join 
	     organisationType ot with (noLock) on o.organisationTypeID = ot.organisationTypeID 
	                                                inner join
	                                flag f on  dbo.listfind (maptoflagidlist, f.flagid) = 1
	                                                inner join
	                                flag fInsert on  dbo.listfind (mapToFlagIdList, fInsert.flagid) = 1  and charindex(convert(varchar,fInsert.flagid), mapToFlagIdList) = 1
	    
	
	if (@debug =1 ) print convert(varchar, getdate(),109) + ' sp_fieldTrigger_orgTypeModified flags calculated'
	
		exec setRadioFlag @flagid=null, @entityid=null,@userid = @RecordUpdatesAgainstID,@updatedByPersonid = @triggerInitiatedByPersonID 
	
	
	Set nocount off
	
	if (@debug =1 ) print convert(varchar, getdate(),109) + ' sp_fieldTrigger_orgTypeModified end'
END