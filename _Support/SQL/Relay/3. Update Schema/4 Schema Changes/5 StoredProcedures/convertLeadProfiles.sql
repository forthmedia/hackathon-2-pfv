/*
	NJH
	A stored procedure to move across lead profiles to the appropriate converted entities. It is currently based on naming convention,
	so the lead profiles/attributes must have the same name as the converted entity profiles.
	For example, lead_myProfile would map to opp_myProfile.
*/

if exists (select 1 from sysobjects where name = 'convertLeadProfiles')
BEGIN
	drop procedure convertLeadProfiles
END

GO
CREATE  Procedure [dbo].[convertLeadProfiles]
	@convertedEntityName varchar(20)
AS
	declare @updateSql nvarchar(max) = 'merge into v'+@convertedEntityName+'_update ce using '
	if (@convertedEntityName = 'organisation')
		set @updateSql = @updateSql + '(select l.organisationID,i.* from location l inner join #convertedLeads i on l.locationID = i.convertedLocationID) i on ce.organisationID = i.organisationID'
	else
		set @updateSql = @updateSql + '#convertedLeads i on  ce.'+@convertedEntityName+'ID = i.converted'+@convertedEntityName+'ID'
		
	set @updateSql = @updateSql + ' when matched then update set lastUpdated=getDate(),lastUpdatedBy=i.lastUpdatedBy,lastUpdatedByPerson=i.lastUpdatedByPerson'
	declare @flagUpdate nvarchar(max) = ''
	declare @flagGroupUpdate nvarchar(max) = ''
	declare @profilePrefix char(3) = left(@convertedEntityName,3)
	declare @numColsToUpdate int = 0
	
	select 
		@numColsToUpdate = @numColsToUpdate+1,
		@flagUpdate = ','+entityFlags.flagTextID+' = '+leadFlags.flagTextID,
		@flagGroupUpdate = ','+entityFlags.flagGroupTextID+' = replace('+leadFlags.flagGroupTextID+',''lead_'','''+left(entityFlags.flagGroupTextID,3)+'_'')',
		@updateSql = @updateSql + case when entityFlags.flagTextID like @profilePrefix+'[_]%' then @flagUpdate else case when entityFlags.dataTable='boolean' then @flagGroupUpdate else '' end end
	from (
			/* if getting boolean flags, use the flagGroup as this will contain the profiles in the view */
			select distinct flagGroupTextID,'' as flagTextID,dataType, dataTable
				from 
				vFlagDef fg where  flagGroupTextID like @profilePrefix+'[_]%' and dataTable='boolean'
				and len(flagGroupTextID) > 0
				union
				select distinct flagGroupTextID,flagTextID,dataType, dataTable
				from 
				vFlagDef f  where flagGroupTextID like @profilePrefix+'[_]%' and dataTable!='boolean'
				and len(flagGroupTextID) > 0 and len(flagTextID) > 0
			) entityFlags
		inner join (
			select distinct flagGroupTextID,'' as flagTextID,dataType, dataTable
			from 
			vFlagDef fg where flagGroupTextID like 'lead[_]%' and dataTable='boolean'
			union
			select distinct flagGroupTextID,flagTextID,dataType, dataTable
			from 
			vFlagDef f where flagGroupTextID like 'lead[_]%' and dataTable!='boolean'
		) leadFlags
		 on leadFlags.dataType = entityFlags.dataType
			and entityFlags.flagGroupTextID in (
				replace(leadFlags.flagGroupTextID,'lead_',@profilePrefix+'_')
			)
			and (entityFlags.datatable!='boolean' and entityFlags.flagTextID in (
				replace(leadFlags.flagTextID,'lead_',@profilePrefix+'_')
			) or (entityFlags.datatable ='boolean' and entityFlags.flagTextID = ''))
	where (leadFlags.dataTable != 'boolean' and leadFlags.flagTextId like 'lead[_]%' and entityFlags.flagTextID like @profilePrefix+'[_]%') 
		or leadFlags.dataTable = 'boolean'

		if (@numColsToUpdate > 0)
		begin
			exec (@updateSql+';')
		end