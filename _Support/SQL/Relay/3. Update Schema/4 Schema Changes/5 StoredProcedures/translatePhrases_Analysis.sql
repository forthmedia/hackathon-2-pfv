IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'translatePhrases_Analysis' and Type = 'P')
	DROP PROCEDURE translatePhrases_Analysis
	
GO
	
CREATE    Procedure [dbo].[translatePhrases_Analysis]
		@tableName                  varchar(100) ,
		@doAnalysis		varchar(3) = 'No',
		@logRequests	int = 0,
		@appIdentifier	varchar(100) = '',
		@pageIdentifier	varchar(100) = ''


as
set nocount on


declare @cmd nvarchar(4000)

/*
Version History
1.0

6/9/04 This code was separated from the translatePhrase custom tag so that it 
could be used by itself - particularly when phrases where being stored in memory
WAB 17/01/07   corrected a problem which had existed since split from translatePhrases - entity names were not being updated to entityids.  Need for Resurrection of analysis code.
		also added some joins to language and country table in second query but actually not used.
		doAnalysis could be recoded into a single query.  a QoQ could be used for getting the column headers

LHID8224 2011/11/24	NYB	Control Content->Record Translations was not working:  
						Increased the varchar size of @tableName


*/
	-- update entityTypeIDs from entityType s in the table
	select @cmd = 'update ' + 
		@tableName + ' 
	set entityTypeID = s.entityTypeID 
	from ' +
		@tableName + ' t inner join 
		schemaTable s on t.entityType = s.entityName'

	 exec(@cmd)


IF @logRequests = 1
	BEGIN
		
		select @cmd = '
			insert into phraseRequest   (phrasetextid, entityid, entitytypeid,appidentifier,pageidentifier, created)
			select c.phrasetextid, 
				convert(int,c.entityid), 
				c.entitytypeid, 
				@appIdentifier, 
				@pageIdentifier, 
				getdate()
			from ' + 
				@tableName  + ' as c 
					left outer join 
				phraseRequest pr 
				on   	c.phrasetextid = pr.phrasetextid 
					and  convert(int,c.entityid) = pr.entityid 
					and c.entityTypeID = pr.entityTypeid 
					and @pageIdentifier = pr.pageIdentifier
					and @appIdentifier = pr.appIdentifier
			where 
				pr.phraseRequestid is null
				and isNUmeric(c.entityid) = 1
				'
			
print @cmd

	exec	sp_executesql  @cmd ,  N'@pageIdentifier varchar(100),@appIdentifier varchar(100)',  @pageIdentifier =@pageIdentifier , @appIdentifier =@appIdentifier 




	END

	

IF @doAnalysis = 'Yes'
/*

These queries are very much designed for use with the FNL crossTab tag
First one defines the headings and the second one the data

*/


	BEGIN



		select @cmd = 		
		'select 
			distinct
			p.languageid,
			p.countryid,
			convert(varchar,p.languageid) + ''_'' + convert(varchar,p.countryid) as LanguageAndCountry,
			isNull((select language from language where p.languageid = languageid),''No Language'')  as language,
			 (select isNull(ISOCode,CountryDescription) from country where countryid = p.countryid union select ''Any Country'' where p.countryid = 0)  as country
		from 
			' + @tableName +' as c
			left outer join 	
			vphrases as p
			on p.phrasetextid = c.phrasetextid
			and convert(varchar,p.entityid) = c.entityid
			and p.entityTypeID = c.EntityTypeID	
		where p.languageid is not null
		order by p.languageid, p.countryid'

		exec (@cmd)



		select @cmd = 		
		'select distinct
			c.phrasetextid,
			p.phraseid,
			c.phraseTextid + ''_'' + c.EntityType + ''_'' + convert(varchar,c.entityid)  as rowID,
			c.phraseTextid + case when c.EntityType <> '''' then ''_'' + c.EntityType + ''_'' + convert(varchar,c.entityid)  else '''' end  as identifier,
		 	case when p.languageid is not null then ''x'' else '''' end as number, 
			p.languageid,
			p.countryid,
			isNull(convert(varchar,p.languageid) + ''_'' + convert(varchar,p.countryid),''Null'') as LanguageAndCountry,
			p.entityid,p.entitytypeid
--			,case when p.languageid = 0 then ''No Language'' else l.language end as language_,
--			case when p.countryid = 0 then ''Any Country'' else cty.countrydescription end as country


		from 
			' + @tableName +' as c
			left outer join 	
			vphrases as p
			on p.phrasetextid = c.phrasetextid
			and convert(varchar,p.entityid) = c.entityid
			and p.entityTypeID = c.EntityTypeID	
--			left join language l on p.languageid = l.languageid
--			left join country cty on p.countryid = cty.countryid
		order by c.phraseTextID,p.entityid,p.entitytypeid, p.languageid, p.countryid'


		exec (@cmd)

	END




