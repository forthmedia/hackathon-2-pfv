/****** Object:  StoredProcedure [dbo].[sp_fieldTrigger_oppDistiModified]    Script Date: 04/24/2014 10:39:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_fieldTrigger_oppDistiModified] 
	@modEntityid int, 
    @modaction varchar(10),
    @triggerInitiatedBy int, 
    @RecordUpdatesAgainstID int,
    @triggerInitiatedByPersonID int = null
AS
Set nocount on

declare @distiFlagID int
select @distiFlagID = flagID from flag where flagTextID = dbo.getSettingValue('leadManager.distiLocFlagList')


/* distiLocationID now holds a locationID !!!!! */

-- delete any other radio buttons in the group
if (isNull(@distiFlagID,0) != 0)
begin
	delete booleanFlagData
	from
		booleanFlagdata bfd
		inner join vFlagDef v on v.flagID = bfd.flagID
		inner join vFlagDef v1 on v.flagID = @distiFlagID and v.flagGroupID = v1.flagGroupID
		inner join location l on bfd.entityID = l.locationID
		inner join opportunity opp on opp.distiLocationID = l.locationID
		inner join #modifiedEntityIDs m on m.entityID = opp.opportunityID
	where bfd.flagId != @distiFlagID

	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated,lastUpdatedByPerson) 
	select distinct l.locationID,@distiFlagID,@RecordUpdatesAgainstID,getDate(),@RecordUpdatesAgainstID,getDate(),@triggerInitiatedByPersonID
	from #modifiedEntityIDs m
		inner join opportunity opp on opp.opportunityID = m.entityID
		inner join location l on opp.distiLocationID = l.locationID
		left join booleanflagdata bfd on bfd.entityID = l.locationID and bfd.flagID = @distiFlagID
	where
		bfd.entityID is null
end	
Set nocount off