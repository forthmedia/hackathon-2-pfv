/* adding the merge only dedupe methods */

if not exists (select 1 from dedupeMatchMethod where dedupeTypeID = 3) and exists(select 1 from dedupeMatchMethod where dedupeExecOrder = 6)
update dedupeMatchMethod set dedupeExecOrder = dedupeExecOrder+1 where dedupeExecOrder>5
GO

if not exists (select dedupeMatchMethodID from dedupeMatchMethod where dedupeEntityType = 'organisation' and dedupeTypeID=3)
begin
	insert into dedupeMatchMethod (dedupeMatchMethodID,dedupeMatchMethodDescription,dedupeEntityType,dedupeExecOrder,active,dedupeTypeID)
	values (12,'Organisation: MergeOnly Dupes','Organisation',2,1,3)
end
GO
if not exists (select dedupeMatchMethodID from dedupeMatchMethod where dedupeEntityType = 'location' and dedupeTypeID=3)
begin
	insert into dedupeMatchMethod (dedupeMatchMethodID,dedupeMatchMethodDescription,dedupeEntityType,dedupeExecOrder,active,dedupeTypeID)
	values (13,'Location: MergeOnly Dupes','Location',4,1,3)
end
GO
if not exists (select dedupeMatchMethodID from dedupeMatchMethod where dedupeEntityType = 'person' and dedupeTypeID=3)
begin
	insert into dedupeMatchMethod (dedupeMatchMethodID,dedupeMatchMethodDescription,dedupeEntityType,dedupeExecOrder,active,dedupeTypeID)
	values (14,'Person: MergeOnly Dupes','Person',6,1,3)
end
GO


if not exists(select * from FieldTrigger where TriggerName = 'sp_fieldTrigger_priLeadContact')
	insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'flag',flagid,0,1,'sp_fieldTrigger_priLeadContact' from flag where flagtextid= 'PrimaryLeadContact'
if exists(select * from information_schema.routines where routine_name='sp_fieldTrigger_priLeadContact')
	drop procedure sp_fieldTrigger_priLeadContact
GO
CREATE PROCEDURE [dbo].[sp_fieldTrigger_priLeadContact] 
	@flagid int, 
	@entityid int,
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@RecordUpdatesAgainstID int
AS
Set nocount on

/*if a user is allocated as the PrimaryLeadContact - org integer flag - 
they will be flagged with [person boolean flag] PortalLeadManager  */
IF @modaction = 'FA' or @modaction = 'FM' 
BEGIN
	declare @orgFlagID int
	declare @perFlagID int
	if exists(select flagid from flag where flagtextid= 'PrimaryLeadContact')
		begin
			if exists(select flagid from flag where flagtextid= 'PortalLeadManager')
				begin
					select @orgFlagID=flagid from flag where flagtextid= 'PrimaryLeadContact';
					select @perFlagID=flagid from flag where flagtextid= 'PortalLeadManager';
					insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
					select data,@perFlagID,createdBy,created,lastupdatedBy,lastupdated from integerflagdata i 
					where flagid=@orgFlagID and entityid=@entityid and data not in 
					(select entityid from booleanflagdata where flagid=@perFlagID and entityid=i.data);
				end
		end
END
Set nocount off

GO
if exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='vendorAccountManagerPersonID')
	begin
		if not exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='vendorAccountManagerPersonID' and FieldMonitor=1)
			update ModEntityDef set FieldMonitor=1 where TableName='opportunity' and FieldName='vendorAccountManagerPersonID' 
	end
GO
if not exists(select * from FieldTrigger where TriggerName = 'sp_fieldTrigger_oppAcMgrModified')
	begin
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppAcMgrModified' from ModEntityDef where TableName='opportunity' and FieldName='vendorAccountManagerPersonID'
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppAcMgrModified' from ModEntityDef where TableName='opportunity' and FieldName='Whole Record'
	end
if exists(select * from information_schema.routines where routine_name='sp_fieldTrigger_oppAcMgrModified')
	drop procedure sp_fieldTrigger_oppAcMgrModified 
GO

CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppAcMgrModified] 
	@modEntityID int, 
	@entityid int,
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@RecordUpdatesAgainstID int

AS
Set nocount on

/*if a user is allocated as the vendorAccountManagerPersonID - opp field -  
they will be flagged with [person boolean flag] 'isAccountManager'  */
declare @perFlagID int
set @perFlagID = 0
if exists(select flagid from flag where flagtextid= 'isAccountManager')
	select @perFlagID=flagid from flag where flagtextid= 'isAccountManager';

IF @modaction = 'oppA' and @perFlagID > 0
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select vendorAccountManagerPersonID,@perFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
	from opportunity opp where opportunityid=@entityid and vendorAccountManagerPersonID not in 
	(select entityid from booleanflagdata where flagid=@perFlagID and entityid=opp.vendorAccountManagerPersonID);
END

IF @modaction = 'oppM' and @perFlagID > 0
BEGIN
	if exists (select * from ModEntityDef where TableName='opportunity' and FieldName='vendorAccountManagerPersonID' and ModEntityID=@modEntityID)
		begin 
			insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
			select vendorAccountManagerPersonID,@perFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
			from #I as i where vendorAccountManagerPersonID not in 
			(select entityid from booleanflagdata where flagid=@perFlagID and entityid=i.vendorAccountManagerPersonID);
		end
END
Set nocount off

GO
if exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='vendorSalesManagerPersonID')
	begin
		if not exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='vendorSalesManagerPersonID' and FieldMonitor=1)
			update ModEntityDef set FieldMonitor=1 where TableName='opportunity' and FieldName='vendorSalesManagerPersonID' 
	end
GO

if not exists(select * from FieldTrigger where TriggerName = 'sp_fieldTrigger_oppSalesMgrModified')
	begin
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppSalesMgrModified' from ModEntityDef where TableName='opportunity' and FieldName='vendorSalesManagerPersonID'
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppSalesMgrModified' from ModEntityDef where TableName='opportunity' and FieldName='Whole Record'
	end
GO

if exists(select * from information_schema.routines where routine_name='sp_fieldTrigger_oppSalesMgrModified')
	drop procedure sp_fieldTrigger_oppSalesMgrModified 
GO

CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppSalesMgrModified] 
	@modEntityID int, 
	@entityid int,
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@RecordUpdatesAgainstID int

AS
Set nocount on

/*if a user is allocated as the vendorAccountManagerPersonID - opp field -  
they will be flagged with [person boolean flag] 'isAccountManager'  */
declare @perFlagID int
set @perFlagID = 0
if exists(select flagid from flag where flagtextid= 'isSalesManager')
	select @perFlagID=flagid from flag where flagtextid= 'isSalesManager';

IF @modaction = 'oppA' and @perFlagID > 0
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select vendorSalesManagerPersonID,@perFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
	from opportunity opp where opportunityid=@entityid and vendorSalesManagerPersonID not in 
	(select entityid from booleanflagdata where flagid=@perFlagID and entityid=opp.vendorSalesManagerPersonID);
END

IF @modaction = 'oppM' and @perFlagID > 0
BEGIN
	if exists (select * from ModEntityDef where TableName='opportunity' and FieldName='vendorSalesManagerPersonID' and ModEntityID=@modEntityID)
		begin 
			insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
			select vendorSalesManagerPersonID,@perFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
			from #I as i where vendorSalesManagerPersonID not in 
			(select entityid from booleanflagdata where flagid=@perFlagID and entityid=i.vendorSalesManagerPersonID);
		end
END
Set nocount off

GO
if exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='distiLocationID')
	begin
		if not exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='distiLocationID' and FieldMonitor=1)
			update ModEntityDef set FieldMonitor=1 where TableName='opportunity' and FieldName='distiLocationID' 
	end
GO
if not exists(select * from FieldTrigger where TriggerName = 'sp_fieldTrigger_oppDistiModified')
	begin
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppDistiModified' from ModEntityDef where TableName='opportunity' and FieldName='distiLocationID'
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppDistiModified' from ModEntityDef where TableName='opportunity' and FieldName='Whole Record'
	end
if exists(select * from information_schema.routines where routine_name='sp_fieldTrigger_oppDistiModified')
	drop procedure sp_fieldTrigger_oppDistiModified 
GO

CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppDistiModified] 
	@modEntityID int, 
	@entityid int,
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@RecordUpdatesAgainstID int

AS
Set nocount on

/*if an organisation is allocated as the distiLocationID - opp field -  
that organisation will be flagged with [org boolean flag] 'Tier1Tier2DistributorDistri'  */
declare @distiFlagID int
set @distiFlagID = 0
if exists(select flagid from flag where flagtextid= 'Tier1Tier2DistributorDistri')
	select @distiFlagID=flagid from flag where flagtextid= 'Tier1Tier2DistributorDistri';

IF @modaction = 'oppA' and @distiFlagID > 0
BEGIN
	insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
	select distiLocationID,@distiFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
	from opportunity opp where opportunityid=@entityid and distiLocationID not in 
	(select entityid from booleanflagdata where flagid=@distiFlagID and entityid=opp.distiLocationID);
END

IF @modaction = 'oppM' and @distiFlagID > 0
BEGIN
	if exists (select * from ModEntityDef where TableName='opportunity' and FieldName='distiLocationID' and ModEntityID=@modEntityID)
		begin 
			insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
			select distiLocationID,@distiFlagID,lastupdatedBy,lastupdated,lastupdatedBy,lastupdated 
			from #I as i where distiLocationID not in 
			(select entityid from booleanflagdata where flagid=@distiFlagID and entityid=i.distiLocationID);
		end
END
Set nocount off
GO

if exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='partnerLocationID')
	begin
		if not exists (select ModEntityID from ModEntityDef where TableName='opportunity' and FieldName='partnerLocationID' and FieldMonitor=1)
			update ModEntityDef set FieldMonitor=1 where TableName='opportunity' and FieldName='partnerLocationID' 
	end
GO
if not exists(select * from FieldTrigger where TriggerName = 'sp_fieldTrigger_oppPartnerModified')
	begin
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppPartnerModified' from ModEntityDef where TableName='opportunity' and FieldName='partnerLocationID'
		insert into FieldTrigger (FieldType,FieldID,orderingIndex,active,Triggername) select 'ModEntity ',ModEntityID,0,1,'sp_fieldTrigger_oppPartnerModified' from ModEntityDef where TableName='opportunity' and FieldName='Whole Record'
	end
if exists(select * from information_schema.routines where routine_name='sp_fieldTrigger_oppPartnerModified')
	drop procedure sp_fieldTrigger_oppPartnerModified 
GO

CREATE PROCEDURE [dbo].[sp_fieldTrigger_oppPartnerModified] 
	@modEntityID int, 
	@entityid int,
	@modaction varchar(10),
	@triggerInitiatedBy int, 
	@RecordUpdatesAgainstID int

AS
Set nocount on

/*if a location is allocated as the partnerLocationID - opp field -  
their organisation will be flagged with [org boolean flag] 'OppPartner'  */
declare @partnerFlagID int
set @partnerFlagID = 0
if exists(select flagid from flag where flagtextid= 'OppPartner')
	select @partnerFlagID=flagid from flag where flagtextid= 'OppPartner';

IF (@modaction = 'oppA' or @modaction = 'oppM') and @partnerFlagID > 0
BEGIN
	if exists (select * from ModEntityDef where TableName='opportunity' and FieldName='partnerLocationID' and ModEntityID=@modEntityID)
		begin
			print @entityid
			insert into booleanflagdata (entityid,flagid,createdBy,created,lastupdatedBy,lastupdated) 
			select organisationID,@partnerFlagID,opp.lastupdatedBy,opp.lastupdated,opp.lastupdatedBy,opp.lastupdated 
			from #I as opp inner join location l on opp.partnerLocationID = l.locationID
				left join booleanFlagData bfd on bfd.entityid = l.organisationID and bfd.flagID=@partnerFlagID
			where opportunityid=@entityid
				and bfd.entityID is null
		end
END
Set nocount off

GO

/* WAB 2016-04-06 build problems subsequent to changing table name to opportunityProduct.  Only run this is a TABLE called  oppProducts exists */
if not exists (select top 1 * from information_schema.tables where table_name='oppProductsDel') and exists (select 1 from information_schema.tables where table_name='oppProducts' and table_type='table')
	exec generate_mrAudit @tableName='oppProducts',@mnemonic='OPPP', @ModEntityDefOn=1,@ModTriggerOn=1
GO

declare @oppCustomModentityid int

if not exists (select 1 from ModEntityDef where TableName='opportunity' and FieldName='oppCustom1')
Begin
select @oppCustomModentityid = MAX(modentityid)+1 from ModEntityDef where TableName='opportunity'
insert into modentitydef
(ModEntityID,TableName,FieldMonitor,FieldName,FlagID,FlagDesc,R1Share,R1FieldNumber,R1TableName,R1FieldName,R1OurFieldName,R2Share,R2FieldNumber,R2TableName,R2FieldName,R2OurFieldName,R3Share,R3FieldNumber,R3TableName,R3FieldName,R3OurFieldName,R4Share,R4FieldNumber,R4TableName,R4FieldName,R4OurFieldName,R5Share,R5FieldNumber,R5TableName,R5FieldName,R5OurFieldName,BlacklisttypeID,CFFormParameters)
values
(@oppCustomModentityid,'opportunity',0,'oppCustom1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
end

if not exists (select 1 from ModEntityDef where TableName='opportunity' and FieldName='oppCustom2')
Begin
select @oppCustomModentityid = MAX(modentityid)+1 from ModEntityDef where TableName='opportunity'
insert into modentitydef
(ModEntityID,TableName,FieldMonitor,FieldName,FlagID,FlagDesc,R1Share,R1FieldNumber,R1TableName,R1FieldName,R1OurFieldName,R2Share,R2FieldNumber,R2TableName,R2FieldName,R2OurFieldName,R3Share,R3FieldNumber,R3TableName,R3FieldName,R3OurFieldName,R4Share,R4FieldNumber,R4TableName,R4FieldName,R4OurFieldName,R5Share,R5FieldNumber,R5TableName,R5FieldName,R5OurFieldName,BlacklisttypeID,CFFormParameters)
values
(@oppCustomModentityid,'opportunity',0,'oppCustom2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
end

if not exists (select 1 from ModEntityDef where TableName='opportunity' and FieldName='oppCustom3')
Begin
select @oppCustomModentityid = MAX(modentityid)+1 from ModEntityDef where TableName='opportunity'
insert into modentitydef
(ModEntityID,TableName,FieldMonitor,FieldName,FlagID,FlagDesc,R1Share,R1FieldNumber,R1TableName,R1FieldName,R1OurFieldName,R2Share,R2FieldNumber,R2TableName,R2FieldName,R2OurFieldName,R3Share,R3FieldNumber,R3TableName,R3FieldName,R3OurFieldName,R4Share,R4FieldNumber,R4TableName,R4FieldName,R4OurFieldName,R5Share,R5FieldNumber,R5TableName,R5FieldName,R5OurFieldName,BlacklisttypeID,CFFormParameters)
values
(@oppCustomModentityid,'opportunity',0,'oppCustom3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
end

if not exists (select 1 from ModEntityDef where TableName='opportunity' and FieldName='oppCustom4')
Begin
select @oppCustomModentityid = MAX(modentityid)+1 from ModEntityDef where TableName='opportunity'
insert into modentitydef
(ModEntityID,TableName,FieldMonitor,FieldName,FlagID,FlagDesc,R1Share,R1FieldNumber,R1TableName,R1FieldName,R1OurFieldName,R2Share,R2FieldNumber,R2TableName,R2FieldName,R2OurFieldName,R3Share,R3FieldNumber,R3TableName,R3FieldName,R3OurFieldName,R4Share,R4FieldNumber,R4TableName,R4FieldName,R4OurFieldName,R5Share,R5FieldNumber,R5TableName,R5FieldName,R5OurFieldName,BlacklisttypeID,CFFormParameters)
values
(@oppCustomModentityid,'opportunity',0,'oppCustom4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
end

if not exists (select 1 from ModEntityDef where TableName='organisation' and FieldName='vatnumber')
Begin
select @oppCustomModentityid = MAX(modentityid)+1 from ModEntityDef where TableName='organisation'
insert into modentitydef
(ModEntityID,TableName,FieldMonitor,FieldName,FlagID,FlagDesc,R1Share,R1FieldNumber,R1TableName,R1FieldName,R1OurFieldName,R2Share,R2FieldNumber,R2TableName,R2FieldName,R2OurFieldName,R3Share,R3FieldNumber,R3TableName,R3FieldName,R3OurFieldName,R4Share,R4FieldNumber,R4TableName,R4FieldName,R4OurFieldName,R5Share,R5FieldNumber,R5TableName,R5FieldName,R5OurFieldName,BlacklisttypeID,CFFormParameters)
values
(@oppCustomModentityid,'organisation',0,'vatnumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
end

/* Add tab, carriage return and newline to the list of characters to replace with empty string*/
if not exists(select 1 from charReplacement where charToReplace = char(10))
insert into charReplacement (charToReplace,replacementChar)
values (char(10),'')

if not exists(select 1 from charReplacement where charToReplace = char(13))
insert into charReplacement (charToReplace,replacementChar)
values (char(13),'')

if not exists(select 1 from charReplacement where charToReplace = char(9))
insert into charReplacement (charToReplace,replacementChar)
values (char(9),'')
