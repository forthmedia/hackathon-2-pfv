createPlaceholderObject 'procedure',  'addSystemColumns'
GO


/*
WAB 2017-01-25
A stored procedure to standardise the way that we add the system columns to tables
There are so many bits of script knocking around that do this and they all end up with different defaults and degrees of nullability
Also it is a fiddle to make columns not null when there are already rows in the database, so people tend to ignore it (or just use the default which can be inappropriate)
This procedure allows some flexibility on nulls because sometimes there are backwards compatibility problems (especially when adding the lastudpatedbyperson column)
In its default settings it doesn't add created/lastupdated because we ought to be phasing them out in favour of the personID versions

WAB 2017-02-27 during PROD2016-3487   Make sure that any del table stays in synch
*/
alter procedure addSystemColumns 
		  @tablename sysname
		, @excludeCreatedByLastUpdatedBy bit = 1
		, @allowNullCreatedByLastUpdatedBy bit = 0
		, @allowNullCreatedByPersonLastUpdatedByPerson bit = 1
		, @defaultCreatedDate dateTime = 0

AS
	declare @sqlcmd nvarchar(max)
	declare @modificationsMade bit = 0	

	if not exists (select * from information_schema.columns where table_name= @tablename and column_name='created')
	begin
			set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] add [created] datetime NULL
							ALTER TABLE [dbo].['+@tablename+'] ADD  CONSTRAINT [DF_'+@tablename+'_created]  DEFAULT (getdate()) FOR [created]'	
			exec (@sqlcmd)

			set @sqlcmd = 'update [dbo].['+@tablename+'] set [created]  = @defaultCreatedDate where created is null
							alter TABLE [dbo].['+@tablename+'] alter column [created] datetime NOT NULL'
			exec sp_executesql @sqlcmd, N'@defaultCreatedDate datetime',  @defaultCreatedDate
			set @modificationsMade  = 1
	end
    

	if not exists (select * from information_schema.columns where table_name= @tablename and column_name='lastupdated')
	begin
			set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] add [lastupdated] datetime NULL
							ALTER TABLE [dbo].['+@tablename+'] ADD  CONSTRAINT [DF_'+@tablename+'_lastupdated]  DEFAULT (getdate()) FOR [lastupdated]'	
			exec (@sqlcmd)

			set @sqlcmd = 'update [dbo].['+@tablename+'] set [lastupdated]  = created where lastupdated is null
						alter TABLE [dbo].['+@tablename+'] alter column [lastupdated] datetime NOT NULL'
			exec (@sqlcmd)
			set @modificationsMade  = 1
	end

	IF not (@excludeCreatedByLastUpdatedBy = 1)
	BEGIN

		if not exists (select * from information_schema.columns where table_name= @tablename and column_name='createdby')
		begin
			select @sqlcmd='alter TABLE [dbo].['+@tablename+'] add [createdby] int NULL'
			exec (@sqlcmd)

			set @sqlcmd = 'update [dbo].['+@tablename+'] set [createdby] = 0 where createdby is null'
			exec (@sqlcmd)

			IF @allowNullCreatedByLastUpdatedBy = 0
			BEGIN
				set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] alter column [createdby] int NOT NULL'
				exec (@sqlcmd)
			END
			
			set @modificationsMade  = 1
		end	

		if not exists (select * from information_schema.columns where table_name= @tablename and column_name='lastUpdatedBy')
		begin
			select @sqlcmd='alter TABLE [dbo].['+@tablename+'] add [lastUpdatedBy] int NULL'
			exec (@sqlcmd)
			
			set @sqlcmd = 'update [dbo].['+@tablename+'] set [lastUpdatedBy] = createdby where lastUpdatedBy is null'
			exec (@sqlcmd)

			IF @allowNullCreatedByLastUpdatedBy = 0
			BEGIN
				set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] alter column [lastUpdatedBy] int NOT NULL'
				exec (@sqlcmd)
			END

			set @modificationsMade  = 1
		end	

	end


	
    if not exists (select * from information_schema.columns where table_name= @tablename and column_name='createdByPerson')  
	begin
		select @sqlcmd='alter TABLE [dbo].['+@tablename+'] add [createdByPerson] int NULL'
		exec (@sqlcmd)
		
		set @sqlcmd = 'update [dbo].['+@tablename+'] set [createdByPerson] = 0 where createdByPerson is null'
		exec (@sqlcmd)

		IF @allowNullCreatedByPersonLastUpdatedByPerson = 0
		BEGIN
			set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] alter column [createdByPerson] int NOT NULL'
			exec (@sqlcmd)
		END
		
		set @modificationsMade  = 1
	end	


    if not exists (select * from information_schema.columns where table_name= @tablename and column_name='lastUpdatedByPerson')
    begin  
		select @sqlcmd='alter TABLE [dbo].['+@tablename+'] add [lastUpdatedByPerson] int NULL'
		exec (@sqlcmd)
		
		set @sqlcmd = 'update [dbo].['+@tablename+'] set [lastUpdatedByPerson] = createdByPerson where lastUpdatedByPerson is null'
		exec (@sqlcmd)

		IF @allowNullCreatedByPersonLastUpdatedByPerson = 0
		BEGIN
			set @sqlcmd = 'alter TABLE [dbo].['+@tablename+'] alter column [lastUpdatedByPerson] int NOT NULL'
			exec (@sqlcmd)
		END
		set @modificationsMade  = 1
	end	


	/* If there is a del table then need to keep it in synch */    
	IF (@modificationsMade  = 1) and exists (select 1 from sysobjects where name = @tablename + 'del')
	BEGIN
		exec generate_mrauditdel @tablename
	END