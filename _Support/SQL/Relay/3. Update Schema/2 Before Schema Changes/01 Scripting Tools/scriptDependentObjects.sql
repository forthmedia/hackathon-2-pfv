IF exists (select 1 from sysobjects where name = 'scriptDependentObjects')
	/* can't use my createPlaceHolderObject procedure here because of the recursive nature of the function */ 
	DROP FUNCTION scriptDependentObjects

GO


CREATE function [dbo].[scriptDependentObjects] (
	@objectName sysname,  /* object name, can be in tableName.ColumnName format */
	@xtypes varchar(50) = null  /* to limit object types */
)

RETURNS @returnTable TABLE 
		(	objectName sysname, 
			columnName sysname null, 
			xtype varchar (10),
			type varchar(20), 
			checkExistsScript nvarchar(max), 
			OKToCreateScript nvarchar(max), 
			createscript nvarchar(max), 
			dropScript nvarchar(max),
			id int identity(1,1),
			orderIndex int null
		)
AS
BEGIN
	/*
	WAB 2014-11-03	A function which returns a table of objects which are dependent upon a given object
					In particular those objects which will prevent the given object being altered  (so for example schema bound views rather than all views)
	WAB 2014-12-02 	Changes because of change in return from scriptTableIndexes()
	WAB 2015-01-21 	Change recursive step so that it brings back checkExistsScript
	WAB 2016-01-27	deal with constraints and defaults on the incoming table/column
	WAB 2016-03-22	Problems not picking up FullText Indexes on
	NJH 2016/08/25	PROD_2016_1190 (Put identity columns on POL tables) Deal with Foreign keys
	WAB	2016-09-05	Deal with Foreign Keys pointing to this table's Primary Key
					Deal with multi-column keys 
					Was missing some triggers (Picked up triggers which referenced the table, but not necessarily all triggers on the table (if they did not actually reference the table explicitly eg MRAudit triggers)
					Deal with Primary Key
					Add an orderIndex
	WAB 2016-12-05	Spurious Foreign keys/Triggers being returned when a columnName is passed in.  Check that they refer to the column name
	*/

	


	/* support for @objectName to be in table.column notation */
	declare 
		@orginalObjectName sysname = @objectName,  /* objectName as passed into the sp, could be in table.column format */
		@columnName sysname,
		@findDot int = CHARINDEX('.',@objectName,1)
		
	IF @findDot <> 0	
	BEGIN
		Select 
			@columnName = RIGHT (@objectName,len(@objectName)-@findDot), 
			@objectName = LEFT (@objectName,@findDot-1)
	END
	

	insert into @returnTable(objectName , columnName, xtype , type, checkExistsScript, OKToCreateScript, createscript ,  dropScript )
	select
		o.name as objectName, 
		col.name as columnName,
		xtype,
		o.type + case when o.parent_obj <> object_id (@objectName) then '(Referenced)' else '' end,
		checkExistsScript = 'select 1 from sysObjects where name = ''' + o.name + ''' and xtype = '''+xtype +'''', 
		OKToCreateScript = 
			CASE when xType = 'c' and col.name is not null then
				'IF exists (select 1 from information_schema.columns where table_name = ''' + object_name (parent_obj) + ''' and column_name = ''' + col.name + ''')' + char(10)
			ELSE
					''	
			END
		, 
		createScript = 
			/* for a contstraint we need to build the create script, other object scripts come from OBJECT_DEFINITION() function */
			CASE when xType = 'c' then
				 'alter table '  + object_name (parent_obj) + ' add constraint ' + o.name + ' CHECK (' + OBJECT_DEFINITION(o.id) + ')'
			ELSE  
				OBJECT_DEFINITION(o.id) 
			END
		, 
		dropScript = 
			/* for a contstraint we need do an alter table plus a drop, other objects just drop */
			CASE when xType = 'c' then
				'Alter Table ' + object_name (parent_obj)
			ELSE
				''
			END
			+	' drop ' + case when xType = 'v' then 'View' when xType='TR' then 'Trigger' when xType='C' then 'constraint' end + ' ' + o.name 

		FROM 
			sysobjects o 
			left join sys.dm_sql_referencing_entities('dbo.' + @objectName,'OBJECT') e ON o.id = e.referencing_id
			left join sys.check_constraints sc on sc.object_id = o.id
			left join sys.columns col on sc.parent_object_id = col.object_id and sc.parent_Column_ID = col.column_id

		where 
			(e.referencing_ID is not null OR o.parent_obj = object_id (@objectName) )
			AND
			/* if @xtypes passed in use it (with a rather hacky IN clause) or limit to Views, Triggers, Contraints (can be extended if needed) */
			xType IN ('V','TR','C')  -- limit to these types (FK,PK dealt with elsewhere)
			AND
			(
				(isnull(@xtypes,'') = '' ) 
					or 
				dbo.listfind (isnull(@xtypes,''), rtrim(xType)) <> 0 
				
			)
			/* if getting views, we only get schema bound ones */			
			and (xtype <> 'v' OR OBJECT_DEFINITION(o.id)  like '%with schemabinding%')
			/* if a columnName has been passed in and we are looking at view, then only return it if the view definition contains that column may give false positive but shouldn't give false negative */
			and (isnull(@columnName,'') = '' OR xtype <> 'v' OR (isnull(@columnName,'') <> '' AND OBJECT_DEFINITION(o.id)  like '%' + isnull(@columnName,'') + '%') )
			/* if a columnName has been passed in and we are looking at constraint then can filter by column  */
			and (isnull(@columnName,'') = '' OR xtype <> 'c' OR (col.name is not null and isnull(@columnName,'') = col.name) )
			/* if a columnName has been passed in and we are looking at trigger then check the trigger (without comments) for the columnname  */
			and (isnull(@columnName,'') = '' OR xtype <> 'tr' OR (dbo.RegExReplaceX('/\*.*?\*/', OBJECT_DEFINITION(o.id), '', dbo.RegExOptionEnumeration(1,1,0,0,1,0,0,0,0)) like '%' + @columnname +'%' ))

 

			/* deal with defaults and constraints on self */
			insert into @returnTable (objectname, columnName,xtype,type, checkExistsScript, OKToCreateScript, createScript,dropscript)
			select 
				   so.name
				 , sc.name
				 , 'D'
				 , 'DEFAULT'
				 , 'select 1 where object_id (''' + so.name + ''') is not null'
				 , null
				 , [createScript] =   'ALTER TABLE ' + OBJECT_NAME(sc.id) + ' WITH NOCHECK ADD CONSTRAINT ' + so.name +  ' DEFAULT ' + sm.text + ' FOR ' + QuoteName(sc.name)
				 , [dropScript] = 'alter table ' + OBJECT_NAME(sc.id) + ' drop constraint ' + so.name + ';'

			from 
				syscolumns  sc 
				   INNER JOIN
			   sysobjects as so on sc.cdefault = so.id
				   INNER JOIN
			   syscomments as sm on sc.cdefault = sm.id
			where 
				OBJECTPROPERTY(so.id, N'IsDefaultCnst') = 1
				and OBJECT_NAME(sc.id) = @objectName
				and (@columnName  is null or sc.name = @columnName )
				
		
	
	
	
		/* if the object is a view or a table with indexes referencing @columnName
			then we need to script its indexes separately
			
       */
		IF EXISTS (select 1 from sysobjects where name = @objectName and xtype in ('v','u'))
		BEGIN
			insert into @returnTable(objectName , xtype , type, checkExistsScript,createscript ,  dropScript )
			select 
			CASE WHEN TYPE = 'FULLTEXTINDEX'
				THEN 'FULLTEXT  ' + indexName
			ELSE
				@objectName + '.' + indexName
			END	, 
			
			'i', type, checkExistsScript, createScript, 
			CASE WHEN TYPE = 'FULLTEXTINDEX'
				THEN 'DROP FULLTEXT INDEX ON ' + indexName
			ELSE
				'DROP INDEX ' + @objectName + '.' + indexName
			END	
			from  dbo.scriptTableIndexes (@orginalObjectName,default)
		END
		
		
		/* NJH 2016/08/25 Deal with any foreign keys 
		WAB 2016-09-07  Replaced with a version that does the primary key as well
						(also does contraints, but these are actually handled elsewhere)
						Also fixed to deal with multi-column keys
		*/
		IF isNull(@xtypes,'') = '' or dbo.listfind (@xtypes,'FK') <> 0 or dbo.listfind (@xtypes,'PK') <> 0
		BEGIN
			insert into @returnTable(objectName , xtype , type, checkExistsScript, createscript ,  dropScript )
			SELECT
				TC.CONSTRAINT_NAME
				,CASE TC.CONSTRAINT_TYPE
					WHEN 'PRIMARY KEY'
						THEN 'PK' 
					WHEN 'CHECK'
						THEN 'CK'
					WHEN 'FOREIGN KEY'
						THEN 'FK' 
				  END		
				, CASE TC.CONSTRAINT_TYPE
					WHEN 'PRIMARY KEY'
						THEN 'PK' 
					WHEN 'CHECK'
						THEN 'CK'
					WHEN 'FOREIGN KEY'
						THEN 'FK' + CASE WHEN	TC.TABLE_NAME <> @objectname THEN '(Other Table)' ELSE '' END
				  END
				, 'select 1 from sysObjects where name = ''' + TC.CONSTRAINT_NAME + '''' 
				,CASE TC.CONSTRAINT_TYPE
					WHEN 'PRIMARY KEY'
						THEN 'ALTER TABLE [dbo].[' + @objectname + '] ADD CONSTRAINT [' + TC.CONSTRAINT_NAME + '] PRIMARY KEY' + ' (' + dbo.concatenate('['+CCU.COLUMN_NAME + '],') + ')'
					/* Removed constraints from this query - already dealt with above
					WHEN 'CHECK'
						THEN 'ALTER TABLE [dbo].[' + @objectname + ']  WITH CHECK ADD  CONSTRAINT [' + TC.CONSTRAINT_NAME + '] CHECK  (' + OBJECT_DEFINITION(OBJECT_ID(TC.CONSTRAINT_NAME)) + '); ALTER TABLE [dbo].[' + @objectname + '] CHECK CONSTRAINT [' + TC.CONSTRAINT_NAME + ']'
					*/	
					WHEN 'FOREIGN KEY'
						THEN 'ALTER TABLE [dbo].[' + 	CASE WHEN	TC.TABLE_NAME = @objectname THEN @objectname ELSE TC.TABLE_NAME END  + ']  
							WITH CHECK ADD  CONSTRAINT [' + TC.CONSTRAINT_NAME + '] 
							FOREIGN KEY(' + dbo.concatenate('['+ INFORMATION_SCHEMA_FK.COLUMN_NAME + '],') +  ') 
							REFERENCES [dbo].[' + INFORMATION_SCHEMA_FK.REFERENCED_TABLE_NAME + '] (' + dbo.concatenate('['+ INFORMATION_SCHEMA_FK.REFERENCED_COLUMN_NAME + '],') +  ')'
							+ case when update_rule != 'NO_ACTION' then ' ON UPDATE '+ cast(update_rule COLLATE Latin1_General_CI_AI as varchar(max)) else '' end 
							+ case when delete_rule != 'NO_ACTION' then ' ON DELETE '+ cast(delete_rule COLLATE Latin1_General_CI_AI as varchar(max)) else '' end 
							-- + ';ALTER TABLE [dbo].[' + tc.TABLE_NAME + '] CHECK CONSTRAINT [' + TC.CONSTRAINT_NAME + ']'
				END 

				, 'alter table ' + TC.TABLE_NAME + ' drop constraint ' + TC.CONSTRAINT_NAME
			FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC
			JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE CCU ON TC.CONSTRAINT_NAME = CCU.CONSTRAINT_NAME
			LEFT JOIN (
				SELECT  
					 KCU1.CONSTRAINT_NAME AS CONSTRAINT_NAME
					,KCU1.TABLE_NAME AS TABLE_NAME 
					,KCU1.COLUMN_NAME AS COLUMN_NAME 
					,KCU1.ORDINAL_POSITION AS ORDINAL_POSITION 
					,KCU2.CONSTRAINT_NAME AS REFERENCED_CONSTRAINT_NAME 
					,KCU2.TABLE_NAME AS REFERENCED_TABLE_NAME 
					,KCU2.COLUMN_NAME AS REFERENCED_COLUMN_NAME 
					,KCU2.ORDINAL_POSITION AS REFERENCED_ORDINAL_POSITION 
					,rc.update_rule
					,rc.delete_rule
				FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

				INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
					ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
					AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
					AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

				INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU2 
					ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG  
					AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA 
					AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME 
					AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION 
			) AS INFORMATION_SCHEMA_FK ON TC.CONSTRAINT_NAME = INFORMATION_SCHEMA_FK.CONSTRAINT_NAME and CCU.COLUMN_NAME = INFORMATION_SCHEMA_FK.COLUMN_NAME
			WHERE 
				TC.CONSTRAINT_TYPE <> 'CHECK'
				AND
				(TC.TABLE_NAME = @objectname or REFERENCED_TABLE_NAME = @objectname) 
				AND 
				(@columnname is null or ccu.column_name = @columnname)
			group by 
			TC.CONSTRAINT_NAME, TC.CONSTRAINT_Type, TC.TABLE_Name,   INFORMATION_SCHEMA_FK.REFERENCED_TABLE_NAME, INFORMATION_SCHEMA_FK.REFERENCED_TABLE_NAME, update_rule , delete_rule



		END		
		
			
		/* add a level of recursion.  Initally to pickup schema bound indexes on views */
		DECLARE @recurseObjectName sysname
		DECLARE recurse_cursor CURSOR LOCAL FOR

			/* get any dependent objects which are views */
			SELECT objectName from @returnTable
			where xtype = 'v'

			OPEN recurse_cursor
			FETCH NEXT FROM recurse_cursor INTO @recurseObjectName
				WHILE (@@FETCH_STATUS = 0)

					BEGIN
						
						insert into @returnTable(objectName , columnName, xtype , type, checkExistsScript, createscript ,  dropScript )	
						SELECT 
							objectName , columnName, xtype , type, checkExistsScript, createscript ,  dropScript
						FROM 
							dbo.scriptDependentObjects (@recurseObjectName,  'i')
	
						FETCH NEXT FROM recurse_cursor INTO @recurseObjectName
					
					END	
			
			CLOSE  recurse_cursor
			DEALLOCATE  recurse_cursor
	
	
	


	/* WBA add some rudimentary ordering */
	Update 	@returnTable
	set orderIndex = case	
		when xType = 'PK' THEN 1
		when xType = 'FK' THEN 2
		when xType = 'V' THEN 3
		else 4
		END
		


	RETURN
	
END



