	IF EXISTS (select 1 from sysObjects where name = 'createPlaceHolderObject')
		DROP Procedure createPlaceHolderObject
	
	GO
	
	create procedure  
		createPlaceHolderObject 
		@ObjectType sysname, /* VIEW, FUNCTION, TABLEFUNCTION, INLINEDTABLEFUNCTION, TRIGGER, INSTEADOFTRIGGER, PROCEDURE */
		@objectName sysname,
		@parameters nvarchar(max) = ''  /* For functions that need to be created with correct arguments (esp. if called recursively */  
	AS
		/* 	
		WAB 2014-10-29	
		This script creates a placeholder object (function,procedure, .. to be extended) if it does not already exist.
		The idea is that you use this in a release script and then use an ALTER statement to create/update the object
		This makes our release scripts easier
			i) Don't have to script a test and drop
			ii) No chance of a script error leaving us without a function  
		
		WAB 2015-02-25 add support for inlined table function
		WAB 2015-11-04 add support for insteadOf Trigger

	*/
	
	
		/* support for @objectName to be in table.object notation (for triggers, indexes) */
		declare 
			@tablename sysname = '',
			@findDot int = CHARINDEX('.',@objectName,1)
			
		IF @findDot <> 0	
		BEGIN
			Select  
				@tablename = LEFT (@objectName,@findDot-1),
				@objectName = RIGHT (@objectName,len(@objectName)-@findDot)
		END
	
		declare @SQL nvarchar(max)
		
		IF @objectType = 'Function'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type IN ('FN'))
				BEGIN
					Set @SQL = 
						'CREATE FUNCTION [dbo].[' + @objectName +' ] (' + @parameters + ') returns int as
						BEGIN
							return 1
						END'
					exec (@sql)
				
				END
		END
		IF @objectType = 'TableFunction'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type IN ('TF'))
				BEGIN
					Set @SQL = 
						'CREATE FUNCTION [dbo].[' + @objectName +' ] (' + @parameters + ') RETURNS @x TABLE (x int) 
						BEGIN
							RETURN
						END'
					exec (@sql)
				
				END
		END
		ELSE IF @objectType = 'InlinedTableFunction'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type IN ('IF'))
				BEGIN
					Set @SQL = 
						'CREATE FUNCTION [dbo].[' + @objectName + ' ] (' + @parameters + ') RETURNS TABLE AS 
							RETURN (select 1 as x)
						'
					exec (@sql)
				
				END
		END
		ELSE IF @objectType = 'PROCEDURE'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type = N'P')
				BEGIN
					set @SQL = 
						'CREATE Procedure [' + @objectName +' ] as'
					exec (@sql)
				
				END
		END
		ELSE IF @objectType = 'view'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type = N'V')
				BEGIN
					set @SQL = 
						'CREATE View [' + @objectName +' ] as select 1 as dummy'
					exec (@sql)
				
				END
		END
		ELSE IF @objectType = 'trigger'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type = N'TR' and parent_obj = OBJECT_ID(@tablename))
				BEGIN
					set @SQL = 
						'CREATE Trigger [' + @objectName +'] ON [' + @tablename + '] FOR INSERT AS return'
					print @sql	
					exec (@sql)
				
				END
		END
		ELSE IF @objectType = 'insteadoftrigger'
		BEGIN
			IF  NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(@objectName) AND type = N'TR' and parent_obj = OBJECT_ID(@tablename))
				BEGIN
					set @SQL = 
						'CREATE Trigger [' + @objectName +'] ON [' + @tablename + '] instead of update AS return'

					print @sql	
					exec (@sql)
				
				END
		END
		
		
