createPlaceHolderObject 'procedure','dropAndRecreateDependentObjects'
GO


ALTER procedure [dbo].[dropAndRecreateDependentObjects] 
	@objectName sysname,  /* name of the object */
	@action varchar(30),		/* drop, recreate, test */
	@tempTable sysname = '' OUTPUT
AS

/*
WAB 2014-11-03

A procedure for Dropping and Recreating dependent objects so that a parent object can be updated

For example when updating the length of a field

Table Columns referenced by schema bound views
Stored Procedures referenced in contraints

2014-12-02 WAB remove an if statement regarding preventing attempt to drop indexes which no longer existed - now dealt with on all objects with a check exists
2015-01-21	WAB	Improvements so does not try to drop opbjects which do not exist (which can happen if an object was deleted as a consequence of deleting something else earlier)
2016-01-27	WAB Better (any) handling of FullText Indexes.  They need to be dropped outside of the transaction with a call of action DROPFULLTEXTINDEXES
2016-03-23	WAB	Problems with dropping and reccreating full text indexes.  Not exactly sure of problem, but made a change!
2016-09-07	WAB	2016_1190 (Put identity columns on POL tables) Minor change so that blank query not output
			use new orderIndexColumn returned by scriptDependentObjects
2017-02-02	WAB	Make temporary table name unique to the SPID and return name of table (some procedures need to doctor what gets recreated)
*/

	DECLARE
		@sql nvarchar(max), 
		@scriptColumn sysname, 
		@objectType varchar(10),
		@xtypes varchar(50), 
		@xtype varchar(2),
		@count int


	/* support for @objectName to be in table.column notation */
	declare 
		@baseObjectName sysname = @objectName,
		@columnName sysname,
		@findDot int 
	
	SET @findDot = CHARINDEX('.',@objectName,1)	
		
	IF @findDot <> 0	
	BEGIN
		Select @columnName = RIGHT (@objectName,len(@objectName)-@findDot), @baseObjectName = LEFT (@objectName,@findDot-1)
	END

	
	/* for different object types we are interested in different dependent objects, 
		To be expanded as we go along
	*/
	SELECT 
		@xtypes = 
			CASE	
				WHEN xtype in ('U','V') THEN 'V,I,C,D,PK,TR'
				WHEN xtype = 'FN' THEN 'C'
				ELSE null 
			END
		FROM 
			sysobjects 
		WHERE name = @baseObjectName

	SET	@tempTable = '[##dropAndRecreateDependentObjects_' + @objectName + '_' + convert(varchar,@@spid) + ']' 


	IF @action = 'DROP' OR @action = 'TEST' 
	BEGIN
		/* for drop and test we create a temporary table of scripts */
		SET @sql = 'select * into ' + @tempTable + ' from dbo.scriptDependentObjects(@objectName,@xtypes)'
		exec sp_executeSQL @sql,N'@objectName sysname,@xtypes varchar(50)',@objectName = @objectName,@xtypes=@xtypes
		print 'Temp table created: ' + @tempTable

		select @scriptColumn = 'dropScript'
		
		SET @sql = 'select @count = count(1) from ' + @tempTable + ' where type = ''FULLTEXTINDEX'' '
		exec sp_executesql @sql, N'@count int OUTPUT', @count = @count OUTPUT
		
		IF @count <> 0 
		BEGIN
			Print 'There is a full text index.  You must run dropAndRecreateDependentObjects in DROPFULLTEXTINDEXES mode before the tranaction, in RECREATEFULLTEXTINDEXES mode after the transaction'
			IF @action != 'Test' /* NJH 2016/08/25 - don't return if in test mode so that the table gets dropped and we can see what would be done */
			Return
		END 		

		
		
	END
	ELSE IF @action = 'DROPFULLTEXTINDEXES' 
	BEGIN

		/* for drop and test we create a temporary table of scripts */
		SET @tempTable = replace(@tempTable,']', '_FULLTEXT]')
		SET @sql = 'select * into ' + @tempTable + ' from dbo.scriptDependentObjects(@objectName,''I'') where type = ''FULLTEXTINDEX'' '
		print 'Temp table created: ' + @tempTable 
		exec sp_executeSQL @sql ,N'@objectName sysname,@xtypes varchar(50)', @objectName = @objectName, @xtypes=@xtypes
		select @scriptColumn = 'dropScript'
		
		
	END
	ELSE IF @action = 'RECREATE' OR @action = 'RECREATEFULLTEXTINDEXES'
	BEGIN
		
		select @scriptColumn = 'createScript'
		
		
		IF 	@action = 'RECREATEFULLTEXTINDEXES' 
		BEGIN	
			SET @tempTable = replace(@tempTable,']', '_FULLTEXT]')
			IF OBJECT_ID('tempdb..' + @tempTable) is null
			BEGIN
				Print 'Temp Table does not exist, assume no full text indexes'		
				return
			END	
		END
	
		
	END
	ELSE
	BEGIN
		Print '@Action (' + @action + ') must be DROP, RECREATE, TEST or RECREATEFULLTEXTINDEXES'
		return
	END

IF @action = 'TEST'
BEGIN
	set @sql = 'select * from ' + @tempTable
	exec(@sql)

END
ELSE
BEGIN
	IF  @@Trancount = 0 AND @action NOT like '%FULLTEXTINDEXES'
	BEGIN
		Print 'Must be called in a transaction'
	END
	ELSE
	BEGIN
		set @sql = '
					DECLARE scriptCursor CURSOR  FOR
						select objectname, ' + @scriptColumn + ', XTYPE , checkExistsScript from '
					+ @tempTable 
					+  ' order by orderIndex ' + case when @action like '%recreate%' then 'asc' else 'desc' end + ', id'
		exec(@sql)
		
		declare @objName sysname, @scriptSQL nvarchar(max), @checkScriptSQL nvarchar(max), @objectExists bit
		OPEN scriptCursor
		FETCH NEXT FROM scriptCursor into @objName, @scriptSQL,@XTYPE, @checkScriptSQL
		while @@FETCH_STATUS = 0
		begin
			-- print @scriptSQL
				print @action + ' ' + @objName

				SET @objectExists = null
				
				IF isNull(@checkScriptSQL,'') <> '' 
				BEGIN
					set @checkScriptSQL = 'select @objectExists = case when exists (' + @checkScriptSQL + ') then 1 else 0 end '
					-- print @checkScriptSQL 
					exec sp_executeSQL @checkScriptSQL, N'@objectExists bit OUTPUT',@objectExists = @objectExists OUTPUT
				END
				
				IF @objectExists is null OR (@objectExists = 1 AND @action like 'DROP%') OR (@objectExists = 0 AND @action like 'RECREATE%') 
				BEGIN
					-- print @scriptSQL
					exec sp_executeSQL @scriptSQL
				END
				ELSE
					Print 'Did Not ' + @action	
		
			fetch next from scriptCursor into @objName, @scriptSQL,@XTYPE , @checkScriptSQL
		end

		CLOSE scriptCursor
		DEALLOCATE scriptCursor


	END
END

IF (@action = 'RECREATE' and OBJECT_ID('tempdb..' + @tempTable + '_FULLTEXT') is not null) 
	Print 'Remember that you must also run with action = RECREATEFULLTEXTINDEXES'  

IF (@action = 'RECREATE'  OR @action = 'TEST'  OR @action = 'RECREATEFULLTEXTINDEXES'  )
BEGIN
	SET @sql = 'drop table ' + @tempTable 
	exec (@sql)
END


