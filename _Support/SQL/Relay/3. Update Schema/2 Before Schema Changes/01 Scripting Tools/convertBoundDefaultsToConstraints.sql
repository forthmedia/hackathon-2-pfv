createPlaceHolderObject 'procedure', 'convertBoundDefaultsToConstraints'

GO

/*
WAB 2016-09-29	PROD2016_1190 
				Function to change Bound Defaults (which are a bit old hat) to default constraints 
				Found on internet!
				Used before scripting and recreating the POL tables with identities, since my scripts work better with default constraints!

*/

alter procedure convertBoundDefaultsToConstraints @tablename sysname

AS

BEGIN
	declare @SQL nvarchar (max) = ''
	
	SELECT 
		@sql +=
			'EXEC sp_unbindefault '''
			+ QUOTENAME(s.name) + '.' + QUOTENAME(o.name) + '.' + QUOTENAME(c.name)
			+ '''; ALTER TABLE ' + QUOTENAME(s.name) + '.' + QUOTENAME(o.name)
			+ ' ADD CONSTRAINT [DF_' +  o.name +  '_' + c.name
			+ '] DEFAULT ' 
			+ dbo.regExmatch('(?<=As ).*',text, 1)
			+ ' FOR ' + QUOTENAME (c.name ) + ' ;'
			+ char(10) 
	FROM sys.columns c
	JOIN sys.syscomments t ON c.default_object_id = t.id
	JOIN sys.objects o ON o.object_id = c.object_id
	JOIN sys.schemas s ON s.schema_id = o.schema_id
	WHERE 
	o.name = @tablename and
	OBJECTPROPERTY(c.default_object_id, 'IsDefault') = 1

	IF @sql <> ''
	BEGIN
		print @sql
		exec (@sql)
	END
END


