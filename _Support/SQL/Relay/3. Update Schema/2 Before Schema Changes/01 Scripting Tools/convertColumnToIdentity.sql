createPlaceholderObject 'procedure', 'convertColumnToIdentity'

GO

/*
2016-09-07	WAB	2016_1190 (Put identity columns on POL tables)
			Stored procedure to change column to an identity (and keeping it as the first column) 
2017-02-02	WAB Altered to get tempTableName back from 	dropAndRecreateDependentObjects rather than guessing, since dropAndRecreateDependentObjects now uses a spid specific table name
*/


ALTER  procedure convertColumnToIdentity 
	  @tablename sysname 
	, @columnname sysname 
	
AS	



declare
	  @columnlist nvarchar(max) = ''
	, @createScript nvarchar(max)
	, @sql nvarchar(max)
	, @comma varchar(1) = ''

IF COLUMNPROPERTY (object_id(@tablename),@columnname ,'isIdentity') = 0
BEGIN

	/* create the script for regenerating the basic table */
	select @createScript = createScript from dbo.scriptTable (@tablename,'','') where type = '01 CREATE TABLE'

	exec dropAndRecreateDependentObjects @tablename, 'DROPFULLTEXTINDEXES'

	begin tran
			/* drop (but remember) all dependent objects */
			declare @tempTableName sysname
			exec dropAndRecreateDependentObjects @objectName = @tablename, @action = 'drop', @tempTable = @tempTableName OUTPUT
			
			/* can't have a default on an identity, so make sure there isn't one on the new identiy column */
			set @sql = 'delete from ' + @tempTableName + ' where TYPE = ''DEFAULT'' and columnName = ''' + @columnname + ''' '			
			exec (@sql)

			/* rename old table */
			set @sql = 'sp_rename ' + @tablename + ', ' + @tablename + '_tmp'
			exec (@sql)

			/* recreate a table with the identity */
			set @createScript= REPLACE (@createScript, '['+@columnname + '] int', '['+@columnname + '] int identity(1,1)' )
			exec (@createScript)

			/* now re-insert all the data, remembering to exclude computed columns */ 
 			select 
 				@columnList += @comma + name
 				,@comma = ','
			from 
				syscolumns 	
			where 
				id = OBJECT_id (@tablename)
				and COLUMNPROPERTY(id,name,'IsComputed') = 0
					
			order by colorder		

			set @sql = 
						'alter table ' + @tablename + ' DISABLE TRIGGER ALL ' +
						'set identity_insert ' + @tablename + ' ON
						insert into ' + @tablename + '( '
						+ @columnList + ')
						select ' + @columnList + '
						from ' + @tablename + '_tmp
						set identity_insert ' + @tablename + ' OFF ' +
						'alter table ' + @tablename + ' ENABLE TRIGGER ALL ' 

			exec (@sql)

			/* recreate all the triggers and stuff */
			exec dropAndRecreateDependentObjects @tablename, 'recreate'

			
			/* drop the old (renamed) table)  */
			set @sql = 'drop table ' + @tablename + '_tmp'
			exec (@sql)
						
	commit
		
		exec dropAndRecreateDependentObjects @tablename, 'RECREATEFULLTEXTINDEXES'
				
END
ELSE
BEGIN
	Print @tableName + '.' + @columnName + ' is already an identity'
END

GO

