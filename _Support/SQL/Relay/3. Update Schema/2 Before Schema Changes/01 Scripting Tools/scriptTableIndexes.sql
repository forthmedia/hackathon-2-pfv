createPlaceHolderObject 'tablefunction','scriptTableIndexes'
GO

ALTER FUNCTION scriptTableIndexes (
	@tableName sysname,   /* a table or view */
	@TargetTableName sysname 
)
RETURNS @returnTable TABLE 
(createscript nvarchar(max),indexName sysname,type sysname, checkExistsScript nvarchar(max))

AS

/*
WAB 2014-11-03 	A function to return the CREATE script for the indexes on any table (or view)
				Guts of function from the internet
WAB 2014-12-02 	A few fixes.  Was picking up some index without a name (which I don't understand, something todo with a HEAP!)				
				Was getting index name wrong when more than one column in it
				Changed column name in the returned table objectName to indexName
				Added support for @tableName to be passed in in table.columname format.  Just gets indexes which reference that column
WAB 2015-04-29	Added support for filtered indexes
WAB 2016-01-27	Alter so that does not pick up fulltext index if column is specified and is not in that index
WAB 2016-03-22	Alter so that picks up full text indexes on views which reference table specified
*/


BEGIN

	/* 	Support for @tableName to be in table.column notation 
		Allows us to just get the indexes which depend upon a specific column

	*/
	declare 
		@originalTableName sysname = @tableName,
		@columnName sysname,
		@findDot int,
		@hasFullTextIndex bit
	
	SET @findDot = CHARINDEX('.',@originalTableName,1)	
		
	IF @findDot <> 0	
	BEGIN
		Select @columnName = RIGHT (@originalTableName,len(@originalTableName)-@findDot), @tableName = LEFT (@originalTableName,@findDot-1)
	END


	IF @targetTableName is null
		SET @targetTableName  = @tablename

	DECLARE @index_id INT
	DECLARE @index_name SYSNAME
	DECLARE @is_unique BIT
	DECLARE @type_desc NVARCHAR(50)
	DECLARE @fill_factor INT
	DECLARE @script_body NVARCHAR(2000)
	DECLARE @checkExists_script_body NVARCHAR(2000)
	DECLARE @filter_definition NVARCHAR(max)
	DECLARE index_cursor CURSOR LOCAL FOR

		SELECT DISTINCT
			I.index_id,
			I.name,
			I.is_unique,
			I.type_desc,
			I.fill_factor,
			I.filter_definition
			
		FROM sys.indexes I
			JOIN sys.objects T ON I.object_id = T.object_id
			JOIN sys.index_columns IC ON  I.object_id = IC.object_id AND I.index_id = IC.index_id
			JOIN sys.columns C ON IC.object_id = C.object_id AND IC.column_id = C.column_id

		WHERE
			I.is_primary_key = 0
--			AND I.is_unique = 0
			AND I.is_unique_constraint = 0
			AND T.is_ms_shipped = 0
			AND T.name = @tablename
			and (	T.type = 'V'
						OR
					(T.type = 'U' and @columnName is null)
						OR
					(T.type = 'U' and @columnName is not null and @columnName = c.name)
				)
			
			and i.name is not null

	OPEN index_cursor
	FETCH NEXT FROM index_cursor INTO @index_id,@index_name,@is_unique,@type_desc,@fill_factor, @filter_definition

	WHILE (@@FETCH_STATUS = 0)

		BEGIN


			SET @script_body = 'CREATE ' + CASE @is_unique WHEN 1 THEN ' UNIQUE ' ELSE '' END + ' ' + @type_desc + ' INDEX [' + @index_name + ']'
			
			SET @script_body += ' ON [dbo].[' + @TargetTableName + '] ('

			SET @script_body += (

				SUBSTRING(
					(
						SELECT 
							','+ColumnDefinition
						FROM (
							SELECT
								'[' + C.name + '] ' + CASE WHEN IC.is_descending_key = 0 THEN 'ASC' ELSE 'DESC' END AS ColumnDefinition
							FROM sys.indexes I
							JOIN sys.index_columns IC ON  I.object_id = IC.object_id AND I.index_id = IC.index_id
							JOIN sys.columns C ON IC.object_id = C.object_id AND IC.column_id = C.column_id
							JOIN sys.objects T ON I.object_id = T.object_id
							WHERE
								I.is_primary_key = 0
								-- AND I.is_unique = 0
								AND I.is_unique_constraint = 0
								AND T.is_ms_shipped = 0
								AND T.name = @tablename
								AND I.index_id = @index_id
								AND IC.is_included_column = 0
						) AS COLS
						FOR XML PATH('')
					),2,1000)
			)

			SET @script_body += ')'
			
			IF (SELECT COUNT(*) AS included_columns
							FROM sys.indexes I
							JOIN sys.index_columns IC ON  I.object_id = IC.object_id AND I.index_id = IC.index_id
							JOIN sys.columns C ON IC.object_id = C.object_id AND IC.column_id = C.column_id
							JOIN sys.objects T ON I.object_id = T.object_id
							WHERE
								I.is_primary_key = 0
								-- AND I.is_unique = 0
								AND I.is_unique_constraint = 0
								AND T.is_ms_shipped = 0
								AND T.name = @tablename
								AND I.index_id = @index_id
								AND IC.is_included_column = 1) > 0
				BEGIN
					SET @script_body += ' INCLUDE ( '

					SET @script_body += (

						SUBSTRING(
							(
								SELECT 
									','+ColumnDefinition
								FROM (
									SELECT
										'[' + C.name + '] ' AS ColumnDefinition
									FROM sys.indexes I
									JOIN sys.index_columns IC ON  I.object_id = IC.object_id AND I.index_id = IC.index_id
									JOIN sys.columns C ON IC.object_id = C.object_id AND IC.column_id = C.column_id
									JOIN sys.objects T ON I.object_id = T.object_id
									WHERE
										I.is_primary_key = 0
										-- AND I.is_unique = 0
										AND I.is_unique_constraint = 0
										AND T.is_ms_shipped = 0
										AND T.name = @tablename
										AND I.index_id = @index_id
										AND IC.is_included_column = 1
								) AS COLS
								FOR XML PATH('')
							),2,1000)
					)

					SET @script_body += ' ) '
				END

			IF @filter_definition is not null
			BEGIN
				SET @script_body += ' WHERE ' + @filter_Definition
			END	


			SET @script_body += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON'
			
			IF @fill_factor > 0
				BEGIN
					SET @script_body += ', FILLFACTOR = ' + CONVERT(NVARCHAR(10),@fill_factor)
				END
			
			SET @script_body += ') ON [PRIMARY]'
			
			SET @checkExists_script_body = 'Select 1 from sys.indexes where name = ''' + @index_name + ''' AND OBJECT_NAME(object_id) = ''' + @tablename + '''' 

			insert into @returnTable (createScript,indexName,type, checkExistsScript)
			VALUES
			(@script_body,@index_name,'Index',@checkExists_script_body)

			FETCH NEXT FROM index_cursor into @index_id,@index_name,@is_unique,@type_desc,@fill_factor, @filter_definition

		END

	CLOSE index_cursor
	DEALLOCATE index_cursor



	/*	WAB 2016-03-22 
		We weren't picking up full text indexes on views which referenced the given table
		Find full text tables and pop names in a table
	*/
	declare @fullTextIndexNames as Table (name sysname)

	insert into @fullTextIndexNames (name)
	/* index on actual table (or view) */
	select 
		object_name(fic.object_id)
	from
		sys.fulltext_index_columns fic
			inner join 
		sys.columns c on c.[object_id] = fic.[object_id]
    					and c.[column_id] = fic.[column_id]
	where 
		object_name (c.[object_id]) = @tablename
		and (@columnName is null or c.name = @columnName)

	union 
	
	/* indexes on dependent views */
	select 
		object_name(ftic.object_id )
	from
		sys.sql_dependencies dep
			inner join
		sys.fulltext_index_columns	 ftic on ftic.object_id =  dep.object_id
	WHERE
		object_name(referenced_major_id) = @tablename  and (@columnName is null or COL_NAME(referenced_major_id, referenced_minor_id) = @columnName)


	/* now generate the scripts for the full text indexes found */
	insert into @returnTable 
	(createScript,indexName,type,checkExistsScript)
	select
		dbo.scriptFullTextIndex (name),
		name,
		'FULLTEXTINDEX',
		'select 1 from sys.fulltext_indexes  where object_id = object_id(''' + name + ''')'
	from 
		@fullTextIndexNames
				
		


RETURN

END


