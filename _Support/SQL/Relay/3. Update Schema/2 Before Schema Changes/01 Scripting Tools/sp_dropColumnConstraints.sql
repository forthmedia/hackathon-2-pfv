IF EXISTS (SELECT 1 from sysobjects where name = 'sp_dropColumnConstraints')
	DROP PROCEDURE sp_dropColumnConstraints

GO

CREATE PROCEDURE sp_dropColumnConstraints @tablename sysname, @columnName sysname
AS
/*
WAB 2014-12-02  
Drops constraints for a column
Split out from sp_dropColumn and now called by it

Also useful when adding a NOT NULL column to a table containing existing data
Add column with a default and then drop the default contraints

*/

	declare @constraintName sysname, @sql nvarchar(max) = ''

	DECLARE constraintCursor CURSOR  FOR
	
	select 
		 d.name 
	from sys.columns  c 
			left join 
			sys.default_constraints  d on d.parent_object_id = c.object_id and c.column_id = d.parent_column_id
	where 
		c.name = @columnName and OBJECT_NAME(c.object_id) = @tableName
		and d.name is not null
	UNION
	select 
		chk.name
	from 
		sys.check_constraints chk
			inner join 
		sys.columns col   on chk.parent_object_id = col.object_id and col.column_id = chk.parent_column_id
			inner join 
		sys.tables st   on chk.parent_object_id = st.object_id
	where 
		 st.name = @tablename
		 and col.name = @columnName

		OPEN constraintCursor
		FETCH NEXT FROM constraintCursor into @constraintName
		while @@FETCH_STATUS = 0
		begin
			SET @SQL = 'alter table ' + @tablename + ' drop constraint ' + @constraintName + ';'
			exec sp_executeSQL @SQL
			Print 'Constraint '  + @constraintName + ' dropped'
		
			FETCH NEXT FROM constraintCursor into @constraintName 
		end

		CLOSE constraintCursor
		DEALLOCATE constraintCursor

