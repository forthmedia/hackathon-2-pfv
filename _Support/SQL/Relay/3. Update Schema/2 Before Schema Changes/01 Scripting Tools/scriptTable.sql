createPlaceHolderObject @ObjectType = 'TableFunction',	@objectName = 'ScriptTable'

GO

ALTER FUNCTION ScriptTable (@tableName sysname,@createSuffix sysname,@renameSuffix sysname)
RETURNS @returnTable TABLE 
(createscript nvarchar(max),type nvarchar (50), objectName sysname, renameScript nvarchar(max), checkExistsScript nvarchar(max), dropScript nvarchar(max))

AS

/* 
RW for WAB 2014-10 
Needed a script which could be used as part of a process for changing the POL tables to use identity Columns (which involves recreating the table from scratch dynamically)
Not used yet
WAB 2014-12-02 update because of change in return from scriptTableIndexes()
WAB 2015-01-26 ditto (returns indexName not objectName)  
WAB 2016-09-06	PROD_2016_1190 (Put identity columns on POL tables) 
				Project to change POL tables actually carried out, so resurrected this code.  
				All the dependent objects can now be generated from scriptDependentObjects
				Now called from 'convertColumnToIdentity' procedure (and infact I only use the create statement - the dependent objects are dealt with by dropAndrecreateDependantObjects)
WAB 2016-09-20	Add support for computed columns
*/

BEGIN
DECLARE @TargetTableName NVARCHAR(200) = @tableName + @createSuffix

-- SET CONCAT_NULL_YIELDS_NULL OFF

insert into @returnTable (createScript,type,objectname,renamescript,checkExistsScript,dropScript)
SELECT
	'CREATE TABLE [' + @TargetTableName + '] (' + O.list + ')'  AS [Script]
	,'01 CREATE TABLE' AS ScriptType
	, @tablename as objectName
	, ''
	, ''
	, ''
FROM sys.objects SO
CROSS APPLY (
	SELECT ' [' + column_name + '] ' +
		CASE WHEN COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA+'.'+TABLE_NAME),COLUMN_NAME,'IsComputed') = 0 THEN
			 data_type + CASE data_type
			WHEN 'bit'
				THEN ''
			WHEN 'int'
				THEN ''
			WHEN 'datetime'
				THEN ''
			WHEN 'sql_variant'
				THEN ''
			WHEN 'text'
				THEN ''
			WHEN 'ntext'
				THEN ''
			WHEN 'xml'
				THEN ''
			WHEN 'numeric'
				THEN '(' + CAST(numeric_precision AS VARCHAR) + ', ' + CAST(numeric_scale AS VARCHAR) + ')'
			WHEN 'decimal'
				THEN '(' + CAST(numeric_precision AS VARCHAR) + ', ' + CAST(numeric_scale AS VARCHAR) + ')'
			ELSE COALESCE('(' + CASE 
						WHEN character_maximum_length = - 1
							THEN 'MAX'
						ELSE CAST(character_maximum_length AS VARCHAR)
						END + ')', '')
			END + ' ' + CASE 
			WHEN EXISTS (
					SELECT object_id
					FROM sys.columns
					WHERE object_name(object_id) = @tablename
						AND NAME = column_name
						AND COLUMNPROPERTY(object_id, NAME, 'IsIdentity') = 1
					)
				THEN 'IDENTITY(' + CAST(ident_seed(SO.NAME) AS VARCHAR) + ',' + CAST(ident_incr(SO.NAME) AS VARCHAR) + ')'
			ELSE ''
			END + ' ' + (
			CASE 
				WHEN IS_NULLABLE = 'No'
					THEN 'NOT '
				ELSE ''
				END
			) + 'NULL ' 
			/*  WAB 2016-09-30 remove defaults from script, now come as separate line items from scriptDependentObjects().  Note that bound defaults must be changed to default constraints first
				 + CASE 
				WHEN information_schema.columns.COLUMN_DEFAULT IS NOT NULL and information_schema.columns.COLUMN_DEFAULT not like '%create default%'
					THEN 'DEFAULT ' + information_schema.columns.COLUMN_DEFAULT
				ELSE ''
				END 
			*/
			
		ELSE
			'AS ' + (SELECT definition FROM sys.computed_columns WHERE object_id = OBJECT_ID(table_name) and name = column_name)
		END
			+ ', '
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE table_name = SO.NAME
	ORDER BY ordinal_position
	FOR XML PATH('')
	) O(list)
WHERE SO.type = 'U'
AND SO.NAME = @tablename



UNION

-- Views, but will include indexes for indexed views
-- Indexes
-- Triggers
-- Primary Key
-- Foreign Key
select 
	createScript
	, CASE 
		WHEN xType = 'PK' THEN '02 PRIMARY KEY'
		WHEN xType = 'FK' THEN '03 FOREIGN KEY'
		WHEN xType = 'D' THEN '04 DEFAULT CONSTRAINT'
		WHEN xType = 'C' THEN '04 CONSTRAINT'
		WHEN xType = 'V' THEN '05 VIEW'
		WHEN xType = 'I' THEN '06 INDEX'
		WHEN xType = 'TR' THEN '07 TRIGGER'
		ELSE ''
	  END	
	, objectName
	, 'sp_rename ''' + objectName + ''',''' + objectName + @renameSuffix + ''',''VIEW'''
	, checkExistsScript
	, dropScript
 from  dbo.scriptDependentObjects(@tablename,'')
 where xtype in ('V','I','TR','PK','FK','C','D')
  and type not in ('TR(Referenced)')


ORDER BY ScriptType




return

END


