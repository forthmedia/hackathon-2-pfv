IF EXISTS (SELECT 1 from sysobjects where name = 'sp_dropColumn')
	DROP PROCEDURE sp_dropColumn

GO

CREATE PROCEDURE sp_dropColumn @tablename sysname, @columnName sysname
AS
/*
WAB 2014-10-09
Deletes a column from a table if it exists
Deals with deleting the default at the same time

WAB 2014-12-02  Split out the dropping of contraints into a separate stored procedure
*/

IF exists (select * from information_schema.columns where table_name = @tablename and column_name = @columnName)
BEGIN

	exec sp_dropColumnConstraints @tablename, @columnName

	declare @sql nvarchar(max)
	
	SET @SQL = 'alter table ' + @tablename + ' drop column ' + @columnName
	exec sp_executeSQL @sql
	Print 'Column ' + @tablename + '.' + @columnName + ' Removed'

END
ELSE
BEGIN
	Print 'Column ' + @tablename + '.' + @columnName + ' Does not Exist, not Dropped'
END

