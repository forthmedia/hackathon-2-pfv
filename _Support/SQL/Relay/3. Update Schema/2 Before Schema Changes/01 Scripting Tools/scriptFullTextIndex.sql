createPlaceHolderObject 'function','scriptFullTextIndex'
GO

ALTER Function dbo.scriptFullTextIndex (@tablename sysname)
returns nvarchar(max)
AS
BEGIN

DECLARE @Catalog NVARCHAR(128),
		@SQL NVARCHAR(MAX),
		@COLS NVARCHAR(4000),
		@Owner NVARCHAR(128),
		@Table NVARCHAR(128),
		@Object_ID INT,
		@Index_ID INT,
		@NL CHAR(2)  = CHAR(13)+CHAR(10)

				SELECT	
						@object_id = i.object_id
						,@Owner = u.Name
						,@Table = t.Name
						,@Catalog = c.name
						 ,@index_id = unique_index_id
				FROM	sys.objects as t
				JOIN	sysusers as u
					ON	u.uid = t.schema_id
				JOIN	sys.fulltext_indexes i
					ON	t.object_id = i.object_id
				JOIN	sys.fulltext_catalogs c
					ON i.fulltext_catalog_id = c.fulltext_catalog_id
				WHERE	t.Name =  @tablename

			IF @object_id is not null
			BEGIN

				-- Script Fulltext Index
				SELECT	@COLS = NULL,
						@SQL = 'CREATE FULLTEXT INDEX ON ' + QUOTENAME(@Owner)+'.'+QUOTENAME(@Table)+' ('  +@NL
				-- Script columns in index
				SELECT	@COLS = COALESCE(@COLS+',','') + c.Name + ' Language ' + CAST(Language_id as varchar) +' '+@NL
				FROM	sys.fulltext_index_columns as fi
				JOIN	sys.columns as c
					ON	c.object_id = fi.object_id
						AND c.column_id = fi.column_id
				WHERE	fi.object_id = @Object_ID

				-- Script unique key index
				SELECT	@SQL = @SQL + @COLS + ') ' + @NL + 'KEY INDEX '+ i.Name + @NL+
						'ON ' + @Catalog + @NL +
						'WITH CHANGE_TRACKING ' + fi.change_tracking_state_desc + @NL 
				FROM	sys.indexes as i
				JOIN	sys.fulltext_indexes as fi
					ON	i.object_id = fi.object_id
				WHERE	i.Object_ID=@Object_ID
						AND Index_Id = @Index_ID

			END

	return @SQL
END