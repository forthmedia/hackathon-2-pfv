/*
WAB 2017-03-14
Drop the backup views which have been created by createFlagView
No longer needed (probably never were)
Caused problems when using dropAndRecreateDependentObjects:
	They have been renamed from something else and the object_definition() for their associated triggers refers back to the original name of the object
	This causes an issue when we try to recreate the triggers and they are created on the wrong object (or more likely fail to create, because the trigger already exists on that object)
*/

declare @sql nvarchar (max) =''

select 
	@sql += 'drop view ' + name + char(10)
from 
	sysobjects 
where
	name like 'v%_bak'
	and xtype = 'v'

exec (@sql)