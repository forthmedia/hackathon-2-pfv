/* 
	NJH 2016/01/05 - removed friendly name.. select description on it's own and changed the select for the label to get entityname if label is null 
	NJH 2016/06/07  - JIRA PROD2016-342 - added another column to deal with plural labels.
	NJH 2016/06/27	- JIRA ROD2016-1304 - add API meta data
*/
if exists (select 1 from sysObjects where name = 'schemaTable')
	DROP view schemaTable

GO

CREATE VIEW [dbo].[schemaTable]
AS
select entitytypeid, entityname,NameExpression, uniquekey, Description, mnemonic, FlagsExist, ScreensExist, CustomEntity, [in_vEntityName], label, 
	case 
		when label='person' then 'People' 
		when right(label,1) = 'y' then substring(label,1,len(label)-1) + 'ies' 
		when right(label,1) = 's' then label 
		else label+'s' 
	end as [label_plural],
	apiActive,
	apiGet,
	apiPost,
	apiPut,
	apiDelete
from (
SELECT     	entitytypeid 
			, entityName 
			, NameExpression 
			, dbo.getTablePrimaryKey(entityName) AS uniquekey 
			, Description
			, FlagsExist
			, mnemonic
			, ScreensExist 
			, CustomEntity
			, [in_vEntityName]
			, isNull(label,UPPER(LEFT(entityName,1))+LOWER(SUBSTRING(entityName,2,LEN(entityName)))) as label --when autocreating the label from the entityName we autocapitalise the first letter
			,apiActive
			,apiGet
			,apiPost
			,apiPut
			,apiDelete
FROM        dbo.schemaTableBase
) as schemaTableBase
GO