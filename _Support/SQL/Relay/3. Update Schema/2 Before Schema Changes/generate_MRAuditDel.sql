IF EXISTS (SELECT * FROM [dbo].[sysobjects]	WHERE ID = object_id(N'[dbo].[Generate_MRauditDel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[Generate_MRauditDel] 
GO

CREATE PROCEDURE [dbo].[Generate_MRauditDel] 
						@TableName	sysname
						
			as

/*
SKP
Creates a Del Table
Called by Generate_MRaudit
Does a Select Into from the old table, but deals with getting rid of any identity columns

WAB 2011/02/01 Adjusted so that it will recreate a del table if it is out of step with the main table
				Included a bit of error handling/rollback if problem occurs while copying data from the old table
WAB 2013-06-19  deal with error if existing del table has a column which is no longer in the parent table 
WAB 2013-12-10  Deal with del tables which are reference in views
WAB 2014-02-18	Moved position of an begin Tran so that rollback did not leave a cursor knocking around
NJH	2015/07/03	Re-create any existing indexes on the del table after the table has been recreated.
WAB 2016-01-05	Update data types to sysname and nvarchar(max)
WAB 2016-04-06	Protect against _temp table existing from a previous failed run
WAB 2016-04-25	Make sure that del table created as dbo
WAB 2016-06-09	Replaced rather rudimentary code which dealt with dependent views/indexes on del table during drop and recreate with dropAndRecreateDependentObjects 
				Was throwing errors when there were two	views dependent on del table and also dependent on each other 
				(a rather unusual scenario involving vEntityName (the original version) and a connector view 		
				
*/

set nocount on

declare @columnName sysname, 
		@sql nvarchar (max), 
		@columnList nvarchar (max), 
		@isidentity as bit,
		@delTableExists as bit,
		@delTableCorrect as bit,
		@tempDelTableName as sysname,
		@DelTableName as sysname,
		@status int


select @DelTableName = @tablename  +  'del'

/* Is there already a del table? */
SELECT @delTableExists = case when exists (select 1 FROM information_schema.columns WHERE table_name=@DelTableName) then 1 else 0 end

/* if there is already a del table then does it match the base table? */
IF @delTableExists = 1
BEGIN
	
	select @delTableCorrect = case when exists (
		/* This query will bring back a result if the del table does not match the original table */  
		select t1.column_name 
			--t1.column_name, t2.column_name,t1.ordinal_position,t2.ordinal_position
		
		 from 
		information_schema.columns t1
		left join 
		information_schema.columns t2 on 
				t1.table_name + 'del' = t2.table_name 
			and t1.column_name = t2.column_name 
			and t1.data_type = t2.data_type 
			and isnull(t1.character_maximum_length,0) = isnull(t2.character_maximum_length,0)
			/* This matches on ordinal position taking into account that there can be gaps in postion */
			and (select count(1) from information_schema.columns tt1 where tt1.table_name = t1.table_name and tt1.ordinal_position <= t1.ordinal_position) = (select count(1) from information_schema.columns tt2 where tt2.table_name = t2.table_name and tt2.ordinal_position <= t2.ordinal_position)
		where t1.table_name = @TableName
		and t2.column_name is null
	) then 0 else 1 end
		

END


IF @delTableExists = 0 OR @delTableCorrect = 0
		BEGIN	
		/* 
			Get a query of all the columns to make an insert statement (a quick way of making a new table)
			Deal with not having the identity column in the del table
		
		*/
		declare myCursor cursor for 
		select 
			  column_name, 
			  columnproperty(object_id(table_name), column_name,'IsIdentity') as isIdentity
			  from information_schema.columns where table_name = @tableName  order by ordinal_position

			  select @sql = ''

		OPEN myCursor
		-- Perform the first fetch.
		FETCH NEXT FROM myCursor into @columnName, @isIdentity
		-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
		WHILE @@FETCH_STATUS = 0
		BEGIN
			  IF @sql <> '' 
					SELECT @sql = @sql + ','
		            
			  IF @isIdentity = 1
					BEGIN
						  select @sql = @sql + 'convert(int, ' + @columnName + ') as ' + @columnName 
					END
			  ELSE
					BEGIN
						  select @sql = @sql + @columnName
					END   
		      
		      
			  FETCH NEXT FROM myCursor into @columnName, @isIdentity
		END

		CLOSE myCursor
		DEALLOCATE myCursor


		/* If the del table already exists then we create a temporary table first and then copy the old data into it */
		IF @delTableExists = 1
		BEGIN
			select @tempDelTableName = @DelTableName + '_Temp'
			/* WAB 2016-04-06 sometimes we have a table left over from a failed run, 
				check and delete if necessary */
			IF object_Id(@tempDelTableName) is not null 
			BEGIN
				declare @deleteSQL nvarchar(max) = 'drop table ' + @tempDelTableName
				exec (@deleteSQL )
			END
		END	
		ELSE
		BEGIN
			select @tempDelTableName = @DelTableName
		END

		SELECT @sql = 'SELECT ' + @Sql + ' into ' + 'dbo.' + @tempDelTableName + ' from [' + @tablename + '] where 1 = 0 '
		exec (@sql)

		IF @delTableExists = 1
			BEGIN
			
				/* Copy data from existing del table to new one 
					need to get list of columns in the existing del table
				*/
					declare myCursor cursor for 
					select 
						  column_name
					from 
						information_schema.columns 
					where 
							table_name = @deltableName  
						and column_name in (select column_name from information_schema.columns where table_name = @tableName  )  /* WAB 2013-06-19 added this line, to handle cases when the del table has, for some reason, got a column which the parent table does not have */
					order by ordinal_position


				  select @columnList = ''

					OPEN myCursor
					-- Perform the first fetch.
					FETCH NEXT FROM myCursor into @columnName
					-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
					WHILE @@FETCH_STATUS = 0
					BEGIN
						  IF @columnList <> '' 
								SELECT @columnList = @columnList + ','
					            
						  select @columnList = @columnList + @columnName
					      
					      
						  FETCH NEXT FROM myCursor into @columnName
					END

					CLOSE myCursor
					DEALLOCATE myCursor



				begin tran

        			exec dropAndRecreateDependentObjects @delTableName, 'drop'

						set @sql  ='DROP table ' + @DelTableName
						exec @status=sp_executesql @stmt= @sql
						IF @status <> 0 GOTO ErrorTrap
						
						EXEC @status = sp_rename	@tempDelTableName ,@DelTableName ,'Object'  
						IF @status <> 0 GOTO ErrorTrap
	
						print @delTableName + ' recreated'
				
			        exec dropAndRecreateDependentObjects @delTableName, 'recreate'

				COMMIT TRANSACTION
				
				GOTO TheEnd
				
				/*===========================================================================================*/
				-- Error Trap
				/*===========================================================================================*/
				ErrorTrap:

				   ROLLBACK TRANSACTION
					set @sql  ='DROP table ' + @tempDelTableName
					exec @status=sp_executesql @stmt= @sql



			END
		ELSE	
			print @delTableName + ' created'
END
ELSE
	print @delTableName + ' exists and is correct'


TheEnd:
