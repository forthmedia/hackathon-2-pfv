/*
Create Trigger for keeping a column in the base table up to date
*/

IF exists (select 1 from sysobjects where name = 'DefaultPhraseUpdate')
	DROP TRIGGER DefaultPhraseUpdate
GO

create trigger [dbo].[DefaultPhraseUpdate] 
on [dbo].[Phrases]
for update, insert

AS
/* WAB 2010/02/02 Experiment 
For an entity phrase.
If there is a column in the entity table with the same name as the phraseTextID plus suffix _defaultTranslation then keep it up to date with the default translation of country 0
WAB 2010/12  Put into 8.3 although not actively used
WAB 2011/1/24  tried implementing on the training certification table and required a bit of a query re-write
*/

if exists (
	select 1 from 
		inserted i 
			inner join 
		phrases p  on p.ident = i.ident
			inner join	
		phraseList pl on p.phraseid = pl.phraseid and pl.entityTypeID <> 0

)


BEGIN

	/* Get distinct values of entityTypeID and phraseTextID */
	
	select phraseid, defaultforthiscountry into #inserted from inserted 	/* can't refer to inserted in exec sp_execuresql, just need the phraseid and can't use the phrasetext anyway  */

	DECLARE @tablename nvarchar(200), @columnname nvarchar(200), @uniqueKey nvarchar(200), @phraseText nvarchar(4000), @entityID int , @entityTypeID int , @phraseTextID nvarchar(100),@colLength int
	DECLARE @SQL nvarchar (4000)

	DECLARE n_cursor CURSOR FOR
		select distinct sc.table_name, sc.column_name, uniqueKey, s.entityTypeID, pl.phrasetextID, sc.character_maximum_Length
			from 
				inserted i 
					inner join 
				phrases p  on p.ident = i.ident
					inner join	
				phraseList pl on p.phraseid = pl.phraseid and pl.entityTypeID <> 0
					inner join 
				schematable s on s.entitytypeid = pl.entitytypeid
					inner join 
				information_schema.columns sc on sc.table_name = s.entityname and sc.column_name = pl.phrasetextID + '_defaultTranslation'
			where isnull(uniqueKey,'') <> '' 	


	OPEN n_cursor
	
	FETCH NEXT FROM n_cursor
		INTO @tablename , @columnname,  @uniqueKey, @entityTypeID,@phraseTextID,@colLength

		DECLARE @conversionCode nvarchar (1000);
		
		if @colLength = -1 --BF-540 RJT This occures with nvarcharmax, in this case we don't need to worry about trunkation anyway
			select @conversionCode='phraseText ';
		else	
			select @conversionCode='convert(nvarchar(' + convert(varchar,@colLength) +'),phraseText) ';
		
		WHILE @@FETCH_STATUS = 0
			BEGIN
				select @sql = '
					update ' + @tablename + ' set ' + @columnname + '=' + @conversionCode +
					'from ' + @tablename + ' ent
					inner join vphrases p	
						on ent.' + @uniqueKey + ' = p.entityid  
							and p.entityTypeID  = ' + convert(varchar,@entityTypeID) + '
							and p.phraseTextID = ''' + @phraseTextID+''' 
					inner join #inserted	i
						on i.phraseid = p.phraseid and i.defaultforthiscountry = 1
				where
					p.countryid = 0
					and 	p.defaultforthiscountry = 1	
				'						

				exec sp_executesql @sql
				   FETCH NEXT FROM n_cursor
		INTO @tablename , @columnname, @uniqueKey, @entityTypeID,@phraseTextID,@colLength

			END

	CLOSE n_cursor
	DEALLOCATE n_cursor
	
END