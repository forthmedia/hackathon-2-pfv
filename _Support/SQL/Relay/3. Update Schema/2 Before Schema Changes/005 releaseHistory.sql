/*
WAB 2013-06-18 A table to record which sql scripts have been applied to a database
				And a stored procedure to insert/update items 
*/

if not exists (select 1 from sysobjects where name = 'releaseHistory')
BEGIN
	create table releaseHistory
	(Type  varchar(20),
	name varchar(200),
	revision int,
	lastupdated datetime
	)
END

GO


if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='isOK')
alter table releaseHistory add isOK bit not null default(1)

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='functionRevision')
alter table releaseHistory add functionRevision int null

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='coldFusionInstanceID')
alter table releaseHistory add coldFusionInstanceID int null

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='created')
BEGIN
	alter table releaseHistory add created datetime not null default getDate()
	declare @sql nvarchar(max) = 'update releaseHistory set created = lastUpdated where created > getDate()-.1 and lastupdated is not null'
	exec (@sql)
END	

GO

if not exists (select 1 from information_schema.columns where table_name  = 'ReleaseHistory' and column_name='lastupdated' and column_default is not null)
BEGIN
	update releaseHistory set lastupdated = created where lastupdated is null
	alter table releaseHistory alter column lastupdated datetime not null 
	ALTER TABLE releaseHistory ADD CONSTRAINT DF_releaseHistory_lastupdated DEFAULT (getDate()) FOR lastupdated;
END	

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='metaData')
alter table releaseHistory add metaData nvarchar(max) null
GO

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='databaseName')
alter table releaseHistory add databaseName nvarchar(max) null
GO

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='ID')
alter table releaseHistory add ID int not null identity
GO

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='revision' and data_type='nvarchar' and CHARACTER_MAXIMUM_LENGTH=500)
Begin
	alter table releaseHistory alter column revision nvarchar(500)
End
GO

if not exists (select 1 from information_schema.columns where table_name='ReleaseHistory' and column_name='name' and data_type='nvarchar' and CHARACTER_MAXIMUM_LENGTH=500)
Begin
	alter table releaseHistory alter column name nvarchar(500)
End
GO


if exists (select 1 from sysobjects where name = 'upsertReleaseHistory')
	drop procedure upsertReleaseHistory
GO


	create procedure upsertReleaseHistory
		@Type varchar(20),
		@name varchar (500),
		@revision varchar (500)
	AS

	IF EXISTS (select 1 from releaseHistory where type = @type and name = @name)
		BEGIN
			Update releaseHistory
			SET revision = @revision, lastupdated = getdate()
			where type = @type and name = @name
		END
	ELSE	
		BEGIN
			Insert into releaseHistory
			(Type,name,revision,lastupdated,created)
			values (@type,@name,@revision,getdate(),getDate())
		END