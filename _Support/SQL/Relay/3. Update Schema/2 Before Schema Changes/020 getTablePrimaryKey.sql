createPlaceHolderObject 'function', 'getTablePrimaryKey'

GO

alter FUNCTION [dbo].[getTablePrimaryKey] (@TableName sysname)  

returns sysname AS

/*
Author WAB  
Date  2010/09/14 
Gets the name of the primary Key column of a table
Assumes a single column primary key
2016-06-27	WAB Implement a quicker query.  Problems occurring because this function used in schemaTable view (perhaps we should just hard code most primarykeys in schemaTableBase)
*/
begin

declare @result sysname


SELECT @result =COLUMN_NAME
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + CONSTRAINT_NAME), 'IsPrimaryKey') = 1
AND TABLE_NAME = @TableName

return @result
END
