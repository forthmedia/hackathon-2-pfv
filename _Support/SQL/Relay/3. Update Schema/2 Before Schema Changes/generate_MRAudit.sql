createPlaceholderObject 'procedure', 'Generate_MRaudit'

GO

ALTER  procedure Generate_MRaudit 
						@TableName	varchar(50),
						@mnemonic	VARCHAR(5) = null,
						@ModEntityDefOn as bit =1,
						@ModTriggerOn as bit =0,
						@entityTypeID int  = null   -- WAb 2011/03/10 optional, but needed if want to create a record in schemaTable
						
			
			as


/**
			
			8/11/04 Modified by WAB to deal with complex flag tables.  
			Probably won't deal with complex flags with a multivalue flagtable
			2008/10/?  GCC mods to	deal with sql 2005 etc where columns_updated not available to called sp
			2008/11/24  WAB mod to above to make @columns_updated binary(20) (enough for table with 160 columns)
			2009/04/22  WAB deal with flag tables being either normal or complex			
			2009/04/22  WAB don't need entries in modentitydef for normal flag tables
			2010/10/19  SKP Move Del table creation to seperate stored procedure
			2011/03/10 WAB So that we keep all entityTypeIDs in synch, we now have to pass in an entityTypeID to create a record in the schema table.
						ModRegisterTriggerAction altered at same time so that it does not (usually) require and entry in schematable  
			
			2012-03-08	WAB	Fixed Major Bug!  When run for a second time was adding duplicate rows into modentityDef
			2012-09-26	WAB was turning on trigger for lastupdated and lastudpatedby columns.  
						Was setting modtrigger.flagTable column to 5 for booleanflagdata - should have been 1  
			2013-09-25	WAB	CASE 437153 Was updating FieldMonitor to 1 (ON) even if had previously been set to 0 (OFF)
							Also added a few more fields which should never be monitored	
			2014-03-26 WAB still trying to prevent modentityids overwriting each other
			2014-09-15	NJH	make mnemonic non-required. Get the value from schemaTableBase table if it is set. Don't update schemaTableBase with passed in value
			2015-10-23	WAB	Added support for deleting all associated flags when an entity is deleted (and doind a protected flag check)
			2016-06-01	WAB For PROD-1124 (SODmaster API). Add support for tables without single column primary keys. Instead, identity column specified in schemaTable.uniqueKey 
			*/
			

			set nocount on
			
			declare @sqlcmd varchar(5000),
				@UniqueCol sysname,
				@flagTable int  ,
				@fieldname    varchar(100) ,
				@cnt int,
				@startid int,
				@dummy int
			
			if (select count(convert(sysname,c.name))
			from syscolumns c
				inner join sysindexes i on i.id = c.id
							and (i.status & 0x800) = 0x800
			 	inner join syscolumns c1 on c1.id = c.id
							and c1.colid <= i.keycnt
			where c.id = object_id(@tableName)
			  and c.name = index_col (@tableName, i.indid, c1.colid)) = 1
			
			
			    BEGIN   -- has a unique key column
				select @UniqueCol=convert(sysname,c.name)
				from syscolumns c
					inner join sysindexes i on i.id = c.id
							and (i.status & 0x800) = 0x800
				 	inner join syscolumns c1 on c1.id = c.id
							and c1.colid <= i.keycnt
				where c.id = object_id(@tableName)
				  and c.name = index_col (@tableName, i.indid, c1.colid)
			
				select @flagtable = 0
			    END
			
			
			ELSE IF 
				(select count(1)
				from syscolumns c
				where c.id = object_id(@tableName)
				and name in ('flagid','entityid')) = 2
				
				BEGIN -- is a flag table
				   select @UniqueCol = ''
				   	-- if the table has a data field then assume it is a normal flag table, if not then it is a complex flag
					IF exists (select 1 from syscolumns c where c.id = object_id(@tableName) and name = 'data') OR @tableName = 'booleanflagdata' 
				  	   select @flagtable = 1    
					ELSE
				  	   select @flagtable = 5    

				END
			
			-- 2016-06-01	WAB For PROD-1124 (SODmaster API). Add support for tables without single column primary keys. Instead, identity column specified in schemaTable.uniqueKey
			ELSE IF 
				exists (select * from schemaTable where entityName = @TableName and uniquekey is not null)
				BEGIN
			
					select @UniqueCol = uniqueKey from schemaTable where entityName = @TableName
					select @flagtable = 0
				END	
				
			ELSE
				begin
				   RAISERROR ('This table has a composite key. Create schematable entry and specify uniquekey column and try again',
				      16, 1)
				   RETURN 1
				end
			
			-- is there an entityTypeID from this table
			IF EXISTS (select 1 from dbo.schematable where entityname =@tablename)
				BEGIN
					select @entityTypeID = entityTypeID from dbo.schematable where entityname =@tablename
					update dbo.schematablebase
					   set --mnemonic = @mnemonic,
					       UniqueKey = @UniqueCol
					 where entityName = @tableName
				END	 
			ELSE
				BEGIN
					IF (@entityTypeID is null)
						BEGIN
						   RAISERROR ('You Must supply parameter @entityTypeID',
						      16, 1)
						   RETURN 1
							Return 1
						END	
					ELSE
						BEGIN
							IF NOT EXISTS (select 1 from dbo.schematable where entityTypeID = @entityTypeID and entityname <> @tablename)							
								insert dbo.schematablebase(entitytypeid, entityName,mnemonic,UniqueKey,screensexist,flagsexist) 
								select @entityTypeID,@tableName,@mnemonic,@UniqueCol,0,0 
							ELSE
								BEGIN
									RAISERROR ('There is already an entity with this entityTypeID ' ,
						      		16, 1)
						   			RETURN 1
						   		END
								

						END					
				END
				
			
			-- Create entries in the ModEntityDef
			-- WAB 2009/04/22 not required for plain flag tables
			IF @flagtable <> 1    
			BEGIN
				DECLARE modentity_cursor CURSOR LOCAL STATIC FOR
				select 'Whole Record', 0 as orderindex
				where not exists (select 1 from modentityDef where tablename = @tablename and fieldname = 'Whole Record')
				
				UNION

 				select c.name, c.colid as orderindex 
					from sysobjects s 
						inner join syscolumns c on c.id = s.id
						left join modentityDef  med on med.fieldname = c.name and s.name = med.tablename
				 where s.name=@TableName
				   and s.type = 'U'
				   and med.fieldname is null  /* WAB 2012-03-08 this is Null was missing, which meant that existing rows were being duplicated, Aargh.  Must have been my mistake  */

				UNION
				select 'AnyFlag',999 as orderindex
				where not exists (select 1 from modentityDef where tablename = @tablename and fieldname = 'AnyFlag')

				 order by orderindex


				/*
				This did sometimes cause problems with tables of more than 100 columns
				Really no need to have consecutive numbering, but it looks quite nice so I'll try to do it 
				WAB 2012-03-21 changed to start at 	@entityTypeID * 1000000
				WAB 2014-03-26 Still always have problems with the person table because @entityTypeID is 0 and so the startID is 0 and trounces over the original ids, so put in a special case for person  
				*/
				select @startId = dbo.maximumvalue(case when @entityTypeID = 0 then 1000000 - 1000 else @entityTypeID * 1000000 end,
					isnull((select max(modentityid) from modentitydef where tablename = @tablename),0) +1)

--				if exists (select 1 from modentitydef where modentitydef  
				set @cnt = 0		
				OPEN modentity_cursor
				FETCH NEXT FROM modentity_cursor into @fieldname,@dummy 
					WHILE @@FETCH_STATUS = 0
						BEGIN
							
							/* WAB 2012-09-26 added lastupdated and lastupdatedby which were missing for some reason */
							insert ModEntityDef(ModEntityID,TableName,FieldMonitor,FieldName)
							values (@startid + @cnt,
									@tablename,
									case 
										when 
												@fieldName not in ('created','createdby','lastupdated','lastupdatedby','lastupdatedbyperson') 
											and @fieldName not like '%_defaultTranslation'
											and @fieldName not like 'hasRecordRights%'											 
											and @fieldName not like 'hasCountryScope%'
											and @fieldName not like '%matchName%'
										then 1 
										else 0 end,

									@fieldname
									)
				
							FETCH NEXT FROM modentity_cursor into @fieldname,@dummy  
							set @cnt = @cnt + 1	
				
						END
				CLOSE modentity_cursor 
				DEALLOCATE modentity_cursor 


				/*
				delete ModEntityDef where TableName=@tableName
			
				insert ModEntityDef(ModEntityID,TableName,FieldMonitor,FieldName)
				 select (sc.entitytypeid*100),s.name,0,'Whole Record'
					from sysobjects s 
						inner join dbo.schematable sc on sc.entityname = s.name
				 where s.name=@TableName
				   and s.type = 'U'
			
				insert ModEntityDef(ModEntityID,TableName,FieldMonitor,FieldName)
				*/
				
				
				
				
			END		

		
			-- Create an entry in the ModTrigger table for each of the 3 database actions (Insert, Update Delete).  This will require a 'Name' to be used as part of the audit records which go into  ModRegister.
			delete modtrigger where tablename = @tablename
			insert modtrigger(Name, ISON, TableName, ModAction,FlagTable)
				select isNull(sc.mnemonic,'')+'A',0,@TableName,'I',@flagtable
					from dbo.schematable sc where entityname=@TableName
			insert modtrigger(Name, ISON, TableName, ModAction,FlagTable)
				select isNull(sc.mnemonic,'')+'M',0,@TableName,'U',@flagtable
					from dbo.schematable sc where entityname=@TableName
			insert modtrigger(Name, ISON, TableName, ModAction,FlagTable)
				select isNull(sc.mnemonic,'')+'D',0,@TableName,'D',@flagtable
					from dbo.schematable sc where entityname=@TableName
			
			-- Create an .._MRAudit trigger for Phrase
			declare @triggerNeedsUpdating bit
			select  @triggerNeedsUpdating = 1 from sysobjects so inner join syscomments sc on sc.id = so.id	where so.name = @Tablename+'_MRAudit' and xtype='TR' and text not like '%deleteEntityFlagsTriggerAction%'
			
			if not exists (select * from sysobjects where name = @Tablename+'_MRAudit' and xtype='TR') 
				OR @triggerNeedsUpdating = 1
				
			BEGIN
				IF @triggerNeedsUpdating = 1 
				BEGIN
					select @sqlcmd	= 'Drop TRIGGER ' + @Tablename + '_MRAudit'
					exec (@sqlCmd) 
				END
				
				
				select @sqlcmd	=	
					'CREATE TRIGGER '+@Tablename+'_MRAudit ON dbo.'+@Tablename+' FOR INSERT,UPDATE,DELETE AS '
					+	'
					IF @@rowcount = 0
						return
						
					set nocount on
					declare @status int, @rowsInserted int, @rowsDeleted int
					declare @columns_Updated binary (20)
					
					select * into #D from deleted
					select @rowsDeleted = @@rowcount
					select * into #I from inserted
					select @rowsInserted = @@rowcount

					/* if rowcount selecting into #i is zero then must be a delete, we will check for protected flags and delete flags first */
					IF @rowsInserted = 0 and @rowsDeleted > 0
					BEGIN
						exec @status=deleteEntityFlagsTriggerAction @@procid
						if @status <> 0
						begin
							rollback tran
							return
						end
					END

					select @columns_Updated = columns_updated()

					exec @status=ModRegisterTriggerAction @@procid, @columns_Updated=@columns_Updated

					if @status <> 0
					begin
						rollback tran
						return
					end'
				exec (@sqlcmd)
			END
			ELSE
			Print 'Trigger ' + @Tablename+'_MRAudit' + ' already exists' 			

			
/*  WAB 2013-09-25 
	This script is switching Monitoring ON on fields where is has previously been switched off, this is not a good idea so I am removing the code			

		if (@ModTriggerOn = 1)
		BEGIN
			update modentitydef set fieldmonitor = 1 
			where tablename = @tablename  
			and fieldname not in ('lastupdated','lastupdatedby','created','createdby')

		END
*/
		
		if (@ModEntityDefOn = 1)
		BEGIN
			update modtrigger set ison = 1 where tablename = @tablename 
		END

			
			-- Create a Del table.
			
			-- select @sqlcmd = 'select * into '+@TableName+'Del from '+@TableName+' where 1=2'
			
			-- exec (@sqlcmd)
			-- two lines above commented out and replaced with new procedure

			exec Generate_MRAuditDel @tablename = @tablename


