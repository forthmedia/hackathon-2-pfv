createPlaceHolderObject @objectName = 'CsvToTable', @objectType = 'tablefunction'

GO

ALTER Function [dbo].[CsvToTable] ( @List nvarchar(max)) 
returns @Table table 
	(Value nvarchar(max))
AS

/*
WAB 2015-10-25, after an NYB function of same name which does not appear to have been committed 

Convert a list of values into a table

*/

BEGIN

	declare @xmllist  xml = '<dataitem>'+ replace(dbo.xmlformat( @list ),',','</dataitem><dataitem>')+'</dataitem>'
	
	INSERT INTO 
		@Table (value)
	SELECT 
		x.y.value('.','nvarchar(max)') AS IDs
	FROM 
		 @xmllist.nodes ('dataitem') as x (y)
	where x.y.value('.','nvarchar(max)') <> ''

	RETURN
END


