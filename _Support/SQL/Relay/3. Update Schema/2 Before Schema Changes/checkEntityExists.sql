
-- drop existing check constraints that are using the function
declare @constraintSql nvarchar(2000)

DECLARE constraintCursor CURSOR LOCAL STATIC FORWARD_ONLY FOR
    SELECT
    'ALTER TABLE ' + RTRIM(o.name) + ' drop constraint ' +  cc.name AS constraintSql
    --cc.Definition as constraintText,c.name as columnName
    FROM
       sys.check_constraints cc
	inner join sys.objects o on o.object_id = cc.parent_object_id
	inner join sys.Columns c on  o.object_id = c.object_id and c.column_id = cc.parent_column_id
	where cc.type = 'C'
	and cc.Definition like '%checkEntityIDExists%'
        
OPEN constraintCursor
fetch next from constraintCursor into @constraintSql

while @@FETCH_STATUS = 0
begin
	exec sp_executesql @statement=@constraintSql
	fetch next from constraintCursor into @constraintSql
end

CLOSE constraintCursor
DEALLOCATE constraintCursor


if exists (select 1 from sysobjects where name='CheckEntityIDExists' and type='FN')
drop function dbo.CheckEntityIDExists

GO

CREATE FUNCTION dbo.CheckEntityIDExists (@entityid INT, @entityTypeID INT)
RETURNS BIT AS
BEGIN
    IF ((@entityID is null) or (EXISTS (SELECT 1 FROM vEntityName WHERE entityid = @entityid and entityTypeID = @entitytypeid )))
    BEGIN
        RETURN 1;
    END

    RETURN 0;

END