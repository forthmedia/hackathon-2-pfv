/*
Stored Procedure for doing Phrase Migration

Uses 
1.	Where a column in a table contains a phraseTextID and we want to convert to use entity phrases.
	We copy all the existing phrases into new phrases linked to the table's entityID
	(Normally in this case @columnName will be something like  wxyzPhraseTextID)

2.	(Since 2013) @columnContainsPhraseTextID = 0
	Where a column in a table contains the actual text which we want to convert into a phrase
	We create new phrases lined to the table's entityID and copy the text from the column into the new phrase
	These are created as countryid = 0, languageid = 1
	(Normally in this case @columnName will be something like  Title or Description)

Parameters
	@phrasetextID - the phraseTextID to be used for the new entity phrases.  Usually something like Title or Description
	@tableName - the name of the entityTable
	@columnName - The column in the table which contains either a phraseTextID or the Phrase Text
	@columnContainsPhraseTextID (bit) - Whether @columnName contains a phraseTextID or the Phrase Text


WAB 2012-01-24  altered so that @columName can be '' - this just creates and populates the _defaultTranslation column  
WAB 2013-05-28  CASE 435405 Added @columnContainsPhraseTextID so that we could create entity phrases from columns containing the text which needs to be a translation
WAB 2013-10-10	Improved performance for very large migrations (Lexmark Products).  Improved one join do some of the inserts in batches of 10000
				Discovered infinite loop trying to create _defaultTranslations if the phrase does not have a countryid=0 translation.  
				Have put in a fix, but ought to be aware that in this case a _defaultTranslation will never be translated
*/

if exists (select 1 from sysobjects where name = 'migrateToEntityPhrases')
	drop procedure migrateToEntityPhrases

GO
create procedure migrateToEntityPhrases
@phrasetextID varchar(100) , @tableName varchar(100),@columnName varchar(100), @columnContainsPhraseTextID bit = 1
as

	/*
	If @columnName is blank then this procedure will just create a defaultTranslation column and populate it
	*/
	
	declare @entityTypeID int,@identitycolumnName varchar(100),@sql nvarchar(2000),@duplicatephraseTextIDsql nvarchar(2000), @initialPhraseid INT, @tableHasColumnWithSameNameAsPhraseTextID int, @rowcount int, @maxRowsPerLoop int, @message nvarchar(1000)
	select @entityTypeID = entityTypeID, 	 @identitycolumnName = uniquekey  from schematable where entityname = @tablename
	select @initialPhraseid = max(phraseid) from phraselist
	select @tableHasColumnWithSameNameAsPhraseTextID = case when exists (select 1 from information_schema.columns where table_name = @tablename and column_name = @phrasetextid) then 1 else 0 end
	
	select @maxRowsPerLoop = 10000
	
	-- print 'entityTypeID = ' +convert(varchar, @entityTypeID )
	-- print 'Unique Key ' + @identitycolumnName
	print 'Migrating Phrases for ' + @tablename + '.' + @columnName+ ' to ' + @phraseTextID
	
	IF @columnname <> ''
	BEGIN
		-- Create entries in the phraselist table for each of the phrasetextIDs, with correct entityType and entityID
		select @sql = '
		insert into phraseList
		(phraseID,phraseTextID,entityTypeID,entityID)
		select ' + convert(varchar,@initialPhraseid) + ' + ' + @identitycolumnname + ',@phrasetextID , @entityTypeID , '+@IDENTITYColumnName+' 
		from ' +@TABLENAME + '
		left join phraselist pl ON pl.phraseTextID = @phrasetextID  and pl.entityTypeID = @entityTypeID  and pl.entityid = '+ @identityColumnName + '
		where '
		+@ColumnName +' <> '''' and
		pl.phraseid is null'

		set rowcount  @maxRowsPerLoop
		set noCount ON
		select @rowCount = 1
		WHILE @rowCount <> 0 
			BEGIN
				EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
				select @rowCount = @@rowCount
				select @message = case when @rowCount = 0 then 'Finished ' ELSE 'Created ' + convert(varchar,@rowCount) END + ' PhraseList Entries'
				RAISERROR (@message, 10, 1) WITH NOWAIT
				
			END
			
		set rowcount  0
		set noCount OFF

		
		-- Now copy the text of each phrase
			IF (@columnContainsPhraseTextID = 1)
				BEGIN
					select @sql = '
						insert into phrases
						(phraseID,phraseText,LanguageID,CountryID, defaultForThisCountry)
						select pl.phraseID,v.phraseText,v.Languageid,v.Countryid, v.defaultForThisCountry from '
						+ @tablename + 
					' inner join 
						vphrases v on ' + @columnname + ' = v.phraseTextID and v.entityTypeId = 0
						 inner join
						phraseList pl on pl.phraseTextID = @phrasetextID and pl.entityID = ' + @identityColumnName +' and pl.entityTypeID = @entityTypeID 
						 left join
						phrases p on p.phraseID = pl.phraseid and p.countryid = v.countryid and p.languageid = v.languageid
						where '
						+@ColumnName +' <> '''' and
						p.phraseid is null
					'
				END
			ELSE
				BEGIN
					select @sql = '
						insert into phrases
						(phraseID,phraseText,LanguageID,CountryID)
						select pl.phraseID,' + @columnname + ',1,0 from '
						+ @tablename + 
					' inner join 
						phraseList pl on pl.phraseTextID = @phrasetextID and pl.entityID = ' + @identityColumnName +' and pl.entityTypeID = @entityTypeID 
						 left join
						phrases p on p.phraseID = pl.phraseid 
						where '
						+@ColumnName +' <> '''' and
						p.phraseid is null
					'
				
				END		
		
		set rowcount  @maxRowsPerLoop
		set noCount ON
		select @rowCount = 1
		WHILE @rowCount <> 0 
			BEGIN
				EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
				select @rowCount = @@rowCount
				select @message = case when @rowCount = 0 then 'Finished ' ELSE 'Created ' + convert(varchar,@rowCount) END + ' Phrase Entries'
				RAISERROR (@message, 10, 1) WITH NOWAIT
			END
		set rowcount  0
		set noCount OFF
		
		/* SQL for getting items where there is more than one identical phraseTextID in the column
		   need to create a phrase within phrase
			 only going to do this for flags (which often use Yes/No)
		*/
		
		IF @tablename = 'FLAG'
		BEGIN
			select @duplicatephraseTextIDsql = '
			 from ' + @tablename + ' where ' + @columnname + ' in (
			select ' + @columnName + ' from ' +@tablename + ' where ' + @columnName + ' <> '''' group by ' + @columnName + ' having count(1) > 1)'
			-- 
			select @sql = 'select '+ @identitycolumnName + ', ' + @columnName  +@duplicatephraseTextIDsql + 'order by ' + @columnName
			-- print @sql
			EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
			-- deletes phrases created above
			select @sql = 'delete  phrases from 
			 phraseList pl 
			  inner join 
			 phrases p on p.phraseid = pl.phraseid 
			 where 
			  pl.phrasetextid =   @phraseTextID 
			  and pl.entityTypeID = @entityTypeID 
			  and pl.entityid in (select ' + @identitycolumnName  + @duplicatephraseTextIDsql + ')'
			-- print @sql
			EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
		
			-- and replaces with phrase within phrase
			select @sql = '
			insert into phrases
			(phraseid,languageid,countryid,phraseText)
			select  pl.phraseid,0,0,''phr_''+'  + @columnName  
			+ ' from  ' + @tablename + 
			'
			  inner join 
			 phraselist pl on 
			  phraseTextID = @phraseTextID 
			  and pl.entityTypeID = @entityTypeID 
			  and pl.entityID = ' + @identityColumnName + '
			 where ' + 
			 @identityColumnName + ' in (select ' + @identityColumnName + @duplicatephraseTextIDsql + ')'
			 
			EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
		 
		END
	END
	
	-- For flags which don't have a namePhrasetextid then need to create a default translation from the name
	IF @tableName in ('flag','flagGroup')
	BEGIN
		select @sql = '
		insert into phraseList
		(phraseID,phraseTextID,entityTypeID,entityID)
		select distinct ' + convert(varchar,@initialPhraseid) + ' + ' + @identitycolumnname + ', @phrasetextID , @entityTypeID , '+@IDENTITYColumnName+' 
		from ' +@TABLENAME + '
		where 
		isnull('+@ColumnName +','''') = '''' and '
		+ @phraseTextID +' <> '''' and
		not exists
		(select 1 from phraselist where phraseTextID = @phrasetextID  and entityTypeID = @entityTypeID  and entityid = '+ @identityColumnName + ')'

		EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
	
	
		-- Now create each phrase from the name or desription
		-- assuming that the phraseTextID has the same name as the column
		select @sql = '
		insert into phrases
		(phraseID,phraseText,LanguageID,CountryID, defaultForThisCountry)
		select pl.phraseID,' + @phrasetextid+ ',1,0, 1 from '
		+ @tablename + 
		' inner join
		phraseList pl on pl.phraseTextID = @phrasetextID and pl.entityID = ' + @identityColumnName +' and pl.entityTypeID = @entityTypeID 
			 left join
		phrases p on p.phraseID = pl.phraseid 
	
		where 
		isnull('+@ColumnName +','''') = '''' and '
		+ @phraseTextID +' <> '''' and
			p.phraseid is null
		'
	
		EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID 
	
	END
	
	/*
	WAB 2011/01/24
	Add in a _defaultTranslation colum to the table and populate it 
	*/
	if not exists  (select 1 from information_schema.columns where column_name = @phrasetextID + '_defaultTranslation' and table_name = @tablename )
	BEGIN
		select @sql = 'alter table ' + @tablename + ' add  ' + @phrasetextID + '_defaultTranslation nvarchar(400)' 
		exec (@sql)
		print 'Added DefaultTranslation Column for ' + @tableName 
	
	END 
	
		select @sql = 'update phrases set phraseText=phraseText
						from phrases  inner join phraselist pl
						on phrases.phraseid = pl.phraseid
						inner join  ' + @tablename  + ' ent  on pl.entityid = ent.' + @identitycolumnName + '
						where 
							entityTYpeID = ' + convert(varchar,@entityTypeID) + '  
							and phraseTextid = ''' + @phrasetextID + ''' 
							and defaultforthiscountry = 1 
							and phrases.countryid = 0    /* WAB 2013-10-10 discovered some phrases which did not have a countryid=0 version, so a _defaultTranslation was never created, which gave an infinite loop) */
							and ' + @phrasetextID + '_defaultTranslation is null '


		-- disable a couple of triggers which can cause this part to run very slowly
		IF EXISTS (select 1 from sysobjects where name = 'entityPhraseLastUpdatedUpdate')
			DISABLE TRIGGER entityPhraseLastUpdatedUpdate ON phrases;
		DISABLE TRIGGER insteadOfUpdatePhrases ON phrases;


		set rowcount  @maxRowsPerLoop
		set noCount ON
		select @rowCount = 1
		WHILE @rowCount <> 0 
			BEGIN

				exec (@sql)
				select @rowCount = @@rowCount;
				
				select @message = case when @rowCount = 0 then 'Finished ' ELSE 'Created ' + convert(varchar,@rowCount) END + ' _defaultTranslation entries'
				RAISERROR (@message, 10, 1) WITH NOWAIT

			END
		set rowcount  0
		set noCount OFF

		IF EXISTS (select 1 from sysobjects where name = 'entityPhraseLastUpdatedUpdate')
			ENABLE TRIGGER entityPhraseLastUpdatedUpdate ON phrases;
		ENABLE TRIGGER insteadOfUpdatePhrases ON phrases;
		
	
	
	
	
	IF @columnName <> '' 
	BEGIN
		-- This tries to show old and new stuff
		select @sql = '
		select ' + @identitycolumnname + 
		case when @tableHasColumnWithSameNameAsPhraseTextID =1 then ', ' +  @phrasetextid else '' end
		 + ', '+  @columnName +    ', p1.phraseid as wasPhraseID, p1.phrasetext wasPhraseText, p2.phraseid as nowPhraseID, p2.phrasetext nowPhrasetext, p2.languageid, p2.countryid 
	from '  +  @tablename + ' 
	left join vphrases p1 on p1.phrasetextid = ' + @columnname + ' and p1.entityTypeid = 0
	left join vphrases p2 on p2.phrasetextid = @phrasetextid and p2.entityid = ' +@identitycolumnname+' and p2.entityTypeid = @entityTypeid and ((p1.languageid = p2.languageid and p1.countryid = p2.countryid) or (p1.phraseid is null))
	order by ' + @identitycolumnname
	
		-- print @sql
		EXEC sp_executesql @sql,N'@phraseTextID varchar(100),@entityTypeID int',@phrasetextID=@phraseTextid,@entityTypeID =@entityTypeID
	
	END
