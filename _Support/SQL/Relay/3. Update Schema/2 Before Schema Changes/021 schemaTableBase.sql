If not exists (select 1 from information_schema.tables where table_name='schemaTableBase')
Begin
CREATE TABLE [dbo].[schemaTableBase](
	[entityTypeID] [int] NOT NULL,
	[entityName] [varchar](50) NOT NULL,
	[NameExpression] [nvarchar](50) NULL,
	[UniqueKey] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[FlagsExist] [bit] NOT NULL,
	[mnemonic] [varchar](5) NULL,
	[ScreensExist] [bit] NOT NULL,
	[CustomEntity] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_schemaTableBase] PRIMARY KEY CLUSTERED 
(
	[entityTypeID] ASC
) ON [PRIMARY]
) ON [PRIMARY]

End

GO


/* WAB 2015-09-29 Add if statement to protect against non existence of relayserver 
				First attempt failed.  Need to make it into dynamic sql, otherwise just won't compile
				Second attempt failed, even if relayServer exists, schema table may not.  More dynamic SQL
*/
IF Exists (select 1 from sys.databases where name = 'relayserver')
BEGIN	

	declare @tableExists bit, 
			@checkForTableSQL nvarchar(max) = 'select @tableExists = count(1) from relayserver.information_schema.tables where table_name = ''schemaTable'' '

	exec sp_executeSQL @checkForTableSQL,N'@tableExists bit OUTPUT', @tableExists = @tableExists OUTPUT
		
	IF @tableExists <> 0
	BEGIN
       	declare @fullSQL nvarchar(max) = '
   
   			  insert into schemaTableBase(entitytypeID,entityname,nameExpression,uniqueKey,description,flagsExist,mnemonic,screensExist)
   			  select s.entitytypeID,s.entityname,s.nameExpression,s.uniqueKey,s.description,s.flagsExist,s.mnemonic,s.screensExist 
   			  from relayserver.dbo.schemaTable s
    				left join schemaTableBase b on s.entityTypeID = b.entityTypeId
    				where b.entityTypeID is null
       	       	
   	 			if exists(select 1 from relayserver.information_schema.columns where table_name = ''schemaTable'' and column_name = ''customEntity'')
   	 			Begin
   	 				declare @sql varchar(max)
   	 				select @sql = ''update stb
   	 					set stb.customEntity = st.customEntity
   	 					from dbo.schemaTableBase as stb
   	 					join relayserver.dbo.schematable st  on stb.entitytypeID = st.entitytypeID'' 
   	 				exec (@sql)
   	 			End
        		
    		'
		exec (@fullSQL)

	END
        	
END              
       
IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=469) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'trngTrainingProgram') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TPRG') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (469, 'trngTrainingProgram', 'TrainingProgramID', 0, 'TPRG', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=470) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'QuizQuestion') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'QQ') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (470, 'QuizQuestion', 'QuestionID', 0, 'QQ', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=471) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'QuizAnswer') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'QA') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (471, 'QuizAnswer', 'AnswerID', 0, 'QA', 0);
GO

-- 2012-07-27 PPB SMAA001 added entities FundRequest and RWPersonAccount because I have referenced them from application.com.rights.getRightsFilterWhereClause() 

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=472) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'FundRequest') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'FR') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (472, 'FundRequest', 'FundRequestID', 0, 'FR', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=473) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'RWPersonAccount') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'RWPA') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (473, 'RWPersonAccount', 'RWPersonAccountID', 0, 'RWPA', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=474) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'BudgetGroup') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'BGR') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (474, 'BudgetGroup', 'BudgetGroupID', 0, 'BGR', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=475) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'BudgetGroupPeriodAllocation') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'BGPA') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (475, 'BudgetGroupPeriodAllocation', 'BudgetGroupPeriodAllocationID', 0, 'BGPA', 0);
GO

/* case 431268 */
/*WAB 2014-10-13 removed null from flagsExist column - now seems to have a constraint */ 
if not exists(select 1 from schemaTableBase where entityTypeID=66 and entityName='FundCompanyAccount')
INSERT INTO dbo.schemaTableBase ([entitytypeid], [entityName], [NameExpression], [UniqueKey], [Description], [FlagsExist], [mnemonic], [ScreensExist]) 
VALUES (66, 'fundCompanyAccount', 'FundCompanyAccount', N'accountID', NULL,0,NULL,0)
				
/* uncomment and run this if the SQL server contains only 2012 spring or later DBs - not to be run on pre 2012 Spring DB servers
UPDATE dbo.schemaTableBase SET FlagsExist=0 WHERE entityName='quizTaken' AND FlagsExist=1
*/

/* Case 431518 */
update dbo.schemaTableBase set entityTypeID=206 where entityName='eventCountry' and entityTypeID is null and not exists (select 1 from dbo.schemaTableBase where entityTypeId=206)

/* Case 431681 */
update dbo.schemaTableBase set entityTypeID=270 where entityName='oppProducts' and entityTypeID is null and not exists (select 1 from dbo.schemaTableBase where entityTypeId=270)

/* NJH 2012/11/19 CASE 432093  - added a few new schema table entries to support mod register actions for elearning */
IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=476) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'trngCertificationRule') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TCR') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (476, 'TrngCertificationRule', 'certificationRuleID', 0, 'TCR', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=477) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'trngModuleSet') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TMS') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (477, 'TrngModuleSet', 'trngModuleSetID', 0, 'TMS', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=478) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'trngModuleCourse') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TMC') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (478, 'TrngModuleCourse', 'trngModuleCourseID', 0, 'TMC', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=479) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'trngModuleSetModule') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TMSM') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (479, 'TrngModuleSetModule', 'trngModuleSetModuleID', 0, 'TMSM', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=480) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'eligibilityRule') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'ER') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (480, 'EligibilityRule', 'eligibilityRuleID', 0, 'ER', 0);
GO

--
update dbo.schemaTableBase set flagsExist=1 where entityName='files' and flagsExist=0

/* NJH 2012/11/27 - create a schema entry for fileFamily view, so that we can create flags against file families */
IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=481) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'fileFamily') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'FF') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (481, 'FileFamily', 'fileFamilyID', 1, 'FF', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=482) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'QuizQuestionPool') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'QQP') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (482, 'QuizQuestionPool', 'QuizQuestionPoolID', 0, 'QQP', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=483) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'specialisationRule') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'SR') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (483, 'specialisationRule', 'specialisationRuleID', 0, 'SR', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=484) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'decimalFlagData') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'DECF') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (484, 'decimalFlagData', 'flagID', 0, 'DECF', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=485) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'textMultipleFlagData') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TMF') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (485, 'textMultipleFlagData', 'flagID', 0, 'TMF', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=486) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'discussionMessage') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'DSM') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (486, 'discussionMessage', 'discussionMessageID', 0, 'DSM', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=487) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'entityComment') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'ENC') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (487, 'entityComment', 'entityCommentID', 0, 'ENC', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=488) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'relayActivity') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'RA') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (488, 'relayActivity', 'ID', 0, 'RA', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=489) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'discussionGroup') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'DSG') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (489, 'discussionGroup', 'discussionGroupID', 0, 'DSG', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=490) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'twitterAccount') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'TA') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (490, 'twitterAccount', 'twitterAccountID', 0, 'TA', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=491) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'message') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'MSG') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (491, 'message', 'message', 0, 'MSG', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=492) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'settings') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'SET') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (492, 'settings', 'settings', 0, 'SET', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=493) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'messagesent') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'MSGSNT') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (493, 'messagesent', 'message', 0, 'MSGS', 0);
GO

IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=494) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'commFromDisplayName') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (494, 'commFromDisplayName', 'commFromDisplayNameID', 0, NULL, 0);
GO

/* NJH the mnemonic for files entry was not set */
update schemaTableBase set mnemonic='F' where entityname='Files' and mnemonic!='F' 

update dbo.schemaTableBase set entityname='broadcastAccount',uniqueKey='broadcastAccountID',mnemonic='BCA' where entityname='twitterAccount'

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=496) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'activityType') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'AT') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, description, mnemonic,flagsExist,screensExist) VALUES (496, 'activityType','Activity Types','AT',1,1);
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=497) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'siteDefDomain') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'SDD') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (497, 'siteDefDomain','SDD',0,0);
GO

/* 02/06/2014  Connector - added 3 entityTypes to track changes to various tables */
IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=508) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'connectorObject') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'CONO') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (508, 'connectorObject','CONO',0,0);
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=501) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'connectorMapping') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'CONM') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (501, 'connectorMapping','CONM',0,0);
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=502) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'connectorColumnValueMapping') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'CONVM') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (502, 'connectorColumnValueMapping','CONVM',0,0);
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=512) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'financialFlagData') AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE mnemonic = 'FFD') 
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (512, 'financialFlagData','FFD',0,0);
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=513) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'xFinancialFlagData')
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (513, 'xFinancialFlagData',NULL,0,0);
GO

update schemaTableBase set mnemonic='Lead' where entityName='lead' and mnemonic is null

if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='in_vEntityName')
alter table schemaTableBase add [in_vEntityName] bit CONSTRAINT [DF_schemaTable_in_vEntityName] default 0
GO

if not exists (select 1 from schemaTableBase where [in_vEntityName] = 1)
begin
	update schemaTableBase set [in_vEntityName] = 1 
	where entityName in ('files','element') or entityname in (select table_name from information_schema.columns where column_name like 'crm%ID')
end

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=517) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'CJMTemplate')
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (517, 'CJMTemplate','CJM',0,0);
GO

/* AXA Quotes */
/* NJH commented out for now as the quotes table do not exist.
IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=518) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'quote')
	INSERT INTO schemaTableBase (entitytypeid, entityName, NameExpression, uniquekey, mnemonic,flagsExist,screensExist,in_vEntityName) VALUES (518, 'quote', 'quoteName','quoteID','quote',1,0,1);

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=519) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'quoteProduct')
	INSERT INTO schemaTableBase (entitytypeid, entityName, NameExpression, uniquekey, mnemonic,flagsExist,screensExist,in_vEntityName) VALUES (519, 'quoteProduct', NULL,'quoteProductID','QP',1,0,1);
*/
/* END Quotes */


IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=520) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'trngTrainingPath')
	INSERT INTO schemaTableBase (entitytypeid, entityName, NameExpression, uniquekey, mnemonic,flagsExist,screensExist,in_vEntityName) VALUES (520, 'trngTrainingPath', NULL,null,null,1,0,1);
GO

/* NJH 2015/09/30 set the screens exist so that profiles appear in the dropdown */
update schemaTableBase set screensExist=1 where entityName in ('lead','fundRequestActivity','Opportunity') and isNull(screensExist,0) = 0

/* 2015/11/13 Prod 2015-235 - add label to schema table.. slightly nicer than friendly name */
if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='label')
alter table schemaTableBase add label varchar(100)
GO

update schemaTableBase set label='Organization' where entityName = 'organisation' and label is null
update schemaTableBase set label='Training Program' where entityName = 'trngTrainingProgram' and label is null
update schemaTableBase set label='Product Group' where entityName = 'productGroup' and label is null
update schemaTableBase set label='Product Category' where entityName = 'productCategory' and label is null
update schemaTableBase set label='Promotion' where entityName = 'rwPromotion' and label is null
update schemaTableBase set label='Course' where entityName = 'trngCourse' and label is null
update schemaTableBase set label='Training Path' where entityName = 'trngTrainingPath' and label is null
update schemaTableBase set label='Specialization' where entityName = 'specialisation' and label is null
update schemaTableBase set label='Certification' where entityName = 'trngCertification' and label is null
update schemaTableBase set label='Registration' where entityName = 'trngPersonCertification' and label is null
update schemaTableBase set label='Discussion Group' where entityName = 'discussionGroup' and label is null
update schemaTableBase set label='Module' where entityName = 'trngModule' and label is null
update schemaTableBase set label='Order' where entityName = 'orders' and label is null

/* WAB 2016-03-15 Added ProductSet, not sure it is used much/ever but there are some entries in the OOTB recordRights table */
IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=551) AND 
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'ProductSet')
	INSERT INTO schemaTableBase (entitytypeid, entityName, mnemonic,flagsExist,screensExist) VALUES (551, 'ProductSet','prs',0,0);

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=561) AND
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'SODMaster')
	insert into schemaTableBase (entitytypeid, entityName, uniquekey, FlagsExist, ScreensExist, label) VALUES(561, 'SODMaster', 'sodMasterID', 0, 0, 'SODMaster');
GO

IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=560) AND
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'productFamily')
	INSERT INTO schemaTableBase (entitytypeid, entityName, uniquekey, FlagsExist, ScreensExist, label) VALUES(560, 'productFamily', 'PRODUCTFAMILYID', 0, 0, 'ProductFamily');


IF NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entitytypeid=563) AND
NOT EXISTS (SELECT entitytypeid FROM schemaTableBase WHERE entityName = 'banner')
	INSERT INTO schemaTableBase (entitytypeid, entityName, uniquekey, FlagsExist, ScreensExist, label) VALUES(563, 'banner', null, 0, 0, 'Banner');

/* 2016/06/27 PROD2016-1304 - add API meta data */
if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='apiActive')
alter table schemaTableBase add apiActive bit not null CONSTRAINT [DF_schemaTable_apiActive] default 0
GO

if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='apiGet')
alter table schemaTableBase add apiGet bit not null CONSTRAINT [DF_schemaTable_apiGet] default 0
GO

if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='apiPost')
alter table schemaTableBase add apiPost bit not null CONSTRAINT [DF_schemaTable_apiPost] default 0
GO

if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='apiPut')
alter table schemaTableBase add apiPut bit not null CONSTRAINT [DF_schemaTable_apiPut] default 0
GO

if not exists (select 1 from information_schema.columns where table_name='schemaTableBase' and column_name='apiDelete')
alter table schemaTableBase add apiDelete bit not null CONSTRAINT [DF_schemaTable_apiDelete] default 0
GO

update schemaTable set nameExpression='fullname' where entityName='person' and nameExpression !='fullname'

/* 2017/03/06 MRE PROD2016-3525: Changes to allow configuration of budgets */
IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=565) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'BudgetGroupType') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'BGRT') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, NameExpression, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (565, 'BudgetGroupType', 'description', 'BudgetGroupTypeID', 0, 'BGRT', 0);
GO

/* 2017/03/06 MRE PROD2016-3523: Change to allow configuration of budget periods */
IF NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entitytypeid=566) AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE entityName = 'BudgetPeriodType') AND 
NOT EXISTS (SELECT [entityTypeID] FROM dbo.schemaTableBase WHERE mnemonic = 'BGPT') 
	INSERT INTO dbo.schemaTableBase (entitytypeid, entityName, NameExpression, UniqueKey, FlagsExist, mnemonic, ScreensExist) VALUES (566, 'BudgetPeriodType', 'description', 'PeriodTypeID', 0, 'BGPT', 0);
GO

/* 2017/03/07 MRE PROD2016-3524: Entity 474 is BudgetGroup */
UPDATE schemaTableBase SET NameExpression ='description' WHERE entityTypeId=474 AND NameExpression IS NULL