exec createPlaceHolderObject 'function', 'getTableIdentity'

GO
alter FUNCTION [dbo].[getTableIdentity] (@TableName sysname)  

returns sysname AS

/*
Author WAB  
Date  2015-01-28
Gets the name of the Identity Column of a table
This is what I  really wanted when I wrote getTablePrimaryKey
*/
BEGIN

	declare @result sysname
	
	
	select 
		@result = col.[name]
	FROM 
		[syscolumns] col
			JOIN 
		[sysobjects] obj ON obj.[id] = col.[id]
	WHERE 
			obj.type = 'U' 
		AND col.[status] & 128 = 128
		and obj.name = @tablename
	
	return @result

END
