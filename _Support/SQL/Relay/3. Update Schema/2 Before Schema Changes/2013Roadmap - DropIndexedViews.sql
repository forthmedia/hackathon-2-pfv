IF EXISTS(SELECT * FROM sys.views WHERE name = 'vOrganisationSearch') 
	DROP VIEW dbo.vOrganisationSearch;
GO

IF EXISTS(SELECT * FROM sys.views WHERE name = 'vLocationSearch') 
	DROP VIEW dbo.vLocationSearch;
GO

IF EXISTS(SELECT * FROM sys.views WHERE name = 'vPersonSearch') 
	DROP VIEW dbo.vPersonSearch;
GO