
IF EXISTS (select 1 from sysobjects where name='blogEntries' and type='u')
drop table blogEntries

IF EXISTS (select 1 from sysobjects where name='blogs' and type='u')
drop table blogs

IF EXISTS (select 1 from sysobjects where name='CONCollectionProduct' and type='u')
drop table CONCollectionProduct

IF EXISTS (select 1 from sysobjects where name='CONCollectionGroupRule' and type='u')
drop table CONCollectionGroupRule

IF EXISTS (select 1 from sysobjects where name='CONCollectionGroup' and type='u')
drop table CONCollectionGroup

IF EXISTS (select 1 from sysobjects where name='CONCollection' and type='u')
drop table CONCollection

IF EXISTS (select 1 from sysobjects where name='CONGroup' and type='u')
drop table CONGroup

IF EXISTS (select 1 from sysobjects where name='CONRule' and type='u')
drop table CONRule

IF EXISTS (select 1 from sysobjects where name='contract' and type='u')
drop table [contract]

IF EXISTS (select 1 from sysobjects where name='CCTransaction' and type='u')
drop table CCTransaction

IF EXISTS (select 1 from sysobjects where name='countyGroup' and type='u')
drop table countyGroup

IF EXISTS (select 1 from sysobjects where name='countyGroupName' and type='u')
drop table countyGroupName

IF EXISTS (select 1 from sysobjects where name='CompanyTypes' and type='u')
drop table CompanyTypes

IF EXISTS (select 1 from sysobjects where name='discountGroup' and type='u')
drop table discountGroup

IF EXISTS (select 1 from sysobjects where name='DTSParameters' and type='u')
drop table DataSourceDTSParams

IF EXISTS (select 1 from sysobjects where name='DTSParameters' and type='u')
drop table DTSParameters

IF EXISTS (select 1 from sysobjects where name='DTSParameterType' and type='u')
drop table DTSParameterType

IF EXISTS (select 1 from sysobjects where name='fonts' and type='u')
drop table fonts

IF EXISTS (select 1 from sysobjects where name='hyperlink' and type='u')
drop table hyperlink

IF EXISTS (select 1 from sysobjects where name='images' and type='u')
drop table images

IF EXISTS (select 1 from sysobjects where name='LocatorCountryHomePages' and type='u')
drop table LocatorCountryHomePages

IF EXISTS (select 1 from sysobjects where name='LocatorFlagMatch' and type='u')
drop table LocatorFlagMatch

IF EXISTS (select 1 from sysobjects where name='LocatorDefDefaults' and type='u')
drop table LocatorDefDefaults

IF EXISTS (select 1 from sysobjects where name='LocatorKnownIPAddresses' and type='u')
drop table LocatorKnownIPAddresses

IF EXISTS (select 1 from sysobjects where name='LocatorNonReportingIPs' and type='u')
drop table LocatorNonReportingIPs

IF EXISTS (select 1 from sysobjects where name='oppPool' and type='u')
drop table oppPool

IF EXISTS (select 1 from sysobjects where name='oppPoolHistory' and type='u')
drop table oppPoolHistory

IF EXISTS (select 1 from sysobjects where name='archLocation' and type='u')
drop table archLocation

IF EXISTS (select 1 from sysobjects where name='archPerson' and type='u')
drop table archPerson

IF EXISTS (select 1 from sysobjects where name='product_cogs' and type='u')
drop table product_cogs

IF EXISTS (select 1 from sysobjects where name='statusControlAction' and type='u')
drop table statusControlAction

IF EXISTS (select 1 from sysobjects where name='TelFaxChangeRules' and type='u')
drop table TelFaxChangeRules

IF EXISTS (select 1 from sysobjects where name='reportParallelColumns' and type='u')
drop table reportParallelColumns

IF EXISTS (select 1 from sysobjects where name='ReservedLocID' and type='u')
drop table ReservedLocID

IF EXISTS (select 1 from sysobjects where name='ReservedPerID' and type='u')
drop table ReservedPerID

IF EXISTS (select 1 from sysobjects where name='RelayVar' and type='u')
drop table RelayVar

IF EXISTS (select 1 from sysobjects where name='RelayTagUsage' and type='u')
drop table RelayTagUsage

IF EXISTS (select 1 from sysobjects where name='xPerson' and type='u')
drop table xPerson

IF EXISTS (select 1 from sysobjects where name='xPersonDataSource' and type='u')
drop table xPersonDataSource

IF EXISTS (select 1 from sysobjects where name='xPersonLookup' and type='u')
drop table xPersonLookup

IF EXISTS (select 1 from sysobjects where name='xLocation' and type='u')
drop table xLocation

IF EXISTS (select 1 from sysobjects where name='xLocationDataSource' and type='u')
drop table xLocationDataSource

IF EXISTS (select 1 from sysobjects where name='xLocationLookup' and type='u')
drop table xLocationLookup

IF EXISTS (select 1 from sysobjects where name='xOrganisation' and type='u')
drop table xOrganisation

IF EXISTS (select 1 from sysobjects where name='xOrgDataSource' and type='u')
drop table xOrgDataSource

IF EXISTS (select 1 from sysobjects where name='xOrgLookup' and type='u')
drop table xOrgLookup

IF EXISTS (select 1 from sysobjects where name='verityCollections' and type='u')
drop table verityCollections

IF EXISTS (select 1 from sysobjects where name='ReportGeneric' and type='u')
drop table ReportGeneric

IF EXISTS (select 1 from sysobjects where name='ReportGenericDef' and type='u')
drop table ReportGenericDef

IF EXISTS (select 1 from sysobjects where name='ReportGenericLink' and type='u')
drop table ReportGenericLink

IF EXISTS (select 1 from sysobjects where name='ReportGenericDefault' and type='u')
drop table ReportGenericDefault

IF EXISTS (select 1 from sysobjects where name='wysiwyg' and type='u')
drop table wysiwyg

IF EXISTS (select 1 from sysobjects where name='schemaDefinition' and type='u')
drop table schemaDefinition

IF EXISTS (select 1 from sysobjects where name='wabtemptable' and type='u')
drop table wabtemptable

IF EXISTS (select 1 from sysobjects where name='wabTestSite' and type='u')
drop table wabTestSite

IF EXISTS (select 1 from sysobjects where name='rebateDiscountParams' and type='u')
drop table rebateDiscountParams

IF EXISTS (select 1 from sysobjects where name='ruleInstance' and type='u')
drop table ruleInstance

IF EXISTS (select 1 from sysobjects where name='RuleParameter' and type='u')
drop table RuleParameter

IF EXISTS (select 1 from sysobjects where name='RuleValue' and type='u')
drop table RuleValue

IF EXISTS (select 1 from sysobjects where name='Rule' and type='u')
drop table [Rule]

IF EXISTS (select 1 from sysobjects where name='knowledgebase' and type='u')
drop table knowledgebase

IF EXISTS (select 1 from sysobjects where name='link' and type='u')
drop table link

IF EXISTS (select 1 from sysobjects where name='userLinkBackup' and type='u')
drop table userLinkBackup

IF EXISTS (select 1 from sysobjects where name='userLinkDistinct' and type='u')
drop table userLinkDistinct

IF EXISTS (select 1 from information_schema.views where table_name='vOrganisationScoring')
drop view vOrganisationScoring

IF EXISTS (select 1 from information_schema.views where table_name='vPortalUsersByJobFunction')
drop view vPortalUsersByJobFunction

IF EXISTS (select 1 from information_schema.views where table_name='vR_siteVisits_week_country')
drop view vR_siteVisits_week_country

IF EXISTS (select 1 from information_schema.views where table_name='vOrgListAlt_WithScores')
drop view vOrgListAlt_WithScores

IF EXISTS (select 1 from information_schema.views where table_name='passwordnull')
drop view passwordnull

IF EXISTS (select 1 from information_schema.views where table_name='vMatchingOrgsByCountry4')
drop view vMatchingOrgsByCountry4

IF EXISTS (select 1 from information_schema.views where table_name='vCTA1_BooleanFlagDataOrgByCountry')
drop view vCTA1_BooleanFlagDataOrgByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vCTB1_vCTA1_Weekly')
drop view vCTB1_vCTA1_Weekly

IF EXISTS (select 1 from information_schema.views where table_name='vCTA3_BooleanFlagDataPersByCountry')
drop view vCTA3_BooleanFlagDataPersByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vCTC1_vCTB1_vCTA1_SumOrgsForProfile')
drop view vCTC1_vCTB1_vCTA1_SumOrgsForProfile

IF EXISTS (select 1 from information_schema.views where table_name='vCT_BooleanFlagDataPersByCountry')
drop view vCT_BooleanFlagDataPersByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vCTC2_vCTB1_vCTA1_SumPersonsForProfile_Attribute')
drop view vCTC2_vCTB1_vCTA1_SumPersonsForProfile_Attribute

IF EXISTS (select 1 from information_schema.views where table_name='distiLocations')
drop view distiLocations

IF EXISTS (select 1 from information_schema.views where table_name='vCT_BooleanFlagDataOrgByCountry')
drop view vCT_BooleanFlagDataOrgByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vContentSearch')
drop view vContentSearch

IF EXISTS (select 1 from information_schema.views where table_name='vCTC1_vCTB1_vCTA3_SumPersonsForProfile_Attribute')
drop view vCTC1_vCTB1_vCTA3_SumPersonsForProfile_Attribute

IF EXISTS (select 1 from information_schema.views where table_name='vCTC2_vCTB1_vCTA1_SumOrgsForProfile_Attribute')
drop view vCTC2_vCTB1_vCTA1_SumOrgsForProfile_Attribute

IF EXISTS (select 1 from information_schema.views where table_name='ListFlagsLocation')
drop view ListFlagsLocation

IF EXISTS (select 1 from information_schema.views where table_name='ListFlagsPerson')
drop view ListFlagsPerson

IF EXISTS (select 1 from information_schema.views where table_name='vBooleanFlagDataByCreatedDate')
drop view vBooleanFlagDataByCreatedDate

IF EXISTS (select 1 from information_schema.views where table_name='vCT_IntegerFlagDataPersByCountry')
drop view vCT_IntegerFlagDataPersByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vCTA2_BooleanFlagData_PeopleAtOrganisationsByCountry')
drop view vCTA2_BooleanFlagData_PeopleAtOrganisationsByCountry

IF EXISTS (select 1 from information_schema.views where table_name='vCTB1_vCTA2_Weelky')
drop view vCTB1_vCTA2_Weelky

IF EXISTS (select 1 from information_schema.views where table_name='vCTC1_vCTB1_vCTA2_SumPeopleAtOrgsForProfile')
drop view vCTC1_vCTB1_vCTA2_SumPeopleAtOrgsForProfile

IF EXISTS (select 1 from information_schema.views where table_name='vFNLOrgsToSupress')
drop view vFNLOrgsToSupress

IF EXISTS (select 1 from information_schema.views where table_name='view1')
drop view view1

IF EXISTS (select 1 from information_schema.views where table_name='AllLivePeopleatPartners')
drop view AllLivePeopleatPartners

IF EXISTS (select 1 from information_schema.views where table_name='vOrgList2')
drop view vOrgList2

IF EXISTS (select 1 from information_schema.views where table_name='vOrgList')
drop view vOrgList

IF EXISTS (select 1 from information_schema.views where table_name='Flaglists')
drop view Flaglists

IF EXISTS (select 1 from information_schema.views where table_name='v_DEFAULT_CONSTRAINTS')
drop view v_DEFAULT_CONSTRAINTS

IF EXISTS (select 1 from information_schema.views where table_name='vLeadListing2')
drop view vLeadListing2

IF EXISTS (select 1 from information_schema.views where table_name='vPhoning')
drop view vPhoning

IF EXISTS (select 1 from information_schema.views where table_name='vPhoning2')
drop view vPhoning2

IF EXISTS (select 1 from information_schema.views where table_name='vR_RegPerAndOrgForOrgBooleanFlagGroup_week_country')
drop view vR_RegPerAndOrgForOrgBooleanFlagGroup_week_country

IF EXISTS (select 1 from information_schema.views where table_name='vTradeUpAnalysis')
drop view vTradeUpAnalysis

IF EXISTS (select 1 from information_schema.views where table_name='vVerityElements')
drop view vVerityElements

IF EXISTS (select 1 from information_schema.views where table_name='vUsersByJobFunction')
drop view vUsersByJobFunction

IF EXISTS (select 1 from information_schema.views where table_name='linkedphrases')
drop view linkedphrases

IF EXISTS (select 1 from information_schema.views where table_name='vCommdetailUniquelocations')
drop view vCommdetailUniquelocations

IF EXISTS (select 1 from information_schema.views where table_name='vEmailCommResponse')
drop view vEmailCommResponse

IF EXISTS (select 1 from information_schema.views where table_name='vEmailCommResponseByPerson')
drop view vEmailCommResponseByPerson

IF EXISTS (select 1 from information_schema.views where table_name='vReportGenericDef')
drop view vReportGenericDef

IF EXISTS (select 1 from sysobjects where name='locatorStats' and type='P')
drop procedure locatorStats

IF EXISTS (select 1 from sysobjects where name='locatorStatsAll' and type='P')
drop procedure locatorStatsAll

IF EXISTS (select 1 from sysobjects where name='modListWriteOutBatches' and type='P')
drop procedure modListWriteOutBatches

IF EXISTS (select 1 from sysobjects where name='modRegisterContents' and type='P')
drop procedure modRegisterContents

IF EXISTS (select 1 from sysobjects where name='modTransferStatusSince' and type='P')
drop procedure modTransferStatusSince

IF EXISTS (select 1 from sysobjects where name='updDataSource' and type='P')
drop procedure updDataSource

IF EXISTS (select 1 from sysobjects where name='updDataSourceDTSParams' and type='P')
drop procedure updDataSourceDTSParams

/*Remove entries from the schemaTable */
delete schematablebase
from schematablebase s
	left join sysobjects o on s.entityName = o.name
where o.name is null


IF EXISTS (select 1 from sysobjects where name='modEntityList' and type='P')
drop procedure modEntityList
