/* 
Add WITH STOPLIST OFF after the test DB is upgraded to SQL 2008
example: CREATE FULLTEXT INDEX ON [dbo].vOrganisationSearch (searchableString) KEY INDEX vOrganisationSearch_PK WITH STOPLIST OFF;

Saved the drop statements just in case indexes need to be rebuilt
DROP FULLTEXT INDEX ON [dbo].textMultipleFlagData
DROP FULLTEXT INDEX ON [dbo].product
DROP FULLTEXT INDEX ON [dbo].trngModule
DROP FULLTEXT INDEX ON [dbo].files
DROP FULLTEXT INDEX ON [dbo].phrases
DROP FULLTEXT INDEX ON [dbo].vDiscussionMessageSearch
DROP FULLTEXT INDEX ON [dbo].vEntityCommentSearch
DROP FULLTEXT INDEX ON [dbo].phrases
DROP FULLTEXT INDEX ON [dbo].vLocationSearch
DROP FULLTEXT INDEX ON [dbo].vPersonSearch
DROP FULLTEXT INDEX ON [dbo].vDiscussionGroupSearch
DROP FULLTEXT INDEX ON [dbo].vOrganisationSearch
DROP FULLTEXT INDEX ON [dbo].vPhrasesSearch
DROP FULLTEXT INDEX ON [dbo].fileFamily
DROP FULLTEXT INDEX ON [dbo].fileType
DROP FULLTEXT INDEX ON [dbo].fileTypeGroup
*/
-- Create Full Text Catalog
IF NOT EXISTS (SELECT 1 FROM sys.fulltext_catalogs WHERE [name] = 'FullTextCat')
	CREATE FULLTEXT CATALOG FullTextCat AS DEFAULT	
GO

ALTER FULLTEXT CATALOG FullTextCat 
REBUILD WITH ACCENT_SENSITIVITY = OFF;
GO

declare @stopListSql nvarchar(20)
declare @sql nvarchar(max)
declare @isSqlServer2008 bit, @isSqlServer2008orAbove bit

select @isSqlServer2008orAbove = case when CONVERT(int, SUBSTRING(CONVERT(varchar(15), SERVERPROPERTY('productversion')), 0, CHARINDEX('.', CONVERT(varchar(15), SERVERPROPERTY('productversion'))))) < 10 then 0 else 1 end
select @isSqlServer2008 = case when CONVERT(int, SUBSTRING(CONVERT(varchar(15), SERVERPROPERTY('productversion')), 0, CHARINDEX('.', CONVERT(varchar(15), SERVERPROPERTY('productversion'))))) = 10 then 1 else 0 end


/* if 2008 server, set to run in 2008 comptability mode.. could be running in 2005 compatibiliy mode, but full text indexing will then not work */
if (@isSqlServer2008 = 1 and exists (select 1 from sys.databases where name=db_name() and compatibility_level < 100))
begin
	select @sql = 'ALTER DATABASE '+db_name()+' SET COMPATIBILITY_LEVEL = 100'
	exec sp_executesql @sql
end

select @stopListSql = ''
if (@isSqlServer2008orAbove = 1) 
begin
	select @stopListSql = ' WITH STOPLIST OFF'
end

-- Create a full text index on organisation
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vOrganisationSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vOrganisationSearch (searchableString) KEY INDEX vOrganisationSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create a full text index on location
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vLocationSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vLocationSearch (searchableString) KEY INDEX vLocationSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on person
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vPersonSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vPersonSearch (searchableString) KEY INDEX vPersonSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on discussionGroup
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vDiscussionGroupSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vDiscussionGroupSearch (searchableString) KEY INDEX vDiscussionGroupSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on entityComment
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vEntityCommentSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vEntityCommentSearch (searchableString) KEY INDEX vEntityCommentSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on discussionMessage
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vDiscussionMessageSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vDiscussionMessageSearch (searchableString) KEY INDEX vDiscussionMessageSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on phrases
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].phrases'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].phrases (phraseText) KEY INDEX IX_Phrases_Ident'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on vPhrasesSearch
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].vPhrasesSearch'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].vPhrasesSearch (phraseText) KEY INDEX vPhrasesSearch_PK'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on product
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].product'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].product (sku) KEY INDEX PK_Product'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on trngModule
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].trngModule'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].trngModule (moduleCode) KEY INDEX PK_moduleID'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on files
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].files'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].files (filename,name,url,description) KEY INDEX primarykey'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on fileTypes
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].fileType'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].fileType (type,path) KEY INDEX primarykey'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on fileTypeGroup
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].fileTypeGroup'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].fileTypeGroup (heading) KEY INDEX primarykey'+@stopListSql;
	exec sp_executesql @sql
end

-- Create full text index on fileFamily
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].fileFamily'))
begin
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].fileFamily (title) KEY INDEX PK_fileFamily'+@stopListSql;
	exec sp_executesql @sql
end

-- Add a single column unique key and create full text index on textMultipleFlagData
IF NOT EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].textMultipleFlagData'))
begin
	CREATE UNIQUE INDEX rowID_idx ON textMultipleFlagData (rowID)
	select @sql = 'CREATE FULLTEXT INDEX ON [dbo].textMultipleFlagData (data) KEY INDEX rowID_idx'+@stopListSql;
	exec sp_executesql @sql
end
GO
