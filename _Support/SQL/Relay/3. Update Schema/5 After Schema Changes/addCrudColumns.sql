/* NJH 2012/05/18 go through the schema table, and add lastUpdated,lastUpdatedBy and lastUdpatedByPerson on tables where 
	these columns don't exist 
	NJH 2015/10/02 - JIRA Prod2015-110 added createdByPerson as a standard field to capture who created the record, particularly for portal users	
	WAB 2017-01-06 - added relatedFile and reformatted query 
*/
declare @tablename varchar(50)
declare @sqlcmd varchar(4000)

declare entity_cursor cursor for
    select distinct entityName from schemaTable s inner join sysobjects so
		on s.entityName = so.name and so.type='u'
	where (s.flagsExist = 1
		or s.entityName like '%flagData')
		and entityName not in ('commDetail','vEntityName') --specifically exclude these tables

	UNION

	select 	
		entityName 
	from 
		(values 
			  ('fileType')
			, ('FundRequestType')
			, ('QuizAnswer')
			, ('EmailDef')
			, ('ProductCategory')
			, ('trngModule')						/* 2013-08-28	YMA	Case 436702 lastUpdatedByPerson was missing from trngmodule */
			, ('trngModuleSetModule')				/* 2016-05-13	GCC	Case 449473 lastUpdatedByPerson was missing from trngModuleSetModule */		
			, ('relatedFile')						/* 2017-01-06	WAB RT-125 Some systems did not have lastupdatedByPerson on this table because flagsExist was 0 */		
		) as x (entityName)

open entity_cursor
fetch next from entity_cursor into @tablename

while @@FETCH_STATUS = 0
begin
	select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@tablename+''' and column_name=''lastUpdated'')
		begin
			alter TABLE [dbo].['+@tablename+'] add [lastUpdated] datetime NULL
			ALTER TABLE [dbo].['+@tablename+'] ADD  CONSTRAINT [DF_'+@tablename+'_lastUpdated]  DEFAULT (getdate()) FOR [lastUpdated]
		end'
    exec (@sqlcmd)
    select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@tablename+''' and column_name=''lastUpdatedBy'')  
      alter TABLE [dbo].['+@tablename+'] add [lastUpdatedBy] int NULL'
    exec (@sqlcmd)
    select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@tablename+''' and column_name=''lastUpdatedByPerson'')  
      alter TABLE [dbo].['+@tablename+'] add [lastUpdatedByPerson] int NULL'
    exec (@sqlcmd)
    select @sqlcmd='if not exists (select * from information_schema.columns where table_name='''+@tablename+''' and column_name=''createdByPerson'')  
      alter TABLE [dbo].['+@tablename+'] add [createdByPerson] int NULL'
    exec (@sqlcmd)
    
 	if (@tablename not like '%flagdata%' or @tablename = 'eventFlagData') /* eventFlagData does have lastUpdatedByPerson */
		exec Generate_MRAuditDel @tablename = @tablename
	fetch next from entity_cursor into @tablename
end

close entity_cursor;
deallocate entity_cursor;
GO