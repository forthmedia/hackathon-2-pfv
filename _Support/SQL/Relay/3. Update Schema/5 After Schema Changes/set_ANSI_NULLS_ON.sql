/*
WAB 2017-02-21

Recompile all procedures which have been created with ANSI_NULLS OFF

*/

declare db_cursor CURSOR FOR  
SELECT 
	-- name = OBJECT_NAME([object_id]), uses_ansi_nulls, 
	dbo.regexreplace (OBJECT_DEFINITION (object_id), 'create\s*proc',  'alter proc') as definition
  FROM 
	sys.sql_modules m
		inner join
	sysobjects o on m.object_id = o.id
where 
	uses_ansi_nulls = 0
	and xtype in ('p')

declare @sql nvarchar(max)

OPEN db_cursor;
FETCH NEXT FROM db_cursor INTO @sql
WHILE @@FETCH_STATUS = 0
BEGIN   
	SET ANSI_NULLS ON       
	exec (@sql)

	FETCH NEXT FROM db_cursor INTO @sql;
END

CLOSE db_cursor;
DEALLOCATE db_cursor;       

