/* NJH 2015/09/22 Jira Prod2015-34 - create entity views for any entity that has flagsExist=1 and 
	where the views don't already exist */
declare @viewname sysname
declare @entityName sysname

declare entity_cursor cursor for
    select 
		entityName as entityName, 'v'+entityName as viewName 
	from schemaTable s
		inner join information_schema.tables t on s.entityName = t.table_name
		left join information_schema.tables v on 'v'+s.entityName = v.table_name
    where s.flagsExist = 1
		and v.table_name is null
    order by s.entityName
    
open entity_cursor
fetch next from entity_cursor into @entityName, @viewName

	while @@FETCH_STATUS = 0
	begin
		exec createFlagView @entityName=@entityName,@viewName=@viewName
		fetch next from entity_cursor into @entityName, @viewName
	end
	
close entity_cursor;
deallocate entity_cursor;