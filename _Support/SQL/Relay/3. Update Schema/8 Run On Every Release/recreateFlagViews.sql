/*
WAB 2017-01-23 PROD2016-3299 Recreate All Flag Views
For use after upgrades where fields might get added/deleted from base tables
*/


declare @viewName sysname, @tablename sysname, @includeBooleans bit
declare  flagViews cursor for 


select 
	entityname tableName,
	name as viewName,
	dbo.regExIsMatch ('withBoolean',name,1) as includeBooleans
from 
	sysobjects o
	inner join
	syscomments c on o.id = c.id
	inner join
	schemaTable s on s.entityName = replace (right (name, len(name) - 1), 'withboolean' ,'')
where 
	type = 'v' 
	and left (name,1) = 'v' 
	and right (name,4) <> '_bak'
and text like 'create view  dbo.%_tmp%'   -- All views created with createFlagView have this form (really ought to have a distinct comment in there)


open flagViews
fetch next from flagViews into @tablename, @viewName, @includeBooleans

	while @@FETCH_STATUS = 0
	begin
		Print 'recreate ' + @viewName
		exec createFlagView @tablename, @viewName, @includeBooleans


		fetch next from flagViews into @tablename, @viewName, @includeBooleans
	end

	
close flagViews;
deallocate flagViews;


