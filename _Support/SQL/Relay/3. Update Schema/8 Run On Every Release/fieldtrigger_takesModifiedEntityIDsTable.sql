/*
WAB 2016-04-19
Long Long Ago I changed the way that field triggers worked and what parameters they took.
(originally they operated on a single row at a time, new versions are set based)
In order to keep compatibility with old versions I added a column to the fieldTrigger table which tells me which version is being used.
This field is populated automatically, but needs to be nulled if a field trigger is changed from version 1 to version 2
This query will update the fieldTrigger table if it detects a recent update to a field trigger

(Actually the whole thing is probably nonsense - some developer worked out that these procedures can just reference the #I and #D tables that we create, so there is actually no need to pass in a separate table of entityids)
*/

update
	fieldtrigger 
set 
	takesModifiedEntityIDsTable = null	
from
	fieldtrigger 
		inner join 
	sysobjects on triggername = name
where 
	refdate  > getdate() - 1
	
