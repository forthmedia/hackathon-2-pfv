/*
WAB 2013-06-05 Make sure that all Del Tables are inSynch
Should be run at end of every release
*/

declare @tablename sysname
declare @mnemonic varchar(10)

declare entity_cursor cursor for
    select 
    	parenttable.table_name, mnemonic 
    from information_schema.tables deltable
    			inner join  
    	 information_schema.tables parenttable on deltable.table_name = parenttable.table_name + 'del'
    		inner join schemaTable s on parenttable.table_name = s.entityName   
    where deltable.table_Name like '%del'
    order by parenttable.table_name
    
open entity_cursor
fetch next from entity_cursor into @tablename, @mnemonic

	while @@FETCH_STATUS = 0
	begin
		print @tablename
		if (@mnemonic is not null)
			exec Generate_MRAudit @tablename = @tablename, @mnemonic = @mnemonic
		else
			exec Generate_MRAuditDel @tablename = @tablename
		fetch next from entity_cursor into @tablename, @mnemonic
	end

	
close entity_cursor;
deallocate entity_cursor;

/* re-generate the vEntityName view */
--exec generate_vEntityName
exec synchEntityName @tablename=null
