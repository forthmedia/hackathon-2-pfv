<cfcomponent extends="testbox.system.BaseSpec" >

<cfscript>




/**
*
* @hint Checks behaviour of nulling fields
*
*/ 
function beforeTests () {
	/* ensure that there is at least one lead */
	var thecount= new Query(sql = "select count(leadid) as thecount from lead").execute().getResult().thecount;		
	if (thecount is 0) {
		variables.entityUtilities = new "unittests.merge.entityUtilities" ();	
		variables.entityUtilities.createANewEntityrecord ("lead");
	}
}


function testNull_numeric () {

	// get a core numeric field
	var field = new Query(sql = "select name from vfieldmetadata where tablename = 'lead' and dataType = 'numeric' and source = 'core' and foreignKey is null and isnullable = 1").execute().getResult().name;		
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		

	lead = application.com.entities.load('lead', leadid);
	var currentValue = 	lead.get (field);

	lead.set (field,1);
	lead.save ();

	lead = application.com.entities.load('lead', leadid);
	
	lead.set (field,"");
	lead.save ();

	lead = application.com.entities.load('lead', leadid);

	lead.set (field,currentValue);

	lead.save ();
}


function testNull_numeric_notnullable_shouldFail () {

	// get a core numeric field
	var field = new Query(sql = "select name from vfieldmetadata where tablename = 'lead' and dataType = 'numeric' and source = 'core' and foreignKey is null and isnullable = 0 and name not like 'created%' and name not like 'lastup%' and name <> 'leadid' ").execute().getResult().name;		
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		

	lead = application.com.entities.load('lead', leadid);
	writedebug(field);
	writedebug("<BR>");
			
	var currentValue = 	lead.get (field);

	lead.set (field,1);
	lead.save ();

	lead = application.com.entities.load('lead', leadid);
	
	lead.set (field,"");

	$assert.throws( target =  function(){ lead.save (); }, regex = "Not Nullable" );

}


function testNull_bit () {

	// get a core numeric field
	var field = new Query(sql = "select name from vfieldmetadata where tablename = 'lead' and dataType = 'bit' and source = 'core' and foreignKey is null and isnullable = 1").execute().getResult().name;		
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		

	lead = application.com.entities.load('lead', leadid);
	writedebug(field);
	writedebug("<BR>");
			
	var currentValue = 	lead.get (field);

	lead.set (field,1);
	lead.save ();

	lead = application.com.entities.load('lead', leadid);
	writedebug(lead.get (field));
	writedebug("<BR>");
	
	lead.set (field,"");
	lead.save ();

	lead = application.com.entities.load('lead', leadid);
	writedebug(lead.get (field));
	writedebug("<BR>");

	lead.set (field,currentValue);

	lead.save ();
}


function test_ValidationError_SetABitToAString () {

	// get a core numeric field
	var field = new Query(sql = "select name from vfieldmetadata where tablename = 'lead' and dataType = 'bit' and source = 'core' and foreignKey is null").execute().getResult().name;		
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		

	lead = application.com.entities.load('lead', leadid);
			
	var currentValue = 	lead.get (field);

	lead.set (field,"A");

	validationResult = lead.validate ();
	$assert.isEqual (arrayLen(validationResult.validationErrors),1,"Should be a validation error");

	$assert.throws( target =  function(){ lead.save (); }, regex = "Must Be Boolean" );


}

function test_ValidationError_SetANumericToAString () {

	// get a core numeric field
	var field = new Query(sql = "select name from vfieldmetadata where tablename = 'lead' and dataType = 'numeric' and source = 'core' and foreignKey is null").execute().getResult().name;		
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		

	lead = application.com.entities.load('lead', leadid);
			
	var currentValue = 	lead.get (field);

	lead.set (field,"A");

	validationResult = lead.validate ();
	$assert.isEqual (arrayLen(validationResult.validationErrors),1,"Should be a validation error");

	$assert.throws( target =  function(){ lead.save (); }, regex = "Must Be Numeric" );


}


function testLookupListTextID () {

	// get a lookup field on lead table
	var field = new Query(sql = "select * from vfieldmetadata where tablename = 'lead' and picklistvaluessql like '%lookup%'").execute().getResult().name;		
	var textIDField =  rereplacenocase (field,"id$","textid");

	/* get a lead */
	var leadID = new Query(sql = "select min(leadid) as leadid from lead").execute().getResult().leadid;		
	lead = application.com.entities.load('lead', leadid);

	/* check that doing a get for fieldName without the ID will bring back textID */
	var IDValue = 	lead.get (field);
	var textIDValue = lead.get (textIDField);

	if (IDValue is not "") {
		$assert.isEqual (textIDValue,application.lookup[IDValue].lookuptextid,"First Test");
	}	else {
		$assert.isEqual (textIDValue,"","First Test");	
	}


	/* change value (using ID) */
	var sql = "select * from lookuplist where fieldname = 'LEAD#field#' "; 
	if (IDValue is not "") {
		sql &= " and lookupid <> #IDValue#";
	} 
	var newStatus = new Query(sql = sql ).execute().getResult();		
	lead.set (field,newStatus.lookupID);


	var IDValue = 	lead.get (field);
	var textIDValue = lead.get (textIDField);

	$assert.isEqual (textIDValue,newStatus.lookuptextid,"Second Test");
	$assert.isEqual (IDValue,newStatus.lookupID,"Second Test");


	/* change value (using TextID) */
	var newvalue = new Query(sql = "select * from lookuplist where fieldname = 'LEAD#field#' and lookupid <> #IDValue#").execute().getResult();		
	lead.set (textIDField,newStatus.lookupTextID);

	var IDValue = 	lead.get (field);
	var textIDValue = lead.get (textIDField);

	$assert.isEqual (textIDValue,newStatus.lookuptextid,"Third Test");
	$assert.isEqual (IDValue,newStatus.lookupID,"Third Test");



	lead.save ();


}

/* allows writing some debug while developing, but won't muck up automated unit tests which need to return pure XML (or whatever)*/
function writeDebug (string) {
	if (not structKeyExists (url,"reporter")) {
		writeOutput (string);
	}
}

</cfscript>

</cfcomponent>


	