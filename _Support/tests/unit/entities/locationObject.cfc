<cfcomponent extends="testbox.system.BaseSpec" >

<cfscript>




/**
*
* @hint Checks that location.countryName and location.isocode are exposed and change if countryid changes
*
*/ 
function testLocationCountryName () {

	var locationid = new Query(sql = "select max(locationid) as locationid from location").execute().getResult().locationid;		
	locationObject = application.com.entities.load('location', locationid);
	
	var countryID = locationObject.getCountryID();
	var countryqry = new Query(sql = "select * from country where countryid = #countryid# ").execute().getResult();				

	var anothercountryqry = new Query(sql = "select * from country where countryid <> #countryid# and isoCode is not null").execute().getResult();				

	$assert.isEqual( locationObject.countryName, countryQry.CountryDescription, "") ; 
	$assert.isEqual( locationObject.getCountryName(), countryQry.CountryDescription, "") ; 
	$assert.isEqual( locationObject.countryIsoCode, countryQry.isocode, "") ; 
	$assert.isEqual( locationObject.getCountryISOCode(), countryQry.isocode, "") ; 


	// change country
	locationObject.setCountryID (anothercountryqry.countryid);

	$assert.isEqual( locationObject.countryName, anotherCountryQry.CountryDescription, "") ; 
	$assert.isEqual( locationObject.getCountryName(), anotherCountryQry.CountryDescription, "") ; 
	$assert.isEqual( locationObject.countryIsoCode, anotherCountryQry.isocode, "") ; 
	$assert.isEqual( locationObject.getCountryISOCode(), anotherCountryQry.isocode, "") ; 


}


</cfscript>

</cfcomponent>


