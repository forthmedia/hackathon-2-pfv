<cfcomponent extends="testbox.system.BaseSpec" >

<cfscript>




/**
*
* @hint Checks that getMagicnumber() works
*
*/ 
function testPersonMagicNumber () {

	var personid = new Query(sql = "select max(personid) as personid from person").execute().getResult().personid;		
	personObject = application.com.entities.load('person', personid);

	$assert.isEqual( personObject.getMagicNumber (), application.com.login.getMagicNumber(entityID =personid, entityTypeID=0) , "") ; 

}



</cfscript>

</cfcomponent>


