<cfcomponent extends="testbox.system.BaseSpec" >

	<!---
		Test the operation of cf_includeJavscriptOnce
		Particularly the behaviour of automatic extending and translations
		Developed after CASE 446496

		Note use of Inhead attribute to get the output inline (ie not in the htmlhead) so that we can read it easily

	--->


	<cffunction name="beforeTests">

		<cfset variables.files = {
				justcode = {location="code"
							,name = "unitTest_justCode.js"
							,content = "  phr.thankyou_justcode  "
							}
				,justrelay = {location="relay"
							,name = "unitTest_justRelay.js"
							,content = "  phr.thankyou_justrelay  "
							}
				,extendedrelay = {location="relay"
							,name = "unitTest_extended.js"
							,content = " phr.thankyou_extendedrelay  "
							}
				,extendedcode = {location="code"
							,name = "unitTest_extended.js"
							,content = "   phr.thankyou_extendedcode   "
							}
			}>

		<cfloop collection = "#variables.files#" item="key">
			<cfset filedetails = variables.files[key]>
			<cfif fileDetails.location is "relay">
				<cfset filedetails.relativePath = "/" & filedetails.name >
			<cfelse>
				<cfset filedetails.relativePath = "/" & filedetails.location & "/" & filedetails.name >
			</cfif>

			<cfset filedetails.absolutePath = application.paths [filedetails.location] & "/"  & filedetails.name >
			<cffile action="write" file="#filedetails.absolutePath#" output="#filedetails.content#">
		</cfloop>

		<cfset structclear(application.phrasesInJavascript)>

	</cffunction>


	<cffunction name="setup">
		<cfif structKeyexists (request, "includeJavascriptOnce")>
			<cfset structClear (request.includeJavascriptOnce)>
		</cfif>
	</cffunction>


	<cffunction name="afterTests">

		<cfloop collection = "#variables.files#" item="key">
			<cfset filedetails = variables.files[key]>
			<cfif fileExists (filedetails.absolutePath)>
				<cfset filedelete(filedetails.absolutePath)>
			</cfif>
		</cfloop>

		<cfset application.com.request.clearHeaderBuffer()>

	</cffunction>


	<!--- This test reproduces bug in Case 446496
		enable a custom template to be loaded and translated even if it isn't overriding a core js file
	--->

	<cffunction name="callincludeJavascriptOnce">
		<cfargument name="file" required=true>
		<cfargument name="ajax" default = "true">

		<cfif ajax>
			<cfset url.returnFormat = "plain">
		<cfelse>
			<cfset structDelete (url,"returnFormat")>
		</cfif>

		<cfset var result = "">
		<cfsavecontent variable = "result">
			<cf_includeJavascriptOnce Template="#file#" useHTMLHead="false"  translate=true>
		</cfsavecontent>

		<cfreturn result>

	</cffunction>


	<cffunction name="test_LoadAndTranslateACustomJSFile">
		<cfargument name="ajax" default = "false">

		<cfset var result = callincludeJavascriptOnce (variables.files.justCode.relativePath,ajax)>

		<!--- We should check for the link rel and also a phrase output--->
		<cfset $assert.includes (result, "phr['thankyou_justCode']")>


	</cffunction>

	<cffunction name="test_LoadAndTranslateACoreJSFile">
		<cfargument name="ajax" default = "false">

		<cfset var result = callincludeJavascriptOnce (variables.files.justRelay.relativePath,ajax)>

		<cfset $assert.includes (result, "phr['thankyou_justRelay']")>


	</cffunction>

	<cffunction name="test_LoadAndTranslateAnExtendedCoreJSFile">
		<cfargument name="ajax" default = "false">

		<cfset var result = callincludeJavascriptOnce (variables.files.extendedrelay.relativePath,ajax)>
		<cfset $assert.includes (result, "phr['thankyou_extendedRelay']")>
		<cfset $assert.includes (result, "phr['thankyou_extendedCode']")>


	</cffunction>


	<cffunction name="test_LoadAndTranslateACustomJSFileWhichHAsACoreVersionButWeDontWantTheCoreToLoad">
		<cfargument name="ajax" default = "false">

		<cfset var result = callincludeJavascriptOnce (variables.files.extendedCode.relativePath,ajax)>
		<cfset $assert.notincludes (result, "phr['thankyou_extendedRelay']")>
		<cfset $assert.includes (result, "phr['thankyou_extendedCode']")>

	</cffunction>


	<cffunction name="test_LoadAndTranslateACustomJSFile_ajax">

		<cfset test_LoadAndTranslateACustomJSFile(true)>

	</cffunction>

	<cffunction name="test_LoadAndTranslateACoreJSFile_ajax">

		<cfset test_LoadAndTranslateACoreJSFile(true)>

	</cffunction>

	<cffunction name="test_LoadAndTranslateAnExtendedCoreJSFile_ajax">

		<cfset test_LoadAndTranslateAnExtendedCoreJSFile(true)>

	</cffunction>


	<cffunction name="test_LoadAndTranslateACustomJSFileWhichHAsACoreVersionButWeDontWantTheCoreToLoad_Ajax">

		<cfset test_LoadAndTranslateACustomJSFileWhichHAsACoreVersionButWeDontWantTheCoreToLoad(true)>

	</cffunction>


</cfcomponent>