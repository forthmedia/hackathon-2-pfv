<cfcomponent extends="testbox.system.BaseSpec">

	<cffunction name="testisFloat1" returntype="void" access="public">

		<cfscript>
			objComponent = createObject("component","com.globalFunctions");	
			intExpected = "true";
			intActual = objComponent.isFloat(eInt="3.2");

			$assert.isEqual(intExpected,intActual);		
		</cfscript>	

	</cffunction>
	
	<cffunction name="testisFloat2" returntype="void" access="public">

		<cfscript>
			objComponent = createObject("component","com.globalFunctions");	
			intExpected = "false";
			intActual = objComponent.isFloat(eInt="test.");

			$assert.isEqual(intExpected,intActual);		
		</cfscript>	

	</cffunction>

</cfcomponent>