<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-10-19 	To Test that security system within webservices\callWebservice is working correctly by making HTTP requests to self 
				Phase 1:  	Checks for that encrypted arguments are working
							Checks that argument collections are supported for encrypted arguments (PROD2016-1444)
				
 --->

<cffunction name="beforeTests">

	<!--- 	Move a test WS cfc into the webservices directory
			Stored as .cfc_ and then renamed so that does testBox does not attempt to run it as a test suite
	 --->
	<cfset var currentDirectory = 	reverse(listRest(reverse(GetCurrentTemplatePath()),"\"))>
	<cfset variables.webserviceName = 	"securityTestsWS">
	<cfset variables.webserviceFile = 	"#webserviceName#.cfc">
	
	<cffile action="copy" source="#currentDirectory#/#webserviceFile#_" destination="#application.paths.relay#/webservices">
	<cffile action="rename" source="#application.paths.relay#/webservices/#webserviceFile#_" destination="#application.paths.relay#/webservices/#webserviceFile#">

	<!--- Get session cookie --->
	<cfhttp url= #request.currentSite.protocolAndDomain# result="result">
	
	<cfset variables.jSessionID = getCookieFromResponse (result, "jsessionID")>

</cffunction>


<cffunction name="afterTests">
	<cffile action="delete" file="#application.paths.relay#/webservices/#variables.webServiceFile#">
</cffunction>


<cfscript>

/* Make a call with correctly encrypted argument */
function test_encryptedArgument () {
	
	var argumentValue = 321;
	var result = makeCall (methodName = "functionWithEncryptedAttribute", jSessionID = variables.jSessionID, args = {encryptedArgument= application.com.security.encryptVariableValue ('encryptedArgument',argumentValue) });
	
	$assert.isEqual( argumentValue , result.fileContent ) ; 			

}


/* Make a call with correctly encrypted argument in argument collection  */
function test_encryptedArgument_usingJSONArgumentCollection () {
	
	var argumentValue = 567;
	var argumentCollection = {encryptedArgument = application.com.security.encryptVariableValue ('encryptedArgument',argumentValue)};
	var argumentCollectionJSON = serializeJson (argumentCollection );
	var result = makeCall (methodName = "functionWithEncryptedAttribute", jSessionID = variables.jSessionID, args = {argumentCollection = argumentCollectionJSON});

	$assert.isEqual( argumentValue , result.fileContent ) ; 			

}


/* Make a call with correctly encrypted argument in argument collection but make the argument collection a deep structure  */
function test_encryptedArgument_usingDeepJSONArgumentCollection () {
	
	var argumentValue = 567;
	var argumentCollection = {encryptedArgument = application.com.security.encryptVariableValue ('encryptedArgument',argumentValue), x = {y = 1, z = {a = 1}}};
	var argumentCollectionJSON = serializeJson (argumentCollection );
	var result = makeCall (methodName = "functionWithEncryptedAttribute", jSessionID = variables.jSessionID, args = {argumentCollection = argumentCollectionJSON});

	$assert.isEqual( argumentValue , result.fileContent ) ; 			

}



/* Make a call with incorrectly encrypted argument */
function test_nonencryptedArgument () {
	
	var argumentValue = 3;
	var result = makeCall (methodName = "functionWithEncryptedAttribute", jSessionID = variables.jSessionID, args = {encryptedArgument= argumentValue });
	
	
	$assert.match( result.fileContent, "parameter error" )  ; 			

}


/* Make a call where login is required */
function test_LoginRequired () {
	
	var result = makeCall (methodName = "functionRequiringLogin", jSessionID = variables.jSessionID);
	
	$assert.match( result.fileContent, "Login Required" )  ; 			

}


/* generic function for calling http with cookie */
function makeCall (   required string methodname 
					, string jSessionID = ""
					, struct  args = {}) {
	
	var theURL = "#request.currentSite.protocolAndDomain#/webservices/callWebservice.cfc";

	var httpRequest = new http().setURL (theURL);
  		httpRequest.addParam(type="URL", name="method", value="callwebservice");
  		httpRequest.addParam(type="cookie", name="jsessionID", value="#jSessionID#", encoded = "false");
  		httpRequest.addParam(type="URL", name="webservicename", value="#webserviceName#");
  		httpRequest.addParam(type="URL", name="returnFormat", value="plain");	
  		httpRequest.addParam(type="URL", name="methodName", value="#methodName#")	;

	for (local.item in args) {
		httpRequest.addParam(type="URL",name="#local.item#",value="#args[local.item]#")	;
	}
	
	
	return httpRequest.send().getPrefix();

}


function getCookieFromResponse (struct response, string cookieName) {

	var cookieValue = "";

	for (key in response.responseHeader["set-cookie"]) {
		local.cookie = response.responseHeader["set-cookie"][key];
		var findArray = application.com.regExp.refindAllOccurrences ("#cookieName#=(.*?);", local.cookie, {"1" = "cookie"});
		if (arrayLen (findArray)) {
			cookieValue = findArray [1].cookie;
			break;
		}
	}
	
	return cookieValue;
}

</cfscript>


</cfcomponent>