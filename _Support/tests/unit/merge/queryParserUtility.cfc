	<!--- 
		WAB Functions copied from my TFQO project which I wanted in my unit tests.
		Plan to move them somewhere else at some point
		 Note that only reason it extends testbox is so that it can be skipped
	--->


<cfcomponent output=false skip = true extends="testbox.system.BaseSpec">
	<cffunction name="parseQueryByRegExp" access="public" returnType="struct" output="false">
		<cfargument name="queryString" type="string" required="true">

		<cfset var result = {isOK = false}>

		<cfset var queryStringNoComments = application.com.regExp.removeAllComments (string = queryString, preserveLength = false)>

		<cfset var findAllBrackets = application.com.regexp.getMatchedOpenersAndClosers (string = queryStringNoComments, openingRegExp="\(", closingRegExp="\)")>
		<cfset var regExp = "(select\s*(distinct\s*)?(?:top\s*([0-9]*))?(.*?))\s(from\s(.*?))(\swhere\s(.*?))?(\sgroup\sby\s(.*?))?(\shaving\s(.*?))?(\sorder\sby\s(.*))?\Z">
		<cfset var groups = {1="Select",2="distinct",3="top",4="SelectList",5="From",6="FromTables", 7="where",8="whereClause", 9="groupby", 10="groupbylist",11="having",12="havingClause",13="orderby", 14="orderbyList"}>

		<cfset var find = application.com.regExp.refindAllOccurrences(reg_expression = regexp, string = queryStringNoComments, excludeArray= findAllBrackets,groupnames = groups)>

		<cfif arrayLen(find)>
			<cfset result = find[1]>
			<cfset result.isOK = true>
			<cfif result.orderby is not "">
				<cfset result.withoutOrderby = replacenocase(result.string,result.orderby,"")>
			<cfelse>
				<cfset result.withoutOrderby = result.string>
			</cfif>
			<cfset result.body = replacenocase(result.withoutOrderby,result.select,"")>

		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="parseASelectList" output="false">
		<cfargument name="selectList">
		<cfscript>
			var	aliasToSqlMapping = {};
			var columnArray = [];

			// This regular expression finds all fields of the form  TABLEALIAS and TABLEALIAS.FIELDNAME   (ie those without an AS)
			// WAB 2016-11-11 altered so that SQL and alias are the same (so we have fully qualified aliases)
			var regExp = "(?:\A|,)\s*(((?:[0-9A-Z_\.\*]*\.)?[0-9A-Z_\*]*))\s*(?=,|\Z)";   /* added \* so will find * as a select item, not sure what to do with it!) */
			var groupNames = {1 = "sqlExpression",2="alias"};

			/* we have to deal with fields which include brackets (ie functions) because these can contain strings of the form   ,xxxx,  which trick my regular expression which is looking for a very similar pattern
				Items containing functions will (or should) always have an AS, so it doesn't matter that I blank these out for my first search
			 */
			var positionOfBrackets = application.com.regexp.getMatchedOpeningAndClosingTags(selectList,"\(","\)");

			var fieldsArray = application.com.regexp.refindAllOccurrences(reg_expression = regExp,string = selectList, groupNames =  groupnames, excludeArray = positionOfBrackets, excludeArrayReplacementCharacter = "*");

			// Now blank out all the items which we have found so that we can look for the items with aliases
			// I am sometimes left with a leading comma, so remove that as well
			// No longer have to worry about brackets - I hope!

			var blankedOutQueryString = application.com.regexp.ReplaceByPositionArray(inputString = selectList,positionArray = fieldsArray, preserveLength = true);
			blankedOutQueryString = reReplace(blankedOutQueryString,"\A\s*,","","ALL");

			// This regular expression finds all fields of the form  FIELDNAME AS ALIAS and TABLE.FIELDNAME  AS ALIAS (ie those with an AS)
			var regExp = "(?<=\A|,)(.*?)\s+AS\s+([0-9A-Z_]*?)\s*(?=,|\Z)";
			groupNames = {1 = "sqlExpression", 2 = "alias"};
			var aliasedFieldsArray = application.com.regexp.refindAllOccurrences(reg_expression = regExp,string = blankedOutQueryString, groupnames = groupnames, useJavaMatch = true);  /* this regular expression needs to use the javaMatch since has synatx unsupported in CF */


			// now merge the two together and sort back into the original order
			fieldsArray = application.com.structureFunctions.arrayMerge(fieldsArray,aliasedFieldsArray);
			fieldsArray = application.com.structureFunctions.arrayOfStructsSort (fieldsArray,"position");

			arrayEach(fieldsArray,function(item ) {
				var thisMapping = {SQL = arguments.item.sqlexpression, tableAliases = []};
				thisMapping.tableAliases = getTableAliasesFromSQLString (arguments.item.sqlexpression);
				aliasToSqlMapping[arguments.item.alias] = thisMapping;
				arrayAppend(columnArray,arguments.item.alias);
			}) ;


		</cfscript>

		<cfset var result = {aliasToSqlMapping = aliasToSqlMapping, columnArray = columnArray}>

		<cfreturn result>
	</cffunction>


	<cffunction name="getTableAliasesFromSQLString" output="false" returnType ="array">
		<cfargument name="string">

		<cfscript>
			var result = [];
			var lookForTableAlias = application.com.regExp.refindAllOccurrences ("(?:\A|\s|\()([A-Z_][0-9A-Z_]*?)\.[A-Z_]",string,{1="tablealias"});
			arrayEach (lookForTableAlias,function (aliasItem ) {
				/* TBD this can give duplicates */
				arrayAppend(result,aliasItem.tableAlias);
			});

			return result;
		</cfscript>

	</cffunction>

</cfcomponent>