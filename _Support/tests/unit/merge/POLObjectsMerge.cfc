<!--- 

	WAB 2016-1-18 

   	Tests whether the merge system can handle POL entityObjects
	Deals with:
		No merge struct - therefore merge fields come from relayCurrentUser
		personID/locationID/organisationID in merge struct 
		person/location/organisation in merge stuct
 
 --->


<cfcomponent extends="testbox.system.BaseSpec" output="false">

<cfscript>

variables.entities = {};
variables.entityUtilities = new "entityUtilities" ();


/**
* @hint  create a set of Organisation/Location/Person records and return personid
*/


function getUserPersonID () {

	if (not structKeyExists (variables,"userPersonID")) {
		variables.userPersonID = variables.entityUtilities.createPOLCollection().person.getpersonid();
		writedebug ("User PersonID: " & variables.userPersonID & "<BR>");
	}

	return variables.userPersonID;
}

function getPersonID () {
	if (not StructKeyExists (variables,"PersonID")) {
		var variables.PersonID = variables.entityUtilities.createPOLCollection().person.getpersonid();
		writedebug ("PersonID: " & variables.PersonID);
	}
	return variables.PersonID;
}


function getCurrentUserFields ( personLocationOrganisation ){

	if (not structKeyExists (variables.entities,"userPerson")) {
	
		variables.existingCurrentUserPersonID = request.relayCurrentUser.personid;
		variables.entities.UserPerson = variables.entityUtilities.updateEntityWithRandomData ('person', getUserPersonID() );
		var orgAndLoc =  new Query(sql = "select locationid, organisationid from person where personid = #getUserPersonID()#").execute().getResult();
		variables.entities.userLocation = variables.entityUtilities.updateEntityWithRandomData ('location', orgAndLoc.locationid);
		variables.entities.userOrganisation = variables.entityUtilities.updateEntityWithRandomData ('organisation', orgAndLoc.organisationid);
		application.com.relayCurrentUser.reinitialise (getUserPersonID());
		application.com.relayCurrentUser.setQueryParamDebug();
		writedebug ("User Populated: " & getUserPersonID() & "<BR>");
		
	}
	
	return variables.entities["user#personLocationOrganisation#"].fieldValues;
}



function getTestEntityDetails ( entityType ){

	if (not structKeyExists (variables.entities,entityType)) {
	
		if (listfindnocase ("person,location,organisation", entityType)) {
		
			variables.entities.person = 	variables.entityUtilities.updateEntityWithRandomData ('person', getPersonID());
			var orgAndLoc =  new Query(sql = "select locationid, organisationid from person where personid = #getPersonID()#").execute().getResult();
			variables.entities.location = variables.entityUtilities.updateEntityWithRandomData ('location', orgAndLoc.locationid);
			variables.entities.organisation = variables.entityUtilities.updateEntityWithRandomData ('organisation', orgAndLoc.organisationid);
			writedebug ("Person Populated: " & getPersonID() & "<BR>");
		} else {
		
			variables.entities[entityType] = variables.entityUtilities.updateEntityWithRandomData (entityType, orgAndLoc.organisationid);			
		}
	}
	
	return variables.entities[entityType];
}




function afterTests  ( ){
	
	/* reset relayCurrentUser to previous */
	if (structKeyExists (variables, "existingCurrentUserPersonID")) {
		application.com.relayCurrentUser.reinitialise (variables.existingCurrentUserPersonID);
	}	

}
	

function beforeTests  ( ){

}




function tst_AnyEntity (string entity, struct fieldValues, struct mergeStruct, boolean debug = false) {
	
	for (item in fieldValues) {
		row = fieldValues[item];	
		mergeObject = createObject ("component","com.mergeFunctions").initialiseMergeStruct (mergeStruct = mergeStruct);


		var stringToEvaluate = "[[#entity#.#item#]]";	
		if (debug) {writedebug(stringToEvaluate & ":" & row.value);}
		result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
		if (debug) {writedebug(result & "<BR>");}

		$assert.isEqual( listSort(row.value,"text") , listSort(result,"text"), "Testing field #stringToEvaluate#") ; // ListSort deals with multiple flags which may return in any order

		if (structKeyExists (row , "linkedEntityAlias")) { 
			/* test derived fields of form  field_name 
			var stringToEvaluate = "[[#entity#.#row.linkedEntityAlias#_name]]";	
			writedebug(stringToEvaluate & ":" & row.linkedEntityName );
			result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
			writedebug(result & "<BR>");
			$assert.isEqual( listSort(row.linkedEntityName,"text") , listSort(result,"text"), "Testing field #stringToEvaluate#") ; // ListSort deals with multiple flags which may return in any order
			*/

			/* and using full object notation */
			var stringToEvaluate = "[[#entity#.#row.linkedEntityAlias#.#row.linkedEntityNameExpression#]]";	
			if (debug) {writedebug(stringToEvaluate & ":" & row.linkedEntityName );}
			result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
			if (debug) {writedebug(result & "<BR>");}
			$assert.isEqual( listSort(row.linkedEntityName,"text") , listSort(result,"text"), "Testing field #stringToEvaluate#") ; // ListSort deals with multiple flags which may return in any order

		}
		
	}

}


/* This tests merge fields of the form [[user.person.aField]]  */
function test_user_person_userNotation () {
	tst_AnyEntity (entity = 'user.person', fieldValues = getCurrentUserFields ("person"), mergeStruct = {} );

}


/* This tests merge fields of the form [[person.aField]].  Should pick up the user structure automatically  */
function test_user_person_personNotation () {
	
	tst_AnyEntity (entity = 'person', fieldValues = getCurrentUserFields ("person"), mergeStruct = {} );

}


/* This tests merge fields of the form [[user.person.afield]]  */
function test_user_location_userNotation () {
	
	tst_AnyEntity (entity = 'user.location', fieldValues = getCurrentUserFields ("location"), mergeStruct = {} );

}


/* This tests merge fields of the form [[person.afield]].  Should pick up the user structure automatically  */
function test_user_location_locationNotation () {
	
	tst_AnyEntity (entity = 'location', fieldValues = getCurrentUserFields ("location"), mergeStruct = {} );

}


/* This tests merge fields of the form [[user.person.afield]]  */
function test_user_organisation_userNotation () {
	
	tst_AnyEntity (entity = 'user.organisation', fieldValues = getCurrentUserFields ("organisation"), mergeStruct = {} );

}


/* This tests merge fields of the form [[organisation.aField]].  Should pick up the user structure automatically  */
function test_user_organisation_organisationNotation () {
	
	tst_AnyEntity (entity = 'organisation', fieldValues = getCurrentUserFields ("organisation"), mergeStruct = {} );

}


/* This tests merge fields of the form [[person.aField]]  */
function test_object_person_byPerson () {
	
	tst_AnyEntity (	  entity = 'person'
					, fieldValues = getTestEntityDetails ("person").fieldValues
					, mergeStruct = {person = application.com.entities.load ('person', getTestEntityDetails ("person").entityID)} );

}


/* This tests merge fields of the form [[person.aField]] when only personid is passed in  */
function test_object_person_byPersonid () {
	
	tst_AnyEntity (entity = 'person', fieldValues = getTestEntityDetails ("person").fieldValues, mergeStruct = {personid = getTestEntityDetails ("person").entityid} );

}


/* This tests location merge fields of the form [[location.aField]] when only personid is passed in  */
function test_object_location_byPersonid () {

	tst_AnyEntity (entity = 'location', fieldValues = getTestEntityDetails ("location").fieldValues, mergeStruct = {personid = getTestEntityDetails ("person").entityid} );
}


/* This tests location merge fields of the form [[location.aField]] when only personid is passed in  */
function test_object_location_byPerson () {

	tst_AnyEntity (
					entity = 'location'
					, fieldValues = getTestEntityDetails ("location").fieldValues
					, mergeStruct = {person = application.com.entities.load ('person', getTestEntityDetails ("person").entityID)} 
				);
}

/* This tests merge fields of the form [[organisation.aField]] when only personid is passed in  */
function test_object_organisation_byPersonid () {

	tst_AnyEntity (	  entity = 'organisation'
					, fieldValues = getTestEntityDetails ("organisation").fieldValues
					, mergeStruct = {personid = getTestEntityDetails ("person").entityid} );
}


/* This tests merge fields of the form [[organisation.aField]] when only locationid is passed in  */
function test_object_organisation_byLocationid () {

	tst_AnyEntity (entity = 'organisation', fieldValues = getTestEntityDetails ("organisation").fieldValues, mergeStruct = {locationid = getTestEntityDetails ("location").entityid} );
}



function test_object_person_isPrimaryContact_Function () {
	
	var stringToEvaluate = "[[person.isPrimaryContact()]]";	
	person = application.com.entities.load ('person', getPersonID());		
	mergeObject = createObject ("component","com.mergeFunctions").initialiseMergeStruct (mergeStruct = {person = person});
	result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
	$assert.isEqual( result, false , "This might fail, cause I don't know what the answer should be") ; 

}


function test_object_user_isPrimaryContact_Function () {
	
	var stringToEvaluate = "[[user.person.isPrimaryContact()]]";	
	mergeObject = createObject ("component","com.mergeFunctions").initialiseMergeStruct (mergeStruct = {});
	result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
	$assert.isEqual( result, false , "This might fail, cause I don't know what the answer should be") ; 

}


/* allows writing some debug while developing, but won't muck up automated unit tests which need to return pure XML (or whatever)*/
function writeDebug (string) {
	if (not structKeyExists (url,"reporter")) {
		writeOutput (string);
	}
}
</cfscript>

</cfcomponent>



