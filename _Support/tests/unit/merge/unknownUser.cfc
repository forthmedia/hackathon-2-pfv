<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2017-02-01 Unit Test for RT-227  (Error if person merge field used on a logged out page)
 --->

<cfscript>

function  beforeTests  () {
		variables.currentUser = request.relayCurrentUser.personid;
		application.com.relayCurrentUser.reinitialise (404);
}

function  afterTests () {
		application.com.relayCurrentUser.reinitialise (variables.currentUser );
}


/*
* WAB 2017-02-01 RT-   Before fix applied this test would fail with "The value returned from the getEntity function is not of type struct" in merge.cfc
* 
* */
function  test_APersonField () {

		stringToEvaluate = "[[person.firstname]]";
		mergeObject = createObject ("component","com.mergeFunctions").initialiseMergeStruct ();
		result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
		$assert.isEqual (result,"");
}


</cfscript>

</cfcomponent>


