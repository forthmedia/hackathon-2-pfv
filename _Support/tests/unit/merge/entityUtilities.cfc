/*
* WAB 2017-01
* Various functions for creating entities containing test data
* Note that only reason it extends testbox is so that it can live within the unit test folders and then be skipped
*/
 
 
component output=false skip = true extends="testbox.system.BaseSpec" {

variables.queryParser = new "queryParserUtility" ();


/*
* Creates a new record for given entity, populated with a minimum of random data
*/ 
function createANewEntityrecord (entityType, fieldsStruct = {}) {

		var entity  = application.com.entities.new (entityType);

		// create random data for any required but not defaulted fields
		var sql = "select * from vfieldmetadata where tablename = '#entityType#' and isNullable = 0 and readonly = 0 and name not in ('lastupdated','created','createdby','lastupdatedby') and name <> '#entityType#ID' ";
		if (structCount(fieldsStruct)) {
			sql &= "and name not in (#listQualify(structKeyList(fieldsStruct),"'")# )";
		}
		fields =  new Query(sql = sql).execute().getResult();

 		for (local.row in fields)	{ 
			entity.set (row.name,createRandomDataForField(row).value);
		}
		for (field in fieldsStruct)	{ 
			entity.set (field,fieldsStruct[field]);
		}

		entityID = entity.save().entityID;
		
		return entity;
}



function createPOLCollection () {
	var result = {};
		var result.organisation = createANewEntityrecord ('organisation');
		var result.location = createANewEntityrecord ('location', {organisationID = result.organisation.getorganisationID(), active = 1} );
		result.location.setAccountTypeID (1);	result.location.save (); /* Not a required field but actually seems to be necessary for unique email validation to work */
		var result.person = createANewEntityrecord ('person', {locationID = result.location.getLocationID(), active = 1 });
		return result;
}

function deletePOLRecords (polRecords) {

		qry=  new Query(sql = "	delete from person where personid = :personid
								delete from location where locationid = :locationid
								delete from organisation where organisationid = :organisationid
				");
		qry.addParam (name="personid",cfsqltype="cf_sql_integer", value = polRecords.person.getPersonid());
		qry.addParam (name="locationid",cfsqltype="cf_sql_integer", value = polRecords.location.getlocationid());
		qry.addParam (name="organisationid",cfsqltype="cf_sql_integer", value = polRecords.organisation.getorganisationid());

		qry.execute();
	
}



/* 
* Return a selection of fields on given entity, hopefully one of each possibly type
* */
function getRandomSelectionOfFields  (string entity){ 

	var fieldsqry = new Query(sql = "select
										name 
									from (
											select 
												name 
												,rank () over (partition by dataType, source, foreignKey order by name) as _rank
											 from 
											 	vfieldmetadata
											where 
												tablename = '#entity#' 
												and name not in ('#entity#id','locationid','organisationid','created','lastupdated','lastupdatedby','createdby','lastupdatedbyperson','campaignid','progresstextid','organisationtypeid','active','emailstatus')
												and dataType <> 'date' /* due to problems with merge fields at moment! */
										) as x
									where 
										_rank = 1
									"
									, cachedWithin=createTimespan(0, 1, 0, 0)
									, name = "somefieldsqry"
									).execute().getResult();

	return valueList (fieldsqry.name);
	
}

/*
* Creates some random data for given fields and updates the underlying object
* Returns structure containing each field set and value set to
* Also returns a query that can be used to restore entity to original state
*/
function updateEntityWithRandomData  (	  string entity
								, numeric entityID
								, string fieldList = ""  
							){ 

	var result = {entity = entity, entityID = entityID};

	if (arguments.fieldList is "") {
		arguments.fieldList = getRandomSelectionOfFields (entity);
	}
	
	var fieldsqry = new Query(sql = "select 
										dataType, name, foreignKey, picklistvaluessql,foreignKeyValuesSQL,source 
									from 
									 	vfieldmetadata
									where 
										tablename = '#entity#' 
										and name in (#listQualify (fieldList,'''')# )
									"
										, name = "fieldsqry"
										).execute().getResult();


	/* create some random values for those different types */
	local.fields = "";
	result.fieldValues = {};

	for (local.row in fieldsqry)	{

		if (not listFindNoCase ("radio,checkbox", row.dataType)) {
			fields = listAppend (fields, row.name);
			result.fieldValues[row.name] = row;

			var data = createRandomDataForField (row);

			if (not structKeyExists (data, "value") ) {
				structDelete(result.fieldValues , row.name); 
			} else {
				structAppend (row, data);
			}
		}	
		
	}



	/* get the current record (for restoring to normal */
	result.currentRecord = new Query(sql = "select #fields# from v#entity# where #entity#ID= #entityID#").execute().getResult();			

	/* create a query to set the test values (and reset the original ones), saving the restore one in variables scope for reuse at the end */
	local.updateQuerySQL = "update v#entity#_update set ";
	local.comma = "";
		
	for (item in result.fieldValues) {
		local.updateQuerySQL &= "#comma# #item# = :#item# ";
		local.comma = ",";
	}

	local.updateQuerySQL &= " , lastupdatedbyPerson = 0";
	local.updateQuerySQL &= " where #entity#id = #entityID#";

	local.updateqry = new Query(sql = local.updateQuerySQL);
	result.restoreQry = new Query(sql = local.updateQuerySQL);

	for (item in result.fieldValues) {
		local.updateqry.addParam (name = item, value = result.fieldValues[item].value, cfsqltype= result.fieldValues[item].cfsqlType, scale=2);
		result.restoreQry.addParam (name = item, value = result.currentRecord[item][1], cfsqltype= result.fieldValues[item].cfsqlType, Null = (result.currentRecord[item][1] is ""), scale=2); 
	}

	updateqry.execute();			

	return result;
	
}



function createRandomDataForField (metadata) {

	var result = {};

	switch (lcase(metadata.dataType)) {
		case "bit" :
			result.value = randrange (0,1);
			result.cfsqltype = "cf_sql_bit";
			break;

		case "date" :
			result.value = now();
			result.cfsqltype = "cf_sql_timestamp";
			break;

		case "text" :
			result.cfsqltype = "cf_sql_varchar";
			if (metadata.name contains "email"){
				result.value = RandString(10) & "." & RandString(10) & "@" & RandString(10) & ".com";
			} else if (metadata.pickListValuesSQL is "") {	
				result.value = RandString(10);
			} else {
				result.value = getRandomItemFromPickList(metadata.pickListValuesSQL).dataValue;
			}	

			break;

		case "textMultiple" :
			if (metadata.pickListValuesSQL is "") {	
				/* can't have comma or ; in the text multiples */
				result.value = listappend(rereplace(RandString(10),"[,;]","","ALL"),rereplace(RandString(10),"[,;]","","ALL"));
				result.cfsqltype = "cf_sql_varchar";
			} else {
				result.value = listappend(getRandomItemFromPickList(metadata.pickListValuesSQL).dataValue,getRandomItemFromPickList(metadata.pickListValuesSQL).dataValue) ;
				result.cfsqltype = "cf_sql_varchar";
			}	
			break;

		case "financial" :
		case "decimal" :
			result.value = int(rand() * 10000)/100;
			result.cfsqltype = "cf_sql_numeric";
			break;

		case "numeric" :
			if (metadata.foreignKeyValuesSQL is not "" OR metadata.pickListValuesSQL is not "") {
				if (metadata.foreignKeyValuesSQL is not "") {
					randomItem = getRandomItemFromPickList(metadata.foreignKeyValuesSQL);
				} else {
					randomItem = getRandomItemFromPickList(metadata.pickListValuesSQL);
				}	
				result.value = randomItem.dataValue;	
				if (metadata.foreignKey is not "") {
					if (structKeyExists (application.EntityTypeID, listFirst(metadata.foreignKey,".") ) ) {
						result.linkedEntityName = randomItem.displayValue;
						result.linkedEntityAlias = reReplaceNocase( metadata.name & "_", "id_\Z" , "");
						result.linkedEntityNameExpression = application.com.relayEntity.getEntityType(listFirst(metadata.foreignKey,".")).nameExpression;
					}
				}
			} else {
				result.value = int(rand() * 10000);
			}

			result.cfsqltype = "cf_sql_integer";
			break;
		case "integerMultiple" :
			if (metadata.foreignKey is "" or metadata.FOREIGNKEYVALUESSQL is "") {
				result.value = int(rand() * 10000) & "," & int(rand() * 10000);
			}	else {
				randomItem1 = getRandomItemFromPickList(metadata.foreignKeyValuesSQL).dataValue;
				randomItem2 = getRandomItemFromPickList(metadata.foreignKeyValuesSQL).dataValue;
				result.value = listappend(randomItem1,randomItem2);	
			}
			result.cfsqltype = "cf_sql_varchar";
			break;
		default:
			writeDebug ("Not Dealt with #metadata.dataType# #metadata.name#");
	}
			
	return result;
	
}


	
function RandString (required numeric length) {
	
	local.result = Chr(RandRange(65, 90));

	for (local.i=1; i<=length -2; i++)	{
		result &= Chr(RandRange(97, 122));
	}
	
		result &= Chr(RandRange(32, 64));

	return result;
}


function getRandomItemFromPickList (required string picklistSQL) {
						picklistSQL = listfirst (picklistSQL, "|");  // to deal with some very old valid value stuff
						picklistSQL = application.com.relayTranslations.checkforandevaluatecfvariables (picklistSQL);
						queryparsed = variables.queryParser.parseQueryByRegExp (picklistSQL);
						selectListParsed = variables.queryParser.parseASelectList (queryparsed.selectList);
						var sqlWithoutOrderBy = queryparsed.withoutOrderBy;
						var countItems = new Query(sql = "select count(1) as theCount from (#sqlWithoutOrderBy#) as X").execute().getResult();
						var randomRow = randRange (1,countItems.thecount);
						var linkedEntity = new Query(sql = "select 
																dataValue ,
																displayValue 
															from 
																(
																	#replacenocase (sqlWithoutOrderBy,'SELECT','SELECT row_number() over (order by #selectListParsed.aliasToSQLMapping[selectListParsed.columnArray[1]].sql#) as therow,')#
																)	as x
															where
																theRow = #randomRow#	
												").execute().getResult();
	
						/* deal with non existence of entity (particular problem with leads where the OOTB does not have any data)- so create a random record */
						if (linkedEntity.recordCount is 0) {
							var entityType = listFirst (queryparsed.fromtables," ");
							createANewEntityrecord (entityType);
							linkedEntity = 	getRandomItemFromPickList (argumentCollection = arguments);
						}
	

	return linkedEntity;
}


/* allows writing some debug while developing, but won't muck up automated unit tests which need to return pure XML (or whatever)*/
function writeDebug (string) {
	if (not structKeyExists (url,"reporter")) {
		writeOutput (string);
	}
}


}