<cfcomponent extends="testbox.system.BaseSpec" >

<cfscript>

variables.entityUtilities = new "entityUtilities" ();


/**
*
*
*/ 

function beforeTests () {
	var createTestEmail = new Query(sql = "delete from emailDef where emailTextID like 'UnitTest%'").execute().getResult();						
	
	
}

function afterTests () {
	var deleteTestEmail = new Query(sql = "delete from emailDef where emailTextID like 'UnitTest%'").execute().getResult();						

	if (structKeyExists (variables,"POLCollection")) {
		variables.entityUtilities.deletePOLRecords(variables.POLCollection );
	}
}


/*
* Get a Person/Location/Organisation collection
* */
function getPOLCollection () {

	if (not structKeyExists (variables,"POLCollection")) {
		variables.POLCollection = variables.entityUtilities.createPOLCollection();
	}
	return variables.POLCollection;

}



function AddEmail (emailTextID, body) {
	
	var sql = "
		delete  from emaildef where emailTextID = '#emailTextID#'
		declare 
			  @emailID int
			, @entityTypeID  int = (select entityTypeID from schemaTable where entityName = 'emaildef')

		insert into emaildef (emailtextID)
		values ('#emailTextID#')

		set @emailID  = scope_identity()
		
		exec addNewPhraseAndTranslation @phraseTextID = 'body', @phraseText = :body, @entityID = @emailID, @entityTypeID = @entityTypeID 
	";
	
	var addEmail = new Query(sql = sql);
	addEMail.addParam (name = 'body', cfsqltype="cf_sql_varchar", value = body);
	addEMail.execute().getResult();					
		

}

function dropEmail (emailtextID) {
	var x = new Query(sql = "delete from emailDef where emailTextID = '#emailTextID#'").execute().getResult();					
}

function getTwoRadioProfiles (entityTypeID) {
	var  result = new Query(sql = "select top 2 f.flagGroupTextID, f.flagTextID, f.flag as flagName, 'phr_' + f_.namephraseTextID as phrase
									from 
									vflagdef f
										inner join flag f_ on f.flagid = f_.flagid
									where f.flagGroupID =
										(select top 1 flagGroupID from vflagDef where entityTypeID = #entityTypeID# and dataType = 'radio' and flagGroupTextID <> '' and flagTextID <> ''
										group by flagGroupID
										having count (1) > 1)
							").execute().getResult();			
	return result;		
}


function getPersonID () {
	var  result = new Query(sql = "select top 1 personid from person where active = 1").execute().getResult().personid;					
	return result;
}



function getACustomEntity () {
	var result = new Query(sql = "select 
									*
									, (select top 1 1 from ventityname e where e.entityTypeID = s.entityTypeID and deleted = 0) as hasEntities
									, (select top 1 1 from vflagdef f where f.entityTypeID = s.entityTypeID and datatype = 'radio') as  hasRadioFlag
								from 
									schemaTable s
								where customentity = 1
								order by hasRadioFlag desc, hasEntities desc 
							").execute().getResult();						
							
	if (result.hasEntities is 0) {
		variables.entityUtilities.createANewEntityrecord (result.entityName);
	}
	
	if (result.hasRadioFlag is 0) {
		writeOutput ("need to create a radio group for customEntity #result.entityName#");
	}

	return result;	
}



function testRelayIfInEmail () {

	var personid = getPersonID();
	var personObject = application.com.entities.load ('person', personid);
	var radios = getTwoRadioProfiles(0);
	var getCurrentProfileValue = personObject.get (radios.flagGroupTextID);

	personObject.set (radios.flagGroupTextID, radios.flagTextID);
	personObject.save();

	var emailTextID = 'UnitTest';
	var body = "<relay_if showProfileValue (profileID = '#radios.flagGroupTextID#') is '#radios.flagName#'>TRUE<relay_else>FALSE</relay_if>";
	addEMail (emailTextID = 'UnitTest', body = body);

	var result = application.com.email.calculateEmailContent (emailTextID = emailTextID, personid = personid);

	$assert.isEqual ("TRUE", result.body , "relayIF should evaluate to true");

	// change radio
	personObject.set (radios.flagGroupTextID, radios.flagTextID[2]);
	personObject.save();
	sleep(2000);  // because there is some caching on shoProfileValue

	var result = application.com.email.calculateEmailContent (emailTextID = emailTextID, personid = personid);

	$assert.isEqual ("FALSE", result.body , "relayIF should evaluate to false");

	
	dropEmail (emailTextID);



}


/**
* @hint: Tests that email test mode will pick up the last entity in a table and evaluate the relay_if correctly
*/ 
function test_RelayIf_CustomEntity_RadioField_TestMode () {

	var customEntity = getACustomEntity ();
	var entityID = new Query(sql = "select top 1 #customEntity.uniqueKey# as entityID from #customEntity.entityName# order by #customEntity.uniqueKey# desc").execute().getResult().entityID;					

	var Object = application.com.entities.load (customEntity.entityName, entityID);

	var radios = getTwoRadioProfiles(customEntity.entityTypeID);

	var getCurrentProfileValue = Object.get (radios.flagGroupTextID);

	object.set (radios.flagGroupTextID, radios.flagTextID);
	object.save();

	var emailTextID = 'UnitTest';
	var body = "<relay_if #customEntity.entityName#.#radios.flagGroupTextID# is '#radios.phrase#'>TRUE<relay_else>FALSE</relay_if>";
	addEMail (emailTextID = 'UnitTest', body = body);

	var result = application.com.email.calculateEmailContent (emailTextID = emailTextID, personid = request.relaycurrentuser.personid, test = 1);

	$assert.isEqual ("TRUE", result.body , "Relay IF should return true");

	// change value of radio
	object.set (radios.flagGroupTextID, radios.flagTextID[2]);
	object.save();

	sleep(2000);  // because there is some caching on showProfileValue

	var result = application.com.email.calculateEmailContent (emailTextID = emailTextID, personid = request.relaycurrentuser.personid, test = 1);

	$assert.isEqual ("FALSE", result.body , "Relay IF should return false");
	
	dropEmail (emailTextID);



}


/*
*	WAB 2017-02-03
*  General test of email merge
* Wanted to check that recent changes to merge.cfc to deal with entityObjects (specifically POL) had not caused problems in the email system
* */
function test_POLMergeFields () {
	
	var emailTextID = 'UnitTest';
	var body = "[[person.firstname]][[location.sitename]][[organisation.organisationName]][[magicNumber]]";
	addEMail (emailTextID = 'UnitTest', body = body);

	var POLCollection = getPOLCollection ();

	var result = application.com.email.calculateEmailContent (emailTextID = emailTextID, personid = POLCollection.person.getpersonid());

	$assert.isEqual (
				POLCollection.person.getFirstName() 
				& POLCollection.location.getSiteName()
				& POLCollection.organisation.getOrganisationName()
				& POLCollection.person.getMagicNumber() 				
				, result.body , "");		
		
	dropEmail (emailTextID);

}

</cfscript>

</cfcomponent>


