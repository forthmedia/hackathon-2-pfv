<!--- 

 --->


<cfcomponent extends="testbox.system.BaseSpec" output="false">

<cfscript>


function test_NonExistentMergeField () {

	var field = "opportunity.opportunityid";
	var stringToEvaluate = "[[" & field & "]]";	
	mergeObject = createObject ("component","com.mergeFunctions").initialiseMergeStruct (mergeStruct = {});
	result = mergeObject.checkforandevaluateCfvariables(phraseText = stringToEvaluate );
	$assert.isEqual( "opportunity.opportunityid", result, "Should return the original merge field minus the [[ ]]; ie: #field#") ; 
	
	/* check that merge error has been logged */
	
	var checkForErrorLog = new Query(sql = "select * from relayerror where visitID = #request.relaycurrentuser.visitid# and type = 'merge error' and datastruct like '%#field#%' and errordatetime > dateAdd(s,-1,getdate())").execute().getResult();		

	$assert.isNotEqual( 0, checkForErrorLog.recordCount, "Should be a merge error logged in RelayErrors" ) ; 	
	

}

</cfscript>

</cfcomponent>



