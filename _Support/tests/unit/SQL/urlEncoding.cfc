<cfcomponent extends="testbox.system.BaseSpec" >
<cfprocessingdirective pageEncoding="utf8">

<!--- 
2016-10-17 WAB PROD2016-2544 Unit test for new SQL function dbo.fn_char2percentEncoded() and altered dbo.fn_urlEncode
				Test function results against #URLEncodedFormat()#
 --->

<cfscript>

function Test_fn_char2percentEncoded () {

	/* 
	Test some standard character which need encoding and some more obscure ones which exerise fn_char2percentEncoded() at the upper range
 
 	Here is some test data
	Code for some of the more obscure characters came from
	http://www.ltg.ed.ac.uk/~richard/utf-8.cgi?input=57344&mode=decimal
	The simpler ones came from W3 schools

	I am not actually using the utf8Expected column, but rather comparing the result to urlEncodedFormat 
 	*/
				
	var encodeCharactersQuery = new query (sql = "
				select character, utf8Expected, dbo.fn_char2PercentEncoded (character) as utfencoded
				from 
				( values 
				
					 (N'€','%e2%82%ac')
					,(N'Ä','%c3%84')	
					,(N'ÿ','%c3%bf')	
					,(N'Ā','%c4%80')		
					,(N'ǿ','%c7%bf')	
					,(N'Ȁ','%c8%80')	
					,(N'ȁ','%c8%81')	
					,(N'!','%21')
					,(N'""','%22')
					,(N'##','%23')
					,(N'$','%24')
					,(N'%','%25')
					,(N'&','%26')
					,(N'''','%27')
					,(N'(','%28')
					,(N')','%29')
					,(N'*','%2A')
					,(N'+','%2B')
					,(N',','%2C')
					,(N'-','%2D')
					,(N'.','%2E')
					,(N'/','%2F')
					,(N':','%3A')
					,(N';','%3B')
					,(N'<','%3C')
					,(N'=','%3D')
					,(N'>','%3E')
					,(N'?','%3F')
					,(N'@','%40')
					,(N'[','%5B')
					,(N'\','%5C')
					,(N']','%5D')
					,(N'^','%5E')
					,(N'_','%5F')
					,(N'`','%60')
					,(N'{','%7B')
					,(N'|','%7C')
					,(N'}','%7D')
					,(N'~','%7E')
					,(N'','%7F')
					,(N'€','%E2%82%AC') -- incorrect in original data (there are various different Euro Signs)
					,(N'‚','%E2%80%9A')
					,(N'ƒ','%C6%92')
					,(N'„','%E2%80%9E')
					,(N'…','%E2%80%A6')
					,(N'†','%E2%80%A0')
					,(N'‡','%E2%80%A1')
					,(N'ˆ','%CB%86')
					,(N'‰','%E2%80%B0')
					,(N'Š','%C5%A0')
					,(N'‹','%E2%80%B9')
					,(N'Œ','%C5%92')
					,(N'Ž','%C5%BD')
					,(N'‘','%E2%80%98')
					,(N'’','%E2%80%99')
					,(N'“','%E2%80%9C')
					,(N'”','%E2%80%9D')
					,(N'•','%E2%80%A2')
					,(N'–','%E2%80%93')
					,(N'—','%E2%80%94')
					,(N'˜','%CB%9C')
					,(N'™','%E2%84%A2') -- incorrect in original data
					,(N'š','%C5%A1')
					,(N'›','%E2%80%BA') -- incorrect in original data
					,(N'œ','%C5%93')
					,(N'ž','%C5%BE')
					,(N'Ÿ','%C5%B8')
					,(N'¡','%C2%A1')
					,(N'¢','%C2%A2')
					,(N'£','%C2%A3')
					,(N'¤','%C2%A4')
					,(N'¥','%C2%A5')
					,(N'¦','%C2%A6')
					,(N'§','%C2%A7')
					,(N'¨','%C2%A8')
					,(N'©','%C2%A9')
					,(N'ª','%C2%AA')
					,(N'«','%C2%AB')
					,(N'¬','%C2%AC')
					,(N'­','%C2%AD')
					,(N'®','%C2%AE')
					,(N'¯','%C2%AF')
					,(N'°','%C2%B0')
					,(N'±','%C2%B1')
					,(N'²','%C2%B2')
					,(N'³','%C2%B3')
					,(N'´','%C2%B4')
					,(N'µ','%C2%B5')
					,(N'¶','%C2%B6')
					,(N'·','%C2%B7')
					,(N'¸','%C2%B8')
					,(N'¹','%C2%B9')
					,(N'º','%C2%BA')
					,(N'»','%C2%BB')
					,(N'¼','%C2%BC')
					,(N'½','%C2%BD')
					,(N'¾','%C2%BE')
					,(N'¿','%C2%BF')
					,(N'À','%C3%80')
					,(N'Á','%C3%81')
					,(N'Â','%C3%82')
					,(N'Ã','%C3%83')
					,(N'Ä','%C3%84')
					,(N'Å','%C3%85')
					,(N'Æ','%C3%86')
					,(N'Ç','%C3%87')
					,(N'È','%C3%88')
					,(N'É','%C3%89')
					,(N'Ê','%C3%8A')
					,(N'Ë','%C3%8B')
					,(N'Ì','%C3%8C')
					,(N'Í','%C3%8D')
					,(N'Î','%C3%8E')
					,(N'Ï','%C3%8F')
					,(N'Ð','%C3%90')
					,(N'Ñ','%C3%91')
					,(N'Ò','%C3%92')
					,(N'Ó','%C3%93')
					,(N'Ô','%C3%94')
					,(N'Õ','%C3%95')
					,(N'Ö','%C3%96')
					,(N'×','%C3%97')
					,(N'Ø','%C3%98')
					,(N'Ù','%C3%99')
					,(N'Ú','%C3%9A')
					,(N'Û','%C3%9B')
					,(N'Ü','%C3%9C')
					,(N'Ý','%C3%9D')
					,(N'Þ','%C3%9E')
					,(N'ß','%C3%9F')
					,(N'à','%C3%A0')
					,(N'á','%C3%A1')
					,(N'â','%C3%A2')
					,(N'ã','%C3%A3')
					,(N'ä','%C3%A4')
					,(N'å','%C3%A5')
					,(N'æ','%C3%A6')
					,(N'ç','%C3%A7')
					,(N'è','%C3%A8')
					,(N'é','%C3%A9')
					,(N'ê','%C3%AA')
					,(N'ë','%C3%AB')
					,(N'ì','%C3%AC')
					,(N'í','%C3%AD')
					,(N'î','%C3%AE')
					,(N'ï','%C3%AF')
					,(N'ð','%C3%B0')
					,(N'ñ','%C3%B1')
					,(N'ò','%C3%B2')
					,(N'ó','%C3%B3')
					,(N'ô','%C3%B4')
					,(N'õ','%C3%B5')
					,(N'ö','%C3%B6')
					,(N'÷','%C3%B7')
					,(N'ø','%C3%B8')
					,(N'ù','%C3%B9')
					,(N'ú','%C3%BA')
					,(N'û','%C3%BB')
					,(N'ü','%C3%BC')
					,(N'ý','%C3%BD')
					,(N'þ','%C3%BE')
					,(N'ÿ','%C3%BF')
					,(N'כ','%D7%9B')
					,(N'ס','%D7%A1')
					,(N'י','%D7%99')
					,(N'ז','%D7%96')
					,(N'ר','%D7%A8')
					,(N'ט','%D7%98')
					,(N'ו','%D7%95')
					,(N'ת','%D7%AA')
					,(N'ל','%D7%9C')
					,(N'כ','%D7%9B')
					,(N'נ','%D7%A0')
					
				) x (character,utf8Expected)
				
				-- This line was used when this was a pure SQL test - where dbo.fn_char2PercentEncoded (character) <> utf8Expected

		").execute().getResult();

		for (row in encodeCharactersQuery) {
			$assert.isEqual( row.utfencoded , urlencodedformat(row.character), htmlEditFormat("#row.character# should encode to #urlencodedformat(row.character)#") ) ; 
		}
			
}



function fn_urlEncode (string) {
	
		var encodeString = new query (sql = "
											select dbo.[fn_urlencode] (N'#string#')	as encodedString
										").execute().getResult();

		$assert.isEqual( encodeString.encodedString , urlencodedformat(string), htmlEditFormat("#string# should encode to #urlencodedformat(string)#") ) ; 

}


function test_Hebrew (string) { 
		
		fn_urlEncode ('is תיכנות a"£רכזת ') ;
	
}
		


</cfscript>
</cfcomponent>


<!--- 
IF @@rowcount <> 0
	RAISERROR (N'There is a problem with fn_char2PercentEncoded, the previous query should return no results',16,16)
	
	
select dbo.[fn_urlencode] (N'is תיכנות a"£רכזת ')	 --->