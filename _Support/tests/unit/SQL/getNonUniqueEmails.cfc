<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-07-08  Written when I altered this function very slightly

Tests operation of 
	SQL function getNonUniqueEmails
	

--->

<cfscript>


function runFunction(personid,emailAddress, accounttypeid) {
	
	var result = new query (sql = "
		declare @personEmails as personEmailTableType
				
		insert into @personemails (personId,email,accountTypeID)
		values (#personId#,'#email#',#accountTypeID#)
		
		select * from dbo.getNonUniqueEmails(@personEmails)
	
	").execute().getPrefix().recordCount;

	return result;
}




function getPersonRecord (excludedEmail = false) {

	var sql = " 
				select top 1 
					personid, email, accountTypeID, bfd.flagid
				from 
					person p 
						inner join 
					location l on p.locationid = l.locationid
						left outer join 
					(booleanflagdata bfd inner join flag f ON f.flagID = bfd.flagID and f.flagTextID='DeletePerson') on bfd.entityID = p.personId
						inner join organisation o on o.organisationid =l.organisationid
					inner join organisationType ot on ot.organisationTypeID = o.organisationTypeID and ot.typeTextID != 'EndCustomer'
					
				where 
					isnull(email,'') <> ''
	
	";

	sql &= " and dbo.listFind (isNull('#application.com.settings.getSetting('plo.excludedEmailsFromUniqueValidation')#',''),email) "   ;
	sql &= (excludedEmail)?" <> 0": " = 0" ;
	
	
	var getPerson = new query (sql = sql).execute().getResult();

	return getPerson;
}
	

function test_Duplicate () { 
	
	var person = 	getPersonRecord ();
	var rows = runFunction (personid = 0, email = person.email, accountTypeID = person.accountTypeID);
	
	// A duplicate
	$assert.isEqual( 1 , rows, person.email & "should be a duplicate") ; 

	
}

	
function test_Duplicate_DifferentOrgType () { 
	
	var person = 	getPersonRecord ();
	var rows = runFunction (personid = 0, email = person.email, accountTypeID = person.accountTypeID  + 1);
	
	// Not a duplicate - different Type
	$assert.isEqual( 0 , rows) ; 

	
}


function test_Duplicate_IgnoredEmail () { 

	var person = 	getPersonRecord ();
	currentSetting = application.com.settings.getSetting('plo.excludedEmailsFromUniqueValidation');
	newSetting = listAppend (currentSetting, person.email);
	application.com.settings.InsertUpdateDeleteSetting (variableName ='plo.excludedEmailsFromUniqueValidation', variablevalue = newSetting );
	
	var rows = runFunction (personid = 0, email = person.email, accountTypeID = person.accountTypeID );

	// Not a duplicate - Ignored Item
	$assert.isEqual( 0 , rows) ; 

	application.com.settings.InsertUpdateDeleteSetting (variableName ='plo.excludedEmailsFromUniqueValidation', variablevalue = currentSetting );
	
}


function test_NotDuplicate_RandomEmail () { 
	
		var rows = runFunction (personid = 0, email = "abcdef@1224.4555", accountTypeID = 1 );
	
		// Not a duplicate
		$assert.isEqual( 0 , rows) ; 

	
}

</cfscript>




</cfcomponent>	
