/**
 * samlMetadataProcessingService
 * 
 * @author Richard.Tingle
 * @date 15/01/16
 **/
component accessors=true output=false persistent=false  skip="skipIfLiveSite"{
		function skipIfLiveSite () {
			return !application.testsite;
		}
		
		function test_validMetadata() {
			var validMetadata='<?xml version="1.0" encoding="UTF-8"?>'&
				'<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" ID="tSOTJVz7E.5UbtDY28-QovYvZ58" cacheDuration="PT1440M" entityID="dev19-gaext">'&
				   '<md:SPSSODescriptor protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">'&
				      '<md:AssertionConsumerService index="0" Location="http://dev19-gaext/saml.cfm" Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" isDefault="true" />'&
				      '<md:AttributeConsumingService index="0">'&
				         '<md:ServiceName xml:lang="en">AttributeContract</md:ServiceName>'&
				         '<md:RequestedAttribute Name="email" />'&
				      '</md:AttributeConsumingService>'&
				   '</md:SPSSODescriptor>'&
				   '<md:ContactPerson contactType="administrative" />'&
				'</md:EntityDescriptor>';
				
			var processSPMetadata=new WebServices.samlMetadataProcessingWS();
			var returnJSON =processSPMetadata.processSPMetadata(validMetadata);
	
			returnedStruct=DeserializeJSON(returnJSON);
			
			$assert.isTrue(returnedStruct.isOk);
			$assert.isEqual("http://dev19-gaext/saml.cfm", returnedStruct.assertionConsumerURL);
			$assert.isEqual("dev19-gaext", returnedStruct.entityID);
			$assert.isEqual("dev19-gaext", returnedStruct.entityName);
			
		}

	
		
		function test_validXMLInvalidMetadata() {
			var validMetadata='<?xml version="1.0" encoding="UTF-8"?>' &
					'<yes>' &
						'<xmlIsGreat>' &
							'yes' &
						'</xmlIsGreat>' &
					'</yes>';
				
			var processSPMetadata=new WebServices.samlMetadataProcessingWS();
			var returnJSON =processSPMetadata.processSPMetadata(validMetadata);
	
			returnedStruct=DeserializeJSON(returnJSON);
			
			$assert.isFalse(returnedStruct.isOk);
			$assert.isTrue(len(returnedStruct.message)>0); // we leave it up to the implementation to decide what the message is, but there must be one

			
		}
		
		
		function test_invalidXML() {
			var validMetadata='test!!! CFGBHFDHGZSDRTBut No!!';
				
			var processSPMetadata=new WebServices.samlMetadataProcessingWS();
			var returnJSON =processSPMetadata.processSPMetadata(validMetadata);
	
			returnedStruct=DeserializeJSON(returnJSON);
			
			$assert.isFalse(returnedStruct.isOk);
			$assert.isTrue(len(returnedStruct.message)>0); // we leave it up to the implementation to decide what the message is, but there must be one

			
		}
		
	
	}