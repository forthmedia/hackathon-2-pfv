<!--- 
WAB 2016-05-02  For BF-673 

For debugging problems in unit tests
	 translate.test_cftranslate_withTranslationLinks
	 translate.test_onRequestEnd_withTranslationLinks
--->

<cfset translationUnitTest = createObject("component","unittests/com/relaytranslations/translate")>
<cfset text = translationUnitTest.getTextForLinkTst()>


<cfoutput>
	This page can be used to see wbat is actually going on if there is a failure in unit tests:<br />
	&nbsp;&nbsp;&nbsp;	 translate.test_cftranslate_withTranslationLinks  <br />
	&nbsp;&nbsp;&nbsp;	 translate.test_onRequestEnd_withTranslationLinks  <br /><br />

	We are testing whether translation T Links are working.  <br />
	In particular the code which makes sure that T Links do not appear within other Anchors, or within the attributes of HTML tags<br /><br />

	The text to be translated is below.<br />
	When it is translated, there should be a T link everywhere where there is :LINK and none where there is a :NOLINK<br /><br />
	
	<br />
	<!-- StartDoNotTranslate -->
	#text#
	<!-- EndDoNotTranslate -->
	<br /><br /><br />
</cfoutput>



<cfoutput>
	This it the result of translating with cf_translate showTranslationLink=true<br />
	<br />
</cfoutput>
	<cf_translate showTranslationLink="true">
		<cfoutput>#text#</cfoutput>
	</cf_translate>



<cfset application.com.relayCurrentUser.updateContentSettings(ShowTranslationLinks = true)>


<cfoutput>
	<br /><br /><br />
	This is the result of translating with onRequestEnd with ShowTranslationLinks = true<br /><br />
	#text#
	<br /><br />
</cfoutput>

<cf_abort>



<cf_translate showTranslationLink="true">
	This is an extra test.<br />
	There is no HTML in this block, there should be a link here<br />
	phr_thankyou:LINK
</cf_translate>



