<cfcomponent extends="testbox.system.BaseSpec" skip="skipIfLiveSite">

<!--- 
WAB 2016-01-14 

Tests operation of 
	com.relayTranslations.addNewPhraseAndTranslationV2()
	com.relayTranslations.addPhrasesFromLoadFile() 	(which calls the above)

	In particular testing the behaviour of the OverwriteUnEdited update mode as added in JIRA PROD2015-513

--->

<cfscript>

variables.phraseTextID = 'aTestPhrase';


function skipIfLiveSite () {
	return !application.testsite;
}

function deletePhrase (phraseTextID) {

	var qry = new query (sql = "delete from phrases where phraseid in (select phraseid from phraselist where phraseTextID = :phraseTextID and entityTypeid = 0)");
	qry.addParam(name="phraseTextID", value = phraseTextID, cfsqltype="cf_sql_varchar");
	var result = qry.execute();

	var qry =  new query (sql = "delete from phraseList where phraseTextID = :phraseTextID and entityTypeid = 0");
	qry.addParam(name="phraseTextID", value = phraseTextID, cfsqltype="cf_sql_varchar");
	var result = qry.execute();

}

function beforeTests () {
	deletePhrase (variables.phraseTextID);
}

function afterTests_ () {
	deletePhrase (variables.phraseTextID);
}


function test_Add_Update_AndOverwriteUnEdited() {
	deletePhrase (variables.phraseTextID);
	
	var phraseText = '123';
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText);
	$assert.isEqual( insertresult.details.phraseAdded , 1) ; 
	$assert.isEqual( insertresult.details.translationAddedUpdated , 1) ; 
	var getPhrase = application.com.relayTranslations.translatePhrase (phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 


	/* update with overwrite unedited */
	var phraseText = '1234';
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText, updateMode = "OverwriteUnEdited");
	$assert.isEqual( insertresult.details.phraseAdded , 0) ; 
	$assert.isEqual( insertresult.details.translationAddedUpdated , 1) ; 
	var getPhrase = application.com.relayTranslations.translatePhrase (phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 


	/* update with overwrite  */
	var phraseText = '12345';
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText, updateMode = "Overwrite");
	$assert.isEqual( insertresult.details.phraseAdded , 0) ; 
	$assert.isEqual( insertresult.details.translationAddedUpdated , 1) ; 
	var getPhrase = application.com.relayTranslations.translatePhrase (phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 


	/* update with overwrite unedited again, should not work*/
	var phraseText = '123456';
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText, updateMode = "OverwriteUnEdited");
	$assert.isEqual( insertresult.details.phraseAdded , 0) ; 
	$assert.isEqual( insertresult.details.translationAddedUpdated , 0) ; 
	$assert.isEqual( insertresult.details.notUpdatedBecauseEdited , 1) ; 
	var getPhrase = application.com.relayTranslations.translatePhrase (phraseTextID);
	$assert.isNotEqual( getPhrase, phraseText) ; 

	deletePhrase (variables.phraseTextID);
}


function test_Add_Update_Language() {

	var phraseText1 = 'Language 1';
	var language1 = 1;
	var phraseText2 = 'Language 2';
	var language2 = 2;

	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText1, languageid = language1);
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText2, languageid = language2);

	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID, languageID = 1);
	$assert.isEqual( getPhrase, phraseText1) ; 

	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID, languageID = 2);
	$assert.isEqual( getPhrase, phraseText2) ; 

	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID, languageID = 3);
	$assert.isEqual( getPhrase, phraseText1) ; 

	deletePhrase (variables.phraseTextID);
}

function test_addPhrasesFromLoadFile() { 
	
	deletePhrase (variables.phraseTextID);
	
	/* upload a phrase */
	phraseText = "1234";
	var fileContent = "#phraseTextID#|#phraseText#|1|0";
	application.com.relayTranslations.addPhrasesFromLoadFile (phraseFileContent = fileContent, updatemode = "overwriteUnedited") ;
	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 
	
	/* now upload a new translation, should overwrite */
	phraseText = "12345";
	var fileContent = "#phraseTextID#|#phraseText#|1|0";
	application.com.relayTranslations.addPhrasesFromLoadFile (phraseFileContent = fileContent, updatemode = "overwriteUnedited") ;
	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 

	/* now do an edit of the phrase */
	phraseText = "123456";
	var insertresult = application.com.relayTranslations.addNewPhraseAndTranslationV2 (phraseTextID = phraseTextID, phraseText = phraseText);

	/* now upload a new translation, should fail to overwrite */
	phraseText= "1234567";
	var fileContent = "#phraseTextID#|#phraseText#|1|0";
	application.com.relayTranslations.addPhrasesFromLoadFile (phraseFileContent = fileContent, updatemode = "overwriteUnedited") ;
	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID);
	$assert.isNotEqual( getPhrase, phraseText) ; 
	
	/* try again in overwrite mode */
	application.com.relayTranslations.addPhrasesFromLoadFile (phraseFileContent = fileContent, updatemode = "overwrite") ;
	var getPhrase = application.com.relayTranslations.translatePhrase (phrase = phraseTextID);
	$assert.isEqual( getPhrase, phraseText) ; 

	deletePhrase (variables.phraseTextID);
}




</cfscript>

</cfcomponent>