<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-04-29

Tests general operation of the translation system including:
	WAB 2016-05-02  BF-673 Check that translation T links are created when asked for (and not created in places where they will not work)
					Check that phrases within Javascript are encoded correctly
--->

<cffunction name="getTextForLinkTst">
	<cfset var result = "">
	
	<cfsavecontent variable="result">
		<TABLE class="withBorder">
			<TR>
				<TD>There should be link after this phrase</TD>
				<TD>phr_thankyou:LINK</TD>
			</TR>
		
			<TR>
				<td>And within an anchor</td>
				<td><A>phr_thankyou:LINK </A></td>
			</TR>
		
			<tr>
				<td>There should not be a link in the submit button name</td>		
				<td><input type="submit" value="phr_thankyou:NOLINK" ></td>
			</tr>
		
			<tr>
				<td>There should not be a link within javascript</td>		
				<td>
					<script>
						document.write ('This was written using document.write: phr_thankyou:NOLINK')
					</script>
				</td>
			</tr>


		</table>	
		
	</cfsavecontent>

	<cfreturn result>
	
</cffunction>



<cffunction name="test_cftranslate_withTranslationLinks" hint="Checks that Translation T Links are output when requested, but not in inappropriate places.  Using CF_TRANSLATE">

	<cfsavecontent variable = "translatedText">
		<cf_translate showTranslationLink="true">
			<cfoutput>#getTextForLinkTst()#</cfoutput>
		</cf_translate>
	</cfsavecontent>

	<cfset validateTranslationLinks (translatedText)>

</cffunction>

<!--- If this test fails you can use /unittests/com/relaytranslations/translationlinktest.cfm to help with debugging --->
<cffunction name="test_cftranslate_withTranslationLinks_2" hint="Checks that Translation T Links are output when requested, but not in inappropriate places.  Using CF_TRANSLATE">

	<cfsavecontent variable = "translatedText">
		<cf_translate showTranslationLink="true">
		There is no HTML in this block, there should be a link here<br />
		phr_thankyou:LINK
		</cf_translate>
	</cfsavecontent>

	<cfset validateTranslationLinks (translatedText)>

</cffunction>


<!--- If this test fails you can use /unittests/com/relaytranslations/translationlinktest.cfm to help with debugging --->
<cffunction name="test_onRequestEnd_withTranslationLinks" hint="Checks that Translation T Links are output when requested, but not in inappropriate places.  Using onRequestEnd">

	<cfset application.com.relayCurrentUser.updateContentSettings (showTranslationLinks = true)>
	<cfset var translatedText = application.com.request.postProcessContentString (getTextForLinkTst())>
	<cfset application.com.relayCurrentUser.updateContentSettings (showTranslationLinks = false)>

	<cfset validateTranslationLinks (translatedText)>

</cffunction>


<cffunction name="validateTranslationLinks" >
	<cfargument name = "translatedText">

	<!--- 	Take the translated text and look at everywhere where there is the text :LINK or :NOLINK 
	
			If we find </a>:NOLINK then there is a problem
			If we find :LINK not preceded by </a> then there is a problem
	--->

	<cfset var incorrectNoLinkregExp = "</a>:NOLINK">
	<cfset $assert.notMatch( translatedText, incorrectNoLinkregExp, "Found a translation link where there should not be one.  Refer to /unittests/com/relaytranslations/translationlinktest.cfm for help debugging." ) > 
	
	<cfset var incorrectLinkregExp = "([^>])\w*:LINK">
	<cfset $assert.notMatch( translatedText, incorrectLinkregExp, "No translation link where there should be one.  Refer to /unittests/com/relaytranslations/translationlinktest.cfm for help debugging.") > 
	
	
</cffunction>


<cffunction name="test_JavascriptEncoding" hint="Checks that quotes are encoded if within a script tag">
	<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID = 'APhraseContainingQuote', phraseText = "This is a single quote ' .")>

	<cfsavecontent variable = "translatedText">
		<cf_translate >
			<script>
				x = ('phr_APhraseContainingQuote')	
			</script>
		</cf_translate>
	</cfsavecontent>

	<cfset var encodedQuoteRegExp = "\\'">
	<cfset $assert.Match( translatedText, encodedQuoteRegExp, "The quote has not been encoded for javascript" ) > 


</cffunction>


<cffunction name="afterTests">
	<cfset application.com.request.clearHeaderBuffer()>
</cffunction>




</cfcomponent>