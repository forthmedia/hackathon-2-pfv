<cfcomponent extends="testbox.system.BaseSpec" skip="skipIfLiveSite">

<cfscript>


function skipIfLiveSite () {
	return !application.testsite;
}

function beforeTests () {  


}


/* WAB 2016-10-07 Created test when I altered relayPLO.formatAddress() to accept a location argument */
function test_ComparePassingLocationIDandLocationQuery (		) 
{
	
	/* choose a random location */
	var LocationID = new query (sql = "select top 1 locationid from location where sitename <> '' and address1 <> '' and address2 <> '' and address5 <> '' ").execute().getResult().locationid;		

	/* get address format with original technique - passing in a locationid */
	var resultWithLocationID = application.com.relayPLO.formatAddress(locationID = locationid);
	
	/* get address format with new technique - passing in a location query */
	var Location = new query (sql = "select *  from location l inner join country c on l.countryid = c.countryid where locationid = #locationid# ").execute().getResult();			
	var resultWithLocationQuery = application.com.relayPLO.formatAddress(location = location);	;

	$assert.isEqual( resultWithLocationID , resultWithLocationQuery ) ; 
	
}


function test_ComparePassingLocationIDandLocationStructure (		) 
{
	
	/* choose a random location */
	var LocationID = new query (sql = "select top 1 locationid from location where sitename <> '' and address1 <> '' and address2 <> '' and address5 <> '' ").execute().getResult().locationid;		

	/* get address format with original technique - passing in a locationid */
	var resultWithLocationID = application.com.relayPLO.formatAddress(locationID = locationid);
	
	/* get address format with new technique - passing in a location structure */
	var Location = new query (sql = "select *  from location l inner join country c on l.countryid = c.countryid where locationid = #locationid# ").execute().getResult();			
	var locationStructure = application.com.structurefunctions.queryRowToStruct (location);
	var resultWithLocationQuery = application.com.relayPLO.formatAddress(location = locationStructure);	

	$assert.isEqual( resultWithLocationID , resultWithLocationQuery ) ; 
	
}


</cfscript>

</cfcomponent>