<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-11-11 PROD2016-2731

I changed this function for performance reasons, so this unit test confirms that the old and new versions give same answer

--->


	<!---  	This is the old verison of the function but still pointing to the same list of attributes
			It has been changed slightly because the structure containing the lists was changed, but otherwise the logic is the same
	--->
	<cfset this.tagAttributeLists = application.com.relayForms.tagAttributeLists>
	<cffunction name="convertTagAttributeStructToValidAttributeListOld" returntype="string" output="false">
		<cfargument name="attributeStruct" type="struct" required="true">
		<cfargument name="tagName" type="string" default="input">

		<cfset var tagNameAttributes = this.tagAttributeLists.byTagName[arguments.tagName]>
		<!--- NJH 2016/02/26 - check for type specific attributes --->
		<cfif structKeyExists(arguments.attributeStruct,"type") and arguments.attributeStruct.type neq "" and structKeyExists(this.tagAttributeLists.byType,arguments.attributeStruct.type)>
			<cfset tagNameAttributes = this.tagAttributeLists.byType[arguments.attributeStruct.type]>
		</cfif>
		<cfset var attrRegExp = "\A(#this.tagAttributeLists.global.standard#|#this.tagAttributeLists.global.rw#|#tagNameAttributes#)\Z">
		<cfset var attr = "">

		<cfloop list="disabled,readonly,multiple,required,checked" index="attr">
			<cfif structKeyExists(arguments.attributeStruct,attr)>
				<cfif isBoolean(arguments.attributeStruct[attr]) and not arguments.attributeStruct[attr]>
					<cfset structDelete(arguments.attributeStruct,attr)>
				<cfelseif attr neq "required">
					<cfset arguments.attributeStruct[attr] = attr>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn application.com.structureFunctions.convertStructureToNameValuePairsForHTMLOutput(struct=arguments.attributeStruct,includeKeysRegExp=attrRegExp)>
	</cffunction>

<cfscript>


function test_1() {

	/* Note that important thing to test was that the items which use a regExp  (on.* and data-.*) still work and that an unwanted attribute is removed */
	var attrs = {	onthis="aaaa", 
					"data-ddd"= "AAA", 
					id="person_jobfunction_checkbox", 
					requiredfunction="-1", 
					type="input", 
					arandomattribute = "wab",
					datemask="yyyy-mm-dd",
					requiredMessage= "ssss"
					 	};
	
	var oldResult =  convertTagAttributeStructToValidAttributeListOld (attrs);
	var newResult = application.com.relayForms.convertTagAttributeStructToValidAttributeList (attrs); 

	var oldResultAsStruct = application.com.structureFunctions.convertNameValuePairStringToStructure (oldResult);
	var newResultAsStruct = application.com.structureFunctions.convertNameValuePairStringToStructure (newResult);

	$assert.isEqual(oldResultAsStruct,newResultAsStruct);
}




</cfscript>

</cfcomponent>