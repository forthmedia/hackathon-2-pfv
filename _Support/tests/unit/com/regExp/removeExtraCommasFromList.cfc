<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-04-20   
Written for Case 449069 which was caused by this function not dealing with three or more successive commas


--->

<cfscript>


function test_Trailing() {
	var result = application.com.globalFunctions.removeEmptyListElements ("3918,");

	$assert.isEqual(result,"3918");
}


function test_Leading() {
	var result = application.com.globalFunctions.removeEmptyListElements (",3918");

	$assert.isEqual(result,"3918");
}

function test_DoubleLeading() {
	var result = application.com.globalFunctions.removeEmptyListElements (",,3918");

	$assert.isEqual(result,"3918");
}

function test_DoubleTrailing() {
	var result = application.com.globalFunctions.removeEmptyListElements ("3918,,");

	$assert.isEqual(result,"3918");
}

function test_TripleTrailing() {
	var result = application.com.globalFunctions.removeEmptyListElements ("3918,,,");

	$assert.isEqual(result,"3918");
}

function test_LeadingAndTrailing() {
	var result = application.com.globalFunctions.removeEmptyListElements (",,1,2,3,,");

	$assert.isEqual(result,"1,2,3");
}

function test_InMiddle() {
	var result = application.com.globalFunctions.removeEmptyListElements ("1,,2,,,3");

	$assert.isEqual(result,"1,2,3");
}


function test_WithSpace() {
	var result = application.com.globalFunctions.removeEmptyListElements ("1,,2,, ,,3");

	$assert.isEqual(result,"1,2, ,3");
}

function test_WithTrailingAndLeadingSpace() {
	var result = application.com.globalFunctions.removeEmptyListElements (" 1,,2,, ,,3 ");

	$assert.isEqual(result," 1,2, ,3 ");
}


function test_WithEverything() {
	var result = application.com.globalFunctions.removeEmptyListElements (" ,,,,1,,,,,,2,, ,,3,,, ,, ");

	$assert.isEqual(result," ,1,2, ,3, , ");
}


</cfscript>

</cfcomponent>