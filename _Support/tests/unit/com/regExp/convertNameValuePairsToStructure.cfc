<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-11-28   PROD2016-2768 
From old 'unit' test created when convertNameValuePairStringToStructure was first written
Converted to testbox when convertNameValuePairStringToStructure was modified to deal with very long strings

--->

<cfscript>

	function genericTst(string inputString, struct expectedResult, string delimiter) {
	
		var result = application.com.regExp.convertNameValuePairStringToStructure (inputString = inputString, delimiter = delimiter);
		$assert.isEqual(expectedResult, result);
		
	}

	function genericTst_commaDelimiter(string inputString, struct expectedResult) {
		genericTst	(argumentCollection = arguments, delimiter = ",");
	}
	
	function genericTst_spaceDelimiter(string inputString, struct expectedResult) {
		genericTst	(argumentCollection = arguments, delimiter = " ");
	}

	function genericTst_ampDelimiter(string inputString, struct expectedResult) {
		genericTst	(argumentCollection = arguments, delimiter = "&");
	}


	function testVeryLongString() {
		var string = repeatString ('abcdefghijklmnopqrstuvwxyz',1000);
		genericTst_spaceDelimiter ('a="#string#"', {a=string});
	}

	/* note that the regular expression which parses these strings still collapses with very long strings with escaped quotes 
		Roadmap system fell over with more than ~ 5000 characters
	*/	
	function testVeryLongStringWithEscapedDoubleQuotes () {
		var string = repeatString ('abcdefghijklmnopqrst""uvwxyz',200);
		var expectedResult = replace(string,'""','"','ALL');
		genericTst_spaceDelimiter ('a="#string#"', {a=expectedResult});
	}

	function testItemWithNoEqualsSign() {
		genericTst_commaDelimiter ("a,b=1", {a="",b=1});
	}


	function testItemWithNoValue() {
		genericTst_spaceDelimiter ("a=1234 b= c=2678", {a=1234,b="",c=2678});
	}


	// this is a case of a bad string - what happens?
	function testBadlyFormattedOddQualifier () {
		genericTst_commaDelimiter ("FILEID='1',XID='", {FILEID=1,XID="'"});
	}


	// No gap between qualifier and next item
	function testMoGapBetweenQualifierAndNextItem () {
		genericTst_spaceDelimiter ('a="1234"onclick="56 78'' '' &imgrefurl='' + this"', {a="1234", onclick="56 78' ' &imgrefurl=' + this"});
	}


	function testSpaceAfterEqualsSign () {
		genericTst_spaceDelimiter ("a=1234 b= 1 c=2678", {a=1234,b=1,c=2678});
	}


	function testEscapedSingleQuote () {
		var string = "1'' 2";
		var result = replace (string,"''","'","ALL");
		genericTst_spaceDelimiter ("a='#string#' b=2 c=3 ", {a=result,b=2, c=3});
	}


	function testEscapedDoubleQuote () {
		genericTst_spaceDelimiter ('a="10 00" b="2000""3000" c=2000', {a="10 00", b='2000"3000' ,c=2000});
	}


	function testEscapedSingleQuote2 () {
		genericTst_spaceDelimiter ('a="100"b="200''''3" c=200', {a="100",b="200''3", c=200});
	}


  	function testSingleQuotesInsideDoubleQuotes () {		
  		genericTst_spaceDelimiter (	'a="1"b=" 2''s''s"c="3"',{a="1",b=" 2's's",c="3"});
  	}


  	function test10 () {
  		genericTst_spaceDelimiter (	'a="1"b=" 2''''s''''s"c="3 1"',{a="1",b=" 2''s''s",c="3 1"});
  	}	
  	

  	function test11 () {
  		genericTst_spaceDelimiter (	'a="1"b=" 2""s""s"c="3"',{a="1",b=' 2"s"s',c="3"});
  	}


  	function test12 () {
  		genericTst_commaDelimiter ('a="1",b="",c=2',{a=1,b="",c=2});
  	}


  	function test13 () {
  		genericTst_commaDelimiter ('a="1",b=,c=2',{a=1,b="",c=2});
  	}


  	function test14 () {
  		genericTst_commaDelimiter ('a="1234",onclick="12 34'' '' &imgrefurl='' + this"',{a="1234",onclick="12 34' ' &imgrefurl=' + this"});
  	}


  	function test15 () {
  		genericTst_commaDelimiter ('a="12,34",onclick="12 34'' '' &imgrefurl='' + this"',{a="12,34",onclick="12 34' ' &imgrefurl=' + this"});
  	}


  	function test16 () {
  		genericTst_commaDelimiter ("a='1'' 2',b=2,c=3",{a='1'' 2',b=2, c=3});
	}


  	function test17 () {
  		genericTst_commaDelimiter (	'a="10 00", b="2000""3,000", c=2000',{a="10 00", b='2000"3,000' ,c=2000});
	}


  	function test18 () {
  		genericTst_commaDelimiter (	'a="100",b="200''''3", c=200',{a="100",b="200''3", c=200});
  	}


  	function test19 () {
  		genericTst_commaDelimiter (	'a="1",b=" 2''s''s",c="3"',{a="1",b=" 2's's",c="3"});
  	}


  	function test20 () {
  		genericTst_commaDelimiter (	'a="1",b=" 2''''s''''s",c="3 1"',{a="1",b=" 2''s''s",c="3 1"});
	}



  	function test21 () {
  		genericTst_commaDelimiter (	'a="1",b=" 2""s""s",c="3"',{a="1",b=' 2"s"s',c="3"});
  	}


  	function test22 () {
  		genericTst_ampDelimiter ('a=1&b=&c=2',{a=1,b="",c=2});
  	}


  	function test23 () {
  		genericTst_ampDelimiter ('a=1&b=&c=2',{a=1,b="",c=2});
  	}


  	function test24 () {
  		genericTst_ampDelimiter ('a="1234"&onclick="12 34'' '' &imgrefurl='' + this"',{a="1234",onclick="12 34' ' &imgrefurl=' + this"});
	}


  	function test25 () {
  		genericTst_ampDelimiter ('a="12,34"&onclick="12 34'' '' &imgrefurl='' + this"',{a="12,34",onclick="12 34' ' &imgrefurl=' + this"});
  	}


  	function test26 () {
  		genericTst_ampDelimiter ("a='1'' 2'&b=2&c=3",{a='1'' 2',b=2, c=3});
  	}


  	function test27 () {
  		genericTst_ampDelimiter ("a='1'' 2'&b = 2&c= 3",{a='1'' 2',b=2, c=3});
  	}


  	function test28 () {
  		genericTst_ampDelimiter ("a='1'' 2'&b = 2 &c= 3 ",{a='1'' 2',b=2, c=3});
  	}



  	function test29 () {
  		genericTst_ampDelimiter (	'a="10 00"& b="2000""3&000"& c=2000',{a="10 00", b='2000"3&000' ,c=2000});
  	}


  	function test30 () {
  		genericTst_ampDelimiter (	'a="100"&b="200''3"& c=200',{a="100",b="200'3", c=200});
  	}



  	function test31 () {
  		genericTst_ampDelimiter (	"a='100'&b='200''3'& c='200'",{a="100",b="200'3", c=200});
  	}


  	function test32 () {		
  		genericTst_ampDelimiter (	'a="1"&b=" 2''s''s"&c="3"',{a="1",b=" 2's's",c="3"});
  	}


  	function test33 () {
  		genericTst_ampDelimiter (	'a="1"&b=" 2''''s''''s"&c="3 1"',{a="1",b=" 2''s''s",c="3 1"});
  	}

	
  	function test34 () {
  		genericTst_ampDelimiter (	'a="1"&b=" 2""s""s"&c="3"',{a="1",b=' 2"s"s',c="3"});
  	}


  	function test35 () {
  		genericTst_commaDelimiter (	"a='1',b='xx=3&yy=4'",{a='1',b='xx=3&yy=4'});
  	}


  	function test36 () {
  		genericTst_ampDelimiter (	"x=1&y=2&z=",{x=1,y=2,z=''});
  	}


  	function test37 () {
  		genericTst_ampDelimiter (	"x=1&x=2",{x="1,2"});
  	}

  	function test38 () {
  		genericTst_spaceDelimiter (	"x=1 y z=1",{x="1",y="",z="1"});
  	}





</cfscript>

</cfcomponent>