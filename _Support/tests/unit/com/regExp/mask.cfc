<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-02-08


--->

<cfscript>

masksAndRegExp = {
	usa = {
		 Mask = "(999) 999 9999"
		,RegExp = "\A\(\d\d\d\) \d\d\d \d\d\d\d\Z"
	}
	
	, Everything = {
		Mask = "(AAA).XXX-9999-?"
		,RegExp = "\A\([A-Za-z][A-Za-z][A-Za-z]\)\.\w\w\w-\d\d\d\d-.\Z"
		}	

};


function test_USA1() {
	var result = application.com.regExp.doesStringMatchMask (string = "(012) 345 2222", mask = masksAndRegExp.usa.mask);

	$assert.isEqual(result,1);
}


function test_USA2() {
	var result = application.com.regExp.doesStringMatchMask (string = "(012) 345 22222", mask = masksAndRegExp.usa.mask);

	$assert.isEqual(result,0);
}


function test_USA3() {
	var result = application.com.regExp.doesStringMatchMask (string = "012 345 2222", mask = masksAndRegExp.usa.mask);

	$assert.isEqual(result,0);
}


function test_maskToRegExp() {
	var result = application.com.regExp.convertMaskToRegularExpression (mask = masksAndRegExp.usa.mask);

	$assert.isEqual(result,masksAndRegExp.usa.regExp);
}


function test_EverythingmaskToRegExp() {
	var result = application.com.regExp.convertMaskToRegularExpression (mask = masksAndRegExp.everything.mask);

	$assert.isEqual(result,masksAndRegExp.everything.regExp);
}


function test_Everything1() {
	var result = application.com.regExp.doesStringMatchMask (string = "(ABC).A12-1345-!", mask = masksAndRegExp.everything.mask);

	$assert.isEqual(result,1);
}

function test_Everything2() {
	/* this should fail because of a 9 where must be letter */
	var result = application.com.regExp.doesStringMatchMask (string = "(AB9).A12-1345-!", mask = masksAndRegExp.everything.mask);

	$assert.isEqual(result,0);
}


</cfscript>
</cfcomponent>