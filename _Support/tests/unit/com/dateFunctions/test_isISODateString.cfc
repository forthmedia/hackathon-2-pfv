component extends="testbox.system.BaseSpec" {


	public void function beforeTests() output="false" {
		this.dateFunctions=new com.dateFunctions();
	}

	public void function testPositiveExample() output="false" {
		result = this.dateFunctions.isISODateString('2015-03-02');
		$assert.isTrue(result,"Positive Case: 2015-03-02 is correct");
	}		
		
	public void function testNegativeExample(){
		result = this.dateFunctions.isISODateString('Hello World 2015-03-02');
		$assert.isFalse(result,"Negative Case: Hello World 2015-03-02 is wrong");
		
		result = this.dateFunctions.isISODateString('2015-03-02 Hello World');
		$assert.isFalse(result,"Negative Case: 2015-03-02Hello World  is wrong");
		
		result = this.dateFunctions.isISODateString('2015-03');
		$assert.isFalse(result,"Negative Case: 2015-03");
		
		result = this.dateFunctions.isISODateString('15-03-02');
		$assert.isFalse(result,"Negative Case: 15-03-02");
	}
}