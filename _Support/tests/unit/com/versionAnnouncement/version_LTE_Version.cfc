/**
 * version_LTE_Version
 * 
 * @author Richard.Tingle
 * @date 29/02/16
 **/
component  extends="testbox.system.BaseSpec" skip="skipIfLiveSite"{
	public void function testMajorMinorVersionsSame() {
        
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,1,1), createVersion(2015,1,2)));
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,1,1), createVersion(2015,1,1)));
        $assert.isEqual( false ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,1,2), createVersion(2015,1,1)));
    }
    
    public void function testMajorVersionsSame() {
        
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,1,200), createVersion(2015,2,100)));
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,1,50), createVersion(2015,1,51)));
        $assert.isEqual( false ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2015,2,2), createVersion(2015,1,1)));
    }
    
    public void function testMajorVersionsdifferent() {
        
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2014,1,200), createVersion(2015,2,100)));
        $assert.isEqual( true ,application.com.versionAnnouncement.version_LTE_Version( createVersion(2014,2,100), createVersion(2015,1,200)));
    }
    
    private struct function createVersion(numeric major, minor, maintainance){
    	return {yearVersion=major,majorVersion=minor,minorVersion=maintainance};
    }
    
    function skipIfLiveSite () {
       return !application.testsite;
    }
    
    
}