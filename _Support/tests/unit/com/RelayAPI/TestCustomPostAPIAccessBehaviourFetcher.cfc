component extends="mxunit.framework.TestCase"{


	public void function beforeTests(){

		this.personBehaviour=new com.RelayAPI.OnAPIAccess_Blank();
		this.locationBehaviour=new com.RelayAPI.OnAPIAccess_Blank();
		this.missingBehaviour=new com.RelayAPI.OnAPIAccess_Blank();

		this.standardStruct={};
		StructInsert(this.standardStruct,"OnAPIAccess_Person", this.personBehaviour);
		StructInsert(this.standardStruct,"OnAPIAccess_Location", this.locationBehaviour);
		this.customPostAPIAccessBehaviourFetcher = new com.RelayAPI.CustomPostAPIAccessBehaviourFetcher(this.standardStruct,this.missingBehaviour);

	}

	public void function testGetCustomBehaviourObject_exists(){

		returnedPerson=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject("person");
		assertSame(returnedPerson,this.personBehaviour);

		returnedLocation=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject("location");
		assertSame(returnedLocation,this.locationBehaviour);

		assertNotSame(returnedPerson, returnedLocation);
	}

	public void function testGetCustomBehaviourObject_NotExists(){

		returnedValue=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject("made_up_entity");

		assertSame(returnedValue, this.missingBehaviour);
	}


}