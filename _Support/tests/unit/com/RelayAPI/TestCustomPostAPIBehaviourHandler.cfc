component extends="mxunit.framework.TestCase"{

	private struct function getMockedCustomPostAPIAccessBehaviourFetcher_forPerson(){
		var mockStruct={};
		
		mockStruct.onAPIAccessCustomBehaviour_mock=getMockBox().createEmptyMock("com.RelayAPI.OnAPIAccess_Blank");

		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onSuccessfulInsert", callLogging="true");
		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onFailedInsert", callLogging="true");
		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onSuccessfulUpdate", callLogging="true");
		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onFailedUpdate", callLogging="true");
		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onSuccessfulDelete", callLogging="true");
		mockStruct.onAPIAccessCustomBehaviour_mock.$(method="onFailedDelete", callLogging="true");

		mockStruct.customPostAPIAccessBehaviourFetcher_mock=getMockBox().createEmptyMock('com.relayapi.CustomPostAPIAccessBehaviourFetcher');
		mockStruct.customPostAPIAccessBehaviourFetcher_mock.$(method="getCustomBehaviourObject",returns=mockStruct.onAPIAccessCustomBehaviour_mock, callLogging="true");
	
		return mockStruct;
	}



	public void function testhandleCustomBehaviour_insert_success(){
		
		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();
		
		customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_insert("person", 4,true,{value="test"});

		//customPostAPIAccessBehaviourFetcher_mock.verify().getCustomBehaviourObject("Person");

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onSuccessfulInsert[1][1]); 
		$assert.isEqual({value="test"},log_onAPIAccessCustomBehaviour.onSuccessfulInsert[1][2]); 

	}

	public void function testhandleCustomBehaviour_insert_fail(){

		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();

		var customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_insert("person", 4,false,{value="test"});

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onFailedInsert[1][1]); 
		$assert.isEqual({value="test"},log_onAPIAccessCustomBehaviour.onFailedInsert[1][2]); 
		

	}

	public void function testhandleCustomBehaviour_update_success(){
		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();

		customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_Update("person", 4,true,{value="test"});

		//customPostAPIAccessBehaviourFetcher_mock.verify().getCustomBehaviourObject("Person");

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onsuccessfulUpdate[1][1]); 
		$assert.isEqual({value="test"},log_onAPIAccessCustomBehaviour.onsuccessfulUpdate[1][2]); 

	}

	public void function testhandleCustomBehaviour_update_fail(){

		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();

		customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_Update("person", 4,false,{value="test"});

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onfailedUpdate[1][1]); 
		$assert.isEqual({value="test"},log_onAPIAccessCustomBehaviour.onfailedUpdate[1][2]); 
	}


	public void function testhandleCustomBehaviour_delete_success(){
		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();


		customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_Delete("person", 4,true);

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onSuccessfulDelete[1][1]); 
	}

	public void function testhandleCustomBehaviour_delete_fail(){

		var mockStruct=getMockedCustomPostAPIAccessBehaviourFetcher_forPerson();

		customPostAPIBehaviourHandler=new com.relayapi.CustomPostAPIBehaviourHandler(mockStruct.customPostAPIAccessBehaviourFetcher_mock);

		customPostAPIBehaviourHandler.handleCustomBehaviour_Delete("person", 4,false,{value="test"});

		//verify the person behaviour was requested
		var log_customPostAPIAccessBehaviourFetcher=mockStruct.customPostAPIAccessBehaviourFetcher_mock.$callLog();

		$assert.isEqual("person",log_customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject[1][1]); //(reading backwards) first argument of first call to getCustomBehaviourObject is person 

		var log_onAPIAccessCustomBehaviour=mockStruct.onAPIAccessCustomBehaviour_mock.$callLog();

		$assert.isEqual(4,log_onAPIAccessCustomBehaviour.onFailedDelete[1][1]); 
	}

}
