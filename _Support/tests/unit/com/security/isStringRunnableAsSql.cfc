<cfcomponent extends="testbox.system.BaseSpec" >

<!---
	NJH 2017/02/21 FOR RT-271

--->

<cfscript>
function beforeTests () {
	/* create a table to run scripts against */
	new Query(sql = "create table testsecurity(id int)").execute();
}

function afterTests () {
	/* create a table to run scripts against */
	new Query(sql = "drop table testsecurity").execute();
}


function test_invalidString_notRunnable() {

	var string = "Select at least one check box for: I confirm I have authorization from my company to sign up to the Terms and Conditions on its behalf";
	var result = application.com.security.isStringRunnableAsSql (string = string);
	$assert.isEqual (false,result);
}


function test_alterString_runnable() {

	var string = "alter table testsecurity add isSecure bit";
	var result = application.com.security.isStringRunnableAsSql (string = string);
	$assert.isEqual (true,result);
}


function test_validStringWithSetNoExecOff_runnable() {

	var string = "select 1 from testsecurity;set noexec off; select 1 from testsecurity";
	var result = application.com.security.isStringRunnableAsSql (string = string);
	$assert.isEqual (true,result);
}


function test_invalidStringWithSetNoExecOff_notRunnable() {

	var string = "Select at least one check box for: set noexec off; I confirm I have authorization from my company to sign up to the Terms and Conditions on its behalf";
	var result = application.com.security.isStringRunnableAsSql (string = string);
	$assert.isEqual (false,result);
}


function test_dropString_runnable() {

	var string = "drop table testsecurity;";
	var result = application.com.security.isStringRunnableAsSql (string = string);
	$assert.isEqual (true,result);
}

</cfscript>


</cfcomponent>