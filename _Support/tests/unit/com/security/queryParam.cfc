<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-05-17 for BF-710

--->

<cfscript>


function test_typeNumeric_NotNumeric() {
	
	var input = "a";
	$assert.throws( function(){ application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_numeric"); } );

}


function test_typeNumeric_Numeric() {
	
	var input = "2";
	var result = application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_Numeric"); 
	$assert.isEqual (input,result);

}


function test_typeNumeric_NegativeDecimals() {
	
	var input = "-2.12";
	var result = application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_Numeric"); 
	$assert.isEqual (input,result);

}


function test_typeNumeric_NegativeDecimalsList() {
	
	var input = "-2.12,-2.13,-2.14";
	var result = application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_Numeric"); 
	$assert.isEqual (input,result);

}


function test_typeNumeric_leadingTrailingSpaces() {
	
	var input = " -2.12 ";
	var result = application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_Numeric"); 
	$assert.isEqual (input,result);

}


function test_typeNumeric_listWithSpaces() {
	
	var input = " -2.12 ,  -2.13 ,  -2.14 ";
	var result = application.com.security.queryParam (value  = input, cfsqltype = "cf_sql_Numeric"); 
	$assert.isEqual (input,result);

}



</cfscript>


</cfcomponent>