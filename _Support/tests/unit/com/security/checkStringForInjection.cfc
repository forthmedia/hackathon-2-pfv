<cfcomponent extends="testbox.system.BaseSpec" >

<!---
WAB 2016-05-17 for BF-710

--->

<cfscript>
function beforeTests () {
	/* create a table to run scripts against */
	new Query(sql = "IF object_ID('testsecurity') is null BEGIN create table testsecurity(id int) END").execute();
}

function afterTests () {
	/* create a table to run scripts against */
	new Query(sql = "drop table testsecurity").execute();
}


function doTst (any input, boolean isOK, string comment = "" ) { 

	var dangerousSQLResult = application.com.security.checkStringForInjection (string=input, type="dangerousSQL").isOK;		
	var sqlSelectResult = application.com.security.checkStringForInjection (string=input, type="sqlSelect").isOK;		
	
	var overAllResult = dangerousSQLResult AND sqlSelectResult;

	$assert.isEqual (isOK, overAllResult, comment);		
}

function test_dropTableStatement_dangerousSqlInjection() {

	doTst  (
				"a string here;drop table testsecurity"
				, false
	) ;
}

function test_alterTableStatement_dangerousSqlInjection() {

	doTst  (
				"a string here;alter table testsecurity add isSecure bit"
				, false
	) ;

}

function test_dropDBStatement_dangerousSqlInjection() {

	doTst  (
				"a string here;drop database mydb"
				, false
	) ;
}

function test_deleteStatementWithSpaces_dangerousSqlInjection() {

	doTst  (
				"delete  from   testsecurity"
				, false
	) ;

}


function test_deleteStatementWithComments_dangerousSqlInjection() {

	doTst  (
				"delete/**/from/**/testsecurity"
				, false
	) ;

}

function test_deleteStatementWithComment_() {

	doTst  (
				"\*WAB*\delete from testsecurity"
				, false
	) ;

}

function test_validString_notSqlSelectInjection() {

	doTst  (
				"Select at least one check box for: I confirm I have authorization from my company to sign up to the Terms and Conditions on its behalf"
				, true
	) ;

}


function test_selectStatement_sqlSelectInjection() {

	doTst  (
				"some string here;Select id from testSecurity; --put in some comment here"
				, false
	) ;
}

function test_selectStatementInBrackets_sqlSelectInjection() {

	doTst  (
				"(Select id from testSecurity)"
				, false
	) ;
	
}

function test_selectStatementInBracketsWithComment_sqlSelectInjection() {

	doTst  (
				"( /* wab */Select id from testSecurity)"
				, false
	) ;

}

function test_selectStatementInBracketsWithSpaces_sqlSelectInjection() {

	doTst  (
				" (   Select id from testSecurity)"
				, false
	) ;
}

/* 2017-02-23 WAB Before fix was applied these calls (date, numeric, boolean) would error */
function test_JavaCastNumeric_sqlSelectInjection() {

	doTst  (
				javacast ("int", 0)
				, true
	) ;

}

function test_JavaCastBoolean_sqlSelectInjection() {

	doTst  (
				javacast ("boolean", 0)
				, true
	) ;
}

function test_date_sqlSelectInjection() {

	doTst  (
				now()
				, true
	) ;

}

function test_commentsWhereSpacesShouldBe() {

	doTst  (
				"Select/*  */1 from testSecurity"
				, false
	) ;
}


function test_commentsWhereSpacesShouldBeDrop() {

	doTst  (
				"Drop/*  */table/* */testSecurity"
				, false
	) ;	
}


function test_StringWithTwoSQLStatementsOneHiddenInQuotes() {

	/*  before improvements, the first drop would get picked up and we would determine that the SQL was not runnable, missing the second drop
	 	in fact the whole statement is runnable if put where an integer might be expected
	*/ 
	doTst  (
				" 10; /*WAB1*/ select 'drop table testsecurity'; drop table testsecurity"
				, false
	) ;	
}



</cfscript>


</cfcomponent>