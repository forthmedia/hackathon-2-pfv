<cfcomponent extends="testbox.system.BaseSpec" >

<!--- 
WAB 2016-01-14 

Tests operation of 
	com.errorHander.handleError() 
	and a few associated functions

--->

<cfscript>



function test_extractTemplateInfoFromErrorStructure_blankStructure() {
	var result = application.com.errorhandler.extractTemplateInfoFromErrorStructure ({});
	$assert.key (result,"filename","A non existent tag context should still return the filename key");
	$assert.isEqual(result.filename,"");
}

function test_extractTemplateInfoFromErrorStructure_NoTagContext() {
	var result = application.com.errorhandler.extractTemplateInfoFromErrorStructure (variables.errors.noTagContext);
	$assert.key (result,"filename","A non existent tag context should still return the filename key");
	$assert.isEqual(result.filename,"");
}

function test_extractTemplateInfoFromErrorStructure_EmptyTagContext() {
	var result = application.com.errorhandler.extractTemplateInfoFromErrorStructure (variables.errors.emptyTagContext);
	$assert.key (result,"filename","A non existent tag context should still return the filename key");
	$assert.isEqual(result.filename,"");
}


function test_extractTemplateInfoFromErrorStructure_cfqueryParam() {
	var result = application.com.errorhandler.extractTemplateInfoFromErrorStructure (variables.errors.cf_querparam);
	$assert.key (result,"relativefilename");
	$assert.isEqual ("\Relay\errorHandler\createanerror.cfm",result.relativeFileName,"Function should have detected that the error actually occured in createanerror.cfm, not where it was thrown in cf_queryparam");
}

function test_extractTemplateInfoFromErrorStructure_undefinedVariable() {
	var result = application.com.errorhandler.extractTemplateInfoFromErrorStructure (variables.errors.undefinedVariable);
	$assert.key (result,"relativefilename");
	$assert.isEqual ("\Relay\errorHandler\createanerror.cfm",result.relativeFileName,"");
}

function test_extractTemplateInfoFromErrorStructure_RecordError() {
	transaction {
		savecontent variable="dontWantErrorMessageOnScreenThough" {
			var result = application.com.errorhandler.handleError (variables.errors.cf_querparam);
			// the above function sets a 500 status code which can be confusing, so set back to 200
			getpagecontext().getresponse().setstatus(200);
		}
		$assert.isNotEqual (0,result,"An error should have been saved to the database");

		transaction action="rollback";
	}
}
	
</cfscript>



<cfset variables.errors = {}>
<!--- 	This is a json encoding of a real error but has had some nulls removed because these behave differently from the original when deserialised 
		and the tagContext had to be serialised separately and pasted in manually 
	--->

<!--- because relayPath changes on each machine, need to use a variable when creating the error structures - plus encoding for json --->
<cfset relaypath = replace (application.paths.relay,"\","\\","ALL")>


<cfset variables.errors.cf_querparam = '
			{	
			"Suppressed":[],"Type":"Application","code":""
			,"tagContext":[{"RAW_TRACE":"\tat cfcreateanerror2ecfm751275842.runPage(#relaypath#\\errorHandler\\createanerror.cfm:59)","ID":"??","TEMPLATE":"#relaypath#\\errorHandler\\createanerror.cfm","LINE":59,"TYPE":"CFML","COLUMN":0},{"RAW_TRACE":"\tat cfapplication2ecfc751688446$funcONREQUEST.runFunction(#relaypath#\\application.cfc:16)","ID":"CFINCLUDE","TEMPLATE":"#relaypath#\\application.cfc","LINE":16,"TYPE":"CFML","COLUMN":0}]
			,"Detail":"Data type error \"a\" is not cf_sql_integer. File:\\Relay\\errorHandler\\createanerror.cfm. Line:196","Message":"Data type error a cf_sql_integer numeric.","LocalizedMessage":"Data type error a cf_sql_integer numeric.","ErrorCode":""
		}
	'>

<cfset variables.errors.undefinedVariable = '
		{
			"Suppressed":[],"Type":"Expression","name":"XXXX"
			,"tagContext":[{"RAW_TRACE":"\tat cfcreateanerror2ecfm751275842.runPage(#relaypath#\\errorHandler\\createanerror.cfm:59)","ID":"??","TEMPLATE":"#relaypath#\\errorHandler\\createanerror.cfm","LINE":59,"TYPE":"CFML","COLUMN":0},{"RAW_TRACE":"\tat cfapplication2ecfc751688446$funcONREQUEST.runFunction(#relaypath#\\application.cfc:16)","ID":"CFINCLUDE","TEMPLATE":"#relaypath#\\application.cfc","LINE":16,"TYPE":"CFML","COLUMN":0}] 
			,"Detail":"","ErrNumber":0,"Message":"Variable XXXX is undefined.","LocalizedMessage":"Variable XXXX is undefined."
		} 
	'>

<cfset variables.errors.noTagContext = '
		{
			"Suppressed":[],"Type":"Expression","name":"XXXX"
			,"Detail":"","ErrNumber":0,"Message":"Variable XXXX is undefined.","LocalizedMessage":"Variable XXXX is undefined."} 
	'>

<cfset variables.errors.emptyTagContext = '
		{
			"Suppressed":[],"Type":"Expression","name":"XXXX"
			,"tagContext":[]
			,"Detail":"","ErrNumber":0,"Message":"Variable XXXX is undefined.","LocalizedMessage":"Variable XXXX is undefined."} 
	'>


<cfloop collection = "#variables.errors#" item="item">
	<cfif isJSON(variables.errors[item])>
		<cfset variables.errors[item] = deserializejson (variables.errors[item])>
	</cfif>
</cfloop>

</cfcomponent>	
