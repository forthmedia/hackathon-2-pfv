<!---

WAB 2016-01-11
Test scripts for checking 
	application.com.flag.setFlagData 
	application.com.flag.processFlagForm
	/flags.flagTask.cfm

--->

<cfcomponent extends="testbox.system.BaseSpec" skip="skipIfLiveSite">
<cfscript>
variables.reCreateTestEntity = false;   // this parameter can be set to true if you edit the stored procedure _EntityUpsert_CreateTestEntity and need to regenerate
variables.clearDownTestEntity = listfind ("0,1",application.testsite);
variables.entityType = "entityUpsertTest";


function skipIfLiveSite () {
	return !application.testsite;
}

function beforeTests () {
	/* create a test entity */
	var testForSP = new query (sql = "select 1 from sysobjects where name = '_EntityUpsert_CreateTestEntity'");
	if (testForSP.execute().getPrefix().recordCount == 0 or reCreateTestEntity) {
		var scriptPath = expandpath ("\unitTests") & "\..\sql\entityUpsert\_EntityUpsert_CreateTestEntity.sql";
		if (fileExists (scriptPath)) {
			var fileContent = fileRead(scriptPath);
			var querySections = application.com.regExp.refindAllOccurrences (reg_expression = "(.+?)(GO|\Z)", string = filecontent, groupnames = {1="SQL",2="theGO"}, usejavamatch = false);
			arrayEach (querySections, function (item) {
				if (trim (item.sql) is not "") {
					new query (sql = item.sql).execute();
				}
			});
		}
	};
	
	var testForTable = new query (sql = "select 1 from sysobjects where name = 'EntityUpsertTest'");
	if (testForTable.execute().getPrefix().recordCount == 0 OR reCreateTestEntity) {
		new query (sql = "exec _EntityUpsert_CreateTestEntity").execute();
	
		/* reload entity structure */
		application.com.flag.createFlagTypeAndFlagEntityTypeStructures ();
		application.com.applicationVariables.setEntityTypeVariables();	
		application.com.flag.loadApplicationStructures ();

	}	


	/* create an entity */
	entityqry = new query (sql = "insert into entityUpsertTest (recordUID,textnotnull,dateNotNullFromVarchar) values  ('123','a',getdate()); select scope_identity() as entityUpsertTestID").execute();	
	/* NOTE this IS a global variable */
	variables.entityID = entityqry.getResult().entityUpsertTestID;

}


function afterTests_ () { 

	var deleteRows = new query (sql = "delete from #entityType#").execute();		

	if (clearDownTestEntity) {
		
		var dropTable = new query (sql = "drop table #entityType#");		
	
	}

}

function test_SetIntegerMultipleFlag() {
	genericProcessFlagForm ("anIntegerMultipleFlag","#randRange (1,1000)#,#randRange (1,1000)#,#randRange (1,1000)#,#randRange (1,1000)#");
}


function test_SetTextMultipleFlag() {
	genericProcessFlagForm ("aTextMultipleFlag","A#randRange (1,1000)#,B#randRange (1,1000)#,C#randRange (1,1000)#,D#randRange (1,1000)#");
}


function genericSet (flagTextID, value) {
	application.com.flag.setFlagData (flagid = flagTextID, data = value, entityid = entityID);
	var checkResult = application.com.flag.getFlagData (flagid = flagTextID, entityid = entityID).data;
	$assert.isEqual( value , checkResult ) ; 

}


function genericProcessFlagForm (flagTextID, value) {
	var flagType = application.com.flag.getflagStructure(flagTextID).flagType.name;
	form["frm#entityType#FlagList"] = "#flagType#_#flagTextID#";
	form ["#flagType#_#flagTextID#_#entityID#_orig"] = "";
	form ["#flagType#_#flagTextID#_#entityID#"] = value;
	application.com.flag.processFlagForm (entityType = entityType , entityID = entityID);
	var checkResult = application.com.flag.getFlagData (flagid = flagTextID, entityid = entityID);
	/* listsort done for the multiple flags, but OK for others */
 	$assert.isEqual( listsort(ListRemoveDuplicates(value), "text") , listSort(valueList(checkResult.data), "text")) ; 
 	structClear (form);

}


function genericProcessFlagTask (flagTextID, value) {
	var flagType = application.com.flag.getflagStructure(flagTextID).flagType.name;
	var frmentityid = entityID;
	var frmtask = "update";
	form["flagList"] = "#flagType#_#flagTextID#";
	form ["#flagType#_#flagTextID#_#entityID#_orig"] = "";
	form ["#flagType#_#flagTextID#_#entityID#"] = value;
	include "/flags/flagTask.cfm";
	var checkResult = application.com.flag.getFlagData (flagid = flagTextID, entityid = entityID);
	/* listsort done for the multiple flags, but OK for others */
 	$assert.isEqual( listsort(ListRemoveDuplicates(value), "text") , listSort(valueList(checkResult.data), "text")) ; 
 	structClear (form);

}


function genericProcessFlagFormGroup (flagGroupTextID, value, oldvalue = "") {

	var flagType = application.com.flag.getflagGroupStructure(flagGroupTextID).flagType.name;
	form["frm#entityType#FlagList"] = "#flagType#_#flagGroupTextID#";
	form ["#flagType#_#flagGroupTextID#_#entityID#"] = value;
	form ["#flagType#_#flagGroupTextID#_#entityID#_orig"] = oldvalue;
	application.com.flag.processFlagForm (entityType = entityType , entityID = entityID);
	var checkResult = application.com.flag.getFlagGroupData (flagGroupid = flagGroupTextID, entityid = entityID);

	/* listsort done for the multiple flags, but OK for others */
 	$assert.isEqual( listsort(ListRemoveDuplicates(value), "numeric") , listSort(valueList(checkResult.flagId), "numeric")) ; 
 	structClear (form);

}

function genericProcessFlagTaskGroup (flagGroupTextID, value, oldvalue = "") {

	var frmentityid = entityID;
	var frmtask = "update";
	var flagType = application.com.flag.getflagGroupStructure(flagGroupTextID).flagType.name;
	var flagList = "#flagType#_#flagGroupTextID#";
	form ["#flagType#_#flagGroupTextID#_#entityID#"] = value;
	form ["#flagType#_#flagGroupTextID#_#entityID#_orig"] = oldvalue;
	include "/flags/flagTask.cfm";
	var checkResult = application.com.flag.getFlagGroupData (flagGroupid = flagGroupTextID, entityid = entityID);

	/* listsort done for the multiple flags, but OK for others */
 	$assert.isEqual( listsort(ListRemoveDuplicates(value), "numeric") , listSort(valueList(checkResult.flagId), "numeric")) ; 
 	structClear (form);

}


function genericDelete (flagTextID) {
	application.com.flag.deleteFlagData (flagid = flagTextID, entityid = entityID);
	var checkResult = application.com.flag.getFlagData (flagid = flagTextID, entityid = entityID).data;
	$assert.isEmpty( checkResult) ; 
}


/* Integer */
function test_SetIntegerFlag() {
	var flagTextID = "anIntegerFlag";
	genericSet (flagTextID, randRange (1,1000));
	genericSet (flagTextID, randRange (1,1000));
	genericDelete (flagTextID);	

}

function test_SetIntegerFlag_FlagForm() {
	var flagTextID = "anIntegerFlag";
	genericProcessFlagForm (flagTextID, randRange (1,1000));
	genericProcessFlagForm (flagTextID, randRange (1,1000));
	genericDelete (flagTextID);	
}


function test_SetIntegerFlag_FlagTask() {
	var flagTextID = "anIntegerFlag";
	genericProcessFlagTask (flagTextID, randRange (1,1000));
	genericProcessFlagTask (flagTextID, randRange (1,1000));
	genericDelete (flagTextID);	
}


/* Date */
function test_SetDateFlag() {
	var flagTextID = "aDateFlag";
	genericSet (flagTextID, now () );
	genericSet (flagTextID, dateAdd ("d", -1, now () ) );
	genericDelete (flagTextID);	
}

function test_SetDateFlag_FlagForm() {
	var flagTextID = "aDateFlag";
	genericProcessFlagForm (flagTextID, now () );
	genericProcessFlagForm (flagTextID, dateadd("d",1,now ()));
	genericDelete (flagTextID);	
}


function test_SetDateFlag_FlagTask() {
	var flagTextID = "aDateFlag";
	genericProcessFlagTask (flagTextID, now () );
	genericProcessFlagTask (flagTextID, dateadd("d",1,now ()));
	genericDelete (flagTextID);	
}


/* Text */
function test_SetTextFlag() {
	var flagTextID = "aTextFlag";
	genericSet (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericSet (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericDelete (flagTextID);	
}

function test_SetTextFlag_FlagForm() {
	var flagTextID = "aTextFlag";
	genericProcessFlagForm (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericProcessFlagForm (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericDelete (flagTextID);	
}

function test_SetTextFlag_FlagTask() {
	var flagTextID = "aTextFlag";
	genericProcessFlagTask (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericProcessFlagTask (flagTextID, "A String with ' and "" and ## and a random bit " & randRange (1,1000));
	genericDelete (flagTextID);	
}


/* Decimal */
function test_SetDecimalFlag() {
	var flagTextID = "aDecimalFlag";
	genericSet (flagTextID, randRange (1,100000)/100);
	genericSet (flagTextID, randRange (1,100000)/100);
	genericDelete (flagTextID);	
}

function test_SetDecimalFlag_FlagForm() {
	var flagTextID = "aDecimalFlag";
	genericProcessFlagForm (flagTextID , randRange (1,100000)/100);
	genericProcessFlagForm (flagTextID , randRange (1,100000)/100);
	genericDelete (flagTextID);	
}

function test_SetDecimalFlag_FlagTask() {
	var flagTextID = "aDecimalFlag";
	genericProcessFlagTask (flagTextID , randRange (1,100000)/100);
	genericProcessFlagTask (flagTextID , randRange (1,100000)/100);
	genericDelete (flagTextID);	
}


/* Financial */
function test_SetFinancialFlag() {
	var flagTextID = "aFinancialFlag";
	genericSet (flagTextID , randRange (1,100000)/100);
	genericSet (flagTextID , randRange (1,100000)/100);
	genericDelete (flagTextID);	
}

function test_SetFinancialFlag_FlagForm() {
	var flagTextID = "aFinancialFlag";
	genericProcessFlagForm (flagTextID , randRange (1,100000)/100);
	genericProcessFlagForm (flagTextID , randRange (1,100000)/100);
	genericDelete (flagTextID);	
}

function test_SetFinancialFlag_FlagTask() {
	var flagTextID = "aFinancialFlag";
	genericProcessFlagTask (flagTextID , randRange (1,100000)/100);
	genericProcessFlagTask (flagTextID , randRange (1,100000)/100);
	genericDelete (flagTextID);	
}


/* Checkbox */
function test_BooleanFlag() {
	application.com.flag.setBooleanFlag (flagTextID = "aBooleanFlag", entityID = entityID);
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = "aBooleanFlag", entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = "aBooleanFlag", entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = "aBooleanFlag", entityID = entityID) );
		
}

function test_checkboxFlag_FlagForm() {
	flagTextID = "aBooleanFlag";
	flagStructure = application.com.flag.getFlagStructure (flagTextID );
	genericProcessFlagFormGroup (flagStructure.flagGroup.flagGroupTextID, flagStructure.flagID);
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID , entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = "aBooleanFlag", entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID , entityID = entityID) );
		
}

function test_checkboxFlag_FlagTask() {
	flagTextID = "aBooleanFlag";
	flagStructure = application.com.flag.getFlagStructure (flagTextID );
	genericProcessFlagTaskGroup (flagStructure.flagGroup.flagGroupTextID, flagStructure.flagID);
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID , entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = "aBooleanFlag", entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID , entityID = entityID) );
		
}


function test_checkboxFlag_FlagForm_TwoFlags() {
	flagTextID1 = "aBooleanFlag";
	flagTextID2 = "aBooleanFlagBit";
	flagStructure1 = application.com.flag.getFlagStructure (flagTextID1 );
	flagStructure2 = application.com.flag.getFlagStructure (flagTextID2 );

	genericProcessFlagFormGroup (flagStructure1.flagGroup.flagGroupTextID, "#flagStructure1.flagID#,#flagStructure2.flagID#");
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID1 , entityID = entityID) );
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = flagTextID1, entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID1 , entityID = entityID) );
	$assert.isEqual( true, application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
	application.com.flag.unsetBooleanFlag (flagID = flagTextID2, entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );		
}

function test_checkboxFlag_FlagTask_TwoFlags() {
	flagTextID1 = "aBooleanFlag";
	flagTextID2 = "aBooleanFlagBit";
	flagStructure1 = application.com.flag.getFlagStructure (flagTextID1 );
	flagStructure2 = application.com.flag.getFlagStructure (flagTextID2 );

	genericProcessFlagTaskGroup (flagStructure1.flagGroup.flagGroupTextID, "#flagStructure1.flagID#,#flagStructure2.flagID#");
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID1 , entityID = entityID) );
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = flagTextID1, entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID1 , entityID = entityID) );
	$assert.isEqual( true, application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
	application.com.flag.unsetBooleanFlag (flagID = flagTextID2, entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );		
}


/* Radio */
function test_radioFlag_FlagForm() {
	flagTextID1 = "radio1";
	flagStructure1 = application.com.flag.getFlagStructure (flagTextID1 );
	genericProcessFlagFormGroup ( flagGroupTextID = flagStructure1.flagGroup.flagGroupTextID, value = flagStructure1.flagID, oldvalue = "");
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID1 , entityID = entityID) );
	
	
	flagTextID2 = "radio2";
	flagStructure2 = application.com.flag.getFlagStructure (flagTextID2 );
	genericProcessFlagFormGroup ( flagGroupTextID = flagStructure2.flagGroup.flagGroupTextID, value = flagStructure2.flagID, oldvalue = flagStructure1.flagID);
	$assert.isEqual( true , application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
	
	application.com.flag.unsetBooleanFlag (flagID = flagTextID2, entityID = entityID);	
	$assert.isEqual( false, application.com.flag.checkBooleanFlagByID (flagID = flagTextID2 , entityID = entityID) );
		
}



</cfscript>
</cfcomponent>

