component extends="testbox.system.BaseSpec" skip="skipIfLiveSite" {

    function beforeTests() {
        // executes before all tests
    }

    function afterTests() {
        // executes after all tests
    }

    function skipIfLiveSite () {
       return !application.testsite;
    }

    /**
    * @hint Simple test to check the default values for the SalesForce settings.
    * TODO: add other/future SF related settings to this test if appropriate
    **/
    public void function testSalesForceSettings() {
        var triggerAutoResponseEmailKey = "connector.salesforce.apiHeaders.emailHeader.triggerAutoResponseEmail";
        var triggerOtherEmailKey = "connector.salesforce.apiHeaders.emailHeader.triggerOtherEmail";
        var triggerUserEmailKey = "connector.salesforce.apiHeaders.emailHeader.triggerUserEmail";

        // get current SF email trigger settings
        var triggerAutoResponseEmail = application.com.settings.getSetting(triggerAutoResponseEmailKey);
        var triggerOtherEmail = application.com.settings.getSetting(triggerOtherEmailKey);
        var triggerUserEmail = application.com.settings.getSetting(triggerUserEmailKey);

        // enable all
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerAutoResponseEmailKey, variableValue=1);
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerOtherEmailKey, variableValue=1);
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerUserEmailKey, variableValue=1);

        // check the expected values
        $assert.isTrue(application.com.settings.getSetting(triggerAutoResponseEmailKey), "Expected to be true");
        $assert.isEqual(true, application.com.settings.getSetting(triggerOtherEmailKey));
        $assert.isEqual(1, application.com.settings.getSetting(triggerUserEmailKey));

        // set back to the original values
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerAutoResponseEmailKey, variableValue=triggerAutoResponseEmail);
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerOtherEmailKey, variableValue=triggerOtherEmail);
        application.com.settings.InsertUpdateDeleteSetting(variableName=triggerUserEmailKey, variableValue=triggerUserEmail);
    }

}