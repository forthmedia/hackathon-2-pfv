<!--- 
A file to hammer the security system to try to generate XML corruptions
Calls testFileSecurity() multiple times in multiple threads and then automatically reloads to start all over again 
Best to run from a few separate browsers
 --->

<cfoutput>
	Start: #now()# <br />
</cfoutput>

<!--- 	Get a query of files which we are going to test 
		Start a number of threads to loop through the files
		Call testFileSecurity on each file	
--->
<cfdirectory action="list" name="files" directory="#application.paths.relay#" filter="*.cfm" recurse=true>

<cfset feed = {files = files, number = 200, threads = 4}>
<cfset threadnames = "">

<cfloop index="i" from = "1" to = "#feed.threads#">
	<cfset threadnames = listappend (threadnames,i)>
	 <cfthread name="#i#" feed= #feed#>
		<cfset thread.result = []>
		<cfloop index="i" from="1" to= "#feed.number#">
			<cfset num = max(1,int (rand() * feed.files.recordcount))>
			<cfset directory = feed.files.directory[num]>
			<cfset name = feed.files.name[num]>
			<cfset scriptname = replaceNoCase (directory & "\" & name,application.paths.relay,"")>
			<cfset securityResult = application.com.security.testFileSecurity(scriptname = scriptname)>
			<cfset arrayAppend (thread.result,{name = name, result = securityResult.isok})>
		</cfloop>	

	</cfthread>

</cfloop>

<cfthread action="join" name= #threadnames# />


<cfoutput>	
	Threads: #feed.threads#	<br />	
	Files per thread: #feed.number#<br />
 	End: #now()# <br />
</cfoutput>


<!--- Final Test to look for a corruption.  This file should be accessible --->
<cfset securityResult = application.com.security.testFileSecurity(scriptname = "index.cfm")>

<cfif not securityResult.isOK>
	<cfoutput>
		<font color="red">Corruption has occurred</font>
	</cfoutput>
	<cfdump var="#cfthread#">
<cfelse>
	Reloading <br />
	<script>
	location.href = location.href 
	</script>
</cfif>

