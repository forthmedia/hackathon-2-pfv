
<cfparam name="method" default = "edit" >

<cfquery name="dummyScreen">
select
	'#entityType#'	as fieldSource
	, name as fieldTextID
	, 0 as required
	, 0 as readonly
	, 1 as thisLineCondition	
	, label 
	, '' as specialFormatting
	, '#method#' as method
from
	vFieldMetaData
where
	tablename = '#entityType#'		
</cfquery>


<cfparam name="entityType" >
<cfparam name="entityID" >


	<cfscript>
		entity = application.com.entities.load (entityType = entityType, entityID = entityID);
		formObject = new form.formobject (entity = application.com.entities.load(entityType, entityID), definition  = dummyScreen );
		html = formObject.render();

		writeoutput ("
			#html#
		");
	</cfscript>



	
