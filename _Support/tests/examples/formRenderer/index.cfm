<cf_includeJavascriptOnce template="/javascript/openWin.js">

<cfset screensToShow = [
	  {entityType = "lead", entityID = 327, screenid = 561, comment = "A lead"}
	, {entityType = "lead", entityID = 0, screenid = 561, comment = "A new lead"}
	, {entityType = "person", entityID = 1, screenid = 39, comment = "A Person with Location fields"}
	, {entityType = "person", entityID = 1, screenid = 433, comment = "Another person with location fields"}
	, {entityType = "person", entityID = 1, screenid = 501, comment = "A Person screen demonstrating Conditional Display"}
	
]>


<cfloop array = #screensToShow# index = "item">
	<cfoutput>
			<a href="displayAForm.cfm?entityType=#item.entityType#&entityID=#item.entityID#&ScreenID=#item.screenid#" target="_new">#item.comment#</a><br />
	</cfoutput>
</cfloop>

	<a href="displayEveryField.cfm?entityType=person&entityID=1" target="_blank">Every Person Field</a><br />
	<a href="displayEveryField.cfm?entityType=person&entityID=1&method=view" target="_blank">Every Person Field (view Mode)</a><br />
	<a href="displayEveryField.cfm?entityType=location&entityID=1" target="_blank">Every Location Field</a><br />
	<a href="displayEveryField.cfm?entityType=location&entityID=1&method=view" target="_blank">Every Location Field (view Mode)</a><br />


<cfquery name="getAllScreens">
	select screenid, screenName, entityType  from screens where entityType is not null
</cfquery>

<script>
	function displayScreen (obj) {
		var entityType = obj[obj.selectedIndex].getAttribute('data-entityType')
		console.log(entityType)
		var screenID = $F(obj)
		openWin ('displayAForm.cfm?entityType=' + entityType + '&ScreenID=' + screenID,'window' + screenID)
		
	}
</script>

<cf_select name="x" query="#getAllScreens#" display="screenname" value="screenid" onChange="displayScreen(this)" dataAttributes="entityType" nullText="Choose a screen" showNull=true>


Other Test Pages (not forms)<br />
	<a href="personQuery.cfm" target="_blank">Run Person Query</a><br />
	<a href="LocationQuery.cfm" target="_blank">Run Location Query</a><br />
	<a href="LocationAddPerson.cfm" target="_blank">Add Person to Location Object</a><br />
	
	<a href="testScreen.cfm?" target="_blank">A manually crafted screen for testing some of the special features of old style screens</a><br />	
	
	<a href="/testbox/test-browser/index.cfm?path=%2Fentities&action=runTestBox" target="_blank">Unit Tests</a><br />	
