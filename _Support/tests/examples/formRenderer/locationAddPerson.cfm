<cf_readCode position="before">
	<!-- Load a Location -->
	<cfset locationObj = application.com.entities.load("location", 5010 )>
	
	<!-- get People -->
	<cfset people = locationObj.getPersons()>
	<cfdump var="#people#">

</cf_readCode>


<br />

<cf_readCode position="before">
	<!-- Add a new Person to the location -->
	<cfset add = locationObj.addPerson(firstName = 'william',lastname = "bibby")>
	<cfoutput>Record #add.getPersonID()# #add.personid# added</cfoutput>
	<cfdump var="#locationObj.getPersons()#">
</cf_readCode>	

<br />

<cf_readCode position="before">
	<!-- Now Delete Person -->
	<cfset locationObj.removePerson (firstName = 'william',lastname = "bibby")>
	<cfdump var="#locationObj.getPersons()#">

</cf_readCode>	