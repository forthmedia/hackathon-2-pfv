<cfparam name="method" default = "false" >

<cfquery name="dummyScreen">
select
	fieldSource, fieldTextID, evalString, required, thisLineCondition, Label, specialFormatting, readonly
from
	(
		-- , ('fieldSource', 'fieldTextID', 'evalString', 0, 'true', 'label', '','false')
		values 
				('query','myquery', 'select personid from person where personid = [[user.personid]]', 0,'','Run A Query', '', 'false')

			 , ('Text','myQuery.personid =  [[myQuery.personid]]', '', 0, 'myquery.personid = user.personid', 'This line only appears if condition ''myquery.personid = user.personid'' is true (which it is)', '', 'false')

			 , ('Text','person.FirstName =  [[person.FirstName]].  ', '', 0, '', 'Use a merge field to output any data from person object using [[]] notation', '', 'false')

			 , ('Text','person.location.locAccManager_.fullname: [[person.location.locAccManager_.fullname]].  ', '', 0, '', 'ditto', '', 'false')

			
			, ('include', '\examples\formRenderer\aFileToInclude.cfm', '', 0, 'true', '', '','false')

			, ('Text', 'The file include in the above line set the variable AVARIABLESETINTEMPLATE =  [[AVARIABLESETINTEMPLATE]].  And there should be a hidden field below here and then a postprocess hidden field', '', 0, 'true', 'label', '','false')

			, ('HiddenField', 'aHIddenField', 'The Value is [[AVARIABLESETINTEMPLATE]]', 0, 'true', 'label', '','false')
		
			, ('screenPostProcess', '123', '', 0, 'true', 'label', '','false')

			, ('Label', '', '', 0, 'true', 'label', 'This is the label','false')
			
			 , ('sectionStart', 'A section Heading.  Below here I am going to display a flagGroup and then a random list of flags', '', 0, 'true', '', '','false')

			, ('flagGroup', 'flagGroup_318', '', 0, 'true', '', '','false')

			, ('flagList', 'flag_2753,flag_2235', '', 0, 'true', 'This is a flag list', '','false')

			, ('sectionEnd', 'A section Heading', '', 0, 'true', '', '','false')
			
			, ('person', 'firstname', '[[ucase(value)]]', 0, 'true', 'The line uses an expression in the evalString [[ucase(value)]]', '', 'true')

			, ('person', 'firstname', '[[lcase(thisLineData)]]', 0, 'true', 'The line uses an expression in the evalString [[lcase(thisLineData)]]', '', 'true')

			, ('person', 'firstname', 'actually: [[person.lastname]]', 0, 'true','The line uses an expression in the evalString ''Actually: [[person.lastname]]'' ', '', 'true')
						
	) as x 	(fieldSource, fieldTextID, evalString, required, thisLineCondition, Label, specialFormatting,readonly)
</cfquery>


<cfparam name="entityType" default="person">
<cfparam name="entityID" default="1">


	<cfscript>
		entity = application.com.entities.load (entityType = entityType, entityID = entityID);
		formObject = new form.formobject (entity = application.com.entities.load(entityType, entityID), definition  = dummyScreen );
		html = formObject.render();

		writeoutput ("
			#html#
		");
	</cfscript>

