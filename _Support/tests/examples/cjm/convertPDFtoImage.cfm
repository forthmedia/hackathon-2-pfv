This file is used to test the operation of the PDF to Image Conversion Routine<br />
It will convert any files in its folder<BR>


<cfscript>
pdfDecode = application.com.relayImage.getpdfDecoder();
pdfDecodeVersion = pdfDecode.version;
</cfscript>

<cfoutput>
	The conversion is using jPedal #pdfDecodeVersion#<br /><br />
</cfoutput>

<cfset directoryDetails = application.com.fileManager.getFileDetailsFromPath (GetCurrentTemplatePath())>
<cfdirectory name="pdfFiles" action="list" directory="#directoryDetails.path#" filter="*.pdf">

<cfset temporaryFolder = application.com.fileManager.getTemporaryFolderDetails()>

<cfloop query="pdffiles">
	<cfset newFile = application.com.relayImage.ConvertPdfToImage (
		source = "#directory#\#name#",
		destination = "#temporaryFolder.path#\",
		filename = "#name#",
		page = 1,
		type="PNG",
		width = "2550",
		height = "3300"
	)>
	
	<cfoutput>
		<a href="#name#" target="pdf">#name#</a><br />
		<a href="#temporaryFolder.webpath#/#newFile#" target="image">#newFile#</a><br /><br />
	</cfoutput>
</cfloop>	
	
	
