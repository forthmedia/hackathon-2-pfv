This file is used to test the operation of the PDF to Image API do.convertapi.com/Pdf2Image<br />
It will convert any files in its folder<BR>

<cfset directoryDetails = application.com.fileManager.getFileDetailsFromPath (GetCurrentTemplatePath())>
<cfdirectory name="pdfFiles" action="list" directory="#directoryDetails.path#" filter="*.pdf">

<cfset temporaryFolder = application.com.fileManager.getTemporaryFolderDetails()>

<cfloop query="pdffiles">

	<cfset pdfFileName = listFirst(name,".")>

	<cfhttp 
		url="https://do.convertapi.com/Pdf2Image" 
		method="POST"
		path="#temporaryFolder.path#" 
	    file="#pdfFileName#.zip"> 
		>
		<cfhttpparam name="apikey" type="FORMFIELD" value="562764758">
		<cfhttpparam name="file" type="file" file="#directory#\#name#">
		<cfhttpparam name="OutputFormat" type="FORMFIELD" value="JPG">
		<cfhttpparam name="JpgQuality" type="FORMFIELD" value="100">

		<cfhttpparam name="ImageResolutionH" type="FORMFIELD" value="300">
		<cfhttpparam name="ImageResolutionV" type="FORMFIELD" value="300">
		
	</cfhttp>


		<cfzip
			action = "list"
			file = "#temporaryFolder.path#/#pdfFileName#.zip"
			name="files"
		> 

		<cfzip
			action = "unzip"
			destination = "#temporaryFolder.path#"
			file = "#temporaryFolder.path#/#pdfFileName#.zip"
		> 

		<cfloop query = "files">
			<cfset newfileName = "#pdfFileName#-#currentRow#.jpg">
			<cffile action="rename" source="#temporaryFolder.path#/#name#" destination="#temporaryFolder.path#/#newfileName#">
			<cfoutput><a href="#temporaryFolder.webpath#/#newfileName#" target="jpg">#newfileName#</a></cfoutput><br />
		</cfloop>



</cfloop>	
	
	
