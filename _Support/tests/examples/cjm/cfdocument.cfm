<!--- 
Check whether the CFDocument is working properly

version 6 of jPedal.jar seems to have some problem with fonts
 --->

<cfdocument userAgent="html" localURL="yes" format="PDF" overwrite="true" unit="in"  pageType="custom" pagewidth="8.5" pageheight="11" margintop="0" marginbottom="0" marginright="0" marginleft="0">

<div class="textNode" style="width: 570px; height: 288px; font-family: Arial; font-size: 40px; font-weight: bold; font-style: normal; text-align: left; color: rgb(0, 0, 0);">
This is Arial Font when exported as a web template
</div>

</cfdocument>
