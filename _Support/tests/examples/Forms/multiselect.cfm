<cfquery name="countries">
select top 10 countryid as datavalue, countrydescription as displayValue
from country
</cfquery>

This section was used to check that the multiselect plugin was working when inside a table (which it wasn't at some point)

<table>
	<tr>
		<td>
			This select box is inside a table
			<cf_select name = "country" query="#countries#" multiple = true>

		</td>
	</tr>
</table>


This select box is not
			<cf_select name = "country2" query="#countries#" multiple = true>



This section tests whether the multiselect works when a help icon is displayed

<cfset request.relayFormDisplayStyle = "HTML-Table">

<table>

<cf_relayformdisplay>

	<cf_relayformElementDisplay name="country3" label = "A select box" type="select" multiple = "true" notetext = "wdaadsadad" noteTextPosition="right" noteTextImage ="true" query = #countries# currentvalue= "">

</cf_relayformdisplay>

</table>