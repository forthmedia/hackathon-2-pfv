<!--- 
WAB 2016-10-24 To demonstrate the use of requiredFunction attribute
 --->
<cfset application.com.request.setDocumentH1(Text="Form Validation Example using requiredFunction")>

Try submitting the form.<br />
You should find that Field 2 is required.<br />
Now add some text in Field 1, the 'requiredness' of field 2 should disappear.<br />
<br />

<form >
<cf_readCode>
	<cf_relayFormDisplay autoRefreshRequiredClass=true>
			<cf_relayFormElementDisplay label ="Field 1" fieldname="field1" relayFormElementType="text" currentValue="">
			<cf_relayFormElementDisplay label ="Field 2" fieldname="field2" relayFormElementType="text" currentValue="" 
										requiredFunction="return (jQuery('##field1').val()=='')?true:false" 
										requiredMessage="This Field is Required if no value is entered in Field 1">
			<cf_relayFormElementDisplay label ="Submit" relayFormElementType="submit">
	</cf_relayFormDisplay>
</cf_readCode>
</form>
	