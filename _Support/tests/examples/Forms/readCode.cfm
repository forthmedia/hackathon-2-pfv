<!---
WAB 2016-01-25

Tag to display the code which is being run


Very useful coding example pages - you can see the code and the result without diving into the source code


Usage
<cf_readCode>
	Put the tag
	around any code
	that you
	want to be displayed on screen
</cf_readCode>

It will automatically output the code either before or after the code is run
	position = before/after

Or set display=false
and use
	<cf_outputCode> to output the code anywhere you please


--->

<cfparam name = "attributes.display" default = "true">
<cfparam name = "attributes.position" default = "after" >
<cfparam name = "attributes.ignoreLinesAtStart" default = "0">
<cfparam name = "attributes.ignoreLinesAtEnd" default = "0">

<cfif thisTag.executionMode is "start">
	<cfset start = application.com.globalfunctions.getFunctionCallingFunction ('readCode.cfm')>
	<cfset readCode = application.com.globalFunctions.getLinesFromAFile(filename = start.templateAbsolutePath, line = start.linenumber + 1 + attributes.ignoreLinesAtStart, untilRegexp = "<\/cf_readcode>")>

	<cfset caller.readcode = readcode>
	<cfif attributes.display and attributes.position is "before">
		<cf_outputCode>
	</cfif>

<cfelseif attributes.display and attributes.position is "after">
	<cf_outputCode>
</cfif>




