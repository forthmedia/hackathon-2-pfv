<!---
WAB 2016-01-25

Tag to display the code which is being run

See <cf_readcode>

--->

<cfif structKeyexists (caller,"readCode")>
	<cfset getLines = caller.readCode>

	<cfset result = "<pre><code>">
	<cfset len = arrayLen(getLines.lines) - 1>
	<cfloop index="Line" from = 1 to = #len#>
		<cfset result &= "#htmleditformat(getLines.lines[line])#<BR>">
	</cfloop>
	<cfset result &= "</code></pre>">

	<cfoutput>#result#</cfoutput>
</cfif>