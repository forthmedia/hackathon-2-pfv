<cf_includejavascriptonce template="/javascript/checkObject.js">
<cf_includejavascriptonce template="/javascript/verifyObjectV2.js">
<cf_includejavascriptonce template="/javascript/rwFormValidation.js" translate="true">
<cf_includejavascriptonce template="/javascript/cf/cfform.js">



<form id="myForm" method="post" novalidate="true" enctype="multipart/form-data" >
<cf_relayformdisplay autoRefreshRequiredClass=true applyValidationErrorClass=true showValidationErrorsInline= true autoValidate=true>

	<input type="submit">

	<cf_relayFormElementDisplay fieldname="Field1" relayFormElementType="html" NoteTextImage="false"
		label ="Field 1"
		noteText = "A field with a Required Attribute."
		>
			<input name="Field1" id="Field1" required="true" class=" form-control">
			<input name="Field1_required" type="hidden" value="ssss">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay fieldname="Field2" relayFormElementType="html" NoteTextImage="false"
		label ="Field 2"
		noteText = "A required field with a Numeric Range and a required message"
		>
			<input  name="Field2" id="Field2" required="true" message="Must be numeric" validate="numeric" range="-99.99,99.99" requiredMessage="Yes This Really IS Required" class=" form-control">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 2a"	noteText = "A field using Coldfusion numeric validate"	>
			<input name="Field2a" id="Field2a" message="Must be numeric" validate="numeric" class=" form-control">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay fieldname="Email" relayFormElementType="html" NoteTextImage="false"
		label ="Email"
		noteText = "A field with Email Validation."
		>
			<input name="email" id="email" required="true" validate="email" class=" form-control" message="Must be a valid email address">
			<input name="email_required" type="hidden" value="">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>




	<cfquery name="aQuery">
		select * from
		(values (1,1), (2,2), (3,3)) as x (dataValue,displayValue)
	</cfquery>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="A Multiselect"	noteText = "A required multiselect"	>
		<cf_select query =#aQuery# name="multiselect" multiple="true" required=true currentvalue = "">
	</cf_relayFormElementDisplay>
	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 2b"	noteText = "A field using Coldfusion Validate='RegEx' pattern. "	>
			<input name="Field2b" id="Field2b" message="Must Start With @" validate="regex" pattern="^@" value = "1" class=" form-control">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>


	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 4"	noteText = "This checkbox determines the required status of the next 2 input boxes"	>
			Field 5 Required if just this item checked <input name="checkbox1"  type="checkbox" value=1><BR>
			Field 6 Required if just this item checked <input name="checkbox1"  type="checkbox" value=2>
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay name = "Field5" relayFormElementType="html" NoteTextImage="false"	label ="Field 5"	noteText = "Fill in if 1 is checked"	>
			<input id="Field5" name="Field5" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,1) && !isItemInCheckboxGroupChecked (this.form.checkbox1,2)) {return true} else {return false}" class=" form-control">
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay name = "Field6" relayFormElementType="html" NoteTextImage="false"	label ="Field 6"	noteText = "Fill in if 2 is checked"	>
			<input id="Field6" name="Field6" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,2) && !isItemInCheckboxGroupChecked (this.form.checkbox1,1)) {return true} else {return false}" class=" form-control">
	</cf_relayFormElementDisplay>


	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Radio 1"	noteText = "Required Radio"	>
			<div class="validation-group">
				<div class="form-group">
					<div class="radio-inline">
						<label>
							1 <input name="radio1"  type="radio" value=1>
						</label>
					</div>
					<div class="radio-inline">
						<label>
							2 <input name="radio1"  type="radio" value=2>
						</label>
					</div>
					<div class="radio-inline">
						<label>
							clear <input name="radio1"  type="radio" value="">
						</label>
					</div>
				</div>
				<input type="hidden" name="radio1_required" id="radio1_required" required="1">
			</div>
	</cf_relayFormElementDisplay>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Checkboxes 1"	noteText = "2 Items are required"	>
			<div class="validation-group">
				<div class="form-group">
					<div class="checkbox-inline">
						<label>
							1 <input name="checkbox1"  type="checkbox" value=1>
						</label>
					</div>
					<div class="checkbox-inline">
						<label>
							2 <input name="checkbox1"  type="checkbox" value=2>
						</label>
					</div>
					<div class="checkbox-inline">
						<label>
							3  <input name="checkbox1"  type="checkbox" value="3">
						</label>
					</div>
				</div>
				<input type="hidden" name="checkbox1_required" id="checkbox1_required" required="2">
			</div>
	</cf_relayFormElementDisplay>


	<input type="submit">
</cf_relayformdisplay>
</form>



<input type="button" value="Submit Using Javascript" onclick = "triggerFormSubmit($('myForm'))">


<script>
function afunctionwhichrequiresValueToBeX (form, field, value) {
	if (value == 'X') {
		return true
	} else {
		return false
	}
}

</script>



<!---
<script>
initialiseRelayFormValidation($('myForm'),{autoRefreshRequiredClass:true,applyValidationErrorClass:true})

fireEvent_($('inline'),'change')
fireEvent_($('auto'),'change')


</script>


 --->




