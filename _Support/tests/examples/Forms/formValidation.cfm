<cf_includejavascriptonce template="/javascript/checkObject.js">
<cf_includejavascriptonce template="/javascript/verifyObjectV2.js">
<cf_includejavascriptonce template="/javascript/rwFormValidation.js" translate="true">
<cf_includejavascriptonce template="/javascript/cf/cfform.js">


<cfparam name="adate" default = "0">



<form id="myForm" method="post" novalidate="true" enctype="multipart/form-data" >
<cf_relayformdisplay autoRefreshRequiredClass=true applyValidationErrorClass=true showValidationErrorsInline= true autoValidate=true>

	<input type="submit">

	<cf_relayFormElementDisplay label ="A date" fieldname="adate" relayFormElementType="html" NoteTextImage="false">
		<cf_readcode display="false">
			<cf_relaytimefield required="true" fieldname="adate" label="A Date" currentvalue="#adate#" showclearlink="true" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay fieldname="Field1" relayFormElementType="html" NoteTextImage="false"
		label ="Field 1"
		noteText = "A field with a Required Attribute."
		>
		<cf_readcode display="false">
			<input name="Field1" id="Field1" required="true" class=" form-control">
			<input name="Field1_required" type="hidden" value="ssss">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay fieldname="Field2" relayFormElementType="html" NoteTextImage="false"
		label ="Field 2"
		noteText = "A required field with a Numeric Range and a required message"
		>
		<cf_readcode display="false">
			<input  name="Field2" id="Field2" required="true" message="Must be numeric" validate="numeric" range="-99.99,99.99" requiredMessage="Really Required" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>
	<cf_relayFormElementDisplay name="Field2_"
		relayFormElementType="html" NoteTextImage="false"
		label ="Field 2"
		noteText = "A field with an onValidate attribute.<BR>
		This is backwards compatible with ColdFusion's onValidate attribute.<BR>
		The attribute is the name of a javascript function<BR>
		The function takes (form,object,value) and returns true or false.<BR>
		The message displayed comes from the message attribute"	>
			<cf_readcode display="false">
				<input  name="Field2_" id="Field2_"  message="The value in this field must be X" onValidate="afunctionwhichrequiresValueToBeX" class=" form-control">
			</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 2a"	noteText = "A field using Coldfusion numeric validate"	>
		<cf_readcode display="false">
			<input name="Field2a" id="Field2a" message="Must be numeric" validate="numeric" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cfquery name="aQuery">
		select * from
		(values (1,1), (2,2), (3,3)) as x (dataValue,displayValue)
	</cfquery>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="A Multiselect"	noteText = "A multiselect"	>
		<cf_readcode display="false">
			<cf_select query =#aQuery# name="multiselect" multiple="true" required=true >
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 2b"	noteText = "A field using Coldfusion Validate='RegEx' pattern"	>
		<cf_readcode display="false">
			<input name="Field2b" id="Field2b" message="Must Start With @" validate="regex" pattern="^@" value = "1" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>
	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 3"	noteText = "This field has a submit function - it writes something to the console"	>
		<cf_readcode display="false">
			<input  name="Field3" id="Field3" submitFunction="console.log('Submit function run on ' + this.id); return false" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Radio A"	noteText = "A radio which is required (the particular item rather than the group).  Need to make sure that the action of focusing on the first required item does not set the radio"	>
		<cf_readcode display="false"><br />
			<input name="aradio"  type="radio" value=1 required=true >
			<br />clear <input name="aradio"  type="radio" value="">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>
	
		<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Radio 2"	noteText = "A required radio group with no validation-group div"	>
		<cf_readcode display="false"><br />
			1 <input name="radio2"  type="radio" value=1><BR>
			2 <input name="radio2"  type="radio" value=2>
			clear <input name="radio2"  type="radio" value="">
			<input type="hidden" name="radio2_required" id="radio2_required" required=1 >
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 4"	noteText = "This checkbox determines the required status of the next 3 input boxes"	>
		<cf_readcode display="false"><br />
			Field 5 Required if just this item checked <input name="checkbox1"  type="checkbox" value=1><BR>
			Field 6 Required if just this item checked <input name="checkbox1"  type="checkbox" value=2>
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay name = "Field5" relayFormElementType="html" NoteTextImage="false"	label ="Field 5"	noteText = "Fill in if 1 is checked"	>
		<cf_readcode display="false">
			<input id="Field5" name="Field5" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,1) && !isItemInCheckboxGroupChecked (this.form.checkbox1,2)) {return true} else {return false}" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay name = "Field6" relayFormElementType="html" NoteTextImage="false"	label ="Field 6"	noteText = "Fill in if 2 is checked"	>
		<cf_readcode display="false">
			<input id="Field6" name="Field6" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,2) && !isItemInCheckboxGroupChecked (this.form.checkbox1,1)) {return true} else {return false}" class=" form-control">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay name = "Field7" relayFormElementType="html" NoteTextImage="false"	label ="Field 7"	noteText = "Fill in if 1 AND 2 are checked.  Demonstrates use of a requiredFunction returning a string"	>
		<cf_readcode display="false">
			<input id="Field7" name="Field7" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,1) && isItemInCheckboxGroupChecked (this.form.checkbox1,2)) {return 'Required because 1 and 2 are checked'} else {return false}">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Radio 1"	noteText = "This radio determines the required status of the next input box and is itself required if checkbox 1 set"	>
		<cf_readcode display="false">
			<div class="validation-group">
				<div class="form-group">
					<div class="radio-inline">
						<label>
							1 <input name="radio1"  type="radio" value=1>
						</label>
					</div>
					<div class="radio-inline">
						<label>
							2 <input name="radio1"  type="radio" value=2>
						</label>
					</div>
					<div class="radio-inline">
						<label>
							clear <input name="radio1"  type="radio" value="">
						</label>
					</div>
				</div>
				<input type="hidden" name="radio1_required" id="radio1_required" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,1)) {return true} else {return false}" submitFunction="console.log(this.form.radio1, checkObject(this.form.radio1));  if(checkObject(this.form.radio1) == 0) {return phr.required} else {return ''}">
			</div>
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 9"	noteText = "Fill in if Radio 1 is checked"	>
		<cf_readcode display="false">
			<input id="Field9" name="Field9" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.radio1,1)) {return true} else {return false}">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 10"	noteText = "Fill in if Radio 2 is checked"	>
		<cf_readcode display="false">
			<input id="Field10" name="Field10" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.radio1,2) ) {return true} else {return false}">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>

	<div>&nbsp;</div>

	<cf_relayFormElementDisplay relayFormElementType="html" NoteTextImage="false"	label ="Field 11"	noteText = "Fill in if Check box 1 AND 2 are checked"	>
		<cf_readcode display="false">
			<input id="Field11" name="Field11" type="Text" requiredFunction="if (isItemInCheckboxGroupChecked (this.form.checkbox1,1) && isItemInCheckboxGroupChecked (this.form.checkbox1,2)) {return true} else {return false}">
		</cf_readcode>
	</cf_relayFormElementDisplay>
	<cf_outputCode>


	<input type="submit">
</cf_relayformdisplay>
</form>




<script>
function afunctionwhichrequiresValueToBeX (form, field, value) {
	if (value == 'X') {
		return true
	} else {
		return false
	}
}

</script>



<!---
<script>
initialiseRelayFormValidation($('myForm'),{autoRefreshRequiredClass:true,applyValidationErrorClass:true})

fireEvent_($('inline'),'change')
fireEvent_($('auto'),'change')


</script>


 --->




