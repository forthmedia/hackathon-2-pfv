'''
    FN_test.py 
    Testing functions against FN pages
    
    Clive Stewart 19/3/10    
'''
#----------------------------------------------------------------------
#   TODO: 
#----------------------------------------------------------------------
#   test scripting a simple use case including RHS steps
#
#   check for missing links
#
#   explicit location - find a way to click the LH links in order
#       try //div[@class="x-panel-body "]//a/text()
#
#   Cope with link not found 
#   count pass/fails
#
#   synchronise on RH panel
#       try wait_for_frame_to_load('x:')
#
#   self.selenium.get_attribute(xpath)    
#
#   split and structure the program
#   understand xpath checker / firebug checking
#   Copy across to SVN
#   run on desktop box
#   run as junit - produce correct results
#----------------------------------------------------------------------

# NOTE To try : getAllWindowIds

# //tr[3]//a
# //div[@class="x-panel-body"]//div//tr[4]//a
# 
# This gets the names of all LH the links 
# //div[@class="x-panel-body "]//a/text()
# //div[@class="x-tab-panel-bwrap "]//a/text()


pausing=True
delayBetweenLinks=5
logfileName='output.log'
linksListFilename='allLhLinks.txt'

import sys
import unittest
from os.path import isfile
from time import sleep, asctime

sys.path.append("C:\Selenium\selenium-remote-control-1.0.3\selenium-python-client-driver-1.0.1")
from selenium import selenium

#sys.path.append("C:\Selenium\selenium-remote-control-1.0.3\selenium-java-client-driver-1.0.1")
#from selenium import selenium as JavaSelenium

#startPage    ="http://relayware8int.staging.relayware.com/index.cfm?"       # Sabna's system
#startPage    ="http://relayware9int.staging.relayware.com/index.cfm?"       # My system
#startPage    ="http://Test1-relaywareint.staging.relayware.com/index.cfm?"  # Vanilla system

#startPage    ="http://site1.demo.relayware.com/index.cfm?"                  # Demo system
startPage    ="http://test1-relaywareint/"                                   # Vanilla system

userName     ="Mike Morgan"
userPassword ="Mike"

#browserType="*opera"
#browserType="*iexplore"
browserType="*firefox"

homeLink     ='//a[contains(text(),"Home")]'
loginLink    ="//a[@class='loginLink']"
logoutLink   ="//a[@title='Logout']"
usernameField="//input[@id='frmUsername']"
passwordField="//input[@id='frmPassword']"

pageWaitTime="10000"  # milliseconds to wait for timeout

# Defined errors to check for
knownErrorTypes = {}
#knownErrorTypes["error"]                          ='ERROR'
knownErrorTypes["devHelp2@foundation-network.com"]='TEMPLATE'
knownErrorTypes["sorry"]                          ='PERMISSION'
knownErrorTypes["unexpected error"]               ='EXCEPTION'
#knownErrorTypes['File or directory not found']   ='FILENOTFOUND'
knownErrorTypes["sod_landingPageText"]            ='SODPAGETEXT'

def output(*msg):
    msg=' '.join(map(str,msg))
    print msg
    
    # Write out to a logfile 
    fl=open(logfileName,'a')
    fl.write(msg+'\n')
    fl.close()
      
#----------------------------------------------------------------------------------------
# Save and restore the session ID to save time opening and closing browsers
sessionIdfile='sessionId'
def storeSessionID(idn):
   fl=open(sessionIdfile,'w')
   fl.write(str(idn))
   fl.close()

def getSessionID():
   if isfile(sessionIdfile):
      fl=open(sessionIdfile,'r')
      idn = fl.read()
      #print 'idn',idn
      fl.close()
      if idn=='None':
         idn=None
   output('restored session ID',idn)
   return idn

def nukeSession():
   storeSessionID(None)
   getSessionID()
   sys.exit()
   
def pause(*msg):
    msg=' '.join(map(str,msg))
    if msg=='': msg=('Any key to continue')
    output(msg)
    if pausing: 
        a=raw_input('x to exit...')
        if a in ['x','X']:
            sys.exit()

def getKnownLinksList():
    fl=open(linksListFilename,"r")
    lines=fl.readlines()
    fl.close()
    return lines

# Initialise the logfile        
fl=open(logfileName,'w')
timeStamp=asctime()
fl.write("Testing started:"+timeStamp+'\n')
fl.close()
        
def getErrorCode(linkRecord):
    linkText=linkRecord
    expectErrorType=None
    for errorType in knownErrorTypes.values():
        if linkRecord.find(errorType)!=-1:
            expectErrorType=errorType
            print 'will expect error',expectErrorType
            linkText=linkRecord.strip()[:len(linkRecord)-len(errorType)-1]                                    
    return linkText,expectErrorType 
    
passes   =0
fails    =0
newfails =0
newFailsList=[]

def passTest(*reason):
    global passes, fails, newfails, newFailsList
    passes+=1
    output('Test passed',*reason)
    
def failTest(*reason):
    global passes, fails, newfails, newFailsList
    fails+=1
    output('Test failed as expected',*reason)

def newFailTest(*reason):
    global passes, fails, newfails, newFailsList
    newfails+=1
    msg='Unexpected (new) failure '+' '.join(map(str,reason))
    output(msg)
    newFailsList.append(msg)
    
def showFinalStats():    
    output('Tests passed',passes)
    output('Tests failed',fails)
    #output('New fails',newfails)
    output('\n',newfails,'New failures\n'+'\n'.join(newFailsList))
    pause()
    

class FNTest(unittest.TestCase):

    def __init__(self,extraParmIlost):
        
        try:        
            self.selenium=selenium("localhost", 4444, browserType, startPage)            
            self.restart()            
            
            # Use browsers internal xpath interpreter - to avoid problems with xhtml needing  x: before all xpath expressions
            self.selenium.allow_native_xpath('false')                        
            
            if 1:
                self.quickTest()            
            else:
                self.testAllLinks()            
                self.logout()
                showFinalStats()
                
        except Exception, e:            
            pause("Exception:",e)

    # Get the real links off the page
    def getRealLinksAndNames(self):
        linkIdToNames={}
        self.selenium.allow_native_xpath('true')
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:        
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'            
                linkText=self.selenium.get_text(linkLocator)
                linkIdToNames[linkLocator]=linkText
        return linkIdToNames        
                
    # Count how many instances of each link name in the list
    def countLinks(self,linksList):
        countedLinks={}
        for linkName in linksList:
            linkName=linkName.strip()
            if linkName=='':
                pass
            elif linkName.find('#')==0:
                pass
            elif linkName in countedLinks.keys():
                countedLinks[linkName]+=1
            else:
                countedLinks[linkName]=1
        return countedLinks

    def quickTest(self):

        # Several identically named links and xpath breaks so identify dom expression
        
        #self.selenium.open(startPage)                                             
        self.selenium.allow_native_xpath('true')

        linkcount=int(self.selenium.get_xpath_count('//a'))
        print 'linkcount:',linkcount
        
        for n in range(0,linkcount-1,10):
            linkLocator='dom=function foo() { return document.links['+str(n)+']; }; foo();'
            try:
                linkText=self.selenium.get_text(linkLocator)
                #linkRef=self.selenium.get_attribute(linkLocator+'\\@href') # How do we do this with DOM ?
                if linkText!='':
                    print n,':',linkText
                    
                self.wait_then_click(linkLocator)                    
                sleep(2)   # SHOULD SYNCHRONISE HERE 
                self.checkForExceptions()            
                
            except:
                pass
            
        pause()
    
    def savedTest(self):
        expectedLinksList=getKnownLinksList()
        countedExpectedLinks=self.countLinks(expectedLinksList)
        
        realLinksAndNames=self.getRealLinksAndNames()
        realLinksList=realLinksAndNames.values()
        countedRealLinks=self.countLinks(realLinksList)
        
        for linkName in countedExpectedLinks.keys():
            if not linkName in countedRealLinks.keys():
                failTest('Missing expected link',linkName)
            elif countedExpectedLinks[linkName] != countedRealLinks[linkName]:
                failTest('Mismatch',linkName,'expected:',countedExpectedLinks[linkName],'found:',countedRealLinks[linkName])
            else:
                passTest('Found expected link:',linkName)
            
        for linkName in countedRealLinks.keys():
            if not linkName in countedExpectedLinks.keys():
                failTest('Extra real link',linkName)
               
        # This opens the real links in 'random' order checking the page
        # We can only open real links via their discovered ID's
        for linkXpath in realLinksAndNames.keys():
            linkText=realLinksAndNames[linkXpath]
            self.checkLink(linkXpath,linkText)  #,expectedErrorType=None)
                
        showFinalStats()                                      
        pause()
        sys.exit()
        
        #linkText='Reports'
        #clickCountedLink(linkText)
        
    def getMatchingLinks(self,requiredLinkText):
        xpaths=[]
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:        
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'            
                linkTextFound=self.selenium.get_text(linkLocator)
                if requiredLinkText==linkTextFound:
                    linkHref=self.selenium.get_attribute(linkLocator+'\\@href')
                    xpaths.append('//a[@href="'+linkHref+'"]')                    
        return xpaths
        
    def restart(self):

        savedSessionId=getSessionID()
        currentUrl=None

        if savedSessionId==None:
            output('no saved session - creating a new one')
            #self.selenium.stop()
            self.selenium.start()
            storeSessionID(self.selenium.sessionId)
        else: 
            output('Found saved session - restoring',savedSessionId)
            self.selenium.sessionId=savedSessionId

        currentUrl=None 
        try:
            currentUrl=self.selenium.get_location()
            output('currentUrl',currentUrl)
            
        except Exception, e:
            self.selenium.start()
            storeSessionID(self.selenium.sessionId)
         
        if currentUrl==None: 
            output('currentUrl ',currentUrl,' != startPage',startPage)
            self.selenium.window_maximize()
            self.selenium.window_focus()
            self.selenium.open(startPage)      
            self.selenium.wait_for_page_to_load(pageWaitTime)
        elif currentUrl.find(startPage)==-1:  
            self.selenium.open(startPage)                  
        if self.selenium.is_element_present(loginLink)==1:
            self.login(userName, userPassword)
                         
    def login(self,userName,userPassword=None):
            output('Logging on as',userName)
            self.selenium.type(usernameField, userName)
            if userPassword!=None:
                self.selenium.type(passwordField, userPassword)
            self.selenium.click(loginLink)
            self.wait_for_xpath(homeLink)
        
    def logout(self):
        self.selenium.click(logoutLink)

# -------------------------------------------------------------------------------
# TESTING - KEPT FOR REFERENCE
# -------------------------------------------------------------------------------

    def testAllLinks(self):
    
        self.selenium.open(startPage)      
        expectedLinks=getKnownLinksList()
        
        skipingLines=False
        
        for expectedLink in expectedLinks:
            expectedLink=expectedLink.strip()
            
            if expectedLink.find('##')==0:
                pass
            elif expectedLink.find('#')==0 and expectedLink.find('STOP')!=-1 and expectedLink.find('##')!=0:
                pause('STOP found in file - bye')
                sys.exit()
            elif expectedLink.find('#')==0 and expectedLink.find('SKIPON')!=-1:
                skipingLines=True
            elif expectedLink.find('#')==0 and expectedLink.find('SKIPOFF')!=-1:
                skipingLines=False            
            elif expectedLink.find('#')==0:
                output('Ignored comment',expectedLink.strip())
            elif expectedLink.strip()=='':
                pass
            elif not skipingLines:    
                expectedLink,expectedErrorType=getErrorCode(expectedLink)
                linkLocator='//a[text()="'+expectedLink+'"]'
                self.checkLink(linkLocator,expectedErrorType,expectedLink)                
            else:
                output('Skipped line:',expectedLink)
        pause()

    def printHowManyLinks(self):                
        self.selenium.allow_native_xpath('true')
        linkcount=self.selenium.get_xpath_count('//table[1]//a')
        print 'linkcount //table[1]//a',linkcount
        linkcount=self.selenium.get_xpath_count('//a')
        print 'linkcount //a',linkcount
        linkcount=self.selenium.get_all_links()
        print 'linkcount get_all_links',len(linkcount)
        pause()
        
    def getAllLinks(self):
        self.selenium.allow_native_xpath('true')
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:        
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'            
                linkText=self.selenium.get_text(linkLocator)
                print 'linkText',linkText
        pause()
                                        
# -------------------------------------------------------------------------------
#  Completed test running code 
# -------------------------------------------------------------------------------
        
    def checkLink(self,linkLocator,expectedErrorType=None,linkText=''):
        msg='Checking link:'+linkText
        if expectedErrorType!=None: msg=msg+'   expecting error:'+expectedErrorType
        output(msg)
        self.wait_then_click(linkLocator)                    
        sleep(delayBetweenLinks)   # SHOULD SYNCHRONISE HERE 
        self.checkForExceptions(expectedErrorType)            
        self.selenium.open(startPage)                                             
        
# -------------------------------------------------------------------------------
#   Page handling funtions
# -------------------------------------------------------------------------------

    def wait_for_xpath(self,xpath):
        n=0
        while not self.selenium.is_element_present(xpath) and n<100:
            sleep(0.1)
            n+=1
            
    def wait_then_click(self,xpath):
        self.wait_for_xpath(xpath)
        try:
            self.selenium.click(xpath)
        except Exception, e:
            failTest("missing link",xpath)
        
    def click_if_there(self,linkLocator):
        while self.selenium.is_element_present(linkLocator):
            print 'click_if_there',linkLocator
            self.selenium.click(linkLocator)
        
    def tryToClick(self,xpath):
        global pageWaitTime
        oldPageWait=pageWaitTime
        pageWaitTime="1000"
        try:
            self.wait_then_click(xpath)
        except:
            output('Not found',xpath)
        pageWaitTime=oldPageWait
    
    def openLinkLocator(self,linkLocator):
        self.wait_then_click(linkLocator)
        linkText=self.selenium.get_text(linkLocator)
        print 'Clicking link:',linkText
        print 'Link locator: ',linkLocator
        sleep(2)
       
    def checkForExceptions(self,expectedError=None):
    
        #print 'checkForExceptions expectedError:',expectedError
        
        # Locations to search for messages
        xpaths=[
            '//body/font[2]',                                           # for the exception panel
            '//body/p',                                                 # for sorry you do not have permission text
#            '//body//tr',                                               # for Error 1012: No Query has been specificed - does not work ... xhtml again !!
            '//a[contains(text(),"devHelp2@foundation-network.com")]',  # for the error panel
            ]
            
        txt=""
        actualError=None
        for xpath in xpaths:
            if self.selenium.is_element_present(xpath):
                txt=self.selenium.get_text(xpath)

                for pageErrorText in knownErrorTypes.keys():
                    if txt.find(pageErrorText)!=-1:                
                        actualError=knownErrorTypes[pageErrorText]
                    
        if actualError==None and expectedError==None:
            passTest('Page is OK')
        elif actualError==expectedError:
            failTest(actualError)
        else:
            newFailTest(actualError)
                
# -------------------------------------------------------------------------------    
if __name__ == "__main__":    
    try:
        FNTest("name")        
    except Exception, e:
        pause("Outer exception:",e,'Any key to continue...')
# -------------------------------------------------------------------------------    
