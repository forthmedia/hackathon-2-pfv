'''
    FN_test.py 
    Testing functions against FN pages
    
    Clive Stewart 19/3/10
    
    TODO: 
        try wait_for_frame_to_load('x:')
        
        self.selenium.get_attribute(xpath)    
'''

#Certification>Profiles
#Products>XML Import
#Data>Export Data
#Data>Selections
#eCommerce>Trade Up
#eMarketing>Selections
#eService>Service Contracts
#Forecasts
#Forecasts>Forecast Viewer
#Forecasts>Forecast List
#Locator
#Locator>Edit Geodata 
#Profiles
#NO DETECTOR Checking link Sales Out Data
#Score Card
#Score Card>Scorecard Variance
#Sites>Profiles
#Screens>Profiles
#My Tools>Selections
#My Tools>Export Data
#My Tools>Analytics


startFromLink='Security'

# NOTE To try : getAllWindowIds

delayBetweenLinks=4
pausing=True
logfileName='output.log'

# If you want to just visit selected links add to this list
interestingLinks=[]

#interestingLinks+=['Trade Up','Service Contracts','Analytics']     # EXCEPTIONS

# TEMPLATE NOT FOUND
#interestingLinks+=['Export Data','Selections','Newsletters','Service Contracts','Knowledgebase','Forecasts','Forecast Viewer'
#            'Locator','Edit Geodata','Projects','Profiles','Sales out Data','Score Card','Filetype','Profiles','Analytics']

#interestingLinks+=['sys_nav_PCM_Masks']                            # STRANGE ERROR

#interestingLinks+=['My Accounts','My Actions','My Teams','My Opportunities','My Reports','Reports']  # Not found ... 
#interestingLinks+=['XML Import','Score card','Tests','Trade Up','Service Contracts','Analytics'] # Vanilla systems exceptions
#interestingLinks+=['XML Import','Score card','Tests','Scorecard Variance'] # Exceptions
#interestingLinks+=['Fund Requests'] # Empty
#interestingLinks+=['Configure','Salesforce'] # DB query error
##interestingLinks=map(lower,interestingLinks)

# To prevent use of any links
forbiddenLinks= []

import sys
import unittest
from os.path import isfile
from time import sleep, asctime
sys.path.append("C:\Selenium\selenium-remote-control-1.0.3\selenium-python-client-driver-1.0.1")
from selenium import selenium

#startPage    ="http://site1.demo.relayware.com/index.cfm?"                  # Demo system
#startPage    ="http://relayware8int.staging.relayware.com/index.cfm?"       # Sabna's system
#startPage    ="http://relayware9int.staging.relayware.com/index.cfm?"       # My system
#startPage    ="http://Test1-relaywareint.staging.relayware.com/index.cfm?"  # Vanilla system
startPage    ="http://test1-relaywareint/"                                   # Vanilla system

userName     ="Mike Morgan"
userPassword ="Mike"

browserType="*firefox"

homeLink     ='//a[contains(text(),"Home")]'
loginLink    ="//a[@class='loginLink']"
logoutLink   ="//a[@title='Logout']"
usernameField="//input[@id='frmUsername']"
passwordField="//input[@id='frmPassword']"

pageWaitTime="10000"  # milliseconds to wait for timeout

# Defined errors to check for
ERROR     ='ERROR'
TEMPLATE  ='TEMPLATE'
EXCEPTION ='EXCEPTION'
PERMISSION='PERMISSION'
knownErrorTypes = {}
knownErrorTypes["error"]='ERROR'
knownErrorTypes["devHelp2@foundation-network.com"]='EXCEPTION'
knownErrorTypes["sorry"]='PERMISSION'

def getKnownLinks():
    linksList=[]
    import sys
    fl=open("allLhLinks.txt","r")
    lines=fl.readlines()
    fl.close()
    for line in lines:
        if line.strip()!='':
            print line.strip()
    sys.exit()
    return linksList

#----------------------------------------------------------------------------------------
# Save and restore the session ID to save time opening and closing browsers
sessionIdfile='sessionId'
def storeSessionID(idn):
   fl=open(sessionIdfile,'w')
   fl.write(str(idn))
   fl.close()

def getSessionID():
   if isfile(sessionIdfile):
      fl=open(sessionIdfile,'r')
      idn = fl.read()
      #print 'idn',idn
      fl.close()
      if idn=='None':
         idn=None
   output('restored session ID',idn)
   return idn

def nukeSession():
   storeSessionID(None)
   getSessionID()
   sys.exit()
   
def pause(*msg):
    msg=' '.join(map(str,msg))
    if msg=='': msg=('Any key to continue')
    output(msg)
    if pausing: 
        raw_input()

# Initialise the output file        
fl=open(logfileName,'w')
timeStamp=asctime()
fl.write("Testing started:"+timeStamp+'\n')
fl.close()
        
def output(*msg):
    msg=' '.join(map(str,msg))
    print msg
    
    # Write out to a logfile 
    fl=open(logfileName,'a')
    fl.write(msg+'\n')
    fl.close()
       
class FNTest(unittest.TestCase):

    def __init__(self,extraParmIlost):
        
        try:        
            self.selenium=selenium("localhost", 4444, browserType, startPage)
            
            self.restart()
            
            # Use browsers internal xpath interpreter - to avoid problems with xhtml needing  x: before all xpath expressions
            self.selenium.allow_native_xpath('false')            
            
            self.justtesting()
            
            #self.runTest()
            #self.logout()
            
        except Exception, e:            
            pause("Exception:",e)

    def restart(self):

        savedSessionId=getSessionID()
        currentUrl=None

        if savedSessionId==None:
            output('no saved session - creating a new one')
            #self.selenium.stop()
            self.selenium.start()
            storeSessionID(self.selenium.sessionId)
        else: 
            output('Found saved session - restoring',savedSessionId)
            self.selenium.sessionId=savedSessionId

        currentUrl=None 
        try:
            currentUrl=self.selenium.get_location()
            output('currentUrl',currentUrl)
            
            # Load the start page as a way of clearing tabs / going home - errors with all other approaches
            # TESTING TESTING
            #self.selenium.open(startPage)                          

        except Exception, e:
            self.selenium.start()
            storeSessionID(self.selenium.sessionId)
         
        if currentUrl==None: 
            output('currentUrl ',currentUrl,' != startPage',startPage)
            self.selenium.window_maximize()
            self.selenium.window_focus()
            self.selenium.open(startPage)      
            self.selenium.wait_for_page_to_load(pageWaitTime)
        elif currentUrl.find(startPage)==-1:  
            self.selenium.open(startPage)                  
        if self.selenium.is_element_present(loginLink)==1:
            self.login(userName, userPassword)
                         
#    def setUp(self):
#        self.selenium = selenium("localhost", 4444, "*firefox", startPage)
#        self.selenium.start()
        
#    def tearDown(self):
#        self.selenium.stop()

    def login(self,userName,userPassword=None):
            output('Logging on as',userName)
            self.selenium.type(usernameField, userName)
            if userPassword!=None:
                self.selenium.type(passwordField, userPassword)
            self.selenium.click(loginLink)
            self.wait_for_xpath(homeLink)
            
    def logout(self):
        self.selenium.click(logoutLink)

# -------------------------------------------------------------------------------
# TESTING - KEPT FOR REFERENCE
# -------------------------------------------------------------------------------

    def justtesting(self):
    
        linksListFilename='allLhLinks.txt'
        lfl=open(linksListFilename,'r') 
        lines=lfl.readlines()
        lfl.close()
        for line in lines:
            
            if line.find('#')==0 and line.find('STOP')!=-1:
                pause('STOP found in file - bye')
                sys.exit()
                
            for errorType in knownErrorTypes.values():
                #print 'checking for errorType',errorType
                if line.find(errorType)!=-1:
                    print 'expecting error',errorType
                    linkText=line.strip()[:len(line)-len(errorType)-2]
                    linkLocator='//a[text()="'+linkText+'"]'
                    self.checkLink(linkLocator,linkText,errorType)                
        pause()
        
        
    def justSaved(self): 
        if True:
            if line.strip()=='':
                pass
            elif line.find('#')==0:
                pass
            elif line.find('#STOP')==0:
                pause('STOP found in file - bye')
                sys.exit()
            elif line.find('EXCEPTION')==0:
                print 'Expecting an Exception message'
                linkText=linkText[len('EXCEPTION'):]
                linkLocator='//a[text()="'+linkText+'"]'
                self.checkLink(linkLocator,linkText,'EXCEPTION')
            elif line.find('ERROR')==0:
                print 'Expecting an Error message'
                linkText=linkText[len('ERROR'):]
                linkLocator='//a[text()="'+linkText+'"]'
                self.checkLink(linkLocator,linkText,'ERROR')
            else:
                linkText=line.strip()    
                linkLocator='//a[text()="'+linkText+'"]'
                self.checkLink(linkLocator,linkText)
        pause("any key")
        sys.exit()
    
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'
                linkText=self.selenium.get_text(linkLocator)
                print 'linkText',linkText
                print self.selenium.get_attribute('//a[@id="'+linkId+'"]/@id')
                
                #is_text_present

    def printMissingLinks(self):        
        print 'Getting missing links'
        knownLinks=getKnownLinks()
        
        linkIds=self.selenium.get_all_links()
        
        for linkText in knownLinks:
            print 'checking linkText',linkText
            linkLocator='//a[text()="'+linkText+'"]'
                        
            print 'count for ',linkText,self.selenium.get_xpath_count(linkLocator)
            #linkId=self.selenium.get_STUFF(linkLocator+/@id)                      
        pause()
        sys.exit()

    def printAllLinks(self):        
        print 'Getting all links'
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:
            #print 'linkId',linkId
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'
                linkText=self.selenium.get_text(linkLocator)
                print 'linkText',linkText
        pause()
        sys.exit()
                
    def clickTestLink(self):
        self.selenium.allow_native_xpath('false')
        self.selenium.click('//a[text()="Profiles"]')
        self.selenium.click('//a[text()="My Actions"]')
        self.selenium.click('//a[text()="Profiles"]')
        pause()
        self.selenium.open(startPage)                                             
        sys.exit()
        
    def printHowManyLinks(self):                
        self.selenium.allow_native_xpath('true')
        linkcount=self.selenium.get_xpath_count('//table[1]//a')
        print 'linkcount //table[1]//a',linkcount
        linkcount=self.selenium.get_xpath_count('//a')
        print 'linkcount //a',linkcount
        linkcount=self.selenium.get_all_links()
        print 'linkcount get_all_links',len(linkcount)
        pause()
        
    def getAllLinks(self):
        self.selenium.allow_native_xpath('true')
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:        
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'            
                linkText=self.selenium.get_text(linkLocator)
                print 'linkText',linkText
        pause()
        sys.exit()
                                
# -------------------------------------------------------------------------------
#  Completed test running code 
# -------------------------------------------------------------------------------
    def runTest(self):
    
        if interestingLinks!=[]:
            for i in interestingLinks: output(i)
            pause('Only visiting specified links - any key to continue... ')

        started=True
        if startFromLink!=None:
            pause('Starting part way through from:',startFromLink,' ... any key ...')            
            started=False
        
        linkIds=self.selenium.get_all_links()
        for linkId in linkIds:
            if linkId!='':
                linkLocator='//a[@id="'+linkId+'"]'
                linkText=self.selenium.get_text(linkLocator)
                
                if started==False and linkText!=startFromLink:
                    pass
                elif started==False and linkText==startFromLink:
                    started=True                
                elif linkText in forbiddenLinks:
                    output('Forbidden link',linkText)
                elif linkText.lower().find("reset")!=-1: 
                    output('Ignoring:',linkText)
                elif linkText=="":
                    pause('ERROR: No link text'+linkId)                    
                elif interestingLinks==[]:
                    self.checkLink(linkLocator,linkText)
                elif linkText in interestingLinks:
                    self.checkLink(linkLocator,linkText)
                else:
                    # Ignore links if not in interesting links
                    pass  
                                                           
        pause('Finished - any key to continue...')
        
    def checkLink(self,linkLocator,linkText,expectedError=None):
        output('Checking link',linkText,'expected error',expectedError)
        self.wait_then_click(linkLocator)                    
        sleep(delayBetweenLinks)
        if expectedError!=None:
            self.checkForExceptions(expectedError)            
        self.selenium.open(startPage)                                             
        
# -------------------------------------------------------------------------------
#   Page handling funtions
# -------------------------------------------------------------------------------

    def wait_for_xpath(self,xpath):
        n=0
        while not self.selenium.is_element_present(xpath) and n<100:
            sleep(0.1)
            n+=1
            
    def wait_then_click(self,xpath):
        self.wait_for_xpath(xpath)
        self.selenium.click(xpath)
        
    def click_if_there(self,linkLocator):
        while self.selenium.is_element_present(linkLocator):
            print 'click_if_there',linkLocator
            self.selenium.click(linkLocator)
        
    def tryToClick(self,xpath):
        global pageWaitTime
        oldPageWait=pageWaitTime
        pageWaitTime="1000"
        try:
            self.wait_then_click(xpath)
        except:
            output('Not found',xpath)
        pageWaitTime=oldPageWait
    
    def openLinkLocator(self,linkLocator):
        self.wait_then_click(linkLocator)
        linkText=self.selenium.get_text(linkLocator)
        print 'Clicking link:',linkText
        print 'Link locator: ',linkLocator
        sleep(2)
       
    def checkForExceptions(self,expectedError=None):
    
        print 'checkForExceptions expectedError:',expectedError
        # Locations to search for messages
        xpaths=[
            '//body/font[2]',                                           # for the exception panel
            '//body/p',                                                 # for sorry you do not have permission text
#           '//body//tr',                                               # for Error 1012: No Query has been specificed - does not work ... xhtml again !!
            '//a[contains(text(),"devHelp2@foundation-network.com")]',  # for the error panel
            ]
            
        txt=""
        actualError=None
        for xpath in xpaths:
            if self.selenium.is_element_present(xpath):
                txt=self.selenium.get_text(xpath)

                for pageErrorText in knownErrorTypes.keys():
                    if txt.find(pageErrorText)!=-1:                
                        actualError=knownErrorTypes[pageErrorText]
                    
        if actualError==expectedError:
            output('Found expected error:',actualError)
        else:
            pause("Unexpected error panel found:",actualError)
    
    # TESTING Used as a test function to detect new messages
    def checkForMissingTemplates(self):
    
        linkLocator='//a[contains(text(),"Export Data")]'
        self.wait_then_click(linkLocator)                    
        
        # Locations to search for messages
        xpaths=[
            '//a[contains(text(),"devHelp2@foundation-network.com")]',    # for the error panel
            ]
            
        txt=""
        for xpath in xpaths:
            if self.selenium.is_element_present(xpath):
                txt=self.selenium.get_text(xpath)
                pause('FOUND',txt)
                if txt.find("devHelp2@foundation-network.com")!=-1:
                    pause("Error panel found:",txt)
                        
# -------------------------------------------------------------------------------    
if __name__ == "__main__":    
    try:
        FNTest("name")        
    except Exception, e:
        pause("Outer exception:",e,'Any key to continue...')
# -------------------------------------------------------------------------------    
