/*
This file tests the basic operation of getElementTree
In particular in respect to setting rights on an element

*/

declare @tempElements as table (textid sysname, parenttextid sysname null, id int)
insert into @tempElements (textid, parentTextID)
select * from
(values ('Testing_parent', null)
		,('Testing_1', 'Testing_parent')
		,('Testing_2', 'Testing_parent')
		,('Testing_1_1', 'Testing_1')
		,('Testing_1_2', 'Testing_1')) as x (textid, parenttextid)

declare @numberOfElements 		int
select @numberOfElements = count(1) from @tempElements

declare @rowcount int = -1

WHILE @rowcount <> 0
BEGIN
	insert into element
	(elementtextid,headline, parentid, sortorder, statusid)

	select t.textid, t.textid, case when parentTextID is null then 0 else p.id end, 1, 4
	from 
	@tempElements t
	left join element e on t.textid = e.elementTextID
	left join element p on t.parenttextid = p.elementTextID
	where e.id is null and case when parentTextID is null then 0 else p.id end is not null
	
	select @rowcount = @@rowcount
END

/* get the ids of the elements into the temp table */
update
	@tempElements
set 
	id = e.id
from 		
	@tempElements te
		inner join
	element e on e.elementtextid = te.textid	


/* create some usergroups */
declare @tempUserGroups as table (name sysname, id int)
insert into @tempUserGroups (name)
select * from
(
values    ('Test User A')
		, ('Test User B')
		, ('Test User C')
)		
	as x (name)

insert into usergroup (name, usergroupid)
select 
	tug.name, (select  max(usergroupid) from usergroup) + row_number ()  over (order by tug.name)
from 
	@tempUserGroups tug 
		left join
	usergroup ug on ug.name= tug.name	
where 
	ug.usergroupid is null




declare 
	  @Testing_parent int
	, @Testing_1 int
	, @Testing_1_1 int
	, @userA int
	, @userB int
	, @userC int

select  
	  @Testing_parent =  (select id from element where elementtextID = 'Testing_parent')
	, @Testing_1 =  (select id from element where elementtextID = 'Testing_1')
	, @Testing_1_1 =  (select id from element where elementtextID = 'Testing_1_1')
	, @userA = (select convert(varchar, usergroupid) from usergroup where name = 'Test User A')
	, @userB = (select usergroupid from usergroup where name = 'Test User B')
	, @userC = (select usergroupid from usergroup where name = 'Test User C')
	

/* delete all existing rights from these records */
delete
	recordrights
from 
	recordrights rr
		inner join 
	@tempElements e on rr.recordid = e.id and rr.entity = 'element'


delete
	countryscope
from 
	countryscope cs
		inner join 
	@tempElements e on cs.entityid = e.id and cs.entity = 'element'

declare 
	@countryid varchar = '9'
	,@userGroupList varchar(100) = @userA
	
declare @table as table (x int)

print 'test tree with no rights on it'
insert into @table
exec getElementTree @elementid = @Testing_parent,  @userGroupList = '1', @returnViewRightsLists = 1, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements 
BEGIN
	RAISERROR (N'Failed',16,16 )
END


print 'Set One row to require usergroup A'
exec setRecordRights @entity = 'element', @recordid = @Testing_1_1, @level = 1, @userGroupList = @userA, @groupAnd = 0, @groupNot =0, @updatedByPerson = 1 


print 'Get Tree with for no usergroups, should lose one row'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = '-999',  @returnViewRightsLists = 1, @useTempRightsTable = 1, @personid = -200, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements -1
BEGIN
	RAISERROR (N'Failed',16,16 )
END


print 'Get Tree for usergroup A, should get full tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @personid = -200, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements 
BEGIN
	RAISERROR (N'Failed',16,16 )
END


Print 'No country scoping so getting tree for country 9 should bring back whole tree '
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 9, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements 
BEGIN
	RAISERROR (N'Failed',16,16 )
END

print 'Set countryscope of one element to 9'
exec setCountryScope @entity = 'element', @entityid = @Testing_1_1, @level = 1, @countryiDList = '9'

print 'Get Tree for countryid 9 should bring back whole tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 9, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements 
BEGIN
	RAISERROR (N'Failed',16,16 )
END

print 'Get Tree for countryid 2 should bring back reduced tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 2, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements - 1
BEGIN
	RAISERROR (N'Failed',16,16 )
END


print 'Set countryscope of one element to 35 (benelux)'
exec setCountryScope @entity = 'element', @entityid = @Testing_1_1, @level = 1, @countryiDList = '35'
print 'But set Level11 countryscope (ie exclude) to 12 (luxemourg)'
exec setCountryScope @entity = 'element', @entityid = @Testing_1_1, @level = 11, @countryiDList = '12'


print 'Get Tree for countryid 1 (belgium) should bring back whole tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 1, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements 
BEGIN
	RAISERROR (N'Failed',16,16 )
END

print 'Get Tree for countryid 12 (luxembourg) should bring back reduced tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 12, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements - 1
BEGIN
	RAISERROR (N'Failed',16,16 )
END


print 'Get Tree for countryid 2 should also bring back reduced tree'
insert into @table
exec getElementTree @elementid = @Testing_parent, @userGroupList = @userA,  @returnViewRightsLists = 1, @useTempRightsTable = 1, @countryid = 2, @justReturnNodes = 1
IF @@rowcount <> @numberOfElements - 1
BEGIN
	RAISERROR (N'Failed',16,16 )
END

