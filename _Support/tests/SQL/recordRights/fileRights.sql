/*
This file tests the basic operation of getFile
In particular in respect to setting rights on files, fileTypes and fileTypeGroups
*/


declare 
	  @entity sysname = 'files'
	, @entityID int 
	, @fileID int 
	, @fileTypeID int 
	, @fileTypeGroupID int 
	, @personID int = 1
	, @usergroupA varchar(10) 
	, @usergroupB varchar(10) 
	, @usergroupC varchar(10) 
	, @test nvarchar(max)
	, @usergroupAandB varchar(10)
	, @usergroupBandC varchar(10)
	, @usergroupAandBandC varchar(10)

select @userGroupA = convert(varchar,min(usergroupID)) from usergroup
select @userGroupB = convert(varchar,min(usergroupID)) from usergroup where userGroupID <> convert(int, @userGroupA)
select @userGroupC = convert(varchar,min(usergroupID)) from usergroup where userGroupID <> convert(int, @userGroupA) and userGroupID <> convert(int, @userGroupB)

select 
		 @usergroupAandB  = @usergroupA + ',' + @usergroupB
		,@usergroupBandC  = @usergroupB + ',' + @usergroupC
		,@usergroupAandBandC  = @usergroupAandB + ',' + @usergroupC



Begin tran

	if not exists (select 1 from files where filename = 'FileForTestingrights')
	BEGIN
		insert into files (filename, fileTypeID)
		values ('FileForTestingrights',(select min (fileTypeID) from fileType))
	END
	select @fileID = fileID, @fileTypeID = fileTypeID  from files where filename = 'FileForTestingrights'	
	delete from recordrights where recordid = @fileID and entity= 'files'



	
	set @test = 'setRecordRights: Set Permission Level 1 on file, for usergroup A, person not in userGroupA '
	print @test
	exec setRecordRights @entity = @entity, @recordid = @fileID, @level = 1, @userGroupList = @usergroupA, @groupAnd = 0, @groupNot =0 , @updatedByPerson = 1
	delete from rightsgroup where usergroupid = @usergroupA and personid = @personid
	
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 0 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 1 on file, for usergroup A, add person to userGroupA '
	print @test
	insert into rightsGroup (usergroupid, personid) values (@usergroupA , @personid)

	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 1 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 1 on file, for usergroup A AND B, person not in userGroupB '
	print @test
	exec setRecordRights @entity = @entity, @recordid = @fileID, @level = 1, @userGroupList = @usergroupAandB, @groupAnd = 1, @groupNot =0 , @updatedByPerson = 1
	delete from rightsgroup where usergroupid = @usergroupB and personid = @personid
	
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 0 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 1 on file, for usergroup A AND B, add person to userGroupB '
	print @test
	insert into rightsGroup (usergroupid, personid) values (@usergroupB , @personid)
	
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 1 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 11 on file, for NOT usergroup C, person not in userGroupC - should be visible'
	print @test
	exec setRecordRights @entity = @entity, @recordid = @fileID, @level = 11, @userGroupList = @usergroupC, @groupAnd = 0, @groupNot =1 , @updatedByPerson = 1
	delete from rightsgroup where usergroupid = @usergroupC and personid = @personid
	
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 1 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 1 on file, for usergroup A AND B, add person to userGroupC - should be not visible'
	print @test
	insert into rightsGroup (usergroupid, personid) values (@usergroupC , @personid)
	
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 0 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	print 'reset rights'
	delete from recordrights where recordid = @fileID and entity= 'files'
	delete from recordrights where recordid = @fileTypeID and entity= 'fileType'
	delete from rightsgroup where personid = @personid and (usergroupid = @userGroupA OR usergroupid = @userGroupB OR usergroupid = @userGroupC)
	
	set @test = 'setRecordRights: Set Permission Level 1 on file for usergroup A, Permission Level 1 on fileType for usergroup B. Person just in usergroupA'
	print @test
	insert into rightsGroup (usergroupid, personid) values (@usergroupA , @personid)
	exec setRecordRights @entity = @entity, @recordid = @fileID, @level = 1, @userGroupList = @usergroupA, @groupAnd = 0, @groupNot = 0 , @updatedByPerson = 1
	exec setRecordRights @entity = 'fileType', @recordid = @fileTypeID, @level = 1, @userGroupList = @usergroupB, @groupAnd = 0, @groupNot = 0 , @updatedByPerson = 1

	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 0 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END


	set @test = 'setRecordRights: Set Permission Level 1 on file for usergroup A, Permission Level 1 on fileType for usergroup B. Person in usergroupA and usergroupB'
	print @test
	insert into rightsGroup (usergroupid, personid) values (@usergroupB , @personid)
	if not exists (select * from dbo.getFiles(@personid,0,0)	where fileID = @fileid and hasViewRights = 1 )
	BEGIN
			RAISERROR (N'Failed',16,16 )
			select @test, * from dbo.getFiles(@personid,0,0)	where fileID = @fileid

	END



rollback

