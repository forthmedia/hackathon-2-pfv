/*
This file tests the operation of
	setRecordRights
	getRecordRightsByUserGroup

In particular it tests retrieving  Level1 rights for Elements, where the rights are derived from Levels 1 and 11

*/

declare 
	@entity sysname = 'element'
	, @entityID int = 1000    -- a random element, does not need to exist
	, @usergroupA varchar(10) 
	, @usergroupB varchar(10) 
	, @usergroupC varchar(10) 
	, @test nvarchar(max)
	, @usergroupAandB varchar(10)
	, @usergroupBandC varchar(10)
	, @usergroupAandBandC varchar(10)

select @userGroupA = convert(varchar,min(usergroupID)) from usergroup
select @userGroupB = convert(varchar,min(usergroupID)) from usergroup where userGroupID <> convert(int, @userGroupA)
select @userGroupC = convert(varchar,min(usergroupID)) from usergroup where userGroupID <> convert(int, @userGroupA) and userGroupID <> convert(int, @userGroupB)

select 
		 @usergroupAandB  = @usergroupA + ',' + @usergroupB
		,@usergroupBandC  = @usergroupB + ',' + @usergroupC
		,@usergroupAandBandC  = @usergroupAandB + ',' + @usergroupC



begin tran
set @test = 'setRecordRights: Set Permission Level 1 on an element, for usergroup A '
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 1, @userGroupList = @usergroupA, @groupAnd = 0, @groupNot =0 , @updatedByPerson = 1
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupA and permission = 1)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, * from recordrights where recordid = @entityID and entity = @entity  

END

set @test = 'getRecordRights for userGroupA should show Level 1'
print @test
if not exists (select 1 
				from getRecordRightsTableByUserGroup (10,@usergroupA)
				where recordid = @entityID and level1 = 1  
				)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test,  * 
				from getRecordRightsTableByUserGroup (10,@usergroupA)
				where recordid = @entityID 

END


set @test = 'Set same but with a NOT '
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 1, @userGroupList = null , @groupAnd = 0, @groupNot = 1, @updatedByPerson = 1 

set @test = 'getRecordRights for userGroupA should show Level1 = 0'
print @test
if not exists (select 1 
				from getRecordRightsTableByUserGroup (10,@usergroupA)
				where recordid = @entityID and level1 = 0  
				)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, * 
				from getRecordRightsTableByUserGroup (10,@usergroupA)
				where recordid = @entityID 
END


set @test = 'Set Permission Level 11 on same element, for usergroups A and B (with AND)'
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 11, @userGroupList = @userGroupAandB, @groupAnd = 1, @groupNot = 0, @updatedByPerson = 1 


set @test = 'Usergroup A should now have levels 1 and 11'
print @test
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupA and permission = 1 + 1024)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, * from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupA 
END


set @test = 'Usergroup B should now have level 11'
print @test
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupB and permission = 1024)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, * from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupB
END



set @test = 'getRecordRights for userGroupA should show Level11 = 0 (because of AND with usergroupB)'
print @test
if not exists (select 1 
				from getRecordRightsTableByUserGroup (10,@userGroupA)
				where recordid = @entityID and level11 = 0 
				)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, * 
		from getRecordRightsTableByUserGroup (10,@userGroupA)
				where recordid = @entityID
END


set @test = 'getRecordRights for userGroupB should show Level11 = 0 (because of AND with usergroupA)'
print @test
if not exists (select 1 
				from getRecordRightsTableByUserGroup (10,@userGroupB)
				where recordid = @entityID and level11 = 0 
				)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
				from getRecordRightsTableByUserGroup (10,@userGroupB)
				where recordid = @entityID  

END



set @test = 'getRecordRights for userGroupA and userGroupB should now  show Level11 = 1 '
print @test
if not exists (select 1 
				from getRecordRightsTableByUserGroup (10,@userGroupAandB)
				where recordid = @entityID and level11 = 1 
				)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
				from getRecordRightsTableByUserGroup (10,@userGroupAandB)
				where recordid = @entityID  

END


set @test = 'Set Permission Level 1 on same element, for usergroups B and C'
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 1, @userGroupList = @userGroupBandC, @groupAnd = 0, @groupNot =0, @updatedByPerson = 1 


set @test = 'Usergroup A should now have lost Level 1 and be left with 11'
print @test
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupA and permission = 1024)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupA
END


set @test = 'Usergroup B should now have Level 1 and 11'
print @test
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupB and permission = 1 + 1024 )
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupB
select * from recordrights where recordid = @entityID and entity = @entity 
END



set @test = 'Usergroup C should now have Level 1 '
print @test
if not exists (select 1 from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupC and permission =  1)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from recordrights where recordid = @entityID and entity = @entity and usergroupid = @usergroupC
END



set @test = 'Set Permission Level 1 on same element, for and C'
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 1, @userGroupList = @usergroupC, @groupAnd = 0, @groupNot =0, @updatedByPerson = 1 

set @test = 'Check Level1 rights, should only be 1 when usergroups A,B and C'
print @test
set @test = 'Try A'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID and level1 = 0)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID
END

set @test = 'Try A,B'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,@userGroupAandB) where recordid = @entityID and level1 = 0)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID
END

set @test = 'Try A,B,C'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,@userGroupAandBandC) where recordid = @entityID and level1 = 1)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID
END



set @test = 'Check Level1 rights, with just a NOT UserGroupA at level 11'
print @test
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 1, @userGroupList = '', @groupAnd = 0, @groupNot =0, @updatedByPerson = 1 
exec setRecordRights @entity = @entity, @recordid = @entityID, @level = 11, @userGroupList = @userGroupA, @groupAnd = 0, @groupNot =1, @updatedByPerson = 1 

set @test = 'UserGroup A should give no rights'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID and level1 = 0)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,@userGroupA) where recordid = @entityID
END


set @test = 'UserGroup B should give rights'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,@userGroupB) where recordid = @entityID and level1 = 1)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,@userGroupB) where recordid = @entityID
END


set @test = 'No usergroups at all should give rights'
print @test
if not exists (select * from getRecordRightsTableByUserGroup (10,'') where recordid = @entityID and level1 = 1)
BEGIN
		RAISERROR (N'Failed',16,16 )
		select @test, *
		from getRecordRightsTableByUserGroup (10,'') where recordid = @entityID
END


rollback


