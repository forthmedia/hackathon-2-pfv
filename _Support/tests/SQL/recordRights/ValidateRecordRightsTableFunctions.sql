/*
WAB 2016-06-22

Validate that all the different recordRights function bring back the same answer

*/

declare   @personIDList nvarchar(max) 
		,  @elementIDList nvarchar(max) 
		, @personid varchar
		, @sql nvarchar(max)
		, @sqlbase nvarchar(max)
		, @userGroupIDList nvarchar(max)
		, @count int

select @personIDList  = dbo.Concatenate(CONVERT(varchar,personid) + ',') from person where PersonID in (Select top 20 PersonID from person order by personid)

select @elementIDList  = dbo.Concatenate(CONVERT(varchar,recordid) + ',') from 
	(select 
			recordid ,
			level1,
			RANK () OVER (partition by level1 order by recordid) as _rank
	from 
			dbo.getRecordRightsTableByPerson(10, 1) as y
	) as x
	where _rank < 10		
			
declare myCursor CURSOR FOR
	select convert(varchar,value) as personid
	from
		dbo.csvToTable (@personIDList) 

OPEN myCursor
		FETCH NEXT FROM myCursor into @personid
		while @@FETCH_STATUS = 0
		begin

			select @userGroupIDList = dbo.concatenate (convert(varchar,usergroupID) + ',') from RightsGroup where PersonID = convert(int,@personid)

			set @sqlbase = '
				from 
					dbo.getRecordRightsTableByPeople(10,''' + @personIDList + ''',''' + @elementIDList + '''  ) as allPeople
						left join 
					dbo.getRecordRightsTableByPerson(10,' + @personid + ') onePerson on allPeople.personid = ' + @personid + ' and allPeople.recordid = onePerson.recordid
						left join
					dbo.getRecordRightsTableByUserGroup(10,''' + @userGroupIDList + ''') onePersonByUserGroup on allPeople.personid = ' + @personid + ' and allPeople.recordid = onePersonByUserGroup.recordid
				where 					
					 -- 1=1 OR -- remove previous comment to see what is actually going on 
					allPeople.personid = ' + @personid + '
					AND
						(
						allPeople.level1 <> onePerson.level1
						OR 
						allPeople.level2 <> onePerson.level2
						OR
						allPeople.level1 <> onePersonByUserGroup.level1
						OR 
						allPeople.level2 <> onePersonByUserGroup.level2
						OR 
						allPeople.level1 <> dbo.checkRecordRights (10, convert(int,allPeople.personid),allPeople.recordID,1) 

						)
				'
				set @sql = 'select @count = count(1) ' + @sqlBase
				
				exec sp_executeSQL @sql, N'@count int OUTPUT', @count = @count OUTPUT
				
				IF @count <> 0 
				BEGIN
					RAISERROR ('FAILED',16,16)

					set @sql = '
							select 
							  allPeople.personid
							, allPeople.recordid
							, allPeople.level1
							, onePerson.level1
							, onePersonByUserGroup.level1
							, allPeople.level2
							, onePerson.level2
							, onePersonByUserGroup.level2
							, dbo.checkRecordRights (10, allPeople.personid,allPeople.recordID,1) as Level1_Individual ' 
						+ @sqlBase
					
					exec (@sql)
				END
				ELSE
				BEGIN
					print 'OK'
				END
				
			FETCH NEXT FROM myCursor into @personid
		end
	
	CLOSE myCursor
	DEALLOCATE myCursor


