/*
WAB 2015-10-14 a script to test setBooleanFlag stored procedure
Tests 3 modes  
*/

declare @checkboxflagid1 int, @checkboxFlagTextID1 sysname, @personid1 int
declare @checkboxflagid2 int, @checkboxFlagTextID2 sysname, @personid2 int

select top 1 @checkboxflagid1 = flagid, @checkboxFlagTextID1 = flagTextID from vflagdef where dataType = 'checkbox' and entityTable = 'person' and isnull(flagTextID,'')  <> '' 
select top 1 @checkboxflagid2 = flagid, @checkboxFlagTextID2 = flagTextID from vflagdef where dataType = 'checkbox' and entityTable = 'person' and isnull(flagTextID,'')  <> '' and flaggroupid not in (select flagGroupID from flag where flagid = @checkboxflagID1)

select top 1 @personid1 = personid from person 
	left join booleanflagdata bfd1 on bfd1.entityid = personid and bfd1.flagid = @checkboxflagid1 
	left join booleanflagdata bfd2 on bfd2.entityid = personid and bfd2.flagid = @checkboxflagid2 
where bfd1.flagid is null and bfd2.flagid is null

select top 1 @personid2 = personid from person 
	left join booleanflagdata bfd1 on bfd1.entityid = personid and bfd1.flagid = @checkboxflagid1 
	left join booleanflagdata bfd2 on bfd2.entityid = personid and bfd2.flagid = @checkboxflagid2 
where bfd1.flagid is null and bfd2.flagid is null and personid <> @personid1


/* set and delete single item  by flagid */
print 'Single Row by entityID, flagid'
print 'Insert 1 row'
exec setBooleanFlag @flagid = @checkboxflagid1, @entityid = @personid1, @userid = 1, @updatedByPersonID = @personid1
IF not exists (select 1 from booleanFlagData where entityID = @personid1 and flagID = @checkboxflagid1)
	RAISERROR ('Set failed',16,16)

print 'Delete 1 row'
exec deleteBooleanFlag @flagid = @checkboxflagid1, @entityid = @personid1, @userid = 1, @updatedByPersonID = @personid1
IF exists (select 1 from booleanFlagData where entityID = @personid1 and flagID = @checkboxflagid1)
	RAISERROR ('Delete Failed',16,16)

/* set and delete single item  by flagtextid */
print 'Single Row by entityID, flagTextid'
print 'Insert 1 row'
exec setBooleanFlag @flagid = @checkboxflagtextid1, @entityid = @personid1, @userid = 1, @updatedByPersonID = @personid1
IF not exists (select 1 from booleanFlagData where entityID = @personid1 and flagID = @checkboxflagid1)
	RAISERROR ('Set Failed',16,16)

print 'Insert Again 0 rows'
exec setBooleanFlag @flagid = @checkboxflagtextid1, @entityid = @personid1, @userid = 1, @updatedByPersonID = @personid1
IF not exists (select 1 from booleanFlagData where entityID = @personid1 and flagID = @checkboxflagid1)
	RAISERROR ('Set Failed',16,16)

print 'Delete 1 row'
exec deleteBooleanFlag @flagid = @checkboxflagtextid1, @entityid = @personid1, @userid = 1, @updatedByPersonID = @personid1
IF exists (select 1 from booleanFlagData where entityID = @personid1 and flagID = @checkboxflagid1)
	RAISERROR ('Delete Failed',16,16)



print 'Using table with entityID column'
/* set by entity table */
if object_id ('tempdb..#setFlag_EntityTable') is not null
	drop table #setFlag_EntityTable

select entityid into 
#setFlag_EntityTable
from
(values (@personid1), (@personid2)) as x (entityid)


print 'FlagID'
print 'Insert 2 rows'
/* Numeric FlagID*/
exec setBooleanFlag @flagid = @checkboxFlagID1, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityTable'
IF exists (select 1 from #setFlag_EntityTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and flagID = @checkboxflagid1 where bfd.flagid is null)
	RAISERROR ('Set Failed',16,16)

print 'Delete 2 rows'
exec deleteBooleanFlag @flagid = @checkboxFlagID1, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityTable'
IF exists (select 1 from #setFlag_EntityTable ef inner join booleanFlagData bfd on bfd.entityID = ef.entityid and flagID = @checkboxflagid1)
	RAISERROR ('Delete Failed',16,16)

/* Text FlagID*/
print 'FlagTextID'
print 'Insert 2 rows'
exec setBooleanFlag @flagid = @checkboxFlagTextID1, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityTable'
IF exists (select 1 from #setFlag_EntityTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and flagID = @checkboxflagid1 where bfd.flagid is null)
	RAISERROR ('Set Failed',16,16)

print 'Delete 2 rows'
exec deleteBooleanFlag @flagid = @checkboxFlagTextID1, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityTable'
IF exists (select 1 from #setFlag_EntityTable ef inner join booleanFlagData bfd on bfd.entityID = ef.entityid and flagID = @checkboxflagid1)
	RAISERROR ('Delete Failed',16,16)


drop table #setFlag_EntityTable



print 'Using table with entityID and FlagID (numeric) column'
/* set by entity and flag table (numeric flagid) */
if object_id ('tempdb..#setFlag_EntityAndFlagTable') is not null
	drop table #setFlag_EntityAndFlagTable

select entityid, flagid into 
#setFlag_EntityAndFlagTable
from
(values (@personid1), (@personid2)) as e (entityid)
inner join 
(values (@checkboxflagid1), (@checkboxflagid2)) as f (flagid) on 1=1

print 'Insert 4 rows'
exec setBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityAndFlagTable'
IF exists (select 1 from #setFlag_EntityAndFlagTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = ef.flagid where bfd.flagid is null)
	RAISERROR ('Set Failed',16,16)

print 'Insert Again 0 rows'
exec setBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityAndFlagTable'
IF exists (select 1 from #setFlag_EntityAndFlagTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = ef.flagid where bfd.flagid is null)
	RAISERROR ('Set Failed',16,16)

print 'Delete 4 rows'
exec deleteBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityAndFlagTable'
IF exists (select 1 from #setFlag_EntityAndFlagTable ef inner join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = ef.flagid)
	RAISERROR ('Delete Failed',16,16)

drop table #setFlag_EntityAndFlagTable



print 'Using table with entityID and FlagID (text) column'
/* set by entity and flag table (text flagid) */
if object_id ('tempdb..#setFlag_EntityAndFlagTextTable') is not null
	drop table #setFlag_EntityAndFlagTextTable

select entityid, flagid into 
#setFlag_EntityAndFlagTextTable
from
(values (@personid1), (@personid2)) as e (entityid)
inner join 
(values (@checkboxflagTextid1), (@checkboxflagTextid2)) as f (flagid) on 1=1

print 'insert 4 rows'
exec setBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityAndFlagTextTable'
IF exists (select 1 from #setFlag_EntityAndFlagTextTable ef inner join flag f on f.flagTextID = ef.flagid left join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = f.flagid where bfd.flagid is null)
	RAISERROR ('Set Failed',16,16)

print 'delete 4 rows'
exec deleteBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityAndFlagTextTable'
IF exists (select 1 from #setFlag_EntityAndFlagTextTable ef inner join flag f on f.flagTextID = ef.flagid inner join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = f.flagid)
	RAISERROR ('Delete Failed',16,16)

drop table #setFlag_EntityAndFlagTextTable





/* set by entity, flag and data table (numeric flagid) */
if object_id ('tempdb..#setFlag_EntityFlagAndDataTable') is not null
	drop table #setFlag_EntityFlagAndDataTable

select entityid, flagid, 1 as data into 
#setFlag_EntityFlagAndDataTable
from
(values (@personid1), (@personid2)) as e (entityid)
inner join 
(values (@checkboxflagid1), (@checkboxflagid2)) as f (flagid) on 1=1

print 'Test with data column '
print 'Insert 4 rows'
exec setBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityFlagAndDataTable'
IF exists (select 1 from #setFlag_EntityFlagAndDataTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = ef.flagid where case when data = 0 and bfd.flagid is not null then 1 when data = 1 and bfd.flagid is null then 1 else 0 end = 1)
	RAISERROR ('Set Failed',16,16)

set noCount on
update #setFlag_EntityFlagAndDataTable set data = 0

print 'Delete 4 rows'
exec setBooleanFlag @flagid = null, @entityid = null, @userid = 1, @updatedByPersonID = @personid1, @tablename = '#setFlag_EntityFlagAndDataTable'
IF exists (select 1 from #setFlag_EntityFlagAndDataTable ef left join booleanFlagData bfd on bfd.entityID = ef.entityid and bfd.flagID = ef.flagid where case when data = 0 and bfd.flagid is not null then 1 when data = 1 and bfd.flagid is null then 1 else 0 end = 1)
	RAISERROR ('Set Failed',16,16)

drop table #setFlag_EntityFlagAndDataTable


