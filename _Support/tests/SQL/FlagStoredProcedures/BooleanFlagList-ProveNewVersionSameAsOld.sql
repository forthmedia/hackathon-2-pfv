/*
WAB 2016-06-20	PROD2016-1295 
				This is a one off piece of code to prove that the new version on function dbo.booleanFlagList is the same as the old one
				Somewhat over crafted, but was interested to see the performance improvement
				(Note, it contains the full text of the old version)

*/
createPlaceHolderObject 'function','BooleanFlagListOLD'
GO
	alter FUNCTION [dbo].[BooleanFlagListOLD] (@flagGroupID int,@EntityID int,@returnValue varchar(20)='name')  
	RETURNS nvarchar(max) AS  
	BEGIN 


	DECLARE @returnString nvarchar(max)
	DECLARE @flagName nvarchar(200)
	DECLARE @n int


	if (@returnValue = 'name')
	begin
		DECLARE n_cursor CURSOR FOR
		SELECT 
		CASE WHEN isNull(f.NamePhraseTextID,'') = '' 
			THEN f.Name 
			ELSE 'phr_'+f.NamePhraseTextID
		END as Name
		FROM booleanflagdata b 
			INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
		WHERE b.entityID = @EntityID
		ORDER BY f.orderingIndex
	end
	else if (@returnValue = 'flagName')
	begin
		DECLARE n_cursor CURSOR FOR
		SELECT f.Name as Name
			FROM booleanflagdata b 
			INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
			WHERE b.entityID = @EntityID
			ORDER BY f.orderingIndex
	end
	else
	begin
		DECLARE n_cursor CURSOR FOR
		 SELECT case when isNull(f.flagTextID,'') = '' then convert(varchar,f.flagID) else f.flagTextID end as flagTextID FROM booleanflagdata b 
			INNER JOIN flag f ON f.flagid = b.flagid AND f.flaggroupid=@flagGroupID
		WHERE b.entityID = @EntityID
		ORDER BY f.orderingIndex
	end

	SET @returnString = ''
	SET @n = 1

	OPEN n_cursor


	FETCH NEXT FROM n_cursor
	INTO @flagName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		 if (SELECT @n) = 1
			begin
				SET @returnString = @flagName
			end
		 else
			begin
   			SET @returnString = @returnString + ',' + @flagName
			end
		 SET @n = @n + 1
	   FETCH NEXT FROM n_cursor
			INTO @flagName
	END

	CLOSE n_cursor
	DEALLOCATE n_cursor

	Return @returnString

	END

GO

declare @flagGroupID int, @tablename sysname, @uniqueKey sysname, @sql nvarchar(max), @sqlFrom nvarchar(max)
declare @result int, @startTime datetime, @endtime datetime, @oldms int, @newms int, @improvement DECIMAL (8,1)


	DECLARE flagGroups CURSOR FOR
	 SELECT flagGroupID , tablename, uniqueKey
	 from flagGroup fg
		inner join FlagEntityType et on et.entitytypeid = fg.entityTypeID
	 where fg.FlagTypeID in (2,3) 
	


OPEN flagGroups
	FETCH NEXT FROM flagGroups INTO @flagGroupID, @tablename, @uniqueKey

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		set @sqlFrom = '
			from 
				' + @tablename + '
			where
					dbo.booleanFlagList (@flagGroupID,ENTITYID, ''flagTextID'') <> dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''flagTextID'') 
				OR	dbo.booleanFlagList (@flagGroupID,ENTITYID, ''name'') <> dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''name'') 
				OR	dbo.booleanFlagList (@flagGroupID,ENTITYID, ''flagName'') <> dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''flagName'') 			
			'
		set @sql = '
			select 
				@result = count(1) ' 
			+ @sqlFrom	


		select @sql = REPLACE (@sql,'ENTITYID',@uniqueKey)	
	
		print 'FlagGroup:' + convert(varchar, @flagGroupID) 
		exec sp_executeSQL @sql, N'@flagGroupID int, @result int OUTPUT',@flagGroupID = @flagGroupID,@result = @result OUTPUT

		IF @Result <> 0
		BEGIN
			RAISERROR ('Error - the queries should bring back zero records',16,16)
			
			set @sql = '
				select			
					dbo.booleanFlagList (@flagGroupID,ENTITYID, ''flagTextID''), dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''flagTextID'') 
					, dbo.booleanFlagList (@flagGroupID,ENTITYID, ''name''), dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''name'') 
					, dbo.booleanFlagList (@flagGroupID,ENTITYID, ''flagName''), dbo.booleanFlagListOLD (@flagGroupID,ENTITYID, ''flagName'') 			
				' + @sqlFrom	
	
			select @sql = REPLACE (@sql,'ENTITYID',@uniqueKey)
			print @sql
			exec sp_executeSQL @sql, N'@flagGroupID int', @flagGroupID = @flagGroupID

		END

		set @sql = '
			select 
				 @result = count(1)
			from 
				' + @tablename + '
			where
				dbo.booleanFlagListOLD (@flagGroupID,' + @uniqueKey + ', ''flagTextID'')	<> '''' '


			set @startTime = GETDATE()
			exec sp_executeSQL @sql, N'@flagGroupID int, @result int OUTPUT', @flagGroupID = @flagGroupID , @result = @result OUTPUT
			set @endTime = GETDATE()
			set @oldms = datediff(ms,@startTime,@endtime)

			set @sql = REPLACE (@sql,'OLD','')
			set @startTime = GETDATE()
			exec sp_executeSQL @sql, N'@flagGroupID int, @result int OUTPUT', @flagGroupID = @flagGroupID , @result = @result OUTPUT
			set @endTime = GETDATE()
			set @newms = datediff(ms,@startTime,@endtime)

			set @improvement = (convert(float,@oldms)/convert(float,@newms))
		
			print convert(varchar, @result) + ' Rows'
			print 'Old Time: ' +  convert(varchar, @oldms) + 'ms'
			print 'New Time: ' +  convert(varchar, @newms) + 'ms'
			print 'Performance Improvement: ' + convert(varchar, @improvement)  + 'x'

		FETCH NEXT FROM flagGroups INTO @flagGroupID, @tablename, @uniqueKey
	END

CLOSE flagGroups
DEALLOCATE flagGroups


GO

Drop function BooleanFlagListOLD

