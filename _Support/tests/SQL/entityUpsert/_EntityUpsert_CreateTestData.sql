/* see entityUpsert_TestHarness.sql for info */
 
IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = '_EntityUpsert_CreateTestData' and Type = 'P')
	DROP PROCEDURE _EntityUpsert_CreateTestData

IF object_id('tempdb..##_EntityUpsert_TestData') is not null
	drop table ##_EntityUpsert_TestData
GO

CREATE PROCEDURE _EntityUpsert_CreateTestData AS



	/*********************************************
	Creation of Dataload Table
	**********************************************/

	IF object_id('tempdb..##_EntityUpsert_TestData') is not null
		drop table ##_EntityUpsert_TestData

	create table ##_EntityUpsert_TestData (
						  rowid int identity(1,1)
						, recordUID VARCHAR (10)
						, batchUID VARCHAR (10)
						, entityUpsertTestID int
						, columnsToProcess  nvarchar(max)
						, expectedAction sysname
						, expectedFailField sysname
						, expectedResult sysname
						, comment nvarchar(max)
						, processNulls bit default 0
						, textNotNull nvarchar(100)
						, textNull nvarchar(40)
						, dateNotNullFromVarcharWithDefault nvarchar(100)
						, dateNotNullFromVarchar nvarchar(100)
						, datenull nvarchar(100)
						, datenull2 dateTime
						, number nvarchar(100)
						, aNumericFromVarChar nvarchar(20)
						, aDecimal DECIMAL (12,4) null
						, aDecimalFromVarchar VARCHAR(30) NULL
						, integerNotNullDefault varchar(max) 
						, anInteger INT NULL
						, anIntegerFromVarchar VARCHAR (40) NULL
						, foreignKey nVARCHAR(100)
						, aGUID uniqueIdentifier
						, aUniqueColumn nVARCHAR(10)
						, aUniqueColumnWithFilter nVARCHAR(10)
						, aUniqueColumnWithNonNullFilter nVARCHAR(10)
						, abooleanflagGroup nvarchar (100)
						, abooleanflagGroup2 nvarchar (100)
						, abooleanflag nvarchar (10)
						, abooleanflagbit bit
						, aRadioGroup nvarchar(100)
						, radio1 bit
						, radio2 bit
						, anIntegerFlag nvarchar(20)
						, anIntegerFlagFromInteger int
						, anIntegerFlagLinked nvarchar(10)
						, anIntegerMultipleFlag nvarchar(10)
						, anIntegerMultipleFlagLinked nvarchar(50)

						, aTextFlag nvarchar(10)
						, aTextFlag2 nvarchar(10)
						, aDateFlag nvarchar(20)
						, aDateFlag2 nvarchar(20)
						, adecimalFlag nvarchar(20)
						, exposedAsBit nvarchar(20)
						, exposedAsBitUsingBit bit

						)



		declare @aValidPersonID1 varchar(10), @aValidPersonID2 varchar(10), @aValidPersonID3 varchar(10), @validPersonIDList1 varchar (20), @validPersonIDList2 varchar (20), @validPersonIDList3 varchar (20)
		select @aValidPersonID1 = convert(varchar,min(personid)), @aValidPersonID2 = convert(varchar,max(personid))  from person

		select	@aValidPersonID3 = convert(varchar,min(personid))  from person 
		where	personid > convert(int,@aValidPersonID1)

		select	@validPersonIDList1  = @aValidPersonID1 + ',' + @aValidPersonID2, 
				@validPersonIDList2  = @aValidPersonID1 + ',' + @aValidPersonID3, 
				@validPersonIDList3  = @validPersonIDList1  + ',' + @aValidPersonID3
		

	/*********************************************
	Populate ##_EntityUpsert_TestData table
	**********************************************/

	insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess						,expectedAction,expectedFailField,			expectedResult,		comment,													textNotNull,		dateNotNullFromVarcharWithDefault, dateNotNullFromVarchar,	dateNull,	datenull2,	number, integerNotNullDefault,	foreignKey)
	values	
			/* Lots of inserting/updating not null fields - many fail*/ 
			(1,			1,				'textNotNull',							'FAIL',			'dateNotNullFromVarchar',	'missingRequiredField',	'dateNotNullFromVarchar required but not processed	',		'someText'	,		null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'textNotNull,dateNotNullFromVarchar',	'FAIL',			'dateNotNullFromVarchar',	'required',				'dateNotNullFromVarchar processed but is null	',			'someText'	,		null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'dateNotNullFromVarchar',				'FAIL',			'TextNotNull',				'missingRequiredField',	'TextNotNull required but not processed',					null,				null,					convert(varchar,getdate()),	null,		null,		null,	null,					null),
			(1,			1,				'textNotNull,dateNotNullFromVarchar',	'FAIL',			'TextNotNull',				'required',				'TextNotNull processed but is null',						null,				null,					convert(varchar,getdate()),	null,		null,		null,	null,					null),
			(1,			1,				'textNotNull,dateNotNullFromVarchar',	'FAIL',			'dateNotNullFromVarchar',	'required',				'dateNotNullFromVarchar required but set to blank === null ','sometext',		null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'textNotNull,dateNotNullFromVarchar' ,	'INSERT',		'',							'',						'A Good insert at last',									'someText',			null,					convert(varchar,getdate()),	null,		null,		null,	null,					null),
			(1,			1,				'textNotNull' ,							'UPDATE',		'',							'',						'TextNotNull required updated to blank (which is OK) ',		'',					null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'textNotNull' ,							'FAIL',			'TextNotNull',				'DataTooLong',			'Text Too Long',											REPLICATE('X',21),	null,					convert(varchar,getdate()),	null,		null,		null,	null,					null),
			(1,			1,				'textNotNull' ,							'FAIL',			'TextNotNull',				'required',				'TextNotNull required updated to Null',						null,				null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'textNotNull' ,							'UPDATE',		'',							'',						'TextNotNull Updated',										'change text',		null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'textNotNull' ,							'UNCHANGED',	'',							'',						'TextNotNull Unchanged',									'change text',		null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'dateNotNullFromVarchar' ,				'FAIL',			'dateNotNullFromVarchar',	'required',				'dateNotNullFromVarchar updated to Null',					null,				null,					null,						null,		null,		null,	null,					null),
			(1,			1,				'dateNotNullFromVarchar' ,				'UPDATE',		'',							'',						'dateNotNullFromVarchar updated',							null,				null,					convert(varchar,getdate()+1),null,		null,		null,	null,					null),
			(1,			1,				'dateNotNullFromVarchar' ,				'UNCHANGED',	'',							'',						'dateNotNullFromVarchar unchanged',							null,				null,					convert(varchar,getdate()+1),null,		null,		null,	null,					null),
			(1,			1,				'dateNotNullFromVarcharWithDefault' ,	'FAIL',			'dateNotNullFromVarcharWithDefault','required',		'Updating to Null ',										null,				'',						null,						null,		null,		null,	null,					null),

			/*  */ 
			(2,			1,				'ALL',									'INSERT',		'',							'',						'',															'someText',			null,					getdate(),					null,		null,		null,	null,					null),
			(2,			0,				'ALL',									'UNCHANGED',	'',							'',						'',															'someText',			null,					null,						null,		null,		null,	null,					null),
			(2,			0,				'ALL',									'FAIL',			'dateNull',					'NotValidDate',			'Not a Date',												null,				null,					null,						'2014/13/12',null,		null,	null,					null),
			(2,			0,				'ALL',									'FAIL',			'dateNull',					'NotValidDate',			'Not a Date',												null,				null,					null,						'blah'		,null,		null,	null,					null),
			(2,			0,				'ALL',									'FAIL',			'dateNotNullFromVarcharWithDefault','NotValidDate',	'Not a Date',												null,				'blah',					null,						null		,null,		null,	null,					null),
			(2,			0,				'ALL',									'UPDATE',		'dateNull',					'required',				'Not a Date',												null,				null,					'',							null,		null,		null,	null,					null),
			(2,			0,				'ALL',									'UPDATE',		'',							'',						'',															null,				null,					null,						null		,null,		'1.02',	null,					null),
			(2,			0,				'ALL',									'UPDATE',		'',							'',						'Update number',											null,				null,					null,						null		,null,		'2',	null,					null),
			(2,			0,				'ALL',									'UPDATE',		'',							'',						'Update number to null using blank - should work',			null,				null,					null,						null		,null,		'',		null,					null),


			(3,			1,				'textNotNull,dateNotNullFromVarchar' ,	'INSERT',		'',							'',						'A Good insert at last',									'someText',			null,					getdate(),					null,		null,		null,	null,					null),
			(3,			0,				'integerNotNullDefault',				'UPDATE',		'',							'',						'',															null,				null,					null,						null		,null,		null,	'123',					null),
			(3,			0,				'datenull',								'UPDATE',		'',							'Dec 13 2014 12:00AM',	'Update datenull  to a value',								null,				null,					null,						'2014/12/13',null,		null,	null,					null),
			(3,			0,				'datenull',								'UPDATE',		'',							'',						'Update datenull  to null using blank - should succeed',	null,				null,					null,						''			,null,		null,	null,					null),
			(3,			0,				'integerNotNullDefault',				'FAIL',			'integerNotNullDefault',	'NotNumeric',			'Update integerNotNullDefault not numeric - should fail',	null,				null,					null,						''			,null,		null,	'aa',					null),
			(3,			0,				'integerNotNullDefault',				'FAIL',			'integerNotNullDefault',	'NumberTooLarge',		'Update integerNotNullDefault too long - should fail',		null,				null,					null,						''			,null,		null,	'1234567890111111',					null),
			(3,			0,				'integerNotNullDefault',				'FAIL',			'integerNotNullDefault',	'required',				'Update integerNotNullDefault to null using blank - should fail',null,			null,					''							,null,		null,		null,	'',						null),


			(4,			0,				'ALL',							'INSERT',		'',					'',						'',														'some text',	null,					getdate(),		null		,getdate(),	null,	null,					null)		,


			(5,			0,				'ALL',							'FAIL',			'foreignKey',		'InvalidForeignKey',	'Foreign Key Error',									'some text',	null,					getdate(),		null		,getdate(),	null,	null,					'2')	,	
			(5,			0,				'ALL',							'FAIL',			'foreignKey',		'NotNumeric',			'Foreign Key or Not Numeric Error',						'some text',	null,					getdate(),		null		,getdate(),	null,	null,					'a')		,
			(5,			0,				'ALL',							'INSERT',		'',					'',						'',														'some text',	null,					getdate(),		null		,getdate(),	null,	null,					'1')		,
			(5,			0,				'ALL',							'FAIL',			'foreignKey',		'NotNumeric',			'Foreign Key or Not Numeric Error',						'some text',	null,					getdate(),		null		,getdate(),	null,	null,					'a')		,
			(5,			0,				'ALL',							'FAIL',			'foreignKey',		'InvalidForeignKey',	'Foreign Key Error',									'some text',	null,					getdate(),		null		,getdate(),	null,	null,					'2'),		


			(6,			1,				'ALL' ,							'INSERT',		'',					'',						'set lots of fields ',									'change text',	'2014/02/28',			getdate()+1,	'2014/02/28','2014/02/28',	'1',	'1',					'1'),
			(6,			0,				'ALL' ,							'UNCHANGED',	'',					'',						'should be no changes ',								'change text',	'2014/02/28',			getdate()+1,	'2014/02/28','2014/02/28',	'1',	'1',					'1')


	insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,								textNull)
	values	
			/* check that case changes are picked up */
			(1,					0,		'TextNull',			'UPDATE',		'',						'',						'',										'THIS SHOULD GO LOWER CASE'),
			(1,					0,		'TextNull',			'UPDATE',		'',						'',						'change case',							'this should go lower case'),
			/* check that the processNulls value works */
			(2,					0,		'TextNull',			'UPDATE',		'',						'',						'',										'A value'),
			(2,					0,		'TextNull',			'UNCHANGED',	'',						'',						'check that null is not processed',		null),
			(2,					1,		'TextNull',			'UPDATE',		'',						'',						'check that null is processed',			null)
		
	/* check that unique index constraint is picked up */
	insert into ##_EntityUpsert_TestData
			(recordUID, 	columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,					aGUID)
	values	(4,				'ALL',				'UPDATE',		'',						'',						'Add aGUID to one record', '1A9A8159-FFFA-4750-B417-5F3C400CD075'),
			(5,				'ALL',				'FAIL',			'aGUID',				'UniqueIndexConflict',	'Add same aGUID to another record', '1A9A8159-FFFA-4750-B417-5F3C400CD075')


	/* This creates lots of blank records in one go, for use later */
	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,				textnotnull,dateNotNullFromVarchar)
	select recordUID, columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,				textnotnull,dateNotNullFromVarchar from 
	(values	('textnotnull,dateNotNullFromVarchar',				'insert',		'',						'',						'',						'1',			getdate()))  as d (columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,				textnotnull,dateNotNullFromVarchar)
	inner join
	(values (7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30)) as T (recordUID) on 1 = 1


	insert into ##_EntityUpsert_TestData
			(recordUID, 	columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,		abooleanflag)
	values	(7,				'abooleanflag',		'UPDATE',		'',						'',						'-',		'1'),
			(7,				'abooleanflag',		'UPDATE',		'',						'',						'',			'0'),
			(7,				'abooleanflag',		'UPDATE',		'',						'1',					'',			'true'),
			(7,				'abooleanflag',		'UPDATE',		'',						'0',					'',			'false')

	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,abooleanflagbit)

	values	(7,			'abooleanflagbit',		'UPDATE',		'',						'',						'', 1)
			,(7,		'abooleanflagbit',		'UPDATE',		'',						'',						'', 0)
			,(7,		'abooleanflagbit',		'UPDATE',		'',						'',						'', 1)
			,(7,		'abooleanflagbit',		'UPDATE',		'',						'',						'', 0)

	insert into ##_EntityUpsert_TestData
			(recordUID,	columnsToProcess,						expectedAction,	expectedFailField,						expectedResult,		abooleanflagGroup,					abooleanflagGroup2,					comment)
	values	(7,			'abooleanflagGroup,abooleanflagGroup2',	'UNCHANGED',	'',										'',						'',									'',									'')
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UPDATE',		'',										'',						'abooleanflag,abooleanflagbit',		'abooleanflag2,abooleanflag2bit',	'')
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UNCHANGED',	'',										'',						'abooleanflagbit,abooleanflag',		'abooleanflag2bit,abooleanflag2',	'Reverse order, no data change')
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'FAIL',			'abooleanflagGroup,abooleanflagGroup2',	'NotValidFlag',			'xxxxx',							'yyyyy',							'Should Fail Validation and Fail update (in earlier versions the update would not fail but would null all the existing data)')
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UPDATE',		'',										'',						'abooleanflagbit,abooleanflag',		'',									'')
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UPDATE',		'',										'',						'abooleanflagbit,abooleanflag',		'abooleanflag2',					'')		
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UPDATE',		'',										'',						'abooleanflagbit2,abooleanflag',	'abooleanflag2bit',					'')		
			,(7,		'abooleanflagGroup,abooleanflagGroup2',	'UPDATE',		'',										'',						'',									'',									'')

	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,	aRadioGroup)
	values	(7,			'aRadioGroup',		'UPDATE',		'',						'',						'',			'radio1'),
			(7,			'aRadioGroup',		'UPDATE',		'',						'',						'',			'radio2'),
			(7,			'aRadioGroup',		'UPDATE',		'',						'',						'',			'')


	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess, expectedAction,	expectedFailField,		expectedResult,		comment,	Radio1, Radio2)
	values	(16,			'Radio1,radio2',		'UPDATE',		'',						'',						'',			1,			null)
			,(16,		'Radio1,radio2',		'UNCHANGED',		'',						'',						'',			null, 		0)
			,(16,		'Radio1,radio2',		'UPDATE',		'',						'',						'',			null,		1)
			,(16,		'Radio1,radio2',		'UPDATE',		'',						'',						'',			1,			null)
			,(16,		'Radio1,radio2',		'UPDATE',		'',						'',						'',			1,			1)
			,(16,		'Radio1,radio2',		'UPDATE',		'',						'',						'',			0,			0)


	insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess				,expectedAction,expectedFailField,	expectedResult,		comment,	textNotNull,	dateNotNullFromVarchar,anIntegerFlag	)
	values	(20,			1,			'textNotNull,dateNotNullFromVarchar,anIntegerFlag' ,		'FAIL',	'anintegerFlag',	'NotNumeric',					'',			'someText',	getdate(),	'+81-3-5210-5187'	)


	insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,	anIntegerFromVarChar, anIntegerFlag, anIntegerFlagFromInteger)
	values	
			(8,					0,		'anintegerFlag',		'fail',			'anintegerFlag',		'NotNumeric',			'',				'',				'aaaa',		NULL) 
			,(8,				0,		'anIntegerFromVarChar',	'update',		'',						'',						'a',			'0',			NULL,		null) 
			,(8,				0,		'anintegerFlag',		'fail',			'anintegerFlag',		'NumberTooLarge',		'',				'',				'1.22E16',	NULL) 
			,(8,				0,		'anIntegerFromVarChar',	'fail',			'anIntegerFromVarChar',	'NotNumeric',			'',				'BBB',			NULL,		NULL) 
			,(8,				0,		'anIntegerFromVarChar',	'fail',			'anIntegerFromVarChar',	'NumberTooLarge',		'',				'1.223E16',		null,		NULL) 
			,(8,				0,		'anIntegerFromVarChar',	'fail',			'anIntegerFromVarChar',	'NotNumeric',			'',				'(012) 1023',	null,		NULL) 

			,(8,				0,		'anIntegerFromVarChar',	'update',		'',						'',						'b',			'',				null,		 null) 
			,(8,				0,		'anIntegerFromVarChar',	'unchanged',	'',						'',						'',				'',				NULL,		NULL) 
			,(8,				1,		'anIntegerFromVarChar',	'update',		'',						'',						'nulls',		'1',			NULL,		 null) 
			,(8,				1,		'anIntegerFromVarChar',	'update',		'',						'',						'nulls',		null,			NULL,		 null)		
			,(8,				1,		'anIntegerFromVarChar',	'update',		'',						'',						'nulls',		'2',			NULL,		NULL) 
			,(8,				1,		'anIntegerFromVarChar',	'update',		'',						'',						'',				'',				NULL,		NULL)		

			
			,(8,				0,		'anintegerFlag',	'fail',			'anintegerFlag',		'notnumeric',			'',				'',			'(012) -', null) 
			,(8,				0,		'anintegerFlag',	'fail',			'anintegerFlag',		'notnumeric',			'',				'',			'+44 01', null) 
			,(8,				0,		'anintegerFlag',	'fail',			'anintegerFlag',		'notnumeric',			'',				'',			'[999]', null) 
			,(8,				0,		'anintegerFlag',	'fail',			'anintegerFlag',		'notnumeric',			'',				'',			'+81-3-5210-5187', null) 
			,(8,				0,		'anintegerFlag',	'update',		'',						'',						'',				'',			'1', null) 
			,(8,				0,		'anintegerFlag',	'update',		'',						'',						'',				'',			'2', null) 
			,(8,				0,		'anintegerFlag',	'unChanged',	'',						'',						'',				'',			'2', null) 
			,(8,				0,		'anintegerFlag',	'update',		'',						'',						'',			'',			'', null) 
			,(8,				0,		'anintegerFlag',	'unChanged',	'',						'',						'',			'',			'', null) 
			,(8,				0,		'anintegerFlag',	'update',		'',						'1',						'',			'',			'1.23', null) 
			,(8,				0,		'anintegerFlag',	'unChanged',	'',						'1',						'edge case!',	'',			'1.45', null) 
			,(8,				0,		'anintegerFlag',	'unChanged',	'',						'1',						'edge case!',	'',			'1.55', null) 
			,(8,				0,		'anintegerFlag',	'update',		'',						'',						'',				'',			'6', null) 
			,(8,				0,		'anintegerFlag,anIntegerFlagFromInteger',	'update',	'',	'',						'This tests converting to int from two different data types',	'',			'7.1', 7)



	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,	 aTextFlag,		aTextFlag2	)
	values	(9,			'aTextFlag',		'update',		'',						'',						'',			'A',			null) 
			,(9,		'aTextFlag',		'Unchanged',	'',						'',						'',			'a',			null) 
			,(9,		'aTextFlag',		'UPDATE',		'',						'',						'',			'B',			null) 
			,(9,		'aTextFlag',		'UPDATE',		'',						'',						'',			'',				null) 
			,(9,		'aTextFlag',		'UPDATE',		'',						'',						'',			'C',			null)		
			,(9,		'aTextFlag',		'UNCHANGED',	'',						'',						'',			null,			null)		


	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,															expectedAction,	expectedFailField,		expectedResult,		comment,	 aTextFlag,	aTextFlag2, aDateFlag, adateFlag2,anIntegerFlag,anIntegerFlagLinked	)
	values	(11,		'aTextFlag,aTextFlag2,anIntegerFlag,anIntegerFlagLinked,aDateFlag,adateFlag2',		'UPDATE',	'',						'',						'',			'A',		'X',	'1/2/34',	'2/4/2011','1',@aValidPersonID1	)		
			,(11,		'aTextFlag,aTextFlag2,anIntegerFlag,anIntegerFlagLinked,aDateFlag,adateFlag2',		'UPDATE',	'',						'',						'',			'B',		'Y',	'1/2/34',	'1/2/34',  '2',@aValidPersonID2)		
			,(11,		'aTextFlag,aTextFlag2,anIntegerFlag,anIntegerFlagLinked,aDateFlag,adateFlag2',		'UPDATE',	'',						'',						'',			'',			'',			'',		'', '', '')		
			,(11,		'aTextFlag,aTextFlag2,anIntegerFlag,anIntegerFlagLinked,aDateFlag,adateFlag2',		'UNCHANGED','',						'',						'',			'',			'',			'',		'',	'',	'')		
							


	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,			comment,	 aDateFlag)
	values	(10,		'aDateFlag',		'update',		'',						'Mar  4 2012 12:00AM',	'',			'2012/3/4') 
			,(10,		'aDateFlag',		'UPDATE',		'',						'May  6 2013 12:00AM',	'',			'2013/5/6') 
			,(10,		'aDateFlag',		'UPDATE',		'',						'',						'',			'') 
			,(10,		'aDateFlag',		'FAIL',			'aDateFlag',			'NotValidDate',			'',			'2001/14/14') 
			,(10,		'aDateFlag',		'UNCHANGED',	'',						'',			'',			null) 




	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,										expectedAction,	expectedFailField,			expectedResult,		comment,	 anIntegerMultipleFlag, anIntegerMultipleFlagLinked)
	values	(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'update',		'',							'',						'',			'1,2',				'4,5') 
			,(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'update',		'',							'',						'',			'1,2,3',			'4,5,6') 
			,(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'update',		'',							'',						'',			'1,3',				'4,5,6') 
			,(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'FAIL',		'anIntegerMultipleFlagLinked',	'InvalidForeignKey',	'',			'1,2,3',			'4,5,-1') 
			,(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'FAIL',		'anIntegerMultipleFlagLinked',	'NotNumeric',			'',			'1,2,3',			'4,B') 
			,(12,		'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'FAIL',		'anIntegerMultipleFlag',		'NotNumeric',			'',			'1,A,3',			'4,5') 

	/*
	Some tests of numeric validation on flags
	*/

	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,		expectedAction,	expectedFailField,		expectedResult,		comment,			aDecimalFlag, aNumericFromVarChar )
	values	(13,		'aDecimalFlag',			'update',		'',						'',						'',				'12.21' ,		'') 
			,(13,		'aDecimalFlag',			'update',		'',						'12230.00',				'',				'1.223E4',		'' ) 
			,(13,		'aDecimalFlag',			'fail',			'aDecimalFlag',			'NotNumeric',			'',				'Not a Number', '' ) 
			,(13,		'aDecimalFlag',			'fail',			'aDecimalFlag',			'NumberTooLarge',		'',				'1.223E16', '' ) 
			,(13,		'aDecimalFlag',			'update',		'aDecimalFlag',			'9999990000000000.00',	'Largest number','9.99999E15', '' ) 
			,(13,		'aNumericFromVarChar',	'update',		'',						'12233.300',			'',			'',				'1.22333E4' ) 
			,(13,		'aNumericFromVarChar',	'update',		'',						'22133.300',			'',			'',				'2.21333E4' ) 
			,(13,		'aNumericFromVarChar',	'fail',			'aNumericFromVarChar',	'NotNumeric',			'',			'',				'Not a NUmber' ) 
			,(13,		'aNumericFromVarChar',	'fail',			'aNumericFromVarChar',	'NumberTooLarge',		'',			'',				'1.0001E7' ) 
			,(13,		'aNumericFromVarChar',	'update',		'aNumericFromVarChar',	'9666600.000',			'',			'',				'9.6666E6' ) 



	/*
	Note that 14 and 15 must be processed together
	*/

	insert into ##_EntityUpsert_TestData
			(recordUID, columnsToProcess,															expectedAction,	expectedFailField,				expectedResult,		comment,										 aUniqueColumn,	 aUniqueColumnWithFilter,	 aUniqueColumnWithNonNullFilter)
	values	(14,		'aUniqueColumn,aUniqueColumnWithFilter,aUniqueColumnWithNonNullFilter',		'update',		'',								'',						'',											'1',				2,							3	) 
			,(15,		'aUniqueColumn',															'update',		'',								'',						'',											'2',				null,						null) 
			,(15,		'aUniqueColumn',															'fail',			'aUniqueColumn',				'UniqueIndexConflict',	'',											'1',				null,						null) 
			,(15,		'aUniqueColumnWithNonNullFilter',											'fail',			'aUniqueColumnWithNonNullFilter','UniqueIndexConflict',	'',											null,				null,						3) 
			,(15,		'aUniqueColumnWithFilter',													'fail',			'aUniqueColumnWithFilter',		'UniqueIndexConflict',	'Fails because already have record set to 2',null,				2,							null) 
			,(15,		'aUniqueColumnWithFilter',													'update',		'',								'',						'',											null,				100,						null) 
			,(14,		'aUniqueColumnWithFilter',													'update',		'',								'',						'Does not fail because unique index only for values less than 10',null,	100,				null) 



	/* testing integer multiple flags with multiple rows at a time */
	insert into ##_EntityUpsert_TestData
			(recordUID, batchUID,	columnsToProcess,											expectedAction,	expectedFailField,		expectedResult,		comment,	 anIntegerMultipleFlag, anIntegerMultipleFlagLinked)
	values	(21,		'imf1'		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,	'reset',		'',						'',						'',						'',				'') 
			,(22,		'imf1'		,	null											,		'reset',		'',						'',						'',						'',				'') 
			,(21,		'imf2'		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,	'update',		'',						'',						'',						'1,2',			@validPersonIDList1 ) 
			,(22,		'imf2'		,	null											,		'update',		'',						'',						'',						'1,2,3',		@validPersonIDList2) 
			,(21,		'imf3'		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,	'update',		'',						'',						'',						'1,2',			@validPersonIDList3) 
			,(22,		'imf3'		,	null											,		'unchanged',	'',						'',						'',						'1,2,3',		@validPersonIDList2) 
			,(21,		'imf4'		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,	'unchanged',	'',						'',						'',						'1,2',			@validPersonIDList3) 
			,(22,		'imf4'		,	null											,		'unchanged',	'',						'',						'Shouldn''t do nulls',	null,			null) 
			,(21,		'imf5'		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,	'update',		'',						'',						'Should do blanks'	,	null,			'') 
			,(22,		'imf5'		,	null											,		'unchanged',	'',						'',						'Shouldn''t do nulls',	null,			null) 


	/* testing integer multiple flags with multiple rows at a time */
	insert into ##_EntityUpsert_TestData
			(recordUID, batchUID,	columnsToProcess,											expectedAction,	expectedFailField,		expectedResult,		comment,	 anIntegerMultipleFlag, anIntegerMultipleFlagLinked)
	values	(27,		''		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'reset',		'',						'',						'',						'',				'') 
			,(27,		''		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'update',		'',						'',						'',						'1,2,3',		@validPersonIDList2) 
			,(27,		''		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked'	,		'unchanged',	'',						'',						'',						'1,2,3',		@validPersonIDList2) 
			,(27,		''		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'unchanged',	'',						'',						'Shouldn''t do nulls',	null,			null) 
			,(27,		''		,	'anIntegerMultipleFlag,anIntegerMultipleFlagLinked',		'unchanged',	'',						'',						'Shouldn''t do nulls',	null,			null) 


	/*
	Integer Flag With Linked Entity
	Check Foreign Key and Numeric
	*/
	insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,										anInteger, anIntegerFlagLinked)
	values	
			(23,				0,		'anintegerFlagLinked',	'fail',		'anIntegerFlagLinked',	'notnumeric',			'',											'',			'a') 
			,(23,				0,		'anintegerFlagLinked',	'fail',		'anIntegerFlagLinked',	'InvalidForeignKey',	'',											'',			'-' + @aValidPersonID1) 
			,(23,				0,		'anIntegerFlagLinked',	'update',	'',						'',						'',											'',			@aValidPersonID1) 
			,(23,				0,		'anIntegerFlagLinked',	'fail',		'anIntegerFlagLinked',	'InvalidForeignKey',	'OK.Will fail foreign key validation, but do Update',	'',			convert(varchar,convert(numeric(5,2),@aValidPersonID1) +.4 ))
			,(23,				0,		'anIntegerFlagLinked',	'update',	'',						'',						'',											'',			@aValidPersonID2)
			,(23,				0,		'anIntegerFlagLinked',	'unChanged','',						'',						'',											'',			@aValidPersonID2) 
			


	/* testing integer multiple flags with with process nulls set */
	insert into ##_EntityUpsert_TestData
			(recordUID, batchUID,	columnsToProcess,				processNulls, expectedAction,	expectedFailField,		expectedResult,		comment,	 anIntegerMultipleFlag)
	values	(24,		''		,	'anIntegerMultipleFlag'	,		1,				'reset',		'',						'',						'',						''		) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		1,				'update',		'',						'',						'',						'1,2'	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		1,				'update',		'',						'',						'Should do null'	,	null	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		1,				'update',		'',						'',						'',						'1,3'	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		1,				'update',		'',						'',						'Should do blank'	,	''	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		1,				'unchanged',	'',						'',						''	,					null	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		0,				'update',		'',						'',						'',						'1,2'	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		0,				'unchanged',	'',						'',						'Shouldnot do null'	,	null	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		0,				'update',		'',						'',						'',						'1,3'	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		0,				'update',		'',						'',						'Should do blank'	,	''	) 
			,(24,		''		,	'anIntegerMultipleFlag'	,		0,				'unchanged',	'',						'',						''	,					null	) 


	/* try integer flags with processNulls on & off */
insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,	anIntegerFlag)
	values	
			(26,				0,		'anintegerFlag',		'update',			'',					'',						'',				'1') 
			,(26,				0,		'anintegerFlag',		'unchanged',			'',					'',					'Should not process null',			null) 			
			,(26,				1,		'anintegerFlag',		'update',			'',					'',						'Should process null',			null) 			
			
				
/* some numeric items, check for truncation and data type */
insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,			anInteger,	anIntegerFromVarChar,	aDecimal, adecimalFromVarChar)
	values	
			(28,				0,		'aninteger',			'update',	'',						'',						'',				1,			NULL,					NULL,		null) 
			,(28,				0,		'anintegerFromVarchar',	'update',	'',						'2',					'',				NULL,		'2.1',					NULL,		null) 
			,(28,				0,		'aDecimal',				'update',	'',						'',						'',				NULL,		null,					1.23,		null) 			
			,(28,				0,		'aDecimalFromVarChar',	'update',	'',						'2.4500',				'',				NULL,		null,					null,		'2.45') 						
			,(28,				0,		'anintegerFromVarchar',	'fail',		'anintegerFromVarchar',	'NotNumeric',			'',				NULL,		'1,2',					NULL,		null) 
			,(28,				0,		'aDecimalFromVarChar',	'fail',		'aDecimalFromVarChar',	'NotNumeric',			'',				NULL,		null,					null,		'2.45,3.67') 									

/* Exposed As Bit */
insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,			exposedAsBit)
	values	
			(30,				0,		'exposedAsBit',	'update',		'',						'',					'',				'true'			) 
			,(30,				0,		'exposedAsBit',	'update',		'',						'',					'',				'false'			) 
			
/* Exposed As Bit using bit field to update*/
insert into ##_EntityUpsert_TestData
			(recordUID, processNulls,	columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment,			exposedAsBitUsingBit)
	values	
			(30,				0,		'exposedAsBitUsingBit',	'update',		'',						'',					'',				'1'			) 
			,(30,				0,		'exposedAsBitUsingBit',	'update',		'',						'',					'',				'0'			) 			