	/* see entityUpsert_TestHarness.sql for info */

IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = '_EntityUpsert_CreateTestEntity' and Type = 'P')
	DROP PROCEDURE _EntityUpsert_CreateTestEntity

if exists (select 1 from sysObjects where name  = 'entityUpsertTest ')
BEGIN
	drop table entityUpsertTest
END

GO

CREATE PROCEDURE _EntityUpsert_CreateTestEntity AS

/*********************************************
Initialisation of the entity table and flags
**********************************************/

/* drop and recreate entityUpsertTest */
	if exists (select 1 from sysObjects where name  = 'entityUpsertTest ')
	BEGIN
		drop table entityUpsertTest 
	END
	
	create table entityUpsertTest (
		entityUpsertTestID int identity(1,1),
		recordUID varchar(max) not null,
		textNotNull varchar(20) not null,
		textNotNullWithDefault varchar(max) not null default 'The Default Value',
		textNull varchar (30) null,
		dateNotNullFromVarchar datetime not null,
		dateNotNullFromVarcharWithDefault dateTime not null default getDate(),
		dateNull datetime null,
		dateNull2 datetime null,
		number float null,
		aDecimal DECIMAL (12,4) null,
		aDecimalFromVarchar DECIMAL (12,4) null,
		aNumericFromVarChar numeric (10,3) null,
		integerNotNullDefault int not null default 0,
		anInteger INT NULL,
		anIntegerFromVarchar int null ,
		foreignKey int null,
		aGUID uniqueIdentifier default newID(),
		aUniqueColumn int null  default  100 + (rand() * 10000000),
		aUniqueColumnWithFilter int null,
		aUniqueColumnWithNonNullFilter int null,
		created datetime,
		createdby int,
		lastupdatedby int,
		lastupdated datetime,
		lastupdatedbyperson int
	CONSTRAINT [PK_entityUpsertTest] PRIMARY KEY CLUSTERED 
	(
		[entityUpsertTestID] ASC
	) ON [PRIMARY]
	) ON [PRIMARY]
	
		
		
		
 	/* add schemaTable entry */
	declare @entityName sysname = 'entityUpsertTest', @entityTypeID int, @flagGroupID int, @flagGroupTextID varchar(50) , @flagTextID varchar(50) 
	if not exists (select 1 from schemaTable where entityName = @entityName)
	BEGIN
		insert into schemaTableBase (entityName, entityTypeID,flagsExist, screensexist)
		select @entityName, (select max(entityTypeID) from schemaTableBase) + 1,1,0
	END
	select @entityTypeID = entityTypeID from schemaTable where entityName = @entityName
	
	exec Generate_MRaudit @tablename = @entityName
	
	delete from modregister 
	where entityTypeID = @entityTypeID
	
	
	/* add a checkbox boolean flagGroup */
	select @flagGroupTextID = 'aBooleanFlagGroup', @flagTextID = 'aBooleanFlag'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (2,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add 2 checkbox flags */
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = @flagTextID + 'bit'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = @flagTextID + '2'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	select @flagGroupTextID = 'aBooleanFlagGroup2', @flagTextID = 'aBooleanFlag2'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (2,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add 2 checkbox flags */
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = @flagTextID + 'bit'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = @flagTextID + 'bit2'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	/* exposed As Bit*/
	select @flagGroupTextID = 'exposedAsBit1', @flagTextID = 'exposedAsBit'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated,formattingparameters)
		values (2,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE(),'isBitField=true' )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )


	/* exposed As Bit using a Bit field to update*/
	select @flagGroupTextID = 'exposedAsBitUsingBit1', @flagTextID = 'exposedAsBitUsingBit'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated,formattingparameters)
		values (2,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE(),'isBitField=true' )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )

	
	/* add a radio boolean flagGroup */
	select @flagGroupTextID = 'aRadioGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (3,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'radio1'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = 'radio2'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID )
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	
	/* add an integer flagGroup */
	select @flagGroupTextID = 'anIntegerGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (6,@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'anIntegerFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	/* add a flag */
	select @flagTextID = 'anIntegerFlagFromInteger'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )

	/* add a flag */
	select @flagTextID = 'anIntegerFlagLinked'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated,linkstoentitytypeid)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE(),0 )
	
	
	
	/* add an integer flagGroup */
	select @flagGroupTextID = 'aTextGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'text'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'aTextFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagTextID = 'aTextFlag2'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	/* add an integer flagGroup */
	select @flagGroupTextID = 'aDateGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'Date'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'aDateFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	select @flagTextID = 'aDateFlag2'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	/* add an integer flagGroup */
	select @flagGroupTextID = 'anIntegerMultipleGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'integerMultiple'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'anIntegerMultipleFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	select @flagTextID = 'anIntegerMultipleFlagLinked'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated, linksToEntityTypeID )
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE(),0 )
	
	
	/* add an text multiple flagGroup */
	select @flagGroupTextID = 'aTextMultipleGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'textMultiple'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'aTextMultipleFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )


	/* add an decimal flagGroup */
	select @flagGroupTextID = 'aDecimalGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'Decimal'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'aDecimalFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	
	/* add an financial flagGroup */
	select @flagGroupTextID = 'afinancialGroup'
	if not exists (select 1 from flagGroup where entityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID)
		insert into flagGroup (flagTypeID,entityTypeID,parentFlagGroupID, flagGroupTextID, name, active,Viewing,edit,search,download,viewingAccessrights,editAccessrights,searchAccessrights,downloadAccessrights,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values ((select flagTypeID from FlagType where DataTable = 'financial'),@entityTypeID,0,@flagGroupTextID,@flagGroupTextID,1,1,1,1,1,1,1,1,1,1,0,0,GETDATE(),0,0,GETDATE() )
	
	select @flagGroupID = flagGroupID from flagGroup where EntityTypeID = @entityTypeID and flagGroupTextID = @flagGroupTextID
	
	/* add a flag */
	select @flagTextID = 'afinancialFlag'
	if not exists (select 1 from flag where flagGroupID = @flagGroupID and FlagTextID = @flagTextID)
		insert into flag (flagGroupID, flagTextID, name,active,orderingindex,createdby,createdbyPerson,created,LastUpdatedBy,LastUpdatedByPerson,lastupdated)
		values (@flagGroupID,@flagTextID,@flagTextID,1,1,0,0,GETDATE(),0,0,GETDATE() )
	

	if exists (select 1 from sysobjects where name = 'entityUpsertTest_foreignKey')
	BEGIN
		drop table entityUpsertTest_foreignKey 
	END
	
	create table	entityUpsertTest_foreignKey  (foreignKey int NOT NULL PRIMARY KEY)
	INSERT INTO entityUpsertTest_foreignKey VALUES (1)
	
	ALTER TABLE entityUpsertTest
	ADD FOREIGN KEY (foreignKey)
	REFERENCES entityUpsertTest_foreignKey(foreignKey)
	
	create unique index UX_aGUID 
	on entityUpsertTest (aGUID) 
		
	create unique index entityUpsertTest_aUniqueColumn 
	on entityUpsertTest (aUniqueColumn )
	
	create unique index entityUpsertTest_aUniqueColumnWithNonNullFilter 
	on entityUpsertTest (aUniqueColumnWithNonNullFilter )
	where aUniqueColumnWithNonNullFilter is not null
	
	create unique index entityUpsertTest_aUniqueColumnWithFilter 
	on entityUpsertTest (aUniqueColumnWithFilter )
	where aUniqueColumnWithFilter   < 10 


	IF NOT EXISTS (select * from sys.types where name = 'entityUpsertTestAnalyseResultTableType')
		CREATE type entityUpsertTestAnalyseResultTableType as table (isOK bit, message nvarchar(200),rowid int, comment varchar(max))
	
	
	EXEC createflagview @entityName = 'entityUpsertTest', @viewName = 'ventityUpsertTestWithBoolean', @includeBooleans = 1
	
	