declare 
	@processRecordUIDs varchar  (max)     -- =  '7'   -- leave null to process all or add a comma separated list of ids
	,@debug bit = 0

/*
WAB 2015-01

A script to test entityUpsert and its validation in various situations 
Mod:
2016-05-27	WAB  Added code to check that values updated correctly (prompted by not detecting missing decimal places) 


How it works
============
1.		Creates an entity table called upsertEntityTest with various columns of different types, nullable or not, with defaults or not.
		Also creates a number of flags for that table
		(now done in separate file  _EntityUpsert_CreateTestEntity.sql)

2.	 	Creates a table ##_EntityUpsert_TestData. 
		(now done in separate file _EntityUpsert_CreateTestData.sql) 
		This has columns which map to each of the columns/flags in upsertEntityTest
		It also has control columns which indicate what is expected to happen when that each row is processed

3.		Populates ##_EntityUpsert_TestData with programmer specified rows which are designed to test upsertEntity.  
		Some are designed to fail, some designed to update, some designed to insert, some designed to not change any data
		Each row specifies whether it will result in an update/insert/unchanged/fail.  
		For failures it additionally specifies the nature of the expected validation error
		
		For successes it checks that the new value is correct (only works when updating a single field)

4.		Loops over ##_EntityUpsert_TestData table in effect using each row as a single upsert table.
		First it does a validation check and if there is a validation error it chacks that it is expected
 		Then it trys the upsert and checks that the result is as expected - including checking that if a validation error is raised the upsert does fail.

5.		If you want to test processing more than one row at a time then you need to link rows together by putting a unique value in the batchUID column
		Then you must set columnsToProcess to be null for all but one of the row (the first usually)


How to use
==========
1.  	To run every test make sure that @processRecordUIDs is null, to do a subsection of tests (useful when working in a single area) 
		set to the appropriate UUID
		If you want to clean up everything afterwards set @cleanUp = 1

2.		The result should be a couple of queries, one showing all the modRegister entries and one just outputing the ##_EntityUpsert_TestData table
		The messages should be a list of OKs

3.		If there are errors you will get a number of extra queries returned, and messages containing ERROR.
		Each of these queries gives the current and previous row in ##_EntityUpsert_TestData and some indication of what was expected and what actually happened.

		There is one test which is known to fail (unavoidably) so you will always get atleast one! 


How to populate ##_EntityUpsert_TestData table (edit file _EntityUpsert_CreateTestData.sql and run)  
================================
		The column recordUUID is used to identify the record to be updated, you will notice that each recordUUID may appear lots of times in the ##_EntityUpsert_TestData table
		This allows you to do an insert followed by lots of updates on the same record (or indeed lots of failed inserts)  

		There are some nonNull fields on the upsertEntityTest table, so unfortunately you always have to populate them!  
		But otherwise your insert statement to populate ##_EntityUpsert_TestData only needs to worry about the field you are interested in testing and the control fields	namely:
			 columnsToProcess,	expectedAction,	expectedFailField,		expectedResult,		comment
		Look at the current data for examples!

*/

declare 
	 @cleanUp bit = 0
	,@doValidate bit = 1
	,@startDate datetime = getdate()	
	,@useTemporaryTable bit  = 1  /* set to 0 for easier debugging (since you can run all the debug queries) */


	declare @entityName sysname = 'entityUpsertTest', @entityTypeID int, @dateObjectCreated datetime, @dateProcedureCreated datetime, @objectID int
	select @entityTypeID = entityTypeID from schemaTable where entityName = @entityName

	/* This section makes sure that the entityUpsertTest table (and its flags and things) has been created and is at its latest version */
	select @dateProcedureCreated = crdate from sysobjects where name = '_EntityUpsert_CreateTestEntity'
	select @dateObjectCreated = crdate from sysobjects where name = @entityName

	if @dateProcedureCreated is Null
	BEGIN
		RAISERROR ('You must run script _EntityUpsert_CreateTestEntity.sql first',16,16)
		return
	END
	
	IF (@dateObjectCreated is null or @dateProcedureCreated > @dateObjectCreated)
	BEGIN
		exec _EntityUpsert_CreateTestEntity
	END

	/* clear out all data from entityUpsertTest */
	update entityUpsertTest set lastupdated = getdate()
	delete from entityUpsertTest 
	
	/* flags should be deleted at same time (will shortly be done by trigger)*/
	delete from booleanflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)
	delete from textflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)
	delete from integerflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)
	delete from integermultipleflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)
	delete from dateflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)
	delete from decimalflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID)

	delete from modregister where entityTypeID = @entityTypeID


	/* This section makes sure that the ##_EntityUpsert_TestData  has been created and is at its latest version */
	SET @objectID = object_id ('tempdb..##_EntityUpsert_TestData') 
		
	select @dateProcedureCreated = crdate from sysobjects where name = '_EntityUpsert_CreateTestData'
	select @dateObjectCreated = crdate from tempdb..sysobjects where id = @objectID
	
	if @dateProcedureCreated is Null
	BEGIN
		RAISERROR ('You must run script _EntityUpsert_CreateTestData.sql first',16,16)
		return
	END

	IF (@dateObjectCreated is NUll or @dateProcedureCreated > @dateObjectCreated)
	BEGIN
		exec _EntityUpsert_CreateTestData
	END	
	
	/* clear out entityIDs from ##_EntityUpsert_TestData table */
	update ##_EntityUpsert_TestData set entityUpsertTestID = null

	


	
	/* Now process the test data */

	declare @processNullsVar varbinary(100)
    declare @truncateVar varbinary(100), @updateVar varbinary(100)
    declare @whereClause varchar(50)
    declare @validate int, @rows int
	declare @sqlCmd nvarchar(max)
    
    declare @rowid int, @batchUID varchar(10),@comment nvarchar(max), @columnsToProcess nvarchar(max), @returnTableName  sysname, @processNulls bit


	DECLARE @analyseResult entityUpsertTestAnalyseResultTableType



	DECLARE loop cursor local static for
	
		SELECT rowID, batchUID, columnsToProcess ,processNulls
		FROM 
			##_EntityUpsert_TestData
				left join 
			dbo.csvToTable(@processRecordUIDs) as records on recorduid = records.value
		where
			(@processRecordUIDs is null or records.value is not null)
			and columnsToProcess is not null 
		order by rowid

		declare @personid int = 0
	OPEN loop
		
		FETCH next from loop into @rowID, @batchUID, @columnsToProcess, @processNulls
 		WHILE @@fetch_status = 0 
		BEGIN

                select @processNullsVar = case when @processNulls = 1 then -1 else 0 end
                select @truncateVar = 0
                select @updateVar =  case when @columnsToProcess = 'ALL' then 0xfffffffffffffffffffffff else dbo.columnListToBitmask ('##_EntityUpsert_TestData','recordUID,' + @columnsToProcess) end

				IF isnull(@batchUID,'') = ''
					select @whereClause = 'rowid = ' + convert(varchar,@rowid) 
				ELSE
					select @whereClause = 'batchUID = ''' + @batchUID + ''''

				IF object_ID ('tempdb..##tempDataLoad') is not null
					DROP table ##tempDataLoad

				set @SQLCmd = 'select * into ##tempDataLoad from ##_EntityUpsert_TestData where '  + @whereclause
				exec (@SQLCmd)


				select @validate = @doValidate   --  will be either 1 or 0
				select @personid = @personid + 1

				WHILE @validate >= 0
				BEGIN
					BEGIN TRY
						IF object_ID ('tempdb..##results') is not null
							DROP table ##results	
						
						IF object_ID ('tempdb..##validate_results') is not null
							DROP table ##validate_results	

						delete from @analyseResult 

						select @returnTableName = '##' + case when @validate = 1 then 'validate_' else '' end + 'results' 

						declare @ErrorMessage varchar(max), @successmessage varchar(max), @message varchar(max)
						Select @ErrorMessage = null, @successmessage = null

						exec entityUpsert
							@validate = @validate,
							@upsertTableName = '##_EntityUpsert_TestData', 
							@columns_Updated  =   @updateVar  , 
							@entityName = 'entityUpsertTest',
							@lastUpdatedByPerson = 5,
							@processNulls = @processNullsVar,
							@truncate = @truncateVar,
							@whereClause = @whereClause,
							@useTemporaryTable = @useTemporaryTable,
							@debug = @debug,
							@returnTableName = @returnTableName 



						IF @validate = 1 
						BEGIN

							insert into @analyseResult
							select 
								case when r.upsertUniqueID is not null and dl.expectedAction = 'FAIL' THEN
									case when dbo.listFind(expectedFailField,field) <> 0 and message = expectedResult then 
										1
									when dbo.listFind(expectedFailField,field) <> 0 and message <> expectedResult then 
										0
									else
										0
									end	
								when r.upsertUniqueID is null and dl.expectedAction = 'FAIL' THEN
									0
								when r.upsertUniqueID is not null and dl.expectedAction <> 'FAIL' THEN
									0
								when r.upsertUniqueID is null and dl.expectedAction <> 'FAIL' THEN
									1
								else 
									0
								end	as isOK,
								case when r.upsertUniqueID is not null and dl.expectedAction = 'FAIL' THEN
									case when dbo.listFind(expectedFailField,field) <> 0 and message = expectedResult then 
										'Validation Failed As Expected. ' + comment
									when dbo.listFind(expectedFailField,field) <> 0 and message <> expectedResult then 
										'Did not expected ' + message + 'validation error. ' + comment
									else
										'Did not expected validation error for field ' + field
									end	
								when r.upsertUniqueID is null and dl.expectedAction = 'FAIL' THEN
									'Should have received validation error. '

								when r.upsertUniqueID is not null and dl.expectedAction <> 'FAIL' THEN
									'Validation error not expected. '
								when r.upsertUniqueID is null and dl.expectedAction <> 'FAIL' THEN
									'Passed Validation'
								else 
									'Not sure what happened'	
								end as message,
								isnull(dl.rowID, r.upsertUniqueID) as rowid,
								dl.comment as comment
								
							from 
								##tempDataLoad dl left outer join ##validate_results r on dl.rowID = r.upsertUniqueID
							

						END
						ELSE  /* Validate = 0 */
						BEGIN
						
							/* update entityID on ##_EntityUpsert_TestData table when an insert is done */
							update ##_EntityUpsert_TestData set EntityUpsertTestID = entityID
							from ##results r inner join ##_EntityUpsert_TestData dl on dl.rowID = r.upsertUniqueID
							where r.action = 'INSERT'
							
							
							insert into @analyseResult
							select 
								case when dl.expectedAction = r.action OR dl.expectedAction = 'reset' and r.action in ('update','unchanged') then 
									1 
								else 
									0 
								end as isOK
								,case when dl.expectedAction = r.action OR dl.expectedAction = 'reset' and r.action in ('update','unchanged') then 
									dl.expectedAction 
								else 
									'Expected ' + dl.expectedAction + '. Got ' + r.action 
								end as message
								,dl.rowid
								,dl.comment
							from 
								##tempDataLoad dl left outer join ##results r on dl.rowID = r.upsertUniqueID
						
							IF @columnsToProcess NOT LIKE '%,%' AND @columnsToProcess <> 'ALL'
							BEGIN
								DECLARE @TestValueResult nvarchar(MAX)
								set @TestValueResult = null
								declare @TestValueSQL nvarchar(MAX) =
									'
									SELECT 
										@TestValueResult = ''Updated Value Not Correct. '' + convert(varchar(max), e.' + @columnsToProcess + ') + '' should be ''+ convert(varchar(max), t.' + @columnsToProcess + ') 
									FROM 
										@analyseResult a
											INNER join
										##_EntityUpsert_TestData t	ON a.rowid = t.rowid
											INNER JOIN
										ventityUpsertTestWithBoolean e ON e.entityUpsertTestID = t.entityUpsertTestID 
									WHERE
										case when expectedResult <> '''' then expectedResult  else convert(varchar(max), t.' + @columnsToProcess + ' ) end <> convert(varchar(max),e.' + @columnsToProcess + ')'
									
								EXEC sp_executesql 	@TestValueSQL, N'@analyseResult entityUpsertTestAnalyseResultTableType readonly, @TestValueResult nvarchar(max) OUTPUT', @analyseResult, @TestValueResult OUTPUT

								IF @TestValueResult IS NOT NULL
								BEGIN
									PRINT @TestValueResult
								END	
							END


						END

					
					END TRY
					BEGIN CATCH
						
						
						IF exists (select 1 from ##tempDataLoad where expectedAction = 'FAIL')  and @validate = 0 
						BEGIN
								INSERT INTO @analyseResult
								select 1,' Upsert Failed as Expected', @rowid,''
						END
						ELSE 
						BEGIN
							INSERT INTO @analyseResult
							select 0,
								CASE WHEN @validate = 1  THEN 'Unexpected Failure during Validation.  ' ELSE 'Unexpected Failure.  ' END + error_message(),
								@rowid,''
	 					END		
					END CATCH


					set @message = ''
					select 
						@message += case when @message <> '' then char(10) else '' end + 
									CASE WHEN isOK = 1 then 'OK' else 'ERROR' end + ': Row ' + convert(varchar,rowid) + ' ' + message
									+ '. ' + isNull(comment,'')
					from @analyseResult	
					
					print @message

					IF exists (select 1 from @analyseResult where isOK = 0) 
					BEGIN
						select a.*, dl.* from @analyseResult a inner join ##_EntityUpsert_TestData dl on dl.rowid = a.rowid where a.isOK = 0
					END 

					


					
					SET @validate = @validate -1 
				END

				set nocount on
				update ##_EntityUpsert_TestData
				SET entityUpsertTestID = y.entityUpsertTestID 
				from 
					##_EntityUpsert_TestData  
						inner join 
					##_EntityUpsert_TestData as y on ##_EntityUpsert_TestData.recordUID = y.recordUID 
				where ##_EntityUpsert_TestData.entityUpsertTestID is null	and y.entityUpsertTestID is not null
				

		FETCH next from loop into @rowID, @batchUID, @columnsToProcess, @processNulls
		END  
		
		CLOSE loop
		DEALLOCATE loop


/*
select * from vmodregister 
where entityTypeID = @entityTypeID
and moddate > @startDate 
ORDER BY moddate DESC
*/

-- CLEAR UP
IF @cleanUp = 1
BEGIN
	declare @entityTypeID_ int, @entityName_ sysname = 'entityUpsertTest'
	select @entityTypeID_ = entityTypeID from schemaTable where entityName = @entityName_
	print @entityTypeID_ 
	select flagid,flaggroupid from vflagdef where entityTypeID = @entityTypeID_
	delete from booleanflagdata where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID_)
	
	delete from flag where flagid in (select flagid from vflagdef where entityTypeID = @entityTypeID_)
	delete from flagGroup where entityTypeID = @entityTypeID_
	delete from schemaTable where entityTypeID = @entityTypeID_
	delete from modregister where entityTypeID = @entityTypeID_
	
END




	select *
		FROM 
			##_EntityUpsert_TestData
				left join 
			dbo.csvToTable(@processRecordUIDs) as records on recorduid = records.value
		where
			@processRecordUIDs is null or records.value is not null
		order by rowid






