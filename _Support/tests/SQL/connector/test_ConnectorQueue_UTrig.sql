/*
WAB 2015-10-14

test_ConnectorQueue_uTrig
Validates that the connectorQueue trigger is correctly unsetting the synch flag 

*/

declare @personObjectID int, @personID int, @crmPerID varchar(100), @contactObjectID int, @personConnectorSynchStatus varchar(100), @ErrorMessage nvarchar(200)
select @personObjectID = id from connectorObject where object = 'person' and relayware = 1
select @contactObjectID = id from connectorObject where object = 'contact' and relayware = 0
select @personID = personid from person where crmperid is not null 
select @crmPerID = crmperid from person where crmperid is not null and personid <> @personid 


if not exists (select 1 from connectorQueue where connectorObjectID = @personObjectID and objectid = @personid)
BEGIN
	insert into connectorQueue
	(connectorObjectID,objectid,modifiedDateUTC)
	values (@personObjectID, @personid, getdate())
END

if not exists (select 1 from connectorQueue where connectorObjectID = @contactObjectID and objectid = @crmPerID)
BEGIN
	insert into connectorQueue
	(connectorObjectID,objectid,modifiedDateUTC)
	values (@contactObjectID, @crmPerID, getdate())
END


update connectorQueue set synchAttempt = 0 where connectorObjectID = @personObjectID and objectID = @personid
select @personConnectorSynchStatus = isnull(personConnectorSynchStatus,'') from vperson where personid = @personid 
IF @personConnectorSynchStatus <> ''
BEGIN
	SET @ErrorMessage  = 'personConnectorSynchStatus should be null after synchAttempt set to 0. Is ' + @personConnectorSynchStatus
	RAISERROR (@ErrorMessage ,16,16)
END

update connectorQueue set synchAttempt = 5 where connectorObjectID = @personObjectID and objectID = @personid
select @personConnectorSynchStatus = isnull(personConnectorSynchStatus,'') from vperson where personid = @personid 
IF @personConnectorSynchStatus <> 'PersonSuspended'
BEGIN
	SET @ErrorMessage  = 'personConnectorSynchStatus should be PersonSuspended after synchAttempt set to 0. Is ' + @personConnectorSynchStatus
	RAISERROR (@ErrorMessage ,16,16)
END	


update connectorQueue set synchAttempt = 0 where connectorObjectID = @contactObjectID and objectID = @crmPerID
select @personConnectorSynchStatus = isnull(personConnectorSynchStatus,'') from vperson where crmPerID = @crmPerID
IF @personConnectorSynchStatus <> ''
BEGIN
	SET @ErrorMessage  = 'personConnectorSynchStatus should be null after synchAttempt set to 0. Is ' + @personConnectorSynchStatus
	RAISERROR (@ErrorMessage ,16,16)
END

update connectorQueue set synchAttempt = 5 where connectorObjectID = @contactObjectID and objectID = @crmPerID
select @personConnectorSynchStatus = isnull(personConnectorSynchStatus,'') from vperson where crmPerID = @crmPerID
IF @personConnectorSynchStatus <> 'PersonSuspended'
BEGIN
	SET @ErrorMessage  = 'personConnectorSynchStatus should be PersonSuspended after synchAttempt set to 0. Is ' + @personConnectorSynchStatus
	RAISERROR (@ErrorMessage ,16,16)
END	

