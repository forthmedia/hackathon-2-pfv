var gulp = require('gulp');
var sequence = require('run-sequence');
var argv = require('yargs').argv;
var fs = require('fs');
//config contains all constants and variables - passed to all tasks. Also parses command parms
var global_config = require('./gulp/global.config');

//recursively imports all task modules
fs.readdirSync(global_config.TASKDIR)
    .map(function (file) {
        require(global_config.TASKDIR + file)(global_config);
    });

// (re)Build the common assets (Vendor, shared components, etc)
gulp.task('build:common', function() {
    sequence('clean:common' , 'docs:common' , 'vendor' , 'common' )
})

// (re)Build the common assets (Vendor, shared components, etc)
gulp.task('build:app', function( done ) {

    if ( !argv.app ) {
        console.log("--app parameter is required");
        process.exit();
    }
    sequence( 'app' )
})

gulp.task('cache:clear', function (callback) {
    return cache.clearAll(callback)
})

// default task cleans target drectory, builds source, starts server and watches source for changes
gulp.task('default', function () {
	console.log("There is no gulp default task.");		
	console.log("To build common and vendor distributions use gulp build:common.");		
    // watch could be more granular if running tasks gets lengthy
    // gulp.watch(config.WATCH.sass, ['sass']);
    // gulp.watch(config.WATCH.javascript, ['javascript']);
    // gulp.watch(config.WATCH.templates, ['templates']);
})
