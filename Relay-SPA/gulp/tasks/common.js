var gulp = require('gulp');
var argv = require('yargs').argv;
var path = require('path');
var $ = require('gulp-load-plugins')();

// Combine JavaScript into one file
// In production, the file is minified
module.exports = function (globals) {

    gulp.task('common', ['common:javascript']);

    gulp.task('common:javascript', function () {
        var uglify = $.if(globals.isProduction, $.uglify()
            .on('error', function (e) {
                console.log(e);
            }));
        return gulp.src(globals.SOURCE.common.js)
            .pipe($.concat('app.common.js'))
            .pipe(uglify)
            .pipe(gulp.dest(globals.TARGET.common + '/js'))
    });


};
