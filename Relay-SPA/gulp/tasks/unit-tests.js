'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

var karma = require('karma');

var files = [{
        pattern: '../Relay/SPA/common/js/vendor.js',
        watched: true
    },
    {
        pattern: 'node_modules/angular-mocks/angular-mocks.js',
        watched: false
    },
    {
        pattern: 'src/common/common.module.js',
        watched: true
    },
    {
        pattern: 'src/common/**/!(*.spec|*.xlat.service).js',
        watched: true
    },
    {
        pattern: 'src/common/**/*.xlat.service.js',
        watched: true
    },
    {
        pattern: 'src/common/**/*.spec.js',
        watched: true
    }
]

module.exports = function(config) {

    function runTestsCommon(singleRun, done) {
        karma.server.start({
            configFile: __dirname + '/../../karma.conf.js',
            files: files,
            singleRun: singleRun,
            autoWatch: !singleRun
        }, done());
    }

    gulp.task('test:common', [], function(done) {
        runTestsCommon(true, done);
    });

    gulp.task('test:auto', ['watch'], function(done) {
        runTestsCommon(false, done);
    });
};