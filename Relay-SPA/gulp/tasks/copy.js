var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
module.exports = function (config) {

    gulp.task('copy', ['bootstrap:fonts', 'uigrid:fonts']);

    gulp.task('bootstrap:fonts', function () {
        return gulp.src(config.SOURCE.fonts.bootstrap)
            .pipe(gulp.dest(config.TARGET.webroot + '/translationBulk/assets/fonts/bootstrap'));
    });

    gulp.task('uigrid:fonts', function () {
        return gulp.src(config.SOURCE.fonts.uigrid)
            .pipe(gulp.dest(config.TARGET.webroot + '/translationBulk/assets/css'));
    });


};
