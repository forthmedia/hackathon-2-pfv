var gulp = require('gulp');
var sequence = require('run-sequence');
var $ = require('gulp-load-plugins')();

// Combine JavaScript into one file
// In production, the file is minified (Vendor code is always minified)
module.exports = function (globals) {

    gulp.task('vendor', ['vendor:javascript']);

    gulp.task('vendor:javascript', function () {
        return gulp.src(globals.SOURCE.vendor.js)
            .pipe($.concat('vendor.js'))
            .pipe($.uglify())
            .pipe(gulp.dest(globals.TARGET.common + '/js'))
    });

};