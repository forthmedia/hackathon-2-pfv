var gulp = require('gulp');
var gulpDocs = require('gulp-ngdocs');
// Delete the target assets folder
// This happens every time a build starts
module.exports = function (config) {

    gulp.task('docs:common', [], function () {
    return gulpDocs.sections( 
        {
            common: {
                glob: config.SOURCE.common.js,
                title: 'Common API'
            }
        })
        .pipe(gulpDocs.process( {
            title: 'Relayware Angular SPA Documentation',
            html5Mode: false,
            startPage: '/'
        } ))
        .pipe(gulp.dest('./docs'));
    });



};
