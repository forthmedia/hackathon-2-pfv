var gulp = require('gulp');
var argv = require('yargs').argv;
var path = require('path');
var del = require('del');
var sequence = require('run-sequence');
var $ = require('gulp-load-plugins')();
var install = require("gulp-install");
var config = require( '../../src/apps/test/test.config');
   
module.exports = function (globals) {
    
    var uglify = $.if(globals.isProduction, $.uglify()
    .on('error', function (e) {
        console.log(e);
    }));

    /**
     * The 'app' task runs javascript and sass builds asynchronously
     */
    gulp.task('app', function() {
        sequence( 'app:clean' ,  ['app:javascript' , 'app:scss' , 'app:templates' , 'app:vendor' ] )
        gulp.watch( 'src/apps/' + argv.app + '/**/*.scss', ['app:scss']);
        gulp.watch( 'src/apps/' + argv.app + '/**/*.js', ['app:javascript']);
        gulp.watch( 'src/apps/' + argv.app + '/**/*.html', ['app:templates']);
    });

    /**
     * Clean the app assets folder
     */
    gulp.task('app:clean', function ( ) {
        return del(
            globals.TARGET.apps + argv.app + '/assets', { force: true }
        );
    })


    /**
     * Use app:install if you have custom dependencies specified in package.json]
     */
    gulp.task('app:install', function () {
        gulp.src(['src/apps/' + argv.app + '/package.json'])
        .pipe(install());
    });

    /**
     * Create a vendor js file if there are custom dependencies
     */
    gulp.task('app:vendor', function () {
        return gulp.src(config.SOURCE.vendor.js)
            .pipe($.concat( 'app.' + argv.app + '.vendor.js'))
            .pipe($.uglify())
            .pipe(gulp.dest(globals.TARGET.apps + argv.app + '/assets/js'))
    });

    /**
     * Concatentate the application js files, and minify if production
     */
    gulp.task('app:javascript', function () {
        return gulp.src([
            globals.SOURCE.apps + argv.app + '/**/*.js',
            '!' + globals.SOURCE.apps + argv.app + '/**/*.spec.js',
            '!' + globals.SOURCE.apps + argv.app + '/test.config.js'
        ])
            .pipe($.concat( 'app.' + argv.app + '.js'))
            .pipe(uglify)
            .pipe(gulp.dest(globals.TARGET.apps + argv.app + '/assets/js'))
    });

    /**
     * Loads HTML partials into $templateCache
     */
    gulp.task('app:templates', function () {

        return gulp.src(path.join(globals.SOURCE.apps, argv.app, '**/*.html'))
            .pipe($.ngHtml2js({
                prefix: argv.app + '/',
                moduleName: argv.app,
                declareModule: false
            }))
            .pipe($.uglify())
            .pipe($.concat('app.' + argv.app + '.templates.js'))
            .pipe(gulp.dest(globals.TARGET.apps + '/' + argv.app + '/assets/js'))

    });
    /**
     * Build the application css file
     */
    gulp.task('app:scss', function () {

        var minifycss = $.if(config.isProduction, $.cleanCss());

        var injectFiles = gulp.src([
            globals.SOURCE.apps + argv.app + '/**/*.scss',
            '!' + globals.SOURCE.apps + argv.app + '/app.scss',
            '!' + globals.SOURCE.apps + argv.app + '/node_modules/**/*.scss'
        ],
            {
                read: false
            });

        var injectOptions = {
            transform: function (filePath) {
                return '@import \'' + filePath + '\';';
            },
            starttag: '// injector',
            endtag: '// endinjector',
            addRootSlash: false
        };

        return gulp.src( './src/apps/'  + argv.app + '/app.scss')
            .pipe($.sourcemaps.init())
            .pipe($.inject(injectFiles, injectOptions))
            .pipe($.sass({
                includePaths: config.SOURCE.sass // just for custom @import paths for this application
            })
                .on('error', $.sass.logError))
            .pipe($.autoprefixer({
                browsers: config.COMPATIBILITY
            }))
            .pipe($.concat('app.' + argv.app + '.css'))
            .pipe(minifycss)
            .pipe($.if(!config.isProduction, $.sourcemaps.write()))
            .pipe(gulp.dest(globals.TARGET.apps + '/' + argv.app + '/assets/css'))

    })

};
