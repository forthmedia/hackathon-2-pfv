var gulp = require('gulp');
var del = require('del');
// Delete the target assets folder
// This happens every time a build starts
module.exports = function (config) {

    gulp.task('clean:common', function () {
        return del(
            config.TARGET.common , { force: true }
        );
    })

};
