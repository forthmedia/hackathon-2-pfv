var argv = require('yargs').argv;

// Check for --production flag
//TODO: should be in some kind of ENV implementation

var isProduction = !!(argv.production);

// Browsers to target when prefixing CSS.
var COMPATIBILITY = ['last 2 versions', 'ie >= 9'];

// File paths to various assets are defined here.
var SOURCE = {
    apps: './src/apps/' ,
    fonts: {
        bootstrap: [
            'bower_components/bootstrap-sass/assets/fonts/bootstrap/**/*.*'
        ],
        uigrid: [
            'bower_components/angular-ui-grid/ui-grid.{eot,svg,ttf,woff}'
        ]
    },
    common: {
        js: [
            '!src/common/**/*.spec.js',
            'src/common/**/*.js'
        ],
        sass: [

        ]
    },
    vendor: {
        js:  [
        'node_modules/angular/angular.js',
        'node_modules/angular-ui-router/release/angular-ui-router.js',
        'node_modules/angular-bootstrap/ui-bootstrap.js',
        'node_modules/angular-bootstrap/ui-bootstrap-tpls.js'
        ],
        sass:  [
        ]
    }
};

// File paths to WATCH.
var WATCH = {
    sass: [
        'src/assets/scss/**/*.scss',
        'src/common/**/*.scss',
        'src/apps/**/*.scss'
    ],
    javascript: [
        'src/assets/js/**/*.js',
        'src/apps/**/*.js',
        'src/common/**/*.js'
    ],
    templates: [
        'src/apps/**/*.html',
        'src/common/**/*.html'
    ]
};

// File paths to various targets.
var TARGET = {
    common: '../Relay/SPA/common',
    apps: '../Relay/SPA/'
};

// Config - gets passed to all tasks.
module.exports = {
    isProduction: isProduction,
    PORT: 8079,
    SERVERROOT: './build',
    APPROOT: './Relay-SPA/src/apps/',
    TASKDIR: './gulp/tasks/',
    COMPATIBILITY: COMPATIBILITY,
    WATCH: WATCH,
    SOURCE: SOURCE,
    TARGET: TARGET
};