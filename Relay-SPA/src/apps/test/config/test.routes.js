(function() {
    'use strict';

    angular.module('test')
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/home');

            $stateProvider
                .state('test', {
                    url:'/',
                    views: {
                        '': {
                            templateUrl: 'test/app.test.html'
                            },
                        'header': {
                                template: 'Default Header'
                            }
                    }
                })
               .state('test.home', {
                    url:'home',
                    views: {
                        'content@test': {
                            templateUrl: 'test/modules/test.one.home.html'
                        }
                    }
                })
                .state('test.portal', {
                    url:'portal',
                    views: {
                        'content@test': {
                            templateUrl: 'test/modules/test.one.portal.html'
                        }
                    }
                })
                .state('test.sales', {
                    url:'sales',
                    views: {
                        'content@test': {
                            templateUrl: 'test/modules/test.one.sales.html'
                        }
                    }
                })
                .state('test.partner', {
                    url:'partner',
                    views: {
                        'content@test': {
                            templateUrl: 'test/modules/test.one.partner.html'
                        }
                    }
                })
        }])


})();
