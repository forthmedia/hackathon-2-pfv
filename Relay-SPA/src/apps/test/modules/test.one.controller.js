(function() {
    'use strict';

    angular.module( 'test.one' )
    .controller( 'UIGridController' , ctrl );

    ctrl.$inject = [ ];

    function ctrl(  ) {

        var vm = this;

        vm.gridData = [
            {
                "firstName": "Cox",
                "lastName": "Carney",
                "company": "Enormo",
                "employed": true
            },
            {
                "firstName": "Lorraine",
                "lastName": "Wise",
                "company": "Comveyer",
                "employed": false
            },
            {
                "firstName": "Nancy",
                "lastName": "Waters",
                "company": "Fuelton",
                "employed": false
            }
        ];
    }


})();
