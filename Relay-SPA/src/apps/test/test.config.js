var argv = require('yargs').argv;

var isProduction = !!(argv.production);

var SOURCE = {
    vendor: {
        js:  [
        './node_modules/ng-table/bundles/ng-table.js'
        ],
        sass:  [
        ]
    }
};

module.exports = {
    isProduction: isProduction,
    SOURCE: SOURCE
};