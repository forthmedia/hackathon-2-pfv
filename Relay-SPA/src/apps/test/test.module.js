(function() {
'use strict';
// A module to verify the build process

angular.module('test' , [
    
    // Include common module
    'common', 
    // Application sub-modules
    'test.one'    
    ])

angular.module('test.one', []);
    
})();