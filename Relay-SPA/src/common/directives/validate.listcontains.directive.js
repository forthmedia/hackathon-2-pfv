(function() {
/**
		custom validator for form inputs
		checks if value is contained in a simple array
		
*/

angular.module('common')
.directive('rwArrayContains', dir )

function dir( ) {
  var directive = {
    require: '?ngModel',
    scope : {
    	rwArrayData : '='
    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.rwArrayContains = function(modelValue, viewValue) {
        return _.indexOf( scope.rwArrayData , modelValue ) === -1 
      };
    }
  };
  return directive;	
};


})();
