(function() {
  'use strict';

	angular
		.module('common')
		.directive('fmFileModel' , fileModelDirective)

	fileModelDirective.$inject = [ '$parse' ]

        function fileModelDirective( $parse ) {

        var directive =  {
                require: '?ngModel',
                restrict: 'A',
                link: function(scope, element, attrs , ngModel) {
                    var model = $parse(attrs.fmFileModel);
                    var modelSetter = model.assign;

                    element.bind('change', function(){
                        ngModel.$setViewValue(element[0].files[0].name);
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };

	    return directive;

	  }

})();
