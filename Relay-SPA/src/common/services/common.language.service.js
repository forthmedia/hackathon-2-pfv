/**
 * @ngdoc service
 * @name common.service:languageService
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.languageService' , languageService );

    languageService.$inject = [ '$http' ];

    function languageService( $http ) {
        var service = {
            getLanguages: getLanguages
        };
        return service;

        /**
         * @ngdoc method
         * @name getLanguages
         * @methodOf common.service:languageService
         * @returns {promise} http request
         * @description
         * Makes http __GET__ to /webservices/callwebservice.cfc
         *  * method=callwebservice
         *  * webservicename=api_as_webservice
         *  * returnFormat=JSON
         *  * methodname=getLanguages
         */
        function getLanguages() {
            return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnFormat=JSON&methodname=getLanguages')
                .then(getLanguagesComplete)
                .catch(getLanguagesFailed);

                function getLanguagesComplete(response) {
                    return response.data;
                }

                function getLanguagesFailed(error) {
                    return error.data;
                }
            }

        }
})();