/**
 * @ngdoc service
 * @name common.service:xlatService
 * @requires $window
 * @requires common.service:pageService
 * @description
 * A description of the controller, service or filter
 */
(function () {
    'use strict';

    angular
        .module('common')
        .factory('common.xlatService', xlatService)

    xlatService.$inject = ['$window']

    function xlatService($window) {

        var service = {};
        service.xlat = xlat;

        return service;

        /**
         * @ngdoc method
         * @name xlat
         * @methodOf common.service:xlatService
         * @description
         * Translates a phrase
         *
         * @param {string} label phrase to translate
         * @returns {string} phrase translated phrase
         */
        function xlat(label) {

            if ($window.phr.hasOwnProperty(label)) {
                return $window.phr[label];
            } else {
                return label;
            }
        }
    };

})();