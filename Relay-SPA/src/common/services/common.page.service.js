/**
 * @ngdoc service
 * @name common.service:pageService
 * @description
 * This services provides methods which are useful for managing a page object
 * To use this service in controllers inject 'common.pageService'
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.pageService' , pageService );

    pageService.$inject = [ ];

    function pageService(  ) {

        var page = {
            "pageTitle" : "Default Page Title"
        };

        var service = {
            getPage: getPage,
            getPageTitle: getPageTitle,
            setPageTitle: setPageTitle
        };

        return service;

        /**
         * @ngdoc method
         * @name getPage
         * @methodOf common.service:pageService
         * @description
         * Returns a page object containing the following keys
         *  { 
         *      pageTitle : 
         *  }
         * @returns {object} page object
         */
        function getPage() {
            return page;
        }

        /**
         * @ngdoc method
         * @name setPageTitle
         * @methodOf common.service:pageService
         * @description
         * sets title on page object
         * 
         * @param {string} title the page title
         */
        function setPageTitle( title ) {
            page.pageTitle = title;
        }
        /**
         * @ngdoc method
         * @name getPageTitle
         * @methodOf common.service:pageService
         * @description
         * gets title from page objectcommon.service:
         *
         */
        function getPageTitle( ) {
            return page.pageTitle;
        }
    }

})();
