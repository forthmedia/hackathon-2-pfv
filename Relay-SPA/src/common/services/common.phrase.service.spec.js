
describe( 'Service: common.phraseService' , function() {

    var services = ['getAllPhrases' , 'getAllEntityPhrases' , 'getEntityTypePhrases' , 'getPhraseEntityTypes']
    var languageService;
    var $httpBackend;
    var url = "/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnFormat=JSON&methodname=";

    beforeEach(module('common'));

    beforeEach( inject( ['common.phraseService' , '$httpBackend' , function( _phraseService_ , _$httpBackend_ ) {
        phraseService = _phraseService_;
        $httpBackend = _$httpBackend_;

    }]))

    it("Service should exist" ,  function() {
        expect( !!phraseService ).toBe(true);
    })


    services.forEach( function( service ) { // All services methods can be tested with the same expectations

        it( service + ": Should return data" ,  function() {

            $httpBackend.when('GET', url & service ).respond(200, { foo: 'bar' });

            phraseService[service]()
                .then( function(response) {
                    expect( response ).toEqual( { foo : 'bar' });           
                }) 
            $httpBackend.flush();
        })

        it( service + ": Should return error with bad response" ,  function() {

            $httpBackend.when('GET', url & service ).respond(500, 'error' );

            phraseService[service]()
                .then( function(response) {
                    expect( response ).toEqual( 'error' );           
                }) 
            $httpBackend.flush();
        })

    })

})

