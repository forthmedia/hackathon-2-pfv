(function() {
'use strict';

angular.module('common')
.factory('moment' , momentService )
.factory('_' , lodashService )
.factory('common.utils' , utils )

    momentService.$inject = [ '$window' ];
    lodashService.$inject = [ '$window' ];

    function momentService( $window ) {
    return $window.moment;
    }

    function lodashService( $window ) {
    return $window._;
    }

    function utils(  ) {

        var service = {
            createUUID: createUUID
        };
        return service;

        function createUUID() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }
    }

})();
