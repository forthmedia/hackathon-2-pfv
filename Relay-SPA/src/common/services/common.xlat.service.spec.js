
describe( 'Service: common.xlatService' , function() {

    var xlatSservice;
    var $window;

    beforeEach(module('common'));

    beforeEach( inject( [ '$window' , 'common.xlatService' , function( _$window_ , _xlatService_  ) {
        $window = _$window_;
        xlatService = _xlatService_;
        $window.phr = { translationExists : 'translation exists'};
    }]))

    it("Service should exist" ,  function() {
        expect( !!xlatService ).toBe(true);
    })

    it("xlat: Should translate a phrase if translation available" ,  function() {

        expect( xlatService.xlat('translationExists')).toBe('translation exists');
    })

    it("xlat: Should return original phrase if no translatione" ,  function() {

        expect( xlatService.xlat('translationNotExists')).toBe('translationNotExists');
    })


})
