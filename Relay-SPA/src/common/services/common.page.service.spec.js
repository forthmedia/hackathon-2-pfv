
describe( 'Service: common.pageService' , function() {

    beforeEach(module('common'));

    beforeEach( inject( ['common.pageService' , function( _pageService_ ) {
        pageService = _pageService_;

    }]))

    it("Service should exist" ,  function() {
        expect( !!pageService ).toBe(true);
    })
    it("Should have a default page object" ,  function() {
        expect( pageService.getPage ).not.toThrow(); 
    })
    it("page object should have pageTitle" ,  function() {
        expect( pageService.getPage().pageTitle ).toBeDefined(); 
    })
    it("getPageTitle should return page.pageTitle" ,  function() {
        expect( pageService.getPage().pageTitle ).toEqual( pageService.getPageTitle() ); 
    })
    it("setPageTitle should set a new Title" ,  function() {
        pageService.setPageTitle("foo"); 
        expect( pageService.getPageTitle() ).toBe("foo"); 
    })


})
