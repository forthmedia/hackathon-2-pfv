
describe( 'Service: common.languageService' , function() {

    var languageService;
    var $httpBackend;
    var url = "/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnFormat=JSON&methodname=";

    beforeEach(module('common'));

    beforeEach( inject( ['common.languageService' , '$httpBackend' , function( _languageService_ , _$httpBackend_ ) {
        languageService = _languageService_;
        $httpBackend = _$httpBackend_;

    }]))

    it("Service should exist" ,  function() {
        expect( !!languageService ).toBe(true);
    })

    it("getLanguages: Should return data" ,  function() {

        $httpBackend.when('GET', url & 'getLanguages' ).respond(200, { foo: 'bar' });

        languageService.getLanguages()
            .then( function(response) {
                expect( response ).toEqual( { foo : 'bar' });           
            }) 
        $httpBackend.flush();
    })

    it("It should return a bad response" ,  function() {

        $httpBackend.when('GET', url & 'getLanguages' ).respond(500, 'error');

        languageService.getLanguages()
            .then( function(response) {
                expect( response ).toEqual( 'error' );           
            }) 
        $httpBackend.flush();
    })

})
