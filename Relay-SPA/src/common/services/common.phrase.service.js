/**
 * @ngdoc service
 * @name common.service:phraseService
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.phraseService' , phraseService );

    phraseService.$inject = ['$http' ];

    function phraseService( $http ) {

        var phraseStruct = "";

        var service = {
            getAllPhrases: getAllPhrases,
            getAllEntityPhrases: getAllEntityPhrases,
            getEntityTypePhrases: getEntityTypePhrases,
            getPhraseEntityTypes: getPhraseEntityTypes,
            getPhraseStructure: getPhraseStructure
        };
        return service;

    ////////////

    function getPhraseStructure() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getPhraseStruct')
            .then( function( response ) { phraseStruct = response} )
            .catch( function ( error) { console.log('XHR Failed for getPhraseStructure.' +  error.data);});
    }

    function getAllPhrases() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getAllPhrases')
            .then(getPhrasesComplete)
            .catch(getPhrasesFailed);

            function getPhrasesComplete(response) {
                return response.data;
            }

            function getPhrasesFailed(error) {
                return( error.data );
            }
        }

    function getAllEntityPhrases( entityTypeID) {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getAllEntityPhrases&entityTypeID=' + entityTypeID )
            .then(getPhrasesComplete)
            .catch(getPhrasesFailed);

            function getPhrasesComplete(response) {
                return response.data;
            }

            function getPhrasesFailed(error) {
                return( error.data );
            }
        }

    function getEntityTypePhrases( entityTypeID ) {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getEntityTypePhrases&entityTypeID=' + entityTypeID )
            .then(getEntityTypePhrasesComplete)
            .catch(getEntityTypePhrasesFailed);

            function getEntityTypePhrasesComplete(response) {
                return response.data;
            }

            function getEntityTypePhrasesFailed(error) {
                return( error.data );
            }
        }

    function getPhraseEntityTypes() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getPhraseEntityTypes')
            .then(getEntityTypesComplete)
            .catch(getEntityTypesFailed);

            function getEntityTypesComplete(response) {
                return response.data;
            }

            function getEntityTypesFailed(error) {
                return( error.data );
            }
        }
    }

})();
