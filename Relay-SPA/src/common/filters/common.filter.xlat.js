/**
 * @ngdoc filter
 * @name common.filter:xlat
 * @description
 * Filter that translates a phrase
 * @requires common.service:xlatService
 * @example
 * &lt;p&gt;{{ 'phr.phrase' | xlat }}&lt;/p&gt;
 */
(function() {
'use strict';

angular.module('common')
	.filter( 'xlat' , xlatFilter )

	xlatFilter.$inject = [ "common.xlatService"];

	function xlatFilter( xlatService ) {
		return function(label, parameters) {
			label = label.replace('phr.' , '');	
			return xlatService.xlat(label);
		};
	}
})();