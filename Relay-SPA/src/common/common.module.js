/**
 * @ngdoc overview
 * @name common
 * @description
 * This module includes the following modules for use in all applications
 *  *  ngAnimate
 *  *  ngSanitize
 *  *  ngMessages
 *  *  ui.grid
 *  *  ui.bootstrap
 *  *  ui.router
 *  *  checklist-model    
 *  *  isteven-multi-select

 */
(function() {
'use strict';

angular.module('common' , [

    // 'ngAnimate',
    // 'ngSanitize',
    // 'ngFileUpload',
    // 'naif.base64',
    // 'ngMessages',
    // 'ui.grid',
    'ui.bootstrap',
    // 'ui.grid.selection',
    // 'ui.grid.exporter',
    // 'ui.grid.autoResize', 
    // 'ui.grid.pagination',
    'ui.router',
    // 'checklist-model',    
    // 'isteven-multi-select',
    // 'MessageCenterModule',
    // 'ngTable',
    // 'ckeditor'

    ])
})();
