<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		HomepageWidgetDisplay.cfm
Author:
Date created:

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
11/-1/2012		RMB		P-LEN024 - CR066 - Added div to widget display to alow scolling content.
11/11/2014		SB		Removed all tables.
--->

<cfif not thisTag.hasEndTag>
   <cfabort showError="The HomepageWidgetDisplay tag requires an end tag.">
</cfif>

<cfparam name="attributes.headerText" default="">
<cfparam name="attributes.moreLink" default="">
<cfparam name="attributes.iconIMG" default="">
<cfparam name="attributes.width" default="400">

<cfif thisTag.executionMode is "Start">
	<cfoutput>
	<div class="HPW_TopTable">
		<h2>
			<div class="fa fa-industry"></div>
			#htmleditformat(attributes.headerText)#
			<cfif attributes.moreLink neq "">
				<div class="HPW_MoreLinkTD fa fa-arrow-right">
					<a href="#attributes.moreLink#" class="HPW_MoreLink">phr_more</a>
				</div>
			</cfif>
		</h2>
		<div id="HPW_TopTableContentContainer">
		<!--- <cfif attributes.iconIMG neq ""><div class="HPW_ContentIconTD"><img src="#attributes.iconIMG#" class="HPW_ContentIconIMG"></div></cfif> --->
			<div style="overflow-y: auto; overflow-x: hidden;" name="ScrollDiv">
	</cfoutput>
</cfif>
<cfif thisTag.executionMode is "End">
	<cfoutput>
			</div>

	</cfoutput>
</cfif>
</div>
