<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	Author: SWJ
	Date: 	01 March 2002
	Purpose: To catch and report missing templates when using CFINCLUDE
	
	Usage: <CF_RelayInclude template="">

 --->

<cftry>
	<cfinclude template = "#attributes.template#">
		<cfcatch type="MissingInclude">
			<cfoutput>
				<TABLE BORDER="2" CELLPADDING="3">
					<TR>
						<TD>
							<h2>CFINCLUDE information</h2>
							<CFIF isDefined("SCRIPT_NAME")>Called from: #htmleditformat(CGI.SCRIPT_NAME)#?#htmleditformat(request.query_string)#<br></CFIF>
							Cannot find template #htmleditformat(cfcatch.missingFileName)#.<BR>
							<CFIF isDefined("CGI.HTTP_REFERER")>Referrer: #htmleditformat(CGI.HTTP_REFERER)#</CFIF>
						</TD>
					</TR>
				</TABLE>
			</cfoutput>
		</cfcatch>
</cftry>	
