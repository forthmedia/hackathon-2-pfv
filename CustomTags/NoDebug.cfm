<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CF_forceNoDebug
WAB 2011/10/18
After various performance problems caused by debug being on, created this very simple tag.
It tests whether debug is ON and if it is ON reloads the page with _cf_nodebug=true on the URL string
This prevents debug information BOTH being displayed AND stored in memory.
Note:  Will not work properly if the page has received a POST - since I only pass on the query_String
	   Will not work properly if called after a CF_setting showdebugoutput = false (because isDebugOn() returns false but debug information may still be being stored during the request)

Note that if debug is required (for debugging!) then use _cf_noDebug=false
 --->

<cfif application.com.globalFunctions.isDebugOn() and not StructKeyExists(url,"_cf_noDebug")>
	<cflocation url="//#request.currentSite.domainandRoot#/#cgi.script_name#?_cf_noDebug=true&#cgi.query_string#" addtoken=false>
</cfif>
