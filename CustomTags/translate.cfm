<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
FNL_translate.cfm

Purpose:
	To take a text within the tags and convert any instances of
	phr_abcdxyz   or phr_absbs_element_xx_yy
	into phrases of required language
	
Parameters
	Doesn't need any - will bring back translation in appropriate language
	can force language and  country with

	language = languageid
	countryID = countryid
	defaultLanguage = defaultlanguageid

	doAnalysis = Yes/No outputs an analysis of phrases translated
	trimPhrase = int  (trims phrases to a certain length  - used for analysis)
	
	LogRequests  (false)  all translations requested  (false can be over riden by session.LogTranslationRequests)
	appIdentifier     identifier of app for logging  (more usually set from session.LogTranslationAppIdentifier)
	pageIdentifier    identifier of page  for logging

	
Usage:Put tags around any document to be translated


Mods:
2001-04-11		WAB made sure that if phrases appear more than once in a page then only get into translation table once
			also added scope "ALL" to the replaces
			These changes has been done before, but must have been a version problem!
			
2001-05-18		WAB Added code to track all translations which have been requested using session.LogTranslationRequests
2001-06-25		WAB Added check for session variable session.translation.showtranslationlink
2001-07-04		WAB Changed trim so that HTML tags are removed by RE, so no risk of unclosed tags left lying around
2001-07-04		WAB Altered so that if no phr in the text then doesn't bother try to call the stored procedure or do replacements
2001-07-04		WAB altered code to track translations to use form.LogTranslationRequests and form.LogTranslationAppIdentifier as well - useful for unattended running (eg put in an application.cfm somewhere)
2001-07-05		WAB added phraseTextID to the analysis crosstab			
2001-07-11		WAB added CFSilent (which required extraContent variable for those cases when need to bypass cfsilent
2001-07-12		WAB added use of variable form.LogTranslationPageIdentifier
2001-07-13		WAB added can pass in a list of phrases in variable phrases, returns them as variables
June/July 2004  WAB made large change so that phrases are stored in memory.
Basic idea is:
Use the existing stored procedure to find the appropriate phrase for a given language/country combination
Two structures are created.  
One structure holds the phrase text and is keyed on the actual language/country combination
The other structure is keyed on the language/country requested and holds a pointer to the best fit phrase in the other 
This saves repeating phrases lots of times
2004-06-09		WAB  the doAnalysis code was updated so that it worked with the memory resident phrases.  
			New stored procedure translatePhrases_analysis was created so that analysis could be done without running the code to recalculate all the phrases and load them into memory

2004-10-12		WAB added a test for cf_translate nested inside another cf_translate.  Only processes on the outside tag unless overwritten by attributes.processEvenIfNested
			WAB put the cfsilent back in.  I now rememeber that it is required for CFMAIL - otherwise get lots of blank lines
2004-11-02		WAB sorted out problem with where phrases is at very end of content - added |$ into some regexes.  
2005-01-10  	WAB changed so that language and country information come from relayCurrentUser Structure
2005-09-07	WAB When tag is passed a list of phrases, the code is supposed to not worry whether they have phr_ appended to them or not.  Turned out that this wasn't working, so corrected 
2005-09-21	WAB corrected bug which was adding a space to the end of translations when setting variables  (space was just before #iif
2005-10-19	WAB moved code which evaluates variables in phrases into a separate function and added support for [[ ]] notation
2006-02-09	WAB added encode attribute which allows the phrase to be encoded for eg javascript  (currently only javascript has been programmed and just escapes ' to \')
2006-02-20	WAB	record whether a phrase requires evaluating so that don't need to check every time.  Not that phrase structure needs to be blown at release time - relies on com.relaytranslations
2006-06-05	WAB when translating a list of phrases, now also returns a request structure, request.translations[phrasetextid] with the phrases in it.  
2006-12-05 	WAB check for attributes.countryid and attributes.language being blank
2007-01-17 	WAB made the analysis function work again (was inside a cfsilent and needed to use the 'extracontent' variable
2007-02-13	WAB having done the above, no longer want to have doAnalysis to default to true when translation links are on.
2007-05-08	WAB added chr(10) by itself to javascript encoding
2007-06-26	WAB added mergeStruct
2009/02/27  NJH Bug Fix Sophos RelayWare Set Up Issue 1873 - extended phraseTextID in temp table to be 100 rather than 50
2009/03		WAB V3 created.   Was then lost in a disc crash and recreated from my memory!
2009-04-02 	NYB	Sophos - needed to increase the size of the PhrasetextID to stop error in Approvals Process 
				change temptable creation to be based on PhraseList table instead of having hardcoded values to permanently ensure future continuity with the db 
NYB 2009-07-21 Sophos LHID2462 - added additional criteria to cfif in phrase checking area.  
				Search for entitytype in EntityTypeID struct, and then confirm it doesn't exist as a PhraseTextID before treating 
				like a non-standard phrase (ie, with EntityID > 0).
				NB: Initial sites receiving this should be tested for performance issues on browsing external pages - as results inconclusive on dev
2011/02/08	WAB Fixed a rather major bug! Main effect was in only emails though!
				Original parameter to <cf_translate> was language (no ID suffix)
				All the new cfc code used languageID and I forgot that I would need to do something when passing all the attributes into the cfc functions
				Now backwards and forwards compatible
2012-10-03	WAB knock on from CASE 431031 changed attribute dontSendEvaluationErrorWarningEmail to more understandable logMergeErrors (and NOT'ed)								
2014-11-25	WAB	CASE 442080 - reinstate translation Ts and logging.  Removed some blocks of code and replaced with functions in relayTranslations
2016-02-24 	WAB Added support for request.relayCurrentUser.content.doNotTranslate
2016-11-29	WAB	PROD2016-2837 doAnalysis/phrase scraping not working
 ---><cfset parentTags = listrest(getbasetaglist())><!--- Note that listrest(getbasetaglist()) must be the first item (atleast not within any tags in this template ---><CFIF NOT thisTag.HasEndTag ><cfoutput>You have not specified an end tag for CF_TRANSLATE</cfoutput><CF_ABORT><cfexit method="EXITTAG"></cfif><cfsilent><cfsetting ENABLECFOUTPUTONLY="Yes"><!--- has to be silent if used within cfmail and needs to be on first line --->

<cfset extraContentHTML = "">

<!--- 
 
 This first line checks for:  
1: start tag and attributes.phrases not defined  (i.e. cf_translate being used in usual way)  don't do anything, content gets processed by end tag
2: end tag and attributes.phrases defined  (i.e. a list of phrases has been passed) so don't do anything - content gets processed by opening tag
3: cf_translate is nested inside other cf_translates so don't process unless specifically asked to, or we are translating a list of phrases

(WAB: I think that it is all on one line to prevent any odd whitespace, but I can't actually remember and I am not sure why it doesn't use enableCFoutputOnly)
  ---> 

<!--- by default don't process if tag is nested.  However must process if in a cfmail --->
<cfparam name="attributes.processEvenIfNested" default="true"><!--- if set to true here, then this functionality has been disabled - doesn't work when I have a stub for cf_translate and then cfinclude the correct version, 'cause cfinclude is always the last tag--->
<cfif parenttags contains "CFMAIL" or parenttags contains "cf_executeRelayTAG">
	<cfset attributes.processEvenIfNested = true>
</cfif>
  
<cfif 	(thistag.executionMode is "start" and not isdefined("attributes.phrases")) 
  		or (thistag.executionMode is "end" and isdefined("attributes.phrases")) 
		or (listfindnocase(parentTags,"CF_TRANSLATE") is not 0 and not attributes.processEvenIfNested and not isdefined("attributes.phrases"))>
<cfelse><!--- ie end tag --->
	<cfparam name="attributes.phrases" default="">
	<cfparam name="attributes.DefaultLanguage" default="1">     <!--- 1 is English --->
	<cfparam name="attributes.language" type="string" default = "">		
	<cfparam name="attributes.languageID" type="string" default = "#attributes.language#">		<!--- WAB 2011/02/08 see comment at top  --->
	<cfparam name="attributes.countryID" type="string" default = "">		
	<cfparam name="attributes.exactMatch" default="false">
	<cfparam name="attributes.showTranslationLink" default="#application.com.relayTranslations.showTranslationLinks()#">     
	<CFPARAM name="attributes.evaluateVariables" default= "true">
	<CFPARAM name="attributes.executeRelaytags" default= "true">
	<CFPARAM name="attributes.debug" default= "false">
	<cfparam name="attributes.nullText" default="">
	<cfparam name="attributes.onNullShowPhraseTextID" default="yes">
	<cfparam name="attributes.logrequests" default = "#application.com.relayTranslations.isLogRequestsSet()#">
	<cfparam name="attributes.appIdentifier" default = "#application.com.relayTranslations.LogRequestsAppIdentifier()#">		
	<cfparam name="attributes.pageIdentifier" default="">
	<cfparam name="attributes.entityType" default="">	
	<cfparam name="attributes.entityID" default="">		
	<cfparam name="attributes.forceReload" default="false">
	<cfparam name="attributes.translationLinkCharacter" default="T">
	<cfparam name="attributes.encode" default="">
	<cfparam name="attributes.mergeStruct" default="#structNew()#">   <!--- a structure of things which can be merged into a phrase, referenced as [[merge.x]] --->
	<cfparam name="attributes.logMergeErrors" default = "true">
	<cfparam name="attributes.useMemoryResident" default="true">
	<CFPARAM name="attributes.doAnalysis" default= "No">


	<cfscript>
	if (isdefined("attributes.phrases")) 
		{
			createVariables = true ;
		}else{
			createVariables = false ;
		}	

	</cfscript>	

	<cfif not isdefined("application.com")>
		<!--- running somewhere that doesn't have com objects and stuff, do nothing
			actually if attributes.phrases is not "" need to return request.translations with the correct keys in it
		 --->
		 <cfif attributes.phrases is not "">
			<cfparam name="request.translations" default = "#structNew()#">
			<!--- need to loop through here creating the correct fake structure --->
		</cfif>	

	<cfelseif attributes.phrases is not "">
		<cfparam name="request.translations" default = "#structNew()#">
		<cfset attributes.phrases = replaceNoCase (attributes.phrases,"phr_","","ALL")>
		<cfset attributes.phrases = replaceNoCase (attributes.phrases," ","","ALL")>  <!--- remove any spurious spaces in the list (V2 did this accidentally so this keeps backwards compatible) --->
		<cfset TranslationResult = application.com.relaytranslations.translateListOfPhrases (listOfphrases = attributes.phrases,argumentCollection = attributes)>
		<cfloop collection="#TranslationResult.phrases#" item="identifier">

			<cfset request.translations[identifier] = TranslationResult.phrases[identifier].phraseText>
<!--- 				<cfset request.translations[identifier] = "#thisphraseText##iif(showTranslationLink,DE(" #thistranslationLink#>#attributes.translationLinkCharacter#</A>"),DE(""))#"> --->
				<cfset request.translations["phr_#identifier#"]  = request.translations[identifier]>
					<CFSET "caller.phr_#identifier#" = request.translations[identifier]>  					
			
			<cfif attributes.exactMatch>
				<CFSET "caller.fld_#identifier#" = TranslationResult.phrases[identifier].updateIdentifier>						
			</cfif>
			
		</cfloop>

	<cfelse>

		<cfif not structKeyExists (request,"relayCurrentUser") OR  not request.relayCurrentUser.content.doNotTranslate>
			<cfset attributes.string = thisTag.generatedContent>  <!--- note, I pass this in the attribute collection rather than as an individual variable because otherwise the content can appear in debug which causes mayhem if it happens to have JS in it --->
			<!--- 2014-11-25	WAB	CASE 442080 if no encoding specified we will do autoencoding 
				2016-11-29	WAB	PROD2016-2837 doAnalysis does not work in conjunction with translateStringWithAutoEncode 
			--->
			<cfif attributes.encode is "" and not attributes.doAnalysis>
				<cfset translationResult = application.com.relaytranslations.translateStringWithAutoEncode (argumentCollection = attributes )>
			<cfelse>
				<cfset translationResultStructure = application.com.relaytranslations.translateStringComplicated (argumentCollection = attributes )>
				<cfset translationResult = translationResultStructure.string>
			</cfif>
			
			<cfset thisTag.GeneratedContent = translationResult>	 
			<!--- <cfset extraContentHTML = extraContentHTML & translationResult.debug> --->
		</cfif>

	</cfif>


		<!--- Output analysis crosstab table --->
		<cfif attributes.doAnalysis >   
			<cfsaveContent variable = "analysisHTML">
			<CF_Translate showTranslationLink="true" doAnalysis = "false" trimPhrase="30" LogRequests="false" processEvenIfNested=true>
				<cfif translationResultStructure.queries.data.recordcount is not 0>
					<CF_FNLCrossTabV2
						columnHeadingQuery = #translationResultStructure.queries.headings#
						dataQuery = #translationResultStructure.queries.data#
						ColumnIDField = "LanguageANDcountry"
						ColumnHeadingEval = "'##Language##<BR>##country##'"
						RowHeadingEval = "'phr_##Identifier##<BR><FONT size=1>(##Identifier##)</font> (##phraseid##)'"
						valueEval = "'x'"
					>
				</cfif>			
			</CF_Translate>
			</cfsavecontent>
			<cfset extraContentHTML = extraContentHTML & analysisHTML>
			<cfset request.translationAnalysis.data = translationResultStructure.queries.data>
			
		</cfif>	
		
	
		<!--- Load translation JS if necessary --->
		<cfif attributes.showTranslationLink or attributes.doAnalysis >
			<cf_IncludeJavascriptOnce template = "/javascript/doTranslation.js">
		</cfif>

</cfif>


</cfsilent><cfoutput>#extraContentHTML#</cfoutput><cfsetting ENABLECFOUTPUTONLY="No"><!--- DO NOT PUT ANY EXTRA LINE BREAKS AT THE END OF THIS FILE --->