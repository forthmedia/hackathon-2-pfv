<!--- �Relayware. All Rights Reserved 2014 --->
<!---
<CF_FormFieldsURLString>

David McLean 26 Feb 2002

Takes the caller's form fields if any exist and returns them 
in one string in the caller scope called URLString that can be
fed into a URL Query String
--->


<CFIF IsDefined("caller.FIELDNAMES")>
	<CFSET FIELDNAMES = caller.FIELDNAMES>

	<CFSET Fields="">
	<CFLOOP LIST="#FIELDNAMES#" INDEX="x" DELIMITERS=",">
		<CFSET Fields = Fields & x & "=" & URLEncodedFormat(evaluate("#x#")) & "&">
	</CFLOOP>
	<CFIF IsDefined("attributes.AdditionalVariables")>
		<CFLOOP LIST="#attributes.AdditionalVariables#" INDEX="x" DELIMITERS=",">
			<CFIF IsDefined("caller.#x#")>
				<CFSET Fields = Fields & x & "=" & URLEncodedFormat(evaluate("caller.#x#")) & "&">
			</CFIF>
		</CFLOOP>
	</CFIF>
	
	
	<CFSET caller.URLString=Fields>
<CFELSE>
	<CFSET caller.URLString="">
</CFIF>

