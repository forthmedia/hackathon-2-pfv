<!--- �Relayware. All Rights Reserved 2014 --->
<style>
.reportheader {
font-family:arial;
font-weight:bold;
font-size:16px;
background:8888ff;color:000000
}
.reportcolheader{
font-family:arial;
font-weight:bold;
font-size:12px;
background:aaaaff;
color:000000
}
.row2{
background:ccccff;font-family:arial;
font-weight:normal;
font-size:12px;
}

.row1{
background:ddddff;font-family:arial;
font-weight:normal;
font-size:12px;
}
.reporttext{
font-family:arial;
font-weight:normal;
font-size:12px;
}
</style>
<cfsetting enablecfoutputonly = "yes">
<cfset errors="">
<!--- limit output to that within cfoutput tags only --->
<!--- use attributes.xxx for called variables --->
<!--- set default values
If query is null then we are using this for output purposes only. No data is involved.
Fieldlist: The columns as they appear in the database query.
	Default column list: all columns, as taken from the query columnlist value
FieldTitleList: The columns as they are to appear in headers -order corresponds to order in FieldList
	Default column headings: column names with underscores replaced as spaces
	
Example usage:
	<cf_report
	query="getServerInfo"
	fieldlist="ServerName,FailureStartDate,FailureEndDate,PollCount,LastPolled"
	fieldtitlelist="Server Name,Failure Start Date,Failure End Date,Poll Count,Last Polled"
	fieldsumlist="0,0,0,0,0"
	maxrows=20
	root=""
	backimg="back.gif"
	pagenext="pagenext.gif"
	pageprev="pageprev.gif"
	title="Last 3 days server failures"
	info="Extra info listed here..."
	border=1
	cellspacing=2
	cellpadding=2
	width="100%"
	stepsback=1
	lightrow="row1"
	darkrow="row2"
	header="reportheader"
	colheader="reportcolheader"
	text="text"
>

--->
<cfparam name="attributes.query" default="null">
<cfif attributes.query neq "null">
<cfparam name="attributes.fieldlist" default="#evaluate("##caller.#attributes.query#.columnlist##")#">
<cfparam name="attributes.fieldtitlelist" default="#replace(attributes.fieldlist,"_"," ","all")#">
<cfparam name="attributes.fieldsumlist" default="0">
<cfparam name="attributes.fieldfilterlist" default="0">
<cfparam name="attributes.fieldsumlist" default="0">
<cfparam name="attributes.fieldformatlist" default="0">
<cfparam name="attributes.fieldlinklist" default="">
<cfparam name="attributes.fieldurllist" default="">
<Cfparam name="attributes.fieldlinkurl" default="">
<cfparam name="attributes.fieldlinkdata" default="">
</cfif>

<!--- attributes.maxrows shows the maximum number of rows of data to display per page --->
<cfparam name="attributes.lightrow" default="row1">
<cfparam name="attributes.darkrow" default="row2">
<cfparam name="attributes.header" default="reportheader">
<cfparam name="attributes.colheader" default="reportcolheader">
<cfparam name="attributes.text" default="reporttext">
<cfparam name="attributes.maxrows" default="100">
<cfif not isnumeric(attributes.maxrows)>
<cfset attributes.maxrows=50>
<cfset errors=errors&"A non numeric value was entered for Maxrows, 50 has been used">
</cfif>
<!--- define basic report parameters --->
<!--- relative filename of images --->
<cfparam name="attributes.root" default="">
<cfparam name="attributes.backimg" default="back.gif">
<cfparam name="attributes.pagenext" default="pagenext.gif">
<cfparam name="attributes.pageprev" default="pageprev.gif">
<!--- title, display on the header bar of the report. --->
<cfparam name="attributes.title" default="Untitled Report">
<!--- Additional text to display in header section --->
<cfparam name="attributes.info" default="">
<!---table appearance--->
<cfparam name="attributes.border" default="1">
<cfparam name="attributes.cellspacing" default="0">
<cfparam name="attributes.cellpadding" default="3">
<!--- set default table width --->
<cfparam name="attributes.width" default="100%">
<!--- define start and end rows of data to display --->
<CFPARAM name="caller.url.startrow" default="1">
<CFPARAM name="caller.url.endrow" default="1">
<CFPARAM name="caller.form.startrow" default="0">
<CFPARAM name="caller.form.endrow" default="0">

<!--- define number of steps back the back button goes when clicked (uses javascript:history.go(-#stepsback#)--->
<cfparam name="attributes.stepsback" default="1">

<!--- now we set local variables from our attributes.values --->
<cfif attributes.query neq "null">
<cfset query="caller."&attributes.query>
<cfelse>
<cfset query="null">
</cfif>
<!--- --->
<!--- Begin info--->
<!--- --->
<cfif query neq "null">
<cfset fieldlist=attributes.fieldlist>
<cfset fieldtitlelist=attributes.fieldtitlelist>
<cfset fieldsumlist=attributes.fieldsumlist>
</cfif>
<cfset maxrows=attributes.maxrows>
<cfset backimg=attributes.backimg>
<cfset pagenext=attributes.pagenext>
<cfset pageprev=attributes.pageprev>
<cfset title=attributes.title>
<cfset info=attributes.info>
<cfset border=attributes.border>
<cfset cellspacing=attributes.cellspacing>
<cfset cellpadding=attributes.cellpadding>
<cfset width=attributes.width>
<cfset startrow=caller.url.startrow>
<cfset endrow=caller.url.endrow>
<cfif caller.form.startrow gt 0>
<cfset startrow=caller.form.startrow>
<cfset endrow=caller.form.endrow>
</cfif>
<cfset stepsback=attributes.stepsback>


<cfif query neq "null">
	<!--- set recordcount of the query --->
	<cfset recordcount = evaluate("###query#.recordcount##")>
	<!--- set range of data to display for this page --->
	<CFSET endrow=startrow+maxrows>
	<!--- check to see if potential last row exceeds number of records in query --->
	<CFIF endrow gt recordcount>
			<!--- if it does, set the end row to be the LASt row --->
			<CFSET endrow=recordcount>
	</CFIF>
<cfelse>
	<cfset recordcount=0>
</cfif>
<!--- if output is "scr" i.e. screen, create as table formatted data. we may in the future add various other output types i.e. .csv, .tab, XML etc etc --->

<cfsetting enablecfoutputonly="no">
<!---create header bar containing back button, title, recordcounts, next & previous buttons for data, extra info.....--->
<CFOUTPUT>
<TABLE width="100%" border="0" cellspacing=0 cellpadding=3>
	<TR>
			<TD width="25" align=center >
				<A href="javascript:history.go(-#stepsback#);">
					<IMG src="#attributes.root##backimg#" alt="" border="0">
				</A>
			</TD>
			<TD colspan="4" class="#attributes.header#">
				<B>#htmleditformat(title)#</B>
			</TD>
	</TR>
	<TR>
	<td></td>
		<TD colspan="2" class="#attributes.text#"><!---display number of records, if appropriate --->
		<CFIF IsDefined("#query#.RecordCount") and query is not "null">		
				<CFIF RecordCount EQ 0>No records were
				<CFELSEIF RecordCount EQ 1>1 record was
				<CFELSE>#htmleditformat(RecordCount)# records were
				</CFIF>
			found for the selected criteria.
		</CFIF>
		#htmleditformat(info)#
		</TD>
	</tr>
<!--- show which record rows are being listed --->
	<CFIF recordcount gt 1 >
		<TR>
			<td></td>
			<TD colspan="2">
				<table border=0>

					<td class="#attributes.text#">
<cfset thispage=ceiling(startrow/maxrows)>
<!--- remove current startrow from query string, as we will be appending a new start row for links --->
<cfset useurl=rereplace(request.query_string,'&startrow=[0-9][0-9][0-9]','','all')>
Displaying Page</td>
<td width=12>
<cfif startrow gt 1>
<cfset new=startrow-maxrows>
<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#&startrow=#new#"><img src="#attributes.root##pageprev#" alt="View previous page of data" border=0></a>
</cfif>
</td>

<form action="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#" method="post" name="navform">
<td>
<select name="startrow" onchange="document.navform.submit()">
<cfset count=0>
<cfloop index="page" from="1" to="#recordcount#" step="#maxrows#"><cfset count=count+1>
<option value=#page# <cfif count eq thispage>selected</cfif>>#count#
</cfloop>
</select>
</td>
<td width=12>
<cfif endrow lt recordcount>
<cfset new=startrow+maxrows>
<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#&startrow=#new#"><img src="#attributes.root##pagenext#" alt="View next page of data" border=0></a>
</cfif>
</td>

</form>
<td class="#attributes.text#">
 (Records #htmleditformat(startrow)# - #htmleditformat(endrow)#)
					</td>
					<td>&nbsp; &nbsp;</td>
</table>

				</TD>

			</TR>

		</CFIF>

</TABLE>

</CFOUTPUT>
<!--- --->
<!--- End info--->
<!--- --->
<cfif query neq "null">
<!--- --->
<!--- Begin header--->
<!--- --->

<cfset go=1>
<cfif isdefined("#query#.recordcount")>
<cfif evaluate("###query#.recordcount##") eq 0>
<cfset go=0>
</cfif>
</cfif>
<cfif go eq 1>
<cfoutput>
<table width="#width#" border="#border#" cellspacing=#attributes.cellspacing# cellpadding=#attributes.cellpadding# rules="cols">
	<TR></cfoutput>
		<CFSET count=0>
		<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">

				<CFSET count=count+1>
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
<cftry>
<cfset tmp=evaluate("###query#.#thiscolfield###")>
<CFOUTPUT><TD class="#attributes.colheader#" valign="top">#htmleditformat(thiscol)#&nbsp;</TD></CFOUTPUT>
<cfcatch>
<cfset errors = errors &"<br>An invalid column name (#thiscolfield#) was entered">
<CFOUTPUT><TD class="reportcolheader" valign="top">Invalid Column</TD></CFOUTPUT>
</cfcatch>
</cftry>


		</CFLOOP>

<CFOUTPUT></TR></cfoutput>
</cfif>


<!--- --->
<!--- End header--->
<!--- --->

<!--- --->
<!--- Begin Output--->
<!--- --->
<cfsetting enablecfoutputonly = "no" >

<!--- assuming we have a query.....process the data --->
<cfif query neq "null">
<cfif evaluate("###query#.recordcount##") gt 0>
<!--- loop through specified rows of data --->
<cfloop query="#query#" startrow="#startrow#" endrow="#endrow#">
<!--- change color of alternate rows --->
<cfif evaluate("###query#.currentrow##") Mod 2>
<cfset class="#attributes.lightrow#">
<cfelse>
<cfset class="#attributes.darkrow#">
</cfif>
<!--- set current col count --->
		<CFSET count=0>
<!--- loop over the list of fields, to create columnsfor this row --->
		<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">

				<CFSET count=count+1>
				<!--- set field containing value --->
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
				<CFSET thissumfield=gettoken(fieldlist,count,",")>


<cfif len(trim(thiscolfield)) gt 0>
<!--- get the value of this col --->
<cftry>
<cfset value=evaluate("###query#.#thiscolfield###")>
<cfset valid=1>
<cfcatch>
<cfset value="Invalid Column">
<cfset valid=0>
</cfcatch>
</cftry>

<cfelse>
<cfset value="">
<cfset valid=1>
</cfif>

<cfset align="">
<cfif isnumeric(value)>
<!--- if the value is numeric, right justify this cell --->
<cfset align="right">
</cfif>				

<CFOUTPUT><TD class="#class#" valign="top" align=#align#></CFOUTPUT>
<CFSET VALUE=TRIM(VALUE)>
<!--- replace special url chars so Netscape can cope, the bag of shite !
--->
<cfset urlvalue=replace(value," ","%20","all")>
<!--- [insert drumroll here] output the value !!!--->

<cfif gettoken(fieldsumlist,count,",") eq 1>
<!--- if it is a summable field, check for the existance of the variable holding its current total. If this doesnt exist, create it. Replace out special chars in variable name.
--->
<cfset value=numberformat(value,"__________.00")>
<cfset sumvar="sum_#thissumfield#">
<cfset sumvar = rereplace(sumvar,'[[:punct::]]','','all')>
<cfif isdefined(sumvar)>
<cfset "#sumvar#"=val(evaluate("#sumvar#"))+val(value)>
<Cfelse>
<cfset "#sumvar#"=val(value)>
</cfif>
</cfif>
<CFOUTPUT>#trim(value)#</a></CFOUTPUT>
<cfif len(trim(value)) eq 0>
<!---chuck in a hard space so Netscape can parse the table correctly --->
<CFOUTPUT>&nbsp;</CFOUTPUT>
</cfif>
<CFOUTPUT></TD></CFOUTPUT>
		</CFLOOP>

<CFOUTPUT></TR></CFOUTPUT>
</cfloop>
</cfif>
</cfif>

<!--- --->
<!--- End Output--->
<!--- --->

<!--- --->
<!--- Begin Footer--->
<!--- --->

<cfif evaluate("###query#.recordcount##") gt 0>

	<cfoutput><TR></cfoutput>
		<CFSET count=0>
		<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">

				<CFSET count=count+1>
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
				<CFSET thissumfield=gettoken(fieldlist,count,",")>
			<CFOUTPUT>
				<TD class="reportcolheader" valign="top" align=right>
				<cfset sumvar="sum_#thissumfield#">
<cfset sumvar = rereplace(sumvar,'[[:punct::]]','','all')>
				<cfif isdefined(sumvar)><cfset sum=evaluate("#sumvar#")>#numberformat(sum,"__________.00")#
<cfelse>&nbsp;</cfif></TD></CFOUTPUT>
		</CFLOOP>
	</TR>



</cfif>

</table></table>
</table>
</cfif>
<cfsetting enablecfoutputonly="no">
<cfif len(errors) gt 0>
<b>Some errors were encountered while generating this report.</b>
<br>
<cfoutput>#htmleditformat(errors)#</cfoutput>
</cfif>


