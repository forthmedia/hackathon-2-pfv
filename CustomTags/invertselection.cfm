<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			invertselection.cfm
Author:			DJH
Date created:	25 January 2000

Description:

Implemented as a custom tag, this template allows a user to select
checkboxes not currently selected in a group of checkboxes and vice versa - i.e. 
performs a logical NOT on the values. When included inside a form tag in a cold fusion template,
either an anchor tag or a button will be included which when clicked will perform the inversion.

Usage:

<cf_invertselection	form="theformname"
					checkbox="checkboxname"
					displayas="ANCHOR|BUTTON"
					select="selectname"

theformname is the name of the form the checkboxes are contained in.
checkbox is the name given to the group of checkbox elements
displayas is a choice between ANCHOR or BUTTON, and decides how to output the inverter
select is the name of the select field if using on a select

Version history:
1
2 - added ability to select from a select box.
--->

<!--- default values --->
<cfparam name="caller.calledalready" default="no">
<cfparam name="attributes.form" default="">
<cfparam name="attributes.checkbox" default="">
<cfparam name="attributes.displayas" default="anchor">
<cfparam name="attributes.select" default="">

<!--- abort if the minimum amount of information has not been filled in --->

<cfif attributes.checkbox is "" and attributes.select is "">
	<!--- must have the checkbox name --->
	<cfexit>
<cfelseif attributes.displayas is "anchor" and attributes.form is "">
	<!--- whilst form is not needed if its a button, it is needed if its an anchor --->
	<cfexit>
</cfif>

<!--- first, insert the javascript function into the head of this template
if this custom tag has already been used once or more in the page, theres no need to re-include 
the javascript function in the header...
 --->
<cfif not caller.calledalready>
	<cfif attributes.checkbox is not "">
		<cfhtmlhead text="
		<script language=""JavaScript"">
		// function to invert the selection of a set of checkboxes
		function invertSelection(checkbox)
		{			
			var ctr
			for (ctr=0; ctr < checkbox.length; ctr++)
			{
				checkbox[ctr].checked = !checkbox[ctr].checked
			}
		}
		</script>
		">
	<cfelse>
		<cfhtmlhead text="
		<script language=""JavaScript"">
		// function to invert the selection of a set of checkboxes
		function invertSelection(aselect)
		{
			var ctr
			for (ctr=0; ctr < aselect.length; ctr++)
			{
				aselect.options[ctr].selected = !aselect.options[ctr].selected
			}
		}
		</script>
		">
	</cfif>
	<cfset caller.calledalready="yes">
</cfif>

<!--- now, create the button or anchor. --->

<cfif attributes.select is not "" and attributes.checkbox is "">
	<cfset attributes.checkbox = attributes.select>
</cfif>

<cfif attributes.displayas is "anchor">	
	<cfoutput>
		<a href="javascript:void(invertSelection(document.#attributes.form#.#attributes.checkbox#))">Invert Selection</a>
	</cfoutput>
<cfelse>
	<cfoutput>
		<CF_INPUT type="button" value="Invert selection" onclick="invertSelection(this.form.#attributes.checkbox#)">
	</cfoutput>
</cfif>
