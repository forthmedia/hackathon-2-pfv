<!--- �Relayware. All Rights Reserved 2014 --->
<CFSILENT>
<!--- generic Template to  get a record from the person, location or organisation tables
	also generates default queries for new records (set entityID non numeric)
--->

<!---  
parameters
	entityType
	entityID
	getParents (optional) will get all the parent records as well (eg ask for person, get location and organisation as well)
	
default values can be set in the calling template in the form tableName_fieldName_Default = xxx  (eg person_salutation_default = "Mr"	)

When new person, a locationid is required.  Will pick up attributes.locationid 
When new location, a countryid is required.  Will pick up attributes.countryid  
a countryID is required  for adding new locations or people.  Can either be passed as countryID, or will be picked up as 


Date : May 2000 - still in development



--->


<!--- 
returns
	query named "#entityType#"

 --->
 
<CFPARAM name="attributes.entityid">
<CFPARAM name="attributes.entityType">
<CFPARAM name="attributes.getRights" default = "false">

<CFSET entityType = attributes.entityType>
<CFSET entityID = attributes.entityID>
 
<CFINCLUDE template = "getRecordSub.cfm">


<CFIF isDefined("attributes.getRights")>
	<CFIF isNUmeric(attributes.entityid)>
		<!--- check rights to this record --->
		<!--- slight duplication of effort here because getrecordRights also give a record containing the name of the entity!! --->
		<CFSET entityType="#attributes.entityType#">
		<CFSET entityID = "#attributes.entityid#">
		<cfinclude template="getRecordRightsSub.cfm">
		<!--- returns query rights. with the rights and  query entity. with the current record --->
	<CFELSE>
		<CFSET rights = queryNew("updateOkay,AddOkay,Level2,Level3,Level4")>
		<CFSET x = queryAddRow (rights)>
		<CFSET x = querysetCell (rights,"addokay",1)>
	</CFIF>	
	<CFSET caller.rights = rights>
</cfif>
	
</CFSILENT>
