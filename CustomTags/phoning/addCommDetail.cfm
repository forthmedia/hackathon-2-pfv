<!--- �Relayware. All Rights Reserved 2014 --->
<CFSILENT>
<!--- 
File name:		addCommDetail.cfm
Author:			Simon WJ
Date created:	2001-10-16

	Objective - to add a commDetail record to either the adHocComms commid
				or one you specify to the tag in commid
		
	Syntax	  -	
	
	<CF_addCommDetail
	personID 	 = #personID#
	locationID   = #locationID#
	commTypeID	 = #getCommType.commTypeId#
	commReasonID = #form.frmcommReasonID#
	commStatusID = #form.frmcommStatusID#
	feedback	 = "#tmpFeedback#"
	>   
	
	NB we do not have to set a commid.  If one is not set then the commdetail 
	record is added to a record with the name 'AdHocComms'
	
	Returns - 
			caller.addedCommDetailID - the id of the newley added commdetail record
			caller.thisCommID - the id of the communication

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
2001-10-16		SWJ		First version
2002-04-26		SWJ		Added a param for comFormLID.  This may be redundant but 
						it's there any way.
2002-06-25		SWJ		Modified getLookupID to check for a record and add one if none found with itemText = 'eMail - Ad Hoc'
2005-02-22		WAB		Changes to deal with long feedback - code moved from addcallrecord.cfm to here 
2008/04/21		WAB		see comment
2008/06/30     SSS      Added Nvarchar Support
2009/05/19		WAB		removed transaction hardening, seemed to be causing locks and things on Trend
2011-01-13		MS		LID:4314 Insert SQL updated to record active campaigns line 179 & 191
2011-07-04		NYB		P-LEN024 moved splitStringIntoArray function from communications to globalFunctions
2013-08-16 		PPB		Case 436510 show commdetail.commSenderId as contactedBy	if it's available
2013-11-21		WAB		2013RoadMap2 Item 25 Feedback is now nVarchar(max) - remove code which deal twith splitting long feedback over mulitple records
2013-12-03 		WAB 	2013RoadMap2 Item 25 Contact History.  Store Subject/Body in new CommDetailContent table rather than in the feedback column

Enhancement still to do:


	
--->
</CFSILENT>
<CF_checkMandatoryParams callingTemplate="addCommDetail.cfm" relayParam="personID,locationID,commTypeID">

<CFPARAM name="attributes.personID">
<CFPARAM NAME="attributes.locationID" DEFAULT=0>
<CFPARAM NAME="attributes.commTypeID">
<CFPARAM NAME="attributes.commReasonID" DEFAULT=0>
<CFPARAM NAME="attributes.commStatusID">
<CFPARAM NAME="attributes.commID" DEFAULT="">
<CFPARAM NAME="attributes.feedback" DEFAULT="">
<CFPARAM NAME="attributes.subject" DEFAULT="">
<CFPARAM NAME="attributes.body" DEFAULT="">
<CFPARAM NAME="attributes.commFormLID" DEFAULT="8">
<CFPARAM NAME="attributes.datesent" DEFAULT="#Now()#">
<CFPARAM NAME="frmProjectRef" DEFAULT="">
<CFPARAM NAME="FreezeDate" DEFAULT="#Now()#">
<CFPARAM NAME="attributes.commCampaignID" DEFAULT=0>
<CFPARAM NAME="attributes.commSenderId" DEFAULT=0>				<!--- 2013-08-15 PPB Case 436510 --->

<!--- If the commDetail record is an Email and no communication record has been identified then get the details  
      of the AdHoc email communication record 
	WAB 2013-11-21 removed code which would add the adHocComms communication if it did not exist.  Is always in OOTB
			Still not very satisfactory!		
--->
<CFIF attributes.commId EQ "">
 	<CFQUERY NAME="checkDefaultMailComm" DATASOURCE="#application.siteDataSource#">
		select commid as DefaultCommID from communication
		where title = 'AdHocComms'
	</CFQUERY>

	<CFSET thisCommID = checkDefaultMailComm.DefaultCommID>

<CFELSE>
	<CFSET thisCommID = attributes.commId>
</CFIF>	

<CFSET caller.thisCommID = thisCommID>

<!--- WAB 2013-11-21
Feedback is now nvarchar(max) so do not need to worry about long feedback
 --->

			<!--- Case 437596 NJH 2013/11/04 - check that the commSenderID is also not an empty string --->
			<CFQUERY result = "InsertCommDetailRecordResult" NAME="InsertCommDetailRecord" DATASOURCE="#application.SiteDataSource#">
				declare @locationid int, @commDetailID int 

				select @locationid = locationid from person where personid =  <cf_queryparam value="#attributes.personid#" CFSQLTYPE="CF_SQL_INTEGER" >     <!--- had to do this at the start because wanted to use scope_identity() an CF did not seem to like it when used with "insert into select from " --->
				
				INSERT INTO commdetail (commid
				                       ,personid
									   ,locationid
									   ,CommFormLID
									   ,Feedback
									   ,DateSent
									   ,lastupdated
									   ,lastupdatedby
									   ,commtypeid
									   ,commreasonid
									   ,commstatusid
									   ,CommCampaignID
									   ,commSenderId)
				values ( <cf_queryparam value="#thisCommID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				       <cf_queryparam value="#attributes.personid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					   @locationid,
					   <cf_queryparam value="#attributes.commFormLID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					   <cf_queryparam value="#attributes.feedback#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					   <cf_queryparam value="#attributes.datesent#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
					   <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
					   #request.relayCurrentUser.userGroupID#,
					   <cf_queryparam value="#attributes.commTypeId#" CFSQLTYPE="CF_SQL_INTEGER" >,
					   <cf_queryparam value="#val(attributes.commReasonId)#" CFSQLTYPE="CF_SQL_INTEGER" >,
					   <cf_queryparam value="#val(attributes.commStatusID)#" CFSQLTYPE="CF_SQL_INTEGER" >,
					   <cf_queryparam value="#attributes.commCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					   <cfif attributes.commSenderID eq 0 or attributes.commSenderID eq "">NULL<cfelse><cf_queryparam value="#attributes.commSenderID#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>		<!--- 2013-08-15 PPB Case 436510 --->
						)

				 select @commDetailID = SCOPE_IDENTITY()

				<cfif attributes.body is not "" OR attributes.subject is not "" >
				insert into commDetailContent
				(commdetailID, subject,body)
				values (@commDetailID, <cf_queryparam value="#attributes.Subject#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#attributes.body#" cfsqltype="cf_sql_varchar">)
				</cfif>

				select @commDetailID as commdetailid
			</CFQUERY>


<!--- <CFSET caller.addedCommDetailID = getCommDetailID.commDetailID> --->
<CFSET caller.addedCommDetailID = InsertCommDetailRecord.commDetailID>





