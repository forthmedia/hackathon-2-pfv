<!--- �Relayware. All Rights Reserved 2014 --->


<CFPARAM NAME="frmPersonID" DEFAULT="2">
<CFPARAM NAME="frmCommID" DEFAULT="3382">

<CFIF isdefined("frmentityType")>

	<CFQUERY NAME="GetLocation" DATASOURCE="#application.SiteDataSource#">
	Select SiteName AS Name, locationid
	FROM Location 
	WHERE 
	<CFIF frmentityType IS "Location">
		locationId =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	<cfelseif frmentityType IS "Person">
		locationId = (Select locationid from person where personid =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )	
	</cfif>
	</CFQUERY>
	
	<CFSET locationid = getlocation.locationid>

<CFELSE>

	<CFQUERY NAME="GetLocation" DATASOURCE="#application.SiteDataSource#">
	Select SiteName AS Name 
	FROM Location 
	WHERE locationId =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

</cfif>



<!--- Query returning search results --->
<CFQUERY name="SearchResult" dataSource="#application.SiteDataSource#">
	SELECT      Flag.Name As CallResult, 
				Communication.Title, 
				FirstName+' '+LastName AS PersonName, 
				commdetail.DateSent, 
				commdetail.Feedback, 
				Communication.CommID, 
				commdetail.LocationID, 
				commdetail.lastupdatedby, 
				(select name from usergroup where usergroupid = commdetail.lastupdatedby) as agent
	FROM        Flag, Communication, {oj Person RIGHT OUTER JOIN commdetail ON Person.PersonID = commdetail.PersonID }
	WHERE       Flag.FlagID = commdetail.CommStatus AND
	                commdetail.CommID = Communication.CommID 
		   AND (commdetail.LocationID =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	ORDER BY  commdetail.DateSent DESC
</CFQUERY>


<cf_head>
    <cf_title>CommHistory - Search Result</cf_title>
	
</cf_head>


<H1>Contact History</H1>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3">
	<TR bgcolor="cccccc">
<!--- 	    <TD>&nbsp;</TD> --->
	    <TH>Campaign</TH>
		<TH>Who with</TH>
		<TH>Agent</TH>
	    <TH>Feedback/Notes</TH>
	    <TH>Date of Call</TH>
	    <TH>Call Result</TH>	
	</TR>
	<CFOUTPUT query="SearchResult">
		<TR bgcolor="#IIf(CurrentRow Mod 2, DE('ffffff'), DE('ffffcf'))#">
<!--- 			<TD><A href="CommHistory_Detail.cfm?ID=#URLEncodedFormat(CommDetailID)#">[detail]</A></TD> --->
			<TD>#htmleditformat(Title)#</TD>
			<TD>#htmleditformat(PersonName)#</TD>
			<TD>#htmleditformat(agent)#</TD>
			<TD>#htmleditformat(Feedback)#</TD>
			<TD>#htmleditformat(DateSent)#</TD>
			<TD>#htmleditformat(CallResult)#</TD>
		</TR>
	</CFOUTPUT>
		<TR><TDcolspan="5">&nbsp;</td></tr>
		<TR><TD colspan="5"><CFOUTPUT><A HREF="../phoning/addcallrecord.cfm?frmlocationid=#locationid#">Add a New Call Record</a></cfoutput></td></tr>
</TABLE>






