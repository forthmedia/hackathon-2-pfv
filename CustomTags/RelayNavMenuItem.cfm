<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NAME: <CF_RelayNavMenuItem>

NOTES:
 Part of the <CF_RelayNavMenu> tag, see RelayNavMenu.CFM for details.

AUTHOR:
 Simon WJ 2001-10-28
 
 WAB 2005-07-22 added description attribute
 
--->

<!--- Associate this tag with <CF_RelayNavMenu> --->
<CFASSOCIATE BASETAG="CF_RelayNavMenu" DATACOLLECTION="menuItems">
<CFPARAM NAME="attributes.thisDir" TYPE="string" DEFAULT="">
<CFPARAM NAME="attributes.target" DEFAULT="">
<CFPARAM NAME="attributes.description" DEFAULT="">