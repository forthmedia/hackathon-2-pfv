<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	this tag will manage a select box's onchange handler in order to stop the mousewheel from activating it.
	call this tag AFTER the select box who's onchange you want managed.
	the params to pass are:
		selectBoxName - name of your selectbox
		formName - name of the form your select box is in
		onChangeFunction - string of the js function (with params) you want to call onchange of your select box. e.g. onChangeFunction="doFunction(document.myForm.mySelect,'jump')"
		jumpToIndex - if your select is preselecting an index based on values, pass this index in. it must be an index which is counted from 0 and not 1. default value for this field is 0.

	2007-03-20 WAB doctored all the variable names so that contained the form name.  We have instances of same selectbox on different forms on same page.
	2007-06-12 WAB doctored so that formName is not required.  If not passed in then uses getElementById to get object (so ID must be set)
really ought to make a generic JS function out of this if pos
--->

<cfparam name="attributes.selectBoxName">
<cfparam name="attributes.formName" default = "">
<cfparam name="attributes.onChangeFunction">
<cfparam name="attributes.jumpToIndex" default="0">
<cfoutput>
	<script language="javascript">
		
			var manageSelect_#attributes.formName#_#attributes.selectBoxName# = <cfif attributes.formName is not "">document.#attributes.formName#.#attributes.selectBoxName#<cfelse>document.getElementById('#attributes.selectBoxName#')</cfif>;
		
	<cfif not isDefined("request.mouse_wheel_set")>
		/* remove focus from select box when mousewheel is used. - unfortunately, this isn't quick enough to stop the scroll of a select happening, and so is used in conjunction with timeouts and other clever stuff ;) */
		document.onmousewheel = function() {
			document.body.focus();
		}
		<cfset request.mouse_wheel_set = true>
	</cfif>
		/* delay call to onchange method in order to give the onblur time to set th evariable no_run. */
		var change_soc_id_#attributes.formName#_#attributes.selectBoxName# = null;
		var no_run_#attributes.formName#_#attributes.selectBoxName# = false;
		function file_action_on_change_#attributes.formName#_#attributes.selectBoxName#() {

			window.clearTimeout(change_soc_id_#attributes.formName#_#attributes.selectBoxName#);
			change_soc_id_#attributes.formName#_#attributes.selectBoxName# = window.setTimeout("doOnChange_#attributes.formName#_#attributes.selectBoxName#()",200);
		}
		function doOnChange_#attributes.formName#_#attributes.selectBoxName#() {
			if (!no_run_#attributes.formName#_#attributes.selectBoxName#) {
			#attributes.onChangeFunction#; 	
			} else {
				manageSelect_#attributes.formName#_#attributes.selectBoxName#.selectedIndex = #attributes.jumpToIndex#;
			}
		}
		/* delay resetting the no_run variable back to false. ensure the delay is longer than the onchange delay. */
		var blur_soc_id_#attributes.formName#_#attributes.selectBoxName# = null;
		
		function file_action_on_blur_#attributes.formName#_#attributes.selectBoxName#() {
			window.clearTimeout(blur_soc_id_#attributes.formName#_#attributes.selectBoxName#);
			blur_soc_id_#attributes.formName#_#attributes.selectBoxName# = window.setTimeout("reset_file_actions_#attributes.formName#_#attributes.selectBoxName#()",750);
		}
		/* reset value of no_run so that select is active once more. this function is called after a timeout onblur of the select. */
		function reset_file_actions_#attributes.formName#_#attributes.selectBoxName#() {
			no_run_#attributes.formName#_#attributes.selectBoxName# = false;
		}
		/* set select's onblur to this. */
		function handle_on_blur_#attributes.formName#_#attributes.selectBoxName#() {
			no_run_#attributes.formName#_#attributes.selectBoxName#=true;
			file_action_on_blur_#attributes.formName#_#attributes.selectBoxName#();
		}
		/* set onblur function */
		manageSelect_#attributes.formName#_#attributes.selectBoxName#.onblur = handle_on_blur_#attributes.formName#_#attributes.selectBoxName#;
		manageSelect_#attributes.formName#_#attributes.selectBoxName#.onchange = file_action_on_change_#attributes.formName#_#attributes.selectBoxName#;
	</script>
</cfoutput>
