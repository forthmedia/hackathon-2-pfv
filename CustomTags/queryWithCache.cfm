<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="attributes.datasource" >
<cfparam name="attributes.name" >
<cfparam name="attributes.refreshCacheNow" default = false >
<cfparam name="attributes.cachedWithin"  default = 0 >

<cfif thisTag.ExecutionMode is "end">
	
	
	<cfset queryObject = CreateObject("component","\relay\templates\fnlqueryObject")>
	<cfset queryObject.sql = thisTag.generatedContent>
	<cfset queryObject.name = attributes.name>
	<cfset queryObject.datasource = attributes.datasource>

	<cfset x = queryObject.runWithCache(timespan = attributes.cachedWithin, refreshCacheNow = attributes.refreshCacheNow)>

	<cfset caller[attributes.name] = x>

	<cfset thisTag.generatedContent = ""> 
</cfif>

