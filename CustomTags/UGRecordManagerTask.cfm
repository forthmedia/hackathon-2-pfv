<!--- �Relayware. All Rights Reserved 2014 --->
<!---Check if some users need to be deleted--->

<!--- <CFIF Not IsDefined("Attributes.NewEntityID") AND #UGEntityID# EQ 0>

If you've set attribute EntityID to "New" in the custom tag "UGRecordManager" 
you must add attribute "NewEntityID" to the custom tag "UGRecordManagerTask" and it must be a number.

<CF_ABORT>

</CFIF>

Mods:

1.5		WAB added ability to set what permission level is being used and added new attributes to do with that functionality - should be backwards compatible
WAB 2005-09-27    now returns value caller.UGRecordManager_UpdatesDone   (true/false) to say whether an update has been done
2007-10-22 WAB   CF8 changes   -  
2011/10/05 PPB LID7939 use sendEmail for actions  
2015-12-01	WAB PROD2015-290 Visibility Project.  Implementation of 'ExtendedRights' - AND/OR/NOT
--->

	<CFPARAM DEFAULT="" NAME="Attributes.NewEntityID">
	<CFPARAM DEFAULT="" NAME="Users">
	<cfset updateDone = false>	

<cfif structKeyExists(caller,"FieldNames")>

	<CFLOOP index="thisField" list = "#caller.FieldNames#">
	<!--- loop through all the form fields looking for variables of the form UG_#Entity#_#EntityID#_orig --->
	
		<CFIF left(thisField,3) is "UG_" and listLen(thisField,"_") is 5 and right(thisField,5) is "_orig">
	
			<CFSET thisEntityType = listGetAt(thisField,2,"_")>
			<CFSET thisEntityID = listGetAt(thisField,3,"_")>
			<CFSET thisLevel = listGetAt(thisField,4,"_")>
			
			<CFSET previousUsers = evaluate(thisField)>
			<CFIF isDefined("UG_#thisEntityType#_#thisEntityID#_#thisLevel#")>
				<CFSET currentUsers = evaluate("UG_#thisEntityType#_#thisEntityID#_#thisLevel#")>
			<CFELSE>
				<CFSET currentUsers = "">
			</cfif>
	
			<CFIF not isNumeric(thisEntityID)>
				<CFSET thisEntityID = evaluate("#thisEntityID#")>
			<!--- NJH 2013/05/20 - added this so that we could add UG Record Manager scope when adding a record, rather than when a record has already been added. --->
			<cfelseif structKeyExists(caller,"UGrecordManagerEntityID") and thisEntityID eq 0>
				<CFSET thisEntityID = caller.UGrecordManagerEntityID>
			</cfif>

			<cfset groupAnd = "null">			
			<cfset groupNot = "null">

			<cfif structKeyExists (form, "UG_#thisEntityType#_#thisEntityID#_#thisLevel#_GroupANDNot")>
				<cfset GroupANDNot = form[ "UG_#thisEntityType#_#thisEntityID#_#thisLevel#_GroupANDNot"]>
				<cfset groupAnd = listfirst (GroupANDNot,"_")>			
				<cfset groupNot = listlast (GroupANDNot,"_")>
			</cfif>

				
			<CFQUERY NAME="setRecordRights" DATASOURCE="#Application.SiteDataSource#">
			declare 
				  @userGroupsAdded nvarchar(max)
				, @userGroupsRemoved nvarchar(max)
				
			exec setRecordRights 
				  @entity = <cf_queryparam value="#thisEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				, @recordid = <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				, @level = <cf_queryparam value="#thisLevel#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">
				, @userGroupList = <cf_queryparam value="#currentUsers#" CFSQLTYPE="CF_SQL_VARCHAR" >
				, @groupAnd = #groupAnd#
				, @groupNot = #groupNot# 	
				, @previoususerGroupList = <cf_queryparam value="#PreviousUsers#" CFSQLTYPE="CF_SQL_VARCHAR"  >
				, @userGroupsAdded = @userGroupsAdded OUTPUT
				, @userGroupsRemoved = @userGroupsRemoved OUTPUT
				, @UpdatedByPerson = <cf_queryparam value="#request.relaycurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
				
			select 	@userGroupsAdded as userGroupsAdded, @userGroupsRemoved as userGroupsRemoved  
			</cfquery>
	

			<cfif setRecordRights.userGroupsAdded is not "" OR setRecordRights.userGroupsRemoved is not "" >
				<cfset updateDone = true>
			</cfif>

			<!--- Work out who is a new 
			MDC looking into how to send emails to new users. NOT WORKING YET--->
			<CFIF setRecordRights.userGroupsAdded is not "" and thisEntityType eq 'actions' or thisEntityType eq 'OpportunityManagement'>

				<cfquery name="GetEmail" datasource="#application.sitedatasource#">
					select 	usergroupid, person.personid, firstname, lastname, email
					From 	userGroup 
								inner join
							person 	on person.personid = usergroup.usergroupid
					where 	
						userGroupID  in ( <cf_queryparam value="#setRecordRights.userGroupsAdded#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfquery>
						
				<cfloop query = "GetEmail"> 
						<CFIF '#thisEntityType#' eq 'actions'>	
						
							<!--- START: 2011/10/05 PPB LID7939 lifted code from emailsender to build-up mergeStruct and use sendEmail --->
						
							<!--- <cfmodule template="/genericEmails/emailSender.cfm" emailTextID="ActionsEmail" entityType="actions" entityID="#thisEntityID#" PersonID="#PersonID#" cfmailType = "HTML"> --->						
							
							<cfquery name="getEntityType" datasource="#application.siteDataSource#">
								select * from actions where actionid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</cfquery>
							<cfif getEntityType.entitytypeid eq 0>
								<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
								select pa.firstname + ' ' + pa.lastname as Owner, ActionWith = 
								case when a.entityid > 0 then (select p.firstname + ' ' + p.lastname + ' - ' + o.organisationname
									from person p
									join organisation o on o.organisationid = p.organisationid 
									join actions a1 on p.personid = a1.entityid
									and a1.actionid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > ) end,
								a.*
								from actions a
								join person pa on pa.personid = a.ownerpersonid
								where a.actionid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								</cfquery>
								<cfset urlAttr = "frmsrchpersonID">
							<cfelseif getEntityType.entitytypeid eq 2>
							<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
								select pa.firstname + ' ' + pa.lastname as Owner, ActionWith = 
								case when a.entityid > 0 then (select o.organisationname
									from organisation o 
									join actions a1 on o.organisationid = a1.entityid
									and a1.actionid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > ) end,
								a.*
								from actions a
								join person pa on pa.personid = a.ownerpersonid
								where a.actionid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
								</cfquery>
								<cfset urlAttr = "frmsrchOrgID">
							</cfif>

							<cfparam name="mergeStruct" type="struct" default="#structNew()#">
								
							<cfset mergeStruct.Owner = GetEntityDetails.Owner>
							<cfset mergeStruct.ActionWith = GetEntityDetails.ActionWith>
							<cfset mergeStruct.entityID = GetEntityDetails.entityID>
							<cfset mergeStruct.ActionID = GetEntityDetails.ActionID>
							<cfset mergeStruct.Description = GetEntityDetails.Description>
							<cfset mergeStruct.DueDate = DateFormat(GetEntityDetails.Deadline,'dd-mmm-yy')>
							<cfset mergeStruct.entityLink = "#request.currentSite.httpProtocol##HTTP_HOST#/index.cfm?relocateto=data/dataFrame.cfm?#urlAttr#=#thisEntityID#">

							<cfset application.com.email.sendEmail(emailTextID="ActionsEmail",personID=PersonID,mergeStruct=mergeStruct)>
							<!--- END: 2011/10/05 PPB LID7939 --->
							
						<CFELSEIF '#thisEntityType#' eq 'OpportunityManagement'>
							<!--- NJH 2011/04/04 - replaced generic emails with the following... --->
							<cfif structKeyExists(caller,"mergeStruct")>
								<cfset mergeStruct = caller.mergeStruct>
							<cfelse>
								<cfif structKeyExists(caller,"form")>
									<cfset form = caller.form>
								</cfif>
								<cfinclude template="/leadManager/opportunityEmailMergeBuilder.cfm">
							</cfif>
							<cfset application.com.email.sendEmail(emailTextID="opportunityEmail",personID=PersonID,mergeStruct=mergeStruct)>
							<!--- <cfmodule template="/genericEmails/emailSender.cfm" emailTextID="OpportunityEmail" entityType="opportunity" entityID="#thisEntityID#" PersonID="#PersonID#" cfmailType = "HTML"> --->
						</CFIF>
					</cfloop>
				</CFIF> 
	
		</CFIF>		
				
	</CFLOOP>

</cfif>	
	
<cfset caller.UGRecordManager_UpdatesDone = updateDone>

