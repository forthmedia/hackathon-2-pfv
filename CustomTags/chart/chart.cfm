<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			chart.cfm	
Author:				Russell Burdikin
Date started:		2011/11/9
	
Description:			
Set of custom tags to wrap round CFCHART so that images not created in the CFIDE directory (which is not accessible for security reasons)

Has child tags which cf_chartSeries and cf_chartdata

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-11-07	IH	Case 431528 change value in param tag from UseFileName to UseWebPath
2015-06-17 	WAB	Altered some comments and removed an evaluate.  
			Noticed that code was supporting attributes.name but was still outputting the chart.  Bit odd to support attributes.name given that whole point of using cf_chart rather than cfchart is to write to an accessible location - but corrected code anyway.
			Decided to put all the graphs in the same directory (within the users temporary directories) so can do some clearing up of old files during the session
Possible enhancements:

--->
 

 <cfif thistag.executionMode is "end">

	<cfchart name = "savedChart" attributeCollection = "#attributes#">

		<cfif structKeyExists(thisTag,"chartSeries")>
			<cfloop index = "i" from = 1 to=#arrayLen(thisTag.chartSeries)# >
				<cfif structKeyExists (thisTag.chartSeries[i],"query")>
					<cfset variables[thisTag.chartSeries[i].query] = caller [thisTag.chartSeries[i].query]>
				</cfif>
				
				<cfchartSeries attributeCollection = #thisTag.chartSeries[i]#>

					<cfif structKeyExists(thisTag.chartSeries[i],"chartData")>
						<cfloop index = "j" from = 1 to=#arrayLen(thisTag.chartSeries[i].chartData)# >
							<cfchartData attributeCollection = #thisTag.chartSeries[i].chartData[j]#>
						</cfloop>
					</cfif>

				
				</cfchartSeries> 

			</cfloop>
		</cfif>

	</cfchart>

	<cfparam name="attributes.format" default="Flash">

    <cfswitch expression="#attributes.format#">
        <cfcase value="flash"><cfset UsrFileExt = '.swf'></cfcase>
        <cfcase value="png"><cfset UsrFileExt = '.png'></cfcase>
        <cfcase value="jpg"><cfset UsrFileExt = '.jpg'></cfcase>
    </cfswitch>


    <cfset FileNameOnly = "#dateformat(now(), "yyyymmdd")##timeformat(now(), "hhmmsslll")##UsrFileExt#">
	<cfset UseFileName = "#application.paths.temp#\#FileNameOnly#">
	<cfset SetUpPaths = application.com.filemanager.getTemporaryFolderDetails()>
	<cfset CopyFileTo = "#SetUpPaths.sessionPath#\cfchart">
	<cfif not directoryExists (CopyFileTo)>
		<cfdirectory action="create" directory="#copyFileTo#">
	</cfif>
	<cfset UseWebPath = "#SetUpPaths.webSessionPath#\cfchart\#FileNameOnly#">
	<!--- Not actually going to use the subfolder so lets delete it --->
	<cfdirectory action="delete" directory="#SetUpPaths.path#"> 


	<cfif structKeyExists (attributes,"name")>
		<cfset caller[attributes.name] = savedChart>
	<cfelse>
	
		<cffile 
		    action = "write" 
		    file = "#UseFileName#"
		    output = "#savedChart#">
		    
		<cffile 
		    action = "copy"
		    source = "#UseFileName#"
		    destination = "#CopyFileTo#">
	
	
	        <cfswitch expression="#attributes.format#">
	    		      <cfcase value="flash">
	
					<cfoutput>
						<div style="z-index:-5;" align="left">
							<object>
								<param name="movie" value="#UseWebPath#">
			     			   <embed src="#UseWebPath#" width="#attributes.chartwidth#" height="#attributes.chartheight#"></embed>
							</object>
						</div>
					</cfoutput>
			
	    		      </cfcase>
	    		      <cfcase value="png"><cfoutput><img src="#UseWebPath#"></cfoutput></cfcase>
	    		      <cfcase value="jpg"><cfoutput><img src="#UseWebPath#"</cfoutput>></cfcase>
	    		    </cfswitch>
	
	
		<cffile 
		    action = "delete" 
		    file = "#UseFileName#">		    

	</cfif>
	
	<!--- clear up any old images in the directory 
		Would eventually get done at end of session, but in some cases (of reports refreshing themselves) we are creating new charts every minute
	--->
	<cfdirectory directory="#CopyFileTo#" name="directorylist">
	<cfloop query="directorylist">
		<cfif dateDiff("n",dateLastModified,now()) GT 10>
			<cffile action="delete" file="#directory#/#name#">
		</cfif>
	</cfloop>
	
</cfif>





 

