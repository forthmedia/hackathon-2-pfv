<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			chartSeries.cfm	
Author:				Russell Burdikin
Date started:		2011/11/9
	
Description:			
Child of chartSeries.cfm cf_chartSeries customtag

Replicates <CFCHARTSERIES>
 --->

 <cfif thistag.executionMode is "end" or not thistag.hasEndTag >
	<cfif structKeyExists (thistag,"chartData")>
		<cfset attributes.chartData = thistag.chartData>
	</cfif>
	
	<CFASSOCIATE BASETAG="CF_Chart" DATACOLLECTION="chartSeries">
	
</cfif>	

