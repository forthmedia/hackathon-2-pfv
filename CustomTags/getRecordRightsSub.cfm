<!--- �Relayware. All Rights Reserved 2014 --->
<CFSILENT>
<!--- sub routine for determining rights to a record 

Somewhat specific to Person/Location/organisations but could be expanded to look at recordrights table

mods:

WAB 2001-03-26		Added check on organisation country


--->

<CFQUERY NAME="getEntityType" DATASOURCE="#application.SiteDataSource#">				
	SELECT * from flagEntityType
	WHERE tableName =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>
	
<CFIF "Person,Location,country,Organisation" contains getEntityType.tableName>
	<!--- Need to check country rights --->
	<CFSET reqdCountryID = evaluate("#getEntityType.tableName#.Countryid")>
	<CFINCLUDE template = "templates\qryGetCountries.cfm">
	<!--- returns query findCountries --->
	<!--- might need to check for rights to individual people or location here --->
	<CFSET rights = findCountries>

	<!--- check for partner rights/restrictions here--->

	<CFIF findCountries.Level4 IS 1 and "Person,Location,Organisation" contains getEntityType.tableName><!--- level 4 defined as OwnOrganisationOnly --->
		<!--- partner rights, check that this record is from same organisation--->
		<CFQUERY NAME="getOrganisation" DATASOURCE="#application.SiteDataSource#">
		select Organisationid from person where personid = #request.relayCurrentUser.personid#
		</CFQUERY>

		<CFIF getOrganisation.Organisationid IS NOT getRecord.Organisationid>
			<!--- create a null query --->
			<CFSET rights = queryNew("updateOkay,AddOkay,Level2,Level3,Level4")>
	
		</cfif>
		
		<!--- special case for when a partner is adding themselves, needs to have add rights --->
		<CFIF request.relayCurrentUser.isUnknownUser>
			<CFSET temp = queryAddRow(rights)>
			<CFSET temp = querySetCell(rights,AddOkay,1)>
			<CFSET temp = querySetCell(rights,Level3,1)>
		</cfif>
	</CFIF>
<CFELSE>
	<CFQUERY NAME="checkRight" DATASOURCE="#application.SiteDataSource#">
	SELECT 
		Convert(bit,sum(rr.permission & 8)) AS Level4,
		Convert(bit,sum(rr.permission & 4)) AS Level3,
		Convert(bit,sum(rr.permission & 2)) AS Level2,
		Convert(bit,sum(rr.permission & 4)) AS AddOkay,
		Convert(bit,sum(rr.permission & 2)) AS UpdateOkay
	FROM RecordRights AS rr, RightsGroup AS rg
	WHERE 
		rr.UserGroupID = rg.UserGroupID
		AND rg.PersonID = #request.relayCurrentUser.personid#
	</CFQUERY>
	<CFSET rights = checkRight>
</CFIF> 

</CFSILENT>
