<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2012-09-18

lockUpdate.cfm

A Function to be used within a CF_LOCK block

For long running requests within locks, use this tag to feedback information to other processes.
Pass any attributes and these get added to the metaData structure

It also updates the lastUpdated date, and so prevents lock being deleted by housekeeping task thinking that it is orphaned

2013-02-12	WAB Sprint 15&42 Comms.  Return lock information into caller scope.  Allows, for example, Process B adding metadata to Process A's lock to force an exit.
2013-10 	WAB During CASE 437315 Comms Speed/Memory. Distinguish between locks created inside and outside of threads
 --->

<cfparam name="attributes.name" default="">

<cfif attributes.name is "">
	<!--- wanted to use getbasetagdata("cf_lock").attributes.name but this won't work if called within a cfc, so just use request.openLocks --->
	<cfset attributes.name = listLast(application.com.globalfunctions.getThreadOrRequestScope().openLocks)>	
</cfif>

<cfset metadata = structCopy(attributes)>
<cfset structDelete (metadata,"name")>

<cfset caller.cf_lockupdate = application.com.globalFunctions.updateProcessLock(lockname = attributes.name, metadata= variables.metadata)>
