<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2009/04/29

twoSelectsRelatedByExclusion

For drawing two selects each based on a single query, but where a small number of combinations of items are not shown

Eg.
Used for displaying Language and Country drop downs excluding the few which have already been translated

Used in EditPhraseWidget

Currently only coded for use with one on a page

2009/07/17	WAB	Had problem when names of the selects were blank.  So added an if statement
2009/09/16 WAB Problem when had apostophe in Country names, altered evaluation of JSON
2009/09/22 WAB altered so that when first select changed, the second select always tries to have #Attributes.selected2# selected
2015/11/26 SB Bootstrap
--->

<cfparam name="attributes.query1">
<cfparam name="attributes.query2">
<cfparam name="attributes.excludeQuery"> <!--- query with the pairs which are not to be shown --->
<cfparam name="attributes.excludeQueryKey1"> <!--- names of columns in the above query --->
<cfparam name="attributes.excludeQueryKey2">
<CFPARAM NAME="Attributes.ValueColumn1" DEFAULT="value"> <!--- Note at the moment this value can not be changed --->
<CFPARAM NAME="Attributes.ValueColumn2" DEFAULT="value"> <!--- Note at the moment this value can not be changed --->
<CFPARAM NAME="Attributes.Display1" default = "display"> <!--- Note at the moment this value can not be changed --->
<CFPARAM NAME="Attributes.Display2" default = "display"> <!--- Note at the moment this value can not be changed --->
<CFPARAM NAME="Attributes.Name1"> <!--- Form field names --->
<CFPARAM NAME="Attributes.Name2">
<CFPARAM NAME="Attributes.ID1" default = "#Attributes.Name1#"><!--- element IDs --->
<CFPARAM NAME="Attributes.ID2" default = "#Attributes.Name2#">
<CFPARAM NAME="Attributes.selected1" default = ""> <!--- initial values selected --->
<CFPARAM NAME="Attributes.selected2" default = "">
<CFPARAM NAME="Attributes.onChangeFunction1" default =""> <!--- javascript function to be called when selects are changed.    --->
<CFPARAM NAME="Attributes.onChangeFunction2" default ="">
<CFPARAM NAME="Attributes.HTMLbetween" default ="">

<cfsetting enablecfoutputonly="true">

<cfset excludedStruct = application.com.structureFunctions.queryToTwoDimensionalStruct(query = attributes.excludeQuery,key1="#attributes.ExcludeQueryKey1#",key2 ="#attributes.ExcludeQueryKey2#" )>
<cfoutput>
	<select class="form-control" id = "#Attributes.ID1#" <cfif attributes.name1 is not "">name="#htmleditformat(Attributes.Name1)#"</cfif> onchange = "loadSecondSelect (this,$('#htmleditformat(Attributes.ID2)#'),select2Items,excludedPairs,'#htmleditformat(attributes.selected2)#');#htmleditformat(attributes.onChangeFunction1)#">
	</select>
	#Attributes.HTMLbetween#
	<select class="form-control" id = "#Attributes.ID2#"  <cfif attributes.name2 is not "">name = "#htmleditformat(Attributes.Name2)#" </cfif> onchange="#htmleditformat(attributes.onChangeFunction2)#">
	</select>
</cfoutput>

<!--- WAB 2009/09/15 alteration here --->
<cfset select1JSON = application.com.mxAjax.jsonencode(attributes.query1)>
<cfset select2JSON = application.com.mxAjax.jsonencode(attributes.query2)>
<cfset excludedPairsJSON = application.com.mxAjax.jsonencode(excludedStruct)>

<cfoutput>
	<script>
		select1Items = #select1JSON#
		select2Items = #select2JSON#
		excludedPairs = #excludedPairsJSON#

			function loadFirstSelect (firstSelectObj,firstSelectItems,selectedValue)  {
				for (i=0;i<firstSelectItems.RECORDCOUNT;i++) {
			   		var newOption = new Option(firstSelectItems.DATA.DISPLAY[i], firstSelectItems.DATA.VALUE[i]);
			     		firstSelectObj.options[i] = newOption;
						if (selectedValue == newOption.value) {firstSelectObj.options[i].selected = true}
				}

			}

			function loadSecondSelect (firstSelectObj,secondSelectObj,secondSelectItems,excludeItems,selectedValue)  {

				firstSelectValue = firstSelectObj.value
				secondSelectObj.length = 0
				// delete all items - basically deletes all the optgroups
				for (i=secondSelectObj.childNodes.length-1;i>=1;i--) {
					secondSelectObj.removeChild(secondSelectObj.childNodes[i])
				}

				useOptGroups = (secondSelectItems.DATA.OPTGROUP)?true:false
				currentOptGroup = ''
				optGroupToBeAdded = ''
				optGroupObject = secondSelectObj  // start with the optgroup object being the actual select

				for (i=0;i<secondSelectItems.RECORDCOUNT;i++) {
					valueToBeAdded = secondSelectItems.DATA.VALUE[i]
					textToBeAdded = secondSelectItems.DATA.DISPLAY[i]
					if (useOptGroups) {
						optGroupToBeAdded= secondSelectItems.DATA.OPTGROUP[i]
					}


					if (!(excludeItems[firstSelectValue] && excludeItems[firstSelectValue][valueToBeAdded])) {

						if (currentOptGroup != optGroupToBeAdded) {
							optGroupObject = document.createElement('optgroup')
							optGroupObject.label =  optGroupToBeAdded
							secondSelectObj.appendChild(optGroupObject) ;
							currentOptGroup = optGroupToBeAdded;
						}

						newOption = document.createElement('option');
						newOption.innerHTML  = textToBeAdded
						newOption.value = valueToBeAdded
						if (selectedValue == newOption.value) {newOption.selected = true}

						optGroupObject.appendChild(newOption)
					}
				}
			}

		function initialiseRelatedSelects_#jsStringFormat(Attributes.ID1)# (selectedValue1,selectedValue2) {
			loadFirstSelect ($('#jsStringFormat(Attributes.ID1)#'),select1Items,selectedValue1)
			loadSecondSelect ($('#jsStringFormat(Attributes.ID1)#'),$('#jsStringFormat(Attributes.ID2)#'),select2Items,excludedPairs,selectedValue2 )

		}

		initialiseRelatedSelects_#jsStringFormat(Attributes.ID1)# ('#jsStringFormat(Attributes.selected1)#','#jsStringFormat(Attributes.selected2)#')
	</script>
</cfoutput>

<cfsetting enablecfoutputonly="false">