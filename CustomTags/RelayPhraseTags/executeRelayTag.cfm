<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		**********************************************************
		
			RELAY PHRASE TAGS

		**********************************************************

		David A McLean August 2001
		
		Objective...
		
		To provide the ability to Include CFM Templates within phrases.
		It has always been possible to render HTML tags in phrases, but not CFM
		tags. Relay tags now permit this in a rather basic form.
		
		The current version permits only one tag per phrase. If more than one
		Relay tag is contained in a phrase the second and subsequent tags will
		be ignored.
		
		How it works...
		
		There is a table called RelayTag which contains data relating to Relay Tags
		This data is retrieved and then each tag is searched for in the phrase.
		When a tag is found in the phrase, its relevant Tag Template is included.
		The Tag Template should be named in the form #TagTextID#_Tag.cfm

		If no tags are found the phrase is returned in variable Relay.Phrase
		
		Where a tag is found the phrase is devided into two and Relay.Phrase1 and 
		Relay.Phrase2 are returned. To indicate whether a tag is found a boolean
		value Relay.PlaceHolderFound
		
		Some tags require additional variables or processing, and these should be 
		included by setting the "SpecificValues" field in the table to 1 then creating a
		file in the TagTemplates subdirectory containing the variables or processing, and
		name this file #Tag.TagTextID#_Tag.cfm

Modifications:
SWJ		2002-06-19	Modified the datasource on getTags to RelayServer.
WAB      2004-07-07	Modified so that it can do more than one relay tag on a page
				Restructured slightly as well to make use of regular expressions	
				parameters can be passed in within the relay tag, but still supports parameters set in the element table
		
2006-05-02 WAB messed about with calling relaytags as customtags (dependent on updateRelayTagStructure and associated application variables)

PPB		2008-08-11 Bug Fix (see inline comment for detail)

2009/02/11 WAB added check for the file existing
2009/07/08  Problem (on dev) with variables getting overwritten, changed re to RelayTagRe and x to RelayTagReFindResult  [re was being set in the sqlinjectionchecker which was occasionally being run inside tags ]
--->


<CFIF NOT thisTag.HasEndTag >
	<cfoutput>You have not specified an end tag for CF_ExecuteRelayTag</cfoutput>
	<cfexit method="EXITTAG">
</cfif>

<CFSILENT>
		
<cfif thistag.executionMode is "end">

	<cfset phrase = thisTag.generatedContent>
	
	<CFIF isDefined("attributes.parameters")>
		<CF_EvaluateNameValuePair
			NameValuePairs = #attributes.parameters#
		>
	</cfif>

	<!--- Check for the Element parameter 
			Note this may be removed in future as tags may be called in phrases that are not 
			element phrases. Currently these tags are essentially an extension of elements.
			The EleemntID is passed back to the caller as RelayTag.ElementID as this is the 
			variable that will be referred during the phrase include. This is a precaution
			against the value of ElementID changing between this call and the include.
	--->
	<CFIF isDefined("attributes.ElementID")>
		<CFSET ElementID=attributes.ElementID>
		<CFSET relaytag.ElementID=attributes.ElementID><!--- some relaytemplates expect this value to be set --->
	<CFELSE>
	</CFIF>		



	<!--- use regularexpression to see if there is a relay tag in the phrase --->
	<cfset RelayTagRe="<(RELAY_.*?)>" >
	<cfset RelayTagReFindResult = refindnocase (RelayTagRe,phrase,0,true)>
	<cfset matchfound = false>
	<cfset counter = 0>
	<cfset attributeStructure = structNew()>

	<!---
		PPB 2008-08-11: I added the "and (x.pos[2] is not 0)" condition to resolve a bug that caused a crash on the 2nd loop (when there was only 1 RELAY_ADDCONTACTFORM in the phrase)
	--->
	<cfloop condition = "(RelayTagReFindResult.pos[1] is not 0) and (RelayTagReFindResult.pos[2] is not 0) and counter LT 100">
	
		<cfset counter = counter + 1>  <!--- just used to prevent continuous looping ifthere's a problem eg if a relaytag contains itself recursively--->
		<cfset tagfound  = mid (phrase,RelayTagReFindResult.pos[2],RelayTagReFindResult.len[2])>
		<!--- Now need to check whether there are any parameters in the tag --->
		<cfset firstspace = findnocase (" ", tagfound)> 
		<cfif firstspace is not  0 >
			<cfset parameters = mid (tagfound,firstspace, len(tagfound)-firstspace+1)>
			<cfset tagfound =left (tagfound,firstspace-1)>

			<!--- WAB 2005-09-13
			changed from CF_EvaluateNameValuePair to a new function which will deal spaces in values (as long as they are put in quotes)
			<!--- evaluate any parameters in the tag - delimiters can be space or comma.  can't handle spaces or commas in quoted variables I'm afraid !--->
			<CF_EvaluateNameValuePair
				NameValuePairs = #parameters# delimiter=" ,"
			>
			 --->
			<cfset attributeStructure = application.com.regExp.convertNameValuePairStringToStructure(parameters)>
		
		</cfif>

		<cfif structKeyExists(application.RelayTagKeyedByTagName,tagfound) >
			<cfset matchfound = true>
			<cfset theTag = application.RelayTagKeyedByTagName[tagfound]>
			<CFSET caller.RelayTag.TagTemplate = "#theTag.CFM#">
			<CFSET nSpecificValues = #theTag.SpecificValues#>
			<CFIF nSpecificValues>
				<CFINCLUDE TEMPLATE="TagTemplates/#theTag.TagTagTextID#_Tag.cfm">
			</CFIF>
			
			<!--- START: AJC 2008-05-09 Issue 270: Training - relayadd contact form adds all new contacts to org: 1074 - to include variables in the variables scope from relayINI for example, done only for addcontactform for now --->
			<cfset RelayTagRe2 = RelayTagRe>
			<cfif theTag.tag is "RELAY_ADDCONTACTFORM">
				<cfset callerStruct = duplicate(caller)>
				<cfif structKeyExists(callerStruct,"ThisTag")>
					<cfset structDelete(callerStruct,"ThisTag")>
				</cfif>
				<cfset structAppend(attributeStructure,callerStruct,"No")>
			</cfif>

			<cfsaveContent variable = "relayTagContentHTML">
				<cfsetting enablecfoutputonly="no" >
					<cfif not theTag.fileExists><!---  2009/02/11 WAB added check--->
						<cfoutput>File for tag #htmleditformat(theTag.Tag)# does not exist <!--  #theTag.cfm# --><BR></cfoutput>  
					<cfelseif theTag.isCustomTag>
						<cfmodule template="#theTag.tagPath#"  attributecollection="#attributeStructure#">
					<cfelse>
						<cfloop index = "name" list= #structKeyList(attributeStructure)#> 
							<cfset "#name#"	= attributeStructure[name]>
						</cfloop>
						<cfinclude template="#theTag.tagPath#">
					</cfif>
							
				<cfsetting enablecfoutputonly="yes" >
			</cfsavecontent>

<!--- 			<cfset phrase = left(phrase,RelayTagReFindResult.pos[1]-1) & relayTagContent & right (phrase,len(phrase) - (RelayTagReFindResult.pos[1] + RelayTagReFindResult.len[1]))> --->
			<cfset phrase = reReplaceNoCase (phrase,RelayTagRe2,relayTagContentHTML,"one")>
			<!--- END: AJC 2008-05-09 Issue 270: Training - relayadd contact form adds all new contacts to org: 1074 - to include variables in the variables scope from relayINI for example, done only for addcontactform for now --->	
		<cfelse>

<!--- 			<cfset phrase = left(phrase,x.pos[1]-1) & "#mid(tagfound,2,len(tagfound)-2)# Tag Not Found" & right (phrase,len(phrase) - (x.pos[1] + x.len[1]))> --->
			<cfset phrase = reReplaceNoCase (phrase,RelayTagRe,"#tagfound# Tag Not Found","one")>
		</cfif>
		<cfset RelayTagReFindResult = refindnocase (RelayTagRe,phrase,0,true)>
	</cfloop>

	<cfset thisTag.GeneratedContent = phrase>

</cfif>

</CFSILENT>

