<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			phraseEditWidget.cfm
Author:				WAB
Date started:			2008/02/12

Description:
A widget for editing translations
Initially used in RelayXMLEditor

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
WAB 2009/03/- Adding table of already translated languages
WAB 2009/05 - added rich text support and all sorts of jazzy ajax
WAB 2009/07  - added back plain text area support
WAB 2010/10/05 - Added support for defaultForThisCountry
NJH 2012/12/04 - work out whether to use richtextarea or textarea when in autoTextArea mode, added as part of 2013 Roadmap CR 18
NJH 2013/02/22	Added maxChars and readonly functionality. Use cf_relayTextField rather than textArea
WAB 2013-05-15 	CASE 435127 No longer have separate ckeditor config files for each entitytype, instead put a relayContext attribute on the textarea
NJH	2013/05/30	Case 435229 - if a user doesn't have language rights, then don't display the add language link and don't display the language valid values on a new record. Rather, just create a hidden field with the users language
NYB	2014-03-06	Case 438921 added if siteStructure.isok around <cfset relayContentsCss = trim(siteStructure.stylesheet)>
NJH	2014/12/10	Load stylesheets dynamically from request.currentsite.stylesheet. Pass in whole array to cfeditor_config.js
Possible enhancements:


 --->


<cfparam name="attributes.phraseTextID"  default = "">  <!--- generally required, but doesn't ahev to be supplied if just want to output the translation list --->
<cfparam name="attributes.entityID" default = 0>
<cfparam name="attributes.entityType" default = "">
<cfparam name="attributes.displayAs" default = ""> <!--- Input,TextArea,RichTextArea--->
<cfparam name="attributes.rows" default = "4">
<cfparam name="attributes.cols" default = "40">
<cfparam name="attributes.required" default = false>
<cfparam name="attributes.message" default = "#attributes.phraseTextID# Field Required">
<cfparam name="attributes.newTranslationLanguageDropDown" default = true>
<cfparam name="attributes.showOtherTranslationsTable" default = true>
<cfparam name="attributes.showCurrentLanguageString" default = true>
<cfparam name="attributes.allphraseTextIDsOnPage" default = "#attributes.phraseTextID#">  <!--- determines what phraseTextIds get dealt with in the translation list, leave blank and is dealt with automatically --->
<cfparam name="attributes.disabled" default = false>
<cfparam name="attributes.showTranslationTableLabel" default="true">
<cfparam name="attributes.promptForSave" default="true" pattern="true|false">
<cfparam name="attributes.saveFormFunction" default="">
<cfparam name="attributes.saveChangesMessage" default="Save Changes?">
<cfparam name="attributes.showMergeFields" default=false>
<cfparam name="attributes.showTextToolbar" default=false>
<cfparam name="attributes.maxChars" default=""> <!--- NJH 2013/02/22 added maxchars and readonly functionality --->
<cfparam name="attributes.readonly" default="false">
<cfparam name="attributes.shortenURL" default="false">
<cfparam name="attributes.label" default="#attributes.phraseTextID#">
<cfparam name="attributes.maxLength" default="4000">
<cfparam name="attributes.requiredFunction" default="">
<cfparam name="attributes.submitFunction" default="">
<cfparam name="attributes.size" default="50">
<cfparam name="attributes.showTranslationLink" default="true">

<cfif left(attributes.label,4) eq "phr_">
	<cfset attributes.label = application.com.relayTranslations.translatePhrase(phrase=attributes.label)>
</cfif>

<cfparam name ="request.editPhraseWidget" default = "#structNew()#">
<cfparam name="request.editPhraseWidget.newPhrase" default= false>

<cfset showLanguageDropDownForNewTranslation = false>
<cfset thisFieldID = "">

<cfif attributes.entityType is "">
	<cfset EntityTypeID = 0>
<cfelseif isNumeric(attributes.entityType)>
	<cfset EntityTypeID = attributes.entityType>
<cfelse>
	<cfset EntityTypeID = application.entityTypeid[attributes.entitytype]>
</cfif>

<!--- do previous updates--->
<cfif not structKeyexists(request,"phraseUpdate")>
	<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>
</cfif>


<cf_includejavascriptonce template="/javascript/doTranslation.js">
<cf_includejavascriptonce template="/javascript/editPhraseWidget.js">
<cf_includejavascriptonce template="/javascript/checkObject.js">

<cfif attributes.phrasetextID is not "">
	<cfif not structKeyExists (request.editPhraseWidget,"languageid")><!--- this variable might have been set by this control earlier in the request, or if this form has some sort of drop down then this variable might have been passed --->
		<cfif structKeyExists (request,"phraseUpdate") and structKeyExists (request.phraseUpdate,"lastUpdate")>
			<cfset request.editPhraseWidget.LanguageID = request.phraseUpdate.lastUpdate.languageid>
			<cfset request.editPhraseWidget.CountryID   = request.phraseUpdate.lastUpdate.countryID>
		<cfelseif IsDefined("phrLanguageID") >
			<cfset request.editPhraseWidget.LanguageID = phrLanguageID>
			<cfif IsDefined("phrCountryID") >
				<cfset request.editPhraseWidget.CountryID   = phrCountryID>
			<cfelse>
				<cfset request.editPhraseWidget.CountryID   = 0>
			</cfif>

		<cfelse>

			<!--- find out what translations exist for this phrase, get the default one, make sure this language/country combination is used for the whole page								--->
			<cfset defaultLanguageAndCountry = application.com.relayTranslations.getDefaultLanguageAndCountryForEditingAPhrase(phraseTextID = attributes.allphraseTextIDsOnPage, entityID = attributes.EntityID , entityType=attributes.entityType)>
			<!--- by putting into form scope these are available later in the same request and so if two translations on a page we get same language on each --->
			<cfset request.editPhraseWidget.LanguageID   = defaultLanguageAndCountry.LanguageID>
			<cfset request.editPhraseWidget.CountryID   = defaultLanguageAndCountry.countryID>

			<cfif defaultLanguageAndCountry.phraseExists is false and attributes.newTranslationLanguageDropDown>
				<cfset request.editPhraseWidget.LanguageID = -1>
				<cfset request.editPhraseWidget.newPhrase = true>
			</cfif>

		</cfif>
	</cfif>


	<!--- gets translation of phrase in chosen language --->
	<cfset getTranslations = application.com.relayTranslations.getEntityPhrases(phraselist = attributes.phraseTextID, entityType = attributes.entityType, entityID = attributes.entityid,countryid =request.editPhraseWidget.CountryID, languageid =request.editPhraseWidget.LanguageID,evaluateVariables=false,exactMatch=true,debug=false)>
	<cfset thisTranslation = getTranslations[attributes.entityid][attributes.phraseTextID]>
	<cfset thisFieldName = getTranslations[attributes.entityid].updateidentifier[attributes.phraseTextID]>
	<cfset thisPhraseIdentifier = getTranslations[attributes.entityid].phraseIdentifier[attributes.phraseTextID]>
	<cfset thisFieldID = "phrupd_#thisPhraseIdentifier#_">
	<cfset thisDefaultForThisCountry = getTranslations[attributes.entityid].DefaultForThisCountry[attributes.phraseTextID]>
	<cfset iseditable = application.com.relayTranslations.doesUserHaveRightsToCountryLanguageCombination (countryid = request.editPhraseWidget.CountryID, languageid =request.editPhraseWidget.LanguageID )>

	<!--- NJH 2012/12/04 - added as part of 2013 Roadmap CR 18  - work out whether to use richtextarea or textarea--->
	<cfif attributes.displayAs eq "autoTextArea">
		<cfset autoTextArea = true>
		<cfset attributes.displayAs = "RichTextArea">
		<cfif thisTranslation neq "" and not application.com.regExp.isStringHTML(inputString=thisTranslation)>
			<cfset attributes.displayAs = "textArea">
		</cfif>
	</cfif>

	<!--- output in required format --->
	<cfoutput>

	<cfif attributes.message is not "">
		<cfset attributes.message = application.com.relayTranslations.translateString(attributes.message)>
	</cfif>

	<!-- StartDoNotTranslate -->
	<cfif attributes.displayAs is "RichTextArea">

		<!---<cf_includeJavascriptOnce template="/ckeditor/ckeditor_source.js">--->
		<cf_includeJavascriptOnce template="/ckeditor/ckeditor.js">
		<cf_includeJavascriptOnce template="/ckfinder/ckfinder.js">

		<cfset thisTranslation =application.com.relayTranslations.replaceRelayTagChevronsWithCurlies(thisTranslation)>
		<cfoutput><div class="validation-group"><TextArea name="phrupd_#thisPhraseIdentifier#_" id = "#thisFieldID#" rows = #attributes.rows# cols=#attributes.cols# label="#attributes.label#" relaycontext = '#attributes.entityType#' <cfif attributes.required>submitFunction="if (editor.getData().trim() == '') {return 'Required'} else {return '';}"</cfif>>#HTMLEDITFORMAT(thisTranslation)#</TEXTAREA></div></cfoutput>

		<cfset relayContentsCss = "">
		<cfif structKeyExists(session,"ParentElementIDAndSiteName")>
			<cfset siteStructure = application.com.relayCurrentSite.getSiteDefStructureByDomain(listLast(session.ParentElementIDAndSiteName,"|"))>

			<cfif siteStructure.isOk>
				<cfloop list="#trim(siteStructure.stylesheet)#" index="stylesheet">
					<cfif not findNoCase("bootstrap",stylesheet)>
						<cfset relayContentsCss = listAppend(relayContentsCss,trim(stylesheet))>
					</cfif>
				</cfloop>
			</cfif>
		</cfif>

		<cfif relayContentsCss eq "">
			<cfset relayContentsCss = "/code/styles/defaultPartnerStyles.css">
		</cfif>

		<script>
			<cfoutput>CKInstanceName = '#JsStringFormat(thisFieldID)#';

			var relayContentsCss = [#listQualify(relayContentsCss,"'")#];

			var editor = CKEDITOR.replace( CKInstanceName,
				 {
				 	customConfig : '/javascript/ckeditor_config.js',
					filebrowserLinkUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Files&relayContext=#jsStringFormat(attributes.entityType)#&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
					filebrowserImageUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Images&relayContext=#jsStringFormat(attributes.entityType)#&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
					filebrowserFlashUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Flash&relayContext=#jsStringFormat(attributes.entityType)#&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/'
				 }
			 );

			CKFinder.setupCKEditor( editor, {basePath:'/ckfinder/'});
			//startupPath config, etc, can only be set in the config.js file.. see http://docs.cksource.com/CKFinder_2.x/Developers_Guide/ColdFusion/CKEditor_Integration

			<!--- <cfif fileExists("#application.paths.relay#/ckfinder/ckfinder_config_#attributes.entityType#.js")>
				CKFinder.config.customConfig = '/ckfinder/ckfinder_config_#attributes.entityType#.js';
			</cfif> --->

			</cfoutput>
<!---
								<cf_includejavascriptonce template="/javascript/editor.js">

									CKEditor.BasePath = '/CKEditor/'
									var oCKEditor = new CKEditor('phrupd_#thisPhraseIdentifier#_','100%','400','Default','#jsstringformat(thisTranslation)#');


									oCKEditor.Create();
									var myGlobalEditorInstanceForTesting
									function CKEditor_OnComplete( editorInstance ) {

										// enable/disable the editor after the html has been set
										editorInstance.Events.AttachEvent('OnAfterSetHTML', function (editorInstance) {
											<!--- if whole page is to be disabled then make sure that editor stays --->
											<cfif attributes.disabled>
												editorInstance.LinkedField.disabled = true;
											</cfif>
											editorDisabled = editorInstance.LinkedField.disabled
										   	CKEditor_disable (editorInstance,editorDisabled);
											editorInstance.ResetIsDirty()  // assumption is that sethtml only used when we are reloading a translation by Ajax, and we want the isdirty flag to be reset
										})

										<!--- if whole page is to be disabled, or this language combination is not editable then disable editor --->
										<cfif attributes.disabled or not isEditable>
											CKEditor_disable(editorInstance,'true')
										</cfif>

										if (typeof(CKEditor_OnComplete_Custom) != 'undefined') {
											CKEditor_OnComplete_Custom(editorInstance)
										}
									}
	--->
		</script>

		<!--- NJH 2012/12/04 - added as part of 2013 Roadmap CR 18 --->
		<cfif isDefined("autoTextArea") and (attributes.entityid eq 0 or not isNumeric(attributes.entityID))>
			<div class="infoblock">To create text content, use 'Source' mode and save.</div>
		</cfif>

		<!--- <cftextarea id= "phrupd_#thisPhraseIdentifier#_" name = "phrupd_#thisPhraseIdentifier#_" rows="#attributes.rows#" cols="#attributes.cols#" required=#attributes.required# message=#attributes.message#>#thisTranslation#</cftextarea> --->
	<cfelseif attributes.displayAs is "TextArea">
		<cfif attributes.showTextToolbar>
			<cfmodule template="/templates/textAreaControlBar.cfm"
				textAreaName="#thisFieldID#"
				showHTMLPageTags = "false"
				showHTMLTags="false"
				showMergeFields=#attributes.showMergeFields#
				context=#attributes.entityType#>
		</cfif>

		<cfset textFieldAttributes = {readOnly=attributes.readOnly,usecfform=false,fieldname="#thisFieldID#", name="#thisFieldID#", id = "#thisFieldID#", rows = attributes.rows, cols=attributes.cols, currentValue=thisTranslation,label=attributes.label,requiredFunction=attributes.requiredFunction,submitFunction=attributes.submitFunction,required=attributes.required}>
		<cfif attributes.maxChars neq "">
			<cfset textFieldAttributes.maxChars = attributes.maxChars>
		</cfif>

		<cf_relayTextField attributeCollection=#textFieldAttributes#>
		<!--- NJH 2013/02/22 Use above to support max chars... <cfoutput><TextArea name="phrupd_#thisPhraseIdentifier#_" id = "phrupd_#thisPhraseIdentifier#_" rows = #attributes.rows# cols=#attributes.cols#>#HTMLEDITFORMAT(thisTranslation)#</TEXTAREA></cfoutput> --->

	<cfelse>
		<cfset tempAttributeCollection = structNew()>
		<cfset tempAttributeCollection.type = "text" >
		<cfset tempAttributeCollection.id = thisFieldID >
		<cfset tempAttributeCollection.name = tempAttributeCollection.id>
		<cfset tempAttributeCollection.value = "#thisTranslation#" >
		<cfset tempAttributeCollection.size = attributes.size >
		<cfset tempAttributeCollection.required=attributes.required>
		<cfset tempAttributeCollection.message=attributes.message>
		<cfset tempAttributeCollection.label=attributes.label>
		<cfset tempAttributeCollection.maxLength=attributes.maxLength>
		<!--- NJH 2013/02/22 --->
		<cfif attributes.readonly>
			<cfset tempAttributeCollection.readonly=attributes.readonly>
		</cfif>
		<cfif attributes.disabled or not isEditable>
			<cfset tempAttributeCollection.disabled=attributes.disabled>
		</cfif>
		<cfif attributes.disabled>
			<cfset tempAttributeCollection.permanentlyDisabled=attributes.disabled>
		</cfif>
		<cfset tempAttributeCollection.requiredFunction=attributes.requiredFunction>

		<cf_input attributeCollection = #tempAttributeCollection#>
	</cfif>
	<!-- EndDoNotTranslate -->

	<!--- NJH 2013 Roadmap 2013/03/18 - needed to shorten URL in the message text --->
	<cfif attributes.shortenURL>
		<cf_input type="hidden" id="#thisPhraseIdentifier#_shortenURL" name="#thisPhraseIdentifier#_shortenURL" value="true">
	</cfif>

	<cfif not structKeyExists (request.editPhraseWidget,"languageidAndcountryidFieldsOutput") and not request.editPhraseWidget.newPhrase>
		<CF_INPUT type="hidden" ID = "phrlanguageid" name = "phrlanguageid" value="#request.editPhraseWidget.LanguageID#">
		<CF_INPUT type="hidden" ID = "phrcountryid" name = "phrcountryid" value="#request.editPhraseWidget.CountryID#">
		<cfset request.editPhraseWidget.languageidAndcountryidFieldsOutput = true>
	</cfif>
		<!---
				At one stange had separate language fields for each phrase, now just have a single one for all phrases
				<input type="hidden" ID = "phrupdlanguageid_#thisPhraseIdentifier#_" name = "phrupdlanguageid_#thisPhraseIdentifier#_" value="#request.editPhraseWidget.LanguageID#">
				<input type="hidden" ID = "phrupdcountryid_#thisPhraseIdentifier#_" name = "phrupdcountryid_#thisPhraseIdentifier#_" value="#request.editPhraseWidget.CountryID#">
			--->
		<!--- NJH 2012/11/05 Case 431545 UAT - added entityTypeID 0 in condition below, so that we get the T showing for non-entity phrases.--->
		<cfif isnumeric(attributes.entityID)  and (entityTypeID eq 0 or attributes.entityID is not 0) and attributes.showTranslationLink><div class="translationCMS"><A HREF="javascript:doTranslation('#attributes.phraseTextID#_#entityTypeid#_#attributes.entityID#',false)" title="Show All Translations">phr_translations_text</A></div></cfif>  <!--- Don't display T if this is a new record--->

	</cfoutput>

</cfif>


	 <!---
 ***************
 Output the Current Language String or a New Language drop down
 ***************
 --->

	<cfif attributes.showCurrentLanguageString and not structKeyExists(request.editPhraseWidget,"CurrentLanguageStringOutput")>
		<!--- nasty - must be way of doing with div --->
		<cfif request.editPhraseWidget.newPhrase>
			<cfif request.relaycurrentuser.LanguageEditRights is not "" and request.relaycurrentuser.LanguageEditRights neq 0>
				<!--- new (or untranslated) element just gets a language drop down - must be for all countries --->
				<CFQUERY NAME="getLanguages" DATASOURCE="#application.SiteDataSource#">
				select languageid as datavalue, language as displayValue from language where languageid in (#request.relaycurrentuser.LanguageEditRights#)
				</CFQUERY>
					<div class="languageContainerCMS">
						<label>Language</label>
						<CF_displayValidValues
								formFieldName = "phrLanguageID"
								validValues="#getLanguages#"
								currentValue = "#request.relaycurrentuser.languageid#"
								showNull = true
								NullText = "Select a Language"
						>
						<div id="anyCountry">Any Country</div>
					</div>

					<cfset request.editPhraseWidget.languageidAndcountryidFieldsOutput = true>
			<cfelse>
				<cfoutput><input type="hidden" name = "phrlanguageid" value = "#request.relayCurrentUser.languageId#"></cfoutput>
			</cfif>
			<input type="hidden" name = "phrcountryid" value = "0">
		<cfelse>
			<!--- converts IDs to text --->

			<cfset languageAndCountryName = application.com.relayTranslations.getlanguageAndCountryName(countryid  = request.editPhraseWidget.CountryID, languageid = request.editPhraseWidget.LanguageID, defaultForThisCountry = thisDefaultForThisCountry)	>
				<cfoutput><div id="translationString">#htmleditformat(languageAndCountryName.FullString)#</div></cfoutput>

		</cfif>

		<cfset request.editPhraseWidget.CurrentLanguageStringOutput	=true>
	<cfelse>

		<!--- NJH 2013/08/21- we want to remove the language dropdown by setting attributes.showCurrentLanguageString to false. However, when we do that
			we don't get the phrcountryID and phrlanguageid variables. However, these also get set in another if statement above, so we need to check both
			CurrentLanguageStringOutput and languageidAndcountryidFieldsOutput variables.
		 --->
		<cfif not structKeyExists(request.editPhraseWidget,"CurrentLanguageStringOutput") and not structKeyExists (request.editPhraseWidget,"languageidAndcountryidFieldsOutput")>
			<input type="hidden" name = "phrcountryid" value = "0">
			<cfoutput><input type="hidden" name = "phrlanguageid" value = "#request.relayCurrentUser.languageId#"></cfoutput>
			<cfset request.editPhraseWidget.CurrentLanguageStringOutput	=true>
		</cfif>
	</cfif>


 <!---
 ***************
 Output the Other Translations Table
 ***************
 --->
	<cfif not structKeyExists(request.editPhraseWidget,"TranslationLangAndCountryOutput") and attributes.showOtherTranslationsTable> <!--- Just want once on a page --->

		<cfif not structKeyExists(request.editPhraseWidget,"TranslationLangAndCountryOutput") and isNumeric(attributes.entityid) >  <!--- WAB 2009/03/09 added is numeric check - if is a new entity there won't be any translations --->
			<cfset getTranslatedLanguages = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase(entityType = attributes.entityType, entityID = attributes.entityid,defaultPhraseTextIDList=attributes.allphraseTextIDsOnPage)>
		</cfif>


		<cfoutput>
		<BR>

		<cfif attributes.showOtherTranslationsTable and isNumeric(attributes.entityid) and not request.editPhraseWidget.newPhrase>  <!--- WAB 2009/03/09 added is numeric check - if is a new entity there won't be any translations --->
			<div id="translationTable">
			#application.com.relayTranslations.generateTranslatedLanguagesTable(translatedLanguagesQuery = getTranslatedLanguages,currentLanguageID = request.editPhraseWidget.LanguageID, currentCountryID =request.editPhraseWidget.CountryID,showLabel=attributes.showTranslationTableLabel )#
			</div>
			<div id="addNewLanguageDiv" style="display:none;">
				<cfset allLanguagesAndCountriesStruct = application.com.relayTranslations.getAllCountriesAndLanguagesForUser()>

				<cf_twoSelectsRelatedByExclusion
					query1 = #allLanguagesAndCountriesStruct.languages#
					query2 = #allLanguagesAndCountriesStruct.countries#
					excludeQuery = 	#getTranslatedLanguages#
					excludeQueryKey1 = 	"languageID"
					excludeQueryKey2 = 	"countryID"
					name1 = ""
					name2 = ""
					id1 = "newLanguageID"
					id2 = "newCountryID"
					selected2 = "0"
					onchangefunction2 = "newLanguageChangeV2()"
				> <a href="javascript:newLanguageChangeV2()">Add</a>


					<script>
					<cfif attributes.saveFormFunction is not "">
					function editPhraseWidgetUnderlyingFormSave() {
						return #jsStringFormat(attributes.saveFormFunction)#
					}
					</cfif>
					var editPhraseWidgetPromptForSave = #jsStringFormat(attributes.promptForSave)#
					var editPhraseWidgetMsgSaveChanges = '#jsStringFormat(attributes.savechangesmessage)#'

					function newLanguageChangeV2 ()	{
						languageID = $('newLanguageID').value
						countryID = $('newCountryID').value
						entityID = #jsStringFormat(attributes.entityID)#
						entityTypeID = #jsStringFormat(entityTypeid)#
						if (languageID != '' && countryID != '') {
							changeLanguage(languageID,countryID,entityTypeID,entityID)
							hideAddNewLanguage()
						}
					}


					function hideAddNewLanguage ()	{
						$('addNewLanguageDiv').hide()
						$('addNewLanguageLink').show()
					}

					function showAddNewLanguage (entityTypeID,entityID)	{
						$('addNewLanguageDiv').show()
						$('addNewLanguageLink').hide()
					//	loadFirstSelect ($('newLanguageID'),languageItems)
					//	loadSecondSelect ($('newLanguageID'),$('newCountryID'),countryItems,translateLanguages )
					}

					function newLanguageChange (languageID,countryID,entityTypeID,entityID)	{
						if (languageID != '' && countryID != '') {
							changeLanguage(languageID,countryID,entityTypeID,entityID)
							hideAddNewLanguage()
						}
					}


				</script>
			</div>
			<cfif not attributes.disabled and not request.editPhraseWidget.newPhrase and request.relaycurrentuser.LanguageEditRights neq 0>
				<A id="addNewLanguageLink" HREF="javascript:showAddNewLanguage(entityTypeID = '#entityTypeid#', entityID = '#attributes.entityID#')" class="btn btn-primary">phr_add_new_language</A>
			</cfif>
		</cfif>

		<cfset request.editPhraseWidget.TranslationLangAndCountryOutput = true>
		</cfoutput>
	</cfif>

<cfset caller.editPhraseWidget = {id = thisFieldID}>


