<!--- �Relayware. All Rights Reserved 2014 --->
<CFSETTING enableCfoutputOnly="yes">
<!---

File:			GetPhrase.cfm
Author:			DJH
Date created:	17 April 2000

Description:	Custom tag to allow listing, adding and updating of phrases.

Returns:

				Phrase variables are generated with the following format:
				Format depends whether exact match of language and country is required or whether a best match was required
				
				For exact match
					Phrase text for given language and country in: phr_#PhraseTextID#_#EntityType#_#EntityID#_#LanguageID#_#CountryID#
					Field name to use for editing and updating in: fld_#PhraseTextID#_#EntityType#_#EntityID#_#LanguageID_CountryID#
					If you are only dealing with a single entityid then #EntityType#_#EntityID#_ may be dropped
				
				For best Match
					If LanguageID is not set or exactMatch = 0 then the tag works out the language to show 
					for the current user and it is not appended to the variable names.

					Phrase text in: phr_#PhraseTextID#_#EntityType#_#EntityID#				
					If you are only dealing with a single entityid then #EntityType#_#EntityID#_ may be dropped ie use phr_#PhraseTextID#
					fld_ variable is returned but best not used since the actual language is not obvious 
				
								
				Where EntityTypeID and EntityID are 0 they are ignored.

				WAB comment:
				These items not done at moment, see comments in text				
				|		Also returns a variable defaulting to phr_PhraseFields containing a comma delimited list of all
				|		variable names that were defined.
				
				|		Also returns an array defaulting to phr_Language which has the languageid as the index and the language name as the
				|		value
				
				|		Also returns a structure defaulting to phr_entitytypes which has the tablename as the structure key and the 
				|		entitytypeid as the value.


Usage:			CF_GetPhrase	Action="Action"
								PhraseList="Phrase List"
								EntityType="Entity Type"
								EntityID="EntityID"
								CountryID="CountryID"
								Language="Language"
								DefaultLanguage="Default Language"
								Query="Query"
								>

Action:
Optional. Default is "List" Comma delimited list of the actions to perform:
 - "List" returns the relevant phrases translated. 
 - "Edit" allows the phrases to be edited. 
 - "Update" is used in an action template and processes and "Edit" operations just performed.

Phrase List:
Required. Comma delimited list of the phrases to translate. The list should NOT have list elements qualified with single quotes
as they will be added by the custom tag.

Entity Type:
Optional (Required if Entity parameter is used). Identifies the entity type this phrase is related to.

EntityID:
Optional (Required if EntityType parameter is used). Identifies the entities this phrase is related to.

Language:
Optional. Comma delimited list of languages of phrases to return. If "ALL" is specified, returns all language phrases. Can be in
an ID format or a language word format
Do not supply if you want the tag to work out the current language being used


Default Language:
Optional. Specifies an alternative default language to use if specified language phrase translation is unavailable. The default
default language (!) is English. Can be in an ID format or a language word format.

createBlankTranslations   default=false
determines whether a blank translation of a phrases should be added to the phrases table.
Usually assume that blank means that the translation has not been done, so would want to default to a different language 

exactMatch  1 or 0
Not usually necessary, but can be used to override default behaviour, which is ..
If a language and countryid is specified then it is assumed that an exact match is required
If no language and countryid is specified then the language comes from cookies and things and a best match is found

Query:
Optional. Specifies the name of a query to return phrase results in.
Defaults to Translations

Version history:
1

WAB 2000-06-16  modified so that if phrase is blank then record is not saved (or existing one is deleted)
			added created/lastupdated  by and date.

WAB Nov 2000 - added countryID to phrases system			

WAB Jan 2001 - complete overhaul to deal with countryids and to use new stored procedures


WAB 2004-07-06  WAB added lines to deal with phrases being held in memory - needs to reset them on save

WAB 2004-10-25  added an N for the insert into the nText field

AJC 2005-03-11  Changed conditional statement to not allow blank updates inc <p>&nbsp;</p>
--->

<!--- This variable is used to show which phrases aren't connected to an entityType
	it was set to -1, but this caused problems with variable names
	could be 99.
	0 is OK , except that entityTypeID 0 is person.  Should be OK since both entityTypeID and entityID are set to zero
 --->
<CFSET noEntityID = 0>


<cfparam name="attributes.debug" default="no">
<cfparam name="attributes.action" default="List">
<cfparam name="attributes.phrasefields" default="phr_PhraseFields">
<cfparam name="attributes.LanguageArray" default="phr_Languages">
<cfparam name="attributes.EntityTypeStruct" default="phr_EntityTypes">
<cfparam name="attributes.DefaultLanguage" default="1">     <!--- 1 is English --->
<cfparam name="attributes.allowTranslationLink" default=false>
<cfparam name="attributes.showNoTranslation" default=true>
<cfparam name="attributes.query" default="translations">
<cfparam name="attributes.evaluateVariables" default=true>
<cfparam name="attributes.nullText" default="">
<cfparam name="attributes.onNullShowPhraseTextID" default="no">



<cfparam name="attributes.createBlankTranslations" default=false>  <!--- determines whether a blank translation should be added.  Usually assume that blank means that the translation has not been done, so would want to default to a different language --->

<cfset sitedatasource="#application.sitedatasource#">   <!--- wab changed from caller. to application.  since caller does not necessarily have sitedatasource variable--->

<!--- Lets see what parameters are required and available --->
<cfif not isDefined("attributes.PhraseList") and attributes.action is not "update">  <!--- wab --->
	<!--- The PhraseList attribute parameter is required --->
	<cfoutput>
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_GetPhrase:</b></p> <p>Parameter <b>PhraseList</b> which is required was not supplied.</p></td>
		</tr>
	</table>
	</cfoutput>
	<cfexit>
</cfif>


<cfif isdefined("attributes.language") and not isdefined("attributes.countryid")
		or
	  not isdefined("attributes.language") and isdefined("attributes.countryid")>
	<!--- WAB 2001-01-08 added this test.  I think that it is the right thing to do--->
	<cfoutput>
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_GetPhrase:</b></p> 
		<p>Parameter language must be specified along with countryid  (and vice versa)
			[please contact William if you think that this logic is incorrect!!]
		</p></td>
		</tr>
	</table>
	</cfoutput>
	<cfexit>
	  
	  	  
</cfif>



<cfif isDefined("attributes.EntityID") is not isDefined("attributes.EntityType")>
	<!--- If Entity is Defined and not EntityType or vice versa, error --->
	<cfoutput>
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_GetPhrase:</b></p> <p>You have only defined one of the following attributes where
		either both, or none are required:</p>
		<ul><li><b>EntityID</b></li><li><b>EntityType</b></li></ul>
		</td>
		</tr>
	</table>
</cfoutput>
	<cfexit>
</cfif>

<cfparam name="attributes.EntityID" default="#noEntityID#">
<cfparam name="attributes.EntityType" default="">

<!--- get the id of this entitytype --->
<CFIF attributes.EntityType is not "">
	<cfset entityTypeID = application.entityTypeID[attributes.EntityType]>
<cfelse>
	<cfset EntityTypeID=0>
</cfif>




<!--- 
WAB: 2001-01-08 removed this, I don't think that it is used, and if it is needed then 
	we might as well make it an application variables

<!--- 
The fields returned could contain more than one language - lets create an array of all the languages so we can 
easily find them out 
--->
<cfif not isDefined("caller.#attributes.LanguageArray#")>
	<cfquery name="Language" datasource="#sitedatasource#">
	select
		Language,
		LanguageID	
	from
		Language
	</cfquery>
	
	<cfset tmp = ArrayNew(1)>
	<cfloop query="Language">
		<cfset tmp[Language.LanguageID] = Language.Language>
	</cfloop>
	<cfset "caller.#attributes.LanguageArray#" = tmp>
</cfif>	
--->


<!--- 
WAB: 2001-01-08 removed this, Idon't think that it is used, and if it is needed then 
	we might as well make them application variables
	
<!--- we will also create a structure containing the entity types. --->
<cfif not isDefined("caller.#attributes.EntityTypeStruct#")>

	<cfquery name="EntityType" datasource="#sitedatasource#">
	select
		entitytypeid,
		tablename
	from
		flagentitytype
	</cfquery>
	<cfset tmp = StructNew()>
	<cfloop query = "EntityType">
		<cfset dummy = StructInsert(tmp, EntityType.TableName, EntityType.EntityTypeID)>
	</cfloop>
	<cfset "caller.#attributes.EntityTypeStruct#" = structcopy(tmp)>
	

</cfif>
 --->




<!--- now do appropriate based on the actions for this call --->
<cfif attributes.action contains "List">
	
	<!--- 
	Let's see whether the languages have been passed as numeric or literal words - if all, get all the 
	Languages the current user has rights to view - 
	wab: only need to worry about the languages a person has rights to when they want to edit that language 
	
	**********  Bear in mind we may not know who the current user is. ****************
	
	--->

	<!--- mask the language parameter --->
	<!--- WAB: REMOVED <cfparam name="attributes.Language" default=""> --->

	<!--- 
	If Language has not been specified, we can fairly safely assume that we should set the language to the users language 	
	wab:  no this is not true - need to look at the cookie.defaultLanguageID setting first
	--->
	

	<cfinclude template = "templates\getPhraseLanguageAndCountry.cfm">


	<CFSET caller.requiredLanguage= requiredLanguage>

	<!---  need to decide whether we are looking for an exact language/country match or a best fit
		basically if attributes.language is not defined then it must be a best fit,
		
		but problem is if a single specific language is requested, maybe we are looking for a best fit
		for that language.
		
		I think that possibly if attributes.language is passed then attributes.country must also be  
		
	--->
	<cfif attributes.debug >
		<cfoutput>
		exec translateEntityPhrases
		@PhraseTextIDList = '#attributes.PhraseList#',
		@EntityIDList = '#attributes.EntityID#',
		@EntityType = '#attributes.EntityType#',
		@RequiredCountry = '#RequiredCountry#',
		@RequiredLanguageList = '#RequiredLanguage#',
		@FallbackLanguage ='#attributes.defaultLanguage#',
		@ExactMatch = #iif(attributes.exactMatch,DE('1'),DE('0'))#,
		@DoAnalysis = '#iif(attributes.exactMatch,DE('Yes'),DE('No'))#',
		@NullText = '#attributes.nulltext#',
		@onNullShowPhraseTextID = '#iif(attributes.onNullShowPhraseTextID,DE('1'),DE('0'))#'</cfoutput>
	</cfif>
	
	<CFSTOREDPROC PROCEDURE="translateEntityPhrases" DATASOURCE="#application.siteDataSource#" debug="true">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PhraseTextIDList" VALUE="#attributes.PhraseList#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@EntityIDList" VALUE="#attributes.EntityID#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@EntityType" VALUE="#attributes.EntityType#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@RequiredCountry" VALUE="#RequiredCountry#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@RequiredLanguageList" VALUE="#RequiredLanguage#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@FallbackLanguage" VALUE="#attributes.defaultLanguage#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ExactMatch" VALUE="#iif(attributes.exactMatch,DE('1'),DE('0'))#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@DoAnalysis" VALUE="#iif(attributes.exactMatch,DE('Yes'),DE('No'))#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@NullText" VALUE="#attributes.nulltext#" >
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@onNullShowPhraseTextID" VALUE="#iif(attributes.onNullShowPhraseTextID,DE('1'),DE('0'))#" >
		
		<CFPROCRESULT NAME="getPhrases" RESULTSET="1"> 
		<CFPROCRESULT NAME="Headings" RESULTSET="2"> 
		<CFPROCRESULT NAME="Data" RESULTSET="3"> 
		
	</CFSTOREDPROC>		
		<!--- Create a list of variables which contain the required phrases --->	
		<CFLOOP QUERY="getPhrases">
			
			<!--- Evaluate any Cold Fusion Variables in the phrase --->
			<CFSET thisPhraseText = phraseText>
			<CFIF attributes.evaluateVariables and refind ("##[[:graph:]]*##",thisPhraseText) is not 0>
				<CFTRY>
					<CFSET thisPhraseText = evaluate('"#thisPhraseText#"')>
				<CFCATCH>
				</CFCATCH>
				</CFTRY>
			</CFIF>


			<CFIF isdefined("cookie.translator") and attributes.allowTranslationLink>		
				<CFSET "caller.phr_#identifier#" = '#thisPhraseText# #translationLink#>T</A>'>
				<CFSET "caller.phr_#quickidentifier#" = '#thisPhraseText# #translationLink#>T</A>'>
			<cfelse>
				<CFSET "caller.phr_#identifier#" = '#thisPhraseText#'>
				<CFSET "caller.phr_#quickidentifier#" = '#thisPhraseText#'>
			</CFIF>
		
			<CFSET "caller.fld_#identifier#"  = "phrupd_#updateidentifier#">
			<CFSET "caller.fld_#quickidentifier#"  = "phrupd_#updateidentifier#">
			<CFSET "caller.fldstub_#quickidentifier#"  = "phrupd_#updateidentifierstub#">		
		
			<cfif attributes.debug is "yes">
				<cfoutput>
				variable phr_#htmleditformat(identifier)# and phr_#htmleditformat(quickidentifier)#set to #evaluate("caller.phr_#htmleditformat(identifier)#")#<BR>
				variable fld_#htmleditformat(identifier)# set to #evaluate("caller.fld_#htmleditformat(identifier)#")#<BR>
				</cfoutput>
			</cfif>
			

		</CFLOOP>		


	
	<cfif attributes.debug is "yes">
		<cfoutput>
		<p>
		Default Language is set to #htmleditformat(attributes.DefaultLanguage)#<br>
		Chosen Language is set to #htmleditformat(requiredLanguage)# <br>
		Chosen Country is set to #htmleditformat(requiredCountry)# <br>
		</p>
		</cfoutput>	
	</cfif>

		<cfset "caller.#attributes.query#" = getPhrases>
	<CFIF attributes.exactMatch>
		<cfset caller.PhraseAnalysisheadings = headings>
		<cfset caller.PhraseAnalysisdata =data>
	</cfif>	
	
</cfif>


<cfif attributes.action contains "Update">

	<!--- phrases will be updated here --->
	<!--- assume that the form fields have been created with the format:
		phr_#phrasetextid#_#entityType#_#entityid#_#language#_#countryid#
	 --->

	<CFIF isDefined ("caller.form.fieldnames")>
		<cfloop list="#caller.form.fieldnames#" index="afield">

		<cfif left(afield,7) is "phrupd_">

				<!--- process the field - it's one we're interested in. --->
				<cfset aPhrase = evaluate("#afield#")	>

				<!--- use a regular expression to extract the stuff --->
				<CFSET struct = reFindNoCase("phrupd_([a-zA-Z0-9_]+)_([0-9]+)_([a-zA-Z0-9]+)_([0-9\-]+)_([0-9]+)$",aField,1,"true")>					
				
					<CFSET positionArray = struct.pos>
					<CFSET LenArray = struct.len>					

				<cfif LenArray[1] is 0>
					<!--- if we don't find the first regular expression, then it is possible that the fieldname doesnot have
						the language and countryid bits 
						in this case the variables phrCountryID and phrLanguageID should be set
						--->
					<CFSET struct = reFindNoCase("phrupd_([a-zA-Z0-9_]+)_([0-9]+)_([a-zA-Z0-9]+)[_]*$",aField,1,"true")>				
					<cfset countryid = phrCountryID>
					<cfset languageid = phrLanguageID>
					<CFSET positionArray = struct.pos>
					<CFSET LenArray = struct.len>					
				<cfelse>
					<cfset countryid = mid(aField,positionArray[6],LenArray[6])>
					<cfset languageid = mid(aField,positionArray[5],LenArray[5])>
				</cfif>
				
	
				<cfset entityID = mid(aField,positionArray[4],LenArray[4])>
				<cfset entityTypeID = mid(aField,positionArray[3],LenArray[3])>
				<cfset phraseTextID = mid(aField,positionArray[2],LenArray[2])>
	

				
				<!---  it is possible that the entityID is not numeric - 
					in the case of a record having just been added 
					we need to find the recordid
					either it will have been passed to tag as a parameter (eg newRecord = 123)
					or it will be a variable in the calling page
					--->
				<CFIF not isNumeric(entityID)	>
					<CFIF isDefined("#attributes.entityID#")>
						<CFSET entityID = evaluate("#attributes.entityID#")>
					<CFELSEIF isDefined("caller.#entityID#")>
						<CFSET entityID = evaluate("caller.#entityID#")>
					<CFELSE>
						<CFOUTPUT>#entityID# is not defined</cfoutput>
						<CFBREAK>
					</CFIF>
			
				</cfif>
	
				<!--- only do updates/inserts if country and language are set  --->
				<cfif isNUmeric(countryid) and isNUmeric(languageid)>
						
					<!--- this bit resets the structure holding the translations in memory 
						entity names rather than ids are used, so have to convert
					--->
	
					<cfif EntityTypeID is not 0>
						<cfset entityname = application.entityType[EntityTypeID].tablename>
						<cfset keytodelete = "#phrasetextid#_#entityname#_#entityid#">
					<cfelse>
						<cfset keytodelete = "#phrasetextid#">
					</cfif>
					<cfset x = application.com.relaytranslations.resetPhraseStructureKey(keytodelete)>

					<!--- test whether phrase has already been created in the phraselist (header table)--->
					<CFQUERY NAME="Phrase_Exists" DATASOURCE="#SiteDataSource#">			
					select 
						* 
					from 
						phraselist 
					where 
						phraseTextId =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						and entitytypeid =  <cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and entityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
		
					<CFIF Phrase_Exists.recordCount IS 0>
						<!--- doesn't exist so create --->
						<CFQUERY NAME="CreatePhrase" DATASOURCE="#SiteDataSource#">			
						insert into phraseList 
						(
							phraseid,
							PhraseTextID,
							EntityTypeID,
							EntityID,
							created,
							createdby
						)
						Select
							max(phraseid) + 1, 
							<cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
							<cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
							<cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							getDate(),
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
	
						from 
							phraseList
						</cfquery>			


					</cfif>
					
					<!--- check whether translation exists --->
					<CFQUERY NAME="Translation_Exists" DATASOURCE="#SiteDataSource#">			
						select 
							phrases.* 
						from 
							phraselist, 
							phrases
						where 
							phrases.phraseid = phraseList.phraseid
							and phraseTextId =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
							and languageid =  <cf_queryparam value="#languageid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and countryid =  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and entitytypeid =  <cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and entityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
	
					<CFIF Translation_Exists.recordCount is 0 and not (aPhrase is  "" and not attributes.createBlankTranslations)>
		
						<CFQUERY NAME="Phrase_Insert" DATASOURCE="#SiteDataSource#">
							Insert Into Phrases  
							(
								Phraseid,
								languageid,
								Countryid,
								phraseText,
								created,
								createdby,
								lastupdated,
								lastupdatedby
							)
							select 
								phraseid, 
								<cf_queryparam value="#languageid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
								<cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" >,	
								<cf_queryparam value="#aPhrase#" CFSQLTYPE="CF_SQL_VARCHAR" >,
								getDate(),
								<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,							
								getDate(),
								<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >							
							from 
								phraselist 
							where 
								phraseTextId =  <cf_queryparam value="#phraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
								and entitytypeid =  <cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" > 
								and entityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFQUERY>		

			
					<CFELSEIF 	Translation_Exists.recordCount is not 0 
								and (not (aPhrase is "" or aPhrase is "<p>&nbsp;</p>") and not attributes.createBlankTranslations)
								and compare(Translation_Exists.phraseText,aPhrase) is not 0
								>
							<CFQUERY NAME="Phrase_Update" DATASOURCE="#SiteDataSource#">
							UPDATE 
								Phrases
							SET 
								Phrasetext =  <cf_queryparam value="#aPhrase#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
								lastUpdated = getDate(),
								lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >							
							from 
								phrases
							where 
								phrases.ident =  <cf_queryparam value="#Translation_Exists.ident#" CFSQLTYPE="CF_SQL_Integer" > 
							</CFQUERY>
					<CFELSEIF Translation_Exists.recordCount is not 0 and aPhrase is "" and not attributes.createBlankTranslations>
							<CFQUERY NAME="Phrase_Delete" DATASOURCE="#SiteDataSource#">
							Delete 
							from 
								phrases
							where 
								phrases.ident =  <cf_queryparam value="#Translation_Exists.ident#" CFSQLTYPE="CF_SQL_Integer" > 
							</CFQUERY>
	
					</cfif>
		

					<!--- update defaultLanguage if necessary --->
					<!--- currently only done if phrase exists --->
					<cfif translation_exists.recordcount is not 0>
						<cfif StructKeyExists(form,"phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#")>
						
							<cfset defaultLanguageForThisCountry = form["phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#"]>

							<cfif (defaultLanguageForThisCountry is languageid and Translation_Exists.defaultForThisCountry is not 1)
								or (defaultLanguageForThisCountry is not languageid and Translation_Exists.defaultForThisCountry is  1)>
								

							<CFQUERY NAME="Phrase_Update" DATASOURCE="#SiteDataSource#">
							UPDATE 
								Phrases
							SET 
								<cfif StructKeyExists(form,"phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#")>
									<cfset defaultLanguageForThisCountry = form["phrDefaultLang_#phraseTextID#_#entityTypeID#_#entityid#_#countryID#"]>
									<cfif defaultLanguageForThisCountry is languageid>
									defaultForThisCountry = 1,
									<cfelse>
									defaultForThisCountry = 0,
									</cfif>
								</cfif>
								lastUpdated = getDate(),
								lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >							
							where 
								phrases.ident =  <cf_queryparam value="#Translation_Exists.ident#" CFSQLTYPE="CF_SQL_Integer" > 
							</CFQUERY>

							</cfif>
						</cfif>


						<cfif structKeyExists(form,"frmPhrDefaultForThisCountry")>
						
							<CFQUERY NAME="Phrase_Update" DATASOURCE="#SiteDataSource#">
							UPDATE 
								Phrases
							SET 
								defaultForThisCountry = <cfif form.frmPhrDefaultForThisCountry is 1>1<cfelse>0</cfif>,
								lastUpdated = getDate(),
								lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >							
							where 
								phrases.ident =  <cf_queryparam value="#Translation_Exists.ident#" CFSQLTYPE="CF_SQL_Integer" >  
								AND defaultForThisCountry <>  <cfif form.frmPhrDefaultForThisCountry is 1>1<cfelse>0</cfif>
							</CFQUERY>

						</cfif>
					</cfif>
	
				</cfif>
		
			</cfif>
		
		</cfloop>
	</cfif>

	
</cfif>

<CFSETTING enableCfoutputOnly="no">


