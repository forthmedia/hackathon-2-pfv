<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		checkMandatoryParams.cfm
Author:			Simon WJ
Date created:	23 August 2001

	Objective - Use this tag to show a decent error message to the user if 
				the parameters are not correctly passed to a screen.  This will also log
				error messages and notify support. 
		
	Syntax	  -	<CF_checkMandatoryParams callingTemplate="dataFrame.cfm" relayParam="fieldNames">
	
	Parameters  relayParam - this is the parameter that we are checking when we call the tag
				callingTemplate - this is the calling tamplate name 
				
	Return Codes - none

Amendment History:

DD-MMM-YYYY		Initials 	What was changed
2001-08-23		SWJ			First version created

Enhancement still to do:

1.  looping

--->

<CFPARAM name="attributes.relayParam">
<CFPARAM NAME="attributes.callingTemplate">

<CFIF isDefined('Cookie.USER')>
	<CFQUERY NAME="getUserName" DATASOURCE="#application.siteDataSource#" DBTYPE="ODBC">
		select Name from userGroup 
			where userGroupID = #request.relayCurrentUser.usergroupid#
	</CFQUERY>
	<cfset UserName = getUserName.Name>
<CFELSE>
	<cfset UserName = "User unknown">
</CFIF>

<CFLOOP INDEX="i" LIST="attributes.relayParams">

	<CFIF not isdefined("attributes.relayParam")>
		<TABLE BORDER="2" CELLPADDING="3" BGCOLOR="White">
		<TR>
			<TD><CFOUTPUT>#htmleditformat(attributes.callingTemplate)#.cfm requires #htmleditformat(i)#.  Please contact
				technical support for assistance telling them you have got this 
				message and have come from #htmleditformat(HTTP_REFERRER)#</CFOUTPUT>
				<cfif Server.OS.Version gt 5>
					<CFLOG TEXT="#UserName# got an error using #attributes.callingTemplate#.cfm missing #i#. They came from #HTTP_REFERER#" 
					LOG="APPLICATION" FILE="RelayError" 
					TYPE="Error" DATE="yes" TIME="yes" APPLICATION="yes">
				</CFIF>
			</TD>
		</TR>
		</TABLE>
		<CF_ABORT>
	</cfif>
</CFLOOP>

