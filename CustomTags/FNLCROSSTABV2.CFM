<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
FNL Cross Tab

A custom Tag for displaying a cross Tab report

Authors:  WAB and AH

Date: end of 1999 probably

Mods:  WAB Jan 2001 
		Created new version which allows the various column names and things to be set dynamically
	2001-05-15	Modified so that works with cfsetting cfoutputonly enabled
	WAB 2016-11-29 During PROD2016-2837 (work on analysePhraseRequest.cfm which uses this tag), altered the HTMLEditFormat() to santiseHTML()) 

Usage:

The tag is supplied with two queries
One query (Attributes.ColumnHeadingQuery) defines the columns which will be present 
in the report.  These must be in the same order as they will appear in the data query
The "index" column in this query is defined as attributes.ColumnIDField  (must be a single field)
The actual headings which appear can be different.  Define an eval string with  attributes.ColumnHeadingEval

Second Query (Attributes.DataQuery) defines the data
This also needs an "index" column defined as attributes.RowIDField  (This can actually be an eval string combining more than one column)
The row headings can be defined with attributes.RowHeadingEval

The actual value which appears in the grid is defined by attributes.ValueEval

If you want to be able to download the results then set Attributes.DownloadLink = Yes

If you want to be able to click on an item in the grid then set 
Attributes.doDrillDown = yes
You will then need to supply further attributes

	Attributes.DrillDownAction  ie. template to call
	and the names of the form variable which this template is expecting.  
	Attributes.DrillDownColumnName  
	Attributes.DrillDownRowName
	
	so the template will get parameters   #drilldownColumnName# = value of columnIDField
							and 			#drilldownRowName# = value of rowIDField

 --->


<!---Override Default Styles in order to make smaller letters for the comlumn headings--->
<cfoutput>
<STYLE>
 .Headings  {
                  	font-family : 'Arial', 'Verdana', 'Sans-Serif';
                  	font-size : 8pt;
                  	font-weight : normal;
                  	color : Red;
					background-color : ##C7C7C7;
                  }
				  
  A:Hover  {
                  	font-family : 'Arial', 'Verdana', 'Sans-Serif';
                  	font-weight : bold;
                  	color : Red;
                  	font-size : 9pt;
                  }
				  
.Numbers	{ font-size : 9pt; }
</STYLE>
</cfoutput>


<CFPARAM NAME="Attributes.ColumnHeadingQuery">
<CFPARAM Name ="attributes.ColumnIDField" default = "columnID"> 
<CFPARAM Name ="attributes.ColumnHeadingEval" default = "#attributes.ColumnIDField#"> 

<CFPARAM NAME="Attributes.DataQuery">
<CFPARAM Name ="attributes.RowIDField" default="rowID"> 
<CFPARAM Name ="attributes.RowHeadingEval" default = "#attributes.RowIDField#"> 
<CFPARAM Name ="attributes.ValueEval" default = "number"> 


<CFPARAM NAME="Attributes.DownloadLink" DEFAULT="No">

<CFPARAM NAME="Attributes.doDrillDown" DEFAULT="No">
<cfif Attributes.doDrillDown is "yes">
	<CFPARAM NAME="Attributes.DrillDownAction">
	<CFPARAM NAME="Attributes.DrillDownColumnName">
	<CFPARAM NAME="Attributes.DrillDownRowName">
	<CFSET ColumnName = #Attributes.ColumnName#>
	<CFSET RowName = #Attributes.RowName#>
	<CFSET Action = #Attributes.DrillDownAction#>

	<cfset doDrillDown = true>
<cfelse>
	<cfset doDrillDown = false>
</cfif>


<!--- USE PASSED QUERIES WITHIN THIS CODE AS "Headings" and "Data" --->
<cfset headings = attributes.ColumnHeadingQuery>
<cfset data = attributes.DataQuery>



<!---Catch Parameters passed through the Custom Tag--->


<CFSET DownLoadLink = #Attributes.DownloadLink#>
<CFIF 	DownloadLink is "Yes" and (not isdefined("attributes.DownloadLocalPath") or not isdefined("attributes.DownloadWebPath"))>
	Download File path not specified
<CFELSEIF DownloadLink is "Yes">
	<CFSET DownloadLocalPath  = attributes.DownloadLocalPath>
	<CFSET DownloadWebPath  = attributes.DownloadWebPath>
</CFIF>




<!---Create a list of ColumnIDs--->
<CFSET  columnids = evaluate("valuelist(Headings.#attributes.columnidField#)")>


<cfif doDrillDown>
	<!---Dynamically set values of the hidden fields---> 
	<CFOUTPUT>
	<SCRIPT>
	
	function drilldown(colvalue,rowvalue){
	form = window.document.myForm
	form.#jsStringFormat(columnName)#.value = colvalue
	form.#jsStringFormat(rowName)#.value = rowvalue
	form.submit()
	
	}
	
	
	</SCRIPT>

	<!---Dynamically define the Form Action and other two required attributes, all of this passed as
	the parameter "Action" through the Custom Tag--->
	<FORM ACTION="#action#" METHOD="post" NAME="myForm">
	
	<CF_INPUT TYPE="hidden" NAME="#ColumnName#" VALUE="">
	<CF_INPUT TYPE="hidden" NAME="#RowName#" VALUE="">
	
	
	</FORM>

	</CFOUTPUT>

</cfif>


<!--- also build a file which can be downloaded - initialise variable to hold details--->
<!--- actually build the variable whether it is wanted or not --->
<CFSET downloadString="">
<CFSET delimiter=chr(9)>
<CFSET newline= chr(10) >


<CFIF DownloadLink is "Yes">
	<!---Delete previous file left by the same user--->
	<CFIF #FileExists(DownloadLocalPath)# IS "Yes">
		<CFFILE ACTION="DELETE" FILE="#DownloadLocalPath#">
	</CFIF>
</cfif>

<cfoutput>
<TABLE border=1>

<TR><TD></TD>
</cfoutput>
<!---Output Column Headings--->
<CFOUTPUT query="Headings">
	<TD class="headings">#application.com.security.sanitiseHTML(evaluate(attributes.columnheadingEval))#</TD>
	<CFSET downloadString = downloadString & delimiter & evaluate(attributes.columnheadingEval)> 

</cfoutput>

<cfoutput></TR> </cfoutput>


<CFSET downloadString = downloadString & newline>

<!---Output Rowheadings--->
<CFOUTPUT query="Data" group="#attributes.rowidField#">
	
	<TR>
		<TD>#application.com.security.sanitiseHTML(evaluate(attributes.rowheadingEval))#</TD>
		<CFSET downloadString = downloadString & evaluate(attributes.rowheadingEval)>
	<CFSET counter = 1>
	


	<CFOUTPUT>

<!---Compare the list columnids with record set columnid from the Query, where the data is the same output
numbers, where it is not (NULL positions in the record set) put non-breaking space instead--->

		<CFLOOP condition="counter LTE headings.recordcount and counter LTE listlen(columnids)  and listgetat(columnids,counter) is not evaluate(attributes.columnidField) ">
																<!---  have to check for completelt null columns--->
		<TD>&nbsp;</tD>	
			<CFSET downloadString = downloadString & delimiter>
			<CFSET counter = counter+1>
		</cfloop>
		
		<cfif counter LTE headings.recordcount ><!--- this is needed to guard against completely null rows --->
		<TD align="right" class="numbers">
			<cfif doDrillDown>
				<A HREF="javascript:drilldown('#evaluate(attributes.ColumnIDField)#','xxx')">#htmleditformat(evaluate(attributes.ValueEval))#</A>
			<cfelse>
				#htmleditformat(evaluate(attributes.ValueEval))#
			</cfif>
			
		</tD>	
			<CFSET downloadString = downloadString & delimiter & evaluate(attributes.ValueEval)>
			<CFSET counter = counter+1>
		</cfif>

			
	</cfoutput>

	
	<CFLOOP condition = "counter LTE headings.recordcount">
	<TD>&nbsp;</tD>		
			<CFSET downloadString = downloadString & delimiter>
			<CFSET counter = counter+1>
	</CFLOOP>


	 <CFSET downloadString = downloadString & newline>


</TR>
</cfoutput>
	 
<cfoutput>
</table>
</cfoutput>

<CFIF DownloadLink is "Yes">

	<CFFILE ACTION="WRITE" FILE="#DownloadLocalPath#" OUTPUT="#downloadString#">

	<A HREF="<CFOUTPUT>#htmleditformat(DownloadWebPath)#</CFOUTPUT>">Download this table</A>
</CFIF>	




