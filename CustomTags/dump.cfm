<!--- 
2016-10-25 PPB created


ASSUMPTIONS:
file type is html

ATTRIBUTES:
ref =  a short code that identify the module and source code location eg MDFI_037   (MDF Insert dump 37)
		the part upto the "_" goes in the middle of the filename; the whole ref is used in the dump label and when checking against restrictedRefs ;  also helps us find them in source code for removal
restricted - if exists the dump doesn't happen unless dumpSettings.cf_dump_restrictedRefs contains it

POSSIBLE EXTENTIONS:
we could check the file size and delete it if it is large ; I want to time the overhead of doing this as the files only last 1 day so may not be worth it
delete the file based on a session param
add a url link to dump file in email 

NOTES:
see the wiki entry CF_DUMP for usage instructions

 --->

<!--- Will suggested using the "errors" struct because it persists across logins; adding a substruct of errors is probably more messy (to always check it exists) that prefixing all vbles with "cf_dump_" --->
<!--- <cfset dumpSettings = session.relayCurrentUserStructure.errors>	 --->		
<cfset dumpSettings = request.relayCurrentUser.errors>								<!--- when we set the session vbles we copy them to request scope; I'm not sure which is most appropriate here  --->	

<!--- if its restricted, check if the user wants this dump --->  
<cfif NOT StructKeyExists(attributes,"restricted") OR NOT attributes.restricted OR (attributes.restricted AND StructKeyExists(dumpSettings,"cf_dump_restrictedRefs") AND ListFindNoCase(dumpSettings.cf_dump_restrictedRefs,attributes.ref))>		<!---  ideally would match on the first part of the ref eg restrictedRef=MDFI finds MDFI_023 but not AMDFI_012 --->

	<cfset dump_header = "#DateFormat(now(),"yyyy-mm-dd")# #TimeFormat(now(),"HH:nn")#">

	<cfif StructKeyExists(attributes,"ref")>
		<cfset dump_header = dump_header & " " & #attributes.ref#>
	</cfif>
	
	<cfset callingFunction = application.com.globalFunctions.getFunctionCallingFunction()>

	<cfset dump_header = dump_header & " " & callingFunction.template & " line " & callingFunction.lineNumber >

	<!--- auto generated filename (also used for the subject of an email if one is sent) --->	
	<cfset dump_filename = "dump_#DateFormat(now(),"yyyymmdd")#">
	
	<cfif StructKeyExists(attributes,"ref")>															<!--- a few chars to add to filename typically to identify source of call eg module and source code location --->
		<cfset dump_filename = dump_filename & "_" & #ListFirst(attributes.ref,'_')#>					<!--- the ref upto the first "_" --->
	</cfif>
	
	<cfif StructKeyExists(dumpSettings,"cf_dump_filenameInfix") AND dumpSettings.cf_dump_filenameInfix neq "">												<!--- a few chars to add to filename typically to identify user eg PPB so that I only see dumps for my session not mixed up with others --->
		<cfset dump_filename = dump_filename & "_" & #dumpSettings.cf_dump_filenameInfix#>
	</cfif>

	
	<cfif StructKeyExists(attributes,"label")>
		<cfset dump_label = #attributes.label#>
	<cfelse>
		<cfset dump_label = "">																					<!--- ideally would use the name of the var as the default label if one isn't provided  --->
	</cfif>
	
	<cfif StructKeyExists(dumpSettings,"cf_dump_output") AND dumpSettings.cf_dump_output neq "">				<!--- allow the user to send in their own filename or divert output to "browser"  --->
		<cfset dump_output = dumpSettings.cf_dump_output>			
	<cfelseif StructKeyExists(attributes,"output")>																<!--- allow the developer to send in a specific filename to override the generated one  --->
		<cfset dump_output = attributes.output>			
	<cfelse>
		<!--- 
		Will suggested the temp file be put in com.fileManager.getTemporaryFolderDetails() to give a session related temp folder which is web-accessible
		advantage: a link to be sent in the email rather than the full output (which gets squashed if its a wide query)
		disadvantage: its not so easy to see dump output from other person's sessions
		best of both: we could have create a cf_dump folder in \userfiles\content\filesTemp (as per connector); we would need a housekeeping task to purge it periodically
		--->
		<cfset dump_output =  "#application.paths.temp#\#dump_filename#.html">								
	</cfif>

	
	<cfif isSimpleValue(attributes.var)>
		<cfset dumpVar = dump_label & ": " & attributes.var>												<!--- for simple values label isn't output by cfdump so tack it onto the front of the var --->
	<cfelse>
		<cfset dumpVar = attributes.var>
	</cfif>
	
	<cfif StructKeyExists(dumpSettings,"cf_dump_email") AND dumpSettings.cf_dump_email neq "" AND FindNoCase('@relayware.com',dumpSettings.cf_dump_email)>
		<cfmail to="#dumpSettings.cf_dump_email#" from="dump@relayware.com" subject="#dump_filename#" type="html">
			<cfdump var="#dump_header#">
			<cfdump var="#dumpVar#" label="#dump_label#">
		</cfmail>
	<cfelse>
		<cfdump var="#dump_header#" output="#dump_output#" format="html">
		<cfdump var="#dumpVar#" label="#dump_label#" output="#dump_output#" format="html">
	</cfif>
</cfif>
