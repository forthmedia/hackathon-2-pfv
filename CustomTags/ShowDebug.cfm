<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		ShowDebug.cfm
Author:			SWJ
Date created:	2 Aug 2000

Description:	Custom tag to allow listing, adding and updating of phrases.

Returns:
				


Usage:			
	<CF_ShowDebug	
		TemplateName="HomePage.cfm"
		Section="inside xyz loop"
		variables="var1,var2,var3"
	>

		Used by the body of the tag
		
			TemplateName:
				Required. This is the name of the template that your calling from.
		
			Section:
				Required. Tells you where in the template as this tag can be called multiple times
					
			Variables:
				Required. Comma separated list of variable names which are looped around to show there settings.
		


Date Tested:
Tested by:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
18 May 2001	SWJ	I changed the show test so that it checks for a cookie.CFShowDebug.  
			This can be set by linking to setShowDebug.cfm

--->


<!--- Check the Syntax of the Tag --->

<!--- Required parameters --->
<CFIF not isDefined("attributes.TemplateName")>  
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_ShowDebug:</b></p> <p>Parameter <b>templateName</b> which is required must contain the template name.</p></td>
		</tr>
	</table>
	<cfexit>
<CFELSE>
	<CFSET Section ="#attributes.Section#">
</CFIF>

<CFIF not isDefined("attributes.Section")>  
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_ShowDebug:</b></p> <p>Parameter <b>Section</b> which is required must describe the section of the template you are calling this from.</p></td>
		</tr>
	</table>
	<cfexit>
<CFELSE>
	<CFSET Section ="#attributes.Section#">
</CFIF>

<CFIF not isDefined("attributes.variables")>  
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_ShowDebug:</b></p> <p>Parameter <b>variables</b> which is required must contain a comma separated list of variables for processing.</p></td>
		</tr>
	</table>
	<cfexit>
<CFELSE>
	<CFSET variables ="#attributes.variables#">
</CFIF>


<CFIF isDefined('cookie.FNLshowDebug')> 
	<CFIF cookie.FNLshowDebug IS "Yes" > 
		<CFOUTPUT>
		<TABLE BORDER="2" BGCOLOR="Yellow">
			<tr>
			<TD BGCOLOR="Yellow"><p>
				<B><FONT COLOR="Red">Debug info for template:</FONT></B> #htmleditformat(attributes.templateName)#<BR>
				<B>Section of template:</B> #htmleditformat(attributes.section)#<BR>
				<B>Variables are:</B><BR>
				<CFLOOP INDEX="variable" LIST="#attributes.variables#">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#htmleditformat(variable)# = <B><CFIF isdefined('variable')>#htmleditformat(evaluate(variable))#<CFELSE>undefined</CFIF></B><BR>
				</CFLOOP>
			<CFIF isdefined('FieldNames')> 
			  <CFLOOP INDEX="i" LIST="#FieldNames#">
			  	#htmleditformat(i)# = #htmleditformat(evaluate(i))#
			  </CFLOOP>
			</CFIF>
			</p></TD>
			</tr>
		</table>
		</CFOUTPUT>
	</CFIF>
</CFIF>

