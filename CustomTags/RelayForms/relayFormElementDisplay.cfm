<!--- 
WAB 2016-11-07 FormRenderer Moved this function to relayDisplay.cfc
--->

<cfif (thisTag.executionMode eq "end" and thisTag.hasEndTag) or not thisTag.hasEndTag>
	<cfif thisTag.hasEndTag>
		<cfset attributes.html = thisTag.generatedContent>
		<cfset thisTag.generatedContent = "">
	</cfif>	

	<!--- WAB 2010/09/28 --->
	<cfif not listfindnocase(getbasetaglist(),"CFFORM")>
		<cfset argumentCollection.useCFFORM = false>
	</cfif>

	<cfoutput>#application.com.relayDisplay.relayFormElementDisplay(argumentCollection = attributes)#</cfoutput>
</cfif>
