<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayFormElement.cfm
Author:				SWJ
Date started:		2006-03-27

Description:		If no layout controls are needed <CF_relayFormElement> can be called directly
					to return the form elements without layout and labels.
					For relay layout to be applied it should bot be called directly but through
					<CF_relayFormDisplay> / <CF_relayFormElementDisplay>.

					For complete documentation on relayforms, please see
					\\Fnl-usa2\F N L\Relay Documentation\RelayWare Core Technology\RelayFormArchitecture.doc

Amendment History:

Date (YYYY-MM-DD)	Initials 	Version		What was changed

11-May-2006			GCC			1.1			Restructuring work to further separate form elements form layout
06-June-2006		NJH			1.2			CR-LEN507 Added button and file types

2008/05/21			WAB			minor mod put cfcase values in a list
2009/03/13			NJH			CR-SNY675 Moved some code into here from various input files in an effort to consolidate it. This includes:
								1. setting a message
								2. setting the label to be the fieldname if it's empty.
								3. translating the message and the label
2009/04/17			NJH			Sophos Stargate 2 projects - replaced single quotes in translated message.
2014-08-11			AXA			added param for attributes.type to fix bootstrap error on partner portal
2015-06-09			AHL			Case 444625 allowing empty null fields and provide them with a default null phrase
2015-06-30			WAB			Reverted the change for Case 444625 above.  Had too many knock on effects of null Text appearing where it should not (for example in radio groups).
								Probably worth another try at some points
2016-01-28			WAB			BF-255 Performance. Remove Unneccesary translates.  If leads to any problem with single quotes and cfForm javascript then get rid of the cfform
2016/02/05			NJH			Add support for type and changed references to relayFormElementType to type so that type can be support for relayFormElement itself. should be backwards compatible.
2016-12-21			WAB			PROD2016-3019 Removed the blank defaults for nullText and nullValue.  nullText is actually defaulted to phr_Ext_SelectAValue in relayforms.validValueDisplay and is not required by any other templates.  By setting this blank default we lose nullText throughout the system
Possible enhancements:

	1. standard cases - country drop down, date pickers etc

 --->
<cfsetting enablecfoutputonly="true">
<cfparam name="attributes.maxLength" type="numeric" default="50">
<cfparam name="attributes.disabled" default="false" >
<cfparam name="attributes.validateAt" default="onSubmit,onServer">
<cfparam name="attributes.validate" default="">
<cfparam name="attributes.onValidate" default="">
<cfparam name="attributes.message" default="">
<cfparam name="attributes.onChange" default="">
<cfparam name="attributes.onClick" default="">
<cfparam name="attributes.pattern" default="">
<cfparam name="attributes.class" default="form-control">
<cfparam name="attributes.required" type="string" default="false"> <!--- NJH 2009/03/13 CR-SNY675 --->
<cfparam name="attributes.label" default=""> <!--- NYB 2009/03/18 Demo Bugs - added --->
<cfparam name="attributes.relayFormElementType" type="string" default="text">
<cfparam name="attributes.type" type="string" default="#attributes.relayFormElementType#">

<cfparam name="attributes.requiredFunction" type="string" default="">

<cfparam name="attributes.id" default = "#attributes.fieldname#">
<cfparam name="attributes.name" default = "#attributes.fieldname#"> <!--- NJH 2016/12/19 PROD2016-3000 - param the name attribute --->

<cfif attributes.requiredFunction neq "">
	<cfset request.refreshFormRequiredClass = true>
</cfif>

<!--- NJH 2015/08/12 JIRA PROD2015-19 - pass through data attributes --->
<cfif structKeyExists(attributes,"dataAttributes") and isStruct(attributes.dataAttributes)>
	<cfloop collection="#attributes.dataAttributes#" item="dataAttribute">
		<cfset attributes["data-#dataAttribute#"] = attributes.dataAttributes[dataAttribute]>
	</cfloop>
</cfif>

<!--- NJH 2009/03/13 CR-SNY675 moved here to consolidate some code. This code was in various files and should be the default for any input type --->

<!--- catch to display 'useful' info if a label has not been supplied --->
<cfif attributes.label eq "">
	<cfset attributes.label = attributes.fieldName>
</cfif>

<cfif attributes.message eq "">
	<cfswitch expression="#attributes.type#">
		<cfcase value="numeric">
			<cfset attributes.message="#attributes.label# Phr_Sys_formValidation_ValidNumber">
		</cfcase>
		<cfdefaultcase>
			<cfif attributes.validate neq "">
				<cfswitch expression="#attributes.validate#">
					<cfcase value="email">
						<cfset attributes.message="#attributes.label# Phr_Sys_formValidation_Email">
					</cfcase>
					<cfdefaultcase>
						<cfset attributes.message="#attributes.label# Phr_Sys_formValidation_Valid">
					</cfdefaultcase>
				</cfswitch>
			</cfif>
		</cfdefaultcase>
	</cfswitch>
	<cfif attributes.required>
		<cfif attributes.message neq "">
			<cfset attributes.message= attributes.message & " Phr_and Phr_Sys_formValidation_Req">
		<cfelse>
			<cfset attributes.message = "#attributes.label# Phr_Sys_formValidation_Req">
		</cfif>
	</cfif>
</cfif>

<!--- <cfset attributes.type = attributes.relayFormElementType>  ---><!--- 2014-08-11 AXA added param for attributes.type --->
<cfswitch expression="#attributes.type#">
	<!--- NJH 2008/07/29  CR-LEN507 --->

	<cfcase value="HTML">
		<!--- NOTE: Do not HTMLEditFormat this one! --->
		<cfif isDate(attributes.currentValue)>
			<cfset odbcDateField = CreateODBCDateTime(attributes.currentValue)>
			<cfoutput><div class="col-xs-12">#lsDateFormat(odbcDateField,"medium")# #lsTimeFormat(odbcDateField,"medium")#</div></cfoutput>
		<cfelse>
			<cfoutput><div class="col-xs-12">#attributes.currentValue#</div></cfoutput>
		</cfif>
	</cfcase>
	<cfcase value="text,numeric,radio,checkbox,file">  <!--- WAB 2008/05/21 put all these items into one cfcase --->
		<CF_relayInputField
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="textArea">
		<cf_relayTextField
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="hidden">
		<cf_relayHiddenField
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="autoComplete">
		<cf_relayAutoComplete
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="select">
		<cf_relaySelectField
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="submit">
		<cf_relaySubmitButton
			attributecollection=#attributes#
			>
	</cfcase>
	<cfcase value="button">
		<cf_relayButton
			attributecollection=#attributes#
			>
	</cfcase>

	<cfdefaultcase>
		<CF_relayInputField
			attributecollection=#attributes#
			>
	</cfdefaultcase>
</cfswitch>
<cfsetting enablecfoutputonly="false">
