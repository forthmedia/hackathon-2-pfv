<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayValidatedField.cfm
Author:				SWJ
Date started:		23 March 2004

Description:		This stops the user entering anything other than decimal chars

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2006-06-29		WAB				altered restrict keystrokes js to handle firefox event model
2012-08-16 		PPB 			Case 430059 added required attribute
2013-02-13 		NYB 			Case 433619 added call to restrictOnChange function
2013-09-18 		WAB 			Added NumericList Type
								Addde attributes.popUpMessage to allow suppression of the popup message (have defaulted to false which isn't backwards compatible but we developers think that the pop up message is a pain)
2014-08-11		RPW Create new profile type: "Financial"
2014-09-08		RPW	Financial Flag - Error in selections and downloads
2014-10-14		RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site
2015-10-06 		WAB Jira PROD2015-41 Added support for range attribute
2016-02-04		RJT	BF-408 Corrected styling of financial profiles (e.g. ones that should display with a $ at the beginning)
Possible enhancements:

<CF_relayValidatedField
	fieldName="frmFieldName"
	currentValue="#query.dateField#"
	charType="alpha,numeric,decimal,YorN,alphaNumeric"
	size="5"
	maxLength="20"
	helpText="phr_dateFieldHelpText"
	popUpWarning= false/true
>

 --->
<cfif Not isDefined("attributes.charType")>
	<cfoutput>You must define charType for field #htmleditformat(attributes.fieldName)#</cfoutput>
	<cfexit method="EXITTAG">
</cfif>

<cfswitch expression="#attributes.charType#">
	<cfcase value="alpha"><cfset restrictKeystrokesTo = "keybAlpha"></cfcase>
	<cfcase value="numeric"><cfset restrictKeystrokesTo = "keybNumeric"></cfcase>
	<cfcase value="numericlist"><cfset restrictKeystrokesTo = "keybNumericList"></cfcase>
	<cfcase value="decimal"><cfset restrictKeystrokesTo = "keybDecimal"></cfcase>
	<cfcase value="financial"><cfset restrictKeystrokesTo = "keybDecimal"></cfcase>	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
	<cfcase value="YorN"><cfset restrictKeystrokesTo = "keybYN"></cfcase>
	<cfcase value="alphaNumeric"><cfset restrictKeystrokesTo = "keybAlphaNumeric"></cfcase>
	<cfcase value="variableName"><cfset restrictKeystrokesTo = "keybCFVariable"></cfcase>
	<cfcase value="telephone"><cfset restrictKeystrokesTo = "keybTelephone"></cfcase> <!--- 2014-10-14	RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site --->
	<cfdefaultcase><cfset restrictKeystrokesTo = "keybAlphaNumeric"></cfdefaultcase>
</cfswitch>

	<cfparam name="attributes.popUpWarning" default="false">

	<cfif NOT attributes.popUpWarning>
		<cfset restrictKeystrokesTo = restrictKeystrokesTo & "NM">
	</cfif>

<CF_includeonce template="/templates/relayFormJavaScripts.cfm">

	<cfparam name="attributes.size" default="5">
	<cfparam name="attributes.maxlength" default="20">
	<cfparam name="attributes.required" default="false">	<!--- 2012-08-16 PPB Case 430059 added required and requiredMessage attribute --->
	<cfparam name="attributes.label" default="#attributes.fieldName#">
	<cfparam name="attributes.class" default="">

	<cfif not request.relayFormDisplayStyle neq "HTML-div">
		<cfset attributes.class = attributes.class & " form-control">
	</cfif>

	<cfset attrStruct = structNew()>
	<cfscript>
		if (structKeyExists (attributes,"range")) {
			attrStruct.range = attributes.range;
		}

		// 2014-08-11		RPW Create new profile type: "Financial"
		variables.fieldPrefix = "";
		switch(attributes.charType)
		{
			case "decimal":
				attrStruct.validate="regex";
				attrStruct.pattern = "^[-]?([0-9]+(\.)?[0-9]*|[0-9]*(\.)?[0-9]+)$";
				attrStruct.message = "Must be a valid decimal value";
				break;
			case "financial":
				
				if (IsDefined("attributes.extendedParameters.currencySign")) {
					if (request.relayFormDisplayStyle == "HTML-div") {
						if (!isDefined("attributes.extendedParameters.FlagTypeCurrency") OR attributes.extendedParameters.FlagTypeCurrency !="UD"){
							variables.fieldPrefix = '<span class="input-group-addon">' & attributes.extendedParameters.currencySign & '</span>'; //" This is here to correct a syntax highlighting bug in eclipse'
						}
					} else {
						variables.fieldPrefix = attributes.extendedParameters.currencySign;
						
					}
				}

				

				attrStruct.validate="regex";
				variables.inputWidth = 11;

				//2014-09-08	RPW	Financial Flag - Error in selections and downloads
				if (IsDefined("attributes.extendedParameters.FlagTypeDecimalPlaces") && IsNumeric(attributes.extendedParameters.FlagTypeDecimalPlaces)) {
					variables.decimalPlaces = attributes.extendedParameters.FlagTypeDecimalPlaces;
				} else {
					variables.decimalPlaces = 0;
				}
				attrStruct.message = " Must be a valid monetary value to " & variables.decimalPlaces & " Decimal Places";

				variables.mask = "";
				if (variables.decimalPlaces) {
					attrStruct.pattern = "^[0-9]+(\.\d{1," & variables.decimalPlaces & "})?$";

					if (IsNumeric(attributes.currentValue)) {
						variables.mask &= ".";

						for (variables.p=0;variables.p < variables.decimalPlaces;variables.p++) {
							variables.mask &= "_";
						}

						attributes.currentValue = NumberFormat(attributes.currentValue,variables.mask);
					}

				} else {

					attrStruct.pattern = "^[0-9]+(\.\d{0,1})?$";

					if (IsNumeric(attributes.currentValue)) {
						attributes.currentValue = Round(attributes.currentValue);
					}
				}
				
				if (IsDefined("attributes.extendedParameters.FlagTypeCurrency")) {
					if (attributes.extendedParameters.FlagTypeCurrency=="UD") {
						
						variables.inputWidth = 8;

						if (Len(attributes.extendedParameters.currencyISOCode)) {
							variables.defaultCurrency = attributes.extendedParameters.currencyISOCode;
						} else {
							variables.countryISOCodeList = application.com.relayCountries.getCountryDetails(countryID=attributes.extendedParameters.countryId).CountryCurrency;
							variables.defaultCurrency = ListFirst(variables.countryISOCodeList);
						}

						variables.getCurrencies = application.com.screens.getCurrencies();

						variables.fieldNameIdent = ListLast(attributes.fieldName,"_");

						if (IsNumeric(variables.fieldNameIdent)) {
							variables.fieldNameIdent++;
						} else {
							variables.fieldNameIdent = 1;
						}

						variables.preSelectName = ListDeleteAt(attributes.fieldName,3,"_") & "_" & variables.fieldNameIdent;
						//RJT data-role="none" required to stop jQuery mobile from putting spurious spns in front of the select
						variables.fieldPrefix &= '<select data-role="none" name="' & variables.preSelectName & '" class="form-control col-sm-4">'; //' - here to reolve syntax highlighting bug

						for (variables.c=1;variables.c <= variables.getCurrencies.recordCount;variables.c++) {
							variables.sel = "";
							if (variables.getCurrencies.currencyISOCode[variables.c]==variables.defaultCurrency) {
								variables.sel = " selected";
							}
							variables.fieldPrefix &= '<option value="' & variables.getCurrencies.currencyISOCode[variables.c] & '"' & variables.sel & '>' & variables.getCurrencies.currencySign[variables.c] & ' ' & variables.getCurrencies.currencyName[variables.c] & '</option>';
						}

						variables.fieldPrefix &= "</select>";

						attributes.class = attributes.class & " col-sm-8";

					}
				}
				
				break;
		}
	</cfscript>

	<cfif structKeyExists(attributes,"requiredMessage")>
		<cfset attrStruct.requiredMessage=attributes.requiredMessage>
	</cfif>
	<cfoutput>
	<!--- NYB Case 433619 2013-02-13 added onChange in input: --->
	<!--- 2014-08-11	RPW Create new profile type: "Financial" --->
	<cfif FindNoCase(attributes.charType,"financial") AND request.relayFormDisplayStyle EQ "HTML-div">
		<cfif IsDefined("attributes.extendedParameters.FlagTypeCurrency") and attributes.extendedParameters.FlagTypeCurrency EQ "UD">
			<div class="row form-inline" style="margin-left: 0px">
		<cfelse>
			<div class="input-group">
		</cfif>	
	</cfif>
	
	#variables.fieldPrefix#
	<CF_INPUT data-role="none" type="text" class="#attributes.class#" label="#attributes.label#" name="#attributes.fieldName#" size="#attributes.size#" maxlength="#attributes.maxlength#" onKeyPress="restrictKeyStrokes(event,#restrictKeystrokesTo#)" onChange="restrictOnChange(#restrictKeystrokesTo#,this)" value="#attributes.currentValue#" required="#attributes.required#" attributeCollection="#attrStruct#">		<!--- 2012-08-16 PPB Case 430059 added required and requiredMessage attributes --->
	<cfif FindNoCase(attributes.charType,"financial") AND request.relayFormDisplayStyle EQ "HTML-div"></div></cfif>
	</cfoutput>