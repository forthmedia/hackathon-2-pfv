<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayTimeField.cfm
Author:				SWJ
Date started:		2004-03-08

Description:		This provides two select lists for specifying the time

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2005-02-28			WAB			altered to use numberformat to get leading zeros (rather than a hard coded list)
2005-04-26			WAB 		altered to allow use of attributes.currentdate instead of hour and minute
2005-05-03			WAB 		deal with attributes.currentdate being null
2005-06-03		WAB 		added minutestep parameter (eg minutestep=15 does just 00, 15,30,45)
2006-03-14		WAb			added an onChange attribute  (same one used for hour and minutes)
2007-06-03		WAB			changed default of minutes and made sure that worked if minutestep not 1
2009/05/08		NJH			P-FNL003 added disabled attribute
2009/12/07		WAB			Internationalisation, added attributes.showAsLocalTime, also added attributes.fieldname so don't need hourFieldName and minutesFieldName
2013/02/19		NJH			Added readonly support

USAGE:

<CF_relayTimeField
	currentHour="#left(currentTime,2)#" optional defults to 10
	currentMinute="#mid(currentTime,4,2)#" optional defults to 0
	or use currentValue = aDate
	hourFieldName="hourField"
	minuteFieldName="minuteField"
>

When the data is processed it will probably need to be converted into a date variable e.g.
	<cfset myDate = CreateDateTime(mid(FORM.txtDate,7,4), mid(FORM.txtDate,4,2),
		left(FORM.txtDate,2),
	  	FORM.hourField, FORM.minuteField, 00)>

or use application.com.datefunctions.combineDateAndTimeFormFields ()  requires a date field with a name such as myDate and the hour and minute fields to by myDate_hr and myDate_min


 --->
	<cfparam name="attributes.MinuteStep" default="15">
<cfparam name="attributes.disabled" default="false"> <!--- NJH 2009/05/08 P-FNL003 Content Editor --->
<cfparam name="attributes.readonly" default="false">
<cfparam name="attributes.showAsLocalTime" default="false">   <!--- WAb 2009/12/08 --->
<cfparam name="attributes.class" default="">  <!--- JIRA PROD2016-1436 --->

<cfif isdefined("attributes.fieldname")>
	<cfset attributes.hourFieldName = attributes.fieldname & "_hr">
	<cfset attributes.minuteFieldName = attributes.fieldname & "_min">
</cfif>

<cfif isDefined("attributes.currentvalue")>
		<cfset variables.currentValue = attributes.currentvalue>
	<cfelse>
		<cfset variables.currentValue = now()>
	</cfif>

	<cfif isDate(variables.currentvalue)>
		<cfif attributes.showAsLocalTime>
			<cfset variables.currentValue = application.com.datefunctions.convertServerDateTimeToClientDateTime (currentValue)>
		</cfif>
		<cfset attributes.currentHour = datepart("h",variables.currentValue)>
		<cfset attributes.currentMinute = datepart("n",variables.currentValue)>
	<cfelse>
		<cfset attributes.currentHour = 0>
		<cfset attributes.currentMinute = 0>
	</cfif>

	<cfif not isDefined("attributes.currentvalue") and attributes.MinuteStep is not 1>
		<cfset attributes.currentMinute = int(attributes.currentMinute / attributes.MinuteStep) * attributes.MinuteStep>
</cfif>



	<cfparam name="attributes.onChange" default = "">

	<cfoutput>
		<select id="#attributes.hourFieldName#" name="#attributes.hourFieldName#" <cfif attributes.class neq "">class="#attributes.class#"</cfif> <cfif attributes.onChange is not "">onchange="#htmleditformat(attributes.onChange)#"</cfif> <cfif attributes.disabled>disabled</cfif>>
			<cfif not attributes.readOnly>
				<cfloop index="i" from="0" to="23">
					<cfset temp = numberformat(i,"00")>
					<option value="#temp#" #iif(temp eq attributes.currentHour, de("SELECTED"), de(""))#>#temp#</option>
				</cfloop>
			<cfelse>
				<cfset hourValue = numberformat(attributes.currentHour,"00")>
				<option value="#hourValue#" SELECTED>#hourValue#</option>
			</cfif>
		</select>

		<select id="#attributes.minuteFieldName#" name="#attributes.minuteFieldName#" <cfif attributes.class neq "">class="#attributes.class#"</cfif> <cfif attributes.onChange is not "">onchange="#htmleditformat(attributes.onChange)#"</cfif> <cfif attributes.disabled>disabled</cfif>>
			<cfif not attributes.readOnly>
				<cfset itemSelected=false>
				<cfloop index="i" from="0" to="59" step="#attributes.minuteStep#">
					<cfset temp = numberformat(i,"00")>
					<option value="#temp#" <cfif i eq attributes.currentMinute>SELECTED<cfset itemSelected=true></cfif> >#temp#</option>
				</cfloop>
			<cfelse>
				<cfset itemSelected=true>
				<cfset minValue = numberformat(attributes.currentMinute,"00")>
				<option value="#minValue#" SELECTED>#minValue#</option>
			</cfif>
		<cfif itemSelected is false>
			<option value="#attributes.currentMinute#" SELECTED >#numberformat(attributes.currentMinute,"00")#</option>
		</cfif>
		</select>

		<cfif attributes.showAsLocalTime>
			<CF_INPUT type="hidden" name="#attributes.fieldname#_localTime">
		</cfif>

		<cfset title="phr_serverTime">
		<cfif attributes.showAsLocalTime>
			<cfset title="phr_localTime">
		</cfif>
		<span title="#title#"><img src="/images/icons/time.png"></span>
	</cfoutput>

