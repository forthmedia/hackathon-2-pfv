<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">
<!---
File name:			relayDateField.cfm
Author:				SWJ
Date started:		2004/03/08

Description:		This provides a date field with a date control

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2004/10/08	 		NM			Ability to enable a date range while all other dates are disabled.
2006/03/14			WAB			modified the handling of onChange attribute.  Was on the readonly textbox, so never fired, have now put on the clicks to show the calendar and to clear
2007/06/11			WAB/NH		changes so that can be used with relayFormElements  . Note also changed calendarpopup.js
2008/01/2008		WAB/NH modified so that can have more than one on a page
2008/04/10			WAB removed a line causing errors  - I think that Nat and I may have added it accidentally on previous work
2009/09/01			NJH			changed thisFormname to formName.. done as part of elearning module 8.1
2009-05-07 			NYB 		ATI Bug 2169 - requires release of CalenderPopup.js first
								added params:  attributes.yearSelectStartOffset and attributes.yearSelectEndOffset
2009-05-08			NJH			P-FNL003 Content Editor - added disabled - if disabled, don't show clear link and don't make calendar picker a link.
2009/06/02			WAB			continuing above, made the date field disabled as well - had to convert to an attribute collection, but this is good because the code was repeated 4 time otherwise
2009/12/07			WAB			Internationalisation, added attributes.showAsLocalTime
2011/01/04			PPB 		LID4718 changed readonly = "" to readonly = "true" for date input field
2011/11/13			WAB			LID 7965 Moved get "dateTypeInfo" into dateFunctions
2011-12-06			NYB 		LHID8224 added in translation for title of Calendar popup
2012-03-15			IH			Change to jQuery datepicker
2012-03-15			IH			Add "phr_dateFieldHelpText" as default value for attributes.helpText
2013/02/19			NJH			Added readonly support
2013-02-20			WAB		433389	Required message not needed when using cf_input - rwFormValidation does it automatically from the label
2013-02-20			WAB			Reinstated the Clear link
2013-08-13			YMA			Case 436439 do not htmleditformat the clearlinktext as it could contain html.
2014-10-28			RPW			CORE-666 OPP Edit Screen needs multiple XML Changes
2014-11-18			RPW			CORE-97 Phase 1: Extend database fields for Leads
2015-01-15 			ACPK		CORE-1027 Fix to check showAllYearsStart & showAllYearsEnd against 'null' string instead of empty string
2015-02-18 			ACPK 		FIFTEEN-203 Fixed datepicker not displaying correct year range in year dropdown menu
2015-05-05			NJH			Added ability to evaluate the disableFrom/To date fields that come in if they are not a date, to allow us to disable ranges for date flags (added as an undocumented parameter)
2015-06-05			NJH			Remove JS comments and replace with CF comments. Also set the format for the current value if the input was disabled. The jQuery didn't seem to change the format when disabled
2016-01-28			WAB			BF-255 Performance. Remove Unneccesary translates
Possible enhancements:


USAGE: To create a date field use the tag below

<CF_relayDateField
	currentValue="#query.dateField#"
	fieldName="dateFieldName"
	anchorName="anchorX"
	attributes.yearSelectStartOffset="2"
	attributes.yearSelectEndOffset="2"
>

When the field is processed you need to build a valid date from the data you have e.g.
<cfset myDate = CreateDateTime(mid(dateFieldName,7,4), mid(dateFieldName,4,2), left(dateFieldName,2),  00, 00, 00)>

To hide drop downs surround them with a div and pass the names of the div into the tag in divsToHide

ENABLING AND DISABLING DATES:

default behaviour has all dates enabled.
to DISABLE or ENABLE a range of dates use the attributes.disableFromDate (default="null"), attributes.disableToDate (default="null") and attributes.enableRange params (default="false").
	possible uses:
		if attributes.disableFromDate eq "null" and attributes.disableToDate eq valid date and attributes.enableRange eq "false"/"true"
			all dates UPTO attributes.disableToDate will be DISABLED.
			EG:
				<CF_relayDateField
					currentValue="#query.dateField#"
					fieldName="dateFieldName"
					anchorName="anchorX"
					disableToDate="#dateFormat(now(),'yyyy-mm-dd')#"
				>
		if attributes.disableFromDate eq valid date and attributes.disableToDate eq "null" and attributes.enableRange eq "false"/"true"
			all dates FROM attributes.disableFromDate will be DISABLED.
			EG:
				<CF_relayDateField
					currentValue="#query.dateField#"
					fieldName="dateFieldName"
					anchorName="anchorX"
					disableFromDate="#dateFormat(now(),'yyyy-mm-dd')#"
				>
		if attributes.disableFromDate eq valid date and attributes.disableToDate eq valid date and attributes.enableRange eq "false"
			all dates BETWEEN attributes.disableFromDate and attributes.disableToDate will be DISABLED.
			EG:
				<CF_relayDateField
					currentValue="#query.dateField#"
					fieldName="dateFieldName"
					anchorName="anchorX"
					disableFromDate="2004-01-01"
					disableToDate="#dateFormat(now(),'yyyy-mm-dd')#"
				>
		if attributes.disableFromDate eq valid date and attributes.disableToDate eq valid date and attributes.enableRange eq "true"
			all dates BETWEEN attributes.disableFromDate and attributes.disableToDate will be ENABLED and all other dates will be DISABLED.
			EG:
				<CF_relayDateField
					currentValue="#query.dateField#"
					fieldName="dateFieldName"
					anchorName="anchorX"
					disableFromDate="2004-01-01"
					disableToDate="#dateFormat(now(),'yyyy-mm-dd')#"
					enableRange="true"
				>

 --->
<!--- CASE 431147: PJP: 19/10/2012:  Added label attribute to select, enabled required field popup --->
<cfparam name="attributes.label" default="#attributes.fieldName#">
<cfparam name="attributes.onchange" default="">
<cfparam name="attributes.onBlur" default="">
<cfparam name="attributes.divName" default="">
<cfparam name="attributes.divToHide" default="">
<cfparam name="attributes.currentValue" default="">

<!--- NB: despite their names these fields actually function as "enableFromDate" and "enableToDate"  --->
<cfparam name="attributes.enableRange" default="false">
<cfparam name="attributes.disableFromDate" default="null">
<cfparam name="attributes.disableToDate" default="null">

<cfparam name="attributes.showClearLink" default="false">
<cfparam name="attributes.ClearLinkText" default="Clear">
<cfparam name="attributes.required" default="false">
<cfparam name="attributes.requiredMessage" default=""><!--- 2014-11-18	RPW	CORE-97 Added requiredMessage --->
<cfparam name="attributes.message" default="#attributes.label# phr_required">

<!--- START: NYB 2009-05-07 ATI Bug 2169 - added params: --->
<cfparam name="attributes.yearSelectStartOffset" default="2">
<cfparam name="attributes.yearSelectEndOffset" default="2">
<!--- END: NYB 2009-05-07 ATI Bug 2169 - added params: --->
<cfparam name="attributes.id" default="#attributes.fieldName#">	<!--- 2011/06/27 PPB REL106 pass the id defined on cf_relayFormElement tag through --->
<cfparam name="attributes.readonly" default="false">
<cfparam name="attributes.class" default="">

<!--- RMB -  P-RIL001 - Fix to allow year drop to show full range for dates like DOB - START --->
<cfscript>
showAllYears = false;
if(StructKeyexists(attributes, "enableRange") AND attributes.enableRange AND
		StructKeyexists(attributes, "disableFromDate") AND attributes.disableFromDate NEQ '' AND
		StructKeyexists(attributes, "disableToDate") AND attributes.disableToDate NEQ ''){

	showAllYears=true;
	/* 2015-02-18 ACPK FIFTEEN-203 Fixed datepicker not displaying correct year range in year dropdown menu by switching disableToDate and disableFromDate attributes */
	showAllYearsStart = MID(attributes.disableToDate, 1, 4);
	showAllYearsEnd = MID(attributes.disableFromDate, 1, 4);

}
</cfscript>
<!--- RMB -  P-RIL001 - Fix to allow year drop to show full range for dates like DOB - END --->

<cfparam name="attributes.helpText" default="phr_dateFieldHelpText">
<!---
	WAB 28/2/05  set this datemask, intention is to be able to use a different one (ie USA) if necessary
	WAB 18/06/2007 made into an attribute
--->
<cfparam name="attributes.dateType" default="#request.relaycurrentuser.localeInfo.dateType#">
<cfset dateTypeInfo = application.com.dateFunctions.getDateTypeFormattingInfo(attributes.dateType)>

<cfparam name="attributes.showAsLocalTime" default="false">   <!--- WAb 2009/12/08 --->
<cfif attributes.showAsLocalTime and isdate (attributes.currentValue)>
	<cfset attributes.currentValue = application.com.datefunctions.convertServerDateTimeToClientDateTime (attributes.currentValue)>
</cfif>

<!--- NJH 2015/05/05 - try to evaulate the disableFromDate if it's not a date, as it could be passed in as a string containing 'now()' --->
<cfif attributes.disableFromDate neq "null" and not isDate(attributes.disableFromDate)>
	<cftry>
		<cfset attributes.disableFromDate = evaluate(attributes.disableFromDate)>
		<cfcatch>
		</cfcatch>
	</cftry>
	<cfif not isDate(attributes.disableFromDate)>
		<cfoutput>The date in disableFromDate is not a recognised date.</cfoutput>
		<cfexit method="EXITTAG">
	</cfif>
</cfif>

<cfif attributes.disableToDate neq "null" and not isDate(attributes.disableToDate)>
	<cftry>
		<cfset attributes.disableToDate = evaluate(attributes.disableToDate)>
		<cfcatch>
		</cfcatch>
	</cftry>
	<cfif not isDate(attributes.disableToDate)>
		<cfoutput>The date in disableToDate is not a recognised date.</cfoutput>
		<cfexit method="EXITTAG">
	</cfif>
</cfif>

<!--- <cf_IncludeJavascriptOnce template = "/javascript/lib/jquery/datepicker/jquery-ui-1.8.18.custom.min.js"> --->
<cf_htmlhead text='<link rel="stylesheet" href="/styles/datepicker/redmond/jquery-ui-1.8.18.custom.css" type="text/css" media="all" />'>

<!--- include locale specific language file --->
<cfif request.relaycurrentuser.localeinfo.locale neq "en_US">
	<cfset basePath="/javascript/lib/jquery/datepicker/">
	<cfif !structKeyExists(application,"stDatepicker")>
		<cflock name="datepicker" timeout="30">
			<!--- double lock to prevent resetting application.stDatepicker --->
			<cfif !structKeyExists(application,"stDatepicker")>
				<cfset stDatepicker={}>
				<cfdirectory action="list" directory="#expandPath('..#basePath#')#" name="qList" filter="jquery.ui.datepicker-*">
				<cfloop query="qList">
					<cfset locale=replacelist(name,"jquery.ui.datepicker-,.js",",")>
					<cfset stDatepicker[locale]=name>
				</cfloop>
				<cfset application.stDatepicker = stDatepicker>
			</cfif>
		</cflock>
	</cfif>

	<cfset locale=lCase(listFirst(request.relaycurrentuser.localeinfo.locale,"_")) & "-" & uCase(listLast(request.relaycurrentuser.localeinfo.locale,"_"))>
	<cfif structKeyExists(application.stDatepicker,locale)>
		<cf_IncludeJavascriptOnce template = "#basePath##application.stDatepicker[locale]#">
	<cfelseif structKeyExists(application.stDatepicker,request.relaycurrentuser.languageIsoCode)>
		<cf_IncludeJavascriptOnce template = "#basePath##application.stDatepicker[request.relaycurrentuser.languageIsoCode]#">
	</cfif>
</cfif>

<!--- 2011/06/27 PPB REL106 pass the id defined on cf_relayFormElement tag through --->
<!--- CASE 431147: PJP: 19/10/2012:  Added label attribute to select, enabled required field popup --->
<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
<!--- 2014-11-18	RPW	CORE-97 Added requiredMessage --->
<!--- WAB 2016-04-25 Allow additional attributes, such as data-, to be passed through to input box.  Implement a loop and regExp (which includes all current attributes which were being passed through unchanged)   --->
<cfset attributeCollection = {type="text", name=attributes.fieldName, value=dateformat(attributes.currentValue,dateTypeInfo.dateMask), size="10", maxlength="12", readonly="readonly", class="datepicker inlineDisplay form-control " & attributes.class, TITLE=attributes.helpText, datemask=dateTypeInfo.datemask}>		<!--- PPB LID4718 changed readonly = "" to readonly = "true" --->
<cfloop collection ="#attributes#" item="key">
	<cfif refindNocase("\A(data-.*|label|id|required|on.*|message|requiredMessage)\Z", key)>
		<cfset attributeCollection [key] = attributes[key]>
	</cfif>
</cfloop>


<cfset disableCalendar = false>
<cfif (structKeyExists(attributes,"disabled") and attributes.disabled) or attributes.readOnly>
	<cfset disableCalendar = true>
	<cfset attributeCollection.disabled = true>
</cfif>

<cfif structKeyExists(attributes,"requiredFunction") and attributes.requiredFunction neq "">
	<cfset attributeCollection.requiredFunction = attributes.requiredFunction>
</cfif>
<cfif structKeyExists(attributes,"submitFunction") and attributes.submitFunction neq "">
	<cfset attributeCollection.submitFunction = attributes.submitFunction>
</cfif>

<!--- NJH Case 432100  - added change by month/year --->
<cfsavecontent variable="datepickerJS">
<cfoutput>
<script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function($) {
	$( "###attributes.id#" ).datepicker({
		<cfif disableCalendar>disabled: true,</cfif>
		<cfif attributes.enableRange>
		<cfif isDate(attributes.disableFromDate)>maxDate: '#dateFormat(attributes.disableFromDate,dateTypeInfo.dateMask)#',</cfif>
		<cfif isDate(attributes.disableToDate)>minDate: '#dateFormat(attributes.disableToDate,dateTypeInfo.dateMask)#',</cfif>
		</cfif>
<!--- RMB -  P-RIL001 - Fix to allow year drop to show full range for dates like DOB - START
* 2015-01-15 - ACPK CORE-1027 - Fix to check showAllYearsStart & showAllYearsEnd against 'null' string instead of empty string --->
		<cfif showAllYears AND showAllYearsStart NEQ 'null' AND showAllYearsEnd NEQ 'null'>
		yearRange:"#showAllYearsStart#:#showAllYearsEnd#",
		<!--- 2014/11/24	YMA	CORE-989 increase year range when no specific start or end dates are set. --->
		<CFELSE>
		yearRange:"c-100:c+100",
		</cfif>
<!--- RMB -  P-RIL001 - Fix to allow year drop to show full range for dates like DOB - END --->
		showOn: 'both',
		buttonImageOnly: true,
		buttonImage: "/styles/datepicker/calendar.jpg",
		showAnim: 'fadeIn',
		duration: 100,
		dateFormat: '#replaceList(dateTypeInfo.dateMask,"yyyy,MM","yy,mm")#',
		buttonText: '#attributes.helpText#',
		changeMonth:true,
		changeYear:true,
		gotoCurrent: true

	}).keydown(function(event) {return cancelBackspace(event)});

	function cancelBackspace(event) {
		if (event.keyCode == 8) {
			return false;
	    }
	}
});
//]]>
</script>
</cfoutput>
</cfsavecontent>

<cfhtmlhead text="#datepickerJS#">


<!--- 		WAB / NH 11/06/2007  for relayFormElement
	if within a cfform tag then we need to use cfinputs so that "required" stuff works
--->
<cfoutput>

<!--- NJH format the current value as jquery doesn't do it when disabled --->
<cfif disableCalendar>
	<cfset attributes.currentValue = dateFormat(attributes.currentValue,dateTypeInfo.dateMask)>
</cfif>

<cfif listfindnocase(getBasetagList(),"cfform")>
	<cfinput attributeCollection=#attributeCollection#>
<cfelse>
	<cfset structDelete (attributeCollection,"message")>
	<cf_input attributeCollection=#attributeCollection#>
</cfif>
<cf_input type="hidden" name="#attributes.fieldName#_#dateTypeInfo.dateHiddenFieldSuffix#" >

	<!--- 2013-08-13	YMA	Case 436439 do not htmleditformat the clearlinktext as it could contain html. --->
	<cfif attributes.showClearLink and not disableCalendar and not attributes.required>
		<A HREF="##" onClick="jQuery('###attributes.id#').datepicker('setDate',null) ;<cfif attributes.onChange is not "">#htmleditformat(attributes.onChange)# ;</cfif> return false;" NAME="#htmleditformat(attributes.id)#null" ID="#htmleditformat(attributes.id)#null" TITLE="#htmleditformat(attributes.ClearLinkText)#null">
		#attributes.ClearLinkText#</A>
	</cfif>


</cfoutput>

<cfsetting enablecfoutputonly="no">
