<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Generic Tag for outputting undocumented parameters to a 2 column Layout

Just pass in a structure of all the parameters to be output

Optionally have an add New Parameter Link

2012-09-21 PPB Case 430402 add translate="true" to restrictKeyStrokes.js call
2012-04-24 WAB	Case 432049  Problems occur if parameter has name Required.  Field ends up called   frmAdditionalParameter_required - which ColdFusion interprets as a required field
				Change code so that frmAdditionalParameter is used as a suffix
2016-11-29	WAB During Form Renderer/Screen Builder Project Alter code to use jQuery rather than home spun table manipulation code
--->


<cfparam name="attributes.parameterStructure" type="struct">
<cfparam name="attributes.LabelText" default = "Advanced Parameters">    <!--- blank out if no label required--->
<cfparam name="attributes.AddNewParameterLinkText" default = "Add an advanced parameter"> <!--- blank out if adding parameters not allowed --->
<cfparam name="attributes.tableID" type="string">
<cfparam name="attributes.parameterSuffix" type="string" default="frmAdditionalParameter">
<cfparam name="attributes.hideParameter" type="string" default="">


	<!--- Loop through parameters outputing them a row at a time--->
	<cfif structCount (attributes.parameterStructure) gt 0>
			<cfif attributes.labelText is not "">
				<cfoutput>
					<tr id="UndocumentedParametersLabelRow">
							<td colspan=2>#htmleditformat(attributes.labelText)#</td>
							<td ></td>
					</tr>
				</cfoutput>
			</cfif>

				<!--- NJH 2014/08/11 - if we're using the partnerPack parameter, then don't expose it to the user. Currently used in elements to help define which elements belong to which partner pack --->
				<cfloop collection = #attributes.parameterStructure# item = "parameter">
					<cfif not listFindNoCase(attributes.hideParameter,parameter)>
						<cfoutput>
							<tr>
								<td><label>#htmleditformat(parameter)#</label></td>

								<td><CF_INPUT class="form-control" type="text" name="#parameter#_#attributes.parameterSuffix#" value="#attributes.parameterStructure[parameter]#"></td>
							</tr>
						</cfoutput>
					<cfelse>
						<CF_INPUT type="hidden" name="#parameter#_#attributes.parameterSuffix#" value="#attributes.parameterStructure[parameter]#">
					</cfif>
				</cfloop>
	</cfif>


	<cfif attributes.AddNewParameterLinkText is not "">

			<cf_includejavascriptonce template = "/javascript/restrictKeyStrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->
			<cfoutput>
				<tr id="addNewParameterLinkRow">
					<td colspan="2">
						<a href="javascript:addNewParameter()">#htmleditformat(attributes.AddNewParameterLinkText)#</a>
					</td>
				</tr>
			</cfoutput>

			<!--- generate the javascript for adding extra rows--->
			<cfsavecontent variable = "rowHTML">
				<cfoutput>
					<TR id="UndocumentedParameterRow_INDEX">
						<TD align="right">
							<INPUT type="text" name="INDEX_#attributes.parameterSuffix#_name"  onKeyPress="restrictKeyStrokes(event,keybCFVariableNM)" onchange="if (this.value.length > 1 & !isNaN(this.value.substring(0,1)) ) {alert ('Parameter name must not start with a number')}" PlaceHolder="Parameter Name">
						</TD>
						<td>
							<INPUT type="text" name="INDEX_#attributes.parameterSuffix#" class="form-control" PlaceHolder="Parameter Value">
						</td>
					</TR>
				</cfoutput>
			</cfsavecontent>

				<script>
				var addNewParameterIndex = 0;
				var rowHTML = <cfoutput>#serializeJSON(rowHTML)#</cfoutput>
				function addNewParameter () {
					var addBeforeObj = jQuery ('#addNewParameterLinkRow')	;
					addNewParameterIndex ++;
					addBeforeObj.before(rowHTML.replace (/INDEX/g,addNewParameterIndex));
				}
			</script>

	</cfif>


