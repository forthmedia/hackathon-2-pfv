<!--- �Relayware. All Rights Reserved 2014 --->
<!---

relayDateDropDowns.cfm

Author WAB 2004-10-20

shows a date as a series of drop downs

Plenty still to do
 - javascript when required
 - allowable date ranges

NJH 2007/02/05   added show individual items
WAB	2007/02/11	added Javascript verification
WAB  2010/10/20   rejigged code around request.dropdownStartYear and request.DateDropDownYearsAhead because they were overriding progammatical setting of attributes
				actually needs converting to settings which become the defaults of cfparam
WAB 2012/11		Don't output required attribute unless required = true
WAB 2015-06-22 FIFTEEN-384 Add time support to date flags - add localTime Support
 --->


<CFPARAM name ="attributes.currentValue">
<CFPARAM name ="attributes.fieldName">
<!--- NJH 2007/02/05  --->
<cfparam name="attributes.showDay" type="boolean" default="true">
<cfparam name="attributes.showMonth" type="boolean" default="true">
<cfparam name="attributes.showYear" type="boolean" default="true">
<cfparam name="attributes.class" type="string" default="form-control dateSelect">

<cfparam name="attributes.startYear" type="numeric" default="0">

<cfparam name="attributes.showAsLocalTime" default="false">   <!--- WAB 2015/06/22 --->
<cfif attributes.showAsLocalTime and isdate (attributes.currentValue)>
	<cfset attributes.currentValue = application.com.datefunctions.convertServerDateTimeToClientDateTime (attributes.currentValue)>
</cfif>



<!--- 2007/03/13 GCC - Dirty method for setting system wide date pickers for clients who don't want 90 years past and 10 years future
I couldn't face working out how to pass it down the chain from the screen but that is what is needed in V2!
A passed in NumYears or StartYear will override these values
WAB 2011/10/18 moved this bit of code and added condition so that request.DateDropDownStartYear only takes effect if attributes.startYear not passed in
WAB 2011/12/14 Turned out that the above had caused another error (on Kerio) (numYears not defined).  I have now reordered the IFs and the CFPARAMs to hopefully get it right
 --->

<cfif attributes.startYear eq 0 and structkeyexists(request,"DateDropDownStartYear")>
	<cfset attributes.startYear = request.DateDropDownStartYear>
</cfif>

<cfif not structKeyExists (attributes,"numYears")  and structkeyexists(request,"DateDropDownYearsAhead")>
	<cfset attributes.numYears = datepart('yyyy',now()) + request.DateDropDownYearsAhead - attributes.startYear>
</cfif>

<cfparam name="attributes.numYears" type="numeric" default="100">

<cfif attributes.startYear eq 0>
	<cfset attributes.startYear = datepart('yyyy',now())-attributes.numYears+10>
</cfif>





<cfparam name="attributes.required"  default="0">
<cfparam name="attributes.requiredMessage"  default="Enter A Date">


<cfif isDate(attributes.currentValue)>
	<cfset currentDay = datepart("d",attributes.currentValue)>
	<cfset currentMOnth = datepart("m",attributes.currentValue)>
 	<cfset currentYear = datepart("yyyy",attributes.currentValue)>
<cfelse>
	<cfset currentDay = "">
	<cfset currentMOnth = "">
 	<cfset currentYear = "">
 </cfif>

<cfoutput>
	<cfif attributes.showDay>
		<select class="#attributes.class#" id ="#attributes.fieldName#Day" name="#attributes.fieldName#Day" size="1" <cfif attributes.required>required=#attributes.required#</cfif>>
				<option value="" >phr_sys_Day</option>
			<cfloop index="i" from="1" to="31" step="1">
				<option value="#i#" <cfif i is currentDay>SELECTED</cfif>>#htmleditformat(i)#</option>
			</cfloop>
		</select>
	</cfif>
	<cfif attributes.showMonth>
		<select class="#attributes.class#" id="#attributes.fieldName#Month" name="#attributes.fieldName#Month" size="1" <cfif attributes.required>required=#attributes.required#</cfif>>
		<cfset j = 1>
				<option value="" >phr_sys_Month</option>
			<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
				<option value="#j#" <cfif j is currentMonth>SELECTED</cfif>>phr_sys_shortMonth_#htmleditformat(i)#</option>
				<cfset j = j + 1>
			</cfloop>
		</select>
	</cfif>
	<cfif attributes.showYear>
		<select class="#attributes.class#" id ="#attributes.fieldName#Year" name="#attributes.fieldName#Year" size="1" <cfif attributes.required>required=#attributes.required#</cfif>>
				<option value="" >phr_sys_Year</option>
			<cfloop index="i" from="#attributes.startYear#" to="#attributes.startYear+attributes.numYears#" step="1">
				<option value="#i#" <cfif i is currentYear>SELECTED</cfif>>#htmleditformat(i)#</option>
			</cfloop>
		</select>
	</cfif>
</cfoutput>


<!--- WAB 2007-02-12 deal with javascript for required
	currently only coded for use in screens
--->
<cfif attributes.Required>
	<cfoutput>
	<script>  <!--- this script should be included inly once per request --->
		function isRelayDateDropDownSelected (fieldName) {

				dayObj = document.getElementById (fieldName + 'Day')
				monthObj = document.getElementById (fieldName + 'Month')
				yearObj = document.getElementById (fieldName + 'Year')

				<!--- because we don''t necessarily have every select box on any given page,we check for its existence and whether it is null --->
				if (dayObj && dayObj.options[dayObj.selectedIndex].value == '') {
					return false
				} else 	if (monthObj && monthObj.options[monthObj.selectedIndex].value == '') {
					return false
				} else if (yearObj && yearObj.options[yearObj.selectedIndex].value == '') {
					return false
				} else {
					return true
				}

		}

	</script>
	</cfoutput>

	 <cfif isDefined("caller.javascriptVerification")>  <!--- must be being called in a screen --->
		 <cfset caller.javascriptVerification = caller.javascriptVerification & " msg +=  (!isRelayDateDropDownSelected('#attributes.fieldname#'))?'#replace(attributes.requiredMessage,"'","\'","ALL")#\n':'';">
	</cfif>
</cfif>



