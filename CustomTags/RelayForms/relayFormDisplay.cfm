<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayFormDisplay.cfm
Author:				SWJ
Date started:		2006-03-27

Description:		This is a standard wrapper tag that controls the opening and closing layout
					of a relayForm. The model supports various display options for displaying
					form controls.  These are switched via request.relayFormDisplayStyle.
					Styles include:

					no-label: this simply returns with no layout changes and would be used
							when the field layout is controlled manually.
					HTML-table: this is used when the form layout is inside a table with
							two columns column 1 containing the label and 2 the field.

					It should be used in conjunction with a cfform tag.

					If no layout controls are needed <CF_relayFormObject> can be called directly
					to return the form elements without layout and labels OR called as normal with a
					relayFormDisplayStyle of no-label.

					For complete documentation on relayforms, please see
					\\Fnl-usa2\F N L\Relay Documentation\RelayWare Core Technology\RelayFormArchitecture.doc

Amendment History:

Date (YYYY-MM-DD)	Initials 	Version		What was changed

11-May-2006			GCC			1.1			Restructuring work to further separate form elements form layout
7-June-2006			NJH			1.2			Added translation for validation error messages
2009/04/17			NJH						Sophos Stargate Added id for table
2010/09/28			WAB 					change caller.relayForm to request.relayform to deal with situations when caller is not present (eg when using functions)
											Try to plumb in rwFormValidation
2011/12/05			WAB						Change styling of rwFormValidation	Validation Errors
2012-01-26 			NYB 					Case:426292 added translate="true" to rwFormValidation.js include
2013-03				WAB/NJH					Changes to formValidation code
12/11/2015			SB						Bootstrap
2016-01-29			WAB						Added extra random number to initialisation function, fails if two forms on page
2016-01-29			WAB						Make inline validation messages the default, and remove attribute for defining the name of the validation class

Possible enhancements:

	1. work out a way to have cfform in here without throwing a compile error.
	2. validation - required, masks etc

 --->


<cfif not thistag.HasEndTag>
	<cfthrow message="CF_relayFormDisplay requires an end tag to be placed after the </cfform> tag.">
</cfif>

<cfparam name="attributes.relayFormDisplayStyle" default="#request.relayFormDisplayStyle#">

<!--- Start of form layout --->
<cfif ThisTag.ExecutionMode IS 'start'>
	<cfset request.relayForm = structNew()>  <!--- for storing list of fields displayed --->
	<cfset request.relayForm.fieldList = structNew()>
	<cfset request.relayForm.fieldList.All = "">
	<cfset request.relayForm.useRelayValidation = false>
	<cfset request.relayForm.useCFAjax = false >
	<cfparam name = "attributes.class" default="#request.relayCurrentUser.isInternal?'':'form-horizontal'#">
	<cfparam name = "attributes.id" default=""> <!--- NJH 2009/04/17 Sophos Stargate - added id for table --->
	<cfparam name = "attributes.name" default="#attributes.id#">

	<cfparam name = "attributes.showValidationErrorsInline" default="true" type="boolean">
	<cfparam name = "attributes.applyValidationErrorClass" default="#attributes.showValidationErrorsInline#" type="boolean">
	<cfparam name = "attributes.bindValidationWithListener" default="true" >
	<cfparam name = "attributes.autoValidate" default="#attributes.showValidationErrorsInline#" >
	<cfparam name = "attributes.autoRefreshRequiredClass" default="#IIF(structKeyExists(request,'refreshFormRequiredClass'),DE('true'),DE('false'))#" > <!--- can probably just be set to true --->

	<cfswitch expression="#attributes.relayFormDisplayStyle#">
		<cfcase value="HTML-table"><!--- start the HTML-table layout --->
			<cfoutput>
				<table class="table table-hover" <cfif attributes.id neq ""> id="#htmleditformat(attributes.id)#"</cfif><cfif attributes.name neq ""> name="#htmleditformat(attributes.name)#"</cfif>>
			</cfoutput>
		</cfcase>
		<cfcase value="HTML-div"><!--- start the HTML-div layout --->
			<cfoutput>
				<div id="relayFormDisplay" <cfif attributes.class neq "">class="#attributes.class#"</cfif> <cfif attributes.id neq ""> id="#htmleditformat(attributes.id)#"</cfif><cfif attributes.name neq ""> title="#htmleditformat(attributes.name)#"</cfif>>
			</cfoutput>
		</cfcase>
		<cfcase value="HTML-Form"><!--- no additional layout info ---></cfcase>
		<cfdefaultcase><!--- no additional layout info ---></cfdefaultcase>
	</cfswitch>

</cfif>

<!--- End of form layout --->
<cfif ThisTag.ExecutionMode IS 'end'>
	<cfswitch expression="#attributes.relayFormDisplayStyle#">
		<cfcase value="HTML-table"><!--- end the HTML-table layout --->
			<cfoutput>
				</table>
			</cfoutput>
		</cfcase>
		<cfcase value="HTML-div"><!--- start the HTML-div layout --->
				</div>
		</cfcase>
		<cfcase value="HTML-Form"><!--- no additional layout info ---></cfcase>
		<cfdefaultcase><!--- no additional layout info ---></cfdefaultcase>
	</cfswitch>

	<!---  Are there any items displayed using relayValidation--->
	<cfif request.relayForm.useRelayValidation >
		<cf_includeJavascriptOnce template= "/javascript/rwFormValidation.js" translate="true"><!--- 2012-01-26 NYB Case:426292 added translate="true" --->
		<cf_includeJavascriptOnce template= "/javascript/verifyObjectV2.js">
		<cf_includeJavascriptOnce template= "/javascript/checkObject.js">

		<cfif request.relayForm.useCFAjax >
			<cf_includeJavascriptOnce template= "/javascript/cf/ajax/messages/cfmessage.js">
			<cf_includeJavascriptOnce template= "/javascript/cf/ajax/package/cfajax.js">
		</cfif>

		<cfif listfindnocase(getbasetaglist(),"CFFORM")>
			<!--- we are in a CFFORM but some of our fields are not using cfform
				so we put in a hidden field to get the relayValidation to run after the CFFORM validation has run

				NJH 2016/01/14 - changed name of hidden field as it was conflicting with similiar input in screens
			--->
			<cfif not thisTag.GeneratedContent contains "dummyVerifyInput">
				<cfinput type="hidden" name="dummyRelayValidationOnCFForm" value="" onValidate="doRelayValidationOnCFForm">
			</cfif>
		<cfelse>
			<cf_includeJavascriptOnce template= "/javascript/cf/cfform.js">
			<!--- we are not in CFFORM, so add an onSubmit function --->
			<!--- need to know which form we are on, I have put an id on this bit of script so we can then traverse up and find the form, may be a better way  --->

			<cfset randomNumber = int(rand() * 10000)>
			<cfoutput>
			<span id="#randomNumber#" style="display:none;"></span>
			<script>
				setFormOnSubmit_#randomNumber# = function () {
					var thisForm = $('#jsStringFormat(randomNumber)#').up('form');

					initialiseRelayFormValidation (
						thisForm,
							{
								applyValidationErrorClass:#jsStringFormat(attributes.applyValidationErrorClass)#,
								showValidationErrorsInline:#jsStringFormat(attributes.showValidationErrorsInline)#,
								autoRefreshRequiredClass:#jsStringFormat(attributes.autoRefreshRequiredClass)#,
								bindValidationWithListener:#jsStringFormat(attributes.bindValidationWithListener)#,
								autoValidate:#jsStringFormat(attributes.autoValidate)#
							}
					)
				}

				<cfif structKeyExists(url,"returnFormat")>
					window.setTimeout(setFormOnSubmit_#randomNumber#,1000);
				<cfelse>
					Event.observe(window, 'load', function() {setFormOnSubmit_#randomNumber#();})
				</cfif>
			</script>
			</cfoutput>
		</cfif>
	</cfif>

	<cfset caller.relayForm = request.relayForm>
</cfif>