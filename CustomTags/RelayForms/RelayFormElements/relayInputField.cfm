<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayInputField.cfm
Author:				SWJ
Date started:		2006-03-22

Description:		This provides a standard input field which can display in various ways based on request.relayFormStyle

Examples:
<CF_relayInputField	currentValue="#query.dateField#" fieldName="frmFieldName" label="phr_labelText">
<CF_relayInputField	currentValue="#qryOrgname#" fieldName="insOrgName" label="Phr_Sys_OrgName (Phr_Sys_OrganisationID :<CFOUTPUT>#frmOrganisationID#</CFOUTPUT>)">
Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
07-JUN-2006		NJH			Added validation and error message building
2007-02-27		NJH 		Translated the message before it went off to the javascript.
2007-10-02 		NJH 		Added mask field  - particularly used for phone numbers

2008-05-21		WAB			Changed to pass an attributescollection into cfinput
							Automatically deal with blacklists on fields
2008-09-01 		NJH			Changed to pass through the attributes collection - done as part of elearning 8.1
2008-09-24		WAB			CR-TND561 Req8 mods for emailUnique
2008-11-13		NJH			T-10 Bug Fix Issue 942 - Added a style to the table around the radio buttons.
2009-03-13		NJH			CR-SNY675 - moved some code from here to relayFormElement.cfm - which includes setting the message, setting label to fieldname if not set and translating the message and label.
2009-04-30 		WAB 		added support for multiple columns of radio buttons
2009-07-08 		NJH 		P-FNL069 - added support for regexp validation
2010-03-08		NJH			P-PAN002 - provide support for readonly
2010-06-07		WAB			p_FNL088 Added useCFForm Attribute so code can be used without CFFORM.
							Not implemented for all controls yet
							Needs rwFormValidation.js to deal with required fields
2010-11-09		WAB			Added htmleditformat encoding to calls to convertStructureToNameValuePairs()
2011-10-13		WAB			LID 7965 (in passing) added support for time fields alongside date field
2012-01-17		NYB  		Case425166 added js if attributeCollection.onValidate is "validateEMail
2012-09-12 		PPB 		Case 430249 don't allow entry of commas in a numeric field (unless the users locale expects them as a decimal seperator)
2012-09-21 		PPB 		Case 430402 add translate="true" to restrictKeyStrokes.js
2012-09-22 		PPB 		Case 430249 use the locale decimal separator to display the number
2012-09-24 		PPB/WAB		Case 430249 add a _LSNumeric field for numerics
2012-09-25		PPB			LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED
2013/02/26		NJH			Added support for uploading files of a certain type.
2014-10-14		RPW			CORE-777 Textboxes  validation for Phone number and mobile in portal site
2015-03-09		AHL			Case 443967 My Company Profile - Edit Location Details - Error
2015-12-17		SB			Bootstrap and added an ID on the table
2016-04-20		WAB			Noticed <cfoutput>s missing around divs of class form-group and radio-inline
2016-06-13		WAB			During PROD2016-347. Required Validation not working for radios (actually radios were getting the required attribute)
2016-11-16		WAB 		Form Renderer Add support for attributes.decimalPlaces
							Add hidden field (#attributes.name#_checkBox) for checkBoxes so that we can detect when they have been unchecked
2017-02-22		WAB			PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases
Possible enhancements:

 --->

<cfparam name="attributes.size" type="numeric" default="30">
<cfparam name="attributes.required" default="no">
<cfparam name="attributes.mask" type="string" default="">
<cfparam name="attributes.pattern" type="string" default=""> <!--- NJH 2009-07-08 P-FNL069 --->
<cfparam name="attributes.readonly" default="no"> <!--- NJH 2010-03-08 P-PAN002 --->
<cfparam name="attributes.usecfform" type="boolean" default="#listfindnocase(getbasetaglist(),"CFFORM")#"> <!--- WAB 2010-06-07 trying something out for Settings Project--->
<cfparam name="attributes.accept" type="string" default="#application.com.settings.getSetting('files.allowedfileextensions')#">
<cfparam name="attributes.acceptType" type="string" default=""> <!--- can be things like  images/videos, etc... --->
<cfparam name="attributes.listAlternateImages" type="string" default="">
<cfparam name="attributes.displayHeight" type="numeric" default="80">
<cfparam name="attributes.allowfiledelete" type="boolean" default="true">
<cfparam name="attributes.class" default="form-control">

<cfif attributes.type is "image">
	<cfparam name="attributes.imageSrc" type="string" default="/images/buttons/side82_log_e.gif">
</cfif>

<cfif attributes.type is "date">
	<cfparam name="attributes.datemask" type="string" default="dd/MM/yyyy">
	<cfparam name="attributes.dateHiddenFieldSuffix" type="string" default="eurodate">
	<cfparam name="attributes.disableFromDate" default="null">
	<cfparam name="attributes.disableToDate" default="null">
	<cfparam name="attributes.enableRange" default="false">
</cfif>

<cfif not request.relayFormDisplayStyle neq "HTML-div" and not listfind("file",attributes.type)>
	<cfset attributes.class= attributes.class & " form-control">
</cfif>



<!---
WAB build up an attribute collection to pass into cfinput,
rather than having lots of similar but not quite identical calls
also gets round the onvalidate problem
could have built up collection with each attribute, but decided to start with all the attributes passed in, modify any that needed modifying and then delete any unwanted ones
 --->
	<cfscript>
		attributeCollection = structCopy(attributes);
		structDelete(attributeCollection,"type");
		structDelete(attributeCollection,"accept");
		structDelete(attributeCollection,"acceptType");
		attributeCollection.name=attributes.fieldName ;
		attributeCollection.id=attributes.id ;
		structDelete(attributeCollection,"fieldName");
		if (attributes.type is "checkbox") {
			//PPB 2010-12-20 REL099 a checkbox should have either "checked" or no attribute set checked="false" still checks the box (at least in FF)
			structDelete(attributeCollection,"checked");
			if ((attributes.currentValue is "true") or (structKeyExists(attributes,"checked") and attributes.checked is "true")) {
				attributeCollection.checked="true";
			}
			attributeCollection.class="checkbox";
			//attributeCollection.checked =  iif(attributes.currentValue is attributes.value,true,false);
		} //else {

		// for a checkbox, we pass in a value as well as a currentValue.. so, only set value if value not passed through
		if (not structKeyExists(attributeCollection,"value")) {
			attributeCollection.value=attributes.currentvalue;
		}
		if (attributes.disabled is not "" and attributes.disabled) {
			// can't have required on a disabled select box
			attributeCollection.required = false;
		}
		if (attributes.disabled is "" or not attributes.disabled) {
			structDelete(attributeCollection,"disabled");
		}
		// NJH 2010-03-10 P-PAN002
		if (not attributes.readonly) {
			structDelete(attributeCollection,"readonly");
		}

		// to work around onValidate being "", as this is treated by cfinput as a function which returns false
		if (attributes.onValidate is "") {
			structDelete(attributeCollection,"onvalidate");
		}
	 	if (attributes.type is "image") {
			attributeCollection.src = attributeCollection.imagesrc ;
		}
		if (attributes.requiredFunction eq "") {
			structDelete(attributeCollection,"requiredFunction");
		}
	</cfscript>

	<!---  delete spurious attributes --->
	<!---
	2013/03/18 NJH Now handled in convertTagAttributeStructToValidAttributeList
	<cfset	unwantedAttributes = "imagesrc,currentvalue,valuealign,labelAlign,notetextimage,spancols,labelsuffix,nowrap,notetext,list,notetextposition,usecfform,nullvalue,notr,trstyle">

	<cfloop index="key" list = #unwantedAttributes#>
		<cfset structDelete(attributeCollection,key)>
	</cfloop> --->

	<!--- WAB 2008-09-24 CR-TND561 Req8 mods here for validating email unique --->

	<cfset FieldInfoStruct = application.com.applicationVariables.getFieldInfoStructureByAlias(attributes.fieldName) >
	<cfif FieldInfoStruct.isOK>

		<cfif FieldInfoStruct.hasBlackList>
			<cf_includejavascriptonce template = "/javascript/verifyemail.js">  <!--- WAB 2008-09-24  --->
			<cfset thequery = FieldInfoStruct.BlackList.query>
			<cfset attributeCollection.blackList = valueList(thequery.data,"|")>
			<cfset attributeCollection.blackListMessage = "Phr_Sys_formValidation_Blacklist_Email">
		</cfif>

		<cfset structappend(attributeCollection,FieldInfoStruct.cfFormParameters,false)>
	</cfif>

	<!--- WAB 2016-03-04 BF-522 removed  code for Case425166.  Required'ness is now handled elsewhere --->
	<cfif structKeyExists (attributeCollection,"onValidate") and attributeCollection.onValidate is "validateEMail">
		<cf_includejavascriptonce template = "/javascript/verifyemail.js"> 
	</cfif>


	<cfswitch expression="#attributes.type#">
		<cfcase value="text,file,password,image,checkbox,radio">
   			<cfswitch expression="#attributes.validate#">
   				<cfcase value="numeric">
   					<cfset attributeCollection.type="number">
   				</cfcase>
   				<cfcase value="email">
   					<cfset attributeCollection.type="email">
   				</cfcase>
				<!--- 2014-10-14	RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site --->
   				<cfcase value="telephone">
					<cf_includeJavascriptOnce template="/javascript/restrictKeyStrokes.js" translate="true">
					<cfset attributeCollection.onKeyPress="restrictKeyStrokes(event,keybTelephoneNM)">
					<cfset attributeCollection.validate = "">
   				</cfcase>
   				<cfdefaultcase>
					<cfset attributeCollection.type=attributes.type>
   				</cfdefaultcase>
   			</cfswitch>
		</cfcase>
		<cfcase value="numeric">
			<cfif not listfindnocase("numeric,float,integer", #attributeCollection.validate#)>		<!--- PPB 2008-10-07 added integer (for NABU 185) --->
				<cfset attributeCollection.validate = "numeric">
			</cfif>
			<!--- 2012-09-12 PPB Case 430249 don't allow entry of commas (unless the users locale expects them as a decimal seperator --->
			<cf_includeJavascriptOnce template="/javascript/restrictKeyStrokes.js" translate="true">	<!--- 2012-09-21 PPB Case 430402 add translate="true" --->
			<cfset attributeCollection.onKeyPress="restrictKeyStrokes(event,keybDecimalNM)">	<!--- use keybDecimalNM so that a decimal seperator is allowed but don't report a decimal message (NM) when an invalid key is hit --->
			<!--- <cf_input type="hidden" name="#attributeCollection.name#_LSnumeric"> ---> <!--- LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED --->
			<cfif structKeyExists (attributeCollection,"decimalPlaces")>
				<cfset numbermask = "0" & ((attributeCollection.decimalPlaces gt 0)?"."&repeatString ("9",attributeCollection.decimalPlaces):"")>
				<cfset attributeCollection.value = numberFormat(attributeCollection.value,numbermask)>
			</cfif>
		</cfcase>
	</cfswitch>

	<cfswitch expression="#attributes.type#">
		<cfcase value="text,file,password,image,checkbox,numeric">
			<!--- <cfif attributes.type eq "numeric">
				<!--- 2012-09-22 PPB Case 430249 regardless of locale decimal numbers are stored on the database with a full-stop for the decimal separator, so now convert back to the locale separator; note: I don't use LSNumberFormat because it requires a mask for decimals so replace is easier than working out number of decimals (if any) --->
				<!--- <cfset attributeCollection.value = replace(attributeCollection.value,".",request.relaycurrentuser.LocaleInfo.decimalInfo.decimalPoint)>  ---> <!--- LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED --->
			</cfif> --->

			<cfif attributes.type eq "file">
				<cf_includeJavascriptOnce template="/javascript/fileDownload.js">
				<cfif attributes.accept neq "">
					<cfif not structKeyExists(attributeCollection,"submitFunction") or attributeCollection.submitFunction eq "">
						<cfset attributecollection.submitFunction = "return verifyFileExtension(this.value,'#attributes.accept#');">
					</cfif>
					<cfset attributecollection.accept = attributes.accept>

					<cfif attributes.accept eq "pdf" or attributes.acceptType eq "pdf">
						<cfset attributecollection.accept = "application/pdf">
					</cfif>
				</cfif>
				<cfif attributes.acceptType eq "image">
					<cfset attributecollection.submitFunction = "return verifyImageFile(this.value);">
					<cfset attributecollection.accept = "image/*">
				</cfif>
			</cfif>
			<cfif attributes.usecfform> <!--- WAB 2010-06-08 Idea for Settings Project--->
				<cfinput attributeCollection = #attributecollection#>
			<cfelse>
			
				<cf_input attributecollection = "#attributecollection#" includeNulls = "false">
				<cfif attributeCollection.required>
					<CF_INPUT type="hidden" name="#attributeCollection.name#_required" value="#attributeCollection.message#">
				</cfif>
			</cfif>

			<!--- WAB 2016-11-16 In order to detect checkboxes which have been unchecked we add this hidden field --->
			<cfif attributes.type is "checkbox">
				<cf_input type="hidden" name="#attributeCollection.name#_checkBox" value="0">
			</cfif>


			<!--- NJH roadmap 2013 - trying to get a standard output when we have an image field. this displays an image and provides a delete button.  --->
			<!--- YMA 2013-06-21	Add ability to show a default image and the ability to pick your image from a list of alternative images.  Originally built to be used by Genericemails/emails.cfm --->
			<cfif attributes.type eq "file">

				<cfif attributes.acceptType eq "image">
					<cfset attributes.listAlternateImages = application.com.structureFunctions.convertNameValuePairStringToStructure(attributes.listAlternateImages,',','=')>
					<cfif structCount(attributes.listAlternateImages) gt 0>
						<cfoutput>
							<script>
								setDefaultImage = function(x,i) {
									tdObject = jQuery(x).parents("table").parents("td");
									tdObject.find("img##Image_#jsstringformat(attributeCollection.name)#").attr("src", x.src);
									tdObject.find("input###jsstringformat(attributeCollection.name)#").attr("value","");
									tdObject.find("input###jsstringformat(attributeCollection.name)#_default").attr("value",i);
								}
							</script>
							<table><tr>
								<td>phr_orChooseADefaultImage</td>
								<td>
									<cfloop collection="#attributes.listAlternateImages#" item="i">
										<img onClick="javascript:setDefaultImage(this,'#attributes.listAlternateImages[i]#');" id="#i#" src="<cfif len(attributes.listAlternateImages[i]) eq 0>/images/misc/blank.gif<cfelse>#attributes.listAlternateImages[i]#</cfif>" height="#attributes.displayHeight/2#" />
									</cfloop>
								</td>
							</tr></table>
						</cfoutput>
					</cfif>

					<cfoutput>
						<cfif fileExists(expandPath(attributes.currentValue))>
							<br /><img id="Image_#attributeCollection.name#" height="#attributes.displayHeight#" src="#attributes.currentValue#?refresh=#rand()*100000#" /> <cfif (not attributes.required) and attributes.allowfiledelete><span><cf_input type="checkbox" id="#attributes.fieldname#_delete">&nbsp;<label for="#attributes.fieldname#_delete">phr_sys_delete</label></span></cfif>
							<cfif structCount(attributes.listAlternateImages) gt 0>
								<CF_INPUT type="hidden" name="#attributeCollection.name#_default" value="">
							</cfif>
						<cfelseif structKeyExists(attributes,"defaultImage") and len(attributes.defaultImage) gt 0 and fileExists(expandPath(attributes.defaultImage))>
							<br /><img id="Image_#attributeCollection.name#" height="#attributes.displayHeight#" src="#attributes.defaultImage#" />
							<CF_INPUT type="hidden" name="#attributeCollection.name#_default" value="#attributes.defaultImage#">
						<cfelseif structCount(attributes.listAlternateImages) gt 0>
							<br /><img id="Image_#attributeCollection.name#" height="#attributes.displayHeight#" src="/images/misc/blank.gif" />
							<CF_INPUT type="hidden" name="#attributeCollection.name#_default" value="">
						</cfif>
					</cfoutput>

				<!--- if the file is not an image (which will be very rare! - only in products when uploading brochure to keep backwards compatibility) ... show the filename and display a delete checkbox--->
				<cfelseif attributes.currentValue neq "" and fileExists(expandPath(attributes.currentValue))>
					<span class"filename">
						<cfoutput>
						<a href="#htmlEditFormat(attributeCollection.value)#" target="blank">
							<cfset fileIcon = application.com.filemanager.getFileDisplayIcon(filename=listLast(attributes.currentValue,"/"))>
							<cfif fileIcon.iconExists>
							#application.com.security.sanitiseHTML(fileIcon.imageTag)#
							<cfelse>
							#htmlEditFormat(listLast(attributes.currentValue,"/"))#
							</cfif>
						</a>
						<cfif (not attributes.required) and attributes.allowfiledelete><span><cf_input type="checkbox" id="#attributes.fieldname#_delete">&nbsp;<label for="#attributes.fieldname#_delete">phr_sys_delete</label></span></cfif>
						</cfoutput>
					</span>
				</cfif>
			</cfif>
		</cfcase>

		<cfcase value="textArea">
			<cfif attributes.usecfform> <!--- WAB 2010-06-08 Idea for Settings Project--->
				<cfinput attributeCollection = #attributecollection#>
			<cfelse>
				<cfoutput><textArea #application.com.relayForms.convertTagAttributeStructToValidAttributeList(attributeStruct = attributecollection,tagName="textArea")# >#attributeCollection.value#</textarea></cfoutput>
			</cfif>
		</cfcase>

		<cfcase value="radio">
			<cfparam name="attributes.valueColumn" default="value">
			<cfparam name="attributes.columns" default="1"> <!---  WAb 2009-04-30 Default to a single column of radios--->
			<cfif isDefined("attributes.list") >
				<!--- create a query out of the list --->
				<cfset theQuery = queryNew("#attributes.valueColumn#,display")>

				<cfloop index="item" list = "#attributes.list#">
					<cfset value = trim(listfirst(item,"|#application.delim1#"))>  <!--- #application.delim1# (�) for backwards compatability with valid values--->
					<cfset display = listlast(item,"|#application.delim1#")>
					<cfset queryaddrow(theQuery,1)>
					<cfset querysetcell(theQuery,"#attributes.valueColumn#",value)>
					<cfset querysetcell(theQuery,"display",display)>
				</cfloop>
				<cfset attributes.query = theQuery>
				<cfset attributes.display = "display">
			</cfif>

			<!--- Doesn't make sense for every radio to be required, so remove the required attribute.  Requiredness is then dealt with separately later --->
			<cfset structDelete (attributeCollection,"required")>

			<cfset radioChecked="false">
					<!--- 2007-04-16 WAB added a table around this code, otherwise impossible to line things up
						just very basic - no formatting and only single column layout, but could be extended

					 --->
					 <!--- NJH 2008-11-13 T-10 Bug Fix Issue 942 - added style to the table to get the help icon appearing to the right of the table
					 	rather than below it.
					  --->
					<cfif request.relayFormDisplayStyle neq "html-div">
						<cfoutput><Table id="relayInputFieldTable"></cfoutput>
                    <cfelse>
                        <cfoutput><div class="form-group validation-group"></cfoutput>
					</cfif>
					<cfloop query="attributes.query">
						<cfset attributeCollection.value =attributes.query[attributes.valueColumn][currentrow]>
							<cfif attributeCollection.value  eq attributes.currentValue>
								<cfset attributeCollection.checked = true>
								<cfset radioChecked="true">
							<cfelse>
								<cfset attributeCollection.checked = false>
							</cfif>

                        <cfif structKeyExists(attributecollection,"class")>
                            <cfset attributecollection.class = replaceNoCase(attributecollection.class,"form-control","radio")>
                        </cfif>

						<!--- WAB 2009-04-30 add support for multiple columns --->
						<cfif request.relayFormDisplayStyle neq "html-div">
							<CFIF currentRow MOD attributes.columns IS 1>
								<cfoutput><TR></cfoutput>
							</CFIF>

								<cfoutput>
                                <td valign="top">
                                    <cfif attributes.usecfform>
                                        <cfinput attributeCollection = #attributecollection#>
                                    <cfelse>
                                        <cf_input attributecollection = #attributecollection#>
                                    </cfif>
                                </td>
                                <td>#application.com.security.sanitiseHTML(display)#</td>
                                </cfoutput>

                            <CFIF currentRow MOD attributes.columns IS 0 or currentRow is attributes.query.recordcount>
                                <cfoutput></TR></cfoutput>
                            </CFIF>
						<cfelse>
							<cfoutput><div class="radio-inline"></cfoutput>
								<cfif attributes.usecfform>
									<cfinput attributeCollection = #attributecollection#>
								<cfelse>
									<cf_input attributecollection = #attributecollection#>
								</cfif>
                                <cfoutput><label>#application.com.security.sanitiseHTML(display)#</label></cfoutput>
                            <cfoutput></div></cfoutput>
                        </cfif>

<!---
 							<cfif variables.value eq attributes.currentValue>
								<cfset radioChecked="true">
								<cfinput type="radio" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#variables.value#" checked="Yes" disabled="#attributes.disabled#" onClick="#attributes.onClick#" required="#attributes.required#" message="#attributes.message#">
							<cfelse>
								<cfinput type="radio" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#variables.value#" checked="No" disabled="#attributes.disabled#" onClick="#attributes.onClick#" required="#attributes.required#" message="#attributes.message#"  >
							</cfif>
 --->

					</cfloop>

					<!--- no value set and a default passed draw another box. --->
				<cfif not radioChecked and structKeyExists(attributes,"nullText") and attributes.nullText neq "">
					<cfif request.relayFormDisplayStyle neq "html-div">
						<cfoutput><tr><td><cfinput type="radio" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#evaluate(attributes.nullValue)#" checked="Yes" disabled="#attributes.disabled#"></td><td>#htmleditformat(attributes.nullText)#</td></tr></cfoutput>
					<cfelse>
						<cfoutput>
                            <div class="radio-inline">
                                <cfinput type="radio" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#evaluate(attributes.nullValue)#" checked="Yes" disabled="#attributes.disabled#">
                                <label>#htmleditformat(attributes.nullText)#</label>
                            </div>
                        </cfoutput>
					</cfif>
				</cfif>

					<!--- Required.  Note that attributeCollection.required is removed (because otherwise every radio becomes required), but that attributes.required is still set --->
					<cfif attributes.required>
						<cfoutput><CF_INPUT type="hidden" name = "#attributecollection.Name#_required" submitFunction="return verifyObjectv2(this.form['#attributecollection.Name#'],'phr_Required',1)" ></cfoutput>	<!--- 2015-03-09 AHL Case 443967 My Company Profile - Edit Location Details - Error --->
					</cfif>

					<cfif request.relayFormDisplayStyle neq "html-div">
						<cfoutput></TABLE></cfoutput>
                    <cfelse>
                        <cfoutput></div></cfoutput> <!--- close the form-group div --->
					</cfif>

		</cfcase>

		<cfcase value="date">
			<cfif attributes.disableFromDate neq "null">
				<cfset attributes.disableFromDate = dateFormat(attributes.disableFromDate,"yyyy-mm-dd")>
			</cfif>
			<cfif attributes.disableToDate neq "null">
				<cfset attributes.disableToDate = dateFormat(attributes.disableToDate,"yyyy-mm-dd")>
			</cfif>
			<cfif attributes.onValidate eq "">
				<!--- NJH 2008-09-01 changed to pass through the attributes collection - done as part of elearning 8.1 --->
				<cf_relayDateField  fieldName="#attributes.fieldName#" anchorName="anchor_#attributes.fieldName#" attributecollection=#attributes#>
				<cfif structKeyExists(attributes,"showTime") and attributes.showtime>
						<cf_relaytimefield
							hourfieldName="#attributes.fieldName#_Hr"
							minutefieldName="#attributes.fieldName#_Min"
							anchorname = "anchor_#attributes.fieldName#_T"
							attributecollection=#attributes#>

				</cfif>

				<!---
				<cf_relayDateField  fieldName="#attributes.fieldName#" anchorName="anchor_#attributes.fieldName#" currentValue="#attributes.currentValue#" required="#attributes.required#" message="#attributes.message#" datemask="#attributes.dateMask#" dateHiddenFieldSuffix="#attributes.dateHiddenFieldSuffix#" disableFromDate="#attributes.disableFromDate#" disableToDate="#attributes.disableToDate#" enableRange="#attributes.enableRange#"> --->
			</cfif>
		</cfcase>

<!--- 		<cfcase value="text">
			<!--- to work around onValidate being "", as this is treated by cfinput as a function which returns false --->
			<cfif attributes.onValidate eq "">
				<cfinput type="text" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" size="#attributes.size#" maxlength="#attributes.maxLength#" disabled="#attributes.disabled#" validateAt="#attributes.validateAt#" pattern="#attributes.pattern#" validate="#attributes.validate#" message="#attributes.message#" required="#attributes.required#" mask="#attributes.mask#" blackList = #blackList#>
			<cfelse>
				<cfinput type="text" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" size="#attributes.size#" maxlength="#attributes.maxLength#" disabled="#attributes.disabled#" validateAt="#attributes.validateAt#" pattern="#attributes.pattern#" onValidate="#attributes.onValidate#" message="#attributes.message#" required="#attributes.required#" mask="#attributes.mask#" blackList = #blackList#  >
			</cfif>
		</cfcase>
 --->
<!--- 		<cfcase value="numeric">
			<cfif listfindnocase("numeric,float", #attributes.validate#)>  <!--- NJH 2007-10-03 was <cfif not listfindnocase.... --->
				<cfinput type="Text" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" size="#attributes.size#" maxlength="#attributes.maxLength#" disabled="#attributes.disabled#" validateAt="#attributes.validateAt#" validate="#attributes.validate#" pattern="#attributes.pattern#" message="#attributes.message#" required="#attributes.required#">
			<cfelse>
				<cfinput type="Text" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" size="#attributes.size#" maxlength="#attributes.maxLength#" disabled="#attributes.disabled#" validateAt="#attributes.validateAt#" validate="numeric" message="#attributes.message#" required="#attributes.required#">
			</cfif>
		</cfcase>
 --->

		<!--- <cfcase value="checkbox">
			<cfif attributes.Value eq attributes.currentValue>
				<cfinput type="checkbox" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.Value#" checked="Yes" disabled="#attributes.disabled#" required="#attributes.required#" message="#attributes.message#"  onClick="#attributes.onClick#" onChange="#attributes.onChange#">
			<cfelse>
				<cfinput type="checkbox" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.Value#" checked="No" disabled="#attributes.disabled#" required="#attributes.required#" message="#attributes.message#"  onClick="#attributes.onClick#" onChange="#attributes.onChange#">
			</cfif>
		</cfcase>
		<cfcase value="file">
			<cfif attributes.onValidate eq "">
				<cfinput type="file" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" message="#attributes.message#" onClick="#attributes.onClick#" onChange="#attributes.onChange#">
			<cfelse>
				<cfinput type="file" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" onValidate="#attributes.onValidate#" message="#attributes.message#" onClick="#attributes.onClick#" onChange="#attributes.onChange#">
			</cfif>
		</cfcase>
		<cfcase value="password">
			<cfif attributes.onValidate eq "">
				<cfinput type="password" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" message="#attributes.message#">
			<cfelse>
				<cfinput type="password" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" message="#attributes.message#" onValidate="#attributes.onValidate#">
			</cfif>
		</cfcase>
		<cfcase value="image">
			<cfif attributes.onValidate eq "">
				<cfinput type="image" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" message="#attributes.message#" src="#attributes.imageSrc#">
			<cfelse>
				<cfinput type="image" name="#attributes.fieldName#" id="#attributes.fieldName#" size="#attributes.size#" required="#attributes.required#" message="#attributes.message#" src="#attributes.imageSrc#" onValidate="#attributes.onValidate#">
			</cfif>
		</cfcase> --->
	</cfswitch>


