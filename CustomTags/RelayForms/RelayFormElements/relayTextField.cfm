<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayTextField.cfm
Author:				SWJ
Date started:		2004-03-08

Dmscription:		This provides a text field

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2006-03-22	SWJ		As part of the introduction of relayFormStyle we included that here.
2009/03/13	NJH		CR-SNY moved some generic code from here to relayFormElement.cfm
2010/03/08	NJH		P-PAN002 - provide support for readonly and disabled
2013-10-01 	NYB 	Case 437100 added ID to attributeStruct
2015-01-08 	PPB 	P-KAS039 pass tabindex through
2017-02-22	WAB		PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases

Possible enhancements:

<CF_relayTextField
	maxChars="1000"
	currentValue="#query.dateField#"
	cols="50"
	rows="5"
	fieldName="frmFieldName"
	helpText="phr_dateFieldHelpText"
>

 --->

<CF_includeonce template="/templates/relayFormJavaScripts.cfm">

	<cfparam name="attributes.maxChars" default="1000">
	<cfparam name="attributes.cols" default="50">
	<cfparam name="attributes.rows" default="5">
	<cfparam name="attributes.backgroundColour" default="transparent">
	<cfparam name="attributes.multiple" default="1">
	<cfparam name="attributes.required" default="no">
	<cfparam name="attributes.message" default="">
	<cfparam name="attributes.disabled" default="false">
	<cfparam name="attributes.readonly" default="false">
	<cfparam name="attributes.usecfform" type="boolean" default="#listfindnocase(getbasetaglist(),"CFFORM")#"> <!--- WAB 2010/06/07 trying something out for Settings Project--->
	<cfparam name="attributes.maxCharsPosition" type="string" default="right">
	<cfparam name="attributes.id" default="#attributes.fieldName#"> <!--- 2013-10-01 NYB Case 437100 added --->
	<cfparam name="attributes.class" default="form-control">
	<cfparam name="attributes.tabindex" default=""> 	<!--- 2015-01-08 PPB P-KAS039 --->

	<!--- NJH 2010/09/27 P-PAN004 - set maxChars to maxLength if maxLength defined.. this means that we don't have a special
		attribute for text field. --->
	<cfif structKeyExists(attributes,"maxLength")>
		<cfset attributes.maxChars = attributes.maxLength>
	</cfif>
	<cfif not request.relayFormDisplayStyle neq "HTML-div">
		<cfset attributes.class= attributes.class & " form-control">
	</cfif>

		<cfoutput>

			<!--- P-PAN002 NJH 2010/03/08 - pass added readonly and disabled and pass in via structure --->
			<cfset attributeStruct = structNew()>
			<cfif attributes.readOnly>
				<cfset attributeStruct.readonly=attributes.readOnly>
			</cfif>
			<cfif attributes.disabled>
				<cfset attributeStruct.disabled=attributes.disabled>
			</cfif>
			<cfset attributeStruct.onclick="doleft(#attributes.maxChars#,this,this.id+'charsLeft')">
			<cfset attributeStruct.onkeyup="doleft(#attributes.maxChars#,this,this.id+'charsLeft')">
			<cfset attributeStruct.cols=attributes.cols>
			<cfset attributeStruct.rows=attributes.rows>
			<cfset attributeStruct.name=attributes.fieldName>
			<cfset attributeStruct.id=attributes.id><!--- 2013-10-01 NYB Case 437100 changed to = id (not fieldname) --->
			<cfset attributeStruct.value = attributes.currentValue>
			<cfif structKeyExists(attributes,"class")>
				<cfset attributeStruct.class = attributes.class>
			</cfif>
			<cfif structKeyExists(attributes,"onFocus")>
				<cfset attributeStruct.onFocus = attributes.onFocus>
			</cfif>
			<cfif structKeyExists(attributes,"onBlur")>
				<cfset attributeStruct.onBlur = attributes.onBlur>
			</cfif>
			<cfif structKeyExists(attributes,"onChange")>
				<cfset attributeStruct.onChange = attributes.onChange>
			</cfif>
			<cfif attributes.required>
				<cfset attributeStruct.required = attributes.required>
			</cfif>
			<cfif structKeyExists(attributes,"requiredFunction")>
				<cfset attributeStruct.requiredFunction = attributes.requiredFunction>
			</cfif>
			<cfif structKeyExists(attributes,"submitFunction")>
				<cfset attributeStruct.submitFunction = attributes.submitFunction>
			</cfif>
			<cfset attributeStruct.message = attributes.message>
			<cfset attributeStruct.tabindex = attributes.tabindex>			<!--- 2015-01-08 PPB P-KAS039 --->

			<cfif attributes.usecfform>
				<cftextarea type="Text" attributeCollection=#attributeStruct#></cftextarea>
			<cfelse>
				<cfoutput><textArea #application.com.relayForms.convertTagAttributeStructToValidAttributeList(attributeStruct = attributeStruct, tagName="textArea")#>#attributeStruct.value#</textArea></cfoutput>
			</cfif>

			<!--- NJH 2013/02/22 - don't show amount remaining on a readonly. Set the width in limitTextArea.js, as when there is no text, there is a big blank space. In
				the case where the textarea is a tranlsation widget, it looks funny to have the 'T' out in the center of the page. Also remove the name, as it causes problems
				when trying to process a translation --->

			<cfif attributes.maxChars GT 0 and not attributes.readOnly>
				<cfif attributes.maxCharsPosition eq "bottom"><br></cfif>
				<p class="charsLeft" id="#attributeStruct.id#charsLeft"></p>
			</cfif>
		</cfoutput>
