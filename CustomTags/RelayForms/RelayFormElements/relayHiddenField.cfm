<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayInputField.cfm	
Author:				SWJ
Date started:		2006-03-22
	
Description:		This provides a standard input field which can display in various ways based on request.relayFormStyle

Examples:
<CF_relayInputField	currentValue="#query.dateField#" fieldName="frmFieldName" label="phr_labelText">
<CF_relayInputField	currentValue="#qryOrgname#" fieldName="insOrgName" label="Phr_Sys_OrgName (Phr_Sys_OrganisationID :<CFOUTPUT>#frmOrganisationID#</CFOUTPUT>)">	
Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2017-02-22			WAB			PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases

Possible enhancements:

 --->

<CF_INPUT type="Hidden" value="#attributes.currentValue#" attributeCollection="#attributes#" includeKeysRegexp="\A(id|name|value|submitfunction|fieldname|currentValue|type|label)\Z">
