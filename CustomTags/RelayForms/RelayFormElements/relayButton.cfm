<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayButton.cfm
Author:				NJH
Date started:		June 2006

Description:

Examples:
Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2010/03/26			NJH			P-PAN002 - added support for disabled.
2012/10/08			IH			Case 431126 fix attributes.id
2017-02-22			WAB			PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases
Possible enhancements:

 --->
<!--- first include the mandatory RelayForm field attributes.  These will alert a developer if they are not completed --->

<cfparam name="attributes.currentValue" default="phr_Update">
<cfparam name="attributes.onClick" default="">
<cfparam name="attributes.usecfform" type="boolean" default="#listfindnocase(getbasetaglist(),"CFFORM")#"> <!--- WAB 2010-06-07 trying something out for Settings Project--->
<cfparam name="attributes.class" default="">
<!-- 
<cfif not request.relayFormDisplayStyle neq "HTML-div">
	<cfset attributes.class= attributes.class & "btn btn-primary">
</cfif> -->

<cfset attributes.class= attributes.class & " btn btn-primary">
<cfset attributeStruct = structNew()>
<cfset attributeStruct.type="button">
<cfset attributeStruct.name=attributes.fieldName>
<cfset attributeStruct.id=attributes.id>
<cfset attributeStruct.value=attributes.currentValue>
<cfset attributeStruct.onClick=attributes.onClick>
<cfset attributeStruct.class=attributes.class>
<cfif structKeyExists(attributes,"disabled") and attributes.disabled>
	<cfset attributeStruct.disabled=true>
</cfif>

<!--- NJH 2015/08/12 JIRA PROD2015-19 - pass through data attributes --->
<cfset dataKeys = application.com.structureFunctions.getStructureKeysByRegExp(struct=attributes,regexp="\Adata-")>
<cfloop list="#dataKeys#" index="dataKey">
	<cfset attributeStruct[dataKey] = attributes[dataKey]>
</cfloop>

<cfif attributes.useCfForm>
	<cfinput attributeCollection=#attributeStruct#>
<cfelse>
	<cfoutput><input #application.com.structureFunctions.convertStructureToNameValuePairs(struct = attributeStruct, delimiter=" ",qualifier='"',includeNulls = false,encode="HTMLAttribute")# ></cfoutput>
</cfif>

