<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relaySubmitButton.cfm
Author:				WAB
Date started:		May 2006

Description:

Examples:
Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2009-04-03	NYB	Sophos - fixed tab order of main Add Contact Form
2014-08-18	RMC - fixed issue with "disabled" attribute not working by amending 'disabled=""' to 'disabled="disabled"'
2016/01/07	NJH		Remove tabindex default.
2016/03/11	RJT		BF-557 Remove stray form-control which should never be present on buttons
2017-02-22	WAB		PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases
Possible enhancements:

 --->

<cfparam name="attributes.currentValue" default="phr_Update">
<cfparam name="attributes.disabled" default="false">
<cfparam name="attributes.usecfform" type="boolean" default="#listfindnocase(getbasetaglist(),"CFFORM")#">
<!--- NYB 2009-04-03 - Sophos Stargate - added to give correct tabOrder to Add Contact Form --->
<cfparam name="attributes.tabIndex" default="">
<cfparam name="attributes.class" default="">

<!--- RJT/WAB/SB BF-557 There should never be an form-control classes on submit buttons but as this is called by relayFormElement they can end up here. This removes them--->
<cfset attributes.class =  reReplaceNoCase(attributes.class,"(\s|\A)form-control(\s|\Z)","\1\2") >



<!-- <cfif not request.relayFormDisplayStyle neq "HTML-div">
	<cfset attributes.class= attributes.class & " btn btn-primary">
</cfif> -->
<cfset attributes.class= attributes.class & " btn btn-primary">

<CFSET attributeStruct = {type="submit",name=attributes.fieldName,id=attributes.id,value = attributes.currentvalue,onClick = attributes.onClick, class = attributes.class,tabIndex=attributes.tabIndex}>
<cfif attributes.disabled>
	<CFSET attributeStruct.disabled="disabled"><!--- RMC 18/Aug/2014: Added "disabled" attribute --->
</cfif>

<cfif attributes.useCFFORM>
	<cfinput attributeCollection = #attributeStruct#>
<cfelse>
	<cfoutput><input #application.com.structureFunctions.convertStructureToNameValuePairs(struct = attributeStruct, delimiter=" ",qualifier='"',includeNulls=false)#></cfoutput>
</cfif>


<!---
	<cfif attributes.disabled>
		<!--- NYB 2009-04-03 - Sophos Stargate - added tabIndex="#attributes.tabIndex#" --->
		<cfinput type="Submit" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" onClick="#attributes.onClick#" class="#attributes.class#" tabIndex="#attributes.tabIndex#" disabled>
	<cfelse>
		<!--- NYB 2009-04-03 - Sophos Stargate - added tabIndex="#attributes.tabIndex#" --->
		<cfinput type="Submit" name="#attributes.fieldName#" id="#attributes.fieldName#" value="#attributes.currentValue#" onClick="#attributes.onClick#" class="#attributes.class#" tabIndex="#attributes.tabIndex#">
	</cfif>
 --->
