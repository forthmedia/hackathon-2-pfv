<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayAutoComplete.cfm
Author:				YMA
Date started:		2014-02-26

Description:		An input field that will auto complete from web service call to query values to assist with text input.
					You must pass in a function in the format of application.com.opportunity.getOppPartners(countryID=9)
					You must specifiy the function parameters by name
					The function you are calling should respect the argument 'term' and use this to filter its results.
Examples:
					<cf_relayFormElement function="com.opportunity.getOppPartners(countryID = #application.com.relayPLO.getPersonDetails(personID = arguments.personID).countryID)#" display="siteName" value="locationid" required="true" relayFormElementtype="autoComplete" fieldname="Organisation#randomNumber#" label="Organisation" currentvalue="">
					<cf_relayAutoComplete function="com.opportunity.getOppPartners(countryID = #application.com.relayPLO.getPersonDetails(personID = #arguments.personID).countryID)#" display="siteName" value="locationid" required="true" fieldname="Organisation#randomNumber#" label="Organisation" currentvalue="">

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed

Possible enhancements:
					It would be good to enhance this tag to work off a valid value list or select query.
					Although no performance enhancements will be gained by doing so it would be good for look and feel improvements.

 --->

<cfparam name="attributes.function" default="">
<cfparam name="attributes.display" default="displayValue">
<cfparam name="attributes.value" default="dataValue">
<cfparam name="attributes.required" default="yes">
<cfparam name="attributes.multiple" default="No">
<cfparam name="attributes.disabled" default="No">
<cfparam name="attributes.style" default="">
<cfparam name="attributes.tabIndex" default="1">
<cfparam name="attributes.readonly" default=0>
<cfparam name="attributes.fieldname" default="">
<cfparam name="attributes.label" default="">

<cf_includeJavascriptOnce template = "/javascript/protoTypeSpinner.js">
<cf_includeJavascriptOnce template = "/javascript/openWin.js">
<cf_includeCssOnce template="/javascript/lib/jquery/css/themes/smoothness/jquery-ui.css" />
<cf_head>
	<cfoutput>
	<script>
		jQuery(function() {
			jQuery('##select_#attributes.fieldname#').autocomplete({
				// If we want to modify this to work from a valid value list we can pass that into a jason struct and set that as the source rather than a webService call.  See jQuery autoComplete documentation
				source: function(request,response,url){
						var searchParam  = request.term;
						$('#attributes.fieldname#spinner').spinner({position:'right'});
						jQuery.ajax({
							url:'/webservices/callWebService.cfc?method=callWebService&webServiceName=relayScreens&methodName=searchBindFunction',
							data: {
								term:searchParam,
								function:'#JSStringFormat(attributes.function)#',
								display:'#JSStringFormat(attributes.display)#',
								value:'#JSStringFormat(attributes.value)#',
								returnFormat:'json'},
							dataType: "json",
							type:"GET",
							success: function(data){
								if(data.length == 0){
									data[0] = jQuery.parseJSON('{"label":"phr_noResults","value":""}');
								}
								$('#attributes.fieldname#spinner').removeSpinner();
								jQuery('##select_#attributes.fieldname#').click()
								response(jQuery.map(data, function(item){
								return {
									label:item.label,
									value:item.value
									};
								}))
							}
						});
					},
					minLength: 1, // set to 1 so that we can search for personID as well
					select: function( event, ui ) {
						setSelectedEntityID#attributes.fieldname#(ui.item.label,ui.item.value);
						return false;
					}
				}).focus(function(){
       			jQuery(this).data("autocomplete").search(jQuery(this).val());
    		});
		});

		function setSelectedEntityID#attributes.fieldname# (label,value) {
			if(value != ''){
				jQuery('###attributes.fieldname#').val(value);

				// Fire on change event so any bind functions watching this field trigger run
				var el = document.getElementById('#attributes.fieldname#')
				if (document.createEvent) { // DOM Level 2 standard
					var evt;
					evt = document.createEvent("HTMLEvents");
					evt.initEvent("change", false, true);
					el.dispatchEvent(evt);
				} else if (el.fireEvent) { // IE
					el.fireEvent('onchange');
				}

				jQuery('##select_#attributes.fieldname#').val(label);
			}
		}
	</script>
	</cfoutput>
</cf_head>

<cfif attributes.function eq "">
	<cfoutput>You must pass in a function.</cfoutput>
	<cfExit method="EXITTAG">
</cfif>

<cfoutput>
	<table cellpadding="0" cellspacing="0"><tr><td>
	<div style="float:left">
		<cf_relayFormElement required="#attributes.required#" class="searchBox" type="text" fieldname="select_#attributes.fieldname#" label="#attributes.label#" currentvalue="#attributes.currentValue#">
	</div>
	<div id="#attributes.fieldname#spinner" style="float:left; display:block;"></div>
	</td></tr></table>
</cfoutput>
<cf_relayFormElement type="hidden" fieldname="#attributes.fieldname#" currentvalue="#attributes.currentValue#" label="#attributes.label#">