﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayInputField.cfm
Author:				SWJ
Date started:		2006-03-22

Description:		This provides a standard input field which can display in various ways based on request.relayFormStyle

Examples:
<CF_relayInputField	currentValue="#query.dateField#" fieldName="frmFieldName" label="phr_labelText">
<CF_relayInputField	currentValue="#qryOrgname#" fieldName="insOrgName" label="Phr_Sys_OrgName (Phr_Sys_OrganisationID :<CFOUTPUT>#frmOrganisationID#</CFOUTPUT>)">
Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2006-10-24			WAB			Added a parameter attributes List so that a list of comma separated list of "valid values" be passed in.  Can do value|displayValue   (using verical pipe to delimit)
2007/03/28			NJH			Translate the label for javascript if it was passed as a phrase
2007/12/12			WAB			changed to use attribute collection
2009/03/11			NJH			CR-SNY675 - added bind function. You must pass in either a bindFunction or a query. Also moved some code from here to relayFormElement.cfm to consolidate some code.
2009-04-03			NYB			Sophos - fixed tab order of main Add Contact Form
2010/03/03			NJH			P-PAN002 - set the selected value for a dropdown when the dropdown is bound (ie. when bind function is specified). Aslo added readonly attribute, which
								means that only the selected value is shown.
2010/06/07			WAB			p_FNL088 Added useCFForm Attribute so code can be used without CFFORM.
								Not implemented for all controls yet
								Needs rwFormValidation.js to deal with required fields
2010/06/23			NAS			P-CLS002 - Clearswift Deal Reg
2010/07/			WAB			fnl088 added some cfoutputs, must be a displaycfoutputonly somewhere
2010/09/22			WAB			Allow th non cfform stuff to deal with either lists or queries
2011-05-25 			NYB 		REL106 added the option of passing list through without separate value for display
2011/12/05			WAB			Changed some long winded code to use a  new changeStructureKeys() function
2012/12/10			NJH			Roadmap 2013 - call relayForms.displayValidvalues to display the select box.
2014-08-26			AHL			Case 440849 Approving Deal Registrations Opportunities. Ignore Dummy queries
2014/11/03			DXC  		#442297 Issue with readonly select boxes when user hadn't previously selected a value
2014-11-13			NJH			Changed the way readonly was displayed.. Change the name on the select and make it disabled and provide a hidden field with the name of the original ID.
2015/02/19			DXC			443913 - fix for attributes.type not always defined & causing errors...
2016-01-29			WAB			Plumb in multiple select plugin.  Used by default.  reacts to existing useJqueryMultiSelect attribute
2016-02-01			WAB			A few more changes - make sure that attributes.multiple and attributes.displayAs are consistent
								Default Size based on value of multiple
2016/02/10			NJH			JIRA PROD2015-566: bit of a clean up. Pass through currentValue as part of merge struct to getValidValuesFromString
2017-02-22			WAB			PROD2016-3469 Removed call to defaultRelayFormFieldAttributes.  No longer serves a useful purpose and was giving odd message in some cases.  Added a default for attributes.label in the one place where might be a problem
Possible enhancements:

 --->
<cfsetting enablecfoutputonly="true">

<cfparam name="attributes.query" default="">
<cfparam name="attributes.bindFunction" default=""> <!--- NJH 2009/03/11 CR-SNY675 --->
<cfparam name="attributes.bindOnLoad" default="true"> <!--- NJH 2009/03/11 CR-SNY675 --->
<cfparam name="attributes.display" default="displayValue">
<cfparam name="attributes.value" default="dataValue">
<cfparam name="attributes.required" default="yes">
<cfparam name="attributes.multiple" default="No">
<cfparam name="attributes.size" default="#(attributes.multiple)?5:1#">
<cfparam name="attributes.displayAs" default="#(attributes.multiple)?'multiselect':'select'#"> <!--- select,radio,checkbox,multiselect --->
<!--- Quick check to make sure that multiple is set correctly, some code just sets displayAs=multiselect --->
<cfif listfindNoCase ("multiselect,twoselects,checkbox", attributes.displayAs)  and not attributes.multiple>
	<cfset attributes.multiple = true>
</cfif>
<cfparam name="attributes.disabled" default="No">
<cfparam name="attributes.style" default="">
<cfparam name="attributes.onClick" default="">
<cfparam name="attributes.queryPosition" default="below">
<!--- NYB 2009-04-03 - Sophos Stargate - added to give correct tabOrder to Add Contact Form --->
<cfparam name="attributes.tabIndex" default="">
<cfparam name="attributes.readonly" default=0>
<cfparam name="attributes.usecfform" type="boolean" default="#listfindnocase(getbasetaglist(),"CFFORM")#"> <!--- WAB 2010/06/07 trying something out for Settings Project--->
<cfparam name="attributes.validvalues" default="">
<cfparam name="attributes.useJqueryMultiSelect" default="#(attributes.multiple)?true:false#"> <!--- NJH 2014/06/17 - Task Core-64 - certifications relaytag --->
<cfparam name="attributes.class" default="form-control">


<cfif not request.relayFormDisplayStyle neq "HTML-div">
	<cfset attributes.class = attributes.class & " form-control">
</cfif>

<!--- JIRA PROD2015-566 NJH 2016/02/10 - bit of a clean up. Replaced below with this.
	JIRA BF-659 - only go through here if attributes.query is not a query and we don't have a bind function set --->
	
	<!--- 
	WAB 2016-11-16 (screen renderer) removed these items from if statement, since was causing query to be run even if bind function was set
			Couldn't work out why they were needed.  Should be enough for attributes.query to not be a query
	or attributes.validvalues neq "" or (structKeyExists(attributes,"list") and attributes.list neq "") --->
<cfif (not isQuery(attributes.query) and (attributes.bindFunction eq "" or attributes.bindonload eq false)) >
	<cfif attributes.validValues neq "">
		<cfset queryString = attributes.validValues>
	<cfelseif structKeyExists(attributes,"list") and attributes.list neq "">
		<cfset queryString = attributes.list>
	<cfelse>
		<cfset queryString = attributes.query>
	</cfif>

	<cfset attributes.query = application.com.relayForms.getValidValuesFromString(string=queryString,displayColumn=attributes.display,valueColumn=attributes.value,mergeStruct={currentValue=attributes.currentValue})>
</cfif>
	<!--- <cfif listLen(attributes.validvalues) eq 1>
		<cfset attributes.validFieldName = attributes.validvalues>
		<cfinclude template="../../validValues/getValidValues.cfm">

		<cfset attributes.query=validValues>
		<cfset attributes.display = "displayValue">
		<cfset attributes.value = "dataValue">

		<!--- 2010/06/23			NAS			P-CLS002 - Clearswift Deal Reg - null text has already been set, so we don't want it set again below --->
		<cfset structDelete(attributes,"nullText")>
	<cfelse>
		<cfset attributes.list = attributes.validValues>
	</cfif>
</cfif>


<!--- NJH 2010/03/24 P-PAN002 - if readonly, then just get the selected value
	WAB 2011/04/20 LID 6343, fixed bug by testing the datatype of the value column
	Also felt that NJH logic for setting currentValue to 0 was wrong - surely only necessary when currentValue is blank   (maybe should be if string)
--->
<cfif structKeyExists(attributes,"list")>
	<!--- create a query out of the list --->
	<!--- work out the datatype of the value column.... --->
	<cfif isNumeric(listFirst(listfirst(attributes.list),"|#application.delim1#"))>
		<cfset valueDataType = "integer">
	<cfelse>
		<cfset valueDataType = "varchar">
	</cfif>
	<cfset theQuery = queryNew("value,display","#valueDataType#,varchar")>

	<cfloop index="item" list = "#attributes.list#">
		<cfset value = listfirst(item,"|#application.delim1#")>  <!--- #application.delim1# (bent pipe) for backwards compatability with valid values--->
		<cfset display = listlast(item,"|#application.delim1#")>
		<cfset queryaddrow(theQuery,1)>
		<cfset querysetcell(theQuery,"value",value)>
		<cfset querysetcell(theQuery,"display",display)>
	</cfloop>
	<cfset attributes.query = theQuery>
	<cfset attributes.display = "display">
	<cfset attributes.value = "value">
</cfif> --->


<!--- NJH 2013/02/20 - moved the readonly check to be after the isList check, as attributes.query wasn't a query if it was a list.. and so readonly didn't work for validValue lists.. --->
<cfif attributes.readOnly>
	<!--- <cfif isQuery(attributes.query)>
		<cfset valueColumnString = application.com.relayForms.doesColumnNeedQuotes(queryObject = attributes.query, columnName = attributes.value)>
		<cfif attributes.currentValue  is "" and not valueColumnString>
			<cfset attributes.currentValue = 0>
		</cfif>

		<cfif attributes.query.RecordCount gt 0>		<!--- 2014-08-26 AHL Case 440849 Approving Deal Registrations Opportunities. --->
		    <cfquery name="attributes.query" dbType="query">
			    select * from attributes.query where [#attributes.value#] = <cfif valueColumnString>'#attributes.currentValue#'<cfelse>#attributes.currentValue#</cfif>
		    </cfquery>
		</cfif>		<!--- 2014-08-26 AHL Case 440849 Approving Deal Registrations Opportunities --->
	</cfif> --->
	<cfoutput><input type="hidden" value="#attributes.currentValue#" id="#attributes.fieldName#" name="#attributes.fieldName#"></cfoutput>
</cfif>

<!--- NJH 2009/03/11  CR-SNY675 - must pass in a query or a bind function --->
<cfif not isQuery(attributes.query) and attributes.bindFunction eq "">
	<cfoutput>You must pass in either a query object or a bind function.</cfoutput>
	<cfExit method="EXITTAG">
</cfif>

<!--- WAB 2007/12/12  wanted to be able to pass in optgroup to cfselect, but is one of those values which cannot be blank
	had to change code so that we could pass an attributeCollection
	This also meant that we could get rid of the duplication of code to deal with the disabled parameter (which cannot take value false)
	Tried just copying all the incoming attributes and passing through but ran into problems
 --->
	<cfscript>
		attributeCollection = duplicate(attributes); // NJH 2013/01/28 - duplicate the attributes, so that attributes get passed through by default. Not sure whether this will break anything or not. Shouldn't....
		//attributeCollection.queryPosition=attributes.queryPosition;
		attributeCollection.name=attributes.fieldName;
		//attributeCollection.id=attributes.id ;  // WAB id paramed earlier

		if (attributeCollection.readonly) {
			attributes.disabled=true;
			attributeCollection.name = attributeCollection.name&"_display";
			attributeCollection.id=attributeCollection.ID&"_display";
		}


		if (attributes.bindFunction neq "") {
			attributeCollection.bind=attributes.bindFunction;
			attributeCollection.bindOnLoad=attributes.bindOnLoad;
		}
		if (not isQuery(attributes.query)) {
			structDelete(attributeCollection,"query");
		}
		attributeCollection.selected=attributes.currentValue;
		/*
		attributeCollection.display=attributes.display;
		attributeCollection.value=attributes.value;

		attributeCollection.onChange=attributes.onChange;
		attributeCollection.required=attributes.required;
		attributeCollection.message=attributes.message;
		attributeCollection.size=attributes.size;
		attributeCollection.multiple=attributes.multiple;*/
		if (attributes.disabled) {
			attributeCollection.disabled=attributes.disabled;
			// can't have required on a disabled select box
			attributeCollection.required = false;
		} else {
			structDelete(attributeCollection,"disabled");
		}
		if (attributes.readOnly) {
			attributeCollection.readOnly=attributes.readOnly;
		} else {
			structDelete(attributeCollection,"readOnly");
		}
		/*attributeCollection.readOnly=attributes.readOnly;
		attributeCollection.style=attributes.style;
		attributeCollection.onClick=attributes.onClick;
		//NYB 2009-04-03 - Sophos Stargate - added to give correct tabOrder to Add Contact Form
		attributeCollection.tabIndex=attributes.tabIndex;
		if (structkeyexists (attributes,"group")) {
				attributeCollection.group = attributes.group;
		}*/
	</cfscript>

	<cfif attributes.useCFForm>  <!--- WAB 2010/06/08 for Settings Project/FNL088--->

		<cfset validCFSelectAttributes = "class,id,message,multiple,name,onchange,onClick,query,queryPosition,readonly,required,selected,size,style,tabIndex,display,value,disabled,bind,bindOnLoad">
		<cfloop collection="#attributeCollection#" item="attr">
			<cfif not listFindNoCase(validCFSelectAttributes,attr)>
				<cfset structDelete(attributeCollection,attr)>
			</cfif>
		</cfloop>

		<cfif structKeyExists(attributes,"query") and isQuery(attributes.query)>
			<cfset attributeCollection.query="attributes.query">
		</cfif>

		<cfselect
			attributeCollection = #attributeCollection#
			>
			<cfif structKeyExists(attributes,"nullText") and attributes.nullText neq "">
				<CFOUTPUT><option value="#attributes.nullValue#">#htmleditformat(attributes.nullText)#</option></CFOUTPUT>
			</cfif>

			<!--- P-PAN002 NJH 2010/03/03 - get the selected value to show when the dropdown is bound
			<cfif (structKeyExists(attributes,"bindFunction") and attributes.bindFunction neq "")
					and structKeyExists(attributes,"query") and isQuery(attributes.query)>

				<cfif not attributes.readOnly>
					<cfloop query="attributes.query">
						<CFOUTPUT><option value="#evaluate(attributes.value)#" <CFIF attributes.currentValue eq evaluate(attributes.value)>selected="selected"</CFIF> >#evaluate(attributes.display)#</option></CFOUTPUT>
					</cfloop>
				<cfelse>
					<cfloop query="attributes.query">
						<cfif attributes.currentValue eq evaluate(attributes.value)><CFOUTPUT><option value="#evaluate(attributes.value)#" selected="selected" >#evaluate(attributes.display)#</option></CFOUTPUT></cfif>
					</cfloop>
				</cfif>
			</cfif> --->
		</cfselect>

		<!--- 2006-06-05 NJH add hidden field for cfform server validation (should javascript be disabled) --->
		<cfif attributes.required eq "yes">
			<cfparam name="attributes.label" default = "#attributes.fieldName#">
			<cfinput type="hidden" name="#attributes.fieldName#_required" id="#attributes.fieldName#_required" value="#attributes.label# is required">
		</cfif>

	<cfelse>

		<cfif structKeyExists(attributes,"nullText") and attributes.nullText neq "">
			<cfset attributeCollection.nulltext =  attributes.nullText>
			<cfset attributeCollection.showNull = "true">
		<cfelseif structKeyExists(attributes,"showNull") and attributes.showNull>
			<cfset attributeCollection.showNull = "true">
		<cfelse>
			<cfset attributeCollection.showNull =  false>
		</cfif>

		<cfif attributes.multiple>
			<cfset attributeCollection.listSize = attributeCollection.Size>
		</cfif>

		<!--- 03-11-2014 DXC START #442297 --->
		<cfif attributes.readOnly>
			<!--- some explanation - when a form was set read only and a select was populated from a query, if the currentvalue was not a row
			in this query then the query was queryOfQueries to 0 rows but the Null handler was being suppressed--->
			<cfif structKeyExists(attributes,"type") and attributes.type eq "select"><!--- DXC 443913  --->
				<cfif isdefined("attributeCollection.currentValue") and (attributeCollection.currentValue eq "" or attributeCollection.currentValue eq "0")>
					<!--- we want the showNull handler to function normally where it's a select and the current selected value is 0 or NULL--->
			 	<Cfelseif not isdefined("attributeCollection.query")>
				 	<!--- readOnly is being handled by filtering the validvalues to one row, so don't show a null row --->
			 		<cfset attributeCollection.shownull = false>
			 	<Cfelseif isdefined("attributeCollection.query") and attributes.query.recordcount gt 0>
			 		<!--- if there are 1 or more query results matching currently selected value, suppress null handler --->
					<cfset attributeCollection.shownull = false>
				<Cfelse>
					<!--- we want the showNull handler to function normally --->
				</cfif>
			<Cfelse>
			 	<cfset attributeCollection.shownull = false><!--- readOnly is being handled by filtering the validvalues to one row, so don't show a null row --->
			</cfif>
		</cfif>
		<!--- 03-11-2014 DXC END #442297 --->

		<cfif attributeCollection.displayAs eq "twoSelects" and attributeCollection.size eq 1>
			<cfset attributeCollection.size = 5>
		</cfif>

		<cfif not structKeyExists(attributeCollection,"displayAs")>
			<cfset attributeCollection.displayAs = "select">
		</cfif>
		<!--- <cfif structKeyExists(attributes,"displayGroupAs")>
			<cfset attributeCollection.displayGroupAs = attributes.displayGroupAs>
		</cfif> --->


		<!--- if for some reason attributes.useJqueryMultiSelect has been set to false then we need to overide the default behaviour by setting the plugin option multipleSelect to false --->
		<cfif attributes.multiple and not attributes.useJqueryMultiSelect >
			<cfparam name="attributeCollection.pluginOptions" default = "#structNew()#">
			<cfset attributeCollection.pluginOptions.multipleSelect = false>
		</cfif>

		<cfoutput>#application.com.relayForms.validValueDisplay(argumentCollection=attributeCollection)#</cfoutput>
	</cfif>


<cfsetting enablecfoutputonly="false">