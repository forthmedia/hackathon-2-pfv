<!--- �Relayware. All Rights Reserved 2014 --->
<CFSETTING enablecfoutputonly="YES">
<!---
    XMLDataSource
    
    Reads the XML data and parses it

    David Maddison maddog@wildfusion.com, WildFusion.com, 2001
    Based on ideas by Tom Dyson, Torchbox.com, 2001
--->

<!--- Start Tag Code --->
<CFIF ThisTag.ExecutionMode EQ "Start">

    <!--- Required Parameters --->
    <CFPARAM Name="Attributes.Name">
    
    <!--- Optional Parameters --->
    <CFPARAM Name="Attributes.Type" Default="Text">
    <CFPARAM Name="Attributes.URL" Default="">
    <CFPARAM Name="Attributes.ProxyServer" Default="">
    <CFPARAM Name="Attributes.ProxyPort" Default="">
    <CFPARAM Name="Attributes.FileName" Default="">
    <CFPARAM Name="Attributes.CachedWithin" Default="">
    <CFPARAM Name="Attributes.CacheName" Default="#Attributes.Name#">    
    
    <!--- Check the input parameters --->
    <CFSWITCH Expression="#Attributes.Type#">
        <CFCASE Value="URL">
            <CFIF Attributes.URL EQ "">
                <CFTHROW Type="XMLDatasource.MissingURL" Message="MissingURL Attribute :You must supply the URL, for type URL">
            </CFIF>
        </CFCASE>
        
        <CFCASE Value="File">
            <CFIF Attributes.FileName EQ "">
                <CFTHROW Type="XMLDatasource.MissingFileName" Message="MissingFileName Attribute: You must supply the filename for type File">
            </CFIF>
        </CFCASE>
    </CFSWITCH>

    <!--- If CachedWithin is specified, see if this is already in the cache --->
    <CFIF Attributes.CachedWithin NEQ "">

        <!--- Make sure the CacheName attribute has been passed --->
        <CFIF Attributes.CacheName EQ "">
            <CFTHROW Type="XMLDatasource.MissingCacheName" Message="Missing CacheName Attribute: You must supply a cache name attributes to use CachedWithin">
        </CFIF>
    
        <!--- Make Sure the application scope has been initialised --->
        <CFIF NOT IsDefined("Application.ApplicationName")>
            <CFTHROW Type="XMLDataSource.ApplicationMissing" Message="XMLDataSource Cache Error: You must specify an applicatoin to use caching.  Use CFAPPLICATION">
        </CFIF>
        
        <!--- Make sure the XMLDatasource Cache has been created --->
        <CFIF NOT StructKeyExists(Application, "XMLDataSourceCache")>
            <CFLOCK Name="XMLDataSourceCache" Type="Exclusive" Timeout="60">
                <CFSET Application.XMLDataSourceCache = StructNew()>               
            </CFLOCK>
        </CFIF>
        
        <CFSET bFoundInCache = False>
        
        <!--- See if this is in the cache --->
        <CFLOCK Name="XMLDataSourceCache" Type="ReadOnly" Timeout="60">
            <CFIF StructKeyExists(Application.XMLDataSourceCache, Attributes.CacheName)>
                <CFSET stCache = Application.XMLDataSourceCache[Attributes.CacheName]>

                <!--- See if we should use this cached version --->
                <CFIF now() - Attributes.CachedWithin LE stCache.TimeStamp>

                    <!--- Return the cached parser --->
                    <CFSET "Caller.#Attributes.Name#" = stCache.Parser>

                    <!--- Get out of here! --->
                    <CFSET bFoundInCache = True>
                </CFIF>
            </CFIF>
        </CFLOCK>        

        <!--- Set a flag to tell the end tag to cache this --->
        <CFSET bCache = TRUE>
    <CFELSE>
        <CFSET bCache = FALSE>       
        <CFSET bFoundInCache = False>         
    </CFIF>
        
    <!--- Create variable to hold the XML --->
    <CFSET XMLDoc = "">
</CFIF>
    
<!--- End tag Code --->    
<CFIF NOT bFoundInCache AND ((ThisTag.HasEndTag AND ThisTag.ExecutionMode EQ "End") OR NOT ThisTag.HasEndTag)>    

    <!--- Retrieve the XML Data based on the type --->
    <CFSWITCH Expression="#Attributes.Type#">
   
        <CFCASE Value="Text">        

            <!--- If there is no end tag, maybe the XML isn't inline --->
            <CFIF NOT ThisTag.HasEndTag>
            
                <!--- Lets see if we can provide an intelligent message --->
                <CFIF Attributes.FileName NEQ "">                
                    <CFTHROW Type="XMLDatasource.MissingXML" Message="XMLDatasource : No XML Specified, use Type='File' to specify a filename">
                <CFELSE>
                    <CFIF Attributes.URL NEQ "">
                        <CFTHROW Type="XMLDatasource.MissingXML" Message="XMLDatasource : No XML Specified, use Type='URL' to specify a URL">                    
                    <CFELSE>
                        <CFTHROW Type="XMLDatasource.MissingXML" Message="XMLDatasource : No XML Specified, you must specify the XML inside the tag, or from a file or URL">                                            
                    </CFIF>                    
                </CFIF>
            </CFIF>
                            
            <!--- Retrieve XML from the generated content --->
            <CFSET XMLDoc = ThisTag.GeneratedContent>

            <!--- Resolve any variables in the XMLDoc --->
            <CFSET STart=1>
            <CFSET pos = REFind("[##][^## #chr(13)##chr(10)#]+[##]",XMLDoc,Start,true)>
    
            <!--- Loop until all variable names are resolved --->    
            <CFLOOP Condition="pos.pos[1] NEQ 0">
            
                <!--- Fetch the matched expression from the XMLDoc --->
                <CFSET Expression = Mid(XMLDoc,pos.pos[1],pos.len[1])>
                
                <!--- Work out what the variable is --->
                <CFSET newExp = "Caller.#Mid(Expression, 2, Len(Expression) -2)#">
                
                <!--- Remove the dynamic expression from the XMLDoc --->
                <CFSET XMLDoc = RemoveChars(XMLDoc, pos.pos[1], pos.len[1])>
                
                <!--- Catch any errors --->
                <CFTRY>
                
                    <!--- Insert caller string --->
                    <CFSET XMLDoc = Insert(Evaluate(newExp),XMLDoc,pos.pos[1] -1)>
                
                    <CFCATCH Type="Expression">
                        <CFTHROW Type="XMLDatasource.Expression" Message="XMLDatasource: The Expression #Expression# cannot be resolved">
                    </CFCATCH>
                </CFTRY>
            
                <!--- Find any more expressions --->
                <CFSET Start = pos.pos[1] + Len(newExp)>
                <CFSET Pos = REFind("[##][^##]*[##]",XMLDoc,Start,true)>
            </CFLOOP>
                
        </CFCASE>
        
        <CFCASE Value="File">

            <!--- Retrieve XML From A File --->
            <CFTRY>
                <CFFILE Action="Read" File="#Attributes.FileName#" Variable="XMLDoc"> 
                
                <CFCATCH Type="Any">
                    <CFTHROW Type="XMLDatasource.ErrorReadingFile" Message="FileReading Error : #CFCATCH.Message#">
                </CFCATCH>
            </CFTRY>
        </CFCASE>

        <CFCASE Value="URL">
            
            <!--- Retrieve XML From a URL --->
            <CFTRY>
                <!--- Decide how to call the URL --->
                <CFIF Attributes.ProxyServer EQ "">
                    <CFHTTP URL="#Attributes.URL#" Method="Get" ThrowOnError="Yes">                    
                <CFELSE>
                    <CFIF Attributes.ProxyPort EQ "">
                        <CFHTTP URL="#Attributes.URL#" Method="Get" ProxyServer="#Attributes.ProxyServer#" ThrowOnError="Yes">
                    <CFELSE>
                        <CFHTTP URL="#Attributes.URL#" Method="Get" ProxyServer="#Attributes.ProxyServer#" ProxyPort="#Attributes.ProxyPort#" ThrowOnError="Yes">                    
                    </CFIF>
                </CFIF> 
                
                <!--- Read the URL Contents into the string --->
                <CFSET XMLDoc = CFHTTP.FileContent>
                
                <CFCATCH Type="Any">
                    <CFTHROW Type="XMLDataSource.ErrorRetrivingURL" Message="URLRetrival Error: #CFCATCH.Message#">
                </CFCATCH>
            </CFTRY>
        </CFCASE>
    </CFSWITCH>

    <!--- Create the XML Parser --->
    <CFOBJECT Action="create" Class="MSXML2.FreeThreadedDOMDocument" Name="xom">
    <CFSET xom.validateonparse = true>

    <CFTRY>
    
        <!--- Load up the XML text into the Parser --->
        <CFSET temp = xom.loadXML('#XMLDoc#')>

        <CFCATCH Type="ANY">
            <CFTHROW Type="XMLDatasource.ParseError" Message="XMLDataSource.ParseError : #CFCATCH.Message#">
        </CFCATCH>
    </CFTRY>        

    <!--- Check for errors --->
    <CFSET ParseError = xom.parseerror>
    <CFIF ParseError.ErrorCode NEQ 0>
        <CFTHROW Type="XMLDataSource.ParserError" Message="XMLParseError : #ParseError.Reason#, Line:#ParseError.Line#, Text:#HTMLEditFormat(ParseError.srcText)#">
    </CFIF>       

    <!--- See if we should store this parser in the cache --->
    <CFIF bCache>

        <!--- Now Create a cache structure --->
        <CFSET stCache = StructNew()>
        <CFSET stCache.Parser = xom>
        <CFSET stCache.TimeStamp = now()>

        <!--- Cache this structure --->
        <CFLOCK Name="XMLDatasourceCache" Type="Exclusive" Timeout="60">
            <CFSET Application.XMLDataSourceCache[Attributes.CacheName] = stCache>
        </CFLOCK>
    </CFIF>
    
    <!--- Clear the generated content so that it doesn't appear in the output stream--->
    <CFSET ThisTag.GeneratedContent = "">
    
    <!--- Return the parser --->
    <CFSET "Caller.#Attributes.Name#" = xom>
</CFIF>

<!--- Wipe the Generated content --->
<CFSET ThisTag.GeneratedContent ="">

<CFSETTING enablecfoutputonly="NO">