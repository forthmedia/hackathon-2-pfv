<!--- �Relayware. All Rights Reserved 2014 --->
<CFSETTING enablecfoutputonly="YES">
<!---
    XMLQuery
    
    Provides a QUERY interface to XML documents
    
    David Maddison maddog@wildfusion.com, WildFusion.com, 2001
    Based on Work by Tom Dyson, Torchbox.com, 2001
--->

<CFIF ThisTag.ExecutionMode EQ "Start">
    <!--- Requred Parameters --->
    <CFPARAM Name="Attributes.Datasource">
    <CFPARAM Name="Attributes.Name">
    
    <!--- Optional Parameters --->
    <CFPARAM Name="Attributes.CachedWithin" Default="">
    <CFPARAM Name="Attributes.SortColumn" Default="">
    <CFPARAM Name="Attributes.SortDirection" Default="asc">
    <CFPARAM Name="Attributes.SortType" Default="text">
    <CFPARAM Name="Attributes.bThrowOnEmpty" Default="NO">
    <CFPARAM Name="Attributes.Type" Default="XPath">
        
    <!--- Make sure the given datasource exists --->
    <CFIF NOT IsDefined("Caller.#Attributes.Datasource#")>
        <CFTHROW Type="XMLQuery.DatasourceMissing" Message="XMLQuery : Datasource '#Attributes.Datasource#' not Found">
    <CFELSE>
        <!--- Get a reference to the datasource --->
        <CFSET xom = Evaluate("Caller.#Attributes.Datasource#")>
    </CFIF>
    
    <!--- Make sure the query type if either XPath or XQuery --->
    <CFIF ListFind("XPath,XQuery,SQL",Attributes.Type) EQ 0>
        <CFTHROW Type="XMLQuery.InvalidTypeParam" Message="XMLQuery: Type can only have the values XPath, XQuery or SQL, not '#Attributes.Type#'">
    </CFIF>
  
<CFELSE>

    <!--- Get the XPath Info --->
    <CFSET XPath = ThisTag.GeneratedContent>

    <!--- Resolve any variables in the XPath --->
    <CFSET STart=1>
    <CFSET pos = REFind("[##][^## #chr(13)##chr(10)#]+[##]",XPath,Start,true)>

    <!--- Loop until all variable names are resolved --->    
    <CFLOOP Condition="pos.pos[1] NEQ 0"> 
    
        <!--- Fetch the matched expression from the XPath --->
        <CFSET Expression = Mid(XPath,pos.pos[1],pos.len[1])>
        
        <!--- Work out what the variable is --->
        <CFSET newExp = "Caller.#Mid(Expression, 2, Len(Expression) -2)#">
        
        <!--- Remove the dynamic expression from the XPath --->
        <CFSET XPath = RemoveChars(XPath, pos.pos[1], pos.len[1])>
        
        <!--- Catch any errors --->
        <CFTRY>
        
            <!--- Insert caller string --->
            <CFSET XPath = Insert(Evaluate(newExp),XPath,pos.pos[1] -1)>
        
            <CFCATCH Type="Expression">
                <CFTHROW Type="XMLQuery.Expression" Message="XMLQuery: The Expression #Expression# cannot be resolved">
            </CFCATCH>
        </CFTRY>
    
        <!--- Find any more expressions --->
        <CFSET Start = pos.pos[1] + Len(newExp)>
        <CFSET Pos = REFind("[##][^##]*[##]",XPath,Start,true)>
    </CFLOOP>
    
    <!--- Clear Generated Content --->
    <CFSET ThisTag.GeneratedContent = "">
       
    <CFSET bFoundInCache = FALSE>
    
    <!--- If CachedWithin is specified, see if this is already in the cache --->
    <CFIF Attributes.CachedWithin NEQ "">

        <!--- Make Sure the application scope has been initialised --->
        <CFIF NOT IsDefined("Application.ApplicationName")>
            <CFTHROW Type="XMLQuery.ApplicationMissing" Message="XMLQuery Cache Error: You must specify an applicatoin to use caching.  Use CFAPPLICATION">
        </CFIF>
        
        <!--- Make sure the XMLQueryCache Cache has been created --->
        <CFIF NOT StructKeyExists(Application, "XMLQueryCache")>
            <CFLOCK Name="XMLQueryCache" Type="Exclusive" Timeout="60">
                <CFSET Application.XMLQueryCache = StructNew()>               
            </CFLOCK>
        </CFIF>
                
        <!--- See if this is in the cache --->
        <CFLOCK Name="XMLQueryCache" Type="ReadOnly" Timeout="60">
            <CFIF StructKeyExists(Application.XMLQueryCache, Attributes.Datasource)>
            
                <!--- Get the query cache for this datasource --->
                <CFSET stDatasourceQueryCache = Application.XMLQueryCache[Attributes.Datasource]>
            
                <CFSET QueryCacheKey = URLEncodedFormat(XPath)>
            
                <!--- See if this query is in the datasource cache --->
                <CFIF StructKeyExists(stDatasourceQueryCache, QueryCacheKey)>
            
                    <CFSET stQuery = stDatasourceQueryCache[URLEncodedFormat(XPath)]>

                    <!--- See if we should use this cached version --->
                    <CFIF now() - Attributes.CachedWithin LE stQuery.TimeStamp>
                    
                        <!--- Return the cached Query --->
                        <CFSET "Caller.#Attributes.Name#" = stQuery.Query>

                        <!--- Tell the tag to exit after this lock --->
                        <CFSET bFoundInCache = True>
                    </CFIF>

                </CFIF>            
            </CFIF>
        </CFLOCK>        
        
        <!--- Set a flag to tell the end tag to cache this --->
        <CFSET bCache = TRUE>
    <CFELSE>
        <CFSET bCache = FALSE>        
    </CFIF>

  <!--- Hack Alert : Use if to skip rather than CFEXIT which seems to keep crashing the CF Server --->    
  <CFIF NOT bFoundInCache>        

    <!--- Create variable to hold the XML --->
    <CFSET XMLDoc = "">
    
    <!--- load xml into dom and get out elements from supplied xpath --->
    <CFSET de = xom.documentElement>

    <!--- Query the XML, based on the Query Type --->
    <CFIF Attributes.Type EQ "XPath">
        
        <CFTRY>
            <CFSET nodeList = de.selectNodes(XPath)>
    
            <CFCATCH Type="Any">
                <CFTHROW Type="XMLQuery.XPathError" Message="XMLQuery XPathError : Error Applying XPath">
            </CFCATCH>
        </CFTRY>        
    <CFELSE>
        <CFIF Attributes.Type EQ "SQL">
            <CFSET nodeList = de.selectNodes("*")>
        <CFELSE>
            <CFTHROW Type="XMLQuery.XQueryNotImplemented" Message="XMLQuery : XQuery queries are not available yet">
        </CFIF>            
    </CFIF>
        
    <CFSCRIPT>
    	/* get the attributes for the selected element */
    	pnode = nodeList.item(0);
    	if (isdefined('pnode.childnodes')) {
    	    pchildren = pnode.childnodes;
    	    name = pNode.nodeName;
    	    atts = pnode.selectNodes('@*');
    	    attslist = '';
    	}
    	else {
    	    abortnow = 'yes';
    	}
    </CFSCRIPT>
      
    <CFSET bRecordsetEmpty = False>        
    
    <!--- Return an empty record set --->
    <CFIF IsDefined('abortnow')>
        <CFIF Attributes.bThrowOnEmpty>
            <CFTHROW Type="XMLQuery.EmptyRecordset" Message="XMLQuery : EmptyRecordset">
        <CFELSE>
            <CFSET EmptySet = QueryNew('')>
            
            <CFSET "Caller.#Attributes.Name#" = EmptySet>
            <CFSET bRecordsetEmpty = TRUE>
        </CFIF>        
    </CFIF>
    
<!--- Hack Alert: Skip this code rather than use CFEXIT which seems to crash CF Server --->
<CFIF NOT bRecordsetEmpty>    
    
    <!--- create a list of attributes from the first element so we can define a new query --->
    <cfif atts.length gt 0>
    	<cfloop from="0" to="#Evaluate("atts.length - 1")#" index="i">
    		<cfset att = atts.item(i)>
    		<cfset attslist = listAppend(attslist,"#name#_#att.nodeName#")>
    	</cfloop>
    </cfif>
    
    <!--- slow but flexible --->
    <!--- check all returned elements to see if they have new attributes --->
    <cfloop from="1" to="#Evaluate("nodelist.length - 1")#" index="j">
    		<cfset thisnode = nodeList.item(j)>
    		<cfset thisatts = thisnode.selectNodes('@*')>
    		<cfloop from="0" to="#Evaluate("thisatts.length - 1")#" index="k">
    			<cfset thisatt = thisatts.item(k)>
    			<cfif not listfindnocase(attslist,"#name#_#thisatt.nodename#")>
    				<cfset attslist = listAppend(attslist,"#name#_#thisatt.nodeName#")>
    			</cfif>
    		</cfloop>
    </cfloop>
    <!--- /slow but flexible --->
    
    <!--- add to the list of attributes from childnodes of the first element, if it has any --->
    <cfif pchildren.length gt 1>
    	<cfloop from="0" to="#Evaluate("pchildren.length - 1")#" index="m">
    		<cfset att = pchildren.item(m)>
    		<cfif not listfindnocase(attslist,"#name#_#att.nodename#")>
    			<cfset attslist = listAppend(attslist,"#name#_#att.nodeName#")>
    		</cfif>
    	</cfloop>
    </cfif>

    <!--- create an empty query --->
    <cfset xmlcolumns = name & ',' & attslist>
    <cfset xmlq = QueryNew(xmlcolumns)>

    <!--- loop over each returned element --->
    <cfloop from="0" to="#Evaluate("nodelist.length-1")#" index="i">
    
    	<!--- get the element name and value --->
    	<cfset onode = nodeList.item(i)>
    	<cfset name = oNode.nodeName>
    	<cfset value = onode.text>

    	<!--- get all attributes for the element --->
    	<cfset atts = oNode.selectNodes('@*')>
    	<cfset attvaluelist = "">
    	<cfset myattslist = "">
    	<cfif atts.length gt 0>
    		<cfloop from="0" to="#Evaluate("atts.length - 1")#" index="j">
    			<cfset att = atts.item(j)>
    			<cfset myattslist = listAppend(myattslist,"#name#_#att.nodeName#",'|')>
    			<cfif len(att.text) eq 0><cfset att.text = "_nothing_"></cfif>
    			<cfset attvaluelist = listAppend(attvaluelist,att.value,'|')>
    		</cfloop>
    	</cfif>
    	
    	<!--- get all childnodes for the element, if it has any --->
    	<cfset kinder = onode.childnodes>
    	<cfif kinder.length gt 1>
    		<cfloop from="0" to="#Evaluate("kinder.length - 1")#" index="kin">
    			<cfset eachkinder = kinder.item(kin)>
    			<cfset myattslist = listAppend(myattslist,"#name#_#eachkinder.nodename#","|")>
    			<cfif len(eachkinder.text) eq 0><cfset eachkinder.text = "_nothing_"></cfif>
    			<cfset attvaluelist = listAppend(attvaluelist,eachkinder.text,"|")>
    		</cfloop>
    	</cfif>
    	
    	<cfset temp = QueryAddRow(xmlq)>
    	<!--- add the first element name and its value to the query --->
    	<cfset temp = QuerySetCell(xmlq,name,value)>
    	<cfset counter = 1>
    	<cfloop list = "#myattslist#" index = "i" delimiters="|">
    	<cfscript>
    		valuetoput = replace(listgetat(attvaluelist,counter,'|'),'_nothing_','','one');
    		QuerySetCell(xmlq,i,valuetoput);
    		counter = counter + 1;
    	</cfscript>
    	</cfloop>
    </cfloop>
    <!--- now sort query, if required--->
    <cfif attributes.sortcolumn NEQ "">
    	<cfscript>
    	/* establish local versions of queries */
    	MyArray = ArrayNew(1);
    	MyQuery = xmlq;
    	NewQuery = QueryNew(MyQuery.ColumnList);
    	/* get out sort arguments from list */
    	sortcolumn = attributes.sortcolumn;
    	sortorder =  attributes.sortdirection;      
    	sorttype =  attributes.sorttype;  
    	</cfscript>

    	<!--- add rownumber to end of each row's value --->
    	<CFLOOP query="MyQuery">
    	  <cfset myarray[currentrow] = Evaluate("MyQuery.#sortcolumn#") & numberformat(currentrow, "000009")>
    	  <cfset temp = QueryAddRow(NewQuery)>
    	</CFLOOP>
    	
    	<!--- sort array --->
    	<cfset temp = ArraySort(MyArray, sorttype, sortorder)>
    	
    	<!--- populate new query, row by row, with appropriate row of new query --->
    	<cfloop from=1 to=#MyQuery.RecordCount# index="This">
    	  <cfset row = Val(Right(MyArray[This], 6))>
    	  <cfloop list="#MyQuery.ColumnList#" index="Col">
    	    <cfset temp = QuerySetCell(NewQuery, col, evaluate("myquery.#col#[row]"), this)>
    	  </cfloop>
    	</cfloop>
    	<cfset xmlq = myquery>
    </cfif>
            
    <!--- See if we should store this parser in the cache --->
    <CFIF bCache>
            
        <!--- Now Create a cache structure --->
        <CFSET stCache = StructNew()>
        <CFSET stCache.Query = xmlq>
        <CFSET stCache.TimeStamp = now()>

        <!--- Cache this structure --->
        <CFLOCK Name="XMLDatasourceCache" Type="Exclusive" Timeout="60">
        
            <!--- Make sure the datasource cache is created --->
            <CFIF NOT StructKeyExists(Application.XMLQueryCache, Attributes.Datasource)>
                <CFSET Application.XMLQueryCache[Attributes.Datasource] = StructNew()>
            </CFIF>
           
            <!--- Add this query to the datasource cache --->
            <CFSET Application.XMLQueryCache[Attributes.Datasource][URLEncodedFormat(XPath)] = stCache> 
        </CFLOCK>
    </CFIF>
        
    <!--- Do a QofQ if type is SQL --->
    <CFIF Attributes.Type EQ "SQL">
        <!--- Rename the query to the same name as the datasource --->
        <CFSET "#Attributes.Datasource#" = xmlq>
    
        <CFTRY>              
        
            <!--- Perform Query --->
            <CFQUERY name="#Attributes.Name#" datasource="Attributes.Datasource" dbtype="Query">
                #PreserveSingleQuotes(XPath)#
            </CFQUERY>
        
            <CFCATCH type="Database">
                <CFTHROW Type="XMLQuery.SQLQueryError" Message="XMLQuery.SQLQueryError : #CFCATCH.Message#  <br><b>Available columns</b> : #xmlq.ColumnList# <br><b>SQL:</b>#htmleditformat(XPath)#">    
            </CFCATCH>
        </CFTRY>
        
        <!--- Return the query --->
        <CFSET "Caller.#Attributes.Name#" = Evaluate("#Attributes.Name#")>
    <CFELSE>        
    
        <!--- Return the query --->
        <CFSET "Caller.#Attributes.Name#" = xmlq>
    </CFIF>        
        
  </CFIF>    
 </CFIF>  
</CFIF>

<CFSETTING enablecfoutputonly="NO">