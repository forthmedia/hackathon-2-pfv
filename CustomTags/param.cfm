<!--- 
�Relayware. All Rights Reserved 2014
WAB 2009/04/28 
WAB 2011/11/28 extended to deal with attributes other than name and default
<CFPARAM> will not allow random attributes
This is a custom version of cfparam which can have additional attributes.
The additional parameters are ignored when the template is called, but can be read out of the source file in order to create eg XML editors 

CFPARAM will not take an attributeCollection so need to do different cases long hand
TYPE and PATTERN can be defaulted, but DEFAULT, MAX and MIN cannot
So ended up with a few if statements to do the different combinations
Don't have max/min with no default (do we ever use max/min anyway!)

2013-09-18 	WAB Added some non standard attributes and types which alter behaviour when cf_param is called (as opposed to the additional attributes which we ignore)
				Added support for a numericList type
				Added support for allowNull (which enables you to specify a type but with the option of a null value)
2016-01-15 	WAB PROD2015-518 Added a try catch and then rethrow so that it reports the file and line where the <cf_param> was actually called
--->

<cfparam name="attributes.name">
<cfparam name="attributes.type" default = "any">
<cfparam name="attributes.pattern" default = "">

<cfparam name="attributes.allowNull" default = "false">

<!--- 	WAB 2013-09-18 Our own allowNull attribute.  
		Need to do a CFPARAM, then test for value being null, 
		If null then can exit, otherwise leave to go through the type checking later in the code
 --->

<cfif attributes.allowNull>
	<cfparam name="caller.#attributes.name#" default="#attributes.default#">
	<cfif caller[attributes.name] is "">
		<cfexit>		
	</cfif>
</cfif>


<!--- 	WAB 2013-09-18 Added own numericList data type. A convenient shorthand for a regExp 
		WAB 2013-10-17 Extended so will do any regExp defined in regExp.standardexpressions
		Currently   numericList, ID, nameCharacters
		Could cause problems if someone adds one with the same name as a built in type
		Perhaps should be a custom attribute of our own
 --->

<cfif listFindNoCase(structKeyList(application.com.regExp.standardexpressions), attributes.type) >
	<cfset attributes.pattern = application.com.regExp.standardexpressions[attributes.type]>  
	<cfset attributes.Type = "regex">
</cfif>

<cftry>
	<CFIF not structKeyExists(attributes,"default")>
		<cfparam name="caller.#attributes.name#" type="#attributes.type#" pattern="#attributes.pattern#">
	<cfelseif attributes.type is "range">
		<cfif structKeyExists(attributes,"min")>
			<cfparam name="caller.#attributes.name#" type="range" min = #attributes.min#>
		</cfif>
		<cfif structKeyExists(attributes,"max")>
			<cfparam name="caller.#attributes.name#" type="range" min = #attributes.max#>
		</cfif>
	<CFELSE>
		<cfif attributes.type eq "numeric">
			<cfset attributes.default = val(attributes.default)>
		</cfif>
		<cfparam name="caller.#attributes.name#" default="#attributes.default#" type="#attributes.type#" pattern="#attributes.pattern#">
	</CFIF>

	<cfcatch>
		<!---  WAB 201It would be use--->
		<cfif cfcatch.message IS "Invalid Parameter Type.">
			<cfset callingTemplate = application.com.globalfunctions.getFunctionCallingFunction()>
			<cfset newDetail = cfcatch.Detail & " File:#callingTemplate.templatePath#. Line:#callingTemplate.linenumber#">
			<cfif structKeyExists (caller,attributes.name)>
				<cfset value = caller[attributes.name]>
				<cfset newDetail = replace(newDetail,"value","value (#caller[attributes.name]#)")>
			</cfif>
			
			<cfthrow detail="#newDetail# " message = "#cfcatch.message#">
		<cfelse>
			<cfrethrow>
		</cfif>

	</cfcatch>
</cftry>
