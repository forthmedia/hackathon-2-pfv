<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			loadRelayINIfile.cfm
Author:				SWJ
Date started:		2003-07-26
	
Description:		

This custom tag is called from some application.cfm files.  It checks if an INI file exists and call
 the INI file if one is found.

 Usage:
Place it in a second level application.cfm after any variables are set e.g.

<cfscript>
// any variables below can be overridden in customer INI
defaultVarName = "setting";
</cfscript>
<!--- any of the above variables can be over ridden in the INI file --->
<cf_loadRelayINIfile iniFileName="forecastINI">



Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Possible enhancements:


 --->

<CFIF not isDefined("attributes.iniFileName")>
	<cfabort showerror="iniFileName must be passed to loadRelayINIfile">
</CFIF>

<cfif fileexists("#application.paths.code#\cftemplates\#attributes.iniFileName#.cfm")>
	<cfinclude template="/code/cftemplates/#attributes.iniFileName#.cfm">
</cfif>
