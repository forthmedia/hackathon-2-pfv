<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">

<!---
File name:			addPartnerRegistrationDataForm.cfm
Author:				RPW
Date started:		2014-09-19

Purpose:	This is a copy of the code file.
			Y:\clientCodeFiles\RW2014Roadmap2_PartnerCloud\Code\CFTemplates\Modules\Registration\CustomTags\addRelayContactDataForm.cfm


Usage: 	<cf_addRelayContactData
			datasource = "relayDB"
			attributes.showCols = "" mandatory list of columns that will show
			madatoryCols = "" mandatory the list of cols that will be mandatory
			defaultCountryID = "9" mandatory
			frmNext = "" optional - where it goes after it has processed
			frmDebug = "" optional yes or no - if set to yes the frmNext form will not be processed.
						instead a message telling you what happened will show.
			frmReturnMessage = optional yes or no.  Default is no. Returns a success/failure message.
			flagStructure = this contains a structure containing flag data to set. See below.
			showMagicNumber
			>

		The form submits to remoteAdd.cfm
		By default person.firstname, person.lastname, person.email and coname are shown.
		If coname is blank then the person is added to a company called people with no org.
		If country is omitted then it defaults to the UK - id 9.

		FlagStructure - this should contain rows in a structure containing the fields
		required to set a flag.	These are:
			frmEntityTypeID - 0 for person, 1 for location, 2 for org
			frmFlagID - The ID of the Profile attribute
			frmvalue - the value to set

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

10/03/2005 WAB & SWJ Changed the salutation field so that it uses cfselect and a slightly modified version
			of the cfForm.js which is in the Relay/javascript folder.
06/07/2005 WAB altered any validvalue fields to use cfselect with a new call to cf_getvalidvalues.  required new file customtags\validvalues\getValidValues.cfm
			meant I was able to remove code which considered salutation to be a special case
09/05/2006			AJC			country HAS to be selected first, then the form reloads.
27/06/2006	GCC	P_SNY039 Added language select, VAT number and converted to use CF_relayFormDisplay
15/01/2007  SSS Added a step metheod to use it pass increment in the datacapturemethod varible. Can pass in
                 token and orgnisation name to prepopulate the fields.
12/04/2007	AJC Added OrganisationTypeID as a hidden field for later use. REF[Trend]
26/04/2007	SSS	Added Ajax to build dropdown for languages depending on country selected
05/10/2007  GAD Added Phone masking by country features
23/11/2007	NJH	Added a region dropdown which populates address9 if address9 is shown.
10/04/2008  SSS I have added a switch that will enable people to by pass the org check if they pass in a magic number if the magic number is incorrect it will then
				Make you do a check.
12/05/2008	SSS	I have added a error meassage if the Email link is sent incorrectly.
11/08/2008	PPB Added 'Select A Value' to County/Region/State (Province) dropdown
2008/09/24	WAB CR-TND561 Req8 Mods for checking emailUniqueness
2008/11/10  NJH Bug Fix Trend Nabu Support Issue 1301 - added a case of province and region for incremental type.
14/11/2008	AJC Hide Magic Number
2009/07/09	NJH	P-FNL069 - validate firstname and lastname by accepting only alphabetic UTF characters plus space. This is using the cfform validation.
2009/07/31      SSS     P-fnl069 Add captcha to the contact form
2009-08-26	NYB  LHID2527 - fixed tab order of fields
2009/10/29	NAS	LID - 2782 - Translate 'Location'
2010/03/24	NAS LID - 3155 - Captcha issue
2010/06/30  PPB P-PAN002 New Opp screen - we want to limit the country dropdown to list just the countries relevant to the reseller
2010/09/21	NJH	LID 3956 Check the org type when doing unique email validation. Skip the check for end customers
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2011/05/23 	PPB LID5656 language dropdown wasn't populating sometimes
2011/10/12  PPB LID7969 don't reset request.relayCurrentUser.showForCountryID if the user is logged in (in which case the user will be registering a deal for a 3rd party not registering themselves for the first time)
2014-10-14	RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site
2015-09-15	MDC	FIFTEEN-407	Remove the catpcha from the company page of the registration.  When available it just kept looping around back to the person screen, where you had to fill out the captcha details again.
2016/02/10	NJH	JIRA Prod2015-343 - cleaned up this file and added domain matching.
2016-03-09 	WAB	 PROD2016-684 Added attribute relayFormDisplayClass
2016-10-03	WAB Added an isQoQ to a cf_queryparam - allows it to run in our debug mode
2016-12-30	MRE PROD2016-2620 Added form auto population and loc/org field locking when entering registration from SSO

Possible enhancements:
1.  Convert qRelayFieldList to a table in relayDB with a default field label

 --->

<cfparam name = "attributes.showCaptcha" default="false"> <!--- NJH 2009/07/20 P-FNL069 captcha prototype --->
<cfparam name = "attributes.personScreenID" default="">
<cfparam name = "attributes.organisationScreenID" default="">

<cfparam name="attributes.domainMatch" default="false" type="boolean"/>
<cfparam name="attributes.restrictDomainMatchByCountry" default="true" type="boolean"/>
<cfparam name="attributes.allowUnknownDomain" default="true" type="boolean"/>
<cfparam name="attributes.allowNewAddress" default="false" type="boolean"/>

<!---MRE 2016/12/30 PROD2016-2620 form pre-filling with SSO data --->
<cfparam name="attributes.autoPopulate" default="false" type="boolean"/>
<cfparam name="attributes.populationDetails" default="#structNew()#" type="struct"/>
<cfparam name="attributes.lockAddress" default="false" type="boolean"/>
<cfparam name="attributes.autoPopulateSettings" default="#structNew()#" type="struct"/>

<cf_head>
<cfoutput>
<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
	<script type='text/javascript' src='/mxajax/core/js/prototype.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxAjax.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxSelectdisabled.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxData.js'></script>
	<cfscript>
		qrynull=querynew("tempcol");
	</cfscript>

	<script language="javascript">
		// 2011/05/23 PPB LID5656 add wsdl on end to avoid conflict with code to get local timezone on first load
		var url = "#request.currentSite.httpProtocol##CGI.HTTP_HOST#/WebServices/mxAjaxCountries.cfc?wsdl";

	    function init() {
	     new mxAjax.Select({
	      parser: new mxAjax.CFArrayToJSKeyValueParser(),
	      executeOnLoad: true,
	      target: "frmShowLanguageID",
	      paramArgs: new mxAjax.Param(url,{param:"countryid={insCountryID}", cffunction:"getcountrylanguages"}),
	      source: "insCountryID"
	     });
	    }

		addOnLoadEvent(function() {init();});
	</script>
</cfif>
<script language="javascript">
	<!--- p-fnl069 SSS 2009/07/31 added this for captcha mx ajax calls--->
	function validateCaptcha() {

		<!--- PPB 2010/03/01 P-LENxxx pre sales: added this condition to avoid a "document.details.frmcaptcha.value is null" error; would be ideal not to call this from the FORM onSubmit call but the iif syntax is awkward --->
		if ("#attributes.showCaptcha#" == "false") {
			return true;
		} else {

			userResponse=document.details.frmcaptcha.value;
			captchaResponse=document.details.frmcaptcha_hash.value;

			var timeNow = new Date();
			page = domainAndRoot + '/WebServices/relaycaptchaWS.cfc?wsdl';
			captchaParameters  = 'returnFormat=JSON&method=getcaptchavalidation&time='+timeNow+ '&userResponse='+userResponse+'&captchaResponse='+captchaResponse;

			var myAjax = new Ajax.Request(
				page,
				{
						method: 'get',
						asynchronous: false,
						parameters: captchaParameters,
						evalJSON: 'force',
						debug : false
				}
			)
			var captchaResponse = myAjax.transport.responseText.evalJSON();

			if (captchaResponse) {
				return true;

			} else {

				var timeNow = new Date();
				page = domainAndRoot + '/webservices/relaycaptchaWS.cfc?wsdl';
				updatercaptchaParameters  = '&method=drawCaptcha&captchaerror=true&time='+timeNow;

				var div = 'captchadiv';
				var myAjax = new Ajax.Updater(
					div,
					page,
					{
						method: 'get',
						parameters: updatercaptchaParameters,
						evalJSON: 'force',
						debug : false,
						onComplete: function () {document.details.frmcaptcha.focus();}
					});

				return false;

			}
		}
	}
</script>
</cfoutput>
<cf_head>

<cfparam name = "attributes.hideDirectLineExtension" default="true" > <!--- pkp 22/04/2008 add switch to hide extension text box --->
<cfparam name = "attributes.initialiseuser" default = "false">
<cfparam name = "attributes.liveLanguage" default = "false"> <!--- ss will only show live langauges for people to pick from. --->
<cfparam name = "attributes.skipcompanycheck" default="false"> <!--- SSS 2008/04/10 --->
<cfparam name = "attributes.showMagicNumber" default="true" >

<cfparam name = "form.nextstep" default="#structKeyExists(attributes,'nextstep')?attributes.nextstep:1#">
<cfparam name="attributes.insorganisationTypeID" type="numeric" default="1">
<cfparam name = "attributes.dataCaptureMethod" default="2step">
<cfparam name = "attributes.restrictToUserOrgCountries" default="false">  <!--- WAB 2010/11/08 added a param to fix a bug, think probably set in a relaytag --->

<cfparam name = "ErrorMessageOrganisation" default="false">
<cfparam name = "phonemask" default="">

<cfparam name = "attributes.relayFormDisplayClass" default = "">

<cfset nextStep=form.nextStep>
<cfset addPerson = false>

<!--- MRE 2016/12/30 PROD2016-2620 Autocreatio of users --->
<cfif attributes.autoPopulate>
	<cfset autoPopulate(attributes.populationDetails)>
</cfif>

<!--- NJH 2016/01/06 JIRA PROD2015-343 - domain matching --->
<cfset domainMatchNoMatchesMessage = "">
<cfif attributes.domainMatch>
	<cfif structKeyExists(form,"insEmail") and structKeyExists(form,"addContactCountryButton")>
		<cfset domainMatchArgs = {emailAddress=trim(form.insEmail)}>
		<cfif attributes.restrictDomainMatchByCountry>
			<cfset domainMatchArgs.countryID = form.insCountryID>
		</cfif>
		<cfset domainMatchResult = application.com.relayPLO.getDomainMatch(argumentCollection=domainMatchArgs)>

		<!--- no matches --->
		<cfif not domainMatchResult.domainMatched>
			<!--- we have 'allowUnknownDomain' set to false but no matches - return user to country/language/email screen but with message. If allowUnknownDomain, then continue with person registration --->
			<cfif not attributes.allowUnknownDomain>
				<cfset domainMatchNoMatchesMessage = "phr_registration_domainDoesNotMatch">
			<cfelse>
				<cfset nextStep=6>
			</cfif>
		<cfelse>
			<cfset nextStep=6>
		</cfif>
	</cfif>

	<!--- this form variable will hold the location that the person has selected as their location. it will hold the locationId and the organisationID. --->
	<cfif structKeyExists(form,"insOrganisationLocationID") and listLen(form.insOrganisationLocationID,"|") eq 2>
		<cfset form.insOrganisationID = listLast(form.insOrganisationLocationID,"|")>
		<cfset form.insLocationID = listFirst(form.insOrganisationLocationID,"|")>
		<cfset addPerson = true>
	</cfif>
</cfif>

<cfif structKeyExists(URL,"oo")>
	<cfset form.oo = URL.oo>
</cfif>

<!--- NJH 2009/07/20 P-FNL069 captcha prototype --->
<cfif attributes.showCaptcha and structKeyExists(form,"frmCaptcha")>
	<cfset failedCaptcha = false>
	<cfif hash(ucase(form.frmCaptcha)) neq form.frmCaptcha_Hash>
	  	<cfset failedCaptcha = true>
	</cfif>
	<cfif failedCaptcha>
		 <cfoutput><span class="required">phr_sys_captchaError</span></cfoutput>
	</cfif>
</cfif>

<cfif structKeyExists(form,"AddContactOrganisationButton")>

	<cfquery name="CheckValidOrganisation">
		Select OrganisationID
		from organisation
		where (Organisation.OrganisationName = <cf_queryparam value="#insSiteName#" cfsqltype="cf_sql_varchar"> or
				Organisation.aka = <cf_queryparam value="#insSiteName#" cfsqltype="cf_sql_varchar">)
		and organisation.OrganisationID = <cf_queryparam value="#application.com.login.checkMagicNUmber(MagicNumber=frmMagicNumber,entityTypeID=2)#" cfsqltype="cf_sql_integer">
	</cfquery>

	<cfif CheckValidOrganisation.recordcount EQ 0>
		<cfset ErrorMessageOrganisation = true>
	<cfelse>
		<cfset nextstep = nextstep + 1>
	</cfif>
</cfif>

<!--- here we want to add a new location. Go to the 'Add location' form --->
<cfif structKeyExists(form,"addNewLocationButton")>
	<cfset nextstep = 4>
<!--- a new location has just been submitted --->
<cfelseif structKeyExists(form,"addContactlocationButton") or structKeyExists(form,"addLocationNewButton")>
	<cfset nextstep = 5>
<cfelseif isdefined("personDetails") and personDetails neq "">
	<cfset nextstep = 6>
	<cfset secondpass = true>
<cfelseif (isdefined("companyDetails") and companyDetails neq "") or (isdefined("continueToOrgQuestions") and continueToOrgQuestions neq "")>
	<cfset nextstep = 65>
	<cfset secondpass = true>
</cfif>

<!--- NJH 2009/07/27 P-FNL069 CAPTCHA - check if the captcha was on and whether it was successful --->
<cfif addPerson or (structKeyExists(form,"addContactSaveButton") and (not isDefined("failedCaptcha") or not failedCaptcha)) and
	not (nextstep eq 65 OR (isdefined("personDetails") and personDetails neq "") OR (isdefined("companyDetails") and companyDetails neq ""))>
	<cfparam name="frmDebug" type="boolean" default="no">

	<cfinclude template="/remoteAddTask.cfm">

	<!--- WAB 08/02/06 enables person to be added and immediately become the currentuser --->
	<cfif attributes.initialiseuser is true and not request.relaycurrentuser.isInternal>
		<cfset application.com.login.initialiseAnExternalUser(thisPersonID)>
	</cfif>

	<cfif structKeyExists(caller,"message")>
		<cfset caller.message = message>
	</cfif>

	<cfif structKeyExists(attributes,"debug")>
		<cfoutput>#message#</cfoutput>
	</cfif>

	<cfif structKeyExists(form,"portalMemberFlagTextID")>
		<cfquery name="GetOrgID" datasource="#application.sitedatasource#">
			Select OrganisationID from person where personID = <cf_queryparam value="#thisPersonID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<!--- add them to the members flag --->
		<cfset application.com.flag.setBooleanFlag(GetOrgID.OrganisationID,form.portalMemberFlagTextID)>
	</cfif>

	<!--- Save screen data --->
	<cfset url.newPersonID = request.relaycurrentuser.personID>
	<cfset url.newLocationID = request.relaycurrentuser.LocationID>
	<cfset url.newOrganisationID = request.relaycurrentuser.OrganisationID>

	<cf_updateData >

	<!--- If registering following an SSO, log user in as a Portal User to allow any workflows to take place --->
	<cfif attributes.autoPopulate>
		<cfset application.com.flag.setbooleanflag(
			entityid=thisPersonID, 
			flagtextid=attributes.autoPopulateSettings.approvalStatus,
			deleteOtherRadiosInGroup="true")>
		<cfset application.com.relayUserGroup.addPersonToUserGroup(
			personID=thisPersonID,
			userGroup=application.com.relayUserGroup.convertUsergroupNamesToUsergroupIDs("Portal User"))>
		<cfset filter= new singleSignOn.OAuth2.client.OAuth2AuthenticationFilterPlugin()>
		<cfif structKeyExists(attributes.autoPopulateSettings,"postMappings")>
			<cfset application.com.relayEntity.updatePersonDetails(thisPersonID, attributes.autoPopulateSettings.postMappings)>		
		</cfif>
		<cfset loginError=filter.logInUserFromPersonID(
			thisPersonID,
			attributes.autoPopulateSettings.authoriser,
			cookie.OAuth2State)>
		<cfif structKeyExists(loginError,"explanation")>
			<cfset loginError.currentUser=new com.relayCurrentUser()>
			<cfset application.com.errorHandler.recordRelayError_Warning(
            type="OAuth2AuthenticationError",
            Severity="error",
            WarningStructure=loginError)>
		</cfif>
	</cfif>

	<cfset showIntroText = "No">
<cfelse>

<!--- NJH 2007/02/02 --->
	<cfparam name="form.selectedCountry" type="boolean" default="0">
	<!--- 2007/02/22 GCC If they choose a country that is different to their show for countryID set it to be the same --->
	<!--- 2011/10/12 PPB LID7969 don't set showForCountryID if the user is logged in (in which case the user will be registering a deal for a 3rd party not registering themselves for the first time) --->
	<cfif form.selectedCountry eq 1 and structKeyExists(form,"insCountryID") and form.insCountryID neq request.relayCurrentUser.Content.showForCountryID and not request.relayCurrentUser.isLoggedIn>
		<cfset request.relayCurrentUser.Content.showForCountryID = form.insCountryID>
	</cfif>

	<!--- 2007/01/03 - GCC - if lang + country passed skip to form --->
	<cfif structkeyexists(url,"lang")>
		<cfif listlen(lang,"-") eq 2>

			<cfquery name="getCountry" datasource="#application.siteDataSource#">
				SELECT CountryID
				FROM Country
				where ISOCode = <cf_queryparam value="#listLast(lang,'-')#" cfsqltype="cf_sql_varchar">
			</cfquery>
			<cfif getCountry.recordcount eq 1>
				<cfset form.insCountryID = getCountry.CountryID>
			</cfif>

			<cfquery name="getLanguage" datasource="#application.siteDataSource#">
				SELECT LanguageID
				FROM         Language
				where ISOcode = <cf_queryparam value="#listLast(lang,'-')#" cfsqltype="cf_sql_varchar">
			</cfquery>

			<cfif getLanguage.recordcount eq 1>
				<cfset form.frmShowLanguageID = getLanguage.LanguageID>
			</cfif>

			<cfif nextstep EQ 1>
				<cfif getLanguage.recordcount eq 1 and getCountry.recordcount eq 1>
					<cfset nextstep = attributes.dataCaptureMethod eq "incremental"?2:6>
				</cfif>
			</cfif>
		<cfelse>

		</cfif>
	<cfelse>
		<!--- add to make sure the form comes up for the quickcard stuff --->
		<!--- NJH 2009/07/27 P-FNL069 CAPTCHA  - add check for form variables, as if the captcha is incorrectly entered, we want to redisplay the form --->
		<!--- SSS/AJC 2010/07/13 fixed this so that incremental contact form would work --->
		<cfif ((structKeyExists(Url,"FrmCountryID") or structKeyExists(form,"insCountryID")) and attributes.dataCaptureMethod eq "incremental") and not nextstep eq 65>
			<cfset nextstep = 6>
		</cfif>
	</cfif>

	<cfif structKeyExists(form,"insCountryID") and (nextstep eq 65 or nextstep eq 6 or nextstep eq 4 or nextstep eq 5 ) >
		<!--- get phone mask --->
		<cfset phoneMask = application.com.commonQueries.getCountry(countryid=form.insCountryID).telephoneFormat>
 	</cfif>

	<cfif nextstep eq 6>
		<cfset attributes.personDataOnly = 1>
	</cfif>

	<cfif nextstep eq 65>
		<cfset attributes.personDataOnly = 0>
		<cfset attributes.OrganisationDataOnly = 1>
	</cfif>

<!--- NJH 2007/02/02 --->
		<cfscript>
		// this query controls the behanvious of each form element
		//NJH 2009/07/08 P-FNL069 - added regexPattern to validate firstname and lastname to accept only alphabetic UTF characters plus space
			qRelayFieldList = QueryNew("fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,readOnly");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insSalutation");
				temp = QuerySetCell(qRelayFieldList, "type", "validValueList");
				temp = QuerySetCell(qRelayFieldList, "params", "validFieldName=person.salutation");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_Salutation");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insFirstName");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_FirstName");
				temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insLastName");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_LastName");
				temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insJobDesc");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_ext_jobDesc");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insEmail");
				temp = QuerySetCell(qRelayFieldList, "type", "email");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_EmailAddress");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insOfficePhone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DirectLine");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insMobilePhone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_MobilePhone");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insLocEmail");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_LocEmail");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);  // MRE 2016/12/30 PROD2016-2620 Address lockable when registering from SSO
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "inslocationID");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=80");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insSiteName");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=255");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insaka");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_aka");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insVATnumber");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_VATnumber");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress1");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Address");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress2");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress3");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress4");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_TownCity");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress5");
				temp = QuerySetCell(qRelayFieldList, "type", "Province");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_CountyRegionState");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress6");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_CountyRegionState");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress7");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBox");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress8");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBoxPostcode");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress9");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBoxTown");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress10");
				temp = QuerySetCell(qRelayFieldList, "type", "Region");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_GreaterRegion");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insPostalCode");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=10,maxfieldsize=20");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_PostalCodeZip");
				temp = QuerySetCell(qRelayFieldList, "readOnly", attributes.lockAddress);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insCountryID");
				temp = QuerySetCell(qRelayFieldList, "type", "countryList");
				temp = QuerySetCell(qRelayFieldList, "params", "");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Country");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insTelephone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Switchboard");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insFax");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_FaxInclCountryCode");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insOrgURL");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_WebSiteAddress");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insUsername");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Username");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insSex");
				temp = QuerySetCell(qRelayFieldList, "type", "gender");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Gender");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insDOB");
				temp = QuerySetCell(qRelayFieldList, "type", "dateOfBirth");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DateOfBirth");
		</cfscript>


	<cfif nextstep eq 6>
		<cfset fieldsToShow = "insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone">
	<cfelseif nextstep eq 65>
		<cfset fieldsToShow = "insSiteName,insCountryID,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insAddress6,insAddress7,insAddress8,insAddress9,insAddress10,insPostalCode,insTelephone,insFax,insOrgURL,insLocEmail">
	<cfelse>
		<cfset fieldsToShow = "insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insLocEmail,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insAddress6,insAddress7,insAddress8,insAddress9,insAddress10,insPostalCode,insCountryID,insTelephone,insFax,insOrgURL,insUsername,insSex,insDOB">
		<cfif isdefined('insorganisationID') and val(insorganisationID) neq 0>
			<cfset fieldsToShow = listAppend(fieldsToShow,"inslocationID")>
		</cfif>
	</cfif>

	<cfquery name="qRelayFieldList" dbType="query">
		select * from qRelayFieldList where fieldname in (<cf_queryparam value="#fieldsToShow#" cfsqltype="cf_sql_varchar" list="true" isQoQ="true">)
	</cfquery>

	<cfset emailAttributes = {}>
	<cfif application.com.relayPLO.isUniqueEmailValidationOn().isOn>
		<cfset emailAttributes = {personid = 0, orgTypeID=attributes.insorganisationTypeID,uniqueEmail=true,uniqueEmailMessage="phr_sys_formValidation_UniqueEmailRequired"}>
	</cfif>

	<!--- This block below will by-pass the organisation check unless a magic number is incorrect SSS 10/04/2008--->
	<cfset magicNumberCompanyName = "">
	<cfset magicNumber = "">

	<cfif structKeyExists(form,"oo") and nextstep EQ 2>
		<cfset magicNumberOrganisationID = listfirst( form.oo , "-")>
		<cfset magicNumber = form.oo>

		<cfquery name="GetExistingOrganisation" datasource="#application.sitedatasource#">
			Select o.OrganisationName as orgname, o.organisationID
			from Organisation o
			where o.OrganisationID = <cf_queryparam value="#magicNumberOrganisationID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfset magicNumberCompanyName = GetExistingOrganisation.orgname>

		<cfif attributes.skipCompanyCheck>
			<cfif GetExistingOrganisation.recordcount>
				<cfquery name="CheckValidOrganisation" datasource="#application.sitedatasource#">
					Select OrganisationID
					from organisation
					where (Organisation.OrganisationName = <cf_queryparam value="#GetExistingOrganisation.orgname#" cfsqltype="cf_sql_varchar"> or
							Organisation.aka = <cf_queryparam value="#GetExistingOrganisation.orgname#" cfsqltype="cf_sql_varchar">)
					and organisation.OrganisationID = <cf_queryparam value="#GetExistingOrganisation.organisationID#" cfsqltype="cf_sql_integer">
				</cfquery>

				<cfset form.insSiteName = GetExistingOrganisation.orgname>
				<cfset form.frmMagicNumber = form.oo>
				<cfset form.frmshowlanguageID = frmshowlanguageID>
				<cfset form.inscountryID = inscountryID>
				<cfset form.SelectedCountry = SelectedCountry>
				<cfset nextstep = 3>
			<cfelse>
				<cfoutput>#application.com.relayUI.message(message="phr_addcontactform_URLErrorMeassage",messageType="error")#</cfoutput>
				<cfset nextstep = 0>
			</cfif>
		</cfif>
	</cfif>

	<!--- 09/05/2006 AJC P_COR001 CountryID now has to be selected first --->
	<cfif (form.selectedCountry is 0 and not (structKeyExists(form,"insCountryID") and structKeyExists(form,"frmShowLanguageID"))) or (attributes.domainMatch and domainMatchNoMatchesMessage neq "")>
		<cfif not structKeyExists(attributes,"CountryGroup") or attributes.CountryGroup is false>
			<cfquery name="getCountries" datasource="#application.siteDataSource#">
				SELECT DISTINCT CountryID,
				case when ltrim(isnull(localcountrydescription, '')) = '' then CountryDescription else localcountrydescription end as CountryDescription
				FROM Country
					where isNull(isocode,'') <> ''
					<cfif attributes.restrictToUserOrgCountries>
						AND CountryID IN (SELECT DISTINCT Location.CountryID FROM Location INNER JOIN Person ON Location.OrganisationID = Person.OrganisationID WHERE (Person.PersonID = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">))
					</cfif>
				ORDER BY CountryDescription ASC
			</cfquery>
		<cfelse>
			 <cfif IsNumeric(attributes.CountryGroup)>
				<cfset CountryGroupID = attributes.CountryGroup>
			<cfelse>
				<cfquery name="getCountryGroup" datasource="#application.siteDataSource#">
					select C1.countryID
					from Country c1
					where countrydescription = <cf_queryparam value="#attributes.CountryGroup#" cfsqltype="cf_sql_varchar">
				</cfquery>

				<cfset CountryGroupID = getCountryGroup.CountryID>
			</cfif>
			<cfquery name="getCountries" datasource="#application.siteDataSource#">
				SELECT  c1.CountryID,
						case when ltrim(isnull(c1.localcountrydescription, '')) = '' then c1.CountryDescription else c1.localcountrydescription end as CountryDescription
					FROM Country c1 INNER JOIN CountryGroup cg1 ON c1.CountryID = cg1.CountryMemberID
					WHERE (cg1.CountryGroupID = <cf_queryparam value="#CountryGroupID#" cfsqltype="cf_sql_integer">)
					AND isNull(isocode,'') <> ''
					order by c1.countryDescription
			</cfquery>
		</cfif>

		<cfif not (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
			<cfquery name="getLanguages" datasource="#application.siteDataSource#">
				SELECT DISTINCT [LanguageID],
				case when ltrim(isnull(localLanguageName, '')) = '' then language else localLanguageName end as languageDescription
				FROM         Language
				<cfif attributes.liveLanguage>
					where languageID in (<cf_queryparam value="#request.currentSite.liveLanguageIDs#" cfsqltype="cf_sql_integer" list="true">)
				</cfif>
				ORDER BY languageDescription ASC
			</cfquery>
		</cfif>


		<cfoutput>
			<form method="POST" name="details" novalidate="true">
		</cfoutput>
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#attributes.defaultCountryID#" fieldName="insCountryID" query="#getCountries#" display="CountryDescription" value="countryid" label="Phr_Ext_Country" required="Yes" nulltext="Phr_ChooseACountry" nullValue="" readOnly="#attributes.lockAddress?true:false#">
				<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
					<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmShowLanguageID" size="1" query="#qrynull#" display="tempcol" value="tempcol" label="Phr_Ext_Language" required="Yes">
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#request.relaycurrentuser.languageID#" fieldName="frmShowLanguageID" size="1" query="#getLanguages#" display="languageDescription" value="languageID" label="Phr_Ext_Language" required="Yes">
				</cfif>

				<!--- this will show a message if set. --->
				<cfoutput>#application.com.relayUI.message(message=domainMatchNoMatchesMessage,messageType="info")#</cfoutput>

				<cfif attributes.domainMatch>
					<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="insEmail" label="Phr_Ext_EmailAddress" validate="email" onValidate="validateEmail" required="yes" attributeCollection=#emailAttributes#>
				</cfif>

				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactCountryButton" currentValue="phr_continue" label="" class="button">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="1" fieldname="selectedCountry">
				<cfif structKeyExists(form,"oo")>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.oo#" fieldname="oo">
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.dataCaptureMethod eq 'incremental'?2:6#" fieldname="nextstep">
			</cf_relayFormDisplay>
		<cfoutput>
			</form>
		</cfoutput>


	<!--- as far as I can tell, we only go in here with magic numbers....the partner enters the company name to validate it... If successful, we then go to step 3 --->
	<cfelseif nextstep EQ 2>

		<cfoutput>
			<form method="POST" name="details" novalidate="true">
		</cfoutput>
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfif ErrorMessageOrganisation>
					<cfoutput>#application.com.relayUI.message(message="phr_addcontactform_organisationdoesnotexists",messageType="info")#</cfoutput>
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insSiteName" currentValue="#magicNumberCompanyName#" label="Phr_Ext_OrgCompanyName" size="50" maxlength="100" required="yes">
				<CF_relayFormElementDisplay relayFormElementType="#attributes.showMagicNumber?'text':'hidden'#" fieldName="frmMagicNumber" currentValue="#magicNumber#" label="phr_Ext_EncodedNumber" size="50" maxlength="100" required="yes">

				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactOrganisationButton" currentValue="phr_continue" label="" class="button">
			</cf_relayFormDisplay>
		<cfoutput>
		</form>
		</cfoutput>

	<!--- magic number supplied and it has been validated.... get partner to choose their location....  --->
	<cfelseif nextstep EQ 3>
		<!--- select a location --->
		<cfquery name="getLocation" datasource="#application.siteDataSource#">
			select LocationID value, '' as display, * from location
				where location.organisationID = <cf_queryparam value="#CheckValidOrganisation.organisationID#" cfsqltype="cf_sql_integer">
			order by sitename
	 	</cfquery>

		<cfloop query="getLocation">
			<!--- get a formatted address and pop it into the query, ready to be used by the radio button displayer --->
			<cfset formattedAddress = application.com.screens.evaluateAddressFormat(location= getLocation,row=currentrow,separator="<BR>",finalcharacter="<BR>&nbsp;",showcountry=true)>
			<cfset querySetCell(getLocation,"display", formattedAddress, currentrow)>
		</cfloop>

		<cfif getLocation.recordcount EQ 0>
			<cfoutput>#application.com.relayUI.message(message="phr_addcontactform_noLocationFound",messageType="info")#</cfoutput>
		<cfelse>

			<cfoutput>
				<form method="POST" name="details" novalidate="true">
			</cfoutput>
				<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
					<!--- 2009/10/29	NAS	LID - 2782 - Code updated (label) to Translate 'Location' --->
					<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" message="phr_LocationRequired" fieldName="inslocationID" label="phr_location" query = "#getlocation#" display="display" value="value" required="yes" noteText="Please select a location">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CheckValidOrganisation.organisationID#" fieldname="insOrganisationID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
					<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactlocationButton" currentValue="phr_continue" label="" class="button">
				</cf_relayFormDisplay>
			<cfoutput>
			</form>
			</cfoutput>
		</cfif>

		<cfoutput>
		<form method="POST" name="addNewlocationButton" novalidate="true">
		</cfoutput>
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CheckValidOrganisation.organisationID#" fieldname="insOrganisationID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addNewlocationButton" currentValue="phr_Addnewlocation" label="" class="button">
			</cf_relayFormDisplay>
		<cfoutput>
		</form>
		</cfoutput>

	<cfelseif nextstep EQ 4>
		<!--- Add a new location incremental--->
		<cfparam name = "attributes.ShowLocationCols" default = "insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax">

		<cfoutput>
		<form method="POST" name="details" novalidate="true">
		</cfoutput>
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
					<cfset outputRegistrationFormFields(qRelayFieldList=qRelayFieldList)>
				</cfif>

			<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">

			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insOrganisationID#" fieldname="insOrganisationID">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">

			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addlocationnewButton" currentValue="phr_continue" label="" class="button">
		</cf_relayFormDisplay>
		<cfoutput>
		</form>
		</cfoutput>

	<cfelseif nextstep EQ 5>
			<!--- add new person incremental--->
			<cfparam name = "attributes.showPersonCols" default = "insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone,insOfficePhoneExt,insSex,insDOB">

		<cfoutput>
			<form method="POST" name="details" novalidate="true">
		</cfoutput>
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
					<cfset outputRegistrationFormFields(qRelayFieldList=qRelayFieldList)>
				</cfif>

		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">

				<cfif isdefined("inslocationID")>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inslocationID#" fieldname="inslocationID">
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS1#" fieldname="INSADDRESS1">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS2#" fieldname="INSADDRESS2">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS3#" fieldname="INSADDRESS3">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS4#" fieldname="INSADDRESS4">
					<!--- 2007/11/23 GCC Not always used in address capture --->
					<cfif isdefined("INSADDRESS5")>
						<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS5#" fieldname="INSADDRESS5">
					</cfif>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSPOSTALCODE#" fieldname="INSPOSTALCODE">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSTELEPHONE#" fieldname="INSTELEPHONE">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSFAX#" fieldname="INSFAX">
				</cfif>
				<cfif structKeyExists(attributes,"showIntroText")>
		<!--- CONTROL IntroText in relayTags/addCOntactForm --->
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.showIntroText#" fieldname="showIntroText">
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#" fieldname="frmNext">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmDebug#" fieldname="frmDebug">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmReturnMessage#" fieldname="frmReturnMessage">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.flagStructure#" fieldname="flagStructure">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmShowLanguageID#" fieldname="insLanguage">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insOrganisationID#" fieldname="insOrganisationID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">

				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactSaveButton" currentValue="phr_continue" label="" class="button">
		</cf_relayFormDisplay>
		<cfoutput>
		</form>
		</cfoutput>

	<cfelseif nextstep EQ 6>
		<cfset form.personDetails="">
		<cfset form.companyDetails="">
		<cfset form.continueToOrgQuestions="">

		<cfset submitfunction = attributes.showcaptcha?"return validateCaptcha();":"">

		<cfform method="POST" name="details" onsubmit="#submitfunction#" novalidate="true">

		<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">

		<cfif isdefined("secondpass") and not structKeyExists(form,"hidesecondpass")>
			<cfset form.hidesecondpass=true>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="true" fieldname="hidesecondpass">
		</cfif>

		<!--- if we're domain matching and we've got a result, we don't want to show these 'buttons'.... essentially, if person matched, they skip the company details.. if they didn't, then go through normal process  --->
		<cfif not (attributes.domainMatch and isDefined("domainMatchResult") and domainMatchResult.domainMatched)>
			<cfoutput>
			<div id="requestLoginProgress">
				<CF_relayFormElement relayFormElementType="submit" fieldname="personDetails" currentValue="phr_personDetails" label="" class="selected button" disabled="true">
				<CF_relayFormElement relayFormElementType="submit" fieldname="companyDetails" currentValue="phr_companyDetails" label="" class="button">
			</div>
			</cfoutput>
		</cfif>

		<cfif not structKeyExists(form,"hidesecondpass")>
			<CF_relayFormElementDisplay fieldname="test" relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#">
			<cfif structKeyExists(attributes,"showIntroText")>
			<!--- CONTROL IntroText in relayTags/addCOntactForm --->
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.showIntroText#" fieldname="showIntroText">
			</cfif>
			<cfif not structKeyExists(form,"insLanguage")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmShowLanguageID#" fieldname="insLanguage">
			</cfif>
			<cfif not structKeyExists(form,"insCountryID")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
			</cfif>
			<cfif not structKeyExists(form,"selectedCountry")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="selectedCountry">
			</cfif>
			<cfif isdefined("personAddedSetForm") and personAddedSetForm eq "true">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="true" fieldname="personAdded">
			</cfif>
		</cfif>

		<cfif (structKeyExists(attributes,"organisationScreenID") and attributes.organisationScreenID) neq "">
			<cfif structKeyExists(form,"frmorganisationflaglist")>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmorganisationflaglist#" fieldname="frmorganisationflaglist">
			</cfif>
			<cfif structKeyExists(form,"frmlocationflaglist")>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmlocationflaglist#" fieldname="frmlocationflaglist">
			</cfif>
		</cfif>

		<!--- get the list of locations when doing domain matching --->
		<cfif attributes.domainMatch and isDefined("domainMatchResult") and domainMatchResult.domainMatched>
			<cfquery name="selectLocations">
				select cast(locationID as varchar)+'|'+cast(organisationId as varchar) as value,
					sitename + ' ' + isNull(address1,'') + ', ' + isNull(address2,'') + ' ' + isNull(address4,'') + ', ' + isNull(address5,'') + ' ' + c.countryDescription as display, 0 as sortIdx, sitename
				from location l
					inner join country c on c.countryID = l.countryId
					where locationID in (<cf_queryparam value="#domainMatchResult.locationIDList#" cfsqltype="cf_sql_integer" list="true">)
				union
				select '0' as value,'phr_registration_locationNotListed' as display, 1 as sortIdx,'' as sitename
				order by sortIdx,sitename
			</cfquery>

			<cf_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="insOrganisationLocationID" label="Phr_Ext_OrgCompanyName" query = "#selectLocations#" display="display" value="value" required="true" nullText="#attributes.allowNewAddress?'phr_registration_selectCompany':''#" nullvalue="">
		</cfif>

		<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
			<cfset outputRegistrationFormFields(qRelayFieldList=qRelayFieldList)>
		</cfif>
		<cfif structKeyExists(attributes,"personScreenID") and attributes.personScreenID neq "">
			<CF_aSCREEN formName = "details">
	        	<CF_aScreenItem
	              	screenid=#attributes.personScreenID#
					personid = "newPersonID"
					organisationid = "newOrganisationID"
					locationid = "newLocationID"
					countryID="#request.relaycurrentuser.content.showforcountryID#"
					method="edit"
	                >
			</CF_aSCREEN>
		</cfif>

		<!--- NJH 2009/07/20 P-FNL069 captcha prototyping --->
		<cfif attributes.showCaptcha>
			<cfoutput>
		<tr>
			<td colspan="2">Phr_Ext_Captcha_instructions</td>
		</tr>
		<tr>
			<td valign="bottom">&nbsp;</td>
			<td>
			<div id="captchadiv">
				<cfset captchaHTML = application.com.commonQueries.drawCaptcha()>
				#captchaHTML#
			</div>
			</td>
		</tr>
		</cfoutput>
 		</cfif>

		<cfif not structKeyExists(form,"hidesecondpass")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.insorganisationTypeID#" fieldname="insorganisationTypeID">
		</cfif>
		<cfloop collection="#form#" item="VarName">
			<cfif not ListFindNoCase("nextStep,personDetails,companyDetails,addContactSaveButton,frmpersonflaglist,frmorganisationflaglist,frmlocationflaglist", #VarName#)>
				<cfset print = 1>
				<cfset script = "">
					<cfloop query="qRelayFieldList">
						<cfif UCase(qRelayFieldList.fieldname) eq UCase(left(VarName,len(qRelayFieldList.fieldname)))>
							<cfset print = 0>
						<cfelseif isdefined("frmpersonflaglist")>
							<cfloop list="#frmpersonflaglist#" index="flagfieldname">
								<cfif UCase(flagfieldname) eq UCase(left(VarName,len(flagfieldname)))>
									<cfset print = 0>
									<cfset script = flagfieldname>
								</cfif>
							</cfloop>
						</cfif>
					</cfloop>
				<cfif print eq 1>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#Form[VarName]#" fieldname="#VarName#">
				</cfif>
				<cfif (print eq 0) and (script neq "")>
					<cfif Form[VarName] neq '' and listfindnocase("date,text,integer",left(script,find("_",script)-1))>
						<cfoutput>
						<script>
							{
								if(document.getElementById("#script#_newPersonID").attributes.getNamedItem("type").value == "text"
									|| document.getElementById("#script#_newPersonID").attributes.getNamedItem("type").value == "date"
									|| document.getElementById("#script#_newPersonID").attributes.getNamedItem("type").value == "integer"){
										document.getElementById("#script#_newPersonID").setAttribute("value","#Form[VarName]#");
								}
							}
						</script>
						</cfoutput>
					</cfif>
					<cfif Form[VarName] neq '' and listfindnocase("radio,checkbox",left(script,find("_",script)-1)) and Form[VarName] gt 0>
						<cfoutput>
						<script>
							{
								if(#Form[VarName]# != "0")	{
									for (i=0;i<=document.getElementsByName("#script#_newPersonID").length;i++){
										if((document.getElementsByName("#script#_newPersonID")[i].getAttribute("type") == "radio") &&
										document.getElementsByName("#script#_newPersonID")[i].getAttribute("value") == "#Form[VarName]#") {
											document.getElementsByName("#script#_newPersonID")[i].setAttribute("checked","true");
										} else if(document.getElementsByName("#script#_newPersonID")[i].getAttribute("type") == "checkbox") {
											values = "#Form[VarName]#";
											values = values.split(",");
											for (h=0;h<=values.length;h++){
												if(document.getElementsByName("#script#_newPersonID")[i].getAttribute("value") == values[h]){
													document.getElementsByName("#script#_newPersonID")[i].setAttribute("checked","true");
												}
											}
										} else if(document.getElementsByName("#script#_newPersonID")[i].tagName == "SELECT"){
											for (j=0;j<=document.getElementsByName("#script#_newPersonID")[i].options.length;j++){
												if(document.getElementsByName("#script#_newPersonID")[i].options[j].getAttribute("value") == "#Form[VarName]#"){
													document.getElementsByName("#script#_newPersonID")[i].options[j].setAttribute("selected","true");
												}
											}
										}
									}
								}else{
									for (j=0;j<=document.getElementsByName("#script#_newPersonID").length;j++){
										if(document.getElementsByName("#script#_newPersonID")[j].getAttribute("type") == "radio"){
											if(document.getElementsByName("#script#_newPersonID")[j].getAttribute("value")=="0"){
												document.getElementsByName("#script#_newPersonID")[j].removeAttribute("checked");
											}
										}
									}
								}


							}
						</script>
						</cfoutput>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="continueToOrgQuestions" currentValue="phr_nextstep" label="" class="button">
		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="65" fieldname="nextstep">

		</cf_relayFormDisplay>
		</cfform>

		<!--- 2011/01/11 GCC - dodgy workaround for issue with CF that a required select cannot be focussed to after applying masks
		hard coded attempt to focus to insSalutation at the end - if insSalutation is not in the form it will focus on the first field (if it is not another select of course!)
		 --->

		<cfoutput>
		<script  type="text/javascript">
		if( document.getElementById && document.getElementById('insSalutation'))
		{
		    // set focus to first field in the form
		    document.getElementById('insSalutation').focus();

		}
		if( document.all && document.all['insSalutation'])
		{
		    // set focus to first field in the form
		   document.all['insSalutation'].focus();
		}
		</script>
		</cfoutput>


	<!---Add company details --->
	<cfelseif nextstep EQ 65>
		<cfset form.personDetails="">
		<cfset form.companyDetails="">
		<cfset form.continueToOrgQuestions="">

		<cfform method="POST" name="details" novalidate="true">

		<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">

		<cfif isdefined("secondpass") and not structKeyExists(form,"hidesecondpass")>
			<cfset form.hidesecondpass=true>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="true" fieldname="hidesecondpass">
		</cfif>

		<cfoutput>
		<div id="requestLoginProgress">
			<CF_relayFormElement relayFormElementType="submit" fieldname="personDetails" currentValue="phr_personDetails" label="" class="button">
			<CF_relayFormElement disabled="true" relayFormElementType="submit" fieldname="companyDetails" currentValue="phr_companyDetails" label="" class="selected button">
		</div>
		</cfoutput>

		<cfif not structKeyExists(form,"hidesecondpass")>
			<CF_relayFormElementDisplay fieldname="test" relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#">
			<cfif structKeyExists(attributes,"showIntroText")>
			<!--- CONTROL IntroText in relayTags/addCOntactForm --->
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.showIntroText#" fieldname="showIntroText">
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#" fieldname="frmNext">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmDebug#" fieldname="frmDebug">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmReturnMessage#" fieldname="frmReturnMessage">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.flagStructure#" fieldname="flagStructure">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.insorganisationTypeID#" fieldname="insorganisationTypeID">
			<cfif not structKeyExists(form,"insLanguage")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmShowLanguageID#" fieldname="insLanguage">
			</cfif>
			<cfif not structKeyExists(form,"insCountryID")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
			</cfif>
			<cfif not structKeyExists(form,"selectedCountry")>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="selectedCountry">
			</cfif>
			<cfif isdefined("personAddedSetForm") and personAddedSetForm eq "true">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="true" fieldname="personAdded">
			</cfif>
		</cfif>

		<cfif (structKeyExists(attributes,"personScreenID") and attributes.personScreenID) neq "" AND (structKeyExists(form,"frmpersonflaglist"))>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmpersonflaglist#" fieldname="frmpersonflaglist">
		</cfif>

		<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
			<cfset outputRegistrationFormFields(qRelayFieldList=qRelayFieldList)>
		</cfif>

		<cfif structKeyExists(attributes,"organisationScreenID") and attributes.organisationScreenID neq "">
			<CF_aSCREEN formName = "details">
	        	<CF_aScreenItem
	              	screenid=#attributes.organisationScreenID#
					personid = "newPersonID"
					organisationid = "newOrganisationID"
					locationid = "newLocationID"
					countryID="#request.relaycurrentuser.content.showforcountryID#"
					method="edit"
	                >
			</CF_aSCREEN>
		</cfif>

		<cfloop collection="#form#" item="VarName">
			<cfif not ListFindNoCase("nextStep,personDetails,companyDetails,addContactSaveButton,frmpersonflaglist,frmorganisationflaglist,FRMLOCATIONFLAGLIST", VarName)>
				<cfset print = 1>
				<cfset script = "">
					<cfloop query="qRelayFieldList">
						<cfif UCase(qRelayFieldList.fieldname) eq UCase(left(VarName,len(qRelayFieldList.fieldname)))>
							<cfset print = 0>
						<cfelseif isdefined("frmorganisationflaglist")>
							<cfloop list="#frmorganisationflaglist#" index="flagfieldname">
								<cfif UCase(flagfieldname & "_newOrganisationID") eq UCase(VarName)>
									<cfset print = 0>
									<cfset script =flagfieldname>
								</cfif>
							</cfloop>
						</cfif>
					</cfloop>
					<cfloop query="qRelayFieldList">
						<cfif UCase(qRelayFieldList.fieldname) eq UCase(left(VarName,len(qRelayFieldList.fieldname)))>
							<cfset print = 0>
						<cfelseif isdefined("FRMLOCATIONFLAGLIST")>
							<cfloop list="#FRMLOCATIONFLAGLIST#" index="flagfieldname">
								<cfif UCase(flagfieldname & "_NEWLOCATIONID") eq UCase(VarName)>
									<cfset print = 0>
									<cfset script = flagfieldname>
								</cfif>
							</cfloop>
						</cfif>
					</cfloop>
				<cfif print eq 1>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#Form[VarName]#" fieldname="#VarName#">
				</cfif>
				<cfif (print eq 0) and (script neq "")>
					<cfif Form[VarName] neq '' and listfindnocase("date,text,integer",left(script,find("_",script)-1))>
						<cfoutput>
						<script>
							{
								if(document.getElementById("#script#_newLocationID").attributes.getNamedItem("type").value == "text"
									|| document.getElementById("#script#_newLocationID").attributes.getNamedItem("type").value == "date"
									|| document.getElementById("#script#_newLocationID").attributes.getNamedItem("type").value == "integer"){
										document.getElementById("#script#_newLocationID").setAttribute("value","#Form[VarName]#");
								}
								if(document.getElementById("#script#_newOrganisationID").attributes.getNamedItem("type").value == "text"
									|| document.getElementById("#script#_newOrganisationID").attributes.getNamedItem("type").value == "date"
									|| document.getElementById("#script#_newOrganisationID").attributes.getNamedItem("type").value == "integer"){
										document.getElementById("#script#_newOrganisationID").setAttribute("value","#Form[VarName]#");
								}
							}
						</script>
						</cfoutput>
					</cfif>
					<cfif Form[VarName] neq '' and listfindnocase("radio,checkbox",trim(left(script,find("_",script)-1)))>
						<cfoutput>
						<script>
							{
								if(#Form[VarName]# != "0")	{
									for (i=0;i<=document.getElementsByName("#script#_newLocationID").length;i++){
										if((document.getElementsByName("#script#_newLocationID")[i].getAttribute("type") == "radio") &&
										document.getElementsByName("#script#_newLocationID")[i].getAttribute("value") == "#Form[VarName]#") {
											document.getElementsByName("#script#_newLocationID")[i].setAttribute("checked","true");
										} else if(document.getElementsByName("#script#_newLocationID")[i].getAttribute("type") == "checkbox") {
											values = "#Form[VarName]#";
											values = values.split(",");
											for (h=0;h<=values.length;h++){
												if(document.getElementsByName("#script#_newLocationID")[i].getAttribute("value") == values[h]){
													document.getElementsByName("#script#_newLocationID")[i].setAttribute("checked","true");
												}
											}
										} else if(document.getElementsByName("#script#_newLocationID")[i].tagName == "SELECT"){
											for (j=0;j<=document.getElementsByName("#script#_newLocationID")[i].options.length;j++){
												if(document.getElementsByName("#script#_newLocationID")[i].options[j].getAttribute("value") == "#Form[VarName]#"){
													document.getElementsByName("#script#_newLocationID")[i].options[j].setAttribute("selected","true");
												}
											}
										}
									}
								}else{
									for (j=0;j<=document.getElementsByName("#script#_newLocationID").length;j++){
										if(document.getElementsByName("#script#_newLocationID")[j].getAttribute("type") == "radio"){
											if(document.getElementsByName("#script#_newLocationID")[j].getAttribute("value")=="0"){
												document.getElementsByName("#script#_newLocationID")[j].removeAttribute("checked");
											}
										}
									}
								}
								if(#Form[VarName]# != "0")	{
									for (i=0;i<=document.getElementsByName("#script#_newOrganisationID").length;i++){
										if((document.getElementsByName("#script#_newOrganisationID")[i].getAttribute("type") == "radio") &&
										document.getElementsByName("#script#_newOrganisationID")[i].getAttribute("value") == "#Form[VarName]#") {
											document.getElementsByName("#script#_newOrganisationID")[i].setAttribute("checked","true");
										} else if(document.getElementsByName("#script#_newOrganisationID")[i].getAttribute("type") == "checkbox") {
											values = "#Form[VarName]#";
											values = values.split(",");
											for (h=0;h<=values.length;h++){
												if(document.getElementsByName("#script#_newOrganisationID")[i].getAttribute("value") == values[h]){
													document.getElementsByName("#script#_newOrganisationID")[i].setAttribute("checked","true");
												}
											}
										} else if(document.getElementsByName("#script#_newOrganisationID")[i].tagName == "SELECT"){
											for (j=0;j<=document.getElementsByName("#script#_newOrganisationID")[i].options.length;j++){
												if(document.getElementsByName("#script#_newOrganisationID")[i].options[j].getAttribute("value") == "#Form[VarName]#"){
													document.getElementsByName("#script#_newOrganisationID")[i].options[j].setAttribute("selected","true");
												}
											}
										}
									}
								}else{
									for (j=0;j<=document.getElementsByName("#script#_newOrganisationID").length;j++){
										if(document.getElementsByName("#script#_newOrganisationID")[j].getAttribute("type") == "radio"){
											if(document.getElementsByName("#script#_newOrganisationID")[j].getAttribute("value")=="0"){
												document.getElementsByName("#script#_newOrganisationID")[j].removeAttribute("checked");
											}
										}
									}
								}


							}
						</script>
						</cfoutput>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactSaveButton" currentValue="phr_nextstep" label="" class="button">
		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">

		</cf_relayFormDisplay>
		</cfform>

		<!--- 2011/01/11 GCC - dodgy workaround for issue with CF that a required select cannot be focussed to after applying masks
		hard coded attempt to focus to insSalutation at the end - if insSalutation is not in the form it will focus on the first field (if it is not another select of course!)
		 --->

		<cfoutput>
		<script  type="text/javascript">
		if( document.getElementById && document.getElementById('insSalutation'))
		{
		    // set focus to first field in the form
		    document.getElementById('insSalutation').focus();

		}
		if( document.all && document.all['insSalutation'])
		{
		    // set focus to first field in the form
		   document.all['insSalutation'].focus();
		}
		</script>

		<script  type="text/javascript">
		//script to re-set the checkbox's and radios

		if( document.getElementById && document.getElementById('insSalutation'))
		{
		    // set focus to first field in the form
		    document.getElementById('insSalutation').focus();

		}
		if( document.all && document.all['insSalutation'])
		{
		    // set focus to first field in the form
		   document.all['insSalutation'].focus();
		}
		</script>
		</cfoutput>
	</cfif>
</cfif>



<cffunction name="outputRegistrationFormFields" access="public" output="true">
	<cfargument name="qRelayFieldList" type="query" required="true" hint="Query of fieldnames to output">

	<cfparam name="attributes.hideCompanyName" default="No" type="boolean">
	<cfparam name="form.insCountryID" default="#attributes.defaultCountryID#">
	<cfset countryDetails = application.com.commonQueries.getCountry(countryid=form.insCountryID)>

	<!--- MRE PROD2016-2620 Fixed bug found in testing where boolean field is null--->
	<cfif countryDetails.VATrequired>
		<cfset attributes.mandatoryCols = listAppend(attributes.mandatoryCols,"insVATnumber")>
	</cfif>

	<cfloop query="qRelayFieldList">
				<cfset foundInForm = "0">
				<cfset fieldValue = "">
				<cfloop collection="#form#" item="VarName">
					<cfif UCase(qRelayFieldList.fieldname) eq UCase(VarName)>
						<cfset fieldValue = Form[VarName]>
						<cfset foundInForm = "1">
					</cfif>
				</cfloop>

				<!--- get any name value pairs defined in params and make them available to this scope --->
				<CF_EvaluateNameValuePair NameValuePairs = "#qRelayFieldList.params#">

				<cfif listfindNocase(attributes.showCols,qRelayFieldList.fieldName) neq 0>
					<cfset isMandatory = listfindNocase(attributes.mandatoryCols,qRelayFieldList.fieldName)?true:false>

					<cfif not (fieldName eq "insSiteName" and attributes.hideCompanyName eq "yes")>

						<cfif left(defaultFieldLabel,3) is "phr">
							<cfset thisfieldlabel = defaultFieldLabel>
						<!--- NJH 2010/10/14 I have commented out the above condition after Will had made his translation changes. I have hoped to preserve the logic --->
						<cfelseif application.com.relayTranslations.doesPhraseTextIDExist("phr_#qRelayFieldList.fieldName#") and qRelayFieldList.fieldName neq application.com.relayTranslations.translatePhrase(phrase="phr_#qRelayFieldList.fieldName#")>
							<cfset thisFieldLabel="phr_#qRelayFieldList.fieldName#">
						<cfelse>
							<cfset thisfieldlabel = qRelayFieldList.defaultFieldLabel>
						</cfif>

						<!--- NJH 2009/07/08 P-FNL069 - added regex pattern checking for text input fields --->
						<cfset pattern = "">
						<cfset validate = "">
						<cfif qRelayFieldList.regexPattern neq "">
							<cfset pattern = qRelayFieldList.regexPattern>
							<cfset validate = "regex">
						</cfif>
						<cfset addressLock = qRelayFieldList.readOnly neq ""?qRelayFieldList.readOnly:false>


						<cfswitch expression="#qRelayFieldList.type#">
							<cfcase value="textInput">
								<!--- 2006/06/19 P_SNY039 GCC - this would be easier if this form was converted to use CF_RelayFormDisplay but this is good for now--->
								<cfif fieldName eq "insVATnumber">
									<!--- 2006/07/08 GCC Sony CR - only ask for VAT number if it is required --->
									<cfif isMandatory eq 1>
										<cfset VATvalidate = "">
										<cfset VATpattern = "">
										<cfif countryDetails.VATformat neq "">
											<cfset VATvalidate = "regular_expression">
											<cfset VATpattern = countryDetails.VATformat>
										</cfif>

										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" required="#isMandatory#" validate="#VATvalidate#" pattern="#VATpattern#">
									</cfif>
								<cfelseif fieldName eq "insTelephone"  or fieldName eq "insMobilePhone" or fieldName eq "insFax" >
									<!--- 2014-10-14	RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site --->
									<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" required="#isMandatory#" mask="#phonemask#" validate="telephone">

								<cfelseif  fieldName eq "insOfficePhone" >
									<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" required="#isMandatory#" mask="#phonemask#" validate="telephone">
									<cfif not attributes.hideDirectLineExtension>
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insOfficePhoneExt" currentValue="#fieldValue#" label="Phr_Ext_DirectLineExt" size="10" maxlength="10"  mask="99999">
									</cfif>
								<cfelse>
									<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" required="#isMandatory#" validate="#validate#" pattern="#pattern#" readOnly="#addressLock#">
								</cfif>
							</cfcase>
							<cfcase value="Province">
								<cfquery name="GetFullNameSwitch" datasource="#application.siteDataSource#">
									select ProvinceAbbreviation
									from country
									where countryID = <cf_queryparam value="#form.insCountryID#" cfsqltype="cf_sql_integer">
								</cfquery>
								<cfquery name="getProvince" datasource="#application.siteDataSource#">
									select p.name, p.Abbreviation
									from Province p
									where countryID = <cf_queryparam value="#form.insCountryID#" cfsqltype="cf_sql_integer">
									order by p.name
								</cfquery>
								<cfif getProvince.recordcount EQ 0>
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#">
								<cfelse>
									<!--- PPB 11/08/08 added 'Select A Value' to dropdown --->
									<cfif GetFullNameSwitch.ProvinceAbbreviation EQ "" or GetFullNameSwitch.ProvinceAbbreviation EQ "false">
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="name" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" readOnly="#attributes.lockAddress?true:false#">
									<cfelse>
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="Abbreviation" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" readOnly="#attributes.lockAddress?true:false#">
									</cfif>
								</cfif>
							</cfcase>
							<!--- NJH 2007/11/23 added region used in a partner locator... this is tied to address10 --->
							<cfcase value="Region">
								<cfquery name="getRegion" datasource="#application.siteDataSource#">
									select r.name
									from Region r
									where countryID = <cf_queryparam value="#form.insCountryID#" cfsqltype="cf_sql_integer">
									order by r.name
								</cfquery>
								<cfif getRegion.recordcount EQ 0>
									<cf_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#">
								<cfelse>
									<cf_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getRegion#" display="name" value="name" nullText="" nullValue="" size="1" required="#isMandatory#" disabled="#attributes.lockAddress?true:false#">
								</cfif>
							</cfcase>
							<cfcase value="validValueList">
								<cfif isDefined("validFieldName")>
									<cf_getValidValues validFieldName = "#validFieldName#" countryID = #form.insCountryID#>
									<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#validvalues#" display="displayvalue" value="dataValue" nullText="" nullValue="" size="1" required="#isMandatory#">
								<cfelse>
									You must define a validFieldName for a validValueList form element type.
								</cfif>
							</cfcase>

							<cfcase value="countryList">
								<cfquery name="getCountries" datasource="#application.siteDataSource#">
									SELECT DISTINCT CountryID, CountryDescription
										  FROM Country
										  where ISOCode is not null
									ORDER BY CountryDescription ASC
								</cfquery>
								<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#form.insCountryID#" fieldName="insCountryID" size="1" query="#getCountries#" display="CountryDescription" value="CountryID" label="phr_Ext_Country" disabled="True">
								<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
								</cfcase>

							<cfcase value="email">
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="insEmail" label="#thisfieldlabel#" validate="email" onValidate="validateEmail" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" readonly="#attributes.domainMatch?true:false#" attributeCollection=#emailAttributes#>
							</cfcase>

							<cfcase value="gender">
								<cf_querySim>
									getStati
									display,value
									Phr_male|M
									Phr_female|F
								</cf_querySim>
								<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#fieldValue#" fieldName="insSex" label="#thisfieldlabel#" query="#getStati#" display="display" value="value">
							</cfcase>

							<cfcase value="dateOfBirth">
								<cfSaveContent variable="DOBData">

									<cfselect name="insDOBDay" size="1" message="Enter Your Day of Birth" required="#isMandatory#">
											<option value="" SELECTED>Day</option>
										<cfloop index="i" from="1" to="31" step="1">
											<option value="#i#" >#i#</option>
										</cfloop>
									</cfselect>

									<cfselect name="insDOBMonth" size="1"  message="Enter Your Month of Birth" required="#isMandatory#">
									<cfset j = 1>
											<option value="" >Month</option>
										<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
											<option value="#j#" >#i#</option>
											<cfset j = j + 1>
										</cfloop>
									</cfselect>

									<cfinput type="Text" name="insDOBYear" range="1930,1988" message="Enter Your Year of Birth (between 1930-1988)" required="#isMandatory#" size="4">
								</cfsavecontent>
								<CF_relayFormElementDisplay relayFormElementType="HTML" label="#thisfieldlabel#" currentValue="#DOBData#">
							</cfcase>

						</cfswitch>
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#fieldValue#" fieldname="#qRelayFieldList.fieldName#">
				</cfif>
				</cfif>
			</cfloop>

</cffunction>

<cfscript>
	private void function autoPopulate(struct userDetails) {
		var processedStruct = arguments.userDetails;
		if (structKeyExists(processedStruct,"insCountryID") AND processedStruct.insCountryID neq 0) {
			form.insCountryID=processedStruct.insCountryID;
			attributes.defaultCountryID=processedStruct.insCountryID;
			// If there's a non-zero country we have auto populated location data
			if (processedStruct.insCountryID) {
				attributes.lockAddress=true;
			}
			structDelete(processedStruct,"insCountryID");
		}

		for (var item in processedStruct) {
			// Don't overwrite on form submissions
			if (not structKeyExists(form, "#item#")) {
				form[item] = processedStruct[item];	
			}
			structDelete(processedStruct,item); 
		}
		

		// Lock email after language has been selected
		if (structKeyExists(form,"frmShowLanguageID")) {
			attributes.domainMatch=true;
		}
	}
</cfscript>


<cfsetting enablecfoutputonly="No">