<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayXMLCrudLud.cfm	
Author:				SWJ
Date started:		2007-05-20
	
Description:		Can simply be inserted into an XML screen as <CF_relayXMLCrudLud>
					to create a standard created, last updated block (see projects/projectList.cfm)

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Possible enhancements:


 --->

<field name="created" label="Date Added" control="created"></field>
<field name="createdby" label="Added By" control="createdby"></field>
<field name="lastupdated" label="Last Updated" control="lastupdated"></field>
<field name="lastupdatedby"  label="Last Updated By" control="lastupdatedby"></field>
