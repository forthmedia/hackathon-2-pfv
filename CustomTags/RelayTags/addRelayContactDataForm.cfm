<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">

<!---
File name:			addRelayContactDataForm.cfm
Author:				SWJ
Date started:		2003-02-19

Purpose: to provide a single screen for adding contact data
			(person, location and orgsniation) to a Relay DB.

Usage: 	<cf_addRelayContactData
			datasource = "relayDB"
			attributes.showCols = "" mandatory list of columns that will show
			madatoryCols = "" mandatory the list of cols that will be mandatory
			defaultCountryID = "9" mandatory
			frmNext = "" optional - where it goes after it has processed
			frmDebug = "" optional yes or no - if set to yes the frmNext form will not be processed.
						instead a message telling you what happened will show.
			frmReturnMessage = optional yes or no.  Default is no. Returns a success/failure message.
			flagStructure = this contains a structure containing flag data to set. See below.
			showMagicNumber
			>

		The form submits to remoteAdd.cfm
		By default person.firstname, person.lastname, person.email and coname are shown.
		If coname is blank then the person is added to a company called people with no org.
		If country is omitted then it defaults to the UK - id 9.

		FlagStructure - this should contain rows in a structure containing the fields
		required to set a flag.	These are:
			frmEntityTypeID - 0 for person, 1 for location, 2 for org
			frmFlagID - The ID of the Profile attribute
			frmvalue - the value to set

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed

2005-03-10 WAB & SWJ Changed the salutation field so that it uses cfselect and a slightly modified version
			of the cfForm.js which is in the Relay/javascript folder.
2005-07-06 WAB altered any validvalue fields to use cfselect with a new call to cf_getvalidvalues.  required new file customtags\validvalues\getValidValues.cfm
			meant I was able to remove code which considered salutation to be a special case
2006-05-09			AJC			country HAS to be selected first, then the form reloads.
2006-06-27	GCC	P_SNY039 Added language select, VAT number and converted to use CF_relayFormDisplay
2007-01-15  SSS Added a step metheod to use it pass increment in the datacapturemethod varible. Can pass in
                 token and orgnisation name to prepopulate the fields.
2007-04-12	AJC Added OrganisationTypeID as a hidden field for later use. REF[Trend]
2007-04-26	SSS	Added Ajax to build dropdown for languages depending on country selected
2007-10-05  GAD Added Phone masking by country features
2007/11/23	NJH	Added a region dropdown which populates address9 if address9 is shown.
2008/04/10  SSS I have added a switch that will enable people to by pass the org check if they pass in a magic number if the magic number is incorrect it will then
				Make you do a check.
2008-05-12	SSS	I have added a error meassage if the Email link is sent incorrectly.
2008-08-11	PPB Added 'Select A Value' to County/Region/State (Province) dropdown
2008/09/24	WAB CR-TND561 Req8 Mods for checking emailUniqueness
2008/11/10  NJH Bug Fix Trend Nabu Support Issue 1301 - added a case of province and region for incremental type.
2008-11-14	AJC Hide Magic Number
2009/07/09	NJH	P-FNL069 - validate firstname and lastname by accepting only alphabetic UTF characters plus space. This is using the cfform validation.
2009/07/31      SSS     P-fnl069 Add captcha to the contact form
2009-08-26	NYB  LHID2527 - fixed tab order of fields
2009/10/29	NAS	LID - 2782 - Translate 'Location'
2010/03/24	NAS LID - 3155 - Captcha issue
2010/06/30  PPB P-PAN002 New Opp screen - we want to limit the country dropdown to list just the countries relevant to the reseller
2010/09/21	NJH	LID 3956 Check the org type when doing unique email validation. Skip the check for end customers
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2011/05/23 	PPB LID5656 language dropdown wasn't populating sometimes
2011/10/12  PPB LID7969 don't reset request.relayCurrentUser.showForCountryID if the user is logged in (in which case the user will be registering a deal for a 3rd party not registering themselves for the first time)
2012-01-17	NYB Case425166 added the ability to make the email field non-required: replaced required="yes" with required="#isMandatory#"
2012-08-06 	PPB Case 428368 remove alias from getCountries order by clause
2013-04-04 	NYB	Case 433821 show VAT even if not required - by making new setting plo.hideVATNumberFieldWhenNotMandatory=false
2014-11-25	AXA	Case 442790 fix checkValidOrganisation query to handle organisation names with apostrophes
2015-02-11	AHL	Case 443196 Language select is empty with http request (not httpS)
2016-03-04	WAB	Add attribute relayFormDisplayStyle and default to "".  This is used to force the removal of the form-horizontal class which is getting added by default on the Portal
2016/06/13 	NJH	JIRA PROD2016-347 - allow partners to invite people to their location. Pass in person magic number so that we can get the locationID of the partner doing the inviting. Re-factored the magic number code to make this happen.
				Added two new attributes. AddNewAddress  - used to toggle the visiblity of the 'addNewLocation' button; showAllLocationsForInvitedPartner - show all the locations of the user's organisation who has done the inviting

Possible enhancements:
1.  Convert qRelayFieldList to a table in relayDB with a default field label

 --->
<cfoutput>

<cfparam name = "attributes.showCaptcha" default="false"> <!--- NJH 2009/07/20 P-FNL069 captcha prototype --->

<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
	<script type='text/javascript' src='/mxajax/core/js/prototype.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxAjax.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxSelectdisabled.js'></script>
	<script type='text/javascript' src='/mxajax/core/js/mxData.js'></script>
	<cfscript>
		qrynull=querynew("tempcol");
	</cfscript>

	<script language="javascript">
		// 2011/05/23 PPB LID5656 add wsdl on end to avoid conflict with code to get local timezone on first load
		var url = "/WebServices/mxAjaxCountries.cfc?wsdl";		<!--- 2015-02-11 AHL Case 443196 Language select is empty with http request (not httpS) --->

	    function init() {
	     new mxAjax.Select({
	      parser: new mxAjax.CFArrayToJSKeyValueParser(),
	      executeOnLoad: true,
	      target: "frmShowLanguageID",
	      paramArgs: new mxAjax.Param(url,{param:"countryid={insCountryID}", cffunction:"getcountrylanguages"}),
	      source: "insCountryID"
	     });
	    }

		addOnLoadEvent(function() {init();});
	</script>
</cfif>
<script language="javascript">
	function EnableSelect(){
		document.details.frmShowLanguageID.disabled = false;
		}
//p-fnl069 SSS 2009/07/31 added this for captcha mx ajax calls
function validateCaptcha() {

	// PPB 2010/03/01 P-LENxxx pre sales: added this condition to avoid a "document.details.frmcaptcha.value is null" error; would be ideal not to call this from the FORM onSubmit call but the iif syntax is awkward
	// 2010/03/24	NAS LID - 3155 - Captcha issue
	//if ("<!--- <cfoutput>#attributes.showCaptcha#</cfoutput> --->//" == "false") {
	//	return true;
	//} else {


	// PPB 2010/03/01 P-LENxxx pre sales: added this condition to avoid a "document.details.frmcaptcha.value is null" error; would be ideal not to call this from the FORM onSubmit call but the iif syntax is awkward
	if ("<cfoutput>#jsStringFormat(attributes.showCaptcha)#</cfoutput>" == "false") {
		return true;
	} else {

		userResponse=document.details.frmcaptcha.value;
		captchaResponse=document.details.frmcaptcha_hash.value;
		//alert(document.details.frmcaptcha_hash.value);
		//alert(userResponse);

		var timeNow = new Date();
		page = '/WebServices/relaycaptchaWS.cfc?wsdl';
		captchaParameters  = 'returnFormat=JSON&method=getcaptchavalidation&time='+timeNow+ '&userResponse='+userResponse+'&captchaResponse='+captchaResponse;

		var myAjax = new Ajax.Request(
			page,
			{
					method: 'get',
					asynchronous: false,
					parameters: captchaParameters,
					evalJSON: 'force',
					debug : false
			}
		)
		var captchaResponse = myAjax.transport.responseText.evalJSON();

		if (captchaResponse) {

			return true;

		} else {

			var timeNow = new Date();
			page = '/webservices/relaycaptchaWS.cfc?wsdl';
			updatercaptchaParameters  = '&method=drawCaptcha&captchaerror=true&time='+timeNow;

			var div = 'captchadiv';
			var myAjax = new Ajax.Updater(
				div,
				page,
				{
					method: 'get',
					parameters: updatercaptchaParameters,
					evalJSON: 'force',
					debug : false,
					onComplete: function () {document.details.frmcaptcha.focus();}
				});

			return false;

		}
	}
}
// 2010/03/24	NAS LID - 3155 - Captcha issue
//}
</script>
</cfoutput>

<cfparam name = "attributes.hideDirectLineExtension" default="true" > <!--- pkp 2008-04-22 add switch to hide extension text box --->
<cfparam name = "attributes.initialiseuser" default = "false">
<cfparam name = "attributes.liveLanguage" default = "false"> <!--- ss will only show live langauges for people to pick from. --->
<cfparam name = "attributes.showRequiredSymbol" default="false">  <!--- NJH 2006/11/27 --->
<cfparam name = "attributes.skipcompanycheck" default="false"> <!--- SSS 2008/04/10 --->
<!--- START: 2008-11-14	AJC Hide Magic Number --->
<cfparam name = "attributes.showMagicNumber" default="true" >
<!--- END: 2008-11-14	AJC Hide Magic Number --->
<cfif isdefined("attributes.nextstep")>
	<cfparam name = "form.nextstep" default="#attributes.nextstep#">
<cfelse>
	<cfparam name = "form.nextstep" default="1">
</cfif>
<cfparam name="attributes.insorganisationTypeID" type="numeric" default="1">
<cfparam name = "attributes.datacapturemethod" default="2step">
<cfparam name = "attributes.restrictToUserOrgCountries" default="false">  <!--- WAB 2010/11/08 added a param to fix a bug, think probably set in a relaytag --->
<cfparam name="attributes.showAllLocationsForInvitedPartner" default="false"> <!--- NJH 2016/06/13 --->
<cfparam name="attributes.allowNewAddress" default="true"> <!--- NJH 2016/06/13 --->

<cfparam name = "ErrorMessageorganisation" default="false">

<cfparam name = "phonemask" default="">
<cfparam name = "attributes.relayFormDisplayClass" default = "">

<!--- organisation magic number --->
<cfif not structKeyExists(form,"frmMagicNumber")>
	<cfif structKeyExists(url,"oo")>
		<cfset form.frmMagicNumber = url.oo>
		<cfset form.magicNumberEntityTypeID = application.entityTypeId.organisation>

	<!--- person magic number - introduced as part of JIRA PROD2016-347 so that we could add a person at the "inviters" location --->
	<cfelseif structKeyExists(url,"pNumber")>
		<cfset form.frmMagicNumber = url.pNumber>
		<cfset form.magicNumberEntityTypeID = application.entityTypeId.person>
	</cfif>
</cfif>

<!--- NJH 2009/07/20 P-FNL069 captcha prototype --->
<cfif attributes.showCaptcha and structKeyExists(form,"frmCaptcha")>
	<cfif hash(ucase(form.frmCaptcha)) neq form.frmCaptcha_Hash>
	  	<cfset failedCaptcha = true>
	</cfif>
	<cfif isDefined("failedCaptcha")>
		 <cfoutput><span class="required">phr_sys_captchaError</span></cfoutput>
	</cfif>
</cfif>

<!--- deal with magic numbers --->
<cfif structKeyExists(form,"frmMagicNumber") and form.frmMagicNumber neq "">
	<!--- <cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount") and form.magicNumberEntityTypeID eq application.entityTypeId.person>
		<cfquery name="GetMagicNumberEntity">
			Select sitename as entityName, l.locationID as entityID
			from location l
				inner join person p on l.locationID = p.locationID
			where p.personID =  <cf_queryparam value="#listfirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	<cfelse>
		<cfquery name="GetMagicNumberEntity" datasource="#application.sitedatasource#">
			Select o.OrganisationName as entityName, o.organisationID as entityID
			from Organisation o
			where o.OrganisationID =  <cf_queryparam value="#listfirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	</cfif> --->
	<cfset magicNumberEntityID = application.com.login.checkMagicNumber(MagicNumber=form.frmMagicNumber,entityTypeID=form.magicNumberEntityTypeID)>

	<cfif magicNumberEntityID NEQ 0>
		<cfset selectEntityTypeID = "organisation">

		<!--- JIRA PROD2016-347 if we're location aware, then we want to get location data; otherwise organisation. However, the magic number if person based, so we join
			from person to location table to get the location data.
			and from person to organisation to get the organisation data.
			God knows what happens if the magic number is organisation based!
			 --->
		<cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount") and form.magicNumberEntityTypeID eq application.entityTypeId.person>
			<cfset selectEntityTypeID = "location">
		</cfif>
		<cfset magicNumberEntityStruct = application.com.relayEntity.getEntityType(entityTypeID=form.magicNumberEntityTypeID)>

		<cfquery name="CheckValidMagicNumberEntity">
			Select mne.#application.com.relayEntity.getEntityType(entityTypeID=selectEntityTypeID).uniqueKey# as entityID, mne.organisationID
				,<cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")>l.sitename<cfelse>o.organisationName</cfif> as entityName
			from #magicNumberEntityStruct.tablename# mne
				<cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")>
					inner join location l on l.locationId = mne.locationID
				<cfelse>
					inner join organisation o on o.organisationId = mne.organisationid
				</cfif>
			where mne.#magicNumberEntityStruct.uniqueKey# = <cf_queryparam value="#magicNumberEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cfif>
</cfif>


<cfif isdefined("AddContactOrganisationButton")>

	<!--- NJH 2016/06/13 moved to above so everything to deal with magic numbers is in one place.
	<cfset x = application.com.login.checkMagicNUmber(MagicNumber=FRMMAGICNUMBER,entityTypeID=application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?0:2)>

	<!--- START: 2014-11-25 AXA replace apostrophe with underscore  --->
	<cfif FindNoCase("`",insSiteName) GT 0>
		<cfset insSiteName = Replace(insSiteName,"`","_","ALL") />
	</cfif>
	<!--- END: 2014-11-25 AXA replace apostrophe with underscore  --->

	<!--- WAB 2007-05-22 added the Ns --->
	<cfquery name="CheckValidOrganisation" datasource="#application.sitedatasource#">   <!--- WAB 2007-05-22 added the Ns --->
		Select OrganisationID
		from organisation
		<!--- START: 2014-11-25	AXA	Case 442790 fix checkValidOrganisation query to handle organisation names with apostrophes --->
		where (Organisation.OrganisationName LIKE  <cf_queryparam value="#insSiteName#" CFSQLTYPE="CF_SQL_VARCHAR" >  or
				Organisation.aka LIKE  <cf_queryparam value="#insSiteName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		<!--- END: 2014-11-25	AXA	Case 442790 fix checkValidOrganisation query to handle organisation names with apostrophes --->
		and organisation.OrganisationID =  <cf_queryparam value="#x#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery> --->

	<cfif CheckValidMagicNumberEntity.recordcount EQ 0>
		<cfset nextstep = nextstep>
		<cfset ErrorMessageorganisation = true>
	<cfelse>
		<cfset nextstep = nextstep + 1>
	</cfif>
</cfif>
<cfif isdefined("addnewlocationButton")>
	<cfset nextstep = 4>
</cfif>
<cfif isdefined("addContactlocationButton")>
	<cfset nextstep =5>
</cfif>
<cfif isdefined("addlocationnewButton")>
	<cfset nextstep =5>
</cfif>

<!--- NJH 2009/07/27 P-FNL069 CAPTCHA - check if the captcha was on and whether it was successful --->
<cfif isDefined("addContactSaveButton") and (not isDefined("failedCaptcha") or (isDefined("failedCaptcha") and not failedCaptcha))>
	<cfparam name="frmDebug" type="boolean" default="no">

	<cfinclude template="/remoteAddTask.cfm">

	<!--- WAB 2006-02-08 enables person to be added and immediately become the currentuser --->
	<cfif attributes.initialiseuser is true and not request.relaycurrentuser.isInternal>
		<cfset application.com.login.initialiseAnExternalUser(thisPersonID)>
	</cfif>

	<cfif isDefined("caller.message")>
		<cfset caller.message = message>
	</cfif>

	<cfif structKeyExists(attributes,"debug")>
		<cfoutput>#message#</cfoutput>
	</cfif>

	<cfif isDefined('form.portalMemberFlagTextID')>
		<cfquery name="GetOrgID" datasource="#application.sitedatasource#">
			Select OrganisationID from person where personID =  <cf_queryparam value="#thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfscript>
		// add them to the members flag
		application.com.flag.setBooleanFlag(GetOrgID.OrganisationID,form.portalMemberFlagTextID);
		</cfscript>
	</cfif>

	<cfset showIntroText = "No">
<cfelse>
<!--- NJH 2007/02/02 --->
	<cfparam name="form.selectedCountry" type="boolean" default="0">
	<!--- 2007/02/22 GCC If they choose a country that is different to their show for countryID set it to be the same --->
	<!--- 2011/10/12 PPB LID7969 don't set showForCountryID if the user is logged in (in which case the user will be registering a deal for a 3rd party not registering themselves for the first time) --->
	<cfif form.selectedCountry eq 1 and structKeyExists(form,"insCountryID") and form.insCountryID neq request.relayCurrentUser.Content.showForCountryID and not request.relayCurrentUser.isLoggedIn>
		<cfset request.relayCurrentUser.Content.showForCountryID = form.insCountryID>
	</cfif>

	<!--- 2007/01/03 - GCC - if lang + country passed skip to form --->
	<cfif structkeyexists(url,"lang")>
		<cfif listlen(lang,"-") eq 2>

			<cfquery name="getCountry" datasource="#application.siteDataSource#">
				SELECT CountryID
				FROM Country
				where ISOCode =  <cf_queryparam value="#listLast(lang,"-")#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
			<cfif getCountry.recordcount eq 1>
				<cfset form.insCountryID = getCountry.CountryID>
			</cfif>

			<cfquery name="getLanguage" datasource="#application.siteDataSource#">
				SELECT LanguageID
				FROM         Language
				where ISOcode =  <cf_queryparam value="#listFirst(lang,"-")#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
			<cfif getLanguage.recordcount eq 1>
				<cfset form.frmShowLanguageID = getLanguage.LanguageID>
			</cfif>
			<cfif nextstep EQ 1>
				<cfif getLanguage.recordcount eq 1 and getCountry.recordcount eq 1>
					<cfif Comparenocase(attributes.datacapturemethod, "incremental") EQ 0>
						<cfset nextstep = 2>
					<cfelse>
						<cfset nextstep = 6>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>

		</cfif>
	<cfelse>
		<!--- add to make sure the form comes up for the quickcard stuff --->
		<!--- NJH 2009/07/27 P-FNL069 CAPTCHA  - add check for form variables, as if the captcha is incorrectly entered, we want to redisplay the form --->
		<!--- SSS/AJC 2010/07/13 fixed this so that incremental contact form would work --->
		<cfif (isdefined("Url.FrmCountryID") or structKeyExists(form,"insCountryID")) and Comparenocase(attributes.datacapturemethod, "incremental") NEQ 0>

			<cfset nextstep = 6>
		</cfif>
	</cfif>
	<cfif isdefined("form.insCountryID") and (nextstep eq 6 or nextstep eq 4 or nextstep eq 5 ) >
		<!--- get phone mask --->
		<cfset phoneMask = application.com.commonQueries.getCountry(countryid=form.insCountryID).telephoneformat>
 	</cfif>

<!--- NJH 2007/02/02 --->
		<cfscript>
		// this query controls the behanvious of each form element
		//NJH 2009/07/08 P-FNL069 - added regexPattern to validate firstname and lastname to accept only alphabetic UTF characters plus space
		qRelayFieldList = QueryNew("fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern");
		if (isdefined("attributes.personDataOnly") and attributes.personDataOnly eq 1){
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insSalutation");
			temp = QuerySetCell(qRelayFieldList, "type", "validValueList");
			temp = QuerySetCell(qRelayFieldList, "params", "validFieldName=person.salutation");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_Salutation");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insFirstName");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_FirstName");
			temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insLastName");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_LastName");
			temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insEmail");
			temp = QuerySetCell(qRelayFieldList, "type", "email");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_EmailAddress");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insCountryID");
			temp = QuerySetCell(qRelayFieldList, "type", "countryList");
			temp = QuerySetCell(qRelayFieldList, "params", "");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Country");
		}
		else {
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insSalutation");
				temp = QuerySetCell(qRelayFieldList, "type", "validValueList");
			temp = QuerySetCell(qRelayFieldList, "params", "validFieldName=person.salutation");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_Salutation");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insFirstName");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_FirstName");
			temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insLastName");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_LastName");
			temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insJobDesc");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_ext_jobDesc");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insEmail");
			temp = QuerySetCell(qRelayFieldList, "type", "email");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_EmailAddress");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insOfficePhone");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DirectLine");
			temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);

		//<!--- pkp 2008-04-28 bug 214 to remove duplicate ext fields   --->
		//newRow  = QueryAddRow(qRelayFieldList);
			//temp = QuerySetCell(qRelayFieldList, "fieldName", "insOfficePhoneExt");
			//temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			//temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=10,maxfieldsize=10");
			//temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DirectLineExt");



		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insMobilePhone");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_MobilePhone");
			temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);

		if (isdefined('insorganisationID') and val(insorganisationID) neq 0 )
		{
			newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "inslocationID");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=80");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");
		}

		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insSiteName");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=255");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insaka");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_aka");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insVATnumber");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_VATnumber");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress1");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Address1");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress2");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Address2");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress3");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Address3");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress4");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_TownCity");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress5");
			//if (isdefined('form.insCountryID') and (form.insCountryID eq 17 or form.insCountryID eq 276)){
				//temp = QuerySetCell(qRelayFieldList, "type", "validValueList");
				//temp = QuerySetCell(qRelayFieldList, "params", "validFieldName=location.address5");
				//temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_AddressState");}
			//else {
				temp = QuerySetCell(qRelayFieldList, "type", "Province");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_CountyRegionState");//}

		// NJH 2006-12-05 added address6-address9 as these fields need to be captured for countries such as the Netherlands.
		// This was part of moving the quickCardAdd to use this custom tag
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress6");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_CountyRegionState");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress7");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBox");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress8");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBoxPostcode");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress9");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_POBoxTown");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insAddress10");
			temp = QuerySetCell(qRelayFieldList, "type", "Region");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Sys_GreaterRegion");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insPostalCode");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=10,maxfieldsize=20");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_PostalCodeZip");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insCountryID");
			temp = QuerySetCell(qRelayFieldList, "type", "countryList");
			temp = QuerySetCell(qRelayFieldList, "params", "");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Country");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insTelephone");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Switchboard");
			temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insFax");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_FaxInclCountryCode");
			temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insOrgURL");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_WebSiteAddress");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insUsername");
			temp = QuerySetCell(qRelayFieldList, "type", "textInput");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Username");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insSex");
			temp = QuerySetCell(qRelayFieldList, "type", "gender");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Gender");
		newRow  = QueryAddRow(qRelayFieldList);
			temp = QuerySetCell(qRelayFieldList, "fieldName", "insDOB");
			temp = QuerySetCell(qRelayFieldList, "type", "dateOfBirth");
			temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
			temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DateOfBirth");
		}
		</cfscript>

	<!--- 2006-05-09 AJC P_COR001 CountryID now has to be selected first --->
	<cfif form.selectedCountry is 0 and not (structKeyExists(form,"insCountryID") and structKeyExists(form,"frmShowLanguageID"))>
		<cfif not isdefined("attributes.CountryGroup") or attributes.CountryGroup is false>
			<cfquery name="getCountries" datasource="#application.siteDataSource#">
				SELECT DISTINCT CountryID,
				case when ltrim(isnull(localcountrydescription, '')) = '' then CountryDescription else localcountrydescription end as CountryDescription
				FROM Country
					where isNull(isocode,'') <> ''
					<cfif attributes.restrictToUserOrgCountries>
						AND CountryID IN (SELECT DISTINCT Location.CountryID FROM Location INNER JOIN Person ON Location.OrganisationID = Person.OrganisationID WHERE (Person.PersonID = #request.relayCurrentUser.personID#))
					</cfif>
				ORDER BY CountryDescription ASC
			</cfquery>
		<cfelse>
			 <cfif IsNumeric(attributes.CountryGroup)>
				<cfset CountryGroupID = attributes.CountryGroup>
			<cfelse>
				<cfquery name="getCountryGroup" datasource="#application.siteDataSource#">
					select C1.countryID
					from Country c1
					where countrydescription =  <cf_queryparam value="#attributes.CountryGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfset CountryGroupID = getCountryGroup.CountryID>
			</cfif>

			<cfquery name="getCountries" datasource="#application.siteDataSource#">
				SELECT  c1.CountryID,
						case when ltrim(isnull(c1.localcountrydescription, '')) = '' then c1.CountryDescription else c1.localcountrydescription end as CountryDescription
					FROM Country c1 INNER JOIN CountryGroup cg1 ON c1.CountryID = cg1.CountryMemberID
					WHERE (cg1.CountryGroupID =  <cf_queryparam value="#CountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
					AND isNull(isocode,'') <> ''
					order by countryDescription		<!--- 2012-08-06 PPB Case 428368 remove c1. alias so it refers to the CountryDescription defined in this query not the underlying table --->
			</cfquery>
		</cfif>

		<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
		<cfelse>
			<cfquery name="getLanguages" datasource="#application.siteDataSource#">
				SELECT DISTINCT [LanguageID],
				case when ltrim(isnull(localLanguageName, '')) = '' then language else localLanguageName end as languageDescription
				FROM         Language
				<cfif attributes.liveLanguage>
					where languageID  in ( <cf_queryparam value="#request.currentSite.liveLanguageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				ORDER BY languageDescription ASC
			</cfquery>
		</cfif>

		<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" name="details" onSubmit="EnableSelect()" >
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#attributes.defaultCountryID#" fieldName="insCountryID" size="1" query="#getCountries#" display="CountryDescription" value="countryid" label="Phr_Ext_Country" required="Yes" nulltext="Phr_ChooseACountry" nullValue="">
				<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
					<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmShowLanguageID" size="1" query="#qrynull#" display="tempcol" value="tempcol" label="Phr_Ext_Language" required="Yes">
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#request.relaycurrentuser.languageID#" fieldName="frmShowLanguageID" size="1" query="#getLanguages#" display="languageDescription" value="languageID" label="Phr_Ext_Language" required="Yes">
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactCountryButton" currentValue="phr_continue" label="" spanCols="No" valueAlign="left" class="button">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="1" fieldname="selectedCountry">
				<cfif structKeyExists(form,"frmMagicNumber")>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmMagicNumber#" fieldname="frmMagicNumber">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
				</cfif>
				<cfif Comparenocase(attributes.datacapturemethod, "incremental") EQ 0>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="2" fieldname="nextstep">
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="6" fieldname="nextstep">
				</cfif>
			</cf_relayFormDisplay>
		</cfform>
	</cfif>
	<!--- This block below will by-pass the organisation check unless a magic number is incorrect
	SSS 2008/04/10--->
	<cfif structKeyExists(form,"frmMagicNumber") AND attributes.skipcompanycheck and nextstep EQ 2>

		<!--- NJH 2016/06/13 - commented out as done on top of file

			<cfset listfirstP = listfirst( form.magicNumber , "-")>

			 <cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")>
				<cfquery name="GetMagicNumberEntity">
					Select sitename as entityName, l.locationID as entityID
					from location l
						inner join person p on l.locationID = p.locationID
					where p.personID =  <cf_queryparam value="#listfirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			<cfelse>
				<cfquery name="GetMagicNumberEntity" datasource="#application.sitedatasource#">
					Select o.OrganisationName as orgname, o.organisationID as x
					from Organisation o
					where o.OrganisationID =  <cf_queryparam value="#listfirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfif> --->

			<cfif CheckValidMagicNumberEntity.recordcount NEQ 0>
				<!--- <cfquery name="CheckValidOrganisation" datasource="#application.sitedatasource#">
					Select OrganisationID
					from organisation
					where (Organisation.OrganisationName =  <cf_queryparam value="#GetExistingOrganisation.orgname#" CFSQLTYPE="CF_SQL_VARCHAR" >  or
							Organisation.aka =  <cf_queryparam value="#GetExistingOrganisation.orgname#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					and organisation.OrganisationID =  <cf_queryparam value="#GetExistingOrganisation.x#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery> --->

				<cfset form.insSiteName = CheckValidMagicNumberEntity.entityName>
				<!--- <cfset form.frmMagicNumber = form.magicNumber> --->
				<cfset form.frmshowlanguageID = frmshowlanguageID>
				<cfset form.inscountryID = inscountryID>
				<cfset form.SelectedCountry = SelectedCountry>
				<cfset nextstep = 3>
			<cfelse>
				<cfoutput>
				<form method="POST" name="messageform" id="messageform">
					<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
						<CF_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="phr_addcontactform_URLErrorMeassage">
					</cf_relayFormDisplay>
				</form>
				</cfoutput>
				<cfset nextstep = 0>
			</cfif>
	</cfif>
	<cfif nextstep EQ 2>
		<!--- check organisation to make sure it exists --->
		<!--- NJH 2016/06/13 - commented out as done at top of file.
		<cfif isdefined("form.oo")>
			<cfset listfirstP = listfirst( form.oo , "-")>
			<cfquery name="GetExistingOrganisation" datasource="#application.sitedatasource#">
				Select Organisation.OrganisationName as orgname
				from Organisation
				where Organisation.OrganisationID =  <cf_queryparam value="#listfirstP#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
		</cfif> --->

		<cfoutput>
		<form method="POST" name="details" id="details">
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfif ErrorMessageorganisation>
					<CF_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="phr_addcontactform_organisationdoesnotexists">
				</cfif>
				<cfif structKeyExists(form,"frmMagicNumber")>
					<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insSiteName" currentValue="#CheckValidMagicNumberEntity.entityName#" label="Phr_Ext_OrgCompanyName" size="50" maxlength="100" tabindex="8" required="yes">
					<!--- START: 2008-11-14	AJC Hide Magic Number --->
					<cfif attributes.showMagicNumber>
						<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmMagicNumber" currentValue="#form.frmMagicNumber#" label="phr_Ext_EncodedNumber" size="50" maxlength="100" tabindex="8" required="yes">
					<cfelse>
						<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmMagicNumber#" fieldname="frmMagicNumber">
					</cfif>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
					<!--- END: 2008-11-14	AJC Hide Magic Number --->
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insSiteName" currentValue="" label="Phr_Ext_OrgCompanyName" size="50" maxlength="100" tabindex="8" required="yes">
					<!--- START: 2008-11-14	AJC Hide Magic Number --->
					<cfif attributes.showMagicNumber>
						<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmMagicNumber" currentValue="" label="phr_Ext_EncodedNumber" size="50" maxlength="100" tabindex="8" required="yes">
					<cfelse>
						<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="" fieldname="frmMagicNumber">
					</cfif>
					<!--- END: 2008-11-14	AJC Hide Magic Number --->
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactOrganisationButton" currentValue="phr_continue" label="" spanCols="No" valueAlign="left" class="button">
			</cf_relayFormDisplay>
		</form>
		</cfoutput>
	</cfif>
	<cfif nextstep EQ 3>
		<!--- select a location --->
		<cfquery name="getLocation" datasource="#application.siteDataSource#">
			select LocationID as value, '' as display, *
			from location l
				<!--- NJH 2016/06/13 JIRA PROD2016-347 only get/show approved locations if in location-aware mode --->
				<cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")>
				inner join vBooleanFlagData bfd on bfd.entityID = l.locationID and bfd.flagTextId='locApproved'
				</cfif>
			where 
				<!--- if only showing location for the partner who did the inviting --->
				<cfif application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount") and not attributes.showAllLocationsForInvitedPartner>
					locationID = <cf_queryparam value="#CheckValidMagicNumberEntity.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- show all locations at the organisation --->
				<cfelse>
					organisationID = <cf_queryparam value="#CheckValidMagicNumberEntity.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
			order by sitename
	 	</cfquery>

		<cfloop query = "getLocation">
			<!--- get a formatted address and pop it into the query, ready to be used by the radio button displayer --->
			<cfset formattedAddress = application.com.screens.evaluateAddressFormat(location= getLocation,row=currentrow,separator="<BR>",finalcharacter="<BR>&nbsp;",showcountry=true)>
			<cfset querySetCell(getLocation,"display", formattedAddress, currentrow)>
		</cfloop>

		<cfif not getLocation.recordcount>
			<cfoutput><p>phr_addcontactform_noLocationFound</p></cfoutput>
		<cfelse>
			<cfoutput>
			<form method="POST" name="details" id="details">
				<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
					<!--- 2009/10/29	NAS	LID - 2782 - Code updated (label) to Translate 'Location' --->
					<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#getLocation.recordCount eq 1?getLocation.locationId[1]:''#" message="phr_LocationRequired" fieldName="inslocationID" label="phr_location" query = "#getlocation#" display="display" value="value" required="yes" noteText="Please select a location">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
					<!--- <CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CheckValidOrganisation.organisationID#" fieldname="insOrganisationID"> --->

					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CheckValidMagicNumberEntity.organisationID#" fieldname="insOrganisationID">

					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
					<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactlocationButton" currentValue="phr_continue" label="" spanCols="Yes" valueAlign="center" class="button">
				</cf_relayFormDisplay>
			</form>
			</cfoutput>
		</cfif>

		<cfif attributes.allowNewAddress>
			<cfoutput>
			<form method="POST" name="addnewlocationButton" id="addnewlocationButton">
				<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CheckValidMagicNumberEntity.organisationID#" fieldname="insOrganisationID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
					<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addnewlocationButton" currentValue="phr_Addnewlocation" label="" spanCols="yes" valueAlign="center" class="button">
				</cf_relayFormDisplay>
			</form>
			</cfoutput>
		<cfelse>
			<!--- if we can't add a new location, then output a message. Here we have a merge field available for the person's email who has done the inviting --->
			<cfif magicNumberEntityTypeID eq application.entityTypeID.person and magicNumberEntityID neq 0>

				<cfquery name="getPersonEmail">
					select email from person where personID = <Cf_queryparam value="#magicNumberEntityID#" cfsqltype="cf_sql_integer">
				</cfquery>

				<!--- if user is not allowed to add a location, output a message saying what they need to do next --->
				<cfoutput><p>#application.com.relayTranslations.translatePhrase(phrase="addcontactform_locationNotInListContactAdmin",mergeStruct={invitedByEmailAddress=getPersonEmail.email})#</p></cfoutput>
			</cfif>
		</cfif>
	</cfif>
	<cfif nextstep EQ 4>
			<!--- Add a new location incremental--->
			<cfparam name = "attributes.ShowLocationCols" default = "insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax">
			<cfparam name="form.insCountryID" default="#attributes.defaultCountryID#">
			<cfset countryDetails = application.com.commonQueries.getCountry(countryid=form.insCountryID)>
			<cfparam name="fieldSize" default="25" type="numeric">
			<cfparam name="maxFieldSize" default="30" type="numeric">
			<cfparam name="attributes.hideCompanyName" default="No" type="boolean">
		<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" name="details" >
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfoutput>
				<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
			<cfif countryDetails.VATrequired>
				<cfset attributes.mandatoryCols = listAppend(attributes.mandatoryCols,"insVATnumber")>
			</cfif>
			<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
			<cfset tabIndex = 1>
			<!--- END:  2009-08-26 LHID2527 --->

			<cfloop query="qRelayFieldList">
				<!--- get any name value pairs defined in params and make them available to this scope --->
				<CF_EvaluateNameValuePair NameValuePairs = "#qRelayFieldList.params#">
				<cfif listfindNocase(attributes.ShowLocationCols,qRelayFieldList.fieldName) neq 0>
					<cfif listfindNocase(attributes.mandatoryCols,qRelayFieldList.fieldName) neq 0>
						<cfset isMandatory = "Yes">
					<cfelse>
						<cfset isMandatory = "No">
					</cfif>

					<cfif not (fieldName eq "insSiteName" and attributes.hideCompanyName eq "yes")>

						<cfif left(defaultFieldLabel,3) is "phr">
							<!--- LID 5395 - NJH - changed from this to below <cfset thisfieldlabel = evaluate("#defaultFieldLabel#")> --->
							<cfset thisfieldlabel = defaultFieldLabel>
						<!--- If translation returns 'no translation' or the returned phrase isn't different to the sent phrase don't use it --->
						<!--- <cfelseif findNoCase("no translation",evaluate("phr_#qRelayFieldList.fieldName#"),1) eq 0 AND qRelayFieldList.fieldName neq trim(evaluate("phr_#qRelayFieldList.fieldName#"))> --->

						<!--- NJH 2010/10/14 I have commented out the above condition after Will had made his translation changes. I have hoped to preserve the logic --->
						<cfelseif application.com.relayTranslations.doesPhraseTextIDExist("phr_#qRelayFieldList.fieldName#") and qRelayFieldList.fieldName neq application.com.relayTranslations.translatePhrase(phrase="phr_#qRelayFieldList.fieldName#")>
							<cfset thisFieldLabel="phr_#qRelayFieldList.fieldName#">
						<cfelse>
							<cfset thisfieldlabel = #qRelayFieldList.defaultFieldLabel#>
						</cfif>

						<!--- NJH 2009/07/08 P-FNL069 - added regex pattern checking for text input fields --->
						<cfset pattern = "">
						<cfset validate = "">
						<cfif qRelayFieldList.regexPattern neq "">
							<cfset pattern = qRelayFieldList.regexPattern>
							<cfset validate = "regex">
						</cfif>

						<cfswitch expression="#qRelayFieldList.type#">

							<cfcase value="textInput">
								<!--- 2006/06/19 P_SNY039 GCC - this would be easier if this form was converted to use CF_RelayFormDisplay but this is good for now--->
									<cfif fieldName eq "insVATnumber">
										<!--- 2006/07/08 GCC Sony CR - only ask for VAT number if it is required --->
										<cfif isMandatory eq 1>
											<cfset VATvalidate = "">
											<cfset VATpattern = "">
											<cfif countryDetails.VATformat neq "">
												<cfset VATvalidate = "regular_expression">
												<cfset VATpattern = countryDetails.VATformat>
											</cfif>
											<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#VATvalidate#" pattern="#VATpattern#">
										</cfif>
									<cfelseif fieldName eq "insTelephone" or fieldName eq "insOfficePhone"  or fieldName eq "insMobilePhone" or fieldName eq "insFax" >
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#">
									<cfelse>
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#validate#" pattern="#pattern#">
									</cfif>
								</cfcase>

							<!--- NJH 2008/11/10 Bug Fix Trend Nabu Support Issue 1301 - insAddress5 is of type Province.. this was missing in this case statement where we're doing an incremental add.
								Added both Province and Region.. It's not great that we've got this in two places...
							 --->
							<cfcase value="Province">
								<cfquery name="GetFullNameSwitch" datasource="#application.siteDataSource#">
									select ProvinceAbbreviation
									from country
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfquery>
								<cfquery name="getProvince" datasource="#application.siteDataSource#">
									select p.name, p.Abbreviation
									from Province p
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
									order by p.name
								</cfquery>
								<cfif getProvince.recordcount EQ 0>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
								<cfelse>
									<!--- PPB 2008-08-11 added 'Select A Value' to dropdown --->
									<cfif GetFullNameSwitch.ProvinceAbbreviation EQ "" or GetFullNameSwitch.ProvinceAbbreviation EQ "false">
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="name" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
									<cfelse>
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="Abbreviation" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
									</cfif>
								</cfif>
							</cfcase>
							<cfcase value="Region">
								<cfquery name="getRegion" datasource="#application.siteDataSource#">
									select r.name
									from Region r
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
									order by r.name
								</cfquery>
								<cfif getRegion.recordcount EQ 0>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<cf_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
								<cfelse>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<cf_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getRegion#" display="name" value="name" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
								</cfif>
							</cfcase>

							<cfcase value="validValueList">
								<cfif isDefined("validFieldName")>
									<cf_getValidValues validFieldName = "#validFieldName#" countryID = #form.insCountryID#>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#validvalues#" display="displayvalue" value="dataValue" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
								<cfelse>
									You must define a validFieldName for a validValueList form element type.
								</cfif>
							</cfcase>

							<cfcase value="countryList">
								<cfquery name="getCountries" datasource="#application.siteDataSource#">
									SELECT DISTINCT CountryID, CountryDescription
										  FROM Country
										  where ISOCode is not null
									ORDER BY CountryDescription ASC
								</cfquery>
								<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#form.insCountryID#" fieldName="insCountryID" size="1" query="#getCountries#" display="CountryDescription" value="CountryID" label="phr_Ext_Country" disabled="Yes" tabindex="#tabIndex#">
								<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
								</cfcase>

							<cfcase value="email">
								<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="insEmail" label="#thisfieldlabel#" validate="email" onValidate="validateEmail" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" personid=0 tabindex="8" orgType="#attributes.insorganisationTypeID#">	   <!--- WAB 2008/09/24 CR-TND561 Req8 personid required for unique email validation, in this case it is a new record so set to 0--->
							</cfcase>

							<cfcase value="gender">
								<cf_querySim>
									getStati
									display,value
									Phr_male|M
									Phr_female|F
								</cf_querySim>
								<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" fieldName="insSex" label="#thisfieldlabel#" query="#getStati#" display="display" value="value" tabindex="#tabIndex#">
							</cfcase>

							<cfcase value="dateOfBirth">
								<cfSaveContent variable="DOBDataHTML">
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBDay" size="1" message="Enter Your Day of Birth" required="#isMandatory#" tabindex="#tabIndex#">
											<option value="" SELECTED>Day</option>
										<cfloop index="i" from="1" to="31" step="1">
											<option value="#i#" >#htmleditformat(i)#</option>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBMonth" size="1"  message="Enter Your Month of Birth" required="#isMandatory#" tabindex="#tabIndex#">
									<cfset j = 1>
											<option value="" >Month</option>
										<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
											<option value="#j#" >#htmleditformat(i)#</option>
											<cfset j = j + 1>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfinput type="Text" name="insDOBYear" range="1930,1988" message="Enter Your Year of Birth (between 1930-1988)" required="#isMandatory#" size="4" tabindex="#tabIndex#">
								</cfsavecontent>
								<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="HTML" label="#thisfieldlabel#" currentValue="#DOBDataHTML#" tabindex="#tabIndex#">
							</cfcase>

						</cfswitch>
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="" fieldname="#qRelayFieldList.fieldName#">
				</cfif>
				</cfif>
				<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
				<cfset tabIndex = tabIndex + 1>
				<!--- END:  2009-08-26 LHID2527 --->
			</cfloop>
		</cfif>
		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">
		</cfoutput>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insOrganisationID#" fieldname="insOrganisationID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
				<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addlocationnewButton" currentValue="phr_continue" label="" spanCols="No" valueAlign="left" class="button" tabindex="#tabIndex#">
		</cf_relayFormDisplay>
		</cfform>
	</cfif>
	<cfif #nextstep# EQ 5>
			<!--- add new person incremental--->
			<cfparam name = "attributes.showPersonCols" default = "insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone,insOfficePhoneExt,insSex,insDOB">
			<cfparam name="form.insCountryID" default="#attributes.defaultCountryID#">
			<cfset countryDetails = application.com.commonQueries.getCountry(countryid=form.insCountryID)>
			<cfparam name="fieldSize" default="25" type="numeric">
			<cfparam name="maxFieldSize" default="30" type="numeric">
			<cfparam name="attributes.hideCompanyName" default="No" type="boolean">
		<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" name="details" >
			<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">
				<cfoutput>
				<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
			<cfif countryDetails.VATrequired>
				<cfset attributes.mandatoryCols = listAppend(attributes.mandatoryCols,"insVATnumber")>
			</cfif>
			<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
			<cfset tabIndex = 1>
			<!--- END:  2009-08-26 LHID2527 --->
			<cfloop query="qRelayFieldList">
				<!--- get any name value pairs defined in params and make them available to this scope --->
				<CF_EvaluateNameValuePair NameValuePairs = "#qRelayFieldList.params#">
				<cfif listfindNocase(attributes.showPersonCols,qRelayFieldList.fieldName) neq 0>
					<cfif listfindNocase(attributes.mandatoryCols,qRelayFieldList.fieldName) neq 0>
						<cfset isMandatory = "Yes">
					<cfelse>
						<cfset isMandatory = "No">
					</cfif>

					<cfif not (fieldName eq "insSiteName" and attributes.hideCompanyName eq "yes")>

						<cfif left(defaultFieldLabel,3) is "phr">
							<!--- LID 5395 - NJH - changed from this to below <cfset thisfieldlabel = evaluate("#defaultFieldLabel#")> --->
							<cfset thisfieldlabel = defaultFieldLabel>
						<!--- If translation returns 'no translation' or the returned phrase isn't different to the sent phrase don't use it --->
						<!--- <cfelseif findNoCase("no translation",evaluate("phr_#qRelayFieldList.fieldName#"),1) eq 0 AND qRelayFieldList.fieldName neq trim(evaluate("phr_#qRelayFieldList.fieldName#"))>
							<cfset thisfieldlabel = evaluate("phr_#qRelayFieldList.fieldName#")> --->
						<!--- NJH 2010/10/14 I have commented out the above condition after Will had made his translation changes. I have hoped to preserve the logic --->
						<cfelseif application.com.relayTranslations.doesPhraseTextIDExist("phr_#qRelayFieldList.fieldName#") and qRelayFieldList.fieldName neq application.com.relayTranslations.translatePhrase(phrase="phr_#qRelayFieldList.fieldName#")>
							<cfset thisFieldLabel="phr_#qRelayFieldList.fieldName#">
						<cfelse>
							<cfset thisfieldlabel = #qRelayFieldList.defaultFieldLabel#>
						</cfif>

						<!--- NJH 2009/07/08 P-FNL069 - added regex pattern checking for text input fields --->
						<cfset pattern = "">
						<cfset validate = "">
						<cfif qRelayFieldList.regexPattern neq "">
							<cfset pattern = qRelayFieldList.regexPattern>
							<cfset validate = "regex">
						</cfif>

						<cfswitch expression="#qRelayFieldList.type#">

							<cfcase value="textInput">
								<!--- 2006/06/19 P_SNY039 GCC - this would be easier if this form was converted to use CF_RelayFormDisplay but this is good for now--->
									<cfif fieldName eq "insVATnumber">
										<!--- 2006/07/08 GCC Sony CR - only ask for VAT number if it is required --->
										<cfif isMandatory eq 1>
											<cfset VATvalidate = "">
											<cfset VATpattern = "">
											<cfif countryDetails.VATformat neq "">
												<cfset VATvalidate = "regular_expression">
												<cfset VATpattern = countryDetails.VATformat>
											</cfif>
											<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#VATvalidate#" pattern="#VATpattern#">
										</cfif>
								<!--- NYF 2008-06-05 NABU bug 440 - hideDirectLineExtension=true doesn't work on Org is known Add Contact Form
											added additional else line for fieldName eq "insOfficePhone" and hideDirectLineExtension code from "#nextstep# eq 6" area --->
									<cfelseif fieldName eq "insTelephone" or fieldName eq "insMobilePhone" or fieldName eq "insFax" >
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#">
									<cfelseif fieldName eq "insOfficePhone">
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#" noTR="#iif(not attributes.hideDirectLineExtension,de('START'),de(''))#">
										<cfif not attributes.hideDirectLineExtension>
											<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insOfficePhoneExt" currentValue="" label="Phr_Ext_DirectLineExt" size="10" maxlength="10" tabindex="#tabIndex#"  mask="99999" noTR="END">
										</cfif>
									<cfelse>
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#validate#" pattern="#pattern#">
									</cfif>
								</cfcase>

							<cfcase value="validValueList">
								<cfif isDefined("validFieldName")>
									<cf_getValidValues validFieldName = "#validFieldName#" countryID = #form.insCountryID#>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#validvalues#" display="displayvalue" value="dataValue" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
								<cfelse>
									You must define a validFieldName for a validValueList form element type.
								</cfif>
							</cfcase>

							<cfcase value="countryList">
								<cfquery name="getCountries" datasource="#application.siteDataSource#">
									SELECT DISTINCT CountryID, CountryDescription
										  FROM Country
										  where ISOCode is not null
									ORDER BY CountryDescription ASC
								</cfquery>
								<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#form.insCountryID#" fieldName="insCountryID" size="1" query="#getCountries#" display="CountryDescription" value="CountryID" label="phr_Ext_Country" disabled="Yes" tabindex="#tabIndex#">
								<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
								</cfcase>

							<cfcase value="email">
								<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="insEmail" label="#thisfieldlabel#" validate="email" onValidate="validateEmail" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" personid=0 tabindex="#tabIndex#" orgType="#attributes.insorganisationTypeID#">
							</cfcase>

							<cfcase value="gender">
								<cf_querySim>
									getStati
									display,value
									Phr_male|M
									Phr_female|F
								</cf_querySim>
								<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" fieldName="insSex" label="#thisfieldlabel#" query="#getStati#" display="display" value="value" tabindex="#tabIndex#">
							</cfcase>

							<cfcase value="dateOfBirth">
								<cfSaveContent variable="DOBDataHTML">
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBDay" size="1" message="Enter Your Day of Birth" required="#isMandatory#" tabindex="#tabIndex#">
											<option value="" SELECTED>Day</option>
										<cfloop index="i" from="1" to="31" step="1">
											<option value="#i#" >#htmleditformat(i)#</option>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBMonth" size="1"  message="Enter Your Month of Birth" required="#isMandatory#" tabindex="#tabIndex#">
									<cfset j = 1>
											<option value="" >Month</option>
										<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
											<option value="#j#" >#htmleditformat(i)#</option>
											<cfset j = j + 1>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfinput type="Text" name="insDOBYear" range="1930,1988" message="Enter Your Year of Birth (between 1930-1988)" required="#isMandatory#" size="4" tabindex="#tabIndex#">
								</cfsavecontent>
								<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="HTML" label="#thisfieldlabel#" currentValue="#DOBDataHTML#" tabindex="#tabIndex#">
							</cfcase>

						</cfswitch>
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="" fieldname="#qRelayFieldList.fieldName#">
				</cfif>
				</cfif>
			<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
			<cfset tabIndex = tabIndex + 1>
			<!--- END:  2009-08-26 LHID2527 --->
			</cfloop>
		</cfif>
		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">
		</cfoutput>
				<cfif isdefined("inslocationID")>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inslocationID#" fieldname="inslocationID">
				<cfelse>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS1#" fieldname="INSADDRESS1">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS2#" fieldname="INSADDRESS2">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS3#" fieldname="INSADDRESS3">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS4#" fieldname="INSADDRESS4">
					<!--- 2007/11/23 GCC Not always used in address capture --->
					<cfif isdefined("INSADDRESS5")>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSADDRESS5#" fieldname="INSADDRESS5">
					</cfif>
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSPOSTALCODE#" fieldname="INSPOSTALCODE">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSTELEPHONE#" fieldname="INSTELEPHONE">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#INSFAX#" fieldname="INSFAX">
				</cfif>
				<cfif isDefined("attributes.showIntroText")>
		<!--- CONTROL IntroText in relayTags/addCOntactForm --->
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.showIntroText#" fieldname="showIntroText">
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#" fieldname="frmNext">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmDebug#" fieldname="frmDebug">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmReturnMessage#" fieldname="frmReturnMessage">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.flagStructure#" fieldname="flagStructure">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmShowLanguageID#" fieldname="insLanguage">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insOrganisationID#" fieldname="insOrganisationID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#insSiteName#" fieldname="insSiteName">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmMagicNumber#" fieldname="frmMagicNumber">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.magicNumberEntityTypeID#" fieldname="magicNumberEntityTypeID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmshowlanguageID#" fieldname="frmshowlanguageID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#inscountryID#" fieldname="inscountryID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#SelectedCountry#" fieldname="SelectedCountry">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#nextstep#" fieldname="nextstep">
				<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactSaveButton" currentValue="phr_continue" label="" spanCols="No" valueAlign="left" class="button" tabindex="#tabIndex#">
		</cf_relayFormDisplay>
		</cfform>
	</cfif>
	<cfif #nextstep# EQ 6>
		<!--- Complete form just add country this form is displayed --->
		<cfparam name="form.insCountryID" default="#attributes.defaultCountryID#">
		<cfset countryDetails = application.com.commonQueries.getCountry(countryid=form.insCountryID)>

		<cfparam name="fieldSize" default="25" type="numeric">
		<cfparam name="maxFieldSize" default="30" type="numeric">
		<cfparam name="attributes.hideCompanyName" default="No" type="boolean">

		<!--- 2010/03/24	Start	NAS LID - 3155 - Captcha issue --->
		<cfif attributes.showcaptcha>
			<cfset submitfunction = "return validateCaptcha();">
		<cfelse>
			<cfset submitfunction = "">
		</cfif>
		<!--- 2010/03/24	Stop	NAS LID - 3155 - Captcha issue --->

		<!--- 2010/03/24	NAS LID - 3155 - Captcha issue --->
		<cfform action="" method="POST" name="details"  onsubmit="#submitfunction#">

		<cf_relayFormDisplay class="#attributes.relayFormDisplayClass#">

		<cfoutput>
		<!--- <cf_encryptHiddenFields> --->

		<cfif isDefined("attributes.showIntroText")>
		<!--- CONTROL IntroText in relayTags/addCOntactForm --->
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.showIntroText#" fieldname="showIntroText">
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmNext#" fieldname="frmNext">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmDebug#" fieldname="frmDebug">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.frmReturnMessage#" fieldname="frmReturnMessage">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.flagStructure#" fieldname="flagStructure">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.frmShowLanguageID#" fieldname="insLanguage">

		<cfif isDefined("qRelayFieldList") and isQuery(qRelayFieldList)>
			<cfif countryDetails.VATrequired>
				<cfset attributes.mandatoryCols = listAppend(attributes.mandatoryCols,"insVATnumber")>
			</cfif>
			<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
			<cfset tabIndex = 1>
			<!--- END:  2009-08-26 LHID2527 --->
			<cfloop query="qRelayFieldList">
				<!--- get any name value pairs defined in params and make them available to this scope --->
				<CF_EvaluateNameValuePair NameValuePairs = "#qRelayFieldList.params#">

				<cfif listfindNocase(attributes.showCols,qRelayFieldList.fieldName) neq 0>
					<cfif listfindNocase(attributes.mandatoryCols,qRelayFieldList.fieldName) neq 0>
						<cfset isMandatory = "Yes">
					<cfelse>
						<cfset isMandatory = "No">
					</cfif>
					<cfif not (fieldName eq "insSiteName" and attributes.hideCompanyName eq "yes")>

						<cfif left(defaultFieldLabel,3) is "phr">
							<cfset thisfieldlabel = defaultFieldLabel>
						<!--- If translation returns 'no translation' or the returned phrase isn't different to the sent phrase don't use it --->
						<!--- <cfelseif findNoCase("no translation","phr_#qRelayFieldList.fieldName#",1) eq 0 AND qRelayFieldList.fieldName neq trim(evaluate("phr_#qRelayFieldList.fieldName#"))>
							<cfset thisfieldlabel = evaluate("phr_#qRelayFieldList.fieldName#")> --->

						<!--- NJH 2010/10/14 I have commented out the above condition after Will had made his translation changes. I have hoped to preserve the logic --->
						<cfelseif application.com.relayTranslations.doesPhraseTextIDExist("phr_#qRelayFieldList.fieldName#") and qRelayFieldList.fieldName neq application.com.relayTranslations.translatePhrase(phrase="phr_#qRelayFieldList.fieldName#")>
							<cfset thisFieldLabel="phr_#qRelayFieldList.fieldName#">
						<cfelse>
							<cfset thisfieldlabel = qRelayFieldList.defaultFieldLabel>
						</cfif>

						<!--- NJH 2009/07/08 P-FNL069 - added regex pattern checking for text input fields --->
						<cfset pattern = "">
						<cfset validate = "">
						<cfif qRelayFieldList.regexPattern neq "">
							<cfset pattern = qRelayFieldList.regexPattern>
							<cfset validate = "regex">
						</cfif>

						<!--- NJH 2009/07/27 Captcha prototype --->
						<cfset fieldValue = "">
						<cfif isDefined("failedCaptcha") and failedCaptcha>
							<cfif structKeyExists(form,fieldName)>
								<cfset fieldValue = evaluate("form."&fieldName)>
							</cfif>
						</cfif>

						<cfswitch expression="#qRelayFieldList.type#">
							<cfcase value="textInput">
								<!--- 2006/06/19 P_SNY039 GCC - this would be easier if this form was converted to use CF_RelayFormDisplay but this is good for now--->
									<cfif fieldName eq "insVATnumber">
										<!--- 2006/07/08 GCC Sony CR - only ask for VAT number if it is required --->
										<!--- 2013-04-04 NYB Case 433821 - show this even if not required - using getSetting("plo.hideVATNumberFieldWhenNotMandatory") --->
										<cfif (application.com.settings.getSetting("plo.hideVATNumberFieldWhenNotMandatory") and isMandatory eq 1) or not application.com.settings.getSetting("plo.hideVATNumberFieldWhenNotMandatory")>
											<cfset VATvalidate = "">
											<cfset VATpattern = "">
											<cfif countryDetails.VATformat neq "">
												<cfset VATvalidate = "regular_expression">
												<cfset VATpattern = countryDetails.VATformat>
											</cfif>
											<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#VATvalidate#" pattern="#VATpattern#">
										</cfif>
									<cfelseif fieldName eq "insTelephone"  or fieldName eq "insMobilePhone" or fieldName eq "insFax" >
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#">

									<cfelseif  fieldName eq "insOfficePhone" >
										<!--- pkp 2008-04-22 adding switch to turn off and on the ext box need to set hideDirectLineExtension = true or false--->
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#" noTR="#iif(not attributes.hideDirectLineExtension,de('START'),de(''))#">
										<cfif not attributes.hideDirectLineExtension>
											<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insOfficePhoneExt" currentValue="#fieldValue#" label="Phr_Ext_DirectLineExt" size="10" maxlength="10" tabindex="#tabIndex#"  mask="99999" noTR="END">
										</cfif>
									<cfelse>
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="#fieldValue#" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#validate#" pattern="#pattern#">
									</cfif>
								</cfcase>
							<cfcase value="Province">
								<cfquery name="GetFullNameSwitch" datasource="#application.siteDataSource#">
									select ProvinceAbbreviation
									from country
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfquery>
								<cfquery name="getProvince" datasource="#application.siteDataSource#">
									select p.name, p.Abbreviation
									from Province p
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
									order by p.name
								</cfquery>
								<cfif getProvince.recordcount EQ 0>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
								<cfelse>
									<!--- PPB 2008-08-11 added 'Select A Value' to dropdown --->
									<cfif GetFullNameSwitch.ProvinceAbbreviation EQ "" or GetFullNameSwitch.ProvinceAbbreviation EQ "false">
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="name" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
									<cfelse>
										<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="Abbreviation" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
									</cfif>
								</cfif>
							</cfcase>
							<!--- NJH 2007/11/23 added region used in a partner locator... this is tied to address10 --->
							<cfcase value="Region">
								<cfquery name="getRegion" datasource="#application.siteDataSource#">
									select r.name
									from Region r
									where countryID =  <cf_queryparam value="#form.insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
									order by r.name
								</cfquery>
								<cfif getRegion.recordcount EQ 0>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<cf_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
								<cfelse>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<cf_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getRegion#" display="name" value="name" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
								</cfif>
							</cfcase>
							<cfcase value="validValueList">
								<cfif isDefined("validFieldName")>
									<cf_getValidValues validFieldName = "#validFieldName#" countryID = #form.insCountryID#>
									<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
									<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#fieldValue#" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#validvalues#" display="displayvalue" value="dataValue" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
								<cfelse>
									You must define a validFieldName for a validValueList form element type.
								</cfif>
							</cfcase>

							<cfcase value="countryList">
								<cfquery name="getCountries" datasource="#application.siteDataSource#">
									SELECT DISTINCT CountryID, CountryDescription
										  FROM Country
										  where ISOCode is not null
									ORDER BY CountryDescription ASC
								</cfquery>
								<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
								<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#form.insCountryID#" fieldName="insCountryID" size="1" query="#getCountries#" display="CountryDescription" value="CountryID" label="phr_Ext_Country" disabled="Yes" tabindex="#tabIndex#">
								<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#form.insCountryID#" fieldname="insCountryID">
								</cfcase>

							<cfcase value="email">
								<!--- NYB 2009-08-26 LHID2527 - replaced tabindex="8" with tabindex="#tabIndex#" : --->
								<!--- NYB 2012-01-17 Case425166 replaced required="yes" with required="#isMandatory#": --->

								<!--- 2015/02/03 YMA P-ARB001 Dont display unique email message if the org type is end customer. --->
								<cfset orgType=attributes.insorganisationTypeID>
								<cfif isNumeric(orgType)>
									<cfset orgType = application.organisationType[orgType].TYPETEXTID>		<!--- CASE 426500 NJH 2012/03/08 - val'd the orgTypeTextId because it could be 1.0 (from the webservice) ; note: val() removed for PartnerCloud but don't know if deliberate --->
								</cfif>

								<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#fieldValue#" fieldName="insEmail" label="#thisfieldlabel#" validate="email" onValidate="validateEmail" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#" personid = "0" tabindex="#tabIndex#" orgType="#attributes.insorganisationTypeID#">
								<!--- 2015/02/03 YMA P-ARB001 Dont display unique email message if the org type is end customer. --->
								<cfif application.com.relayPLO.isUniqueEmailValidationOn().isOn and orgType neq "EndCustomer">
									<CF_relayFormElementDisplay relayFormElementType="html" label= "" currentvalue = "phr_sys_formValidation_UniqueEmailOnScreenLabel" spancols=true>
								</cfif>
							</cfcase>

							<cfcase value="gender">
								<cf_querySim>
									getStati
									display,value
									Phr_male|M
									Phr_female|F
								</cf_querySim>
								<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#fieldValue#" fieldName="insSex" label="#thisfieldlabel#" query="#getStati#" display="display" value="value" tabindex="8">
							</cfcase>

							<cfcase value="dateOfBirth">
								<cfSaveContent variable="DOBDataHTML">
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBDay" size="1" message="Enter Your Day of Birth" required="#isMandatory#" tabindex="#tabIndex#">
											<option value="" SELECTED>Day</option>
										<cfloop index="i" from="1" to="31" step="1">
											<option value="#i#" >#htmleditformat(i)#</option>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfselect name="insDOBMonth" size="1"  message="Enter Your Month of Birth" required="#isMandatory#" tabindex="#tabIndex#">
									<cfset j = 1>
											<option value="" >Month</option>
										<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
											<option value="#j#" >#htmleditformat(i)#</option>
											<cfset j = j + 1>
										</cfloop>
									</cfselect>
									<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
									<cfinput type="Text" name="insDOBYear" range="1930,1988" message="Enter Your Year of Birth (between 1930-1988)" required="#isMandatory#" size="4" tabindex="#tabIndex#">
								</cfsavecontent>
								<CF_relayFormElementDisplay relayFormElementType="HTML" label="#thisfieldlabel#" currentValue="#DOBDataHTML#">
							</cfcase>

						</cfswitch>
					<cfelse>
						<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#fieldValue#" fieldname="#qRelayFieldList.fieldName#">
					</cfif>
				</cfif>
				<!--- START:  NYB 2009-08-26 LHID2527 - added: --->
				<cfset tabIndex = tabIndex + 1>
				<!--- END:  2009-08-26 LHID2527 --->
			</cfloop>
		</cfif>

		<!--- NJH 2009/07/20 P-FNL069 captcha prototyping --->
		<cfif attributes.showCaptcha>
		<tr>
			<td colspan="2">Phr_Ext_Captcha_instructions</td>
		</tr>
		<tr>
			<td valign="bottom">&nbsp;</td>
			<td>
			<div id="captchadiv">
				<cfset captchaHTML = application.com.commonQueries.drawCaptcha()>
				#captchaHTML#
			</div>
			</td>
		</tr>
 		</cfif>
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#attributes.insorganisationTypeID#" fieldname="insorganisationTypeID">
		<!--- NYB 2009-08-26 LHID2527 - added tabindex="#tabIndex#" : --->
		<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addContactSaveButton" currentValue="phr_continue" label="" spanCols="No" valueAlign="left" class="button" tabindex="#tabIndex#">
		<CF_relayFormElementDisplay relayFormElementType="MESSAGE" label="" spanCols="Yes" currentValue="phr_mandatoryFields">
		<!--- </cf_encryptHiddenFields> --->
		</cfoutput>
		</cf_relayFormDisplay>
		</cfform>

<!--- 2011/01/11 GCC - dodgy workaround for issue with CF that a required select cannot be focussed to after applying masks
hard coded attempt to focus to insSalutation at the end - if insSalutation is not in the form it will focus on the first field (if it is not another select of course!)
 --->

<cfoutput>
<script  type="text/javascript">
if( document.getElementById && document.getElementById('insSalutation'))
{
    // set focus to first field in the form
    document.getElementById('insSalutation').focus();

}
if( document.all && document.all['insSalutation'])
{
    // set focus to first field in the form
   document.all['insSalutation'].focus();
}
</script>
</cfoutput>


	<!--- 2006-05-09 AJC P_COR001 CountryID now has to be selected first --->
	</cfif>
</cfif>

<cfsetting enablecfoutputonly="No">

