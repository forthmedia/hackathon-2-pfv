<cfif request.relayCurrentUser.isInternal> <!--- 2014-08-12	AXA	changes to allow forms to be displayed on partner portal. --->
	<cf_relayNavMenu pageTitle="#pageTitle#">

		<!--- 2014-03-18 YMA Case 435852 Track Module and Courseware history - Intention in later Relayware versions to roll out modification history to all editors perhaps within the editor itself rather than the top head. --->
		<cf_include template="/code/cftemplates/editorTopHeadExtension.cfm" checkIfExists="true">

		<cfif not readOnlyRecord>
			<cfif showSave>
				<cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="FormSubmit(this);" id="SaveBtn">
			</cfif>
			<cfif showSaveAndReturn>
				<cf_relayNavMenuItem MenuItemText="phr_sys_saveAndReturn" CFTemplate="Javascript:jsSaveAndReturn();">
			</cfif>

			<cfif showSaveAndAddNew>
				<cf_relayNavMenuItem MenuItemText="phr_sys_SaveAndAddNew" CFTemplate="Javascript:jsSaveAndAddNew();">
			</cfif>
		</cfif>
		<cfif not hideBackButton>
			<cf_relayNavMenuItem MenuItemText="phr_Back" CFTemplate="#BackForm#">
		</cfif>
	</cf_relayNavMenu>
</cfif> <!--- 2014-08-12	AXA	changes to allow forms to be displayed on partner portal. --->