<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="attributes.dir" type="string">
<cfparam name="attributes.filename" type="string">
<cfparam name="attributes.endRowDelim" type="string" default="#chr(13)##chr(10)#">
<cfparam name="attributes.delim" type="string" default="#application.delim1#">
<cfparam name="attributes.tablename" type="string">
<cfparam name="attributes.tableOptions" type="string" default="">
<cfparam name="attributes.columnNames" type="string">
<cfparam name="attributes.numberElementsExpected" type="numeric">
<cfparam name="attributes.sourceCollectionMethod" type="string" default="absolutePath">
<cfparam name="attributes.filterWhereClause" type="string" default="">


<cfsetting requesttimeout="6000" >

<cfscript>
		importHeader 										= structnew();
		request.importHeader.dir 							= attributes.dir;
		request.importheader.filename						= attributes.filename;
		request.importheader.EndRowDelim					= attributes.EndRowDelim;
		request.importheader.Delim							= attributes.Delim;
		request.importheader.Tablename						= attributes.Tablename;
		request.importheader.Tableoptions					= attributes.Tableoptions;
		request.importheader.ColumnNames    				= attributes.ColumnNames;  
		request.importheader.NumberElementsExpected			= attributes.NumberElementsExpected;
		request.importheader.sourceCollectionMethod			= attributes.sourceCollectionMethod;
		request.importheader.filterWhereClause				= attributes.filterWhereClause;
</cfscript>

<cf_querysim>
	csvcomponent
	tableColumnName,filePosition		
	<CFOUTPUT>#htmleditformat(attributes.fieldnames)#</CFOUTPUT>
</cf_querysim> 

<cfif CompareNoCase("delete", request.importheader.Tableoptions) EQ 0>
	<cfset deleterecords = application.com.dbtools.DeleteRecords(tablename=request.importheader.Tablename)>
<cfelseif CompareNoCase("truncate", request.importheader.Tableoptions) EQ 0>
	<cfset Truncaterecords = application.com.dbtools.TruncateRecords(tablename=request.importheader.Tablename)>
</cfif>

	<cfset indexCounter = 0>
	<cfset importprocess = "">
	<cfset importLost = "">

<!--- blindingly fast for very large files (> 2mB) but only works for perfectly formed files - will throw an error if any rows are incomplete or too long --->
<!--- request.importHeader.dir needs to be the path from webroot to the file e.g. /content/cftemplates/--->
<cfif request.importheader.sourceCollectionMethod eq "http">
	<cftry>

		<cfhttp
		 url="#request.currentSite.httpProtocol##request.currentsite.domain##request.importHeader.dir##request.importheader.filename#"
		 method="GET"
		 name="importprocess"
		 delimiter="#request.importheader.Delim#"
		 textqualifier=""
		 firstrowasheaders="yes"
		 columns="#valuelist(csvcomponent.tableColumnName)#"
		 charset=""
		 />
		 
		 <cfif request.importheader.filterWhereClause neq "">
			 <cfquery name="importprocess" dbtype="query">
			 	select * from importprocess where #request.importheader.filterWhereClause#
			</cfquery>		 
		 </cfif>
		 
		<cfcatch type="any">
			<cfmail from="errors@foundation-network.com" 
					to = "errors@foundation-network.com"
					subject="Relaytocsv http section has fallen over"
					type="html">
						<cfdump var="#cfcatch#">			
			</cfmail>
		</cfcatch>
	</cftry>	
		<cfset x = putData()>
	
		<!--- more efficient than single inserts but will get stack overflow if this number is too large
		- recursive loop to reduce the number of insert statements we fire at the database --->
		<cffunction name="putData">
			<cfargument name="queryStartRow" default="1">
			<cfargument name="increment" default="99">
			
				<cfscript>
					var insertcsv = "";
					var queryEndRow = "";
					var newstartrow = "";
				</cfscript>
	
			<cfif importprocess.recordcount lt arguments.queryStartRow + arguments.increment>
				<cfset arguments.increment = importprocess.recordcount - arguments.queryStartRow>
			</cfif>
			<cfset queryEndRow = arguments.queryStartRow + arguments.increment>		
			
			<cfquery name="insertcsv" datasource="#application.sitedatasource#">
			   INSERT INTO #request.importheader.Tablename# (#valuelist(csvcomponent.tableColumnName)#)
			   <cfloop query="importprocess" startrow = "#queryStartRow#" endrow = "#queryEndRow#">
			      SELECT
					<cfloop list="#valuelist(csvcomponent.tableColumnName)#" index="myColumnName">
						'#evaluate(myColumnName)#'<cfif listFindNoCase(#valuelist(csvcomponent.tableColumnName)#,#myColumnName#) neq #request.importheader.NumberElementsExpected#>,</cfif>
					</cfloop>
			      <cfif currentrow LT queryEndRow>UNION ALL</cfif>
			   </cfloop>
			</cfquery>
			
			<cfset newstartrow = queryEndRow+1>
			<cfif importprocess.recordcount gt newstartrow>
				<cfreturn putData(queryStartRow=newstartrow,increment=arguments.increment)>
			<cfelse>
				<cfreturn>
			</cfif>	
		</cffunction>

<cfelse>

	<!--- 2008/03/31 GCC now done using buffer reader for performace reasons --->

	<cffile action="READ" file="#request.importHeader.dir##request.importheader.filename#" variable="csvimport">

	<cfloop index="logRow" list="#csvimport#" delimiters="#request.importheader.EndRowDelim#">
		<cfset logRow = '^' & Replace(logRow, "#request.importheader.Delim#", "#request.importheader.Delim#^", "ALL")>
		<cfif #listLen(logRow,request.importheader.Delim)# eq #request.importheader.NumberElementsExpected#>
			<cfset importprocess = importprocess & logRow & request.importheader.EndRowDelim>
		<cfelse>
			<cfset request.importLost = importLost & logrow & request.importheader.EndRowDelim>
		</cfif>
	</cfloop>
	
	<cfloop index="logRowProcess" list="#importprocess#" delimiters="#request.importheader.EndRowDelim#">
		<cfset rowArray = listToArray (logRowProcess,#request.importheader.Delim#)>
		<cfset indexCounter = indexCounter + 1>
			<cfif not (request.importheader.ColumnNames and indexCounter eq 1)>
				<cfquery name="insertcsv" datasource="#application.siteDataSource#">
					INSERT into  #request.importheader.Tablename# (
						<cfloop index="index" from="1" to="#csvcomponent.recordcount#"step="1">
							<cfif index NEQ 1>,</cfif>
							#csvcomponent["tableColumnName"][index]#
						</cfloop>)
						
					values(
					<cfloop index="index" from="1" to="#csvcomponent.recordcount#" step="1">
						<cfset tempValue = rowArray[csvcomponent["fileposition"][index]]>
						<cfset tempValue = removeChars(tempValue,1,1)>
							<cfif index NEQ 1>,</cfif>
							<cf_queryparam value="#tempValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfloop>
					)
				</cfquery>
			</cfif>
	</cfloop>

</cfif>
