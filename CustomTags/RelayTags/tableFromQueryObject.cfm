﻿<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">
<!---
File name:		tableFromQueryObject.cfm
Author:			SWJ
Date started:	2003-01-18
Description:	This provides a standard relay Table when passed a query object.
				There is detailed documentation at
				http://fnl-europe/devdocs/relayWare_help/tableFromQueryObject.htm

Usage:
<CF_tableFromQueryObject
	queryObject="#queryObject#"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#
	numRowsPerPage="#numRowsToReturn#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#" insert the text "thisurlkey" for substitution, otherwise it is appended as before
	keyColumnKeyList="opportunity_ID"
	keyColumnTargetList="mainFrame"
	keyColumnOpenInWindowList="yes" use yes and no to define
	keyColumnIconList="images/myImage.gif" use null if no image required in list
	keyColumnOnClickList=""
	thisKeyColumnLinkDisplay="" link or button

	OpenWinNameList 	- a comma delimited list of names for windows to be openned - use "no" when a link is not openning
							in a new window or when it is but you want the default ="MyWindow"
	OpenWinSettingsList	- a colon delimited list of window settings which are themselves comma delimited - use "no" as above

	hideTheseColumns="#leadAndOppListHideTheColumns#"
	showTheseColumns="#leadAndOppListShowTheColumns#" optionally overrides the query column list and display order

	allowColumnSorting="yes"
	currencyFormat="budget"
	dateFormat="#dateFormat#" List of Columns that need date formating
	dateTimeFormat="#dateFormat#" List of Columns that need datetime formating
	numberFormat="#numberFormat#" List of Columns that need number formatting
	numberFormatMask="#numberFormatMask#" - list of mask descriptions to apply - must be one per number format column if passed
											allowed format descriptions:	decimal (thousand separators and 2 decimal places)
																			default (thousand separators and rounded up to integer with no decimal places)
	BooleanFormat="yes"
	FilterSelectFieldList="#leadAndOppListFilterSelectColumnList#"
	FilterSelectFieldList2="#leadAndOppListFilterSelectColumnList#"

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"

	queryWhereClauseStructure="#queryWhereClause#"

	totalTheseColumns="" - comma delim list of column names to be totalled (e.g. a,b,c,d)
	averageTheseColumns="" - comma delim list of column names to be averaged (columns must also be in totalTheseColumns - Should consider rename to calculateTheseColumns e.g. a,b,c)
	averageColumnsToUse="" - comma delim list of pipe delimited column name pairs (numerator|denominator) to use for sub-total and total % difference calculation (need an entry for every column in averageTheseColumns e.g. to use e/f*100 for cols a and c above e|f,,e|f)
	GroupByColumns=""

	startRow="#startRow#"
	passThroughVariablesStructure = ""

	rowIdentityColumnName="PersonID"
	functionListQuery="#qFunctionList#"

	searchColumnList="opportunity_id,description"
	searchColumnDataTypeList="1,0"	user-defined data types, e.g. 1 = integer, 0 = string
	searchColumnDisplayList="Phr_opportunityID,Phr_description"

	alphabeticalIndexColumn="Account"

	columnTranslation="true"    This translates column headings
	ColumnTranslationPrefix="default is phr_Sys_Report_"
	ColumnHeadingList="Replaces column headings"
	showCellColumnHeadings="yes" floats a mouseover div of the column heading
	selectbyday = "true"

	IllustrativePriceFromCurrencyColumn=""	This is a column name that holds the currency iso code to convert from

	translateTheseColumns = ""   pre-translates content of a column

	toolTipColumnList=""			- comma delim list of column names to have a tooltip
	toolTipContentColumnList=""		- comma delim list of column names which hold the content to be displayed in the tooltip
>

	functionListQuery needs to be a query object

Eample syntax for queryWhereClause
<cfscript>

queryWhereClause = StructNew();
// select 1
queryWhereClause["1"] = StructNew();
queryWhereClause["1"]["1"] = StructNew(); // item 1
queryWhereClause["1"]["1"]["title"] = "Clauses";
queryWhereClause["1"]["1"]["sql"] = "";
queryWhereClause["1"]["2"] = StructNew(); // item 2
queryWhereClause["1"]["2"]["title"] = "Job Description not null";
queryWhereClause["1"]["2"]["sql"] = "p.jobDesc is not null";
queryWhereClause["1"]["3"] = StructNew(); // item 3
queryWhereClause["1"]["3"]["title"] = "Job Description null";
queryWhereClause["1"]["3"]["sql"] = "p.jobDesc is null";
// select 2
queryWhereClause["2"] = StructNew();
queryWhereClause["2"]["1"] = StructNew(); // item 1
queryWhereClause["2"]["1"]["title"] = "Mores clauses";
queryWhereClause["2"]["1"]["sql"] = "";
queryWhereClause["2"]["2"] = StructNew(); // item 2
queryWhereClause["2"]["2"]["title"] = "Job Description null";
queryWhereClause["2"]["2"]["sql"] = "p.jobDesc is null";

</cfscript>


Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
14-Aug-2003			KAP			added passThroughVariablesStructure attribute to propagate form fields through filtering requests
2003-08-24			SWJ			Added showFunctionList
05-Nov-2003			KAP			changes to paging and filtering
08 October 2004		NM			over the past month:
								Grouping fixed with maxrows. use of incr var to control displayed rows.
								Subtotaling and totaling in grouping done on a page by page basis with summary results on final page.
								Summary results also on "view all" page.
								Target added to link. use keyColumnTargetList to define.
								Ability to open link in a popup added. use keyColumnOpenInWindowList to define. either "yes" or "no" value required for each item in the keyColumnURLList.
								Add image as link added. use keyColumnIconList to define. specify either image path from / or null if no image required for an item in keyColumnURLList.
								ability to add more than one url param to url.
									usage:
										keyColumnURLList="myPage.cfm?myFirstUrlParam=|mySecondUrlParam=,mySecondPage?secondLinkUrlParam="
										keyColumnKeyList="myFirstUrlParamInQuery|mySecondUrlParamInQuery,secondLinkUrlParamInQuery"
									main list of urls and keys is delimited as usual by comma(,)
									sub-delimiting used in order to add multiple url params based on multiple items in query.
									use of pipe (|) for sub-delimiting.
									length of list in url and key items must match, otherwise defaults to only use the first item in each list.
									for each extra url item, delimit with pipe. e.g. myPage.cfm?myFirstUrlParam=|mySecondUrlParam= and so forth
									for each extra url item, make sure to add corresponding key e.g. myFirstUrlParamInQuery|mySecondUrlParamInQuery
21 March 05			GCC			Added popupDescription include and showCellColumnHeadings attribute to
22 3 05				WAB			Put in some code I had developed earlier - corrected problem with 2nd and third drop downs.
05 May 2005			DAM			Added 2 new attributes OpenWinNameList and OpenWinSettingsList. These are used when a keyColumn link
								is openned in a new window. Added relevant comments above.
24 May 2005			AJC			Added ColumnTranslationPrefix - This is due to leads and opps functionality being used multiple times and in different ways on an installation... Included for Sony VIP
10 Aug 2005			AJC			Removed the mysterious ****** which appeared everywhere
2005-10-11			WAB			added attribute ColmunHeadingList
2005-10-26			WAB			added automatic detection of url or form variables startRow and numRowsOnPage
2005-11-01			WAB			came across problem with keyColumns when using a groupby.  copied a bit of code from the non group by part of the code
2005-11-07			WAb			Increased max rows on excel download from 2047 to 999999.  Not sure why was set to 2047 - it just needs some number for the cfquery to work
2006-05-02			AJC			Added attribute ColmunHeadingList to above list
2006-05-03			AJC			Applied ColumnHeadingList to the filter list also
2006-11-21 			WAB			hidethesecolumns wasn't working  (headings disappearing but not data columns)
2006-12-13			WAB			problem with having more than one (large) tables on a page and needing to navigate through the tables - javascript got confused.  Had to uniquely define the forms.  Note that if you go to record x on table one then also go to record x on table 2!
2007-03-16			SSS			Problem with filter options not Separating changed a replace for a listchangedelims function. Date and name is above the change.
2007-08-03			SSS			There need to be days added to this so that people could select days to search on as well as months.
2007-09-03			SWJ
2007-10-29			SSS			Made a change to the code that opens a window so that it a object is not passed back we will not loose the search results.
2008/03/25			WAB			added FF support for context menus
2008/04/15			GCC			Extended with new attribute numberFormatMask to enable masks to be applied to numberFormat columns for decimals etc.
2008/04/15			WAB/PKP		Problem with radio filters not getting propagated properly, needed to be added to the sortForm.  Also problem with how additionalArgsStructure was being created (contaminating URL)
2008-05-15			GCC			Added averageColumnsToUse to enable an average to be taken based on 2 other columns subtotals/totals rather than an average of the values in the column itself.
								Very flaky! All cols used must be numberFormat, col to fudge the average for must be rendered after the 2 cols.
								Param is a comma delimited list for each value in averageTheseColumns and with that an emtpy string for do nothing or a | delim column pair - the first being the numerator and the second the denominator for the average calculation.
2008/05/21			NJH			Added currencyLocale which tells the displayer to display the any values in the columns specified in the currencyFormat attribute in a particular locale.
2008-06-12 			NYF 		for CR-LEN548, added the option of a Label to the data filter, passed into tag by attribute queryWhereClauseLabel which defaults to ""
2008/06/18 			GCC 		Reworked showCellColumnHeadings code to work much more efficiently and to use Qtip.js instead of popUpDescription.js
2008/07/02			WAB			Added keyColumnOnClickList.  Note that variables are merged into this field just by embedding cfvariables into the string (use two #s).  Use *comma for literal commas
2008-07-07 			GCC			This comment added by another concientious programmer who noticed that Claridge had done some work and did not add a comment to the header
								He did some work on this file to correct work done on 2008/06/18 which didn't work when columns were not translated were
2008-07-08			AJC			Added openAsExceljs() javascript function. This fixes issues previously had when passing the filter form variables via the url. Now, just have "javascript:openAsExceljs()" as the href.
2008/07/09			WAB			Further bug in showCellColumnHeadings when opening in excel and translation On.  Turned out that excel headings were not being translated.
2009-02-09			NJH			Added BooleanFormat - displays a "yes" or "no" value
2009-02-09			SSS        took out a <cfif so that team selection can be reset
2009/04/16 			WAB 		OpenAsExcel Javascript moved and put in a cfif so does not appear when tfqo is generating an excel file
2009/04/20			NJH			Bug Fix All Sites Issue 2072 - if in group by mode and opening as excel, force all output as text, rather than links, etc.
2009/05/12			NJH			CR-SNY675-1 - hideColumns when column in hideTheseColumns and use translated column heading when in excel
2009/05/20			WAB			Added attributes.translateTheseColumns to pretranslate columns.  Also update to customTags\translateQueryColumn.cfm
2009/07/20			SSS			P-fnl069 encrypt parameters in links
2009/09/14 			WAB 		LID 2614. don't urlencode parameter which are about to be encrypted, also added use of encryptURL function
2009/09/21 			WAB 		LID 2618  Problem downloading to excel - blank reports.  Tracked down to CFCONTENT nested insidde CF_TRANSLATE
							Also added attributes.excelFileName parameter
2010/02/16			WAB		Added an id attribute to each row (based on the rowIdentityColumn field)
							Added columnname attribute to the first row of the table - can then be used for updating individual cells
2010/03/09			NJH			P-PAN002 - added support for rollup reporting. Added new attribute called 'IllustrativePriceFromCurrencyColumn' which holds the column name for the currency to convert from. If this
								is passed in, it it used; otherwise IllustrativePriceFromCurrency is used as per normal.
2010/03/15			WAB		bug fix to change above
2010/04/13			NAS			P-PAN002 - if 'IllustrativePriceFromCurrencyColumn' is being passed in the 'Grand Total' for this column is supressed.
2010/04/21			NAS			P-PAN002 - Add two new variables "PriceColumns" and "PriceFromCurrencyColumn" so local currency can be used on amounts
2010/04/28			NJH			P-PAN002 added new variable 'KeyColumnLinkDisplayList' which determines how to display a link
2010/07/09			PPB			P-PAN002 show/hide the buttons/links on a row based on query data (eg if button query column = 'RenewButton', return another column called 'showRenewButton' to dictate whether the button is visible)
2010/07/22			PPB			P-PAN002 added code to handle PriceColumns/PriceFromCurrencyColumn for a GROUPED report and extend TFQO to allow 2 group by columns
2010/07/30			NJH			P-PAN002 - cleaned up code for rollup reporting. Commented out code for IllustrativePriceFromCurrencyColumn as it wasn't finished, and it wasn't being used at the moment, although it might be in the future.. I doubt it however..
2010/08/04			NJH			P-PAN002 - added exchangeRateExists variable, so that if an exchange rate doesn't exist for a currency, then the numbers aren't totalled
2010/09/06			WAB		 LID 2481 & 2499 when using cf_translateQueryColumn, pass in the start and end rows (needed a bit of code reordering to do this)
2010/09/23			PPB		P-LEN022 LID 4116 if showing a boolean format column then the filters should display Yes/No not 1/0
2010/10/13			NAS		LID4317: Teams - Lead Passing listing no filter possible
2010/11/10			NAS		LID4706: CR015.2 : Performance/Usability CR
2011/02/27			AWJR		P-LEN023 allow bogusForm to have a different name
2011-03-03 			NYB		LHID4944: added 'replace " with -' to id entry of TR in the main body - to stop the layout being thrown out by entries with with "'s in them
2011/03/07			WAB		LHID4944  I think that it would be better to replace " with &quot;
2011/03/16			MS			P-SNY107 Added support for timeformat - especially when exporting to excel
2011/05/18			WAB		Javascript Problems because the dummy <form> is inside the <table>
							Had to move the whole sortForm outside of the table and move the dummy form to around the main Table
2011/08/05 			PPB 	LID7090 added LSParseCurrency() to strip off currency formatting from x:num
2011/08/10 			PPB 	LID7090 save cellValue to use as x:num cos LSParseCurrency() works on current locale only
2011-09-16 			NYB		P-SNY106 added class="#i#", ie columnName as the td & th class
2011-10-14			NYB		LHID7283 added the columnName as the td class where link uses js
2011/10/26			PPB		CR-LEN066 added support for tooltips (multiple changes: search for thisColumnHasToolTip) (see above for usage)
2011/10/27			PPB		CR-LEN066 added support for tooltips to GROUPed TFQO too
2011/11/15 			PPB 	LID7090 save cellValue to use as x:num (continued)
2011/11/28			NYB		LHID8224 added replace " " with "_" around thisColumnHeading
2011/12/08			WAB		Added support for saving excel file to disk (gets put in temporary directory, path is returned in caller.tfqo)
2012/04/02			IH		Case 426265 add query of queries caching to improve performance
2012/04/04			IH		Case 426266 Improve the way filter list is generated
2012/04/16			WAB		Case 427404 saving to excel was not including the excelHeader
2012/04/17			WAB		Case 427404 some values in x:num had leading spaces, causing excel to throw an error, add trim
2012/04/17			WAB		Case 426266 Some more changes to improev performance of filters on large query objects - will improve the speed when filters are first loaded
2012-04-23 			WAB 	Add <cf_sessionTokenField> to bogusForm
2012-05-01			WAB		Add utf-8 to cffile for saveAsExcel
2012-05-09			WAB		Use two CF_translates, one around header and one round main data - trying to prevent cf_translate crashing on very large datasets
2012-10-09			WAB		Case 431103.  Change to currency handling, get rid of anything based on the user's locale - which does not make sense in a currency context.
							Removed atttributes.currencyLocale replaced with attributes.currencyISO and attributes.currencyISOColumn
2012/11/13 			YMA 	CASE 429879. Update TFQO to get Yes/No translations before entering loop
2012-11-30 			PPB 	Case 430586 remove the hidden list of checkbox values from a previous run otherwise the new list will be appended to it (and leave old values for checkboxes that were unchecked)
2012-12-10			IH		Case 432398 set a default value for excelClass
2013-05-14 			NYB 	Case 434909 added attributes.disableShowAllLink
2013-07-17 			NYB 	Case 436116 added attributes.displayMessage
2013-10-07			YMA		noForm param to remove all form occurances.  This will prevent any page controls from showing.
2014-02-21			NJH		Case 438891 added recordCount lte 500 to don't show the 'show All' link if more than 500 records.
2014-03-26			WAB		CASE 439260 added use of noForm atribute in a few more places - allows TFQO to be used within other forms even though with reduced functionality
2014-06-17			NJH		Case 435831/Task Core-64 - certifications relaytag - Added support for modal. Added class modal on the link
2014-06-24			SB		Responsive/Bootstrap
2014-09-03			WAB		CASE 441552 Fix group totalling on currency rollup
2014-09-16			WAB		CASE 441722 Alter how excel file is generated and delivered.  Save file to disk and use CFContent file attribute.  Allows code to work on portal within the merge system
2014-11-12					Some styles brought from responsive branch (responsiveTable et al)
2015-01-21			AHL		Case 443209  Case Page, can't view all case comments/entries in list, can't view pages beyond first page.  Add an id to the Paging Form
2015/03/04			NJH		Jira Fifteen 267 - altered the getnumberFormatMaskValue function to accept actual masks, rather than pre-defined masks
2015-03-06			RPW		FIFTEEN-267 - Custom Entities - Added ability to display financial flags
2015-03-16			RPW		Wrapped phrase Ext_tablequery_recordwasfound in <label> to make it display the same as when there are multiple results.
2015-04-09			AHL		Case 444215 Pagination request for My Progress. Last element not inclusive
2015-07-01			WAB		Filter form not working in certain cases (eg on myApprovals.cfm) due to form within table issues.
2015-09-30			ACPK	PROD2015-69 Fixed non-functioning Show All Records link
2015/09/30			NJH		JIRA PROD2015-35 Added date filter for single date column that uses the calendar fields. Also put toggle capability on filter section.
2015-10-08          DAN     Case 445973 - replace table with divs wrapping select for functionListQuery
2015-10-26			RJT		PROD2015-255 - UI correction, in the filters a "one value" select drop down will now look like a disabled select rather than plain text
2015-11-11			SB		Bootstrap
2015/11/20			NJH		PROD2015-429 - add clear button to reset filter form. Removed clear filter from filter dropdown.
2015-11-27          ACPK    PROD2015-457 Added new doNotSortTheseColumns attribute; used to disable sorting for specified columns
2016-05-31  		ESZ 	case 449146 Translated Dates in Certificates
2016-06-16  		WAB  	PROD2016-876 Added attribute to allow rowIdentity to be encrypted
2016-06-20			RJT		When clearing filters cleared the sort order by removing it (not setting it to "") so it can then be defaulted to whatever the default is
2016-06-16  		WAB  	PROD2016-876 Added attribute to allow rowIdentity to be encrypted
2016-11				NJH		Added encryption of row identity when it is used as the ID of the <TR>
2017-01-31			WAB		ONB-458 Problem with above - needed to test for rowIdentityColumnName being null	

Enhancements still to do:

support images on link
support javascript links

--->

<cffunction name = "getnumberFormatMaskValue">
	<cfargument name="numberFormatMask" required="true" type="string">
	<cfargument name="columnPosition" required="true" default="0">

	<cfset var mask = gettoken(numberFormatMask,columnPosition,",")>

	<cfif mask neq "">
		<cfif find("_",mask)>
			<cfreturn replaceNoCase(mask,"*comma",",","ALL")>
		<cfelse>
			<cfswitch expression="#mask#">
				<cfcase value="decimal">
					<cfreturn ",_-_.__">
				</cfcase>
			</cfswitch>
		</cfif>
	</cfif>


	<cfreturn ",_-_">
</cffunction>

<!--- 2016-06-02 ESZ Case 449146 Translated Dates in Certificates  --->
<cffunction name = "getDateFormatMaskValue">
	<cfargument name="dateFormatMask" required="true" type="string">
	<cfargument name="columnPosition" required="true" default="0">

	<cfset var mask = Trim(gettoken(dateFormatMask,columnPosition,","))>
	<cfreturn mask eq "" ? "medium" : mask>

</cffunction>

<!--- <cf_translate> WAB 2009/09/21 moved further down, must be below the excel cfcontent--->

<cfparam name="attributes.queryWhereClauseLabel" default="">
<!--- 2005/03/21 - GCC  --->
<cfparam name="attributes.showCellColumnHeadings" type="boolean" default="no">
<cfif attributes.showCellColumnHeadings eq "yes" or structKeyExists(attributes,"toolTipColumnList")  >
	<!--- <cfoutput><cfinclude template="/javascript/popupDescription.js"></cfoutput> --->
	<cfoutput><cf_includeJavascriptOnce template="/javascript/qTip.js"></cfoutput>
</cfif>

<cfparam name="attributes.useInclude" type="boolean" default="true">
<cfparam name="attributes.numberFormatMask" type="string" default="">

<!--- ==============================================================================
SWJ  05-09-2007  These are new include files
=============================================================================== --->
<cfparam default="" name="attributes.preInclude" type="string">
<cfparam default="" name="attributes.postInclude" type="string">

<!--- 2013-05-16 NYB Case 434909 - added --->
<cfparam default="false" name="attributes.disableShowAllLink" type="boolean">

<!--- Don't want to perform the check on live sites --->
<cfif application.testsite eq 0>
	<cfset attributes.useInclude = "false">
</cfif>

<!---	check whether it is required that the caller file uses the tableFromQuery-QueryInclude.cfm include.
		if true, check that the file has used it, otherwise setup an error status. --->
<cfif attributes.useInclude>
	<cfif not structKeyExists(caller,"includeFileIncluded")>
		<cfset errorStatus = 1>
	</cfif>
</cfif>

<cfparam name="attributes.passThroughVariablesStructure" type="struct" default="#StructNew()#">

<cfif structKeyExists(caller,"passThruVars")>
	<cfset attributes.passThroughVariablesStructure = caller["passThruVars"]>
</cfif>

<cfparam name="attributes.queryObject">

<cfif not IsQuery(attributes.queryObject) or (structKeyExists(attributes,"filterQueryObject") and not IsQuery(attributes.filterQueryObject))>
	<cfoutput>phr_Ext_tablequery_Youmustpassaqueryobjectto CF_tableFromQueryObject</cfoutput>
	<cfexit>
</cfif>

<cfparam name="attributes.sortOrder" default="">
<cfif attributes.sortOrder is "" and isDefined("form.sortOrder")>
	<cfset attributes.sortOrder = form.sortOrder> <!--- WAB added, one less parameter to pass in - although won't pick up the initial sort order--->
</cfif>
<cfparam name="attributes.openAsExcel" type="boolean" default="false">
<cfparam name="attributes.saveAsExcel" type="boolean" default="false">
<cfparam name="attributes.ExcelFileName" type="string" default="#replaceNoCase(listLast(cgi.script_Name,"/"),".cfm",".xls")#">   <!--- WAB 2009/09/21 LID 2618  added new parameter--->
<cfif structKeyExists(caller,"openAsExcel")>
	<cfset attributes.openAsExcel = caller["openAsExcel"]>
</cfif>
<cfset createAsExcel = attributes.openAsExcel or attributes.saveAsExcel>

<cfparam name="attributes.allowColumnSorting" default="yes">
<cfparam name="attributes.hideTheseColumns" default="">
<cfparam name="attributes.sumTheseColumns" default="">
<cfparam name="attributes.averageTheseColumns" default="">
<cfparam name="attributes.averageColumnsToUse" default="">
<cfparam name="attributes.totalTheseColumns" default="">
<cfparam name="attributes.GroupByColumns" default="">
<cfparam name="attributes.showAsLocalTime" default="true">
<cfparam name="attributes.tableClass" default="withBorder">
<cfparam name="attributes.tableAlign" default="center">
<cfparam name="attributes.HidePageControls" type="boolean" default="no">

<cfparam name="attributes.radioFilterLabel" type="string" default="">
<cfparam name="attributes.radioFilterDefault" type="string" default="">
<cfparam name="attributes.radioFilterName" type="string" default="">
<cfparam name="attributes.radioFilterValues" type="string" default="">


<cfparam name="attributes.FilterSelectFieldList" type="string" default="">  <!--- WAb for backwards compatability --->
<cfparam name="attributes.FilterSelectFieldList1" default= #attributes.FilterSelectFieldList#>
<cfparam name="attributes.FilterSelectFieldList2" type="string" default="">
<cfparam name="attributes.FilterSelectFieldList3" type="string" default="">

<cfparam name="attributes.filterSelectValue" type="string" default="">
<cfparam name="attributes.filterSelectValue2" type="string" default="">
<cfparam name="attributes.filterSelectValue3" type="string" default="">

<cfparam name="attributes.checkBoxFilterName" type="string" default="">
<cfparam name="attributes.checkBoxFilterLabel" type="string" default="">
<cfparam name="attributes.dateFilter" type="string" default="">
<cfparam name="attributes.formName" default="frmBogusForm">
<!--- define start and end rows of data to display --->
<!--- WAB added 2005-10-26 if startrow is a url or form variable (ie past from a previous page) then use it as long as attributes.startrow is not being used--->
<cfif not structKeyExists(attributes,"startrow") and  isDefined("startrow")>
	<cfset attributes.startrow = startrow>
</cfif>
<cfparam name="attributes.startrow" type="numeric" default="1">
<CFPARAM name="attributes.endrow" type="numeric" default="1">
<cfparam name="attributes.selectbyday" type="boolean" default="false">

<cfparam name="attributes.columnTranslation" type="boolean" default="false">
<cfparam name="attributes.columnTranslationPrefix" default="phr_Sys_Report_">

<cfparam name="attributes.id" default = "aReport">  <!--- id required for drilldown incase more than one report on a page --->

<cfparam name="attributes.TranslateTheseColumns" type="string" default="">  <!--- WAB 2009/05/20 pre-translates contents of a column (quicker than oututting phrs)--->

<!--- WAB 2006-13-12 - problems with having multiple tables on a page, all the javascript only expects a single table,
	needed to start naming forms uniquely, so have had to have a unique ID
--->
<cfparam name="request.tableFromQueryObjectidsOnPage" default = "">
<cfif listfind (request.tableFromQueryObjectIdsOnPage,attributes.id) is not 0>
	<cfset attributes.id = attributes.id & (listlen(request.tableFromQueryObjectidsOnPage) + 1)>  <!--- just add a number to the ID = should make it unique unless very unlucky! could have done a loop --->
</cfif>
<cfset request.tableFromQueryObjectidsOnPage = listappend (request.tableFromQueryObjectidsOnPage, attributes.id)>

<cfif attributes.columnTranslationPrefix is "">
	<cfset attributes.columnTranslationPrefix = "phr_Sys_Report_">
</cfif>

<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
<cfparam name="attributes.IllustrativePriceColumns" type="string" default=""> <!--- columns that require illustrative pricing to be shown next to them --->
<cfparam name="attributes.IllustrativePriceFromCurrency" type="string" default=""> <!--- conversion from currency ISOCode --->
<cfparam name="attributes.IllustrativePriceToCurrency" type="string" default=""> <!--- conversion to currency ISOcode --->
<cfparam name="attributes.IllustrativePriceDateColumn" type="string" default=""> <!--- query column to use for date of conversion--->
<cfparam name="attributes.IllustrativePriceMethod" type="string" default="2"> <!--- method for getting the price --->
<!--- <cfparam name="attributes.IllustrativePriceFromCurrencyColumn" type="string" default=""> ---> <!--- NJH 2010/03/09 P-PAN002 the column holding the currency value to convert from when using illustrative  pricing--->

<!--- NAS 2010/04/21 P-PAN002 show correct currency amounts driven from currencyISO code from table see oppCustomerController.cfm for example --->
<cfparam name="attributes.rollUpPriceFromCurrencyColumn" type="string" default=""> <!--- NJH 2010/03/09 P-PAN002 the column holding the currency value to convert from for rollup reporting --->
<cfparam name="attributes.rollUpPriceToCurrency" type="string" default="">
<cfparam name="attributes.rollUpCurrency" type="boolean" default="false">

<cfparam name="attributes.useTeams" type="boolean" default="false">
<cfparam name="attributes.useTeamsPersonIDColumn" default="">

<cfparam name="attributes.DisplayMessage" default=""><!--- 2013-07-17 NYB Case 436116 added --->
<cfparam name="attributes.noForm" default="false"><!--- 2013-10-07	YMA	noForm param to remove all form occurances.  This will prevent any page controls from showing. --->
<cfparam name="attributes.dateFormatMask" type="string" default=""><!--- 2016-06-02 ESZ Case 449146 Translated Dates in Certificates  --->

<cfset excelClass="xl25">

<!--- NJH 2010/07/29 - P-PAN002 set up some defaults for rollup reporting, so we can just pass in rollUpCurrency=true --->
<cfif attributes.rollUpCurrency>
	<cfset exchangeRateExists = true>
	<cfif attributes.rollUpPriceFromCurrencyColumn eq "">
		<cfset attributes.rollUpPriceFromCurrencyColumn = "currency">
	</cfif>
	<cfif attributes.rollUpPriceToCurrency eq "">
		<cfquery name="getCurrencyForCountry" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,5,0)#">
			select countryCurrency from country where isoCode = <cf_queryparam value="#request.relayCurrentUser.countryIsoCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfset attributes.rollUpPriceToCurrency = listFirst(getCurrencyForCountry.countryCurrency)>
	</cfif>
</cfif>

<!--- setup rowCount for excel --->
<cfif structKeyExists(caller,"excelHeaderRows")>
	<cfset rowCount = caller["excelHeaderRows"]>
<cfelse>
	<cfset rowCount = 0>
</cfif>

<!--- setup alphabetical list for excel column call use --->
<cfset aList="A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z">
<cfset alphaList = aList>
<cfloop list="#aList#" index="alpha1">
	<cfloop list="#aList#" index="alpha2">
		<cfscript>
			alphaList = listAppend(alphaList,alpha1&alpha2);
		</cfscript>
	</cfloop>
</cfloop>



<!--- attributes.showTheseColumns will override the query column list and display order --->
<cfif isdefined( "attributes.showTheseColumns" )>
	<cfset showColumnList = attributes.showTheseColumns>
<cfelse>
	<cfset showColumnList = attributes.queryObject.columnList>
	<!--- WAB 2006-11-21 hidethese columns wasn't working --->
	<cfloop index="hideCol" list ="#attributes.hidethesecolumns#">
		<cfset pos = listFindNoCase(showColumnList,hidecol)>
		<cfif pos is not 0>
			<cfset showColumnList = listdeleteat(showColumnList,pos)>
		</cfif>
	</cfloop>
</cfif>

<cfset columnHeadingList = iif( isdefined( "attributes.columnHeadingList" ), "attributes.columnHeadingList", "showColumnList" )>
<cfif listLen(showColumnList) is not listLen(columnHeadingList)>
	<cfoutput>phr_Ext_tableQuery_ColumnHeadingListError <CF_ABORT></cfoutput>
</cfif>


<cfparam name="attributes.CurrencyFormat" type="string" default="">
<cfparam name="attributes.CurrencyISO" type="string" default=""> <!--- A single currency per column, we use the currency symbol --->
<cfparam name="attributes.CurrencyISOColumn" type="string" default="">  <!--- different currencies in the same column, we append the ISO Code to the currency--->


<!--- If attributes.CurrencyFormat and attributes.CurrencyISO are set, then precalculate the Currency Symbol and the excel class to use --->
<cfset currencyStruct = {}>
<cfif attributes.CurrencyFormat is not "" and listlen(attributes.CurrencyISO) is listlen(attributes.CurrencyFormat)>
	<cfset i = 0>
	<cfloop index="CurrencyColumn" list="#attributes.CurrencyFormat#">
		<cfset i ++>
		<cfset currencyISO = listgetat(attributes.currencyISO,i)>
		<cfset currencyStruct[CurrencyColumn] = application.com.currency.getCurrencyByISOCode(currencyISO)>
	</cfloop>
</cfif>

<!--- If there is a currencyISOColumn and we are downloading as excel then we need to add the column to the download --->
<cfif attributes.CurrencyFormat is not "" and listlen(attributes.CurrencyISOColumn) is listlen(attributes.CurrencyFormat) and createAsExcel>
	<cfset i = 0>
	<cfloop index="CurrencyColumn" list="#attributes.CurrencyFormat#">
		<cfset i ++>
		<cfset currencyISOColumn = listgetat(attributes.currencyISOColumn,i)>
		<cfif not listFindNoCase(showColumnList,currencyISOColumn)>
			<cfset position = listFindNoCase(showColumnList,currencyColumn)>
			<cfset showColumnList = listInsertAt(showColumnList,position,currencyISOColumn)>
			<cfset columnHeadingList = listInsertAt(columnHeadingList,position,currencyISOColumn)>
		</cfif>
	</cfloop>
</cfif>



<cfif attributes.IllustrativePriceColumns neq "" or attributes.rollUpPriceToCurrency neq "">

	<cfif attributes.IllustrativePriceColumns neq "">
		<cfset currencyISO = attributes.IllustrativePriceToCurrency>
	<cfelse>
		<cfset currencyISO = attributes.rollUpPriceToCurrency>
	</cfif>

	<cfset getCurrency = application.com.currency.getCurrencyByISOCode(currencyISO)>
	<cfset excelCurrencyClass = getCurrency.currencyMso>
	<cfset convertToCurrencySign = getCurrency.currencySign>

</cfif>

<!--- 2012/11/13 YMA Case 429879. Update TFQO to get translations before entering loop--->
<cfif structKeyExists(attributes,"booleanFormat") and attributes.booleanFormat is not "">
	<cfset yesNoTranslations = application.com.relayTranslations.translateListOfPhrases("yes,no")>
	<cfset yesTranslation = yesNoTranslations.phrases.yes.phraseText>
	<cfset noTranslation = yesNoTranslations.phrases.no.phraseText>
</cfif>


<!--- --------------------------------------------------------------------- --->
<!--- nasty code to specifically handle the excel link --->
<!--- --------------------------------------------------------------------- --->
<cfif createAsExcel>
	<cfloop collection="#url#" item="urlItem">
		<cfset form[urlItem] = url[urlItem] />
	</cfloop>
	<cfset attributes.startrow = 1 />
	<cfset attributes.numRowsPerPage = 999999 />
</cfif>
<!--- --------------------------------------------------------------------- --->

<!--- --------------------------------------------------------------------- --->
<!--- function select --->
<!--- --------------------------------------------------------------------- --->
<cfparam name="attributes.rowIdentityColumnName" type="string" default="#ListFirst( attributes.queryObject.ColumnList )#">
<cfparam name="attributes.encryptrowIdentity" type="boolean" default="false">
<cfparam name="attributes.functionListQuery" type="query" default="#QueryNew( "functionID,functionName,securityLevel,url,windowFeatures" )#">
<cfparam name="attributes.showFunctionList" type="boolean" default="#Evaluate( "attributes.functionListQuery.RecordCount gt 0" )#">
<cfparam name="attributes.FunctionListComment" default="no">

<cfparam name="form.frmRowIdentity" type="string" default="">
<!--- --------------------------------------------------------------------- --->

<!--- --------------------------------------------------------------------- --->
<!--- text search --->
<!--- --------------------------------------------------------------------- --->
<cfparam name="attributes.searchColumnList" type="string" default="">
<cfparam name="attributes.searchColumnDisplayList" type="string" default="#attributes.searchColumnList#">
<cfparam name="attributes.searchColumnDataTypeList" type="string" default="">

<cfparam name="form.frmSearchColumnDataType" type="string" default="">
<cfparam name="form.frmSearchText" type="string" default="">
<cfparam name="form.frmSearchColumn" type="string" default="">
<cfparam name="form.frmSearchComparitor" type="string" default="">
<cfparam name="form.frmFromDate" type="string" default="">
<cfparam name="form.frmToDate" type="string" default="">

<!--- NJH 2013/04/24 - added search box --->
<cfparam name="attributes.showTextSearch" type="boolean" default="false">
<cfparam name="attributes.searchTextColumns" default="#listAppend(showColumnList,attributes.rowIdentityColumnName)#">

<cfset searchComparitorList="Phr_Sys_search_none,Phr_Sys_search_equals,Phr_Sys_search_like,Phr_Sys_search_greater,Phr_Sys_search_less">
<cfloop index="searchColumn" from="#Evaluate( "ListLen( attributes.searchColumnDisplayList ) + 1" )#" to="#ListLen( attributes.searchColumnList )#">
	<cfset attributes.searchColumnDisplayList = ListAppend( attributes.searchColumnDisplayList, ListGetAt( attributes.searchColumnList, searchColumn ) )>
</cfloop>
<cfloop index="searchColumn" from="#Evaluate( "ListLen( attributes.searchColumnDataTypeList ) + 1" )#" to="#ListLen( attributes.searchColumnList )#">
	<cfset attributes.searchColumnDataTypeList = ListAppend( attributes.searchColumnDataTypeList, 0 )>
</cfloop>
<!--- --------------------------------------------------------------------- --->

<!--- --------------------------------------------------------------------- --->
<!--- query where clause --->
<!--- --------------------------------------------------------------------- --->
<cfparam name="attributes.queryWhereClauseStructure" type="struct" default="#StructNew()#">
<cfif structKeyExists(caller,"queryWhereClause")>
	<cfset attributes.queryWhereClauseStructure = caller["queryWhereClause"]>
</cfif>
<cfparam name="form.frmWhereClauseList" type="string" default="">
<!--- --------------------------------------------------------------------- --->

<!--- --------------------------------------------------------------------- --->
<!--- paging and filtering --->
<!--- --------------------------------------------------------------------- --->
<cfparam name="form.filterselect" type="string" default="">
<cfparam name="form.filterselectvalues" type="string" default="">
<cfparam name="form.filterselect1" type="string" default="">
<cfparam name="form.filterselectvalues1" type="string" default="">
<cfparam name="form.filterselect2" type="string" default="">
<cfparam name="form.filterselectvalues2" type="string" default="">
<cfparam name="form.filterselect3" type="string" default="">
<cfparam name="form.filterselectvalues3" type="string" default="">

<cfparam name="form.frmAlphabeticalIndex" type="string" default="">
<cfparam name="form.frmAlphabeticalIndexList" type="string" default="">

<cfparam name="attributes.alphabeticalIndexColumn" type="string" default="">
<cfset attributes.hideTheseColumns = ListAppend( attributes.hideTheseColumns, "alphabeticalIndex" )>

<!--- 2015-11-27 ACPK    PROD2015-457 Added new doNotSortTheseColumns attribute; used to disable sorting for specified columns --->
<cfparam name="attributes.doNotSortTheseColumns" type="string" default="">

<cfset thisColumnHasToolTip = false> <!--- defined high for safety; should put ToolTip functionality into GROUP TFQO stuff too --->

<!---
<cfparam name="attributes.columnInclusionList" type="string" default="">
<cfparam name="attributes.columnInclusionTemplateList" type="string" default="">
 --->
<!--- --------------------------------------------------------------------- --->

<!--- <CFPARAM name="caller.form.startrow" default="1">
<CFPARAM name="caller.form.endrow" default="1"> --->
<cfset startrow = attributes.startrow>
<cfset endrow = attributes.endrow>

<!--- attributes.maxrows shows the maximum number of rows of data to display per page --->
<!--- WAB added 2005-10-26  if numRowsPerPage is a url or form variable (ie past from a previous page) then use it as long as attributes.numRowsPerPage is not being used--->
<!--- 2015-09-30	ACPK	PROD2015-69 Fixed non-functioning Show All Records link --->
<cfif isDefined("numRowsPerPage")>
	<cfset attributes.numRowsPerPage = numRowsPerPage>
</cfif>
<cfparam name="attributes.numRowsPerPage" default="50">

<cfif not isnumeric(attributes.numRowsPerPage)>
	<cfset attributes.numRowsPerPage=50>
	<cfset errors=errors&"A non numeric value was entered for numRowsPerPage, 50 has been used">
</cfif>

<!--- WAB 2010/09/06 moved this line from further down because needed value of endRow
	Note that I cannot how attributes.endrow is ever used
	Also I can't see the end row being used in the group'ed loop
 --->
<cfset endRow = startRow + attributes.numRowsPerPage - 1 >	<!--- 2015-04-09 AHL Case 444215 Pagination request for My Progress. Last element not inclusive--->

<!--- 	WAB 2009/05/20 added translateTheseColumns
 		WAB 2010/09/06 Added start and end rows.  Not sure about the value of endRow
--->
<cfif attributes.translateTheseColumns is not "">
	<cf_translateQueryColumn
			query = #attributes.queryObject#
			columnName = #attributes.translateTheseColumns#
			onNullShowPhraseTextID = true
			startRow = #attributes.startRow#
			endRow = #EndRow#
					>
</cfif>


<!--- START: 2008-07-08	AJC openAsExceljs() code --->
<!--- <cfif isDefined("attributes.keyColumnOpenInWindowList") and listFindNoCase(attributes.keyColumnOpenInWindowList,"yes") neq 0 and createAsExcel eq false>
	<cfoutput><script src="/javascript/openWin.js" type="text/javascript" language="JavaScript1.2"></script></cfoutput>
</cfif> --->
<!--- END: 2008-07-08	AJC openAsExceljs() code --->

<!--- WAB 2009/04/16 put this code inside an if statement so does not appear in excel downloads (also moved down the file a bit) --->
<cfif createAsExcel eq false>
	<!--- START: 2008-07-08	AJC openAsExceljs() code --->
	<cf_includeJavascriptOnce template="/javascript/openWin.js">
	<!--- function for openAsExcel --->
	<cfoutput>
		<script>
			function openAsExceljs()
			{
				var str ="";
				var qs = "";
				/* If the filterselectform exists */



				if(document.FilterSelectForm)
				{
					str = document.FilterSelectForm.action;
					if(str.search(/\?/)== -1)
					{
						qs = "?"
					}
					else
					{
						qs = "&"
					}
					if(str.search(/openAsExcel/)== -1)
					{
						str = str+qs+'openAsExcel=true';
						document.FilterSelectForm.action=str;
						document.FilterSelectForm.target = '_blank';
					}
					document.FilterSelectForm.submit();
					document.FilterSelectForm.target = '';

					document.FilterSelectForm.action = str.replace(/[\?|&]openAsExcel=true/, "");

				}
				/* if the filterselectform does not exist */
				else
				{
					str = location.href;
					if(str.search(/\?/)== -1)
					{
						qs = "?"
					}
					else
					{
						qs = "&"
					}

					if(str.search(/openAsExcel/)== -1)
					{
						str = str+qs+'openAsExcel=true';
					}
					javascript:void(openWin(str,'openAsExcel','width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'));
				}
			}
		</script>
	</cfoutput>
	<!--- END: 2008-07-08	AJC openAsExceljs() code --->
</cfif>


<cf_translate>  <!-- WAB 2009/09/21 LID 2618  moved from further up, must be below the excel cfcontent--->

	<cfoutput>
	<cfif createAsExcel eq false>
	<script language="JavaScript1.2" type="text/javascript">
		function reSort(sortOrder) {
			var form = document.sortForm;
			form.sortOrder.value = sortOrder;
			form.submit();
		}

		function clearFilters() {
			var filterForm = jQuery('##FilterSelectForm');
			filterForm.find(':input:not([type=hidden]):enabled:not([type=button]):not([type=submit])').val('');
			jQuery('##sortOrder').remove(); //kill sortorder because if its absent it gets defaulted, if its "" then it doesn't'
			filterForm.submit();
		}

		function SetFilterSelectValues(nVariableName,nValue,nWhich) {


		// Change the contents of the filter values list
		// depending upon the value selected in the filter column list

		if(nValue == ""){
			// if set to null then make sure that the linked value select is null as well
			nSelect = eval('document.FilterSelectForm.FilterSelectValues' +nWhich)
			nSelect.selectedIndex = 0 ;
			document.FilterSelectForm.submit()
		}else{

			nVariable = eval(nVariableName + '_' + nWhich);
			nSelect = eval('document.FilterSelectForm.FilterSelectValues' +nWhich)
			nSelect.options.length = 1

			<!--- NJH 2012/05/31 CRM 428539 - re-did the filtering. Set the variable as an array now, with the value and display | delimited ... (ie. value|display)
				The value was being used for both the text and value, which 'worked' for most cases, except where the display and value were different, such as for boolean formats
				where the value is 1 but the display is 'yes'.
			--->
			for (x=0;x<nVariable.length;x++) {
				valueText = nVariable[x];
				valueTextArray = valueText.split('|');
				value = valueTextArray[0];
				text = valueTextArray[1];

				opt = new Option()
				opt.value = value.replace( /~/g, "," );
				opt.text  = text.replace( /~/g, "," ); // commas were escaped as ~
				nSelect.options[nSelect.options.length]=opt
			}
		}
	}
<!--- --------------------------------------------------------------------- --->
<!--- function select --->
<!--- --------------------------------------------------------------------- --->
<cfif attributes.showFunctionList>
	function checkboxValuesToList( sourceCheckbox )
	{
	  var returnValue = "";
	  var checkboxIndex = 0;
	  var listDelimiter = "";

	  if ( (sourceCheckbox.checked) )
	  {
	    returnValue = sourceCheckbox.value;
	  }
	  else
	  {
	    for ( checkboxIndex = 0; checkboxIndex < sourceCheckbox.length; checkboxIndex++ )
	    {
	      if ( sourceCheckbox[checkboxIndex].checked )
	      {
	        returnValue += listDelimiter;
	        returnValue += sourceCheckbox[checkboxIndex].value;
	        listDelimiter = ",";
	      }
	    }
	  }

	  return returnValue;
	}

	var functionObject = new Object();

	<cfloop query="attributes.functionListQuery">
		functionObject["<cfoutput>#JSStringFormat( attributes.functionListQuery.functionID )#</cfoutput>"] = new Object();
		functionObject["<cfoutput>#JSStringFormat( attributes.functionListQuery.functionID )#</cfoutput>"]["url"] = "<cfoutput>#JSStringFormat( attributes.functionListQuery.url )#</cfoutput>";
		functionObject["<cfoutput>#JSStringFormat( attributes.functionListQuery.functionID )#</cfoutput>"]["windowFeatures"] = "<cfoutput>#JSStringFormat( attributes.functionListQuery.windowFeatures )#</cfoutput>";
	</cfloop>

	var checkboxValueList = ""; // NJH 2011/06/07 Made this a global variable so that it could be referenced in the code that builds the dropdown functions

	function eventFunctionOnChange( callerObject ){

	  if ( functionObject[callerObject[callerObject.selectedIndex].value]["url"] != "" ){

		checkboxValueList = checkboxValuesToList( document.forms["#attributes.formName#"]["frmRowIdentity"] );
		var thisForm = document.forms["#attributes.formName#"];

		<!--- NJH 2014/08/15 - commented out the checking of the javascript. not sure why that condition was put in, as we still want to make sure that rows are selected --->
		if ( checkboxValueList == "" <!--- && functionObject[callerObject[callerObject.selectedIndex].value]["url"].indexOf( "javascript:" ) != 0 ---> ){
	      alert( "Please select one or more rows first" );
	    }

		else if ( functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"] != "" ){
		  var newURL = functionObject[callerObject[callerObject.selectedIndex].value]["url"];
		  var strArr = newURL.split("?");
		  var formItemName = "";
		  if (strArr.length >= 2) {
		  	var strArr2 = strArr[1].split("&");
			var strArr3 = strArr2[strArr2.length-1].split("=");
		  	formItemName = strArr3[0];
			//alert((newURL.length - 1) - (formItemName.length));
			var tmpURL = newURL.substring(0,(newURL.length) - (formItemName.length));
			newURL = tmpURL;
		  }
		  //var tmpForm = document.createElement('form');
		  var tmpForm = thisForm;
		  var ninput = document.createElement('input');
		  ninput.type = 'hidden';
		  ninput.id = formItemName;			// 2012-11-30 PPB Case 430586
		  ninput.name = formItemName;
		  ninput.value = ninput.defaultValue = checkboxValueList;
		  tmpForm.method = "post";
		  tmpForm.action = newURL;
		  tmpForm.onsubmit = window.open( "", "functionWindow", functionObject[callerObject[callerObject.selectedIndex].value]["windowFeatures"], true );
		  tmpForm.target = "functionWindow";


		  // 2012-11-30 PPB Case 430586 remove the hidden list of checkbox values from a previous run otherwise the new list will be appended to it (and leave old values for checkboxes that were unchecked)
		  if (document.getElementById(formItemName)!=undefined) {
			  var origFormItem = document.getElementById(formItemName);
			  origFormItem.parentNode.removeChild(origFormItem);
		  }

		  tmpForm.appendChild(ninput);
		  //document.body.appendChild(tmpForm);

		  tmpForm.submit();
	    }

	   	else {
	      window.location.href = functionObject[callerObject[callerObject.selectedIndex].value]["url"] + escape( checkboxValueList );
	  	}
	  }

	  callerObject.selectedIndex = 0;

	  return true;

	}

	function checkboxSetAll( checkCheckbox )
	{
	  var checkboxIndex = 0;

	  if ( !(document.forms["#attributes.formName#"]["frmRowIdentity"].length) )
	  {
	    document.forms["#attributes.formName#"]["frmRowIdentity"].checked = checkCheckbox;
	  }
	  else
	  {
	    for ( checkboxIndex = 0; checkboxIndex < document.forms["#attributes.formName#"]["frmRowIdentity"].length; checkboxIndex++ )
	    {
	      document.forms["#attributes.formName#"]["frmRowIdentity"][checkboxIndex].checked = checkCheckbox;
	    }
	  }

	  return true;
	}

	jQuery(document).ready(function() {
		jQuery("input[type='checkbox'][name='frmRowIdentity']").click(function(){
			if(!this.checked && jQuery("##checkAllRowIdentities").attr('checked')){
				jQuery("##checkAllRowIdentities").attr('checked',false);
			} else if (!jQuery("input[type='checkbox'][name='frmRowIdentity']:not(:checked)").length){
				jQuery("##checkAllRowIdentities").attr('checked',true);
			};
		})
	});


</cfif>
<!--- --------------------------------------------------------------------- --->

	</script>
	</cfif>
	</cfoutput>


<cfif createAsExcel eq false or not attributes.noForm>
<!--- ==============================================================================
       SET UP THE FILTERS
	   If there are any filters defined in FilterSelectFieldList
		Set Up the filter lists. This requires building lists of distinct values for each
		filter field, in Javascript, so that the contents of the select lists can bechanged at will.
		Cannot use the ValueList function because commas will corrupt the list.
=============================================================================== --->
	<cfif not structKeyExists(attributes,"filterqueryObject")>
		<!--- create filterQueryObject from queryObject
			we used to just default filterQueryObject to attributes.queryObject, but on very large queryObjects it can be very slow doing multiple QoQs on the whole queryObject
			so we start by creating a smaller query with all the distinct values and using this as the base for creating filters
		--->
		<!--- get list of all columns being used in the filters --->
		<cfset filterlist = "">
		<cfloop index="filterNumber" from = "3" to = "1" step = "-1">
			<cfset filterlist = listappend(filterList,attributes["FilterSelectFieldList#filterNumber#"])>
		</cfloop>
		<cfset filterList = application.com.globalFunctions.removeDuplicates(filterList)>

		<cfif filterList is not "">
			<!--- now create a query with all the distinct combinations of filter values --->
			<CFQUERY Name="attributes.filterqueryObject" dbtype="query">
				SELECT DISTINCT #filterList#
				FROM attributes.queryObject
			</CFQUERY>
		</cfif>

	</cfif>

	<cfloop index="filterNumber" from = "3" to = "1" step = "-1">

		<cfset thisFilterSelectFieldList = attributes["FilterSelectFieldList#filterNumber#"] >

			<CFLOOP LIST="#thisFilterSelectFieldList#" INDEX="nFieldName">
				<cfif structKeyExists(attributes,"booleanFormat") and listFindNoCase(attributes.booleanFormat,nFieldName) neq "0">
					<!--- P-LEN022 LID 4116 if showing a boolean format column then the filters should display Yes/No not 1/0 --->
					<CFQUERY Name="Filter#nFieldName#_#filterNumber#" datasource="#application.siteDataSource#">
						SELECT 1 AS FilterValue,'phr_Yes' AS FilterDisplay UNION SELECT 0 AS FilterValue,'phr_No' AS FilterDisplay ORDER BY FilterValue DESC
					</CFQUERY>
<!---
					<cf_querySim>
						Filter#nFieldName#_#filterNumber#
						display,value
						phr_Yes|1
						phr_No|0
					</cf_querySim>
 --->
				<cfelse>
					<!--- 2012/04/02	IH	Case 426265 add query of queries caching to improve performance - DO NOT REMOVE EXTRA AND IN QUERY - vital to spoof caching because QOQ caching does not look at changes in base query --->
					<cfset cacheMinutes = 1>
					<cfif attributes.filterqueryObject.recordCount gt 10000>
						<cfset cacheMinutes = 5>
					<cfelseif attributes.filterqueryObject.recordCount gt 100000>
						<cfset cacheMinutes = 15>
					</cfif>
					<CFQUERY Name="Filter#nFieldName#_#filterNumber#" dbtype="query" cachedwithin="#createTimeSpan(0,0,cacheMinutes,0)#">
						SELECT DISTINCT #nFieldName# AS FilterValue, #nFieldName# AS FilterDisplay, upper(#nFieldName#) AS FilterOrderValue
						FROM attributes.filterqueryObject
						where '#request.relaycurrentuser.personid##GetBaseTemplatePath()##attributes.filterqueryObject.recordCount#' = '#request.relaycurrentuser.personid##GetBaseTemplatePath()##attributes.filterqueryObject.recordCount#'
						and '#request.relaycurrentuser.personid##GetBaseTemplatePath()##attributes.filterqueryObject.recordCount#' = '#request.relaycurrentuser.personid##GetBaseTemplatePath()##attributes.filterqueryObject.recordCount#'
						Group By #nFieldName#
						Order By FilterOrderValue
					</CFQUERY>
				</cfif>

			<!--- 2012/04/04	IH	Case 426266 Improve the way filter list is generated --->
			<!--- NJH 2012/05/31 CRM 428539 - create the variable as an array, and have it contain elements which are the value and display | delimited --->
				<cfset aFilter=[]>
				<cfloop query="Filter#nFieldName#_#filterNumber#">
					<!--- NJH if the filter value contains a hash, then the DE function below falls over due to the hash existing.. so, here, we just escape it. --->
					<cfset filterValueWithHashEscaped = replace(FilterValue,chr(35),chr(35)&chr(35),"ALL")>
					<cfset thisValue=Replace( JSStringFormat( iif(FilterValue is '',de('null'),de(filterValueWithHashEscaped))&"|"&FilterDisplay), ",", "~", "all" )>
					<cfset arrayAppend(aFilter,thisValue)>
				</cfloop>

				<CFOUTPUT>
				<SCRIPT LANGUAGE="JavaScript1.2">
				var #nFieldName#_#filterNumber# = [#listQualify(arrayToList(aFilter),'"')#];
				</SCRIPT>
				</CFOUTPUT>

			</CFLOOP>
	</cfloop>

<!--- --------------------------------------------------------------------- --->
<!--- error display --->
<!--- --------------------------------------------------------------------- --->
	<cfif isDefined("errorStatus")>
		<cfoutput>
		<table align="#attributes.tableAlign#" data-role="table" data-mode="reflow" class="table table-hover table-striped table-bordered table-condensed #attributes.tableClass#">
			<tr>
				<td align="center" valign="top">
					<p>phr_Ext_tableQuery_ErrorOccured</p>
					<cfswitch expression="#errorStatus#">
						<cfcase value="1">
							<p>phr_Ext_tablequery_QueryIncludeError</p>
						</cfcase>
					</cfswitch>
				</td>
			</tr>
		</table>
		</cfoutput>
		<!--- <cfexit method="EXITTAG"> --->
	</cfif>
<!--- --------------------------------------------------------------------- --->
<!--- function select --->
<!--- --------------------------------------------------------------------- --->
	<cfif attributes.showFunctionList>
		<cfoutput>
            <!--- START: 2015-10-08 DAN Case 445973
				  2015/11/30 SB Bootstrap
			--->
            <div id="functionListDiv" class="grey-box-top">
			<cfif attributes.FunctionListComment eq "yes">
				<!--- MDC 20-01-05 I've added the above parmam into the top of this page
				If you add in the param into the table from query object you will get a translateable phrase
				opposite the function dropdown, (so long as you have cftranslate around the table from query object.
				TO DO: we need to be able to trim the function name to create a unique translation--->
				<cfset G = attributes.functionListQuery.functionName>
				<cfset C = #replace(G,"_","","ALL")#>
				<cfset B = C>
				<cfset A = #replace(B," ","","ALL")#>
				<div id="functionListCommentDiv">phr_Function_#A#</div>
			</cfif>
                <div id="functionListQueryDiv" class="form-group input-width-50">
					<select class="form-control" name="frmFunction" onChange="eventFunctionOnChange(this);">
						<cfloop query="attributes.functionListQuery">
							<cfif attributes.functionListQuery.securityLevel eq "">
								<cfset securityLevelApproved = true>
							<cfelse>
								<cfset securityLevelApproved = false>
								<cfloop index="securityLevelEntry" list="#attributes.functionListQuery.securityLevel#">
									<!---
									<cfif ListFindNoCase( session["securitylevels"], securityLevelEntry ) neq 0>
										<cfset securityLevelApproved = true>
										<cfbreak>
									</cfif>
									--->
									<cfset securityLevelApproved = application.com.login.checkSecurityList(securityLevelEntry)>
									<cfif securityLevelApproved><cfbreak></cfif>
								</cfloop>
							</cfif>

							<cfif securityLevelApproved>
								<option value="#attributes.functionListQuery.functionID#">#application.com.security.sanitiseHTML(attributes.functionListQuery.functionName)#</option>
							</cfif>
						</cfloop>
					</select>
                </div>
            </div>
            <!--- END: 2015-10-08 DAN Case 445973 --->
		</cfoutput>
	</cfif>

<!--- ==============================================================================
     FILTER SECTION
=============================================================================== --->
	<cfif attributes.radioFilterName gt 0 or attributes.FilterSelectFieldList1 GT 0
		or attributes.checkBoxFilterName gt 0 or attributes.searchColumnList neq "" or attributes.useTeams
		or StructCount( attributes.queryWhereClauseStructure ) neq 0 or attributes.showTextSearch or attributes.dateFilter neq ""
		>   <!--- WAb 2008/10/13 added test for queryWhereClauseStructure --->


	<!--- --------------------------------------------------------------------- --->
	<!--- recycle url parameters except for startrow --->
	<!--- --------------------------------------------------------------------- --->
	<cfset queryString = "">
	<cfloop collection="#url#" item="urlItem">
		<cfif urlItem neq "startrow">
			<cfset queryString = queryString & "&" & urlItem & "=" & URLEncodedFormat( url[urlItem] )>
		</cfif>
	</cfloop>

	<!--- 2015-07-01 WAB Move <FORM> tag outside of table --->
	<cfparam name="form.showFilters" default="false">
	<cfoutput>
		<script>
			<cf_translate encode="javascript">
				phr['tfqo_hideFilters'] = 'phr_tfqo_hideFilters';
				phr['tfqo_showFilters'] = 'phr_tfqo_showFilters';
			</cf_translate>


			function showHideFilters() {
				var $_filterDiv = jQuery('##filterDiv');
				$_filterDiv.slideToggle('slow',function() {var filterVisible=$_filterDiv.is(':visible');jQuery('##showHideFilterLink').html(filterVisible?phr.tfqo_hideFilters:phr.tfqo_showFilters);jQuery('##showFilters').val(filterVisible);});
			}
		</script>
		<div id="showHideFilters"><a href="" onclick="showHideFilters();return false;" id="showHideFilterLink" class="btn btn-primary">phr_tfqo_#form.showFilters?"hide":"show"#Filters</a></div>

<div id="filterDiv" <cfif not form.showFilters>style="display:none;"</cfif>>
	<form action="#cgi.SCRIPT_NAME#?#queryString#" method="post" name="FilterSelectForm" id="FilterSelectForm">
		<input type="hidden" value="#form.showFilters#" name="showFilters" id="showFilters">
	<table id="filterTable_#attributes.id#" name="filterTable" width="100%" border="0" cellspacing="0" cellpadding="3" align="#attributes.tableAlign#" class="#attributes.tableClass#table table-hover table-striped table-condensed">
	<tr>
	</cfoutput>
<!--- --------------------------------------------------------------------- --->
	<cfoutput>
		<CFIF IsDefined("sortOrder")>
			<CF_INPUT Name="sortOrder" Type="Hidden" Value="#sortOrder#">
		</CFIF>
	</cfoutput>
<!--- --------------------------------------------------------------------- --->
<!--- propagate form fields through filtering requests --->
<!--- --------------------------------------------------------------------- --->
		<cfif IsStruct( attributes.passThroughVariablesStructure )>
			<cfloop collection="#attributes.passThroughVariablesStructure#" item="formFieldName">
				<cfif formFieldName neq "fieldnames" AND formFieldName DOES NOT CONTAIN "frmwhereclause"> <!--- WAb 2008/10/13 added bit to keep where clause out of these hidden variables --->
					<cfoutput><CF_INPUT type="hidden" name="#formFieldName#" value="# attributes.passThroughVariablesStructure[formFieldName] #"></cfoutput>
				</cfif>
			</cfloop>
		</cfif>


<!--- ==============================================================================
      MyTeams
=============================================================================== --->

		<cfif attributes.useTeams>

		<CFQUERY NAME="TeamName" datasource="#application.siteDataSource#">
			Select TeamId, TeamName
			FROM MyTeam
			WHERE createdby = #request.relaycurrentuser.personid#
			ORDER BY TeamName
		</CFQUERY>

			<!--- NJH 2009/02/03 changed so that one can manage teams no matter how many teams there are. --->
			<cf_includejavascriptonce template="/javascript/extExtension.js">
			<cfset createNewTeamLink = "javascript:void(openNewTab('MyTeams','My Teams','/homepage/myTeam.cfm'));">

			<cfoutput>
			<cfif TeamName.recordcount gt 0>
				<select name="frmTeam" class="form-control">
					<!---
					SSS 2009-02-09 good rid of this <cfif so that you can select a team and unselect a team
					<CFIF NOT IsDefined("frmTeam")></CFIF>
					--->
					<OPTION VALUE="">Please select a Team

				<CFLOOP QUERY="TeamName">
					<OPTION VALUE=#TeamId# <CFIF IsDefined("frmTeam")><CFIF TeamId EQ #frmTeam#>SELECTED</CFIF></CFIF> >#htmleditformat(TeamName)#
				</CFLOOP>
					</select> <a href="#createNewTeamLink#">Click here</a> to manage your teams.
				<CF_INPUT type="hidden" name="frmTeamColumn" value = "#attributes.useTeamsPersonIDColumn#">
			<cfelseif TeamName.recordcount eq 0>
					You have no teams. <a href="#createNewTeamLink#">Click here</a> to create a new team.
				<!--- START 2010/10/13			NAS		LID4317: Teams - Lead Passing listing no filter possible  --->
				<!--- <CF_ABORT> --->

			<!--- <cfelseif TeamName.recordcount eq 1>
					Your team : #TeamName.TeamName#. <a href="#createNewTeamLink#">Click here</a> to manage your team.
				<input type="hidden" name="frmTeam" value = "#TeamName.TeamId#">
				<input type="hidden" name="frmTeamColumn" value = "#attributes.useTeamsPersonIDColumn#"> --->
			<!--- START 2010/10/13			NAS		LID4317: Teams - Lead Passing listing no filter possible  --->
			</cfif>
			</cfoutput>
		</cfif>

<!--- --------------------------------------------------------------------- --->
<!--- ==============================================================================
      FilterSelectFieldList
=============================================================================== --->
	<cfloop index="filterNumber" from="1"  to = "3">

		<cfset thisFilterList = evaluate("attributes.FilterSelectFieldList#filterNumber#")>
		<!--- SSS 2007-03-16 --->
		<cfset thisFilterList = ListChangeDelims(thisFilterList, "#application.delim1#", ",")>
		<cfset currentFilterChosen = evaluate ("form.FilterSelect#filterNumber#")>
		<cfset currentFilterValue  = evaluate ("form.FilterSelectValues#filterNumber#")>
		<cfif listlen(thisFilterList,"#application.delim1#") is 1><cfset currentFilterChosen = thisFilterList></cfif><!--- If only one item then default to select that item --->


		<CFIF listlen(thisFilterList,"#application.delim1#") GT 0>
			<cfoutput>
			<td colspan="#listLen( showColumnList )#" align="left" valign="top">
				<DIV ID=FilterSelect#filterNumber#>
					<!--- NJH 2015/11/13 PROD2015-319 move label outside of if statement --->
					<label class="tfqoFilterLabel" id="FilterSelectLabel#filterNumber#" for="FilterSelect#filterNumber#">phr_tfqo_filter</label>
					<cfif listlen(thisFilterList,"#application.delim1#") is not 1>
						<!--- Select Box for filter name --->
						<select name="FilterSelect#filterNumber#" class="form-control" onChange="SetFilterSelectValues(document.FilterSelectForm.FilterSelect#filterNumber#.options[document.FilterSelectForm.FilterSelect#filterNumber#.selectedIndex].value,document.FilterSelectForm.FilterSelect#filterNumber#.options[document.FilterSelectForm.FilterSelect#filterNumber#.selectedIndex].value,#filterNumber#)">

							<option value="">Phr_Sys_SelectFilter</option>
							<!--- 2006-05-03 AJC P_CHW001 - Added code to replace dbfieldnames with ColumnHeadingList as the display name. Value value stays the same. --->
							<cfset variables.currentlistelement=1>
							<cfloop index="filterName" list="#thisFilterList#" delimiters="#application.delim1#">
								<option value="#filterName#" <cfif filterName is currentFilterChosen >selected</cfif> >#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & filterName', 'replace(ListGetAt(thisFilterList, variables.currentlistelement,"#application.delim1#"),"_"," ","all")' )#</option>
								<cfset variables.currentlistelement=variables.currentlistelement+1>
							</cfloop>
							<!--- <option value="">Phr_Sys_ClearFilter</option> --->
						</select>
					<cfelse>
						<cfset filterName = thisFilterList>
						<cfset displayName = filterName>
						<CF_INPUT type=hidden name="FilterSelect#filterNumber#" value = "#filterName#">
						<cfif attributes.columnTranslation>
							<cfset displayName = attributes.columnTranslationPrefix & filterName>
						<cfelse>
							<cfif structKeyExists(attributes,"ColumnHeadingList") and ListFindNoCase(attributes.showTheseColumns,filterName) gt 0>
								<cfset displayName = ListGetAt(attributes.ColumnHeadingList, ListFindNoCase(attributes.showTheseColumns,filterName))>
							</cfif>
							<cfset displayName = replace(displayName,"_"," ","all")>
						</cfif>
						<select name="FilterSelect#filterNumber#" disabled="disabled" class="form-control">
							<option value="">#displayName#</option>
						</select>
					</cfif>

			 	<span class="tfqoFilterSelectBy">Phr_Sys_by</span>

				<!--- Select Box for filter value --->
				 <select name="FilterSelectValues#filterNumber#" class="form-control">
					<option value="">Phr_Sys_SelectFilterValue </option>
					<CFIF IsDefined("Filter#currentFilterChosen#_#filterNumber#")>
						<CFSET nQuery=evaluate("Filter#currentFilterChosen#_#filterNumber#")>
						<CFLOOP Query="nQuery">
							<option value="<cfif FilterValue is ''>null<cfelse>#FilterValue#</cfif>"<CFIF (FilterValue is not '' and FilterValue is currentFilterValue) or (FilterValue is '' and currentFilterValue is 'null')> selected</CFIF>>#htmleditformat(FilterDisplay)#
						</CFLOOP>
					</CFIF>
				</select>
				</DIV>
			</td>
			</cfoutput>
		</CFIF>
	</cfloop>


<!--- <!--- ==============================================================================
      FilterSelectFieldList
=============================================================================== --->
		<CFIF listlen(attributes.FilterSelectFieldList) GT 0>
			<td colspan="#listLen( showColumnList )#" align="left" valign="top">
				<cfif isDefined("attributes.FilterSelectFieldList")>
					<select name="FilterSelect" onChange="SetFilterSelectValues(eval(document.FilterSelectForm.FilterSelect.options[document.FilterSelectForm.FilterSelect.selectedIndex].value),document.FilterSelectForm.FilterSelect.options[document.FilterSelectForm.FilterSelect.selectedIndex].value)">

						<option value="">Phr_Sys_SelectFilter</option>
					<cfloop index="i" list="#attributes.FilterSelectFieldList#">
							<option value="#i#" <cfif structkeyexists(FORM,FilterSelect) and FORM.FilterSelect eq i>SELECTED</cfif>>#iif( attributes.columnTranslation, '"phr_Sys_Report_" & i', 'replace(i,"_"," ","all")' )#</option>
						</cfloop>
						<option value="">Phr_Sys_ClearFilter</option>
					</select>
				</cfif>
			 Phr_Sys_by <select name="FilterSelectValues">
				<CFIF IsDefined("FormFilterSelectValues")>
				<CFSET nQuery=evaluate("Filter#FormFilterSelect#")>
						<CFLOOP Query="nQuery">
						<CFOUTPUT><OPTION Value="#FilterValue#"<CFIF FilterValue IS FormFilterSelectValues>SELECTED</CFIF>>#FilterValue#</CFOUTPUT>
					</CFLOOP>
				<CFELSE>
					<OPTION VALUE= "">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</CFIF>
				</select>
			</td>
		</CFIF>

<!--- ==============================================================================
      FilterSelectFieldList2
=============================================================================== --->
		<CFIF listlen(attributes.FilterSelectFieldList2) GT 0>
			<td colspan="#listLen( showColumnList )#" align="left" valign="top">
				<cfif isDefined("attributes.FilterSelectFieldList2")>
					<select name="FilterSelect2" onChange="SetFilterSelectValues2(eval(document.FilterSelectForm.FilterSelect2.options[document.FilterSelectForm.FilterSelect2.selectedIndex].value),document.FilterSelectForm.FilterSelect2.options[document.FilterSelectForm.FilterSelect2.selectedIndex].value)">
						<option value="">Phr_Sys_SelectFilter 2</option>
						<cfloop index="i" list="#attributes.FilterSelectFieldList2#">
							<option value="#i#">#iif( attributes.columnTranslation, '"phr_Sys_Report_" & i', 'replace(i,"_"," ","all")' )#</option>
						</cfloop>
						<option value="">Phr_Sys_Clearfilter 2</option>
					</select>
				</cfif>
			 Phr_Sys_by <select name="FilterSelectValues2">
				<CFIF IsDefined("FormFilterSelectValues2")>
				<CFSET nQuery=evaluate("Filter#FormFilterSelect2#")>
						<CFLOOP Query="nQuery">
						<CFOUTPUT><OPTION Value="#FilterValue2#"<CFIF FilterValue2 IS FormFilterSelectValues2>SELECTED</CFIF>>#FilterValue2#</CFOUTPUT>
					</CFLOOP>
				<CFELSE>
					<OPTION VALUE= "">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</CFIF>
				</select>
			</td>
		</CFIF>
 --->
<!--- --------------------------------------------------------------------- --->
<!--- text search --->
<!--- --------------------------------------------------------------------- --->
		<cfif attributes.searchColumnList neq ""
			and (listLen(attributes.searchColumnList) eq listLen(attributes.searchColumnDataTypeList))
						and (listLen(attributes.searchColumnList) eq listLen(attributes.searchColumnDisplayList))>
		<!--- <cfif attributes.searchColumnList neq ""> --->
			<cfoutput>
			<td align="left">
				<label class="tfqoFilterLabel">phr_Sys_SearchBy</label>
				<CF_INPUT type="hidden" name="frmSearchColumnDataType" value="#ListGetAt( attributes.searchColumnDataTypeList, iif( ListFindNoCase( attributes.searchColumnList, form["frmSearchColumn"]  ) neq 0, 'ListFindNoCase( attributes.searchColumnList, form["frmSearchColumn"]  )', 1 ) )#">
				<cfif ListLen( attributes.searchColumnList ) eq 1>
					<CF_INPUT type="hidden" name="frmSearchColumn" value="# attributes.searchColumnList #">
					#htmleditformat(attributes.searchColumnDisplayList)#
				<cfelse>
					<select name="frmSearchColumn" class="form-control" onChange='this.form["frmSearchColumnDataType"].value = [#ListQualify( attributes.searchColumnDataTypeList, """" )#][this.selectedIndex];'>
						<cfloop index="searchColumn" from="1" to="#ListLen( attributes.searchColumnList )#">
							<option value="#ListGetAt( attributes.searchColumnList, searchColumn )#"#iif( form["frmSearchColumn"] eq ListGetAt( attributes.searchColumnList, searchColumn ), de( " selected" ), de( "" ) )#>#ListLast( ListGetAt( attributes.searchColumnDisplayList, searchColumn ), "." )#</option>
						</cfloop>
					</select>
				</cfif>
				<select name="frmSearchComparitor" class="form-control">
					<cfloop index="searchComparitor" list="#searchComparitorList#">
						<option value="#ListFindNoCase( searchComparitorList, searchComparitor )#"#iif( form["frmSearchComparitor"] eq ListFindNoCase( searchComparitorList, searchComparitor ), de( " selected" ), de( "" ) )#>#htmleditformat(searchComparitor)#</option>
					</cfloop>
				</select>
				<CF_INPUT type="text" name="frmSearchText" value="# form["frmSearchText"] #" size="20" maxlength="50">
			</td>
			</cfoutput>
		</cfif>
<!--- --------------------------------------------------------------------- --->


<!--- ==============================================================================
       RADIO FILTER - designed to support one radio filter group
=============================================================================== --->
		<cfif structKeyExists(attributes,"radioFilterLabel") and structKeyExists(attributes,"radioFilterDefault")
			and (listLen(attributes.radioFilterLabel) eq listLen(attributes.radioFilterDefault))
			and (listLen(attributes.radioFilterName) eq listLen(attributes.radioFilterName))
			and listLen(attributes.radioFilterName) gt 0
			>

<cfoutput>
			<td valign="top">
				<label class="tfqoFilterLabel" id="FilterRadioLabel" for="#attributes.radioFilterName#">#htmleditformat(attributes.radioFilterLabel)#</label>
				<cfset checkLast = "False">
				<cfset checked = "False">
				<cfloop index="i" list="#attributes.radioFilterValues#" delimiters="-">
					<!--- if nothing checked by the end set the last one to be checked --->
					<cfif i eq listLast(#attributes.radioFilterValues#,"-") and not checked>
						<cfset checkLast = "True">
					</cfif>
					<label>
						<INPUT TYPE="radio" NAME="#attributes.radioFilterName#" VALUE="#i#" <CFIF (not structKeyExists(form,attributes.radioFilterName) and i eq attributes.radioFilterDefault) or (structKeyExists(form,attributes.radioFilterName) and form[attributes.radioFilterName] is i) or checkLast><cfset checked = "true">Checked</CFIF>> #htmleditformat(i)#
					</label>
				</cfloop>
			</TD>
			</cfoutput>
		</cfif>

		<!--- NJH 2013/04/24 - 2013 Roadmap  - show the search textBox. --->
		<cfif attributes.showTextSearch>
			<cfset columnAndType = "">
			<!--- pass through the available columns and their datatypes, so that we can build up the where clause correctly. We want to search through all displayed fields as well as the primary key --->
			<cfloop array="#getMetaData(attributes.queryObject)#" index="columnData">
				<cfif listFindNoCase(attributes.searchTextColumns,columnData.name)>
					<cfset columnAndType = listAppend(columnAndType,"#columnData.name#|#columnData.typeName#")>
				</cfif>
			</cfloop>

			<cfoutput>
			<td valign="top">
				<span id="searchBoxFilter">
					<label id="searchStringLabel" for="searchString" class="tfqoFilterLabel">Search:</label> <input type="text" name="searchString" id="searchString" class="form-control searchBox" maxlength="50" placeholder="Search" value="#structKeyExists(form,'searchString')?form.searchString:''#">
					<input type="hidden" value="#columnAndType#" name="searchStringColumnsAndTypes">
				</span>
				<!--- <cfif structKeyExists(form,"searchString") and form.searchString neq ""> --->
					<a class="smallLink" href="" onclick="document.FilterSelectForm.searchString.value='';return false;<!--- document.FilterSelectForm.submit() --->">Clear</a>
				<!--- </cfif> --->
			</td>
			</cfoutput>

		</cfif>

		<!--- NJH PROD2015-35 - added date filter for single column--->
		<cfif attributes.dateFilter neq "" and listLen(attributes.dateFilter) eq 1>
			<cfoutput>
				<td valign="top">
					<div id="tfqoDateFilterLabel"><label id="dateFilterLabel" for="frmFromDate" class="tfqoFilterLabel">#attributes.columnTranslation?attributes.columnTranslationPrefix&attributes.dateFilter:application.com.regExp.camelCaseToSpaces(string=attributes.dateFilter)#</div> <div id="tfqoFromDate"><div class="tfqoDateFromToLabel">phr_From</div> <CF_relayDateField currentValue="#structKeyExists(form,'frmFromDate')?form.frmFromDate:''#" fieldName="frmFromDate" helpText="" enableRange="true" showClearLink="true" class="form-control"></div> <div id="tfqoToDate"><div class="tfqoDateFromToLabel">phr_To</div> <CF_relayDateField currentValue="#structKeyExists(form,'frmToDate')?form.frmToDate:''#" fieldName="frmToDate" helpText="" enableRange="true" showClearLink="true" class="form-control"></div>
					<input type="hidden" value="#attributes.dateFilter#" name="dateFilter">
				</td>
			</cfoutput>
		</cfif>

<!--- ==============================================================================
       CHECKBOX FILTER - designed to support one radio filter group
=============================================================================== --->
		<cfif structKeyExists(attributes,"checkBoxFilterLabel") and structKeyExists(attributes,"checkBoxFilterName")
			and (listLen(attributes.checkBoxFilterLabel) eq listLen(attributes.checkBoxFilterName))
			>
			<cfloop index="i" list="#attributes.checkBoxFilterName#">
				<!--- if this checkbox is defined in the FORM scope the it was previously checked --->
				<cfif structKeyExists(form,i)><cfset showChecked = TRUE><cfelse><cfset showChecked = FALSE></cfif>
					<cfoutput>
					<td valign="top">#getToken(attributes.checkBoxFilterLabel,listFindNoCase(attributes.checkBoxFilterName,i),",")# &nbsp;&nbsp;
						<CF_INPUT class="form-control" type="checkbox" name="#i#" value="1" Checked="#iif( showChecked,true,false)#">
					</TD>
					</cfoutput>
			</cfloop>
		</cfif>

<!--- ==============================================================================
       FunctionList - designed to support a functionList drop down
=============================================================================== --->
<!---
		<cfif isDefined("attributes.functionListQuery") and isQuery(attributes.functionListQuery)>
			<SCRIPT language = "javascript"  src="/javascript/dropdownFunctions.js"></script>
			<SCRIPT language = "javascript"  src="/javascript/checkBoxFunctions.js"></script>
			<td valign="top"><SELECT NAME="select1" SIZE="1" STYLE="font-family: Arial, Helvetica, sans-serif; font-size: 10px;" onChange="doDropDown(document.FilterSelectForm.select1)">
			<cfloop index="i" list="#attributes.checkBoxFilterName#">
			<!--- security should only show if specified only rows requiring security will be speicified --->
				<cfif application.com.login.checkInternalPermissions ("ApprovalTask", "Level1")  >
					<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') { openWin( '/callTemplate.cfm?template=profileManager/profileProcess.cfm&frmProcessid=RegApproval&frmstepid=0&frmPersonID='+checks,'PopUp','width=600,height=660,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); newWindow.focus() } else {noChecks('Person')}">&nbsp;Phr_Sys_ApprovePer
				</cfif>
			</cfloop></SELECT></TD>
		</cfif>
 --->
	<cfoutput>
	</tr>
	<tr>
		<td id="filterControlButtonsTD" colspan="20">
			<div id="resetTFQOFilterDiv"><cf_input type="button" name="resetBtn" class="btn btn-primary" value="Phr_Sys_Clear" onclick="clearFilters();return false;"></div>
			<div id="submitTFQOFilterDiv"><cf_input type="submit" name="GO" class="btn btn-primary" value="Phr_Sys_GO"></div>
		</td>
	</tr>
	</cfoutput>
<!--- --------------------------------------------------------------------- --->
<!--- query where clause --->
<!--- --------------------------------------------------------------------- --->
				<cfif StructCount( attributes.queryWhereClauseStructure ) neq 0>
					<cfoutput>
					<tr>
					<td colspan="99" align="left">
						<!--- NYF 2008-06-12 CR-LEN548 - (2 lines) --->
						#htmleditformat(attributes.queryWhereClauseLabel)#
						<CF_INPUT type="hidden" name="frmWhereClauseList" value="#form["frmWhereClauseList"]#">
					</cfoutput>
						<cfloop collection="#attributes.queryWhereClauseStructure#" item="whereClauseStructureItem">
								<cfif attributes.selectbyday>

									<cfif structkeyexists(attributes.queryWhereClauseStructure[whereClauseStructureItem][1], "day")>
										<cfoutput>
											<select class="form-control" name="#attributes.queryWhereClauseStructure[whereClauseStructureItem][1].day#">
												<option value="">#attributes.queryWhereClauseStructure[whereClauseStructureItem][1].dayTitle#</option>
												<cfloop index="i" from="1" to="31">
													<option value="#i#"<cfif i EQ #Evaluate(attributes.queryWhereClauseStructure[whereClauseStructureItem][1].day)#>selected</cfif>>#htmleditformat(i)#</option>
												</cfloop>
											</select>
										</cfoutput>
									</cfif>
								</cfif>
							<cfoutput><select name="frmWhereClause#whereClauseStructureItem#" class="form-control" onChange="eventWhereClauseOnChange(this);"></cfoutput>
<!---
							<cfloop collection="#attributes.queryWhereClauseStructure[whereClauseStructureItem]#" item="whereClauseItem">
							<!--- would have looped collection but the keys were out of order!!! --->
 --->
							<cfset tmpKeyList = ListToArray( StructKeyList( attributes.queryWhereClauseStructure[whereClauseStructureItem] ) )>
							<!--- <cfoutput><script>alert("#attributes.queryWhereClauseStructure[whereClauseStructureItem]["1"]["sql"]#");</script></cfoutput> --->


							<!--- <cfif structCount(attributes.queryWhereClauseStructure[whereClauseStructureItem]) gte 2 and structKeyExists(attributes.queryWhereClauseStructure[whereClauseStructureItem],"1")>
								<cfif isDate(attributes.queryWhereClauseStructure[whereClauseStructureItem][2]["sql"]) or isNumeric(attributes.queryWhereClauseStructure[whereClauseStructureItem][2]["sql"])>
									<cfset ArraySort( tmpKeyList, "numeric" )>
								<cfelse>
									<cfset ArraySort( tmpKeyList, "textnocase" )>
								</cfif>
							<cfelse> --->
							<cfset ArraySort( tmpKeyList, "numeric" )>
							<cfparam name="form['frmWhereClause#whereClauseStructureItem#']" default="">
							<!--- </cfif> --->
							<cfloop index="whereClauseItem" list="#ArrayToList( tmpKeyList )#">

								<cfset whereClauseItemSelected = ( ListFindNoCase( form["frmWhereClause#whereClauseStructureItem#"], attributes.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["sql"], "|" ) neq 0 )>
								<cfoutput><option value="#HTMLEditFormat( attributes.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["sql"] )#"#iif( whereClauseItemSelected, de( " selected" ), de( "" ) )#>#attributes.queryWhereClauseStructure[whereClauseStructureItem][whereClauseItem]["title"]#</option></cfoutput>
							</cfloop>
							<cfoutput></select></cfoutput>
						</cfloop>

					<cfoutput>
							<script language="JavaScript1.2" type="text/javascript">
							<!--
							function eventWhereClauseOnChange( callerObject )
							{
							  var whereClauseSuffixArray = new Array( #ListQualify( StructKeyList( attributes.queryWhereClauseStructure ), '"' )# );
							  var whereClauseDelimiter = "";
							  with ( callerObject.form )
							  {
							    elements["frmWhereClauseList"].value = "";
							    for ( whereClauseIndex = 0; whereClauseIndex < whereClauseSuffixArray.length; whereClauseIndex++ )
							    {
							      if ( elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].value != "" )
							      {
							        elements["frmWhereClauseList"].value += whereClauseDelimiter + elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].value;
							        whereClauseDelimiter = "|";
							      }
							      else
							      {
							        elements["frmWhereClause" + whereClauseSuffixArray[whereClauseIndex]].selectedIndex = 0;
							      }
							    }
							  }
							  return true;
							}
							//-->
							</script>
						</td>
						</tr>
					</cfoutput>
				</cfif>
<!--- --------------------------------------------------------------------- --->

	<cfoutput>
	</table>
	</form>
	</div>
	</cfoutput>

	</cfif>

<!--- ==============================================================================
    PAGE CONTROLS SECTION
=============================================================================== --->
	<cfif attributes.HidePageControls eq "no" and not attributes.noform>
	<cfoutput>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="#attributes.tableAlign#" class="table table-hover table-striped table-condensed tfqoShowRecords">

			<tr>
				<td class="noDataTitle"><!---display number of records, if appropriate --->

<!--- --------------------------------------------------------------------- --->
<!--- recycle url parameters except for startrow and numRowsPerPage --->
<!--- --------------------------------------------------------------------- --->
						<cfset queryString = "">
						<cfloop collection="#url#" item="urlItem">
						<cfif urlItem neq "startrow" and urlItem neq "numRowsPerPage">
							<cfset queryString = queryString & "&" & urlItem & "=" & URLEncodedFormat( url[urlItem] )>
						</cfif>
						</cfloop>
<!--- --------------------------------------------------------------------- --->
						<!--- 2015-01-21 AHL Case 443209, add ID attribute to form --->
						<form action="#cgi["script_name"]#?#queryString#" method="post" name="frmRecordPagingForm_#attributes.id#" id="frmRecordPagingForm_#attributes.id#">

						<CF_INPUT type="hidden" name="startRow" value="#startrow#">
						<CF_INPUT type="hidden" name="numRowsPerPage" value="#attributes.numRowsPerPage#">
						<CF_INPUT type="hidden" name="frmSearchColumnDataType" value="#form["frmSearchColumnDataType"]#">
						<CF_INPUT type="hidden" name="frmSearchText" value="#form["frmSearchText"]#">
						<CF_INPUT type="hidden" name="frmSearchColumn" value="#form["frmSearchColumn"]#">
						<CF_INPUT type="hidden" name="frmSearchComparitor" value="#form["frmSearchComparitor"]#">

						<!--- JC: Modified to check for existence of FORM scope and radio variable --->
						<cfif trim(attributes.radioFilterName) neq "">
							<CF_INPUT type="hidden" name="#attributes.radioFilterName#" value="#form[attributes.radioFilterName]#">
						</cfif>

						<CF_INPUT type="hidden" name="filterselect1" value="#form["filterselect1"]#">
						<CF_INPUT type="hidden" name="filterselectvalues1" value="#form["filterselectvalues1"]#">
						<CF_INPUT type="hidden" name="filterselect2" value="#form["filterselect2"]#">
						<CF_INPUT type="hidden" name="filterselectvalues2" value="#form["filterselectvalues2"]#">

						<CF_INPUT type="hidden" name="sortOrder" value="#attributes.sortOrder#">
						<CF_INPUT type="hidden" name="frmFromDate" value="#form["frmFromDate"]#">
						<CF_INPUT type="hidden" name="frmToDate" value="#form["frmToDate"]#">
						<CF_INPUT type="hidden" name="dateFilter" value="#attributes.dateFilter#">
<!--- --------------------------------------------------------------------- --->
<!--- propagate form fields through paging requests --->
<!--- --------------------------------------------------------------------- --->
						<cfif IsStruct( attributes.passThroughVariablesStructure )>
							<cfloop collection="#attributes.passThroughVariablesStructure#" item="formFieldName">
								<cfif ListFindNoCase( "startrow,numRowsPerPage,frmSearchColumnDataType,frmSearchText,frmSearchColumn,frmSearchComparitor,fieldnames", formFieldName ) eq 0>
									<CF_INPUT type="hidden" name="#formFieldName#" value="# attributes.passThroughVariablesStructure[formFieldName] #">
								</cfif>
							</cfloop>
						</cfif>
<!--- --------------------------------------------------------------------- --->
					<cfif attributes.queryObject.recordCount gt 0>
						<div id="recordsFound_#attributes.id#" class="tfqoRecordsFound">
						<CFIF attributes.queryObject.recordCount EQ 0>
							<p>phr_Ext_tablequery_norecordsfound</p>
						<CFELSEIF attributes.queryObject.recordCount EQ 1>
							<!--- 2015-03-16	RPW	Wrapped phrase Ext_tablequery_recordwasfound in <label> to make it display the same as when there are multiple results. --->
							<cfif not request.relaycurrentuser.isinternal><label></cfif><p>1 phr_Ext_tablequery_recordwasfound</p><cfif not request.relaycurrentuser.isinternal></label></cfif>
						<CFELSE>
<!--- --------------------------------------------------------------------- --->
<!--- build argument structure for recordPager --->
<!--- --------------------------------------------------------------------- --->
							<cfset additionalArgsStructure = structCopy(url)> <!--- WAB 2008/04/15 added the struct Copy, otherwise this procedure contaminates the url structure (which is used later on)  --->
							<cfset StructAppend( additionalArgsStructure, form, true )>
							<cfset StructDelete( additionalArgsStructure, "startrow" )>
							<cfset StructDelete( additionalArgsStructure, "fieldnames" )>
							<cfloop collection="#additionalArgsStructure#" item="additionalArg">
								<cfif additionalArgsStructure[additionalArg] eq "">
									<cfset StructDelete( additionalArgsStructure, additionalArg )>
								</cfif>
							</cfloop>
<!--- --------------------------------------------------------------------- --->

								<cfif not request.relaycurrentuser.isinternal>
									<div class="form-horizontal">
									<div class="form-group">
										<div class="col-xs-12 col-sm-3 control-label mailboxLabel"><label>
											<cfif structKeyExists(attributes,"numRowsPerPage") and attributes.numRowsPerPage lt attributes.queryObject.RecordCount>#attributes.numRowsPerPage# phr_Ext_tablequery_of</cfif> #attributes.queryObject.RecordCount# phr_Ext_tablequery_recordsfound.
										</label></div>
								<cfelse>
									<cfif structKeyExists(attributes,"numRowsPerPage") and attributes.numRowsPerPage lt attributes.queryObject.RecordCount>#attributes.numRowsPerPage# phr_Ext_tablequery_of</cfif> #attributes.queryObject.RecordCount# phr_Ext_tablequery_recordsfound.&nbsp;
								</cfif>
									<cf_recordPager
											totalRecordCount="#attributes.queryObject.recordCount#"
											recordsPerPage="#attributes.numRowsPerPage#"
											startRow="#startrow#"
		 									additionalArgs="#additionalArgsStructure#"
											labelText="phr_Gotopage"
											caller="#GetFileFromPath( GetCurrentTemplatePath() )#"
											id="#attributes.id#">
									<CFIF attributes.queryObject.recordCount gt attributes.numRowsPerPage>
								<cfif not request.relaycurrentuser.isinternal>
									</div>
									<!--- 2013-05-16 NYB Case 434909 - added disableShowAllLink - 2014-02-21 NJH Case 438891 added recordCount lte 500--->
									<CFIF not attributes.disableShowAllLink and attributes.queryObject.recordCount lte 500>
									<a href="javascript:void( document.forms['frmRecordPagingForm_#attributes.id#']['startRow'].value='1');void(document.forms['frmRecordPagingForm_#attributes.id#']['numRowsPerPage'].value='#attributes.queryObject.RecordCount#');document.forms['frmRecordPagingForm_#attributes.id#'].submit();">phr_Ext_tablequery_ShowAllRecords #attributes.queryObject.RecordCount#</a>
	<!---
									&nbsp;&nbsp;&nbsp;<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?startRow=1&numRowsPerPage=#attributes.queryObject.RecordCount#">Show All #attributes.queryObject.RecordCount# Records</a><!--- #request.query_string# --->
	 --->
		 							<!--- 2013-05-16 NYB Case 434909 - added --->
									</CFIF>
									</div>
								<cfelse>
									&nbsp;
									<!--- 2013-05-16 NYB Case 434909 - added disableShowAllLink - 2014-02-21 NJH Case 438891 added recordCount lte 500--->
									<CFIF not attributes.disableShowAllLink and attributes.queryObject.recordCount lte 500>
									<a href="javascript:void( document.forms['frmRecordPagingForm_#attributes.id#']['startRow'].value='1');void(document.forms['frmRecordPagingForm_#attributes.id#']['numRowsPerPage'].value='#attributes.queryObject.RecordCount#');document.forms['frmRecordPagingForm_#attributes.id#'].submit();">phr_Ext_tablequery_ShowAllRecords #attributes.queryObject.RecordCount#</a>
	<!---
									&nbsp;&nbsp;&nbsp;<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?startRow=1&numRowsPerPage=#attributes.queryObject.RecordCount#">Show All #attributes.queryObject.RecordCount# Records</a><!--- #request.query_string# --->
	 --->
		 							<!--- 2013-05-16 NYB Case 434909 - added --->
									</CFIF>
								</cfif>

								</cfif>
							</CFIF>
							</div>
						</CFIF>
<!--- --------------------------------------------------------------------- --->
<!--- paging and filtering --->
<!--- --------------------------------------------------------------------- --->
					<cfif ListFindNoCase( attributes.queryObject.ColumnList, attributes.alphabeticalIndexColumn ) neq 0>

						<cfif form["frmAlphabeticalIndexList"] eq "">
							<cfquery name="getAlphabeticalIndex" dbtype="query">
							SELECT DISTINCT alphabeticalIndex
							FROM attributes.queryObject
							ORDER BY alphabeticalIndex ASC
							</cfquery>
							<cfset form["frmAlphabeticalIndexList"] = ValueList( getAlphabeticalIndex.alphabeticalIndex )>
						</cfif>

						<cfif ListLen( form["frmAlphabeticalIndexList"] ) gt 1>
							phr_Ext_tablequery_Showrecordsbeginingwith #attributes.alphabeticalIndexColumn#
							<cfloop index="alphabeticalIndexItem" list="#form["frmAlphabeticalIndexList"]#">
								&nbsp;
								<cfif alphabeticalIndexItem neq form["frmAlphabeticalIndex"]>
									<a href="javascript:void(document.forms['frmRecordPagingForm_#attributes.id#']['frmAlphabeticalIndex'].value='#JSStringFormat( alphabeticalIndexItem )#');document.forms['frmRecordPagingForm_#attributes.id#']['startRow'].value='1';document.forms['frmRecordPagingForm_#attributes.id#'].submit();">
								</cfif>
								#htmleditformat(alphabeticalIndexItem)#
								<cfif alphabeticalIndexItem neq form["frmAlphabeticalIndex"]>
									</a>
								</cfif>
							</cfloop>
							<cfif form["frmAlphabeticalIndex"] neq "">
								&nbsp;
								<a href="javascript:void(document.forms['frmRecordPagingForm_#attributes.id#']['frmAlphabeticalIndex'].value='');document.forms['frmRecordPagingForm_#attributes.id#']['startRow'].value='1';document.forms['frmRecordPagingForm_#attributes.id#'].submit();">*</a>
							</cfif>
						</cfif>
						<CF_INPUT type="hidden" name="frmAlphabeticalIndex" value="# form["frmAlphabeticalIndex"] #">
						<CF_INPUT type="hidden" name="frmAlphabeticalIndexList" value="# form["frmAlphabeticalIndexList"] #">
					</cfif>
<!--- --------------------------------------------------------------------- --->
					</form>
				</TD>
				<!--- <td colspan="2">
					<cfset useurl=rereplace(request.query_string,'&startrow=[0-9][0-9][0-9][0-9]','','all')>
					<form action="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#" method="post" name="navform">
						Show <select name="numRowsPerPage" onchange="document.navform.submit()">
								<option value="#attributes.numRowsPerPage#">Records per page</option>
								<cfif attributes.queryObject.recordCount gt 20>
									<!--- if over 40 rows show various other options --->
									<cfloop index="i" from="4" to="1" step="1">
									<option value="#attributes.numRowsPerPage#/#i#" <cfif attributes.numRowsPerPage eq attributes.numRowsPerPage/i>selected</cfif>>#attributes.numRowsPerPage#/#i#</option>
									</cfloop>
								</cfif>
								<option value="#attributes.queryObject.recordCount#" <cfif attributes.queryObject.recordCount eq attributes.numRowsPerPage>selected</cfif>>ALL</option>
							</select>
						<CFIF IsDefined("FilterSelectValues")>
							<INPUT Name="FilterSelectValues" Type="Hidden" Value="#FilterSelectValues#">
						</CFIF>
						<CFIF IsDefined("FilterSelect")>
							<INPUT Name="FilterSelect" Type="Hidden" Value="#FilterSelect#">
						</CFIF>
						<cfloop index="i" list="#attributes.checkBoxFilterName#">
							<!--- if this checkbox is defined in the FORM scope the it was previously checked --->
							<cfif isDefined("FORM.#i#")>
								<CF_INPUT type="Hidden" name="#i#" value="1">
							</cfif>
						</cfloop>
					</form>
				</td> --->
			</tr>
		</TABLE>
	</CFOUTPUT>
</cfif>

<!--- 2013-07-17 NYB Case 436116 added: --->
<cfif attributes.DisplayMessage neq ""><CFOUTPUT><div class="infoblock">#attributes.DisplayMessage#</div></CFOUTPUT></cfif>

	<cfif not attributes.noForm>
<!--- --------------------------------------------------------------------- --->
<!--- recycle url parameters except for sortOrder --->
<!--- --------------------------------------------------------------------- --->
	<cfset queryString = "">
	<cfloop collection="#url#" item="urlItem">
	<cfif urlItem neq "sortorder">
		<cfset queryString = queryString & "&" & urlItem & "=" & URLEncodedFormat( url[urlItem] )>
	</cfif>
	</cfloop>
<!--- --------------------------------------------------------------------- --->
	<cfoutput>
	<form action="#cgi["script_name"]#?#queryString#" method="post" name="sortForm">
	<CF_INPUT type="hidden" name="startRow" value="#startrow#">
	<CF_INPUT type="hidden" name="numRowsPerPage" value="#attributes.numRowsPerPage#">
	<CF_INPUT type="hidden" name="frmSearchColumnDataType" value="#form["frmSearchColumnDataType"]#">
	<CF_INPUT type="hidden" name="frmSearchText" value="#form["frmSearchText"]#">
	<CF_INPUT type="hidden" name="frmSearchColumn" value="#form["frmSearchColumn"]#">
	<CF_INPUT type="hidden" name="frmSearchComparitor" value="#form["frmSearchComparitor"]#">

	<CF_INPUT type="hidden" name="filterselect" value="#form["filterselect"]#">
	<CF_INPUT type="hidden" name="filterselectvalues" value="#form["filterselectvalues"]#">
	<!--- WAB 2008/10/01 Fixes issue with filterselect1 not being passed when sorting a report--->
	<cfloop index="i" from = 1 to = 3>
		<CF_INPUT type="hidden" name="filterselect#i#" value="#form["filterselect#i#"]#">
		<CF_INPUT type="hidden" name="filterselectvalues#i#" value="#form["filterselectvalues#i#"]#">
	</cfloop>

	<CF_INPUT type="hidden" name="frmAlphabeticalIndex" value="#form["frmAlphabeticalIndex"]#">
	<CF_INPUT type="hidden" name="frmAlphabeticalIndexList" value="#form["frmAlphabeticalIndexList"]#">
	<CF_INPUT type="hidden" name="frmFromDate" value="#form["frmFromDate"]#">
	<CF_INPUT type="hidden" name="frmToDate" value="#form["frmToDate"]#">
	<CF_INPUT type="hidden" name="dateFilter" value="#attributes.dateFilter#">
</cfoutput>
<!--- --------------------------------------------------------------------- --->
<!--- propagate form fields through filtering requests --->
<!--- --------------------------------------------------------------------- --->
		<cfif IsStruct( attributes.passThroughVariablesStructure )>
			<cfloop collection="#attributes.passThroughVariablesStructure#" item="formFieldName">
				<cfif ListFindNoCase( "sortorder,fieldnames", formFieldName ) eq 0>
					<CFOUTPUT><CF_INPUT type="hidden" name="#formFieldName#" value="# attributes.passThroughVariablesStructure[formFieldName] #"></CFOUTPUT>
				</cfif>
			</cfloop>
		</cfif>

<!--- 2008/04/15 WAB PKP copied this from another section because this field not getting propagated  --->
		<!--- JC: Modified to check for existence of FORM scope and radio variable --->
			<cfif trim(attributes.radioFilterName) neq "">
				<cfoutput>
					<CF_INPUT type="hidden" name="#attributes.radioFilterName#" value="#evaluate(attributes.radioFilterName)#">
				</cfoutput>
			</cfif>


<!--- --------------------------------------------------------------------- --->
		<CFOUTPUT><input type="hidden" name="sortOrder" value=""></CFOUTPUT>
<!---
		<CFIF IsDefined("FilterSelectValues")>
			<INPUT Name="FilterSelectValues" Type="Hidden" Value="#FilterSelectValues#">
		</CFIF>
		<CFIF IsDefined("FilterSelect")>
			<INPUT Name="FilterSelect" Type="Hidden" Value="#FilterSelect#">
		</CFIF>
 --->
		<cfloop index="i" list="#attributes.checkBoxFilterName#">
			<!--- if this checkbox is defined in the FORM scope the it was previously checked --->
			<cfif isDefined("FORM.#i#")>
				<CFOUTPUT><CF_INPUT type="Hidden" name="#i#" value="1"> </CFOUTPUT>
			</cfif>
		</cfloop>
		<cfoutput></form></cfoutput>
	</cfif>

</cfif><!--- end of an if (not openasExcel)  --->
</CF_TRANSLATE>
<!--- ==============================================================================
    SORTABLE TITLE SECTION
=============================================================================== --->

<cfsavecontent variable="tableContent">
<CF_TRANSLATE>
<cfif not createAsExcel>
	<cfif not attributes.noForm>
		<cfoutput>
		<form action="" name="#attributes.formName#">
			<cf_sessionTokenField> <!--- 2012-04-23 WAB this form is sometimes posted, by eventFunctionOnChange() function, in which case it needs the session token.  doesn't get it automatically because does not have the POST attribute, so bung the rwSessionToken field onto it manually  --->
		<!--- --------------------------------------------------------------------- --->
		<!--- propagate form fields through filtering requests --->
		<!--- --------------------------------------------------------------------- --->
			<cfif IsStruct( attributes.passThroughVariablesStructure )>
				<cfloop collection="#attributes.passThroughVariablesStructure#" item="formFieldName">
					<cfif ListFindNoCase( "sortorder,fieldnames", formFieldName ) eq 0>
						<CFOUTPUT><CF_INPUT type="hidden" name="#formFieldName#" value="# attributes.passThroughVariablesStructure[formFieldName] #"></CFOUTPUT>
					</cfif>
				</cfloop>
			</cfif>
		</cfoutput>
	</cfif>

<CFOUTPUT>
	<table id="tfqo_#attributes.id#"  data-role="table" data-mode="reflow" class="table table-hover table-striped table-bordered table-condensed #attributes.tableClass#">
</CFOUTPUT>
		<CFOUTPUT>
		<thead>
 		<tr>
		</CFOUTPUT>
			<cfset thisColumnHeadingList = "">
			<cfif attributes.showFunctionList>
				<CFOUTPUT><th><input type="checkbox" id="checkAllRowIdentities" onclick="checkboxSetAll(this.checked)" /></th></CFOUTPUT>
			</cfif>
			<cfloop index="x" from = "1" to = #listlen(showColumnList)#>
				<cfset i = listgetat(showColumnList,x)>
				<cfset thisColumnHeading = listgetat(columnHeadingList,x)>
				<cfif attributes.columnTranslation>
					<!--- 2011/11/28 NYB LHID8224 added replace " " with "_" around thisColumnHeading --->
					<cfset thisColumnHeading = attributes.columnTranslationPrefix & replace(thisColumnHeading," ","_","all")>
				<cfelse>
					<cfset thisColumnHeading = replace(thisColumnHeading,"_"," ","all")>
				</cfif>
<!--- 			<cfloop index="i" list="#showColumnList#"> --->
				<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
					<CFOUTPUT>
					<th columnname="#i#" id="#i#"> <!--- WAB 2010/02/16 added column name for dynamic updating of data  --->
					<cfif attributes.allowColumnSorting eq "yes" and listfindnocase(attributes.doNotSortTheseColumns,i) eq 0> <!--- 2015-11-27 ACPK    PROD2015-457 If column name is listed in doNotSortTheseColumns, disable sorting for this column --->
						<cfif i eq ListFirst( attributes.sortOrder, " " )>
							<cfswitch expression="#ListLast( attributes.sortOrder, " " )#">
							<cfcase value="asc">
								<a href="javaScript:reSort( '#i# DESC' );" class="th_link" title="Sort Z-A">
								<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A" height="11" width="12">
							</cfcase>
							<cfcase value="desc">
								<a href="javaScript:reSort( '#i# ASC' );" class="th_link" title="Sort A-Z">
								<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z" height="11" width="12">
							</cfcase>
							<cfdefaultcase>
								<a href="javaScript:reSort( '#i# DESC' );" class="th_link" title="Sort Z-A">
							</cfdefaultcase>
							</cfswitch>
						<cfelse>
							<a href="javaScript:reSort( '#i# ASC' );" class="th_link" title="Sort A-Z">
						</cfif>
						#application.com.security.sanitiseHTML(thisColumnHeading)#</a>
					<cfelse>
						#application.com.security.sanitiseHTML(thisColumnHeading)#
					</cfif>
					<!--- 2008/06/18 - GCC - solving the fact that re-translating the column headings for every row was lazy coding!
					build a list of already translated column headings to use in cell qTip --->
					<cfset thisColumnHeadingList = listAppend(thisColumnHeadingList,thisColumnHeading)>
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "" and attributes.IllustrativePriceFromCurrency neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							(#attributes.IllustrativePriceFromCurrency#)
						</cfif>
					</cfif>
					</th>
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th>#htmleditformat(thisColumnHeading)# (#htmleditformat(attributes.IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
						</cfif>
					</cfif>
					</CFOUTPUT>
				</cfif>
			</cfloop>
	<CFOUTPUT>
		</tr>
		</thead>
	</cfoutput>
<cfelse>
	<!--- just show simple headings if in Excel --->
	<cfoutput>
	<cfset thisColumnHeadingList = "">
	<table  border="0" cellspacing="1" cellpadding="3" align="#attributes.tableAlign#" class="#attributes.tableClass# table table-hover table-striped table-condensed">

 		<tr><cfset rowCount = rowCount + 1>
			<cfloop index="x" from = "1" to = #listlen(showColumnList)#>
				<cfset i = listgetat(showColumnList,x)>

				<!--- 2008/07/09 WAB excel columns weren't being translated --->
				<cfset thisColumnHeading = listgetat(columnHeadingList,x)>
				<cfif attributes.columnTranslation>
					<cfset thisColumnHeading = attributes.columnTranslationPrefix & thisColumnHeading>
				<cfelse>
					<cfset thisColumnHeading = replace(thisColumnHeading,"_"," ","all")>
				</cfif>

				<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
					<th>
						#htmleditformat(thisColumnHeading)#<!--- #replace(i,"_"," ","all")# ---> <!--- NJH 2009/05/12 CR-SNY675 - changed to use thisColumnHeading as it wasn't getting translated--->
					<!--- 2008/06/18 - GCC - solving the fact that re-translating the column headings for every row was lazy coding!
					build a list of already translated column headings to use in cell qTip --->
					<cfset thisColumnHeadingList = listAppend(thisColumnHeadingList,thisColumnHeading)>
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0 and attributes.IllustrativePriceFromCurrency neq "">
							(#attributes.IllustrativePriceFromCurrency#)
						</cfif>
					</cfif>
					</th>
					<!--- CR_ATI031 add additional headings for illustrative columns--->
					<cfif attributes.IllustrativePriceColumns neq "">
						<cfif listFindNoCase(attributes.IllustrativePriceColumns,i,",") neq 0>
							<th>#htmleditformat(thisColumnHeading)# (#htmleditformat(attributes.IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
		</tr>
	</cfoutput>
</cfif>

</cf_translate>
<cfif attributes.columnTranslation>
	<cf_translate phrases="#thisColumnHeadingList#"/>
</cfif>

<!--- ==============================================================================
    MAIN DATA TABLE SECTION
=============================================================================== --->

<cf_translate>
	<cfif attributes.queryObject.recordCount gt 0 and listLen(attributes.GroupByColumns) eq 0>
		<cfset newGroup = true>
		<!--- if there are no GroupByColumns defined  --->
		<!--- WAB 2010/09/06 moved higher up code, needed for cf_translatequerycolumn <cfset endRow = startRow + attributes.numRowsPerPage> --->
		<cfloop query="attributes.queryObject" startrow="#startrow#" endrow = "#endrow#">
			<cfoutput>
				<!--- NYB 2011-03-03 LHID4944: added 'replace " with &quot;' to id entry --->
				<cfif attributes.rowIdentityColumnName is not "">
					<cfset variables.IDValue = Replace(attributes.queryObject[attributes.rowIdentityColumnName][attributes.queryObject.CurrentRow],'"','&quot;',"all")>
					<cfif attributes.encryptRowIdentity>
						<cfset variables.IDValue = application.com.security.encryptVariableValue(attributes.rowIdentityColumnName,variables.IDValue)>
					</cfif>
				</cfif>
				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF> <cfif attributes.rowIdentityColumnName is not "">id="#htmleditformat(attributes.rowIdentityColumnName)#=#variables.IDValue#" </cfif>>  <!--- WAB 2010/02/16 ;  WASB 2010/03/15 added cfif attributes.rowIdentityColumnName is not "" --->
			</cfoutput>
			<cfset rowCount = rowCount + 1>
			<cfif newGroup><cfset excelBeginCount = rowCount><cfset newGroup = false></cfif>
			<!--- --------------------------------------------------------------------- --->
			<!--- function select --->
			<!--- --------------------------------------------------------------------- --->
						<cfif attributes.showFunctionList and createAsExcel eq false>
							<cfoutput><td valign="top"><CF_INPUT type="checkbox" name="frmRowIdentity" value="#attributes.queryObject[attributes.rowIdentityColumnName][attributes.queryObject.CurrentRow]#" checked="#iif(false,true,false)#" style="height: 14; width: 14;" encrypt="#attributes.encryptRowIdentity#"></td></cfoutput>
						</cfif>
			<!--- --------------------------------------------------------------------- --->
			<cfloop index="i" list="#showColumnList#">
				<cfset cellValue = attributes.queryObject[i][currentRow]>								<!--- 2011/11/15 PPB LID7090 save cellValue to use as x:num --->
				<cfset cellRendition = "" />
				<cfset cellType = 0 />
				<cfif attributes.showCellColumnHeadings eq "yes" and attributes.columnTranslation>
					<cfset thisTitleText = evaluate(gettoken(thisColumnHeadingList,listfindnocase(showColumnList,i),","))>
				<cfelse>
					<cfset thisTitleText = gettoken(thisColumnHeadingList,listfindnocase(showColumnList,i),",")>
				</cfif>

				<!--- START 2011/10/26 PPB added support for tooltips --->
				<cfset thisColumnHasToolTip = false>
				<cfif structKeyExists(attributes,"toolTipColumnList") and structKeyExists(attributes,"toolTipContentColumnList")
						and (listLen(attributes.toolTipColumnList) eq listLen(attributes.toolTipContentColumnList))
						and listFindNoCase(attributes.toolTipColumnList,i) neq "0">

					<cfset thisTitleText=#evaluate(gettoken(attributes.toolTipContentColumnList,listFindNoCase(attributes.toolTipColumnList,i),","))#>
					<cfset thisColumnHasToolTip = true>
				</cfif>
				<!--- END 2011/10/26 PPB added support for tooltips --->


				<!--- NJH 2009/05/12 CR-SNY675-1 - actually remove columns when hiding columns --->
				<cfif listFindNoCase(attributes.hideTheseColumns,i) neq 0>

				<cfelseif structKeyExists(attributes,"keyColumnList") and structKeyExists(attributes,"keyColumnURLList")
						and (listLen(attributes.keyColumnList) eq listLen(attributes.keyColumnURLList))
						and listFindNoCase(attributes.keyColumnList,i) neq "0">

					<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags --->
					<cfscript>
						if (ListFirst(i,"_")=="flag") {
							variables.secondaryData = "";
							if (StructKeyExists(attributes.queryObject,i & "B")) {
								variables.secondaryData = attributes.queryObject[i&"B"][currentRow];
							}
							cellValue = application.com.flag.FormatFlagData(flagID=ListLast(i,"_"),flagValue=cellValue,secondaryData=variables.secondaryData);
						}
					</cfscript>

					<CFSET thiskeyColumnURL=gettoken(attributes.keyColumnURLList,listFindNoCase(attributes.keyColumnList,i),",")>
					<CFSET thiskeyColumnKey=gettoken(attributes.keyColumnKeyList,listFindNoCase(attributes.keyColumnList,i),",")>

					<cfset thiskeyColumnTarget = "">
					<cfif structKeyExists(attributes,"keyColumnTargetList") and listLen(attributes.keyColumnTargetList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnTarget = gettoken(attributes.keyColumnTargetList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

					<cfset thiskeyColumnOpenInWindow = "no">
					<cfif structKeyExists(attributes,"keyColumnOpenInWindowList") and listLen(attributes.keyColumnOpenInWindowList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnOpenInWindow = gettoken(attributes.keyColumnOpenInWindowList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

					<cfset thiskeyColumnOpenWinName = "no">
					<cfif structKeyExists(attributes,"OpenWinNameList") and listLen(attributes.OpenWinNameList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnOpenWinName = gettoken(attributes.OpenWinNameList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

					<cfset thiskeyColumnOpenWinSettings = "no">
					<cfif structKeyExists(attributes,"OpenWinSettingsList") and listLen(attributes.OpenWinSettingsList,";") eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnOpenWinSettings = gettoken(attributes.OpenWinSettingsList,listFindNoCase(attributes.keyColumnList,i),";")>
					</cfif>

					<cfset thiskeyColumnIcon = "null">
					<cfif structKeyExists(attributes,"keyColumnIconList") and listLen(attributes.keyColumnIconList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnIcon = gettoken(attributes.keyColumnIconList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

					<!--- 2008/07/02 WAB added keyColumnOnClickList  --->
					<cfset thiskeyColumnOnClick = "">
					<cfif structKeyExists(attributes,"keyColumnOnClickList") and listLen(attributes.keyColumnOnClickList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnOnClick = trim(gettoken(attributes.keyColumnOnClickList,listFindNoCase(attributes.keyColumnList,i),","))>
						<cfif thiskeyColumnOnClick is not ""><cfset thiskeyColumnOnClick = evaluate('"#replacenocase(thiskeyColumnOnClick,"*comma",",","ALL")#"')></cfif>
					</cfif>

					<!--- NJH 2010/04/28 -P-PAN002 - pass in how to display the link --->
					<cfset thisKeyColumnLinkDisplay = "link">
					<cfif structKeyExists(attributes,"KeyColumnLinkDisplayList") and listLen(attributes.KeyColumnLinkDisplayList) eq listLen(attributes.keyColumnList)>
						<cfset thisKeyColumnLinkDisplay = getToken(attributes.KeyColumnLinkDisplayList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

					<cfset thiskeyColumnContextMenu = "">
					<cfif structKeyExists(attributes,"keyColumnContextMenuList") and listLen(attributes.keyColumnContextMenuList) eq listLen(attributes.keyColumnList)>
						<cfset thiskeyColumnContextMenu = gettoken(attributes.keyColumnContextMenuList,listFindNoCase(attributes.keyColumnList,i),",")>
						<cfif structKeyExists(attributes,"keyColumnContextMenuIDExpressionList") and listLen(attributes.keyColumnContextMenuIDExpressionList) eq listLen(attributes.keyColumnList)>
							<cfset thisKeyColumnContextMenuIDExpression = gettoken(attributes.KeyColumnContextMenuIDExpressionList,listFindNoCase(attributes.keyColumnList,i),",")>
						</cfif>
					</cfif>

					<!--- NJH 2014/06/17 - Case 435831/Task Core-64 - certifications relaytag --->
					<cfset thisKeyColumnOpenAsModal = "no">
					<cfif structKeyExists(attributes,"keyColumnOpenAsModalList") and listLen(attributes.keyColumnOpenAsModalList) eq listLen(attributes.keyColumnList)>
						<cfset thisKeyColumnOpenAsModal = getToken(attributes.keyColumnOpenAsModalList,listFindNoCase(attributes.keyColumnList,i),",")>
					</cfif>

<!--- --------------------------------------------------------------------- --->
<!--- substitute parameter --->
<!--- --------------------------------------------------------------------- --->
					<!--- use for more than 1 param --->
					<cfset thiskeyColumnURL = replacenocase(thiskeyColumnURL,"*comma",",","ALL")>   <!--- a hack WAB put in because he needed a comma in a URL, but couldn't get it in via the list --->
					<cfif listLen(thiskeyColumnKey,"|") gt 1>
						<cfif listLen(thiskeyColumnURL,"|") eq listLen(thiskeyColumnKey,"|")>
							<cfset tmpURL = "">
							<cfset lIncr = 1>
							<cfloop list="#thiskeyColumnKey#" index="item" delimiters="|">
								<cfif tmpURL eq "">
									<cfset tmpURL = "#listGetAt(thiskeyColumnURL,lIncr,'|')##evaluate(listGetAt(thiskeyColumnKey,lIncr,'|'))#">
								<cfelse>
									<cfset tmpURL = "#tmpURL#&#listGetAt(thiskeyColumnURL,lIncr,'|')##evaluate(listGetAt(thiskeyColumnKey,lIncr,'|'))#">
								</cfif>
								<cfset lIncr = lIncr+1>
							</cfloop>
							<cfset URLToGo = tmpURL>
						<cfelse>
							<cfset URLToGo = "#listGetAt(thiskeyColumnURL,1,'|')##evaluate(listGetAt(thiskeyColumnKey,1,'|'))#">
						</cfif>
					<cfelse>
						<cfif FindNoCase( "thisurlkey", thiskeyColumnURL ) neq 0>
							<cfset URLToGo = ReplaceNoCase( thiskeyColumnURL, "thisurlkey", JSStringFormat( evaluate(thiskeyColumnKey) ), "one" )>
							<!--- <td valign="top"><cfif createAsExcel eq false><a href="#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#" CLASS="smallLink"<cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>>#evaluate(i)#</a>&nbsp;<cfelse>#evaluate(i)#</cfif></td> --->
						<cfelse>
							<!--- WAB 2009/09/14 LID 2614 removed urlencoded format because not required (ie breaks)if the url is going to be encrypted <cfset URLToGo = "#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#"> --->
							<cfset URLToGo = "#thiskeyColumnURL##evaluate(thiskeyColumnKey)#">
							<!--- <td valign="top"><cfif createAsExcel eq false><a href="#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#" CLASS="smallLink"<cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>>#evaluate(i)#</a>&nbsp;<cfelse>#evaluate(i)#</cfif></td> --->
						</cfif>
					</cfif>

					<!--- Start P-fnl069 SSS 2009/07/20 encryptingurl params in tfqo --->
					<!--- NJH 2009/08/28 LID 2591 - don't encrypt url if it's a javascript function --->
					<cfif isdefined("URLToGo") and not find("javascript:",URLToGo) and trim(URLToGo) neq "">
						<cfset urltogo = application.com.security.encryptURL (URLToGo)>
							<!--- 	WAb 2009/09/15 removed all of this and replaced with a single function call
						<cfset path = gettoken(URLToGo,1,"?")>
						<cfset urlqueryString = '?' & gettoken(URLToGo,2,"?")>
						<cfset encryptedUrlVariables = application.com.security.encryptQueryString(queryString=urlqueryString)>
						<cfset urltogo = path & encryptedUrlVariables>
							--->
					</cfif>

					<!--- End P-fnl069 SSS 2009/07/20 encryptingurl params in tfqo --->
					<cfif thiskeyColumnOpenInWindow>
						<!--- START 2010/11/10			NAS		LID4706: CR015.2 : Performance/Usability CR --->
						<cfif left(thiskeyColumnOpenWinName, 4) EQ "Tab_" >
							<cf_includeJavascriptOnce template="/javascript/extExtension.js">
							<cfif thiskeyColumnOpenWinSettings eq "no">
								<cfset winSettings="">
							<cfelse>
								<cfset winSettings=thiskeyColumnOpenWinSettings>
							</cfif>
							<cfset hrefValue = "javascript:void(openNewTab('#listRest(thiskeyColumnOpenWinName,'_')#', '#listrest(thiskeyColumnOpenWinName,'_')#', '#URLToGo#',{#winSettings#}));">
						<cfelse>
							<cfif thiskeyColumnOpenWinName eq "no">
								<cfset winName="MyWindow">
							<cfelse>
								<cfset winName=thiskeyColumnOpenWinName>
							</cfif>
							<cfif thiskeyColumnOpenWinSettings eq "no">
								<cfset winSettings="width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">
							<cfelse>
								<cfset winSettings=thiskeyColumnOpenWinSettings>
							</cfif>

							<cfset hrefValue = "javascript:void(openWin('#URLToGo#','#winName#','#winSettings#'));">
						</cfif>
					<!--- STOP 2010/11/10			NAS		LID4706: CR015.2 : Performance/Usability CR --->
					<cfelse>
						<cfset hrefValue = URLToGo>
					</cfif>

					<!--- 2008/07/02 WAB added keyColumnOnClick  --->
					<cfoutput><!--- <td valign="top"><cfif createAsExcel eq false><a href="#hrefValue#" <cfif attributes.showCellColumnHeadings eq "yes">title="#thisTitleText#"</cfif> CLASS="smallLink" <cfif trim(thiskeyColumnOnClick) neq ""> onclick="#thiskeyColumnOnClick#" </cfif>  <cfif trim(thiskeyColumnContextMenu) neq ""> onclick = "javascript:fnLeftClickContextMenu (event) " contextmenu="#thiskeyColumnContextMenu#" id="tbqo--#attributes.id#--#evaluate(thisKeyColumnContextMenuIDExpression)#--#i#"</cfif>  <cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>><cfif thiskeyColumnIcon neq "null"><img src="/#thiskeyColumnIcon#" border="0" alt="#evaluate(i)#"><cfelse>#evaluate(i)#</a></cfif><cfelse>#evaluate(i)#</cfif></td> --->
						<!--- NJH 2010/0428 P-PAN002 - can now display links in various ways... as a button or a link at the moment --->
						<td class="#i#">
							<cfif createAsExcel eq false>
								<cfset showLink = true>
								<cfset showLinkAs = "link">
								<cfif isDefined('show#i#')>		<!--- if there is a column returned in the query called 'show' + the column name of the button (eg. showRenewButton) then use it to dictate whether the link/button is displayed  --->
									<cfset showLink = evaluate('show#i#')>
								</cfif>
								<cfif isDefined('show#i#As')>		<!--- if there is a column returned in the query called 'show'+columnName+'As' (eg. showRenewButtonAs) then use it to dictate whether the output is displayed as text or as a link  --->
									<cfset showLinkAs = evaluate('show#i#As')>
								</cfif>
								<cfif showLinkAs eq "text">
									#application.com.security.sanitiseHTML(cellValue)#
								<cfelse>
									<!--- NJH 2014/06/19 Case 435831 - add modal --->
									<cfif thisKeyColumnLinkDisplay eq "link"><cfif showLink><a href="#hrefValue#" <cfif attributes.showCellColumnHeadings eq "yes">title="#htmleditformat(thisTitleText)#"</cfif> class="smallLink <cfif thisKeyColumnOpenAsModal>modal</cfif>" <cfif trim(thiskeyColumnOnClick) neq ""> onclick="#htmleditformat(thiskeyColumnOnClick)#" </cfif> <cfif thiskeyColumnTarget neq ""> target="#htmleditformat(thiskeyColumnTarget)#"</cfif>><cfif thiskeyColumnIcon neq "null"><img src="/#thiskeyColumnIcon#" border="0" alt="#cellValue#"><cfelse>#application.com.security.sanitiseHTML(cellValue)#</cfif></a></cfif>
									<cfelseif thisKeyColumnLinkDisplay eq "button"><cfif showLink><CF_INPUT class="btn btn-primary" type="button" onClick="#thiskeyColumnOnClick#" value="#cellValue#"/></cfif>
									</cfif>
								</cfif>
							<cfelse>
								#application.com.security.sanitiseHTML(cellValue)#
							</cfif>
						</td>
					<!--- <cfif attributes.showCellColumnHeadings eq "yes">onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()"</cfif> --->

					</cfoutput>
<!--- --------------------------------------------------------------------- --->
				<cfelseif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,i) neq "0">
					<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,i))>
					<cfset cellRendition = LSNumberFormat(cellValue,"#NumberFormatMaskValue#") />
					<cfset cellType = 1 />

				<cfelseif structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,i) neq "0">

					<!--- NJH 2010/03/11 P-PAN002 - if using rollUpCurrency, do the conversion --->
					<cfset cellValue = numberFormat(cellValue,"0.00")>
					<cfset cellRendition = cellValue>
					<cfset excelClass = "xl25">
					<cfset cellType = 2 /> <!--- Currency --->

					<cfif attributes.rollUpCurrency>

						<cfset originalValue = cellValue>

						<cfscript>
							fromCurrency = attributes.queryObject[attributes.rollUpPriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
							rollUpPrice = application.com.currency.convert(
								method = 2,
								fromValue = originalValue,
								fromCurrency = fromCurrency,
								toCurrency = attributes.rollUpPriceToCurrency,
								date = ''
								);
						</cfscript>

						<!--- need to set the query column value for totalling down below, as we need to total on the new values... --->
						<cfset attributes.queryObject[i][attributes.queryObject.CurrentRow] = rollUpPrice.toValueNumeric>
						<cfif not attributes.showCellColumnHeadings>
							<cfset thisTitleText = "">
						</cfif>
						<cfset thisTitleText=thisTitleText&" ("&rollUpPrice.fromSign&decimalFormat(originalValue)&")">

						<cfif rollUpPrice.conversionRate eq "">
							<cfset exchangeRateExists = false>
						</cfif>

						<cfset cellValue = rollUpPrice.toValue>				<!--- 2011/08/10 PPB LID7090 save cellValue to use as x:num --->

						<cfif not createAsExcel>
							<cfset cellRendition = rollUpPrice.toSign & rollUpPrice.toValue>
						<cfelse>
							<cfset excelClass = excelCurrencyClass>
							<cfset cellRendition = rollUpPrice.toValue>
						</cfif>
					<cfelseif structKeyExists(currencyStruct,i) >
						<cfif not createAsExcel>
							<cfset cellRendition = currencyStruct[i].currencySign & lsNumberFormat(cellValue,"9,999.99")> <!--- case 435341 NJH 2013/06/03 --->

						<cfelse>
							<cfset excelClass = currencyStruct[i].currencyMSO>
						</cfif>


					<cfelseif structKeyExists (attributes,"currencyISOColumn") and listLen(attributes.currencyISOColumn) eq listLen(attributes.currencyFormat)>
						<cfset currencyISO = evaluate (listgetat(attributes.currencyISOColumn,listfindnocase(attributes.currencyFormat,i)))>

						<cfif not createAsExcel>
							<cfset cellRendition = lsNumberFormat(cellValue,"9,999.99") & " " & currencyISO> <!--- case 435341 NJH 2013/06/03 --->
						</cfif>

					</cfif>


				<!--- NJH 2008/09/02 added a boolean format - added during elearning module development for R8.1 --->
				<cfelseif structKeyExists(attributes,"booleanFormat") and listFindNoCase(attributes.booleanFormat,i) neq "0">
					<cfif isBoolean(cellValue)>
						<!--- 2012/11/13 YMA Case 429879. Update TFQO to get translations before entering loop--->
						<cfif cellValue eq 1>
							<cfset cellRendition = yesTranslation>
						<cfelse>
							<cfset cellRendition = noTranslation>
						</cfif>
					<cfelse>
						<cfset cellRendition = cellValue>
					</cfif>
					<cfset cellType = 5 /> <!--- NJH 2009/01/02 CR-LEX581 bug Fix - cell type was 2 which is a date rather than a string --->
				<cfelseif structKeyExists(attributes,"dateFormat") and listFindNoCase(attributes.dateFormat,i) neq "0">
					<!--- <cfset cellRendition = dateFormat(evaluate(i),"dd-mmm-yy") /> --->
					<!--- TODO WAB 2008/05/21 NABU problem, please check beore releasing trying to sort out US date formatting problems.  If opening as excel we don't need to format the date, just tell excel that it is  a date and excel will do the rest  - we hope  - needs testng on a US machine--->
					<!--- 2011/08/04 NAS but we need to format when we pass a date timestamp rather than a string --->
					<!--- 2016-06-02 ESZ Case 449146 Translated Dates in Certificates  --->
					<cfset dateFormatMaskValue = getDateFormatMaskValue(dateFormatMask = attributes.dateFormatMask, columnPosition = listFindNoCase(attributes.dateFormat,i))>
					<cfif not createAsExcel><cfset cellRendition = lsdateFormat(cellValue,"#dateFormatMaskValue#") /><cfelse><cfset cellRendition = #dateformat(cellValue,"yyyy/mm/dd")# /></cfif>
					<cfset cellType = 3 />
				<cfelseif structKeyExists(attributes,"dateTimeFormat") and listFindNoCase(attributes.dateTimeFormat,i) neq "0"> <!--- NJH 2009/01/12 - created as part of scheduled task monitor report as time as important as well as the date --->
					<cfif not createAsExcel and isDate(cellValue)>
						<cfset cellRendition = application.com.dateFunctions.dateTimeFormat(date=cellValue,showAsLocalTime=attributes.showAsLocalTime,showIcon=false)>
						<!--- <cfset cellRendition = "#lsdateFormat(cellValue,"medium")# #lsTimeFormat(cellValue,"medium")#" /> --->
					<cfelse>
						<cfset cellRendition = cellValue />
					</cfif>
					<cfset cellType = 3 />
				<!---START: 2011/03/16 added to handle timeformat --->
				<cfelseif structKeyExists(attributes,"TimeFormat") and listFindNoCase(attributes.TimeFormat,i) neq "0"> <!--- NJH 2009/01/12 - created as part of scheduled task monitor report as time as important as well as the date --->
					<cfif not createAsExcel and isDate(cellValue)><cfset cellRendition = "#lsTimeFormat(cellValue,"medium")#" /><cfelse><cfset cellRendition = cellValue /></cfif>
					<cfset cellType = 6 />
				<!---END: 2011/03/16 added to handle timeformat --->
				<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags --->
				<cfelseif structKeyExists(attributes,"financialFormat") and listFindNoCase(attributes.financialFormat,i) neq "0">
					<cfscript>
						if (ListFirst(i,"_")=="flag") {
							variables.secondaryData = "";
							if (StructKeyExists(attributes.queryObject,i & "B")) {
								variables.secondaryData = attributes.queryObject[i&"B"][currentRow];
							}
							cellRendition = application.com.flag.FormatFlagData(flagID=ListLast(i,"_"),flagValue=cellValue,secondaryData=variables.secondaryData);
						}
						cellType = 2;
					</cfscript>
				<cfelseif not isnumeric(i)>
					<cfset cellRendition = cellvalue />
					<cfset cellType = 5 />
				<cfelse>
					<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
						<cfset cellRendition = cellValue />
						<cfset cellType = 4 />
					</cfif>
				</cfif>

				<cfif cellType neq 0>
	<!--- --------------------------------------------------------------------- --->
	<!--- extra cell attributes for Excel --->
	<!--- --------------------------------------------------------------------- --->
								<cfswitch expression="#cellType#">
								<!--- need a case here for type 1 - number format --->
									<cfcase value="1">
										<cfset excelCellAttribute = ' x:num="' & TRIM(attributes.queryObject[i][attributes.queryObject.CurrentRow]) & '"' />
									</cfcase>
									<cfcase value="2">
										<!--- NJH 2010/07/29 replaced with below <cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '" class="' & excelClass & '"' /> --->
										<!--- 2011/08/05 PPB LID7090 use cellValue to strip off currency formatting so that XL doesn't receive it as text and not be able to calculate totals on it --->
										<cfset excelCellAttribute = ' x:num="' & cellValue & '" class="' & excelClass & '"' />
									</cfcase>
									<cfcase value="3">
										<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
											<!--- <cfset excelCellAttribute = ' x:num="' & DateDiff( "d", CreateDate( 1900, 1, 1 ), attributes.queryObject[i][attributes.queryObject.CurrentRow] ) & '" class=xl24' />
											 <cfset excelCellAttribute = ' x:num="' & trim(dateFormat(attributes.queryObject[i][attributes.queryObject.CurrentRow],"dd/mm/yyyy")) & '" class=xl24' />
											 ---><cfset excelCellAttribute = ' class="xl24"' />
										<cfelse>
											<cfset excelCellAttribute = '' />
										</cfif>
									</cfcase>
								<!---START: 2011/03/16 added to handle timeformat --->
								<cfcase value="6">  <!--- time --->
									<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
										<cfset excelCellAttribute = ' class="xlTime"' />
									<cfelse>
										<cfset excelCellAttribute = '' />
									</cfif>
								</cfcase>
								<!---END: 2011/03/16 added to handle timeformat --->
									<cfdefaultcase>
										<cfset excelCellAttribute = '' />
									</cfdefaultcase>
								</cfswitch>
	<!--- --------------------------------------------------------------------- --->
					<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()"</cfif> --->
					<!--- NYB 2011-09-16 P-SNY106 added class="#i#" --->
					<cfoutput><td <!--- <cfif celltype eq 1 or celltype eq 2>align="right"</cfif> valign="top" ---> class="#i#" #excelCellAttribute#><span <cfif attributes.showCellColumnHeadings eq "yes" or (attributes.rollUpCurrency and cellType eq 2) or thisColumnHasToolTip>title="#thisTitleText#"</cfif>>#cellRendition#</span></cfoutput>
	<!--- --------------------------------------------------------------------- --->
	<!--- table inclusion --->
	<!--- --------------------------------------------------------------------- --->
	<!---
					<cfif ListFindNoCase( attributes.columnInclusionList, i ) neq 0>
						<cfinclude template="#ListGetAt( attributes.columnInclusionTemplateList, ListFindNoCase( attributes.columnInclusionList, i ) )#">
					</cfif>
	 --->
					<cfif IsDefined( "caller.udfTableCallBack" ) and IsCustomFunction( caller.udfTableCallBack )>
						<cfoutput>#caller.udfTableCallBack( i, attributes.queryObject[i][attributes.queryObject.CurrentRow], cellType )#</cfoutput>
					</cfif>
	<!--- --------------------------------------------------------------------- --->
					<cfoutput></td>
					</cfoutput>
					<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
					<cfif attributes.IllustrativePriceColumns neq ""
						and listFindNoCase(attributes.IllustrativePriceColumns,i) gt 0
						and listFindNoCase("1,2",celltype)>
						<cfswitch expression="#attributes.IllustrativePriceMethod#">
							<cfcase value="2">
								<cfscript>
									if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
										exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
									} else {
										exchangeDate = '';
									};
									/* NJH P-PAN002 2010/03/09 - if IllustrativePriceFromCurrencyColumn has been set, use it. Used for rollup reporting */
									fromCurrency = attributes.IllustrativePriceFromCurrency;
									/*if (attributes.IllustrativePriceFromCurrencyColumn neq "") {
										fromCurrency = attributes.queryObject[attributes.IllustrativePriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
									}*/
									IllustrativePrice = application.com.currency.convert(
										method = #attributes.IllustrativePriceMethod#,
										fromValue = cellValue,
										fromCurrency = fromCurrency,
										toCurrency = attributes.IllustrativePriceToCurrency,
										date = '#exchangeDate#'
										);
								</cfscript>

								<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
							</cfcase>
						</cfswitch>
						<cfswitch expression="#cellType#">
							<cfcase value="1">
								<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '"' />
							</cfcase>
							<cfcase value="2">
								<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '" class="' & excelCurrencyClass & '"' />
							</cfcase>
						</cfswitch>
						<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
						<cfoutput><td valign="top"#excelCellAttribute#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#thisTitleText#"</cfif>>#IllustrativePriceOutput#</span></td></cfoutput>
					</cfif>
				</cfif>

			</cfloop>
			<cfoutput></tr></cfoutput>
		</cfloop>

		<cfif not newGroup><cfset excelEndCount = rowCount><cfset newGroup = true></cfif>
<!--- ==============================================================================
	    GROUP BY SECTION
	=============================================================================== --->
	<cfelse>
		<!--- if there ARE GroupByColumns defined  --->
		<cfif attributes.queryObject.recordCount gt 0>

			<cfif listLen(attributes.totalTheseColumns) neq 0>
				<!--- keeps track of subtotal cells for excel grand total formula --->
				<cfset excelSubTotals = structNew()>
				<cfset subTotals = structNew()>
				<cfloop list="#attributes.totalTheseColumns#" index="col">

					<cfset subTotals[col] = structNew()>
					<cfset excelSubTotals[col] = structNew()>
				</cfloop>
			</cfif>
			<CFSET thisGroupByColumn=attributes.GroupByColumns>
			<cfset incr = 1>
			<cfset subIncr = 1>

			<!--- PPB 2010/08/04 P-PAN002 added the facility to be able to group by 2 columns;
			 this was done to be able to group the pipeline report by Currency and Status; this functionality was later removed hence at time of writing this facility isn't used
			 nb. because we don't want to total on a mix of currencies, it currently isn't possible to group on 2 columns and show grand totals without extra work below
			  --->

			<cfset groupBy1 = ListFirst(attributes.GroupByColumns)>
			<cfif listlen(attributes.GroupByColumns) eq 2>
				<cfset groupBy2 =  listGetAt(attributes.GroupByColumns,2)>
			<cfelse>
				<cfset groupBy2 =  groupBy1>		<!--- when grouping by only 1 column (ie normal mode) we 'cheat' by grouping by that column twice --->
			</cfif>


			<cfoutput query="attributes.queryObject" group="#groupBy1#" startrow="#startrow#">
			<cfoutput group="#groupBy2#">

				<cfif isDefined("subTotals")>
					<cfloop list="#attributes.totalTheseColumns#" index="col">
						<cfloop list="#attributes.GroupByColumns#" index="groupCol">
							<cfset subTotals[col][evaluate(groupCol)] = 0>
							<cfset excelSubTotals[col][evaluate(groupCol)] = 0>
						</cfloop>
					</cfloop>
				</cfif>
				<!--- group by section --->
				<cfif not isDefined("nextAction")>
					<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
					<cfif structKeyExists(attributes,"IllustrativePriceColumns")>
						<cfset groupBySpan = #listLen(showColumnList)# + #listLen(attributes.IllustrativePriceColumns)#>
					<cfelse>
						<cfset groupBySpan = #listLen(showColumnList)#>
					</cfif>
				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>><cfset rowCount = rowCount + 1>
					<TD colspan="#groupBySpan#">
						<cfif not createAsExcel><BR></cfif>
						<strong>
						<cfloop list="#attributes.GroupByColumns#" index="groupCol">
							#htmleditformat(evaluate(groupCol))#
						</cfloop>
						</strong>
					</TD>
				</TR>
				</cfif>
				<cfset newGroup = true>
				<cfoutput>
					<cfif incr lte attributes.numRowsPerPage>
						<cfset incr = incr + 1>
						<cfset subIncr = subIncr + 1>
						<!--- <tr><td colspan="#listLen(showColumnList)#">incr: #incr# - numRowsPerPage: #attributes.numRowsPerPage# - incr lte attributes.numRowsPerPage? : #incr lte attributes.numRowsPerPage#</td></tr> --->
						<!--- detail rows --->
						<cfif attributes.rowIdentityColumnName is not "">
							<cfset variables.IDValue = Replace(attributes.queryObject[attributes.rowIdentityColumnName][attributes.queryObject.CurrentRow],'"','&quot;',"all")>
							<cfif attributes.encryptRowIdentity>
								<cfset variables.IDValue = application.com.security.encryptVariableValue(attributes.rowIdentityColumnName,variables.IDValue)>
							</cfif>
						</cfif>
						<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF> <cfif attributes.rowIdentityColumnName is not "">id="#htmleditformat(attributes.rowIdentityColumnName)#=#variables.IDValue#" </cfif>> 
						<cfset illustrativePriceAlphaIncr = 1>
						<cfset rowCount = rowCount + 1>
						<cfif newGroup><cfset excelBeginCount = rowCount><cfset newGroup = false></cfif>
						<cfloop index="i" list="#showColumnList#">
							<cfset cellValue = attributes.queryObject[i][currentRow]>								<!--- 2011/11/15 PPB LID7090 save cellValue to use as x:num --->
							<cfset cellRendition = "" />
							<cfset excelFormat = ""/>
							<cfset cellType = 0 />

							<cfif attributes.showCellColumnHeadings eq "yes" and attributes.columnTranslation>
								<cfset thisTitleText = evaluate(gettoken(thisColumnHeadingList,listfindnocase(showColumnList,i),","))>
							<cfelse>
								<cfset thisTitleText = gettoken(thisColumnHeadingList,listfindnocase(showColumnList,i),",")>
							</cfif>

							<!--- START 2011/10/27 PPB added support for tooltips to GROUPed TFQO too --->
							<cfset thisColumnHasToolTip = false>
							<cfif structKeyExists(attributes,"toolTipColumnList") and structKeyExists(attributes,"toolTipContentColumnList")
									and (listLen(attributes.toolTipColumnList) eq listLen(attributes.toolTipContentColumnList))
									and listFindNoCase(attributes.toolTipColumnList,i) neq "0">

								<cfset thisTitleText=#evaluate(gettoken(attributes.toolTipContentColumnList,listFindNoCase(attributes.toolTipColumnList,i),","))#>
								<cfset thisColumnHasToolTip = true>
							</cfif>
							<!--- END 2011/10/27 PPB added support for tooltips --->


							<!--- NJH 2009/05/12 CR-SNY675-1 - actually remove columns when hiding columns --->
							<cfif listFindNoCase(attributes.hideTheseColumns,i) neq 0>

							<cfelseif structKeyExists(attributes,"keyColumnList") and structKeyExists(attributes,"keyColumnURLList")
									and (listLen(attributes.keyColumnList) eq listLen(attributes.keyColumnURLList))
									and listFindNoCase(attributes.keyColumnList,i) neq "0">
								<CFSET thiskeyColumnURL=gettoken(attributes.keyColumnURLList,listFindNoCase(attributes.keyColumnList,i),",")>
								<CFSET thiskeyColumnKey=gettoken(attributes.keyColumnKeyList,listFindNoCase(attributes.keyColumnList,i),",")>

								<cfset thiskeyColumnTarget = "">
									<cfif structKeyExists(attributes,"keyColumnTargetList") and listLen(attributes.keyColumnTargetList) eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnTarget = gettoken(attributes.keyColumnTargetList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<cfset thiskeyColumnOpenInWindow = "no">
								<cfif structKeyExists(attributes,"keyColumnOpenInWindowList") and listLen(attributes.keyColumnOpenInWindowList) eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnOpenInWindow = gettoken(attributes.keyColumnOpenInWindowList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<cfset thiskeyColumnOpenWinName = "no">
								<cfif structKeyExists(attributes,"OpenWinNameList") and listLen(attributes.OpenWinNameList) eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnOpenWinName = gettoken(attributes.OpenWinNameList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<cfset thiskeyColumnOpenWinSettings = "no">
								<cfif structKeyExists(attributes,"OpenWinSettingsList") and listLen(attributes.OpenWinSettingsList,";") eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnOpenWinSettings = gettoken(attributes.OpenWinSettingsList,listFindNoCase(attributes.keyColumnList,i),";")>
								</cfif>

								<cfset thiskeyColumnIcon = "null">
								<cfif structKeyExists(attributes,"keyColumnIconList") and listLen(attributes.keyColumnIconList) eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnIcon = gettoken(attributes.keyColumnIconList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<!--- 2008/07/02 WAB added keyColumnOnClickList  --->
								<cfset thiskeyColumnOnClick = "">
								<cfif structKeyExists(attributes,"keyColumnOnClickList") and listLen(attributes.keyColumnOnClickList) eq listLen(attributes.keyColumnList)>
									<cfset thiskeyColumnOnClick = trim(gettoken(attributes.keyColumnOnClickList,listFindNoCase(attributes.keyColumnList,i),","))>
									<cfif thiskeyColumnOnClick is not ""><cfset thiskeyColumnOnClick = evaluate('"#replacenocase(thiskeyColumnOnClick,"*comma",",","ALL")#"')></cfif>
								</cfif>

								<!--- NJH 2010/04/28 -P-PAN002 - pass in how to display the link --->
								<cfset thisKeyColumnLinkDisplay = "link">
								<cfif structKeyExists(attributes,"KeyColumnLinkDisplayList") and listLen(attributes.KeyColumnLinkDisplayList) eq listLen(attributes.keyColumnList)>
									<cfset thisKeyColumnLinkDisplay = getToken(attributes.KeyColumnLinkDisplayList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<!--- NJH 2014/06/17 - Case 435831/Task Core-64 - certifications relaytag - pass in if to display as modal --->
								<cfset thisKeyColumnOpenAsModal = "no">
								<cfif structKeyExists(attributes,"keyColumnOpenAsModalList") and listLen(attributes.keyColumnOpenAsModalList) eq listLen(attributes.keyColumnList)>
									<cfset thisKeyColumnOpenAsModal = getToken(attributes.keyColumnOpenAsModalList,listFindNoCase(attributes.keyColumnList,i),",")>
								</cfif>

								<cfset thiskeyColumnURL = replacenocase(thiskeyColumnURL,"*comma",",","ALL")>   <!--- a hack WAB put in because he needed a comma in a URL, but couldn't get it in via the list --->
								<!--- use for more than 1 param --->
								<cfif listLen(thiskeyColumnKey,"|") gt 1>
									<cfif listLen(thiskeyColumnURL,"|") eq listLen(thiskeyColumnKey,"|")>

										<cfset tmpURL = "">
										<cfset lIncr = 1>
										<cfloop list="#thiskeyColumnKey#" index="item" delimiters="|">
											<cfif tmpURL eq "">
												<cfset tmpURL = "#listGetAt(thiskeyColumnURL,lIncr,'|')##evaluate(listGetAt(thiskeyColumnKey,lIncr,'|'))#">
											<cfelse>
												<cfset tmpURL = "#tmpURL#&#listGetAt(thiskeyColumnURL,lIncr,'|')##evaluate(listGetAt(thiskeyColumnKey,lIncr,'|'))#">
											</cfif>
											<cfset lIncr = lIncr+1>
										</cfloop>
										<cfset URLToGo = tmpURL>
									<cfelse>
										<cfset URLToGo = "#listGetAt(thiskeyColumnURL,1,'|')##evaluate(listGetAt(thiskeyColumnKey,1,'|'))#">
									</cfif>
								<cfelse>
									<cfif FindNoCase( "thisurlkey", thiskeyColumnURL ) neq 0>
										<cfset URLToGo = ReplaceNoCase( thiskeyColumnURL, "thisurlkey", JSStringFormat( evaluate(thiskeyColumnKey) ), "one" )>
										<!--- <td valign="top"><cfif createAsExcel eq false><a href="#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#" CLASS="smallLink"<cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>>#evaluate(i)#</a>&nbsp;<cfelse>#evaluate(i)#</cfif></td> --->
									<cfelse>
										<cfset URLToGo = "#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#">
										<!--- <td valign="top"><cfif createAsExcel eq false><a href="#thiskeyColumnURL##URLEncodedFormat(evaluate(thiskeyColumnKey))#" CLASS="smallLink"<cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>>#evaluate(i)#</a>&nbsp;<cfelse>#evaluate(i)#</cfif></td> --->
									</cfif>

								</cfif>

								<!--- NJH 2014/06/19 Case 435831 - add this functionality for groupBy --->
								<cfif isdefined("URLToGo") and not find("javascript:",URLToGo) and trim(URLToGo) neq "">
									<cfset urltogo = application.com.security.encryptURL (URLToGo)>
								</cfif>

								<cfif thiskeyColumnOpenInWindow>
									<cfif thiskeyColumnOpenWinName eq "no">
										<cfset winName="MyWindow">
									<cfelse>
										<cfset winName=thiskeyColumnOpenWinName>
									</cfif>
									<cfif thiskeyColumnOpenWinSettings eq "no">
										<cfset winSettings="width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">
									<cfelse>
										<cfset winSettings=thiskeyColumnOpenWinSettings>
									</cfif>

									<cfset hrefValue = "javascript:void(openWin('#URLToGo#','#winName#','#winSettings#'));"> <!--- NJH 2009/01/29 Bug Fix All Sites Issue 1701 - added void --->
								<cfelse>
									<cfset hrefValue = URLToGo>
								</cfif>
								<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
								<!--- NJH 2009/04/20 Bug Fix All Sites Issue 2072 - if open as excel, just output text --->
								<td class="#i#" valign="top">
									<!--- NJH 2010/0428 P-PAN002 - can now display links in various ways... as a button or a link at the moment --->
									<cfif createAsExcel eq false>
										<cfset showLink = true>
										<cfset showLinkAs = "link">
										<cfif isDefined('show#i#')>		<!--- if there is a column returned in the query called 'show' + the column name of the button (eg. showRenewButton) then use it to dictate whether the link/button is displayed  --->
											<cfset showLink = evaluate('show#i#')>
										</cfif>
										<cfif isDefined('show#i#As')>		<!--- if there is a column returned in the query called 'show'+columnName+'As' (eg. showRenewButtonAs) then use it to dictate whether the output is displayed as text or as a link  --->
											<cfset showLinkAs = evaluate('show#i#As')>
										</cfif>
										<cfif showLinkAs eq "text">
											#application.com.security.sanitiseHTML(cellValue)#
										<cfelse>
											<cfif thisKeyColumnLinkDisplay eq "link"><a href="#hrefValue#" <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif> class="smallLink <cfif thisKeyColumnOpenAsModal>modal</cfif>" <cfif trim(thiskeyColumnOnClick) neq ""> onclick="#htmleditformat(thiskeyColumnOnClick)#" </cfif> <cfif thiskeyColumnTarget neq ""> target="#htmleditformat(thiskeyColumnTarget)#"</cfif>><cfif thiskeyColumnIcon neq "null"><img src="/#thiskeyColumnIcon#" border="0" alt="#cellValue#"><cfelse>#application.com.security.sanitiseHTML(cellValue)#</cfif></a>
											<cfelseif thisKeyColumnLinkDisplay eq "button"><input type="button" onClick="#IIF(trim(thiskeyColumnOnClick) neq "",DE(thiskeyColumnOnClick),DE(hrefValue))#" value="#cellValue#" <cfif thisKeyColumnOpenAsModal>class="modal"</cfif>/>
											</cfif>
										</cfif>
									<cfelse>
										#application.com.security.sanitiseHTML(cellValue)#
									</cfif>
								</td>
								<cfset illustrativePriceAlphaIncr = illustrativePriceAlphaIncr + 1>
								<!--- <td valign="top"><a href="#thiskeyColumnURL##evaluate(thiskeyColumnKey)#"<cfif thiskeyColumnTarget neq ""> target="#thiskeyColumnTarget#"</cfif>>#evaluate(i)#</a>&nbsp;</td> --->
							<cfelseif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,i) neq "0">
								<!--- <cfset cellRendition = numberFormat(evaluate(i),"___,___,___,___") /> --->
									<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,i))>
									<cfset cellRendition = LSNumberFormat(cellValue,"#NumberFormatMaskValue#") />
								<cfset cellType = 1 />

							<cfelseif structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,i) neq "0">

								<!--- NJH 2010/03/11 P-PAN002 - if using rollUpCurrency, do the conversion --->
								<cfset cellValue = numberFormat(cellValue,"0.00")>
								<cfset cellRendition = cellValue>
								<cfset excelClass = "xl25">
								<cfset cellType = 2 /> <!--- Currency --->

								<cfif attributes.rollUpCurrency>

									<cfset originalValue = cellValue>

									<cfscript>
										fromCurrency = attributes.queryObject[attributes.rollUpPriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
										rollUpPrice = application.com.currency.convert(
											method = 2,
											fromValue = originalValue,
											fromCurrency = fromCurrency,
											toCurrency = attributes.rollUpPriceToCurrency,
											date = ''
											);
									</cfscript>

									<!--- need to set the query column value for totalling down below, as we need to total on the new values... --->
									<cfset attributes.queryObject[i][attributes.queryObject.CurrentRow] = rollUpPrice.toValueNumeric>
									<cfif not attributes.showCellColumnHeadings>
										<cfset thisTitleText = "">
									</cfif>
									<cfset thisTitleText=thisTitleText&" ("&rollUpPrice.fromSign&decimalFormat(originalValue)&")">

									<cfif rollUpPrice.conversionRate eq "">
										<cfset exchangeRateExists = false>
									</cfif>

									<cfset cellValue = rollUpPrice.toValue>				<!--- 2011/08/10 PPB LID7090 save cellValue to use as x:num --->

									<cfif not createAsExcel>
										<cfset cellRendition = rollUpPrice.toSign & rollUpPrice.toValue>
									<cfelse>
										<cfset excelClass = excelCurrencyClass>
										<cfset cellRendition = rollUpPrice.toValue>
									</cfif>
								<cfelseif structKeyExists(currencyStruct,i) >
									<cfif not createAsExcel>
										<cfset cellRendition = currencyStruct[i].currencySign & lsNumberFormat(cellValue,"9,999.99")>	 <!--- case 435341 NJH 2013/06/03 --->
									<cfelse>
										<cfset excelClass = currencyStruct[i].currencyMSO>
									</cfif>


								<cfelseif structKeyExists (attributes,"currencyISOColumn") and listLen(attributes.currencyISOColumn) eq listLen(attributes.currencyFormat)>
									<cfset currencyISO = evaluate (listgetat(attributes.currencyISOColumn,listfindnocase(attributes.currencyFormat,i)))>

									<cfif not createAsExcel>
										<cfset cellRendition = lsNumberFormat(cellValue,"9,999.99") & " " & currencyISO>	 <!--- case 435341 NJH 2013/06/03 --->
									</cfif>

								</cfif>

							<!--- NJH 2008/09/02 added a boolean format - added during elearning module development for R8.1 --->
							<cfelseif structKeyExists(attributes,"booleanFormat") and listFindNoCase(attributes.booleanFormat,i) neq "0">
								<cfif isBoolean(cellValue)>
									<!--- 2012/11/13 YMA Case 429879. Update TFQO to get translations before entering--->
									<cfif cellValue eq 1>
										<cfset cellRendition = yesTranslation>
									<cfelse>
										<cfset cellRendition = noTranslation>
									</cfif>
								<cfelse>
									<cfset cellRendition = cellValue>
								</cfif>
								<cfset cellType = 5 /> <!--- NJH 2009/01/02 CR-LEX581 bug Fix - cell type was 2 which is a date rather than a string --->

							<cfelseif structKeyExists(attributes,"dateFormat") and listFindNoCase(attributes.dateFormat,i) neq "0">
								<!--- <cfset cellRendition = dateFormat(evaluate(i),"dd-mmm-yy") /> --->
									<cfset dateFormatMaskValue = getDateFormatMaskValue(dateFormatMask = attributes.dateFormatMask, columnPosition = listFindNoCase(attributes.dateFormat,i))>
									<cfset cellRendition = LSDateFormat(cellValue, "#dateFormatMaskValue#")/>
								<cfset cellType = 3 />

							<cfelseif structKeyExists(attributes,"dateTimeFormat") and listFindNoCase(attributes.dateTimeFormat,i) neq "0"> <!--- NJH 2009/01/12 - created as part of scheduled task monitor report as time as important as well as the date --->
								<cfif not createAsExcel and isDate(cellValue)>
									<cfset cellRendition = application.com.dateFunctions.dateTimeFormat(date=cellValue,showAsLocalTime=attributes.showAsLocalTime,showIcon=false)>
									<!--- <cfset cellRendition = "#lsdateFormat(cellValue,"medium")# #lsTimeFormat(evaluate(i),"medium")#" /> --->
								<cfelse>
									<cfset cellRendition = cellValue />
								</cfif>
								<cfset cellType = 3 />
							<!---START: 2011/03/16 added to handle timeformat --->
							<cfelseif structKeyExists(attributes,"TimeFormat") and listFindNoCase(attributes.TimeFormat,i) neq "0"> <!--- NJH 2009/01/12 - created as part of scheduled task monitor report as time as important as well as the date --->
								<cfif not createAsExcel and isDate(cellValue)><cfset cellRendition = "#lsTimeFormat(cellValue,"medium")#" /><cfelse><cfset cellRendition = cellValue /></cfif>
								<cfset cellType = 6 />
							<!---END: 2011/03/16 added to handle timeformat --->
							<!--- NJH 2010/03/11 P-PAN002 - append the currency sign to the value.
							<cfelseif structKeyExists(attributes,"IllustrativePriceFromCurrencyColumn") and attributes.IllustrativePriceFromCurrencyColumn neq "" and listFindNoCase(attributes.IllustrativePriceColumns,i)>
								<cfset cellRendition = convertToCurrencySign&decimalFormat(evaluate(i))>
								<cfset cellType = 2>
								--->
							<cfelse>
								<cfif listFindNoCase(attributes.hideTheseColumns,i) eq 0>
									<cfset cellRendition = cellValue />
									<cfset cellType = 4 />
								</cfif>
							</cfif>
							<cfif cellType neq 0>
<!--- --------------------------------------------------------------------- --->
<!--- extra cell attributes for Excel --->
<!--- --------------------------------------------------------------------- --->
								<cfswitch expression="#cellType#">
									<cfcase value="1">
										<cfset excelCellAttribute = ' x:num="' & TRIM(attributes.queryObject[i][attributes.queryObject.CurrentRow]) & '" class="#i#"' />
									</cfcase>
									<cfcase value="2">
										<cfif attributes.queryObject[i][attributes.queryObject.CurrentRow] eq "">
											<cfset excelCellAttribute = ' x:num="0.00" class="' & excelClass & ' #i#"' />
										<cfelse>
											<!--- NJH 2010/07/29 - replaced with below <cfset excelCellAttribute = ' x:num="' & attributes.queryObject[i][attributes.queryObject.CurrentRow] & '" class="' & excelClass & '"' /> --->
											<!--- 2011/08/05 PPB LID7090 added LSParseCurrency() to strip off currency formatting so that XL doesn't receive it as text and not be able to calculate totals on it --->
											<cfset excelCellAttribute = ' x:num="' & cellValue & '" class="' & excelClass & ' #i#"' />
										</cfif>
									</cfcase>
									<cfcase value="3">
										<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
											<!--- <cfset excelCellAttribute = ' x:num="' & trim(dateFormat(attributes.queryObject[i][attributes.queryObject.CurrentRow],"dd/mm/yyyy")) & '" class=xl24' />
										 ---><cfset excelCellAttribute = ' class="xl24 #i#"' />
										 <cfelse>
											<cfset excelCellAttribute = '' />
										</cfif>
									</cfcase>
									<!---START: 2011/03/16 added to handle timeformat --->
									<cfcase value="6">  <!--- time --->
										<cfif IsDate( attributes.queryObject[i][attributes.queryObject.CurrentRow] )>
											<cfset excelCellAttribute = ' class="xlTime #i#"' />
										<cfelse>
											<cfset excelCellAttribute = '' />
										</cfif>
									</cfcase>
									<!---END: 2011/03/16 added to handle timeformat --->
									<cfdefaultcase>
										<cfset excelCellAttribute = 'class="#i#"' />
									</cfdefaultcase>
								</cfswitch>
<!--- --------------------------------------------------------------------- --->
								<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
								<td #excelCellAttribute#><span <cfif attributes.showCellColumnHeadings eq "yes" or (attributes.rollUpCurrency and cellType eq 2) or thisColumnHasToolTip>title="#thisTitleText#"</cfif>>#cellRendition#</span>
<!--- --------------------------------------------------------------------- --->
<!--- table inclusion --->
<!--- --------------------------------------------------------------------- --->
								<cfif IsDefined( "caller.udfTableCallBack" ) and IsCustomFunction( caller.udfTableCallBack )>
									#caller.udfTableCallBack( i, attributes.queryObject[i][attributes.queryObject.CurrentRow], cellType )#
								</cfif>
<!--- --------------------------------------------------------------------- --->
								</td>
								<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
								<cfif attributes.IllustrativePriceColumns neq ""
									and listFindNoCase(attributes.IllustrativePriceColumns,i) gt 0>
									<cfif cellValue eq "">
										<cfset value = 0.00>
									<cfelse>
										<cfset value = cellValue>
									</cfif>
									<cfswitch expression="#attributes.IllustrativePriceMethod#">
										<cfcase value="2">
											<cfscript>
												if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
													exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
												} else {
													exchangeDate = '';
												};

												/* NJH P-PAN002 2010/03/09 - if IllustrativePriceFromCurrencyColumn has been set, use it. Used for rollup reporting */
												fromCurrency = attributes.IllustrativePriceFromCurrency;
												/*if (attributes.IllustrativePriceFromCurrencyColumn neq "") {
													fromCurrency = attributes.queryObject[attributes.IllustrativePriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
												}*/
												IllustrativePrice = application.com.currency.convert(
													method = #attributes.IllustrativePriceMethod#,
													fromValue = value,
													fromCurrency = fromCurrency,
													toCurrency = attributes.IllustrativePriceToCurrency,
													date = #exchangeDate#
													);
											</cfscript>

											<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
										</cfcase>
									</cfswitch>
									<cfif createAsExcel>
										<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=#listGetAt(alphaList,illustrativePriceAlphaIncr)##rowcount# * #IllustrativePrice.conversionRate#'">
										</cfif>
									<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & i', 'replace(i,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
									<td valign="top"#excelFormat#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>>#htmleditformat(IllustrativePriceOutput)#</span></td>
									<cfset illustrativePriceAlphaIncr = illustrativePriceAlphaIncr + 1>
								</cfif>
								<cfset illustrativePriceAlphaIncr = illustrativePriceAlphaIncr + 1>
							<cfif attributes.showFunctionList>
								<td valign="top"></td>
							</cfif>
						</cfif>
							<!--- find out if col must be subtotaled and add next value --->
							<cfif isDefined("subTotals")>
								<cfif structKeyExists(subTotals,i)>
									<!--- 	2014-09-03	WAB	CASE 441552.  Needs to refer back the query rather than the cellValue variable. CellValue can have formatting which makes it non numeric.  In case of rollup currency, the queryObject is updated to reflect the rollup currency --->
									<cfif isNumeric(attributes.queryObject[i][attributes.queryObject.CurrentRow])>
										<cfset subTotals[i][evaluate(GroupBy2)] = subTotals[i][evaluate(GroupBy2)] + attributes.queryObject[i][attributes.queryObject.CurrentRow]>
									</cfif>
								</cfif>
							</cfif>
						</cfloop>
						</tr>
					<cfelse>
						<cfif not isDefined("nextAction")>
							<cfset nextAction = 1>
						</cfif>
					</cfif>
				</cfoutput>
				<cfif not newGroup><cfset excelEndCount = rowCount><cfset newGroup = true></cfif>

				<!--- display of subtotals --->
				<cfif incr lte attributes.numRowsPerPage or (isDefined("nextAction") and nextAction eq 1)>
					<cfif isDefined("subTotals")>
						<TR class="oddrow"><cfset rowCount = rowCount + 1>
							<cfset colcounter = 0>
							<cfset alphaIncr = 1>
							<cfloop index="thisCol" list="#showColumnList#">

							<cfset excelFormat = "">

							<cfif attributes.showCellColumnHeadings eq "yes" and attributes.columnTranslation>
								<cfset thisTitleText = evaluate(gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),","))>
							<cfelse>
								<cfset thisTitleText = gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),",")>
							</cfif>

							<!--- loop through the columnlist of the query object --->
							<cfif listFindNoCase(structKeyList(subTotals),thisCol) eq "0" and listFindNoCase(attributes.hideTheseColumns,thisCol) eq 0>
								<td valign="top"><cfif colcounter eq "0"><strong>Sub Total<cfif listLen(thisGroupByColumn) eq 1> #htmleditformat(evaluate(thisGroupByColumn))#<cfelse>s</cfif></strong><cfif attributes.HidePageControls eq "no" and structKeyExists(attributes,"numRowsPerPage") and attributes.numRowsPerPage lt attributes.queryObject.RecordCount> (this&nbsp;page)</cfif></cfif></td>
								<cfset alphaIncr = alphaIncr + 1>
								<!--- <td colspan="#listLen(showColumnList)#">#subTotals[attributes.totalTheseColumns]#</td> --->
							<cfelseif listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0" and structKeyExists(subTotals,thisCol)>
								<!--- if this column is numeric and in the totalTheseColumns attribute work out the total for this column by thisGroupByColumn --->

								<cfif createAsExcel>
									<cfif structkeyexists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
										<cfset excelFormat = " x:fmla='=SUM(#listGetAt(alphaList,alphaIncr)##excelBeginCount#:#listGetAt(alphaList,alphaIncr)##excelEndCount#)'">
									<cfelse>
										<cfset excelFormat = " class='.xl25' x:fmla='=SUM(#listGetAt(alphaList,alphaIncr)##excelBeginCount#:#listGetAt(alphaList,alphaIncr)##excelEndCount#)'">
									</cfif>
								</cfif>

								<!--- GCC here --->
								<!--- GCC - 2005/02/24 Override totalling with averaging if specified --->
								<cfset subTotalOverRide = "">
								<cfif structKeyExists(attributes,"averageTheseColumns") and listFindNoCase(attributes.averageTheseColumns,thisCol) neq "0">

									<cfset subTotalRows = subIncr - 1>
									<cfif isdefined ("attributes.averageColumnsToUse") and trim(gettoken(attributes.averageColumnsToUse,listFindNoCase(attributes.averageTheseColumns,thisCol),",")) neq "">
										<cfset averageColsList = gettoken(attributes.averageColumnsToUse,listFindNoCase(attributes.averageTheseColumns,thisCol),",")>
										<cfset averageNumerator = listFirst(averageColsList,"|")>
										<cfset averageDenominator = listLast(averageColsList,"|")>
										<cfif subTotals[averageDenominator][evaluate(attributes.GroupByColumns)] neq 0>
											<cfset subTotalValue = subTotals[averageNumerator][evaluate(attributes.GroupByColumns)] / subTotals[averageDenominator][evaluate(attributes.GroupByColumns)] * 100>
										<cfelse>
											<cfset subTotalValue = "0">
										</cfif>
									<cfelse>
										<cfset subTotalValue = subTotals[thisCol][evaluate(attributes.GroupByColumns)] / subTotalRows>
									</cfif>
									<cfset subTotalOverRide = "phr_Avg">
								</cfif>

								<cfif isdefined('subTotalOverRide') and len(subTotalOverRide) gt 0>
									<cfset outputValue = subTotalValue>
								<cfelse>
									<cfset outputValue = subTotals[thisCol][evaluate(GroupBy2)]>
								</cfif>

								<cfif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
									<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,thisCol))>
									<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
									<td #excelFormat#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#thisTitleText#"</cfif>><cfif isdefined('subTotalOverRide') and len(subTotalOverRide) gt 0>#subTotalOverRide# #LSnumberFormat(outputValue,"#NumberFormatMaskValue#")#<cfelse>#LSnumberFormat(outputValue,"#NumberFormatMaskValue#")#</cfif><!--- &nbsp;&nbsp; ---></span></td>
									<cfset excelSubTotals[thisCol][evaluate(attributes.GroupByColumns)] = "#listGetAt(alphaList,alphaIncr)##rowCount#">
									<cfset alphaIncr = alphaIncr + 1>
								<cfelseif (structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,thisCol) neq "0")>
								<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->

									<!--- NJH 2010/03/11 P-PAN002 - if using a currency column, then output number with the appropriate currency sign --->
									<!--- PPB 2010/07/22 P-PAN002 - done for PriceFromCurrencyColumn too --->
									<cfset outputValue = decimalFormat(outputValue)>
									<cfif attributes.rollUpCurrency>
										<cfif exchangeRateExists>
											<cfset outputValue = convertToCurrencySign&outputValue>
										<cfelse>
											<cfset outputValue = "--">
										</cfif>
									<cfelseif structKeyExists(currencyStruct,thisCol) >
											<cfset outputValue = currencyStruct[thisCol].currencySign & outputValue>
									</cfif>


									<td #excelFormat#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>><cfif isdefined('subTotalOverRide') and len(subTotalOverRide) gt 0>#htmleditformat(subTotalOverRide)# #htmleditformat(outputValue)#<cfelse>#htmleditformat(outputValue)#</cfif></span></td>
									<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
									<cfif attributes.IllustrativePriceColumns neq ""
										and listFindNoCase(attributes.IllustrativePriceColumns,thisCol) gt 0>
										<cfswitch expression="#attributes.IllustrativePriceMethod#">
											<cfcase value="2">
												<cfscript>
													if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
														exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
													} else {
														exchangeDate = '';
													};

													/* NJH P-PAN002 2010/03/09 - if IllustrativePriceFromCurrencyColumn has been set, use it. Used for rollup reporting */
													fromCurrency = attributes.IllustrativePriceFromCurrency;
													/*if (attributes.IllustrativePriceFromCurrencyColumn neq "") {
														fromCurrency = attributes.queryObject[attributes.IllustrativePriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
													}*/
													IllustrativePrice = application.com.currency.convert(
														method = #attributes.IllustrativePriceMethod#,
														fromValue = outputValue,
														fromCurrency = fromCurrency,
														toCurrency = attributes.IllustrativePriceToCurrency,
														date = #exchangeDate#
														);
												</cfscript>
												<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
											</cfcase>
										</cfswitch>
										<cfif createAsExcel>
											<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=SUM(#listGetAt(alphaList,alphaIncr)##excelBeginCount#:#listGetAt(alphaList,alphaIncr)##excelEndCount#) * #IllustrativePrice.conversionRate#'">
										</cfif>
										<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
										<td class="#i#" valign="top"#excelFormat#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>><cfif isdefined('subTotalOverRide') and len(subTotalOverRide) gt 0>#htmleditformat(subTotalOverRide)# </cfif>#htmleditformat(IllustrativePriceOutput)#</span></td>
										<cfset alphaIncr = alphaIncr + 1>
									</cfif>
<!---
									<cfset excelSubTotals[thisCol][evaluate(attributes.GroupByColumns)] = "#listGetAt(alphaList,alphaIncr)##rowCount#">
 --->
									<cfset excelSubTotals[thisCol][evaluate(GroupBy2)] = "#listGetAt(alphaList,alphaIncr)##rowCount#">
<!---
									<cfset excelSubTotals[thisCol][evaluate(GroupBy2)] = "#listGetAt(alphaList,alphaIncr)##rowCount#">
 --->
									<cfset alphaIncr = alphaIncr + 1>
								</cfif>
<!--- 								<cfelse>
								<td>#thisCol# cannot be totalled</td>
							</cfif>
--->						</cfif>
							<!--- increment the column Counter --->
							<cfset colcounter = colcounter + 1>

							<cfif attributes.showFunctionList>
								<td valign="top"></td>
								<cfset alphaIncr = alphaIncr + 1>
							</cfif>
						</cfloop>
						</tr>
					<cfset subIncr = 1>
					</cfif>
					<cfif isDefined("nextAction") and nextAction eq 1>
						<cfset nextAction = 2>
					</cfif>
				</cfif>

			</cfoutput>
			</cfoutput>
		</cfif>

	<!--- <cfelse>
		<tr><td>No records found.</td></tr> --->
	</cfif>

<!--- ==============================================================================
    GRAND TOTAL SECTION
=============================================================================== --->
	<cfif attributes.queryObject.recordCount gt 0 and listLen(attributes.totalTheseColumns) neq 0>
		<cfoutput>
			<tr><cfset rowCount = rowCount + 1><td height="10px" colspan="#listLen( showColumnList )#"></td></tr>
			<TR class="oddrow"><cfset rowCount = rowCount + 1>
			<cfset colcounter = 0>
			<cfset alphaIncr = 1>
			<!--- set the column counter --->
			<cfloop index="thisCol" list="#showColumnList#">
				<cfset excelFormat = "">
				<cfif attributes.showCellColumnHeadings eq "yes" and attributes.columnTranslation>
					<cfset thisTitleText = evaluate(gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),","))>
				<cfelse>
					<cfset thisTitleText = gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),",")>
				</cfif>
				<!--- loop through the columnlist of the query object --->
				<cfif listFindNoCase(attributes.totalTheseColumns,thisCol) eq "0" and listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0">
					<!--- if this column is not in the totalTheseColumns attribute do this --->
					<td valign="top"><cfif colcounter eq "0"><b>Grand Total</b><cfif isDefined("subTotals") and attributes.HidePageControls eq "no" and structKeyExists(attributes,"numRowsPerPage") and attributes.numRowsPerPage lt attributes.queryObject.RecordCount> (this&nbsp;page)</cfif></cfif></td>
					<cfset alphaIncr = alphaIncr+1>
				<cfelseif listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0">
					<cfif isDefined("subTotals")>
						<cfif structKeyExists(subTotals,thisCol)>
							<cfset totalValue = 0>
							<cfloop collection="#subTotals[thisCol]#" item="colItem">
								<cfset totalValue = totalValue + subTotals[thisCol][colitem]>
							</cfloop>
						</cfif>
						<cfif structKeyExists(excelSubTotals,thisCol)>
							<cfset addRows = "">
							<cfloop collection="#excelSubTotals[thisCol]#" item="colItem">
								<cfset addRows = listAppend(addRows,excelSubTotals[thisCol][colItem],"+")>
							</cfloop>
							<cfif createAsExcel>
								<cfif structkeyexists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
									<cfset excelFormat = " x:fmla='=#addRows#'">
								<cfelse>
									<cfset excelFormat = " class='#excelClass#' x:fmla='=#addRows#'">
								</cfif>
							</cfif>
						</cfif>
					<cfelse>
						<!--- if this column is in the totalTheseColumns attribute get the total for this column --->
						<cfquery name="getThisColTotal" dbtype="query">
							select sum([#thisCol#]) as thisColTotal from attributes.queryObject
						</cfquery>
						<cfset totalValue = getThisColTotal.thisColTotal>
					</cfif>
					<!--- GCC - 2005/02/24 Override totalling with averaging if specified --->
					<cfset totalOverRide = "">
					<cfif structKeyExists(attributes,"averageTheseColumns") and listFindNoCase(attributes.averageTheseColumns,thisCol) neq "0">
						<!--- using group by incr is available - without group by the row count - 2 is the total number of rows in the result set --->
						<cfif structKeyExists(attributes,"GroupByColumns") and listLen(attributes.GroupByColumns) neq 0>
							<cfset subTotalRows = incr - 1>
						<cfelse>
							<cfset subTotalRows = rowcount - 2>
						</cfif>
						<cfif isdefined ("attributes.averageColumnsToUse") and trim(gettoken(attributes.averageColumnsToUse,listFindNoCase(attributes.averageTheseColumns,thisCol),",")) neq "">
							<cfset averageColsList = gettoken(attributes.averageColumnsToUse,listFindNoCase(attributes.averageTheseColumns,thisCol),",")>
							<cfset averageNumerator = listFirst(averageColsList,"|")>
							<cfset averageDenominator = listLast(averageColsList,"|")>
							<cfquery name="getDenominatorColTotal" dbtype="query">
								select sum([#averageDenominator#]) as thisColTotal from attributes.queryObject
							</cfquery>
							<cfif getDenominatorColTotal.thisColTotal neq 0>
								<cfquery name="getNumeratorColTotal" dbtype="query">
									select sum([#averageNumerator#]) as thisColTotal from attributes.queryObject
								</cfquery>
								<cfset totalValue = getNumeratorColTotal.thisColTotal / getDenominatorColTotal.thisColTotal * 100>
							<cfelse>
								<cfset totalValue = "0">
							</cfif>
						<cfelse>
							<cfset totalValue = totalValue / subtotalrows>
						</cfif>
						<cfset totalOverRide = "phr_Avg">
					</cfif>
					<cfif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
						<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,thisCol))>
						<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
						<td #excelFormat#><b><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#thisTitleText#"</cfif>><cfif isdefined('totalOverRide') and len(totalOverRide) gt 0>#totalOverRide# </cfif>#LSnumberFormat(totalValue,"#NumberFormatMaskValue#")#<!--- &nbsp;&nbsp; ---></span></b></td>
						<cfset alphaIncr = alphaIncr+1>
					<cfelseif structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,thisCol) neq "0">
						 <!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
						<cfset totalValue = decimalFormat(totalValue)>
						<cfif attributes.rollUpCurrency>
							<cfif exchangeRateExists>
								<cfset totalValue = convertToCurrencySign&totalValue>
							<cfelse>
								<cfset totalValue = "--">
							</cfif>
						<cfelseif structKeyExists(currencyStruct,thisCol) >
								<cfset totalValue = currencyStruct[thisCol].currencySign & totalValue>
						</cfif>


						<!--- NAS 2010/04/13 P-PAN002 - if using a currency column, then output nothing
						<cfif structKeyExists(attributes,"IllustrativePriceFromCurrencyColumn") and attributes.IllustrativePriceFromCurrencyColumn neq "">
							<td valign="top"<!--- #excelFormat# --->><span <cfif attributes.showCellColumnHeadings eq "yes">title="#thisTitleText#"</cfif>><cfif isdefined('totalOverRide') and len(totalOverRide) gt 0>#totalOverRide# </cfif><!--- #LSCurrencyFormat(totalValue,"local")# --->&nbsp;</span></td>
						<cfelse>--->
							<td #excelFormat#><b><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>><cfif isdefined('totalOverRide') and len(totalOverRide) gt 0>#htmleditformat(totalOverRide)# </cfif>#htmleditformat(totalValue)#</span></b></td>
						<!--- </cfif> --->
						<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
						<cfif attributes.IllustrativePriceColumns neq ""
							and listFindNoCase(attributes.IllustrativePriceColumns,thisCol) gt 0>
								<cfswitch expression="#attributes.IllustrativePriceMethod#">
									<cfcase value="2">
										<cfscript>
											//if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
											//	exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
											//} else {
											//	exchangeDate = '';
											//};

											/* NJH P-PAN002 2010/03/09 - if IllustrativePriceFromCurrencyColumn has been set, use it. Used for rollup reporting */
											fromCurrency = attributes.IllustrativePriceFromCurrency;
											/*if (attributes.IllustrativePriceFromCurrencyColumn neq "") {
												fromCurrency = attributes.queryObject[attributes.IllustrativePriceFromCurrencyColumn][attributes.queryObject.CurrentRow];
											}*/
											IllustrativePrice = application.com.currency.convert(
												method = #attributes.IllustrativePriceMethod#,
												fromValue = totalValue,
												fromCurrency = fromCurrency,
												toCurrency = attributes.IllustrativePriceToCurrency,
												date = ''
												);
										</cfscript>
										<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
									</cfcase>
								</cfswitch>
								<cfif createAsExcel>
									<cfif isDefined("subTotals")>
										<!--- NAS 2010/04/13 P-PAN002 - if using a currency column do not process currency conversion --->
										<!--- PPB 2010/07/22 P-PAN002 - was done for IllustrativePriceFromCurrencyColumn now also for PriceFromCurrencyColumn --->
										<cfif attributes.rollUpCurrency<!---  or (structKeyExists(attributes,"IllustrativePriceFromCurrencyColumn") and attributes.IllustrativePriceFromCurrencyColumn neq "") --->>
											<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=(#addRows#)'">
										<cfelse>
											<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=(#addRows#) * #IllustrativePrice.conversionRate#'">
										</cfif>
									<cfelse>
										<!--- NAS 2010/04/13 P-PAN002 - if using a currency column do not process currency conversion --->
										<!--- PPB 2010/07/22 P-PAN002 - was done for IllustrativePriceFromCurrencyColumn now also for PriceFromCurrencyColumn --->
										<cfif attributes.rollUpCurrency <!--- or ((structKeyExists(attributes,"IllustrativePriceFromCurrencyColumn") and attributes.IllustrativePriceFromCurrencyColumn neq "")) --->>
											<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=SUM(#listGetAt(alphaList,alphaIncr)##excelBeginCount#:#listGetAt(alphaList,alphaIncr)##excelEndCount#)'">
										<cfelse>
											<cfset excelFormat = " class='#excelCurrencyClass#' x:fmla='=SUM(#listGetAt(alphaList,alphaIncr)##excelBeginCount#:#listGetAt(alphaList,alphaIncr)##excelEndCount#) * #IllustrativePrice.conversionRate#'">
										</cfif>
									</cfif>
								</cfif>
							<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
							<td #excelFormat#><b><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>><cfif isdefined('totalOverRide') and len(totalOverRide) gt 0>#htmleditformat(totalOverRide)# </cfif>#htmleditformat(IllustrativePriceOutput)#<!--- &nbsp;&nbsp; ---></span></b></td>
						</cfif>
						<cfset alphaIncr = alphaIncr+1>
					</cfif>

				</cfif>
				<!--- increment the column Counter --->
				<cfset colcounter = colcounter + 1>
				<cfif attributes.showFunctionList>
					<td></td>
					<cfset alphaIncr = alphaIncr+1>
				</cfif>

			</cfloop>
			</tr>
		</cfoutput>
	<!--- <cfelse>
		<tr><td>No records found.</td></tr> --->
	</cfif>



<!--- ==============================================================================
    FINAL PAGE SUB AND GRAND TOTAL SECTION
=============================================================================== --->
	<cfif isDefined("subTotals") and structKeyExists(attributes,"numRowsPerPage") and attributes.numRowsPerPage lte attributes.queryObject.RecordCount>
		<cfif (startRow + attributes.numRowsPerPage) gte attributes.queryObject.RecordCount>
			<cfoutput>
				<tr><cfset rowCount = rowCount + 1><td height="10px" colspan="#listLen( showColumnList )#"></td></tr>
				<TR class="evenRow"><cfset rowCount = rowCount + 1>
					<TD colspan="#listLen(showColumnList)#"><br><strong>Final Totals</strong></TD>
				</TR>
			</cfoutput>

			<cfset finalTotals = structNew()>
			<cfloop list="#attributes.totalTheseColumns#" index="col">
				<cfset finalTotals[col] = structNew()>
			</cfloop>

			<cfoutput query="attributes.queryObject" GROUP="#attributes.GroupByColumns#">
				<cfif isDefined("finalTotals")>
					<cfloop list="#attributes.totalTheseColumns#" index="col">
						<cfset finalTotals[col][evaluate(attributes.GroupByColumns)] = 0>
					</cfloop>
				</cfif>
				<cfoutput>
					<cfloop index="thisCol" list="#showColumnList#">
						<cfif isDefined("finalTotals")>
							<cfif structKeyExists(finalTotals,thisCol)>
								<cfif isNumeric(evaluate(thisCol))>
									<cfset finalTotals[thisCol][evaluate(attributes.GroupByColumns)] = finalTotals[thisCol][evaluate(attributes.GroupByColumns)] + evaluate(thisCol)>
								</cfif>
							</cfif>
						</cfif>
					</cfloop>
				</cfoutput>
				<TR class="oddrow"><cfset rowCount = rowCount + 1>
					<cfset colcounter = 0>
					<cfloop index="thisCol" list="#showColumnList#">
						<cfif attributes.showCellColumnHeadings eq "yes" and attributes.columnTranslation>
							<cfset thisTitleText = evaluate(gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),","))>
						<cfelse>
							<cfset thisTitleText = gettoken(thisColumnHeadingList,listfindnocase(showColumnList,thisCol),",")>
						</cfif>
						<!--- loop through the columnlist of the query object --->
						<cfif listFindNoCase(structKeyList(subTotals),thisCol) eq "0" and listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0">
							<td><cfif colcounter eq "0"><strong>Sub Total<cfif listLen(thisGroupByColumn) eq 1> #htmleditformat(evaluate(thisGroupByColumn))#<cfelse>s</cfif></strong><cfelse></cfif></td>
							<!--- <td colspan="#listLen(showColumnList)#">#subTotals[attributes.totalTheseColumns]#</td> --->
						<cfelseif listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0" and structKeyExists(subTotals,thisCol)>
							<!--- if this column is numeric and in the totalTheseColumns attribute work out the total for this column by thisGroupByColumn --->
							<cfif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
								<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,thisCol))>
								<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
								<td><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>>#lsnumberFormat(finalTotals[thisCol][evaluate(attributes.GroupByColumns)],"#htmleditformat(NumberFormatMaskValue)#")#</span></td>
							<cfelseif structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,thisCol) neq "0">
								<cfif attributes.rollUpCurrency>
									<cfif exchangeRateExists>
										<cfset totalValue = convertToCurrencySign&decimalFormat(finalTotals[thisCol][evaluate(attributes.GroupByColumns)])>
									<cfelse>
										<cfset totalValue = "--">
									</cfif>
								</cfif>

								<cfset totalValue = decimalFormat(finalTotals[thisCol][evaluate(attributes.GroupByColumns)])>
								<cfif attributes.rollUpCurrency>
									<cfif exchangeRateExists>
										<cfset totalValue = convertToCurrencySign&totalValue>
									<cfelse>
										<cfset totalValue = "--">
									</cfif>
								<cfelseif structKeyExists(currencyStruct,thisCol) >
										<cfset totalValue = currencyStruct[thisCol].currencySign & totalValue>
								</cfif>

								<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
								<td><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>>#htmleditformat(totalValue)#</span></td>
							</cfif>
						</cfif>
						<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
						<cfif structKeyExists(attributes,"IllustrativePriceColumns")
							and listFindNoCase(attributes.IllustrativePriceColumns,thisCol) gt 0>
							<cfswitch expression="#attributes.IllustrativePriceMethod#">
								<cfcase value="2">
									<cfscript>
										if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
											exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
										} else {
											exchangeDate = '';
										};
										IllustrativePrice = application.com.currency.convert(
											method = #attributes.IllustrativePriceMethod#,
											fromValue = finalTotals[thisCol][evaluate(attributes.GroupByColumns)],
											fromCurrency = attributes.IllustrativePriceFromCurrency,
											toCurrency = attributes.IllustrativePriceToCurrency,
											date = #exchangeDate#
											);
									</cfscript>
									<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
								</cfcase>
							</cfswitch>
							<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '" class="' & excelCurrencyClass & '"' />
							<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
							<td valign="top"#excelCellAttribute#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#thisTitleText#"</cfif>>#IllustrativePriceOutput#</span></td>
						</cfif>
						<!--- increment the column Counter --->
						<cfset colcounter = colcounter + 1>
						<cfif attributes.showFunctionList>
							<td valign="top"></td>
						</cfif>
					</cfloop>
				</tr>

			</cfoutput>
			<!--- display final grand total --->
			<cfoutput>
			<tr><cfset rowCount = rowCount + 1><td height="10px" colspan="#listLen( showColumnList )#"></td></tr>
			<TR class="oddrow"><cfset rowCount = rowCount + 1>
				<cfset colcounter = 0>
				<cfloop index="thisCol" list="#showColumnList#">
					<!--- loop through the columnlist of the query object --->
					<cfif listFindNoCase(attributes.totalTheseColumns,thisCol) eq "0" and listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0">
						<!--- if this column is not in the totalTheseColumns attribute do this --->
						<td><cfif colcounter eq "0"><strong>Grand Total</strong></cfif></td>
					<cfelseif listFindNoCase(attributes.hideTheseColumns,thisCol) eq "0">
						<cfif structKeyExists(finalTotals,thisCol)>
							<cfset totalValue = 0>
							<cfloop collection="#finalTotals[thisCol]#" item="colItem">
								<cfset totalValue = totalValue + finalTotals[thisCol][colitem]>
							</cfloop>
						</cfif>
						<cfoutput>
						<cfif structKeyExists(attributes,"numberFormat") and listFindNoCase(attributes.numberFormat,thisCol) neq "0">
							<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormatMask, columnPosition = listFindNoCase(attributes.numberFormat,thisCol))>
							<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
							<td><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>>#LSNumberFormat(totalValue,"#htmleditformat(NumberFormatMaskValue)#")#</span></td>
						<cfelseif structKeyExists(attributes,"currencyFormat") and listFindNoCase(attributes.currencyFormat,thisCol) neq "0">
							<cfset totalValue = decimalFormat(totalValue)>
							<cfif attributes.rollUpCurrency>
								<cfif exchangeRateExists>
									<cfset totalValue = convertToCurrencySign&totalValue>
								<cfelse>
									<cfset totalValue = "--">
								</cfif>
							<cfelseif structKeyExists(currencyStruct,thisCol) >
									<cfset totalValue = currencyStruct[thisCol].currencySign & totalValue>
							</cfif>
							<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
							<td><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#htmleditformat(thisTitleText)#"</cfif>>#htmleditformat(totalValue)#</span></td>
						</cfif>
						<!--- CR_ATI031 Show Illustrative pricing in lead and Opp --->
						<cfif attributes.IllustrativePriceColumns neq ""
							and listFindNoCase(attributes.IllustrativePriceColumns,thisCol) gt 0>
							<cfswitch expression="#attributes.IllustrativePriceMethod#">
								<cfcase value="2">
									<cfscript>
										//if (structkeyexists(attributes,"IllustrativePriceDateColumn") and attributes.IllustrativePriceDateColumn neq "") {
										//	exchangeDate = evaluate(#attributes.IllustrativePriceDateColumn#);
										//} else {
										//	exchangeDate = '';
										//};
										IllustrativePrice = application.com.currency.convert(
											method = #attributes.IllustrativePriceMethod#,
											fromValue = totalValue,
											fromCurrency = attributes.IllustrativePriceFromCurrency,
											toCurrency = attributes.IllustrativePriceToCurrency,
											date = ''
											);
									</cfscript>
									<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
								</cfcase>
							</cfswitch>
							<cfswitch expression="#cellType#">
							<!--- need a case here for type 1 - number format --->
								<cfcase value="1">
									<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '"' />
								</cfcase>
								<cfcase value="2">
									<cfset excelCellAttribute = ' x:num="' & IllustrativePrice.toValue & '" class="' & excelCurrencyClass & '"' />
								</cfcase>
							</cfswitch>
							<!--- onmouseover="pop('#iif( attributes.columnTranslation, 'attributes.columnTranslationPrefix & thisCol', 'replace(thisCol,"_"," ","all")' )#','cccccc')" onmouseout="kill()" --->
							<td valign="top"#excelCellAttribute#><span <cfif attributes.showCellColumnHeadings eq "yes" or thisColumnHasToolTip>title="#thisTitleText#"</cfif>>#IllustrativePriceOutput#</span></td>
						</cfif>
						</cfoutput>
					</cfif>
					<!--- increment the column Counter --->
					<cfset colcounter = colcounter + 1>
					<cfif attributes.showFunctionList>
						<td valign="top"></td>
					</cfif>

				</cfloop>
			</tr>
		</cfoutput>
		</cfif>
	</cfif>
	<cfoutput></table></cfoutput>
	<cfif not createAsExcel or not attributes.noForm><cfoutput></form></cfoutput></cfif>
</cf_translate>
</cfsavecontent>

	<!--- WAB 2013-10-15 Seems as if very large reports generate so much whitespace that they overflow some buffer or other, and prevent onrequestEnd adding stuff to the htmlhead.
		problem goes away if white space is removed before outputting
	--->
	<cfset TableContent = application.com.regexp.removewhitespace(TableContent)>

	<!--- 2012/04/16 WAB saveAsExcel was not including the excelHeader--->

<cfif attributes.openAsExcel or attributes.saveAsExcel>
	<!--- 	WAB 2014-09-16 CASE 441722 Change the cfcontent to use a file rather than cfoutputing the content
		This solved problems when tfqo was used on the portal and called from within merge.cfc
		So the openAsExcel code now uses the file which was previously only created for saveAsExcel
	--->

	<cfsavecontent variable="excelHeader">
		<cfinclude template="/templates/excelHeaderInclude.cfm">
	</cfsavecontent>

	<!--- Get a web accessible temporary folder (not actually necessary for the openAsExcel, but needed for the saveAsExcel)
		write the excel file to that location
	 --->
	<cfset tempDir = application.com.fileManager.getTemporaryFolderDetails()>
	<cfset caller.tfqo = {excelAbsolutePath = "#tempDir.path##attributes.excelfilename#",excelWebPath="#tempDir.webpath##attributes.excelfilename#"}>
	<cffile action="write" file="#caller.tfqo.excelAbsolutePath#" output="#excelHeader##tableContent#</body>" charset="utf-8">

	<cfif attributes.openAsExcel>
		<!--- 2009/06/02 GCC LID2315 This charset declaration ensures excel recognises the doc as UTF-8 and treats special characters correctly
		It will erase any page contents when in excel mode that are delivered before this point - I don't see this as a problem...
		--->
		<cfheader name="content-Disposition" value="filename=#attributes.excelFileName#">
		<cfcontent type="application/msexcel;charset=UTF-8" file="#caller.tfqo.excelAbsolutePath#" deleteFile="yes">
	</cfif>


<cfelse>
		<cfoutput>#TableContent#</cfoutput>
</cfif>






<cfsetting enablecfoutputonly="No">

