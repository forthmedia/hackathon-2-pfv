<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	******************************
			RELAYXMLREPORT
	******************************
	
	David A McLean 2002 21 Jan
	
	Attributes:
	
	entityRecordID
	
	
	
		Things to Do
		
		1. Row Numbers
		2. Sums
		3. Graphs
		4. Download
		5. Radio Done
		6. Row CheckBoxes and Function Dropdown
		7. PersonID filter Done
		8. Check Box filter
		9. Formatting
		
		
***************************************************************************

Original source Code and concept :
File name:		QuickReport.cfm
Author:			SWJ
Date created:	2 Aug 2000

Date (YYYY-MM-DD)	Initials 	What was changed
2007-09-17			SSS			There where a load of <cfsets inside getReportData query block moving this out of the query block 
								fixed the problem
2011/04/27 			WAB			LID 6322 - fixed problems passing querystring to next page.  reportname being duplicated and startrow not being removed								 
2011/11/28			NYB			LHID8224 replaced "No records were Found.", "1 record was found." and "#RecordCount# records were found."  
								with translations:  Sys_recordsFound will need to be edited to replace (s) with s and Sys_recordFound 
								will need to be added (both in sysTranslated_Phrases.txt) before this can be released

--->

<CFOUTPUT>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="/javascript/InputSQLCheck.js"></SCRIPT>
</CFOUTPUT>
<SCRIPT LANGUAGE="JavaScript1.2">

	function SetFilterSelectValues(nVariable,nValue) {
		// Change the contents of the filter values list
		// depending upon the value selected in teh filter column list
		
		if(nValue == ""){
			document.FilterSelectForm.submit()	
		}else{

			nSelect = document.FilterSelectForm.FilterSelectValues
			nSelect.options.length = 0
			
			opt = new Option()
			opt.text  = "            "
			opt.value = ""
			nSelect.options[nSelect.options.length]=opt
			
			x=nVariable.indexOf(",")
			y=nVariable.substr(0,x)
			nVariable = nVariable.slice(x+1,nVariable.length)
			opt = new Option()
			opt.text  = y
			opt.value = y
			nSelect.options[nSelect.options.length]=opt
			
			while (x>-1){
				x=nVariable.indexOf(",")
				y=nVariable.substr(0,x)
				nVariable = nVariable.slice(x+1,nVariable.length)
				opt = new Option()
				opt.text  = y
				opt.value = y
				nSelect.options[nSelect.options.length]=opt
			}
			nSelect.options[nSelect.options.length-1]=null
		}
	}
	
	function SortSubmit(nColumn,nSortType){

		document.ColumnHeaderForm.SortType.value = nSortType
		document.ColumnHeaderForm.SortReportBy.value = nColumn
		document.ColumnHeaderForm.submit()
	
	}
	
	function InputFilterRemove(){
	
		for (var i in document.FilterInputForm){
			if (i.indexOf("InputFilter_") > -1){
				nField = eval("document.FilterInputForm." + i)
				nField.value = ""
			}
		}
		document.FilterInputForm.submit()
	}
	
	function InputFilterApply(){
	
		for (var i in document.FilterInputForm){
			if (i.indexOf("InputFilter_") > -1){
				nField = eval("document.FilterInputForm." + i)
				x = nField.value
				CheckSQLInput(x,document.FilterInputForm,"<CFOUTPUT>#request.currentSite.protocolAndDomain#</CFOUTPUT>","<CFOUTPUT>#jsStringFormat(SCRIPT_NAME)#</CFOUTPUT>")
			}
		}
		document.FilterInputForm.submit()
	}
	
</SCRIPT>

<!---	***************************
			INITIALISE
		***************************
--->



<CFSET errors="">

<CFSET AttributesError = "">
<CFIF IsDefined("attributes.ImagePath")>
	<CFSET ImagePath=attributes.ImagePath>
<CFELSE>
	<CFSET ImagePath="/images/misc">
</CFIF>

<!--- Check Attributes not in XML File --->
<CFIF IsDefined("attributes.FileLocation")>
	<CFSET FileLocation=attributes.FileLocation>
<CFELSE>
	<CFSET FileLocation=replace("#application.paths.relayware#\XML\ReportList.xml","/","\")>
</CFIF>

<CFIF not fileExists("#FileLocation#")>
	<CFSET AttributesError="Error 1001: No ReportList.xml file found; looking for #FileLocation#">
</CFIF>		
<CFIF IsDefined("attributes.reportName")>
	<CFSET RelayReportName=attributes.reportName>
<CFELSE>
	<CFSET AttributesError = "Error 1002: No reportName Attribute has been supplied; this is a required Attribute.">
</CFIF>
<CFIF IsDefined("attributes.FilterAttributeValue")>
	<CFSET FilterAttributeValue=attributes.FilterAttributeValue>
</CFIF>
<CFIF IsDefined("attributes.FilterAttribute")>
	<CFSET FilterAttribute=attributes.FilterAttribute>
</CFIF>
<CFIF IsDefined("attributes.FilterWhereClause")>
	<CFSET FilterWhereClause=attributes.FilterWhereClause>
</CFIF>

<CFIF IsDefined("attributes.FormFieldNames")>
	<CFSET FormFieldNames=attributes.FormFieldNames>
</CFIF>

<CFSET ReservedFieldList = "InputFilter_,RadioFilter_,SubmitInputFilter,SortReportBy,SortType,Reportname,startrow,FilterSelect,FilterSelectValues">

<!--- Get Report XML Parameters --->
<cffile action="read" file="#FileLocation#" variable="myxml">
<cf_xmlq name="getReportItems" xpath="//report[@name='#RelayReportName#']" xml="#myxml#">
<cf_xmlq name="getFields" xpath="//report[@name='#RelayReportName#']/Field" xml="#myxml#">

<CFIF IsDefined("getFields.FIELD_NAME")>
	<CFSET fieldlist=valuelist(getFields.FIELD_NAME)>
<CFELSE>
	<CFSET AttributesError = "Error 1003: No fieldlist Attribute has been supplied; this is a required Attribute.">
</CFIF>

<CFIF IsDefined("getFields.FIELD_TITLE")>
	<CFSET fieldtitlelist =valuelist(getFields.FIELD_TITLE)>
<CFELSE>
	<CFSET AttributesError = "Error 1004: No fieldtitlelist Attribute has been supplied; this is a required Attribute.">
</CFIF>

<CFIF IsDefined("getFields.FIELD_EDITORURL")>
	<CFSET editorurllist = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_EDITORURL) IS 0>
			<CFSET editorurllist = listappend(editorurllist,"none",",")>
		<CFELSE>
			<CFSET editorurllist = listappend(editorurllist,FIELD_EDITORURL,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_GROUP")>
	<CFSET FieldGrouping = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_GROUP) IS 0>
			<CFSET FieldGrouping = listappend(FieldGrouping,"none",",")>
		<CFELSE>
			<CFSET FieldGrouping = listappend(FieldGrouping,FIELD_GROUP,",")>
			<CFSET GroupField= FIELD_GROUP>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_FORMATFUNCTION")>
	<CFSET FormatFunction = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_FORMATFUNCTION) IS 0>
			<CFSET FormatFunction = listappend(FormatFunction,"none",",")>
		<CFELSE>
			<CFSET FormatFunction = listappend(FormatFunction,FIELD_FORMATFUNCTION,",")>
		</CFIF>

	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_FORMATMASK")>
	<CFSET FormatMask = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_FORMATMASK) IS 0>
			<CFSET FormatMask = listappend(FormatMask,"none",",")>
		<CFELSE>
			<CFSET FormatMask = listappend(FormatMask,FIELD_FORMATMASK,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_TDALIGN")>
	<CFSET TDAlign = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_TDALIGN) IS 0>
			<CFSET TDAlign = listappend(TDAlign,"none",",")>
		<CFELSE>
			<CFSET TDAlign = listappend(TDAlign,FIELD_TDALIGN,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_TDVALIGN")>
	<CFSET TDVAlign = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_TDVALIGN) IS 0>
			<CFSET TDVAlign = listappend(TDVAlign,"none",",")>
		<CFELSE>
			<CFSET TDVAlign = listappend(TDVAlign,FIELD_TDVALIGN,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_TDCLASS")>
	<CFSET TDClass = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_TDCLASS) IS 0>
			<CFSET TDClass = listappend(TDClass,"none",",")>
		<CFELSE>
			<CFSET TDClass = listappend(TDClass,FIELD_TDCLASS,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getFields.FIELD_SUM")>
	<CFSET fieldsumlist = "">
	<CFLOOP QUERY="getFields">
		<CFIF len(FIELD_SUM) IS 0>
			<CFSET fieldsumlist = listappend(fieldsumlist,"none",",")>
		<CFELSE>
			<CFSET fieldsumlist = listappend(fieldsumlist,FIELD_SUM,",")>
		</CFIF>
	</CFLOOP>
</CFIF>

<CFIF IsDefined("getReportItems.report_OddEvenFormat") AND getReportItems.report_OddEvenFormat IS "Yes">
	<CFSET evenRow="evenRow">
	<CFSET oddRow="oddRow">
<CFELSE>
	<CFSET evenRow="">
	<CFSET oddRow="">
</CFIF>
<CFIF IsDefined("getReportItems.report_SortByColumns")>
	<CFSET SortByColumns ="#getReportItems.report_SortByColumns#">
<CFELSE>
	<CFSET SortByColumns ="yes">
</CFIF>
<CFIF IsDefined("getReportItems.report_AddNew") and isdefined("editorurllist")>
	<CFSET AddNew ="#getReportItems.report_AddNew#">
	<CFIF AddNew IS Not "Yes">
		<CFSET AddNew ="No">
	<CFELSE>
		<CFLOOP LIST="#editorurllist#" INDEX="nURL">
			<CFIF nURL IS NOT "none">
				<cfif nURL eq "generic"><!--- use the generic editor screen --->..
					<CFSET AddNewURL="/callTemplate.cfm?template=templates/editXMLGeneric.cfm&add=Yes&Editor=" & getReportItems.Report_Editor>
				<cfelse>
					<CFSET AddNewURL=nURL & "?add=Yes" & "&Editor=" & getReportItems.Report_Editor>
				</cfif>
				<CFIF Isdefined("caller.ElementID")><!--- i.e. it's being used within elementTemplate ? --->
					<CFSET AddNewURL=AddNewURL & "&ElementID=" & caller.ElementID>
				</CFIF>
			</CFIF>
		</CFLOOP>
	</CFIF>
<CFELSE>
	<CFSET AddNew ="No">
</CFIF>
<!--- Get the report Data 
	Note FilterSelect and SortReportBy
--->
<!--- Look for # parenthesis variables in the query string --->
<CFIF IsDefined("getReportItems.report_query")>
	<CFSET nQuery = getReportItems.report_query>
	<CFSET y = 1>
	<CFSET x = findnocase(chr(35),nQuery,y)>
	<CFLOOP CONDITION="x GT 0">
		<CFSET x = findnocase(chr(35),nQuery,y)>
		<CFIF x GT 0>
			<CFSET y = findnocase(chr(35),nQuery,x + 1)>
			<CFSET nVariable = mid(nQuery,x+1, (y-x)-1)>
			<CFTRY>
				<CFSET nValue = evaluate("#nVariable#")>
				<CFSET CurrentScope=true>
				<CFCATCH>
					<CFSET CurrentScope=false>
				</CFCATCH>
			</CFTRY>
			<CFIF CurrentScope IS true>
				<CFSET nQuery = RemoveChars(nQuery, x, (y-x)+1)>
				<CFSET nValue = evaluate("#nVariable#")>
				<CFSET nQuery = Insert(nValue, nQuery, x-1)>
			<CFELSEIF IsDefined("caller.#nVariable#")>
				<CFSET nQuery = RemoveChars(nQuery, x, (y-x)+1)>
				<CFSET nValue = evaluate("caller.#nVariable#")>
				<CFSET nQuery = Insert(nValue, nQuery, x-1)>
			<CFELSE>
				<CFSET AttributesError = "Error 1005: Variable #nVariable# is refered to in the report query however it is not available in the caller scope.">
				<CFOUTPUT>#htmleditformat(AttributesError)#</CFOUTPUT>
				<CF_ABORT>
			</CFIF>
			
		</CFIF>
	</CFLOOP>
	

	<CFTRY>
	<CFQUERY name="getALLReportData" datasource="#application.siteDataSource#">
		#preservesinglequotes(nQuery)# 
	</CFQUERY>

	<!--- RADIO Filter 
		this needs to be done before the query because they often have a defualt filter 
	
	--->
	<cf_xmlq name="getReportRadioFilter" xpath="//report[@name='#RelayReportName#']/RadioFilter" xml="#myxml#">

	<CFIF getReportRadioFilter.recordcount GT 0>
		<CFIF IsDefined("getReportRadioFilter.RadioFilter_FieldName")>
			<CFSET RadioFilter_FieldNames =valuelist(getReportRadioFilter.RadioFilter_FieldName)>
			<CFLOOP LIST="#RadioFilter_FieldNames#" INDEX="thisfield">
				<CFIF Not findnocase(thisfield,getALLReportData.columnlist) GT 0>
			<CFSET AttributesError = "Error 1006: The value #thisfield# is supplied as a RadioFilter_FieldName, however this is not a memeber of the query column list; #getALLReportData.columnlist#.">
				</CFIF>
			</CFLOOP>
		<CFELSE>
			<CFSET AttributesError = "Error 1007: A RadioFilter_FieldNames has not been supplied; this is a required Attribute when RadioFilter is supplied.">
		</CFIF>
		<CFIF IsDefined("getReportRadioFilter.RadioFilter_DisplayName")>
			<CFSET RadioFilter_DisplayName =valuelist(getReportRadioFilter.RadioFilter_DisplayName)>
		<CFELSE>
			<CFSET AttributesError = "Error 1008: A RadioFilter_DisplayName has not been supplied; this is a required Attribute when RadioFilter is supplied.">
		</CFIF>
		<CFIF IsDefined("getReportRadioFilter.RadioFilter_FilterFunction")>
			<CFSET RadioFilter_FilterFunction=valuelist(getReportRadioFilter.RadioFilter_FilterFunction)>
		<CFELSE>
			<CFSET AttributesError = "Error 1009: A RadioFilter_FilterFunction has not been supplied; this is a required Attribute when RadioFilter is supplied.">
		</CFIF>
		<CFIF IsDefined("getReportRadioFilter.RadioFilter_FilterDisplay")>
			<CFSET RadioFilter_FilterDisplay=valuelist(getReportRadioFilter.RadioFilter_FilterDisplay)>
		<CFELSE>
			<CFSET AttributesError = "Error 1010: A RadioFilter_FilterDisplay has not been supplied; this is a required Attribute when RadioFilter is supplied.">
		</CFIF>
		<CFIF IsDefined("getReportRadioFilter.RadioFilter_Default")>
			<CFSET RadioFilter_Defaults = "">
			<CFLOOP QUERY="getReportRadioFilter">
				<CFIF RadioFilter_Default IS "">
					<CFSET RadioFilter_Defaults = listappend(RadioFilter_Defaults,"no",",")>
				<CFELSE>
					<CFSET RadioFilter_Defaults = listappend(RadioFilter_Defaults,RadioFilter_Default,",")>
					<CFIF Not IsDefined("form.RadioFilter_#getReportRadioFilter.RadioFilter_FieldName#")>
						<CFSET "form.RadioFilter_#getReportRadioFilter.RadioFilter_FieldName#" = getReportRadioFilter.RadioFilter_FilterFunction>
					</CFIF>
				</CFIF>
			</CFLOOP>
		</CFIF>
	</CFIF>
		
		<CFSET POS_OrderBy = findnocase("Order By",nQuery)>
		<CFIF POS_OrderBy GT 0 AND IsDefined("SortReportBy")>
			<CFSET Use_Query = Left(nQuery,POS_OrderBy -1)>
		<CFELSEIF POS_OrderBy GT 0 AND NOT IsDefined("SortReportBy")>
			<CFSET Use_Query = Left(nQuery,POS_OrderBy -1)>
			<CFSET Use_OrderBy = Right(nQuery,len(nQuery)-POS_OrderBy + 1)>
		<CFELSE>
			<CFSET Use_Query = nQuery>
		</CFIF>
		<CFIF IsDefined("FilterWhereClause")>
			<CFIF findnocase("where",#nQuery#) EQ 0>
				<CFSET Use_Query= Use_Query & " WHERE " & FilterWhereClause>
			<CFELSE>
				<CFSET Use_Query= Use_Query & " AND " & FilterWhereClause>
			</CFIF>
		</CFIF>
	<CFQUERY name="getReportData" datasource="#application.siteDataSource#">
			#preservesinglequotes(Use_Query)#
		<CFIF IsDEfined("FilterAttribute") AND IsDefined("FilterAttributeValue")>
			<CFSET nCounter=1>
			<CFLOOP LIST="#FilterAttribute#" INDEX="FA">
				<CFSET FAV = gettoken("#filterattributevalue#",nCounter)>
				<CFIF findnocase("group by",#nQuery#)>
					HAVING #FA# =  <cf_queryparam value="#FAV#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				<CFELSE>
					<CFIF findnocase("where",#nQuery#) EQ 0>
						WHERE #FA# =  <cf_queryparam value="#FAV#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					<CFELSE>
						AND #FA# =  <cf_queryparam value="#FAV#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</CFIF>
				</CFIF>
				<CFSET nCounter=nCounter+1>
			</CFLOOP>
		</CFIF>
		<CFIF IsDefined("FilterSelect") AND FilterSelect IS NOT "">
			<CFSET FormFilterSelect = FilterSelect>
			<CFSET FormFILTERSELECTVALUES = FILTERSELECTVALUES>
			<CFIF findnocase("group by",#Use_Query#)>
				HAVING #FilterSelect# =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			<CFELSE>
				<CFIF findnocase("where",#Use_Query#) EQ 0>
					WHERE #FilterSelect# =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				<CFELSE>
					AND #FilterSelect# =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				</CFIF>
			</CFIF>
		<CFELSE>
			<CFIF findnocase("group by",#Use_Query#)>
					HAVING 1 = 1
			<CFELSE>
				<CFIF findnocase("where",#Use_Query#) EQ 0>
						WHERE 1 = 1 
				</CFIF>
			</CFIF>
			<CFLOOP LIST="#fieldlist#" INDEX="thisfield">
				<CFIF IsDefined("InputFilter_#thisfield#") AND evaluate("InputFilter_#thisfield#") IS NOT "">
					<CFSET InputFilters = True>
					AND #thisfield# #evaluate("InputFilter_#thisfield#")#
				</CFIF>
			</CFLOOP>
			<CFIF Not IsDefined("InputFilters")>
				<CFLOOP LIST="#getALLReportData.columnlist#" INDEX="thisfield">
					<CFIF IsDefined("RadioFilter_#thisfield#") AND evaluate("RadioFilter_#thisfield#") IS NOT "">
						<CFSET RadioFilters = True>
						AND #thisfield# #evaluate("RadioFilter_#thisfield#")#
					</CFIF>
				</CFLOOP>
			</CFIF>
		</CFIF>
		<CFIF IsDefined("SortReportBy")>
			Order By <cf_queryObjectName value="#SortReportBy#"> #SortType#
		<CFELSE>
			<CFIF IsDefined("Use_OrderBy")>
				#Use_OrderBy#
			</CFIF>
		</CFIF>

	</CFQUERY>
	<CFCATCH>
			<CFQUERY name="getReportData" datasource="#application.siteDataSource#">
				#preserveSingleQuotes(nQuery)# 
			</CFQUERY>
			<CFSET BadInPutFilter = "The filter you entered caused an error.">
	</CFCATCH>
	<cfcatch type="Database">
		<CFSET AttributesError="Error 1011: The following database error was encountered while executing the report Query : <br><br>#cfcatch.detail#">
	</CFCATCH>
	</CFTRY>
	<!--- Perform full query to ensure we have all distinct values for Filtering --->

<CFELSE>
	<CFSET AttributesError= "Error 1012: No Query has been specificed">
</CFIF>
<!--- Is There an Editor Defined? --->
<CFIF isDefined("getReportItems.Report_Editor")>
	<CFIF isDefined("getReportItems.Report_PrimaryKey")>
		<CFIF findnocase(getReportItems.Report_PrimaryKey,getReportData.columnlist) EQ 0>
			<CFSET AttributesError= "Error 1013: A primary key has been defined that is not returned in the report's query column list.">
		</CFIF>
	<CFELSE>
		<CFSET AttributesError= "Error 1014: A primary key must be specified as an Editor has been specified.">
	</CFIF>
</CFIF>

<!--- INPUT FILTER --->

<CFIF IsDefined("getReportItems.report_FilterInputList")>
	<CFSET FilterInputField ="#getReportItems.report_FilterInputList#">
</CFIF>

<CFIF AttributesError IS NOT "">
	<CFOUTPUT>#htmleditformat(AttributesError)#</CFOUTPUT>
	<CF_ABORT>
</CFIF>

<!--- SELECT FILTERS --->
<cf_xmlq name="getReportSelectFilter" xpath="//report[@name='#RelayReportName#']/FilterSelect" xml="#myxml#">

<!--- If there are any filters defined in the XML 
	Set Up the filter lists. This requires building lists of distinct values for each
	filter field, in Javascript, so that the contents of the select lists can be
	changed at will.
--->
<CFIF getReportSelectFilter.recordcount GT 0>
	<CFSET FilterSelectFieldList = valuelist(getReportSelectFilter.FilterSelect_FieldName)>
	<CFLOOP LIST="#FilterSelectFieldList#" INDEX="nFieldName">
		<CFQUERY Name="Filter#nFieldName#" dbtype="query">
			SELECT DISTINCT #nFieldName# AS FilterValue FROM getALLReportData Order By <cf_queryObjectName value="#nFieldName#">
		</CFQUERY>
		<CFSET nQuery=evaluate("Filter#nFieldName#")>
		<CFSET x=valuelist(nQuery.FilterValue)>
		<CFOUTPUT>	
		<SCRIPT LANGUAGE="JavaScript1.2">
			
			var #jsStringFormat(nFieldName)# = '#jsStringFormat(x)#,'
		
		</SCRIPT>
		</CFOUTPUT>
	</CFLOOP>
</CFIF>


<!--- This next setting needs to be set after calling XML tags 
		or they will crash
--->
<cfsetting enablecfoutputonly = "yes">

<!--- set recordcount of the query --->
<cfset recordcount = getReportData.recordcount>


<!--- define basic report parameters --->
<!--- relative filename of images --->
<cfparam name="attributes.root" default="">
<cfparam name="attributes.backimg" default="back.gif">
<cfparam name="attributes.pagenext" default="pagenext.gif">
<cfparam name="attributes.pageprev" default="pageprev.gif">
<!--- title, display on the header bar of the report. --->
<cfparam name="attributes.title" default="Untitled Report">
<cfparam name="attributes.showHeader" default="Yes">
<!--- Additional text to display in header section --->
<cfparam name="attributes.info" default=" ">
<!--- define start and end rows of data to display --->

<CFPARAM name="caller.url.startrow" default="1">
<CFPARAM name="caller.url.endrow" default="1">
<CFPARAM name="caller.form.startrow" default="0">
<CFPARAM name="caller.form.endrow" default="0">

<!--- define number of steps back the back button goes when clicked (uses javascript:history.go(-#stepsback#)--->
<cfparam name="attributes.stepsback" default="1">





<CFIF IsDefined("getReportItems.report_maxRows")>
	<cfif not isnumeric(getReportItems.report_maxRows)>
		<cfset errors=errors&"A non numeric value was entered for Maxrows, 18 has been used">
	</cfif>
	<CFIF IsDefined("caller.url.maxrows")>
		<CFSET maxrows = caller.url.maxrows>
	<CFELSE>
		<cfset maxrows=getReportItems.report_maxRows>
	</CFIF>
	
	<cfset startrow=caller.url.startrow>
	<cfset endrow=caller.url.endrow>
	<cfif caller.form.startrow gt 0>
		<cfset startrow=caller.form.startrow>
		<cfset endrow=caller.form.endrow>
	</cfif>
	<!--- set range of data to display for this page --->

	<CFSET endrow=startrow+maxrows-1>

	<!--- check to see if potential last row exceeds number of records in query --->
	<CFIF endrow gt recordcount>
	<!--- if it does, set the end row to be the LASt row --->
	<CFSET endrow=recordcount>
	</CFIF>
<CFELSE>
	<CFSET startrow=1>
	<CFSET endrow=recordcount>
</CFIF>

<cfset title=getReportItems.report_title>
<cfset info=attributes.info>

<cfset stepsback=attributes.stepsback>

<!--- if output is "scr" i.e. screen, create as table formatted data. we may in the future add various other output types i.e. .csv, .tab, XML etc etc --->
<CFIF AttributesError IS NOT "">
	<CFOUTPUT>#AttributesError#</CFOUTPUT>
	<CF_ABORT>
</CFIF>

<cfsetting enablecfoutputonly="no">
<!---create header bar containing back button, title, recordcounts, next & previous buttons for data, extra info.....--->
<CFOUTPUT>

<!--- ****************************************************************************************************************
				Title Section																						  
***************************************************************************************************************** --->
<CFIF getReportItems.report_showHeader eq "Yes">
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>	
			<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
				<A HREF="../reportHome.cfm" Class="Submenu">Report Menu&nbsp;&nbsp;</A>
			</TD>
		</TR>
	</table>
<CFELSE>
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TH ALIGN="LEFT" VALIGN="top">
				<B>#htmleditformat(title)#</B>
			</TH>
			<TH ALIGN="Right" VALIGN="top">
				<CFIF IsDefined("AddNew") AND AddNew EQ "Yes">
					<A href="#AddNewURL#" CLASS="submenu">Add New Record</A>
				<CFELSE>
					&nbsp;
				</CFIF>
			</TH>
		</TR>
		
	</table>

</CFIF>
<!--- ****************************************************************************************************************
				Control section																						  
***************************************************************************************************************** --->

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">

	<TR>
		<td>&nbsp;</td>
		<TD><!---display number of records, if appropriate --->
			<CFIF IsDefined("getReportData.RecordCount")>
				<cf_translate>
				<CFIF RecordCount EQ 0>
					phr_sys_noRecordsFound
				<CFELSEIF RecordCount EQ 1>
					1 phr_sys_recordsFound
				<CFELSE>
					#htmleditformat(RecordCount)# phr_sys_recordFound
				</CFIF>
				</cf_translate>
			</CFIF>
		#htmleditformat(info)#
		</TD>
	</tr>
	<CFIF isDefined("maxrows")>
<!--- show which record rows are being listed --->
	<CFIF recordcount gt getReportItems.report_maxRows >
		<TR>
			<td>&nbsp;</td>
			<TD>
				<cfset thispage=ceiling(startrow/maxrows)>
				<!--- remove current startrow from query string, as we will be appending a new start row for links --->
				<cfset useurl = application.com.regExp.removeItemFromNameValuePairString(inputString = request.query_string,itemToDelete = "startRow", delimiter="&")>
				<!--- Wab 2011/04/27 replace this with above code which is more reliable <cfset useurl=rereplace(request.query_string,'&startrow=[0-9][0-9][0-9]','','all')> --->
				
				<CFIF recordCount lte maxrows>
					Page 1 of 1 (Records #startrow# - #endrow#)
				<CFELSE>
					<form name="navform" method="post">
					Page <select name="startrow" onchange="Javascript:document.navform.submit()">
							<cfset count=0>
							<cfloop index="page" from="1" to="#recordcount#" step="#maxrows#"><cfset count=count+1>
								<option value=#page# <cfif count eq thispage>selected</cfif>>#count#
							</cfloop>
						</select>
					 (Records #startrow# - #endrow#)
					 	<CFIF IsDefined("FormFieldNames")>
							<CFLOOP LIST="#FormFieldNames#" INDEX="nField">
								<CFIF findnocase(nField,ReservedFieldList) EQ 0>
									<CFSET nFieldValue=evaluate("caller.#nField#")>
									<CF_INPUT Name="#nField#" Type="Hidden" Value="#nFieldValue#">
								</CFIF>
							</CFLOOP>
						</CFIF>
					</form>
				</CFIF>
				
			</td>
		</TR>
		<TR>
			<td>&nbsp;</td>
			<td>
				<cfif startrow gt 1>
					<cfset new=startrow-maxrows>
					<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#&startrow=#new#">Previous page</a>&nbsp;
				</cfif>
				<cfif endrow lt recordcount>
					<cfset new=startrow+maxrows>
					<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#useurl#&startrow=#new#">Next page</a>&nbsp;
				</cfif>
				<CFIF recordCount gt maxrows>
					<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#request.query_string#&maxrows=#RecordCount#&module=#RelayReportName#">Show All #htmleditformat(RecordCount)# Records</a>
				</cfif>
				<CFIF Isdefined("getReportItems.report_maxRows") and maxrows EQ RecordCount>
					<a href="#request.currentSite.httpProtocol##cgi.http_host##cgi.script_name#?#request.query_string#&maxrows=#getReportItems.report_maxRows#&module=#RelayReportName#">Show #htmleditformat(getReportItems.report_maxRows)# Records per page</a>
				</CFIF>
			</td>
		</TR>
	</CFIF>
	</CFIF>
</TABLE>



</CFOUTPUT>
<!--- --->
<!--- End info--->
<!--- --->

<!--- --->
<!--- Begin header--->
<!--- --->

<cfset go=1>
<cfif isdefined("getreportdata.recordcount")>
	<cfif getreportdata.recordcount eq 0>
		<cfset go=0>
	</cfif>
</cfif>
<cfif go eq 1>

<!--- ****************************************************************************************************************
				Filter and OrderBy	Section																			  
***************************************************************************************************************** --->

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<CFIF isDefined("BadInPutFilter")>
	<TR>
	<TD><CFOUTPUT>#htmleditformat(BadInPutFilter)#</CFOUTPUT>
	</TD>
	</TR>
	</CFIF>
	
	
		<!---	************************
					SELECT FILTERS 
				************************ --->
		<CFIF IsDefined("FilterSelectFieldList")>
		<FORM NAME="FilterSelectForm" METHOD="post">
		<TR WIDTH="100%">
			<TD>Filter <SELECT name="FilterSelect" onchange="SetFilterSelectValues(eval(document.FilterSelectForm.FilterSelect.options[document.FilterSelectForm.FilterSelect.selectedIndex].value),document.FilterSelectForm.FilterSelect.options[document.FilterSelectForm.FilterSelect.selectedIndex].value)">
					<OPTION VALUE= "">&nbsp;
				<CFLOOP LIST="#FilterSelectFieldList#" INDEX="nFieldSelect">
					
					<CFOUTPUT><OPTION VALUE= "#nFieldSelect#" <CFIF IsDefined("FormFilterSelect") AND FormFilterSelect IS nFieldSelect>SELECTED</CFIF>>#htmleditformat(nFieldSelect)#</CFOUTPUT>
				</CFLOOP>
			</SELECT></TD>
			<TD> by <SELECT name="FilterSelectValues" onchange="document.FilterSelectForm.submit()">

					<CFIF IsDefined("FormFilterSelectValues")>
					<CFSET nQuery=evaluate("Filter#FormFilterSelect#")>
 						<CFLOOP Query="nQuery">
							<CFOUTPUT><OPTION Value="#FilterValue#"<CFIF FilterValue IS FormFilterSelectValues>SELECTED</CFIF>>#htmleditformat(FilterValue)#</CFOUTPUT>
						</CFLOOP> 
					<CFELSE>
						<OPTION VALUE= "">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</CFIF>
			</SELECT></TD>
			
		<CFOUTPUT>		
		<CFIF IsDefined("RelayReportName")>
			<CF_INPUT Name="Reportname" Type="Hidden" Value="#RelayReportName#">
		</CFIF>
		<CFIF IsDefined("SortReportBy")>
			<CF_INPUT Name="SortReportBy" Type="Hidden" Value="#SortReportBy#">
		</CFIF>
		<CFIF IsDefined("SortType")>
			<CF_INPUT Name="SortType" Type="Hidden" Value="#SortType#">
		</CFIF>
		</CFOUTPUT>
		</TR>
		</FORM>
		</CFIF>
		
		<!---	************************
					INPUT FILTERS 
				************************ --->
		<FORM NAME="FilterInputForm" METHOD="post">
		<TR>
		<CFIF IsDefined("FilterInputField")>
		<CFOUTPUT>
			<CFSET count=0>
			<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">
				<CFSET count=count+1>
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
				<CFSET nInputField=gettoken(FilterInputField,count,",")>
				<CFIF nInputField EQ 1>
				<cftry>
					<cfset tmp=evaluate("getreportdata.#thiscolfield#")>
					<CFIF IsDefined("InputFilter_#thiscolfield#") AND evaluate("InputFilter_#thiscolfield#") IS NOT "">
						<CFSET thisFilterValue = evaluate("InputFilter_#thiscolfield#")>
					<CFELSE>
						<CFSET thisFilterValue = "">
					</CFIF>
							<TD>#htmleditformat(thiscol)#&nbsp;<CF_INPUT NAME="InputFilter_#thiscolfield#" TYPE="Text" VALUE="#thisFilterValue#"></TD>
					<cfcatch>
						<cfset errors = errors &"<br>An invalid column name (#thiscolfield#) was entered">
						<TD>Invalid Column</TD>
					</cfcatch>
				</cftry>
				</CFIF>
			</CFLOOP>
			<TD><INPUT NAME="SubmitInputFilter" TYPE="Button" VALUE="Apply" onclick="InputFilterApply()"></TD>
			<TD><INPUT NAME="SubmitInputFilter" TYPE="Button" VALUE="Remove" onclick="InputFilterRemove()"></TD>
		</cfoutput>
		</CFIF>
		<CFOUTPUT>		
		<CFIF IsDefined("RelayReportName")>
			<CF_INPUT Name="Reportname" Type="Hidden" Value="#RelayReportName#">
		</CFIF>
		<CFIF IsDefined("SortReportBy")>
			<CF_INPUT Name="SortReportBy" Type="Hidden" Value="#SortReportBy#">
		</CFIF>
		<CFIF IsDefined("SortType")>
			<CF_INPUT Name="SortType" Type="Hidden" Value="#SortType#">
		</CFIF>
		</CFOUTPUT>
		</TR>
		</FORM>
		<!---	************************
					RADIO FILTERS 
				************************ --->
		<FORM NAME="FilterRadioForm" METHOD="post">
		<CFIF getReportRadioFilter.recordcount GT 0>
			
			<CFSET x_thisRadioField="">
			<CFSET nCounter=1>
			<CFLOOP LIST="#RadioFilter_FieldNames#" INDEX="thisRadioField">
				<CFIF x_thisRadioField Is Not thisRadioField>
					<CFIF ncounter GT 1></TR></CFIF>
					<TR>
					<CFOUTPUT>#gettoken(RadioFilter_DisplayName,nCounter,",")# is </CFOUTPUT>
					<CFSET CheckedField=false>
				</CFIF>

				<CFIF evaluate("RadioFilter_#thisRadioField#") EQ gettoken(RadioFilter_FilterFunction,nCounter,",")>
					<CFSET CheckIt=true><CFSET CheckedField=true>
				<CFELSEIF CheckedField IS false AND gettoken(RadioFilter_Defaults,nCounter,",") IS "Yes">
					<CFSET CheckIt=true>
				<CFELSE>
					<CFSET CheckIt=false>
				</CFIF>
				<CFOUTPUT> #gettoken(RadioFilter_FilterDisplay,nCounter,",")# <CF_INPUT TYPE="Radio" NAME="RadioFilter_#thisRadioField#" VALUE="#gettoken(RadioFilter_FilterFunction,nCounter,",")#" CHECKED="#iif( CheckIt IS true,true,false)#" onclick="document.FilterRadioForm.submit()"></CFOUTPUT>
				
				<CFSET nCounter=nCounter+1>
				<CFSET x_thisRadioField=thisRadioField>
			</CFLOOP></TR>
		</CFIF>
		<CFOUTPUT>		
		<CFIF IsDefined("RelayReportName")>
			<CF_INPUT Name="Reportname" Type="Hidden" Value="#RelayReportName#">
		</CFIF>
		<CFIF IsDefined("SortReportBy")>
			<CF_INPUT Name="SortReportBy" Type="Hidden" Value="#SortReportBy#">
		</CFIF>
		<CFIF IsDefined("SortType")>
			<CF_INPUT Name="SortType" Type="Hidden" Value="#SortType#">
		</CFIF>
		</CFOUTPUT>
		</FORM>


</TABLE>

<!--- ****************************************************************************************************************
				Listing section																						  
***************************************************************************************************************** --->

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM Name="ColumnHeaderForm" METHOD="post">
	<TR>
		<CFOUTPUT>
			<CFSET count=0>
			<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">
				<CFSET count=count+1>
				<CFSET thiscolfield=gettoken(fieldlist,count,",")>
				<CFSET thisGroup = "none">
				<CFIF isDefined("FieldGrouping")>
					<CFSET thisGroup = gettoken(FieldGrouping,count,",")>
				</CFIF>
				<CFIF thisGroup IS "none">
					<cftry>
						<cfset tmp=evaluate("getreportdata.#thiscolfield#")>
							<CFIF SortByColumns IS "Yes">
								<TH><TABLE CELLPADDING="0" CELLSPACING="0">
								<TR><TH></TH>
								<TH><A HREF="javascript:SortSubmit('#thiscolfield#','desc')"><IMG src="#imagePath#/desc.gif" BORDER="0" ALT="Sort by #thiscol# in descending order"></A></TH>
								<TH>&nbsp;#htmleditformat(thiscol)#&nbsp;</TH>
								<TH><A HREF="javascript:SortSubmit('#thiscolfield#','asc')"><IMG src="#imagePath#/asc.gif" BORDER="0" ALT="Sort by #thiscol# in ascending order"></A></TH>
								</TR>
								</TABLE></TH>
							<CFELSE>
								<TH>#htmleditformat(thiscol)#&nbsp;</TH>
							</CFIF>
						<cfcatch>
							<cfset errors = errors &"<br>An invalid column name (#thiscolfield#) was entered">
							<TH>Invalid Column</TH>
						</cfcatch>
					</cftry>
				</CFIF>
			</CFLOOP>
		</cfoutput>
	</TR>

	<CFOUTPUT>	
	<CFIF IsDefined("FormFilterSelectValues")>
		<CF_INPUT Name="FilterSelectValues" Type="Hidden" Value="#FormFilterSelectValues#">
	</CFIF>
	<CFIF IsDefined("FormFilterSelect")>
		<CF_INPUT Name="FilterSelect" Type="Hidden" Value="#FormFilterSelect#">
	</CFIF>
	<CFLOOP LIST="#getALLReportData.columnlist#" INDEX="thisfield">
		<CFIF IsDefined("InputFilter_#thisfield#") AND evaluate("InputFilter_#thisfield#") IS NOT "">
			<CFIF IsDefined("InputFilter_#thisfield#") AND evaluate("InputFilter_#thisfield#") IS NOT "">
				<CFSET thisFilterValue = evaluate("InputFilter_#thisfield#")>
			<CFELSE>
				<CFSET thisFilterValue = "">
			</CFIF>
			<CF_INPUT NAME="InputFilter_#thisfield#" TYPE="hidden" VALUE="#thisFilterValue#">
		</CFIF>
		<CFIF IsDefined("RadioFilter_#thisfield#") AND evaluate("RadioFilter_#thisfield#") IS NOT "">
			<CFIF IsDefined("RadioFilter_#thisfield#") AND evaluate("RadioFilter_#thisfield#") IS NOT "">
				<CFSET thisFilterValue = evaluate("RadioFilter_#thisfield#")>
			<CFELSE>
				<CFSET thisFilterValue = "">
			</CFIF>
			<CF_INPUT NAME="RadioFilter_#thisfield#" TYPE="hidden" VALUE="#thisFilterValue#">
		</CFIF>
	</CFLOOP>
	<CFIF IsDefined("FormFieldNames")>
		<CFLOOP LIST="#FormFieldNames#" INDEX="nField">
			<CFIF findnocase(nField,ReservedFieldList) EQ 0>
				<CFSET nFieldValue=evaluate("caller.#nField#")>
				<CF_INPUT Name="#nField#" Type="Hidden" Value="#nFieldValue#">
			</CFIF>
		</CFLOOP>
	</CFIF>
	<CF_INPUT Name="Reportname" Type="Hidden" Value="#RelayReportName#">
	<INPUT Name="SortReportBy" Type="Hidden" Value="">
	<INPUT Name="SortType" Type="Hidden" Value="">
	</CFOUTPUT>
</FORM>
</cfif>

<cfsetting enablecfoutputonly = "no" >

<!--- assuming we have a query.....process the data --->

	<cfif getreportdata.recordcount gt 0>
		<!--- start outer loop through specified rows of data --->
		<CFSET maxrow=endrow-startrow + 1>
		<CFIF IsDefined("GroupField")>
			<CFINCLUDE TEMPLATE="ReportRowGroup.cfm">
		<CFELSE>
			<CFINCLUDE TEMPLATE="ReportRow.cfm">
		</CFIF>

		<!--- end outer loop of data --->
	</cfif>


<!--- --->
<!--- End Output--->
<!--- --->

<!--- --->
<!--- Begin Sum--->
<!--- --->

<cfif getreportdata.recordcount gt 0>
	<TR>
		<CFSET count=0>
		<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">
			<CFSET count=count+1>
			<CFSET thiscolfield=gettoken(fieldlist,count,",")>
			<CFSET thissumfield=gettoken(fieldlist,count,",")>
			<TH align=right>
				<cfset sumvar="sum_#thissumfield#">
				<cfset sumvar = rereplace(sumvar,'[[:punct::]]','','all')>
				<cfif isdefined(sumvar)>
					<cfset sum=evaluate("#sumvar#")>
					<CFIF IsDefined("showDecimals") AND showDecimals eq "yes">
						<CFOUTPUT>#numberformat(sum,"___,____,___.00")#</CFOUTPUT>
					<CFELSE>
						<CFOUTPUT>#numberformat(sum,"___,____,___")#</CFOUTPUT>
					</CFIF>
					
				<cfelse>
					&nbsp;
				</cfif>
			</TH>
		</CFLOOP>
	</TR>

</cfif>

</table>
<!--- <CFOUTPUT>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
	<TR>
		<TD HEIGHT="50" COLSPAN="3" BACKGROUND="/images/misc/footerbg2000.gif" STYLE="BORDER-RIGHT: steelblue 1pt solid;BORDER-Left: steelblue 1pt solid;BORDER-Bottom: steelblue 1pt solid;">		
			&nbsp;
		</TD>
	</TR>
</TABLE>
</CFOUTPUT> --->





<cfsetting enablecfoutputonly="no">
<cfif len(errors) gt 0>
<b>Some errors were encountered while generating this report.</b>
<br>
<cfoutput>#htmleditformat(errors)#</cfoutput>
</cfif>



