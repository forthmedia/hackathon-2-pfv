<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

2008/07/17 	NJH		Bug Fix T-10 Issue 805. Added Foreign Key to the editor link as XML Editor needs it if the table has a foreign key.

 --->

<cfloop query="getreportdata" startrow="#startrow#" ENDROW="#endrow#">

<CFOUTPUT><TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddRow"<CFELSE> class="evenRow"</CFIF>></CFOUTPUT>
<!--- set current col count --->
<CFSET count=0>
<!--- loop over the list of fields, to create columnsfor this row --->
<CFLOOP index="thiscol" list="#fieldtitlelist#" delimiters=",">
	<CFSET count=count+1>
	<!--- set field containing value --->
	<CFSET thiscolfield=gettoken(fieldlist,count,",")>
	<CFSET thissumfield=gettoken(fieldlist,count,",")>
	<CFSET thisGroup = "none">
	<CFIF isDefined("FieldGrouping")>
		<CFSET thisGroup = gettoken(FieldGrouping,count,",")>
	</CFIF>
	<CFIF thisGroup IS NOT "">
	
		<cfif len(trim(thiscolfield)) gt 0>
			<!--- get the value of this col --->
			<cftry>
				<cfset value=evaluate("getreportdata.#thiscolfield#")>
				<cfset valid=1>
				<cfcatch>
					<cfset value="Invalid Column">
					<cfset valid=0>
				</cfcatch>
			</cftry>
		
		<cfelse>
			<cfset value="">
			<cfset valid=1>
		</cfif>

		<cfset align="">
		<cfif isnumeric(value)>
			<!--- if the value is numeric, right justify this cell --->
			<cfset align="right">
		</cfif>	
		
		<!---
		
			Change
			
			add width and class
		
		
		---->
		
		
		
		<CFSET thisTDAlign = "left">
		<CFIF IsDefined("TDAlign")>
			<CFIF gettoken(TDAlign,count,",") IS NOT "none">
				<CFSET thisTDAlign = gettoken(TDAlign,count,",")>
			</CFIF>
		</CFIF>
		<CFSET thisTDVAlign = "top">
		<CFIF IsDefined("TDVAlign")>
			<CFIF gettoken(TDVAlign,count,",") IS NOT "none">
				<CFSET thisTDVAlign = gettoken(TDVAlign,count,",")>
			</CFIF>
		</CFIF>
		<CFSET thisTDClass = "">
		<CFIF IsDefined("TDClass")>
			<CFIF gettoken(TDClass,count,",") IS NOT "none">
				<CFSET thisTDClass = gettoken(TDClass,count,",")>
			</CFIF>
		</CFIF>
		
		<CFOUTPUT><TD valign="#thisTDVAlign#" align="#thisTDAlign#" class="#thisTDClass#"></CFOUTPUT>
		
		
		<CFSET VALUE=TRIM(VALUE)>
		<!--- replace special url chars so Netscape can cope, the bag of shite !
		--->
		<cfset urlvalue=replace(value," ","%20","all")>
		<!--- [insert drumroll here] output the value !!!--->
		<CFIF isDefined("fieldsumlist")>
		<cfif gettoken(fieldsumlist,count,",") eq 1>
			<!--- if it is a summable field, check for the existance of the variable holding its current total. 
				If this doesn't exist, create it. Replace out special chars in variable name.--->
			<CFIF IsDefined("showDecimals") AND showDecimals eq "yes">
				<cfset value=numberformat(value,"___,____,___.00")>
			<CFELSE>
				<cfset value=numberformat(value,"___,____,___")>
			</CFIF>

			<cfset sumvar="sum_#thissumfield#">
			<cfset sumvar = rereplace(sumvar,'[[:punct::]]','','all')>
			<cfif isdefined(sumvar)>
				<cfset "#sumvar#"=val(evaluate("#sumvar#"))+val(value)>
			<Cfelse>
				<cfset "#sumvar#"=val(value)>
			</cfif>
		</cfif>
		</CFIF>
		<CFOUTPUT>
				<!---
		
			Change
			
	
		---->
		
		<CFIF IsDefined("FormatFunction")>
			<CFSET thisFunction=gettoken(FormatFunction,count,",")>
			<CFSET thisMask="none">
			<CFIF IsDefined("FormatMask")>
				<CFSET thisMask=gettoken(FormatMask,count,",")>
			</CFIF>
			<CFIF thisFunction IS NOT "none">
				<CFIF thisMask IS NOT "none">
					<CFSET value = evaluate("#thisFunction#('#value#','#thisMask#')")>
				<CFELSE>
					<CFSET value = evaluate("#thisFunction#('#value#')")>
				</CFIF>
			</CFIF>
		</CFIF>
		
			<!--- if it is a URL field, check for the existance of the fieldlinkURL.--->
		<CFIF isDefined("getReportItems.Report_Editor") and gettoken(editorurllist,count,",") is not "none">
			<CFSET thisfieldlinkurl=gettoken(editorurllist,count,",")>
			<CFIF findnocase("?",thisfieldlinkurl, 1)>
				<CFSET thisfieldlinkurl = thisfieldlinkurl & "&" & "Editor=" & getReportItems.Report_Editor>
			<CFELSE>
				<CFSET thisfieldlinkurl = thisfieldlinkurl & "?" & "Editor=" & getReportItems.Report_Editor>
			</CFIF>
			<CFIF Isdefined("caller.ElementID")>
				<CFSET thisfieldlinkurl = thisfieldlinkurl & "&ElementID=" & caller.ElementID>
			</CFIF>
			<CFLOOP LIST="#getReportItems.Report_PrimaryKey#" INDEX="ThisPK">
				<CFSET thisfieldlinkurl = thisfieldlinkurl & "&#ThisPK#=" & #evaluate(ThisPK)#>
			</CFLOOP>
			<!--- NJH 2008/07/17 Bug Fix T-10 Issue 805. Added a foreign key into the link so that the xml editor could use it when needed --->
			<!--- NJH 2008/07/25 Bug Fix T-10 Issue 851... Hmmmm, didn't check if it was defined first and thereby created another bug. --->
			<cfif isDefined("getReportItems.Report_ForeignKey")>
			<CFLOOP LIST="#getReportItems.Report_ForeignKey#" INDEX="ThisFK">
				<CFSET thisfieldlinkurl = thisfieldlinkurl & "&#ThisFK#=" & #evaluate(ThisFK)#>
			</CFLOOP>
			</cfif>
			<CFIF IsDEfined("FilterAttribute") AND IsDefined("FilterAttributeValue")>
				<CFSET nCounter=1>
				<CFLOOP LIST="#FilterAttribute#" INDEX="FA">
					<CFSET FAV = gettoken("#filterattributevalue#",nCounter)>
					<CFSET thisfieldlinkurl = thisfieldlinkurl & "&#FA#=" & #FAV#>
				</CFLOOP>
			</CFIF>

			<A HREF="#thisfieldlinkurl#">#htmleditformat(trim(value))#</A>
		<cfelse>
			#application.com.security.sanitiseHTML(trim(value))#
		</cfif>
		</CFOUTPUT>
		
		<cfif len(trim(value)) eq 0>
			<!---chuck in a hard space so Netscape can parse the table correctly --->
			&nbsp;
		</cfif>
		
		

		</TD>
	</CFIF>
</CFLOOP><!--- end inner loop for list of fields --->

</TR>


				
</cfloop>
