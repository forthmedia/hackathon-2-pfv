<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			entityLastUpdatedInfo.cfm
Author:				SWJ
Date started:		2003-08-24

Purpose:	To provide a tag that can be called to simply insert two rows in a
			standard format that show the created and last updated details for
			a given entity

Usage:

		<cf_entityLastUpdatedInfo
			created="#getOrgDetails.Created#"
			createdby="#getOrgDetails.CreatedBy#"
			lastUpdated="#getOrgDetails.LastUpdated#"
			lastUpdatedby="#getOrgDetails.LastUpdatedBy#">


Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2016-01-22 SB Removed Tables and added |

Possible enhancements:


 --->

<cf_translate>

<cfparam name="createdByName" default="Unknown">
<cfparam name="updatedByName" default="Unknown">


<!--- show the details of who created and updated the record last --->
<cfscript>
	if (len(attributes.createdBy) gt 0 and isNumeric(attributes.createdBy)) {
	userInfo = application.com.commonQueries.getUserGroupDetails(attributes.createdBy);
	createdByName = userInfo.userGroupName;
	}
</cfscript>
<cfoutput>
	<div id="entityLastUpdated" class="row">
		<div class="col-xs-12 col-sm-12 col-md-3">
			<label>Phr_Sys_CreatedBy</label> #htmleditformat(createdByName)# - #dateFormat(attributes.created,"dd-mmm-yyyy")# #timeFormat(attributes.created,"HH:MM")#

			<cfscript>
			if (len(attributes.lastUpdatedBy) gt 0 and isNumeric(attributes.lastUpdatedBy)) {
			userInfo = application.com.commonQueries.getUserGroupDetails(attributes.lastUpdatedBy);
			updatedByName = userInfo.userGroupName;
			}
			</cfscript>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3">
			<label>Phr_Sys_LastUpdatedBy</label>
			#htmleditformat(updatedByName)# - #dateFormat(attributes.lastupdated,"dd-mmm-yyyy")# #timeFormat(attributes.lastupdated,"HH:MM")#
		</div>
	</div>
</cfoutput>

</cf_translate>