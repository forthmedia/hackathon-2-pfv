<!--- ©Relayware. All Rights Reserved 2014 --->
<cfsilent>
<!---
TAG NAME:			CF_RelayRecordManager (based on CF_SQLWIZARD by Mark Murphy)
VERSION:			1.2
DATE:				2000-25-09
EMAIL:				markoos@hotmail.com
WEBSITE:			www.fusionresource.com (Coming October 2000)
CF VERSION:			4.51+
DATABASE:			SQL Server 2000
DESCRIPTION:		CF_SQLWizard is a full select, add, edit, and delete interface to any MSSQL table.  It supports
					ALL nonbinary* SQL 2000 data types, fills in form fields with column defaults, and validates all user input before db submission.
					It uses modified code from another tag I wrote, CF_DHTMLQUERY, to provide a client-side resortable dhtml table if IE4+.
					*binary support coming in a future release

MINIMUM FORMAT: 	<CF_SQLWIZARD DATASOURCE="" TABLE="">

REQUIRED FIELDS
---------------
datasource:		SQL Server ODBC data source
table:			table in the default db for that odbc datasource

OPTIONAL FIELDS (see cfparam's below)
---------------
FIELD NAME				DEFAULT			DESCRIPTION
delete_confirm			"Yes"			Show an "are you sure?" javascript dialogue before deleting record.
allow_add				"Yes"			Allow the user to add a new record.
allow_edit				"Yes"			Allow the user to edit records.
allow_delete			"Yes"			Allow the user to delete records.
show_pk					"Yes"			Shows which field(s) are part of the primary key on the add/edit tables.
show_required			"Yes"			Shows which field(s) are required in the add/edit tables.
show_datatype			"Yes"			Shows the details of the datatype next to the form fields on the add/edit tables.
query					N/A				Specify a query other than SELECT * FROM #attributes.table#
(table formatting)						Table & font formatting attributes -- self explanatory.

ALL ATTRIBUTES (with default values):
--------------------------------------
<CF_SQLWIZARD DATASOURCE="" TABLE="" delete_confirm="yes" allow_select="yes" allow_add="yes" allow_edit="yes"
	allow_delete="yes" show_pk="yes" show_required="yes" show_datatype="yes" query="">


COMMENTS / KNOWN ISSUES:
-------------------
-The ODBC datasource/cold fusion service must have adequate permissions to the Information Schema views and the
	database table you want to process.
-This tag doesn't work if the table or column names have spaces in them.  This is bad practice anyway, you should
	use underscores in between words.
-The table specified must have a primary key.
-This tag doesn't support custom data types.
-This tag doesn't support rules, check constraints, table constraints or foreign keys.  It is meant for a single table.
	It will still work
 unless the update violates one of these conditions.  Use at your own risk when there are conditions on the table.
-The metadata query is cached to reduce database load.  To change the table and reload the page, you have to restart CF Server.


Version History:
----------------
1.0							Initial Release
1.1							Fixed a bug that made text default values show up wrapped in single quotes.
1.2		29-Sep-2005	GCC		Added code to remove Carriage returns from content passed to WDDX
1.3		2006-06-06 NJH		(P_FNL017) Altered code to display form elements with standard wrapper tags.
1.4 	2007/05/21 GCC 		Added the ability to specify a file of params to enable specific columns in specific tables to be uneditable.
		2008-07-24	NYF		Bug 525: was throwing up error where nullable boolean columns contained NULL.  Fixed using try-catch.  Also changed cfscript around Display to display booleans as No, Yes or Not Set
							525b: fixing 525 revealed a problem with trying to add entries to tables with an Allow Null boolean column
								- the Not Set option returns a value of 'on' which it then trys to insert.  Fixed.
							525c: system throws an error on Edit & Delete when a table doesn't have a primary key - being:  pkpacket
								- have fixed by setting allow_edit & allow_delete to no when a primary key doesn't exist
		2009/02/27	NJH		Bug Fix Issue 1906 All Sites - when editing a record with a date, the date defaulted to now() rather than the value. This now defaults to value.
		2009/02/27	NJH		Bug Fix Issue 1883 Demo - Records weren't being deleted as there was a cfexit in the code. I commented that out
							and also tied up some code by putting a cftry around the query so that we can give nice, friendly errors when they occur. I've catered for someone
							trying to delete a record where the record in question is a foreign key to another record in another table.
		2009/04/24	NJH		Bug Fix All Sites Issue 2079 - add support for nvarchar or ntext
		2009/06/22	NJH		Bug Fix Issue Lexmark Support 2250 - SQL server 2005 puts double parenthesis around a default value. Added regular expression to remove all parenthesis.
		2009/07/13	WAB		Added support for running a custom file after the form has been processed.  File with name of form   \templates\relayRecordManager_#tableName#.cfm
		2012-11-26 	WAB 	CASE 432242 Deal with defaults on Varchar fields
		2014-08-21	NJH		Deal with CF server validation problem when db field is called 'required'. This causes a problem was an field_required field gets generated. So, if
							we have a field called 'required', we rename it to 'fieldRequired'. Added an extra column to the metaData query called actual column name which we use
							when querying/writing to the DB. But otherwise use the column_name value as usual.
		2014-09-10	RPW		Add Lead Screen on Portal
		2014-13-10	SB		Removed font and font size tags. These can be styled using css and not html font elements.
		2015-09-23	ACPK	CORE-1003 Include TD elements in table regardless of whether edit/delete allowed
		2015-11-25  SB		Bootstrap
		2016-06-27	WAB		PROD2016-1334 callRelayRecordManager security.  Table must be encrypted as must the values in pkpacket
		2017-01-13	WAB/ESZ PROD2016-2649 Added support for 'systemFields' (lastupdated/created/lastupdatedby/createdby etc) (they get updated correctly and there is a readonly section at bottom of edit page)
							Took opportunity to delete some bits of dead code
		2017-01-24	RP		PROD2016-2949 enhancement for foreign key columns to show them as dropdown

--->
<!---required data--->
	<cfparam name="attributes.datasource">
	<cfparam name="attributes.table">

<!---important preferences--->
	<cfparam name="attributes.delete_confirm" default="yes">
	<cfparam name="attributes.allow_add" default="yes">
	<cfparam name="attributes.allow_edit" default="yes">
	<cfparam name="attributes.allow_delete" default="yes">
	<cfparam name="attributes.show_pk" default="yes">
	<cfparam name="attributes.show_required" default="yes">
	<cfparam name="attributes.show_datatype" default="yes">
	<cfparam name="attributes.fkstruct" default="#StructNew()#">

<!---table attributes--->
	<cfparam name="attributes.border" default="1">
	<cfparam name="attributes.numberOfColumns" type="numeric" default="5">
	<cfparam name="attributes.width" default="500">
	<cfparam name="attributes.cellspacing" default="0">
	<cfparam name="attributes.cellpadding" default="2">
	<cfparam name="attributes.bordercolor" default="silver">
	<cfparam name="attributes.font" default="arial,helvetica">
	<cfparam name="attributes.font_color" default="black">
	<cfparam name="attributes.font_size" default="-1">
	<cfparam name="attributes.onDelete" default="">

<!--- required for forms --->
	<cfparam name="request.relayFormDisplayStyle" default="HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">

<!--- queries used to produce radio button values --->
	<cf_querySim>
        getStati
        display,value
        Phr_Sys_Yes|1
        Phr_Sys_No|0
	</cf_querySim>

	<cf_querySim>
        getStatiWithNull
        display,value
        Phr_Sys_Yes|1
        Phr_Sys_No|0
        Not Set|NULL
	</cf_querySim>

<!--- any of the above variables can be over ridden in the INI file --->
	<cfinclude template="/templates/RelayRecordManagerINI.cfm">

	<cfquery name="get_uneditableFields" dbtype="query">
select ColumnName from uneditableFields
where lower(tablename) = '#attributes.table#'
</cfquery>

<!------------------------------------------------------------------------------------------------------------------------------------>
<!-------------------------------------------Query metadata, form attarray------------------------------------------------------------>
<!------------------------------------------------------------------------------------------------------------------------------------>

<!---get the attribute metadata for the requested table--->
	<cfquery name="get_metadata" datasource="#attributes.datasource#">
	-- 1
	SELECT case when INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME = 'required' then 'fieldRequired' else INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME end as column_name,
		INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME as actual_column_name,
		INFORMATION_SCHEMA.COLUMNS.DATA_TYPE,
	    case when INFORMATION_SCHEMA.COLUMNS.CHARACTER_MAXIMUM_LENGTH = -1 then 4000 else  INFORMATION_SCHEMA.COLUMNS.CHARACTER_MAXIMUM_LENGTH end as CHARACTER_MAXIMUM_LENGTH,
	    INFORMATION_SCHEMA.COLUMNS.IS_NULLABLE,
	    INFORMATION_SCHEMA.COLUMNS.COLUMN_DEFAULT,
	    INFORMATION_SCHEMA.COLUMNS.DATETIME_PRECISION,
	    INFORMATION_SCHEMA.COLUMNS.NUMERIC_PRECISION,
	    INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_TYPE,
	    INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_NAME,
	    IS_IDENTITY = COLUMNPROPERTY(OBJECT_ID('#attributes.table#'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsIdentity'),
		IS_COMPUTED = COLUMNPROPERTY(OBJECT_ID('#attributes.table#'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsComputed'),
		IS_GUID = COLUMNPROPERTY(OBJECT_ID('#attributes.table#'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsRowGuidCol')
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ON
	    INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_CATALOG = INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_CATALOG
	   	AND INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_SCHEMA = INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_SCHEMA
	    AND INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_NAME = INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_NAME

	RIGHT OUTER JOIN INFORMATION_SCHEMA.COLUMNS ON
	   	INFORMATION_SCHEMA.KEY_COLUMN_USAGE.TABLE_NAME = INFORMATION_SCHEMA.COLUMNS.TABLE_NAME
		AND INFORMATION_SCHEMA.KEY_COLUMN_USAGE.COLUMN_NAME = INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME
		AND INFORMATION_SCHEMA.KEY_COLUMN_USAGE.TABLE_CATALOG = INFORMATION_SCHEMA.COLUMNS.TABLE_CATALOG
	WHERE (INFORMATION_SCHEMA.COLUMNS.TABLE_NAME =  <cf_queryparam value="#attributes.table#" CFSQLTYPE="CF_SQL_VARCHAR" > )
    ORDER BY INFORMATION_SCHEMA.COLUMNS.ORDINAL_POSITION
	</cfquery>

	<!---errorcheck empty result or some other issue that prevented gathering table metadata--->
	<cfoutput>
        <table class="table table-hover table-striped">
            <tr>
                <th>Record Manager Issue</th>
            </tr>
        <tr>
        <td><cfif not get_metadata.recordcount>
            The table #htmleditformat(attributes.table)# is not in the database.
		<cfelseif not listcontainsnocase(valuelist(get_metadata.constraint_type), "primary key")>
            Table #htmleditformat(attributes.table)# doesn't have a primary key.

	</cfif>
        </td>
        </tr>
        </table>
	</cfoutput>

	<!---initialize attributes array that is used throughout--->
	<cfset attarray = arraynew(1)>
	
	<!--- 2017-01-13	WAB PROD2016-2649 System Fields
			Variables for dealing with SystemFields.  Has been designed so can handle not having all of the fields on a table--->	
	<cfset systemFieldUpdateValues = {createdBy = request.relayCurrentUser.usergroupid, createdByPerson = request.relayCurrentUser.personid,created = "getdate()",lastupdatedBy = request.relayCurrentUser.usergroupid, lastupdatedByPerson = request.relayCurrentUser.personid,lastupdated = "getdate()"}>
	<cfset systemFields = structKeyList (systemFieldUpdateValues)>
	<cfset columnList = valueList (get_metadata.column_Name)>
	<cfset hasSystemFields = false>

<!---2008-07-24 NYF 525c - added -> --->
	<cfset pk_exists="no">
<!--- <- 525c --->

	<cfloop query="get_metadata">
		<cfscript>
		if (not listFindNoCase (systemFields,COLUMN_NAME) ) {
			//setup a structure for each column
			tmpStruct = structnew();
			tmpStruct.COLUMN_NAME = COLUMN_NAME;
			tmpStruct.ACTUAL_COLUMN_NAME = ACTUAL_COLUMN_NAME;
			tmpStruct.CHARACTER_MAXIMUM_LENGTH = CHARACTER_MAXIMUM_LENGTH;
			// WAB 2012-11-26 CASE 432242 regExp removes the brackets and optionally the single quotes from the defaults so (xxx) and ('xxx') are converted to xxx
			// Previously there was code to do the brackets further down the file, but moved it all up here
			tmpStruct.COLUMN_DEFAULT = jsstringformat(rereplace(COLUMN_DEFAULT,"\A\(['\(]?(.*?)['\)]?\)\Z", "\1"));
			tmpStruct.CONSTRAINT_TYPE = CONSTRAINT_TYPE;
			if (CONSTRAINT_TYPE eq 'PRIMARY KEY') {
				pk_exists="yes";
			}
			// Change COLUMN_NAME to display column name from corresponding referenced column
			if (CONSTRAINT_TYPE eq 'FOREIGN KEY') {
				qry = new Query();
				qry.setDatasource("#attributes.datasource#");
				qry.setSQL("select dbo.getforeignKeyForTableColumn('#attributes.table#','#tmpStruct.actual_column_name#') as target");
				getTarget = qry.execute();
				entityType = application.com.relayEntity.getEntityType('#ListFirst(getTarget.getResult().target,".")#');
				//tmpStruct.COLUMN_NAME = entityType.NameExpression;
			}
			tmpStruct.CONSTRAINT_NAME = CONSTRAINT_NAME;
			tmpStruct.DATA_TYPE = DATA_TYPE;
			tmpStruct.DATETIME_PRECISION = DATETIME_PRECISION;
			tmpStruct.IS_COMPUTED = IS_COMPUTED;
			tmpStruct.IS_GUID = IS_GUID;
			tmpStruct.IS_IDENTITY = IS_IDENTITY;
			tmpStruct.IS_NULLABLE = trim(IS_NULLABLE);
			tmpStruct.NUMERIC_PRECISION = NUMERIC_PRECISION;
			arrayAppend (attarray, tmpStruct);
		} else {
			hasSystemFields = true;
		}
		</cfscript>
	</cfloop>

	<!---2008-07-24 NYF 525c - added -> --->
	<cfif pk_exists eq "no">
		<cfset attributes.allow_delete="no">
		<cfset attributes.allow_edit="no">
	</cfif>

	<!--- WAB 2016-06-27
			Table parameter and value(s) pkpacket.
			Table parameter uses standard code
			PKpacket more complicated so have had to hand craft some code.
			Not ideal because using some functions which I would prefer stayed 'private'
	--->

	<cf_checkFieldEncryption fieldnames="table">

	<cfset checkPKPacket = false>
	<cfif structKeyExists (form,"pkpacket")>
		<cfset pkScope = form>
		<cfset checkPKPacket = true>
		<cfelseif structKeyExists (url, "pkpacket")>
		<cfset pkScope = url>
		<cfset checkPKPacket = true>
	</cfif>

	<cfif checkPKPacket>
		<!---decode the pk values passed in--->
		<cfwddx action="wddx2cfml" input="#pkScope.pkpacket#" output="pkstruct">
		<cfscript>
			for(column in pkstruct) {
				value = pkstruct[column];
				if (application.com.security.isValueEncrypted (value)) {
					pkstruct[column] = application.com.security.decryptVariableValue( name = column, value = value);
				} else {
					writeoutput ("Invalid Parameter")	;
					abort;
				}
			}

		</cfscript>
	</cfif>



</cfsilent>

<cfsetting enablecfoutputonly="yes">
<cfif isdefined("form.update_type")>
	<cfswitch expression="#form.update_type#">
		<cfcase value="add">
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<!-----------------------------------------------------Add record form handler-------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<cfif attributes.allow_add>
				<cfset errors = "">
				<!---loop through all the non-identity, non-computed, non-identifier columns--->
				<cfloop from="1" to="#arraylen(attarray)#" index="column">
					<cfif (not attarray[column].is_identity) AND (not attarray[column].is_computed) AND (not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type))>
						<cfset value = evaluate("form.field_" & attarray[column].column_name)>
						<!---see if it was required but left blank--->
						<cfif (not comparenocase(attarray[column].is_nullable, "NO")) AND (not len(value))>
							<cfset errors = errors & "<LI>You didn't enter a value for the required field <b>'#attarray[column].column_name#'</b>.<BR><BR>">
						</cfif>

						<cfswitch expression="#attarray[column].data_type#">
							<!---2008-07-24 NYF Bug 525 - removed 'bit' from case options: --->
							<cfcase value="int,smallint,tinyint,decimal,numeric,float,real,money,smallmoney" delimiters=",">
								<!---see if it's not numeric--->
								<cfif (len(trim(value))) AND (not isnumeric(value))>
									<cfset errors = errors & "<LI>The value <b>'#evaluate("form.field_" & attarray[column].column_name)#'</b> you entered for field <b>'#attarray[column].column_name#'</b> needs to be numeric.<BR><BR>">
								</cfif>
							</cfcase>
							<cfcase value="char,varchar,nchar,nvarchar" delimiters=",">
								<!---see if the length is too long--->
								<cfif (comparenocase(attarray[column].data_type, "text")) AND (len(value) GT attarray[column].character_maximum_length)>
									<cfset errors = errors & "<LI>You entered a value for the field <b>'#attarray[column].column_name#'</b> that was longer than the maximum #attarray[column].character_maximum_length# characters.<BR><BR>">
								</cfif>
							</cfcase>
							<cfcase value="datetime,smalldatetime" delimiters=",">
								<!---see if it's not a date--->
								<cfif len(trim(value)) AND not isdate(value)>
									<cfset errors = errors & "<LI>The value <b>'#value#'</b> you entered for field <b>'#attarray[column].column_name#'</b> needs to be a valid date/time.<BR><BR>">
								</cfif>
							</cfcase>
						</cfswitch>
					</cfif>
				</cfloop>

				<!---halt if there are any errors--->
				<cfif len(errors)>
					<cfoutput><p>There was a problem with your form data...</p><p>#htmleditformat(errors)# Please click your browser's back button and try again.</p></cfoutput>
					<cfexit>
				</cfif>

				<!---construct INSERT sql statement--->
				<cfscript>
					//start querystring
					insertValue=false;
					querystring1 = "INSERT INTO " & attributes.table & "(";
					querystring2 = " VALUES(";

					//loop through columns, constructing query
					comma = "";
					for(column=1; column LTE arraylen(attarray); column = column + 1) {
						if((not attarray[column].is_identity) AND (not attarray[column].is_computed) AND (not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type)) AND (len(evaluate("form.field_" & attarray[column].column_name)))) {

							//2008-07-24 NYF Bug 525 - added if for: if not bit, or if bit and not of value 1 or 2, then don't incude - which should always put in as NULL
							if (not (attarray[column].data_type eq 'bit') or (attarray[column].data_type eq 'bit' and ((evaluate("form.field_" & attarray[column].column_name) eq 1) or (evaluate("form.field_" & attarray[column].column_name) eq 0)))) {
								//needs to be added to the attributes list
								value = evaluate("form.field_" & attarray[column].column_name);
								insertValue=true;
								querystring1 &= comma & attarray[column].actual_column_name ;
								if(isnumeric(value)) {
									querystring2 &= comma & application.com.security.queryParam (value = value, cfsqltype="cf_sql_numeric") ;
								} else {
									if(attarray[column].data_type eq 'datetime'){
										querystring2 &=  comma & application.com.security.queryParam (value = value, cfsqltype="cf_sql_timestamp") ; 
									}else{
										querystring2 &= comma & application.com.security.queryParam (value = value, cfsqltype="cf_sql_varchar");
									}
								}
							}
						comma = ",";
						}
					}
					
					// 2017-01-13	WAB PROD2016-2649 Add Support for System Fields					
					if (hasSystemFields) {
						// Add on values for all the systemFields which exist on the table
						for (col in systemFieldUpdateValues) {
							if (listFindNoCase(columnList,col)) {
								queryString1 &= "," & col;
								querystring2 &= "," & systemFieldUpdateValues[col];
							}
						}
					}

					// close
					querystring = querystring1 & ")" & querystring2 & ")";
					message = "Record added successfully.";

					if (not insertValue) {
						queryString="";
						message = "No values to add.";
					}
				</cfscript>
			<cfelse>
				<cfthrow detail="Sorry, this page doesn't allow additions to this table"><!---user tried to hack page--->
			</cfif>
		</cfcase>

		<cfcase value="edit_post">
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<!-------------------------------------------------EDIT RECORD FORM HANDLER----------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<cfif attributes.allow_edit>
				<cfif isdefined("form.edit")>

					<cfset errors = "">

					<!---loop through all the non-identity, non-computed, non-timestamp columns--->
					<cfloop from="1" to="#arraylen(attarray)#" index="column">
						<cfif (not attarray[column].is_identity) AND (not attarray[column].is_computed) AND (not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type))>

							<cfset value = evaluate("form.field_" & attarray[column].column_name)>

							<!---see if it was required but left blank--->
							<cfif (not comparenocase(attarray[column].is_nullable, "NO")) AND (not len(value))>
								<cfset errors = errors & "<LI>You didn't enter a value for the required field <b>'#attarray[column].column_name#'</b>.<BR><BR>">
							</cfif>

							<cfswitch expression="#attarray[column].data_type#">
								<cfcase value="int,smallint,tinyint,bit,decimal,numeric,float,real,money,smallmoney" delimiters=",">
									<!---see if it's not numeric--->
									<cfif (len(trim(value))) AND (not isnumeric(value)) and not attarray[column].is_Nullable>
										<cfset errors = errors & "<LI>The value <b>'#evaluate("form.field_" & attarray[column].column_name)#'</b> you entered for field <b>'#attarray[column].column_name#'</b> needs to be numeric.<BR><BR>">
									</cfif>
								</cfcase>
								<cfcase value="char,varchar" delimiters=",">
									<!---see if the length is too long--->
									<cfif (comparenocase(attarray[column].data_type, "text")) AND (len(value) GT attarray[column].character_maximum_length)>
										<cfset errors = errors & "<LI>You entered a value for the field <b>'#attarray[column].column_name#'</b> that was longer than the maximum #attarray[column].character_maximum_length# characters.<BR><BR>">
									</cfif>
								</cfcase>
								<cfcase value="datetime,smalldatetime" delimiters=",">
									<!---see if it's not a date--->
									<cfif len(trim(value)) AND not isdate(value)>
										<cfset errors = errors & "<LI>The value <b>'#value#'</b> you entered for field <b>'#attarray[column].column_name#'</b> needs to be a valid date/time.<BR><BR>">
									</cfif>
								</cfcase>
							</cfswitch>
						</cfif>
					</cfloop>

					<!---halt if there are any errors--->
					<cfif len(errors)>
						<cfoutput><p>There was a problem with your form data...</p><p>#htmleditformat(errors)# Please click your browser's back button and try again.</p></cfoutput>
						<cfexit>
					</cfif>

					<!---construct UPDATE sql statement--->
					<cfscript>
						//start querystring
						querystring = "UPDATE " & attributes.table & " SET ";

						//loop through columns, constructing query
						comma = "";
						for(column=1; column LTE arraylen(attarray); column = column + 1) {
							if((not attarray[column].is_identity) AND (not attarray[column].is_computed) AND not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type)  ) {  // SWJ removed AND (len(evaluate("form.field_" & attarray[column].column_name))) so that fields with black columns are included
								//needs to be added to the attributes list
								value = form["field_" & attarray[column].column_name];
								querystring &= comma & attarray[column].actual_column_name & "=";
								if(isnumeric(value)) {
									querystring &=  application.com.security.queryParam (value = value, cfsqltype="cf_sql_numeric");
								}	
								// Trend Nabu support Issue 541 - Cannot update money to be blank/null
								else
								{
									// NJH 2013/05/02 - a bit of a hack, I must admit.. but if a radio button as a display of not set and it is selected, then the value comes back as 'on'... can't work out why.. but this should is where the 'not sets' should be coming through anyways..
									if(len(value) eq 0 or listFindNoCase("int,smallint,tinyint,bit,decimal,numeric,float,real,money,smallmoney",attarray[column].data_type))
									{
										querystring &= "NULL";
									} else if (listFindNoCase('datetime,smalldatetime',attarray[column].data_type)) {
										querystring &= createODBCDateTime(value) ;
									}
									else
									{
										//NJH 2009/04/24 Bug Fix All Sites Issue 2079 - if type is nvarchar or ntext, append the N when updating
										//GCC 2014/07/17 - 441024 fix inability to save edits to records that have a datetime field - added escaping of timestamps as for add
										if(attarray[column].data_type eq 'datetime'){
											querystring &= application.com.security.queryParam (value = value, cfsqltype="cf_sql_timestamp") ;
										}else{
											querystring &= application.com.security.queryParam (value = value, cfsqltype="cf_sql_varchar");
										}

									}

								}
								comma = ",";
							}
						}

					// 2017-01-13	WAB PROD2016-2649 Add Support for System Fields	
					if (hasSystemFields) {
						// Add on values for all the lastupdated systemFields which exist on the table
						for (col in systemFieldUpdateValues) {
							if (listFindNoCase(columnList,col) and col contains "lastupdated") {
								queryString &= "," & col & " = " & systemFieldUpdateValues[col];
							}
						}
					}

						querystring = querystring & " WHERE ";
						for(column in pkstruct) {
							value = evaluate("pkstruct." & column);
							querystring = querystring & column & "=";
							if(isnumeric(value)) querystring = querystring & value & " AND ";
							else querystring = querystring & "'" & replace(value, "'", "''", "all") & "' AND ";
						}
						querystring = left(querystring, len(querystring)-5); //trim last " AND "

						message = "Record edited successfully.";
					</cfscript>

				</cfif>
			<cfelse>
				<cfthrow detail="Sorry, this page doesn't allow edits to this table"><!---user tried to hack page--->
			</cfif>
		</cfcase>

		<cfcase value="delete">
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<!----------------------------------------------DELETE RECORD FORM HANDLER------------------------------------------------------------>
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<cfif attributes.allow_delete>
				<!---
				    NJH 2009/02/27 Bug Fix Demo Issue 1883 - commented out the cfexit.. not sure why it was here.
				<cfexit> --->

				<!---11-30-2016 ESZ PROD2016-2649 Add Support for System Fields	--->
				<cfscript>
					// make where clause
					whereclause = " WHERE ";
					AND_ = "";
					for(column in pkstruct) {
						value = evaluate("pkstruct." & column);
						whereclause &= column & "=";
						if(isnumeric(value)) {
							whereclause &= value;
						} else {
							whereclause &= "'" & replace(value, "'", "''", "all") ;		
						}
						whereclause &= AND_;
						AND_ = " AND ";							
					}

					querystring ='';
					if (hasSystemFields) {
						// Do an update of the system fields just before delete
						comma = "";
						for (col in systemFieldUpdateValues) {
							if (listFindNoCase(columnList,col) and col contains "lastupdated") {
								queryString &= comma & col & " = " & systemFieldUpdateValues[col];
								comma = ",";
							}
						}
						if (len(queryString)) {
							querystring = "update #attributes.table# SET " & querystring;
							querystring &= whereclause & chr(10);
						}
					}

					querystring = querystring & " DELETE FROM " & attributes.table & #whereclause#;

				</cfscript>

			<cfelse>
				<cfthrow detail="Sorry, this page doesn't allow deletions to this table"><!---user tried to hack page--->
			</cfif>
		</cfcase>

		<cfcase value="edit">
			<!------------------------------------------------------------------------------------------------------------------------------------>
			<!-------------------------------------------------EDIT RECORD INTERFACE-------------------------------------------------------------->
			<!------------------------------------------------------------------------------------------------------------------------------------>

			<cfscript>
			aliasedColumnList = "T." & rereplace(valueList(get_metadata.actual_column_Name),",",",T.","ALL");
			querystring = "SELECT #aliasedColumnList#";
			if (listFindNoCase(valueList(get_metadata.column_Name),"fieldRequired")) {
				querystring = queryString & ",required as fieldRequired";
			}

			joins = "";
			additionalSelects = "";

			// 2017-01-13	WAB PROD2016-2649 Add Support for System Fields					
			if (hasSystemFields) {
				// Add createbyName and LastUpdatedByName to the query
				// Supports not having both lastUpdatedBy and lastUpdatedByPerson.  If both then lastUpdatedByPerson is king
				if (listFindNoCase (columnList, "lastupdatedByPerson")) {
					joins &= " left join vEntityName lastup on lastup.entityTypeID = 0 and lastup.entityID = t.lastUpdatedByPerson";
					additionalSelects &= ", isNull(lastup.name,'Unknown') as lastUpdatedByName";
				} else if (listFindNoCase (columnList, "lastupdatedBy")) {
					joins &= " left join usergroup lastup on lastup.userGroupID = t.lastUpdatedBy";
					additionalSelects &= ", isNull(lastup.name,'Unknown') as lastUpdatedByName";
				} else {
					additionalSelects &= ", 'Unknown' as lastUpdatedByName";
				}

				if (listFindNoCase (columnList, "createdByPerson")) {
					joins &= " left join vEntityName creat on creat.entityTypeID = 0 and creat.entityID = t.createdByPerson";
					additionalSelects &= ", isNull(creat.name,'Unknown') as createdByName";
				} else if (listFindNoCase (columnList, "createdBy")) {
					joins &= " left join usergroup creat on creat.userGroupID = t.createdBy";
					additionalSelects &= ", isNull(creat.name,'Unknown') as createdByName";
				} else {
					additionalSelects &= ", 'Unkown' as createdByName";
				}
			}

				querystring = queryString & additionalSelects & " FROM " & attributes.table & " AS T " & joins & " WHERE ";
				for(column in pkstruct) {
					value = evaluate("pkstruct." & column);
					querystring = querystring & column & "=";
					if(isnumeric(value)) querystring = querystring & value & " AND ";
					else querystring = querystring & "'" & replace(value, "'", "''", "all") & "' AND ";
				}
				querystring = left(querystring, len(querystring)-5); //trim last " AND "
			</cfscript>

			<cfquery datasource="#attributes.datasource#" name="getrecord">
				#preservesinglequotes(querystring)#
			</cfquery>
			
			
			<!---create a form that submits to itself--->
			<cfform action="#request.query_string_and_script_name#" method="POST">
				<!---Table that loops through the table attributes, making the proper form control for each--->
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="pkpacket" currentValue="#form.pkpacket#" label="">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="update_type" currentValue="edit_post" label="">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="Edit Record" spanCols="yes" cellClass="Submenu" valueAlign="left">

					<!---loop through each attribute--->
					<cfloop from="1" to="#arraylen(attarray)#" index="column">

						<cfset elemLabel = UCase(#attarray[column].actual_column_name#)>
						<cfif (not comparenocase(attarray[column].constraint_type, "primary key")) AND (attributes.show_pk)>
							<cfset elemLabel = #elemLabel# & "(pk)">
						</cfif>

						<!---show data type and length in noteText attribute--->
						<cfset elemNoteText = "">
						<cfif attributes.show_datatype>
							<cfset elemNoteText = "Datatype: #attarray[column].data_type#">
							<cfif len(attarray[column].character_maximum_length)>
								<cfswitch expression="#attarray[column].data_type#">
									<cfcase value="char,varchar,nchar,nvarchar,binary,varbinary" delimiters=",">
										<cfset elemNoteText = elemNoteText & "(#attarray[column].character_maximum_length#)">
									</cfcase>
								</cfswitch>
							</cfif>
						</cfif>

						<!----nullability--->
						<cfset elemRequired = "no">
						<cfif (attributes.show_required) AND (not comparenocase(trim(attarray[column].is_nullable), "NO"))>
							<cfset elemRequired = "yes">
						</cfif>

						<!---skip identity and computed columns--->
						<cfif (attarray[column].is_identity NEQ 1) AND (attarray[column].is_computed NEQ 1)  AND (not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type))						>
					

							<cfif get_uneditableFields.recordcount gt 0 and listfindnocase(valuelist(get_uneditableFields.ColumnName,","),attarray[column].column_name,",") gt 0>
								<cfset value = evaluate("getrecord." & attarray[column].column_name)>
								<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="">
								<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="#value#" label="#elemLabel#">
							<cfelse>
								<!--- Show data from column which is foreign key in the dropdown --->
								<cfif attarray[column].constraint_type eq "FOREIGN KEY">
									<cfset value = evaluate("getrecord." & attarray[column].actual_column_name)>
									<cfset elemLabel = UCase(#attarray[column].column_name#)>

									<cfquery name="getQry" datasource="#application.siteDataSource#">
										select dbo.getForeignKeySqlForTableColumn ('#attributes.table#','#attarray[column].actual_column_name#') as query
									</cfquery>
									<cfset query = getQry.query>
									<cfif StructKeyExists(attributes.fkstruct,attarray[column].actual_column_name) and StructKeyExists(attributes.fkstruct[attarray[column].actual_column_name],"WhereClause")>
									<cfset query = replaceNoCase(getQry.query, "where", "where #attributes.fkstruct[attarray[column].actual_column_name].WhereClause# and ")>
									</cfif>						
									<cfquery name="getRefs" datasource="#application.siteDataSource#">
										#preserveSingleQuotes(query)#
									</cfquery>
									<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#value#" fieldName="field_#attarray[column].column_name#" size="1" query="#getRefs#" display="displayValue" value="dataValue" label="#elemLabel#" disabled="No" >
								<cfelse>
								<cfset value = evaluate("getrecord." & attarray[column].column_name)>
								<cfswitch expression="#attarray[column].data_type#">
									<!---------------numeric datatypes---------------->

									<!---bit: radio buttons--->
									<cfcase value="bit">
										<cfif (not comparenocase(attarray[column].is_nullable, "YES")) and (not len(value))>
											<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" fieldName="field_#attarray[column].column_name#" label="#elemLabel#" query="#getStatiWithNull#" display="display" value="value" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
										<cfelse>
											<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(value GT 0,1,0)#" fieldName="field_#attarray[column].column_name#" label="#elemLabel#" query="#getStati#" display="display" value="value" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
										</cfif>
									</cfcase>

									<!--- integers: textbox length of the precision--->
									<cfcase value="int,smallint,tinyint" delimiters=",">
										<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="#elemLabel#" maxlength="#attarray[column].numeric_precision#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#" validate="integer">
									</cfcase>

									<!---other numbers: textbox length of the precision--->
									<cfcase value="float,smallmoney,money,numeric,decimal,real" delimiters=",">
										<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="#elemLabel#" maxlength="#attarray[column].numeric_precision#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
									</cfcase>

									<!----------------text datatypes--------------->

									<!---char, varchar, nchar, nvarchar: text box if fewer than 50, textarea otherwise--->
									<cfcase value="char,varchar,nchar,nvarchar" delimiters=",">
										<cfif attarray[column].character_maximum_length LTE 50>
											<cf_relayFormElementDisplay relayFormElementType="text" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="#elemLabel#" maxlength="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
										<cfelse>
											<cf_relayFormElementDisplay relayFormElementType="textArea" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="#elemLabel#" cols="40" rows="#attarray[column].character_maximum_length lte 250?5:10#" maxlength="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
										</cfif>
									</cfcase>
									<cfcase value="text,ntext" delimiters=",">
										<cf_relayFormElementDisplay relayFormElementType="textArea" fieldname="field_#attarray[column].column_name#" currentValue="#value#" label="#elemLabel#" cols="40" rows="10" maxChars="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
									</cfcase>

									<!---datetime--->
									<!--- NJH 2009/02/27 - Bug Fix Issue 1906 All Sites - changed the currentValue from using now() to value --->
									<cfcase value="datetime,smalldatetime" delimiters=",">
									<!--- SSS 2009/03/31 changed so that updated date is changed on each change still 1906--->
										<cfif #attarray[column].column_name# EQ "updated">
											<cfset datecurrentValue = request.requestTime>
										<cfelse>
											<cfset datecurrentValue = value>
										</cfif>
										<!--- GCC 2014/07/17 - 441024 fix inability to save edits to records that have a datetime field - added showTime="true" --->
										<cf_relayFormElementDisplay showTime="true" relayFormElementType="date" fieldname="field_#attarray[column].column_name#" currentValue="#dateformat(datecurrentValue, 'm/d/yyyy')# #timeformat(datecurrentValue, 'h:mm:ss tt')#" label="#elemLabel#" maxlength="50" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
									</cfcase>
								</cfswitch>
								</cfif>
							</cfif>
						</cfif><!---skip identity column--->
					</cfloop>
					<cfsetting enablecfoutputonly="no">

					<!--- 2017-01-13	WAB PROD2016-2649 Add Support for System Fields	--->
					<cfif hasSystemFields>
						<cfif listFindNoCase (columnList, "created")>
							<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="Phr_Sys_CreatedBy" currentValue="">
								<cfoutput>#htmleditformat(getRecord.createdByName)# - #dateFormat(getRecord.created,"dd-mmm-yyyy")# #timeFormat(getRecord.created,"HH:MM")# </cfoutput>
							</cf_relayFormElementDisplay>
						</cfif>
						<cfif listFindNoCase (columnList, "lastupdated")>
							<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="Phr_Sys_LastUpdatedBy" currentValue="">
								<cfoutput>#htmleditformat(getRecord.lastupdatedByName)# - #dateFormat(getRecord.lastupdated,"dd-mmm-yyyy")# #timeFormat(getRecord.lastupdated,"HH:MM")#</cfoutput>
							</cf_relayFormElementDisplay>
						</cfif>
					</cfif>
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="">
						<cf_relayFormElement relayFormElementType="submit" currentValue="Save" fieldname="edit" label="">
						<cf_relayFormElement relayFormElementType="submit" currentValue="Cancel" fieldname="cancel" label="">
					</cf_relayFormElementDisplay>
				</cf_relayFormDisplay>
			</cfform>
			<CFEXIT>
		</cfcase>
	</cfswitch>

<!--- NJH 2009/03/30 Bug Fix Demo 1883 -moved to below...
<cfif isdefined("form.cancel")>
    <cfoutput><font face="#attributes.font#" size="-1">Record update cancelled successfully.</font><BR></cfoutput>
<cfelseif isdefined("variables.message")>
    <cfoutput><font face="#attributes.font#" size="-1">#message#</cfoutput></font><BR>
</cfif>	 --->

	<cfsilent>
		<!--- NJH 2009/02/27 Bug Fix Demo Issue 1883 - records were getting deleted that had a foreign key still. Put the query inside a cftry
		    and tried to output a friendly error message.
		--->

		<cfset variables.type = 'success'>
		<cftry>
	<!----------------------------DATABASE UPDATE--------------->
	<cfif isdefined("variables.querystring") and variables.queryString neq "">
		<!---post the update to the database--->
		<cfquery datasource="#attributes.datasource#">
			#preservesinglequotes(querystring)#
		</cfquery>
	</cfif>

			<cfcatch type="database">
				<cfset variables.message = cfcatch.queryError>
				<cfset variables.type = 'error'>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Relay Record Manager",Severity="error",caughtError=cfcatch,WarningStructure=attributes)>
				<cfif findNoCase("DELETE statement conflicted with COLUMN REFERENCE constraint",cfcatch.queryError)>
					<cfset groupNames = {1="table"}>
					<cfset regExpResult = application.com.regexp.refindAllOccurrences("table ['|""](.*?)['|""]",cfcatch.queryError,groupNames)>
					<cfset variables.message = "This record cannot be deleted as an associated record exists in the #regExpResult[1].table# table.">
					<cfelseif refindNoCase("INSERT statement conflicted with .* FOREIGN KEY constraint",cfcatch.queryError)>
					<cfset groupNames = {1="table",2="column"}>
					<cfset regExpResult = application.com.regexp.refindAllOccurrences("table ['|""](.*?)['|""].*?column ['|""](.*?)['|""]",cfcatch.queryError,groupNames)>
					<cfset variables.message = "This record cannot be inserted as no associated record exists in the #regExpResult[1].table# table as referenced by column #regExpResult[1].column#.">
				</cfif>
			</cfcatch>
		</cftry>

	</cfsilent>

	<!---
    	WAB 2009/07/13
	    Trying idea for special code to  run after the update
    	Specifically implemented for fileTypes and change of the secured file setting, but generally applicable.
	    The primary key(s) of the table can be referenced as pkStruct[columnName]

	 --->
	<cfif fileExists ("#application.paths.relay#\templates\relayrecordmanager_#attributes.table#.cfm")>
		<cfinclude template = "\templates\relayrecordmanager_#attributes.table#.cfm">
	</cfif>



	<!--- NJH 2009/02/27 Bug Fix Demo Issue 1883 - I moved this down here so that I could add to the message if there was a problem with running the query. --->
	<cfif isdefined("form.cancel")>
		<cfoutput><p>#application.com.relayUI.message(message="Record update cancelled successfully.")#</p></cfoutput>
	<cfelseif isdefined("variables.message")>
		<cfoutput><p>#application.com.relayUI.message(message=message,type=variables.type)#</cfoutput></p><BR>
	</cfif>

</cfif><!---form was posted--->

<!------------------------------------------------------------------------------------------------------------------------------------>
<!---------------------------------------------QUERY RECORDSET & SORT JAVASCRIPT------------------------------------------------------>
<!------------------------------------------------------------------------------------------------------------------------------------>
<cfsilent>
    <table class="table table-hover table-striped">
    <TR>
    <TD ALIGN="LEFT" VALIGN="top" CLASS="Submenu">
	<CFOUTPUT>Table #htmleditformat(attributes.table)#</CFOUTPUT>
    </TD>
    <TD ALIGN="RIGHT" VALIGN="top" CLASS="Submenu">
	<CFOUTPUT>
            <A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
	</CFOUTPUT>
    </TD>
    </TR>
    </table>

	<!---construct query string--->
	<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->

	<!--- Get all foreign keys in the table --->
	<cfquery name="getfk" datasource="#attributes.datasource#">
		select referencedTable,parentColumn,referencedColumn
		from getForeignKeysForTable('#attributes.table#')
	</cfquery>

	<cfset columnsToSelect = "">
	<cfset joinClause = "">

	<!---2017/03/06 MRE PROD2016-3523 inserted table name on columnsToSelect to handle tables sharing column names --->
	<cfloop query="getfk">
		<cfset entityType = application.com.relayEntity.getEntityType('#getfk.referencedTable#')>
		<cfif entityType.NameExpression neq "">
			<cfset columnsToSelect &= ", #getfk.referencedTable#.#entityType.NameExpression#">
			<cfset joinClause &= " join #getfk.referencedTable# on #attributes.table#.#getfk.parentColumn# = #getfk.referencedTable#.#getfk.referencedColumn#">
		</cfif>
	</cfloop>

	<cfscript>
	if (IsDefined("attributes.queryParams") && IsArray(attributes.queryParams) && ArrayLen(attributes.queryParams)) {
		querystring = "SELECT #attributes.table#.* #columnsToSelect# FROM #attributes.table# #joinClause#" & " WHERE 1=1";

		for (variables.d=1;variables.d <= ArrayLen(attributes.queryParams);variables.d++) {

			querystring &= " AND " & attributes.queryParams[variables.d].fieldName & " = '" & attributes.queryParams[variables.d].fieldValue & "'";

		}

	} else {
		querystring = "SELECT #attributes.table#.* #columnsToSelect# FROM #attributes.table# #joinClause#";
	}	
	</cfscript>

	<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->
	<cfif not(isdefined("attributes.username") and isdefined("attributes.password"))>
		<cfquery name="getdata" datasource="#attributes.datasource#">
			#PreserveSingleQuotes(querystring)#
		</cfquery>
	</cfif>
	<!--- 2005/09/27 - GCC - Remove carriage returns before passing to WDDX
	Actual output is used for the update so there should be no issue with 'losing' them when editing
	but let's see...--->
	<CFOUTPUT query="getdata">
		<CFLOOP list="#getdata.columnList#" index="q">
			<cfset pointer = "getdata." & #q# & "[" & currentrow & "]">
			<cfset key = #q#>
			<cfset x = structupdate(getdata,key,replace(replace(evaluate(pointer),chr(10),"","ALL"),chr(13),"","ALL"))>
		</CFLOOP>
	</CFOUTPUT>

	<!--- Add column(s) to query which are the encrypted version of the PK--->
	<cfloop array="#attarray#" index="columnStruct">
		<cfif columnStruct.CONSTRAINT_TYPE is "Primary Key">
			<cfset application.com.security.encryptQueryColumn (query = getdata, columnName = columnStruct.column_name, updatecolumnName = columnStruct.column_name & "_enc")>
		</cfif>
	</cfloop>

	<cfscript>
	// work out the number of columns to show in the listing part
	if (arraylen(attarray) LTE attributes.numberOfColumns) numberOfColumnsToShow = arraylen(attarray);
	else numberOfColumnsToShow = attributes.numberOfColumns;
	//browser has to be IE 4.0+ to allow client-side resorting
	if ((find("Mozilla",cgi.user_agent)) AND (find("MSIE",cgi.user_agent)) AND (Trim(ListGetAt(ListGetAt(cgi.user_agent,2,"MSIE"),1,";")) GTE 4)) compatible = 1;
	else compatible = 0;

	fontstring = '<font face="' & attributes.font & '" color="' & attributes.font_color & '" size="' & attributes.font_size & '">'; //shaves a few milliseconds

	//render the header row of the table once so browser doesn't have to re-render every sort
	query_tabletop = '<TABLE class="table table-hover table-striped">';
	query_tabletop = query_tabletop & '<tr>';

	for(column=1; column LTE numberOfColumnsToShow; column = column + 1) {
		column_name = attarray[column].column_name;
		query_tabletop = query_tabletop & '<th nowrap>' & column_name;
		if(compatible) { //only offer sort buttons if it's a compatible browser
			query_tabletop = query_tabletop & '&nbsp;<CF_INPUT type="button" style="width:14px;font:normal 8pt verdana,arial,helvetica,sans-serif" value="&##8595;" onclick="resort(' & chr(39) & column_name & chr(39) & ',1)">';
   			 query_tabletop = query_tabletop & '&nbsp;<CF_INPUT type="button" style="width:14px;font:normal 8pt verdana,arial,helvetica,sans-serif" value="&##8593;" onclick="resort(' & chr(39) & column_name & chr(39) & ',0)">';
		}
	    query_tabletop = query_tabletop & '<BR></th>';
    }
    query_tabletop = query_tabletop & '<th>phr_edit</th><th>phr_delete</th></tr>';
	</cfscript>
</cfsilent>

<cfsetting enablecfoutputonly="no">
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<cfwddx action="cfml2js" input="#getdata#" toplevelvariable="getdata">
<cfwddx action="cfml2js" input="#attarray#" toplevelvariable="attarray">

//include reduced and whitespace-optimized wddx library that powers the DHMTL sort
function wddxSerializer_serializeValue(obj) {var bSuccess=true; var val; if (obj==null) {this.write("<null/>");} else if (typeof(val=obj.valueOf())=="string") {this.serializeString(val);} else if (typeof(val=obj.valueOf())=="number") {if ( typeof(obj.getTimezoneOffset)=="function" && typeof(obj.toGMTString)=="function") {this.write("<dateTime>" + (obj.getYear()<1000 ? 1900+obj.getYear() : obj.getYear()) + "-" + (obj.getMonth() + 1) + "-" + obj.getDate() + "T" + obj.getHours() + ":" + obj.getMinutes() + ":" + obj.getSeconds()); if (this.useTimezoneInfo) {this.write(this.timezoneString);} this.write("</dateTime>");} else {this.write("<number>" + val + "</number>");}} else if (typeof(val=obj.valueOf())=="boolean") {this.write("<boolean value='" + val + "'/>");} else if (typeof(obj)=="object") {if (typeof(obj.wddxSerialize)=="function") {bSuccess=obj.wddxSerialize(this);} else if ( typeof(obj.join)=="function" && typeof(obj.reverse)=="function" && typeof(obj.sort)=="function" && typeof(obj.length)=="number") {this.write("<array length='" + obj.length + "'>"); for (var i=0; bSuccess && i<obj.length; ++i) {bSuccess=this.serializeValue(obj[i]);} this.write("</array>");} else {if (typeof(obj.wddxSerializationType)=='string') {this.write('<struct type="'+ obj.wddxSerializationType +'">')} else {this.write("<struct>");} for (var prop in obj) {if (prop!='wddxSerializationType') {bSuccess=this.serializeVariable(prop, obj[prop]); if (! bSuccess) {break;}}} this.write("</struct>");}} else {bSuccess=false;} return bSuccess;} function wddxSerializer_serializeAttr(s) {for (var i=0; i<s.length; ++i) {this.write(this.at[s.charAt(i)]);}}
function wddxSerializer_serializeAttrOld(s) {this.write(s);}
function wddxSerializer_serializeString(s) {this.write("<string>"); for (var i=0; i<s.length; ++i) {this.write(this.et[s.charAt(i)]);} this.write("</string>");}
function wddxSerializer_serializeStringOld(s) {this.write("<string><![CDATA["); pos=s.indexOf("]]>"); if (pos!=-1) {startPos=0; while (pos!=-1) {this.write(s.substring(startPos, pos) + "]]>]]&gt;<![CDATA["); startPos=pos + 3; if (startPos<s.length) {pos=s.indexOf("]]>", startPos);} else {pos=-1;}} this.write(s.substring(startPos, s.length));} else {this.write(s);} this.write("]]></string>");}
function wddxSerializer_serializeVariable(name, obj) {var bSuccess=true; if (typeof(obj)!="function") {this.write("<var name='"); this.preserveVarCase ? this.serializeAttr(name) : this.serializeAttr(name.toLowerCase()); this.write("'>"); bSuccess=this.serializeValue(obj); this.write("</var>");} return bSuccess;}
function wddxSerializer_write(str) {this.wddxPacket +=str;}
function wddxSerializer_serialize(rootObj) {this.wddxPacket=""; this.write("<wddxPacket version='1.0'><header/><data>"); var bSuccess=this.serializeValue(rootObj); this.write("</data></wddxPacket>"); if (bSuccess) {return this.wddxPacket;} else {return null;}}
function WddxSerializer() {if (navigator.appVersion!="" && navigator.appVersion.indexOf("MSIE 3.")==-1) {var et=new Array(); var n2c=new Array(); var c2n=new Array(); var at=new Array(); for (var i=0; i<256; ++i) {var d1=Math.floor(i/64); var d2=Math.floor((i%64)/8); var d3=i%8; var c=eval("\"\\" + d1.toString(10) + d2.toString(10) + d3.toString(10) + "\""); n2c[i]=c; c2n[c]=i; if (i<32 && i!=9 && i!=10 && i!=13) {var hex=i.toString(16); if (hex.length==1) {hex="0" + hex;} et[n2c[i]]="<char code='" + hex + "'/>"; at[n2c[i]]="";} else if (i<128) {et[n2c[i]]=n2c[i]; at[n2c[i]]=n2c[i];} else {et[n2c[i]]="&#x" + i.toString(16) + ";"; at[n2c[i]]="&#x" + i.toString(16) + ";";}} et["<"]="&lt;"; et[">"]="&gt;"; et["&"]="&amp;"; at["<"]="&lt;"; at[">"]="&gt;"; at["&"]="&amp;"; at["'"]="&apos;"; at["\""]="&quot;"; this.n2c=n2c; this.c2n=c2n; this.et=et; this.at=at; this.serializeString=wddxSerializer_serializeString; this.serializeAttr=wddxSerializer_serializeAttr;} else {this.serializeString=wddxSerializer_serializeStringOld; this.serializeAttr=wddxSerializer_serializeAttrOld;} var tzOffset=(new Date()).getTimezoneOffset(); if (tzOffset >=0) {this.timezoneString='-';} else {this.timezoneString='+';} this.timezoneString +=Math.floor(Math.abs(tzOffset) / 60) + ":" + (Math.abs(tzOffset) % 60); this.preserveVarCase=false; this.useTimezoneInfo=true; this.serialize=wddxSerializer_serialize; this.serializeValue=wddxSerializer_serializeValue; this.serializeVariable=wddxSerializer_serializeVariable; this.write=wddxSerializer_write;}
function wddxRecordset_isColumn(name) {return (typeof(this[name])=="object" && name.indexOf("_private_")==-1);}
function wddxRecordset_getRowCount() {var nRowCount=0; for (var col in this) {if (this.isColumn(col)) {nRowCount=this[col].length; break;}} return nRowCount;}
function wddxRecordset_addColumn(name) {var nLen=this.getRowCount(); var colValue=new Array(nLen); for (var i=0; i<nLen; ++i) {colValue[i]=null;} this[this.preserveFieldCase ? name : name.toLowerCase()]=colValue;}
function wddxRecordset_addRows(n) {for (var col in this) {if (this.isColumn(col)) {var nLen=this[col].length; for (var i=nLen; i<nLen + n; ++i) {this[col][i]=null;}}}}
function wddxRecordset_getField(row, col) {return this[this.preserveFieldCase ? col : col.toLowerCase()][row];}
function wddxRecordset_setField(row, col, value) {this[this.preserveFieldCase ? col : col.toLowerCase()][row]=value;}
function wddxRecordset_wddxSerialize(serializer) {var colNamesList=""; var colNames=new Array(); var i=0; for (var col in this) {if (this.isColumn(col)) {colNames[i++]=col; if (colNamesList.length>0) {colNamesList +=",";} colNamesList +=col;}} var nRows=this.getRowCount(); serializer.write("<recordset rowCount='" + nRows + "' fieldNames='" + colNamesList + "'>"); var bSuccess=true; for (i=0; bSuccess && i<colNames.length; i++) {var name=colNames[i]; serializer.write("<field name='" + name + "'>"); for (var row=0; bSuccess && row<nRows; row++) {bSuccess=serializer.serializeValue(this[name][row]);} serializer.write("</field>");} serializer.write("</recordset>"); return bSuccess;}
function wddxRecordset_dump(escapeStrings) {var nRows=this.getRowCount(); var colNames=new Array(); var i=0; for (var col in this) {if (typeof(this[col])=="object") {colNames[i++]=col;}} var o="<table border=1><tr><td><b>RowNumber</b></td>"; for (i=0; i<colNames.length; ++i) {o +="<td><b>" + colNames[i] + "</b></td>";} o +="</tr>"; for (var row=0; row<nRows; ++row) {o +="<tr><td>" + row + "</td>"; for (i=0; i<colNames.length; ++i) {var elem=this.getField(row, colNames[i]); if (escapeStrings && typeof(elem)=="string") {var str=""; for (var j=0; j<elem.length; ++j) {var ch=elem.charAt(j); if (ch=='<') {str +="&lt;";} else if (ch=='>') {str +="&gt;";} else if (ch=='&') {str +="&amp;";} else {str +=ch;}} o +=("<td>" + str + "</td>");} else {o +=("<td>" + elem + "</td>");}} o +="</tr>";} o +="</table>"; return o;}
function WddxRecordset() {this.preserveFieldCase=false; if (typeof(wddxRecordsetExtensions)=="object") {for (var prop in wddxRecordsetExtensions) {this[prop]=wddxRecordsetExtensions[prop]}} this.getRowCount=wddxRecordset_getRowCount; this.addColumn=wddxRecordset_addColumn; this.addRows=wddxRecordset_addRows; this.isColumn=wddxRecordset_isColumn; <cfif compatible>this.sort = wddxRecordset_sort;</cfif> this.getField=wddxRecordset_getField; this.setField=wddxRecordset_setField; this.wddxSerialize=wddxRecordset_wddxSerialize; this.dump=wddxRecordset_dump; if (WddxRecordset.arguments.length>0) {if (typeof(val=WddxRecordset.arguments[0].valueOf())=="boolean") {this.preserveFieldCase=WddxRecordset.arguments[0];} else {var cols=WddxRecordset.arguments[0]; var nLen=0; if (WddxRecordset.arguments.length>1) {if (typeof(val=WddxRecordset.arguments[1].valueOf())=="boolean") {this.preserveFieldCase=WddxRecordset.arguments[1];} else {nLen=WddxRecordset.arguments[1]; if (WddxRecordset.arguments.length>2) {this.preserveFieldCase=WddxRecordset.arguments[2];}}} for (var i=0; i<cols.length; ++i) {var colValue=new Array(nLen); for (var j=0; j<nLen; ++j) {colValue[j]=null;} this[this.preserveFieldCase ? cols[i] : cols[i].toLowerCase()]=colValue;}}}}
function wddxBinary_wddxSerialize(serializer) {serializer.write( "<binary encoding='" + this.encoding + "'>" + this.data + "</binary>"); return true;}
function WddxBinary(data, encoding) {this.data=data!=null ? data : ""; this.encoding=encoding!=null ? encoding : "base64"; this.wddxSerialize=wddxBinary_wddxSerialize;}

<cfif compatible>
    var global_sort_column, global_sort_order;
    var allow_edit = <cfoutput>#iif(attributes.allow_edit, 1, 0)#</cfoutput>;
var allow_delete = <cfoutput>#iif(attributes.allow_delete, 1, 0)#</cfoutput>;

function resort(sort_column, sort_order) { //redraw the recordset div after resorting
    if(sort_column == global_sort_column && sort_order == global_sort_order) return false; //save processing if same button clicked twice
    var nRows = getdata.getRowCount();
    var divtext = '<cfoutput>#jsstringformat(query_tabletop)#</cfoutput>'; //table header
    getdata = getdata.sort(sort_column, sort_order); //resort recordset before redrawing

	for(row = 0; row < nRows; row++) {
    	divtext += '<tr>';
	    for (column in attarray) {
    	    data = getdata.getField(row, attarray[column].column_name);
        if(typeof(data.getTimezoneOffset) == "function" && typeof(data.toGMTString) == "function") { //date formatting routine
        	    //format date
	            var tempdata = "";
    	        tempdata += (data.getMonth()+1) + "/" + data.getDate() + "/" + (data.getYear()<1000 ? 1900+data.getYear() : data.getYear()) + " ";
        	    var temphours = data.getHours()<12 ? data.getHours() : data.getHours()-12;
            	tempdata += temphours ? temphours : "12";
	            tempdata += ":" + (data.getMinutes() ? data.getMinutes() : "00") + ":" + (data.getSeconds() ? data.getSeconds() : "00") + " ";
    	        tempdata += (data.getHours()<12 ? "AM" : "PM");
        	    data = tempdata;
	        }
    	    divtext += '<td>' + data + '&nbsp;</td>';
	    }
		if(allow_edit) divtext += '<TD><A HREF="##" onClick="process_form(' + row + ',\'edit\');"><IMG SRC="/images/MISC/iconEditPencil.gif" ALT="Edit" BORDER=0></A><!--- <input type="button" style="font:normal 8pt verdana,arial,helvetica,sans-serif" name="edit" value="Edit" onclick="process_form(' + row + ',\'edit\');"> ---><BR></TD>';
	    if(allow_delete) divtext += '<TD><A HREF="##" onClick="process_form(' + row + ',\'delete\');"><IMG SRC="/images/MISC/iconDeleteCross.gif" ALT="Expire Login" BORDER=0></A><!--- <input type="button" style="font:normal 8pt verdana,arial,helvetica,sans-serif" name="delete" value="Delete" onclick="process_form(' + row + ',\'delete\');"> ---><BR></TD>';
        divtext += '</tr>';
    }
	divtext += '</table>';
	global_sort_column = sort_column;
	global_sort_order = sort_order;
	document.all.recordsetDiv.innerHTML = divtext;
}

    ////////////////////////////////////////////
    //SORT EXTENSION OF JS WDDXRECORDSET OBJECT
    function wddxRecordset_sort(sort_column, sort_order) {
        //this function takes a wddx recordset and returns it resorted by selection sort
        //sort column is column name, sort order: 1 is ASC, 0 is DESC
        var nRows = this.getRowCount();	if(!nRows) return this; //account for the empty recordset
        var colarray = new Array(); var dataarray = new Array(); //arrays for the row number and the column data to minimize swapping
        var start_index = 0; var temp = ''; var currentval = '';

        for(row=0; row<=nRows-1; row++) { //populate the temp arrays
            colarray[row] = row;
            dataarray[row] = this.getField(row,sort_column);
            if(typeof(dataarray[row])=="string") dataarray[row] = dataarray[row].toLowerCase();
        }

        if(sort_order) { //ascending sort
            while(start_index < nRows-1) { //loop from the start index to the end n-1 iterations
                var min = dataarray[start_index];
                var min_index = start_index;
                for(row=start_index+1; row <= nRows-1; row++) {
                    currentval =  dataarray[row];
                    if(currentval < min) {
                        min = currentval; min_index = row;
                    }
                }
                if(start_index != min_index) { //swap
                    temp1 = dataarray[min_index]; temp2 = colarray[min_index];
                    dataarray[min_index] = dataarray[start_index]; colarray[min_index] = colarray[start_index];
                    dataarray[start_index] = temp1; colarray[start_index] = temp2;
                }
                start_index++;
            }
        }

        else { //descending sort
            while(start_index < nRows-1) { //loop from the start index to the end n-1 iterations
                var max = dataarray[start_index];
                var max_index = start_index;
                for(row=start_index+1; row <= nRows-1; row++) {
                    currentval =  dataarray[row];
                    if(currentval > max) {
                        max = currentval; max_index = row;
                    }
                }
                if(start_index != max_index) { //swap
                    temp1 = dataarray[max_index]; temp2 = colarray[max_index];
                    dataarray[max_index] = dataarray[start_index]; colarray[max_index] = colarray[start_index];
                    dataarray[start_index] = temp1; colarray[start_index] = temp2;
                }
                start_index++;
            }
        }

        //now rewrite original recordset and return
        var newset = new WddxRecordset;	newset.addRows(nRows); for(column in attarray) newset.addColumn(attarray[column].column_name);
        for(row=0; row<nRows; row++) for(column in attarray) newset.setField(row, attarray[column].column_name, this.getField(colarray[row], attarray[column].column_name));
        return newset;
    }
</cfif>

function process_form(row, type) {
    //construct the pkstruct to identify the record to edit or delete then submit the form
    // Note, uses the encrypted version of the PK
	<cfif attributes.delete_confirm>if(type=="edit" || type=="delete" && confirm("Are you sure you want to delete this record?")) {</cfif>
    pkstruct = new Object();
    for(col in attarray) { //find the primary keys and append to the pkarray
        if(attarray[col].constraint_type=='PRIMARY KEY') pkstruct[attarray[col].column_name] = getdata.getField(row, attarray[col].column_name + '_enc');
    }

    //serialize pkstruct, put into hidden form field, and submit the form
    wddxSerializer = new WddxSerializer();
    document.updateform.update_type.value = type;
    document.updateform.pkpacket.value = wddxSerializer.serialize(pkstruct);
    isOK = true;
	<cfif attributes.onDelete neq "">if (type == 'delete') {isOK = <cfoutput>#attributes.onDelete#</cfoutput>}</cfif>
    if (isOK) {
        document.updateform.submit();
    }
<cfif attributes.delete_confirm>}</cfif>
}
//-->
</SCRIPT>

<!---form for the edit and delete buttons--->
<cfform action="#request.query_string_and_script_name#" method="POST" name="updateform">
	<cf_relayFormElement relayFormElementType="hidden" fieldname="pkpacket" currentValue="" label="">
	<cf_relayFormElement relayFormElementType="hidden" fieldname="update_type" currentValue="" label="">

	<!---display the column headings in the proper order with underscores stripped--->
    <div ID="recordsetDiv">
	<cfoutput>#application.com.security.sanitiseHTML(query_tabletop)#</cfoutput>

	<!---loop through the rows--->
	<cfoutput query="getdata"><TR><cfset pkstruct = structnew()>
		<cfloop from="1" to="#numberOfColumnsToShow#" index="column"><!--- 2003-01-09 modified to just bring back first 5 columns --->
                <TD><cfsilent>
				<!---display the data or a blank in this cell, replacing special characters so it renders--->

				<!---special displays for special datatypes--->
				<cfscript>
					if(listfindnocase("datetime,smalldatetime", attarray[column].data_type)) {
						//change date and time to format specified in attributes
						if(len(trim(evaluate(attarray[column].column_name)))) displayval = dateformat(evaluate(attarray[column].column_name), "m/d/yyyy") & " " & timeformat(evaluate(attarray[column].column_name), "h:mm:ss tt");
						else displayval = "";
					}
					else {
						//2008-07-24 NYF Bug 525 - replaced "displayval = (evaluate(attarray[column].column_name));" with ->
						if(attarray[column].data_type eq "bit") { //2008-07-24 NYF Bug 525
							try {
								if(evaluate(attarray[column].column_name) eq 1) {
									displayval = "Yes";
								} else {
									displayval = "No";
								}
							} catch (any excpt) {
								displayval = "Not Set";
							}
						} else {
							displayval = (evaluate(attarray[column].column_name));
						}
						//<- Bug 525
					}
				</cfscript>
			</cfsilent>#htmleditformat(displayval)#&nbsp;<BR></TD>
		</cfloop>
		<!--- 2015-09-23	ACPK	CORE-1003 Include TD elements in table regardless of whether edit/delete allowed --->
        <TD><cfif attributes.allow_edit><A HREF="##" onClick="process_form(#evaluate(currentrow-1)#,'edit');return false;"><IMG SRC="/images/MISC/iconEditPencil.gif" ALT="Edit" BORDER=0></A><!--- <cf_relayFormElement relayFormElementType="button" fieldName="edit" currentValue="Edit" label="" onClick="process_form(#evaluate(currentrow-1)#,'edit');"> ---></cfif></TD>
        <TD><cfif attributes.allow_delete><A HREF="##" onClick="process_form(#evaluate(currentrow-1)#,'delete');return false;"><IMG SRC="/images/MISC/iconDeleteCross.gif" ALT="Expire Login" BORDER=0></A><!--- <cf_relayFormElement relayFormElementType="button" fieldName="delete" currentValue="Delete" label="" onClick="process_form(#evaluate(currentrow-1)#,'delete');"> ---></cfif></TD>
        </TR>
	</cfoutput>
        </TABLE></div>
</cfform>
<cfsilent>

<!------------------------------------------------------------------------------------------------------------------------------------>
<!------------------------------------------------Add Record Interface---------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------>
</cfsilent>
<cfif attributes.allow_add>

<!---Table that loops through the table attributes, making the proper form control for each--->
<!---create a form that submits to itself--->
	<cfform action="#cgi.script_name#?#request.query_string#" method="POST" noValidate="true">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="Add a New Record" spanCols="yes" cellClass="Submenu" valueAlign="left">
			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="update_type" currentValue="add" label="">

			<!---loop through each attribute--->
			<cfsetting enablecfoutputonly="yes">
			<cfloop from="1" to="#arraylen(attarray)#" index="column">
				<!---prepare the data for the loop--->
				<!--- GCC - 2005/09/20 - Altered to allow for JS script escaping at line 147 and added nvarchar check too --->
				<cfset default_value = attarray[column].column_default>
				<cfscript>
					//2014-09-10	RPW		Add Lead Screen on Portal
					if (IsDefined("attributes.queryParams") && IsArray(attributes.queryParams) && ArrayLen(attributes.queryParams)) {
						for (variables.d=1;variables.d <= ArrayLen(attributes.queryParams);variables.d++) {
							if (attributes.queryParams[variables.d].fieldName==attarray[column].column_name) {
								default_value = attributes.queryParams[variables.d].fieldValue;
							}
						}
					}
				</cfscript>

				<!--- setting the element labels --->
				<cfset elemLabel = UCase(#attarray[column].actual_column_name#)>
				<cfif (not comparenocase(attarray[column].constraint_type, "primary key")) AND (attributes.show_pk)>
					<cfset elemLabel = #elemLabel# & "(pk)">
				</cfif>

				<!---show data type and length in noteText attribute--->
				<cfset elemNoteText = "">
				<cfif attributes.show_datatype>
					<cfset elemNoteText = "Datatype: #attarray[column].data_type#">
					<cfif len(attarray[column].character_maximum_length)>
						<cfswitch expression="#attarray[column].data_type#">
							<cfcase value="char,varchar,nchar,nvarchar,binary,varbinary" delimiters=",">
								<cfset elemNoteText = elemNoteText & "(#attarray[column].character_maximum_length#)">
							</cfcase>
						</cfswitch>
					</cfif>
				</cfif>

				<!----nullability--->
				<cfset elemRequired = "no">
				<cfif (attributes.show_required) AND (not comparenocase(trim(attarray[column].is_nullable), "NO"))>
					<cfset elemRequired = "yes">
				</cfif>

				<!---skip identity, computed, and identifier columns--->
				<cfif (attarray[column].is_identity NEQ 1) AND (attarray[column].is_computed NEQ 1) AND (not listfindnocase("timestamp,uniqueidentifier,binary,varbinary,image", attarray[column].data_type))>

					<!--- Show data from column which is foreign key in the dropdown --->
					<cfif attarray[column].constraint_type eq "FOREIGN KEY">
						<cfset elemLabel = UCase(#attarray[column].column_name#)>
						<cfquery name="getQry" datasource="#application.siteDataSource#">
							select dbo.getForeignKeySqlForTableColumn ('#attributes.table#','#attarray[column].actual_column_name#') as query
						</cfquery>
						<cfif getQry.query neq "">
							<cfset query = getQry.query>
							<cfif StructKeyExists(attributes.fkstruct,attarray[column].actual_column_name) and StructKeyExists(attributes.fkstruct[attarray[column].actual_column_name],"WhereClause")>
								<cfset query = replaceNoCase(getQry.query, "where", "where #attributes.fkstruct[attarray[column].actual_column_name].WhereClause# and ")>
							</cfif>
							<cfquery name="getRefs" datasource="#application.siteDataSource#">
								#preserveSingleQuotes(query)#
							</cfquery>
							<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#default_value#" fieldName="field_#attarray[column].column_name#" size="1" query="#getRefs#" display="displayValue" value="dataValue" label="#elemLabel#" disabled="No" >
						</cfif>
					<cfelse>
					<!---switch/case for the type of attribute--->
					<cfswitch expression="#attarray[column].data_type#">

						<!---------------numeric datatypes---------------->
						<!---bit: radio buttons--->
						<cfcase value="bit">
							<cfif not comparenocase(attarray[column].is_nullable, "YES")>
								<cfif not len(attarray[column].column_default)>
									<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" fieldName="field_#attarray[column].column_name#" label="#elemLabel#" query="#getStatiWithNull#" display="display" value="value" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#" columns=2>
								<cfelse>
									<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(attarray[column].column_default GT 0,1,0)#" fieldName="field_#attarray[column].column_name#" label="#elemLabel#" query="#getStatiWithNull#" display="display" value="value" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#" columns=2>
								</cfif>
							<cfelse>
								<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(attarray[column].column_default GT 0,1,0)#" fieldName="field_#attarray[column].column_name#" label="#elemLabel#" query="#getStati#" display="display" value="value" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#" columns=2>
							</cfif>
						</cfcase>

						<!---integers: textbox length of the precision--->
						<cfcase value="int,smallint,tinyint" delimiters=",">
							<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="field_#attarray[column].column_name#" currentValue="#default_value#" label="#elemLabel#" maxlength="#attarray[column].numeric_precision#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#" validate="integer">
						</cfcase>

						<!---other numbers: textbox length of the precision--->
						<cfcase value="float,smallmoney,money,numeric,decimal,real" delimiters=",">
							<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="field_#attarray[column].column_name#" currentValue="#default_value#" label="#elemLabel#" maxlength="#attarray[column].numeric_precision#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
						</cfcase>

						<!----------------text datatypes--------------->

						<!---char, varchar, nchar, nvarchar: text box if fewer than 50, textarea otherwise--->
						<cfcase value="char,varchar,nchar,nvarchar" delimiters=",">
							<cfif attarray[column].character_maximum_length LTE 50>
								<cf_relayFormElementDisplay relayFormElementType="text" fieldname="field_#attarray[column].column_name#" currentValue="#default_value#" label="#elemLabel#" maxlength="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
							<cfelse>
								<cf_relayFormElementDisplay relayFormElementType="textArea" fieldname="field_#attarray[column].column_name#" currentValue="#default_value#" label="#elemLabel#" cols="40" rows="#attarray[column].character_maximum_length lte 250?5:10#" maxlength="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
							</cfif>
						</cfcase>
						<cfcase value="text,ntext" delimiters=",">
							<cf_relayFormElementDisplay relayFormElementType="textArea" fieldname="field_#attarray[column].column_name#" currentValue="#default_value#" label="#elemLabel#" cols="40" rows="10" maxChars="#attarray[column].character_maximum_length#" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
						</cfcase>

						<!---datetime--->
						<cfcase value="datetime,smalldatetime" delimiters=",">
							<cf_relayFormElementDisplay showTime="true" relayFormElementType="date" fieldname="field_#attarray[column].column_name#" currentValue="#dateformat(request.requestTime, 'm/d/yyyy')# #timeformat(request.requestTime, 'h:mm:ss tt')#" label="#elemLabel#" maxlength="50" noteText="#elemNoteText#" noteTextPosition="right" required="#elemRequired#">
						</cfcase>
					</cfswitch>
					</cfif>			
				</cfif><!---skip identity column--->
			</cfloop>
			<cf_relayFormElementDisplay relayFormElementType="submit" currentValue=" Add Record " fieldname="add_post" label="">
		</cf_relayFormDisplay>
	</cfform>
</cfif><!---allow add--->
<cfsetting enablecfoutputonly="no">
