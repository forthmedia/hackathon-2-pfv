<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayCrossTabReport.cfm	
Author:			SWJ
Date started:	2004-11-14
	
Description:	This provides a standard cross tab report when passed a query object.

Usage:			This is used in reports/crossTabLibrary.cfm

		The query should be built like this:
		<cfquery name="qReportData" datasource="#application.siteDataSource#">
			select timesheetYearMonth as reportRows, fullname as reportColumns, sum(hours) as reportUnits
				from projTimeSheet t
				inner join vPeople v ON v.personid = t.deliveredByPersonID
				where projectID in (#url.projectID#)
				group by timesheetYearMonth, fullname
		</cfquery>
		Note the aliases for reportRows, reportColumns and reportUnits
		
		<cf_relayCrossTabReport	reportQuery = "#qReportData#" title="Project Time by Person" hideFooter="true" numberFormat="decimal">

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
19-Apr-08	SWJ		Added the following new attributes: showDebug, reportTitle, hideFooter, numberFormat
					Reversed the reportCols and reportRows as they were the wrong way around
					Modified the css classes so they worked better

Possible enhancements:

 --->

<cfif not isQuery(attributes.reportQuery)>
	<p>The reportQuery attribute of RelayCrossTabReport custom tag must contain a valid query object.  
	Example syntax: <pre>&lt;cf_relayCrossTabReport	reportQuery = "#htmleditformat(qReportData)#" title="Project Time by Person" hideFooter="true"&gt;</pre>
	</p>
	<p>
	The CFQuery should look like this:
	<pre>&lt;cfquery name="qReportData" datasource="#htmleditformat(application.siteDataSource)#"&gt;
	select timesheetYearMonth as reportRows, fullname as reportColumns, sum(hours) as reportUnits
		from projTimeSheet t
		inner join vPeople v ON v.personid = t.deliveredByPersonID
		where projectID in (#htmleditformat(url.projectID)#)
		group by timesheetYearMonth, fullname
	&lt;/cfquery&gt;</pre>
	</br>Note the aliases for reportRows, reportColumns and reportUnits
	</p>
	<CF_ABORT>
</cfif>

<!--- <cfdump var="#attributes.reportQuery.ColumnList#"> --->

<cfif listFindNoCase(attributes.reportQuery.ColumnList,"reportColumns") eq 0>
	<p>You must call the column alias for the report columns reportColumns.</p>
	<CF_ABORT>
</cfif>

<cfif listFindNoCase(attributes.reportQuery.ColumnList,"reportRows") eq 0>
	<p>You must call the column alias for the report rows reportRows.</p>
	<CF_ABORT>
</cfif>

<cfif listFindNoCase(attributes.reportQuery.ColumnList,"reportUnits") eq 0>
	<p>You must call the column alias for the report units reportUnits.</p>
	<CF_ABORT>
</cfif>

<cfparam name="attributes.hideFooter" default="false">
<cfparam name="attributes.reportTitle" default="">
<cfparam name="attributes.numberFormat" type="string" default="">


<style>
td.reportColHeading {text-align:center;font-weight:bold}
td.reportTotal {text-align:center;font-weight:bold}
td.reportCell {text-align:center;}
div.reportClass {align:left;margin:20;}
</style> 

<cffunction name = "getnumberFormatMaskValue">
	<cfargument name="numberFormatMask" required="true" type="string">
	
	<cfswitch expression="#numberFormatMask#">
		<cfcase value="decimal">
			<cfreturn ",_-_.__">
		</cfcase>
		<cfdefaultcase>
			<cfreturn ",_-_">
		</cfdefaultcase>
	</cfswitch>	
	
</cffunction>

<cfquery name="getReportingCols" dbtype="query">
	select distinct reportColumns as reportingCols from attributes.reportQuery
</cfquery> 

<cfquery name="getReportingRows" dbtype="query">
	select distinct reportRows as reportingRows from attributes.reportQuery
</cfquery> 


<!--- build a structure that contains the row totals --->
<cfquery name="getReportingRowTotals" dbtype="query">
	select reportRows as reportingRow, sum(reportUnits) as rowTotal 
	from attributes.reportQuery
	group by reportRows
</cfquery> 

<cfset reportingRowTotalsStruct = structNew()>
<cfloop query="getReportingRowTotals">
	<cfif not structKeyExists( reportingRowTotalsStruct, "#reportingRow#" )>
		<cfset structInsert( reportingRowTotalsStruct, "#reportingRow#", rowTotal )>
	</cfif>
</cfloop>


<!--- build a structure that contains the row totals --->
<cfquery name="getReportingColTotals" dbtype="query">
	select reportColumns as reportingCol, sum(reportUnits) as colTotal 
	from attributes.reportQuery
	group by reportColumns
</cfquery> 

<!--- get grand total totals --->
<cfquery name="getGrandTotal" dbtype="query">
	select sum(reportUnits) as grandTotal 
	from attributes.reportQuery
</cfquery>

<cfset reportingColTotalsStruct = structNew()>
<cfloop query="getReportingColTotals">
	<cfif not structKeyExists( reportingColTotalsStruct, "#reportingCol#" )>
		<cfset structInsert( reportingColTotalsStruct, "#reportingCol#", colTotal )>
	</cfif>
</cfloop>


<!--- build a double keyed structure using a pipe delimeter to hold the units ---> 
<cfset reportingDataStruct = structNew()>
<cfloop query="attributes.reportQuery">
	<cfif not structKeyExists( reportingDataStruct, "#reportRows#|#reportColumns#" )>
		<cfset structInsert( reportingDataStruct, "#reportRows#|#reportColumns#", reportUnits )>
	</cfif>
</cfloop>

<!--- Show the structures if showDegug is true --->
<cfparam name="attributes.ShowDebug" default="false">
<cfif attributes.ShowDebug>
	<cfdump var="#attributes.reportQuery#" expand="false">
	<cfdump var="#getReportingRows#" expand="false">
	<cfdump var="#getReportingCols#" expand="false">
	<cfdump var="#getReportingRowTotals#" expand="false">
	<cfdump var="#getReportingColTotals#" expand="false">
	<cfdump var="#reportingRowTotalsStruct#" expand="false">
	<cfdump var="#reportingColTotalsStruct#" expand="false">
	<cfdump var="#reportingDataStruct#" expand="false">
</cfif>

<cfset numberFormatMaskValue = getnumberFormatMaskValue(numberFormatMask = attributes.numberFormat)>

<cfoutput>
<div class="reportClass">
<TABLE BORDER="1" CELLSPACING="1" CELLPADDING="3" CLASS="withBorder">
	<cfif len(attributes.reportTitle) gt 0>
		<!--- if attributes.reportTitle gt 0 show a title for the report --->
		<cfset totalReportCols = getReportingCols.recordCount + 2>
		<tr>
			<th colspan="#totalReportCols#">#htmleditformat(attributes.reportTitle)#</th>
		</tr>
	</cfif>
	
	<!--- Set up the column titles --->
	<tr>
		<td>&nbsp;</td>
		<cfif getReportingCols.recordCount gt 10><td class="reportColHeading">Total</th></cfif>
		<cfloop query="getReportingCols">
		       <td class="reportColHeading">#htmleditformat(getReportingCols.reportingCols)#</td>
		</cfloop>
		<!--- <th>Total</th> --->
	</tr> 
	
	<!--- if there are more than 20 rows show a total at the top as well --->
	<cfif getReportingCols.recordCount gt 20>
	 	<tr>
			<td class="reportTotal">Total</td>
			<cfif getReportingCols.recordCount gt 10><td>&nbsp;</td></cfif>
			<cfloop query="getReportingCols">
			    <td class="reportTotal">#numberFormat(structFind( reportingColTotalsStruct, "#htmleditformat(reportingCols)#" ),"#htmleditformat(numberFormatMaskValue)#")#</td>
			</cfloop>
			<!--- <th>&nbsp;</th> --->
		</tr>
	</cfif>

    <cfloop query="getReportingRows">
	    <cfset thisReportingRow = getReportingRows.reportingRows>
	    <tr>
			<td>#thisReportingRow#</td>
			<cfif getReportingCols.recordCount gt 10><td>#structFind( reportingRowTotalsStruct, "#thisReportingRow#" )#</td></cfif>
	          <cfloop query="getReportingCols">
	                 <cfif structKeyExists( reportingDataStruct, "#thisReportingRow#|#getReportingCols.reportingCols#" )>
	                     <td class="reportCell">#numberFormat(structFind( reportingDataStruct, "#htmleditformat(thisReportingRow)#|#htmleditformat(getReportingCols.reportingCols)#" ),"#htmleditformat(numberFormatMaskValue)#")#</td>
	                 <cfelse>
	                     <td class="reportCell">0</td>
	                 </cfif>
					 
	          </cfloop>
			 <td>#numberFormat(structFind( reportingRowTotalsStruct, "#htmleditformat(thisReportingRow)#" ),"#htmleditformat(numberFormatMaskValue)#")#</td>
	    </tr>
    </cfloop>
	<tr>
		<td class="reportTotal">Total</td>
		<cfif getReportingCols.recordCount gt 10><th>&nbsp;</th></cfif>
		<cfloop query="getReportingCols">
			<!--- for each column output a total from the reportingColTotalsStruct --->
		    <td class="reportTotal">#numberFormat(structFind( reportingColTotalsStruct, "#htmleditformat(reportingCols)#" ),"#htmleditformat(numberFormatMaskValue)#")#</td>
		</cfloop>
		<td class="reportTotal">#numberFormat(getGrandTotal.grandTotal,"#htmleditformat(numberFormatMaskValue)#")#</td>
	</tr>
</table>
</div>
</cfoutput>

<cfif attributes.hideFooter eq false>
	<!--- this allows us to control when the footer is shown if we are showing multiple tags on a page --->
	<cfinclude template="/templates/InternalFooter.cfm">
</cfif>
<!--- <cfdump var="#getReportingColTotals#">

<cfdump var="#reportingDataStruct#"> --->
