<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayAdminMenu.cfm	
Author:				SWJ
Date started:		2003-01-23
	
Description:		Creates a standard admin menu.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Possible enhancements:


 --->


<cfif not isDefined("attributes.qAdminMenuQuery")>
	RelayAdminMenu query object qAdminMenu is required. 
</cfif>
<cfif not isDefined("attributes.PageHeading")>
	PageHeading is required. 
</cfif>

<cfparam name="message" default=" ">
<cfif isDefined("action")>
	<cfinvoke component="relay.com.relayDataLoad" method="#action#" returnvariable="message">
		<cfinvokeargument name="tableName" value="#variables.tableName#"/>
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	</cfinvoke>
</cfif>  


<table>
	<cfoutput>
	<tr><td colspan="3" class="heading">#htmleditformat(attributes.PageHeading)#</td></tr>
	<tr><td colspan="3" class="messageText"><cfif IsSimpleValue(message)>#application.com.relayUI.message(message=message)#</cfif></td></tr>
	</cfoutput>
	<cfoutput query="attributes.qAdminMenuQuery">
	<TR>
		<TD WIDTH="20">&nbsp;</TD>
		<TD HEIGHT="20" ALIGN="right" VALIGN="TOP">
			<A HREF="/#URL#">#htmleditformat(name)#</A>
		</TD>
		<TD VALIGN="Top">#htmleditformat(description)#</TD>
	</TR>
	</cfoutput>
</table>

<cfif IsQuery(message)>
	<CF_tableFromQueryObject queryObject="#message#">
</cfif>
