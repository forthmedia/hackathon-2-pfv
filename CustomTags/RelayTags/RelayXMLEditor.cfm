<!--- �Relayware. All Rights Reserved 2014 --->
<cf_includejavascriptonce template="/javascript/prototypespinner.js">
<!---
	**************************
			Quick Edit
	**************************

	David A McLean 2002 16 Jan

	Attributes:

	XML File 				An XML file must be present in the XML directory
							on the root of the website. This file contains
							the information required to build all editors.
							This tag queries that XML file to obtain the required
							Editor Information
	Usage:  http://relay.foundation-network.com/relayAdmin/helpFiles/RelayWare_Help/RelayXMLEditor.htm

***************************************************************************

Original source Code and concept :
File name:		QuickScreen.cfm
Author:			SWJ
Date created:	2 Aug 2000

Version History

Date		Initals		Details
2002-03-18	DAM			Major revisions to permit new records adding as well as existing records editing
2004-05-01  SWJ 		Changed to you attributes.datasource and paramed datasource
2005-05-17	DAM			Modified getPK query to refined absolute means of determining the PK.
2005-09-26  GCC			Altered delimiting for JS functions to  "?" from ";"
2007-03-03	SWJ			Added attributes.showDebug which CFDUMPS xml structure
2007-02-28	WAB			Added ability to edit things which are not in tables (for relayTagEditors)
						Did a bit of a rework to remove use of lists
						Added a Required to the XML structure - not implemented on all input types yet
						Also added Max Length and Size
2007-05-20	SWJ			Added save and return menu link
						Added save and new menu link
						Also added a display function for created, last updated, lastUpdate, createdBy info
						Added the ability to define record rights by passing showRecordRights
						Added the ability to define country scope by passing showCountryScope
2007-08-20	SWJ			Added the ability to include a file for master-detail screens
2008-09-02	NJH			Fixed a bug whereby the YorN values weren't displaying correctly. Y was always checked.
2008-09-23	NJH			Fixed a bug some country scoping bugs. The conditions were using 'showRecordRights' rather than 'showCountryScope'
2009-01-02	NJH			CR-LEX581 Added enctype to the form to cater for file inputs.
2009/02/11  WAB		Added an inline entity Translation box
				Required changes to allow fieldName to be blank
				Uses Control = "Translation" and various items in an new PARAMETERS field (displayas, rows,cols, phrasetextID etc
2009/03/12  WAB		Moved the loading of rico.js for accordian - was interferring with other js libraries
2009/04/17  SSS     Enabled relayxmlEditor so you can send in required for dates
2009/04/29  WAB    Allow comment lines (still need to confirm parameters)
2009/08/28	NJH		Use some relayForm element to take advantage of validation. Should eventually move all fields to relayForm fields.
2010/08/04	PPB		Support TwoSelects ComboList as a control type
2010/12/06	NAS/MS	LID5017: Fix back link(s)
2011-03-02	NYB 	LHID5772: changed query to be able to handle functions - when passed prefixed with "func:".  If not passed with application.com then will add this
					see elearning/specialisationRules.cfm for initial usage

2011/03/16	AJC		Added code to created and lastupdated to populate that date value
2011-06-27	NYB		Changed DefaultList to use ? as it's delimiter - as the comma was causing issues with defaults containing commas
2011-10-14	NYB		LHID7283 added concepts:
					- attributes.additionalPreSubmitJS - javascript that will run after hitting Save, just prior to the form being submitted
					- new XML attribute:  fieldDisplay, which will display as the label instead of fieldValue - to customise labels.  Displays fieldValue if fieldDisplay doesn't exist
2011/11/22	PPB		CR-LEN061 - added support for uploading image files
2012/12/25	NJH		Social CR - added field_readOnly,field_noteText as valid fields to be passed in.
2013/02/20	NJH		Sprint 4 Item 13 - Added support for readonly records as well as running a function after a record has been saved.
2013-06-25	YMA		Case 435916 Allow treatEmtpyFieldAsNull to be passed in so we can null fields.
2013-04-18 	NYB 	Case 434536 nb. THIS IS NOT REQUIRED IN 2013 #nfield#_orig ARE CREATED AUTOMATICALLY AND HIDDEN IN FORMSTATE
2014-08-12	AXA		changes to allow forms to be displayed on partner portal.  added internal checks to hide functionality in portal.
2014-09-02	RPW		Add Lead Screen on Portal
2014-10-07	RPW		CORE-749 There is no confirmation bubble to say that a change has been saved in the lead edit screen. - Replaced message
2014-10-28	RPW		Changed Messages functionality to take closeAfter parameter on Set rather than get
2014-12-03	NJH		Provide more support for pre/post save functions. Ie. pass in the form as part of the arguments, as well as whether we're dealing with a new or existing record.
					Also remove the message from the url string as we shouldn't be using that anymore
2015/03/05	NJH		Jira Fifteen-264 - Removed the 'not isInternal' condition on the showSaveAndReturn functionality. Having that meant that internal users went back to the editor.
2015-05-13	AHL		P-Kas059 Quoting the parameter so we can use textids too
2015-09-14	WAB/AHL		Defaulting the country id to 0 if the entity has not be been initialized
2015-10-20	ESZ 	Case 445886 Prevent Duplicate Deal Regs from partner hitting button twice
2015-12-10	ESZ		Case 446388 Extend time warning displayed when load file of incorrect type to Media Library
2015-10-30  DAN     PROD2015-260 - updates to log the errors after tx rollback
2015-11-22  SB		Bootstrap
2016/02/01	NJH		Added rights to query that gets record.
2016/02/09	RJT		BF-494 Added support for specifying zip type uploads in the accept parameter of the xml for the XMLEditor
2016-03-01	WAB		Was not saving posts from blank multiselect boxes.  Add code to look for #field#_orig existing and #field# not existing
2016-03-03	WAB		Remove unneeded attributes related to usergroup/countryscope controls. All these attributes should be defined on the individual xml node
					And removed the showCountryScope code - again this featur eis now controlled by adding a line to the XML.
2016-04-30	WAB 	BF-648	ActivityStream Messages being created before translations are created
2016-06-15  WAB		PROD2016-876 (Reliance Security Scan)/ PROD2016-1265 Encrypt the entityID hidden field and check for that encryption when doing an update
					Encrypting will add some protection from form tampering and will allow the value to be passed to ajax functions which require encrypted values
2016-07-20  DAN     Case 450949 - only add specific activity if it's enabled in the settings
2016-09-21	WAB		PROD2016-2347 Flags not saving after initial creation	
2016-01-09	DAN		PROD2016-2470 - add support for TIFFs and BMPs accepted image formats

HOW IT WORKS:

IMAGE UPLOAD
eg courses.cfm
<field name="" control="imageFileName" label="phr_eLearning_CourseImage" parameters="filePrefix=Thumb_,fileExtension=png,displayHeight=90" description=""></field>
this definition will save a file named "Thumb_1234.png" where courseId=1234

files are uploaded to content\linkImages in a subfolder named according to the entity defined in the XML <editor> tag (eg trngCourse)
define a field in the XML with control="imageFileName"; you may define more than one per entity, but you should specify a filePrefix to distinguish them
you can send in optional "parameters": filePrefix, fileExtension, displayHeight, displayWidth
if you specify fileExtension=png and select a jpg file to load, both files will be saved
you can send in displayHeight & displayWidth to resize the display of the image in the editor (this does not resize the image)

Possible enhancements:
for control="imageFileName", we could send in additional parameters Height and Width to actually resize the image (easy using CFIMAGE)

--->

<!---	***********************
			INITIALISE
		***********************

		AttributesError will be set if any issues are discovered
--->
<CFSET thisTag= "RelayXMLEditor">
<cfparam name="attributes.xmlSourceVar" default="getSourceFromFileLocation">
<cfparam name="attributes.hideBackButton" type="boolean" default="false">
<cfparam name="attributes.showDebug" type="boolean" default="false">
<cfparam name="attributes.showSave" type="boolean" default="true"><!--- shows a link saying Save which saves and keeps the user on the edit screen --->
<cfparam name="attributes.redirect" type="boolean" default="true">
<cfparam name="attributes.showSaveAndReturn" type="boolean" default="false"><!--- adds a link saying Save and Return which takes the user back to backform after they save --->
<cfparam name="attributes.showSaveAndAddNew" type="boolean" default="false"><!--- adds a link saying Save and Add New which saves the record and then starts a new record --->
<cfparam name="attributes.backform" type="string" default=""><!--- 2010/12/06			NAS/MS		LID5017: Fix back link(s) --->
<cfparam name="attributes.saveAndReturn" type="boolean" default="false"><!--- 2014-09-10	RPW		Add Lead Screen on Portal --->


<cfparam name="attributes.preDisplayIncludeFile" type="string" default="empty.cfm">
<cfparam name="attributes.postSaveIncludeFile" type="string" default="empty.cfm"><!--- includes the file just after the save section --->
<cfparam name="attributes.includeFile" type="string" default="empty.cfm">
<cfparam name="attributes.additionalSubmitText" type="string" default=""> <!--- NJH 2008/08/20 some additional submit text - elearning 8.1 --->
<cfparam name="attributes.recordRightsCaption1" type="string" default="People"> <!--- NJH 2009/02/04 CR-SNY047 --->
<cfparam name="attributes.CountryAndRegionList" type="string" default="#listappend(request.relaycurrentuser.countrylist,request.relaycurrentuser.regionlist)#"> <!--- NJH 2009/02/04 CR-SNY047 --->
<cfparam name="attributes.hideCountryGroups" type="boolean" default="false">
<cfparam name="attributes.cfmlCallerName" default="#listLast(cgi.script_name,'/')#">
<cfparam name="attributes.treatEmtpyFieldAsNull" type="boolean" default="false">
<cfparam name="attributes.checkRights" type="boolean" default="false"> <!--- NJH 2016/02/02 - want to start checking rights on records as we currently don't. However, can't do this across the board so pass it in when needed. Would like to one day check rights on all objects --->

<cfset menuItems = "hideBackButton,showSave,showSaveAndReturn,showSaveAndAddNew">

<cfloop list="#menuItems#" index="menuName">
	<cfif structKeyExists(form,menuName)>
		<cfset attributes[menuName] = form[menuName]>
	</cfif>
</cfloop>

<cfset messageType = "success">
<CFSET AttributesError="">

<!--- by default xmlSourceVar is set to read from a file --->
<cfif attributes.xmlSourceVar eq "getSourceFromFileLocation">
	<!--- A pointer to the XML File --->
	<CFIF structKeyExists(attributes,"FileLocation")>
		<CFSET editorFileLocation=attributes.FileLocation>
	<CFELSE>
		<CFSET editorFileLocation=replace("#application.paths.relayware#\XML\EditorList.xml","/","\")>
	</CFIF>

	<CFIF fileExists("#editorFileLocation#")>
		<!--- set myXML var to the contents of the file --->
		<cffile action="read" file="#editorFileLocation#" variable="myxml">
	<CFELSE>
		<CFSET AttributesError="No EditorList.xml file found; looking for #editorFileLocation#">
	</CFIF>
</cfif>

<!--- NJH 2006/08/09 moved from inside the previous if statement as editor name was not being set if xmlSourceVar was not "getSourceFromFileLocation" --->
<!--- Check the attributes for this tag --->
<CFIF structKeyExists(attributes,"editorName")>
	<CFSET editorName=attributes.editorName>
<CFELSE>
	<CFSET AttributesError = "No editorName Attribute has been supplied; this is a required Attribute.">
</CFIF>

<CFIF IsDefined("Added")>
	<CFSET edit=true>
<CFELSE>
	<CFIF structKeyExists(attributes,"Add") and attributes.Add>
		<CFSET addnew=attributes.add>
		<CFIF addnew EQ "no">
			<CFSET edit=true>
		</CFIF>
	<CFELSE>
		<CFSET edit=true>
	</CFIF>
</CFIF>

<!--- Report any Attribute Errors and Abort --->
<CFIF AttributesError IS NOT "">
	<CFOUTPUT>
	<table border="2">
		<tr>
		<td><p><b>Error Processing Custom Tag CF_#htmleditformat(thisTag)#:</b></p>
		<p>#htmleditformat(AttributesError)#</p></td>
		</tr>
	</table></CFOUTPUT>
	<cfexit method="EXITTAG">
</CFIF>


<cffunction name="getEditor" returntype="xml">

	<cfif attributes.xmlSourceVar eq "getSourceFromFileLocation">
		<cfset editor = xmlSearch(myxml,"//editor[@name='#editorName#']")>
	<cfelse>
		<cfset editor =xmlSearch(attributes.xmlSourceVar,"//editor/")>
	</cfif>

	<cfreturn editor[1]>
</cffunction>

<cfset thisXmlEditor = getEditor()>
<cfset entity = thisXmlEditor.xmlAttributes.entity>
<cfset title = thisXmlEditor.xmlAttributes.title>


<!--- The following attributes are optional so they need to be contructed manually --->
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfif entity is not "">
<!--- Check the table exists in the current datasource --->

	<CFQUERY NAME="VerifyTable" DATASOURCE="#application.siteDataSource#">
		sp_tables N'#entity#'
	</CFQUERY>

	<CFIF VerifyTable.Recordcount GT 0>
		<!--- Get the column information of the enitity --->
		<CFQUERY NAME="GetColumnDetails" DATASOURCE="#application.siteDataSource#">
			sp_columns #entity#
		</CFQUERY>

		<CFTRY>
			<CFQUERY NAME="GetPK" DATASOURCE="#application.siteDataSource#">
				select
					c.COLUMN_NAME
				from
					INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
					INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
				where
					pk.TABLE_NAME =  <cf_queryparam value="#entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and
					CONSTRAINT_TYPE = 'PRIMARY KEY'
					and
					c.TABLE_NAME = pk.TABLE_NAME
					and
					c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME
			</CFQUERY>

			<CFCATCH>
				<CFSET AttributesError = "It has not been possible to obtain data regarding the primary keys on entity #entity#. ">
				<CFSET AttributesError = AttributesError & "Ensure that the SQL Server is configured for DATA ACCESS. ">
				<CFSET AttributesError = AttributesError & "This can be done using the command sp_serveroption, please do not alter the configuration of your server without contacting your Database Administrator for assistance, as changing this option may conflict with other applications.">
				<CFSET OK=0>
			</CFCATCH>
		</CFTRY>

		<CFIF GetPK.Recordcount EQ 0>
			<CFSET AttributesError = "Entity #entity# does not have a Primary Key defined and hence records cannot be added or updated. ">
			<CFSET AttributesError = AttributesError & "Please contact your Database Adminsitrator for assistance. ">
			<CFSET OK=0>
		</CFIF>
		<CFIF GetPK.Recordcount GT 1>
			<CFSET AttributesError = "Entity #entity# has a composite primary key. Composite primary keys are not currently handled by this process. ">
			<CFSET AttributesError = AttributesError & "Please contact your Database Adminsitrator for assistance. ">
			<CFSET OK=0>
		</CFIF>

	<CFELSE>
		<CFSET AttributesError = "The value #entity# supplied for entity is not a valid table or updatable view for the current datasource.">
	</CFIF>

	<!--- Report any Attribute Errors and Abort --->
	<CFIF AttributesError IS NOT "">
		<CFOUTPUT>
		<table border="2">
			<tr>
			<td><p><b>Error Processing Custom Tag CF_#htmleditformat(thisTag)#:</b></p>
			<p>#htmleditformat(AttributesError)#</p></td>
			</tr>
		</table></CFOUTPUT>
		<cfexit method="EXITTAG">
	</CFIF>

	<cfif not structKeyExists(form,GetPK.column_Name)>
		<cfif structKeyExists(url,GetPK.column_Name)>
			<cfset form[GetPK.column_Name] = url[GetPK.column_Name]>
		<cfelse>
			<cfset form[GetPK.column_Name] = 0>
		</cfif>
	</cfif>

	<cfif structKeyExists(form,"added") or structKeyExists(form,"Update")>
		<cfif structKeyExists(form,"Update")>
			<cf_checkFieldEncryption fieldNames = "#GetPK.COLUMN_NAME#">
		</cfif>
		
		<cfset timeStamp= request.requestTime>
		<cfset imageFields = "">
		<cfif form[GetPK.COLUMN_NAME] eq "">
			<cfset form[GetPK.COLUMN_NAME] = 0>
		</cfif>

		<cfset entityDetails = {}>
		<cfset columnsInTable = valueList(GetColumnDetails.column_name)>

		<cfset result = {isOK = true,entityID=form[GetPK.COLUMN_NAME]}>
		<cfset message = "">
		<cfset errorMessage = "">
		<cfset messageType = "success">

		<!--- NJH 2013/02/20- added the ability to run a function post saving a record, rather than having to include a .cfm. The form gets passed through to the function --->
		<cfif structKeyExists(attributes,"preSaveFunction")>
			<cf_transaction action="begin">
				<cftry>
					<cfset myFunction = attributes.preSaveFunction>
					<cfset preSaveResult = myFunction(argumentCollection=form,fieldCollection=form)>

					<cfset theError = preSaveResult>

					<cfcatch>
						<cfset preSaveResult.isOK = false>
						<cfset theError = cfcatch>
					</cfcatch>
				</cftry>

				<cfif not preSaveResult.isOK>
					<cf_transaction action="rollback">
					<cfset errorID = application.com.errorHandler.recordRelayError_Warning(type="XML Editor PreSaveFunction",Severity="error",caughtError=theError,WarningStructure=form)>

					<cfset preSaveResult.message = "Problem saving record. Refer an administrator to errorID #errorID#.">
					<cfset errorMessage = preSaveResult.message>
					<cfset result.isOK = false>
				</cfif>
			</cf_transaction>
		</cfif>

		<cfloop collection="#form#" item="field">
			<!--- 2016-03-01	WAB		Was not saving posts from blank multiselect boxes.  So now look for #field#_orig existing and #field# not existing --->
			<cfset fieldWithoutOrig = replaceNoCase(field,"_orig","")>
			<!--- if it's not a file field and the field is in the fieldlist--->
			<cfif field neq GetPK.COLUMN_NAME and listFindNoCase(form.fieldList,field) and application.com.relayEntity.getTableFieldStructure(tablename=entity,fieldname=field).isOk>
				<cfset entityDetails[field] = form[field]>
			<cfelseif refindNocase("_orig\Z",field) and  application.com.relayEntity.getTableFieldStructure(tablename=entity,fieldname=fieldWithoutOrig).isOk and not structKeyExists (entityDetails,fieldWithoutOrig)>

				<cfset entityDetails[fieldWithoutOrig] = "">
			<!--- NJH 2013/02/20 if entityTypeID is not defined in application, then just check against the table column names... probably could/should just do this in all cases... --->
			<cfelseif field neq GetPK.COLUMN_NAME and listFindNoCase(columnsInTable,field)>
				<cfset entityDetails[field] = form[field]>
			</cfif>
		</cfloop>

		<!--- if using cfinsert/cfupdate, set some crud columns --->
		<cfif not structKeyExists(application.entityTypeID,entity)>
			<cfset crudList = "lastUpdatedBy,lastUpdatedByPerson">
			<cfif form[GetPK.COLUMN_NAME] eq 0>
				<cfset crudList = listAppend(crudList,"createdBy,createdByPerson")>
			<cfelse>
				<cfset crudList = listAppend(crudList,"lastUpdated")>
			</cfif>
			<cfloop list="#crudList#" index="crudField">
				<cfif listFindNoCase(columnsInTable,crudField) and not structKeyExists(form,crudField)>
					<cfif listFindNoCase("lastUpdatedByPerson,createdByPerson",crudField)>
						<cfset form[crudField] = request.relayCurrentUser.personID>
					<cfelseif crudField eq "lastUpdated">
						<cfset form[crudField] = request.requestTime>
					<cfelse>
						<cfset form[crudField] = request.relayCurrentUser.userGroupID>
					</cfif>
					<cfset entityDetails[crudField] = form[crudField]>
				</cfif>
			</cfloop>
		</cfif>

        <cfset errorStruct = structNew()>

		<cf_transaction action="begin">
			<cftry>
				<cfif result.isOK>
					<cfif form[GetPK.COLUMN_NAME] eq 0>
						<cfif structKeyExists(application.entityTypeID,entity)>
							<cfset result = application.com.relayEntity.insertEntity(entityDetails=entityDetails,table=entity,insertIfExists=true)>
						<cfelse>

							<cftry>
								<cfinsert datasource="#application.siteDataSource#" tablename="#entity#" formfields="#structKeyList(entityDetails)#">

								<cfquery name="PKInfo" dbType="query">
									SELECT TYPE_NAME FROM GetColumnDetails WHERE Column_Name = '#GetPK.COLUMN_NAME#'
								</cfquery>
								<cfif GetPK.Recordcount EQ 1 and PKInfo.TYPE_NAME IS "int identity">
									<cfquery name="AddedRecord" dataSource="#application.siteDataSource#">
										SELECT max(#GetPK.COLUMN_NAME#) as entityId FROM #entity#
									</cfquery>
									<cfset result.entityID = AddedRecord.entityId>
								<cfelse>
									<cfset result.entityID = evaluate("#GetPK.COLUMN_NAME#")>
								</cfif>

								<cfcatch>
									<cfset result.isOK=false>
									<cfset result.message = cfcatch.message>
								</cfcatch>
							</cftry>
						</cfif>

						<cfset form[GetPK.COLUMN_NAME] = result.entityID>
						<cfset newRecordID = result.entityID>
						<!--- 2016-04-30	WAB 	BF-648	ActivityStream Messages being created before translations are created, so add ProcessPhraseUpdateForm() here --->
						<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>

						<cfif result.isOk>
							<!--- NJH 2012/01/17 - Social Media - add an activity when adding certifications, courses and modules --->
							<cfif listFindNoCase("trngModule,trngCourse,trngCertification",entity)>
                                <!--- 2016-07-20 DAN Case 450949 - only add specific activity if it's enabled in the settings --->
                                <cfif application.com.settings.getSetting("socialMedia.enableSocialMedia") AND application.com.settings.getSetting("socialMedia.share.#entity#.added")>
    								<cfset shareMergeStruct = {module="phr_title_#entity#_#newRecordID#"}>

    								<cfset application.com.service.addRelayActivity(action="added",performedOnEntityID=newRecordID,performedOnEntityTypeID=application.entityTypeID[entity],mergeStruct=shareMergeStruct,performedByEntityID=application.com.settings.getSetting("theClient.clientOrganisationID"),performedByEntityTypeID=2)>
    					        </cfif>
							</cfif>

							<cfset message = "Record Added">
						<cfelse>
							<cfset errorMessage = result.message>
						</cfif>

					<!--- an update --->
					<cfelse>
						<cfif structKeyExists(application.entityTypeID,entity)>
							<cfset result = application.com.relayEntity.updateEntityDetails(entityDetails=entityDetails,table=entity,entityID=form[GetPK.COLUMN_NAME],updateDeleted=true,treatEmtpyFieldAsNull=attributes.treatEmtpyFieldAsNull)>
							<!--- 2016-04-30	WAB 	BF-648	add ProcessPhraseUpdateForm() (not strictly necessary for activityStream, but makes sense for completeness)--->
							<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>

						<cfelse>
							<cftry>
								<cfupdate datasource="#application.siteDataSource#" tablename="#entity#" formfields="#structKeyList(entityDetails)#,#GetPK.COLUMN_NAME#">

								<cfcatch>
									<cfset result.isOK=false>
									<cfset result.message = cfcatch.message>
								</cfcatch>
							</cftry>
						</cfif>
						<cfif result.isOK>
							<cfset message = "Record Updated">
						<cfelse>
							<cfset errorMessage = result.message>
						</cfif>
						<cfset caller.url[GetPK.COLUMN_NAME] = result.entityID>
					</cfif>
				</cfif>


				<cfset form.isOK = result.isOK>

				<cfif result.isOK>

					<cfset UgRecordManagerEntityID = result.entityID>
					<cfset countryScopeEntityID = result.entityID>
					<CF_UGRecordManagerTask>
					<CF_CountryScopeTask>

					<!--- update any flags on the page --->
					<cfset flagArgs = {entityType=entity,entityID=form[GetPK.COLUMN_NAME]}>
					<!--- For a newly added record the flag form fields will use entityID=0, so we have to tell processFlagForm about this --->
					<cfif structKeyExists(form,"added")>
						<cfset flagArgs.formEntityID=0>
					</cfif>
					<cfset application.com.flag.processFlagForm(argumentCollection=flagArgs)>

					<cfset uploadResult = uploadFiles(entityId=form[GetPK.COLUMN_NAME])>

					<cfif not uploadResult.isOK>
						<cfset errorMessage = errorMessage& " "& uploadResult.message>
					</cfif>

					<cfquery name="getRecordDetails">
						SELECT e.* FROM #entity# AS E
						<cfif attributes.checkRights>
						#application.com.rights.getRightsFilterQuerySnippet(entityType=entity,alias="e").join#
						</cfif>
						WHERE e.#GetPK.COLUMN_NAME# = #form[GetPK.COLUMN_NAME]#
					</cfquery>

					<!--- 20/09/2007 SWJ added to support additional logic being added to the save processing --->
					<cfif attributes.postSaveIncludeFile neq "empty.cfm">
						<cfinclude template="#attributes.postSaveIncludeFile#">
					</cfif>

					<!--- NJH 2013/02/20- added the ability to run a function post saving a record, rather than having to include a .cfm. The form gets passed through to the function --->
					<cfif structKeyExists(attributes,"postSaveFunction")>
						<cfset functionArgs = {}>
						<cftry>
							<cfset myFunction = attributes.postSaveFunction>
							<cfset recordDetails = application.com.structureFunctions.queryToStruct(query=getRecordDetails,key=GetPK.COLUMN_NAME)>
							<cfset functionArgs = recordDetails[form[GetPK.COLUMN_NAME]]>
							<cfset structAppend(functionArgs,uploadResult)>
							<cfset structAppend(functionArgs,form,false)>
							<cfset functionArgs.newRecord = structKeyExists(form,"added")?true:false>
							<cfset postSaveResult = myFunction(argumentCollection=functionArgs)>

							<cfcatch>
								<cfset postSaveResult.isOK = false>
                                <cfset errorStruct.argumentCollection = {type="XML Editor PostSaveFunction",Severity="error",catch=cfcatch,WarningStructure=functionArgs}> <!--- PROD2015-260 --->
							</cfcatch>
						</cftry>

						<cfif postSaveResult.isOK>
							<cfset message = message& " "&postSaveResult.message>
						</cfif>
					</cfif>
				</cfif>

				<cfif errorMessage neq "" OR structkeyexists(errorStruct, "argumentCollection")>
					<cfthrow message="Error saving record. #errorMessage#">

				</cfif>

				<cf_transaction action="commit">

				<cfcatch>
					<cf_transaction action="rollback">

                    <!---PROD2015-260 - log this error and also previous catch if exists --->
                    <cfset errorID = application.com.errorHandler.recordRelayError_Warning(type="XML Editor",Severity="error",caughtError=cfcatch)>
                    <cfif structkeyexists(errorStruct, "argumentCollection")>
                        <cfset errorIDPostSaveResult = application.com.errorHandler.recordRelayError_Warning(argumentCollection=errorStruct.argumentCollection)>
                        <cfset postSaveResult.message = "Problem saving record. Refer an administrator to errorID #errorID# and #errorIDPostSaveResult#">
                    <cfelse>
                        <cfset postSaveResult.message = "Problem saving record. Refer an administrator to errorID #errorID#">
                    </cfif>
                    <cfset errorMessage = errorMessage & " " & postSaveResult.message>

				</cfcatch>
			</cftry>
		</cf_transaction>
<cfsetting showdebugoutput="true">


        <!--- 2015-12-10	ESZ		Case 446388 --->
		<cfset closeafter=3>
		<cfif errorMessage neq "">
			<cfset message = errorMessage>
			<cfset messageType = "warning">
			<cfset closeafter=0>
		</cfif>

			<cfscript>
				//2014-09-10	RPW	Add Lead Screen on Portal
				//2014-10-28	RPW	Changed Messages functionality to take closeAfter parameter on Set rather than get
				//2015-12-31	ESZ	Case 446388 - closeafter as variable
				application.com.relayui.setMessage(
					message=message,
					type=messageType,
					scope="session",
					closeAfter=#closeafter#
				);
			</cfscript>

		<!--- 2007-05-20 SWJ added to support saveAndReturn to listing screen --->
		<!--- 2014-09-16	RPW		Add Lead Screen on Portal
				2015/03/05	NJH		Jira Fifteen-264 - Removed the 'not isInternal' condition on the below. Having that meant that internal users went back to the editor.
		--->
		<cfif structKeyExists(form,"saveAndReturn") and form.saveAndReturn and messageType eq "success">
			<script language="javascript" type="text/javascript">
				<cfoutput>location.href = '#jsStringFormat(form.backform)#';</cfoutput>
			</script>
		</cfif>

		<!--- 2007-05-20 SWJ added to support SaveAndAddNew which takes the user to a blank screen --->
		<cfif structKeyExists(form,"SaveAndAddNew") and form.SaveAndAddNew and attributes.redirect EQ true and messageType eq "success">

			<!--- set the goToURL to the current template plus the editor strings so that it opens in new record mode --->
			<cfset goToURL = "#cgi.SCRIPT_NAME#?editor=yes&add=yes&backform=#form.backform#&#GetPK.COLUMN_NAME#=0">

			<cfloop collection="#url#" item="urlParam">
				<cfif not listFindNoCase("editor,add,backform,message,#GetPK.COLUMN_NAME#",urlParam)>
					<cfset goToURL = goToURL&"&#urlParam#=#url[urlParam]#">
				</cfif>
			</cfloop>

			<cfloop list="#menuItems#" index="menuName">
				<cfif not structKeyExists(url,menuName)>
					<cfset goToURL = goToURL&"&#menuName#=#form[menuName]#">
				</cfif>
			</cfloop>
			<!--- set form.backform current script which will usually show the listing screen.  If we don't set this then back becomes the http.referrer which gets incorrect results --->

			<script language="javascript" type="text/javascript">
				<cfoutput>location.href = '#jsStringFormat(goToURL)#';</cfoutput>
			</script>
		</cfif>
	</cfif>
</cfif>

<!--- 2015-05-13 AHL P-Kas059 Quoting the parameter so we can use textids too  --->
<cfquery name="getRecordDetails">
	SELECT e.* FROM #entity# AS E
	<cfif attributes.checkRights>
	#application.com.rights.getRightsFilterQuerySnippet(entityType=entity,Alias="e").join#
	</cfif>
	WHERE e.#GetPK.COLUMN_NAME# = <Cf_queryparam value="#form[GetPK.COLUMN_NAME]#" cfsqltype="cf_sql_integer">
</cfquery>

<!--- if we don't have sufficient rights, then stop. --->
<cfif attributes.checkRights and getRecordDetails.recordCount eq 0 and form[GetPK.COLUMN_NAME] neq 0>
	<cfset application.com.relayUI.setMessage(message="Insufficient rights",type="warning")>
	<cf_abort>
</cfif>

<CFPARAM NAME="message" TYPE="string" DEFAULT="">
<cfset editorCFM = "/"&listFirst(cgi.SCRIPT_NAME,"/")&"/"&attributes.cfmlCallerName&".cfm">

<SCRIPT LANGUAGE="JavaScript">
<!--
<cfoutput>
var entityType_parent = '#entity#';
var parentEntityID = #form[GetPK.COLUMN_NAME]#;
var parentUniqueKey = '#GetPK.COLUMN_NAME#';
var childColumnsToShow = '';
var showRecordDeleteOption = '';
var entityType_child = '';
</cfoutput>

function showChildRecords(childEntityType,columnsToShow,showRecordDelete) {
	// if we're calling this for the first time, then set some global variables which we then use in other functions
	if (childColumnsToShow == '') {
		childColumnsToShow = columnsToShow;
	}
	if (showRecordDeleteOption == '') {
		showRecordDeleteOption = showRecordDelete;
	}
	if (entityType_child == '') {
		entityType_child = childEntityType;
	}

	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayXMLEditorWS&methodName=showChildRecords&_cf_nodebug=true&returnFormat=json',
        	data:{parentEntityType:entityType_parent,parentEntityID:parentEntityID,childEntityType:childEntityType,columnsToShow:childColumnsToShow,editorcfm:'<cfoutput>#editorCFM#</cfoutput>',showRecordDelete:showRecordDeleteOption},
        	dataType:'json',
        	success: function(data,textStatus,jqXHR) {
				jQuery('#childRecordsDiv').html(data);
			}
		});
}

function deleteChildRecord(childEntityType,childEntityID){
	if (confirm('Are you sure you want to delete the current record?')) {
		jQuery.ajax(
	    	{type:'post',
	        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayXMLEditorWS&methodName=deleteChildRecord&_cf_nodebug=true&returnFormat=json',
	        	data:{childEntityType:childEntityType,childEntityID:childEntityID},
	        	dataType:'json',
	        	success: function(data,textStatus,jqXHR) {
					showChildRecords(childEntityType);
				}
			});
	}
}

function addChildRecord(childEntityType){
	parentObj = jQuery('#add'+childEntityType+'Record').get(0);
	inputElems = getInputElements(parentObj)

	setValidateForInputElements(inputElems,true);

	for (i = 0;i<inputElems.length; i++)  {
		inputElems[i].removeAttribute('novalidate');
	}

	if (doRelayValidationOnArrayOfElements(parentObj,inputElems,extendRelayValidationOptions({showValidationErrorsInline:false}))) {
		var argString=serializeInputElements(inputElems)
		argString = argString+'&'+parentUniqueKey+'='+parentEntityID;

		jQuery.ajax(
    	{type:'post',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayXMLEditorWS&methodName=addChildRecord&_cf_nodebug=true&returnFormat=json',
        	data:argString+'&childEntityType='+childEntityType,
        	dataType:'json',
        	success: function(data,textStatus,jqXHR) {
				showChildRecords(childEntityType);
			}
		});
	}
}

// SWJ 2007-05-20
function jsSaveAndReturn() {
	document.editorForm.saveAndReturn.value = "yes";
	FormSubmit();
}

// SWJ 2007-05-20
function jsSaveAndAddNew() {
	document.editorForm.SaveAndAddNew.value = "yes";
	FormSubmit();
}
function FormSubmit(buttonObj){
	form = document.editorForm;

	// if we have child records showing, then set the novalidate attribute on the child record inputs so that they don't get picked up in the form validation for the parent record.
	if (entityType_child != '') {
		parentObj = jQuery('#add'+entityType_child+'Record').get(0);
		inputElems = getInputElements(parentObj)
		setValidateForInputElements(inputElems,false);
	}
	if (form.onsubmit) {
		okToSubmit = form.onsubmit()
	} else {
		okToSubmit = true
	}

	if (okToSubmit) {
		if (buttonObj) {
			$(buttonObj).spinner({position:'left'})
//			$(form).spinner()
			$(window.document.body).spinner()
			jQuery('#save input').prop('disabled', true); //ESZ Case 445886 2015-10-20
		}

		form.submit()
	}
}
//-->
</SCRIPT>

<CFIF attributes.BackForm EQ "">
	<CFIF structKeyExists(form,"backForm")>  <!--- WAB added this if - it deals with the issue of multiple saves on the same page leading to the back button only going back one page.  Instead we pick up the value of backform which has been posted --->
		<CFSET BackForm = form.backForm>
	<CFELSEIF IsDefined("HTTP_REFERER")>
		<CFSET BackForm = HTTP_REFERER>
	<CFELSE>
		<CFSET BackForm = SCRIPT_NAME>
	</CFIF>
	<!--- if you need to add variables to the back button hyperlink below then pass the variables into this tag
		  via attributes.backButtonURLVars need to be looped over and built into an evaluated list of links.
		  NB,	--->
	<cfif structKeyExists(attributes,"backButtonURLVars")>
		<cfset backButtonVars = "">
		<cfset debugDuplicateVars = "">
		<cfset counter = 1>
		<cfloop list="#attributes.backButtonURLVars#" index="variableName">
			<cfif counter eq 1>
				<cfset varSeperator = "?">
			<cfelse>
				<cfset varSeperator = "&">
			</cfif>
			<!--- Here we make sure we do not add duplicate variables --->
			<cfif isDefined("variableName")>
				<cfif findNoCase(variableName,backform,0) eq 0>
					<cfset backButtonVars = "#backButtonVars##varSeperator##variableName#=#evaluate(variableName)#">
				<cfelse>
					<!--- if a duplicate is found stick it in this variable so that we can output it for debugging later if attributes.showDebug is true --->
					<CFSET debugDuplicateVars = debugDuplicateVars & variableName & "|">
				</cfif>
			<cfelse>
				Error: you must pass the variable:#variableName# into the tag either in the URL or FORM scope
			</cfif>
			<cfset counter = counter + 1>
		</cfloop>

		<CFSET BackForm = BackForm & backButtonVars>
	</cfif>
<!--- 2010/12/06			NAS/MS		LID5017: Fix back link(s) --->
<cfelse>
	<CFSET BackForm = attributes.BackForm>
</CFIF>

<!--- NJH 2013/02/20 - add ability to make form readonly --->
<cfset currentRecord = getRecordDetails>
<cfset readOnlyRecord=false>

<cfif structKeyExists(thisXmlEditor.xmlAttributes,"readonly") and ((isBoolean(thisXmlEditor.xmlAttributes.readOnly) and thisXmlEditor.xmlAttributes.readOnly) or (not isBoolean(thisXmlEditor.xmlAttributes.readOnly) and evaluate(thisXmlEditor.xmlAttributes.readOnly)))>
	<cfset readOnlyRecord = true>
</cfif>

<cfif readOnlyRecord>
	<cfset title = "#title# (Read Only)">
</cfif>

<!--- Create a tophead cfm for xmlEditor so that messages get displayed in a standard way (in on request end) --->
<cfset application.com.request.setTophead(topHeadCfm="\customTags\relayTags\relayXmlEditorTopHead.cfm",pageTitle=title,readOnlyRecord=readOnlyRecord,BackForm=BackForm,showSave=attributes.showSave,showSaveAndReturn=attributes.showSaveAndReturn,showSaveAndAddNew=attributes.showSaveAndAddNew,hideBackButton=attributes.hideBackButton)>


<!--- 	2016-09-21	WAB		PROD2016-2347 Flags not saving after initial creation
		Underlying problem caused by URL.add getting propagated through the URL
		And also the field form.added getting propagated unnecessarily
		So I am having to delete url.add and recreate the query_string
 --->
<cfset structDelete (url,"formstate")>
<cfset structDelete (url,"add")>
<cfset queryString  = application.com.structureFunctions.convertStructureToNameValuePairs(struct = URL,delimiter="&",encode="URL")>

<!--- NJH 2009/02/01 CR-LEX581 added enctype to the form as we we're dealing with file uploads --->
<cfoutput>
	<form action="?#application.com.security.encryptQueryString(queryString)#" method="POST" id="editorForm" name="editorForm" enctype="multipart/form-data" <cfif structKeyExists(thisXmlEditor.xmlAttributes,"onsubmit")>onsubmit="<cfoutput>#thisXmlEditor.xmlAttributes.onsubmit#</cfoutput>"</cfif>>
</cfoutput>

<CFIF IsDefined("AddNew") and AddNew IS "yes">
	<input type="hidden" name="Added" value="">
	<!--- SWJ this should set the editor to update mode so that any fields that should show in that mode --->
	<cfset structDelete(URL,"AddNew")>
<CFELSEIF isdefined("Update") OR IsDefined("Edit")>
	<input type="hidden" name="Update" value="Update">
</CFIF>

<!--- 	2016-09-21	WAB		PROD2016-2347
		Removed this line.  
		After a save this item continued to be added to the form.
		Was causing flag update code to think that we were in the middle of an Add, so update was failing
		Should not be needed, the lines above do what is needed
	<CFIF isdefined("Added")>
		<INPUT Name="Added" TYPE="hidden" VALUE="">
	</CFIF> 
--->

<cfoutput>
<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->
<input type="hidden" name="saveAndReturn" value="#attributes.saveAndReturn#">		<!--- added to control saveAndReturn behaviour --->
<input type="hidden" name="SaveAndAddNew" value="no">		<!--- added to control SaveAndAddNew behaviour --->
</cfoutput>
<cfloop list="#menuItems#" index="menuName">
	<cf_input type="hidden" name="#menuName#" value="#attributes[menuName]#">
</cfloop>

<CF_INPUT type="hidden" name="BackForm" value="#BackForm#">  <!--- this is set to the value of the listing screen normally --->
<input type="hidden" name="OtherFormVars" value="">
<input type="hidden" name="OtherFormVals" value="">
<cfif structKeyExists(attributes,"passThroughVariablesStructure")>
	<cfloop collection="#attributes.passThroughVariablesStructure#" item="formFieldName">
		<cfif formFieldName neq "fieldnames">
			<cfoutput><CF_INPUT type="hidden" name="#formFieldName#" value="#attributes.passThroughVariablesStructure[formFieldName] #"></cfoutput>
		</cfif>
	</cfloop>
</cfif>

<!--- 	NJH 2016/02/09 - set portal class to be the override class for now to sort out various portal issues that form-horizontal causes
		WAB 2016/03/09 - set class to blank, to prevent default of form-horizontal.  All RelayXML Editor stuff is Stacked regardless of site
--->
<cfset formDisplayAttributes = {bindValidationWithListener="false", autoRefreshRequiredClass="true", class=""}>

<cf_relayFormDisplay attributeCollection=#formDisplayAttributes#>

	<cfif attributes.preDisplayIncludeFile neq "empty.cfm">
		<cfinclude template="#attributes.preDisplayIncludeFile#">
	</cfif>

	<cfset request.fieldList = "">
	<cfset thisXmlEditor = getEditor()>

	<cfset application.com.relayDisplay.displayXMLEditorFields(fieldNodes=thisXmlEditor.xmlChildren,entityType=entity,recordDetails=getRecordDetails,entityID=form[getPK.column_Name],argumentCollection=attributes,readOnly=readOnlyRecord,primaryKey=GetPK.COLUMN_NAME)>

	<cfoutput>
		<cf_encryptHiddenFields>
			<input type="hidden" name="fieldlist" id="fieldList" value="#request.fieldList#">
			<!--- for backwards compatibility --->
		</cf_encryptHiddenFields>
		<cfif not attributes.xmlSourceVar eq "getSourceFromFileLocation">
			<input type="hidden" name="#GetPK.COLUMN_NAME#" id="#GetPK.COLUMN_NAME#" value="#application.com.security.encryptVariableValue(name = GetPK.COLUMN_NAME, value = form[GetPK.COLUMN_NAME])#">
		</cfif>

		<!--- 2014-04-22 - RMB - P-KAS024 - New functionality to show screen by entity type -
		added to repleace missing abilty to config file screens. but this will allow any entity
		type that use this tag - to add new screen to capute data by config only - START
		2014-06-11 RMB/WAB pass in countryID as well.
		--->

		<cfset useScreenByName = Entity & "Edit_Custom">

		<cfif application.com.screens.doesScreenExist(CurrentScreenId = useScreenByName)>


			<cfscript>
			aScreenArgs = {
				screenid = useScreenByName,
				method = "edit",
				entity = Entity
			};

			aScreenArgs["#GetPK.COLUMN_NAME#"] = form[GetPK.COLUMN_NAME]	;
			if (listFindNoCase (getRecordDetails.columnList,"countryid")) {
				aScreenArgs['countryID'] = getRecordDetails.countryID;
			} else {
				aScreenArgs['countryID'] = request.relayCurrentUser.countryID;
			}
			/* START 2015-09-14 WAB/AHL Defaulting the country id to 0 if the entity has not be been initialized */
			if (aScreenArgs['countryID'] is "") {
				aScreenArgs['countryID'] = 0;
			}
			/* END 2015-09-14 WAB/AHL */

			</cfscript>
				<!--- NJH PROD2015-87 - change the way the sceen is called in. Now have a new function that handles it much nicer --->
				<cfset application.com.relayDisplay.showScreenElementsInDisplayXML(screenID=useScreenByName,countryID=aScreenArgs.countryID,entityID=form[getPK.column_Name])>
			<!---
<tr>
				<td colspan="2" align="center">
						<cf_ascreen formName = "editorForm">
							<cf_ascreenitem attributeCollection = #aScreenArgs# >
						</cf_ascreen>
				</td>
			</tr>
 --->

		</cfif>


		<!--- 2007-08-23 SWJ Added to support include file --->
		<tr>
			<td colspan="2" align="center">
				<cfif attributes.includeFile neq "empty.cfm">
					<cftry>
						<cfinclude template="#attributes.includeFile#">
						<cfcatch type="missinginclude">
							<p>#htmleditformat(cfcatch.message)#</p>
						</cfcatch>
					</cftry>
				</cfif>
			</td>
		</tr>
	</cfoutput>
</cf_relayFormDisplay>
<cfoutput>
</form>
</cfoutput>


<!--- 2011/11/22 PPB CR-LEN061 - added support for uploading image files --->
<cffunction name="uploadFiles" returntype="struct">
	<cfargument name="entityID" type="numeric" required="true">

	<cfset var fileCount = 1>
	<cfset var filePrefix = "">
	<cfset var fileExtension = "">
	<cfset var parametersStruct = structNew()>
	<cfset var fileAttributes = structNew()>
	<cfset var destination = "">
	<cfset var targetFileRelativePath = "/linkImages/#entity#/#arguments.entityID#">
	<cfset var targetFilePath = "#application.paths.content##targetFileRelativePath#">
	<cfset var cffile = structNew()>
	<cfset var filename = "#filePrefix##arguments.entityID#.#fileExtension#">
	<cfset var entityFiles = "">
	<cfset var accept = "image/jpeg,image/jpg,image/gif,image/pjpeg,image/png,image/bmp,image/x-ms-bmp,image/tiff,application/pdf">
	<cfset var fileArray = arrayNew(1)>
	<cfset var targetSize = 80>
	<cfset var updateRecord = false>
	<cfset var result = {isOK=true,message="",filesUploaded="",filesDeleted=""}>
	<cfset var createThumbnail = false>
	<cfset var targetFileName = "">
	<cfset var fileSource = "">
	<cfset var checkField = "">

	<cfif arguments.entityID neq 0 and attributes.xmlSourceVar neq "getSourceFromFileLocation">
		<cfset fileArray =xmlSearch(attributes.xmlSourceVar,"//field[contains(translate(@control,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'file')]")>

		<cfloop array="#fileArray#" index="fileField">
			<cfset fileAttributes = fileField.xmlAttributes>
			<cfset fileName = "">
			<cfset filePrefix = "">
			<cfset fileExtension = "">
			<cfset accept = "">
			<cfset createThumbnail = false>

			<cfif fileAttributes.name eq ""> <!--- we only process the files that don't have a name associated with them.. if it has a name, then we assume that we've coded something speicifically for that file.. done in the file edit screen.. --->

				<!--- filter the accept types if it has been specified in the xml --->
				<cfif structKeyExists(fileAttributes,"accept")>
					<cfloop list="#fileAttributes.accept#" index="acceptType">
						<cfif acceptType eq "pdf">
							<cfset accept = listAppend(accept,"application/pdf")>
						<cfelseif listFindNoCase("jpeg,jpg,gif,pjpeg,png,bmp,tiff",acceptType)>
							<cfset accept = listAppend(accept,"image/#acceptType#")>
						<cfelseif acceptType eq "zip">
							<cfset accept = listAppend(accept,"application/zip")>
						</cfif>
					</cfloop>
				<cfelseif structKeyExists(fileAttributes,"acceptType") and fileAttributes.acceptType eq "image">
					<cfset accept = "image/jpeg,image/jpg,image/gif,image/pjpeg,image/png,image/bmp,image/x-ms-bmp,image/tiff">
				</cfif>

				<cfset parametersStruct = structNew()>
				<cfif structKeyExists(fileAttributes,"parameters")>
					<cfset parametersStruct = application.com.structureFunctions.convertNameValuePairStringToStructure(fileAttributes.parameters,",")>
				</cfif>

				<cfif StructKeyExists(parametersStruct,"FilePrefix")>
					<cfset filePrefix  = parametersStruct.FilePrefix>
				</cfif>
				<cfif StructKeyExists(parametersStruct,"FileExtension")>
					<cfset fileExtension  = parametersStruct.FileExtension>
				</cfif>
				<cfif structKeyExists(parametersStruct,"height")>
					<cfset targetSize = parametersStruct.height>
				<cfelseif structKeyExists(parametersStruct,"width")>
					<cfset targetSize = parametersStruct.width>
				</cfif>
				<cfset targetFileName = "#filePrefix##arguments.entityID#">
				<cfif structKeyExists(parametersStruct,"filePrefix") and parametersStruct.filePrefix eq "Thumb_">
					<cfset createThumbnail = true>
				</cfif>

				<cfif (structKeyExists(form,"file#fileCount#") and form["file#fileCount#"] neq "") or StructKeyExists(form,"file#fileCount#_delete") or (structKeyExists(form,"file#fileCount#_default") and form["file#fileCount#_default"] neq "")>		<!--- if an image has been selected or is marked for delete --->

					<cfif (structKeyExists(form,"file#fileCount#") and form["file#fileCount#"] neq "")>
						<cfset fileSource = "file#fileCount#">
						<cfset checkField = fileSource>
					<cfelseif structKeyExists(form,"file#fileCount#_default") and form["file#fileCount#_default"] neq "">
						<cfset checkField = "file#fileCount#_default">
						<cfset fileSource = form[checkField]>
					</cfif>

					<!--- NJH we want to delete regardless, particularly if a file extension is not specified, as that will make sure we have only one image specified. For example,
						if a user uploads a jpg and then a png, we will end up having two files for this entity with two different extensions, and no way of deleting the second image.. this keeps the image up to date. --->
						<!--- delete all files with this prefix+entityid regardless of extension (if a jpg is uploaded with parameter "fileExtension=png" in the xml, then a png is created too --->
					<cfdirectory name="entityFiles" action="list" directory="#targetFilePath#" filter="#filePrefix##arguments.entityID#.*">

					<cfloop query="entityFiles">
						<cffile action="delete" file="#entityFiles.directory#\#entityFiles.name#">
						<cfset result.filesDeleted = listAppend(result.filesDeleted,entityFiles.name)>
					</cfloop>

					<cfif entityFiles.recordCount>
						<cfset updateRecord = true>
					</cfif>

					<cfif form[checkField] neq "">	<!--- if an image has been selected --->
						<cfset destination = "#targetFilePath#\#filePrefix##arguments.entityID#">

						<cfif fileExtension neq "">
							<cfset fileName = "#filePrefix##arguments.entityID#.#fileExtension#">
						</cfif>

						<cfif createThumbnail>
							<cfset structAppend(result,application.com.relayImage.createThumbnail(targetSize=targetSize,sourceFile=fileSource,targetFilePath=targetFilePath,targetFileName=targetFileName,targetFileExtension=fileExtension))>

							<cfif result.isOK>
								<cfset updateRecord = true>
								<cfset result.filesUploaded = listAppend(result.filesUploaded,result.imageURL)>
							</cfif>
						<cfelse>
							<cfset cffile = application.com.fileManager.uploadFile(fileField="#fileSource#",destination=targetFilePath,nameConflict="Overwrite",accept=accept)>

							<cfif cffile.isOK>
								<!--- if fileextension hasn't been specified --->
								<cfif fileExtension eq "">
									<cfset fileExtension = cffile.clientFileExt>
								</cfif>
								<cffile action="rename" source="#targetFilePath#\#cffile.clientFileName#.#cffile.clientFileExt#" destination="#targetFilePath#\#targetFileName#.#fileExtension#">
								<cfset filename = "#targetFileName#.#fileExtension#">

								<cfif cffile.clientFileExt neq "pdf">
									<!--- if the file extension from the XML is provided and is different from that of the uploaded file, do the conversion (may as well keep both) --->
									<cfif fileExtension neq "" and fileExtension neq cffile.clientFileExt>
										<cfset application.com.relayImage.convertImage(sourcePath=targetFilePath,sourceName=fileName,targetName=fileName)>
									</cfif>

									<cfif structKeyExists(parametersStruct,"height") and structKeyExists(parametersStruct,"width")>
										<cfset application.com.relayImage.resizeImage(targetWidth=parametersStruct.width,targetHeight=parametersStruct.height,sourcePath=targetFilePath,sourceName=fileName,targetName=fileName)>
									</cfif>
								</cfif>

								<cfset result.filesUploaded = listAppend(result.filesUploaded,'/content#targetFileRelativePath#/#fileName#')>
								<cfset updateRecord = true>
							<cfelse>
								<cfset result.isOK=false>
								<cfset result.message = cffile.message>
							</cfif>
						</cfif>
					</cfif>
				</cfif>

				<cfset fileCount = fileCount + 1>
			</cfif>
		</cfloop>
	</cfif>


	<cfif updateRecord>
		<cfquery name="setLastUpdated" datasource="#application.siteDataSource#">
			update #entity# set lastUpdated=getDate(),
				lastUpdatedBy=#request.relayCurrentUser.usergroupID#,
				lastUpdatedByPerson=#request.relayCurrentUser.personID#
			where #GetPK.COLUMN_NAME# = <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="cf_sql_integer">
		</cfquery>
	</cfif>

	<cfreturn result>
</cffunction>

