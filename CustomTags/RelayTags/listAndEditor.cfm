<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			listAndEitor.cfm
Author:				SWJ/NH
Date started:		2006-08-09

Description:		This tag will create a listing screen using quesry from table object and a editor screen
					for the main entity that relates to the listing screen.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
23-Jun-08 SWJ  Added dateFormat and currencyFormat  to the attributes list
2011-03-02	NYB 	LHID5772 changed to pass attributes through as a collection
2011-03-02	WAB 	Continuing from NYB, corrected a couple of bugs and then decided that all the cfparams were superfulous if they were being passed as a collection so removed
					Removed all the excel shenanigans, since this is now done automatically by tfqo
					I do wonder whether we should delete the non tfqo parameters from the relayXMLeditor call (and vice versa)
2014-08-11	AXA		allow for listandeditor to work on partner portal
2014-09-10	RPW		Add Lead Screen on Portal
29-09-2014 SB Added div around code for styling
2014-10-06	RPW		Lead screen not saving and displaying message
2015-02-10	RPW		Leads - Added ability to customize leads form for customers.
2015/11/13	NJH	Jira Prod2015-231 A little re-work around the header. Default the heading when in listing mode.

Possible enhancements:


 --->


<cfparam name="attributes.topHead" type="string" default="">

<div id="listAndEditorContainer">

<!--- save the content for the xml to define the editor --->
<cfif not structKeyExists(attributes,"xmlSource")>
	Error: you must provide xmlSource of the Editor screen
	<CF_ABORT>
<cfelse>
	<cfset attributes.xmlSourceVar = attributes.xmlSource>
</cfif>

<cfif (isDefined("editor") and editor eq "yes")>

	<cfparam name="attributes.editorPostSaveIncludeFile" default="empty.cfm"> <!--- PPB 2010/08/19 P-LEN022 --->

	<!--- this is a parameter whose name changes, so have to do a special test --->
	<cfif structKeyExists (attributes,"editorPostSaveIncludeFile")>
		<cfset attributes.postSaveIncludeFile = attributes.editorPostSaveIncludeFile>
	</cfif>
	<!--- NJH 2013/02/20 - add any url variables to the attribute scope, as things like 'hideBackButton' get passed on the url --->
	<cfset structAppend(attributes,url,true)>

	<cfif isDefined("add") and add eq "yes">

		<CF_RelayXMLEditor
			editorName = "thisEditor"
			add="yes"
			thisEmailAddress = "relayhelp@foundation-network.com"
			attributeCollection=#attributes#
		>
	<cfelse>
		<CF_RelayXMLEditor
			editorName = "thisEditor"
			thisEmailAddress = "relayhelp@foundation-network.com"
			attributeCollection=#attributes#
		>
	</cfif>
<cfelse>

	<cfparam name="attributes.queryData">
	<cfparam name="attributes.numRowsPerPage" default="100">
	<cfparam name="attributes.openAsExcel" type="boolean" default="false">	<!--- NJH 2009/02/01 CR-LEX581 --->
	<cfparam name="attributes.cfmlCallerName" default="#listLast(cgi.script_name,'/')#">

	<!--- Grab the entityname from the xml if it is available --->
	<cfif isXml(attributes.xmlSourceVar)>
		<cfset editor =xmlSearch(attributes.xmlSourceVar,"//editor/")>
		<cfset entityName = editor[1].xmlAttributes.entity>
		<cfset application.com.request.setTopHead(pageTitle=application.com.relayEntity.getEntityType(entityTypeID=entityName).label &"s")>
	</cfif>

	<cfif attributes.TopHead is not "">
		<cf_include template="#attributes.TopHead#" checkIfExists=true>
	</cfif>

	<!--- NYB 2011-03-02 LHID5772: changed to pass attributes to attributeCollection - to make adding new attributes easier and less error prone --->
	<!--- STCR 2012-04-19 P-REL109: changed to prevent overwriting keyColumnURLList if it has been specified explicitly (ie if the user wants to specify links from more than one column --->
	<cfif not structKeyExists(attributes,"keyColumnURLList") or attributes.keyColumnURLList eq "">
		<cfset attributes.keyColumnURLList = "">
		<cfloop list="#attributes.keyColumnKeyList#" index="key">
			<cfset attributes.keyColumnURLList= listAppend(attributes.keyColumnURLList,"#attributes.CFMLCallerName#.cfm?editor=yes&#key#=")>
		</cfloop>
	</cfif>
	<cfset attributes.queryObject = attributes.queryData>
	<cfset structDelete (attributes,"queryData")>

	<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->
	<!--- 2014-10-06	RPW		Lead screen not saving and displaying message --->
	<!--- 2015-02-10	RPW		Leads - Added ability to customize leads form for customers. --->

	<cfif NOT request.relayCurrentUser.isInternal AND fileExists("#application.paths.code#\cftemplates\Modules\#attributes.cfmlCallerName#\listExternal.cfm")>
		<cfinclude template="/code/cftemplates/Modules/#attributes.cfmlCallerName#/listExternal.cfm">
	<cfelseif request.relayCurrentUser.isInternal AND fileExists("#application.paths.code#\cftemplates\Modules\#attributes.cfmlCallerName#\listInternal.cfm")>
		<cfinclude template="/code/cftemplates/Modules/#attributes.cfmlCallerName#/listInternal.cfm">
	<cfelse>
		<CF_tableFromQueryObject
			attributeCollection=#attributes#
		>
	</cfif>

</cfif>
</div>