<!--- �Relayware. All Rights Reserved 2014 --->
<!---
			Attribute 	: debug - optional

			Various code in the menu may be rendered if debug is listed.
--->

<CFIF IsDefined("attributes.debug")>
	<CFSET debug=true>
</CFIF>
<CFIF IsDefined("attributes.useShowonmenu")>
	<CFSET useShowonmenu = attributes.useShowonmenu>
<CFELSE>
	<CFSET useShowonmenu = true>
</CFIF>
<!---
	Attribute 	: elementTree - optional

	The element tree can be explicitly supplied as an attribute, if it is not
	and a query exists in the caller scope called "getVisibleElements", this is used.
--->
<CFSET sitedatasource = application.sitedatasource>
<CFIF not IsDefined("caller.getVisibleElements") and not IsDefined("attributes.elementTree")>
ERROR -	There is no element tree defined!
	<CF_ABORT>
<CFELSEIF IsDefined("caller.getVisibleElements") and not IsDefined("attributes.elementTree")>
	<CFSET getVisibleElements = caller.getVisibleElements>
<CFELSEIF not IsDefined("caller.getVisibleElements") and IsDefined("attributes.elementTree")>
	<CFSET getVisibleElements = attributes.getVisibleElements>
</CFIF>

<!---
			Attribute 	: type

			It is concievable that other methods of navigation may be called from here
			these would be referred by the attribute "type", the default being "HM"
--->
<CFIF not IsDefined("attributes.type") or (IsDefined("attributes.type") and attributes.type IS "HM")>

	<!--- Set Javascript attributes  related to menu type HM--->
	

	<CFIF IsDefined("attributes.MenuWidth")>
		<CFSET HM_GL_MenuWidth = attributes.MenuWidth>
	<CFELSE>
		<CFSET HM_GL_MenuWidth = 150>
	</CFIF>
	
	<CFIF IsDefined("attributes.FontFamily")>
		<CFSET HM_GL_FontFamily  = attributes.FontFamily>
	<CFELSE>
		<CFSET HM_GL_FontFamily  = "verdana,geneva,arial,helvetica,sans-serif">
	</CFIF>
	
	<CFIF IsDefined("attributes.FontSize")>
		<CFSET HM_GL_FontSize   = attributes.FontSize>
	<CFELSE>
		<CFSET HM_GL_FontSize   = "8">
	</CFIF>
	
	<CFIF IsDefined("attributes.FontBold")>
		<CFSET HM_GL_FontBold   = attributes.FontBold>
	<CFELSE>
		<CFSET HM_GL_FontBold   = "false">
	</CFIF>
	
	<CFIF IsDefined("attributes.FontItalic")>
		<CFSET HM_GL_FontItalic   = attributes.FontItalic>
	<CFELSE>
		<CFSET HM_GL_FontItalic   = "false">
	</CFIF>
	
	<CFIF IsDefined("attributes.FontColor")>
		<CFSET HM_GL_FontColor   = attributes.FontColor>
	<CFELSE>
		<CFSET HM_GL_FontColor   = "FFFFFF">
	</CFIF>
	
	<CFIF IsDefined("attributes.FontColorOver")>
		<CFSET HM_GL_FontColorOver   = attributes.FontColorOver>
	<CFELSE>
		<CFSET HM_GL_FontColorOver   = "000000">
	</CFIF>
	
	<CFIF IsDefined("attributes.BGColor")>
		<CFSET HM_GL_BGColor   = attributes.BGColor>
	<CFELSE>
		<CFSET HM_GL_BGColor   = "000000">
	</CFIF>
	
	<CFIF IsDefined("attributes.BGColorOver")>
		<CFSET HM_GL_BGColorOver   = attributes.BGColorOver>
	<CFELSE>
		<CFSET HM_GL_BGColorOver   = "FF9900">
	</CFIF>
	
	<CFIF IsDefined("attributes.ItemPadding")>
		<CFSET HM_GL_ItemPadding   = attributes.ItemPadding>
	<CFELSE>
		<CFSET HM_GL_ItemPadding   = 2>
	</CFIF>
	
	<CFIF IsDefined("attributes.BorderWidth")>
		<CFSET HM_GL_BorderWidth   = attributes.BorderWidth>
	<CFELSE>
		<CFSET HM_GL_BorderWidth   = 1>
	</CFIF>
	
	<CFIF IsDefined("attributes.BorderColor")>
		<CFSET HM_GL_BorderColor   = attributes.BorderColor>
	<CFELSE>
		<CFSET HM_GL_BorderColor   = "808080">
	</CFIF>
	
	<CFIF IsDefined("attributes.BorderStyle")>
		<CFSET HM_GL_BorderStyle   = attributes.BorderStyle>
	<CFELSE>
		<CFSET HM_GL_BorderStyle   = "solid">
	</CFIF>
	
	<CFIF IsDefined("attributes.SeparatorSize")>
		<CFSET HM_GL_SeparatorSize   = attributes.SeparatorSize>
	<CFELSE>
		<CFSET HM_GL_SeparatorSize   = 1>
	</CFIF>
	
	<CFIF IsDefined("attributes.SeparatorColor")>
		<CFSET HM_GL_SeparatorColor   = attributes.SeparatorColor>
	<CFELSE>
		<CFSET HM_GL_SeparatorColor   = "gray">
	</CFIF>
	
	<CFIF IsDefined("attributes.ImageSrc")>
		<CFSET HM_GL_ImageSrc   = attributes.ImageSrc>
	<CFELSE>
		<CFSET HM_GL_ImageSrc   = "/relayware/images/Widgets/tri.gif">
	</CFIF>
	
	<CFIF IsDefined("attributes.ImageSrcLeft")>
		<CFSET HM_GL_ImageSrcLeft   = attributes.ImageSrcLeft>
	<CFELSE>
		<CFSET HM_GL_ImageSrcLeft   = "/relayware/images/Widgets/tri.gif">
	</CFIF>
	
	<CFIF IsDefined("attributes.ImageSize")>
		<CFSET HM_GL_ImageSize   = attributes.ImageSize>
	<CFELSE>
		<CFSET HM_GL_ImageSize   = 5>
	</CFIF>
	
	<CFIF IsDefined("attributes.ImageHorizSpace")>
		<CFSET HM_GL_ImageHorizSpace   = attributes.ImageHorizSpace>
	<CFELSE>
		<CFSET HM_GL_ImageHorizSpace   = 2>
	</CFIF>
	
	<CFIF IsDefined("attributes.ImageVertSpace")>
		<CFSET HM_GL_ImageVertSpace   = attributes.ImageVertSpace>
	<CFELSE>
		<CFSET HM_GL_ImageVertSpace   = 2>
	</CFIF>
	
	<CFIF IsDefined("attributes.ChildOffset")>
		<CFSET HM_GL_ChildOffset   = attributes.ChildOffset>
	<CFELSE>
		<CFSET HM_GL_ChildOffset   = 2>
	</CFIF>
	
	<CFIF IsDefined("attributes.MenuStart")>
		<CFSET MenuStart   = attributes.MenuStart>
	<CFELSE>
		<CFSET MenuStart   = 123>
	</CFIF>
	
	<CFSET MenuNow=MenuStart>
	
	<CFIF IsDefined("attributes.MenuMove")>
		<CFSET MenuMove   = attributes.MenuMove>
	<CFELSE>
		<CFSET MenuMove   = 120>
	</CFIF>
	
	<CFIF IsDefined("attributes.MenuDrop")>
		<CFSET MenuDrop   = attributes.MenuDrop>
	<CFELSE>
		<CFSET MenuDrop   = 77>
	</CFIF>
	
	<CFIF IsDefined("attributes.Template")>
		<CFSET Template   = attributes.Template>
	<CFELSE>
		<CFSET Template   = "ElementTemplate.cfm">
	</CFIF>

	<CFINCLUDE TEMPLATE="/relayware/templates/RelayJSNav.cfm">
</CFIF>





