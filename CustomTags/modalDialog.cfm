<!---
File name:		modalDialog.cfm
Author:			NJH
Date started:	2014/06/17

Description:	Custom tag to contain all the functionality needed to display a link in a modal window. Currently based on fancy-box. Looks for links of class modal.

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:


 --->
<cfif thisTag.executionMode is "start">
	<cfparam name="attributes.type" default="">
	<cfparam name="attributes.afterClose" default="">
	<cfparam name="attributes.beforeClose" default="">
	<cfparam name="attributes.autoSize" default="false">
	<cfparam name="attributes.size" default="medium">
	<cfparam name="attributes.identifier" default=".modal">
	<cfparam name="attributes.locked" default="false">

	<cfif attributes.size eq "small">
		<cfset attributes.width = "30%">
		<cfset attributes.height = "30%">
	<cfelseif attributes.size eq "medium">
		<cfset attributes.width = "50%">
		<cfset attributes.height = "50%">
	<cfelse>
		<cfset attributes.width = "90%">
		<cfset attributes.height = "90%">
	</cfif>

	<cf_includeJavascriptOnce template="/javascript/lib/fancybox/jquery.fancybox.pack.js">
	<cf_includeCssOnce template="/javascript/lib/fancybox/jquery.fancybox.css">

	<!--- only include modal js once per identifier --->
    <cfparam name="request.modalDialogIncluded" default="#structNew()#">
    <cfif not structKeyExists(request.modalDialogIncluded,attributes.identifier)>
		<cfset validAttributes = "width,height,type,afterClose,autoSize,beforeClose,beforeShow,scrollOutside">

		<cfsetting enablecfoutputonly="true">
		<cfoutput>
			<cf_head>
			<script>
				jQuery(document).ready(function() {
					jQuery('#attributes.identifier#').fancybox({
					<cfloop list="#validAttributes#" index="attr">
						<cfif structKeyExists(attributes,attr) and attributes[attr] neq "">
						<cfset quote = "">
						<cfif not (isNumeric(attributes[attr]) or isBoolean(attributes[attr]) or left(attributes[attr],8) eq "function")><cfset quote = "'"></cfif>
						#attr#: <cfif listFindNoCase("width,height",attr)>(jQuery(window).width() < 992)?'90%':</cfif>#quote##attributes[attr]##quote#,
						</cfif>
					</cfloop>
						<!--- this needed to be set to view youtube in fancy box in the fileList relay tag. May need to remove it for other cases --->
						<cfif attributes.type eq "iframe">
						iframe : {
      						preload: false
						},
						</cfif>
						helpers: {
							overlay: {
							  locked: #attributes.locked#
							}
						}
					});
				});
			</script>
			</cf_head>
		</cfoutput>
		<cfsetting enablecfoutputonly="false">

    	<cfset request.modalDialogIncluded[attributes.identifier] = true>
    </cfif>
</cfif>