<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB  2010/06
An extension to CFLOG which automatically works out the milliseconds since 
		a) 	The first entry in the log for the current request
	and b)	The previous entry in the log for the current request

--->
<cfparam name="request.log" default= #structNew()#>
<cfset thisTickCount = getTickCount()>
<cfif not structKeyexists(request.log,attributes.file)>
	<cfset request.log[attributes.file] = {startTick = thisTickCount,lastLog = thisTickCount}>
</cfif>


<cfset timeSinceRequestStart =  thisTickCount - request.log[attributes.file].startTick>
<cfset timeSinceLastLog = thisTickCount - request.log[attributes.file].LastLog>
<cfset request.log[attributes.file].LastLog = thisTickCount> 

<cfset attributes.Text = timeSinceRequestStart & " " & timeSincelastLog & " " & attributes.text>

<cflog attributeCollection = #attributes#>
