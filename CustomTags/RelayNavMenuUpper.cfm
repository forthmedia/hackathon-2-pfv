<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NAME: <CF_RelayNavMenuUpper>

NOTES:
 Part of the <CF_RelayNavMenu> tag, see RelayNavMenu.CFM for details.

AUTHOR:
 Simon WJ 2001-10-28
--->

<!--- Associate this tag with <CF_RelayNavMenu> --->


<CFASSOCIATE BASETAG="CF_RelayNavMenu" DATACOLLECTION="menuUpper">

<CFPARAM NAME="attributes.current" TYPE="string" DEFAULT=" ">
<CFPARAM NAME="attributes.description" TYPE="string" DEFAULT=" ">

