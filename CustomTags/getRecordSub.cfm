<!--- �Relayware. All Rights Reserved 2014 --->
<!--- sub procedure of getRecord.cfm - can be called more than once 


Mods : 
2001-03-12		WAB		changed so that lastupdatedby name comes from the usergroup name not from person - overcomes problems if person does not exist
2001-03-15		WAB		Added createdbyname and lastupdatedby name to the query which creates new records - possible cause of a bug.
2001-05-15		WAB		Altered joins on queries (rather than using sub queries) to try and speed things up
2001-07-11		WAB		createdbyname and lastupdatedby name  were reversed (join wrong)
2005-03-31    	AR     	Added code to allow it to deal with Organisations correctly. 
2005-05-23		WAB 	Corrected a bug when dealing with organisations.  Happened to occur only when the organisation  in question had an id which wasn't the same as a locationid in the database
						Code ran OK, but caused problems later on in screen\qryscreendefinition.cfm
						I think that AR's change above isn't required now that I have fixed this, so I have commented it out
2013-07-09 		NYB 	Case 435968

--->


<CFQUERY NAME="getEntityType" DATASOURCE="#application.SiteDataSource#">				
SELECT * from flagEntityType
WHERE tableName =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<!--- is entityID numeric --->
<CFIF isNumeric(entityId)>
 <!--- existing Record --->
		<cfquery NAME="getRecord" datasource="#application.SiteDataSource#">	 
 		SELECT 
			#preserveSingleQuotes(getEntityType.NameExpression)# as name,
			ugu.name as lastupdatedbyName,
			ugc.name as createdbyName,
			<CFIF getEntityType.TableName is "person">l.countryid,</cfif>   
			<CFIF "location,Organisation,Person" contains getEntityType.TableName>(select count(1) from location where e.Organisationid = location.Organisationid) as NumberLocationInOrganisation,</cfif>   
			<CFIF "location,Organisation,Person" contains getEntityType.TableName>(select count(1) from person where e.Organisationid = person.Organisationid) as NumberPersonInOrganisation,</cfif>   
			<CFIF "location,Person" contains getEntityType.TableName>(select count(1) from person where e.locationid = person.Locationid) as NumberPersonInLocation,</cfif>   
	 	e.*
		from #getEntityType.TableName# as e
		<CFIF getEntityType.TableName is "person">
		inner join 
			location l on l.locationid = e.locationid
		</cfif>
		left outer join usergroup ugc on e.createdby = ugc.usergroupid
		left outer join usergroup ugu on e.lastupdatedby = ugu.usergroupid
		where e.#getEntityType.UniqueKey# =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>


<CFELSE>
 <!--- new Record --->

	<!--- need to have a countryid for person, location, organisation to pull out correct default values--->
	<CFIF isDefined("attributes.locationid")>
		<CFSET 	locationid  = attributes.locationid>
		<CFSET person_locationID_default = locationid>
	<CFELSEIF isDefined("caller.person_locationID_default")>
		<CFSET 	locationid  = caller.person_locationID_default>
	<CFELSEIF entityType is "Person" >
		LocationID required
	</CFIF>


	<CFIF isDefined("attributes.countryid")>
		<CFSET countryid = attributes.countryid>
	<CFELSEIF isDefined("caller.location_countryid_default")>
		<CFSET countryid = caller.location_countryid_default>
	<CFELSEIF isDefined("caller.organisation_countryid_default")>
		<CFSET countryid = caller.organisation_countryid_default>
	<CFELSEIF isDefined("locationid") and isNumeric(locationid)>
		<cFQUERY NAME="getCountry" DATASOURcE="#application.SiteDataSource#">	
		SELECT countryID from location where locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>	
		<CFSET countryID = getCountry.CountryID>
	<CFELSEIF isDefined("organisationid") and isNumeric(organisationid)>
		<cFQUERY NAME="getCountry" DATASOURcE="#application.SiteDataSource#">	
		SELECT countryID from organisation where organisationid =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>	
		<CFSET countryID = getCountry.CountryID>
	<!--- START: 2013-07-09 NYB Case 435968 added --->
	<cfelseif isdefined(frmCountryID)>
		<CFSET countryID = frmCountryID>
	<!--- END: 2013-07-09 NYB Case 435968 added --->
	<CFELSE>
		<CFOUTPUT>CountryID required.!! </CFOUTPUT>
		
	</CFIF>
		
		<cfif isDefined("countryid")>
		<CFSET location_countryID_default = countryid>
		</cfif>

	<CFIF isDefined("attributes.organisationid")>
		<CFSET location_organisationid_default = attributes.organisationid>
	</CFIF>



	<!--- get Default values for this record ---> 
		<CFSET tableName = entityType>
		<CFINCLUDE template = "getDefaultValues.cfm">
	
	<!--- get definition of table --->	
		<CFSET tablerequired = entityType>
		<CFINCLUDE template="screen\qryGetTableDefinition.cfm">


	<!--- loop round definition creating a record --->
		<cFQUERY NAME="getRecord" DATASOURcE="#application.SiteDataSource#">
			SELEcT  '#entityID#' as #entityType#ID,  <!--- for some reason which I cannot remember, the getTableDefinition excludes the ID field --->
				<cFLOOP index="thisField" list="#AllFields#" >
					<CFSET value = ''>
					<CFIF isdefined("#entityType#_#thisField#_Default")>
						<CFSET value = evaluate("#entityType#_#thisField#_Default")>
					<CFELSEIF isdefined("caller.#entityType#_#thisField#_Default")>
						<CFSET value = evaluate("caller.#entityType#_#thisField#_Default")>
					</CFIF>
					'#value#' as #thisField#,
				</cFLOOP>	
					1 as #entityType#active,
					'' as #entityType#lastupdated,
					'' as #entityType#lastupdatedby,
					'' as lastupdatedbyName,
					'' as CreatedbyName
					<CFIF entityType is "Person">, #countryid# as countryid</cfif>
				FROM DUAL		
		</cFQUERY>
	

</cfif>

<CFSET "#entityType#" = getRecord>
<CFSET "caller.#entityType#" = getRecord>
<!--- AR 2005-03-31 Added code to allow it to deal with Organisations. --->
<!--- 
WAB 2005-05-23 removed, see comment above
<cfparam name="continueRecursion" default="true"> --->

<CFIF isDefined("attributes.getParents")>
	<CFIF entityType is "Person">
		<CFSET entityType = "Location">
		<CFSET entityID = "#getRecord.locationid#">
		<CFINCLUDE template = "getRecordSub.cfm">
	<CFELSEIF entityType is "Location" <!--- and continueRecursion --->>
		<!--- 		<cfset continueRecursion = false> --->		
		<CFSET entityType= "Organisation" >
		<CFSET entityID = "#getRecord.Organisationid#">
		<CFINCLUDE template = "getRecordSub.cfm">
	<!--- 	
	WAB 2005-05-23 commented this out - organisations don't have any parents, not sure why it is here , but causes problems later on!
	<CFELSEIF entityType is "Organisation" and continueRecursion>
		<cfset continueRecursion = false>
		<CFSET entityType = "Location">
		<cfif isdefined("attributes.locationid")>
			<CFSET entityID = attributes.locationid>
		</cfif>
		<CFINCLUDE template = "getRecordSub.cfm">
	 --->	
	
	</CFIF>
</CFIF>
