<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			mail.cfm	
Author:				WAB
Date started:		2009-01-14
	
Description:			
Custom Tag to process all mail sent by relay and prevent emails going to non fnl addresses on dev boxes (test site <> 0)

Has child tags which cf_mailparam and cf_mailpart
which deal with cfmailparam and cfmailpart

Some related code in email.cfc

On Live sites this code 

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-11-07	IH	Case 431724 Set thisTag.generatedContent to empty string before the cfmail tag

Possible enhancements:


 --->
 
 
 

 <cfif thistag.executionMode is "end">

	<!--- Do the addresses need to be doctored (usually just a test on application.testsite, but could be modified --->
	<cfif application.com.email.doEmailsNeedToBeRedirected()>
		<cfset oldAttributes = structNew()>
		<!--- loop through the to,bcc,cc addresses and doctor them --->
		<cfloop index="field" list = "to,bcc,cc">
			<cfif structKeyExists (attributes,field)>
				<!--- use function to alter all the addresses --->
				<cfset oldAttributes[field] = attributes[field]> <!--- just for debug really --->
				<cfset attributes[field] = application.com.email.doctorEmailAddressListForRedirection(attributes[field])>			
			</cfif>
		</cfloop>
	</cfif>

	<!--- This is the actual cfmail tag, all the attribute passed in to cf_mail are passed on to cfmail 
	Note use of CFSILENT to prevent randoom spaces in email (not a problem in HTML mails, but a pest in Text)
	--->
	<!--- 
	WAB 2012-03-12 CASE 426955 remove excess whitespace from HTML Emails - for Lotus Notes.  Bug raised against two specific comms emails, but this solution generally applicable 	
	--->
	<cfset generatedContent = thisTag.GeneratedContent>
	<cfif structKeyExists (attributes,"Type") and attributes.Type is "HTML">
		<cfset generatedContent=application.com.regExp.removeWhiteSpace(generatedContent)>	
	</cfif>
	
	<cfset thisTag.generatedContent = "">

	<cfmail attributeCollection = "#attributes#"><cfsilent>
		<cfif structKeyExists(thisTag,"mailParams")>
			<cfloop index = "i" from = 1 to=#arrayLen(thisTag.mailParams)# >
				<cfmailParam attributeCollection = #thisTag.mailParams[i]#>
			</cfloop>
		</cfif>
		</cfsilent><cfif structKeyExists(thisTag,"mailParts")><cfloop index = "i" from = 1 to=#arrayLen(thisTag.mailParts)# ><cfset content = thisTag.mailParts[i].generatedCOntent><cfif thisTag.mailParts[i].type is "HTML"><cfset content=application.com.regExp.removeWhiteSpace(content)></cfif><cfmailPart type = #thisTag.mailParts[i].type#>#content#</cfmailPart></cfloop></cfif>#generatedContent#
		</cfmail>



	<cfif structKeyExists(attributes,"debug")>
		<cfset params = application.com.email.getCurrentEmailRedirectionParameters()>
		
		<cfoutput><table>
				<tr><td>Allowed EmailRegular Expression</td><td></td><td>#htmlEditFormat(params.allowedEmailAddressRegExp)#</td></tr>
				<tr><td>Redirect to Email</td><td></td><td>#htmlEditFormat(params.replacementEmailAddress)#</td></tr>
			<cfloop index="field" list = "to,bcc,cc">
				<cfif structKeyExists (attributes,field)>
					<tr><TD>#htmlEditFormat(oldAttributes[field])#</TD><TD>---></TD><TD>#htmlEditFormat(Attributes[field])#</TD></tr>	
				</cfif>
			</cfloop>	
		</table></cfoutput>
	</cfif>
	
 </cfif>



 
