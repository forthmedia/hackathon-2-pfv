<!--- �Relayware. All Rights Reserved 2014 --->

<!---  ************************************
				   Encrypt URL
	   ************************************
		
		David A McLean
		22 July 2002
		

--->

<CFIF IsDefined("attributes.Seed")>
	<CFSET Seed= attributes.Seed>
<CFELSE>
	No Seed supplied
	<CF_ABORT>
</CFIF>

<CFIF isDefined("attributes.Method")>
	<CFSET Method = attributes.Method>
	<CFIF Method IS "Decrypt">
		<CFIF IsDefined("attributes.queryString")>
			<CFIF len(attributes.queryString) GT 0>
				<CFSET queryString = attributes.queryString>
			<CFELSE>
				Zero length queryString for Method Get
			</CFIF>
		<CFELSE>
			No queryString for Method Get
			<CF_ABORT>
		</CFIF>
		<CFSET qStrIn = decrypt(URLDecode(queryString),Seed)>

		<CFSET NameValuePairs=qStrIn>
		<CFSET delimiter = "&">
		<CFPARAM NAME="Attributes.mode" default = "set">
		<CFLOOP list="#NameValuePairs#" index="thispair" delimiters="#delimiter#">
			<CFIF listlen(thisPair,"=") GT 1>   
				<cfset variablename = listfirst(thispair,"=")>
				<cfset value = listrest(thispair,"=")>
				<CFIF attributes.mode is "set">
					<CFSET "caller.#variablename#" = value>
				<CFELSEIF attributes.mode is "param">
					<CFPARAM Name="caller.#variablename#" default="#value#">
				<CFELSEIF attributes.mode is "null">
					<CFSET "caller.#variablename#" ="">
				<CFELSE>
					<CFOUTPUT>Invalid Mode Setting #htmleditformat(mode)#</cfoutput><CF_ABORT>
				</cfif>
			</cfif>
	
		</CFLOOP> 
		
		
	<CFELSEIF Method IS "Encrypt">
		<CFIF IsDefined("attributes.queryString")>
			<CFIF len(attributes.queryString) GT 0>
				<CFSET queryString = attributes.queryString>
			<CFELSE>
				Zero length queryString for Method Send
			</CFIF>
		<CFELSE>
			No queryString for Method Send
			<CF_ABORT>
		</CFIF>
		<CFSET caller.queryStringEnc = URLEncodedFormat(encrypt(queryString,Seed))>
	<CFELSE>
		No Method
		<CF_ABORT>
	</CFIF>
</CFIF>


