<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		scheduledTask.cfm
Author:			NJH
Date started:	13-10-2008

Description:	This is a tag that logs information about the scheduled task that is run. Any scheduled task that is run
				should have the code wrapped with this tag. A schedule result struct is defined in the caller which can be used
				to store any result that the user may want. One of the keys of this structure is 'isOk', that if set to false,logs the entry
				as an error. For example, if we're expecting something that we didn't get, then set scheduleResult.isOk = false.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2009/02/24			NJH			Bug Fix All Sites Issue 1890 - Added the ? between script name and queryString in the attributes.url. This was causing duplicate entries getting
								logged in the scheduledTaskDefinition as teh code didn't think that the url already existed.
2010/03/23			NJH			LID 3154
2011/04/04			NYB			LID5822: Add a relevant errorid
2011-05-19			NJH			8.3.1 Added locking mechanism.
2015-06-22 			WAB			Added support for onErrorEMailTo attribute.  This removed a reason for putting a cftry around the outside of cf_scheduledTask, which had led to processLocks not being deleted
								Also changed way that errors are recorded back to scheduledTaskLog table.
								scheduledTaskLog.ErrorID is now only updated on an uncaught error.
								If there are caught errors/warnings during the running of the scheduled task then the IDs are recorded in request.scheduledTask.errorsLogged array
								These Ids are then put into the result structure
2015-07-02			WAB 		maximumExpectedLockMinutes (note needs db change)

Possible enhancements:


 --->

<cfparam name="attributes.name" type="string" default="#listFirst(listLast(cgi.SCRIPT_NAME,'/'),'.')#"> <!--- use the name of the page that is running as the name of the scheduled task --->
<!--- NJH 2010/03/23 LID 3154 - if query string is empty, don't add ? --->
<cfparam name="attributes.url" type="string" default="#cgi.SCRIPT_NAME##IIF(request.query_string neq "",DE('?'),DE(''))##request.query_string#"> <!--- NJH 2009/02/24 Bug Fix All Sites Issue 1890 - added ?--->
<cfparam name="attributes.lock" type="boolean" default="false">
<cfparam name="attributes.ignoreLock" type="boolean" default="false">
<cfparam name="attributes.onErrorEmailTo" type="string" default="">
<cfparam name="attributes.maximumExpectedLockMinutes" type="numeric" default="120">


<cfset parentTags = listrest(getbasetaglist())>

<cfif not listFindNoCase(parentTags,"cf_scheduledTask")>

	<cfif not thistag.HasEndTag>
		<cfthrow message="CF_scheduledTask requires an end tag.">
	</cfif>

	<!--- forces the errorHandler_exception.cfm file to run --->
	<cfset request.relayCurrentUser.showErrors = false>

	<cfif ThisTag.ExecutionMode IS 'start'>

		<cfif attributes.onErrorEmailTo is not "">
			<cfset application.com.errorHandler.setOnError(emailTo = attributes.onErrorEmailTo)>
		</cfif>

		<!--- NJH 2013/03/12 Case 433498 - set scheduledTask Person --->
		<cfset application.com.globalFunctions.setScheduledTaskAsCurrentUser()>

		<!--- this structure is used to store the results of the processing. It it logged and then sent in an email --->
		<cfif not structKeyExists(caller,"scheduleResult")>
			<cfset caller.scheduleResult = {isOk = true, server=cgi.server_name}>
		</cfif>

		<cfset queryString = request.query_string neq ""?"?#request.query_string#":"#request.query_string#">
		<cfset scriptAndQueryString = "#cgi.SCRIPT_NAME##queryString#">

		<cfset cfAdminTasksQry = application.com.relayAdmin.getCFScheduledTasks()>

		<cfquery name="getScheduledTaskData" dbtype="query">
			select * from cfAdminTasksQry where url like '%#scriptAndQueryString#' and url like '%#cgi.HTTP_HOST#%'
				and status='Running' <!--- paused <> 1 and disabled <> 1 --->
		</cfquery>

		<cfif getScheduledTaskData.recordCount>
			<cfset attributes.url=getScheduledTaskData.url>
			<cfset cfTaskName=getScheduledTaskData.task>
			<cfset lockName = cfTaskName>
		<cfelse>
			<cfset cfTaskName="">
			<cfset lockName = attributes.name> <!--- shouldn't really get in here if the scheduled task has been set up correctly --->
		</cfif>

		<!--- 2012-09-19 WAB changes to setProcessLock function, now returns whether lock successful --->
		<cfset caller.scheduleResult.scheduledTaskLocked = false>
		<cfif attributes.lock>
			<cfset setLock = application.com.globalFunctions.setProcessLock(lockname=lockName, onErrorEmailTo = attributes.onErrorEmailTo , maximumExpectedLockMinutes = attributes.maximumExpectedLockMinutes)>
			<cfif setLock.timedOut and not attributes.ignoreLock>
				<cfset caller.scheduleResult.scheduledTaskLocked = true>
				<cfoutput>The process #lockName# is already running</cfoutput>
				<cfexit method="exitTag">
			</cfif>
		</cfif>

		<!--- create a new schedule task if it doesn't already exist --->
		<cfquery name="getScheduledTaskDefID" datasource="#application.siteDataSource#">
			<!--- if not exists (select 1 from scheduledTaskDefinition where name='#attributes.name#') --->
			if not exists (select 1 from scheduledTaskDefinition where url  like  <cf_queryparam value="%#scriptAndQueryString#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					insert into scheduledTaskDefinition (name,url,cfTaskName,created,createdBy,lastUpdated,lastUpdatedBy)
					values (<cf_queryparam value="#attributes.name#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#attributes.url#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#cfTaskName#" CFSQLTYPE="CF_SQL_VARCHAR" >,GetDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,GetDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >)

					select scope_identity() as scheduledTaskDefID
				end
			else
				begin
					<!--- select scheduledTaskDefID from scheduledTaskDefinition where name='#attributes.name#' --->
					select scheduledTaskDefID from scheduledTaskDefinition where url  like  <cf_queryparam value="%#scriptAndQueryString#" CFSQLTYPE="CF_SQL_VARCHAR" >

					<cfif cfTaskName neq "">
					declare @name varchar
					select @name= cfTaskname from scheduledTaskDefinition where url  like  <cf_queryparam value="%#scriptAndQueryString#" CFSQLTYPE="CF_SQL_VARCHAR" >
					if @name <>  <cf_queryparam value="#cfTaskName#" CFSQLTYPE="CF_SQL_VARCHAR" >  or @name is null
					begin
						update scheduledTaskDefinition set cfTaskName =  <cf_queryparam value="#cfTaskName#" CFSQLTYPE="CF_SQL_VARCHAR" >  where url  like  <cf_queryparam value="%#scriptAndQueryString#" CFSQLTYPE="CF_SQL_VARCHAR" >
					end
					</cfif>
				end
		</cfquery>

		<cfset scheduledTaskDefID = getScheduledTaskDefID.scheduledTaskDefID>

		<cfset runByScheduler = 0>
		<cfif cgi.HTTP_USER_AGENT eq "CFSCHEDULE">
			<cfset runByScheduler = 1>
		</cfif>

		<cfquery name="insertScheduledTaskLog" datasource="#application.siteDataSource#">
			insert into scheduledTaskLog (scheduledTaskDefID,startTime,parameters,RunByScheduler,created,createdBy,lastUpdated,lastUpdatedBy)
			values (<cf_queryparam value="#scheduledTaskDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,GetDate(),<cf_queryparam value="#request.query_string#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#runByScheduler#" CFSQLTYPE="CF_SQL_bit" >,GetDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,GetDate(),<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >)

			select scope_identity() as scheduledTaskLogID
		</cfquery>

		<cfset request.scheduledTask = {scheduledTaskLogID = insertScheduledTaskLog.scheduledTaskLogID, errorsLogged = arrayNew(1)}>

	</cfif>

	<!--- End of form layout --->
	<cfif ThisTag.ExecutionMode IS 'end'>

		<!--- <cfwddx action="cfml2wddx" input="#caller.scheduleResult#" output="wddxScheduleResult"> --->
		<cfif arrayLen (request.scheduledTask.errorsLogged)>
			<cfset caller.scheduleResult.errorsLogged = request.scheduledTask.errorsLogged>
		</cfif>

		<!--- updating the log --->
		<cfquery name="updateScheduledTaskLog" datasource="#application.siteDataSource#">
			update scheduledTaskLog
			set endTime = GetDate(),
				duration = dateDiff(second,startTime,GetDate()),
				result =  <cf_queryparam value="#serializeJSON(caller.scheduleResult)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<!--- 2011/04/04 NYB LID5822: Add a relevant errorid --->
				<cfif not caller.scheduleResult.isOk><cfif structkeyexists(caller.scheduleResult,"errorID")>errorID = #caller.scheduleResult.errorID#<cfelse>errorID = 0</cfif>,</cfif>
				lastUpdated = GetDate(),
				lastUpdatedBy = #request.relayCurrentUser.userGroupId#
			where scheduledTaskLogID =  <cf_queryparam value="#request.scheduledTask.scheduledTaskLogID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif not runByScheduler>
			<cfdump var="#caller.scheduleResult#">
		</cfif>

		<cfif attributes.lock>
			<cfset application.com.globalFunctions.deleteProcessLock(lockName=lockName)>
		</cfif>
	</cfif>
</cfif>
