<!---
  Created by martin.elgie on 15/11/2016.
  Launches a request to an OAuth2 Authorisation Server
--->

<CFPARAM NAME="attributes.OAuth2ID" type="string">
<CFPARAM NAME="attributes.autolaunch" type="boolean" default="false">
<CFPARAM NAME="attributes.launchButtonText" type="string" default="phr_Login">
<CFPARAM NAME="attributes.autoLaunchDelay" type="numeric" default="0"> <!--- If autolaunching how long it will wait for (used in the redirect page) --->
<CFPARAM NAME="attributes.serverManager" default=#createObject("component","singleSignOn.common.editorBackingBean")#>
<CFPARAM NAME="attributes.relayState" default="#structKeyExists(url, 'urlrequested')?url.urlrequested:''#">
<CFPARAM NAME="attributes.beginWorkflow" type="string" default="">

<cfscript>

    httpHelper = new singleSignOn.OAuth2.client.CFHttpHelper().init();
    filter = new singleSignOn.OAuth2.client.OAuth2AuthenticationFilterPlugin();


    OAuthServer=attributes.serverManager.retrieveServer(attributes.OAuth2ID);
    state=structNew();
    state.url=httpHelper.protocol & cgi.server_name & httpHelper.getUrlRequested();
    state.beginWorkflow=attributes.beginWorkflow;
    state.server=OAuthServer.getClientID();

    stateString=filter.encryptState(state);

    if (structKeyExists(form, "serverID") and attributes.OAuth2ID eq form.serverID and form.auto eq attributes.autolaunch) {
        variables.callbackURL = "#httpHelper.callbackURL#";

        if (httpHelper.checkHttps(callbackURL) AND 
            httpHelper.checkHttps(OAuthServer.getAuthoriseURL()) AND 
            httpHelper.checkHttps(OAuthServer.getDataURL()) AND 
            httpHelper.checkHttps(OAuthServer.getTokenURL())) {

            cookie.OAuth2LastID=attributes.OAuth2ID;
            cookie.OAuth2StateToken=filter.generateRandomToken();
            state.token=cookie.OAuth2StateToken;
            state.autoLaunch=attributes.autolaunch;
            stateString=filter.encryptState(state);

            authUrl="#OAuthServer.getAuthoriseURL()##filter.getNoRequestAuthParameters(serverIn=OAuthServer, state=stateString)#";
            location(authUrl, false);
        } else {
            errorStruct=structNew();
            errorStruct.explanation="One or more URLs is not secure (https is required).";
            errorStruct.server=OAuthServer.getServerName();
            errorStruct.callbackURL=variables.callbackURL;
            errorStruct.authorisationURL=OAuthServer.getAuthoriseURL();
            errorStruct.tokenURL=OAuthServer.getTokenURL();
            errorStruct.dataURL=OAuthServer.getDataURL();
            errorId=application.com.errorHandler.recordRelayError_Warning(type="OAuth2AuthenticationError",Severity="error",WarningStructure=errorStruct);
            writeOutput("<p>phr_OAuth2_SSOError</p>");
            writeOutput("Error ID: #errorId#");
        }
    }

</cfscript>

<cfif NOT structKeyExists(variables,"callbackURL")> <!-- Avoid getting stuck in a loop-->
<cfoutput>
    <!---Make the request to /oauth/initiate--->
    <form action="" id="OAuthRequest" method="post">
        <input type="hidden" name="RelayState" value="#stateString#"/>
        <input type="hidden" name="serverID" value="#attributes.OAuth2ID#"/>
        <input type="hidden" name="auto" value="#attributes.autolaunch#">
        <cfif not attributes.autolaunch>
            <cfoutput>
                    <input class="form-control btn btn-primary" name="signInWithRelayware" value="#attributes.launchButtonText#" id="signInWithRelayware" type="submit">
            </cfoutput>
        </cfif>
    </form>

    <cfset validServersArray = attributes.serverManager.getValidServers()>
    <cfif attributes.autolaunch 
            AND arrayLen(validServersArray) is 1 
            AND validServersArray[1].clientID eq OAuthServer.clientId>
        <script>
        setTimeout(function() {
            jQuery('##OAuthRequest').submit();
        },#attributes.autoLaunchDelay*1000#);
        </script>
    </cfif>
</cfoutput>
</cfif>