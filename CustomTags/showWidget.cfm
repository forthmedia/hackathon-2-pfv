<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="attributes.widgetData" default="">
<cfparam name="attributes.widgetDisplay" default="">
<cfparam name="attributes.displayRecords" default="5">
<cfparam name="attributes.widgetParams" default="">

<cfinvoke component="#application.com.relayhomePageWidgetData#" method="Get#attributes.widgetData#ListData" returnvariable="wData" widgetParams="#attributes.widgetParams#">

<cfswitch expression="#attributes.widgetDisplay#">
	<cfcase value="List">
		<cfif isDefined("wData") and wData.recordCount neq 0>
			<cf_widgetListDisplay widgetData="#wData#" displayRecords="#attributes.displayRecords#">
		</cfif>
	</cfcase>
</cfswitch>