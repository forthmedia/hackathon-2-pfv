<!--- �Relayware. All Rights Reserved 2014 --->
<!--- This custom tag removes duplicate entries from a list 

By William Bibby
2000-03-27

Parameters:
List   		the list to be deduped
variable 	variable to return the new list

example: 	<CF_ListUnique list = "d,a,d,a,ew,we,ew" variable = "example">
			returns  example="a,d,e,ew,we"

--->

<CFPARAM name="attributes.list">
<CFPARAM name="attributes.variable">


<cfset newList = "">

<cfloop index="thisItem" List="#attributes.list#">

	<cfif listfindnocase (newList,thisItem) is 0>
		<cfset newList = listappend(newList,thisItem)>
	</cfif>

</cfloop>

<!--- 	<!--- convert the list to an array and then sort it --->
	<CFSET temp = listToArray (attributes.list)>
	<CFSET x = arraySort(temp,"textnocase")>

	<CFSET newList = "">


		<!--- loop through this array, discarding any dupes --->	
		<CFLOOP index="I" FROM = 1 to = #arrayLen(temp)#>
			<CFIF listLast (newList) IS NOT temp[i]>
				<CFSET newlist = listAppend(newList,temp[i])>
			</cfif>
		</cfloop>
 --->
<CFSET "caller.#attributes.variable#" = newList>
