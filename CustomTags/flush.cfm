<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2012-06-15
CF_FLUSH
To be used instead of CFFLUSH
Adds the DOCTYPE and other starting tags before the flush is done, otherwise the request falls over at the end when we try to add these tags and call cfhtmlhead
 --->
<cfsetting enablecfoutputonly="true">
<cfif thisTag.executionMode is "start">
	<cfset content = application.com.request.getGeneratedContent()>
	<cfset content = application.com.request.postProcessContentString(content)>
	<cfif not application.com.request.haveHTMLDocumentOpeningTagsBeenAdded()>
		<cfset content = application.com.request.addHTMLDocumentOpeningTags(content)>
	</cfif>
	<cfset getPageContext().getOut().clearBuffer()>
	<cfoutput>#content#</cfoutput> 
	<cfflush>
</cfif>
<cfsetting enablecfoutputonly="false">