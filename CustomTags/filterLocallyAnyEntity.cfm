<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			filterLocallyAnyEntity.cfm
Author:				RJT
Date started:		14/03/2016

Description:		Similar to filterSelectAnyEntity in that an interface for selection is created, but it operates locally, creating a json structure which can be later interpreted.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->

<cfset this.serverSideFormValidation = false>

<cf_includejavascriptonce template = "/javascript/filterLocallyAnyEntity.js">

<!--- details about the entity being filtered. E.g. if a activity score rule is being filtered based on the person who obtained the activity the filteredByTypeID is person and the filteredEntityTypeID is activityScoreRule. You can  --->
<CFPARAM NAME="attributes.filteredByTypeID" TYPE="numeric">
<CFPARAM NAME="attributes.currentFilter" TYPE="string"> <!---The json structure that is created to hold filters --->
<cfparam name="attributes.name"  TYPE="string"> <!--- what the hidden field that contains the filter information is called--->
<cfset attributes.tablename = application.entityType[attributes.filteredByTypeID].TABLENAME>

<!--- get all the fields available to the entity --->

<cfset oEntityFields = application.com.filterSelectAnyEntity.getCurrentEntityFields(entityTypeID=attributes.filteredByTypeID,tablename=attributes.tablename)>

<cfoutput>


	<cfif attributes.currentFilter EQ "">
		<cfset attributes.currentFilter=application.com.filterSelectAnyEntity.getStarterFilter(attributes.filteredByTypeID)>
	</cfif>



	<div id="filterContainer">
		<cfset aStructSorted=structKeyArray(oEntityFields)>
		<cfset arraySort(aStructSorted,'textNoCase')>

		<input type="hidden"  name="#attributes.name#" value="#htmlEditFormat(attributes.currentFilter)#" id="#attributes.name#"  >
		<table id="#attributes.name#_filterTable" class="table table-hover table-striped table-bordered">
			<!--- select field to filter on --->
			<tr>
				<th>Field</th><th>Operator</th><th>Comparison</th><th>Add Delete</th>
			</tr>


			<cfset currentFilters = application.com.filterSelectAnyEntity.interpretCurrentFilterForDisplay(currentFilterJSON=attributes.currentFilter)>
			<!--- output current filters --->
			<cfloop query="currentFilters">
				<tr>
					<td>#HTMLEditFormat(field)#</td>
					<td>#HTMLEditFormat(operator)#</td>
					<!---2016/12/08 GCC 453488 - COMPARATOR can be an array with one of / not one of operators --->
					<cfif isarray(COMPARATOR)>
						<td>#HTMLEditFormat(ArrayToList(COMPARATOR, ","))#</td>
					<cfelse>
						<td>#HTMLEditFormat(COMPARATOR)#</td>
					</cfif>
					<td><a href="##" onclick="deleteFilter('#attributes.name#',jQuery(this).closest('tr').index())" class="btn btn-primary">phr_delete</a></td>
				</tr>
			</cfloop>
			<!--- load filterEditor --->
			<tr id="#attributes.name#_filterEditor">
				<!--- field --->
				<td id="#attributes.name#_filterFieldContainer">
					<select id="#attributes.name#_filterField" name="filterField" class="filterField" onchange="loadOperator(jQuery('###attributes.name#_operatorContainer'),jQuery('###attributes.name#_comparisonContainer'),this.value,#attributes.filteredByTypeID#)">
						<option value="">phr_ext_selectavalue</option>
						<cfloop array="#aStructSorted#" index="i" >
							<option value="#i#">#oEntityFields[i].FIELDDESCRIPTOR.NAME#</option>
						</cfloop>
					</select>
				</td>

				<!--- operator --->
				<td id="#attributes.name#_operatorContainer">

				</td>

				<!--- comparison --->
				<td id="#attributes.name#_comparisonContainer">

				</td>

				<!--- add --->
				<td id="#attributes.name#_comparisonAdd">
					<a href="##" onclick="addFilter('#attributes.name#')" class="btn btn-primary">phr_add</a>
					<div id="#attributes.name#_operatorMissingError" class="validationErrorMessage" style="display: none;">phr_filter_emptyDataNotPermitted</div>
				</td>
			</tr>
		</table>
	</div>
</cfoutput>