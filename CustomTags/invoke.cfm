<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2012-01-18

After problems with speed of CFINVOKE when making calls across our clusters, 
I ended up writing this version which uses cfhttp

 --->

<cfif thisTag.executionMode is "start">

	<cfif not structKeyExists(attributes,"webservice")>
		<cfoutput>
			This tag can only handle invoking webservices. 
			<BR>Probably only coldFusion ones!
		</cfoutput>
		<CF_ABORT>
	</cfif>

	<cfparam name="attributes.timeout" default="1">
	<cfparam name="attributes.returnVariable">
	
	<!--- take a copy of the attributes to pass through to webservice - WDDX needs to be of struct type rather than attributeCollection type--->
	<cfset argumentCollection = duplicate(attributes)>

	<!--- delete a few items which we do not want to pass through --->
	<cfloop list="timeout,returnvariable,webservice,method" index="index">
		<cfset structDelete (argumentCollection,index)>		
	</cfloop>

	<!--- make the CFHTTP call --->
	<cfhttp url="#replace(attributes.webservice,"?wsdl","")#" timeout="#attributes.timeout#" >
		<!--- seem to neeed wsdl and method as separate url variables, 
			but others can be in form of an argumentCollection --->
		<cfhttpparam type="URL" name="wsdl" value="">
		<cfhttpparam type="URL" name="method" value="#attributes.method#">

		<cfwddx action="cfml2wddx" input="#argumentCollection#" output="argumentCollectionWDDX">
		<cfhttpparam type="URL" name="argumentCollection" value="#argumentCollectionWDDX#">

	</cfhttp>

	<cfset urlCalled = attributes.webservice & "&argumentCollection=#argumentCollectionWDDX#&method=#attributes.method#" >
	<cfset caller.cf_invoke = {urlCalled = urlCalled}>

	<cfset statusCode = left(cfhttp.statuscode,3)>

	<cfif statusCode is "200" and isxml(cfhttp.filecontent)>
			<cfwddx action="wddx2cfml" input="#cfhttp.filecontent#" output="result">
	<cfelse>
		<cfif statuscode  is 404>
			<cfset errorDetail = "Error: 404 Not Found.">
		<cfelseif statuscode  is 408>
			<cfset errorDetail = "Error: 408 Read timed out.">
		<cfelse>
			<cfset errorDetail = "Error: #cfhttp.statuscode# #cfhttp.filecontent#">
		</cfif>

		<cfthrow detail="#errordetail#">
	</cfif>

	<cfset caller[attributes.returnVariable] = result>

</cfif>