<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Custom Tag for setting and unsetting flags 
Author WAB

Date:	many moons ago

Purpose:  For setting flags for a list of entities

History:  It works, but needs development.  
		Basically it uses all the existing update code in flagtask.cfm 
		It checks for rights (not sure how well) and then sets the variables required by flagtask.cfm
		Not very efficient		

Parameters
	Required Parameters
		List of entity ids  EntityID - Can be in the form of a select Statement
		List of FlagIds or flagtextids, FlagID  
		List of Values for each flag Value
		EntityType  - type of the entity - used to cross check with the flag
		FlagEntityType  - type of the Flag entity (added 22/3 used to cross check with the flag, defaults to the same as EntityType)

	optional parameters: 
		givefeedback (shown if parameter is defined)  [no sign of this working]
		userID  needed if no cookie set or to override 
		

Example:
		<CF_SetFlag 
			flagID="#frmFlagID#"
			entityId = "#frmEntityID#"
			EntityType = "#frmEntityTypeID#"
			Value = "#frmvalue#"
			giveFeedBack = "true">
	
		
amendments 
2001-01-17		WAB: took a look at it while using it for something, added code to allow entityType to  be numeric or character
			Need to make some stored procedures to check for rights to a flag and to do updates.
2001-03-22		WAB:  Realised that it was reasonable to pass in a list of entities of a different Type to the flag. added attributes.FlagEntityType for the cross check
2001-06-28		WAB: Problem if flagging say people from a list of locations and one of the locations doesn't have any people, so added in a is not null
2004-09-29		WAB: added
2005-03-23		WAB:  Appeared not to work if entitytype and flagentitytye not the same (getentities query not working properly!)
2005-09-19		WAB: Added a dynmically generated timeout to stop it all timing out.  Really ought to redo the whole thing to be more efficient
2006-09-05 	WAB:  removed some queries and replaced with call to application.entityType
2006-09-05 	NJH: added a job interception. If the number of entities to be set is greater than the job threshold, a job is created
2009-03-18  WAB: Problems with very large ID lists (passed in as select statements but passed on as ValueLists.  Rejigged, see below
2014-09-16	RPW	CORE-147 Manage Account Management New Account Approvals Alow for FlagID set to "on"
			
--->


<CFPARAM name="attributes.flagID" >    	<!--- list of flags either ids or textids --->
<cfparam name="attributes.EntityId" > 		<!--- list of entityIDs can be a select statement --->
<CFPARAM name="attributes.EntityType" >		<!--- used to confirm that flagentitytype and  actual entity type match--->
<CFPARAM name="attributes.FlagEntityType" default = "#attributes.EntityType#" >		<!--- used to confirm that flagentitytype and  actual entity type match--->
<CFPARAM name="attributes.Value">			<!--- value to set for each flag  (0 and 1 for booleans --->
<CFPARAM name="attributes.OrigValue"  default = "">  <!--- WAB July 04 - added so that I can unset flags as well as set them --->
<CFPARAM name="attributes.giveFeedback" default = "false"  >
<CFPARAM name="attributes.UserID"  default="">  <!--- usually only needs setting if there isn't a user cookie (ie if beign called by a scheduled process) --->
<CFPARAM name="attributes.flagright" default="">			<!--- this can be set to "System" to over write standard rights (ie on special screens) --->

<CFSET sitedatasource = #application.sitedataSource#>
<CFSET giveFeedback = attributes.giveFeedback>
<CFSET entityType = attributes.entityType>
<CFSET flagEntityType = attributes.flagEntityType >
<CFSET userID = attributes.userID>
<CFIF userID is "">
	<CFSET userID = request.relayCurrentUser.usergroupid>
</cfif>

<cfset result = structNew()>

<!--- NJH 2006-09-21 --->
<cfset flagID = attributes.flagID>
<cfset value = attributes.Value>


	<!--- WAB Added 2001-01-17 to allow entityType to be passed as number or text --->
<!--- 
		WAB 2006-09-05 removed query and replaced with call the application structure
	<CFQUERY NAME="getEntityType" DATASOURCE="#application.sitedatasource#">	
	select Tablename, * from flagEntityType
	where 
		<cfif isNumeric(entityType)>
		EntityTypeID = #entityType#
		<cfelse>
		tableName = '#entityType#'
		</cfif>

	</CFquery>	
	<cfset entityType = getEntityType.tablename> --->
	
<cfset entityTypeStruct = application.entityType[entityType]>
<cfset entityType = entityTypeStruct.tablename>

<!--- <cfdump var="#entityTypeStruct#"> <cfdump var="#entityType#"> ---> 

	
	
<!--- 	
	WAB 2006-09-05 removed query and replaced with call the application structure
	<CFQUERY NAME="getFlagEntityType" DATASOURCE="#application.sitedatasource#">	
	select Tablename, * from flagEntityType
	where 
		<cfif isNumeric(flagentityType)>
		EntityTypeID = #flagentityType#
		<cfelse>
		tableName = '#flagentityType#'
		</cfif>
	</CFquery>	
	<cfset flagentityType = getFlagEntityType.tablename>
 --->
<cfset flagEntityTypeStruct = application.entityType[flagEntityType]>
<cfset flagEntityType = flagentityTypeStruct.tablename>

<CFSET Freezedate=now()>


<!--- get entity names for feedback 
 also gets entity ids if given as a select statement 
2009-03-18  WAB: Problems with very large ID lists (passed in as select statements but passed on as ValueLists.
				decided that we would pass on a select statement instead
				did mean creating the correct select statement for (say) passing on organisationids when personids are passed in
				this did already exist within the getEntities query, but needed to be split up so that could be used twice
				also tested for the EntityType being passed in being the same as the flagentity type - in which case can use the original attributes.entityID 
				
--->

<CFSAVECONTENT VARIABLE = "SQLSnippet">
	<cfoutput>
		<cfif "person,location,organisation" contains FlagEntityType>
			organisation
				left outer  join 
			location 
				on organisation.organisationid = location.organisationid	
				left outer join 
			person 
				on location.locationid = person.locationid
		<cfelse>
		
			#EntityType#
	
		</cfif>
	WHERE 
		#EntityType#.#EntityTypeStruct.uniquekey# in (#replace(attributes.EntityId,"'","''","ALL")#)
		and #flagentitytype#.#flagEntityTypeStruct.uniquekey# is not null			
	</cfoutput>
</CFSAVECONTENT>
		

<CFQUERY NAME="getEntities" DATASOURCE="#application.sitedatasource#">	
	SELECT Distinct
		#flagentitytype#.#flagentityTypeStruct.uniquekey#  as entityID, 
		#preserveSingleQuotes(flagentityTypeStruct.NameExpression)# as entityName
	FROM
		#preserveSingleQuotes(SQLSnippet)#
</CFQUERY>


<cfif EntityType is FlagEntityType>
	<cfset getEntityIDSQL = attributes.EntityId>
<cfelse>
	<cfset getEntityIDSQL = "
		SELECT Distinct
			#flagentitytype#.#flagentityTypeStruct.uniquekey#
		FROM	
			#preserveSingleQuotes(SQLSnippet)#
	">

</cfif>


<!--- <CFQUERY NAME="getEntities" DATASOURCE="#application.sitedatasource#">	
	Select distinct
		#flagentitytype#.#flagentityTypeStruct.uniquekey# as entityID, 
		#preserveSingleQuotes(flagentityTypeStruct.NameExpression)# as entityName
	FROM 
		<cfif "person,location,organisation" contains FlagEntityType>
			organisation
				left outer  join 
			location 
				on organisation.organisationid = location.organisationid	
				left outer join 
			person 
				on location.locationid = person.locationid
		<cfelse>
		
			#EntityType#
	
		</cfif>
	WHERE 
		#EntityType#.#EntityTypeStruct.uniquekey# in (#attributes.EntityId#)
		and #flagentitytype#.#flagEntityTypeStruct.uniquekey# is not null			
</CFQUERY>
 --->

<!--- wab 2005-09-19 put in a calculated timeout.  We will allow .5 seconds per row , only set if more than 30 seconds required (would be useful to know what the current requesttimeout is--->
<cfset timePerRow = 0.5>
<cfset timeRequired = getEntities.recordCount * timePerRow>
<cfif timeRequired gt 30>
	<cfsetting requesttimeout="#timeRequired#">
</cfif>

<!--- timeout added WAB 2005-09-19 --->

<!--- NJH 2006-09-20 can we continue running this task? --->
<!--- 
jobs commented out temporarily
<cfif (not isDefined("request.runningMassApproval")) or (isDefined("request.runningMassApproval") and not request.runningMassApproval)>
	<cfif not application.com.jobs.canTaskRun(1,getEntities.recordCount)>
		<cfset result = application.com.jobs.handleTaskData(1,ValueList(getEntities.entityID),flagID,EntityType,flagEntityType,Value,giveFeedback) >
		<cfif result.jobCreated>
			<cfoutput><p>#result.message#</p></cfoutput>
			<cfif isDefined("caller.createJob")>
				<cfset caller.createJob = true>
			</cfif>
			<cfreturn>
		</cfif>
	</cfif>
</cfif>
 --->




	<CFLOOP Index="I" FROM="1" TO="#listlen(attributes.FlagID)#">
		<CFSET thisflag = listgetat(attributes.FlagID,I)>

		<!--- 2014-09-16	RPW	CORE-147 Manage Account Management New Account Approvals Alow for FlagID is not set "none of the above" --->
		<cfif IsNumeric(thisflag)>

		<cfif attributes.Value is not "">
			<CFSET thisflagValue = listgetat(attributes.Value,I)>
		<cfelse>
			<CFSET thisflagValue = "">
		</cfif>	

			<cfset getFlag =application.com.flag.getFlagStructure(thisFlag)>
			
			<CFSET flaggroupid=getflag.flaggroupid>

			<CFIF getFlag.entityType.TableName is not flagentityType>
				<CFOUTPUT>Flag type (#getFlag.entityType.TableName#) does not match specified entity type (#flagentityType#)</cfoutput>
				<CF_ABORT>
			</cfif>


	<!--- Does user have edit rights?
	must have rights to flaggroup and any parent flaggroup
	--->
	<!--- query doesn NOT check parent group access rights --->
				<CFSET flagMethod = "edit">
		<CFQUERY NAME="getFlagRights" DATASOURCE=#application.sitedatasource#>
		SELECT 1
		from flaggroup as fg
		where fg.flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND (	(fg.#flagMethod#AccessRights = 0 AND fg.#flagMethod# <> 0)
				OR
				(fg.#flagMethod#AccessRights <> 0	AND EXISTS (SELECT 1 
																FROM vFlagGroupRights as fgr, 
																	userGroup as ug
																WHERE fg.FlagGroupID = fgr.FlagGroupID
																AND fgr.PersonID = ug.PersonID 
																and ug.usergroupid =  <cf_queryparam value="#userid#" CFSQLTYPE="CF_SQL_INTEGER" > 
																AND fgr.#flagMethod# <> 0)
				OR '#attributes.FlagRight#' = 'SystemTask'
				)
			)

		</CFQUERY>		


		<CFIF getflagrights.recordcount is 0>		
	
			<CFOUTPUT>You do not have rights to #htmleditformat(flagmethod)# this flag (#htmleditformat(getFlag.flagID)#)</CFOUTPUT>
	
		<CFELSE>
		


			<cfif  (getFlag.flagType.Name is "Radio"  or getFlag.flagType.Name is "CheckBox") and thisFlagValue is 0>
				<cfset thisresult = application.com.flag.deleteflagdata(flagid = thisFlag, entityID = getEntityIDSQL)>			
			<cfelse>
				<cfset thisresult = application.com.flag.setflagdata(flagid = thisFlag, entityID = getEntityIDSQL, data = thisFlagValue,returnNumberNotAffected=giveFeedback)>	
			</cfif>	

<!--- 
			<cfif  (getFlag.flagType.Name is "Radio"  or getFlag.flagType.Name is "CheckBox") and thisFlagValue is 0>
				<cfset thisresult = application.com.flag.deleteflagdata(flagid = thisFlag, entityID = valuelist(getEntities.entityid))>			
			<cfelse>
				<cfset thisresult = application.com.flag.setflagdata(flagid = thisFlag, entityID = valuelist(getEntities.entityid), data = thisFlagValue,returnNumberNotAffected=giveFeedback)>	
			</cfif>	
 --->
			<cfset result[thisFlag] = thisResult>
			<cfset result[thisFlag].name = getFlag.Name> 

<!--- 			<!--- wab added this for unsetting flags --->
			<cfif attributes.OrigValue is not "">
				<CFSET thisflagOrigValue = listgetat(attributes.OrigValue,I)>
			<cfelseif  getFlag.flagType.Name is "Radio"  or getFlag.flagType.Name is "CheckBox">
				<CFSET thisflagOrigValue = abs(thisFlagValue-1)>		
			<cfelse>
				<CFSET thisflagOrigValue =  thisFlagValue & "-">			
			</cfif>
		
			<CFIF getFlag.flagType.Name is "Radio"  or getFlag.flagType.Name is "CheckBox">
				<CFSET thisFlagForm =  "#getFlag.flagType.Name#_#getFlag.flagGroupID#">
				<CFSET flagList = listappend(flagList, thisFlagForm)>
				<CFSET "#thisFlagForm#_#entityID#" =  getFlag.flagID * thisFlagValue>
				<CFSET "#thisFlagForm#_#entityID#_orig" =  getFlag.flagID * thisFlagOrigValue>

				
			<CFELSE>
				<CFSET thisFlagForm ="#getFlag.flagType.Name#_#getFlag.flagID#">
				<CFSET flagList = listappend(flagList, thisFlagForm)>
				<CFSET "#flagList#_#entityID#" =  thisFlagValue>
				<CFSET "#flagList#_#entityID#_orig" =  thisFlagOrigValue >
					
			</cfif>
 --->

		
		</CFIF>
		
		</cfif>

	</cfloop>				

	<cfset caller.SetFlagResult = result>

