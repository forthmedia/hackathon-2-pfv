<!--- �Relayware. All Rights Reserved 2015 --->
<!--- Two selects Combo Struct --->
<!---


File name:		TwoSelectsComboStruct.cfm
Author:			RJT
Date started:	2015-09-24

Description:	A version of the TwoSelectsCombo that takes structs rather than querys, it converts to queries then delegates to TwoSelectsComboQuery.

				NOTE: although coldfusion structs have no order (and so the combo box will display in any order) if you use
				<cfset mystruct = createObject("java", "java.util.LinkedHashMap").init() /> instead it will behave as a struct while
				maintaining order.


Example usage:
				<cfscript>
					testSelected = createObject("java", "java.util.LinkedHashMap").init();
					testSelected["alpha"]="ALPHA";
					testSelected["beta"]="BETA";

					testNotSelected = createObject("java", "java.util.LinkedHashMap").init();
					testNotSelected["gamma"]="GAMMA";
					testNotSelected["delta"]="DELTA";

				</cfscript>

				<CF_TwoSelectsComboStruct
				    NAMESelected="Selected"
				    NAMENotSelected="NotSelected"
				    SIZE="6"
				    WIDTH="150px"
				    FORCEWIDTH="50"
				    structSelected="#testSelected#"
				    structNotSelected="#testNotSelected#"
				    CaptionSelected="<FONT SIZE=-1><B>Selected:</B></FONT>"
				    CaptionNotSelected="<FONT SIZE=-1><B>Not Selected:</B></FONT>"
					UP="No"
					DOWN="No"
					FORMNAME="edit"> <!--- NOTE: form name, not ID --->

				The selected values are returned as NAMESelected in the form scope


Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed

--->


<!--- TAG PARAMETERS --->
<!--- Really all of these are mandatory, however they have defaults because they can be added in other ways for backwards compatibility---->
<CFPARAM NAME="Attributes.NameSelected" DEFAULT="">
<CFPARAM NAME="Attributes.NameNotSelected"  DEFAULT="">
<CFPARAM NAME="Attributes.structSelected" DEFAULT="#structNew()#">
<CFPARAM NAME="Attributes.structNotSelected" DEFAULT="#structNew()#">
<CFPARAM NAME="Attributes.CaptionSelected" DEFAULT="">
<CFPARAM NAME="Attributes.CaptionNotSelected" DEFAULT="">
<!--- SEE TwoSelectsComboQUERY for extra supported attributes which this method delegates to--->

<cfscript>
	//translate to CF_TwoSelectsComboQuery attributes (but still allow the old ones)
	if (NOT structKeyExists(Attributes,"Name2")){
		Attributes.Name2=Attributes.NameSelected;
	}
	structDelete(Attributes,"NameSelected");

	if (NOT structKeyExists(Attributes,"Name")){
		Attributes.Name=Attributes.NameNotSelected;
	}
	structDelete(Attributes,"NameNotSelected");

	if (NOT structKeyExists(Attributes,"Caption2")){
		Attributes.Caption2=Attributes.CaptionSelected;
	}
	structDelete(Attributes,"CaptionSelected");

	if (NOT structKeyExists(Attributes,"Caption")){
		Attributes.Caption=Attributes.CaptionNotSelected;
	}
	structDelete(Attributes,"CaptionNotSelected");

	if (structKeyExists(Attributes,"struct1") or structKeyExists(Attributes,"struct2")){
		throw (message = "struct1 and struct2 are not supported arguments, use structSelected and structNotSelected instead, this can also be done for Caption and Name", type = "Invalid Arguments", errorCode = "Invalid Arguments");
	}


	//Query1 is selected, note the wrong way round from other attributes
	Attributes.Query1= queryNew("column1,column2", "VarChar,VarChar");

	for(key in Attributes.structSelected){
		queryAddRow(Attributes.Query1);
		querySetCell(Attributes.Query1,"column1",key);
		querySetCell(Attributes.Query1,"column2", Attributes.structSelected[key]);
	}

	//Query2 is nonselected
	Attributes.Query2= queryNew("column1,column2", "VarChar,VarChar");

	for(key in Attributes.structNotSelected){
		queryAddRow(Attributes.Query2);
		querySetCell(Attributes.Query2,"column1",key);
		querySetCell(Attributes.Query2,"column2", Attributes.structNotSelected[key]);
	}

</cfscript>

<!--- Delegate to CF_TwoSelectsComboQuery--->
<CF_TwoSelectsComboQuery attributecollection="#attributes#">
