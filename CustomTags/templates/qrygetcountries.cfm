<!--- �Relayware. All Rights Reserved 2014 --->
<!--- comes up with a list of all
		countries to which a User
		has at least read rights

USAGE:
		<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
		qrygetcountries creates a variable CountryList

		  AND l.CountryID IN (#Variables.CountryList#)
--->

<!--- mods
WAB 2000-03-02 added stuff to return a comma separated list containing both the ids and the countrynames
 --->


<!--- 2000-09-29 WAB added code so that the permission query brings back column names which are the same as the definitions in th
Permissiondefinition Table
Unfortunately view and add are keywords!
--->
<CFQUERY NAME="getPermissions" DATASOURCE=#application.SiteDataSource#>
Select 
	PermissionLevel, 
	PermissionName ,
	CASE WHEN permissionName in ('view','add') THEN '_' + permissionName ELSE permissionName END as ModifiedPermissionName  <!--- unfortunately view and add are SQL reserved words so can't be used as column names later on --->
from PermissionDefinition
where SecurityTypeID = (SELECT c.SecurityTypeID FROM SecurityType AS c
         					WHERE c.ShortName = 'RecordTask')
</CFQUERY>


<CFPARAM name="ReqdCountryID" default = "0">

	<!--- lists all Countries to which User has rights --->
	<CFQUERY NAME="findCountries" DATASOURCE="#application.sitedatasource#">
	SELECT r.CountryID,
			c.countryDescription as CountryIDandName, 
			<CFLOOP query="getPermissions">
			Convert(bit,sum(r.permission & #evaluate("2 ^ (#permissionLevel#-1)")#)) AS #replace(ModifiedPermissionName," ","","ALL")#,
			</cfloop>
			Convert(bit,sum(r.permission & 8)) AS Level4,
			Convert(bit,sum(r.permission & 4)) AS Level3,
			Convert(bit,sum(r.permission & 2)) AS Level2,
			Convert(bit,sum(r.permission & 4)) AS AddOkay,
			Convert(bit,sum(r.permission & 2)) AS UpdateOkay

	FROM Rights AS r, RightsGroup AS rg, Country as c
	WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e
							 WHERE e.ShortName = 'RecordTask')
	AND r.UserGroupID = rg.UserGroupID
	AND c.countryid = r.countryid
	AND rg.PersonID = #request.relayCurrentUser.personid#
	<CFIF ReqdCountryID is not 0>
		AND r.CountryID =  <cf_queryparam value="#ReqdCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
	AND r.Permission > 0
	GROUP BY r.CountryID, c.countryDescription
	ORDER BY CountryDescription
<!--- 	HAVING Sum((r.Permission\1) MOD 2) > 0	<!--- at least read permissions ---> --->
	</CFQUERY>

	<CFSET CountryList = 0>
	
	<CFIF findCountries.RecordCount GT 0>
		<CFIF findCountries.CountryID IS NOT "">
			<!--- top record (or only record) is not null --->
			<CFSET CountryList = ValueList(findCountries.CountryID)>
			<CFSET CountryIDandNameList = valueList(findCountries.CountryIDandName)	>
		
		</CFIF>
	</CFIF>

	<!--- reset defaults (in case called more than once in a single page --->
	<CFSET partnerCheck=false>
	<CFSET ReqdCountryID=0>
		