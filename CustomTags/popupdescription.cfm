<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes" >
<cfparam name="attributes.attachToElementID" default=""><!--- the id for the html element that will trigger the popup description display. --->
<cfparam name="attributes.triggerEvent" default="onmouseover"><!--- the event that triggers the popup... still requires some work though. --->
<cfparam name="attributes.textToDisplay" default=""><!--- if just some text to display, use this. --->
<cfparam name="attributes.divIDWithContent" default=""><!--- if you have an html element to display instead of just the text. --->
<cfparam name="attributes.divID" default="div_#createUUID()#"><!--- an id for the display div, if divIDWithContent is not used. --->
<cfparam name="attributes.displayStyle" default="dropshadownoimages"><!--- to move between display types for display container. this assumes that divIDWithContent is not specified. --->

<cfparam name="attributes.displayContainerWidth" default="300"><!--- width of the display container. this assumes that divIDWithContent is not specified. --->
<cfparam name="attributes.displayOffsetX" default="0">
<cfparam name="attributes.displayOffsetY" default="0">

<cfparam name="request.popUpDescriptionRun" default="false">

<!--- 2007-09-26 the style and javascript code is output every time this tag is called (could be 20 times per page)
	 WAB added a check --->

<cfif not	request.popUpDescriptionRun >
	<cfset request.popUpDescriptionRun = true>
	<cfoutput>
		<style type="text/css">
			.basicLayout {
				border: 1px solid ##000000;
				background-color: ##FFFFFF;
				padding: 4px 4px 4px 4px;
				width: #attributes.displayContainerWidth#px;
			}
			
			/*dropshadow*/
			.wrap1, .wrap2, .wrap3 {
				display: inline-table;
				/* \*/display: block;/**/
			}
			.wrap1 {
				float: left;
				background: url(/images/dropshadows/dropshadow1/shadow.gif) right bottom no-repeat;
			}
			.wrap2 {
				background: url(/images/dropshadows/dropshadow1/corner_bl.gif) left bottom no-repeat;
				background-position: -4px;
			}
			.wrap3 {
				padding: 0 5px 5px 0;
				background: url(/images/dropshadows/dropshadow1/corner_tr.gif) right top no-repeat;
				background-position: -4px;
			}
			.wrap3 img {
				display:block;
			}
			
			/* CSS container shadow */
			##shadow-container {
				position: relative;
				left: 3px;
				top: 3px;
				margin-right: 3px;
				margin-bottom: 3px;
			}
			
			##shadow-container .shadow2,
			##shadow-container .shadow3,
			##shadow-container .container {
				position: relative;
				left: -1px;
				top: -1px;
			}
			
			##shadow-container .shadow1 {
				background: ##F1F0F1;
			}
			
			##shadow-container .shadow2 {
				background: ##DBDADB;
			}
			
			##shadow-container .shadow3 {
				background: ##B8B6B8;
			}
			
			##shadow-container .container {
				background: ##ffffff;
				border: 1px solid ##848284;
				padding: 10px;
				width: #attributes.displayContainerWidth#px;
			}
		/* CSS container shadow */
			
			/*rounded corners*/
			/*div##nifty{ margin: 0 10%;background: ##9BD1FA}
			b.rtop, b.rbottom{display:block;background: ##FFF}
			b.rtop b, b.rbottom b{display:block;height: 1px; overflow: hidden; background: ##9BD1FA}
			b.r1{margin: 0 5px}
			b.r2{margin: 0 3px}
			b.r3{margin: 0 2px}
			b.rtop b.r4, b.rbottom b.r4{margin: 0 1px;height: 2px}
			.displayArea { background-color:##A2B6CC; }
			.rtop, .rbottom{display:block}
			.rtop *, .rbottom *{display: block; height: 1px; overflow: hidden; background-color: ##A2B6CC;}
			.r1{margin: 0 5px}
			.r2{margin: 0 3px}
			.r3{margin: 0 2px}
			.r4{margin: 0 1px; height: 2px}*/
			</style>
		
			<script language="javascript">
			function displayObj(objName,offsetX,offsetY) {
				var obj = document.getElementById(objName);
				var oX = 0;
				var oY = 0;
		        obj.style.display="block";
				if (typeof(offsetX) != "undefined") {
					oX = offsetX;
				}
				if (typeof(offsetY) != "undefined") {
					oY = offsetY;
				}
				var str = "";
				str += "left before:"+parseInt(posx);
				
				obj.style.left = parseInt(posx)+parseInt(oX)+'px';
				
				str += "\nleft after:"+obj.style.left;
				str += "\ntop before:"+parseInt(posy);
				
				obj.style.top = parseInt(posy)+parseInt(oY)+'px';
				str += "\ntop after:"+obj.style.top;
				//alert(str);
			}
			
			function hideObj(objName) {
				var obj = document.getElementById(objName);
		        obj.style.display="none";
			}
				
			function storeMousePos(e) {
				posx = 0;
				posy = 0;
				if (!e) var e = window.event;
				if (e.pageX || e.pageY)
				{
					posx = e.pageX;
					posy = e.pageY;
				}
				else if (e.clientX || e.clientY)
				{
					posx = e.clientX + document.body.scrollLeft;
					posy = e.clientY + document.body.scrollTop;
				}

			}
			
			document.onmousemove = storeMousePos;
			</script>
	</cfoutput>
</cfif>


<cfset objToDisplayID = attributes.divID>
<cfif attributes.divIDWithContent is not "">
	<cfset attributes.textToDisplay = "">
	<cfset objToDisplayID = attributes.divIDWithContent>
	<cfoutput>
		<script>
			var displayObj = document.getElementById("#jsStringFormat(objToDisplayID)#");
			displayObj.style.display = "none";
			displayObj.style.position = "absolute"
		</script>
	</cfoutput>
</cfif>


<cfif attributes.attachToElementID is "">
	<cfoutput><img src="/images/misc/iconhelpsmall.gif" border="0" #attributes.triggerEvent#="displayObj('#objToDisplayID#','#attributes.displayOffsetX#','#attributes.displayOffsetY#')" onmouseout="hideObj('#objToDisplayID#')"></cfoutput>
<cfelse>
	<cfoutput>
		<script>
			var triggerObj = document.getElementById("#jsStringFormat(attributes.attachToElementID)#");
			triggerObj.#jsStringFormat(attributes.triggerEvent)# = function() {displayObj('#jsStringFormat(objToDisplayID)#','#jsStringFormat(attributes.displayOffsetX)#','#jsStringFormat(attributes.displayOffsetY)#')};
			triggerObj.onmouseout = function() {hideObj('#jsStringFormat(objToDisplayID)#')};
		</script>
	</cfoutput>
</cfif>
<!--- use default obj for display --->
<cfif attributes.textToDisplay is not "">
	
		<cfif attributes.displayStyle is "dropshadowimages">
			<cfoutput>
			<!--- drop shadow images --->
			<div id="#objToDisplayID#" style="position:absolute; top:0px; left:0px; display:none;">
				<div class="wrap1">
					<div class="wrap2">
						<div class="wrap3">
							<div class="basicLayout">
								#application.com.security.sanitiseHTML(attributes.textToDisplay)#
							</div>
						</div>
					</div>
				</div>
			</div>
			</cfoutput>
		</cfif>
		<cfif attributes.displayStyle is "dropshadownoimages">
			<!--- drop down no images --->
			<cfoutput>
			<div id="#objToDisplayID#" style="position:absolute; top:0px; left:0px; display:none;">
				<div id="shadow-container">
					<div class="shadow1">
						<div class="shadow2">
							<div class="shadow3">
								<div class="container">
									#application.com.security.sanitiseHTML(attributes.textToDisplay)#
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</cfoutput>
		</cfif>
		<!--- rounded --->
		<!--- <div id="#objToDisplayID#" style="position:absolute; top:0px; left:0px; display:none;">
			<div id="nifty">
				<b class="rtop">
				  <b class="r1"></b> <b class="r2"></b> <b class="r3"></b> <b class="r4"></b>
				</b>
				<span class="displayArea">#attributes.textToDisplay#</span>
				<b class="rbottom">
				  <b class="r4"></b> <b class="r3"></b> <b class="r2"></b> <b class="r1"></b>
				</b>
			</div>
		</div> --->
	
</cfif>
<cfsetting enablecfoutputonly="No" >
