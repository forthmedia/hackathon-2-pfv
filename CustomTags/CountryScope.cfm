<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			UGRecordManager.cfm
Author:			AH
Date created:	20 April 2000

Description:	

Custom Tag. Allows CountryIDs to be associated with entities, creating Country scope of an entity.

Dependencies:

Expects parameters: Entity, EntityID (promotion, application etc...)

				
Usage: <CF_CountryScope entity="element" entityid="#RECORDID#" Form="MyForm">


				
Entity: 
Required. Specifies entity in the table RecordRights

EntityID: 
Required. Specifies RecordID in the table RecordRights

Also Very Important: In order to process this tag place the tag "UGRecordManagerTask" wherever you are
processing the results of this form.

Version history:
1	-	Initial version
2	- 	WAB altered code so that can handle more than one of these on a single page.  In particular
	the entityid and type are now encoded in the name of the select box.  The task searches for form fields of
	the form CS_#Entity#_#EntityDet#.
SWJ		2002-07-15	Removed the table from around the tag.
WAB 	2005-10-10 altered query to make sure that any countries listed have an entry in the countryGroup Table (basically removes empty regions from list)
WAB		March 2006	altered the countryscope table to have a permission column along the lines of the recordrights table
					new parameter Level which defaults to 1 and gives backwards compatability

WAB Sept 2006    added attribute countryIDFilterList - used to pass in the list of coutries which are to be shown in the "Available" List
2015-12-01	WAB PROD2015-290 Visibility Project.  Implement the new multiselect.
--->

<CFSET delim1=application.delim1>

<CFPARAM NAME="Attributes.Entity" >
<CFPARAM NAME="Attributes.EntityID" >
<CFPARAM NAME="Attributes.Form" DEFAULT="">
<CFPARAM NAME="Attributes.showDefaultCountry" DEFAULT="false">
<CFPARAM NAME="Attributes.method" DEFAULT="edit">
<CFPARAM NAME="Attributes.level" DEFAULT="1">
<CFPARAM NAME="Attributes.countryIDFilterList" DEFAULT="">
<CFPARAM NAME="Attributes.hideCountryGroups" TYPE="BOOLEAN" DEFAULT="false"> <!--- NJH 2008/04/19 Want to hide country groups in promotion Edit  --->
<CFPARAM NAME="Attributes.width" DEFAULT="250"> <!--- 2008/05/21 GCC allow width override to be passed in --->

	<CFSET Entity = Attributes.Entity>
	<CFSET EntityID = Attributes.EntityID>
	

<cfif attributes.method is "edit">

	<!---Get all available  (whoever has a UserGroupID created in the UserGroup table--->
		<CFQUERY NAME="GetAllCountries" DATASOURCE="#Application.SiteDataSource#">
		select 
			* ,
			case when cs.countryid is not null then 1 else 0 end as isselected
		from
			(
				select distinct
					CountryDescription, 
					CountryID , 
					CASE WHEN isoCode is Null Then 2 Else 4 END as SortOrder

				from
					<!--- wab 2005-10-10 added this join to get rid of empty regions  --->
					Country c inner join countryGroup cg on c.countryid = cg.countrygroupid
				where
					1 = 1		
				<cfif Attributes.countryIDFilterList is not "">
					AND CountryID  IN ( <cf_queryparam value="#Attributes.countryIDFilterList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )			
				</cfif>
				
				<!--- NJH 2008/04/19 bring only countries back --->
				<cfif attributes.hideCountryGroups>
					and isoCode is not null
				</cfif>
				
				<CFIF Attributes.showDefaultCountry>
					union
					
					select 'Default', 0, 1
				</CFIF>
			
				<!--- NJH 2008/04/19 if showing country groups --->
				<cfif not attributes.hideCountryGroups>
					union
					
					select '----------------', 0, 3 
				</cfif>
			) as c
			
			left join
			
			countryScope cs
			
			ON 		c.countryid = cs.countryid	
					AND Entity = '#Entity#'
					<CFIF isNumeric(entityID)>
					AND EntityID = #EntityID# 
					and permission & power(2,#attributes.level# -1) <> 0
					<CFELSE>
					AND 1 = 0
					</CFIF>

			
			ORDER BY isselected desc, sortOrder, countryDescription
		</CFQUERY>
		
	
		<cfset pluginOptions = {multipleSelect = true}>
		<!--- The user drop down --->
		<cfoutput>
		#application.com.relayForms.ValidValuedisplay (query=GetAllCountries, name="CS_#Entity#_#EntityID#_#attributes.level#",  value="countryid", display="countryDescription", displayAs="multiselect", plugInOptions = pluginOptions, keepOrig = true )#
		</cfoutput>
		
		
	

<cfelse>

	<CFQUERY NAME="getCountryScope" DATASOURCE="#Application.SiteDataSource#">
	select dbo.getCountryScopeList (#entityID#,'#entity#',#attributes.level#,1)	 as list
	</CFQUERY>

	<cfoutput >#getCountryScope.list#</cfoutput>


</cfif>
	
<!--- </TABLE> --->




