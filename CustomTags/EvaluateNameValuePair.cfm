<!--- �Relayware. All Rights Reserved 2014 --->
<!--- evaluates a list of name value pairs

<cf_evaluateNameValuePairs
	NameValuePairs = "x=1,y=2,z=3"     
	delimiter   = ","   optional (default ",")
	mode = "set" or "param" or null  optional (default = set)
	scope = ""      (optional eg  "url" or "form"
	
	
	or   NameValuePairs = "x=1&y=2&z=3"
	
	
>

2008/09/24 WAB altered to allow use of dotnotation in the variable names ()
2012/06/18	IH	Case 428853 trim "variablename" to prevent issues with trailing white spaces
 --->

<CFPARAM NAME="Attributes.NameValuePairs">
<CFPARAM NAME="Attributes.delimiter" default = ",">
<CFPARAM NAME="Attributes.mode" default = "set"><!--- can be param - doesn't overwrite existing values or null sets all to blank--->
<CFPARAM NAME="Attributes.scope" default = "caller">  <!--- url, form, request --->
<cfparam name="attributes.collectionName" default = "evaluateNameValuePairCollection">  <!--- WAB added 2006-10-11 - returns the collection of name value pairs --->
<cfparam name="attributes.DotNotationToStructure" default = "false"> <!--- WAB 2008/09/24 if name value pairs have dot notation then creates substructures (note that variables with those names will not be created --->


<!--- WAB 2005-11-30 replaced original code with a regular expression version which can handle quoted strings and things
	only tested with delimiters space and , and & 
	unfortunately it turned out that this custom tag is sometimes run before the application.com objects have been loaded, so I have had to put in a test for this case
 --->
<cfif isDefined("application.com.regExp")>
	<cfset regExpobject = application.com.regExp>
<cfelse>
	<cfset regExpobject =  CreateObject("component","relay.com.regexp")>
</cfif>

<cfset theStruct = regExpobject.convertNameValuePairStringToStructure (inputString=attributes.NameValuePairs,delimiter=attributes.delimiter, DotNotationToStructure = attributes.DotNotationToStructure)>

<cfloop collection="#theStruct#" item="i">	
	<cfset value = theStruct[i]>
	<cfset variablename = trim(i)><!--- 2012/06/18	IH	Case 428853 trim "variablename" to prevent issues with trailing white spaces --->
	<cfif variablename is not ""><!--- added wab 2006-02-10 can't think how this could happen, but got an error due to it being blank --->		
		<CFIF attributes.mode is "set">
			<CFSET "#Attributes.scope#.#variablename#" = value>
		<CFELSEIF attributes.mode is "param">
			<CFPARAM Name="#Attributes.scope#.#variablename#" default="#value#">
		<CFELSEIF attributes.mode is "null">
			<CFSET "#Attributes.scope#.#variablename#" ="">
		<CFELSE>
			<CFOUTPUT>Invalid Mode Setting #htmleditformat(mode)#</cfoutput><CF_ABORT>
		</cfif>
	</cfif> 
</cfloop>

<cfset caller [attributes.collectionName] =theStruct>
	
<!--- <cfoutput>#attributes.NameValuePairs#</cfoutput>
<cfdump var="#theStruct#"> --->

<!--- <CFLOOP list="#attributes.NameValuePairs#" index="thispair" delimiters="#attributes.delimiter#">
	<CFIF listlen(thisPair,"=") GT 1>   <!--- just check that there is a pair--->
		<cfset variablename = trim(listfirst(thispair,"="))>
		<cfset value = listrest(thispair,"=")>
		<CFIF attributes.mode is "set">
			<CFSET "#Attributes.scope#.#variablename#" = value>
		<CFELSEIF attributes.mode is "param">
			<CFPARAM Name="#Attributes.scope#.#variablename#" default="#value#">
		<CFELSEIF attributes.mode is "null">
			<CFSET "#Attributes.scope#.#variablename#" ="">
		<CFELSE>
			<CFOUTPUT>Invalid Mode Setting #mode#</cfoutput><CF_ABORT>
		</cfif>
	</cfif>

</CFLOOP> 
 --->

