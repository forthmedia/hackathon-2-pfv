<!--- �Relayware. All Rights Reserved 2014 --->

<!---Override Default Styles in order to make smaller letters for the comlumn headings--->
<STYLE>
 .Headings  {
                  	font-family : 'Arial', 'Verdana', 'Sans-Serif';
                  	font-size : 8pt;
                  	font-weight : normal;
                  	color : Red;
					background-color : #C7C7C7;
                  }
				  
  A:Hover  {
                  	font-family : 'Arial', 'Verdana', 'Sans-Serif';
                  	font-weight : bold;
                  	color : Red;
                  	font-size : 9pt;
                  }
				  
.Numbers	{ font-size : 9pt; }
</STYLE>



<CFPARAM NAME="Attributes.Query1">
<CFPARAM NAME="Attributes.Query2">
<CFPARAM NAME="Attributes.Action">
<CFPARAM NAME="Attributes.ColumnName">
<CFPARAM NAME="Attributes.RowName">
<CFPARAM NAME="Attributes.DownloadLink" DEFAULT="No">





<!--- USE PASSED QUERIES WITHIN THIS CODE AS "Headings" and "Data" --->
<CFSET Headings = Evaluate("Caller.#Attributes.Query1#")>
<CFSET Data = Evaluate("Caller.#Attributes.Query2#")>

<!---Catch Parameters passed through the Custom Tag--->
<CFSET Action = #Attributes.Action#>
<CFSET ColumnName = #Attributes.ColumnName#>
<CFSET RowName = #Attributes.RowName#>

<CFSET DownLoadLink = #Attributes.DownloadLink#>
<CFIF 	DownloadLink is "Yes" and (not isdefined("attributes.DownloadLocalPath") or not isdefined("attributes.DownloadWebPath"))>
	Download File path not specified
<CFELSE>
	<CFSET DownloadLocalPath  = attributes.DownloadLocalPath>
	<CFSET DownloadWebPath  = attributes.DownloadWebPath>
</CFIF>




<!---Create a list of ColumnIDs--->
<CFSET  columnids = valuelist(Headings.columnid)>



<!---Dynamically set values of the hidden fields---> 
<SCRIPT>

function drilldown(colvalue,rowvalue){
form = window.document.myForm
<CFOUTPUT>
form.#jsStringFormat(columnName)#.value = colvalue
form.#jsStringFormat(rowName)#.value = rowvalue
</CFOUTPUT>
form.submit()

}


</SCRIPT>




<CFOUTPUT>

<!---Dynamically define the Form Action and other two required attributes, all of this passed as
the parameter "Action" through the Custom Tag--->
<FORM ACTION="#action#" METHOD="post" NAME="myForm">

<CF_INPUT TYPE="hidden" NAME="#ColumnName#" VALUE="">
<CF_INPUT TYPE="hidden" NAME="#RowName#" VALUE="">


</CFOUTPUT>

<!--- also build a file which can be downloaded - initialise variable to hold details--->
<!--- actually build the variable whether it is wanted or not --->
<CFSET downloadString="">
<CFSET delimiter=chr(9)>
<CFSET newline= chr(10) >


<CFIF DownloadLink is "Yes">
	<!---Delete previous file left by the same user--->
	<CFIF #FileExists(DownloadLocalPath)# IS "Yes">
		<CFFILE ACTION="DELETE" FILE="#DownloadLocalPath#">
	</CFIF>
</cfif>


<TABLE border=1>

<TR><TD></TD>

<!---Output Column Headings--->
<CFOUTPUT query="Headings">
	<TD class="headings">#htmleditformat(columnheading)#</TD>
	<CFSET downloadString = downloadString & delimiter & columnheading> 
</cfoutput>

</TR> <CFSET downloadString = downloadString & newline>

<!---Output Rowheadings--->
<CFOUTPUT query="Data" group="rowid">
	
	<TR>
		<TD>#htmleditformat(rowheading)#</TD>
		<CFSET downloadString = downloadString & rowheading>
	<CFSET counter = 1>
	


	<CFOUTPUT>

<!---Compare the list columnids with record set columnid from the Query, where the data is the same output
numbers, where it is not (NULL positions in the record set) put non-breaking space instead--->

		<CFLOOP condition="#listgetat(columnids,counter)# is not #columnid#">
		<TD>&nbsp;</tD>	
			<CFSET downloadString = downloadString & delimiter>
			<CFSET counter = counter+1>
		</cfloop>
		
		<TD align="right" class="numbers"><A HREF="javascript:drilldown('#ColumnID#','#rowid#')">#htmleditformat(number)#</A></tD>	
			<CFSET downloadString = downloadString & delimiter & number>
			<CFSET counter = counter+1>
	</cfoutput>

	
	<CFLOOP condition = "counter LTE listlen(columnids)">
	<TD>&nbsp;</tD>		
			<CFSET downloadString = downloadString & delimiter>
			<CFSET counter = counter+1>
	</CFLOOP>


	 <CFSET downloadString = downloadString & newline>

</cfoutput>
</TR>

	 
</table>

<CFIF DownloadLink is "Yes">

	<CFFILE ACTION="WRITE" FILE="#DownloadLocalPath#" OUTPUT="#downloadString#">

	<A HREF="<CFOUTPUT>#htmleditformat(DownloadWebPath)#</CFOUTPUT>">Download this table</A>
</CFIF>	

</FORM>


