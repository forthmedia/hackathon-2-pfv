<CFPARAM NAME="attributes.currentField" default="">
<CFPARAM NAME="attributes.availableFields" type="string"> <!--- Actually must be a list but we're not allowed to say that--->
<CFPARAM NAME="attributes.currentMappingFromAssertion" default="1">
<CFPARAM NAME="attributes.currentMapping" default=""> <!---Either an assertion key or a hard coded value --->
<CFPARAM NAME="attributes.suppressScript" default="false">
<CFPARAM NAME="attributes.lockMappingType" default="false">
<CFPARAM NAME="attributes.deletable" default="true">

<cfif not attributes.suppressScript>
	<cf_includeJavascriptOnce template="/singleSignOn/SAML/js/SAMLIDPManagement.js">
</cfif>

<cfscript>
	//sanities truthy and falsey values to 1 and 0
	if (attributes.currentMappingFromAssertion){
		attributes.currentMappingFromAssertion=1;	
	}else{
		attributes.currentMappingFromAssertion=0;
	}
	
</cfscript>


<cfoutput>
	
	<tr>
		<td>
			<CF_relayFormElementDisplay disabled="#attributes.lockMappingType#" relayFormElementType="select" id="" currentValue="#attributes.currentField#" list="#attributes.availableFields#" fieldName="mappingFieldRelayware" label="" onChange="ensureSufficientTableSpace(this);">
		</td>
		<td>
			<CF_relayFormElementDisplay disabled="#attributes.lockMappingType#" relayFormElementType="select" id="" currentValue="#attributes.currentMappingFromAssertion#" fieldName="currentMappingFromAssertion" label="" list="1#application.delim1#phr_SAML_FromAssertion,0#application.delim1#phr_SAML_HardCoded">
		</td>
		<td>
			<input name="mappingValue" value="#attributes.currentMapping#"  maxLength="500"/>
		</td>
		<td>
			<cfif attributes.deletable>
				<a href="##" onclick="ensureSufficientTableSpace(this); jQuery(this).closest('tr').remove(); return false;" class="btn btn-primary">phr_delete</a>
			</cfif>
		</td>
	</tr>
</cfoutput>