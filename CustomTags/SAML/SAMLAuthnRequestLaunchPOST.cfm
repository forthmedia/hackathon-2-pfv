<!--- Launches an authentication request (either automatically or via a button click) to a SAML IDP --->
<CFPARAM NAME="attributes.relayState" default="#structKeyExists(url, 'urlrequested')?url.urlrequested:''#">
<CFPARAM NAME="attributes.SAMLIdentityProviderID" type="numeric">
<CFPARAM NAME="attributes.autolaunch" type="boolean" default="false">
<CFPARAM NAME="attributes.launchButtonText" type="string" default="phr_Login">
<CFPARAM NAME="attributes.autoLaunchDelay" type="numeric" default="0"> <!--- If autolaunching how long it will wait for (used in the redirect page) --->

<!--- for efficiency we can pass an idpManager if we like, if not one is created  --->
<CFPARAM NAME="attributes.idpManager" default='#application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPManager").init()#'>

<cfscript>

	identityProvider=attributes.idpManager.getIDPByID(attributes.SAMLIdentityProviderID);
	relayareAsSPConfig=application.com.saml.getRelaywareAsSPConfigForCurrentSite();
	samlRequestBuilder=application.javaloader.create("singleSignOn.samlserviceprovider.AuthnRequestGenerator").init(relayareAsSPConfig);
	certificateManager = new singleSignOn.SAML.IDP.CertificateManager();

	//a SAML ID may contain letters but must start with a letter, hence the RW
	requestID=Replace("RW"&CreateUUID(),"-","","all");
	lock type="Exclusive" scope="Session" timeout="20" {
		if(StructKeyExists(Session, "authnRequestIds")) {
			ArrayAppend(Session.authnRequestIds, requestID);
		} else {
			Session.authnRequestIds = ArrayNew(1);
			ArrayAppend(Session.authnRequestIds, requestID);
		}
	}


    password = certificateManager.getCertificateStorePassword();
    certificateAlias = identityProvider.getCertificateToUse();
    certificationKeyStorePath =certificateManager.getCertificateStoreLocation();
    certificationKeyStorePath = Replace(certificationKeyStorePath, "\", "\\", "all");

	SAMLRequestXML = samlRequestBuilder.buildRequest(requestID, identityProvider.getAuthnRequestURL(), certificationKeyStorePath, password, certificateAlias);

</cfscript>

<cfoutput>
	<form action="#identityProvider.getAuthnRequestURL()#" id="SAMLRequest" method="post">
       	<input type="hidden" name="RelayState" value="#attributes.RelayState#"/>
		<input type="hidden" name="SAMLRequest" value='#toBase64(toString(SAMLRequestXML), "utf-8")#'/>
		<cfif not attributes.autolaunch>
			<!---Not CF_relayFormElement as we need the form-control class--->
			<cfoutput>
				<input class="form-control btn btn-primary" name="signInWithRelayware" value="#attributes.launchButtonText#" id="signInWithRelayware" type="submit">
			</cfoutput>
		</cfif>
	</form>
	
	<cfif attributes.autolaunch>
		<script>
				setTimeout(function() {
				    jQuery('##SAMLRequest').submit();
				},#attributes.autoLaunchDelay*1000#);
		</script>

	</cfif>

</cfoutput>

