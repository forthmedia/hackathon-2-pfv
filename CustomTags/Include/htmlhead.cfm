<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">
<!---
2012/03/15	IH This tag is a wrapper around cfhtmlhead with the added benefit of checking if "text" is already included. 
--->

<cfparam name="attributes.text" default="">

<cfif thisTag.executionMode is "start">
	<cfif !structKeyExists(request,"stHTMLHead")>
		<cfset request.stHTMLHead={}>
	</cfif>
	
	<cfif !structKeyExists(request.stHTMLHead,attributes.text)>
		<cfset request.stHTMLHead[attributes.text]=1>
		<cfhtmlhead text="#attributes.text#">		
	</cfif>
</cfif>
<cfsetting enablecfoutputonly="no">
