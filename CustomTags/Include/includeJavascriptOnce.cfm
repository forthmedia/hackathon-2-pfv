<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2005-02-07   Template based on includeonce.cfm to allow a javascript file to be included
			via cfhtmlhead and only included once.

			requires attribues.template
			Can either be a path relative to the relay root (in which case http://xyz.com/#urlroot# is pre-pended
			or a full path with http:// at the beginning


WAB 2008/01/25  changes so that secure protocol can be used if necessary


WAB  2010/03/25 added translate functionality
WAB 2100/06/25 added mergeStruct to the translate functionality
WAB 2011/05/10 discovered that do not need to have http: or https:, can just use //.
		This means we do not have to worry about secure/insecure.
		Actually we should just be able to do relative paths
NYB 2011-12-09 LHID8232 added test for structkeyexists(url,"openAsWord")
WAB 2012-02-16 Added caching of translations in .js files
WAB 2012-10-03 CASE 431031 added argument logMergeErrors to translation call, so don't log merge errors unless expecting to do merging
WAB 2012-12-05 CASE 430659 added anticaching number
WAB 2013-03-26 Added loadScript() function for ajax requests
WAB 2013-09-18 A better solution to CASE 431031 - don't try to evaluate merge variables if no mergeStruct.  Gets rid of exception in the debug
NJH 2015/03/16	work for PGI - add includeCustom attribute which allows us to include a custom version of the file to provide custom functionality.
WAB 2015-12-01	During PROD2015-290 Visibility Project. Added callbacks - initially used for loading the multiselect
WAB 2016-01-21	PROD2015-370 Fixed callbacks - which weren't working and add synchronousCallBack attribute (see comment next to attribute declaration)
GCC/DAN/NJH	2015/12/03 (committed 2016-01-22)	CASE 446496 Enable a custom template to be loaded and translated even if it isn't overriding a core js file
WAB 2016-01-22	Phrases not working in ajax mode (going into the head).  Renamed attributes.inhead to attributes.useHTMLHead (and negated) because previous name was awful (my fault!)
NJH 2016/11/11	Moved the code to a function in request.cfc
--->

<cfsetting enablecfoutputonly="true">

<cfif (structKeyexists (url,"returnFormat") and url.returnFormat is "JSON")
		or
		structkeyexists(url,"openAsWord")
>
	<cfexit>
</cfif>

<cfif thisTag.executionMode is "start">
    <cfparam name="attributes.template" />
    <cfparam name="attributes.translate" default="false" />
    <cfparam name="attributes.mergeStruct" default="#structNew()#" />
    <cfparam name="attributes.useHTMLHead" default="true"/>
    <cfparam name="attributes.callback" default=""/>
    <cfparam name="attributes.synchronousCallback" default="false" type="boolean"/> <!--- If you have an Ajax request makes calls this tag twice for the same js file with two callbacks, then the second callback will fail.  Until I recode completely to collect all the calls and output them at the end, we have to go with a synchronous load --->
	<cfparam name="attributes.includeCustom" default="#not listContainsNoCase(attributes.template,'code','/')? true:false#"/> <!--- don't include if this is a custom file already --->

	<cfset application.com.request.includeJavascriptOnce(argumentCollection=attributes)>
</cfif>

<cfsetting enablecfoutputonly="false">