<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/06/14
Include a file if it exists - and optionally include another file if it does not
NOTE: only works for absolute paths, won't work for paths relative to the current directory
Useful for including files in the code directories without having to do lots of fileexists()
Now extended to do same as includeWithCheckPermissionAndIncludes - but rather better!

2012/03/13 PPB/WAB AVD001 added support for includeOnce 
2012-03-26  AVD001 added support for includes within the customTags directories
2012-02-07 PPB/WAB setting caller variables to cope with a query vble and a variables vble in the caller with the same name
2014-10-12 NJH - did a few changes, so that we check whether file exists on fullpath, rather than expanding attributes.template. This was to deal
			with trying to include files in custom tags. expanding the template value in this case did not evaluate properly
--->
	<cfparam name="attributes.template" >
	<cfparam name="attributes.onNotExistsTemplate" default="">
	<cfparam name="attributes.checkIfExists" default="#iif(attributes.onNotExistsTemplate is not "",true,false)#">  <!--- if onNotExistsTemplate is set then automatically have to check for existence--->
	<cfparam name="attributes.doDirectoryInitialisation" default="false">
	<cfparam name="attributes.applySecurity" default="false">
	<cfparam name="attributes.includeOnce" default="false">	<!--- 2012/03/13 PPB/WAB AVD001 Will added this facility to avoid using includeOnce --->

	<cfparam name="request.TemplateIncluded" default="#structNew()#">

	<cfset local_=structNew()>	<!--- localise some variables so they don't get passed back into the caller scope --->

	
<cfif thisTag.ExecutionMode is "start">

	<!--- START: 2012/03/13 PPB/WAB AVD001 --->
	<cfif attributes.includeOnce>
		<!--- WAB 2012-03-26 added code to deal with use within the customTags directory (where expand path does not work)--->		
		<cfif listFirst(attributes.template,"\/") is "customtags">
			<cfset fullPath = application.paths.customTags & "\" & listRest(attributes.template,"\/")>
			<cfset attributes.template = "..\" & listRest(attributes.template,"\/")>
		<cfelse>
			<cfset fullPath = expandPath(attributes.template)>
		</cfif>

		<cfif structKeyExists(request.templateIncluded, fullPath)>
			<cfexit  method="exitTag">
		<cfelse>
			<cfset request.templateIncluded[fullPath] = "true">
		</cfif>
	<cfelse>
		<cfset fullPath = expandpath (attributes.template)>
	</cfif>           
	<!--- END: 2012/03/13 PPB/WAB AVD001 --->

	<cfif attributes.checkIfExists>
		<cfif not fileExists (fullPath)> 
			<cfif attributes.onNotExiststemplate is not "">
				<cfset 	attributes.template = attributes.onNotExistsTemplate>
			<cfelse>
				<cfexit method="EXITTAG"> 
			</cfif>	
		</cfif>
	</cfif>

	<!--- Copies all the variables in the caller scope into the local variable scope, otherwise would not behave like cfinclude --->
  	<cfloop item="local_.variablename" collection= #caller#>
		<cfif not listfindnocase ("attributes,caller",local_.variablename)>
			<cfset variables[local_.variablename] = caller.variables[local_.variablename]>		<!--- 2012-02-07 PPB/WAB was caller[local_.variablename]  --->
		</cfif>	
	</cfloop>   

	<cfif attributes.doDirectoryInitialisation>
		<cfset local_.securityObj = application.com.security>
		<cfset local_.securityResult = local_.securityObj.getFileSecurityRequirements(scriptname = attributes.Template )> 
		<cfif attributes.applySecurity and not local_.securityResult.isOK>
			<cfoutput>You do not have access to this file</cfoutput>
			<CF_ABORT>
		</cfif>
	
		<!--- This piece of code identical to doCheckPermissionAndInclude, but I didn't want to do another include --->
		<cfif local_.securityResult.security is not "" and local_.securityResult.security is not "none">
			<cfset local_.securityTask = listFirst (local_.securityResult.security,":")>
			<cfset local_.securityLevel = listLast (local_.securityResult.security,":")>
			<cfset checkPermission = application.com.login.checkInternalPermissions (local_.securityTask,"",local_.securityLevel)>   <!--- this will get passed back to caller scope --->
		</cfif>
					
		<cfif arrayLen(local_.securityResult.includeFiles) gt 0>
			<cfloop index="local_.i" from = "1" to = #arrayLen(local_.securityResult.includeFiles)#>
				<cftry>
						<cfinclude template = "#local_.securityResult.includeFiles[local_.i]#">
						<cfcatch>
						</cfcatch>
				</cftry>	
			</cfloop>
		</cfif>	

		<cfset thisDir = local_.securityResult.thisDir>		<!--- this will get passed back to caller scope --->
	</cfif>



	<cfinclude template = "#attributes.template#">

	<!--- get any variables set in this template back into caller scope 
		NJH 2012/08/22 CASE 430260 - for some reason, the variables scope had a key of "  ", which then caused this to fail, as it wasn't a valid variable name
			not sure how it got to be in the variables scope if it's not a valid variable name!!!
	--->
	<cfloop item="variablename" collection= #variables#>
		<cfif not listfindnocase ("attributes,caller,local",variablename) and trim(variableName) neq "">
			<cfset caller[variablename] = variables[variablename]>
		</cfif>

	</cfloop> 
	
</cfif>
