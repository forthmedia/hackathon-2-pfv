<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	2015/11/13 NJH	Jira Prod2015-231 A little re-work around the header. Put the header div in here, as well as outputing the document H1.
--->
<cfset currentDirPath = replace(cgi.script_name,listLast(cgi.script_name,"/"),"")>
<cfset currentDir = replace(currentDirPath,"/","","ALL")>

<cfif structKeyExists(request.tophead,"showTopHead") and not request.topHead.showTopHead>
	<cfset topHead = "">
<cfelse>
	<cfif not structKeyExists(request.topHead,"topHeadcfm")>
		<cfset topHead = currentDir &"TopHead.cfm">
		<cfif not fileExists(expandPath(topHead))>
			<cfset topHead = "">
		</cfif>
	<cfelse>
		<!---<cfset topHead = replace(request.topHead.topHeadcfm,"/","")>--->
		<cfset topHead = request.topHead.topHeadcfm>

		<cfif listFindNoCase("code,relay,customTags",listFirst(request.topHead.topHeadcfm,"\/"))>
			<cfset currentDirPath="">
		</cfif>
	</cfif>
</cfif>

<!--- if the document h1 text hasn't been specifically set, then use menu item name if it exists. Otherwise, use the page title. If that's not set, then use the directory name as a default --->
<cfif application.com.request.getDocumentH1() eq "">
	<cfset menuItem = application.com.relaymenu.getMenuItemByUrl()>
	<cfif structCount(menuItem) and ltrim(menuItem.tabText) neq "">
		<cfset application.com.request.setDocumentH1(text="phr_sys_nav_#replace(lTrim(menuItem.tabText),' ','_','ALL')#")>
	<cfelse>
		<!--- NJH 2015/12/30 BF-127 - only camelcase the current dir... anything else means that we potentially lose our phraseTextID --->
		<cfset application.com.request.setDocumentH1(text=structKeyExists(request.topHead,"pageTitle")?request.topHead.pageTitle:application.com.regExp.camelCaseToSpaces(string=currentDir))>
	</cfif>
</cfif>

<!--- NJH - 2015/11/13 - have hard-coded this exception for now. It may be that other pages also can't have the header, in which case a function will be created. --->
<cfif cgi.script_name neq "/index.cfm">
<cfoutput>
<div class="header">
	<cfset application.com.request.outputDocumentH1()>

	<cfif tophead neq "">
		<cfif not structKeyExists(request.topHead,"queryString")>
			<cfset request.tophead.queryString = request.QUERY_STRING>
		</cfif>

		<cfloop collection="#request.topHead#" item="topHeadVar">
			<cfif structKeyExists(request.topHead,topHeadVar)>
				<cfset setvariable(topHeadVar,request.topHead[topHeadVar])>
			</cfif>
		</cfloop>
		<cf_include template="#currentDirPath##topHead#" checkIfExists="true" includeOnce="true">
	</cfif>
</div>
</cfoutput>
</cfif>