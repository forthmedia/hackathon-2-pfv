<!---

File name:			includeCssOnce.cfm
Author:				NJH
Date started:		2014/06/11
Description:		Css version of include template once

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016-01-21 			WAB			PROD2015-370 Added support for correct behaviour during ajax requests (ie don't put in head)
								Could possibly do same as include_javascriptonce where we check for existence
2016-01-22			WAB 		Renamed attributes.inhead to attributes.useHTMLHead (and negated) because previous name was awful (my fault!)
2016/11/11			NJH 		Moved the code to a function in request.cfc

Possible enhancements:

--->

<cfsetting enablecfoutputonly="true">

<cfif thisTag.executionMode is "start">
    <cfparam name="attributes.template" />
    <cfparam name="attributes.useHTMLHead" default="true"/>
	<cfparam name="attributes.checkIfExists" default="false"/>
	<cfparam name="attributes.includePortalcss" default="#not request.relayCurrentUser.isInternal and not listContainsNoCase(attributes.template,'code','/') and left(attributes.template,1) eq '/'?true:false#"/>
	<cfparam name="attributes.rel" default="stylesheet">

	<cfset application.com.request.includeCSSOnce(argumentCollection=attributes)>
</cfif>

<cfsetting enablecfoutputonly="false">