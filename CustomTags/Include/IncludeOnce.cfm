<!--- �Relayware. All Rights Reserved 2014 --->
<!---
The includeonce tag works very much like cfinclude, however it
ensures that files are only included one time in the course of a
request. For instance, if you make a request for file "a" which
includes file "b" and file "c", and file "b" also includes file
"c", the includeonce tag will ensure that file "c" is only included
once instead of twice.

@attribute template (required) The template you want to include, just
           like you were using cfinclude.

2004-09-13 WAB there is problem with using this template in that it seems to screw up 
non standard characters (such as accented) elsewhere on a page.  
problem seemed to be caused by  getPageContext().include(attributes.template) so I have replaced it with cfinclude

2005-02-07	WAB altered the variable that the list of included files is stored in  - made it a structure below request - just for neatness
2005-04-19  WAB altered so that can check for existence of the file before trying to include it.  Needs to be given the absolute path to the template

--->

<cfif thisTag.executionMode is "start">
    <cffunction name="cfinclude" access="public">
        <cfargument name="template" required="true" />
        <cfinclude template="#arguments.template#" />
    </cffunction>
    <cfparam name="attributes.template" />
	<cfparam name="attributes.checkexistence" default= "false"> <!--- if set to true then need to give the path of the template (although I am sure that there must be some way of finding it out from the system)--->
	<cfparam name="attributes.path" default= "">
	 
	

		<cfif not isDefined("request.templateIncluded")>
			<cfset request.templateIncluded = structNew ()>
		</cfif>
	

       <!---  Get the full path to the file being included. The full
        path is the key we use to see if it has been included
        previously.
        --->
        <cfset fullPath = expandPath(attributes.template)>
        
        <!---  Check to see if the key already exists in the request
         scope. Only include the file if it does not exist.
         --->
		
        <cfif ((structKeyExists(request.templateIncluded, fullPath) is false) or (request.templateIncluded[fullPath] neq "true"))>
        
      	    <cfset request.templateIncluded[fullPath] = "true">
			<cfif not attributes.checkExistence  or (fileexists(attributes.path))>
			
		    <cfinclude template = "#attributes.template#"> 

			</cfif>

		</cfif> 
 


</cfif>








<!--- 	<cfscript>
        //
        // Get the full path to the file being included. The full
        // path is the key we use to see if it has been included
        // previously.
        //
        fullPath = expandPath(attributes.template);

        //
        // Check to see if the key already exists in the request
        // scope. Only include the file if it does not exist.
        //
        if ((structKeyExists(request, fullPath) is false) or (request[fullPath] neq "true"))
        {
            request[fullPath] = "true";
            getPageContext().include(attributes.template);
        }
    </cfscript>
 --->	

