<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		widgetListDisplay.cfm
Author:
Date created:

Amendment History:

DD-MMM-YYYY	Initials 	What was changed
11/-1/2012		RMB		P-LEN024 - CR066 - Added more logic to expand widget display more columns
											Also added "linktype" and "changetabtext"
--->

<cfparam name="attributes.widgetData">
<cfparam name="attributes.displayRecords" default="">

<cfif attributes.displayRecords is "">
	<cfset attributes.displayRecords = attributes.widgetData.recordCount>
</cfif>
<cfparam name="attributes.displayRecords" default="">

<TABLE cellpadding="0" cellspacing="0" class="W_Table">
	<cfoutput query="attributes.widgetData" maxrows="#attributes.displayRecords#">
	<TR>
		<cfloop from="1" to="#colAmount#" index="i">
			<cfset displayText = evaluate("displayText#i#")>
			<cfset link = evaluate("link#i#")>
			<cfset icon = evaluate("icon#i#")>
			<!--- 11/01/2012 - RMB - P-LEN024 - CR066 ---->
			<cfif ISDefined("linktype#i#")>
				<cfset linktype = evaluate("linktype#i#")>
			<cfelse>
				<cfset linktype = "">
			</cfif>

			<cfif ISDefined("changetabtext#i#")>
				<cfset chgtabtext = evaluate("changetabtext#i#")>
			<cfelse>
				<cfset chgtabtext = "">
			</cfif>

			<!--- <cfif request. relayCurrentVersion.UIVersion eq 2> --->
			<!--- <cfif colAmount GTE 3 AND i EQ 1>
				<cfset AddStyleToRow = "width:185px">
			<cfelseif colAmount GTE 3 AND i GTE 2>
				<cfset AddStyleToRow = "width:16px">
			<cfelse>
				<cfset AddStyleToRow = "">
			</cfif>--->

			<td>
				<!--- 11/01/2012 - RMB - P-LEN024 - CR066 ---->

				<cfif chgtabtext NEQ ''>
				  <cfset HolddisplayText = displayText>
				  <cfset displayText = "#chgtabtext#">
				</cfif>

				<cfif isDefined("iconClass#i#")>
					<cfset iconClass = evaluate("iconClass#i#")>
				<cfelse>
					<cfset iconClass = "">
				</cfif>

				<cfif linktype IS ''>
				  <cfsavecontent variable="UseThisLink">javaScript:void(parent.manageClick('#jsStringFormat(replace(displayText,"_","","ALL"))#','#jsStringFormat(displayText)#','#jsStringFormat(link)#',true,{reuseTab:true,iconClass:'#iconClass#'}))</cfsavecontent>
				<cfelseif linktype IS  'PopUp'>
				  <cfsavecontent variable="UseThisLink">javaScript:void(window.open('#jsStringFormat(link)#','SendComToSelflink','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0'))</cfsavecontent>
				<cfelseif linktype IS  'JSLink'>
				  <cfsavecontent variable="UseThisLink">javaScript:void(#jsStringFormat(link)#)</cfsavecontent>
				<cfelse>
				  <cfsavecontent variable="UseThisLink">javaScript:alert('Unrecognized Link Type')</cfsavecontent>
				</cfif>

				<cfif chgtabtext NEQ ''>
				  <cfset displayText = HolddisplayText>
				</cfif>

				<!--- 11/01/2012 - RMB - P-LEN024 - CR066 ---->
				<cfif link neq "">
					<a href="##" onclick="#UseThisLink#" <cfif i eq 1> class="W_ArrowIcon"</cfif>>
				<cfelse>
					<div <cfif i eq 1>class="W_ArrowIcon"</cfif>>
				</cfif>

				<cfif icon neq "">
					<img src="#icon#" border="0" alt="#displayText#">
				<cfelseif displayText neq "">
				#displayText#
				</cfif>

				<cfif link neq "">
					</a>
				<cfelse>
					</div>
				</cfif>
			</td>
			<!--- <cfelse>
				<td><cfif link neq ""><a href="#link#"<cfif i eq 1> class="W_ArrowIcon"</cfif>><cfelse><div <cfif i eq 1>class="W_ArrowIcon"</cfif>></cfif><cfif icon neq ""><img src="#icon#" border="0"><cfelseif displayText neq "">#displayText#</cfif><cfif link neq ""></a><cfelse></div></cfif></td>
			</cfif> --->
		</cfloop>
	</TR>
	</cfoutput>
</TABLE>