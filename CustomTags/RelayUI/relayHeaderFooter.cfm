<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		relayHeaderFooter.cfm
Author:			NJH
Date started:	10-08-2009

Description:	Custom tag to provide a generic header/footer for an internal page.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2010-04-28			NJH			P-PAN002 - don't run tag twice if nested.
2012-03				NJH			DocType, Head etc now done in onRequestEnd
2012-04				WAB			But we had fogotten to add code to put deal with <body> attributes
Possible enhancements:


 --->

<cfsetting enablecfoutputonly="true">

<cfset parentTags = listrest(getbasetaglist())>

<cfif not listFindNoCase(parentTags,"cf_relayHeaderFooter")>

<cfparam name="attributes.title" type="string" default="#replace(script_name,'.cfm','')#">
<cfparam name="attributes.currentDir" type="string" default="#replace(cgi.script_name,listLast(cgi.script_name,'/'),'')#">
<cfparam name="attributes.showLoadingIcon" type="boolean" default="false">
<cfparam name="attributes.onload" type="string" default="">
<cfparam name="attributes.onClick" type="string" default="">
<cfparam name="attributes.onContextMenu" type="string" default="">
<cfparam name="attributes.onMouseMove" type="string" default="">
<cfparam name="attributes.topHead" type="string" default="#replace(attributes.currentDir,"/","","ALL")#TopHead.cfm">
<cfparam name="attributes.styleSheets" type="string" default="/styles/relaywareStylesV2.css">
<cfparam name="attributes.showFavIcon" type="boolean" default="false">
<cfparam name="attributes.showFooter" type="boolean" default="true">
<cfparam name="attributes.openAsExcel" type="boolean" default="false">
<cfparam name="attributes.topHeadVars" type="struct" default="#structNew()#">


<cfif thisTag.executionMode is "start">

	<cfif not attributes.openAsExcel>
		<cfoutput>
		<!--- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
				<cf_title>#htmleditformat(attributes.title)#</cf_title> --->

				<cf_includejavascriptOnce template="/javascript/viewEntity.js">
				<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

				<cfif not request.relayCurrentUser.isInternal>
					<!--- <cfif not isdefined("request.StylesLoaded") or (isDefined("request.StylesLoaded") and not request.StylesLoaded)>
						<cfhtmlhead text='<link href="#request.currentSite. stylesheet#" rel="stylesheet" media="screen,print" type="text/css">'>
						<cfset request.StylesLoaded=true>
					</cfif> --->
				<cfelse>
				<cfloop list="#attributes.styleSheets#" index="stylesheet">
					<cfoutput>
						<cfhtmlhead text='<link href="#htmleditformat(stylesheet)#" rel="stylesheet" media="screen,print" type="text/css">'>
					</cfoutput>
				</cfloop>
				</cfif>

				<cfif attributes.showFavIcon>
					<cfoutput><cfhtmlhead text='<link rel="shortcut icon" type="image/ico" href="/code/borders/internal/favicon.ico">'></cfoutput>
				</cfif>


			<!--- </cf_head>

			<cf_body onload="<cfif attributes.showLoadingIcon>setShowLoadingIconForDocLinks();</cfif>#htmleditformat(attributes.onload)#" onclick="#htmleditformat(attributes.onClick)#" oncontextmenu="#htmleditformat(attributes.onContextMenu)#" onMouseMove="#htmleditformat(attributes.onMouseMove)#">
 --->		</cfoutput>

			<!--- Send attributes which were on the BODY through to request.cfc --->
			<cfif attributes.onload is not "">
				<cfset application.com.request.setBodyAttribute("onload",attributes.onload)>
			</cfif>
			<cfif attributes.onClick is not "">
				<cfset application.com.request.setBodyAttribute("onClick",attributes.onClick)>
			</cfif>
			<cfif attributes.onContextMenu is not "">
				<cfset application.com.request.setBodyAttribute("onContextMenu",attributes.onContextMenu)>
			</cfif>
			<cfif attributes.onMouseMove is not "">
				<cfset application.com.request.setBodyAttribute("onMouseMove",attributes.onMouseMove)>
			</cfif>

			<cfif attributes.topHead neq "">
				<cfif fileExists("#application.paths.relay#\#replace(attributes.currentDir,'/','\','ALL')#\#attributes.topHead#")>
					<!--- set for backwards compatibility --->
					<cfset thisDir = attributes.currentDir>
					<cfset current = listLast(cgi.script_name,'/')>
					<cfset queryString = request.QUERY_STRING>
					<cfif isDefined("caller.queryString")>
						<cfset queryString = caller.queryString>
					</cfif>

					<cfloop collection="#attributes.topHeadVars#" item="var">
						<cfset setvariable(var,attributes.topHeadVars[var])>
					</cfloop>
					<table>
						<tr><td>
					<cfinclude template="/#attributes.currentDir#/#attributes.topHead#">
						</td></tr>
					</table>
				</cfif>
		</cfif>

	<!---
		CASE 426033 - remove from here as TFQO deals with this and having this done twice breaks the report in new browser versions
	<cfelse>
		<cfcontent type="application/msexcel">
		<cfheader name="content-Disposition" value="filename=#replace(listLast(script_name,'/'),'.cfm','')#.xls">
		<cfinclude template="/templates/excelHeaderInclude.cfm"> --->
	</cfif>

<cfelseif thisTag.executionMode is "end" and not attributes.openAsExcel>
		<!--- <cfif attributes.showFooter and request.relayCurrentUser.isInternal>
			<CFINCLUDE TEMPLATE="/templates/InternalFooter.cfm">
		</cfif>
		<cfoutput>


		</cfoutput> --->
</cfif>

</cfif>
<cfsetting enablecfoutputonly="false">
