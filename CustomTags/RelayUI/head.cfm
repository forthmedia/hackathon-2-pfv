<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="true">

<cfif thisTag.executionMode is "end">
	
	<!---Start 2012-07-26  PJP - Case #429715 Part of Fix, custom tag so that generated output is blanked for htmlhead type, otherwise duplicated --->
	<cfsavecontent variable="thisGeneratedContent">
		<cf_translate>
			<cfoutput>#thisTag.generatedContent#</cfoutput>
		</cf_translate>
	</cfsavecontent>
	<cfif not application.com.request.isAjax()>
		<cfhtmlhead text="#thisGeneratedContent#">
	<cfelse>
		<cfoutput>#thisGeneratedContent#</cfoutput>
	</cfif>
	
	<cfset thisTag.generatedContent="">
	<!---End 2012-07-26  PJP - Case #429715 Part of Fix, custom tag so that generated output is blanked for htmlhead type, otherwise duplicated --->
	
</cfif>
<cfsetting enablecfoutputonly="false">