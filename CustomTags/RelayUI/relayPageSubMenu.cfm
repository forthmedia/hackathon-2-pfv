<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayPageSubMenu.cfm	
Author:				SWJ
Date started:		2007-05-27
	
	Description...		
		
		A standard page sub-menu tag for normal pages

	Objective...
		
		To provide to provide a standard navigation header for all of the Relay screens.
		
	How it works...
		
		It simply draws a standard submenu table.
		
	Syntax...
		<CF_relayPageSubMenu pageTitle="TestPage" showBackButton="true"  linkURL="linkURL"  linkText="linkText" linkTarget="default" >
	
	Parameters...
	  	hideSubMenu - this is used to hide the tag under some circumstances
		pageTitle - shows a title for the page
		showBackButton - shows a  

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Possible enhancements:


 --->
 
<cfparam name="attributes.hideSubMenu" type="boolean" default="false"> <!--- either yes or No --->
<CFPARAM NAME="attributes.pageTitle" TYPE="string" DEFAULT="">
<cfparam name="attributes.showBackButton" type="boolean" default="false"> <!--- available in case the root is not URLRoot --->
<CFPARAM NAME="attributes.linkURL" TYPE="string" DEFAULT=""> <!--- this is available in case the form is being included from another directory --->
<CFPARAM NAME="attributes.linkText" TYPE="string" DEFAULT=""> <!--- this causes the upper menu 'tag' to be selected as current --->
<CFPARAM NAME="attributes.linkTarget" TYPE="string" DEFAULT=""> <!--- this causes the upper menu 'tag' to be selected as current --->

<cfif attributes.hideSubMenu eq false>

	<cfoutput>
		<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
			<TR class="Submenu">
				<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">#htmleditformat(attributes.pageTitle)#</TD>
				
				<cfif attributes.showBackButton or listLen(attributes.linkURL) gt 0>				
							<TD ALIGN="right" VALIGN="TOP" class="Submenu">
								<cfif attributes.showBackButton>
									<a href="javascript:history.back()" class="Submenu">Back</a> |  
								</cfif>
								<cfif (listLen(attributes.linkURL) + listLen(attributes.linkText) + listLen(attributes.linkTarget))/3 eq listLen(attributes.linkURL)>
									<cfloop index="i" list="#attributes.linkURL#">
										<cfset thisText = gettoken(attributes.linkText,listFindNoCase(attributes.linkURL,i),",")>
										<cfset thisTarget = gettoken(attributes.linkTarget,listFindNoCase(attributes.linkURL,i),",")>
										<a href="#i#" class="Submenu" <cfif thisTarget neq "default">target="#htmleditformat(thisTarget)#" </cfif>>#htmleditformat(thisText)#</a>
									</cfloop>
								</cfif>
							</TD>
				</cfif>
			</TR>
		</TABLE>
	</cfoutput>

</cfif>