<!--- �Relayware. All Rights Reserved 2014 --->
<!--- OVERVIEW
custom tag to bring together some of the screens functionality
this bit is concerned with outputting stuff as defined in a single screen in the screen definition table


parameters:
	ScreenID    - either id or textid  (required)
	ScreenQuery  - if query has already been run then can be passed.
	#entityType#  - (optional) a parameter with the name of an entitytype, which points to query defining that entity type
					eg.  Person  =  #getPerson#  Location = #getLocation#   note query passed with ##
					or Person  =  #getPersonAndLocation#  Location = #getLocationAndPerson#
					If you don't have a query (and don't need to refer to a query) then the entityId
					can be passed as a parameter of the form #entityType#ID
					eg.  personid = 1, countryid = 9
	method - 		defaults to view can also be edit

Usage examples:

	<CF_aScreenItem ScreenID="#application. defaultOrgProfileScreen#" organisation=#getOrganisation# method="edit">
		In this example the form variable was set using <cfset form.frmEntityType = "organisation">
		and the getOrganisation query was run to make sure the correct data for the entity was available i.e.
			<CFQUERY NAME="getOrganisation" DATASOURCE="#siteDataSource#">
				select * from organisation where organisationID = #getLocation.organisationID#
			</CFQUERY>

	<CF_aScreenItem screenid="512" ordersid=#frmorderid# method="edit">


--->

<!--- Modification History:
2001-08-14	SWJ		Added Usage examples to the notes above.
2005-07-06	WAB		problem if countryid passed in - was getting countryid from organisation - which is wrong
2005-09-13	WAB 	added preinclude code
2006-03-06	WAB		- replaced application.typeEntityTable with application.entityType
2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup
2011/05/25	NYB		REL106 added support for non PLO entities - to make screen code more generic
2011-09-02 	NYB 	LHID7693 reset DisplayAs to "" (from twoselect - which it was being stuck in)
2011/09/27	WAB		P-TND109 create screenFormattingCollection to pass to showScreenTable.
					This structure was already being defaulted in showScreenTable.cfm and was supposed to be created from screen.parameters and passed from here, but wasn't
					Also any values passed in in attributes.parameters are added to this collection
2013-01-08 	PPB 	Case 432409 re-worked an assignment construction which was causing an non-reproducable error for Avid
2013-07-09 	NYB 	Case 435968
2013-08-09	PPB 	Case 436529 added defence against frmCountryID='' in conjunction with 435968
2014-10-27	RPW		Allow for viewer being blank
2016-03-09 	WAB	 PROD2016-684 Added attribute divLayout, default to stacked but can be overridden on a screen by screenbasis
					Changed name of the screendefinition query so that it could be referred to in showScreenTable.cfm
--->

<CFSETTING enablecfoutputonly="yes">


<cfset parentTag = getBaseTagData("CF_ASCREEN")>
<cfset thisFormName = parentTag.attributes.FormName>
<CFPARAM name="attributes.screenid" >    <!--- can be either a screenid or a screenetxtid--->
<CFPARAM name="attributes.screenQuery" default = "">  <!--- if the query has already been run , then pass a pointer to it--->
<CFPARAM name="attributes.parameters" default = "">
<CF_EvaluateNameValuePair  NameValuePairs="#attributes.parameters#" collectionName="aScreenParametersStructure">

<CFPARAM name="attributes.method" default = "view">
<CFPARAM name="attributes.hideLabels" default = "false">

<CFPARAM name="attributes.divLayout" default = "stacked"> <!--- or horizontal --->

<!--- Loop through all the possible entityTypes to check whether any parameters have been passed for that entityType--->
<CFSET entitiesPassed = ""> <!--- variable to record which entities have been passed --->
<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
<CFSET entitiesIDPassed = "">
<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

<CFLOOP collection = "#application.entitytype#" item = "entityKey">
	<cfset entity = application.entitytype[entityKey].tablename>
	<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
	<cfset variables.entityUniqueKey = application.entitytype[entityKey].uniquekey>
	<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
	<!--- is there a query being passed for this entityType--->
	<CFIF structKeyExists(attributes,entity)>
		<!--- add this entityType to list--->
		<CFSET entitiesPassed = listappend(entitiesPassed,entity)>
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<CFSET entitiesIDPassed = listappend(entitiesIDPassed,entityKey)>
		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

		<!--- pass query --->
		<CFSET "#entity#" = attributes[entity]>

		<!--- get entityid from query --->
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<CFSET entityID = evaluate("#entity#.#variables.entityUniqueKey#")>
		<CFSET "#variables.entityUniqueKey#"=entityID>
		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

		<!--- get lastUpdated from query and save --->
		<!--- START 2011/05/25 NYB REL106 added if around lastUpdated --->
		<CFIF isdefined("#entity#.lastUpdated")>
			<CFSET "caller.#entity#_lastupdated_#entityID#"= evaluate("#entity#.lastUpdated")>
		</CFIF>
		<!--- END 2011/05/25 NYB REL106 --->

	<!--- no query being passed, but there is an id being passed for this EntityType--->
	<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
	<CFELSEIF variables.entityUniqueKey neq "" and structKeyExists(attributes,variables.entityUniqueKey)>
	<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<!--- add this entityType to list--->
		<CFSET entitiesPassed = listappend(entitiesPassed,entity)>
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<CFSET entitiesIDPassed = listappend(entitiesIDPassed,entityKey)>
		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

		<!--- set entityId --->
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

		<!--- START: 2013-01-08 PPB Case 432409 removed the use of evaluate just in case it contributed to the non-reproducable error
		<CFSET entityID = evaluate("attributes.#variables.entityUniqueKey#")>
		--->
		<CFSET entityID = attributes[variables.entityUniqueKey]>
		<!--- END: 2013-01-08 PPB Case 432409 --->
		<CFSET "#variables.entityUniqueKey#"= entityID>

		<!--- START: 2013-01-08 PPB Case 432409 this construction was causing an non-reproducable error for Avid so I have attempted re-working it
		<CFSET "#entity#.#variables.entityUniqueKey#"= entityID>
		 --->
		<CFSET prefixedVariableName = "#entity#" & "." & "#variables.entityUniqueKey#">
		<CFSET  "#prefixedVariableName#" = entityID>
		<!--- END: 2013-01-08 PPB Case 432409 --->

		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

		<!--- set lastUpdated to "" --->
		<CFSET "caller.#entity#_lastupdated_#entityID#"= "">

		<!--- special case for orders  --->
		<CFIF entity is "orders">
			<CFSET "orderid"=attributes["#entity#id"]>
			<CFSET "order.orderid"=attributes["#entity#id"]>

		</CFIF>

	</cfif>

</cfloop>

<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
<CFSET variables.eCount = 0>
<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

<!--- set various parameters based on the entities which have been passed --->
<CFLOOP index="Entity" list=#entitiesPassed#>
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<CFSET variables.eCount = variables.eCount+1>
		<cfset variables.entityUniqueKey=application.entitytype[listgetat(entitiesIDPassed,variables.eCount)].uniquekey>

		<CFSET "frm#variables.entityUniqueKey#"=evaluate("#variables.entityUniqueKey#")>   <!--- should change code so only needs one of personid and frmPersonid--->
		<CFSET "#entity#forbiddenfields"= "">   <!--- hack - make this an application variable or something  --->
		<CFSET "#entity#includefields"= "">   <!--- hack - make this an application variable or something  --->
		<!--- create a caller variables of the form #entityType#flaglist etc. e.g. PersonFlagList--->
		<CFPARAM name="caller.#variables.entityUniqueKey#List" default = "">
		<CFPARAM name="caller.#entity#flagList" default = "">
		<CFPARAM name="caller.#entity#fieldList" default = "">
		<CFSET "#entity#flagList" = caller["#entity#flagList"]>
		<CFSET "#entity#fieldList" = caller["#entity#fieldList"]>   <!--- lists all the fields used, eg firstname, lastname --->
		<CFSET caller["#variables.entityUniqueKey#List"] = listappend(caller["#variables.entityUniqueKey#List"],evaluate("#variables.entityUniqueKey#"))>
		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
</cfloop>

<!--- 	code always needs a countryid,
		person/location records need correct countryID to show correct screen
--->
<cfif structKeyExists(variables,"countryid")>
	<!--- OK --->
<CFELSEIF (entitiesPassed contains "person" or entitiesPassed contains "location") >
	<CFSET variables.countryID = location.countryID>
<CFELSEIF entitiesPassed contains "organisation">
	<CFSET variables.countryID = organisation.countryID>
<CFELSEIF isDefined("attributes.countryID")>
	<CFSET variables.countryID = attributes.countryID>

<!--- NJH 2015/04/21 - brought this into the big cfif statement from below...--->
<cfelseif isdefined("frmCountryID") and frmCountryID neq ''>			<!--- 2013-08-09 PPB Case 436529 added the frmCountryID neq ''; it seems that this section might belong in the cfif above but here it overrides even when variables.countryID existed previously, so I'll leave it --->
	<CFSET variables.countryID = frmCountryID>
<CFELSE>
	<CFPARAM name="variables.countryid" default="0">
</cfif>

<CFSET screenid=attributes.screenid>
<CFSET frmMethod=attributes.method>
<CFSET hideLabels=attributes.hideLabels>

<!--- have we already run the query for the screenDefinition,
	or have we been passed the name of a query which has already been run ?
	If not, then query for it, otherwise just set the query variable
	--->

<CFSET screenqueryname = "screen_#screenid#_#variables.countryid#_#frmMethod#">
<!--- I've set this to a unique name -
		and saved it as a caller variable -
		effectively caches it within the calling template--->
<CFIF structKeyExists (caller,screenqueryname)>
	<!--- query already been run and cached in calling template, no action required  --->
<CFELSEIF attributes.screenQuery is "" >
	<!--- need to get definition, screenqueryname already defined above --->
	<CFPARAM name="frmreadonlyfields" default="''">   <!--- parameter can be passed eg: ='person_email','location_fax' --->
	<CFPARAM name="frmForbiddenFields" default = "''">
	<CFPARAM name="frmreadonlywithhiddenfields" default="''">
	<CFPARAM name="frmMethod" default="view">

	<cfset screenAndDefinition = application.com.screens.getScreenAndDefinition(
							screenid = screenid,
							countryid = countryid,
							method = frmMethod,
							readonlyfields = frmReadOnlyFields,
							forbiddenFields = frmforbiddenFields ,
							readonlywithhiddenfields =frmreadonlywithhiddenfields
							)>
	<!--- 	<CFINCLUDE template="qryScreenDefinition.cfm"> --->

	<cfset getScreen = screenAndDefinition.screen>
	<cfset caller[screenQueryName] = screenAndDefinition.Definition>
	<cfset caller.getScreen = getScreen>  <!--- WAB 2004-10-12 this was a hack so that if this screen has a postinclude parameter it can be tested by the calling page and included after the form has been closed --->
<CFELSE>
	<!--- we've been passed the query, just save it --->
	<CFSET caller[screenQueryName] = Caller["#Attributes.screenQuery#"]>
</cfif>

<cfset screenDefinitionQry = Caller[screenQueryName]>

<cfif isDefined("getScreen")>
	<!--- Include any files before the main form on a screen, useful if additional forms are required --->
	<CFLOOP index="filetoinclude" list="#getscreen.preinclude#">
		<CFINCLUDE template="#fileToInclude#">
	</cfloop>
	<!--- 2014-10-27	RPW	Allow for viewer being blank --->
	<cfif Len(getScreen.viewer) AND getScreen.viewer neq "showScreen">
		<cf_include Template="#getScreen.viewer#">
	</cfif>
</cfif>


<CFSET javascriptVerification = caller.javascriptVerification>
<CFSET formFieldList = caller.formFieldList>

<!--- parameters which need to be set before going into the screen stuff --->
<CFSET keepwithprevious = false>
<CFPARAM name="col1Width" default="">




<!--- loop through the screenDefinition --->
<cfset fn = application.com.redirectorAllowedFunctions>   <!--- WAB 2006-11-20 alias these functions so that they can be used in conditions and expressions --->
<cfset screenFormattingCollection = application.com.regExp.convertNameValuePairStringToStructure (inputString=getScreen.parameters,delimiter=",")>

<cfif structKeyExists (screenFormattingCollection,"divLayout")>
	<cfset divLayout = screenFormattingCollection.divLayout>
<cfelse>
	<cfset divLayout = attributes.divLayout>
</cfif>

<cfset structAppend (screenFormattingCollection,aScreenParametersStructure)>  <!--- aScreenParametersStructure overwrites the other --->

<cFLOOP QUERY="screenDefinitionQry" >
	<cfset DisplayAs = ""> <!--- 2011-09-02 NYB LHID7693 not sure if this is the best place to reset it, but seems to do the job without breaking anything else --->
	<cfinclude template="/screen/ShowScreenTable.cfm">
	<CFIF isDefined("breakScreen")>
		<CFBREAK>
	</cfif>
</cFLOOP>

<CFLOOP index="entity" list="#entitiesPassed#">
	<CFSET caller["#entity#flagList"] = evaluate("#entity#flagList")>
	<CFSET caller["#entity#fieldList"] = evaluate("#entity#fieldList")>
</cfloop>

<CFSET caller.javascriptVerification = javascriptVerification>

<CFSETTING enablecfoutputonly="no">

