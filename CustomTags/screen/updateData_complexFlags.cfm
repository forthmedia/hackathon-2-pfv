<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

updateData_ComplexFlags.cfm

Update procedure for complex flags, called from updatedata.cfm 


Author:  WAB

Date: Long ago


Mods:

March 2001	WAB added some code for flags which have multiple instances (such as notes) - 
2001-06-18		WAB Added code for checking for numeric fields during update (in particular needed for date fields)

march 2003 - DAM added some additional logic to cover problematic updates that do not have a pointer to
				a specific record and additional logic to cover failure if trigger returning ID


--->

<CFSET updateRequired = false>


				<!--- get details of this flag --->
				<CFQUERY NAME="GetFlag" datasource="#application.siteDataSource#">
				select
					*
				from
					flag as f,
					flaggroup as fg,
					flagtype as ft
				where
					<CFIF isNumeric(listgetat(flag,2,"_"))>
						FlagID =  <cf_queryparam value="#listgetat(flag,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					<CFELSE>	
						FlagTextID =  <cf_queryparam value="#listgetat(flag,2,"_")#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</CFIF>	
				and fg.flaggroupid = f.flaggroupid
				and fg.flagtypeid = ft.flagtypeid
				</CFQUERY>
			
				<CFSET tabletype = getFlag.dataTable>
				<CFIF GetFlag.SubEntityTypeID is not "">
					<CFSET SubEntityID = #listgetat(flag,3,"_")#>
				</CFIF>
				

				<!--- get list of the numeric, date, and nullable fields --->
				<CFSET tablerequired = #getFlag.datatableFullName#>
				<CFINCLUDE template="qryGetTableDefinition.cfm">


				<CFSET fieldList = "">
				<CFSET RE = "#flag#_.*_#formentityid#_Orig">  <!--- regular expression, search for the "orig" field since an unchecked checkbs will not show up otherwise --->

				<CFLOOP index="thisFormField" list="#form.fieldNames#">

				<!--- loop through all form fields looking for the ones related to this flag --->				
					<CFIF reFindNoCase("#RE#",thisFormField) GT 0 >
						<!--- found a form field related to this flag --->
						<!--- use this regular expression to extract the field name, could use listgetats, but since I introduced the idea of subEntities, you don't know the exact position in the list of the field name, without looking at the length of the list --->
						<CFSET field = 	reReplaceNoCase(thisFormField,"#flag#_(.*)_#formentityid#_Orig","\1")>
						<CFSET fieldList = listappend(fieldList, field)> <!--- extract the name of the field  and add name to a list --->
						<CFPARAM name="form.#flag#_#Field#_#formEntityID#" default="">  <!--- set a default (important in case of select boxes/checkboxes where the variable might not exist --->
					</CFIF>
				</CFLOOP>	
		
				<!--- need to check for any date fields which might be on the page 
						either need the day/month/year concatenating 
						or mm-dd-yyyy input converting
						similar code in updatedata 
						--->	
				<CFLOOP index="thisfield" list="#fieldList#">
					<cfif listFindNoCase(datefields,thisfield) IS  not 0>
						<!--- this is a date field 
							check for existence of day, month, year fields
							actually I assume that all are there if the day is there
							then check for format mm-dd-yyyy
						--->
						<cfset fieldname = "#flag#_#thisField#_#formEntityID#">
						<cfif isDefined("#fieldname#Day") and evaluate("#fieldname#Day") is not "">
							<cfset "#fieldname#" = CreateDateTime (evaluate("#fieldname#Year"),evaluate("#fieldname#Month"),evaluate("#fieldname#Day"),0,0,0)>
						<cfelseif isDefined("#fieldname#") and listlen(evaluate("#fieldname#"),"-") is 3 and len(trim(evaluate("#fieldname#"))) LTE 10>
							<cfset fieldValue = evaluate("#fieldname#")>
							<cfset "#fieldname#" = CreateDateTime (listgetat(fieldValue,3,"-"),listgetat(fieldValue,2,"-"),listgetat(fieldValue,1,"-"),0,0,0)> 
						</cfif>
					</cfif>
				</CFLOOP>


				<!--- 
					Make a structure of all the fields for this flag
					If the flag has multiple instances then we have a different structure for each
					
				--->
					<cfset fieldStructure = structNew()>
						<CFLOOP index="thisField" list="#fieldList#">
							<cfset tempidentityID = 0>
							<CFSET tempField = thisField>
							<CFIF GetFlag.hasMultipleInstances is 1 and listlen(thisField,"_") gte 2>				
								<CFSET tempidentityID = listgetat(thisField,1,"_")>
								<CFSET tempField = listrest(thisField,"_")>
							</cfif >

								<cfparam name="fieldStructure[tempidentityID]" default = #structNew()#>
								<cfparam name="fieldStructure[tempidentityID].fields" default = #structNew()#>
								<cfparam name="fieldStructure[tempidentityID].updateRequired"  default= false>
								<cfif tempfield is not "deleteItem">
									<cfset tempFIeldValue = evaluate("#flag#_#thisField#_#formEntityID#")>
									<cfparam name="fieldStructure[tempidentityID].fields[tempFIeld]" default = #structNew()#>								
									<cfset fieldStructure[tempidentityID].fields[tempField].value = tempFIeldValue> 
									<cfparam name="fieldStructure[tempidentityID].fields[tempField].updateRequired"  default= false>
									<CFIF evaluate("#flag#_#thisField#_#formEntityID#") is not evaluate("#flag#_#thisField#_#formEntityID#_orig")>
										<cfset fieldStructure[tempidentityID].updateRequired = true>
										<cfset fieldStructure[tempidentityID].fields[tempField].updateRequired = true>
									</CFIF>
								<cfelse>
									<cfif tempFIeldValue is 1>
										<cfset fieldStructure[tempidentityID].updateRequired = true>
										<cfset fieldStructure[tempidentityID].deleteItem = true>
									</cfif>
								</cfif>
								
						</CFLOOP>

				
			<cfloop collection = #fieldStructure# item="identity">


							
				<cfif fieldStructure[identity].updateRequired>
					<!--- get current Flag data --->
					<CFQUERY NAME="getFlagData" DATASOURCE=#application.sitedatasource# DEBUG>
					SELECT
						*
			  		FROM
						#tableType#FlagData
			 		WHERE
						EntityID =  <cf_queryparam value="#actualEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND flagID =  <cf_queryparam value="#getflag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					<CFIF GetFlag.SubEntityTypeID is not "">
					<!--- --->
					AND subentityid =  <cf_queryparam value="#subEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfif>
					<CFIF identity is not "" and identity is not 0>
						<CFIF isNUmeric(identity) and identity is not 0>
						and #getFlag.datatableFullName#id =  <cf_queryparam value="#identity#" CFSQLTYPE="CF_SQL_INTEGER" > 
						<CFELSE>
						and 1 = 0 <!--- new instance --->
						</CFIF>
					</CFIF>					
					</CFQUERY>



					<CFIF getFlag.WDDXStruct is not "">
						<!--- 
						*****
						This flag is stored as WDDX
						This section converts the form fields back into WDDX
						*****
						--->

							<CFWDDX ACTION="WDDX2CFML" INPUT="#getFlag.WDDXStruct#" OUTPUT="defaultStructure">						
						<CFIF getFlagData.recordcount is 0>
							<!--- new flag, create a blank array to put the results in  --->
							<cfset currentStructure = arrayNew(1)>  
						<CFELSE>
							<!--- existing flag, create a structure from the existing data  --->
								<cftry>
									<CFWDDX ACTION="WDDX2CFML" INPUT="#getFlagData.Data#" OUTPUT="currentStructure">
									<cfcatch>
										<CFWDDX ACTION="WDDX2CFML" INPUT="#getFlag.WDDXStruct#" OUTPUT="currentStructure">									
									</cfcatch>
								</cftry>


						</CFIF>

						<!--- some wddx structures are arrays, for ease of coding I am making them all arrays, even if I save it wihtout being an array--->
						<cfif not isArray (currentStructure)>
							<cfset x = arrayNew(1)>
							<cfset x[1] = currentStructure>
							<cfset currentStructure = x>  
							<cfset wddxarray = false>
						<cfelse>	
							<cfset wddxarray = true>
						</cfif>
						
						<!--- loop through field list updating the structure, need to check that key exists first --->
						<cfset itemsToDelete = "">
						<cfset wddxUpdateStructure = structNew()>
							<CFLOOP collection = #fieldStructure[identity].fields# item="field" >
								<cfset fieldValue = fieldStructure[identity].fields[field].value>
								<cfif listlen(field,"_") is 2>
									<cfset arrayIndex = listfirst(field,"_")>								
									<cfset fieldname = listlast(field,"_")>
								<cfelse>
									<cfset fieldname = field>
									<cfset arrayIndex =1>
								</cfif>
								<cfparam name="wddxUpdateStructure[arrayIndex]" default = #structNew()#>
								<cfparam name="wddxUpdateStructure[arrayIndex].fields" default = #structNew()#>
								<cfparam name="wddxUpdateStructure[arrayIndex].updateRequired" default = false>
								<cfparam name="wddxUpdateStructure[arrayIndex].deleteRequired" default = false>
								<cfif fieldname is "deleteItem" >
									<cfif fieldvalue is 1>
										<cfset wddxUpdateStructure[arrayIndex].deleteRequired = true>
									</cfif>
								<cfelse>
								<cfset wddxUpdateStructure[arrayIndex].fields[fieldname] = fieldvalue>
								<cfif fieldStructure[identity].fields[field].updaterequired>
									<cfset wddxUpdateStructure[arrayIndex].updateRequired = true>
								</cfif>
								</cfif>
							</cfloop>								

							<CFLOOP collection = #wddxUpdateStructure# item="arrayIndex" >
								<cfif wddxUpdateStructure[arrayIndex].updateRequired or wddxUpdateStructure[arrayIndex].deleteRequired>
										<cfset actualarrayindex  = arrayIndex>
									<cfif not isNumeric(arrayIndex)>
										<cfset actualarrayindex  = arrayLen(currentStructure) + 1>
										<cfset currentStructure[actualarrayIndex] = structNew()>
									</cfif>
									<cfif not wddxUpdateStructure[arrayIndex].deleteRequired>
										<cfloop collection = #wddxUpdateStructure[arrayIndex].fields# item="fieldname">
											<CFSET	currentStructure[actualarrayIndex][fieldname] = wddxUpdateStructure[arrayIndex].fields[fieldname]>
										</cfloop>
									<cfelse>
										<cfset itemsToDelete = listappend (itemsToDelete,actualarrayIndex)>
									</cfif>
								</cfif>
							</CFLOOP>
					
						<cfif itemsToDelete is not "">
							<!--- <cfoutput>#itemsToDelete#</cfoutput> --->
							<cfset itemsToDelete = listsort(itemsToDelete,"numeric","desc")>
							<cfloop index = "itemIndex" list = "#itemsToDelete#">
								<cfset arrayDeleteat(currentStructure,itemIndex)>
							</cfloop>
						</cfif>
		
						<cfif not wddxarray>
							<cfset currentStructure = currentStructure [1]>
						</cfif>

						<CFWDDX ACTION="CFML2WDDX" INPUT="#currentStructure#" OUTPUT="data">
						
						<!--- these line effectively says that all the form fields are now held as WDDX in a 'field' called data which will be updated below --->
						<cfset fieldStructure[identity].fields = structNew()>
						<CFSET fieldStructure[identity].fields.data.value = data>

				
					</cfif>
<!--- 
End of Creating WDDX packet

 --->				



	
	<!--- update or insert data depending upon whether there is an exising record 
		DAMcL - 2003 03 06 - Added additional conditions so that insert is executed
		in the absence of data that identifies a specific row to update.

		WAB 2004-10-11 didn't work with eventflagdata - tried to do insert instead of update - changed if statement from 
		this here below
		<CFIF getFlagData.recordcount is 0 or identityID is "" or not isdefined("session.flagProcessRowID")>
	--->

	<cfif structKeyExists (fieldStructure[identity],"deleteItem")>

							<CFQUERY NAME="deleteData" DATASOURCE=#application.sitedatasource# DEBUG>
						delete from 
							#tableType#FlagData
						where
							EntityID =  <cf_queryparam value="#actualentityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and  FlagID =  <cf_queryparam value="#getFlag.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
						<CFIF isdefined("session.flagProcessRowID")>
						and fd.id =  <cf_queryparam value="#session.flagProcessRowID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFIF>
						<CFIF GetFlag.SubEntityTypeID is not "">
						and   subentityid  = #request.relayCurrentUser.personid#
						</cfif>
						<CFIF identity is not "">
						and #getFlag.datatableFullName#id =  <cf_queryparam value="#identity#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfif>
						</CFQUERY>		

	<CFelseIF getFlagData.recordcount is 0 or (identity is not "" and identity is not 0 and not isdefined("session.flagProcessRowID"))>

		<CFQUERY NAME="InsertData" datasource="#application.siteDataSource#">
						INSERT INTO
							#tableType#FlagData
						(EntityID, FlagID			
						<CFIF GetFlag.SubEntityTypeID is not "">
							,subentityid
						</cfif>
						<CFLOOP item="field" collection="#fieldStructure[identity].fields#">
							, #field#
						</CFLOOP>					
						, created
						, createdby
						, lastUpdated
						, lastUpdatedBy
						)
	
						VALUES
						(<cf_queryparam value="#actualEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >, #getFlag.FlagID#
						<CFIF GetFlag.SubEntityTypeID is not "">
							, #listfirst(cookie.user	,"-")#
						</cfif>
						<CFLOOP item="field" collection="#fieldStructure[identity].fields#">
							, 
								<CFSET thisfieldValue = fieldStructure[identity].fields[field].value>
								<CFIF listFindNoCase(numericfields,field) IS 0 and listFindNoCase(datefields,field) IS  0>
									<cf_queryparam value="#thisFieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
								<CFELSE>
									<cfif thisFieldValue is "">
										<CFIF listFindNoCase(nullablefields,field) IS not 0>
										null
										<cfelse>
										0	
										</CFIF> 
									<cfelse>
									#thisFieldValue# 
									</cfif>
								</cfif> 	
						</CFLOOP>					
						,<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
						,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >
						,<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
						,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >)				
						</CFQUERY>
						<!---
							DAM - 2003 03 06 - Added pull from database to get inserted id
							the trigger should pull this back, however it appears not to sometimes
							so this is a back up. There is transaction above this already!
						--->

					<!--- for flags with identityColumns have a trigger which returns the inserted ID --->
					<cfif identity is not "" and not isNumeric(identity)>

						<CFIF not isdefined("InsertData.InsertedRecordID")>
							<CFQUERY NAME="InsertData" datasource="#application.siteDataSource#">
								Select  max(#getFlag.dataTableFullName#id) as InsertedRecordID from #getFlag.dataTableFullName#
							</CFQUERY>
						</CFIF>
							
							<cfset "#identity#" = InsertData.insertedRecordID>
					</cfif>
					
					<CFELSE>
				
						<CFQUERY NAME="UpdateData" DATASOURCE=#application.sitedatasource# DEBUG>
						Update
							#tableType#FlagData
						SET 
						<CFLOOP item="field" collection="#fieldStructure[identity].fields#">
							<CFSET thisfieldValue = fieldStructure[identity].fields[field].value>
							 #field# = 
								<CFIF listFindNoCase(numericfields,field) IS 0 and listFindNoCase(datefields,field) IS  0>
									N'#thisFieldValue#'
								<CFELSE>
									<cfif thisFieldValue is "">
										<CFIF listFindNoCase(nullablefields,field) IS not 0>
										null
										<cfelse>
										0	
										</CFIF> 
									<cfelse>
									#thisFieldValue# 
									</cfif>
								</cfif> 	
							 ,
						</CFLOOP>					
						lastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
						lastUpdatedby = #request.relayCurrentUser.userGroupID#
						where
							EntityID =  <cf_queryparam value="#actualentityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and  FlagID =  <cf_queryparam value="#getFlag.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
						<CFIF isdefined("session.flagProcessRowID")>
						and fd.id =  <cf_queryparam value="#session.flagProcessRowID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</CFIF>
						<CFIF GetFlag.SubEntityTypeID is not "">
						and   subentityid  = #request.relayCurrentUser.personID#
						</cfif>
		<CFIF isdefined("session.flagProcessRowID")>
		and id =  <cf_queryparam value="#session.flagProcessRowID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		<CFIF identity is not "" and identity is not 0>
		and #getFlag.datatableFullName#id =  <cf_queryparam value="#identity#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
						</CFQUERY>		
		
					</cfif>
		
	</cfif>				<!--- end of updateRequired --->
</cfloop>
