<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes" >
<!--- CF_screen
displays a screen
parent of CF_screenitem

parameters: stylesheet

Mods:

unknown 	WAB		Added use of form.protocol which is defaulted to "http://"  . Had a specific application which needed screens on a secure page
2005-04-12	RND		Added javascript for collapsable divs at lower levels in structure.
2006-02-09    WAB      added encoding of javascript popus correctly
2006-03-06	WAB		- replaced application.typeEntityTable with application.entityType
2007/08/18	WAB		moved updateablefields variable
2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup
2009/03/10   WAB/SSS  altered toggle visibility function
2011/05/25	NYB		REL106 added support for non PLO entities - to make screen code more generic
2011/09/26	WAB		allow verifyInputFunctionName to be passed in to support 2 forms on 1 page
2011/09/27	WAB		P-TND109 Automatically integrate our form verification into CFFORM verification
2012/07/20	IH		Case 429348 escape single quotes in Pleaseprovidethefollowinginformation translations
2012/07/31	IH		Case 429348 nicer solution
2013-05-28	WAB/NJH	Added rwFormValidation to screens.  Removed need for attributes.formname since was sometimes set wrong, now use our .up() solution.
--->

<cfparam name="request.screenArrowUp" default="/images/MISC/iconDoubleArrowUp.gif">
<cfparam name="request.screenArrowDown" default="/images/MISC/iconDoubleArrowDown.gif">
<cfparam name="attributes.verifyInputFunctionName" default = "verifyInput">


<cfoutput>
<script>
	// Pre-load images.
	var iconDoubleArrowUp = new Image();
	iconDoubleArrowUp.src = "#request.screenArrowUp#";
	var iconDoubleArrowDown = new Image();
	iconDoubleArrowDown.src = "#request.screenArrowDown#";

	// Toggle div visibility & image
	function toggleVisibility( divName, callerObject )
	{
		<!--- WAB/SSS 2009/03/10 this code dodn't work in FF, replaced --->
 //	  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
	  theDiv = 	document.getElementById(divName)
	  theDiv.style.display = ( theDiv.style.display == "" ) ? "none" : "";
	  if ( callerObject )
	  {
		callerObject.src = ( theDiv.style.display == "" ) ? iconDoubleArrowUp.src : iconDoubleArrowDown.src;
	  }
	  return true;
	}
</script>
</cfoutput>


<CFIF NOT thisTag.HasEndTag >
	<cfoutput>You have not specified a "/CF_SCREEN" tag</cfoutput>
	<cfexit method="EXITTAG">
</cfif>


<CFIF thisTag.executionMode IS "START">
	<CFSET form.starttime = now()>
	<CFPARAM name="form.Protocol" default = "#request.currentSite.httpProtocol#">
	<CFPARAM name="attributes.StyleSheet" default = "">
	<CFSET caller.formfieldList = "">   <!--- lists all the (editable) fields used by entity, eg person_firstname_1234, person_lastname_1234. Only one of a particular field allowed per page--->
	<CFSET caller.javascriptVerification = "">
	<CFSET caller.updateableFields = false>  <!--- WAB moved from END tag, so that can be set by included templates which have their own custom controls --->
</CFIF>

<CFIF thisTag.executionMode IS "END">
	<!--- tag to close off displayaScreen Tag --->
	<CFPARAM name="caller.javascriptVerification" default="">   <!--- should already have been set --->

	<!---  --->

	<CFSET tableList="">

	<CFLOOP collection = "#application.entitytype#" item = "entityKey">
		<cfset entity = application.entitytype[entityKey].tablename>
		<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<cfset entityUniqueKey = application.entitytype[entityKey].UniqueKey>

		<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
		<CFIF structKeyExists(caller,"#entity#fieldList") AND structKeyExists(caller,"#entity#flagList")>
			<CFIF caller["#entity#fieldList"] is not "" or caller["#entity#flagList"] is not "">
				<CFSET caller.updateableFields = true>
				<!--- remove duplicates from the fieldlist variable--->
				<CF_listUnique list = "#caller["#entity#flagList"]#" variable = "flaglist">
				<CF_listUnique list = "#caller["#entity#fieldList"]#" variable = "fieldlist">
				<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
				<CF_listUnique list = "#caller["#entityUniqueKey#List"]#" variable = "idlist">
				<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
				<CFOUTPUT>
					<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
					<CF_INPUT type="hidden" name="frm#entityUniqueKey#list" value = "#idList#">
					<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
					<CF_INPUT type="hidden" name="frm#entity#fieldlist" value = "#fieldList#">
					<CF_INPUT type="hidden" name="frm#entity#flaglist" value = "#flagList#">
					<!--- put in lastupdated fields --->
					<CFLOOP index = "entityID" list = "#idList#">
					<!--- START 2011/05/25 NYB REL106 added if --->
					<CFIF structKeyExists(caller,"#entity#_lastupdated_#entityID#")>
						<CF_INPUT type="hidden" name="#entity#_lastupdated_#entityID#" value = "#caller["#entity#_lastupdated_#entityID#"]#">
					</CFIF>
					<!--- END 2011/05/25 NYB REL106 --->
					</cfloop>
					<cfif entity is "location" or entity is "organisation">
						<CFSET tableList = listAppend(entity,tablelist)>
						<!--- need to make sure that location comes before person! --->
					<cfelse>
						<CFSET tableList = listAppend(tablelist,entity)>
					</cfif>
				</CFOUTPUT>
			</cfif>
		</cfif>
	</cfloop>

	<CFOUTPUT>
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#createODBcDateTime(now())#">
	</cfoutput>

	<CFIF caller.updateableFields is true>

		<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmTableList" VALUE="#tableList#">
		</CFOUTPUT>

		<CF_INCLUDEJAVASCRIPTONCE template = "/javascript/verifyObjectv2.js">
		<CF_INCLUDEJAVASCRIPTONCE template = "/javascript/checkObject.js">
		<CF_INCLUDEJAVASCRIPTONCE template = "/javascript/verifyEmail.js">
		<!--- <CF_INCLUDEJAVASCRIPTONCE template = "/javascript/verifyEmailV2.js"> 2008/09/24 all code moved to verifyEmail.js --->
		   <!--- WAB 2008/09/24 for email validation in verifyEmail.js --->



		<cfif thisTag.generatedContent & caller.javascriptVerification contains "verifyTelephone">
				<CF_INCLUDEJAVASCRIPTONCE template = "/javascript/verifyTelephone.js">
		</cfif>

		<cfif caller.javascriptVerification contains "DateDropDown">
				<!--- <CF_INCLUDEJAVASCRIPTONCE template = "/javascript/date dropdown.js"> --->
				<cfparam name="request.dateDropDownFunctionIncluded" default="0">
				<cfif not request.dateDropDownFunctionIncluded>
					<CFOUTPUT>
					<script>
						// WAB 20/10/04
						// used for verifying dates entered via the three dropdowns
						function numberofDateDropDownsSelected(formName,baseFieldName) {
							dayfield = eval(formName+'.'+baseFieldName+'Day') ;
							monthfield = eval(formName+'.'+baseFieldName+'Month') ;
							yearfield = eval(formName+'.'+baseFieldName+'Year') ;

							return checkObject(dayfield) + checkObject(monthfield) + checkObject(yearfield)
						}
					</script>
					</CFOUTPUT>
					<cfset request.dateDropDownFunctionIncluded=1>
				</cfif>
		</cfif>


		<CFOUTPUT>

		<cf_includeJavascriptOnce template = "/javascript/rwFormValidation.js" translate="true">
		<!---   WAB 2016-05-10 BF-694 rwFormValidation requires cfform.js
				Usually fine because this tag is often called within a cfform which includes cfform.js by itself, but fails in other instances
		 --->
		<cfif not listfindnocase(getbasetaglist(),"CFFORM")>
			<cf_includeJavascriptOnce template = "/javascript/cf/cfform.js">
		</cfif>

		<cfset randomNumber = rand()>
		<span id="#randomNumber#" style="display:none;"></span>
		<SCRIPT>

		function #attributes.verifyInputFunctionName# ()  {

			msg = '' ;
			var form = $('#jsStringFormat(randomNumber)#').up('form');

			<!--- form = window.document.#attributes.formName#  ; --->

 			<CF_TRANSLATE ENCODE="javascript" processEvenIfNested=true><!--- WAB added 2006-02-09 to deal with encoding strings for javascript --->
			#caller.javascriptVerification# ;
			</cf_translate>
			fnlErrorArray = getRelayValidationErrorArrayForArrayOfElements ($(form).getElements())
			if (fnlErrorArray.length) {
				msg += convertErrorArrayToJavaScriptAlertString(fnlErrorArray,{labelSeparator:':'})
			}

			return msg ;
		}

		</script>
		</CFOUTPUT>
	<cfelse>

		<CFOUTPUT>
		<SCRIPT>

		function #attributes.verifyInputFunctionName# ()  {
			return ''
		}
		</script>
		</CFOUTPUT>

	</cfif>

	<cfif listfindnocase(getbasetaglist(),"CFFORM")>
		<!--- WAB 2011/09/27 a hacky way of integrating our own validation checks into the coldfusion validation system
			NJH 2016/01/14 - changed name of hidden field as it was conflicting with similiar input in relayFormDisplay
		--->
		<cfinput type="hidden" onvalidate="run_#attributes.verifyInputFunctionName#" name="dummyVerifyInput" >
		<cf_translate encode="javascript">
		<cfoutput>
		<script>
		function run_#attributes.verifyInputFunctionName#(formobj,obj,value) {

			msg = #attributes.verifyInputFunctionName# ()
			if (msg) {
				_CF_onError(formobj, obj.name, value, msg);
			}

			/* This puts a nice message at the top of the popup, plus a blank line */
			_CF_error_messages.unshift ('Phr_Pleaseprovidethefollowinginformation \n')

			return true
		}
		</script>
		</cfoutput>
		</cf_translate>
	</cfif>


</cfif>

<CFSETTING enablecfoutputonly="no">
