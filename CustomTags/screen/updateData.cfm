<!--- �Relayware. All Rights Reserved 2014 --->
<!--- OVERVIEW:
Generic template for updating person, location (and with a few mods) organisation records and flags
usually called from showscreen.cfm, or from an updateable report page.

Parameters Expected:
frmTableList - list of tables to be updated eg:  Person,Location
frm#tableName#idList (table names as defined in frmTableList) a list of ids on the page for that table.  Eg frmLocationIdlist=1 frmPersonIDList=1,2,3,999067
frm#tablename#fieldlist  list of updateable fields for the given table
Form Fields of the form #tableName#_#fieldName#_#entityId# and hidden fields of the form #tableName#_#fieldName#_#entityId#_orig giving the original setting of each field
frm#tablename#flaglist  list of flags for each #tablename#

Only updates records if changes have been made

EntityIds which are nonNumeric are assumed to be new records and are added to the database

--->

<!--- Modification History:

140100  WAB  Made test for changes to records, case sensitive
5/00  WAB major mods, make use of record rights tag
2000-06-01 WAB added caller.message
2006-02-08   	WAB		security was preventing external users updating their own records because 404 does not have recordTask rights
				so put in a hack which allows anyone to update their own record
				note that there is very little security on flags
2007-02-06 		WAB   	change to processing of date dropdowns
2008/05/01	WAB 	altered way we deal with rights to edit own record
2008/07/30 	NYF		Bug Fix T-10 Issue 583: Added support for creating country screens
					which were always possible to create, but never supported properly throughout the code
2008-10-22	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup
2009/05/22	SSS		LH 2269 When the location match name was created it assumned that location fields for the match name would exists.
					Now if they do not exist the system will get them from the database and update the matchname that way.

2009/09/22	WAB 	LID 2663 Implemented new rights checking for adding locations and people (see notes 2008/05/01)
2011/05/25	NYB		REL106 added support for non PLO entities - to make screen code more generic
2011/09/28	WAB 	P-TND109 (Registration Project) - had to do work on rights for adding new POL records - basically opens up the whole system - a bit worrying really!
					TBD sanity check on this!
2012/04/11	WAB		CASE 427591 Moved the enablecfoutputonly="no" at end file up a few lines to just before calling redirector.cfm so that templates called in by redirector.cfm don't have to have cfoutputs around everything
2012-11-06	IH		Case 431763 only add comma after "active" it is is not the last field
2013-03-12	WAB 	CASE 433498 update lastUpdatedByPersonID field
2013-07-05	YMA		Case 436087 add some variables to form scope
2013/11/20	NJH		Case 438077 changed the locationmatchname update statement, as the new function meant that some of the variables needed to change.
2014-02-03	WAB 	CASE 437561 Allow this file to work to update core (non flag) fields on addContact pages where the additional fields are included using screens (so when we come to update these fields there is already a core record in the database)
2014/05/13	NJH		Look for entity specific insert function in relayEntity to call rather than the generic insertEntity.
2015/02/10	NJH		Jira Fifteen-106 - put try/catch around record update. If errored, then log error and set the message type to error.
2015-06-22 	WAB 	During FIFTEEN-384 (adding time to date flags), removed call to combineDateFormFields(), now done in applicationMain
					Also added code to remove duplicate tableNames and entityIDs from lists (noticed a screen where updates were being done 4 times)
2016/09/01	NJH		JIRA PROD2016-1190 - removed code to deal with creating matchnames. Matchnames are generated now before inserting the data automatically.
--->

<CFSETTING enablecfoutputonly="yes">
<CFSET updateTime = CreateODBCDateTime(Now())>
<CFPARAM NAME="CUserGroup" DEFAULT="0">
<CFPARAM NAME="Message" DEFAULT="">
<CFPARAM NAME="MessageType" DEFAULT="">

<!--- decipher any parameters passed in frmGlobalParameters --->
<CFPARAM NAME="frmGlobalParameters" DEFAULT="">
<CF_EvaluateNameValuePair nameValuePairs="#frmGlobalParameters#">

<CFIF isdefined ("frmTableList")>
<!--- only do updates if this variable is defined --->

	<CFTRANSACTION>
		<cfset frmTableList = application.com.globalFunctions.RemoveListDuplicates(frmTableList)>
		<!--- loop through tables on page --->
		<CFLOOP index="tablename" list="#frmTableList#">

			<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
			<cfif structKeyExists (application.entitytypeid,tablename)>
				<cfset variables.entityUniqueKey = application.entitytype[application.entitytypeid[tablename]].uniquekey>
					<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->

				<!--- load table definition --->
				<CFIF not isDefined("#TableName#Def")>
					<!--- get definition of the table --->
					<CFSET tablerequired = tablename>
					<CFINCLUDE template="qryGetTableDefinition.cfm">
				</cfif>


			<!--- Loop through ids on page --->
				<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
				<cfset entityIDList = application.com.globalFunctions.RemoveListDuplicates(evaluate('frm#variables.entityUniqueKey#list'))>
				<CFLOOP index="formEntityID" list="#entityIDList#">
				<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
						<CFSET actualEntityID = formEntityID>   	<!--- used later - for a new record, formEntityID might be 'newperson' , when new record is added, actualEntityID is set to the new id --->

					<!--- WAB 2014-02-03 CASE 437561 add support for non numeric formEntityID but variable url.formEntityID defined (ie a record has just been added)
								Changed some references to formEntityID further down the code to actualEntityID
					--->
					<cfif not isNumeric (formEntityID) and isDefined(formEntityID)>
						<cfset actualEntityID = evaluate (formEntityID)>
					</cfif>

					<CFIF formEntityID is 0>
						<CFBREAK>
					</cfif>
					<CFSET rightsChecked = false>

					<!--- need to check for any date fields which might be on the page
							either need the day/month/year concatenating
							or mm-dd-yyyy input converting
							similar code in customtags\updatedata and updatedatacomplexflags
											--->

					<CFLOOP index="fieldname" list="#evaluate('frm#tablename#fieldlist')#">
						<cfif listFindNoCase(datefields,listgetat(fieldname,2,"_")) IS  not 0>
							<!--- this is a date field
								check for existence of day, month, year fields
								actually I assume that all are there if the day is there
								then check for format mm-dd-yyyy

							--->
							<cfif isDefined("#fieldname#_#formEntityID#Day") and evaluate("#fieldname#_#formEntityID#Day") is not "">
								<!--- will fall over if day, month or year is blank! --->
								<cfset "#fieldname#_#formEntityID#" = CreateDateTime (evaluate("#fieldname#_#formEntityID#Year"),evaluate("#fieldname#_#formEntityID#Month"),evaluate("#fieldname#_#formEntityID#Day"),0,0,0)>
							<cfelseif isDefined("#fieldname#_#formEntityID#") and listlen(evaluate("#fieldname#_#formEntityID#"),"-") is 3 and len(trim(evaluate("#fieldname#_#formEntityID#"))) LTE 10>
								<cfset fieldValue = evaluate("#fieldname#_#formEntityID#")>
								<cfset "#fieldname#_#formEntityID#" = CreateDateTime (listgetat(fieldValue,3,"-"),listgetat(fieldValue,2,"-"),listgetat(fieldValue,1,"-"),0,0,0)>
							</cfif>
						</cfif>
					</CFLOOP>




					<!--- Have there been any changes to this entity? --->
					<!--- Loop though fields for current table and id --->
					<CFSET recordchanged=false>
					<CFLOOP index="fieldname" list="#evaluate('frm#tablename#fieldlist')#">	<!--- list eg: person_firstname person_lastname person_email  --->
						<cfif isDefined("#fieldname#_#formEntityID#_orig")>	<!--- checks that this entity really is on the page - was having problems when we have person lists on person pages --->
							<CFPARAM name="#fieldname#_#formEntityID#" default="">  <!--- need to param the form field in case it is a radio button which isn't set  --->
							<CFIF compare(trim(evaluate("#fieldname#_#formEntityID#")), trim(evaluate("#fieldname#_#formEntityID#_orig"))) IS NOT 0>
							<!--- debug: <CFOUTPUT>#fieldname#_#formEntityID#, #evaluate("#fieldname#_#formEntityID#")# #evaluate("#fieldname#_#formEntityID#_orig")#</cfoutput>				   --->
								<CFSET recordchanged=true>
								<CFBREAK>
							</cfif>
						</cfif>
					</cfloop>

				 	<CFIF recordchanged is  true>
						<!--- Have been some changes so need to do update --->

						<!--- check this isn't a new record --->
						<CFIF isnumeric(formEntityID)>
							<!--- this is an update --->
							<!--- WAB 2008/05/01	did work here to deal with rights to edit own record --->
							<!--- check rights to this record --->
							<!--- 2011/05/25 NYB P-REL106 - changed from relayPLO to relayEntity: --->
							<cfset getEntityDetails = application.com.relayEntity.getEntityStructure(entityTypeID = application.entityTypeID[tableName],entityid = formEntityID,getEntityQueries=true)>
			<!--- 				<CF_getRecordRights entityType="#tableName#" entityID = "#formEntityID#"> --->
								<!--- returns query rights. with the rights and  query entity. with the current record --->
			<!--- 					<CFSET thisRecordName = entity.name>
								<CFSET thisRecordLastUpdated = entity.lastUpdated>
								<CFSET thisRecordLastUpdatedBy = entity.lastUpdatedByName>

			 --->
							<!--- START 2011/05/25 NYB REL106 various changes made between HERE --->
								<cfset thisRecord = structnew()>
								<cftry>
									<cfif structkeyexists(getEntityDetails,"entityname")>
					 					<CFSET thisRecord.Name = getEntityDetails.entityname>
									<cfelse>
					 					<CFSET thisRecord.Name = "">
									</cfif>
									<CFSET thisRecord.LastUpdated = getEntityDetails.queries[tablename].lastUpdated>
									<CFSET thisRecord.LastUpdatedBy = getEntityDetails.queries[tablename].lastUpdatedByName>
									<cfcatch type="any">
										<!--- continue... --->
									</cfcatch>
								</cftry>


							<!--- NYF 2008/07/30 Bug 583: replaced -> ---
								<cfset rights = application.com.rights.getPolrights(entityStruct = getEntityDetails)>

							<!--- WAB 2006-02-08 added a hack to allow anyone to update their own record (or their own location/organisation)
								WAB 2008/05/01	and removed now that dealt with in above sp
										and not
										(structkeyExists (request.relaycurrentuser,"#tablename#id") and formEntityID is request.relaycurrentuser["#tablename#id"])
							--->
							<CFIF 	(rights.recordcount is 0 or rights.updateOkay is 0) 							>

								<CFSET #message# = "Sorry, but you do not have permission to edit #thisRecordName#.">

							<CFELSE>
							!--- with: --->
							<cfif tablename neq "Country" and tablename neq "LocatorDef">
								<!--- NJH 2011/06/15 - changed function name from getPolRights to getUserRightsForEntity, which then in turn calls getPolRights --->
								<cfset rights = application.com.rights.getUserRightsForEntity(entityType=tablename,entityID=actualEntityID,entityStruct = getEntityDetails)>
								<!--- WAB 2006-02-08 added a hack to allow anyone to update their own record (or their own location/organisation)
									WAB 2008/05/01	and removed now that dealt with in above sp
											and not
											(structkeyExists (request.relaycurrentuser,"#tablename#id") and formEntityID is request.relaycurrentuser["#tablename#id"])
								--->
								<!--- NJH 2013/10/16 - check first if updateOK exists in rights.. if so, use it.. Doesn't exist for (some) non-POl entitytypes --->
								<CFIF structKeyExists(rights,"updateOk")>
									<cfset rights.edit = rights.updateOK>
								</CFIF>
								<cfif not rights.edit>
									<CFSET rightsOK = false>
									<CFSET #message# = "Sorry, but you do not have permission to edit #thisRecord.Name#.">
								<CFELSE>
									<CFSET rightsOK = true>
								</cfif>
							<cfelse>
								<CFSET rightsOK = true>
							</cfif>

							<cfif rightsOK>
							<!--- <- Bug 583 --->

								<!--- check that record hasn't been updated in meantime --->
								<!---
									NYF 2008/07/30 Bug 583: added (not structkeyexists(thisRecord,"LastUpdated")) O
									2014/01/07	YMA	Case 438444 proceed if LastUpdated is null as well
									WAB 2014-02-03 CASE 437561 Added a hack so that lastUpdated field is ignored if it is blank.  This occurs when an addcontact form has added a record and there were also core fields in a screendefinition included on the addcontact page.  Don't enforce the datediff stuff
								--->
								<CFIF (not structkeyexists(thisRecord,"LastUpdated")) or (thisRecord.LastUpdated eq "") or  evaluate("#tablename#_lastupdated_#formEntityID#") is "" OR datediff("s", thisRecord.LastUpdated, listfirst(evaluate("#tablename#_lastupdated_#formEntityID#"))) GE 0>
									<!---<cfif tableName eq "Location">

											NJH 2016/08/18 JIRA PROD2016-1190 - not sure why this is happening here, as matchnames get updated when needed. Maybe this just saves a bit of time when later trying to update null matchnames.
											Commenting this out for now unless good reason needed.
										<cfset locMatchKeyList=application.com.dbTools.getLocationMatchnameEntries()>

										<cfset locMatchName = "">

										<cfloop list="#locMatchKeyList#" delimiters="#application.delim1#" index="keyItemIndex">
											<cfset keyItem = "#tablename#_#keyItemIndex#_#formEntityID#">
											<!---2009/05/22 SSS LH 2269 if these fields aren't present - get value from DB bcause have not been edited - allows foro if only one of the fields was on screen to be changed
											downside is that if none are on the form we still update but better that than missing partial updates.--->
											<cfif isdefined(keyItem)>
												<cfset locMatchName = listAppend(locMatchName,evaluate(keyItem),application.delim1)>
												<!--- <cfif locMatchName eq "">
													<cfset locMatchName = evaluate(keyItem)>
												<cfelse>
													<cfset locMatchName = locMatchName & " #application.delim1# " & evaluate(keyItem)>
												</cfif> --->
												<cfset keyItemValue = evaluate(keyItem)>
											<cfelse>
												<cfquery name="getDBlocationFieldValue" datasource="#application.sitedatasource#">
													Select #keyItemIndex# as keyItemValue from #tableName# where #variables.entityUniqueKey# =  <cf_queryparam value="#formEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
												</cfquery>
												<cfset locMatchName = listAppend(locMatchName,getDBlocationFieldValue.keyItemValue,application.delim1)>
												<!--- <cfif locMatchName eq "">
													<cfset locMatchName = getDBlocationFieldValue.keyItemValue>
												<cfelse>
													<cfset locMatchName = locMatchName & " #application.delim1# " & getDBlocationFieldValue.keyItemValue>
												</cfif>	 --->
												<cfset keyItemValue = getDBlocationFieldValue.keyItemValue>
											</cfif>
											<cfset locMatchArgs[keyItemIndex] = keyItemValue>
										</cfloop>
										<cfset locMatchName = application.com.dbTools.getLocationMatchname(argumentCollection=locMatchArgs)>
									</cfif> --->

									<cftry>
										<!--- Update Entity table if there any changes --->
										<CFQUERY NAME="update#tablename#" DATASOURCE=#application.sitedatasource# >
										UPDATE #tablename#
										SET
										<CFLOOP INDEX="FieldName" LIST="#evaluate('frm#tablename#fieldlist')#">
											<!--- bit of a hack here to test for numeric fields and for the two active fields which have the be distinguished between--->
											<CFPARAM name="#fieldname#_#formEntityID#" default="">  <!--- need to param the form field in case it is a radio button which isn't set or some other instances  --->
											<cfset data = #Evaluate("#FieldName#_#formEntityID#")#>
											<cfset field = listgetat(fieldname,2,"_")>

											<!--- NJH 2011/06/15 - add field level rights  --->
											<cfif (not listFindNoCase("country,locatorDef",tablename) and application.com.rights.doesUserHaveRightsToEntityField(entityType=tablename,entityId=formEntityID,entityRights=rights,permission="edit",field=field)) or (listFindNoCase("country,locatorDef",tablename))>
											 	<CFIF listcontains("personactive,locationactive",field) IS NOT 0>
													active =  <cf_queryparam value="#data#" CFSQLTYPE="cf_sql_float" ><cfif ListLast(evaluate('frm#tablename#fieldlist')) neq FieldName>,</cfif>
												<cfelse>
													<cfif field neq variables.entityUniqueKey>
														<cf_queryobjectname value="#field#"> =
														<CFIF listFindNoCase(numericfields,field) IS 0 and listFindNoCase(datefields,field) IS  0>
															<cf_queryparam value="#data#" CFSQLTYPE="cf_sql_varchar" >
														<CFELSE>
															<cfif data is "">
																<CFIF listFindNoCase(nullablefields,field) IS not 0>
																null
																<cfelse>
																0
																</CFIF>
															<cfelseif listFindNoCase(datefields,field)>
																<cf_queryparam value="#data#" CFSQLTYPE="cf_sql_timestamp" >
															<cfelse>
																<cf_queryparam value="#data#" CFSQLTYPE="cf_sql_numeric" >
															</cfif>
														</cfif>
														<cfif ListLast(evaluate('frm#tablename#fieldlist')) neq FieldName>,</cfif>
													</cfif>
												</cfif>
											</cfif>
										</CFLOOP>
										<!--- NJH 2016/08/18 JIRA PROD2016-1190
										<cfif tablename eq "location">
											<!--- ,Matchname=substring(RTRIM(Replace(#matchReplace.replaceStatement#REPLACE(N' #locMatchName# ','.',' ')#replace(matchReplace.replaceValueStatement,"**","'","ALL")#,' ','')),1,500) --->
											,Matchname=substring(<cf_queryparam value="#locMatchname#" CFSQLTYPE="cf_sql_varchar" >,1,500)
										</cfif> --->
										<CFIF structkeyexists(thisRecord,"LastUpdated")>
											,LastUpdated =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
											,LastUpdatedBy = #request.relayCurrentUser.usergroupid#
											,LastUpdatedByPerson = #request.relayCurrentUser.personid#
										</CFIF>
										<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
										WHERE  #variables.entityUniqueKey# =  <cf_queryparam value="#formEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
										<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
										</CFQUERY>
										<CFSET message=message& "#thisRecord.Name# record updated <BR>">

										<cfcatch>
											<cfset errorId = application.com.errorHandler.recordRelayError_Warning(type="Screen Update",Severity="error",caughtError=cfcatch)>
											<CFSET message=message&"#thisRecord.Name# record could not be saved. Please refer an administrator to this errorID: #errorID#">
											<cfset messageType = "error">
										</cfcatch>
									</cftry>
								<CFELSE>
									<CFSET message=message&"#thisRecord.Name# record has been edited ">
									<CFIF structkeyexists(thisRecord,'LastUpdated')><cfset message=message&"by #thisRecord.LastUpdatedBy# (at #timeformat(thisRecord.lastupdated,"HH:mm:ss")#)"></cfif>
									<CFSET message=message&"since you opened the record, no update saved<BR>">
								</CFIF>
							</cfif>
						<cfelse>
							<!--- this is a new record --->
							<!--- need to check add rights for this country / record,--->
							<!--- LID 2663 WAB 2009/09/22
								altered test for rights to use the new(ish) getPolrights function
							 --->

						<cfif request.relayCurrentUser.isInternal>
							<CFIF tableName IS "Person">
								<CFSET thisLocationID = #evaluate("person_locationid_#formEntityID#")#>
								<CFIF NOT isNumeric("thisLocationID") >
									<!--- must be a person and location being added at same time --->
									<CFSET thislocationid = evaluate(thislocationid)>
								</cfif>

								<!--- LID 2663 removed, replaced with below  <CF_getRecordRights entityType="location" entityID = "#thisLocationID#"> --->
								<cfset getEntityDetails = application.com.relayEntity.getEntityStructure(entityTypeID = 1,entityid = thisLocationID,getEntityQueries=false)>
								<cfset rights = application.com.rights.getPolrights(entityStruct = getEntityDetails )>

							<CFELSEIF tableName IS "Location">
								<!--- LID 2663 removed, replaced with below 	<CF_getRecordRights entityType="country" entityID = "#evaluate("Location_countryid_#formEntityID#")#"> --->
								<cfset temporganisationid = form["Location_organisationid_#formEntityID#"]>
								<cfif not isNumeric(temporganisationid)><cfset temporganisationid = evaluate(temporganisationid)></cfif>
								<cfset getEntityDetails = application.com.relayEntity.getEntityStructure(entityTypeID = 2,entityid = temporganisationid,getEntityQueries=false)>
 								<cfset rights = application.com.rights.getPolrights(entityStruct = getEntityDetails )>
							<cfelse>
								<cfset getEntityDetails = application.com.relayEntity.getEntityStructure(entityTypeID = 2,entityid = formEntityID,getEntityQueries=false)>
								<cfset rights = {updateOkay=1,addOkay=1,recordcount=1}>
							</cfif>

							<cfif structkeyexists(getEntityDetails,"entityname")>
			 					<CFSET thisRecord.Name = getEntityDetails.entityname>
							<cfelse>
			 					<CFSET thisRecord.Name = "">
							</cfif>
						<cfelse>
							<!--- WAB 2011/09/28
							For Trend Registration Project
							Slightly unsure about this, but we need portal users to register themselves to new or existing organisations
							But it does basically open up the whole system!
							 --->
							<cfset rights = {recordCount=1,addOkay=1}>

						</cfif>


							<CFIF (rights.recordcount is 0 or rights.addOkay is 0 )>  <!--- WAB 2007/10/04 added hack to allow a user to add a location to their own organisation
										WAB 2009/09/23 LID 2663 should not be required now with ownOrganisationTask
								and not
								(tablename is "location" and request.relaycurrentuser.organisationid is form["location_organisationid_#formEntityID#"])
									--->
								<CFSET #message# = "Sorry, but you do not have permission to add records to #thisRecord.Name#.">
							<CFELSE>
								<cfif fileexists("#application.paths.relay#\screen\add#tablename#.cfm")>
									<cfinclude template="/screen/add#tablename#.cfm">

								<cfelse>
									<cfset entityDetailsStruct = structnew()>
									<CFLOOP INDEX="FieldName" LIST="#evaluate('frm#tablename#fieldlist')#">
										<cfif listgetat(fieldname,2,"_") neq variables.entityUniqueKey>
											<cfset x = StructInsert(entityDetailsStruct, listgetat(fieldname,2,"_"), Evaluate("#FieldName#_#formEntityID#"))>
										</cfif>
									</CFLOOP>

									<!--- NJH 2014/05/13 - Saleforce Connector work.. look for entity specific insert function. In this case, I needed to call insertLocation as that deals with setting some default variables. --->
									<cfif structKeyExists(application.com.relayEntity,"insert#tablename#")>
										<cfset functionArgs = structNew()>
										<cfset functionArgs["#processEntity#Details"]=entityDetailsStruct>
										<cfinvoke component=#application.com.relayEntity# method="insert#tablename#" argumentCollection=#functionArgs# returnvariable="newentity">
									<cfelse>
										<cfset newEntity = application.com.relayentity.insertEntity(entityTypeID=application.entityTypeID[tablename],table=tablename,entityDetails=entityDetailsStruct)>
									</cfif>
									<cfset newEntityId = newEntity.entityID>
								</cfif>
								<!--- END 2011/05/25 NYB REL106 ... and HERE --->

								<!--- bit of a problem here - if form is resubmitted, we don't readd the person, but we still set these variables, not sure of knock on effects --->
								<CFSET "#actualEntityID#" = newentityid>   <!--- eg newlocation = 27123 --->
								<CFSET actualEntityID = newentityid>
								<!--- NJH 2010/03/24 P-PAN002 - added caller scope, so that we could reference new identity in calling page --->
								<CFPARAM name="caller.#tablename#idsadded" default="">
								<CFSET "caller.#tablename#idsadded" = listappend(evaluate("caller.#tablename#idsadded"), newentityid)>

								<!--- this bit should set frmLocationID and frmPersonID which some templates use --->
								<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
								<CFSET "frm#variables.entityUniqueKey#" = newentityid>
								<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->


								<!--- this complicated bit should help when a person is being added --->
								<!--- if there is a variable frmCurrentEntityID, which itself is the name of a variable (such as newPerson) then if newPerson is defined, evaluate it --->
								<!--- eg. (ie frmcurrenentityid = 'newpers' and newpers=125893)  --->

								<!---
								<CFIF isDefined("frmcurrentEntityID") >	<!--- check that this variable exists --->
									<CFIF  not isNumeric(frmcurrentEntityID)>  <!--- mustn't be numeric if we are going to do the isdefined test below --->
										<CFIF  isDefined(frmcurrentEntityID)> <!--- check whether this variable points to another variable (ie frmcurrenentityid = 'newpers' and newpers=125893) --->
											<CFSET frmcurrentEntityID = evaluate(frmcurrentEntityID)>  <!--- actually set the current entity --->
										</CFIF>
									</cfif>
								</CFIF>
								--->

								<CFIF isDefined("frmcurrentEntityID") and not isNumeric(frmcurrentEntityID) and frmcurrentEntityID is not "" and isDefined(frmcurrentEntityID)>
									<CFSET frmcurrentEntityID = evaluate(frmcurrentEntityID)>  <!--- actually set the current entity --->
								</CFIF>


							</CFIF>	<!--- end of rights check --->


						</CFIF><!--- end of adding new record --->


					</CFIF> 		<!--- end of recordchange=true --->


			<!--- **************************************
					Update Flags
				 ************************************** --->

			<!--- <CFOUTPUT>debug: #evaluate('frm#tablename#flaglist')#	 </CFOUTPUT>   --->

					<!--- 	check that the actualEntityID is Numeric
							If it is not numeric then we are dealing with a new entity record.
							If we are dealing with a person or a location being added then the actualEntityID
							will have been set above, but entities where the entityRecord has been added outside of this
							tag, we need to look for a varibable with the same name as the value of EntityID

							For example if the original form was created with entityID = "newItem",
							Then after adding the newItem you should <CFSET form.newItem = #newItemID#)>
							Note that making it a form. variable allows it to be read easily by the custom tag.

					--->

						<CFIF not isnumeric(actualEntityID)>
							<CFIF isDefined (actualEntityID)>
								<CFSET actualEntityID = evaluate(actualEntityID)>

							<CFELSE>
								<CFOUTPUT><!-- #actualEntityID# is not defined --></cfoutput> <!--- WAB 2011/10/04 output this error in a comment, it sometimes isn't a problem--->
								<CFBREAK>
								<!--- don't go through flags if this is an new item line and a new item has not been added --->

							</cfif>


						</CFIF>



					<!--- loop through flags on page for this entity--->
					<CFLOOP index="flag" list="#evaluate('frm#tablename#flaglist')#">



						<CFIF listfirst(flag,"_") is NOT "Flag">
							<!--- regular flags --->





							<CFPARAM name="#flag#_#formEntityID#" default="">  <!--- if nothing selected then the field will not be defined --->
							<!--- <cfoutput>DEBUG: #flag#_#formEntityID# ; #evaluate("#flag#_#formEntityID#")# ; #evaluate("#flag#_#formEntityID#_orig")#</cfoutput>			   --->

							<CFIF isDefined("#flag#_#formEntityID#_orig")>
								<!---
								WAB 2015-06-22 During FIFTEEN-384 (adding time to date flags)
								This now moved to applicationMain.doctorFormFields
								<cfif listFirst(flag,"_") is "date">
									<cfset "#flag#_#formEntityID#" = application.com.dateFunctions.combineDateFormFields(baseVariableName = "#flag#_#formEntityID#", scope="form")>
								</cfif>
								1--->
								<CFIF compare(evaluate("#flag#_#formEntityID#"), evaluate("#flag#_#formEntityID#_orig")) IS NOT 0>

									<!--- need to update this flag--->
									<CFSET frmTask = "update">

										<CFSET frmentityid=actualEntityID>

									<CFIF isnumeric(formEntityID)>
										<CFSET flaglist=flag>
										<!--- 2013-07-04	YMA	FlagTask is now looking in the form for these variables so they now belong in the form.--->
										<CFSET form["#flag#_#formEntityID#"] = evaluate("#flag#_#formEntityID#") >
										<CFSET form["#flag#_#formEntityID#_Orig"] = evaluate("#flag#_#formEntityID#_Orig")>
									<CFELSE>
										 <!--- ie this is a newly added record --->
										<CFSET flaglist=replace(flag,formEntityID,actualEntityID,"All")>
										<!--- 2013-07-04	YMA	FlagTask is now looking in the form for these variables so they now belong in the form.--->
										<CFSET form["#flag#_#actualEntityID#"] = evaluate("#flag#_#formEntityID#") >
										<CFSET form["#flag#_#actualEntityID#_Orig"] = evaluate("#flag#_#formEntityID#_Orig")>
										<!--- <CFSET "#flag#_#actualEntityID#" = evaluate("#flag#_#formEntityID#") >
										<CFSET "#flag#_#actualEntityID#_Orig" = evaluate("#flag#_#formEntityID#_Orig")> --->
									</CFIF>


									<CFINCLUDE template="\relay\flags\flagtask.cfm">
									<!--- 2013-08-27	YMA	Case 436691 Record Updated message should not display on the portal. --->
									<cfif structKeyExists(request,"relaycurrentuser") and request.relaycurrentuser.isInternal>
										<CFSET message= "Record updated.">
									</cfif>

								</cfif>

							</cfif>

						<CFELSE>
								<!--- **************************************
										complex flags
									 ************************************** --->

							<CFINCLUDE template="updateData_complexFlags.cfm">


						</cfif><!--- end of complex flags --->


					</cfloop><!--- end of loop through flags --->



				<!--- if a person is updating their own data then we need to refresh the relayCurrentUserStructure --->
				<cfif (tablename is 'person' and formEntityID is request.relaycurrentUser.personid) or
					(tablename is 'location' and formEntityID is request.relaycurrentUser.locationid) or
					(tablename is 'organisation' and formEntityID is request.relaycurrentUser.organisationid)
				>
					<cfset x = application.com.relayCurrentUser.updatePersonalData()>
				</cfif>


			 	</cfloop><!--- end of loop through entityids --->

				<!--- WAB CR_SNY584
					want to track the fact that a user has accessed a screen and clicked on the save button
				 --->
				<cfif isDefined("frmLoadedScreenId")>
					<CFQUERY NAME="insertScreenTracking" DATASOURCE=#application.sitedatasource#>
					insert into screenTracking
						(screenid, personid, entityType,entityid,created)
						select
						screenid, <cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#tablename#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#actualEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >,getdate()
						from screens
						where <cfif isNumeric(frmLoadedScreenId)>screenid = <cf_queryparam value="#frmLoadedScreenId#" CFSQLTYPE="cf_sql_integer" ><cfelse>screentextid = <cf_queryparam value="#frmLoadedScreenId#" CFSQLTYPE="cf_sql_varchar" ></cfif>
					</CFQUERY>
				 </cfif>

			<cfelse>
					<!--- this isn't a supported entityType --->
			</cfif>
		</cfloop><!--- end of loop through tables --->
	</CFTRANSACTION>
</CFIF><!--- end of if TableList Defined--->


	<cfif isdefined("fieldnames") and fieldnames contains "rf_">
		<CF_relatedFile Action="update">
	</cfif>


<CFSETTING enablecfoutputonly="no">
<CFIF isdefined("frmscreenpostprocessid")>
	<CFIF isdefined("frmprocessid")><cfset rememberFrmProcessID = frmProcessID><cfelse><cfset rememberFrmProcessID = ""></cfif>
		<cfloop list="#frmscreenpostprocessid#" index="thisProcess">
			<cfset frmProcessid = thisprocess>
			<cfinclude template="/screen/redirector.cfm">
		</cfloop>
	<cfset frmprocessid = rememberFrmProcessID>
</cfif>


<CFPARAM name="caller.message" default="">
<CFSET caller.message = caller.message & message>
<CFSET caller.messageType = messageType>


