<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Template	qryGetTableDefinition.cfm

Author		WAB

Date		?

Purpose		Works out which fields in a table are numeric and returns list of these fields
			Used as an include file in updateData.cfm so that update and insert queries have quotes in correct places
			

Mods		2000-06-01  WAB added a structure to hold results for a particular table.  Can be tested for and saves being run more than once in a template
 --->

<cfinclude template="/templates/qryGetTableDefinition.cfm">
 
