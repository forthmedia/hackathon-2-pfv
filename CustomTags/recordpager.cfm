<!--- �Relayware. All Rights Reserved 2014 --->
<!---
The recordpager tag automatically builds a row of linked page
numbers to allow users to navigate through pages of records. It is
a highly customizable tag, both in terms of appearance and
functionality.

Note that if you pass in a total number of records that is less
than or equal to the number of records you want to display per
page, this tag immediatly exits (without causing an error) since
in this instance, you have no need for a record pager.

USAGE: <cf_recordPager totalRecordCount="x" recordsPerPage="20" startRow="x">

Required attributes:
@attribute totalRecordCount (required) The total number of records (records per page * number of pages).
@attribute recordsPerPage   (required) The number of records you want to display on each page. 
@attribute startRow         (required) The row you begin displaying records on.

@attribute pageName              The page that the numbers should link to. The default is the name of
                                 the page that the tag is used on.
@attribute padding               The number of spaces between the numbers. Default is 2.
@attribute styleClass            The name of the style class you want applied to the page numbers.
                                 By default, no class is specified.
@attribute currentPageStyleClass The style of the page number that is currently selected. This number
                                 is the only one that is not linked and it typically rendered in
                                 bold.
@attribute additionalArgs        A structure containing any name/value pairs that you want appended to
                                 pageName and sent to the target page when one of the page numbers 
                                 is clicked on. You do not need to URL encode your values; it is
                                 automatically done for you.

@dependency UDF /functions/structToQuerystring.cfm
@dependency UDF /functions/getCurrentPageName.cfm


WAB 2011/11/22 removed individual links for each page and replaced with a Select Box
PYW 2015/08/05 P-TAT005. Mod to work with non-report page e.g. new account approvals
DAN 2015/12/09 Case 446006 - fix paging for student progress page

--->

<cfparam name="attributes.caller" type="string" default=""> <!--- just want to make sure that if this is called from anywhere other than tableFromQueryObject.cfm it does not blow up --->

<cfif thisTag.executionMode is "start">
    <!--- Required attributes --->
    <cfparam name="attributes.totalRecordCount" type="numeric" />
    <cfparam name="attributes.recordsPerPage"   type="numeric" />
    <cfparam name="attributes.startRow"         type="numeric" />
	<cfparam name="attributes.labelText" 		type="string">

    <!--- If there is only one page of records, simply exit. --->
    <cfif attributes.totalRecordCount lte attributes.recordsPerPage>
        <cfexit method="exitTag" />
    </cfif>

    <!--- Set defaults for optional attributes--->
    <cfparam name="attributes.padding" type="numeric" default="2" />

    <!--- Include the getCurrentPageName UDF --->
    <cfscript>
	    function getCurrentPageName()
	    {
	        var path = getPageContext().getRequest().getServletPath();
	        return listGetAt(path, listLen(path, "/"), "/");
	    }
	</cfscript>

    <cfparam name="attributes.pageName" type="string" default="#getCurrentPageName()#"  />


    <!--- Include the structToQuerystring UDF --->
    <cfscript>
	    function structToQuerystring(struct)
	    {
	        var qs = "";
	        var i = 1;
	        var keys = structKeyArray(struct); 
	        for (; i lt arrayLen(keys) + 1; i = i + 1)
	        {
	            if (i neq 1)
	            {
	                qs = qs & "&";
	            }
	            qs = qs & keys[i] & "=" & urlEncodedFormat(struct[keys[i]]);
	        }
	        return qs;
	    }
	</cfscript>


    <!--- Create the padding variable --->
    <cfset spaces = "" />
    <cfloop from="1" to="#attributes.padding#" index="padding_index">
        <cfset spaces = spaces & "&nbsp;" />
    </cfloop>

    <!--- Create style class variables --->
    <cfset styleClassTag = "" />
    <cfif isDefined("attributes.styleClass")>
        <cfset styleClassTag = " class='" & attributes.styleClass & "'" />
    </cfif>

    <cfset currentPageStyleClassTag = "" />
    <cfif isDefined("attributes.currentPageStyleClass")>
        <cfset currentPageStyleClassTag = " class='" & attributes.currentPageStyleClass & "'" />
    </cfif>

    <!--- Figure out the total number of pages --->
    <cfset totalNumberOfPages = attributes.totalRecordCount / attributes.recordsPerPage />

    <cfif attributes.totalRecordCount mod attributes.recordsPerPage is not 0>
        <cfset totalNumberOfPages = incrementValue(totalNumberOfPages) />
    </cfif>

    <!--- Build the additionalArgs query string if necessary --->
    <cfset aa = "" />
    <cfif isDefined("attributes.additionalArgs")>
        <cfset aa = "&" & structToQuerystring(attributes.additionalArgs)  />
    </cfif>
<!--- WAB 2011/11/22 removed individual links and replaced with a Select Box (below)
    <!--- Build the paging links --->
    <cfoutput>#attributes.labelText# 
        <cfloop from="1" to="#totalNumberOfPages#" index="total_index">
            <cfif total_index is not 1>
                #spaces#
            </cfif>
            <cfset tmpStartRow = ((total_index * attributes.recordsPerPage) - (attributes.recordsPerPage - 1)) />
            <cfif attributes.startRow eq tmpStartRow>
                <span#currentPageStyleClassTag#>#total_index#</span>
            <cfelse>
				<cfif attributes.caller eq "">
                	<a href="#attributes.pageName#?startRow=#tmpStartRow&aa#"#styleClassTag#>#total_index#</a>
				<cfelse>
                	<a href="javascript:void(document.forms['frmRecordPagingForm_#attributes.id#']['startRow'].value='#tmpStartRow#');document.forms['frmRecordPagingForm_#attributes.id#'].submit();"#styleClassTag#>#total_index#</a>
				</cfif>
            </cfif>
        </cfloop>
    </cfoutput>
	 --->
	<!--- NJH 2012/02/13 CASE 426669: pass in the additional args which was missing. --->
	<cfoutput>
		<cfif attributes.caller eq "">
			<script>
				function changePage (obj) {
					window.location.href = '#jsStringFormat(attributes.pageName)#?startRow=' + obj.options[obj.selectedIndex].value +'#jsStringFormat(aa)#'
				}
			</script>
	
		<cfelse>
			<!--- PYW 2015/08/05 P-TAT005. Mod to work with non-report page e.g. new account approvals --->
			<script>
				function changePage (obj) {
					page = obj.options[obj.selectedIndex].value;
				    <cfif isDefined("attributes.formName")>
					pagingForm = document.forms['#attributes.formName#'];
				    <cfelse>
					pagingForm = document.forms['frmRecordPagingForm_#jsStringFormat(attributes.id)#'];
				    </cfif>

                    if (typeof pagingForm !== 'undefined') {
                        pagingForm['startRow'].value=page;
                        pagingForm.submit();
                    } else {
                        window.location.href = '#jsStringFormat(attributes.pageName)#?startRow=' + page +'#jsStringFormat(aa)#'
                    }

				}
			</script>
	
		</cfif>

	    
		<div class="col-xs-12 col-sm-3">
			<label class="flagAttributeLabel">#htmleditformat(attributes.labelText)#</label>
			<select onchange="changePage(this)" class="form-control">
			  <cfloop from="1" to="#totalNumberOfPages#" index="total_index">
		            <cfset tmpStartRow = ((total_index * attributes.recordsPerPage) - (attributes.recordsPerPage - 1)) />
					<option value="#tmpStartRow#" <cfif attributes.startRow eq tmpStartRow>selected</cfif>>#htmleditformat(total_index)#
		        </cfloop>
			</select>
		</div>
	</cfoutput>
</cfif>


