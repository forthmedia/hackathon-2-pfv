<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		addAction.cfm
Author:			Simon WJ
Date created:	2001-11-24

	Objective - Custom tag for adding an action to the action table
		
	Syntax	  -	 
	
	Parameters - 
				entityID,entityTypeID,dueDate
				
	Return Codes - none
	
Example usage:

		<CF_addAction
			ownerPersonID 	= #frmOwnerPersonID#
			entityID   		= #frmNactPersonID#
			entityTypeID 	= 0
			description		= #frmDescription#
			priroty			= #frmpriority#
			notes			= #form.frmActionNotes#
			actionTypeID	= #form.frmActionTypeID#
			deadline		= #createodbcdatetime(frmNextCallDate)#
			footer			= #emailfooter#
		>   


Amendment History:

DD-MMM-YYYY	Initials 	What was changed
2001-11-24		SWJ		First version
2005-03-30        RJP     Added email footer variable for user emails external website functionality
2005-03-30        RJP     Added 'created by' variable external website functionality
2005-03-31        RJP     Added portalaction switch to cater for external website functionality
2008/06/30     SSS      Added Nvarchar Support
2011-01-13		MS		LID:4314 Insert SQL for query insertAction updated to record active campaigns against actions
--->

<CF_checkMandatoryParams 
	callingTemplate="addCommDetail.cfm" 
	relayParam="entityID,entityTypeID,dueDate">

<CFPARAM NAME="attributes.entityID" TYPE="numeric">
<CFPARAM NAME="attributes.actionStatusID" TYPE="numeric" DEFAULT="2">
<CFPARAM NAME="attributes.entityTypeID" TYPE="numeric">
<CFPARAM NAME="attributes.campaign" TYPE="numeric"><!--- Mustafa added campaignID --->
<CFPARAM NAME="attributes.deadline" TYPE="numeric">
<CFPARAM NAME="attributes.notes" TYPE="string">
<CFPARAM NAME="attributes.footer" TYPE="string" default="">
<CFPARAM NAME="attributes.description" TYPE="string" DEFAULT="none">
<CFPARAM NAME="attributes.priority" TYPE="numeric">
<CFPARAM NAME="attributes.actionTypeID" TYPE="numeric">
<CFPARAM NAME="attributes.ownerPersonID" TYPE="numeric">
<CFPARAM NAME="FreezeDate" TYPE="date" DEFAULT="#Now()#">
<CFPARAM NAME="attributes.checkMultipleActions" TYPE="string" DEFAULT="no">
<CFPARAM NAME="attributes.portalaction" TYPE="numeric" DEFAULT="0">
<CFPARAM NAME="attributes.StatusID" TYPE="numeric" DEFAULT="2">
<CFPARAM NAME="attributes.CompletedTime" TYPE="string" default="">
<CFPARAM NAME="attributes.CompletedDate" TYPE="string" default="">


<cfif #attributes.portalaction# eq 1>  <!--- for external users where 'created by webuser' isn't enough information --->
 <cfset varcreatedby = #attributes.entityID#> 
<cfelse>
 <cfset varcreatedby = #request.relayCurrentUser.usergroupid#>
</cfif> 

<CFIF attributes.checkMultipleActions eq "yes">
	<CFQUERY NAME="checkEntityActions" DATASOURCE="#application.siteDataSource#">
		select actionID from actions 
		where entity =  <cf_queryparam value="#attributes.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		and entityTypeID =  <cf_queryparam value="#attributes.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and owmerPersonID =  <cf_queryparam value="#attributes.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<!--- If there is already an action for this entity, list them and let the user choose to 
		add new or add to existing --->
	<CFIF checkEntityActions.recordCount gt 0>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<TH></TH>
		</TR>
		<CFOUTPUT QUERY="checkEntityActions">
			<TR>
				<TD></TD>
			</TR>
		</CFOUTPUT>
		</TABLE>
			<form name="addActionMain" action="#cgi.script_name#?#request.query_string#" method="post" 
				enctype="multipart/form-data" onSubmit="return checkForm(this)">
			</form>
	</cfif>
</cfif>

<cfif #attributes.CompletedDate# IS NOT "">
	<cfset CompletedDateTime = createodbcdatetime(attributes.CompletedDate+attributes.CompletedTime)>
<cfelse>
	<cfset CompletedDateTime = "NULL">
</cfif>
<CFQUERY NAME="insertAction" DATASOURCE="#application.SiteDataSource#">
	INSERT INTO actions(ownerPersonID, CreatedBy, Description, 
		created, Deadline, Notes, priority,
		actionTypeID,entityID,entityTypeID, actioncampaignId,
		actionStatusID,
		CompletedDate)
	VALUES(<cf_queryparam value="#attributes.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#varcreatedby#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#attributes.description#" CFSQLTYPE="CF_SQL_VARCHAR" >,
		<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#attributes.deadline#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#attributes.notes#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#attributes.priority#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#attributes.actionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#attributes.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#attributes.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#attributes.campaign#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#attributes.StatusID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#CompletedDateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		)
	select SCOPE_IDENTITY() as actionID
</CFQUERY>

<!---
<CFQUERY NAME="GetActionID" DATASOURCE="#application.SiteDataSource#">
	SELECT ActionID
	  FROM actions
	 WHERE ownerPersonID =  <cf_queryparam value="#attributes.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   AND createdby =  <cf_queryparam value="#varcreatedby#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   AND created =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</CFQUERY>

 <CFIF GetActionID.RecordCount IS 1> --->
	<cfset thisActionID = insertAction.actionID>
	<CFQUERY NAME="InsertUserRights" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
			SELECT userGroup.UserGroupID,'actions',
						<cf_queryparam value="#thisActionID#" CFSQLTYPE="CF_SQL_INTEGER" >,'3' AS Permission
				FROM userGroup
			 WHERE userGroup.PersonID =  <cf_queryparam value="#attributes.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	<cfif attributes.ownerPersonID neq request.relayCurrentUser.personid> 
		<CFQUERY NAME="InsertNote" DATASOURCE="#application.SiteDataSource#">
		 	Insert into actionnote (actionid,creatorid,created,notetext,
			datestart,dateend,timetaken,cost,newowner,newstatus,type)
			Values (<cf_queryparam value="#thisActionID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#varcreatedby#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,'',
			null, null, null, 0,<cf_queryparam value="#attributes.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#attributes.actionStatusID#" CFSQLTYPE="CF_SQL_INTEGER" >,'Allocation')
		</CFQUERY>
		
 		<CFQUERY NAME="InsertUserRights" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
			SELECT userGroup.UserGroupID,'actions',
						<cf_queryparam value="#thisActionID#" CFSQLTYPE="CF_SQL_INTEGER" >,'3' AS Permission
				FROM userGroup
			 WHERE userGroup.PersonID =  <cf_queryparam value="#varcreatedby#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY> 
	</cfif>
<!--- </CFIF> --->



<CFIF attributes.ownerPersonID neq varcreatedby>
	<!--- email owner of new action assigned to him/her --->
	<cfset actionID = thisActionID>
	<cfinclude template="/actions/ActionEmail.cfm">	
</CFIF>

