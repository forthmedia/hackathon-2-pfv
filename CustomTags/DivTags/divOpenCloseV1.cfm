<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting 	enablecfoutputonly="yes">
<!--- 
cf_divOpenClose.cfm

WAB June 2005

Custom tag for opening and closing DIVs (or other items such as TRs).
Outputs images and text links which toggle divs and which change depending upon state of divs

attributes

DivID   required - Id of the div to control (can be a list of divs)
openimage / closeimage  names of images to use
opentext / close Text    text to display on the text link
startOpen     true/false   - whether to start in open or closed state
rememberSettings  true/false    - whether to remember open/closed state for the next page load
pageid  - unique ID for page (if rememberSettings is true then will prevent confusion with divs with same name)


Returns
Variable #divStyle# is set, this needs to be put in the style of the DIV you are controlling (this makes sure thatthe initial state is correct
Use.

<cf_divOpenClose  divID="myDiv">
	<cf_divOpenClose_Image> <cf_divOpenClose_text>
	<div id="myDiv"  style="#divStyle#">
	text inside the div
	</div>	
</cf_divOpenClose  >
	

 --->

<cfparam name= "request.openCloseDiv.Images" default="#structNew()#">

<cfif thisTag.executionMode is "start">
	<cfparam name = "attributes.divID">
	<cfparam name = "attributes.alsoToggleDivIDs" default = "">
<!--- WAB 2009/09/23 altered default images.
	Long time ago the actual images were changed so arrowup was actually a down arrow and arrowdown was a right arrow.
	Not all sites seem to have had changes anyway, so have altered defaults instead	
	<cfparam name = "attributes.openImage" default = "/images/MISC/iconDoubleArrowDown.gif">
	<cfparam name = "attributes.closeImage" default = "/images/MISC/iconDoubleArrowUp.gif">
 --->	
	<cfparam name = "attributes.openImage" default = "/images/MISC/arrow_right.gif">
	<cfparam name = "attributes.closeImage" default = "/images/MISC/arrow_down.gif">

	<cfparam name = "attributes.openText" default = "Open">
	<cfparam name = "attributes.closeText" default = "Close">
	<cfparam name = "attributes.startOpen" default = "true">
	<cfparam name = "attributes.imageID" default = "#attributes.divID#_img">
	<cfparam name = "attributes.textAnchorID" default = "#attributes.divID#_anc">
	<cfparam name = "attributes.imageAnchorID" default = "#attributes.divID#_Ianc">
	<cfparam name = "attributes.imagePassThrough" default = "">
	<cfparam name = "attributes.textAnchorPassThrough" default = "">
	<cfparam name = "attributes.imageAnchorPassThrough" default = "">
	<cfparam name = "attributes.rememberSettings" default = "yes">
	<cfparam name = "attributes.onOpenCloseDivIDs" default="">
	<cfparam name = "attributes.onOpenSetFocusTo" default="">
	<cfparam name = "attributes.pageID" default = "">	
	
	<cf_includeJavascriptOnce template = "/javascript/divOpenClose.js">

	<cfset allDivsToToggle = listappend(attributes.alsoToggleDivIDs,attributes.divID)>
	<cfif attributes.closeImage is ""><cfset attributes.closeImage = "/images/MISC/blank.gif"></cfif>
	
	<!--- have we already pre-loaded these images? --->
	<cfif not structKeyExists (request.openCloseDiv.Images,"#attributes.openImage##attributes.closeImage#")>
		<cfset imageSet = "Image_#attributes.DivID#">
		<cfset request.openCloseDiv.Images["#attributes.openImage##attributes.closeImage#"] = imageSet>
<!--- 	debug	<cfoutput>images loaded for #attributes.openImage# and #attributes.closeImage# id = #imageSet#</cfoutput> --->
			<cfoutput>
			<script>
				divImages['#jsStringFormat(imageSet)#'] = new Array()
				divImages['#jsStringFormat(imageSet)#'].imgUp = new Image() ;
				divImages['#jsStringFormat(imageSet)#'].imgDown = new Image() ;
				divImages['#jsStringFormat(imageSet)#'].imgUp.src  = '#jsStringFormat(attributes.closeImage)#';
				divImages['#jsStringFormat(imageSet)#'].imgDown.src  = '#jsStringFormat(attributes.openImage)#';

	
			</script>
			</cfoutput>
		
	<cfelse>
		<cfset imageSet = request.openCloseDiv.Images["#attributes.openImage##attributes.closeImage#"]>			
	</cfif>

			<cfoutput>
			<script>

				function divtoggle_#jsStringFormat(attributes.divID)# (forceOpenClose) {
					return multipleDivOpenClose('#jsStringFormat(allDivsToToggle)#','#jsStringFormat(attributes.imageID)#','#jsStringFormat(imageSet)#','#jsStringFormat(attributes.pageid)#','#jsStringFormat(attributes.textAnchorID)#','#jsStringFormat(attributes.openText)#','#jsStringFormat(attributes.closeText)#',forceOpenClose)
				}

				function divtoggle2_#jsStringFormat(attributes.divID)# () {

					isNowOpen = divtoggle_#jsStringFormat(attributes.divID)#() ;

					if (isNowOpen) {
					<cfloop index="divid" list="#attributes.onOpenCloseDivIDs#">
					if (window.divtoggle_#jsStringFormat(divid)#){divtoggle_#jsStringFormat(divid)#('close');}  <!--- check function exists and then run it --->		
					</cfloop>
					
						<cfif attributes.onOpenSetFocusTo is "">

							// this puts the focus to anchor (either the image or the text anchor)
							focusObject = document.getElementById('#jsStringFormat(attributes.imageAnchorID)#')

							if (focusObject) {
								focusObject.focus()	
							} else {
								focusObject = document.getElementById('#jsStringFormat(attributes.textAnchorID)#')
								if (focusObject) {
									focusObject.focus()	
								}	
							}
						<cfelse>
							focusObject = document.getElementById('#jsStringFormat(attributes.onOpenSetFocusTo)#')
							if (focusObject) {
								focusObject.focus()	
							}	
						
						</cfif>

						
					}

					
					
				}
				
				
			</script>
			</cfoutput>
	
	<!--- work out whether item should be open or closed --->	
		<cfset open = true>
		<cfset cookieID = 'Div_' & attributes.pageID & '_' & listfirst(attributes.divID)>
	<cfif attributes.rememberSettings>
		<cfif structKeyExists (cookie,cookieID)>
			<cfif cookie[cookieID] is 1>
				<cfset open = true>
			<cfelse>
				<cfset open = false>
			</cfif>
		<cfelseif not attributes.startOpen>
				<cfset open = false>
		</cfif>
	<cfelseif not attributes.startOpen> 
				<cfset open = false>
	</cfif>

	
	<cfif open>
		<cfset caller.divStyle = "display:">
	<cfelse>
		<cfset caller.divStyle = "display:none;">
	</cfif>

	

	<cfset attributes.open = open >  <!--- easy way of passing to child tag --->
	<cfset attributes.imageSet = imageSet >  <!--- easy way of passing to child tag --->
	
</cfif>

<cfsetting enablecfoutputonly="No" >

