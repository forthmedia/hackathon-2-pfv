<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
cf_divOpenClose.cfm

contents moved to divOpenClosev1.cfm
 --->

<cfif not thisTag.hasEndTag>
	<cfthrow message="Invalid construct" detail="The cf_divOpenClose tag requires an end tag">
</cfif>

<cfparam name = "attributes.version" default="1">
<cfinclude template="divOpenClosev#attributes.version#.cfm">
