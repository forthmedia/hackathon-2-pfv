<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name = "attributes.divID">
<cfparam name = "attributes.title">
<cfparam name = "attributes.level" default="1">
<cfparam name = "attributes.startCollapsed" default="false">
<cfparam name = "attributes.disable" default="false">
<cfparam name = "attributes.disable" default="false">
<cfparam name = "attributes.onclick" default="">
<cfparam name = "attributes.wrapperClass" default="divOpenCloseWrapper">
<cfparam name = "attributes.level1HeaderClass" default="level1Header">
<cfparam name = "attributes.level2HeaderClass" default="level2Header">
<cfparam name = "attributes.level1ContentsClass" default="level1Contents">
<cfparam name = "attributes.level2ContentsClass" default="level2Contents">
<cfparam name = "attributes.style" default="">

<cfset level1ExpandedImg = "../images/MISC/arrow_down.gif">
<cfset level1CollapsedImg = "../images/MISC/arrow_right.gif">
<cfset level2ExpandedImg = "../images/MISC/icon_mkTreeMinus.gif">
<cfset level2CollapsedImg = "../images/MISC/icon_mkTreePlus.gif">

<cfif thisTag.executionMode is "start">
	<cfoutput>
		<cfif attributes.disable or not attributes.startCollapsed>
			<cfset initialStyle = "display: block;">
		<cfelse>
			<cfset initialStyle = "display: none;">
		</cfif>
		<cfif attributes.level eq 1>
			<cfset initialImage = level1ExpandedImg>
			<cfset headerClass = attributes.level1HeaderClass>
			<cfset contentsClass = attributes.level1ContentsClass>
			<cfif attributes.startCollapsed>
				<cfset initialImage = level1CollapsedImg>
			</cfif>
		<cfelse>
			<cfset initialImage = level2ExpandedImg>
			<cfset headerClass = attributes.level2HeaderClass>
			<cfset contentsClass = attributes.level2ContentsClass>
			<cfif attributes.startCollapsed>
				<cfset initialImage = level2CollapsedImg>
			</cfif>
		</cfif>

		<cfif not isdefined("request.javascriptWritten")>
			<cf_includeJavascriptOnce template = "/javascript/divOpenCloseV2.js">
			<cfset request.javascriptWritten = "true">
		</cfif>
		<div id="level#attributes.level#" class="#attributes.wrapperClass#">
			<div id="level#attributes.level#header" class="#headerClass#">
				<cfif not attributes.disable><img src="#initialImage#" alt="" width="16" height="15" border="0" onClick="toggleVisibility( '#attributes.divID#', this,'#attributes.level#');"></cfif>
				&nbsp;#htmleditformat(attributes.title)#
			</div>
			<div id="#attributes.divID#" class="#contentsClass#" style="#initialStyle##attributes.style#" <cfif attributes.onclick neq "">onclick="#htmleditformat(attributes.onclick)#"</cfif> >
	</cfoutput>
<cfelse>
	<cfoutput>
		</div>
	</div>
	</cfoutput>
</cfif>