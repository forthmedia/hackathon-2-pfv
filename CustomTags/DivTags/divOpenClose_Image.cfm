<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting 	enablecfoutputonly="yes">
<!--- 
WAB June 2005

Part of the divOpenClose custom tag
Used for displaying the image

Mods

2005-09-27	WAB		Better handling of toggling multiple divs from one button
2008-04-18		WAB		added a cancelbubble - if this image is in say a div which is also toggling the same item, need to make sure that event isn't passed to it because it just toggles it back again
2009/09/23    WAB   decided to allow generated content, which gets put in its own anchor
 --->

<cfset baseTag = "CF_DivOpenClose">
<CFASSOCIATE BASETAG="#baseTag#" >

<cfset parentAttributes = getBaseTagData(baseTag).attributes>

<cfif not isDefined("attributes.anchorpassthrough")>
	<cfset attributes.anchorpassthrough = parentattributes.imageanchorpassthrough >
</cfif>
<cfif not isDefined("attributes.imagepassthrough")>
	<cfset attributes.imagepassthrough = parentattributes.imagepassthrough >
</cfif>

<cfif not thisTag.hasEndTag or thisTag.executionmode is "end">
	<cfset generatedContent = thisTag.generatedContent>
	<cfset thisTag.generatedContent = "">
<cfoutput>
	<A id="#parentattributes.imageAnchorID#" HREF="javascript:void(0)" onClick="javascript: divtoggle2_#listFirst(parentattributes.divID)#();  event.cancelBubble = true; "  #attributes.AnchorPassThrough#>
		<img <!--- align=left ---> src="<cfif parentattributes.Open>#htmleditformat(parentattributes.closeImage)#<cfelse>#htmleditformat(parentattributes.openImage)#</cfif>" name = "#htmleditformat(parentattributes.imageID)#" id = "#htmleditformat(parentattributes.imageID)#" border = 0 #htmleditformat(attributes.imagePassThrough)# >
		</A>	
		<cfif generatedContent is not "">
			&nbsp;<A HREF="javascript:void(0)" onClick="javascript: divtoggle2_#listFirst(parentattributes.divID)#();  event.cancelBubble = true; "  #attributes.AnchorPassThrough#>#application.com.security.sanitiseHTML(generatedContent)#</A>	<!--- do not htmleditformat this one --->
		</cfif>
</cfoutput> 
</cfif>

<cfsetting 	enablecfoutputonly="no">

