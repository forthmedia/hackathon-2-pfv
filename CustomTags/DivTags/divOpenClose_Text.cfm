<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting 	enablecfoutputonly="yes">
<!--- 
WAB June 2005

Part of the divOpenClose custom tag
Used for displaying the Text

Mods 

2005-09-27	WAB		Added handling of changing text
2005-09-27	WAB		Better handling of toggling multiple divs from one button

 --->


<cfif not thisTag.hasEndTag>
	<cfoutput>divOpenClose_Text Requires End Tag</cfoutput>
	<cfexit>
</cfif>

<cfset baseTag = "CF_DivOpenClose">
<CFASSOCIATE BASETAG="#baseTag#" >

<cfparam name ="atributes.passthrough" default= "">
<cfset parentAttributes = getBaseTagData(baseTag).attributes>

<cfif not isDefined("attributes.anchorpassthrough")>
	<cfset attributes.anchorpassthrough = parentattributes.textanchorpassthrough >
</cfif>


<cfif thisTag.executionMode is "start">
	<cfoutput>
	<A id = "#parentattributes.textAnchorID#" HREF="javascript:void()" onClick="javascript:divtoggle2_#listFirst(parentattributes.divID)#(); return false;"  #attributes.AnchorPassThrough#>
	</cfoutput>

<cfelse>
	
	<cfoutput>
	<cfif parentattributes.Open>#parentattributes.closeText#<cfelse>#parentattributes.openText#</cfif>	
	</A></cfoutput>
</cfif>
<cfsetting 	enablecfoutputonly="no">
