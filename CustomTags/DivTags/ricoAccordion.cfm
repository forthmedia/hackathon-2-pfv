<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Custom Tag to display a rico accordion

Cut out of code by SWJ by WAB

Nov 2006


 --->

 
<cfparam name = "attributes.ContentQuery">
<cfparam name = "attributes.TitleColumn" default = "title">
<cfparam name = "attributes.DetailColumn" default = "detail">
<cfparam name = "attributes.width" default = "380px">
<cfparam name = "attributes.height" default = "400px">
<cfparam name = "attributes.margin" default = "5px">
<!--- <cfparam name = "attributes.styleSheet" default = "#request.currentsite. stylesheet#"> --->

	
	<cf_includeJavascriptOnce template = "/javascript/rico.js">

	
<cfif attributes.styleSheet is not "">
	<LINK REL="stylesheet" HREF="#htmleditformat(attributes.styleSheet)#">
<cfelse>
<style>
	.accordionTabTitleBar {
	   	font-size           : 12px;
		padding             : 4px 6px 4px 6px;
	   	border-style        : solid none solid none;
		border-top-color    : #BDC7E7;
		border-bottom-color : #182052;
	   	border-width        : 1px 0px 1px 0px;
	}
	
	.accordionTabTitleBarHover {
	   	font-size        : 11px;
		background-color : #1f669b;
		color            : #000000;
	}
	
	.accordionTabContentBox {
	   	font-size        : 11px;
	   	border           : 1px solid #1f669b;
	   	border-top-width : 0px;
	   	padding          : 10px;
	}

	.accordionTabContentBox p {
	   	font-size        : 11px;
	}
	
	.accordionTabContentBox ol, ul, li {
		list-style-type: disc;
		font-size: 11px;
		line-height: 1.8em;
		margin-top: 0.2em;
		margin-bottom: 0.1em; 
	{

</style>
</cfif>	
	
<cfset contentQuery = attributes.contentQuery>
	<cfoutput>
	<table><tr>
	<td valign="top">
		<div id="accordionDiv" style="width:#attributes.width#; margin:#attributes.margin#; border-top-width:1px; border-top-style:solid;">
			<cfloop query="ContentQuery">
			   	<div id="overviewPanel">
				    <div id="overviewHeader" class="accordionTabTitleBar">#contentQuery[attributes.TitleColumn][currentrow]# </div>
				  	<div id="panel1Content" class="accordionTabContentBox">
			       		#contentQuery[attributes.DetailColumn][currentrow]#
			      	</div>  
			   </div>
		   </cfloop>
		</div>
	</td>
	</tr>
	</table>


<script>
	new Rico.Accordion( $('accordionDiv'), {panelHeight:#replaceNoCase(attributes.height,"px","")#,expandedBg:'cc0000',collapsedBg:'cc0000',hoverBg:'cc0000',collapsedTextColor:'ffffff',borderColor:'cccccc'} );
</script>
	</cfoutput>

	