<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting 	enablecfoutputonly="yes">
<!--- 
WAB June 2005

Part of the divOpenClose custom tag
Call this in the middle of any html tag to output the onclick event
Used for opening and closing a div based on clicking on any object


usage
eg 
<TR <cf_divOpenClose_onClick>  >  ><TD>click here to open</TD></TR>

Mods

 --->

<cfset baseTag = "CF_DivOpenClose">
<CFASSOCIATE BASETAG="#baseTag#" >

<cfset parentAttributes = getBaseTagData(baseTag).attributes>



<cfoutput>
	onClick="javascript:divtoggle2_#listFirst(parentattributes.divID)#(); return false;" 
</cfoutput> 

<cfsetting 	enablecfoutputonly="no">
