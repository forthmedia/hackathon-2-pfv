<!--- �Relayware. All Rights Reserved 2014 --->

<!--- NJH 2015/07/14 - deal with case when typename isn't defined in qoq metadata. Default to nvarchar(255) 
		WAB 2015-11-25 Change 255 to Max
--->
<cfparam name="attributes.query">
<cfparam name="attributes.name">
<cfset theQuery= attributes.query>
<cfparam name="attributes.columnList"  default="#thequery.columnList#" >

<!--- CREATE TABLE --->
<CFSET METADATA = getMetaDataStructure(theQuery)>
<cfset firstpass = true>
<cfset schema = "">

<cfif left(attributes.name,1) eq "##">
	<cfset schema = "tempdb..">
</cfif>

<cfquery name="createTable">
	if object_id('#schema##attributes.name#') is not null
		drop table #attributes.name#

	create table #attributes.name#
	(
		<cfloop index="column" list="#attributes.columnlist#">
			<cfif firstpass><cfset firstpass = false><cfelse>,</cfif>
			#column#
			<cfif structKeyExists(metadata[column],"typeName")>
				<cfswitch expression="#metadata[column].typename#">
					<cfcase value="varchar">nvarchar (max)</cfcase>
					<cfcase value="integer,bigint">int</cfcase>
					<cfcase value="date">datetime</cfcase>
					<cfcase value="bit">bit</cfcase>
				</cfswitch>
			<cfelse>
				nvarchar (max)
			</cfif>
		</cfloop>
	)
</cfquery>

<cfset endRow=0>
<cfset processNumRecords = 1000>
<cfset counter = (theQuery.recordCount\processNumRecords)+1>
<cfloop from="1" to="#counter#" step="1" index="count">
	<cfset startRow = endRow+1>
	<cfif count neq counter>
		<cfset endRow = count*processNumRecords>
	<cfelse>
		<cfset endRow = startRow-1+(theQuery.recordCount mod processNumRecords)>
	</cfif>

	<cfif startRow lt theQuery.recordCount or theQuery.recordCount eq 1>
		<cfquery name="insertData">
			insert into #attributes.name#
			<cfloop query="theQuery" startRow="#startRow#" endrow="#endRow#">
			<cfset firstPass=true>
			select 	<cfloop index="column" list="#attributes.columnlist#">
						<cfif firstpass>
							<cfset firstpass = false>
						<cfelse>,
						</cfif>
						<cfset typename = structKeyExists(metadata[column],"typename")?metadata[column].typename:"varchar">
						<cf_queryParam value="#theQuery[column][currentrow]#" cfsqltype="cf_sql_#typename#" useCFVersion=false>
					</cfloop>
			<cfif currentRow lt endRow>union</cfif>
			</cfloop>
		</cfquery>
	</cfif>
</cfloop>

<!--- <cfloop query="attributes.query">

	<cfset firstpass = true>
	<cfquery name = z datasource ="#application.sitedatasource#">
		insert into #attributes.name#
		values (
		<cfif firstpass><cfset firstpass = false><cfelse>,</cfif>
		<cfloop index="column" list="#attributes.columnlist#">
			'#attributes.query[column][currentrow]#'
		</cfloop>
		)
	</cfquery>
</cfloop> --->




	<cffunction name="getMetaDataStructure">
		<cfargument name="queryObject">

		<cfset var metadataArray = getMetaData(queryObject)>
		<cfset var i = 0>
		<cfset var result = structNew()>

		<cfloop index="i" from = "1" to="#arrayLen(metaDataArray)#">
			<cfset result[metaDataArray[i].name] = metaDataArray[i]>
			<cfset result[metaDataArray[i].name].position = i>
		</cfloop>

		<cfreturn result>

	</cffunction>
