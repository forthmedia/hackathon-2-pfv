<!--- �Relayware. All Rights Reserved 2014 --->
<!------------------------------------------------------------------------
NAME:         	CF_RandomPassword
FILE:	        RandomPassword.cfm
VERSION:	  	1.0	
CREATED:	 	1998-23-02
LAST MODIFIED:	1998-24-02
AUTHOR:      	Rob Bilson (rbils@amkor.com)
DESCRIPTION:  	CF_RandomPassword is a custom CFML tag that allows a user
				to generate random passwords for use in their applications.
				The parameters governing the creation of the passwords is
				fully customizable.  Passwords can be of any length and
				can contain numbers, symbols, uppercase, and lowercase
				letters in any combination.  This tag can easily be
				incorporated into registration modules to allow for the
				automatic generation of random passwords.
COPYRIGHT:    	This tag is freeware and can thus be freely used, copied,
		      	modified, and distributed as long as this header is left
			  	intact.  Please annotate any changes in the code before
			  	modifying.  If you use this tag out on the Internet, I
			 	would appreciate hearing how you are implementing it.
			  	Feel free to e-mail me at: rbils@amkor.com
DISCLAIMER:  	No waranties, either expressed or implied are granted
			  	with this software.  This program is provided "as is".
			  	The author, Amkor Electronics, and anyone else affiliated
			  	with the creation and use of this tag shall assume no
			  	liability for problems arising out of the use of this tag.
------------------------------------------------------------------------->
<!---
Mods:  CPS 04/Apr/01  Include new parameter attributes.excludeCharacters to exclude them from the list
                      of available characters from which the random password is generated 	   
 --->

<!--- Assign parameters values based on attributes --->
<CFIF ISDEFINED("attributes.NumberOfCharacters")>
		<CFIF attributes.NumberOfCharacters IS NOT "">
				<CFSET NumberOfCharacters=attributes.NumberOfCharacters>
			<CFELSE>
				<CFSET NumberOfCharacters=7>
		</CFIF>
	<CFELSE>
		<CFSET NumberOfCharacters=7>
</CFIF>

<CFIF ISDEFINED("attributes.UseSymbols")>
		<CFIF attributes.UseSymbols IS NOT "">
				<CFSET UseSymbols=attributes.UseSymbols>
			<CFELSE>
				<CFSET UseSymbols="No">
		</CFIF>
	<CFELSE>
		<CFSET UseSymbols="No">
</CFIF>

<CFIF ISDEFINED("attributes.UseNumbers")>		
		<CFIF attributes.UseNumbers IS NOT "">
				<CFSET UseNumbers=attributes.UseNumbers>
			<CFELSE>
				<CFSET UseNumbers="Yes">
		</CFIF>
	<CFELSE>
		<CFSET UseNumbers="Yes">
</CFIF>
		
<CFIF ISDEFINED("attributes.UseUppercaseLetters")>		
		<CFIF attributes.UseUppercaseLetters IS NOT "">
				<CFSET UseUppercaseLetters=attributes.UseUppercaseLetters>
			<CFELSE>
				<CFSET UseUppercaseLetters="Yes">
		</CFIF>
	<CFELSE>
		<CFSET UseUppercaseLetters="Yes">
</CFIF>
		
<CFIF ISDEFINED("attributes.UseLowercaseLetters")>		
		<CFIF attributes.UseLowercaseLetters IS NOT "">
				<CFSET UseLowercaseLetters=attributes.UseLowercaseLetters>
			<CFELSE>
				<CFSET UseLowercaseLetters="Yes">
		</CFIF>
	<CFELSE>
		<CFSET UseLowercaseLetters="Yes">
</CFIF>

<CFIF UseSymbols IS "No" AND UseNumbers IS "No" AND UseUpperCaseLetters IS "No" AND UseLowerCaseLetters IS "No">
	<CFSET caller.RandomPassword="No random password can be generated because all attributes have a ""No"" Value.">
<CFELSE>

<!---------------------------------------------------------------------------
	  This sets the characters list.  If you don't want to use certain
	  characters, simply remove their corresponding ascii character
	  number from the appropriate list
---------------------------------------------------------------------------->
<!--- NJH 2010/09/20 - removed single quote from symbols list - value 39 --->
<CFSET Symbols="33,34,35,36,37,38,40,41,42,43,44,45,46,47,58,59,60,61,62,63,64,91,92,93,94,95,96,123,124,125,126">
<CFSET Numbers="48,49,50,51,52,53,54,55,56,57">
<CFSET UppercaseLetters="65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90">
<CFSET LowercaseLetters="97,98,99,100,101,102,103,104,105,106,107,109,110,112,113,114,115,116,117,118,119,120,121,122">
<!---Removed numbers zero, one - letters O, el in order to avoid confusion--->


<!--- Begin app --->
<CFSET CharacterList="">
<CFSET CharacterString="">
<CFIF UseSymbols IS "Yes">
<CFSET CharacterList=ListAppend(CharacterList, Symbols, ",")>
</CFIF>

<CFIF UseNumbers IS "Yes">
<CFSET CharacterList=ListAppend(CharacterList, Numbers, ",")>
</CFIF>

<CFIF UseUppercaseLetters IS "Yes">
<CFSET CharacterList=ListAppend(CharacterList, UppercaseLetters, ",")>
</CFIF>

<CFIF UseLowercaseLetters IS "Yes">
<CFSET CharacterList=ListAppend(CharacterList, LowercaseLetters, ",")>
</CFIF>

<CFIF IsDefined("attributes.ExcludeCharacters")>
	<CFSET ExcludeCharacters = attributes.ExcludeCharacters>
<CFELSE>
	<!---Removed numbers zero, one - letters O, el in order to avoid confusion--->
	<CFSET ExcludeCharacters = '48,49,108,111'>
</CFIF>	 
	
<CFLOOP INDEX="ExcludeCharacter" LIST="#ExcludeCharacters#">
	<CFIF ListFind(CharacterList, ExcludeCharacter) GT 0>
		<CFSET CharacterList = ListDeleteAt(CharacterList, ListFind(CharacterList, ExcludeCharacter))>
	</CFIF>		
</CFLOOP>
	
<CFSET TheLength=ListLen(CharacterList)>
	
<CFLOOP INDEX="CharacterPlaces"
        FROM="1" TO="#NumberOfCharacters#">

<CFSET GetPosition=RandRange(1,TheLength)>
<CFSET Character=ListGetAt(CharacterList, GetPosition, ",")>
<CFSET CharacterString=ListAppend(CharacterString, #CHR(Character)#, ",")>
</CFLOOP>

<CFSET caller.RandomPassword=#ListChangeDelims(CharacterString, "")#>
</CFIF> 

