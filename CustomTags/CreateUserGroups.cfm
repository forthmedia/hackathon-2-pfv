<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CreateUserGroups.cfm

Template to add a person to UserGroups 

It is based on either
	they meet the criteria of a selection (as defined in userGroupDefinition).
	or they have particular flags set (as defined in rightsFlagGroup)

(rightsFlagGroup is the easiest to use and understand, but it is somewhat 
less flexible (can't handle flag A AND flag B) type definitions.  However we'll
see how we get on with it for the time being and if need be we can start using the 
selections version)


Takes PersonID  personID of person to check
Optional debug = true

eg.  <CF_createUserGroups personid = 999067>

WAB 2000-06-05 created the selections stuff using selectrefresh.cfm as a basis
		flag stuff is from some code that Paul wrote


 --->

<!--- Mods:
2007/02/13	GCC 	Dodgy attempt to set visibility user groups based on org integer flags for the person
2001-03-14		WAB		Added a special case for flagid = 0.  In this automatically adds the person to the specified usergroup regardless of flags etc.
					(added so that all users can be added to the 'web user' user group)


 --->

<cfparam NAME="attributes.personid" default="request.relaycurrentuser.personid">
<CFPARAM name="attributes.debug" default = "false">


<cfset application.com.login.addPersonToUserGroupsByProfile(attributes.personid)>

<CFIF attributes.debug>
	<CFQUERY NAME="getUserGroups" DATASOURCE="#application.SiteDataSource#">
	select 
		ug.name,
		s.title,
		case when rg.PersonID is not null then 1 else 0 END as member
		 
	from 
		selection as s,  userGroupDefinition as ugd, userGroup as ug, rightsGroup as rg
	where 
			 ug.userGroupid = ugd.userGroupid
		and ugd.selectionid = s.selectionid
		and ug.usergroupid *= rg.usergroupid 
		and (rg.personid =  <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  or rg.personid is Null)
	order by 
		rg.personid desc

	</CFQUERY>



	<TABLE>
		<tr><td>UserGroup Name</td><td>Selection Title</td></tr>
	<CFOUTPUT query="getUserGroups" group="member">
		
		<TR><TD colspan="2"><CFIF member is 1>Member of<CFELSE>Not a member of</cfif></P></td></tr>
		<CFOUTPUT>
		<tr><td>#htmleditformat(name)#</td><td>	(#htmleditformat(title)#)</td></tr>
		</cfoutput>			
	
	</cfoutput>		
	</table>
	
	Note where a UserGroup is defined by more than one selection, the above list does not tell you which selections the person is actually in (just one of them or none of them)!

</CFIF>	
	

