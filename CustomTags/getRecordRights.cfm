<!--- �Relayware. All Rights Reserved 2014 --->
<CFSILENT>
<!--- cf_getRecordRights --->
<!--- 

Purpose
	A stab at a general purpose 

takes
	entityType    eg person, location, (anything in the flagentityTypeTable)
	entityID		record number
	
returns
	query called "rights" giving level of rights to entity  zero records if no rights
	also returns a query called "entity" giving the record
	
Mods

WAB 2001-03-26		Added check on organisation country	

 --->

 
<CFPARAM name="attributes.entityType">
<CFPARAM name="attributes.entityID"> 
 


<!---
Get information about this entityType from the flagEntityType table
Probably ought to be stored as an application structure

 --->
<CFQUERY NAME="EntityType" DATASOURCE=#application.SiteDataSource#>				
SELECT * from flagEntityType
WHERE tableName =  <cf_queryparam value="#attributes.entityType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>


 <!---
1) Look at the entity table and get the record in question
a) for person and location, need to find the countryid and check country rights
 	could in theory have recordrights at a person/location level and these coulb be implemented here
b) rights to an organisation are undefined, probably need rights to at least one country that the organisation has locations in
c) other tables we could go straight to the record rights table for, although some tables have a bit
field indicating whether that record is using record rights
So all in all, probably need to get the underlying record


	--->
	

<CFSET tableName = entityType.TableName>

			<CFQUERY NAME="EntityRecord" DATASOURCE=#application.SiteDataSource#>				
				Select 	#tablename#.*,
						#preserveSingleQuotes(EntityType.NameExpression)# as name,
						<CFIF "location,Person" contains tableName>
						countryid, location.organisationid,
						</CFIF>
						<CFIF tableName is "organisation">
						countryid, 
						</CFIF>
						(select name from usergroup where #tablename#.lastupdatedby = usergroupid) as LastUpdatedbyName
				From 	#tablename# 
						<CFIF tablename is "Person">		
						,Location
						</CFIF>
				where	#EntityType.UniqueKey# =  <cf_queryparam value="#attributes.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						<CFIF tablename is "Person">		
						AND Person.LocationId = Location.LocationID
						</CFIF>
			</CFQUERY> 
 

				<CFSET caller.entity = EntityRecord>

<CFIF "Person,Location,country,organisation" contains tableName>
	<!--- Need to check country rights --->
	<CFSET reqdCountryID = entityRecord.Countryid>
	<CFSET partnerCheck =true>
	<CFINCLUDE template = "templates\qryGetCountries.cfm">
	<!--- returns query findCountries --->
	
	
	<!--- might need to check for rights to individual people or location here --->


	<CFSET caller.rights = findCountries>

	<!--- check for partner rights/restrictions here--->

		<CFIF findCountries.Level4 IS 1 and "Person,Location,organisation" contains tableName>
				<!--- partner rights, check that this record is from same organisation--->
				<CFQUERY NAME="getOrganisation" DATASOURCE=#application.SiteDataSource#>
				select Organisationid from person where personid = #request.relayCurrentUser.personid#
				</CFQUERY>

				<CFIF getOrganisation.Organisationid IS NOT entityRecord.Organisationid>
					<!--- create a null query --->
					<CFSET rights = queryNew("updateOkay,AddOkay,Level2,Level3,Level4")>
					<CFSET caller.rights = rights>

				</cfif>
				
				<!--- special case for when a partner is adding themselves, needs to have add rights --->
				<CFIF request.relayCurrentUser.personid is 0>
					<CFSET temp = queryAddRow(rights)>
					<CFSET temp = querySetCell(rights,AddOkay,1)>
					<CFSET temp = querySetCell(rights,Level3,1)>
					<CFSET caller.rights = rights>				
				</cfif>
				
		</CFIF>


<CFELSE>


	<CFQUERY NAME="checkRight" DATASOURCE=#application.SiteDataSource#>
	SELECT 
		Convert(bit,sum(rr.permission & 8)) AS Level4,
		Convert(bit,sum(rr.permission & 4)) AS Level3,
		Convert(bit,sum(rr.permission & 2)) AS Level2,
		Convert(bit,sum(rr.permission & 4)) AS AddOkay,
		Convert(bit,sum(rr.permission & 2)) AS UpdateOkay

	FROM RecordRights AS rr, RightsGroup AS rg
	WHERE 
			rr.UserGroupID = rg.UserGroupID
		AND rg.PersonID = #request.relayCurrentUser.personid#
<!--- 	HAVING Sum(rr.Permission) > 0 --->
	</CFQUERY>

	<CFSET caller.rights = checkRight>

</CFIF> 
</CFSILENT>
