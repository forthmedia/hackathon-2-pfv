<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			UGRecordManager.cfm
Author:			AH
Date created:	20 April 2000

Description:

Custom Tag. Allows users to be added to User Groups and Security Tasks to be created and associated with user groups.

Dependencies:

Expects parameters: Entity, EntityID (promotion, application etc...)


Usage: <CF_UGRecordManager entity="elements" entityid="#RECORDID#" Form="MyForm">

Entity:
Required. Specifies entity in the table RecordRights

EntityID:
Required. Specifies RecordID in the table RecordRights

Level:
Optional. Permission level.  defaults to 1 (so that value of the permission field will be 1 [ 2 ^ (level -1)].  So Level 3 deals with permission 4

DefaultUserGroup:
OPtional.  If there is no existing record in the recordRights table, then can set a default usergroupid to appear

Also Very Important: In order to process this tag place the tag "UGRecordManagerTask" wherever you are
processing the results of this form.

Version history:
1	-	Initial version
1.5		WAB added ability to set what permission level is being used and added new attributes to do with that functionality - should be backwards compatible
1.6		2001-07-01 SWJ modified getAllUsers to supress person.loginExpires > getDate()
13-Nov-2003	KAP		New suppressGroups attribute added
NYB 2011-04-01 LHID5410 changing the path for 8.3 webservices
2015-12-01	WAB PROD2015-290 Visibility Project.  Implementation of 'ExtendedRights' - AND/OR/NOT 
2016-09-29	WAB	PROD-2405 	By default, do not show Personal UserGroups at Level1 (Rights to view things on Portal should only ever be given to actual user groups)
							But do allow existing data to show
							And (as an experiment) keep the current user's usergroup on display

--->
<CFPARAM NAME="Attributes.Entity" >
<CFPARAM NAME="Attributes.EntityID">
<CFPARAM NAME="Attributes.Form" DEFAULT="">
<CFPARAM NAME="Attributes.level" DEFAULT="1" type="numeric">
<CFPARAM NAME="Attributes.DefaultUserGroup" DEFAULT="">
<CFPARAM NAME="Attributes.caption1" DEFAULT="Potential Members:">
<CFPARAM NAME="Attributes.caption2" DEFAULT="Current Members:">
<CFPARAM NAME="Attributes.method" DEFAULT="edit">
<CFPARAM NAME="Attributes.userGroupTypes" TYPE="string" DEFAULT=""> <!--- NJH 2009/02/04 P-SNY047 - limit the user groups shown to just certain types --->
<CFPARAM NAME="Attributes.userGroupTypesToExclude" TYPE="string" DEFAULT="#(attributes.level is 1)?'personal':''#"> <!--- NJH 2009/02/04 P-SNY047 - limit the user groups shown to just certain types --->
<CFPARAM NAME="Attributes.userGroupFilter" TYPE="boolean" DEFAULT="false"> <!--- NJH 2009/02/04 P-SNY047 - draw a filter of usergroup types. If this is true, we then bind
																			the 'ALL User Groups' dropdown to this filter. --->
<CFPARAM NAME="Attributes.singleUserGroupTypeOnly" Type="boolean" DEFAULT="false"> <!--- NJH 2009/02/10 P-SNY047 enforces rule that only one user group type can be chosen. So, if filter changes, the list is reset --->

<cfparam name="attributes.suppressGroups" type="boolean" default="false">
<cfparam name="attributes.width" type="string" default="150"> <!--- NJH 2009/03/11 P-SNY047 --->
<cfparam name="attributes.operatorsToShow" type="string" default="0_0">
<cfparam name="attributes.defaultOperator" type="string" default="#listfirst(attributes.operatorsToShow)#">
<cfparam name="attributes.hideOperatorText" type="boolean" default="#iif (attributes.operatorsToShow is "0_0",1,0)#">
<cfparam name="attributes.useExtendedRights" type="boolean" default="false">

	<CFSET EntityType = Attributes.Entity>
	<CFSET EntityID = Attributes.EntityID>
	<CFSET level = Attributes.level>
	<CFSET EntityTypeID = application.entityTypeID[entityType]>


<cfif attributes.method is "edit" and level is 1 and attributes.useExtendedRights is true>

	<cfset structDelete (attributes, "defaultOperator")>
	<cfset structDelete (attributes, "useExtendedRights")>
	<cf_UGRecordManager level = 1 attributeCollection = #attributes# operatorsToShow = "0_0,1_0,0_1" >
		<!---2015/12/08 SB Added HTML  --->
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div id="ugrrecordjoin">
					<a id="addmorerightslink">Add More Visibility Restrictions +</a>
					<b id="and">AND</b>
				</div>
			</div>
		</div>
	<cfset outerDivID = "ug_11">
	<cfoutput>
		<div id="#outerDivID#">
		<cf_UGRecordManager level = 11 attributeCollection = #attributes# operatorsToShow = "0_0,1_0,0_1" hideOperatorText= false defaultOperator = "0_1">
		</div>
			<script>

			<cfif not hasRightsSet>
				jQuery('###outerDivID#').hide()
				jQuery('##addmorerightslink').show().on('click', function () {
																	console.log (this)
																	this.hide();
																	jQuery('###outerDivID#').show();
																	jQuery('##and').show()
															})
				jQuery('##and').hide()
			<cfelse>
				jQuery('##addmorerightslink').hide()
			</cfif>
			</script>
	</cfoutput>


<cfelse>

	<cfif attributes.method is "edit">

	<!---Create lists of existing members--->
		<CFQUERY NAME="GetUsers" DATASOURCE="#Application.SiteDataSource#">
			select distinct
				name ,
				ug.usergroupid ,
				userGroupType as optGroup
			from
				UserGroup as ug,
				recordRights as rr
			where
				ug.UserGroupID = rr.UserGroupID
			and	Entity =  <cf_queryparam value="#EntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and permission & power(2,#level# -1) <> 0
			<CFIF isNumeric(entityID)>
			AND RecordID =  <cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<CFELSE>
			AND 1 = 0
			</CFIF>
			ORDER BY userGroupType, Name
		</CFQUERY>

		<cfset caller.hasRightsSet = getUsers.RecordCount>

		<CFSET OrigListOfUserGroupID = valuelist(GetUsers.userGroupID)>
		<cfif getUsers.recordCount is 0  and attributes.defaultUserGroup is not "">
			<CFQUERY NAME="GetUsers" DATASOURCE="#Application.SiteDataSource#">
				select distinct
					name,
					ug.usergroupid,
					userGroupType as optGroup
				from
					UserGroup as ug
				where
					ug.UserGroupID  in ( <cf_queryparam value="#attributes.defaultUserGroup#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				ORDER BY Name
			</CFQUERY>

		</cfif>

		<CFSET ListOfUserGroupID = valuelist(GetUsers.userGroupID)>

	<!--- NJH 2009/02/09 P-SNY047 - moved the query below to a function --->
	<!---Get all available users (whoever has a UserGroupID created in the UserGroup table--->
<!--- 		<CFQUERY NAME="GetAllUsers" DATASOURCE="#Application.SiteDataSource#">
		<cfif not attributes.suppressGroups>
			select name as Column2, usergroupid as Column1, 1 as orderby
			from usergroup
			where personid is null
			<CFIF ListOfUserGroupID is not "">
			and usergroupid NOT IN (#ListOfUserGroupID#)
			</cfif>
			<cfif attributes.userGroupTypes neq "">
				and userGroupType in ('#replace(attributes.userGroupTypes,",","','","ALL")#')
			</cfif>

			union

			select '----------------', 0 , 2

			union
		</cfif>
			select name as Column2, usergroupid as Column1, 3 as orderby
				FROM usergroup inner join person on userGroup.personid = person.personid
				where userGroup.personid is not null
				and person.loginExpires > getDate()
			<CFIF ListOfUserGroupID is not "">
			and usergroupid NOT IN (#ListOfUserGroupID#)
			</cfif>
			<cfif attributes.userGroupTypes neq "">
				and userGroup.userGroupType in ('#replace(attributes.userGroupTypes,",","','","ALL")#')
			</cfif>
		<!--- </CFIF> --->

			order by orderby , name
		</CFQUERY> --->

		<cfscript>
				GetAllUsers = application.com.relayUserGroup.getUserGroupsForSelect(userGroupTypes=attributes.userGroupTypes,suppressGroups=attributes.suppressGroups, userGroupTypesToExclude = attributes.userGroupTypesToExclude, additionalUserGroupIDsToInclude = listAppend(ListOfUserGroupID, request.relayCurrentUser.userGroupID));
		</cfscript>

		<!--- NJH 2009/02/09 P-SNY047 - add a filter to the usergroup --->
		<cfif attributes.userGroupFilter>
			<cfquery name="getUserGroupTypes" datasource="#application.siteDataSource#">
				select distinct userGroupType as display, userGroupType as value from userGroup
				<cfif attributes.userGroupTypes neq "">
					where userGroupType  in ( <cf_queryparam value="#attributes.userGroupTypes#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) <!--- '#replace(attributes.userGroupTypes,",","','","ALL")#' --->
				</cfif>
			</cfquery>

			<!--- NYB 2011-04-01 LHID5410 changing the path for 8.3 webservices --->
			<cfset bindFunction = 'cfc:webservices.relayUserGroupWS.getUserGroupsForSelect({UG_#EntityType#_#EntityID#_#level#_filter},{UG_#EntityType#_#EntityID#_#level#_hidden})'>
			<cfset filterQuery = getUserGroupTypes>
		<cfelse>
			<cfset bindFunction = "">
			<cfset filterQuery = "">
		</cfif>

				<!--- get current and/not operator, and if no record use the default supplied --->
				<CFIF isNumeric(entityID)>
	
					<cfquery name="getOperator">
					select
						  distinct
						  GroupAnd_Level#level# as GroupAND,
						  GroupNot_Level#level# as GroupNot
					from vrecordrightsoperator rro
							inner join
						recordrights rr on rr.recordid  = rro.recordid and rr.entityTypeID  = rro.entityTypeID
					where
						rro.recordid = #EntityID# 
						and rro.entityTypeID = #EntityTypeID#
						and rr.permission & power(2,#level# - 1) <> 0
					</cfquery>
				</cfif>

				<cfif not isNumeric(entityID) or getOperator.recordCount is 0>
					<cfset getOperator = {	  groupAnd = listFirst (attributes.defaultOperator,"_")
											, groupNot = listLast (attributes.defaultOperator,"_")
										}>
				</cfif>

				<!--- 	This query both works out the valid values (which ones to display depends on attribute settings)
						Also works out which is currently selected
						And makes sure that if we aren't displaying the drop down then some read only text is displayed
				 --->

				<cfquery name ="validValues">
				declare
					  @currentGroupAnd bit = <cf_queryparam value="#getOperator.groupAnd#" CFSQLTYPE="CF_SQL_BIT">
					, @currentGroupNot bit = <cf_queryparam value="#getOperator.groupNot#" CFSQLTYPE="CF_SQL_BIT">

				select *
				from (
						select
							convert(varchar,options.groupAnd )+ '_' + convert(varchar,options.groupNot) as value,
							text as display,
							case when options.groupand = @currentGroupAnd  and options.groupnot = @currentGroupNot then 1 else 0 end as isSelected,
							groupAnd,
							groupNot

						from
								(
								values
									 (0,0,'Any Of')
									,(1,0,'All Of')
									,(0,1,'None Of')
									,(1,1,'')
								) as options (groupand,groupnot,text)
					) as x
				where
					(groupand = @currentGroupAnd  and groupnot = @currentGroupNot) <!--- make sure that current value is there --->
					OR
					value in (<cf_queryparam value="#attributes.operatorsToShow#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)

				</cfquery>

				<cfoutput>
					<cfset showOperatorColumn = false>
					<cfset rightColumnClass = "col-sm-12">
					<cfif validValues.recordCount GT 1 OR not attributes.hideOperatorText>
						<cfset showOperatorColumn = true>
						<cfset rightColumnClass = "col-sm-9">
					</cfif>
					<div class="row">
						<cfif showOperatorColumn>
							<div class="col-sm-3">
								<cfset groupOperatorFieldName = "UG_#EntityType#_#EntityID#_#level#_GroupANDNot">
								<cfif validValues.recordCount GT 1>
									<!--- The drop down for AND/OR/NOT--->
									#application.com.relayForms.ValidValuedisplay (query= validValues, name=groupOperatorFieldName, value="value", display="display" )#
								<cfelseif not attributes.hideOperatorText>
									<cf_input value="#validValues.display#" disabled="true" class="form-control" />
									<cfinput name="#groupOperatorFieldName#" type="hidden" value="#validValues.value#">
								</cfif>
							</div>
						</cfif>
							<div class="#rightColumnClass#">
								<cfset pluginOptions = {multipleSelect = true}>
								<!--- The user drop down --->
								#application.com.relayForms.ValidValuedisplay (query=getAllUsers, name="UG_#EntityType#_#EntityID#_#level#", selected = valuelist(GetUsers.usergroupid), value="usergroupid", display="name", displayAs="multiselect", plugInOptions = pluginOptions, keepOrig = true )#
							</div>
					</div>
				</cfoutput>




<cfelse>

		<CFQUERY NAME="GetUsers" DATASOURCE="#Application.SiteDataSource#">
		select dbo.getRecordRightsList (#entityId#,#EntityTypeID#,#level#,1) as list
		</cfquery>

		<cfoutput >
			#htmleditformat(GetUsers.list)#<BR>
	</cfoutput>

</cfif>

</cfif>





