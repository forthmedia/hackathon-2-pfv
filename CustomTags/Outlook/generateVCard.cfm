<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">

<cfparam name="attributes.lastname">
<cfparam name="attributes.firstname">
<cfparam name="attributes.mi" default="">
<cfparam name="attributes.company" default="">
<cfparam name="attributes.workphone1" default="">
<cfparam name="attributes.workphone2" default="">
<cfparam name="attributes.mobilephone" default="">
<cfparam name="attributes.workfax" default="">
<cfparam name="attributes.workaddress" default="">
<cfparam name="attributes.workcity" default="">
<cfparam name="attributes.workstate" default="">
<cfparam name="attributes.workzip" default="">
<cfparam name="attributes.email" default="">
<cfparam name="attributes.url" default="">
<cfparam name="attributes.workcountry" default="United Kingdom">
<cfparam name="attributes.revdate" default="#dateformat(now(),"yyyymmdd")#">
<cfif isdate(attributes.revdate)>
	<cfset attributes.revdate = dateformat(attributes.revdate,"yyyymmdd")>
</cfif>
<cfparam name="attributes.r_vcard">

<cfsavecontent variable="caller.#attributes.r_vcard#">
<CFOUTPUT>BEGIN:VCARD
VERSION:2.1
N:#attributes.lastname#;#attributes.firstname#;#attributes.mi#
FN:#attributes.firstname# #attributes.mi# #attributes.lastname#
TITLE:#attributes.jobdesc#
ORG:#attributes.company#
TEL;WORK;VOICE:#attributes.workphone1#
TEL;WORK:#attributes.workphone2#
TEL;CELL:#attributes.mobilephone#
TEL;WORK;FAX:#attributes.workfax#
ADR;WORK:;;#attributes.workaddress#;#attributes.workcity#;#attributes.workstate#;#attributes.workzip#;#attributes.workcountry#
URL;WORK:#attributes.url#
<cfif attributes.email is not "">
EMAIL;PREF;INTERNET:#attributes.email#
</cfif>
REV:#attributes.revdate#T095246Z
END:VCARD</cfoutput>
</cfsavecontent>

<cfsetting enablecfoutputonly="No">