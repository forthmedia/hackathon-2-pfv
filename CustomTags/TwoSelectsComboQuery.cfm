<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Two selects Combo --->
<!---

19 05 00 attacked by WAB so that you can have more than one on a page
13 06 01 CPS amended MoveFromTo to allow movement of multiple selections
2009/02/09   WAB Added doubleclick functionality
2009/02/09	NJH	P-SNY047 added functionality whereby a user could bind the first select box based on values to another select box
			which is drawn if a third query (filterQuery) is passed through and the bindFunction is set. This is currently being used for
			UGRecordManager, where the 'All User Groups' dropdown is bound to a drop down showing the userGroupTypes.
2011/04/01	WAB change so that can be used outside of CFForm
			Uses new cf_select rather than cfselect
			Tested that it works with a bind function
2011/11/30			WAB			Mods to JS to make sure up/down worked on any form
								Needed to add attributes.doSort to prevent automatic sorting when up/down
2013-02-19	WAB	Discovered that dblClick did not work from right to left (actually did a left to right instead)
2013-02-20	WAB	Sprint 15&42 Comms - added support for custom isDirty functions on elements
2013-05-23 WAB Subsequent to NJH changes, no longer need the isDirty stuff I had added
2013/09/03	NJH	Case 436844 - Removed the required attribute off the second select, and added it to the hidden field.
2013/09/03	NJH	Case 437964 - add the filter label
2015-03-02	WAB Added data-defaultValue attribute to hidden field so that isDirty() js function picks up changes to this control
2016-01-21	WAB	PROD2015-370 Implement multiselect box throughout.  This control is overriden unless using a bind, has a filter or useJqueryMultiSelect is false
--->


<!--- SUSPEND OUTPUT TO ELIMINATE WHITESPACE --->
<CFSETTING ENABLECFOUTPUTONLY="YES">

<!--- TAG PARAMETERS --->
<CFPARAM NAME="Attributes.Name">
<CFPARAM NAME="Attributes.Name2">
<CFPARAM NAME="Attributes.Query1" DEFAULT="">
<CFPARAM NAME="Attributes.Query2" DEFAULT="">
<CFPARAM NAME="Attributes.Value" DEFAULT="column1">
<CFPARAM NAME="Attributes.Value2" DEFAULT="#Attributes.Value#">
<CFPARAM NAME="Attributes.Display" DEFAULT="column2">
<CFPARAM NAME="Attributes.Display2" DEFAULT="#Attributes.Display#">
<CFPARAM NAME="Attributes.Caption" DEFAULT="">
<CFPARAM NAME="Attributes.Caption2" DEFAULT="">
<CFPARAM NAME="Attributes.Size" DEFAULT="10">
<CFPARAM NAME="Attributes.Size2" DEFAULT="#Attributes.Size#">
<CFPARAM NAME="Attributes.Width" DEFAULT="">
<CFPARAM NAME="Attributes.Width2" DEFAULT="#Attributes.Width#">
<CFPARAM NAME="Attributes.ForceWidth" DEFAULT="">
<CFPARAM NAME="Attributes.ForceWidth2" DEFAULT="#Attributes.ForceWidth#">
<CFPARAM NAME="Attributes.ForceWidthChar" DEFAULT="&nbsp;">
<CFPARAM NAME="Attributes.ForceWidthChar2" DEFAULT="#Attributes.ForceWidthChar#">
<CFPARAM NAME="Attributes.required" DEFAULT="0">
<CFPARAM NAME="Attributes.label" DEFAULT="">
<CFPARAM NAME="Attributes.Up" DEFAULT="Yes">
<CFPARAM NAME="Attributes.Down" DEFAULT="Yes">
<CFPARAM NAME="Attributes.Left" DEFAULT="Yes">
<CFPARAM NAME="Attributes.Right" DEFAULT="Yes">
<CFPARAM NAME="Attributes.UpText" DEFAULT="Up">
<CFPARAM NAME="Attributes.DownText" DEFAULT="Dn">
<CFPARAM NAME="Attributes.LeftText" DEFAULT="<<">
<CFPARAM NAME="Attributes.RightText" DEFAULT=">>">
<CFPARAM NAME="Attributes.FormName" DEFAULT="forms[0]">
<CFPARAM NAME="Attributes.VAlign" DEFAULT="MIDDLE">
<CFPARAM NAME="Attributes.onChange" DEFAULT="">
<CFPARAM NAME="Attributes.bindFunction" TYPE="String" DEFAULT=""> <!--- NJH 2009/02/09 P-SNY047 - a bind function to use --->
<CFPARAM NAME="Attributes.filterQuery" DEFAULT=""> <!--- NJH 2009/02/09 P-SNY047 - the query to populate the filter dropdown which dropdown1 is bound to. --->
<CFPARAM NAME="Attributes.filterCurrentValue" DEFAULT=""> <!--- NJH 2009/02/10 P-SNY047 - the current value of the filter query --->
<CFPARAM NAME="Attributes.resetOnFilterChange" Type="boolean" DEFAULT="false"> <!--- NJH 2009/02/10 P-SNY047 - removes all values from the 'selected' values dropdown. Set this if wanting to select only one type of filter value.--->
<CFPARAM NAME="Attributes.disabled" DEFAULT="false">  <!--- NOTE  WAB 2011/10/04 added this but discovered that CFSELECT does not seem to support disabled on a multiselect box--->
<CFPARAM NAME="Attributes.doSort" DEFAULT="#iif(Attributes.Up,false,true)#">
<CFPARAM NAME="Attributes.Title" DEFAULT="#Attributes.Display#">
<CFPARAM NAME="Attributes.Title2" DEFAULT="#Attributes.title#">
<CFPARAM NAME="Attributes.readonly" DEFAULT="false">
<cfparam name="attributes.filterlabel" default="">
<cfparam name="attributes.class" default="">
<cfparam name="attributes.useJqueryMultiSelect" default="true">

<!--- WAB 2016-02-02 If attributes.up true (ie the up and down buttons are specified, so the jQueryMultiSelect cannot be used, regardless of setting of useJqueryMultiSelect --->
<cfif attributes.up>
	<cfset attributes.useJqueryMultiSelect = false>
</cfif>

<cfif not request.relaycurrentuser.isinternal>
	<cfset attributes.class = attributes.class & " form-control">
</cfif>

<CFSET Name1 = Attributes.Name>
<CFSET Name2 = Attributes.Name2&"_select">

<CFPARAM NAME="Attributes.RightOnClick" DEFAULT="moveFromTo(this,'#name1#','#name2#',#Attributes.doSort#);">
<CFPARAM NAME="Attributes.LeftOnClick" DEFAULT="moveFromTo(this,'#name2#','#name1#',#Attributes.doSort#);">

<cfif not isQuery(attributes.query1)>
	<cfset query1 = Caller[Attributes.Query1]>
<cfelse>
	<cfset query1 = attributes.query1>
</cfif>
<cfif not isQuery(attributes.query1)>
	<cfset query2 = Caller[Attributes.Query2]>
<cfelse>
	<cfset query2 = attributes.query2>
</cfif>


<!---
WAB 2016-01-21
Conversion to use multiselect plugin rather than twoselectscombo
Join the two queries back together and pass to <cf_select>/com.relayForms.validvalueDisplay()

Does not work if
a) there is a filterQuery ans bindFunction
b) up/down buttons

Ideally I wanted to change all the places which called twoselectscombo to call com.relayForms.validvalueDisplay() or <cf_select> directly
Since these places will have gone to great lengths to create two queries, and it seems a bit pointless to just join them back together here!
However in the end needed the quick and dirty solution


 --->
<cfif NOT (attributes.bindFunction neq "" and isQuery(attributes.filterQuery)) and NOT attributes.up and attributes.useJqueryMultiSelect>
	<cfquery name="combined" dbtype="query">
	select [#attributes.value#], [#attributes.display#], 1 as isSelected from query1
		Union
	select [#attributes.value2#], [#attributes.display2#], 0 as isSelected from query2
	</cfquery>

	<cfscript>
		attributes.name = attributes.name2;
		structEach (attributes, function (key,value) {
			if (key contains "2") {
				structDelete (attributes,key);
			}

		});
	</cfscript>
	<cf_select query = #combined# displayAs="multiselect" attributeCollection = #attributes#>

<cfelse>


	<CFOUTPUT>
	<cf_includejavascriptonce template = "/javascript/twoselectscombo.js">

	<cfif not attributes.readonly>
		<div class="validation-group">
		<TABLE>
		<!--- NJH 2009/02/09 - show the filter dropdown  if the bindfunction has been passed through and if the filterQuery is not empty.
			This bit of javascript will select all values in the 'selectedValue' select box as the filter is changed.
			had problem with binding to the right hand select box because any click on it caused a reload.
			Ended up getting round it by binding to a hidden field and updating the hidden field when required.
			--->
		<cfif attributes.bindFunction neq "" and isQuery(attributes.filterQuery)>
			<TR>
				<TD colspan="2">
					<cfoutput>
						<script>

							var currentFilterIndex;

							function SelectAllItems(obj) {
								for (i=0;i<obj.length;i++) {
									if (obj[i].value != '') {
										obj[i].selected = true;
									}
								}
							}

							function RemoveAllItems(thisObj,obj) {

								if (obj.options.length > 0 && obj.options[0].value != '') {
									if (confirm ('Changing the filter will remove the items currently selected. Do you wish to continue?')) {
										obj.options.length = 0; // removes the options
										obj.innerHTML = ''; // removed the objGroups

										// set an empty option
										var optn = document.createElement("OPTION");
										optn.text = '';
										optn.value = '';
										obj.options.add(optn);
									} else {
										thisObj.selectedIndex = currentFilterIndex;
									}
								}

							}

							// this puts the value of the 'selected' dropdown into a hidden variable to be used by the binding function
							function putValuesInHiddenField (obj) {
								var result = '';
								for (i=0;i<obj.length;i++){
									if (obj[i].value != '') {
										result = result + ((result=='')?'':',') + obj[i].value
									}
								}
								document.getElementById(obj.name + '_hidden'  ).value = result
							}

						</script>
					</cfoutput>

					<cfif not attributes.resetOnFilterChange>
						<cfset onChangeFunction = "putValuesInHiddenField(document.getElementById('#Attributes.Name2#'))">
					<cfelse>
						<cfset onChangeFunction = "RemoveAllItems(this,document.getElementById('#Attributes.Name2#'))">
					</cfif>

					<cfajaximport>

					<cf_select name="#Attributes.Name2#_filter" query=#attributes.filterQuery# value="value" display="display" onClick="currentFilterIndex=this.selectedIndex;" selected="#Attributes.filterCurrentValue#" visible="Yes" enabled="Yes" onchange="#onChangeFunction#" pluginOptions="#{'multipleSelect' = false}#" ></cf_select> #attributes.filterLabel#

					<!--- couldn't work out how to do a valuelist on a column name which was a variable--->
					<CFQUERY NAME="TEMP" DBTYPE="QUERY">
					SELECT #ATTRIBUTES.value# AS theVALUE FROM QUERY1
					</CFQUERY>
					<CF_INPUT TYPE="HIDDEN" ID= "#Attributes.Name2#_hidden" value="#valueList(temp.theVALUE)#">
				</TD>
			</TR>
		</cfif>

			<TR VALIGN="#Attributes.VAlign#">
				<TD>

				<CFIF Trim(Attributes.Caption) is not "">
				#application.com.security.sanitiseHTML(Attributes.Caption)#<BR>
				</CFIF>

				<!--- if a bind function has been passed through and if we have a query to populate the select box that we're binding to. --->
				<cfset additionalAttributesCollection = structNew()>
				<cfset additionalAttributesCollection.multiple="yes">
				<cfset additionalAttributesCollection.ID=Name1>
				<cfset additionalAttributesCollection.NAME=Name1>
				<cfset additionalAttributesCollection.SIZE=attributes.Size>
				<cfif Val(Attributes.Width) is not 0>
					<cfif isNumeric(Attributes.Width)>
						<cfset attributes.Width = "#Attributes.Width#px;">
					</cfif>
					<cfset additionalAttributesCollection.STYLE="width:#Attributes.Width#;">
				</cfif>
				<cfset additionalAttributesCollection.onChange="#Attributes.onChange#">
				<cfset additionalAttributesCollection.ONDBLCLICK = "#Attributes.RightOnClick#">
				<cfset additionalAttributesCollection.value = "#Attributes.Value2#">
				<cfset additionalAttributesCollection.display="#Attributes.Display2#">
				<cfset additionalAttributesCollection.displayAs="multiselect">
				<cfif listFindNoCase(query2.columnList,"optGroup")>
					<cfset additionalAttributesCollection.group = "optGroup">
				</cfif>
				<cfif attributes.bindFunction neq "" and isQuery(attributes.filterQuery)>
					<cfset additionalAttributesCollection.bind="#attributes.bindFunction#">
					<cfset additionalAttributesCollection.bindonload=true>
				<cfelse>
					<cfset additionalAttributesCollection.query = query2>
				</cfif>
				<cfif attributes.disabled><!--- WAB 2011/10/04 added support for disabled, but discovered that CFSELECT didn't support it along with multiple, so have got around problem by using passthrough --->
					<cfset additionalAttributesCollection.disabled =  true>
					<cfset additionalAttributesCollection.passthrough =  " disabled">
				</cfif>
				<cfset additionalAttributesCollection.title = attributes.title>
				<cfset additionalAttributesCollection.readonly =  attributes.readonly>
				<cfset additionalAttributesCollection.class =  attributes.class>
				<cfset additionalAttributesCollection["data-twoSelects"] = true> <!--- NJH JIRA PROD2016-2299 - identify this select as a two-selects. Used when binding...(relayForms.createJavascriptBind  --->


				<CF_SELECT attributeCollection = #additionalAttributesCollection# queryPosition="before" pluginOptions="#{'multipleSelect' = false}#">
				</CF_SELECT>

				</TD>
				<TD>

			<cfif not attributes.disabled and not attributes.readonly>
				<CFIF Attributes.Right is "Yes">
						<CF_INPUT TYPE="BUTTON" class="smallButton btn btn-default" VALUE="#Attributes.RightText#" onClick="#Attributes.RightOnClick#"><BR>
				</CFIF>

		  		<CFIF Attributes.Left is "Yes">
						<CF_INPUT TYPE="BUTTON" class="smallButton btn btn-default" VALUE="#Attributes.LeftText#" onClick="#Attributes.LeftOnClick#">
				</CFIF>
			</cfif>

				</TD>
				<TD>

				<CFIF Trim(Attributes.Caption2) is not "">
					#application.com.security.sanitiseHTML(Attributes.Caption2)#<BR>
				</CFIF>

				<!--- NJH 2009/02/09 P-SNY047 added ID and group; changed to cfselect to make use of the group attribute. --->
				<cfset additionalAttributesCollection = structNew()>
				<cfset additionalAttributesCollection.multiple="yes">
				<cfset additionalAttributesCollection.ID=Name2>
				<cfset additionalAttributesCollection.NAME=Name2>
				<cfset additionalAttributesCollection.SIZE=Attributes.Size2>
				<cfif Val(Attributes.Width2) is not 0>
					<cfif isNumeric(Attributes.Width2)>
						<cfset attributes.width2 = "#Attributes.Width2#px;">
					</cfif>
					<cfset additionalAttributesCollection.STYLE="width:#Attributes.Width2#;">
				</cfif>
				<cfset additionalAttributesCollection.onChange=Attributes.onChange>
				<cfset additionalAttributesCollection.ONDBLCLICK = "#Attributes.LeftOnClick#">
				<cfset additionalAttributesCollection.query = query1>
				<cfset additionalAttributesCollection.value = Attributes.value2>
				<cfset additionalAttributesCollection.display=Attributes.display2>
				<cfset additionalAttributesCollection.displayAs="multiselect">
				<cfif listFindNoCase(query1.columnList,"optGroup")>
					<cfset additionalAttributesCollection.group = "optGroup">
				</cfif>
				<!---<cfset additionalAttributesCollection.required = attributes.required> --->
				<cfset additionalAttributesCollection.label = attributes.label>
				<!--- <cfset additionalAttributesCollection.Submitfunction = "SelectAllItems"> --->
				<cfif attributes.disabled><!--- WAB 2011/10/04  see comment on other select box --->
					<cfset additionalAttributesCollection.disabled =  true>
					<cfset additionalAttributesCollection.passthrough =  " disabled">
				</cfif>
				<cfset additionalAttributesCollection.title = attributes.title2>
				<cfset additionalAttributesCollection.readonly =  attributes.readonly>
				<cfset additionalAttributesCollection.class =  attributes.class>

				<CF_SELECT attributeCollection = #additionalAttributesCollection# queryPosition="before" pluginOptions="#{'multipleSelect' = false}#">
					<!--- <CFIF  Val(Attributes.ForceWidth2) is not 0>
						<OPTION VALUE="">#RepeatString(Attributes.ForceWidthChar2, Attributes.ForceWidth2)#
					</CFIF> --->
				</CF_SELECT>

				<cfquery name="getSelectedValues" dbtype="query">
					select [#attributes.value2#] as selectedValues from query1
				</cfquery>

				<cfset hiddenAttrCollection= {name=attributes.name2,value=valueList(getSelectedValues.selectedValues),required=attributes.required, "data-defaultValue" = valueList(getSelectedValues.selectedValues)}>
				<cf_input type="hidden" attributeCollection=#hiddenAttrCollection#>

					</TD>
					<TD>

				<cfif not attributes.disabled and not attributes.readonly>
		  			<CFIF Attributes.Up is "Yes">
						<CF_INPUT TYPE="BUTTON" class="smallButton btn btn-default" VALUE="#Attributes.UpText#" onClick="Move(this,'#name2#',-1);"><BR>
					</CFIF>

					  <CFIF Attributes.Up is "Yes">
					  	<CF_INPUT TYPE="BUTTON" class="smallButton btn btn-default" VALUE="#Attributes.DownText#" NAME="BtnDn" onClick="Move(this,'#name2#',1);">
					</CFIF>
			</cfif>

				</TD>

			</TR>

		</TABLE>
		</div>
	<cfelse>
		<cfloop query="query1">
			#query1[attributes.display][currentRow]# <BR>
		</cfloop>
	</cfif>

	</CFOUTPUT>
</cfif>

<!--- RESUME NORMAL OUTPUT --->
<CFSETTING ENABLECFOUTPUTONLY="NO">

