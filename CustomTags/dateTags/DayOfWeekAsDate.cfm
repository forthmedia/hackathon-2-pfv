<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	<CF_DayOfWeekAsDate>
	
	Author: Austin Lorenzen
		Systems Analyst, Short's Travel Management
		austin@shortstravel.com
	
	Date created: 2001-29-11
	
	Purpose: This tag was originally designed so that you can just ask for the date of a particular day of the week, when all you know is a date, and what day of the week you would like.
		Originally, this was used to cut out redundant code in an online internally developed payroll system (100% ColdFusion/Microsoft SQL Server 7-based, of course!), where I had to know
		what date Monday and Friday were for a week to calculate overtime.
	
	Inputs:
		Name			Object/data type			Description
		----			----------------			-----------
		Date			ColdFusion date object		The date which you already know
		DayOfWeek		Integer (1-7)				Number of the day of week you would like to know (1=Sunday, 7=Saturday, just like DayOfWeek in standard ColdFusion library)
	
	Outputs:
		Name			Object/data type			Description
		----			----------------			-----------
		DateAsDate		ColdFusion date object		Date of DayOfWeek for the week containing Date
			
	Example: I know a date of 2001-28-11, which is a Wednesday.  However, I would like to programatically find the Monday for that week.  Call the tag as shown below:
		<cfset dateIKnow = CreateDate(2001, 11, 28)>
		<cf_DayOfWeekAsDate Date = dateIKnow DayOfWeek = 2>
		<cfoutput>#DateFormat(DateAsDate, "m/d/yyyy"</cfoutput>
				DISPLAYS
			2001-26-11
		
		This returns 2001-26-11, which is the Monday of that week
--->		

<cfif #DayOfWeek(attributes.Date)# LTE (attributes.DayOfWeek - 1)>
	<cfset caller.DateAsDate = DateAdd("d", Abs(attributes.DayOfWeek - DayOfWeek(attributes.Date)), attributes.Date)>
<cfelse>
	<cfset caller.DateAsDate = DateAdd("d", -Abs(attributes.DayOfWeek - DayOfWeek(attributes.Date)), attributes.Date)>
</cfif>