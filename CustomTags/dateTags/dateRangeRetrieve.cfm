<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CF_DateRangeRetrieve

WAB 2005-10-11

retrieves the values submitted from dateRangeSelectBox.cfm

I have just taken the code which is suggested in dateRangeSelectBox and put it in here 'cause it is starting to appear in more than one place

WAB 2006-03-13 added parameter to allow return variable to be a structure
 --->
 
<CFPARAM NAME="ATTRIBUTES.DateRange">
<CFPARAM NAME="ATTRIBUTES.returnStruct" default = "">

<cfif ATTRIBUTES.returnStruct is not "">
	<cfset caller[ATTRIBUTES.returnStruct] = structNew()>
	<cfset returnVar= caller[ATTRIBUTES.returnStruct]>
<cfelse>
	<cfset returnVar= caller>
</cfif>
	
 
	<CFSET returnVar.StartDate=#CREATEODBCDATE(ListGetAt(ATTRIBUTES.DateRange,1))#>
	<CFSET returnVar.EndDate=#CREATEODBCDATE(ListGetAt(ATTRIBUTES.DateRange,2))#>
	<CFSET returnVar.Range=#ListGetAt(ATTRIBUTES.DateRange,3)#>

 