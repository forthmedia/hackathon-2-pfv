<!--- �Relayware. All Rights Reserved 2014 --->
<!--- CF_CalcWeek v1.0 was written by Cory Aiken of CF_WebApps (http://www.cfwebapps.com)
	
	  			 All Rights Reserved
	
	This is freeware, and NO warranties are expressed or implied.

	Code is PUBLIC DOMAIN, free to use and modify as desired.  Neither the Author,
	nor Allaire Corp., or any other agencies or companies or families or
	friends or pets affiliated with the Author or Allaire Corp., are responsible
	for any use or misuse of this code, nor are they liable for any Bad Stuff that
	may happen by one using it. No technical support will be provided
	by the Author or Allaire Corp.

Description:

	This tag calculates the date of the starting day and ending day 
	of the week based on a date you pass.  You can set any day of the week 
	as the starting day and the same with the ending day based on each day's 
	ordinal number. (eg. Sunday =1, Monday =2) If you set the beginning day 
	of the week later than the ending day then the end day is advanced one week.  
	(For example: If I set the start of a week to be Friday and the end of the 
	week to be Monday then the Monday returned will be the following Monday, not 
	a date earlier than the start.) Also if the start day is later than the end 
	day and the date to be checked does not fall between the two ordinal days, the 
	values returned will be for the next "week period."  Default BeginDay and EndDay
	is Sunday (1) and Saturday (7).

USE: To call this tag

<CF_CalcWeek
	CheckDate="#Date#" 
	BeginDay="#OrdinalBeginDay#" 
	EndDay="#OrdinalEndDay">
	
Values Returned: #StartWeek# and #EndWeek#
CheckDate is Required, BeginDay and EndDay are optional.
Cold Fusion date functions should be used to correctly display these values
If the required attributes are not passed it returns #ErrorCode# of 1, otherwise it is 0.
--->
<CFPARAM NAME="Attributes.BeginDay" DEFAULT="1">
<CFPARAM NAME="Attributes.EndDay" DEFAULT="7">
<CFPARAM name="Caller.ErrorCode" default="0">


<CFIF #Attributes.CheckDate# NEQ "">	

	<!--- Determine what day (ordinal number) of the week is submitted --->
	<CFSET DayOrdinal = #DayofWeek(Attributes.CheckDate)#>
	<!--- Set the StartWeek and EndWeek variables based on DayOrdinal --->
	<CFIF #Attributes.BeginDay# GT #Attributes.EndDay#>
		<cfif #DayofWeek(Attributes.CheckDate)# LTE #Attributes.EndDay#>
			<CFSET Attributes.BeginDay = #Attributes.BeginDay# -7>
			<cfset Attributes.EndDay = #Attributes.EndDay# - 7>
		</cfif>
		<cfset Attributes.EndDay = #Attributes.EndDay# + 7>
	</cfif>


	<CFSET Caller.StartWeek = ((#Attributes.CheckDate#) - (#DayOrdinal#-#Attributes.BeginDay#))>
	<CFSET Caller.EndWeek = ((#Attributes.CheckDate#) + (#Attributes.EndDay#-#DayOrdinal#))>
	  
<cfelse>
  <CFSET ErrorCode = 1>
</cfif>     