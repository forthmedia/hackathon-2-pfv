<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NAME:           CF_DateRange
FILE:			DateRange.cfm
CREATED:		1999-22-04
LAST MODIFIED:	1999-22-04
VERSION:	    1.0
AUTHOR:         Ed Hodder (ehodder@TheArtBoard.com)
DESCRIPTION:    This tag returns a start date and end date in ODBC format
			    for queries. Supports current, previous, and numbered
			    months, quarters, or years. Returns StartDate and EndDate.
				Now also retuns #range# which is a name for the range as used in cf_dateRangeSelectBox
Usage:			<cf_DateRange 
				Type="[month/quarter]"
				Value="[current/previous/{1-12 month number, 1-4 quarter number}]"
				Year="[current/previous/{any year}]">	
				With no attributes the tag return StartDate equal to the first day 
				of current year	and EndDate returns last day of current year. For
				the date range of a particular year do not use the Type and Value
				attributes.

2005-10-26 	WAB: Modified so that also returns variable "range" which is compatible with the range used in cf_dateRangeSelectBox
			needed to do this so that when cf_dateRangeSelectBox selects the correct item when first used
2006-03-13		WAB: added type PreviousDays for a date range previous to (and including) Now()				
			WAB 2006-03-13 added parameter to allow return variable to be a structure
--->

 
<CFPARAM NAME="ATTRIBUTES.returnStruct" default = "">


<cfif IsDefined("attributes.year")>
	<cfif IsNumeric("#attributes.year#")>
		<cfset YearValue=attributes.Year>
	<cfelseif LCase("#attributes.Year#") is "previous">
		<cfset YearValue=Year("#Now()#")-1>
	<cfelseif LCase(attributes.Year) is "current">
		<cfset YearValue=Year("#Now()#")>
	<cfelse>
		<cfset YearValue=Year("#Now()#")>
	</cfif>
<!--- <cfelseif attributes.year is "">
	<cfset YearValue=Year("#Now()#")>
 --->
<cfelse>
	<cfset YearValue=Year("#Now()#")>
</cfif>

<cfif not IsDefined("attributes.Type") and not IsDefined("attributes.Value")>
	<cfset FirstDay="01/01/" & YearValue>
	<cfset LastDay="12/31/" & YearValue>
	<cfset Range = "All " & YearValue>
<cfelseif attributes.type is "">
	<cfset FirstDay="01/01/" & YearValue>
	<cfset LastDay="12/31/" & YearValue>
	<cfset Range = "All " & YearValue>
<cfelse>
	<cfif LCase(attributes.type) is "month">
		<cfif not IsDefined("attributes.Value")>
			<cfset Month=Month("#Now()#")>
		<cfelse>
			<cfif IsNumeric("#attributes.Value#")>
				<cfset Month=attributes.Value>
			<cfelseif LCase(attributes.Value) is "previous">
			<cfset Month=Month("#Now()#") - 1>
				<cfif Month is 0>
					<cfset Month=12>
					<cfset YearValue=YearValue-1>
				</cfif>
			<cfelseif LCase(attributes.Value) is "current">
				<cfset Month=Month("#Now()#")>
			<cfelse>
				<cfset Month=Month("#Now()#")>
			</cfif>
		</cfif>
		<cfset FirstDay=Month & "/01/" & YearValue>
		<cfset LastDay=Month & "/" & DaysInMonth("#FirstDay#") & "/" & YearValue>
		<cfset Range = dateFormat(Month & "/01/00","mmm") & " " & YearValue>
	<cfelseif LCase(attributes.Type) is "quarter">
		
		<cfif not IsDefined("attributes.Value")>
			<cfset ThisQuarter=Quarter("#Now()#")>
		<cfelse>
			<cfif LCase(attributes.Value) is "previous">
				<cfset ThisQuarter=Quarter("#Now()#") - 1>
			<cfelseif IsNumeric("#attributes.Value#")>
				<cfset ThisQuarter=attributes.Value>
			<cfelseif LCase(attributes.Value) is "current">
				<cfset ThisQuarter=Quarter("#Now()#")>
			</cfif>
		</cfif>
		
	<cfelseif LCase(attributes.Type) is "previousdays">
		<cfset LastDay=dateadd("d",now(),1)>
		<cfset FirstDay=dateadd("d",now(),-attributes.value) >
		<cfset Range = "previousDays#attributes.value#">
	</cfif>
	<cfif IsDefined("thisquarter")>
		<cfif ThisQuarter LT 1>
			<cfset YearValue=YearValue - 1>
			<cfset FirstDay="10/01/" & YearValue>
			<cfset LastDay="12/31/" & YearValue>
			<cfset Range = "4Q " & YearValue>
		<cfelseif ThisQuarter LT 2>
			<cfset FirstDay="01/01/" & YearValue>
			<cfset LastDay="03/31/" & YearValue>
			<cfset Range = "1Q " & YearValue>
		<cfelseif ThisQuarter LT 3>
			<cfset FirstDay="04/01/" & YearValue>
			<cfset LastDay="06/30/" & YearValue>
			<cfset Range = "2Q " & YearValue>
		<cfelseif ThisQuarter LT 4>
			<cfset FirstDay="07/01/" & YearValue>
			<cfset LastDay="09/30/" & YearValue>
			<cfset Range = "3Q " & YearValue>
		<cfelseif ThisQuarter LT 5>
			<cfset FirstDay="10/01/" & YearValue>
			<cfset LastDay="12/31/" & YearValue>
			<cfset Range = "4Q " & YearValue>
		</cfif>
	</cfif>

</cfif>


<cfif ATTRIBUTES.returnStruct is not "">
	<cfset caller[ATTRIBUTES.returnStruct] = structNew()>
	<cfset returnVar= caller[ATTRIBUTES.returnStruct]>
<cfelse>
	<cfset returnVar= caller>
</cfif>

<cfset returnVar.StartDate=CreateODBCDate(ParseDateTime(FirstDay))>
<cfset returnVar.EndDate=CreateODBCDate(ParseDateTime(LastDay))>
<cfset returnVar.Range=Range>
