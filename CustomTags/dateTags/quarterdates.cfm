<!--- �Relayware. All Rights Reserved 2014 --->
<!--- This tag was designed by Kenneth A. Thomann --->

<!--- Tag: Quarter Dates --->

<!---Checks and sets the tag attributes---------------->
<CFPARAM NAME="Attributes.date" DEFAULT="#Now()#">
<CFPARAM NAME="Attributes.which" DEFAULT="current">

<cfif IsDate(#Attributes.date#) is "No">
<cfset #Attributes.date# = #Now()#>
</cfif>


<!---Figure out dates--->
<cfif #Attributes.which# is "current">
<cfset nextdate = CreateDate(Year(#Attributes.date#), 1, 1)>
<cfset nextquarter = 1>
<cfelse>
<cfset nextdate = #Attributes.date#>
<cfset nextquarter = Quarter(#variables.nextdate#)>
</cfif>


<cfloop index="f" From="1" To="4">

<cfset theyear = Year(#variables.nextdate#)>

<CFSWITCH EXPRESSION="#nextquarter#">
 <CFCASE VALUE="1">
  <cfset Caller.quarter1_start = #CreateDate(theyear, 1, 1)#>
  <cfset Caller.quarter1_end = DateAdd("m", 2, variables.nextdate)>
 </CFCASE>
 <CFCASE VALUE="2">
  <cfset Caller.quarter2_start = #CreateDate(theyear, 4, 1)#>
  <cfset Caller.quarter2_end = DateAdd("m", 2, variables.nextdate)>
 </CFCASE>
 <CFCASE VALUE="3">
  <cfset Caller.quarter3_start = #CreateDate(theyear, 7, 1)#>
  <cfset Caller.quarter3_end = DateAdd("m", 2, variables.nextdate)>
 </CFCASE>
 <CFCASE VALUE="4">
  <cfset Caller.quarter4_start = #CreateDate(theyear, 10, 1)#>
  <cfset Caller.quarter4_end = DateAdd("m", 2, variables.nextdate)>
 </CFCASE>
</CFSWITCH>

<cfif #Attributes.which# is "previous">
  <cfset nextdate = #DateAdd("m", -3, variables.nextdate)#>
  <cfset nextquarter = #Quarter(variables.nextdate)#>
<cfelse>
  <cfset nextdate = #DateAdd("m", 3, variables.nextdate)#>
  <cfset nextquarter = #Quarter(variables.nextdate)#>
</cfif>

</cfloop>