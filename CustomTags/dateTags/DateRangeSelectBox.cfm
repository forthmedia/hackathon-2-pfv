<!--- �Relayware. All Rights Reserved 2014 --->
<CFSETTING ENABLECFOUTPUTONLY="YES">
<!---
NAME:           CF_DateRangeSelectBox
FILE:			DateRangeSelectBox.cfm
CREATED:		1999-12-09
LAST MODIFIED:	1999-12-09
VERSION:	    1.0
AUTHOR:         David Perkins (dperkins@milehigh.net)
DESCRIPTION:    This tag builds a SELECT box with month, quarter, and year values for the present
				year and past years. The tag returns a start date and end date in ODBC format
			    for queries. This tag was written for use in a report generator.
				It let management pick a month/quarter/year to report on.<BR>
Usage:			<cf_DateRangeSelectBox BoxName="MyDateRangeBox"  ... optional parameters>
				***Required parameters ***
				BoxName="MyDateRangeBox" (the name you assign to the SELECT box)
				***Optional parameters ***
				StartYear="1999" [2000, 2001, etc] (if not present, we default to the current year)
				StopYearInPast="1995" [1994, 1990, etc] (if not present, we default to 1995)
				ShowQuarters="True" [Shows the 1Q 1995, 3Q 1997, etc list values] (if not present, we default to True)
				ShowYears="True" [Shows the All 1997, All 1996, etc list values] (if not present, we default to True)
				SelectedRange="May 1997" [March 1999, etc] (if not present, we make the Select list focus on the current month. If present but illegal, we let the Select list focus on the very first element )
				FirstOptionText="Jan 1998" (fill in the blank if you want a special date range for the first element in the Select box)
				FirstOptionValue="{ts '1998-01-01 00:00:00'},{ts '1998-02-01 00:00:00'},Jan 1998" (fill in the blank if you want a special date range for the first element in the Select box [make sure the value is in the proper 'list' format]

Errors:			Incoming values are not range-checked. Errors are not returned to the calling template.


Mods

WAB 2005-10-11 added a passThrough parameter
WAB 2006-03-13  changed ordering of the months so that they go backwards from 12 to 1
			 added attribute showFutureDates (default false) hides months in the future by default
			added Attributes.ShowPreviousDays to allow items such as "last 60 days"
--->

<!--- This tag forms a SELECT box with date ranges for common report criteria (Month, Quarter, Year) --->
<!--- A SELECT box is built with <option VALUE=xx> fields like below.
		<OPTION VALUE="{ts '1998-01-01 00:00:00'},{ts '1998-02-01 00:00:00'},Jan 1998">Jan 1998</OPTION>
		<OPTION VALUE="{ts '1998-02-01 00:00:00'},{ts '1998-03-01 00:00:00'},Feb 1998">Feb 1998</OPTION>
--->
<!--- You eventually retrieve the various values (startdate, stopdate, and text) by using code like the following:
		<CFSET StartDate=#ListGetAt(#DateRange#,1)#>
		<CFSET StopDate=#ListGetAt(#DateRange#,2)#>
		<CFSET Range=#ListGetAt(#DateRange#,3)#>
	  OR, if you want to query a database based on the date selected, you use
		<CFSET StartDate=#CREATEODBCDATE(ListGetAt(#DateRange#,1))#>
		<CFSET StopDate=#CREATEODBCDATE(ListGetAt(#DateRange#,2))#>
		<CFSET Range=#ListGetAt(#DateRange#,3)#>

	then, in the query that need the date criteria, use
		WHERE DateField BETWEEN #StartDate# AND #StopDate#
--->


<CFPARAM NAME="ATTRIBUTES.BoxName"><!--- The 'name' for the select box (required) --->
<CFPARAM NAME="Attributes.StartYear" DEFAULT="#Year(Now())#"><!--- Default to start the Select List with 'This Year' --->
<CFPARAM NAME="Attributes.StopYearInPast" DEFAULT="#Year(dateAdd("yyyy","-1",Now()))#"><!--- Default to stop the list with 1995 --->
<CFPARAM NAME="Attributes.ShowQuarters" DEFAULT="True"><!--- Default to Show the 'Quarters' (1Q 1999, 2Q 1999) --->
<CFPARAM NAME="Attributes.ShowYears" DEFAULT="True"><!--- Default to Show the 'Years' (All 1999, All 1998) --->
<CFPARAM NAME="Attributes.ShowPreviousDays" DEFAULT=""><!--- add list of numbers eg  30,60,90   to give Last 30 Days, Last 60 Days--->
<CFPARAM NAME="Attributes.SelectedRange" DEFAULT=""><!--- Default to 'select' the current month/year (otherwise, it should be the same as the 'range' parameter feeds back) --->
<CFPARAM NAME="Attributes.FirstOptionText" DEFAULT=""><!--- Default for the First Option in the select box --->
<CFPARAM NAME="Attributes.FirstOptionValue" DEFAULT="">
<CFPARAM NAME="Attributes.passThrough" DEFAULT="">  <!--- added by WAB --->
<CFPARAM NAME="Attributes.showFutureDates" DEFAULT="false">

<cfset yearNow = year(now())>
<cfset monthNow = month(now())>


<CFOUTPUT><SELECT NAME="#ATTRIBUTES.BoxName#" #attributes.passthrough# class="form-control"></CFOUTPUT>
	<CFIF Attributes.FirstOptionText IS NOT "">
	<CFOUTPUT><OPTION VALUE="#Attributes.FirstOptionValue#">#htmleditformat(Attributes.FirstOptionText)#</OPTION></CFOUTPUT>
	</CFIF>

	<cfif attributes.ShowPreviousDays is not "">
		<cfloop index="days" list = "#attributes.ShowPreviousDays#">
			<CFSET tRange="previousDays#days#">
			<CFOUTPUT>
			<OPTION <CFIF tRange IS Attributes.SelectedRange>SELECTED </CFIF> VALUE="#createODBCDate(now()-days)#,#createODBCDate(now()+1)#,#htmleditformat(tRange)#">Last #htmleditformat(days)# days</OPTION></CFOUTPUT>
		</cfloop>
	</cfif>

	<!--- Loop for all years in the list from StartYear to StopYear --->
	<!--- Note: The list 'runs backwards'...the Stop Year is in the past --->
	<CFLOOP INDEX="iYear" FROM="#ATTRIBUTES.StartYear#" TO="#ATTRIBUTES.StopYearInPast#" STEP="-1">
		<CFSET tRange="All " & "#iYear#">
		<CFIF Attributes.ShowYears IS TRUE>
			<CFOUTPUT><OPTION <CFIF tRange IS Attributes.SelectedRange>SELECTED </CFIF>VALUE="#DateAdd("YYYY","0","1/1/#htmleditformat(iYear)#")#,#DateAdd("YYYY","1","1/1/#htmleditformat(iYear)#")#,#htmleditformat(tRange)#">#htmleditformat(tRange)#</OPTION>
			</CFOUTPUT>
		</CFIF>
		<!--- Code to populate a Select box with Month/Quarter/Year values (useful for DATE RANGES in a Report) --->
		<CFLOOP INDEX="iMonth" FROM="12" TO="1" step =-1>
			<CFSET bMonth=iMonth-1>
			<CFIF iMonth MOD 3 IS 0 and ( Attributes.showFutureDates or iYear lt yearNow or (iYear eq yearNow and (iMonth-1)\3  lte (monthNow-1)\3))>  <!--- note \ does an integer division - this should get the current quarter outputted --->
				<CFIF Attributes.ShowQuarters IS TRUE>
					<!--- Form and insert the QUARTER number into the listbox (remember, we only execute this code on months 3,6,9, and 12) --->
					<CFSET iQtr=iMonth\3>
					<CFSET bQtr=iQtr-1>
					<CFSET tRange = #LISTGETAT("1Q #iYear#,2Q #iYear#,3Q #iYear#,4Q #iYear#","#iqtr#")#>
					<CFOUTPUT><OPTION <CFIF tRange IS Attributes.SelectedRange>SELECTED </CFIF>VALUE="#DateAdd("Q","#bQtr#","1/1/#iYear#")#,#DateAdd("Q","#iQtr#","1/1/#iYear#")#,#tRange#">#htmleditformat(tRange)#</OPTION>
					</CFOUTPUT>
				</CFIF>
			</CFIF>
				<!--- Form the list-box text and corresponding value for each month. --->
			<CFIF Attributes.showFutureDates or iYear lt yearNow or (iYear eq yearNow and iMonth  lte monthNow)>
				<CFSET tRange=#LISTGETAT("Jan #iYear#,Feb #iYear#,Mar #iYear#,Apr #iYear#,May #iYear#,Jun #iYear#,Jul #iYear#,Aug #iYear#,Sep #iYear#,Oct #iYear#,Nov #iYear#,Dec #iYear#","#iMonth#")#>
				<CFOUTPUT><OPTION <CFIF tRange IS Attributes.SelectedRange>SELECTED <CFELSEIF (Attributes.SelectedRange IS "") AND (iMonth IS Month(NOW()) AND (iYear IS Year(NOW())))> SELECTED</CFIF> VALUE="#DateAdd("M","#bMonth#","1/1/#iYear#")#,#DateAdd("M","#iMonth#","1/1/#iYear#")#,#tRange#">#htmleditformat(tRange)#</OPTION>
				</CFOUTPUT>
			</cfif>
		</CFLOOP>
	</CFLOOP>
	<CFSETTING ENABLECFOUTPUTONLY="NO">
</SELECT>
