<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		addShowActionLink.cfm
Author:			SWJ
Date created:	2002-05-22

	Objective - to provide a listing screen with a show/add action solumn content
		
	Syntax	  -	
	
				<CF_addShowActionLink
					EntityType  = organisation
					entityID 	= #organisationID#
					helpText	= #organisationName#
					testVal		= #actions#
					>
	
	Note that the parameters passed should mostly have pound signs around them

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2002-05-22	SWJ		First version

Enhancement still to do:



--->

<CFSETTING ENABLECFOUTPUTONLY="Yes">

<CF_checkMandatoryParams 
	callingTemplate="addShowActions.cfm" 
	relayParam="EntityType,entityID,helpText,testVal">

<CFPARAM NAME="attributes.class" TYPE="string" DEFAULT="smallLink">
<CFPARAM NAME="attributes.entityType" TYPE="string">
<CFPARAM NAME="attributes.entityID" TYPE="numeric">
<CFPARAM NAME="attributes.helpText" TYPE="string">
<CFPARAM NAME="attributes.testVal" TYPE="numeric">

<CFOUTPUT>
	<CFIF attributes.testVal eq 0>
		<A HREF="javaScript:addActionRecord('#attributes.entityType#',#attributes.entityID#)" TITLE="Add Action for #attributes.helpText#" CLASS="#attributes.class#"><IMG SRC="/IMAGES/MISC/IconFlagGrey.gif" BORDER="0"></a><BR>
	<cfelse>
		<A href="javaScript:showActionRecord('#attributes.entityType#',#attributes.entityID#)" TITLE="Show Actions for #attributes.helpText#" CLASS="#attributes.class#"><IMG SRC="/IMAGES/MISC/IconFlag.gif" BORDER="0"></a>
	</CFIF>
	
</CFOUTPUT>

<CFSETTING ENABLECFOUTPUTONLY="No">
