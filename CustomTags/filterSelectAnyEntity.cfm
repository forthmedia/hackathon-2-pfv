<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			filterSelectAnyEntity.cfm
Author:				YMA
Date started:		01/08/2014

Description:		Pass in an entityType and produce an interface to select any filter.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2015-01-23			ACPK		CORE-1019 Removed redundant link

Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:

 --->

<!--- details about the entity being filtered E.g. if a activity score rule is being filtered based on the person who obtained the activity the entityTypeID(filteredByTypeID) is person and the relatedEntityID(filteredEntityTypeID) is activityScoreRule --->
<CFPARAM NAME="attributes.entityTypeID" TYPE="numeric">
<CFPARAM NAME="attributes.relatedEntityID" TYPE="numeric">
<CFPARAM NAME="attributes.relatedEntityTypeID" TYPE="numeric">
<cfset attributes.tablename = application.entityType[attributes.entityTypeID].TABLENAME>

<!--- get all the fields available to the entity --->
<cfset oEntityFields = application.com.filterSelectAnyEntity.getCurrentEntityFields(entityTypeID=attributes.entityTypeID,tablename=attributes.tablename)>

<cfoutput>
<script>
function loadOperator(field) {
		jQuery('##operatorContainer').html("");
		jQuery('##comparisonContainer').html("");

		if(field != ""){
			jQuery.ajax(
		    	{
		        	type: "GET",
	                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=loadOperator&returnFormat=plain',
		        	data:{'field':field,'entityTypeID':#attributes.entityTypeID#},
		        	dataType:'text',
		        	success: function(data) {jQuery('##operatorContainer').html(data);}
		    	});
		}
	}
function loadComparison(field,operator) {
		jQuery('##comparisonContainer').html("");

		if(field != ""){
			jQuery.ajax(
		    	{
		        	type: "GET",
	                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=loadComparison&returnFormat=plain',
		        	data:{'field':field,'operator':operator,'entityTypeID':#attributes.entityTypeID#},
		        	dataType:'text',
		        	success: function(data) {jQuery('##comparisonContainer').html(data);}
		    	});
		}
	}
function addFilters(field,operator) {

		var comparisonB = "";

		 var comparisonB = jQuery("##comparisonB :selected").map(function () {
						        return jQuery(this).val();
						    }).get().join(',');

		if(comparisonB.length == 0){ comparisonB = jQuery('##comparisonB').val()}

		if (typeof comparisonB !== "undefined"){
			comparisonB = comparisonB.toString();
		}else{
			comparisonB = "";
		}

		jQuery.ajax(
	    	{
	        	type: "GET",
                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=saveFilter&returnFormat=plain',
	        	data:{	'filterField':jQuery('##filterField').val(),
	        			'filterEntityTypeID':#attributes.entityTypeID#,
	        			'operatorDisplay':jQuery('##operator :selected').text(),
	        			'operator':jQuery('##operator').val(),
	        			'comparisonB': comparisonB,
	        			'comparisonC':jQuery('##comparisonC').val(),
	        			'RELATEDENTITYID': #attributes.relatedEntityID#,
	        			'RELATEDENTITYTYPEID': #attributes.relatedEntityTypeID#,},
	        	dataType:'text',
	        	success: function(data) {jQuery('##filterEditor').prev('tr').after(data);}
	    	});

		jQuery('##filterField').val("");
		jQuery('##operatorContainer').html("");
		jQuery('##comparisonContainer').html("");

	}
function deleteFilter(filterCriteriaID) {
		jQuery.ajax(
	    	{
	        	type: "GET",
                url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=filterSelectAnyEntityWS&methodName=deleteFilter&returnFormat=json',
	        	data:{	'filterCriteriaID':filterCriteriaID},
	        	dataType:'json',
	        	success: function(data) {jQuery('tr ##' + filterCriteriaID).remove();}
	    	});

	}
</script>
<div id="filterContainer">
	<cfset aStructSorted=structKeyArray(oEntityFields)>
	<cfset arraySort(aStructSorted,'textNoCase')>

	<form name="filterForm" id="filterForm">
	<table id="filterTable" class="table table-hover table-striped table-bordered">
		<!--- select field to filter on --->
		<tr>
			<th>Field</th><th>Operator</th><th>Comparison</th><th>Add Delete</th>
		</tr>
		<!--- get current filters --->
		<cfset currentFilters = application.com.filterSelectAnyEntity.getFilterCriteriaForEntity(relatedentityID=attributes.relatedEntityID,relatedEntityTypeID=attributes.relatedEntityTypeID)>
		<!--- output current filters --->
		<cfloop query="currentFilters">
			<tr id="#filterCriteriaID#">
				<td>#field#</td>
				<td>#operator#</td>
				<td>#comparison#</td>
				<td><a href="##" onclick="deleteFilter(#filterCriteriaID#)" class="btn btn-primary">phr_delete</a></td>
			</tr>
		</cfloop>
		<!--- load filterEditor --->
		<tr id="filterEditor">
			<!--- field --->
			<td id="filterFieldContainer">
				<select id="filterField" name="filterField" onchange="loadOperator(this.value)">
					<option value="">phr_ext_selectavalue</option>
					<cfloop array="#aStructSorted#" index="i" >
						<option value="#i#">(#oEntityFields[i].FIELDDESCRIPTOR.FIELDTYPE#: #i#) #oEntityFields[i].FIELDDESCRIPTOR.NAME#</option>
					</cfloop>
				</select>
			</td>

			<!--- operator --->
			<td id="operatorContainer">

			</td>

			<!--- comparison --->
			<td id="comparisonContainer">

			</td>

			<!--- add --->
			<td id="comparisonAdd">
				<a href="##" onclick="addFilters()" class="btn btn-primary">phr_add</a>
			</td>
		</tr>
	</table>
	</form>
	<!--- 2015-01-23	ACPK	CORE-1019 Removed redundant link --->
	<!---<a href="##">Expression Editor</a>--->
</div>
</cfoutput>