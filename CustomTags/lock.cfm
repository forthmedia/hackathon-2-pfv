<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2012-09-18

A Custom Tag wrapper for Nat's Process lock functionality

Works in a similar way to CFLOCK, but locks across whole cluster
Also returns useful structure in caller.cf_lock
	cflock.timedOut      you can use instead of a cftry to tell if the lock was successful.  
	cflock.lockwait      tells you how long it took to obtain the lock (or not) and in the case of a timeout give.
	cflock.detail        in the case of a timeout gives you information about the current lock 


2012-11-?	WAB Various mods in area of metaData - any non standard attributes are thrown into a metadata structure
				Added code to allow locks with same name to be nested within the same request without the inner lock getting a timeout
2013-01-15 	WAB	Comms Improvements.  Altered to handle changed return type of getLock.lockdetail
2015-07-02	WAB Add support for onErrorEmailTo and maximumExpectedLockMinutes (note needs db change)
 --->

<cfif thisTag.HasEndTag is false>
	Needs End Tag
	<CF_ABORT>
</cfif>

<cfparam name="attributes.name" >
<cfparam name="attributes.exclusiveWith" default="">
<cfparam name="attributes.timeout" default=0>
<cfparam name="attributes.throwOnTimeout" default=true>
<cfparam name="attributes.abortOnTimeout" default=false>
<cfparam name="attributes.onerrorEmailTo" default="" >
<cfparam name="attributes.maximumExpectedLockMinutes" default = "120">

<!--- All non standard attributes are popped into a metadata structure --->
<cfset metadata = structCopy(attributes)>
<cfloop list="name,timeout,throwonTimeout,abortOnTimeout,exclusiveWith,abortMessage,maximumExpectedLockMinutes,onErrorEmailTo" index="key">
	<cfset structDelete(metadata,key)>
</cfloop>


<cfif thisTag.executionMode is "start">
	<cfset caller.cf_lock = {timedOut = false}>

	<!--- Test for this lock being nested inside a lock of the same name - which we allow --->
	<cfif structKeyExists (request,"openLocks") and listFindNoCase (request.openLocks,attributes.name)>
		<cfset thisTag.nested = true>
		<cfset caller.cf_lock.nested = true>
		<cfexit method="exitTemplate">
	</cfif>

	<cfparam name="throwOnTimeout" default="true">

	<cfset getLock = application.com.globalFunctions.setProcessLock(lockname = attributes.name, timeout = attributes.timeout,metadata=metadata, exclusiveWith=attributes.exclusiveWith, onerrorEmailTo = attributes.onerrorEmailTo, maximumExpectedLockMinutes = attributes.maximumExpectedLockMinutes)>
	<cfset caller.cf_lock = getLock>

	<cfif getLock.timedOut is true>
		<cfif arrayLen(getLock.lockdetail)>
			<cfset defaultMessage = "Process #getLock.lockdetail[1].lockname# locked by #getLock.lockdetail[1].createdby# at #getLock.lockdetail[1].created#">		
		<cfelse>
			<cfset defaultMessage = "Process locked">
		</cfif>

		<cfif attributes.throwOnTimeout>
			<cfthrow type="Lock_" detail="#defaultMessage#">
		<cfelseif attributes.abortOnTimeout>
			<cfparam name="attributes.abortMessage" default="#defaultMessage#">
			<cfoutput>#attributes.abortMessage#</cfoutput><cf_abort>
		<cfelse>
			<cfexit method="exittag"> <!--- will continue after end tag --->
		</cfif>
	</cfif>

<cfelse>

	<!--- NB when called within a cfc caller.cf_lock ought to be var'ed, if it isn't then we can get problems with caller.cf_lock being set by a another call to cf_lock (which will have timeod out) --->	
	<cfset caller.cf_lock = {timedOut = false}>

	<cfif structKeyExists (thisTag,"nested")>
		<cfexit method="exitTemplate">
	</cfif>

	<cfset application.com.globalFunctions.deleteProcessLock(lockname = attributes.name)>

</cfif>