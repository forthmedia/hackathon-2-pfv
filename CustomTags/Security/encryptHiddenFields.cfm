<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			encryptHiddenFields.cfm	
Author:				WAB
Date started:			2009/06/26
	
Description:			
This file was created to deal with form tampering
The idea is that the developer codes as normal using hidden form fields but wraps his form code with this tag.
It removes all hidden fields and replaces them with a single encrypted structure.

Underdevelopment


Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2009/09/14 WAB added a dontRemoveRegExp attribute and a excludeVariableNamesRegExp to make it easier to deal with encrypting all fields except a few defined items
2011/06    WAB  Added support for individual field encryption
Possible enhancements:


 --->
<cfparam name="attributes.variableNamesRegExp" default = "">
<cfparam name="attributes.excludeVariableNamesRegExp" default = "">
<cfparam name="attributes.remove" default = "true">
<cfparam name="attributes.removeRegExp" default = "">
<cfparam name="attributes.dontRemoveRegExp" default = "">
<cfparam name="attributes.useformstate" default = "true">

<cfset dontRemoveFieldsRegExp = "(_required)|(_date)|(_eurodate)">

 
 <cfif thisTag.executionMode is "end">

	<cfset encryptResult = application.com.security.encryptHiddenFields (
								contentString = thisTag.generatedContent, 
								argumentCollection = attributes
								)>

 	<cfset thistag.GeneratedContent = encryptResult.alteredContentString>
 </cfif>

