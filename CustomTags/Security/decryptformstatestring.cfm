<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			decryptFormState.cfm
Author:				WAB
Date started:			2009/06/24

Description:
Code to decrypt the hidden FormState field and reinstate the values in it as form variables

If run without any parameters it decrypts the formstate variables from both the URL and FORM scopes

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2010/02/01 WAB LID 2977  code to detect form resubmissions
2010/02/10 WAB  	Altered to allow multiple formStateStrings on the same page (just loop over the list)
2011/04/26 WAB		Added URL decoding of values passed in URL.formstate.  Problem appeared when we introduced request.query_string which encodes URL strings automatically; they are normally decoded automatically but this does not happen then the querystring is encrypted and decrypted - so had to add this to my code.  Unsure of knock on effects
2011/06		WAB		Added decryption of individually encrypted variables
2016/07/19 NJH		JIRA PROD2016-1444 - decrypt any values that were getting passed through as argumentCollection. Append argumentCollection to the original scope (form or url)
2016/10/18 WAB		PROD2016-1444 - (reopened).  Problems occurred if argumentCollection was a deep structure.  Only append items in argumentCollection to the scope if they are simple values

Possible enhancements:

Ability to stop if a discrepancy is detected

 --->




<cfparam name="attributes.formStateString" default = "">
<cfparam name="attributes.scope" default = form>
<cfparam name="attributes.otherscope" default = form>

<cfparam name="request.encryption.encryptedFields" default= #structNew()#> <!--- NJH 2009/07/17 P-FNL069 initialise the encrypted fields structure so that it always exists --->

<cfset warningMessage = "">

<!---2011/07/22 GCC - form scope does not exist for SOAP requests e.g. /WebServices/relayWS.cfc?wsdl --->
<cfif not isdefined("form")>
	<cfset form=structnew()>
</cfif>
<!--- look for singly encrypted variables --->
<cfset scopes = {form = form,  url = url}>

<cfloop item = "thisScopeName" collection = "#scopes#">
		<cfset thisScope = scopes[thisScopeName]>

	 	<!--- NJH 2016/07/19 - 	JIRA PROD2016-1444 - decrypt any values that were getting passed through as argumentCollection. 
	 							Append items in argumentCollection to the original scope (form or url)
	 			WAB 2016-10-18 	 Only append simpleValues, structures in URL/FORM scopes caused problems further down the security system
	 	 --->

		<cfif structKeyExists(thisScope,"argumentCollection")>
			<cfset argCollection = thisScope.argumentCollection>
			<cfif isJson(argCollection)> <!--- argument collection can be passed as json - deserialise before appending to struct --->
				<cfset argCollection = deserializeJson(argCollection)>
			</cfif>

			<cfif isStruct (argCollection)>
				<cfloop collection =#argCollection# item="key">
					<cfif isSimpleValue (argCollection[key]) and not structKeyExists (thisScope,key)>
						<cfset thisScope[key] = argCollection[key]>		
					</cfif>
				</cfloop>
			</cfif>
		</cfif>
		
		<cfloop item="variablename" collection = #thisScope#>
			<cfif application.com.security.isValueEncrypted (thisScope[variablename])>
				<cfset thisScope[variablename] = application.com.security.decryptVariableValue(name=variablename,value=thisScope[variablename])>
				<cfset request.encryption.encryptedFields[variablename] = thisScope[variablename]>
				<cfif thisScope[variablename] is "">
					<cfoutput>Form Expired</cfoutput><CF_ABORT>
				</cfif>
			</cfif>
		</cfloop>

</cfloop>


<!---
	this strange construction allows me to process both form and url scopes in the same loop

--->
<cfset scopes = arrayNew(1)>
<cfif attributes.formStateString is "" >
	<cfif structKeyExists(form,"formState")>
		<cfset tmpstruct = {scope=form,string=form['formstate'],otherscope= url,urldecode=false}>
		<cfset arrayAppend(scopes,tmpstruct )>
	</cfif>
	<cfif structKeyExists(url,"formState")>
		<cfset tmpstruct = {scope=url,string=url['formstate'],otherscope = form,urldecode=true}>
		<cfset arrayAppend(scopes,tmpstruct )>
	</cfif>
<cfelse>
		<cfset tmpstruct = {scope=attributes.scope,string=attributes.formstateString,urldecode=false}>
		<cfset arrayAppend(scopes,tmpstruct )>
</cfif>


<cfloop index = "i" from="1" TO = "#arrayLen(scopes)#" >
		<cfset formStateList = scopes[i].string >
		<cfset thisScope = scopes[i].scope >
		<cfset otherScope = scopes[i].otherscope>

	<!--- WAB 2010/02/10 added this loop --->
	<cfloop index="thisString"  list = "#formStateList#">
				<!--- NJH 2009/07/29 use the application.encryptionMethod passKey
				<cfset passKey = "g!?&23a-*2"> --->

				<cfset formStateResult = application.com.security.decryptFormStateVariable(formStateString=thisString)>

				<cfif not formStateResult.isok>
					<cfset application.com.errorHandler.recordRelayError_Warning (severity="WARNING", Type="Security Form Tampering",message="FormState Variable Corrupted: #cgi.script_name#")>
					<cfif application.testSite eq 2>
						<cfoutput>Tamper Alert ! <BR>
						"FormState" variable has been tampered  (This message only displayed on Dev Sites.  All other sites just Abort)
						</cfoutput>
					</cfif>
					<CF_ABORT>
				<cfelse>

					<cfset formStateStructure = formStateResult.result>
					<!--- when passed on the URL I send a lighter weight structure so test which type it is
						with the lighter weight structure we don't issue warnings
					--->
					<cfif structKeyExists  (formStateStructure,"fields") and isStruct(formStateStructure.fields)>
						<cfset hiddenfields = formStateStructure.fields>
						<cfset formStructure = true>
					<cfelse>
						<cfset hiddenfields = formStateStructure>
						<cfset formStructure = false>
					</cfif>

		<!--- 					<cfwddx action="WDDX2CFML" output="formStateStructure" input="#formStateWDDX#" >  --->

					<!--- Now loop though the structure reinstating values of form variables--->
					<cfloop item="fieldName" collection = #hiddenfields#>
						<!--- we could check for tampering of fields which weren't removed from the form, but since we are reinstating all the values anyway, it is not important --->
						<cfif formStructure and listfindnocase(formStateStructure.removed,fieldname) is 0>
							<!--- this is a field which was left on the form --->
							<cfif not structKeyExists (thisScope,fieldname)>
								<!--- field has disappeared, maybe a _required has been removed --->
								<cfset warningMessage = warningMessage & "Field #fieldName# has disappeared from the form being submitted<BR>">
							<cfelseif hiddenfields[fieldname] is not thisScope [fieldname]>
								<!---  has changed on the form, value will be overwritten but we will send an email incase it is a problem --->
								<cfset warningMessage = warningMessage & "Field #fieldName# changed from <BR>#hiddenfields[fieldname]#<BR> to<BR>#thisScope[fieldname]# <BR>">
							</cfif>
						</cfif>

						<!---
							check that this variable doesn't exist in the  other scope
							This prevents someone putting a variable on the URL and bypassing our security completely  (because we are not very good at scoping things)
						 --->

						<cfif structKeyExists (otherScope,fieldname) and otherScope[fieldname] is not  hiddenfields[fieldname]>
							<cfset warningMessage = warningMessage & "Field #fieldName# exists in both form and url scope #otherScope[fieldname]# #hiddenfields[fieldname]# <BR>">
						</cfif>


							<cfif scopes[i].urldecode>
								<cfset thisScope [fieldname] = urldecode(hiddenfields[fieldname])>
							<cfelse>
								<cfset thisScope [fieldname] = hiddenfields[fieldname]>
							</cfif>

							<cfset request.encryption.encryptedFields[fieldName] = hiddenfields[fieldname]>
					</cfloop>


					<!---
					2010/02/01 WAB LID 2977  check for resubmission of a page
					--->
					<cfif structKeyExists(formStateStructure, "uniqueID")>  <!--- uniqueID only passed from Forms, not URLs --->
						<cfif structKeyExists (session, "encryptedForms") and structKeyExists (session.encryptedForms,formStateStructure.uniqueID)>
							<cfset request.encryption.formResubmitted = false>
							<cfset structDelete (session.encryptedForms,formStateStructure.uniqueID)> <!--- now delete the key so that next time it is submitted the key does not exist --->
						<cfelse>
							<cfset request.encryption.formResubmitted = true> <!--- or could be a new session --->
						</cfif>

						<cfif structKeyExists (formStateStructure,"useOnce") and  formStateStructure.useOnce and request.encryption.formResubmitted>
							<cfoutput>This form has already been submitted once (or your session has expired).</cfoutput><CF_ABORT>
						</cfif>

					</cfif>

					<cfif structKeyExists(formStateStructure, "singleSession")><!--- tied to a session --->
						<cfif formStateStructure.sessionid is not session.sessionid>
							<cfoutput>This form has expired (new session).</cfoutput><CF_ABORT>
						</cfif>
					</cfif>

					<cfif structKeyExists(formStateStructure, "ttl_minutes")>
						<cfif dateDiff("n",formStateStructure.formdate,now()) gte formStateStructure.ttl_minutes>
							<cfoutput>This form has expired.</cfoutput><CF_ABORT>
						</cfif>
					</cfif>


				</cfif>
	</cfloop>

</cfloop>



	<cfif warningMessage is not "">

		<cfset warningStructure = {formStateStructure = formStateStructure}>
		<cfset application.com.errorHandler.recordRelayError_Warning (severity="WARNING", Type="Security Form Tampering",message="Form Tampering: #cgi.script_name#", warningStructure =warningStructure)>

	</cfif>




