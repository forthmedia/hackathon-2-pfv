<!--- 
WAB 2015-11-17
PROD2015-406 SQL Injection Security
A tag to be used in a similar way to cf_queryparam, but to be used when names of columns and tables are dynamic 
(and therefore open to sql injection)
--->
<cfif thisTag.executionMode is "start">
	<cfparam name="attributes.value">
	<cfoutput>#application.com.security.queryObjectName(attributes.value)#</cfoutput>
</cfif>	