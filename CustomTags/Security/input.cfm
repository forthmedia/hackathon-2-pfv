﻿<!--- �Relayware. All Rights Reserved 2014 

Filename:	input.cfm

Purpose: 	To safely encode attributes for an input tag
			Also (via the convertTagAttributeStructToValidAttributeList function) automatically deals with HTML attributes which cannot be set to false (such as required and disabled)


WAB 2015-03-18 JIRA Fifteen-303	Removed all code related to displaying labels for checkboxes/radios.  Put back in relayForms.cfc
2015-11-13  ACPK    Add default form-control class to text/select input if none assigned
2016-06-16  		WAB  	PROD2016-876 Added encrypt attribute for automatic encryption 
--->

<cfsetting enablecfoutputonly="true">
<cfif thisTag.executionMode is "start">
	<!---
	CF_INPUT
	WAB 2011-12-08
	Security Project
	A tag to replace <INPUT> and make sure that all attributes are htmlEncoded
	 --->
	<!--- Because we can't have cfif statements inside cf_input, I am going to deal with checked and selected so that we can have checked=false --->
	<cfif structKeyExists (attributes,"checked")>
		<cfif isBoolean(attributes.checked) and not attributes.checked>
			<cfset structDelete(attributes,"checked")>
		<cfelse>
			<cfset attributes.checked="checked">
		</cfif>
	</cfif>
	<!---
	2013/03/18 NJH Now handled in convertTagAttributeStructToValidAttributeList
	<cfif structKeyExists (attributes,"disabled")>
		<cfif isBoolean(attributes.disabled) and not attributes.disabled>
			<cfset structDelete(attributes,"disabled")>
		<cfelse>
			<cfset attributes.disabled="disabled">
		</cfif>
	</cfif> --->

	<!--- NJH 2012/10/25 Social CR --->
	<cfif not structKeyExists(attributes,"name") and structKeyExists(attributes,"id")>
		<cfset attributes.name = attributes.id>
	</cfif>
	<cfif structKeyExists(attributes,"name") and not structKeyExists(attributes,"id")>
		<cfset attributes.id = attributes.name>
	</cfif>

	<!--- deal with argument which can control which keys are used (or not) --->
	<!--- <cfset conversionArguments = {delimiter=" ",qualifier='"', encode="HTMLAttribute",includeNulls=true,excludeKeysRegexp=""}>
	<cfif structKeyExists (attributes,"includeNulls")>
		<cfset conversionArguments.includeNulls = attributes.includeNulls>
		<cfset structDelete(attributes,"includeNulls")>
	</cfif>
	<cfif structKeyExists (attributes,"excludeKeysRegexp")>
		<cfset conversionArguments.excludeKeysRegexp = attributes.excludeKeysRegexp>
		<cfset structDelete(attributes,"excludeKeysRegexp")>
	</cfif>
	<cfif structKeyExists (attributes,"includeKeysRegexp")>
		<cfset conversionArguments.includeKeysRegexp = attributes.includeKeysRegexp>
		<cfset structDelete(attributes,"includeKeysRegexp")>
	</cfif>

	<cfif structKeyExists(attributes,"required") and isBoolean(attributes.required) and not attributes.required>
		<cfset structDelete(attributes,"required")>
	</cfif> --->
	<cfif structKeyExists(attributes,"type")>
		<cfif listfindnocase("checkbox,radio",attributes.type)>
			<cfif structKeyExists(attributes,"class")>
				<cfset attributes.class=replaceNoCase(attributes.class,"form-control",attributes.type)>
			<cfelse>
				<cfset attributes.class=attributes.type>
			</cfif>
		<cfelseif listfindnocase("submit,button",attributes.type)>
			<cfif structKeyExists(attributes,"class")>
				<cfset attributes.class=replaceNoCase(attributes.class,"form-control","btn btn-primary")>
			<cfelse>
				<cfset attributes.class="btn btn-primary">
			</cfif>
		<!--- 2015-11-13  ACPK    Add default form-control class to text/select input if none assigned --->
		<cfelseif listfindnocase("text,select",attributes.type)>
			<cfif !structKeyExists(attributes,"class")>
				<cfset attributes.class="form-control">
			</cfif>
		</cfif>

	</cfif>

	<cfif structKeyExists(attributes,"encrypt") and attributes.encrypt>
		<cfparam name="attributes.encryptSingleSession" default="true">
		<cfset attributes.value = application.com.security.encryptVariableValue (value = attributes.value, name= attributes.name, singleSession = attributes.encryptSingleSession)>
		<cfset structDelete (attributes,"encrypt")>
		<cfset structDelete (attributes,"encryptSingleSession")>
	</cfif>
	
<cfoutput>
		<!--- 
		WAB 2015-03-18 	Removed all code related to displaying labels for checkboxes/radios.  
				Does not belong in this tag, and was causing knock on effects.  Put back in relayForms.cfc
				(Knock on effect was that single checkboxes being displayed in a columnar layout ended up with two labels (one in the left hand column and one to the right of the checkbox))
		--->
	<INPUT #application.com.relayForms.convertTagAttributeStructToValidAttributeList(attributeStruct=attributes,tagName="input")# />
</cfoutput>
</cfif>
<cfsetting enablecfoutputonly="false">