<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			includeWithSecurity.cfm	
Author:				WAB
Date started:			2009/06/23
	
Description:			
Part of the Relay Access Control System
Used if a template needs to be included from a directory which, if called directly on the URL, 
would have a checkPermission query set and some files automatically included.



Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
WAB 2009/07/14 changed name of fileAndDirectorySecurity.cfc to security.cfc

Possible enhancements:




 --->

	<cfparam name="attributes.template" type="STRING">			
	<cfparam name="attributes.debug" default = "false">			
	
	<CFSET Template  = attributes.template>
	<CFSET debug  = attributes.debug>

	<!--- Copies all the variables in the caller scope into the local variable scope --->
	<cfset structAppend(variables,caller) >

	<cfset securityObj = application.com.security>
	<cfset securityResult = securityObj.getFileSecurityRequirements(scriptname = Template )> 

	<cfmodule template="doCheckPermissionAndIncludes.cfm" securityResultStructure=#securityResult#>

	
	<cfif debug>
		<cfdump var="#securityResult#">
		<!--- <cfdump var="#checkPermission#"> --->
	</cfif>
	<cfinclude template="#Template#">



 

