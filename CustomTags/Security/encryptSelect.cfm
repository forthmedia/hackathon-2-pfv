<!--- �Relayware. All Rights Reserved 2014 --->
<cfif thisTag.executionMode is "end">
	<cfset regExpSelect = "<SELECT.*?name=([""'])?(.*?)(\1).*?>(.*?)</select>">
	<cfset groupsSelect = {1="quote",2="name",3="quote2",4="options"}>
	<cfset regExpOption = "<OPTION.*? value=([""'])?(.*?)(\1).*?>">
	<cfset groupsOption = {1="quote",2="value",3="quote2"}>
	
	<cfset AllSelects = application.com.regExp.refindAllOccurrences(regExpSelect, thisTag.GeneratedContent,groupsSelect)>
	
	<cfset newGeneratedContent = thistag.GeneratedContent>

	<cfloop array = "#AllSelects#" index="select">
		<cfset newSelectText = select.string>
		<cfset AllOptions = application.com.regExp.refindAllOccurrences(regExpOption, select.options,groupsOption)>	
		<cfloop array="#AllOptions#" index="option">

			<cfset value = option.value>
			<cfset encryptedValue = application.com.security.encryptVariableValue(name=select.name,value = option.value)>
			<cfset newOptionString = replace(option.string,option.quote&option.value&option.quote2,option.quote&encryptedvalue&option.quote2)>
			<cfset newSelectText = replace(newSelectText,option.string,newOptionString)>			
		</cfloop>

		<cfset newGeneratedContent = replace(newGeneratedContent,select.string,newSelectText)>				
	</cfloop>
	

	<cfset thisTag.generatedContent = newGeneratedContent>

</cfif>