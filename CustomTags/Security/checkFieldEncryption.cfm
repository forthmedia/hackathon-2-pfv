<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2011/06/01 LID 6769
Checks that the named fields have been passed as encrypted values (if fieldnames exist)
If have not been encrypted then aborts
2011/11/17 WAB added support for regExp. Default (and backwards compatible) is that fieldnames are just a list of field names, but regexps can be used
2012-02-08 added code so that we can tell where this tag is called from (and log that information)
2016-03-10 WAB logging error was not working - function getWhereCalledFrom() to get calling template must have been moved & renamed
 --->
<cfparam name="attributes.fieldNames" default = "">
<cfparam name="attributes.regExp" default = "false">


<cfset encryptionTest = application.com.security.confirmFieldsHaveBeenEncryptedWithFeedback(fieldnames = attributes.fieldNames, regExp = attributes.regExp)>

<cfif not encryptionTest.isOK>
	<cfset callingTemplate = application.com.globalfunctions.getFunctionCallingFunction()>
	<cfset warningStructure = {failedFieldNames = encryptionTest.failedFieldNames, template=callingTemplate}>
	<cfset application.com.errorHandler.recordRelayError_Warning (severity="WARNING", Type="Security Field Not Encrypted",message="Fields Not Encrypted: #cgi.script_name#", warningStructure =warningStructure)>
	<cfif false and application.com.relayCurrentSite.isEnvironment('Dev')>
		<cfoutput>
			Error: Fields not encrypted: <BR> 
			#replace(encryptionTest.failedFieldNames,",","<BR>","ALL")#   <BR>
			(This message not displayed on Live Sites) <br />
			Called from #htmleditformat(callingTemplate.template)# line #htmleditformat(callingTemplate.linenumber)#
		</cfoutput>
		<cf_abort>
	</cfif>

	<cfthrow detail="Encryption Error. Fields #encryptionTest.failedFieldNames# are not encrypted. File:#callingTemplate.templatePath#. Line:#callingTemplate.linenumber#" message = "Encryption Error. Fields #encryptionTest.failedFieldNames# are not encrypted." >
	
</cfif>


