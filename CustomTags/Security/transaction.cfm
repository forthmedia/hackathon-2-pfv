<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-07-31	IH	Case 429379 added tag to handle nested transactions
 --->
<cfparam name="request.openTransaction" default = 0>
<cfparam name="attributes.action" default="">

<cfif not thistag.HasEndTag and attributes.action is "BEGIN">
	<cfthrow message="cf_transaction requires an end tag.">
</cfif>

<cfif thisTag.executionMode is "start">

	<cfif attributes.action is "BEGIN">
		<cfif request.openTransaction eq 0>
			
			<cfquery name="transactionBegin" datasource = "#application.sitedatasource#">
				BEGIN tran
			</cfquery>
		</cfif>
		<cfset request.openTransaction++ >
		
	<!--- if we're at a top level transaction and committing, then commit. Otherwise, if we're rolling back, then just roll back --->	
	<cfelseif (attributes.action is "COMMIT" and request.openTransaction eq 1) or attributes.action is "ROLLBACK">
		<cftry>
			<cfquery name="transaction#attributes.action#" datasource = "#application.sitedatasource#">
				IF @@TRANCOUNT > 0 #attributes.action# tran
			</cfquery>
			<cfcatch>
			</cfcatch>
		</cftry>
	</cfif>
<cfelseif attributes.action is "BEGIN"> 
	<!--- we are at closing </cftranslate> (but not a self closed commit or rollback) --->

	<cfif structKeyExists(request,"openTransaction") and request.openTransaction is 1>
		<!--- we probably won't get in here often, but if someone just does an opening and a closing transaction, then it's assumed
			the the transation will be committed. --->
		<cfquery name="transactionEnd" datasource = "#application.sitedatasource#">
			IF @@TRANCOUNT > 0 COMMIT tran
		</cfquery>
	</cfif>

	<!--- this decrement is particularly important when we are in a nested cf_transaction --->
	<cfset request.openTransaction-- >
</cfif>