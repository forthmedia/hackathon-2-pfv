<cfsilent>
<!--- �Relayware. All Rights Reserved 2014 --->
	<!---
	WAB 2011-05-12

	The Relayware Wrapper for Running CFQueryParam
	Automatically switches to our own version if being run outside of a CFQUERY
	Can/could also switch to debug mode to switch on our own version - this will make it possible to run the SQL in CF debug directly


	2013-04-24	WAB Case 434803 deal with nvarchar(max) - nText incompatibility by switching to our own version if length of string is greater than 4000 characters

AMENDMENT HISTORY
2013-07-10	NYB	Case 436017 limit size of lists using CFVersion
2013-11-13  WAB Added support for null=true parameter
2014-11-25 	AXA Case 442806 prevent cfqueryparam rounding of decimal numbers
2016-01-11	WAB allow getDate() as a valid sql_timeStamp
2016-02-08	WAB	If value is blank then assume null for Numeric and Date types
2016/03/21	NJH	Use cf version if length over 4000
 --->
	<cfparam name="attributes.useCFVersion" default="true">
	<cfif structKeyExists (attributes,"null")>
		<cfparam name="attributes.value" default="">  <!--- 2013-11-13  WAB If attributes.null is passed then attributes.value is not necessary, but most of my code expects it.  Seems not to matter passing a blank attributes to value to cfqueryparam --->
	</cfif>
	<cfset sizeLimitOfCfTag = 2096><!--- 2013-07-10 NYB Case 436017 added --->

	<cfif thisTag.executionMode is "end">
		<cfexit>
	</cfif>

	<cfset simpleDataType = application.dataTypeLookup[attributes.cfsqltype].simple>

	<cfif isdefined("request.relayCurrentUser") and isdefined("request.relayCurrentUser.errors.queryParamDebug")and request.relayCurrentUser.errors.queryParamDebug>
		<cfset attributes.useCFVersion = false>
	<cfelseif not listfindnocase(getbaseTagList(),"cfquery") >
		<!--- If not inside a cfquery then don't use CF version, look at list of parent tags to determine --->
		<cfset attributes.useCFVersion = false>
	<cfelseif structKeyExists(attributes,"allowSelectStatement")>
		<cfset attributes.useCFVersion = false>
	<cfelseif listfindNoCase("numeric,date,bit",simpleDataType) and (attributes.value is "null" or attributes.value is "") >   <!--- null only really an issue during a set, since in a where clause you would have had to use is/is not rather than = <> --->
		<!--- CFQUERYPARAM can't deal with value="null" (you have to use the null attribute instead) --->
		<cfset attributes.null = true>
	<!--- NJH 2016/03/21 Sugar 448675 - not sure why, but this caused an issue when updating a large translation. don't think it's needed anyway.
	<cfelseif len(attributes.value) GT 4000>
		<cfset attributes.useCFVersion = false>--->
	<!--- START: 2013-07-10 NYB Case 436017 added --->
	<cfelseif structkeyexists(attributes,"list") and attributes.list and ListLen(attributes.value) gt sizeLimitOfCfTag>
		<cfset attributes.useCFVersion = false>
	<cfelseif simpleDataType is "date" and attributes.value IS "getDate()" >
		<cfset attributes.useCFVersion = false>
	</cfif>
	<!--- END: 2013-07-10 NYB Case 436017 added --->

	<!--- START: 2014-11-25 AXA set a default scale attribute for decimal numbers if none provided to prevent unwanted rounding
				WAB 2016-03-09 BF PROD2016-729/CASE 448451 same with numeric (and changed scale to 3, just in case someone wants 0.125!)
	--->
	<cfif listFindNoCase("cf_sql_decimal,cf_sql_numeric",attributes.cfsqltype)>
		<cfif NOT structKeyExists(attributes,'scale')>
			<cfset attributes.scale = 3 />
		</cfif>
	</cfif>
	<!--- END: 2014-11-25 AXA set a default scale attribute for decimal numbers if none provided to prevent unwanted rounding--->

<!--- NOTE!!! vvvvvv This all has to be on one line to prevent stray carriage returns.  <CFQUERYPARAM> has to be outside cfsilent --->
</cfsilent><cfif attributes.useCFVersion ><cfqueryparam attributeCollection = #attributes#><cfelse><cfoutput>#application.com.security.queryparam(argumentCollection = attributes)#</cfoutput></cfif>