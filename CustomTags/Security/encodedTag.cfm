<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="true"><cfif thisTag.executionMode is "start"><cfoutput><#TAGNAME# #application.com.structureFunctions.convertStructureToNameValuePairs(struct=attributes,delimiter=" ",qualifier='"', encode="HTMLAttribute")#></cfoutput><cfelse><cfoutput></#TAGNAME#></cfoutput></cfif><cfsetting enablecfoutputonly="false"><!--- 
WAB 2013-10-10 	
Security Project
A file to output a tag with encoded attributes
Is included by custom tags such as A.cfm (<CF_A>), TEXTAREA.cfm (<CF_TEXTAREA>) etc.
Saves having to repeat this code umpteen times
All needs to stay on one line to prevent unwanted whitespace between opening and closing tags,
Seems oblivious to <cfsetting enablecfoutputonly="true"> when there is a cfoutput in the calling page
 --->
