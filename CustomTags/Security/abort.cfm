<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CF_ABORT
WAB
Our own version of CFABORT calls all the onRequestEnd processing.


WAB 2013-01-28 Sprint 15&42 Comms. Clear up any processLocks here rather than in onRequestEnd 
WAB 2016-03-17	PROD2016-118  Housekeeping Improvements - remove housekeeping call
--->

<cfset application.com.globalFunctions.deleteThisRequestsProcessLocks()>
<cfset application.com.request.doOnRequestEnd()>
<cfabort attributeCollection=#attributes#>


