<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			doCheckPermissionAndIncludes.cfm	
Author:				WAB
Date started:			2009/06/23
	
Description:			
	Part of the Relay Access Control System
	When security is checked we also in some cases need to 
		i) return a checkPermission query 
		ii) include some initialisation files for the given directory
	[This makes the system backwards compatible with the previous system of every directory having its own application.cfm]	

	Variables which are set in these initialisation files need to be in the local variable scope.
	Only way I could think of doing it is to run the included files and then copy the variables scope to the caller scope
	Difficult to do with a cfc which cannot access caller scope (although we could have passed in the variables scope to a function, but was worried about variable contamination)
	Therefore had to do with a custom tag
	Having done this for the include files, thought might as well deal with the permissions query in the same way

	This custom tag is past the result of the getFileSecurityRequirements function
	

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2009/07/08  WAB   deal with securityResultStructure.security = none.  (didn't actually cause a bug, but looked odd in debug)
2010/07/14  WAB		when passing back variables to caller scope,  prevent variables called attributes or caller getting set 
Possible enhancements:

 --->
		<!--- 

		--->

		<cfparam name="attributes.securityResultStructure" type="struct">			
	
	<cfif attributes.securityResultStructure.security is not "" and attributes.securityResultStructure.security is not "none">
		<cfset securityTask = listFirst (attributes.securityResultStructure.security,":")>
		<cfset securityLevel = listLast (attributes.securityResultStructure.security,":")>
		<cfset checkPermission = application.com.login.checkInternalPermissions (securityTask,"",securityLevel)>   
		<cfset caller.checkPermission = checkPermission>
	</cfif>
					
			<cfif arrayLen(attributes.securityResultStructure.includeFiles) gt 0>
			<cfloop index="i" from = "1" to = #arrayLen(attributes.securityResultStructure.includeFiles)#>
				<cftry>
						<cfinclude template = "#attributes.securityResultStructure.includeFiles[i]#">
						<cfcatch>
						</cfcatch>
				</cftry>	
			</cfloop>
			
			<cfloop item="variableName" collection = "#variables#">
				<cfif not listfindnocase ("attributes,caller,local",variablename)>
					<cfset caller[variableName] = variables[variableName]>
				</cfif>
			</cfloop>

		</cfif>	
		

			<!--- This is for backwards compatibility and allows us to get rid of most application.cfm s --->
			<cfset caller.thisDir = attributes.securityResultStructure.thisDir>

