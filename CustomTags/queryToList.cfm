<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
queryToList.cfm

Custom tag which takes a query and creates a comma separated file or a variable

Author WAB

Date November 2000

Attributes:
Query [required]   query to be operated on (I think you need to pass it in the form query=#getReport# to pass by reference)
Variable			name of variable to return result in  defaults to "result"
Delimiter			defaults to a tab
FileName			fully qualified path of file to be saved
ColumnList			List of columns to use - defaults to all the columns


A fairly rough query

I want to make it so that the columns can be evaluated (for example a column called status with values 1,2,3,4 could be defined as being #statusArray[status]#
You can actually do this at the moment by putting statusArray[status] in the column list, but the heading would be all wrong.  Also statusArray would need scope of form. to get it into the custom tag



 --->
 
 
 <CFPARAM name="attributes.query">
 <CFPARAM name="attributes.variable" default = "result">
 <CFPARAM name="attributes.delimiter" default="	"><!--- tab ---> 
 <CFPARAM name="attributes.filename" default="">
  <CFPARAM name="attributes.columnList" default = "">
 
 

 <CFSET delimiter = attributes.delimiter>
 <CFSET newLine = chr(10)>
 <CFSET thisQuery = #attributes.query#>

	<CFIF attributes.columnList is "">
		 <CFSET columnList = thisQuery.columnList>
	<CFELSE>
		 <CFSET columnList = attributes.columnList>
	</cfif>

		 
 <CFSET columnListEval = replaceNoCase (columnList,",","###delimiter###","ALL")>
 <CFSET columnListEval = "'##" & columnListEval & "## #newLine#'">
 <CFSET headingList = replaceNoCase (columnList,",","#delimiter#","ALL") & newLine>
 
 

	 <CFSET tmpList = headingList> 
 <CFLOOP query="thisQuery">
 	
 	<CFSET tmpList = tmpList & evaluate(columnListEval) >
 
 </cfloop>
 
 
 <CFSET "caller.#attributes.variable#" = tmpList>
 
 <CFIF attributes.filename is not "">
 	
	<CFIF fileExists(attributes.filename)>
		<CFFILE ACTION="Delete"
			    FILE="#attributes.filename#"
				>

	</cfif>

		<CFFILE ACTION="Write"
			    FILE="#attributes.filename#"
    			OUTPUT="#tmpList#"
				>

 
 </cfif>
 
 
 
 