<!--- �Relayware. All Rights Reserved 2014 --->
<!---  relatedFile_UpdateSubroutine.cfm

william's new update procedure

2008-04-23    WAB/NJH  Bug Fix. Similar to Lexmark bug 39 in relatedfile.cfm. Check if the directory exists. If not, create it
2009/07/10    NJH      P-FNL069 - added file security
2009/09/09    WAB		LID 2512	Changed uploaded date to be datetime
2009/09/30		NJH		LID 2710	Check the permissions if fileID is passed through. Continue processing if the user is the one
						who uploaded the file or if the user has adminTask level 3. If the check fails, do nothing.
2010/04/08    GCC    LID 3187 - enabled files to be uploaded successfully with non-latin characters
2010/08/06		NJH		LID 3793 - RW8.3 - changed the way security is handled to reflect the new security structure
2010/11/09		WAB 	LID 4616.  Changed variable file to fileUploadResult.  Seemed to be some sort of contamination
						Have added a message and abort if the file is of a non-allowed type
2011/11/14		WAB 	Remove all code which refers to secure file stubs - no longer used in 8.3
2012-10-03 		PPB 	Case 430552 when updating a file the delete original failed
2012-10-23		WAB		P-XIR001 Add a hook so that entityID is passed into merge struct as #entityName#ID  (eg FundRequestActivityID)
2014-10-28		RPW		CORE-666 OPP Edit Screen needs multiple XML Changes
04/02/2016      DAN     Case 447930 - add few checks to prevent undefined errors
2016/03/10		NJH		JIRA PROD2016-472 Synching Attachments - set lastupdated/created columns to insert/update statements
--->

<!---START LH2201 2009/05/26 SSS Added translated error messages --->
<cf_translate>
<!---END LH2201 2009/05/26 SSS Added translated error messages --->
	<cfparam name="thisReplaceAllowed" default="no">

				<!--- now upload the file --->
			<cfif thisFile is not "">
                                <!--- NJH 2009/07/10 - use function to check if file is valid (ie. has allowed extension) --->
				<cfset fileUploadResult=application.com.fileManager.uploadFile(fileField="#thisFileField#",destination="#startpath#")>
				<!--- <cffile action="upload" destination="#startpath#" nameconflict="MAKEUNIQUE" filefield="#thisFileField#"> --->
				<!--- <cfset thisFilesize = fileUploadResult.FileSize> --->

				<cfif not fileUploadResult.isOK>
					<cfset message_HTML = fileUploadResult.message>
					<cfoutput><div class="errorblock">#message_HTML#</div></cfoutput>
					<CF_ABORT>
				</cfif>
			</cfif>



	<!--- going to do upload or something --->

	<!--- file to be uploaded, and the database to be updated. --->

	<!--- need to check whether the file category only allows one of those types - this is checked in the query --->

	<cfif thisAction is "put">
		<!--- file may already have been uploaded - if so, don't bother --->
		<!--- lets find out which files are currently related to this entity. --->
		<!--- NJH 2009/07/14 P-FNL069 - bring back whether file is secure --->
		<cfquery name="FileList" datasource="#application.sitedatasource#">
			select
				rf.FileID,
				rf.FileName,
				rft.SingleFile,
				rft.FilePath,
				rft.secure
			from
				RelatedFile as rf,
				RelatedFileCategory as rft
			where
				rf.FileCategoryID = rft.FileCategoryID
				and rf.EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				and rft.FileCategoryID =  <cf_queryparam value="#thisCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and rf.Language =  <cf_queryparam value="#thisLanguage#" CFSQLTYPE="CF_SQL_INTEGER" >
                <cfif isDefined("fileUploadResult")>
				and (
					(rf.filename  like  <cf_queryparam value="%#fileUploadResult.clientFile#" CFSQLTYPE="CF_SQL_VARCHAR" >  and rft.SingleFile = 0)
						or
					(rft.SingleFile = 1)
					)
                </cfif>
		</cfquery>
	<cfelse>
		<!--- NJH 2009/07/14 P-FNL069 - bring back whether file is secure --->
		<cfquery name="FileList" datasource="#application.sitedatasource#">
			select
				rf.FileID,
				rf.FileName,
				rf.FileSize,
				rft.SingleFile,
				rft.FilePath,
				rft.secure

			from
				RelatedFile as rf,
				RelatedFileCategory as rft
			where
				rf.FileCategoryID = rft.FileCategoryID
				and rf.FileID =  <cf_queryparam value="#thisFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfset FileSize = iif(FileList.FileSize is "",DE(0),DE(FileList.FileSize))>
		<cfset OldFilename = FileList.FileName>
	</cfif>

	<cfif FileList.recordcount gt 0 and thisReplaceAllowed is "no">
	<!---START LH2201 2009/05/26 SSS Added translated error messages --->
		<script language="JavaScript">
			alert("phr_relatedfile_FileAlreadyexists")
		</script>
	<!---END LH2201 2009/05/26 SSS Added translated error messages --->
		<cfset donothing=true>
	</cfif>


	<!--- NJH 2009/09/30 LID 2710 - get owner for file to determine permissions --->
	<cfif isDefined("thisFileID") and isNumeric(thisFileID)>
		<cfquery name="getFileOwner" datasource="#application.siteDataSource#">
			select uploadedBy from relatedFile where fileID =  <cf_queryparam value="#thisFileID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<!--- we aren't the owner of the file or we don't have sufficient priviledges, so don't do anything. --->
		<cfif not (getFileOwner.uploadedBy eq request.relayCurrentUser.personID or application.com.login.checkInternalPermissions("adminTask","Level3"))>
			<cfset doNothing = true>
		</cfif>
	</cfif>

	<!--- make sure the entity type exists - if not alert and quit --->
	<cfquery name="GetFileCategory" datasource="#application.sitedatasource#">
		select
			FileCategoryID,
			FileCategory,
			FileCategoryEntity,
			uniqueKey as EntityUniqueKey,
			FilePath,
			PrefixFile,
			fileUploadedEmail,  <!--- NJH 2007/06/25 added functionality to fire email when file uploaded or modified --->
			fileModifiedEmail,
			secure <!--- NJH 2009/07/14 P-FNL069 --->
		from
			RelatedFileCategory
			left join
			schemaTable on FileCategoryEntity = entityName
		where
			FileCategoryID =  <cf_queryparam value="#thisCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfif GetFileCategory.recordcount is 0>
	<!---START LH2201 2009/05/26 SSS Added translated error messages --->
		<script language="Javascript">
			alert("phr_relatedfile_recognisedType")
		</script>
	<!---END LH2201 2009/05/26 SSS Added translated error messages --->
		<cfset donothing=true>
	</cfif>

	<!--- make sure a \ is at the end of the path --->
	<cfset FilePath = GetFileCategory.FilePath>
	<cfif right(GetFileCategory.FilePath,1) is not "\">
		<cfset FilePath = GetFileCategory.FilePath & "\">
	</cfif>

	<cfif not donothing>

		<cfif GetFileCategory.secure>
			<cfset fileStartPath = secureStartPath>
		<cfelse>
			<cfset fileStartPath = startPath>
		</cfif>

		<!--- need to have a unique filename per language --->
		<cfif thisLanguage is not "0">
			<cfset lang_prefix="#thisLanguage#~">
		<cfelse>
			<cfset lang_prefix="">
		</cfif>
		<!--- create a filename based on the entity id and the filename supplied. --->
		<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
		<cfif GetFileCategory.PrefixFile is true>
			<cfif Len(attributes.uploadUUID)>
				<cfset FilePrefix = attributes.uploadUUID & "_">
			<cfelse>
				<cfset FilePrefix = thisEntity & "_">
			</cfif>
		<cfelse>
			<cfset FilePrefix = "">
		</cfif>

		<!--- pkp 2008-03-11 sony bug 61 replace space with _ --->
		<cfif isDefined("fileUploadResult") and (thisAction is "put" or (thisAction is "Update" and thisFile is not ""))>
			<cfset newfilename=fileStartPath & FilePath & lang_prefix & FilePrefix & replace(fileUploadResult.ClientFile," ","_","All")>
		<cfelse>
			<cfset newfilename=fileStartPath & FilePath & replace(getfilefrompath(FileList.filename)," ","_","All")>
		</cfif>
		<!--- pkp 2008-03-11 sony bug 61 replace space with _ --->
		<cfset newfilenamenopath = getfilefrompath(newfilename)>
		<cfset newPathNofileName = replace(newfilename,"\#newfilenamenopath#","")>

		<cftransaction>
			<!--- now rename/moved the uploaded file --->
			<cfif thisFile is not "" and isDefined("fileUploadResult") and fileUploadResult.isOK>

				<cfif not directoryExists("#fileStartPath##FilePath#")>
					<cfset	application.com.globalFunctions.CreateDirectoryRecursive(startpath&FilePath)>
				</cfif>

				<cfif not directoryExists(newPathNofileName)>
					<cfset application.com.globalFunctions.CreateDirectoryRecursive(newPathNofileName)>
				</cfif>

				<cfset filesize = fileUploadResult.FileSize>
				<!--- it will upload with the original name - need to change that to the related name --->
				<cffile action="MOVE" source="#fileUploadResult.serverdirectory#\#fileUploadResult.serverfile#" destination="#newfilename#">

				<!--- use cf_imagesize custom tag to set width, height if it is an image (only accurate with GIF) --->
				<cfif fileUploadResult.clientFile contains "gif">
					<!--- get the width and height of the file --->
					<cf_imagesize file="#newfileName#">
					<cfset properties="WIDTH=#WIDTH#,HEIGHT=#HEIGHT#">
				</cfif>
			</cfif>

			<!--- now record file information in the record --->
			<!--- NJH 2009/07/10 P-FNL069 - if file is ok(ie. was allowed), then update data --->
			<cfif isDefined("fileUploadResult") and fileUploadResult.isOK>
			<cfif FileList.recordcount is 0>

				<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
				<cfscript>
					if (Len(attributes.uploadUUID)) {
						variables.uploadedFileName = ListDeleteAt(newfilenamenopath,1,"_");
					} else {
						variables.uploadedFileName = "";
					}
				</cfscript>
				<cfquery name="InsertRelatedFileRecord" datasource="#application.sitedatasource#">
					insert into RelatedFile
					(
						EntityID,
						FileCategoryID,
						FileName,
						UploadedFileName,
						<cfif thisDescription is not "">
							Description,
						</cfif>
						UploadedBy,
						Uploaded,
						ReadLevel,
						WriteLevel,
						Language,
						FileSize
						<cfif isDefined("properties")>
							,
							properties
						</cfif>
						,UploadUUID
						,lastUpdated
						,lastUpdatedBy
						,lastUpdatedByPerson
						,created
						,createdBy
					)
					values
					(
						<cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#GetFileCategory.FileCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#newfilenamenopath#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#uploadedFileName#" CFSQLTYPE="CF_SQL_VARCHAR" null="#!Len(uploadedFileName)#">,
						<cfif thisDescription is not "">
							<cf_queryparam value="#thisDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						</cfif>
							<!--- #request.relayCurrentUser.personid#, --->
							<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#createodbcdatetime(now())#" CFSQLTYPE="cf_sql_timestamp" >, <!--- WAB 2009/09/09 changed from date to datetime, needed to know accurate time --->
						1,
						3,
						<cf_queryparam value="#thisLanguage#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#FileSize#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfif isDefined("properties")>
							,
							<cf_queryparam value="#properties#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
						,<cf_queryparam value="#attributes.uploadUUID#" CFSQLTYPE="CF_SQL_IDSTAMP" null="#!Len(attributes.uploadUUID)#" maxlength="36">
						,getDate()
						,<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
						,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
						,getDate()
						,<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
					)
				</cfquery>
			<cfelse>

					<!--- NJH 2010/08/06 RW8.3 - changed for new security structure --->
					<cfset fileNameAndPath = "#variables.FilePath##FileList.FileName#">
					<cfif GetFileCategory.secure>
						<!--- <cfset fileNameAndPath = fileNameAndPath & ".cfm"> --->
						<cfset fileNameAndPath = secureStartpath&fileNameAndPath>
					<cfelse>
						<cfset fileNameAndPath = startpath&fileNameAndPath>
					</cfif>

				<!--- if only one file allowed per type, make sure the original file is deleted --->
				<cfif FileList.FileName is not newFileNameNoPath and fileExists(fileNameAndPath) and thisUpdateAll is 0>

					<!--- <cffile action="DELETE" file="#startpath##variables.FilePath##FileList.FileName#"> --->	<!--- 2012-10-03 PPB Case 430552 the start of the comment block that commented this line out had been inadvertently removed --->
					<cffile action="DELETE" file="#fileNameAndPath#">
				</cfif>

				<cfquery name="UpdateRelatedFileRecord" datasource="#application.sitedatasource#">
					Update
						RelatedFile
					Set
						EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						FileCategoryID =  <cf_queryparam value="#GetFileCategory.FileCategoryID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						FileName =  <cf_queryparam value="#newfilenamenopath#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						Description =  <cf_queryparam value="#thisDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						UploadedBy = #request.relayCurrentUser.personid#,
						Uploaded = #createodbcdatetime(now())#,   <!--- WAB 2009/09/09 changed from date to datetime, needed to know accurate time --->
						ReadLevel = 1,
						WriteLevel = 3,
						Language =  <cf_queryparam value="#thisLanguage#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						FileSize =  <cf_queryparam value="#FileSize#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfif isDefined("properties")>
							,
							properties =  <cf_queryparam value="#properties#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
						,lastUpdated = getDate()
						,lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
						,lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
					where
						FileID =  <cf_queryparam value="#FileList.FileID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<!---
				If the file that was uploaded is referenced more than once as a related file, need to update the other
				references to it - if the user asked us to...
				--->
				<cfif thisUpdateAll is 1>
					<cfquery name="UpdateAll" datasource="#application.sitedatasource#">
						update
							RelatedFile
						set
							Filename =  <cf_queryparam value="#newfilenamenopath#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							FileSize =  <cf_queryparam value="#variables.FileSize#" CFSQLTYPE="CF_SQL_INTEGER" >
							<cfif isDefined("properties")>
								,
								properties =  <cf_queryparam value="#properties#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfif>
							,lastUpdated = getDate()
							,lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
							,lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
						from
							RelatedFile as rf,
							RelatedFileCategory as rft
						where
							rf.FileCategoryID = rft.FileCategoryID
							and Filename =  <cf_queryparam value="#OldFilename#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and entityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
							and rft.FilePath =  <cf_queryparam value="#FileList.FilePath#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>
				</cfif>
			</cfif>

			</cfif>
		</cftransaction>

		<!--- NJH 2007/06/25 fire an email if a file has been uploaded or modified --->
		<!--- set possible merge variables --->
		<cfset mergeStruct = structNew()>
		<cfset mergeStruct.file= structNew()>
		<cfset mergeStruct.file.Name = newfilenamenopath>
		<cfset mergeStruct.file.description = thisDescription>

		<cfif GetFileCategory.ENTITYUNIQUEKEY is not "">
			<cfset mergeStruct[GetFileCategory.ENTITYUNIQUEKEY]  = thisEntity>
		</cfif>

		<cfif (len(GetFileCategory.fileUploadedEmail) gt 0) and (FileList.recordCount eq 0)>
			<cfset application.com.email.sendEmail(emailTextID="#GetFileCategory.fileUploadedEmail#",personID=request.relayCurrentUser.personID, entityID = #thisEntity#, mergeStruct = mergeStruct)>
		<cfelseif (len(GetFileCategory.fileModifiedEmail) gt 0) and (FileList.recordCount eq 1)>
			<cfset application.com.email.sendEmail(emailTextID="#GetFileCategory.fileModifiedEmail#",personID=request.relayCurrentUser.personID, entityID = #thisEntity#, mergeStruct = mergeStruct)>
		</cfif>

	</cfif>
</cf_translate>

