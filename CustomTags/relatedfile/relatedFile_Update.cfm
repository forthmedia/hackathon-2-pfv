<!--- �Relayware. All Rights Reserved 2014 --->
<!---  2009/07/14 NJH P-FNL069 - added file security
	GCC 2010/04/08 LID 3187 - enabled files to be uploaded successfully with non-latin characters
	NJH 2010/08/06	LID 3793 - RW8.3 - changed the way security is handled to reflect the new security structure
	WAB 2011/03/14 replaced repeated code (delete file) with a cfc call
	
	2013-08-13	YMA		Case 436513 allow related file categories to choose between secure or not secure.
	
--->
<!--- deletion Update --->
 <cfif isDefined("RelatedFile_frmAction") and RelatedFile_frmAction is "delete" and attributes.NoUpdate is "no" >

	<!--- need to add security rights in here --->
	
	<cfset allowedtodelete = true>
	
	<cfif allowedtodelete>
		<!--- WAB 2011/03/14 converted one of the delete links to ajax, so put all this code in relatedFile.cfc --->
		<cfset application.com.relatedFile.deleteRelatedFileByID(RelatedFile_delFileID)>

		<!--- WAB 2011/03/14 removed and replaced with cfc call
		<!--- delete procedure goes here - this is where some tracking may come in handy--->
		<cftransaction>
			<!--- get the name of the file to delete --->
			<!--- NJH 2009/07/14 P-FNL069 - added secure to the query --->
			<cfquery name="GetFileDetails" datasource="#application.sitedatasource#">
				select
					rf.FileName,
					rft.FilePath,
					rf.uploadedBy,
					rft.secure
				from
					RelatedFile as rf,
					RelatedFileCategory as rft
				where
					rf.FileCategoryID = rft.FileCategoryID
					and rf.FileID = #RelatedFile_delFileID#
					<!--- NJH 2009/07/08 P-FNL069 - if we don't have adminTask:Level3, only get files that the user has uploaded. --->
					<cfif not application.com.login.checkInternalPermissions("adminTask","Level3")>
						and rf.uploadedBy = #request.relayCurrentUser.personID#
					</cfif>
			</cfquery>
			
			<cfif GetFileDetails.recordcount is 1>
			
				<!--- make sure that no other reference to this file exists --->
				
				<cfquery name="checkOne" datasource="#application.sitedatasource#">
					select
						rf.FileName
					from
						RelatedFile as rf,
						RelatedFileCategory as rft
					where
						rf.FileCategoryID = rft.FileCategoryID
						and rf.FileName = N'#getfiledetails.FileName#'
						and rft.FilePath = '#getfiledetails.FilePath#'
				</cfquery>
				
				<cfset fileNameAndPath = "#GetFileDetails.FilePath#\#getfiledetails.FileName#">
				<cfif GetFileDetails.secure>
					<cfset fileNameAndPath = secureStartPath&fileNameAndPath>
				<cfelse>
					<cfset fileNameAndPath = startPath&fileNameAndPath>
				</cfif>
				<!--- NJH 2009/07/08 - added the check for adminTask:level3 or if user was the one uploading the files.. a bit overkill, but makes it that much more secure.  --->
				<cfif checkOne.recordcount is 1 and fileExists(fileNameAndPath)
					and (application.com.login.checkInternalPermissions("adminTask","Level3") or GetFileDetails.UploadedBy eq request.relayCurrentUser.personID)
				>
					<!--- NJH 2009/07/13 P-FNL069 - remove stub if secure file. --->
					<!--- <cfif GetFileDetails.secure>
						<cfset application.com.fileManager.removeSecureFileAndStub(fileName=getfiledetails.FileName,path=GetFileDetails.FilePath)>
					</cfif>
					
					<!--- perform the actual file deletion --->					
					<cffile action="DELETE" file="#startpath##GetFileDetails.FilePath#\#getfiledetails.FileName#"> --->
					<cffile action="DELETE" file="#fileNameAndPath#">
				</cfif>

				<!--- delete the appropriate row from the table --->
				
				<cfquery name="deletefiledetails" datasource="#application.sitedatasource#">
					delete from
						RelatedFile
					where
						FileID = #RelatedFile_delFileID#
						<!--- NJH 2009/07/08 P-FNL069 - if we don't have adminTask:Level3, only delete files that the user has uploaded. --->
						<cfif not application.com.login.checkInternalPermissions("adminTask","Level3")>
							and uploadedBy = #request.relayCurrentUser.personID#
						</cfif>
				</cfquery>		
				
			</cfif>
		</cftransaction>
		--->
	</cfif>

</cfif>


<!--- update for the uploads from the put form--->
<cfif isDefined("RelatedFile_frmAction") and (RelatedFile_frmAction is "put" or RelatedFile_frmAction is "Update") and attributes.NoUpdate is "no">
	
	<cfset thisReplaceAllowed = RelatedFile_ReplaceAllowed>
	<cfset thisEntity = RelatedFile_Entity>
	<cfset thisCategoryID  = RelatedFile_FileCategoryID>
	<cfset thisLanguage = RelatedFile_Language>
	<cfset thisFile = RelatedFile_Filename>
	<cfset thisFileField = "RelatedFile_FileName">
	<cfset thisFileID = RelatedFile_FileID>
	<cfset thisDescription = RelatedFile_description>
	<cfset thisAction = RelatedFile_frmAction>
	<cfset thisUpdateAll = RelatedFile_UpdateAll>
	<cfset donothing = false>

	<cfinclude template="relatedFile_UpdateSubroutine.cfm">
	

	

<!--- update for manage --->
<cfelseif isDefined("RelatedFile_frmAction") and RelatedFile_frmAction is "manage" and attributes.NoUpdate is "no" >

	<cfif RelatedFile_FileCategorySelect is "">
		<!--- adding a new entity category record --->		
		<cftransaction>
			<cfquery name="NewEntity" datasource="#application.sitedatasource#">
				insert into RelatedFileCategory
				(
					FileCategoryEntity,
					FileCategory,
					FileCategoryDescription,
					FilePath,
					Active,
					SingleFile,
					PrefixFile,
					CreatedBy,
					Created,
					secure
				)
				values
				(
					<cf_queryparam value="#RelatedFile_FileCategoryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#RelatedFile_FileCategory#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#RelatedFile_FileCategoryDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#RelatedFile_FilePath#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#RelatedFile_Active#" CFSQLTYPE="cf_sql_float" >,
					<cf_queryparam value="#RelatedFile_SingleFile#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#RelatedFile_PrefixFile#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#createODBCDate(now())#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
					<cf_queryparam value="#RelatedFile_Secure#" CFSQLTYPE="CF_SQL_bit" >
				)
			</cfquery>
			
			<!--- now see whether a directory exists for this new entity category --->
			<cfif not DirectoryExists("#startpath##RelatedFile_FilePath#")>
				<cfdirectory action="create" directory="#startpath##RelatedFile_FilePath#">
			</cfif>	
		</cftransaction>
	<cfelse>
		<!--- updating an existing record --->
		<!--- get the existing details --->
		<cftransaction>
			<cfquery name="GetRelatedFileCategoryDetails" datasource="#application.sitedatasource#">
				select
					FileCategoryID,
					FileCategoryEntity,
					FileCategoryDescription,
					FilePath,
					SingleFile,
					Active,
					CreatedBy,
					Created,
					secure
				from
					RelatedFileCategory
				where
					FileCategoryID =  <cf_queryparam value="#RelatedFile_FileCategorySelect#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			
			<!--- if the FilePath has changed, need to rename the directory --->
			
			<cfif GetRelatedFileCategoryDetails.FilePath is not RelatedFile_FilePath and not DirectoryExists("#startpath##RelatedFile_FilePath#")>
				<cfdirectory action="rename" directory="#startpath##GetRelatedFileCategoryDetails.FilePath#" newdirectory="#startpath##RelatedFile_FilePath#">
			</cfif>
	
			<!--- 2013-08-13	YMA		Case 436513 allow related file categories to choose between secure or not secure. --->
			<cfif GetRelatedFileCategoryDetails.secure neq form.relatedFile_Secure>
				<!--- get files belonging to catagory --->
				<cfquery name="GetRelatedFileDetails" datasource="#application.sitedatasource#">
					select
						*
					from
						RelatedFile
					where
						FileCategoryID =  <cf_queryparam value="#RelatedFile_FileCategorySelect#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				<!--- move files to or from secure --->
				<cfloop query="GetRelatedFileDetails">
					<cfset application.com.filemanager.moveFileToOrFromSecure(form.relatedFile_Secure,GetRelatedFileDetails.filename,GetRelatedFileCategoryDetails.FilePath)>
				</cfloop>
			</cfif>
			
			<cfquery name="UpdateRelatedFileDetails" datasource="#application.sitedatasource#">
				update
					RelatedFileCategory
				set
					FileCategoryEntity =  <cf_queryparam value="#RelatedFile_FileCategoryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					FileCategory =  <cf_queryparam value="#RelatedFile_FileCategory#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					FileCategoryDescription =  <cf_queryparam value="#RelatedFile_FileCategoryDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					FilePath =  <cf_queryparam value="#RelatedFile_FilePath#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					Active =  <cf_queryparam value="#RelatedFile_Active#" CFSQLTYPE="cf_sql_float" > ,
					SingleFile =  <cf_queryparam value="#RelatedFile_SingleFile#" CFSQLTYPE="CF_SQL_bit" > ,
					PrefixFile =  <cf_queryparam value="#RelatedFile_PrefixFile#" CFSQLTYPE="CF_SQL_bit" > ,
					secure = <cf_queryparam value="#RelatedFile_Secure#" CFSQLTYPE="CF_SQL_bit" >
				where
					FileCategoryID =  <cf_queryparam value="#RelatedFile_FileCategorySelect#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>				
		</cftransaction>
	</cfif>
</cfif>




<!--- update for uploads using new style naming convention--->
<cfif isDefined("form.FieldNames") and  form.FieldNames contains "rf_filename" and attributes.action contains "update" and attributes.NoUpdate is "no">

	<cfloop index="aField" list="#form.FieldNames#">

		<cfif aField contains "rf_filename">

			<cfif evaluate(afield) is not "">

				<cfset isOK = true>
				<cfset thisFileField = aField>
				<cfset thisFile = evaluate(aField)>
				<cfset thisIdentifier = listrest(listrest(aField,"_"),"_")>
				<cfset thisCategoryID = listgetAt(thisIdentifier,1,"_")>					
				<cfset thisEntity = listGetAt(thisIdentifier,2,"_")>
				<cfif thisCategoryID is "0">
					<!--- category not defined, so must be a form field (select box) to tell us --->
					<cfif not isDefined("rf_Category_#thisIdentifier#")>
						<cfoutput>No Category Defined</cfoutput>
						<cfset isOK = false>
					<cfelse>
						<cfset thisCategoryID = evaluate("rf_Category_#thisIdentifier#")>
					</cfif>
				
				</cfif>
				
				<cfif thisEntity is 0>
					<cfif not isDefined("rf_Entity_#thisIdentifier#")>
						<cfoutput>No Entity Defined</cfoutput>
						<cfset isOK = false>
					<cfelse>
						<cfset thisEntity = evaluate("rf_Entity_#thisIdentifier#")>
					</cfif>
				</cfif>
				
				<cfparam name ="rf_Language_#thisIdentifier#" default = "0">
				<cfset thisLanguage = evaluate("rf_Language_#thisIdentifier#")>
				<cfparam name ="rf_Description_#thisIdentifier#" default = "">			
				<cfset thisDescription = evaluate("rf_Description_#thisIdentifier#")>
				<cfset thisAction = "put">
				<cfset thisReplaceAllowed = true>
				<cfset donothing = false>
				<cfset thisUpdateAll = false>
				
				<cfif isOK>
					<cfinclude template = "relatedFile_UpdateSubRoutine.cfm">
				</cfif>
					
			</cfif>		
		</cfif>
	
	</cfloop>

</cfif>	

