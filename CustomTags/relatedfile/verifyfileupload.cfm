<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		verifyFileUpload.cfm	
Author:			NJH  
Date started:	10-07-2009
	
Description:	Client side validation to check file extension against  what the application permits.		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<script>
	function verifyFile(filename) {
		var dot = filename.lastIndexOf(".");
		if (dot != -1) {
			var fileExt = filename.substr(dot+1,filename.length);

			<cfoutput>if (! (<cfloop list="#application.allowedFileExtensions#" index="ext">fileExt.toLowerCase() == ('#jsStringFormat(lcase(ext))#') || </cfloop> 1==0)) {
				alert('The file entered is not valid. Valid files are of type:\n#jsStringFormat(application.allowedFileExtensions)#')</cfoutput>
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
</script>
