<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			RelatedFile.cfm
Author:			DJH
Date created:	20 March 2000

Description:

Custom Tag. Allows files to be uploaded to the server, and be related to uniquely identifiable entities
in the system.

Dependencies:

Expects the CF_ImageSize custom tag to be available.
Expects the CF_EvaluateNameValuePair tag to be available.

Usage:

<CF_RelatedFile
				Action="Action"
				Entity="Entity"
				EntityType="Entity Type"
				FileCategory="File Category"
				FileID="File ID"
				Query="Query"
				Variable="variable"
				Filehandle="File Handle"
				WebHandle="Web Handle"
				ReplaceAllowed="Replace Allowed"
				DescriptionLength="DescriptionLength"
				Language="Language"
				PreferredLanguage="Preferred Language"
				showLanguage="true"
				showRelatedeFileCategory="true"
				NoUpdate="No Update"
				InForm = "Yes/No"  <!--- wab --->

Action:
Required. Specifies one or both of the following:
�	List - Displays a list of the files related to the current entity
�	Put - Displays the upload form
�	Get - Allows a text files contents to be loaded into a user specified variable name - this can therefore only be sensibly
			used where an entity, entity type and file category return only one file

Entity:
Optional where Action contains "List", otherwise required. Uniquely identifies the entity the files are related to.

Entity Type:
Required. Identifies types of entities the custom tag should be looking for.

File Category:
Optional. If specified, restricts the File Categorys allowed for either listing/putting/getting.

File ID:
Optional. Used where action is "Get" and is used in place of entity, entitytype, file category and language to uniquely identify a file

Query:
Optional. Only used where Action contains "List". Specifies the name of a query to return with query rows containing related file
data.

Variable:
Required where action is "Get". Provides the name of the variable to place the contents of the file into.

File Handle:
Optional. Used where action is "Get". Returns the dos file path of the file.

WebHandle:
Optional. Used where action is "Get". Returns the web file path of the file.

ReplaceAllowed:
Optional. Only used where action contains "Put". Indicates whether the replace checkbox appears. Default is Yes.


DescriptionLength:
Optional. Can be either "Long" or "Short". Default is "Short" - displays textbox 30 chars - maxlength 50. "Long" displays textarea
max 4000 chars.

Language:
Optional. The languages to be returned by the tag in comma delimited format.

PreferredLanguage:
Optional. The preferred language to return a related file in. Where the preferred language is not available, will return the
related file with no language set.

No Update:
Optional. If defined and yes, prevents any updating from occuring.


Version history:
1	-	Initial version
1.1	-	Replaced the session variables which were being called to make the tag work better
1.2 -	Added edit functionality
1.3 -	corrected a problem where double quote stopped edit functionality from working.

2		2001-05-20	WAB created Version 2 - split out some of the templates, made it return variables required for programmer to design own upload form and made it so that multiple files can be uploaded on one form.
2.01	2001-06-14   WAB altered behaviour when no files returned from a get  (doesn't display error message and resets variables to blank)
		09 May 2005 SWJ Added showLanguage and showRelatedFileCategory logic
2.0.2	27 May 2005 AJC Bug fix - The RelatedFileGet.cfm file created by this page now included the fix for open/save files correctly. Before the name was coming up as RelatedFileGet instead of the actual filename
		2009/05/21 NJH	P-SNY069 - changed text to phrases and added a phrase prefix.
1.4	 -  NJH 2007/03/01 check user's permissions before showing the delete file link. It shows only if you're the one who's uploaded it or you have adminTask:Level3 rights
		NJH 2009/01/23 Bug Fix Demo Issue 1595 - put a try/catch around the uploading of files as it falls over if the file is 0 bytes.
		NJH 2009/01/23 Bug Fix Demo Issue 1670 - removed the white background color.
		NJH 2009/07/08 P-FNL069 and LHID 2368 - check to see whether user has permission to delete files before actually deleting them. Current permission levels are
			you can be a user with adminTask:Level3 or if you are the user who has uploaded the file. Implemented secure file so only if you are logged in can you view files
			that have been marked as secure.
		NJH 2009/09/30 LID 2710 - added some security around editing files. Can only edit files you have uploaded or if you have adminTask Level3
        SSS 2009/02/04  Released plex044
		GCC 2010/04/08 LID 3187 - enabled files to be uploaded successfully with non-latin characters
		NJH 2010/08/06	LID 3793 - RW8.3 - changed the way security is handled to reflect the new security structure
		WAB 2011/03/14 replaced repeated codewith a cfcs call (deleting files and getting file query)
		Also made the delete an ajax function
		STCR 2011-04-05 LID5859 Fix Ajax delete for IE, fix delete link cursor behaviour.
		PPB	2011/08/17 REL109 hooks for restyling
		WAB 2011/10/20 changed call to relatedFileWS to use callWebservice.cfc
		Imre 2012/03/07 - CASE 424404 check if update file has already been called before calling it again
		2012/03/12 PPB P-AVD001 changed cf_include call
		2012-03-26 WAB cf_include call still does not work, reverted back to original version which does work rather than hacking cf_include
		2013-08-13 YMA Case 436513 allow related file categories to choose between secure or not secure.
		2013-12-02 AXA Case: 438218 replaced View Files with language phrase for international support
		2014-08-08 AXA Partner Cloud added default of 0 for attribute.entity, to fix bug for Business Plan and Support Case relaytags on the portal.
		2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes
		2015-02-06 ACPK FIFTEEN-101 added missing heading for icon column of table
		2015-10-02	ACPK	PROD2015-144 Use filename extensions to identify files
		2015-10-26	ACPK	PROD2015-142 Encode filename in thumbnail URLs
        2015-12-03  DAN     Case 446402 - minor layout improvements
--->

<!--- masked parameters --->

<cfparam name="attributes.Entity" default="0"> <!--- 2014-08-08 AXA Partner Cloud added default of 0 for attribute.entity, to fix bug for Business Plan and Support Case relaytags on the portal. --->
<cfparam name="attributes.EntityType" default="">
<cfif isDefined("attributes.FileCategory") and attributes.FileCategory is not "">
	<cfset FileCategoryList = attributes.FileCategory>
</cfif>
<cfparam name="attributes.FileCategory" default="">
<cfparam name="attributes.DescriptionLength" default="Short">
<cfparam name="attributes.replaceallowed" default="yes">
<cfparam name="attributes.NoUpdate" default="no">
<cfparam name="attributes.webhandle" default="webhandle">
<cfparam name="attributes.filehandle" default="filehandle">
<cfparam name="attributes.includehandle" default="includehandle">
<cfparam name="attributes.ftphandle" default="ftphandle">
<cfparam name="attributes.InForm" default="false">
<cfparam name="attributes.showLanguage" default="false">
<cfparam name="attributes.showFileCategoryDescription" default="false">
<cfparam name="attributes.showFileDescription" default="true"> <!--- NJH 2010/02/03 P-LEX044 --->
<cfparam name="attributes.phrasePrefix" type="string" default=""> <!--- NJH 2009/05/19 P-SNY069 --->
<cfparam name="attributes.listPhrasePrefix" type="string" default=""> <!--- RMB 2013-04-11 P-MIC002 - Added 'listPhrasePrefix' --->
<cfparam name="attributes.showSize" default="true"><!--- RMB 2013-04-11 P-MIC002 - Added 'showSize' --->
<cfparam name="attributes.showLoadedBy" default="true"><!--- RMB 2013-04-11 P-MIC002 - Added 'showLoadedBy' --->
<cfparam name="attributes.uploadUUID" default=""><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->

<!--- constants --->
<cfset startpath="#application.paths.userfiles#\content\">
<cfset secureStartPath="#application.paths.secure#\content\">
<cfset webpath="#request.currentsite.domainandroot#/content/"> <!--- NJH 2009/08/17 was "#request.currentsite.domainandroot##application. userfilespath#/content/" --->

<!--- first lets make sure that the minimum required parameters have been passed to the tag. --->
<cfif not isdefined("attributes.Action")>
	Error: You must at least provide an action to this tag.
	<cfexit>
</cfif>

<cf_translate>

<cfif attributes.action is "delete">
	<!--- want to procedurally delete a file and relatedfile record - i.e. not interactively --->

	<!--- first, check that the necessary parameters have been supplied --->

	<cfif not (isDefined("attributes.entity") and isDefined("attributes.entitytype"))>
		Error - you must define action, entity, and entitytype parameters
		<cfexit>
	</cfif>


	<!--- need to add security rights in here --->

	<cfset allowedtodelete = true>

	<cfif allowedtodelete>
		<!--- delete procedure goes here - this is where some tracking may come in handy--->
		<cfset application.com.relatedFile.deleteRelatedFileByEntityID(entityID = attributes.entity,entityType = attributes.entitytype)>

		<!--- WAB 2011/03/14 removed and replaced with cfc call
		<cftransaction>
			<!--- get the name of the file to delete --->
            <!--- NJH 2009/07/14 P-FNL069 - added secure to the query --->
			<cfquery name="GetFileDetails" datasource="#application.sitedatasource#">
				select
					rf.FileName,
					rft.FilePath,
					rf.uploadedBy,
					rft.secure
				from
					RelatedFile as rf,
					RelatedFileCategory as rft
				where
					rf.FileCategoryID = rft.FileCategoryID
					and rft.FileCategoryEntity = '#attributes.entitytype#'
					and rf.entityID = #attributes.entity#
					<!--- NJH 2009/07/08 P-FNL069 - if we don't have adminTask:Level3, only get files that the user has uploaded. --->
					<cfif not application.com.login.checkInternalPermissions("adminTask","Level3")>
						and rf.uploadedBy = #request.relayCurrentUser.personID#
					</cfif>
			</cfquery>

			<cfif GetFileDetails.recordcount is 1>

				<!--- make sure that no other reference to this file exists --->

				<cfquery name="checkOne" datasource="#application.sitedatasource#">
					select
						rf.FileName
					from
						RelatedFile as rf,
						RelatedFileCategory as rft
					where
						rf.FileCategoryID = rft.FileCategoryID
						and rf.FileName = N'#getfiledetails.FileName#'
						and rft.FilePath = '#getfiledetails.FilePath#'
				</cfquery>

				<!--- NJH 2010/08/06 RW8.3 - removed reference to .cfm if file was secure.. instead, look in secure directory --->
				<cfset fileNameAndPath = "\#GetFileDetails.FilePath#\#getfiledetails.FileName#">
				<cfif GetFileDetails.secure>
					<cfset fileNameAndPath = secureStartPath&fileNameAndPath>
				<cfelse>
					<cfset fileNameAndPath = startpath&fileNameAndPath>
				</cfif>
				<!--- NJH 2009/07/08 - added the check for adminTask:level3 or if user was the one uploading the files.. a bit overkill, but makes it that much more secure.  --->
				<cfif checkOne.recordcount is 1 and fileExists(fileNameAndPath)
					and (application.com.login.checkInternalPermissions("adminTask","Level3") or GetFileDetails.UploadedBy eq request.relayCurrentUser.personID)
				>
					<!---
					NJH 2010/08/06 RW8.3 - commented out code as we don't need this anymore
					NJH 2009/07/13 P-FNL069 - remove stub if secure file.
					<cfif GetFileDetails.secure>
						<cfset application.com.fileManager.removeSecureFileAndStub(fileName=getfiledetails.FileName,path=GetFileDetails.FilePath)>
					</cfif>--->

					<!--- perform the actual file deletion --->
					<!--- NJH 2010/08/06 RW8.3 - commented out for below code <cffile action="DELETE" file="#startpath##GetFileDetails.FilePath#\#getfiledetails.FileName#"> --->
					<cffile action="DELETE" file="#fileNameAndPath#">
				</cfif>

				<!--- delete the appropriate row from the table --->

				<cfquery name="deletefiledetails" datasource="#application.sitedatasource#">
					delete
						rf
					from
						RelatedFile as rf,
						RelatedFileCategory as rft
					where
						rf.FileCategoryID = rft.FileCategoryID
						and rft.FileCategoryEntity = '#attributes.entitytype#'
						and rf.entityID = #attributes.entity#
						<!--- NJH 2009/07/08 P-FNL069 - if we don't have adminTask:Level3, only delete files that the user has uploaded. --->
						<cfif not application.com.login.checkInternalPermissions("adminTask","Level3")>
							and rf.uploadedBy = #request.relayCurrentUser.personID#
						</cfif>
				</cfquery>

			</cfif>
		</cftransaction>

		--->
	</cfif>

</cfif>

<cfif not attributes.inForm and (attributes.action contains "put" or attributes.action contains "manage")><!--- WAB --->
	<!--- now see if there were any form fieldnames --->
	<cfif isDefined("Fieldnames")>
		<cfset processed_fields = "">
		<cfset passedformfields = "">
		<!--- loop round the form fieldnames and create a pipe delimited list containing all of them. --->
		<cfloop list="#fieldnames#" index="afield">
			<cfif listfind(processed_fields, afield) is 0 and afield does not contain "RelatedFile_" and afield does not contain "RF_">
				<cfif passedformfields is "">
					<cfset passedformfields = "#afield#=" & evaluate("#afield#")>
				<cfelse>
					<cfset passedformfields = passedformfields & "|" & "#afield#=" & evaluate("#afield#")>
				</cfif>
				<cfset processed_fields = listappend(processed_fields, afield)>
			</cfif>
		</cfloop>
	</cfif>
	<cfparam name="passedformfields" default="">
	<!---
	because the get facility needs an extra cfm file to work, make sure the file exists where it's expected - if not,
	copy the file across...
	--->
</cfif>


<!--- variables --->

<!---
this template is used to manage the actual adding of data too - where this is the case, it cannot be called as a custom
tag because a form submission is doing the work. Need to be able to load the screen that this tag was included in.
--->
<!--- ################ This section is for processing the form submissions #################### --->
<!--- needs to be included only once.  Do not try to replace with cf_includeonce - does not handle customTags directory and attribute variables do not get passed through --->
<cfif not structKeyExists (request,"relatedFileUpdateDone")>
	<cfinclude template = "relatedFile_Update.cfm">
	<cfset request.relatedFileUpdateDone = true>
</cfif>
<!--- ################ End of section for processing the form submissions #################### --->



<!---
Action may contain more than just one action - therefore need to check and possibly respond to there being more than one
action - hence separate cfif statements.
--->

<cfif attributes.action is "Manage">
	<CFINCLUDE TEMPLATE = "relatedFile_Manage.cfm">

</cfif>

<cfif attributes.action contains "Get">
	<cfparam name="attributes.variable" default="">
	<cfparam name="attributes.WebHandle" default="">
	<cfparam name="attributes.FileHandle" default="">
	<cfparam name="attributes.FileID" default="">

	<cfif (attributes.entity is "" or attributes.entitytype is "" or attributes.FileCategory is "") and attributes.FileID is "">
		Error: If you specify Get as the action, you must set either FileID or all of Entity, EntityType, FileCategory
		<cfexit>
	</cfif>

	<cfif "#attributes.variable##attributes.webhandle##attributes.filehandle#" is "">
		Error: If you specify Get as the action, you must set one of FileHandle, WebHandle, or Variable attributes.
		<cfexit>
	</cfif>

	<!--- WAB 2011/03/14 removed code below and replaced with this cfc call --->
	<cfif attributes.FileID is "" and attributes.Entity is not "" and attributes.EntityType is not "">
		<cfset fileList = application.com.relatedFile.getFileDetailsByEntityID (entityType = attributes.EntityType,entityID = attributes.Entity, FileCategory = attributes.FileCategory)>
	<cfelseif attributes.FileID is "" >
		<cfset fileList = application.com.relatedFile.getFileDetailsByFileID (fileID = attributes.FileID)>
	<cfelse>
		<!--- This is an error! --->
	</cfif>

	<!--- WAB 2011/03/14 removed and replaced with cfc call
	<cfquery name="FileList" datasource="#application.sitedatasource#">
		select
			rf.FileName,
			rft.FilePath,
			rft.secure
			--case when rft.secure = 1 then rf.fileName+'.cfm' else rf.filename end as secureFileName
		from
			RelatedFile as rf
			inner join RelatedFileCategory as rft on rf.FileCategoryID = rft.FileCategoryID
			left join Language as L on rf.Language = l.LanguageID
		where
			0=0
			<cfif attributes.FileID is "">
				<cfif attributes.Entity is not "" and attributes.EntityType is not "">
					and rf.EntityID = #attributes.Entity#
					and rft.FileCategoryEntity = '#attributes.EntityType#'
				</cfif>
				<cfif isDefined("QuotedList")>
					and rft.FileCategory in (#preservesinglequotes(QuotedList)#)
				</cfif>
				<!---<cfif isDefined("attributes.Language")>
					and l.Language in (#ListQualify(attributes.Language,"'",", ")#)
				<cfelse>
					and rf.language = 0
				</cfif>
				<cfif isDefined("attributes.PreferredLanguage")>
					and l.Language in (#ListQualify(attributes.PreferredLanguage,"'",", ")#)
				<cfelse>
					and rf.language = 0
				</cfif>--->
			<cfelse>
				and FileID = #attributes.FileID#
			</cfif>
	</cfquery>
	--->

	<!--- WAB 2011/03/14 all file paths now come from the query --->
	<!--- NJH 2010/08/06 RW8.3 - changed the filenames to handle the new security structure --->
	<!--- NJH 2009/07/14 - P-FNL069 - changed fileName to secureFileName --->
	<cfif FileList.recordcount is 1>
		<cfif attributes.variable is not "">
			<cffile action="READ" variable="caller.#attributes.variable#" file="#FileList.fileHandle#">
		</cfif>
		<cfif attributes.FileHandle is not "">
			<cfset "caller.#attributes.FileHandle#" = "#FileList.fileHandle#">
		</cfif>
		<cfif attributes.WebHandle is not "">
			<cfset "caller.#attributes.WebHandle#" = "#WebHandle#">
		</cfif>

		<!--- set the include path up correctly
		ftp\#FileList.FilePath#\
		--->

		<cfset CallingSubDir = getdirectoryfrompath(caller.cgi.cf_template_path)>
		<cfset CallingSubDir = replacenocase(CallingSubDir,"#application.paths.relay#\","")>
		<!--- we now have the directory --->
		<cfset includepath = REReplace(CallingSubDir, "[[:alnum:]]", "", "ALL")>
		<cfset includepath = Replace(includepath,"\","../","ALL")>
		<cfset "caller.#attributes.includehandle#" = "#includepath#ftp/#filelist.filepath#/#filelist.fileName#">
		<cfset "caller.#attributes.ftphandle#" = "ftp/#filelist.filepath#/#filelist.fileName#">
	<cfelse>
		<!--- either no files or too many files returned, set return variables to "" --->
		<cfif attributes.variable is not "">
			<CFSET "caller.#attributes.variable#" = "">
		</cfif>
		<cfif attributes.FileHandle is not "">
			<cfset "caller.#attributes.FileHandle#" = "">
		</cfif>
		<cfif attributes.WebHandle is not "">
			<cfset "caller.#attributes.WebHandle#" = "">
		</cfif>

		<cfset "caller.#attributes.includehandle#" = "">
		<cfset "caller.#attributes.ftphandle#" = "">

		<cfif FileList.recordcount GT 1>
		Error: only one file can be returned with the get attribute - your criteria returned <cfoutput>#FileList.recordcount#</cfoutput>
		</cfif>

		<cfexit>
	</cfif>
</cfif>


<cfif attributes.action contains "List" or isDefined("attributes.query") or attributes.action contains "variables" >
	<!--- This is where we provide a list of the files related to the entity we are looking at. --->

	<!--- Check that the minimum number of parameters for this action have been supplied. --->
	<cfif attributes.EntityType is "" and attributes.Entity is not "">
		Error: If you pass Entity as a parameter, you must also pass EntityType (the table the entity is from)
		<cfexit>
	</cfif>

	<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
	<cfset fileList = application.com.relatedFile.getFileDetailsByEntityID (entityID = val(attributes.entity),entityType = attributes.entityType,fileCategory = attributes.fileCategory,getrights = true,uploadUUID=attributes.uploadUUID)> <!--- 2014-08-08 AXA Partner Cloud added default of 0 for attribute.entity, to fix bug for Business Plan and Support Case relaytags on the portal. --->

	<!--- WAB 2011/03/14 removed and replaced with cfc call
	<!--- NJH 2010/08/06 RW8.3 - changed handling of secure files to handle the new security structure --->
	<!--- lets find out which files are currently related to this entity. --->
	<cfquery name="FileList" datasource="#application.sitedatasource#">
		select
			rf.FileID,
			rf.EntityID,
			rf.FileCategoryID,
			rf.FileName,
			case when isNull(rft.secure,0)=1 then '#secureStartPath#' else '#startpath#' end + rft.FilePath + '\' + rf.FileName as FileHandle,
			'#request.currentSite.httpProtocol##webpath#' + rft.FilePath + '/' + rf.FileName as WebHandle, <!--- 2009/07/14 P-FNL069 - add .cfm if secure file --->
			rf.Description,
			rf.UploadedBy,
			rf.Uploaded,
			rf.ReadLevel,
			rf.WriteLevel,
			rf.Expires,
			rf.FileSize,
			rf.Properties,
			isnull(l.Language,'None') as Language,
			rf.Language as LanguageID,
			rft.SingleFile,
			isNull(rft.PrefixFile,0) as PrefixFile, -- NJH 2008/04/22 added isNull as code below is looking for a boolean value
			rft.FileCategoryEntity,
			rft.FileCategory,
			rft.FileCategoryDescription,
			rft.FilePath,
			rft.CreatedBy,
			rft.Created,
			rft.Active,
			isnull(p.firstname,'') + ' ' + isnull(p.lastname,'') as Loadedby,
			rft.secure
		from
			RelatedFile as rf
			inner join RelatedFileCategory as rft on rf.FileCategoryID = rft.FileCategoryID
			<!--- 2007-11-01 MDC added for Trend Nabu P-TND008 --->
			left outer join person p on p.personid = rf.uploadedby
			left join Language as L on rf.Language = l.LanguageID
		where
			0=0
			<cfif attributes.Entity is not "">
				and rf.EntityID = #attributes.Entity#
			</cfif>
			<cfif attributes.EntityType is not "">
				and rft.FileCategoryEntity = '#attributes.EntityType#'
			</cfif>
			<cfif isDefined("QuotedList")>
				and rft.FileCategory in (#preservesinglequotes(QuotedList)#)
			</cfif>
			<!---<cfif isDefined("attributes.Language")>
				and l.Language in (#listqualify(attributes.Language,"'",", ")#)
			</cfif>	--->
			<!---<cfif isDefined("attributes.PreferredLanguage")>
				and l.Language in (#ListQualify(attributes.PreferredLanguage,"'",", ")#)
			<cfelse>
				and rf.language = 0
			</cfif>--->

	</cfquery>

	--->

	<!--- if there are attributes inside the properties field, append the columns to the query --->

	<cfset PropertiesColumn = valuelist(FileList.Properties)>
	<cfset DummyArray=ArrayNew(1)>
	<cfset addProperties = false>
	<cfif PropertiesColumn contains "WIDTH">
		<!--- need to append width as a column to this query --->
		<cfset dummy = QueryAddColumn(FileList, "Width", DummyArray)>
		<cfset addProperties=true>
	</cfif>
	<cfif PropertiesColumn contains "HEIGHT">
		<!--- need to append height as a column to this query --->
		<cfset dummy= QueryAddColumn(FileList, "Height", DummyArray)>
		<cfset addProperties=true>
	</cfif>
	<!--- if properties columns have been appended, add the data into them --->
	<cfif addProperties>
		<!--- loop around the query --->
		<cfloop query="FileList">
			<cfif FileList.Properties is not "">
				<cfset variables.width="">
				<cfset variables.height="">
				<!--- evaluate the name value pairs --->
				<cf_EvaluateNameValuePair NameValuePairs="#Properties#">
				<!--- set the appropriate field with the appropriate values --->
				<cfset dummy = QuerySetCell(FileList, "Width", variables.Width , FileList.currentRow )>
				<cfset dummy = QuerySetCell(FileList, "Height", variables.Height , FileList.currentRow )>
			</cfif>
		</cfloop>
	</cfif>

	<cfif isDefined("attributes.WebHandle")>
		<cfif FileList.recordcount gt 1>
			<cfset "caller.#attributes.WebHandle#" = valuelist(FileList.WebHandle)>
		<cfelseif FileList.recordcount is 1>
			<cfset "caller.#attributes.WebHandle#" = FileList.WebHandle>
		</cfif>
	</cfif>

	<cfif isDefined("attributes.FileHandle")>
		<cfif FileList.recordcount gt 1>
			<cfset "caller.#attributes.FileHandle#" = valuelist(FileList.FileHandle)>
		<cfelseif FileList.recordcount is 1>
			<cfset "caller.#attributes.FileHandle#" = FileList.FileHandle>
		</cfif>
	</cfif>


	<!--- if the query parameter has been set, we just want to return a query with the list of files - not display them --->
	<cfif isdefined("attributes.Query")>
		<cfset "caller.#attributes.Query#" = FileList>
	</cfif>

	<cfif attributes.action contains "list">
		<cfset "caller.relatedfile_FileList" = FileList>
		<!--- WAB 2011/03/14 make delete an ajax call--->

						<script language="javascript">
							function deleteFile(obj, FileID)
							{

								if (confirm("phr_relFile_ClickOkToDeleteOtherwiseCancel"))
								{

									parameters = 'FileID=' + FileID
									page = '/webservices/callWebservice.cfc?wsdl&method=callwebservice&webservicename=relatedfileWS&methodname=deleteRelatedFile&_cf_nodebug=true&returnformat=json'
									var myAjax = new Ajax.Request(
											page,
											{
												method: 'post',
												parameters:  parameters,
												asynchronous: true,
												onComplete: 	function (requestObject,JSON) {
													json = requestObject.responseText.evalJSON(true)

													if (json.ISOK) {
														// now delete row from table
														rowObj = $(obj).up('tr') <!--- STCR 2011-04-05 LID5859 Need to use $(this) for Prototype API in IE, otherwise the file deletes but appears to have failed because the page does not update. --->
														rowObj.remove();
													} else {
														alert (json.MESSAGE)
													}


													}
											}
										)

								}


							/*
								if (confirm("phr_relFile_ClickOkToDeleteOtherwiseCancel"))
								{
									document.upload.RelatedFile_frmAction.value = doWhat
									document.upload.RelatedFile_delFileID.value = FileID
									document.upload.submit()
								}
								)
							*/

							}
						</script>

		<!--- lets output the results of the query as a list on screen --->
		<table id="relatedFileTable" class="screenTable table table-striped table-hover table-bordered" data-role="table" data-mode="reflow">
		<!--- RMB - P-MIC002 - 2013-04-11 - Added 'listPhrasePrefix' - START --->
		<cfoutput>
			<thead>
				<tr>
					<cfif attributes.showLoadedBy>
					<th>phr_#attributes.listPhrasePrefix#relFile_LoadedBy</th>
					</cfif>
					<th>phr_#attributes.listPhrasePrefix#relFile_Filename</th>
					<!--- NJH 2010/02/03 P-LEX044 added switch to show file description --->
					<cfif attributes.showFileDescription>
						<th>phr_#attributes.listPhrasePrefix#relFile_Description</th>
					</cfif>
					<cfif attributes.showLanguage>
						<th>phr_#attributes.listPhrasePrefix#relFile_Language</th>
					</cfif>
					<cfif attributes.showFileCategoryDescription>
						<th>phr_#attributes.listPhrasePrefix#relFile_Type</th>
					</cfif>
					<cfif attributes.showSize>
					<th>phr_#attributes.listPhrasePrefix#relFile_Size</th>
					</cfif>
					<!--- 2015-02-06 ACPK FIFTEEN-101 added missing heading for icon column of table--->
					<th>phr_#attributes.listPhrasePrefix#relFile_Icon</th>
					<th colspan="3">phr_#attributes.listPhrasePrefix#relFile_Actions</th> <!--- Case 446402 - since this its the last column in the table header we are OK to put colspan even if greater than the remaining bits --->
				</tr>
			</thead>
		</cfoutput>
		<!--- RMB - P-MIC002 - 2013-04-11 - Added 'listPhrasePrefix' - END --->

			<cfoutput query="FileList">
				<cfset f_name = fileList.filehandle> <!--- NJH 2010/08/06 RW8.3 - removed  &".cfm"--->
				<cfif fileExists(f_name)>
				<cfset width=0>
				<cfset height=0>

				<cfif FileList.currentRow mod 2 eq 1>
					<cfset rowClass = "dataRowOdd">
				<cfelse>
					<cfset rowClass = "dataRowEven">
				</cfif>

				<tr valign="top" class="#rowClass#">
					<cfif attributes.showLoadedBy>
					<td>#htmleditformat(loadedby)#</td>
					</cfif>

					<cfif FileList.PrefixFile>
						<cfset thefile=listrest(Filename,"_")>
					<cfelse>
						<cfset thefile=FileList.Filename>
					</cfif>
					<!--- NJH 2009/09/30 LID 2710 - added some security around editing files. Can only edit files you have uploaded or if you have adminTask Level3. --->
					<td class="relatedFilelink"><cfif attributes.action contains "list" and attributes.action contains "put" and (application.com.login.checkInternalPermissions("adminTask","Level3") or UploadedBy eq request.relayCurrentUser.personID)>
							<a href="javascript:edit(#FileList.FileID#)">#htmleditformat(thefile)#</a>
						<cfelse>
							#htmleditformat(thefile)#
						</cfif>
					</td>
					<!--- NJH 2010/02/03 P-LEX044 added switch to show file description --->
					<cfif attributes.showFileDescription><td>#htmleditformat(Description)#</td></cfif>

					<cfif attributes.showLanguage>
						<td>#htmleditformat(Language)#</td>
					</cfif>
					<cfif attributes.showFileCategoryDescription>
						<td>#htmleditformat(FileCategoryDescription)#</td>
					</cfif>
					<cfif attributes.showSize>
					<td><cfif filesize lt 1024>#htmleditformat(filesize)# bytes<cfelse>#numberformat(FileList.filesize/1024)# kb</cfif></td>
					</cfif>
						<td nowrap="nowrap">
					<!--- display a tiny version of it... --->
					<cf_evaluateNameValuePair nameValuePairs="#FileList.properties#">
					<cfparam name="displaywidth" default="15">
					<cfif variables.width is not 0 and variables.height is not 0>
						<cfset ratio=variables.height/variables.width>
						<!--- height is going to be 15 - maintain the ratio. --->
						<cfset displaywidth= 15/ratio>
					</cfif>
					<!--- 2015-10-02	ACPK	PROD2015-144 Use filename extensions to identify files --->
					<cfif listFindNoCase("jpg,jpeg,gif,png",ListLast(FileName,"."))>
						<img border="0" width="#int(variables.displaywidth)#" height="15" src="#request.currentSite.httpProtocol##webpath##filelist.FilePath#/#URLEncodedFormat(filename)#"> <!--- 2015-10-26	ACPK	PROD2015-142 Encode filename in thumbnail URLs --->
						<span class="relatedFileDimensions">(#htmleditformat(variables.width)# x #htmleditformat(variables.height)#)</span>
					<cfelse>
						<img border="0" width="#int(variables.displaywidth)#" height="15" src="/code/borders/images/default_icon.png">
					</cfif>
					</td>


					<!---

					if the file is allowed to be retrieved, display a link to pull the file off the server.

					This is a security specific requirement - add when defined.

					--->
					<cfif 0 is 0>
						<!--- NJH 2009/07/17 P-FNL069 - encrypt the query string
						WAB 2009/09/15 altered to use encryptURL rather than encryptQueryString
						--->
						<td><a href="#application.com.security.encryptURL('/RelatedFileGet.cfm?get=yes&FileID=#FileID#')#" class="btn btn-primary" id="relatedFileGet">phr_relFile_GetFile</a></td>
					</cfif>

					<!---

					If the current user has rights to delete the file, display a link allowing them to do so

					This is a security specific requirement - add when defined.

					--->
					<cfif 0 is 0>
						<!--- NJH 2007/03/01 check the user's permission levels before displaying the delete link  --->
						<cfif hasDeleteRights>
						<td><a href="##" onclick="deleteFile(this,#FileID#); return false;" class="btn btn-primary" id="relatedFileDelete">phr_relFile_DeleteFile</a></td><!--- STCR 2011-04-05 LID5859 Need an href attribute in order for the browser to display a link-style pointer. Return false so that the href is not followed. --->
						</cfif>
					</cfif>

					<!---
					if the file is viewable, display a link which allows the user to view it in a new window

					Add the security requirements to this when defined.

					--->
					<!--- 2015-10-02	ACPK	PROD2015-144 Use filename extensions to identify files --->
					<cfif listFindNoCase("jpg,jpeg,gif,htm,html,txt",ListLast(FileName,".")) and not secure>
						<!--- 2013-12-02 AXA Case: 438218 replaced View Files with language phrase for international support --->
						<td><a href="javascript:void(window.open('#request.currentSite.httpProtocol##webpath##filelist.FilePath#/#filename#','',''))" class="btn btn-primary" id="relatedFileView">phr_relFile_ViewFile</a></td>
					</cfif>

				</tr>
				</cfif>
			</cfoutput>
			<cfif FileList.recordcount is 0>
			<cfoutput>
			<tr>
				<td colspan="8">phr_relFile_NoFilesRelatedToThis phr_#htmleditformat(attributes.entitytype)#.</td> <!--- Case 446402 - colspan across all the columns in the header, OK even if actual number is lower than 5 --->
			</tr>
			</cfoutput>
			</cfif>
		</table>
	</cfif>
</cfif>



<cfif attributes.action contains "Put" or attributes.action contains "variables">
	<!--- list the different types of files - from the RelatedFileCategory table --->

	<cfquery name="FileCategoryList" datasource="#application.sitedatasource#">
		select

			rft.FileCategoryID,
			rft.FileCategoryID as datavalue,
			rft.FileCategoryEntity,
			rft.FileCategory,
			rft.FileCategoryDescription,
			rft.FileCategoryDescription as displayValue,
			rft.FilePath,
			rft.CreatedBy,
			rft.Created,
			rft.Active,
			rft.SingleFile
		from
			RelatedFileCategory as rft
		where
			Active = 1
			and FileCategoryEntity =  <cf_queryparam value="#attributes.EntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif isDefined("FileCategoryList")>
				and FileCategory in (<cf_queryparam value="#FileCategoryList#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>
	</cfquery>

	<cfquery name="languages" datasource="#application.sitedatasource#">
		select
			language,
			languageID,
			language as displayValue,
			languageID as dataValue

		from
			Language
		order by
			Language
	</cfquery>

</cfif>

<cfif attributes.action contains "variables">
	<!--- returns all the details in variables --->
	<cfif fileCategoryList.recordCount is 1>
		<cfset categoryid = fileCategoryList.fileCategoryID>
	<cfelse>
		<cfset categoryid = 0>
	</cfif>
	<cfset identifier = "#categoryID#_#attributes.entity#">

	<cfset tmp = structNew()>
	<cfset tmp.fieldname = structNew()>
	<cfset tmp.formfieldname.filename = "rf_filename_#identifier#">
	<cfset tmp.formfieldname.Description = "rf_description_#identifier#">
	<cfset tmp.formfieldname.Category = "rf_category_#identifier#">
	<cfset tmp.identifier = identifier>
	<cfset tmp.categoryQuery = #fileCategoryList#>
	<cfset tmp.languageQuery = #languages#>
	<cfset tmp.filelist = filelist>

	<cfset caller.relatedFileVariables = tmp>

</cfif>


<cfif attributes.action contains "Put">
	<!--- this outputs the form for doing the upload --->
	<cfinclude template = "relatedFile_Put.cfm">
</cfif>

</cf_translate>

