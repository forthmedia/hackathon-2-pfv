<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2009/09/14  WAB this file didn't exists, but was called in relatedFileV2.cfm, so this code cut out of RelatedFileV1.cfm

2013-08-13	YMA		Case 436513 allow related file categories to choose between secure or not secure.
2015-11-23	SB		Bootstrap
2016/09/08	NJH		JIRA PROD2016-2315  - make fileCategoryID visible.
 --->


	<!--- this is where the file categories are managed --->

	<!--- add some security stuff in here --->
	<cfset allowedtomanage=true>

	<cfif allowedtomanage>
		<cfquery name="RelatedFileCategories" datasource="#application.sitedatasource#">
			select
				FileCategoryID,
				FileCategoryEntity,
				FileCategory,
				FileCategoryDescription,
				FilePath,
				SingleFile,
				Active,
				PrefixFile,
				Secure
			from
				RelatedFileCategory
			where
				0=0
				<cfif attributes.entitytype is not "">
					and FileTypeEntity =  <cf_queryparam value="#attributes.EntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif isDefined("FileCategoryList")>
					and FileCategory in (<cf_queryparam value="#FileCategoryList#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
				</cfif>
			order by
				FileCategoryEntity,
				FileCategory
		</cfquery>

		<script language="javascript">
			// not too much data is expected, so using javascript to handle will make things nicer
			var records = new Array()
			// array constructor
			function record(FileCategoryEntity, FileCategory, FileCategoryDescription, FilePath, SingleFile, Active, PrefixFile, Secure,FileCategoryID)
			{
				this.FileCategoryEntity = FileCategoryEntity
				this.FileCategory = FileCategory
				this.FileCategoryDescription = FileCategoryDescription
				this.FilePath = FilePath
				this.SingleFile = SingleFile
				this.Active = Active
				this.PrefixFile = PrefixFile
				this.Secure = Secure
				this.FileCategoryID = FileCategoryID
			}
			// load the array
			<cfoutput query="RelatedFileCategories">
			records[#jsStringFormat(FileCategoryID)#] = new record("#jsStringFormat(FileCategoryEntity)#", "#jsStringFormat(FileCategory)#", "#jsStringFormat(FileCategoryDescription)#", "#jsStringFormat(FilePath)#", "#jsStringFormat(SingleFile)#", "#jsStringFormat(Active)#", "#jsStringFormat(PrefixFile)#", "#jsStringFormat(Secure)#",#fileCategoryID#)
			</cfoutput>

			// function to load the data
			function loadRecord(aselect)
			{
				if (aselect.selectedIndex > 0)
				{
					thisrec = aselect.options[aselect.selectedIndex].value
					// load a record
					aselect.form.RelatedFile_FileCategoryEntityHidden.value = records[thisrec].FileCategoryEntity
					aselect.form.RelatedFile_FileCategoryEntity.value = records[thisrec].FileCategoryEntity
					aselect.form.RelatedFile_FileCategory.value = records[thisrec].FileCategory
					aselect.form.RelatedFile_FileCategoryDescription.value = records[thisrec].FileCategoryDescription
					aselect.form.RelatedFile_FilePath.value = records[thisrec].FilePath
					aselect.form.RelatedFile_FileCategoryID.value = records[thisrec].FileCategoryID
					SingleFileVal = records[thisrec].SingleFile
					if (SingleFileVal == 1)
						aselect.form.RelatedFile_SingleFile[0].checked = true
					else
						aselect.form.RelatedFile_SingleFile[1].checked = true
					ActiveVal = records[thisrec].Active
					if (ActiveVal == 1)
						aselect.form.RelatedFile_Active[0].checked = true
					else
						aselect.form.RelatedFile_Active[1].checked = true
					PrefixFileVal = records[thisrec].PrefixFile
					if (PrefixFileVal == 1)
						aselect.form.RelatedFile_PrefixFile[0].checked = true
					else
						aselect.form.RelatedFile_PrefixFile[1].checked = true
					SecureVal = records[thisrec].Secure
					if (SecureVal == 1)
						aselect.form.RelatedFile_Secure[0].checked = true
					else
						aselect.form.RelatedFile_Secure[1].checked = true
					aselect.form.submitbutton.value = "Update this record"
				}
				else
				{
					// blank em out
					blankEm(aselect)
				}
			}

			function blankEm(aselect)
			{
				aselect.form.RelatedFile_FileCategoryEntityHidden.value = aselect.form.RelatedFile_FileCategoryEntitySelect.options[aselect.form.RelatedFile_FileCategoryEntitySelect.selectedIndex].value
				aselect.form.RelatedFile_FileCategoryEntity.value = aselect.form.RelatedFile_FileCategoryEntitySelect.options[aselect.form.RelatedFile_FileCategoryEntitySelect.selectedIndex].value
				aselect.form.RelatedFile_FileCategorySelect.selectedIndex = 0
				aselect.form.RelatedFile_FileCategory.value = ""
				aselect.form.RelatedFile_FileCategoryDescription.value = ""
				aselect.form.RelatedFile_FilePath.value = ""
				aselect.form.RelatedFile_SingleFile[0].checked = true
				aselect.form.RelatedFile_SingleFile[1].checked = null
				aselect.form.RelatedFile_Active[0].checked = true
				aselect.form.RelatedFile_Active[1].checked = null
				aselect.form.RelatedFile_PrefixFile[0].checked = true
				aselect.form.RelatedFile_PrefixFile[1].checked = null
				aselect.form.RelatedFile_Secure[0].checked = null
				aselect.form.RelatedFile_Secure[1].checked = true
				aselect.form.RelatedFile_FileCategoryID.value = "";
				aselect.form.submitbutton.value = "Add this record"
			}

		</script>



		<cfoutput>
		<form name="upload" action="#cgi.script_name#?#request.query_string#" method="post" enctype="multipart/form-data" onSubmit="return checkForm(this)">
			<CF_INPUT type="hidden" name="RelatedFile_entity" value="#attributes.entity#">
			<CF_INPUT type="hidden" name="RelatedFile_entitytype" value="#attributes.entitytype#">
			<input type="hidden" name="RelatedFile_frmAction" value="manage">
			<input type="hidden" name="RelatedFile_FileCategoryEntityHidden">
		</cfoutput>
			<!--- output the fields that were included in the calling template as hidden fields --->
			<cfloop list="#passedformfields#" delimiters="|" index="afield">
				<cfoutput><CF_INPUT type="hidden" name="#listfirst(afield,"=")#" value="#listrest(afield,"=")#"></cfoutput>
			</cfloop>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="grey-box-top">
						<h2>Managing the related files categories</h2>
						<p>To manage the categories, choose the relevant entity and category from the selection below - the data for the selected category will be automatically displayed. If you wish to create a new entity, make sure "Create a	new Entity" is selected below. If you wish to create a new category, make sure "Create a new Category" is selected below.</p>
						<cf_TwoSelectsRelated	query="RelatedFileCategories"
												name1="RelatedFile_FileCategoryEntitySelect"
												name2="RelatedFile_FileCategorySelect"
												display1="FileCategoryEntity"
												display2="FileCategoryDescription"
												value1="FileCategoryEntity"
												value2="FileCategoryID"
												emptytext1="Create a new Entity"
												emptytext2="Create a new Category"
												size1="6"
												size2="6"
												forcewidth1="50"
												forcewidth2="50"
												htmlbetween="&nbsp;&nbsp;"
												onChange1="blankEm(this)"
												onchange="loadRecord(this)"
												formname="upload">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="grey-box-top relatedFileManCol2">
					<!--- Box 0 --->
						<h4>Entity</h4>
						<p>This is usually the name of the table you are relating entities to. You cannot change the value of existing entities - only use it to add new types of entity.</p>
						<input type="text" class="form-control" name="RelatedFile_FileCategoryEntity" value="" size="30" onchange="if(this.form.RelatedFile_FileCategoryEntitySelect.selectedIndex > 0) this.value = this.form.RelatedFile_FileCategoryEntityHidden.value">
					<!--- Box 1 --->
						<h4>File Category</h4>
						<p>This is the category.</p>
						<input type="text" class="form-control" name="RelatedFile_FileCategory" value="" size="30">
						<h4>File Category ID</h4>
						<p>This is the unique category ID.</p>
						<input type="text" class="form-control" name="RelatedFile_FileCategoryID" value="" size="30" disabled>
					<!--- Box 2 --->
						<h4>File Category Description</h4>
						<p>This is the Description for the category. This will usually be the same as the category but formatted with whitespace.</p>
						<input type="text" class="form-control" name="RelatedFile_FileCategoryDescription" value="" size="30">
					<!--- Box 3 --->
						<h4>File Path</h4>
						<p>This is the path you wish files of this entity and category to be placed in under the <cfoutput>#htmleditformat(startpath)#</cfoutput> directory. Make sure you omit any trailing backslashes.</p>
						<input type="text" class="form-control" name="RelatedFile_FilePath" value="" size="30" onchange="if(this.value.charAt(this.value.length -1) == '\\' || this.value.charAt(this.value.length -1) == '/') this.value = this.value.substring(0,this.value.length -1)">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<!--- Box 4 --->
					<div class="grey-box-top">
						<h3>Single File?</h3>
						<p>If Yes, only one file for this category for an entity may be uploaded. If no, many files for an entity may be uploaded.</p>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_SingleFile" value="1"> Yes</label>
						</div>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_SingleFile" value="0"> No</label>
						</div>
					</div>
					<!--- Box 5 --->
					<div class="grey-box-top">
						<h3>Active?</h3>
						<p>This indicates whether the category is Active.</p>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_Active" value="1"> Yes</label>
						</div>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_Active" value="0"> No</label>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<!--- Box 6 --->
					<div class="grey-box-top">
						<h3>Prefix File?</h3>
						<p>This indicates whether the files that are uploaded have the entity ID prefixed to help ensure uniqueness of the file name.</p>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_PrefixFile" value="1"> Yes</label>
						</div>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_PrefixFile" value="0"> No</label>
						</div>
					</div>
					<!--- Box 7 --->
					<div class="grey-box-top">
						<h3>Secure File?</h3>
						<p>This indicates whether the files that are uploaded are stored in a secure directory.</p>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_Secure" value="1"> Yes</label>
						</div>
						<div class="radio">
							<label><input type="radio" name="RelatedFile_Secure" value="0" checked="true"> No</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<!--- <h3>Submit changes</h3>
					<p>Click the button to submit the changes.</p> --->
					<input type="submit" name="submitbutton" value="Add this record" class="btn btn-primary">
				</div>
			</div>

		</form>
	</cfif>

