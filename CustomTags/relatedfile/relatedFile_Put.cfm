<!--- �Relayware. All Rights Reserved 2014 --->
<!---

this outputs the form for doing the upload

2009/05/19	NJH	P-SNY069 Added phrasePrefix to the translations and changed some text to be phrases.
2009/07/10	NJH	P-FNL069 Added file extension verification
2009/09/30 	NJH LID 2710 - only put files into this array if you have admin Task level 3 or if you uploaded the file
2011/08/17 	PPB REL109 hooks for restyling
2014/06/25 	SB Removed all tables and added more hooks and reponsive Bootstrap
2104-11-07 PPB P-KAS049 removed the '(one)' or '(many)' extension on the descriptions in the File category dropdown
2015-11-27 DN/SB Case 446402 - improving replace question layout
04/02/2016 DAN Case 447930 - rename submit button id to not override submit function (fix for form.submit is not a function)
2016/02/09	NJH BF-210 BF-210 - removed the 'replace allowed' radio buttons as they cause confusion.
--->

<!--- NJH 2009/07/10 P-FNL069 - file upload verification. JS file to check the file extension --->
<cfinclude template="verifyFileUpload.cfm">


 	<cfparam name="caller.RFFormIndex" default="0">
	<cfset caller.RFFormIndex = caller.RFFormIndex + 1>
	<!---
	NJH 2009/09/30 - removed the edit function as a) it wasn't working as it didn't submit the form, and b) files can be managed
	by deleting and uploading files
	--->
	<cfif attributes.action contains "list" and not isDefined("caller.RFUpdateCodeExists")>
		<script language="JavaScript">

			var records = new Array()

			// array record locator

			function recordFind(afileID)
			{
				for (i=0; i<records.length; i++)
				{
					if (records[i].FileID == afileID)
					break
				}
				return i
			}


			// array constructor

			function record(FileID, Filename, Description, LanguageID, FileCategoryID, FilePath, PrefixFile, SingleFile)
			{
				this.FileID = FileID
				this.Filename = Filename
				this.Description = Description
				this.LanguageID = LanguageID
				this.FileCategoryID = FileCategoryID
				this.FilePath = FilePath
				this.PrefixFile = PrefixFile
				this.SingleFile = SingleFile
			}
			// load the array - need to replace any double quote characters with a different character - using ^
			<cfoutput query="FileList">
				<!--- NJH 2009/09/30 LID 2710 - only put files into this array if you have admin Task level 3 or if you uploaded the file --->
				<cfif application.com.login.checkInternalPermissions("adminTask","Level3") or UploadedBy eq request.relayCurrentUser.personID>
			records[records.length] = new record(#jsStringFormat(FileID)#, "#jsStringFormat(Filename)#", "#rereplace(Description,chr(34),"^","ALL")#", #iif(LanguageID is "", de(0), de(LanguageID))#, #jsStringFormat(FileCategoryID)#, "#jsStringFormat(FilePath)#", #jsStringFormat(PrefixFile)#, #jsStringFormat(SingleFile)#)
				</cfif>
			</cfoutput>

			function setselectedIndex(theValue, theSelect)
			{
				for (i=0; i<theSelect.length; i++)
				{
					if(theValue == theSelect.options[i].value)
					{
						theSelect.selectedIndex = i
						break
					}
				}
			}

			// function to allow an existing record to be edited
			function edit(afileID)
			{
				var i = recordFind(afileID)
				document.upload.RelatedFile_FileID.value = afileID
				document.upload.RelatedFile_description.value = records[i].Description.replace(/\^/g, '"')
				setselectedIndex(records[i].LanguageID, document.upload.RelatedFile_language) <!--- NJH 2016/09/08 JIRA PROD2016-1430 - capitalise the R --->
				setselectedIndex(records[i].FileCategoryID, document.upload.RelatedFile_FileCategoryID)
				document.upload.RelatedFile_SubmitButton.value = "Update"
				document.upload.RelatedFile_frmAction.value = "Update"
			}

		</script>
		<cfset caller.RFUpdateCodeExists = true>
	</cfif>
	<!--- this is where the form is displayed which allows a user to upload --->

	<!--- Check that the minimum number of parameters for this action have been supplied. --->
	<cfif not (isDefined("attributes.Entity") and isDefined("attributes.EntityType"))>
		Error: To display the put form, you must define the entity and entity type (i.e. the table the entity is from
		you'll be uploading related files for).
		<cfexit>
	</cfif>

	<cfif not isDefined("caller.RFCheckFormExists")>
		<script language="Javascript">

			// function to see how many files with the same filename and type exist
			function countInstances(afileID)
			{
				var recordcount = 1
				var recidx = recordFind(afileID)
				for (i = 0; i < records.length; i++)
				{
					if (i != recidx && records[i].Filename == records[recidx].Filename && records[i].FilePath == records[recidx].FilePath)
					{
						recordcount++
					}
				}
				return recordcount
			}

			// check that the record doesn't do anything naughty to any records in existance...
			function updateAllowed(afileID)
			{
				var retval = true
				// make sure that the records array exists - if not, that is actually allowed - cos you can
				// just do put action without list when calling the tag
				if (records.length == null)
				{
					retval = true
				}
				else
				{
					if(records.length == 1)
					{
						retval = true // if theres only one, no harm can be done!
					}
					else
					{
						// need to make sure were not treading on any toes here.
						// if this row is not the row we're updating and the row has the same file category
						// and is a singlefile type category, cannot continue.
						for (i=0; i<records.length; i++)
						{
							// scenario: single file getting in the way of a different single file already in existance
							if (afileID != records[i].FileID
								&& records[i].SingleFile == true
								&& records[i].LanguageID == document.upload.RelatedFile_language.options[document.upload.RelatedFile_language.selectedIndex].value
								&& records[i].FileCategoryID == document.upload.RelatedFile_FileCategoryID.options[document.upload.RelatedFile_FileCategoryID.selectedIndex].value)
							{
								alert("Sorry, but the update you are attempting to make would result in another related file being overwritten. You need to delete that file first if you really wish to do this.")
								retval = false
								break
							}
							// scenario: many files allowed. make sure the filename does not match a filname from the same category,
							// language. PrefixFile may be true or false, so need to account for that.
							if (document.upload.RelatedFile_filename.value.length > 0
								&& afileID != records[i].FileID
								&& records[i].SingleFile == false
								&& records[i].LanguageID == document.upload.RelatedFile_language.options[document.upload.RelatedFile_language.selectedIndex].value
								&& records[i].FileCategoryID == document.upload.RelatedFile_FileCategoryID.options[document.upload.RelatedFile_FileCategoryID.selectedIndex].value)
							{
								// does it end with the same filename? Check FilePrefix to see whether to strip some chars from
								// filename
								if (records[i].PrefixFile == true)
									tmpstr = records[i].Filename.slice(records[i].PrefixFile.indexOf("_"))
								else
									tmpstr = records[i].Filename

								regexp = new RegExp(tmpstr + "$")
								if (regexp.test(document.upload.RelatedFile_filename.value))
								{
									alert("Sorry, but the update you are attempting to make would result in another related file being overwritten. You need to delete that file first if you really wish to do this.")
									retval = false
									break
								}
							}
						}
					}
				}
				return retval
			}

			function checkForm(aform)
			{
				<!--- NJH 2009/07/10 P-FNL069 - check that the file has a valid extension --->
				if (aform.RelatedFile_filename.value.length > 0) {
					if (!verifyFile(aform.RelatedFile_filename.value)) {
						return false;
					}
				}

				// first check whether they're uploading or updating
				if (aform.RelatedFile_SubmitButton.value == "Update")
				{
					if (updateAllowed(aform.RelatedFile_FileID.value))
					{
						if (confirm("If you're sure you want to replace information for the file you clicked on click OK. Otherwise click Cancel. If you want to clear all the information from the form, click Reset."))
						{
							if (countInstances(aform.RelatedFile_FileID.value) > 1)
							{
								// need to get the user to decide whether they are going to update all the existing references to the
								// file or whether they will be just doing the one and keeping the other file at the same time.
								if (confirm("The file you've chosen to update is referenced more than once. To update the other references to reflect the new file, click OK. To only update the information for this file, click Cancel - the file you are updating from will be kept to maintain the validity of the other references."))
									aform.RelatedFile_UpdateAll.value = 1
							}
							// WAb not needed aform.RelatedFile_actualfilename.value = aform.RelatedFile_filename.value
							return true
						}
						else
							return false
					}
					else
						return false
				}
				else
				{
					// make sure at least the filename field has been completed.
					if (aform.RelatedFile_filename.value.length > 0)
					{
						// wab not needed aform.RelatedFile_actualfilename.value = aform.RelatedFile_filename.value
						return true
					}
					else
					{
						alert("You must at least provide a filename and a File Category to upload!")
						return false
					}
				}

			}

		</script>
		<cfset caller.RFCheckFormExists = true>
	</cfif>

	<!--- display the form --->
	<cfparam name="cgi.script_name" default="">
	<cfparam name="request.query_string" default="">

	<!--- <cfif true or attributes.replaceAllowed or FileList.recordCount eq 0 or not fileList.singleFile[1]> --->
	<div id="relatedFilePutTable" class="screenTable">

		<cfoutput>
			<form name="upload" action="#cgi.script_name#?#request.query_string#" method="post" enctype="multipart/form-data" onSubmit="return checkForm(this)" onReset="RelatedFile_SubmitButton.value='Upload'; RelatedFile_frmAction.value='put'">
				<input type="hidden" name="RelatedFile_frmAction" value="put">
				<CF_INPUT type="hidden" name="RelatedFile_entity" value="#attributes.entity#">
				<CF_INPUT type="hidden" name="RelatedFile_entitytype" value="#attributes.entitytype#">
				<input type="hidden" name="RelatedFile_delFileID" value="">
				<input type="hidden" name="RelatedFile_FileID" value="">
<!--- not needed WAB				<input type="hidden" name="RelatedFile_actualfilename"> --->
				<input type="hidden" name="RelatedFile_UpdateAll" value="0">
		<!--- </cfoutput> --->

		<!--- output the fields that were included in the calling template as hidden fields --->

		<cfloop list="#passedformfields#" delimiters="|" index="afield">
			<!--- <cfoutput> ---><CF_INPUT type="hidden" name="#listfirst(afield,"=")#" value="#listrest(afield,"=")#"><!--- </cfoutput> --->
		</cfloop>


		<cfif attributes.DescriptionLength is "Short">
			<!--- <div class="row" id="relatedFileShowMsgIOS">
				<div class="col-xs-12">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadLabel</h3>
					<p>phr_relatedFile_relatedFileShowMsgIOS</p>
				</div>
			</div> --->
			<div class="row hideIOS">
				<div id="fileToUploadColRF" class="col-xs-12 col-sm-6 equalHeight">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadLabel</h3>
					<div class="Margin">
						phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadTextShort
					</div>
					<div class="relatedFileFormEl">
						<input type="file" name="RelatedFile_filename" size="30" id="chooseFile" class="btn btn-primary">
					</div>
				</div>
				<div id="descriptionColRF" class="col-xs-12 col-sm-6 equalHeight">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#DescriptionLabel</h3>
					<div class="Margin">
						phr_RelFile_#htmleditformat(attributes.phrasePrefix)#DescriptionText
					</div>
					<div class="relatedFileFormEl">
						<input type="text" name="RelatedFile_description" size="30" maxlength="255" id="RelatedFile_description">
					</div>
				</div>
			</div>
		<cfelse>
			<div class="row hideIOS">
				<div id="fileToUploadColRF" class="col-xs-12 col-sm-6 equalHeight">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadLabel</h3>
					<div class="Margin">
						phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadTextLong
					</div>
					<div class="relatedFileFormEl">
						<input type="file" name="RelatedFile_filename" size="30" id="chooseFile" class="btn btn-primary">
					</div>
				</div>
				<div id="descriptionColRF" class="col-xs-12 col-sm-6 equalHeight">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#DescriptionLabel</h3>
					<div class="Margin">
						phr_RelFile_#htmleditformat(attributes.phrasePrefix)#DescriptionTextLong
					</div>
					<div class="relatedFileFormEl">
						<textarea name="RelatedFile_description" cols="30" rows="6" id="RelatedFile_description"></textarea>
					</div>
				</div>
			</div>
		</cfif>

		<cfif attributes.showLanguage or attributes.showFileCategoryDescription>

		</cfif>
			<div class="row hideIOS">
			<cfif attributes.showLanguage>
			<div id="languageColRF" class="relatedFile_section col-xs-12 col-sm-6 equalHeight">
				<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#LanguageLabel</h3>
				<div class="Margin">
					phr_RelFile_#htmleditformat(attributes.phrasePrefix)#LanguageText
				</div>
				<div class="relatedFileFormEl">

					<select name="RelatedFile_language" id="RelatedFile_language">
						<option value="0">phr_None
						<cfloop query="languages">
							<option value="#languageid#">#htmleditformat(Language)#
						</cfloop>
					</select>
				</div>
			</div>
			<cfelse>
				<input type="hidden" name="relatedFile_language" value="0">
			</cfif>

			<cfif FileCategoryList.recordcount gt 1 and attributes.showFileCategoryDescription>
			<div id="fileCategoryColIdRF" class="relatedFile_section col-xs-12 col-sm-6 equalHeight">
				<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#FileCategoryLabel</h3>
				<div class="Margin">
					phr_RelFile_#htmleditformat(attributes.phrasePrefix)#FileCategoryText
				</div>
				<div class="relatedFileFormEl">
					<select name="RelatedFile_FileCategoryID" id="RelatedFile_FileCategoryID">
						<cfloop query="FileCategoryList">
							<option value="#FileCategoryID#" <cfif currentrow is 1 >selected</cfif>>#htmleditformat(FileCategoryDescription)# 	<!--- (<cfif SingleFile>phr_one<cfelse>phr_many</cfif>) ---> <!--- 2104-11-07 PPB P-KAS049 removed the '(one)' or '(many)' extension  --->
							<!--- wab added code to select the first entry by default--->
						</cfloop>
					</select>
				</div>
			</div>
			<cfelse>
				<CF_INPUT type="hidden" name="RelatedFile_FileCategoryID" value="#FileCategoryList.filecategoryID[1]#">
			</cfif>
			</div>
		<cfif attributes.showLanguage or attributes.showFileCategoryDescription>

		</cfif>
			<div class="row hideIOS">
				<div id="uploadTheFileRF" class="relatedFile_section col-xs-12 col-sm-12">
					<h3>phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadTheFileLabel</h3>
					<div class="margin">
							phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadTheFileText
						<cfif attributes.replaceallowed>
							phr_RelFile_#htmleditformat(attributes.phrasePrefix)#UploadFTRepAllowed
						</cfif>
					</div>
					<div class="relatedFileFormEl form-group">
						<!--- NJH 2016/02/09 BF-210 - don't show this part of the form... causing confusion... may have to re-instate it?????
						<cfif attributes.replaceallowed>
						<!--- start: 2015-11-27 DN/SB Case 446402 --->
                        <p><b>phr_relFile_Replace</b></p>
						<label for="yes" class="radio-inline"><input type="radio" name="RelatedFile_ReplaceAllowed" id="yes" value="yes" checked> phr_Yes</label>
						<label for="no" class="radio-inline"><input type="radio" name="RelatedFile_ReplaceAllowed" id="no" value="no"> phr_No</label>
						<!--- end: 2015-11-27 DB/SB Case 446402 --->
						<cfelse>
						<input type="hidden" name="RelatedFile_ReplaceAllowed" value="no">
						</cfif> --->
						<input type="hidden" name="RelatedFile_ReplaceAllowed" value="#attributes.replaceallowed#">
						<input type="reset" value="phr_relFile_reset" id="reset" class="btn btn-primary">
						<input type="submit" name="RelatedFile_SubmitButton" value="phr_relFile_Upload" id="btnSubmit" class="btn btn-primary"> <!--- Case 447930 rename id --->
					</div>
				</div>
			</div>

		</form>
		</cfoutput>
	</div>
<!--- </cfif> --->
