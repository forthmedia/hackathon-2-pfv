<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
countryScopeTask



WAB 2005-09-27    now returns value caller.UGRecordManager_UpdatesDone   (true/false) to say whether an update has been done
WAB		March 2006	altered the countryscope table to have a permission column along the lines of the recordrights table
2015-12-01	WAB PROD2015-290 Visibility Project.  Replaced queries with a single sp 

 --->


<cfset updateDone = false>	

<CFIF isDefined("caller.FieldNames")> 

	<CFLOOP index="thisField" list = "#caller.FieldNames#">
	<!--- loop through all the form fields looking for variables of the form CS_#Entity#_#EntityID#_#level#_orig --->

		<CFIF left(thisField,3) is "CS_" and listLen(thisField,"_") is 5 and right(thisField,5) is "_orig">

			<CFSET thisEntityType = listGetAt(thisField,2,"_")>
			<CFSET thisEntityID = listGetAt(thisField,3,"_")>
			<CFSET thisLevel = listGetAt(thisField,4,"_")>

	
			<CFSET previousCountries = evaluate(thisField)>
			<CFIF isDefined("CS_#thisEntityType#_#thisEntityID#_#thisLevel#")>
				<CFSET currentCountries = evaluate("CS_#thisEntityType#_#thisEntityID#_#thisLevel#")>
			<CFELSE>
				<CFSET currentCountries = "">
			</cfif>
		
			<CFIF not isNumeric(thisEntityID)>
				<CFSET thisEntityID = evaluate("#thisEntityID#")>
			<!--- NJH 2013/04/02 - added this so that we could add country scope when adding a record, rather than when a record has already been added. --->
			<cfelseif structKeyExists(caller,"countryScopeEntityID") and thisEntityID eq 0>
				<CFSET thisEntityID = caller.countryScopeEntityID>
			</cfif>


			<CFQUERY NAME="setCountryScope" DATASOURCE="#Application.SiteDataSource#">
			declare 
					  @countriesAdded varchar(max) = ''  
					, @countriesRemoved varchar(max) = '' 

			exec setCountryScope
				  @entity = <cf_queryparam value="#thisEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				, @entityid = <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				, @level = <cf_queryparam value="#thisLevel#" CFSQLTYPE="CF_SQL_INTEGER" >
				, @countryIDList  = <cf_queryparam value="#CurrentCountries#" CFSQLTYPE="CF_SQL_VARCHAR"  >
				, @previousCountryIDList  = <cf_queryparam value="#PreviousCountries#" CFSQLTYPE="CF_SQL_VARCHAR"  >
				, @countriesAdded = @countriesAdded OUTPUT 
				, @countriesRemoved = @countriesRemoved OUTPUT 

			select @countriesAdded as countriesAdded, @countriesRemoved as countriesRemoved
			</CFQUERY>

			<cfif setCountryScope.countriesAdded is not "" OR setCountryScope.countriesRemoved is not "" >
				<cfset updateDone = true>
			</cfif>

	
		</CFIF>
	</cfloop>
</cfif> 

<cfset caller.CountryScope_UpdatesDone = updateDone>

