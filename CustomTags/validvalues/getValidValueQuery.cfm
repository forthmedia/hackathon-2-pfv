<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- get valid values for a given fieldname (variable of form fieldsource_fieldtextid) --->
	<!--- returns a query called validValues--->


<!--- Mods:
2000-10-19 WAB this used to be a tag, but changed back to an include to increase performance and since only used in one place
		also reworked so that it returns a query rather than a list
2000-01-06	reworked the way that lookups work - query defined in the valid value database
2007-02-07	WAB  added a special cfcatch which sets the cfcatch message to "Valid Value Error" plus the original message, can be caught in calling templates
2007/02/28      WAB   enablecfoutputonly required in the catch
2007/11/13    WAB moved position of the catch
2008/04/09    WAB added support for attributes.entityid being passed
 --->		
	

<CFPARAM name="validFieldName">
<CFPARAM name="parentValidFieldName" default="">
<CFPARAM name="invalidValues" default="">
<CFPARAM name="countryid" default="0">




		<CFSET lookupvalue = "">			<!--- tells query to bring back all values, not single value --->
		<CFINCLUDE template = "qryvalidvalue.cfm">
		<!---  runs query getvalidvalues--->	

		<CFIF getValidValues.recordCount IS 0 and parentValidFieldName is not "">
			<!--- if no valid values then, have a look for valid values inherited from a parent --->
			<CFSET validFieldName = parentValidFieldName>

			<CFINCLUDE template = "qryvalidvalue.cfm">
			<!---  runs query getvalidvalues--->		
	
		</cfif>



		<CFIF getValidValues.displayvalue is "*lookup">		
		
			<!--- valid values need looking up --->
			<CFSET thisquery = listFirst(getValidValues.datavalue,"|") >
			<cfif isDefined("attributes.entityid") and attributes.entityid is not "">  <!--- WAB 2008/04/09 I found that I was calling these tags with an entityID attribute but had never coded it in here - oops, so now added in the code to deal--->
				<cfset entityID = attributes.EntityID>
			<cfelseif isDefined("caller.EntityID")>  <!--- WAB 2006-05-22 added this so that #entityID# can be referenced in the query - needed now that this is always called as a customtag--->
				<cfset entityID = caller.EntityID>
			</cfif>
			<cftry>
				<CFSET thisQuery = evaluate('"#thisquery#"')>
				
				<!---  runs another query getvalidvalues--->	
				<cFQUERY NAME="getValidValues" DATASOURcE=#application.sitedataSource# >
				#preservesinglequotes(thisQuery)#
				</CFQUERY>	

				<cfcatch>  <!--- WAB 2007/11/13 moved this catch to include the cfquery as well as the evaluate --->
					<cfsetting enablecfoutputonly="no" > <!--- to reverse out the "yes" at beginning of tag --->
					<cfthrow message = "Valid Value Error: #cfcatch.message# . Field: #validFieldName#"  >
				</cfcatch>
			</cftry>	


			<!--- need to remove any invalid values if these have been defined--->
			<CFIF invalidvalues is not "">
					<!--- need to remove values which aren't allowed 
						bit of a problem since there isn't a queryDeleteRow function.
						For the moment I will blank out the data and display columns
					--->

					<CFLOOP query="getValidValues" >
					
						<CFIF listFindNoCase(invalidValues, datavalue) is not 0>
							<CFSET x = querySetCell(getValidValues,"dataValue","",currentRow)>
							<CFSET x = querySetCell(getValidValues,"displayValue","",currentRow)>
						</cfif>
						
					</cfloop>

			</CFIF>
			



		</CFIF>

		
<CFSET validvalues = getvalidvalues>	
	

