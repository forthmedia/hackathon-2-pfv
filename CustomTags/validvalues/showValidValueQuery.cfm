<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes" > 
<!--- displays a dropdown or radio buttons of valid values --->
<!--- requires variables: formFieldName  (name of form field
							displayas  (blank defaults to dropdown list)
							ValidValueList
							currentvalue
							allowcurrentvalue
							showNull allowNull	
--->

<!--- AMENDMENT HISTORY

Modification of showValidValueList which uses a query rather than an oddly delimited list

WAB :  2000-10-18


WAB: 2001-07-02 added displayas = "singlecheckbox", useful for displaying a single checkbox (you will need to set a single valid value (often = 1)), 
WAB 2004-06-09	fixed some bugs
WAB 2004-18-08	added the form attribute when calling  CF_TwoSelectsComboQuery
WAB 2006-06-21  Added support for CFFORM on select boxes
WAB 2007-02-20  support for multiple current values
2009-06-02 	NYB LHID2300 - not translating 'Select a value for ' in Javascript popup, requiredMessage now passed in from displayValidValues
			Also the non cfform select box was not creating any javascript validation
2009-10-27	NAS LID - 2737 Mod code to make 'Select a value' translateable in drop down lists.
2011-06-21	NYB	P-LEN024 - added the ability to disable fields
2011/10/03	WAB having <cfselect enabled=false> doesn't disable the select box, so have changed to to use disabled
			don't really see the point of having both an enabled and a disabled attribute, rest of the sytem uses disabled and this tag hasn't been used yet with the enabled attribute, so I am going to remove it
2011/12/05	WAB added _required fields to radio/checkbox and gave them to have onSubmit functions
2012-01-26 	NYB Case:426292 added label attribute to select
2013/07/02	IH Case 436014 remove javaScriptVerification
2013-07-11 	NYB Case 436087 when a field is disabled change the id/name to #original#_disabled then created a hidden field with id/name eq'g original
2015-03-09	AHL Case 443967 My Company Profile - Edit Location Details - Error

 --->

<!--- START: NYB LEN024 2011-06-17 --->
<CFPARAM NAME="attributes.disabled" default="false">
<!--- END: NYB LEN024 2011-06-17 --->

<!--- START: 2013-07-11 NYB Case 436087 added: --->
<cfif attributes.disabled>
	<cfset variables.formFieldID_orig = variables.formFieldID>
	<cfset variables.formFieldName_orig = variables.formFieldName>
	<cfset variables.formFieldID = "#variables.formFieldID#_disabled">
	<cfset variables.formFieldName = "#variables.formFieldName#_disabled">
</cfif>
<!--- END: 2013-07-11 NYB Case 436087 --->

<CFIF allowNewValue is 1>  
	<cfoutput>
	<cf_translate encode="javascript">
	<cf_includeJavascriptOnce template="/javascript/newSelectValue.js">
	<script>
		function newValue(thisObject) {
			if (thisObject[thisObject.selectedIndex].value == '*') {

				newSelectValue (thisObject,'#jsStringFormat(NewValuePopUpText)#')
		
			}		
		}
		
	
	</script>
	</cf_translate>
	</cfoutput>
</cfif>

<cf_translate>
		<!--- if current value is allowed, but it is not in the valid value list, then add it to end 
			WAB 2007-02-20 needed to alter this to handle possibility of currentValue actually being a list of values - 
		--->
		<CFIF allowcurrentvalue is true and currentvalue is not ""> 
				<cfset tempListOfDataValues = arrayToList(validValues[attributes.dataValueColumn])>
			<cfloop index = "thisCurrentValue" list = "#currentValue#">
				<cfif listFindNoCase(tempListOfDataValues, thisCurrentValue) IS 0>
					<CFSET row= queryAddrow(validValues,1)>
					<CFSET x = querySetCell(validValues, attributes.displayValueColumn, thisCurrentValue,row)>
					<CFSET x = querySetCell(validValues, attributes.dataValueColumn, thisCurrentValue,row)>
				</cfif>
			</cfloop>
		</CFIF>

			<CFIF attributes.method is "edit">
			
						<CFIF displayas is "radio">

							<CFSET c = 0>
							<CFSET widtha = Round((100 / column)-(10/column))>
							<CFSET widthb = Round((100 - (column * widtha))/column)>

							<cFOUTPUT>
							
							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TABLE class="booleanflag">
									<CFIF showNull is true>
										<CFPARAM name = "nullText" default="none">
										<CFSET c = c + 1>
										<CFIF c MOD column IS 1><TR></CFIF>
										<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="#widthb#%"><CF_INPUT TYPE="RADIO" NAME="#formFieldName#" ID="#formFieldID#" VALUE=""  disabled="#iif( attributes.disabled,true,false)#" > </TD>
										<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#htmleditformat(nullText)#</TD>
										<CFIF c MOD column IS 0></TR></CFIF>
									</CFIF>
							
								<CFLOOP query="validValues">
									<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]>
									<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]>
	
									<CFSET c = c + 1>
									<CFIF c MOD column IS 1><TR></CFIF>
									<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="#widthb#%"><CF_INPUT TYPE="RADIO" NAME="#formFieldName#" VALUE="#dataValue#" checked="#iif( dataValue IS  trim(currentValue),true,false)#"  > </TD>
									<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#htmleditformat(displayValue)#</TD>
									<CFIF c MOD column IS 0></TR></CFIF>
								</CFLOOP>
									<CFIF c MOD COLUMN IS NOT 0>
										<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))# >&nbsp;<BR></TD></TR>
									</CFIF>
	
									
								</TABLE>
							<cfelse>
								<CFIF showNull is true>
									<CFPARAM name = "nullText" default="none">
									<div class="radio col-xs-12 col-sm-6 col-md-4">
										<label>
											<CF_INPUT TYPE="RADIO" NAME="#formFieldName#" ID="#formFieldID#" VALUE=""  disabled="#iif( attributes.disabled,true,false)#" >
											#htmleditformat(nullText)#
										</label>
									</div>
								</CFIF>
							
								<CFLOOP query="validValues">
									<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]>
									<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]>
									<div class="radio col-xs-12 col-sm-6 col-md-4">
										<label>
											<CF_INPUT TYPE="RADIO" NAME="#formFieldName#" VALUE="#dataValue#" checked="#iif( dataValue IS  trim(currentValue),true,false)#"  >
											#htmleditformat(displayValue)#
										</label>
									</div>
								</CFLOOP>
							</cfif>
							</cFOUTPUT>		

							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->									
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#formFieldName#,'\n#requiredMessage#','1');" >
								<cfoutput><CF_INPUT type="hidden" name = "#formFieldName#_required" submitFunction="verifyObjectv2(form['#formFieldName#'],'#REQUIREDmessage#','#required#')" ></cfoutput>	<!--- 2015-03-09 AHL Case 443967 My Company Profile - Edit Location Details - Error --->

							</cfif>
							
						
						<CFELSEIF displayas is "dropdown" OR displayas is "SELECT" or displayas is "" or displayas is "MultiSelect">	


							<cfif not attributes.useCFFORM>
								
								<!--- START: 2012-01-26 NYB Case:426292 added: --->
								<cfif not structkeyexists(attributes,"label")>
									<cfif structkeyexists(attributes,"name")>
										<cfset attributes.label = "phr_"&formFieldName>
									<cfelse>
										<cfset attributes.label = "">
									</cfif>
								</cfif>
								<!--- END: 2012-01-26 NYB Case:426292 --->
	
								<cFOUTPUT>
									
									<!--- NYB LEN024 2011-06-17 - added disabled to SELECT --->
									<!--- 2012-01-26 NYB Case:426292 added label to SELECT --->
									<SELECT ID="#formFieldID#" NAME="#formFieldName#" SIZE="#listsize#" <cfif attributes.class neq "">class="#attributes.class#"</cfif> <cfif attributes.style neq "">style="#htmlEditFormat(attributes.style)#"</cfif> <cfif required>required = #required# message="#htmleditformat(requiredmessage)#"</cfif> <CFIF displayas is "multiselect">Multiple</cfif> <CFIF onChange is not "">onchange = "#htmleditformat(onChange)#"</cfif> <cfif allowNewValue is 1>onChange = "javascript:newValue(this.form.#htmleditformat(formFieldName)#)"</cfif> #attributes.passthrough# <cfif attributes.disabled>disabled</cfif> label="#htmleditformat(attributes.label)#">
	
	
										<CFIF showNull is true >  <!--- WAB: did have and displayas is not "MultiSelect", but can't remember exactly why and don't think it is right --->
											<CFPARAM name = "nullText" default="phr_Ext_SelectAValue"> <!--- 2009-10-27	NAS LID - 2737 Mod code to make 'Select a value' translateable in drop down lists. --->
											<OPTION VALUE="" >#htmleditformat(nullText)#</OPTION>
										</CFIF>
	
										<CFIF allowNewValue is 1>  
											<OPTION VALUE="*" >#htmleditformat(NewValueText)#</OPTION>
										</CFIF>
	
										<CFLOOP query="validvalues" >
											<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]> 
											<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]> 
	
											<CFIF not (dataValue is "" and displayValue is "")>
												<OPTION VALUE="#htmleditformat(trim(dataValue))#" <CFIF listfindNoCase(currentValue, trim(dataValue)) IS NOT 0 > SELECTED="TRUE" </CFIF>>#displayValue#</OPTION>
											</cfif>
										</cfloop>
								</SELECT>
								
								</cfoutput>		

								<!--- WAB 2009/06/03 wasn't doing JS validation --->
								<CFIF required is not 0>
									<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#formFieldName#,'\n#requiredMessage#','#required#');" >
								</cfif>


							<cfelse>
								<cfif allowNewValue is 1><cfset onChange = "javascript:newValue(this.form.#formFieldName#)"></cfif>
								<!--- 2009-06-02 NYB LHID2300 added required Message --->
								<!--- NYB LEN024 2011-06-17 added attributes.enabled --->
								<!--- WAB passing enabled=false doesn't disable a select box, 
										so enabled="#attributes.enabled#"  does not work
										need to use an attribute collection containing, if required, a disabled attribute.
								--->
								<cfset attributeCollection = {										
										query="validValues",
										value = #attributes.dataValueColumn#,
										display = #attributes.displayValueColumn#,
										selected = #currentValue#, 
										name="#formFieldName#", 
										size="#listsize#", 
										visible="Yes" ,
										passthrough="#attributes.passthrough#", 
										onchange="#onchange#", 
										required="#required#" ,
										queryposition = "below",
										message = "#requiredMessage#",
										class="#attributes.class#"
										}>
								<cfif attributes.disabled>
									<cfset attributeCollection.disabled = true>
								</cfif>		
								
								<cfselect attributeCollection = #attributeCollection#>

										<cfoutput>
										<CFIF showNull is true >  <!--- WAB: did have and displayas is not "MultiSelect", but can't remember exactly why and don't think it is right --->
											<CFPARAM name = "nullText" default="Select A Value">
											<OPTION VALUE="" >#htmleditformat(nullText)#</OPTION>
										</CFIF>

										<CFIF allowNewValue is 1>  
											<OPTION VALUE="*" >#htmleditformat(NewValueText)#</OPTION>
										</CFIF>
										
										</cfoutput>
									  							
								</cfselect> 
							
							</cfif>

						<CFELSEIF displayas is "checkbox" >	

							<CFSET c = 0>
							<CFSET widtha = Round((100 / column)-(10/column))>
							<CFSET widthb = Round((100 - (column * widtha))/column)>

							<cFOUTPUT>	
							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TABLE class="booleanflag">
									<CFLOOP query="validvalues" >
	
										<cfset dataValue = validValues[attributes.dataValueColumn][currentRow]>
										<cfset displayValue = validValues[attributes.displayValueColumn][currentRow]>
	
										<CFIF not (dataValue is "" and displayValue is "")>
											<CFSET c = c + 1>
											<CFIF c MOD column IS 1><TR></CFIF>
											<TD ALIGN="RIGHT" class="booleanflag" WIDTH="#widthb#%"><INPUT TYPE="checkbox" NAME="#formFieldName#" VALUE="#trim(dataValue)#" <CFIF listfindNoCase(currentValue, trim(dataValue)) IS NOT 0 >checked</CFIF> <cfif attributes.disabled>disabled</cfif> >  </TD>
											<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#htmleditformat(displayValue)# </TD>
											<CFIF c MOD column IS 0></TR></CFIF>
										</CFIF>
									</CFLOOP>
				
									<CFIF c MOD COLUMN IS NOT 0>
										<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))# >&nbsp;<BR></TD></TR>
									</CFIF>
	
								</TABLE>
							<cfelse>
								<CFLOOP query="validvalues" >
									<cfset dataValue = validValues[attributes.dataValueColumn][currentRow]>
									<cfset displayValue = validValues[attributes.displayValueColumn][currentRow]>

									<CFIF not (dataValue is "" and displayValue is "")>
										<div class="checkbox col-xs-12 col-sm-6 col-md-4">
											<label>
												<INPUT TYPE="checkbox" NAME="#formFieldName#" VALUE="#trim(dataValue)#" <CFIF listfindNoCase(currentValue, trim(dataValue)) IS NOT 0 >checked</CFIF> <cfif attributes.disabled>disabled</cfif> >
												#htmleditformat(displayValue)#
											</label>
										</div>
									</CFIF>
								</CFLOOP>
							</cfif>
							</cFOUTPUT>		
							
							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#formFieldName#,'\n#requiredMessage#','1');" >
								<cfoutput><CF_INPUT type="hidden" name = "#formFieldName#_required" submitFunction="verifyObjectv2(form['#formFieldName#'],'#REQUIREDmessage#','#required#')" ></cfoutput>	<!--- 2015-03-09 AHL Case 443967 My Company Profile - Edit Location Details - Error --->
						
							</cfif>
						<CFELSEIF displayas is "singlecheckbox" >	
							<!--- designed to display a single check box with no adornments such as tables --->
								<CFLOOP query="validvalues" >

									<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]>
									<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]>

									<CFIF not (dataValue is "" and displayValue is "")>
										<CFOUTPUT>
										<INPUT TYPE="checkbox" ID="#formFieldID#" NAME="#formFieldName#" VALUE="#trim(dataValue)#" <CFIF listfindNoCase(currentValue, trim(dataValue)) IS NOT 0 >checked</CFIF> <cfif attributes.disabled>disabled</cfif> >
										#displayValue# 
										</cfoutput>
									</CFIF>
								</CFLOOP>
			


							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#formFieldName#,'\n#requiredMessage#','1');" >
							</cfif>


						<CFELSEIF displayas is "twoselects"	>
						

								<!--- the TwoSelectsCombo tag takes two queries, so I need to convert my lists into queries containing the correct items (in the correct order) --->
								<!--- create a query for the selected items, the same length as the number of selelected items --->
								<!--- NJH 2013/03/28 - had to add the datatype, as a qoq was falling over with type not defined in twoSelectsComboQuery --->
								<CFSET selected = queryNew("column1,column2","varchar,varchar")>
								<CFIF currentValue is not "">
									<CFSET x = queryAddRow(selected,listlen(currentValue))>
								</cfif>
								<!--- create a blank query for the non-selected items--->
								<CFSET notSelected = queryNew("column1,column2","varchar,varchar")>

										<!--- loop through putting the correct items in each query --->

										<CFLOOP query="validvalues" >

											<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]>
											<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]>


											<CFIF not (dataValue is "" and displayValue is "")>
							
												<CFIF listfindnocase(currentValue, dataValue) is not 0 > 
													<!--- insert into selected query (inthe correct order) --->
													<CFSET x = querySetCell(selected,"column1",dataValue,listfindnocase(currentValue, dataValue))>
													<CFSET x = querySetCell(selected,"column2",displayValue,listfindnocase(currentValue, dataValue))>
																			
												<CFELSE>
													
													<CFSET x = queryAddRow(notSelected)>
													<CFSET x = querySetCell(notSelected,"column1",dataValue)>
													<CFSET x = querySetCell(notSelected,"column2",displayValue)>
											
												
												</CFIF> 
											</CFIF>
												
										</CFLOOP>
									
										
								
								  <CF_TwoSelectsComboQuery
								    NAME="List_#formFieldName#"
								    NAME2="#formFieldName#"
								    SIZE="6"
								    WIDTH="150px"
								    FORCEWIDTH="50"
									QUERY1="selected"
									QUERY2="notselected"
								    CAPTION="<FONT SIZE=-1><B>Here Are Your Choices:</B></FONT>"
								    CAPTION2="<FONT SIZE=-1><B>Make Your Selections:</B></FONT>"
									UP="Yes" 
									DOWN="Yes"
									FORMNAME = "#Attributes.formName#"
									disabled = #attributes.disabled#
									required="#attributes.required#"
									>  <!--- WAB 2011/04/2011 I added support for disabled here, but it does not work--->					
							
						</CFIF>


						<CFIF trim(jsVerify) is not "">

								<CFSET javascriptVerification = javascriptVerification & "msg = msg + #jsVerify#;" >									
									
						</cfif>							

						<!--- START: 2013-07-11 NYB Case 436087 moved to below: ---
						<CFIF attributes.keepOrig>
							<cFOUTPUT>
							<CF_INPUT TYPE="Hidden" NAME="#formFieldName#_orig" VALUE="#trim(CurrentValue)#" >
							</cFOUTPUT>
						</CFIF>
						!--- END: 2013-07-11 NYB Case 436087 --->

				<CFELSE>
					<!--- view Mode --->
					<CFLOOP query="validvalues" >					

						<cfset dataValue = validvalues[attributes.dataValueColumn][currentRow]>
						<cfset displayValue = validvalues[attributes.displayValueColumn][currentRow]>

						<CFIF trim(dataValue) IS  trim(currentValue)>
							<CFOUTPUT>#trim(DisplayValue)#</cfoutput>
						</CFIF>				
					</CFLOOP>	
				
				</cfif>

<!--- START: 2013-07-11 NYB Case 436087 added: --->
<cfif attributes.disabled>
	<cfset variables.formFieldID = variables.formFieldID_orig>
	<cfset variables.formFieldName = variables.formFieldName_orig>
</cfif>

<CFIF attributes.keepOrig>
	<cFOUTPUT>
	<CF_INPUT TYPE="Hidden" NAME="#formFieldName#_orig" VALUE="#trim(CurrentValue)#" >
	</cFOUTPUT>
</CFIF>

<cfif attributes.disabled>
	<cFOUTPUT>
	<CF_INPUT TYPE="Hidden" NAME="#formFieldName#" VALUE="#trim(CurrentValue)#" >
	</cFOUTPUT>
</cfif>
<!--- END: 2013-07-11 NYB Case 436087 --->

</cf_translate>

<cfsetting enablecfoutputonly="No" > 

