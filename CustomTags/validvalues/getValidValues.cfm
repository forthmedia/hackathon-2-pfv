<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
getValidValues

stub customtag to call getValidValuequery when you want the valid values but not the displaying
useful if using cfselect for the displaying

WAB: 2005-07-06
NYB 2009-06-16 LHID2310 - changed dataValue to cast(dataValue as varchar) as was breaking on 2005 box

2014-03-04 PPB Case 439066 added defence against non-existence of entityDetails.ID
2015-04-01	WAB Deal with field names containing underscores (especially from flagTextIDs). See com.relayForms.convertFieldNameForValidValueTable() for details
 --->
 
 
<CFPARAM NAME="Attributes.validFieldName">
<CFPARAM NAME="Attributes.parentValidFieldName" DEFAULT="">
<CFPARAM NAME="Attributes.countryid" DEFAULT="0">
<CFPARAM NAME="Attributes.returnQuery" DEFAULT="">
<CFPARAM name="attributes.nullText" Default = "phr_pleaseChooseOne">
<CFPARAM name="attributes.showNull" Default = "true">
<CFPARAM name="attributes.delim1" Default = "#application.delim1#">
		
		<!--- 2013-11-08	YMA	Pass in all entityDetails IDs so they are available throughout. --->
		<cfset entityDetails = structFindKey(variables,'entityDetails','one')>

		<cfif not arrayisempty(entityDetails)>
			<cfset entityDetails = entityDetails[1].value>

			<cfif StructKeyExists(entityDetails,"ID")>				<!--- 2014-03-04 PPB Case 439066 added defence against non-existence of entityDetails.ID --->
				<cfloop collection="#entityDetails.ID#" item="ID">
					<cfif isNumeric(ID)>
						<cfset "#application.entityType[ID].uniqueKey#" = #entityDetails.ID[ID]#>
					</cfif>
				</cfloop>
			</cfif>													<!--- 2014-03-04 PPB Case 439066 --->
		</cfif>
 
		<CFSET validFieldName = application.com.relayForms.convertFieldNameForValidValueTable(attributes.validFieldName)>
		<CFSET parentValidFieldName = attributes.parentValidFieldName>
		<CFSET countryid = attributes.countryid>
		<CFSET delim1 = attributes.delim1>

		<CFINCLUDE template = "getValidValueQuery.cfm">
		
		

		<cfif attributes.showNull>
			<!--- add null item  to the beginning of the query --->
			<!--- LID3694: NAS 2010-07-28 added sortorder to keep sortorder as the following query reordered using datavalue --->
			<cfset nulls  = querynew("datavalue,displayvalue,sortorder", "VarChar,VarChar,integer")>
			<cfset queryaddrow(nulls,1)>
			
			<!--- NYB 2009-06-16 LHID2310 - changed dataValue to cast(dataValue as varchar) as was breaking if valid values query did not cast/ convert --->
			<cfquery name="validvalues" dbtype="query">
				select
					<!--- LID3694: NAS 2010-07-28 added sortorder to keep sortorder as the following query reordered using datavalue --->
					'' as dataValue, '#attributes.nulltext#' as displayValue, sortorder from nulls 
			union
				select
					<!--- LID3694: NAS 2010-07-28 added sortorder to keep sortorder as the following query reordered using datavalue --->
					cast(dataValue as varchar), displayValue, sortorder from validvalues
					<!--- LID3694: NAS 2010-07-28 added sortorder to keep sortorder as the following query reordered using datavalue --->
					order by sortorder
			</cfquery>
		</cfif>

		<cfset caller.validvalues = validvalues>
		

