<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
CF_SELECT
WAB 2011/03/28
Code to replace cfselect when a cfform is not available
Should support most of the same attributes, plus a few extra ones!
 --->

<cfif not thistag.hasEndTag or thistag.executionMode is "end">
	<cfset generatedContent = trim(thistag.generatedContent)>
	<cfset thistag.generatedContent = "">
	
	<cfset selectOutput_html = application.com.relayForms.validvalueDisplay(argumentCollection = attributes)>
	
	<cfif generatedContent is not "">
		<cfif not structKeyExists (attributes,"queryPosition") or attributes.queryPosition is "above">
			<cfset selectOutput_html = rereplacenocase(selectOutput_html,"(<select.*?>)","\1#application.com.security.sanitiseHTML(generatedcontent)#")>
		<cfelse>
			<cfset selectOutput_html = rereplacenocase(selectOutput_html,"(</select>)","#application.com.security.sanitiseHTML(generatedcontent)#\1")>
		</cfif>
	</cfif>
	
	<cfoutput>#selectOutput_html#</cfoutput>
</cfif>


