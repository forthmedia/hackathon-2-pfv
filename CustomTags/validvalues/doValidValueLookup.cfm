<!--- �Relayware. All Rights Reserved 2014 

2015-04-01	WAB Deal with field names containing underscores (especially from flagTextIDs). See com.relayForms.convertFieldNameForValidValueTable() for details
--->

<cfparam name="attributes.validfieldname" >
<cfparam name="attributes.lookupvalue" >
<cfparam name="attributes.countryid" default = "0">

<cfset validfieldname =attributes.validfieldname>
<CFSET validFieldName = application.com.relayForms.convertFieldNameForValidValueTable(attributes.validFieldName)>

<cfset countryid =attributes.countryid>
<cfset lookupvalue =attributes.lookupvalue>

<cfinclude template = "doLookup.cfm">

<cfset caller.lookupresult = lookupresult>
