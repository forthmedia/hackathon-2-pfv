<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">
<!---
DisplayValidValues

Code for displaying dropdowns,checkboxes, radio buttons etc

Items to be displayed can be defined in any of the following forms:
	A query
	A list
	A reference to the ValidFieldValues table



Mod History
	Original version (displayValidValuesList.cfm)by WAB a long time ago
	Various modifications over the ages

	Nov - Jan 2000
	Original version couldn't take queries so this functionality added
	Name changed to displayValidValues.cfm so that
		a) It didn't sound list specific
		b) There wouldn't be incompatability problems



Example usage:

<CF_displayValidValue
	formFieldName = "frmcountryid"
	validvalues= "#CountryIDandNameList#"
	nulltext="select A country"
	method = "view"
	onChange = "submit()"
	currentValue = #frmcountryID#
	displayas = "radio"
	>


Mods:


2001-04-02	WAB		Added parameter currentRecord.  Allows passing of a query which defines the current record.  This means that the valid value lookup can refer to values in the current record as #currentRecord.fieldname#
WAB 2006-06-21  Added parameter useCFFORM
WAB/NYB   2009/06/03 LHID2300 mods to the javascript validation message
NAS 	2009/10/27	LID - 2737 Mod code to make 'Select a value' translateable in drop down lists.
2011-06-21	NYB	P-LEN024 - added the ability to disable fields
2011/09/08	PPB P-LEN024 - support bind functionality
2011/12/05	WAB	added test for bind not being null
2015-04-01	WAB Deal with field names containing underscores (especially from flagTextIDs). See com.relayForms.convertFieldNameForValidValueTable() for details
2016-02-01	WAB	Change to call code in relayForms.cfc
				TODO
					Check for use of these variables in calls to cf_displayValidValues
2016-03-09	WAB Change default null text for multiselect (used as placeholder in the plugin)
2016-09-21  WAB PROD2016-2295 - problems with brand new records - where entityID not numeric
 --->


<CFPARAM NAME="Attributes.formFieldName">	<!--- name of form field to be displayed --->
<CFPARAM NAME="Attributes.formFieldID" default = "#Attributes.formFieldName#">	<!--- IDof form field to be displayed --->
<CFPARAM NAME="Attributes.validFieldName" DEFAULT="#application.com.relayForms.convertFieldNameForValidValueTable(attributes.formFieldName)#">  <!--- name to lookup in valid values table --->
<CFPARAM NAME="Attributes.parentValidFieldName" DEFAULT="">  <!--- name to lookup in valid values table --->
<CFPARAM name="attributes.validValues" Default = "">  <!--- list or query of valid values, if not defined then lookup done on validfieldname --->
<CFPARAM name="attributes.inValidValues" Default = ""> <!--- list of values not to use --->
<CFPARAM name="attributes.parameters" Default = "">  <!--- miscellaneous parameters --->
<CFPARAM name="attributes.countryid" Default = "0">
<CFPARAM name="attributes.currentRecord" Default = "">   <!--- query defining the current record - allows lookups to be related to any field in the current record--->
<cfset currentRecord = attributes.currentRecord>
<CFPARAM name="attributes.currentValue" Default = "">
<CFPARAM name="attributes.method" Default = "edit">
<CFPARAM name="attributes.multiple" Default = "false">
<CFPARAM name="attributes.displayas" Default = "#(attributes.multiple)?'multiselect':'select'#"><!--- select, radio, checkbox, multiselect --->
<CFPARAM name="attributes.column" Default = "1"><!--- number of columns for radio buttons and checkboxes --->
<CFPARAM name="attributes.listSize" Default = "1"> <!--- length of multiselects --->
<CFPARAM name="attributes.nullText" Default = "#(attributes.multiple)?'phr_sys_Select_Values':'phr_Ext_SelectAValue'#"> <!--- NAS 	2009/10/27	LID - 2737 Mod code to make 'Select a value' translateable in drop down lists --->
<CFPARAM name="attributes.showNull" Default = "#(attributes.multiple)?false:true#">
<CFPARAM name="attributes.required" Default = "0">
<CFPARAM name="attributes.dataType" Default = "">
<CFPARAM name="attributes.allowCurrentValue" Default = "false">
<CFPARAM name="attributes.allowNewValue" Default = "0">
<CFPARAM name="attributes.NewValueText" Default = "Add a new value">
<CFPARAM name="attributes.NewValuePopUpText" Default = "Enter the new value">
<CFPARAM name="attributes.inputOnNoValidValues" Default = "True"><!--- if no valid values found, revert to a text box --->
<CFPARAM name="attributes.noValidValuesText" Default = "No Valid Values"><!--- text to display on no valid values --->
<CFPARAM name="attributes.delim1" Default = "#application.delim1#"><!--- delimiter between data and display values of validvalue list --->
<CFPARAM name="attributes.delim2" Default = ","><!--- delimiter between valid values --->
<CFPARAM name="attributes.jsVerify" Default = "">
<CFPARAM name="attributes.onChange" Default = "">
<CFPARAM name="attributes.passthrough" Default = "">
<CFPARAM name="attributes.displayValueColumn" default = "displayValue">
<CFPARAM name="attributes.dataValueColumn" default = "dataValue">
<CFPARAM name="attributes.keepOrig" Default = "true">
<CFPARAM NAME="Attributes.formName" default="mainForm">
<CFPARAM NAME="Attributes.useCFFORM" default="false">
<CFPARAM NAME="Attributes.style" default="">
<CFPARAM NAME="Attributes.class" default="form-control">
<CFPARAM name="attributes.requiredMessage" Default = ""> <!--- WAB 2009/06/02 --->
<!--- START: NYB LEN024 2011-06-17 --->
<CFPARAM NAME="attributes.disabled" default="false">
<!--- END: NYB LEN024 2011-06-17 --->



		
		<cfscript>
			map ={formFieldName = "name",formFieldID = "id", displayValueColumn = "display", dataValueColumn = "value", requiredLabel = "label", currentValue = "selected"};
			application.com.structurefunctions.changeStructureKeys (attributes,map);
			
			if (isQuery(attributes.validValues)) {
				attributes.query = attributes.validValues;
				
			} else if (structKeyExists(attributes,"bind") and attributes.bind is not "") {
				// bind OK
				
			} else if (attributes.validValues is not "") {
				// list of items in attributes.validValues, 
				// Note that we are assuming main delimiter is , and data/display delimiter is delim1 - bent pipe
				attributes.query = application.com.relayForms.createQueryFromValidValueList (list = attributes.validValues, valueColumn=attributes.value, displayColumn=attributes.display, delim1 = attributes.delim1);
				
			} else {
				
				// we need to get validvalues from db based on the validFieldName
				
				mergeStruct = {countryid = attributes.countryid };
				if (isDefined("attributes.entityid") and attributes.entityid is not "") {
					mergeStruct.entityid  = attributes.EntityID;
				} else if (isDefined("caller.EntityID")) {
					mergeStruct.entityID = caller.EntityID;
				}
				
				
				// WAB 2016-09-21 PROD2016-2295
				// Problems with brand new records where entityID may not be numeric
				if (structKeyExists (mergeStruct,"entityID") and not isNumeric (mergeStruct.entityID)) {
					mergeStruct.entityID = 0 ;
				}
				
				// 2016-05-17 DAN Case 449543 - fix for populating drop-down values based on flags
				attributes.validFieldName = application.com.relayForms.convertFieldNameForValidValueTable(attributes.validFieldName);

				attributes.query = application.com.relayForms.getValidValuesFromString (string = attributes.validFieldName, excludeValues = attributes.inValidValues, displayColumn = attributes.display, valueColumn = attributes.value, mergeStruct = mergeStruct );
				
			}

			structDelete (attributes, "validValues");


		</cfscript>
		
		<cfoutput>#application.com.relayForms.validValueDisplay(argumentCollection = attributes)#</cfoutput>
		

		<!--- Sadly, need to keep support for screens validation --->
		<cfif structKeyExists (caller, "javascriptVerification") and attributes.required is not 0>
			<!---
			WAB 2009/06/02 mods so that for a required field, either pass in a whole RequiredMessage or a Required Label which is combined with the words "select a value for" (translated) --->
			<CFIF attributes.requiredMessage is "">
				<CFPARAM name="attributes.label">
					<!--- 2009/06/02 NYB LHID2300 - changed "select a value for #attributes.requiredLabel#" to "#phr_ext_SelectAValueFor#"
						therefore:  ext_SelectAValueFor MUST contain form.requiredLabel, eg: Select a value for [[RequiredLabel]]
						created this way to support languages that do not support the above grammatical construct --->
				<cfset attributes.requiredMessage = application.com.relayTranslations.translatePhrase(phrase="ext_SelectAValueFor", mergeStruct = {requiredLabel = attributes.label, label = attributes.label})>
			</cfif>
			
			<CFSET caller.javascriptVerification &= "msg = msg + verifyObjectv2(form.#attributes.name#,'\n#attributes.requiredMessage#','#attributes.required#');" >
		</cfif>


<cfsetting enablecfoutputonly="no" >







<!---  
All this code now replaced with calls to relayForms.cfc

		<CF_EvaluateNameValuePair  NameValuePairs="#attributes.parameters#">

		<!--- any of the parameters (just about) may have been passed as name value pairs, so more params --->
		<CFPARAM name=validFieldName default = #Attributes.validFieldName#>
		<CFPARAM name=displayAs 	default =  #attributes.displayAs#>
		<CFPARAM name=column 		Default = #attributes.column#>
		<CFPARAM name=listSize 	Default = #attributes.listSize#>
		<CFPARAM name=nullText 		Default = #attributes.nullText#>
		<CFPARAM name=showNull 		Default = #attributes.showNull#>
		<CFPARAM name=required		Default = #attributes.required#>
		<CFPARAM name=allowCurrentValue 	Default = #attributes.allowCurrentValue#>
		<CFPARAM name=allowNewValue 	Default = #attributes.allowNewValue#>
		<CFPARAM name=NewValueText 	Default = #attributes.NewValueText#>
		<CFPARAM name=NewValuePopUpText 	Default = #attributes.NewValuePopUpText#>
		<CFPARAM name=inputOnNoValidValues 	Default = #attributes.inputOnNoValidValues#>
		<CFPARAM name=noValidValuesText 	Default = #attributes.inputOnNoValidValues#>
		<CFPARAM name=validValues 	Default = #attributes.ValidValues#>
		<CFPARAM name=inValidValues 	Default = #attributes.inValidValues#>
		<CFPARAM name=jsVerify 	Default = #attributes.jsVerify#>
		<CFPARAM name=requiredMessage  Default = #attributes.requiredMessage#><!--- WAB 2009/06/02 --->
		<CFPARAM name=class  Default = #attributes.class#>


		





		<CFSET formFieldName = attributes.formFieldName>
		<CFSET formFieldID = attributes.formFieldID>
		<CFSET currentValue = attributes.currentValue>
		<CFSET delim1 = attributes.delim1>
		<CFSET delim2 = attributes.delim2>
		<CFSET onChange = attributes.onChange>
		<CFSET dataValue = attributes.dataValueColumn>
		<CFSET displayValue = attributes.displayValueColumn>

		<CFSET javascriptVerification = "">

		<!--- WAB 2011/09/08
				add support for binding
				this code was copied from relaySelectField (which then called this template).  By putting it here we can do bind in screens (by adding an additional parameter bind=cfc:application.relay... etc
				eventually would hope to use my cf_select tag which encapsulates and updates all this functionality, attributes.currentRecord
				but needed sooner than that!
		 --->

		<cfif structKeyExists(attributes,"bind") and attributes.bind is not "">
			<cfparam name="attributes.bindOnLoad" default="true">
			<cfparam name="attributes.entityID" default="0">


			<cfset attributes.inputOnNoValidValues = false>
			<cfset attributes.bind = application.com.relayTranslations.checkForAndEvaluateCFVariables(phraseText = attributes.bind, mergeStruct = attributes)>
			<cfset bindArgs = {bind = attributes.bind, name= attributes.formFieldName,value = ATTRIBUTES.dataValueColumn, display=attributes.displayValueColumn,type="select",bindOnLoad=attributes.bindOnLoad,selected = attributes.currentValue,entityID = attributes.entityid}>
			<cfif structKeyExists(attributes,"nullText")>
				<cfset bindArgs.nullText = attributes.nullText>
			</cfif>
			<cfif attributes.bindonload>
				<cfset validValues=queryNew('#attributes.dataValueColumn#,#attributes.displayValueColumn#')>
			</cfif>
			<cfoutput>#application.com.relayForms.createJavascriptBind(argumentCollection=bindArgs).scriptWithOnload_js#</cfoutput>
			<cfset structDelete (attributes,"bind")>
		</cfif>


		<CFIF not isQuery(validValues) and validValues is "" >
			<CFSET delim2=chr(9)>   <!--- tab - WAB test, so that we can handle valid values which have commas in them --->


				<CFSET validFieldName = application.com.relayForms.convertFieldNameForValidValueTable(validFieldName)>
				<CFSET parentValidFieldName = attributes.parentValidFieldName>
				<CFSET countryid = attributes.countryid>
				<CFINCLUDE template = "getValidValueQuery.cfm">
				<!--- returns query called validvalues --->

		</cfif>

		<CFSET anyValidValues = (isQuery(validValues) and validValues.recordCount is not 0) or (not isQuery(validValues) and validValues is not "") >


		<CFIF not anyValidValues and attributes.inputOnNoValidValues is true>
			<CFOUTPUT><CF_INPUT class="#attributes.class# form-control" type="Text" Name="#formFieldName#" Value="#currentValue#" Size="20" MAxLength="50">
			<CFIF attributes.keepOrig>
				<CF_INPUT class="#attributes.class# form-control" type="Hidden" Name="#formFieldName#_orig" Value="#currentValue#" >
			</cfif>
			</cfoutput>


		<CFELSE>

			<CFIF not anyValidValues and attributes.inputOnNoValidValues is false and nullText eq "">
				<CFSET validValues = attributes.noValidValuesText>
			</CFIF>

			<!--- display valid values --->
			<CFIF not isQuery(validValues)><!--- WAB testing new code which makes use of a query rather than a list of valid values --->
				<CFINCLUDE template = "showValidValueList.cfm">
			<CFELSE >
				<CFINCLUDE template = "showValidValueQuery.cfm">
			</CFIF>


		</cfif>



--->


