<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- do a lookup fieldname (variable of form fieldsource_fieldtextid) --->
	<!---  variable lookupvalue set to a value which is to be looked up --->

<!--- Mods:
2000-01-06	reworked the way that lookups work - query defined in the valid value database
 --->		

	<!--- also needs to have countryid, and countryandRegionList for current location --->

	<!---  bit of a hack because the validValue table structure uses a . rather than a _ as its separator--->
<CFPARAM name="invalidValues" default="">
<CFPARAM name="delim1" default="#application.delim1#">
<CFPARAM name="parentValidFieldName" default="">

		<CFINCLUDE template = "qryvalidvalue.cfm">
		<!---  runs query getvalidvalues, but should bring back a single record--->	

		<CFIF getValidValues.recordCount IS 0 and parentValidFieldName is not "">
			<!--- if no valid values then, have a look for valid values inherited from a parent --->
			<CFSET validFieldName = parentValidFieldName>
			<CFINCLUDE template = "qryvalidvalue.cfm">
			<!---  runs query getvalidvalues--->		
	
		</cfif>
		
		
		
		<CFIF getValidValues.displayvalue is "*lookup">		
			<!--- need to lookup the result --->

			<!---  first implementation used the same query for defining the whole valid value list and
				the lookup.  Involves a tortuous use of #iif (lookupvalue is not "", DE(),DE())# etc.
			Sept 2000, introduced new format for defining query to bring back lookup value.  
			 both queries are in the data value field separated by a "|"
			--->
						
			<CFSET thisquery = listLast(getValidValues.datavalue,"|") >
			<CFSET thisQuery = evaluate('"#thisquery#"')>

			<!--- put in some error handling to catch badly written queries and unexpected values --->
			<CFTRY>	
				<!---  runs another query getvalidvalues--->	
				<cFQUERY NAME="getValidValues" DATASOURcE=#application.sitedataSource# >
					#preservesinglequotes(thisQuery)#
				</CFQUERY>	

				<CFCATCH>
				
					<CFSET getValidValues= querynew("")>  <!--- this wil create a query with recordcount = 0 so the look up result will come back as "xxx - not a valid value"  --->
				
				</CFCATCH>
				
			</CFTRY>	
				
		</CFIF>



		<CFIF getValidValues.recordcount is 0>
			<!--- no valid values found --->
			<CFSET lookupresult = "#lookupvalue# - (not a valid value)">

		<cfelse>

			<CFSET lookupresult = valuelist(getValidValues.displayvalue)>
		
		</CFIF>