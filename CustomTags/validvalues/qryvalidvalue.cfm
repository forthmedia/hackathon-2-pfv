<!--- �Relayware. All Rights Reserved 2014 --->
<!--- query for looking up valid values.
		if lookupvalue is set then a single result will be returned, otherwise all valid values are returned --->

<!--- Mods:
2000-01-06	Added code for invalid values
NYB 2009-02-25 - All Sites Bug 1902
2009/03/25 WAB  LID 2014 allow lists of countryids
 --->		
		
		<CFIF countryid is not 0 and isnumeric(countryid ) > <!--- WAb 2009/03/25 added the isNumeric.  This allows passing in a list of countryids, specifically for valid values used in searches when all country options are required --->
			<!--- START: NYB 2009-02-25 - All Sites Bug 1902 - Added try/catch --->
			<cftry>
				<CFSET regionList = application.CountryGroupsbelongedTo[countryid]>
				<cfcatch type="any">
					<CFSET regionList = countryid>
				</cfcatch>			
			</cftry>
			<!--- END: NYB 2009-02-25 - All Sites Bug 1902 - Added try/catch --->
		<CFELSE>
			<CFSET regionList = "">
		</cfif>
		
		<CFSET Countryandregionlist = countryid & iif(regionlist is not "",DE(",#regionList#"),DE(""))>
		
		
		<cFQUERY NAME="getValidValues" DATASOURcE=#application.sitedataSource# >
		select 
			validvalue as displayvalue, 
			CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
			sortorder,
		CASE WHEN isNull(datavalue,'') = '' THEN validvalue ELSE datavalue + '#delim1#' + validvalue END as thisvalidvalue
		 from validfieldvalues
		where fieldname  in ( <cf_queryparam value="#validFieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		AND countryID = 0
		<CFIF countryandregionlist IS NOT 0>
		AND NOT EXISTS (SELEcT 1 FROM validfieldvalues
				WHERE fieldname  in ( <cf_queryparam value="#validFieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				and countryID  in ( <cf_queryparam value="#countryandregionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
		</CFIF>
				
			<CFIF lookupvalue is not "">
			AND (datavalue =  <cf_queryparam value="#lookupvalue#" CFSQLTYPE="CF_SQL_VARCHAR" >  OR validvalue = '*lookup' or (isNull(dataValue,'') = '' and validvalue =  <cf_queryparam value="#lookupvalue#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
			</CFIF>		

			<CFIF invalidvalues is not "">
			AND (datavalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) or datavalue is null)
			AND validvalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>
			

		<CFIF regionlist is not "">
		UNION			

		select 
			validvalue, 
			CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
			sortorder,
		CASE WHEN isNull(datavalue,'') = '' THEN validvalue ELSE datavalue + '#delim1#' + validvalue END as thisvalidvalue
		from validfieldvalues
		where fieldname  in ( <cf_queryparam value="#validFieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		AND countryID  in ( <cf_queryparam value="#regionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )					
			AND NOT EXISTS (SELEcT 1 FROM validfieldvalues 
							WHERE fieldname  in ( <cf_queryparam value="#validFieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
							AND countryID in ( <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))

			<CFIF lookupvalue is not "">
			AND (datavalue =  <cf_queryparam value="#lookupvalue#" CFSQLTYPE="CF_SQL_VARCHAR" >  OR validvalue ='*lookup')
			</CFIF>		

			<CFIF invalidvalues is not "">
			AND (datavalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) or datavalue is null)
			AND validvalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>
			

		</CFIF>
		UNION
				
		select 
			validvalue, 
			CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
			sortorder,
		CASE WHEN isNull(datavalue,'') = '' THEN validvalue ELSE datavalue + '#delim1#' + validvalue END as thisvalidvalue
		from validfieldvalues
		where fieldname  in ( <cf_queryparam value="#validFieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		AND countryID  in ( <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )   <!--- WAB 2009/03/25 to allow list of countrids to be passed, changed from = to in --->


			<CFIF lookupvalue is not "">
			AND (datavalue =  <cf_queryparam value="#lookupvalue#" CFSQLTYPE="CF_SQL_VARCHAR" >  OR validvalue ='*lookup')
			</CFIF>		
	
			<CFIF invalidvalues is not "">
			AND (datavalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">) or datavalue is null)
			AND validvalue not in (<cf_queryparam value="#invalidValues#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>


		order by sortorder, validvalue
				
		</CFQUERY>

