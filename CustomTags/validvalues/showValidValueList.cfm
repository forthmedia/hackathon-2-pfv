<!--- �Relayware. All Rights Reserved 2014 --->
<!--- displays a dropdown or radio buttons of valid values --->
<!--- requires variables: formFieldName  (name of form field
							displayas  (blank defaults to dropdown list)
							ValidValueList
							currentvalue
							allowcurrentvalue
							showNull allowNull	


mods WAB 2000-12-20 changed parameters in checkObjectV2

WAB: 2001-07-09 added displayas = "singlecheckbox", useful for displaying a single checkbox (you will need to set a single valid value (often = 1)), 
WAB 2004-18-08	added the form attribute when calling  CF_TwoSelectsComboQuery
2009-06-02 	NYB LHID2300 - not translating 'Select a value for ' in Javascript popup, requiredMessage now passed in from displayValidValues
2009-10-27	NAS LID - 2737 Mod code to make 'Select a value' translateable in drop down lists.
2009/12/03   WAB added ID field for reports project
2010/06/07   WAB corrected multiselect selecting for settings project
2011-06-21	NYB	P-LEN024 - added the ability to disable fields
2011/12/05	WAB added _required fields to radio/checkbox and gave them to have onSubmit functions
2013/07/02	IH Case 436014 remove javaScriptVerification
2015-03-09	AHL Case 443967 My Company Profile - Edit Location Details - Error
--->


<!--- Note that no formatting has been done for radio buttons - ie only works with 2 or 3 of them on the same line! --->
<!--- <CFPARAM name = "displayas"  default="">
<CFPARAM name = "allowcurrentvalue" default="false">
<CFPARAM name=  "showNull" default="false">
<CFPARAM name = "allowNull" default="#showNull#">  <!--- this to be phased out and replaced with shownull --->
<CFPARAM name = "column" default="2">
<CFPARAM name = "jsverify" default="">
<CFPARAM name = "required"  default="0">
<CFPARAM name = "listsize"  default="1">
 --->

<!--- START: NYB LEN024 2011-06-17 --->
<CFPARAM NAME="attributes.disabled" default="false">
<!--- END: NYB LEN024 2011-06-17 --->

<!--- 
not sure this is actually the correct logic
<CFIF required is 0>
	<CFSET showNull = true> <!--- if not a required field, then must be possible to select blank --->
</cfif>
 --->
			<!--- if current value is allowed, but it is not in the valid value list, then add it to end --->
<cfset itemSelected = false>

			<CFIF attributes.method is "edit">
			

					<CFIF allowNewValue is 1>  
						<cfoutput>
						<cf_translate encode="javascript">
						<SCRIPT language = "javascript"  src="/javascript/newSelectValue.js"></script> 
						<script>
							function newValue(thisObject) {
								if (thisObject[thisObject.selectedIndex].value == '*') {
					
									newSelectValue (thisObject,'#jsStringFormat(NewValuePopUpText)#')
							
								}		
							}
							
						
						</script>
						</cf_translate>
						</cfoutput>
					</cfif>

					<cf_translate>
			
						<CFIF displayas is "radio">

							<CFIF showNull is true>
								<CFPARAM name = "nullText" default="none">
								<CFSET validValues = listappend(" #delim1##nullText#",validValues,#delim2#)>
							</CFIF>
							
							<CFSET c = 0>
							<CFSET widtha = Round((100 / column)-(10/column))>
							<CFSET widthb = Round((100 - (column * widtha))/column)>

							<cFOUTPUT>
							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TABLE class="booleanflag">
								<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
									<CFSET c = c + 1>
									<CFIF c MOD column IS 1><TR></CFIF>
									<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="#widthb#%"><INPUT TYPE="RADIO" NAME="#formFieldName#" VALUE="#trim(listfirst(validvalue,delim1))#" <CFIF #trim(listfirst(validvalue,delim1))# IS  trim(currentValue)>checked <cfset itemSelected = true></CFIF>  > </TD>
									<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#listlast(validvalue,delim1)# </TD>
									<CFIF c MOD column IS 0></TR></CFIF>
								</CFLOOP>
									<cfif itemselected is false and allowcurrentvalue>
										<CFSET c = c + 1>
										<CFIF c MOD column IS 1><TR></CFIF>
										<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="#widthb#%"><CF_INPUT TYPE="RADIO" NAME="#formFieldName#" VALUE="#currentValue#" checked > </TD>
										<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#htmleditformat(currentValue)# </TD>
										<CFIF c MOD column IS 0></TR></CFIF>
									</cfif>
									<CFIF c MOD COLUMN IS NOT 0>
										<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))# >&nbsp;<BR></TD></TR>
									</CFIF>
								</TABLE>
							<cfelse>
								<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
									<div class="radio col-xs-12 col-sm-6 col-md-4">
										<label>
											<INPUT TYPE="RADIO" NAME="#formFieldName#" VALUE="#trim(listfirst(validvalue,delim1))#" <CFIF #trim(listfirst(validvalue,delim1))# IS  trim(currentValue)>checked <cfset itemSelected = true></CFIF>  >
											#listlast(validvalue,delim1)#
										</label>
									</div>
								</CFLOOP>
							</cfif>
							</cFOUTPUT>		

							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(this.form.#formFieldName#,'\n#requiredMessage#','1');" >
								<cfoutput><CF_INPUT type="hidden" name = "#formFieldName#_required" submitFunction="verifyObjectv2(form.#formFieldName#,'#REQUIREDmessage#','#required#')" ></cfoutput>	<!--- 2015-03-09 AHL Case 443967 My Company Profile - Edit Location Details - Error --->

							</cfif>
							
						
						<CFELSEIF displayas is "dropdown" OR displayas is "SELECT" or displayas is "" or displayas is "MultiSelect">	

							<CFIF showNull is true >  <!--- WAB: did have and displayas is not "MultiSelect", but can't remember exactly why and don't think it is right --->
								<CFPARAM name = "nullText" default="phr_Ext_SelectAValue"> <!--- 2009-10-27	NAS LID - 2737 Mod code to make 'Select a value' translateable in drop down lists. --->
								<CFSET validValues = listappend(" #delim1##nullText#",validValues, #delim2#)>
							</CFIF>


							<cFOUTPUT>
					
							<SELECT ID="#formFieldID#" NAME="#formFieldName#" SIZE="#listsize#" required = "#required#" <cfif isDefined("class") and class neq "">class="#class#"</cfif> <cfif required>message="#requiredmessage#"</cfif> <CFIF displayas is "multiselect">Multiple</cfif> <CFIF onChange is not "">onchange = "#htmleditformat(onChange)#"</cfif> <cfif allowNewValue is 1>onChange = "javascript:newValue(this.form.#htmleditformat(formFieldName)#)"</cfif> #attributes.passthrough# <cfif attributes.disabled>disabled</cfif>><!--- NYB LEN024 2011-06-17 - added disabled--->
									<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
												<!--- 2010/06/07   WAB corrected multiselect selecting for settings project --->
											<cfset dataValue = trim(listfirst(validvalue,delim1))>
											<cfset displayValue = listlast(validvalue,delim1)>

												<OPTION VALUE="#htmleditformat(dataValue)#" <CFIF listfindNoCase(currentValue, trim(dataValue)) IS NOT 0 > SELECTED="true"  <cfset itemSelected = true></CFIF>>#displayValue# </OPTION>
									</cfloop>
									<cfif itemSelected is false and allowcurrentvalue>
												<OPTION VALUE="#currentValue#" SELECTED="true" >#htmleditformat(currentValue)# </OPTION>
									</cfif>
									<CFIF allowNewValue is 1>  
										<OPTION VALUE="*" >#htmleditformat(NewValueText)#</OPTION>
									</CFIF>

									
							</SELECT>
							</cfoutput>		

							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(this.form.#formFieldName#,'\n#requiredMessage#','#required#');" >
							</cfif>


						<CFELSEIF displayas is "checkbox" >	

							<CFSET c = 0>
							<CFSET widtha = Round((100 / column)-(10/column))>
							<CFSET widthb = Round((100 - (column * widtha))/column)>

							<cFOUTPUT>		
							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TABLE class="booleanflag">
									<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
	
									<CFSET c = c + 1>
									<CFIF c MOD column IS 1><TR></CFIF>
									<TD ALIGN="RIGHT" class="booleanflag" WIDTH="#widthb#%"><CF_INPUT TYPE="checkbox" NAME="#formFieldName#" VALUE="#trim(listfirst(validvalue,delim1))#" checked="#iif( listfindNoCase(currentValue, trim(listfirst(validvalue,delim1))) IS NOT 0 ,true,false)#"  ></TD>
									<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">#listlast(validvalue,delim1)# </TD>
									<CFIF c MOD column IS 0></TR></CFIF>
								
									</CFLOOP>
				
									<CFIF c MOD COLUMN IS NOT 0>
										<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))# >&nbsp;<BR></TD></TR>
									</CFIF>
	
								</TABLE>
							<cfelse>
								<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
									<div class="checkbox col-xs-12 col-sm-6 col-md-4">
										<label>
											<CF_INPUT TYPE="checkbox" NAME="#formFieldName#" VALUE="#trim(listfirst(validvalue,delim1))#" checked="#iif( listfindNoCase(currentValue, trim(listfirst(validvalue,delim1))) IS NOT 0 ,true,false)#"  >
											#listlast(validvalue,delim1)#
										</label>
									</div>
								</CFLOOP>
							</cfif>
							</cFOUTPUT>		

							<CFIF required is not 0>
								<!--- 2009-06-02 NYB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(this.form.#formFieldName#,'\n#requiredMessage#','1');" >
								<CFOUTPUT>
									<CF_INPUT type="hidden" name = "#formFieldName#_required" submitFunction="verifyObjectv2(form['#formFieldName#'],'#REQUIREDmessage#','#required#')" >	<!--- 2015-03-09 AHL Case 443967 My Company Profile - Edit Location Details - Error --->
								</cfoutput>
							</cfif>


						<CFELSEIF displayas is "singlecheckbox" >	
							<!--- designed to display a single check box with no adornments such as tables --->
							<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">

								<CFOUTPUT>
								<CF_INPUT TYPE="checkbox" NAME="#formFieldName#" VALUE="#trim(listfirst(validvalue,delim1))#" checked="#iif( listfindNoCase(currentValue, trim(listfirst(validvalue,delim1))) IS NOT 0 ,true,false)#"  > 
								#trim(listlast(validvalue,delim1))#
								</cfoutput>
							</CFLOOP>



							<CFIF required is not 0>
								<!--- 2009-06-02 WAB LHID2300 changed to use requiredMessage --->								
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(this.form.#formFieldName#,'\n#requiredMessage#','1');" >
							</cfif>


						<CFELSEIF displayas is "twoselects"	>
						

								<!--- the TwoSelectsCombo tag takes two queries, so I need to convert my lists into queries containing the correct items (in the correct order) --->
								<!--- create a query for the selected items, the same length as the number of selelected items --->
								<CFSET selected = queryNew("column1,column2")>
								<CFIF currentValue is not "">
									<CFSET x = queryAddRow(selected,listlen(currentValue))>
								</cfif>
								<!--- create a blank query for the non-selected items--->
								<CFSET notSelected = queryNew("column1,column2")>

										<!--- loop through putting the correct items in each query --->

										<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">
								
											<CFSET thisData = trim(listfirst(validvalue,application.delim1))>			
											<CFSET thisDisplay = trim(listlast(validvalue,application.delim1))>			
								
											<CFIF listfindnocase(currentValue, thisData) is not 0 > 
												<!--- insert into selected query (inthe correct order) --->
												<CFSET x = querySetCell(selected,"column1",thisdata,listfindnocase(currentValue, thisData))>
												<CFSET x = querySetCell(selected,"column2",thisDisplay,listfindnocase(currentValue, thisData))>
																		
											<CFELSE>
												
												<CFSET x = queryAddRow(notSelected)>
												<CFSET x = querySetCell(notSelected,"column1",thisdata)>
												<CFSET x = querySetCell(notSelected,"column2",thisDisplay)>
										
											
											</CFIF> 
											
										</CFLOOP>
								
										
								
								  <CF_TwoSelectsComboQuery
								    NAME="List_#formFieldName#"
								    NAME2="#formFieldName#"
								    SIZE="6"
								    WIDTH="150px"
								    FORCEWIDTH="50"
									QUERY1="selected"
									QUERY2="notselected"
								    CAPTION="<FONT SIZE=-1><B>Here Are Your Choices:</B></FONT>"
								    CAPTION2="<FONT SIZE=-1><B>Make Your Selections:</B></FONT>"
									UP="Yes" 
									DOWN="Yes"
									FORMNAME = "#Attributes.formName#"
									>						
									
						</CFIF>

						</cf_translate>


						<CFIF trim(jsVerify) is not "">

								<CFSET javascriptVerification = javascriptVerification & "msg = msg + #jsVerify#;" >									
									
						</cfif>							

						<CFIF attributes.keepOrig>
							<cFOUTPUT>
							<cf_translate>
							<CF_INPUT TYPE="Hidden" NAME="#formFieldName#_orig" VALUE="#trim(CurrentValue)#" >
							</cf_translate>
							</cFOUTPUT>
						</CFIF>

				<CFELSE>
					<!--- view Mode --->
					<CFOUTPUT>
						<cf_translate>
						<CFLOOP index="validvalue" list="#validValues#" delimiters = "#delim2#">					
							<CFIF #trim(listfirst(validvalue,delim1))# IS  trim(currentValue)>
								#trim(listLast(validvalue,delim1))#
							</CFIF>				
						</CFLOOP>	
						</cf_translate>
					</cfoutput>
				</cfif>



<!--- <!--- reset all to default --->							
<CFSET displayas="">
<CFSET allowcurrentvalue="false">
<CFSET allowNull="false">
<CFSET showNull="false">
<CFSET column="2">
<CFSET listsize="1"> --->
								

