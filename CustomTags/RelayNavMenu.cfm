<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">
<!---
		**********************************************************

	Template name:	relayNavMenu.cfm
	Author:			Simon WJ
	Date created:	28 October 2001


		**********************************************************

	Objective...

		To provide to provide a standard navigation header for all of the Relay screens
		that allows multiple level

	How it works...

		It shows the subTags in RelayNavMenuParam and outputs a menu and
		subMenu structure with the correct data.

	Syntax...
		<CF_RelayNavMenu pageTitle="TestPage" thisDir="/swjtest">
			<CF_RelayNavMenuParam MenuItem="Item1">
		</CF_RelayNavMenu>


	Parameters...
	  	pageTitle - this is the parameter that we are checking when we call the tag
		thisDir - this is the calling tamplate name

	Return Codes - none


	NOTE: requires correct doctype - as set in relayHeaderFooter
Amendment History:

DD-MMM-YY	Initials What was changed
2009/05/11	NJH		Created version 3. Features include a fixed menu and highlighting an element that has been selected. I think that previous versions
					tried this, but it wasn't working in all cases. Also stripped out a lot of code that didn't appear to be used.
2011/06/27	WAB		Has just started to be used!  Problem with the condition on the target attribute, needed to change an OR to an AND
2011/09/20	WAB		LID 7801 Brought back an option the old style static menu (lenovo needed two on a page)
2011-10-14	NYB		LHID7283 - added the ability to open menu links in a new tab
2012-04-16	Englex 	P-REL109 - added the ability to include a file to the left of the nav items
2013-06-20	WAB 	Add tabOptions attribute when using runInNewTab
2013-06-25	WAB		CASE 435923 Add support for an ID attribute
2014-10-30
2015-02-23	NJH		Jira Task Fifteen-148 - added cfoutput around script block that adds the button open/close functionality
2015/11/13	NJH		Jira Task Prod2015-231 - some header rework. Now output the header div in includeTopHead file, so that header is built regardless of whether this file is called.
Enhancement still to do:

1.  looping

--->

<cfif ThisTag.ExecutionMode IS 'start'>
<cfoutput>
<cf_head>
<script>
	/*===================
		  SB 29/10/14: Slide out header nav
    =====================*/
	jQuery(document).ready(function() {

		menuSlideOut();

		function menuSlideOut(){
			//jQuery's animate() won't function correctly for width: "auto", so this workaround is required
			var autoWidth = jQuery('##slideHeader ul').width() + "px";
			//minimise nav menu
			jQuery('.headerCloseHeaderMenu').click(function(){
				jQuery('##slideHeader ul').animate({
					width: "0px"
					}, 500, function() {
					// Animation complete.
				});
				jQuery('.headerCloseHeaderMenu').hide();
				jQuery('.headerOpenHeaderMenu').show();
			});
			//restore nav menu to original width
			jQuery('.headerOpenHeaderMenu').click(function(){
				jQuery('##slideHeader ul').animate({
					width: autoWidth
					}, 500, function() {
					// Animation complete.
				});
				jQuery('.headerOpenHeaderMenu').hide();
				jQuery('.headerCloseHeaderMenu').show();
			});
		}
	});
</script>
</cf_head>
</cfoutput>
</cfif>

<cfif not thistag.HasEndTag>
	<cfthrow message="CF_RelayNavMenu requires an end tag">
</cfif>

<CFIF isDefined('form.goToTemplate')>
	<CFSet shortRoot = replace(application.URLRoot,"/","","ALL")>
	<CFSET thisFile = "#shortRoot#/#form.thisDir#/#form.goToTemplate#.cfm">
	<CFIF FileExists(expandPath(thisFile)) is "yes">
		<CFLOCATION URL="#application.URLRoot#/#form.thisDir#/#form.goToTemplate#.cfm"addToken="false">
	<CFELSE>
		There is no file of that name
	</CFIF>
</CFIF>

<cfif ThisTag.ExecutionMode IS 'end'>
<CFPARAM NAME="attributes.frmruninpopup" TYPE="string" DEFAULT="No"> <!--- either yes or No --->
<CFPARAM NAME="attributes.pageTitle" TYPE="string" DEFAULT=""><!--- provides the title of the relay module this template appears in --->
<CFPARAM NAME="attributes.rootDir" TYPE="string" DEFAULT=""> <!--- available in case the root is not URLRoot --->
<CFPARAM NAME="attributes.thisDir" TYPE="string" DEFAULT=""> <!--- this is available in case the form is being included from another directory --->
<CFPARAM NAME="attributes.current" TYPE="string" DEFAULT=""> <!--- this causes the upper menu 'tag' to be selected as current --->
<CFPARAM NAME="attributes.currentTemplateName" TYPE="string" DEFAULT="#script_name#">
<!--- START: 2012-04-16	Englex 	P-REL109 - added the ability to include a file to the left of the nav items --->
<CFPARAM NAME="attributes.navInclude" TYPE="string" DEFAULT=""> <!--- includes a file to the left of the nav list --->
<!--- END: 2012-04-16	Englex 	P-REL109 - added the ability to include a file to the left of the nav items --->
<CFPARAM Name='thisTag.menuItems' default=#arrayNew(1)#><!--- Protect against no sub-tags --->
<CFPARAM Name='thisTag.menuUpper' default=#arrayNew(1)#><!--- Protect against no sub-tags --->
<cfparam name="useRollOvers" default="true">
<CFPARAM NAME="tabClicked" DEFAULT="tab1">
<cfparam name="attributes.useStaticNav" default="FALSE">  <!--- WAB 2011/09/20 added this parameter so that in certain circumstances we can use the old fashioned static nav menu  - was required where Lenovo had two nav menus on a page --->

<cfset currentTemplateWithQueryString = attributes.CurrentTemplateName&IIF(cgi.QUERY_STRING neq "",DE("?"),"")&cgi.QUERY_STRING>

<CFOUTPUT>

<!--- 2011-10-14 NYB LHID7283 - added: --->
<cf_includejavascriptOnce template = "/javascript/extExtension.js">

<cfif not attributes.useStaticNav>
	<!---
	<div class="header">
		<cfset application.com.request.outputDocumentH1()> --->
		<!--- START: 2012-04-16	Englex 	P-REL109 - added the ability to include a file to the left of the nav items --->
		<cfif attributes.navInclude neq "">
			<cfinclude template="#attributes.navInclude#">
		</cfif>
		<!--- End: 2012-04-16	Englex 	P-REL109 - added the ability to include a file to the left of the nav items --->
		<cfif isDefined("thisTag.menuItems") and arrayLen(thisTag.menuItems) gt 0>
			<div id="slideHeader">
				<button type="button" class="headerCloseHeaderMenu fa fa-angle-right"></button>
				<button type="button" class="headerOpenHeaderMenu fa fa-angle-left"></button>
				<ul>
					<CFIF attributes.frmruninpopup IS NOT "True">

						<!--- Loop over the attribute sets of all sub-tags --->
						<CFLOOP index=i from=1 to=#arrayLen(thisTag.menuItems)#>
							<CFSET subAttribs = thisTag.menuItems[i]>


							<CFSILENT>
							<!--- Get the attributes structure --->

							<!--- Perform other operations --->
		<!--- 					<CFIF findNoCase(subAttribs.CFTemplate,script_name) eq 0> --->
							<CFIF not (subAttribs.CFTemplate contains attributes.CurrentTemplateName and subAttribs.CFTemplate eq currentTemplateWithQueryString)>
								<CFSET SubMenuClass = "Submenu">
							<CFELSE>
								<CFSET SubMenuClass = "subMenuCurrent">
							</CFIF>
							 </CFSILENT>
								<!--- SWJ: 11-Apr-08 If request.relayCurrentVersion.UIVersion is 2 or greater we should ignore the target if
								the target is set to be main.  Main was the name of the old frame set in UI v1 --->
									<!--- START: 2011-10-14 NYB LHID7283 - rewrote --->
									<li><A
										<cfif structkeyexists(subAttribs,"runInNewTab") and subAttribs.runInNewTab>
										<cfparam name="subAttribs.tabOptions" default = "{}">
										onclick="javascript:openNewTab('#htmleditformat(subAttribs.tabName)#','#htmleditformat(subAttribs.tabTitle)#','#htmleditformat(subAttribs.CFTemplate)#',#subAttribs.tabOptions#)" HREF="##"
										<cfelse>HREF="#htmleditformat(subAttribs.CFTemplate)#" <cfif structkeyexists(subAttribs,"onclick")>onClick="#subAttribs.onclick#"</cfif>
										</cfif>
										<cfif structkeyexists(subAttribs,"onclick")>
										onclick="#subAttribs.onclick#"
										</cfif>
										<cfif structkeyexists(subAttribs,"id")>
										id="#subAttribs.id#"
										</cfif>
										<CFIF (subAttribs.target neq "") AND subAttribs.target neq "mainSub" and not (structkeyexists(subAttribs,"runInNewTab") and subAttribs.runInNewTab)> TARGET="#htmleditformat(subAttribs.target)#" </CFIF> <CFIF subAttribs.description neq ""> title="#htmleditformat(subAttribs.description)#" </CFIF> Class="#htmleditformat(SubMenuClass)#">#application.com.security.sanitiseHTML(subAttribs.MenuItemText)#</A></li>
									<!--- END: 2011-10-14 NYB LHID7283 --->
						</CFLOOP>
					</CFIF>
				</ul>
			</div>
		</cfif>
	<!--- </div> --->
	<!--- <div class="headerSpacer"></div> --->

<cfelse>
	<!--- This is the old style nav menu --->
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="left" VALIGN="top" NOWRAP class="Submenu">
				&nbsp;#htmleditformat(attributes.pageTitle)#
			</td>
			<TD ALIGN="right" VALIGN="top" NOWRAP class="Submenu">

				<CFIF attributes.frmruninpopup IS NOT "True">

					<!--- Loop over the attribute sets of all sub-tags --->
					<CFLOOP index=i from=1 to=#arrayLen(thisTag.menuItems)#>
						<CFSILENT>
						<!--- Get the attributes structure --->
						<CFSET subAttribs = thisTag.menuItems[i]>

						<!--- Perform other operations --->
						<CFIF findNoCase(subAttribs.CFTemplate,SCRIPT_NAME) eq 0>
							<CFSET SubMenuClass = "Submenu">
						<CFELSE>
							<CFSET SubMenuClass = "SubmenuCurrent">
						</CFIF>
						</CFSILENT>
							<!--- SWJ: 11-Apr-08 If request. relayCurrentVersion.UIVersion is 2 or greater we should ignore the target if
							the target is set to be main.  Main was the name of the old frame set in UI v1 --->
							<A  HREF="#subAttribs.CFTemplate#"<CFIF <!--- (subAttribs.target neq "" and request. relayCurrentVersion.UIVersion lt 2) or  --->subAttribs.target neq "mainSub"> TARGET="#htmleditformat(subAttribs.target)#" </CFIF> <CFIF subAttribs.description neq ""> title="#htmleditformat(subAttribs.description)#" </CFIF> Class="#htmleditformat(SubMenuClass)#">#htmleditformat(subAttribs.MenuItemText)#</A>
						<!--- Only show a divider if there are no more records --->
						<CFIF i neq arrayLen(thisTag.menuItems)> | <CFELSE>&nbsp;</CFIF>
					</CFLOOP>
				</CFIF>
			</TD>
		</TR>
	</TABLE>

</cfif>


</CFOUTPUT>
</CFIF>
<cfsetting enablecfoutputonly="no">