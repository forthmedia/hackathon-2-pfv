<!--- �Relayware. All Rights Reserved 2014 --->
<!--- custom tag to generate a temporary file name --->
<!--- passed a filename, a userid (optional) and an extension --->
<!--- deletes any old file names with which match the same pattern --->
<!--- returns a new file name (and path) and a URL pointing to the file --->

<!--- one purpose of this tag is to overcome problems we have been having
whereby files seem to have locks on them even after they have been deleted
particular problem if a file is downloaded and then we try to delete it.  We therefore
add a timestamp so that filenames are likely to be unique--->

<!--- 2004-11-02  WAB added \ between #application.userFilesabsolutePath# and #tempFileDirectory# 
		i think that #application.userFilesabsolutePath# must sometimes have \ at the end and sometimes not
		don't think it matters if there are double \\
	2004-11-22 wab needed application. userFilesPath in the webfilepath
	2010/10/05 removed application. userfilespath - not sure whether this template actually used
--->

<CFPARAM name = "attributes.filename">
<CFPARAM name = "attributes.userid" default = "#request.relayCurrentUser.usergroupid#">
<CFPARAM name = "attributes.extension" default = "txt">

<CFSET filename = attributes.filename>
<CFSET userid = attributes.userid>
<CFSET extension = attributes.extension>

<CFSET tempFileDirectory = "temp">	<!--- this could become an application variable --->


<CFSET tempFilePath = "#application.userFilesabsolutePath#\content\#tempFileDirectory#">
<CFSET webFilePath = "/content/#tempFileDirectory#">

<!--- check for old files by same user to delete --->
<CFDIRECTORY ACTION="LIST" DIRECTORY="#tempFilePath#" FILTER="#filename#_#Userid#_*.#extension#" NAME="fileList">

<CFLOOP QUERY="fileList">
	<CFIF fileExists("#tempFilePath#\#Name#")>
		<CFFILE ACTION="DELETE" FILE="#tempFilePath#\#Name#">
	</cfif>
</CFLOOP>



<!--- create new file name and return dos and web paths--->
<CFSET newFileName = "#filename#_#Userid#_#timeFormat(now(),"mmss")#.#extension#">
<CFSET caller.dosFilePath = "#tempFilePath#\#newfileName#">
<CFSET caller.webFilePath = "#request.currentSite.httpProtocol##webFilePath#/#newfileName#">	
	

