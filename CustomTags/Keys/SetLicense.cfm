<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		******************
			SetLicense
		******************

		David A McLean July 2002




--->





<CFIF IsDefined("attributes.License")>
	<CFSET License = attributes.License>
<CFELSE>
	No License Key
	<CF_ABORT>
</CFIF>

<!--- Check If the License has been accessed --->

<CFQUERY NAME = "CheckAccess" DATASOURCE="#application.sitedatasource#">
	SELECT 1 FROM RelayLicenseSeats
	WHERE LicenseKey =  <cf_queryparam value="#License#" CFSQLTYPE="CF_SQL_VARCHAR" >
		AND accessed IS NOT NULL
</CFQUERY>

<CFIF CheckAccess.recordcount EQ 1>
	<CFSET RelaySetLicenseKey = 1>
	<CFSET RelaySetLicenseCode = "Invalid - License Already Accessed">
</CFIF>

<CFIF NOT IsDefined("RelaySetLicenseKey")>
	<!--- Set the user's cookie with a person key --->
	<CFIF isDefined("cookie.RKID")>
		<CFSET RelayKey=cookie.RKID>
	<CFELSE>
		<CFIF isDefined("request.relayCurrentUser.personid") AND not isdefined("cookie.RKID")>
			<CFQUERY NAME="CheckPersonKey" DATASOURCE="#application.sitedatasource#">
				SELECT RelayKey FROM RelayEntityKeys
					WHERE entity='person'
						AND entityid =  <cf_queryparam value="#ListGetAt(Cookie.USER,1,'-')#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<CFIF CheckPersonKey.recordcount EQ 0>
				<CF_CreateKey entity="person" entityid="#ListGetAt(Cookie.USER,1,'-')#">
				<cfset application.com.globalFunctions.cfcookie(EXPIRES="NEVER", NAME="RKID", VALUE="#RelayKey#")>
			<CFELSE>
				<CFSET RelaySetLicenseKey = 4>
				<CFSET RelaySetLicenseCode = "Invalid - No RKID but person has Entity Key">
			</CFIF>
		<CFELSE>
			<CFSET RelaySetLicenseKey = 2>
			<CFSET RelaySetLicenseCode = "Invalid - No User Cookie">
		</CFIF>

	</CFIF>

</CFIF>


<CFIF NOT isdefined("RelaySetLicenseKey")>

<!--- Update the License --->
	<CFQUERY NAME="SetLicense" DATASOURCE="#application.sitedatasource#">
		UPDATE RelayLicenseSeats
			SET accessed=#createodbcdatetime(now())#,
				PersonID =  <cf_queryparam value="#ListGetAt(Cookie.USER,1,'-')#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				RKID =  <cf_queryparam value="#RelayKey#" CFSQLTYPE="CF_SQL_VARCHAR" >
		WHERE LicenseKey =  <cf_queryparam value="#License#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</CFQUERY>

</CFIF>

<CFIF isdefined("RelaySetLicenseKey")>
	<CFSET caller.RelaySetLicenseKey=RelaySetLicenseKey>
</CFIF>
<CFIF isdefined("RelaySetLicenseCode")>
	<CFSET caller.RelaySetLicenseCode=RelaySetLicenseCode>
</CFIF>