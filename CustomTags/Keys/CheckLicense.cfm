<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Function Check License --->

<CFIF isdefined("attributes.sku")>
	<CFSET sku=attributes.sku>
<CFELSE>
	<CFSET RelayValidationKey = 4>
	<CFSET RelayValidationCode = "Invalid - No Sku Supplied - Error">
</CFIF>

<CFIF isdefined("attributes.producttype")>
	<CFSET producttype=attributes.producttype>
<CFELSE>
	<CFSET RelayValidationKey = 4>
	<CFSET RelayValidationCode = "Invalid - No Product Type Supplied - Error">
</CFIF>

<CFIF NOT IsDefined("RelayValidationKey")>
	<CFIF isDefined("cookie.RKID")>
		<CFSET RKID=cookie.RKID>
	<CFELSE>
		<CFSET RelayValidationKey = 1>
		<CFSET RelayValidationCode = "Invalid - No RKID - Ask For License Key">
	</CFIF>
</CFIF>

<CFIF NOT IsDefined("RelayValidationKey")>
	<CFQUERY NAME="GetProductKey"  DATASOURCE="#application.sitedatasource#">
		SELECT RelayKey FROM 
		
		(SELECT EntityID, RelayKey FROM RelayEntityKeys WHERE Entity =  <cf_queryparam value="#producttype#" CFSQLTYPE="CF_SQL_VARCHAR" > ) rk
					
					INNER JOIN 
		(SELECT entityid FROM entityskus WHERE SKU =  <cf_queryparam value="#sku#" CFSQLTYPE="CF_SQL_VARCHAR" >  and entity =  <cf_queryparam value="#producttype#" CFSQLTYPE="CF_SQL_VARCHAR" > ) p 
		
					ON p.entityid = rk.entityid
	</CFQUERY>
	<CFIF GetProductKey.recordcount EQ 0>
		<CFSET RelayValidationKey = 3>
		<CFSET RelayValidationCode = "Invalid - No License For SKU - Error">
	</CFIF>
	<CFIF GetProductKey.recordcount GT 1>
		<CFSET RelayValidationKey = 2>
		<CFSET RelayValidationCode = "Invalid - Not a Unique SKU - Error">
	</CFIF>
	<CFIF GetProductKey.recordcount EQ 1>
		<CFSET skuKey = GetProductKey.RelayKey>
	</CFIF>
</CFIF>

<CFIF NOT IsDefined("RelayValidationKey")>
	<CFQUERY NAME="CheckForLicense" DATASOURCE="#application.sitedatasource#">
	SELECT 1 FROM RelayLicenseSeats
	WHERE
		RKID =  <cf_queryparam value="#RKID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND charindex('#skuKey#',LicenseKey) > 0
	</CFQUERY>
	
	<CFIF CheckForLicense.recordcount GT 0>
		<CFSET RelayValidationKey = 0>
		<CFSET RelayValidationCode = "Valid">
	<CFELSE>
		<CFSET RelayValidationKey = 1>
		<CFSET RelayValidationCode = "Invalid - No License - Ask For License Key">
	</CFIF>
</CFIF>

<CFIF NOT IsDefined("RelayValidationKey")>
	<CFSET RelayValidationKey = 6>
	<CFSET RelayValidationCode = "Invalid - Non Specific Error - Error">
</CFIF>

<CFIF isdefined("RelayValidationKey")>
	<CFSET caller.RelayValidationKey=RelayValidationKey>
</CFIF>
<CFIF isdefined("RelayValidationCode")>
	<CFSET caller.RelayValidationCode=RelayValidationCode>
</CFIF>