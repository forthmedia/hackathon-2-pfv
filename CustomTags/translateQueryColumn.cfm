<!--- �Relayware. All Rights Reserved 2014 --->
<cfsilent>
<!---
File name:			translateQueryColumn.cfm	
Author:				WAB
Date started:			2005-05-10
	
Description:			
Uses cf_Translate to translate a column of a query

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
WAB 2009/05/20  modified to work on more than one column
WAB 2010/09/06 LID 2481 & 2499 added startRow and endRow attributes to speed up when called from tfqo 
	Ought to be modified to use the new translate cfc functions, but these weren't live when needed
WAB 2012-09-26 Use new cfc functions
WAB 2012-09-27 Added support for translating columns which might contain more than just a single phrase
Enhancements still to do:


 --->
 
 <cfparam name="attributes.query">  <!--- pointer to query (rather than name) so pass as #myquery# --->
 <cfparam name="attributes.columnname">  <!--- name of column to translate --->
 <cfparam name="attributes.outputcolumnname" default = #attributes.columnname#>  <!--- can put result in another column (must exist) --->
 <cfparam name="attributes.nullText" default="">
 <cfparam name="attributes.onNullShowPhraseTextID" default="no">
 <cfparam name="attributes.startRow" default="1">
 <cfparam name="attributes.endRow" default="#attributes.query.recordCount#">
 <cfparam name="attributes.singlePhrase" default="true"> <!--- true: column contains a single phrase, false: column contains a string which might itself contain a phrase --->
 
<cfif thisTag.executionMode is "start">
	<cfset thequery = attributes.query>
	
	<cfif attributes.singlePhrase>	
		<!--- make a list of the items in the column
			couldn't work out how to make valelist do it for me when the column nameis in a variable!
			WAB 2009/05/20 replaced with an evaluate which is faster, if nasty!
		<cfloop query="theQuery">
			<cfset phraselist = listappend(phraselist,theQuery[attributes.columnname][currentrow])>
		</cfloop> 
		
		 --->
		<Cfset phraselist ="">
		<cfloop index="thisColumn" list = "#attributes.columnName#">
			<cfset phraseList = listappend(phraseList,evaluate ("valueList(theQuery.#thisColumn#)"))> 
		</cfloop>
		
		<cfset translations = application.com.relayTranslations.translateListOfPhrases(listofphrases = #replacenocase(phraselist,"phr_","","ALL")#, nullText=#attributes.nulltext#,  onNullShowPhraseTextID =#attributes.onNullShowPhraseTextID#)>	
	
		<!--- update the original query --->
		<cfloop query=theQuery startrow="#attributes.startRow#" endrow="#attributes.endRow#">
			<cfloop index="I" from = "1" to = "#listLen (attributes.columnName)#">
				<cfset thisColumn = listgetat(attributes.columnName,i)>
				<cfset outputColumn = listgetat(attributes.outputColumnName,i)>
				<cfset thisValue = theQuery[thisColumn][currentrow]>
				<cfset thisValueNoPhr = replaceNoCase(thisValue,"phr_","","ALL")>
				<cfif left(thisValue,4) is "phr_" and translations.phrases[thisValueNoPhr].phraseText is not "">
					<cfset querySetCell(theQuery,outputColumn,translations.phrases[thisValueNoPhr].phraseText,currentrow)> 
				</cfif>
			</cfloop>
		</cfloop>

	<cfelse>
	
		<cfloop query=theQuery startrow="#attributes.startRow#" endrow="#attributes.endRow#">
			<cfloop index="I" from = "1" to = "#listLen (attributes.columnName)#">
				<cfset thisColumn = listgetat(attributes.columnName,i)>
				<cfset outputColumn = listgetat(attributes.outputColumnName,i)>
				<cfset thisValue = theQuery[thisColumn][currentrow]>
				<cfif thisValue contains "phr_">
					<cfset querySetCell(theQuery,outputColumn,application.com.relayTranslations.translateString(thisValue),currentrow)> 				
				</cfif>
			</cfloop>
		</cfloop>
		
	
	</cfif>	
		
</cfif>

</cfsilent>

