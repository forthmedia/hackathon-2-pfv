var $_form = '';
var $_saveButton = '';
var $_buttonBarButtons = '';

jQuery(document).ready(function() {
	getSaveButton().click(function(){
		var form = getForm();
		var formID = form.attr('id');

		getButtonBarButtons().prop('disabled',true);
	
		if (form.serialize()== form.data('serialize')) {
			submitFormObject(formID);
		} else {
			if (form.get(0).checkValidity()) {
				submitFormObject(formID);
			} else {
				//clicking the save button will fail, but will trigger any user messages etc
				jQuery('#saveButton')[0].click();
			}
		}
	});
});

function getForm() {
	if ($_form == '') {
		$_form = jQuery('#save').closest('form');
	}
	return $_form;
}

function getSaveButton() {
	if ($_saveButton == '') {
		$_saveButton = jQuery('#save');
	}
	return $_saveButton;
}

function getButtonBarButtons() {
	if ($_buttonBarButtons == '') {
		$_buttonBarButtons = jQuery('#actionContainer').find(":button,:submit");
	}
	return $_buttonBarButtons;
}

function submitFormObject(formID) {
	var jqxhr = jQuery.post(
		'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=entityWS&methodName=save&returnFormat=json'
		, jQuery('#'+formID).serialize()
		, function(data) {
			getButtonBarButtons().prop('disabled',false);
			console.log(data)
			if (data.ISOK == true) {
				alert('going to page x');
			}
		}
	).fail(function() {
		alert( "Problem saving record." );
  	})
}