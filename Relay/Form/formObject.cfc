<!---
  --- formObject
  --- ----------
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   04/11/16
  --->
<cfcomponent accessors="true" output="false" persistent="false">

	<cfset variables.header = {}>
	<cfset variables.itemsArray = []>
	<cfset variables.formAttributes = {method = "post", id="mainForm", name="mainForm", noValidate="noValidate", enctype="multipart/form-data", action=""}>
	<cfset variables.buttonArray = []>
	<cfset variables.readonly = false>
	<cfset variables.showCancel = true>
	<cfset variables.showSave = true>
	<cfset variables.showButtonBarInHeader = request.relayCurrentUser.isInternal?true:false>
	<cfset variables.renderToStructureResult = []>
	<cfset this._renderToStructure = false>

	<cffunction name="init">
		<cfargument name="entity" type="any" required="true">
		<cfargument name="screenID" type="string" required="false">
		<cfargument name="items" type="array" required="false">

		<cfset setEntity (entity)> <!--- needs loading first --->

		<cfloop Collection=#arguments# item="local.item">
			<cfif local.item is not "entity">
				<cfif isdefined("arguments.#item#")>
					<cfset var func = variables["set#item#"]>
					<cfset func (arguments[item])>
				</cfif>
			</cfif>
		</cfloop>

		<!--- set the ID/name of the form to include the entityName to provide somewhat of a unique id --->
		<cfset var formName = lcase(getEntity().getEntityType())&"Form">
		<cfset setFormAttribute("id",formName)>
		<cfset setFormAttribute("name",formName)>

		<cfreturn this>
	</cffunction>


	<cffunction name="getFormAttribute" access="public" output="false" returnType="any">
		<cfargument name="name" type="string" required="true">

		<cfreturn structKeyExists(variables.formAttributes,arguments.name)?variables.formAttributes[arguments.name]:"">
	</cffunction>


	<cffunction name="setFormAttribute" access="public" output="false" returnType="void">
		<cfargument name="name" type="string" required="true">
		<cfargument name="value" type="any" required="true">

		<cfset variables.formAttributes[arguments.name] = arguments.value>
	</cffunction>


	<cffunction name="setCountryID" output="false" returnType="void">
		<cfargument name="countryID" type="any" required="true">

		<cfset variables.countryID = arguments.countryID>
	</cffunction>


	<cffunction name="getCountryID" output="false" returnType="numeric">
		<cfset var result = 0>
		<cfif structKeyExists (variables,"countryid")>
			<cfset result = variables.countryid>
		<cfelse>
			<cfset result = getEntity().countryid>
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="appendFormElements" output="false">

		<cfset arrayAppend (variables.itemsArray, {type="screen", args = arguments})>

		<cfset var definition = application.com.screens.getScreenAndDefinition(screenID=arguments.screenid,countryid=0)>

		<cfset variables.header = application.com.structureFunctions.queryRowToStruct(definition.screen)>
		<cfset variables.items = definition.definition> <!--- application.com.structureFunctions.QueryToArrayOfStructures(query=definition.definition)> --->

	</cffunction>


	<cffunction name="getItems" output="false" returnType="array">
		<cfreturn variables.items>
	</cffunction>


	<cffunction name="getHeader" output="false" returnType="struct">
		<cfreturn variables.header>
	</cffunction>


	<cffunction name="setEntity" output="false">
		<cfargument name="entity" type="any" required="true">

		<cfset variables.entity = arguments.entity>
	</cffunction>


	<cffunction name="setScreenID" output="false">
		<cfargument name="screenid" type="any" required="true">

		<cfset appendScreenById (screenid)>
	</cffunction>


	<cffunction name="setDefinition" output="false">
		<cfargument name="definition" type="any" required="true">

		<cfset appendDefinition (definition)>
	</cffunction>


	<cffunction name="setHeader" output="false">
		<cfargument name="header" type="any" required="true">

		<cfset variables.header = arguments.header>
	</cffunction>


	<cffunction name="getEntity" output="false" returnType="any">
		<cfreturn variables.entity>
	</cffunction>


	<cffunction name="set" output="false" returnType="void" hint="Set an attribute">
		<cfargument name="name" type="string" required="true">
		<cfargument name="value" type="any" required="true">

		<cfif structKeyExists(variables,arguments.name)>
			<cfset variables[arguments.name] = arguments.value>
		</cfif>
	</cffunction>


	<cffunction name="get" output="false" returnType="any" hint="Get an attribute">
		<cfargument name="name" type="string" required="true">

		<cfif structKeyExists(variables,arguments.name)>
			<cfreturn variables[arguments.name]>
		</cfif>
		<cfreturn "">
	</cffunction>


	<cffunction name="appendElementDisplay" output="false" >
		<cfset arrayAppend (variables.itemsArray, {type="FormElementDisplay", args = arguments})>
	</cffunction>


	<cffunction name="appendScreenByID" output="false" >
		<cfargument name="screenid" type="any" required="true">

		<cfset arrayAppend (variables.itemsArray, {type="Definition", args = {object = new formElementsObject (screenid = arguments.screenid, entity = getEntity(), formObject = this)} })>

	</cffunction>


	<cffunction name="appendDefinition" output="false" >
		<cfargument name="Definition" type="any" required="true">

		<cfset arrayAppend (variables.itemsArray, {type="Definition", args = {object = new formElementsObject (Definition = arguments.Definition, entity = getEntity(), formObject = this)} })>
	</cffunction>


	<cffunction name="appendButton" access="public" output="false" >
		<cfset arrayAppend (variables.buttonArray, arguments)>
	</cffunction>


	<cffunction name="prependButton" access="public" output="false" >
		<cfset arrayprepend (variables.buttonArray, arguments)>
	</cffunction>


	<cffunction name="render" output="false" returnType="string">

		<cfset local.resultHTML = "">
		<cfscript>
			for (local.item in itemsArray) {
				if (item.type is "FormElementDisplay") {
					local.resultHTML &= application.com.relayDisplay.relayFormElementDisplay(argumentCollection = item.args);
				} else if (item.type is "HTML") {
					local.resultHTML &= item.HTML;
				} else if (item.type is "Definition"){
					local.resultHTML &= item.args.object.render(renderToStructure = this._renderToStructure);
					if (this._renderToStructure) {
						arrayAppend (variables.renderToStructureResult, item.args.object.getrenderToStructureResult(), true);
					}
				}
			}

			if (this.get("showCancel")) {
				prependButton (value="Cancel", name="cancel", onclick="onCancel();");
			}
			if (this.get("showSave")) {
				appendButton (value="Save", readonly="#getReadonly()#", name="save", onclick="triggerFormSubmit(this.form)");
			}
		</cfscript>

		<cfsaveContent variable="local.result">
			<cfset application.com.request.includeJavascriptOnce(template="/form/js/formObject.js",translate="true")>
			<cfoutput>
			<form #application.com.relayForms.convertTagAttributeStructToValidAttributeList(attributeStruct=variables.formAttributes, tagName="form")#>
				<cf_input type="hidden" name="entitytype" value="#lcase(getEntity().getEntityType())#" encrypt="true">
				<cf_input type="hidden" name="#lcase(getEntity().getEntityUniqueKey())#" value="#getEntity().getPrimaryKey()#" encrypt="true">
				<cf_input type="hidden" name="rwFormProcessor" value="com.entities.processFormScope()" encrypt="true">

				<cf_relayFormDisplay autoRefreshRequiredClass="true">
					#local.resultHTML#
					<cfset request.relayForm.useRelayValidation = true>  <!--- Hack required because I am doing my call to the form elements outside the tag --->
				</cf_relayFormDisplay>

				#getButtonBarOpeningDivHTML()#
				#outputButtonBar()#
				</div>
			</form>
			</cfoutput>
		</cfsaveContent>

		<cfreturn local.result>
	</cffunction>


	<cffunction name="setReadonly" output="false" returnType="void" access="public">
		<cfargument name="readonly" type="boolean" required="true">
		<cfset variables.readonly = arguments.readonly>
	</cffunction>


	<cffunction name="getReadOnly" output="false" returnType="boolean" access="private">
		<cfreturn variables.readonly>
	</cffunction>


	<cffunction name="renderToStructure" output="false" returnType="struct">

		<cfset 	this._renderToStructure = true>
		<cfset render ()>
		<cfset 	this._renderToStructure = false>
		<cfreturn { layout = variables.renderToStructureResult}>

	</cffunction>


	<cffunction name="outputButtonBar" output="false" returnType="string" access="private">

		<cfset var buttonBarHTML = '<ul class="list-inline">'>
		<cfset var buttonClass = " btn btn-primary">

		<cfloop array=#variables.buttonArray# index="local.buttonAttrs">
			<cfset local.buttonAttrs.type = "button">
			<cfset local.buttonAttrs.class = structKeyExists(local.buttonAttrs,"class")?local.buttonAttrs.class & buttonClass:buttonClass>
			<cfset buttonBarHTML &= '<li><input #application.com.relayForms.convertTagAttributeStructToValidAttributeList (AttributeStruct = local.buttonAttrs, TagName="input" )#></li>'>
		</cfloop>

		<cfset buttonBarHTML &= "</ul>">
		<cfreturn buttonBarHTML>
	</cffunction>


	<cffunction name="getButtonBarOpeningDivHTML" access="private" output="false" returnType="string">
		<cfset var divID = variables.showButtonBarInHeader?"slideHeader":"actionContainer">
		<cfset var divHTML = '<div id="#divID#" class="actionContainer">'>
		<cfif variables.showButtonBarInHeader>
			<cfset divHTML &= '<button type="button" class="headerCloseHeaderMenu fa fa-angle-right"></button>
								<button type="button" class="headerOpenHeaderMenu fa fa-angle-left"></button>'>
		</cfif>
		<cfreturn divHTML>
	</cffunction>
</cfcomponent>