<cfcomponent>

	<cfset variables.fieldsDisplayed = {}>
	<cfset variables.formObject = {}>

	<cfset variables.formElementsArray = []>

	<cfset variables.collectScreenDefinitionArray = []>

	<cffunction name="init">

		<cfscript>
			var fieldSourceFunctionArray = application.com.globalfunctions.getArrayOfCFCMethods(this,"fieldSource_");
			variables.fieldSourceFunctionsStruct = {};
			for (var i in fieldSourceFunctionArray) {
				variables.fieldSourceFunctionsStruct[replace(i.name,"fieldSource_","")] = "";
			}

		</cfscript>

		<cfloop Collection=#arguments# item="local.item">
			<cfset variables[local.item] = arguments[local.item]>
<!---
			<cfset var func = variables["set#item#"]>
			<cfset func (arguments[item])>
 --->
		</cfloop>

		<cfset variables._renderToStructure = false>

	</cffunction>




	<cffunction name="render">
		<cfargument name="readOnly" type="string" default="false">
		<cfargument name="renderToStructure" type="boolean" default="false">

		<cfif arguments.renderToStructure>
			<cfset variables._renderToStructure = true>
		</cfif>

		<cfif structKeyExists (variables,"screenid")>
			<cfset var result = outputFormElementsByID (screenid = variables.screenid, readOnly = readOnly)>
		<cfelse>
			<cfset var result = outputFormElementsByDefinition (definition = definition, entity = variables.entity, readOnly= readOnly)>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="outputFormElementsByID" access="public" output="false" returnType="string" hint="Output Form Elements given a screenid and an object">
		<cfargument name="screenID" type="string" required="true">
		<cfargument name="readOnly" type="string" default="false">

		<cfset var result = "">
		<cfif application.com.screens.doesScreenExist(currentScreenId=arguments.screenID)>
			<cfset var screenAndDef = application.com.screens.getScreenAndDefinition(screenid= arguments.screenID, countryID= entity.getCountryID(), screenVersion = 2)>
			<cfset var screenDefQry = screenAndDef.definition>

			<cfset result = outputFormElementsByDefinition (definition = screenDefQry, entity = variables.entity, readOnly= "#arguments.readOnly# OR #screenAndDef.screen.readonly#")>

		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="outputFormElementsByDefinition" access="public" output="false" returnType="string" hint="Display screen elements on a form that is drawn with a display XML">
		<cfargument name="definition" type="query" required="true">
		<cfargument name="entity" type="any" required="true">
		<cfargument name="readOnly" type="string" default="false">


		<cfscript>
		var result = "";
		if (not structKeyExists(arguments, "mergeObject")) {
			var mergeObject = createObject ("component","com.mergeFunctions");
			mergeObject.initialiseMergeStruct (mergeStruct = local);
			mergeObject.initialiseMergeStruct (mergeStruct = arguments);
			var mergeStruct [entity.getEntityType()] = entity;
			mergeObject.initialiseMergeStruct (mergeStruct = mergeStruct);
		}

		var form_readOnly = mergeObject.evaluateAString(arguments.readOnly).result;

		var nullRow = {   maxlength = ""
						, size	= ""
						, useValidValues = 0
						, evalstring = ""
						, displayAs = ""
					}	;


		var unsupportedFieldSources = ['##addressTable##'
									, 'alllocationflags'
									, 'allpersonflags'
									, 'blank'
									, 'cfparam'
									, 'EngagementType'
									, 'interactionType'
									, 'Screen'
									, 'SpecialEffect'
		];

		var itemsToEvaluateToBoolean = ['readOnly','required'];
		var itemsToEvaluateToString = ['displayAs'];

			for (var row in definition) {
					structAppend(row, nullRow, false);
					var showthisline = true;

					if (row.thisLineCondition neq "") {
						try {
							showthisline = mergeObject.evaluateAString(row.thislinecondition).result;
						}
						catch (any cfcatch) {
							application.com.errorHandler.recordRelayError_Warning(type="Screen", Severity="error", caughtError = cfcatch, warningStructure=arguments);
						}
					}


					if (arrayFindNoCase (unsupportedFieldSources, row.fieldSource) ) {
						result &= '<div class="warningblock">Item #row.fieldSource# is unsupported</div>';
						showthisline = false;
					}

					if (showthisline) {

						/* bit of work to do here to decide which bits of the row get put into the thisLineArguments
						* do the functions which deal with own field sources get the thisLineArguments structure or just the row, or are they one and the same
						*/
						var thisLineArguments = { maxLength =row.maxLength
										, size = row.size
										, useValidValues = row.useValidValues
										, readOnly = row.readOnly
										, required = row.required
										, label = row.Label
										, displayAs = row.displayAs
										};

						structAppend(thisLineArguments,application.com.structureFunctions.convertNameValuePairStringToStructure(inputString= row.specialFormatting,delimiter=","));


						for (local.item in itemsToEvaluateToBoolean) {
							thisLineArguments[local.item] = mergeObject.evaluateAString(thisLineArguments[local.item]).result;
						}

						for (local.item in itemsToEvaluateToString) {
							thisLineArguments[local.item] = mergeObject.checkForAndEvaluateCFVariables(thisLineArguments[local.item]);
						}


						thisLineArguments.readonly = form_readOnly OR thisLineArguments.readOnly;


						/* first look for fieldsources which are dealt with by their own function */
						if (structKeyExists (variables.fieldSourceFunctionsStruct, row.fieldSource)) {
							var func = variables["fieldSource_" & row.fieldSource];
							result &= func(row = row, entity = entity, mergeObject = mergeObject);

						/* anthing else must be an object */
						} else {
							/* else it must be an object */

							mapScreenArgumentsToRelayDisplayArguments(entity = entity, thisLineArguments = thisLineArguments, fieldTextID = row.fieldTextID, fieldSource = row.fieldSource);

							/* valid values need to be evaluated here because they might have been picked up from the object metadata, but if left to be done in the validvalue code, don't pick up the entity object for merging */
							if (structKeyExistsAndNotNull (thisLineArguments, "validValues")) {
								thisLineArguments["validValues"] = mergeObject.checkForAndEvaluateCFVariables(thisLineArguments["validValues"]);
							}


							thisLineArguments.currentValue = entity.getObject(row.fieldSource).get(row.fieldTextID);

							if (entity.isNewRecord() && structKeyExists (thisLineArguments,"defaultValue")) {
								thisLineArguments.currentValue = mergeObject.checkForAndEvaluateCFVariables(thisLineArguments.defaultValue);
							}

							fieldsDisplayed [row.fieldTextID] = thisLineArguments.readonly;

							if (row.evalString is not "") {
								mergeObject.appendMergeStruct ({value = thisLineArguments.currentValue, thislinedata = thisLineArguments.currentValue});
								thisLineArguments.currentValue = mergeObject.checkForAndEvaluateCFVariables(row.evalString);
							}

							thisLineArguments.fieldName = lcase("#row.fieldSource#_#row.fieldTextID#");

							result &= callRelayFormElementDisplay(argumentCollection= thisLineArguments, entityID=0, countryID=0);
						}


					}
			}

		</cfscript>

		<cfreturn result>

	</cffunction>


	<cffunction name="mapScreenArgumentsToRelayDisplayArguments" output="false">
		<cfargument name="entity" type="any" required="true">
		<cfargument name="thisLineArguments" type="struct" required="true">
		<cfargument name="fieldSource" type="string" required="true">
		<cfargument name="fieldTextID" type="string" required="true">

		<cfscript>

		 var mapDisplayAsToType = {  select = "select"
									,multiselect = "select"
									,text = "text"
									,decimal = 'numeric'
									,financial = 'numeric'
									,integer = 'numeric'
									,date = 'date'
									,textArea = 'textArea'
									,memo = 'textArea'
									,checkbox = 'select'
									,radio = 'select'
									,hidden = 'hidden'
									};

		var describe = entity.describe("#fieldSource#.#fieldTextID#");

		/*
			Take various items from describe structure
			merge these into the items in thisLineArguments either if does not already exist, or if existing value is ""
 		*/
		tempStruct = describe.getDisplayAttributes();
		for (local.item in tempStruct) {
			if (not structKeyExistsAndNotNull(thisLineArguments,local.item)) {
				thisLineArguments[local.item] = tempStruct[local.item];
			}
		}

		if (structKeyExists (thisLineArguments, "displayAs") and structKeyExists (mapDisplayAsToType, thisLineArguments.displayAs)) {
				thisLineArguments.Type = mapDisplayAsToType[thisLineArguments.displayAs];
		}


		if (describe.getValidate() is "numeric" and describe.getDisplayAs() is "text") {
			thisLineArguments.Type = "numeric";
		}


		if (thisLineArguments.useValidValues and not StructKeyExists (thisLineArguments, "validvalues")) {
			/* this is probably redundant now because vFieldMetaData automatically looks in the validFieldValues table */
			thisLineArguments.validvalues = "#entity.getEntityType()#.#fieldTextID#";   /* or flag.validValues */
		}


		if (thisLineArguments.displayAs is "multiselect") {
			thisLineArguments.multiple = true;
			// By default multiselects should not show any null text
			param thisLineArguments.showNull = false;
		}

		if (structKeyExistsAndEquals(thisLineArguments, "Type", "select" ) or describe.getIsNullable()) {
			// By default selects should show null text - PROD2016-3317 - or anything that is describes as being nullable.. only affect select/radio in reality
			param thisLineArguments.showNull = true;
		}

		/* deal with validvalues coming from describe but separate bind function or query attribute*/
		if (StructKeyExistsAndNotNull (thisLineArguments, "validvalues")  and (StructKeyExistsAndNotNull (thisLineArguments, "bindFunction") OR StructKeyExistsAndNotNull (thisLineArguments, "query") ) ) {
			structDelete (thisLineArguments, "validvalues");
		}

		if (describe.getDataType() is "bit" and not structKeyExists (thisLineArguments, "validvalues")) {
			thisLineArguments.value = 1;   /* Not sure what to do here, really */
			thisLineArguments.type = 'checkbox';
		}


		/* a non nullable field is always required, whatever the line actually says */
		if (not describe.getIsNullable()) {
			thisLineArguments.required = 1;
		}


		if (thisLineArguments.maxLength is "" and describe.getMaxLength() is not "") {
			thisLineArguments.maxLength = describe.getMaxLength();
		} else if (describe.getMaxLength() is not "") {
			thisLineArguments.maxLength = min (describe.getMaxLength(), thisLineArguments.maxLength);
		} else {
			thisLineArguments.maxLength = -1;
		}

		if (thisLineArguments.size is "") {
			thisLineArguments.size  = 0;
		}

		if (structKeyExists (thisLineArguments,"requiresvalueinotherflag") ) {
			writeoutput("requiresvalueinotherflag attribute needs converting to data-showOtherFields");

			abort;

		}

		thisLineArguments.readonly = thisLineArguments.readonly or describe.getReadonly();

		return thisLineArguments;

		</cfscript>
	</cffunction>





			<!---
			This is the block of code which shows all the fieldSources which need to be dealt with for backwards compatibility

			<cfswitch expression = #thisFieldSource#>
				<cfcase value="flaggroup,flag,html,SpecialEffect">
					<CFINCLUDE template="showScreenElement_#thisFieldSource#.cfm">
				</cfcase>
				<cfcase value="allpersonflags,alllocationflags,allelementflags,allorganisationflags">
					<CFINCLUDE template="showScreenElement_AllFlags.cfm">
				</cfcase>
				<cfcase value="text">
					<cFOUTPUT>#htmleditformat(FieldTextID)#</cFOUTPUT>
				</cfcase>
				<cfcase value="phrase">
					 <cFOUTPUT>phr_#htmleditformat(FieldTextID)#</cFOUTPUT>
				</cfcase>
				<cfcase value="FullAddress">
					<CFOUTPUT>#application.com.screens.evaluateAddressFormat (location = location)#
					<!--- #evaluate(application.addressFormat[variables.Countryid])# --->
					</CFOUTPUT>
				</cfcase>
				<cfcase value="link">
					<!--- this code originally from reports! --->
					<CFIF findnocase("gif",fieldtextid)  is not 0>
						<CFSET anchortext = "<IMG SRC='/images/MISC/#fieldtextid#' BORDER=0>">
					<CFELSE>
						<CFSET anchortext = evaluate(fieldtextid) >
					</CFIF>
					<CFOUTPUT><a HREF="#evaluate(thisevalstring)#">#htmleditformat(anchortext)#</A></CFOUTPUT>
				</cfcase>
				<cfcase value="checkbox">
					<CFPARAM name="#fieldtextid#" default="">
					<CFOUTPUT><CF_INPUT TYPE="CHECKBOX" NAME="#fieldtextid#" VALUE="#evaluate(thisevalstring)#"  CHECKED="#iif(ListFind(evaluate(fieldtextid),evaluate(thisevalstring)) IS NOT 0,true,false)#"> </cfoutput>
				</cfcase>
				<cfcase value="selection">
					<CFOUTPUT><CF_INPUT TYPE="CHECKBOX" NAME="#thisFieldSource#_#fieldtextid#" VALUE="#evaluate(thisevalstring)#"  CHECKED="#iif(ListFind(evaluate("thisFieldSource_fieldtextid"),evaluate(thisevalstring)) IS NOT 0,true,false)#"> </cfoutput>
				</cfcase>
				<cfcase value="CFBREAK,CFSET,CFPARAM">
					<CFIF thisFieldSource is "CFBREAK">
						<CFSET breakScreen = true>
					<CFELSEIF thisFieldSource is "CFSET">
						<CF_EvaluateNameValuePair nameValuePairs="#thisfieldTextID#">
					<CFELSEIF thisFieldSource is "CFPARAM">
						<CF_EvaluateNameValuePair nameValuePairs="#thisfieldTextID#" mode="param">
					</cfif>
				</cfcase>

				<!--- NJH 2009/08/25 LID 2558 - added another case of error when the lineCondition could not be evaluated. --->
				<cfcase value="error">
					<cfparam name="errorMsg" type="string" default="">
					<cfoutput><span class="required">#htmleditformat(errorMsg)#</span></cfoutput>
				</cfcase>

				<cfcase value="relatedFile">
						<!--- this functionality added by WAB 5/01
							room for lots more switches - this is a basic implementation
						 --->
						 <cfset filecategory = thisFieldTextID>
						 <cfquery name="getRelatedFileCategory" dataSource="#application.sitedatasource#">
							 select * from relatedFileCategory where
							 <cfif isNumeric(filecategory)>filecategoryID <cfelse>	 filecategory  	 </cfif>='#filecategory#'
						 </cfquery>
						<!--- WAB 2015-12-14 BF-79 use getEntityType to get uniqueKey field (for cases where uniqueKey is not just #tablename#ID) --->
						<cfset relatedFileEntityID = evaluate(application.com.relayentity.getEntityType(getRelatedFileCategory.fileCategoryEntity).uniqueKey)>
						<CFPARAM name="allowReplace" default= "true">

						<!--- NJH 2009/07/27 P-FNL069 changed from v2 to relatedFile which then calls V2 --->
						<CF_relatedfile query="test" action="variables" entityType="#getRelatedFileCategory.fileCategoryEntity#" entity = "#relatedFileEntityID#" filecategory="#getRelatedFileCategory.fileCategory#" >

						<cfif relatedFileVariables.filelist.recordcount is not 0>
							<!--- <cfoutput>Existing file(s): <BR></cfoutput> --->
							<cfif displayas is "image">
								<CFPARAM name="height" default= "">
								<CFPARAM name="width" default= "">
								<cfoutput query="relatedFileVariables.filelist">
								<IMG <cfif variables.height is not "">height=#htmleditformat(variables.height)#</cfif> <cfif variables.width is not "">width=#htmleditformat(variables.width)#</cfif> src="#htmleditformat(webhandle)#"><BR>
								</cfoutput>
							<cfelse>
								<cfoutput query="relatedFileVariables.filelist">
								<A HREF="#webhandle#" target="_blank">#htmleditformat(filename)#</A><BR>
								</cfoutput>
							</cfif>
						</cfif>

						<CFIF thislineMethod IS "Edit" and (allowreplace is true or relatedFileVariables.filelist.recordcount is 0)>
							<cfoutput><CF_INPUT type="file" name="#relatedFileVariables.formfieldname.filename#" size="30"> </cfoutput>
							<CFSET caller.updateableFields = true>
							<cfif required gt relatedFileVariables.filelist.recordcount>
								<!--- WAB 2010/10/20 support for required message --->
								<cfif requiredMessage is "">
									<cfset requiredMessage = "Enter a file for #requiredLabel#">
								</cfif>
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#relatedFileVariables.formfieldname.filename#,'  #requiredMessage#','1');" >
							</cfif>
						</cfif>


						<cfset filecategory = "">
						<cfset allowreplace = true>

				</cfcase>


			</cfswitch>
			 --->

	<cffunction name="getFieldsDisplayed">
		<cfreturn variables.fieldsDisplayed>
	</cffunction>


	<cffunction name="fieldSource_HTML">
		<cfargument name="row" type="struct" required="true">
		<cfreturn callRelayFormElementDisplay(type="html", html = row.fieldTextID)>
	</cffunction>


	<cffunction name="fieldSource_Text">
		<cfargument name="row" type="struct" required="true">
		<cfset var text = mergeObject.checkForAndEvaluateCFVariables(row.fieldTextID)>
		<cfreturn callRelayFormElementDisplay(type="html", html = text, label = row.label)>
	</cffunction>


	<cffunction name="fieldSource_Value" hint="Evaluates a string an outputs it">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">

		<cfset var value = mergeObject.evaluateAString(row.evalstring)>
		<cfreturn callRelayFormElementDisplay(type="html", html = value, label = row.label)>

	</cffunction>


	<cffunction name="fieldSource_set" hint="Takes name value pairs and uses to set variables">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">

		<cfset fields = application.com.structureFunctions.convertNameValuePairStringToStructure (fieldTextID)>
		<cfset mergeObject.appendMergeStruct(mergeStruct = fields)>

		<cfreturn "">
	</cffunction>


	<cffunction name="fieldSource_HiddenField" hint="Hidden field with name fieldTextID and value of evalstring">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">
		<cfargument name="entity" type="struct" required="true">

		<cfreturn '<INPUT type="hidden" name="#row.FieldTextID#" value="#mergeObject.checkForAndEvaluateCFVariables(row.evalString)#">'>

	</cffunction>


	<cffunction name="fieldSource_ScreenPostProcess">
		<cfreturn '<INPUT type="hidden" name="frmScreenPostProcessID" value="#row.fieldTextID#">'>
	</cffunction>


	<cffunction name="fieldSource_Label" hint=" TBD - does not seem to be possible to output a label by itself">
		<cfreturn callRelayFormElementDisplay(type="html", html = "", label = row.fieldTextID)>
	</cffunction>


	<cffunction name="fieldSource_query" hint="Evaluates a string an outputs it">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">
		<cfargument name="entity" type="struct" required="true">

		<cfset var queryString = mergeObject.checkForAndEvaluateCFVariables(row.evalstring)>
		<cfset result = "">

		<cfset var re = "(\A|[^A-Za-z0-9])(drop|create|truncate|insert|update|delete)(\Z|[^A-Za-z0-9])">
		<cfif reFindNoCase(re,queryString) is not 0>
			<cfset result = "disallowed query #htmleditformat(queryString)#">
		<cfelse>
			<CFQUERY name="local.qryResult" dataSource="#application.sitedatasource#">
				#preserveSingleQuotes(queryString)#
			</cfquery>
			<cfset mergeObject.appendMergeStruct(mergeStruct = {"#row.fieldTextID#" = local.qryResult})>
		</cfif>

		<cfreturn "">

	</cffunction>


	<cffunction name="fieldSource_SectionStart" hint="">
		<cfreturn 	callRelayFormElementDisplay(type="html", html = '<SECTION><div class="sectionHeading">Section Start: #mergeObject.checkForAndEvaluateCFVariables(row.fieldTextId)#</div>' )>
	</cffunction>

	<cffunction name="fieldSource_SectionEnd" hint="">
		<cfreturn 	callRelayFormElementDisplay(type="html", html = '<div class="sectionFooter">Section End: #mergeObject.checkForAndEvaluateCFVariables(row.fieldTextId)#</div></SECTION>' )>
	</cffunction>


	<cffunction name="fieldSource_formWidget">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="entity" type="struct" required="true">

		<cftry>
			<cfset result = "">
			<cfset var widgetResult  = application.com.formWidgets.getWidgetData(entity=arguments.entity, widgetTextID=row.fieldTextID, widgetArguments=row.evalString)>

			<cfif structCount(widgetResult.buttonAttributes) >
				<cfset variables.formObject.appendButton (argumentCollection = widgetResult.buttonAttributes)>
			</cfif>

			<cfcatch>
				<cfset widgetResult.lineHTML = "Widget FAILED. #cfcatch.message#">
			</cfcatch>
		</cftry>

		<cfif widgetResult.lineHTML is not "">
			<cfset var result = callRelayFormElementDisplay(type="html", html = widgetResult.lineHTML)>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="fieldSource_includeRelayFile" hint="Include a template">
		<cfreturn fieldSource_include (argumentCollection = arguments, basePath = "/relay/")>
	</cffunction>

	<cffunction name="fieldSource_includeUserFile" hint="Include a template">
		<cfreturn fieldSource_include (argumentCollection = arguments, basePath = "/code/")>
	</cffunction>


	<cffunction name="fieldSource_include" hint="Include a template">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">
		<cfargument name="entity" type="any" required="true">
		<cfargument name="basePath" type="string" default="">

		<!--- cannot include a template which requires login security --->
		<cfif request.relayCurrentUser.IsInternal or (FindNocase("admin/", #FieldTextID#) IS 0
			AND FindNocase("flags/", #FieldTextID#) IS 0
			AND FindNocase("data/", #FieldTextID#) IS 0
			AND FindNocase("communicate/", #FieldTextID#) IS 0
			AND FindNocase("emailprocs/", #FieldTextID#) IS 0
			AND FindNocase("report/", #FieldTextID#) IS 0
			AND FindNocase("salesOutData/", #FieldTextID#) IS 0
			AND FindNocase("scheduled/", #FieldTextID#) IS 0
			AND FindNocase("selection/", #FieldTextID#) IS 0)<!--- --->
			<!--- AND FindNocase("templates/", #FieldTextID#) IS 0 --->
			>

			<cfset relativePath = basepath & row.fieldTextID>

			<cfif fileExists (expandPath (relativePath))>
				<cfsetting enablecfoutputonly="no">
					<cfsavecontent variable = "local.result">
						<cfinclude template = #relativePath#>
					</cfsavecontent>
				<cfsetting enablecfoutputonly="yes">
				<cfset variablesSet = local>
				<cfset structDelete (variablesSet,"arguments")>
				<cfset variablesSet = duplicate (variablesSet)>
				<cfset structDelete (variablesSet,"result")>

				<cfset mergeObject.appendMergeStruct (variablesSet)>
			<cfelse>
				<cfset local.result = '<p style="color:##FF0000">The included file ''#htmleditformat(row.FieldTextID)#'' could not be found. </p>'>

			</cfif>

		<cfelse>
			<cfset local.result = '<p style="color:##FF0000">''#htmleditformat(row.FieldTextID)#'' is not a valid page to include.  It may compromise data security.</p>'>
		</cfif>

		<cfreturn callRelayFormElementDisplay(type="html", html = local.result)>

	</cffunction>


	<cffunction name="fieldSource_flaglist" hint="A list of flags">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">
		<cfargument name="entity" type="any" required="true">

		<cfquery name = "local.getFlags">
		select
				 entityTable  as fieldSource
				, flagTextID as fieldTextID
				, 0 as required
				, '' as thisLineCondition
				, f.flag as Label
				, '' as specialFormatting
				, '#row.readonly#' as readonly

		from
			vflagdef f
		where
			f.flagTextID in  (<cf_queryparam value="#row.fieldTextID#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			<!--- TBD could get order correct and probably support the names of boolean groups as well --->
		</cfquery>

		<cfreturn outputFormElementsByDefinition (definition = local.getFlags, entity = entity, mergeObject = mergeObject, readonly = row.readonly )>



	</cffunction>

	<cffunction name="fieldSource_flaggroup" hint="A list of flags">
		<cfargument name="row" type="struct" required="true">
		<cfargument name="mergeObject" type="struct" required="true">
		<cfargument name="entity" type="any" required="true">

		<cfquery name = "local.getFlagGroupItems">
		select
				 entityTable  as fieldSource
				, flagTextID as fieldTextID
				, 0 as required
				, '' as thisLineCondition
				, f.name as label
				, '' as specialFormatting
				, '#row.readonly#' as readonly

		from vflagGroup fg
				inner join
			flag f on f.flagGroupID = fg.flagGroupID
		where
			fg.flagGroupTextID = '#row.fieldTextID#'

		</cfquery>

		<cfreturn outputFormElementsByDefinition (definition = getFlagGroupItems, entity = entity, mergeObject = mergeObject, readonly = row.readonly )>


	</cffunction>


	<cffunction name="callRelayFormElementDisplay">

		<cftry>
			<cfif not variables._renderToStructure>
				<cfreturn application.com.relayDisplay.RelayFormElementDisplay (argumentCollection = arguments)>
			<cfelse>
				<cfreturn collectScreenDefinition (argumentCollection = arguments)>
			</cfif>

			<cfcatch>
				<cfif application.testSite is 2 or true>
					<cfdump var="#cfcatch#">
					<cfdump var="#arguments#">
					<cfabort>
				</cfif>
				<cfrethrow>
			</cfcatch>

		</cftry>

	</cffunction>

	<cffunction name="collectScreenDefinition">

		<cfset arrayAppend (variables.collectScreenDefinitionArray, arguments)>
		<cfreturn "">

	</cffunction>

	<cffunction name="getrenderToStructureResult">

		<cfreturn  variables.collectScreenDefinitionArray>

	</cffunction>

	<cffunction name="structKeyExistsAndNotNull">
		<cfargument name="struct" type="struct" required="true">
		<cfargument name="Key" type="string" required="true">

		<cfreturn  structKeyExists (arguments.struct, arguments.Key) and arguments.struct[arguments.key] is not "">

	</cffunction>


	<cffunction name="structKeyExistsAndEquals">
		<cfargument name="struct" type="struct" required="true">
		<cfargument name="Key" type="string" required="true">
		<cfargument name="value" type="string" required="true">

		<cfreturn  structKeyExists (arguments.struct, arguments.Key) and arguments.struct[arguments.key] is arguments.value>

	</cffunction>


</cfcomponent>