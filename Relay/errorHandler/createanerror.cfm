<!--- �Relayware. All Rights Reserved 2014 --->
<cfif isDefined("showErrors")>
	<cfset application.com.relayCurrentUser.setShowErrors (showDump = iif(showErrors,true,false))>
</cfif>


<cfset errors = [
	"Error calling a coldfusion function",
	"Variable Not Defined Error",
	"SQL Error",
	"Misc Error",			
	"CFInclude Error",
	"Misc Error",			
	"Timeout Error (takes a 10 secs)",				
	"Site wide Error Handler Test",
	"Another Timeout Error ",				
	"Another SQL Error",				
	"An error which is caught and logged",				
	"A warning which is logged",				
	"A Javascript Error",				
	"A translation which has a merge error",
	"Struct key not defined (with name of a setting)",
	"cf_queryparam"
				]>



<cfset numberOfErrors = arrayLen(errors)>

<cfparam name="error" default="0">

<cfoutput>

<form>

<select name="error" onchange="this.form.submit()">
	<option value="">Choose an error
	<cfloop index="i" from = "1" to=#arrayLen(errors)#>
		<option value="#i#" <cfif i is error>SELECTED</cfif> >#htmleditformat(errors[i])#
	</cfloop>
</select>
</form>
You can <a href="?showErrors=#not(request.relayCurrentUser.errors.showDump)#">#iif(request.relayCurrentUser.errors.showDump,de("Log Errors"),de("Dump Errors"))#</a>  by using the URL parameter showErrors=#not(request.relayCurrentUser.errors.showDump)#  <br />
<BR>
<a href="/errorhandler/errorDetails.cfm">This page</a> gives you details of the last error  <br /><br />




<cfswitch expression = #error#>
	<cfcase value= 0>
	
	</cfcase>
	<cfcase value= 1>
		#right("william",0)#
	</cfcase>
	
	<cfcase value= 2>
		#htmleditformat(xxxx)#
	</cfcase>

	<cfcase value= 3>
		<cfquery datasource = "#application.sitedatasource#">
		insert into booleanflagdata (flagid, entityid) values (1,1)
		</cfquery> 
	</cfcase>

	<cfcase value= 4>
		<cfset x = application.com.relayincentive.addRWTransactionItems()>
	</cfcase>

	<cfcase value= 5>
		<cfinclude template="xx">
	</cfcase>
	
	<cfcase value= 6>
			<cfset x = listgetat("a",0)>
	</cfcase>

	<!--- This creates a timeout error --->
	<cfcase value= 7>
			<!--- set a very short timeout 
			then run a query which goes well past this timeout
			--->
			<cfsetting requesttimeout="1">  
			<cfquery datasource = "#application.sitedatasource#">
			waitfor  delay '00:00:10'
			</cfquery> 
	
			<cfquery name="x" datasource = "#application.sitedatasource#">
			select 1
			</cfquery>

	</cfcase>

	<cfcase value= 8>
			<BR><BR>
		Site-Wide Error Handler Test <BR>
		In a few seconds this page will redirect to a non-compling template, you should get a sensible error message
		<script>
			function redirect() {
				window.location.href = '/errorHandler/createAnErrorBadTemplate.cfm'
			}
			window.setTimeout(redirect,2000)
		
		</script>
	
	</cfcase>

	<cfcase value= 9>
			<cfsetting requesttimeout="1">
			<cfquery datasource = "#application.sitedatasource#" timeout = 1>
			update person set lastname = lastname where personid = #request.relaycurrentuser.personid#
			</cfquery> 

			<cfquery datasource = "#application.sitedatasource#">
			select * from person where personid = #request.relaycurrentuser.personid#
			</cfquery> 

	</cfcase>

	<cfcase value= 10>
		<cfquery datasource = "#application.sitedatasource#" >
			select x from person
		</cfquery>
	</cfcase>
	
	<cfcase value= 11>
		<cftry>
			<cfquery datasource = "#application.sitedatasource#" >
				select x from person
			</cfquery>
			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_warning(catch=cfcatch,sendemail = true,addUserToDataStruct = true)>
				<cfoutput><BR><BR>This Error has been caught and an email sent!</cfoutput>
			</cfcatch>	
		</cftry>	
	</cfcase>


	<cfcase value= 12>
				<cfset warningStructure = {a=1,b=2}>
				<cfset application.com.errorHandler.recordRelayError_warning(warningStructure = warningStructure,sendemail = true,addUserToDataStruct = true,message = "This is a Test Warning")>
				<cfoutput><BR><BR>This isn't an error, just a warning written to the logs </cfoutput>
	</cfcase>


	<cfcase value= 13>

			<script type="text/javascript" src="/errorHandler/createAJavasciptError.js" ></script>
			
			<script>
			function badFunction () {
				nonExistentObject = document.getElementById('x')
				alert (nonExistentObject.x)
			}
			window.setTimeout(badFunction,1000)
			
			anotherBadFunction()
			</script>
			<p>
			

			<cfif request.relayCurrentUser.errors.showDump>
				Javascript errors are not being logged at the moment.<br />
				We will now set errors to be logged and re-load.<br />
				<cfset application.com.relayCurrentUser.setShowErrors (showDump = false)>				
				<script>
				function redirect() {
					window.location.href = '/errorHandler/createAnError.cfm?error=#error#'
				}
				window.setTimeout(redirect,2000)
				</script>
			<cfelse>
				2 Javascript errors should have been logged.  Use the link above to check<br />
			
			</cfif>

	
	</cfcase>
	
	<cfcase value= 14>
				<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextId="ATestPhrase",phraseText="This is a test phrase with some merge fields: A=[[a]]; B=[[B]] ")>
				<cfset mergeStruct = {a=1}>
				<cf_translate mergeStruct=#mergeStruct#>
					phr_ATestPhrase
				</cf_translate>
	</cfcase>
	
	<cfcase value= 15>
				<cfset This_Is_A_Test_Of_The_ErrorHandler_Please_Ignore = {}>
				<cfset y = This_Is_A_Test_Of_The_ErrorHandler_Please_Ignore.clientName>
	</cfcase>

	<cfcase value= 16>
			<cf_queryparam value="a" cfsqltype="cf_sql_integer">
	</cfcase>


<a href="/errorhandler/createAJavascriptError.cfm">This page</a>  will create a javascript error </a> <br />


</cfswitch>

</cfoutput>
 
