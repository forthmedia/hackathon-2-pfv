<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			.cfm
Author:
Date started:			/xx/07

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-09-26 	WAB 	A few changes to support improved reporting of Javascript Errors
2012-10-05	WAB		Removed stuff to do with comments and fixes which we aren't using
2013-05-28 	WAB		Added link to toggle showLeadingWhitespace setting  (and tidied up other debug toggles)
2016-01-13	WAB		PROD2015-515 Remove the toggles to new \debug.cfm
					Previous and Next Links can now stay with same session

Possible enhancements:


 --->


<cfif request.relayCurrentUser.errors.showLink>

	<CF_PARAM name="errorID" default="0" type="numeric">
	<CFPARAM name="visitid" default="0" type="numeric">
	<CFPARAM name="nextPrevious" default="0" type="numeric">

	<cfif errorid is 0>
		<cfquery name="getLastError" datasource = "#application.sitedatasource#">
		select
			max(relayerrorid) as relayErrorID
		from
			relayerror e
		<cfif visitid is not 0>
		where
			e.visitid =  <cf_queryparam value="#visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>

		</cfquery>
		<cfset errorid = getLastError.relayErrorID>
	</cfif>


	<cfif nextPrevious is not 0>
		<cfquery name="qryNextPrevious" datasource = "#application.sitedatasource#">
		select
			<cfif abs(nextPrevious) GT 1>
				#(nextPrevious GT 0)?"max":"min"#
			<cfelse>
				#(nextPrevious GT 0)?"min":"max"#
			</cfif>
				(relayErrorID)  as id
		from
			relayerror  e
		where
			1 = 1
			<cfif abs(nextPrevious) EQ 1>
				and e.relayerrorid 	#(nextPrevious GT 0)?">":"<"#  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfif visitid is not 0>
				and e.visitid =  <cf_queryparam value="#visitid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>

		</cfquery>

		<cfif qryNextPrevious.id is not "">
			<cfset errorid = qryNextPrevious.id>
		<cfelse>
			<cfoutput>#application.com.relayUI.message(message = '#(nextPrevious GT 0)?"Last":"First"# Error', type="warning")#</cfoutput>
		</cfif>

	</cfif>


	<cfquery name="thisError" datasource = "#application.sitedatasource#">
	select
		e.*
	from
		relayerror  e
	where
		e.relayerrorid =  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfif thisError.recordCount is 0>
		<cfoutput>#application.com.relayUI.message("Error #errorID# not found","error")#</cfoutput>
		<cf_abort>
	</cfif>


	<cfquery name="sameError" datasource = "#application.sitedatasource#">
	select
		top 10 e2.personid, e2.errordatetime,e2.diagnostics,e2.relayErrorid
	from
		relayerror  e
			inner join
		relayerror e2 on e.relayErrorFixID = e2.relayErrorFixID  and e.relayerrorid <> e2.relayerrorid
	where
		e.relayerrorid =  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and e.relayErrorFixID is not null
	order by e2.errorDateTime Desc
	</cfquery>

	<cfquery name="similarErrors" datasource = "#application.sitedatasource#">
	select
		top 10 e2.personid, e2.errordatetime,e2.diagnostics,e2.relayErrorid
	from
		relayerror  e
			left join
		relayerror e2 on e.filename = 	e2.filename and e.linenumber = e2.linenumber and e.relayerrorid <> e2.relayerrorid  and e.filename <> ''
				and (e2.relayErrorFixID is null or isnull(e.relayErrorFixID,0) <> isNull(e2.relayErrorFixID,0) )
			left join
		relayErrorFix f2 on e2.relayErrorFixID = f2.relayErrorFixID
	where
		e.relayerrorid =  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and e2.errorDateTime > getdate()-30
		and f2.relayErrorFixID is Null  -- ie not fixed

	union

	select
		top 10 e2.personid, e2.errordatetime,e2.diagnostics,e2.relayErrorid
	from
		relayerror  e
			left join
		relayerror e2 on e.filename = 	e2.filename and e.linenumber = e2.linenumber and e.relayerrorid <> e2.relayerrorid and e.filename <> ''
				and (e2.relayErrorFixID is null or isnull(e.relayErrorFixID,0) <> isNull(e2.relayErrorFixID,0) )
			left join
		relayErrorFix f2 on e2.relayErrorFixID = f2.relayErrorFixID
	where
		e.relayerrorid =  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and e2.errorDateTime > getdate()-30
		and f2.relayErrorFixID is not Null	-- ie fixed

	order by
		e2.errorDateTime Desc
	</cfquery>

	<cfif isDefined("showSimultaneousErrors")>
		<cfquery name="SimultaneousErrors" datasource = "#application.sitedatasource#">
			select top 10 e2.personid, e2.errordatetime,e2.diagnostics,e2.relayErrorid ,e2.relayErrorFixid , f2.status from
			relayerror e2
				left join
			relayErrorFix f2 on e2.relayErrorFixID = f2.relayErrorFixID
		where
		e2.errorDateTime between #dateAdd("n",-5,thisError.errorDateTime)# and #dateAdd("n",5,thisError.errorDateTime)#
		and e2.relayerrorid <>  <cf_queryparam value="#errorid#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfif similarErrors.recordcount is not 0>
			and e2.relayerrorid  not in ( <cf_queryparam value="#valuelist(similarErrors.relayerrorid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
		order by e2.errorDateTime asc
		</cfquery>
	</cfif>


	<cf_includejavascriptonce template="/javascript/checkboxfunctions.js">

<cfoutput>

	<BR>
	<form name="theForm">
	<table>

		<TR>
			<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
				<cfif visitID is not 0>
					<a href="?errorid=#errorid#&nextprevious=-2&visitid=#visitid#">First</a> &nbsp;&nbsp;&nbsp;
				</cfif>
				<a href="?errorid=#errorid#&nextprevious=-1&visitid=#visitid#">Previous</a> &nbsp;&nbsp;&nbsp;
				#htmleditformat(thiserror.relayErrorID)# &nbsp;&nbsp;&nbsp;
				<a href="?errorid=#errorid#&nextprevious=1&visitid=#visitid#">Next</a>&nbsp;&nbsp;&nbsp;
				<a href="?errorid=#errorid#&nextprevious=2&visitid=#visitid#">Last</a> &nbsp;&nbsp;&nbsp;
				<br />
				<cfif visitID is 0>
					Showing All Errors.<br />
					<a href="?visitId=#request.relaycurrentuser.visitid#&errorid=#errorid#&nextPrevious=1">Show Errors in My Session</a><br />
					<a href="?visitId=#thisError.visitid#&errorid=#errorid#&nextPrevious=1">Show Errors in Same Session As This Error</a><br />
				<cfelse >
					Showing Errors In <cfif visitID is request.relaycurrentuser.visitid>This<cfelse>A Specific</cfif> Session. <br />
					<a href="?visitId=0&errorid=#errorid#&nextPrevious=1">Show Errors For All Sessions</a>
				</cfif>
			</TD>
		</TR>


		<TR>
			<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
				&nbsp;
			</TD>
		</TR>

		<TR>
			<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
				Source: #thisError.Source#
			</TD>
		</TR>

		<TR>
			<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
				Type: #thisError.Type#
			</TD>
		</TR>
		<tr><th></th><th>Diagnostics</th><th>Date</th><th>PersonID</th><th>Link</th></tr>
			<tr><td colspan="4"><strong>This Error</strong></td></tr>
			<tr><td></td><td>#htmleditformat(thiserror.diagnostics)#</td><td>#htmleditformat(thiserror.errorDateTime)#</td><td>#htmleditformat(thiserror.personid)#</td><td></td></tr>

		<cfif sameError.recordcount is not 0>
			<tr><td colspan="4"><strong>Same Error</strong></td></tr>
			<cfloop query = "sameError">
				<tr><td></td><td>#htmleditformat(diagnostics)#</td><td>#htmleditformat(errorDateTime)#</td><td>#htmleditformat(personid)#</td><td><A HREF="#request.currentSite.httpProtocol##cgi.http_host#/errorHandler/errorDetails.cfm?errorid=#relayErrorID#">Details</A></td></tr>
			</cfloop>
		</cfif>

		<cfif similarErrors.recordcount is not 0><tr><td colspan="4"><strong>Other similar recent errors </strong></td></tr>
			<tr><Td><a href="javascript:toggleCheckboxes(document.theForm,'otherErrorIDs',/.*/)">all</a></TD></tr>
			<cfloop query = "similarErrors">
				<tr><td></td><td>#htmleditformat(diagnostics)#</td><td>#htmleditformat(errorDateTime)#</td><td>#htmleditformat(personid)#</td><td><A HREF="#request.currentSite.httpProtocol##cgi.http_host#/errorHandler/errorDetails.cfm?errorid=#relayErrorID#">Details</A></td></tr>
			</cfloop>
		</cfif>

		<cfif isDefined("showSimultaneousErrors") and SimultaneousErrors.recordcount is not 0>
			<tr><td colspan="4"><strong>Other Simultaneous Errors</strong></td></tr>
			<cfloop query = "SimultaneousErrors">
				<tr><td></td><td>#htmleditformat(diagnostics)#</td><td>#htmleditformat(errorDateTime)#</td><td>#htmleditformat(personid)#</td><td><A HREF="#request.currentSite.httpProtocol##cgi.http_host#/errorHandler/errorDetails.cfm?errorid=#relayErrorID#">Details</A></td></tr>
			</cfloop>

		<cfelse>
			<tr><td colspan=3><A href="?errorID=#errorID#&&visitid=#visitid#&showSimultaneousErrors=1">Show any error within 5 minutes of this error</A> </td></tr>
		</cfif>



	</table>
	</form>

	<BR><BR><BR>
</cfoutput>

	<cfif thisError.datastruct is not "">
		<cfset deserialiseddataStruct = {}>
		<cftry>
			<cfif isJSON (thisError.datastruct)>
				<cfset deserialiseddataStruct = deserializeJSON(thisError.datastruct)>
			<cfelseif isWDDX(thisError.datastruct)>
				<CFWDDX ACTION="WDDX2CFML" INPUT="#thisError.datastruct#" OUTPUT="deserialiseddataStruct">
			</cfif>

			<cfcatch>
				<cfoutput>Unable to deserialise the Error Structure<BR></cfoutput>
				<!--- This occurs if the error structure is very large and SQL doesn't bring the whole of the field back --->

			</cfcatch>
		</cftry>

		<cfif structKeyExists (deserialiseddataStruct,"user")>
			<cfset application.com.errorHandler.outputUserDetails (deserialiseddataStruct.user)>
		</cfif>

		<cfif structKeyExists (deserialiseddataStruct,"cferror")>
			<cfset errorStruct = deserialiseddataStruct.cferror>
			<cfset application.com.errorHandler.outputErrorWithDebug (errorStruct)>
		</cfif>

		<cfif structKeyExists (deserialiseddataStruct,"sourcefile")  and  structKeyExists (deserialiseddataStruct,"jserror")>
			<cfoutput><PRE>#application.com.errorHandler.outputErrorLines(deserialiseddataStruct.sourcefile)#</PRE></cfoutput>
		</cfif>

		<cfif structKeyExists(deserialiseddataStruct,"form")>
			<cfset FormStruct = deserialiseddataStruct.form>
			<cfoutput>Form Variables</cfoutput>
			<cfdump var="#deserialiseddataStruct.form#">
			<!--- Bit of a hack to show values which have been stored in the formState variable and to remove the singleSession attribute if it is there--->
			<cfif structKeyExists(FormStruct,"formSTATE")>
				Values in FormState Variables<BR>
				<cfset newFormStateString = "">
				<cfloop index="thisString"  list = "#FormStruct.formSTATE#">
					<cfset formStateResult = application.com.security.decryptFormStateVariable(formStateString=thisString)>
						<cfif formStateResult.isOK>
							<cfdump var="#formStateResult.result.fields#">
							<cfset structDelete (formStateResult.result,"singleSession")>
							<cfset structDelete (formStateResult.result,"SessionID")>
							<cfset newFormStateResult = application.com.security.createFormStateVariable(formStateResult.result.fields)>
							<cfset newFormStateString = listappend(newFormStateString,newFormStateResult)>
						</cfif>
				</cfloop>
				<cfset FormStruct.formSTATE = newFormStateString>
			</cfif>
		</cfif>

		<cfif structKeyExists(deserialiseddataStruct,"URL")>
			<cfoutput>URL Variables</cfoutput>
			<cfdump var="#deserialiseddataStruct.url#">
		</cfif>


		<cfif structKeyExists (deserialiseddataStruct,"cferror")>
			<cfoutput>
				This form should enable you to reproduce the error<BR>

				<cfset submitTo = "#htmleditFormat(thisError.domain)#/#htmleditformat(listfirst(thisError.template,"?"))#"> <!--- template field sometimes gets trucncated so does not have all the url variables, so recreate from the URL structure --->
				<cfif structKeyExists(deserialiseddataStruct,"URL")>
					<cfset submitTo = listappend(submitTo,application.com.structureFunctions.convertStructureToNameValuePairs(struct = deserialiseddataStruct.URL,delimiter="&",encode="URL"),"?")>
				</cfif>

				Submitting to  #htmlEditFormat(submitTo)#<BR>
				<form action="http://#htmlEditFormat(submitTo)#" method = "post" target="_new">

					<cfif structKeyExists(deserialiseddataStruct,"form")>
						<table>
						<!--- remove _sessionToken, a new one will be put in (but won't work if going to another domain - oops) --->
						<cfset structDelete (formStruct,"_rwSessionToken")>
						<cfloop item="field" collection="#formStruct#">
								<tr><td>#htmleditformat(field)#</td><td> <CF_INPUT type="text" name="#field#" value="#formStruct[field]#"></td></tr>
						</cfloop>
						</table>
					</cfif>
					<BR><input type="submit" value="Recreate Error!">
				</form>
			</cfoutput>
		</cfif>

		<!--- Dump any Keys not specifically dealt with --->
		<cfloop item="key" collection="#deserialiseddataStruct#">
			<cfif not listFindNoCase("form,url,user,cferror,sourcefile", key)>
				<cfoutput>#htmleditformat(key)#<BR></cfoutput>
				<Cfif structKeyExists(deserialiseddataStruct,key)>
					<cfdump var="#deserialiseddataStruct[key]#"><BR><BR>
				</cfif>
			</cfif>
		</cfloop>

		<cfif structKeyExists(deserialiseddataStruct,"user")>
			<cfoutput>User Structure</cfoutput>
			<cfdump var="#deserialiseddataStruct.user#">
		</cfif>

	</cfif>


<cfelse>
	Only accessible to Relayware Inc
</cfif>



