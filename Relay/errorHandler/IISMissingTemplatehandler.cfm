<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			missingTemplateHandler.cfm
Author:				WAB
Date started:			2009/06/29

Description:
Idea for handling secure files via the missing template handler
Still under development
Needs to be combined with code for the regular 404 error  - ie external404.cfm



Amendment History:

Date 		Initials 	What was changed
2013-02-19	PPB			Case 433662  use phrases for error messages

Possible enhancements:


NOTE requires opening in directorySecurity.xml
NOTE requires IIS changes



 --->

 <!--- IIS passes a url variable with the name which looks something like this
	404;HTTP://DEV-CFINT/TEST/WILLIAM/SECURITY/WABTEST.XX
	may also have the port number afer the domain
	So we are going to have to loop through the URL structure looking for this pattern

	if there are URL parameters then the first one is tacked onto the end of the 404 Variable and the value of the parameter is the value of the 404 variable,
	 but other parameters appear normally

 	problem with cgi.query_string not dealing with non ascii characters,
	so have to loop through the url collection
	Not sure that I can guarantee that the 404 variable will be the first

 --->
<cfsetting enablecfoutputonly="Yes" showdebugoutput="false">
<cfif listFirst (cgi.query_string,";") is "404">


	<cfset originalQueryString = "">

	 <cfloop collection = "#url#" item="item">

		 <cfif listFirst (item,";") is "404">

			<cfset originalURLWithQueryString = listRest (item,";")>
			<cfset originalURL =  listFirst (originalURLWithQueryString,"?")>

			<cfif listLen (originalURLWithQueryString,"?") gt 1>
				<cfset originalQueryString =  originalQueryString & "&" & listRest (originalURLWithQueryString,"?") & "=" & url[item]>
			</cfif>

		  	<cfset regExpResult =  application.com.regExp.refindAllOccurrences ("http[s]*://#request.currentsite.domainAndRoot#[\:]*[0-9]*(.*)",originalURL)>
			<cfset scriptName = regExpResult[1][2]>

		 <cfelse>
			<cfset 	 originalQueryString = originalQueryString & "&" & item & "=" & url[item]>
		 </cfif>

	 </cfloop>


			<!--- Now have the script name and the queryString --->

				<!--- is this a secure file --->
				<!--- must start with content --->
				<!--- this function does the secure file stuff, really need a function which returns something to us here! --->
					<cfset fileDetails = application.com.filemanager.getFileIDFromScriptName (scriptName)>
					<cfif fileDetails.isOK>
						<cfset application.com.filemanager.manageSecuredFileSecurity(scriptName)>
						<CF_ABORT>
					<cfelse>
						<!--- What else might it be!
							If it is looking for a file in the core directories which is not there then we could redirect to that same file in the content or code directory
						--->
						<cfset originalScriptName = scriptName>
						<cfset scriptName = replaceNoCase(scriptname,"/content/","/")> <!--- NJH 2010/08/13 - I don't think content is supposed to be part of the script, as we then repeat content because it's already in the application.paths.content variable --->
						<cfif originalQueryString is not "">
							<cfset originalQueryString  = "?" & originalQueryString >
						</cfif>

						<cfif fileExists (application.paths.content & scriptname)>
							<!--- <cfinclude template="/content/#scriptName#"> --->
							<cflocation url="/content#scriptName##originalQueryString#" addtoken="false">
							<CF_ABORT>
						<!--- NJH 2010/08/16 look in the code directory if the file is not available in content.. this is more for handling files in userfiles or scheduled task that may be looking at the old content directory, when in fact the code has now moved to 'code' --->
						<cfelseif fileExists(application.paths.code & scriptname)>
							<cflocation url="/code#scriptName##originalQueryString#" addtoken="false">
							<CF_ABORT>
						<cfelseif fileExists (application.paths.relay & "/defaultRootDirectory" & scriptname)>
							<cflocation url="/defaultRootDirectory#scriptName##originalQueryString#" addtoken="false">
							<CF_ABORT>
						</cfif>

					</cfif>
					<cfheader statusCode = "404"   statusText = "Not Found"> <!--- 2016-09-06 GCC PROD2016-2300/451486 remove #htmleditformat(originalScriptName)# as a security risk of reflecting user entered data --->
					<cfoutput>404 phr_sys_error_TemplateNotFound</cfoutput>	<!--- 2013-02-19 PPB Case 433662 use phrase --->

				<!--- we are going to log the file not found
					however certain paths (such as cfdebug looking for images in /cfide) can be ignored
					--->
				<cfset regExpForNotWarning = "/CFIDE/.*GIF|favicon\.ico">

				<cfif refindNoCase(regExpForNotWarning,originalScriptName) is 0>
					<cfset application.com.errorHandler.recordRelayError_Warning (Type="Missing File",Message="Missing File: " & originalScriptName,addToDataStruct="" )>
				</cfif>


</cfif>








