<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2011/02
SitewideErrorHandler\SitewideErrorHandler.cfm

This is a fall back error handling template.
Most error handling is in applicationMain.cfc
This probably only called if there is a CF Template which does not compile

Some Points to note
1.  there can only be one of these on a cold server instance, even if the instance is running more than one version of relayware
2.	CF Admin has to point to this template using a relative path, therefore it will be necessary to set up a mapping \sitewideerrorhandler.
3.	No application.cfc is run so we have to get applicationScope ourselves
4.	No customTags or mappings are available (so we for example couldn't include a file from \code - although we could cffile it)


 --->

		<cfif structKeyExists (server,"serverInitialisationObj")>

			<cfset siteDetails = server.serverInitialisationObj.getSiteForThisDomain()>

			<cfif siteDetails.isOK>
				<cfset appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
				<cfset applicationScope = appObj.getApplicationScope(siteDetails.cfappname)>
	
				<cfset errorHandlerObject = applicationScope.com.errorHandler>
		
				<cfset exception = cferror>		
					<cfif structKeyExists (exception,"rootcause")>
						<cfset exception = exception.rootcause>
					</cfif>



					<cfset 	errorHandlerObject.handleError (exception = exception)	>

			
			<cfelse>
			
				Error

			</cfif>


		</cfif>


