<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			ValidValueTask.cfm
Author:			Simon WJ
Date created:	24 November 1999

Description:
This provides a mechanism for inserting or updating a record in the ValidFieldValues table.
It expects a form variable frmButton which tells it whether it should insert or update
the record it is being passed.  It works in close conjunction with ValidValue.cfm which it calls
at the end as an included file.

Version history:
1  Initial version supported certain fields.
2  SWJ 7-Feb-2000 version supporte more fields
 WAB 250200 corrected problem if frmInAnalysis not defined
WAB 2007-02-20 added nvarchar support
 
--->

<CFPARAM NAME="frmCase" DEFAULT="T">
<CFPARAM NAME="frmDefault" DEFAULT="0">
<CFPARAM NAME="frmInAnalysis" DEFAULT="0">
<CFPARAM NAME="Debug" DEFAULT="No">

<!--- Debug info for troubleshooting --->
<CFIF Debug IS 'Yes'>
	<CFOUTPUT>
		<FONT SIZE="-6">
		<CFIF isDefined('frmValidFieldID')>
			frmValidFieldID = #htmleditformat(frmValidFieldID)#
		</CFIF>
		frmValidValue = #htmleditformat(frmValidValue)#, 
		frmFieldName = #htmleditformat(frmFieldName)#, 
		frmCase = #htmleditformat(frmCase)#, 
		frmOrder = #htmleditformat(frmOrder)#, 
		frmCountryID = #htmleditformat(frmCountryID)#, 
		frmDefault = #htmleditformat(frmDefault)#, 
		datavalue = '#htmleditformat(frmDataValue)#', 
		InAnalysis = #htmleditformat(frmInAnalysis)#, 
		Score = #htmleditformat(score)#
		TableName =#ListFirst(frmFieldName,".")#</FONT>
	</CFOUTPUT>
</CFIF>


<CFIF #frmButton# IS "Update">
	<CFQUERY NAME="UpdateValidValues" DATASOURCE="#application.SiteDataSource#">
		Update ValidFieldValues
		Set ValidValue =  <cf_queryparam value="#frmValidValue#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			SortOrder =  <cf_queryparam value="#frmOrder#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			DefaultFieldCase =  <cf_queryparam value="#frmCase#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			DefaultFieldValue =  <cf_queryparam value="#frmDefault#" CFSQLTYPE="CF_SQL_smallint" > ,
			datavalue =  <cf_queryparam value="#frmDataValue#" CFSQLTYPE="CF_SQL_VARCHAR" > , 
			InAnalysis =  <cf_queryparam value="#frmInAnalysis#" CFSQLTYPE="CF_SQL_bit" > , 
			Score =  <cf_queryparam value="#frmscore#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		Where ValidFieldID =  <cf_queryparam value="#frmValidFieldID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
<CFELSE>
	<cfset frmFieldName = Replace(Replace(#frmFieldName#," ","","All"),"'","","All")> <!--- NJH 2006/06/23  Remove any spaces or single quotes (required for the elements in LeadCapture form to be displayed properly) --->
	<CFQUERY NAME="InsertValidValues" DATASOURCE="#application.SiteDataSource#">
		INSERT into ValidFieldValues
			( ValidValue, SortOrder, FieldName, CountryID, 
			DefaultFieldCase, DefaultFieldValue, LanguageID, datavalue, InAnalysis, Score)
		VALUES
			(<cf_queryparam value="#frmValidValue#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmOrder#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmFieldName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
			<cf_queryparam value="#frmCase#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmDefault#" CFSQLTYPE="CF_SQL_smallint" >, 1, <cf_queryparam value="#frmDataValue#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmInAnalysis#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#frmscore#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</CFQUERY>
</CFIF>

<cfinclude template="/dataTools/ValidValues.cfm">






