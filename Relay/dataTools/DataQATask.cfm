<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Debug info for troubleshooting --->
<CFPARAM NAME="Debug" DEFAULT="No">
<CFIF Debug IS 'Yes'>
	<CFOUTPUT>
		<FONT SIZE="-6">frmOldValue = #htmleditformat(frmOldValue)#, 
		frmNewValue = #htmleditformat(frmNewValue)#, 
		frmFieldName = #htmleditformat(frmFieldName)#, 
		frmRadio = #htmleditformat(frmRadio)#, 
		frmCountryID = #htmleditformat(frmCountryID)#, 
		TableName =#ListFirst(frmFieldName,".")#</FONT>
	</CFOUTPUT>
</CFIF>

<CFSET TableName =#ListFirst(frmFieldName,".")#>
<CFSET updatetime = CreateODBCDateTime(Now())>

<CFQUERY NAME="UpdateFieldName" DATASOURCE="#application.SiteDataSource#">
UPDATE #TableName#  	
	<CFIF #frmRadio# IS "Upper">
		set #frmFieldName# =  <cf_queryparam value="#Ucase(Replace(frmOldValue,"'","''","All"))#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	<CFELSEIF #frmRadio# IS "Lower">
		set #frmFieldName# =  <cf_queryparam value="#LCase(Replace(frmOldValue,"'","''","All"))#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	<CFELSE>
		set #frmFieldName# =  <cf_queryparam value="#frmNewValue#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFIF>
		, lastupdatedBy = #request.relayCurrentUser.usergroupid#,
		LastUpdated =  <cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	<CFIF #TableName# IS "Person">
   	FROM person, location 
   	</CFIF>
	<CFIF #frmSelectionID# IS NOT 0>
			, selectiontag 
	</CFIF> 

   		WHERE  #frmFieldName# =  <cf_queryparam value="#frmOldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > 
   	<CFIF #TableName# IS "Person">
   		AND location.locationID = person.locationID
   	</CFIF>
	<CFIF #frmSelectionID# IS NOT 0>
		AND selectiontag.entityid = Person.personid 
		AND selectiontag.selectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF> 

   		AND location.countryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	
	select @@rowCount as recsUpdated
</CFQUERY>

<cfset message = "#UpdateFieldName.recsUpdated# #TableName# records updated.">

<CFQUERY NAME="InsertValidationRule"
        DATASOURCE="#application.SiteDataSource#">
	INSERT into ValidationRules (FieldName, CountryID, OldValue, NewValue)
	VALUES (<cf_queryparam value="#frmFieldName#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmOldValue#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmNewValue#" CFSQLTYPE="CF_SQL_VARCHAR" >)
</CFQUERY>


<cfinclude template="/dataTools/DataQA.cfm">




