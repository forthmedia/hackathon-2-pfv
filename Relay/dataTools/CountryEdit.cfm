<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Name: CountryEdit.cfm

Author: WAB

Date: 2000-06-01

Purpose:
	Displays a screen for editing Country Table fields and flags.
	Makes use of the <CF_ascreen> tag system
	Requires a screen definition

Mods:

2008-07-08 NJH	Bug Fix T-10 Issue 583. Changed form to cfform.
2008-07-30 NYF	Bug Fix T-10 Issue 583: Changed country listing to ordered by: 
				CountryGroup then Country, then these both listed alphabetically
2009-02-24 NYB	Sophos Stargate 2
2009-02-27	NYB	bug 1902 removed "Select A Value" from the Country drop down - as selecting this throws an error
2009-04-07	NYB	Sophos -	onChange doesn't work when you add more then 1 flag to the Country Edit screen
							removed auto flaglist
2011-05-25	NYB		REL106 added Locator Definition Screen here

 --->



<cf_ascreenupdate>

<cfparam name="frmcountryid" default= "#request.relaycurrentuser.countryID#">
<cfparam name="Locatorid" default= "0">
<cfparam name="frmAddNew" default= "0">

<cfset LocatorScreenExists = "false">

<cfif application.com.relayMenu.isModuleActive(moduleID=36)>
	<cfset LocatorScreenExists = application.com.screens.doesScreenExist(CurrentScreenId="CountryLocatorScreen")>
</cfif>

<cfset frmmethod= "edit">

<cfquery name="getCountries" datasource="#application.sitedatasource#">
	select countryid, countrydescription from (
		select 0 as sortorder, countryid, countrydescription,
		ISOCode
		from country inner join countryGroup on country.countryID = countryGroup.countryGroupID
		where isNull(isocode,'') = ''	and countryMemberID in (#request.relaycurrentuser.countrylist#) 
		AND countryDescription = 'All Countries'
	UNION
		select 1 as sortorder, countryid, countrydescription,
		ISOCode
		from country inner join countryGroup on country.countryID = countryGroup.countryGroupID
		where isNull(isocode,'') = ''	and countryMemberID in (#request.relaycurrentuser.countrylist#)
		AND countryDescription != 'All Countries'
	UNION
		select 2 as sortorder, countryid, countrydescription,
		ISOCode
		from country 
		where isNull(isocode,'') <> ''	
		and countryid in (#request.relaycurrentuser.countrylist#)
	) as d 
	order by Sortorder, countrydescription
</cfquery>

<cfquery name="country" datasource="#application.sitedatasource#">
	select * from country where countryid =  <cf_queryparam value="#frmcountryid#" CFSQLTYPE="CF_SQL_INTEGER" >  
</cfquery>

<cfif LocatorScreenExists>
	<cfset LocatorDef = application.com.relayLocator.getLocatorDefinition(countryID=frmcountryid, cache="false")>
	<cfset LocatorDefPK = application.entitytype[application.entitytypeid["LocatorDef"]].UNIQUEKEY>
	<cfif LocatorDef.recordcount gt 0>
		<cfquery name="LocatorDef" datasource="#application.sitedatasource#">
			select top 1 * from LocatorDef where LocatorID  in ( <cf_queryparam value="#valuelist(LocatorDef.LocatorID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
		</cfquery>
		<cfset "#LocatorDefPK#" = evaluate("LocatorDef.#LocatorDefPK#")>
		<cfset frmAddNew="0">
	<cfelse>
		<cfif frmAddNew>
			<cfset LDefColumns = application.com.dbtools.getTableDetails(tablename='LocatorDef',returncolumns="true",excludeColumn=LocatorDefPK)>
			<cfquery name="LocatorDef" datasource="#application.sitedatasource#">
				select cast(#LocatorDefPK# as varchar(100)) as #LocatorDefPK#,
				#valuelist(LDefColumns.Column_Name)# from LocatorDef 
				where LocatorID = 0 
			</cfquery>
			<cfset x = QueryAddRow(LocatorDef)>
			<cfset x = QuerySetCell(LocatorDef,"CountryGroupID",frmcountryid)>
			<cfset x = QuerySetCell(LocatorDef,"#LocatorDefPK#","addnew")>
		</cfif>
	</cfif>
</cfif>


<cf_head>
	<cf_title>Countries</cf_title>

	<script>
	function move(index) {
		form= document.mainForm
		currentSelected = form.frmCountryID.selectedIndex
		thisLength = form.frmCountryID.length
			// check that not past begining or end of the list (note that item one isn't allowed)
			if (currentSelected + index > -1 && currentSelected + index < thisLength) {
				form.frmCountryID.selectedIndex	= currentSelected + index
				submitCountryEditForm()	//NYB 2009-04-07 renamed from submitForm
			} else {
				alert('Sorry, you\'ve reached the end!')
			}
		
	}
	function viewentityFlags(entityID,entityTypeID,flagGroupID,frmBack){ 
		var form = document.flagListForm;
		form.frmcurrentEntityID.value = entityID;
		form.frmEntityType.value = entityTypeID;
		form.frmflagGroupID.value = flagGroupID;
		form.frmBack.value = frmBack;
		form.frmNext.value = frmBack;
		form.submit();	
	}
	//NYB 2009-04-07 renamed from submitForm
	function submitCountryEditForm() {
		triggerFormSubmit(document.mainForm)
	}

	function addLocatorDef() {
		var form = document.mainForm;
		form.frmAddNew.value = '1';
		document.mainForm.submit()	
	}

	function goToCountry(CountryIDVar) {
		var form = document.mainForm;
		form.frmCountryID.value = CountryIDVar;
		document.mainForm.submit()	
	}

	</script>

</cf_head>

<cfset application.com.request.setTophead(pageTitle="Edit Standard Country Data")>

<cfparam name="message" default="">

<cfif message neq "">
	<cfoutput>
		#application.com.relayui.message(message=message,type='success')#
	</cfoutput>
</cfif>

<form name="mainForm" method="post" >
	<cf_relayformdisplay>
		<cf_relayFormElementDisplay type="html">

				<!--- NYB 2009-02-27 bug 1902 - added showNull = "false" --->
				<!--- NYB 2009-04-07 renamed submitForm to submitCountryEditForm  --->
				<cf_displayValidValues 
					validvalues = "#getCountries#"
					dataValueColumn = "countryid"
					displayValueColumn = "countryDescription"
					formfieldname = "frmCountryID"
					currentvalue = "#frmCountryID#"
					onchange = "javascript:submitCountryEditForm()" 
					showNull = "false"
				>	
		</cf_relayFormElementDisplay>
		
		<div class="form-accordion-heading"><h3>Country Data</h3></div>
		<div class="form-accordion-body">

			<cf_ascreen formname = "mainForm">
				<cf_ascreenitem screenid="countryEdit" country="#country#" method="edit">
			</cf_ascreen>

		</div>
		
		<cfif LocatorScreenExists>
			<div class="form-accordion-heading"><h3>Locator Data</h3></div>
			<div class="form-accordion-body">			
				<cf_input name="LocatorID" value="#LocatorID#" type="hidden">		
				<cf_input name="frmAddNew" value="#frmAddNew#" type="hidden">		
		
				<cfif frmAddNew or (LocatorDef.RecordCount gt 0 and LocatorDef.CountryGroupID eq frmCountryID)>
					<cf_ascreen formname = "mainForm">
						<cf_ascreenitem screenid="CountryLocatorScreen" locatordef="#locatordef#" method="edit">
					</cf_ascreen>
				<cfelse>
					<cf_relayFormElementDisplay type="html">
						<cfoutput>
						There is currently no Locator Definition for this country<cfif LocatorDef.RecordCount gt 0 and len(application.countryiso[frmCountryID]) gt 0>, it is currently using the Locator Definition for country group <a href="javascript:goToCountry(#LocatorDef.CountryGroupID#)">#application.countryname[LocatorDef.CountryGroupID]#</a><cfelseif len(application.countryiso[frmCountryID]) eq 0> group</cfif>.  <br/><br/><a href="javascript:addLocatorDef()">Click here</a> if you wish to create one.</td>
						</cfoutput>
					</cf_relayFormElementDisplay>
				</cfif>
			</div>	
		</cfif>
		
		<cf_input type="hidden" name="country_LastUpdated_#country.countryid#" value = "#country.lastupdated#">
	</cf_relayformdisplay>
</form>

<cfoutput>
<form action="/data/entityFlagList.cfm" method="post" name="flagListForm" target="mainSub">
	<input type="hidden" name="frmcurrentEntityID" value="0">
	<input type="hidden" name="frmEntityType" value="0">
	<input type="hidden" name="frmflagGroupID" value="">
	<input type="hidden" name="frmBack" value="0">
	<input type="hidden" name="frmNext" value="0">
</form>
</cfoutput>


