<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			ValidValues.cfm
Author:			Simon WJ
Date created:	24 November 1999

Description:
This provides a screen for managing the ValidFieldValues table.

This screen supports various modes:
 - if passed frmFieldName, frmCountryID it will bring up the current field values for a given country and
  field.
 - if frmShowHeading IS Yes then it will show the heading section which allows the user to change which
 fields and country you are working on


Known problems/improvements:
 - for some reason frmCountryID=0 or a country group does not work
 - at the moment you can add duplicate values in the ValidFieldValues for a given fieldname and default value


Version history:
1  Initial version supported certain fields.
2  SWJ 7-Feb-2000 version supports more fields and additional functions.

 WAB 250200 Corrected problems with country 0 - implemented through application.countryname
 NJH 23-Jun-2006 Added functionality to create a valid value list. Should be used primarily for a new LeadType capture.

2012/03/05	NJH/IH	CASE 426937 - do not translate the valid value when editing
2015/12/18  SB      Bootstrap
--->
<cf_translate>

<CFPARAM NAME="frmShowHeading" DEFAULT="Yes">
<cfinclude template="/templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT '' AS countryID, 'Choose the country' AS CountryDescription, 1 as myOrder
	UNION
	SELECT 0 AS countryID, 'All Countries' AS CountryDescription, 2 as myOrder
	UNION
	SELECT CountryID, CountryDescription, 3 as myOrder
	FROM Country
	   WHERE CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY myOrder,CountryDescription
</CFQUERY>

<CFSET current="validValues.cfm">

<CFIF frmShowHeading IS "Yes">
<CFSET frmShowHeading = #frmShowHeading#>
<cfset request.relayFormDisplayStyle = "HTML-div">
<cfinclude template="/templates/relayFormJavaScripts.cfm">

	<CFIF (NOT isDefined("frmFieldName") and NOT isDefined("frmCountryID")) and NOT isDefined("NewValidValueSubmit")>
		<!--- Only show these instructions the first time you come into the screen --->
		<cf_relayFormDisplay>
		<TR>
			<TD COLSPAN="2" STYLE="background-color: #FFFFCE;">
			Using this tool you can define default field values for certain fields in the database.
			Default values for a field must be defined one country at a time.  By selecting a country
			 and a field from the pull down menus below and clicking Get Current Values, you will be able
			presented with a list of the current values for that field.
			</TD>
		</TR>
		<TR>
		<TD COLSPAN="2">&nbsp;</TD>
		</TR>
		<TR><TD>
			<cfform name="NewValidValueForm" action="/dataTools/ValidValues.cfm" method="POST" >
				<cf_relayFormElement relayFormElementType="submit" fieldname="NewValidValueSubmit" currentValue="phr_CreateNewValidValue" label="" valueAlign="left">
			</cfform>
		</TD></TR>
		</cf_relayFormDisplay>
	</CFIF>


	<!--- <cfoutput><FORM NAME="ThisForm" ACTION="/dataTools/ValidValues.cfm" METHOD="POST"></cfoutput> --->
    <CFFORM NAME="ThisForm" ACTION="/dataTools/ValidValues.cfm" METHOD="POST" >
	<!--- <TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder"> --->
		<cf_relayFormDisplay>
			<cfif NOT isDefined("frmCountryID")>
				<cfset frmCountryID = "">
			</cfif>

			<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmCountryID" label="Country" currentValue="#frmCountryID#" query="#getCountries#" display="CountryDescription" value="CountryID" required="yes" message="You must specify a country">
<!--- 			<TR><TD>Country:</TD>
				<TD>
					<SELECT NAME="frmCountryID">
					<OPTION VALUE="">Choose the country
					<OPTION VALUE="0">All countries
					<CFOUTPUT QUERY="getCountries">
						<OPTION VALUE="#CountryID#"> #HTMLEditFormat(CountryDescription)#</CFOUTPUT>
						<!--- <OPTION VALUE="#CountryID#"#IIf(CompareNoCase(frmCountryDescription,CountryDescription) IS 0,DE(" SELECTED"),DE(""))#>#CountryDescription# --->
					</SELECT>
					<INPUT TYPE="Hidden" NAME="frmCountryID_required" VALUE="You must specify a country">
				</TD>
			</TR> --->

			<CFQUERY NAME="GetDistinctValidFieldNames" DATASOURCE="#application.SiteDataSource#">
 				SELECT '' as FieldNameValue, 'Specify which field you want to define values for' as FieldNameDisplay
				UNION
				SELECT distinct  FieldName as FieldNameValue, FieldName as FieldNameDisplay
				FROM  dbo.ValidFieldValues
				<!--- 2012-07-25 PPB P-SMA001 commented out
				WHERE countryID in (0,#Variables.CountryList#)
				 --->
				WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="ValidFieldValues").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->

				ORDER by FieldNameValue
			</CFQUERY>
			<cfset FieldNameList = ValueList(GetDistinctValidFieldNames.FieldNameValue)>

			<cfset fieldNameRequiredMessage = "Phr_Sys_YouMustProvideAFieldToAnalyse">
			<cfif isDefined("NewValidValueSubmit")>
				<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmFieldName" currentValue="" label="Name" required="yes" message="#fieldNameRequiredMessage#">
			<cfelse>
				<cfif isDefined("frmFieldName") and NOT listfindnocase("#FieldNameList#",Replace(Replace(#frmFieldName#," ","","All"),"'","","All"))>  <!--- if the text entered doesn't already exist as a valid value --->
					<cfset frmFieldName = Replace(Replace(#frmFieldName#," ","","All"),"'","","All")> <!--- Remove any spaces or single quotes (required for the elements in LeadCapture form to be displayed properly) --->
					<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmFieldName" currentValue="#frmFieldName#" label="Name" required="yes" message="#fieldNameRequiredMessage#">
				<cfelse>
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmFieldName" currentValue="#IIF(isDefined('frmFieldName'),Evaluate(DE('frmFieldName')),DE(''))#" label="Which Field" query="#GetDistinctValidFieldNames#" display="FieldNameDisplay" value="FieldNameValue" required="yes" message="#fieldNameRequiredMessage#">
				</cfif>
			</cfif>

<!---  			<TR><TD>
					Which Field:
				</TD>
				<TD>
					<SELECT NAME="frmFieldName">
						<OPTION VALUE="">Specify which field you want to define values for
						<CFOUTPUT QUERY="GetDistinctValidFieldNames"><OPTION VALUE="#FieldName#">#FieldName#</CFOUTPUT>
					</SELECT>
				<INPUT TYPE="Hidden" NAME="frmFieldName_required" VALUE="You must provide a field to analyse">
				</TD>
			</TR> --->
<!--- 			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="Submit" VALUE="  Get Current Values  ">
					<INPUT TYPE="Reset">
				</TD>
			</TR> --->
			<cfset submitLabel = IIF(isDefined("NewValidValueSubmit"),DE("phr_CreateNewValidValue"),DE("  phr_GetCurrentValues  "))>
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" label="" currentValue="#submitLabel#" valueAlign="left">
		</cf_relayFormDisplay>
	</CFFORM>


	</CFIF>

<CFPARAM NAME="MaxSortOrder" DEFAULT="1">

<!--- If the query has called the screen then run this next bit --->
<CFIF isDefined("frmFieldName") and isDefined("frmCountryID")>
	<H2></H2>
	<CFSET TableName =#ListFirst(frmFieldName,".")#>

	<CFQUERY NAME="GetValidValues" DATASOURCE="#application.SiteDataSource#">
	SELECT CountryID, ValidFieldID, FieldName, ValidValue, datavalue, InAnalysis, Score,
			DefaultFieldValue, sortorder, DefaultFieldCase, LanguageID
	FROM dbo.ValidFieldValues
		WHERE (CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (FieldName =  <cf_queryparam value="#frmFieldName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		Order by sortorder
	</CFQUERY>
	<CFIF #GetValidValues.RecordCount# GT 0>
		<CFSET MaxSortOrder = ListLast(ValueList(GetValidValues.SortOrder)) >
		<CFIF MaxSortOrder is not "">
			<CFSET MaxSortOrder = MaxSortOrder + 1>
		<CFELSE>
			<CFSET MaxSortOrder = 1>
		</cfif>

	</CFIF>

	<CFIF frmCountryID is not 0>
		<CFSET thisCountryName = application.countryName[frmCountryID]>
	<CFELSE>
		<CFSET thisCountryName = "All Countries">
	</cfif>


	<table class="table" id="validValuesTable">
		<tr>
			<th>ValidValue - Specify the valid value.</th> <!--- 5 --->
			<th>Data value - Specify an alternative data value that will be saved instead of
			the Valid Value field.</th>
			<th>Default&nbsp;<BR>Choose one record as the default value.</th> <!--- 5 --->
			<th>Sort Order - <B>Note:</B> Records with the same Ordering Index are sorted alphabetically.
				should be ordered 1 to 7.</th> <!--- 5 --->
			<th>Default Case - Specify the default case for this record.</th>
			<th>Include in Analyisis - Check if you want this to appear in analysis.</th>
			<th>Score - Specify the score for this record.</th>
			<th></th>
		</tr>
			<tr>
				<td colspan="8">
					<CFIF #GetValidValues.RecordCount# GT 0>
						<CFOUTPUT><p>Current valid field values for #htmleditformat(frmFieldName)# in #htmleditformat(thisCountryName)#</p></CFOUTPUT>
					<CFELSE>
						<CFOUTPUT><p>There are currently no field values for #htmleditformat(frmFieldName)# in #htmleditformat(thisCountryName)#</p></CFOUTPUT>
					</CFIF>
				</td>
			</tr>

	<CFLOOP QUERY="GetValidValues">
		<CFOUTPUT>
			<FORM ACTION="/dataTools/ValidValueTask.cfm" METHOD="POST">
			<CF_INPUT TYPE="Hidden" NAME="frmFieldName" VALUE="#frmFieldName#">
			<INPUT TYPE="Hidden" NAME="frmFieldName_required" VALUE="You must choose a field and 'Find Current Values First'">
			<CF_INPUT TYPE="Hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
			<CF_INPUT TYPE="Hidden" NAME="frmCountryDescription" VALUE="#getCountries.CountryDescription#">
			<CF_INPUT TYPE="Hidden" NAME="frmValidFieldID" VALUE="#ValidFieldID#">
			<tr>
			<CFIF CurrentRow MOD 2 IS NOT 0>
				<CFSET StyleColor = '##f5f5f5'>
			<CFELSE>
				<CFSET StyleColor = 'White'>
			</CFIF>
				<td style="background-color: #StyleColor#;">
					<!--- CASE 426937 NJH/IH 2012/03/05 Do not translate the valid value --->
					<!-- StartDoNotTranslate -->
					<CF_INPUT TYPE="Text" NAME="frmValidValue" VALUE="#ValidValue#" SIZE="25" MAXLENGTH="50">
				</td>
					<!-- EndDoNotTranslate -->

								<!--- DataValue --->
				<td style="background-color: #StyleColor#;">
				<cfif ValidValue eq "*lookup">
					<textarea cols="30" rows="10" name="frmDataValue" class="form-control">#DataValue#</textarea>
				<cfelse>
					<CF_INPUT TYPE="Text" NAME="frmDataValue" VALUE="#DataValue#" SIZE="25" MAXLENGTH="500">
				</cfif>
				</td>
				<td style="background-color: #StyleColor#;">
					<CF_INPUT TYPE="Checkbox" label="default" NAME="frmDefault" VALUE="1"  Checked="#iif(DefaultFieldValue IS 1,true,false)#"> Default
				</td>
				<td style="background-color: #StyleColor#;">
					<SELECT NAME="frmOrder" class="form-control">
					<CFLOOP INDEX="LoopCount" FROM="1" TO="20">
						<CFIF LoopCount IS SortOrder>
							<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#
						<CFELSE>
							<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
						</CFIF>
					</CFLOOP></SELECT>
				</td>

				<!--- Default case settings --->
				<td style="background-color: #StyleColor#;">
					<div class="radio">
						<CF_INPUT TYPE="Radio" NAME="frmCase" VALUE="T"  Checked="#iif(DefaultFieldCase IS 'T',true,false)#"> Title Case
					</div>
					<div class="radio">
						<CF_INPUT TYPE="Radio" NAME="frmCase" VALUE="U"  Checked="#iif(DefaultFieldCase IS 'U',true,false)#"> ALL UPPER
					</div>
					<div class="radio">
						<CF_INPUT TYPE="Radio" NAME="frmCase" VALUE="L"  Checked="#iif(DefaultFieldCase IS 'L',true,false)#"> all lowercase
					</div>
				</td>
				<!--- In Analysis --->
				<td style="background-color: #StyleColor#;">
					<CF_INPUT TYPE="Radio" NAME="frmInAnalysis" VALUE="1"  Checked="#iif(InAnalysis IS 1,true,false)#"> Yes<BR>
					<CF_INPUT TYPE="Radio" NAME="frmInAnalysis" VALUE="0"  Checked="#iif(InAnalysis IS 0,true,false)#"> No
				</td>
				<!--- Score --->
				<td style="background-color: #StyleColor#;">
					<CF_INPUT TYPE="Text" NAME="frmScore" VALUE="#Score#" SIZE="5" MAXLENGTH="10">
				</td>
				<!--- Update button --->
				<td align="CENTER" STYLE="background-color: #StyleColor#;">
					<INPUT TYPE="Submit" NAME="frmButton" VALUE="Update" class="btn btn-primary">
				</td>
			</tr>
			</FORM>
		</CFOUTPUT>
	</CFLOOP>


	<CFOUTPUT>
	<FORM ACTION="/dataTools/ValidValueTask.cfm" METHOD="POST">
		<CF_INPUT TYPE="Hidden" NAME="frmFieldName" VALUE="#frmFieldName#">
		<CF_INPUT TYPE="Hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
		<CF_INPUT TYPE="hidden" NAME="frmShowHeading" VALUE="#frmShowHeading#">
		<CF_INPUT TYPE="Hidden" NAME="frmCountryDescription" VALUE="#getCountries.CountryDescription#">
	</CFOUTPUT>
		</tr>

<!--- This is the add record line                                                   --->
		<tr>
			<td colspan="8">
				<CFIF #GetValidValues.RecordCount# GT 0>
					<CFOUTPUT><p>Fill out the fields below to add additional values for #htmleditformat(frmFieldName)# in #htmleditformat(thisCountryName)#</p></CFOUTPUT>
				<CFELSE>
					<CFOUTPUT><p>Complete the fields below to add field values for #htmleditformat(frmFieldName)# in #htmleditformat(thisCountryName)#.
					</p></CFOUTPUT>
				</CFIF>
			</td>
		</tr>

		<tr>
			<td style="background-color: #f5f5f5;">
				<INPUT class="form-control" TYPE="Text" NAME="frmValidValue"SIZE="25" MAXLENGTH="50">
			</td>
						<!--- DataValue --->
			<td style="background-color: #f5f5f5;">
				<INPUT class="form-control" TYPE="Text" NAME="frmDataValue"  SIZE="25" MAXLENGTH="500">
			</td>
			<td style="background-color: #f5f5f5;">
				<div class="checkbox">
					<label for="frmDefault">
						<INPUT TYPE="Checkbox" id="frmDefault" NAME="frmDefault" VALUE="1">Default
					</label>
				</div>
			</td>
			<td style="background-color: #f5f5f5;">
				<SELECT NAME="frmOrder" class="form-control">
			<CFOUTPUT>
				<CFLOOP INDEX="LoopCount" FROM="1" TO="20">
					<CFIF LoopCount IS MaxSortOrder>
						<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#
					<CFELSE>
						<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
					</CFIF>
				</CFLOOP>
			</CFOUTPUT>
				</SELECT>

			</td>

			<!--- Default case settings --->
			<td style="background-color: #f5f5f5;">
				<div class="checkbox">
					<label>
						<INPUT TYPE="Radio" NAME="frmCase" VALUE="T" CHECKED> Title Case
					</label>
				</div>
				<div class="checkbox">
					<label>
						<INPUT TYPE="Radio" NAME="frmCase" VALUE="U"> ALL UPPER
					</label>
				</div>
				<div class="checkbox">
					<label>
						<INPUT TYPE="Radio" NAME="frmCase" VALUE="L"> all lowercase
					</label>
				</div>
			</td>
			<!--- In Analysis --->
			<td style="background-color: #f5f5f5;">

					<label class="checkbox-inline">
						<INPUT TYPE="Radio" NAME="frmInAnalysis" VALUE="1"> Yes
					</label>
					<label class="checkbox-inline">
						<INPUT TYPE="Radio" NAME="frmInAnalysis" VALUE="0"> No
					</label>

			</td>
			<!--- Score --->
			<td style="background-color: #f5f5f5;">
				<INPUT class="form-control" TYPE="Text" NAME="frmScore" SIZE="5" MAXLENGTH="10">
			</td>
			<!--- AddButton --->
			<td style="background-color: #f5f5f5;">
				<INPUT TYPE="Submit" NAME="frmButton" VALUE="Add Record" class="btn btn-primary">
			</td>

		</tr>

		</FORM>

	</table>
<CFELSE>

</CFIF>





</cf_translate>








