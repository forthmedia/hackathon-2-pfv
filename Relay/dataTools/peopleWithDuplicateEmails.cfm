<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NYB/WAB 2009-09-14 LHID2614 - fixed email link to open in new tab
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Usage Report</cf_title>
	</cf_head>
	
	
<!--- START:  NYB/WAB 2009-09-14 LHID2614 - added: --->	
<cf_includeJavascriptOnce template = "/javascript/extextension.js">
<!--- END:  NYB/WAB 2009-09-14 LHID2614 --->

<CFSET REPORTBEGINDATE=NOW()-30>
<CFSET REPORTENDDATE=NOW()>

<cfparam name="sortOrder" default="email">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="dateFormat" type="string" default="Last_Login_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->

<cfquery name="multipleEmails" datasource="#application.SiteDataSource#">
	select email,count(*) as number_of_instances from person
where 1=1
<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
group by email
having count(*) > 1

order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Multiple Emails</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">
		<CFOUTPUT>
		List of people records where there are multiple instances of an email address
		</CFOUTPUT>
	</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#multipleEmails#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="350"
	startRow="#startRow#"
	
	keyColumnList="email"
	<!--- START:  NYB/WAB 2009-09-14 LHID2614 - replaced: ---	
	keyColumnURLList="../data/dataFrame.cfm?frmEmail="
	!--- WITH:  NYB/WAB 2009-09-14 LHID2614 --->
	keyColumnURLList="javascript:void(openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmEmail=thisURLKey'*comma{reuseTab:true}));"
	<!--- END:  NYB/WAB 2009-09-14 LHID2614 --->
	keyColumnKeyList="email"
	
	allowColumnSorting="yes"
	numberFormat="number_of_instances"
>
</cf_translate>




