<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		reportAutomaticDedupe.cfm
Author:			GCC
Date started:		2005-10-03

Description:		This provides a report of incentive account creation dates and dates of first product claim.

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

03-Oct-2005			GCC			V1
02-Dec-2015			SB			Bootstrap
Enhancements still to do:
 --->
<cfquery name="thisWeek" datasource="#application.sitedatasource#">
select datepart(week,getdate()) as weeknumber
</cfquery>
<!--- GCC tweaked to enable the reports to continue to roll into 2006 and beyond --->
<cfquery name="thisYear" datasource="#application.sitedatasource#">
select datepart(year,getdate()) as yearnumber
</cfquery>

<cfset startYear = 2007>
<cfset startWeek = 9>
<cfquery name="OrgDedupesByCountry" datasource="#application.sitedatasource#">
select CalendarYear, WeekOfCalendarYear,
	weekcommencing as reportcolumns,
	country as reportrows,
	sum(people) as reportunits
				from calendarweek c
				left join
	(select
			yearorder, weekorder, weekcommencing,
			country,people,entitytype
		from vR_Dedupes_entity_week_country
		where entitytype = 'Organisation'
		AND countryID in (#request.relaycurrentuser.countrylist#)
		<cfif structkeyexists(application,"LiveCountryIDs")>
			AND countryID  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
) a
on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
	where ((calendaryear =  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear >=  <cf_queryparam value="#startweek#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear >  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" > ))
	AND (( calendaryear =  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear <  <cf_queryparam value="#thisweek.weeknumber+1#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear <  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" > ))
	group by CalendarYear, WeekOfCalendarYear, weekcommencing, country
					order by CalendarYear, WeekOfCalendarYear asc
</cfquery>

<cfquery name="LocDedupesByCountry" datasource="#application.sitedatasource#">
select CalendarYear, WeekOfCalendarYear,
	weekcommencing as reportcolumns,
	country as reportrows,
	sum(people) as reportunits
				from calendarweek c
				left join
	(select
			yearorder, weekorder, weekcommencing,
			country,people,entitytype
		from vR_Dedupes_entity_week_country
		where entitytype = 'location'
		AND countryID in (#request.relaycurrentuser.countrylist#)
		<cfif structkeyexists(application,"LiveCountryIDs")>
			AND countryID  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
) a
on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
	where ((calendaryear =  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear >=  <cf_queryparam value="#startweek#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear >  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" > ))
	AND (( calendaryear =  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear <  <cf_queryparam value="#thisweek.weeknumber+1#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear <  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" > ))
	group by CalendarYear, WeekOfCalendarYear, weekcommencing, country
					order by CalendarYear, WeekOfCalendarYear asc
</cfquery>
<cfquery name="PerDedupesByCountry" datasource="#application.sitedatasource#">
select CalendarYear, WeekOfCalendarYear,
	weekcommencing as reportcolumns,
	country as reportrows,
	sum(people) as reportunits
				from calendarweek c
				left join
	(select
			yearorder, weekorder, weekcommencing,
			country,people,entitytype
		from vR_Dedupes_entity_week_country
		where entitytype = 'person'
		AND countryID in (#request.relaycurrentuser.countrylist#)
		<cfif structkeyexists(application,"LiveCountryIDs")>
			AND countryID  in ( <cf_queryparam value="#request.currentSite.liveCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
) a
on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
	where ((calendaryear =  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear >=  <cf_queryparam value="#startweek#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear >  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" > ))
	AND (( calendaryear =  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear <  <cf_queryparam value="#thisweek.weeknumber+1#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear <  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" > ))
	group by CalendarYear, WeekOfCalendarYear, weekcommencing, country
					order by CalendarYear, WeekOfCalendarYear asc
</cfquery>

<cfset nReport = CreateObject("component","report.reporting")>
<cfset nReport.userCalendar = "week">

<table class="table table-hover table-striped">
	<tr>
		<td colspan="4">Duplicates automatically removed by week by country</td>
	</tr>
	<tr>
		<th>Totals</th>
		<th>Organisations</th>
		<th>Locations</th>
		<th>People</th>
	</tr>
	<tr>
		<td>
			<cfset nReport.showCumulative = "yes">
			<cfset nFilter = "((yearorder =" & startYear & " and weekorder >=" & startweek & " ) OR (yearorder >" & startYear & " )) AND (( yearorder =" & thisYear.yearnumber & " and weekorder <" & thisweek.weeknumber+1 & " ) OR (yearorder <" & thisYear.yearnumber & " ))">
			<cfset x=nReport.commonColumnReport(commoncolumnreport = 'AutomaticDeduplication',whereclause=nFilter,showContextMenu = false)>
		</td>
		<td>
			<cfset x=nReport.crossTabReport(OrgDedupesByCountry,'no','no','bottom','order by CalendarYear, WeekOfCalendarYear')>
		</td>
		<td>
			<cfset x=nReport.crossTabReport(LocDedupesByCountry,'no','no','bottom','order by CalendarYear, WeekOfCalendarYear')>
		</td>
		<td>
			<cfset x=nReport.crossTabReport(PerDedupesByCountry,'no','no','bottom','order by CalendarYear, WeekOfCalendarYear')>
		</td>
	</tr>
</table>