<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			archiveScheduleSettings.cfm	
Author:				NYB
Date started:		2008/12/02
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->

<cf_translate>

<cfparam name="sortOrder" default="ArchiveDefID">




<cf_head>
	<cf_title>Archive Schedule Settings</cf_title>
</cf_head>



<cfscript>
 	// this changes the active status of a certification
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayElearning.inactivateSpecialisation(specialisationID=listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="236" name="thisEditor" entity="ArchiveDef" title="Archive Schedule Settings">
			<!--- <field name="ArchiveDefID" label="phr_Archive_ArchiveDefID" description="This records unique ID." control="hidden"></field> --->
			<field name="Name" label="phr_Archive_Name" description="The archive name." control="readonly"></field>
			<field name="Description" label="phr_Archive_Description" description="The archive description." control="readonly"></field>
			<field name="AgeValue" label="phr_Archive_AgeValue" description="" control="text"></field>
			<field name="AgePart" label="phr_Archive_AgePart" description="The country the certification is valid in." 
					control="select" query="select 'd' as value,'day' as display, 1 as orderIndex
							union select 'ww' as value,'week' as display, 2 as orderIndex
							union select 'm' as value,'month' as display, 3 as orderIndex
							union select 'yyyy' as value,'year' as display, 4 as orderIndex
							order by orderIndex"></field>
			<group label="System Fields" name="systemFields">				
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedBy"></field>
			</group>
		</editor>	
	</editors>
	</cfoutput>
</cfsavecontent>


<!---

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "#functionType# Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Inactivate Archive Process";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:if (confirm('Are you sure you want to inactivate this archive process?\n .')) {document.frmBogusForm.submit();} ;//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Activate Archive Process";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:{document.frmBogusForm.submit();} ";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_Tagging";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_TagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(true));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_UntagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(false));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();
</cfscript>

	keyColumnList="Code"
	keyColumnKeyList="specialisationID"
	topHead="/elearning/eLearningTopHead.cfm"
    functionListQuery="#comTableFunction.qFunctionList#"


<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="ArchiveDef"
	showSaveAndReturn = "true"
	showTheseColumns="Name,Description,AgeValue,AgePart"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	rowIdentityColumnName="ArchiveDefID"
	topHead="/elearning/eLearningTopHead.cfm"
>

   	[entityRecordID = "{entityRecordID}"]
   	[add="{Yes | No}"]
   	[showDebug="{true | false}"]
   	backButtonURLVars = "FRMCURRENTENTITYID,FRMENTITYTYPE,FRMNEXT,SELINDEX,THISSCREENTEXTID"

<CF_RelayXMLEditor
   	editorName = "thisEditor"
  	xmlSourceVar = "#xmlSource#"
   	thisEmailAddress = "relayhelp@foundation-network.com"
 	showSaveAndReturn = "true"		
	showSaveAndAddNew = "true"		
	showDebug="false"
	showRecordRights = "true"
>

	xmlSource="#xmlSource#" 
	cfmlCallerName="specialisations"
	keyColumnList="Code"
	keyColumnKeyList="specialisationID"
	showSaveAndReturn = "true"
	showTheseColumns="Code,Description,Number_Of_Rules,Number_Of_Rules_To_Pass,Active,Country"
	includeFile = "/eLearning/specialisationsInclude.cfm"
	useInclude="false"
	sortOrder="#sortOrder#"
	topHead="/elearning/eLearningTopHead.cfm"
	queryData="#getData#"
	rowIdentityColumnName="specialisationID"
    functionListQuery="#comTableFunction.qFunctionList#"


--->
<cfif isDefined("editor") and (editor eq "yes" or isDefined("add") or add eq "yes")>
	<cfquery name="getData" datasource="#application.siteDataSource#">
		select * from ArchiveDef 
		where active=1
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
<cfelse>
	<cfquery name="getData" datasource="#application.siteDataSource#">
		select * from (
			select ArchiveDefID,[Name],isNULL(Description,'&nbsp;') as Description,AgeValue,ap.AgePart 
			from ArchiveDef ad 
			inner join 
			(
				select 'd' as [value],'day' as AgePart
				union select 'ww' as [value],'week' as AgePart
				union select 'm' as [value],'month' as AgePart
				union select 'yyyy' as [value],'year' as AgePart
			) as ap
			on ad.AgePart=ap.[value]
			where ad.active=1
		) as d
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

	
<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="archiveScheduleSettings"
	keyColumnList="Name"
	keyColumnKeyList="ArchiveDefID"
	showSaveAndReturn = "true"
	showTheseColumns="Name,Description,AgeValue,AgePart"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	topHead="/dataTools/dataToolsBlankTopHead.cfm"
	columnTranslation="true"
	columnTranslationPrefix="phr_Archive_"
>




</cf_translate>



