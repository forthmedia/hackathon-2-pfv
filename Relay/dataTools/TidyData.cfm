<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Tidy Data</cf_title>
</cf_head>



<cfif isDefined("URL.method") and isDefined("URL.tablename")>
<cfscript>
	// reload dbTools first just so that it is the latest version
	application.com.dbTools = createObject("component", "relay.com.dbtools");
	if (URL.tablename eq "person" and URL.method eq "trimData") {
		message = application.com.dbTools.trimData("person","salutation,firstname,lastname,email,jobDesc");
	}
	if (URL.tablename eq "location" and URL.method eq "trimData") {
		message = application.com.dbTools.trimData("location","sitename,address1,address2,address3,address4,address5,postalCode");
	}
	if (URL.tablename eq "organisation" and URL.method eq "trimData") {
		message = application.com.dbTools.trimData("organisation","organisationName,matchname,orgURL,AKA");
	}
</cfscript>

</cfif>  

<cfoutput>

<h2>Tidy Data</h2>
<p><cfif Isdefined("message")>#application.com.security.sanitiseHTML(message)#</cfif>&nbsp;</p>

<p><a href="tidyData.cfm?method=trimData&tablename=person">Remove leading and trailing spaces from Person salutation,firstname,lastname,email</a></p>

<p><a href="tidyData.cfm?method=trimData&tablename=location">Remove leading and trailing spaces from Location sitename and address fields</a></p>

<p><a href="tidyData.cfm?method=trimData&tablename=organisation">Remove leading and trailing spaces from Organisation name, URL and AKA fields</a></p>
</cfoutput>


