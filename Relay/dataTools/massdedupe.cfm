<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	MassDedupe.cfm
Author:		WAB then SWJ
Date created:	Initially in May 99 however updated in Sept 2000

Description:	This file gets all of the matching organisations from a view called
				vMatchingOrgsByCountry.  It then groups them by the first organisation and
				lists them for the user to check.  Having done that it allows the
				user to merge

				vMatchingOrgsByCountry is defined be matching the first 20 characters
				of orgnames in the same country where the locations of those orgs
				are in a defined view called vPartnerEntityIDs.

				vPartnerEntityIDs gets a list of LocationID's from thr booleanFlagData table
				by searching for flag ID's that are in a view called vPartnerFlagIDs.

				vPartnerFlags creates a list of flagIDs for a given set of PartnerTypes.

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ	Fixed the delete query to work with the new x-table structure

Enhancements still to do:

need to warn the user their will be a delay while we update the matchnames
--->
<cfoutput>
<SCRIPT type="text/javascript">
	function dedupePersons(){
		var form = document.personDedupeForm;
		var checked = 0;
		for(i=0; i<form.frmDupPeople.length; i++){
			if(form.frmDupPeople[i].checked==true){
				checked=1;
			}
		}
		if(!checked){
			alert("You must select at least two people who are not users to start the dedupe process.");
		}
		else{
			form.submit();
		}
	}

	function dedupeOrganisations(){
		var form = document.personDedupeForm;
		var checked = 0;
		for(i=0; i<form.frmorgids.length; i++){
			if(form.frmorgids[i].checked==true){
				checked=1;
			}
		}
		if(!checked){
			alert("You must select at least two organisations to start the dedupe process.");
		}
		else{
			form.action='/data/orgDedupe.cfm';
			form.submit();
		}
	}
</script>
</cfoutput>

<cfinclude template="/templates/qrygetcountries.cfm">
<cfparam name="message" default="">
<cfparam name="frmViewForMatchingRecords" default="">
<!--- qrygetcountries creates a variable CountryList --->

<cfif not compareNocase(left(frmViewForMatchingRecords, 12), "vMatchingOrg")>
	<cfset orgDedupe = 1>
<cfelse>
	<cfset orgDedupe = 0>
</cfif>

<cfif not compareNocase(left(frmViewForMatchingRecords, 12), "vMatchingPer")>
	<cfset perDedupe = 1>
<cfelse>
	<cfset perDedupe = 0>
</cfif>

<cfif not compareNocase(left(frmViewForMatchingRecords, 6), "person")>
	<cfset perDedupe = 1>
<cfelse>
	<cfset perDedupe = 0>
</cfif>

<CFIF findCountries.RecordCount EQ 1>
	<CFSET frmCountryID = variables.CountryList>
</CFIF>
<!--- <cfinclude template="/templates/qrygetSelections.cfm"> --->

<!--- This will return a list of the views for the organisations
	which could be used to match
	records in the database --->
<CFQUERY NAME="GetViews" DATASOURCE="#application.SiteDataSource#">
	select table_name
	from information_schema.views
	where table_name like 'vMatchingOrgsByCountry%' or table_name like 'vMatchingPerson%'
</CFQUERY>

<cfif orgDedupe>
	<CFQUERY NAME="checkForNullMatchnames" DATASOURCE="#application.SiteDataSource#">
		select top 1 organisationid from organisation where matchname is null
	</CFQUERY>

	<cfif checkForNullMatchnames.recordCount gt 0>
		<cfscript>
			//NJH 2016/09/01 JIRA PROD2016-1190 message = application.com.dbtools.updateOrgMatchname();
			application.com.matching.updateMatchFields(tablename="organisation",entityType="organisation");
		</cfscript>
	</cfif>

	<CFIF isDefined("frmViewForMatchingRecords") and frmViewForMatchingRecords neq "">
		<CFQUERY NAME="getMyCountries" DATASOURCE="#application.SiteDataSource#">
			SELECT CountryID, CountryDescription,
				count(CountryID) as numRecs
			FROM #frmViewForMatchingRecords#
			WHERE CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			group by CountryID, CountryDescription
			   ORDER BY CountryDescription
		</CFQUERY>
	</CFIF>

	<CFIF isDefined("frmCountryID") >
		<cfif orgDedupe>
			<CFQUERY NAME="GetMatchingOrgs" DATASOURCE="#application.siteDataSource#" maxrows="100">
			 SELECT * from #frmViewForMatchingRecords#
			 <cfif frmCountryID neq 0 and frmCountryID neq "">
				where countryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			 </cfif>
			 ORDER BY organisationName
			</CFQUERY>
		</cfif>
	</CFIF>

<cfelse>

	<CFQUERY NAME="getMyCountries" DATASOURCE="#application.SiteDataSource#">
		SELECT c.CountryID, CountryDescription,
				count(c.CountryID) as numRecs
			FROM vMatchingPersonByEmailLastNameFirstName v
			inner join country c ON c.countryid = v.countryid
			WHERE c.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			group by CountryDescription, c.CountryID
			   ORDER BY CountryDescription
	</CFQUERY>

</cfif>

<cfif perDedupe and isDefined("frmCountryID")>
	<cfif not compareNoCase(FRMVIEWFORMATCHINGRECORDS, "personByEmail")>
		<CFQUERY NAME="countDupes" DATASOURCE="#application.siteDataSource#" >
			select distinct p.email, o.organisationName,
			(select count(email) from person where email = p.email) as countduplicates
			from person p, organisation o
			where o.organisationID = p.organisationID
			and o.countryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and ltrim(rtrim(email)) <> ''
			and ltrim(rtrim(email)) IS NOT NULL
		</cfquery>


	<cfelseif not compareNoCase(FRMVIEWFORMATCHINGRECORDS,"personByNameSurname")>
		<CFQUERY NAME="countDupes" DATASOURCE="#application.siteDataSource#" >
			select distinct p.firstName, p.lastName, o.organisationName,
			(select count(personID) from person where firstName = p.firstName and lastName = p.lastName) as countduplicates
			from person p, organisation o
			where o.organisationID = p.organisationID
			and o.countryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and firstName <> ''
			AND firstName IS NOT NULL
			AND lastName <> ''
			AND lastName IS NOT NULL
		</cfquery>
	</cfif>

	<CFQUERY NAME="GetOnlyDupes" dbtype="query">
		select * from countDupes
		WHERE countduplicates > 1
	</cfquery>

</cfif>




<cf_head>
	<cf_title>Dedupe Organisations</cf_title>
</cf_head>


<CFSET current="massDedupe.cfm">

<CFIF NOT isDefined("frmCountryID")>
<!--- FRMDUPPEOPLECHECK --->
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<cfoutput><FORM NAME="thisForm" ACTION="/dataTools/massdedupe.cfm" METHOD="POST"></cfoutput>

		<!--- Only show these instructions the first time you come into the screen --->
		<TR>
			<TD COLSPAN="2" CLASS="CreamBackground">
			Using this tool you can merge matching organisations.  First you must choose the view you
			want to use.  Each view will find matches in different ways.  This should be self explanatory
			from the name of the view.  Different views will find different matches.
			<br><strong>Note: This process can run for 20-30 seconds as it checks the whole database.</strong>
			</TD>
		</TR>
		<TR>
		<TD COLSPAN="2"><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></TD>
		</TR>

		<TR><TD>Choose the method you want to use to find possible dupes:</TD>
			<TD><SELECT NAME="frmViewForMatchingRecords">
				<CFOUTPUT QUERY="GetViews">
					<OPTION VALUE="#table_name#" <cfif frmViewForMatchingRecords eq table_name>selected</cfif>>#htmleditformat(table_name)#
				</CFOUTPUT>
					<OPTION VALUE="personByEmail" <cfif frmViewForMatchingRecords eq "personByEmail">selected</cfif>>Dedupe person records by email
					<OPTION VALUE="personByNameSurname" <cfif frmViewForMatchingRecords eq "personByNameSurname">selected</cfif>>Dedupe person records by name surname
				</SELECT>
				<INPUT TYPE="Hidden" NAME="frmViewForMatchingRecords_required" VALUE="You must specify a country">
			</TD>
		</TR>
		<CFIF isDefined("frmViewForMatchingRecords")and frmViewForMatchingRecords neq "">
			<cfset buttonText = "Get Organisations for the selected country">
			<TR><TD>Choose the country you want to work on:</TD>
				<TD><SELECT NAME="frmCountryID">
					<CFIF getMyCountries.RecordCount GT 1>
						<OPTION VALUE="">Select a country to work on
					</CFIF>
						<!--- <OPTION VALUE="0">All countries --->
					<CFOUTPUT QUERY="getMyCountries">
						<OPTION VALUE="#CountryID#">#htmleditformat(CountryDescription)# #htmleditformat(NumRecs)# possible duplicate organisation records to check
					</CFOUTPUT>
					</SELECT>
					<INPUT TYPE="Hidden" NAME="frmCountryID_required" VALUE="You must specify a country">
				</TD>
			</TR>
		<cfelse>
			<cfset buttonText = "Run the selected view.">
		</CFIF>
	<!---
		We could introduce this at a later date
		<TR><TD>Choose a selection (optional):</TD>
			<TD><SELECT NAME="frmSelectionID">
				<OPTION VALUE="0">Narrow your choice to a selection within a country
				<CFOUTPUT QUERY="getSelections">
					<OPTION VALUE="#SelectionID#"> #HTMLEditFormat(title)#</CFOUTPUT>
				</SELECT>
			</TD>
		</TR>
	 --->
	 	<TR>
			<TD>&nbsp;</TD>
			<TD><cfoutput><CF_INPUT type="button" value="#buttonText#" onClick="this.value = 'Please wait.  This can take 20-30 seconds...'; this.disabled = true; document.thisForm.submit()"></cfoutput></TD>
		</TR>

	</FORM>
	</TABLE>
</CFIF>

<CFIF isDefined("frmCountryID") and orgDedupe>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<CFOUTPUT>
		<FORM ACTION="/dataTools/massdedupeTask.cfm" NAME="ThisForm" METHOD="POST">
		<CF_INPUT TYPE="hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
		<CF_INPUT TYPE="hidden" NAME="frmViewForMatchingRecords" VALUE="#frmViewForMatchingRecords#">

	</CFOUTPUT>
	<CFSET I = 0>

		<TR>
			<TH>Merge?</TH>
			<TH>ID (country)</TH>
			<TH>This record</TH>
			<TH>Matches these records </TH>
			<TH>ID (Country)</TH>
		</TR>
		<CFOUTPUT query="getmatchingOrgs" group="OrgIDToKeep">
			<CFSET I=I+1>
			<TR>
				<TD VALIGN="top"><CF_INPUT TYPE="checkbox" NAME="frmOKtodo" VALUE="#I#" SIZE="50"></TD>
				<TD VALIGN="top"><a href="../data/dataFrame.cfm?frmsrchOrgID=#OrgIDToKeep#" target="_blank">#htmleditformat(OrgIDToKeep)#</a> (#htmleditformat(countryDescription)#)</TD>
				<TD VALIGN="top"><CF_INPUT TYPE="text" NAME="frmNewOrgName_#I#" VALUE="#organisationName#" SIZE="30"> <P></td>
				<CF_INPUT TYPE="HIDDEN" NAME="frmIDtoKeep_#I#" VALUE="#OrgIDToKeep#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmAllIDs_#I#" VALUE="#OrgIDToKeep#">
			</tr>

			<CFOUTPUT>
				<CF_INPUT TYPE="HIDDEN" NAME="frmAllIDs_#I#" VALUE="#OrgIDToDelete#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmIDtoDelete_#I#" VALUE="#OrgIDToDelete#">
				<tr>
					<TD>&nbsp </td>
					<TD>&nbsp </td>
					<TD>&nbsp </td>
					<TD>#htmleditformat(organisationName)#</td>
					<TD><a href="../data/dataFrame.cfm?frmsrchOrgID=#OrgIDToDelete#" target="_blank">#htmleditformat(OrgIDToDelete)#</a> (#htmleditformat(countryDescription)#)</TD>
				</TR>
			</cfoutput>
			<TR><TD COLSPAN="5"><HR WIDTH="100%" SIZE="1" COLOR="Navy"></TD></TR>
		</cfoutput>


	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmNumberonPage" VALUE="#I#">
	</cfoutput>
	<tr>
		<TD COLSPAN="5">
			<INPUT TYPE="submit" NAME="Merge this list" VALUE="Merge this list">
		</td>
	</tr>

	</FORM>

	<CFIF findCountries.RecordCount GT 0>
	<cfoutput><FORM NAME="ThisForm" ACTION="/dataTools/massdedupe.cfm" METHOD="POST"></cfoutput>
		<INPUT TYPE="submit" NAME="Choose another country" VALUE="Choose another country">
	</FORM>
	</CFIF>
	</table>
</CFIF>

<CFIF isDefined("frmCountryID") and perDedupe>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">

	<cfoutput>
		<form action="/data/persondedupe.cfm" method="post" name="personDedupeForm" id="personDedupeForm" target="_blank">
	</cfoutput>

	<TR>
		<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_PersonID</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Person</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Dedupe Phr_Sys_Person</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Email</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_OrganisationID</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Organisation</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Dedupe Phr_Sys_Organisation</TH>
	</TR>

	<cfoutput query="getOnlyDupes">
		<CFQUERY NAME="getDupeDetails" DATASOURCE="#application.siteDataSource#">
			select p.*, o.organisationname
			FROM person p
			join organisation o ON o.organisationid = p.organisationid
			<cfif not compareNoCase(FRMVIEWFORMATCHINGRECORDS, "personByEmail")>
				WHERE ltrim(rtrim(p.email)) =  <cf_queryparam value="#getOnlyDupes.email#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfelseif not compareNoCase(FRMVIEWFORMATCHINGRECORDS,"personByNameSurname")>
				WHERE ltrim(rtrim(p.firstName)) =  <cf_queryparam value="#getOnlyDupes.firstName#" CFSQLTYPE="CF_SQL_VARCHAR" >  AND ltrim(rtrim(p.lastName)) =  <cf_queryparam value="#getOnlyDupes.lastName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		</cfquery>
		<cfset loc_orgName = organisationName>
		<cfloop query="getDupeDetails">


			<tr>
				<td>#htmleditformat(personid)#</td>
				<td>#htmleditformat(FirstName)# #htmleditformat(LastName)#</td>
				<td align="center"><CF_INPUT name="frmDupPeople" type="checkbox" value="#personID#"></td>
				<td>#htmleditformat(email)#</td>
				<td>#htmleditformat(organisationID)#</td>
				<td><a href="../data/dataFrame.cfm?frmsrchOrgID=#organisationID#" target="_blank">#htmleditformat(organisationName)#</a></td>
				<td align="center"><CF_INPUT name="frmorgids" type="checkbox" value="#organisationID#"></td>
			</tr>
		</cfloop>

	</cfoutput>

	<cfoutput>
		<CF_INPUT name="frmCountryID" type="hidden" value="#frmCountryID#">
	</cfoutput>

	<input name="FRMFLAGIDS" type="hidden" value="0">
	<INPUT TYPE="HIDDEN" NAME="frmSiteName" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmFax" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmEmail" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmAddress" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmPostalCode" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmFirstName" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmLastName" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmTelephone" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmSelectionID" VALUE="0">
	<INPUT TYPE="HIDDEN" NAME="frmsrchpersonID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmsrchlocationID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNoContacts" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmAccountMngrID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmLooseMatch" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmSearchType" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmsrchOrgID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmsrchAccNmbr" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmDrillLevel" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmDupeLocs" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmDupePers" VALUE="">
	<INPUT TYPE="Hidden" NAME="frmCommid" VALUE="0">
	<INPUT TYPE="Hidden" NAME="frmStart" VALUE="">
	<INPUT TYPE="Hidden" NAME="frmStartP" VALUE="">
	<INPUT TYPE="Hidden" NAME="frmStop" VALUE="">
	<INPUT TYPE="Hidden" NAME="frmLocationID" VALUE="">
	</form>
</table>
</cfif>