<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NYB	2009-09-14	LHID2613 - fixed link to open in new tab
WAb 2009-09-15	adjustments to above
2012-10-04	WAB		Case 430963 Remove Excel Header
--->

<!--- TODO: 'The query getBrokenEmailAddresses must in include the 'NOT' after the 'WHERE' ' [godfrey.smith]- February 20, 2008, 17:53:39 PM --->

<!--- params --->
<cfparam name="sortOrder" default="email">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">
<cfparam name="personScreen" default="defaultPerson"><!--- 2008-07-14 GCC What is the default person screen with the new 3 pane view? --->
<cfsetting requesttimeout="600" >
<!--- bring in the table display --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<!--- START:  NYB 2009-09-14 LHID2613 - added: --->
<cf_includeJavascriptOnce template = "/javascript/extextension.js">
<!--- END:  NYB 2009-09-14 LHID2613 --->

<!--- give the user the choice of excel --->
<cfparam name="openAsExcel" type="boolean" default="false">



	<cf_head>
		<cf_title>Bad Email Format Report</cf_title>
	</cf_head>


<!--- 'I am the query that returns all incorrectly formatted email addresses' [godfrey.smith]- February 20, 2008, 14:54:41 PM --->
<!--- add not after ther where when releasing to live --->
<cftransaction isolation="read_uncommitted">
<cfquery name="getBrokenEmailAddresses" datasource="#application.siteDataSource#">
	SELECT personID,email,firstname,lastname
		FROM person p with (nolock) inner join organisation o with (noLock)
			ON p.organisationId = o.organisationID
	WHERE not
	(
	CHARINDEX(' ',LTRIM(RTRIM([Email]))) = 0
	AND		 email like '%@%'
	AND 	CHARINDEX(char(13),LTRIM(RTRIM([Email]))) = 0 -- No carriage returns
	AND 	CHARINDEX(char(10),LTRIM(RTRIM([Email]))) = 0 -- No line feed characters
	AND 	CHARINDEX('["]',LTRIM(RTRIM([Email]))) = 0 -- No double quote characters
	AND 	CHARINDEX(char(9),LTRIM(RTRIM([Email]))) = 0 -- No tab characters
	AND  	LEFT(LTRIM([Email]),1) <> '@'
	AND  	RIGHT(RTRIM([Email]),1) <> '.'
	AND  	CHARINDEX('.',[Email],CHARINDEX('@',[Email])) - CHARINDEX('@',[Email]) > 1
	AND  	LEN(LTRIM(RTRIM([Email]))) - LEN(REPLACE(LTRIM(RTRIM([Email])),'@','')) = 1
	AND  	CHARINDEX('.',REVERSE(LTRIM(RTRIM([Email])))) >= 3
	AND  	(CHARINDEX('.@',[Email]) = 0 AND CHARINDEX('..',[Email]) = 0)
	)
	and len([email]) > 0
	<!--- 2012-07-19 PPB P-SMA001 	and o.countryID in (#request.relayCurrentUser.countryList#) --->
	#application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause#	<!--- 2012-07-19 PPB P-SMA001 --->
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

</cftransaction>

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Invalid Email Address Report </strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">
		<p>List of persons whose email address is incorrect. Usual causes are typo errors or spaces.</p>
		<p>Click through to edit details.</p>
	</td></tr>
</table>

<!--- WAB 2009-09-15 rather than writing out the whole link 3 times, create it once here --->
<cfset link = "javascript:void(openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmsrchpersonID=thisurlkey'*comma{reuseTab:true}))">

<cf_tableFromQueryObject
	queryObject="#getBrokenEmailAddresses#"
 	numRowsPerPage="#numRowsPerPage#"
 	showTheseColumns = "email,firstname,lastname"
	keyColumnList="email,firstname,lastname"

	<!--- START: NYB 2009-09-14 - LHID2613
	keyColumnURLList="/data/entityScreens.cfm?frmEntityScreen=#personScreen#&frmEntityType=0&frmcurrentEntityID=,/data/entityScreens.cfm?frmEntityScreen=#personScreen#&frmEntityType=0&frmcurrentEntityID=,/data/entityScreens.cfm?frmEntityScreen=#personScreen#&frmEntityType=0&frmcurrentEntityID="
	keyColumnOpenInWindowList="yes,yes,yes"
	WITH: NYB 2009-09-14 - LHID2613 --->
	keyColumnURLList="#link#,#link#,#link#"

	keyColumnKeyList="personID,personID,personID"
	FilterSelectFieldList="email,firstname,lastname"
	FilterSelectFieldList2="email,firstname,lastname"
	sortOrder="#sortorder#"
	useInclude = "false"
>