<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Name: countryGroupEdit.cfm

Author: WAB

Date: 2000-06-01

Purpose:
	Displays a screen for editing Country Groups.


Work to be done:
Needs a box for adding new country group



Mods:

WAB 2004-09-29   corrected problem when no countries in a country group
WAB 2016-01-29	Remove twoSelects Combo.  Add relayformdisplay.
				Alter the editor so that it works!
				Ended up doing rather more than intended
 --->



<!--- save the content for the xml to define the editor --->

<!--- 2007-04-02 GCC Hidden fields need to have no label in order to be parsed correctly by the editor it appears --->
<cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="232" name="thisEditor" entity="country" title="Country Group">
		<field name="CountryDescription" label="Country Group" description="Country Description Name" required="true"></field>
		<field name="CountryGroupTypeID" label="Type" description="Country Group  Type" required="true" control="select" query="select countryGroupTypeID as value, description as display from countryGroupType where countryGroupTypeID &lt;&gt; 1"/>
		<field name="AllowVatReduction" label="phr_vat_allow_reduction" default="0" description="Allow VAT Reduction." control="YorN"></field>
		<field name="VatRequired" label="phr_vat_required" default="0" description="" control="YorN"></field>
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

<!---Create New Group queries--->
<CFIF IsDefined("updateCountryGroups") and isDefined("SelectedCountryIDs") and isDefined("frmCountryGroupID")>

	<!--- PPB 2010-07-29 report any duplicates if the CountryGroupType does not allow UniqueCountries;
	normally this isn't necessary because the interface doesn't offer you countries that have been assigned,
	however if a country group is set to NOT UniqueCountries and duplicates are enetered then subsequently it is set to UniqueCountries, this code is helpful to de-dupe them
	ideally we would not allow a country group to be set to UniqueCountries if there were dups but the maintainance of the table is done with relayrecordmanager so it's not possible (?) to hook bespoke code into it  --->

	<cfset doTheUpdate = true>

	<CFQUERY NAME="qryCountryGroupType" datasource="#application.siteDataSource#">
		select UniqueCountries from CountryGroupType where CountryGroupTypeId =  <cf_queryparam value="#frmCountryGroupTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<cfif qryCountryGroupType.UniqueCountries>
		<CFQUERY NAME="qryDuplicates" datasource="#application.siteDataSource#">
			SELECT  countryGroupCountry.countryDescription AS countryGroupCountryDescription, countryMemberCountry.countryDescription AS countryMemberCountryDescription
			FROM countryGroup cg
			inner join country countryGroupCountry on cg.countryGroupID = countryGroupCountry.countryID
			inner join country countryMemberCountry on cg.countryMemberID = countryMemberCountry.countryID
			where countryMemberID  in ( <cf_queryparam value="#SelectedCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and countryGroupID <>  <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 			<!--- not the group we're looking at --->
			and countryGroupID <> countrymemberid				<!--- not itself --->
			and countryGroupCountry.countryGroupTypeId =  <cf_queryparam value="#frmCountryGroupTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfif qryDuplicates.recordcount gt 0>
			<cfset doTheUpdate = false>

			<cf_translate>
			<br />
			<span style="color:red">phr_ErrorCountryGroupDuplicateCountries</span>
			<br /><br />
			<div style="margin-left:180px;" >
				<table style="border:thin solid;" >
					<tr>
						<th>phr_Country</th><th>phr_CountryGroup</th>
					</tr>
					<cfoutput query="qryDuplicates">
						<tr>
							<td>#htmleditformat(countryMemberCountryDescription)#</td><td>#htmleditformat(countryGroupCountryDescription)#</td>
						</tr>
					</cfoutput>
				</table>
			</div>
			</cf_translate>
		</cfif>
	</cfif>

	<cfif doTheUpdate>

		<cftransaction>
			<CFQUERY NAME="update" datasource="#application.siteDataSource#">
				delete from
					countryGroup
				where
					countryGroupID =  <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif SelectedCountryIDs is not "">
					and countryMemberID not in (<cf_queryparam value="#SelectedCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER" list="true" >)

					</cfif>

					INSERT INTO countryGroup(countryGroupID,countryMemberID,CreatedBy,created,lastUpdatedBy,lastUpdated)
					SELECT
						<cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
						,countryID
						,<cf_queryparam value="#ListGetAt(Cookie.User, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
						,getDate()
						,<cf_queryparam value="#ListGetAt(Cookie.User, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
						,getDate()
				FROM
					country c
						left join
					countryGroup cg on 	c.countryID = cg.countryMemberID and cg.countryGroupID = <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				where
					cg.countryMemberID is null
					and countryID  in ( <cf_queryparam value="#SelectedCountryIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
		</cftransaction>
	</cfif>

</CFIF>


<!--- NJH 2007-05-03 --->
<CFPARAM name="frmCountryGroupTypeID" default= "2">

<cfquery name="getCountryGroups" datasource="#application.siteDataSource#">
	select countryGroupTypeID, description, 0 as countryID, 'Please select a country group' as countryDescription, 1 as ordering
		from countryGroupType where countryGroupTypeID <> 1
	union
	select cgt.countryGroupTypeID, cgt.description, c.countryID, c.countryDescription, 2 as ordering from countryGroupType cgt
		inner join country c on c.countryGroupTypeID = cgt.countryGroupTypeID
	where cgt.countryGroupTypeID <> 1 and isoCode is null
	order by countryGroupTypeID, ordering, countryDescription
</cfquery>

<cfif not isDefined("frmCountryGroupID")>
	<cfquery name="getAllCountriesID" datasource="#application.siteDataSource#">
		select countryID from country where countryDescription = 'all countries'
	</cfquery>

	<cfset frmCountryGroupID = getAllCountriesID.countryID>
</cfif>

<!---
<CFPARAM name="frmCountryGroupID" default= "#getAllCountriesID.countryID#">

<cfquery name="getAllCountryGroups" datasource="#application.SiteDataSource#">
select convert(varchar(3),countryid) + '#application.delim1#' + countrydescription as validValue
	from country where ISOCode is null
		order by countryDescription
</cfquery> --->

<CFIF IsDefined("frmCountryGroupID")>

	<!--- NJH 2007-05-03 determine whether countries must be unique within that type of country groups --->
	<cfquery name="getUniqueCountries" datasource="#application.siteDataSource#">
		select uniqueCountries from countryGroupType where countryGroupTypeID =  <cf_queryparam value="#frmCountryGroupTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<!---Get all available users (whoever has a UserGroupID created in the UserGroup table--->
	<CFQUERY NAME="getAllCountries" datasource="#application.siteDataSource#">
		select countryid as dataValue, countrydescription as displayValue, case when cg.countrymemberid is not null then 1 else 0 end as isSelected
		from country c
				left join
			CountryGroup cg ON c.CountryID = cg.CountryMemberID and cg.countryGroupID = <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		where
			isNull(isocode,'') <> ''
			<!--- NJH 2007-05-04 can only have one country in one countryGroup if uniqueCountries is true --->
			<cfif getUniqueCountries.uniqueCountries eq 1>
				and countryID not in
				(select distinct countryMemberID from countryGroup cg inner join
						country c on cg.countryGroupID  = c.countryID
					where c.isoCode is null and c.countryGroupTypeID =  <cf_queryparam value="#frmCountryGroupTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</cfif>
		order by countryDescription
	</CFQUERY>
</CFIF>





<cfif isdefined('url.action') and url.action eq "add">

	<CF_RelayXMLEditor
		editorName = "thisEditor"
		xmlSourceVar = "#xmlSource#"
		add="yes"
		thisEmailAddress = "relayhelp@foundation-network.com">

<cfelse>

	<cfset application.com.request.setDocumentH1 (text = "Edit Country Group Members")>
	<CFPARAM name="message" default="">

	<form name="mainForm" method="post">
		<cf_relayFormDisplay>
			<cf_RelayFormElementDisplay relayFormElementType="HTML" label="Country Group Name">
				<CF_TwoSelectsRelated
					QUERY="GetCountryGroups"
					NAME1="frmCountryGroupTypeID"
					NAME2="frmCountryGroupID"
					VALUE1="countryGroupTypeID"
					VALUE2="countryID"
					DISPLAY1="description"
					DISPLAY2="countryDescription"
					SELECTED1="#IIF(isDefined('frmCountryGroupTypeID'),'frmCountryGroupTypeID',DE(''))#"
					SELECTED2="#IIF(isDefined('frmCountryGroupID'),'frmCountryGroupID',DE(''))#"
					MULTIPLE3="No"
					FORMNAME = "mainForm"
					width1 = "200px"
					width2 = "300px"
					ONCHANGE = "document.mainForm.submit();">
			</cf_RelayFormElementDisplay>
		</cf_relayFormDisplay>
	</form>

	<cfif isDefined("frmCountryGroupID")>

		<form method="post" name="form1"  >
			<cf_relayFormDisplay>

				<cf_RelayFormElementDisplay relayFormElementType="HTML" label="Country Group Members">
					<CF_INPUT TYPE="Hidden" NAME="SelectedCountryIDs" VALUE="">
					<cf_select name="SelectedCountryIDs" query="#getAllCountries#"  multiple="true">
					<CF_INPUT TYPE="Hidden" NAME="frmCountryGroupTypeID" VALUE="#frmCountryGroupTypeID#">
					<CF_INPUT TYPE="Hidden" NAME="frmCountryGroupID" VALUE="#frmCountryGroupID#">
				</cf_RelayFormElementDisplay>
				<cf_RelayFormElementDisplay relayFormElementType="Submit" fieldname="updateCountryGroups" value="Confirm Changes" label="">
			</cf_relayFormDisplay>
		</form>
	</cfif>

</cfif>



