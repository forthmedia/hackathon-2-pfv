<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		dedupeMatchesList.cfm	
Author:			SWJ
Date created:	2004-08-21

Description:	This is designed to show the dedupematches table

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03/Oct/2008			NJH			Bug Fix T-10 Issue 1061 - Added a merge state table to hold the merge state description. Fixed some bugs with TFQO while in there.
2012-10-04	WAB		Case 430963 Remove Excel Header 

--->
<cfparam name="dedupeEntityType" default="organisation">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Dedupe Matches</cf_title>
	</cf_head>


<cfif dedupeEntityType eq "organisation">
	<cfparam name="sortOrder" default="organisation_name">
	<cfparam name="numRowsPerPage" default="200">
	<cfparam name="startRow" default="1">
	<cfparam name="alphabeticalIndexColumn" default="Organisation_name"> <!--- NJH 2008-10-03 a bug fix that I noticed --->
	
	<!--- NJH 2008-10-03 Bug Fix T-10 Issue 1061 Added a mergeState table to store the description in--->
	<cfquery name="getDedupeMatches" datasource="#application.siteDataSource#">
		select * from (
		select Country,Organisation_name,OrganisationID,alphabeticalIndex,
			Description as Merge_State
		from vDedupeMatchesByOrg v
			inner join dedupeMergeState dms on v.merge_State = dms.mergeState
		) base
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

<!--- NJH 2008-10-03 altered some TFQO settings to fix some bugs... --->
<CF_tableFromQueryObject 
	queryObject="#getDedupeMatches#"
	sortOrder = "#sortOrder#"
	startRow = "#startRow#"
	numRowsPerPage="400"
	keyColumnList="organisation_name"
	keyColumnURLList="/data/dataFrame.cfm?frmSrchOrgID="
	keyColumnKeyList="OrganisationID"
	keyColumnOpenInWindowList="yes"
	FilterSelectFieldList="Country,Merge_State"
	alphabeticalIndexColumn="#alphabeticalIndexColumn#"
	hideTheseColumns="OrganisationID,alphabeticalIndex"
	useInclude="true"
>





