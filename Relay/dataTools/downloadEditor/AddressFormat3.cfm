<!--- �Relayware. All Rights Reserved 2014 --->
<!---

NJH 2009-04-20	Bug Fix All Sites Issue 2069 - check if there are derived fields first before updating formatdef table.
2013-04-03		YMA		Case 434502
--->

<CFSET TableSourceLookup = StructNew()>
<CFSET temp=StructInsert(TableSourceLookup,"LocationID","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address1","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address2","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address3","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address4","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address5","location")>
<!--- <CFSET temp=StructInsert(TableSourceLookup,"Address6","location")> --->
<CFSET temp=StructInsert(TableSourceLookup,"Address7","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address8","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Address9","location")>
<CFSET temp=StructInsert(TableSourceLookup,"PostalCode","location")>
<CFSET temp=StructInsert(TableSourceLookup,"SiteName","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Telephone","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Fax","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Direct","location")>
<CFSET temp=StructInsert(TableSourceLookup,"Salutation","person")>
<CFSET temp=StructInsert(TableSourceLookup,"FirstName","person")>
<CFSET temp=StructInsert(TableSourceLookup,"LastName","person")>
<CFSET temp=StructInsert(TableSourceLookup,"Email","person")>
<CFSET temp=StructInsert(TableSourceLookup,"EmailStatus","person")>
<CFSET temp=StructInsert(TableSourceLookup,"Faxphone","person")>
<CFSET temp=StructInsert(TableSourceLookup,"Officephone","person")>
<CFSET temp=StructInsert(TableSourceLookup,"Homephone","person")>
<CFSET temp=StructInsert(TableSourceLookup,"MobilePhone","person")>
<CFSET temp=StructInsert(TableSourceLookup,"Derived","derived")>
<CFSET temp=StructInsert(TableSourceLookup,"PersonID","person")>
<CFSET temp=StructInsert(TableSourceLookup,"CountryDescription","country")>


<CFSET FormFields = ListDeleteAt(form.fieldnames,ListFindNoCase(form.FIELDNAMES,"DerivedFieldValue"))>
<CFSET FormFields = ListDeleteAt(form.fieldnames,ListFindNoCase(form.FIELDNAMES,"submit"))>
<CFSET FormFields = ListDeleteAt(FormFields,ListFindNoCase(FormFields,"DOWNLOADFORMATID"))>
<CFSET FormFields = ListDeleteAt(FormFields,ListFindNoCase(FormFields,"_RWSESSIONTOKEN"))>	<!--- 2013-04-03	YMA	Case 434502 --->

<CFSET ID = DownloadFormatID>

<CFSET NewFormFields = FormFields>

<CFLOOP LIST="#formfields#" INDEX="field">
	<CFIF REFindNoCase("value",field) GT 0>
		<CFSET NewFormFields = ListDeleteAt(NewFormFields,ListFindNoCase(NewFormFields,field))>
	</CFIF>
</CFLOOP>

<CFSET FormFields = NewFormFields>

<CFLOOP LIST="#NewFormFields#" INDEX="field">
	<CFIF REFindNoCase("sortorder",field) GT 0>
		<CFSET FormFields = ListDeleteAt(FormFields,ListFindNoCase(FormFields,field))>
	</CFIF>
</CFLOOP>

<CFTRANSACTION>
	<CFQUERY NAME="DeleteAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
		DELETE FROM DownloadFormatDef WHERE DownloadFormatID =  <cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFQUERY NAME="InsertAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO DownloadFormatDef
		(FieldSource,HeadingLabel,SortOrder,TableSource,DownloadFormatID)
		VALUES
		('LocationID','LocationID',100,'location',<cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" >)
	</CFQUERY>
	<CFQUERY NAME="InsertAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO DownloadFormatDef
		(FieldSource,HeadingLabel,SortOrder,TableSource,DownloadFormatID)
		VALUES
		('PersonID','PersonID',100,'person',<cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" >)
	</CFQUERY>
	<CFLOOP LIST="#FormFields#" INDEX="field">
		<CFSET theSortOrder = field & "SortOrder">
		<CFSET theLabel = field & "value">
		
		<CFQUERY NAME="InsertAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO DownloadFormatDef
			(FieldSource,HeadingLabel,SortOrder,TableSource,DownloadFormatID)
			VALUES
			(<cf_queryparam value="#field#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Evaluate(theLabel)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Evaluate(theSortOrder)#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#StructFind(TableSourceLookup,field)#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</CFQUERY>
	</CFLOOP>
</CFTRANSACTION>

<CFQUERY NAME="GetDerivedFormatDefs" DATASOURCE="#application.SiteDataSource#">
	SELECT ItemID FROM DownloadFormatDef
	WHERE TableSource='derived'
	AND DownloadFormatID =  <cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- NJH 2009-04-20 Bug Fix All Sites Issue 2069 - check if record count is more than 0 --->
<cfif GetDerivedFormatDefs.recordCount gt 0>
	<CFQUERY NAME="InsertAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
		Update DownloadFormatDef SET
		fieldsource =  <cf_queryparam value="#form.DerivedFieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		WHERE ItemID =  <cf_queryparam value="#GetDerivedFormatDefs.ItemID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</cfif>

<CFQUERY NAME="GetAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
	SELECT DownloadFormatDef.*, DownloadFormat.Name
	FROM DownloadFormatDef INNER JOIN DownloadFormat ON DownloadFormatDef.DownloadFormatID=DownloadFormat.ItemID
	WHERE DownloadFormatDef.DownloadFormatID =  <cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	ORDER BY DownloadFormatDef.SortOrder
</CFQUERY>




<cf_head>
	<cf_title>Address Format Editor</cf_title>
</cf_head>


<CFSET current="addressFormat3.cfm">
<cfinclude template="/dataTools/dataToolsTopHead.cfm">

Details have been updated for <CFOUTPUT><B>#htmleditformat(GetAddressFormatDefs.Name)#</B></CFOUTPUT><P>
<TABLE CELLSPACING="2" CELLPADDING="2" BORDER="0">
<TR>
    <TD><B>Field</B></TD>
    <TD WIDTH="20">&nbsp;</TD>
    <TD><B>Label</B></TD>
    <TD WIDTH="20">&nbsp;</TD>
    <TD><B>Table</B></TD>
    <TD WIDTH="20">&nbsp;</TD>
	<TD><B>Sort Order</B></TD>
</TR>
<CFOUTPUT QUERY="GetAddressFormatDefs">
<TR>
    <TD>#htmleditformat(FieldSource)#</TD>
    <TD WIDTH="20">&nbsp;</TD>
    <TD>#htmleditformat(HeadingLabel)#</TD>
    <TD WIDTH="20">&nbsp;</TD>
    <TD>#htmleditformat(TableSource)#</TD>
    <TD WIDTH="20">&nbsp;</TD>
    <TD>#htmleditformat(SortOrder)#</TD>
</TR>
</CFOUTPUT>
</TABLE>
<P>
<cfoutput><A HREF="/dataTools/downloadEditor/AddressFormat1.cfm">Return to Country Definitions List</A></cfoutput>


