<!--- �Relayware. All Rights Reserved 2014 --->
<CFIF IsDefined("form.submit")>
	<CFIF form.submit EQ "Add Country">
		<CFLOCATION URL="AddressCountryEdit.cfm?action=add"addToken="false">
		<CF_ABORT>
	<CFELSEIF form.submit EQ "Delete Country">
		<CFLOCATION URL="AddressCountryEdit.cfm?action=delete&ItemID=#form.DownloadFormat#"addToken="false">
		<CF_ABORT>
	</CFIF>
</CFIF>

<CFQUERY NAME="GetAddressFormatDefs" DATASOURCE="#application.SiteDataSource#">
	SELECT *
	FROM DownloadFormatDef
	WHERE DownloadFormatID =  <cf_queryparam value="#form.DownloadFormat#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFSET delim = "#Chr(9)#">

<CFSET AddressTables = ValueList(GetAddressFormatDefs.TableSource, delim)>
<CFSET AddressFields = ValueList(GetAddressFormatDefs.FieldSource, delim)>
<CFSET AddressLabels = ValueList(GetAddressFormatDefs.HeadingLabel, delim)>
<CFSET AddressOrder = ValueList(GetAddressFormatDefs.SortOrder, delim)>




<cf_head>
	<cf_title>Address Format Editor</cf_title>
</cf_head>


<CFSET current="addressFormat2.cfm">
<cfinclude template="/dataTools/dataToolsTopHead.cfm">

<p>Please check and supply a heading and sort order for the fields that you require.</p>
<CFOUTPUT>
<FORM NAME="AddressFormatUpdate" ACTION="/dataTools/downloadEditor/AddressFormat3.cfm" METHOD="POST">


<CF_INPUT TYPE="hidden" NAME="DownloadFormatID" VALUE="#form.DownloadFormat#">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<TR>
    <TD><B>Address1</B><BR><INPUT TYPE="checkbox" NAME="Address1" VALUE="Address1" <CFSET position=ListFindNoCase(AddressFields,"Address1", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address1Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address1" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address1SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="1" </CFIF> ></TD>
    <TD><B>Address2</B><BR><INPUT TYPE="checkbox" NAME="Address2" VALUE="Address2" <CFSET position=ListFindNoCase(AddressFields,"Address2", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address2Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address2" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address2SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="2" </CFIF> ></TD>
    <TD><B>Address3</B><BR><INPUT TYPE="checkbox" NAME="Address3" VALUE="Address3" <CFSET position=ListFindNoCase(AddressFields,"Address3", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address3Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address3" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address3SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="3" </CFIF> ></TD>
</TR>
    <TD><B>Address4</B><BR><INPUT TYPE="checkbox" NAME="Address4" VALUE="Address4" <CFSET position=ListFindNoCase(AddressFields,"Address4", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address4Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address4" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address4SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="4" </CFIF> ></TD>
    <TD><B>Address5</B><BR><INPUT TYPE="checkbox" NAME="Address5" VALUE="Address5" <CFSET position=ListFindNoCase(AddressFields,"Address5", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address5Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address5" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address5SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="5" </CFIF> ></TD>
    <TD>&nbsp;<!--- <B>Address6</B><BR><INPUT TYPE="checkbox" NAME="Address6" VALUE="Address6" <CFSET position=ListFindNoCase(AddressFields,"Address6", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address6Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address6" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address6SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="6" </CFIF> > ---></TD>
</TR>
<TR>
    <TD><B>Address7</B><BR><INPUT TYPE="checkbox" NAME="Address7" VALUE="Address7" <CFSET position=ListFindNoCase(AddressFields,"Address7", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address7Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address7" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address7SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="7" </CFIF> ></TD>
    <TD><B>Address8</B><BR><INPUT TYPE="checkbox" NAME="Address8" VALUE="Address8" <CFSET position=ListFindNoCase(AddressFields,"Address8", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address8Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address8" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address8SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="8" </CFIF> ></TD>
    <TD><B>Address9</B><BR><INPUT TYPE="checkbox" NAME="Address9" VALUE="Address9" <CFSET position=ListFindNoCase(AddressFields,"Address9", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="Address9Value" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Address9" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="Address9SortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="9" </CFIF> ></TD>
</TR>
<TR>
    <TD><B>PostalCode</B><BR><INPUT TYPE="checkbox" NAME="PostalCode" VALUE="PostalCode" <CFSET position=ListFindNoCase(AddressFields,"PostalCode", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="PostalCodeValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="PostalCode" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="PostalCodeSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="10" </CFIF> ></TD>
    <TD><B>CountryDescription</B><BR><INPUT TYPE="checkbox" NAME="CountryDescription" VALUE="CountryDescription" <CFSET position=ListFindNoCase(AddressFields,"CountryDescription", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="CountryDescriptionValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="CountryDescription" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="CountryDescriptionSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="11" </CFIF> ></TD>
    <TD><B>Telephone</B><BR><INPUT TYPE="checkbox" NAME="telephone" VALUE="telephone" <CFSET position=ListFindNoCase(AddressFields,"telephone", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="telephoneValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="telephone" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="telephoneSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="12" </CFIF> ></TD>
</TR>
<TR>
    <TD><B>fax</B><BR><INPUT TYPE="checkbox" NAME="fax" VALUE="fax" <CFSET position=ListFindNoCase(AddressFields,"fax", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="faxValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="fax" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="faxSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="13" </CFIF> ></TD>
    <TD><B>direct</B><BR><INPUT TYPE="checkbox" NAME="direct" VALUE="direct" <CFSET position=ListFindNoCase(AddressFields,"direct", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="directValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="direct" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="directSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="14" </CFIF> ></TD></TD>
    <TD><B>salutation</B><BR><INPUT TYPE="checkbox" NAME="salutation" VALUE="salutation" <CFSET position=ListFindNoCase(AddressFields,"salutation", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="salutationValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="salutation" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="salutationSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="15" </CFIF> ></TD>
</TR>
<TR>
    <TD><B>FirstName</B><BR><INPUT TYPE="checkbox" NAME="FirstName" VALUE="FirstName" <CFSET position=ListFindNoCase(AddressFields,"FirstName", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="FirstNameValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="FirstName" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="FirstNameSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="16" </CFIF> ></TD>
    <TD><B>LastName</B><BR><INPUT TYPE="checkbox" NAME="LastName" VALUE="LastName" <CFSET position=ListFindNoCase(AddressFields,"LastName", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="LastNameValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="LastName" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="LastNameSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="17" </CFIF> ></TD>
    <TD><B>Email</B><BR><INPUT TYPE="checkbox" NAME="Email" VALUE="Email" <CFSET position=ListFindNoCase(AddressFields,"Email", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="EmailValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="Email" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="EmailSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="18" </CFIF> ></TD>
<TR>
    <TD><B>EmailStatus</B><BR><INPUT TYPE="checkbox" NAME="EmailStatus" VALUE="EmailStatus" <CFSET position=ListFindNoCase(AddressFields,"EmailStatus", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="EmailStatusValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="EmailStatus" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="EmailStatusSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="19" </CFIF> ></TD>
    <TD><B>faxphone</B><BR><INPUT TYPE="checkbox" NAME="faxphone" VALUE="faxphone" <CFSET position=ListFindNoCase(AddressFields,"faxphone", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="faxphoneValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="faxphone" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="faxphoneSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="20" </CFIF> ></TD>
    <TD><B>officephone</B><BR><INPUT TYPE="checkbox" NAME="officephone" VALUE="officephone" <CFSET position=ListFindNoCase(AddressFields,"officephone", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="officephoneValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="officephone" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="officephoneSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="21" </CFIF> ></TD>
</TR>
<TR>
    <TD><B>homephone</B><BR><INPUT TYPE="checkbox" NAME="homephone" VALUE="homephone" <CFSET position=ListFindNoCase(AddressFields,"homephone", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="homephoneValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="homephone" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="homephoneSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="22" </CFIF> ></TD>
    <TD><B>mobilephone</B><BR><INPUT TYPE="checkbox" NAME="mobilephone" VALUE="mobilephone" <CFSET position=ListFindNoCase(AddressFields,"mobilephone", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="mobilephoneValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="mobilephone" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="mobilephoneSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="23" </CFIF> ></TD>
    <TD><B>SiteName</B><BR><INPUT TYPE="checkbox" NAME="SiteName" VALUE="SiteName" <CFSET position=ListFindNoCase(AddressFields,"SiteName", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="SiteNameValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="SiteName" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="SiteNameSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="24" </CFIF> ></TD>
</TR>
<TR>
    <TD COLSPAN="3">Please check and enter the derived rule information in the box below</TD>
</TR>

<!--- #IIF(mergeInfo.sex EQ "M", DE("Male Salutation"), DE("Female Salutation"))#  --->

<TR>
    <TD COLSPAN="3"><B>Derived</B><BR><INPUT TYPE="checkbox" NAME="derived" VALUE="derived" <CFSET position=ListFindNoCase(AddressTables,"derived", delim)><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="derivedValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position, delim)#" <CFELSE> VALUE="derived" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="derivedSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position, delim)#" <CFELSE> VALUE="25" </CFIF> ><BR>
	<INPUT TYPE="text" NAME="DerivedFieldValue" <CFIF position GT 0> VALUE="#htmleditformat(ListGetAt(AddressFields,position, delim))#" <CFELSE> VALUE="##IIF(mergeInfo.sex EQ "M", DE("Male Salutation"), DE("Female Salutation"))##" </CFIF> SIZE="80" MAXLENGTH="120"></TD>
<!--- 	<TD COLSPAN="3"><B>Derived Field Rule</B><BR><INPUT TYPE="checkbox" NAME="derived" VALUE="derived" <CFSET position=ListFindNoCase(AddressFields,"derived")><CFIF position GT 0>CHECKED</CFIF>>&nbsp;&nbsp;<INPUT TYPE="text" NAME="derivedValue" <CFIF position GT 0> VALUE="#ListGetAt(AddressLabels,position)#" <CFELSE> VALUE="Derived" </CFIF> SIZE="20" MAXLENGTH="20"> <INPUT TYPE="text" NAME="derivedSortOrder" SIZE="2" MAXLENGTH="2" <CFIF position GT 0> VALUE="#ListGetAt(AddressOrder,position)#" <CFELSE> VALUE="25" </CFIF> ><BR>
	<INPUT TYPE="text" NAME="DerivedValue" SIZE="80" MAXLENGTH="120"></TD>
 ---></TR>
 </TABLE>
<P>
<INPUT TYPE="submit" NAME="Submit" VALUE="Update Details">
</CFOUTPUT>
</FORM>


