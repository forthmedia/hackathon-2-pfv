<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:			
Author:			
Date created:	

Description: Fix junk carriage returns in a specified table and column

			Look for char(13) and char(10)

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:

--->

<cf_head>
	<cf_title>Find CRLF's or TAB chars</cf_title>
</cf_head>


<!--- Select Organisationid into #org
From #TableName#
Where #ColumnName# like '%'+char(10)+'%' OR #ColumnName# like '%'+char(13)+'%'

select count(*) from #org
 --->
 
 
 
<!--- WARNING this needs to be passed as a parameter --->
<CFPARAM NAME="primarykey" DEFAULT="locationID">


<!--- this procedure produces the corrected data --->
<CFIF isDefined('fixRecords')>
	<CFQUERY NAME="UpdateChars" datasource="#application.sitedatasource#">
		Update [#DatabaseName#].dbo.#TableName#
		Set #ColumnName#=stuff(#ColumnName#,charindex( char(#char#),#ColumnName#),1,' ')
		Where #ColumnName# like '%'+char(#char#)+'%'
	</CFQUERY>	
</CFIF>

<CFQUERY NAME="ShowFixes" datasource="#application.sitedatasource#" DBNAME="#databaseName#">
	Select #primaryKey# as TablePK, stuff(#ColumnName#,charindex( char(#char#),#ColumnName#),1,'char #char#')
		AS NewValue, #ColumnName# AS OldValue
		From [#DatabaseName#].dbo.#TableName#
		Where #ColumnName# like '%'+char(#char#)+'%'
		<CFIF isdefined('locationIDs')> or #primaryKey# in (#locationIDs#)</CFIF>
</CFQUERY>


<H2><CFOUTPUT>Records in #htmleditformat(DatabaseName)#.dbo.#htmleditformat(TableName)#.#htmleditformat(ColumnName)# with char(#htmleditformat(char)#)</CFOUTPUT></H2>
<TABLE BORDER="1">
	<TR>
		<TH>Primary Key</TH>
		<TH>Char problems</TH>
		<TH>Actual values</TH>
	</TR>
	<CFOUTPUT QUERY="ShowFixes">
		<TR>
			<TD>#htmleditformat(TablePK)#</TD>
			<TD>#htmleditformat(NewValue)#</TD>
			<TD>#htmleditformat(OldValue)#</TD>
		</TR>
	</CFOUTPUT>
</TABLE>
<CFOUTPUT>#ShowFixes.RecordCount# row(s) returned<BR>
			locationIDs = #htmleditformat(valueList(ShowFixes.TablePK))#
</CFOUTPUT>
<CFIF ShowFixes.RecordCount GT 0>
	<CFOUTPUT>
	<FORM ACTION="FixJunkCarriageReturns.cfm?#request.query_string#" method="post">
		<INPUT TYPE="submit" NAME="fixRecords" VALUE="fixRecords">
		<CF_INPUT TYPE="hidden" NAME="locationIDs" VALUE="#valueList(ShowFixes.TablePK)#">
	</FORM>
	</CFOUTPUT>
<CFELSE>
<P><B>No records to fix</B></P>
</CFIF>




