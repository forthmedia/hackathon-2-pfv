<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:	MassDedupe2.cfm
Author:		WAB then SWJ then NM
Date created:	Initially in May 99 however updated in Sept 2000 and updated (completely reworked for mass deduping) in August 2004

Description:	This file gets all of the matching organisations from a view called
				vMatchingOrgsByCountry.  It then groups them by the first organisation and
				lists them for the user to check.  Having done that it allows the
				user to merge

				vMatchingOrgsByCountry is defined be matching the first 20 characters
				of orgnames in the same country where the locations of those orgs
				are in a defined view called vPartnerEntityIDs.

				vPartnerEntityIDs gets a list of LocationID's from thr booleanFlagData table
				by searching for flag ID's that are in a view called vPartnerFlagIDs.

				vPartnerFlags creates a list of flagIDs for a given set of PartnerTypes.

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ	Fixed the delete query to work with the new x-table structure

24-Aug-2006			NJH 	Gave user a choice of which records to match on a manual merge, not necessarily matching them all.
16-May-2008			NJH		Display the results of the merge
25-Jun-2009			NJH		Bug Fix Sony1 Support Issue 2387. Increase timeout from 5 minutes to 20
2014-11-18			GCC		Add trims to getMatchAmount QoQs because lower in QoQ removes leading spaces so without a trim you get a missmatch
2015-11-22			SB		Bootstrap
Enhancements still to do:

need to warn the user their will be a delay while we update the matchnames
--->

<!--- NJH 2009/06/25 Bug Fix Sony1 Support Issue 2387. Increase timeout from 5 minutes to 20 --->
<cfsetting requesttimeout="1200" enablecfoutputonly="false">
<cfoutput>
<SCRIPT type="text/javascript">

	function dedupePersons(){
		var form = document.personDedupeForm;
		var checked = 0;
		for(i=0; i<form.frmDupPeople.length; i++){
			if(form.frmDupPeople[i].checked==true){
				checked=1;
			}
		}
		if(!checked){
			alert("You must select at least two people who are not users to start the dedupe process.");
		}
		else{
			form.submit();
		}
	}

	function dedupeOrganisations(){
		var form = document.personDedupeForm;
		var checked = 0;
		for(i=0; i<form.frmorgids.length; i++){
			if(form.frmorgids[i].checked==true){
				checked=1;
			}
		}
		if(!checked){
			alert("You must select at least two organisations to start the dedupe process.");
		}
		else{
			form.action='/data/orgDedupe.cfm';
			form.submit();
		}
	}

	function getSelectedRadioIndex(obj) {
		for (var i = 0; i < obj.length; i++) {
			if (obj[i].checked) {
				return i;
			}
		}
	}
	//disables the radio group for the duplicate set and then opens the relevant action page in a popup, passing vars on url.
	//reason for popup was origionally in order to affect caller window with js. this may not be necessary anymore. will look into this once decided.
	function doManualMerge(mergeObj,incr,type,amount) {
		//var mergeValue = mergeObj[getSelectedRadioIndex(mergeObj)].value;
		//disableItem(incr,amount);
		var mergeValue = "";
		var checkBoxArray = document.getElementsByName(mergeObj);
		var firstElement = true;

		// NJH 2006/08/24 get entities to manually merge from checked checkboxes
		for (var i = 0; i < checkBoxArray.length;i++) {
			if (checkBoxArray[i].checked) {
				if (firstElement) {
					mergeValue = checkBoxArray[i].value;
					firstElement = false;
				} else {
					mergeValue = mergeValue + "," + checkBoxArray[i].value;
				}
			}
		}

		if (type == "organisation") {
			var winToOpen = '/data/orgdedupe.cfm?frmorgids='+mergeValue+'&frmRunInPopUp=true&frmRadioName='+incr+'&frmRadioAmount='+amount;
			window.open(winToOpen,'mergeWin','width=750,height=800,left=50,top=50,location=no,menubar=no,resizable=yes,status=yes,scrollbars=yes');
		}

		if (type == "location") {
			var winToOpen = '/data/locdedupe.cfm?frmDupLocs='+mergeValue+'&frmRunInPopUp=true&frmRadioName='+incr+'&frmRadioAmount='+amount;
			window.open(winToOpen,'mergeWin','width=750,height=800,left=50,top=50,location=no,menubar=no,resizable=yes,status=yes,scrollbars=yes');
		}
	}

	function doManualMergePerson(mergeObj,incr,amount,countryID) {
		//var mergeValue = mergeObj[getSelectedRadioIndex(mergeObj)].value;
		var mergeValue = "";
		var checkBoxArray = document.getElementsByName(mergeObj);
		var firstElement = true;

		// NJH 2006/08/24 get entities to manually merge from checked checkboxes
		for (var i = 0; i < checkBoxArray.length;i++) {
			if (checkBoxArray[i].checked) {
				if (firstElement) {
					mergeValue = checkBoxArray[i].value;
					firstElement = false;
				} else {
					mergeValue = mergeValue + "," + checkBoxArray[i].value;
				}
			}
		}

		var winToOpen = '/data/persondedupe.cfm?frmDupPeople='+mergeValue+'&frmCountryID='+countryID+'&frmRunInPopUp=true&frmRadioName='+incr+'&frmRadioAmount='+amount;
		window.open(winToOpen,'mergeWin','width=750,height=800,left=50,top=50,location=no,menubar=no,resizable=yes,status=yes,scrollbars=yes');
	}
	//disables the radio group specified. unchecks any checkd values in the group. sets merge button text to "Merged". runs check for enabling of auto merge button.
	function disableItem(itemNumber,amount) {
		var radioObj = eval("document.dedupeForm.frmMergeType"+itemNumber);
		var mergeButtonObj = eval("document.dedupeForm.frmDoMerge"+itemNumber);

		for (var i = 0; i < radioObj.length;i++) {
			if (radioObj[i].checked) {
				radioObj[i].checked = false;
			}
			radioObj[i].disabled = true;
			mergeButtonObj.value = "Merged."
			mergeButtonObj.disabled = true;
			autoMergeEnableDisable(amount);
		}

		// NJH 2006/08/24 disable checkboxes after a manual merge has been completed
		var checkBoxObjs = document.getElementsByName('frmIncInMerge'+itemNumber);
		for (var i = 0; i < checkBoxObjs.length;i++) {
			checkBoxObjs[i].disabled=true;
		}

	}
	//loop through available radio groups and checks if any are in the manual merge state. if so, disables to auto merge button. if not, enables it.
	function autoMergeEnableDisable(amount) {
		var isDisabled = false;
		for (var i = 1; i <= amount; i++) {
			var radioObj = eval("document.dedupeForm.frmMergeType"+i);

			//alert(radioObj);
			if (radioObj[1].checked) {
				isDisabled = true;
				break;
			}

		}
		document.dedupeForm.autoMerge.disabled = isDisabled;
	}
	//runs decision popup for user and returns user input.
	function doConfirm() {
		return confirm("This action will disable this duplicate for automatic merging regardless of whether you run the manual merge operations or not.\nHowever, the duplicate will be re-enabled for automatic merging in the next set of duplicates if you do not run any of the manual merging routines now.\n\n are you sure you want to do this?.");
	}

	// NJH 2006/08/24 enable/disable checkboxes
	function disableCheckBoxes(checkBox,disable) {
		var checkBoxArray = document.getElementsByName(checkBox);
		for (var i = 0; i < checkBoxArray.length;i++) {
			checkBoxArray[i].disabled = disable;
		}
	}

</script>
</cfoutput>

<cfparam name="message" default="">
<cfparam name="dedupeEntityType" default="">
<cfparam name="MatchMethodID" default="">
<cfparam name="allowDedupeAcrossOrgs" type="boolean" default="no">  <!--- NJH 2008/02/14 Blocked/unBlocked --->

<CFIF listlen(request.relaycurrentuser.countrylist,",") EQ 1>
	<CFSET frmCountryID = request.relaycurrentuser.countrylist>
</CFIF>

<!--- get duplicate type (tableName) we are working with. is passed in list from select box option on first page. --->
<CFIF isDefined("frmMatchData") >
	<cfset dedupeEntityType = "#listGetAt(frmMatchData,1)#">
	<cfset matchMethodID = "#listGetAt(frmMatchData,4)#">

	<!--- NJH 2008/02/08 TND 510 Blocked/Unblocked. Getting the dedupeType--->
	<cfscript>
		dedupeTypeID = application.com.dedupe.getMatchMethodData(dedupeMatchMethodID=matchMethodID).dedupeTypeID;
	</cfscript>

	<!--- NJH 2008/02/08 if the merge type is merge only, merge locations that are in different organisations--->
	<cfif dedupeTypeID eq 3>
		<cfset allowDedupeAcrossOrgs = "yes">
	</cfif>
</cfif>

<!--- if user has clicked merge matches, this will run. --->
<CFIF isDefined("frmRunMatches")>
	<!--- retrieve all items set for automatic merging and set into one list. --->
	<!--- retrieve all items set for not duplicates and set into one list. --->
	<!--- final lists are structured: IDTOKEEP|IDTODELETE,IDTOKEEP|IDTODELETE. --->
	<cfset runMatches = "">
	<cfset notDupes = "">
	<cfloop from="1" to="#frmMatchAmount#" index="x">
		<!--- if the element is checked on previous form, and the element has IDs to loop through. --->
		<cfif structKeyExists(form,"frmMergeType"&x) and listLen(evaluate("frmMergeType"&x),",|") gte 2>
			<!--- "N"~ at start of item indicates that the item is selected for "not a dupelicate". --->
			<cfif listGetAt(evaluate("frmMergeType"&x),1,"~") eq "N">
				<cfif notDupes eq "">
					<cfset notDupes = listDeleteAt(evaluate("frmMergeType"&x),1,"~")>
				<cfelse>
					<cfset notDupes = notDupes & "," & trim(listDeleteAt(evaluate("frmMergeType"&x),1,"~"))>
				</cfif>
			<!--- otherwise it must be a duplicate to auto merg. --->
			<cfelse>
				<cfif runMatches eq "">
					<cfset runMatches = evaluate("frmMergeType"&x)>
				<cfelse>
					<cfset runMatches = runMatches & ","&trim(evaluate("frmMergeType"&x))>
				</cfif>
			</cfif>
		</cfif>
	</cfloop>

	<!--- if there are duplicates to auto merge. --->
	<cfif runMatches neq "">
		<!--- setup array of information about merges for diplay later on. --->
		<cfset mergedMatches = arrayNew(1)>
		<!--- loop first level of list with only "," as delimiter. leaving list items of 2 ID's seperated with "|". --->
		<cfloop list="#runMatches#" index="match">
			<!--- retrieve data for the 2 ID's in resultant list. pass these records to method getBestMatchData() and have one good set of data returned.
				  write good data to table. --->
			<cfscript>
				goodData = application.com.dedupe.getEntityData(listGetAt(match,1,"|"),dedupeEntityType);
				badData = application.com.dedupe.getEntityData(listGetAt(match,2,"|"),dedupeEntityType);
				bestMatch = application.com.dedupe.getBestMatchData(listGetAt(match,1,"|"),listGetAt(match,2,"|"),dedupeEntityType);
				application.com.dedupe.writeBestMatchData(bestMatch,dedupeEntityType,listGetAt(match,1,"|"));
			</cfscript>
			<!--- NJH 2008/02/07 TND510 Blocked/Unblocked.. include a client specific pre-merge file and wrap the merge into a transaction--->
			<cftransaction>
				<cftry>

					<cfif fileexists("#application.paths.code#\cftemplates\dedupePreMerge.cfm")>
						<cfinclude template="/code/cftemplates/dedupePreMerge.cfm">
					</cfif>

					<!--- do merge on data based on dedupeEntityType. also store relevant data into array for display later on, based on dedupeEntityType. --->
					<cfswitch expression="#dedupeEntityType#">
						<cfcase value="Person">
							<!--- do merge --->
							<cfscript>
								success = application.com.dedupe.mergePerson(listGetAt(match,1,"|"),listGetAt(match,2,"|"));
							</cfscript>

							<cfset itemStruct = structNew()>
							<!--- NJH 2008/05/16 mergePerson doesn't return anything, so set the result to success --->
							<cfset itemStruct.Result = success.message>
							<cfset itemStruct.kept = structNew()>
							<cfset itemStruct.kept.PersonID = listGetAt(match,1,"|")>
							<cfset itemStruct.kept.FirstName = bestMatch.FirstName>
							<cfset itemStruct.kept.LastName = bestMatch.LastName>
							<cfset itemStruct.kept.Email = bestMatch.Email>
							<cfset itemStruct.deleted = structNew()>
							<cfset itemStruct.deleted.PersonID = listGetAt(match,2,"|")>
							<cfset itemStruct.deleted.FirstName = badData.FirstName>
							<cfset itemStruct.deleted.LastName = badData.LastName>
							<cfset itemStruct.deleted.Email = badData.Email>
							<cfset aa = ArrayAppend(mergedMatches,itemStruct)>
						</cfcase>
						<cfcase value="Organisation">
							<!--- do merge --->
							<cfscript>
								success = application.com.dedupe.mergeOrganisation(listGetAt(match,1,"|"),listGetAt(match,2,"|"));
							</cfscript>

							<cfset itemStruct = structNew()>
							<!--- NJH 2008/05/16 mergeOrganisation doesn't return anything, so set the result to success --->
							<cfset itemStruct.Result = success.message>
							<cfset itemStruct.kept = structNew()>
							<cfset itemStruct.kept.OrganisationID = listGetAt(match,1,"|")>
							<cfset itemStruct.kept.OrganisationName = bestMatch.OrganisationName>
							<cfset itemStruct.deleted = structNew()>
							<cfset itemStruct.deleted.OrganisationID = listGetAt(match,2,"|")>
							<cfset itemStruct.deleted.OrganisationName = badData.OrganisationName>
							<cfset aa = ArrayAppend(mergedMatches,itemStruct)>
						</cfcase>

						<cfcase value="Location">
							<!--- do merge --->
							<!--- NJH 2008/02/14 TND510 Blocked/Unblocked..allow deduping of locations across organisations for a match method of 'MergeOnly'--->
							<cfscript>
								success = application.com.dedupe.mergeLocation(IDToKeep=listGetAt(match,1,"|"),IDToDelete=listGetAt(match,2,"|"),allowDedupeAcrossOrgs=allowDedupeAcrossOrgs);
							</cfscript>

							<cfset itemStruct = structNew()>
							<cfset itemStruct.Result = success.message>
							<cfset itemStruct.kept = structNew()>
							<cfset itemStruct.kept.LocationID = listGetAt(match,1,"|")>
							<cfset itemStruct.kept.LocationAddress = bestMatch.address1 & " " & bestMatch.address2 & " " & bestMatch.postalCode>
							<cfset itemStruct.deleted = structNew()>
							<cfset itemStruct.deleted.LocationID = listGetAt(match,2,"|")>
							<cfset itemStruct.deleted.LocationAddress = badData.address1 & " " & badData.address2 & " " & badData.postalCode>
							<cfset aa = ArrayAppend(mergedMatches,itemStruct)>
						</cfcase>
					</cfswitch>

					<cfif fileexists("#application.paths.code#\cftemplates\dedupePostMerge.cfm")>
						<cfinclude template="/code/cftemplates/dedupePostMerge.cfm">
					</cfif>

					<cfcatch type="Database">
						<cftransaction action="rollback"/>
					</cfcatch>
				</cftry>
			</cftransaction>

		</cfloop>
	</cfif>

	<!--- if there are duplicates to set as non duplicates. --->
	<cfif notDupes neq "">
		<!--- setup array of information about duplicates for diplay later on. --->
		<cfset notDupeMatches = arrayNew(1)>
		<cfloop list="#notDupes#" index="ndmatch">
			<!--- retrieve data for the 2 ID's and then set mathced pair as non duplicated in dedupeMAtches table. --->
			<cfscript>
				goodData = application.com.dedupe.getEntityData(listGetAt(ndmatch,1,"|"),dedupeEntityType);
				badData = application.com.dedupe.getEntityData(listGetAt(ndmatch,2,"|"),dedupeEntityType);
				application.com.dedupe.setNotDupe(listGetAt(ndmatch,1,"|"),listGetAt(ndmatch,2,"|"),dedupeEntityType);
			</cfscript>

			<!--- store relevant data into array for display later on, based on dedupeEntityType. --->
			<cfswitch expression="#dedupeEntityType#">
				<cfcase value="person">
					<cfset itemStruct = structNew()>
					<cfset itemStruct.kept = structNew()>
					<cfset itemStruct.kept.PersonID = listGetAt(ndmatch,1,"|")>
					<cfset itemStruct.kept.FirstName = goodData.FirstName>
					<cfset itemStruct.kept.LastName = goodData.LastName>
					<cfset itemStruct.kept.Email = goodData.Email>
					<cfset itemStruct.kept.MatchName = goodData.FirstName&goodData.LastName&goodData.Email>
					<cfset itemStruct.deleted = structNew()>
					<cfset itemStruct.deleted.PersonID = listGetAt(ndmatch,2,"|")>
					<cfset itemStruct.deleted.FirstName = badData.FirstName>
					<cfset itemStruct.deleted.LastName = badData.LastName>
					<cfset itemStruct.deleted.Email = badData.Email>
					<cfset itemStruct.deleted.MatchName = badData.FirstName&badData.LastName&badData.Email>
					<cfset aa = ArrayAppend(notDupeMatches,itemStruct)>
				</cfcase>
				<cfcase value="organisation">
					<cfset itemStruct = structNew()>
					<cfset itemStruct.kept = structNew()>
					<cfset itemStruct.kept.OrganisationID = listGetAt(ndmatch,1,"|")>
					<cfset itemStruct.kept.OrganisationName = goodData.OrganisationName>
					<cfset itemStruct.kept.MatchName = goodData.MatchName>
					<cfset itemStruct.deleted = structNew()>
					<cfset itemStruct.deleted.OrganisationID = listGetAt(ndmatch,2,"|")>
					<cfset itemStruct.deleted.OrganisationName = badData.OrganisationName>
					<cfset itemStruct.deleted.MatchName = badData.MatchName>
					<cfset aa = ArrayAppend(notDupeMatches,itemStruct)>
				</cfcase>
				<cfcase value="location">
					<cfset itemStruct = structNew()>
					<cfset itemStruct.kept = structNew()>
					<cfset itemStruct.kept.LocationID = listGetAt(ndmatch,1,"|")>
					<cfset itemStruct.kept.LocationAddress = goodData.address1 & " " & goodData.address2 & " " & goodData.postalCode>
					<cfset itemStruct.kept.MatchName = Trim(ReplaceNoCase(ReplaceNoCase(ReplaceNoCase(goodData.address1," ","","all"),".","","all"),",","","all")) & "-" & Trim(ReplaceNoCase(goodData.postalCode," ","","all"))>
					<cfset itemStruct.deleted = structNew()>
					<cfset itemStruct.deleted.LocationID = listGetAt(ndmatch,2,"|")>
					<cfset itemStruct.deleted.LocationAddress = badData.address1 & " " & badData.address2 & " " & badData.postalCode>
					<cfset itemStruct.deleted.MatchName = Trim(ReplaceNoCase(ReplaceNoCase(ReplaceNoCase(badData.address1," ","","all"),".","","all"),",","","all")) & "-" & Trim(ReplaceNoCase(badData.postalCode," ","","all"))>
					<cfset aa = ArrayAppend(notDupeMatches,itemStruct)>
				</cfcase>
			</cfswitch>
		</cfloop>
	</cfif>
</CFIF>

<!--- run on first visual page for populating select box. --->
<CFIF NOT isDefined("frmMatchData") and NOT isDefined("frmRunMatches")>
	<!--- Get matches for coutries associated to user, seperated by duplicateType and tableName. --->
	<cfscript>
		getMyCountries = application.com.dedupe.getMatchesForSelect(request.relaycurrentuser.countrylist);
	</cfscript>

	<!--- <cfinvoke component="application.com.dedupe" method="getMatchesForSelect" returnvariable="getMyCountries" countryIDs="#request.relaycurrentuser.countrylist#"> --->
</cfif>


<!---If this page has been run automatically we don't need this output --->
<CFIF not isDefined("scheduledMatch")>
	<!--- run on all pages once a match type is selected. retrieves the match data. --->
	<CFIF isDefined("frmMatchData") >
		<cfscript>
			getAllMatches = application.com.dedupe.getMatchesForDisplay(tablename = dedupeEntityType,duplicateType = listGetAt(frmMatchData,2),countryID = listGetAt(frmMatchData,3),matchMethodID = listGetAt(frmMatchData,4));
		</cfscript>
		<!--- <cfinvoke component="application.com.dedupe" method="getMatchesForDisplay" returnvariable="getAllMatches" tableName="#dedupeEntityType#" duplicateType="#listGetAt(frmMatchData,2)#" countryID="#listGetAt(frmMatchData,3)#"> --->
	</CFIF>

		<cf_includejavascriptonce template = "/javascript/openwin.js">

	<CFSET current="massDedupe2.cfm">
	<!--- first page... slection of match type to run. --->
	<CFIF NOT isDefined("frmMatchData") and NOT isDefined("frmRunMatches")>
		<cfoutput><FORM NAME="thisForm" ACTION="/dataTools/massdedupe2.cfm" METHOD="POST"></cfoutput>

			<!--- Only show these instructions the first time you come into the screen --->
			<p>
				Using this tool you can merge matching organisations, persons and locations.  First you must choose the view you
				want to use.  Each view will find matches in different ways.  This should be self explanatory
				from the name of the view.  Different views will find different matches.
			</p>
			<p><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></p>
			<cfif getMyCountries.recordCount gt 0>
			<div class="form-group">
				<label for="frmMatchData">Choose the match you want to work on:</label>
				<SELECT NAME="frmMatchData" id="frmMatchData">
				<CFIF getMyCountries.RecordCount GT 1>
					<OPTION VALUE="">Select a match to work on
				</CFIF>
				<CFOUTPUT QUERY="getMyCountries">

					<OPTION VALUE="#tableName#,#duplicateType#,#CountryID#,#dedupeMatchMethodID#">#htmleditformat(CountryDescription)# (#htmleditformat(numrecs)#) #htmleditformat(dedupeViewType)# duplicate #htmleditformat(dedupeMatchMethodDescription)#
				</CFOUTPUT>
				</SELECT>
				<INPUT TYPE="Hidden" NAME="frmMatchData_required" VALUE="You must specify a match type.">
			</div>
			<cfelse>

					<cfoutput>
						<p><strong>Currently no potential duplicates exist for countries you manage data for.</strong></p>
						<cfset hasCorrectSecurity = application.com.login.checkInternalPermissions ("AdminTask", "Level3")>
						<cfif hasCorrectSecurity>
							<h3>To generate duplicates please follow one of the two links below:</h3>
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<div class="grey-box-top">
										<p>Re-create Match Keys if you need to regenerate the organisation match keys.  (Match keys are used in some of the matching methods employed for finding organisational duplicates.)</p>
										<a href="/dataTools/updateMatchNames.cfm" class="btn btn-primary">Re-create Match Keys</a>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="grey-box-top">
										<p>Find Dupes if you want to run matching routines to find duplicates in the database.</p>
										<a href="/dataTools/dedupeFindDupes.cfm" class="btn btn-primary">Find Dupes</a>
									</div>
								</div>
							</div>
							<p>(Both of these options are available in the menu links above.)</p>
						</cfif>
					</cfoutput>
			</cfif>

			<cfif getMyCountries.recordCount gt 0>
				<cfoutput><input type="button" value="Display Duplicates." onClick="this.value = 'Please wait.  This can take 20-30 seconds...'; this.disabled = true; document.thisForm.submit()"></cfoutput>
			</cfif>
		</FORM>
	</CFIF>

	<!--- if match is chosen and type is Person. --->
	<CFIF isDefined("frmMatchData") AND dedupeEntityType eq "Person">
	<cf_translate>
		<cfif isDefined("mergedMatches")>
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>Result</TH>
					<TH>Kept PersonID</TH>
					<TH>Kept Person Name</TH>
					<TH>Deleted PersonID</TH>
					<TH>Deleted Person Name</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(mergedMatches)#" index="i">
					<cfoutput>
					<tr>
						<td>#mergedMatches[i].result#</td>
						<td>#mergedMatches[i].kept.PersonID#</td>
						<td>#mergedMatches[i].kept.FirstName# #mergedMatches[i].kept.LastName#</td>
						<td>#mergedMatches[i].deleted.PersonID#</td>
						<td>#mergedMatches[i].deleted.FirstName# #mergedMatches[i].deleted.LastName#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0 and not isDefined("notDupeMatches")>
			<p>Next matches to merge.</p>
			</cfif>
		</cfif>
		<cfif isDefined("notDupeMatches")>
			<p>Matches set as not duplicates.</p>
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>PersonID</TH>
					<TH>Person Name</TH>
					<TH>Email</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(notDupeMatches)#" index="i">
					<cfif i mod 2 eq 0>
						<cfset className="evenRow">
					<cfelse>
						<cfset className="oddRow">
					</cfif>
					<cfoutput>
					<tr class="#className#">
						<td>#notDupeMatches[i].kept.PersonID#</td>
						<td>#notDupeMatches[i].kept.FirstName# #notDupeMatches[i].kept.LastName#</td>
						<td>#notDupeMatches[i].kept.Email#</td>
					</tr>
					<tr class="#className#">
						<td>#notDupeMatches[i].deleted.PersonID#</td>
						<td>#notDupeMatches[i].deleted.FirstName# #notDupeMatches[i].deleted.LastName#</td>
						<td>#notDupeMatches[i].deleted.Email#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0>
			<p>Next matches to merge.</p>
			</cfif>
		</cfif>
	<TABLE class="table table-hover table-striped massdedupe2">

		<CFOUTPUT>
			<form action="/dataTools/massdedupe2.cfm" method="post" name="dedupeForm" id="dedupeForm">
		</CFOUTPUT>

		<TR>
			<TH ALIGN="Left" VALIGN="BOTTOM">&nbsp;</TH>
			<!--- <TH ALIGN="Left" VALIGN="BOTTOM">MatchID</TH> --->
			<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_PersonID</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Person</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Email</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Location and Country</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Automatic Merge</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM" colspan="2">Manual Merge</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Not a Duplicate</TH>

			<!--- <TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_OrganisationID</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Phr_Sys_Organisation</TH>
			<TH ALIGN="Left" VALIGN="BOTTOM">Dedupe Phr_Sys_Organisation</TH> --->
		</TR>
		<cfquery name="getMatchMaximum" dbtype="query">
			select distinct lower(MatchName)
			from getAllMatches
		</cfquery>
		<cfset trueMatchAmount = getMatchMaximum.recordCount>
		<cfset lastMatch = "">
		<cfset classList = "oddrow,evenrow">
		<cfset useClass = listGetAt(classList,2)>
		<cfset keepID = "">
		<cfset passData = "">
		<cfset incr = 0>
		<cfoutput query="getAllMatches">
			<cfset matchname=replace(matchname,"�","ss","ALL")>
			<cfset autoMergeData = "">
			<cfif lastMatch neq "#matchname#">
				<cfquery name="getMatchAmount" dbtype="query">
					select ID as matchID
					from getAllMatches
					where lower(matchName) = '#trim(lcase(matchName))#'
				</cfquery>
				<cfset matchAmount = getMatchAmount.recordCount>
				<cfset keepID = ID>
				<cfif useClass eq listGetAt(classList,1)>
					<cfset useClass = listGetAt(classList,2)>
				<cfelse>
					<cfset useClass = listGetAt(classList,1)>
				</cfif>
				<cfset lastMatch = "#matchname#">
				<cfloop query="getMatchAmount">
					<cfif matchID neq keepID>
						<cfif autoMergeData eq "">
							<cfset autoMergeData = keepID&"|"&matchID>
						<cfelse>
							<cfset autoMergeData = autoMergeData&","&keepID&"|"&matchID>
						</cfif>
					</cfif>
				</cfloop>
			</cfif>
			<tr class="#useClass#">
				<td>#htmleditformat(action)#</td>
				<!--- <td>#dedupeMatchID#</td> --->
				<td>#htmleditformat(ID)#</td>
				<td><a href="javascript:void(newWindow = openWin('/data/dataFrame.cfm?frmSrchPersonID=#ID#','#ID#','toolbar=no,titlebar=no,resizable=yes,status=no,menubar=no,fullscreen=no,scrollbars=yes'));">#htmleditformat(FirstName)# #htmleditformat(LastName)#</a></td>
				<td>#htmleditformat(email)#</td>
				<td>#htmleditformat(country)#</td>

				<cfif keepID eq ID>
					<cfset incr = incr+1>
					<cfset radioName = "frmMergeType#incr#">
					<cfset buttonName = "frmDoMerge#incr#">
					<cfset checkBoxName="frmIncInMerge#incr#">
					<td align="center" rowspan="#matchAmount#">A<CF_INPUT name="#radioName#" type="radio" value="#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);" checked></td>
				</cfif>
					<!--- NJH 2006/08/24 include checkboxes to allow a selection for a manual merge --->
					<cfset checkBoxName="frmIncInMerge#incr#">
					<td><CF_INPUT name="#checkBoxName#" type="checkbox" value="#ID#" checked disabled></td>
				<cfif keepID eq ID>
					<td align="center" rowspan="#matchAmount#">M<input name="#radioName#" type="radio" value="#valueList(getMatchAmount.matchID)#" onClick="#buttonName#.disabled = false;<cfif matchAmount gt 2>disableCheckBoxes('#htmleditformat(checkBoxName)#',false);</cfif>autoMergeEnableDisable(#htmleditformat(trueMatchAmount)#);"><CF_INPUT name="#buttonName#" type="button" value="Merge" disabled onClick="doManualMergePerson('#checkBoxName#',#incr#,#trueMatchAmount#,'#countryID#');"></td> <!--- onClick="doManualMergePerson('#radioName#',#incr#,#trueMatchAmount#,'#countryID#');" --->
					<td align="center" rowspan="#matchAmount#">N<CF_INPUT name="#radioName#" type="radio" value="N~#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);"></td>
				</cfif>
			</tr>

		</cfoutput>
		<tr>
			<td colspan="9" align="right"><input type="button" value="Merge Matches." onClick="this.value = 'Please wait.  This can take several minutes...'; this.disabled = true; document.dedupeForm.submit()" name="autoMerge"></td>
		</tr>
		<cfoutput>
			<CF_INPUT type="hidden" name="frmMatchAmount" value="#trueMatchAmount#">
			<CF_INPUT type="hidden" name="frmMatchData" value="#frmMatchData#">
			<input type="hidden" name="frmRunMatches" value="true">
		</cfoutput>
		</form>
	</table>
	</cf_translate>
	</cfif>

	<CFIF isDefined("frmMatchData") AND dedupeEntityType eq "Organisation">
		<cfif isDefined("mergedMatches")>
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>Result</TH>
					<TH>Kept OrganisationID</TH>
					<TH>Kept Organisation Name</TH>
					<TH>Deleted OrganisationID</TH>
					<TH>Deleted Organisation Name</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(mergedMatches)#" index="i">
					<cfoutput>
					<tr>
						<td>#mergedMatches[i].result#</td>
						<td>#mergedMatches[i].kept.OrganisationID#</td>
						<td>#mergedMatches[i].kept.OrganisationName#</td>
						<td>#mergedMatches[i].deleted.OrganisationID#</td>
						<td>#mergedMatches[i].deleted.OrganisationName#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0 and not isDefined("notDupeMatches")>
			<p>Next matches to merge.</p>
			</cfif>
		</cfif>
		<cfif isDefined("notDupeMatches")>
			<p>Matches set as not duplicates.</p>
			<!--- <cfdump var="#notDupeMatches#"> --->
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>OrganisationID</TH>
					<TH>Organisation Name</TH>
					<TH>Match Name</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(notDupeMatches)#" index="i">
					<cfif i mod 2 eq 0>
						<cfset className="evenRow">
					<cfelse>
						<cfset className="oddRow">
					</cfif>
					<cfoutput>
					<tr class="#className#">
						<td>#notDupeMatches[i].kept.OrganisationID#</td>
						<td>#notDupeMatches[i].kept.OrganisationName#</td>
						<td>#notDupeMatches[i].kept.MatchName#</td>
					</tr>
					<tr class="#className#">
						<td>#notDupeMatches[i].deleted.OrganisationID#</td>
						<td>#notDupeMatches[i].deleted.OrganisationName#</td>
						<td>#notDupeMatches[i].deleted.MatchName#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0>
			<br><br>Next matches to merge.
			</cfif>
		</cfif>
		<TABLE class="table table-hover table-striped massdedupe2">
		<cfif getAllMatches.recordCount eq 0>
			<TR><TD>There are no more matches for this type. Please return to the selection.</TD></TR>
		<cfelse>

			<CFOUTPUT>
				<form action="/dataTools/massdedupe2.cfm" method="post" name="dedupeForm" id="dedupeForm">
			</CFOUTPUT>
				<TR>
					<TH>Action</TH>
					<TH>OrganisationID</TH>
					<TH>Organisation Name</TH>
					<TH>Match Name</TH>
					<TH>Country</TH>
					<TH>Automatic Merge</TH>
					<TH colspan="2">Manual Merge</TH>
					<TH>Not a Duplicate</TH>
					<!--- <TH>Leave For Now</TH> --->

				</TR>
				<!--- get the amount of actual matches displayed per page, based on the action 'keep'. --->
				<cfquery name="getMatchMaximum" dbtype="query">
					select distinct lower(MatchName)
					from getAllMatches
				</cfquery>
				<cfset trueMatchAmount = getMatchMaximum.recordCount>
				<cfset lastMatch = "">
				<cfset matchAmount = 0>
				<cfset classList = "oddrow,evenrow">
				<cfset useClass = listGetAt(classList,2)>
				<cfset keepID = "">
				<cfset incr = 0>
				<CFOUTPUT query="getAllMatches">
					<cfset autoMergeData = "">
					<cfif lastMatch neq "#matchName#">
						<cfquery name="getMatchAmount" dbtype="query">
							select ID as matchID
							from getAllMatches
							where lower(matchName) = '#trim(lcase(matchName))#'
						</cfquery>
						<cfset matchAmount = getMatchAmount.recordCount>
						<cfset keepID = ID>
						<cfif useClass eq listGetAt(classList,1)>
							<cfset useClass = listGetAt(classList,2)>
						<cfelse>
							<cfset useClass = listGetAt(classList,1)>
						</cfif>
						<cfset lastMatch = "#matchName#">

						<cfloop query="getMatchAmount">
							<cfif matchID neq keepID>
								<cfif autoMergeData eq "">
									<cfset autoMergeData = keepID&"|"&matchID>
								<cfelse>
									<cfset autoMergeData = autoMergeData&","&keepID&"|"&matchID>
								</cfif>
							</cfif>
						</cfloop>
					</cfif>
					<tr class="#useClass#">
						<td>#htmleditformat(action)#</td>
						<td>#htmleditformat(ID)#</td>
						<td><a href="javascript:void(newWindow = openWin('/data/dataFrame.cfm?frmSrchOrgID=#ID#','#ID#','toolbar=no,titlebar=no,resizable=yes,status=no,menubar=no,fullscreen=no,scrollbars=yes'));">#htmleditformat(OrganisationName)#</a></td>
						<td>#htmleditformat(MatchName)#</td>
						<td>#htmleditformat(CountryDescription)#</td>
						<cfif keepID eq ID>
							<cfset incr = incr+1>
							<cfset radioName = "frmMergeType#incr#">
							<cfset buttonName = "frmDoMerge#incr#">
							<cfset checkBoxName="frmIncInMerge#incr#">
							<td align="center" rowspan="#matchAmount#">A<CF_INPUT name="#radioName#" type="radio" value="#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);" checked></td>
						</cfif>
						<!--- NJH 2006/08/24 include checkboxes to allow a selection for a manual merge --->
						<cfset checkBoxName="frmIncInMerge#incr#">
						<td><CF_INPUT name="#checkBoxName#" type="checkbox" value="#ID#" checked disabled></td>
						<cfif keepID eq ID>
							<td align="center" rowspan="#matchAmount#">M<input name="#radioName#" type="radio" value="#valueList(getMatchAmount.matchID,"|")#" onClick="#buttonName#.disabled = false;<cfif matchAmount gt 2>disableCheckBoxes('#htmleditformat(checkBoxName)#',false);</cfif>autoMergeEnableDisable(#htmleditformat(trueMatchAmount)#);"><CF_INPUT name="#buttonName#" type="button" value="Merge" disabled onClick="doManualMerge('#checkBoxName#',#incr#,'organisation',#trueMatchAmount#)"></td>
							<td align="center" rowspan="#matchAmount#">N<CF_INPUT name="#radioName#" type="radio" value="N~#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);"></td>
						</cfif>
					</tr>

				</cfoutput>
				<tr>
					<td colspan="9" align="right"><input type="button" value="Merge Matches." onClick="this.value = 'Please wait.  This can take several minutes...'; this.disabled = true; document.dedupeForm.submit()" name="autoMerge"></td>
				</tr>
				<cfoutput>
					<CF_INPUT type="hidden" name="frmMatchAmount" value="#trueMatchAmount#">
					<CF_INPUT type="hidden" name="frmMatchData" value="#frmMatchData#">
					<input type="hidden" name="frmRunMatches" value="true">
				</cfoutput>

			</FORM>
		</cfif>
		</table>
	</cfif>

	<CFIF isDefined("frmMatchData") AND dedupeEntityType eq "Location">
		<cfif isDefined("mergedMatches")>
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>Result</TH>
					<TH>Kept LocationID</TH>
					<TH>Kept Location Address</TH>
					<TH>Deleted LocationID</TH>
					<TH>Deleted Location Name</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(mergedMatches)#" index="i">
					<cfoutput>
					<tr>
						<td>#mergedMatches[i].result#</td>
						<td>#mergedMatches[i].kept.LocationID#</td>
						<td>#mergedMatches[i].kept.LocationAddress#</td>
						<td>#mergedMatches[i].deleted.LocationID#</td>
						<td>#mergedMatches[i].deleted.LocationAddress#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0 and not isDefined("notDupeMatches")>
			<br><br>Next matches to merge.
			</cfif>
		</cfif>
		<cfif isDefined("notDupeMatches")>
			<p>Matches set as not duplicates.</p>
			<TABLE class="table table-hover table-striped massdedupe2">
				<TR>
					<TH>LocationID</TH>
					<TH>Location Address</TH>
					<TH>Match Name</TH>
				</TR>
				<cfloop from="1" to="#arrayLen(notDupeMatches)#" index="i">
					<cfif i mod 2 eq 0>
						<cfset className="evenRow">
					<cfelse>
						<cfset className="oddRow">
					</cfif>
					<cfoutput>
					<tr class="#className#">
						<td>#notDupeMatches[i].kept.LocationID#</td>
						<td>#notDupeMatches[i].kept.LocationAddress#</td>
						<td>#notDupeMatches[i].kept.MatchName#</td>
					</tr>
					<tr class="#className#">
						<td>#notDupeMatches[i].deleted.LocationID#</td>
						<td>#notDupeMatches[i].deleted.LocationAddress#</td>
						<td>#notDupeMatches[i].deleted.MatchName#</td>
					</tr>
					</cfoutput>
				</cfloop>
			</TABLE>
			<cfif getAllMatches.recordCount neq 0>
			<br><br>Next matches to merge.
			</cfif>
		</cfif>
		<TABLE class="table table-hover table-striped massdedupe2">
		<cfif getAllMatches.recordCount eq 0>
			<TR><TD>There are no more matches for this type. Please return to the selection.</TD></TR>
		<cfelse>

			<CFOUTPUT>
				<form action="/dataTools/massdedupe2.cfm" method="post" name="dedupeForm" id="dedupeForm">
			</CFOUTPUT>
				<TR>
					<TH>Action</TH>
					<TH>LocationID</TH>
					<TH>Location Address</TH>
					<TH>Match Name</TH>
					<TH>Organisation</TH>
					<TH>Country</TH>
					<TH>Automatic Merge</TH>
					<TH colspan="2">Manual Merge</TH>
					<TH>Not a Duplicate</TH>
					<!--- <TH>Leave For Now</TH> --->

				</TR>
				<!--- get the amount of actual matches displayed per page, based on the action 'keep'. --->
				<cfquery name="getMatchMaximum" dbtype="query">
					select distinct lower(MatchName)
					from getAllMatches
				</cfquery>
				<cfset trueMatchAmount = getMatchMaximum.recordCount>
				<cfset lastMatch = "">
				<cfset matchAmount = 0>
				<cfset classList = "oddrow,evenrow">
				<cfset useClass = listGetAt(classList,2)>
				<cfset keepID = "">
				<cfset incr = 0>

				<CFOUTPUT query="getAllMatches">
					<cfset autoMergeData = "">
					<cfif lastMatch neq "#matchName#">
						<cfquery name="getMatchAmount" dbtype="query">
							select ID as matchID
							from getAllMatches
							where lower(matchName) = '#trim(lcase(matchName))#'
						</cfquery>
						<cfset matchAmount = getMatchAmount.recordCount>
						<cfset keepID = ID>
						<cfif useClass eq listGetAt(classList,1)>
							<cfset useClass = listGetAt(classList,2)>
						<cfelse>
							<cfset useClass = listGetAt(classList,1)>
						</cfif>
						<cfset lastMatch = "#matchName#">

						<cfloop query="getMatchAmount">
							<cfif matchID neq keepID>
								<cfif autoMergeData eq "">
									<cfset autoMergeData = keepID&"|"&matchID>
								<cfelse>
									<cfset autoMergeData = autoMergeData&","&keepID&"|"&matchID>
								</cfif>
							</cfif>
						</cfloop>
					</cfif>
					<tr class="#useClass#">
						<td>#htmleditformat(action)#</td>
						<!--- <td>#dedupeMatchID#</td> --->
						<td>#htmleditformat(ID)#</td>
						<td>#htmleditformat(LocationAddress)#</td>
						<td>#htmleditformat(MatchName)#</td>
						<td><a href="javascript:void(newWindow = openWin('/data/dataFrame.cfm?frmSrchLocationID=#ID#','#ID#','toolbar=no,titlebar=no,resizable=yes,status=no,menubar=no,fullscreen=no,scrollbars=yes'));">#htmleditformat(organisationName)#</a></td>
						<td>#htmleditformat(CountryDescription)#</td>
						<cfif keepID eq ID>
							<cfset incr = incr+1>
							<cfset radioName = "frmMergeType#incr#">
							<cfset buttonName = "frmDoMerge#incr#">
							<cfset checkBoxName="frmIncInMerge#incr#">
							<td align="center" rowspan="#matchAmount#">A<CF_INPUT name="#radioName#" type="radio" value="#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);" checked></td>
						</cfif>
						<!--- NJH 2006/08/24 include checkboxes to allow a selection for a manual merge --->
						<cfset checkBoxName="frmIncInMerge#incr#">
						<td><CF_INPUT name="#checkBoxName#" type="checkbox" value="#ID#" checked disabled></td>
						<cfif keepID eq ID>
							<td align="center" rowspan="#matchAmount#">M<input name="#radioName#" type="radio" value="#valueList(getMatchAmount.matchID)#" onClick="#buttonName#.disabled = false;<cfif matchAmount gt 2>disableCheckBoxes('#htmleditformat(checkBoxName)#',false);</cfif>autoMergeEnableDisable(#htmleditformat(trueMatchAmount)#);"><CF_INPUT name="#buttonName#" type="button" value="Merge" disabled onClick="doManualMerge('#checkBoxName#',#incr#,'location',#trueMatchAmount#)"></td>
							<td align="center" rowspan="#matchAmount#">N<CF_INPUT name="#radioName#" type="radio" value="N~#autoMergeData#" onClick="#buttonName#.disabled = true;disableCheckBoxes('#checkBoxName#',true);autoMergeEnableDisable(#trueMatchAmount#);"></td>
						</cfif>
					</tr>
				</cfoutput>
				<tr>
					<td colspan="10" align="right"><input type="button" value="Merge Matches." onClick="this.value = 'Please wait.  This can take several minutes...'; this.disabled = true; document.dedupeForm.submit()" name="autoMerge"></td>
				</tr>
				<cfoutput>
					<CF_INPUT type="hidden" name="frmMatchAmount" value="#trueMatchAmount#">
					<CF_INPUT type="hidden" name="frmMatchData" value="#frmMatchData#">
					<input type="hidden" name="frmRunMatches" value="true">
				</cfoutput>

			</FORM>

		</cfif>
		</table>
	</cfif>
</CFIF>