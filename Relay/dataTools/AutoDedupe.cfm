<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			autoDedupe.cfm	
Author:				
Date started:			/xx/07
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
05-FEB-2008			NJH			Using dedupeTypeID rather than dedupeViewType
15-Oct-2008			NJH			CR-TND562 if we're running dedupe as a scheduled task, then log the results
25-Jun-2009			NJH			Bug Fix Sony1 Support Issue 2387. Increase timeout from 10 minutes to 20
2011/01/25			WAB			Mods to record errors to table and not lock up
2011/03/09			WAB			LID 5849 Added some debug
2013-09-19 			NYB 		Case 436820 rearranged and added code to make it possible to just run for a specific countryid and dedupeMatchMethod passed via the url
								replaced siteDomain with cgi.SERVER_NAME as this was causing an error
2013-11-26			WAB			Case 438202 - added a maxNumberToProcess parameter
								Fixed bug in Case 436820
2014-02-06 			PPB 		Case 438810 removed <br>'s from result message to avoid error 
 
Possible enhancements:


 --->

<!--- 10 minute timeout JIC --->
<!--- NJH 2009/06/25 Bug Fix Sony1 Support Issue 2387. Increase timeout from 10 minutes to 20 --->
<cfsetting enablecfoutputonly="No" requesttimeout="1200">
<cfset defaultScheduleResult = {isOK=true,message=""}>
<cfparam name="scheduleResult" default=#defaultScheduleResult#>
<cfparam name="maxNumberToProcess" default=5 type="numeric">


<!--- 
Check if this page is already running (by automated process perhaps)
to avoid deduping the same records.

This variable is set to stopped at the end of the page. In the event of the page falling over this may not get set to stopped.
Call with reset to clear.
 --->
<cfif structkeyexists(url,"reset")>
	<cfif structkeyexists(application,"AutoDedupeState")>
		<cfset temp = structdelete(application,"AutoDedupeState")>
	</cfif>
	Reset
	<CF_ABORT>
</cfif>

<cfif structkeyexists(application,"AutoDedupeState")>
	<cfif application.AutoDedupeState eq "running">
		Already running
		<cfmail to="errors@foundation-network.com,relayhelp@foundation-network.com" subject="AutoDedupe Needs Reseting on #request.currentSite.domainAndRoot#" from="errors@foundation-network.com">
			To do this go to #request.currentSite.protocolAndDomain#/scheduled/scheduledAutoDedupe.cfm?reset once you have investigated why it failed.
		</cfmail>
		<CF_ABORT>
	<!--- NJH 2008/02/15 if we're jammed, don't keep running. --->
	<cfelseif application.AutoDedupeState eq "jammed">
		Jammed
		<CF_ABORT>
	</cfif>
	</cfif>

<!--- NJH 2008/02/15 A basic check that autoDedupe is not jammed and is running smoothly --->
<cfquery name="getMergesCreatedOverAWeekAgo" datasource="#application.siteDataSource#">
	select count(*) as countMerges from dedupeMatches dm inner join
		dedupeMatchLog dml on dm.dedupeMatchLogID = dml.dedupeMatchLogID inner join
        dedupeMatchMethod dmm on dml.dedupeMatchMethodID = dmm.dedupeMatchMethodID
	where dm.created > getDate()-7 and
		dmm.dedupeTypeID in (2,3) and
		dm.mergeState = 'T'
</cfquery>


<cfif getMergesCreatedOverAWeekAgo.countMerges gt 0>
	<cfset application.AutoDedupeState = "jammed">
	<cfmail to="errors@foundation-network.com,relayhelp@foundation-network.com" subject="#cgi.SERVER_NAME# AutoDedupe Jammed" from="errors@foundation-network.com">
		It is possible that autoDedupe is jammed. The process has temporarily stopped. Please investigate and go to #request.currentSite.protocolAndDomain#/scheduled/scheduledAutoDedupe.cfm?reset after your investigation to restart autoDedupe.
	</cfmail>
</cfif>

<!--- START: 2013-09-19 NYB Case 436820 - moved from below--->
<cfparam name="countryID" default="">
<cfparam name="dedupematchmethodID" default="">
<cfparam name="dedupeEntityType" default="">
<cfparam name="dedupeViewType" default="">
<cfparam name="dedupeTypeID" default=""> <!--- NJH 2008/02/05 --->
<!--- END: 2013-09-19 NYB Case 436820 --->

<!--- get the method with the minimum execOrder as default method --->
<!--- NJH 2008/02/05 inner join to dedupeViewType table --->
<cfquery name="getDefaultMatchMethod" datasource="#application.SiteDataSource#">
	select top 1 name as dedupeViewType, dedupeEntityType, dedupeMatchMethodID, dedupeExecOrder, mm.dedupeTypeID
	from 	dedupeMatchMethod mm inner join dedupeType t
		on mm.dedupeTypeID = t.dedupeTypeID
	where 	active <> 0
	<!--- START: 2013-09-19 NYB Case 436820 - added --->
	<cfif dedupematchmethodID neq "">
		and dedupematchmethodID =  <cf_queryparam value="#dedupematchmethodID#" CFSQLTYPE="cf_sql_integer" >
	</cfif>
	<cfif dedupeEntityType neq "">
		and dedupeEntityType =  <cf_queryparam value="#dedupeEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfif>
	<cfif dedupeViewType neq "">
		and dedupeViewType =  <cf_queryparam value="#dedupeViewType#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfif>
	<cfif dedupeTypeID neq "">
		and mm.dedupeTypeID=#dedupeTypeID#
	</cfif>
	<!--- END: 2013-09-19 NYB Case 436820 --->
	order by dedupeExecOrder asc
</cfquery>

<!--- if no default match method, skip it all! --->
<cfif getDefaultMatchMethod.recordcount gt 0>
	<cftry>
		<cfset application.AutoDedupeState = "running">
		<!--- 2013-09-19 NYB Case 436820 - removed ---
		<cfscript>
			CountryIDs = application.com.dedupe.GetOrganisationMatchCountryIDs();
		</cfscript>
		--- 2013-09-19 NYB Case 436820 --->
		
		<!--- get the last finished match --->
		<cfquery name="getLastCompletedCountryID" datasource="#application.sitedatasource#">
			SELECT     	TOP 1 dal.countryid,dal.matchmethodID, MAX(dal.created) AS MostRecentlyAutoDupedDate,dmm.dedupeExecOrder
			FROM        dedupeAutomationLog dal inner join
						dedupeMatchMethod dmm on dmm.dedupeMatchMethodID = dal.matchMethodID
			WHERE     	(dedupeQuantity = 0)
						<!--- START: 2013-09-19 NYB Case 436820 - added --->
						<cfif countryid neq "">
							and dal.countryid=#countryid#
						</cfif>
						<cfif dedupematchmethodID neq "">
							and dedupematchmethodID =  <cf_queryparam value="#dedupematchmethodID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>
						<cfif dedupeEntityType neq "">
							and dedupeEntityType =  <cf_queryparam value="#dedupeEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
						<cfif dedupeTypeID neq "">
							and dedupeTypeID =  <cf_queryparam value="#dedupeTypeID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>			
						<!--- END: 2013-09-19 NYB Case 436820 --->
			GROUP BY 	dal.countryid,dal.matchmethodID,dmm.dedupeExecOrder
			ORDER BY 	MAX(dal.created) DESC
		</cfquery>	
		
		<!--- START: 2013-09-19 NYB Case 436820 - moved from below --->
		<!--- default to the method with min execOrder --->
		<cfset CountryIDs = application.com.dedupe.GetOrganisationMatchCountryIDs()>
		<!--- END: 2013-09-19 NYB Case 436820 --->
		<!--- globally scoped system - remember to remove countryid from the joins in the dedupeMatchMethod Table --->
		<!--- <cfif (isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 0)> --->
		<cfif not application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
			<!--- the only country we want to operate in is ''all countries''! --->
			<cfset CountryIDs = 37>
		</cfif>
		
		<!--- if there is a completed match --->
		<cfif getLastCompletedCountryID.countryid neq "">
			<!--- move on to the next match method and possibly country --->
			
			<!--- NJH 2006/08/24 get the next Match method information --->
			<!--- nm changes to increment based on active fields and not just +1 --->
			<!--- NJH 2008/02/05 inner join to dedupeViewType table --->
			<cfquery name="getNextMatchMethod" datasource="#application.SiteDataSource#">
				select top 1 name as dedupeViewType, dedupeEntityType, dedupeMatchMethodID, dedupeExecOrder, mm.dedupeTypeID
				from 	dedupeMatchMethod mm inner join dedupeType t
					on mm.dedupeTypeID = t.dedupeTypeID
				where 	dedupeExecOrder >  <cf_queryparam value="#getLastCompletedCountryID.dedupeExecOrder#" CFSQLTYPE="CF_SQL_Integer" > 
				and active <> 0
						<!--- START: 2013-09-19 NYB Case 436820 - added --->
						<cfif dedupematchmethodID neq "">
							and dedupematchmethodID =  <cf_queryparam value="#dedupematchmethodID#" CFSQLTYPE="cf_sql_integer" >
						</cfif>
						<cfif dedupeEntityType neq "">
							and dedupeEntityType =  <cf_queryparam value="#dedupeEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>

						<cfif dedupeTypeID neq "">
							and mm.dedupeTypeID=#dedupeTypeID#
						</cfif>	
						<!--- END: 2013-09-19 NYB Case 436820 --->
				order by dedupeExecOrder asc
			</cfquery>
			
			<!--- 2013-09-19 NYB Case 436820 - moved here from inside cfelse: --->
			<cfset changeCountryID = "false">
			<cfif getNextMatchMethod.recordCount eq 0>
				<!--- START: 2013-09-19 NYB Case 436820 - added if --->
				<cfif countryid eq "">
				<cfset changeCountryID = "true">
				</cfif>
				<!--- END: 2013-09-19 NYB Case 436820 --->
				<cfset dedupeMatchMethodID = getDefaultMatchMethod.dedupeMatchMethodID>
				<cfset dedupeEntityType = getDefaultMatchMethod.dedupeEntityType>
				<cfset dedupeViewType = getDefaultMatchMethod.dedupeViewType>
				<cfset dedupeTypeID = getDefaultMatchMethod.dedupeTypeID> <!--- NJH 2008/02/05 --->
			<cfelse>
				<cfset dedupeMatchMethodID = #getNextMatchMethod.dedupeMatchMethodID#>
				<cfset dedupeEntityType = #getNextMatchMethod.dedupeEntityType#>
				<cfset dedupeViewType = #getNextMatchMethod.dedupeViewType#>
				<cfset dedupeTypeID = getNextMatchMethod.dedupeTypeID> <!--- NJH 2008/02/05 --->
			</cfif>
			
			<!--- if global never change country --->
			<!--- <cfif (isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 0)> --->
			<cfif not application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
				<cfset changeCountryID = "false">
				<cfset getLastCompletedCountryID.countryid = 37>
			</cfif>
				
				
			<cfif changeCountryID>
				<!--- and the last completed country is in the list --->
				<cfif listfind(CountryIDs,getLastCompletedCountryID.countryid,",") gt 0 and listfind(CountryIDs,getLastCompletedCountryID.countryid,",") lt listlen(CountryIDs,",")>
					<cfset tokenpos = listfind(CountryIDs,getLastCompletedCountryID.countryid,",") + 1>
					<cfset countryID = gettoken(CountryIDs,tokenpos,",")>
				<cfelse>		
					<!--- hunt for it by incrementing --->
					<!--- if we can't go up skip it and start again with the default --->
					<cfif getLastCompletedCountryID.countryid lt listlast(CountryIDs)>
						<cfloop index="countryIX" from="#getLastCompletedCountryID.countryid#" to="#listlast(CountryIDs)#" step="1">
							<cfif listfind(CountryIDs,countryIX,",") gt 0>
								<cfset countryID = countryIX>
							<cfbreak>
							</cfif>
						</cfloop>
					</cfif>
				</cfif>
			<cfelse>
				<!--- START: 2013-09-19 NYB Case 436820 - added IF --->
				<cfif countryid eq "">
					<cfset countryID = getLastCompletedCountryID.countryid>
				</cfif>
				<!--- END: 2013-09-19 NYB Case 436820 --->
			</cfif>
		</cfif>


		
		<!--- START: 2013-09-19 NYB Case 436820 - moved above ---
		<!--- default to the method with min execOrder --->
		<cfparam name="countryID" default="#listfirst(CountryIDs,",")#">
		<cfparam name="dedupematchmethodID" default="#getDefaultMatchMethod.dedupeMatchMethodID#">
		<cfparam name="dedupeEntityType" default="#getDefaultMatchMethod.dedupeEntityType#">
		<cfparam name="dedupeViewType" default="#getDefaultMatchMethod.dedupeViewType#">
		<cfparam name="dedupeTypeID" default="#getDefaultMatchMethod.dedupeTypeID#"> <!--- NJH 2008/02/05 --->
		!--- END: 2013-09-19 NYB Case 436820 - moved above --->
		
		<!--- START: 2013-09-19 NYB Case 436820 - added --->
		<cfif countryid eq "">
			<cfset countryid = listfirst(CountryIDs,",")>
		</cfif>
		<!--- END: 2013-09-19 NYB Case 436820 - added --->
		<!--- WAB 2013-11-26 fix bug in 436820, needed to check that all these aren't blank--->
		<cfif dedupematchmethodID is ""><cfset dedupematchmethodID = getDefaultMatchMethod.dedupeMatchMethodID></cfif>
		<cfif dedupeEntityType is ""><cfset dedupeEntityType = getDefaultMatchMethod.dedupeEntityType></cfif>
		<cfif dedupeViewType is ""><cfset dedupeViewType = getDefaultMatchMethod.dedupeViewType></cfif>
		<cfif dedupeTypeID is ""><cfset dedupeTypeID = getDefaultMatchMethod.dedupeTypeID></cfif>

				
		<!--- get the last successful merge search in this set --->
		<cfquery name="getLastMergeDate" datasource="#application.sitedatasource#">
			SELECT     MAX(dedupeMatches.created) AS lastMergeDate,max(dedupeAutomationLog.created) as lastRunDate
			FROM         dedupeMatches INNER JOIN
						  dedupeMatchLog ON dedupeMatches.dedupeMatchLogID = dedupeMatchLog.dedupeMatchLogID INNER JOIN
						  dedupeAutomationLog ON dedupeMatchLog.dedupeMatchMethodID = dedupeAutomationLog.matchmethodid
			WHERE     (dedupeAutomationLog.matchmethodid =  <cf_queryparam value="#dedupeMatchMethodID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (dedupeAutomationLog.countryid =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>
		
		<!--- work out the time elapsed since last dupe find for this set --->
		<cfif not isdate(getLastMergeDate.lastMergeDate)>
			<cfset dateDiffMins = 9999999>
		<cfelse>
			<cfif getLastMergeDate.lastMergeDate gte getLastMergeDate.lastRunDate>
				<cfset recentdate = getLastMergeDate.lastMergeDate>
			<cfelse>
				<cfset recentdate = getLastMergeDate.lastRunDate>
			</cfif>
			<cfset dateDiffMins = datediff("n", recentdate, now())>
		</cfif>
		<!--- debug --->
		<!--- NJH 2008/10/15 CR-TND562 if we're running dedupe as a scheduled task, then log what's been done. --->
		<cfif structkeyexists(url,"debug") or (isDefined("runAsScheduleTask") and runAsScheduleTask eq 1)>
			<cfset messageHTML = "countryID: #countryID# - dedupematchmethodID: #dedupeMatchMethodID#">				<!--- 2014-02-06 PPB Case 438810 removed <br>'s  --->
			
			<cfoutput>#messageHTML#
			<!--- countryID: #countryID#<br>
			dedupematchmethodID: #dedupeMatchMethodID#<br> --->
			</cfoutput>
			<cfset scheduleResult.dedupeResult = messageHTML>
			<cfset application.AutoDedupeState = "stopped">
			<cfset scheduleResult.countryid = countryid>
			<cfset scheduleResult.dedupeMatchMethodID = dedupeMatchMethodID>
		</cfif>
		<cfscript>
			dedupeFinished = 0;
			deDupedAmount = 0;
			
			if (dedupeTypeID neq 3) { // if we're not doing a mergeOnly dedupe, then get new matches
			//if (dedupeViewType neq "MergeOnly") {
			//P_SNY039 GCC 2006/06/19
			//RMB - 2013-02-21 - 433866 - Missing 'Plo.' from getSetting --->
			if (listFindNoCase(application.com.settings.getSetting("Plo.mergeAndDelete.autodedupe.ExcludedCountryIDlist"),countryID) neq 0) {
				dedupeFinished=1;
			} else {
				// get next 5 matches
				getAllMatches = application.com.dedupe.getMatchesForDisplay(tableName = dedupeEntityType,duplicateType = 'D',countryID = countryID,MatchMethodID = dedupeMatchMethodID, maxNumberToProcess = maxNumberToProcess);
					// if no matches ever or no matches merged for at least 24 hours (definite) or 7 days (possible) run a fresh match
					if ((getAllMatches.recordcount eq 0) or ((dedupeTypeID eq 2 and dateDiffMins gt 1440) or (dedupeTypeID neq 2 and dateDiffMins gt 10080))) { 
					//if ((getAllMatches.recordcount eq 0) or ((dedupeViewType eq "Definate" and dateDiffMins gt 1440) or (dedupeViewType neq "Definate" and dateDiffMins gt 10080))) {
						//if (isDefined("request .country ScopeOrganisationRecords") and request .country ScopeOrganisationRecords eq 0) {
						if (not application.com.settings.getSetting("plo.countryScopeOrganisationRecords")) {
							newMatches = application.com.dedupe.dedupeRecordsFromView(dedupeMatchMethodID);
						} else {
					newMatches = application.com.dedupe.dedupeRecordsFromView(dedupeMatchMethodID, countryID);
						}
					getAllMatchesAgain = application.com.dedupe.getMatchesForDisplay(dedupeEntityType,'D',countryID,dedupeMatchMethodID);
					if (getAllMatchesAgain.recordcount eq 0) {
						dedupeFinished = 1;
					}
				}
			}
			}
			// NJH  2006/08/24  autoDeDupe only if the dedupe matches are definite (ie Definite and MergeOnly.. They aren't 'Possible')
			if ((dedupeFinished neq 1) and (dedupeTypeID neq 1)) {
			//if ((dedupeFinished neq 1) and (dedupeViewType neq "Possible")) {
				//there are matches - dedupe - return of 0 means all done
				deDupedAmount = application.com.dedupe.AutoDeDupe (tableName = dedupeEntityType,duplicateType = 'D',countryID = countryID,MatchMethodID = dedupeMatchMethodID,maxNumberToProcess = maxNumberToProcess);
				scheduleResult.numberDeduped = deDupedAmount;
			} 
			//log date time against match combo - 0 quantity will trigger new method / country on next run of this page
			x = application.com.dedupe.AutoDeDupeLog (dedupeEntityType,countryID,dedupeMatchMethodID,deDupedAmount);
		 </cfscript>
			 
		 <cfset application.AutoDedupeState = "stopped">
		 <cfcatch type="Any">
			<cfset scheduleResult.isok= false>
			<cfset scheduleResult.error = cfcatch.message>
			<!--- WAB 2011/01/26 decided that in event of error should:
				a) record a warning
				b) record that this mactch method had been done, so that next match method will run next time
				c) set application.AutoDedupeState to  stopped so that whole process does not lock up 
			 --->
			<cfset application.com.errorHandler.recordRelayError_Warning (severity="Error",Type="AutoDedupeError",message="#cfcatch.message#",caughtError = cfcatch,warningstructure = scheduleResult)>
			<cfset application.com.dedupe.AutoDeDupeLog (dedupeEntityType,countryID,dedupeMatchMethodID,0)>
			<cfset application.AutoDedupeState = "stopped">

			<!--- 
			<cfmail to="errors@foundation-network.com" subject="#cgi.SERVER_NAME# AutoDedupe Error" from="errors@foundation-network.com">
				#cfcatch.message#
				#cfcatch.detail#
				#cfcatch.type#
			</cfmail>
			--->

		 </cfcatch>
	 </cftry>
<cfelse>
	No Match Methods Available. Cannot Dedupe!
	<!--- START: 2013-09-19 NYB Case 436820 - replaced siteDomain with cgi.SERVER_NAME --->
	<cfmail to="errors@foundation-network.com,relayhelp@foundation-network.com" subject="AutoDedupe Has No Match Mathods to use on #cgi.SERVER_NAME#" from="errors@foundation-network.com">
		There are no active match methods available for deduping on #cgi.SERVER_NAME#.
	<!--- END: 2013-09-19 NYB Case 436820 --->
	</cfmail>
</cfif>
