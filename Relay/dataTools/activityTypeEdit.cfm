<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Name: ActivityTypeEdit.cfm

Author: NJH

Date: 2013-10-16

 --->

<cf_ascreenupdate>

<cfparam name="frmAddNew" default= "0">

<!--- <cfset frmmethod= "edit"> --->

<cfquery name="getActivityTypes">
	select isnull(description,activityType) as displayvalue, activityTypeID as datavalue
	from activityType
	order by activityType
</cfquery>

<cfparam name="frmActivitytypeID" default="#getActivityTypes.datavalue#">

<cfquery name="ActivityType">
	select activityTypeID,activityType,description
			,case when lastupdated is null then getdate() else lastupdated end as lastupdated 
	from activityType where activityTypeID =  <cf_queryparam value="#frmActivitytypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  
</cfquery>

<cf_head>
	<cf_title>Activity Type Edit</cf_title>

	<script>
	function move(index) {
	form= document.mainForm
	currentSelected = form.frmActivityTypeID.selectedIndex
	thisLength = form.frmActivityTypeID.length
		// check that not past begining or end of the list (note that item one isn't allowed)
		if (currentSelected + index > -1 && currentSelected + index < thisLength) {
			form.frmActivityTypeID.selectedIndex	= currentSelected + index
			submitActivityTypeEditForm();
		} else {
			alert('Sorry, you\'ve reached the end!')
		}
		
	}
	
	function submitActivityTypeEditForm() {
		msg = verifyInput()
		
		if (msg != '') {
			alert(msg)
		} else {
			document.mainForm.submit()	
		}
	}
	</script>

</cf_head>

<cfset application.com.request.setTophead(pageTitle="Edit ActivityType Data")>

<cfparam name="message" default="">

<cfif message neq "">
	<cfoutput>
		#application.com.relayui.message(message=message,type='success')#
	</cfoutput>
</cfif>

<cfform name="mainForm" method="post">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	
		
		<tr>
			<td colspan="2" class="screenLabel"><p class="noBottomMargin"><b>Choose activity type to edit</b></p></td>
		</tr>
		<tr>
			<td class="screenLabel">Activity Type</td>
			<td>
				<cf_displayValidValues 
					validValues = "#getActivityTypes#"
					formfieldname = "frmActivityTypeID"
					currentvalue = "#frmActivitytypeID#"
					onchange = "javascript:submitActivityTypeEditForm()" 
					showNull = "false"
				>	
			</td>
		</tr>
		<cf_ascreen formname = "mainForm">
			<cf_ascreenitem screenid="activityTypeEdit" activityType="#ActivityType#" method="edit">
		</cf_ascreen>
		
		<cfoutput><cfinput type="hidden" name="country_LastUpdated_#frmActivitytypeID#" value = "#ActivityType.lastupdated#"></cfoutput>
</table>
</cfform>