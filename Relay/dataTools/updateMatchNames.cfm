<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
NYB 	2009-05-12 added Location dedupe code
NJH  2016/09/01 JIRA PROD2016-1190 - update matching methods
--->



<cf_head>
	<cf_title>Update Match Names</cf_title>
</cf_head>

<!--- 2013-12-18 NYB Case 436891 - removed
<cfparam name="form.updateOrgMatchNames" default="no">
 --->

<cfparam name="message" default="#ArrayNew(1)#" type="array">

<cfif structkeyexists(form,"updateOrgMatchNames")>
	<cfset application.com.matching.updateMatchFields(tablename="organisation",entityType="organisation")>
	<cfset arrayAppend(message,"Organisation match names updated in Relay organisation table")>
</cfif>
<cfif structkeyexists(form,"updateLocMatchNames")>
	<cfset application.com.matching.updateMatchFields(tablename="location",entityType="location")>
	<cfset arrayAppend(message,"Location match names updated in Relay location table")>
</cfif>
<cfif structkeyexists(form,"updatePerMatchNames")>
	<cfset application.com.matching.updateMatchFields(tablename="location",entityType="location")>
	<cfset arrayAppend(message,"Person match names updated in Relay person table")>
</cfif>

<cfif not ArrayIsEmpty(message)>
<div class="successblock">
	<cfoutput>
		<cfloop index="i" from="1" to=#ArrayLen(message)#>
			<cfif i gt 1><br /></cfif>#application.com.security.sanitiseHTML(message[i])#
		</cfloop>
	</cfoutput>
</div>
</cfif>
<p>Use this tool to reset entity match names.  Match names are one of the ways used to find matches.  They form
different strings that make it possible to match organisations, locations and people.</p>
<p><strong>Note: This process can run for many minutes as it checks the whole database.</strong></p>

<form action="" method="post">
	<div class="form-group">
		<h4>Update:</h4>
	<div class="checkbox">
		<label><input type="checkbox" name="updateOrgMatchNames"> &nbsp; Organisation Match Names</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="updateLocMatchNames"> &nbsp; Location Match Names</label>
	</div>
	<div class="checkbox">
		<label><input type="checkbox" name="updatePerMatchNames"> &nbsp; Person Match Names</label>
	</div>
	<input type="submit" name="update" value="Yes" class="btn btn-primary">
</form>
<!--- END: 2013-12-18 NYB Case 436891 added --->
