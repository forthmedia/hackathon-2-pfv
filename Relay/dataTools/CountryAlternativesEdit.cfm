<!--- ©Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			CountryAlternativesEdit.cfm
Author:				Roksolana Povaliy
Date started:		2017/01/23
	
Description:			

creates country alternative view

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed

Possible enhancements:

 --->

<cfset fkstruct = StructNew()>

<cfset fkstruct["CountryID"] = StructNew()>
<cfset fkstruct["CountryID"].WhereClause = " ISOCode IS NOT NULL">

<CF_RelayRecordManager 
	DATASOURCE="#application.siteDatasource#" 	 	
	TABLE="countryAlternatives" 
	screenTitle=" "  
	allow_add="Yes"  
	allow_edit="Yes"  
	fkstruct = "#fkstruct#"
>
