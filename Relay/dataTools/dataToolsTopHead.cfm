<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="pageTitle" default="">

<CF_RelayNavMenu pageTitle="#pageTitle#" moduleTitle="" thisDir="datatools">
	<!--- <CFIF findNoCase("massdedupe.cfm",SCRIPT_NAME) or findNoCase("massdedupe.cfm",request.query_string)>
		<cfif isDefined("perDedupe") and perDedupe and isDefined("getOnlyDupes")>
			<CF_RelayNavMenuItem MenuItemText="Merge Checked Persons" CFTemplate="javascript:dedupePersons()">
			<CF_RelayNavMenuItem MenuItemText="Merge Checked Organisations" CFTemplate="javascript:dedupeOrganisations()">
			<CF_RelayNavMenuItem MenuItemText="Refresh List" CFTemplate="javascript:location.reload(true)">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Search for Dupes" CFTemplate="/dataTools/massDedupe.cfm">
	</CFIF> --->
	<cfset hasCorrectSecurity = application.com.login.checkInternalPermissions ("AdminTask", "Level3")>
	<cfif hasCorrectSecurity>
		<CFIF findNoCase("massdedupe2.cfm",SCRIPT_NAME)>
			<CF_RelayNavMenuItem MenuItemText="View all matches" CFTemplate="/dataTools/dedupeMatchesList.cfm">
			<CF_RelayNavMenuItem MenuItemText="Find Dupes" CFTemplate="/dataTools/dedupeFindDupes.cfm">
			<!--- <CF_RelayNavMenuItem MenuItemText="Update Match Names" CFTemplate="/dataTools/updateMatchNames.cfm"> --->
		</CFIF>
		<!--- <CFIF findNoCase("updateMatchNames.cfm",SCRIPT_NAME)>
			<CF_RelayNavMenuItem MenuItemText="Perform Merges" CFTemplate="/dataTools/massDedupe2.cfm">
			<CF_RelayNavMenuItem MenuItemText="View all matches" CFTemplate="/dataTools/dedupeMatchesList.cfm">
			<CF_RelayNavMenuItem MenuItemText="Find Dupes" CFTemplate="/dataTools/dedupeFindDupes.cfm">
		</CFIF> --->
		<CFIF findNoCase("dedupeFindDupes.cfm",SCRIPT_NAME)>
			<CF_RelayNavMenuItem MenuItemText="Perform Merges" CFTemplate="/dataTools/massDedupe2.cfm">
			<CF_RelayNavMenuItem MenuItemText="View all matches" CFTemplate="/dataTools/dedupeMatchesList.cfm">
	<!--- 		<CF_RelayNavMenuItem MenuItemText="Update Match Names" CFTemplate="/dataTools/updateMatchNames.cfm"> --->
		</CFIF>
	</cfif>
	<CFIF findNoCase("dedupeMatchesList.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Perform Merges" CFTemplate="/dataTools/massDedupe2.cfm">
		<CF_RelayNavMenuItem MenuItemText="Find Dupes" CFTemplate="/dataTools/dedupeFindDupes.cfm">
		<!--- <CF_RelayNavMenuItem MenuItemText="Update Match Names" CFTemplate="/dataTools/updateMatchNames.cfm"> --->
	</CFIF>

	<CFIF findNoCase("countryGroupEdit.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add New Country Group" CFTemplate="/dataTools/countryGroupEdit.cfm?action=add">
	</CFIF>
	<CFIF findNoCase("countryEdit.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Previous" CFTemplate="javascript:move(-1);">
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:submitCountryEditForm();">	
		<CF_RelayNavMenuItem MenuItemText="Next" CFTemplate="javascript:move(1);">	
	</CFIF>
	<!--- Kick clives build machine into action --->
	<CFIF findNoCase("activityTypeEdit.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Previous" CFTemplate="javascript:move(-1);">
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:submitActivityTypeEditForm();">	
		<CF_RelayNavMenuItem MenuItemText="Next" CFTemplate="javascript:move(1);">	
	</CFIF>

</CF_RelayNavMenu>
