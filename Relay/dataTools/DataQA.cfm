<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:		DataQA.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
08-10-2008			NJH			Fixed a bug T-10 Issue 1064 - Incorrect country description was showing
07-12-2010			MS			LID4798: Spelling error fixed - 'you' changed to 'your'

Possible enhancements:


 --->



<cfinclude template="/templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT CountryID, CountryDescription
	FROM Country
   WHERE CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
   ORDER BY CountryDescription
</CFQUERY>

<cfinclude template="/templates/qrygetSelections.cfm">

<cf_head>
	<cf_title>Data Quality Utility</cf_title>
</cf_head>


<CFSET current="dataQA.cfm">

<cf_relayFormDisplay>
<cfform NAME="ThisForm" ACTION="/dataTools/DataQA.cfm" METHOD="POST">


	<CFIF NOT isDefined("frmFieldName") and NOT isDefined("frmCountryID")>
	<!--- Only show these instructions the first time you come into the screen --->
	<TR>
		<TD COLSPAN="2" CLASS="CreamBackground">
		Using this tool you can check and correct the values in certain fields across the 
		whole database.  Corrections must be done one country at a time.  By selecting a country
		 and a field from the pull down menus below and clicking Get Current Values, you will be able 
		presented with a list of the current values for that field. 
		You can then correct the values you are not pleased with. <BR><BR>
		<!--- 07-12-2010	MS	LID4798: Spelling error fixed - 'you' changed to 'your' --->
		You can further narrow the records to work on by adding a selection to your search criteria.
		
		</TD>
	</TR>
	<TR>
	<TD COLSPAN="2">&nbsp;</TD>
	</TR>

	</CFIF>
	
	<cf_relayFormElementDisplay label="Country" relayFormElementType="select" fieldname="frmCountryId" currentvalue="" query="#GetCountries#" display="countryDescription" value="countryID" required="true" nullText="Select a country to work on">
		
	<cf_querySim>
		getFieldNames
		value,display
		Person.salutation|Person.salutation
		Person.Language|Person.Language
		""|-----
		Location.address3|Location.address3
		Location.address4|location.address4 - Town
		Location.address5|location.address5 - City
	</cf_querySim>

	<cf_relayFormElementDisplay label="Field" relayFormElementType="select" fieldname="frmFieldName" currentvalue="" query="#getFieldNames#" display="display" value="value" required="true" nullText="Select a field to work on">

	<TR><TD>Choose a selection (optional):</TD>
		<TD><SELECT NAME="frmSelectionID">
			<OPTION VALUE="0">Narrow your choice to a selection within a country 
			<CFOUTPUT QUERY="getSelections">
				<OPTION VALUE="#SelectionID#"> #HTMLEditFormat(title)#</CFOUTPUT>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD><INPUT TYPE="Submit" VALUE="  Get Current Values  ">
			<INPUT TYPE="Reset">
		</TD>
	</TR>
	</TR>
		
		<TD COLSPAN="2">NOTE: If you wish to specify valid values for any field, please <A HREF="/dataTools/ValidValues.cfm">edit Valid Values</A>
		to specify the values you want.</TD>
	<TR>

</cfform>
</cf_relayFormDisplay>

<!--- If the query has called the screen then run this next bit --->
<CFIF isDefined("frmFieldName") and isDefined("frmCountryID")>
	<CFSET TableName =#ListFirst(frmFieldName,".")#>

	<CFQUERY NAME="GetValues" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT #frmFieldName# AS FieldValue, Country.CountryDescription, 
		Count(#frmFieldName#) AS NumberofRecs
		FROM Location, Country  
		
		<CFIF #frmSelectionID# IS NOT 0 or #TableName# IS "Person">
			, Person 
		</CFIF>
		
		<CFIF #frmSelectionID# IS NOT 0>
			, selectiontag 
		</CFIF> 
		
			WHERE Location.CountryID = Country.CountryID
		<CFIF #frmSelectionID# IS NOT 0  or #TableName# IS "Person">
			AND Location.LocationID = Person.LocationID
		</CFIF>
		
		<CFIF #frmSelectionID# IS NOT 0>
			AND selectiontag.entityid = Person.personid 
			AND selectiontag.selectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF> 
			GROUP BY Location.CountryID, #frmFieldName#, Country.CountryDescription
			HAVING Location.CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
		<CFIF isDefined("frmFieldCriteria")>
			<CFIF #frmFieldCriteria# IS NOT "">
			#frmFieldName# = #frmFieldCriteria#
			</CFIF>
		</CFIF>
	</CFQUERY>

	<CFQUERY NAME="GetValidValues" DATASOURCE="#application.SiteDataSource#">
		SELECT      CountryID, ValidFieldID, FieldName, ValidValue, DefaultFieldValue
		FROM         dbo.ValidFieldValues 
		WHERE       (CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (FieldName =  <cf_queryparam value="#frmFieldName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		ORDER By SortOrder
	</CFQUERY>
	<CFSET ValidValues = #ValueList(GetValidValues.ValidValue)#>

	<cfparam name="message" default="">
	<cfoutput><p class="messageText">#application.com.security.sanitiseHTML(message)#</p></cfoutput>

	<TABLE border=1 cellpadding=3 cellspacing=0>
		<TR><TD COLSPAN="5"><H2><!--- <CFOUTPUT>Current field values for #frmFieldName# in #ListGetAt(ValueList(getCountries.CountryDescription), ListFind(ValueList(getCountries.CountryID), frmCountryID))#</CFOUTPUT> --->
			<!--- NJH 2008-10-08 Bug Fix T-10 Bugs Issue 1064 - incorrect country description was showing with the above code--->
			<CFOUTPUT>Current field values for #htmleditformat(frmFieldName)# in #getCountries.CountryDescription[ListFind(ValueList(getCountries.CountryID),frmCountryID)]#</CFOUTPUT>
		</H2></TD></TR>
		<TR>
		    <TH WIDTH="100">Current values for <CFOUTPUT>#htmleditformat(frmFieldName)#</CFOUTPUT>
				<FONT SIZE="-2">Click Edit to change record values</FONT>
			</TH>  
		    <TH>Number of records with this value</TH>  
		    <TH>Change To&nbsp;<FONT SIZE="-2"><BR>Specify the valid value.  
				<CFOUTPUT><A HREF="/dataTools/ValidValues.cfm?frmFieldName=#frmFieldName#&frmCountryID=#frmCountryID#" STYLE="font-size: -2;">
				<FONT SIZE="-2">Click here</FONT></A> if you want to add/edit 
				a pull down list of <BR>valid values for #htmleditformat(frmFieldName)#</CFOUTPUT></FONT></TH> 
			<TH>Uppercase/Lowercase</TH> 
			<TH>&nbsp;</TH> 
		</TR>
<CFLOOP QUERY="GetValues">		
	<CFOUTPUT>
		<cfFORM ACTION="/dataTools/DataQATask.cfm" METHOD="POST">
			<CF_INPUT TYPE="Hidden" NAME="frmOldValue" VALUE="#FieldValue#">
			<CF_INPUT TYPE="Hidden" NAME="frmFieldName" VALUE="#frmFieldName#">
			<INPUT TYPE="Hidden" NAME="frmNewValue_required" VALUE="You must provide a field value to update">
			<CF_INPUT TYPE="Hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
			<CF_INPUT TYPE="Hidden" NAME="frmSelectionID" VALUE="#frmSelectionID#">
			<CF_INPUT TYPE="Hidden" NAME="frmCountryDescription" VALUE="#CountryDescription#">
			<TR>
			<CFIF CurrentRow MOD 2 IS NOT 0> 
				<CFSET StyleColor = '##FFFFCE'>
			<CFELSE>
				<CFSET StyleColor = 'White'>
			</CFIF>
			
				<TD STYLE="background-color: #StyleColor#;"><!--- <A HREF="/dataTools/dataQAEditRecs.cfm?frmOldValue=#URLEncodedFormat(FieldValue)#&frmFieldName=#URLEncodedFormat(frmFieldName)#&frmCountryID=#frmCountryID#&frmSelectionID=#URLEncodedFormat(frmSelectionID)#">Edit </A> --->'#HTMLEditFormat(FieldValue)#'</TD>
				<TD STYLE="background-color: #StyleColor#;" ALIGN="CENTER">#htmleditformat(NumberofRecs)#</TD>

				<TD STYLE="background-color: #StyleColor#;">
					<CFIF #GetValidValues.RecordCount# GT 0>
						<SELECT NAME="frmNewValue">
						<CFLOOP QUERY="GetValidValues">
							<OPTION VALUE="#ValidValue#">#htmleditformat(ValidValue)# </OPTION>
						</CFLOOP>
						</SELECT>
					<CFELSE>	
						<cfINPUT TYPE="Text" NAME="frmNewValue" SIZE="25" MAXLENGTH="25" required="true">
					</CFIF>
				</TD>
				
				<TD STYLE="background-color: #StyleColor#;">
					<INPUT TYPE="Radio" NAME="frmRadio" VALUE="T" CHECKED>Title Case<BR>
					<INPUT TYPE="Radio" NAME="frmRadio" VALUE="U">ALL UPPERCASE<BR>
					<INPUT TYPE="Radio" NAME="frmRadio" VALUE="L">all lowercase
				</TD>
				<TD STYLE="background-color: #StyleColor#;"><INPUT TYPE="Submit" VALUE="Update value"></TD>
			</TR>
		</cfFORM>
	</CFOUTPUT>	
</CFLOOP>	
</TABLE>

</CFIF>











