<!--- �Relayware. All Rights Reserved 2014 --->

<cfsetting enablecfoutputonly="NO" requesttimeout="600">

<cfparam name="message" default="">
<cfparam name="form.countryIDs" default="">
	<cfscript>
		qMatchMethods = application.com.dedupe.getAllMatchMethods(dedupeTypeID=1); // NJH TND510 Blocked/unBlocked: moved dedupeViewType to dedupeTypeID
	</cfscript>

<cfif structKeyExists(form,"generateOrgMatches")>

	<cfif dedupeMatchMethodID neq "">
		<cfif form.countryIDs neq "">
		<cfscript>
			temp = application.com.dedupe.RemoveNoLongerValidDupes(matchMethodID = form.dedupeMatchMethodID,countryIDlist = form.countryIDs);
			message = application.com.dedupe.dedupeRecordsFromView(form.dedupeMatchMethodID,form.countryIDs);
		</cfscript>
		<cfelse>
			<cfset message = "You must select at least one Country.">
		</cfif>
	<cfelse>
		<cfset message = "You must select a Match Method to run from the drop down list.">
	</cfif>
</cfif>
<CFQUERY NAME="findLiveCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT distinct c.countryDescription,
	convert(varchar(3),c.countryid) + '#application.delim1#' +  c.countryDescription as CountryIDandName
	FROM country c
	INNER JOIN organisation o ON o.countryID = c.countryID
	<!--- 2012-07-19 PPB P-SMA001 WHERE o.countryID in (#request.relaycurrentuser.countryList#) --->
	WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# 	<!--- 2012-07-19 PPB P-SMA001 --->
	order by c.countryDescription
</CFQUERY>

<h2><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></h2>
<p>Choose the method and the countries to find possible duplicate records.</p><p><strong>Note: This process can run for 20-30 seconds as it checks the whole database.</strong></p>

<cfset numOfColumns = 6>
<cfset colspan = 2*numOfColumns>

<cfset request.relayFormDisplayStyle = "HTML-table">

<cf_relayFormDisplay formWidth="100%">
<cfform  method="post">
	<cfoutput>
		<tr><td colspan="#colspan#"><div><label>Match method:</label> <cf_relayFormElement relayFormElementType="select" currentValue="" label="Match Method" fieldName="dedupeMatchMethodID" query="#qMatchMethods#" value="dedupeMatchMethodID" display="dedupeMatchMethodDescription" required="Yes" nullText="Select a match method to use" nullValue=""></td></tr>
		<tr><th colspan="#colspan#">Select Countries</strong></th></tr>
	</cfoutput>
	<cfoutput query="findLiveCountries">
		<cfif findLiveCountries.currentrow MOD numOfColumns eq 1>
			<tr>
		</cfif>
		<!--- <span style="width=180px"><input type="checkbox" name="countryIDs" value="#listFirst(CountryIDandName,application.delim1)#">#ListLast(CountryIDandName,application.delim1)#</span> --->
			<td><cf_relayFormElement relayFormElementType="checkbox" fieldName="countryIDs" currentValue="" value="#listFirst(CountryIDandName,application.delim1)#"></td><td>#ListLast(CountryIDandName,application.delim1)#</td>
		<cfif findLiveCountries.currentrow MOD numOfColumns eq 0>
			</tr>
		<cfelseif findLiveCountries.recordCount eq findLiveCountries.currentrow>
			<cfset thisColspan = 2*(numOfColumns - (findLiveCountries.currentrow MOD numOfColumns))>
			<td colspan="#thisColspan#">&nbsp;</td>
			</tr>
		</cfif>
	</cfoutput>

	<cfoutput>
		<tr><td colspan="#colspan#"><cf_relayFormElement relayFormElementType="submit" fieldName="generateOrgMatches" currentValue="Generate matches" class="btn btn-primary"></td></tr> <!--- <input type="submit" onClick="this.value = 'Please wait.  This can take several minutes...'; this.disabled = true;" name="generateOrgMatches" value="Yes"> --->
<!--- <input type="submit" name="generateOrgMatches" value="Yes"> --->
	</cfoutput>
</cfform>
</cf_relayFormDisplay>


