<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name: LiabilityReport.cfm			
Author:	Chris Snape		
Date created: 2001-04-03	

	Objective - This template selects the liability data for a given quarter and region
	
	Parameters - Quarter - the quarter to be reported on 
				 Region - the region to be reported on 
				
	Return Codes - None

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:


--->

<!--- Get the quarters that the user will select from --->
<CFQUERY NAME="getQuarters" datasource="#application.siteDataSource#">
	SELECT *
	  FROM calendarQuarter
	 WHERE startDate <= getdate() 
</CFQUERY>

<!--- Get the countries the user has access to --->
<CFINCLUDE TEMPLATE="../templates/qryGetCountries.cfm">

<!--- Get the regions designated for the Incentive Program --->
<CFQUERY NAME="getInFocusRegions" datasource="#application.siteDataSource#">
	SELECT cou.countryId
	      ,cou.countryDescription
	  FROM country cou
		  ,booleanFlagData bfd
		  ,flag fla
	 WHERE cou.countryId = bfd.entityId 
	   AND bfd.flagId = fla.flagId
	   AND fla.flagTextId = 'IncentiveRegion'
  ORDER BY cou.countryId					  
</CFQUERY>

<CFSET InFocusRegions = ValueList(getInFocusRegions.countryId)>

<!--- Get the regions designated for the Incentive Program where the user has access to at least one country in that region --->	   
<CFQUERY NAME="getRegions" datasource="#application.siteDataSource#">
	SELECT cou.countryId
	      ,cou.countryDescription
	  FROM country cou
		  ,booleanFlagData bfd
		  ,flag fla
	 WHERE cou.countryId = bfd.entityId 
	   AND bfd.flagId = fla.flagId
	   AND fla.flagTextId = 'IncentiveRegion'
 	   AND EXISTS (SELECT NULL 
	                 FROM countryGroup cgr
	                WHERE cgr.countryMemberId  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	                  AND cgr.countryGroupId = cou.countryId)
  ORDER BY cou.countryId					  
</CFQUERY>

<CFSET RegionAccess = ValueList(getRegions.countryId)>

<!--- First time round set the following 2 parameters to ensure the custom tag CF_HTML2Excel does not create a file --->
<CFIF NOT IsDefined("calendarQuarterId")>

	<CFSET createFile = 'No'>	
	<CFSET File = ''>

<CFELSE>
<!--- Get the details for the Quarter selected by the user --->
	<CFQUERY NAME="getQuarter" datasource="#application.siteDataSource#">
		SELECT *
		  FROM calendarQuarter
		 WHERE calendarQuarterId =  <cf_queryparam value="#calendarQuarterId#" CFSQLTYPE="CF_SQL_INTEGER" >  
	</CFQUERY>
	
	<CFSET year = getQuarter.calendarYear> 
	<CFSET quarter = getQuarter.quarterOfCalendarYear>	
	<CFSET quarterDesc = getQuarter.calendarQuarterDesc>
	<CFSET createFile = 'Yes'>

</CFIF>

<FORM ACTION="LiabilityReport.cfm" METHOD="post">

	<TABLE>
		<TR><TD CLASS="Title2">Select Reporting Quarter:</TD> 
			<TD>
				<SELECT NAME="calendarQuarterId">
					<CFLOOP QUERY="getQuarters">
						<CFOUTPUT><OPTION VALUE="#calendarQuarterId#" <CFIF IsDefined("calendarQuarterId") AND IsDefined("Form.calendarQuarterId") AND Form.calendarQuarterId EQ calendarQuarterId>SELECTED</CFIF>>#htmleditformat(CalendarQuarterDesc)#</OPTION></CFOUTPUT>
					</CFLOOP>
				</SELECT>	
			</TD>
		</TR>	
		
		<CFOUTPUT>
		<!--- Abort if the user does not have access to any regions designated for the Incentive programme --->
		<CFIF getRegions.recordCount EQ 0>
			Sorry you cannot access data on any of the regions covered by this report. 		
			<CF_ABORT>
		<!--- If the user only has access to one region, for example, the InFocus champions, then the report
		      can only be run for that region. Therefore do not show the region selection box --->  
		<CFELSEIF getRegions.recordCount EQ 1>	
			<CF_INPUT NAME="region" TYPE="Hidden" VALUE="#getRegions.countryId#">
		<CFELSE>
			<TR><TD CLASS="Title2">Select Reporting Region:</TD>
				<TD>
					<SELECT NAME="region">
						<!--- If the user has access to all the regions specified as part of the InFocus programmme then
						      allow the option to run the report for all regions --->
						<CFIF InFocusRegions EQ RegionAccess>
							<OPTION VALUE="#InFocusRegions#" <CFIF IsDefined("region") AND IsDefined("Form.region") AND Form.region EQ InFocusRegions>SELECTED</CFIF>>All Regions</OPTION>
						</CFIF>
						<CFLOOP QUERY="getRegions">
							<OPTION VALUE="#countryId#" <CFIF IsDefined("region") AND IsDefined("Form.region") AND Form.region EQ countryId>SELECTED</CFIF>>#htmleditformat(countryDescription)#</OPTION>
						</CFLOOP>
					</SELECT>		
				</TD>
			</TR>	
		</CFIF>		
		</CFOUTPUT>	
		<TR><TD COLSPAN="2"><INPUT TYPE="Submit" NAME="Submit" VALUE="Run Report"></TD></TR>	
	</TABLE>

	<CFIF IsDefined("Region")> 

		<CFSET Regions = '#Region#'>		
		<!--- If the user has specified 'All Regions' then this loop will call the procedure for each region. 
		      Otherwise the parameter will only contain the id of the individual region selected --->
		<CFLOOP INDEX="Region" LIST="#Regions#">

			<CFQUERY NAME="getCountry" datasource="#application.siteDataSource#">
				SELECT countryDescription
				  FROM country 
				 WHERE countryId =  <cf_queryparam value="#region#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
	
			<CFSET country = getCountry.countryDescription>
	
			<CFSTOREDPROC PROCEDURE="rwGetLiabilityDetails" DATASOURCE="#application.siteDataSource#" debug="true">
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Year" VALUE="#getQuarter.calendarYear#">
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Quarter" VALUE="#getQuarter.quarterOfCalendarYear#">
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Region" VALUE="#Region#">
		
				<CFPROCRESULT NAME="getPointsAccrued" RESULTSET="1"> 
				<CFPROCRESULT NAME="getPreviousPoints" RESULTSET="2">
				<CFPROCRESULT NAME="getNetLiability" RESULTSET="3">
				<CFPROCRESULT NAME="getFrozenPoints" RESULTSET="4">
				<CFPROCRESULT NAME="getAgeingPoints" RESULTSET="5">								
	
			</CFSTOREDPROC>	

			<!--- Include the stylesheet so that the final Excel spreadsheet has all the required styles --->
			

 			<TABLE>
				<TR><TD COLSPAN="4">&nbsp;</TD></TR>
				<TR><TD CLASS="ReportLevel1" COLSPAN="4"><!--- <CFOUTPUT>#ucase(application. ProgramName)# NJH 2010/08/23 Removed for 8.3</CFOUTPUT> ---> LIABILITY REPORT</TD></TR>
				<TR><TD CLASS="ReportLevel2">Quarter:</TD> 
					<TD COLSPAN="2"><CFOUTPUT><CFIF IsDefined("calendarQuarterId")>#htmleditformat(QuarterDesc)#</CFIF></CFOUTPUT></TD>
				</TR>	
				<TR><TD CLASS="ReportLevel2">Region:</TD>
					<TD COLSPAN="2"><CFOUTPUT><CFIF IsDefined("region")>#htmleditformat(country)#</CFIF></CFOUTPUT></TD>
				</TR>	
				<TR><TD>&nbsp;</TD></TR>
				<TR><TD COLSPAN="3" CLASS="Title2" BGCOLOR="gray">Points Accrual</TD></TR>	
			</TABLE>

 			<TABLE>
				<TR>
					<TD>
						<TABLE>		
							<TR>
								<TD>
									<TABLE>
										<TR>
											<TD CLASS="HeaderLevel2">Weekly</TD>							
											<TD COLSPAN="3">&nbsp;</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD>
									<TABLE BORDER="1">
										<TR ALIGN="center">
											<TD CLASS="HeaderLevel3">Week</TD>
											<TD CLASS="HeaderLevel3">W/E Date</TD>											
											<TD CLASS="HeaderLevel3">No SOD<BR>Weeks</TD>											
	 										<TD CLASS="HeaderLevel3">Base</TD>
											<TD CLASS="HeaderLevel3">Promotions</TD>
											<TD CLASS="HeaderLevel3">Miscellaneous</TD>											
											<TD CLASS="HeaderLevel3">Total</TD>				
										</TR>
	
										<CFOUTPUT QUERY="getPointsAccrued">
											<TR ALIGN="right">
												<TD ALIGN="center">#htmleditformat(week)#</TD>
											    <TD ALIGN="center">#DateFormat(weekEndingDate, "DD-MMM-YY")#</TD>												
												<TD>#htmleditformat(SODWeeks)#</TD>												
												<TD>#htmleditformat(basePoints)#</TD>
												<TD>#htmleditformat(promotionPoints)#</TD>
												<TD>#htmleditformat(miscPoints)#</TD>												
												<TD>#htmleditformat(totalPoints)#</TD>
											</TR>
										</CFOUTPUT>
									</TABLE>
								</TD>
							</TR>
						</TABLE>	
					</TD>	
					<TD>&nbsp;</TD>
					<TD>
						<TABLE>
							<TR>
								<TD>
									<TABLE>			
										<TR>
											<TD CLASS="HeaderLevel2">Cumulative</TD>
											<TD COLSPAN="3">&nbsp;</TD>											
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD>
									<TABLE BORDER="1">				
										<TR ALIGN="center">
											<TD CLASS="HeaderLevel3">Week</TD>
											<TD CLASS="HeaderLevel3">W/E Date</TD>											
											<TD CLASS="HeaderLevel3">Base</TD>
											<TD CLASS="HeaderLevel3">Promotions</TD>
											<TD CLASS="HeaderLevel3">Miscellaneous</TD>											
											<TD CLASS="HeaderLevel3">Total</TD>				
										</TR>
		
										<CFOUTPUT QUERY="getPointsAccrued">
											<TR ALIGN="right">
												<TD ALIGN="center">#htmleditformat(week)#</TD>
												<TD ALIGN="center">#DateFormat(weekEndingDate, "DD-MMM-YY")#</TD>												
												<TD>#htmleditformat(cumBasePoints)#</TD>
												<TD>#htmleditformat(cumPromotionPoints)#</TD>
												<TD>#htmleditformat(cumMiscPoints)#</TD>												
												<TD>#htmleditformat(cumTotalPoints)#</TD>
											</TR>
										</CFOUTPUT>
									</TABLE>
								</TD>
							</TR>			
						</TABLE>	
					</TD>
				</TR>	
				<TR>
					<TD COLSPAN="10">Note Miscellaneous Points may appear during the first five weeks after a partner has registered to prevent their balance from going negative</TD>
				</TR>
			</TABLE>

			<TABLE>
				<TR><TD>&nbsp;</TD></TR>
				<TR><TD CLASS="Title2" COLSPAN="3">Net Liability to Date</TD></TR>
		    	<TR><TD CLASS="HeaderLevel1" COLSPAN="5">Summary of points carried forward from previous quarters:</TD></TR>
			</TABLE>
			<TABLE>
				<CFOUTPUT>
				<TR>
					<TD CLASS="ReportLevel2" COLSPAN="3">Cumulative Points Accrued:</TD>
					<TD>#htmleditformat(getPreviousPoints.Accrued)#</TD>
				</TR>
				<TR>
					<TD CLASS="ReportLevel2" COLSPAN="3">Cumulative Points Used:</TD>
					<TD>#htmleditformat(getPreviousPoints.Used)#</TD>			
				</TR>
				<TR>
					<TD CLASS="ReportLevel2" COLSPAN="3">Cumulative Points Expired:</TD>
					<TD>#htmleditformat(getPreviousPoints.Expired)#</TD>			
				</TR>
				</CFOUTPUT>
			</TABLE>
	
			<TABLE BORDER="1">
				<TR ALIGN="center">
					<TD CLASS="HeaderLevel3">Week</TD>
					<TD CLASS="HeaderLevel3">W/E Date</TD>					
					<TD CLASS="HeaderLevel3" COLSPAN="2">Total Accrued</TD>
					<TD CLASS="HeaderLevel3" COLSPAN="2">Used</TD>
			 		<TD CLASS="HeaderLevel3" COLSPAN="2">Expired</TD>				
		 			<TD CLASS="HeaderLevel3">Balance / Liability</TD>			
				</TR>
				<TR ALIGN="center">
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>					
					<TD>Week</TD>
					<TD>Cumulative</TD>
					<TD>Week</TD>
					<TD>Cumulative</TD>
					<TD>Week</TD>
					<TD>Cumulative</TD>						
					<TD>&nbsp;</TD>			
				</TR>
				
				<CFOUTPUT QUERY="getNetLiability">
					<TR ALIGN="right">
						<TD ALIGN="center">#htmleditformat(week)#</TD>
						<TD ALIGN="center">#DateFormat(weekEndingDate, "DD-MMM-YY")#</TD>						
						<TD>#htmleditformat(accruedPoints)#</TD>
						<TD>#htmleditformat(cumAccruedPoints)#</TD>
						<TD>#htmleditformat(usedPoints)#</TD>
						<TD>#htmleditformat(cumUsedPoints)#</TD>
						<TD>#htmleditformat(expiredPoints)#</TD>
						<TD>#htmleditformat(cumExpiredPoints)#</TD>								
						<TD>#htmleditformat(liability)#</TD>
					</TR>
				</CFOUTPUT>
			</TABLE>
		
			<TABLE>
				<TR>			
					<TD CLASS="ReportLevel2" COLSPAN="2">Points Currently Frozen:</TD>
					<TD><CFOUTPUT>#htmleditformat(getFrozenPoints.pointsFrozen)#</CFOUTPUT></TD>
				</TR>			
				<TR><TD>&nbsp;</TD></TR>
				<TR>
					<TD CLASS="Title2" COLSPAN="2">Points Ageing Report</TD>  
					<TD> (Note these figures are only relevant to the current quarter)</TD>
				</TR>
			</TABLE>	

			<TABLE BORDER="1">
				<TR ALIGN="center">
					<TD CLASS="HeaderLevel3">Liability</TD>
					<TD CLASS="HeaderLevel3" COLSPAN="2">Due to expire this<BR>quarter</TD>
					<TD CLASS="HeaderLevel3" COLSPAN="2">Due to expire within<BR>next quarter</TD>
			 		<TD CLASS="HeaderLevel3" COLSPAN="2">Due to expire within<BR>2 quarters</TD>				
		 			<TD CLASS="HeaderLevel3" COLSPAN="2">Due to expire within<BR>3 quarters</TD>			
				</TR>		
		
				<CFOUTPUT>
					<TR ALIGN="right">
						<TD>#htmleditformat(getAgeingPoints.liability)#</TD>
						<TD COLSPAN="2">#htmleditformat(getAgeingPoints.expiring0Qtr)#</TD>
						<TD COLSPAN="2">#htmleditformat(getAgeingPoints.expiring1Qtr)#</TD>
						<TD COLSPAN="2">#htmleditformat(getAgeingPoints.expiring2Qtr)#</TD>
						<TD COLSPAN="2">#htmleditformat(getAgeingPoints.expiring3Qtr)#</TD>												
					</TR>		
				</CFOUTPUT>			
			</TABLE>
		
<!---    			</CF_HTML2Excel> --->
	
			<BR>
				
<!--- 			<CFIF IsDefined("Form.calendarQuarterId")>
				<CFOUTPUT><A HREF="#request.currentSite.httpProtocol##siteDomain##application. UserFilesPath#/Reports/#File#.xls">click here to download this report</A></CFOUTPUT>	
			</CFIF>	
 --->			
			<TABLE BORDER=0 CELLPADDING="5" CELLSPACING="0" WIDTH="100%">
				<TR><TD>&nbsp;</TD></TR>
		        <TR>
					<CFOUTPUT><TD CLASS="Title3">End of Report for #htmleditformat(country)# Region</TD></CFOUTPUT>
				</TR>
			</TABLE>			

		</CFLOOP>	

	</CFIF>
		
</FORM>

