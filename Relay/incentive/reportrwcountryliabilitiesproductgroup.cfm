<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportRWCountryLiabilitiesProductGroup.cfm	
Author:				Godfrey Smith
Date started:		2008-02-21
Description:		Displays a report of points earned/spent by product	

Amendment History:
Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:
 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<!--- give the user the choice of excel --->
<cfparam name="openAsExcel" type="boolean" default="false">
<!--- set some default values for output object --->
<cfparam name="form.showcomplete" default="0">
<cfparam name="useFullRange" type="boolean" default="true">
<cfparam name="sortorder" type="string" default="country">
<cfparam name="startrow" type="string" default="1">
<cfparam name="numrowsperpage" type="string" default="100">
<!--- <cfparam name="countryGroupTypeID" type="numeric" default="3"> --->
<!--- <cfparam name="frmPriceISOcode" type="string" default="RWP"> --->
<cfparam name="created" type="date" default="#now()#">

<!--- <cfset frmCurrencyPerPoint = "1"> --->
<cfset useDateRange ="true">
<cfset dateRangeFieldName="created">
<cfset dateField="created">
<cfset tableName="RWTransaction">
<cfinclude template="/templates/DateQueryWhereClause.cfm">


<!--- query to obtain points data --->
<cfquery name="getCountryLiabilities" datasource="#application.siteDataSource#">
SELECT country,Product_Group, sum(earned) as earned from(
	SELECT * FROM (
		SELECT   rwt.created as created, Country.CountryDescription AS Country, ProductGroup.Description AS Product_Group, RWTransactionItems.points as earned --rwt.CreditAmount AS Earned
			FROM RWTransaction rwt INNER JOIN
			RWTransactionItems ON rwt.RWTransactionID = RWTransactionItems.RWTransactionID INNER JOIN
			Product ON RWTransactionItems.ProductID = Product.ProductID INNER JOIN
			ProductGroup ON Product.ProductGroupID = ProductGroup.ProductGroupID INNER JOIN
			RWCompanyAccount ON rwt.AccountID = RWCompanyAccount.AccountID INNER JOIN
			organisation ON RWCompanyAccount.OrganisationID = organisation.OrganisationID INNER JOIN
			Country ON organisation.CountryID = Country.CountryID
			<!--- 2012-07-25 PPB P-SMA001 commented out
			WHERE organisation.countryID in (#request.relayCurrentUser.countryList#)
			 --->
			where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="RWTransaction",alias="o").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
		) base
	WHERE 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	) base
GROUP BY country,Product_Group
ORDER BY <cf_queryObjectName value="#sortorder#">
</cfquery>
<!--- / query to obtain points data --->


<cf_head>
	<cf_title>RW Country Liability by Product Group</cf_title>
</cf_head>


<!--- 'fix for display irregularity - good old tables :) replaced nbsp;'s with cellpadding' [godfrey.smith]- February 21, 2008, 15:23:17 PM --->
<cfoutput>
<table border="1" cellspacing="0" cellpadding="5" width="100%">
	<tr class="submenu">
		<td align="left" valign="top" class="submenu">Country Liability by Product Group</td>
		<td align="right" valign="top" class="submenu">
			<a href="reportRWCountryLiabilitiesProductGroup.cfm?#queryString#&openAsExcel=true" class="submenu">Excel</a> 
		</td>
	</tr>
</table>
</cfoutput>

<cf_translate>



<!--- <cfquery name="getPointsConversion" datasource="#application.siteDataSource#">
	select currencyPerPoint, priceIsoCode
	from RWpointsConversion where currencyPerPoint is not null
</cfquery>

<cfquery name="getCurrencyPerPoint" datasource="#application.siteDataSource#">
	select case when currencyPerPoint is null then 1 else currencyPerPoint end as currencyPerPoint
	from RWpointsConversion where currencyPerPoint is not null
		and priceISOCode = '#frmPriceISOcode#' 
</cfquery>

<cfset frmCurrencyPerPoint = getCurrencyPerPoint.currencyPerPoint> --->

<!--- <cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	passThruVars = StructNew();
	StructInsert(passthruVars, "showComplete", form.showComplete);
	//StructInsert(passthruVars, "frmPriceISOcode", frmPriceISOcode);
	StructInsert(passthruVars, "useFullRange", useFullRange);
</cfscript> --->

<cfset request.relayFormDisplayStyle="HTML-table">



<!--- <cfset includeFileIncluded = true>	 --->

<!--- display populated object --->
<CF_tableFromQueryObject queryObject="#getCountryLiabilities#" 
	FilterSelectFieldList = "Country,Product_Group"
	FilterSelectFieldList2="Country,Product_Group"
	showTheseColumns="Country,Product_Group,Earned"
	allowColumnSorting="no"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	groupbycolumns="Country"
	totalthesecolumns="Earned"
	queryWhereClauseStructure="#queryWhereClause#"
	passThroughVariablesStructure="#passThruVars#"
	numberFormat="Earned"
	openAsExcel="#openAsExcel#"
	sortOrder = "#sortOrder#"
>
</cf_translate>



