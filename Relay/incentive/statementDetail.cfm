﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---

File:			StatementDetailV2.cfm
Author:			GCC
Date created:	2006/03/14

Description:	Displays the week breakdown of the statement.
Restyled and layout simplified for users.

Version history:
1

Amendment History:

Date		Initials 	What was changed

2011/02/01	PPB			LID5463 - protected against showNoOfUsers variable not existing
2014/08/27 				Core-545 - statement is not repsonsive, also fix for the case of text-overlapped on Android, ipad and ie8.
2016-01-13	AHL Case 447535 Filtering Incentive Statement by Organisation
--->

<!--- organisationID is passed into this Template
	we need to check that the person viewing the statment has rights to view this organisationid
 --->


<SCRIPT type="text/javascript">

function DisplayOrder(OrderID)
{
	win = window.open("../Orders/DisplayOrder.cfm?CurrentOrderID=" + OrderID,"PopUp","resizable,scrollbars,status,menubar,width=" + parseInt(screen.availWidth * 0.7, 10) + ",height=" + parseInt(screen.availHeight * 0.7, 10))
	win.moveTo(parseInt(screen.availWidth * 0.1, 10), parseInt(screen.availHeight * 0.1, 10))
	win.focus()
}

</script>

<cfscript>
	oldlocale=setlocale("English (UK)");

	//WORK OUT STATEMENT TYPE
	if (structKeyExists(url, "statementType")){
		statementType = url.statementType;
	}
	if (structKeyExists(form, "statementType")){
		statementType = form.statementType;
	}
	//default to statement Type - personal
	if(not structKeyExists(variables, "statementType")){
		statementType = "company";
	}

	//WORK OUT PERSONID
	if (structKeyExists(url, "personID")){
		personID = listfirst(url.personID,",");
	}
	if (structKeyExists(form, "personID")){
		personID = form.personID;
	}
	//work out through currentuser
	if(not structKeyExists(variables, "personID")){
		personID = request.relaycurrentuser.personID;
	}


	//WORK OUT ORGANISATIONID
	if (structKeyExists(url, "organisationID")){
		organisationID = url.organisationID;
	}
	if (structKeyExists(form, "organisationID")){
		organisationID = form.organisationID;
	}
	//work out through personID
	if(not structKeyExists(variables, "organisationID")){
		getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID = variables.personID);
		organisationID = getFullPersonDetails.organisationID;
	}

	companyAccount = application.com.relayIncentive.getCompanyAccount(organisationID);

	//first table
	if(not compareNoCase(statementType, "company")){
		weekStruct = application.com.relayIncentive.getWeekDetails(WeekEnding=weekEndingDate,organisationID=organisationID); /* 2016-01-13	AHL Case 447535 Filtering Incentive Statement by Organisation */
	}
	else{
		weekStruct = application.com.relayIncentive.getWeekDetails(weekEndingDate, personID);
	}

	//second table
	if(not compareNoCase(statementType, "company")){
		transactionItemsQry = application.com.relayIncentive.getTransactionItemsPerWeek(WeekEnding=weekEndingDate,organisationID=organisationID); /* 2016-01-13	AHL Case 447535 Filtering Incentive Statement by Organisation */
	}
	else{
		transactionItemsQry = application.com.relayIncentive.getTransactionItemsPerWeek(weekEndingDate, personID);
	}

	//A_663 showed company orders for the week and never personal
	if(not compareNoCase(statementType, "company")){
		SpentPointsDetail = application.com.relayIncentive.spentPointsDetail(organisationID,weekendingdate);
	}
	else{
		SpentPointsDetail = application.com.relayIncentive.spentPointsDetail(organisationID,weekendingdate, personID);
	}
</cfscript>

<cf_translate>


<cfoutput>

<cf_head>
	<cf_title>phr_Incentive_Statement_Title</cf_title>
</cf_head>

<cfif isDefined("url.printme")>
	<cfif browser is "ie4">
		<cf_body leftmargin="0" onload="alert('<cfoutput>phr_Incentive_JS_HowToPrint</cfoutput>')">
	<cfelse>
		<cf_body leftmargin="0" onload="window.print()">
	</cfif>
<cfelse>

</cfif>
<a name="top"></a>

<cfset colcount = 6>
<cfset showStatement_BP = "true">
<cfset showStatement_PP = "true">
<cfset showStatement_AC = "true">
<cfset showStatement_CL = "true">
<cfset showStatement_SP = "true">

<cfif lsnumberformat(weekStruct.pointsClaimed,",") EQ 0>
	<cfset showStatement_BP = "false">
	<cfset colcount = colcount - 1>
</cfif>
<cfif lsnumberformat(weekStruct.pointsPromotion,",") EQ 0>
	<cfset showStatement_PP = "false">
	<cfset colcount = colcount - 1>
</cfif>
<cfif lsnumberformat(weekStruct.pointsVerified,",") EQ 0>
	<cfset showStatement_AC = "false">
	<cfset colcount = colcount - 1>
</cfif>
<cfif lsnumberformat(weekStruct.PointsAwaitingVerification,",") EQ 0>
	<cfset showStatement_CL = "false">
	<cfset colcount = colcount - 1>
</cfif>
<cfif lsnumberformat(weekStruct.PointsSpent,",") EQ 0>
	<cfset showStatement_spSP = "false">
	<cfset colcount = colcount - 1>
</cfif>

<!--- lets diplay all the standard stuff first of all --->
<!--- display the summary at the top first of all --->
<p align="left" class="title">
			phr_Incentive_Statement_DetHeading
</p>

<TABLE class="responsiveTable table table-striped table-bordered table-condensed">
	<thead>
	<tr valign="top">
		<th align="center"><b>phr_Incentive_Statement_WeekEndAbbr phr_Date</b></th>
		<cfif showStatement_BP>
		<th align="center"><b>phr_Incentive_Statement_BP</b></th>
		</cfif>
		<cfif showStatement_PP>
		<th align="center"><b>phr_Incentive_Statement_PP</b></th>
		</cfif>
		<cfif showStatement_AC>
		<th align="center"><b>phr_Incentive_Statement_AC</b></th>
		</cfif>
		<cfif showStatement_CL>
		<th align="center"><b>phr_Incentive_Statement_CL</b></th>
		</cfif>
		<cfif showStatement_SP>
		<th align="center"><b>phr_Incentive_Statement_SP</b></th>
		</cfif>
	</tr>
	</thead>
	<tbody>
	<tr class="evenrow">
		<td data-title="phr_Incentive_Statement_WeekEndAbbr phr_Date">#lsdateformat(weekendingdate, "dd-mmm")#<br></td>
		<cfif showStatement_BP>
		<td data-title="phr_Incentive_Statement_BP">#lsnumberformat(weekStruct.pointsClaimed,",")#<br></td>
		</cfif>
		<cfif showStatement_PP>
		<td data-title="phr_Incentive_Statement_PP">#lsnumberformat(weekStruct.pointsPromotion,",")#<br></td>
		</cfif>
		<cfif showStatement_AC>
		<td data-title="phr_Incentive_Statement_AC">#lsnumberformat(weekStruct.pointsVerified,",")#<br></td>
		</cfif>
		<cfif showStatement_CL>
		<td data-title="phr_Incentive_Statement_CL">#lsnumberformat(weekStruct.PointsAwaitingVerification,",")#<br></td>
		</cfif>
		<cfif showStatement_SP>
		<td data-title="phr_Incentive_Statement_SP">#lsnumberformat(weekStruct.PointsSpent,",")#<br></td>
		</cfif>
	</tr>
	</tbody>
</table>

<!--- output the base points stuff --->

<p><a name="BP"></a></p>

<p align="left" class="title">
		phr_Incentive_Statement_BP phr_Incentive_Statement_Earned
</p>

<!-- Score Card Points Earned table-->
<table class="responsiveTable table table-striped table-bordered table-condensed">
    <thead>
</cfoutput>

	<cfoutput>
		<tr>
			<th align="center"><b>phr_Incentive_Statement_Status</b></th>
			<th><b>phr_Incentive_Statement_ProductCode</b></th>
			<!--- <td><b>phr_Description</b></td> --->
			<th align="center"><b>phr_Incentive_Statement_QtyBought</b></th>
			<cfif isDefined("showNoOfUsers") and showNoOfUsers> <!--- GCC 2008/05/06 CR-TND552 --->	<!--- 2011/02/01 PPB LID5463 added isdefined --->
				<td  align="center"><b>phr_incentive_No_Of_Users</b></th>
			</cfif>
			<th align="center"><b>phr_Incentive_Statement_PointsPerUnit</b></th>
			<th align="center"><b>phr_Incentive_Statement_TotalPoints</b></th>

			<!--- <td align="right"><b>phr_Incentive_Statement_NetQty</b></td> --->
		</tr>
		<tr></tr>
	</cfoutput>
</thead>
<tbody>
<CFIF transactionItemsQry.recordCount>
	<cfset localClass="oddrow">
	<cfoutput query="transactionItemsQry">
	<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
	<tr class="#localClass#">
		<td data-title="phr_Incentive_Statement_Status">phr_incentive_statement_#htmleditformat(RWTransactionTypeID)#</td>
		<td data-title="phr_Incentive_Statement_ProductCode">#htmleditformat(sku)#</td>
		<!--- <td>#BasePointsDetail.Description#</td> --->
		<td data-title="phr_Incentive_Statement_QtyBought">#htmleditformat(quantity)#</td>
		<cfif isDefined("showNoOfUsers") and showNoOfUsers> <!--- GCC 2008/05/06 CR-TND552 --->	<!--- 2011/02/01 PPB LID5463 added isdefined --->
		<td data-title="phr_Incentive_Statement_QtyBought"><cfif len(NoOfUsers)>#htmleditformat(NoOfUsers)#<cfelse>&nbsp;</cfif></td>
		</cfif>
		<td data-title="phr_Incentive_Statement_PointsPerUnit">#lsnumberformat(pointsPerUnit,",")#</td>
		<td data-title="phr_Incentive_Statement_TotalPoints">#lsnumberformat(points,",")#</td>

		<!--- <td align="right">#BasePointsDetail.Points#</td> --->
	</tr>
	</cfoutput>
<CFELSE>
	<cfset localClass="oddrow">
	<!--- <cfoutput>
		<tr>
			<td colspan="5"  class="title">Phr_Incentive_Statement_NoPointsThisWeek</td>
		</tr>
	</cfoutput> --->
</cfif>
</tbody>
</table>

<cfoutput>
<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
<p align="left"><b></b>phr_Incentive_Statement_BPPointsThisWeek #lsnumberformat(weekStruct.pointsVerified,",")#</b></p>
</cfoutput>
<!--- output the Spent Points stuff --->

<cfoutput>
<p><a name="SP"></a></p>
<p align="left" class="title">
			phr_Incentive_Statement_S
</p>
<!-- Spent stars/points - confirmed orders table -->
<TABLE class="responsiveTable table table-striped table-bordered table-condensed" >

</cfoutput>

<cfoutput>
	<thead>
		<tr>
			<th><b>phr_Incentive_Statement_CampaignName</b></th>
			<th><b>phr_Incentive_Statement_CampaignID</b></th>
			<th><b>phr_Incentive_Statement_WeekEndAbbr phr_Date</b></th>
			<th><b>phr_Incentive_Statement_NumPoints</b></th>
		</tr>
		<tr></tr>
	</thead>
</cfoutput>
	<tbody>
<CFIF SpentPointsDetail.recordCount>
		<cfset localClass = "oddrow">
		<cfoutput query="SpentPointsDetail">
		<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
		<tr class="#localClass#">
		<!--- 	<td width="249">#SpentPointsDetail.notes#</td>
			<td width="125">#SpentPointsDetail.PtsSpentID#</td> --->
			<td data-title="phr_Incentive_Statement_CampaignName" ><a href="javascript:DisplayOrder(#SpentPointsDetail.OrderID#)">phr_Incentive_Statement_Title_#htmleditformat(RWTRansactionTypeID)#</a></td>
			<td data-title="phr_Incentive_Statement_CampaignID">#htmleditformat(SpentPointsDetail.OrderID)#</td>
			<td data-title="phr_Incentive_Statement_WeekEndAbbr phr_Date">#lsdateformat(SpentPointsDetail.weekendingdate,"dd-mmm")#</td>
			<td data-title="phr_Incentive_Statement_NumPoints">#lsnumberformat(SpentPointsDetail.Spent,",")#</td>
		</tr>
		</cfoutput>
<CFELSE>
	<cfset localClass="oddrow">
	<!--- <cfoutput>
		<tr>
			<td colspan="5" class="title">phr_incentive_200603_Statement_NoPointsSpentThisWeek</td>
		</tr>
	</cfoutput> --->
</CFIF>
	</tbody>
</table>

<cfoutput>
<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
	<span align="left" class="title"></span>

<cfif isdefined("printme")>
	<br>
	<a href="javascript: history.go(-1)">Back</a>
<cfelse>
	<SCRIPT type="text/javascript">
	function printMe()
	{
		var now = new Date()
		now = now.getTime()
		window.location.href = ('<cfoutput>#jsStringFormat(script_name)#?#jsStringFormat(request.query_string)#</cfoutput>' + '&printme=yes&c=' + now)
	}
	</script>

	<br>
	<!--- <a href="javascript: printMe()">Print</a> --->
</cfif>
</cfoutput>




</cf_translate>

<!--- reset the locale --->
<cfset tmp = setlocale(oldlocale)>
