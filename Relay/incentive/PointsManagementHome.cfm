<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		PointsManagementHome.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
23-Oct-2008			NJH			Bug Fix Trend Support Issue 992 - A large number of records (ie. 17000) were being returned, causing the page to time out
								I changed the filtering so that the date was based on the dateQueryWhereClause file and was not using the TFQO filter.
								The default filter for the registered date is the current month. I also added 'with (noLock)' in the view.
07-Jan-2009			NJH			Bug Fix Sony Issue 1573 - when no accounts were registered in the current month, TFQO didn't display and therefore the
								user was unable to change filters to see some data. TFQO now displays whether there is data or not.
12-05-2009			SSS			Changed monthasstring to dateformat as monthasstring is locale spcific so falls over as may is reported back as mai
28-01-2010			NJH			LID 3018 - changed dateformat for above change to just be month and year.
2012-10-04	WAB		Case 430963 Remove Excel Header 
Possible enhancements:


 --->



<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Incentive Accounts</cf_title>

<!---Initialize variables expected later in the code--->
<CFPARAM DEFAULT="Last" NAME="frmType"> 



	<SCRIPT>
		
<!---Make sure either the name of a company or the name of a person is entered when placing an order---> 
		function checkordering(){
		
		var form = window.document.form1
		var companyfield = form.frmCompany.value
		var namefield = form.frmName.value
		
		
		if (companyfield == '' && namefield == '') {
			alert("You must enter either a Company or a Person's Name")
			
		} 
		else form.submit()
		}
	</SCRIPT>
	
<!--- Search form --->
<CFIF IsDefined("frmSearch") and frmSearch eq "yes" >
	<TABLE align="center">
		<TR>
			<TD colspan="3" align="center"><H2>Find a Points Account</H2></TD>
		</TR>
					
		<cfoutput><FORM ACTION="#SCRIPT_NAME#?frmsearch=no" NAME="form1" METHOD="post"></cfoutput>
			<input type="hidden" name="frmSearch" value="no">
			<TR>
				<TD align="right">Company Name:</TD> 
				<TD colspan="2"><INPUT TYPE="text" NAME="frmCompany"></TD>
			</TR>
	 		<TR>
				<TD align="right">Person Name:</TD> 
				<TD colspan="2"><INPUT TYPE="text" NAME="frmName"><BR></TD>
			</TR>
			
			<TR>
				<TD colspan="3">
				
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="First" <CFIF frmType IS 'First'>Checked</CFIF>> First name
				&nbsp; &nbsp;
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="Last" <CFIF frmType IS 'Last'>Checked</CFIF>> Last name
				&nbsp; &nbsp;
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="Full" <CFIF frmType IS 'Full'>Checked</CFIF>> Full name
				
				</TD>
			</TR>
		
		
		
		<TR>
			<TD colspan="3" align="center"><A HREF="javascript:checkordering()"><CFOUTPUT><IMG SRC="/images/BUTTONS/c_search_e.gif" BORDER=0 ALT="Search"></CFOUTPUT></A></TD>
		</TR>
				
		</FORM>

</TABLE>
				
				
				
				

			

<CFELSE>

 <cfscript>
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayIncentive.disablePersonalAccount(listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>


<!---  get all the people or companies corresponding to the search criteria
and list them or, if a straight match, go directly to the points management screen--->

<!--- 	SELECT DISTINCT o.organisationName as account, o.organisationID, 
	c.countryid, ca.accountID, 
	CASE WHEN transactions > 0 THEN 'Active' ELSE '0 Transactions' END as statement,
	'Claim' as claim_points,

	LEFT( UPPER( o.organisationName ), 1 ) as alphabeticalIndex,

	<CFIF isDefined("frmName") and frmName IS NOT ''>Person.Firstname, Person.LastName, Person.PersonID,</CFIF>
			c.CountryDescription AS Country
	FROM organisation o 
		INNER JOIN RWCompanyAccount ca ON o.organisationID = ca.organisationID
		LEFT OUTER JOIN vRWTransactionCount rwt ON rwt.accountID = ca.accountID
		INNER JOIN Country c ON c.CountryID = o.CountryID
	<CFIF isDefined("frmName") and frmName IS NOT ''>
		INNER JOIN Person ON o.organisationID = p.organisationID
		INNER JOIN RWPersonalAccount ON p.PersonID = pa.personID
	</CFIF>
	WHERE 1=1

	<cfif isdefined( "form.frmAlphabeticalIndex" ) and form["frmAlphabeticalIndex"] neq "">
		AND LEFT( UPPER( o.organisationName ), 1 ) = '#form["frmAlphabeticalIndex"]#'
	</cfif>

	<CFIF isDefined("frmCompany") and frmCompany IS NOT ''>
		AND o.organisationName LIKE '#frmCompany#%'
	</CFIF>
	<CFIF isDefined("frmName") and frmName IS NOT ''>
		<CFSWITCH EXPRESSION="#FrmType#">
			<CFCASE VALUE="First">AND p.FirstName LIKE '#Trim(frmName)#%'</CFCASE>
			<CFCASE VALUE="Last">AND p.LastName LIKE '#Trim(frmName)#%'</CFCASE>
			<CFCASE VALUE="Full">AND p.FirstName+' '+p.LastName = '#frmName#'</CFCASE>
		</CFSWITCH>
	</CFIF> --->
	<cfparam name="sortorder" default = "organisation,account_holder">
	<cfparam name="FORM.Account_InActive" default="No">
	<cfparam name="startRow" default="1">
	<cfparam name="numRowsPerPage" default="100">

	<!--- NJH 2008/10/23 Trend support Bug Fix Issue 992. Changed the date filter to use dateQueryWhereClause rather than
	using a TFQO filter. Also added 'With (noLock)' on the view.
	 --->
<!--- 	<cfparam name="form.FRMWHERECLAUSEC" default="#left(monthAsString(datepart("m",now())),3)#-#datepart("yyyy",now())#">
	<cfparam name="form.FRMWHERECLAUSED" default="#left(monthAsString(datepart("m",now())),3)#-#datepart("yyyy",now())#">
 --->	
 	<!--- SSS 2009/05/12 bug fix 2186 START fixed a bug here monthAsString is locale spcific so falls over if you set your browser to french because may is reported
	back as mai --->
	<!--- NJH 2010/01/28 LID 3018 - changed date format from "dd-mmm-yyy" to the following below as we're only wanting a month and year for TFQO. --->
	<cfparam name="form.FRMWHERECLAUSEC" default="#dateformat(now(),'mmm-yyyy')#">
	<cfparam name="form.FRMWHERECLAUSED" default="#dateformat(now(),'mmm-yyyy')#">
<!--- SSS 2009/05/12 bug fix 2186 END --->
	
	<cfset tableName="vRWPersonAccounts">
	<cfset dateField="Date_Registered">
	<cfset dateRangeFieldName="Date_Registered">
	<cfparam name="useFullRange" default="false">

	<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT * from vRWPersonAccounts
		WHERE 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		and Account_InActive =  <cf_queryparam value="#FORM.Account_InActive#" CFSQLTYPE="CF_SQL_VARCHAR" >  
		<!--- 2012-07-20 PPB P-SMA001 		and countryID in (#request.relaycurrentuser.CountryList#) --->
		#application.com.rights.getRightsFilterWhereClause(entityType="RWPersonAccount",alias="vRWPersonAccounts").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->	
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>
	
	<!---Company matches--->
	<!--- 
		NJH 2009/01/07 Bug Fix Sony Issue 1573. A previous bug fix filtered the report to show only those accounts that registered in the current month
		by default. However, if there were no account, then TFQO didn't display to allow the user to change the filter values.
	<CFIF GetDetails.RecordCount GT 0> --->
		
		<cfinclude template="pointsManagementHomeFunctions.cfm">
			
		<CF_tableFromQueryObject queryObject="#getDetails#" 
			keyColumnList="account_holder,personal_account"
			keyColumnURLList="../data/dataFrame.cfm?frmsrchpersonID=,statement.cfm?statementType=personal&personID="
			keyColumnKeyList="personID,personid"
			keyColumnOpenInWindowList="yes,no"
			
			FilterSelectFieldList="ISOCode"
			<!--- FilterSelectFieldList2="ISOCode,Date_Registered" --->
			numRowsPerPage="#numRowsPerPage#"
			startRow="#startRow#"
			
			useInclude="true"
			
			radioFilterLabel="Show inactive accounts?"
			radioFilterDefault="No"
			radioFilterName="Account_InActive"
			radioFilterValues="No-Yes"
		
			dateFormat = "date_registered"
			hideTheseColumns="countryid,organisationID,personid,Account_InActive"
			sortOrder="#sortOrder#"
			alphabeticalIndexColumn="Account"
			openAsExcel="true"
			
			rowIdentityColumnName="personal_account"
			functionListQuery="#comTableFunction.qFunctionList#"
		>
<!---If no records found--->
	<!--- <CFELSEIF GetDetails.RecordCount EQ 0>
		No records matching the given criteria. It could be that a account has not yet been set for this Company.
	</CFIF> --->
</CFIF>