<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET pScope = "EnterPoints">  <!--- use pScope to group the phrases for translation --->
<cfparam name="ExpiryDate" default="#now()#">
<cfparam name="ThisWeek" default="#now()#">
<cfparam name="PersonID" default="100">
<cfparam name="RWInsertBasePoints__ExpiryDate" default="#ExpiryDate#">
<cfparam name="RWInsertBasePoints__ThisWeek" default="#ThisWeek#">
<cfparam name="RWInsertBasePoints__PersonID" default="#PersonID#">
<cfstoredproc procedure="dbo.RWInsertBasePoints" datasource="#application.SiteDataSource#">
  <!--- mm_procname="RWInsertBasePoints" --->
  <cfprocparam type="IN" dbvarname="@ExpiryDate" value="#RWInsertBasePoints__ExpiryDate#" cfsqltype="CF_SQL_TIMESTAMP">
  <cfprocparam type="IN" dbvarname="@ThisWeek" value="#RWInsertBasePoints__ThisWeek#" cfsqltype="CF_SQL_TIMESTAMP">
  <cfprocparam type="IN" dbvarname="@PersonID" value="#RWInsertBasePoints__PersonID#" cfsqltype="CF_SQL_INTEGER">
</cfstoredproc>
<cfparam name="FORM.CountryID" default="1">
<cfparam name="GetDistis__MMColParam" default="#FORM.CountryID#">
<cfquery name="GetDistis" DATASOURCE="#application.SiteDataSource#">
	SELECT LocationID, SiteName 
	FROM dbo.vDistiList 
	WHERE CountryID =  <cf_queryparam value="#GetDistis__MMColParam#" CFSQLTYPE="CF_SQL_INTEGER" >  
		ORDER BY SiteName ASC 
</cfquery>
<cfset RWInsertBasePoints__RETURN_VALUE=CFSTOREDPROC.STATUSCODE>

<CFINCLUDE TEMPLATE="../templates/qryGetDistributors.cfm">

<cf_head>
<cf_title><CFOUTPUT>phr_#htmleditformat(pScope)#_WindowTitle</CFOUTPUT></cf_title>

</cf_head>

<CF_Translate> <cf_displayBorder> 
<p><b><CFOUTPUT>phr_#htmleditformat(pScope)#_PageTitle</CFOUTPUT></b></p>
<form name="form1" method="post" action="">
  <table width="75%" border="1">
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>phr_#htmleditformat(pScope)#_PurchasedFrom</td>
      <td> 
        <select name="frmDistiID">
          <cfloop query="GetDistis">
            <option value="<cfoutput>#htmleditformat(GetDistis.LocationID)#</cfoutput>" ><cfoutput>#htmleditformat(GetDistis.SiteName)#</cfoutput></option>
          </cfloop>
        </select>
      </td>
    </tr>
    <tr> 
      <td>phr_#htmleditformat(pScope)#_ValueOfPurchase</td>
      <td>
        <input type="text" name="textfield">
      </td>
    </tr>
	    <tr> 
      <td>phr_#htmleditformat(pScope)#_DateOfPurchase</td>
      <td>
        <CF_INPUT type="text" name="#ThisWeek#">
      </td>
    </tr>

  </table>
</form>
</CF_DisplayBorder> </CF_Translate> 


