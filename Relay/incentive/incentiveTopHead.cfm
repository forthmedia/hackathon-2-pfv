<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="list">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<!--- NJH 2009/05/12 CR-SNY675-1 - added if statement. Did not need back for most reports. Not entirely sure this is where the back menu
		should be showing...Validate --->
	<cfif findNoCase("statement.cfm",SCRIPT_NAME,1)>
	<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:history.go(-#attributes.pagesBack#);">
	</cfif>

	<!--- <CF_RelayNavMenuItem MenuItemText="Liability Report" CFTemplate="liabilityReport.cfm"> 
	<CF_RelayNavMenuItem MenuItemText="Search" CFTemplate="pointsManagementHome.cfm?frmSearch=yes">--->
	<CFIF findNoCase("PointsManagementHome.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF> 
	
	<CFIF findNoCase("reportRWRegApproval.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF> 

	<!--- NJH P-TND065 --->
	<CFIF findNoCase("reportRWPointsExpire.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	
	<!--- NJH 2009/05/12 CR-SNY675-1 --->
	<CFIF findNoCase("validateIncentiveClaims.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>

	<!--- AS 2016/07/21 PROD2016-1468 --->
	<CFIF findNoCase("reportList.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>

</CF_RelayNavMenu>



