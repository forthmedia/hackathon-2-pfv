<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Incentive Accounts</cf_title>
	</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Points Claimed by Product Group</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWPersonalPointsClaimedByProductGroup.cfm?openAsExcel=yes" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>

<cf_translate>

	<cfparam name="sortorder" default = "date_claimed">
	<cfparam name="FORM.Account_InActive" default="No">

	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT * from vRWPersonalPointsClaimedByProductGroup
		WHERE 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>
	
	
	<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 0>
		
		<!--- <cfinclude template="pointsManagementHomeFunctions.cfm"> --->
			
		<CF_tableFromQueryObject queryObject="#getDetails#" 
			keyColumnList="claimed_by"
			keyColumnURLList="../data/dataFrame.cfm?frmsrchpersonID="
			keyColumnKeyList="personID"
			keyColumnOpenInWindowList="yes"
			
			FilterSelectFieldList = "ISOCode,month_claimed,Quarter_Claimed"
			FilterSelectFieldList2 = "ISOCode,month_claimed,Quarter_Claimed"
			numRowsPerPage="400"
			
			useInclude="true"
			
<!--- 			radioFilterLabel="Show inactive accounts?"
			radioFilterDefault="No"
			radioFilterName="Account_InActive"
			radioFilterValues="No-Yes"
--->		
			hideTheseColumns="personID,countryID"
			sortOrder="#sortOrder#"
			alphabeticalIndexColumn="Account"
			
			dateFormat="date_claimed"
			
			<!--- rowIdentityColumnName="personal_account"
			functionListQuery="#comTableFunction.qFunctionList#" --->
		>
	
<!---If no records found--->
<CFELSEIF GetDetails.RecordCount EQ 0>

	No records matching the given criteria. It could be that a account has not yet been set for this Company.

</CFIF>
	


</cf_translate>





