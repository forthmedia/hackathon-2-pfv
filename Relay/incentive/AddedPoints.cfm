<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Mods:

WAB 	2001-03-23		Alerted link to statement so that it passes the organisationid - needed for internal users using this page so that they get the correct statement


 --->

	<TABLE BORDER="0" WIDTH="440" CELLPADDING="2" CELLSPACING="0">
		<TR>
			<TD COLSPAN="3"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" WIDTH="445" HEIGHT="1" ALT="">
			</TD>
		</TR>
		<TR>
			<TD WIDTH="5"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" WIDTH="5" HEIGHT="1" ALT="">
			</TD>
			<TD WIDTH="435" VALIGN="top"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" WIDTH="125" HEIGHT="1" ALT=""><BR CLEAR="all">
			<H1>Your Completed Scorecard</H1>
			<P>These are the points you awarded for your previous month's purchases of #htmleditformat(application.client)# products and Key Partner activities.</P>
			<P>&nbsp;</P>
			
			<TABLE BORDER="0" WIDTH="435" CELLSPACING="0" CELLPADDING="0">
			<TR>
				<TD VALIGN="TOP">&nbsp;
				</TD>
				<TD VALIGN="TOP"><B>Purchases from (name of supplier/distributor):</B>
				</TD>
				<TD VALIGN="TOP"><B>Products purchased<BR>(EURO - &euro;):</B>
				</TD>
				<TD valign="TOP"><B>Date Purchased<BR></B>
				</TD>					
			</TR>
 			<CFLOOP INDEX="i" FROM="1" TO="5" STEP="1">
			<TR>
				<TD VALIGN="TOP"><P><B><CFOUTPUT>#htmleditformat(i)#.</CFOUTPUT></B></P></TD>
				<TD VALIGN="TOP">
				
					<CFQUERY NAME="GetAccount" DATASOURCE="#application.SiteDataSource#">
						SELECT accountId, organisationId 
						  FROM rwCompanyAccount rca
						 WHERE rca.organisationId =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				    </CFQUERY>
					
					<CFSET FRMSUPPLIER = "Form.frmSupplier#i#">		
					<CFSET frmSupplierID = #evaluate(frmSupplier)#>
					<CFQUERY NAME="GetOrganisation" DATASOURCE="#application.SiteDataSource#">
						SELECT organisationName
						  FROM organisation
						 WHERE organisationId =  <cf_queryparam value="#evaluate(frmsupplier)#" CFSQLTYPE="CF_SQL_INTEGER" >  
					</CFQUERY>					
 					<CFIF getOrganisation.RecordCount EQ 0>
						&nbsp;
					<CFELSE> 	
						<CFOUTPUT> #getOrganisation.organisationName# </CFOUTPUT>					
 					 </CFIF> 
				</TD>
				<TD VALIGN="TOP">
					<CFSET FRMAMOUNT = "Form.frmAmount#i#">
					<CFSET frmAmountPassed = #evaluate(frmAmount)#>
					<CFOUTPUT> #evaluate(frmAmount)# </CFOUTPUT>
				</TD>
				<TD VALIGN="TOP">
					<CFSET FRMDATE = "Form.frmDate#i#">
					<CFSET frmDatePassed = #evaluate(frmDate)#>					
 					<CFIF getOrganisation.RecordCount EQ 0>
						&nbsp;					
					<CFELSE>						
						<CFOUTPUT> #evaluate(frmDate)# </CFOUTPUT>
					</CFIF>
				</TD>				
			</TR>

		<!--- 	debug
		<cfoutput>
			exec RWAccruePoints
						@AccountID=#GetAccount.AccountId#,
 						@PtsAccID=0,
						@AccruedDate = '#frmDatePassed#',
						@PointsAmount = 0,
						@CashAmount= '#frmAmountPassed#', @PriceISOCode='EUR',
						@CurrencyPerPoint = '',
						@PersonID ='#request.relayCurrentUser.personid#',
						@ReasonID ='',
						@ReasonText = '',
						@Type = 'BP',
						@PPType  '',
						@DistiOrgID = '#frmSupplierId#'
			
			</cfoutput> --->

				<CFIF frmSupplierID GT 0> 
					<CFSTOREDPROC PROCEDURE="RWAccruePoints" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@AccountID" VALUE="#GetAccount.AccountId#" NULL="No">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PtsAccID" VALUE="" NULL="Yes">
  						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_TIMESTAMP" DBVARNAME="@AccruedDate" VALUE="#frmDatePassed#" NULL="No"> 
						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmount" VALUE="" NULL="Yes">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmount" VALUE="#frmAmountPassed#" NULL="No">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="EUR" NULL="No">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CurrencyPerPoint" VALUE="" NULL="Yes">
						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PersonID" VALUE="#request.relayCurrentUser.personid#" NULL="No">					
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ReasonID" VALUE="" NULL="Yes">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@ReasonText" VALUE="" NULL="Yes">
						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Type" VALUE="BP" NULL="No">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PPType" VALUE="" NULL="Yes">
 						<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@DistiOrgID" VALUE="#frmSupplierId#" NULL="No">
					</CFSTOREDPROC>				
				</cfif>
			</CFLOOP>

			</TABLE>	
			</TD>
		</TR>
	</TABLE>