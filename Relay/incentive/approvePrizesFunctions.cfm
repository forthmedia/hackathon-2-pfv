<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eServiceFunctions.cfm
Author:				KAP
Date started:		2003-09-03
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_prizes_functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_prizes_approval";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

//comTableFunction.functionName = "&nbsp;Opportunity Letter";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "/content/mergeDocs/MSWordLetterTemplate.cfm?personid=2&ignoreExtraneousParameter=";
//comTableFunction.windowFeatures = "width=1024,height=800,toolbar=1,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
//comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_prizes_approve_checkedClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "/incentive/ApproveOrderStatus.cfm?action=ApproveClaim&frmRowIdentity=";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_prizes_rejected_checkedClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "/incentive/RejectOrder.cfm?frmorderID=";
comTableFunction.windowFeatures = "width=1024,height=800,toolbar=1,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
  /*javascript:document.frmBogusForm.submit();*/
comTableFunction.functionListAddRow();
</cfscript>