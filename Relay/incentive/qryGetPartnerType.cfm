<!--- �Relayware. All Rights Reserved 2014 --->
<!--- identify what partnertype 

Mods:

2001-03-26 	WAB this used to work on the partnertype of the user - but it may be an internal 
			user working on a partner record
			need to get the partner type of the organisation. 	
			Unfortunately the flag is location based, so a bit of a hack!

--->

<cfquery name="PartnerType" DATASOURCE="#application.SiteDataSource#">
SELECT 
	f.FlagTextID,
	OrganisationID
FROM 
	Location as l 
	INNER JOIN BooleanFlagData as bfg ON l.LocationID = bfg.EntityID 
	INNER JOIN Flag as f ON bfg.FlagID = f.FlagID 
	INNER JOIN FlagGroup as fg ON f.FlagGroupID = fg.FlagGroupID
WHERE 
	l.organisationid =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and fg.FlagGroupTextID = 'PartnerType'
</cfquery>

<cfset PartnerType=PartnerType.FlagTextID>

