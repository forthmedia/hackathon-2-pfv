<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			validateIncentiveClaims.cfm	
Author:				
Date started:		
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-JUL-2008			NJH			Added functionality to show the items making up a transaction via an ajax call rather than
								opening the claim information up in a new window. This was part of CR-SNY644.
20-SEP-2008			AJC			CR-SNY657 Add function to extend verify claim
2012-03-26 			WAB 		Altered WebService functions to use returnFormat=json
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
<!--- YA 2009-07-20 P-SNY077 parameter 'countryScopeIncentiveRecords' added to control country scoping --->
<cfparam name="countryScopeIncentiveRecords" type="boolean" default="true">

<!--- get the users countries --->

<!--- YA 2009-07-20 P-SNY077 - commented out the bellow code and added new section below which checks 
	  for 'countryScopeIncentiveRecords' a more appropriately named paramater considering the location of this report.--->
<!--- <cfif isDefined("countryScopeOpportunityRecords") and countryScopeOpportunityRecords eq "1"> 
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif> ---> 

<cfparam name="sortOrder" default="created">
<cfparam name="numRowsPerPage" default="50"><!---=== set to 50 for LIVE ===--->
<cfparam name="startRow" default="1">
<cfif isdefined('URL.action') and URL.action eq "cancelClaim">
	<cfset FORM.verified = "Yes">
</cfif>
<cfparam name="FORM.verified" default="No">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="person_Name,_"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataFrame.cfm?frmsrchpersonID=, "> <!--- this can contain a list of matching columns that contain the URL of the editor template   incentivePointsClaimByProductFamily.cfm?readOnly=yes&rwtransactionid= --->
<cfparam name="keyColumnKeyList" type="string" default="personid, "><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="dateFormat" type="string" default="Contract_Ends"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->

<!--- NJH 2008-07-02 CR-SNY644 --->
<cfparam name="keyColumnOnClickList" type="string" default=" ,showTransactionItems(this*comma##rwtransactionid##);return false">
<!--- YA 2009-07-20 P-SNY077 - added country to columns list --->
<cfparam name="claimsReport_showTheseMasterColumns" type="string" default="Created,Credit_Amount,Organisation_Name,PersonID,Person_Name,Country,RWTransactionID,verified,_">
<!--- NJH 2009-05-12 CR-SNY675-1 - added child columns to show - for excel primarily --->
<cfparam name="claimsReport_showTheseChildColumns" type="string" default="Product,SerialNumber,InvoiceNumber,EndUserCompanyName,DiscountPrice,Quantity,Points">

<!--- <cfparam name="claimsReport_joinFlagColumns" type="string" default=""> <!--- a list of flagTextIDs --->
<cfparam name="claimsReport_showFlagColumns" type="string" default=""> --->
<cfset application.com.request.setTopHead(pageTitle = "phr_incentive_ValidateIncentiveClaims")>
<cf_title>Product Contracts</cf_title>

<cfif openAsExcel>
	<!--- NJH 2009-05-12 CR-SNY675-1 - remove the _ and append child columns to master columns --->
	<cfset claimsReport_showTheseMasterColumns = replace(claimsReport_showTheseMasterColumns,",_","")>
	<cfset claimsReport_showTheseMasterColumns = listAppend(claimsReport_showTheseMasterColumns,claimsReport_showTheseChildColumns)>
	<!--- PJP 18/12/2012: CASE 428582: If opening as Excel, make sure the verified url varible is assign to the form variable --->
	<cfif isdefined('url.verified')>
		<cfset FORM.verified = url.verified>
	</cfif>
<cfelse>

	<cfscript>
		if(structKeyExists(URL, "frmRowIdentity")){
			for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
				if (URL.action eq "verifyClaim") {
					v = application.com.relayIncentive.verifyClaim(listGetAt(URL.frmRowIdentity,x));
					// START: 2008-09-20 AJC CR-SNY567 Add function to extend verify claim
					if (structKeyExists(application.com.relayIncentive,"extendedVerifyClaim"))
					{
						e = application.com.relayIncentive.extendedVerifyClaim(listGetAt(URL.frmRowIdentity,x));
					}
					// END: 2008-09-20 AJC CR-SNY567 Add function to extend verify claim
				}
				else if (URL.action eq "cancelClaim") {
					c = application.com.relayIncentive.cancelClaim(listGetAt(URL.frmRowIdentity,x));
				}
			}
			// NJH 2008-12-09 CR-SNY671 clear the variables so that we don't re-verify claim when filters are being used.
			URL.frmRowIdentity = "";
			URL.action = "";
		}
	</cfscript>

</cfif>

<!--- NJH 2008-07-02 CR-SNY644 --->
<cfif fileexists("#application.paths.code#\cftemplates\validateIncentiveClaimsQry.cfm")>
	<!--- Client version of the query loads if there is one --->
	<cfinclude template ="/code/cftemplates/validateIncentiveClaimsQry.cfm">
<cfelse>
 
<!--- <cfquery name="vRWTransactionList" datasource="#application.SiteDataSource#">
	select v.*
		'<img src="/images/icons/bullet_arrow_down.png">' as _
		<cfloop list="#claimsReport_showTheseColumns#" index="columnName">
			<cfset booleanFlag = false>
			<cfset isFlagColumn = false>
			<cfloop list="#claimsReport_joinFlagColumns#" index="flagTextID">
				<cfif findNoCase(flagTextID,columnName)>
					<cfset isFlagColumn = true>
					<cfif application. flag[flagTextID].flagType.dataTableFullname eq "booleanFlagData">	
					,case when ft#columnName# is null then '' else 'Yes' end as #replace(columnName,".","_","ALL")#
					</cfif>
					<cfset booleanFlag=true>
					<cfbreak>
				</cfif>
			</cfloop> 
			<cfif not booleanFlag and isFlagColumn>
				,#columnName# as #replace(columnName,".","_","ALL")#
			<cfelseif not isFlagColumn>
				,v.#columnName#
			</cfif>
		</cfloop>
	from vRWTransactionList v
	<cfloop list="#claimsReport_joinFlagColumns#" index="flagTextID">
		left outer join (#application .flag[flagTextID].flagType.dataTableFullname# ft#flagTextID# inner join flag f#flagTextID# on ft#flagTextID#.flagID = f#flagTextID#.flagID and f#flagTextID#.flagTextID = '#flagTextID#')
			ON ft#flagTextID#.entityID = v.#application. flag[flagTextID].entityType.uniqueKey#
	</cfloop>
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	and verified = '#FORM.verified#'
	order by <cf_queryObjectName value="#sortOrder#">	
</cfquery> 

<!--- we're adding the dropdown icon to the end of the report --->
<cfset claimsReport_showTheseColumns = listAppend(claimsReport_showTheseColumns,"_")>
--->
<!--- YA 2009-07-20 P-SNY077 - added ''and vRWTransactionList.countryID in (#request.relayCurrentUser.countryList#)'' to restrict results by the users country rights --->

	<cfquery name="vRWTransactionList" datasource="#application.SiteDataSource#">
		select distinct #replace(claimsReport_showTheseMasterColumns,",_","")#
		<cfif not openAsExcel>
			,'<img src="/images/icons/bullet_arrow_down.png">' as _
		</cfif>
		from vRWTransactionList
		where 1=1
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		and verified =  <cf_queryparam value="#FORM.verified#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<cfif isDefined("countryScopeIncentiveRecords") and countryScopeIncentiveRecords> 
		<!--- 2012-07-26 PPB P-SMA001 commented out
		and vRWTransactionList.countryID in (#request.relayCurrentUser.countryList#)
		 --->
		#application.com.rights.getRightsFilterWhereClause(entityType="RWTransaction",alias="vRWTransactionList").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
		</cfif>
		order by <cf_queryObjectName value="#sortOrder#">	
	</cfquery>
	
</cfif>

<!--- NJH 2009-05-12 CR-SNY675-1 commented out. Moved to above and now showing excel link --->
<!--- <table width="100%" cellpadding="3">
	<tr>
		<td><strong>phr_incentive_ValidateIncentiveClaims</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2"></td></tr>
</table> --->
<cfinclude template="validateIncentiveFunctions.cfm">

<cfif not openAsExcel>
	
	
	<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
	
	<cfset columnToShowAjaxAt = listFindNoCase(vRWTransactionList.columnList,"rwtransactionID")>
	
	<script>
		<cfoutput>var columnToShowAjaxAt = #jsStringFormat(columnToShowAjaxAt)#</cfoutput>
		function showTransactionItems (obj,id) {
	
			currentRowObj = getParentOfParticularType (obj,'TR')
			currentTableObj = getParentOfParticularType (obj,'TABLE')
	
			totalColspan = 0
			for (i=0;i<currentRowObj.cells.length;i++) {
				totalColspan  += currentRowObj.cells[i].colSpan
			}
	
			infoRow =  id + '_info'
			infoRowObj = $(infoRow)
			
			
			if (!infoRowObj ) {
				page = '/webservices/relayIncentiveWS.cfc?wsdl&method=getRWTransactionItems&returnFormat=json'
				page = page + '&rwTransactionID='+id;
				<!--- NJH 2009-03-02 CR-SNY671 --->
				<cfif incentiveSettings.uploadClaimInvoice>
					<cfoutput>page = page + '&showInvoiceLink=true';</cfoutput>
				</cfif>
				<cfif not incentiveSettings.hideSerialNo>
					<cfoutput>page = page + '&showSerialNo=true';</cfoutput>
				</cfif>
	
				// changed to post to ensure proper refresh
				var myAjax = new Ajax.Request(
					page, 
					{
						method: 'post', 
						parameters: '', 
						evalJSON: 'force',
						debug : false,
						onComplete: function (requestObject,JSON) {
							json = requestObject.responseJSON
							rowStructure = new Array (2)
							rowStructure.row = new Array (1) 
							rowStructure.cells = new Array (1) 
							/*rowStructure.cells[0] = new Array (1)   // just trying indenting the result cell a bit
							rowStructure.cells[0].content  = ''
							rowStructure.cells[0].colspan  = 0//columnToShowAjaxAt -4
							rowStructure.cells[0].align  = 'right'*/
							rowStructure.cells[0] = new Array (1) 
							rowStructure.cells[0].content  = json.CONTENT 
							rowStructure.cells[0].colspan  = totalColspan //-rowStructure.cells[0].colspan
							rowStructure.cells[0].align  = 'right'
	
							regExp = new RegExp ('_info')
							deleteRowsByRegularExpression (currentTableObj,regExp)
							
							addRowToTable (currentTableObj,rowStructure,obj.parentNode.parentNode.rowIndex + 1,infoRow)
						
							}
						
						
					});
		
			} 
			else
			{
				regExp = new RegExp (infoRow)
				deleteRowsByRegularExpression (currentTableObj,regExp)
			}
			
			return 
		}
		
		function getParentOfParticularType (obj,tagName) {	
			if (obj.parentNode.tagName == tagName) 	{
				return obj.parentNode
			} else {
				return getParentOfParticularType (obj.parentNode,tagName)
			}
		
		}
	</script>
</cfif>

<CF_tableFromQueryObject
	queryObject="#vRWTransactionList#"
	queryName="vRWTransactionList"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
		
	useInclude="true"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOnClickList="#keyColumnOnClickList#"
	keyColumnOpenInWindowList="yes,no"
	FilterSelectFieldList="Person_Name,Country"
	FilterSelectFieldList2="Person_Name,Country"
	
	radioFilterLabel="Verified Only"
	radioFilterDefault="#FORM.verified#"
	radioFilterName="verified"
	radioFilterValues="No-Yes"
	
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	dateFormat="created"
	
	rowIdentityColumnName="RWTransactionID"
	functionListQuery="#comTableFunction.qFunctionList#"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_incentive_report_"
	showTheseColumns="#claimsReport_showTheseMasterColumns#"
>

<cfif not openAsExcel>
	<div id="dummy"></div>
</cfif>