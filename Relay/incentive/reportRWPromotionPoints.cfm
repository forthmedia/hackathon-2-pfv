<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<!--- 2005-07-08 - GCC - Extended due to timeouts on live Sony --->
<cfsetting enablecfoutputonly="No" requesttimeout="180">
<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Promotion Points Report</cf_title>
	</cf_head>

<cfparam name="sortOrder" default="Promotion">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="personid"><!--- this can contain a list of matching key columns e.g. primary key --->


<cfquery name="getClaims" datasource="#application.SiteDataSource#">
select OrgID, PersonID, TransactionID, Promotion, Product, Points, Country,
Company, Name, Created, countryid
from vRWPromotionPoints
	WHERE 1=1		
	<!--- 2012-07-26 PPB P-SMA001 commented out
	and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="RWTransaction",alias="vRWPromotionPoints").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by Promotion 
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Promotion Points Report</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWPromotionPoints.cfm?#queryString#&openAsExcel=true" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getClaims#"
	queryName="getClaims"
	sortOrder = "Promotion"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	numberFormat="Points"
	dateFormat="Created"
		
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
	showTheseColumns="Country,OrgID,Company,PersonID,Name,TransactionID,Promotion,Product,Points,Created"
 	hideTheseColumns="countryID"
	columnTranslation="False"
	
	FilterSelectFieldList="Country,Company,Name,Promotion,Product"
	FilterSelectFieldList2="Country,Company,Name,Promotion,Product"
	GroupByColumns="Promotion"
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	totalthesecolumns="Points"
	
	
	rowIdentityColumnName="PersonId"
>

</cf_translate>




