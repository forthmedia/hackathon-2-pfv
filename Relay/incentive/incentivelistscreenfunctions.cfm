<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			IncentiveListScreenFunctions.cfm
Author:				KAP
Date started:		06-Nov-2003

Description:			

Called by reportRWRegistrationApproval.cfm via tableFromQueryObject custom tag

Example of inclusion of extra logic in the tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Nov-2004			MDC			Initial version

Possible enhancements:

--->

<!--- --------------------------------------------------------------------- --->
<!--- optional function called by tableFromQueryObject custom tag --->
<!--- --------------------------------------------------------------------- --->
<cffunction name="udfTableCallBack" access="private" returntype="string" output="no">
	<cfargument name="udfColumnName" type="string" required="yes" />
	<cfargument name="udfColumnValue" type="string" required="yes" />
	<cfargument name="udfColumnType" type="string" required="yes" /> <!--- 0=unknown, 1=number, 2=currency, 3=date, 4=string --->

	<cfset udfReturnValue = "" />

	<cfswitch expression="#udfColumnName#">
	

	<cfcase value="calculated_budget">
		<!--- NOTE: the pricing_status is the holder column and then the oppPricingStatusID.current row is the logic check. --->
		<cfif listfind('3,4,5,6', attributes.queryObject["oppPricingStatusID"][attributes.queryObject.CurrentRow]) neq 0>
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/MISC/iconInformationRed.gif" alt="Required approval level: #attributes.queryObject["SPRequiredApprovalLevel"][attributes.queryObject.CurrentRow]# 
Current approval level: #attributes.queryObject["SPCurrentApprovalLevel"][attributes.queryObject.CurrentRow]#." width="14" height="14" border="0">' />
		<cfelseif attributes.queryObject["oppPricingStatusID"][attributes.queryObject.CurrentRow] eq "8">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/MISC/iconInformationApproved.gif" alt="Special pricing fully approved to level: #attributes.queryObject["SPCurrentApprovalLevel"][attributes.queryObject.CurrentRow]#" width="14" height="14" border="0">' />
		</cfif>
	</cfcase>

	
<!---  ---------------------------------------------------------------------  --->
	
<!--- --------------------------------------------------------------------- --->

	<cfdefaultcase>
		<cfreturn>
	</cfdefaultcase>
	
	</cfswitch>

	<cfreturn udfReturnValue />
</cffunction>
<!--- --------------------------------------------------------------------- --->

