<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		ApproveOrderStatus.cfm.cfm	
Author:			SSS
Date started:		2008/07/29
	
Description:		This enbales admin user to login and approve or reject large orders..

Usage:			

Amendment History:

Date (YYYY-MMM-DD)	Initials 	What was changed
2008-july-29		SSS			New email and email text for lexmark email def LoyaltyApprovedEmail
2009-aug-11			SSS			moved the email file into the cfif so the email only goes if the order is approved
2011/05/06			PPB			LID5378: switched request.setPendingStatus to setting incentive.pointsAboveWhichOrderConfirmationRequired and restricted list to use it 

Enhancements still to do:

 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Order Approvals</cf_title>


<cfparam name="sortOrder" default="created">
<cfparam name="numRowsPerPage" default="50"><!---=== set to 50 for LIVE ===--->
<cfparam name="startRow" default="1">
<cfif isdefined('URL.action') and URL.action eq "cancelClaim">
	<cfset FORM.verified = "Yes">
</cfif>
<cfparam name="FORM.verified" default="No">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="orderID"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../orders/vieworder.cfm?action=page&currentOrderID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="orderID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="dateFormat" type="string" default="Contract_Ends"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->


<cfif isdefined("URL.frmRowIdentity")>
	<cfloop index="orderID" list="#url.frmRowIdentity#" delimiters=",">
		<cfquery name="getorderAccount" DATASOURCE="#application.SiteDataSource#">
			select rwa.AccountID, ord.ordervalue, o.organisationname, o.organisationID, p.firstname, p.lastname, ord.personID
			, ord.shipper, ord.ShipperReference, o.countryID
			from orders ord inner join
			person p on ord.personID = p.personID inner join
			organisation o on o.organisationID = p.organisationID inner join
			RWCompanyAccount rwa on o.organisationID = rwa.organisationID
			where ord.orderID =  <cf_queryparam value="#orderID#" CFSQLTYPE="CF_SQL_INTEGER" >   
		</cfquery>
		<cfif isdefined('URL.action') and URL.action eq "ApproveClaim">
			<cfset approveprize = application.com.relayIncentive.ApprovePrize(orderID=orderID)>
		<!--- 2008/09/30 GCC moved out to a client specific file --->
			<!--- 2009/03/11 SSS moved the email file into the cfif so the email only goes if the order is approved --->
		<cfif fileexists("#application.paths.code#\cftemplates\PostClaimApprovalSubmit.cfm")>
			<cfinclude template="/code/cftemplates/PostClaimApprovalSubmit.cfm">
		</cfif>
		</cfif>
	</cfloop>
	<cflocation url="approveorderstatus.cfm"addToken="false">
</cfif>

<cfset pointsAboveWhichOrderConfirmationRequired = application.com.settings.getSetting('incentive.pointsAboveWhichOrderConfirmationRequired')>

<cfquery name="getOrderInfo" datasource="#application.SiteDataSource#">
	select * from (
		select o.orderID, o.ordervalue, p.firstname, p.LastName, organisation.organisationname, o.created
			from orders o inner join
			person p on o.PersonID = p.personID inner join
			organisation on p.organisationID = organisation.organisationID 
			inner join status s on o.orderstatus = s.statusid
		where s.statusTextID = 'Pending'
		) as base
	where 1=1 
	<cfif pointsAboveWhichOrderConfirmationRequired neq "">
		and ordervalue >  <cf_queryparam value="#application.com.settings.getSetting('incentive.pointsAboveWhichOrderConfirmationRequired')#" CFSQLTYPE="cf_sql_numeric" >
	<cfelse>
		and 1=0
	</cfif>
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>


<cfinclude template="approvePrizesFunctions.cfm">



<CF_tableFromQueryObject 
	queryObject="#getOrderInfo#"
	queryName="getOrderInfo"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	useInclude="true"
	numberFormat="ordervalue"
	showTheseColumns="orderID,ordervalue,firstname,lastname,organisationname"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnTargetList="_blank"
	keyColumnOpenInWindowList="no,yes"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	radioFilterValues="No-Yes"
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	dateFormat="created"
	rowIdentityColumnName="orderID"
	functionListQuery="#comTableFunction.qFunctionList#"
>
