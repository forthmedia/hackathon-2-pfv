<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			RejectOrder.cfm
Author:				SSS
Date started:		11/Jan/2008
	
Description:			

allows the etering of a reason for a order to be cancelled and the the points for the order to be refunded.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
11-Jan-2008			SSS			Initial version
2008-04-10			SSS			A email will now be sent when a order is rejected direct to the points administrator telling 			
								them the reason and the order number that has been rejected.

Possible enhancements:

To be able to refund part of the points not all the points
 --->
<cf_translate>

<cfset refundPromotionID = application.com.settings.getSetting("incentive.refundPromotion")>
<cfif refundPromotionID eq "">
	You must set up a refund promotion in settings.
	<CF_ABORT>
</cfif>

<cfif isdefined("CancelOrder")>
	<cfloop index="i" list="#FRMOrderID#" delimiters=",">
	
	<cfquery name="getorderAccount" DATASOURCE="#application.SiteDataSource#">
		select rwa.AccountID, ord.ordervalue, o.organisationname, o.organisationID, p.firstname, p.lastname, ord.personID
		from orders ord inner join
		person p on ord.personID = p.personID inner join
		organisation o on o.organisationID = p.organisationID inner join
		RWCompanyAccount rwa on o.organisationID = rwa.organisationID
		where ord.orderID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >   
	</cfquery>
	<cfquery name="GetDatabaseServerDate" DATASOURCE="#application.SiteDataSource#">
		SELECT CONVERT(VARCHAR(19), GETDATE(), 120) as AccruedDate
	</cfquery>
	
	<cfset getpointsadministrator = application.com.flag.getFlagData (flagid = "LoyaltyPointsAdministratorID", entityID = getorderAccount.organisationID)>
	<cfset emailtextid = "incentive_RejectOrder">
	<!--- using wills merge structure stuff may need testing to see if released to lexmark --->
	<cfscript>
		merge = StructNew();
		merge.frmReason = frmReason;
		merge.OrderFirstName = getorderAccount.firstname;
		merge.OrderLastName = getorderAccount.lastname;
		merge.orderID = i;
	</cfscript> 
	<!--- <cfset form.frmReason = frmReason>
	<cfset form.OrderFirstName = getorderAccount.firstname>
	<cfset form.OrderLastName = getorderAccount.lastname>
	<cfset form.orderID = i>  --->
	
	<cfset sendrejectedemail = application.com.email.sendEmail(emailTextID=emailtextid,personID=getpointsadministrator.data, cfmailtype="html",mergeStruct = merge)>

	<cfset Variables.pointsamount = getorderAccount.ordervalue>

		<cfset TransactionID = application.com.relayIncentive.RWAccruePoints(
																AccountId= getorderAccount.AccountID,
																AccruedDate= CreateODBCDateTime(GetDatabaseServerDate.AccruedDate),
																PointsAmount= Variables.pointsamount,
																PersonID= getorderAccount.personID)>
																
		<cfset ItemTransaction = application.com.relayIncentive.AddRWTransactionItems(
																RWTransactionID=transactionID,
                                                                ItemID=0,
                                                                quantity=1,
                                                                points= Variables.pointsamount,
                                                                endUserCompanyName=getorderAccount.organisationname,
                                                                pointsType='PP',
                                                                RWpromotionID=refundpromotionID)>
		
		<cfset totalPointsClaimed = application.com.relayIncentive.submitClaim(
                                                                RWTransactionID = transactionID,
                                                                RWTransactionTypeID = "AC",
                                                                distiID = 0)>
																
		<CFQUERY NAME="UpdateOrders" dataSource="#application.siteDataSource#">
		     update orders
			 	set approverNotes =  <cf_queryparam value="#FRMREASON#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					OrderStatus		= 5
		     WHERE orderID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   </CFQUERY>

	</cfloop>
	<script>
     window.opener.location.reload();
     window.close();  
   </script>
<cfelse>
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<cfoutput>
		<cfform method="post">
			<cf_relayFormDisplay>
				<CF_relayFormElementDisplay relayFormElementType="MESSAGE" currentValue="phr_prizes_reasonmessage" spanCols="yes" valueAlign="left">
				<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="frmReason" currentValue="" label="phr_Prizes_Reason" cols="35" rows="10" tabindex="1" required="yes">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#FRMOrderID#" fieldname="FRMOrderID">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="CancelOrder" currentValue="submitbutton" label="Phr_Prizes_RejectOrder" spanCols="No" valueAlign="left" class="button">
			</cf_relayFormDisplay>
		</cfform>
	</cfoutput>
</cfif>
</cf_translate>