<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportRWCountryLiabilities.cfm	
Author:				NJH
Date started:		2007-05-03
	
Description:		Displays a report of points earned/spent by country	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	 2008-07-18		NJH			Bugs - Trend Bug Fix Issue 814 - Included transactions that held points from the bringing over of the initial Advantage balances.


Possible enhancements:


 --->




<cf_head>
	<cf_title>RW Country Liablities Report</cf_title>
</cf_head>


<cf_translate>

<cfparam name="form.showcomplete" default="0">
<cfparam name="useFullRange" type="boolean" default="true">
<cfparam name="countryGroupTypeID" type="numeric" default="3">
<cfparam name="frmPriceISOcode" type="string" default="RWP">

<cfset frmCurrencyPerPoint = "1">
<cfset useDateRange ="true">
<cfset dateRangeFieldName="rwt.created">
<cfset dateField="created">
<cfset tableName="RWTransaction">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">

<cfquery name="getPointsConversion" datasource="#application.siteDataSource#">
	select currencyPerPoint, priceIsoCode
	from RWpointsConversion where currencyPerPoint is not null
</cfquery>

<cfquery name="getCurrencyPerPoint" datasource="#application.siteDataSource#">
	select case when currencyPerPoint is null then 1 else currencyPerPoint end as currencyPerPoint
	from RWpointsConversion where currencyPerPoint is not null
		and priceISOCode =  <cf_queryparam value="#frmPriceISOcode#" CFSQLTYPE="CF_SQL_VARCHAR" >  
</cfquery>

<cfset frmCurrencyPerPoint = getCurrencyPerPoint.currencyPerPoint>

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "showComplete", form.showComplete);
StructInsert(passthruVars, "frmPriceISOcode", frmPriceISOcode);
StructInsert(passthruVars, "useFullRange", useFullRange);
</cfscript>


<cfset request.relayFormDisplayStyle="HTML-table">
<cf_relayFormDisplay>
	<cfform  method="post" name="changeCurrencyForm">
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" spanCols="yes">
		phr_showAs:
		<cf_relayFormElement relayFormElementType="select" fieldname="frmPriceISOcode" label="phr_showAs" currentValue="#frmPriceISOcode#" query="#getPointsConversion#" display="priceISOcode" value="priceISOcode" onChange="javascript:form.submit();">
		</cf_relayFormElementDisplay>
		<!--- <cf_relayFormElement relayFormElementType="hidden" fieldname="frmPriceISOcode" label="" currentValue="#frmPriceISOcode#"> --->
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmWhereClauseA" label="" currentValue="#frmWhereClauseA#">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmWhereClauseB" label="" currentValue="#frmWhereClauseB#">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmWhereClauseC" label="" currentValue="#frmWhereClauseC#">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmWhereClauseD" label="" currentValue="#frmWhereClauseD#">
	</cfform>
</cf_relayFormDisplay>

<cfif frmPriceISOcode eq "EUR">
	<cfset x = setLocale("Dutch (standard)")>
<cfelseif frmPriceISOcode eq "GBP">
	<cfset x = setLocale("English (UK)")>
<cfelseif frmPriceISOcode eq "USD">
	<cfset x = setLocale("English (US)")>
</cfif>


<cfquery name="getCountryLiabilities" datasource="#application.siteDataSource#">
	select countryGroup,Country,SUM(Earned) as earned,SUM(Spent) as spent from (
	SELECT     cg1.CountryDescription AS CountryGroup, c.CountryDescription as country, 
					rwt.CreditAmount*#frmCurrencyPerPoint# AS earned, 
	                rwt.DebitAmount*#frmCurrencyPerPoint# AS spent
	FROM         RWTransaction rwt INNER JOIN
	                      RWCompanyAccount rwca ON rwt.AccountID = rwca.AccountID INNER JOIN
	                      organisation o ON rwca.OrganisationID = o.OrganisationID INNER JOIN
	                      Country c ON o.countryID = c.CountryID INNER JOIN
	                      CountryGroup cg ON c.CountryID = cg.CountryMemberID INNER JOIN
	                      Country cg1 ON cg.CountryGroupID = cg1.CountryID
	WHERE cg1.countryGroupTypeID =  <cf_queryparam value="#countryGroupTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<!--- NJH 2008-07-18 Bugs Trend Issue 814 Bug Fix  - include transactions created as part of the bringing over of the initial balances from Advantage --->
			/*and	rwt.rwTransactionID not in (select rwTransactionID from rwTransactionItems where rwPromotionID =6)*/
	<cfif isDefined("FRMWHERECLAUSEA") and FRMWHERECLAUSEA neq "">
		AND rtrim(datename(q, rwt.created)) = #right(listfirst(FRMWHERECLAUSEA,"-"),1)#
				and datepart(yy, rwt.created) = #listLast(FRMWHERECLAUSEA,"-")#
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED eq ""> 
		AND rwt.created >=  <cf_queryparam value="01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "" and isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC eq ""> 
		AND rwt.created < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq ""> 
		AND rwt.created >=  <cf_queryparam value="01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		AND rwt.created < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
	</cfif>
	
	) base
	where 1=1
	<cfif isDefined("FILTERSELECT1") and FILTERSELECT1 neq "" and isDefined("FILTERSELECTVALUES1") and FILTERSELECTVALUES1 neq "">
		AND #FilterSelect1# =  <cf_queryparam value="#FilterSelectValues1#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfif>
	GROUP BY CountryGroup, country
	ORDER BY CountryGroup, country 
</cfquery>

<cfset includeFileIncluded = true>	

<cfif frmPriceISOcode eq "RWP">
	<CF_tableFromQueryObject queryObject="#getCountryLiabilities#" 
		FilterSelectFieldList = "CountryGroup"
		showTheseColumns="country,earned,spent"
		sortOrder="country"
		totalTheseColumns="earned,spent"
		GroupByColumns="CountryGroup"
		queryWhereClauseStructure="#queryWhereClause#"
		passThroughVariablesStructure="#passThruVars#"
		numberFormat="earned,spent"
	>
<cfelse>
	<CF_tableFromQueryObject queryObject="#getCountryLiabilities#" 
		FilterSelectFieldList = "CountryGroup"
		showTheseColumns="country,earned,spent"
		sortOrder="country"
		totalTheseColumns="earned,spent"
		GroupByColumns="CountryGroup"
		queryWhereClauseStructure="#queryWhereClause#"
		passThroughVariablesStructure="#passThruVars#"
		currencyFormat="earned,spent"
	>
</cfif>

</cf_translate>



