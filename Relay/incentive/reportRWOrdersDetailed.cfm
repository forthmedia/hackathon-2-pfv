<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			orders_INI.cfm	
Author:				?????
Date started:		????
	
Description:		Orders Detail Report

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
NYB	25-09-2009	CR-SNY658 added orderCatalogueCampaignID variable (set in incentiveINI) and CampaignID where condition in query - was removed from View query
2012-10-04	WAB		Case 430963 Remove Excel Header 

--->


<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Orders Report</cf_title>
	</cf_head>
	
<!--- NJH 2007-05-30 look for a client version of the file. If it exists, include it. --->
<cfif fileExists("#application.paths.code#\cftemplates\reportRWOrdersDetailed.cfm")>
	<cfinclude template="/code/cftemplates/reportRWOrdersDetailed.cfm">
<cfelse>
	
<!--- 2012-07-26 PPB P-SMA001 commented out
<cfinclude template="/templates/qryGetCountries.cfm">
 --->
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="personid"><!--- this can contain a list of matching key columns e.g. primary key --->
	<!--- NYB 2008-09-25 CF-SNY658: --->
	<cfparam name="orderCatalogueCampaignID" type="string" default="5">
	<cfif ListLen(orderCatalogueCampaignID) gt 1>
		<cfparam name="sortOrder" default="Catalogue,OrderID desc">		
	<cfelse>
		<cfparam name="sortOrder" default="OrderID desc">		
	</cfif>



	<!--- NYB 2008-09-25 CF-SNY658  - replaced: ---
<cfquery name="getOrders" datasource="#application.SiteDataSource#">
	select AccountID, isnull(firstName,' ')+' '+ isnull(lastname,' ') as Name, Company, Country, OrderID, ProductGroup, Product, Quantity,
Total, Created, personid, email, locationid, countryid, orderstatus, status
from vIncentiveOrders
	WHERE 1=1		
	and countryid in (#countryList#)

	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>
	!--- NYB with: --->
	<cfquery name="getOrders" datasource="#application.SiteDataSource#">
		select * from (
			select <cfif ListLen(orderCatalogueCampaignID) gt 1>p.promoName as Catalogue, </cfif>
			vIO.AccountID, isnull(vIO.firstName,' ')+' '+ isnull(vIO.lastname,' ') as Name, vIO.Company, vIO.Country, vIO.OrderID, vIO.ProductGroup, vIO.Product, vIO.Quantity,
			vIO.Total, vIO.Created, vIO.personid, vIO.email, vIO.locationid, vIO.countryid, vIO.orderstatus, vIO.status
			from vIncentiveOrders vIO 
			<cfif ListLen(orderCatalogueCampaignID) gt 1>inner join promotion p on vIO.CampaignID=p.CampaignID</cfif> 
			WHERE vIO.CampaignID  in ( <cf_queryparam value="#orderCatalogueCampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )		
			<!--- 2012-07-26 PPB P-SMA001 commented out
			and vIO.countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="Orders",alias="vIO").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
			
		) as d 
		WHERE 1=1 
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#"> 
	</cfquery>
	<cfset showColumns="AccountID,Name,Email,Company,Country,OrderID,ProductGroup,Product,Quantity,Total,Created,Status">
	<cfset FilterSelectList="Country,Company,ProductGroup,Product,Status">
	<cfif ListLen(orderCatalogueCampaignID) gt 1>
		<cfset showColumns="Catalogue,#showColumns#">
		<cfset FilterSelectList="Catalogue,#FilterSelectList#">
	</cfif>
	<!--- <- NYB 2008-09-25 --->
<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Detailed Orders Report</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWOrdersDetailed.cfm?#queryString#&openAsExcel=true" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getOrders#"
	queryName="getOrders"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	numberFormat="Total"
	dateFormat="Created"
		
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
		showTheseColumns="#showColumns#"
 	hideTheseColumns="PersonID,locationid,countryid,orderstatus"
	columnTranslation="False"
	
		FilterSelectFieldList="#FilterSelectList#"
		FilterSelectFieldList2="#FilterSelectList#"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	totalthesecolumns="Total"
	
	
	rowIdentityColumnName="PersonId"
>

</cf_translate>
</cfif>




