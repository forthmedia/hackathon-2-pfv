<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eServiceFunctions.cfm
Author:				KAP
Date started:		2003-09-03
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_IncentiveFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_IncentiveClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Disable checked accounts";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:document.frmBogusForm.submit();";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();
</cfscript>

