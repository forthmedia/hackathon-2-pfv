<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			IncentiveGeneralPhrases.cfm
Author:			DJH
Date created:	2000-09-22

Description:	All the "General" Phrases to be used across the system

Version history:
1

--->

<!---Translations--->
<cfset GeneralPhrases = 
"Incentive_Intro_Header_Gold,
Incentive_Intro_Header_Silver,
Incentive_Intro_Header_Bronze,
Incentive_Intro_Title,
Incentive_General_MoreInfo,
Incentive_General_Register,
Incentive_General_UpdateRegister,
Incentive_General_AddPerson,
Incentive_ConfirmEntry,
Incentive_AllMandatory,
Incentive_General_SC,
Incentive_Participant_Subscriber,
Incentive_Participant_ProjCoord,
Incentive_Participant_MgmtContact,
Back,
Print,
ToContinue,
ClickHere,
AcceptTermsConditions,
ViewTermsConditions,
SubmitRegistration,
Name,
Company,
OR,
FirstName,
Surname,
Email,
Initial,
Optional,
Update,
SelectOne,
TermsConditions,
InformationTopics,
Location,
Date,
Description
">

<cfset JSPhrases=
"Incentive_JS_HowToPrint,
Incentive_JS_SCReg_PMNotSelected,
Incentive_JS_SCReg_PMAlreadySelected,
Incentive_JS_NomPM_NoChoice,
Incentive_JS_MustAcceptTermsConditions,
Incentive_JS_BadEmail,
Incentive_JS_AllMandatory,
Incentive_JS_TelNoRequired,
Incentive_JS_SearchPM_SurnameRequired
">

