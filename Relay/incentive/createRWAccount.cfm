<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

CreateRWAccount.cfm

Purpose:	To create a reward account
			Written to use with ProcessActions

Author:  WAB

Requires:
	frmOrganisationID
	(will also accept frmlocationID and work out the organisationid and set frmOrganisationid)
	
Returns:
	frmAccountID	
	
 --->
 
 
<!---  --->
<cfif isdefined("frmOrganisationid")>

<cfelseif isdefined("frmLocationID") or isdefined("frmPersonID")>
	 <cfquery name="getOrg" DATASOURCE="#application.SiteDataSource#">
		select organisationid
		from
		<cfif isdefined("frmLocationID")>
		location where locationid =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelseif isdefined("frmPersonID")>
		person where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 		
		</cfif>
	 </cfquery>
	<cfset frmOrganisationid = getOrg.organisationid>

<cfelse>
	<!--- parameter not defined --->
	
</cfif>

<!--- NJH 2010/03/30 P-LEX041 - replaced code with function below --->

 <!---  check whether account already exists --->
<!---  <cfquery name="check" DATASOURCE="#application.SiteDataSource#">
 	select top 1 accountID from rwcompanyAccount where organisationid = #frmOrganisationID# order by created desc
 </cfquery>
 

<CFPARAM name="frmStartDate" default="#now()#">
 
 <cfif check.recordCount is 0>
	 <cfquery name="insert" DATASOURCE="#application.SiteDataSource#">
		insert into rwCompanyAccount
			(OrganisationID, DateTermsAgreed, Created, CreatedBy,Updated,UpdatedBy, AccountDeleted,PointStartDate)
		values 		
			(#frmOrganisationID#, 
				#frmStartDate#, 
				getDate(), 
				#request.relayCurrentUser.personid#,
				getDate(), 
				#request.relayCurrentUser.personid#,
				0,
				#frmStartDate#)
	 </cfquery>

	  <cfquery name="check" DATASOURCE="#application.SiteDataSource#">
	  	select * from rwcompanyAccount where organisationid = #frmOrganisationID#
	  </cfquery>
<cfelse>

	<!--- NJH 2010/03/29 P-LEX041 - if an organisation already has an account that exists but it is deleted, then update the account --->
	 <cfquery name="unDeleteOrgAccount" datasource="#application.SiteDataSource#">
		 update rwCompanyAccount set AccountDeleted=0,
		 	Updated = #request.requestTime#,
		 	UpdatedBy = #request.relayCurrentUser.userGroupID#
		 where accountID=#check.accountID#
		 	and AccountDeleted = 1
	</cfquery>
</cfif>
 

<cfset frmAccountID = check.accountid > --->

<cfset frmAccountID = 0>
<cfif isDefined("frmOrganisationID")>
	<cfset frmAccountID = application.com.relayIncentive.createRWAccount(organisationID = frmOrganisationID)>
</cfif>
 
 
 
 
 
 
