<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		reportRWRegVClaim.cfm	
Author:			GCC
Date started:		2005-10-03
Description:		This provides a report of incentive account creation dates and dates of first product claim.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

03-Oct-2005			GCC			V1

Enhancements still to do:


 --->

<cfquery name="thisWeek" datasource="#application.sitedatasource#">
select datepart(week,getdate()) as weeknumber
</cfquery>
<!--- GCC tweaked to enable the reports to continue to roll into 2006 and beyond --->
<cfquery name="thisYear" datasource="#application.sitedatasource#">
select datepart(year,getdate()) as yearnumber
</cfquery>

<cfset startYear = 2004>
<cfset startWeek = 36>

<cfquery name="regDataByCountry" datasource="#application.sitedatasource#">
select CalendarYear, WeekOfCalendarYear, 
	weekcommencing as reportcolumns,
	country as reportrows,
	count(distinct people) as reportunits
				from calendarweek c
				left join
	(select 
			yearorder, weekorder, weekcommencing,
			country,
			people
		from vR_RewardsRegPeople_week
) a
on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
	where ((calendaryear =  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear >=  <cf_queryparam value="#startweek#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear >  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" > ))	
	AND (( calendaryear =  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear <  <cf_queryparam value="#thisweek.weeknumber+1#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear <  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" > ))
	group by CalendarYear, WeekOfCalendarYear, weekcommencing, country
					order by CalendarYear, WeekOfCalendarYear asc
</cfquery>


<cf_head>
<cf_title>Registrations</cf_title>
</cf_head>
<cfset nReport = CreateObject("component","report.reporting")>
<cfset nReport.userCalendar = "week">

<table cellpadding="5" border=0 width="100%">
	<tr>
 		<th>Incentive Registrations by week</th>
		<th>Incentive Registrations by week by country</th>
	</tr>
	<tr>
 	<td valign="top" align="left" width="300" nowrap>
		<cfset nReport.showCumulative = "yes">
		<cfset nFilter = "((yearorder =" & startYear & " and weekorder >=" & startweek & " ) OR (yearorder >" & startYear & " )) AND (( yearorder =" & thisYear.yearnumber & " and weekorder <" & thisweek.weeknumber+1 & " ) OR (yearorder <" & thisYear.yearnumber & " ))">
		<cfset w=nReport.commonColumnReport(commonColumnReport = 'RewardsRegistrationContacts',whereclause = nFilter,UseLiveCountryIDs = False, useCountryFilter = False)>		
	</td>
	<td valign="top" align="left">
		<cfset x=nReport.crossTabReport(regDataByCountry,'no','no','bottom','order by CalendarYear, WeekOfCalendarYear')>
	</td>
	</tr>
	<tr>
 		<th>Active Users by first claim week</th>
		<th>Active Users by first claim week by country</th>
	</tr>
	<tr>
<cfset iReport = CreateObject("component","report.reporting")>
<cfset iReport.userCalendar = "week">
<cfset startYear = 2005>
<cfset startWeek = 23>

<cfquery name="claimDataByCountry" datasource="#application.sitedatasource#">
select CalendarYear, WeekOfCalendarYear, 
	weekcommencing as reportcolumns,
	country as reportrows,
	count(distinct people) as reportunits
				from calendarweek c
				left join
	(select 
			yearorder, weekorder, weekcommencing,
			country,
			people
		from vR_RewardsClaimPeople_week
) a
on c.WeekOfCalendarYear = a.weekorder and c.Calendaryear = a.yearorder
	where ((calendaryear =  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear >=  <cf_queryparam value="#startweek#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear >  <cf_queryparam value="#startYear#" CFSQLTYPE="CF_SQL_Integer" > ))	
	AND (( calendaryear =  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" >  and weekofcalendaryear <  <cf_queryparam value="#thisweek.weeknumber+1#" CFSQLTYPE="CF_SQL_Integer" > )
	OR (calendaryear <  <cf_queryparam value="#thisYear.yearnumber#" CFSQLTYPE="CF_SQL_Integer" > ))
	group by CalendarYear, WeekOfCalendarYear, weekcommencing, country
					order by CalendarYear, WeekOfCalendarYear asc
</cfquery>
	 	<td valign="top" align="left" width="300" nowrap>
		<cfset iReport.showCumulative = "yes">
		<cfset nFilter = "((yearorder =" & startYear & " and weekorder >=" & startweek & " ) OR (yearorder >" & startYear & " )) AND (( yearorder =" & thisYear.yearnumber & " and weekorder <" & thisweek.weeknumber+1 & " ) OR (yearorder <" & thisYear.yearnumber & " ))">
		<cfset y=iReport.commonColumnReport(commonColumnReport = 'RewardsClaimsContacts',whereclause = nFilter,UseLiveCountryIDs = False, useCountryFilter = False)>		
	</td>	
	<td valign="top" align="left">
		<cfset z=iReport.crossTabReport(claimDataByCountry,'no','no','bottom','order by CalendarYear, WeekOfCalendarYear')>
	</td>
	</tr>
</table>


