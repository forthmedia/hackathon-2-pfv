<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
	
<cf_head>
	<cf_title>Incentive Balance</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Personal Balance</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="ReportPersonBalance.cfm?openAsExcel=yes" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>


<!--- get the users countries --->

<cfparam name="startrow" default="1">
<cfparam name="sortOrder" default="Country">
<cfparam name="numRowsPerPage" default="250">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="/data/dataFrame.cfm?frmsrchpersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="PersonID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">


<cfquery name="getPersonalBalance" datasource="#application.SiteDataSource#">
select 	Country, Company, Name, Balance, PersonID, OrgID
from	vIncentiveBalanceReport
where 	1=1
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by Country, Company, Name
</cfquery>


<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getPersonalBalance#"
	queryName="getPersonalBalance"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	numberFormat="Balance"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOpenInWindowList="yes"
	showTheseColumns="Country,Company,Name,Balance"
	hideTheseColumns="PersonID, OrgID"

	columnTranslation="no"
	FilterSelectFieldList="Country,Company"
	totalTheseColumns="Balance"
	GroupByColumns="country"
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"

>
</cf_translate>


