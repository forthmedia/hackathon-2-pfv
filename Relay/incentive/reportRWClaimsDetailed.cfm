<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<!--- 2005-07-08 - GCC - Extended due to timeouts on live Sony--->
<cfsetting enablecfoutputonly="No" requesttimeout="360">
<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Claims Report</cf_title>

<!--- 2012-07-26 PPB P-SMA001 commented out	
<cfinclude template="/templates/qryGetCountries.cfm">
--->
<cfparam name="sortOrder" default="Company">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="personid"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="ClaimReportDetailedStartDate" default="False">
<cfparam name="CRDStartDate" type="string" default="">

<cfquery name="getClaims" datasource="#application.SiteDataSource#">
select personid, Name, Company, locationid, ClaimID, Product, ProductGroup, Quantity, 
Points, Category, countryid, Country, Created, verified, Flagdata, AccountID, EndUser, Disti,
Flag, Invoice 
from vIncentiveClaims
	WHERE 1=1
	<!--- 2012-07-26 PPB P-SMA001 commented out		
	and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="RWTransaction",alias="vIncentiveClaims").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
	and pointstype not in ('pp')
<cfif isDefined("ClaimReportDetailedStartDate") and ClaimReportDetailedStartDate eq "True">
	and created >  <cf_queryparam value="#CRDStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</cfif>	
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Detailed Claims Report</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWClaimsDetailed.cfm?#queryString#&openAsExcel=true" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getClaims#"
	queryName="getClaims"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	numberFormat="Points"
	dateFormat="Created"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	showTheseColumns="AccountID,PersonID,Name,Company,Country,ClaimID,Category,Flag,ProductGroup,Product,Quantity,Points,EndUser,Disti,Invoice,Created,Verified,Flagdata"
 	hideTheseColumns="locationid,countryid"
	columnTranslation="False"
	FilterSelectFieldList="Country,Company,Category,ProductGroup,Product,Verified,EndUser,Disti,Flag"
	FilterSelectFieldList2="Country,Company,Category,ProductGroup,Product,Verified,EndUser,Disti,Flag"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	totalthesecolumns="Points"
	
	rowIdentityColumnName="PersonId"
>

</cf_translate>




