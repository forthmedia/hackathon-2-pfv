<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 


getAndCheckOrganisationID


this template either:

checks that the organisationid passed to the template is 
	either
		the organisation of the person accessing the system
	or
		the person accessing the system has rights to view all organisation accounts

or if no organisationid is passed
	sets the organisationid to the organisationid of the cookie

Author:  WAB


Date: 2001-03-23
	
	
 --->
 
 
 	<!--- Special code to allow this page to be called from the Viewer --->
	<CFIF isDefined ("frmEntityType")>
		<!--- get OrganisationID --->
		<cfquery name="getOrgID" DATASOURCE="#application.SiteDataSource#">	
		select organisationid
		from #frmEntityType#
		where #frmEntityType#id =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		
		<CFSET organisationid = getOrgId.organisationid>
	
	</cfif>
	
	<!--- If organisationID is passed into this Template then we need to check that 
	the person viewing the statment has rights to view this organisationid
	otherwise we just get organisationid from the person's cookie
	 --->
	<!--- ==============================================================================
      WARNING: IA HAVE HARD CODED  accessAnyAccount to make it work.  It needs re-working to
	  fit into the new security concepts.                                                  
=============================================================================== ---> 
	<cfset accessAnyAccount = 1>
	 
	<CFIF  isDefined("organisationid") and  accessAnyAccount is 1>
		<!--- No action required, this person can view any organisation's statement --->
	 
	<CFELSE>
	
		<!--- get the OrganisationID of the person accessing the system --->
		<cfquery name="getOrgID" DATASOURCE="#application.SiteDataSource#">
		select OrganisationID 
		from Person 
		Where personID = #request.relayCurrentUser.personid#
		</CFQUERY>
	
		<CFIF isDefined("organisationid") and checkPermission.accessAnyAccount is not 1>
			<!--- check that organisation from cookie is the same as id passed --->	
			<CFIF getOrgID.organisationid is not organisationid>
				<CFOUTPUT>Sorry you do not have rights to view Statements for this Organisation</cfoutput>
				<!--- Outside of borderset so can abort safely--->
				<CF_ABORT>
			</cfif>
	
	
		<CFELSE>
		
			<CFSET organisationid=getOrgId.organisationid>	
	
		</CFIF>
	
	</CFIF>

