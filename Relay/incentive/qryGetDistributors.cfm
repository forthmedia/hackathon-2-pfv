<!--- �Relayware. All Rights Reserved 2014 --->
<!--- qryGetDisttibutors.cfm will return a list of distis defined  in the database
	Parameters:
		frmCountryID (optional): this will scope the list to the coutries
		defined in the query.

Modified		
WAB 2001-03-23   mod so that the list of distibutors is related to the country of the organisation in question
		
CPS 07-Mar-01 Amend query to select distinct organisations from the view to ensure that 
              when adding points the correct organisationId is supplied to RWAccruePoints
			  rather than the locationId
--->


<CFPARAM NAME="frmCountryIDList" DEFAULT="">

<CFQUERY NAME="GetDistis" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT d.organisationId, d.organisationName,
<!--- 	                locationID, SiteName, ---> 
					d.CountryDescription, d.specificURL, d.telephone, d.Fax 
	FROM dbo.vDistiList d
		INNER JOIN countryGroup cg on d.countryid = cg.countrymemberid
		INNER JOIN organisation o on o.countryid = cg.countrygroupid
	where
		o.organisationid =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	Order by countryDescription, d.organisationName
</CFQUERY>
