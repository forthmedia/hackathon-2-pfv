<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Product Group Categories</cf_title>
	</cf_head>
	

<cfparam name="sortOrder" default="Category_Name">

<cfquery name="getNewRegistations" datasource="#application.SiteDataSource#">
select *
from vProductGroupCategories
	WHERE 1=1	
<!--- 2011-03-16 PPB commented out cos looks suspicious - where does countryList come from?; as its in "incentive" folder possibly should use setting countryScopeIncentiveRecords see validateIncentiveClaim
	<cfif isDefined("countryScopeOpportunityRecords") and countryScopeOpportunityRecords eq "1">
	and countryid in (#countryList#)
	</cfif>
 --->	
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Product Group Categories</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportProductGroupCategories.cfm?openAsExcel=yes" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getNewRegistations#"
	queryName="getNewRegistations"
	sortOrder ="#sortOrder#"
	useInclude="true"
	keyColumnList=""
	keyColumnURLList=""
	keyColumnKeyList=""
 	hideTheseColumns=""
	columnTranslation="false"
	FilterSelectFieldList="Category_Name,Description"
	FilterSelectFieldList2="Category_Name,Description"

	numRowsPerPage="400"
	allowColumnSorting="yes"
>
</cf_translate>



