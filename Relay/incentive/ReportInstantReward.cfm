<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Instant Reward Report</cf_title>
	</cf_head>
	

<cfparam name="sortOrder" default="RwPromotionID">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<!---
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">
--->
<cfparam name="dateFormat" type="string" default="Claimdate,Promotionstartdate,PromotionEnddate"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->

<cfquery name="GetInstantRewards" datasource="#application.SiteDataSource#">
select * from(
	select IR.Code, IR.value, IR.ClaimDate, IR.RWtransactionID, RWP.RWPromotionCode, IR.RWPromotionID, o.organisationname,
	       o.organisationID, p.personID,
       case when claimedby is null then ' ' else p.firstname + ' ' + p.lastname End as Name,
	   case when claimdate is null then 'Voucher Not Claimed' else 'Voucher Claimed' End As Status,
	   RWP.startdate as Promotionstartdate, RWP.Enddate as PromotionEnddate
	from InstantReward IR inner join
	RWPromotion RWP on IR.RWPromotionID = rwp.rwpromotionID left outer join
	person p on IR.ClaimedBY = p.personID left outer join
	organisation o on p.organisationID = o.organisationID ) as base
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">	
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Instant Reward Report</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="ReportInstantReward.cfm?#queryString#&openAsExcel=true" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#GetInstantRewards#"
	queryName="GetInstantRewards"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
 	<!--- hideTheseColumns="Personid,app" --->
	showTheseColumns="Code,Value,RWPromotionCode,Promotionstartdate,PromotionEnddate,Claimdate,RWtransactionID,personID,Name,organisationname,Status"
	keyColumnList="personID,Name,organisationname"
	keyColumnURLList="/data/dataframe.cfm?frmsrchpersonID=,/data/dataframe.cfm?frmsrchpersonID=,/data/dataframe.cfm?frmsrchOrgID="
	keyColumnKeyList="personID,personID,organisationID"
	FilterSelectFieldList="RWPromotionCode"
	FilterSelectFieldList2="Status"
	<!--- totalTheseColumns="NUMBER_OF_LOGINS" --->
	GroupByColumns="RWPromotionCode"
	allowColumnSorting="no"
	dateFormat="#dateFormat#"
>
</cf_translate>


