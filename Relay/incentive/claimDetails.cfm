<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Claim Details</cf_title>
</cf_head>


<cfif structKeyExists(URL,"rwTransactionID") and structKeyExists(URL,"validate") and URL.action eq "validate">
	<cfscript>
		application.com.relayIncentive.verifyClaim(URL.rwTransactionID);
	</cfscript>
</cfif>

<cfscript>
	transactionDetails = getTransactionDetails(URL.rwTransactionID);
	personDetails = application.com.commonQueries.getFullPersonDetails(transactionDetails.personID);
</cfscript>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Validate Claim</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="claimDetails.cfm?rwTransactionID=#URL.rwTransactionID#&action=validate" CLASS="Submenu">Save</a> | 
			<A HREF = "validateIncentiveClaims.cfm" CLASS="Submenu">Back</a>
		</TD>		
	</TR>
</TABLE>

<br><br>
<cfset url.readonly=1>
<cfinclude template="/incentive/incentivePointsClaimByProductFamily.cfm">


