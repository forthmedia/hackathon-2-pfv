<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NJH	2009/03/12	 Demo Bug Fix Issue 1967 - made the title a phrase
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cf_translate>

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Balance">
<cfparam name="numRowsPerPage" default="500">
<cfparam name="startRow" default="1">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">
<cfparam name="url.NextMonthDate" default="">
<cfparam name="url.FilterSelect2" default="">
<cfparam name="url.FilterSelectvalues2" type="string" default="">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfinclude template="reportRWPointsExpireFunctions.cfm">

<!--- ''create a date --->
<cfset CurrMonthDate = createdate(year(now()), month(now()), daysinmonth(now()))>
<!--- ''get the beginning of next month --->
<cfset CurrMonthDate = "#CreateODBCDateTime(CurrMonthDate + 1)#">
<!--- ''format to pass in as date type for object --->
<cfset NextMonthDate = createdate(year(CurrMonthDate), month(CurrMonthDate), 1)>

<!--- ''check to see if the records need to be filtered --->
<cfif isDefined("url.FilterSelect1")>
	<!--- ''filter records by user's selected date --->
	<cfset NextMonthDate = url.FilterSelect1>
</cfif>

<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	if (not isDefined("passThruVars")) {
		passThruVars = StructNew();
	}
	StructInsert(passthruVars, "NextMonthDate", url.NextMonthDate);
	StructInsert(passthruVars, "url.FilterSelect2", url.FilterSelect2);
	StructInsert(passthruVars, "url.FilterSelectvalues2", url.FilterSelectvalues2);

	objGetExpPoints = application.com.relayIncentive.getExpirePointsBeforeDate(expiryDate = NextMonthDate, returnPersonInfo = true);
</cfscript>

<cfquery name="objGetExpPoints" dbType="query">
	select *, countryDescription as country, balance as Points_To_Expire, OrganisationName as organisation_name from objGetExpPoints
	where 1=1
	and countryID in (#request.relayCurrentUser.countryList#)
	<!---	2012-07-24 PPB P-SMA001 getRightsFilterWhereClause() cannot be used in a QOQ (cos it uses an IN clause) so if more than simple country-scoping is required this should be moved into the main query	--->				 

	<cfif isDefined("url.FilterSelect2") AND isDefined("url.FilterSelectvalues2") AND isValid("integer", url.FilterSelectvalues2) AND url.FilterSelect2 GT 0>
		<cfif url.FilterSelect2 EQ 1>
			<!--- ''greater than --->
			 AND balance > #url.FilterSelectvalues2#	
		<cfelseif url.FilterSelect2 EQ 2>
			<!--- ''less than --->
			AND balance <= #url.FilterSelectvalues2# 
		</cfif>
	</cfif>
	order by balance desc
</cfquery>


	<cf_head>
		<cf_title>phr_incentive_incentivePointsExpiration</cf_title>
	</cf_head>
	
	<!--- <cfinclude template="IncentiveTopHead.cfm"> --->
	
	<!--- ''display --->
	<table width="100%" cellpadding="3">
		<tr>
			<td align="left" valign="top" class="submenu">phr_incentive_incentivePointsExpiration</td>
			<td align="right" valign="top" class="submenu">
				<cfoutput>
					<a href="reportRWPointsExpire.cfm?#queryString#&openAsExcel=true" class="submenu">Excel</a>
				</cfoutput>
			</td>
		</tr>
		<tr><td colspan="2">
			<p>Here you can create a selection of organisations with points that are due to expire at the end of the month shown in expiry date.
			<br />To filter the results by future months please select your required expiry date.</p>
		</td></tr>
	</table>

	
	<script language="JavaScript">
	/* A script to resubmit the form but introduce a date value to the query*/
	/* godfrey smith - Foundation Network 2008-03-07 */
	function filterByMonth(obj){
		document.frmMonthFilterCustom.NextMonthDate.value = obj.options[obj.selectedIndex].value;
		//alert(document.frmMonthFilterCustom.NextMonthDate.value);
		document.frmMonthFilterCustom.submit();
	}
	</script>

	<br />
	<!--- ''custom select filters for future months and point volume --->
	<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" class="withborder">
	
		<form name="frmMonthFilterCustom" id="frmMonthFilterCustom" action="reportRWPointsExpire.cfm" method="get">
			<cfoutput>
				<input type="hidden" name="NextMonthDate" id="NextMonthDate" value="#IIF(isDefined('url.FilterSelectvalues2'),Evaluate(DE('url.FilterSelectvalues2')),DE(''))#"> 
			</cfoutput>
		<tr>
			<td>
				Expire Date <select name="FilterSelect1" onChange="filterByMonth(this);"> 
				<cfset x = month(CurrMonthDate)>
				<cfset y = year(CurrMonthDate)>
				
				<cfloop index="i" from="1" to="12" step="1">
					<cfif x EQ 13><cfset x = 1><cfset y = y + 1></cfif>
					<cfset NextMonthDate = createdate(y, x, 1)>
					<cfoutput><option value="#NextMonthDate#"<cfif isDefined("url.NextMonthDate")AND url.NextMonthDate EQ NextMonthDate> selected</cfif>>#DateFormat(NextMonthDate,"medium")#</option></cfoutput><br />
					<cfset x = x + 1>
				</cfloop>
			</td>
		</tr>
		<tr>	
			<td align="left">Filter by 
			<select name="FilterSelect2">
				<option value="0">Select Points Filter</option>
				<option value="1"<cfif isDefined("url.FilterSelect2")AND url.FilterSelect2 EQ 1> selected</cfif>>Points Greater Than</option>
				<option value="2"<cfif isDefined("url.FilterSelect2")AND url.FilterSelect2 EQ 2> selected</cfif>>Points Less Than Or Equal To</option>
			</select>
			<cfoutput>
				<input type="text" name="FilterSelectvalues2" value="#IIF(isDefined('FilterSelectvalues2'),Evaluate(DE('FilterSelectvalues2')),DE(''))#" size="10" maxlength="15"> points.
			</cfoutput>
			</td>
			<td valign="top"> 
				<!--- Spacer --->
			</td>
			<td>
			<input type="button" name="GO" value="Go" onclick="filterByMonth(document.frmMonthFilterCustom.FilterSelect1);">
			</td>
		</tr>
			</select>
			</form> 
	</table>
	
	<br />
	

<!--- <cfset includeFileIncluded = true>	 --->
<!--- SSS 2009/03/31 took this cf_translate out to add one at the top of the file bug 1967 START--->
<!--- <cf_translate> --->

<CF_tableFromQueryObject 
	queryObject="#objGetExpPoints#"
	queryName="objGetExpPoints"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	passThroughVariablesStructure="#passThruVars#"
	startRow="#startRow#"
	keyColumnOpenInWindowList="yes"
	showTheseColumns="OrganisationID,Organisation_Name,Country,Person,PersonID,Points_To_Expire"
 	hideTheseColumns="accountID"
	columnTranslation="false"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="no"
	rowIdentityColumnName="PersonId"
	functionListQuery="#comTableFunction.qFunctionList#"
	useInclude="false"
	searchColumnList=""
	numberFormat="Points_To_Expire"
>
<!--- </cf_translate> --->
<!--- SSS 2009/03/31 took this cf_translate out to add one at the top of the file bug 1967 END--->
<cfif not openAsExcel>
	
	
	
</cfif>

</cf_translate>

