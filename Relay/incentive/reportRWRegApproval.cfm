<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		reportRWRegApproval.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Oct-2008			NJH			CR-SNY665 - add new columns to the report based on flags. The flag text ids are passed in as a param.
								The new created columns will be shown and will be added to the filter list. The report will display 'Yes' or 'No' depending
								on whether the flag is set for that entity or not.
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->


<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>New Registrations</cf_title>
	</cf_head>
	
	<cfinclude template="IncentiveTopHead.cfm">

<cfparam name="sortOrder" default="Reg_Date">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="personid"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="incentiveMemberFlagTextID" type="string" default="approvedIncentiveUser">
<cfparam name="incentiveRegFlagTextID" type="string" default="incetiveRegistration">
<cfparam name="emailTextID" type="string" default="incentiveUserNameEmail">

<!--- NJH 2008-10-28 CR-SNY665 - param that holds additional columns to show in the report based on flagTextIDs--->
<cfparam name="incentiveRegReportAdditionalFlagTextIDs" type="string" default="">


<!--- 2006-03-16 - GCC - CR_SNY588 Change Password --->
<cfif application.com.settings.getSetting("security.passwordGenerationMethod") eq "user">
	<cfset emailTextID = "incentiveUserNameLinkEmail">
</cfif>

<cfinclude template="reportRWRegApprovalFunctions.cfm">

	<cfscript>
		qGetEmailDetails = application.com.email.getEmailDetails(emailTextID);
	</cfscript>
	 
	<CFIF qGetEmailDetails.recordcount neq 1>
		
		<cfoutput><p>You must add an email to Workflow/Email with the textID of #htmleditformat(emailTextID)#</p></cfoutput>
		<CF_ABORT>
	</CFIF>

 <cfscript>
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			// writeoutput(listGetAt(URL.frmRowIdentity,x));
			application.com.flag.setBooleanFlag(listGetAt(URL.frmRowIdentity,x), incentiveMemberFlagTextID);
			incentiveAccountID = application.com.relayIncentive.createRWAccount(0,0,listGetAt(URL.frmRowIdentity,x));
			incentivePersonAccountID = application.com.relayIncentive.addPersonToRWAccount(incentiveAccountID, listGetAt(URL.frmRowIdentity,x));
			application.com.login.createUserNamePassword(listGetAt(URL.frmRowIdentity,x));
			application.com.email.sendEmail(emailTextID,listGetAt(URL.frmRowIdentity,x));
 					
			//2005-08-09 GCC Check for registration promotions - need persons countryID and their registration date
			getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(listGetAt(URL.frmRowIdentity,x));
			getRegistrationDetails = application.com.relayIncentive.getRegistrationDetails(personID = listGetAt(URL.frmRowIdentity,x), flagTextID = '#incentiveRegFlagTextID#');
			request.promotionDate = "registrationDate";
			application.com.relayIncentive.getActivePromotions(countryID = getFullPersonDetails.countryID, personID = getFullPersonDetails.personID, triggerPoint = 'registration',	nowDate = createODBCDateTime(getRegistrationDetails.regDate));
		}
 }
 </cfscript>

<!--- NJH 2008-10-28 CR-SNY665 - add some flag columns to the report. Show either a 'Yes' or 'No' if they have been set or not --->

<cfquery name="getNewRegistations" datasource="#application.SiteDataSource#">
	select * from (
select Name, Company, Country, Reg_Date, OrgID, personid, FlagData
		<cfloop list="#incentiveRegReportAdditionalFlagTextIDs#" index="flagTextID">
			, case when #flagTextID#.flagID is not null then 'Yes' else 'No' end as #replace(application.com.flag.getFlagStructure(flagTextID).translatedName," ","_","ALL")#
		</cfloop>
		from vIncentiveApprovals v
		<cfloop list="#incentiveRegReportAdditionalFlagTextIDs#" index="flagTextID">
			<cfset flagStructure = application.com.flag.getFlagStructure(flagTextID)>
			left join #flagStructure.flagType.dataTableFullName# #flagTextID# with (noLock) on #flagTextID#.entityID = v.#flagStructure.entityType.uniqueKey# and #flagTextID#.flagID =  <cf_queryparam value="#flagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfloop>
	) as base
	WHERE 1=1		
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>New Registrations</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">Here you can approve or reject new registrations for the rewards program</td></tr>
</table>
</cfoutput>

<!--- NJH 2008-10-28 CR-SNY665 - show the new columns and add them to the filter list --->
<cfset showTheseColumns = "Name,Company,Country,Reg_Date,FlagData">
<cfset filterList = "Country,Company">

<cfloop list="#incentiveRegReportAdditionalFlagTextIDs#" index="flagTextID">
	<cfset showTheseColumns = listAppend(showTheseColumns,replace(application.com.flag.getFlagStructure(flagTextID).translatedName," ","_","ALL"))>
	<cfset filterList = listAppend(filterList,replace(application.com.flag.getFlagStructure(flagTextID).translatedName," ","_","ALL"))>
</cfloop>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getNewRegistations#"
	queryName="getNewRegistations"
	sortOrder = "#sortOrder#"
	openAsExcel = "true"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOpenInWindowList="yes"
	showTheseColumns="#showTheseColumns#"
 	hideTheseColumns="orgID,PersonID"
	columnTranslation="true"
	FilterSelectFieldList="#filterList#"
	FilterSelectFieldList2="#filterList#"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	rowIdentityColumnName="PersonId"
	functionListQuery="#comTableFunction.qFunctionList#"
>
</cf_translate>



