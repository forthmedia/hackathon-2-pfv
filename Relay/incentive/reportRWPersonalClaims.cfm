<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportRWPersonalClaims.cfm	
Author:				
Date started:			/xx/07
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
30-JUN-2008			NJH			Made some changes so that the report could be included into a screen. (CR-SNY646)
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->


<cfparam name="openAsExcel" type="boolean" default="false">

<!--- NJH 2008-06-30 CR-SNY646 start--->
<cfparam name="showTheseColumns" type="string" default="SITENAME,CLAIMED_BY,DESCRIPTION,ISOCODE,DATE_CLAIMED,MONTH_CLAIMED,QUARTER_CLAIMED,RWTRANSACTIONID,POINTS">
<cfparam name="keyColumnList" type="string" default="claimed_by,RWTransactionID">
<cfparam name="keyColumnURLList" type="string" default="../data/dataFrame.cfm?frmsrchpersonID=,../incentive/incentivePointsClaimByProductFamily.cfm?readOnly=yes&rwtransactionid=">
<cfparam name="keyColumnKeyList" type="string" default="personID,RWTransactionID">
<cfparam name="keyColumnOpenInWindowList" type="string" default="yes,yes">
<cfparam name="FilterSelectFieldList" type="string" default="ISOCode,Month_Claimed,Quarter_Claimed,Sitename">
<cfparam name="FilterSelectFieldList2" type="string" default="ISOCode,Month_Claimed,Quarter_Claimed,Sitename">

<cfparam name="showExcelLink" type="boolean" default="true">
<cfparam name="frmEntityID" type="numeric" default=0>
<!--- NJH 2008-06-30 CR-SNY646 end--->

	<cf_head>
		<cf_title>Personal Claims</cf_title>
	</cf_head>
	
<cfset title="Accruals/Claims in the last 30 days">

<!--- NJH 2008-06-30 CR-SNY646 if we're showing claims for a person in a screen... --->
<cfif (isDefined("frmEntityID") and isDefined("frmEntityTypeID") and frmEntityID neq 0 and frmEntityTypeID eq 0) or (isDefined("frmPersonID") and frmPersonID neq 0)>
	<cfset title = "">
	<cfset keyColumnList="">
	<cfset keyColumnURLList="">
	<cfset keyColumnKeyList="">
	<cfset keyColumnOpenInWindowList = "">
	<cfset showTheseColumns="DESCRIPTION,DATE_CLAIMED,MONTH_CLAIMED,QUARTER_CLAIMED,RWTRANSACTIONID,POINTS">
</cfif>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu"><cfoutput>#htmleditformat(title)#</cfoutput></TD>
		<cfif showExcelLink>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWPersonalClaims.cfm?openAsExcel=yes" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
		</cfif>
	</TR>
</TABLE>

<cf_translate>

	<cfparam name="sortorder" default = "date_claimed">
	<cfparam name="FORM.Account_InActive" default="No">
	<cfparam name="startrow" default="1">
	
	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT * from vRWPersonalClaims
		WHERE 1=1
		<!--- NJH 2008-06-30 CR-SNY646 --->
		<cfif isDefined("frmEntityID") and isDefined("frmEntityTypeID") and frmEntityID neq 0 and frmEntityTypeID eq 0>
			and personID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelseif isDefined("frmPersonID") and frmPersonID neq 0>
			and personID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelse>
		and dateDiff(day, date_claimed,getDate()) < 30 
		</cfif>
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>
	
	
	<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 0>
		
		<!--- <cfinclude template="pointsManagementHomeFunctions.cfm"> --->
			
		<CF_tableFromQueryObject queryObject="#getDetails#" 
			keyColumnList="#keyColumnList#"
			keyColumnURLList="#keyColumnURLList#"
			keyColumnKeyList="#keyColumnKeyList#"
			keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"
			startrow="#startrow#"
			FilterSelectFieldList = "#FilterSelectFieldList#"
			FilterSelectFieldList2 = "#FilterSelectFieldList2#"
			numRowsPerPage="600"
			
			useInclude="true"
			
			showTheseColumns="#showTheseColumns#"
			hideTheseColumns="personID,countryID"
			sortOrder="#sortOrder#"
			alphabeticalIndexColumn="Account"
			numberFormat="points"
			dateFormat="date_claimed"
			totalTheseColumns="points"
			groupByColumns="rwTransactionID"
			
			<!--- rowIdentityColumnName="personal_account"
			functionListQuery="#comTableFunction.qFunctionList#" --->
		>
	
<!---If no records found--->
<CFELSEIF GetDetails.RecordCount EQ 0>

	No records matching the given criteria. It could be that a account has not yet been set for this Company.

</CFIF>
	


</cf_translate>





