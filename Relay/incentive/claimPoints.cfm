<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Amendment History:

07-Mar-2001	CPS 	Ensure that organisationId and OrganisationName are displayed for 
					the supplier/Distributor as the procedure RWAccruePoints expect 
					the organisationId to be passed in 
2004-08-07	SWJ		Modified to call incentivePointsClaimByAmount by default
--->





<script>
	registeredQry = application.relayIncentive.isPersonRegistered(request.relayCurrentUser.personid);
</script>

<cfif not registeredQry.recordCount>
	<script>
		alert("You must be registered with the incentive program in order to claim points!");
	</script>
	<CF_ABORT>
</cfif>


<cf_head>
<cf_title>Claim Points</cf_title>
</cf_head>

<CFIF request.relayCurrentUser.isInternal >  <!--- WAB 2006-01-25 Remove form.userType     <CFIF UserType is "internal">--->
	<cfinclude template="incentiveTopHead.cfm">
</cfif>

<cfif URL.methodToClaimIncentivePoints eq "amount">
	<cfinclude template="incentivePointsClaimByAmount.cfm">
</cfif>

<cfif URL.methodToClaimIncentivePoints eq "Product">
	<cfinclude template="incentivePointsClaimByProduct.cfm">
</cfif>



