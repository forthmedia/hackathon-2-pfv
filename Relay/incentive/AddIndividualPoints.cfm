<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

AddIndividualPoints.cfm


Author:  Chris Snape

Purpose:	Created to be used by processActions to add points to an account

Parameters:
frmOrganisationID
frmRWPromoID  (Optional)  RWPromotion  - If not set then assumed manual points
frmPointsAmount  (optional) amount of points
frmCashAmount	(optional) amount of cash
frmCurrency		required with frmCashAmount	- currency
(note one of frmPointsAmount or frmCashAmount is required)
frmDatePassed (optional) accrual date

--->


 
<CFQUERY NAME="GetAccount" DATASOURCE="#application.SiteDataSource#">
	SELECT accountId, organisationId , getdate() as date   <!--- getting the date here from sql server incase the  apps server has its time ahead of the sqlserver--->
	  FROM rwCompanyAccount rca
	 WHERE rca.organisationId =  <cf_queryparam value="#frmOrganisationId#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
					

<CFIF getAccount.RecordCount EQ 0>

	An Account does not exist for this Organisation <BR>
	No Points added

<cfelse>


	
	<CFIF IsDefined("frmRWPromoID")> 
		<CFSET PointsType = 'PP'>
	<CFELSE>
		<CFSET PointsType = 'MP'>	
	</CFIF>
	
	<CFPARAM name="frmDatePassed"  default="#GetAccount.date#">
	

<CFSTOREDPROC PROCEDURE="RWAccruePoints" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@AccountID" VALUE="#GetAccount.AccountId#" NULL="No">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PtsAccID" VALUE="" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_TIMESTAMP" DBVARNAME="@AccruedDate" VALUE="#frmDatePassed#" NULL="No"> 
			
		<CFIF IsDefined("frmPointsAmount")> 
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmount" VALUE="#frmPointsAmount#" NULL="No">
		<CFELSE>		
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmount" VALUE="" NULL="Yes">	
		</CFIF>		
		<CFIF IsDefined("frmCashAmount")>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmount" VALUE="#frmCashAmount#" NULL="No">
		<CFELSE>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmount" VALUE="" NULL="Yes">		
		</CFIF>	
		<CFIF IsDefined("frmCurrency")>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="#frmCurrency#" NULL="No">
		<CFELSE>		
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="" NULL="Yes">	
		</CFIF>
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CurrencyPerPoint" VALUE="" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PersonID" VALUE="#request.relayCurrentUser.personid#" NULL="No">					
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ReasonID" VALUE="" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@ReasonText" VALUE="" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Type" VALUE="#PointsType#" NULL="No">
		<CFIF IsDefined("frmRWPromoID")>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PPType" VALUE="#frmRWPromoID#" NULL="No">
		<cfelse>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PPType" VALUE="" NULL="Yes">
		</cfif>
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@DistiOrgID" VALUE="" NULL="Yes">

		<CFPROCRESULT NAME="debug" RESULTSET="1"> 

	</CFSTOREDPROC>		 
	


</CFIF>	
