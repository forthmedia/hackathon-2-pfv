<!--- �Relayware. All Rights Reserved 2014 --->
<cfset incentiveSettings = application.com.settings.getSetting("incentive")>
<!--- WAB 2010/10/20 all replaced with settings
<!---
File name:			incentiveParams.cfm
Author:				SWJ
Date started:		2004-04-29

Purpose:	central storage for incentive params

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfparam name="incentiveSystemName" type="string" default="Incentive">
<cfparam name="promoid" type="string" default="incentiveCatalogue">

<!--- methodToClaim IncentivePoints defines the default way you can claim points
		in the system. 2 methods of methodToClaim IncentivePoints are currently supported
		amount or product.  See claimPoints.cfm for more details.
--->
<cfparam name="methodToClaim IncentivePoints" default="product">

<!--- this variable controls whether we see the distributor in claim screens
--->
<cfparam name="incentiveClaimsUseDistributor" default="yes">

<!---THIS IS THE PRODUCT CATALOGUE OF THE PRODUCTS FOR CLAIMING POINTS--->
<cfparam name="defaultClaimsProductCatalogueCampaignID" default="4">

<!---THIS IS THE PRODUCT CATALOGUE OF THE PRODUCTS FOR INCENTIVE PRIZES--->
<cfparam name="defaultPrizesProduct CatalogueCampaignID" default="5">

<cfparam name="showNoOfUsers" default="0"> <!--- NJH 2008/03/05 P-TND065 shows the number of users column in the company statement--->

<cfparam name="ProgChampTextID" type="string" default="KeyContacts Primary"> <!--- NJH 2008/07/14 Bug Fix T-10 534 added param for flag textID for incentive points administrator   --->

<cfparam name="uploadClaimInvoice" type="boolean" default="false"> <!--- NJH 2009/03/02 CR-SNY671 - switch determining whether invoices are being uploaded for claims --->

<cfparam name="hideSerialNo" type="boolean" default="false"> <!--- NJH 2009/03/02 CR-SNY671 - show the serial number or not when creating/viewing claims --->

<!--- Added here during race --->
<cf_include template="\code\cftemplates\incentiveINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\incentiveINI.cfm")>
	<!--- incentiveINI can be used to over-ride default
		global application variables and params defined in incentiveParams.cfm --->
	<cfinclude template="/code/cftemplates/incentiveINI.cfm">
</cfif> --->
--->
