<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Amendment History:

07-Mar-2001	CPS 	Ensure that organisationId and OrganisationName are displayed for 
					the supplier/Distributor as the procedure RWAccruePoints expect 
					the organisationId to be passed in 

--->


<CFINCLUDE template="qryGetDistributors.cfm">






<!-- saved from url=(0022)http://internet.e-mail -->


<cf_head>
<cf_title>Add Points</cf_title>
</cf_head>

<CFIF UserType is "internal">
	<cfinclude template="incentiveTopHead.cfm">
</cfif>


<cf_displayBorder>

<SCRIPT LANGUAGE="JavaScript">

<!--

function verifyForm() {
	var form = document.addpoints
		msg = ''
		NoAmount = false
		NoSupplier = false			
		
	//check that the declare checkbox has been checked
	if (!form.DeclareCorrect.checked)
	{
		msg = 'You must declare that the given details are accurate and correct before continuing\n'
	}

	//check that at least one supplier has been specified
	if ((form.frmSupplier1.selectedIndex + form.frmSupplier2.selectedIndex + form.frmSupplier3.selectedIndex + form.frmSupplier4.selectedIndex + form.frmSupplier5.selectedIndex) == 0)
	{
		msg += 'At least one supplier must be specified\n'
	}
	
	for (i=1; i<6; i++)
	{
		//check that for every supplier entered a corresponding amount is declared	
		if (eval('form.frmSupplier'+i+'.selectedIndex') != 0 && eval('form.frmAmount'+i+'.value') == '')
		{
			NoAmount = true					
		}	

		//check that for every amount entered a corresponding supplier is declared			
		if (eval('form.frmSupplier'+i+'.selectedIndex') == 0 && eval('form.frmAmount'+i+'.value') != '' && eval('form.frmAmount'+i+'.value') != 0)
		{
			NoSupplier = true					
		}		
	}    

	if (NoAmount)
	{
		msg += 'An amount must be specified where a supplier is set\n'		
	}	

	if (NoSupplier)
	{
		msg += 'A supplier must be specified where an amount is set\n'		
	}
	
	if (msg != '') 
	{
		alert (msg);
	}
	else 
	{
		//Don't seem to be any errors on the form
		form.submit();
	}
}			

function doPageTotal() 
{
	// function to add up all the points and display them on the screen
	var form = document.addpoints
		tot = 0
	for (i=1; i<6; i++)
	{
		if (eval('form.frmAmount'+i+'.value') != '')
		{
			tot = parseInt(eval('form.frmAmount'+i+'.value')) + parseInt(tot)
		}	
	}    
	
	form.KingstonSalesAmount.value = tot;
}

function alertDateChecked(afield)
{
	// function to display an alert box if the date is invalid
	if (!checkEuroDate(afield.value))
	{
		alert('Invalid Date')
		afield.focus()
		afield.select()
		return false
	}
}

function checkEuroDate(theDate) 
{
    if (theDate.length == 0)
        return true;
	isplit = theDate.indexOf('-');

	if (isplit == -1 || isplit == theDate.length)
		return false;
    sDay = theDate.substring(0, isplit);
	monthSplit = isplit + 1;
	isplit = theDate.indexOf('-', monthSplit);

	if (isplit == -1 || (isplit + 1 ) == theDate.length)
		return false;
    sMonth = theDate.substring((sDay.length + 1), isplit);
	sYear = theDate.substring(isplit + 1);
	sMonth = checkMonth(sMonth);

	if (!numberRange(sMonth, 1, 12)) // check month
		return false;
	else
	if (!checkInteger(sYear)) //check year
		return false;
	else
	if (!numberRange(sYear, 0, null)) //check year
		return false;
	else
	if (!checkInteger(sDay)) //check day
		return false;
	else
	if (!checkDay(sYear, sMonth, sDay)) //check day
		return false;
	else
		return true;
}

function checkMonth(theMonth) {
	var monthList1 = new Array ("jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
	var returnMonth = -1;
	for(var i = 0; i < 12; i++) {
		if(monthList1[i] == theMonth.toLowerCase()) {
			returnMonth = i+1;
		}
	}
	return returnMonth;
}

function checkDay(checkYear, checkMonth, checkDay) {
	maxDay = 31;
	if (checkMonth == 4 || checkMonth == 6 ||
			checkMonth == 9 || checkMonth == 11)
		maxDay = 30;
	else
	if (checkMonth == 2) {
		if (checkYear % 4 > 0)
			maxDay =28;
		else
		if (checkYear % 100 == 0 && checkYear % 400 > 0)
			maxDay = 28;
		else
			maxDay = 29;
	}
	return numberRange(checkDay, 1, maxDay);
}

function checkInteger(checkThis) {
	var newLength = checkThis.length
	for(var i = 0; i != newLength; i++) {
		aChar = checkThis.substring(i,i+1)
		if(aChar < "0" || aChar > "9") {
			return false
		}
	}
	return true
}

function numberRange(object_value, min_value, max_value) {
    if (min_value != null) {
        if (object_value < min_value)
		return false;
	}

    if (max_value != null) {
		if (object_value > max_value)
		return false;
	}
    return true;
}

//-->

</SCRIPT>
<cfoutput>
	<TABLE border="0" width="440" cellpadding="2" cellspacing="0">
		<TR>
			<TD colspan="3"><IMG src="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" width="445" height="1" alt="">
			</TD>
		</TR>
		<TR>
			<TD width="5"><IMG src="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" width="5" height="1" alt="">
			</TD>
			<TD width="435" valign="top"><IMG src="<CFOUTPUT></CFOUTPUT>/images/MISC/blank.gif" width="125" height="1" alt=""><BR clear="all">
 			<H2>Sales Claim Form</H2>
			<P>Please complete this form to ensure that you are awarded your points for your 
			previous month's purchases and Partner activities.</P>
 			<P>&nbsp;</P>  
				<FORM name="addpoints" method="post" action="/incentive/addedpoints.cfm">			
					<CF_INPUT type="hidden" name="organisationid" value = "#organisationid#">
<!--- 				<FORM name="keypartner_scorecard" method="post" action="pointformreg.asp"> --->
					<TABLE border="0" width="435" cellspacing="0" cellpadding="0">
						<TR>
							<TD valign="TOP">&nbsp;
							</TD>
							<TD valign="TOP"><B>Purchases from (name of supplier/distributor):</B>
							</TD>
							<TD valign="TOP"><B>Product purchased<BR>(EURO - &euro;):</B>
							</TD>
							<TD valign="TOP"><B>Date Purchased<BR></B>
							</TD>							
						</TR>
						
 						<CFLOOP INDEX="i" FROM="1" TO="5" STEP="1">
						<TR>
							<TD VALIGN="TOP"><P><B>#htmleditformat(i)#.</B></P></TD>
							<TD VALIGN="TOP">
								<SELECT NAME="frmSupplier#i#"> 	
									<OPTION VALUE="0">Please specify distributor</OPTION>
									<CFLOOP QUERY="GetDistis">
										<OPTION VALUE="#OrganisationID#">#htmleditformat(OrganisationName)#</OPTION>
									</CFLOOP>
								</SELECT>								
							</TD>
 							<TD VALIGN="TOP"><CF_INPUT TYPE="Text" NAME="frmAmount#i#" SIZE="15" MAXLENGTH="40" ONBLUR="Javascript:doPageTotal()"></TD>
 							<TD VALIGN="TOP"><CF_INPUT TYPE="Text" NAME="frmDate#i#" SIZE="15" MAXLENGTH="40" VALUE="#DateFormat(now())#" ONBLUR="alertDateChecked(this)"></TD>
						</TR>
						</CFLOOP> 
						<TR>	
						<TD COLSPAN="2">&nbsp;</TD>	
						<TD>		
							<INPUT type="Text" name="KingstonSalesAmount" size="15" maxlength="40" DISABLED>						
						</TD>						
						<TD VALIGN="middle">
							&euro; (EURO). 
						</TD>
						</TR>

					</TABLE>
<!--- 					<INPUT type="Text" name="Month" size="10" maxlength="20" value="month">&nbsp;
					<INPUT type="Text" name="Year" size="10" maxlength="20" value="year">the purchase of Kingston memory products amounted to 
					<INPUT type="Text" name="KingstonSalesAmount" size="10" maxlength="20">&euro; (EURO).<BR><BR> --->
					<BR> <BR>
					<INPUT type="checkbox" name="DeclareCorrect" value="yes"><B>I declare that the given details and figures are accurate and correct.</B><BR><BR>
<!---   					<INPUT type="SUBMIT" value="Submit"> --->
 					<span class="submit"><A HREF="javascript:verifyForm();">phr_Continue</A></span>
					<BR><BR>
					<FONT size="1" face="verdana, arial">The person who completes the Scorecard must be a registered partner. We reserve the right to request substantiating proof of the declared purchases and routine checks will be made with the distributor. The given details will be used explicitly for the purposes intended by the program.</FONT>
			</FORM>
			</TD>
		</TR>
	</TABLE><BR><BR>
</cfoutput>

</CF_DISPLAYBORDER>


