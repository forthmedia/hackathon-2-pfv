<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			incentivePointsClaimByProductFamily.cfm
Author:				?
Date started:		?

Description:		This is designed for defining a screen that can submit to itself
					and set up an new account.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

17-Feb-2005			GCC			LS submisions altered to LA (Sony wanted auto approval of league claims)
15-Mar-2005			WAB			Mod to sort out JS problem in mozilla#
28-May-2005			GCC			CR_SNY517 - Added a param to limit claim dates to x days in the past

2005-09-07			WAB			Bug  "Element THISPRODUCTID is undefined in FORM. "
								Tracked down to people using the back button and then submitting the form, javascript not picking up unselected drop down (was testing for selectedIndex ==0, but the back button was giving selectedIndex = -1
2005/09/29			GCC			A_675 Show non-product promotions on the statement detail view
2005-12-14			WAB			fixed a JS bug in isSerialNumberRequired() if the null item on the select boxes was chosen the function jsut returns
2006-03-22			WAB			changed a variable timeN to request.requestTime
2006-04-12			WAB			in an attempt to prevent javascript errors, I have put all the javascript into the <head> </head> to make sure that it gets loaded before people can click on anything
2008-03-06			NJH			P-TND065 - added the NoOfUsers for Trend to the company statement. This can be switched on/off with the showNoOfUsers parameter.
2008-12-09			NJH			Bugs Sony Bug Fix Issue 1459 - changed thisFormName to formName for the date picker.
2008-12-09			NJH			Bugs Demo Bug Fix Issue 1671 - removed the inline styling and the bgColor="white" on the tables.
2009/02/03			NJH			Bugs T-10 Issue 1490 - added "snoLabel" as a hidden field, as the javascript was expecting it, but it didn't exist if the product category group record count was 0.
2009/06/25			NJH			Bug Fix Sony Issue 2395 - restrict productAndGroupAndCat query to just manual products
2010/11/23			WAB			remove a cfabort and replace with an IF Allows full borderset to show even if an error message is displayed
2012-03-26 			WAB 		Altered WebService functions to use returnFormat=json
2015-02-17			ACPK		FIFTEEN-196 Fixed datepicker not allowing dates to be selectable
Possible enhancements:


 --->

<cf_checkFieldEncryption fieldNames="rwTransactionID">

<cfif fileexists("#application.paths.code#\cftemplates\incentivePointsClaimByProductFamily.cfm")>
	<cfinclude template="code/cftemplates/incentivePointsClaimByProductFamily.cfm">
<cfelse><!--- checking if a customer version of this file exists - if it does use that else continue on doing what it did before. --->


<!--- this include provides the logic for the Relay Form Custom Tags --->
<cfoutput>
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
</cfoutput>


<cfparam name="form.RWTransactionID" default="0">
<cfparam name="form.quantity" default="1">
<cfparam name="form.serialNo" default="">
<cfparam name="form.EndUserCompanyName" default="">
<cfparam name="form.productCategoryID" default="0">
<cfparam name="form.productFamilyID" default="0">
<cfparam name="form.DistiID" default="0">
<cfparam name="productCatalogueCampaignID" default="4">
<cfparam name="form.frmTask" default="">
<cfparam name="sortOrder" default="SKU DESC">
<cfparam name="productGroupOnTop" default="1">
<cfparam name="duplicateLineItem" default="0">
<cfparam name="showNoOfUsers" default="0"> <!--- NJH 2008/03/05 P-TND065 --->

<cfparam name="validClaimPendingStatuses" default="CI">

<cfparam name="hideSerialNo" default="no">
<cfparam name="hideInvoiceNo" default="no">
<cfparam name="hideInvoiceDate" default="no">
<cfparam name="hideEndUserCompanyName" default="no">
<cfparam name="hideDistiID" default="no">

<cfparam name="serialNoRequired" default="no">
<cfparam name="invoiceNoRequired" default="no">
<cfparam name="invoiceDateRequired" default="no">
<cfparam name="endUserCompanyNameRequired" default="no">
<cfparam name="distiIDRequired" default="no">

<cfparam name="claimPointsFrom" type="date" default="2005-06-01">
<cfparam name="claimPointsTo" type="date" default="#dateFormat(now(),'yyyy-mm-dd')#">
<!--- GCC - 2005/06/28 - CR_SNY517 START --->
<cfparam name="claimWindowDays" type="numeric" default="28">
<!--- GCC - 2005/06/28 - CR_SNY517 END --->
<cfparam name="constrainProductGroupsByFlag" type="boolean" default="true">

<!--- NJH 2009/03/02 CR-SNY671 start --->
<cfparam name="uploadClaimInvoice" type="boolean" default="false">
<cfparam name="serialNoLength" type="numeric" default="50">
<cfparam name="claimInvoiceRequired" type="boolean" default="no">

<!--- if we're uploading invoices, the invoice number needs to become mandatory. --->
<cfif uploadClaimInvoice>
	<cfset hideInvoiceNo = "no">
	<cfset invoiceNoRequired = "yes">
</cfif>


<!--- NJH 2009/03/02 CR-SNY671 end --->

<!--- NJH 2009/03/02 CR-SNY671 - added incentive_claim_JS_uploadInvoice to list of phrases needing translating --->
<cf_translate phrases="incentive_NotAuthorisedToClaimAnyProducts,incentive_claim_JS_uploadInvoice"/>


<cfscript>
	claimAlreadySubmitted = false;
	rightsOK = true;

	if(not len(form.productCategoryID)){
		form.productCategoryID = 0;
	}

	if(structKeyExists(url, "RWTransactionID")){
		form.RWTransactionID = url.RWTransactionID;
	}

	//get user details
	getPersonDetails = application.com.relayIncentive.getPersonOrgAndCountry(request.relayCurrentUser.personID);
	userCountry = getPersonDetails.countryID;
	userOrganisation = getPersonDetails.organisationID;

	// GCC - 2005/06/28 - CR_SNY517 START
	//Germany can claim any time
	if (userCountry eq 4) {
		claimWindowDays = "28";
		claimPointsFrom = "2005-07-01";
	}
	// GCC - 2005/06/28 - CR_SNY517 END

	//start transaction
	if(not compareNoCase(frmTask, "createTransaction")){
		companyAccount = application.com.relayIncentive.getCompanyAccount(variables.userOrganisation).accountID;
		form.RWTransactionID = application.com.relayIncentive.RWAccruePoints(AccountID=variables.companyAccount,PointsAmount=0,RWTransactionType='CI',AccruedDate=CreateODBCDateTime(request.requestTime));
	}
	// Get the claim status.
	claimStatus = application.com.relayIncentive.getRWTransactionType(form.RWTransactionID);

	if (listFindNoCase(claimStatus,validClaimPendingStatuses) OR NOT LEN(form.frmTask)) {
		// we can now continue.
		//add line items
		if(not compareNocase(frmTask,"addProduct")){
			//if(not application.com.relayIncentive.checkDuplicateLineItem(form.RWTransactionID,form.thisProductID)){
			argCollection=structNew();
			if (structKeyExists(form,"invoiceNo")) {
				argCollection.invoiceNo = form.invoiceNo;
			}
			application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=form.RWTransactionID,ItemID=form.thisProductID,quantity=form.quantity,distiID=form.distiID,serialNo=form.serialNo,invoiceDate=form.invoiceDate,endUserCompanyName=form.endUserCompanyName,argumentCollection=argCollection);
			/*}
			else{
				duplicateLineItem=1;
			}*/
		}

		//remove .line items
		if(not compareNoCase(frmTask, "removeProduct")){
			application.com.relayIncentive.removeRWTransactionItems(form.RWTransactionID,form.FRMPRODUCTDELETIONLIST);

		}
		if(not compareNoCase(frmTask, "submitClaim")){

			/* Determine the number of different product 'types' of product; League or Claim points.
				If we have more than one type of claim, then split them into two claims,
				else just submit the claim.
			*/
			// We currently work with two types of claim.
			totalPointsClaimed = application.com.relayIncentive.getTransactionValue(rwTransactionID=form.RWTransactionID);

			ItemCampaignIds = application.com.relayIncentive.getRWTransactionItemCampaignIds(form.RWTransactionID);
			if (ItemCampaignIds.recordcount eq 1) {
				// We have one 'set' of products so can proceed.

				//claim submitted
				if (ItemCampaignIds.campaignID eq 6){
					// We need to change the claim submission from CI to LS.
					totalPointsClaimed = application.com.relayIncentive.submitClaim(
					RWTransactionID = form.RWTransactionID,
					RWTransactionTypeID = "LA",
					distiID = form.distiID);
				}
				else {


					//get user details
					getPersonDetails = application.com.relayIncentive.getPersonOrgAndCountry(request.relayCurrentUser.personID);

					// 2005/07/08 - GCC - promotion check BEFORE submitting thre claim
					//and check for any promotions
					application.com.relayIncentive.getActivePromotions(
						countryID=getPersonDetails.countryID,
						triggerPoint='claim',
						pointsPassed=totalPointsClaimed,
						nowDate=request.requestTime,RWTransactionID=form.RWTransactionID);

					// Standard
					totalPointsClaimed = application.com.relayIncentive.submitClaim(
					RWTransactionID = form.RWTransactionID,
					RWTransactionTypeID = "CL",
					distiID = form.distiID);

				}

			}
			else {
				// We need to create another transaction for the 'League' items. Type 'LS' - League Submitted
				companyAccount = application.com.relayIncentive.getCompanyAccount(variables.userOrganisation).accountID;
				LeagueRWTransactionID = application.com.relayIncentive.RWAccruePoints(AccountID=variables.companyAccount,PointsAmount=0,RWTransactionType='LA',AccruedDate=CreateODBCDateTime(request.requestTime));

				// Now move products from one transaction to another.
				application.com.relayIncentive.MoveRWTransactionItems(
					sourceRWTransactionID = form.RWTransactionID,
					targetRWTransactionID = LeagueRWTransactionID,
					productCampaignId = 6);

				//get user details
				getPersonDetails = application.com.relayIncentive.getPersonOrgAndCountry(request.relayCurrentUser.personID);

				// 2005/07/08 - GCC - promotion check BEFORE submitting thre claim
				//and check for any promotions
				application.com.relayIncentive.getActivePromotions(
					countryID=getPersonDetails.countryID,
					triggerPoint='claim',
					pointsPassed=totalPointsClaimed,
					nowDate=request.requestTime,RWTransactionID=form.RWTransactionID);

				// Submit the claims.
				totalPointsClaimedCL = application.com.relayIncentive.submitClaim(
					RWTransactionID = form.RWTransactionID,
					RWTransactionTypeID = "CL",
					distiID = form.distiID);
				totalPointsClaimedLS = application.com.relayIncentive.submitClaim(
					RWTransactionID = LeagueRWTransactionID,
					RWTransactionTypeID = "LA",
					distiID = form.distiID);

			}
		}

		else{
			// GCC - 2005/09/29 - A_675 Show non-product promotions on the statement detail view
			if (structKeyExists(url, "readOnly")) {
				statementView = "true";
			} else {
				statementView = "false";

			}
			//get product group and products query
			qProductAndGroupList = application.com.relayIncentive.getProductAndGroupList(productCatalogueCampaignID=productCatalogueCampaignID, countryID=variables.userCountry, productCategoryID=form.productCategoryID, constrainByFlag=variables.constrainProductGroupsByFlag, statementView=statementView);

			//If product categories present, get the query for three selects
		/*if(qProductAndGroupList.recordCount eq 0) {
				writeoutput (phr_incentive_NotAuthorisedToClaimAnyProducts);
				application.com.email.sendEmail("NotAuthorisedToClaimAnyProducts",request.relayCurrentUser.personid);
				application.com.globalFunctions.abortIT();
		}*/

			//when transaction active, get all line items
			if(form.RWTransactionID){
				transactionItems = application.com.relayIncentive.getTransactionItems(RWTransactionID = form.RWTransactionID, statementView = statementView);
				qGetOrgDistis = application.com.commonQueries.getOrgDistis(application.siteDataSource,variables.userOrganisation);
			}



			//if the two select query is limited by the category,
			//get the full one for the category query
			if(form.productCategoryID){
			qProductAndGroupListAll = application.com.relayIncentive.getProductAndGroupList(productCatalogueCampaignID, variables.userCountry, form.productCategoryID, variables.constrainProductGroupsByFlag);
				//list of product groups from the group/product query
				rawGroupList = valueList(qProductAndGroupListAll.productGroupID);
			}

			else{
				//list of product groups from the group/product query
				rawGroupList = valueList(qProductAndGroupList.productGroupID);
			}

			distinctGroupList = "";
			//loop and dedupe
			for(i=1; i lte listLen(rawGroupList); i = i+1){
				if(not listFind(distinctGroupList, listGetAt(rawGroupList, i))){
					distinctGroupList = listAppend(distinctGroupList, listGetAt(rawGroupList, i));
				}
			}

		//If product categories present, get product categories (if any)
		if(listLen(variables.distinctGroupList) eq 0) {
			rightsOK= false;
		}
		else
		{
			productCategoriesQry = application.com.relayIncentive.getProductGroupCategory(variables.distinctGroupList, form.productFamilyID);

			//If product categories present, get the query for three selects
			// NJH 2009/06/25 Bug Fix Sony Issue 2395 - restrict products to just manual accrual
			if(productCategoriesQry.recordCount){
			qProductAndGroupList = application.com.relayIncentive.getProductAndGroupAndCatList(productCatalogueCampaignID, variables.userCountry,0, 0,'no',variables.constrainProductGroupsByFlag);
			}

			else{
			qProductAndGroupList = application.com.relayIncentive.getProductAndGroupList(productCatalogueCampaignID, variables.userCountry, 0, variables.constrainProductGroupsByFlag);
			}

			/*whenever trasaction is active and some line items already in the basket,
				if productCategories present, set productCategoryID to the last product's category one
				so that if people are claiming from the same category, the select is always selected to the right one*/
			if(form.RWTransactionID and productCategoriesQry.recordCount and not form.productCategoryID and transactionItems.recordCount){
				//get the last item's in the trasanction category
				productCategoryLast = listLast(valueList(transactionItems.productGroupID));
				if(len(productCategoryLast)){
					form.productCategoryID = application.com.relayIncentive.getProductGroupCategory(productCategoryLast).productCategoryID;
				}
				else{
					form.productCategoryID = productCategoriesQry.productCategoryID[1];
				}
			}

			//if productFamilyID passsed, do not limit it backwards
			if(form.productFamilyID){
				productCategoriesQryAll = application.com.relayIncentive.getProductGroupCategory(variables.distinctGroupList, 0);
				productCategoryList = valueList(productCategoriesQryAll.productCategoryID);
			}

			//if product categories present, see is product families also there
			if(productCategoriesQry.recordCount){
				if(not structKeyExists(variables, "productCategoryList")){
					productCategoryList = valueList(productCategoriesQry.productCategoryID);
				}
				//get product families (if any)
				productFamiliesQry = application.com.relayIncentive.getProductFamily(productCategoryList);
			}

			if(form.RWTransactionID){
				RWTransactionQry = application.com.relayIncentive.getTransactionDetails(RWTransactionID);
			}

			//dynamically format the screen
			if(not productGroupOnTop){
				htmlBetween = "</td><td>";
			}
			else{
				htmlBetween = "";
			}

			//dynamically format the screen
			if(not productGroupOnTop){
				htmlBetween2 = "</td><td>";
			}
			else{
				htmlBetween2 = "";
			}
		}
		}
	}
	else {
	claimAlreadySubmitted = true;
	}



</cfscript>

<cfif not RightsOK>
	phr_incentive_NotAuthorisedToClaimAnyProducts
	<cfset application.com.email.sendEmail("NotAuthorisedToClaimAnyProducts",request.relayCurrentUser.personid)>
<cfelseif claimAlreadySubmitted>
	phr_incentive_claim_claimAlreadySubmitted
<cfelseif not compareNocase(form.frmTask, "submitClaim")>
	<!--- We've gone through the submission process and the  --->
	<cfif claimStatus eq "CI">
	phr_incentive_claim_claimBeingProcessed
		<!--- 2006/03/17 - GCC - CR_SNY591 - mechanism to include a cfm file to carry out further actions after a claim has been submitted --->
		<cfif isdefined('postIncludeTemplate')>
			<cfinclude template="#postIncludeTemplate#">
		</cfif>
	</cfif>
<cfelse>

<cfsavecontent variable = "thejavascript_js">
<cf_translate processEvenIfNested=true>
	<script >

	// Global variables
	var isCSS, isW3C, isIE4, isNN4;
	// initialize upon load to let all browsers establish content objects
	function initDHTMLAPI() {

		if (document.images) {
			isCSS = (document.body && document.body.style) ? true : false;
			isW3C = (isCSS && document.getElementById) ? true : false;
			isIE4 = (isCSS && document.all) ? true : false;
			isNN4 = (document.layers) ? true : false;
			isIE6CSS = (document.compatMode && document.compatMode.indexOf("CSS1") >= 0) ? true : false;
		}
	}



	var listPriceArr = new Array();
	var productIDArr = new Array();
	<!--- var fieldsRequiredArr = new Array(); --->


	<cfoutput query="qProductAndGroupList" group="productGroupID">
		<!--- <cfif len(#IncentiveClaimRequiredFields#)>
			fieldsRequiredArr[#jsStringFormat(productGroupID)#] = #jsStringFormat(IncentiveClaimRequiredFields)#;
		<cfelse>
			fieldsRequiredArr[#jsStringFormat(productGroupID)#] = '101111';
		</cfif> --->
		<cfoutput>productIDArr[#jsStringFormat(qProductAndGroupList.currentRow)#] = #jsStringFormat(productID)#;	listPriceArr[#jsStringFormat(qProductAndGroupList.currentRow)#] = #jsStringFormat(int(discountprice))#;
		</cfoutput>
	</cfoutput>


	function addProduct(distiIdRequired, serialNoRequired, invoiceNoRequired, endUserCompanyNameRequired, invoiceDateRequired) {

		var form = document.forms.oppProductForm;

		if(form.distiID && distiIdRequired == 'yes' && form.distiID.selectedIndex == 0){
			alert("phr_incentive_claim_JS_mustSpecifyDistributor");
			form.distiID.focus();

		}

		else if(form.serialNo && serialNoRequired == 'yes' && form.serialNo.value == ''){
			alert("phr_incentive_claim_JS_mustSpecifySerialNumber");
			form.serialNo.focus();

		}

		else if(form.invoiceNo && invoiceNoRequired == 'yes' && form.invoiceNo.value == ''){
			alert("phr_incentive_claim_JS_mustSpecifyInvoiceNumber");
			form.invoiceNo.focus();

		}

		else if(form.invoiceDate && invoiceDateRequired == 'yes' && form.invoiceDate.value == ''){
			alert("phr_incentive_claim_JS_mustSpecifyInvoiceDate");
			form.invoiceDate.focus();

		}

		else if(form.endUserCompanyName && endUserCompanyNameRequired == 'yes' && form.endUserCompanyName.value == ''){
			alert("phr_incentive_claim_JS_mustSpecifyEndUserCompany");
			form.endUserCompanyName.focus();

		}

		// WAB 2005-09-07 altered to pick up selectedIndex = -1 see comments in header
		else if(form.productGroupID.selectedIndex <= 0){
			alert("phr_incentive_claim_JS_mustSelectProductGroup");
		}

		else if(form.thisProductID.selectedIndex <= 0){
			alert("phr_incentive_claim_JS_mustSelectProduct");
		}


		else if (form.quantity && !(form.quantity.value > 0) )
		{
			alert("phr_incentive_claim_JS_mustSpecifyQuantity");
			form.quantity.focus();

		}
		else if(form.serialRequired.value == 1 && form.serialNo.value == ''){
			alert("phr_incentive_claim_JS_mustSpecifySerialNumber");
			form.serialNo.focus();

		}

		else{
			form.frmTask.value = "addproduct";
			form.submit();
		}



	}

	function submitClaim() {
		var errorMsg = '';

		if(document.oppProductForm.confirmVeracity.checked == false){
			errorMsg = 'phr_incentive_claim_JS_confirmClaim';
		}
		<!--- NJH 2009/03/02 CR-SNY671 - if invoice is required, check that a document exists --->
		<cfif claimInvoiceRequired and uploadClaimInvoice>
			if (!hasInvoiceBeenUploaded()) {
				errorMsg = errorMsg + '\n phr_incentive_claim_JS_uploadInvoice';
			}
		</cfif>

		if (errorMsg == '') {
			document.oppProductForm.frmTask.value = "submitClaim";
			document.oppProductForm.submit();
		} else {
			alert(errorMsg);
		}
	}

	<!--- NJH 2009/03/02 CR-SNY671 --->
	function hasInvoiceBeenUploaded() {
		page = '/webservices/relayIncentiveWS.cfc?wsdl&method=getClaimInvoice&returnFormat=json'
		<cfoutput>page = page + '&entityID=#jsStringFormat(form.rwTransactionID)#'</cfoutput>

		var myAjax = new Ajax.Request(
				page,
				{
					method: 'get',
					parameters: '',
					evalJSON: 'force',
					debug : false,
					asynchronous: false
				}
			);

		response = myAjax.transport.responseText.evalJSON();

		<!--- if we have a record returned, then an invoice has been uploaded --->
		if (response.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	function getRawObject(obj) {

		var theObj;

		if (typeof obj == "string") {
			if (isW3C) {
				theObj = document.getElementById(obj);
			} else if (isIE4) {
				theObj = document.all(obj);
			} else if (isNN4) {
				theObj = seekLayer(document, obj);
			} else {
				theObj = document.getElementById(obj); // NJH 2009/02/03 Bug Fix Issue T-10 Issue 1490
			}
		} else {
			// pass through object reference
			theObj = obj;
		}

		return theObj;
	}

	function setSerialNo(){
		ser1 = document.getElementById('serialNo1');
		document.oppProductForm.serialNo.value = ser1.value;
		}

	<!--- 2014/10/01	YNA	Removed - no one knows how or why this works the way it works and no customers are using it so now is the perfect time to remove it. --->
	<!--- function checkIfSerialRequired(pgid){

		if (pgid =='') {return}   <!--- Added WAB 2005-12-14 to prevent js errors further down.  Occurs if user selects the 'select a product group' value from drop down - Doesn't really make sense to do this so OK to just return --->

		var serialNumberObject = getRawObject('sno');
		var serialNumberLabel = getRawObject('snoLabel');
		var quantityCell = getRawObject('qntCell');
		var invoiceNo = getRawObject('invoiceNo');
		var endUserCompanyName = getRawObject('endUserCompanyName');
		var distiID = getRawObject('distiID');


		currentGroupSwitch = fieldsRequiredArr[pgid].toString();


		// 1. serial number
		if(currentGroupSwitch.charAt(1) == 1){
			document.oppProductForm.serialRequired.value = 1;
			serialNumberObject.innerHTML = '<input name="serialNo1" class="form-control" id="serialNo1" maxlength="50" type="text" onChange="javascript:setSerialNo();">';
			serialNumberLabel.innerHTML = 'Serial Number';
			quantityCell.innerHTML = '1';
		}
		else{
			document.oppProductForm.serialRequired.value = 0;
			if (serialNumberObject) {serialNumberObject.innerHTML = '';}
			if (serialNumberLabel) {serialNumberLabel.innerHTML = '';}
//			<!---
//			2005-03-15 WAB altered this line,
//			in the bit of javascript: setLineTotal(totalPoints) totalPoints should actually be a string in quotes,
//			because there are so many set of quotes, I have had to escape these ones
//			quantityCell.innerHTML = '<input name="quantity1" id="quantity1" type="text" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal(totalPoints);">';
//			--->
			quantityCell.innerHTML = '<input name="quantity1" id="quantity1" class="form-control" type="text" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal(\'totalPoints\');">';
		}

		// 2. invoice number
		if(currentGroupSwitch.charAt(2) == 1){
			invoiceNo.disabled = false;
		}
		else{
			invoiceNo.disabled = true;
		}

		// 3. end user company name
		if(currentGroupSwitch.charAt(4) == 1){
			endUserCompanyName.disabled = false;
		}
		else{
			endUserCompanyName.disabled = true;
		}

		//4. distiID
		if(currentGroupSwitch.charAt(5) == 1){
			distiID.disabled = false;
		}
		else{
			distiID.disabled = true;
		}


	} --->


	function setInnerElement(element,productID){

		myObj = getRawObject(element);

		for (var i = 1; i <= productIDArr.length; i++) {
			if(productIDArr[i] == productID){
				myObj.innerHTML = listPriceArr[i];
				itemPrice = listPriceArr[i];
				document.oppProductForm.currentPointsValue.value = itemPrice;
			}
		}
		if(document.oppProductForm.quantity){
			totalPrice = itemPrice*document.oppProductForm.quantity.value;
		}
		else{
			totalPrice = itemPrice;
		}


		totalObj = document.getElementById('totalPoints');
		totalObj.innerHTML = totalPrice;

	}

	function setLineTotal(element){

		qty1 = document.getElementById('quantity1');
		totalPrice = document.oppProductForm.currentPointsValue.value * qty1.value;
		document.oppProductForm.quantity.value = qty1.value;
		myObj = getRawObject(element);
		myObj.innerHTML = totalPrice;


	}

	function resetTotal(){

		singlePriceObj = getRawObject('discountPrice');
		totalObj =  getRawObject('totalPoints');
		serialNumberObj =  getRawObject('sno');

		singlePriceObj.innerHTML = '';
		totalObj.innerHTML = '';
		serialNumberObj.innerHTML = '';

	}


	function eventDeleteOnClick(transactionID){

		with(document.forms["oppProductForm"]){
	  		if(confirm("phr_incentive_claim_JS_ConfirmDeleteProduct")){
				elements["frmProductDeletionList"].value = transactionID;
				elements["frmTask"].value = "removeProduct";
				submit();
			}
	  	}
	}

	</script>
</cf_translate>
</cfsavecontent>
<cfhtmlhead text = #thejavascript_js#>
<!--- this bit of script can't go in the head because it relies on the body being loaded --->
<script>
	// set event handler to initialize API
	initDHTMLAPI();
</script>


	<!--- NJH 2009/01/23 removed as part of Bug Fix Demo Issue 1671
	<style>
	select, input, DIV {
		font-size:10px;
	}
	</style> --->

	<cfif variables.duplicateLineItem>
		<SCRIPT type="text/javascript">
			alert("phr_incentive_claim_JS_alreadyClaimedThisProduct");
		</script>
	</cfif>



<cfif form.RWtransactionID>



<div class="div-responsive"><!--table element to div-->
    	<cfif not structKeyExists(url, "readOnly")>
    		<form name="oppProductCategoryAndFamily" method="post" enctype="multipart/form-data">
    		<cfif productCategoriesQry.recordCount>
	    		<cfoutput>
			    	<div class="row">
			    		<div class="col-xs-12 col-md-12">phr_incentive_claim_Instructions</div>
			    	</div><!--end 1st tr-->

			    	<!--- 2014/09/29 YMA fix double heading.  This now gets added from a web service callback --->
			    	<!---<div class="row hidden-xs hidden-sm div-header">
		            <div class="col-xs-12 col-md-4"><strong>phr_incentive_Claim_SKU</strong></div>
		            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_UnitPrice</strong></div>
		            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_quantity</strong></div>
		            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_TotalPoints</strong></div>
		            <div class="col-xs-12 col-md-2"><strong></strong></div>
		        </div><!--end 2nd tr-->--->

		            <cfif structKeyExists(variables, "productFamiliesQry") and productFamiliesQry.recordCount>

				        <div class="row">
				            <div class="col-xs-12 col-md-12">
				                <select name="productFamilyID" onChange="javascript:if(this.value!=0)document.oppProductCategoryAndFamily.submit()">
								    <option value="0">phr_incentive_Claim_SelectProductFamily</option>
			    					<cfloop query="productFamiliesQry">
			    					   <option value="#productFamilyID#" <cfif form.productFamilyID eq productFamilyID>selected</cfif>>#htmleditformat(name)#</option>
			    					</cfloop>
								</select>
				            </div>
				        </div><!--end 3rd tr-->

		            </cfif>
		    		<cf_encryptHiddenFields>
		    		<CF_INPUT type="hidden" name="RWTransactionID" value="#form.RWTransactionID#">
		    		</cf_encryptHiddenFields>
			    </cfoutput>
            </cfif>
		</form>

		<cfoutput>
				<form name="oppProductForm" id="oppProductForm" method="post" enctype="multipart/form-data">
				<input type="hidden" name="frmTask" value="">
				<input type="hidden" name="currentPointsValue" value="0">

				<input type="hidden" name="debug" value="0">
				<!--- added to guarantee existence - were getting destroyed in NS and FF form domains by innerHTML - not being picked up as form objects after then --->
				<input type="hidden" name="quantity" id="quantity" size="2" maxlength="3" value="1">
				<!--- NJH 2009/03/02 CR-SNY671 - serialNo included below.
				<input type="hidden" name="serialNo" maxlength="50"> --->
				<cfoutput>
					<cf_encryptHiddenFields>
					<CF_INPUT type="hidden" name="RWTransactionID" value="#RWTransactionID#">
					</cf_encryptHiddenFields>
					<CF_INPUT type="hidden" name="productFamilyID" value="#form.productFamilyID#">
				</cfoutput>

        <!--- <div class="row hidden-xs hidden-sm div-header">
            <div class="col-xs-12 col-md-4"><strong>phr_incentive_claim_product</strong></div>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_UnitPrice</strong></div>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_quantity</strong></div>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_TotalPoints</strong></div>
            <div class="col-xs-12 col-md-2"><strong></strong></div>
        </div><!--end 4th tr--> --->

        <div class="row">
				<div class="control-label col-xs-12 col-sm-4 col-md-3 required">
					<label class="control-label required" for="">phr_incentive_claim_product</label>
				</div>
				<div class="col-xs-12 col-sm-8 col-md-9">
                <cfif not productCategoriesQry.recordCount>
								<cf_twoselectsrelated
										query="qProductAndGroupList"
										name1="productGroupID"
										name2="thisProductID"
										display1="translatedProductGroupTitle"
										display2="translatedProductTitle"
										value1="productGroupID"
										value2="productID"
										size1="1"
										size2="auto"
										onChange="javascript:setInnerElement('discountPrice',this.value)"
										autoselectfirst="Yes"
										emptytext1="phr_incentive_Claim_SelectProductGroup"
										emptytext2="phr_incentive_Claim_SelectProduct"
										formname = "oppProductForm"
										htmlbetween="#variables.htmlBetween#"
										labelClass1="dummy"
										labelClass2="dummy">
								<cfelse>
									<cf_threeselectsrelated
										query="qProductAndGroupList"
										name1="productCategoryID"
										name2="productGroupID"
										name3="thisProductID"
										display1="productCategoryName"
										display2="translatedProductGroupTitle"
										display3="translatedProductTitle"
										value1="productCategoryID"
										value2="productGroupID"
										value3="productID"
										size1="1"
										size2="auto"
										size3="auto"
										forcewidth3="20"
										onChange="javascript:resetTotal()"
										onChange3="javascript:setInnerElement('discountPrice',this.value)"
										autoselectfirst="Yes"
										emptytext1="phr_incentive_Claim_SelectProductCategory"
										emptytext2="phr_incentive_Claim_SelectProductGroup"
										emptytext3="phr_incentive_Claim_SelectProduct"
										formname = "oppProductForm"
										htmlbetween="#variables.htmlBetween#"
										htmlbetween2="#variables.htmlBetween2#"
										labelClass1="dummy"
										labelClass2="dummy"
										labelClass3="dummy">
								</cfif>
					</div>
            </div>
			<div class="row">
				<div class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label">phr_incentive_Claim_UnitPrice</label></div>
	            <div class="col-xs-12 col-sm-8 col-md-9" id="discountPrice">&nbsp;</div><!--- Gets overwritten busing javascript by quantity1 or serialNo1 depending upon information capture requirements --->
			</div>
			<div class="row">
           		<div class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label" for="quantity1">phr_incentive_Claim_quantity</label></div>
				<div class="col-xs-12 col-sm-8 col-md-9" id="qntCell">
                	<input class="form-control" type="Text" name="quantity1" id="quantity1" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal('totalPoints');">
           		</div>
			</div>
			<div class="row">
				<div class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label">phr_incentive_Claim_TotalPoints</label></div>
				<div class="col-xs-12 col-sm-8 col-md-9" id="totalPoints">&nbsp;</div>
			</div>
			<div class="row">
				<div class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label">&nbsp;</label></div>
				<div class="col-xs-12 col-sm-8 col-md-9" id="sno">&nbsp;</div>
			</div>
        <!--end 5th tr-->

        <!--- NJH 2009/03/02 CR-SNY671 - this was commented out.. changed condition to be if not hideSerialNo --->
        <cfif not hideSerialNo>

         <div class="row">
			<div id="ino" class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label" for="serialNo">phr_incentive_Claim_SerialNumber</label></div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<CF_INPUT id="serialNo" name="serialNo" maxlength="#serialNoLength#" class="form-control" type="text">
			</div>
        </div><!--end 6th tr-->
        <cfelse>
				<input name="serialNo" type="hidden" value="">
		</cfif>
		<cfif not compareNoCase(hideInvoiceNo, "no")>

        <div class="row">
			<div id="ino" class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label" for="invoiceNo">phr_incentive_Claim_InvoiceNumber</label></div>
			<div class="col-xs-12 col-sm-8 col-md-9">
				<input class="form-control" id="invoiceNo" name="invoiceNo" type="text">
			</div>
        </div><!--end 7th tr-->
        <cfelse>
		  <input name="invoiceNo" type="hidden" value="">
		</cfif>
		<!--- GCC - 2005/06/28 - CR_SNY517 START --->
		<!--- 2015-02-17	ACPK	FIFTEEN-196	fixed datepicker not allowing dates to be selectable by swapping disableToDate and disableFromDate variables--->
		<cfif not compareNoCase(hideInvoiceDate, "no")>
			<cfset windowStartDate = dateFormat(dateadd ("d",-#claimWindowDays# +1,now()),'yyyy-mm-dd')>
			<cfif windowStartDate gt claimPointsFrom>
				<cfset disableToDate = windowStartDate>
			<cfelse>
				<cfset disableToDate = claimPointsFrom>
		</cfif>
		<cfset disableFromDate = claimPointsTo>
				<div class="row">
					<div id="ind" class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label" for="">phr_incentive_Claim_InvoiceDate</label></div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<cf_relayDateField
							currentValue=""
							formName="oppProductForm"
							fieldName="invoiceDate"
							anchorName="anchorX1"
							helpText="phr_dateFieldHelpText"
							disableFromDate="#disableFromDate#"
							disableToDate="#disableToDate#"
							enableRange="true">
					</div>
				</div><!--end 8th tr-->
        <!--- GCC - 2005/06/28 - CR_SNY517 END --->
		<cfelse>
			<input name="invoiceDate" type="hidden" value="">
		</cfif>

		<cfif not compareNoCase(hideEndUserCompanyName, "no")>
				<div class="row">
					<div id="eucn" class="control-label col-xs-12 col-sm-4 col-md-3"><label class="control-label" for="endUserCompanyName">phr_incentive_Claim_EndUserCompanyName</label></div>
					<div class="col-xs-12 col-sm-8 col-md-9">
						<input class="form-control" id="endUserCompanyName" name="endUserCompanyName" type="text">
						phr_incentive_claim_endCustomerInstructions
					</div>
				</div><!--end 9th tr-->

        <cfelse>
			<input name="endUserCompanyName" type="hidden" value="">
		</cfif>

		<cfif not compareNoCase(hideDistiID, "no")>
        <div class="row ">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<label class="control-label" for="distiID">phr_incentive_Claim_PleaseSpecifyDistributor</label>
			</div>
            <div class="col-xs-12 col-sm-8 col-md-9">
                <SELECT id="distiID" NAME="distiID" CLASS="form-control">
    				<OPTION VALUE="0">phr_incentive_Claim_PleaseSpecifyDistributor</OPTION>
    					<CFLOOP QUERY="qGetOrgDistis">
        					<cfoutput>
        							<OPTION VALUE="#OrganisationID#">#htmleditformat(OrganisationName)#</OPTION>
        					</cfoutput>
    					</CFLOOP>
				</SELECT>
            </div>
        </div><!--end 10th tr-->

        <cfelse>
			<input name="distiID" type="hidden" value="0">
		</cfif><!--- NJH 2009/03/02 CR-SNY671 - changed serialNo validation so that it is only yes if we're showing the serialNo and have specified it as being required. --->

        <div class="row div-noborder">
            <div class="col-xs-12 col-sm-9 col-sm-offset-4  col-md-offset-3">
                <input type="button" class="btn btn-primary incentivePointsBtn" onClick="javascript:addProduct('#distiIDRequired#', '#serialNoRequired and not hideSerialNo?"yes":"no"#', '#invoiceNoRequired#', '#endUserCompanyNameRequired#', '#invoiceDateRequired#')" value="phr_incentive_Claim_AddLineItem">
            </div>
        </div><!--end 11th tr-->

        </cfoutput>
		</cfif>
			<!--- ==============================================================================
				   Existing claim items
			=============================================================================== --->
			<cfoutput>
			<cfif structKeyExists(url, "readOnly")>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <cfif not compareNoCase(RWTransactionQry.RWTransactionTypeID, "CL")>
						phr_incentive_Claim_StatusAwaitingValidation
				<cfelseif not compareNoCase(RWTransactionQry.RWTransactionTypeID, "AC")>
						phr_incentive_Claim_StatusValidated
				</cfif>
            </div>
        </div><!--end 12th tr-->
        </cfif>

		<cfif transactionItems.recordCount>
		<h2 id="incentive_table_heading">phr_incentive_table_heading</h2>
        <div class="row hidden-xs hidden-sm div-header">
            <div class="col-xs-12 col-md-4"><strong>phr_incentive_Claim_SKU</strong></div>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_UnitPrice</strong></div>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_quantity</strong></div>
            <cfif showNoOfUsers> <!--- NJH 2008/03/05 P-TND065 --->
                <div class="col-xs-12 col-md-2"><strong>phr_incentive_No_Of_Users</strong></div>
            </cfif>
            <div class="col-xs-12 col-md-2"><strong>phr_incentive_Claim_TotalPoints</strong></div>
            <cfif not structKeyExists(url, "readOnly")>
                <div class="col-xs-12 col-md-2"><strong>phr_incentive_claim_deleteItem</strong></div>
            </cfif>
        </div><!--end 13th tr-->

        <cfset totalPointsLocal = 0>
		<cfset localClass="evenrow">
		<cfloop query="transactionItems">
			<cfset totalPointsLocal = totalPointsLocal+points>
			<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
			<!--- 2005/07/08 - GCC - Display promotion description against promotion points points on the claim detail screen--->

       <div class="row">
			<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_Claim_SKU</strong></div>
            <div class="col-xs-12 col-md-4">
                #htmleditformat(sku)# <cfif sku is not ""><br></cfif>
				<cfif transactionItems.pointstype eq "PP">
    				<cfscript>
    					promotion = application.com.relayIncentive.getPromotion(RWPromotionID=transactionItems.RWPromotionID);
    				</cfscript>
    				(#htmleditformat(promotion.RWPromotionDescription)#)
				<cfelse>
					(<cfif distiOrganisationID eq 0>phr_incentive_Claim_Distributor: Phr_Ext_OtherDisti <cfelseif distiOrganisationID gt 0>phr_incentive_Claim_Distributor: #htmleditformat(application.com.relayIncentive.getDistiName(distiOrganisationID))#</cfif> <cfif isdefined('serialNumber') and len(serialNumber)><br>phr_incentive_Claim_SerialNo: #htmleditformat(serialNumber)# </cfif><cfif isdefined('invoiceNumber') and len(invoiceNumber)> phr_incentive_Claim_InvoiceNumber: #htmleditformat(invoiceNumber)#</cfif><cfif isdefined('invoiceDate') and len(invoiceDate)> phr_incentive_Claim_InvoiceDate: #DateFormat(invoiceDate, "dd-mm-yyyy")#</cfif>)
				</cfif>
            </div>

            <!--- GCC 2008/05/06 CR-TND552 --->
			<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_Claim_UnitPrice</strong></div>
            <div class="col-xs-12 col-md-2">#htmleditformat(pointsPerUnit)#</div>

			<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_Claim_quantity</strong></div>
            <div class="col-xs-12 col-md-2">#htmleditformat(quantity)#</div>

            <cfif showNoOfUsers> <!--- NJH 2008/03/05 P-TND065 --->
				<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_No_Of_Users</strong></div>
                <div class="col-xs-12 col-md-2"><cfif len(NoOfUsers)>#htmleditformat(NoOfUsers)#<cfelse>&nbsp;</cfif></div>
            </cfif>

			<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_Claim_TotalPoints</strong></div>
            <div class="col-xs-12 col-md-2">#htmleditformat(points)#</div>

            <cfif not structKeyExists(url, "readOnly")>
				<div class="col-xs-12 hidden-md hidden-lg"><strong>phr_incentive_claim_deleteItem</strong></div>
                <div class="col-xs-12 col-md-2"><CF_INPUT type="button" value="phr_incentive_Claim_DeleteItem" onClick="eventDeleteOnClick(#RWTransactionItemsID#);"></div>
            </cfif>
        </div><!--end 14th tr-->
        </cfloop>

		<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
		<!--- NJH 2008/03/05 P-TND065 --->
		<cfset colspan=3>
		<cfif showNoOfUsers>
			<cfset colspan=colspan+1>
		</cfif>

        <div class="row">
            <div class="col-xs-12 col-md-4"><strong>phr_incentive_Claim_TOTALPOINTSTHISCLAIM</strong></div>
            <div class="col-xs-12 col-md-4"><strong>#htmleditformat(variables.totalPointsLocal)#</strong></div>
            <cfif not structKeyExists(url, "readOnly")>
                <div class="col-xs-12 col-md-4"></div>
            </cfif>
        </div><!--end 15th row-->

        <cfif not structKeyExists(url, "readOnly")><!--- NJH 2008/03/05 P-TND065 --->
			<cfset colspan=5>
			<cfif showNoOfUsers>
					<cfset colspan=colspan+1>
			</cfif>
        <div class="row">
	            <div class="col-xs-8 col-md-6">
		            phr_incentive_Claim_ConfirmTheClaim
				</div>
	            <div class="col-xs-4 col-md-6">
		            <CF_INPUT type="checkbox" name="confirmVeracity" value="1">
				</div>
        </div><!--end 16th row-->

        <cfif uploadClaimInvoice>
        <div class="row">
            <div class="col-xs-12 col-md-12">
                phr_incentive_claim_invoiceInstructions<br>
				<!--- 2009/12/10 GCC - LID 2922 - encrypt variables to enable this to work post RW8.2 --->
				<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityType=rwTransaction&entityID=#form.rwTransactionID#")>
				<CF_INPUT type="button" value="phr_incentive_uploadInvoice" onClick="javascript:void(openWin('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','InvoiceUpload','width=600,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0'));">
            </div>
        </div><!--end 17th row-->
        </cfif>

        <div class="row div-noborder">
            <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                <input type="button" class="btn btn-primary" value="phr_incentive_SubmitTheClaim" onClick="javascript:submitClaim()">
            </div>
        </div><!--end 18th row-->
        </cfif>
	</cfif>
	<input name="serialRequired" type="hidden" value="0">
	<input name="frmProductDeletionList" type="hidden" value="0">
	</form>
</div> <!--end table to div-->

   </cfoutput>
	<cfelse>

    <div style="height:auto; width:auto"><!--table element to div-->
        <div class="row">
            <div class="col-xs-12 col-md-12">phr_incentive_claim_Introduction</div>
        </div><!--end 1st row-->

        <form name="oppProductForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="frmTask" value="createTransaction">
	    </form>

        <div class="row div-noborder">
            <div class="col-xs-12 col-md-12">
                <cfparam name="useButtons" type="boolean" default="true">
				<cfif useButtons>
					<input type="button" class="btn btn-primary" value="phr_incentive_CreateNewClaim" onClick="javascript:document.oppProductForm.submit();">
				<cfelse>
					<a href="javascript:document.oppProductForm.submit();">phr_incentive_CreateNewClaim</a>
				</cfif>
            </div>
        </div><!--end 2nd row-->
   </div><!--end table to div-->

	</cfif>


</CFIF>


</cfif><!--- End of checking if a customer version of this file exists --->
