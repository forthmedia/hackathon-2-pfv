<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Products Report</cf_title>
	</cf_head>
	
<!--- get the users countries --->
<cfinclude template="/templates/qryGetCountries.cfm">

<cfparam name="sortOrder" default="Product_Category">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">


<cfquery name="getProducts" datasource="#application.SiteDataSource#">
	select Product_Type, ProductID, Flag, Country, Product_Category, Product_Group, 
	Sku, Description, Points, Deleted
	from vIncentiveClaimProducts
	WHERE 1=1		
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Product Report</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWProducts.cfm?#queryString#&openAsExcel=true" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp; 
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getProducts#"
	queryName="getProducts"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	numberFormat="Points"
		
	keyColumnList=""
	keyColumnURLList=""
	keyColumnKeyList=""
	
	showTheseColumns="Product_Type,ProductID,Flag,Country,Product_Category,Product_Group,Sku,Description,Points,Deleted"
 	hideTheseColumns=""
	columnTranslation="False"
	
	FilterSelectFieldList="Product_Type,Country,Product_Category,Product_Group,Deleted"
	FilterSelectFieldList2="Product_Type,Country,Product_Category,Product_Group,Deleted"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	
	
>

</cf_translate>



