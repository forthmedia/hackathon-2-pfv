<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportRWPointsExpireFunctions.cfm
Author:				Godfrey Smith
Date started:		2008-03-14
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Nov-2004			MDC			Initial version

Possible enhancements:

 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_IncentiveFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_Selections";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Save checked records as a selection";
comTableFunction.securityLevel = "";
comTableFunction.url = "/selection/selectalter.cfm?frmtask=save&frmruninpopup=true&frmPersonids=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();
</cfscript>

