<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2008-07-14		NJH		Removed the GetFlagID qry and replaced it with the application variables.
2012-10-04	WAB		Case 430963 Remove Excel Header
--->

<cfparam name="openAsExcel" type="boolean" default="false">

<!---
2010-10-20 now settings
<cfparam name="ProgChampTextID" type="string" default="KeyContacts Primary"> <!--- NJH 2008-07-14 Bug Fix T-10 534 was LoyaltyPointsAdministratorID --->
 --->

	<cf_head>
		<cf_title>Orphaned Points</cf_title>
	</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Points that can no longer be spent</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="reportRWPersonalClaims.cfm?openAsExcel=yes" class="Submenu">Excel</a>&nbsp;&nbsp;&nbsp;
		</TD>
	</TR>
</TABLE>


<cf_translate>

	<cfparam name="sortorder" default = "Country">
	<cfparam name="startrow" default="1">

	<cfset ProgChampFlagStructure = application.com.flag.getFlagStructure(incentiveSettings.ProgChampTextID)>
	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		Select * from (
			SELECT     o.organisationID, o.organisationname as organisation_name, country.LocalCountryDescription as country, reason = CASE WHEN
                          (SELECT     COUNT(personid)
                            FROM          person
                            WHERE      organisationid = o.organisationid) = 0 THEN 'No people' WHEN
                          (SELECT     COUNT(p.personid)
                            FROM          person p INNER JOIN
                                                   rwpersonaccount rwpa ON p.personid = rwpa.personid
                            WHERE      organisationid = o.organisationid AND rwpa.accountdeleted = 0) = 0 THEN 'No People with Accounts' WHEN
                          (SELECT     COUNT(personid)
                            FROM          person
                            WHERE      organisationid = o.organisationid AND personid IN
                                                       (SELECT     data
                                                         FROM          integerflagdata
	                                                         WHERE      entityid = o.organisationid AND flagid =  <cf_queryparam value="#ProgChampFlagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" > )) = 0 THEN 'No PC' ELSE ' OTHER' END, ifd.data as ProgramChampionDataValue
FROM         organisation o INNER JOIN
					country on o.countryID = country.countryID inner join
                      RWCompanyAccount ON o.OrganisationID = RWCompanyAccount.OrganisationID AND o.OrganisationID NOT IN
                          (
						  SELECT DISTINCT RWCompanyAccount.OrganisationID
                            FROM          Country INNER JOIN
                                                   RWCompanyAccount INNER JOIN
                                                   organisation ON RWCompanyAccount.OrganisationID = organisation.OrganisationID INNER JOIN
                                                   RWPersonAccount ON RWCompanyAccount.AccountID = RWPersonAccount.AccountID INNER JOIN
                                                   Person ON organisation.OrganisationID = Person.OrganisationID AND RWPersonAccount.personID = Person.PersonID INNER JOIN
                                                   IntegerFlagData ON Person.PersonID = IntegerFlagData.Data AND organisation.OrganisationID = IntegerFlagData.EntityID ON
                                                   Country.CountryID = organisation.countryID
                            WHERE      (RWCompanyAccount.AccountDeleted = 0) AND (RWPersonAccount.AccountDeleted = 0) AND (Person.Password <> ' ' OR
	                                                   Person.Password IS NOT NULL) AND (IntegerFlagData.FlagID =  <cf_queryparam value="#ProgChampFlagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
								) LEFT OUTER JOIN integerflagdata ifd ON o.organisationid = ifd.entityID AND ifd.flagid =  <cf_queryparam value="#ProgChampFlagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			) as base
			where 1=1
				<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
				order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>

	<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 0>

		<!--- <cfinclude template="pointsManagementHomeFunctions.cfm"> --->

		<CF_tableFromQueryObject queryObject="#getDetails#"
			keyColumnList="Organisation_Name"
			keyColumnURLList="../data/dataFrame.cfm?frmsrchOrgID="
			keyColumnKeyList="organisationID"
			keyColumnOpenInWindowList="yes"
			startrow="#startrow#"
			FilterSelectFieldList = "Reason,Country"
			numRowsPerPage="600"
			useInclude="true"
			showTheseColumns="Organisation_Name,Country,Reason"
			hideTheseColumns="organisationID"
			sortOrder="#sortOrder#"
		>
<!---If no records found--->
<CFELSEIF GetDetails.RecordCount EQ 0>

	There are no orphaned Points at the moment.

</CFIF>


</cf_translate>





