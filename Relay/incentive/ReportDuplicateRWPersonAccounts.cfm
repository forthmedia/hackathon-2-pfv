<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportSODunmatchedLocations.cfm	
Author:				NJH
Date started:		2007/09/14
	
Description:	Produces a report of sod locations that haven't matched to locations in Relayware		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->
 
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="openAsExcel" type="boolean" default="false"> 
<cfparam name="sortOrder" type="string" default="organisation_name,person_name">
<cfparam name="showCols" type="string" default="Organisation_Name,AccountID,Person_Name,PersonID,Person_AccountID">
<cfparam name="numRowsPerPage" default = "100">

<cfparam name="keyColumnList" type="string" default="organisation_name">
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchOrgID=">
<cfparam name="keyColumnKeyList" type="string" default="organisationID">

	
	<cf_head>
		<cf_title>Accounts With Multiple People Report</cf_title>
	</cf_head>
	
	
	<cfinclude template="incentiveTopHead.cfm">

<cfquery name="getPersonAccounts" datasource="#application.siteDataSource#">
	select * from (
		select o.organisationName as organisation_name,o.organisationID, ca.accountID, 
			p.firstName+' '+p.lastName as person_name,p.personID, pa.rwPersonAccountID as person_accountID,
			c.countryDescription as country
		from rwPersonAccount pa inner join rwCompanyAccount ca
				on pa.accountID = ca.accountID left join person p
				on pa.personID = p.personID inner join organisation o
				on o.organisationID = ca.organisationID inner join country c
				on o.countryID = c.countryID
			where pa.accountID in
				(select accountID from rwPersonAccount where testAccount=0 and accountDeleted=0
					group by accountID
					having count(distinct personID) > 1)
			<!--- 2012-07-25 PPB P-SMA001 commented out 		
			and c.countryID in (#request.relayCurrentUser.countryList#)
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="RWPersonAccount",alias="o").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
	) as base
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_tableFromQueryObject 
	queryObject="#getPersonAccounts#"
	queryName="getPersonAccounts"
	sortOrder = "#sortOrder#"
	showTheseColumns="#showCols#"
	openAsExcel="#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOpenInWindowList="yes"
	
	FilterSelectFieldList="Organisation_Name,Country"
>

<cfif not openAsExcel>
	
	
</cfif>

