<!--- ©Relayware. All Rights Reserved 2014 --->
<!---

File:			StatementIncludeV2.cfm
Author:			GCC
Date created:	2006/03/14

Description:	Displays the front page of the statement.
Restyled and layout simplified for users.

Version history:
1

WAB 2006-04-11  opening balance was getting balance at end of 1st day of the month, not the end of the last day of previous month, subtracted one day before calling pointsbalance functions
WAB 2008-04-23 so that can be used as a screen needed to check for no incentive account - added a nasty check and cfabort
WAB 2014-8-19 Remove old functionality - deleteOpenClaim, change to call web service to do the action.
DTR	2014/08/27 Core-545 - statement is not repsonsive, also fix for the case of text-overlapped on Android, ipad and ie8.
--->

<!---
OrganisationID or PersonID can be passed
If they are not passed cookie values are used
in order to work out required values

Parameter statementType (company or personal) can be passed
If not passed, system defaults to personal account
--->

<cf_checkFieldEncryption fieldNames="personID,organisationID,rwTransactionID">

<cfif fileexists("#application.paths.code#\cftemplates\statementIncludeV2.cfm")>
	<!--- Client version of the Company statement loaded on the portal--->
	<cfinclude template ="/code/cftemplates/statementIncludeV2.cfm">
<cfelse>

<cfscript>

	//WORK OUT STATEMENT TYPE
	if (structKeyExists(url, "statementType")){
		statementType = url.statementType;
	}
	if (structKeyExists(form, "statementType")){
		statementType = form.statementType;
	}
	//default to statement Type - personal
	if(not structKeyExists(variables, "statementType")){
		statementType = "company";
	}

	//WORK OUT PERSONID
	if (structKeyExists(url, "personID")){
		personID = url.personID;
	}
	if (structKeyExists(form, "personID")){
		personID = form.personID;
	}
	//work out through cookie
	if(not structKeyExists(variables, "personID")){
		personID = request.relayCurrentUser.personID;
	}


	//WORK OUT ORGANISATIONID
	if (structKeyExists(url, "organisationID")){
		organisationID = url.organisationID;
	}
	if (structKeyExists(form, "organisationID")){
		organisationID = form.organisationID;
	}
	//work out through personID
	if(not structKeyExists(variables, "organisationID")){
		getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID = variables.personID);
		organisationID = getFullPersonDetails.organisationID;
	}


</cfscript>

<!--- default parameters here --->
<!--- <cfparam name="whichmonthdate" default="#dateAdd("m",-1,now())#"> --->

<!--- NJH 2009/08/21 LID 2536 - security release strips out all single quotes. So, put in a check to see that the value is really a date
	 if we replace the slanted quotes for single quotes. If it is, replace slanted quotes with singles again. --->

<cf_includejavascriptOnce template="/incentive/js/statement.js" translate="true">

<cfif isDefined("whichmonthdate")>
	<cfif isDate(replace(whichmonthdate,"`","'","All"))>
		<cfset whichmonthdate = replace(whichmonthdate,"`","'","All")>
	</cfif>
</cfif>

<cfparam name="whichmonthdate" default="#dateAdd('m',0,now())#">
<cfparam name="form.month" default=""> <!--- Saving the month for claims filter ---->
<cfscript>
	if(compareNocase(form.month, "")){
		// this means that user is having a look on previous month.
		whichmonthdate = form.month;
	}
</cfscript>

<cfparam name="datefrom" default="#createdate(year(whichmonthdate), month(whichmonthdate), 1)#">
<cfparam name="dateto" default="#createdate(year(whichmonthdate), month(whichmonthdate), daysinmonth(whichmonthdate))#">
<cfparam name="form.action" default="">

<!---If organisation ID is passed through URL turns it into form variable,
so that for the rest of the processing we can refer to one scope--->
<cfscript>

	//get Org Information
	companyAccount = application.com.relayIncentive.getCompanyAccount(variables.organisationID);
	//adding european date format
	oldlocale=setlocale("English (UK)");

	//originalstartdate = createdate(2000, 10, 1);
	// 2004-08-07 to start date from this company
	originalstartdate = companyAccount.dateTermsAgreed;

	//get the account manager for this partner
	accountManagerDetails = application.com.relayIncentive.getAccountManagerDetails(variables.organisationID);

	personDetails =  application.com.relayIncentive.getPersonDetails(variables.personID);

	//get base points data
	//for company
	if(not compareNocase(variables.statementType, "personal")){
		pointsAccrued = application.com.relayIncentive.getPointsAccrued(variables.organisationID, variables.dateFrom, variables.dateTo, variables.personID);
	}
	//for person
	else if(not compareNocase(variables.statementType, "company")){
		pointsAccrued = application.com.relayIncentive.getPointsAccrued(variables.organisationID, variables.dateFrom, variables.dateTo);
	}



	SAT = 7;
	FirstSatInMonth = SAT - dayofweek(datefrom);
	ASaturday = dateadd("d", FirstSatinMonth, datefrom);
	weeksunion = "select convert(datetime,#ASaturday#) as weekendingdate ";

	while(ASaturday LTE DateTo and ASaturday LTE now()){
		weeksunion = weeksunion & "union select convert(datetime,#ASaturday#) ";
		ASaturday = dateAdd("d", 7, ASaturday);
	}

	//get the next batch of points
	/*arlyExpirePoints = application.com.relayIncentive.getEarlyExpirePoints(variables.organisationID);

	variables.balance = 0;
	if(EarlyExpirePoints.recordCount gt 0){
		for(i=1; i lte earlyExpirePoints.recordCount; i=i+1){
			variables.balance = variables.balance + EarlyExpirePoints.balance[i];
		}
		earlyExpireDate = earlyExpirePoints.expirydate;
		EarlyExpirePoints = 0;
	}
	else{
		earlyExpireDate = now();
		earlyExpirePoints = 0;
	}*/

	/*count up the points available at the end of the month being reported
	It has been decided that the current points available to spend bit should always reflect the end of the current month - i.e
	show all the points available - not the points that were available at the end of a statement month
	We'll use the code below instead therefore, which takes the last day of the current month as dateto*/
	endOfCurrentMonthBalanceDate = createdate(year(now()), month(now()), daysinmonth(now()));

	if(not compareNocase(variables.statementType, "personal")){
		qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,variables.organisationID,variables.personID);
	}

	else if(not compareNocase(variables.statementType, "company")){
		qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,variables.organisationID);
	}

	if(len(trim(qPointsBalance.balance))){
		totalpointsAvailable = qPointsBalance.balance;
	}
	else{
		totalpointsAvailable = 0;
	}

	//reset balancedate to dateto so no other processing is affected
	//balancedate = dateto;

	//count up the points available at the end of the previous month being reported
	// WAB 2006-04-11  subtracted one day from datefrom.  We actually need the balance at the end of the previous day
	balancedate = dateadd('d',-1,datefrom);

	//get balance again
	if(not compareNocase(variables.statementType, "personal")){
		PointsBalance = application.com.relayIncentive.PointsBalance(variables.balancedate,variables.organisationID,variables.personID);
	}

	else if(not compareNocase(variables.statementType, "company")){
		PointsBalance = application.com.relayIncentive.PointsBalance(variables.balancedate,variables.organisationID);
	}

	balancebroughtforward = val(PointsBalance.balance);
</cfscript>

<!--- Partner variables here - taken from queries --->
<cfparam name="PM" default="#companyaccount.pmfirstname# #companyaccount.pmlastname#">
<cfparam name="Company" default="#companyaccount.organisationname#">
<cfparam name="PersonName" default="#persondetails.personname#">

<cfset encryptedURLQueryString = application.com.security.encryptURL(url="#request.query_string_and_script_name#&includeTemplate=sd&organisationID=#variables.organisationID#&personID=#variables.personID#")>
<cfoutput>
<cf_translate>
<SCRIPT type="text/javascript">
	function statementDetail(weekendingdate, bookmark)
	{
		var now = new Date()
		now = now.getTime()
		window.location.href = "#jsStringFormat(encryptedURLQueryString)#&statementType=#jsStringFormat(variables.statementType)#&weekendingdate=" + weekendingdate + "&c=" + now + "##" + (bookmark == null? 'top': bookmark)
	}

	function filterClaim(month){
		// update the value of form.month element used for claims filter;
		document.getElementsByName("month")[0].value= month;
		document.ChooseMonth.submit();



	}

</script>

</cf_translate>
</cfoutput>

<cf_translate>

<cfoutput>

<!--- WAB 2008-04-23 a very nasty check so that this page can be used within a screen --->
<cfif companyAccount.recordCount is 0>
	No Incentive Account
	<CF_ABORT>
</cfif>

<cfif isdefined('hideTagHeader') and not hideTagHeader>
	<h2>phr_statement_Heading</h2>
</cfif>

<div class="row">
	<div class="col-xs-12">
		<dl>
			   <dt>phr_statement_PM</dt>
			   <dd><b>#htmleditformat(PM)#</b> phr_Company <b class="Company">#htmleditformat(Company)#</b></dd>

	  			<cfif not compareNocase(statementType, "personal")>
					<dt></dt>
					<dd> phr_Person <b class="Person">#htmleditformat(PersonName)#</b></dd>
				</cfif>
		</dl>
	</div>
</div>
<!---
<table border="0" width="500" align="center">
	<tr>
		<td align="center">
		phr_statement_PointsExpireOn
		</td>
	</tr>
</table>
 --->
<cfset localClass = "oddrow">
<table  class="responsiveTable table table-striped table-condensed">
	<!--- <tr>
		<!--- netscape is awkward about column widths - this forces them to be correct --->
		<td><img src="../images/misc/spacer.gif" width="109" height="1" alt="" border="0"></td>
		<td><img src="../images/misc/spacer.gif" width="140" height="1" alt="" border="0"></td>
		<td><img src="../images/misc/spacer.gif" width="140" height="1" alt="" border="0"></td>
		<td><img src="../images/misc/spacer.gif" width="15%" height="1" alt="" border="0"></td>
		<td><img src="../images/misc/spacer.gif" width="15%" height="1" alt="" border="0"></td>
		<td><img src="../images/misc/spacer.gif" width="15%" height="1" alt="" border="0"></td>
	</tr> --->
	<thead>
		<tr>
			<th align="right"><b>phr_sys_IncentiveClaims</b></th>
			<th align="right"><b>phr_statement_Earned</b></th>
			<th align="right"><b>phr_statement_Spent</b></th>
			<th align="right"><b>phr_statement_Balance</b></th>
		</tr>
	</thead>

	<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
	<tbody>
	<tr class="#localClass#">
		<td align="left" colspan="3" data-title="phr_sys_IncentiveClaims"><b>phr_incentive_200603_Month_Starting_Balance</b></td>

		<td  data-title="phr_statement_Balance"><b>#lsnumberformat(balancebroughtforward,",")#</b></td>
	</tr>

</cfoutput>



<!---

OK, so we are going to be looping around the weeks in a month -

Only completed weeks will be displayed

--->


<cfset thisWeekEndingDate = "000">
<cfset tmpearned=balancebroughtforward>
<cfset tmpspent=0>
<cfset tmpbalance=balancebroughtforward>


<cfoutput query="PointsAccrued">
	<!---Add points to total only if claim verified--->
	<cfif not compareNoCase("AC", type)>
		<cfset tmpearned = tmpearned + Earned>
	</cfif>

	<cfset tmpspent = tmpspent + spent>
	<cfset tmpbalance = tmpearned - tmpspent>

	<!--- make sure the w/e only appears on the first row in a week --->
	<cfif WeekEndingDate is not thisWeekEndingDate>
		<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>

		<tr class="#localClass#">
			<td colspan="4" align="left"><a class="plain" href="javascript:statementDetail('#urlencodedformat(lsDateFormat(PointsAccrued.weekendingdate,"dd-mmm-yyyy"))#')">phr_statement_WeekEndAbbr #lsDateFormat(weekendingdate,"dd-mmm")#</a>&nbsp;</td>
		</tr>


	</cfif>

	<cfif trim(type) is not "">
		<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
		<tr class="#localClass#" id="#htmleditformat(RWTransactionID)#">
			<cfif not compareNocase(type, "CI")>
				<td data-title="phr_sys_IncentiveClaims" align="left"><a class="plain" href="#application.com.security.encryptURL(url=cgi.script_name&'?etid=ClaimPoints&RWTransactionID='&RWTransactionID&'&personID='&personID)#">phr_incentive_statement_#htmleditformat(type)# #htmleditformat(RWTransactionID)#</a><a href="javascript:delete_claim(#RWTransactionID#, 'phr_incentive_Are_you_sure_delete_Open_Claim', 'phr_incentive_statement_ClaimDeleted', 'phr_statement_error_Attempted_To_Delete_Non_Open_Claim');">phr_incentive_Statement_deleteClaim</a></td>
			<cfelseif listFind("AC,CL", type)>
				<td  data-title="phr_sys_IncentiveClaims" align="left"><a class="plain" href="#application.com.security.encryptURL(url=cgi.script_name&'?etid=ClaimPoints&RWTransactionID='&RWTransactionID&'&readOnly=1&personID='&personID)#">phr_incentive_statement_#htmleditformat(type)# #htmleditformat(RWTransactionID)#</a></td>
			<cfelse>
				<td  data-title="phr_sys_IncentiveClaims" align="left"><a class="plain" href="javascript: statementDetail('#urlencodedformat(lsDateFormat(PointsAccrued.weekendingdate,"dd-mmm-yyyy"))#','#PointsAccrued.type#')">phr_incentive_statement_#htmleditformat(type)# #htmleditformat(RWTransactionID)#</a></td>
			</cfif>

			<td data-title="phr_statement_Earned">#lsnumberformat(earned,",")#</td>
			<td data-title="phr_statement_Spent">#lsnumberformat(Spent,",")#</td>
			<td data-title="phr_statement_Balance">#lsnumberformat(tmpbalance,",")#</td>
		</tr>
	<cfelse>
		<cfif thisWeekendingDate is not "000">
		<tr>
			<td data-title="phr_sys_IncentiveClaims"></td>
			<td  data-title="phr_statement_Earned">0<!--- earned ---></td>
			<td data-title="phr_statement_Spent">0<!--- Spent points in here ---></td>
			<td  data-title="phr_statement_Balance">&nbsp;<!--- balance row ---></td>
		</tr>
		</cfif>
	</cfif>

	<cfset thisWeekEndingDate = WeekEndingDate>

</cfoutput>

<cfoutput>
	<cfset localClass = iif(not CompareNoCase(localClass, "evenrow"), DE("oddrow"), DE("evenrow"))>
	<tr class="#localClass#">
		<td align="left" data-title="phr_sys_IncentiveClaims"><strong><cfif dateTo gt now()>phr_incentive_statement_totalPointsToDate<cfelse>phr_incentive_200603_Month_End_Balance</cfif></strong></td>
		<td data-title="phr_statement_Earned">&nbsp;<!--- earned ---></td>
		<td  data-title="phr_statement_Spent">&nbsp;<!--- Spent points in here ---></td>
		<td  data-title="phr_statement_Balance"><b>#lsnumberformat(tmpBalance,",")#</b></td>
	</tr>
	</tbody>
</table>

<!--- display the footer table --->

<table  class="table table-striped table-condensed">
	<tbody>

	<tr>
		<cfif not isdefined("url.printme")>
			<td class="ToViewPrev">phr_statement_ToViewPrev</td>
			<td>
				<form name="ChooseMonth" action="#cgi.SCRIPT_NAME#?#request.query_string#" method="post">
					<cfset AMonthDate = OriginalStartDate>

					<cfset CurrMonthDate = createdate(year(now()), month(now()), daysinmonth(now()))>

					<select name="whichmonthdate" onchange="filterClaim(this.value)" class="form-control">
						<cfloop condition="AMonthDate LTE CurrMonthDate">
								<option value="#AMonthDate#" #iif(month(DateFrom) is month(AMonthDate) and year(DateFrom) is year(AMonthDate), DE("SELECTED"), DE(""))#>#lsdateformat(AMonthDate, "mm-yyyy")#</option>
							<cfset AMonthDate = dateAdd("m", 1, AMonthDate)>
						</cfloop>
					</select>
				</form>
			</td>

			<cf_encryptHiddenFields>
			<CF_INPUT Type="hidden" name="organisationid" value="#variables.organisationid#">
			<CF_INPUT Type="hidden" name="personid" value="#variables.personid#">
			</cf_encryptHiddenFields>

			<!---These two fields are for deleteing open claims. Simply re-using
			the existing form--->
			<input type="hidden" name="action" value="">
			<input type="hidden" name="month" value=""> <!--- Used for claims filter --->




		</cfif>
	</tr>



	<!--- 'added expiry requirement 1.1.2 - 4.0' [godfrey.smith]- March 06, 2008, 10:12:00 AM --->
	<cfif isDefined("showSoonToExpirePoints") AND showSoonToExpirePoints EQ 1>
	<tr>
		<td colspan="2">
			<!--- ''spacer --->&nbsp;
		</td>
	</tr>

			<!--- ''get the beginning of next month --->
			<cfset expT = "#CreateODBCDateTime(CurrMonthDate + 1)#">

			<!--- ''format to pass in as date type for object --->
			<cfset NextMonthDate = createdate(year(expT), month(expT), 1)>

			<!--- ''DEBUG USE ON DEV <cfif isDefined("URL.showDemo")><cfset NextMonthDate = createdate(2008, 10, 31)></cfif> --->

	<tr>
		<td align="left">
			phr_statement_PointsExpiredOnFirstDayNextMonth (<cfoutput>#DateFormat(CurrMonthDate + 1,"long")#</cfoutput>)
		</td>

			<!--- ''call the fucntion and return query --->
			<cfscript>
			  objGetExpPoints = application.com.relayIncentive.getExpirePointsBeforeDate(expiryDate = NextMonthDate, personid = request.relaycurrentuser.personid);
			</cfscript>

		<td>
		<!--- ''output returned query required data --->
		#NumberFormat(objGetExpPoints.balance, ",")#
		</td>

	</tr>
	</cfif>
	<!--- '/ added expiry requirement 1.1.2 - 4.0' [godfrey.smith]- March 06, 2008, 10:12:00 AM --->



	<cfset str_Contact=#application.com.relayTranslations.translateString("phr_statement_ContactUs")#>
 	<cfif len(str_Contact) gt 0>
		<tr>
			<td colspan="2"><b>#str_Contact#</b></td>
		</tr>
	</cfif>

	<cfset str_Question=#application.com.relayTranslations.translateString("phr_statement_QuestionAccount")#>
 	<cfif len(str_Question) gt 0>
		<tr>
			<td colspan="2">#str_Question#  <!---#AccountManagerDetails.AccMgrName#, #AccountManagerDetails.AccMgrEmail#, #AccountManagerDetails.AccMgrTel#---></td>
		</tr>
	</cfif>

</tbody>
</table>

</cfoutput>



</cf_translate>

<!--- reset the locale --->
<cfset tmp = setlocale(oldlocale)>

</cfif> <!--- End of the outter if to check if a custom version of the file exists in userfiles --->
