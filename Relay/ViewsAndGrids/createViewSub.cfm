<!--- �Relayware. All Rights Reserved 2014 --->
<!--- createviewsub.cfm


really ought to be a subroutine, but htis is a hack --->

<cfset thisGridItem = structCopy(blankGridItem)>
<cfif isDefined("thisColumn.width")>
	<cfset thisGridItem.width = thisColumn.width>
</cfif>
<cfif isDefined("thisColumn.readOnly") and thisColumn.readOnly>
	<cfset thisGridItem.select = "No">
</cfif>
<cfif isDefined("thisColumn.canOrderBY") and thisColumn.canOrderBY>
	<cfset thisGridItem.canOrderBY = true>
</cfif>




<cfif thisColumn.type is "table">
	
		<cfset thisTable = thisColumn.TableName>
		<cfset thisField = thisColumn.field>
		<cfset viewDefinition.whichTables[#thisTable#] = true>
		<cfif isDefined("thisColumn.alias")>
			<cfset alias = thisColumn.alias>
		<cfelseif thisField contains "lastupdated" or thisField Contains "created">
			<cfset alias = "#thisTable#_#thisField#">
		<cfelse>
			<cfset alias = "#thisField#">
		</cfif>

		<cfif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>
		
			<cfset viewdefinition.selectList = LISTAPPEND(viewdefinition.selectList, "#left(thisTable,1)#.#thisField# as #alias# ")>
	
			<cfset viewdefinition.selectListDef = LISTAPPEND(viewdefinition.selectListDef, " 1 as #thisTable#_#thisField# ")>

			<cfset thisGridItem.name = alias>
			<cfset thisGridItem.header = iif(not isdefined("thisColumn.header"),"alias","thisColumn.header")>
			<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>

		</cfif>

			
		
	<cfelseif thisColumn.type is "flag">		
		<cfset flagid = thisColumn.id>
		<cfset theFlagSTructure = application.com.flag.getFlagStructure(flagid)>
		<cfset flagid = theFlagStructure.flagid>  <!--- any textids will be converted to numerics here --->
		<cfif isDefined("thisColumn.tableAlias")>
			<cfset tableAlias = thisColumn.tableAlias>
		<cfelse>
			<cfset tableAlias = "Flag_#flagid#">
		</cfif>

		<cfparam name="thisColumn.joinType" default = "left">
		
		<cfset flagType = theFlagSTructure.flagType.name>
		<cfset flagName = theFlagSTructure.Name>
		<cfset flagName = REReplace(replace(flagname," ","_","ALL"),"[\'\?\-`&\\\/\(\)]","","ALL") >
		<cfset entityType = theFlagSTructure.entityType.tablename>
		<cfset dataTableName = theFlagSTructure.flagType.datatablefullname>
		<cfset viewDefinition.whichTables[#entityType#] = true>
				
		<cfset fieldName = "Flag_#entityType#_#flagType#_#flagid#">
		<cfset LongfieldName = left(flagname,30)>
		<cfif isDefined("thisColumn.field")>
			<cfset flagField = thisColumn.field>
			<cfset fieldName = fieldName & "_" & flagField>
			<cfset LongfieldName = LongfieldName & "_" & flagField>
		<cfelse>		
			<cfset flagField = "data">
		</cfif>
		<cfif isDefined("thisColumn.alias")>
			<cfset alias = thisColumn.alias>
		<cfelseif isDefined("thisColumn.textidAsAlias") and theFlagStructure.flagtextid is not "">
			<cfset alias = theFlagStructure.flagtextid>
		<cfelse>
			<cfset alias = longFieldName>	
		</cfif>



		<cfif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>

			<cfset thisGridItem.name = alias>
			<cfset thisGridItem.header = alias>
	
			
			<cfif (flagType is "radio" or flagType is "checkbox") and flagField is "data">
				<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"case when #tableAlias#.flagid is null then 0 else 1 end as #alias#")>
				<cfset thisGridItem.Type = "boolean">
			<cfelse>
				<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList," #tableAlias#.#flagField# AS #alias# " )>
			</cfif>
				<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #fieldname#")>
				
			<cfif listfind(viewDefinition.flagidsDone,flagid)	is 0 >
				<cfset thisjoin = " #thisColumn.joinType# join #dataTableName#  as #tableAlias#  WITH (NOLOCK) on #left(entitytype,1)#.#ENTITYtype#id = #tableAlias#.entityid and #tableAlias#.flagid = #flagid# ">
				<cfset viewdefinition.joins = viewdefinition.joins & thisjoin>
				<cfset viewDefinition.flagidsDone = listappend(viewDefinition.flagidsDone,flagid)>
			</cfif>

			<cfif theFlagSTructure.linkstoentityTypeid IS NOT "">
			
				<cf_getValidValues validFieldName = "flag.#theFlagSTructure.flagTextid#">
						<cfset thisGridItem.values = valuelist(validvalues.datavalue)>
						<cfset thisGridItem.valuesDisplay = valuelist(validvalues.displayvalue)>						

				
				
				
			
			</cfif>
			
			<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>

		</cfif>

		

	<cfelseif thisColumn.type  is "flaggroup">		

		<cfset flaggroupid = thisColumn.id>
		<cfset theFlagGroupSTructure = application.com.flag.getFlagGroupStructure(flaggroupid)>		
		<cfset flaggroupid = theFlagGroupSTructure.flaggroupid>
		<cfset tableAlias = "Flaggroup_#flaggroupid#">
		<cfset flagGroupType = theFlagGroupSTructure.flagType.Name>
		<cfset flagGroupName = theFlagGroupSTructure.Name>
		<cfset flagGroupName = REReplace(replace(flaggroupname," ","_","ALL"),"[\'\?\-`&\\\/\(\)]","","ALL") >
		<cfset entityType = theFlagGroupSTructure.entityType.tablename>
		<cfset dataTableName = theFlagGroupSTructure.flagType.datatablefullname>
		<cfset viewDefinition.whichTables[#entityType#] = true>
	
		<cfset fieldName = "Flaggroup_#entityType#_#flagGroupType#_#flaggroupid#">
		<cfset LongfieldName = "#flaggroupname#">
		<cfif isDefined("thisColumn.field")>
			<cfif flagGroupType is "event"><cfset longfieldname = "Event"><cfset additionaluniquekey  = "convert(varchar,isnull(#tableAlias#.flagid,0))+ char(45) "></cfif>  <!--- hack! --->
			<cfset flagField = thisColumn.field>
			<cfset fieldName = fieldName & "_" & flagField>
			<cfset LongfieldName = LongfieldName & "_" & flagField>
		<cfelse>		
			<cfset flagField = "data">
		</cfif>
		
		<cfif isDefined("thisColumn.alias")>
			<cfset alias = thisColumn.alias>
		<cfelseif isDefined("thisColumn.textidAsAlias")>
			<cfset alias = theFlagGroupStructure.flaggrouptextid>
		<cfelse>
			<cfset alias = longFieldName>	
		</cfif>

		<cfif flagGroupType is "checkbox" and  ((isDefined("thisColumn.displayas") and thisColumn.displayas is "individualFlags") or (not isDefined("thisColumn.displayas") and isDefined("attributes.showAllCheckboxesIndividually")))>

			<!--- output all items in the flaggroup --->
			<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
			select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
					
			<cfset currentColumn = thisColumn>
			<cfloop query="getFlags">
				<cfset thisColumn.type="flag">
				<cfset thisColumn.id = flagid>
				<cfinclude template="createviewsub.cfm">
			</cfloop>
				<cfset thisGridItem.name = ""> 				<!--- need this to stop last item beign added to the grid twice --->
		
		<cfelseif listfindnocase(viewDefinition.aliasesUsed,alias) is 0>

				<cfset thisGridItem.name = "#alias#">
				<cfset thisGridItem.header = alias>
			
			<cfif flagGroupType is "radio" or flagGroupType is "checkbox">
				<cfif not isDefined("thisColumn.displayas") or thisColumn.displayas is "idlist">
					<cfif isDefined("thisColumn.displayas")>
						<cfset aliasSuffix = "">
					<cfelse>
						<cfset aliasSuffix = "_id"><!--- if no "displayas" is defined then we give both a name list and an id list and we need to distinguish between them--->
					</cfif>
					<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"dbo.BooleanFlagNumericList(#flaggroupid#,#left(entitytype,1)#.#ENTITYtype#id) as #alias##aliasSuffix#")>
					<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #fieldname#")>
	
					<cfset thisGridItem.name = "#alias#">
					<cfset thisGridItem.header = alias>
	
					
	
				</cfif>
		
				<cfif not isDefined("thisColumn.displayas") or thisColumn.displayas is "namelist">
					<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"dbo.BooleanFlagList(#flaggroupid#,#left(entitytype,1)#.#ENTITYtype#id,default) as #alias#")>
					<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #fieldname#_text")>
				</cfif>
				
	
				<!--- get all radio items --->
					<cfif flagGroupType is "radio">
						<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
						select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
					
						<cfset thisGridItem.values = valuelist(getflags.flagid)>
						<cfset thisGridItem.valuesDisplay = valuelist(getflags.name)>						
					</cfif>
			

		
			<cfelseif flaggrouptype is "event">
				<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList," #tableAlias#.#flagField# AS #alias# " )>
				<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #fieldname#")>
				
				<cfif listfind(viewDefinition.flaggroupidsDone,flaggroupid)	is 0 >
					<cfset thisjoin = " left join (#datatablename#  as #tableAlias# inner join flag f on #tableAlias#.flagid = f.flagid and f.flaggroupid = #flaggroupid#) on #left(entitytype,1)#.#ENTITYtype#id = #tableAlias#.entityid ">
					<cfset thisjoin = " left join (select x	.*, f.name from #datatablename#  x inner join flag f on x.flagid = f.flagid and f.flaggroupid = #flaggroupid#) as #tableAlias# on #left(entitytype,1)#.#ENTITYtype#id = #tableAlias#.entityid ">
					<cfset viewdefinition.joins = viewdefinition.joins & thisjoin>
					<cfset viewDefinition.flaggroupidsDone = listappend(viewDefinition.flaggroupidsDone,flaggroupid)>
				</cfif>
		
			<cfelse>
				<!--- output all items in the flaggroup --->
				<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
				select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				
				<cfset currentColumn = thisColumn>
				<cfloop query="getFlags">
					<cfset thisColumn.type="flag">
					<cfset thisColumn.id = flagid>
					<cfinclude template="createviewsub.cfm">
	
				
				</cfloop>
			
			
				
			</cfif>

			<cfset viewDefinition.aliasesUsed = listappend(viewDefinition.aliasesUsed,alias)>

		</cfif>

			
	<cfelseif thisColumn.type  is "Value">					
			<cfset theValue = thisColumn.value>
			<cfset theFieldname = thisColumn.name>
			
			<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"'#theValue#' as #theFieldName#")>
			<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #theFieldName#")>

	<cfelseif thisColumn.type  is "sql">					
			<cfset thesql = thisColumn.value>
			<cfset theFieldname = thisColumn.name>
			<cfset thisGridItem.name = thisColumn.name>
			<cfset thisGridItem.header = thisColumn.name>
			
			<cfset viewdefinition.selectList	= listappend(viewdefinition.selectList,"#thesql# as #theFieldName#")>
			<cfset viewdefinition.selectListDef	= listappend(viewdefinition.selectListDef," 1 as #theFieldName#")>


		
	</cfif>
	

	<cfif thisGridItem.name is not "">			
		<cfif thisGridItem.name contains "updated" or thisGridItem.name contains "created">
			<cfset thisGridItem.select = "No">
		</cfif> 
		<cfset arrayAppend(gridArray,structCopy(thisGridItem))>
	</cfif>		
