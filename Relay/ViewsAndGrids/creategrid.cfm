<!--- �Relayware. All Rights Reserved 2014 --->


<cfparam name="theview">
<cfparam name="definition"  >


<cfparam name="filterOnSelectionIDs" default="">
<cfparam name="FILTER" DEFAULT = "" >
<cfparam name="maxrows" default= "50">
<cfparam name="frmStartAt" default="1">
<cfparam name="frmOrderBy" default="">


<cfset gridArray = definition.gridArray>
<!--- <cfdump var="#gridarray#">    --->

<cfset thegrid = "relayGrid">
<cfset theform = "mainForm">



<!--- do the update --->	
<cfif isDefined("form.fieldnames") and listcontains(fieldnames,"__CFGRID__") is not 0>
	<!--- <cfdump var="#form#">  --->
	<cfset grid = listgetat(fieldnames,listcontains(fieldnames,"__CFGRID__"))>
	<cfset grid = listgetat(grid,3,"__")>
	
	<!--- GET KEY LIST --->
	<cfset fields = "">
	<CFLOOP INDEX="FIELD" LIST = #structKeyList(form)#>
		<CFIF FIELD CONTAINS "#GRID#.original">
			<cfset fields = listappend(fields,listgetat(field,3,"."))>
		
		</CFIF>
	
	</CFLOOP>

	<!--- 	<CFDUMP VAR = "#FORM#">	 --->
	<cfif structKeyExists(form,"#grid#.rowstatus.action")>	
		<cfloop index="i" from = 1 to = #arrayLen(form["#grid#.rowstatus.action"])#>
		
	
			<cfif form["#grid#.rowstatus.action"][i] is "U">
	
				<CFQUERY name="data" datasource="#application.sitedatasource#">
				update #theView#
				set
						<cfloop index = "field" list = "#fields#">
					<cfif field is not "uniqueKey">
						<cfif form["#grid#.#field#"][i] is not form["#grid#.original.#field#"][i] >
							#field# =  <cf_queryparam value="#form["#grid#.#field#"][i]#" CFSQLTYPE="CF_SQL_VARCHAR" >   			,
						</cfif>
					</cfif>
				</cfloop>		
				updatedby = #request.relaycurrentUser.usergroupid#
				where uniqueKey = #form["#grid#.uniquekey"][i]#
				</cfquery>		
			
			</cfif>
		
		
		</cfloop>	
	</cfif>
	
	
</cfif>



<CFQUERY name="data" datasource="#application.sitedatasource#">
select distinct <cfif frmStartAt is not 1><cfset top = frmRecordCount - frmStartAt + 1>top #top# </cfif> v.*  from #theview# v
<cfif filterOnSelectionIDs is not "" and filterOnSelectionIDs is not 0>
	<cfif not definition.whichTables.location>
		inner join person on v.organisationid = p.organisationid
	<cfelseif not definition.whichTables.person>
		inner join person p on p.locationid = v.locationid
	</cfif>
		inner join selectiontag st on st.entityid = personid and st.selectionid  in ( <cf_queryparam value="#filterOnSelectionIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfif>
<cfif filter is not "">
where #FILTER#
</cfif>
order by <cfif frmOrderBy is not "">#frmOrderBy#<cfelse>uniqueKey </cfif><cfif frmStartAt is not 1>desc</cfif>
</CFQUERY>

<cfif frmStartAt is not 1>
<CFQUERY name="data" dbtype="query">
select * from data
order by uniqueKey asc
</cfquery>
</cfif>


<cfif frmStartAt is 1>
	<cfset frmRecordCount = data.recordcount>
</cfif>
<cfset nextpage = frmStartAt + maxrows>
<cfset previouspage = frmStartAt - maxrows>
<cfset lastpage = frmRecordCount - maxrows +1 >


<cfoutput>
<script>
function save(startAt) {
	form = document.#jsStringFormat(theForm)# ;
	if (startAt!= '') {
		form.frmStartAt.value = startAt
	}
	form.submit()
}


</script>

<cfform  method="POST" name="#theform#">
	
	<cfgrid name="#thegrid#" height="500" width="1800" query="data" maxrows="#maxrows#" insert="No" delete="No" sort="Yes" bold="No" italic="No" autowidth="true" appendkey="No" highlighthref="No" griddataalign="LEFT" gridlines="Yes" rowheaders="No" rowheaderalign="LEFT" rowheaderitalic="No" rowheaderbold="No" colheaders="Yes" colheaderalign="LEFT" colheaderitalic="No" colheaderbold="No" selectmode="EDIT" picturebar="No">
		<!--- <cfgridcolumn name="personid" header="personid">
		<cfgridcolumn name="firstname" header="firstname" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" headerbold="No" headeritalic="No">
		<cfgridcolumn name="lastname" header="lastname" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="yes" display="Yes" headerbold="No" headeritalic="No">
		<cfgridcolumn name="sex" header="sex" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="yes" display="Yes" headerbold="No" headeritalic="No" values="M,F" valuesdisplay="Male,Female" valuesdelimiter=","> 
		 ---><!--- <cfgridcolumn name="event_teamcaptain" header="team captain" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="BOOLEAN" headerbold="No" headeritalic="No"> 	 --->
	<cfloop index=I from = 1 to = #arraylen(gridarray)# ><!--- --->

		<cfset thisColumn = gridArray[I]>

			<cfgridcolumn name="#thisColumn.name#" header="#thisColumn.header#" width="#thiscolumn.width#" headeralign="LEFT"  dataalign="LEFT" bold="No" italic="No" select="#thisCOlumn.select#" display="#thisColumn.display#" type="#thisColumn.type#" headerbold="No" headeritalic="No" values="#thisColumn.values#" valuesdisplay="#thisColumn.valuesdisplay#"> 

	</cfloop>

	<cfgridcolumn name="uniquekey" header="key" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="No" display="No" headerbold="No" headeritalic="No">
	
	</cfgrid>
	<br>
<input type="checkbox" name="frmRecreateView" value=1> Check here to rebuild the grid <br>
<CF_INPUT type="hidden"  name="frmRecordCount" value = "#frmRecordCount#">
<CF_INPUT type="hidden"  name="filterOnSelectionIDs" value = "#filterOnSelectionIDs#">
<CF_INPUT type="hidden"  name="frmStartAt" value = "#frmStartAt#">



#min(maxrows,data.recordcount)# rows starting at row #htmleditformat(frmStartAt)#.  Total Rows = #htmleditformat(frmRecordCount)# <BR>


Show 
<select name = "maxRows">
<cfloop index=I from =10 to = 90 step = 10>
<option value="#I#" <cfif I is maxrows>selected</cfif>>#htmleditformat(I)#
</cfloop>
<cfloop index=I from =100 to = 1000 step = 100>
<option value="#I#" <cfif I is maxrows>selected</cfif>>#htmleditformat(I)#
</cfloop>
</select> rows per page
 <BR>

 
Order by 
<select name = "frmOrderBy">
	<option value="">Choose a field to order by
<cfloop index=I from = 1 to = #arraylen(gridarray)# >
	<cfif gridarray[i].canOrderBy>
		<option value="#gridarray[I].name#" <cfif #gridarray[I].name# is frmOrderby>selected</cfif>>#gridarray[I].header#
	</cfif>
</cfloop> 
</select>
<BR>
 
<cfif previouspage gt 0 ><A Href="javascript:save(1)"><<</A> </cfif>&nbsp;&nbsp;&nbsp;
<cfif previouspage gt 0 ><A Href="javascript:save(#previousPage#)"><</A> </cfif>&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;<A Href="javascript:save('')">Save</A>&nbsp;&nbsp;
<cfif nextpage lt frmRecordCount ><A Href="javascript:save(#nextPage#)">></A></A></cfif>&nbsp;&nbsp;&nbsp;
<cfif nextpage lt frmRecordCount ><A Href="javascript:save(#lastPage#)">>></A></A></cfif>&nbsp;&nbsp;&nbsp;

<BR>


<BR> Note: cells will not be updated if the only change is a change of case (sorry!)
<BR> Occasionally data is lost if you click the save button while you are editing a cell.  To prevent this click on another cell before hitting save
<BR> If the grid fails to reappear after a save try clicking the save button again, or click the refresh the browser window by using your right mouse button
</cfform>
</cfoutput>	
