<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

create a view!


 --->


<cfparam name = "attributes.createView" default = true>
<cfparam name = "attributes.outputScripts" default = false>
<cfparam name = "attributes.createSP" default = false>
<cfparam name = "attributes.definitionName" default = "definition">


<!--- 
first bit converts the definition provided as either an xmldocument, string, or screen into a standard array
called columnArray   


 --->

 	<cfset columnArray = arraynew(1)>
 	<cfset joinArray = arraynew(1)>
 	<cfset filterArray = arraynew(1)>

 
<cfif isDefined("attributes.xmlDoc")> 
	<cfparam name="attributes.xmlDoc">
	<cfparam name="attributes.viewName">
 
 	<cfset filepath = application.userFilesAbsolutepath & "\content\" & attributes.xmlDoc>

	<cffile action="read"
 			file="#filepath#"
 			variable="myxml">

	<CFSET XMLstruct = XmlParse(myxml)>
	
  	<cfset elements = XmlSearch(XMLstruct, "//views/view[contains(@name,'#attributes.viewname#')]")>
	
	
	<cfif arrayLen(elements) is 0 >
		<cfoutput>view #attributes.viewname# Not found </cfoutput><CF_ABORT>
	</cfif>
	<cfset definition = elements[1]>
	<cfset xmlcolumnArray = definition.xmlchildren>	

	<!--- convert xml column array list to standard array --->
	
	<cfloop index= "I" from = 1 to = #arrayLen(xmlcolumnArray)# >
		<cfif xmlcolumnArray[I].xmlname is "column">
			<cfset arrayappend(columnArray, xmlcolumnArray[I].xmlattributes)>
		<cfelseif xmlcolumnArray[I].xmlname is "join">
			<cfset arrayappend(joinArray, xmlcolumnArray[I].xmlattributes)>
		<cfelseif xmlcolumnArray[I].xmlname is "filter">
			<cfset arrayappend(filterArray, xmlcolumnArray[I].xmlattributes)>
		</cfif>
		
	</cfloop>
	
	<cfparam name="definition.xmlattributes.filter" default = "">
	<cfset attributes.filter = definition.xmlattributes.filter>


<cfelseif isDefined("attributes.fieldList")>
	<cfparam name="attributes.filter" default="">
	<cfset columnArray = arraynew(1)>	

	<cfloop index = "thisField" list = "#attributes.fieldList#">
		<cfset thisColumn = structNew()>
		<cfset first = listfirst(thisField,"_")>
		<cfset second = listgetAt(thisField,2,"_")>
		
		<cfif listfindnocase("person,location,organisation",first) is not 0>
			<cfset thisColumn.type = "table">
			<cfset thisColumn.tableName = first>
			<cfset thisColumn.field = second	>
		<cfelseif listfindnocase("flagGroup,flag",first) is not 0>
			<cfset thisColumn.type = first>
			<cfset thisColumn.id = second>
			<cfif listLen(thisField,"_") gt 2>
				<cfset thisColumn.field = listrest(listrest(thisField,"_"),"_")>
			</cfif>
			<cfset thisColumn.id = second>
		<cfelseif first is "value" or first is "sql">
			<cfset thisColumn.type = first>
			<cfset thisColumn.value = second>
			<cfset thisColumn.name = listgetAt(thisField,3,"_")>
		</cfif>
		
		<cfset arrayappend(columnArray, thisColumn)>
	</cfloop>
	
<cfelseif isDefined("attributes.screenid")>

	<cfset screenid = attributes.screenid>
	<cfset countryid = 0>
	<cfinclude template = "\screen\qryScreenviewdefinition.cfm">

	<cfparam name="attributes.filter" default="">
	
	<cfloop query = getScreenDefinition>
		
		<cfset thisColumn = structNew()>
		
		<cfif listfindnocase("person,location,organisation",fieldsource) is not 0>
			<cfset thisColumn.type = "table">
			<cfset thisColumn.tableName = fieldsource>
			<cfset thisColumn.field = fieldtextid	>
			<cfset arrayappend(columnArray, thisColumn)>
		<cfelseif listfindnocase("flagGroup,flag",fieldsource) is not 0>
			<cfset thisColumn.type = fieldSource>
			<cfset thisColumn.id = fieldtextid>
			<cfset arrayappend(columnArray, thisColumn)>
		<cfelseif fieldsource is "value">
			<cfset thisColumn.type = "value">
			<cfset thisColumn.value = fieldtextid>
			<cfset arrayappend(columnArray, thisColumn)>
		</cfif>
		

	
	</cfloop>

<!--- 	<cfdump var= "#columnArray#"> --->
</cfif>



<!--- 
Now we loop through column array creating a definition for the view (viewDefinition) 
and an array defining a grid 

 --->


 
<!--- 
loop through working out which tables used 
--- for now assume all three

--->
<cfset viewdefinition = structNew()>
<cfset viewdefinition.selectList = "">
<cfset viewdefinition.selectListDef = "">
<cfset viewdefinition.joins = "">

<cfset viewDefinition.flagidsDone = "">
<cfset viewDefinition.flaggroupidsDone = "">
<cfset viewDefinition.aliasesUsed = "">

<cfset viewDefinition.whichTables = structNew()>
<cfset viewDefinition.whichTables.person = false>
<cfset viewDefinition.whichTables.location = false>
<cfset viewDefinition.whichTables.organisation = false>


<cfset gridArray = arrayNew (1)>

<cfset blankGridItem = structNew()>
<cfset blankGridItem.name = ""> 
<cfset blankGridItem.header = ""> 
<cfset blankGridItem.type = "STRING_NOCASE"> 
<cfset blankGridItem.values = ""> 
<cfset blankGridItem.width = "0"> 
<cfset blankGridItem.select = "yes"> 
<cfset blankGridItem.display = "yes"> 
<cfset blankGridItem.canOrderBy = "false"> 
<cfset blankGridItem.valuesdisplay = ""> 



<cfloop index= "I" from = 1 to = #arrayLen(columnArray)# >

	<cfset thisColumn = columnArray[I]>

	<cfinclude template="createviewsub.cfm">
	
	
</cfloop>






<!--- Which tables do we need to join on --->
<cfset mainJoin = " organisation o WITH (NOLOCK) ">
<cfset viewdefinition.selectList =  listappend(" o.organisationid ",viewdefinition.selectList)>
<cfset viewdefinition.selectListDef = listappend(" 1 as organisationid ",viewdefinition.selectListDef)>
<cfset uniquekey = "convert(varchar,o.organisationid)">
	<cfset thisGridItem = structCopy(blankGridItem)>
	<cfset thisGridItem.Name = "organisationid">
	<cfset thisGridItem.Header = "OrganisationID">
	<cfset thisGridItem.Select = "no">
	<cfset thisGridItem.Width = "30">	
	<cfset arrayappend(gridArray,structCopy(thisGridItem))>


<cfif viewDefinition.whichTables.person or viewDefinition.whichTables.location>
	<cfset mainJoin = mainJoin & " left join location l  WITH (NOLOCK) on o.organisationid = l.organisationid  ">
	<cfset viewdefinition.selectList =  listappend(" l.locationid ",viewdefinition.selectList)>
	<cfset viewdefinition.selectListDef = listappend(" 1 as locationid ",viewdefinition.selectListDef)>
	<cfset uniquekey =  "convert(varchar,l.locationid)">
		<cfset thisGridItem = structCopy(blankGridItem)>
		<cfset thisGridItem.Name = "locationid">
		<cfset thisGridItem.Header = "LocationID">
		<cfset thisGridItem.Select = "no">
		<cfset thisGridItem.Width = "30">
		<cfset arrayappend(gridArray,structCopy(thisGridItem))>

</cfif>

<cfif viewDefinition.whichTables.person >
	<cfset mainJoin = mainJoin & " left join person p  WITH (NOLOCK) on l.locationid = p.locationid  ">
	<cfset viewdefinition.selectList =  listappend(" p.personid ",viewdefinition.selectList)>
	<cfset viewdefinition.selectListDef = listappend(" 1 as personid ",viewdefinition.selectListDef)>
	<cfset uniquekey =  "convert(varchar,p.personid)">
		<cfset thisGridItem = structCopy(blankGridItem)>
		<cfset thisGridItem.Name = "Personid">
		<cfset thisGridItem.Header = "PersonID">
		<cfset thisGridItem.Select = "no">
		<cfset thisGridItem.Width = "30">
		<cfset arrayappend(gridArray,structCopy(thisGridItem))>

</cfif>


<cfset viewdefinition.joins = mainjoin & viewdefinition.joins>

<cfif isDefined("additionaluniquekey")>
	<cfset uniquekey =  uniquekey & " + char(45) + " & additionaluniquekey>
</cfif>


<cfloop index= "I" from = 1 to = #arrayLen(joinArray)# >
	<cfset viewdefinition.joins = viewdefinition.joins & joinArray[I].sql>
</cfloop>


<cfset viewdefinition.selectList = listappend("#uniquekey# as uniquekey ",viewdefinition.selectList)> 
<cfset viewdefinition.selectListDef = listappend(" 1 as uniquekey  ",viewdefinition.selectListDef)>


	
	<cfset viewDefinition.gridArray = gridarray>
	<cfset "caller.#attributes.DefinitionName#" = viewDefinition>

	



<cfif attributes.outputScripts or attributes.createView>
	<cftry>
	<cftransaction>
	
		<cfset query = "	create view dbo.#attributes.viewName#  as 
							select #viewdefinition.selectList#, 0 as updatedby 
							from #viewdefinition.joins#">
		<cfif attributes.filter is not "">
			<cfset query = query & " where #attributes.filter#">
		<cfelseif arraylen(filterArray) is not 0>
			<cfset query = query & " where ">
			<cfloop index= "I" from = 1 to = #arrayLen(filterArray)# >
					<cfset query  = query  & filterArray[I].sql & " and ">
			</cfloop>
					<cfset query  = query  &  " 1 = 1 ">
		</cfif>					
							
	<cfif attributes.outputScripts><CFOUTPUT>#QUERY#</cfoutput> <BR><BR><BR></cfif> 
	
		<cfif attributes.createView>
			<cfquery name="dropView" datasource = "#application.sitedatasource#">
			if exists (select * from information_schema.tables where table_type = 'view' and table_name =  <cf_queryparam value="#attributes.viewname#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					drop view #attributes.viewname#
				end	
			</cfquery>
			<cfquery name="createView" datasource = "#application.sitedatasource#">
			#preserveSingleQuotes(query)#
			</cfquery>
		</cfif>
			
		
	
		<cfset query = "	create view dbo.#attributes.viewName#_def as 
							select #viewdefinition.selectListDef# , 0 as updatedby 
							from dual">
							
	
	<cfif attributes.outputScripts><CFOUTPUT>#htmleditformat(QUERY)#</cfoutput> <BR><BR><BR></cfif>
	
		<cfif attributes.createView>
			<cfquery name="dropView" datasource = "#application.sitedatasource#">
			if exists (select * from information_schema.tables where table_type = 'view' and table_name =  <cf_queryparam value="#attributes.viewname#_def" CFSQLTYPE="CF_SQL_VARCHAR" > )
				begin
					drop view #attributes.viewname#_def
				end	
			</cfquery>
		
			<cfquery name="createView" datasource = "#application.sitedatasource#">
			#query#
			</cfquery>
			
	<!--- 	<cfquery name="droptrigger" datasource = "#application.sitedatasource#">
		if exists (select * from information_schema.routines where table_type = 'view' and table_name = '#attributes.viewname#_def')
			begin
				drop view #attributes.viewname#_def
			end	
		</cfquery>
	 --->
			<cfquery name="createTrigger" datasource = "#application.sitedatasource#">
			create trigger #attributes.viewName#InsteadOf on #attributes.viewName# instead of update,insert,delete
			as 
		
			select * into ##D from deleted
			select * into ##I from inserted
		
			exec updateDynamicView '#attributes.viewName#'  
			</cfquery>
		</cfif>
	
	</cftransaction>	
	
	<cfcatch>
		<cfrethrow>
	</cfcatch>	
	</cftry>
</cfif>

<cfset createSp = attributes.createSP>
<cfif not createSP>
	<cfquery name="checkForSP" datasource = "#application.sitedatasource#">
		select * from information_schema.routines where routine_type = 'procedure' and routine_name = 'updateDynamicView'
	</cfquery>
	<cfif checkForSP.recordcount is 0 >
		<cfset createSP = true>
	</cfif>

</cfif>


<cfif createSP>

	<cfquery name="dropSP" datasource = "#application.sitedatasource#">
	if exists (select * from information_schema.routines where routine_type = 'procedure' and routine_name = 'updateDynamicView')
		begin
			drop procedure updateDynamicView
		end	
	</cfquery>


<cfquery name="createSP" datasource = "#application.sitedatasource#">

/*   
A stored procedure for updating views made up of a mixture of tables and flags

The views have to be created in a particular way - currently using createview.cfm

Basically each view has an associated view (myView_def) which defines where each field comes from



*/



create procedure dbo.updateDynamicView 
	@viewName varchar(50)

as 

	declare	
	@ColName	varchar(80),
	@ColIdentifier	varchar(80),
	@Colid		int,
	@mod8 int,
	@rem8 int,
	@bit int,
	@bitvalue int,
	@byte int,
	@flagid varchar(10),
	@flaggroupid varchar(10),
	@flagtype varchar(30),
	@entitytype varchar(30),
	@flagtable varchar(30),
	@tableType varchar(30),
	@fieldname varchar(30),
	@rest varchar(80),
	@sqlcmd		nvarchar(2000),
	@pointer1 int,
	@pointer2 int,
	@theDate varchar(25),
	@Item1 varchar(30),
	@Item2 varchar(30),
	@Item3 varchar(30),
	@Item4 varchar(30),
	@Item5 varchar(30)

	set nocount on 

	select @theDate = '''' + convert(varchar,getDate()) +'''' 

--	select * from ##d
--	select * from ##i

--	print getdate()

	declare cols insensitive cursor for
	Select c.colid, c.name, c2.name
		from (sysobjects s 
			inner join 
	              syscolumns c on s.id =c.id and s.name= @viewName and s.type ='V' )
			inner join 
		      (sysobjects s2 
			inner join 
	              syscolumns c2 on s2.id =c2.id and s2.name= @viewName + '_def'  and s2.type ='V' )			
			on c.colid = c2.colid

-- and charindex('_',c.name) <> 0
	order by c.colid
		
	open cols


	print COLUMNS_UPDATED()
	fetch next from cols into 
		@colid,@ColName,@colIdentifier

	while @@fetch_status = 0 
	begin

--		print @colid
--		print @colname

       if charindex('_',@colIdentifier) <> 0
 	  begin


		select @bit = (@colid - 1 )% 8 + 1  -- refers to which bit in the byte
		select @bitvalue = power(2,@bit-1)  
		select @byte = ((@colid - 1) / 8) + 1  -- refers to which byte in columns_updated
		

--		print @colname +': byte ' + convert(varchar,@mod8) + '.  bit ' + convert(varchar,@rem8)
		
--		print @colname
--		print 'bit :' + convert(varchar,((@colid - 1 )% 8 + 1) )
--		print 'bit :' + convert(varchar,power(2, ((@colid - 1 )% 8 + 1) - 1) )
--		print 'pmc: ' + convert(varchar,power(2,(@rem8-1)))
--		print 'byte :' + convert(varchar,((@colid - 1) / 8) + 1)
--		print @bit
--		print @byte



		if SUBSTRING(COLUMNS_UPDATED(),@byte,1) & @bitvalue > 0
		begin
			print 'update ' + @colname + ' = ' + @colIdentifier
			
			select @rest = @colIdentifier
			Select @TableType = left(@rest,charindex('_',@rest)-1)
			select @rest = right (@rest,len(@rest) - charindex('_',@rest)) + '_'

			Select @Item2 = left(@rest,charindex('_',@rest)-1)
			select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'

			Select @Item3 = left(@rest,charindex('_',@rest)-1)
			select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'

			Select @Item4 = left(@rest,charindex('_',@rest)-1)
			select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'

			Select @Item5 = left(@rest,charindex('_',@rest)-1)
			select @rest = right (@rest,len(@rest) - charindex('_',@rest))+ '_'


--			print 'table ' + @tableType
--			print 'rest ' + @rest


		  if @tableType = 'Flag'
		  begin
						
			Select @EntityType = @Item2
			Select @flagtype = @Item3 
			Select @flagid = @Item4
			select @fieldname = @Item5

			if @fieldname = ''
				select @fieldname = 'data'

			print 'updating flag: ' + @entityType + ' ' + @flagType + ' ' +@flagid + ' ' +@fieldname


--			select @flagid = right (@colname,len(@colname) - charindex('_',@colname))


			if @flagtype not in ('radio','checkbox')
			begin
				select @flagTable = @flagType + 'flagdata'			
			  	IF @fieldname = 'data'	
				  begin -- flags with single data columns , need to be inserted and deleted if the old/new value is null, else do an update
					select @sqlcmd = 'update ' + @flagtable
						+ ' set ' +@fieldname + ' = i.' + @colname
						+ ' ,lastupdated = ' + @theDate
						+ ' ,lastupdatedby = i.updatedby '
						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
						+ ' inner join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
						+ ' and i.' + @colname + ' <> d.' + @colname 
--						+ ' and d.' + @colname + ' is not null'
						+ ' and i.' + @colname + ' is not null'
	
					exec (@sqlcmd)
		
					select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid,createdby,created,lastupdatedby,lastupdated' +@fieldname + ')'
						+ ' select i.' + @entityType + 'id, i.updatedby,' + @theDate + 'i.updatedby,' + @theDate + ',' + @flagid + ', i.' + @colname
 						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
						+ ' and i.' + @colname + ' is not null'
						+ ' and d.' + @colname + ' is null'

					exec (@sqlcmd)

					-- set lastupdatedby before deleting

					select @sqlcmd = 'update ' + @flagtable 
						+ 'set lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
						+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
						+ ' and d.' + @colname + ' is not null'
						+ ' and i.' + @colname + ' is null'

					exec (@sqlcmd)
					
					select @sqlcmd = 'delete ' + @flagtable 
						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
						+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
						+ ' and d.' + @colname + ' is not null'
						+ ' and i.' + @colname + ' is null'
	
					exec (@sqlcmd)
		
				  end   -- end of flags with a single data column
				else
				  begin  -- flags with multiple data columns , don't delete if a field is null, may need to do inserts
       	 					
					select @sqlcmd = 'insert into ' + @flagtable + '(flagid,entityid,createdby,created,lastupdatedby,lastupdated' + @fieldname + ')'
							+' select distinct ' + @flagid + ',i.' + @entityType + 'id, i.updatedby,' + @theDate + ',i.updatedby,' + @theDate + ', i.' + @colname
							+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
							+ ' left join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
							+ ' where isnull(i.' + @colname + ','''') <> isnull(d.' + @colname + ','''')'
							+ ' and fd.flagid is null  '

--				print @sqlcmd
				exec (@sqlcmd)


					select @sqlcmd = 'update ' + @flagtable
							+ ' set ' +@fieldname + ' = i.' + @colname
							+ ' ,lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
							+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
							+ ' inner join ' +  @flagtable +  ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
							+ ' where isnull(i.' + @colname + ','''') <> isnull(d.' + @colname + ','''')'
	
--				print @sqlcmd
				exec (@sqlcmd)
	
				  

				  end


	end  -- end of other flags
			  else
			begin  -- booleanflagdata flags

			select @flagtable = 'booleanflagdata'

			select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid,createdby,created,lastupdatedby,lastupdated)'
				+ ' select distinct i.' + @entityType + 'id, ' + @flagid + ',i.updatedby,' + @theDate + ',i.updatedby,' + @theDate 
				+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
				+ ' and isnull(i.' + @colname + ',0) = 1 '
				+ ' and isnull(d.' + @colname + ',0) = 0 '

			exec (@sqlcmd)

			select @sqlcmd = 'update ' + @flagtable 
				+ ' set lastupdatedby = i.updatedby , lastupdated = ' + @theDate  
				+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'				
				+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
				+ ' and isnull(i.' + @colname + ',0) = 0 '
				+ ' and isnull(d.' + @colname + ',0) = 1 '

			exec (@sqlcmd)
			
			select @sqlcmd = 'delete ' + @flagtable 
				+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'				
				+ ' inner join ' + @flagtable + ' fd on fd.entityid = i.' + @entityType + 'id and fd.flagid = ' + @flagid
				+ ' and isnull(i.' + @colname + ',0) = 0 '
				+ ' and isnull(d.' + @colname + ',0) = 1 '

			exec (@sqlcmd)
			end


			


		  end  -- end of flag columns
		  else IF @tableType in ('person','location','organisation')
		  begin  -- update a main table column

			select @fieldname = @item2

				print 'update table ' + @tableType + ' ' + @fieldname
					select @sqlcmd = 'update ' + @tableType
						+ ' set ' +@fieldname + ' = i.' + @colname
						+ ' ,lastupdatedby = i.updatedby ,lastupdated = ' + @theDate
						+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
						+ ' inner join ' +  @tableType +  ' x on x.' + @tableType + 'id = i.' + @tableType + 'id '
						+ ' and i.' + @colname + ' <> d.' + @colname 
	
					exec (@sqlcmd)


			

		  end



		  else IF @tableType = 'FlagGroup'
		  begin  -- update a FlagGroup

			Select @EntityType = @Item2
			Select @flagtype = @Item3
			Select @flaggroupid = @Item4
			select @fieldname = @Item5

			if @flagType = 'checkbox' or @flagType = 'radio' 
			begin
				select @flagtable = 'booleanflagdata'	

			-- delete items in deleted but not in inserted
			select @sqlcmd = 'delete booleanflagdata  '
				+ ' from '
				+ '  ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
				+ ' inner join flag f on (dbo.listfind(i.'+@colname+',f.flagid) = 0 and dbo.listfind(i.'+@colname+',f.name) = 0)and f.flaggroupid = ' + @flaggroupid
				+ ' and (dbo.listfind(d.'+@colname+',f.flagid) = 1 or dbo.listfind(d.'+@colname+',f.name) = 1)' 
				+ ' inner join booleanflagdata bfd on bfd.flagid = f.flagid and bfd.entityid = i.' + @entityType + 'id ' 
				
--			print @sqlcmd
			exec (@sqlcmd)
						
			-- insert items not already in the table (i.e in the inserted but  not in deleted)
			select @sqlcmd = 'insert into ' + @flagtable + ' (entityid,flagid)'
				+ ' select distinct i.' + @entityType + 'id, f.flagid '
				+ ' from ##i i inner join ##d d on i.uniqueKey = d.uniquekey'
				+ ' inner join flag f on (dbo.listfind(i.'+@colname+',f.flagid) =1 or dbo.listfind(i.'+@colname+',f.name) =1) and f.flaggroupid = ' + @flaggroupid
				+ ' and (dbo.listfind(d.'+@colname+',f.flagid) = 0 and dbo.listfind(d.'+@colname+',f.name) = 0)' 

--			print @sqlcmd
			exec (@sqlcmd)





			end

			

		  end


		end  -- end of updated columns


	  end	-- end of column names with _ in them
		fetch next from cols into 
			@colid,@ColName,@colIdentifier
	end  -- end of while loop
	
	close cols
	deallocate cols


</cfquery>

</cfif> 