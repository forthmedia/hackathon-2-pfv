This folder is for JARs that are included within coldfusion but we want our java classes to be aware of them. 

You should only place JARs in this location if you know that they exist in the cfusion/lib (or equivalent) folder.
E.g. C:\ColdFusion10\cfusion\lib