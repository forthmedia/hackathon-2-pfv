/**
 * StructAccessor
 *
 * @author Richard.Tingle
 * @date 30/09/15
 **/
component accessors=true output=false persistent=false {

	public any function init(any wrappingService){
		this.wrappingService=arguments.wrappingService;
		return this;
	}

	public any function createComponent(String cannonicalName, array interfacesToWrap, struct constructorArguments){

		var createdObject= createObject("component", cannonicalName);

		if (structKeyExists(createdObject,"init")){
			if (structKeyExists(arguments,"constructorArguments")){
				createdObject.init(argumentCollection=constructorArguments);
			}else{
				createdObject.init();
			}
		}

		var wrappedObject=this.wrappingService.createInstance(createdObject,interfacesToWrap);

		return wrappedObject;
	}

}