/**
 * A wrapper for Query that is more java friendly
 * 
 * @author Richard.Tingle
 * @date 15/04/16
 **/
component accessors=true output=false persistent=false extends="com.adobe.coldfusion.query" {
	public any function init(){
		return this;
	}
	
	public void function setAttribute(String attributeName, any attribute){
		var argumentCol={};
		argumentCol[attributeName]=attribute;
		this.setAttributes(argumentCollection=argumentCol);
	}
	
	/**
	 * addParam is the method that has no real arguments (sigh) hence requiring this wrapper to explicitly name them
	 **/
	public void function addParamByOrder(required String name,required String value, required String SQLType, list="false" ){
		this.addParam(name=name,value=value,cfsqltype=SQLType,list=list);
	}

}