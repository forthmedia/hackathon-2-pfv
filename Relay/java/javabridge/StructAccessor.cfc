/**
 * StructAccessor
 * 
 * @author Richard.Tingle
 * @date 30/09/15
 **/
component accessors=true output=false persistent=false {

	public numeric function getInteger(string structAddress) output="false" {

		var parts=listToArray(structAddress,".");

		if (parts[1] EQ "application"){

			return JavaCast("int",application[parts[2]]);


		}


		return -1;
	}
	
	public any function getWrappedObject(string structAddress, string javaInterface){
		var object=get(structAddress);
		var arrayInterfaces=arrayNew(1);
		arrayAppend(arrayInterfaces,javaInterface);
		
		return application.CFCDynamicProxy.createInstance(object,arrayInterfaces);
	}
	
	
	public any function get(string structAddress){
		var parts=listToArray(structAddress,".");

		if (parts[1] EQ "application"){
			var selectedStruct=application;
		}else if (parts[1] EQ "request"){
			var selectedStruct=request;
		}else if (parts[1] EQ "session"){
			var selectedStruct=request;
		}else{
			throw (message="struct #structAddress# not supported, must be application, request or session");
		}
		
		
		var runningItem=selectedStruct;
		for(var i=2;i LTE arrayLen(parts);i=i+1){
			runningItem=runningItem[parts[i]];
		}
		return runningItem;
	}
	

}