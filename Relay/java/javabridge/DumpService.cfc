/**
 * Wraps the CFDump funtionality for use in java (since CFCs can be injected, functions can't)
 *
 * @author Richs
 * @date 04/10/15
 **/
component accessors=true output=false persistent=false {


	//writing to screen doesn't seem to work (presumably because it looses its place going via java

	public void function writeToFile(required any itemToDump, required string PathToFile, string format="text") output="false" {
		writedump("attempted dump");
		writedump(var=itemToDump,format=format, output=PathToFile);
	}
}