/**
 * WrappingService
 * 
 * @author Richard.Tingle
 * @date 18/04/16
 **/
component accessors=true output=true persistent=false {


	public any function init(required any CFCDynamicProxy) output="false" {
		this.CFCDynamicProxy=arguments.CFCDynamicProxy;
		return this;
	}

	public any function createInstance(required any createdObject, required any interfacesToWrap) output="true" {
		/* TODO: Implement Method */
		return this.CFCDynamicProxy.createInstance(createdObject,interfacesToWrap);
	}
}