/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.opensaml.DefaultBootstrap;
import singleSignOn.samlserviceprovider.idps.IDPMetadataProcessor.Metadata;

/**
 *
 * @author Richard.Tingle
 */
public class IPDMetadataProcessorTest {
    
    public static String IDPmetadata="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "<md:EntityDescriptor entityID=\"rt-portal\" validUntil=\"2026-04-06T15:33:28Z\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">\n" +
                            "		<md:IDPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">\n" +
                            "				<md:KeyDescriptor use=\"signing\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMDANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzLAmd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7jsrnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZVg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlIscHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm\"/>\n" +
                            "				\n" +
                            "		</md:IDPSSODescriptor>\n" +
                            "	</md:EntityDescriptor>";
    
     public static String IDPmetadata_noCert="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "<md:EntityDescriptor entityID=\"rt-portal\" validUntil=\"2026-04-06T15:33:28Z\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">\n" +
                            "		<md:IDPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm\"/>\n" +
                            "				\n" +
                            "		</md:IDPSSODescriptor>\n" +
                            "	</md:EntityDescriptor>";
    
    
    public static String IDPmetadata_alsoEncryption="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "<md:EntityDescriptor entityID=\"rt-portal\" validUntil=\"2026-04-06T15:33:28Z\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">\n" +
                            "		<md:IDPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">\n" +
                            "				<md:KeyDescriptor use=\"encryption\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>EVIL2</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				<md:KeyDescriptor use=\"signing\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMDANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzLAmd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7jsrnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZVg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlIscHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				<md:KeyDescriptor use=\"encryption\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>EVIL</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm\"/>\n" +
                            "				\n" +
                            "		</md:IDPSSODescriptor>\n" +
                            "	</md:EntityDescriptor>";
    
    public static String IDPmetadata_alsoRedirectBinding="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "<md:EntityDescriptor entityID=\"rt-portal\" validUntil=\"2026-04-06T15:33:28Z\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">\n" +
                            "		<md:IDPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">\n" +
                            "				<md:KeyDescriptor use=\"signing\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMDANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzLAmd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7jsrnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZVg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlIscHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-REDIRECT\" Location=\"http://BAD/BAD.cfm\"/>\n" +
                            "				\n" +
                            "				\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm\"/>\n" +
                            "				\n" +

                            "		</md:IDPSSODescriptor>\n" +
                            "	</md:EntityDescriptor>";

    @BeforeClass
    public static void setUpClass() throws Exception {
        DefaultBootstrap.bootstrap();
    }

    
    @Test
    public void readIDPMetadata() throws Exception{
        Metadata expectedMetadata=Metadata.valid(
                "rt-portal",
                "http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm"
        );
        
        IDPMetadataProcessor processor=new IDPMetadataProcessor();
        
        Metadata actualMetadata=processor.readIDPMetadata(IDPmetadata);
        
        assertEquals(expectedMetadata,actualMetadata);
        
    }
    
    @Test
    public void readIDPMetadata_ignoreEncryptionCert() throws Exception{
        Metadata expectedMetadata=Metadata.valid(
                "rt-portal",
                "http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm"
        );
        
        IDPMetadataProcessor processor=new IDPMetadataProcessor();
        
        Metadata actualMetadata=processor.readIDPMetadata(IDPmetadata_alsoEncryption);
        
        assertEquals(expectedMetadata,actualMetadata);
        
    }
    @Test
    public void readIDPMetadata_ignoreRedirectBinding() throws Exception{
        Metadata expectedMetadata=Metadata.valid(
                "rt-portal",
                "http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm"
        );
        
        IDPMetadataProcessor processor=new IDPMetadataProcessor();
        
        Metadata actualMetadata=processor.readIDPMetadata(IDPmetadata_alsoRedirectBinding);
        
        assertEquals(expectedMetadata,actualMetadata);
        
    }
    
    @Test
    public void readIDPMetadata_noCertificate() throws Exception{
 
        
        IDPMetadataProcessor processor=new IDPMetadataProcessor();
        
        Metadata actualMetadata=processor.readIDPMetadata(IDPmetadata_noCert);
        
        assertFalse("Metadata with no certificate isn't valied",actualMetadata.isValid());
        
    }
    
}
