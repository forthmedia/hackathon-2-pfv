/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider;

import java.util.List;
import java.util.Map;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Response;

/**
 *
 * @author Richard.Tingle
 */
public class SAMLAssertionInterpretorTest {
    
  
    //This is a valid assertion but without signatures
    static String assertion_nameIDAndAttributes_string="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer>"
                    + "<saml2p:Status>"
                        + "<saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/>"
                    + "</saml2p:Status>"
                    + "<saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer>rt-portal</saml2:Issuer>"
                + "<saml2:Subject>"
                    + "<saml2:NameID Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:persistent\">5008</saml2:NameID>"
                    + "<saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\">"
                        + "<saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/>"
                    + "</saml2:SubjectConfirmation>"
                + "</saml2:Subject>"
                + "<saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AudienceRestriction>"
                        + "<saml2:Audience>dev19-gaext</saml2:Audience>"
                    + "</saml2:AudienceRestriction>"
                + "</saml2:Conditions>"
                + "<saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AuthnContext>"
                        + "<saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef>"
                    + "</saml2:AuthnContext>"
                + "</saml2:AuthnStatement>"
                + "<saml2:AttributeStatement>"
                    + "<saml2:Attribute Name=\"FIRSTNAME\">"
                        + "<saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Example</saml2:AttributeValue>"
                    + "</saml2:Attribute>"
                    + "<saml2:Attribute Name=\"LASTNAME\">"
                        + "<saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Reseller</saml2:AttributeValue>"
                    + "</saml2:Attribute>"
                + "</saml2:AttributeStatement>"
                + "</saml2:Assertion>"
            + "</saml2p:Response>";
    
    //this is a pretty stupid assertion, but it tests that the system accepts bits of the assertion being missing (which is acceptable)
    static String assertion_noNameIDAndNoAttributes_string="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer>"
                    + "<saml2p:Status>"
                        + "<saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/>"
                    + "</saml2p:Status>"
                    + "<saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer>rt-portal</saml2:Issuer>"
                + "<saml2:Subject>"
                    + "<saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\">"
                        + "<saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/>"
                    + "</saml2:SubjectConfirmation>"
                + "</saml2:Subject>"
                + "<saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AudienceRestriction>"
                        + "<saml2:Audience>dev19-gaext</saml2:Audience>"
                    + "</saml2:AudienceRestriction>"
                + "</saml2:Conditions>"
                + "<saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AuthnContext>"
                        + "<saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef>"
                    + "</saml2:AuthnContext>"
                + "</saml2:AuthnStatement>"
                + "</saml2:Assertion>"
            + "</saml2p:Response>";
    
    
    static String assertion_multipartAttribute_string="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer>"
                    + "<saml2p:Status>"
                        + "<saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/>"
                    + "</saml2p:Status>"
                    + "<saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"
                + "<saml2:Issuer>rt-portal</saml2:Issuer>"
                + "<saml2:Subject>"
                    + "<saml2:NameID Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:persistent\">5008</saml2:NameID>"
                    + "<saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\">"
                        + "<saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/>"
                    + "</saml2:SubjectConfirmation>"
                + "</saml2:Subject>"
                + "<saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AudienceRestriction>"
                        + "<saml2:Audience>dev19-gaext</saml2:Audience>"
                    + "</saml2:AudienceRestriction>"
                + "</saml2:Conditions>"
                + "<saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\">"
                    + "<saml2:AuthnContext>"
                        + "<saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef>"
                    + "</saml2:AuthnContext>"
                + "</saml2:AuthnStatement>"
                + "<saml2:AttributeStatement>"
                    + "<saml2:Attribute Name=\"multi\">"
                        + "<saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Val1</saml2:AttributeValue>"
                        + "<saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Val2</saml2:AttributeValue>"
                    + "</saml2:Attribute>"
                + "</saml2:AttributeStatement>"
                + "</saml2:Assertion>"
            + "</saml2p:Response>";
    
    
    static ResponseStringPreprocessor responseStringPreprocessor=new ResponseStringPreprocessor();
    

    
    public SAMLAssertionInterpretorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        DefaultBootstrap.bootstrap();
    }

    @Test
    public void testInterpret_assertionWithNameIDAndAttributes() throws Exception {
        Response assertion_nameIDAndAttributes=responseStringPreprocessor.stringToResponse(assertion_nameIDAndAttributes_string);
        
        SAMLAssertionInterpretor interpretor=new SAMLAssertionInterpretor();
        
        AssertionContentReport report=interpretor.interpret(assertion_nameIDAndAttributes);
        
        assertEquals("Must pull NameID from assertion","5008",report.getNameID());
        assertTrue(report.hasNameID());
        
        Map<String,List<String>> attributes=report.getAttributes();
        
        assertTrue(attributes.containsKey("FIRSTNAME"));
        assertEquals("Example",attributes.get("FIRSTNAME").get(0));
        assertTrue(attributes.containsKey("LASTNAME"));
        assertEquals("Reseller",attributes.get("LASTNAME").get(0));
        
    }

    @Test
    public void testInterpret_assertionWithNoNameIDAndNoAttributes() throws Exception  {
        Response assertion_noNameIDAndNoAttributes=responseStringPreprocessor.stringToResponse(assertion_noNameIDAndNoAttributes_string);
        
        SAMLAssertionInterpretor interpretor=new SAMLAssertionInterpretor();
        
        AssertionContentReport report=interpretor.interpret(assertion_noNameIDAndNoAttributes);
        
        assertEquals("Must report null for no nameID",report.getNameID(),null);
        assertFalse(report.hasNameID());

        assertEquals(0,report.getAttributes().size());

    }
    
    @Test
    public void testInterpret_multipartAttributes() throws Exception  {
        Response assertion_multipartAttribute=responseStringPreprocessor.stringToResponse(assertion_multipartAttribute_string);
        
        SAMLAssertionInterpretor interpretor=new SAMLAssertionInterpretor();
        
        AssertionContentReport report=interpretor.interpret(assertion_multipartAttribute);
        
        //all this checks that a single attribute having multiple values is acceptable (this is likely in things like usergroups)
        assertEquals(1,report.getAttributes().size());
        assertEquals(2,report.getAttributes().get("multi").size());
        assertEquals("Val1",report.getAttributes().get("multi").get(0));
        assertEquals("Val2",report.getAttributes().get("multi").get(1));
    }
    
}
