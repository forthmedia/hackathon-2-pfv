/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.isA;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.signature.SignatureValidator;
import static singleSignOn.samlserviceprovider.AssertionValidityReport.accept;
import singleSignOn.samlserviceprovider.idps.IDPManager;
import singleSignOn.samlserviceprovider.idps.IdentityProvider;
import singleSignOn.samlserviceprovider.mocks.CertificateProviderMock;

/**
 *
 * @author Richard.Tingle
 */
@RunWith(MockitoJUnitRunner.class)
public class SAMLAssertionValidatorTest {
    private static final String REQUEST_ID = "809707f0030a5d00620c9d9df97f627afe9dcc24";
    private final List<String> requestIds = Arrays.asList(REQUEST_ID);
    private final SAMLValidator samlValidator = new SAMLValidator();
    @Mock
    private IDPManager idpManagerMock;
    @Mock
    private Response responseMock;
    @Mock
    private Issuer issuerMock;
    @Mock
    private SAMLValidator samlValidatorMock;
    @Mock
    private MetadataProvider metadataProviderMock;
    @Mock
    private IdentityProvider identityProviderMock;
    
    DateTime now_forTests=new DateTime(2016, 4,6, 15, 36, 35, 893,DateTimeZone.UTC);
    
    DateTime now_justAfterNotBefore=new DateTime(2016, 4,6, 15, 16, 41, 000,DateTimeZone.UTC);
    DateTime now_BeforeNotBefore_by5Minutes=new DateTime(2016, 4,6, 15, 11, 41, 000,DateTimeZone.UTC);
    
    

    DateTime now_justBeforeNotOnOrAfter=new DateTime(2016, 4,6, 15, 55, 00, 000,DateTimeZone.UTC);
    DateTime now_afterNotOnOrAfter_by5Minutes=new DateTime(2016, 4,6, 16, 00, 00, 000,DateTimeZone.UTC);
    
    
    String goodAssertions_signedKeyA="<?xml version=\"1.0\" encoding=\"UTF-8\"?><saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#id9a7046fe-c1a2-4931-a982-42bd69466876\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"><ec:InclusiveNamespaces xmlns:ec=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"xs\"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>UUkMHPxW91iIxEwDIJ+XypULHsI=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>X8JiAXV1wZ3Hm7GpNpX1x/L8WvK9KOBuXKxy4dtOKr86BR3Ra1jv2jF1+KWDEtY3gbCEvcffFx64+Qk/e4I2b6MdzTR5991Q+laynDVJUzVctafDUjjGOJ+9Buo5MMVpR6YWiUp5zKV9vKAPdulizwT66sGXN6f3mApWL261sokU9norGcn14HFQ37D4osuCngzn1eg5DBNiYYmdsoEgaml5N7N9fRShaShzGUlvHJnjnEYU6i+n/ZQ0l9su2R/NVKZjB5b9bK0s5rb6RJXhQ1CLymPFp1+Jkqii9wDB83e3EheTot94bD//SaBJqZquMubFOJbkNXIzkOOih4BSpQ==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3\n" +
                    "YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJl\n" +
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn\n" +
                    "+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8\n" +
                    "vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMD\n" +
                    "ANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzL\n" +
                    "Amd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1\n" +
                    "etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67\n" +
                    "PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7j\n" +
                    "srnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZ\n" +
                    "Vg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlI\n" +
                    "scHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2p:Status><saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/></saml2p:Status><saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer>rt-portal</saml2:Issuer><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#idda19e9c9-017c-40a9-8577-d7dfc67f7317\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"><ec:InclusiveNamespaces xmlns:ec=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"xs\"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>W4/ozTVrRf9EVdRsSmD5HWbSCJw=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>fRHY1pmFOf2w5BUNue4krIP6Rb8C9nqyrQw2w1CwKiFjeN77QeIFlwyYPSiBcSYF9mEaJZsMJpqF1VpJQCCRhVw97CSozG/KUa1fnmaLUKRLqbsv/rZ+22k0yZF52BczWDwkv3D/H+fKlemD4cfktoUx0KFfE4ywJCrvWKLzF6j/yoKXLONB7GuNRE/fT3L1+hBO1zAnHGhzhYgOwAjaSSB9wasOPp3KFMZn7AC17ilmwcuW2CMrK0NERIN4FAHCewJRd5eb3mOQonFWWwNj0Gf+b5ZbMNvLvLF91WLjWQUw7U1ADlmIoA2OWmX/KTxrNWLr8MkWf0V36oZ6NZaOzA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3\n" +
                    "YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJl\n" +
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn\n" +
                    "+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8\n" +
                    "vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMD\n" +
                    "ANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzL\n" +
                    "Amd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1\n" +
                    "etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67\n" +
                    "PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7j\n" +
                    "srnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZ\n" +
                    "Vg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlI\n" +
                    "scHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2:Subject><saml2:NameID Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:persistent\">5008</saml2:NameID><saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\"><saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/></saml2:SubjectConfirmation></saml2:Subject><saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AudienceRestriction><saml2:Audience>dev19-gaext</saml2:Audience></saml2:AudienceRestriction></saml2:Conditions><saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AuthnContext><saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef></saml2:AuthnContext></saml2:AuthnStatement><saml2:AttributeStatement><saml2:Attribute Name=\"FIRSTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Example</saml2:AttributeValue></saml2:Attribute><saml2:Attribute Name=\"LASTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Reseller</saml2:AttributeValue></saml2:Attribute></saml2:AttributeStatement></saml2:Assertion></saml2p:Response>";

    String tamperedAssertions_signedKeyA_tampererdNameID="<?xml version=\"1.0\" encoding=\"UTF-8\"?><saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#id9a7046fe-c1a2-4931-a982-42bd69466876\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"><ec:InclusiveNamespaces xmlns:ec=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"xs\"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>UUkMHPxW91iIxEwDIJ+XypULHsI=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>X8JiAXV1wZ3Hm7GpNpX1x/L8WvK9KOBuXKxy4dtOKr86BR3Ra1jv2jF1+KWDEtY3gbCEvcffFx64+Qk/e4I2b6MdzTR5991Q+laynDVJUzVctafDUjjGOJ+9Buo5MMVpR6YWiUp5zKV9vKAPdulizwT66sGXN6f3mApWL261sokU9norGcn14HFQ37D4osuCngzn1eg5DBNiYYmdsoEgaml5N7N9fRShaShzGUlvHJnjnEYU6i+n/ZQ0l9su2R/NVKZjB5b9bK0s5rb6RJXhQ1CLymPFp1+Jkqii9wDB83e3EheTot94bD//SaBJqZquMubFOJbkNXIzkOOih4BSpQ==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3\n" +
                    "YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJl\n" +
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn\n" +
                    "+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8\n" +
                    "vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMD\n" +
                    "ANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzL\n" +
                    "Amd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1\n" +
                    "etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67\n" +
                    "PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7j\n" +
                    "srnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZ\n" +
                    "Vg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlI\n" +
                    "scHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2p:Status><saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/></saml2p:Status><saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer>rt-portal</saml2:Issuer><ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"/><ds:SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><ds:Reference URI=\"#idda19e9c9-017c-40a9-8577-d7dfc67f7317\"><ds:Transforms><ds:Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><ds:Transform Algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\"><ec:InclusiveNamespaces xmlns:ec=\"http://www.w3.org/2001/10/xml-exc-c14n#\" PrefixList=\"xs\"/></ds:Transform></ds:Transforms><ds:DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><ds:DigestValue>W4/ozTVrRf9EVdRsSmD5HWbSCJw=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>fRHY1pmFOf2w5BUNue4krIP6Rb8C9nqyrQw2w1CwKiFjeN77QeIFlwyYPSiBcSYF9mEaJZsMJpqF1VpJQCCRhVw97CSozG/KUa1fnmaLUKRLqbsv/rZ+22k0yZF52BczWDwkv3D/H+fKlemD4cfktoUx0KFfE4ywJCrvWKLzF6j/yoKXLONB7GuNRE/fT3L1+hBO1zAnHGhzhYgOwAjaSSB9wasOPp3KFMZn7AC17ilmwcuW2CMrK0NERIN4FAHCewJRd5eb3mOQonFWWwNj0Gf+b5ZbMNvLvLF91WLjWQUw7U1ADlmIoA2OWmX/KTxrNWLr8MkWf0V36oZ6NZaOzA==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3\n" +
                    "YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJl\n" +
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn\n" +
                    "+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8\n" +
                    "vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMD\n" +
                    "ANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzL\n" +
                    "Amd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1\n" +
                    "etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67\n" +
                    "PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7j\n" +
                    "srnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZ\n" +
                    "Vg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlI\n" +
                    "scHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml2:Subject><saml2:NameID Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:persistent\">5009</saml2:NameID><saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\"><saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/></saml2:SubjectConfirmation></saml2:Subject><saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AudienceRestriction><saml2:Audience>dev19-gaext</saml2:Audience></saml2:AudienceRestriction></saml2:Conditions><saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AuthnContext><saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef></saml2:AuthnContext></saml2:AuthnStatement><saml2:AttributeStatement><saml2:Attribute Name=\"FIRSTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Example</saml2:AttributeValue></saml2:Attribute><saml2:Attribute Name=\"LASTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Reseller</saml2:AttributeValue></saml2:Attribute></saml2:AttributeStatement></saml2:Assertion></saml2p:Response>";

    
    String badAssertion_noSignature="<?xml version=\"1.0\" encoding=\"UTF-8\"?><saml2p:Response xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" Destination=\"http://dev19-gaext/saml.cfm\" ID=\"id9a7046fe-c1a2-4931-a982-42bd69466876\" InResponseTo=\"ONELOGIN_809707f0030a5d00620c9d9df97f627afe9dcc24\" IssueInstant=\"2016-04-06T15:36:35.893Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">rt-portal</saml2:Issuer><saml2p:Status><saml2p:StatusCode Value=\"urn:oasis:names:tc:SAML:2.0:status:Success\"/></saml2p:Status><saml2:Assertion xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\" ID=\"idda19e9c9-017c-40a9-8577-d7dfc67f7317\" IssueInstant=\"2016-04-06T15:36:40.864Z\" Version=\"2.0\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"><saml2:Issuer>rt-portal</saml2:Issuer><saml2:Subject><saml2:NameID Format=\"urn:oasis:names:tc:SAML:2.0:nameid-format:persistent\">5008</saml2:NameID><saml2:SubjectConfirmation Method=\"urn:oasis:names:tc:SAML:2.0:cm:bearer\"><saml2:SubjectConfirmationData NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\" Recipient=\"http://dev19-gaext/saml.cfm\"/></saml2:SubjectConfirmation></saml2:Subject><saml2:Conditions NotBefore=\"2016-04-06T15:16:40.864Z\" NotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AudienceRestriction><saml2:Audience>dev19-gaext</saml2:Audience></saml2:AudienceRestriction></saml2:Conditions><saml2:AuthnStatement AuthnInstant=\"2016-04-06T15:36:40.864Z\" SessionNotOnOrAfter=\"2016-04-06T15:56:40.864Z\"><saml2:AuthnContext><saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml2:AuthnContextClassRef></saml2:AuthnContext></saml2:AuthnStatement><saml2:AttributeStatement><saml2:Attribute Name=\"FIRSTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Example</saml2:AttributeValue></saml2:Attribute><saml2:Attribute Name=\"LASTNAME\"><saml2:AttributeValue xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">Reseller</saml2:AttributeValue></saml2:Attribute></saml2:AttributeStatement></saml2:Assertion></saml2p:Response>";

    
    
    //the basic metadata that has the certificate inserted into it
    String IDPmetadata_base="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                            "<md:EntityDescriptor entityID=\"rt-portal\" validUntil=\"2026-04-06T15:33:28Z\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:md=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">\n" +
                            "		<md:IDPSSODescriptor protocolSupportEnumeration=\"urn:oasis:names:tc:SAML:2.0:protocol\">\n" +
                            "				<md:KeyDescriptor use=\"signing\">\n" +
                            "					<ds:KeyInfo>\n" +
                            "						<ds:X509Data>\n" +
                            "							<ds:X509Certificate>INSERTED_CERTIFICATE</ds:X509Certificate>\n" +
                            "						</ds:X509Data>\n" +
                            "					</ds:KeyInfo>\n" +
                            "				</md:KeyDescriptor>\n" +
                            "				\n" +
                            "				<md:SingleSignOnService Binding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Location=\"http://rt-portal/singleSignOn/SAML/IDP/endpoints/idpPost.cfm\"/>\n" +
                            "				\n" +
                            "		</md:IDPSSODescriptor>\n" +
                            "	</md:EntityDescriptor>";
    
    
    
    String keyA="MIICqDCCAZCgAwIBAgIIVBbwq0vCeLkwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3YXJlMB4XDTE2MDQwNjE1MzMyOFoXDTI2MDQwNjE1MzMyOFowFDESMBAGA1UEAwwJUmVsYXl3YXJlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuV0oCkZp3o58+gzKBx3l9Rd16PjesXn+zNEtlZLTE3D0aUwjl6rGH2za2LFoUvLGCdVwPHJsmHpjK5AdnVXX9Ysok7eJCUkFxCvGWaICGb8vz/FhZlatdrWL0ZQuZ9FvYaTmnmJioMgON3Mb+tY8mRf5jYKDwYhZljeJXEO62/p+q1L8sT6NHMDANV4AybIoXKl/kI2759nmGob1MzwgEeLgIrVn7Fx+AToMUhQ9StNTb+6Y5J7h9R6IDh5Cnb3aHzLAmd2xTv4s7pb93oMu1bZ26r7xIay3mbxxixWzZVVcr8LSBM5CVR3/KNQaE8jILZmd6Y9aW7nuyN1etvp2QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBsf7H+9V27YWNad8lpsiWaX5MlqlZH6gOw2Q67PYo+mVYa4pA7/ezqavBG9MM3WHzI0VMakRoEtgB0oq+6aj/f+qkqAGT4pqEHTiAXPDnavMzyck7jsrnrQy1k6FWurMfIHhfYJwq8FfjbdHa6EX/jDD2rUJi5hToetyKyPoO1QpReh58Zp8sJkWoYrGnZVg2T1q45/oOASl2/k915wx0lZQZtWzShAY5co6uJo0lhGyHi3e8HHV6xrQ9I1cTqRo8U3hBChIlIscHyZXPMbO1ENAUWlfzGE/WYeFLCFUnhPYvZW98u9EKrdxOsTMU8Jf6oMu7ixK8J1GRY6i4EXwd+";
    String keyB="MIICqDCCAZCgAwIBAgIIKQUftvXHq/IwDQYJKoZIhvcNAQEFBQAwFDESMBAGA1UEAwwJUmVsYXl3YXJlMB4XDTE2MDQwNzA5NTcwNFoXDTI2MDQwNzA5NTcwNFowFDESMBAGA1UEAwwJUmVsYXl3YXJlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5/24mxZAQ0TSpdA3WvmaKhiQd8Qsp0Q78PHeYXXcMbq1CukYZuvCY70F2Fkc4zYXW3XfeqC0RmUvDkWTckreWnNZfx1Ph4qLcD007WHSqsTjPBlKX5aqxVe7fJXY+0aMRk/eBumXdywRF5wVAhoZkwadD2VtERpUXdNQ+o0Uz+8VDpd4W9Z3W+YSK0lTnCk8P+iAaLgCe6fxlIL66iJZAUxbXmVqIwtdkESl7srUUFD4wllXAR6BRxRP/5zYpYXo1Th1tQtZYp/EjDdyqMhhXsPWK2Hw3Yx1LexmOvSjn6SW8FxHsdqS4Rl58x1x/iRn6SiKyuIsrsTZ1n23Y86EhwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQDL2sdCLDsmI8g5UV/cFrO0PifYTZ3mWuU3bWCSF7XBYDRo5T0VPKrkB8jFh1hSZ9xIv9qchrFFSz52uUA9B4igfCycC1a1g+f2i4kpKp+9Mu2a+a3xpxGiHkNl2etihvLgvJaGWCYkBsbNTTtVPXwPS6N2RcKt+uawBQwm7czqTpZjnP3yE0CkUexWZP46iy5F6avMJJO5PXXMXtfTsbBn1cuFktzPbB1q9WRWUmnn2dZRMlCMK9vBqwUi3HZ3AN/m01n0AwZXo9oNHIU791hdc+Ghml0HCBrASyCFyD8+WUyQ9r3MdMzrWQoYG8ufl/vqb8EtW7MHHiexmVHMoHxo";
    MetadataProvider trustsKeyA=new CertificateProviderMock("rt-portal",IDPmetadata_base.replace("INSERTED_CERTIFICATE", keyA));
    MetadataProvider trustsKeyB=new CertificateProviderMock("rt-portal",IDPmetadata_base.replace("INSERTED_CERTIFICATE", keyB));
    MetadataProvider trustsOtherProvider=new CertificateProviderMock("BAD","NotImportant");

    
    
    
    public SAMLAssertionValidatorTest() {

    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        DefaultBootstrap.bootstrap();
    }

    @Test
    public void testValidate_validCertificateCorrectKey() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_forTests, requestIds);
        
        assertTrue("Must accept a valid Response with a trusted certificate", report.isValid());

    }

    @Test
    public void testValidate_tamperedAssertion() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(tamperedAssertions_signedKeyA_tampererdNameID,now_forTests, requestIds);
        
        assertFalse("Must reject a tampered assertion", report.isValid());
        
        assertEquals("Must reject a tampered assertion",AssertionRejectionReason.SignatureWasInvalid, report.getReason());
        
    }
    
    @Test
    public void testValidate_validCertificateIncorrectKey() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyB,0, idpManagerMock, samlValidator);
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_forTests, requestIds);
        assertFalse("Must reject valid Response with an untrusted trusted certificate", report.isValid());
        assertEquals("Must reject valid Response with an untrusted trusted certificate due to wrong cert",AssertionRejectionReason.SignatureWasInvalid, report.getReason());
        
    }

    @Test
    public void testValidate_untrustedIssuer() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsOtherProvider,0, idpManagerMock, samlValidator);
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_forTests,requestIds);
        assertFalse("Must reject valid Response with an unknown issuer", report.isValid());
        assertEquals("Must reject valid Response with an unknown issuer",AssertionRejectionReason.IssuerNotRecognised, report.getReason());

    }
    
    @Test
    public void testValidate_noSingatures() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        AssertionValidityReport report=validator.validate(badAssertion_noSignature,now_forTests,requestIds);
        assertFalse("Must reject valid Response with an unknown issuer", report.isValid());
        assertEquals("Must reject valid Response with an unknown issuer",AssertionRejectionReason.NoSignature, report.getReason());

    }

    /**
     * Right in the middle of the assertions validityTime
     * @throws Exception 
     */
    @Test
    public void testValidate_nowIsAtIssueTime() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0,idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_forTests,requestIds);
        
        assertTrue("Accept when current time inside valid region", report.isValid());

    }
    
    /**
     * just inside validityTime
     * @throws Exception 
     */
    @Test
    public void testValidate_justAfterStartOfValidity() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_justAfterNotBefore,requestIds);
        
        assertTrue("Accept when current time inside valid region", report.isValid());

    }
    
    /**
     * just outside validityTime
     * @throws Exception 
     */
    @Test
    public void testValidate_justBeforeStartOfValidity() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_BeforeNotBefore_by5Minutes,requestIds);
        
        assertFalse("Reject when current time outside valid region", report.isValid());
        assertEquals("Reject when current time outside valid region",AssertionRejectionReason.NotInPermittedTimePeriod, report.getReason());    
    }
    
    /**
     * just outside validityTime, but region of permission expanded
     * @throws Exception 
     */
    @Test
    public void testValidate_justBeforeStartOfValidityButTimeExpanded() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,10, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_BeforeNotBefore_by5Minutes,requestIds);
        
        assertTrue("Reject when current time outside valid region", report.isValid());
 
    }
    

    @Test
    public void testValidate_justBeforeNotOnOrAfter() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,10, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_justBeforeNotOnOrAfter,requestIds);
        
        assertTrue("Reject when current time outside valid region", report.isValid());
 
    }
    
    @Test
    public void testValidate_afterNotOnOrAfter() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,0, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_afterNotOnOrAfter_by5Minutes,requestIds);
        
        assertFalse("Reject when current time outside valid region", report.isValid());
        assertEquals("Reject when current time outside valid region",AssertionRejectionReason.NotInPermittedTimePeriod, report.getReason());    
    }
    
    @Test
    public void testValidate_afterNotOnOrAfter_butValidityExpanded() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,10, idpManagerMock, samlValidator);
        
        AssertionValidityReport report=validator.validate(goodAssertions_signedKeyA,now_afterNotOnOrAfter_by5Minutes,requestIds);
        
        assertTrue("Reject when current time outside valid region", report.isValid());
  
    }
    
    @Test
    public void shouldRejectWhenInResponseToNotContainsRequestId() throws Exception {
    	SAMLAssertionValidator validator=new SAMLAssertionValidator(trustsKeyA,10, idpManagerMock, samlValidator);
    	AssertionValidityReport result = validator.validate(goodAssertions_signedKeyA,now_BeforeNotBefore_by5Minutes, Arrays.asList("RequestId"));
    	assertFalse("Should reject when inResponseTo doesn't contain request id.", result.isValid());
    	assertEquals(AssertionRejectionReason.InResponseToContainsInvalidId, result.getReason());
    }
    
    @Test 
    public void shouldRejectWhenInResponseToIsMissingAndNotPermitted() throws Exception {
    	SAMLAssertionValidator validator=new SAMLAssertionValidator(metadataProviderMock,0, idpManagerMock, samlValidatorMock);
        mockValidatorsForInResponseToCase();
        
        Mockito.when(responseMock.getInResponseTo()).thenReturn(null);
        Mockito.when(idpManagerMock.isPermittedToInitiateSAML(isA(String.class))).thenReturn(false);
        
        AssertionValidityReport result = validator.validate(responseMock,now_forTests, requestIds);
    	
        Mockito.verify(idpManagerMock).isPermittedToInitiateSAML(isA(String.class));
        assertFalse("Should reject when inResponseTo is missing in the received response.", result.isValid());
    	assertEquals(AssertionRejectionReason.IdentityProviderIsNotPermitted, result.getReason());
    }
    
    @Test 
    public void shouldRejectWhenInResponseToIsEmptyAndNotPermitted() throws Exception {
        SAMLAssertionValidator validator=new SAMLAssertionValidator(metadataProviderMock,0, idpManagerMock, samlValidatorMock);
        mockValidatorsForInResponseToCase();
        
        Mockito.when(responseMock.getInResponseTo()).thenReturn("");
        Mockito.when(idpManagerMock.isPermittedToInitiateSAML(isA(String.class))).thenReturn(false);
    	AssertionValidityReport result = validator.validate(responseMock,now_afterNotOnOrAfter_by5Minutes, requestIds);
        
        Mockito.verify(idpManagerMock).isPermittedToInitiateSAML(isA(String.class));
    	assertFalse("Should reject when inResponseTo is empty in the received response.", result.isValid());
    	assertEquals(AssertionRejectionReason.IdentityProviderIsNotPermitted, result.getReason());
    }

    private void mockValidatorsForInResponseToCase() throws SQLException {
        Mockito.when(responseMock.getIssuer()).thenReturn(issuerMock);
        Mockito.when(issuerMock.getValue()).thenReturn("rt-portal");
        Mockito.when(metadataProviderMock.hasIssuer("rt-portal")).thenReturn(true);
        Mockito.when(metadataProviderMock.getRawMetadataForIssuer("rt-portal")).thenReturn(IDPmetadata_base.replace("INSERTED_CERTIFICATE", keyA));
        Mockito.when(responseMock.isSigned()).thenReturn(true);
        Mockito.when(samlValidatorMock.validateSignature(isA(Response.class), isA(SAMLSignatureProfileValidator.class), isA(SignatureValidator.class))).thenReturn(accept());
        Mockito.when(samlValidatorMock.validateAssertion(isA(Response.class), isA(SignatureValidator.class), Mockito.anyInt(), isA(DateTime.class))).thenReturn(accept());
        Mockito.when(metadataProviderMock.getIdentityProvider("rt-portal")).thenReturn(identityProviderMock);
        Mockito.when(identityProviderMock.getIdentityProviderName()).thenReturn("identityProviderName");
        Mockito.when(idpManagerMock.isPermittedToInitiateSAML(isA(String.class))).thenReturn(false);
    }
}
