/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider.mocks;

import singleSignOn.samlserviceprovider.MetadataProvider;
import singleSignOn.samlserviceprovider.idps.IdentityProvider;

/**
 *
 * @author Richard.Tingle
 */
public class CertificateProviderMock implements MetadataProvider {
    
    private final String trustProvider;
    private final String metadataToReturn;

    public CertificateProviderMock(String trustProvider, String metadataToReturn) {
        this.trustProvider = trustProvider;
        this.metadataToReturn = metadataToReturn;
    }
            
    @Override
    public boolean hasIssuer(String issuer) {
        return issuer.equals(trustProvider);
    }

    @Override
    public String getRawMetadataForIssuer(String issuer) {
        if (hasIssuer(issuer)){
            return metadataToReturn;
        }else{
            throw new RuntimeException("No such issuer");
        }
    }

    @Override
    public IdentityProvider getIdentityProvider(String issuer) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
