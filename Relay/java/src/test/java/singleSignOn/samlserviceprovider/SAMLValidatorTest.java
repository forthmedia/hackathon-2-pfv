package singleSignOn.samlserviceprovider;

import java.util.Arrays;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Response;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.validation.ValidationException;
import static singleSignOn.samlserviceprovider.AssertionRejectionReason.NotInPermittedTimePeriod;
import static singleSignOn.samlserviceprovider.AssertionRejectionReason.SignatureWasInvalid;
import static singleSignOn.samlserviceprovider.AssertionRejectionReason.SignatureWasMalformed;

@RunWith(MockitoJUnitRunner.class)
public class SAMLValidatorTest {

    private final DateTime now = new DateTime(2016, 4, 6, 15, 36, 35, 893, DateTimeZone.UTC);
    private final DateTime before = new DateTime(2016, 3, 6, 15, 36, 35, 893, DateTimeZone.UTC);
    private final DateTime after = new DateTime(2016, 5, 6, 15, 36, 35, 893, DateTimeZone.UTC);

    @Mock
    private Response responseMock;

    @Mock
    private SAMLSignatureProfileValidator profileValidatorMock;

    @Mock
    private SignatureValidator signatureValidatorMock;

    @Mock
    private Signature signatureMock;

    @Mock
    private Assertion assertionMock;

    @Mock
    private Conditions conditionsMock;

    private SAMLValidator testedObject;

    @Before
    public void setUp() {
        testedObject = new SAMLValidator();
    }

    @Test
    public void shouldValidateSignatureProperly() throws ValidationException {
        when(responseMock.getSignature()).thenReturn(signatureMock);

        AssertionValidityReport result = testedObject.validateSignature(responseMock, profileValidatorMock, signatureValidatorMock);
        assertTrue(result.isValid());
        verify(profileValidatorMock).validate(signatureMock);
        verify(signatureValidatorMock).validate(signatureMock);
    }

    @Test
    public void shouldRejectWhenSignatureWasMalformed() throws ValidationException {
        when(responseMock.getSignature()).thenReturn(signatureMock);
        Mockito.doThrow(ValidationException.class).when(profileValidatorMock).validate(signatureMock);

        AssertionValidityReport result = testedObject.validateSignature(responseMock, profileValidatorMock, signatureValidatorMock);
        assertFalse(result.isValid());
        assertEquals(SignatureWasMalformed, result.getReason());
    }

    @Test
    public void shouldRejectWhenSignatureWasInvalid() throws ValidationException {
        when(responseMock.getSignature()).thenReturn(signatureMock);
        Mockito.doThrow(ValidationException.class).when(signatureValidatorMock).validate(signatureMock);

        AssertionValidityReport result = testedObject.validateSignature(responseMock, profileValidatorMock, signatureValidatorMock);
        assertFalse(result.isValid());
        assertEquals(SignatureWasInvalid, result.getReason());
    }

    @Test
    public void shouldValidateAssertionProperlyWhenIsSigned() throws ValidationException {
        when(responseMock.getAssertions()).thenReturn(Arrays.asList(assertionMock));
        when(assertionMock.isSigned()).thenReturn(true);
        when(assertionMock.getSignature()).thenReturn(signatureMock);
        when(assertionMock.getConditions()).thenReturn(conditionsMock);
        when(conditionsMock.getNotBefore()).thenReturn(before);
        when(conditionsMock.getNotOnOrAfter()).thenReturn(after);

        AssertionValidityReport result = testedObject.validateAssertion(responseMock, signatureValidatorMock, 0, now);
        assertTrue(result.isValid());
        verify(signatureValidatorMock).validate(signatureMock);
    }

    @Test
    public void shouldValidateAssertionProperlyWhenIsNotSigned() throws ValidationException {
        when(responseMock.getAssertions()).thenReturn(Arrays.asList(assertionMock));
        when(assertionMock.isSigned()).thenReturn(false);
        when(assertionMock.getConditions()).thenReturn(conditionsMock);
        when(conditionsMock.getNotBefore()).thenReturn(before);
        when(conditionsMock.getNotOnOrAfter()).thenReturn(after);

        AssertionValidityReport result = testedObject.validateAssertion(responseMock, signatureValidatorMock, 0, now);
        assertTrue(result.isValid());
        verifyZeroInteractions(signatureValidatorMock);
    }

    @Test
    public void shouldRejectWhenAssertionSignatureWasInvalid() throws ValidationException {
        when(responseMock.getAssertions()).thenReturn(Arrays.asList(assertionMock));
        when(assertionMock.isSigned()).thenReturn(true);
        when(assertionMock.getSignature()).thenReturn(signatureMock);
        Mockito.doThrow(ValidationException.class).when(signatureValidatorMock).validate(signatureMock);
        when(assertionMock.getConditions()).thenReturn(conditionsMock);
        when(conditionsMock.getNotBefore()).thenReturn(before);
        when(conditionsMock.getNotOnOrAfter()).thenReturn(after);

        AssertionValidityReport result = testedObject.validateAssertion(responseMock, signatureValidatorMock, 0, now);
        assertFalse(result.isValid());
        assertEquals(SignatureWasInvalid, result.getReason());
    }
    
    @Test
    public void shouldRejectWhenNotInPermittedTimePeriod() {
        when(responseMock.getAssertions()).thenReturn(Arrays.asList(assertionMock));
        when(assertionMock.isSigned()).thenReturn(true);
        when(assertionMock.getSignature()).thenReturn(signatureMock);
        when(assertionMock.getConditions()).thenReturn(conditionsMock);
        when(conditionsMock.getNotBefore()).thenReturn(after);
        when(conditionsMock.getNotOnOrAfter()).thenReturn(after);

        AssertionValidityReport result = testedObject.validateAssertion(responseMock, signatureValidatorMock, 0, now);
        assertFalse(result.isValid());
        assertEquals(NotInPermittedTimePeriod, result.getReason());
    }
}
