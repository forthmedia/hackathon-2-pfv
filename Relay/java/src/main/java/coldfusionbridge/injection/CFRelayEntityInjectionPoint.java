package coldfusionbridge.injection;

import coldfusionbridge.interfaces.CFRelayEntity;
@Deprecated
public class CFRelayEntityInjectionPoint {
	static CFRelayEntity relayEntity;

	public static CFRelayEntity getRelayEntity() {
		return relayEntity;
	}

	public static void setRelayEntity(CFRelayEntity relayEntity) {
		CFRelayEntityInjectionPoint.relayEntity = relayEntity;
	}
	
	
	
}
