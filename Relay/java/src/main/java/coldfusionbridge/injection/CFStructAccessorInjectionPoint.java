package coldfusionbridge.injection;

import coldfusionbridge.interfaces.CFStructAccessor;

public class CFStructAccessorInjectionPoint {
	private static CFStructAccessor accessor; //comes from coldfusion
	
	
	/**
	 * Called by coldfusion
	 * @param accessor
	 */
	public static void setCFStructAccessor(CFStructAccessor accessor ){
		CFStructAccessorInjectionPoint.accessor=accessor;
	}
	
	public static CFStructAccessor getCFStructAccessor(){
		if (accessor==null){throw new RuntimeException("Accessor was not injected when accessed. Has the coldfusion side injection been completed?");}
		
		return CFStructAccessorInjectionPoint.accessor;
	}
	
}
