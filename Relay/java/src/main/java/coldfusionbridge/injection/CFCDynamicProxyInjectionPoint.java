package coldfusionbridge.injection;

import coldfusionbridge.interfaces.CFCDynamicProxy;
import coldfusionbridge.interfaces.CFComponentFactory;

public class CFCDynamicProxyInjectionPoint {
	
	private static CFCDynamicProxy cfcDynamicProxy; //comes from coldfusion
	
	public static CFCDynamicProxy getProxy() {
		if (cfcDynamicProxy==null){throw new RuntimeException("cfcDynamicProxy was not injected when accessed. Has the coldfusion side injection been completed?");}
		
		return cfcDynamicProxy;
	}

	public static void setProxy(CFCDynamicProxy cfcDynamicProxy) {
		CFCDynamicProxyInjectionPoint.cfcDynamicProxy = cfcDynamicProxy;
	}


	
	
}
