package coldfusionbridge.injection;

import coldfusionbridge.interfaces.CFComponentFactory;

public class CFComponentFactoryInjectionPoint {
	
	private static CFComponentFactory factory; //comes from coldfusion
	
	public static CFComponentFactory getFactory() {
		if (factory==null){throw new RuntimeException("Factory was not injected when accessed. Has the coldfusion side injection been completed?");}
		
		return factory;
	}

	public static void setFactory(CFComponentFactory factory) {
		CFComponentFactoryInjectionPoint.factory = factory;
	}


	
	
}
