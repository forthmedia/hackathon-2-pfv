package coldfusionbridge.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //we care about this when interfaces are passed in
@Target(ElementType.TYPE) //can used on types only (actually intended for placing on interfaces)
public @interface CFComponentLocation {
	String value();
}
