package coldfusionbridge.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * This annotation indicates that the object returned by a method will actually be 
 * a cold fusion object and should be autowrapped on return
 *
 */

@Retention(RetentionPolicy.RUNTIME) //we care about this when interfaces are passed in
@Target(ElementType.METHOD)
public @interface AutowrapCFObject {
    Class value();
}
