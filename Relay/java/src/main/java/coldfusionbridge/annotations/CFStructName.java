package coldfusionbridge.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation indicates what the interfaces companion component is called
 * in any scope. E.g. @CFComStructName(application.CFCDynamicProxyRelayEntity);
 * @author Richard Tingle
 *
 */
@Retention(RetentionPolicy.RUNTIME) //we care about this when interfaces are passed in
@Target(ElementType.TYPE) //can used on types only (actually intended for placing on interfaces)
public @interface CFStructName {
	String value();
}
