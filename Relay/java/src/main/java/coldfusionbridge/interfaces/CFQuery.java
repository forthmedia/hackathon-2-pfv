/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package coldfusionbridge.interfaces;

import coldfusionbridge.annotations.AutowrapCFObject;
import coldfusionbridge.annotations.CFComponentLocation;
import java.util.Map;

/**
 *
 * @author Richard.Tingle
 */
@CFComponentLocation("java.javaBridge.JaQuery")
public interface CFQuery {
    
    public final String CF_SQL_BIGINT="CF_SQL_BIGINT";
    public final String CF_SQL_BIT="CF_SQL_BIT";
    public final String CF_SQL_CHAR="CF_SQL_CHAR";
    public final String CF_SQL_BLOB="CF_SQL_BLOB";
    public final String CF_SQL_CLOB="CF_SQL_CLOB";
    public final String CF_SQL_DATE="CF_SQL_DATE";
    public final String CF_SQL_DECIMAL="CF_SQL_DECIMAL";
    public final String CF_SQL_DOUBLE="CF_SQL_DOUBLE";
    public final String CF_SQL_FLOAT="CF_SQL_FLOAT";
    public final String CF_SQL_IDSTAMP="CF_SQL_IDSTAMP";
    public final String CF_SQL_INTEGER="CF_SQL_INTEGER";
    public final String CF_SQL_LONGVARCHAR="CF_SQL_LONGVARCHAR";
    public final String CF_SQL_MONEY="CF_SQL_MONEY";
    public final String CF_SQL_MONEY4="CF_SQL_MONEY4";
    public final String CF_SQL_NUMERIC="CF_SQL_NUMERIC";
    public final String CF_SQL_REAL="CF_SQL_REAL";
    public final String CF_SQL_REFCURSOR="CF_SQL_REFCURSOR";
    public final String CF_SQL_SMALLINT="CF_SQL_SMALLINT";
    public final String CF_SQL_TIME="CF_SQL_TIME";
    public final String CF_SQL_TIMESTAMP="CF_SQL_TIMESTAMP";
    public final String CF_SQL_TINYINT="CF_SQL_TINYINT";
    public final String CF_SQL_VARCHAR="CF_SQL_VARCHAR";
    
    
    
     /**
     * Set any valid attribute of the query object, note sql, name and datasource are the most used such attribute
     * @param name
     * @param value 
     */
    void setAttribute(String attributeName, Object attribute);
	
    Map getAttributes(String commaSeperatedListOfAttributes);
	
    void addParamByOrder(String name, String value, String SQLType);

    void addParamByOrder(String name, String value, String SQLType, boolean isList);
    
    /**
     * Run the query and return the result. Due to CF interaction has to be 
     * declared as returning an object but is autowrapped in a CFQueryReturn. 
     * Casting to (CFQueryReturn) is recommended
     * @return 
     */
    @AutowrapCFObject(CFQueryReturn.class)
    Object execute();
    
    void setDataSource(Object datasource);
    
    void setSQL(String sql);


}
