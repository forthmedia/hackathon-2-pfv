package coldfusionbridge.interfaces;

import coldfusionbridge.annotations.CFComponentLocation;

@CFComponentLocation("java.javabridge.DumpService")
public interface CFDumpService {
	public static String HTML="html";
	public static String TEXT="text"; //this is default but feel free to pass it	

	//writing to screen doesn't seem to work (probably because it can't figure out "where it is" after going via java

	public void writeToFile(Object itemToDump, String PathToFile);
	public void writeToFile(Object itemToDump, String PathToFile, String format);
		
}
