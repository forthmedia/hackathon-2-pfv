package coldfusionbridge.interfaces;

import java.util.List;
import java.util.Map;

public interface CFComponentFactory {
	/**
	 * Pass the full name of the object to be constructed (e.g. code.cfTemplates.someObject)
	 * and the constructor arguments (if any - null or an empty map are acceptable values 
	 * to pass). A wrapped object will be returned that java can interact with
	 * @param cannonicalName
	 * @param constructorArguments
	 * @return
	 */
	Object createComponent(String cannonicalName, List<String> interfacesToWrap, Map<String,Object> constructorArguments);
}
