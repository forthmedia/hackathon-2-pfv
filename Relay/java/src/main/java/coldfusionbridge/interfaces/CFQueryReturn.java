/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package coldfusionbridge.interfaces;

import java.util.Map;
import javax.sql.RowSet;

/**
 *
 * @author Richard.Tingle
 */
public interface CFQueryReturn {
    public RowSet getResult();
    
    public Map<String,Object> getPrefix(); //of particular interest is recordcount, an integer
    
}
