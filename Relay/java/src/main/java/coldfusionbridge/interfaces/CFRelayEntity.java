package coldfusionbridge.interfaces;

import java.util.Map;

import coldfusionbridge.annotations.CFComStructName;

@CFComStructName("RelayEntity")
public interface CFRelayEntity {
	@SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
	Map getEntity(Integer entityID, Integer entityTypeID, String fieldList);

        @SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
	Map getEntity(Integer entityID, Integer entityTypeID, String fieldList, Boolean useFriendlyName,String whereClause);
        
        @SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
	Map getEntity(Integer entityID, Integer entityTypeID, String fieldList, Boolean useFriendlyName,String whereClause,Boolean cacheResultsIfOverThreshold,Integer pageSize, String orderBy, Boolean testUserEntityRights, Boolean testLoggedInUserRights, Boolean returnApiName  );
       
        @SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
        Map insertEntity(int entityTypeID,String table, Map<String,Object> entityDetails, Boolean insertIfExists, String matchEntityColumns, Boolean updateMatchNamesForExistenceCheck, Boolean testUserEntityRights );
        
        @SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
        Map updateEntityDetails(int entityID,int entityTypeID, String table, Map<String,Object> entityDetails, Boolean treatEmtpyFieldAsNull, Boolean testUserEntityRights, Boolean updateDeleted);
        
        @SuppressWarnings("rawtypes") //this is a ColdFusion interaction, raw types are unavoidable
        Map deleteEntity(int entityID, int entityTypeID, boolean hardDelete );
        
}