package coldfusionbridge.interfaces;

import java.util.List;

import coldfusionbridge.annotations.CFComStructName;

@CFComStructName("application.CFCDynamicProxyRelayEntity")
public interface CFCDynamicProxy {
	
	public Object createInstance(Object coldFusionObject, List<String> interfacesToImplement);
		
}
