package coldfusionbridge.interfaces;

public interface CFStructAccessor {
	int getInteger(String stuctAddress);
	//double getDouble(String stuctAddress);
	//String getString(String stuctAddress);
	
	Object get(String stuctAddress);
	
	Object getWrappedObject(String stuctAddress, String interfaceName);
	
}
