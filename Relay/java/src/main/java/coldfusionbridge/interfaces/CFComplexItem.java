package coldfusionbridge.interfaces;

import coldfusionbridge.annotations.CFComponentLocation;

@CFComponentLocation("cfTestItems.ComplexItemReturner")
public interface CFComplexItem {
	public String getSomeString();
}
