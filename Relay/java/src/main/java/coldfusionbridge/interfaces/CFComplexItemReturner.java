package coldfusionbridge.interfaces;

import coldfusionbridge.annotations.CFComponentLocation;

@CFComponentLocation("cfTestItems.ComplexItemReturner")
public interface CFComplexItemReturner {
	public CFComplexItem returnComplexItem();
}
