package coldfusionbridge.wrappers;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import coldfusionbridge.annotations.CFComponentLocation;
import coldfusionbridge.injection.CFCDynamicProxyInjectionPoint;
import coldfusionbridge.injection.CFComponentFactoryInjectionPoint;
import coldfusionbridge.injection.CFStructAccessorInjectionPoint;
import coldfusionbridge.interfaces.CFCDynamicProxy;
import coldfusionbridge.interfaces.CFComponentFactory;
import coldfusionbridge.interfaces.CFStructAccessor;
import coldfusionbridge.wrappers.autowrapping.AutowrappingHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;

public class ComponentFactory {
	//a static instance available for convenience
	private static ComponentFactory instance;
	
	private final CFComponentFactory factory;
        private final CFCDynamicProxy cfcDynamicProxy;
        
        public static ComponentFactory getConnectedInstance(){
            if (instance==null){
                instance=new ComponentFactory(CFComponentFactoryInjectionPoint.getFactory(),CFCDynamicProxyInjectionPoint.getProxy() );
            }
            return instance;
        }
        
	private ComponentFactory(CFComponentFactory cfAccessor, CFCDynamicProxy cfcDynamicProxy) {
		super();
		this.factory = cfAccessor;
                this.cfcDynamicProxy=cfcDynamicProxy;
	}
	
	/*
	 * This method uses the CFComponentLocation annotation, the interface that is being passed
	 * needs to have CFComponentLocation annotation
	 */
	public <T> T getWrappedOpbject(Class<T> classToReturn,Map<String,Object> constructorArguments){
		CFComponentLocation annotation=classToReturn.getAnnotation(CFComponentLocation.class);
		if (annotation==null){
			throw new RuntimeException(classToReturn.getSimpleName() + " must have annotation " + CFComponentLocation.class.getSimpleName() + " to use the automatic ColdFusion injection functions");
		}
                if (constructorArguments==null){
                    constructorArguments=new HashMap<>();
                }
                
		
		String location=annotation.value();
		Collection<Class<T>> interfaceToWrapIn=new ArrayList<Class<T>>();
		interfaceToWrapIn.add(classToReturn);
		
		T singleWrappedCFObject = getWrappedObject(location,interfaceToWrapIn,constructorArguments);
                
                AutowrappingHandler invocationHandler=new AutowrappingHandler(singleWrappedCFObject,cfcDynamicProxy);
                    
                return (T) Proxy.newProxyInstance(singleWrappedCFObject.getClass().getClassLoader(),
				new Class<?>[] { classToReturn }, invocationHandler);
                
	}
	
	public <T> T getWrappedObject (String coldFusionFullName, Collection<Class<T>> theInterfaceToBeWrappedIn, Map<String,Object> constructorArguments){
		List<String> interfacesToWrap=new ArrayList<String>();
		
		for(Class<T> interfaceName:theInterfaceToBeWrappedIn){
			interfacesToWrap.add(interfaceName.getCanonicalName());
		}
		
		return (T)factory.createComponent(coldFusionFullName,interfacesToWrap, constructorArguments);
	}
}
