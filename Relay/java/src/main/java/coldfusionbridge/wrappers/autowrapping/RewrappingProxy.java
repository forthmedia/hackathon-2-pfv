package coldfusionbridge.wrappers.autowrapping;

/**
 * This class autowraps any outputs that correspond to coldfusion wrapping interfaces.
 * 
 * To give an example coldfusion component a is wrapped by interface A. One of its methods is
 * getB() which returns coldfusion component b, this will autowrap that in interface B.
 * It is also able to do this for Structs, Maps, Sets, collections and Lists, it is 
 * safest to use those interfaces only rather than the concrete types (although some are supported)
 * 
 * @author RJT
 *
 */
public class RewrappingProxy {

}
