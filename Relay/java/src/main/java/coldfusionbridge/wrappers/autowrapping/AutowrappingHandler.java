package coldfusionbridge.wrappers.autowrapping;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

import coldfusionbridge.annotations.AutowrapCFObject;
import coldfusionbridge.interfaces.CFCDynamicProxy;
import coldfusionbridge.interfaces.CFStructAccessor;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;

public class AutowrappingHandler implements InvocationHandler {

	private final Object wrappedObject;
	
	private final CFCDynamicProxy cfcProxy;
	
	public AutowrappingHandler(Object wrappedObject, CFCDynamicProxy cfcProxy){
		this.wrappedObject=wrappedObject;
		this.cfcProxy=cfcProxy;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

		/*
		 * we get the "natural object", this may be a ColdFusion component which can't
		 * be directly used by java
		*/
		Object unwrappedObject=method.invoke(wrappedObject, args);
                
		AutowrapCFObject autoWrapRequest=method.getAnnotation(AutowrapCFObject.class);
                if (autoWrapRequest!=null){
                    //we've been asked to fully wrap the (presumably) coldfusion object
                    List<String> wrappingInterfaces=new ArrayList<>(1);
                    
                    Class wrappingInterface=autoWrapRequest.value();
                    wrappingInterfaces.add(wrappingInterface.getCanonicalName());
                    
                    //returns a double wrapped object that can rewrap outputs as required
                    Object singleWrappedCFObject=cfcProxy.createInstance(unwrappedObject,wrappingInterfaces);
                    AutowrappingHandler invocationHandler=new AutowrappingHandler(singleWrappedCFObject,cfcProxy);
                    
                    return Proxy.newProxyInstance(singleWrappedCFObject.getClass().getClassLoader(),
				new Class<?>[] { wrappingInterface }, invocationHandler);
                    
                }else{
                    return unwrappedObject; //no command to wrap the return, just pass it on
                }

	}
	

	
	/**
	 * Converts from a list of classes (which makes sense to Java) to a list of Strings each with the full class name
	 * (which make sense in ColdFusion)
	 * @param classList
	 * @return
	 */
	private List<String> convertClassListToStringList(List<Class> classList){
		List<String> stringListOfClasses=new ArrayList<String>(classList.size());
		
		for(Class theClass:classList){
			stringListOfClasses.add(theClass.getCanonicalName());
		}
		return stringListOfClasses;
	}

}
