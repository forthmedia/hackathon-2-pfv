package coldfusionbridge.wrappers;

import coldfusionbridge.annotations.CFComStructName;
import coldfusionbridge.injection.CFStructAccessorInjectionPoint;
import coldfusionbridge.interfaces.CFStructAccessor;
	
public class StructAccessor {
	
	//a static instance available for convenience
	private static StructAccessor instance;
	
	private final CFStructAccessor cfAccessor;

        /**
         * Will return a StructAccessor that wraps an appropriate provider.
         * Probably wraps CFStructAccessorInjectionPoint.getCFStructAccessor()
         * @return 
         */
        public static StructAccessor getConnectedInstance(){
            if (instance==null){
                instance=new StructAccessor(CFStructAccessorInjectionPoint.getCFStructAccessor());
            }
            return instance;
        }
        
	private StructAccessor(CFStructAccessor cfAccessor) {
		this.cfAccessor = cfAccessor;
	}
        
        public Object getRawObject(String structAddress){
            return cfAccessor.get(structAddress);
        }
	
	
	/**
         * This allows the interface to wrap in to be specified and and the CF
         * object to be used to be specified
         * @param <T>
         * @param structAddress
         * @param theInterfaceToBeWrappedIn
         * @return 
         */
	@SuppressWarnings("unchecked") //we're interacting with ColdFusion, so casting is unavoidable
	public <T> T getWrappedObject (String structAddress, Class<T> theInterfaceToBeWrappedIn){
		String interfaceName=theInterfaceToBeWrappedIn.getCanonicalName();
		return (T)cfAccessor.getWrappedObject(structAddress, interfaceName);
	}
	
	/**
	 * For this method to work the interface being passed must be annotated with the CFComName
	 * annotation. This annotation allows the class to be autofound
         * @param <T>
	 * @param theInterfaceToBeWrappedIn
	 * @return
	 */
	public <T> T getWrappedObjectFromCom (Class<T> theInterfaceToBeWrappedIn){

            CFComStructName cfComStructNameAnnotation = theInterfaceToBeWrappedIn.getAnnotation(CFComStructName.class);
            
            if (cfComStructNameAnnotation==null){
                throw new RuntimeException("The interface " + theInterfaceToBeWrappedIn.getSimpleName() + " does not have a " + CFComStructName.class.getSimpleName() + "annotation. To use this method (which autodetects the com struct cfc to use) that annotation must be present");
            }
            
            String comName=cfComStructNameAnnotation.value();
            return getWrappedObject("application.com." + comName,theInterfaceToBeWrappedIn);
	}
	
}
