/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package coldfusionbridge.wrappers;

import java.util.Map;
import javax.sql.RowSet;

/**
 *
 * @author Richard.Tingle
 */
public interface RelayEntity {
    
    public GetEntityReturn getEntity(int entityID, Integer entityTypeID, String fieldList);
    
    public GetEntityReturn getEntities(Integer entityTypeID, String fieldList, String whereClaus);
    
    public InsertEntityReturn insertEntity(Integer entityTypeID, Map<String,Object> entityDetails);
    
    public InsertEntityReturn updateEntityDetails(int entityID, int entityTypeID, Map<String,Object> entityDetails);
    
    public DeleteEntityReturn deleteEntity(int entityID, int entityTypeID, boolean hardDelete );
    
    public static interface GetEntityReturn{
        
        public boolean isOk();
        
        public int getRecordCount();
        
        public RowSet getRecordSet();
        
        public String getErrorCode();
    }
    public static interface InsertEntityReturn{

        public boolean isOk();
        
        public int getEntityID();
        
        public String getMessage();
        
        public String getErrorCode();
    }
    public static interface DeleteEntityReturn{

        public boolean isOk();
        
        public String getReason();
        
        public String getErrorCode();
        
        public boolean getEntityDeleted();
    }
    
}
