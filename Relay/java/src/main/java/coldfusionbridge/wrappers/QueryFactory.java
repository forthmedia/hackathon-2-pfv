/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package coldfusionbridge.wrappers;

import coldfusionbridge.interfaces.CFDumpService;
import coldfusionbridge.interfaces.CFQuery;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Richard.Tingle
 */
public class QueryFactory {
    
    private final ComponentFactory componentFactory;
    private final StructAccessor structAccessor;
    
    /**
     * Builds new queries, requires componentFactory to create the CFQueries and 
     * structAccessor to set their datasource
     * @param componentFactory
     * @param structAccessor 
     */
    public QueryFactory(ComponentFactory componentFactory, StructAccessor structAccessor){
        this.componentFactory=componentFactory;
        this.structAccessor=structAccessor;
    }
    
    public CFQuery getNewQuery(){
        CFQuery query=componentFactory.getWrappedOpbject(CFQuery.class, null);

        Map<String,Object> arguments=new HashMap<>();
        arguments.put("datasource",structAccessor.getRawObject("application.siteDatasource"));
        
        query.setDataSource(structAccessor.getRawObject("application.siteDatasource"));
        
        return query;
    }
    
}
