/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coldfusionbridge.wrappers;

import coldfusionbridge.interfaces.CFDumpService;
import coldfusionbridge.interfaces.CFRelayEntity;
import java.math.BigDecimal;
import java.util.Map;
import javax.sql.RowSet;

/**
 *
 * @author Richard.Tingle
 */
public class RelayEntityColdFusionWrapping implements RelayEntity {
    
    CFRelayEntity rawEntity;
    
    public RelayEntityColdFusionWrapping(CFRelayEntity rawEntity){
        this.rawEntity=rawEntity;
    }
    
    @Override
    public GetEntityReturn getEntity(int entityID, Integer entityTypeID, String fieldList){
        return new GetEntityReturnColdFusionWrapping(rawEntity.getEntity(entityID, entityTypeID, fieldList));
    }

    @Override
    public GetEntityReturn getEntities(Integer entityTypeID, String fieldList, String whereClaus) {
        return new GetEntityReturnColdFusionWrapping(rawEntity.getEntity(null, entityTypeID, fieldList, null, whereClaus, null, null, null, null, null, null));
    }

    @Override
    public InsertEntityReturn insertEntity(Integer entityTypeID, Map<String, Object> entityDetails) {
        return new InsertEntityReturnColdFusionWrapping(rawEntity.insertEntity(entityTypeID, null, entityDetails, null, null, null, null));
    }

    @Override
    public InsertEntityReturn updateEntityDetails(int entityID, int entityTypeID, Map<String, Object> entityDetails) {
        return new InsertEntityReturnColdFusionWrapping(rawEntity.updateEntityDetails(entityID, entityTypeID, null, entityDetails, null, null, null));
    }

    @Override
    public DeleteEntityReturn deleteEntity(int entityID, int entityTypeID, boolean hardDelete) {
        return new DeleteEntityReturnColdFusionWrapping(rawEntity.deleteEntity(entityID, entityTypeID, hardDelete));
    }
    
    
    

    public static class GetEntityReturnColdFusionWrapping implements RelayEntity.GetEntityReturn {
        private final Map rawReturn;
                
        GetEntityReturnColdFusionWrapping(Map rawReturn){
            this.rawReturn=rawReturn;
        }
        
        @Override
        public boolean isOk(){
           return Boolean.parseBoolean((String)rawReturn.get("isOK"));
            
        }
        
        @Override
        public int getRecordCount(){
            return (Integer)rawReturn.get("recordCount");
        }
        @Override
        public RowSet getRecordSet(){
            try{
                return (RowSet)rawReturn.get("recordSet");
            }catch(ClassCastException e){
                if (!isOk()){
                    throw new RuntimeException("Request failed: " + getErrorCode(),e);
                }else{
                    throw e;
                }
                
            }
        }
        public String getErrorCode(){
            return (String)rawReturn.get("errorCode");
        }
    }
    
    public static class InsertEntityReturnColdFusionWrapping implements RelayEntity.InsertEntityReturn {

        private final Map rawReturn;
                
        InsertEntityReturnColdFusionWrapping(Map rawReturn){
            this.rawReturn=rawReturn;
        }
        
        @Override
        public boolean isOk() {
            return Boolean.parseBoolean((String)rawReturn.get("isOK"));
        }

        @Override
        public int getEntityID() {
            Object rawValue=rawReturn.get("entityID");
            
            if (rawValue instanceof Integer){
                return (Integer)rawValue;
            }else if (rawValue instanceof BigDecimal){
                return (int)((BigDecimal)rawValue).doubleValue();
            }else{
                throw new RuntimeException(rawValue.getClass().getCanonicalName() + "isn't a supported getEntityID return");
            }

            
        }

        @Override
        public String getMessage() {
            return (String)rawReturn.get("message");
        }

        @Override
        public String getErrorCode() {
            return (String)rawReturn.get("errorCode");
        }
       
    }
    
    public static class DeleteEntityReturnColdFusionWrapping implements RelayEntity.DeleteEntityReturn {

        private final Map rawReturn;
                
        DeleteEntityReturnColdFusionWrapping(Map rawReturn){
            this.rawReturn=rawReturn;
        }
        
        @Override
        public boolean isOk() {
            return Boolean.parseBoolean((String)rawReturn.get("isOK"));
        }

        @Override
        public String getReason() {
            return (String)rawReturn.get("Reason");
        }

        @Override
        public boolean getEntityDeleted() {
            return Boolean.parseBoolean((String)rawReturn.get("entityDeleted"));
        }

        @Override
        public String getErrorCode() {
            return (String)rawReturn.get("errorCode");
        }
       
    }
    
}
