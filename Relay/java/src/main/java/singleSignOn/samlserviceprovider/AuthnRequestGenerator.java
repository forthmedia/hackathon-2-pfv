/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStore.Entry;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStore.SecretKeyEntry;
import java.security.KeyStore.TrustedCertificateEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnContextComparisonTypeEnumeration;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameIDPolicy;
import org.opensaml.saml2.core.RequestedAuthnContext;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.SecurityConfiguration;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoHelper;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.signature.X509Data;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Element;


/**
 *
 * @author Richard.Tingle
 */
public class AuthnRequestGenerator {
    
    private RelaywareAsSPConfiguration relaywareAsSPConfiguration;
    
    private static final String KEYSTORE_TYPE = "pkcs12";
    
    private static final String AUTHN_CONTEXT_CLASS_REF = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport";
    
    private static final String FORMAT = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
    
    public AuthnRequestGenerator(RelaywareAsSPConfiguration relaywareAsSPConfiguration) throws ConfigurationException {
        this.relaywareAsSPConfiguration=relaywareAsSPConfiguration;
        DefaultBootstrap.bootstrap();
    }
    
    public String buildRequest( String id,  String destinationURL,  String certificationKeyStorePath,  String password,  String certificateAlias) throws MarshallingException, SignatureException{
        XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
        SAMLObjectBuilder<AuthnRequest> builder = (SAMLObjectBuilder<AuthnRequest>) builderFactory.getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
        AuthnRequest authnRequest = builder.buildObject();
        
        setGeneralProperties(id, destinationURL, authnRequest);
        setAuthnContext(builderFactory, authnRequest);
        setNamePolicy(builderFactory, authnRequest);
        setIssuer(builderFactory, authnRequest);
        
        DateTime now=new DateTime(DateTimeZone.UTC);
        authnRequest.setIssueInstant(now);
        
        if (certificateAlias!=null && !certificateAlias.equals("")){
            try {
              setSignature(certificationKeyStorePath, password, certificateAlias, builderFactory,authnRequest);
            } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableEntryException | SecurityException exception) {
              throw new SAMLSignAuthnRequestException("Exception occured during signing authorization request", exception);
            }
        }
        

        MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
        Marshaller marshaller = marshallerFactory.getMarshaller(authnRequest);
        Element authnRequestElement = marshaller.marshall(authnRequest);

       
        String authnRequestString=XMLHelper.nodeToString(authnRequestElement);
        
        return authnRequestString;
    }


    private void setSignature(final String certificationKeyStorePath, final String password, final String certificateAlias, XMLObjectBuilderFactory builderFactory, AuthnRequest authnRequest) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableEntryException, SecurityException, MarshallingException, SignatureException{
      Signature signature = (Signature) builderFactory.getBuilder(Signature.DEFAULT_ELEMENT_NAME).buildObject(Signature.DEFAULT_ELEMENT_NAME);
      //KeyInfo keyInfo = (KeyInfo) builderFactory.getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME).buildObject(KeyInfo.DEFAULT_ELEMENT_NAME);
      
      PasswordProtection passwordProtection = new PasswordProtection(password.toCharArray());
      SecurityConfiguration globalSecurityConfiguration = Configuration.getGlobalSecurityConfiguration();
      
      KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
      FileInputStream fis = new FileInputStream(certificationKeyStorePath);
      keyStore.load(fis, password.toCharArray());
      fis.close();
      
      Entry privateKeyEntry = keyStore.getEntry(certificateAlias, passwordProtection);
      PrivateKey privateKey = ((PrivateKeyEntry)privateKeyEntry).getPrivateKey();
      X509Certificate certificate = (X509Certificate) ((PrivateKeyEntry)privateKeyEntry).getCertificate();

      BasicX509Credential credential = new BasicX509Credential();
      credential.setEntityCertificate(certificate);
      credential.setPrivateKey(privateKey);

      KeyInfoGenerator keyInfoGenerator = SecurityHelper.getKeyInfoGenerator(credential, null, null);
      KeyInfo keyInfo=keyInfoGenerator.generate(credential);

      
      SecurityHelper.prepareSignatureParams(signature, credential, globalSecurityConfiguration, "");
      signature.setKeyInfo(keyInfo);
      signature.setSigningCredential(credential);
      authnRequest.setSignature(signature);  
      
      //must be marshalled before being signed
      Configuration.getMarshallerFactory().getMarshaller(authnRequest).marshall(authnRequest);
      
      Signer.signObject(signature);
      
    }

    private void setIssuer(XMLObjectBuilderFactory builderFactory, AuthnRequest authnRequest) {
      SAMLObjectBuilder<Issuer> issuerBuilder = (SAMLObjectBuilder<Issuer>) builderFactory.getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
      Issuer issuer=issuerBuilder.buildObject();
      issuer.setValue(relaywareAsSPConfiguration.getRelaywareIssuer());
      authnRequest.setIssuer(issuer);
    }


    private void setNamePolicy(XMLObjectBuilderFactory builderFactory, AuthnRequest authnRequest) {
      SAMLObjectBuilder<NameIDPolicy> nameIDBuilder = (SAMLObjectBuilder<NameIDPolicy>) builderFactory.getBuilder(NameIDPolicy.DEFAULT_ELEMENT_NAME);
      NameIDPolicy nameIDPolicy=nameIDBuilder.buildObject();
      nameIDPolicy.setAllowCreate(true);
      nameIDPolicy.setFormat(FORMAT);
      authnRequest.setNameIDPolicy(nameIDPolicy);
    }


    private void setAuthnContext(XMLObjectBuilderFactory builderFactory,
        AuthnRequest authnRequest) {
      SAMLObjectBuilder<AuthnContextClassRef> authnContextClassRefBuilder= (SAMLObjectBuilder<AuthnContextClassRef>) builderFactory.getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME);
      AuthnContextClassRef authnContextClassRef = authnContextClassRefBuilder.buildObject();
      authnContextClassRef.setAuthnContextClassRef(AUTHN_CONTEXT_CLASS_REF);

      SAMLObjectBuilder<RequestedAuthnContext> requestedAuthnContextBuilder = (SAMLObjectBuilder<RequestedAuthnContext>) builderFactory.getBuilder(RequestedAuthnContext.DEFAULT_ELEMENT_NAME);
      RequestedAuthnContext requestedAuthnContext=requestedAuthnContextBuilder.buildObject();
      requestedAuthnContext.setComparison(AuthnContextComparisonTypeEnumeration.EXACT);
      requestedAuthnContext.getAuthnContextClassRefs().add(authnContextClassRef);
      authnRequest.setRequestedAuthnContext(requestedAuthnContext);
    }


    private void setGeneralProperties(String id, String destinationURL, AuthnRequest authnRequest) {
      authnRequest.setID(id);
      authnRequest.setVersion(SAMLVersion.VERSION_20);
      authnRequest.setProviderName(relaywareAsSPConfiguration.getRelaywareIssuer());
      authnRequest.setAssertionConsumerServiceURL(relaywareAsSPConfiguration.getAssertionConsumerURL());
      authnRequest.setProtocolBinding(SAMLConstants.SAML2_POST_BINDING_URI);
      authnRequest.setDestination(destinationURL);
    }
    

    
    
}
