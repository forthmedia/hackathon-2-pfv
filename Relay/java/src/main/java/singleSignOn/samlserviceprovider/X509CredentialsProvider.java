package singleSignOn.samlserviceprovider;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.security.x509.X509Credential;

public class X509CredentialsProvider {

    public X509Credential getX509Credential(String certificationKeyStorePath, String certificateAlias, String password) {
        X509Credential credential = null;
        try {
            credential = getCredential(certificationKeyStorePath, certificateAlias, password);
        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException | UnrecoverableEntryException ex) {
            Logger.getLogger(X509CredentialsProvider.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException("Cannot retrieved X509Credentials", ex);
        }
        return credential;
    }
    
    
    private X509Credential getCredential(String certificationKeyStorePath, String certificateAlias, String password) throws KeyStoreException, FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableEntryException {
        KeyStore keyStore = KeyStore.getInstance("pkcs12");
        try (FileInputStream fis = new FileInputStream(certificationKeyStorePath)) {
            keyStore.load(fis, password.toCharArray());
        }
        KeyStore.PasswordProtection passwordProtection = new KeyStore.PasswordProtection(password.toCharArray());

        KeyStore.Entry privateKeyEntry = keyStore.getEntry(certificateAlias, passwordProtection);

        PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) privateKeyEntry).getPrivateKey();
        X509Certificate certificate = (X509Certificate) ((KeyStore.PrivateKeyEntry) privateKeyEntry).getCertificate();

        BasicX509Credential credential = new BasicX509Credential();
        credential.setEntityCertificate(certificate);
        credential.setPrivateKey(privateKey);
        return credential;
    }
}
