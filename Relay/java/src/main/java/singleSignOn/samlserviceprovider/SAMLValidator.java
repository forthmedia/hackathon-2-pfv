package singleSignOn.samlserviceprovider;

import java.util.List;
import org.joda.time.DateTime;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Response;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.validation.ValidationException;

public class SAMLValidator {


    AssertionValidityReport validateSignature(Response response, SAMLSignatureProfileValidator profileValidator, SignatureValidator signatureValidator) {
        
        //validate that the signature is properly formed
        Signature signature = response.getSignature();
        try {
            profileValidator.validate(signature);
        } catch (ValidationException e) {
            return AssertionValidityReport.reject(AssertionRejectionReason.SignatureWasMalformed, e.getLocalizedMessage());
        }

        //validate the signature itself
        try {
            signatureValidator.validate(signature);
        } catch (ValidationException v) {
            return AssertionValidityReport.reject(AssertionRejectionReason.SignatureWasInvalid, v.getLocalizedMessage() + " | Note that this MAY be caused by a valid signature signed with a key we don't trust. " + v.getLocalizedMessage());
        }
        
        return AssertionValidityReport.accept();
    }

    AssertionValidityReport validateAssertion(Response response, SignatureValidator signatureValidator, int timeMismatchPermitted_minutes, DateTime now, boolean requireSignature) {
        
        //if the asssertion also has a signature we validate it, but its not obliged to have a signature
        List<Assertion> assertions = response.getAssertions();

        if (assertions.size() != 1) {
            return AssertionValidityReport.reject(AssertionRejectionReason.ResponseNotAcceptedFormat, "Only responses with exactly 1 assertion are supported, this response had " + assertions.size() + " assertions");
        }
        Assertion assertion = assertions.get(0);

        //if the assertion isn't signed thats fine as per the SAML spec
        if (assertion.isSigned()) {
            Signature asssertion_sig = assertion.getSignature();
            try {
                signatureValidator.validate(asssertion_sig);
            } catch (ValidationException v) {
                // Cause of this is a valid signature with an unknown key
                if (!v.getLocalizedMessage().equals("Signature cryptographic validation not successful")) {
                    return AssertionValidityReport.reject(AssertionRejectionReason.SignatureWasInvalid, v.getLocalizedMessage());
                }
            }

        } else if (requireSignature) {
            return AssertionValidityReport.reject(AssertionRejectionReason.SignatureWasInvalid, "If responses are unsigned assertions must be signed");
        }
        
        //validate that the assertion hasn't expired
        Conditions conditions = assertion.getConditions();

        boolean passedNotBefore = conditions.getNotBefore().isBefore(now.plusMinutes(timeMismatchPermitted_minutes));
        boolean passedNotOnOrAfter = conditions.getNotOnOrAfter().isAfter(now.minusMinutes(timeMismatchPermitted_minutes));

        if (!(passedNotBefore && passedNotOnOrAfter)) {
            return AssertionValidityReport.reject(AssertionRejectionReason.NotInPermittedTimePeriod, "Failed time constrains, now is " + now.toString() + ", NotBefore was " + conditions.getNotBefore().toString() + ", NotOnOrAfter was " + conditions.getNotOnOrAfter().toString() + ". Passed passedNotBefore: " + passedNotBefore + ", Passed passedNotOnOrAfter: " + passedNotOnOrAfter);
        }

        return AssertionValidityReport.accept();
    }

}
