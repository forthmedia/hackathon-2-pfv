/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider;

import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;

/**
 *
 * @author Richard.Tingle
 */
public class SAMLBoot {
    
    private static boolean hasBooted=false;
    public static void bootIfRequired() throws ConfigurationException{
        if (!hasBooted){
            DefaultBootstrap.bootstrap();
            hasBooted=true;
        }
        
    }
}
