/*
 * (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Richard.Tingle
 */
public class AssertionContentReport {
    private final String issuer;
    private final String nameID;
    private final Map<String,List<String>> attributes;

    protected AssertionContentReport(String issuer,String nameID, Map<String, List<String>> attributes) {
        this.nameID = nameID;
        this.attributes = caseInsensitiseMap(attributes);
        this.issuer=issuer;
    }
    
    /**
     * Rejigs the map to make the keys case insensitive
     * @param attributes
     * @return 
     */
    private Map<String, List<String>> caseInsensitiseMap(Map<String, List<String>> attributes){
        Map<String, List<String>> caseInsensitiseMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        caseInsensitiseMap.putAll(attributes);

        return caseInsensitiseMap;
    }

    public String getIssuer() {
        return issuer;
    }

    
    
    public String getNameID() {
        return nameID;
    }

    public Map<String, List<String>> getAttributes() {
        return attributes;
    }

    public boolean hasAttribute(String attribute){
        return attributes.containsKey(attribute);
    }
    
    /**
     * Returns the attribute at that key, if more than one attribute is at that
     * key a comma seperated list is returned 
     * @param attribute 
     * @return  
     */
    public String getAttributeSingle(String attribute){
        
        List<String> attributes=getAttributeList(attribute);
        
        String value=attributes.get(0);
        
        for(int i=1;i<attributes.size();i++){
            value+=","+attributes.get(i);
        }
        
        return getAttributeList(attribute).get(0);
    } 
    /**
     * Returns all the attributes at a certain key. If no such attribute exists
     * an exception is thrown
     * @param attribute 
     * @return  
     */
    public List<String> getAttributeList(String attribute){
        if (hasAttribute(attribute)){
            return attributes.get(attribute);
        }else{
            String errorMessage="No attribute '" + attribute + "' was found in the assertion. In assertion was: ";
            
            Set<String> attributeKeys=getAttributes().keySet();
            
            for(String attributeKey:attributeKeys){
                errorMessage+=attributeKey +" ";
            }
            errorMessage+=". Total attributes: " + attributeKeys.size();
            
            throw new RuntimeException(errorMessage);
        }

    } 
    
    public boolean hasNameID() {
        return nameID != null;
    }
    
    
}
