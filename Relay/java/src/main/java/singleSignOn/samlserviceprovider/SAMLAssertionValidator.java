/*
 * (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml2.metadata.provider.DOMMetadataProvider;
import org.opensaml.saml2.metadata.provider.MetadataProviderException;
import org.opensaml.security.MetadataCredentialResolver;
import org.opensaml.security.MetadataCredentialResolverFactory;
import org.opensaml.security.MetadataCriteria;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.security.CriteriaSet;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.criteria.EntityIDCriteria;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.signature.SignatureValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static singleSignOn.samlserviceprovider.AssertionRejectionReason.*;
import singleSignOn.samlserviceprovider.idps.IDPManager;
import singleSignOn.samlserviceprovider.idps.IdentityProvider;

/**
 *
 * @author Richard.Tingle
 */
public class SAMLAssertionValidator {

    private final MetadataProvider certificateProvider;
    private final int timeMismatchPermitted_minutes;
    private final IDPManager idpManager;
    private final SAMLValidator samlValidator;
    private final boolean allowUnsignedResponses;
    
    /**
     * Sets up a validator for assertions
     *
     * @param certificateProvider
     * @param timeMismatchPermitted_minutes The authentication will have a
     * restriction on the time that the authentication is valid for, this
     * expands that to account for server time mismatch
     * @throws org.opensaml.xml.ConfigurationException
     */
    public SAMLAssertionValidator(MetadataProvider certificateProvider, int timeMismatchPermitted_minutes, boolean allowUnsignedResponses) throws ConfigurationException {
        this(certificateProvider, timeMismatchPermitted_minutes, allowUnsignedResponses, new IDPManager(), new SAMLValidator());

        SAMLBoot.bootIfRequired();
    }

    public SAMLAssertionValidator(MetadataProvider certificateProvider, int timeMismatchPermitted_minutes, boolean allowUnsignedResponses, IDPManager idpManager, SAMLValidator samlValidator) {
        this.certificateProvider = certificateProvider;
        this.timeMismatchPermitted_minutes = timeMismatchPermitted_minutes;
        this.idpManager = idpManager;
        this.samlValidator = samlValidator;
        this.allowUnsignedResponses = allowUnsignedResponses;
    }

    
    /**
     * This validates the assertion assuming that now is when you want to
     * validate it based on (almost always true except for tests)
     *
     * @param responseString
     * @return
     * @throws java.lang.Exception
     */
    public AssertionValidityReport validate(String responseString, List<String> requestIds) throws Exception {
        return validate(responseString, new DateTime(DateTimeZone.UTC), requestIds);
    }

    public AssertionValidityReport validate(String responseString, DateTime now, List<String> requestIds) throws Exception {
        //convert the string (of XML) to a document
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = docBuilder.parse(new InputSource(new ByteArrayInputStream(responseString.getBytes("utf-8"))));
        Element element = document.getDocumentElement();

        UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
        Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
        Response response = (Response) unmarshaller.unmarshall(element);

        return validate(response, now, requestIds);
    }

    public AssertionValidityReport validate(Response response, DateTime now, List<String> requestIds) throws Exception {

        String issuer = response.getIssuer().getValue();
        String rawMetadata;

        if (certificateProvider.hasIssuer(issuer)) {
            rawMetadata = certificateProvider.getRawMetadataForIssuer(issuer);
        } else {
            return AssertionValidityReport.reject(AssertionRejectionReason.IssuerNotRecognised, "Issuer " + issuer + " not recognised");
        }


        X509Credential credential = getCredentialFromMetadata(issuer, rawMetadata);
        SignatureValidator signatureValidator = new SignatureValidator(credential);

        //first we validate the response signature - since Microsoft Azure doesn't sign responses this is now optional
        if (!this.allowUnsignedResponses) {
            if (!response.isSigned()) {
                return AssertionValidityReport.reject(AssertionRejectionReason.NoSignature, "Responses must be signed (changeable in settings)");
            }

            AssertionValidityReport signatureReport = samlValidator.validateSignature(response, new SAMLSignatureProfileValidator(), signatureValidator);
            if(!signatureReport.isValid()) {
                return signatureReport;
            }
        }
        
        AssertionValidityReport assertionReport = samlValidator.validateAssertion(response, signatureValidator, timeMismatchPermitted_minutes, now, this.allowUnsignedResponses);
        if(!assertionReport.isValid()){
            return assertionReport;
        }
        
        final String inResponseTo = response.getInResponseTo();
        if (inResponseTo == null || inResponseTo.equals("")) {
            if (!isPermittedToInitiateSAML(certificateProvider.getIdentityProvider(issuer))) {
                return AssertionValidityReport.reject(IdentityProviderIsNotPermitted, "Identity provider is not permitted to do an Identity Provider Initiated SAML.");
            }
        } else if (!isValidInResponseTo(inResponseTo, requestIds)) {
            return AssertionValidityReport.reject(InResponseToContainsInvalidId, "InResponseTo filed doesn't contain valid request id.");
        }

        return AssertionValidityReport.accept();
    }

    private boolean isPermittedToInitiateSAML(IdentityProvider ip) throws SQLException {
       return ip == null ? false : ip.isPermittedToInitiateSAML();
    }
    
    private boolean isValidInResponseTo(String inResponseTo, List<String> requestIds) {
        boolean isValid = false;
        for (String requestId : requestIds) {
            if (inResponseTo.contains(requestId)) {
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    private X509Credential getCredentialFromMetadata(String expectedIDP, String rawMetadata) throws MetadataProviderException, SecurityException, SAXException, IOException, ParserConfigurationException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = docBuilder.parse(new InputSource(new ByteArrayInputStream(rawMetadata.getBytes("utf-8"))));
        Element metadataRoot = document.getDocumentElement();

        DOMMetadataProvider idpMetadataProvider = new DOMMetadataProvider(metadataRoot);
        idpMetadataProvider.setRequireValidMetadata(true);
        idpMetadataProvider.setParserPool(new BasicParserPool());
        idpMetadataProvider.initialize();

        MetadataCredentialResolverFactory credentialResolverFactory = MetadataCredentialResolverFactory.getFactory();

        MetadataCredentialResolver credentialResolver = credentialResolverFactory.getInstance(idpMetadataProvider);

        CriteriaSet criteriaSet = new CriteriaSet();
        criteriaSet.add(new MetadataCriteria(IDPSSODescriptor.DEFAULT_ELEMENT_NAME, SAMLConstants.SAML20P_NS));
        criteriaSet.add(new EntityIDCriteria(expectedIDP));

        X509Credential credential = (X509Credential) credentialResolver.resolveSingle(criteriaSet);

        return credential;

    }

}
