package singleSignOn.samlserviceprovider;

public class SAMLSignAuthnRequestException extends RuntimeException{

  private static final long serialVersionUID = 1L;

  private String message;
  
  public SAMLSignAuthnRequestException(String message, Exception e) {
    super(e);
  }
  
  public String getMessage() {
    return this.message;
  }
  
}
