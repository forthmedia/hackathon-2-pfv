/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

import coldfusionbridge.interfaces.CFQuery;
import coldfusionbridge.interfaces.CFQueryReturn;
import coldfusionbridge.interfaces.CFRelayEntity;
import coldfusionbridge.wrappers.ComponentFactory;
import coldfusionbridge.wrappers.QueryFactory;
import coldfusionbridge.wrappers.RelayEntity;
import coldfusionbridge.wrappers.RelayEntityColdFusionWrapping;
import coldfusionbridge.wrappers.StructAccessor;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.RowSet;

/**
 *
 * @author Richard.Tingle
 */
public class IDPManager {
    
    private final RelayEntity dataAccess;
    private final QueryFactory queryDataAccess;
    private final int identityProviderEntityTypeID;
    private static final String IS_PERMITTED_COL_NAME = "isPermittedToInitiateSAML";
     /**
     * This is a self connecting constructor and is primarily intended to be called from coldfusion.
     * 
     * BE AWARE IT CREATES ITS OWN CONNECTION TO THE DB
     */
    public IDPManager(){
        this(
           new RelayEntityColdFusionWrapping(StructAccessor.getConnectedInstance().getWrappedObjectFromCom(CFRelayEntity.class)),
           new QueryFactory(ComponentFactory.getConnectedInstance(), StructAccessor.getConnectedInstance()),
           (int)StructAccessor.getConnectedInstance().getRawObject("application.entityTypeID.SAMLIdentityProvider")
        );

    }
    
    public IDPManager(RelayEntity dataAccess,QueryFactory queryDataAccess, int identityProviderEntityID){
        this.dataAccess=dataAccess;
        this.queryDataAccess=queryDataAccess;
        this.identityProviderEntityTypeID=identityProviderEntityID;
    }
    
    public void delete(IdentityProvider identityProvider){
        RelayEntity.DeleteEntityReturn deleteReturn = dataAccess.deleteEntity(identityProvider.getSAMLIdentityProviderId(), identityProviderEntityTypeID, false);
        if (!deleteReturn.isOk()){
            throw new RuntimeException(deleteReturn.getErrorCode() + ":" +deleteReturn.getReason());
    }
    }

    public void persistIdentityManager(IdentityProvider identityProvider){
        {
       
            Map<String,Object> entityDetails=new HashMap<>();

            entityDetails.put("identityProviderName", identityProvider.getIdentityProviderName());
            entityDetails.put("identityProviderEntityID", identityProvider.getIdentityProviderIdentifier());
            entityDetails.put("authnRequestURL", identityProvider.getAuthnRequestURL());
            entityDetails.put("nameIDField_Relayware", identityProvider.getNameIDField_Relayware());
            entityDetails.put("nameIDField_Foreign", identityProvider.getNameIDField_Foreign());
            entityDetails.put("rawIDPMetadata", identityProvider.getRawIDPMetadata());
            entityDetails.put("active", identityProvider.isActive()?"1":"0");
            entityDetails.put("allowAutoCreate", identityProvider.getAllowAutoCreate()?"1":"0");
            entityDetails.put("autoCreate_SelectLocOrg",identityProvider.getAutoCreate_SelectLocOrg()?"1":"0");
            entityDetails.put("autoCreate_selectedLocationID",identityProvider.getAutoCreate_selectedLocationID());
            entityDetails.put("autoCreate_selectedOrganisationID",identityProvider.getAutoCreate_selectedOrganisationID());
            entityDetails.put("AutoCreate_detectedLoc_ForeignField",identityProvider.getAutoCreate_detectedLoc_ForeignField());
            entityDetails.put("AutoCreate_detectedLoc_RelaywareField",identityProvider.getAutoCreate_detectedLoc_RelaywareField());
            entityDetails.put("permittedToSetUsergroups", identityProvider.isPermittedToSetUsergroups());
            entityDetails.put("assertionAttributeToControlUsergroups", identityProvider.getAssertionAttributeToControlUsergroups());
            entityDetails.put("permittedToControlCountryRights", identityProvider.isPermittedToControlCountryRights());
            entityDetails.put("assertionAttributeToControlCountryRights", identityProvider.getAssertionAttributeToControlCountryRights());
            entityDetails.put("onCreationPortalPersonApprovalStatus", identityProvider.getOnCreationPortalPersonApprovalStatus());
            entityDetails.put("certificateToUse", identityProvider.getCertificateToUse());
            entityDetails.put(IS_PERMITTED_COL_NAME, identityProvider.isPermittedToInitiateSAML());
            
            if (identityProvider.isExistingIDP()){
                RelayEntity.InsertEntityReturn result = dataAccess.updateEntityDetails(identityProvider.getSAMLIdentityProviderId(), identityProviderEntityTypeID, entityDetails);
                if (!result.isOk()){
                    throw new RuntimeException("Error occured during persisting Identity provider " + result.getErrorCode() + ", " + result.getMessage());
                }
            }else{
                RelayEntity.InsertEntityReturn result = dataAccess.insertEntity(identityProviderEntityTypeID, entityDetails);
                identityProvider.setSAMLIdentityProviderId(result.getEntityID()); //so the remaining code is happy
                

                if (!result.isOk()){
                    throw new RuntimeException("Error occured during persisting Identity provider " + result.getErrorCode() + ", " + result.getMessage());
                }

            } 
        }
        
        {    
            //persist the usergroup permissions                
            String usergroupListAtCreation=identityProvider.getUsergroupsAtPersonCreation_asStringList();
            String usergroupsToControl=identityProvider.getUsergroupsPermittedToControl_asStringList();

            StringBuilder sql=new StringBuilder();

            CFQuery query=queryDataAccess.getNewQuery();
            query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);
            query.addParamByOrder("usergroupListAtCreation", usergroupListAtCreation, CFQuery.CF_SQL_INTEGER,true);
            query.addParamByOrder("usergroupsToControl", usergroupsToControl, CFQuery.CF_SQL_INTEGER,true);

            sql.append("Delete from SAMLIdentityProviderUsergroupControl where SAMLIdentityProviderId= :SAMLIdentityProviderId ");

            sql.append("Insert into SAMLIdentityProviderUsergroupControl (SAMLIdentityProviderId,usergroupID,permitsControl,setAtPersonCreation)");
            sql.append("select :SAMLIdentityProviderId , usergroup.usergroupID,"
                    + "case when usergroup.usergroupID in ( :usergroupsToControl ) then 1 else 0 end ,"
                    + "case when usergroup.usergroupID in ( :usergroupListAtCreation ) then 1 else 0 end ");
            sql.append("from usergroup where usergroup.usergroupID in ( :usergroupListAtCreation ) "
                    + "or  "
                    + "usergroup.usergroupID in ( :usergroupsToControl )");

            query.setSQL(sql.toString());
            query.execute();

        }

        {
            //persist the countries at creation

            String countryListAsString="";
            List<Integer> countryList=identityProvider.getUsergroupCountriesAtPersonCreation();
            for(int i=0;i<countryList.size();i++){
                countryListAsString+=countryList.get(i);

                if (i!=countryList.size()-1){
                    countryListAsString+=",";
                }
            }


            StringBuilder sql=new StringBuilder();

            CFQuery query=queryDataAccess.getNewQuery();
            query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);
            query.addParamByOrder("requiredCountries", countryListAsString, CFQuery.CF_SQL_INTEGER,true);

            sql.append("Delete from SAMLIdentityProviderUsergroupCountryControl where SAMLIdentityProviderId= :SAMLIdentityProviderId and setAtPersonCreation=1 ");

            sql.append("Insert into SAMLIdentityProviderUsergroupCountryControl (SAMLIdentityProviderId,countryID,permitsControl,setAtPersonCreation)");
            sql.append("select :SAMLIdentityProviderId , country.countryID, 0,1 ");
            sql.append("from country where country.countryID in ( :requiredCountries ) ");

            query.setSQL(sql.toString());
            query.execute();

        }

        //persist any authorisedSitesToAuthenticate
        {
            StringBuilder sql=new StringBuilder();

            CFQuery query=queryDataAccess.getNewQuery();
            
            query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);
            query.addParamByOrder("requiredSiteDefIDs", identityProvider.getAuthorisedSitesToAuthenticate_asStringList(), CFQuery.CF_SQL_INTEGER,true);
            
            sql.append("Delete from SAMLIdentityProviderAuthorisedSite where SAMLIdentityProviderId= :SAMLIdentityProviderId and siteDefID not in ( :requiredSiteDefIDs ) ");
            sql.append("Insert into SAMLIdentityProviderAuthorisedSite (SAMLIdentityProviderId,siteDefID)"
                    + " select :SAMLIdentityProviderId ,siteDefID "
                    + "from siteDef "
                    + "where siteDef.siteDefID in ( :requiredSiteDefIDs ) "
                    + "and siteDef.siteDefID not in ( select existingSAMLIdentityProviderAuthorisedSites.siteDefID from SAMLIdentityProviderAuthorisedSite existingSAMLIdentityProviderAuthorisedSites where SAMLIdentityProviderId= :SAMLIdentityProviderId  )");
            
            query.setSQL(sql.toString());
            query.execute();
    
        }
        
        //persist any mappings
        {
            StringBuilder mappingsSql=new StringBuilder();
            CFQuery mappingsQuery=queryDataAccess.getNewQuery();

            mappingsQuery.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);
            mappingsSql.append("Delete from SAMLIdentityProviderMapping where SAMLIdentityProviderId= :SAMLIdentityProviderId ; ");

            int rowNumber=0;    
            for(AssertionMapping mapping:identityProvider.getMappings()){
                String rowIdentifier="row" + rowNumber++;

                String relaywareFieldIdentifier= rowIdentifier+"_relaywareField";
                String useAssertionIdentifier= rowIdentifier+"_useAssertion";
                String foreignValueIdentifier= rowIdentifier+"_foreignValue";

                mappingsQuery.addParamByOrder(relaywareFieldIdentifier, mapping.getRelaywareField(), CFQuery.CF_SQL_VARCHAR);
                mappingsQuery.addParamByOrder(useAssertionIdentifier, mapping.isUseAssertion()?"1":"0", CFQuery.CF_SQL_BIT);
                mappingsQuery.addParamByOrder(foreignValueIdentifier, mapping.getForeignValue(), CFQuery.CF_SQL_VARCHAR);

                mappingsSql.append("Insert into SAMLIdentityProviderMapping(SAMLIdentityProviderId,relaywareField,useAssertion,foreignValue)  "
                        + "values ( "
                        + ":" + "SAMLIdentityProviderId" + ", "
                        + ":" + relaywareFieldIdentifier + ", "
                        + ":" + useAssertionIdentifier + ", "
                        + ":" + foreignValueIdentifier + ") "
                );


            }

            mappingsQuery.setSQL(mappingsSql.toString());
            mappingsQuery.execute();
        }
            
        
    }
    
    public IdentityProvider getIDPByID(int id){
      
        IdentityProvider identityProvider=new IdentityProvider(id);
        
        RelayEntity.GetEntityReturn relayEntityReturn = dataAccess.getEntity(id, identityProviderEntityTypeID, getIDPFields());
    
        if (relayEntityReturn.isOk()){
            fillIdentityProviderWithData(identityProvider,relayEntityReturn.getRecordSet());
        }else{
            throw new RuntimeException("While attempting to retrieve " + identityProvider.getSAMLIdentityProviderId() + " an error occured (" + relayEntityReturn.getErrorCode() + ")");
        }
        
        return identityProvider;
    }
    
    public boolean identityProviderExistsByEntityID(String entityID) throws SQLException{
        CFQuery query=queryDataAccess.getNewQuery();
        query.addParamByOrder("identityProviderIdEntityID", entityID, CFQuery.CF_SQL_VARCHAR);
        query.setSQL("select count(*) as identityProviders from SAMLIdentityProvider where identityProviderEntityID = :identityProviderIdEntityID");
        CFQueryReturn result=(CFQueryReturn)query.execute();
        
        RowSet rowSet= result.getResult();
        rowSet.next();
        
        
        return rowSet.getInt("identityProviders")>0;
    }
    
    /**
     * Gets an IDP based on its text identifier (e.g. used as an issuer).
     * 
     * If a single identity provider can't be found then null is returned
     * @param entityID
     * @return 
     */
    public IdentityProvider getIDPByEntityID(String entityID){
        //we don't use relayEntity here as it assumes the where claus is sql safe, which we can't garantee
      
        CFQuery query=queryDataAccess.getNewQuery();
        query.addParamByOrder("identityProviderIdEntityID", entityID, CFQuery.CF_SQL_VARCHAR);
        query.setSQL("select " + getIDPFields() + " from SAMLIdentityProvider where identityProviderEntityID = :identityProviderIdEntityID");
        CFQueryReturn result=(CFQueryReturn)query.execute();
        
        int numberOfRecords=(int)result.getPrefix().get("RecordCount");
        
        if (numberOfRecords!=1){
            return null;
        }else{
        IdentityProvider identityProvider=new IdentityProvider();
        
        RowSet rowSet=result.getResult();
        fillIdentityProviderWithData(identityProvider,rowSet);
        
        return identityProvider;
        }
        
        
    }
    
    private String getIDPFields(){
        return "SAMLIdentityProviderId,"
                + "identityProviderName,"
                + "identityProviderEntityID,"
                + "authnRequestURL,"
                + "nameIDField_Relayware,"
                + "nameIDField_Foreign,"
                + "rawIDPMetadata,"
                + "active,"
                + "allowAutoCreate,"
                + "autoCreate_SelectLocOrg,"
                + "autoCreate_selectedLocationID,"
                + "autoCreate_selectedOrganisationID,"
                + "autoCreate_detectedLoc_RelaywareField,"
                + "autoCreate_detectedLoc_ForeignField,"
                + "permittedToSetUsergroups,"
                + "assertionAttributeToControlUsergroups,"
                + "permittedToControlCountryRights,"
                + "assertionAttributeToControlCountryRights,"
                + "onCreationPortalPersonApprovalStatus,"
                + "assertionAttributeToControlCountryRights,"
                + "certificateToUse,"
                + "isPermittedToInitiateSAML";
    }

    
    private void fillIdentityProviderWithData(IdentityProvider identityProvider, RowSet data){

        try {
            boolean anyData=data.next();
            
            if (!anyData){
                throw new RuntimeException("While attempting to retrieve an identity provider no data was returned");
        
            }
            
            identityProvider.setSAMLIdentityProviderId(data.getInt("SAMLIdentityProviderId"));                     
            identityProvider.setIdentityProviderName(data.getString("identityProviderName"));
            identityProvider.setIdentityProviderIdentifier(data.getString("identityProviderEntityID"));
            identityProvider.setAuthnRequestURL(data.getString("authnRequestURL"));
            identityProvider.setNameIDField_Relayware(data.getString("nameIDField_Relayware"));
            identityProvider.setNameIDField_Foreign(data.getString("nameIDField_Foreign"));
            identityProvider.setRawIDPMetadata(data.getString("rawIDPMetadata"));
            identityProvider.setActive(Boolean.parseBoolean(data.getString("active")));
            identityProvider.setAllowAutoCreate(Boolean.parseBoolean(data.getString("allowAutoCreate")));
            identityProvider.setAutoCreate_selectLocOrg(Boolean.parseBoolean(data.getString("autoCreate_SelectLocOrg")));
            identityProvider.setAutoCreate_selectedLocationID(data.getInt("autoCreate_selectedLocationID"));
            identityProvider.setAutoCreate_selectedOrganisationID(data.getInt("autoCreate_selectedOrganisationID"));
            identityProvider.setAutoCreate_detectedLoc_ForeignField(data.getString("AutoCreate_detectedLoc_ForeignField"));
            identityProvider.setAutoCreate_detectedLoc_RelaywareField(data.getString("AutoCreate_detectedLoc_RelaywareField"));
            identityProvider.setPermittedToSetUsergroups(data.getBoolean("permittedToSetUsergroups"));
            identityProvider.setAssertionAttributeToControlUsergroups(data.getString("assertionAttributeToControlUsergroups"));
            identityProvider.setPermittedToControlCountryRights(data.getBoolean("permittedToControlCountryRights"));
            identityProvider.setAssertionAttributeToControlCountryRights(data.getString("assertionAttributeToControlCountryRights"));
            identityProvider.setOnCreationPortalPersonApprovalStatus(data.getString("onCreationPortalPersonApprovalStatus"));
            identityProvider.setCertificateToUse(data.getString("certificateToUse"));
            identityProvider.setPermittedToInitiateSAML(data.getBoolean(IS_PERMITTED_COL_NAME));
            
            
            {
                CFQuery query=queryDataAccess.getNewQuery();

                query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);

                query.setSQL("select usergroupID, permitsControl, setAtPersonCreation from SAMLIdentityProviderUsergroupControl where SAMLIdentityProviderId = :SAMLIdentityProviderId");

                CFQueryReturn queryResult=(CFQueryReturn)query.execute();
                RowSet rowset=queryResult.getResult();

                boolean moreResults=true;

                List<Integer> usergroupsAtCreation=new ArrayList<>();
                List<Integer> usergroupsControlled=new ArrayList<>();
                while(moreResults){
                    moreResults=rowset.next();

                    if (moreResults){
                        boolean setAtPersonCreation=rowset.getBoolean("setAtPersonCreation");
                        boolean permitsControl=rowset.getBoolean("permitsControl");

                        if (setAtPersonCreation){
                            usergroupsAtCreation.add(rowset.getInt("usergroupID"));
                        }
                        if (permitsControl){
                            usergroupsControlled.add(rowset.getInt("usergroupID"));
                        }

                    }
                }

                identityProvider.setUsergroupsAtPersonCreation(usergroupsAtCreation);
                identityProvider.setUsergroupsPermittedToControl(usergroupsControlled);
            }

            {
                //retrieve any usergroup countrys
                CFQuery query=queryDataAccess.getNewQuery();

                query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);

                query.setSQL("select countryID, permitsControl, permitsControl, setAtPersonCreation from SAMLIdentityProviderUsergroupCountryControl where SAMLIdentityProviderId = :SAMLIdentityProviderId");

                CFQueryReturn queryResult=(CFQueryReturn)query.execute();
                RowSet rowset=queryResult.getResult();

                boolean moreResults=true;

                List<Integer> usergroupCountriesAtCreation=new ArrayList<>();
                List<Integer> usergroupCountriesControlled=new ArrayList<>();
                while(moreResults){
                    moreResults=rowset.next();

                    if (moreResults){
                        boolean setAtPersonCreation=rowset.getBoolean("setAtPersonCreation");
                        boolean permitsControl=rowset.getBoolean("permitsControl");
                        if (setAtPersonCreation){
                            usergroupCountriesAtCreation.add(rowset.getInt("countryID"));
                        }
                        if (permitsControl){
                            usergroupCountriesControlled.add(rowset.getInt("countryID"));
                        }
                    }
                }

                identityProvider.setUsergroupCountriesAtPersonCreation(usergroupCountriesAtCreation);

            }

            //retrieve any authorised sites
            {
                CFQuery query=queryDataAccess.getNewQuery();

                query.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);

                query.setSQL("select siteDefID from SAMLIdentityProviderAuthorisedSite where SAMLIdentityProviderId = :SAMLIdentityProviderId");

                CFQueryReturn queryResult=(CFQueryReturn)query.execute();
                RowSet rowset=queryResult.getResult();

                boolean moreResults=true;

                List<Integer> authorisedSites=new ArrayList<>();
   
                while(moreResults){
                    moreResults=rowset.next();

                    if (moreResults){            
                        authorisedSites.add(rowset.getInt("siteDefID"));
                    }
                }
                
                identityProvider.setAuthorisedSitesToAuthenticate(authorisedSites);
            }

            //retrieve any mappings
            {
                CFQuery mappingQuery=queryDataAccess.getNewQuery();
                mappingQuery.addParamByOrder("SAMLIdentityProviderId", ""+identityProvider.getSAMLIdentityProviderId(), CFQuery.CF_SQL_INTEGER);

                mappingQuery.setSQL("select relaywareField, useAssertion, foreignValue from SAMLIdentityProviderMapping where SAMLIdentityProviderId = :SAMLIdentityProviderId");

                CFQueryReturn queryResult=(CFQueryReturn)mappingQuery.execute();
                RowSet rowset=queryResult.getResult();

                boolean moreResults=true;
                while(moreResults){
                    moreResults=rowset.next();

                    if (moreResults){
                        identityProvider.addMapping(rowset.getString("relaywareField"), rowset.getBoolean("useAssertion"), rowset.getString("foreignValue"));
                    }
                }

            }
                

        } catch (SQLException ex) {
            throw new RuntimeException("While attempting to retrieve " + identityProvider.getSAMLIdentityProviderId() + " an error occured", ex);
        }
        
    }


}
