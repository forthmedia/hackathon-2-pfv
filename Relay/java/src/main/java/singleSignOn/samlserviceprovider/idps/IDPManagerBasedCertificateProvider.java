/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

import java.util.HashMap;
import java.util.Map;
import singleSignOn.samlserviceprovider.MetadataProvider;

/**
 *
 * @author Richard.Tingle
 */
public class IDPManagerBasedCertificateProvider implements MetadataProvider{
    private final IDPManager idpManager;
    
    Map<String,IdentityProvider> enityIDBasedCache=new HashMap<>();

    
    
    public IDPManagerBasedCertificateProvider(IDPManager idpManager) {
        this.idpManager= idpManager;
    }
    
    private IdentityProvider getIdentityProvider_fromCacheIfAvailable(String issuer){
        if (enityIDBasedCache.containsKey(issuer)){
            return enityIDBasedCache.get(issuer);
        }else{
            IdentityProvider provider=idpManager.getIDPByEntityID(issuer);
            enityIDBasedCache.put(issuer, provider);
            return provider;
        }
    }
    
    
    
    @Override
    public boolean hasIssuer(String issuer) {
        
        IdentityProvider provider= getIdentityProvider_fromCacheIfAvailable(issuer);

        return provider!=null; 
    }

    @Override
    public String getRawMetadataForIssuer(String issuer) {
        IdentityProvider provider=getIdentityProvider_fromCacheIfAvailable(issuer);
        
        return provider.getRawIDPMetadata();
    }
    
    @Override
    public IdentityProvider getIdentityProvider(String issuer) {
        return getIdentityProvider_fromCacheIfAvailable(issuer);
    }
}
