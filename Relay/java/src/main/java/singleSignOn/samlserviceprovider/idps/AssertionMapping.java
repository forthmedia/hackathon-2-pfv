/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

/**
 *
 * @author Richard.Tingle
 */
public class AssertionMapping {
    private final String relaywareField;
    private final boolean useAssertion; //if not using the assertion then its hard coded
    private final String foreignValue; //either assertion field to use or the hard coded value.

    public AssertionMapping(String relaywareField, boolean useAssertion, String foreignValue) {
        this.relaywareField = relaywareField;
        this.useAssertion = useAssertion;
        this.foreignValue = foreignValue;
    }

    public String getRelaywareField() {
        return relaywareField;
    }

    public boolean isUseAssertion() {
        return useAssertion;
    }

    public String getForeignValue() {
        return foreignValue;
    }

    
}
