/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Richard.Tingle
 */
public class IdentityProvider {
    
    private static final int NEW_PROVIDER_INDEX=-1; 
    
    // the initial values represent defaults, "" not null as coldfusion hates nulls
    private int samlIdentityProviderId=NEW_PROVIDER_INDEX;	
    private String identityProviderName="";
    private String identityProviderIdentifier="";
    private String authnRequestURL="";
    private String nameIDField_Relayware="";
    private String nameIDField_Foreign="";
    
    private String rawIDPMetadata="";
    private boolean active=true;
    private boolean allowAutoCreate=false;
    private boolean autoCreate_SelectLocOrg=true; //if when autocreating organisation and org is just selected (if not then its detected from the assertion
    private int autoCreate_selectedOrganisationID=0;
    private int autoCreate_selectedLocationID=0;
    
    private String autoCreate_detectedLoc_RelaywareField="";
    private String autoCreate_detectedLoc_ForeignField="";
    
    private String certificateToUse = "";

    private List<Integer> usergroupsOnCreation=new ArrayList<>(0);
    private List<Integer> usergroupCountriesOnCreation=new ArrayList<>(0);
    
    private List<Integer> authorisedSitesToAuthenticate=new ArrayList<>(0);
    
    private boolean permittedToSetUsergroups=false;
    private List<Integer> usergroupsPermittedToControl=new ArrayList<>(0);
    private String assertionAttributeToControlUsergroups="";
    
    private boolean permittedToControlCountryRights=false;
    private String assertionAttributeToControlCountryRights="";
    
    private final Collection<AssertionMapping> mappings=new ArrayList<>();
    
    private String onCreationPortalPersonApprovalStatus="PerApproved"; //flag textID, by default people are approved at creation
    private boolean permittedToInitiateSAML;
    
    public IdentityProvider(){}
    
    public IdentityProvider(int samlIdentityProviderId){
        this.samlIdentityProviderId=samlIdentityProviderId;
    }
    
    public int getSAMLIdentityProviderId() {
        return samlIdentityProviderId;
    }
    public void setSAMLIdentityProviderId(int samlIdentityProviderId) {
        this.samlIdentityProviderId=samlIdentityProviderId;
    }

    public String getIdentityProviderName() {
        return identityProviderName;
    }

    public void setIdentityProviderName(String identityProviderName) {
        if (identityProviderName==null){identityProviderName="";}
        this.identityProviderName = identityProviderName;
    }

    public String getIdentityProviderIdentifier() {
        return identityProviderIdentifier;
    }

    public void setIdentityProviderIdentifier(String identityProviderIdentifier) {
        if (identityProviderIdentifier==null){identityProviderIdentifier="";}
        this.identityProviderIdentifier = identityProviderIdentifier;
    }

    public String getAuthnRequestURL() {
        return authnRequestURL;
    }

    public void setAuthnRequestURL(String authnRequestURL) {
        if (authnRequestURL==null){authnRequestURL="";}
        this.authnRequestURL = authnRequestURL;
    }

    public String getNameIDField_Relayware() {
        return nameIDField_Relayware;
    }

    public void setNameIDField_Relayware(String nameIDField_Relayware) {
        if (nameIDField_Relayware==null){nameIDField_Relayware="";}
        this.nameIDField_Relayware = nameIDField_Relayware;
    }

    public String getNameIDField_Foreign() {
        return nameIDField_Foreign;
    }

    public void setNameIDField_Foreign(String nameIDField_Foreign) {
        if (nameIDField_Foreign==null){nameIDField_Foreign="";}
        this.nameIDField_Foreign = nameIDField_Foreign;
    }

    public String getRawIDPMetadata() {
        return rawIDPMetadata;
    }

    public void setRawIDPMetadata(String rawIDPMetadata) {
        if (rawIDPMetadata==null){rawIDPMetadata="";}
        this.rawIDPMetadata = rawIDPMetadata;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Collection<AssertionMapping> getMappings() {
        return mappings;
    }
    
    public void clearMappings(){
        mappings.clear();
    }
    
    public void addMapping(String relaywareField, boolean useAssertion, String foreignValue){
        mappings.add(new AssertionMapping(relaywareField,useAssertion,foreignValue));
    }
    
    /**
     * Returns the mapping that maps the RW field --> something. If no such mapping
     * exists then null is returned
     * @param relaywareFieldName 
     * @return  
     */
    public AssertionMapping getMappingForRelaywareField(String relaywareFieldName){
        //isn't very efficient, will revisit is causes a problem
        for(AssertionMapping mapping:getMappings()){
            if (mapping.getRelaywareField().equalsIgnoreCase(relaywareFieldName)){
                return mapping;
            }
        }
        
        return null;
    }
    
    public boolean isExistingIDP(){
        return samlIdentityProviderId!=NEW_PROVIDER_INDEX;
    }

    public boolean getAllowAutoCreate() {
        return allowAutoCreate;
    }

    public void setAllowAutoCreate(boolean allowAutoCreate) {
        this.allowAutoCreate = allowAutoCreate;
    }

    public boolean getAutoCreate_SelectLocOrg() {
        return autoCreate_SelectLocOrg;
    }

    public void setAutoCreate_selectLocOrg(boolean autoCreate_SelectLocOrg) {
        this.autoCreate_SelectLocOrg = autoCreate_SelectLocOrg;
    }

    public int getAutoCreate_selectedOrganisationID() {
        return autoCreate_selectedOrganisationID;
    }

    public void setAutoCreate_selectedOrganisationID(Integer autoCreate_selectedOrganisationID) {
        if (autoCreate_selectedOrganisationID==null){
            autoCreate_selectedOrganisationID=0;
        }
        this.autoCreate_selectedOrganisationID = autoCreate_selectedOrganisationID;
    }

    public int getAutoCreate_selectedLocationID() {

        return autoCreate_selectedLocationID;
    }

    public void setAutoCreate_selectedLocationID(Integer autoCreate_selectedLocationID) {
        if (autoCreate_selectedLocationID==null){
            autoCreate_selectedLocationID=0;
        }
        this.autoCreate_selectedLocationID = autoCreate_selectedLocationID;
    }

    /**
     * Returns what a person who has just been autocreated as a result of this 
     * IDP should have as their usergroups. This is seperate (and really excludes)
     * the IDP providing ongoing management of the usergroups
     * @return 
     */
    public List<Integer> getUsergroupsAtPersonCreation(){
        return usergroupsOnCreation; 
    }
    
    public String getUsergroupsAtPersonCreation_asStringList() {
        return this.listToStringList(getUsergroupsAtPersonCreation());
    }
    
    public void setUsergroupsAtPersonCreation(List<Integer> usergroups){
        usergroupsOnCreation=usergroups;
    }
    
    public List<Integer> getAuthorisedSitesToAuthenticate() {
        return authorisedSitesToAuthenticate;
    }
    public String getAuthorisedSitesToAuthenticate_asStringList() {
        return this.listToStringList(getAuthorisedSitesToAuthenticate());
    }

    public void setAuthorisedSitesToAuthenticate(List<Integer> authorisedSitesToAuthenticate) {
        this.authorisedSitesToAuthenticate = authorisedSitesToAuthenticate;
    }
    public void setAuthorisedSitesToAuthenticate_fromStringList(String authorisedSitesToAuthenticate) {
        setAuthorisedSitesToAuthenticate(stringListToList(authorisedSitesToAuthenticate));
    }
    
    
    public List<Integer> getUsergroupCountriesAtPersonCreation(){
        return usergroupCountriesOnCreation;
    }
    
    public void setUsergroupCountriesAtPersonCreation(List<Integer> usergroupCountriesOnCreation){
        this.usergroupCountriesOnCreation=usergroupCountriesOnCreation;
    }
    
    public String getUsergroupCountriesAtPersonCreation_asStringList() {      
        return listToStringList(getUsergroupCountriesAtPersonCreation());
    }
    
    /**
     * Expects a string of the form "6,8,3" etc that are the usergroup IDs that an autocreated
     * person should start with.
     * @param stringList 
     */
    public void setUsergroupCountriesAtPersonCreation_fromStringList(String stringList){
        
        setUsergroupCountriesAtPersonCreation(stringListToList(stringList));
    }
    
    
    /**
     * Expects a string of the form "6,8,3" etc that are the usergroup IDs that an autocreated
     * person should start with.
     * @param stringList 
     */
    public void setUsergroupsAtPersonCreation_fromStringList(String stringList){        
        setUsergroupsAtPersonCreation(stringListToList(stringList));
    }

    public String getAutoCreate_detectedLoc_RelaywareField() {
        return autoCreate_detectedLoc_RelaywareField==null?"":autoCreate_detectedLoc_RelaywareField;
    }

    public void setAutoCreate_detectedLoc_RelaywareField(String autoCreate_detectedLoc_RelaywareField) {
        this.autoCreate_detectedLoc_RelaywareField = autoCreate_detectedLoc_RelaywareField;
    }

    public String getAutoCreate_detectedLoc_ForeignField() {
        return autoCreate_detectedLoc_ForeignField==null?"":autoCreate_detectedLoc_ForeignField;
    }

    public void setAutoCreate_detectedLoc_ForeignField(String autoCreate_detectedLoc_ForeignField) {
        this.autoCreate_detectedLoc_ForeignField = autoCreate_detectedLoc_ForeignField;
    }
    
    
    
    
    @Override
    public String toString() {
        return "IdentityProvider{" + "samlIdentityProviderId=" + samlIdentityProviderId 
            + ", identityProviderName=" + identityProviderName 
            + ", identityProviderIdentifier=" + identityProviderIdentifier + '}';
    }

    public boolean isPermittedToSetUsergroups() {
        return permittedToSetUsergroups;
    }

    public void setPermittedToSetUsergroups(boolean permittedToSetUsergroups) {
        this.permittedToSetUsergroups = permittedToSetUsergroups;
    }

    public List<Integer> getUsergroupsPermittedToControl() {
        return usergroupsPermittedToControl;
    }

    public void setUsergroupsPermittedToControl(List<Integer> usergroupsPermittedToControl) {
        this.usergroupsPermittedToControl = usergroupsPermittedToControl;
    }

    public void setUsergroupsPermittedToControl_fromStringList(String usergroupsPermittedToControl) {      
        setUsergroupsPermittedToControl(stringListToList(usergroupsPermittedToControl));
    }

    public String getUsergroupsPermittedToControl_asStringList() {      
        return listToStringList(getUsergroupsPermittedToControl());
    }
    
    public String getAssertionAttributeToControlUsergroups() {
        return assertionAttributeToControlUsergroups==null?"":assertionAttributeToControlUsergroups;
    }

    public void setAssertionAttributeToControlUsergroups(String assertionAttributeToControlUsergroups) {
        this.assertionAttributeToControlUsergroups = assertionAttributeToControlUsergroups;
    }
    
    /**
     * Converts a comma seperated list to a List
     * @param stringList
     * @return 
     */
    private List<Integer> stringListToList(String stringList){
        if (stringList.length()>0){
            String[] individualElements=stringList.split(",");
        
            List<Integer> list=new ArrayList<>(individualElements.length);
            for(String individualElement:individualElements){
                list.add((Integer.parseInt(individualElement)));
            }
            return list;
        }else{
            return new ArrayList<>(0);
        }
        
        
        
        
    }
    
    /**
     * Converts a List to a comma seperated list
     * @param stringList
     * @return 
     */
    private String listToStringList(List<Integer> list){
        String stringList="";

        for(int i=0;i<list.size();i++){
            stringList+=list.get(i);

            if (i!=list.size()-1){
                stringList+=",";
            }
        }
        
        return stringList;
    }

    public boolean isPermittedToControlCountryRights() {
        return permittedToControlCountryRights;
    }

    public void setPermittedToControlCountryRights(boolean permittedToControlLanguageRights) {
        this.permittedToControlCountryRights = permittedToControlLanguageRights;
    }

    public String getAssertionAttributeToControlCountryRights() {
        if (assertionAttributeToControlCountryRights==null){
            return "";
        }else{
        
            return assertionAttributeToControlCountryRights;
        }
    }

    public void setAssertionAttributeToControlCountryRights(String assertionAttributeToControlLanguageRights) {
        this.assertionAttributeToControlCountryRights = assertionAttributeToControlLanguageRights;
    }

    public String getOnCreationPortalPersonApprovalStatus() {
        //coldfusion hates nulls, convert to ''
        return onCreationPortalPersonApprovalStatus==null?"":onCreationPortalPersonApprovalStatus;
    }
    public String getCertificateToUse() {
        //must not allow null to be returned. CF hates nulls
        return certificateToUse==null?"":certificateToUse;
    }

    public void setOnCreationPortalPersonApprovalStatus(String onCreationPortalPersonApprovalStatus) {
        this.onCreationPortalPersonApprovalStatus = onCreationPortalPersonApprovalStatus;
    }
    public void setCertificateToUse(String certificateToUse) {
      this.certificateToUse = certificateToUse;
    }

    

    public boolean isPermittedToInitiateSAML() {
      return permittedToInitiateSAML;
    }

    public void setPermittedToInitiateSAML(boolean permittedToInitiateSAML) {
      this.permittedToInitiateSAML = permittedToInitiateSAML;
    }  

}
