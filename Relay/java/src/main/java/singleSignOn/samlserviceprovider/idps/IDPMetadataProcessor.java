/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider.idps;

import coldfusionbridge.interfaces.CFDumpService;
import coldfusionbridge.wrappers.ComponentFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Objects;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml2.metadata.SingleSignOnService;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.signature.X509Certificate;
import org.opensaml.xml.signature.X509Data;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import singleSignOn.samlserviceprovider.SAMLBoot;

/**
 *
 * @author Richard.Tingle
 */
public class IDPMetadataProcessor {

    public IDPMetadataProcessor() throws ConfigurationException {
        SAMLBoot.bootIfRequired();
    }
    
    
    
    public Metadata readIDPMetadata(String rawMetadata) throws ParserConfigurationException, SAXException, IOException, UnmarshallingException{
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();
        
        Element element;
        try{
            Document document =docBuilder.parse(new InputSource(new ByteArrayInputStream(rawMetadata.getBytes("utf-8"))));
            element = document.getDocumentElement();
        }catch (Exception e){
            return Metadata.invalid("File passed did not contain valid XML");
        }
        
        
        UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
        Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
        EntityDescriptor metadata = (EntityDescriptor) unmarshaller.unmarshall(element);
        
        String entityName=metadata.getEntityID();
        
        
        
        IDPSSODescriptor descriptor=metadata.getIDPSSODescriptor(SAMLConstants.SAML20P_NS); //returns the first descriptor that supports SAML 2.0
        
        if (descriptor==null){
            return Metadata.invalid("No IDPSSODescriptor found. Is this IDP metadata?");
        }
        

        KeyDescriptor keyDescriptor_signing=null;
        KeyDescriptor keyDescriptor_unspecified=null; //a fall back cert (will report back invalid)
        
        for(KeyDescriptor keyDescriptor:descriptor.getKeyDescriptors()){
            if (keyDescriptor.getUse().equals(UsageType.SIGNING) ){
                keyDescriptor_signing=keyDescriptor;
            }else if(keyDescriptor.getUse().equals(UsageType.UNSPECIFIED) ){
                keyDescriptor_unspecified=keyDescriptor;
            }
        }
        
        if (keyDescriptor_signing==null){
            keyDescriptor_signing=keyDescriptor_unspecified;
        }
        if (keyDescriptor_signing==null){
            return Metadata.invalid("No signing key descriptor found in metadata");
        }
        
        //we check there is a certificate, we don't actually care what it is though
        List<X509Data> x509Datas=keyDescriptor_signing.getKeyInfo().getX509Datas();
        if (x509Datas.isEmpty()){
            return Metadata.invalid("Was no X509Data in the metadata");
        }else{
            X509Data x509Data=x509Datas.get(0);
            x509Data.getX509Certificates();

            List<X509Certificate> certificates=x509Data.getX509Certificates();
            if (certificates.isEmpty()){
                return Metadata.invalid("No X509Certificate in the metadata");
            }
            
        }
           
        //grab the appropriate endpoint
        String endpointLocation=null;
        for(SingleSignOnService singleSignOnService:descriptor.getSingleSignOnServices()){
            if (singleSignOnService.getBinding().equals(SAMLConstants.SAML2_POST_BINDING_URI)){
                endpointLocation=singleSignOnService.getLocation();
            }
        }
        
        if (endpointLocation==null){
            return Metadata.invalid("No HTTPPOST binding endpoint found. Expects a single sign on service with binding " + SAMLConstants.SAML2_POST_BINDING_URI);
        }
        
        return Metadata.valid(entityName,endpointLocation);
        
    }
    
    public static class Metadata{
        private final String entityID;
        private final String singleSignOnServiceURL_POSTBinding;
        private final boolean isValid;
        private final String invalidReason;

        private Metadata(String entityID, String singleSignOnServiceURL_POSTBinding, boolean isValid, String invalidReason) {
            this.entityID = entityID;
            this.singleSignOnServiceURL_POSTBinding = singleSignOnServiceURL_POSTBinding;
            this.isValid = isValid;
            this.invalidReason = invalidReason;
        }
        
        public static Metadata valid(String entityID, String singleSignOnServiceURL_POSTBinding){
            return new Metadata(entityID, singleSignOnServiceURL_POSTBinding,true,"");
        }
        public static Metadata invalid(String invalidReason){
            return new Metadata(null,null,false,invalidReason);
        }

        public String getEntityID() {
            return entityID;
        }


        public String getSingleSignOnServiceURL_POSTBinding() {
            return singleSignOnServiceURL_POSTBinding;
        }

        public boolean isValid() {
            return isValid;
        }

        public String getInvalidReason() {
            return invalidReason;
        }
        
        

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.entityID);
            hash = 97 * hash + Objects.hashCode(this.singleSignOnServiceURL_POSTBinding);
            hash = 97 * hash + (this.isValid ? 1 : 0);
            hash = 97 * hash + Objects.hashCode(this.invalidReason);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Metadata other = (Metadata) obj;
            if (this.isValid != other.isValid) {
                return false;
            }
            if (!Objects.equals(this.entityID, other.entityID)) {
                return false;
            }
            if (!Objects.equals(this.singleSignOnServiceURL_POSTBinding, other.singleSignOnServiceURL_POSTBinding)) {
                return false;
            }
            if (!Objects.equals(this.invalidReason, other.invalidReason)) {
                return false;
            }
            return true;
        }

        
        
        @Override
        public String toString() {
            if (isValid){
                return "Metadata{ invalid because " + invalidReason +"}";
            }else{
                return "Metadata{" + "entityID=" + entityID + ", singleSignOnServiceURL_POSTBinding=" + singleSignOnServiceURL_POSTBinding +'}'; 
            }
        }

        
        
    }
    
}
