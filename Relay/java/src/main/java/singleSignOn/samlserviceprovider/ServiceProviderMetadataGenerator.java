/*
 *  (c)Relayware. All Rights Reserved 2014
 */
package singleSignOn.samlserviceprovider;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.metadata.AssertionConsumerService;
import org.opensaml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml2.metadata.SPSSODescriptor;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.credential.UsageType;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.x509.X509Credential;
import org.opensaml.xml.security.x509.X509KeyInfoGeneratorFactory;
import org.opensaml.xml.util.XMLHelper;
import org.w3c.dom.Element;

/**
 *
 * @author Richard.Tingle
 */
public class ServiceProviderMetadataGenerator {
    
    private final X509CredentialsProvider credentialProvider;
    
    public ServiceProviderMetadataGenerator() throws ConfigurationException {
        SAMLBoot.bootIfRequired();
        credentialProvider = new X509CredentialsProvider();
    }
    
    public String buildMetadata(RelaywareAsSPConfiguration relaywareAsSPConfiguration, String certificationKeyStorePath, String certificateAlias, String password) throws MarshallingException{
        XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();
        SAMLObjectBuilder<EntityDescriptor> builder = (SAMLObjectBuilder<EntityDescriptor>) builderFactory.getBuilder(EntityDescriptor.DEFAULT_ELEMENT_NAME);
        EntityDescriptor descriptor=builder.buildObject();
        descriptor.setEntityID(relaywareAsSPConfiguration.getRelaywareIssuer());
        
        SAMLObjectBuilder<SPSSODescriptor> spssoDescriptorBuilder = (SAMLObjectBuilder<SPSSODescriptor>) builderFactory.getBuilder(SPSSODescriptor.DEFAULT_ELEMENT_NAME);
        SPSSODescriptor spssoDescriptor= spssoDescriptorBuilder.buildObject();
        
        spssoDescriptor.setWantAssertionsSigned(true);   
        if(certificateAlias != null && !"".equals(certificateAlias)) {
            spssoDescriptor.setAuthnRequestsSigned(true);
            signMetadata(certificationKeyStorePath, certificateAlias, password, builderFactory,
              spssoDescriptor);
        } else {
            spssoDescriptor.setAuthnRequestsSigned(false);
        }
        
        spssoDescriptor.addSupportedProtocol(SAMLConstants.SAML20P_NS);
        
        SAMLObjectBuilder<AssertionConsumerService> assertionConsumerServiceDescriptorBuilder = (SAMLObjectBuilder<AssertionConsumerService>) builderFactory.getBuilder(AssertionConsumerService.DEFAULT_ELEMENT_NAME);   
        AssertionConsumerService assertionConsumerService = assertionConsumerServiceDescriptorBuilder.buildObject();
        assertionConsumerService.setIndex(0);
        assertionConsumerService.setBinding(SAMLConstants.SAML2_POST_BINDING_URI);
        assertionConsumerService.setLocation(relaywareAsSPConfiguration.getAssertionConsumerURL());    
        
        spssoDescriptor.getAssertionConsumerServices().add(assertionConsumerService);

        descriptor.getRoleDescriptors().add(spssoDescriptor);    
        
        MarshallerFactory marshallerFactory = Configuration.getMarshallerFactory();
        Marshaller marshaller = marshallerFactory.getMarshaller(descriptor);
        Element metadataElement = marshaller.marshall(descriptor);
       
        String metadataString=XMLHelper.nodeToString(metadataElement);
        
        return metadataString;
    }

    private void signMetadata(String certificationKeyStorePath, String certificateAlias,
        String password, XMLObjectBuilderFactory builderFactory, SPSSODescriptor spssoDescriptor) {
      X509KeyInfoGeneratorFactory keyInfoGeneratorFactory = new X509KeyInfoGeneratorFactory();
      keyInfoGeneratorFactory.setEmitEntityCertificate(true);
      KeyInfoGenerator keyInfoGenerator = keyInfoGeneratorFactory.newInstance();
          
      X509Credential credential = credentialProvider.getX509Credential(certificationKeyStorePath, certificateAlias, password);
      
      KeyDescriptor signKeyDescriptor = (KeyDescriptor)builderFactory.getBuilder(KeyDescriptor.DEFAULT_ELEMENT_NAME).buildObject(KeyDescriptor.DEFAULT_ELEMENT_NAME);
      signKeyDescriptor.setUse(UsageType.SIGNING);
      try {
          // Generating key info. The element will contain the public key. The key is used to by the IDP to verify signatures
          signKeyDescriptor.setKeyInfo(keyInfoGenerator.generate(credential));
      } catch (SecurityException ex) {
          Logger.getLogger(ServiceProviderMetadataGenerator.class.getName()).log(Level.SEVERE, null, ex);
          throw new IllegalStateException(ex);
      }
      spssoDescriptor.getKeyDescriptors().add(signKeyDescriptor);
    }
    
}
