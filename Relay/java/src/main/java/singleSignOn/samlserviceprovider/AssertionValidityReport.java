/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

/**
 *
 * @author Richard.Tingle
 */
public class AssertionValidityReport {

    
    private final boolean valid;
    
    private final AssertionRejectionReason reason;
    private final String message;

    public boolean isValid() {
        return valid;
    }

    public AssertionRejectionReason getReason() {
        return reason;
    }

    public String getMessage() {
        return message;
    }

    
    
    
    
    private AssertionValidityReport(boolean valid, AssertionRejectionReason reason, String message) {
        this.valid = valid;
        this.reason = reason;
        this.message = message;
    }
    public static AssertionValidityReport accept() {
        return new AssertionValidityReport(true,null, "Accept");
    }
    
    
    public static AssertionValidityReport reject(AssertionRejectionReason reason, String message){
        return new AssertionValidityReport(false,reason,message);
    }

    @Override
    public String toString() {
        return "AssertionReport{" + "valid=" + valid + ", reason=" + reason + ", message=" + message + '}';
    }
    
    
}
