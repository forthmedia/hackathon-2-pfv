/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

/**
 *
 * @author Richard.Tingle
 */
public class RelaywareAsSPConfiguration {

    final String issuer; 
    final String assertionConsumerURL; 

    public RelaywareAsSPConfiguration(String issuer, String assertionConsumerURL) {
        this.issuer = issuer;
        this.assertionConsumerURL = assertionConsumerURL;
    }

    public String getRelaywareIssuer() {
        return issuer;
    }

    String getAssertionConsumerURL() {
        return assertionConsumerURL;
    }
    
}
