/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

/**
 *
 * @author Richard.Tingle
 */
public enum AssertionRejectionReason {
    IssuerNotRecognised,
    NoSignature,
    SignatureWasMalformed, 
    SignatureWasInvalid,
    ResponseNotAcceptedFormat, //this will probably have more detail in the message
    NotInPermittedTimePeriod,
    InResponseToContainsInvalidId,
    IdentityProviderIsNotPermitted
}
