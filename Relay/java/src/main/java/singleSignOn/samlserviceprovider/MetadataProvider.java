/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

import singleSignOn.samlserviceprovider.idps.IdentityProvider;

/**
 *
 * @author Richard.Tingle
 */
public interface MetadataProvider {
    
    /**
     * Returns if the CertificateProvider has a certificate for the requested issuer
     * @param issuer
     * @return 
     */
    public boolean hasIssuer(String issuer);
    
    /**
     * Provides a string (which will probably begin MII) that is the certificate of the IDP 
     * @param issuer
     * @return 
     */
    public String getRawMetadataForIssuer(String issuer);
    
    
    /**
     * Provides {@code IdentityProvider} for the given issuer.
     * @param issuer
     * @return {@code IdentityProvider} 
     */
    public IdentityProvider getIdentityProvider(String issuer);
}
