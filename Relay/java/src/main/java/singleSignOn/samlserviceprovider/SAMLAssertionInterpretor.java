/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleSignOn.samlserviceprovider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;

/**
 * This function extracts data from a SAML assertion.
 * 
 * IT DOES NOT VALIDATE IT, that should be done by SAMLAssertionValidator
 * @author Richard.Tingle
 */
public class SAMLAssertionInterpretor {

    public SAMLAssertionInterpretor() throws ConfigurationException {
        SAMLBoot.bootIfRequired();
    }
    


    
    public AssertionContentReport interpret(Response response){
        
        Assertion assertion=response.getAssertions().get(0);
        
        String nameID;
        
        try{
            nameID=assertion.getSubject().getNameID().getValue();
        }catch (NullPointerException e){
            //the nameID is often missing, this is acceptable
            nameID=null;
        }
        
        Map<String,List<String>> attributesMap=new HashMap<>();
        
        List<AttributeStatement> attributeStatements =assertion.getAttributeStatements();
        
        for(AttributeStatement attributeStatement:attributeStatements){
            List<Attribute> attributes = attributeStatement.getAttributes();
            
            for(Attribute attribute:attributes){
                List<String> attributeValues_list=new ArrayList<>();
                
                List<XMLObject> attributeValues=attribute.getAttributeValues();
                
                for(XMLObject attributeValue:attributeValues){
                    
                    attributeValues_list.add(attributeValue.getDOM().getTextContent());
                }
                
                attributesMap.put(attribute.getName(),attributeValues_list );
            }
        }
        
        
        return new AssertionContentReport(response.getIssuer().getValue(), nameID,attributesMap);
    }
    
}
