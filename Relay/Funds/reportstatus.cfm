<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportStatus.cfm	
Author:				???
Date started:		2007-08-29
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
12-09-2012			RMB			P-LEN078 CR-098 - added "fundRequestActivityID" to report
Possible enhancements:

 --->


<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="Organisation_Name">

<cfset tableName="vMDFStatus">
<cfset dateField="Date_Of_Status_Change">
<cfset dateRangeFieldName="approval_date">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm">

<cfquery name="StatusReport" datasource="#application.siteDataSource#">
select * from vMDFStatus
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_title>Status Report</cf_title>

<cfset currencyCols = "amount_Approved">
<cfset dateCols = "Approval_Date,Date_Of_Status_Change">
<cfset filter1cols = "Status,Country,Budget,Activity_Type,Product_Category">

<CF_tableFromQueryObject queryObject="#StatusReport#" 
	showTheseColumns="Status,Date_Of_Status_Change,Organisation_Name,Country,Budget,Amount_Approved,Approval_Date,Request_Activity_ID"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	totalTheseColumns="#currencyCols#"
	dateFormat ="#dateCols#"
	FilterSelectFieldList="#filter1cols#"
	queryWhereClauseStructure="#queryWhereClause#"
	passThroughVariablesStructure="#passThruVars#"
>