<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			fundAccounts.cfm
Author:				NJH
Date started:		2007-06-14
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008/08/18			NJH			CR-SNY651 - changed hard-coded references to http to a variable that gets set to http if cgi.HTTPS is not "on"; else https
2009-09-14 			NYB			LHID2620 - fixed org link to open in new tab
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2010/11/25			PPB			removed a Sel All that caused a js error (tho save still worked) and didn't seem necessary
2011/03/08 			PPB			LID5794 - get the TwoSelectCombo working for selection of multiple account managers if setting UniqueFundManager="No"
2011/03/09 			PPB 		LID5837 - added the mode field so that after saving a fundaccount the user is returned to the list not left in edit mode
2011/06/20			GCC			Changed link to dataframe to use orgid instead of account name to avoid issues with '&' in account names
2012-10-19			IH			Case 429631 add action attribute to editAccountForm
2012-11-15 			PPB 		Case 431990 createFundAccount function now returns a structure
2013-03-06 			NYB 		Case 432006 changed to retain filter after returns from edit
2016-01-29			WAB 		Removed twoselects combolist, switched to html-div layout
2016-02-17			RJT			PROD2015-618 On save the "Fund Account xxxxx was updated successfully" UI message is now displayed
2016-10-11			atrunov		Case https://relayware.atlassian.net/browse/SSPS-48, added UserGroup selection box on Fund Accounts page, this change will go to Core
2016-10-21			atrunov		Case https://relayware.atlassian.net/browse/SSPS-49, integrated User Group Rule for fund account into existing functionality
Possible enhancements:

 --->

<cf_title>Points Management</cf_title>

<!--- START:  NYB 2009-09-14 LHID2620 - added: --->
<cf_includeJavascriptOnce template = "/javascript/extextension.js">
<!--- END:  NYB 2009-09-14 LHID2620 --->

<!---Initialize variables expected later in the code--->
<CFPARAM DEFAULT="Last" NAME="frmType">

<cfparam name="mode" type="string" default="view">
<cfparam name="accountID" type="numeric" default="1">

<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfif structKeyExists(form,"mode")>
	<cfset mode = form.mode>
<cfelseif structKeyExists(url,"mode")>
	<cfset mode = url.mode>
</cfif>

<cfif isDefined("frmAddAccount")>
	<cfscript>
		newFundAccountID = application.com.relayFundManager.createFundAccount(organisationID = frmOrganisationID).accountId;		/* 2012-11-15 PPB Case 431990 createFundAccount function now returns a structure */
		/* need to param frmAccountManagers because multiselect may not be selected */
		param name = "form.frmAccountManagers" default = "";

		managerArray = listToArray(frmAccountManagers);
		for (i=1; i lte arraylen(managerArray); i = i + 1){
			application.com.relayFundManager.addFundAccountManager(accountID=newFundAccountID,fundManagerID=managerArray[i]);
		}

		/* need to param frmAccountManagersGroups because multiselect may not be selected */
		param name = "form.frmAccountManagersGroups" default = "";
		addManagerGroupArray = listToArray(frmAccountManagersGroups);

		for (i=1; i lte arraylen(addManagerGroupArray); i = i + 1){
			if (addManagerGroupArray[i] neq 0) {
				application.com.relayFundManager.addFundAccountManagerGroup(accountID=newFundAccountID,userGroupID=addManagerGroupArray[i]);
			}
		}

		application.com.relayFundManager.assignAvailableBudgetsToAccount(accountID=newFundAccountID);

		application.com.relayui.setMessage(
						message="phr_Fund_Account #newFundAccountID# phr_Sys_WasAddedSuccessfully"
					);

	</cfscript>

<cfelseif isDefined("frmEditAccount")>

	<cfscript>
		if (not isDefined("frmAccountClosed")) {
			frmAccountClosed=0;
		}
		application.com.relayFundManager.updateFundAccount(accountID=accountID,accountClosed=frmAccountClosed);

		/* need to param frmAccountManagers because multiselect may not be selected */
		param name = "form.frmAccountManagers" default = "";
		// add the account managers
		addManagerArray = listToArray(frmAccountManagers);

		for (i=1; i lte arraylen(addManagerArray); i = i + 1){
			// if the personID to be added is not 0.
			if (addManagerArray[i] neq 0) {
				// if the new manager doesn't already exist
				if (not listContains(currentFundManagerIDs,addManagerArray[i])) {
					application.com.relayFundManager.addFundAccountManager(accountID=accountID,fundManagerID=addManagerArray[i]);
				}
			}
		}

		/* need to param frmAccountManagersGroups because multiselect may not be selected */
		param name = "form.frmAccountManagersGroups" default = "";
		addManagerGroupArray = listToArray(frmAccountManagersGroups);

		for (i=1; i lte arraylen(addManagerGroupArray); i = i + 1){
			if (addManagerGroupArray[i] neq 0) {
				if (not listContains(currentFundManagerGroupsIDs,addManagerGroupArray[i])) {
					application.com.relayFundManager.addFundAccountManagerGroup(accountID=accountID,userGroupID=addManagerGroupArray[i]);
				}
			}
		}


		removeManagerArray = listToArray(currentFundManagerIDs);
		for (i=1; i lte arraylen(removeManagerArray); i = i + 1){
			// if the managerID is not in the list of managerIDs to keep
			if (not listContains(frmAccountManagers,removeManagerArray[i])) {
				application.com.relayFundManager.removeFundAccountManager(accountID=accountID,fundManagerID=removeManagerArray[i]);
			}
		}

		removeManagerGroupArray = listToArray(currentFundManagerGroupsIDs);
		for (i=1; i lte arraylen(removeManagerGroupArray); i = i + 1){
			// if the managerID is not in the list of managerIDs to keep
			if (not listContains(frmAccountManagersGroups,removeManagerGroupArray[i])) {
				application.com.relayFundManager.removeFundAccountManagerGroup(accountID=accountID,userGroupID=removeManagerGroupArray[i]);
			}
		}


		application.com.relayui.setMessage(
						message="phr_Fund_Account #accountID# phr_Sys_WasUpdatedSuccessfully"
					);

	</cfscript>
</cfif>

<!--- query to populate the fundManagers dropdown or twoSelectsComboList --->
<cfif mode eq "edit" or mode eq "add">
	
	<cfscript>
		getCurrentFundManagers = application.com.relayFundManager.getFundAccountManagers(accountID=accountID, userGroupRule=false);
		currentFundManagerIDs = valueList(getCurrentFundManagers.personID);
		getAvailablePeopleAtOrg = application.com.relayFundManager.getPotentialFundManagers(accountID=accountID);
		getAvailableFundManagersGroups = application.com.relayFundManager.getPotentialFundManagersGroups();
		getCurrentFundManagersGroups = application.com.relayFundManager.getFundAccountManagersGroups(accountID=accountID);
		currentFundManagerGroupsIDs = valueList(getCurrentFundManagersGroups.Data);
	</cfscript>

</cfif>



<cfif mode eq "edit">

	<cfscript>
		accountQry = application.com.relayFundManager.getFundAccounts(accountID=accountID, ShowLiveAccountsOnly=0);
	</cfscript>
<!--- 	<cfif request.budgetExclusion>
	<cfelse>
		<cfscript>
			getCurrentBudgets = application.com.relayFundManager.getAccountBudgets(accountID=accountID);
			currentBudgetIDs = valueList(getCurrentBudgets.budgetID);
			currentBudgets = valueList(getCurrentBudgets.budget);
			getAvailableBudgets = application.com.relayFundManager.getAvailableBudgetsForAccount(accountID=accountID);
			availableBudgetIDs = valueList(getAvailableBudgets.budgetID);
			availableBudgets = valueList(getAvailableBudgets.budget);
		</cfscript>
	</cfif> --->

	<form name="editAccountForm" method="post">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="accountID" currentValue="#accountID#" label="">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="account" currentValue="#accountID#" label="phr_fund_accountID">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="organisationName" currentValue="#accountQry.organisationName#" label="phr_fund_organisation">
			<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldname="frmAccountClosed" currentValue="#accountQry.accountClosed#" label="phr_fund_accountClosed" value="1">


			<!--- START: 2013-03-06 NYB Case 432006 added: --->
				<cfloop list="filterselect1,filterselect2,filterselectvalues1,filterselectvalues2" index="i">
					<cfif structkeyexists(url,"#i#")>
						<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="#i#" currentValue="#evaluate('url.#i#')#" label="">
					</cfif>
				</cfloop>
			<!--- END: 2013-03-06 NYB Case 432006  --->


			<!--- if we only have one fund manager per account, display single dropdown otherwise show a multi-select box --->
			<cfif false and application.com.settings.getSetting("fundManager.uniqueFundManager")>

				<cfset FundManagerQry = application.com.relayFundManager.getFundAccountManagers(accountID=accountID, userGroupRule=false)>
				<cfquery name="getAllAvailableManagers" datasource="#application.siteDataSource#">
					select p.personID, p.firstname + ' '+ p.lastname as fullname from person p
						inner join location l on l.locationID = p.locationID
						inner join fundCompanyAccount fca on fca.organisationID = l.organisationID
					where accountID =  <cf_queryparam value="#accountID#" CFSQLTYPE="CF_SQL_INTEGER" >
					order by fullname
				</cfquery>

				<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmAccountManagers" currentValue="#FundManagerQry.personID#" label="phr_fund_accountManager" query="#getAllAvailableManagers#" value="personID" display="fullname" nullValue="" nullText="phr_fund_pleaseSelectAFundManager">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="currentFundManagerIDs" currentValue="#valueList(FundManagerQry.personID)#" label="">

			<cfelse>
				<cf_relayFormElementDisplay relayFormElementType="select"
											fieldname="frmAccountManagers"
											currentValue=#valueList(getCurrentFundManagers.personID)#
											label="phr_fund_accountManagers"
											query=#getAvailablePeopleAtOrg#
											multiple = true
											display="fullname"
											value="personid"
											requiredFunction="return (jQuery('##frmAccountManagersGroups').val() != null) ? false : true"
											requiredMessage="phr_fund_accountManagers is required if no value is entered in phr_fund_accountManagersGroups">

				<cf_relayFormElementDisplay relayFormElementType="select"
											fieldname="frmAccountManagersGroups"
											label="phr_fund_accountManagersGroups"
											query = #getAvailableFundManagersGroups#
											currentValue = #valueList(getCurrentFundManagersGroups.Data)#
											multiple = true
											display="displayValue"
											value="dataValue"
											requiredFunction="return (jQuery('##frmAccountManagers').val() != null) ? false : true"
											requiredMessage="phr_fund_accountManagersGroups is required if no value is entered in phr_fund_accountManagers">

				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="currentFundManagerIDs" currentValue="#currentFundManagerIDs#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="currentFundManagerGroupsIDs" currentValue="#currentFundManagerGroupsIDs#" label="">
			</cfif>

			<!--- 2011/03/09 PPB LID5837 added the mode field so that after saving a fundaccount the user is returned to the list not left in edit mode --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="view" label="">

			<!--- 2012/10/24 - RMB - P-LEN056 CR-081 --->
			<cf_relayFormElementDisplay relayFormElementType="html" label="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmCancel" currentValue="phr_Cancel" label="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmEditAccount" currentValue="phr_save" label="">
			</cf_relayFormElementDisplay>
			<!--- 2012/10/12 - RMB - P-LEN056 CR-081 --->

		</cf_relayFormDisplay>
	</form>
<cfelseif mode eq "add">

	<cfoutput>
		<script type='text/javascript' src='/mxajax/core/js/prototype.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxAjax.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxSelect.js'></script>
		<script type='text/javascript' src='/mxajax/core/js/mxData.js'></script>
	</cfoutput>

	<!--- NJH 2008/08/18 CR-SNY651 the protocol variable is set in funds\application .cfm --->

	<script language="javascript">

		<cfoutput>var url = "#request.currentSite.httpProtocol##jsStringFormat(request.relaycurrentUser.sitedomain)#/WebServices/mxAjaxFundRequests.cfc";</cfoutput>

		function init() {
			new mxAjax.Select({
				parser: new mxAjax.CFArrayToJSKeyValueParser(),
				executeOnLoad: true,
				target: "frmOrganisationID",
				paramArgs: new mxAjax.Param(url,{param:"countryID={countryID}", cffunction:"getSelectOrganisations"}),
				source: "countryID"
			});
		}

		addOnLoadEvent(function() {
			init();
		});
	</script>
	<cfscript>
		qrynull=querynew("tempcol");
		countryQry = application.com.commonQueries.getCountries();
	</cfscript>

	<cfquery name="getOrgsWithoutAccounts" datasource="#application.siteDataSource#">
		select organisationID as value, organisationName as display from organisation where organisationID not in
			(select organisationID from fundCompanyAccount)
	</cfquery>

	<!--- 2011/03/08 PPB LID5794...added onSubmit fn (needed when UniqueFundManager = No and we show a TwoSelectsCombo)  --->
	<form name="addAccountForm"  method="post">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="select" fieldname="countryID" currentValue="" label="phr_fund_country" query="#countryQry#" value="countryID" display="country">
			<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmOrganisationID" currentValue="" label="phr_fund_Organisation" query="#qrynull#" value="tempCol" display="tempCol">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="accountManagers" currentValue="" label="phr_fund_accountManagers">

			<!--- START: 2013-03-06 NYB Case 432006 added: --->
				<cfloop list="filterselect1,filterselect2,filterselectvalues1,filterselectvalues2" index="i">
					<cfif structkeyexists(url,"#i#")>
						<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="#i#" currentValue="#evaluate('url.#i#')#" label="">
					</cfif>
				</cfloop>
			<!--- END: 2013-03-06 NYB Case 432006  --->

				<cf_relayFormElementDisplay relayFormElementType="select"
											fieldname="frmAccountManagers"
											currentValue=#valueList(getCurrentFundManagers.personID)#
											label="phr_fund_accountManagers"
											query=#getAvailablePeopleAtOrg#
											multiple = true
											display="fullname"
											value="personid"
											requiredFunction="return (jQuery('##frmAccountManagersGroups').val() != null) ? false : true"
											requiredMessage="phr_fund_accountManagers is required if no value is entered in phr_fund_accountManagersGroups">

			</cf_relayFormElementDisplay>

			<cf_relayFormElementDisplay relayFormElementType="select"
										fieldname="frmAccountManagersGroups"
										label="phr_fund_accountManagersGroups"
										query = #getAvailableFundManagersGroups#
										currentValue = #valueList(getCurrentFundManagersGroups.Data)#
										multiple = true
										display="displayValue"
										value="dataValue"
										requiredFunction="return (jQuery('##frmAccountManagers').val() != null) ? false : true"
										requiredMessage="phr_fund_accountManagersGroups is required if no value is entered in phr_fund_accountManagers">

			<!--- 2012/09/12 - RMB - P-LEN056 CR-081 --->
			<cf_relayFormElementDisplay relayFormElementType="html" label="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmCancel" currentValue="phr_Cancel" label="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmAddAccount" currentValue="phr_add" label="">
			</cf_relayFormElementDisplay>
			<!--- 2012/09/12 - RMB - P-LEN056 CR-081 --->

			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="view" label="">
		</cf_relayFormDisplay>
	</form>

<cfelseif mode eq "view">

	<!--- <SCRIPT>
	<!---Make sure either the name of a company or the name of a person is entered when placing an order--->
			function checkordering(){

			var form = window.document.form1
			var companyfield = form.frmCompany.value
			var namefield = form.frmName.value


			if (companyfield == '' && namefield == '') {
				alert("You must enter either a Company or a Person's Name")

			}
			else form.submit()
			}
		</SCRIPT>

	<!---Search screen--->
	<CFIF IsDefined("frmSearch") and frmSearch eq "yes" >
		<TABLE align="center">
			<TR>
				<TD colspan="3" align="center"><H2>Manage a Funds Account</H2></TD>
			</TR>

			<FORM NAME="form1" METHOD="post">
			<TR>
				<TD align="right">Company Name:</TD>
				<TD colspan="2"><INPUT TYPE="text" NAME="frmCompany"></TD>
			</TR>

			<TR>
				<TD align="right">Person Name:</TD>
				<TD colspan="2"><INPUT TYPE="text" NAME="frmName"><BR></TD>
			</TR>

			<TR>
				<TD colspan="3">
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="First" <CFIF frmType IS 'First'>Checked</CFIF>> First name
				&nbsp; &nbsp;
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="Last" <CFIF frmType IS 'Last'>Checked</CFIF>> Last name
				&nbsp; &nbsp;
				<INPUT TYPE="RADIO" NAME="frmType" VALUE="Full" <CFIF frmType IS 'Full'>Checked</CFIF>> Full name
				</TD>
			</TR>

			<TR>
				<TD colspan="3" align="center"><A HREF="javascript:checkordering()"><CFOUTPUT><IMG SRC="/IMAGES/BUTTONS/c_search_e.gif" BORDER=0 ALT="Search"></CFOUTPUT></A></TD>
			</TR>

			</FORM>

	</TABLE>
	</cfif>


	<!--- save the content for the xml to define the editor --->
	<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="fundAccount" title="Add Funds">
			<field name="AccountID" label="Account ID" description="This records unique ID." control="hidden"></field>
			<field name="Amount" label="Account ID" description="This records unique ID."></field>
		</editor>
	</editors>
	</cfoutput>
	</cfsavecontent>

		<CFPARAM NAME="screenTitle" DEFAULT="">
		<!--- check to see if current user is an approver --->
		<cfset isApprover = application.com.relayFundManager.checkIfUserIsApprover()>


	<cfif isDefined("add") and add eq "yes" and isDefined("FORM.creditAmount")>
		<cfscript>
		message = application.com.relayFundManager.AddFunds(FORM.accountID,FORM.creditAmount);
		</cfscript>
	</cfif>
	<cfif isDefined("editor") and editor eq "yes">
	<form action="" method="post">
		Amount to add <input type="text" name="creditAmount"><br>
		<input type="submit">
		<input type="hidden" name="add" value="yes">
		<cfoutput><input type="hidden" name="accountID" value="#URL.AccountID#"></cfoutput>
	</form>

	<CFELSE>
	 --->
	<!---The SECOND PHASE, get all the people or companies corresponding to the search criteria
	and list them or, if a straight match, go directly to the points management screen--->

	<cfparam name="sortOrder" type="string" default="account">


	<cfset viewFundsSecurityLevel = "fundTask:Level4">

	<cfif NOT application.com.login.checkInternalPermissions(listfirst(viewFundsSecurityLevel,":"),listlast(viewFundsSecurityLevel,":"))>
		<cfscript>
		myObject = createObject("component","relay.com.commonQueries");
		myObject.dataSource = application.siteDataSource;
		myObject.personid = request.relayCurrentUser.personID;
		myObject.userGroupID = request.relayCurrentUser.userGroupID;
		myObject.frmAccountMngrID = request.relaycurrentuser.personID;
		myObject.getUserAccounts();
		</cfscript>
	</cfif>

	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		select * from (
		SELECT o.organisationName as account, o.organisationID,
		c.countryid, f.accountID, --currentFunds as current_funds, spentFunds as spent_funds,
		'<span class="editLink">phr_Edit</span>' as Edit,	c.CountryDescription AS Country,
		bpa.amount as budget_Allocated,b.description as budget,bp.description as budget_period,
		bh.description as budget_Holder
		FROM organisation o
			INNER JOIN fundCompanyAccount f ON o.organisationID = f.organisationID
			INNER JOIN Country c ON c.CountryID = o.CountryID
			inner join fundAccountBudgetAccess faba on f.accountID = faba.accountID and f.budgetID = faba.budgetID
			inner join budget b on faba.budgetID = b.budgetID --and entityTypeID=2
			inner join BudgetGroup bh on bh.BudgetGroupID = b.BudgetGroupID
			left outer join
				(budgetPeriodAllocation bpa inner join budgetPeriod bp on bpa.budgetPeriodID = bp.budgetPeriodID --and (datediff(d,bp.startDate,getDate()) >=0 and dateDiff(d,bp.endDate,getDate()) <=0)
				)
			on bpa.budgetID = b.budgetID
		) base
		WHERE 1=1
		<!--- 2012-07-20 PPB P-SMA001		 and countryid in (#request.RelayCurrentUser.countryList#) --->
		#application.com.rights.getRightsFilterWhereClause(entityType="FundCompanyAccount",alias="base").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->

		<CFIF isDefined("frmCompany") and frmCompany IS NOT ''>
			AND o.organisationName  LIKE  <cf_queryparam value="#frmCompany#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFIF>

		<cfif NOT application.com.login.checkInternalPermissions(listfirst(viewFundsSecurityLevel,":"),listlast(viewFundsSecurityLevel,":"))>
			<cfif myObject.qUserAccounts.recordcount gt 0>
				AND organisationID  IN ( <cf_queryparam value="#valueList(myObject.qUserAccounts.organisationId)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfelse>
				AND 1=0
			</cfif>
		</cfif>

		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>

	<!--- create a fund account for FNL if none exist --->
	<!--- NJH 2007/11/08 - commented this out as a fund account now needs a budget and it will require a bit more work which there is no
		time for at the moment.  --->
	<CFIF GetDetails.RecordCount EQ 0>
		There are currently no fund accounts. To set up a fund account, go to the 3-pane view. Select the organization to create the account for and create the account by selecting 'Set Up Fund Account' from the organization function dropdown.
		<!--- <cfscript>
		temp = application.com.relayFundManager.createFundAccount("1");
		</cfscript>
		<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
			SELECT DISTINCT o.organisationName as account, o.organisationID,
			c.countryid, f.accountID, --'View' as statement,
					c.CountryDescription AS Country
			FROM organisation o
				INNER JOIN fundCompanyAccount f ON o.organisationID = f.organisationID
				INNER JOIN Country c ON c.CountryID = o.CountryID
			WHERE 1=1
		</CFQUERY> --->
	</cfif>

	<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 0>

		<!--- START: 2013-03-06 NYB Case 432006 added: --->
		<cfset addString="">
		<cfloop list="filterselect1,filterselect2,filterselectvalues1,filterselectvalues2" index="i">
			<cfif structkeyexists(form,"#i#")>
				<cfset addString=addString&"&#i#="&urlencodedformat(evaluate("form.#i#"))>
			</cfif>
		</cfloop>
		<!--- END: 2013-03-06 NYB Case 432006  --->

		<!--- <cfinclude template="fundRequestFunctions.cfm"> --->
		<CF_tableFromQueryObject queryObject="#getDetails#"
			keyColumnList="account,edit"
			<!--- START:  NYB 2009-09-14 LHID2620 - replaced: ---
			keyColumnURLList="/data/dataFrame.cfm?frmsitename=,fundAccounts.cfm?mode=edit&accountID="
			!--- WITH:  NYB 2009-09-14 LHID2620 --->
			<!--- 2013-03-06 NYB Case 432006 added "addString": --->
			keyColumnURLList="javascript:void(openNewTab('Search Results'*comma'Search Results'*comma'/data/dataframe.cfm?frmSrchOrgID=thisURLKey'*comma{reuseTab:true}));,fundAccounts.cfm?mode=edit#addString#&accountID="
			<!--- END:  NYB 2009-09-14 LHID2620 --->
			keyColumnKeyList="organisationID,accountID"

			columnTranslation="true"
			columnTranslationPrefix="phr_fund_"

			FilterSelectFieldList = "Country"
			FilterSelectFieldList2 = "Budget_Period"
			showTheseColumns="Account,AccountID,Country,Budget_Holder,Budget,Budget_Period,Budget_Allocated,Edit"
			hideTheseColumns="countryid,organisationID"

			currencyFormat="budget_Allocated"
			<!--- functionListQuery="#comTableFunction.qFunctionList#" --->
			sortOrder="#sortOrder#">

	</CFIF>
</cfif>