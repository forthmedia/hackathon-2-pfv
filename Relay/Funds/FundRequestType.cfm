<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			FundRequestType.cfm
Author:				
Date started:		
	
Description:		List and edit Fund Request Types.	

Amendment History:

Date 		Initials 	What was changed
2011/05/20	PPB			LID6629 introduced table numberlist for sortorder for sql2000 compatibility
2014-10-15	RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:

 --->


<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="sortOrder">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<cfset SortOrderList="select 1 as value,1 as display">
<cfloop index='i' from='2' to='99'>
	<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
</cfloop>

<!--- save the content for the xml to define the editor --->

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="87" name="thisEditor" entity="FundRequestType" title="phr_fund_RequestType">
			<field name="FundRequestTypeID" label="phr_fund_RequestTypeID" description="This records unique ID." control="html"></field>
			<field name="FundRequestTypeTextID" label="phr_fund_RequestTypeTextID" description=""></field>
			<field name="" CONTROL="TRANSLATION" label="phr_fund_RequestType" Parameters="phraseTextID=title" required="true"></field>
			<field name="startdate" CONTROL="Date" label="phr_fund_RequestTypeStartDate" description=""></field>
			<field name="enddate" CONTROL="Date" label="phr_fund_RequestTypeEndDate" description=""></field>
			<field name="active" CONTROL="YorN" label="phr_fund_RequestTypeActive" description=""></field>
			<field name="sortorder" CONTROL="select" label="phr_fund_RequestTypeSortorder" description="" query="SELECT numberValue as display, numberValue as value FROM numberList"></field>
			<field name="created" label="phr_FundRequestType_Created"  description=""></field>
			<field name="createdby" label="phr_FundRequestType_CreatedBy"  description=""></field>
			<field name="lastupdated" label="phr_FundRequestType_updated"  description=""></field>
			<field name="lastupdatedby" label="phr_FundRequestType_updatedby"  description=""></field>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(URL,"editor")>

	<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
	<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_FundRequestTypes")>

	<cfset getData = application.com.FundRequestType.GetFundRequestType(sortOrder=sortOrder)>
<cfelse>
	<cfset application.com.request.setTopHead(topHeadCfm="")>
</cfif>
	
<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="FundRequestType"
	keyColumnList="FundRequestTypetextID"
	keyColumnKeyList="FundRequestTypeID"
	showTheseColumns="FundRequestTypeID,FundRequestType,FundRequestTypeTextID,active"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	columnTranslationPrefix="phr_fund_"
	columnTranslation="true"
	booleanFormat="active"
>