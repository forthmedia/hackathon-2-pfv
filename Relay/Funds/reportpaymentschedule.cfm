<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportPaymentSchedule.cfm	
Author:				NJH
Date started:		2007/09/13
Description:		MDF payment schedule report	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup


Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="Organisation_Name,Date_Of_Payment">

<cfset tableName="fundRequestActivity">
<cfset dateField="created">
<cfset dateRangeFieldName="created">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm">

<cfset MDFDocuments=application.com.settings.getSetting("fundManager.documentTypes")>

<cfquery name="PaymentScheduleReport" datasource="#application.siteDataSource#">
	select * from (
		select bh.description as product_Category, fra.fundrequestactivityID as request_No, o.organisationName as Organisation_Name, p.firstname+' '+p.lastname as Fund_Manager,
			fraa.created as approval_Date, frt.fundRequestType as activity_type, fra.endDate as end_date, 
			frap.amount as amount_Paid, 
				<cfloop list="#MDFDocuments#" index="mdfDocument">
				case
					when docStatus  like  <cf_queryparam value="%#mdfDocument#=Required%" CFSQLTYPE="CF_SQL_INTEGER" >  then 'No' 
					when docStatus  like  <cf_queryparam value="%#mdfDocument#=Open Query%" CFSQLTYPE="CF_SQL_INTEGER" >  then 'No' 
					when docStatus  like  <cf_queryparam value="%#mdfDocument#=Received%" CFSQLTYPE="CF_SQL_INTEGER" >  then 'Yes'
					when docStatus is null then ''
					else 'N/A' 
				end as #mdfDocument#_Approved,
				</cfloop>
			frap.PaymentDate as date_Of_Payment, frap.PaymentRef as payment_ref, fra.created,
			case when b.entityTypeID = 2 then 'Organisation' when entityTypeID=3 then 'Country' when entityTypeID=4 then 'Region' end as budget_Type,
			c.countryDescription as country
		from fundRequestActivity fra inner join fundRequest fr
			on 	 fra.fundRequestID = fr.fundRequestID inner join fundCompanyAccount fca
			on 	 fca.accountID = fr.accountID inner join organisation o
			on 	 o.organisationID = fca.organisationID inner join country c
			on 	 c.countryID = o.countryID inner join FundRequestType frt
			on 	 frt.fundRequestTypeID = fra.fundRequestTypeID inner join fundRequestActivityApproval fraa
			on 	 fraa.fundRequestActivityID = fra.fundRequestActivityID inner join budget b
			on 	 fca.budgetID = b.budgetID inner join BudgetGroup bh
			on 	 b.BudgetGroupID = bh.BudgetGroupID inner join integerMultipleFlagData imfd
			on	 fca.accountID = imfd.data inner join flag f
			on   f.flagID = imfd.flagID and f.flagTextID = 'ExtFundManager' inner join person p
			on	 p.personID = imfd.entityID left outer join fundRequestActivityPayment frap
			on 	 fra.fundRequestActivityID = frap.fundRequestActivityID
		where fra.fundApprovalStatusID in
			(select fundApprovalStatusID from fundApprovalStatus where statusGroup in ('paid'))
	) base
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>


<cf_title>Payment Schedule Report</cf_title>

<cfset currencyCols = "amount_Paid">
<cfset dateCols = "end_date,approval_Date,date_Of_Payment">
<cfset filter1cols = "Product_Category,Budget_Type,Country">

<cfset tempDocList = "">
<cfloop list="#MDFDocuments#" index="mdfDocument">
	<cfset tempDocList = listAppend(tempDocList,"#mdfDocument#_Approved")>
</cfloop>

<CF_tableFromQueryObject queryObject="#PaymentScheduleReport#" 
	showTheseColumns="Product_Category,Request_No,Organisation_Name,Fund_Manager,Approval_Date,Activity_Type,End_Date,Amount_Paid,#tempDocList#,Date_Of_Payment,Payment_Ref"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	totalTheseColumns="#currencyCols#"
	FilterSelectFieldList="#filter1cols#"
	dateFormat ="#dateCols#"
	queryWhereClauseStructure="#queryWhereClause#" 
	passThroughVariablesStructure="#passThruVars#"
>