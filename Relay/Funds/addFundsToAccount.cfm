<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name: addFundsToAccount.cfm

2012-11-15 PPB Case 431990 createFundAccount function now returns a structure 
 --->

<cf_title>phr_sys_fund_CreateAccount</cf_title>

<cfscript>
	newFundAccountID = application.com.relayFundManager.createFundAccount(organisationID = URL.orgID).accountId;		/* 2012-11-15 PPB Case 431990 createFundAccount function now returns a structure */
</cfscript>

<cfif request.relayCurrentUser.isInternal>  <!--- WAB 2006-01-25 Remove form.userType     CompareNoCase(usertype,'internal') eq 0--->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">phr_sys_fund_CreateAccount</TD>
	</TR>
</TABLE>
</cfif>