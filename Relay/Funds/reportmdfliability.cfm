<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportPaymentSchedule.cfm	
Author:				NJH
Date started:		2007/09/13
Description:		A report of people allocated budgets but that haven't submitted an activity for that budget	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="organisation_Name">

<cfquery name="GetBudgetLiabilities" datasource="#application.siteDataSource#">
	select * from vMDFBudgetLiability
	where 1=1
 	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>


<cf_title>MDF Liability Report</cf_title>

<cfset currencyCols = "budget_Allocated">
<cfset dateCols = "">
<cfset filter1cols = "Product_Category,Budget_Period">

<CF_tableFromQueryObject queryObject="#GetBudgetLiabilities#" 
	showTheseColumns="Product_Category,Budget_Period,Budget_Allocated,Organisation_Name,Fund_Manager,Email"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	FilterSelectFieldList="#filter1cols#"
	dateFormat ="#dateCols#"
>