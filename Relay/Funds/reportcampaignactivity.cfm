<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="marketing_campaign">

 <cfset tableName="vMDFCampaignActivity">
<cfset dateField="created">
<cfset dateRangeFieldName="created">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<cfquery name="campaignActivityReport" datasource="#application.siteDataSource#">
select * from vMDFCampaignActivity
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_title>Campaign Activity Report</cf_title>

<cfset currencyCols = "amt_Claimed,amt_Approved,amt_paid">
<cfset dateCols = "end_date">
<cfset filter1cols = "Country,Region,Marketing_Campaign,Product_Category">

<CF_tableFromQueryObject queryObject="#campaignActivityReport#" 
	showTheseColumns="Marketing_Campaign,Country,Region,Activity_Type,End_Date,Amt_Claimed,Amt_Approved,Amt_Paid,partner_name"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	totalTheseColumns="#currencyCols#"
	FilterSelectFieldList="#filter1cols#"
	dateFormat ="#dateCols#"
	groupByColumns="marketing_campaign"
	queryWhereClauseStructure="#queryWhereClause#" 
	passThroughVariablesStructure="#passThruVars#"
>