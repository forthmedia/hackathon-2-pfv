<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="Organisation_Name">

<cfquery name="QuarterlySummaryReport" datasource="#application.siteDataSource#">
select * from vMDFQuarterlySummary
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_title>Quarterly Summary Report</cf_title>

<cfset currencyCols = "total_Cost,amount_Requested,amount_Approved">
<cfset dateCols = "date_Requested,date_Approved,activity_End_Date">
<cfset filter1cols = "Status_Group,Product_Category">

<CF_tableFromQueryObject queryObject="#QuarterlySummaryReport#" 
	showTheseColumns="Product_Category,Organisation_Name,Country,Region,Organisation_ID,Request_No,Activity_Type,Total_Cost,Amount_Requested,Amount_Approved,Date_Requested,Date_Approved,Activity_End_Date,Current_Status,partner_name"
	sortOrder="#sortOrder#"
	selectbyday="true"
	currencyFormat="#currencyCols#"
	totalTheseColumns="#currencyCols#"
	dateFormat ="#dateCols#"
	FilterSelectFieldList="#filter1cols#"
>