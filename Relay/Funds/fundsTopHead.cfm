<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="list">
<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfparam name="pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<!--- 2011/03/09 PPB LID5836 START - this fix was done for budgets.cfm but will have an effect on fundAccounts.cfm and fundRequestActivities.cfm; subsequently limited to budgets.cfm by AJC --->
<cfif findNoCase("budgets.cfm",SCRIPT_NAME)>
	<cfif structKeyExists(form,"mode")>
		<cfset variables.mode = form.mode>
	<cfelseif structKeyExists(url,"mode")>
		<cfset variables.mode = url.mode>
	</cfif>
</cfif>
<!--- 2011/03/09 PPB LID5836 END --->

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="#attributes.thisDir#">
	<!--- 2011/04/13 PPB START --->
	<CFIF findNoCase("budgetGroupPeriodAllocations.cfm",SCRIPT_NAME)>
		<cfif isDefined("mode") and (mode eq "add" or mode eq "edit")>
			<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="/funds/budgetGroupPeriodAllocations.cfm?mode=list">
 		<cfelse>
			<!--- 2011/04/21 PPB LID6392 must be fundAdminTask:level3 to see Add button --->
			<cfif application.com.login.checkInternalPermissions("fundAdminTask","Level3") or application.com.login.checkInternalPermissions("fundTask","Level3")>
				<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="budgetGroupPeriodAllocations.cfm?mode=add">
			</cfif>
		</cfif>
	</CFIF>
	<!--- 2011/04/13 PPB END --->
	<CFIF findNoCase("budgets.cfm",SCRIPT_NAME)>
		<cfif structKeyExists(variables,"mode") and (variables.mode eq "add" or variables.mode eq "edit")>
			<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="/funds/budgets.cfm?mode=view">
 		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="budgets.cfm?mode=add">
		</cfif>
	</CFIF>
	<CFIF findNoCase("fundAccounts.cfm",SCRIPT_NAME)>
		<cfif structKeyExists(variables,"mode") and (variables.mode eq "add" or variables.mode eq "edit")>
			<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="/funds/fundAccounts.cfm?mode=view">
		</cfif>
	</CFIF>
	<!--- 2010/02/25 			SSS 		P-LEX039 MDF you cannot add mdf request for this screen because this now works in 3 pain view --->
	<!---CFIF findNoCase("fundRequests.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="fundRequestActivities.cfm?mode=add">
	</CFIF--->
	
	<!--- 2010/02/24 			NAS 		P-LEX039 MDF Phase 1 --->
	<CFIF findNoCase("entityFrameScreens.cfm",SCRIPT_NAME) or findNoCase("entityFrameScreens.cfm",request.query_string)
			or findNoCase("entityFrameScreens.cfm",SCRIPT_NAME) or findNoCase("entityFrameScreens.cfm",request.query_string)>
    	<CFIF isDefined("frmEntityType") and frmEntityType neq "" and isDefined("frmcurrentEntityID") and isNumeric(frmcurrentEntityID)>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="../funds/fundRequestActivities.cfm?mode=add&mdfpartnerorganisationid=#frmorganisationid#">
		</cfif>
	</CFIF>


 	<CFIF findNoCase("fundRequestActivities.cfm",SCRIPT_NAME)>
		<cfif structKeyExists(variables,"mode") and (variables.mode eq "add" or variables.mode eq "edit")>
			<cfif isDefined("fundRequestID") and fundRequestID neq 0>
				<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="../funds/fundRequestActivities.cfm?mode=add&mdfpartnerorganisationid=#frmorganisationid#">
				<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="/funds/fundRequestActivities.cfm?mode=view&fundRequestID=#fundRequestID#">
			<cfelse>
				<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="../funds/fundRequestActivities.cfm?mode=add&mdfpartnerorganisationid=#frmorganisationid#">
				<CF_RelayNavMenuItem MenuItemText="Cancel" CFTemplate="/funds/fundRequests.cfm?mode=view">
			</cfif>
		</cfif>
		<cfif structKeyExists(variables,"mode") and variables.mode eq "view">
			<!--- NJH 2008/01/28 MDF phase 3. Fund Request activies are opened up in a new window on the internal system, so don't need a back button. Only show it on the external site. --->
			<cfif not request.relayCurrentUser.isInternal>
			<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="/funds/fundRequests.cfm?mode=view">
			</cfif>
			<!---CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="fundRequestActivities.cfm?mode=add&fundRequestID=#fundRequestID#"--->
		</cfif>
	</CFIF>
	<CFIF findNoCase("fundTransactions.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:history.go(-#attributes.pagesBack#);">
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="fundTransactions.cfm?editor=yes&add=yes">
	</CFIF>

	<!--- Reports --->
	<CFIF findNoCase("ReportPaymentSchedule.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportBudgetUsage.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportCampaignActivity.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportClaimsByCountry.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportMDFLiability.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportQuarterlySummary.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	<CFIF findNoCase("ReportStatus.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
	
	<CFIF findNoCase("fundApprovalLevels.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="fundApprovalLevels.cfm?editor=yes&add=yes">
	</CFIF>
	<!--- START: 2011/03/16 AJC P-LEN023 - link to add a new fund request type --->
	<CFIF findNoCase("fundRequestType.cfm",SCRIPT_NAME) and not structKeyExists(URL,"editor")>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="FundRequestType.cfm?editor=yes&add=yes">
	</CFIF>
	<!--- END: 2011/03/16 AJC P-LEN023 - link to add a new fund request type --->	

	<!--- START: 2011/04/07	PPB	LID6190 - link to add a new Approval Status --->
	<CFIF findNoCase("fundApprovalStatus.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="fundApprovalStatus.cfm?editor=yes&add=yes">
	</CFIF>
	<!--- END:   2011/04/07 PPB	LID6190 --->
	
	<CFIF findNoCase("reportFundApprovers.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add New Approver" CFTemplate="reportFundApprovers.cfm?editor=yes&add=yes">
	</CFIF>

</CF_RelayNavMenu>



