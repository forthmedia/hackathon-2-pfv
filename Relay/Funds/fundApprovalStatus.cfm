<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			FundApprovalStatus.cfm
Author:				PPB
Date started:		2011-03-28
	
Description:		List and edit Fund Approval Statuses.	

Amendment History:

Date 		Initials 	What was changed
2011/03/29	PPB			add fundStatusMethod
2011/04/01 	PPB 		LID6101 maintain isApproved and isFullyApproved
2011/05/20	PPB			LID6629 introduced table numberlist for sortorder for sql2000 compatibility
2014-10-15	RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:

 --->


<cf_title>Approval Status</cf_title>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="sortOrder">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<cfset SortOrderList="select 1 as value,1 as display">
<cfloop index='i' from='2' to='99'>
	<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
</cfloop>

<!--- save the content for the xml to define the editor --->

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="87" name="thisEditor" entity="FundApprovalStatus" title="phr_fund_ApprovalStatus">
			<field name="FundApprovalStatusID" label="phr_fund_ApprovalStatusID" description="This records unique ID." control="html"></field>
			<field name="FundStatusTextID" label="phr_fund_StatusTextID" description=""></field>
			<field name="" CONTROL="TRANSLATION" label="phr_fund_ApprovalStatus" Parameters="phraseTextID=Title" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_fund_ExternalStatusText" Parameters="phraseTextID=ExternalStatusText" required="false"></field>
			<field name="NextAvailableStatus" label="phr_fund_NextAvailableStatus" description="Statuses that are relevant in the status dropdown while an activity is at this status"></field>
			<field name="canEditActivity" CONTROL="YorN" label="phr_fund_CanEditActivity" description=""></field>
			<field name="StatusGroup" label="phr_fund_StatusGroup" description=""></field>
			<field name="LevelAvailabilty" label="phr_fund_LevelAvailabilty" description=""></field>
			<field name="emailTextID1" label="phr_fund_EmailTextID1" description="Email definition id of the email that is sent when this status is reached"></field>
			<field name="emailTextID2" label="phr_fund_EmailTextID2" description="Additional email that is sent when this status is reached"></field>
			<field name="fundStatusMethod" label="phr_fund_StatusMethod" description="The cfc method that is fired when this status is reached"></field>
			<field name="isApproved" CONTROL="YorN" label="phr_fund_IsApproved" description="This indicates that this Status is at Approval Level 1 or above (typically all except Pending and Rejected)"></field>
			<field name="isFullyApproved" CONTROL="YorN" label="phr_fund_IsFullyApproved" description="This indicates that this Status is at Fully Approved or above"></field>
			<field name="active" CONTROL="YorN" label="phr_fund_ApprovalStatusActive" description=""></field>
			<field name="sortorder" CONTROL="select" label="phr_fund_ApprovalStatusSortorder" description="" query="SELECT numberValue as display, numberValue as value FROM numberList"></field>

			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(URL,"editor")>
	<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
	<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_FundApprovalStatus")>
	<cfset getData = application.com.FundApprovalStatus.GetFundApprovalStatus(sortOrder=sortOrder)>
<cfelse>
	<cfset application.com.request.setTophead(topHeadCfm="")>
</cfif>
	
<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="FundApprovalStatus"
	keyColumnList="FundStatusTextID"
	keyColumnKeyList="FundApprovalStatusID"
	showTheseColumns="FundApprovalStatusID,FundApprovalStatus,FundStatusTextID,active"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	booleanFormat="active"
>