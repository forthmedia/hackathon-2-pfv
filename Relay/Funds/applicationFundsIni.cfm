<!--- �Relayware. All Rights Reserved 2014 --->
<!--- fundParams sets up some default values for varaiables 
		which can be overridden either by fundINI below or at run time --->

<!--- 
WAB 2010-11-01 fundParams.cfm has been removed and replaced with settings.
Hence fundsINI is no longer of any use
<CFINCLUDE TEMPLATE="fundParams.cfm">

<cf_include template="\code\cftemplates\fundINI.cfm" checkIfExists="true">

--->

<!--- <cfif fileexists("#application.paths.code#\cftemplates\fundINI.cfm")>
	<!--- fundINI can be used to over-ride default 
		global application variables and params defined in fundParams.cfm --->
	<cfinclude template="/code/cftemplates/fundINI.cfm">
</cfif> --->

<!--- determine whether user running application is an approver or manager --->
<cfset isApprover = application.com.relayFundManager.checkIfUserIsApprover()>
<cfset isManager = application.com.relayFundManager.checkIfUserIsManager()>


