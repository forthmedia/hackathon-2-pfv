<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			funRequestFunctions.cfm
Author:				SWJ
Date started:		2004-08-25
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfscript>
comTableFunction = application.com.tableFunction;

comTableFunction.functionName = "Phr_Sys_FundRequestFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_FundRequests";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_ApproveFundRequests";
comTableFunction.securityLevel = "";
comTableFunction.url = "/funds/approveRequests.cfm?frmtask=save&frmruninpopup=true&request_ID=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();
</cfscript>

