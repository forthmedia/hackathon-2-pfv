<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			authoriseBudgetAllocations.cfm	
Author:				NJH
Date started:		2007/09/20
	
Description:		Authorises a budget allocation. If a budget allocation has been authorised, an email is sent to the fund managers
				that have accounts tied to that budget informing them that the budget is authorised and available to them.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
09-jan-2008			SSS			Added extra merge fields to the authorization Email and added extra fields 
								to the getBudgetAllocationsToBeAuthorised query.
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2011-01-18 			PPB			REL099 use AuthoriseBudgetTypes from settings
2011/01/26 			PPB			LID3670
2011-02-15 			NYB			LHID5662 NB. fix clashed significantly with 8.3.1 code so will be applied seperately afterwards (PPB 2011/05/16)
2011/03/09			NAS			LID5790: Fund Manager - Error occurs when authorising a budget
2011/03/21			PPB			LID5790: removed debug
2011/04/18			PPB			LEX052: added check against insufficient BudgetGroupPeriod Funds (and revamped messaging while at it)
2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari)
2014-10-15			RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:


 --->
<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
<cfsetting enablecfoutputonly="true">

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfset application.com.request.setTopHead(pageTitle="phr_fund_AuthorizeBudgetAllocations")>

<cfoutput>
<div id="actionBar">
	<ul>
		<li><a href="javascript:closeWin();">phr_CloseWindow</a></li>
	</ul>
</div>

<cf_title>phr_fund_AuthorizeBudgetAllocations</cf_title>

<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">

<cfparam name="budgetPeriodAllocationID" type="string" default="">  <!--- list of budget IDs to be authorised --->
<!--- PPB 2011-01-18 REL099 now get the entityTypes from settings; original defaulted to entityTypeId=2 and didn't seem to be sent in as anything else (hence country budgets couldn't be approved) --->
<!--- 
<cfparam name="entityTypeID" type="string" default="2"> <!--- list of budget (entity) types that need authorising --->
 --->
<cfset entityTypeID = application.com.settings.getSetting("fundManager.AuthoriseBudgetTypes")>	<!--- comma delimited list of budget (entity) types that can be authorised --->

<cfif len(budgetPeriodAllocationID)>

	<cfquery name="qryBudgetAllocationsToBeAuthorised" datasource="#application.siteDataSource#">
		select bpa.budgetPeriodAllocationID, b.description + '(' + bp.description + ')' as budgetDescription, b.budgetID,bpa.amount as ApprovedAmount,
		bp.description as BudgetQuarter, bp.startdate as BudgetStartDate, bp.enddate as BudgetEndDate, bh.description as BudgetGroup,b.BudgetGroupID,bpa.budgetPeriodID
		from budgetPeriodAllocation bpa inner join
	        dbo.BudgetPeriod bp on bpa.budgetPeriodID = bp.budgetPeriodID inner join
	        dbo.Budget b on b.budgetID = bpa.budgetID inner join
			dbo.BudgetGroup bh on b.BudgetGroupID = bh.BudgetGroupID
		where budgetPeriodAllocationID  in ( <cf_queryparam value="#budgetPeriodAllocationID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			and b.entityTypeID  in ( <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and isNull(authorised,0) <> 1
	</cfquery>

	<cfif qryBudgetAllocationsToBeAuthorised.recordCount gt 0>

			<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
			<tr><td>phr_fund_budgetAllocationAuthorisationResults</td></tr>
			<tr><td>&nbsp;</td></tr>
			
			<cfloop query="qryBudgetAllocationsToBeAuthorised">

				<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
				<tr><td>#htmleditformat(budgetDescription)# (#htmleditformat(budgetID)#)</td>
	
				<cfset qryBudgetGroupPeriodAllocations = application.com.relayFundManager.getBudgetGroupPeriodAllocations(budgetGroupID=qryBudgetAllocationsToBeAuthorised.budgetGroupID, budgetPeriodID=qryBudgetAllocationsToBeAuthorised.budgetPeriodID)>
				<tr><td>
				<cfif qryBudgetAllocationsToBeAuthorised.ApprovedAmount gt qryBudgetGroupPeriodAllocations.amountRemaining>
					: phr_fund_AmountIsGreaterThanTheAvailableAmountForTheBudgetGroupPeriod
				<cfelse>	
		
					<cfquery name="authoriseBudgetAllocations" datasource="#application.siteDataSource#">
						update budgetPeriodAllocation set authorised = 1 
						where budgetPeriodAllocationID =  <cf_queryparam value="#qryBudgetAllocationsToBeAuthorised.budgetPeriodAllocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>

					: phr_fund_budgetPeriodAllocation_Authorised
				
					<!--- email the fund managers saying that the budget allocation has been authorised --->
					<!--- NYB 2011-02-15 LHID5662 added creation of struct here *1:--->	
					<!--- NYB 2011-02-15 LHID5662 added if around sending of email --->
					<cfif not application.com.settings.getSetting("fundManager.excludeEmailsToFundManager")>
						<cfset variables.budgetStruct= structNew()>
						<cfloop query="qryBudgetAllocationsToBeAuthorised">
							<cfset fundManagerQry = application.com.relayFundManager.getFundAccountManagers(budgetID=budgetID)>
							<cfif fundManagerQry.recordcount gt 0>	<!--- PPB 2011/01/26 LID3670 --->
								<!--- NYB 2011-02-15 LHID5662 START various changes:--->	
								<cfset variables.budgetStruct.BudgetQuarter = BudgetQuarter>
								<cfset variables.budgetStruct.BudgetStartDate = dateformat(BudgetStartDate, "dd-mmm-yyyy")>
								<cfset variables.budgetStruct.BudgetEndDate = dateformat(BudgetEndDate, "dd-mmm-yyyy")>
								<cfset variables.budgetStruct.BudgetGroup = BudgetGroup>
								<cfset variables.budgetStruct.IssueDate	  = dateformat(now(), "dd-mmm-yyyy")>
								<cfset variables.budgetStruct.ApprovedAmount = ApprovedAmount>
								<cfloop query="fundManagerQry">
									<cfset variables.mystruct = StructCopy(budgetStruct)>
									<cfset variables.mystruct.telephone = officephone>
									<cfset emailResult = application.com.email.sendEmail(emailTextID="MDFBudgetAuthorised",personID=personID,cfmailtype="html", mergeStruct = variables.mystruct)>
									<cfset StructDelete(variables,"mystruct")>
								</cfloop>
								<!--- NYB 2011-02-15 LHID5662 END--->	
							<cfelse>	
								: phr_fund_fundManagerNotSetUpForBudget - phr_fund_EmailNotSent
							</cfif>	
						</cfloop>
						<!--- NYB 2011-02-15 LHID5662 *1 deleted here:--->	
						<cfset StructDelete(variables,"budgetStruct")>
					</cfif>
					
				</cfif>
				
				<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
				</td></tr>
			</cfloop>

		<cfelse>
			<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
			<tr><td>phr_fund_NoBudgetsWereAuthorised</td></tr>
		</cfif>
		
	<script>
		function closeWin() {
			<cfif qryBudgetAllocationsToBeAuthorised.recordCount gt 0>
				//window.opener.location.reload();
				//PPB 2011/01/26 LID3670 this solves the problem of the budgets.cfm page being reloaded in add mode and creating duplicate row (if the user clicks reload)
				window.opener.location.href=window.opener.location.href.replace('mode=add','mode=view');		//ideally would use a RE instead of hardcoding 'add'
			</cfif>
			window.setTimeout('window.close()',10);
		}
	</script>

</cfif>

<!--- 2014-10-13			RPW			CORE-782 The authorise budget allocations pop-up window errors (Firefox & Safari) --->
</table>

</cfoutput>