<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			createFundAccount.cfm
Author:				???
Date started:		??/??/????
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2012-11-15 			PPB 		Case 431990 don't return a message to the user stating Account(s) Created when it already existed

Possible enhancements:
re-jig so allAccountsCreated is not assumed true and message doesn't default to phr_fund_accountsCreated (wait till we know it has been created or already existed etc)

 --->

<cfparam name="orgIDList" type="string" default="">

<cfset request.relayFormDisplayStyle ="HTML-table">

<cfif request.relayCurrentUser.isInternal>
	<cfif orgIDList eq "">
		You must pass in the organisation IDs to create accounts for.
		<CF_ABORT>
	<cfelse>
		<cfquery name="getOrgTypes">
			select 1 from organisation o
				inner join organisationType ot on o.organisationTypeID = ot.organisationTypeID
			where organisationID in ( <cf_queryparam value="#orgIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				and ot.typeTextID in ('EndCustomer','AdminCompany')
		</cfquery>

		<cfif getOrgTypes.recordCount>
			<cfset application.com.relayUI.setMessage(message="Fund Accounts cannot be created for End Customer or Admin organizations",type="info")>
			<cf_abort>
		</cfif>
	</cfif>

	<!--- Creating a fund account --->
	<cfif structKeyExists(form,"frmCreateAccount")>
		<cfset message = "phr_fund_accountCreated">
		<cfset messageType="success">

		<cfif listlen(orgIDList) gt 1>
			<cfset message = "phr_fund_accountsCreated">
		</cfif>
		<cfset allAccountsCreated = true>

				<cfset BudgetGroupIDList="">
				<cfloop list="#frmBudgetGroupIDList#" index="BudgetGroupID">
					<cfif isDefined("frmBudgetGroupID#BudgetGroupID#")>
						<cfset BudgetGroupIDList = listAppend(BudgetGroupIDList,BudgetGroupID)>
					</cfif>
				</cfloop>

				<cfloop list="#BudgetGroupIDList#" index="BudgetGroupID">
					<cfloop list="#orgIDList#" index="orgID">
						<!--- if a budget hasn't been defined, get the budget --->
						<cfscript>
							if (not isDefined("frmBudgetID")) {
								budgetQry = application.com.relayFundManager.getBudgets(budgetTypeID=frmBudgetTypeID,BudgetGroupID=BudgetGroupID, organisationID = orgID);
								if (budgetQry.recordCount gt 0) {
									frmBudgetID = budgetQry.budgetID;
								}
							}
							if (isDefined("frmBudgetID")) {
								// 2012-11-15 PPB Case 431990 START
								createFundAccountResult = application.com.relayFundManager.createFundAccount(organisationID = orgID,budgetID=frmBudgetID);
								newFundAccountID = createFundAccountResult.accountId;
								if (createFundAccountResult.accountAlreadyExisted) {
									message = "phr_fund_AccountAlreadyExisted";			//I think this template is now only used for a single org at a time so returning this message shouldn't be confusing
									messageType="info";
								}
								// 2012-11-15 PPB Case 431990 END
							} else {
								allAccountsCreated = false;
							}
						</cfscript>
					</cfloop>
				</cfloop>


		<cfif not allAccountsCreated>
			<cfif listlen(orgIDList) eq 1>
				<cfset message  = "phr_fund_AccountNotCreated. phr_fund_pleaseEnsureBudgetExists.">
			<cfelse>
				<cfset message  = "phr_fund_NotAllAccountsCreated. phr_fund_pleaseEnsureBudgetExists.">
			</cfif>
			<cfset messageType="warning">
		</cfif>

		<cfset application.com.relayUI.setMessage(message=message,type=messageType)>

		<cf_relayFormDisplay>
			<!--- <cf_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="#message#"> --->
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="<a href='javascript:window.close();'>phr_closeWindow</a>" valueAlign="right">
		</cf_relayFormDisplay>
	<cfelse>
		<!--- get list of countries from organisations in the list --->
		<cfquery name="getListOfCountryIDsFromOrgs" datasource="#application.siteDataSource#">
			select distinct countryID from organisation where organisationID  in ( <cf_queryparam value="#orgIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>

		<!--- get the regions that are in common to the organisations selected --->
		<cfquery name="getListOfCoutryGroupIDsFromCountries" datasource="#application.siteDataSource#">
			select a.* from (
				select distinct countryGroupID from countryGroup where countryMemberID =  <cf_queryparam value="#getListOfCountryIDsFromOrgs.countryID[1]#" CFSQLTYPE="CF_SQL_INTEGER" > ) a
			<cfloop query="getListOfCountryIDsFromOrgs" startrow="2">
				join
				(select distinct countryGroupID from countryGroup where countryMemberID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) a#currentRow#
				on a.countryGroupID = a#currentRow#.countryGroupID
			</cfloop>
		</cfquery>

		<!--- get the region budgets available to the organisations --->
		<cfquery name="getRegionalBudgets" datasource="#application.siteDataSource#">
			select b.description as budget,b.budgetID from budget b where entityTypeID = 4 and entityID  in ( <cf_queryparam value="#valueList(getListOfCoutryGroupIDsFromCountries.countryGroupID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<!--- only allow org budgets to be created if more than one country is represented by the orgList --->
		<cfif getListOfCountryIDsFromOrgs.recordCount gt 1>

			<cfset message="phr_fund_MoreThanOneCountryIsRepresentedByTheseOrgs.">
			<!--- there are no regional budgets available... just show org --->
			<cfif getRegionalBudgets.recordCount eq 0>
				<cfscript>
					budgetTypesQry = application.com.relayFundManager.getBudgetTypes(budgetTypeIDList="2");
				</cfscript>
				<cfset message=message&"<br>phr_fund_YouCanOnlyCreateAccountsForOrgBudgets.">
			<cfelse>
				<!--- there are regional budgets as well as org budgets available --->
				<cfscript>
					budgetTypesQry = application.com.relayFundManager.getBudgetTypes(budgetTypeIDList="2,4");
				</cfscript>
				<cfset message=message&"<br>phr_fund_YouCanOnlyCreateAccountsForOrgOrRegionBudgets.">
			</cfif>
		<cfelse>
			<cfscript>
				budgetTypesQry = application.com.relayFundManager.getBudgetTypes();
			</cfscript>
		</cfif>

		<script>
			function onRadioBtnClicked(radioBtnName,radioChecked) {
				if (radioChecked.value == 'false') {
					if (radioBtnName.checked){
						radioBtnName.checked=0;
						radioChecked.value=true;
					}
				} else {
					radioChecked.value=false;
				}
			}
		</script>

		<cfif isDefined("message")>
			<cfset application.com.relayUI.setMessage(message=message,type="info")>
		</cfif>

		<cf_relayFormDisplay>
			<form name="createCompanyAccountForm"  method="post">
				<!--- <cfif isDefined("message")>
					<cf_relayFormElementDisplay relayFormElementType="message" fieldname="" currentValue="#message#" label="">
				</cfif> --->
				<cfif not isDefined("frmBudgetTypeID")>
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="phr_fund_pleaseSelectBudgetType." label="" spanCols="yes">
				</cfif>
				<cf_relayFormElementDisplay relayFormElementType="radio" fieldName="frmBudgetTypeID" currentValue="#isDefined('frmBudgetTypeID')?frmBudgetTypeID:''#" label="phr_fund_budgetType" query="#budgetTypesQry#" display="display" value="value" onClick="javascript:createCompanyAccountForm.submit();">

				<cfif isDefined("frmBudgetTypeID")>

					<cfif frmBudgetTypeID eq 4 and getRegionalBudgets.recordCount gt 0>
						<cf_relayFormElementDisplay relayFormElementType="select" fieldName="frmBudgetID" currentValue="#isDefined('frmBudgetID')?frmBudgetID:''#" label="phr_fund_regionalBudgetsAvailable" query="#getRegionalBudgets#" display="budget" value="budgetID" nullValue="" nullText="phr_fund_pleaseSelectABudget" onChange="javascript:createCompanyAccountForm.submit();">
					</cfif>

					<cfif (frmBudgetTypeID eq 4 and isDefined("frmBudgetID") and frmBudgetID neq "") or frmBudgetTypeID neq 4>
						<cfif not isDefined("frmBudgetGroupID")>
							<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="phr_fund_pleaseSelectaBudgetGroup." label="" spanCols="yes">
						</cfif>

						<cfquery name="getBudgetGroupsForRadioBox" datasource="#application.siteDataSource#">
							select distinct bh.BudgetGroupID as value, bh.description as display from BudgetGroup bh
								inner join budget b on bh.BudgetGroupID = b.BudgetGroupID and b.entityTypeID =  <cf_queryparam value="#frmBudgetTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
								<cfif isDefined("frmBudgetID") and frmBudgetID neq "">
									and budgetID =  <cf_queryparam value="#frmBudgetID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfif>
						</cfquery>
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="phr_fund_BudgetGroup.">
						<cfloop query="getBudgetGroupsForRadioBox">
							<cf_querySim>
								getBudgetGroup
								display,value
								<cfoutput>#htmleditformat(rtrim(ltrim(display)))#|#htmleditformat(value)#</cfoutput>
							</cf_querySim>
							<cf_relayFormElement relayFormElementType="radio" fieldName="frmBudgetGroupID#value#" currentValue="#isDefined('frmBudgetGroupID#value#')?evaluate('frmBudgetGroupID#value#'):''#" label="phr_fund_budgetType" query="#getBudgetGroup#" display="display" value="value" onClick="javascript:onRadioBtnClicked(frmBudgetGroupID#value#,frmBudgetGroupID#value#_radioChecked);">
							<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetGroupID#value#_radioChecked" currentValue="true" label="">
						</cfloop>
						</cf_relayFormElementDisplay>
						<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetGroupIDList" currentValue="#valueList(getBudgetGroupsForRadioBox.value)#" label="">
						<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmCreateAccount" currentValue="phr_fund_CreateAccounts" label="">
					</cfif>
				</cfif>
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="orgIDList" currentValue="#orgIDList#" label="">
			</form>
		</cf_relayFormDisplay>
	</cfif>
</cfif>