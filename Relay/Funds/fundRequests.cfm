<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			fundRequests.cfm
Author:				SWJ
Date started:		2004-08-25
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-03-2009			NJH			Bug Fix All Sites Issue 1907 - When viewing fund requests and there are no fund activities for the request, do not display link	which says 'phr_fund_NoActivities'
09/mar/2009         SSS         Bug fix Bugs - Demo Issue 1719 - if the sales out data screen Exists go there if not go to orgdetail screen page
2009-09-14			NJH			LID 2641 - changed url to use new screens code.
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2010-10-20			NYB			LHID 3844. Have rearranged, separated out the cfifs in <cfif not isDefined("editor")>, so you now have a msg when InInternal but NOT isApprover - added suitable msg
2011-01-18			PPB			REL099 use this template for displaying myMDF list too
2011-03-09			NAS			LID5841: My MDF > Fund Account - ORGANISATIONNAME is undefined
2011-03-22			PPB			LID5933: base it on getSetting("versions.myAccountsLayout")='pre-2009' my accounts cf '2009' ver
2011-03-24 			PPB 		LID6033 allow myMDF thro regardless of whether they are an approver or not
2011-03-30 			PPB			don't crash is no orgs for MyMDF
2011-09-19 			WAB 		changed doc type
2011-11-08			RMB			LID6677 right align number of requests
2012-10-04			WAB			Case 430963 Remove Excel Header
2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view
2014-11-6			DTR			442274 - MDF: Funds Available to be displayed on the portal
2015-08-07          DAN         updates to make tag editor working
2015-09-04          DAN         include numeric columns for number formatting
2015-09-16			ACPK		FIFTEEN-408 Set HidePageControls to No to allow user to change pages
24/02/2016          DAN         448236 - dont filter out fundRequests by organisationID if user is MDF approver
07-04-2016 			DCC for ESZ	case 448020 make entry in Fundrequest.cfm a translation
2016-10-24			atrunov		Case https://relayware.atlassian.net/browse/SSPS-51, Visibility of Closed Fund Accounts

Possible enhancements:


 --->

<!--- <cfparam name="phraseprefix" default="">
<CFPARAM NAME="screenTitle" DEFAULT=""> --->
<cf_param label="Sort Order" name="sortOrder" default="Country,Account,Period"/>
<cf_param label="Number of Rows Per Page" name="numRowsPerPage" default="50"/>
<cfparam name="startRow" default="1"/>
<cfparam name="processPage" type="string" default="../funds/fundRequestActivities.cfm"/>
<cf_param label="Show Message" name="showmessage" default="No" type="boolean"/>
<cfparam name="myMDF" default="false"/>										<!--- 2011-01-18 PPB REL099 --->
<cf_param label="Show requests from closed fund accounts" name="showRequestsFromClosedFundAcc" default="No" type="boolean"/>
<cfset ShowLiveAccountsOnly = not showRequestsFromClosedFundAcc>
<!--- <cfparam label="Account Manager FlagTextID" name="accManagerFlagTextID" type="string" default="AccManager"> --->	<!--- 2011-01-18 PPB REL099 --->
<cfset application.com.request.setDocumentH1(text="Fund Requests")>
<cf_translate>

<cf_includejavascriptOnce template="/javascript/viewEntity.js">

<cfset viewFundsSecurityLevel = "fundTask:Level2">

<!--- need fund task level 2 to view fund requests --->
<cfif application.com.login.checkInternalPermissions(listfirst(viewFundsSecurityLevel,":"),listlast(viewFundsSecurityLevel,":"))>
	<cfif not isDefined("editor")>
		<CFIF request.relayCurrentUser.isInternal>
			<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
			<cfparam name="openAsExcel" type="boolean" default="false">
				<cf_head>
					<cf_title>Fund Requests</cf_title>
				</cf_head>
		</cfif>

        <cfif request.relayCurrentUser.isInternal>
			<cfif isApprover or myMDF>		<!--- 2011-03-24 PPB LID6033 allow myMDF thro regardless of whether they are an approver or not (the user must be FundTask Level2 to get the myMDF menu option) --->
				<cfif myMDF>
					<!--- 2011-01-18 PPB REL099; we want to show requests for any orgs that are displayed in My Tools - My Accounts	--->

					<!--- <cfset accountManagerFlagStructure = application.com.flag.getFlagStructure(accManagerFlagTextID)> --->

					<!--- this code is lifted from /homepage/myAccounts.cfm if getSetting("versions.myAccountsLayout")="pre-2009" --->
					<cfscript>
					// create an instance of the object
					myObject = createObject("component","relay.com.commonQueries");
					myObject.dataSource = application.siteDataSource;
					myObject.personid = request.relayCurrentUser.personID;
					myObject.userGroupID = request.relayCurrentUser.userGroupID;
					myObject.frmAccountMngrID = request.relaycurrentuser.personID;
					//myObject.AccountMngrFlagTextID = AccountMngrFlagTextID;
					myObject.getUserAccounts();
					</cfscript>

				</cfif>

				<!--- 2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view --->
				<cfform name="addRequestActivityForm"  method="post" action="/funds/fundrequestactivities.cfm">
					<cf_relayFormDisplay>
						<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="add" label="">
						<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="addActivity" currentValue="phr_fund_addActivity" label="" class="submitButton">
					</cf_relayFormDisplay>
				</cfform>

				<cfquery name="vFundRequestList" datasource="#application.SiteDataSource#">
					select fr.*,'phr_Edit' as Edit,'phr_fund_ViewActivities' as Activities
					from vFundRequestList fr
						inner join fundCompanyAccount fca on fca.accountID = fr.accountID
						inner join organisation o on o.organisationID = fca.organisationID
						<cfif myMDF and not isApprover> <!--- 24/02/2016 DAN 448236 - dont filter out fundRequests by organisationID if user is MDF approver --->
<!---
 						this code is lifted from /homepage/myAccounts.cfm if getSetting("versions.myAccountsLayout")="2009" which may be used in future
							inner join
								(person cam
								inner join #accountManagerFlagStructure.FLAGTYPE.DATATABLEFULLNAME#
									camD on cam.personid=camD.data and camD.flagid in (#accountManagerFlagStructure.FLAGID#)
									and camD.data = #request.relaycurrentuser.personid#
								) on camD.entityID = o.organisationID
							where o.organisationID > 2
 --->
							<cfif myObject.qUserAccounts.recordcount gt 0>
								where o.organisationID  IN ( <cf_queryparam value="#valueList(myObject.qUserAccounts.organisationId)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
							<cfelse>
								<!--- 2011-03-30 PPB don't crash if no orgs for MyMDF --->
								where 1=0 	-- deliberately return nothing
							</cfif>
						<cfelse>
							<!--- 2012-07-20 PPB P-SMA001		where o.countryID in (#request.relayCurrentUser.countryList#) --->
							where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="FundRequest",alias="o").whereClause#	<!--- 2012-07-20 PPB P-SMA001  --->
						</cfif>
						and count_requests > 0

					<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
					order by <cf_queryObjectName value="#sortOrder#">
				</cfquery>

				<!--- SSS 2009-03-09 changed this so that if we have a sales out data screen it will go there otherwise it will go to orgdetail page
				Bug 1719 Bugs - Demo --->
				<cfset ScreenExist = application.com.screens.doesScreenExist(CurrentScreenId="salesOutData")>
				<cfif ScreenExist>
					<cfset Variables.screenloadname = "salesOutData">
				<cfelse>
					<cfset Variables.screenloadname = "OrganisationDetail">
				</cfif>

				<!--- LID 2641 NJH 2009-09-14 - used viewOrganisation function to view screen details in keyColumnUrlList. --->
				<cfif vFundRequestList.recordCount>
					<cf_tableFromQueryObject
						queryObject="#vFundRequestList#"
						queryName="vFundRequestList"
						sortOrder = "#sortOrder#"
						numRowsPerPage="#numRowsPerPage#"
						startRow="#startRow#"
						columnTranslation="true"
						columnTranslationPrefix="phr_fund_"

						showTheseColumns="BudgetGroup,Account,Country,Period,orgBudgetAllocation,count_requests,sum_Requested,sum_Approved,Activities"
					 	hideTheseColumns="countryid,Request_ID"

						keyColumnList="Account,Activities"
						keyColumnURLList="javascript:void(viewOrganisation(thisUrlKey*comma'#Variables.screenloadname#')),../funds/fundRequestActivities.cfm?fundRequestID="
						keyColumnKeyList="organisationID,Request_ID"
						keyColumnOpenInWindowList="no,yes"  <!--- NJH 2008-01-28 MDF phase 3: opened activites in new window --->

						FilterSelectFieldList="BudgetGroup,Period,Country,Account"
						FilterSelectFieldList2="BudgetGroup,Period,Country,Account"

                        <!--- 2015-09-04 DAN - include numeric columns for number formatting --->
						numberformat="count_requests,sum_Requested,sum_Approved,orgBudgetAllocation,fundsAvailable"
                        numberFormatMask="default,decimal,decimal,decimal,decimal"
						currencyFormat="sum_Requested,sum_Approved,orgBudgetAllocation"

						rowIdentityColumnName="Request_ID"
					>
				<cfelse>
					phr_fund_NoRequestsToView
				</cfif>

			<cfelse>
				phr_fund_YouMustBeAFundApproverToViewFundRequests
			</cfif>
		<cfelse><!--- ie,isExternal --->
			<cfif isManager>
				<cfset getRequestsForManager = application.com.relayFundManager.getRequestsForManager(ShowLiveAccountsOnly=ShowLiveAccountsOnly)>
				<cfif showmessage>
					<cfoutput>
						phr_fund_no_actvities_message
					</cfoutput>
				</cfif>

				<cfoutput>
				<cf_param label="Show Columns" name="showTheseColumns" default="BudgetGroup,Period,orgBudgetAllocation,Count_Requests,Sum_Requested,Sum_Approved,fundsAvailable,Activities" validvalues="cfc:com.relayFundManager.getRequestsForManagerColumnlist()" description="Columns shown in fund listing table" displayas="twoselects"/>
				</cfoutput>

				<cfif getRequestsForManager.recordCount>
					<CF_tableFromQueryObject
						queryObject="#getRequestsForManager#"
						queryName="getRequestsForManager"
						sortOrder = "#sortOrder#"
						numRowsPerPage="#numRowsPerPage#"
						startRow="#startRow#"
						useInclude="false"
						tableClass="screenTable"
						columnTranslation="true"
						columnTranslationPrefix="phr_fund_"
						<!--- 2015-09-16	ACPK	FIFTEEN-408 Set HidePageControls to No to allow user to change pages --->
						HidePageControls="no"
						showTheseColumns="#showTheseColumns#"

						keyColumnList="Activities"
						keyColumnURLList="#processPage#fundRequestID="
						keyColumnKeyList="Request_ID"

                        <!--- 2015-09-04 DAN - include numeric columns for number formatting --->
                        numberformat="count_requests,sum_Requested,sum_Approved,orgBudgetAllocation,fundsAvailable"
                        numberFormatMask="default,decimal,decimal,decimal,decimal"

						allowColumnSorting="no"
						dateFormat="created"
						currencyFormat="sum_Requested,sum_Approved,orgBudgetAllocation,fundsAvailable"
					>
				<cfelse>
					phr_fund_NoRequestsToView
				</cfif>

				<!--- lastSubmissionInterval is defined in the INI which tells us the number of days before the end of the quarter that determines
					the cutoff date for new applications. If it's defined and we're still in a valid date, of if it's not defined at all
					(meaning we can always show the add button), show the button. --->
				<!--- we can only create an activity if we have an account set up --->
				<cfset fundAccountsQry = application.com.relayFundManager.getFundAccountsAvailableForActivity(fundManagerID=request.relayCurrentUser.personID, ShowLiveAccountsOnly=ShowLiveAccountsOnly)>
				<cfset lastSubmissionInterval = application.com.settings.getSetting("fundManager.lastSubmissionInterval")>

				<cfif (application.com.relayFundManager.canAddActivity(lastSubmissionInterval=lastSubmissionInterval) or (lastSubmissionInterval eq 0)) and fundAccountsQry.recordCount gt 0>
					<cfform name="addRequestActivityForm"  method="post">
						<cf_relayFormDisplay>
							<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="add" label="">
							<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="addActivity" currentValue="phr_fund_addActivity" label="" class="submitButton">
						</cf_relayFormDisplay>
					</cfform>
				</cfif>
			<cfelse>
				phr_fund_YouMustBeAFundManagerToViewFundRequests
			</cfif>
		</cfif>
	<cfelse>
		<cfif editor eq "yes" and not isDefined("add")>
			<cfquery name="getFundRequest" datasource="#application.siteDataSource#">
				select accountID, budgetPeriodID from fundRequest where fundRequestID =  <cf_queryparam value="#fundRequestID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfset form.accountID = getFundRequest.accountID>
			<cfset form.budgetPeriodID = getFundRequest.budgetPeriodID>
		</cfif>
		<!--- save the content for the xml to define the editor --->
		<cfsavecontent variable="xmlSource">
			<cfoutput>
				<editors>
					<editor id="232" name="thisEditor" entity="fundRequest" title="Fund Request">
						<field name="FundRequestID" label="Request ID" description="This records unique ID." control="hidden"></field>
						<cfif isDefined("add") and add eq "yes">
							<field name="accountID" label="Account" control="SELECT" description="Account this request Relates to." query = "SELECT accountID AS Value, Account AS Display FROM vFundAccounts order by account"></field>
							<field name="budgetPeriodID" label="phr_fund_Period" control="select" query = "SELECT budgetPeriodID AS Value, description AS Display FROM budgetPeriod"></field>
						<cfelse>
							<field name="accountID" label="phr_fund_Account" ForeignKeyEntity="vFundAccounts" ForeignKey="accountID" ForeignKeyValues="account" control="FKReadOnly" query="select Account as display, accountID as value from vFundAccounts where accountID = #accountID#"></field>
							<field name="budgetPeriodID" label="phr_fund_Period" ForeignKeyEntity="budgetPeriod" ForeignKey="budgetPeriodID" ForeignKeyValues="description" control="FKReadOnly" query="select description as display, budgetPeriodID as value from budgetPeriod where budgetPeriodID = #budgetPeriodID#"></field>
						</cfif>
						<!--- <cfif isDefined("FundRequestID") and FundRequestID gt 0>
							<field name="PersonID" label="Contact" control="SELECT" description="Account this request Relates to." query = "SELECT personID AS Value, firstname +' '+lastname AS Display FROM person where organisationID = 1 order by firstname"></field>
						</cfif> --->
						<!--- <field name="quarter" label="Quarter" control="SELECT" description="Account this request Relates to." validvalues = "1,2,3,4"></field>
						<field name="year" label="Year" control="SELECT" description="Account this request Relates to." validvalues = "#listOfYears#"></field> --->
					</editor>
				</editors>
			</cfoutput>
		</cfsavecontent>
		<cfif editor eq "yes" and isDefined("add") and add eq "yes">
			<CF_RelayXMLEditor
				editorName = "thisEditor"
				xmlSourceVar = "#xmlSource#"
				add="yes"
				thisEmailAddress = "relayhelp@foundation-network.com"
			>
		<cfelseif editor eq "yes">

			<cfif request.relayCurrentUser.isInternal>

				<CF_RelayXMLEditor
					xmlSourceVar = "#xmlSource#"
					editorName = "thisEditor"
					thisEmailAddress = "relayhelp@foundation-network.com"
				>
			</cfif>
		</cfif>
	</cfif>

<cfelse>
    phr_insufficientFundRequestsPermission     <!---07-04-2016 case 448020 make entry in Fundrequest.cfm a translation--->
</cfif>

</cf_translate>

