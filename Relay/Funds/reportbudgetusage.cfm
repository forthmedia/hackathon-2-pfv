<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="Budget">

<cfset tableName="vMDFBudgetUsage">
<cfset dateField="created">
<cfset dateRangeFieldName="created">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<cfquery name="StatusReport" datasource="#application.siteDataSource#">
select * from vMDFBudgetUsage
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_title>Report Budget Usage</cf_title>

<cfset currencyCols = "quarterly_Budget,amt_Claimed,amt_Approved,amt_Paid,payments_Pending,Budget_Remaining">
<cfset filter1cols = "Budget_Type,Product_Category,Budget_Period">

<CF_tableFromQueryObject queryObject="#StatusReport#" 
	showTheseColumns="Budget,Country,Product_Category,Budget_Period,Quarterly_Budget,Amt_Claimed,Amt_Paid,Payments_Pending,Budget_Remaining"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	totalTheseColumns="#currencyCols#"
	FilterSelectFieldList="#filter1cols#"
	groupByColumns="Budget"
	queryWhereClauseStructure="#queryWhereClause#" 
	passThroughVariablesStructure="#passThruVars#"
>