<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">


<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" type="string" default="Region">

 <cfset tableName="vMDFClaimsByCountry">
<cfset dateField="created">
<cfset dateRangeFieldName="created">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<cfquery name="StatusReport" datasource="#application.siteDataSource#">
select * from vMDFClaimsByCountry
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_title>Claims By Country Report</cf_title>

<cfset currencyCols = "amt_Claimed,amt_Approved,amt_paid">

<cfset filter1cols = "Budget_Type,Product_Category">

<CF_tableFromQueryObject queryObject="#StatusReport#" 
	showTheseColumns="Region,Country,Product_Category,Num_Of_Claims,Amt_Claimed,Amt_Approved,Amt_Paid"
	sortOrder="#sortOrder#"
	selectbyday="false"
	currencyFormat="#currencyCols#"
	numberFormat="num_of_claims"
	totalTheseColumns="#currencyCols#,Num_Of_Claims"
	FilterSelectFieldList="#filter1cols#"
	groupByColumns="Region"
	queryWhereClauseStructure="#queryWhereClause#" 
	passThroughVariablesStructure="#passThruVars#"
>