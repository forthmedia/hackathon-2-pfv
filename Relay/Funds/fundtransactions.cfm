<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Fund Transactions</cf_title>

<cfparam name="accountID" type="numeric" default="1">

<cfquery name="getTransactions" datasource="#application.SiteDataSource#">
	select * from (
		select ft.*, 'phr_Edit' as Edit,
		bp.description as period,
		b.description as budget
		from fundTransaction ft inner join budgetPeriodAllocation bpa on
			ft.budgetAllocationID = bpa.budgetAllocationID inner join
			budgetPeriod bp on bp.budgetPeriodID = bpa.budgetPeriodID inner join
			budget b on b.budgetID = bpa.budgetID
		where accountID =  <cf_queryparam value="#accountID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	) base where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
</cfquery>
<cf_tableFromQueryObject 
	queryObject="#getTransactions#"
	queryName="getTransactions"
	showTheseColumns="budget,period,creditAmount,Edit"	
 	hideTheseColumns=""
	useInclude="false"
	currencyFormat="creditAmount"
	rowIdentityColumnName="fundTransactionID"
	FilterSelectFieldList = "budget,period"
	keyColumnList="Edit"
	keyColumnURLList="fundTransactions.cfm?editor=yes&fundTransactionID="
	keyColumnKeyList="fundTransactionID"
	keyColumnOpenInWindowList="no"
>

<cfif isDefined("editor") and editor eq "yes" and 1 eq 2>
	<!--- save the content for the xml to define the editor --->
	<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="fundTransaction" title="Add Funds">
			<field name="fundTransactionID" label="Transaction ID" description="This records unique ID." control="hidden"></field>
			<field name="fundTransactionTypeID" label="Transaction Type" description="" control="hidden"></field>
			<field name="accountID" label="Fund Account" description="" control="hidden"></field>
			<field name="budgetAllocationID" label="Budget Allocation" description="" control="hidden"></field>
			<field name="creditAmount" label="Credit Amount" description="" control="text"></field>
		</editor>
	</editors>
	</cfoutput>
	</cfsavecontent>
	<cfif editor eq "yes" and isDefined("add") and add eq "yes">
		<CF_RelayXMLEditor
			editorName = "thisEditor"
			xmlSourceVar = "#xmlSource#"
			add="yes"
			showSaveAndReturn="yes"
			showSaveAndNew="yes"
			thisEmailAddress = "relayhelp@foundation-network.com"
		>
	<cfelseif editor eq "yes">
		
		<cfif request.relayCurrentUser.isInternal> 
			<CF_RelayXMLEditor
				xmlSourceVar = "#xmlSource#"
				editorName = "thisEditor"
				showSaveAndReturn="yes"
				showSaveAndNew="yes"
				thisEmailAddress = "relayhelp@foundation-network.com"
			>
		</cfif>
	</cfif>
</cfif>