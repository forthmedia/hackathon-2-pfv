<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		fundRequestActivityV2.cfm
	Author:		PPB
Date started:	2010/11/25

Description:	New version of fundRequestActivities.cfm (Merge of fundRequestActivities.cfm and opportunityEditV2 technology).

Usage:
To implement a userfiles version, you take a copy of fundRequestActivityDisplayXML (and fundRequestActivityDomainXML) and modify them to suit requirement
You edit fundRequestActivityDisplay.cfm to switch the type/visiblity of the form fields.

You can add custom javascript form validation to the core validation by including /code/CFTemplates/fundRequestActivity_FormValidation.cfm containing javascript code

Amendment History:

Date (YYYY/MM/DD)	Initials 	What was changed

2011/03/09			NAS			LID5847: Fund Manager - New activity save in Firefox displays a screen with alignment issues - text scrolls to far right
2011/03/10			NAS			LID5794: Fund Manager - Unable to set multiple Fund Managers on a Fund Account
2011/03/16 			AJC 		P-LEN023 - Add phrase snippets code for fundrequesttypes
2011/03/22			PPB			LID5985	- sort order
2011/03/24			PPB			LID5935 - moved 2 warnings to errors
2011/03/25			PPB			LID5935 - defend against the amounts being left empty (rather than 0.00) - de-cluttered validation while at it
2011/03/31			PPB			LID6084	- prevent entry of approved amount if approval status is Pending or Rejected
2011/03/31			PPB			LID6104 - add a column for number of proofs in list
2011/04/01          PPB         rename & add a new hook include file
2011/04/04			PPB			send entity ID directly to displayFormField
2011/04/05			PPB			LID6128	- set HidePageControls="no" cos not all rows were being displayed
2011/04/06			AJC			LID6081: Fund Manager - Requests can be made for future period without an approved allocated budget
2011/04/07 			PPB 		no longer use getSetting("fundManager.approvalStatusIDList") - use the new table col isApproved
2011/04/20 			AJC 		Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget
2011/04/28			AJC			Issue 6188: Fund Manager - Save and Add New doesn't work on internally entered MDF requests
2011/05/09			AJC			Issue 6431: Fund Manager - I can no longer select any dates as they are all grayed out in the request form.
2011/05/23 			PPB 		moved a <cfoutput> outside of the <script> (done cos I thought at the time Lex stg had cf admin setting enablecfoutputonly=yes but actually was another due to code error in displaying form elements which has been fixed so this change not really necessary but may as well keep)
2012/05/16 			IH 			Case 427978 remove commas and white spaces from funding fields
2012/10/29			IH			Case 431500 add tableClass to table within addRequestActivityForm
2012-11-27 			PPB 		Case 432120 reset the dates to prevent the user entering dates and then selecting another budget group (for which the dates are not valid)
2013-01-25 			PPB 		Case 433111 javascript to disable dates needed updating to use the jQuery datepicker
2013-07-30 			PPB 		Case 436388 check the amount vs budgetAvailable regardless of whether the amount has changed (see case notes)
2013-07-31 			PPB 		Case 436260 added !editApproval condition so an activity for a previous period can be approved
2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application.
2014-10-14			RPW			CORE-854 Unable to successfully save an approval request
2014-10-20			RPW			CORE-835 The [Cancel] button is not styled the same as the [Add Activity] button
2014-10-29			RPW			CORE-909 Once a fund approval activity is opened there is now way of navigating back to the activity approvals screen.
2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view
2014-11-06 			AXA 		P-PGI001 removed inline styling for section headers and added new class
2015-01-30 			PPB 		P-KAS049 added trId to Budget Group in list header as a styling hook
2016-07-03			AHL			Ticket 1240 (Kanban) SUS001 - MDF Budget Periods on Portal not working
2015-08-06			AHL			SUS001 Adding Rending Functionality
2015-08-20          DAN         K-1443 - fix for calculating total approved amount only if the given value is a number, fix for validation start/end period dates
2015-09-04          DAN         include numeric columns for number formatting
2015-09-07          DAN         K-1491 - fix bug where adding more than one activity in a row does not save the other activity flag values
2015/09/25			NJH			PROD2015-86  - include abiltiy to add screens (similar to opportunityDisplay)
2015-10-26			ACPK		PROD2015-142 Encode percentage signs in webhandle
2015-10-26			AHL			Kanban 1603 Customizing MDF Activity Columns by Settings
2015-11-09          DAN         Case 446455 - CORE - MDF Total is not being calculated automatically
2015-11-19 			PPB 		Risco Initial Implementation added fieldnames as styling hooks
2016-01-08          DAN         Case 446843 - replace hard-coded values (introduced by Kanban 1603)
2016-06-27 DCC for Katya case 450128 Cost of activity is not being calculated
2016-06-29			WAB			During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Should not be necessary ('private variable')
2016-10-13			RJT			PROD2016-2523 (and 451687) Pass the entityID to displayFormField so it can successfully show the current value
Possible enhancements:

--->

<cfparam name="fundRequestID" type="numeric" default=0>
<cfparam name="sortOrder" default="fundRequestActivityID">
<cfparam name="numRowsPerPage" default="50">
<cf_param name="startRow" default="1">
<cf_param name="phrasePrefix" type="string" default="">
<cf_param name="fundRequestActivityID" type="numeric" default=0>
<cfparam name="processPage" type="string" default="fundRequestActivities.cfm?">
<cfparam name="variables.mode" type="string" default="view">
<cf_param name="frmOrganisationID" label="OrganisationID" type="numeric" default="#request.relayCurrentUser.organisationID#">
<cfparam name="form.mdfpartnerOrganisationID" type="numeric" default="#frmOrganisationID#">
<cf_param name="URL.mdfpartnerOrganisationID" type="numeric" default="#form.mdfpartnerOrganisationID#"><!--- 2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view --->

<cfif isDefined("fundRequestID") and fundRequestID eq "">
	<cfset fundRequestID = 0>
</cfif>

<cfif fundRequestID eq 0 and fundRequestActivityID neq 0>
	<cfquery name="getFundRequestID" datasource="#application.siteDataSource#">
		select fundRequestID from fundRequestActivity where fundRequestActivityID =  <cf_queryparam value="#fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfset fundRequestID = getFundRequestID.fundRequestID>
</cfif>

<!--- 2014-10-29			RPW			CORE-909 Once a fund approval activity is opened there is now way of navigating back to the activity approvals screen. --->
<cfif request.relayCurrentUser.isInternal>
	<cf_title>Fund Request Activities</cf_title>
	<cfscript>
		variables.remoteLocation = application.com.security.encryptquerystring("fundRequestActivities.cfm?fundRequestID=" & fundRequestID);
	</cfscript>
	<cfoutput>
	<script language="JavaScript" type="text/javascript">
		jQuery(document).ready(function() {

			if (window.opener) {
				document.getElementById("cancelLink").href = "#variables.remoteLocation#";

				jQuery("##actionBar ul").append(
					jQuery("<li>").append(
						jQuery("<a>").attr("href","javascript:self.close();").append(
						"Close"
				)));

			} else if (window.top !== window.self) {

				<!--- 2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view --->
				if (history.length > 1){
					document.getElementById("cancelLink").href = "javascript:cancelbutton();";
				} else {
					document.getElementById("cancelLink").href = "fundRequestActivityApprovals.cfm";
				}
			}

		});
	</script>
	<div id="actionBar">
		<ul>
			<li><a id="cancelLink" href="fundRequestActivityApprovals.cfm" class="btn btn-primary">phr_Back</a></li>
		</ul>
	</div>
	</cfoutput>
</cfif>



<!--- p-lex039 SSS financeApprover allow access to everthing a approver has once the opp goes to claim level --->
<cfset getfinanceApprover = application.com.flag.getFlagData(flagId="financeApprover", entityID=#request.relayCurrentUser.personID#)>

<cfif getfinanceApprover.recordcount EQ 0>
	<cfset financeApprover = 0>
<cfelse>
	<cfset financeApprover = 1>
</cfif>

<cfif structKeyExists(form,"mode")>
	<cfset variables.mode = form.mode>
<cfelseif structKeyExists(url,"mode")>
	<cfset variables.mode = url.mode>
</cfif>

<!--- 2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application. --->
<cfif variables.mode EQ "processedView">
	<cfset variables.mode = "view">
<cfelseif variables.mode EQ "processedAdd">
	<cfset variables.mode = "add">
</cfif>

<cfset lastSubmissionInterval = application.com.settings.getSetting("fundManager.lastSubmissionInterval")>

<!--- set some class setting depending on whether we're an internal or external user --->
<cfif not request.relayCurrentUser.isInternal>
	<cfset tableClass="withBorder">
	<cfset submitBtnClass="">
	<cfset phraseSuffix="">
<cfelse>
	<!--- 2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view --->
	<cfset tableClass="withBorder">
	<cfset submitBtnClass = "submitButton">
	<cfset phraseSuffix = "Ext">
</cfif>


<!--- AFTER SUBMIT: adding or editing a fund request activity --->
<cfif isDefined("frmAddActivity") or isDefined("frmEditActivity") or isDefined("frmSaveAddNewActivity")>
	<!--- pass arguments through as a form structure --->
	<cfif isDefined("frmAddActivity") or isDefined("frmSaveAddNewActivity")>
		<!--- START:  NYB 2009-09-21 LHID2623 - added: --->
		<!--- WAB NOTE 2009/10/06 (LID 2734), I don't think that this does what is expected, this function brings back a whole query of records and we end up picking a random account --->
		<cfif AccountID eq "" and budgetGroupID neq "">
			<!--- START: 2009-12-07 - AJC - P-LEX039 Need to get by organisationID otherwise it selects random Orgs in the budget! --->
			<cfset form.accountID = application.com.relayFundManager.getAccountFromBudgetGroupID(BudgetGroupID=budgetGroupID,OrganisationID=fundAccountOrganisationID).accountID>
			<!--- END: 2009-12-07 - AJC - P-LEX039 Need to get by organisationID otherwise it selects random Orgs in the budget! --->
		</cfif>

		<!--- END:  NYB 2009-09-21 LHID2623 --->
		<cfset fundRequestActivityID = application.com.relayFundManager.addFundActivityV2(activityFormStruct=form)>
		<cfscript>
			//2014-10-14			RPW			CORE-854 Unable to successfully save an approval request
			application.com.relayui.setMessage(
				message="phr_fund_YourApplicationHasBeenSubmitted",
				type="success",
				scope="session"
			);
		</cfscript>
		<!--- if the save and add new button was clicked, then return to add mode --->
		<cfif isDefined("frmSaveAddNewActivity")>
			<!--- 2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application. --->
			<cfscript>
				//2014-10-14			RPW			CORE-854 Unable to successfully save an approval request
				if (request.relayCurrentUser.isInternal) {
					variables.queryString = CGI.SCRIPT_NAME & "?mode=processedAdd&fundRequestActivityID=" & fundRequestActivityID;
				} else {
					variables.queryString = "/?eid=" & URL.eid & "&mode=processedAdd&fundRequestActivityID=" & fundRequestActivityID;
				}
				variables.queryString = application.com.security.encryptquerystring(variables.queryString);
				location(url=variables.queryString, addToken=false);
			</cfscript>
		<cfelse>
			<!--- 2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application. --->
			<cfscript>
				//2014-10-14			RPW			CORE-854 Unable to successfully save an approval request
				if (request.relayCurrentUser.isInternal) {
					variables.queryString = CGI.SCRIPT_NAME & "?mode=processedView&fundRequestActivityID=" & fundRequestActivityID;
				} else {
					variables.queryString = "/?eid=" & URL.eid & "&mode=processedView&fundRequestActivityID=" & fundRequestActivityID;
				}
				variables.queryString = application.com.security.encryptquerystring(variables.queryString);
				location(url=variables.queryString, addToken=false);
			</cfscript>
		</cfif>
		<!--- END: 2011/04/28 - AJC - Issue 6188: Fund Manager - Save and Add New doesn't work on internally entered MDF requests --->
	<cfelseif isDefined("frmEditActivity")>
		<!--- 2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application. --->
		<cfscript>
			//2014-10-14			RPW			CORE-854 Unable to successfully save an approval request
			application.com.relayFundManager.updateFundActivityV2(activityFormStruct=form);

			application.com.relayui.setMessage(
				message="phr_fund_ApprovalStatus_updated",
				type="success",
				scope="session"
			);

			if (request.relayCurrentUser.isInternal) {
				variables.queryString = CGI.SCRIPT_NAME & "?mode=processedView&fundRequestActivityID=" & fundRequestActivityID;
			} else {
				variables.queryString = "/?eid=" & URL.eid & "&mode=processedView&fundRequestActivityID=" & fundRequestActivityID;
			}
			variables.queryString = application.com.security.encryptquerystring(variables.queryString);
			location(url=variables.queryString, addToken=false);
		</cfscript>
	</cfif>
</cfif>

<cfinclude template="/templates/relayFormJavaScripts.cfm">

<!--- <cfif request.relayCurrentUser.isInternal>
	<cfinclude template="fundsTopHead.cfm">
</cfif> --->

<!--- 2014-10-14			RPW			CORE-854 Unable to successfully save an approval request --->
<!---
<cfset application.com.relayui.displayMessage(scope="session",deleteMessage=true,closeAfter=3)>
 --->

<cfif fundRequestID neq 0>

	<cfquery name="getFundRequestDetails" datasource="#application.siteDataSource#">
		select fr.accountID, fr.budgetPeriodID,o.organisationId,o.organisationName,bp.description as budgetPeriod,
			c.countryID,c.countryDescription as country,bh.BudgetGroupID,bh.description as BudgetGroup,
			b.entityTypeID as budgetType, isNull(bpa.amount,0) as budgetAllocated
		from fundRequest fr inner join fundCompanyAccount fca
			on fca.accountID = fr.accountID inner join budgetPeriod bp
			on bp.budgetPeriodID = fr.budgetPeriodID inner join organisation o
			on o.organisationID = fca.organisationID inner join country c
			on o.countryID = c.countryID inner join fundAccountBudgetAccess faba
			on faba.budgetID = fca.budgetID and faba.accountID = fca.accountID inner join budget b
			on b.budgetID = faba.budgetID inner join BudgetGroup bh
			on bh.BudgetGroupID = b.BudgetGroupID left outer join budgetPeriodAllocation bpa
			on bpa.budgetPeriodID = fr.budgetPeriodID and bpa.budgetID = b.budgetID
		where fundRequestID =  <cf_queryparam value="#fundRequestID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfscript>
		fundManager=application.com.relayFundManager.getFundAccountManagers(accountID=getFundRequestDetails.accountID);
	</cfscript>

	<!--- set the fund manager details --->
	<cfif not fundManager.recordCount>
		<cfset fundManagerDetails = "phr_fund_NoFundManagersHaveBeenSetForThisAccount">
	<cfelseif fundManager.recordCount eq 1>
		<cfset fundManagerDetails = fundManager.fullname <!--- & IIF(len(fundManagers.email) gt 0,DE(" <strong>phr_email</strong>: <a href='mailto:"&fundManagers.email&"'>"&fundManagers.email&"</a>"),DE("")) & IIF(len(fundManagers.officePhone) gt 0,DE(" <strong>phr_officePhone</strong>: "&fundManagers.officePhone),DE("")) & "</br>" --->>
	<cfelse>
		<!--- 2011/03/10			NAS			LID5794: Fund Manager - Unable to set multiple Fund Managers on a Fund Account --->
		<!--- <cfset fundManagerDetails="<table><tr><td>&nbsp;</td><td><strong>phr_email</strong></td><td><strong>phr_officephone</strong></td></tr>"> --->
		<cfset fundManagerDetails="<table style='position: relative; left: 0px; top: -3px;'>">
		<cfloop query="fundManager">
			<!--- 2011/03/10			NAS			LID5794: Fund Manager - Unable to set multiple Fund Managers on a Fund Account --->
			<!--- <cfset fundManagerDetails = fundManagerDetails&"<tr><td>" & fullname &"</td><td><a href='mailto:"&email&"'>"&email&"</a></td><td>"& officePhone & "</td></tr>"> --->
			<cfset fundManagerDetails = fundManagerDetails&"<tr><td>" & fullname &"</td></tr>">
		</cfloop>
		<cfset fundManagerDetails= fundManagerDetails& "</table>">
	</cfif>
</cfif>
<!--- START: 2011/04/20 AJC Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget	--->
<!--- setting the date ranges for adding/editing fund request activities --->
<!--- <cfif application.com.login.checkInternalPermissions("fundTask","level3") and request.relaycurrentuser.isInternal>
	<!--- NJH 2008/01/29 MDF Phase 3. If an approver, don't limit the dates in add/edit mode to just be in the current quarter. --->

	<!--- START: 2011/04/06 AJC LID6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

	<cfquery name="getMaxMinDatesFromBudgetPeriod" datasource="#application.siteDataSource#">

		select min(bp.startDate) as startDate, max(bp.endDate) as endDate from budgetPeriod bp

	</cfquery>

	<!--- END: 2011/04/06 AJC LID6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

	<cfset disableFromDate = getMaxMinDatesFromBudgetPeriod.startDate>
	<cfset disableToDate = getMaxMinDatesFromBudgetPeriod.endDate> --->
<cfif variables.mode eq "add" or variables.mode eq "edit">
	<cfif variables.mode eq "add">
		<!--- START 2016-07-03 AHL Ticket 1240 (Kanban) SUS001 - MDF Budget Periods on Portal not working --->
		<cfset local.budgetGroupID = 0/>
		<cfset local.relayFundManager = CreateObject("component",'relay.webservices.relayFundManagerWS') />
		<cfset local.qryBudgetGroupID = local.relayFundManager.getSelectBudgetGroups(MDFPARTNERORGANISATIONID) />
		<cfset local.budgetGroupID = (local.qryBudgetGroupID.recordCount eq 1)?local.qryBudgetGroupID.BudgetGroupId:0 />
		<cfquery name="qryBudgetPeriodFromBudgetGroup" datasource="#application.sitedatasource#">
				SELECT MIN(bp.startDate) as startDate, MAX(bp.endDate) as endDate FROM BudgetPeriod bp
					INNER JOIN BudgetPeriodAllocation bpa ON bp.budgetPeriodID = bpa.budgetPeriodID
					INNER JOIN budget b ON b.budgetID = bpa.budgetID AND b.BudgetGroupID = <cfqueryparam cfsqltype="cf_sql_integer" value="#local.budgetGroupID#" />
					WHERE bp.endDate > GETDATE()
		</cfquery>
		<cfset disableFromDate = qryBudgetPeriodFromBudgetGroup.endDate>
		<cfset disableToDate = qryBudgetPeriodFromBudgetGroup.startDate>
	<cfelse>
		<cfset budgetPeriodForRequestQry = application.com.relayFundManager.getOrganisationBudgetGroup(budgetGroupID=getFundRequestDetails.budgetGroupID,organisationID=getFundRequestDetails.organisationID)>
		<cfset disableFromDate = budgetPeriodForRequestQry[1].budgetperiodENDDate>
		<cfset disableToDate = budgetPeriodForRequestQry[1].budgetperiodStartDate>
		<!--- END 2016-07-03 AHL Ticket 1240 --->
	</cfif>
	<cfif isdefined("getFundRequestDetails")>

		<cfset qryFundAccounts = application.com.relayFundManager.getAccountFromBudgetGroupID(budgetGroupID=getFundRequestDetails.budgetGroupID,organisationID=getFundRequestDetails.organisationID) />

		<cfquery name="qry_get_budgetperiodDates" datasource="#application.sitedatasource#">
			select min(bp.startDate) as startDate, max(bp.endDate) as endDate from budgetPeriod bp
			inner join BudgetPeriodAllocation bpa on bp.budgetPeriodID = bpa.BudgetPeriodID and bpa.authorised = 1
			inner join Budget b on bpa.budgetID = b.budgetID
			<cfif qryFundAccounts.recordcount neq 0>
				and bpa.budgetID =  <cf_queryparam value="#qryFundAccounts.budgetID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				and bpa.budgetID = -1
			</cfif>
		</cfquery>
		<!--- START 2015-07-03 AHL Ticket 1240 (Kanban) SUS001 - MDF Budget Periods on Portal not working --->
		<cfset disableFromDate = qry_get_budgetperiodDates.endDate>
		<cfset disableToDate = qry_get_budgetperiodDates.startDate>
		<!--- END 2015-07-03 AHL Ticket 1240 (Kanban) --->
	</cfif>

	<!--- 2014-10-10			RPW			CORE-833 If I refresh the fund applications webpage it adds another application. --->
	<cfscript>
		if (!Len(disableFromDate)) {
			disableFromDate = "NULL";
		}
		if (!Len(disableToDate)) {
			disableToDate = "NULL";
		}
	</cfscript>
		<!--- <cfquery name="getMaxMinDatesFromBudgetPeriod" datasource="#application.siteDataSource#">
		select min(bp.startDate) as startDate, max(bp.endDate) as endDate from budgetPeriod bp
		</cfquery> --->

</cfif>

<!--- END: 2011/04/20 			AJC 		Issue 6081: Fund Manager - Requests can be made for future period without an approved allocated budget --->

<cfif variables.mode eq "view">		<!--- this is the list of activities (I wanted to change it to "list" but is defined in several places and could interfere with users of fundRequestActivityV1) --->

	<cfif isApprover or isManager>
		<!--- display the fund request details in the header --->
 		<cf_relayFormDisplay class="#tableClass#">						<!--- PPB added styling for external --->
			<!--- START: 2015-11-19 PPB Risco Initial Implementation added fieldnames as styling hooks --->
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="fundRequestActivityHeader_OrganisationName" currentValue="#getFundRequestDetails.organisationName#" label="phr_fund_account" labelAlign="right">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="fundRequestActivityHeader_FundManager" currentValue="#fundManagerDetails#" label="phr_fund_FundManager" labelAlign="right">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="fundRequestActivityHeader_Country" currentValue="#getFundRequestDetails.country#" label="phr_fund_country" labelAlign="right">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="fundRequestActivityHeader_BudgetPeriod" currentValue="#getFundRequestDetails.budgetPeriod#" label="phr_fund_period" labelAlign="right">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="fundRequestActivityHeader_BudgetGroup" currentValue="#getFundRequestDetails.BudgetGroup#" label="phr_fund_BudgetGroup" labelAlign="right">
			<!--- END: 2015-11-19 PPB Risco Initial Implementation --->
		</cf_relayFormDisplay>


		<!--- START 2015-10-27 AHL Kanban 1603 Customizing MDF Activity Columns by Settings. Moving Function to component --->
		<cfset getFundRequestActivities = application.com.relayFundManager.getFundRequestActivities(fundRequestID=fundRequestID,sortOrder=sortOrder,isManager=isManager)/> <!--- 2016-01-08 DAN Case 446843 - replace hard-coded values --->
		<!--- END 2015-10-27 AHL Kanban 1603 --->

		<!--- START 2015-10-26 AHL Kanban 1603 Customizing MDF Activity Columns by Settings. Changed done in Kaspersky --->
		<cfif request.relayCurrentUser.isInternal>
			<cfset showTheseColumns="#application.com.settings.getSetting('fundManager.showTheseColumnsInternal')#">
		<cfelse>
			<cfset showTheseColumns="#application.com.settings.getSetting('fundManager.showTheseColumnsPortal')#">
		</cfif>
		<!--- END 2015-10-26 AHL Kanban 1603 --->

		<cfif request.relayCurrentUser.isInternal>
			<cfset showTheseColumns = showTheseColumns & ",partnername">
		</cfif>
		<cfset showTheseColumns = showTheseColumns & ",Edit_View,Upload_Proofs,numProofs">

		<!--- display the activities --->
		<cfif getFundRequestActivities.recordCount>
			<cf_tableFromQueryObject
				queryObject="#getFundRequestActivities#"
				queryName="getFundRequestActivities"
				sortOrder = "#sortOrder#"
				numRowsPerPage="#numRowsPerPage#"
				startRow="#startRow#"
				columnTranslation="true"
				columnTranslationPrefix="phr_fund_"
				tableClass="#tableClass#"
				HidePageControls="no"

				showTheseColumns="#showTheseColumns#"
			 	hideTheseColumns=""
				useInclude="false"

				keyColumnList="Edit_View,Upload_Proofs"
				keyColumnURLList="#processPage#mode=edit&fundRequestActivityID=,/screen/entityRelatedFilePopup.cfm?replaceallowed=false&entityType=fundRequestActivity&entityID="
				keyColumnKeyList="fundRequestActivityID,fundRequestActivityID"
				keyColumnOpenInWindowList="no,yes"

                <!--- 2015-09-04 DAN - include numeric columns for number formatting --->
                numberformat="totalCost,requestedAmount,approvedAmount"
                numberFormatMask="decimal,decimal,decimal"
				dateFormat="startDate,endDate"
				currencyFormat="totalCost,requestedAmount,approvedAmount"
				totalTheseColumns="totalCost,requestedAmount,approvedAmount"

				rowIdentityColumnName="fundRequestActivityID"
			>
		<cfelse>
			phr_fund_NoRequestActivitiesToView
		</cfif>

		<!--- if we're a manager on the external portal, display the cancel/add activity buttons --->
		<cfif not request.relayCurrentUser.isInternal and isManager>
			<cfform name="addRequestActivityForm"  method="post">
				<cf_relayFormDisplay class="#tableClass#">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="add" label="">
                    <cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="right">
						<!--- 2014-10-20	RPW	CORE-835 The [Cancel] button is not styled the same as the [Add Activity] button --->
 						<cf_relayFormElement relayFormElementType="button" fieldname="cancel" currentValue="phr_fund_Cancel" label="" onClick="javascript:history.go(-1);">

						<!--- lastSubmissionInterval eq 0 is defined in the INI which tells us the number of days before the end of the quarter that determines
						the cutoff date for new applications. If it's defined and we're still in a valid date, of if it's not defined at all
						(meaning we can always show the add button), show the button. --->
						<!--- START: 2009-12-17 AJC P-LEX039 - Bug fix - Changed from isdefined to = 0 --->
						<cfif (application.com.relayFundManager.canAddActivity(lastSubmissionInterval=lastSubmissionInterval) or (lastSubmissionInterval eq 0)) and (getFundRequestActivities.AccountClosed neq 1)>
						<!--- END: 2009-12-17 AJC P-LEX039 - Bug fix - Changed from isdefined to = 0 --->
							<cf_relayFormElement relayFormElementType="submit" fieldname="addActivity" currentValue="phr_fund_addActivity" label="" class="submitButton">
						</cfif>
					</cf_relayFormElementDisplay>
				</cf_relayFormDisplay>
			</cfform>
		</cfif>

	</cfif>

<cfelse>		<!--- add/edit --->

	<cfparam name="fundRequestActivityID" type="numeric" default="0">
	<cfparam name="returnURL" type="string" default="#cgi.HTTP_REFERER#">

	<cfif variables.mode eq "add">
		<cfset fundRequestActivityID = 0>
        <cfset form.fundRequestActivityID = 0> <!--- 2015-09-07 DAN    K-1491 - fix bug where adding more than one activity in a row does not save the other activity flag values --->
	</cfif>

	<cfset returnURL = #urldecode(returnURL)#>

	<!--- NJH 2015/09/25 - PROD2015-86 - get a list of all the flags/groups to be retrieved in the getfundActivity query so that these fields are available to any evaluate condition for screens --->
	<cfquery name="getFundActivityProfiles">
		select distinct flagTextID as textId from vFlagDef where entityTable = 'fundRequestActivity' and datatable !='boolean' and flagTextID is not null
		union
		select distinct flagGroupTextID as textId from vFlagDef where entityTable = 'fundRequestActivity' and datatable ='boolean' and flagGroupTextID is not null
	</cfquery>

	<cfset qryFundActivity = application.com.relayFundManager.getFundActivityV2(activityID=fundRequestActivityID,flagTextIdList=valueList(getFundActivityProfiles.textId))>	<!--- NB we need to do this even in Add mode cos the queryset is used to setup the form fields --->

	<cfif variables.mode eq "add">

		<cfif not request.relayCurrentUser.isInternal and not isManager>
			phr_fund_YouMustBeAFundManagerToAddFundActivities
		</cfif>

	<cfelseif variables.mode eq "edit">
		<cfset CurrentUserApproverLevelQry = application.com.relayFundManager.GetApproverLevelForCountry(FundRequestID=fundRequestID)>

		<cfset availableBudgetsQry = application.com.relayFundManager.getBudgetAllocationsForAccount(accountID=getFundRequestDetails.accountID,budgetPeriodID=getFundRequestDetails.budgetPeriodID,BudgetGroupID=getFundRequestDetails.BudgetGroupID)>
	</cfif>

	<cfif not structKeyExists(form,"fundRequestActivityID")>
		<!--- 2016-06-29	WAB	During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Did not seem to be serving much purpose --->
		<cfset form.fundRequestActivityID = url.fundRequestActivityID>
	</cfif>

	<cf_includeJavascriptOnce template = "/javascript/openWin.js">

	<cf_includeJavascriptOnce template="/javascript/tableManipulation.js">
	<cf_includeJavascriptOnce template="/javascript/checkObject.js">
	<cf_includeJavascriptOnce template = "/javascript/fnlajax.js">

	<cfinclude template="/templates/relayFormJavaScripts.cfm">

	<!--- if the fundRequestActivity display xml is not loaded into memory (it will get deleted when application variables are loaded), load it --->
	<cfif not structKeyExists(application,"fundRequestActivityXML")>
		<cfset application.com.relayDisplay.loadDisplayXML(tablename="fundRequestActivity")>
	 </cfif>

	<cfset fundRequestActivityDetailsStruct = application.com.structureFunctions.queryRowToStruct(query=qryFundActivity,row=qryFundActivity.recordCount)>

	<cfset fundRequestActivityFieldsXML = duplicate(application.fundRequestActivityXML)>

	<!--- set the various field attributes for display... things such as readonly, make hidden, etc. --->
	<cfinclude template="fundRequestActivityDisplay.cfm">

	<cfform name="fundRequestActivityForm"  method="post" onSubmit="return verifyForm('#mode#',#editApproval#);" noValidate=true> <!---??? > --->
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmTask" label="" currentValue="Save" >
		<cf_relayFormElement relayFormElementType="hidden" fieldname="returnURL" label="" currentValue="#returnURL#" >

		<cf_relayFormDisplay id="fundRequestActivityTable" showValidationErrorsInline="true" class="#tableClass#">
			<cfif form.fundRequestActivityID eq 0>
				<cfset headingPhrase = "phr_NewFundRequestActivity">
			<cfelse>
				<cfset headingPhrase = "phr_FundRequestActivity">
			</cfif>

			<cfoutput>
			<cfif request.relayFormDisplayStyle eq "HTML-table">
				<tr>
					<td colspan="2" style="text-align:center"><h2>#htmleditformat(headingPhrase)#</h2></td>
				</tr>
			<cfelse>
				<h2>#htmleditformat(headingPhrase)#</h2>
			</cfif>
			</cfoutput>

			<cfif isDefined("message")>
				<cf_relayFormElementDisplay relayFormElementType="message" currentValue="#message#" fieldname="message" label="">
			</cfif>

			<!--- loop through the groups/fields --->
			<cfif not request.relayCurrentUser.isInternal and StructKeyExists(fundRequestActivityFieldsXML.fundRequestActivity,"external")>
				<cfset fieldItemsArray = fundRequestActivityFieldsXML.fundRequestActivity.external.xmlChildren>
			<cfelse>
				<cfset fieldItemsArray = fundRequestActivityFieldsXML.fundRequestActivity.internal.xmlChildren>
			</cfif>
			<cfloop from=1 to="#arrayLen(fieldItemsArray)#" step="1" index="idx">
				<cfset node = fieldItemsArray[idx]>
				<!--- <cfdump var="#node#" output="d:\web\tempdebug.html" format="html"> ---> <!--- USEFUL DEBUG IF YOU CAN'T EASILY TELL WHICH FORM FIELD IS CAUSING AN ERROR --->
				<cfif node.xmlName eq "group">
					<cfif not StructKeyExists(displayStruct,node.xmlAttributes.name) or not StructKeyExists(displayStruct[node.xmlAttributes.name],"type") or displayStruct[node.xmlAttributes.name].type neq "hidden">		<!--- if the structure isn't defined default to display it --->
						<cfif request.relayFormDisplayStyle eq "HTML-div">
							<div class="grey-heading">
							<tr>
								<th colspan="2" style="text-align:center" class="sectionHeading" >  <!--- 2014-11-06 AXA P-PGI001 removed inline styling added new class --->
									<cfoutput>phr_fund_#replace(node.xmlAttributes.name," ","")#</cfoutput>
								</th>
							</tr>
							<tr>
								<td colspan="2"><cfoutput>phr_fund_#replace(node.xmlAttributes.name," ","")#Desc</cfoutput></td>
							</tr>
							</div>
						<cfelse>
							<h4><cfoutput>phr_fund_#replace(node.xmlAttributes.name," ","")#</cfoutput></h4>
							<p><cfoutput>phr_fund_#replace(node.xmlAttributes.name," ","")#Desc</cfoutput></p>
						</cfif>

							<cfif arrayLen(node.xmlChildren) gt 0>
								<cfset fieldsArray = node.xmlChildren>
								<cfloop from=1 to="#arrayLen(fieldsArray)#" step="1" index="fieldIdx">
									<cfset node = fieldsArray[fieldIdx]>
									<cfset args = duplicate (node.xmlattributes)>
									<!--- START 2015-08-06 AHL SUS001 Adding Rending Functionality --->
									<cfif StructKeyExists(args,'render') and not evaluate(args.render) >
										<cfcontinue />
									</cfif>
									<!--- END 2015-08-06 AHL SUS001 --->

									<cfif node.xmlName eq "field">

									<!--- RMB - 2012-10-03 - P-XIR001 - Make Custom flags in PS xml code Readonly when edit more it turned off - START --->
										<!--- 2012-10-24 IH Case 431471 commented out recent changes because they made the form read only
										<cfif IsDefined('editMode') AND NOT editMode>
											<cfset node.xmlattributes.READONLY = true>
										</cfif>
										--->
									<!--- RMB - 2012-10-03 - P-XIR001 - Make Custom flags in PS xml Readonly when edit more it turned off - END --->

										<cfset fieldAttributes = application.com.relayDisplay.getQueryAttributeForSelect(fieldAttributes=node.xmlattributes)>
										<cfset application.com.relayDisplay.displayFormField(fieldAttributes=fieldAttributes,entityID=form.fundRequestActivityID)>

								 	<cfelseif node.xmlName eq "space">
								 		<cfif request.relayFormDisplayStyle eq "HTML-table">
								 			<tr><td colspan="2">&nbsp;</td></tr>
								 		</cfif>

									<!--- NJH 2015/09/25 - PROD2015-86 - include a screen --->
								 	<cfelseif node.xmlName eq "screen">
										<cfset useScreenByName = structKeyExists(node.xmlAttributes,"screenID")?node.xmlAttributes.screenID:"fundRequestActivityEdit_Custom">
										<cfset application.com.relayDisplay.showScreenElementsInDisplayXML(screenID=useScreenByName,entityID=form.fundRequestActivityID,countryID=0,entityDetails=fundRequestActivityDetailsStruct)>

								 	<!--- include custom file --->
									<cfelseif node.xmlName eq "includeFile">
										<cfset application.com.relayDisplay.includeFileForDisplay(node=node)>

									<cfelseif node.xmlName eq "embeddedCode">		<!--- this section is for chunks of core code to avoid numerous include files; custom code should be done in an includeFile --->
										<cfif StructKeyExists(displayStruct[node.xmlAttributes.name],"type") and displayStruct[node.xmlAttributes.name].type neq "hidden">
											<cfif node.xmlAttributes.name eq "documentStates">
												<cfoutput>
												<cfset mdfDocStates=application.com.settings.getSetting('fundManager.document.states')>
												<cfif request.relayFormDisplayStyle eq "HTML-table">
													<tr>
														<td width="275" valign="top" class="label">
															<strong>#node.xmlAttributes.label#</strong>
														</td>
														<td>
															<table>
															<tr>
																<td>
																</td>
															</tr>
															<tr>
																<td>
																	&nbsp;
																</td>
																<cfloop list="#MDFDocStates#" index="state">
																<th>
																	#htmleditformat(state)#
																</th>
																</cfloop>
															</tr>
															<cfloop list="#application.com.settings.getSetting('fundManager.document.types')#" index="doc">
															<tr>
																<td>
																	#htmleditformat(doc)#
																</td>
																<cfloop list="#MDFDocStates#" index="state">
																	<cfset  chkedAlready = 0 >
																	<cfset meValStr = doc & '=' & state>


																	<cfif qryFundActivity.docstatus NEQ "">
																		<td align="center">
																			<CF_INPUT type="radio" name="#doc#" value="#state#" checked="#iif( val(ListContainsNoCase(qryFundActivity.docstatus,meValStr,"")) neq 0 ,true,false)#">
																		</td>
																	<cfelse>
																		<td align="center">
																			<input type="radio" name="#doc#" value="#state#" <cfif state EQ "required">checked</cfif>>
																		</td>
																	</cfif>
																</cfloop>
															</tr>
															</cfloop>
															<tr>
																<td>
																	&nbsp;
																</td>
															</tr>
															</table>
														</td>
													</tr>
												<cfelse>
													<div class="form-group row">
														<div class="col-xs-12 col-sm-3 control-label">
															#node.xmlAttributes.label#
														</div>
														<div class="col-xs-12 col-sm-9">
															<table>
																<thead>
																	<tr>
																		<th>
																			&nbsp;
																		</th>
																		<cfloop list="#MDFDocStates#" index="state">
																		<th>
																			#htmleditformat(state)#
																		</th>
																		</cfloop>
																	</tr>
																</thead>
																<tbody>
																	<cfloop list="#application.com.settings.getSetting('fundManager.document.types')#" index="doc">
																		<tr>
																			<td>
																				#htmleditformat(doc)#
																			</td>
																			<cfloop list="#MDFDocStates#" index="state">
																				<cfset  chkedAlready = 0 >
																				<cfset meValStr = doc & '=' & state>


																				<cfif qryFundActivity.docstatus NEQ "">
																					<td align="center">
																						<CF_INPUT type="radio" name="#doc#" value="#state#" checked="#iif( val(ListContainsNoCase(qryFundActivity.docstatus,meValStr,"")) neq 0 ,true,false)#">
																					</td>
																				<cfelse>
																					<td align="center">
																						<input type="radio" name="#doc#" value="#state#" <cfif state EQ "required">checked</cfif>>
																					</td>
																				</cfif>
																			</cfloop>
																		</tr>
																	</cfloop>
																</tbody>
															</table>
														</div>
													</div>
												</cfif>
												</cfoutput>

											<cfelseif node.xmlAttributes.name eq "bankDetails">
												<cfif qryFundActivity.docstatus NEQ "">
													<cfset listredbold = "required, open query">
													<cfloop index="ListElement" list="#qryFundActivity.docstatus#" delimiters = "">
														<cfset DisplayName = listgetat(ListElement, 1, "=")>
														<cfif DisplayName EQ "bankdetails">
															<cfset DisplayName = "Bank Details">
														</cfif>
														<cfset DisplayValue = listgetat(ListElement, 2, "=")>
														<cfif listcontainsnocase(listredbold, DisplayValue) EQ 0>
															<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="#DisplayName#" currentValue="#DisplayValue#" label="#DisplayName#">
														<cfelse>
															<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="#DisplayName#" currentValue="<span class='required'>#htmleditformat(DisplayValue)#</span>" label="#htmleditformat(DisplayName)#">
														</cfif>
													</cfloop>
												</cfif>

											<cfelseif node.xmlAttributes.name eq "activityFiles">
												<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmFileUpload" currentValue="" label="" spanCols="yes">
													<cfoutput>
													<cf_relatedFile action="variables" entity="#FundRequestActivityID#" entitytype="fundRequestActivity">

													<p id="relatedFileCountMessage">There are #relatedFileVariables.filelist.recordCount# files associated with this record.</p>

													<ul>
														<cfloop query="relatedFileVariables.filelist">
															<li><A HREF="javascript:void(window.open('#replace(webhandle,"%","%25","ALL")#','UploadFileViewer','width=400,height=400,toolbar=1,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'))"> #htmleditformat(Description)# (#htmleditformat(filename)#)</A></li> <!--- 2015-10-26	ACPK	PROD2015-142 Encode percentage signs in webhandle --->
														</cfloop>
													</ul>

													<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityID=#fundRequestActivityID#&entityType=fundRequestActivity&replaceallowed=false")>
													<p><a href="javascript:void(window.open('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','UploadFiles','width=600,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'))" onMouseOver="window.status='';return true" onMouseOut="window.status='';return true">phr_#htmleditformat(phraseprefix)#Fund_UploadFilesToThisFundRequest</a></p>
													</cfoutput>
												</cf_relayFormElementDisplay>

											</cfif>
										</cfif>
									</cfif>

								</cfloop>
							</cfif>
					</cfif>

				<cfelseif node.xmlName eq "field">
					<cfset fieldAttributes = application.com.relayDisplay.getQueryAttributeForSelect(fieldAttributes=node.xmlattributes)>
					<cfset application.com.relayDisplay.displayFormField(fieldAttributes=fieldAttributes, entityID=fundRequestActivityID)>

				<!--- include custom file --->
				<cfelseif node.xmlName eq "includeFile">
					<cfset application.com.relayDisplay.includeFileForDisplay(node=node)>

				<!--- NJH 2015/09/25 - PROD2015-86 - include a screen --->
			 	<cfelseif node.xmlName eq "screen">
					<cfset useScreenByName = structKeyExists(node.xmlAttributes,"screenID")?node.xmlAttributes.screenID:"fundRequestActivityEdit_Custom">
					<cfset application.com.relayDisplay.showScreenElementsInDisplayXML(screenID=useScreenByName,entityID=form.fundRequestActivityID,countryID=0,entityDetails=fundRequestActivityDetailsStruct)>

				<cfelseif node.xmlName eq "embeddedCode">
					<cfif StructKeyExists(displayStruct[node.xmlAttributes.name],"type") and displayStruct[node.xmlAttributes.name].type neq "hidden">
						<cfif node.xmlAttributes.name eq "activityHistory">
							<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" spanCols="yes">
								<cfoutput><iframe src="fundRequestActivityHistory.cfm?fundRequestActivityID=#htmleditformat(fundRequestActivityID)#" width="100%"></iframe></cfoutput>
							</cf_relayFormElementDisplay>
						</cfif>
					</cfif>
				</cfif>

			</cfloop>

			<!--- BUTTONS --->
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="right">
				<!--- display the cancel buttons if on the external portal --->
				<cfif not request.relayCurrentUser.isInternal>
					<cf_relayFormElement relayFormElementType="button" fieldname="frmCancel" currentValue="phr_fund_cancel" label="" class="#submitBtnClass#" onClick="javascript:cancelbutton();">
				</cfif>
				<cfif variables.mode eq "add">
					<cfif application.com.relayFundManager.canAddActivity(lastSubmissionInterval=lastSubmissionInterval) or (lastSubmissionInterval eq 0)>
						<cf_relayFormElement relayFormElementType="submit" fieldname="frmAddActivity" currentValue="phr_fund_save" label="" class="#submitBtnClass#">	<!--- was phr_fund_add --->
						<cf_relayFormElement relayFormElementType="submit" fieldname="frmSaveAddNewActivity" currentValue="phr_fund_saveAndAddNew" label="" class="#submitBtnClass#">
					</cfif>
				<cfelse>
					<!--- if we're an approver or if the activity is editable, then display the save button --->
					<cfif ((qryFundActivity.canEditActivity) or (request.relayCurrentUser.isInternal)) and (qryFundActivity.AccountClosed neq 1) >
						<cf_relayFormElement relayFormElementType="submit" fieldname="frmEditActivity" currentValue="phr_fund_save" label="" class="#submitBtnClass#">
					</cfif>
				</cfif>
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="view" label="">
		</cf_relayFormDisplay>
	</cfform>

</cfif>

<cfif request.relayCurrentUser.isInternal>

</cfif>








<!--- some javascript functions etc needed when we're adding or editing --->
<cfif variables.mode neq "view">

	<cf_includejavascriptonce  template="/javascript/date.js">
	<cfinclude template="verifyBudgetPeriod.cfm">

	<!--- 2011/04/07	PPB	changed this list from getSetting("fundManager.approvalStatusIDList") to use the new table col isApproved --->
	<cfquery name="qryFundApprovalStatus" datasource="#application.siteDataSource#">
		select fundApprovalStatusID from fundApprovalStatus where isApproved = 1
	</cfquery>
	<cfset approvalStatusList= ValueList(qryFundApprovalStatus.fundApprovalStatusID)>

	<!--- 2011/05/23 PPB moved the cfoutput outside of the script cos Lex stg had cf admin setting enablecfoutputonly=yes --->
	<cfoutput>
	<script>
		var approvalStatusList='#approvalStatusList#';
		<cfif isdefined("qryFundActivity.fundApprovalStatusID") and qryFundActivity.fundApprovalStatusID neq ''>
			var origFundApprovalStatusID=#qryFundActivity.fundApprovalStatusID#;
		<cfelse>
			var origFundApprovalStatusID='';
		</cfif>
		<cfif isdefined("qryFundActivity.requestedAmount") and val(qryFundActivity.requestedAmount) gt 0>
			var origRequestedAmount=#qryFundActivity.requestedAmount#;
		<cfelse>
			var origRequestedAmount=0;
		</cfif>
		<cfif isdefined("qryFundActivity.approvedAmount") and val(qryFundActivity.approvedAmount) gt 0>
			var origApprovedAmount=#qryFundActivity.approvedAmount#;
		<cfelse>
			var origApprovedAmount=0;
		</cfif>



		function listFind (l,v,d){
			if(!d){d = ",";}
			var r = 0;
			var listToArray = l.split(d);
			for (var i=0; i < listToArray.length; i++){
				if (listToArray[i] == v){
					r = i + 1;
					break;
				}
			}
			return r;
		}

		function verifyForm(mode,editApproval) {
			<cf_translate>
			var form = document.fundRequestActivityForm;
			var errMsg = "";
			var confirmMsg = "";
			var warningMsg = "";
			var requestedAmount = 0;
			var requestedCoFunding = 0;
			var approvedAmount = 0;
			var approvedCoFundingAmount = 0;
			var returnValue = false;

			//if field left empty or with spaces parseFloat() will return NaN
			if (form.requestedAmount!=undefined && !isNaN(parseFloat(form.requestedAmount.value))) {
				requestedAmount = parseFloat(form.requestedAmount.value);
			}

			if (form.requestedCoFunding!=undefined && !isNaN(parseFloat(form.requestedCoFunding.value))) {
				requestedCoFunding = parseFloat(form.requestedCoFunding.value);
			}

			if (form.approvedAmount!=undefined && !isNaN(parseFloat(form.approvedAmount.value))) {
				approvedAmount = parseFloat(form.approvedAmount.value);
			}

			if (form.approvedCoFundingAmount!=undefined && !isNaN(parseFloat(form.approvedCoFundingAmount.value))) {
				approvedCoFundingAmount = parseFloat(form.approvedCoFundingAmount.value);
			}

			if (mode == 'add') {
				//if it's an organisation budget, we need to ensure that the requested amount doesn't exceed the budget allocated.
				if ((form.budgetType.value == 2) && (parseFloat(form.budgetAllocated.value) < parseFloat(form.requestedAmount.value))) {
					errMsg = errMsg+"phr_fund_TheAmountRequestedExceedsTheBudgetAvailableOf "+form.budgetAllocated.value+".\n";
				}
			}

			if (requestedCoFunding > 0 && form.CoFunder!=undefined && trim(form.CoFunder.value)==''  ){
				errMsg= errMsg+"phr_fund_YouMustSelectACoFunder"+".\n";
			}


			//if endDate is undefined we can assume that we are not in mode that allows date edits and so we don't need to do date validation
			if (form.endDate!=undefined) {
				startDateObj = form.startDate;
				startDateMask = startDateObj.getAttribute('datemask');
				endDateObj = form.endDate;
				endDateMask = endDateObj.getAttribute('datemask');

				startDate = getDateFromFormat(startDateObj.value,startDateMask);
				endDate = getDateFromFormat(endDateObj.value,endDateMask);

				if (compareDates(startDateObj.value,startDateMask,endDateObj.value,endDateMask)==1) {
					errMsg= errMsg+"phr_fund_PleaseSelectAnEndDateAfterTheStartDate."+"\n";
				}

				//2013-07-31 PPB Case 436260 added !editApproval condition so an activity for a previous period can be approved
				if (!editApproval) { // 2015-08-20 DAN    K-1443 - move this condition out of the parent IF statement so we can still compare the dates above to validate if start is before end
    				//determine whether user can create request based on dates given. (spanning quarters)
    				errMsg = errMsg+verifyBudgetPeriod(form.startDate.value,form.endDate.value,startDateMask);
                }
			}



			//if we're able to approve a request, verify that the amount approved is valid
			//test whether form fields are undefined in case they are being dispalyed as HTML
			//NB approvalStatusList should not include pending or rejected
			<cfif request.relayCurrentUser.isInternal and isApprover and variables.mode eq "edit">
			if (form.fundApprovalStatusID!=undefined) {
				if (!listFind(approvalStatusList,form.fundApprovalStatusID.value)){
					//if pending/rejected/...
					//2011/03/31 PPB LID6084 prevent entry of approved amount if approval status is Pending or Rejected
					if (approvedAmount > 0) {
						errMsg= errMsg+"phr_fund_YouCannotEnterAnApprovalAmountAtTheSelectedApprovalStatus"+".\n";
					}
				} else {
					//status is at approval level 1 or greater
					//if we aren't in mode to edit the approval section (ie read only) then don't do validation (possibly we could remove the save button for non-approvers instead)
					if (editApproval) {

//						if (approvedAmount != parseFloat(origApprovedAmount))		//2013-07-30 PPB Case 436388 check the amount vs budgetAvailable regardless of whether the amount has changed (see case notes)
//						{															//2013-07-30 PPB Case 436388
							//if the approved amount changes add the approved amount to the budgetallocated and check if they can borrow the new amount
							budgetafterreallocation = parseFloat(origApprovedAmount) + parseFloat(form.budgetAvailable.value);

							if (parseFloat(form.approvedAmount.value) > parseFloat(budgetafterreallocation)){
							errMsg = errMsg+"phr_fund_TheAmountApprovedMustBeLessThanOrEqualToTheBudgetRemaining"+".\n";}
//						}															//2013-07-30 PPB Case 436388
						if (parseFloat(form.budgetAvailable.value) == -1) {
							errMsg = errMsg+"phr_fund_YouCannotApproveTheActivityAsThereIsNoBudgetFromWhichToToDrawFunds"+".\n";
						} else if (approvedAmount <= 0) {
							errMsg= errMsg+"phr_fund_PleaseEnterTheAmountYouWishToApprove"+".\n";
						} else if (approvedAmount != parseFloat(origApprovedAmount) && form.fundApprovalStatusID!=undefined && form.fundApprovalStatusID.value == origFundApprovalStatusID ) {
							//PPB 2011/01/21 check that if the approval amount has been changed that the approval status has also changed
							errMsg= errMsg+"phr_fund_YouCannotChangeTheApprovalAmountWithoutChangingTheApprovalStatus"+".\n";
						} else if (requestedAmount > parseFloat(origRequestedAmount) && approvedAmount > 0) {
							//PPB 2011/01/21 check that if the requested amount has increased that you cant approve the request at the same time to prevent eg a level1 approver changing the requested amount and approving beyond his/her limit
							errMsg= errMsg+"phr_fund_YouMustSaveTheNewRequestedAmountBeforeYouCanApproveTheRequest"+".\n";
						} else {
							//the follow 2 messages can be reported together
							if (approvedAmount > requestedAmount) {
								errMsg = errMsg+"phr_fund_TheApprovedAmountIsMoreThanTheRequestedAmount"+".\n";
							}

							if (approvedCoFundingAmount > requestedCoFunding) {
								errMsg = errMsg+"phr_fund_TheApprovedCoFundingAmountIsMoreThanTheRequestedCoFundingAmount"+".\n";
							}
						}

						//NB. if there's an error don't bother checking for confirms/warnings
						if (errMsg == "") {

							if (approvedAmount < requestedAmount){
								confirmMsg= confirmMsg+"phr_fund_TheApprovedAmountIsLessThanTheRequestedAmount"+".\n";
							}

							if (approvedCoFundingAmount < requestedCoFunding){
								confirmMsg= confirmMsg+"phr_fund_TheApprovedCoFundingAmountIsLessThanTheRequestedCoFundingAmount"+".\n";
							}
						}
					}
				}
			}
			</cfif>

			<cf_include template="/code/CFTemplates/fundRequestActivity_VerifyForm_Extension.cfm" checkIfExists="true">		<!--- this will contain js validation --->

			//if we don't have an error message, submit the form if there are no confirm messages
			if (errMsg == ""){
				if (confirmMsg != ""){
					confirmMsg= "phr_sys_WARNING"+"\n\n"+confirmMsg+"\n"+"phr_sys_ClickOKtoContinue"+".\n";

					if (confirm(confirmMsg)) {
						returnValue = true;
					}
				} else {
					returnValue = true;
				}
			// if we have an error message, abort the form submission
			} else {
				alert(errMsg);
			}

			//if we've passed validation then show warning messages
			if (returnValue) {
				if (warningMsg != "") {
					alert("phr_WARNING"+"\n\n"+warningMsg);
				}
			}

			return returnValue;
		</cf_translate>
		}

		function displayTotalCost() {

			var theForm = document.fundRequestActivityForm;
			<!--- 2012/05/16 IH Case 427978 remove commas and white spaces from funding fields
				NJH 2016/01/21  - in here as I noticed an issue with total amount not getting displayed... removed the calls to unComma as they didn't appear to be necessary as the input field does not allow commas.
			 --->
			//theForm.requestedAmount.value = unComma(theForm.requestedAmount.value);

            // start: 2015-11-09 DAN Case 446455
			var proposedPartnerFunding = 0;
			if (theForm.proposedPartnerFunding!=undefined && !isNaN(parseFloat(theForm.proposedPartnerFunding.value))) {
                theForm.proposedPartnerFunding.value = theForm.proposedPartnerFunding.value;
                proposedPartnerFunding = Number(theForm.proposedPartnerFunding.value);
            }

            var requestedCoFunding = 0;
            if (theForm.requestedCoFunding!=undefined && !isNaN(parseFloat(theForm.requestedCoFunding.value))) {
                theForm.requestedCoFunding.value = theForm.requestedCoFunding.value;
                requestedCoFunding = Number(theForm.requestedCoFunding.value);
            }

			var totalCost = Number(theForm.requestedAmount.value) + proposedPartnerFunding + requestedCoFunding;			//using Number cf parseFloat cos it converts "" to 0
			var totalCostHTMLTDElement = jQuery('label[for="totalCost"]').next('p');
			if (!totalCostHTMLTDElement.length) {
                totalCostHTMLTDElement = jQuery('##totalCost_value');
            }
            // end: 2015-11-09 DAN Case 446455

			theForm.totalCost.value = totalCost;	// this is the hidden form field used for updating the db on submit

			if (totalCostHTMLTDElement!=null) {
				var totalCostHTMLTDElement = totalCostHTMLTDElement.html(totalCost);	//this is the html display; show 2 dps case 450128 23-06-2016 ESZ Cost of activity is not being calculated
			}
		}

		<!--- 2012/05/16 IH Case 427978 remove commas and white spaces from funding fields --->
		function unComma( val, dec )
		{
		    var newVal          = 0,
		        defaultVal      = 0,
		        dec             = (dec || 2);

		    if (!val) return defaultVal;
		    val = val.toString().replace(/,/g, '').replace(/\s/g, '');
		    if (!val) return defaultVal;
		    newVal = parseFloat(val);
		    if (isNaN(newVal)) return defaultVal;
		    return (newVal.toFixed(dec) || defaultVal);
		}

		function displayApprovedTotalCost() {
			var theForm = document.fundRequestActivityForm;

            // 2015-08-20 DAN    K-1443 - fix to only calculate the sum if the given value is a number
			var approvedTotalAmount = 0;
			if (theForm.approvedAmount.value && !isNaN(theForm.approvedAmount.value)) {
			    approvedTotalAmount += Number(theForm.approvedAmount.value);			//using Number cf parseFloat cos it converts "" to 0
			}

			if (theForm.approvedCoFundingAmount.value && !isNaN(theForm.approvedCoFundingAmount.value)) {
			    approvedTotalAmount += Number(theForm.approvedCoFundingAmount.value);
			}

			var approvedTotalAmountHTMLTDElement = document.getElementById('td_approvedtotalamount_value');

			if (approvedTotalAmountHTMLTDElement!=null) {
				approvedTotalAmountHTMLTDElement.innerHTML = approvedTotalAmount.toFixed(2);	//this is the html display; show 2 dps
			}
		}

		// START: 2011/05/09 - AJC - Issue 6431: Fund Manager - I can no longer select any dates as they are all grayed out in the request form.
		// NOTE: Lenovo doesn't call this fn but uses budgetGroupChangedLenovo() in fundRequestActivityDisplay_Extension.cfm instead (to update CURRENCY on the form too)
		function budgetGroupChanged(obj) {
			//2012-11-27 PPB Case 432120 reset the dates to prevent the user entering dates and then selecting another budget group (for which the dates are not valid)
			$('startDate').value = '';
			$('endDate').value = '';

			page = '/webservices/relayFundManagerWS.cfc?wsdl&method=getBudgetGroup'
			<cfif displayBudgetGroupAsHTML>
				page = page + '&budgetGroupID=#getFundRequestDetails.BudgetGroupID#';
			<cfelse>
				page = page + '&budgetGroupID=' + $('budgetGroupID').value;
			</cfif>

			<cfif mode eq "add" and request.relayCurrentUser.isInternal>
				page = page + '&organisationID=' + $('fundAccountOrganisationID').value;
			<cfelseif  mode eq "add" and not request.relayCurrentUser.isInternal>
				page = page + '&organisationID=#request.relayCurrentUser.organisationID#';
			<cfelseif mode eq "edit">
				page = page + '&organisationID=#getFundRequestDetails.organisationID#';
			</cfif>

			// changed to post to ensure proper refresh
			var myAjax = new Ajax.Request(
				page,
				{
					method: 'post',
					parameters: 'returnFormat=json&_cf_nodebug=true',
					evalJSON: 'force',
					debug : false,
					asynchronous: false,
					onComplete: function (requestObject,JSON) {
						json = requestObject.responseJSON

						/* START 2013-01-25 PPB Case 433111 now using a jQuery datepicker */
						jQuery("##startDate").datepicker( "option", "minDate", new Date(json[0].BUDGETPERIODSTARTDATE) );
						jQuery("##startDate").datepicker( "option", "maxDate", new Date(json[0].BUDGETPERIODENDDATE) );

						jQuery("##endDate").datepicker( "option", "minDate", new Date(json[0].BUDGETPERIODSTARTDATE) );
						jQuery("##endDate").datepicker( "option", "maxDate", new Date(json[0].BUDGETPERIODENDDATE) );
						/* END 2013-01-25 PPB Case 433111 */

						}
				});
		}
		// END: 2011/05/09 - AJC - Issue 6431: Fund Manager - I can no longer select any dates as they are all grayed out in the request form.

	</script>
	</cfoutput>

	<cf_include template="/code/CFTemplates/fundRequestActivity_Extension.cfm" checkIfExists="true">		<!--- this will contain js functions --->

</cfif>

<!--- 2014-11-12			RPW			CORE-852 A fund request approver cannot add a fund request in three pane view --->
<script language="JavaScript" type="text/javascript">
	function cancelbutton(){
		if (history.length > 1){
			history.go(-1);
		} else if (window.opener) {
			window.close();
		}
	}
</script>
