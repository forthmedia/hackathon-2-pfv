<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			BudgetGroupApprovers.cfm	
Author:				???
Date started:		??/??/????
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on


Possible enhancements:


 --->
<!--- save the content for the xml to define the editor --->

<cfparam name="screenTitle" default="">


<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Budget Group Approvers</cf_title>

<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">
<cfparam name="keyColumnURLList" type="string" default="">
<cfparam name="url.actiontype" type="string" default="">
<cfparam name="url.fundApproverID" type="string" default="">
<cfparam name="url.budgetgroupID" type="string" default="">
<!--- check to see if current user is an approver --->

<cfif url.actiontype is "assign">
	<cftry>
		<cfif isvalid('integer',url.fundApproverID) and isvalid('integer',url.BudgetGroupID)>
			
			<cfquery name="qry_ins_BudgetGroupApprover" datasource="#application.SiteDataSource#">
				insert into BudgetGroupApprover (budgetgroupID,fundapproverID)
				VALUES (<cf_queryparam value="#url.budgetgroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#url.fundApproverID#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</cfquery>
		
		</cfif>
		<cfcatch></cfcatch>
	
	</cftry>
	
<cfelseif actiontype is "remove">
	<cftry>
		<cfif isvalid('integer',url.fundApproverID) and isvalid('integer',url.BudgetGroupID)>
			
			<cfquery name="qry_del_BudgetGroupApprover" datasource="#application.SiteDataSource#">
				delete from BudgetGroupApprover 
				where budgetgroupID =  <cf_queryparam value="#url.budgetgroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and fundapproverID =  <cf_queryparam value="#url.fundApproverID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		
		</cfif>
		<cfcatch></cfcatch>
	
	</cftry>
</cfif>

<cfset keyColumnURLList = "BudgetGroupApprovers.cfm?BudgetGroupID=">

<cfif isvalid("integer",url.budgetgroupID)>
	<cfset url.budgetgroupID = round(url.budgetgroupID)>
<cfelse>
	<cfset url.budgetgroupID = 0>
</cfif>

<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->
<cfquery name="getBudgetGroup" datasource="#application.SiteDataSource#">
	select bg.BudgetGroupID, bg.description
	from budgetgroup bg
	where budgetgroupID =  <cf_queryparam value="#url.budgetgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>
<!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->
<cfset getBudgetGroupApprover = application.com.relayFundManager.getBudgetGroupApprover(budgetgroupID = url.budgetgroupID)>

<cfquery name="getBudgetGroupApprovers" datasource="#application.SiteDataSource#">
	select fundApproverID, Approver, ApproverLevel,approvalLevelPhrase, Countrydescription, personid, email, fundApproverID, 'remove' as removelink
	from vFundApprovers		
	where fundApproverID IN (select fundApproverID from BudgetGroupApprover where budgetgroupID =  <cf_queryparam value="#url.budgetgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	order by ApproverLevel,approvalLevelPhrase
</cfquery>

<cfquery name="getNonBudgetGroupApprovers" datasource="#application.SiteDataSource#">
	select fundApproverID, Approver, ApproverLevel,approvalLevelPhrase, Countrydescription, personid, email, fundApproverID, 'assign' as assignlink
	from vFundApprovers		
	where fundApproverID NOT IN (select fundApproverID from BudgetGroupApprover where budgetgroupID =  <cf_queryparam value="#url.budgetgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by ApproverLevel,approvalLevelPhrase
</cfquery>

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_BudgetGroupApprovers for #htmleditformat(getBudgetGroup.description)#")>

<cfoutput>
<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<div id="actionBar">
	<ul>
		<li><a href="/funds/reportBudgetGroupApprovers.cfm" class="Submenu">Back</a></li>
	</ul>
</div>

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<table border="0" cellspacing="1" cellpadding="3" width="100%" class="withBorder">
	<tr>
		<td>
			<p>#request.CurrentSite.Title# users who have user rights to approve #htmleditformat(getBudgetGroup.description)# budget group Fund Requests.<br></p>
		</td>
	</tr>
</table>
<table border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td>&nbsp;</td>
		<td><h2>Current #htmleditformat(getBudgetGroup.description)# approvers</h2></td>
	</tr>
</table>
</cfoutput>
<cf_translate>
	<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->
	<cf_tablefromqueryobject 
		queryObject="#getBudgetGroupApprovers#"
		queryName="getBudgetGroupApprovers"
		sortOrder = "ApproverLevel,approvalLevelPhrase"
		
		keyColumnList="removelink"
		keyColumnURLList="BudgetGroupApprovers.cfm?BudgetGroupID=#getBudgetGroup.budgetgroupID#&actiontype=remove&fundApproverID="
		keyColumnKeyList="fundApproverID"
		
		hideTheseColumns="fundApproverID"
		showTheseColumns="Approver,ApproverLevel,approvalLevelPhrase,Countrydescription,PersonID,Email,removelink"

		columnTranslation="true"
		
		allowColumnSorting="yes"
	><!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->
</cf_translate>
<cfoutput>
<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<table border="0" cellspacing="1" cellpadding="3">
	<tr>
		<td>&nbsp;</td>
		<td><h2>Assign approvers to #htmleditformat(getBudgetGroup.description)# budget group</h2></td>
	</tr>
</table>
</cfoutput>
<cf_tablefromqueryobject 
		queryObject="#getNonBudgetGroupApprovers#"
		queryName="getNonBudgetGroupApprovers"
		sortOrder = "ApproverLevel,approvalLevelPhrase"
		
		keyColumnList="assignlink"
		keyColumnURLList="BudgetGroupApprovers.cfm?BudgetGroupID=#getBudgetGroup.budgetgroupID#&actiontype=assign&fundApproverID="
		keyColumnKeyList="fundApproverID"
		
		hideTheseColumns="fundApproverID"
		showTheseColumns="Approver,ApproverLevel,approvalLevelPhrase,Countrydescription,PersonID,Email,assignlink"

		columnTranslation="true"
		allowColumnSorting="yes"
	>
