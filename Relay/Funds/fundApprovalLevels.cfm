<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			Funds/fundApprovalLevels.cfm
Author:				NYB
Date started:		2010-10-26

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010-10-26 			NYB 		Created to give the user an interface where they can select a country from a dropdown - instead of putting in the countryid
2010/11/19			PPB			added Approval Level which was missed in conversion
2010-11-23			PPB			added edit and delete buttons
2011/05/20			PPB			LID6654 translations + duplicate Add button
2014-10-15			RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:
encrypt the delete url

 --->

<cf_title>Fund Approval Levels</cf_title>
<cfset xmlSource = "">
<cfset getApprovalList = "">

<cfparam name="sortOrder" default="fundApprovalLevelid">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<!--- 2010/11/24 PPB delete functionality  --->
<cfif structKeyExists(url,"delete") and url.delete>
	<cfquery name="qryDeleteFundApprovalLevel">
		DELETE FROM FundApprovalLevel WHERE FundApprovalLevelID =  <cf_queryparam value="#url.FundApprovalLevelID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
</cfif>

<cfif structKeyExists(url,"editor")>
	<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="236" name="FundApprovalLevel" entity="fundApprovalLevel" title="phr_fund_FundApprovalLevels">
			<field name="fundApprovalLevelID" label="phr_fund_fundApprovalLevelID" control="html" description="Unique Key."/>
			<field name="lowerApprovalThreshold" label="phr_fund_lowerApprovalThreshold" description="Lower approval threshold of level" range="1,"/>
			<field name="upperApprovalThreshold" label="phr_fund_upperApprovalThreshold" description="Upper approval threshold of level" range="0,"/> <!--- allow 0 as it doensn't look like it's a required field, and it seems to be default to 0 --->
			<field name="countryID" label="phr_Country" description="Country this level relates to" control="select" query="SELECT countryID as value, countryDescription as display, 2 as sortindex from country where countryid not in (37) union select 37 as value, 'All Countries' as display, 1 as sortindex from country order by sortindex,countryDescription"></field>
			<field name="approvalLevel" label="phr_ApprovalLevel" required="true" description="Approval Level"  range="1,"/>		<!--- PPB 2010/11/19 added Approval Level --->
			<field name="ApprovalLevelPhrase" label="phr_fund_ApprovalLevelPhrase" description="The approval level phrase" />
		</editor>
	</editors>
	</cfoutput>
	</cfsavecontent>
<cfelse>
	<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_FundApprovalLevels")>

	<cf_head>
		<script type="text/javascript">
			function deleteFundApprovalLevel(thisurlkey) {
				if (confirm('phr_fund_DeleteFundApprovalLevel')) {
					//strip the existing url params off the url and replace them with the delete params
					window.location.href = window.location.href.split('?')[0] + '?delete=yes&fundApprovalLevelID='+thisurlkey
				}
			}
		</script>
	</cf_head>

	<cfquery name="getApprovalList" datasource="#application.SiteDataSource#">
		select f.*,CountryDescription as Country,'<img border="0" alt="Edit" src="/images/MISC/iconEditPencil.gif">' AS editLink,'<img border="0" alt="Expire Login" src="/images/MISC/iconDeleteCross.gif">' AS deleteLink
		from fundApprovalLevel f
		inner join country c on f.countryid=c.countryid
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

<CF_listAndEditor
	xmlSource="#xmlSource#"
	queryData="#getApprovalList#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	showTheseColumns="fundApprovalLevelID,lowerApprovalThreshold,upperApprovalThreshold,Country,ApprovalLevel,ApprovalLevelPhrase,EditLink,DeleteLink"			<!--- PPB 2010/11/19 added ApprovalLevel & links --->

	keyColumnList="editLink,deleteLink"
	keyColumnURLList="fundApprovalLevels.cfm?editor=yes&fundApprovalLevelID=,javascript:deleteFundApprovalLevel(thisurlkey)"
	keyColumnKeyList="fundApprovalLevelId,fundApprovalLevelId"
	useInclude="false"
	columnTranslation="true"
	columnTranslationPrefix="phr_fund_"
	allowColumnSorting="yes"
>