<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			budgets.cfm
Author:				NJH
Date started:		2007-06-14
Description:

Amendment History:

Date 				Initials 	What was changed
2008-08-18			NJH			CR-SNY651 - changed hard-coded references to http to a variable that gets set to http if cgi.HTTPS is not "on"; else https
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
2004-11-24			PPB			Solved problem with (1) dropdowns not working and (2) the wrong showCols being sent to TFQO
2011/04/01			PPB			switch query to CFC method for easy hook for custom
2011/04/05 			PPB 		LID6119 - translate error message
2011/04/08 			NAS 		LID6151  sort order
2011/04/11 			PPB 		LID6151  set default sort order
2011/04/14 			PPB			LEX052  added check against insufficient BudgetGroupPeriod Funds (and revamped to use bind selects not mxajax.select while at it)
2011/04/21 			PPB 		LEX052 LID6378
2011/04/21 			PPB 		LEX052 LID6376
2011/04/21 			PPB 		LEX052 clear down Budget Group totals when BudgetGroup or Budget changes
2011/04/21 			PPB 		LEX052 LID6377
2011/05/25			PPB			LEX052 put <cfoutput></cfoutput> around javascript cos it wasn't rendering on Lex Stg (ok on dev)
2012/07/06			IH			Case 429290 fix web service path
2012-11-08 			PPB 		Case 431507 avoid counting the amount of the budget currently being edited twice in the validation calculation
2016/01/19			NJH			JIRA 164 - removed cfform and use callwebservice.. cleaned up code a bit.
2016/01/25			NJH			Moved js to js file and changed the selectors to look at new elements.
Possible enhancements:


 --->

<cfparam name="budgetPeriodAllocationID" type="numeric" default="1">
<cfparam name="mode" type="string" default="view">
<cfparam name="sortOrder" default="budgetPeriodAllocationID">		<!--- 2011/04/11 PPB LID6151 set default --->
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfset showCols="BudgetGroup,Budget,BudgetType,EntityName,BudgetPeriod,Amount,budgetRemaining,allocationAuthorised">
<cf_includeJavascriptOnce template="/funds/js/budgets.js">

<!---
<!--- PPB 2010/11/24 PPB created variable authoriseBudgetTypes cos calling getsetting in-line in init2() seemed to insert a newline char and cause a js error  --->
<cfset authoriseBudgetTypes=application.com.settings.getSetting("fundManager.authoriseBudgetTypes")> <!--- WAB 2010/11/01 moved to settings to fix bug, but not sure what I'm doing though #request.authoriseBudgetTypes# --->
 --->
<cfset editBudgetSecurityLevel = "fundTask:Level3">
<cfset viewBudgetSecurityLevel = "fundTask:Level2">

<cfset functionListQuery = queryNew("")>

<cfif application.com.login.checkInternalPermissions(listfirst(viewBudgetSecurityLevel,":"),listlast(viewBudgetSecurityLevel,":"))>

	<cfif application.com.login.checkInternalPermissions(listfirst(editBudgetSecurityLevel,":"),listlast(editBudgetSecurityLevel,":"))>
		<cfset showCols = showCols & ",Edit">

		<cfinclude template="budgetFunctions.cfm">
		<cfset functionListQuery = comTableFunction.qFunctionList>
	</cfif>
	<cfif structKeyExists(form,"mode")>
		<cfset mode = form.mode>
	<cfelseif structKeyExists(url,"mode")>
		<cfset mode = url.mode>
	</cfif>

<!--- 	<cfset request.relayFormDisplayStyle="HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm"> --->

	<cfif structKeyExists(form,"frmAddBudgetAllocation")>
		<cfset application.com.relayFundManager.addBudgetPeriodAllocation(budgetID=frmBudgetID,budgetPeriodID=frmBudgetPeriodID,allocatedAmount=frmBudgetAmount,authorised=frmBudgetAuthorisation)>
	<cfelseif structKeyExists(form,"frmChangeBudgetAllocation")>
		<cfset application.com.relayFundManager.changeBudgetPeriodAllocation(budgetPeriodAllocationID=frmBudgetPeriodAllocationID,amount=frmBudgetAmount)>
	</cfif>

	<cf_title>Budgets</cf_title>
		<cfif mode eq "view">

			<!--- 2011/04/01 PPB LID6101 moved query into CFC so Lenovo ver can easily override it --->
			<cfset getBudgetPeriodAllocations = application.com.relayFundManager.getBudgetPeriodAllocations()>

			<!--- 2011/04/08 NAS LID6151  sort order --->
			<cfquery name="getBudgetPeriodAllocations" dbtype="query">
				select * from getBudgetPeriodAllocations
				order by <cf_queryObjectName value="#sortOrder#">
			</cfquery>

			<cf_tableFromQueryObject
				queryObject="#getBudgetPeriodAllocations#"
				queryName="getBudgetPeriodAllocations"
				sortOrder = "#sortOrder#"
				numRowsPerPage="#numRowsPerPage#"
				startRow="#startRow#"
				columnTranslation="true"
				columnTranslationPrefix="phr_fund_"

				showTheseColumns="#showCols#"
			 	hideTheseColumns=""
				useInclude="false"
				currencyFormat="amount,budgetRemaining"
				rowIdentityColumnName="budgetPeriodAllocationID"

				keyColumnList="Edit"
				keyColumnURLList="budgets.cfm?mode=edit&budgetPeriodAllocationID="
				keyColumnKeyList="budgetPeriodAllocationID"
				keyColumnOpenInWindowList="no"

				FilterSelectFieldList="BudgetGroup,BudgetPeriod,BudgetType,EntityName"
				FilterSelectFieldList2="BudgetGroup,BudgetPeriod,BudgetType,EntityName"

				functionListQuery="#functionListQuery#"
			>

		<cfelseif mode eq "add">

			<cfscript>
				BudgetGroupsQry = application.com.relayFundManager.getBudgetGroups();
			</cfscript>

			<cfquery name="getBudgetGroups" dbtype="query">
				select BudgetGroupID,description, BudgetGroupTypeID from BudgetGroupsQry
				union
				select cast(0 as integer) as BudgetGroupID,'phr_pleaseSelectABudgetGroup' as description,0 as BudgetGroupTypeID from BudgetGroupsQry
				order by BudgetGroupID
			</cfquery>

			<!--- 2010/11/24 PPB there were 2 sets of cf_relayFormDisplay which caused nested TABLES which meant the DOM model got confused and the ajax population of the dropdowns didn't work --->
			<!--- 2011/04/21 PPB LID6377 added action otherwise after editing a record the GO button of the TFQO filters (which fires queryString) re-directed the user back to the edited record --->
			<form name="addBudgetForm" id="addBudgetForm" method="post" novalidate="true" onSubmit="return verifyForm('addBudgetForm');">
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmBudgetGroupID" id="frmBudgetGroupID" currentValue="" label="phr_fund_selectABudgetGroup" query="#getBudgetGroups#" value="BudgetGroupID" display="description" onChange="budgetGroupChanged()">
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmBudgetID" id="frmBudgetID" currentValue="" label="phr_fund_budget" bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName='relayFundManagerWS',methodName='getBudgets_Dropdown',budgetGroupID={frmBudgetGroupID})" bindOnLoad="false" value="dataValue" display="displayValue" required="yes" onChange="budgetChanged()" nullText="phr_fund_pleaseSelectABudget">
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmBudgetPeriodID" id="frmBudgetPeriodID" currentValue="" label="phr_fund_budgetPeriod" bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName='relayFundManagerWS',methodName='getBudgetPeriods_Dropdown',budgetID={frmBudgetID})" bindOnLoad="false" value="dataValue" display="displayValue" onChange="budgetPeriodChanged()" required="yes" nullText="phr_fund_pleaseSelectABudgetPeriod">

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmount" currentValue="" label="phr_fund_budgetGroupPeriodAllocationAmount">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountAuthorised" currentValue="" label="phr_fund_budgetGroupPeriodAllocationAmountAuthorised">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountNotAuthorised" currentValue="" label="phr_fund_budgetGroupPeriodAllocationAmountNotAuthorised">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountRemaining" currentValue="" label="phr_fund_budgetGroupPeriodAllocationAmountRemaining" makeHidden="true">

					<cf_relayFormElementDisplay relayFormElementType="numeric" fieldName="frmBudgetAmount" currentValue="" label="phr_fund_budgetAllocation" required="yes">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldName="frmBudgetAmount_orig" currentValue="0" label="">					<!--- 2012-11-08 PPB Case 431507 --->
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetAuthorisation" currentValue="0" label="">
					<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmAddBudgetAllocation" currentValue="phr_fund_addBudgetAllocation" label="">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="view" label="">
				</cf_relayFormDisplay>
			</form>

		<cfelseif mode eq "edit">

			<!--- 2011/04/01 PPB LID6101 moved this into CFC so Lenovo ver can easily override it --->
			<cfset getBudgetPeriodAllocations = application.com.relayFundManager.getBudgetPeriodAllocations(budgetPeriodAllocationID=budgetPeriodAllocationID)>

			<cfset getBudgetGroupPeriodAllocations = application.com.relayFundManager.getBudgetGroupPeriodAllocations(budgetGroupID=getBudgetPeriodAllocations.budgetGroupID, budgetPeriodID=getBudgetPeriodAllocations.budgetPeriodID)>

			<!--- 2011/04/21 PPB LID6377 added action otherwise after editing a record the GO button of the TFQO filters (which fires queryString) re-directed the user back to the edited record --->
			<form name="changeBudgetForm" id="changeBudgetForm" method="post" novalidate="true" onSubmit="return verifyForm('changeBudgetForm');">
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetPeriodAllocationID" currentValue="#budgetPeriodAllocationID#" label="">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroup" currentValue="#getBudgetPeriodAllocations.BudgetGroup#" label="phr_fund_selectABudgetGroup">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudget" currentValue="#getBudgetPeriodAllocations.budget#" label="phr_fund_budget">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetPeriod" currentValue="#getBudgetPeriodAllocations.budgetPeriod#" label="phr_fund_budgetPeriod" >

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmount" currentValue="#getBudgetGroupPeriodAllocations.amount#" label="phr_fund_budgetGroupPeriodAllocationAmount">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountAuthorised" currentValue="#getBudgetGroupPeriodAllocations.amountAuthorised#" label="phr_fund_budgetGroupPeriodAllocationAmountAuthorised">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountNotAuthorised" currentValue="#getBudgetGroupPeriodAllocations.amountNotAuthorised#" label="phr_fund_budgetGroupPeriodAllocationAmountNotAuthorised">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroupPeriodAllocationAmountRemaining" currentValue="#getBudgetGroupPeriodAllocations.amountRemaining#" label="phr_fund_budgetGroupPeriodAllocationAmountRemaining" makeHidden="true">

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="budgetApproved" currentValue="#getBudgetPeriodAllocations.budgetApproved#" label="phr_fund_frmBudgetApproved">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetApproved" currentValue="#getBudgetPeriodAllocations.budgetApproved#" label="">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetRemaining" currentValue="#getBudgetPeriodAllocations.budgetRemaining#" label="phr_fund_budgetRemaining">
					<cf_relayFormElementDisplay relayFormElementType="numeric" fieldName="frmBudgetAmount" currentValue="#getBudgetPeriodAllocations.amount#" label="phr_fund_budgetAllocation" required="yes">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldName="frmBudgetAmount_orig" currentValue="#getBudgetPeriodAllocations.amount#" label="">		<!--- 2012-11-08 PPB Case 431507 --->

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmLastUpdated" currentValue="#DateFormat(getBudgetPeriodAllocations.lastUpdated,'dd mmm yyyy') & '&nbsp;&nbsp;&nbsp;' & TimeFormat(getBudgetPeriodAllocations.lastUpdated,'hh:mm')#" label="phr_sys_LastUpdated">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmLastUpdatedByFullName" currentValue="#getBudgetPeriodAllocations.lastUpdatedByFullName#" label="phr_sys_LastUpdatedBy">

					<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmChangeBudgetAllocation" currentValue="phr_fund_changeBudgetAllocation" label="">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="view" label="">
				</cf_relayFormDisplay>
			</form>

		</cfif>
<cfelse>
	You do not have sufficient permissions to view the budgets.
</cfif>