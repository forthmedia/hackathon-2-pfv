<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		verifyBudgetPeriod.cfm
Author:			NH
Date started:
Description:

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-10-04 			PPB 		Case 437264 use spanQuarterAllowance in WHERE clause so requests for the previous period can be entered
2015-09-15			ACPK		FIFTEEN-400 Added different error message for when no valid periods exist for selected budget group

Enhancements still to do:


 --->

<!--- START 2013-10-04 PPB Case 437264 use spanQuarterAllowance in WHERE clause so requests for the previous period can be entered --->
<cfset spanQuarterAllowance = application.com.settings.getSetting("fundManager.spanQuarterAllowance") >


<cfquery name="getBudgetPeriods" datasource="#application.siteDataSource#">
	select convert(varchar(10),startDate, 111) as startDate, convert(varchar(10),endDate,111) as endDate,
		convert(varchar(10),dateAdd(d,#spanQuarterAllowance#,endDate),111) as allowedEndDate from budgetPeriod
	where periodTypeID =  <cf_queryparam value="#application.com.settings.getSetting("fundManager.budgetPeriodType")#" CFSQLTYPE="CF_SQL_INTEGER" >  and
		dateDiff(d,GetDate(),dateAdd(d,#spanQuarterAllowance#,endDate)) > 0
</cfquery>
<!--- START 2013-10-04 PPB Case 437264 --->

<cf_translate phrases="Phr_fund_DatesFallOutsideValidBudgetPeriod"/>

<cf_includejavascriptonce  template="/javascript/date.js">
<cf_includejavascriptonce  template="/javascript/cf/wddx.js">
<cfoutput>
	<SCRIPT type="text/javascript">

	<cfwddx action=cfml2js input = #getBudgetPeriods# toplevelvariable = "period">
	<cfwddx input="#Phr_fund_DatesFallOutsideValidBudgetPeriod#" toplevelvariable="invalidDateRange" action="CFML2JS">
	function verifyBudgetPeriod(startDateValue,endDateValue,dateMask)  {
		validPeriod = false;
		msg = invalidDateRange;

		if (startDateValue != '' && endDateValue != '') {
			for (i=0; i<=period.startdate.length-1; i++)  {
				if ((compareDates(period.startdate[i],'yyyy/MM/dd',startDateValue,dateMask) == 0)  && (compareDates(period.allowedenddate[i],'yyyy/MM/dd',endDateValue,dateMask) == 1)) {
					validPeriod = true;
					msg = '';
					break;
				}
			}
		}

		if (!validPeriod) {
			/* 2015-09-15			ACPK		FIFTEEN-400 Added different error message for when no valid periods exist for selected budget group */
			if (period.startdate.length != 0) {
				msg = invalidDateRange +'\nValid periods are:\n';
				for (i=0; i<=period.startdate.length-1; i++)  {
					msg = msg + period.startdate[i] + ' to ' + period.allowedenddate[i]+'\n';
				}
			} else {
				msg = 'phr_fund_NoValidPeriodsForBudgetGroup.';
			}
		}

		return msg
	}
	</script>
</cfoutput>


