<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		fundRequestActivityDisplay.cfm
Author:			PPB
Date started:	2010/11/25

Description:	A file to work out what fields should be displayed and how.

Amendment History:

Date 	   Initials 	What was changed
2011/04/01 PPB          renamed included file
2011/04/07 PPB			moved initial assignment of editActivity to avoid error when adding a new activity
2011/04/19 PPB 			LEX052 START opened up to allow a finance Approver to get a dropdown (not HTML) for ALL statuses of Fully Approved or above
2012/06/11 IH 			Case 428372 Make rejected claim current status changeble back to pending
2014/11/06 DTR			442272 - MDF: Budget Group dropdown on MDF Request Form portal
2015-02-24	RPW			FIFTEEN-222 - Error thrown whem trying to add a fund request for person
2015-08-20 DAN          K-1443 - minor tweak to display star symbol with 'required fields' text

Possible enhancements:


Notes:

if fundRequestID eq 0 we have clicked the add activity button directly from the list of fund requests (fundrequests.cfm)
otherwise we have clicked through to view activities (fundRequestActivityV2.cfm) in which case we know which fundRequest it is

 --->

<cfset displayStruct = structNew()>
<cfset fundRequestActivityReadOnly = false> <!--- lock down the details of fundRequestActivity if... --->

<cfset displayBudgetGroupAsHTML = false>
<cfset displayBudgetGroupAsHTMLinAddMode = false> <!--- display budgetgroup as HTML(text) when budget group have only 1 record in Add mode--->

<!--- initialising the display structure with values from the DB --->
<cfloop list="#structKeyList(fundRequestActivityDetailsStruct)#" index="fieldname">
	<cfset displayStruct[fieldname] = structNew()>
	<cfset displayStruct[fieldname].currentValue = fundRequestActivityDetailsStruct[fieldname]>
	<cfset displayStruct[fieldname].readonly = fundRequestActivityReadOnly>
</cfloop>

<cfset displayStruct.totalCost.value = displayStruct.totalCost.currentvalue>				<!--- must set the value for the makeHidden attribute to work --->
<cfset displayStruct.budgetAvailable.value = displayStruct.budgetAvailable.currentvalue>	<!--- must set the value for the makeHidden attribute to work --->

<cfset editApproval= false>	<!--- 2011/04/07 PPB moved this assignment outside of cfif so it is defined wghen adding a new activity --->

<cfif mode eq "add">
	<cfset displayStruct.fundRequestActivityID.currentValue = 0>

	<cfif request.relayCurrentUser.isInternal>
		<cfset displayStruct.partnerOrganisationID.currentvalue = url.mdfpartnerorganisationid>
		<cfset displayStruct.partnerOrganisationName.currentvalue = application.com.relayPLO.getOrgDetails(url.mdfpartnerorganisationid).organisationName>
		<cfset displayStruct.partnerOrganisationName.relayFormElementType = "html">
	<cfelse>
		<cfset displayStruct.partnerOrganisationID.currentvalue = request.relayCurrentUser.person.organisationID>
		<!--- <cfset displayStruct.partnerOrganisationName.currentvalue = application.com.relayPLO.getOrgDetails(request.relayCurrentUser.person.organisationID).organisationName> --->
	</cfif>

	<cfif (isDefined("fundRequestID") and fundRequestID neq 0)>								<!--- user has listed activities for a fund request then clicked add (@ Jan 2011 portal only) --->
		<!--- <cfset displayStruct.accountID.currentvalue = #getFundRequestDetails.accountID#> --->
		<cfset displayStruct.budgetAllocated.currentvalue = #getFundRequestDetails.budgetAllocated#>
		<cfset displayStruct.budgetType.currentvalue = #getFundRequestDetails.budgetType#>
		<cfset displayStruct.fundAccountOrganisationID.currentvalue = #getFundRequestDetails.organisationId#>
		<cfset displayStruct.fundAccountOrganisationID.relayFormElementType = "html">

		<cfset displayBudgetGroupAsHTML = true>
		<cfset displayStruct.budgetGroupID.currentvalue = #getFundRequestDetails.BudgetGroupID#>
	<cfelse>
		<cfif request.relayCurrentUser.isInternal and isApprover>
			<!--- 2015-02-24	RPW	FIFTEEN-222 - organisationID defaulted to blank above, set value --->
			<cfset displayStruct.fundAccountOrganisationID.currentvalue = request.relayCurrentUser.organisationID>
			<cfset displayStruct.fundAccountOrganisationId.relayFormElementType = "select">
		<cfelse>
			<cfset displayStruct.fundAccountOrganisationID.currentvalue = displayStruct.partnerOrganisationId.currentvalue>
			<cfset displayStruct.fundAccountOrganisationID.relayFormElementType = "html">
 		</cfif>

		<!--- 442272 - MDF: Budget Group dropdown on MDF Request Form portal --->
		<cfset budgetGroup= application.com.relayFundManager.getBudgetGroupsForOrganisation(organisationID=#displayStruct.fundAccountOrganisationID.currentvalue#)>
		<cfif budgetGroup.recordCount eq 1>
			<cfset displayBudgetGroupAsHTMLinAddMode=true>
			<cfset displayStruct.budgetGroupID.currentvalue = #budgetGroup.budgetGroupID#>
		</cfif>
	</cfif>

	<cfset displayStruct.budgetGroupID.label="phr_fund_BudgetGroup#phraseSuffix#">

	<cfset displayStruct.startDate.disableFromDate="#disableFromDate#">
	<cfset displayStruct.startDate.disableToDate="#disableToDate#">
	<cfset displayStruct.endDate.disableFromDate="#disableFromDate#">
	<cfset displayStruct.endDate.disableToDate="#disableToDate#">

	<cfset displayStruct.Approval.type = "hidden" >					<!--- groups defined in the XML are displayed by default --->

	<cfset displayStruct.message_requiredFields.relayFormElementType = "html">
	<cfset displayStruct.message_requiredFields.currentValue="<label class='required'>phr_RequiredFields</label>"> <!--- 2015-08-20 DAN K-1443 - minor tweak to display star symbol with 'required fields' text --->
	<!---  spanCols="yes"  --->
<!---
	<cfset displayStruct.CoFunder.bindOnLoad = false>
 --->
<cfelseif mode eq "edit">
	<!--- 2007/08/24 SSS - show the amount approved as a text box if the activity is still approvable --->
	<!--- <cfif (CurrentUserApproverLevelQry.ApproverLevel GT fundRequestapprovalStatusQry.levelAvailabilty) or (fundRequestapprovalStatusQry.levelAvailabilty EQ "")>
	<cfset availableStatusList = valueList(fundRequestStatusQry.fundApprovalStatusID)>
	<cfloop list="#request.approvalStatusIDList#" index="approvalStatusID">
		<cfif listContains(availableStatusList,approvalStatusID) and approvalStatusID neq qryFundActivity.fundApprovalStatusID>
			<cfset editApproval = true>
			<cfbreak>
		</cfif>
	</cfloop>
	</cfif> --->
	<!--- if the claim approver comes in a fully approved allow the activity to be editable--->

	<cfif application.com.relayFundManager.isBudgetGroupApprover(budgetgroupID=qryFundActivity.budgetgroupID,organisationID=qryFundActivity.fundAccountOrganisationId)>
		<cfif CurrentUserApproverLevelQry.ApproverLevel eq qryFundActivity.currentApprovalLevel>
			<cfset editApproval = true>
			<cfif CurrentUserApproverLevelQry.ApproverLevel eq 1>
				<cfset editMode=true>
			<cfelse>
				<cfset editMode=false>
			</cfif>
		<cfelse>
			<cfset editMode=false>
		</cfif>

	<cfelse>
		<cfset editApproval = false>
		<cfif isManager and qryFundActivity.currentApprovalLevel is 1>
			<cfset editMode = true>
		<cfelse>
			<cfset editMode = false>
		</cfif>
	</cfif>

	<!--- p-lex039 once the claim goes fully approved allow the channel manager in to move the mdf to claim status and put in documants --->
	<cfset getclaimApprover = application.com.flag.getFlagData(flagId="ClaimApprover", entityID=#request.relayCurrentUser.personID#)>
	<cfif #qryFundActivity.FundStatusTextID# EQ "approved" and getclaimApprover.recordcount NEQ 0>
		<cfset editApproval = true>
	</cfif>

	<!--- 2011/04/19 PPB LEX052 START opened this up to allow a finance Approver to get a dropdown (not HTML) for ALL statuses of Fully Approved or above  --->
	<!--- <cfif #qryFundActivity.FundStatusTextID# EQ "claimapproval" and financeApprover> --->
	<cfif application.com.relayFundManager.getFundApprovalStatusByTextID(qryFundActivity.FundStatusTextID).isFullyApproved and financeApprover>
		<cfset editApproval = true>
	</cfif>
	<!--- 2011/04/19 PPB LEX052 END --->

	<cfset displayStruct.fundAccountOrganisationID.relayFormElementType = "html">

	<cfset displayStruct.fundManagerId.currentvalue = #fundManager.personID#>
	<cfset displayStruct.fundManager.currentvalue = #fundManagerDetails#>
	<cfset displayStruct.fundManager.relayFormElementType ="html">

	<!--- if the activity status denies us from editing the activity, show it (or at least most of it) as HTML --->
	<cfif not editMode>
		<cfset CampaignName = application.com.relayFundManager.getSelectedMarketingCampaign(MarketingCampaignID=qryFundActivity.MarketingCampaignID)>

		<cfset displayBudgetGroupAsHTML = true>

		<cfset displayStruct.statusText.relayFormElementType = "html" >
		<cfset displayStruct.fundRequestTypeID.relayFormElementType = "html" >

		<cfset displayStruct.typeDescription.relayFormElementType = "html" >

		<cfset displayStruct.startDate_displayOnly.currentvalue = DateFormat(displayStruct.startDate.currentvalue, "long")>
		<cfset displayStruct.startDate_displayOnly.relayFormElementType = "html" >
		<cfset displayStruct.startDate.relayFormElementType = "hidden" >

		<cfset displayStruct.endDate.currentvalue = DateFormat(displayStruct.endDate.currentvalue, "long")>
		<cfset displayStruct.endDate.relayFormElementType = "html" >

		<cfset displayStruct.budgetPeriod.relayFormElementType = "html" >

		<cfset displayStruct.useMarketingMaterials.relayFormElementType = "html" >
		<cfset displayStruct.useMarketingMaterials.currentvalue = "#iif(displayStruct.useMarketingMaterials.currentvalue eq 1,DE('Yes'),DE('No'))#">

		<cfset displayStruct.marketingCampaignID.relayFormElementType = "html" >

		<cfset displayStruct.totalCost.noteTextImage = false >
		<cfset displayStruct.totalCost.noteText = "" >

		<cfset displayStruct.proposedPartnerFunding.relayFormElementType = "html" >
		<cfset displayStruct.proposedPartnerFunding.noteTextImage = false >
		<cfset displayStruct.proposedPartnerFunding.noteText = "" >

		<cfset displayStruct.requestedAmount.relayFormElementType = "html" >
		<cfset displayStruct.requestedAmount.makeHidden = "true" >				<!--- form vble needed for validation --->
		<cfset displayStruct.requestedAmount.noteTextImage = false >
		<cfset displayStruct.requestedAmount.noteText = "" >

		<cfset displayStruct.requestedCoFunding.relayFormElementType = "html" >
		<cfset displayStruct.requestedCoFunding.makeHidden = "true" >			<!--- form vble needed for validation --->
		<cfset displayStruct.requestedCoFunding.noteTextImage = false >
		<cfset displayStruct.requestedCoFunding.noteText = "" >

		<cfset displayStruct.CoFunder.relayFormElementType = "html" >
		<cfset displayStruct.CoFunder.makeHidden = "true" >

		<cfset displayStruct.roi.relayFormElementType = "html" >
		<cfset displayStruct.roi.noteTextImage = false >
		<cfset displayStruct.roi.noteText = "" >

		<!--- approvedAmount is displayed in 2 positions on screen, hence approvedAmount2 --->
		<cfset displayStruct.approvedAmount2.relayFormElementType = "html">
		<cfset displayStruct.approvedAmount2.currentvalue = displayStruct.approvedAmount.currentvalue>

		<cfset displayStruct.bankDetails.type = "visible" >					<!--- embeddedCode must be explicitly set to "visible" to display --->

		<!--- if we have an amount that is approved, then show the payment details --->
		<cfif qryFundActivity.approvedAmount gt 0>
			<cfset displayStruct.paymentDate.relayFormElementType = "html">
			<cfset displayStruct.paymentDate.currentvalue = dateFormat(displayStruct.paymentDate.currentvalue, "long")>
			<cfset displayStruct.paymentRef.relayFormElementType = "html">
			<cfset displayStruct.paymentAmount.relayFormElementType = "html">
		</cfif>

		<cfset displayStruct.agreeTermsConditions.relayFormElementType = "html" >
		<cfset displayStruct.agreeTermsConditions.currentvalue = "#iif(displayStruct.agreeTermsConditions.currentvalue eq 1,DE('Yes'),DE('No'))#">

		<cfset displayStruct.authorised.relayFormElementType = "html" >
		<cfset displayStruct.authorised.currentvalue = "#iif(displayStruct.authorised.currentvalue eq 1,DE('Yes'),DE('No'))#">

	<cfelse>		<!--- user can edit the activity --->

		<cfset displayBudgetGroupAsHTML = true>

		<cfset displayStruct.budgetPeriod.relayFormElementType = "html" >
		<cfset displayStruct.statusText.relayFormElementType = "html" >
		<cfset displayStruct.message_requiredFields.relayFormElementType = "html">
		<cfset displayStruct.message_requiredFields.currentValue="<label class='required'>phr_RequiredFields</label>"> <!--- 2015-08-20 DAN K-1443 - minor tweak to display star symbol with 'required fields' text --->
	</cfif>

	<cfif request.relayCurrentUser.isInternal>

		<cfset displayStruct.includeCommentsInEmail.currentValue = 0>			<!--- TODO: doesn't seem to work; the code suggests the relayformelement shows checked if value=currentvalue  --->
		<cfset displayStruct.includeCommentsInEmail.value = 1>


		<cfif editApproval>
			<cfset displayStruct.comments.relayFormElementType = "textArea">
			<cfset displayStruct.includeCommentsInEmail.relayFormElementType = "checkBox">

			<cfset displayStruct.documentStates.type = "visible" >					<!--- embeddedCode must be explicitly set to "visible" to display --->

		<cfelse>
			<!--- 2012/06/11 IH Case 428372 Make rejected claim current status changeble back to pending --->
			<cfif !CurrentUserApproverLevelQry.ApproverLevel eq qryFundActivity.currentApprovalLevel>
				<cfset displayStruct.fundApprovalStatusID.relayFormElementType = "hidden" >
			</cfif>

			<cfset displayStruct.statusText2.relayFormElementType = "html" >
			<cfset displayStruct.statusText2.currentvalue = displayStruct.statusText.currentvalue >

			<!--- <cfset displayStruct.fundApprovalStatusID.makeHidden = "true"> --->

			<!--- for now not showing documentStates to save switching them in to readonly if NOT editApproval - not considered v important --->
		</cfif>

		<cfif availableBudgetsQry.recordCount gte 1>
			<!--- NJH 2016/08/15 - JIRA PROD2187 - moved below statement out of if statement below as the elseif statement referenced budgetAllocated as well --->
			<cfset budgetAllocated = application.com.relayFundManager.getBudgetAllocations(budgetPeriodAllocationID=availableBudgetsQry.budgetPeriodAllocationID)>

			<cfif availableBudgetsQry.recordCount eq 1 OR NOT editApproval>

				<cfset displayStruct.budgetAllocation.relayFormElementType = "html">
				<cfset displayStruct.budgetAllocation.currentValue="#availableBudgetsQry.budget#">

				<cfset displayStruct.budgetAvailable.relayFormElementType = "html">
				<cfset displayStruct.budgetAvailable.currentValue="#budgetAllocated.budgetRemaining#">
				<cfset displayStruct.budgetAvailable.makeHidden = "true">

				<cfset displayStruct.budgetPeriodAllocationID.currentValue="#availableBudgetsQry.budgetPeriodAllocationID#">	<!--- hidden --->

			<cfelseif availableBudgetsQry.recordCount gt 1>
				<cfset displayStruct.budgetPeriodAllocationID.relayFormElementType = "select">
				<cfset displayStruct.budgetPeriodAllocationID.currentValue="#availableBudgetsQry.budgetPeriodAllocationID#">

				<cfset displayStruct.budgetAvailable.relayFormElementType="numeric">
				<cfset displayStruct.budgetAvailable.type = "text">						<!--- for now we need to explicitly set type="text" for numerics so it isn't hidden  --->
				<cfset displayStruct.budgetAvailable.currentValue="#budgetAllocated.budgetRemaining#">
			</cfif>

			<cfif #qryFundActivity.FundStatusTextID# EQ "approved" OR NOT editApproval>
				<cfset displayStruct.approvedAmount.relayFormElementType = "html">
				<cfset displayStruct.approvedAmount.makeHidden = "true">

				<cfset displayStruct.approvedCoFundingAmount.relayFormElementType = "html">
				<cfset displayStruct.approvedCoFundingAmount.makeHidden = "true">
			<cfelse>
				<cfset displayStruct.approvedAmount.relayFormElementType = "text">
				<cfset displayStruct.approvedCoFundingAmount.relayFormElementType = "text">
			</cfif>
			<cfset displayStruct.approvedTotalAmount.relayFormElementType = "html">
			<cfset displayStruct.approvedTotalAmount.currentValue="#NumberFormat(qryFundActivity.approvedAmount + qryFundActivity.approvedCoFundingAmount,"0.00")#">

		<cfelse>

			<cfset displayStruct.message_noFundsAllocated.relayFormElementType = "html">
			<cfset displayStruct.message_noFundsAllocated.currentValue="phr_fund_noFundsHaveAllocatedForThisBudgetPeriod. phr_fund_youAreUnableToApproveThisActivity.">
		</cfif>

		<cfif editApproval>
			<cfset displayStruct.paymentDate.relayFormElementType = "date">
			<cfset displayStruct.paymentRef.relayFormElementType = "text">
			<cfset displayStruct.paymentAmount.relayFormElementType = "numeric">
			<cfset displayStruct.paymentAmount.type = "text">						<!--- for now we need to explicitly set type="text" for numerics so it isn't hidden  --->
		<cfelse>
			<cfset displayStruct.paymentDate.relayFormElementType = "html">
			<cfset displayStruct.paymentRef.relayFormElementType = "html">
			<cfset displayStruct.paymentAmount.relayFormElementType = "html">
			<cfset displayStruct.paymentAmount.type = "text">
		</cfif>

		<cfset displayStruct.activityHistory.type = "visible" >						<!--- embeddedCode must be explicitly set to "visible" to display --->

	<cfelse>
		<cfset displayStruct.Approval.type = "hidden" >									<!--- groups defined in the XML are displayed by default --->

		<cfset displayStruct.approvedAmount.relayFormElementType = "html">

		<cfset displayStruct.comments.currentValue="Fund Manager edited request">		<!--- hidden; if external and was edited then enter a comment for the history --->
	</cfif>

	<cfset displayStruct.activityFiles.type = "visible" >						<!--- embeddedCode must be explicitly set to "visible" to display --->
</cfif>


<cfif displayBudgetGroupAsHTML or displayBudgetGroupAsHTMLinAddMode>
	<cfset displayStruct.budgetGroupID.relayFormElementType = "html" >
	<cfset displayStruct.budgetGroupID.bindfunction="">
	<cfset displayStruct.budgetGroupID.query="func:com.relayFundManager.getBudgetGroupsForOrganisation(organisationID=#displayStruct.fundAccountOrganisationID.currentvalue#)">
	<!---
	 i probably should set the query in the domain.xml, but it caused issues where fundAccountOrganisationID=''
	 currently the relayFormElement needs a query to be able to display a select box as HTML; ideally the relayFormElement would be able to display as HTML based on the bindfunction too so the query wouldn't be required
	--->
</cfif>

<!-- ESZ ONB-399 SUSE - MDF Request - Pre-approval by partner moved this up before loop so fields from extension can become NON required too if activity is read-only ! -->
<cf_include template="\code\cftemplates\fundRequestActivityDisplay_Extension.cfm" checkIfExists="true">

<!--- if the fundRequestActivity is read only based on the rules above, then set the fields to readonly (feature borrowed from oppEdit not used at time of dev (Jan 2011) but may be useful later)--->
<cfif fundRequestActivityReadOnly>
	<cfloop list="#structKeyList(oppDetailsStruct)#" index="fieldname">
		<cfset displayStruct[fieldname].readOnly = fundRequestActivityReadOnly>

		<!--- if fundRequestActivity is read-only, don't have to make fields required. --->
		<cfset displayStruct[fieldname].required = false>
	</cfloop>
</cfif>

<!--- feature lifted from oppEdit - possibly useful --->
<!---
<cfset displayStruct["fundRequestActivityID"].encrypt = true> <!--- encrypt the fundRequestActivityID --->
 --->


<!--- now assign these display attributes to the field nodes in the xml structure --->
<cftry>
<!---
	<cfif request.relayCurrentUser.isInternal>
		<cfset fundRequestActivityNodes = fundRequestActivityFieldsXML.fundRequestActivity.internal>
	<cfelse>
		<cfset fundRequestActivityNodes = fundRequestActivityFieldsXML.fundRequestActivity.external>
	</cfif>
 --->
	<cfset fundRequestActivityNodes = fundRequestActivityFieldsXML.fundRequestActivity>

	<cfset application.com.relayDisplay.setFieldAttributesForDisplay(node=fundRequestActivityNodes,displayStruct=displayStruct)>

	<cfcatch><cfdump var="#cfcatch#"><br><cfdump var="#displayStruct#">
		<CF_ABORT>
	</cfcatch>
</cftry>
