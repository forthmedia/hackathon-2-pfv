<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			reportFundApprovers.cfm
Author:				???
Date started:		??/??/????

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-10-13			AJC			Added fundapprovallevelphraseTextID to query and tablefromquery results
2010-10-26 			NYB 		Added getFundApprovalLevels query and subsequent if inside 'editor eq "yes"', and removed Back as it's no longer needed
2011/03/08			PPB			LID5832 - added a unique constraint to fundApprover table and  cftry to display the message neatly
2014-10-15			RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:


 --->
<!--- save the content for the xml to define the editor --->


<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Fund Approver Report</cf_title>

<cfset listOfYears = year(now())>
<cfset listOfYears = listAppend(listOfYears,year(dateAdd("y",1,now())))>

<!--- 2013/12/05	YMA	Case 438290 A message used to show if adding a duplicate fund approver.  Due to changes in relayXMLEditor this is no longer the case so added work-around --->
<cffunction name="preSaveFunction" returnType="struct">
	<cfset result = {isOK=true,message=""}>

	<cfloop collection="#form#" item="field">
		<cfif field neq GetPK.COLUMN_NAME and listFindNoCase(form.fieldList,field) and application.com.relayEntity.getTableFieldStructure(tablename=entity,fieldname=field).isOk>
			<cfset entityDetails[field] = form[field]>
		<cfelseif field neq GetPK.COLUMN_NAME and listFindNoCase(columnsInTable,field)>
			<cfset entityDetails[field] = form[field]>
		</cfif>
	</cfloop>

	<cftry>
		<cfinsert datasource="#application.siteDataSource#" tablename="#entity#" formfields="#structKeyList(entityDetails)#">

		<cfcatch>
			<cfif StructKeyExists(cfcatch,"NativeErrorCode") and cfcatch.NativeErrorCode eq "2627">
				<cfset result.message = application.com.relayTranslations.translatePhrase(phrase="phr_fund_DuplicateApproverForCountryAndLevel")>
			<cfelse>
				<cfset result.message = "Problem saving record. Refer an administrator to errorID #errorID#.">
			</cfif>
			<cfset result.isOK = false>
		</cfcatch>
	</cftry>
	<cf_transaction action="rollback">
	<cfreturn result>

</cffunction>

<cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="172" name="FundApprovers" entity="fundApprover" title="Fund Request Approvers.">
		<field name="fundApproverID" label="Fund Approver ID" description="Unique Key." control="html"/>
		<field name="personid" label="Approver" description="Approvers flagged as Fund Approvers in their profile." control="select" query="SELECT personID AS Value, firstName +' '+lastname AS Display FROM person p INNER JOIN booleanFlagData bfd on bfd.entityID = p.personID INNER JOIN flag f ON f.flagID = bfd.flagID where f.flagTextID = 'fundApprover' and p.personid not in (SELECT personID FROM person p INNER JOIN booleanflagdata del on del.entityid = p.personid INNER JOIN flag f2 ON f2.flagID = del.flagID and f2.flagTextID = 'DeletePerson') order by firstname"/> <!--- 2016/04/28 GCC 449033 - exclude people flagged for deletion from being added as fund approvers--->
		<field name="countryID" label="Country" description="Country this person is an approver in" control="select" query="SELECT countryID as value, countryDescription as display, 2 as sortindex from country where countryid not in (37) union select 37 as value, 'All Countries' as display, 1 as sortindex from country where countryid = 37 order by sortindex,countryDescription"></field>
		<field name="fundApprovalLevelID" label="Approver Level" description="Set the level for this approver." control="select" query="select '('+cast(approvalLevel as varchar)+') '+approvalLevelPhrase as Display, fundApprovalLevelID as value from fundApprovalLevel order by value" />
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

<cfparam name="screenTitle" default="">

<!--- 2010-10-26 NYB added getFundApprovalLevels query and subsequent if inside 'editor eq "yes"'  --->
<cfquery name="getFundApprovalLevels" datasource="#application.siteDataSource#">
	select * from fundApprovalLevel
</cfquery>


<cfif isDefined("editor") and editor eq "yes">
	<!--- 2011/03/08 PPB LID5832 START - added cftry to catch the duplicate error "neatly" (and combined add/edit functionality into one RelayXMLEditor call) --->
	<cfset application.com.request.setTopHead(topHeadCfm="")>

	<cfif getFundApprovalLevels.recordcount eq 0>
		phr_fund_ApprovalLevelRequiredToSetupApprover
	<cfelse>
		<CF_RelayXMLEditor
			editorName = "thisEditor"
			xmlSourceVar = "#xmlSource#"
			add="#iif(isDefined('add') and add eq 'yes',DE('yes'),DE('no'))#"
			thisEmailAddress = "relayhelp@foundation-network.com"
			presavefunction = #preSaveFunction#
		>
	</cfif>
	<!--- 2011/03/08 PPB LID5832 END  --->

</cfif>

<cfif not (isDefined("editor") and editor eq "yes") or isDefined("errored")>

	<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
	<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_FundRequestApprovers")>

	<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">

	<cfparam name="sortOrder" default="countrydescription,approverlevel">
	<cfparam name="numRowsPerPage" default="50">
	<cfparam name="startRow" default="1">
	<cfparam name="checkBoxFilterName" type="string" default="">
	<cfparam name="checkBoxFilterLabel" type="string" default="">
	<cfparam name="keyColumnURLList" type="string" default="">
	<!--- check to see if current user is an approver --->
	<cfset isApprover = application.com.relayFundManager.checkIfUserIsApprover()>
	<!--- if they are show the link to edit approvers --->
	<cfif isApprover>
		<cfset keyColumnURLList = "reportFundApprovers.cfm?editor=yes&fundApproverID=">
	</cfif>
	<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->
	<cfquery name="getApprovers" datasource="#application.SiteDataSource#">
		select Approver, ApproverLevel,approvalLevelPhrase, Countrydescription, personid, email, fundApproverID
		from vFundApprovers
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
	<!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->
	<cfoutput>
	<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
		<tr>
			<td>
				<p>#request.CurrentSite.Title# users who have user rights to approve Fund Requests.
			To change an approver click their name below.  &nbsp;
				To add a new approver for a country choose Add New Approver from the menu above.</p>
			</td>
		</tr>
	</table>
	</cfoutput>

	<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->
	<CF_tableFromQueryObject
		queryObject="#getApprovers#"
		queryName="getApprovers"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"

		keyColumnList="Approver"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="fundApproverID"

		hideTheseColumns="fundApproverID"
		showTheseColumns="Approver,ApproverLevel,approvalLevelPhrase,Countrydescription,PersonID,Email"

		columnTranslation="true"

		FilterSelectFieldList="ApproverLevel,Countrydescription"
		FilterSelectFieldList2="ApproverLevel,Countrydescription"
		allowColumnSorting="yes"
	><!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->

</cfif>