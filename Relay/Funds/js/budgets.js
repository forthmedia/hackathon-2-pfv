//2011/04/21 PPB LEX052 clear down Budget Group totals when BudgetGroup or Budget changes
function budgetGroupChanged() {
	clearBudgetGroupTotals();
}

//2011/04/21 PPB LEX052 clear down Budget Group totals when BudgetGroup or Budget changes
function budgetChanged() {
	clearBudgetGroupTotals();
}

function budgetPeriodChanged() {
	populateBugetGroupPeriodAllocationRemaining();
}


function populateBugetGroupPeriodAllocationRemaining() {

	var elemBudgetGroupID = document.getElementById('frmBudgetGroupID');
	var elemBudgetPeriodID = document.getElementById('frmBudgetPeriodID');
	var selectedBudgetGroupID;
	var selectedBudgetPeriodID;

	if (elemBudgetGroupID.selectedIndex > 0 && elemBudgetPeriodID.selectedIndex > 0) {
		selectedBudgetGroupID = elemBudgetGroupID.options[elemBudgetGroupID.selectedIndex].value;
		selectedBudgetPeriodID = elemBudgetPeriodID.options[elemBudgetPeriodID.selectedIndex].value;

		page = '/webservices/relayFundManagerWS.cfc?wsdl&method=getBudgetGroupPeriodAllocations&returnFormat=json&_cf_nodebug=true';
		page = page + '&BudgetGroupID='+selectedBudgetGroupID+'&budgetPeriodID='+selectedBudgetPeriodID
		// changed to post to ensure proper refresh
		var myAjax = new Ajax.Request(
			page,
			{
				method: 'get',
				evalJSON: 'force',
				asynchronous: false,
				debug : false,
				onComplete: function (requestObject,JSON) {
					json = requestObject.responseJSON;

					jQuery('#frmBudgetGroupPeriodAllocationAmount_value').html(json.AMOUNT);
					jQuery('#frmBudgetGroupPeriodAllocationAmountAuthorised_value').html(json.AMOUNTAUTHORISED);
					jQuery('#frmBudgetGroupPeriodAllocationAmountNotAuthorised_value').html(json.AMOUNTNOTAUTHORISED);
					jQuery('#frmBudgetGroupPeriodAllocationAmountRemaining_value').html(json.AMOUNTREMAINING);
					jQuery('#frmBudgetGroupPeriodAllocationAmountRemaining').val(json.AMOUNTREMAINING);
				}
			});
	}
}

//2011/04/21 PPB LEX052 clear down Budget Group totals when BudgetGroup or Budget changes
function clearBudgetGroupTotals() {
			jQuery('#frmBudgetGroupPeriodAllocationAmount_value').html('&nbsp;');
			jQuery('#frmBudgetGroupPeriodAllocationAmountAuthorised_value').html('&nbsp;');
			jQuery('#frmBudgetGroupPeriodAllocationAmountNotAuthorised_value').html('&nbsp;');
			jQuery('#frmBudgetGroupPeriodAllocationAmountRemaining_value').html('&nbsp;');
			jQuery('#frmBudgetGroupPeriodAllocationAmountRemaining').val(0);
}

function verifyForm(formName) {
	var form = eval('document.'+formName);
	var errMsg = "";

	//2012-11-08 PPB Case 431507 add the original amount of the budget currently being edited onto the amountRemaining to avoid it being accounted for twice
	//eg if this budget amount is 5000 and we are changing it to 6000 and the amountRemaining was 2000 (which is calculated including this budget amount) and  the amountRemaining becomes 2000+5000 so the test 6000>7000 returns false (correctly!)
	var amountRemaining = parseFloat(form.frmBudgetGroupPeriodAllocationAmountRemaining.value)+parseFloat(form.frmBudgetAmount_orig.value);
	// 2011/04/21 PPB LID6378 if there is no budgetGroupAllocation row then need to convert amountRemaining (NULL) to 0
	if (isNaN(amountRemaining)) {
		amountRemaining = 0;
	}

	// 2011/04/21 PPB LEX052 LID6376
	if (parseFloat(form.frmBudgetAmount.value) < 0) {
		errMsg= errMsg+'phr_fund_AmountMustBeGreaterThanZero.\n';
	} else if (parseFloat(form.frmBudgetAmount.value) > amountRemaining) {
		errMsg= errMsg+'phr_fund_NewBudgetAmountIsGreaterThanTheFundsAvailable.\n';
	} else if (formName=='changeBudgetForm') {
		if (parseFloat(form.frmBudgetAmount.value) < parseFloat(form.frmBudgetApproved.value)) {
			errMsg= errMsg+'phr_fund_BudgetPeriodAllocationAmountIsLessThanFundsAlreadyAuthorised.\n';
		}
	}

	if (errMsg == ""){
		return true;
	} else {
		alert(errMsg);
		return false;
	}
}