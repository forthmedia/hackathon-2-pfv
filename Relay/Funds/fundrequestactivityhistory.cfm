<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			fundRequestActivityHistory.cfm	
Author:				???
Date started:		??/??/????
	
Description:			

Amendment History:

Date 		Initials 	What was changed
2010-01-12	AJC			Added a dateformat to the tfqo
2011/04/20 	PPB 		LEX052 activity status translation

Possible enhancements:


 --->

<cf_title>Fund Activity History</cf_title>

<cfparam name="fundRequestActivityID" type="numeric" default=0>

<!--- 2011/04/20 PPB LEX052 activity status translation --->
<cfset ApprovalStatusPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "fundApprovalStatus", phraseTextID = "title",baseTableAlias="frah")>
<cfquery name="getFundActivityHistory" datasource="#application.siteDataSource#">
	select frah.*,p.firstName+ ' ' +p.lastName as fullname
			,#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.select)# as fundApprovalStatus			
	from fundRequestActivityHistory frah 
	inner join fundRequestActivity  fra on frah.fundRequestActivityID = fra.fundRequestActivityID 
	inner join fundApprovalStatus fas on fas.fundApprovalStatusID = frah.fundApprovalStatusID 
	inner join person p on p.personID = frah.createdBy 
	#preservesinglequotes(ApprovalStatusPhraseQuerySnippets.join)#
	where fra.fundRequestActivityID =  <cf_queryparam value="#fundRequestActivityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by frah.created
</cfquery>

<cf_tableFromQueryObject 
	queryObject="#getFundActivityHistory#"
	queryName="getFundActivityHistory"
	HidePageControls="yes"
	showTheseColumns="created,fundApprovalStatus,fullname,comments"	
 	hideTheseColumns=""
	dateformat="created"
	useInclude="false"
	rowIdentityColumnName="ActivityHistoryID"
>