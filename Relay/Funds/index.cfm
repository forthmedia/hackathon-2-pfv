<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name: index.cfm

2012-11-15 PPB Case 431990 createFundAccount function now returns a structure
 --->
<cf_title>Points Management</cf_title>

<!---Initialize variables expected later in the code--->
<CFPARAM DEFAULT="Last" NAME="frmType"> 


<SCRIPT>
<!---Make sure either the name of a company or the name of a person is entered when placing an order---> 
		function checkordering(){
		
		var form = window.document.form1
		var companyfield = form.frmCompany.value
		var namefield = form.frmName.value
		
		
		if (companyfield == '' && namefield == '') {
			alert("You must enter either a Company or a Person's Name")
			
		} 
		else form.submit()
		}
	</SCRIPT>
	
<!---Search screen--->
<CFIF IsDefined("frmSearch") and frmSearch eq "yes" >
	<TABLE align="center">
		<TR>
			<TD colspan="3" align="center"><H2>Manage a Funds Account</H2></TD>
		</TR>
					
		<FORM NAME="form1" METHOD="post">
		<TR>
			<TD align="right">Company Name:</TD> 
			<TD colspan="2"><INPUT TYPE="text" NAME="frmCompany"></TD>
		</TR>

		<TR>
			<TD align="right">Person Name:</TD> 
			<TD colspan="2"><INPUT TYPE="text" NAME="frmName"><BR></TD>
		</TR>
		
		<TR>
			<TD colspan="3">
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="First" <CFIF frmType IS 'First'>Checked</CFIF>> First name
			&nbsp; &nbsp;
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="Last" <CFIF frmType IS 'Last'>Checked</CFIF>> Last name
			&nbsp; &nbsp;
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="Full" <CFIF frmType IS 'Full'>Checked</CFIF>> Full name
			</TD>
		</TR>
		
		<TR>
			<TD colspan="3" align="center"><A HREF="javascript:checkordering()"><CFOUTPUT><IMG SRC="/IMAGES/BUTTONS/c_search_e.gif" BORDER=0 ALT="Search"></CFOUTPUT></A></TD>
		</TR>
				
		</FORM>

</TABLE>
</cfif>


<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="232" name="thisEditor" entity="fundAccount" title="Add Funds">
		<field name="AccountID" label="Account ID" description="This records unique ID." control="hidden"></field>
		<field name="Amount" label="Account ID" description="This records unique ID."></field>
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

	<CFPARAM NAME="screenTitle" DEFAULT="">
	<!--- check to see if current user is an approver --->
	<cfset isApprover = application.com.relayFundManager.checkIfUserIsApprover()>


<cfif isDefined("add") and add eq "yes" and isDefined("FORM.creditAmount")>
	<cfscript>
	message = application.com.relayFundManager.AddFunds(FORM.accountID,FORM.creditAmount);
	</cfscript>
</cfif>
<cfif isDefined("editor") and editor eq "yes">
<form action="" method="post">
	Amount to add <input type="text" name="creditAmount"><br>
	<input type="submit">
	<input type="hidden" name="add" value="yes">
	<cfoutput><CF_INPUT type="hidden" name="accountID" value="#URL.AccountID#"></cfoutput>
</form>			

<CFELSE>

<!---The SECOND PHASE, get all the people or companies corresponding to the search criteria
and list them or, if a straight match, go directly to the points management screen--->

	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT o.organisationName as account, o.organisationID, 
		c.countryid, f.accountID,'phr_Add' as Add_Funds,
		currentFunds as current_funds, spentFunds as spent_funds,
				c.CountryDescription AS Country
		FROM organisation o 
			INNER JOIN fundCompanyAccount f ON o.organisationID = f.organisationID
			INNER JOIN Country c ON c.CountryID = o.CountryID
		WHERE 1=1
		<CFIF isDefined("frmCompany") and frmCompany IS NOT ''>
			AND o.organisationName  LIKE  <cf_queryparam value="#frmCompany#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFIF>
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">	
	</CFQUERY>
	
	<!--- create a fund account for FNL if none exist --->
	<CFIF GetDetails.RecordCount EQ 0>
		<cfscript>
		temp = application.com.relayFundManager.createFundAccount("1").accountId;		/* 2012-11-15 PPB Case 431990 createFundAccount function now returns a structure */
		</cfscript>
		<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#">
			SELECT DISTINCT o.organisationName as account, o.organisationID, 
			c.countryid, f.accountID, --'View' as statement, 'Add' as Add_Funds,
					c.CountryDescription AS Country
			FROM organisation o 
				INNER JOIN fundCompanyAccount f ON o.organisationID = f.organisationID
				INNER JOIN Country c ON c.CountryID = o.CountryID
			WHERE 1=1
		</CFQUERY>
	</cfif>
<!---Company matches--->	
	<CFIF GetDetails.RecordCount GT 0>
		
		<CF_tableFromQueryObject queryObject="#getDetails#" 
			keyColumnList="account,add_funds"
			keyColumnURLList="/data/dataFrame.cfm?frmsitename=,index.cfm?editor=yes&accountID="
			keyColumnKeyList="account,accountID"
			FilterSelectFieldList = "Country"
			hideTheseColumns="countryid,organisationID"
			sortOrder="">
		
	</CFIF>
</CFIF>