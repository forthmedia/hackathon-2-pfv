<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			budgetGroupPeriodAllocations.cfm
Author:				PPB
Date started:		2011/04/12
Description:		List and maintain BudgetGroup/Period Allocations

Amendment History:

Date 		Initials 	What was changed
2011/04/20	PPB			Security access + code clean
2011/04/21 	PPB 		LEX052 LID6376
2011/04/21 	PPB 		LEX052 LID6377
2011/04/21 	PPB 		LEX052 changed mode="view" to "list" cos terminology "view" now relates to going into edit a record in read-only mode
2011/04/21 	PPB 		LEX052 LID6392 fundAdminTask:Level2 users can view details but not edit
2011/05/25	PPB			LEX052 changed security level for view
2012/0/005	IH			Case 428588 fix web service path in "bindfunction"

Possible enhancements:


 --->

<cfparam name="budgetGroupPeriodAllocationID" type="numeric" default="1">
<cfparam name="mode" type="string" default="list">
<cfparam name="sortOrder" default="budgetGroupPeriodAllocationID">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="currencyFormat" default="amount,amountAuthorised,amountNotAuthorised,amountRemaining">

<!--- 2012/02/13 PPB changed this from a cfparam to a cfset because a varible (with columns relating to a person row) was "leaking" in --->
<cfset showCols="BudgetGroup,BudgetGroupType,BudgetPeriod,amount,amountAuthorised,amountNotAuthorised,amountRemaining">


<cfset editBudgetSecurityLevel = "fundAdminTask:Level3">
<cfset viewBudgetSecurityLevel = "fundTask:Level3">

<cfset userHasEditRights = application.com.login.checkInternalPermissions(listfirst(editBudgetSecurityLevel,":"),listlast(editBudgetSecurityLevel,":"))>

<cfset functionListQuery = queryNew("")>

<cfif application.com.login.checkInternalPermissions(listfirst(viewBudgetSecurityLevel,":"),listlast(viewBudgetSecurityLevel,":"))>

	<cfset showCols = showCols & ",Edit">

	<cfif structKeyExists(form,"mode")>
		<cfset mode = form.mode>
	<cfelseif structKeyExists(url,"mode")>
		<cfset mode = url.mode>
	</cfif>
	<!--- <cfset request.relayFormDisplayStyle="HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm"> --->

	<cfif structKeyExists(form,"frmAddBudgetGroupPeriodAllocation")>
		<cfset application.com.relayFundManager.addBudgetGroupPeriodAllocation(budgetGroupID=frmBudgetGroupID,budgetPeriodID=frmBudgetPeriodID,allocatedAmount=frmAmount,authorised=structKeyExists(form,"frmAuthorised")?1:0)>
	<cfelseif structKeyExists(form,"frmUpdateBudgetGroupPeriodAllocation")>
		<cfif userHasEditRights>
			<cfset application.com.relayFundManager.updateBudgetGroupPeriodAllocation(budgetGroupPeriodAllocationID=frmbudgetGroupPeriodAllocationID,amount=frmAmount,authorised=structKeyExists(form,"frmAuthorised")?1:0)>
		</cfif>
	</cfif>

	<cf_title>Budget Group Period Allocations</cf_title>
		<cfif mode eq "list">

			<cfset getBudgetGroupPeriodAllocations = application.com.relayFundManager.getBudgetGroupPeriodAllocations()>

			<!--- 2011/04/21 PPB LEX052 LID6392 fundAdminTask:Level2 users can view details but not edit --->
			<cfif userHasEditRights>
				<cfset linkText = "Edit">
			<cfelse>
				<cfset linkText = "View">
			</cfif>

			<cfquery name="getBudgetGroupPeriodAllocations" dbtype="query">
				select *, '<cfoutput>#linkText#</cfoutput>' AS Edit
				from getBudgetGroupPeriodAllocations
				order by <cf_queryObjectName value="#sortOrder#">
			</cfquery>

			<cf_tableFromQueryObject
				queryObject="#getBudgetGroupPeriodAllocations#"
				queryName="getBudgetGroupPeriodAllocations"
				sortOrder = "#sortOrder#"
				numRowsPerPage="#numRowsPerPage#"
				startRow="#startRow#"
				columnTranslation="true"
				columnTranslationPrefix="phr_fund_"

				showTheseColumns="#showCols#"
			 	hideTheseColumns=""
				useInclude="false"
				currencyFormat="#currencyFormat#"
				rowIdentityColumnName="budgetGroupPeriodAllocationID"

				keyColumnList="Edit"
				keyColumnURLList="budgetGroupPeriodAllocations.cfm?mode=edit&budgetGroupPeriodAllocationID="
				keyColumnKeyList="budgetGroupPeriodAllocationID"
				keyColumnOpenInWindowList="no"

				filterSelectFieldList="BudgetGroup,BudgetGroupType,BudgetPeriod"
				filterSelectFieldList2="BudgetGroup,BudgetGroupType,BudgetPeriod"
			>

		<cfelseif mode eq "add">

			<!--- <cf_includejavascriptonce template="/mxajax/core/js/mxAjax.js">
			<cf_includejavascriptonce template="/mxajax/core/js/mxSelect.js">
			<cf_includejavascriptonce template="/mxajax/core/js/mxData.js"> --->

			<cfset BudgetGroupsQry = application.com.relayFundManager.getBudgetGroups()>

			<cfquery name="getBudgetGroups" dbtype="query">
				select BudgetGroupID,description, BudgetGroupTypeID from BudgetGroupsQry
				union
				select cast(0 as integer) as BudgetGroupID,'phr_pleaseSelectABudgetGroup' as description,0 as BudgetGroupTypeID from BudgetGroupsQry
				order by BudgetGroupID
			</cfquery>

			<!--- 2011/04/21 PPB LID6377 added action otherwise after editing a record the GO button of the TFQO filters (which fires queryString) re-directed the user back to the edited record --->
			<form name="addForm" id="addForm" method="post"  onSubmit="return verifyForm('addForm');" novalidate="true">
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmBudgetGroupID" id="frmBudgetGroupID" currentValue="" label="phr_fund_selectABudgetGroup" query="#getBudgetGroups#" value="BudgetGroupID" display="description">
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmBudgetPeriodID" id="frmBudgetPeriodID" currentValue="" label="phr_fund_budgetPeriod" bindfunction="cfc:relay.webservices.callWebService.callWebService(webServiceName='relayFundManagerWS',methodName='getBudgetPeriods_Dropdown',budgetGroupID={frmBudgetGroupID})" bindOnLoad="false" value="dataValue" display="displayValue" required="yes" nullText="phr_fund_pleaseSelectABudgetPeriod">
					<cf_relayFormElementDisplay relayFormElementType="numeric" fieldName="frmAmount" currentValue="" label="phr_fund_budgetGroupPeriodAllocation_Amount" required="yes" maxLength="11">
					<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldName="frmAuthorised" currentValue="0" label="phr_fund_budgetGroupPeriodAllocation_Authorised" required="no">
					<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmAddBudgetGroupPeriodAllocation" currentValue="phr_fund_budgetGroupPeriodAllocation_Add" label="">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="list" label="">
				</cf_relayFormDisplay>
			</form>

		<cfelseif mode eq "edit">

			<cfset getbudgetGroupPeriodAllocations = application.com.relayFundManager.getbudgetGroupPeriodAllocations(budgetGroupPeriodAllocationID=budgetGroupPeriodAllocationID)>

			<!--- 2011/04/21 PPB LID6377 added action otherwise after editing a record the GO button of the TFQO filters (which fires queryString) re-directed the user back to the edited record --->
			<form name="updateForm" id="updateForm" method="post"  onSubmit="return verifyForm('updateForm');" novaldiate="true">
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmBudgetGroupPeriodAllocationID" currentValue="#budgetGroupPeriodAllocationID#" label="">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetGroup" currentValue="#getbudgetGroupPeriodAllocations.BudgetGroup#" label="phr_fund_selectABudgetGroup">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmBudgetPeriod" currentValue="#getbudgetGroupPeriodAllocations.budgetPeriod#" label="phr_fund_budgetPeriod" >

					<!--- 2011/04/21 PPB LEX052 LID6392 fundAdminTask:Level2 users can view details but not edit --->
					<cfif userHasEditRights>
						<cf_relayFormElementDisplay relayFormElementType="numeric" fieldName="frmAmount" currentValue="#getbudgetGroupPeriodAllocations.amount#" label="phr_fund_budgetGroupPeriodAllocation_Amount" required="yes" maxLength="11">
					<cfelse>
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="frmAmount" currentValue="#getbudgetGroupPeriodAllocations.amount#" label="phr_fund_budgetGroupPeriodAllocation_Amount">
					</cfif>

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmAmountAuthorised" currentValue="#getbudgetGroupPeriodAllocations.amountAuthorised#" label="phr_fund_amountAuthorised" makeHidden="true">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmAmountNotAuthorised" currentValue="#getbudgetGroupPeriodAllocations.amountNotAuthorised#" label="phr_fund_amountNotAuthorised" makeHidden="true">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmAmountRemaining" currentValue="#getbudgetGroupPeriodAllocations.amountRemaining#" label="phr_fund_amountRemaining">

					<!--- 2011/04/21 PPB LEX052 LID6392 fundAdminTask:Level2 users can view details but not edit --->
					<cfif userHasEditRights>
						<cf_relayFormElementDisplay relayFormElementType="checkbox" fieldName="frmAuthorised" currentValue="#getbudgetGroupPeriodAllocations.authorised#" label="phr_fund_budgetGroupPeriodAllocation_Authorised" required="no">
					<cfelse>
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="frmAuthorised" currentValue="#getbudgetGroupPeriodAllocations.authorised eq 1?'Yes':'No'#" label="phr_fund_budgetGroupPeriodAllocation_Authorised">
					</cfif>

					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmLastUpdated" currentValue="#DateFormat(getbudgetGroupPeriodAllocations.lastUpdated,'dd mmm yyyy') & '&nbsp;&nbsp;&nbsp;' & TimeFormat(getbudgetGroupPeriodAllocations.lastUpdated,'hh:mm')#" label="phr_sys_LastUpdated">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmLastUpdatedByFullName" currentValue="#getbudgetGroupPeriodAllocations.lastUpdatedByFullName#" label="phr_sys_LastUpdatedBy">

					<!--- 2011/04/21 PPB LEX052 LID6392 fundAdminTask:Level2 users can view details but not edit --->
					<cfset buttonText = userHasEditRights?"phr_fund_budgetGroupPeriodAllocation_Update":"phr_sys_back">

					<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmUpdateBudgetGroupPeriodAllocation" currentValue="#buttonText#" label="">
					<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="mode" currentValue="list" label="">
				</cf_relayFormDisplay>
			</form>

		</cfif>

<cfelse>
	You do not have sufficient permissions to view the budget group period allocations.
</cfif>

<cf_head>
	<script type="text/javascript">
		function verifyForm(formName) {
			var form = eval('document.'+formName);
			var errMsg = "";

			// 2011/04/21 PPB LEX052 LID6376
			if (parseFloat(form.frmAmount.value) < 0) {
				errMsg= errMsg+'phr_fund_AmountMustBeGreaterThanZero.\n';

			} else if (formName=='updateForm') {
				if (parseFloat(form.frmAmount.value) < parseFloat(form.frmAmountAuthorised.value)) {
					errMsg= errMsg+'phr_fund_BudgetGroupPeriodAllocationAmountIsLessThanFundsAlreadyAuthorised.\n';
				}
			}

			if (errMsg == ""){
				return true;
			} else {
				alert(errMsg);
				return false;
			}
		}
	</script>
</cf_head>