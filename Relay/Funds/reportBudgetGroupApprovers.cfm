<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			reportBudgetGroupApprovers.cfm
Author:				???
Date started:		??/??/????

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-10-13			AJC			Added fundapprovallevelphraseTextID to query and tablefromquery results
2014-10-15			RPW			Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on

Possible enhancements:


 --->
<!--- save the content for the xml to define the editor --->

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfset application.com.request.setTopHead(pageTitle="Phr_Sys_BudgetGroupApprovers")>

<cfset listOfYears = year(now())>
<cfset listOfYears = listAppend(listOfYears,year(dateAdd("y",1,now())))>

<cfparam name="screenTitle" default="">


<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Budget Group Approver Report</cf_title>

<cfparam name="sortOrder" default="description">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">
<cfparam name="keyColumnURLList" type="string" default="">
<!--- check to see if current user is an approver --->

<cfset keyColumnURLList = "BudgetGroupApprovers.cfm?BudgetGroupID=">

<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->
<cfquery name="getBudgetGroups" datasource="#application.SiteDataSource#">
	select * from (
		select bg.BudgetGroupID, bg.description, bgt.description as budgetgrouptype, '<img src="/images/MISC/iconEditPencil.gif" alt="Edit" title="Edit">' as editlink
		from budgetgroup bg
		inner join budgetgrouptype bgt on bg.budgetgrouptypeID = bgt.budgettypeID
	) as base
	where 1=1
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
<!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to query --->

<cfoutput>
<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="withBorder">
	<tr>
		<td>
			<div id="reportBudgetGroupApproversTable1" border="0" cellspacing="0" cellpadding="0" width="100%">
				<div>&nbsp;</div>
				<div>
					<p>#request.CurrentSite.Title# users who have user rights to approve Fund Requests. To modify approvers in a budget group click on the edit link.<br></p>
				</div>
			</div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>
</cfoutput>

	<!--- START: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->
	<cf_tablefromqueryobject
		queryObject="#getBudgetGroups#"
		queryName="getBudgetGroups"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"

		keyColumnList="editlink"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="BudgetGroupID"

		hideTheseColumns="BudgetGroupID"
		showTheseColumns="description,budgetgrouptype,editlink"

		columnTranslation="true"

		FilterSelectFieldList="budgetgrouptype"
		allowColumnSorting="yes"
	><!--- END: 2009-10-13 - AJC - P-LEX039 - Added approverLevelPhraseTextID to tfqo --->