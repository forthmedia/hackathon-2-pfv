<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			fundRequestActivityApprovals.cfm	
Author:				AJC
Date started:		2009/10/19
Description:		Display Fund Request Activities awaiting approval

Amendment History:

Date 		Initials 	What was changed
2011/04/08	PPB 		LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list
2011/05/06	AJC 		LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list
2011-09-16 	NYB 		P-SNY106

Possible enhancements:

 --->

<cf_title>Fund Request Activity Approvals</cf_title>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="country,account,Period">

<cfparam name="mode" type="string" default="view">
<cfparam name="goToUrl" type="string" default="/elearning/certificationRegistrations.cfm?">
<cfparam name="formName" type="string" default="mainForm">

<cfparam name="showTheseColumns" type="string" default="BudgetGroup,Account,Country,Period,orgBudgetAllocation,sum_Requested,sum_Approved,Show_Activities">

<cfparam name="translateColumns" type="boolean" default="true">
<!--- <- P-SOP010 --->
<cfparam name="numRowsPerPage" type="numeric" default=500> <!--- NJH 2009/10/02 LID 2723 --->

<!--- START: 2011-09-16 NYB P-SNY106 - added params: --->		
<CFPARAM NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#"><!--- NYB 2011-09-15 P-SNY106 --->
<CFPARAM NAME="showIncorrectQuestionsOnCompletion" DEFAULT="false"><!--- NYB 2011-09-15 P-SNY106 --->
<!--- END: 2011-09-16 NYB P-SNY106 --->	

<cfset orgAccountManagerTextID=application.com.settings.getSetting("fundManager.orgAccountManagerTextID")>

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfif not request.relayCurrentUser.isInternal>
	<cfif expandFor neq "All">
		<cfloop index = "flagTextID" list = "#expandFor#">
			<cfset x = application.com.flag.evaluateFlagExpression(expression=flagTextID,personid=request.relayCurrentUser.personID)>
			<cfif x eq "" or x eq "true">
				<cfset expandFor = "All">
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>
</cfif>
	
	<cfquery name="getData" datasource="#application.SiteDataSource#">
		select fr.*,'<img src="/images/icons/bullet_arrow_down.png">' as Show_Activities
		from vFundRequestList fr
			inner join fundCompanyAccount fca on fca.accountID = fr.accountID
			inner join organisation o on o.organisationID = fca.organisationID
			<!--- 2012-07-20 PPB P-SMA001		where o.countryID in (#request.relayCurrentUser.countryList#) --->
			where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="FundRequest",alias="fr").whereClause#	<!--- 2012-07-20 PPB P-SMA001  --->

			and count_requests > 0
			
			and 
			(
				fr.fundRequestID IN
				(
					select fra2.fundRequestID
						from fundRequest fr inner join
						fundRequestActivity fra2 on fr.fundrequestID = fra2.fundrequestID inner join
						fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						budgetgroupApprover bga on bg.budgetgroupID = bga.budgetgroupID inner join
						organisation o on fca.organisationID = o.organisationID inner join
						countryGroup cg on o.countryID = cg.countryMemberID inner join
						fundApprover fa on cg.countrygroupID = fa.countryID and bga.fundapproverID = fa.fundapproverID
						inner join fundApprovalLevel fal on fa.fundApprovalLevelID = fal.fundApprovalLevelID and fal.approvalLevel = fra2.currentApprovalLevel
						inner join fundApprovalStatus fas on fra2.fundApprovalStatusID = fas.fundApprovalStatusID
						where fa.personID = #request.relaycurrentuser.personID#
						and (fas.fundStatusTextID != 'approved' or fas.fundStatusTextID is null)
				)
				or
				fr.fundRequestID IN
				(
				
					select fra2.fundRequestID
						from fundRequest fr inner join
						fundRequestActivity fra2 on fr.fundrequestID = fra2.fundrequestID inner join
						fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						organisation o on fca.organisationID = o.organisationID inner join
						fundApprovalStatus fas on fra2.fundApprovalStatusID = fas.fundApprovalStatusID
						inner join integerflagdata ifd on o.OrganisationID = ifd.entityID and ifd.data = #request.relaycurrentuser.personID#
						<!--- START: 2011/05/06 AJC LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list --->
						inner join Flag f ON ifd.flagid = f.flagid and f.flagtextID  IN ( <cf_queryparam value="#orgAccountManagerTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
						<!--- END: 2011/05/06 AJC LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list --->
						where bg.accountManagerApproval= 1 and
						fra2.currentApprovalLevel=1
						and (fas.fundStatusTextID != 'approved' or fas.fundStatusTextID is null)
				)
			)
			
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">	
	</cfquery>

		<cfif isdefined("url.ShowDebugPC")>
			<cfoutput><br/>sortOrder=#htmleditformat(sortOrder)#<br/></cfoutput>
		</cfif>
		
		<cfif getData.recordCount gt 0>
			
			<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
			
			<cfset columnToShowAjaxAt = listFindNoCase(getData.columnList,"fundRequestID")>

			<script>
				<cfoutput>var columnToShowAjaxAt = #jsStringFormat(columnToShowAjaxAt)#</cfoutput>
				function $alternative(element) { 
				  if (arguments.length > 1) {
				    for (var i = 0, elements = [], length = arguments.length; i < length; i++)
				      elements.push($alternative(arguments[i]));
				    return elements;
				  }
				  if (Object.isString(element))
				    element = document.getElementById(element);
				  return Element.extend(element);
				}
				
				function showActivities (obj,id) {
					currentRowObj = getParentOfParticularType (obj,'TR')
					currentTableObj = getParentOfParticularType (obj,'TABLE')

					totalColspan = 0
					for (i=0;i<currentRowObj.cells.length;i++) {
						totalColspan  += currentRowObj.cells[i].colSpan
					}
					
					infoRow =  id + '_info'
					infoRowObj = $(infoRow)
					
					if (!infoRowObj) {
						page = '/webservices/mxAjaxFundRequests.cfc?wsdl&method=getFundActivityApprovals&returnFormat=json'
						<cfoutput>page = page + '&fundRequestID='+id+'&orgAccountManagerTextID=#orgAccountManagerTextID#'</cfoutput>
						// changed to post to ensure proper refresh
						var myAjax = new Ajax.Request(
							page, 
							{
								method: 'post', 
								parameters: '', 
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									rowStructure = new Array (2)
									rowStructure.row = new Array (1) 
									rowStructure.cells = new Array (1) 
									rowStructure.cells[0] = new Array (1) 
									rowStructure.cells[0].content  = json.CONTENT 
									rowStructure.cells[0].colspan  = totalColspan //-rowStructure.cells[0].colspan
									rowStructure.cells[0].align  = 'right'
			
									regExp = new RegExp ('_info')
									deleteRowsByRegularExpression (currentTableObj,regExp)
									
									addRowToTable (currentTableObj,rowStructure,obj.parentNode.parentNode.rowIndex + 1,infoRow)
								
								}
							});
					} 
					else
					{
						regExp = new RegExp (infoRow)
						deleteRowsByRegularExpression (currentTableObj,regExp)
					}
					
					return 
				}
				
				function getParentOfParticularType (obj,tagName) {	
					if (obj.parentNode.tagName == tagName) 	{
						return obj.parentNode
					} else {
						return getParentOfParticularType (obj.parentNode,tagName)
					}
				}
				
				/*function addUserModule(obj,personCertificationID,moduleID,personID) {
					page = '/webservices/relayElearningWS.cfc?wsdl&method=insertUserModuleProgress'
					page = page + '&moduleID='+moduleID
					page = page + '&personID='+personID
					
					var myAjax = new Ajax.Request(
						page,
						{
								method: 'post', 
								parameters: '', 
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									alert(json.CONTENT)
								}
						}
					)
					
					page = '/webservices/relayElearningWS.cfc?wsdl&method=getCertificationItems'
					page = page + '&action=update&personCertificationID='+personCertificationID
					<cfif not request.relayCurrentUser.isInternal>
						<cfoutput>page = page + '&eid='+ #jsStringFormat(request.currentElement.id)#</cfoutput>
					</cfif> 
					// 2011-09-16 			NYB 		P-SNY106
					<cfoutput>parameters='stringencyMode=#jsStringFormat(stringencyMode)#&showIncorrectQuestionsOnCompletion=#jsStringFormat(showIncorrectQuestionsOnCompletion)#';</cfoutput>
					
					div = 'personCertificationDataDiv'
					var myAjax = new Ajax.Updater(
						div,
						page, 
						{
							method: 'get', 
							parameters: parameters , 
							evalJSON: 'force',
							debug : false
						});
				}*/
			</script>
			
			 <cfset keyColumnList = "Show_Activities">
			
			<cfif isdefined("url.ShowQueries")>
				<cfdump var="#getData#">
			</cfif>

			<!--- WAB Case 431103, changed currencyLocale attribute to be currencyISO --->
			<cf_tablefromqueryobject 
				queryObject="#getData#"
				queryName="getData"
				showTheseColumns="#showTheseColumns#"
				hideTheseColumns="fundRequestID"
				ColumnTranslationPrefix="phr_fund_"
				columnTranslation="#translateColumns#"
				useInclude="false"
				keyColumnList="#keyColumnList#"
				keyColumnURLList=" "
				keyColumnKeyList="fundRequestID"
				keyColumnOnClickList="javascript:showActivities(this*comma##fundRequestID##);return false"
				keyColumnOpenInWindowList="no"
				rowIdentityColumnName="fundRequestID"
				HidePageControls="yes"
				sortOrder="#sortOrder#"
				allowColumnSorting="no"
				numRowsPerPage = "#numRowsPerPage#"
				currencyISO = "USD,USD,USD"
				FilterSelectFieldList="BudgetGroup,Period,Country,Account"
				FilterSelectFieldList2="BudgetGroup,Period,Country,Account"
					
				currencyFormat="sum_Requested,sum_Approved,orgBudgetAllocation"
			>
			<!--- 
			<cf_tableFromQueryObject 
					queryObject="#vFundRequestList#"
					queryName="vFundRequestList"
					sortOrder = "#sortOrder#"
					numRowsPerPage="#numRowsPerPage#"
					startRow="#startRow#"
					columnTranslation="true"
					columnTranslationPrefix="phr_fund_"
					
					showTheseColumns="BudgetGroup,Account,Country,Period,orgBudgetAllocation,count_requests,sum_Requested,sum_Approved,Activities"	
				 	hideTheseColumns="countryid,Request_ID"
					
					keyColumnList="Account,Activities"
					keyColumnURLList="javascript:void(viewOrganisation(thisUrlKey*comma'#Variables.screenloadname#')),fundRequestActivities.cfm?fundRequestID="
					keyColumnKeyList="organisationID,Request_ID"
					keyColumnOpenInWindowList="no,yes"  <!--- NJH 2008/01/28 MDF phase 3: opened activites in new window --->
					
					FilterSelectFieldList="BudgetGroup,Period,Country,Account"
					FilterSelectFieldList2="BudgetGroup,Period,Country,Account"
					
					currencyFormat="sum_Requested,sum_Approved,orgBudgetAllocation"
					
					rowIdentityColumnName="Request_ID"
				>
			
			 --->
			<div id="dummy"></div>
		<cfelse>
			<div align="center">phr_funds_noFundRequestActivitiesAwaitingApproval</div>
		</cfif>