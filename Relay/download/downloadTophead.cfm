<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---
1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
1999-02-21  SWJ	Modified javascript to call report/wabcommcheck.cfm
2013-11-05	WAB CASE 437307 Added pageTitle
--->

<CFPARAM NAME="frmruninpopup" DEFAULT="">

<CFPARAM NAME="current" TYPE="string" DEFAULT="downHeader.cfm">
<cfif not isdefined("PageTitle")>
	<cfif isdefined("frmCommID")>
		<cfset PageTitle = "Download #frmCommID#">
	<cfelse>
		<cfset pageTitle = "">
	</cfif>
</cfif> 


<CF_RelayNavMenu pageTitle="#PageTitle#" moduleTitle="Downloads" thisDir="download">
	<CFIF not findNoCase("downheader.cfm",SCRIPT_NAME,1) >
		<cfif application.com.login.checkInternalPermissions("downTask","Level2")>
			<CF_RelayNavMenuItem MenuItemText="New Download" CFTemplate="/download/downheader.cfm" target="mainSub">
		</cfif> 
	</CFIF>
</CF_RelayNavMenu>