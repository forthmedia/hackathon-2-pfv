<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	
1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid rather than a selection
1999-10-04  KT		Remove references to e-mail and fax as this is for downloads only
2000-01-19 WAB   Altered use of "entityIDs" variable.  Had been making it a list of personids, but this caused problems when large numbers, now it is either a list of ids (if this is what has been passed) or a select statement
2013-04-30 NYB Case 343151

2013-08-13	YMA		Case 436435 Only join SelectionTag if we are not tagging individual people to avoid confusing the personID count
2013-11-05	WAB 	CASE 437307 Recrafted query, seems much quicker
--->



<!--- =====================
	 qryCommCounts.cfm
	 ======================
	required inputs:
		a list of selectionIDs: Variables.selectIDlist
		the CommFormLID of the communication: Variables.thisCommFormLID
	 outputs (if record count is > 0):
	 	SelectTitleList (a comma separated list of all comm titles)
		TagPeople
		FeedbackCounts
	<CFINCLUDE TEMPLATE="qryCommCounts.cfm">		
--->
<!--- assumes the user has permission to see the selection in selectIDlist --->

<CFSET SelectTitleList = "">
<CFSET TagPeopleCount = "">
<CFSET FeedbackCounts = "">

<!--- 2012-07-25 PPB P-SMA001 commented out
<CFIF NOT IsDefined("Variables.CountryList")>
	<!--- get the user's countries --->
	<cfinclude template="/templates/qrygetcountries.cfm">
	<!--- qrygetcountries creates a variable CountryList --->
</CFIF>
--->

<CFIF Len(frmPersonIDs) EQ 0>
	<CFQUERY NAME="getCommTitles" DATASOURCE="#application.SiteDataSource#">
 		SELECT s.Title
			FROM Selection AS s with(nolock)
			WHERE  s.SelectionID  IN ( <cf_queryparam value="#Variables.selectIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY s.Title
	</CFQUERY>
	<CFSET SelectTitleList = ValueList(getCommTitles.Title)>
	<!--- START: 2013-04-10 NYB Case 434151 removed ---
	<CFSET EntityIDs="SELECT DISTINCT st1.EntityID FROM SelectionTag st1 with(nolock) WHERE st1.SelectionID IN (#Variables.selectIDlist#) AND st1.Status <> 0 ">
	--- END: 2013-04-10 NYB Case 434151 --->
<CFELSE>
	<CFSET SelectTitleList = "Temporary Selection">							 
	<!--- START: 2013-04-10 NYB Case 434151 removed ---
	<CFQUERY NAME="getpersonids" DATASOURCE="#application.SiteDataSource#">  
		SELECT personid 
		FROM Person with(nolock)
		WHERE Personid  IN ( <cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )			<!---  frmpersonids can be either delimted list or select statement--->
	</CFQUERY>
	<CFSET EntityIDs=valuelist(getpersonids.personid)>
	--- END: 2013-04-10 NYB Case 434151 --->
	<CFIF frmpersonids IS "">
		<CFSET frmpersonids = "0">
	</CFIF>
</CFIF>							 

<!--- get list of locationids --->
<!--- <CFQUERY NAME="getlocationids" DATASOURCE="#application.SiteDataSource#">  
	SELECT distinct locationid 
	FROM Person
	WHERE Personid IN (#entityids#)			
</CFQUERY>

<CFSET LocationIDs=valuelist(getlocationids.locationid)>
 --->
		
<!--- only email or prefer email --->			    

<!--- START: 2013-04-10 NYB Case 434151 replaced:
<CFQUERY NAME="getCommCounts" DATASOURCE="#application.SiteDataSource#">
	SELECT
		(SELECT count(1)
			FROM Person AS p with(nolock), Location AS l with(nolock)
			WHERE p.Active <> 0
			AND p.LocationID = l.LocationID
			<!--- 2012-07-25 PPB P-SMA001 commented out
			AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			
			AND p.PersonID  IN ( <cf_queryparam value="#EntityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		) AS TagPeople,
		(SELECT count(distinct l.locationid)
			FROM Person AS p with(nolock), Location AS l with(nolock)
			WHERE p.Active <> 0
			AND p.LocationID = l.LocationID
			<!--- 2012-07-25 PPB P-SMA001 commented out
			AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			AND p.PersonID  IN ( <cf_queryparam value="#EntityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		) AS TagLocation,
		(SELECT count(distinct l.Organisationid)
			FROM Person AS p with(nolock), Location AS l with(nolock)
			WHERE p.Active <> 0
			AND p.LocationID = l.LocationID
			<!--- 2012-07-25 PPB P-SMA001 commented out
			AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			AND p.PersonID  IN ( <cf_queryparam value="#EntityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		) AS TagOrganisation
</CFQUERY>
with: --->
<CFQUERY NAME="getCommCounts" DATASOURCE="#application.SiteDataSource#">
	SELECT count(p.PersonID) AS TagPeople, count(distinct p.locationid) as TagLocation, count(distinct p.Organisationid) as TagOrganisation
			FROM 
			<!--- 2013-08-13	YMA	Case 436435 Only join SelectionTag if we are not tagging individual people to avoid confusing the personID count --->
			<CFIF Len(frmPersonIDs) EQ 0>
				SelectionTag AS st1 with(nolock) 
					inner join 			
				Person AS p with(nolock) on p.PersonID = st1.EntityID 	and st1.SelectionID IN (<cf_queryparam value="#Variables.selectIDlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)	AND st1.Status <> 0
			<cfelse>	
				Person AS p with(nolock)
			</CFIF>
			#application.com.rights.getRightsFilterQuerySnippet(entityType="person",alias="p",useJoins="true").JOIN#
			
			WHERE p.Active <> 0 
			<CFIF Len(frmPersonIDs) NEQ 0>
				AND p.Personid  IN ( <cf_queryparam value="#frmpersonids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
			</CFIF>
</CFQUERY>
<!--- END: 2013-04-10 NYB Case 434151 replaced --->

<CFSET TagPeopleCount = getCommCounts.TagPeople>
<CFSET TagLocationCount = getCommCounts.TagLocation>
<CFSET TagOrganisationCount = getCommCounts.TagOrganisation>