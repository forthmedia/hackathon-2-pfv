<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---
2000-04-09 SWJ  Removed the stylesheet call from this template as it is now in the top application .cfm in a CFHTMLHEADER call
170200	WAB		Problem with getSelections.RecordCount fixed (recordcount column exists in the selection table)
1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid and to run in a popup window
1999-09-30 KT		this template is now just for downloading communications
2001-05-03	WAB 	made a small change to the action on the <form>  - gave it a full path
2005-05-17 WAB altered query to deal with selections shared with usergroups
2009/02/12   WAB   Added links to previous downloads
2013-11-05		WAB CASE 437307 link to previous downloads can target self
2015-11-12  ACPK    Added Bootstrap; Removed <br> tags and added missing </p> tags
--->


<!---
		user rights to access:
			SecurityTask:commTask
			Permission: Add
---->
<!--- NJH 2011/11/09 - moved security to directorySecurity.xml
<CFIF NOT checkPermission.level2 GT 0>
	You are not authorised to use this function
		<!--- <CFINCLUDE TEMPLATE="downheader.cfm"> --->
	<CF_ABORT>
</CFIF> --->

<CFSET task = "add">

<CFIF IsDefined("btnTask")>
	<!--- btnTask is a form variable that determines which template
		is used in downtask.cfm.  If btnTask exists and we are on this template,
		we can be pretty sure that it is "edit", but check anyway.
		The default task for this template is "add"--->

	<CFIF #btnTask# IS "edit">
		<CFSET task = #btnTask#>
	</CFIF>
</CFIF>

<CFQUERY NAME="getCommType" DATASOURCE="#application.SiteDataSource#">
	SELECT IsDefault,
		  LookupID,
		  ItemText
	 FROM LookupList
	 WHERE FieldName = 'CommTypeLID'
	 AND itemText= 'download'
</CFQUERY>


<cfset getSelections = application.com.selections.getSelections()>
<CFSET numberOfRecords = listLen(valuelist(getSelections.selectionID))>
<!--- <CFQUERY NAME="getSelections" DATASOURCE="#application.SiteDataSource#">
	SELECT s.Title, s.SelectionID, s.Description, s.Created AS datecreated, s.recordcount as Records
      FROM Selection AS s, SelectionGroup AS sg
     WHERE sg.SelectionID = s.SelectionID
	   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
  ORDER BY s.Title
</CFQUERY>
 --->
<CFPARAM NAME="qryFormAction" DEFAULT="downcreate.cfm">
<CFPARAM NAME="qryProjectRef" DEFAULT="">
<CFPARAM NAME="qryTitle" DEFAULT="">
<CFPARAM NAME="qryNotes" DEFAULT="">
<CFPARAM NAME="qryDescription" DEFAULT="">
<CFPARAM NAME="qryCommID" DEFAULT="0">
<CFPARAM NAME="qryLastUpdated" DEFAULT="">
<CFPARAM NAME="qryCommFormLID" DEFAULT="0">
<CFPARAM NAME="qryCommTypeLID" DEFAULT="#getCommType.lookupID#">
<CFPARAM NAME="qryCommFormText" DEFAULT="">
<CFPARAM NAME="frmEmailTemplate" DEFAULT="">   <!--- wab --->
<CFPARAM NAME="frmpersonids" DEFAULT="">   <!--- wab --->
<CFPARAM NAME="frmruninpopup" DEFAULT="">  <!--- wab --->




<!--- WAB 2009/01/06 Add links to previous recent downloads --->
<CFQUERY NAME="getRecentDownloads" DATASOURCE="#application.SiteDataSource#">
select distinct
	top 15 c.title,c.commid,c.Description,cf.filesize,convert(varchar,c.created,101) as createdformatted,
	s.title as selectionName,
	c.created
<!--- 	(select top 1 title from selection s inner join commselection cs on s.selectionid = cs.selectionid where cs.commid = c.commid) as selectionName,
	(select count (1) from commselection cs where cs.commid = c.commid) as NumberOfSelections
 ---> from
 	communication c
 		inner join
	commfile cf on cf.commid = c.commid
		inner join
	commSelection cs on cs.commid = c.commid
		inner join
	selection	s on cs.selectionid = s.selectionid
where
		c.commformLID = 4
	and c.createdby = #request.relaycurrentuser.usergroupid#
	and c.sent <> 0
order by c.created desc
</CFQUERY>


<!---  This function groups up a query and makes the ungrouped field into a  comma separated list
NOW HAVE THIS FUNCTION (with new name) in structureFunctions

--->
<cffunction name="coalesceField">

	<cfargument name ="Query" type="query">
	<cfargument name ="groupBy" type="string">
	<cfargument name ="Field" type="string">
	<cfargument name ="delimiter" type="string" default=",">

	<cfset columnList = Query.columnList>
	<cfset queryAddColumn(Query,"keepThis","integer",arrayNew(1))>

	<!--- loop through query --->
	<cfoutput query ="query" group="#groupBy#">
		<cfset keepThisRowID = currentRow>
		<cfset querySetCell (Query,"keepthis",1, keepThisRowID)>
		<cfoutput>
			<cfif currentRow is not keepThisRowID>
				<cfset querySetCell (Query,field,Query[field][keepThisRowID] & delimiter & Query[field][currentRow],keepThisRowID)>
			</cfif>
		</cfoutput>
	</cfoutput>
	<cfquery name = "result" dbtype="query">
	select #columnList#
	from Query
	where  keepthis = 1
	</cfquery>

	<cfreturn result>

</cffunction>

<!--- I want the selection titles in one field --->
<cfset getRecentDownloads = coalesceField(query = getRecentDownloads,groupby= "commid",field="selectionName",delimiter="<BR>")>

<cf_head>
	<SCRIPT type="text/javascript">
	<!--

	function checkForm (btn) {
		var form = document.ThisForm;
		var msg = "";


<CFIF frmPersonIDs EQ "" and numberOfrecords GT 0>
		if (form.frmSelectID.selectedIndex != -1) {
			if (form.frmSelectID.options[form.frmSelectID.selectedIndex].value == 0) {
				msg += "\n   * You must choose a selection.";
			}
		} else {
			msg += "\n   * You must choose a selection.";
		}
</CFIF>

		if (form.frmTitle.value == "") {
			msg += "\n   * Audit Reference";
		}
		//if (form.frmDescription.value == "") {
		//	msg += "\n   * Purpose";
		//}
		if (msg != "") {
			msg = "\nYou must enter the following information about the download:\n\n" + msg;
			alert(msg);
		} else if (btn == "download" || btn == "save") {
			if (btn == "download") {
				form.frmCommFormLID.value = 4;
			}
			form.frmBtn.value = btn;
			form.submit();
		}

		return;

	}
	//-->
	</SCRIPT>
</cf_head>

<CFOUTPUT>
	<!--- WAB, added full path to the action, if this template is included from downdownload the action gets all confused --->
	<FORM NAME="ThisForm" ACTION="#thisDir#/downcreate.cfm" METHOD=POST>
		<CF_INPUT TYPE="HIDDEN" NAME="frmProjectRef" VALUE="#qryProjectRef#">
		<INPUT TYPE="HIDDEN" NAME="frmTitle_required" VALUE="Subject">
		<CF_INPUT TYPE="HIDDEN" NAME="frmCommFormLID" VALUE="#qryCommFormLID#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmCommTypeLID" VALUE="#qryCommTypeLID#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#qryCommID#">
		<INPUT TYPE="HIDDEN" NAME="frmNotes" VALUE="">
		<CF_INPUT TYPE="HIDDEN" NAME="frmEmailTemplate" VALUE="#frmemailtemplate#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
</CFOUTPUT>

<CFIF task IS "edit">
	<INPUT TYPE="HIDDEN" NAME="frmBtn" VALUE="save">
	<!--- <INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="<CFOUTPUT>#qryCommID#</CFOUTPUT>"> --->
	<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(qryLastUpdated)#">
<CFELSE>
	<INPUT TYPE="HIDDEN" NAME="frmBtn" VALUE="send">
</CFIF>

<cf_relayFormDisplay>
<CFIF Trim(qryCommFormText) IS NOT "">
	Format:	<CFOUTPUT>#htmleditformat(qryCommFormText)#</CFOUTPUT>
</CFIF>
<div class="grey-box-top">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="form-group">
			    <cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmTitle" currentValue="#qryTitle#" label="Audit Reference" required="true" size="30" maxLength="30">
			</div>
		</div>
	    <div class="col-xs-12 col-sm-6">
			<div class="form-group">
			    <cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmDescription" currentValue="#qryDescription#" label="Purpose" size="60" maxLength="80">
			</div>
		</div>
	</div>
	<CFIF frmPersonIDs EQ "">
		<div class="form-group">

		    	<label for="frmSelectID" class="required">Highlight the Selections you wish to download:</label>
	<!--- 		<CFIF getSelections.RecordCount GT 0>
				There is a column called recordcount in the selection table.  If the first selection in the list happens to have zero records in it then everything goes haywire.  Calculate a record count from first principles
				--->

				<CFIF numberOfrecords GT 0>
				<SELECT class="form-control" NAME="frmSelectID" SIZE="<CFIF numberOfrecords GT 10>10<CFELSE><CFOUTPUT>#htmleditformat(numberOfrecords)#</CFOUTPUT></CFIF>" MULTIPLE placeholder="This is a placeholder">
					<CFOUTPUT QUERY="getSelections">
						<OPTION VALUE="#SelectionID#">#DateFormat(datecreated, "dd-mmm-yy")# - #htmleditformat(Title)# - #htmleditformat(Records)# records</OPTION>
					</CFOUTPUT>
				</SELECT>

		</div>
		<div class="row">
			<div class="col-xs-12">
				<p>If a person is tagged in any of the selections highlighted,
				they will be included in the export, whether or not they are untagged in any other selection.</p>
				<CFELSE>
					<P>Sorry, you have no selections.</p>
				</CFIF>
			</div>
		</div>
	</CFIF>
	<CFIF application.com.login.checkInternalPermissions("downTask","Level2") AND #task# IS NOT "edit">
		<div class="row">
		   <div class="col-xs-12">
			   <p>Please complete the Audit Reference and Purpose to start the export.</p>
			</div>
		</div>

		<!---  	WAB 1999-02-11  I have used a bit of a hack here.
		When calling this from the popup window I set qrycommformLID to 4 (for download) or 2 (for send)
		This allows me to only display the required button
		When called normally, qrycommformLID is 0 so both buttons show
		Although I have set qrycommformLID to 2, there is nothing to stop the user chosing to send a fax on the next screen--->

	<CFIF qryCommFormLID IS 4 OR qryCommFormLID IS 0>
		<div class="row">
			<div class="col-xs-12 margin-bottom-15">
				<!--- <INPUT type="button" onclick="checkForm('download');" value="Continue" class="btn btn-primary"> --->
				<cf_relayFormElementDisplay relayFormElementType="button" currentValue="Continue" label="" onclick="checkForm('download');">
			</div>
		</div>
	</CFIF>
	<CFELSEIF #checkPermission.UpdateOkay# GT 0>
		<div class="row">
			<div class="col-xs-12 margin-bottom-15">
	   			<!--- <INPUT type="button" onclick="checkForm('save');" value="Save" class="btn btn-primary"> --->
				<cf_relayFormElementDisplay relayFormElementType="button" currentValue="Save" label="" onclick="checkForm('save');">
			</div>
		</div>
	</CFIF>
	<div class="row">
		<div class="col-xs-12">
			<cfif getRecentDownloads.recordcount gt 0>
			<div Class="Label" valign="top" height = "50"></div>
			<p Class="Label">Retrieve Recent Download</p>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<cfset link = "/download/dodownload.cfm?frmlastupdated=0&DownloadWithCFContent=1&frmCommid=">
		<cf_tableFromQueryObject
		queryObject = #getRecentDownloads#
		showTheseColumns = "createdformatted,Title,Description,SelectionName,FileSize"
		columnHeadingList = "Date,Title<BR>(Click to Download),Description,Selection Used,Size<BR>(KB)"
		useinclude=false
		HidePageControls = true
		keycolumnlist = 'createdformatted,Title,Description,SelectionName,FileSize'
		keycolumnurllist = "#link#,#link#,#link#,#link#,#link#"
		keycolumnkeylist = "commid,commid,commid,commid,commid"
	>
	</div>
</div>
</cfif>
</cf_relayFormDisplay>
</FORM>