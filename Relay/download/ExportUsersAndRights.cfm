<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB 2008/09/09

Testing possibility of using the download code to create a special file without using the front end 
While investigating stuff for Sach

Just creates the variables which would be expected from a form POST

Ideally would make the download code into some functions which would
	i) populate a query
	ii) export the query as excel (which could be done by tfqo)
	
would be able to take 
	any list of flags, 
	any list of pol fields, 	
	a selectionid or query
	
Actually then becomes very similar to some of the view and grid code - infact they both use the same functions for creating the flag joins	
 
 SSS 2008/09/25 changed the commID to a varible that can be passed in
 SSS 2009/01/06 added ShowFlagsByName switch so that flag names would not show
 SSS 2009/01/19 Added a switch that will make the columns database safe
 SSS 2009/01/20 Added a loop so that we can get all the flags for person organisation and location
 WAB 2009/02/12  Changes name of downloadFileFormat variable to frmdownloadFileFormat
 --->
	<cfparam name="CommID">
	<cfset FlagNamelist = "person,location,organisation">
	
	<cfloop index="flagindex" list="#FlagNameList#" delimiters=",">
	<!--- this gets every person flag, could be extended with a loop to get all other flags as well --->
	 	<CFQUERY NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
		select ft.name + '_' + convert(varchar,flaggroupid)  as flag from flagGroup fg inner join flagType ft on fg.flagTypeID = ft.flagTypeID 
		where entityTypeID =  <cf_queryparam value="#application.entitytypeID[flagindex]#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and fg.flagtypeid in (2,3,4,5,6)   <!--- all the standard flag types These are flag types ie boolean, integer --->
		</CFQUERY>
		
		<!--- create required form variables --->
		<cfset form["flagList#flagindex#"] = valueList(getFlags.flag)> 
		<CFLOOP QUERY="getFlags">
			<cfset form[flag] = "yes">
		</CFLOOP>
	</cfloop>

	
	<cfset formatName = "Support Instance">
	
	<CFQUERY NAME="checkForExistingFormat" DATASOURCE="#application.SiteDataSource#">
	SELECT * 
	FROM downloadformat 
	where  Name =  <cf_queryparam value="#formatName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
	
	
	<cfif checkForExistingFormat.recordCount is 0>
		<CFQUERY  NAME="insertdownloadformat" DATASOURCE="#application.SiteDataSource#">
				Insert into downloadformat (Name)  
	  			values (<cf_queryparam value="#formatName#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				SELECT scope_identity() AS itemID
		</cfquery>
		<cfset downloadFormatID = insertdownloadformat.itemID>
	<cfelse>
		<cfset downloadFormatID = checkForExistingFormat.itemID>
		<CFQUERY NAME="deleteOldDef" DATASOURCE="#application.SiteDataSource#">
		delete from downloadFormatDef where DownloadFormatID =  <cf_queryparam value="#downloadFormatID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
	</cfif>

	<CFQUERY NAME="insertdownloadformats" DATASOURCE="#application.SiteDataSource#">
 	<!--- Get all columns in the POL Tables, aliased if names are duplicated --->
	insert into DownloadFormatDef
	 (DownloadFormatID, tablesource, fieldsource,	headinglabel, sortOrder) 

	select  <cf_queryparam value="#downloadFormatID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		cols1.table_name, 
		cols1.column_name, 	
		case when cols2.column_name is not null  then cols1.table_name + '_' + cols1.column_name else cols1.column_name end as heading,
		case when cols1.table_name = 'organisation' then 1000 when cols1.table_name = 'location' then 2000 else 3000 end  + cols1.ordinal_position
	from 
		information_schema.columns cols1 
		left join 
		information_schema.columns cols2 on  cols2.table_name in ('person','location','organisation') and cols1.column_name = cols2.column_name and cols2.table_name <> cols1.table_name and cols1.column_name not in ('personid','locationid','organisationid')
	where cols1.table_name in ('person','location','organisation')

	union 

 	<!--- Additional Columns--->	
	select #downloadFormatID#,'location','COUNTRYDESCRIPTION','Country',2000 + (select ordinal_position from information_schema.columns where table_Name = 'location' and column_name = 'countryid')

	</CFQUERY>


	
	<cfset frmdata = "person">  <!--- download person data --->
	<cfset frmlastupdated= now()>  <!--- just a field which is required --->
	 <!---<cfset form.frmdownloadformat= #getmaxdownloadformat.value#>  defines what fields to output --->
	 <cfset form.frmdownloadformat= #downloadFormatID#>  <!--- defines what fields to output --->
	
	<cfset frmDownloadWithCFContent= 0>
	<cfset frmPersonIDs  = "">
	<cfset downloadCountryRights= true>
	<cfset downloadUserGroups= true>
	<cfset OverrideToRegenerateFile = true>
	<cfset GenerateDatabaseCleanColumns = true>
	<cfset downloadFileFormat = "CSV">
	
	<cfset frmCommID = CommID>  <!--- needs a selection which has already been downloaded once as a communication - need to be logged in as comm owner too. Need to work around this at some point. --->

	<!--- this will not output the written out puts --->
	<cfset ShowFlagsByName = false>
	<cfinclude template = "\relay\download\dodownloadV3.cfm">
	
	<!--- File name is returned (accidentally as fullname) 
	<cffile action="COPY" source="#fullname#" destination="#replace(fullname,".csv",".wab")#" >--->
	
	
	<CFQUERY NAME="deletedownloadformat" DATASOURCE="#application.SiteDataSource#">
	delete FROM DownloadFormatDef where DownloadFormatID =  <cf_queryparam value="#downloadFormatID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	delete FROM DownloadFormat where itemid =  <cf_queryparam value="#downloadFormatID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	

