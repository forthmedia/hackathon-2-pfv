<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 1999-10-04  KT	Download only so remove references to sending e-mail/faxes --->

<!--- added by WAB to get it to work --->
<CFPARAM name="frmFlagID" default="">

<!--- NJH 2011/11/09 - moved security to directorySecurity.xml
<CFIF NOT checkPermission.AddOkay GT 0>
	You are not authorised to use this function
		<!--- <CFINCLUDE TEMPLATE="downheader.cfm"> --->
	<CF_ABORT>
</CFIF> --->

<CFPARAM NAME="frmProjectRef" DEFAULT="">
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">

<cfif not isDefined("frmData")>
	<CFQUERY NAME="insertComm" DATASOURCE="#application.SiteDataSource#">
	INSERT INTO Communication (ProjectRef, Title, Description, CommTypeLID, CommFormLID, Notes, SendDate, Sent, TestFlag, CreatedBy, Created, LastUpdatedBy, LastUpdated #IIF(frmFlagID NEQ "", DE(", FlagID"), DE(""))#) 
	VALUES (<cf_queryparam value="#frmProjectRef#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Left(frmTitle, 30)#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#Left(frmDescription, 30)#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#frmCommTypeLID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmCommFormLID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmNotes#" CFSQLTYPE="CF_SQL_VARCHAR" >, null, 0, 0, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, #FreezeDate# #IIF(frmFlagID NEQ "", DE(", #frmFlagID#"), DE(""))#)
	</CFQUERY>
</cfif>

<CFQUERY NAME="getCommID" DATASOURCE="#application.SiteDataSource#">
	SELECT Max(CommID) AS NewCommID
	  FROM Communication
	 WHERE 
	 <cfif not isDefined("frmData")>
	 	LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND
	 </cfif>
	 	LastUpdatedBy=#request.relayCurrentUser.usergroupid#
</CFQUERY>

<CFIF frmBtn eq "download" AND getCommID.NewCommID neq "">
	<!--- communication is a download so go to the confirm screen --->
	<CFSET frmcommID = getCommID.NewCommID>
	<cfif isDefined("frmData")>
		<CFSET frmLastUpdated = #frmLastUpdated#>
	<cfelse>
		<CFSET frmLastUpdated = CreateODBCDateTime(FreezeDate)>
	</cfif>
	<CFINCLUDE TEMPLATE="downconfirm.cfm">
	<CF_ABORT>
		
<CFELSEIF getCommID.NewCommID eq "">	
	<!--- aggregate functions sometimes return one empty record --->
	<CFSET Message="Your download could not be added.">
	<CFINCLUDE TEMPLATE="downheader.cfm">
	<CF_ABORT>

<CFELSE>
	<!--- otherwise assume they are only saving --->
	<CFSET Message="Your download '#frmTitle#' has been added.">
	<CFINCLUDE TEMPLATE="downheader.cfm">
	<CF_ABORT>
	
</CFIF>

