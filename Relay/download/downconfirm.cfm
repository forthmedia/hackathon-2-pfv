<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---

1999-01-25  WAB	Added code to allow sending to a list of people defined in frmpersonid and to run in a popup window
1999-09-30  KT		Remove all references to e-mail and fax and add Address Format Definitions
1999-12-09 WAB 	changed extension of download file to csv.  When it is text, IE5 just opens it without giving the save option
2001-05-03	WAB 	added support for organisationflags
2001-05-03	WAB 	added radio button so that can download using CFCONTent or as a link , default can be set using application. downloadWithCFContent = true/false
2005-05-17 WAB 	altered query to deal with selections shared with usergroups
2008/03/26   WAB   changed ordering of flagGroups back to orginal intention so that things are grouped properly
2008/11/19	WAB		Flaggroup query not connecting out to the rightsGroup table, so not getting rights inherited from groups
2009/05/14	NJH	 Bug Fix Trend Support Issue 2152 - added translate tags
2009/11/24	NJH	P-LEX041 Req 2 - added "Other options" which has personal points balances at the moment.
2012-11-26	PPB	Case 432122 checkSelection query was failing due to precision of milliseconds
2013-05-14 	NYB Case 435204 issue 2 - query wasn't returning flags with a scope of 0
2013-11-05		WAB 	CASE 437307 Support for Asynchronous download - remove JS message, post to same window
2015-11-13  ACPK    Added Bootstrap to form
--->

<cf_translate>

<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<!---
2003-03-22 in application .cfm
<CFSET securitylist = "DownTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">
 --->
<!--- NJH 2011/11/09 - moved security to directorySecurity.xml
<CFIF NOT checkPermission.AddOkay GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="downheader.cfm">
	<CF_ABORT>
</CFIF> --->

<!---   the application .cfm checks for downtask privleges but
		we also need to check for select privledges
		we can rerun the qrycheckperms but NOTE we have now lost
		the application .cfm query variables.
--->

<CFSET securitylist = "SelectTask:read">
<cfinclude template="/templates/qrycheckperms.cfm">

<CFQUERY NAME="checkSelection" DATASOURCE="#application.SiteDataSource#">
	SELECT CommID FROM Communication
	 WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 AND Abs(DateDiff(ms,LastUpdated,<cf_queryparam value="#frmLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)) < 1000 	<!--- 2012-11-26 PPB Case 432122 ignore a difference of less than a second --->
</CFQUERY>

<CFIF checkSelection.Recordcount IS 0>
	<CFSET message = "The communication record has been changed by another user since you last downloaded it.  Please begin again.">
	<CFINCLUDE TEMPLATE="downheader.cfm">
	<CF_ABORT>
</CFIF>

<!--- check to see if there are already selections for this communciation --->
<CFQUERY NAME="anySelections" DATASOURCE="#application.SiteDataSource#">
	SELECT cs.CommID from CommSelection AS cs
		WHERE cs.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<!--- GCC First time through - when reloaded by radio change skips this --->
<CFIF not isdefined("frmData")>
	<CFIF anySelections.RecordCount IS NOT 0>
		<CFSET message = "There are already selections for this communication.  Please create a new communication.">
		<CFINCLUDE TEMPLATE="downheader.cfm">
		<CF_ABORT>
	</CFIF>


	<CFIF IsDefined("frmSelectID") or frmpersonids is not "">
		<CFIF frmpersonids is "">
	<!--- 		<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
				SELECT #frmCommID#, s.SelectionID, 0
			  	FROM Selection AS s
			  	WHERE s.SelectionID IN (#frmSelectID#)
			    AND s.CreatedBy = #request.relayCurrentUser.usergroupid#
			</CFQUERY> --->
			<!--- can communciate to any selections that are shared as well as owned --->
			<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
				SELECT distinct <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, s.SelectionID, 0
			  	FROM Selection AS s
						inner join
					 SelectionGroup AS sg on sg.SelectionID = s.SelectionID
							inner join
					RightsGroup rg on sg.usergroupid = rg.usergroupid  <!--- WAB 2005-05-17 added this into deal with sharing with usergroups --->
			  	WHERE s.SelectionID  IN ( <cf_queryparam value="#frmSelectID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				AND rg.personid = #request.relayCurrentUser.personid#
			</CFQUERY>

		<CFELSE>
	 		<!--- This communication is to a list of people, not a selection - add a dummy record (needed in get comminfo query - not sure whether this is best way to do this) --->
			<CFQUERY NAME="saveSelection" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO CommSelection (CommID,SelectionID,CommSelectLID)
				Values( <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0, 11)
			</CFQUERY>
			<CFPARAM name="frmselectid" default="">   <!--- needed later in template by regular communication --->
		</CFIF>

	<CFELSE>
		<CFSET message = "You must choose a selection.">
		<CFINCLUDE TEMPLATE="downheader.cfm">
		<CF_ABORT>
	</CFIF>
</CFIF>
<!--- Use the communication to store the last updated info --->
<CFQUERY NAME="checkSelection" DATASOURCE="#application.SiteDataSource#">
	UPDATE Communication
	SET LastUpdated =  getdate()
	WHERE CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >  --xxxx
</CFQUERY>

<CFQUERY NAME="getCommInfo" DATASOURCE="#application.SiteDataSource#">
		SELECT c.Title,
			   c.Description,
			   cs.SelectionID,
			   c.CommFormLID,
			   c.SendDate,
			   c.LastUpdated
		  FROM Communication AS c, CommSelection AS cs
		 WHERE c.CommID =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		   AND c.CommID = cs.CommID
	       AND c.sent = 0 <!--- false --->
</CFQUERY>

<CFSET downloadname = "address">
<CFIF getCommInfo.RecordCount IS NOT 0>
	<CFIF Trim(getCommInfo.Title) IS NOT "">
		<!--- remove all non-alpha and non-numeric characters--->
		<CFSET downloadname = REReplace(getCommInfo.Title,"[^a-z,A-Z,0-9]","","ALL")>
	</CFIF>
</CFIF>

<!--- we assume that LookupID 4 is download
		later in this template
		but if this is a download, then
		there will not be any records
		in CommFile
--->
<!--- New Flags --->

<!--- Get USER countries --->
<!--- <cfinclude template="/templates/qrygetcountries.cfm">
 --->
<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<!--- <CFQUERY NAME="getCountryGroups" DATASOURCE="#application.SiteDataSource#">
SELECT DISTINCT b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE (b.ISOcode IS null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
  AND a.CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
ORDER BY b.CountryID
</CFQUERY>

<CFSET UserCountryList = "0,#Variables.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF> --->

<!--- 2008/11/19	WAB	altered query to get rights correct	--->
<!--- NJH 2016/09/23 PROD2016-2383 - show flaggroups even if expiry not set --->
<CFQUERY NAME="getFlagGroups" DATASOURCE="#application.SiteDataSource#">
SELECT
FlagGroup.FlagGroupID AS Grouping,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.Name,
FlagGroup.Description,
FlagGroup.FlagTypeID,
FlagGroup.ParentFlagGroupID,
flagType.Name as typename,flagType.dataTable as typedata,
FlagGroup.OrderingIndex AS parent_order,
0 AS child_order,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count
FROM FlagGroup
		inner join FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID
WHERE FlagGroup.Active <> 0
and flaggroup.flagtypeid not in (11)
AND (FlagGroup.expiry is null or FlagGroup.Expiry > getDate())
AND ParentFlagGroupID = 0
AND FlagGroup.EntityTypeID in  (0,1,2)
<!--- 2013-05-14 NYB Case 435204 - added "0,":--->
AND FlagGroup.Scope  IN (0,<cf_queryparam value="#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (FlagGroup.DownloadAccessRights = 0 AND FlagGroup.Download <> 0)
	OR (FlagGroup.DownloadAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid
			WHERE FlagGroup.FlagGroupID = fgr.FlagGroupID
			AND rg.personid=#request.relaycurrentuser.personid#
			AND fgr.Download <> 0)))
UNION
SELECT
FlagGroup.ParentFlagGroupID,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.Name,
FlagGroup.Description,
FlagGroup.FlagTypeID,
FlagGroup.ParentFlagGroupID,
flagType.Name as typename,flagType.dataTable as typedata,
p.OrderingIndex,
FlagGroup.OrderingIndex,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID)
FROM FlagGroup
	inner join FlagGroup as p on p.FlagGroupID = FlagGroup.ParentFlagGroupID
		inner join FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID
WHERE FlagGroup.Active <> 0
and flaggroup.flagtypeid not in (11)
AND (FlagGroup.expiry is null or FlagGroup.Expiry > getDate())
AND FlagGroup.ParentFlagGroupID <> 0
AND p.ParentFlagGroupID = 0
AND p.EntityTypeID in  (0,1,2)
AND flaggroup.EntityTypeID in  (0,1,2)
<!--- 2013-05-14 NYB Case 435204 - added "0,":--->
AND FlagGroup.Scope  IN (0,<cf_queryparam value="#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (FlagGroup.DownloadAccessRights = 0 AND FlagGroup.Download <> 0)
	OR (FlagGroup.DownloadAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid
			WHERE FlagGroup.FlagGroupID = fgr.FlagGroupID
			AND rg.personid=#request.relaycurrentuser.personid#
			AND fgr.Download <> 0)))
<!--- WAB 2008/03/26
	the order by clause has been adjusted here but it means that parents are no longer with their children so the report looks very odd
	maybe this needs some more looking at, but for the time being have changed back to something like the original intention
ORDER BY <!--- grouping, ---> EntityTypeID<!--- , parent_order,  child_order --->, Name --->
ORDER BY   EntityTypeID desc, parent_order,  grouping, child_order , flaggroup.Name

</CFQUERY>

<CFQUERY NAME="GetAddressFormats" DATASOURCE="#application.SiteDataSource#">
	SELECT ItemID, Name
	FROM DownloadFormat
	ORDER BY Name
</CFQUERY>


<CFIF getCommInfo.RecordCount IS NOT 0>

<!--- =====================
	 qryCommCounts.cfm
	 ======================
	required inputs:
		a list of selectionIDs: Variables.selectIDlist
		the CommFormLID of the communication: Variables.thisCommFormLID
	 outputs (if record count is > 0):
	 	SelectTitleList (a comma separated list of all comm titles)
		TagPeople
		FeedbackCounts
--->
	<CFSET selectIDlist = ValueList(getCommInfo.SelectionID)>
	<CFSET thisCommFormLID = getCommInfo.CommFormLID>

	<CFINCLUDE TEMPLATE="qryCommCounts.cfm">

	<CFIF TagPeopleCount IS 0>
		<CFSET message = "You must choose a selection which includes tagged people.">
		<CFINCLUDE TEMPLATE="downheader.cfm">
		<CF_ABORT>
	</CFIF>

</CFIF>



<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">
function setDate(form) {
	var day = form.frmDay.options[form.frmDay.selectedIndex].value;
	var month = form.frmMon.options[form.frmMon.selectedIndex].value;
	var year = form.frmYear.options[form.frmYear.selectedIndex].value;
	var datestring = day + "-" + month + "-" + year;
//	document.FormCommSend.frmSendDate.value= datestring;
//	document.FormDownload.frmSendDate.value= datestring;
	form.frmSendDate.value= datestring;

}
function checkForm() {
	var form = document.ThisForm;
	if (form.frmDay) {
		setDate(form);
	} else {
		// assume downloading
		var msg = "\n\nThe system needs to prepare a text file for you which may take a few moments.  \n\nPlease be patient.";
		alert(msg);
	}
	form.submit();

}

function reloadForm() {
	var form = document.ThisForm;
	form.target = "_self";
	form.frmBtn.value = "download";
	form.action = 'downcreate.cfm';
	form.submit();
}
</SCRIPT>
</cf_head>



<!--- <CFINCLUDE TEMPLATE="downtophead.cfm"> --->

<CFIF getCommInfo.RecordCount IS NOT 0>

	<CFSET DefaultSend = DateAdd("d",0,Now())>
	<cfoutput>
		<form class="form-horizontal" action="#thisDir#/doDownload.cfm?ID=#RandRange(5,500)#" method="post" name="ThisForm" id="ThisForm" >
			<CF_INPUT TYPE="HIDDEN" NAME="frmSelectID" VALUE="#frmSelectID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="#frmCommID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmpersonids" VALUE="#frmpersonids#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmruninpopup" VALUE="#frmruninpopup#">
			<INPUT TYPE="HIDDEN" NAME="OverrideToRegenerateFile" VALUE="true">
			<CF_INPUT TYPE="HIDDEN" NAME="frmLastUpdated" VALUE="#CreateODBCDateTime(getCommInfo.LastUpdated)#">
			<INPUT TYPE="HIDDEN" NAME="frmBtn" VALUE="">
	</cfoutput>
	    <h3>Export Summary</h3>
	    <div class="form-group">
			<div class="col-xs-6">Title:</div>
			<div class="col-xs-6">
				<CFOUTPUT>
					#htmleditformat(getCommInfo.Title)#
				</CFOUTPUT>
			</div>
			<div class="col-xs-6">Purpose:</div>
			<div class="col-xs-6">
				<CFOUTPUT>
					#htmleditformat(getCommInfo.Description)#
				</CFOUTPUT>
			</div>
		</div>

		<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>

		<CFIF #Variables.SelectTitleList# IS NOT "">
		  <div class="form-group">
		  	<div class="col-xs-12">
		  		<h4>Selection(s):</h4>
		  		<cfparam name="frmData" default="per">
		  		<CFOUTPUT>
		  			#htmleditformat(Variables.SelectTitleList)#
		  			<div class="col-xs-12">
		  				<div class="radio">
		  					<label>
		  						<INPUT type="radio" name="frmData" value="per" onclick="reloadForm();" <cfif frmData eq "per">checked</cfif>> All Data - #htmleditformat(Variables.TagPeopleCount)# individual(s) are tagged
		  					</label>
		  				</div>
		  			</div>
		  			<div class="col-xs-12">
		  				<div class="radio">
		  					<INPUT type="radio" name="frmData" value="loc" onclick="reloadForm();" <cfif frmData eq "loc">checked</cfif>> Just Download Locations - #htmleditformat(Variables.TagLocationCount)# location(s) are tagged
		  				</div>
		  			</div>
		  			<div class="col-xs-12">
		  				<div class="radio">
		  					<INPUT type="radio" name="frmData" value="org" onclick="reloadForm();" <cfif frmData eq "org">checked</cfif>> Just Download Organisations - #htmleditformat(Variables.TagOrganisationCount)# organisation(s) are tagged
		  				</div>
		  			</div>
		  		</CFOUTPUT>
		  	</div>
		  </div>


			<CFIF getCommInfo.CommFormLID IS 4>
				<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>

				<h4>Flag(s):</h4>
				<p>Use checkboxes below to select flag data required (if any).
				The selection may not include any individuals with flag data.</p>

				<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>

				<h4>Address Format:</h4>
			    <div class="form-group">
                       <div class="col-xs-12">
					    <label for="frmDownloadFormat">Please select your required address format for this download.</label>
						<SELECT ID="frmDownloadFormat" CLASS="form-control" NAME="frmDownloadFormat">
							<CFOUTPUT QUERY="GetAddressFormats">
								<OPTION VALUE="#ItemID#" <CFIF name EQ "Default">SELECTED</CFIF> >#htmleditformat(Name)#</OPTION>
							</CFOUTPUT>
						</SELECT>
					</div>
				</div>

				<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>

				<h4>Other Options:</h4>
				<div class="form-group">
				    <div class="col-xs-12">
				      <div class="checkbox">
				        <label>
							<input type="checkbox" value="true" name="downloadRWPoints" id="downloadRWPoints"> Personal Incentive Balance
						</label>
				      </div>
				    </div>
				</div>

				<CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT>

				<!--- GCC removed at SWJs request - Excel only --->
				<INPUT type="hidden" name="frmDownloadWithCFContent" value= "1">
				<!--- <TR>
					<TD VALIGN="TOP" ALIGN="LEFT" COLSPAN=3>
						<B>File Format:</B>
					</TD>
				</TR>
				<TR>
					<TD>
						&nbsp;
					</TD>
					<TD COLSPAN="3">

						<INPUT type="radio" name="frmDownloadWithCFContent" value= "1" <CFIF isDefined("application. downloadWithCFContent") and application. downloadWithCFContent is true>checked </cfif> > Download into Excel<BR>
						<INPUT type="radio" name="frmDownloadWithCFContent" value= "0" <CFIF not isDefined("application. downloadWithCFContent") or application. downloadWithCFContent is false>checked </cfif> > Download as a Text File

					</TD>
				</TR> --->

			</CFIF>
		<CFELSE>
					<P>A related selection could not be found.</P>
		</CFIF>

<CFIF getCommInfo.CommFormLID IS 4>   <!--- added by WAB 1998-12-28  --->
	<!--- New Flags --->
	<!--- Display choice of Flag Groups to download with selection --->
	<CFIF getFlagGroups.Recordcount IS 0>

		<P>There are no Flag Groups.

	<CFELSE>

		<CFSET flagList = arrayNew(1)>  <!--- List of flag groups displayed with checkboxes = with flags idex is entityTypeID--->
		<CFSET x= arraySet(flaglist,1,3,"Group_0")>
		<CFSET flagTypeName = arrayNew(1)>
		<CFSET flagTypeName[0+1] = "per">
		<CFSET flagTypeName[1+1] = "loc">
		<CFSET flagTypeName[2+1] = "org">

		<CFSET cnt = 0>
		<P><TABLE CLASS="table table-hover">
		<TR><TH>Type</TH>
			<TH>&nbsp;</TH>
			<TH>Name</TH>
			<TH>Description</TH>
		</TR>
		<cfparam name="excludedFlagList" default="">
		<cfif frmData eq "org">
				<cfset excludedFlagList="loc,per">
		<cfelseif frmData eq "loc">
				<cfset excludedFlagList="per">
		</cfif>
		<CFOUTPUT QUERY="getFlagGroups" GROUP="grouping">
			<CFSET thisFlagGroup = FlagGroupID>
			<!--- Wab 2010/09/09 now from query
			<!---<CFSET typeName = ListGetAt(application. typeList,FlagTypeID)> NJH 2010/08/05 RW8.3 - scoped variable --->
			<cfset typeName = application. flagGroup[thisFlagGroup].flagType.name>
			<!---<CFSET typeData = ListGetAt(application. typeDataList,FlagTypeID)> NJH 2010/08/05 RW8.3 - scoped variable --->
			<cfset typeData = application. flagGroup[thisFlagGroup].flagType.datatable>
			--->
			<CFSET cnt = cnt + 1>
			<CFSET flagList[EntityTypeID+1] = flagList[EntityTypeID+1] & ",#typeName#_#thisFlagGroup#">
			<cfif findnocase(#flagTypeName[EntityTypeID+1]#,excludedFlagList) eq 0>

				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<TD>#flagTypeName[EntityTypeID+1]#</TD>
					<TD><CFIF flag_count IS NOT 0><CF_INPUT TYPE="CHECKBOX" NAME="#typeName#_#thisFlagGroup#" VALUE="yes"><CFELSE>-</CFIF></TD>
					<TD><CFIF Name IS NOT "">#HTMLEditFormat(Name)#<CFELSE>No Name</CFIF> <cfif flag_count neq 0 and typedata neq "boolean"><cfset flags = application.com.flag.getFlagGroupFlags(flagGroupID=thisFlagGroup)><span title="#valueList(flags.name)#"><img src="/images/misc/iconhelpsmall.gif"></span></cfif></TD>
					<TD><CFIF Description IS NOT "">#HTMLEditFormat(Description)#<CFELSE>-</CFIF></TD>
				</TR>

	 			<CFOUTPUT>
					<CFIF getFlagGroups.ParentFlagGroupID IS NOT 0>
						<CFSET thisFlagGroup = FlagGroupID>
						<!--- Wab 2010/09/09 now from query
						<!---<CFSET typeName = ListGetAt(application.typeList,FlagTypeID)>  NJH 2010/08/05 RW8.3 - scoped variable --->
						<cfset typeName = application.flagGroup[thisFlagGroup].flagType.name>
						<!---<CFSET typeData = ListGetAt(application.typeDataList,FlagTypeID)>  NJH 2010/08/05 RW8.3 - scoped variable --->
						<cfset typeData = application.flagGroup[thisFlagGroup].flagType.datatable>
	 					--->
						<CFSET flagList[EntityTypeID+1] = flagList[EntityTypeID+1] & ",#typeName#_#thisFlagGroup#">

						<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
							<TD>&nbsp;</TD>
							<TD><CFIF flag_count IS NOT 0><CF_INPUT TYPE="CHECKBOX" NAME="#typeName#_#thisFlagGroup#" VALUE="yes"><CFELSE>-</CFIF></TD>
							<TD><CFIF isDefined("Name")><LI>#HTMLEditFormat(Name)#<CFELSE><LI>No Name</CFIF> <cfif flag_count neq 0 and typedata neq "boolean"><cfset flags = application.com.flag.getFlagGroupFlags(flagGroupID=thisFlagGroup)><span title="#valueList(flags.name)#"><img src="/images/misc/iconhelpsmall.gif"></span></cfif></TD>
							<TD><CFIF Description IS NOT "">#HTMLEditFormat(Description)#<CFELSE>-</CFIF></TD>
						</TR>
					</CFIF>
				</CFOUTPUT>
			</cfif>
		</CFOUTPUT>

		</TABLE>
		<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="flagListPerson" VALUE="#flagList[0+1]#">
			<CF_INPUT TYPE="HIDDEN" NAME="flagListLocation" VALUE="#flagList[1+1]#">
			<CF_INPUT TYPE="HIDDEN" NAME="flagListOrganisation" VALUE="#flagList[2+1]#">
		</CFOUTPUT>
	</CFIF>
</CFIF>  <!--- added by WAB 1998-12-28 --->

	<div class="form-group">
	   <div class="col-xs-12">
			<input type="Submit" class="btn-primary btn" value="Do Export">
		</div>
	</div>

<CFELSE>
	<P>The requested export could not be found.</P>
</CFIF>

</FORM>





</cf_translate>

