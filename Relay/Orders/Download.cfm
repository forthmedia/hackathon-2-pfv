<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods
2001-03-28	CPS		use application variables StatusDescriptions 

--->
<CFIF not isdefined("promoid")>
	No promotion specified
	<CF_ABORT>
</cfif>



<CFQUERY NAME="getOrderIDs" datasource="#application.sitedatasource#">
select 	o.OrderID
from 	orders as o, orderitem as i
where 	o.orderid = i.orderid
and 	o.OrderStatus =  <cf_queryparam value="#constConfirmed#" CFSQLTYPE="CF_SQL_INTEGER" >  
and 	promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<CFIF getOrderIDs.recordCount is 0>
	There are no orders to download <CF_ABORT>

</cfif>

<!--- Set the User Variable --->
<CFSET user = #request.relayCurrentUser.usergroupid#>

<!--- Set the Next Order Status to 3 i.e. Download to Client Logic --->
<CFSET nextOrderStatus = #constDownloaded#>

<!--- Set the Path for file output --->
<!--- <CFSET tmpPath = '/orderhistory/'> --->

<!--- Capture this moment in time --->
<CFSET FreezeDate = request.requestTimeODBC>

<!---  
wab modified this, first time through  works out the name of the file, then calls itself again 
with this filename in the URL so that the downloaded file has this name.
--->

<CFIF not isDefined("tmpFileName")>
	<!--- Pad the Date and Time parts with leading zero's if needed --->
	<CFSET tmpFileName = 'OP-' &  '#YEAR(FreezeDate)#'>

	<CFIF LEN(Month(FreezeDate)) IS 1>
		<CFSET tmpFileName = tmpFileName & '0' & Month(FreezeDate)>
	<CFELSE>
		<CFSET tmpFileName = tmpFileName & Month(FreezeDate)>
	</CFIF>

	<CFIF LEN(Day(FreezeDate)) IS 1>
		<CFSET tmpFileName = tmpFileName & '0' & Day(FreezeDate)>
	<CFELSE>
		<CFSET tmpFileName = tmpFileName & Day(FreezeDate)>
	</CFIF>

	<CFIF LEN(Hour(FreezeDate)) IS 1>
		<CFSET tmpFileName = tmpFileName & '0' & Hour(FreezeDate)>
	<CFELSE>
		<CFSET tmpFileName = tmpFileName & Hour(FreezeDate)>
	</CFIF>

	<CFIF LEN(Minute(FreezeDate)) IS 1>
		<CFSET tmpFileName = tmpFileName & '0' & Minute(FreezeDate)>
	<CFELSE>
		<CFSET tmpFileName = tmpFileName & Minute(FreezeDate)>
	</CFIF>

	<CFIF LEN(Second(FreezeDate)) IS 1>
		<CFSET tmpFileName = tmpFileName & '0' & Second(FreezeDate)>
	<CFELSE>
		<CFSET tmpFileName = tmpFileName & Second(FreezeDate)>
	</CFIF>

	<CFIF isDefined("request.relayCurrentUser.personid")>
		<CFSET tmpDownloader = request.relayCurrentUser.userGroupId>
	<CFELSE>
		<CFSET tmpDownloader = 1>
	</CFIF>

	<CFSET tmpFileName = tmpFileName & '-' & tmpDownloader>

	<!--- now call this file again with the name of the file to download  --->
	<CFLOCATION url= "download.cfm/#tmpFileName#.txt?tmpFileName=#tmpFileName#&tmpDownloader=#tmpDownloader#&promoid=#promoid#"addToken="false">

</cfif>

<!--- Write the Document Title Details --->
<CFFILE ACTION="WRITE" 
		FILE="#Path#\orders\orderhistory\#tmpFileName#.3op" 
		OUTPUT="Order Processing - User ID #tmpDownloader# - #DAY(FreezeDate)# #LEFT(MonthAsString(Month(FreezeDate)),3)# #YEAR(FreezeDate)# #Hour(FreezeDate)#:#Minute(FreezeDate)#:#Second(FreezeDate)#">


<CFOUTPUT>#getOrderIDs.recordcount#</cfoutput>
		

<!--- Now Append all the other data --->
<CFLOOP QUERY="getOrderIDs">
	<!--- Get the Order Header Record --->
	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		select 	o.* ,
			(select countrydescription from country where countryid = delCountryID) as delCountryName,
			(select countrydescription from country where countryid = invCountryID) as invCountryName,
			(select firstname + ' ' + lastname from person where personid = o.personid) as orderedby,
			(select sitename from location where locationid = o.locationid) as sitename
		from 	orders as o
		where 	o.orderid =  <cf_queryparam value="#getOrderIDs.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 

	</CFQUERY>

	<!--- Update the status of the selected order to Downloaded to Client Logic i.e. 3 --->
	<CFQUERY NAME="updateOrderItem" datasource="#application.sitedatasource#">			
	UPDATE Orders 
		SET OrderStatus =  <cf_queryparam value="#nextOrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" > 
	WHERE OrderID =  <cf_queryparam value="#getOrderHeader.OrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<!--- now done by trigger
	<!---  Add a record to the History Log--->	
	<CFQUERY NAME="CreateOrderHistoryRecord" datasource="#application.sitedatasource#"> 
		INSERT INTO OrderHistory
		   (<!--- OrderHistoryID,  --->OrderID,OrderNumber, OrderStatus, CreatedBy, Created)
		   
   		SELECT 	<!--- max(h.OrderHistoryID)+1, --->
				#getOrderHeader.OrderID#,
				'xxx',
				#nextOrderStatus#,
				#user#,	
				#freezedate#
		FROM Orders AS o, OrderHistory AS h
	</CFQUERY>
	 --->
	<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 	*
	FROM vOrderItems
	WHERE	orderid =  <cf_queryparam value="#getOrderIDs.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
	AND 	active = 1
	</CFQUERY>
<!--- CPS 280301 The following line was removed from the CFSET tmpText statement as it is now replaced by an application variable --->
<!--- Order Status  : #listfind(orderstatuslist,getorderheader.orderstatus)# --->	
	<CFSET tmpText= '===============================================================================
Order Details
-------------
Order No      : #getorderheader.orderid# 
Placed by     : #getorderheader.orderedby#, #getorderheader.sitename# 
Date Created  : #dateformat(getorderheader.created)# 
Order Status  : #application.StatusDescriptions[getorderheader.orderstatus]#
SSG ID        : #getorderheader.ssgcompanyid#

Delivery Details
----------------
Contact Name  : #getorderheader.delcontact#
Site Name     : #getorderheader.delsitename#
Address 	  : #getorderheader.deladdress1#
                #getorderheader.deladdress2#
                #getorderheader.deladdress3#
                #getorderheader.deladdress4#
                #getorderheader.deladdress5#
                #getorderheader.delPostalCode#
                #getorderheader.delCountryName#
Contact Phone : #getorderheader.contactphone#
Contact Fax   : #getorderheader.contactfax#
Contact Email : #getorderheader.contactemail#
Customer Ref  : #getorderheader.theirordernumber#

Item Details
------------'>


<!---  taken out, not needed for BCG
 Invoice Details
---------------
Contact Name  : #getorderheader.invcontact#
Site Name     : #getorderheader.invsitename#
Address		  : #getorderheader.invaddress1#
                #getorderheader.invaddress2#
                #getorderheader.invaddress3#
                #getorderheader.invaddress4#
                #getorderheader.invaddress5#
                #getorderheader.invPostalCode#
                #getorderheader.invCountryName#
 --->


<CFFILE ACTION="APPEND" 
		FILE="#Path#\orders\orderhistory\#tmpFileName#.3op" 
		OUTPUT="#tmpText#">

		
	<CFLOOP QUERY="getorderitems">
		<CFSET tmpItem = '	SKU                  : #SKU#
	Description          : #LEFT(Description,58)#
	Quantity             : #Quantity#
	List Price           : #decimalformat(UnitListPrice)#
	Discount Price       : #decimalformat(UnitDiscountPrice)#
	Total Discount Price : #decimalformat(TotalDiscountPrice)#
	......................................................
'>

<CFFILE ACTION="APPEND" 
		FILE="#Path#\orders\orderhistory\#tmpFileName#.3op" 
		OUTPUT="#tmpItem#">

	</CFLOOP>
</CFLOOP>



<!---  1999-05-17 WAB removed this bit - path was hardcoded so wasn't working - I have replaced it with a CFcontent, 'cause I could get that to work--->
<!--- 

The Download file has been prepared<P></P>
Please Click the Link below to download the file.<P></P>
<A HREF="/orders/orderhistory/#tmpFileName#.3op</cfoutput>" TYPE="unknown">*** Download Now ***</A><BR>
<A HREF="Report1.cfm">Back to Main Order Processing Screen</A>




 --->
 
		<CFCONTENT TYPE="application/x-unknown"
			DELETEFILE="No"
			FILE="#Path#\orders\orderhistory\#tmpFileName#.3op">
