<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods
WAB		2001-03-21	mod of getOrderHeader so that it will work even if the location has been deleted
WAB 	030200  	Added delCountryName and invCountryName to query
CPS 	05/Apr/01 	Set up frmLocationId, frmPersonId and include SetFocusLevel template 
???		????		someone must have added ignoreGetOrderHeader and ignoreGetOrderValue but don't know who
WAB		2001-06-11		frmLocationId, frmPersonId need to be within the <cfif ignoreGetOrderHeader is false> statement 
NJH		2009/07/09	P-FNL069 - check if user has access to order. If currentOrderID is not defined, stop so that no error is given about currentOrderID is not defined.

 --->

<cfparam name="ignoreGetOrderHeader" default="false">
<cfparam name="ignoreGetOrderValue" default="false">
 
<!--- NJH 2009/07/09 P-FNL069 - ensure that user has access to order first. That means it will be the user who has created the order if on the portal
	or an internal user.
 --->
<cfset hasAccess=false>
<cfif not isDefined("currentOrderID")>
	<cfset currentOrderID=0>
</cfif>

<cfif not request.relayCurrentUser.isInternal>
	<cfquery name="doesUserHaveAccessToOrder" datasource="#application.siteDataSource#">
		select 1 from orders where orderID =  <cf_queryparam value="#currentOrderID#" CFSQLTYPE="CF_SQL_INTEGER" >  and personID = #request.relayCurrentUser.personID#
	</cfquery>
	
	<cfif doesUserHaveAccessToOrder.recordCount eq 1>
		<cfset hasAccess=true>
	</cfif>
<cfelse>
	<cfset hasAccess=true>
</cfif>

<cfif not hasAccess>
	<CF_ABORT>
</cfif>

 
<cfif ignoreGetOrderHeader is false>

	<cfset getOrderHeader = application.com.relayOrder.getOrderHeader(orderId=currentorderid)/>

	<CFSET frmLocationId = getOrderHeader.locationId>
	<CFSET frmPersonId = getOrderHeader.personId>
	<CFINCLUDE TEMPLATE="SetPartnerLevel.cfm"> 
	
</cfif>
	
<cfset getOrderItems = application.com.relayOrder.getOrderItems(orderId=currentorderid)/>

<cfif ignoreGetOrderValue is false>
	<!--- use this to check consistency of tables --->
	<cfset getOrderValue = application.com.relayOrder.getOrderValue(orderId=currentorderid)/>
	
	<CFIF getordervalue.totalvalue IS NOT getordervalue.totalvalue1><cfoutput>Problem with order value (1) getordervalue.totalvalue: #htmleditformat(getordervalue.totalvalue)# getordervalue.totalvalue1: #htmleditformat(getordervalue.totalvalue1)#</cfoutput><CF_ABORT></cfif>
	<CFIF getordervalue.totallistvalue IS NOT getordervalue.totallistvalue1><cfoutput>Problem with order value (2) getordervalue.totallistvalue: #htmleditformat(getordervalue.totallistvalue)# getordervalue.totallistvalue1: #htmleditformat(getordervalue.totallistvalue1)#</cfoutput><CF_ABORT></cfif>
	<CFIF getordervalue.totallistvalue IS NOT getorderheader.orderlistvalue><cfoutput>Problem with order value (3): getordervalue.totallistvalue: #htmleditformat(getordervalue.totallistvalue)# getorderheader.orderlistvalue: #htmleditformat(getorderheader.orderlistvalue)#</cfoutput><CF_ABORT></cfif>
	<CFIF getordervalue.totalvalue IS NOT getorderheader.ordervalue><cfoutput>Problem with order value (4) getordervalue.totalvalue: #htmleditformat(getordervalue.totalvalue)#  getorderheader.ordervalue: #htmleditformat(getorderheader.ordervalue)#</cfoutput><CF_ABORT></cfif>
	<CFSET orderlastupdated = getOrderHeader.LastUpdated>
</cfif>


