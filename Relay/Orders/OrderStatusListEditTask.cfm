<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Mods:  CPS 230401  Call the appropriate procedure when 'approving' or 'declining'
       CPS 100501  Ensure RWApproveOrder is called when an order goes straight to 'Approved' i.e. when frmMode is 'Confirm'
       CPS 210501  Ensure the Points Amount on the WDDX packet is in sync with the order header and order items when uploading
	               quotes from LSS
 --->	
<!--- Get the Created Date --->
<CFSET freezedate=now()>
<CFSET frmOrderStatus = #frmOrderStatus#>
<CFSET frmPromoId = #frmPromoId#>

<CFPARAM NAME="frmMode" default="Approve">

<CFIF frmMode IS "Approve"> 
	<!--- Update the Record to reflect the status change --->

	<CFSTOREDPROC PROCEDURE="CheckAcceptRule" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrderID" VALUE="#frmOrderId#" NULL="No">
		<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="OrderStatus" DBVARNAME="@NewOrderStatus" NULL="No">
	</CFSTOREDPROC>
<CFELSEIF frmMode IS "Decline">
	<!--- coding for Decline Status --->
	
	<CFSTOREDPROC PROCEDURE="CheckDeclineRule" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrderID" VALUE="#frmOrderId#" NULL="No">
		<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="OrderStatus" DBVARNAME="@NewOrderStatus" NULL="No">
	</CFSTOREDPROC>	
	
<CFELSEIF frmMode IS "Confirm">
	<!--- This is the state when the order is being placed and is set in the promotions table
	in promotions.mail column --->
	<CFIF isdefined("CURRENTORDERID")>
		<CFSET frmOrderID = CURRENTORDERID>
	</CFIF>	
	
	<CFSTOREDPROC PROCEDURE="CheckAcceptRule" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrderID" VALUE="#frmOrderId#" NULL="No">
		<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="OrderStatus" DBVARNAME="@NewOrderStatus" NULL="No">
	</CFSTOREDPROC>
</CFIF>

<CFTRANSACTION>

<!--- CPS 230401  Call the appropriate procedure when 'approving' or 'declining' --->
<CFIF "Approve,Confirm" CONTAINS frmMode AND OrderStatus EQ 15> 
	<CFSTOREDPROC PROCEDURE="RWApproveOrder" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrderID" VALUE="#frmOrderId#" NULL="No">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmount" VALUE="#frmPointsAmount#" NULL="No">	
	</CFSTOREDPROC>	
<CFELSEIF frmMode IS "Decline" AND "18,19,20" CONTAINS OrderStatus>
	<CFSTOREDPROC PROCEDURE="RWCreateTransaction" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrganisationID" VALUE="#frmOrganisationId#" NULL="No">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_CHAR" DBVARNAME="@TxTypeId" VALUE="CN" NULL="No">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmount" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmount" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_CHAR" DBVARNAME="@PriceISOCode" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CurrencyPerPoint" NULL="Yes">		
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@OrderID" VALUE="#frmOrderId#" NULL="No">
	</CFSTOREDPROC>
</CFIF>

<CFINCLUDE TEMPLATE="MailRulesBased.cfm">

<CFQUERY NAME="updateOrder" datasource="#application.sitedatasource#">			
	Update Orders 
	SET 	OrderStatus =  <cf_queryparam value="#OrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" > ,
	lastUpdatedby = #request.relayCurrentUser.usergroupid#,
	lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
 	<CFIF IsDefined("PointsAmount") AND PointsAmount NEQ ''>
		,orderValue  =  <cf_queryparam value="#PointsAmount#" CFSQLTYPE="cf_sql_numeric" >
		,orderListValue =  <cf_queryparam value="#PointsAmount#" CFSQLTYPE="CF_SQL_money" > 
	</CFIF>
	<CFIF IsDefined("PurchaseOrderNo")>
		,theirOrderNumber =  <cf_queryparam value="#PurchaseOrderNo#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFIF>	
	where OrderID =  <cf_queryparam value="#frmOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY> 

</CFTRANSACTION>

<CFIF IsDefined("frmUploadLSSQuote") AND frmUploadLSSQuote EQ 1>
	<!---If this template has been included from the Upload of an LSS quote ensure that 
	     the points amount in the WDDX packet matches the figure input by the LSS taking
		 into account the conversion into points from the local currency --->

	<CFSET "form.flag_#getFlag.flagId#_marketingBudget_#frmItemId#" = #PointsAmount#>
	
	<CF_aScreenUpdate> 

	<CFINCLUDE template = "WDDXFlagOrderItemUpdate.cfm">
	
</CFIF>	

<CFIF frmMode IS NOT "Confirm">
	<!--- This is the state when the order is being placed and is set in the promotions table
	in promotions.mail column --->
	<CFINCLUDE TEMPLATE="OrderStatusListEdit.cfm">
</CFIF>


