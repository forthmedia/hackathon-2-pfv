<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:  WAB 030200  Uses variable Delivery Address to get properly formatted address
 --->	
<!---Mail Template which sends mail to disti--->
<CFSET ordertotal = getOrderValue.TotalValue + getOrderHeader.TaxOnOrder + getOrderHeader.ShippingCharge >
<cfset distiordertotal = getOrderValue.TotalDistiValue>

<CFSET tab1 = 15>
<CFSET tab2 = 45>

<CF_MAIL query="getorderitems" 
		TO="#distiEmail#" 
		FROM="#application.ordersEmailFrom#" 
		CC="#copyAllEmailsToThisAddress#"
		SUBJECT="Order Confirmation: Order No. #currentorderid#"
		TYPE="html">

#phr_order_AlertOrdertoDisti#<br>
#phr_order_OrderNumber#: #repeatString(" ",max(0,tab1-Len(phr_order_OrderNumber)))# #currentorderid#<br>
<CFIF getOrderHeader.requiresPurchaseRef EQ 1>
	#phr_order_ResellerPurchaseRef#: #repeatString(" ",max(0,tab1-Len(phr_order_ResellerPurchaseRef)))# #theirordernumber#<br>
</CFIF>
#phr_order_NumberOfItems#: #repeatString(" ",max(0,tab1-Len(phr_order_NumberOfItems)))# #getorderitems.recordcount#

#phr_order_ItemsOrdered#
================================================================================<CFOUTPUT>
#phr_order_ProductCode#: #repeatString(" ",max(0,tab2-Len(phr_order_ProductCode)))# #SKU#<br> 
#phr_Description#: #repeatString(" ",max(0,tab2-Len(phr_description)))# #Description#<br>
#phr_Quantity#: #repeatString(" ",max(0,tab2-Len(phr_Quantity)))# #Quantity#<br>
#phr_order_DiscountedUnitPrice#: #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedUnitPrice)))# #tmpCurrencySymbol# #numberFormat(UnitDiscountPrice,"___,___,___.__")#<br>
#phr_order_DiscountedTotalPrice#: #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedTotalPrice)))# #tmpCurrencySymbol# #numberFormat(TotalDiscountPrice,"___,___,___.__")#<br>
<cfif not UsePoints>#phr_order_DistiDiscountedUnitPrice#: #repeatString(" ",max(0,tab2-Len(phr_order_DistiDiscountedUnitPrice)))# #tmpCurrencySymbol# #numberFormat(unitListPrice * (1 - DistiDiscount),"___,___,___.__")#<br>
#phr_order_DistiDiscountedTotalPrice#: #repeatString(" ",max(0,tab2-Len(phr_order_DistiDiscountedTotalPrice)))# #tmpCurrencySymbol# #numberFormat(Quantity * unitListPrice * (1 - DistiDiscount),"___,___,___.__")#</cfif><br><br>
</CFOUTPUT>
#phr_order_Totaldiscountedvalue#: #repeatString(" ",max(0,tab2-Len(phr_order_TotalDiscountedValue)))# #tmpCurrencySymbol# #numberFormat(getOrderValue.TotalValue,"___,___,___.__")#<cfif getOrderHeader.ShowDeliveryCalculation><br>
#phr_Delivery#: #repeatString(" ",max(0,tab2-Len(phr_Delivery)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.shippingCharge,"___,___,___.__")#</cfif><cfif getOrderHeader.ShowVATCalculation><br>
#phr_VAT#: #repeatString(" ",max(0,tab2-Len(phr_VAT)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.taxOnOrder,"___,___,___.__")#</cfif><cfif getOrderHeader.ShowDeliveryCalculation or getOrderHeader.ShowVATCalculation><br>
#phr_Total#: #repeatString(" ",max(0,tab2-Len(phr_Total)))# #tmpCurrencySymbol# #numberFormat(orderTotal,"___,___,___.__")#</cfif><br>
#phr_order_TotalDistiDiscountedPrice#: #repeatString(" ",max(0,tab2-Len(phr_order_TotalDistiDiscountedPrice)))# #tmpCurrencySymbol# #numberFormat(DistiOrderTotal,"___,___,___.__")#<br>
================================================================================<cfif not (getOrderHeader.ShowDeliveryCalculation and getOrderHeader.ShowVATCalculation)><br>
#phr_Order_DistiAddShippingAndTax#
</cfif>
#phr_order_ShippingDetails#
================================================================================
#phr_order_Thisorderwillbedeliveredto#:<br>
#getorderheader.delcontact#<br>
#getorderheader.delsitename#<br>
#DeliveryAddress#
================================================================================

#phr_order_BillingDetails#
================================================================================
#getorderheader.invcontact#<br>
#getorderheader.invsitename#<br>
#InvoiceAddress#
================================================================================
</CF_MAIL>

