<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History
-----------------

NYB 2009-09-11 - LHID2632 Bug - changed cfmail parameters

--->

<CFQUERY NAME="qry_update_order" datasource="#application.sitedatasource#"> 
	Update Orders
	set OrderStatus =  <cf_queryparam value="#listgetat(form.cboorderstatus,1)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
	TheirOrderNumber =  <cf_queryparam value="#form.TheirOrderNumber#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
	approverNotes =  <cf_queryparam value="#form.approverNotes#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	where orderid =  <cf_queryparam value="#form.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="qry_get_orderPersonDetails" datasource="#application.sitedatasource#"> 
	select * from Orders
	where orderid =  <cf_queryparam value="#form.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="qry_get_PersonDetails" datasource="#application.sitedatasource#"> 
	select L.countryID, LA.languageID, P.PersonID, isnull(p.firstname,' ') + ' '+ isnull(p.lastname,' ') as fullname, o.organisationname
	from Person P
	INNER JOIN Location L on P.locationID=L.locationID
	INNER JOIN Organisation O on O.organisationID=L.organisationID
	INNER JOIN Language LA on P.Language=LA.Language
	where personid =  <cf_queryparam value="#qry_get_orderPersonDetails.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<cfset form.orderstatustext = listgetat(form.cboorderstatus,2)>

<cfset form.orderstatus= listgetat(form.cboorderstatus,1)>
<cfscript>
	application.com.email.sendEmail(personid=qry_get_orderPersonDetails.personID,emailTextID="ClaimsApproverEmail");
</cfscript>

<cfif form.orderstatus is "2002">

	<cfquery name="qry_get_person_org" datasource="#application.sitedatasource#">
		select organisationid from Person where personid =  <cf_queryparam value="#qry_get_orderPersonDetails.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfscript>
		variables.Staffno=application.com.flag.getFlagData(flagid="ActualTechnicianStaffNumber",entityid=qry_get_orderPersonDetails.personid);
		variables.companycode=application.com.flag.getFlagData(flagid="claimsCompanyCode",entityid=qry_get_person_org.organisationid);
		qGetCountries=application.com.commonQueries.getCountries();
	</cfscript>
	<cfset qryText_1714=application.com.flag.getFlagData(flagid="1714",entityid="#form.orderid#")>
	<cfset qryText_1715=application.com.flag.getFlagData(flagid="1715",entityid="#form.orderid#")>
	<cfset qryText_1716=application.com.flag.getFlagData(flagid="1716",entityid="#form.orderid#")>
	<cfset qryText_1782=application.com.flag.getFlagData(flagid="1782",entityid="#form.orderid#")>
	<cfset Text_1714=qryText_1714.data>
	<cfset Text_1715=qryText_1715.data>
	<cfset Text_1716=qryText_1716.data>
	<cfset Text_1782=qryText_1782.data>
	
	<cfquery name="qry_get_orderitems" datasource="#application.sitedatasource#">
		select orderitem.quantity, orderitem.orderID, orderitem.sku, orderitem.productid, product.description from orderitem,product 
		where orderitem.orderid=#form.orderID#
		and orderitem.productid=product.productid
	</cfquery>
		
	<cfoutput query="qry_get_orderPersonDetails">
		<cfset variables.contactname="#invsitename#">
		<cfset variables.contactphone="#invaddress1#">
		<cfset variables.contactfax="#invaddress2#">
		<cfset variables.invsitename="#invsitename#">
		<cfset variables.invaddress1="#invaddress1#">
		<cfset variables.invaddress2="#invaddress2#">
		<cfset variables.invaddress3="#invaddress3#">
		<cfset variables.invaddress4="#invaddress4#">
		<cfset variables.invaddress5="#invaddress5#">
		<cfset variables.invpostalcode="#invpostalcode#">
		<cfset variables.invCountryID="#invcountryid#">
		<cfset variables.invcontact="#invcontact#">
		<cfset variables.contactphone="#contactphone#">
		<cfset variables.contactfax="#contactfax#">
		<cfset variables.contactemail="#contactemail#">
		<cfset variables.delcontact="#delcontact#">
		<cfset variables.ordernotes="#ordernotes#">
		<cfset variables.ordernumber="#ordernumber#">
		<cfset variables.orderdate="#dateformat(orderdate,"DD-MMM-YYYY")#">
		<cfset variables.createddate="#dateformat(created,"DD-MMM-YYYY")#">
		<cfset variables.shippeddate="#dateformat(shippeddate,"DD-MMM-YYYY")#">
		<cfset variables.delsitename="#delsitename#">
		<cfset variables.deladdress1="#deladdress1#">
		<cfset variables.deladdress2="#deladdress2#">
		<cfset variables.deladdress3="#deladdress3#">
		<cfset variables.deladdress4="#deladdress4#">
		<cfset variables.deladdress5="#deladdress5#">
		<cfset variables.delpostalcode="#delpostalcode#">
		<cfset variables.delCountryID="#delcountryid#">
		<cfset variables.frmpersonid="#personid#">
		<cfset variables.frmpromoid="#promoid#">
		<cfset variables.currentOrderID="#orderid#">
		<cfset variables.orderstatus="#orderstatus#">
		<cfset "variables.orders_lastupdated_#orderid#"="">
		<cfset variables.TheirOrderNumber="#TheirOrderNumber#">
		<cfset variables.approverNotes="#approverNotes#">
	</cfoutput>
	<cfquery name="qoq_get_invcountry" dbtype="query">
		select country from qGetCountries
		where countryid=#variables.invcountryID#
	</cfquery>
	<cfquery name="qoq_get_delcountry" dbtype="query">
		select country from qGetCountries
		where countryid=#variables.delcountryid#
	</cfquery>
	
<!--- AJC 2006-12-14 - This is a little bit dirty but works... If the country the email is meant to go
to does not have the translations setup it changes to the default country i.e. UK 9 --->	
<cfset translateTocountryID = qry_get_PersonDetails.countryID>

<cf_translate countryID="#translateTocountryID#" phrases="phr_VPS_APD_EmailAddress,phr_EXT_CliamStatusEmailSubject"/>
<!---
to="#request.translations.VPS_APD_EmailAddress#"
--->
	<!--- NYB 2009-09-11 - LHID2632 Bug - added bcc to relayhelp, changed from from #application.emailFrom# to #application.emailFrom#@#application.mailfromdomain# --->
	<cf_mail to="#request.translations.VPS_APD_EmailAddress#" bcc="relayhelpcc@foundation-network.com" from="#application.emailFrom#@#application.mailfromdomain#" subject="#phr_EXT_CliamStatusEmailSubject# #variables.currentOrderID#" type="HTML">
		<cf_translate countryID="#translateTocountryID#">
		<table width="75%" border="0" cellspacing="0" cellpadding="0" class=withborder id="resultstable">
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">phr_EXT_ClaimStatusEmailTitle1</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailCallCreationDate:</td>
			<td colspan="2">#variables.createddate#</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailWorkOrderNo:</td>
			<td colspan="2">#variables.TheirOrderNumber#</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailServiceRequest:</td>
			<td colspan="2">AFTER INTERVENTION</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">phr_EXT_ClaimStatusEmailSparePartsOrder</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td align="left">phr_EXT_ClaimStatusEmailReference</td>
			<td align="left">phr_EXT_ClaimStatusEmailQuantity</td>
			<td align="left">phr_EXT_ClaimStatusEmailDescription</td>
		</tr>
		<cfloop query="qry_get_orderitems">
		<CFQUERY NAME="getProdDetails" datasource="#application.sitedatasource#">			
			SELECT Product.productgroupid, productgroupname FROM Product,ProductGroup
			WHERE Product.productgroupid=ProductGroup.productgroupid and productid =  <cf_queryparam value="#productid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		<tr>
			<td align="left">#SKU#</td>
			<td align="left">#quantity#</td>
			<td align="left">#description#</td>
		</tr>
		</cfloop>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">
				phr_EXT_ClaimStatusEmailIntervention
			</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td>
				phr_EXT_ClaimStatusEmailInterventionDate:
			</td>
			<td colspan="2">
				#variables.orderdate#
			</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailExternetNo</td>
			<td colspan="2">#variables.currentOrderID#</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailTechnician</td>
			<td colspan="2">#qry_get_PersonDetails.fullname#</td>
		</tr>
		<tr>
			<td>phr_EXT_ClaimStatusEmailCompanyCode</td>
			<td colspan="2">#variables.companycode.data#</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">
				phr_EXT_ClaimStatusEmailDeliveryAddress
			</td>
		</tr>
		<tr>
			<td colspan="3">
				***********************************************************************
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<cfif variables.delsitename neq "">
		<tr>
			<td>phr_EXT_ClaimStatusEmailCompanyName</td>
			<td colspan="2">#variables.delsitename#</td>
		</tr>
		<tr>
			<td colspan="3">PHR_EXT_ClaimStatusEmailAddress</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.deladdress1#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.deladdress2#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.deladdress4#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.delpostalcode#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#qoq_get_delcountry.country#</td>
		</tr>
		<cfelse>
		<tr>
			<td>phr_EXT_ClaimStatusEmailCompanyName</td>
			<td colspan="2">#variables.invsitename#</td>
		</tr>
		<tr>
			<td colspan="3">PHR_EXT_ClaimStatusEmailAddress</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.invaddress1#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.invaddress2#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.invaddress4#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#variables.invpostalcode#</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">#qoq_get_invcountry.country#</td>
		</tr>
		</cfif>
		</table>
		</cf_translate>	
	</cf_mail>
</cfif>
<cflocation url="reportclaimsapproval.cfm"addToken="false">


