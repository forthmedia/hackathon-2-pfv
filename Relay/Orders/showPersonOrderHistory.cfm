<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		showUserOrderHistory
Author:			SWJ
Date created:	2002-07-29

	Objective - shows the order history for the current userid

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->
<CFIF IsDefined("url.eid")>
	<CFSET eID=url.eid>
</CFIF>

<CFINCLUDE TEMPLATE="/relayware/RelayQueries/qryPersonData.cfm">

<CFPARAM name="requiredstatus" default="#RealOrderStatuses#">
<CFPARAM NAME="frmLocationID" DEFAULT="#thisPersonLocID#">
<CFSET frmPersonID = #thispersonID#>

<cfinclude template="/orders/displayOrderHistory.cfm">