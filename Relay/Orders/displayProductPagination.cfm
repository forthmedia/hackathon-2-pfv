<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="calculationblockloaded" type="boolean" default="false">

<!--- claculation block --->
<cfif not calculationblockloaded>
	<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage>
		<cfset calculationblockloaded ="true">
		<cfif prodStartRow gt NumberPrizesPerPage>
			<!--- previous page calculation--->
			<cfset previousValue = prodStartRow - NumberPrizesPerPage>
		</cfif>
		<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage and (prodStartRow lt getProductsinGroupList.recordcount-NumberPrizesPerPage)>
			<!---next page calculation --->
			<cfset nextValue = prodStartRow + NumberPrizesPerPage>	
		</cfif>
		<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage>
			<!--- quick page links --->
			<cfset totalNumberofprodPages = ceiling(getProductsinGroupList.recordcount / NumberPrizesPerPage)>
			<cfparam name="numberofprodPages" default="9">
			<cfset startProdPage = ((prodStartRow-1)/NumberPrizesPerPage)+1>
			<cfif totalNumberofprodPages lt numberofprodPages>
				<cfset numberofprodPages = totalNumberofprodPages>	
			<cfelse>
				<!--- on page 11 or higher - show 5 pages either side --->		
				<cfset startProdPage = startProdPage -ceiling(numberofprodPages/2)>
			</cfif>
			<cfset endProdPage = startProdPage+numberofprodPages>
			<!--- if the maths result sin less than 10 pages give them 10 anyway --->
			<cfif endProdPage lt numberofprodPages>
				<cfset endProdPage = numberofprodPages+1>
			</cfif>
		</cfif>
	</cfif>
</cfif>
<!--- display block --->
<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage>
	<div align="center" width="100%" id="pagination">
	
			<cfif prodStartRow gt NumberPrizesPerPage>
				<!--- previous page --->
				<cfoutput>
					<a href="javascript:changeproductpage(#previousValue#)" class="prodchangepage">&laquo; <cf_translate>phr_previous</cf_translate></a> <!--- #previousValue# --->
				</cfoutput>
			</cfif>
			<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage>
				<!--- quick page links --->
				<cfloop from="#startProdPage#" to="#endProdPage#" index="prodPageNumber">
					<cfset prodStartRowvalue = ((#prodPageNumber#-1)*#NumberPrizesPerPage#)+1>
					<cfif not prodStartRowvalue gt getProductsinGroupList.recordcount and prodStartRowvalue gt 0>
						<cfoutput>
							<a href="javascript:changeproductpage(#prodStartRowvalue#)" class="prodchangepage"><cfif prodStartRowvalue eq prodStartRow><strong>#htmleditformat(prodPageNumber)#</strong><cfelse>#htmleditformat(prodPageNumber)#</cfif></a> <!--- #prodStartRowvalue# --->
						</cfoutput>
					</cfif>
				</cfloop>
			</cfif>
			<cfif getProductsinGroupList.recordcount gt NumberPrizesPerPage and (prodStartRow lt getProductsinGroupList.recordcount-NumberPrizesPerPage)>
				<!--- next page --->
				<cfoutput>
					<a href="javascript:changeproductpage(#nextValue#)" class="prodchangepage"><cf_translate>phr_next</cf_translate> &raquo;</a><!--- #nextValue# --->
				</cfoutput>
			</cfif>
		
	</div>
</cfif>
		
	

<cfoutput>
<!--- 2009/12/10 GCC - LID 2922 - url encode productgroup to work post RW8.2 --->
<script>
	function changeproductpage(pagenumber)
	{
		var now = new Date()
		now = now.getTime()
		document.mainForm.action = "<cfoutput>#jsStringFormat(cgi.SCRIPT_NAME)#?eid=#jsStringFormat(eid)#<cfif isDefined("sortSchema") and len(sortSchema)>&sortSchema=#jsStringFormat(sortSchema)#</cfif>&ShowProductGroup=#jsStringFormat(ShowProductGroup)#&prodStartRow=" + pagenumber + "&ThisProductGroup=#jsStringFormat(urlencodedformat(ThisProductGroup))#&PromoID=#jsStringFormat(promoid)#</cfoutput>&updatebasket=1&dummy=" + now						
		document.mainForm.submit()
	}
</script>
</cfoutput>
