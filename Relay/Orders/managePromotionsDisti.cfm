<!--- �Relayware. All Rights Reserved 2014 --->
<!--- manage distributors for a promotion --->

<CFPARAM name="promoid">


<!--- if  form.distilist is defined then need to do an update--->
<cfif isDefined("form.distributorList")>
	<CFQUERY NAME="updatePromo" datasource="#application.siteDataSource#">	
	update 
		promotion
	set 
		distributorList =  <cf_queryparam value="#form.distributorList#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	where
		promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		
	</CFQUERY>



</cfif>




<!--- get List of Distributors and countries they are flagged for --->

<CFQUERY NAME="Distis" datasource="#application.siteDataSource#">
	
	SELECT 
		sitename, 
		countryDescription, 
		isocode,
		locationID
	FROM 
		flag as f, 
		integerMultipleFlagData as fd , 
		location as l,
		country as c
	WHERE 
			f.flagid = fd.flagid
		and f.flagtextid = 'distiCountries'
		and l.locationid = fd.entityid
		and fd.data = c.countryid
	order by
		isoCode,
		sitename,
		locationid


</CFQUERY>


<!--- get currentValues --->
	<CFQUERY NAME="promo" datasource="#application.siteDataSource#">
	select 
		promoName,
		distributorList
	from 
		promotion
	where 
		promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>


	<CFSET distiList = "">
	<!--- make a validValueList for my valid value tag --->
	<CFOUTPUT query="Distis" group = "locationid">
		<CFSET distiList = listAppend(distiList,locationid & application.delim1	& sitename & " (",chr(10))>			
		<CFOUTPUT>
			<CFSET distiList = distiList & IsoCode & " ">
	
		</cfoutput>
			<CFSET distiList = distiList & ")">
	</cfoutput>



	<FORM action = "managePromotionsDisti.cfm" method="post">

		<CFOUTPUT>
		<CF_INPUT type="hidden" name="promoid" value="#promoid#">
		Choose the distributors for #htmleditformat(promo.promoName)# <BR>
		</cfoutput>
		<cf_displayValidValues 
			formFieldName="distributorList"
			validValues = "#distiList#"
			currentValue= "#promo.distributorList#"
			displayas = "multiselect"
			listSize = "10"
			delim2="#chr(10)#"
			delim1="#application.delim1#"
			>
		<INPUT type="submit" >

	</form>
	
