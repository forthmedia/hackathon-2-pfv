<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods
CPS 28032001  use application variables StatusDescriptions 

--->

<!---Get authorized countries--->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

<!---Get the country names for the combo box display--->
<CFQUERY NAME="getCountryCombo" datasource="#application.siteDataSource#">
	SELECT CountryDescription, CountryID
	FROM Country
	WHERE CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<!---The results page queries--->
<CFIF IsDefined("CountryCombo")>

	<CFIF #Form.Status# EQ 1>

<!---Promotions query--->	
		<CFQUERY NAME="Promo" datasource="#application.siteDataSource#">
			SELECT PromoID, Count(OrderID) AS OrderCount, 
			(select count(distinct locationid) from orders as o where orders.promoid = o.promoid ) as partnerscount
		
			FROM Orders, Location
		
			WHERE orders.LocationID = location.locationid
			
			AND OrderStatus  IN ( <cf_queryparam value="#RealOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			
			<CFIF #DateFrom# IS NOT "" AND #DateTo# IS NOT "">
			AND Orders.OrderDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND Orders.OrderDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</cfif>
			
			<CFIF #form.CountryCombo# IS NOT 0>
			and Location.CountryID =  <cf_queryparam value="#form.CountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
		
			GROUP BY PromoID

		</CFQUERY>
	
	<CFELSE>

<!---Status Query--->
		<CFQUERY NAME="Promo" datasource="#application.siteDataSource#">
			SELECT distinct OrderStatus, Count(OrderID) AS OrderCount, 
			(select count(distinct locationid) from orders as o where orders.orderstatus = o.orderstatus ) as partnerscount
			
			FROM Orders, Location
		
			WHERE orders.LocationID = location.locationid
			
			AND OrderStatus  IN ( <cf_queryparam value="#RealOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		
			<CFIF #form.CountryCombo# IS NOT 0>
			and Location.CountryID =  <cf_queryparam value="#form.CountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
		
			GROUP BY OrderStatus

		</CFQUERY>
		
		
		
	</cfif>

</cfif>

<!---Drill down by Campaign--->
<CFIF IsDefined("CampaignName")>

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
			<!--- SELECT distinct Location.SiteName, Orders.LocationID, Orders.OrderCurrency, Count(OrderID) AS OrderCount,
			(select sum(distinct ordervalue) from orders as o where orders.locationid = o.locationid) as ordervalue 
			
			
			FROM Orders, Location
		
			WHERE orders.LocationID = location.locationid
			
			AND PromoID = '#CampaignName#'
		
			GROUP BY OrderCurrency, SiteName, Orders.LocationID
			ORDER BY OrderCurrency --->
			
			
			SELECT Orders.promoid AS Campaign, Location.SiteName AS Company, Orders.OrderDate AS OrderDate, Orders.OrderID AS OrderNumber, Orders.OrderValue AS OrderValue
			FROM Orders INNER JOIN Location ON Orders.LocationID = Location.LocationID
			<CFIF #CampaignName# IS NOT ''>WHERE Orders.promoid =  <cf_queryparam value="#CampaignName#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>

	</CFQUERY>


</cfif>


<!---Drill down by Status--->
<CFIF IsDefined("OrderStatus")>

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
			<!---SELECT distinct Location.SiteName, Orders.LocationID, Orders.OrderCurrency, Count(OrderID) AS OrderCount,
			(select sum(distinct ordervalue) from orders as o where orders.locationid = o.locationid) as ordervalue 
			
			
			FROM Orders, Location
		
			WHERE orders.LocationID = location.locationid
			
			AND OrderStatus = '#orderstatus#'
		
			GROUP BY OrderCurrency, SiteName, Orders.LocationID

			ORDER BY OrderCurrency--->
			
			SELECT Orders.OrderStatus AS Status, Location.SiteName AS Company, Orders.OrderDate AS OrderDate, Orders.OrderID AS OrderNumber, Orders.OrderValue AS OrderValue
			FROM Orders INNER JOIN Location ON Orders.LocationID = Location.LocationID
			<CFIF #OrderStatus# IS NOT ''>WHERE Orders.OrderStatus =  <cf_queryparam value="#orderstatus#" CFSQLTYPE="CF_SQL_INTEGER" > </CFIF>
	
	</CFQUERY>


</cfif>








			<SCRIPT type="text/javascript">
			<!--
	

				function displayOrder (page, orderid)  {

					form = document.viewOrder
					
					form.action = page
					form.currentOrderID.value=orderid
					
					form.submit()

				}				



			//-->

			</SCRIPT>




<TABLE WIDTH="100%" CELLPADDING="2" CELLSPACING="2" BORDER="0">
<TR>
	<TD ALIGN="left"><H1>Order Statistics</H1></TD>
	<TD ALIGN="right"><!--- <A HREF="eventHome.cfm">Events Main Page</A> ---></TD>
</TR>
</TABLE>

<CFFORM NAME="eventStats" ACTION="ReportOrders.cfm" METHOD="post">

<INPUT TYPE="hidden" name="two" value="1">

<!---Display Country Combo--->
<SELECT NAME="CountryCombo">
	<OPTION VALUE="0">All
	<CFOUTPUT QUERY="getCountryCombo">
		<OPTION VALUE="#CountryID#" <CFIF IsDefined("CountryCombo")><CFIF CountryCombo EQ CountryID>SELECTED</CFIF><CFELSEIF IsDefined("LinkCountryCombo")><CFIF LinkCountryCombo EQ CountryID>SELECTED</CFIF></CFIF>>#htmleditformat(CountryDescription)#</option>
	</CFOUTPUT>
	</SELECT>

&nbsp;&nbsp;&nbsp;

<!---Display Status Combo--->
<SELECT NAME="Status">
	
	<OPTION VALUE="1" <CFIF IsDefined("Status")><CFIF Status IS 1>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 1>SELECTED</CFIF></CFIF>>By Campaign
	<OPTION VALUE="2" <CFIF IsDefined("Status")><CFIF Status IS 2>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 2>SELECTED</CFIF></CFIF>>By Status
</SELECT>
&nbsp;&nbsp;

<!---Display Date Fields--->
From: <INPUT TYPE="text" NAME="DateFrom" size="10">
	  <INPUT TYPE="HIDDEN" NAME="DateFrom_eurodate">
&nbsp;&nbsp;

To: <INPUT TYPE="text" NAME="DateTo" size="10">
	<INPUT TYPE="HIDDEN" NAME="DateTo_eurodate">
&nbsp;&nbsp;


<!---Begin Search--->
<INPUT TYPE="submit" NAME="StatSubmit" VALUE="  Go  ">
</CFFORM>

<P>

<!---Display Results--->
<CFIF IsDefined("CountryCombo")>
	Analysis of orders</B>
	<P>
	<TABLE CELLPADDING="2" CELLSPACING="2">
	
		<TR>
			<TD COLSPAN="3"><A HREF="JavaScript:emailStats();">E-mail these stats to a 3Com user</A></TD>
		</TR>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		
		<TR>
			<TD WIDTH="200" CLASS="label"><CFIF #form.Status# IS  1>Campaign<CFELSE>Status</cfif></TD>
			<TD WIDTH="100" CLASS="label">Number of orders</TD>
			<TD WIDTH="100" CLASS="label">Number of partners</TD>
			
		</TR>
	
	<CFSET TotalOrderCount = 0>
	<CFSET TotalPartnersCount = 0>
	
	<CFOUTPUT QUERY="Promo">
	
	<CFIF #form.Status# IS  2>
	<CFSET Status = #OrderStatus#>
	</cfif>
		
		<TR>
<!---Links for the Drill-Down passing parameters for stopping the Combo Boxes--->
			<!--- CPS 28032001  use application variables StatusDescriptions --->
			<TD WIDTH="200"><CFIF #form.Status# IS  1><a href="ReportOrders.cfm?CampaignName=#PromoID#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#">#htmleditformat(PromoID)#</a><CFELSE><a href="ReportOrders.cfm?OrderStatus=#Status#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#">#application.StatusDescriptions[Status]#</a></cfif></TD>
<!--- 			<TD WIDTH="200"><CFIF #form.Status# IS  1><a href="ReportOrders.cfm?CampaignName=#PromoID#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#">#PromoID#</a><CFELSE><a href="ReportOrders.cfm?OrderStatus=#Status#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#">#listgetat(orderstatuslist,listfind(RealOrderStatuses,Status))#</a></cfif></TD> --->
			<TD WIDTH="100" align="right">#htmleditformat(OrderCount)#</TD>
			<TD WIDTH="100" align="right">#htmleditformat(PartnersCount)#</TD>
		</TR>
		
		<CFSET TotalOrderCount = TotalOrderCount + OrderCount>
		<CFSET TotalPartnersCount = TotalPartnersCount + PartnersCount>
		</CFOUTPUT>
		
		<TR>
		<TD><CFIF #form.Status# IS  1><a href="ReportOrders.cfm?CampaignName=#PromoID#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#&All=1">Total:</a><CFELSE><a href="ReportOrders.cfm?OrderStatus=#Status#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Status#&All=1">Total:</a></cfif></TD>
		
		<CFOUTPUT>
		<TD align="right"><B>#htmleditformat(TotalOrderCount)#</B></TD>
		<TD align="right"><B>#htmleditformat(TotalPartnersCount)#</B></TD>
		</CFOUTPUT>
		</TR>
	
	
		<TR>
			<TD colspan="3">These figures do not include quotes and canceled orders</TD>
			
		</TR>
	
</TABLE>
</CFIF>







<!---Drill Down results display--->
<CFIF IsDefined("CampaignName") OR IsDefined("OrderStatus")>
	Analysis of orders</B>
	<P>
	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
	
		
	
		<TR>
			<TD COLSPAN="3"><A HREF="JavaScript:emailStats();">E-mail these stats to a 3Com user</A></TD>
		</TR>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<tr>
			<!--- CPS 28032001  use application variables StatusDescriptions --->		
			<cfoutput><td colspan="5" class="label"><CFIF IsDefined("CampaignName")>Promotion: #htmleditformat(Sales.Campaign)#<CFELSE>Status: #application.StatusDescriptions[Sales.Status]#</cfif></td></cfoutput>		
<!--- 			<cfoutput><td colspan="5" class="label"><CFIF IsDefined("CampaignName")>Promotion: #Sales.Campaign#<CFELSE>Status: #ListGetAt(OrderStatusList, Sales.Status)#</cfif></td></cfoutput> --->
		</tr>
		<TR>
			<TD WIDTH="200" CLASS="label">Company</TD>
			<TD WIDTH="100" CLASS="label">OrderDate</TD>
			<TD WIDTH="150" CLASS="label">Order Number</TD>
			<TD WIDTH="150" CLASS="label">Order Value</TD>
			<TD WIDTH="150" CLASS="label">View Order</TD>
			
		</TR>
	
	<CFOUTPUT QUERY="Sales" GROUP="Company">
		<TR>
			
			<TD WIDTH="200">#htmleditformat(Company)#</TD>
			
			<TD WIDTH="100" align="center">#DateFormat(OrderDate, "dd/mm/yyyy")#</TD>
			<TD WIDTH="150">#htmleditformat(OrderNumber)#</TD>
			<TD WIDTH="150">#htmleditformat(LSCurrencyFormat(OrderValue))#</TD>
			<TD WIDTH="150"><A HREF="javascript:displayOrder('<CFIF listfind(SuperUserEditableOrderStatuses,linkstatus ) IS NOT 0>../orders/editorder.cfm<CFELSE>../orders/vieworder.cfm</CFIF>',#htmleditformat(ordernumber)#)" ><IMG SRC="/images/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A> </TD>
			
		</TR>
	</cfoutput>
	
		
	
</TABLE>
</CFIF>





<!--- form used for viewing orders --->
<FORM ACTION="" NAME="viewOrder" METHOD="POST">

	<INPUT TYPE="HIDDEN" NAME="currentOrderID" VALUE="">
	<!--- <INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="frmInViewer=1"> NJH 2009-06-30 P-FNL069--->  <!--- actually not in viewer, but gets rid of look & feel --->

</FORM>


