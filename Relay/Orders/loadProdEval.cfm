<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			
Author:			DJH
Date created:	

Description:

Version history:
1

--->



<!--- create the new product groups --->

<cfquery name="newgroups" datasource="#application.siteDataSource#">
	select distinct		
		ProductGroup
	from
		tmp_ProdEvalProducts
	where
		ProductGroup not in (select Description from ProductGroup where description is not null)
</cfquery>

<cfloop query="newgroups">
	<cfquery name="newnum" datasource="#application.siteDataSource#">
		select max(ProductGroupID) +1 as new from ProductGroup	
	</cfquery>
	<cfquery name="addProductIDs" datasource="#application.siteDataSource#">
		insert into	Productgroup
		(
			ProductGroupID,
			Description
		)
		values
		(
			<cf_queryparam value="#newnum.new#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cf_queryparam value="#newgroups.productGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
		)	
	</cfquery>
</cfloop>

<Cfquery name="getProductGroups" datasource="#application.siteDataSource#">
	select 
		productgroupid,
		description
	from
		productgroup
	where
		description in (select productgroup from tmp_ProdEvalProducts)
</cfquery>

<cfloop query="getProductGroups">
	<cfquery name="updateProductGroupsinProducts" datasource="#application.siteDataSource#">
		update
			tmp_ProdEvalProducts
		set
			ProductGroupID =  <cf_queryparam value="#getProductGroups.ProductGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where
			ProductGroup =  <cf_queryparam value="#getProductGroups.Description#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			
	</cfquery>
</cfloop>

<!--- end of create the new product groups --->

<!--- create the country specific data --->

<cfquery name="getPromoID" datasource="#application.siteDataSource#">
	select
		campaignID
	from
		Promotion
	where
		promoID = 'Evaluation2000'
</cfquery>

<cfset promoID = getPromoID.campaignID>

<cfquery name="createnormalised" datasource="#application.siteDataSource#">
if exists (select * from sysobjects where id = object_id(N'[dbo].[tmp_ProdEvalPricesNormalised]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tmp_ProdEvalPricesNormalised]
</cfquery>
<cfquery name="createnormalised1" datasource="#application.siteDataSource#">
CREATE TABLE [dbo].[tmp_ProdEvalPricesNormalised] (
	[CampaignID] [int] NULL ,
	[ProductGroupID] [int] NULL ,
	[Description] [varchar] (255) NULL ,
	[SKU] [varchar] (50) NULL ,
	[ListPrice] [float] NULL ,
	[PriceISOCode] [varchar] (50) NULL ,
	[MaxQuantity] [int] NULL ,
	[CountryID] [int] NULL 
) ON [PRIMARY]
</cfquery>

<!--- create a query to list all products --->

<cfquery name="basicpricelist" datasource="#application.siteDataSource#">

select distinct
	#promoID# as promoid,
	prd.ProductGroupID,
	prd.description,
	prd.productnumber,
	pri.USD,
	pri.DEM,
	pri.ffr,
	pri.gbp,
	pri.bef,
	pri.nlg,
	pri.esp,
	pri.sek,
	pri.itl,
	pri.dkk,
	pri.skk,
	pri.huf,
	pri.pln,
	pri.ats,
	pri.chf,
	pri.bgl
from
	tmp_ProdEvalProducts as prd inner join
	tmp_ProdEvalPrices as pri on  pri.productnumber = prd.productnumber
order by
	prd.description
</cfquery>

<!--- 

loop through the columns, adding rows into the normalized table 
add a product for each country for each price.

--->

<cfloop list="#basicPriceList.columnlist#" index="acolumn">
	<cfif acolumn is not "promoid" 
	and acolumn is not "ProductGroupID" 
	and acolumn is not "description"
	and acolumn is not "productnumber">
		<!--- go through each column loading data as we go --->
		<cfquery name="addem" datasource="#application.siteDataSource#">
			insert into tmp_ProdEvalPricesNormalised
			(
				campaignid,
				productgroupid,
				description,
				sku,
				listprice,
				priceisocode,
				maxquantity,
				countryid
			)
			select distinct
				<cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				prd.ProductGroupID,
				prd.description,
				prd.productnumber,				
				<cf_queryObjectName value="pri.#acolumn#">,
				<cf_queryparam value="#acolumn#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				1,
				prc.countryid
			from
				tmp_ProdEvalProducts as prd inner join
				tmp_ProdEvalPrices as pri on  pri.productnumber = prd.productnumber
				inner join tmp_ProdEvalCountries as prc on prc.Priceisocode =  <cf_queryparam value="#acolumn#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			order by
				prd.description			
		</cfquery>	
	</cfif>
</cfloop>
<!--- end create the country specific data --->


<!--- get rid of any multi skus from countries they shouldn't exist in --->

<cfquery name="delmultiskus" datasource="#application.siteDataSource#">
	delete from product
	where
		campaignid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and
		(
			(
				sku = '3C1905B-FR'
				and countryid <> 8
			)
			or
			(
				sku = '3C1905B-UK'
				and countryid <> 9
			)
			or
			(
				sku = '3C1905B-SA'
				and countryid <> 49
			)
		)
</cfquery>

<!--- update the nic records to a maxqty of 3 

<cfquery name="nics" datasource="#application.siteDataSource#">
	update 
		tmp_ProdEvalPricesNormalised
	set
		MaxQuantity = 3
	where
		product
	
</cfquery>

--->




<!--- finally, load it into the product table --->
<cfquery name="delthiscampaign" datasource="#application.siteDataSource#">
	delete from product where campaignID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>


<cfquery name="loadProducts" datasource="#application.siteDataSource#">
insert into product
(
	campaignid,
	productgroupid,
	description,
	sku,
	listprice,
	priceisocode,
	maxquantity,
	countryid,
	deleted,
	discountprice
)
select 
	*,
	0, 
	listprice
from 
	tmp_ProdEvalPricesNormalised
</cfquery>

<!--- apply the discounts
Superstack II = 60% of list price
Office Connect = 50% of list price
PCBU - assume 50% of list price
--->

<cfquery name="OfficeConnect" datasource="#application.siteDataSource#">
select
	ProductGroupID
from
	productgroup
where
	description like 'Office%'
	or description like 'PCBU%'
</cfquery>
<cfset officeconnect = valuelist(OfficeConnect.ProductGroupID)>

<cfquery name="Superstack" datasource="#application.siteDataSource#">
select
	ProductGroupID
from
	productgroup
where
	description like 'SuperStack%'
</cfquery>
<cfset Superstack = valuelist(Superstack.ProductGroupID)>


<cfquery name="superstackdiscounts" datasource="#application.siteDataSource#">
update
	product
set
	discountprice = listprice * 0.4
where
	campaignID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and productgroupid  in ( <cf_queryparam value="#superstack#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfquery>

<cfquery name="officeconnectdiscounts" datasource="#application.siteDataSource#">
update
	product
set
	discountprice = listprice * 0.5
where
	campaignID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and productgroupid  in ( <cf_queryparam value="#officeconnect#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfquery>




