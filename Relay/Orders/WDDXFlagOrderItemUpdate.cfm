<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods
CPS 250401  Incorrect points amount being passed to RWCreateTransaction when using WDDX Packet
            Set the variable TotalOrderValue to the points amount allocated by the user. This will 
			then be available in displayordertask.cfm be passed to the stored procedure. 
 --->
 
<CFSET freezedate=now()>

<CFQUERY NAME="GetOrderFlagData" datasource="#application.siteDataSource#">
	SELECT tfd.data,
	       ori.itemid,	   
		   fla.flagtextid    
	  FROM flag fla
	      ,textflagdata tfd
		  ,orderitem ori
     WHERE ori.orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   AND tfd.entityid = ori.itemid
	   AND tfd.flagid = fla.flagid
	   AND fla.flagtextid IN ('UseOwnAgency', 'AdvertLocalAgency')
</CFQUERY>

<CFIF #GetOrderFlagData.RecordCount# IS NOT 0>
	<CFWDDX ACTION="WDDX2CFML" INPUT="#GetOrderFlagData.data#" OUTPUT="thisStructure">

	<CFIF getorderflagdata.flagtextid EQ 'UseOwnAgency'>
		<CFSET WDDXTotalOrderValue = #thisStructure["NumberOfPoints"]#>
	<CFELSE>
		<CFSET WDDXTotalOrderValue = #thisStructure["MarketingBudget"]#>		
	</cfif>

	<CFQUERY NAME="updateOrderItem" datasource="#application.sitedatasource#">			
		UPDATE OrderItem 
		   SET UnitDiscountPrice =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="CF_SQL_decimal" > ,
		       TotalDiscountPrice =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="CF_SQL_decimal" > ,
			   UnitListPrice =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="CF_SQL_decimal" > ,
			   TotalListPrice =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="CF_SQL_decimal" > ,
			   LastUpdatedby = #request.relayCurrentUser.usergroupid#,
		       Lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
 		 WHERE ItemID =  <cf_queryparam value="#GetOrderFlagData.itemid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY> 
	
	<CFQUERY NAME="updateOrder" datasource="#application.sitedatasource#">			
		UPDATE Orders
		   SET OrderValue =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="cf_sql_numeric" >,
		       OrderListValue =  <cf_queryparam value="#WDDXTotalOrderValue#" CFSQLTYPE="CF_SQL_money" > ,
			   LastUpdatedby = #request.relayCurrentUser.usergroupid#,
		       Lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
 		 WHERE OrderID =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

</CFIF>
