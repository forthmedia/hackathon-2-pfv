<!--- �Relayware. All Rights Reserved 2014 --->
<!---The Opening Page--->
<!--- Modifications --->
<!--- WAB 220200  Added search on Email address
	??	??		Somebody updated the promo query so that it works even if there are no orders for a given promotion
	SWJ	2002-03-15	Removed the reporting section and moved it to reports & catalogues
	WAb 2010-10-21 replaced request. NoNewOrdersForInternalUsers with setting
	IH  2012-12-11 Case 431220 change label from Company or Person Name to Company or Surname
--->

<!--- BUG 1742 Start SSS 2009-04-01 as default now place a new order is turned off--->
<cfset NoNewOrdersForInternalUsers = application.com.settings.getSetting("orders.NoNewOrdersForInternalUsers")>

<!--- BUG 1742 End--->
<CFIF CampaignIDAllowed IS ''>
	You don't have rights to any of the Promotional Campaigns. Please contact the Support team.
<CFELSE>

<!---Select Data for the Home Page display (we are not using 'realorderstatus' variable
  	 for we want to report on all orders (one of the criteria is 'Order Status')) --->
<CFQUERY NAME="Promo" datasource="#application.siteDataSource#">
	SELECT
		Count(Orders.OrderID) AS OrderCount,
		promotion.CampaignID,
		Orders.PromoID,
		(select count(distinct locationid) from orders as o where orders.promoid = o.promoid ) as partnerscount,
		promotion.promoID,
		(select ShowDownloadLink from promotion as p where p.promoid = promotion.promoid) as ShowDownloadLink
	FROM
		promotion LEFT OUTER JOIN Orders ON promotion.PromoID = Orders.promoid
	WHERE
		Promotion.CampaignID  IN ( <cf_queryparam value="#CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	GROUP BY
		Orders.promoid,
		promotion.CampaignID,
		promotion.promoID
</CFQUERY>

<!---Get a list of the CampaignIDs the User is entitled to use--->
<CFQUERY NAME="Promotions" datasource="#application.siteDataSource#">
	Select CampaignID, PromoID, PromoName
	From promotion
	Where CampaignID  IN ( <cf_queryparam value="#CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY PromoID
</CFQUERY>

<!---Get a list of all the SKU's for allowed promotions--->
<CFQUERY NAME="SKUCombo" datasource="#application.siteDataSource#">
	Select distinct Product.SKU from Product
	WHERE Product.CampaignID  IN ( <cf_queryparam value="#CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND Product.SKU IS NOT NULL
</CFQUERY>

<!--- NJH 2007-05-02 allow people to search by order status --->
<cfquery name="getOrderStatuses" datasource="#application.siteDataSource#">
	select distinct statusID, description from status where 1=1
	<cfif isDefined("realOrderStatuses") and realOrderStatuses neq "">
		and statusID  in ( <cf_queryparam value="#realOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
</cfquery>

<cfoutput>
<!---Make sure no text is entered in 'order number' text box--->
	<SCRIPT>
		function checkordernumber(){
			var form = window.document.form
			var form1 = window.document.form1
			var ordernumber = window.document.form.OrderNumber.value
			if (isNaN(ordernumber)) {
				alert("Order Number must be a number")
			} else {
				form.submit()
			}
		}

		<!---Make sure either the name of a company or the name of a person is entered when placing an order--->
		function checkordering(){
			var form = window.document.form1
			var companyfield = form.Company.value
			var namefield = form.Name.value
			var emailfield = form.Email.value
			if (companyfield == '' && namefield == ''  && emailfield == ''  ) {
				alert("You must enter either a Company or a Person's Name or Email")
			}
			else {
				form.submit()
			}
		}
	</SCRIPT>
</cfoutput>

<CFINCLUDE TEMPLATE="cCommerceTopHead.cfm">
<CFPARAM NAME="frmType" DEFAULT="Last">
<!---Placing Orders--->
<CFIF NOT IsDefined("Company")>
<cfoutput>
	<div class="withBorder ordersSearchForms">
		<cfif NoNewOrdersForInternalUsers EQ false>
			<FORM ACTION="OrdersHome.cfm" NAME="form1" METHOD="post">
				<div class="row">
					<div class="col-xs-12">
						<h2>Place a new Order</h2>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-3">
						<label class="label" for="companyInput">Company:</label>
					</div>
					<div class="col-xs-9">
						<INPUT id="companyInput" TYPE="text" NAME="Company" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-3">
						<label class="label" for="personalEmail">Person Email:</label>
					</div>
					<div class="col-xs-9">
						<INPUT id="personalEmail" TYPE="email" NAME="email" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-3">
						<label class="label" for="personName">Person Name:</label>
					</div>
					<div class="col-xs-9">
						<INPUT id="personName" TYPE="text" NAME="name" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<INPUT id="firstNameInput" TYPE="RADIO" NAME="frmType" VALUE="First" class="form-control" <CFIF frmType IS 'First'>Checked</CFIF>> 
						<label class="label" for="firstNameInput">First name</label>
						<INPUT id="lastNameInput" TYPE="RADIO" NAME="frmType" VALUE="Last" class="form-control" <CFIF frmType IS 'Last'>Checked</CFIF>> 
						<label class="label" for="lastNameInput">Last name</label>
						<INPUT id="fullNameInput" TYPE="RADIO" NAME="frmType" VALUE="Full" class="form-control" <CFIF frmType IS 'Full'>Checked</CFIF>> 
						<label class="label" for="fullNameInput">Full name</label>
					</div>
				</div>


				<div class="row">
					<div class="col-xs-3">
						<label class="label" for="promotionSelect">phr_order_Catalog:</label>
					</div>
					<div class="col-xs-9">
						<SELECT NAME="Promotion" id="promotionSelect" class="form-control">
							<CFLOOP QUERY="Promotions">
								<OPTION VALUE=#CampaignID#,#PromoID#>#htmleditformat(PromoName)#
							</CFLOOP>
						</SELECT>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<A HREF="javascript:checkordering()" class="ordersHomeLink btn btn-primary">Search</A>
					</div>
				</div>
			</FORM>
		</cfif>

		<cfif NoNewOrdersForInternalUsers EQ false>
			<TD valign="top">
		<cfelse>
			<TD valign="top" align="center">
		</cfif>


		<!--- ======================================================================
					Searching for Previous Orders
		====================================================================== --->
		<FORM ACTION="NewReportOrdersTask.cfm" NAME="form" METHOD="post">
				
			<div class="row">
				<div class="col-xs-12">
					<h2>Search for an existing Order</h2>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-3">
					<label class="label" for="orderNumberInput">Order Number:</label>
				</div>
				<div class="col-xs-9">
					<INPUT TYPE="text" NAME="OrderNumber" id="orderNumberInput" class="form-control">
				</div>
			</div>
					
			<div class="row">
				<div class="col-xs-3">
					<label class="label" for="companyNameInput">Company or Surname:</label>
				</div>
				<div class="col-xs-9">
					<INPUT TYPE="text" NAME="CompanyName" id="companyNameInput" class="form-control">
				</div>
			</div>

			<div class="row">
				<div class="col-xs-3">
					<label class="label" for="catalogSelect">phr_order_Catalog:</label>
				</div>
				<div class="col-xs-9">
					<SELECT NAME="Promotion" id="catalogSelect" class="form-control">
						<CFLOOP QUERY="Promotions">
							<OPTION VALUE="#PromoID#">#htmleditformat(PromoName)#
						</CFLOOP>
					</SELECT>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-3">
					<label class="label" for="skeSelect">Item SKU:</label>
				</div>
				<div class="col-xs-9">
					<SELECT NAME="SKU" id="skeSelect" class="form-control">
						<OPTION VALUE=''>All
						<CFLOOP QUERY="SKUCombo">
							<OPTION VALUE="#SKU#">#htmleditformat(SKU)#
						</CFLOOP>
					</SELECT>
				</div>
			</div>
			
			<!--- NJH 2007-05-02 allow people to search orders by status --->
			<div class="row">
				<div class="col-xs-3">
					<label class="label" for="statusSelect">Status:</label>
				</div>
				<div class="col-xs-9">
					<SELECT NAME="frmStatus" id="statusSelect" class="form-control">
						<OPTION VALUE=''>All
						<CFLOOP QUERY="getOrderStatuses">
							<OPTION VALUE="#statusID#">#htmleditformat(Description)#
						</CFLOOP>
					</SELECT>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<A HREF="javascript:checkordernumber()" class="ordersHomeLink btn btn-primary">Search</A>
				</div>
			</div>
		</FORM>
	</div>
</cfoutput>




<!--- This next section was altered by SWJ and moved to catalogues.cfm and staticReporList.cfm

<!---Orders Reporting--->
			<CFIF #Promo.RecordCount# GT 0>
			<br>
				<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">

					<TR>
						<TD colspan="5" align="center"><H2>Report - Maintain - Download</H2></TD>
					</TR>
					<TR>
						<TD colspan="5">View reports, maintain product lists and manually download order transactions</TD>
					</TR>
					<TR>
						<CFIF IsDefined("Maintenance")>
						<TH>Maintenance</TH>
						</CFIF>
						<TH>Campaigns</TH>
						<TH>Number of Orders</TH>
						<TH>Number Of Partners</TH>
						<CFIF IsDefined("Download")>
						<TH>Download</TH>
						</CFIF>

					</TR>

					<CFSET TotalOrdersCount = Evaluate(1-1)>
					<CFSET TotalPartnersCount = Evaluate(1-1)>

					<CFOUTPUT QUERY="Promo">
					<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
						<CFIF IsDefined("Maintenance")>
						<TD><A HREF="UpdateProducts.cfm?CampaignID=#CampaignID#&Currency=&Country=">
						Products</A></TD>
						</CFIF>
						<TD><A HREF="NewReportOrders.cfm?CampaignID=#CampaignID#&Criteria=2&CountryCombo=0&DateFrom=&DateTo=&DateRange=0,0,0&DeletePie=1">#PromoID#</A></TD>
						<TD align="center">#OrderCount#</TD>
						<TD align="center">#PartnersCount#</TD>
						<CFIF IsDefined("Download") and Promo.ShowDownloadLink>
<!--- 						<TD ><A HREF="Report1.cfm?frmPromoID=#PromoID#">Download #PromoID#</A></TD> --->
							<TD ><A HREF="download.cfm?PromoID=#PromoID#">Download</A></TD>
						<cfelse>
							<TD >&nbsp;</TD>
						</CFIF>
						<cfif promoid is "Evaluation2000" and isDefined("Maintenance")>

							<td><a href="ProdEvalMonthlyReport.cfm">Monthly Report</a></td>
						</cfif>

					</TR>
					<CFSET TotalOrdersCount = #TotalOrdersCount#+#OrderCount#>
					<CFSET TotalPartnersCount = #TotalPartnersCount#+#PartnersCount#>
					<CFSET AllCampaigns = ValueList(Promo.CampaignID)>
					</CFOUTPUT>

						<CFOUTPUT>
							<TR>

							<CFIF isDefined("Maintenance")>
							<TD>&nbsp;</TD>
							</CFIF>
							<TD><A HREF="NewReportOrders.cfm?CampaignID=#AllCampaigns#&Criteria=2&CountryCombo=0&DateFrom=&DateTo=&DateRange=">All Campaigns</A></TD>

							<TD align="center">#TotalOrdersCount#</TD>
							<TD align="center">#TotalPartnersCount#</TD>



							</TR>
						</CFOUTPUT>

							<CFIF NOT IsDefined("Maintenance")>

								<CFSET Message = "- In order to maintain a product set you must have higher level rights<BR>">

							</CFIF>

							<CFIF NOT IsDefined("Download")>

								<CFSET Message = "#Message#- In order to download a product set you must have higher level rights<BR>">

							</CFIF>

							<TR><TD>&nbsp;</TD></TR>
							<CFIF IsDefined("Message")>
							<TR>
								<TD colspan="4">
								<FONT COLOR="Red">
								<CFOUTPUT>#Message#</CFOUTPUT>
								</FONT>
								</TD>
							</TR>
							</cfif>
				</TABLE>

				<CFELSE>
					<H2>There are currently no records for Order Reporting.</H2>
				</CFIF>

			</DIV>


<CFELSE>
 --->
<!---The SECOND PHASE, after order placing started--->


<!---Placing or Viewing Orders Search for the Company or Name;
if a straight match proceed directly to the template, if not
display a list of possible matches--->

	<!--- 2012-07-26 PPB P-SMA001 commented out
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
	 --->

	<CFIF isdefined("name") and isDefined("email")>
	<CFQUERY NAME="GetDetails" datasource="#application.siteDataSource#">
		SELECT Location.*,
				<CFIF Name IS NOT '' or Email is Not ''>Person.*, </CFIF>
				Country.CountryDescription AS CountryName
		FROM Location,
		<CFIF Name IS NOT '' or Email is Not ''>Person,</CFIF>
		 Country
		<!--- 2012-07-26 PPB P-SMA001 commented out
		WHERE Location.CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 --->
		WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->

		<CFIF Name IS NOT '' or Email is Not ''>AND Location.LocationID = Person.LocationID</CFIF>
		AND Location.CountryID = Country.CountryID
		<CFIF Company IS NOT ''>
		AND Location.SiteName  LIKE  <cf_queryparam value="#Form.Company#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFIF>

		<CFIF Name IS NOT ''>
			<CFSWITCH EXPRESSION="#FrmType#">
				<CFCASE VALUE="First">AND Person.FirstName  LIKE  <cf_queryparam value="#Trim(Form.Name)#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFCASE>
				<CFCASE VALUE="Last">AND Person.LastName  LIKE  <cf_queryparam value="#Trim(Form.Name)#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFCASE>
				<CFCASE VALUE="Full">AND {fn CONCAT({fn CONCAT(Person.FirstName,' ')},Person.LastName)} =  <cf_queryparam value="#Form.Name#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFCASE>
			</CFSWITCH>
		</CFIF>

		<CFIF email IS NOT ''>
			AND Person.Email  LIKE  <cf_queryparam value="#Trim(Form.Email)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
	</CFQUERY>

	<SCRIPT>
		<CFINCLUDE TEMPLATE="../screen/jsViewEntity.cfm">
	</SCRIPT>

<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 1>

		<CFIF Company IS NOT ''>

			<TABLE id="ordersHomeTable3" WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">

			<cfoutput query="getDetails">

				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<td width="400">
						<B>#htmleditformat(SiteName)#</B><BR>
						#evaluate(application.addressFormat[countryid])#<BR>
						Telephone: #htmleditformat(Telephone)#<br>
						Fax: #htmleditformat(Fax)# <BR>
					</td>

					<td align="center">

						<table>
							<tr>

								<td align="center">
<!--- 									<a href="javascript:viewEntity('Location',#locationid#, #locationid#, 'orders')"><IMG SRC="/IMAGES/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a>
 --->									<!--- NJH 2009-06-30 P-FNL069 - removed frmGlobalParameters <a href="orderitems.cfm?frmLocationID=#locationid#&frmPromoid=#listlast(Promotion)#&frmGlobalParameters=frmInViewer=1">  <IMG SRC="/IMAGES/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a> --->
										<a href="orderitems.cfm?frmLocationID=#locationid#&frmPromoid=#listlast(Promotion)#" class="ordersHomeLink">  <IMG SRC="/IMAGES/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a>
									</td>

							</tr>
						</table>
					</td>
				</tr>

			</cfoutput>

			</TABLE>


<!---Person Matches--->
		<CFELSEIF Name IS NOT '' or email is not ''>

			<TABLE>

			<cfoutput query="getDetails">

				<tr <CFIF CurrentRow MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>>
					<td width="400">
					<P><B>#htmleditformat(FirstName)# #htmleditformat(LastName)#</B><BR>
					#htmleditformat(SiteName)#</P>
					</td>

					<td align="center">

						<table>
							<tr>
								<td align="center">
<!--- 									<a href="javascript:viewEntity('Location',#locationid#, #locationid#, 'orders')"><IMG SRC="/IMAGES/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a> --->

									<a href="orderitems.cfm?frmPersonID=#personid#&frmPromoid=#listlast(Promotion)#&frmGlobalParameters=frmInViewer=1" class="ordersHomeLink"><IMG SRC="/IMAGES/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>

			</cfoutput>

			</TABLE>

		</CFIF>


<!---If a straight match go directly to the order--->
	<CFELSE>



	<cfset PromoID=#ListGetAt(Promotion, 2)#>

	<cfset frmInViewer=1>

	<CFIF Name IS NOT '' or email is not ''>
		<CFSET frmPersonID=GetDetails.PersonID>
	<CFELSEIF Company IS NOT ''>
		<CFSET frmLocationID=GetDetails.LocationID>
	</cfif>

	<CFIF GetDetails.RecordCount GT 0>

		<CFSET frmGlobalParameters="frmInViewer=1">
		<CFINCLUDE TEMPLATE="OrderItems.cfm">

	<CFELSE>

		No Records. Please try changing Search Criteria.

	</CFIF>

	</cfif>
	</CFIF>

<!---  --->

</CFIF>


</CFIF>

















