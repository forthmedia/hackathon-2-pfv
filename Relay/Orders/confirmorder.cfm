<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment history --->
<!---
wab 1999-06-02 altered email because it was outputing all the phrases multiple times (they were inside <CFOUTPUT></cfoutput> tags and so appearing once for every item in the query
WAB 030200 Added code to generate properly formatted address details for emails, stored in two variables deliveryAddress and InvoiceAddress
WAB 2000-11-15  altered tmpCurrencySymbol to OrderCurrency not countrycurrency
NJH 2009/03/18 - P-SNY047 - check if mail in the promotion is an emailTextID. If so, use it.
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->


<!--- The Currency Symbol Used in this page --->
<CFSET tmpCurrencySymbol = '$'>
<cfparam name="eid" default="">
<!--- sends a confirmation email and then displays the order page again --->

<CFINCLUDE template="qryOrderDetails.cfm">

<!--- what phrase is to be used as the confirmation --->
<CFIF getOrderHeader.ConfirmedPhrase is not "">
	<CFSET confirmationPhrase = getOrderHeader.ConfirmedPhrase>
<CFELSE>
	<cfoutput><CFSET confirmationPhrase = "#phraseprefix#Wearepleasedtobeabletoconfirmthefollowing"></cfoutput>
</cfif>

<cfset promoID = getOrderHeader.promoid>



<!--- The Currency Symbol Used in this page --->
<CFSET tmpCurrencySymbol = GetOrderHeader.OrderCurrency>

<!--- 3 possible email addresses to send things to:
email address of person the order is for from the person table
email address specified in ContactEmail in the order
email address of the user placing the order
 --->


<!---  orderToEmail contains emails of person order is for and the contact email (not duplicated if these are the same)--->
<CFSET orderToEmail="#getorderheader.firstname# #getorderheader.lastname# <#getorderheader.email#>">
<CFIF getorderheader.email IS NOT getorderheader.contactemail>
	<CFSET OrderToEmail=orderToEmail & ", #getorderheader.delContact# <#getorderheader.contactemail#>">
</cfif>

<!--- 	If the order has been created by an internal user then they can be cc'ed by using createdbyEmail variable
		for external users they will get an email anyway
	  --->
<CFIF getOrderHeader.createdby is not application.webuserid>
	<CFSET createdByEmail = getOrderHeader.CreatedByEmail>
<CFELSE>
	<CFSET createdByEmail = "">
</cfif>

<!--- If there is a distributor then find out the email address  --->
<CFPARAM name="distiEmail" default="">
<CFIF getOrderHeader.DistiLocationID is not "">
	<CFQUERY NAME="getDisti" datasource="#application.siteDataSource#">
	select
		sitename,
		data as email
	from
		location l left outer join textFlagData fd on l.locationid = entityID
	where
		1=1
		and
			(
			fd.flagid = (select flagid from flag where flagtextid = 'DistiEmailAddress')
				or
			fd.flagid is null
			)

		and l.locationid =  <cf_queryparam value="#getOrderHeader.DistiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFIF trim(getDisti.email) is "">

					<CFMAIL TO=#application.com.settings.getSetting("emailaddresses.HelpAlertMailbox")#
						FROM=#application.com.settings.getSetting("emailaddresses.AdminPostmaster")#
						SUBJECT="Orders System - no Distributor Email">
						There is no email set for distributor
						#getDisti.sitename#
						</cfmail>


	</cfif>

	<CFSET distiEmail = getDisti.email>

</cfif>

	<!--- get address format --->
	<!--- and create two strings suitable for output in the confirmation mail --->
	<CFSET Countryid = #getOrderHeader.countryid#>
	<CFSET screenid = 'standardaddressformat'>
	<CFSET screenqueryname = "addressFormat">
	<CFSET frmmethod = "VIEW">
	<CFPARAM name="frmreadonlyfields" default="">   <!--- parameter can be passed eg: ='person_email','location_fax' --->
	<CFPARAM name="frmForbiddenFields" default = "">
	<CFPARAM name="frmreadonlywithhiddenfields" default="">

<!--- replaced with CFC call --->
<cfset screenAndDefinition = application.com.screens.getScreenAndDefinition(
						screenid = screenid,
						countryid = countryid,
						method = frmMethod,
						readonlyfields = frmReadOnlyFields,
						forbiddenFields = frmforbiddenFields ,
						readonlywithhiddenfields =frmreadonlywithhiddenfields
						)>
<cfset addressFormat = screenAndDefinition.Definition>

	<CFSET invoiceAddress = "">
	<CFSET DeliveryAddress = "">

	<CFLOOP query="addressFormat">
		<CFIF listfindNoCase(addressFieldsUsed,fieldTextId) IS NOT 0>
			<CFIF #fieldTextID# is NOT "Countryid">
				<CFSET thisLineDataInv = evaluate("getorderheader.Inv#fieldTextID#")>
				<CFSET thisLineDataDel = evaluate("getorderheader.Del#fieldTextID#")>
				<CFSET invoiceAddress = invoiceAddress & iif(thisLinedataInv is not "",DE("#thisLineDataInv##chr(10)#"),DE(""))>
				<CFSET deliveryAddress = deliveryAddress & iif(thisLinedataDel is not "",DE("#thisLineDatadel##chr(10)#"),DE(""))>
			<CFELSE>
				<CFSET invoiceAddress = invoiceAddress & getOrderHeader.invCountryName & ".">
				<CFSET deliveryAddress = deliveryAddress & getOrderHeader.delCountryName & ".">
			</CFIF>
		</cfif>
	</cfloop>


<!---
		Find out if a key needs to be generated if this is a file
--->
	<CFQUERY NAME="KeyNeeded" datasource="#application.siteDataSource#">
		SELECT ek.RelayKey FROM
			Orders o INNER JOIN Orderitem i ON o.orderid = i.orderid and o.orderid =  <cf_queryparam value="#CURRENTORDERID#" CFSQLTYPE="CF_SQL_INTEGER" >
			INNER JOIN EntitySKUs skus ON skus.sku = i.sku and skus.entity='file'
			INNER JOIN files f ON f.fileid = skus.entityid
			INNER JOIN RelayEntityKeys ek ON ek.entityid= f.fileid and ek.entity = 'file'
	</CFQUERY>

	<CFIF KeyNeeded.recordcount GT 0>
		<!--- Generate Key and License--->
		<CF_CreateKey entity="order" entityid=#CURRENTORDERID#>
		<CF_CreateLicense License="#KeyNeeded.RelayKey#-#RelayKey#">
		<!--- Assume That there is No Shipping as the user will collect the product on line --->
		<CFSET noShipping=true>
	</CFIF>
	<CFIF IsDefined("License")>
		<CFINCLUDE TEMPLATE="MailLicense.cfm">

	</CFIF>



<CFIF trim(GetOrderHeader.Mail) IS NOT "">
	<cfloop list="#GetOrderHeader.Mail#" index="aMail" delimiters=", ">
		<!--- NJH 2009/03/18 P-SNY047 - use email defs for sending emails. Check if the last four characters of mail is not .cfm, which we will then assume is an emailTextID --->
		<cfif right(aMail,4) neq ".cfm">
			<cfif application.com.email.doesEmailDefExist(emailTextID=amail)>
				<cfset ordersMergeStruct = application.com.structureFunctions.queryRowToStruct(query=GetOrderHeader,row=GetOrderHeader.currentRow)>
				<cfset ordersMergeStruct.orderItems=getorderitems>
				<cfset emailSent = application.com.email.sendEmail(emailTextID=amail,personID=GetOrderHeader.personID,cfmailType="html",mergeStruct=ordersMergeStruct)>
			</cfif>
		<cfelse>
		<CFINCLUDE TEMPLATE="#aMail#">
		</cfif>
	</cfloop>
</CFIF>


<!--- set the message to display at the top of the next page --->
<CFSET msg="phr_#confirmationPhrase#" >


<!--- extra messages if sending via a distributor --->
<CFIF getOrderHeader.DistiLocationID is not "">
	<!--- 2006/10/19 GCC wasn't getting translated until I did this bit of jiggery pokery. Not sure how sensible it is though... --->
	<cf_translate phrases = "phr_#phraseprefix#Order_yourDistiIs,phr_#phraseprefix#Order_yourDistiEmail"/>
		<CFSET msg= msg & "<P>&nbsp;<br><b>" & #evaluate("phr_#phraseprefix#Order_yourDistiIs")#  &"</b> " & getDisti.sitename>
		<!--- <CFSET msg= msg & "<P><b>" & #evaluate("phr_#phraseprefix#Order_yourDistiEmail")#  &"</b> " & getDisti.email>
 --->

</cfif>




<CFSET frmdisplaymode="view">
<cfset orderConfirmed = true>
<CFINCLUDE template="vieworder.cfm">

<CFIF isdefined("cookie.process")>
		<TABLE align="center">
			<TR>
			<TD>
				<FORM AcTION="/screen/redirector.cfm" METHOD="POST" NAME="mainForm" target="_self">
					<CF_INPUT TYPE="HIDDEN" NAME="frmProcessID" VALUE="#listgetat(cookie.process,1,"-")#">
					<CF_INPUT TYPE="HIDDEN" NAME="frmStepID" VALUE="#listgetat(cookie.process,2,"-")#">

			</FORM>
				<FONT FACE="Arial,Chicago" SIZE="2"><A class="profileBtn" HREF="JavaScript:document.mainForm.submit();"><IMG SRC="<CFOUTPUT></cfoutput>/images/buttons/c_continue_<CFOUTPUT>#htmleditformat(tmpButtonID)#</CFOUTPUT>.gif" ALT="Confirm" BORDER="0"></A>
			</TD>
			</TR>


		</TABLE>
</cfif>



<!--- make sure all bits of the emailscan be translated --->

<cfoutput>
	<!--- NJH 2006/98/18 added showOrderLinkButton which is set in ordersParam.cfm --->
	<cfif isdefined("goToEid") and showOrderLinkButton>
		<cf_translate>
		<form action="/et.cfm" method="post" name="frmReturntoProject">
			<CF_INPUT type="hidden" name="eid" value="#eid#">
			<input type="hidden" name="frmaction" value="locationscreen">
			<CF_INPUT type="hidden" name="projectID" value="#projectID#">
			<input type="submit" name="CMDNewProject" value="phr_vip_ReturntoProjectPage" class="OrdersFormbutton">
		</form>
		</cf_translate>
	</cfif>
</cfoutput>
<!---
now done via trigger
<!---If it is a Promo under Inventory Control update Inventory table--->
<CFIF #GetOrderHeader.InventoryID# NEQ 0>
	<CFQUERY NAME="UpdateInventory" datasource="#application.siteDataSource#">
			UPDATE i
			Set i.NumberInStock = i.NumberInStock-o.Quantity
			FROM Inventory AS i, OrderItem AS o
			WHERE i.SKU = o.SKU
			AND o.OrderID = #currentorderid#
	</CFQUERY>

	</CFIF>

 --->

