<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			UniqueTransPhrases.cfm
Author:			DJH
Date created:	2000-11-28

Description:	When unique translations are used for this promotion, still need to be able to access
				the "phr_whatever" without having to evaluate every phrase - therefore create new variables
				which are named the same as the original ones yet contain the uniquetrans translation.

Version history:
1

--->

<!---so, lets only do this if uniquetrans is set --->

<cfif UniqueTrans is not "">

	<cfset UTLen = len(UniqueTrans)>
	
	<!--- need to loop through any possible lists --->

	<!--- general phrases won't be uniquely translated 
	<cfloop list="#generalphrases#" index="aphrase">
		<cfset "phr_#left(aphrase, len(aphrase) - UTLen)#" = evaluate("phr_#aphrase#")>	
	</cfloop>
	--->

	<cfif isDefined("phrases")>
		<cfset Phrases = listChangeDelims(Phrases, ",",", 
")>
		<cfloop list="#phrases#" index="aphrase">
			<cfset "phr_#left(aphrase, len(aphrase) - UTLen)#" = evaluate("phr_#aphrase#")>	
		</cfloop>
	</cfif>	
	
	<cfif isDefined("JSPhrases")>
		<cfset JSPhrases = listChangeDelims(JSPhrases, ",",", 
")>
		<cfloop list="#JSPhrases#" index="aphrase">
			<cfset "phr_#left(aphrase, len(aphrase) - UTLen)#" = evaluate("phr_#aphrase#")>	
		</cfloop>	
	</cfif>
</cfif>

