<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			orderItems.cfm	
Author:				SWJ
Date started:		2004-07-16
	
Description:		This has been introduced so that order items can be included 
					both from here and as a Relay Tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-09-21	WAB		Added CF_translate tags


Possible enhancements:


 --->

 <cf_translate>
<cfinclude template="orderItemsInclude.cfm">
 </cf_translate>	
