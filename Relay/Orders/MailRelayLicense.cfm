<!--- �Relayware. All Rights Reserved 2014 --->


<cfparam NAME="getorderheader.contactemail" DEFAULT="help@foundation-network.com">
<cfparam NAME="getorderheader.firstName" DEFAULT="">
<cfparam NAME="getorderheader.lastName" DEFAULT="">
<cfparam NAME="M_currentorderid" DEFAULT="0">
<cfparam NAME="License" DEFAULT="NULL">

<CFSET LF=Chr(13) & Chr(10)>
<CF_MAIL   
		TO="#getorderheader.contactemail#" 
		FROM="#application.OrdersEmailFrom#"
		BCC="#application.bccemail#"
		SUBJECT="e-Source Learning Portal - License Issue"
		>
#getorderheader.firstname# #getorderheader.lastname#,
#LF#
The licence number in this email has been issued in accordance with your recent order. 
When you try to access the material relating to this order, on line for the first time, 
you will be asked to authenticate yourself using this license. Subsequently when you access 
the material you will be not be asked to enter the license. Please note that you will only be 
able to access materials purchased from the #request.CurrentSite.Title# on one machine. While we 
appreciate that this may cause some inconvenience, it is necessary in the interests of security.
#LF#
Order : #M_currentorderid##LF#
License : #License##LF#


</CF_MAIL>
