<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			
Author:			DJH
Date created:	

Description:

Version history:
1

--->

<cfquery name="updatephraselist" datasource="#application.siteDataSource#">

update
	phraselist
set
	phrasetextid = 'Order_' + phrasetextid
where
	phrasetextid in 
	('PleaseConfirmOrderDetailsCorrect',
	'Thisisaconfirmedorderandcannotbeedited',
	'MaxOrdValExceeded',
	'MaximumItemsYouCanOrder',
	'ReachedPointsAvailableTotal',
	'CreateOrderFor',
	'DetailsOfOutstandingQuotes',
	'DetailsOfPreviousOrders',
	'DiscountPrice',
	'ListPrice',
	'PleaseSelectTheProductsYouWishToPurchase',
	'PreviouslyOrdered',
	'ProductCode',
	'TotalPrice',
	'ProdEvalAvailableTo',
	'ProdEvalWebsiteDesignedTo',
	'DetailsOfPreviousOrders',
	'ViewTermsConditions',
	'FocalPointHelp',
	'SaveContinue',
	'ChangeItemsInOrder',
	'ConfirmedOrders',
	'PartiallyDispatchedOrders',
	'DispatchedOrders',
	'Choosepaymentmethod',
	'OrderNo',
	'OutstandingQuotes',
	'PreviousOrders',
	'OrderDate',
	'NoMatchingTransactionsFound',
	'OrderedBy',
	'OrderItemsSummary',
	'PleaseTakeAMomentToCheck',
	'Choosepaymentmethod',
	'DeliveryDetails',
	'DiscountPrice',
	'InvoiceDetails',
	'ListPrice',
	'OrderStatus',
	'PaymentMethod',
	'PlacedBy',
	'PleaseSelectAPaymentMethod',
	'SiteName',
	'SubTotal',
	'TotalDiscountPrice',
	'YourPurchaseRef',
	'VATNumber',
	'ChooseADistributor',
	'BillingDetails',
	'DiscountedTotalPrice',
	'DiscountedUnitPrice',
	'ItemsOrdered',
	'NumberOfItems',
	'OrderNumber',
	'PaymentMethod',
	'ProductCode',
	'ShippingDetails',
	'Thankyouforyourorder',
	'Thisorderwillbedeliveredto',
	'Tobepaidby',
	'Totaldiscountedvalue',
	'yourDistiIs',
	'yourDistiEmail',
	'DetailsOfPreviousOrders',
	'ViewTermsConditions',
	'MustEnterNumber',
	'ShoppingBasketEmpty',
	'MaxOrdValExceeded',
	'MustAcceptTermsConditions',
	'MustChooseOneProduct',
	'SelectFromAnyProductGroup',
	'MaximumItemsYouCanOrder',
	'ReachedPointsAvailableTotal',	
	'FocalPointHelp',
	'RegisteredWithDisti',
	'WhatToDoNow1',
	'WhatToDoNow2',
	'ViewShoppingBasket',
	'ProductGroup',
	'Qty',
	'MaxQty',
	'PrevOrd',
	'NoInStock',
	'RewardPoints',
	'AcknowledgeTermsConditions',
	'AcceptTermsConditions',
	'ViewTermsConditions',
	'CreateOrderFor',
	'DetailsOfOutstandingQuotes',
	'DetailsOfPreviousOrders',
	'DiscountPrice',
	'ListPrice',
	'PleaseSelectTheProductsYouWishToPurchase',
	'PreviouslyOrdered',
	'ProductCode',
	'TotalPrice',
	'PleaseEnterAPurchaseOrderNumber',
	'PleaseSelectADistributor'	
	)
</cfquery>

