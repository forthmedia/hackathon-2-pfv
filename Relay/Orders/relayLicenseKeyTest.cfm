<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>testKey</cf_title>

<cfquery name="getSKUsWithLicenses" datasource="#application.siteDataSource#">
	select SKU from entityskus order by SKU
</cfquery>
<script>
function testKey(){
	var nKey = document.myForm.keyText.value;
	var msg = "";
	//alert(nKey);
	// Set a global value for the root date
	var nDatePoint = new Date(71,5,25)
	msg = "Date stamp = " + nDatePoint;
	var i = nKey.indexOf("-",1)
	if (i == -1){
		// the format of nkey is invalid as it does not contain
		// a hyphen separating parts A and B
		//return false
		msg = msg & "\n The format of the key is invalid as it does not contain a hyphen separating parts A and B"
	}
	var j = nKey.indexOf("-",i + 1)
	if (j == -1 && j != nKey.length){
		// the format of nkey is invalid as it does not contain
		// a hyphen separating parts B and C
		msg = msg + "\n The format of the key is invalid as it does not contain a hyphen separating parts A and B"		
	}

	// get the number of days coded in the key
	var nNumber = new Number (nKey.substr(0,i) + nKey.substr(j + 1,nKey.length - j))
	var nDiv	= new Number (nKey.substr(i + 1,(j - i) - 1))
	var nDays	= nNumber / nDiv
	msg = msg + "\n days in the key are " + nDays
	
	// establish two dates to be compared, now and 
	// the root date + the number of days coded in the key
	var nNow	= new Date()
	var nDate	= nDatePoint
	nDate.setTime(nDate.getTime()+60000*60*24*nDays)
	msg = msg + "\n Current date is " + nNow;
	
	// Check the difference in years
	var nYearA = nDate.getFullYear()
	var nYearB = nNow.getFullYear()
	if (nYearA != nYearB){
		//The year part is invalid
		msg = msg + "\n Mismatch on year";
	}
	
	// Check the difference in months
	var nMonthA = nDate.getMonth()
	var nMonthB = nNow.getMonth()
	if (nMonthA != nMonthB){
		//The month part is invalid
		//alert(nDate.getMonth());
		//alert(nNow.getMonth());
		msg = msg + "\n Mismatch on month";
	}
	
	// Check the difference in days
	var nDayA = nDate.getDate()
	var nDayB = nNow.getDate()
	if (nDayA != nDayB){
		//The day part is invalid
		msg = msg + "\n Mismatch on day";
		
	}
	alert(msg);
	document.myForm.submit();
}
</script>

</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Check Relay License Key</TD>
	</TR>
</TABLE>

<p>Please enter the Relay License Key and SKU to test in the fields below.</p>
<CFSET nDate=createodbcdatetime("24-06-1971")>
<CFSET nDays=datediff("d",nDate,createodbcdate(dateconvert("local2utc",now())))>
<CFSET nFactor = int(rand() * 1000)>
<CFSET nKey1=nDays * nFactor>
<CFSET nBreak = int(len(nKey1)/2)>
<CFSET nKey = "#left(nKey1,nBreak)#" & "-#nFactor#-" & "#right(nKey1,len(nkey1)-nBreak)#">
<cfoutput>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
<tr><td>nDate</td><td>#htmleditformat(nDate)#</td></tr><br>
<tr><td>nDays</td><td>#htmleditformat(nDays)#</td></tr><br>
<tr><td>CF Server UTC Time</td><td>#dateconvert("local2utc",now())#</td></tr>

	<form action="" method="post" name="myForm">
		<tr><td>License key:</td> <td><CF_INPUT type="text" name="keyText" value="#nKey#"></td></tr>
		<tr><td>SKU for this license:</td>
			<td>
				<SELECT NAME="SKU">
					<CFLOOP QUERY="getSKUsWithLicenses">
						<OPTION VALUE="#SKU#" <cfif isdefined("form.sku") and getSKUsWithLicenses.sku eq form.SKU> selected</cfif>>#htmleditformat(SKU)#</OPTION>
					</CFLOOP>
				</SELECT>

			</td></tr>
		<tr><td colspan="2"><a href="javascript:testKey()">Click to check the Relay License key</a></td></tr>
	</form>

</cfoutput>


<CFIF isDefined("form.keyText") and structKeyExists(form,"sku") and form.sku neq "">
	<CF_SetLicense License="#form.keyText#">


<CF_CheckLicense sku="course1" producttype="file">

<CFOUTPUT>
<tr><td>Relay Validation Key</td><td>"#htmleditformat(RelayValidationKey)#"</td></tr>
<tr><td>Relay Validation Code</td><td>"#htmleditformat(RelayValidationCode)#"</td></tr>
</CFOUTPUT>
</CFIF>

<!--- <CFIF RelayValidationKey EQ 0>
	User can look at file.
	<CF_ABORT>
</CFIF> --->
</table>


