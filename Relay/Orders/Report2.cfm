<!--- �Relayware. All Rights Reserved 2014 --->
<!--- The Currency Symbol Used in this page --->
<CFSET tmpCurrencySymbol = '$'>

<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
select 	o.* 
		from 	orders as o
where 	o.OrderStatus =  <cf_queryparam value="#btnViewStatus#" CFSQLTYPE="CF_SQL_INTEGER" > 

<CFIF IsDefined("frmPromoID")>
	
		and o.PromoID =  <cf_queryparam value="#frmPromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 

</CFIF>

order by created

</CFQUERY>



<SCRIPT>

<!--
	function verifyForm() {
		var form = document.Dispatch;
			if (verifyCheckBoxClicked() == true) {
			form.submit();
			}
			else {
				window.alert("You must choose an Order to Dispatch");
			}
	}

	function verifyCheckBoxClicked() {
		var form = document.Dispatch;
			for (i=0; i<form.elements.length; i++) {
				document.write('form.chkDispatch[i]');
				if (form.chkDispatch[i].checked=="1") {
					return true;
				} 
			}	
			return false;
	}

//-->
</SCRIPT>
<CFSET tmpForm = 'None.cfm'>

<CFIF btnViewStatus IS 2>
	<CFSET tmpForm = 'Download.cfm'>
</CFIF>
<CFIF btnViewStatus IS 3>
	<CFSET tmpForm = 'Dispatch.cfm'>
</CFIF>


<br>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading"><cfoutput>#ListGetAt(orderstatuslist,btnViewStatus)#</cfoutput> Orders</DIV>
		</TD>
	</TR>
</table>

<FORM METHOD="post" ACTION="<CFOUTPUT>#htmleditformat(tmpForm)#</CFOUTPUT>">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
    <th>Order ID</th>
	<th>Order No</th>
    <th>Date</th>
	<th>Delivery Site Name</th>
   	<th>Value</th>
    <th>Pay Method</th>
    <th>Order Details</th>
	<CFIF btnViewStatus IS 3>
		<th>Edit Order</th>
		<th>Dispatch</th>
		<th>Cancel</th>
	</CFIF>
</tr>

<cfloop query="getOrderHeader">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
    <td><cfoutput>#htmleditformat(OrderID)#</cfoutput></td>
	<td><cfoutput>#htmleditformat(OrderNumber)#</cfoutput></td>
    <td nowrap><cfoutput>#htmleditformat(DAY(OrderDate))# #LEFT(MonthAsString(Month(OrderDate)),3)# #htmleditformat(YEAR(OrderDate))#</cfoutput></td>
	<td nowrap><cfoutput>#htmleditformat(delSitename)#</cfoutput></td>
    <td nowrap align="right"><cfoutput>(#htmleditformat(OrderCurrency)#) #DecimalFormat(OrderValue)#</cfoutput></td>
    <td><cfif #PaymentMethod# IS "">Not Specified<cfelse><cfoutput>#htmleditformat(PaymentMethod)#</cfoutput></cfif></td>
	<td ALIGN="CENTER"><A HREF="Report3.cfm?tmpOrderID=<cfoutput>#htmleditformat(OrderID)#</cfoutput>"><IMG SRC="../images/buttons/c_vieww_e.gif" BORDER="0"></A></td>
<!---	<CFIF btnViewStatus IS 3> --->
		<td ALIGN="CENTER"><A HREF="ReportEditItems.cfm?tmpOrderID=<cfoutput>#htmleditformat(OrderID)#</cfoutput>"><IMG SRC="../images/buttons/c_edit_e.gif" BORDER="0"></A></td>
		<TD><CF_INPUT TYPE="checkbox" NAME="chkDispatch" VALUE="#OrderID#"></td>
		<TD><CF_INPUT TYPE="checkbox" NAME="chkCancel" VALUE="#OrderID#"></td> 
<!---	</CFIF> --->
</tr>
</cfloop>
</table>
<br>
<CFIF btnViewStatus IS 2>
	<DIV ALIGN="center"><INPUT TYPE="SUBMIT" NAME="btnDownload" VALUE=" DOWNLOAD "></DIV>
</CFIF>
<CFIF btnViewStatus IS 3>
	<DIV ALIGN="center"><INPUT TYPE="SUBMIT" NAME="btnDispatchCancel" VALUE=" Dispatch / Cancel " <!--- onclick="verifyForm();" ---> ></DIV>
</CFIF>
</FORM>









