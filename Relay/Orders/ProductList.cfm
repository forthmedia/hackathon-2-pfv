<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
ProductList.cfm
Author 		SWJ
Created		17 March 2002

Shows list of products which you can constrain by passing the following variables

	productGroupID list as comma separated list
	promoID as text string of promoID name

Mods


 --->
<CFPARAM NAME="promoid" DEFAULT="configurator">
<CFQUERY name="GetProducts" datasource="#application.siteDataSource#">
	SELECT Description, SKU, productID,priceISOCode,
	ListPrice, ProductGroup
	FROM vProductList
	WHERE 1=1
	<CFIF isDefined("productGroupID")>
	 	and productGroupID  in ( <cf_queryparam value="#productGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
	<CFIF isDefined("promoID")>	
		and PromoID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFIF>
	order by productGroup, SKU
</CFQUERY>


<cf_head>
	<cf_title>Product List</cf_title>
</cf_head>

<H1>Products that match your needs</H1>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH>SKU/ProductID</TH>
		<TH>Description</TH>
		<TH>List Price</TH>
	</TR>
	<CFOUTPUT QUERY="GetProducts" GROUP="productGroup">
		<TR class="evenRow"><TD COLSPAN="4"><STRONG>#htmleditformat(ProductGroup)#</STRONG></TD></TR>
			<CFOUTPUT>
				<TR>
					<TD><A HREF="/orders/productDetail.cfm?productID=#productID#">#htmleditformat(SKU)#</A></TD>
					<TD>#htmleditformat(Description)#</TD>
					<TD>#htmleditformat(NumberFormat(ListPrice))# #htmleditformat(PriceISOCode)#</TD>
				</TR>
			</CFOUTPUT>
	</CFOUTPUT>
</TABLE>





