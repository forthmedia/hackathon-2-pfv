<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			qryDistiNotFulfilled.cfm
Author:			DJH
Date created:	14 June 2000

Description:	Query to get a list of Orders/Order Items where an order has not been fulfilled

Version history:
1

--->


<cfquery name="qryDistiNotFulfilled" datasource="#application.siteDataSource#">
select 	
	o.* ,
	promotion.*,
	p.firstname,
	p.lastname,
	p.email,
	l.sitename,
	c.countrydescription,
	c.countrycurrency,
	c.countryid,
	c.AllowVATReduction, 
	c.VATFormat,
	(select name from usergroup where usergroupid=o.createdby) as CreatedByName,
	(select email from usergroup, person where person.personid = usergroup.personid and usergroupid=o.createdby) as CreatedByEmail,
	(select countryDescription from Country where countryID = delCountryID) as delCountryName,
	(select countryDescription from Country where countryID = invCountryID) as invCountryName
from
 	orders as o 
	inner join location as l
	on o.locationid=l.locationid
	inner join person as p 
	on o.personid=p.personid
	inner join country as c
	on l.countryid=c.countryid 
	inner join promotion 
	on o.promoid=promotion.promoid
where
	promotion.promoID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	and o.OrderStatus in (2, 10)
	and (select count(*) from vOrderItems where vOrderItems.orderID = o.OrderID) > 0
	and (o.orderdate + #DistiOrderFulfillmentWarning# + 1) <  <cf_queryparam value="#createodbcdate(now())#" CFSQLTYPE="cf_sql_timestamp" >
order by
	OrderDate,
	OrderStatus
</cfquery>
