<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		addOrder4SKU
Author:			SWJ
Date created:	2002-07-29

	Objective - shows the order history for the current userid

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

<!--- AddOrder4SKUWrapper for backwards compatability --->
<CF_DisplayBorder borderset="#application.standardBorderset#">
<cfinclude template="addOrder4SKUInclude.cfm">
</CF_DisplayBorder>
