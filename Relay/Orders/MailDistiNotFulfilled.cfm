<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			MailDistiNotFulfilled.cfm
Author:			DJH
Date created:	2000-08-15

Description:	Mails an email to the disti when an order has not been fulfilled

Version history:
1
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->

<!--- get the translations --->
<cfinclude template="OrdersGeneralPhrases.cfm">

<CFSET Phrases = 
"Order_AcceptTermsConditions,
Order_ViewTermsConditions,
Order_AcknowledgeTermsConditions,
Order_Reseller,
Order_DistiDiscountLevel,
Order_ResellerDiscountLevel,
Order_OrderItemsSummary,
Order_PleaseTakeAMomentToCheck,
Order_Choosepaymentmethod,
Order_DeliveryDetails,
Order_DiscountPrice,
Order_InvoiceDetails,
Order_ListPrice,
Order_OrderStatus,
Order_PaymentMethod,
Order_PlacedBy,
Order_PleaseSelectAPaymentMethod,
Order_SiteName,
Order_SubTotal,
Order_TotalDiscountPrice,
Order_YourPurchaseRef,
Order_ResellerPurchaseRef,
Order_VATNumber,
Order_ChooseADistributor">

<cfset TextPhraseList = listQualify("#GeneralPhrases#,#Phrases#","'",", 
")>

<cf_translate phrases = "#TextPhraseList#">

<!---Mail Template which sends mail to disti--->
<CFSET ordertotal = getOrderValue.TotalValue + getOrderHeader.TaxOnOrder + getOrderHeader.ShippingCharge >
<cfset distiordertotal = getOrderValue.TotalDistiValue>

<CFSET tab1 = 15>
<CFSET tab2 = 45>

<CF_MAIL query="qryDistiNotFulfilled"  
		TO="#distiEmail#" 
		FROM="#supportEmail#" 
		CC="#copyAllEmailsToThisAddress#"
		SUBJECT="Order Confirmation: Order No. #currentorderid#">#phr_order_AlertOrdertoDisti#

#phr_order_OrderNumber# #repeatString(" ",max(0,tab1-Len(phr_order_OrderNumber)))# #currentorderid#
#phr_order_NumberOfItems# #repeatString(" ",max(0,tab1-Len(phr_order_NumberOfItems)))# #getorderitems.recordcount#

#phr_order_ItemsOrdered#
================================================================================<CFOUTPUT>
#phr_order_ProductCode# #repeatString(" ",max(0,tab2-Len(phr_order_ProductCode)))# #SKU# 
#phr_Description# #repeatString(" ",max(0,tab2-Len(phr_description)))# #Description# 
#phr_Quantity# #repeatString(" ",max(0,tab2-Len(phr_Quantity)))# #Quantity#
#phr_order_DiscountedUnitPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedUnitPrice)))# #tmpCurrencySymbol# #numberFormat(UnitDiscountPrice,"___,___,___.__")#
#phr_order_DiscountedTotalPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedTotalPrice)))# #tmpCurrencySymbol# #numberFormat(TotalDiscountPrice,"___,___,___.__")#
#phr_order_DistiDiscountedUnitPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DistiDiscountedUnitPrice)))# #tmpCurrencySymbol# #numberFormat(unitListPrice * (1 - DistiDiscount),"___,___,___.__")#
#phr_order_DistiDiscountedTotalPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DistiDiscountedTotalPrice)))# #tmpCurrencySymbol# #numberFormat(Quantity * unitListPrice * (1 - DistiDiscount),"___,___,___.__")#
</CFOUTPUT>
#phr_order_Totaldiscountedvalue# #repeatString(" ",max(0,tab2-Len(phr_order_TotalDiscountedValue)))# #tmpCurrencySymbol# #numberFormat(getOrderValue.TotalValue,"___,___,___.__")#<cfif getOrderHeader.ShowDeliveryCalculation>
#phr_Delivery# #repeatString(" ",max(0,tab2-Len(phr_Delivery)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.shippingCharge,"___,___,___.__")#</cfif><cfif getOrderHeader.ShowVATCalculation>
#phr_VAT# #repeatString(" ",max(0,tab2-Len(phr_VAT)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.taxOnOrder,"___,___,___.__")#</cfif><cfif getOrderHeader.ShowDeliveryCalculation or getOrderHeader.ShowVATCalculation>
#phr_Total# #repeatString(" ",max(0,tab2-Len(phr_Total)))# #tmpCurrencySymbol# #numberFormat(orderTotal,"___,___,___.__")#</cfif>
#phr_order_TotalDistiDiscountedPrice# #repeatString(" ",max(0,tab2-Len(phr_order_TotalDistiDiscountedPrice)))# #tmpCurrencySymbol# #numberFormat(DistiOrderTotal,"___,___,___.__")#
================================================================================<cfif not (getOrderHeader.ShowDeliveryCalculation and getOrderHeader.ShowVATCalculation)>
#phr_Order_DistiAddShippingAndTax#
</cfif>
#phr_order_ShippingDetails#
================================================================================
#phr_order_Thisorderwillbedeliveredto#:
#getorderheader.delcontact#
#getorderheader.delsitename#
#DeliveryAddress#
================================================================================

#phr_order_BillingDetails#
================================================================================
#getorderheader.invcontact#
#getorderheader.invsitename#
#InvoiceAddress#
================================================================================
</CF_MAIL>


