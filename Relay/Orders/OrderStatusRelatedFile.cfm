<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	OrderStatusRelated File		
Author:	CPS		
Date created: 2001-02-01	

	Objective - To allow Local Service Suppliers to upload files for orders that
	            are currently 'Awaiting Quote From In Focus Agency' - statusid 21.
				The template also allows the LSS to amend the quote from the original
				estimate. 
	
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:

--->


<cf_head>
<cf_title>
</cf_title>
</cf_head>



<CFPARAM name="msg" default="">

<!--- check that order status is still "quote", otherwise can't be edited, unless called via softbanx --->
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
	select 
		o.*,
		p.PromoID,
		p.BorderSet
	from 
		orders as o,
		promotion as p
	where 
		orderid =  <cf_queryparam value="#frmOrderId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and p.promoid = o.promoid
</CFQUERY>

<cfparam name="promoid" default="#getOrderHeader.PromoID#">

<CFIF IsDefined("frmOrderId")>
	<CFSET CurrentOrderId = #frmOrderId#>
</CFIF>


<cf_DisplayBorder BorderSet="#getOrderHeader.BorderSet#"> 

<CFSET frmdisplaymode="view">

<CFINCLUDE template="DisplayOrderRelatedFile.cfm">

<B>phr_Order_CampaignProposalUpload</b><BR><BR>

<cf_relatedFile action="list,put" entity="#OrderItem.OrderItemID#" entitytype="OrderItem">
	
<TABLE align="center">
	<TR> 
		<TD align="center"><FONT FACE="Arial,Chicago" SIZE="2">&nbsp;&nbsp;&nbsp;
			<A HREF="Javascript:submitForm();">phr_Order_SaveContinue</A>	 
		</TD>			
	</tr>
</table>
	
</cf_DisplayBorder> 




