<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		orderEnterVoucherCode.cfm
Author:			JC
Date started:		2004-12-07
	
Description:		This file handles requests from displayorder.cfm to allow user to enter a Voucher
Code to be used against the current order.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancements still to do:
Transction handling to be applied to application.com.orderVoucher.updateOrderItem for when looping over multiple product IDs

Note: Enterng a vouchercode will cause displayorder.cfm to drop out of the element tree (et.cfm)
 --->
<cf_translate>

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cf_head>
<cf_title>phr_Order_OrderEntryVoucher</cf_title>
<cfoutput>
	<!--- NJH 2009/01/05 - Bug Fix All Sites Issue 1561 - use the stylesheet loaded in relay\application .cfm
	<cfif #IsDefined("application. userfilespath")# AND #application. userfilespath# NEQ "">
		<CFHTMLHEAD TEXT="<LINK REL='stylesheet' HREF='#request.currentSite.httpProtocol##SiteDomain#/code/styles/DefaultPartnerStyles.css'>">
	<cfelse>
		<cfinclude template="/templates/loadStylesheets .cfm">
	</cfif> --->

<script>
<!--
 function checkForm() {
	var msg = ""	
	if (document.Voucher.vouchercode.value == '') {
			msg += "phr_Order_JS_MustEnterVoucherCode";
	}

	if (msg != "") {
			alert(msg);
		} else {
			document.Voucher.submit();
		}
}
<cfif #IsDefined("vouchercode")#>
function RunTimer() {
  opener.voucherUpdate();
  document.write("<p class='redbody'>phr_Order_JS_ApplyVoucherMessage</p>");
  document.write("<script language='javascript'>setTimeout('CloseWindow()',2500);");
  document.write("function CloseWindow() {self.close();}");
  document.write("</script>");
}
</cfif>
//  End -->
</script>
</cfoutput>
</cf_head>


<cfset bodyAttributes={}>
<cfif NOT IsDefined("vouchercode")>
	<cfset bodyAttributes.onload = document.Voucher.vouchercode.focus()>
</cfif>
<cf_body attributeCollection = #bodyAttributes#>
<div align="right">[ <a href="javascript:self.close();">phr_Order_JS_cancelAddVoucher</a> ]</div>
<span class="heading1">phr_Order_OrderEntryVoucher</span>
<cfif structkeyExists(Form,"Update")>
	<cfoutput>
	<cfset vouchercode = "#UCase(vouchercode)#">
	<cfswitch expression=#frmAction#>	
		<cfcase value="Voucher">
			<cfscript>
				validationresult = application.com.orderVoucher.validateVoucherCode(orderid,vouchercode);
			</cfscript> 
			<!--- <cfdump var="#validationresult#">
			<CF_ABORT>  --->

			<!--- Single ProductID = Numeric, List of Product IDs = List, Error = text string --->
			<cfif #IsNumeric(validationresult)# OR #ListLen(validationresult,',')# GT 1>
				<cfloop index=i list="#validationresult#">
					<cfset productid = #i#>
					<cfscript>
						updateresult = application.com.orderVoucher.updateOrderItem(orderid,vouchercode,productid);
					</cfscript>
				</cfloop>
			</cfif>
			
			<!--- If ValidationResult not a numeric productid or list of prodids
			OR if updateresult returns an error....--->
			<cfif (NOT #IsNumeric(validationresult)# AND #ListLen(validationresult,',')# EQ 1) OR (#IsDefined("updateresult")# AND #updateresult# NEQ "ok")>
				<p class="redbody">
				<b>
				phr_Order_ErrorAddingVoucher: <cfif #IsDefined("updateresult")# AND #updateresult# NEQ "">#htmleditformat(updateresult)#<cfelse>
				#htmleditformat(validationresult)#
				</cfif>
				
				</b>
				</p>
				<cfset #StructDelete(Form,'update')#>
			<cfelse>
				<!--- If no error has been returned, close window and reload calling page --->
				<SCRIPT type="text/javascript">
				<!--
				RunTimer(); 
				//  End -->
				</script>	
			</cfif>
		</cfcase>
	</cfswitch>
	</cfoutput>
</cfif>
<cfif NOT structkeyExists(Form,"Update")>
	<cfif structkeyExists(Form,"frmAction")>
		<table border="0" cellspacing="0" cellpadding="3" align="left" bgcolor="White" class="withBorder" width="100%" height="100%">
			<tr>
				<td align="center" valign="top">
					<form name="Voucher" method="post" target="_self" onSubmit="checkForm(vouchercode,'Voucher Code');">
						<cfswitch expression=#frmAction#>
							<cfcase value="voucher">
								phr_Order_EnterVoucherCodeMsg<br>
								phr_Order_EnterVoucherCode:
								<input type="text" name="vouchercode" value="">
							</cfcase>
						</cfswitch>
						<cfoutput>
							<CF_INPUT type="hidden" name="callerQueryString" value="#callerQueryString#">
							<input type="hidden" name="Update" value="True">
							<CF_INPUT type="hidden" name="orderid" value="#orderid#">
						</cfoutput>
						<input type="hidden" name="frmAction" value="Voucher">
						<input type="button" name="btnUpdate" value="phr_Order_Update" onClick="checkForm(vouchercode,'Voucher Code');">
					</form>
				</td>
			</tr>
		</table>
	</cfif>

</cfif>

</cf_translate>
