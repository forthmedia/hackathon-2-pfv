<!--- �Relayware. All Rights Reserved 2014 --->
<!--- orderItemsTask.cfm --->
<!--- processes form from orderitem.cfm  --->
<!--- creates or updates an order, updating all the items and recalculating the subtotals, VAT etc. --->

<!--- Mods
WAB 030200  Removed orderHistory queries, now done as a tigger
WAB 030200  Altered how address are copied from the location tables
CPS 230301	Amendments to ensure orders for the 'InFocusIncentiveScheme' are created correctly
WAB 260401	Altered to use application.countrygroupsbelongedto array rather than #currentregion#
CPS 080501	Problems with rounding errors when dealing with 'Points' campaigns 
ANDY 15-5-2001 Problems being generated in the SQL statements with commas in the figures. Ref - Ticket 5262
in the insert statement line 409 - 412 added replace('#unitListPrice#',',','') to remove the commas from the figures before SQL processes the query.

WAB2005-06-07	problem with points over 1000 because the numberformat function outputs everything with commas in it which annoys sql
			have added mask "0" to the number format which gets rid of all the commas, but infact can't see any reason for having numberformat there at all 
			only occurred on the updates ( the change above had dealt with the inserts, probably ought to both use the same method)
			
NJH	2009/07/09	P-FNL069 - form scoped most variables. Replaced cookie with request.relayCurrentUser. 
	
WAB     2009/06/24     RACE
SSS     2010/04/14     LH3173 buy now button fix	

2011/05/06 PPB LID5378: switched request.setPendingStatus to use setting incentive.pointsAboveWhichOrderConfirmationRequired
2015-11-16  DAN PROD2015-364 - products country restrictions

 --->
<!------------------------------------------------------------------->
<!--- ALEX --->
<cfparam name="projectID" default="0">
<cfparam name="currentcountry" default="0">
<cfparam name="UsePersonAsDeliveryDetails" default="1">
<!--- 
WAB 2009/06/24 removed as part of Race Security
<cfif not (isdefined('parentApplicationInclude') and parentApplicationInclude eq "No")>
	<cfinclude template="application .cfm">
</cfif>
--->
<!------------------------------------------------------------------->

<!------------------------------------------------------------------->
<!--- allow viewer to go to somewhere else entirely --->
<CFIF frmNextPage is not "">
	<CFINCLUDE template = "#frmNextPage#">
	<CF_ABORT>
</cfif>

<cfset pointsExceedsValue = false>
<CFSET freezedate=now()>
<!--- NJH 2009/07/02 P-FNL069 replaced this with below
<CFSET user=request.relayCurrentUser.usergroupid> --->
<CFSET user=request.relayCurrentUser.userGroupID>


<cfquery name="GetPromo" datasource="#application.siteDataSource#">
	select * from Promotion where promoid =  <cf_queryparam value="#form.frmpromoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfset UsePoints = GetPromo.UsePoints>

<cfset PointsFactor = 1>
<cfif UsePoints>
	<cfquery name="PointsFactor" datasource="#application.siteDataSource#">
		select CurrencyPerPoint
		from RWPointsConversion
		where PriceISOCode =  <cf_queryparam value="#form.frmOrderCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > 			
	</cfquery>
	
	<cfif PointsFactor.recordcount is 1>
		<cfset PointsFactor = PointsFactor.CurrencyPerPoint>
	<cfelse>
		<cfset PointsFactor = 1>
	</cfif>
</cfif>

<!--- order statuses?  1=quote, 2=confirmed --->

<CFIF frmTask IS "New">
	<!--- Get persons address details --->
	<!--- NJH 2009/07/02 P-FNL069 use frmPersonID from form scope --->
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
	Select l.*, p.*, c.countrydescription as country, c.countryid
	from person as p, 
		location as l, 
		country as c
	where
		p.personid =  <cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and	p.locationid=l.locationid
		and	l.countryid=c.countryid
	</CFQUERY>
	
	<!--- 
	<CFSET Countryid = #getPersonDetails.countryid#>
	<CFSET screenid = 'standardaddressformat'>
	<CFSET frmmethod = "edit">
	<CFINCLUDE template="..\screen\qryscreendefinition.cfm"> --->
	
	<!--- setting the current orderID here to the order ID I have created will still allow the code below to work. --->
	<!--- NJH 2009/07/02 P-FNL069 - get maxOrderID from form scope as it could be passed in via url to manipulate someone else's order. --->
	<cfset currentorderid = form.maxorderID>
	
	<!---  actually,if person has already placed an order, should pull up the last details--->
<!--- 	<CFQUERY NAME="chkOrderID" datasource="#application.sitedatasource#">
		SELECT OrderID from orders
	</CFQUERY> --->


	<!---  before creating order, check that order has not already been created from this page (eg user has hit the reload button)--->
	<CFQUERY NAME="checkforOrder" datasource="#application.sitedatasource#"> 	
	select 
		* 
	from 
		orders
	where
	 	CreatedBy =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and orderdate =  <cf_queryparam value="#form.pageloaded#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</CFQUERY>

	<CFIF checkfororder.recordcount IS NOT 0>
		<!--- this order has actually already been created, reload this page with  frmtask=update and currentorderid= --->
		<!--- strictly speaking we ought to thow them out here but by setting orderlastupdated it allows the update to go ahead --->
		<CFSET frmtask="update">
		<CFSET currentorderid=checkfororder.orderid>		
		<CFSET orderlastupdated=checkfororder.lastupdated>			
		<CFINCLUDE template="orderitemstask.cfm">
		<CF_ABORT>
	</cfif>
	
	
<!--- orderID has already been created the details to finish it are entered here--->
	
	<CFQUERY NAME="updateOrder" datasource="#application.sitedatasource#">
	Update Orders 
	SET OrderNumber = '',
		Orderdate =  <cf_queryparam value="#form.pageloaded#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
		LocationID =  <cf_queryparam value="#getpersondetails.locationid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		PersonID =  <cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		OrderStatus =  <cf_queryparam value="#constQuote#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		OrderCurrency =  <cf_queryparam value="#form.frmOrderCurrency#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		CreatedBy =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		Created =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
		LastUpdatedBy =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		LastUpdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
		<cfif UsePersonAsDeliveryDetails>
			delcontact =  <cf_queryparam value="#getpersondetails.firstname# #getpersondetails.lastname#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			delsitename =  <cf_queryparam value="#getpersondetails.sitename#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<CFLOOP index="i" from="1" to="5">
				<CFSET tmpAddress = evaluate("getpersondetails.address#i#")>
				deladdress#i# =  <cf_queryparam value="#tmpAddress#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			</CFLOOP>
			delpostalcode =  <cf_queryparam value="#getpersondetails.postalcode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			delcountryid =  <cf_queryparam value="#getpersondetails.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes START --->
			contactphone = <CFIF getpersondetails.officephone is not "">'#getpersondetails.officephone#',<CFELSE>'#getpersondetails.telephone#',</cfif>
			contactfax = <CFIF getpersondetails.faxphone is not "">'#getpersondetails.faxphone#',<CFELSE>'#getpersondetails.fax#',</cfif>
			contactemail =  <cf_queryparam value="#getpersondetails.email#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes END --->	
		</cfif>
		invcontact =  <cf_queryparam value="#getpersondetails.firstname# #getpersondetails.lastname#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		invsitename =  <cf_queryparam value="#getpersondetails.sitename#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		<CFLOOP index="i" from="1" to="5">
			<CFSET tmpAddress = evaluate("getpersondetails.address#i#")>
			invaddress#i# =  <cf_queryparam value="#tmpAddress#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		</CFLOOP>
		invpostalcode =  <cf_queryparam value="#getpersondetails.postalcode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		invcountryid =  <cf_queryparam value="#getpersondetails.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		promoid =  <cf_queryparam value="#form.frmPromoId#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		ShippingMethod = '',
		HandlingFee = 0,
		ProjectID =  <cf_queryparam value="#projectID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		where orderID =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<!--- NJH 2009/07/10 P-FNL069 - only update orders that portal user has created. If internal, can update any order. --->
		<cfif not request.relayCurrentUser.isInternal>
			and personID = #request.relayCurrentUser.personID#
		</cfif>
</CFQUERY>
	
<!---
	<CFQUERY NAME="CreateOrder" datasource="#application.sitedatasource#"> 
		INSERT Into Orders
		(
			OrderID,
			OrderNumber, 
			Orderdate, 
			LocationID, 
			PersonID, 
			OrderStatus, 
			OrderCurrency, 
			CreatedBy, 
			Created, 
			LastUpdatedBy, 
			LastUpdated,
			<cfif UsePersonAsDeliveryDetails>
				delcontact,
				delsitename,
				<CFLOOP index="i" from="1" to="5">
					deladdress#i#, 		
				</CFLOOP>
				delpostalcode,
				delcountryid,
				<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes START --->
			 	contactphone,
				contactfax,
				contactemail,
				<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes END --->
			</cfif>
			invcontact,
			invsitename,
			<CFLOOP index="i" from="1" to="5">
				invaddress#i#,	
			</CFLOOP>
			invpostalcode,
			invcountryid, 
			promoid,
			ShippingMethod,
			HandlingFee,
			ProjectID
		)

		Select 	
			<CFIF #chkOrderID.RecordCount# IS 0>
				1,
			<CFELSE>
				Max(o.orderid)+1,
			</CFIF>
			'',
			#pageloaded#,
			#getpersondetails.locationid#,
			#frmpersonid#,
			#constQuote#,
			'#frmOrderCurrency#',
			#user#,
			#freezedate#,
			#user#,
			#freezedate#,
			<cfif UsePersonAsDeliveryDetails>
				'#getpersondetails.firstname# #getpersondetails.lastname#',
				'#getpersondetails.sitename#',
				<CFLOOP index="i" from="1" to="5">
					<CFSET tmpAddress = evaluate("getpersondetails.address#i#")>
					'#tmpAddress#',
				</CFLOOP>
				'#getpersondetails.postalcode#',
				#getpersondetails.countryid#,
			<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes START --->
			<CFIF getpersondetails.officephone is not "">
				'#getpersondetails.officephone#',
			<CFELSE>
				'#getpersondetails.telephone#',
			</cfif>
			<CFIF getpersondetails.faxphone is not "">
				'#getpersondetails.faxphone#',
			<CFELSE>
				'#getpersondetails.fax#',
			</cfif>
			'#getpersondetails.email#',
			<!--- 2005/07/12 - GCC - VIP Phase 1 final sign off changes END --->
			</cfif>
			'#getpersondetails.firstname# #getpersondetails.lastname#',
			'#getpersondetails.sitename#',
			<CFLOOP index="i" from="1" to="5">
				<CFSET tmpAddress = evaluate("getpersondetails.address#i#")>
				'#tmpAddress#',
			</CFLOOP>
			'#getpersondetails.postalcode#',
			#getpersondetails.countryid#,
			'#frmPromoId#',
			'',
			0,'#projectID#'
		<CFIF chkOrderID.RecordCount IS 0>
		from 
			dual
		<CFELSE>	   
		from 
			orders as o					
		</CFIF>	
	</CFQUERY>
--->

	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		SELECT 
			OrderID, 
			*
		from 
			Orders o inner join  promotion p on  p.promoid = o.promoid
			left join 
		location as l
			on  o.locationid=l.locationid
			left join 
		country as c
			on l.countryid=c.countryid
		Where 
			<!--- NJH 2009/07/02 P-FNL069 replaced with below  o.Createdby=#request.relayCurrentUser.usergroupid# --->
			o.Createdby=#request.relayCurrentUser.userGroupID#
			and abs(dateDiff(s,o.created,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)) <= 1 
	</CFQUERY>

	<CFIF getOrderHeader.recordcount IS 0>
		Sorry, Order not added, please try again
		<CF_ABORT>
	</cfif>

	<CFSET currentorderid=getorderHeader.orderid>



	

<CFELSEIF frmTask IS "Update">

	<!--- get details of existing order --->
	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		select 
			* 
		from 
			Orders, 
			promotion
		where 
			orders.orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and promotion.promoid = orders.promoid
	</CFQUERY>

	<CFIF datediff("S",orderlastupdated,getorderheader.lastupdated) GT 1>

		<CFSET msg="The order has been edited by someone else since you loaded the page<BR>I am afraid that your changes have been discarded<BR>">
		<!--- this bit ought to be going back to the page you came from --->
		<CFINCLUDE template="editorder.cfm">
		<CF_ABORT>
	
	</CFIF>
</cfif>

<cfset ShowDeliveryCalculation = GetOrderHeader.ShowDeliveryCalculation>
<cfset ShowVATCalculation = GetOrderHeader.ShowVATCalculation>

<CFIF frmtask IS "New" or frmtask is "Update">
	<!---
	whether update or new, get all existing order items,
	loop through items on form adding/deleting/updating where necessary
	of course for a new order, only updates will ever happen
	--->

	<!--- Get persondetails including the Country ID needed for OrderItems and the product table --->
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
	Select 	
		l.*,
		p.*,
		c.countrydescription as country,
		c.countryid
	from
	 	person as p, 
		location as l, 
		country as c
	where
		p.personid =  <cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and	p.locationid=l.locationid
		and	l.countryid=c.countryid
	</CFQUERY>
	<cfif OrderSingleProduct>
            <!--- SSS LHID 3173 buy now button fix --->
		<CFQUERY NAME="orderExists" datasource="#application.sitedatasource#">
			update orderitem
			set active = 0
			where orderID =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfif>
	
 	<!--- get existing order items, will return nothing for new order --->
	<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
		Select 
			*, cast(productID as varchar(20))+'_'+cast(parentSKUGroup as varchar(20)) as Prod_parentskugroup
		FROM 
			vOrderItems
		WHERE
			orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
			AND active = 1
	</CFQUERY>

	<CFSET currentorderitems = valuelist(getorderitems.productid)>
	<CFSET currentorderitemsProd_parentskugroup=valuelist(getorderitems.Prod_parentskugroup)>

	<CFTRANSACTION>
		<!--- loop through all SKUs in form --->
		<cfloop index="i" from="1" to="#listlen(SKUsonPage)#">
			<!--- check that the quantity set is numeric, and set a variable --->
			<cfset skuid = listgetat(SKUsonPage,i)>
			<cfset thisSKUQuantity = evaluate("quant_" & listgetat(SKUsonPage,i) & "_" & listgetat(SKUsParentsonPage,i))>


			<cfset thisSKUParentSKUGroup = listgetat(SKUsParentsonPage,i)>
			<CFIF not isNumeric(thisSKUQuantity)>
				<CFSET thisSKUQuantity = 0>
			</cfif>
	
			<CFIF thisSKUQuantity IS NOT 0>
				<!--- ie there is a quantity of this item set --->
		
				<!--- get actual SKU --->
				<CFQUERY NAME="getItemDetails" datasource="#application.sitedatasource#">			
					SELECT 
						p.*
					FROM 
						vProductDetails p
                        left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = p.productid and vcs.countryid = <cf_queryparam value="#currentcountry#" cfsqltype="CF_SQL_INTEGER">
					WHERE 
						productid =  <cf_queryparam value="#skuid#" CFSQLTYPE="CF_SQL_INTEGER" > 
						<!--- CPS 2001-03-23 If the promotion makes use of another promotion then ensure the products are for that promotion --->
						<CFIF getPromo.UsePromoId EQ ''>
							AND promoID =  <cf_queryparam value="#form.frmPromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						<CFELSE>						
							AND promoID =  <cf_queryparam value="#getPromo.UsePromoId#" CFSQLTYPE="CF_SQL_VARCHAR" > 			
						</CFIF>	
                        and (p.hascountryScope = 0 or vcs.countryid is not null)
						<!--- AND countryID In (0,#currentcountry#<cfif application.countryGroupsBelongedTo[currentcountry] is not "">,#application.countryGroupsBelongedTo[currentcountry]#</cfif>) --->
				</CFQUERY>
				<!--- SSS Can you please check from this line down to what differences there are can we have a test some where to see if this code will prevent from a edited order from being edited --->
			  	<CFIF listcontains(currentorderitemsProd_parentskugroup, "#skuid#_#thisSKUParentSKUGroup#") IS NOT 0>
						<!--- if the order is confirmed we do not want any products added to this satement --->
						<CFQUERY NAME="orderExists" datasource="#application.sitedatasource#">
							select * 
							from orders
							where orderID =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and OrderStatus = 2
						</CFQUERY>
					<cfif orderExists.recordcount EQ 0>
					<!--- order currently contains this item, need to update record--->	
					<CFQUERY NAME="updateOrderItem" datasource="#application.sitedatasource#">			
						<!--- CPS 080501 Problems with rounding errors when dealing with 'Points' campaigns 
						      Set up 2 variables to store the whole 'Points' value of the discount and list price --->
						<CFIF UsePoints>
							<CFSET UnitDiscountPrice = int(#getitemdetails.DiscountPrice# / #PointsFactor#)>	
							<CFSET UnitListPrice = int(#getitemdetails.ListPrice# / #PointsFactor#)>					
						</CFIF>
						Update 
							OrderItem 
						SET 
							quantity =  <cf_queryparam value="#thisSKUQuantity#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							ParentSKUGroup =  <cf_queryparam value="#thisSKUParentSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
							<!--- CPS 080501 Problems with rounding errors when dealing with 'Points' campaigns 
							      Calculate the total points value using the whole 'Points' unit price --->							
							<CFIF UsePoints>							
								unitdiscountprice =  <cf_queryparam value="#unitDiscountPrice#" CFSQLTYPE="CF_SQL_decimal" > ,
								totaldiscountprice =  <cf_queryparam value="#unitDiscountPrice#" CFSQLTYPE="CF_SQL_decimal" >  * #thisSKUQuantity#,							
								unitlistprice =  <cf_queryparam value="#unitListPrice#" CFSQLTYPE="CF_SQL_decimal" > ,
								totallistprice =  <cf_queryparam value="#unitListPrice#" CFSQLTYPE="CF_SQL_decimal" >  * #thisSKUQuantity#,								
							<CFELSE>	
								unitdiscountprice =  <cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="CF_SQL_decimal" >  / #PointsFactor#,
 								totaldiscountprice =  <cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="CF_SQL_decimal" >  * #thisSKUQuantity# / #PointsFactor#,
								unitlistprice =  <cf_queryparam value="#getitemdetails.ListPrice#" CFSQLTYPE="CF_SQL_decimal" >  / #PointsFactor#,
	 							totallistprice =  <cf_queryparam value="#getitemdetails.ListPrice#" CFSQLTYPE="CF_SQL_decimal" >  * #thisSKUQuantity# / #PointsFactor#,													
							</CFIF>
							<cfif getItemDetails.DistiDiscount is not "">
								DistiDiscount =  <cf_queryparam value="#getItemDetails.DistiDiscount#" CFSQLTYPE="cf_sql_float" > ,
							</cfif>
							<cfif getItemDetails.ResellerDiscount is not "">
								ResellerDiscount =  <cf_queryparam value="#getItemDetails.ResellerDiscount#" CFSQLTYPE="cf_sql_float" > ,
							</cfif>
							ProductID =  <cf_queryparam value="#getitemdetails.ProductID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							PriceISOCode =  <cf_queryparam value="#getitemdetails.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						where 
							ProductID =  <cf_queryparam value="#skuid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and parentSKUGroup =  <cf_queryparam value="#thisSKUParentSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</CFQUERY>
					</cfif>
					<!--- above was wrong before - would use sku to try to identify a row - which is not good enough - skus
					are not necessarily unique --->
		
				<CFELSEIF listcontains(currentorderitemsProd_parentskugroup, "#skuid#_#thisSKUParentSKUGroup#") IS 0 AND listfind(EditableOrderStatuses,GetOrderHeader.OrderStatus) gt 0>

					<!--- only add a line if the order has not already been confirmed --->
					<!--- order doesn't already contains this item, need create new record--->	
					<!--- CPS 080501 Problems with rounding errors when dealing with 'Points' campaigns 
					      Set up 2 variables to store the whole 'Points' value of the discount and list price --->					
					<CFIF UsePoints>
						<!--- Wab added the mask "0" 2005-06-07 to get rid of commas which SQL did not like --->
						<CFSET UnitDiscountPrice = NumberFormat(#getitemdetails.DiscountPrice# / #PointsFactor#,"0")>	
						<CFSET UnitListPrice = NumberFormat(#getitemdetails.ListPrice# / #PointsFactor#,"0")>	
					</CFIF>
					<CFQUERY NAME="insertOrderItems" datasource="#application.sitedatasource#">			 
						Insert Into OrderItem 
						(
							orderid, 
							sku, 
							quantity,
							unitdiscountprice,
							totaldiscountprice,
							unitlistprice, 
							totallistprice, 
							lastupdatedby,
							lastupdated, 
							createdby,
							created, 
							Active,
							ProductID,
							PriceISOCode,
							ParentSKUGroup
							<cfif getItemDetails.DistiDiscount is not "">
								,
								DistiDiscount
							</cfif>
							<cfif getItemDetails.ResellerDiscount is not "">
								,
								ResellerDiscount
							</cfif>
						)
						Values 
						( 	<cf_queryparam value="#currentorderid#" CFSQLTYPE="cf_sql_integer" >,
							<cf_queryparam value="#getitemdetails.sku#" CFSQLTYPE="CF_SQL_VARCHAR" >,					
							<cf_queryparam value="#thisSKUQuantity#" CFSQLTYPE="cf_sql_integer" >,
							<!--- CPS 080501 Problems with rounding errors when dealing with 'Points' campaigns 
							      Calculate the total points value using the whole 'Points' unit price --->								
							<CFIF UsePoints>
								replace(<cf_queryparam value="#unitDiscountPrice#" CFSQLTYPE="CF_SQL_VARCHAR" >,',',''),
								replace(<cf_queryparam value="#unitDiscountPrice#" CFSQLTYPE="CF_SQL_VARCHAR" >,',','') * <cf_queryparam value="#thisSKUQuantity#" CFSQLTYPE="cf_sql_integer" >,						
								replace(<cf_queryparam value="#unitListPrice#" CFSQLTYPE="CF_SQL_VARCHAR" >,',',''),
								replace(<cf_queryparam value="#unitListPrice#" CFSQLTYPE="CF_SQL_VARCHAR" >,',','') * <cf_queryparam value="#thisSKUQuantity#" CFSQLTYPE="cf_sql_integer" >,
							<CFELSE>
	 							<cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="cf_sql_numeric" > / <cf_queryparam value="#PointsFactor#" CFSQLTYPE="cf_sql_numeric" >, 
 								<cf_queryparam value="#evaluate(getitemdetails.DiscountPrice*thisSKUQuantity)#" CFSQLTYPE="cf_sql_numeric" > / <cf_queryparam value="#PointsFactor#" CFSQLTYPE="cf_sql_numeric" >,
	 							<cf_queryparam value="#getitemdetails.ListPrice#" CFSQLTYPE="cf_sql_numeric" > / <cf_queryparam value="#PointsFactor#" CFSQLTYPE="cf_sql_numeric" >,
 								<cf_queryparam value="#evaluate(getitemdetails.ListPrice*thisSKUQuantity)#" CFSQLTYPE="cf_sql_numeric" > / <cf_queryparam value="#PointsFactor#" CFSQLTYPE="cf_sql_numeric" >,								
							</CFIF>
							<cf_queryparam value="#user#" CFSQLTYPE="cf_sql_integer" >,
							<cf_queryparam value="#freezedate#" CFSQLTYPE="cf_sql_timestamp" >,
							<cf_queryparam value="#user#" CFSQLTYPE="cf_sql_integer" >,
							<cf_queryparam value="#freezedate#" CFSQLTYPE="cf_sql_timestamp" >,
							1,
							<cf_queryparam value="#getitemdetails.ProductID#" CFSQLTYPE="cf_sql_integer" >,
							<cf_queryparam value="#getitemdetails.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,
							<cf_queryparam value="#thisSKUParentSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
							<cfif getItemDetails.DistiDiscount is not "">
								,
								#getItemDetails.DistiDiscount#
							</cfif>
							<cfif getItemDetails.ResellerDiscount is not "">
								,
								#getItemDetails.ResellerDiscount#
							</cfif>
						)
					</CFQUERY>
				</CFIF>			
			<CFELSEIF thisSKUQuantity IS 0 AND GetOrderHeader.OrderStatus is not ConstConfirmed>
				<!--- ie no quantity of this item set --->
				<CFIF listcontains(currentorderitemsProd_parentskugroup, "#skuid#_#thisSKUParentSKUGroup#") IS NOT 0>
			
					<!--- need to delete item from order if already exists --->
					<!--- for some reason I decided to just set to inactive rather than delete --->
				
					<CFQUERY NAME="InactivateOrderItem" datasource="#application.sitedatasource#">			
						Update 
							OrderItem 
						SET 
							active=0,
							lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
						where  
							productid =  <cf_queryparam value="#skuid#" CFSQLTYPE="CF_SQL_INTEGER" > 
							--and ParentID = #thisSKUParentSKUGroup#
							and	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
				</CFIF>				
			</cfif>
		</cfloop>
		
		<!---  Calculate Total order value, tax shipping and update the header--->
		<CFSET OrderCountry = getPersonDetails.CountryID>		
		<CFSET VATNUmber = getOrderHeader.VATNumber>		


		<!--- order items value --->
		<CFINCLUDE template="calcOrderValue.cfm"> <!--- returns query getOrderValue --->
		<cfif getordervalue.recordCount>
			<CFSET OrderValue = getordervalue.totalvalue>		
			<CFSET orderWeight = getOrderValue.totalWeight>
			<CFSET shippingmatrix = getOrderHeader.shippingmatrixid>
			
			<cfset argStruct = {spendIncentivePointsType=spendIncentivePointsType,portalViewOnly=portalViewOnly}>
			<cfif isDefined("ShowPointsforUnregisteredUser")>
				<cfset argStruct.ShowPointsforUnregisteredUser = ShowPointsforUnregisteredUser>
			</cfif>
			<cfset PointsAvailable = application.com.relayIncentive.getPointsBalanceForPortalUser(argumentCollection=argStruct).balance>
			
			<cfif usePoints and OrderValue gt PointsAvailable>
				<cftransaction action="rollback"/>
	
				<script>
					alert('The order value exceeds your points available');
				</script>
	
				<cfset pointsExceedsValue=true>
			<cfelse>
	
		
				<!--- work out shipping method --->
				<!--- assume no shipping charge orders with zero value --->
		
				<CFIF OrderValue is 0>
					<CFSET shippingCharge = 0>
					<CFSET shippingMethod = getOrderHeader.shippingMethod>
				<CFELSE>
					<CFSET shippingMethod = getOrderHeader.shippingMethod>
					<CFIF trim(shippingMethod) IS "">
						<!--- guess at default shipping method by getting the shipping method of the the first item --->
						<CFQUERY NAME="getitems" datasource="#application.sitedatasource#">			
							select 
								ShippingMethod
							from 
								vOrderItems
							where
								orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
								and active=1
						</CFQUERY>
						<CFIF getItems.recordCount is not 0>
							<CFSET shippingMethod = getItems.shippingMethod>
						</CFIF>	
					</CFIF>	
					<!--- shipping charge --->
					<CFINCLUDE template="calcShipping.cfm">	
				</CFIF>
		
				<!--- VAT --->
				<CFSET NetValue = OrderValue + shippingCharge>
				<CFINCLUDE template="calcVAT.cfm">	<!--- returns TotalVAT, VATRate --->
			
				<!---  work out order type --->			 
				<CFIF getordervalue.totalvalue is 0>
					<CFSET orderType = "I">
				<CFELSE>
					<CFSET orderType = "P">	
				</cfif>
			
			
				<CFSET totalamount = netValue + TotalVAT >
				<cfset pointsAboveWhichOrderConfirmationRequired = application.com.settings.getSetting('incentive.pointsAboveWhichOrderConfirmationRequired')> <!--- 2011/05/06 PPB LID5378 --->
	
				<CFQUERY NAME="setOrderHeader" datasource="#application.sitedatasource#">			
					Update 	Orders
					SET		orderlistvalue =  <cf_queryparam value="#getordervalue.totallistvalue#" CFSQLTYPE="CF_SQL_money" > ,
							<cfif getordervalue.totalvalue GTE pointsAboveWhichOrderConfirmationRequired and pointsAboveWhichOrderConfirmationRequired neq "">		<!--- 2011/05/06 PPB LID5378 --->
								OrderStatus =  <cf_queryparam value="#constPending#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							</cfif>
							ordervalue =  <cf_queryparam value="#getordervalue.totalvalue#" CFSQLTYPE="cf_sql_numeric" >,
							orderweight =  <cf_queryparam value="#orderweight#" CFSQLTYPE="CF_SQL_decimal" > ,
							ShippingMethod =  <cf_queryparam value="#shippingMethod#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							ShippingCharge =  <cf_queryparam value="#shippingCharge#" CFSQLTYPE="CF_SQL_decimal" > ,
							TaxOnOrder =  <cf_queryparam value="#TotalVAT#" CFSQLTYPE="CF_SQL_money" > ,
							TaxRate =  <cf_queryparam value="#VATRate#" CFSQLTYPE="CF_SQL_decimal" > ,
							OrderType =  <cf_queryparam value="#OrderType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					Where	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			</cfif>
		</cfif>
	</cftransaction>
<!--- <CFSETTING ENABLECFOUTPUTONLY="No"> --->

<!--- 	need to see whether any of the items ordered need to have questions asked
--->
	<CFINCLUDE template="qryOrderDetails.cfm">

	<CFIF listLen(getOrderItems.ScreenID) is not 0>
	
		<CFLOOP query="getOrderItems">
			<CFIF screenid is not "">
				<!--- 
				
				<cf_head>
					<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
				</cf_head>
				
				<CFSET orderItemId = itemID> --->
				
				<CFINCLUDE template="itemquestions.cfm"	>
				<!--- 
				 --->

			</cfif>
		</cfloop>
		<CF_ABORT>
	</cfif>
	
	<!--- 2007/12/17 - GCC - Added as the only way to get this in a borderset after being called from incentiveSelectPrizes.cfm. May cause problems for other order types that aren't called from incentive prizes catalogue? --->
	<cf_DisplayBorder BorderSet="#getOrderHeader.BorderSet#">
		<cfif pointsExceedsValue>
			<cfset eid=thisEid>
			<cf_translate>
				<cfinclude template="orderItemsInclude.cfm">
			</cf_translate>
		<cfelse>
			<CFINCLUDE template="editorder.cfm">
		</cfif>
	</cf_DisplayBorder>
</CFIF>


