<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			/Orders/displayProductsToOrder.cfm
Author:			?,
Date created:	?
				25-FEB-2009

Description:	

To show products and unable people to buy them


Amendment history
2009/02/25 SSS    CR-Lex592 There is now a swicth that will turn stuff of and enable the catalogue to be used on a single order basis
					The switch is called OrderSingleProduct. This has now been used through out the file to turn things on and off
					
2009/05/11			CR-sny673 added the ability to remember products between page changes
2009/06/22	NJH		Bug Fix Issue Lexmark 2390 - added an if in the query that gets the selected products. Check if SKUsonpage is not empty
WAB 2009/09/07 LID 2515 Removed #application. userfilespath#   
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate
2010/12/02	AJC		Remove PointsFactor as the variable will not exist if not using points val(discountprice / PointsFactor) 
2011/08/16	PPB		REL109 added classes/ids to allow the styling to be customised 
2014-03-24	PPB		Case 438880 changed path for thumbnails
--->

 
<cfset parentID=0>

<!--- 
	if there are products to select a order will be created 
	NJH 2008/12/12 Bug Fix Issue T-10 Bugs.. Added isNull to the query.
--->
<cfif getproductlist.recordcount GTE 1>
	<CFQUERY NAME="getOrderID" datasource="#application.sitedatasource#">
		select isNull(max(orderID),0) + 1 as maxorderID
		from orders
	</CFQUERY>
	<CFQUERY NAME="CreateOrder" datasource="#application.sitedatasource#"> 
		INSERT Into Orders
		(
			OrderID, personID
		)
		Values 
		(
			<cf_queryparam value="#getOrderID.maxorderID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<!--- NJH 2009/07/10 P-FNL069 - set the personID if on the portal so that portal users can only update their own. We check later on
			if the person that is trying to update the order is one in the same as the one who created it --->
			<cfif not request.relayCurrentUser.isInternal>
				#request.relayCurrentUser.personID#
			<cfelse>
				null
			</cfif>
		)
	</cfquery>
</cfif>
<cfif not OrderSingleProduct>

	<CFOUTPUT query="getProductsNotinGroupList">
	
		<!--- DJH 2000-12-22 - stop extra space code ---->
		<!--- 2001-05-09 CPS Ensure that the discount price is rounded to whole points --->
		<CFIF UsePoints>
			<cfset pricefields = pricefields & '<INPUT TYPE="HIDDEN" NAME="price_#productid#_#parentID#" VALUE="#val(discountprice / PointsFactor)#">'>	
		<CFELSE>
			<cfset pricefields = pricefields & '<INPUT TYPE="HIDDEN" NAME="price_#productid#_#parentID#" VALUE="#val(discountprice)#">'>		<!--- START: 2010/12/02 AJC Remove PointsFactor as the variable will not exist if not using points val(discountprice / PointsFactor) --->		
		</CFIF>	
		
		<!---  work out quantity of current item ordered --->
		<CFIF listfind(currentorderitems,productid) is not 0>
			<CFSET quantityofCurrentItem = listgetat(currentorderitemsquantity, listfind(currentorderitems,"#productid#"))>
		<CFELSE>
			<CFSET quantityofCurrentItem = 0>
		</cfif>
			<input type="hidden" name="line_#productid#_#parentID#" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentID#")>#evaluate("form.line_#productid#_#parentID#")#</cfif>">
			<INPUT TYPE="hidden" NAME="quant_#productid#_#parentID#" value= "<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentID#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentID)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>">			
		<CFSET SKUsonpage=listappend(SKUsonpage,#productid#)>			
		<CFSET SKUsParentsonpage=listappend(SKUsParentsonpage,#parentid#)>
	</cfoutput>
</cfif>
<cf_translate>										
<CFOUTPUT query="getProductsinGroupList" startrow="#prodStartRow#" maxrows="#NumberPrizesPerPage#">

					<!--- DJH 2000-12-22 - stop extra space code ---->
					<!--- 2001-05-09 CPS Ensure that the discount price is rounded to whole points --->
					<CFIF UsePoints>
						<cfset pricefields = pricefields & '<INPUT TYPE="HIDDEN" NAME="price_#productid#_#parentID#" VALUE="#val(discountprice / PointsFactor)#">'>	
					<CFELSE>
						<cfset pricefields = pricefields & '<INPUT TYPE="HIDDEN" NAME="price_#productid#_#parentID#" VALUE="#val(discountprice)#">'>		<!---  START: 2010/12/02 AJC Remove PointsFactor as the variable will not exist if not using points val(discountprice / PointsFactor) --->		
					</CFIF>		
					<cfif getProductsinGroupList.currentRow mod 2 eq 1>
						<cfset rowClass = "dataRowOdd">
					<cfelse>
						<cfset rowClass = "dataRowEven">
					</cfif>  	
					<TR class="#rowClass#">
					<CFIF ShowPictures>

						<TD ALIGN="center" VALIGN="top">
							<!--- START 2014-03-24	PPB		Case 438880 changed path for thumbnails --->	
							<cfif fileexists("#application.paths.content#\linkImages\product\#productId#\Thumb_#productId#.jpg")>
								<!--- WAB 2009/09/07 LID 2515 Removed #application. userfilespath#   --->
								<A HREF="javascript:detail(#ProductID#)"><IMG alt="#SKU#" src="/content/linkImages/product/#productId#/Thumb_#productId#.jpg" BORDER=0></A>
							</cfif>
							<!--- END 2014-03-24 --->
						</TD>
						
					</CFIF>
					
					<cfif ShowProductCode or ShowProductCodeAsAnchor>								
						<TD ALIGN="left" VALIGN="top">#htmleditformat(SKU)#</td>
					</cfif>

					<cfif ShowDescription>
						<TD ALIGN="left" VALIGN="top">
							<cfif TranslateDescription>
							<A HREF="javascript:detail(#ProductID#)">phr_Description_Product_#htmleditformat(ProductID)#</a>
								
							<cfelse>
								#htmleditformat(Description)#
							</cfif>
							
							<cfif isDefined("cookie.translator") and cookie.translator is not "">
								<a href="javascript: editProduct(#productid#, '#ThisProductGroup#')">...</a>
							</cfif>
						</TD>						
					</cfif>
					<CFIF ShowListPrice>
						<TD class="marketingStore_textAlignCenter" ALIGN="left" VALIGN="top" NOWRAP>
							#decimalformat(ListPrice)#
						</TD>
					</cfif>					
					<CFIF ShowDiscountPrice>
						<TD class="marketingStore_textAlignCenter" ALIGN="left" VALIGN="top">
							#decimalformat(DiscountPrice)#
						</td>
					<cfelseif ShowPointsValue>
						<TD ALIGN="left" VALIGN="top">
							#LSNumberFormat(round(val(DiscountPrice / PointsFactor)))#
						</TD>
					</cfif>
					<cfif ShowAvailability>
						<TD ALIGN="left" VALIGN="top">
							phr_ProductAvailability_Product_#htmleditformat(ProductID)#
						</TD>
					</cfif>
					<CFIF ShowNoInStock>
						<TD class="marketingStore_textAlignCenter" ALIGN="left" VALIGN="top">
							#htmleditformat(NumberAvailable)#
						</TD>
					</CFIF>
	
				<!---  work out quantity of current item ordered --->
				<CFIF listfind(currentorderitems,productid) is not 0>
					<CFSET quantityofCurrentItem = listgetat(currentorderitemsquantity, listfind(currentorderitems,"#productid#"))>
				<CFELSE>
					<CFSET quantityofCurrentItem = 0>
				</cfif>


				<cfif ShowMaxQuantity>
					<TD class="marketingStore_textAlignCenter" ALIGN="left" VALIGN="top">
						#iif(MaxQuantity is 0,DE(""),MaxQuantity )#
					</td>
				</cfif>
				<cfif ShowPreviouslyOrdered>
					<TD class="marketingStore_textAlignCenter" ALIGN="left" VALIGN="top">
						#htmleditformat(GetProductList.PreviouslyOrdered)#
					</td>
				</cfif>
				<!--- cr-SNY673 added to this cfif if the product group is selected as affordable --->
				<!--- NJH 2009/07/23 LID 2472 trim productGroup as spaces will force it to not be equal.--->
				<cfif (ThisProductGroup is "None" or ThisProductGroup is trim(getProductsinGroupList.ProductGroup) or ThisProductGroup is "Affordable" or showallproducts) and ShowQuantity and (MaxQuantity GT PreviouslyOrdered or MaxQuantity is 0 )>
					<!--- only display text if we're on appropriate group, or groups not used --->
					<!--- NJH 2007/01/24 align td's center. They were aligned left. --->
					<!--- SSS 2009/02/25 cr-lex952 no quanities are to be shown if in single order mode --->
					<cfif not OrderSingleProduct>
					<TD ALIGN="center" VALIGN="top">
						<INPUT class="form-control" TYPE="text" NAME="quant_#productid#_#parentID#" value="<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentID#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentID)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>" size="4" maxlength="4" onFocus="valueIs = this.value; fieldIs = #htmleditformat(productID)#; this.select()" onBlur="fieldWas = #htmleditformat(productID)#; doLineTotal(this, #htmleditformat(productID)#, false,0)" onChange="dirty.value = true" style="text-align:right;">
							</td>
					<cfelse>
					
						<!--- SSS 2009/02/25 CR-Lex 952 show a submit button if in single order mode --->
						<cfif not portalViewOnly>
							<TD ALIGN="center" VALIGN="top">
								<CF_INPUT type="button" value="Phr_Incentive_Buynow" class="buynow" onclick="javascript:chooseProduct(#productid#)">
							</TD>
						</cfif>
						<INPUT TYPE="hidden" NAME="quant_#productid#_#parentID#" value="<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentID#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentID)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>" size="4" maxlength="4" onFocus="valueIs = this.value; fieldIs = #htmleditformat(productID)#; this.select()" onBlur="fieldWas = #htmleditformat(productID)#; doLineTotal(this, #htmleditformat(productID)#, false,0)" onChange="dirty.value = true" style="text-align:right;">
					</cfif>
					<cfif ShowLineTotal and not OrderSingleProduct>
							<TD ALIGN="center" VALIGN="top">
								<!---<input type="text" name="line_#productid#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#")>#evaluate("form.line_#productid#")#</cfif>" onFocus="fieldIs = #productID#; doLineTotal(quant_#productid#, #productID#)"> --->
								<input class="form-control" type="text" name="line_#productid#_#parentID#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentID#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentID)#")#</cfif>" onFocus="fieldIs = #htmleditformat(productID)#; doNavigate(this)" DISABLED style="text-align:right;">
							</td>
					<cfelse>
						<input type="hidden" name="line_#productid#_#parentID#" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentID#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentID)#")#</cfif>">
						</td>
					</cfif>
				</cfif>
		
				<CFSET SKUsonpage=listappend(SKUsonpage,#productid#)>			
				<CFSET SKUsParentsonpage=listappend(SKUsParentsonpage,#parentid#)>	
				</tr>
			<!---<cfif ThisProductGroup is "None" or ThisProductGroup is getProductlist.ProductGroup>
				</tr>
			</cfif>--->
		</cfoutput>
		
		<!--- SSS 2009/05/11 added this so that selected products are remembered between page changes only needed if if not in OrderSingleProduct mode --->
		<cfif not OrderSingleProduct>
		
			<!--- NJH 2009/06/22 Bug Fix Lexmark Issue 2390 - moved this into the if clause to tidy things up, as it's only run if not single order. 
				also added the if statement in the query (ie.check if skus on page does not equal an empty string)
			--->
			<!--- only if not single order --->
			<cfquery name = "ProductsSelected" dbtype="query">
				select * from getProductsinGroupList where 1=1 
				<cfif SKUsonpage neq "">
					and productid not in (#SKUsonpage#)
				</cfif>
			</cfquery>
			
			<cfoutput>
			<cfloop query="ProductsSelected">
				<cfif isdefined("form.quant_#productid#_#parentID#") and form["quant_#productid#_#parentID#"] is not 0>
					<input type="hidden" name="line_#productid#_#parentID#" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentID#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentID)#")#</cfif>">
					<INPUT TYPE="hidden" NAME="quant_#productid#_#parentID#" value= "<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentID#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentID)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>">
				</cfif>
			</cfloop>
			</cfoutput>
		</cfif>
</cf_translate>		

