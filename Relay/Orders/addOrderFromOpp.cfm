<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfparam name="opportunityID" type="numeric" default="">

<cf_translate>
<cfoutput>



<cf_head>
	<cf_title>Add Order from Opportunity</cf_title>


</cf_head>

<cf_body style="text-align: center;">

<cfif not isDefined("opportunityID") or opportunityID eq "">
	<cfoutput>opportunityID must be passed as a parameter.</cfoutput>
	<CF_ABORT>
</cfif>

<cfif isDefined("form.ConfirmOppToOrder")>

	<cfset newOrderID = application.com.relayEcommerce.convertOppToOrder(opportunityID=#opportunityID#)>
	<cfoutput>
		<script>
			if(!opener.closed){
				opener.parent.document.location.href='/orders/editOrder.cfm?currentOrderID=#jsStringFormat(newOrderID)#';
				self.close();
			}
		</script>
	</cfoutput>

<cfelse>

	<cfquery name="orderExists" datasource="#application.SiteDataSource#">
		select orderID from opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfquery name="getNumOppProducts" datasource="#application.SiteDataSource#">
		select count(ProductORGroupID) as numOppProducts from opportunityProduct where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfif orderExists.recordCount gt 0 and orderExists.orderID gt 0>
		<!--- An order exists for this opportunity --->
		phr_oppOrderAlreadyExists. <br> phr_oppWouldYouLikeToViewOrder?
	<cfelse>
		<!--- This opportunity does not have products associated with it. Don't create order --->
		<cfif getNumOppProducts.numOppProducts eq 0>
			phr_oppOppDoesNotHaveProducts. <br> phr_oppPleaseAddProductsToOppBeforeCreatingOrder.
		<cfelse>
			<!--- Create an order --->
 	phr_oppYouAreAboutToPlaceAnOrderForThisOpportunity.<br> phr_oppPleaseConfirm.
	</cfif>
	</cfif>

	<cfform name="mainForm" method="post">
		<cfinput type="button" name="cancelButton" value="phr_Cancel" onClick="javascript:self.close();">

		<cfif orderExists.recordCount gt 0 and orderExists.orderID gt 0>
			<cfinput type="button" name="frmViewOrder" value="phr_oppViewOrder" onClick="if(!opener.closed){opener.parent.document.location.href='/et.cfm?eid=PreviousOrders&orderID=#orderExists.orderID#';self.close();}">
		<cfelseif getNumOppProducts.numOppProducts gt 0>
		<cfinput type="submit" name="confirmOppToOrder" value="phr_Confirm">
		</cfif>

	</cfform>
</cfif>



</cfoutput>
</cf_translate>
