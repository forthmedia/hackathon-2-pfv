<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Check that at least one checkbox has been clicked --->
<CFIF NOT IsDefined("chkDispatch") and NOT IsDefined("chkCancel")>
	<!--- If no checkboxes have been checked then throw user back to previous page --->
	<CFLOCATION URL="Report2.cfm?btnViewStatus=3"addToken="false">
	<CF_ABORT>
</CFIF>

<CFSET tmpCurrencySymbol = '$'>

<CFPARAM name="chkDispatch" default="0">
<CFPARAM name="chkCancel" default="0">

<CFQUERY NAME="getOrderHeaderDispatch" datasource="#application.sitedatasource#">
select 	o.* 
		from 	orders as o
where 	o.OrderID  IN ( <cf_queryparam value="#chkDispatch#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>

<CFQUERY NAME="getOrderHeaderCancel" datasource="#application.sitedatasource#">
select 	o.* 
		from 	orders as o
where 	o.OrderID  IN ( <cf_queryparam value="#chkCancel#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>








<table>
<tr>
    <td><B>Order ID</b></td>
	<td><B>Order No</b></td>
    <td><B>Date</b></td>
	<td><B>Delivery Site Name</b></td>
    <td><B>Value</b></td>
    <td><B>Pay Method</b></td>
</tr>

<CFIF getOrderHeaderDispatch.recordcount is not 0>
<tr><TD colspan="6">The following Orders are ready to be marked as Dispatched</td></tr>

<cfloop query="getOrderHeaderDispatch">
<tr>
    <td><cfoutput>#htmleditformat(OrderID)#</cfoutput></td>
	<td><cfoutput>#htmleditformat(OrderNumber)#</cfoutput></td>
    <td><cfoutput>#htmleditformat(DAY(OrderDate))# #LEFT(MonthAsString(Month(OrderDate)),3)# #htmleditformat(YEAR(OrderDate))#</cfoutput></td>
	<td><cfoutput>#htmleditformat(delSitename)#</cfoutput></td>
    <td align="right"><cfoutput>#htmleditformat(tmpCurrencySymbol)##DecimalFormat(OrderValue)#</cfoutput></td>
    <td><cfif #PaymentMethod# IS "">Not Specified<cfelse><cfoutput>#htmleditformat(PaymentMethod)#</cfoutput></cfif></td>
</tr>
</cfloop>
</CFIF>
<CFIF getOrderHeaderCancel.recordcount is not 0>
<tr><TD colspan="6">The following Orders will be marked as cancelled</td></tr>
<cfloop query="getOrderHeaderCancel">
<tr>
    <td><cfoutput>#htmleditformat(OrderID)#</cfoutput></td>
	<td><cfoutput>#htmleditformat(OrderNumber)#</cfoutput></td>
    <td><cfoutput>#htmleditformat(DAY(OrderDate))# #LEFT(MonthAsString(Month(OrderDate)),3)# #htmleditformat(YEAR(OrderDate))#</cfoutput></td>
	<td><cfoutput>#htmleditformat(delSitename)#</cfoutput></td>
    <td align="right"><cfoutput>#htmleditformat(tmpCurrencySymbol)##DecimalFormat(OrderValue)#</cfoutput></td>
    <td><cfif #PaymentMethod# IS "">Not Specified<cfelse><cfoutput>#htmleditformat(PaymentMethod)#</cfoutput></cfif></td>
</tr>
</cfloop>
</CFIF>

</table>
<FORM METHOD="POST" ACTION="ConfirmDespatched.cfm">
	<INPUT TYPE="SUBMIT" NAME="btnConfirm" VALUE=" CONFIRM "> <INPUT TYPE="SUBMIT" NAME="btnCancel" VALUE=" CANCEL ">
	<CF_INPUT TYPE="HIDDEN" NAME="chkDispatch" VALUE="#chkDispatch#">
	<CF_INPUT TYPE="HIDDEN" NAME="chkCancel" VALUE="#chkCancel#">
</FORM>


