<!--- �Relayware. All Rights Reserved 2014 --->







<p>This utility should be used to reset a troublesome license number.</p>
<CFIF IsDefined("license")>
	<CFQUERY NAME="checkL" DATASOURCE="#application.sitedatasource#">
		SELECT 1 FROM RelayLicenseSeats WHERE Licensekey =  <cf_queryparam value="#license#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</CFQUERY>

	<CFIF checkL.recordcount GT 0>
		<CFQUERY NAME="reset" DATASOURCE="#application.sitedatasource#">
			UPDATE RelayLicenseSeats
			SET Accessed = NULL, PersonID = NULL, RKID = NULL
			WHERE Licensekey =  <cf_queryparam value="#license#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFQUERY>
		<cfset application.com.globalFunctions.cfcookie(NAME="RKID", EXPIRES="NOW")>
		<CFQUERY NAME="reset" DATASOURCE="#application.sitedatasource#">
			DELETE FROM RelayEntityKeys WHERE entity='person' AND entityid=#request.relayCurrentUser.personid#
		</CFQUERY>
		<p>License key reset</p>
	<CFELSE>
		<p>The license key entered - <cfoutput>"#htmleditformat(form.license)#"</cfoutput> cannot be found in the RelayLicenseSeats table.</p>
	</CFIF>
<CFELSE>
	<p>Please enter a license in the field below.</p>
</CFIF>

<form action="" method="post" name="mainForm">
	<p>Enter a license key to reset</p>
	<input type="text" name="license" value="0">
	<input type="submit">
</form>


