<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-11-16  DAN PROD2015-364 - products country restrictions

--->
<script>
var records = new Array()</script>
<cfset parentSKUGroup=0>
<cfset filteredproductidlist="">
<cfset filteredproductidlist2="">
<cfset filteredproductidlist3="">
<cfset filteredproductidlist4="">

<!--- get all child products --->
<cfquery name="qry_get_excludeproductIDs" datasource="#application.sitedatasource#">
	Select distinct(productSKUGroup) from productparentsGlobalSKU where campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>
<!--- create productchild list --->
<cfoutput query="qry_get_excludeproductIDs">
	<cfset filteredproductidlist=listappend(filteredproductidlist, productSKUGroup)>
</cfoutput>
<!--- get all first gen products --->
<cfquery name="qoq_get_firstgenproducts" dbtype="query">
Select *,SKUGroup as productSKUGroup from getProductList
where NOT SKUGroup IN (#listqualify(filteredproductidlist,"'")#)
</cfquery>

<!------------------------------------------------------->

<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<!----------------------------------------------------------------------------->
<SCRIPT type="text/javascript">
	<cfoutput query="qoq_get_firstgenproducts">
		var status#SKUGroup#=false;
	</cfoutput>
	function displayaccessories(parentSKUGroup) {
		<cfoutput query="qoq_get_firstgenproducts">
			<cfquery name="qoq_get_secondgenproducts" datasource="#application.sitedatasource#">
				Select distinct(productSKUGroup) from productparentsGlobalSKU
				inner join vproductlist on productparentsGlobalSKU.productskugroup = vproductlist.skugroup
                left join vCountryScope vcs on vproductlist.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = vproductlist.productid and vcs.countryid = <cf_queryparam value="#countryID#" cfsqltype="CF_SQL_INTEGER">
				where parentSKUGroup =  <cf_queryparam value="#productSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  and productparentsGlobalSKU.campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
                AND (vproductlist.hasCountryScope = 0 OR vcs.countryid is not null)
                <!--- AND CountryID =  <cf_queryparam value="#CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  --->
			</cfquery>
			<!--- create product2gen list --->
			
			<cfset parentSKUGroup=productSKUGroup>
			
			if(parentSKUGroup=="#parentSKUGroup#")
			{
				if(status#parentSKUGroup#==false)
				{
					status#parentSKUGroup#=true;
				}
				else
				{
					status#parentSKUGroup#=false;
				}
				<cfloop query="qoq_get_secondgenproducts">
					if(status#parentSKUGroup#==true)
					{
						document.getElementById('product_#qoq_get_secondgenproducts.productSKUGroup#_#parentSKUGroup#').style.display = '';
					}
					else
					{
						document.getElementById('product_#qoq_get_secondgenproducts.productSKUGroup#_#parentSKUGroup#').style.display = 'none';
					}
				</cfloop>
			}
			
			
		</cfoutput>	
	}
</script>
<!----------------------------------------------------------------------------->
<!-------------<!--- output second gen products --->
			//<cfloop query="qoq_get_secondgenproducts">
			
			//if( parentID == "#parentID#" )
			//{
			//  	<cfloop query="qoq_get_secondgenproducts">
			//		document.getElementById('product_#qoq_get_secondgenproducts.productID#_#parentID#').style.display = '';
			//	</cfloop>
			//}
			//</cfloop>		---------------------------------------------------------------->
<!----------------------------------------------------------------------------->

<!-------------------------------------------------------->







<!--- output first gen products---->
<cfloop query="qoq_get_firstgenproducts">

	<cfset parentSKUGroup=0>
	<cfinclude template="displayproductformfields.cfm">
	<!--- if product has children --->
	<cfif NOOFCHILDREN neq 0>
		<!--- get second gen productIDs --->
		<cfquery name="qry_get_childproductIDs2" datasource="#application.sitedatasource#">
			Select distinct(productSKUGroup) from productparentsGlobalSKU
			where parentSKUGroup =  <cf_queryparam value="#productSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  and campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<!--- create product2gen list --->
		<cfloop query="qry_get_childproductIDs2">
			<cfset filteredproductidlist2=listappend(filteredproductidlist2, qry_get_childproductIDs2.productSKUGroup)>
		</cfloop>
		<!--- get second gen products --->
		<cfquery name="qoq_get_secondgenproducts" dbtype="query">
			Select *,SKUGroup as productSKUGroup from getProductList
			where SKUGroup IN (#listqualify(filteredproductidlist2,"'")#)
		</cfquery>
		<cfset parentSKUGroup=#qoq_get_firstgenproducts.productSKUGroup#>
		<!--- output second gen products --->
		<cfloop query="qoq_get_secondgenproducts">
			<cfinclude template="displayproductformfields.cfm">
			<!--- &nbsp;&nbsp;&nbsp;#qoq_get_secondgenproducts.productid# - #qoq_get_secondgenproducts.description#<br> --->
			<!--- if product has children --->
		 	<cfif qoq_get_secondgenproducts.NOOFCHILDREN neq 0>
				<!--- get second gen productIDs --->
				<cfquery name="qry_get_childproductIDs3" datasource="#application.sitedatasource#">
					Select distinct(productSKUGroup) from productparentsGlobalSKU
					where parentSKUGroup =  <cf_queryparam value="#qoq_get_secondgenproducts.productSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  and campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				<!--- create product2gen list --->
				<cfloop query="qry_get_childproductIDs3">
					<cfset filteredproductidlist3=listappend(filteredproductidlist3, qry_get_childproductIDs3.productSKUGroup)>
				</cfloop>
				<!--- get second gen products --->
				<cfquery name="qoq_get_thirdgenproducts" dbtype="query">
					Select *,SKUGroup as productSKUGroup from getProductList
					where SKUGroup IN (#listqualify(filteredproductidlist3,"'")#)
				</cfquery>
				<!--- output second gen products --->
				<cfset parentSKUGroup=#qoq_get_secondgenproducts.productSKUGroup#>
				<cfloop query="qoq_get_thirdgenproducts">
					<!--- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#qoq_get_thirdgenproducts.productid# - #qoq_get_thirdgenproducts.description#<br> --->
					<cfinclude template="displayproductformfields.cfm">
					<!--- if product has children --->
					<cfif qoq_get_thirdgenproducts.NOOFCHILDREN neq 0>
						<!--- get second gen productIDs --->
						<cfquery name="qry_get_childproductIDs4" datasource="#application.sitedatasource#">
							Select distinct(productSKUGroup) from productparentsGlobalSKU
							where parentSKUGroup =  <cf_queryparam value="#qoq_get_thirdgenproducts.productSKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >  and campaignID =  <cf_queryparam value="#campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
						<!--- create product2gen list --->
						<cfloop query="qry_get_childproductIDs4">
							<cfset filteredproductidlist4=listappend(filteredproductidlist4, qry_get_childproductIDs4.productSKUGroup)>
						</cfloop>
						<!--- get second gen products --->
						<cfquery name="qoq_get_fourthgenproducts" dbtype="query">
							Select *,SKUGroup as productSKUGroup from getProductList
							where SKUGroup IN (#listqualify(filteredproductidlist4,"'")#)
						</cfquery>
						<!--- output second gen products --->
						<cfset parentSKUGroup=#qoq_get_thirdgenproducts.productSKUGroup#>
						<cfloop query="qoq_get_fourthgenproducts">
						<!--- 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#qoq_get_fourthgenproducts.productid# - #qoq_get_fourthgenproducts.description#<br> --->
							<cfinclude template="displayproductformfields.cfm">
						</cfloop>
						
						<!--- set list to null --->
						<cfset filteredproductidlist4="">
					</cfif>
				</cfloop>
				<cfset parentSKUGroup=#qoq_get_secondgenproducts.productSKUGroup#>
				<!--- set list to null --->
				<cfset filteredproductidlist3="">
			</cfif>
		 </cfloop>
		<cfset parentSKUGroup=#qoq_get_firstgenproducts.productSKUGroup#>
		<!--- set list to null --->
		<cfset filteredproductidlist2="">
	</cfif>
</cfloop>