<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			qryDistributorOrderHeader.cfm
Author:			DJH
Date created:	14 June 2000

Description:	Query to get a list of Orders/Order Items for a particular distributor.

Version history:
1
2000-11-23 		WAB the created by was always bringing up 'Web User', so now get the name of the person the order is for
1.1			2001-05-12 WAB amended it so that see all orders for that organisation  (so person doesn't have to be at the same location)

--->

<!--- 
Need to identify the distributor - that is done by looking at the current users information, and identifying that 
they are allowed to use the template because they are flagged as the distributor main contact.
--->

<cfquery name="ThisDisti" datasource="#application.siteDataSource#">
select
	location.Organisationid,
	location.LocationID,
	sitename 
from
	location inner join
	person on person.locationid = location.locationid
where
	person.personid = #request.relayCurrentUser.personid#
</cfquery>



<cfquery name="qryDistributorOrderHeader" datasource="#application.siteDataSource#">
select 	
	o.* ,
	promotion.*,
	p.firstname,
	p.lastname,
	p.email,
	l.sitename,
	c.countrydescription,
	c.countrycurrency,
	c.countryid,
	c.AllowVATReduction, c.VATFormat,
	CASE WHEN o.createdby =  <cf_queryparam value="#application.webuserid#" CFSQLTYPE="CF_SQL_INTEGER" >  THEN p.firstname + ' ' + p.lastname ELSE (select name from usergroup where usergroupid=o.createdby) END as CreatedByName,
	(select email from usergroup, person where person.personid = usergroup.personid and usergroupid=o.createdby) as CreatedByEmail,
	(select countryDescription from Country where countryID = delCountryID) as delCountryName,
	(select countryDescription from Country where countryID = invCountryID) as invCountryName
from
 	orders as o, 
	location as l, 
	person as p, 
	country as c, 
	promotion,
	location as distiLocation
where
	o.personid=p.personid
	and	o.locationid=l.locationid
	and	l.countryid=c.countryid
	and	o.promoid=promotion.promoid
	and promotion.promoID =  <cf_queryparam value="#promoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	and o.OrderStatus  in ( <cf_queryparam value="#form.selectedOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and (select count(*) from vOrderItems where vOrderItems.orderID = o.OrderID) > 0
	and DistiLocation.locationid = DistiLocationID
	and DistiLocation.organisationid =  <cf_queryparam value="#ThisDisti.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
order by 
	<!--- User Chooses the order --->
	<cfif isDefined("url.OrderIDSort")>
		OrderID #evaluate("AscDesc_#OrderIDSort#")#
<!---	<cfelseif isDefined("url.OrderNumberSort")>
		OrderNumber #evaluate("AscDesc_#OrderNumberSort#")#--->
	<cfelseif isDefined("url.TheirOrderNumberSort")>
		TheirOrderNumber #evaluate("AscDesc_#TheirOrderNumberSort#")#
	<cfelseif isDefined("url.OrderDateSort")>
		OrderDate #evaluate("AscDesc_#OrderDateSort#")#
	<cfelseif isDefined("url.CreatedByNameSort")>
		(select name from usergroup where usergroupid=o.createdby) #evaluate("AscDesc_#CreatedByNameSort#")#
	<cfelseif isDefined("url.DelCountryNameSort")>
		(select countryDescription from Country where countryID = delCountryID) #evaluate("AscDesc_#DelCountryNameSort#")#
	<cfelseif isDefined("url.ShippedDateSort")>
		ShippedDate #evaluate("AscDesc_#ShippedDateSort#")#
	<cfelseif isDefined("url.OrderStatusSort")>
		OrderStatus #evaluate("AscDesc_#OrderStatusSort#")#
	<cfelse>
		OrderDate,
		OrderStatus
	</cfif>
	
</cfquery>
