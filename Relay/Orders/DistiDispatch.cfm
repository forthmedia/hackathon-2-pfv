<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes">
<!---

File:			DistiDispatch.cfm
Author:			DJH
Date created:	14 June 2000

Description:	Displays a list of orders particular to a Distributor, and allows them to record shipment information
				against orders/order items.

Version history:
1
2000-11-23	WAB 	Added some security - need to have DistributorTools rights to ordertask
					(updated 'distributor main contact' user group to have this permission at the same time)
					Also added site name to the report (below order placed by)
2001-03-28  	CPS     use application variables StatusDescriptions
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
--->


<CFIF checkPermission.DistiTools is 0>
	Sorry you do not have rights to this application 
	<CF_ABORT>

</cfif>


<cfoutput>



<cf_head>
	<cf_title>Distributor Dispatch</cf_title>
	<style type="text/css">
	.bottomline 
	{
		border-bottom-width : 1px;
		border-bottom : 1px solid Gray;
		clear : none;
		display : inline;		
	}
	</style>
</cf_head>
</cfoutput>

<!--- temporary testing params --->

<cfparam name="promoID" default="Evaluation2000">
<cfparam name="distributor" default="">

<cfset DistiOrderStatuses = "2,10,11">

<cfset status_Confirmed = 2>
<cfset status_PartialDispatch = 10>
<cfset status_Dispatched = 11>

<!--- include the query --->
<cfparam name="form.selectedOrderStatuses" default="#DistiOrderStatuses#">


<cf_translate encode="javascript">
<cfoutput>
<script language="JavaScript">

function popUpWindow(url)
{
	var now = new Date()
	now = now.getTime()
	myWin = window.open(url + (url.indexOf('?') > -1? '&': '?' ) + 'dummy=' + now, 'popUp', 'scrollbars,statusbar,width=' + parseInt(screen.availWidth * 0.8) + ',height=' + parseInt(screen.availHeight * 0.8))
	myWin.moveTo(parseInt(screen.availWidth * 0.1), parseInt(screen.availWidth * 0.1)) 
	myWin.focus()
}

function submitForm(urlparam)
{
	form = document.theForm
	if (urlparam != null)
	{
		form.action += "?" + urlparam
	}
	if (datesOK())
		form.submit()
	else
		alert("phr_Order_CheckDates")
}

function alertDateChecked(afield)
{
	// function to display an alert box if the date is invalid
	if (!checkEuroDate(afield.value))
	{
		alert("phr_Order_CheckDates")
		afield.focus()
		afield.select()
		return false
	}
}

function datesOK()
{
	var retval = true
	var re = /[sS]hippedDate/
	var form = document.theForm
	// function to ensure that all the dates on this form have been correctly filled in
	//loop around every date element
	for (var i = 0; i < form.length; i++)
	{
		// aslong as its a date field
		if (re.test(form[i].name))
		{
			// make sure the date is valid
			if (!checkEuroDate(form[i].value))
			{
				retval = false
				break
			}
		}
	}	
	return retval
}

function today (afieldname)
{
	todaydate = "#dateformat(now(),"dd-mmm-yyyy")#"
	afield = eval("document.theForm." + afieldname)
	if (afield.value.length == 0)
		afield.value = todaydate
}

function updateMe(anOrderID)
{
	form = document.theForm
	form.frmAction.value = "update"
	if (anOrderID != null)
	{
		lineUpdateObj = eval("form.lineUpdated_" + anOrderID)
		lineUpdateObj.value = 1
		orderStatusObj = eval("form.orderStatus_" + anOrderID)
		shippedDateObj = eval("form.shippedDate_" + anOrderID)
		if(orderStatusObj.options[orderStatusObj.selectedIndex].value == #jsStringFormat(status_Dispatched)#)
		{ 
			if(shippedDateObj.value.length == 0) 
				today(shippedDateObj.name)
		}
		else
			shippedDateObj.value = ''
	}
}

function itemUpdateMe(alineitem)
{
	form = document.theForm
	lineItemUpdateObj = eval("form.lineItemUpdated_" + alineitem)
	lineItemUpdateObj.value = 1
}


function checkEuroDate(theDate) 
{
    if (theDate.length == 0)
        return true;
	isplit = theDate.indexOf('-');

	if (isplit == -1 || isplit == theDate.length)
		return false;
    sDay = theDate.substring(0, isplit);
	monthSplit = isplit + 1;
	isplit = theDate.indexOf('-', monthSplit);

	if (isplit == -1 || (isplit + 1 ) == theDate.length)
		return false;
    sMonth = theDate.substring((sDay.length + 1), isplit);
	sYear = theDate.substring(isplit + 1);
	sMonth = checkMonth(sMonth);

	if (!numberRange(sMonth, 1, 12)) // check month
		return false;
	else
	if (!checkInteger(sYear)) //check year
		return false;
	else
	if (!numberRange(sYear, 0, null)) //check year
		return false;
	else
	if (!checkInteger(sDay)) //check day
		return false;
	else
	if (!checkDay(sYear, sMonth, sDay)) //check day
		return false;
	else
		return true;
}

function checkMonth(theMonth) {
	var monthList1 = new Array ("jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
	var returnMonth = -1;
	for(var i = 0; i < 12; i++) {
		if(monthList1[i] == theMonth.toLowerCase()) {
			returnMonth = i+1;
		}
	}
	return returnMonth;
}

function checkDay(checkYear, checkMonth, checkDay) {
	maxDay = 31;
	if (checkMonth == 4 || checkMonth == 6 ||
			checkMonth == 9 || checkMonth == 11)
		maxDay = 30;
	else
	if (checkMonth == 2) {
		if (checkYear % 4 > 0)
			maxDay =28;
		else
		if (checkYear % 100 == 0 && checkYear % 400 > 0)
			maxDay = 28;
		else
			maxDay = 29;
	}
	return numberRange(checkDay, 1, maxDay);
}

function checkInteger(checkThis) {
	var newLength = checkThis.length
	for(var i = 0; i != newLength; i++) {
		aChar = checkThis.substring(i,i+1)
		if(aChar < "0" || aChar > "9") {
			return false
		}
	}
	return true
}

function numberRange(object_value, min_value, max_value) {
    if (min_value != null) {
        if (object_value < min_value)
		return false;
	}

    if (max_value != null) {
		if (object_value > max_value)
		return false;
	}
    return true;
}


</script>
</cfoutput>
</cf_translate>

<cfparam name="frmAction" default="">

<!--- if the user has clicked update, or has clicked through to details when they had changed stuff, update everything --->

<cfif frmAction is 'update'>
	<!--- first, process the line item records --->
	<cfif isDefined("ItemIDList")>
		<cfloop list="#ItemIDList#" index="anItemID">
			<cfif evaluate("form.lineItemUpdated_#anItemID#") is true>
				<cfquery name="updateOrderItem" datasource="#application.siteDataSource#">
					update	
						orderItem
					set
						<cfif evaluate("form.quantityShipped_#anItemID#") is not "">
							QuantityShipped = #evaluate("form.quantityShipped_#anItemID#")#,
						</cfif>
						<cfif evaluate("form.ItemShippedDate_#anItemID#") is not "">
							ShippedDate = #createODBCDate(evaluate("form.ItemShippedDate_#anItemID#"))#
						<cfelse>
							ShippedDate = null
						</cfif>
					where
						ItemID =  <cf_queryparam value="#anItemID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>		
			</cfif>	
		</cfloop>		
	</cfif>
	
	
	<!--- now set the status of any records where the line items add up to the record being fully shipped to shipped --->
	
	<cfquery name="updateOrderHeaderDispatched" datasource="#application.siteDataSource#">
		update
			orders
		set
			OrderStatus =  <cf_queryparam value="#status_Dispatched#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			ShippedDate =	(select 
								Max(ShippedDate) 
							from 
								OrderItem
							where
								Orders.OrderID = OrderItem.OrderID
							)
		where
			orderStatus <>  <cf_queryparam value="#status_Dispatched#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and OrderID  in ( <cf_queryparam value="#form.OrderIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and 
			(select
				count(*)
			from
				OrderItem
			where
				Orders.OrderID = OrderItem.OrderID
				and Quantity = QuantityShipped				
			)

			=
			
			(select
				count(*)
			from
				OrderItem
			where
				Orders.OrderID = OrderItem.OrderID
			)
	</cfquery>
	
	<!--- now loop round the various fields updating where necessary. --->
	<!--- now process the order header records - ignoring dispatched records --->
	<cfloop list="#form.OrderIDList#" index="anOrderID">
		<cfif evaluate("form.lineUpdated_#anOrderID#") is true>
			<cfquery name="updateOrderHeader" datasource="#application.siteDataSource#">
				update
					Orders
				set
					OrderStatus = #evaluate("form.OrderStatus_#anOrderID#")#,
					<cfif evaluate("form.ShippedDate_#anOrderID#") is not "">
						ShippedDate = #createodbcdate(evaluate("form.ShippedDate_#anOrderID#"))#,
					</cfif>
					Shipper =  <cf_queryparam value="#evaluate("form.Shipper_#anOrderID#")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					ShipperReference =  <cf_queryparam value="#evaluate("form.ShipperReference_#anOrderID#")#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				where
					orderID =  <cf_queryparam value="#anOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
		<!--- if the status is marked as completely shipped, need to go through the item records and mark each one --->	
		<cfif evaluate("form.OrderStatus_#anOrderID#") is status_Dispatched>
			<!--- 
			set the shippedquantity to the quantity requested and the shipped date to the date set at order header level 
			leaving any fulfilled lines as they were.
			--->
			<cfquery name="updateOrderItems" datasource="#application.siteDataSource#">
				update 
					orderItem
				set					
					QuantityShipped = Quantity,
					ShippedDate = #createodbcdate(evaluate("form.ShippedDate_#anOrderID#"))#
				where
					QuantityShipped <> Quantity
					and orderID =  <cf_queryparam value="#anOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
	
	</cfloop>
	
	
</cfif>


<!--- for column based ordering --->
<cfset AscDesc_1 = "asc">
<cfset AscDesc_0 = "desc">

<cfinclude template="qryDistributorOrderHeader.cfm">


<cfsetting enablecfoutputonly="no">
<cfset statuswords = "">
<cfloop list="#form.selectedOrderStatuses#" index="astatus">
	<!--- CPS 28032001  use application variables StatusDescriptions --->
	<cfset statuswords = statuswords & application.StatusDescriptions[astatus] & "/">
<!--- 	<cfset statuswords = statuswords & listGetAt(OrderStatusList, astatus) & "/"> --->
</cfloop>
<cfset statuswords = left(statuswords, len(statuswords) -1)>


<CFSET Current = "distiDispatch.CFM">
<cfinclude template = "cCommerceTopHead.cfm">

<cf_displayBorder>

<h1><b><cfoutput>#htmleditformat(statuswords)# Orders for #htmleditformat(ThisDisti.SiteName)#</cfoutput></b></h1>
<!--- now lets start to display the stuff they actually want to see --->

<cfparam name="OrderIDSort" default="0">
<!---<cfparam name="OrderNumberSort" default="0">--->
<cfparam name="TheirOrderNumberSort" default="0">
<cfparam name="OrderDateSort" default="0">
<cfparam name="CreatedByNameSort" default="0">
<cfparam name="DelCountryNameSort" default="0">
<cfparam name="ShippedDateSort" default="0">
<cfparam name="OrderStatusSort" default="0">
<cfparam name="ShipperSort" default="0">
<cfparam name="ShipperReferenceSort" default="0">

<!--- for the asc/desc arrows --->
<cfset image_1 = "<img border=""0"" src=""../images/misc/asc.gif"">">
<cfset image_0 = "<img border=""0"" src=""../images/misc/desc.gif"">">

<!--- for the bg --->
<cfset color_1 = "class=""creambackground""">
<cfset color_0 = "">

<cf_translate>
<cfoutput>
<!--- header bit - includes ability to re-order by column --->

<form name="theForm" action="DistiDispatch.cfm" method="post">
<input type="hidden" name="frmAction" value="">
<table border="0" cellpadding="6" cellspacing="0">
<tr>
	<td class="label" colspan="6">
	<input type="checkbox" name="selectedOrderStatuses" value="#status_Confirmed#" <cfif form.selectedOrderStatuses contains "2">checked</cfif>> #phr_Order_ConfirmedOrders#
	&nbsp;&nbsp;&nbsp;
	<input type="checkbox" name="selectedOrderStatuses" value="#status_PartialDispatch#" <cfif form.selectedOrderStatuses contains "10">checked</cfif>> #phr_Order_PartiallyDispatchedOrders#
	&nbsp;&nbsp;&nbsp;
	<input type="checkbox" name="selectedOrderStatuses" value="#status_Dispatched#" <cfif form.selectedOrderStatuses contains "11">checked</cfif>> #phr_Order_DispatchedOrders#
	&nbsp;&nbsp;&nbsp;
	<a href="javascript: void document.theForm.submit()">#phr_Refresh#</a></td>
	&nbsp;&nbsp;&nbsp;
	<td colspan="2" class="label" align="right"><cfif qryDistributorOrderHeader.recordcount gt 0><a href="javascript: void updateMe(); void submitForm('#request.query_string#')">#phr_UpdateRecords#</a></cfif>&nbsp;</td>
</tr>
<cfif qryDistributorOrderHeader.recordcount gt 0>
	<tr>
		<td colspan="5"><font color="red"><b>#phr_Order_AlertUpdateRecords#</b></font></td>
	</tr>
	<tr valign="top">
		<!---<td class="label" width="13%"><a href="javascript: void submitForm('OrderIDSort=#int(not OrderIDSort)#')"><cfif isDefined("url.OrderIDSort")>#evaluate("image_#OrderIDSort#")#</cfif> #phr_Order_OrderID#</a></td>--->
		<td class="label" width="13%"><a href="javascript: void submitForm('TheirOrderNumberSort=#int(not TheirOrderNumberSort)#')"><cfif isDefined("url.TheirOrderNumberSort")>#evaluate("image_#htmleditformat(TheirOrderNumberSort)#")#</cfif> #phr_Order_PurchaseRef#</a></td>
		<td class="label"><a href="javascript: void submitForm('OrderDateSort=#int(not OrderDateSort)#')"><cfif isDefined("url.OrderDateSort")>#evaluate("image_#htmleditformat(OrderDateSort)#")#</cfif> #phr_Order_DateOrderPlaced#</a></td>
		<td class="label"><a href="javascript: void submitForm('CreatedByNameSort=#int(not CreatedByNameSort)#')"><cfif isDefined("url.CreatedByNameSort")>#evaluate("image_#htmleditformat(CreatedByNameSort)#")#</cfif> #phr_Order_OrderPlacedBy#</a></td>
		<!---<td class="label"><a href="javascript: void submitForm('DelCountryNameSort=#int(not DelCountryNameSort)#')"><cfif isDefined("url.DelCountryNameSort")>#evaluate("image_#DelCountryNameSort#")#</cfif> #phr_Order_DelCountryName#</a></td>--->
		<td class="label"><a href="javascript: void submitForm('OrderStatusSort=#int(not OrderStatusSort)#')"><cfif isDefined("url.OrderStatusSort")>#evaluate("image_#htmleditformat(OrderStatusSort)#")#</cfif> #phr_Order_OrderStatus#</a></td>
		<td class="label"><a href="javascript: void submitForm('ShippedDateSort=#int(not ShippedDateSort)#')"><cfif isDefined("url.ShippedDateSort")>#evaluate("image_#htmleditformat(ShippedDateSort)#")#</cfif> #phr_Order_ShippedDate#</a></td>
		<td class="label"><a href="javascript: void submitForm('ShipperSort=#int(not ShipperSort)#')"><cfif isDefined("url.ShipperSort")>#evaluate("image_#htmleditformat(ShipperSort)#")#</cfif> #phr_Order_Shipper#</a></td>
		<td class="label"><a href="javascript: void submitForm('ShipperReferenceSort=#int(not ShipperReferenceSort)#')"><cfif isDefined("url.ShipperReferenceSort")>#evaluate("image_#htmleditformat(ShipperReferenceSort)#")#</cfif> #phr_Order_ShipperReference#</a></td>
		<td class="label">&nbsp;</td>
	</tr>
<cfelse>
	<tr>
		<td class="label" colspan="8">
			<b>#phr_Order_NoRecordsFound#</b>
		</td>
	</tr>
</cfif>
</cfoutput>


<cfloop query="qryDistributorOrderHeader">
<cfoutput>
	<tr #evaluate("color_#int(qryDistributorOrderHeader.currentrow mod 2)#")#>
		<!---<td>
			&nbsp;&nbsp;&nbsp;#OrderID#
		</td>--->
		<td>
			<CF_INPUT type="hidden" name="OrderIdList" value="#OrderID#">
			<CF_INPUT type="hidden" name="lineUpdated_#OrderID#" value="0">		
			#htmleditformat(theirordernumber)#&nbsp;
		</td>
		<td>#dateFormat(OrderDate)#&nbsp;</td>
		<td>#htmleditformat(CreatedByName)#&nbsp;<BR>#htmleditformat(sitename)#</td>
		<!---<td>&nbsp;&nbsp;&nbsp;#DelCountryName#</td>--->
		<td>
			<select name="orderStatus_#OrderID#" onchange="updateMe(#orderID#)">
</cfoutput>
			<cfloop list="#DistiOrderStatuses#" index="astatus">
			<cfoutput>
				<!--- CPS 28032001  use application variables StatusDescriptions --->			
				<option value="#astatus#" <cfif qryDistributorOrderHeader.OrderStatus is astatus>selected</cfif>>#application.StatusDescriptions[aStatus]#
<!--- 				<option value="#astatus#" <cfif qryDistributorOrderHeader.OrderStatus is astatus>selected</cfif>>#listgetAt(orderStatusList, aStatus)# --->
			</cfoutput>
			</cfloop>
			</select>
<cfoutput>
		</td>
		<td>
			<!---<cfif ShippedDate is not "">
				&nbsp;&nbsp;&nbsp;
				#dateFormat(ShippedDate)#
			<cfelse>
				<input type="text" size="8" maxlength="11" name="ShippedDate_#OrderID#" onchange="updateMe(#OrderID#)"><a style="text-decoration: none; font-weight: bold; font-size: large;" href="javascript: void today('ShippedDate_#OrderID#'); updateMe(#OrderID#)">...</a>
			</cfif>--->
			<CF_INPUT type="text" size="8" maxlength="11" name="shippedDate_#OrderID#" value="#dateformat(ShippedDate,"dd-mmm-yyyy")#" onchange="alertDateChecked(this); updateMe(#OrderID#)">
			<!--- <a style="text-decoration: none; font-weight: bold; font-size: large;" href="javascript: void today('ShippedDate_#OrderID#'); updateMe(#OrderID#)">...</a> --->
		</td>
		<td><CF_INPUT type="text" name="Shipper_#OrderID#" size="10" maxlength="50" value="#Shipper#" onchange="updateMe(#OrderID#)"></td>		
		<td><CF_INPUT type="text" name="ShipperReference_#OrderID#" size="7" maxlength="50" value="#ShipperReference#" onchange="updateMe(#OrderID#)"></td>
		<td><a href="javascript:void popUpWindow('DisplayOrder.cfm?CurrentOrderID=#OrderID#&PromoID=#PromoID#&ShowDistiDiscount=yes')">#phr_Details#</a></td>
	</tr>
</cfoutput>
	<!--- if the order is partially dispatched, display the line items... --->
	<cfif orderStatus is status_PartialDispatch>
		<cfset ignoreGetOrderHeader = true>
		<cfset ignoreGetOrderValue = true>
		<cfset currentorderid = OrderID>
		<cfinclude template="qryOrderDetails.cfm">
		<cfif getOrderItems.recordcount gt 0>
		<tr>
			<td></td>
			<td colspan="7" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="2" width="100%">
					<cfoutput>
					<tr>
						<td class="label">SKU</td>
						<td class="label">#phr_description#</td>
						<td class="label">#phr_Order_QtyReq#</td>
						<td class="label">#phr_Order_QtyShipped#</td>
						<td class="label">#phr_Order_ShippedDate#</td>
					</tr>
					</cfoutput>
					<cfoutput query="getOrderItems">
					<CF_INPUT type="hidden" name="itemidList" value="#itemid#">
					<CF_INPUT type="hidden" name="lineItemUpdated_#itemID#" value="0">
					<tr>
						<td class="label">#htmleditformat(SKU)#</td>
						<td class="label">#htmleditformat(Description)#</td>
						<td class="label" align="center">#htmleditformat(Quantity)#</td>
						<td class="label" align="center"><CF_INPUT type="text" size="4" maxlength="5" name="QuantityShipped_#itemID#" value="#QuantityShipped#" onchange="lineItemUpdated_#itemID#.value = 1; today('ItemShippedDate_#ItemID#')"></td>
						<td class="label" align="center"><input type="text" size="8" maxlength="11" name="ItemShippedDate_#ItemID#" value="<cfif ShippedDate is not "">#dateformat(ShippedDate,"dd-mmm-yyyy")#</cfif>" onchange="alertDateChecked(this); lineItemUpdated_#htmleditformat(itemID)#.value = 1">
						<!---<a style="text-decoration: none; font-weight: bold; font-size: large;" href="javascript: void today('ItemShippedDate_#ItemID#'); void updateMe(#orderID#); void(document.theForm.lineItemUpdated_#itemID#.value = 1)">...</a>---></td>
					</tr>
					</cfoutput>
				</table>
			</td>
		</tr>
		</cfif>
	</cfif>
</cfloop>
<cfif qryDistributorOrderHeader.recordcount gt 0>
	<cfoutput>
	<tr>
		
		<td colspan="8" class="label" align="center"><br><a href="javascript: void updateMe(); void submitForm('#request.query_string#')">#phr_UpdateRecords#</a><br>&nbsp;</td></td>
	</tr>
	</cfoutput>
	</table>
	</form>
<cfelse>
	</table>
	</form>
</cfif>
</cf_translate>

</cf_DisplayBorder>



