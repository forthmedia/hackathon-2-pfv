<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:  WAB 030200  Uses variable Delivery Address to get properly formatted address
 --->
<!---BCG Promotion Mail--->
<CF_MAIL query="getorderitems" 
		TO="#createdByEmail#" 
		FROM="#supportEmail#" 
		CC="#copyAllEmailsToThisAddress#"
		SUBJECT="Order Confirmation: Order No. #currentorderid#">#phr_Order_Thankyouforyourorder#

#phr_Order_OrderNumber#: #currentorderid#
#phr_Order_NumberOfItems#: #getorderitems.recordcount#

#phr_Order_ItemsOrdered#
================================================================================<CFOUTPUT>
#phr_Order_ProductCode#: #SKU# 
#phr_Description#:  #Description# 
#phr_quantity#:     #Quantity#
</CFOUTPUT>================================================================================

#phr_Order_ShippingDetails#
================================================================================
#phr_Order_Thisorderwillbedeliveredto#:
#getorderheader.delcontact#
#getorderheader.delsitename#
#DeliveryAddress#
================================================================================
</CF_MAIL>

