<!--- �Relayware. All Rights Reserved 2014 --->
<!---<cfsetting showdebugoutput="No">--->
<!--- displayorder.cfm --->
<!--- used to display details of an order --->
<!--- requires currentorderid--->
<!--- optional frmDisplayMode can take values view (default) or edit --->

<!--- Mods
WAB 030200 Rejigged the address display to use the defined address format
WAB 130300 problem if product weight is null, put in a check for this
SWJ 26 Jan 2000 Added check to show order item questions if they exist.
CPS 29 Jan 2001 Check that the Screenid is numeric when checking to see if order item questions exist .
CPS 09 Feb 2001 Display the Distributor's discount for orders where the Discount Price is displayed.
CPS 13 Feb 2001 Ensure the Unit List Price is displayed for orders on promotions where the showpointsvalue is not set.
				Also calculate and display the Distributor's discounted price
CPS 23 Feb 2001 Ensure the correct organisationid is supplied to order item questions template
CPS 27 Feb 2001 Where the promotion is part of the 'In Focus' Promotion group do not display Shipping Details or Order Type
CPS 28032001 use new application variable to display status description
CPS 09 Apr 2001 Do not display Requires Purchase Number message for 'In Focus Promotions'
CPS 26 Apr 2001 Ensure that Distributor Discount Price is only display to internal users
CPS 04 May 2001 Display correct 'Please Take A Moment To Check' message dependant upon ShowChangeItemsInOrder boolean
WAB  2001-06-21   added enctype to the <FORM>
RJP  2005-04-13  removed voucher code from display
3-May-05	Alex C	Added showIllustrativePicing logic
08-May-05	SWJ		added showOrderNotes and showOrderRelatedFiles logic
17 Jun 05	AJC		Added phraseprefix for Order_PleaseTakeAMomentToCheckNoEdit
Enhancements still to do:
JC	2004-12-07	INTERNAL USER - payment method should be standardised to similar format as for EXTERNAL user
				ie: radio buttons and dynamically populated.

JC	2004-12-08		To be extended to accomodate Multi-Vouchers on a single product and
				multi-non-product-specific vouchers to be applied to order as a whole
NJH	2009/06/30	P-FNL069 - removed frmGlobalParameters as part of security project. Encrypted hidden fields and url variables to entityRelatedFilePopup as well.
WAB 2009/09/14 LID 2625 - mods to encryption of hidden fields so that leaves ones required by the JS
NYB 2010-12-15 LHID3976.  Added include of applicationordersini - seemed to be missing

2014-04-25 PPB Case 439307 fix to code which was disturbed by automated find+replace
2015-10-26		ACPK	PROD2015-142 Encode percentage signs in webhandle
2016-02-03	WAB Mass replace of displayValidValues using lists to use queries
--->
<CFSETTING ENABLECFOUTPUTONLY="no">

<CF_TRANSLATE>

<cfparam name="GoToEid" default="">
<CFPARAM name="frmdisplaymode" default="view">   <!--- can be display,edit --->
<CFPARAM name="msg" default="">
<!--- <CFPARAM name="frmGlobalParameters" default=""> NJH 2009/06/30 P-FNL069 --->
<cfparam name="currentcountry" default="">
<cfparam name="phraseprefix" default="">
<cfparam name="projectID" default="">
<cfparam name="validateSiteName" default=false>

<cfparam name="ShowDistiDiscount" default="no">

<!--- do some  rudimentary browser sniffing - don't worry about full versions - v4 checking is done later on --->
<cfparam name="cgi.http_user_agent" default="">
<cfif cgi.http_user_agent contains "MSIE">
	<!--- internet explorer --->
	<cfif cgi.http_user_agent contains "MSIE 4.0">
		<cfset browser= "IE4">
	<cfelseif cgi.http_user_agent contains "MSIE 5">
		<cfset browser = "IE5">
	</cfif>
<cfelse>
	<!--- navigator --->
	<cfset browser = "Netscape">
</cfif>

<!--- need to build in a check - a confirmed order should not be editable --->

<!--- get details of order --->
<CFINCLUDE Template="qryorderdetails.cfm">
<!--- NYB 2010-12-15 LHID3976.  Added, seemed to be missing: --->
<CFINCLUDE Template="applicationordersini.cfm">
<!--- returns queries getorderheader and getorderitems --->
<!--- status 12 = Awaiting Programme Manager Approval --->
 <CFIF getorderheader.orderstatus IS "12">
	<CFSET form.ProgrammeManagerView = 1>
</CFIF>

<!--- set up the show variables --->

<cfset ShowProductGroup = GetOrderHeader.ShowProductGroup>
<cfset ShowProductGroupHeader = GetOrderHeader.ShowProductGroupHeader>
<cfset ShowTermsConditions = GetOrderHeader.ShowTermsConditions>
<cfset ShowPreviousQuotesLink = GetOrderHeader.ShowPreviousQuotesLink>
<cfset ShowPreviousOrdersLink = GetOrderHeader.ShowPreviousOrdersLink>
<cfset ShowDeliveryCalculation = GetOrderHeader.ShowDeliveryCalculation>
<cfset ShowVATCalculation = GetOrderHeader.ShowVATCalculation>

<cfset ShowProductCode = GetOrderHeader.ShowProductCode>
<cfset ShowProductCodeAsAnchor = GetOrderHeader.ShowProductCodeAsAnchor>
<cfset ShowPictures = GetOrderHeader.ShowPictures>
<cfset ShowDescription = GetOrderHeader.ShowDescription>
<cfset ShowPointsValue = GetOrderHeader.ShowPointsValue>
<cfset ShowLineTotal = GetOrderHeader.ShowLineTotal>
<cfset ShowQuantity = GetOrderHeader.ShowQuantity>
<cfset ShowListPrice = GetOrderHeader.ShowListPrice>
<cfset ShowIllustrativePrice = GetOrderHeader.ShowIllustrativePrice>
<cfset ShowDiscountPrice = GetOrderHeader.ShowDiscountPrice>
<cfset ShowNoInStock = GetOrderHeader.ShowNoInStock>
<cfset ShowMaxQuantity = GetOrderHeader.ShowMaxQuantity>
<cfset ShowPreviouslyOrdered = GetOrderHeader.ShowPreviouslyOrdered>
<cfset ShowListTotal=GetOrderHeader.ShowListTotal>
<cfif ShowListTotal is "">
	<cfset ShowListTotal=0>
</cfif>


<cfset ShowPageTotal = GetOrderHeader.ShowPageTotal>
<cfset ShowDecimals = GetOrderHeader.ShowDecimals>
<cfset ShowProductGroupScreen = GetOrderHeader.ShowProductGroupScreen>

<cfset ShowPurchaseOrderRef = getOrderHeader.ShowPurchaseOrderRef>
<!--- CPS 2001-05-04 set variable ShowChangeItemsInOrder --->
<cfset ShowChangeItemsInOrder = getOrderHeader.ShowChangeItemsInOrder>
<cfset UsePoints = GetOrderHeader.UsePoints>

<cfset UseButtons = GetOrderHeader.UseButtons>

<cfset promoid = getOrderHeader.PromoID>

<!--- 8-May-05 SWJ  --->
<cfset ShowOrderNotes = GetOrderHeader.ShowOrderNotes>
<cfset ShowOrderRelatedFiles = GetOrderHeader.ShowOrderRelatedFiles>

<!--- get address format --->
<CFSET Countryid = #getOrderHeader.countryid#>
<CFSET screenid = 'standardaddressformat'>
<CFPARAM name="frmreadonlyfields" default="">   <!--- parameter can be passed eg: ='person_email','location_fax' --->
<CFPARAM name="frmForbiddenFields" default = "">
<CFPARAM name="frmreadonlywithhiddenfields" default="">
<CFPARAM name="frmMethod" default="view">


<!--- replaced with CFC call --->
<cfset screenAndDefinition = application.com.screens.getScreenAndDefinition(
						screenid = screenid,
						countryid = countryid,
						method = frmMethod,
						readonlyfields = frmReadOnlyFields,
						forbiddenFields = frmforbiddenFields ,
						readonlywithhiddenfields =frmreadonlywithhiddenfields
						)>
<cfset addressFormat = screenAndDefinition.Definition>



<!--- certain functions not shown to external users, so set up a variable  --->
<CFIF not request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    UserType is "External" --->
	<CFSET internalUser=false>
<CFELSE>
	<CFSET internalUser=true>
</cfif>


<!--- decide whether to show VAT reduction stuff --->
<CFIF getOrderHeader.allowVATReduction is not 0 and getOrderHeader.VATFormat is not "" and ShowVATCalculation>
	<CFSET showVATNumber = true>
<CFELSE>
	<CFSET showVATNumber = false>
</CFIF>


<!--- Get Information for distributor dropDown when required
if there turn out not to be any then we won't show the dropdown (by leaving variables.showDistributorList false)
--->

<CFSET showDistributorList = false>
<CFIF getOrderHeader.showDistributorList IS 1>
	<!--- getDistis for this Country --->

	<CFIF getOrderHeader.DistributorList is not "">
	<!--- ie there are some distributors defined  --->
		<CFQUERY NAME="Distis" datasource="#application.siteDataSource#">
		SELECT
			locationID,
			sitename 
		FROM
			flag as f,
			integerMultipleFlagData as fd ,
			location as l
		WHERE
				f.flagid = fd.flagid
			and f.flagtextid = 'distiCountries'
			and l.locationid = fd.entityid
			<!---and fd.data = #getOrderHeader.countryID#--->
			and fd.entityid  in ( <cf_queryparam value="#getOrderHeader.DistributorList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		order by
			sitename
		</CFQUERY>
	<CFELSE>
		<CFSET 	distis.recordCount = 0>
	</cfif>

	<!--- sent an alert to support if there is no distributor available --->
	<CFIF distis.recordCount is 0>
		<CFMAIL TO=#application.com.settings.getSetting("emailaddresses.HelpAlertMailbox")#
			FROM=#application.com.settings.getSetting("emailaddresses.AdminPostmaster")#
			SUBJECT="Orders System - no Distributor">
			There is no distributor set for
			country: #getOrderHeader.countryID#
			promotion: #getOrderHeader.promoid#
			</cfmail>
	<CFELSE>

		<CFSET showDistributorList = true>
	</CFIF>
</CFIF>
<cfoutput>
<!--- WAB this javascript required for checking whether checkboxes are selected--->
	<cf_includejavascriptonce template = "/javascript/checkObject.js">

<SCRIPT type="text/javascript">
<!--

<CFINCLUDE template="..\screen\jsDoTranslation.cfm">

 function secure(){
		var form = document.mainForm;
		if (form.PaymentMethod[form.PaymentMethod.selectedIndex].value == "CC"){
			securesubmit();
		}else{
			window.alert("phr_Order_JS_ChosenNoCreditCardPayment");
		}
}

function securesubmit()	{
		var form = document.secureForm;
		// alert(form.action);
		form.submit();
}


	function verifyForm() {
		var form = document.mainForm;
			msg=''

				// if there has been a voucher payment and there is still a balance to pay then need a message
				if (form.hiddenTotal.value != 0 && isItemInCheckboxGroupChecked(form.PaymentMethod,'VP')) {
					msg += 	'Please select an alternative payment method for the balance\n';
				}



				//check that a payment method has been selected
				<CFIF getOrderHeader.showPaymentOptions is 1>
					if (checkObject(form.PaymentMethod) == 0) {
						msg +=  'phr_Order_PleaseSelectAPaymentMethod\n'
					}
				</CFIF>



<!--- 	WAB 2005-01-11 replaced this with a more simple stuff above (using an existing function)

<!--- JC: JS can only validate Radio buttons where there is more than 1 in a group. As such, if only one payment type exists, radio button is pre-checked and validation step is skipped --->
			<CFIF IsDefined("paymentmethods") AND ListLen(paymentmethods) GT 1>
				// loop over all available payment method radio buttons and validate

				//check that a payment method has been selected
				<CFIF getOrderHeader.showPaymentOptions is 1>


 					for (i=0, n=form.PaymentMethod.length; i<n; i++) {
					   if (form.PaymentMethod[i].checked) {
					      var checkvalue = form.PaymentMethod[i].value;
					      break;
					   }
					}
					if (!(checkvalue)) {
						msg +=  'phr_Order_PleaseSelectAPaymentMethod\n'
					}

				</CFIF>
			</cfif>
--->
			//Check for mandatory telephone number
			<CFIF getOrderHeader.MandatoryContactPhone is 1>
				if ((form.contactPhone.value == 0)||(form.contactPhone.value == '')) {
					msg +=  'phr_Order_PleaseEnterContactPhone\n'
				}
			</CFIF>

			<CFIF internaluser is true AND getOrderHeader.PromotionGroupId NEQ 1>
				if (form.frmShippingMethod.selectedIndex == 0) {
					msg += 'phr_Order_JS_SelectShippingMethod\n'
				}

				<CFIF GetOrderHeader.PromoOrderType IS ''>
				if (form.frmOrderType.selectedIndex == 0) {
					msg += 'phr_Order_JS_SelectOrderType\n'
				}
				</cfif>

			</cfif>

			<CFIF showVATNumber is true>

				if  ( form.frmVATNumber.value != '' && !testVAT(form.frmVATNumber.value)  ){
					msg +=  'phr_Order_JS_InvalidVATNumber\n'

				}
				<cfif GetOrderHeader.VatRequired is true>
					if  ( form.frmVATNumber.value == '') {
						msg +=  'phr_Order_JS_InvalidVATNumber\n'

						}
				</cfif>
			</CFIF>

			if  ( form.delAddress1.value == '') {
				msg +=  'phr_Order_JS_EnterDeliveryAddress\n'

			}

			<CFIF validateSiteName>
				if (form.delSiteName.value == '') {
					msg +=  'phr_Order_JS_EnterDeliverySiteName\n'
				}
			</CFIF>
			<CFIF getOrderHeader.ZeroPrice IS 0>
				if  ( form.invAddress1.value == '') {
					msg +=  'phr_Order_JS_EnterInvioceAddress\n'

				}
			</CFIF>

			<CFIF GetOrderHeader.AskQuestions IS NOT 0>
				<!--- if we are asking Questions then there will be a function verifyInput generated--->
				if (verifyInput) {
					msg = msg + verifyInput ()

				}
			</CFIF>

				// do not display this message for 'In Focus Promotions' as the Purchase Order Number is automatically generated
 			<CFIF GetOrderHeader.requiresPurchaseRef IS NOT 0 AND GetOrderHeader.PromotionGroupId NEQ 1>
				<!--- if we are asking Questions then there will be a function verifyInput generated--->
				if  ( form.theirordernumber.value == '') {
					msg +=  'phr_Order_PleaseEnterAPurchaseOrderNumber\n'

				}
			</CFIF>

			<CFIF variables.showDistributorList >
				if  (form.frmDistiLocationID.selectedIndex == 0) {
					msg +=  'phr_Order_PleaseSelectADistributor\n'

				}


			</CFIF>

			<cfif ShowTermsConditions>
			if (form.acceptTermsConditions != null)
			{
				if (form.acceptTermsConditions[1].checked || (!form.acceptTermsConditions[0].checked && !form.acceptTermsConditions[1].checked))
				{
					msg += "phr_Order_MustAcceptTermsConditions\n"
				}
			}
			</cfif>

			// check to see if the order is for an In Focus Promotion and whether questions for the product are involved.
			// If so verify the input for these questions
			<CFIF GetOrderHeader.PromotionGroupId EQ 1 AND IsNumeric(getOrderItems.Screenid) AND getOrderItems.Screenid GT 0>

			msg += verifyInput ();

			</CFIF>

			if (msg != '') {
				alert (msg)
				return false;
			}else {
				//Don't seem to be any errors on the form
				return true;
			}


	}


	<CFIF showVATNumber is true>

	function testVAT(Number)  {

		<!--- strip out blank space and non character/numeric --->
		Number = Number.replace(/[^0-9A-Za-z]/,'')

		<!--- regular expression for testing VAT Number --->
		VATFormatRe = /<CFOUTPUT>#getOrderHeader.VATFormat#</cfoutput>/

		<!---  test match with regular expression and return--->
		return VATFormatRe.test(Number)


	}
	</CFIF>

	function submitForm(task,nextPage) {
		var form = document.mainForm;
		<CFIF isdefined("PAYMENTMETHOD") and getOrderHeader.showPaymentOptions IS 1 and frmdisplaymode IS "view">
			var payMethod = <CFOUTPUT>"#PAYMENTMETHOD#"</CFOUTPUT>
			if (nextPage !=="editorder") {
				if (payMethod == "CC") {
					form.action="../orders/worldpay.cfm";
				}
				if (payMethod == "PP") {
					form.action="../orders/points.cfm";
				}
			}
		</CFIF>

			form.frmTask.value=task
			form.frmNextPage.value=nextPage
			form.submit();

	}

// JC: 071204 - Added
// original code by Bill Trefzger 1996-12-12

function openWin( windowURL,  windowName, windowFeatures ) {

	return window.open( windowURL, windowName, windowFeatures ) ;
}

 function goOption(frmAction, myform, orderid) // Voucher , VoucherForm, current orderid
{
		var form = eval('document.'+ myform);
		VoucherWindow = openWin( 'about:blank' ,'VoucherWindow', 'left=' + parseInt(screen.availWidth * 0.4)  + ', width=' + parseInt(screen.availWidth * 0.4)  + ', top=' + parseInt(screen.availHeight * 0.3)  + ', height=' + parseInt(screen.availHeight * 0.3) + ',	toolbar=0,  location=0,	directories=0, status=0, menuBar=0,	scrollBars=1, resizable=1');
		form.frmAction.value = frmAction;
		form.orderid.value = orderid;
		form.target='VoucherWindow';
		form.action='/orderEnterVoucherCode.cfm';			/* 2014-04-25 PPB Case 439307 */
		form.submit();
		VoucherWindow.focus();
}

function voucherUpdate() {
	submitForm('UpdateHeader','editorder')
}

//-->
</SCRIPT>
<!--- <link href="#request.currentsite. stylesheet#" rel="stylesheet" media="screen,print" type="text/css"> --->
</cfoutput>

<CFOUTPUT>
<form name="voucherForm" action="" method="post">
	<input type="hidden" name="frmAction">
	<input type="hidden" name="orderid">
	<CF_INPUT type="hidden" name="callerQueryString" value="#request.query_string#">
</form>

<cfif GoToEid is "">
	<cfset mainFormURL="../orders/displayordertask.cfm">
<cfelse>
	<cfset mainFormURL="et.cfm?eid=#GoToEid#">
</cfif>
<FORM ACTION="#mainFormURL#" NAME="mainForm" METHOD="POST" enctype="multipart/form-data" class="order_display_form">

<!--- WAB 2009/09/14 LID 2625, all the fields required by JS were being removed by the encryption process, have now filtered out the ones that are required
	excludeVariableNamesRegExp  - these are hidden fields which are needed on the form and might be changed by the JS so can't be removed
	dontRemoveRegExp	- these are fields which can be encrypted, but the original value needs to remain on the form as well
--->
<cf_encryptHiddenFields excludeVariableNamesRegExp="frmNextPage|frmtask" dontRemoveRegExp="hiddenTotal|PaymentMethod" debug=true>

	<CF_INPUT type="hidden" name="phraseprefix" value="#phraseprefix#">  <!--- NJH 2007/01/18 --->

<cfif GoToEid is not "">
	<CF_INPUT type="hidden" name="eid" value="#GoToEid#">
	<input type="hidden" name="frmaction" value="DisplayOrder">
</cfif>

<!--- auto redirect page if all products on order have been linked to vouchercode --->
<cfquery name="qCheckVouchers" datasource="#application.siteDataSource#">
	select itemid
	from orderitem
	where orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >
	and vouchercode = ''
</cfquery>

<cfif #qCheckVouchers.recordcount# EQ 0>
	<SCRIPT type="text/javascript">
		// removed for dev
		// document.mainForm.submit();
	</script>
</cfif>
		<CFIF frmDisplayMode is "view"><CFSET tmpTask = ""><CFELSE><CFSET tmpTask = "updateheader"></cfif>
		<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#tmpTask#">
		<CF_INPUT TYPE="HIDDEN" NAME="projectID" VALUE="#projectID#">
		<CF_INPUT TYPE="HIDDEN" NAME="currentorderid" VALUE="#currentorderid#">
		<CF_INPUT TYPE="HIDDEN" NAME="orderlastupdated" VALUE="#getorderheader.lastupdated#">
		<INPUT TYPE="HIDDEN" NAME="frmNextPage" VALUE="../orders/vieworder.cfm">
		<!--- <INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="#frmGlobalParameters#"> NJH 2009/06/30 P-FNL069 --->
		<cfif isDefined("form.ThisProductGroup")>
		<CF_INPUT type="hidden" name="ThisProductGroup" value="#form.ThisProductGroup#">
		</cfif>
		<cfif isDefined("eid")>
		<CF_INPUT type="hidden" name="eid" value="#eid#">
		</cfif>

<!---Set variable for indicating 0 price Promotions--->
	<CFSET ZeroPrice = getOrderHeader.ZeroPrice>

<!--- XXXX --->
<CFIF msg IS NOT "">
	<p><FONT COLOR="Red">#htmleditformat(msg)#</FONT></p>
</cfif>
<!--- XXXX --->

<div class="order_details">
	<cfif frmdisplaymode IS "edit">
		<CFIF isDefined("message")>
			<!--- CPS 2001-05-04 - display the relevant message dependant upon ShowChangeItemsInOrder --->
			<p align="justify"><B><FONT COLOR="red">#application.com.security.sanitiseHTML(message)#</FONT></B></p>
		</CFIF>

			<!--- CPS 2001-05-04 - display the relevant message dependant upon ShowChangeItemsInOrder --->
			<!--- 2005/06/17 AJC START:P_SNY011_VIP phraseprefix added --->
			<p align="justify"><B><FONT COLOR="red"><CFIF ShowChangeItemsInOrder>phr_Order_PleaseTakeAMomentToCheck<CFELSE>phr_#htmleditformat(phraseprefix)#Order_PleaseTakeAMomentToCheckNoEdit</CFIF></FONT></B></p>
			<!--- 2005/06/17 AJC END:P_SNY011_VIP phraseprefix added --->

	</cfif>
	<div class="order_top_summary">
		<ul class="list-inline">
			<li><b><cfoutput>phr_#htmleditformat(phraseprefix)#Order:</cfoutput></b> #htmleditformat(getorderheader.OrderNoPrefix)##htmleditformat(getorderheader.orderid)#</li>
			<li><b>phr_Order_PlacedBy</b>
				<CFIF getOrderHeader.Createdby IS application.WebUserID>
					<!--- GCC If company name and person name are the same only display one --->
					#htmleditformat(getorderheader.firstname)# #htmleditformat(getorderheader.lastname)#<cfif not getorderheader.sitename eq getorderheader.firstname & " " & getorderheader.lastname>, #htmleditformat(getorderheader.sitename)#</cfif>
				<CFELSE>
					#htmleditformat(getOrderHeader.CreatedByName)#
				</cfif>
				</li>
			<li><b>phr_DateCreated</b> #dateformat(getorderheader.created)#</li>
			<li><b>phr_Order_OrderStatus</b> phr_status_#replace(application.StatusDescriptions[getorderheader.orderstatus],' ','')#</li>
		</ul>
	</div>
<table>
<cfif ShowPurchaseOrderRef>
	<TR>
	<TD>
		<cfif ShowDistiDiscount>
			<b>phr_#htmleditformat(phraseprefix)#Order_ResellerPurchaseRef</b>
		<cfelse>
			<b>phr_#htmleditformat(phraseprefix)#Order_YourPurchaseRef</b>
		</cfif>
	</TD>
	<TD>
	<cfset TheirOrderNumber = getOrderHeader.TheirOrderNumber>
	<cfif getOrderHeader.RequiresPurchaseRef and getOrderHeader.TheirOrderNumber is "Not Specified">
		<cfset TheirOrderNumber = "">
	</cfif>
<CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="theirordernumber" VALUE="#variables.TheirOrderNumber#" SIZE="20" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.theirordernumber)#</cfif></TD>
	<TD>
	</TD>
</TR>
</cfif>


<!--- Payment and Shipping Methods --->
<!--- old style for external users, new style for internal users --->

<CFIF internalUser is false>

<CFIF getOrderHeader.showPaymentOptions IS 1 and getOrderHeader.OrderValue gt 0>
	<TR>
		<TD>
			phr_Order_PaymentMethod
		</TD>
		<TD colspan="2">
			<CFIF frmdisplaymode IS "edit">

				<CFloop list="#paymentmethods#" index="method">
					<input type="radio" name="PaymentMethod" value="#method#"<CFIF method IS getorderheader.paymentmethod> checked</cfif>
					<!--- Voucher Payments: display popup window for user to enter and validate a Voucher Code --->
					 <cfif #method# EQ "VP">onClick="javascript:goOption('Voucher', 'voucherForm','#htmleditformat(getorderheader.orderid)#');"</cfif>
					> phr_Order_payment_#htmleditformat(method)#<br>
				</cfloop>

			<!---	JC: Removed
				<SELECT NAME="PaymentMethod" >
					<OPTION VALUE="" <CFIF getorderheader.paymentmethod IS "">Selected</cfif> >  phr_Order_Choosepaymentmethod
					<CFloop list="#paymentmethods#" index="method">
							<CFIF method IS "PP" and #request.relayCurrentUser.personid# EQ 2>
								<OPTION VALUE="#method#" <CFIF method IS getorderheader.paymentmethod>Selected</cfif> > phr_Order_payment_#method#
							<CFELSE>
								<OPTION VALUE="#method#" <CFIF method IS getorderheader.paymentmethod>Selected</cfif> > phr_Order_payment_#method#
							</CFIF>
					</CFLOOP>
				</SELECT>
			--->
			<CFELSE>
				phr_Order_payment_#htmleditformat(getorderheader.paymentmethod)#
			</cfif>
		</TD>
	</TR>

	<CFELSE>
		<INPUT TYPE="hidden" NAME="PaymentMethod" VALUE="NC">
	</cfif>

	<CF_INPUT TYPE="HIDDEN" NAME="frmOrderType" VALUE="#getorderheader.ordertype#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmShippingMethod" VALUE="#getOrderHeader.ShippingMethod#">

<CFELSE>	<!--- internal User --->
	<!--- Do not display for orders associated with 'In Focus' Promotions --->
	<CFIF getOrderHeader.PromotionGroupId EQ 1>

		<CF_INPUT TYPE="HIDDEN" NAME="frmOrderType" VALUE="#getorderheader.ordertype#">
		<CF_INPUT TYPE="hidden" NAME="frmShippingMethod" VALUE="#getOrderHeader.ShippingMethod#">

	<CFELSE>
		<TR>
			<TD>
			<b>Shipping Method: </b> <BR>
			<B>Weight</B> #decimalformat(getOrderValue.TotalWeight)#
			</TD>
			<TD colspan="2">
			<CFIF frmdisplaymode IS "edit">

				<cfquery name="qryShippingMethod" datasource="#application.siteDataSource#">
						SELECT *
						FROM 	shippingCost AS s1, ShippingMethod, promotion
						WHERE 	shippingMethod.shippingMethodID = s1.shippingMethodid
						and		promotion.shippingmatrixid = s1.shippingmatrixid
						and		promotion.promoid =  <cf_queryparam value="#getOrderHeader.PromoID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and		(countryid =  <cf_queryparam value="#getOrderHeader.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  or countryid in (select countrygroupid from countrygroup where countrymemberid =  <cf_queryparam value="#getOrderHeader.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or countryid = 0)
						and 	maxweight =    (SELECT Min(s2.MaxWeight)
						        				  FROM 	ShippingCost as s2
					    					      WHERE s2.ShippingMethodID=s1.ShippingMethodID
													and s2.shippingmatrixid = s1.shippingmatrixid
											        AND s2.CountryID=s1.countryid
													AND s2.MaxWeight >  <cf_queryparam value="#iif(getOrderValue.TotalWeight is "",DE("0"),DE(getOrderValue.TotalWeight))#" CFSQLTYPE="CF_SQL_float" >  );
				</cfquery>

				<select name="frmShippingMethod">
					<option value="" <CFIF getOrderHeader.ShippingMethod is "">Selected</cfif>>Select a Shipping Method</option>
					<cfloop query="qryShippingMethod">
						<option value="#ShippingMethodID#" <CFIF getOrderHeader.ShippingMethod is ShippingMethodID>Selected</cfif>>#htmleditformat(qryShippingMethod.Description)# (#htmleditformat(qryShippingMethod.ShippingMethodID)#) (#htmleditformat(qryShippingMethod.Cost)#)</option>
					</cfloop>
				</select>
			<cfelse>
				<cfquery name="qryShippingMethod" datasource="#application.siteDataSource#">
				SELECT *
				FROM ShippingMethod
				where ShippingMethodID =  <cf_queryparam value="#getorderheader.ShippingMethod#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				#HTMLEditFormat(qryShippingMethod.Description)#
			</cfif>
			</TD>
		</TR>



		<CFIF GetOrderHeader.PromoOrderType IS ''>
			<TR>
				<TD>
					<b>Order Type:  </b>
				</TD>
				<TD colspan="2">
					<CFIF frmdisplaymode IS "edit">
						<select name="frmOrderType">
							<option value="" <cfif getorderheader.ordertype IS "">selected</cfif> >Select an Order Type
							<option value="P" <cfif getorderheader.ordertype IS "P">selected</cfif> >P - Product
							<option value="I" <cfif getorderheader.ordertype IS "I">selected</cfif> >I - Information
						</select>
					<cfelse>
						#HTMLEditFormat(getorderheader.OrderType)#
					</cfif>
				</TD>



			</tr>
		<CFELSE>
			<CF_INPUT TYPE="hidden" NAME="frmOrderType" VALUE="#GetOrderHeader.PromoOrderType#">
		</cfif>
	</CFIF>



	<CFIF getOrderHeader.showPaymentOptions IS 1>
	<TR>
		<TD>
		<b>phr_Order_PaymentMethod:</b>
		</TD>
		<TD colspan="2">


			<CFIF frmdisplaymode IS "edit">
				<SELECT NAME="PaymentMethod" >  <!--- onChange="secure();" --->
				<CFIF #Total# NEQ 0>
					<OPTION VALUE="" <CFIF getorderheader.paymentmethod IS "">Selected</cfif>><!--- Choose payment method ---> phr_Order_Choosepaymentmethod
					<option value="CK" <CFIF getorderheader.paymentmethod IS "CK">Selected</cfif> >Cheque
					<option value="CC" <CFIF getorderheader.paymentmethod IS "CC">Selected</cfif> >CreditCard
					<option value="WT" <CFIF getorderheader.paymentmethod IS "WT">Selected</cfif> >Wire Transfer
					<option value="NA" <CFIF getorderheader.paymentmethod IS "NA">Selected</cfif> >Not Available
						<!--- 	<option value="NC" <CFIF getorderheader.paymentmethod IS "NC">Selected</cfif> >No Charge --->
					<option value="N3" <CFIF getorderheader.paymentmethod IS "N3">Selected</cfif> >30Day Credit
				<CFELSE>
					<OPTION VALUE="" <CFIF getorderheader.paymentmethod IS "">Selected</cfif>><!--- Choose payment method ---> phr_Order_Choosepaymentmethod
					<option value="NC">No Charge
				</cfif>
				</SELECT>

			<CFelse>

				#HTMLEditFormat(getorderheader.paymentmethod)#

			</cfif>

		</TD>
	</TR>

	<CFELSE>
	<INPUT TYPE="hidden" NAME="PaymentMethod" VALUE="NC">
	</CFIF>

</CFIF>


<!--- Display a distributor dropDown when required --->
<CFIF variables.showDistributorList>

		<TR>
			<TD>
			<b>phr_#htmleditformat(phraseprefix)#Order_ChooseADistributor: </b>
			</TD>
			<TD colspan="2">

			<cf_displayValidValues
				formFieldName = "frmDistiLocationID"
				validValues = "#distis#"
				dataValueColumn = "locationid"
				displayValueColumn = "sitename"
				currentValue = "#getOrderHeader.distiLocationID#"
				nulltext = "Select a Distributor"
				method = "#frmdisplaymode#">
			</TD>
		</TR>


</CFIF>
</table>


<!--- Delivery Details --->
<p class="del_header"><b>phr_#htmleditformat(phraseprefix)#Order_DeliveryDetails</b></p></td>
<!--- Invoice Details --->
<CFIF ZeroPrice IS NOT 1><p class="inv_header"><b>phr_Order_InvoiceDetails#htmleditformat(promoid)#</b></p></CFIF>
<div class="form-group row">
	<div class="col-xs-12 col-sm-3 control-label">
		<!--- Contact Name --->phr_ContactName
	</div>
	<div class="col-xs-12 col-sm-6">
		<cfif frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="delContact" VALUE="#getorderheader.delContact#" SIZE="20" MAXLENGTH="50"><cfelse>#HTMLEditFormat(getorderheader.delContact)#</cfif>
	</div>
	<div class="col-xs-12 col-sm-3">
		<cfif ZeroPrice IS NOT 1><CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="invContact" VALUE="#getorderheader.invcontact#" SIZE="20" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.invcontact)#</cfif><CFELSE><CF_INPUT TYPE="hidden" NAME="invContact" VALUE="#getorderheader.invcontact#"></CFIF>
	</div>
</div>

<div class="form-group row">
	<div class="col-xs-12 col-sm-3 control-label">
		<cfoutput>phr_#htmleditformat(phraseprefix)#Order_SiteName</b></cfoutput>
	</div>
	<div class="col-xs-12 col-sm-6">
		<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="delSiteName" VALUE="#getorderheader.delSitename#" SIZE="20" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.delSitename)#</cfif>
	</div>
	<div class="col-xs-12 col-sm-3">
		<CFIF ZeroPrice IS NOT 1><CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="invSiteName" VALUE="#getorderheader.invSitename#" SIZE="20" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.invsitename)#</cfif><CFELSE><CF_INPUT TYPE="hidden" NAME="invSiteName" VALUE="#getorderheader.invSitename#"></CFIF>
	</div>
</div>


 	<CFLOOP query="addressFormat">

	<CFIF listfindNoCase(addressFieldsUsed,fieldTextId) IS NOT 0>

		<div class="form-group row">
			<div class="col-xs-12 col-sm-3 control-label">
				<!---	DAM 4 Sept 2002
						Not all field labels in address formats should be translated. There is a column
						called "translateLabel" which tells us whether to do so or not.

						See accompanying note above when address format phrase list is constructed
				--->
					<CFIF #trim(fieldLabel)# is not "">
						<CFIF translatelabel EQ 1>
							phr_#htmleditformat(fieldLabel)#
						<CFELSE>
							#htmleditformat(fieldLabel)#
						</CFIF>
					</cfif>
			</div>
			<div class="col-xs-12 col-sm-6">
			<cfif currentcountry is not "">
				<cfset delcountryID=currentcountry>
			<cfelse>
				<cfset delcountryID=getorderheader.delCountryID>
			</cfif>
				<CFIF fieldTextID is "Countryid">
					<CFSET thisLineData = delCountryID>
					<CFSET thisLineFormField = "delCountryID">
					<CFSET thisLineMethod = "ViewWithHidden">
					<CFINCLUDE template="..\screen\showcountrylist.cfm">
				<CFELSE>
					<cfif currentcountry is not "">
						<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="del#fieldTextId#" VALUE="#evaluate('getorderheader.del#fieldTextID#')#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("getorderheader.del#htmleditformat(fieldTextID)#")#</cfif>
					<cfelse>
						<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="del#fieldTextId#" VALUE="#evaluate('getorderheader.del#fieldTextID#')#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("getorderheader.del#htmleditformat(fieldTextID)#")#</cfif>
					</cfif>
				</cfif>
			</div>

			<div class="col-xs-12 col-sm-3">
				<CFIF ZeroPrice IS NOT 1>
					<CFIF fieldTextID is "Countryid">
						<CFSET thisLineData = getorderheader.INVCountryID>
						<CFSET thisLineFormField = "INVCountryID">
						<CFSET thisLineMethod = frmdisplaymode>
						<CFINCLUDE template="..\screen\showcountrylist.cfm">
					<CFELSE>
						<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="inv#fieldTextID#" VALUE="#evaluate('getorderheader.inv#fieldTextID#')#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("getorderheader.inv#htmleditformat(fieldTextID)#")#</cfif>
					</CFIF>
				</CFIF>
			</div>
	  	</div>

	</cfif>
	</cfloop>


	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			phr_ContactPhone</b>
		</div>
		<div class="col-xs-12 col-sm-6">
			<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="contactPhone" VALUE="#getorderheader.contactphone#" SIZE="20" MAXLENGTH="20"><CFelse>#HTMLEditFormat(getorderheader.contactphone)#</cfif>
		</div>
		<div class="col-xs-12 col-sm-3"></div>
	</div>

	<div class="form-group row">
			<div class="col-xs-12 col-sm-3 control-label">
				phr_ContactFax
		</div>
		<div class="col-xs-12 col-sm-6">
		<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="contactFax" VALUE="#getorderheader.contactfax#" SIZE="20" MAXLENGTH="20"><CFelse>#HTMLEditFormat(getorderheader.contactfax)#</cfif></TD>
		</div>
		<div class="col-xs-12 col-sm-3"></div>
	</div>

	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			phr_ContactEmail
		</div>
		<div class="col-xs-12 col-sm-6">
		<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="contactemail" VALUE="#getorderheader.contactemail#" SIZE="20" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.contactemail)#</cfif></TD>
		</div>
		<div class="col-xs-12 col-sm-3"></div>
	</div>


<CFIF showVATNumber is true>
	<cfif getorderheader.VATNumber eq "">
		<cfset getorderheader.VATNumber = request.relaycurrentuser.organisation.VATnumber>
	</cfif>
	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			phr_Order_VATNumber *
		</div>
		<div class="col-xs-12 col-sm-6">
		<CFIF frmdisplaymode IS "edit"><CF_INPUT class="form-control" TYPE="Text" NAME="frmVATNumber" VALUE="#getorderheader.VATNumber#" SIZE="20" MAXLENGTH="20"><CFelse>#HTMLEditFormat(getorderheader.VATNumber)#</cfif></TD>
		</div>
		<div class="col-xs-12 col-sm-3"></div>
	</div>
<CFELSEIF frmdisplaymode IS "edit">
	<CF_INPUT TYPE="Hidden" NAME="frmVATNumber" VALUE="#getorderheader.VATNumber#" >

</CFIF>


	<p><cfif ShowDistiDiscount><b>phr_#htmleditformat(phraseprefix)#Order_Reseller </cfif><strong>phr_#htmleditformat(phraseprefix)#Order_OrderItemsSummary</strong></b></p>

		<CFSET Total = 0>
		<CFSET listTotal = 0>

		<cfif isdefined("request.showshippingdata")>
			<cfif request.showshippingdata EQ 1>
				<ul class="list-inline">
				<cfif getorderheader.shipper is not "">
					<li><b>Order shipped by:</b> #htmleditformat(getorderheader.shipper)#</li>
				</cfif>
				<cfif getorderheader.shipperReference is not "">
					<li><b>Order shipping Reference:</b> >#htmleditformat(getorderheader.shipperReference)#</li>
				</cfif>
				</ul>
			</cfif>
		</cfif>
		<cfoutput>

		<table  data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder order_details_summary">
		<thead>
			<TR valign="top" class="order_summary_headers">
			<Th>phr_SKU</Th>
			<Th>phr_Description</Th>
			<Th>phr_#htmleditformat(phraseprefix)#Order_Qty</Th>
			<CFIF ZeroPrice IS NOT 1 or UsePoints is 1>
				<cfif ShowPointsValue>
				<Th>phr_#htmleditformat(phraseprefix)#Order_RewardPoints</Th>
				<CFELSEIF ShowListPrice>
				<Th>phr_#htmleditformat(phraseprefix)#Order_ListPrice (#htmleditformat(getOrderItems.PriceISOCode)#)</Th>
				</cfif>
				<cfif ShowDistiDiscount>
					<Th>phr_#htmleditformat(phraseprefix)#Order_DistiDiscountLevel</Th>
					<Th>phr_#htmleditformat(phraseprefix)#Order_ResellerDiscountLevel</Th>
				<cfelse>
					<cfif ShowPointsValue>
						<Th>phr_#htmleditformat(phraseprefix)#Order_TotalPoints</Th>
					<cfelse>
						<Th>phr_#htmleditformat(phraseprefix)#Order_DiscountPrice (#htmleditformat(getOrderItems.PriceISOCode)#)</Th>
						<!--- If the order is against a promotion where the Discount Price is to be displayed then
							  show the Distributor's discount and calculate the Distibutor's discounted price --->
						<!--- CPS 2001-04-26 - only show Distributor's Discount & Price for Internal Users --->
						<CFIF ShowDiscountPrice AND internalUser>
							<Th>phr_#htmleditformat(phraseprefix)#Order_DistiDiscountLevel</Th>
							<Th>phr_#htmleditformat(phraseprefix)#Order_DistiDiscountPrice</Th>
						</CFIF>
						<CFIF ShowListTotal>
							<Th>phr_#htmleditformat(phraseprefix)#Order_ListTotal</Th>
						</cfif>
						<CFIF ShowIllustrativePrice>
						<cfscript>
							toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
						</cfscript>
					<Th>phr_#htmleditformat(phraseprefix)#Order_IllustrativeTotal * (#htmleditformat(toCurrency.countrycurrency)#)</Th>
						</cfif>
						<Th>phr_#htmleditformat(phraseprefix)#Total (#htmleditformat(getOrderItems.PriceISOCode)#)</Th>
					</cfif>
				</cfif>

			</CFIF>
			</TR>
		</thead>
		</cfoutput>
		<!--- build structure of all products paid for by Voucher
		for reproduction below main order form - line 1035+ --->
		<cfset loop = 0>
		<cfset voucherPaid = ArrayNew(2)>
		<tbody>
		<CFLOOP query="getorderitems">
			<!--- if the order is being viewed by the distributor, need to display the discount levels as appropriate --->
			<TR valign="top" class="order_summary_values">
			<TD>#htmleditformat(SKU)# </TD>

			<TD>
				<cfif getOrderHeader.translateDescription>phr_description_product_#htmleditformat(productid)#<cfelse>#htmleditformat(Description)#</cfif>
			</TD>
			<TD>#htmleditformat(Quantity)#</TD>

			<CFIF ZeroPrice IS NOT 1 or UsePoints is 1>
			<!--- Unit List Price not shown correctly for OrderItems connected to promotions that do not have the
				  ShowPointsValue flag set --->
				<cfif ShowPointsValue>
 					<TD>#iif(ShowDecimals,DE("#decimalformat(UnitListPrice)#"), DE("#htmleditformat(Int(UnitDiscountPrice))#"))#</TD>
				<CFELSEIF ShowListPrice>
					<TD>#iif(ShowDecimals,DE("#decimalformat(UnitListPrice)#"), DE("#htmleditformat(Int(UnitListPrice))#"))#</TD>
<!--- 					<TD align="right">#iif(ShowDecimals,DE("#decimalformat(UnitDiscountPrice)#"), DE("#Int(UnitDiscountPrice)#"))#</TD> --->
				</cfif>
				<cfif ShowDistiDiscount>
					<cfset DistiDiscount = getOrderItems.DistiDiscount>
					<cfset ResellerDiscount = getOrderItems.ResellerDiscount>
					<cfif getOrderItems.DistiDiscount is "">
						<cfset DistiDiscount = 0>
					</cfif>
					<cfif getOrderItems.ResellerDiscount is "">
						<cfset DistiDiscount = 0>
					</cfif>
					<TD>#int(variables.DistiDiscount * 100)#%</TD>
					<TD>#int(variables.ResellerDiscount * 100)#%</TD>
				<cfelse>
					<cfif ShowPointsValue>
						<TD>#iif(ShowDecimals,DE("#decimalformat(TotalDiscountPrice)#"), DE("#htmleditformat(Int(TotalDiscountPrice))#"))# </TD>
					<cfelse>
						<TD>#iif(ShowDecimals,DE("#decimalformat(UnitDiscountPrice)#"), DE("#htmleditformat(Int(UnitDiscountPrice))#"))# </TD>
						<!--- If the order is against a promotion where the Discount Price is to be displayed then
							  show the Distributor's discount and calculate the Distibutor's discounted price --->
						<!--- CPS 2001-04-26 - only show Distributor's Discount Price for Internal Users --->
						<CFIF ShowDiscountPrice AND internalUser>
							<CFSET DistiDiscount = getOrderItems.DistiDiscount>
							<CFIF getOrderItems.DistiDiscount is "">
								<CFSET DistiDiscount = 0>
							</CFIF>
							<TD>#int(variables.DistiDiscount * 100)#%</TD>
							<TD>#iif(ShowDecimals,DE("#decimalformat(UnitListPrice-(UnitListPrice*variables.DistiDiscount))#"), DE("#Int(UnitListPrice-(UnitListPrice*variables.DistiDiscount))#"))#</TD>
						</CFIF>
						<CFIF ShowListTotal>
							<TD>#iif(ShowDecimals,DE("#decimalformat(UnitListPrice*Quantity)#"), DE("#Int(UnitListPrice*Quantity)#"))#</TD>
						</CFIF>
						<CFIF ShowIllustrativePrice>
							<cfscript>
								res = application.com.currency.convert(fromValue=#TotalDiscountPrice#, fromCurrency="#getOrderItems.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
							</cfscript>
							<TD>#iif(ShowDecimals,DE("#decimalformat(res.tovalue)#"), DE("#htmleditformat(Int(res.tovalue))#"))#</TD>
						</cfif>
						<TD>#iif(ShowDecimals,DE("#decimalformat(TotalDiscountPrice)#"), DE("#htmleditformat(Int(TotalDiscountPrice))#"))# </TD>
					</cfif>
				</cfif>
				<!--- build array of vouchercode/values --->
				<cfif #vouchercode# NEQ "">
					<cfset loop = loop + 1>
					<cfset voucherPaid[loop][1] = "#vouchercode#">
					<cfset voucherPaid[loop][2] = "#iif(ShowDecimals,DE('#decimalformat(vouchervalue)#'), DE('#Int(vouchervalue)#'))#">
				</cfif>
			</CFIF>
			</TR>
			<cfif ShowDistiDiscount>
				<CFSET Total=Total+val(unitlistPrice)>
			<cfelse>
				<CFSET Total=Total+val(totalDiscountPrice)>
			</cfif>

			<cfif ShowListTotal>
				<cfset listTotal=listTotal+val(UnitListPrice*Quantity)>
			</cfif>
			<!--- SWJ added this next bit to show the order item questions per order item
			This will call the aScreen custom tag which will get the data that is stored in --->
			<!--- CPS Ensure this check is only carried out if the screen id is numeric --->
 			<cfif IsNumeric(Screenid) AND Screenid GT 0>
<!---  			<cfif Screenid > 0> --->

				<!--- CPS The statement was selecting the organisation for user that is logged onto the system
					  who might not be in the same organisation as the person who created the order e.g.
					  the Account Manager. Amend the query to select the organisation for the person
					  that placed the order --->
				<cfquery name="organisation" datasource="#application.siteDataSource#">
					select o.*
					from organisation o inner join
						person p on o.organisationid = p.organisationid
						<!--- where p.personid = #request.relayCurrentUser.personid# --->
						where p.personid =  <cf_queryparam value="#getorderheader.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfquery name="orderItem" datasource="#application.siteDataSource#">
					select * , itemid as orderitemid
					from vOrderItems
					where ItemID =  <cf_queryparam value="#itemid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<TR valign="top" class="order_screen_item">
					<TD>&nbsp </TD>
					<TD colspan="4">
						<CF_aSCREEN>
							<CF_aScreenItem
								screenid="#screenid#"
								orderitem=#orderitem#
								organisation=#organisation#
								method="#frmdisplaymode#">
						</CF_aSCREEN>
					</TD>
				</TR>
			</cfif>
		</CFLOOP>
		</tbody>
		</table>
		<CFIF ZeroPrice IS NOT 1 or UsePoints is 1>
			<cfif ShowDeliveryCalculation or ShowVATCalculation>
				<!--- Sub Total --->
				<div class="order_list_total row">
					<div class="col-xs-6 col-sm-3 control-label"><p>phr_#htmleditformat(phraseprefix)#Order_SubTotal</p></div>
				<cfif ShowListTotal>
					<div class="col-xs-6 col-sm-3">#decimalformat(listTotal)#</div>
				</cfif>
				<cfif ShowIllustrativePrice>
					<cfscript>
						res = application.com.currency.convert(fromValue=#Total#, fromCurrency="#getOrderItems.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
					</cfscript>
					<div class="col-xs-6 col-sm-3">#decimalformat(res.tovalue)#</div>
				</cfif>
				<div class="col-xs-6 col-sm-3">#iif(ShowDecimals,DE("#decimalformat(Total)#"), DE("#htmleditformat(Int(Total))#"))#</div>

				</div>
			</cfif>

			<cfif ShowDeliveryCalculation>
				<!--- Delivery Charge --->
				<div class="order_delivery_total row">
					<div class="col-xs-6 col-sm-3 control-label"><p>phr_Delivery</p></div>
					<div class="col-xs-6 col-sm-3">#iif(ShowDecimals,DE("#decimalformat(GetOrderHeader.ShippingCharge)#"), DE("#htmleditformat(Int(GetOrderHeader.ShippingCharge))#"))#</div>
				</div>
				<CFSET Total=Total+val(getOrderHeader.ShippingCharge)>
			</cfif>


			<cfif ShowVATCalculation>
				<!--- VAT--->
				<div class="order_show_vat row">
					<div class="col-xs-6 col-sm-3 control-label"><p>phr_VAT @ #decimalformat(getOrderHeader.TaxRate)#%</p></div>
					<cfif ShowListTotal>
						<div class="col-xs-6 col-sm-3">#decimalformat(listTotal*(getOrderHeader.TaxRate/100))#</div>
					<CFSET listTotal=listTotal+val(listTotal*(getOrderHeader.TaxRate/100))>
				</cfif>
				<cfif ShowIllustrativePrice>
					<cfscript>
						res = application.com.currency.convert(fromValue=#getOrderHeader.TaxOnOrder#, fromCurrency="#getOrderItems.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
					</cfscript>

					<div class="col-xs-6 col-sm-3">#decimalformat(res.tovalue)#</div>
				</cfif>
				<div class="col-xs-6 col-sm-3">#decimalformat(getOrderHeader.TaxOnOrder)#</div>
				</div>
				<CFSET Total=Total+val(getOrderHeader.TaxOnOrder)>
			</cfif>

			<!--- SWJ 8-May-05 Changed the logic to allow for illustrative pricing as well --->
			<!--- Total --->
			<div class="order_disti_disc row">
					<div class="col-xs-6 col-sm-3 control-label"><p><b>phr_Total</b></p></div>
			<cfif showListTotal>
				<div class="col-xs-6 col-sm-3">#iif(ShowDecimals,DE("#decimalformat(listTotal)#"), DE("#htmleditformat(Int(listTotal))#"))#</div>
			</cfif>
			<cfif ShowIllustrativePrice>
					<cfscript>
						res = application.com.currency.convert(fromValue=#Total#, fromCurrency="#getOrderItems.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
					</cfscript>

					<div class="col-xs-6 col-sm-3">#decimalformat(res.tovalue)#</div>
			</cfif>
			<div class="col-xs-6 col-sm-3">#iif(ShowDecimals,DE("#decimalformat(Total)#"), DE("#htmleditformat(Int(Total))#"))#</div>

			</div>
<!--- === Display paid by voucher panel === --->
				<!---
				Ignore Voucher paid if the only product linked to a voucher is a discount.
				This should never occur in real-world.
				 --->

			<cfif #IsDefined("voucherPaid")# AND #ArrayLen(voucherPaid)# NEQ 0 AND NOT (#ArrayLen(voucherPaid)# EQ 1 AND #voucherPaid[1][2]# LTE 0)>
				<!---
					loop over VoucherPaid array
					[~][1] = vouchercode
					[~][2] = value
					Group by vouchercode
				--->

				<cfset vc = "#voucherPaid[1][1]#">
				<cfset val = "#voucherPaid[1][2]#">
				<cfloop index='i' from='1' to='#ArrayLen(voucherPaid)#'>
					<cfset vouchercode = "#voucherPaid[i][1]#">
					<cfset vouchervalue = "#voucherPaid[i][2]#">
					<cfif #i# EQ #ArrayLen(voucherPaid)# OR(#vc# NEQ #vouchercode# AND NOT (#voucherPaid[i][1]# EQ #voucherPaid[1][1]#))>
						<cfif #i# NEQ 1>
							<cfset val = val + #vouchervalue#>
						</cfif>

							<div class="order_disti_disc row">
								<div class="col-xs-6 col-sm-3 control-label"><p>Paid For Using Voucher:</p></div>
								<div class="col-xs-6 col-sm-3">-#iif(ShowDecimals,DE("#decimalformat(val)#"), DE("#htmleditformat(Int(val))#"))#</div>
							</div>
					<cfelseif i NEQ 1>
						<cfset val = val + #vouchervalue#>
					</cfif>
				</cfloop>
				<div class="order_disti_disc row">
					<div class="col-xs-6 col-sm-3 control-label"><p>Total Due for Payment:</p></div>
					<cfset initial = #iif(ShowDecimals,DE("#decimalformat(Total)#"), DE("#Int(Total)#"))#>
					<cfset voucher = #iif(ShowDecimals,DE("#decimalformat(val)#"), DE("#Int(val)#"))#>
					<cfset totaldue = evaluate(initial - voucher)>
					<div class="col-xs-6 col-sm-3">#iif(ShowDecimals,DE("#decimalformat(totaldue)#"), DE("#Int(totaldue)#"))#</div>
				</div>
			</cfif>
		</CFIF>
		<input type="hidden" name="hiddenTotal" value="<cfif #IsDefined('totaldue')#>#totalDue#<cfelse>#total#</cfif>">


<cfif promoid is "evaluation2000" and cgi.script_name contains "orderitemstask.cfm">
	<p>phr_order_deliverycost</p>
</cfif>


<CFIF showVATNumber is true  and frmdisplaymode IS "edit">

	<p>* phr_pleaseEnterVatNumberInBoxAbove</p>
</cfif>

<CFIF GetOrderHeader.AskQuestions IS NOT 0>
<!--- this has been updated so that instead of having a list of flagids, it will now display a
a screen which has been defined in ScreenDefinition table.  The name of this screen is stored in the AskQuestions Column--->
	<CF_aSCreen>
		<CF_aScreenItem
			screenid="#GetOrderHeader.QuestionIDs#"
			ordersid = "#currentorderid#"
			method = "#frmdisplaymode#">
	</CF_aSCreen>
</CFIF>
<cfif #getorderheader.paymentmethod# EQ "VP" AND #ArrayLen(voucherPaid)# EQ 1 AND #voucherPaid[1][2]# LTE 0>
<p class="redbody"><b>phr_Order_VoucherDiscountProductError</b></p>
</cfif>
<CFIF ShowIllustrativePrice>
<p>* <cfoutput>phr_#htmleditformat(phraseprefix)#illustrativedescription</cfoutput></p>

</CFIF>
<!--- SWJ 8-May-05 added a notes section so that users can add notes to the bottom of the order
		this is controlled in the promotion table by showOrderNotes --->
<cfif showOrderNotes>
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<div class="form-group row">
		<cfif frmdisplaymode IS "edit">
			<div class="col-xs-6 col-sm-3 control-label"><label for="orderNotes">phr_#htmleditformat(phraseprefix)#Order_NotesRegardingThisOrder</label></div>
			<div class="col-xs-12 col-sm-9">
				<cf_relayTextField
					maxChars="2000"
					currentValue="#getOrderHeader.orderNotes#"
					thisFormName="mainForm"
					fieldName="orderNotes"
					class="form-control"
				helpText="phr_dateFieldHelpText"
				>
			</div>
		<cfelse>
			<div class="col-xs-12 col-sm-9 col-sm-offset-3">
				<p>#HTMLEditFormat(getOrderHeader.orderNotes)#</p>
			</div>
		</cfif>

	</div>
</cfif>


<cfif ShowOrderRelatedFiles>
	<div class="form-group row">
		<div class="col-xs-6 col-sm-3 control-label"><p>phr_#htmleditformat(phraseprefix)#Order_FilesRelatingToThisOrder</p></div>
		<div class="col-xs-12 col-sm-9">
			<!--- NJH 2009/07/17 P-FNL069 - show call base version of related file (was V2). base version now calls v2 --->
			<cf_relatedFile action="variables" entity="#getorderheader.orderid#" entitytype="Orders"><!--- <cfdump var="#relatedFileVariables.filelist#"> --->

			<cfif frmdisplaymode IS "view">
				<cfloop query="relatedFileVariables.filelist">
					<p><A HREF="javascript:void(window.open('#replace(webhandle,"%","%25","ALL")#','UploadFileViewer','width=400,height=400,status,resizable,toolbar=1'))">phr_#htmleditformat(phraseprefix)#Order_Download #htmleditformat(Description)# (#htmleditformat(filename)#)</A></p> <!--- 2015-10-26	ACPK	PROD2015-142 PROD2015-142 Encode percentage signs in webhandle --->
				</cfloop>
			</cfif>
			<p id="relatedFileCountMessage">There are #relatedFileVariables.filelist.recordCount# files associated with this record.</p>

				<cfif frmdisplaymode IS "edit">
				<!--- NJH 2009/07/17 P-FNL069 encrypt url variables --->
				<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityID=#getorderheader.orderid#&entityType=orders")>
				<p><a href="javascript:void(window.open('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','UploadFiles','width=600,height=400,status,resizable,toolbar=0'))" onMouseOver="window.status='';return true" onMouseOut="window.status='';return true">phr_#htmleditformat(phraseprefix)#Order_UploadFilesToThisOrder</a></p>
			</cfif>

		</div>
	</div>
</cfif>

<!--- Terms and Conditions section --->

<cfif ShowTermsConditions and frmdisplaymode IS "edit">
	<p>phr_#htmleditformat(phraseprefix)#Order_AcknowledgeTermsConditions <a href="javascript:void(window.open('TermsConditions.cfm?promoid=#promoID#','','width=600,height=300,status,scrollbars=yes,resizable<cfif browser is "IE4">,menubar</cfif>'))">phr_#htmleditformat(phraseprefix)#Order_ViewTermsConditions</a></p>
	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			phr_Order_AcceptTermsConditions
		</div>
		<div class="col-xs-12 col-sm-9">
			<div class="checkbox col-xs-12 col-sm-6 col-md-6">
				<label>phr_Yes</label><input class="radio" type="radio" value="1" name="acceptTermsConditions">
			</div>
			<div class="checkbox col-xs-12 col-sm-6 col-md-6">
				<label>phr_No</label><input class="radio" type="radio" value="0" name="acceptTermsConditions">
			</div>
		</div>
	</div>
</cfif>

</cf_encryptHiddenFields>
</FORM>

</CFOUTPUT>

<!--- </cf_DisplayBorder> --->
<!--- PKP : begin 2008-04-01 LEXMARK BUG Issue 46: Orders pages --->
<cfif isDefined("orderConfirmed") and orderConfirmed and getOrderHeader.ShowClosingMessage>
<div class="order_closing_message">
<cfoutput>
phr_Order_ClosingMessage#htmleditformat(promoid)#<br>
</cfoutput>
</div>

<cfelse>
			<cfoutput>
			<p><span class="required">phr_Order_catalogMessage</span></p>
			</cfoutput>

</cfif>
<!--- PKP : end 2008-04-01 LEXMARK BUG Issue 46: Orders pages --->
</CF_TRANSLATE>

