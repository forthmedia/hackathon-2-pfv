<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			orderRelatedFile.cfm
Author:			SWJ
Date created:	2005-05-08

Description:	Screen to allow files to be associated with an order

Version history:
1

--->

<CFPARAM name="url.OrderID">		




<cf_head>
	<cf_title>Upload Files</cf_title>
</cf_head>


<CF_Translate>
<h1><cfoutput>Phr_Order_UploadFileTitle</cfoutput></h1>
<p>phr_order_UploadFileExplanatoryText<br></p>
<!--- NJH 2009-07-27 P-FNL069 change from V2 to related file which calls relatedFileV2--->
<cf_relatedFile action="list,put" entity="#url.OrderID#" entitytype="Orders">

<input type=button value="phr_CloseWindow" onClick="javascript:window.opener.location.reload(true); if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();">

</CF_Translate>


