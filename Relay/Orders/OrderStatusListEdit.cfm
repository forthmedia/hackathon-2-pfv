<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Mods:
CPS 230401  Pass through the value of the order for 'Approval' and the organisationId for 'Decline'
CPS 300501	Obtain rights to upload LSS files and Invoice Details through the statuscontrol table.
            Include new link to the invoice details screen where appropriate
2001-06-12      CPS Ticket Id: 5574 - Amend query to check that frmMode exists and that it is set to
              'Incomplete' in order to display dummy orders that have been created by the user going
			  to the promotion screen but not entering any details
2001-06-28		WAB removed some spaces from DownLoadFileOrderStatii, these were causing problems - eg listfind didn't work
2001-06-28		WAB changed condition to view uploaded files, added ownagencyorders
2001-06-28		WAB added status 15 (approved) to DownLoadFileOrderStatii
2001-07-05	CPS convert the points value of the order into the local currency when accessed by an LSS (Ticket 5964)
2001-11-19	CPS ensure the check for threshold approval is carried out against both the integerFlagDataOrg and integerFlagData
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
2015-10-26		ACPK	PROD2015-142 Encode percentage signs in webhandle
 --->


<!--- The Currency Symbol Used in this page --->
<CFSET tmpCurrencySymbol = '$'>
<CFSET DownLoadFileOrderStatii = "12,13,14,15,18,19,20">

<!--- may be ordering using organisationid - need to find all the location id's in this org - do that later though --->

<cfquery name="getLocation" datasource="#application.siteDataSource#">
select
	locationid
from
	person
where
	personid = #request.relayCurrentUser.personid#
</cfquery>


<cfparam name="frmOrderStatus" default="">
<cfparam name="btnViewStatus" default="#frmOrderStatus#">

<CFSET frmFlagTextId = 'Not Defined'>

<CFIF request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    <CFIF UserType IS "Internal">--->

	<CFQUERY NAME="checkThresholdApprover" datasource="#application.sitedatasource#">
	<!--- Need to make sure this question is asked the same way it is in the GetEmailAction SP
			Some of the others probably need to be changed in the same way

		DAM 19 Mar 2001

	--->
		SELECT 'X'
		  FROM Person p
		  JOIN IntegerFlagDataOrg ifd ON p.PersonID = ifd.Data
		  JOIN Flag f ON ifd.FlagID = f.FlagID
		 WHERE f.FlagTextID = 'InFocusThresholdApprover'
		   AND PersonID=#request.relayCurrentUser.personid#
		 UNION
		SELECT 'X'
		  FROM flag fla
		      ,integerFlagData ifd
		 WHERE fla.flagTextId = 'InFocusThresholdApprover'
		   AND ifd.flagId = fla.flagId
		   AND ifd.data = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFIF #checkThresholdApprover.RecordCount# IS NOT 0>
		<CFSET frmFlagTextId = 'InFocusThresholdApprover'>
	<CFELSE>
		<CFQUERY NAME="checkSupportPerson" datasource="#application.sitedatasource#">
			SELECT 'X'
			  FROM flag fla
			      ,booleanFlagData bfd
			 WHERE fla.flagTextId = 'InFocusSupportPerson'
			   AND bfd.flagId = fla.flagId
			   AND bfd.entityId = #request.relayCurrentUser.personid#
		</CFQUERY>

		<CFIF #checkSupportPerson.RecordCount# IS NOT 0>
			<CFSET frmFlagTextId = 'InFocusSupportPerson'>
		<CFELSE>
			<CFQUERY NAME="checkAccountMngr" datasource="#application.sitedatasource#">
				SELECT 'X'
				  FROM flag fla
			    	  ,booleanFlagData bfd
				 WHERE fla.flagTextId = 'isAccountManager'
				   AND bfd.flagId = fla.flagId
				   AND bfd.entityId = #request.relayCurrentUser.personid#
			</CFQUERY>

			<CFIF #checkAccountMngr.RecordCount# IS NOT 0>
				<CFSET frmFlagTextId = 'isAccountManager'>
			<CFELSE>
				<CFQUERY NAME="checkLSS" datasource="#application.sitedatasource#">
					SELECT 'X'
					  FROM flag fla
					      ,booleanFlagData bfd
					 WHERE fla.flagTextId = 'localServiceSupplier'
					   AND bfd.flagId = fla.flagId
					   AND bfd.entityId = #request.relayCurrentUser.personid#
				</CFQUERY>

				<CFIF #checkLSS.RecordCount# IS NOT 0>
					<CFSET frmFlagTextId = 'LocalServiceSupplier'>
				<CFELSE>
					<CFQUERY NAME="checkInFocusFinancePerson" datasource="#application.sitedatasource#">
						SELECT 'X'
						  FROM flag fla
						      ,booleanFlagData bfd
						 WHERE fla.flagTextId = 'InFocusFinancePerson'
						   AND bfd.flagId = fla.flagId
						   AND bfd.entityId = #request.relayCurrentUser.personid#
					</CFQUERY>

					<CFIF #checkInFocusFinancePerson.RecordCount# IS NOT 0>
						<CFSET frmFlagTextId = 'InFocusFinancePerson'>
					</CFIF>
				</CFIF>
			</CFIF>
		</CFIF>
	</CFIF>
<CFELSE>

	<CFQUERY NAME="checkProgramMngr" datasource="#application.sitedatasource#">
			SELECT 'X'
			  FROM flag fla
			      ,booleanFlagData bfd
			 WHERE fla.flagTextId = 'InFocusProgramManager'
			   AND bfd.flagId = fla.flagId
			   AND bfd.entityId = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFIF #checkProgramMngr.RecordCount# IS NOT 0>
		<CFSET frmFlagTextId = 'InFocusProgramManager'>
	<CFELSE>
		<CFSET frmFlagTextId = 'ExternalPartner'>
	</CFIF>
</CFIF>


<!--- CPS convert the points value of the order into the local currency when accessed by an LSS (Ticket 5964) --->
<CFIF frmFlagTextId EQ 'LocalServiceSupplier'>
	<CFQUERY NAME="getCurrency" datasource="#application.sitedatasource#">
		SELECT cou.countryCurrency
		  FROM country cou
		      ,location loc
		      ,person per
		 WHERE cou.countryId = loc.countryId
		   AND per.locationId = loc.locationId
		   AND per.personId = #request.relayCurrentUser.personid#
	</CFQUERY>
</CFIF>

<!--- 2012-07-26 PPB P-SMA001 commented out
<CFINCLUDE TEMPLATE="../templates/qryGetCountries.cfm">
 --->

<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
	select
		ord.*
	   ,sta.description
	   ,pro.promoName
	   ,loc.organisationId
	from
		orders as ord
	   ,status sta
	   ,promotion as pro
	   ,location as loc
	where 	1=1
	and pro.promoId = ord.promoId
    AND loc.locationId = ord.locationId
		<CFIF request.relayCurrentUser.isInternal>

		<!--- this check should only be done if the user is internal --->
           AND ord.locationId IN (SELECT loc1.locationid
			                    	FROM location loc1
			                       	<!--- 2012-07-26 PPB P-SMA001 commented out
			                       	WHERE loc1.countryid  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			                       	 --->
									WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="loc1").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
			                       	)
		</CFIF>
		<cfif frmOrderStatus is not "">
			and ord.OrderStatus =  <cf_queryparam value="#frmOrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" >
			and sta.statusId=ord.OrderStatus
		</cfif>

		<!--- CPS 2001-06-12 Following code replaced to check first whether the form has been invoked from
		      the menu option 'Incomplete Order' --->
		<!---
		<CFIF not request.relayCurrentUser.isInternal OR NOT IsDefined("frmMode")>
		<!--- Ignore any dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->
			AND ISNULL(ord.orderValue,0) != 120
		<CFELSE>
		<!--- Only display dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->
			AND ISNULL(ord.orderValue,0) = 120
		</CFIF> --->

		<CFIF IsDefined("frmMode") AND frmMode EQ 'Incomplete'>
		<!--- Only display dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->
			AND ISNULL(ord.orderValue,0) = 120
		<CFELSE>
		<!--- Ignore any dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->
			  <cfif frmOrderStatus eq 1>
			AND ISNULL(ord.orderValue,0) != 120
			</CFIF>
		</CFIF>

<!--- 		<CFIF IsDefined("frmPromoID")>
			and ord.PromoID IN (QuotedValueList('#frmPromoID#'))
		</CFIF> --->

		<CFIF IsDefined("frmPromoID") AND frmPromoId IS NOT "">
			<CFLOOP INDEX="tmpIndex" FROM="1" TO="#ListLen(frmPromoId)#">
				<CFIF tmpIndex EQ 1>
				    AND (ord.PromoId =  <cf_queryparam value="#ListGetAt(frmPromoID,tmpIndex)#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<CFELSE>
	 				OR ord.PromoId =  <cf_queryparam value="#ListGetAt(frmPromoID,tmpIndex)#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</CFIF>
			</CFLOOP>
			)
		</CFIF>

	    <CFIF #frmFlagTextId# IS 'ExternalPartner'>
		<!--- For External Partner Users select orders for the appropriate location
		      the User currently belongs to --->
			AND ord.locationId = (SELECT per.locationId
			                        FROM person per
								   WHERE per.personId = #request.relayCurrentUser.personid#)
   		<CFELSE>
			<CFIF #frmFlagTextId# IS 'isAccountManager'>
			<!--- For Account Managers select orders that are connected to all the
			locations that the Account Manager belongs to --->

				AND loc.organisationId IN (SELECT org.organisationId
						                  	 FROM organisation org
                 	        		        WHERE org.accountMngrId = #request.relayCurrentUser.personid#)

			<CFELSEIF #frmFlagTextId# IS 'InFocusProgramManager'>

			<!--- For Program Managers select orders that are connected to the location
			that the user belongs to as identified by the appropriate FlagTaskId --->

				AND loc.organisationId = (SELECT per.organisationId
						                    FROM flag fla
										        ,booleanFlagData bfd
 											    ,person per
                	        			   WHERE fla.flagTextId =  <cf_queryparam value="#frmFlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" >
                    					     AND bfd.flagId = fla.flagId
                    					     AND bfd.entityId = per.personId
                     					     AND per.personId = #request.relayCurrentUser.personid#)

			<CFELSEIF #frmFlagTextId# IS 'LocalServiceSupplier'>

			<!--- For Local Service Suppliers select the orders of the Products for which they
			are responsible taking into account in which location the order was placed --->

				AND ord.locationId IN (SELECT loc.locationID
			  			                 FROM orderItem ori
            							     ,productSupplier psu
											 ,flag fla
											 ,booleanFlagData bfd
											 ,person per
											 ,location loc
										WHERE ori.orderId = ord.orderId
										  AND ori.productId = psu.productId
										  AND loc.countryId = psu.countryId
										  AND psu.supplierPersonId = per.personId
										  AND bfd.entityId = per.personId
										  AND per.personId = #request.relayCurrentUser.personid#
										  AND bfd.flagId = fla.flagId
										  AND fla.flagTextId =  <cf_queryparam value="#frmFlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			</CFIF>
		</CFIF>

	order by
		ord.created
</CFQUERY>

<CFQUERY NAME="getApproverStatus" datasource="#application.sitedatasource#">
	SELECT stc.statusType
	  FROM status sta
	      ,statusControl stc
	 WHERE sta.statusId =  <cf_queryparam value="#frmOrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" >
	   AND sta.requiresApprover = 1
	   AND stc.statusId = sta.statusId
	   AND stc.ApproverFlagTextId =  <cf_queryparam value="#frmFlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" >

<!--- 	<CFIF IsDefined("frmPromoID")>
	   AND stc.controlContext IN (#frmPromoID#)
	</CFIF> --->

<!--- 	<CFIF IsDefined("frmPromoID") AND frmPromoId IS NOT "">
		<CFLOOP INDEX="tmpIndex" FROM="1" TO="#ListLen(frmPromoId)#">
			<CFIF tmpIndex EQ 1>
			    AND (stc.controlContext = '#ListGetAt(frmPromoID,tmpIndex)#'
			<CFELSE>
 				OR stc.controlContext = '#ListGetAt(frmPromoID,tmpIndex)#'
			</CFIF>
		</CFLOOP>
		)
	</CFIF> --->

</CFQUERY>

<CFSET Coname = listFirst(getOrderHeader.DelSiteName)>

<CFQUERY NAME="GetBorderSet" datasource="#application.siteDataSource#">
	Select borderset from promotion where promoid =  <cf_queryparam value="#listfirst(frmpromoid)#" CFSQLTYPE="CF_SQL_VARCHAR" >
</CFQUERY>


<CF_TRANSLATE >
<cf_DisplayBorder BorderSet="#GetBorderSet.BorderSet#">



<cf_head>
 	<cf_title> <CFOUTPUT>
 		<CFIF #frmFlagTextId# IS 'External Partner'>
			phr_Order_PartnerOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusProgramManager'>
			phr_Order_PMOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'isAccountManager'>
			phr_Order_AMOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'LocalServiceSupplier'>
			phr_Order_LSSOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusSupportPerson'>
			phr_Order_SPOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusThresholdApprover'>
			phr_Order_TAOrderStatusListTitle
		</CFIF>
		<cfif not request.relayCurrentUser.isInternal>
			phr_Order_StatusListEditSubTitle: #htmleditformat(COname)#
		</cfif> </cfoutput>
	</cf_title>
</cf_head>



<SCRIPT>

<!--
	function verifyForm() {
		var form = document.Dispatch;
			if (verifyCheckBoxClicked() == true) {
			form.submit();
			}
			else {
				window.alert("You must choose an Order to Dispatch");
			}
	}

	function verifyCheckBoxClicked() {
		var form = document.Dispatch;
			for (i=0; i<form.elements.length; i++) {
				document.write('form.chkDispatch[i]');
				if (form.chkDispatch[i].checked=="1") {
					return true;
				}
			}
			return false;
	}
	function displayOrder (page, orderid)  {
		form = window.document.viewOrder
		form.action = page
		form.currentOrderID.value=orderid
		form.submit()
	}
	// CPS 300501 new function to enable the upload of invoice details
	function uploadInvoice(orderId)
	{

		url = <CFOUTPUT>"OrderStatusListInvoiceEdit.cfm?frmOrderID=" +orderId + "&frmOrderStatus=#frmOrderStatus#&frmPromoId=#frmPromoId#"</CFOUTPUT>

		var subwindow = window.open(url,"Login","height=400,width=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0")
		subwindow.moveTo(62, 33)
		subwindow.focus()

	}

	function SubmitForm ()  {
		form.submit()
	}

//-->
</SCRIPT>
<CFSET tmpForm = 'None.cfm'>

<CFIF frmOrderStatus IS 1>
	<CFSET tmpForm = 'Download.cfm'>
</CFIF>
<CFIF frmOrderStatus IS 3>
	<CFSET tmpForm = 'Dispatch.cfm'>
</CFIF>

<TABLE>
	<TR><TD width="400"><H1>
		<CFIF #frmFlagTextId# IS 'External Partner'>
			phr_Order_PartnerOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusProgramManager'>
			phr_Order_PMOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'isAccountManager'>
			phr_Order_AMOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'LocalServiceSupplier'>
			phr_Order_LSSOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusSupportPerson'>
			phr_Order_SPOrderStatusListTitle
		<CFELSEIF #frmFlagTextId# IS 'InFocusThresholdApprover'>
			phr_Order_TAOrderStatusListTitle
		</CFIF>
	</H1></td></tr>
	<cfif not request.relayCurrentUser.isInternal><TR><TD width="400"><B><cfoutput>phr_Order_StatusListEditSubTitle: #htmleditformat(COname)#</cfoutput></B></td></tr></cfif>
</TABLE>

<!--- <TABLE>
	<TR><TD width="400"><H1>phr_Order_StatusListEditTitle</H1></td></tr>
	<cfif not request.relayCurrentUser.isInternal><TR><TD width="400"><B><cfoutput>phr_Order_StatusListEditSubTitle: #COname#</cfoutput></B></td></tr></cfif>
</TABLE>
<TABLE> --->

<FORM METHOD="post" ACTION="OrderStatusListEditTask.cfm">

<CFOUTPUT>

<table>
<tr>
    <th>phr_Order_TransactionID</th>
    <th>phr_Order_Transaction</th>
	<cfif request.relayCurrentUser.isInternal><th>phr_Order_CoName</th></cfif>
    <th>phr_Order_Value</th>
    <th>phr_Order_Status</th>
    <th>phr_Order_PromoName</th>
    <th>phr_Order_PurchaseRef</th>
	<CFIF frmOrderStatus IS 1>
		<th>phr_Order_Edit</th>
<!--- 		<Th><B>Dispatch</b></th>
		<Th><B>Cancel</b></th>
 --->
 	<CFELSE>
		<th>phr_Order_View</th>
 	</CFIF>

	<CFIF getApproverStatus.statusType EQ 'Approve'>
		<th>phr_Order_Approval</th>
		<th>phr_Order_Decline</th>
	<!--- CPS 300501 display Upload buttons where required using generic code --->
	<CFELSEIF getApproverStatus.statusType EQ 'UploadQuote'>
		<th>phr_Order_Upload</th>
	<CFELSEIF getApproverStatus.statusType EQ 'UploadInvoice'>
		<th>phr_Order_UploadInvoice</th>
	</CFIF>

<!--- 	<CFIF frmFlagTextId IS 'LocalServiceSupplier' AND frmOrderStatus EQ 21>
		<th>phr_Order_Upload</th>
	</CFIF>		 --->

</tr>
 </cfoutput>

<CF_INPUT TYPE="Hidden" NAME="frmOrderStatus" VALUE="#frmOrderStatus#">

<CFIF IsDefined('frmPromoID')>
	<CF_INPUT TYPE="Hidden" NAME="frmPromoId" VALUE="#frmPromoId#">
</CFIF>

<CFIF frmFlagTextId IS "Not Defined">
	<H1> You do not have sufficient privileges to view orders</h1>
<CFELSE>
<cfoutput query="getOrderHeader">
<tr>
    <td>#htmleditformat(OrderID)#</td>
    <td nowrap>#htmleditformat(DAY(OrderDate))# #LEFT(MonthAsString(Month(OrderDate)),3)# #htmleditformat(YEAR(OrderDate))#</td>
	<cfif request.relayCurrentUser.isInternal><td nowrap>#htmleditformat(delSitename)#</td></cfif>

	<!--- CPS convert the points value of the order into the local currency when accessed by an LSS (Ticket 5964) --->
	<CFIF frmFlagTextId EQ 'LocalServiceSupplier'>
		<CFSTOREDPROC PROCEDURE="RWDoConversion" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmountIn" VALUE="#OrderValue#" NULL="No">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmountIn" VALUE="" NULL="Yes">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="#getCurrency.countryCurrency#" NULL="No">
			<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_DECIMAL" VARIABLE="CashAmount" DBVARNAME="@ConvertedAmount" NULL="No">
		</CFSTOREDPROC>
	    <td nowrap align="right">#DecimalFormat(CashAmount)#</td>
	<CFELSE>
	    <td nowrap align="right">#DecimalFormat(OrderValue)#</td>
	</CFIF>
	<TD>phr_StatusDescription_Status_#htmleditformat(OrderStatus)#</TD>
	<TD>#htmleditformat(promoName)#</TD>
	<TD>#htmleditformat(theirOrderNumber)#</TD>

	<CFIF frmOrderStatus IS 1>
 		<td ALIGN="CENTER"><A HREF="EditOrder.cfm?tmpOrderID=#OrderID#" TARGET="_blank">phr_Order_Edit</A></td>
	<CFELSE>
		<td ALIGN="CENTER"><A HREF="DisplayOrder.cfm?CURRENTORDERID=#OrderID#" TARGET="_blank">phr_Order_View</A></td>
	</CFIF>

	<!--- CPS 300501 code modified to make use of statuscontrol table --->
	<CFIF getApproverStatus.statusType EQ 'Approve'>
 		<td ALIGN="CENTER"><A HREF="OrderStatusListEditTask.cfm?frmMode=Approve&frmOrderID=#OrderID#&frmOrderStatus=#frmOrderStatus#&frmPromoId=#frmPromoId#&frmPointsAmount=#OrderValue#">phr_Order_Approval</A></td>
 		<td ALIGN="CENTER"><A HREF="OrderStatusListEditTask.cfm?frmMode=Decline&frmOrderID=#OrderID#&frmOrderStatus=#frmOrderStatus#&frmPromoId=#frmPromoId#&frmOrganisationId=#organisationId#">phr_Order_Decline</A></td>
<!--- 	</CFIF>

   	<CFIF frmFlagTextId IS 'LocalServiceSupplier' AND frmOrderStatus EQ 21>  --->
	<CFELSEIF getApproverStatus.statusType EQ 'UploadQuote'>
		<td ALIGN="CENTER"><A HREF="OrderStatusRelatedFile.cfm?frmOrderID=#OrderID#&frmPromoId=#frmPromoId#&frmOrderStatus=#frmOrderStatus#">phr_Order_Upload</A></td>
	<CFELSEIF getApproverStatus.statusType EQ 'UploadInvoice'>
 		<td ALIGN="CENTER"><A HREF="javascript: uploadInvoice(#orderId#)">phr_Order_UploadInvoice</A></td>
 	</CFIF>



	<CFIF listfind(DownLoadFileOrderStatii,#orderstatus#) NEQ 0 AND (#PromoId# EQ 'InFocusLSS' or #PromoId# EQ 'OwnAgencyOrders')>

		<CFQUERY NAME="getOrderItem" datasource="#application.sitedatasource#">
			select itemid
			from orderitem as o
			where orderid =  <cf_queryparam value="#OrderId#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<!--- 2001-06-29 WAB changed all of this so that it will show all the files related to this entity, whatever the type --->
		<!--- NJH 2009-07-27 P-FNL069 changed from v2 to relatedFile which now calls V2. --->
		<cf_relatedFile action="variables" entity="#GetOrderItem.ItemID#" entitytype="OrderItem"  >

			<cfloop query="relatedFileVariables.filelist">
		<!--- WAB 2001-06-29  why do we need to worry about what the file type is?
					<CFIF filename contains ".jpg" or filename contains ".jpeg" or filename contains ".gif" or
					      filename contains ".htm" or filename contains ".html" or filename contains ".txt" or
						  filename contains ".pdf" or filename contains ".doc">
						<td ALIGN="CENTER"><A HREF="javascript:void(window.open('#webhandle#','UploadFileViewer','width=400,height=500,status,resizable,toolbar=1'))">phr_Order_Download #filecategoryDescription#</A></td>
					</CFIF> --->
					<td ALIGN="CENTER"><A HREF="javascript:void(window.open('#replace(webhandle,"%","%25","ALL")#','UploadFileViewer','width=400,height=500,status,resizable,toolbar=1'))">phr_Order_Download #htmleditformat(filecategoryDescription)#</A></td> <!--- 2015-10-26	ACPK	PROD2015-142 PROD2015-142 Encode percentage signs in webhandle  --->

			</cfloop>

	</CFIF>

</tr>
</cfoutput>
</CFIF>

</table>
<br>
<CFIF frmOrderStatus IS 99>
	<DIV ALIGN="center"><INPUT TYPE="SUBMIT" NAME="btnDownload" VALUE="phr_Order_StatusAction "></DIV>
</CFIF>
<CFIF frmOrderStatus IS 99>
	<DIV ALIGN="center"><INPUT TYPE="SUBMIT" NAME="btnDispatchCancel" VALUE=" Dispatch / Cancel "
	<!--- onclick="verifyForm();" ---> ></DIV>
</CFIF>

</FORM>
<!--- </CF_TRANSLATE> --->
</cf_DisplayBorder>
</CF_TRANSLATE >









