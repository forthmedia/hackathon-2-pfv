<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Relay License Seats List</cf_title>
</cf_head>




<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="checkBoxFilterName" type="string" default="showComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Show Complete?">

<cfquery name="getLicenseSeats" datasource="#application.siteDataSource#">
	select Relay_Key,License_Key,Sku,Version,Date_Issued,First_access_Date,rkid,licensee 
	from vRelayLicenseData
	where 1=1
	<cfif isDefined("filterSelect") and filterSelect eq "licensee">and licensee =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
	<cfif isDefined("filterSelect") and filterSelect eq "relay_key">and relay_key =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
	<cfif isDefined("filterSelect") and filterSelect eq "sku">and sku =  <cf_queryparam value="#FILTERSELECTVALUES#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
	<cfif isDefined("SORTORDER") and SORTORDER neq "">order by <cf_queryObjectName value="#SORTORDER#"></cfif>
</cfquery>


<CF_tableFromQueryObject 
	queryObject="#getLicenseSeats#"
	sortOrder = "date_Issued"
	numRowsPerPage="500"
	keyColumnList="license_Key"
	keyColumnURLList="relayLicenseKeyReset.cfm?license="
	keyColumnKeyList="license_Key"
	hideTheseColumns=""
	dateFormat="date_Issued,first_access_date"
	currencyFormat=""
	FilterSelectFieldList="Licensee,Relay_Key,SKU"
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	useInclude="false"
>



