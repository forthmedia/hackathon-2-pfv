<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Set the organisationId and focusLevel using either frmLocationId or frmPersonId. 
      frmLocationId will either be the locationid from an existing order or the location  
	  If frmLocationId does not exist then select the location using the supplied personid --->

<!--- identify whether they are gold silver or bronze --->

<!--- 2010/09/17	NAS	LID3976: Unable to approve Order --->
<cf_include template="\code\cftemplates\ordersINI.cfm" checkIfExists="true">

<!--- LID3976 SKP 2010-11-08 --->
<CFIF NOT IsDefined("frmPersonId") AND NOT IsDefined("frmOrganisationId")>
	<B><FONT COLOR="RED">SYSTEM ERROR:</FONT> Cannot identify person or location. Please contact Support</B>
	<CF_ABORT>
</CFIF> 

<CFIF NOT IsDefined("frmOrganisationId") and isDefined("frmPersonID")>
	<CFQUERY NAME="getOrganisationId" DATASOURCE="#application.sitedatasource#">
		SELECT OrganisationId
		  FROM person
		 WHERE personId =  <cf_queryparam value="#frmPersonId#" CFSQLTYPE="CF_SQL_INTEGER" >  
	</CFQUERY>

	<CFSET frmOrganisationId = getOrganisationId.OrganisationId>
</CFIF>

<!--- NJH 2008/11/10 - Bug Fix Sophos Issue 1056 - case when person no longer exists...shouldn't be the case, I would think, but if so, handle it gracefully --->
<cfif frmOrganisationId neq "">
<CFQUERY NAME="PartnerLevel" DATASOURCE="#application.sitedatasource#">
SELECT f.FlagTextID,
	   o.OrganisationID
  FROM Organisation as o 
 	   INNER JOIN BooleanFlagData as bfg ON o.OrganisationID = bfg.EntityID 
	   INNER JOIN Flag as f ON bfg.FlagID = f.FlagID 
	   INNER JOIN FlagGroup as fg ON f.FlagGroupID = fg.FlagGroupID
 WHERE o.OrganisationID =  <cf_queryparam value="#frmOrganisationId#" CFSQLTYPE="CF_SQL_INTEGER" >   
   AND fg.FlagGroupTextID =  <cf_queryparam value="#application.com.settings.getSetting("orders.partnerFlagGroup")#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<CFSET OrganisationID = PartnerLevel.OrganisationID>
<CFSET PartnerLevel=PartnerLevel.FlagTextID>

<cfelse>
	<br><B><FONT COLOR="RED">SYSTEM ERROR:</FONT> Cannot identify organisation and therefore unable to set the partner level. Please contact Support</B>
</cfif>


