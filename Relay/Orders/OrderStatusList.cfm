<!--- �Relayware. All Rights Reserved 2014 --->

<!--- <cfsetting enablecfoutputonly="Yes"> --->

<!--- <CFIF checkPermission.InFocusTools is 0>
	Sorry you do not have rights to this application 
	<CF_ABORT>
</cfif>

Changes:

13 March 2001 SWJ: Modified the main query to not check for country rights unless they are internal
2001-06-12      CPS Ticket Id: 5574 - Amend query to check that frmMode exists and that it is set to
              'Incomplete' in order to display dummy orders that have been created by the user going 
			  to the promotion screen but not entering any details 
2001-11-19	CPS ensure the check for threshold approval is carried out against both the integerFlagDataOrg and integerFlagData
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
 --->


<cf_head>
	<cf_title>Order Status List</cf_title>
</cf_head>

<br>
<!--- <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Order Status List</DIV>
		</TD>
	</TR>
</table> --->

<CFPARAM NAME="frmPromoId" DEFAULT=""> 
<CFSET frmFlagTextId = 'Not Defined'>
 
<CFIF UserType IS "Internal">

	<CFSET ValidStatii="1,3,4,6,7,8,10,11,12,13,14,18,19,20,21,15">
	
	<CFQUERY NAME="checkThresholdApprover" datasource="#application.sitedatasource#"> 
	<!--- Need to make sure this question is asked the same way it is in the GetEmailAction SP
			Some of the others probably need to be changed in the same way 
	
		DAM 19 Mar 2001
			
	--->
		SELECT 'X'
		  FROM Person p
		  JOIN IntegerFlagDataOrg ifd ON p.PersonID = ifd.Data
		  JOIN Flag f ON ifd.FlagID = f.FlagID
		 WHERE f.FlagTextID = 'InFocusThresholdApprover'
		   AND PersonID=#request.relayCurrentUser.personid#
		 UNION 
		SELECT 'X'
		  FROM flag fla
		      ,integerFlagData ifd
		 WHERE fla.flagTextId = 'InFocusThresholdApprover'
		   AND ifd.flagId = fla.flagId
		   AND ifd.data = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFIF #checkThresholdApprover.RecordCount# IS NOT 0> 
		<CFSET frmFlagTextId = 'InFocusThresholdApprover'>
	<CFELSE>
		<CFQUERY NAME="checkSupportPerson" datasource="#application.sitedatasource#"> 
			SELECT 'X'
			  FROM flag fla
			      ,booleanFlagData bfd
			 WHERE fla.flagTextId = 'InFocusSupportPerson'
			   AND bfd.flagId = fla.flagId
			   AND bfd.entityId = #request.relayCurrentUser.personid#
		</CFQUERY>

		<CFIF #checkSupportPerson.RecordCount# IS NOT 0> 
			<CFSET frmFlagTextId = 'InFocusSupportPerson'>
			<CFSET ValidStatii = ListAppend(ValidStatii, "2,100")> 
		<CFELSE>
			<CFQUERY NAME="checkAccountMngr" datasource="#application.sitedatasource#"> 
				SELECT 'X'
				  FROM flag fla
			    	  ,booleanFlagData bfd
				 WHERE fla.flagTextId = 'isAccountManager'
				   AND bfd.flagId = fla.flagId
				   AND bfd.entityId = #request.relayCurrentUser.personid#
			</CFQUERY>		   	  
	
			<CFIF #checkAccountMngr.RecordCount# IS NOT 0> 
				<CFSET frmFlagTextId = 'isAccountManager'>
			<CFELSE>
				<CFQUERY NAME="checkLSS" datasource="#application.sitedatasource#"> 
					SELECT 'X'
					  FROM flag fla
					      ,booleanFlagData bfd
					 WHERE fla.flagTextId = 'LocalServiceSupplier'
					   AND bfd.flagId = fla.flagId
					   AND bfd.entityId = #request.relayCurrentUser.personid#
				</CFQUERY>	

				<CFIF #checkLSS.RecordCount# IS NOT 0> 
					<CFSET frmFlagTextId = 'LocalServiceSupplier'>
				<CFELSE>
					<CFQUERY NAME="checkInFocusFinancePerson" datasource="#application.sitedatasource#"> 
						SELECT 'X'
						  FROM flag fla
						      ,booleanFlagData bfd
						 WHERE fla.flagTextId = 'InFocusFinancePerson'
						   AND bfd.flagId = fla.flagId
						   AND bfd.entityId = #request.relayCurrentUser.personid#
					</CFQUERY>	

					<CFIF #checkInFocusFinancePerson.RecordCount# IS NOT 0> 
						<CFSET frmFlagTextId = 'InFocusFinancePerson'>
					</CFIF>		
				</CFIF>			
			</CFIF>
		</CFIF>			
	</CFIF>	
<CFELSE>

	<CFSET ValidStatii="1,3,4,6,7,8,10,11,12,13,14,18,19,20,21,15">

	<CFQUERY NAME="checkProgramMngr" datasource="#application.sitedatasource#"> 
			SELECT 'X'
			  FROM flag fla
			      ,booleanFlagData bfd
			 WHERE fla.flagTextId = 'InFocusProgramManager'
			   AND bfd.flagId = fla.flagId
			   AND bfd.entityId = #request.relayCurrentUser.personid#
	</CFQUERY>	

	<CFIF #checkProgramMngr.RecordCount# IS NOT 0> 
		<CFSET frmFlagTextId = 'InFocusProgramManager'>
	<CFELSE>	
		<CFSET frmFlagTextId = 'ExternalPartner'>	
	</CFIF>		
</CFIF>
 
<CFIF frmFlagTextId IS "Not Defined">
	<br>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			Sorry this view is only available to Account Managers and the In Focus Support Team 
		</td>
	</tr>
	</table>	
	<CF_ABORT>
</CFIF>

<!--- 2012-07-26 PPB P-SMA001 commented out
<CFINCLUDE TEMPLATE="../templates/qryGetCountries.cfm">
 --->
<CFQUERY NAME="getOrderStatusQuantities" datasource="#application.sitedatasource#">
		SELECT sta.description
		      ,ord.orderStatus
		      ,count(*) NoOfRecords
		  FROM orders as ord
		      ,status as sta
			  ,location as loc
		 WHERE ord.OrderStatus = sta.StatusID
		   AND loc.locationId = ord.locationId
 		   AND sta.StatusId  IN ( <cf_queryparam value="#ValidStatii#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<CFIF UserType IS "Internal">   
		<!--- this check should only be done if the user is internal --->
           AND ord.locationId IN (SELECT loc1.locationid								   
			                        FROM location loc1
			                        <!--- 2012-07-26 PPB P-SMA001 commented out
			                       	WHERE loc1.countryid  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			                        --->
									WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="loc1").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->	
			                        
			                       )
		</CFIF>
		
		<!--- CPS 2001-06-12 Following code replaced to check first whether the form has been invoked from
		      the menu option 'Incomplete Order' --->
		<!--- <CFIF UserType IS "External" OR NOT IsDefined("frmMode")>	
		<!--- Ignore any dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->								   
			AND ISNULL(ord.orderValue,0) != 120										   
		<CFELSE>
		<!--- Only display dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->			
			AND ISNULL(ord.orderValue,0) = 120	
		</CFIF> --->

		<CFIF IsDefined("frmMode") AND frmMode EQ 'Incomplete'>
		<!--- Only display dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->			
			AND ISNULL(ord.orderValue,0) = 120
			and ord.orderstatus=1
		<CFELSE>
		<!--- Ignore any dummy orders that have been created by the user going to the promotion screen
		      but not entering any details --->		
			  		
			AND ( (ord.orderstatus = 1 and ISNULL(ord.orderValue,0) != 120) or (ord.orderstatus <> 1))

		</CFIF>
			
	    <CFIF #frmFlagTextId# IS 'ExternalPartner'>
		<!--- For External Partner Users select orders for the appropriate location 
		      the User currently belongs to --->
			AND ord.locationId = (SELECT per.locationId
			                        FROM person per
								   WHERE per.personId = #request.relayCurrentUser.personid#)
   		<CFELSE>  
			<CFIF #frmFlagTextId# IS 'isAccountManager'>
			<!--- For Account Managers select orders that are connected to all the 
			locations that the Account Manager belongs to --->		
						  
				AND loc.organisationId IN (SELECT org.organisationId
						                  	 FROM organisation org
                 	        		        WHERE org.accountMngrId = #request.relayCurrentUser.personid#)								  
										  
			<CFELSEIF #frmFlagTextId# IS 'InFocusProgramManager'> 
			<!--- For all other users select orders that are connected to the location 
			that the user belongs to as identified by the appropriate FlagTaskId --->		
						  
				AND loc.organisationId = (SELECT per.organisationId
						                    FROM flag fla
    									        ,booleanFlagData bfd
	     									    ,person per
                    	        		   WHERE fla.flagTextId =  <cf_queryparam value="#frmFlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" > 
                        				     AND bfd.flagId = fla.flagId
                        				     AND bfd.entityId = per.personId
                        				     AND per.personId = #request.relayCurrentUser.personid#)								  
			<CFELSEIF #frmFlagTextId# IS 'LocalServiceSupplier'>		

			<!--- For Local Service Suppliers select the orders of the Products for which they 
			are responsible taking into account in which location the order was placed --->												 

				AND ord.locationId IN (SELECT loc.locationID
			  			                 FROM orderItem ori
            							     ,productSupplier psu
											 ,flag fla
											 ,booleanFlagData bfd
											 ,person per
											 ,location loc
										WHERE ori.orderId = ord.orderId
										  AND ori.productId = psu.productId	  
										  AND loc.countryId = psu.countryId
										  AND psu.supplierPersonId = per.personId
										  AND bfd.entityId = per.personId
										  AND per.personId = #request.relayCurrentUser.personid#
										  AND bfd.flagId = fla.flagId
										  AND fla.flagTextId =  <cf_queryparam value="#frmFlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" > ) 
			
			</CFIF>												 										  
		</CFIF>

<!--- 		<CFIF IsDefined("frmPromoID")>
			AND ord.PromoID IN #PromoIds#
		</CFIF> --->
		
		<CFIF IsDefined("frmPromoID") AND frmPromoId IS NOT "">
			<CFLOOP INDEX="tmpIndex" FROM="1" TO="#ListLen(frmPromoId)#">
				<CFIF tmpIndex EQ 1> 
				    AND (ord.PromoId =  <cf_queryparam value="#ListGetAt(frmPromoID,tmpIndex)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				<CFELSE>
	 				OR ord.PromoId =  <cf_queryparam value="#ListGetAt(frmPromoID,tmpIndex)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				</CFIF>
			</CFLOOP>		
			)
		</CFIF>		
		
      GROUP BY sta.description
	          ,ord.orderStatus
	  ORDER BY ord.orderStatus			  
</CFQUERY>		




<!--- temporary testing params --->

<cfparam name="promoID" default="InFocusMarketingStore">
<cfparam name="distributor" default="">

<CFSET orderstatuslist="Quote, Confirmed, Being processed, Complete">
<CFSET RealOrderStatuses="2,3,14,15">


<cfset DistiOrderStatuses = "2,10,11">

<cfset status_Confirmed = 2>
<cfset status_PartialDispatch = 10>
<cfset status_Dispatched = 11>

<!--- include the query --->
<cfparam name="form.selectedOrderStatuses" default="#DistiOrderStatuses#">


<CFQUERY NAME="GetBorderSet" datasource="#application.siteDataSource#">
	Select borderset from promotion where promoid =  <cf_queryparam value="#listfirst(frmpromoid)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>


<cf_DisplayBorder borderset="#GetBorderSet.BorderSet#">
<cf_translate>
<TABLE>
	<TR><TD width="400"><H1>
		<CFIF #frmFlagTextId# IS 'External Partner'>
			<CFOUTPUT>phr_Order_PartnerOrderStatusListTitle</CFOUTPUT>
		<CFELSEIF #frmFlagTextId# IS 'InFocusProgramManager'>
			<CFOUTPUT>phr_Order_PMOrderStatusListTitle</CFOUTPUT>
		<CFELSEIF #frmFlagTextId# IS 'isAccountManager'> 
			<CFOUTPUT>phr_Order_AMOrderStatusListTitle</CFOUTPUT>
		<CFELSEIF #frmFlagTextId# IS 'LocalServiceSupplier'>
			<CFOUTPUT>phr_Order_LSSOrderStatusListTitle</CFOUTPUT>			
		<CFELSEIF #frmFlagTextId# IS 'InFocusSupportPerson'>
			<CFOUTPUT>phr_Order_SPOrderStatusListTitle</CFOUTPUT>			
		<CFELSEIF #frmFlagTextId# IS 'InFocusThresholdApprover'>
			<CFOUTPUT>phr_Order_TAOrderStatusListTitle</CFOUTPUT>				
		</CFIF>
	</H1></td></tr>
	<cfif usertype EQ "external"><TR><TD width="400"><B><cfoutput>phr_Order_StatusListEditSubTitle <!--- : #COname# ---></cfoutput></B></td></tr></cfif>	
</TABLE>

<TABLE>
	<TR>
		<TD WIDTH="503" VALIGN="top">
			<TABLE>
				<CFIF getOrderStatusQuantities.RecordCount EQ 0> 
					<TR>
						<TD><CFOUTPUT>phr_Order_HowThisWorks</CFOUTPUT></td>
					</tr>
				<CFELSE>
					<TR>
	 					<TH><B><CFOUTPUT>phr_Order_TransactionStatus</CFOUTPUT></B></TH>
					    <TH><B><CFOUTPUT>phr_Order_QuantityAtThisStatus</CFOUTPUT></B></TH>
			   			<TH>&nbsp;</TH>
		 			</TR>
					<CFOUTPUT query="getOrderStatusQuantities">		
					<TR>
						<TD> phr_StatusDescription_Status_#htmleditformat(OrderStatus)#</TD>			
						<TD ALIGN="CENTER">
							#htmleditformat(NoOfRecords)#
						</TD>
						<TD ALIGN="CENTER">
							<CFIF #NoOfRecords# IS NOT 0>
<A HREF="OrderStatusListEdit.cfm?frmOrderStatus=#OrderStatus#<CFIF IsDefined('frmPromoID')>&frmPromoID=#htmleditformat(frmPromoID)#</CFIF><CFIF IsDefined('frmMode')>&frmMode=#htmleditformat(frmMode)#</CFIF>" ALT="View #htmleditformat(description)# Orders">phr_Order_view </A>
							</CFIF>
						</TD>
					</TR>		
					</CFOUTPUT>			
				</cfif>				
			</TABLE>
		</TD>
	</TR>
</TABLE>	
</cf_translate>
</cf_DisplayBorder>






