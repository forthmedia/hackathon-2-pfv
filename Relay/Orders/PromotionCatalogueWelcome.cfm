<!--- �Relayware. All Rights Reserved 2014 --->
<!---

****************************************

			EVALUATION TOOL

****************************************


WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
--->



<CFIF NOT IsDefined("promoid")>
	The Evaluation Tool cannot be called without a PromoID (CampaignID)
</CFIF>


<CFQUERY NAME="getCounTRy" datasource="#application.siteDataSource#">
	Select c.isocode
	from location as l, person as p, counTRy as c
	where l.locationid = p.locationid
	and 	l.counTRyid = c.counTRyid
	and p.personid = #request.relayCurrentUser.personid#
</CFQUERY>

<CFIF isdefined("frmprocessid")>
	<cfset application.com.globalFunctions.cfcookie(name="process", value="#frmProcessId#-#frmStepId#")>
</cfif>


<cf_head>
<cf_title><CFOUTPUT>phr_Order_Title_#htmleditformat(PromoID)#</CFOUTPUT></cf_title>

</cf_head>

<cf_translate>
<CF_DISPLAYBORDER BorderSet="#application.standardBorderset#">

<TABLE width="70%">
	<TR><TD>&nbsp;<br></TD></TR>
	<TR>
	    <TD width="10%">
		&nbsp;
		</TD>
		<TD><CFOUTPUT>phr_Order_ProdEvalAvailableTo_#htmleditformat(PromoID)#</CFOUTPUT>
        <p><CFOUTPUT>phr_Order_WebsiteDesignedTo_#htmleditformat(PromoID)#</CFOUTPUT>
		</TD>
      </TR>

      <TR>
	  	<TD></TD>
		<TD align="center">
			<cfinclude template="CategoryList.cfm">
		</TD>
	  </TR>

	<TR>

</TABLE>

</CF_DISPLAYBORDER>
</cf_translate>


