<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			shoppingBasket.cfm
Author:			DJH
Date created:	31 May 2000

Description:	Displays a list of the items currently marked with a value greater than 0.

Version history:
1

WAB 2001-04-26		Altered to use the qryGetProductList.cfm Query
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate

--->




<cf_head>
	<cfoutput><cf_title>#request.CurrentSite.Title# - Shopping Basket</cf_title></cfoutput>


<!--- 
Need to loop through the query string, getting a list of the product id's to get details back for.
--->

<cfset ProductIDList ="">
<cfloop list="#request.query_string#" index="aquant" delimiters="&">	
	<cfif aquant contains "quant_">	
		<cfset ProductIDList = listAppend(productIDList,listGetAt(aquant,2,"_="))>
	</cfif>
</cfloop>

<!--- get promo name --->
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
	select 	
		*
	from 	
		promotion 
	where 	
		promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

	<cfset frmLocationId = currentlocationid>
	<!--- <CFINCLUDE TEMPLATE="SetPartnerLevel.cfm">  --->
	

	<!--- get all active products 
	WAB 2001-04-26 moved this query to a separate template --->
	<CFINCLUDE template = "qryGetProductList.cfm">




<CFSET ZeroPrice = GetOrderHeader.ZeroPrice>
<CFSET MaxOrdNo = getorderheader.MaxOrderNo>

<cfset ShowProductGroup = GetOrderHeader.ShowProductGroup>	
<cfset ShowProductGroupHeader = GetOrderHeader.ShowProductGroupHeader>
<cfset ShowTermsConditions = GetOrderHeader.ShowTermsConditions>

<cfset ShowProductCode = GetOrderHeader.ShowProductCode>
<cfset ShowProductCodeAsAnchor = GetOrderHeader.ShowProductCodeAsAnchor>
<cfset ShowPictures = GetOrderHeader.ShowPictures>
<cfset ShowDescription = GetOrderHeader.ShowDescription>
<cfset ShowPointsValue = GetOrderHeader.ShowPointsValue>
<cfset ShowLineTotal = GetOrderHeader.ShowLineTotal>
<cfset ShowQuantity = GetOrderHeader.ShowQuantity>
<cfset ShowListPrice = GetOrderHeader.ShowListPrice>
<cfset ShowDiscountPrice = GetOrderHeader.ShowDiscountPrice>
<cfset ShowNoInStock = GetOrderHeader.ShowNoInStock>
<cfset ShowMaxQuantity = GetOrderHeader.ShowMaxQuantity>
<cfset ShowPreviouslyOrdered = GetOrderHeader.ShowPreviouslyOrdered>

<cfset ShowPageTotal = GetOrderHeader.ShowPageTotal>
<cfset ShowDecimals = GetOrderHeader.ShowDecimals>

<cfset UsePoints = GetOrderHeader.UsePoints>
<cfset PointsDetailURL = GetOrderHeader.PointsDetailURL>

<cfset TranslateDescription = GetOrderHeader.TranslateDescription>

<cfset PointsFactor = 1>
<cfif UsePoints>

	<cfinclude template="../InFocusWebsite/sql_PointsBalance.cfm">
	<cfset GetTotalPoints.PointsAvailable = PointsBalance.balance>	

	<cfquery name="PointsFactor" datasource="#application.siteDataSource#">
		select
			CurrencyPerPoint
		from
			RWPointsConversion
		where
			PriceISOCode =  <cf_queryparam value="#getProductList.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" > 			
	</cfquery>
	
	<cfif PointsFactor.recordcount is 1>
		<cfset PointsFactor = PointsFactor.CurrencyPerPoint>
	<cfelse>
		<cfset PointsFactor = 1>
	</cfif>
</cfif>
					



<!--- the number of columns that will be used are based on the value of the switches above --->

<cfset NumColumns 	= bitOr(ShowProductCode,ShowProductCodeAsAnchor) 
					+ ShowPictures 
					+ ShowDescription 
					+ ShowPointsValue
					+ ShowLineTotal
					+ ShowQuantity
					+ ShowListPrice
					+ ShowDiscountPrice
					+ ShowNoInStock
					+ ShowMaxQuantity
					+ ShowPreviouslyOrdered>

<script language="JavaScript">
// create a javascript array to hold the various fields which  are used to calculate maxqty etc

var fieldIs
var fieldWas
var valueIs
var pointsAvailable = <cfif isDefined("GetTotalPoints.PointsAvailable") and GetTotalPoints.PointsAvailable gt 0><cfoutput>#jsStringFormat(getTotalPoints.PointsAvailable)#</cfoutput><cfelse>0</cfif>

var records = new Array()

function Record(productID, numberAvailable, maxQuantity, previouslyOrdered, listPrice, discountPrice, displayable)
{
	this.productID = productID;
	this.numberAvailable = numberAvailable;
	this.maxQuantity = maxQuantity;
	this.previouslyOrdered = previouslyOrdered;
	this.listPrice = listPrice;
	this.discountPrice = discountPrice;
	this.displayable = displayable
}


// load the array from the query - only when we are on the correct product group or no product groups are used
// 

<cfoutput query="GetProductList">
records[records.length] = new Record(parseInt(#jsStringFormat(productID)#,10), parseInt(#iif(numberavailable gt 0,de(numberavailable),0)#,10), parseInt(#iif(MaxQuantity neq "", MaxQuantity ,0)#,10), parseInt(#jsStringFormat(PreviouslyOrdered)#,10), parseFloat(#val(val(listprice) / PointsFactor)#,10), parseFloat(#val(val(discountprice) / PointsFactor)#,10), <cfif MaxQuantity is 0 or (MaxQuantity gt PreviouslyOrdered)>true<cfelse>false</cfif>)</cfoutput>

// array record locator

function recordFind(aproductID)
{
	for (i=0; i<records.length; i++)
	{
		if (records[i].productID == aproductID)
		break
	}
	return i
}


// this function could probably be more elegant but still.
function recFindNextDisplayable(idx, direction)
{
	var found = false
	if (direction == "+")
	{
		for (i = idx+1; i < records.length; i++)
		{
			if(records[i].displayable)
			{
				found = true
				break
			}
		}
		
		if(!found)
		{
			// try going from the start - as before it must've got to the end
			for (i=0; i < records.length; i++)
			{
				if(records[i].displayable)
				{
					found = true
					break
				}
			}
		}
	}
	else
	{
		for (i = idx-1; i >= 0; i--)
		{
			if(records[i].displayable)
			{
				found = true
				break
			}		
		}
		if (!found)
		{
			// try going from the end - as before it must've got to the start
			for (i=records.length; i >= 0; i--)
			{
				if(records[i].displayable)
				{
					found = true
					break
				}
			}
		}
	}
	return records[i].productID
}



// function to calculate the total value of items ordered for a line total. also handles the tabulation from
// one field to the next in both directions, taking into account 'missing' fields.
function doLineTotal(object, productID, navigate)
{
	if (navigate == null) var navigate = true
	var recidx = recordFind(productID)
	var maxqty = records[recidx].maxQuantity
	var prevord = records[recidx].previouslyOrdered
	var quantobj = eval('object.form.quant_' + productID)
	var lineobj = eval('object.form.line_' + productID)
	var priceobj = eval('object.form.price_' + productID)
	if((quantobj != null) && (lineobj != null) && (priceobj != null))
	{
		if (quantobj.value.length == 0) quantobj.value = 0
		if (maxqty > 0)
		{
			if (parseInt(quantobj.value,10) + prevord > maxqty)
			{
				alert("<cfoutput>phr_Order_MaximumItemsYouCanOrder</cfoutput> " + (maxqty - prevord))
				quantobj.value = valueIs
				quantobj.focus()
				quantobj.select()
				navigate = false
			}
		}
 		//window.status="FieldIs: " + fieldIs + " FieldWas: " + fieldWas
		<cfif ShowDecimals>
		lineobj.value = formatNum(parseFloat(priceobj.value, 10) * parseInt(quantobj.value, 10), 2)
		<cfelse>
		lineobj.value = parseInt(priceobj.value, 10) * parseInt(quantobj.value, 10)
		</cfif>
 		myparent = window.opener
 		parentForm = myparent.document.mainForm
 		if(parentForm != null)
 		{
 			parentquantobj = eval('parentForm.quant_' + productID)
 			parentlineobj = eval('parentForm.line_' + productID)
 			if (parentquantobj != null)
 				parentquantobj.value = quantobj.value
 			if (parentlineobj != null)
 				parentlineobj.value = lineobj.value
 			myparent.doPageTotal()
 				
 		}
		if(navigate) doNavigate(object)
	}
	doPageTotal()
	<cfif ShowPointsValue>
	// need to make sure the reward points are not exceeded.
	tmp = doTotalPoints()
	</cfif>
}

function doTotalPoints()
{
	var retval = false
	var form = document.mainForm
	var re = /^quant_/
	var tmpPoints = 0

	// loop through each quant field on the form. work out the accumulated price.
	for (i = 0; i < form.length; i++)
	{
		if (re.test(form[i].name) && form[i].value > 0)
		{
			// it's a field we are interested in
			tmpPrice = eval("form.price_" + form[i].name.substr(6))
			tmpPoints += (parseInt(form[i].value) * parseInt(tmpPrice.value))
			if(tmpPoints >= pointsAvailable)
			{
				if (tmpPoints == pointsAvailable && !window.opener.pointsAlertDisplayed) alert("<cfoutput>phr_Order_ReachedPointsAvailableTotal</cfoutput> " + pointsAvailable)
				if (tmpPoints > pointsAvailable && !window.opener.pointsAlertDisplayed) alert("<cfoutput>phr_Order_ExceededPointsAvailableTotal</cfoutput> " + pointsAvailable)
				window.opener.pointsAlertDisplayed = true
				retval = true					
				break
			}
		}
		if (i+1 == form.length && window.opener.pointsAlertDisplayed)
			window.opener.pointsAlertDisplayed = false				
	}
	window.status = "Points: " +tmpPoints
	return retval	
}


function doNavigate(object)
{
	tmpidx = recordFind(fieldIs)
	if (fieldIs >= fieldWas)
	{
		tmpobj = eval("object.form.quant_" + (recFindNextDisplayable(tmpidx, '+')))
		tmpobj.focus()
		tmpobj.select()
	}
	else
	{
		tmpidx = recordFind(fieldWas)					
		tmpobj = eval("object.form.quant_" + (recFindNextDisplayable(tmpidx, '-')))
		tmpobj.focus()
		tmpobj.select()
		//window.status = "FieldIs: " + fieldIs + " FieldWas: " + fieldWas + " object: " + tmpobj.name
	}
}



	// Added formatting function so numbers look better in text box total.
	function formatNum (expr,decplaces)
	{
		var str=(Math.round(parseFloat(expr) * Math.pow(10,decplaces))).toString()
		while (str.length <= decplaces)
		{
			str="0" + str
		}
		var decpoint = str.length - decplaces
		return str.substring(0,decpoint) + "." + str.substring(decpoint,str.length)
	}	
	
	
// function to do all stuff necessary when the screen loads...
function screenLoaded()
{
	var firstquant = eval('document.mainForm.quant_' + records[0].productID)
	if(firstquant != null && firstquant.type == 'text') 
		firstquant.focus()
	doPageTotal()
	return true
}

function doPageTotal()
{
	var tot = 0
		
	// go through each displayable element and add up the totals...
	for (i=0; i < records.length; i++)
	{

		if (records[i].displayable)
		{
			// set the line total - just in case it didn't work!
			lineobj = eval('document.mainForm.line_' + records[i].productID)
			qtyobj = eval('document.mainForm.quant_' + records[i].productID)
			if(lineobj != null && qtyobj != null)
			{
				<cfif showDecimals>
				lineobj.value = formatNum(parseInt(qtyobj.value,10) * parseFloat(records[i].discountPrice,10),2)
				<cfelse>
				lineobj.value = parseInt(qtyobj.value,10) * parseInt(records[i].discountPrice,10)
				</cfif>
				tot += parseFloat(lineobj.value,10)
			}
			
		}
	}
	if (document.mainForm.TotalPrice != null)
	{
		<cfif ShowDecimals>
		document.mainForm.TotalPrice.value = formatNum(tot,2)
		<cfelse>
		document.mainForm.TotalPrice.value = tot
		</cfif>
	}
}






</script>






</cf_head>

<cf_translate>
<cf_body onload="screenLoaded()">

<div align="center">
<h1>Shopping Basket</h1>

<table border="0">
	<!---  Labels Section  --->
	<TR valign="top">
		<CFIF ShowPictures>
			<TD>&nbsp;</TD>
		</CFIF>
		<cfoutput>
		<cfif ShowProductCode or ShowProductCodeAsAnchor>
			<TD><B>phr_Order_ProductCode</b></td>
		</cfif>
		<cfif ShowDescription>
			<TD><B>phr_Description</b></td>
		</cfif>
		<CFIF ShowListPrice>
			<TD align="center"><B>phr_Order_ListPrice<br>(#htmleditformat(GetProductList.PriceISOCode)#)</b></td>
		</CFIF>
		<CFIF ShowDiscountPrice>
			<TD align="center"><B>phr_Order_DiscountPrice<br>(#htmleditformat(GetProductList.PriceISOCode)#)</b></td>
		</cfif>
		<cfif ShowPointsValue>
			<TD align="center"><B>phr_Order_RewardPoints</b></td>
		</CFIF>
		<CFIF ShowNoInStock>
			<TD align="center"><B>phr_Order_NoInStock </b></td>
		</CFIF>
		<cfif ShowMaxQuantity>
			<TD align="center"><B>phr_Order_MaxQty</b></td>
		</cfif>
		<cfif ShowPreviouslyOrdered>
			<TD align="center"><B>phr_Order_PrevOrd</b></td>
		</cfif>		
		<CFIF ShowQuantity>			
			<TD align="center"><B>phr_Order_Qty</b></td>
		</CFIF>
		<cfif ShowLineTotal>
			<TD align="center"><B>phr_Total</b></td>
		</cfif>
		</cfoutput>
	</tr>
	
	
	<!--- Detail Section --->
	
	<CFIF getproductlist.recordcount is 0>
	
		<TR>
			<TD  colspan="5">There are no products set up for this country</td>
		</tr>
	
	<CFELSE>
	
		<form name="mainForm">
	
		<CFOUTPUT query="getproductlist" group="ProductGroupID" >
			<cfif ShowProductGroupHeader>
				<cfif TranslateDescription>
		
					<TR>
						<TD  colspan="#NumColumns#" class="label"><B>phr_Description_ProductGroup_#htmleditformat(ProductGroupID)#</b></td>
					</tr>
				<cfelse>
				</cfif>
				<!--- 
				only display this if we are either not using product groups, 
				or we are and this is the one that is currently selected 
				
				<TR>
					<TD  colspan="#NumColumns#" class="label"><B>#ProductGroup# </b></td>
				</tr>--->
			</cfif>

			<CFOUTPUT>
				<TR>
				
				<CFIF ShowPictures>
					<TD>
					<CFIF IncludeImage EQ 1>
												
						<!--- <CF_RelatedFile action="list" query="productquery" entity="#productID#" entitytype="Product" FileCategory="ProductThumbs">
						
		
						<A HREF="javascript:window.opener.detail(#ProductID#)"><IMG alt="productdescription" src="#productquery.webhandle#" BORDER=0></A> --->
					<CFELSE>
						&nbsp;
					</CFIF>
					</TD>
				</CFIF>
				
				<cfif ShowProductCode or ShowProductCodeAsAnchor>
					<CFIF PromoID IS "RewardCompanyCatalogue">
						<TD>
						<cfif ShowProductCodeAsAnchor>
							<A HREF="../partners/Reward/ItemQuestions.cfm?orderitemid=23">
							#htmleditformat(SKU)#
							</A>
						<cfelse>
							#htmleditformat(SKU)#
						</cfif>
						</td>
					<CFELSEIF PromoID is "Reward">
						<TD>
						<cfif ShowProductCodeAsAnchor>
							<A HREF="../partners/Reward/ManageConsumerProducts.cfm?frmProductID=#ProductID#">
							#htmleditformat(SKU)#
							</A>
						<cfelse>
							#htmleditformat(SKU)#
						</cfif>
						</td>
					<cfelse>
						<TD>#htmleditformat(SKU)#</td>
					</CFIF>
				</cfif>
				<cfif ShowDescription>
					<cfif TranslateDescription>
						
						<TD>phr_Description_Product_#htmleditformat(ProductID)# </td>
					<cfelse>
						<TD>#htmleditformat(Description)# </td>
					</cfif>
				</cfif>
				<CFIF ShowListPrice>
					<TD nowrap align="center">
					  
						#decimalformat(ListPrice)#
					
					</td>
				</cfif>					
				<CFIF ShowDiscountPrice>
					<TD align="center">
					
						#decimalformat(DiscountPrice)#
					
					</td>
				</cfif>
				<cfif ShowPointsValue>
					<TD align="center">
					
						<!--- check with Alex --->
					
					</td>
				</cfif>
				<CFIF ShowNoInStock>
					<TD align="center">
						
						#htmleditformat(NumberAvailable)#
						
					</td>
				</CFIF>

				<cfif ShowMaxQuantity>
					<td align="center">
						#iif(MaxQuantity is 0,DE(""),MaxQuantity )#
					</td>
				</cfif>
				<cfif ShowPreviouslyOrdered>
					<td align="center">
						#htmleditformat(PreviouslyOrdered)#
					</td>
				</cfif>
				<!--- only display text if we're on appropriate group, or groups not used --->

				<TD align="center"><CF_INPUT TYPE="text" NAME="quant_#productid#" value="#evaluate("quant_#productID#")#" size="4" maxlength="4" onFocus="valueIs = this.value; fieldIs = #productID#; this.select()" onBlur="fieldWas = #productID#; doLineTotal(this, #productID#, false)"></td>
				<cfif ShowLineTotal>
					<td align="center">
						<CF_INPUT type="text" name="line_#productid#" size="7" value="#decimalformat(evaluate("quant_#productID# * discountprice"))#" onFocus="fieldIs = #productID#; doLineTotal(quant_#productid#, #productID#)">
						<CF_INPUT TYPE="HIDDEN" NAME="price_#productid#" VALUE="#discountprice#">
					</td>
				</cfif>
		
			</cfoutput>
			</tr>
		</cfoutput>
		
		<!--- Summary/Totals Section --->
		
		<CFIF ZeroPrice IS NOT 1 or UsePoints is 1>
			<cfif ShowPageTotal>
			<tr>
				<td colspan="<cfoutput>#htmleditformat(NumColumns)#</cfoutput>"><hr size="1"></td>
			</tr>		
			</cfif>
			<TR>
				<!--- Total Price displayed only if Prices for the Promotion are not 0--->
			
				<cfif ShowPageTotal>
					<td>&nbsp;</td>
					<td colspan="<cfoutput>#int(numColumns -2)#</cfoutput>">
					<b>
					<CFIF PromoID IS NOT "Reward" AND PromoID IS NOT "RewardCompanyCatalogue">
						<CFOUTPUT>phr_Order_TotalPrice</CFOUTPUT>
					<CFELSE>
						<CFIF PromoID IS NOT "RewardCompanyCatalogue">
							<CFOUTPUT>Total Points</CFOUTPUT>
						</CFIF>
					</CFIF>
					
					</td>
					<TD align="center">						
						<INPUT TYPE="text" NAME="TotalPrice" VALUE="0" size="7" DISABLED>
					</td>
				</cfif>					
			</tr>
		</CFIF>

		</form>

	</cfif>	
	
	<tr>
		<td colspan="<cfoutput>#htmleditformat(numcolumns)#</cfoutput>" align="right"><a href="javascript: window.close()"><cfoutput>phr_Close</cfoutput></a></td>
	</tr>
	
</table>
</div>


</cf_translate>

