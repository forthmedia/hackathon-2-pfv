<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

NewReportOrdersTask.cfm

Author:  ?

Mods  
2001-02-09 		WAB Added column with difference between Disti and Reseller Prices
2001-02-26  	WAB Added download of the table into excel
2001-03-21		WAB changed queries to left join onto location and person incase of deletions
2001-03-28 	CPSuse new application variable to display status description	
2007-05-01	NJH removed code for extra column as it didn't appear to be used anymore.
				Added checkboxes and orders functions for the orders
2011-09-12 	WAB LID 7717 problem saving temporary file to UNC path
 --->



<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
	<cf_includejavascriptonce template = "/javascript/openwin.js">	
	<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">	
	<cf_includejavascriptonce template = "/javascript/dropdownFunctions.js">	
</cf_head>

<cf_translate>
<CFINCLUDE TEMPLATE="OrdersTopHead.cfm">

<cfquery name="getExtraColumnCheck" datasource="#application.siteDataSource#">
select campaignid from promotion
where promoid in ('Evaluation2000')
	<!--- add to this list if you want the finance fields to be displayed for that promotion --->
</cfquery>

<cfif isDefined("campaignID") and listcontains(valuelist(getExtraColumnCheck.campaignid), campaignid)>
	<cfset ExtraColumns = true>
<cfelse>
	<cfset ExtraColumns = false>
</cfif>


<cfparam name="frmAction" default="">

<cfif frmAction is "update">
	<!--- update the orders table with whatever gets added into CreditOrDiscount, AccountingReferenceNumber --->
	<cfloop list="#OrderNumbers#" index="AnOrderNumber">
		<cfquery name="updateorders" datasource="#application.siteDataSource#">
		update 
			orders
		set
			<!---<cfif evaluate("AccountingReferenceNumber_#AnOrderNumber#") is not "">
				AccountingReferenceNumber =  <cf_queryparam value="#evaluate("AccountingReferenceNumber_#AnOrderNumber#")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			</cfif> --->
			AccountingReferenceNumber =  <cf_queryparam value="#evaluate("AccountingReferenceNumber_#AnOrderNumber#")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<cfif evaluate("CreditOrDiscount_#AnOrderNumber#") is not "">
				CreditOrDiscount = #evaluate("CreditOrDiscount_#AnOrderNumber#")#
			<cfelse>
				CreditOrDiscount = NULL
			</cfif>
		where
			OrderID =  <cf_queryparam value="#AnOrderNumber#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cfloop>

</cfif>

<!---Drill down by Product--->
<CFIF IsDefined("SKU") AND NOT IsDefined("OrderNumber")>

	<CFPARAM NAME="OrderNumber" DEFAULT="" >
	<CFPARAM NAME="CompanyName" DEFAULT="" >

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
		SELECT 
			1 as query1,
			Disti.SiteName as Distributor,
			Location.SiteName AS Company, 
			Orders.OrderDate, 
			OrderItem.Quantity AS NumberOfItems, 
			OrderItem.TotalDiscountPrice AS OrderValue, 
			Orders.OrderID AS OrderNumber, 
			Orders.OrderStatus AS Status,
			Orders.OrderCurrency, 
			Orders.AccountingReferenceNumber,
			Orders.CreditOrDiscount,
			orders.ShipperReference,
			UserGroup.Name, 
			Person.FirstName, 
			Person.LastName,
			Promotion.OrderNoPrefix,
			Promotion.CampaignID
		FROM 
			Promotion inner join Orders on  
			Orders.PromoID = Promotion.PromoID
			inner join orderitem on 
			Orders.OrderID = OrderItem.OrderID
			left join Location on 
			Orders.LocationID = Location.LocationID
			inner join UserGroup on 
			Orders.CreatedBy = UserGroup.UserGroupID
			left join Person on 
			Person.PersonID = Orders.PersonID 
			left join Location as Disti on
			Orders.DistiLocationID = Disti.LocationID
		WHERE 
			Orders.OrderStatus  IN ( <cf_queryparam value="#RealOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFIF NOT IsDefined("All")>
			AND OrderItem.SKU =  <cf_queryparam value="#SKU#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</CFIF>
		
			AND Promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			
			<cfif LinkCountryCombo IS NOT 0>
			AND Location.CountryID =  <cf_queryparam value="#LinkCountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			
			<cfif DateFrom IS NOT ''>
			AND Orders.OrderDate >=  <cf_queryparam value="#DateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
			
			<cfif DateTo IS NOT ''>
			AND Orders.OrderDate <=  <cf_queryparam value="#DateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
		
		ORDER BY 
			Orders.OrderDate
	</CFQUERY>
	
<!---Drill down by Status--->
<CFELSEIF IsDefined("OrderStatus")>

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
			
		SELECT 
			2 as query2,
			Disti.SiteName as Distributor,
			Orders.OrderStatus AS Status, 
			Location.SiteName AS Company, 
			Orders.OrderDate AS OrderDate, 
			Orders.OrderID AS OrderNumber, 
			Orders.OrderValue,
			
			Orders.OrderCurrency, 
			Orders.OrderStatus as Status, 
			orders.ShipperReference, 
			Orders.AccountingReferenceNumber,
			Orders.CreditOrDiscount,
			UserGroup.Name, 
			Person.FirstName, 
			Person.LastName,
			Promotion.OrderNoPrefix,
			Promotion.CampaignID
		
		FROM 
			Orders left join Location on
			Orders.LocationID = Location.LocationID
			inner join Promotion on  
			Orders.PromoID = Promotion.PromoID
			inner join UserGroup on
			Orders.CreatedBy = UserGroup.UserGroupID
			left join Person on 
			Person.PersonID = Orders.PersonID 
			left join Location as Disti on 
			Orders.DistiLocationID = Disti.LocationID
		WHERE 
			0 = 0

		<CFIF NOT IsDefined("All")>
		AND Orders.OrderStatus =  <cf_queryparam value="#OrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		
		AND Promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		
		<cfif LinkCountryCombo IS NOT 0>
		AND Location.CountryID =  <cf_queryparam value="#LinkCountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		
		<cfif DateFrom IS NOT ''>
		AND Orders.OrderDate >=  <cf_queryparam value="#DateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFIF>
		
		<cfif DateTo IS NOT ''>
		AND Orders.OrderDate <=  <cf_queryparam value="#DateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFIF>
		
		ORDER BY OrderDate DESC, Company
	
	</CFQUERY>


<!---Drill down by User--->
<CFELSEIF IsDefined("User") AND NOT IsDefined("SKU")>

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
			
		SELECT 
			3 as query3,
			Disti.SiteName as Distributor,
			Orders.OrderID AS OrderNumber, 
			Orders.OrderDate, 
			Orders.OrderValue, 
			UserGroup.Name, 
			Location.SiteName AS Company, 
			Orders.OrderStatus AS Status, 
			Orders.OrderCurrency, 
			Orders.AccountingReferenceNumber,
			Orders.CreditOrDiscount,
			orders.ShipperReference,
			Person.FirstName, 
			Person.LastName,
			Promotion.OrderNoPrefix,
			Promotion.CampaignID
		
		FROM 
			Orders left join Location on
			Orders.LocationID = Location.LocationID
			inner join UserGroup on
			Orders.CreatedBy = UserGroup.UserGroupID
			inner join Promotion on 
			Orders.PromoID = Promotion.PromoID
			left join Person on 
			Person.PersonID = Orders.PersonID 
			left join Location as Disti on
			Orders.DistiLocationID = Disti.LocationID
		WHERE 
			
			0 = 0
						 
			<CFIF NOT IsDefined("All")>
			AND Orders.CreatedBy =  <cf_queryparam value="#User#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			
			<CFIF #CampaignID# IS NOT ''>
			AND Promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFIF>
			
			<CFIF #LinkCountryCombo# IS NOT 0>
			AND Location.CountryID =  <cf_queryparam value="#LinkCountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			
			<cfif DateFrom IS NOT ''>
			AND Orders.OrderDate >=  <cf_queryparam value="#DateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
			
			<cfif DateTo IS NOT ''>
			AND Orders.OrderDate <=  <cf_queryparam value="#DateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
		ORDER BY 
			Orders.OrderDate DESC
	</CFQUERY>

<CFELSEIF IsDefined("OrderNumber")>

	<CFQUERY NAME="Sales" datasource="#application.siteDataSource#">
		SELECT distinct
			4 as query4,
			Disti.SiteName as Distributor,
			Location.SiteName AS Company, 
			Orders.OrderDate, 
			Orders.OrderValue, 
			Orders.OrderID AS OrderNumber, 
			Orders.OrderStatus, 
			Orders.DelContact AS Name,
			Orders.OrderCurrency, 
			Orders.AccountingReferenceNumber,
			Orders.CreditOrDiscount,
			orders.ShipperReference,
			Person.Firstname, 
			Person.Lastname, 
			Orders.OrderStatus as Status,
			Promotion.OrderNoPrefix,
			Promotion.CampaignID
		FROM 
			Location right join orders on 
			Orders.LocationID = Location.LocationID
			inner join promotion on promotion.promoid = orders.promoid
			inner join OrderItem on 
			Orders.OrderID = OrderItem.OrderID
			left join Person on 
			Orders.PersonID = Person.PersonID
			left join Location as Disti on
			Orders.DistiLocationID = Disti.LocationID		
		WHERE 
			
			Orders.PromoID IN (Select PromoID FROM Promotion WHERE CampaignID  IN ( <cf_queryparam value="#CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) 
			
			<CFIF #OrderNumber# IS NOT ''>
				AND Orders.OrderID =  <cf_queryparam value="#OrderNumber#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFELSEIF #CompanyName# IS NOT ''>
				AND (Location.SiteName  LIKE  <cf_queryparam value="#CompanyName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  OR Person.FirstName  LIKE  <cf_queryparam value="#CompanyName#%" CFSQLTYPE="CF_SQL_VARCHAR" >  OR Person.LastName  LIKE  <cf_queryparam value="#CompanyName#%" CFSQLTYPE="CF_SQL_VARCHAR" > )
			<CFELSEIF #SKU# IS NOT ''>
				AND OrderItem.SKU =  <cf_queryparam value="#SKU#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfif>
			<!--- NJH 2007-04-27 allow people to filter by order status--->
			<cfif isDefined("frmStatus") and frmStatus neq ""> 
				AND Orders.OrderStatus =  <cf_queryparam value="#frmStatus#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<cfelseif isDefined("realOrderStatuses") and realOrderStatuses neq "">
				AND Orders.OrderStatus  in ( <cf_queryparam value="#realOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		
		ORDER BY 
			Orders.OrderDate DESC
	</CFQUERY>

</cfif>

<!--- form used for viewing orders --->
<FORM ACTION="" NAME="viewOrder" METHOD="POST">

	<INPUT TYPE="HIDDEN" NAME="currentOrderID" VALUE="">
	<!--- <INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="frmInViewer=1"> NJH 2009-06-30 P-FNL069 --->  <!--- actually not in viewer, but gets rid of look & feel --->

</FORM>


<!---Display Orders Java Script activated by the green button on the drill-down page or 
by a straight match--->

<SCRIPT type="text/javascript">
<!--


	function displayOrder (page, orderid)  {
		form = window.document.viewOrder
		form.action = page
		form.currentOrderID.value=orderid
		form.submit()
	}				
//-->

/*<cfif ExtraColumns>

function checkNumber(theNumberField)
{
	var re = /[^0-9\.]/
	if (re.test(theNumberField.value))
	{
		alert('Please enter a valid number!')
		theNumberField.focus()
		theNumberField.select()
		return false
	}
	return true
}

function doSubmit()
{
	// check through each field which should have a number and make sure it does - at the first error, break out of the loop.
	form = document.myForm
	form.action += '?<cfoutput>#jsStringFormat(request.query_string)#</cfoutput>'
	re = /CreditOrDiscount/
	var ok = true
	for (i=0; i<form.length; i++)
	{
		if (re.test(form[i].name))
		{
			if (!checkNumber(form[i]))
			{
				ok = false
				break
			}				
		}
		
	}	
	if (ok)	
		form.submit()
}
</cfif>*/

</SCRIPT>



<!---Drill Down results display--->
<CFIF IsDefined("SKU") OR IsDefined("OrderStatus") OR IsDefined("User")>

<!--- only make it a form if ExtraColumns are used --->

<!--- <cfif ExtraColumns>
	<form name="mainForm" action="NewReportOrdersTask.cfm" method="post">
	<cfoutput>
		<input type="hidden" name="LinkCountryCombo" value="#LinkCountryCombo#">
		<input type="hidden" name="DateFrom" value="#DateFrom#">
		<input type="hidden" name="DateTo" value="#DateTo#">
		<cfif isDefined("All")>
			<input type="hidden" name="All" value="#All#">
		</cfif>
		<input type="hidden" name="frmAction" value="">
		<input type="hidden" name="CampaignID" value="#CampaignID#">
		<input type="hidden" name="OrderNumbers" value="#valuelist(sales.OrderNumber)#">
	</cfoutput>
</cfif> --->
	
<cfset downloadString="">
<cfset tab=chr(9)>
<cfset lb=chr(10)>



<CFIF Sales.RecordCount NEQ 0 AND Sales.RecordCount NEQ 1>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<!--- NJH 2007-04-25 --->
	<tr>
		<td colspan="10" align="right" nowrap><cfinclude template="orderFunctions.cfm"></td>
	</tr>
	
		<form name="mainForm" action="" method="post">

		<tr>
			<!--- CPS 28032001 use new application variable to display status description --->
			<cfoutput><th colspan="5"><CFIF IsDefined("CampaignName")>Promotion: #htmleditformat(Sales.Campaign)#<CFELSEIF IsDefined("Status")>Status: #application.StatusDescriptions[Sales.Status]#<CFELSEIF IsDefined("User")></cfif></th></cfoutput>
<!--- 			<cfoutput><td colspan="5" class="label"><CFIF IsDefined("CampaignName")>Promotion: #Sales.Campaign#<CFELSEIF IsDefined("Status")>Status: #listgetat(orderstatuslist,listfind(RealOrderStatuses, Sales.Status))#<CFELSEIF IsDefined("User")></cfif></td></cfoutput>			 --->
		</tr>

		<TR>
			<cfif isDefined("relaywareManagedCampaignIdList") and relaywareManagedCampaignIdList neq "">
				<th>&nbsp;</th>
			</cfif>
			<!--- <cfif ExtraColumns>
				<th>Distributor</th>
				<cfset downloadString = downloadString  & "distributor#tab#">
			</cfif> --->
			<Th>
				<!--- <cfif ExtraColumns>
					Partner/Reseller
					<cfset downloadString = downloadString  & "Partner/Reseller#tab#">
				<cfelse> --->
					Company
					<cfset downloadString = downloadString  & "Company#tab#">
				<!--- </cfif> --->
			</th>
			<Th>Order Placed By</th>
			<Th>Delivered To</th>
			<Th align="center">Order Date</th>
			<cfset downloadString = downloadString  & "Order Placed By#tab#Delivered To#tab#Order Date#tab#">
		
		<CFIF IsDefined("SKU") AND NOT IsDefined("OrderNumber")>
			<Th align="center">Number of Items</th>
			<cfset downloadString = downloadString  & "Number of Items#tab#">
		<CFELSEIF IsDefined("Form.OrderNumber")>
			<Th align="center">Person</th>
			<cfset downloadString = downloadString  & "Person#tab#">
		</CFIF>	
			<Th align="center">Order Value</th>
			<cfset downloadString = downloadString  & "Order Value#tab#">

		<!--- <cfif ExtraColumns>
			<Th align="center">Total Disti Price</th>
			<Th align="center">Price Difference</th>
			<cfset downloadString = downloadString  & "Total Disti Price#tab#Price Difference#tab#">

		</cfif> --->
			<Th align="center">Order Number</th>
			<Th align="center">Tracking Number</TH>
			<Th align="left">Status</th>
			<Th align="center">View Order</th>
			<cfset downloadString = downloadString  & "Order Number#tab#Tracking Number#tab#Status#tab#">

			<!--- <cfif ExtraColumns>			
				<th>Credit / Discount Paid</th>
				<Th>Accounting Reference Number</th>
				<cfset downloadString = downloadString  & "Credit / Discount Paid#tab#Accounting Reference Number#tab#">
	
			</cfif> --->

			<cfset downloadString = downloadString  & lb>
		</TR>
	
	<CFOUTPUT QUERY="Sales">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<cfif isDefined("relaywareManagedCampaignIdList") and relaywareManagedCampaignIdList neq "">
				<cfif listFind(relaywareManagedCampaignIdList,campaignID)>
					<td><CF_INPUT type="checkbox" name="frmOrdersCheck" id="#OrderNumber#" value="#OrderNumber#"></td>
				<cfelse>
					<td>&nbsp;</td>
				</cfif>
			</cfif>
			<!--- <cfif ExtraColumns>
				<cfset currentorderid = Sales.OrderNumber>
				<CFQUERY NAME="getordervalue" datasource="#application.sitedatasource#">			
					select 
						sum(quantity * unitdiscountprice) as totalvalue,
						sum(quantity * unitlistprice) as totallistvalue,
						sum(quantity * (unitlistprice * (1 - isnull(DistiDiscount,0)))) as TotalDistiValue,
						sum(totaldiscountprice) as totalvalue1,
						sum(totallistprice) as totallistvalue1,
						sum(prodweight) as totalWeight
					from 
						vorderitems as i
					where	
						i.orderid=#currentorderid#
						and	i.active=1
				</CFQUERY>
				<td>#Distributor#</td>
				<cfset downloadString = downloadString & "#Distributor##tab#">
			</cfif> --->
			<TD>#htmleditformat(Company)#</TD>
			<TD>#htmleditformat(Name)#</TD>
			<TD>#htmleditformat(FirstName)# #htmleditformat(LastName)#</TD>
			<TD align="center">#DateFormat(OrderDate, "dd-mmm-yy")#</TD>
			<cfset downloadString = downloadString & "#Company##tab##name##tab##FirstName# #LastName##tab##DateFormat(OrderDate, "dd/mm/yyyy")##tab#">
		<CFIF IsDefined("SKU") AND NOT IsDefined("OrderNumber")>
			<TD align="center">
				#htmleditformat(NumberOfItems)#
				<cfset downloadString = downloadString & "#NumberOfItems##tab#">			
			</TD>
			
		<CFELSEIF IsDefined("Form.OrderNumber")>
			<TD align="center">
				#htmleditformat(Name)#
				<cfset downloadString = downloadString & "#Name##tab#">			
			</TD>
		</CFIF>

			<TD align="right">
				<CFIF OrderValue IS NOT 0>
					#htmleditformat(OrderCurrency)# #decimalformat(OrderValue)#
					<cfset downloadString = downloadString & "#OrderCurrency# #OrderValue##tab#">			
				<CFELSE>
					N/A
					<cfset downloadString = downloadString & "N/A#tab#">			
				</CFIF>
			</TD>
			<!--- <cfif ExtraColumns>
				<TD align="right">
					#OrderCurrency# #getordervalue.TotalDistiValue#
					<cfset downloadString = downloadString & "#OrderCurrency# #getordervalue.TotalDistiValue##tab#">			
				</TD>
				
				<TD align="right">
					<cfif isNUmeric(OrderValue) and  isNUmeric(getordervalue.TotalDistiValue)>
					 	#evaluate(OrderValue - getordervalue.TotalDistiValue)#
						<cfset downloadString = downloadString & "#evaluate(OrderValue - getordervalue.TotalDistiValue)#">			
					</cfif>
						<cfset downloadString = downloadString & tab>
				</TD>
			</cfif> --->

			<TD align="center">
				#htmleditformat(OrderNoPrefix)##htmleditformat(OrderNumber)#
				<cfset downloadString = downloadString & "#OrderNoPrefix##OrderNumber##tab#">			
			</TD>
			<td>
				#ShipperReference#
				<cfset downloadString = downloadString & "#ShipperReference##tab#">		
			</td>		
			<TD align="left">
				<!--- CPS 28032001 use new application variable to display status description --->			
				#application.StatusDescriptions[Status]#
				<cfset downloadString = downloadString & "#application.StatusDescriptions[Status]##tab#">						
<!--- 				#listgetat(orderstatuslist,listfind(RealOrderStatuses,Status))#				 
				<cfset downloadString = downloadString & "#listgetat(orderstatuslist,listfind(RealOrderStatuses,Status))##tab#">	--->									
			</TD>

			<TD align="center"><A HREF="javascript:displayOrder('<CFIF listfind(SuperUserEditableOrderStatuses,status ) IS NOT 0>../orders/editorder.cfm<CFELSE>../orders/vieworder.cfm</CFIF>',#htmleditformat(ordernumber)#)"><IMG SRC="/IMAGES/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A> </TD>
			<!--- <cfif ExtraColumns>
				<cfif CreditOrDiscount is OrderValue and not isDefined("dontFreeze")>
					<td>
						#decimalformat(CreditOrDiscount)#
					</td>
					<td>
						#AccountingReferenceNumber#
					</td>
				<cfelse>
					<td>
						<input type="text" name="CreditOrDiscount_#OrderNumber#" size="5" maxlength="10" value="#iif(CreditOrDiscount is "", DE(""), DE(decimalformat(CreditOrDiscount)))#" onChange="frmAction.value = 'update'; void checkNumber(this)">
					</td>
					<td>
						<input type="text" name="AccountingReferenceNumber_#OrderNumber#" size="8" maxlength="50" value="#AccountingReferenceNumber#" onChange="frmAction.value = 'update'">
					</td>
				</cfif>

				<cfset downloadString = downloadString & "#decimalformat(CreditOrDiscount)##tab#">						
				<cfset downloadString = downloadString & "#AccountingReferenceNumber##tab#">						

			</cfif> --->
		
		</TR>
			<cfset downloadString = downloadString & lb>
	</cfoutput>
	<!--- <cfif ExtraColumns>
	<tr>
		<td colspan="11" align="right"><a href="javascript: void doSubmit()">Update</a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>		
	</cfif> --->
		<!--- LH 3149--->
		<cfset result=application.com.filemanager.getTemporaryFolderDetails()>
	
		<!--- WAB 2011-09-12 LID 7717
			replacing \\ with \ screws up UNC paths
			not sure either of these lines necessary since CFFILE does not worry which way the slashes are
			so have got rid of these lines
			if there really is a problem then should be fixed in 
			<cfset absoluteURL = replace(result.path,"/","\",'all')>
			<cfset absoluteURL = replace(absoluteURL,"\\","\",'all')>

		--->
		<cfset absoluteURL = result.path>
		<cfset relativeURL = result.webpath>

		<cffile action="WRITE" file="#absoluteURL#download.xls" output="#downloadString#" addnewline="Yes">

	<tr>
		<td colspan="11" align="left">
				<cfoutput><A HREF="#relativeURL#download.xls">Download this Table</a></cfoutput>		
		</td>
	</tr>		




	<CFELSEIF Sales.RecordCount EQ 0>
		
		No orders available. Please try changing Search Criteria. <a href="javascript:history.back();">Back</a>
	
	<CFELSEIF Sales.RecordCount EQ 1>
		
		<CFOUTPUT>
			<CFLOCATION URL="DisplayOrder.cfm?CurrentOrderID=#Sales.OrderNumber#"addToken="false">
		</CFOUTPUT>
		
		<!---<SCRIPT type="text/javascript">
			<CFOUTPUT QUERY="Sales">
			displayOrder('<CFIF listfind(SuperUserEditableOrderStatuses,orderstatus ) IS NOT 0>../orders/editorder.cfm<CFELSE>../orders/vieworder.cfm</CFIF>',#ordernumber#)
			</cfoutput>
		</SCRIPT>--->
		
<!--- NJH 2007-04-25 --->
</form>		
</TABLE>
</cfif>

<!--- <cfif ExtraColumns>
	</form>
</cfif> --->

</CFIF>


</cf_translate>


