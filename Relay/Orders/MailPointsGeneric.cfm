<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:  WAB 030200  Uses variable Delivery Address to get properly formatted address
		SSS 2007-10-30 Added the group attribute to cfmail so the email goes out correctly
 --->	

<!---Generic Mail Template--->
<CFSET ordertotal = getOrderValue.TotalValue>

<CFSET tab1 = 15>
<CFSET tab2 = 30>

<!--- have to translate these items in advance, 'cause we need to know their lengths to get the formatting right --->
<cf_translate phrases="order_OrderNumber,order_NumberOfItems,order_ProductCode,description,order_UnitPoints,order_TotalPoints,quantity"  />

<cfoutput>
<CF_MAIL query="caller.getorderitems" 
		group ="orderID"
		TO="#orderToEmail#" 
		BCC="#application.copyAllEmailsToThisAddress#"
		FROM="#application.ordersEmailFrom#"
		SUBJECT="Order Confirmation: Order No. #currentorderid#"><cf_translate>
phr_order_Thankyouforyourorder#chr(13)#

#phr_order_OrderNumber# #repeatString(" ",max(0,tab1-Len(phr_order_OrderNumber)))# #currentorderid##chr(13)#
#phr_order_NumberOfItems# #repeatString(" ",max(0,tab1-Len(phr_order_NumberOfItems)))# #getorderitems.recordcount##chr(13)#

phr_order_ItemsOrdered
================================================================================
<CFOUTPUT>
#phr_order_ProductCode##repeatString(" ",max(0,tab2-Len(phr_order_ProductCode)))##getorderitems.SKU##chr(13)#
#phr_Description##repeatString(" ",max(0,tab2-Len(phr_description)))#<cfif GetOrderHeader.TranslateDescription>phr_description_product_#productid#<cfelse>#getorderitems.Description#</cfif>#chr(13)#
#phr_Quantity##repeatString(" ",max(0,tab2-Len(phr_Quantity)))##getorderitems.Quantity##chr(13)#
#phr_Order_UnitPoints##repeatString(" ",max(0,tab2-Len(phr_order_UnitPoints)))##numberFormat(getorderitems.UnitDiscountPrice,"___,___,___.__")##chr(13)#
#phr_Order_TotalPoints##repeatString(" ",max(0,tab2-Len(phr_order_TotalPoints)))##numberFormat(getorderitems.TotalDiscountPrice,"___,___,___.__")##chr(13)##chr(13)#
</CFOUTPUT>
#phr_Order_TotalPoints##repeatString(" ",max(0,tab2-Len(phr_Order_TotalPoints)))##numberFormat(orderTotal,"___,___,___.__")##chr(13)#
================================================================================

phr_order_ShippingDetails
================================================================================
phr_order_Thisorderwillbedeliveredto:
#getorderheader.delcontact#
#getorderheader.delsitename#
#DeliveryAddress#
================================================================================
</cf_translate></CF_MAIL>
</cfoutput>
