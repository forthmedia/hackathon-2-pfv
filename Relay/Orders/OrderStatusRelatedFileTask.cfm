<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Get the Created Date --->
<CFSET freezedate=now()>
<CFSET frmMode="Approve">
<CFIF IsDefined("frmAmount") AND frmAmount NEQ ''>
	<CFSTOREDPROC PROCEDURE="RWDoConversion" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmountIn" VALUE="" NULL="Yes">
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@CashAmountIn" VALUE="#frmAmount#" NULL="No">	 
		<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="#frmPriceISOCode#" NULL="No">	 				
		<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_DECIMAL" VARIABLE="PointsAmount" DBVARNAME="@ConvertedAmount" NULL="No">	
	</CFSTOREDPROC>
</cfif>
<CFOUTPUT>Amount: #htmleditformat(frmAmount)# ISO Code: #htmleditformat(frmPriceISOCode)# Points: #htmleditformat(PointsAmount)#</CFOUTPUT>
	
<CFQUERY NAME="getFlag" datasource="#application.siteDataSource#">
	SELECT flagId 
	  FROM flag
	 WHERE flagTextId = 'AdvertLocalAgency' 
</CFQUERY>

<!--- wab: 2001-05-14    had be removed because the included template also 
has a cftransaction tag in it.
 <CFTRANSACTION>  --->

<CFIF IsDefined("frmAmount") AND frmAmount NEQ ''>
<!--- CPS 2001-05-21 Move the update statement to OrderStatusListEditTask so that it is performed 
      immediately after the update to the Order Header --->
<!--- 	<CFQUERY NAME="updateOrderItem" datasource="#application.sitedatasource#">			
		Update OrderItem 
		SET 	UnitDiscountPrice = #PointsAmount#,
		        TotalDiscountPrice = #PointsAmount#,
				UnitListPrice = #PointsAmount#,
				TotalListPrice = #PointsAmount#,
		lastUpdatedby = #request.relayCurrentUser.usergroupid#,
		lastupdated = #freezedate#
		where ItemID = #frmItemID#
	</CFQUERY> ---> 
	
	<CFSET frmPointsAmount = #PointsAmount#>
	<CFSET frmUploadLSSQuote = 1>
	
</CFIF>
	
<CFINCLUDE TEMPLATE="OrderStatusListEditTask.cfm">

<!--- </CFTRANSACTION> --->

