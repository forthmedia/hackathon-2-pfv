<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2009/09/07 LID 2515 Removed #application. userfilespath#   --->

<cfoutput>

<cfif ShowIllustrativePrice>
	<cfscript>
		IllustrativePrice = application.com.currency.convert(fromValue=#DiscountPrice#, fromCurrency="#GetProductList.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
		IllustrativePriceValue = numberformat(IllustrativePrice.tovalue,.99);
	</cfscript>
</cfif>
<script>
<cfif (ThisProductGroup is "None" or ThisProductGroup is getProductlist.ProductGroup or showallproducts)>records[records.length] = new Record(parseInt(#jsStringFormat(productID)#,10), parseInt(#iif(numberavailable gt 0,de(numberavailable),0)#,10), parseInt(#iif(MaxQuantity neq "", MaxQuantity ,0)#,10), parseInt(#iif(PreviouslyOrdered neq "",de(PreviouslyOrdered),0)#,10), parseFloat(#val(val(listprice) / PointsFactor)#,10),parseFloat(#val(val(IllustrativePriceValue) / PointsFactor)#,10), parseFloat(#val(val(discountprice) / PointsFactor)#,10), <cfif MaxQuantity is 0 or (MaxQuantity gt PreviouslyOrdered)>true<cfelse>false</cfif>,'#jsStringFormat(parentSKUGroup)#');</cfif>
</script>

	<!---<cfset pricefields = pricefields & "<INPUT TYPE=""HIDDEN"" NAME=""price_#productid#_#parentSKUGroup#"" VALUE=""#val(discountprice / PointsFactor)#"">">
	<cfset illustrativePricefields = illustrativePricefields & "<INPUT TYPE=""HIDDEN"" NAME=""illustrativeprice_#productid#_#parentSKUGroup#"" VALUE=""#val(IllustrativePrice.tovalue)#"">">
	--->
	<cfif parentSKUGroup neq 0>
		<TR id="product_#productSKUGroup#_#parentSKUGroup#" style="display:none;">
	<cfelse>
		<TR>
	</cfif>
	<CFIF ShowPictures>
		<TD ALIGN="center" VALIGN="top">
		<cfif UseGlobalSKU>
			<cfset imageCountryID = 0>
		<cfelse>
			<cfset imageCountryID = countryID>
		</cfif>
		<cfif isDefined("debug") and debug is "yes">
			#application.paths.content#\productThumbs\CountryID#imageCountryID#\#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg
		</cfif>
		<cfif fileexists("#application.paths.content#\productThumbs\CountryID#imageCountryID#\#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg")>
			<!--- WAB 2009/09/07 LID 2515 Removed #application. userfilespath#   --->
			<IMG alt="#SKU# Image" src="/content/productThumbs/CountryID#imageCountryID#/#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg" BORDER=0>
		<cfelse>
			&nbsp;
		</cfif>
		</TD>		
	</CFIF>
	<cfif ShowProductCode or ShowProductCodeAsAnchor>								
		<TD ALIGN="left" VALIGN="top" nowrap>
			<cfif OrderSingleProduct>
				<a href="javascript:chooseProduct(#productid#)">#htmleditformat(SKU)#</a>
			<cfelse>
				<cfif NOOFCHILDREN neq 0>
					<a href="javascript:displayaccessories('#productSKUGroup#','#parentSKUGroup#')"><strong>#htmleditformat(SKU)# +</strong></a>
				<cfelse>
					<cfif parentSKUGroup is 0>
						<strong>#htmleditformat(SKU)#</strong>
					<cfelse>
						<div style="margin-left:37; white-space:no-wrap;">#htmleditformat(SKU)#</div>
					</cfif>
				</cfif>
			</cfif>
		</td>
	</cfif>
	<cfif ShowDescription>
		<TD ALIGN="left" VALIGN="top">
			<cfif TranslateDescription>
				<cfif OrderSingleProduct><a href="javascript:chooseProduct(#productid#)"><P>phr_Description_Product_#htmleditformat(ProductID)#</P></a><cfelse><P>phr_Description_Product_#htmleditformat(ProductID)#</P></cfif>
			<cfelse>
				<cfif OrderSingleProduct><a href="javascript:chooseProduct(#productid#)"><P>#htmleditformat(Description)#<P></a><cfelse><P>#htmleditformat(Description)#</P></cfif> 
			</cfif>
			
			<cfif isDefined("cookie.translator") and cookie.translator is not "">
				<a href="javascript: editProduct(#productid#, '#ThisProductGroup#')">...</a>
			</cfif>
		</TD>						
	</cfif>
	<CFIF ShowListPrice>
		<TD ALIGN="right" VALIGN="top" NOWRAP>
			#decimalformat(ListPrice)#
		</TD>
	</cfif>
	<CFIF ShowDiscountPrice>
		<TD ALIGN="right" VALIGN="top">
			#decimalformat(DiscountPrice)#
		</td>
	</cfif>
	<CFIF ShowIllustrativePrice>
		<cfscript>
			IllustrativePrice = application.com.currency.convert(fromValue=#DiscountPrice#, fromCurrency="#GetProductList.PriceISOCode#", toCurrency="#toCurrency.countrycurrency#");
		</cfscript>
		<TD ALIGN="right" VALIGN="top" NOWRAP>
			#decimalformat(IllustrativePrice.tovalue)#
		</TD>
	<cfelse>
		<cfset IllustrativePrice=0>
	</cfif>
	<CFIF ShowNoInStock>
		<TD ALIGN="left" VALIGN="top">
			#htmleditformat(NumberAvailable)#
		</TD>
	</CFIF>

<!---  work out quantity of current item ordered --->
<CFIF listfind(currentorderitemsSKU_ParentSKUGroup,"#SKU#_#ParentSKUGroup#") is not 0>
	<CFSET quantityofCurrentItem = listgetat(currentorderitemsquantity, listfind(currentorderitems,productid))>
<CFELSE>
	<CFSET quantityofCurrentItem = 0>
</cfif>

<cfif ShowMaxQuantity>
	<TD ALIGN="left" VALIGN="top">
		#iif(MaxQuantity is 0,DE(""),MaxQuantity )#
	</td>
</cfif>
<cfif ShowPreviouslyOrdered>
	<TD ALIGN="left" VALIGN="top">
		#htmleditformat(GetProductList.PreviouslyOrdered)#
	</td>
</cfif>

<cfif (ThisProductGroup is "None" or ThisProductGroup is getProductlist.ProductGroup or showallproducts) and ShowQuantity and (MaxQuantity GT PreviouslyOrdered or MaxQuantity is 0 )>
	<!--- only display text if we're on appropriate group, or groups not used --->
	<TD ALIGN="center" VALIGN="top"><INPUT class="form-control" TYPE="text" NAME="quant_#productid#_#parentSKUGroup#" value="<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentSKUGroup#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>" size="4" maxlength="4" onFocus="valueIs = this.value; fieldIs = #htmleditformat(productID)#; this.select()" onBlur="fieldWas = #htmleditformat(productID)#; doLineTotal(this, #htmleditformat(productID)#, false,'#htmleditformat(parentSKUGroup)#')" onChange="dirty.value = true" style="text-align:right;"></td>
	<cfif ShowIllustrativePrice>
		<TD ALIGN="right" VALIGN="top">
			<!---<input type="text" name="line_#productid#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#")>#evaluate("form.line_#productid#")#</cfif>" onFocus="fieldIs = #productID#; doLineTotal(quant_#productid#, #productID#)"> --->
			<input class="form-control" type="text" name="lineIllustrative_#productid#_#parentSKUGroup#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.lineIllustrative_#productid#_#parentSKUGroup#")>#evaluate("form.lineIllustrative_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>" onFocus="fieldIs = #htmleditformat(productID)#; doNavigate(this)" DISABLED style="text-align:right;">
		</td>
	<cfelse>
		<input type="hidden" name="lineIllustrative_#productid#_#parentSKUGroup#" value="<cfif isDefined("url.updatebasket") and isdefined("form.lineIllustrative_#productid#_#parentSKUGroup#")>#evaluate("form.lineIllustrative_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>">
	</cfif>
	<cfif ShowLineTotal>
		<TD ALIGN="right" VALIGN="top">
			<!---<input type="text" name="line_#productid#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#")>#evaluate("form.line_#productid#")#</cfif>" onFocus="fieldIs = #productID#; doLineTotal(quant_#productid#, #productID#)"> --->
			<input class="form-control" type="text" name="line_#productid#_#parentSKUGroup#" size="7" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentSKUGroup#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>" onFocus="fieldIs = #htmleditformat(productID)#; doNavigate(this)" DISABLED style="text-align:right;">
		</td>
	<cfelse>
		<input type="hidden" name="line_#productid#_#parentSKUGroup#" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentSKUGroup#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>">
	</cfif>
<cfelse>						
	<!--- debug information!
	<td>
	ThisProductGroup: #ThisProductGroup#<br>
	getProductList.ProductGroup: #getProductList.ProductGroup#<br>
	ShowQuantity: #ShowQuantity#
	MaxQuantity: #MaxQuantity#
	PreviouslyOrdered: #PreviouslyOrdered#
	</td>
	--->
	<input type="hidden" name="line_#productid#_#parentSKUGroup#" value="<cfif isDefined("url.updatebasket") and isdefined("form.line_#productid#_#parentSKUGroup#")>#evaluate("form.line_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>">
	<input type="hidden" name="lineIllustrative_#productid#_#parentSKUGroup#" value="<cfif isDefined("url.updatebasket") and isdefined("form.lineIllustrative_#productid#_#parentSKUGroup#")>#evaluate("form.lineIllustrative_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#</cfif>">
	<INPUT TYPE="hidden" NAME="quant_#productid#_#parentSKUGroup#" value= "<cfif isDefined("url.updatebasket") and isdefined("form.quant_#productid#_#parentSKUGroup#")>#evaluate("form.quant_#htmleditformat(productid)#_#htmleditformat(parentSKUGroup)#")#<cfelse>#htmleditformat(quantityofcurrentitem)#</cfif>">
</cfif>

<CFSET SKUsonpage=listappend(SKUsonpage,#productid#)>
<CFSET ProductSKUsonpage=listappend(ProductSKUsonpage,#SKU#)>	
<CFSET SKUsParentsonpage=listappend(SKUsParentsonpage,#parentSKUGroup#)>				
</tr>
</cfoutput>

