<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
--->
<cfif not isDefined("url.promoid")>
	<cfoutput>For this application to work you must specify the value of PromoID.</cfoutput>
	<CF_ABORT>
</cfif>

<CFQUERY NAME="getCountry" datasource="#application.siteDataSource#">
	Select c.isocode
	from location as l, person as p, country as c
	where l.locationid = p.locationid
	and 	l.countryid = c.countryid
	and p.personid = #request.relayCurrentUser.personid#
</CFQUERY>

<CFIF isdefined("frmprocessid")>
	<cfset application.com.globalFunctions.cfcookie(name="process", value="#frmProcessId#-#frmStepId#")>
</cfif>


<cf_translate>
<cf_head>
<cf_title><cfoutput>phr_Order_Title#htmleditformat(promoid)#</cfoutput></cf_title>

</cf_head>

<!-- start Navigation Banner Table -->

<cf_displayBorder>
<CFSETTING enablecfoutputonly="no">

<TABLE align="left">
	<tr>
		<td>&nbsp;&nbsp;</td>
	</tr>

	<tr>
		<td><h1><cfoutput>phr_Order_Welcome_Heading#htmleditformat(promoid)#</cfoutput></h1></td>
	</tr>
	<TR>
		<td><CFOUTPUT>phr_Order_Welcome_Para1#htmleditformat(PromoID)#</CFOUTPUT></td>
	</tr>

	<tr>
		<td align="center">
			<cfinclude template="CategoryList.cfm">
		</td>
	</tr>

</table>

</cf_DisplayBorder>



</cf_translate>

