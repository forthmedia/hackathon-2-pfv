<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			createProdEvalSpreadsheet.cfm
Author:			DJH
Date created:	

Description:

Version history:
1

--->




<cf_head>
	<cf_title>ProdEval Spreadsheet</cf_title>
</cf_head>



<cfquery name="getcampaignid" datasource="#application.siteDataSource#">
select 
	campaignid
from
	promotion 
where
	promoid = 'Evaluation2000'
</cfquery>

<cfset campaignid = "#getCampaignID.campaignid#">
<cfset filename = "#expandpath("ProdEvalSpreadsheetData.txt")#">
<cfset filename2 = "#expandpath("ProdEvalCountries.txt")#">
<cfset filename3 = "#expandpath("ProdEvalCheckData.txt")#">
<cfset filename4 = "#expandpath("TradeUpData.txt")#">
<cfset filename5= "#expandpath("TradeUpPartnerDiscount.txt")#">
<cfset filename6= "#expandpath("TradeUpManufacturerDiscount.txt")#">
<cfset filename7= "#expandpath("TradeUpMaxPercent.txt")#">

<cfoutput>
<a href="#getfilefrompath(filename)#">#htmleditformat(filename)#</a><br>
<a href="#getfilefrompath(filename2)#">#htmleditformat(filename2)#</a><br>
<a href="#getfilefrompath(filename3)#">#htmleditformat(filename3)#</a><br>
<a href="#getfilefrompath(filename4)#">#htmleditformat(filename4)#</a><br>
<a href="#getfilefrompath(filename5)#">#htmleditformat(filename5)#</a><br>
<a href="#getfilefrompath(filename6)#">#htmleditformat(filename6)#</a><br>
<a href="#getfilefrompath(filename7)#">#htmleditformat(filename7)#</a><br>

</cfoutput>
<cfquery name="output" datasource="#application.siteDataSource#">
select distinct
	prd.sku,
	prd.description,
	pg.description as productgroup,
	prd.maxquantity,
	prd.distidiscount,
	prd.resellerdiscount,
	prd.priceisocode,
	prd.listprice,
	prd.discountprice,
	c.countrydescription
from 
	product as prd,
	productgroup pg,
	country as c
where
	prd.productgroupid = pg.productgroupid
	and c.countryid = prd.countryid
	and prd.campaignid =  <cf_queryparam value="#campaignid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and deleted = 0
order by
	prd.sku,
	prd.distidiscount,
	prd.resellerdiscount
</cfquery>

<cfquery name="TradeUpoutput" datasource="#application.siteDataSource#">
	select 
		SKU, 
		Description, 
		(select 
			validvalue 
		from 
			validfieldvalues 
		where 
		fieldname 
			LIKE 'TradeUp.ProductCategory%' 
			AND Product.ProductType = ValidFieldValues.DefaultFieldValue
		) AS ProductGroup,
		ListPrice
		
	from 
		Product
	where 
		CAMPAIGNID = 24
		
	order by 
		ProductType, 
		SKU, 
		Description
</cfquery>

<cfquery name="TradeUpPartnerDiscount" datasource="#application.siteDataSource#">
SELECT 
	validvalue,
	score,
	(select 
		validvalue 
	from 
		validfieldvalues as v2
	where 
		v2.fieldname LIKE 'TradeUp.ProductCategory%' 
		AND v2.DefaultFieldValue = V.DefaultFieldValue
	) AS ProductGroup
FROM 
	ValidFieldValues as v
WHERE 
	ValidValue like 'focus%'
	AND DefaultFieldValue in (1,2,3,4)
order by
	v.validvalue,
	v.defaultfieldvalue
</cfquery>

<cfquery name="TradeUpManufacturerDiscount" datasource="#application.siteDataSource#">
SELECT 
	validvalue,
	Score,
	(select 
		validvalue 
	from 
		validfieldvalues as v2
	where 
		v2.fieldname LIKE 'TradeUp.ProductCategory%' 
		AND v2.DefaultFieldValue = V.DefaultFieldValue
	) AS ProductGroup
FROM 
	ValidFieldValues as v
WHERE 
	v.FieldName = 'TradeUp.Manufacturer' 
	AND v.DefaultFieldValue in (1,2,3,4)
order by
	v.validvalue,
	v.defaultfieldvalue
</cfquery>

<cfquery name="TradeUpMaxPercent" datasource="#application.siteDataSource#">
SELECT 
	Score,
	(select 
		validvalue 
	from 
		validfieldvalues as v2
	where 
		v2.fieldname LIKE 'TradeUp.ProductCategory%' 
		AND v2.DefaultFieldValue = V.DefaultFieldValue
	) AS ProductGroup

FROM 
	ValidFieldValues as v
WHERE 
	FieldName = 'TradeUp.MaxPercent'
	AND DefaultFieldValue in (1,2,3,4)

</cfquery>

<!--- output a header --->
<cfset data="SKU	Product Group	Description	MaxQuantity	DistiDiscount	ResellerDiscount	USD	EURO	DEM	FFR	GBP	BEF	NLG	ESP	SEK	ITL	DKK	SKK	HUF	PLN	ATS	CHF	BGL	Note">

<!--- create an array to hold the various currency prices --->
<cfset arrdata = ArrayNew(1)>
<cfset val = ArrayClear(arrdata)>
<cfset val = ArraySet(arrdata, 1, 17, 0)>

<!--- create an associative array to know where to outpuyt the various currencies.--->
<cfset tabpos = structnew()>
<CFSET val=StructInsert(tabpos, "USD", 1)>
<CFSET val=StructInsert(tabpos, "EURO", 2)>
<CFSET val=StructInsert(tabpos, "DEM", 3)>
<CFSET val=StructInsert(tabpos, "FFR", 4)>
<CFSET val=StructInsert(tabpos, "GBP", 5)>
<CFSET val=StructInsert(tabpos, "BEF", 6)>
<CFSET val=StructInsert(tabpos, "NLG", 7)>
<CFSET val=StructInsert(tabpos, "ESP", 8)>
<CFSET val=StructInsert(tabpos, "SEK", 9)>
<CFSET val=StructInsert(tabpos, "ITL", 10)>
<CFSET val=StructInsert(tabpos, "DKK", 11)>
<CFSET val=StructInsert(tabpos, "SKK", 12)>
<CFSET val=StructInsert(tabpos, "HUF", 13)>
<CFSET val=StructInsert(tabpos, "PLN", 14)>
<CFSET val=StructInsert(tabpos, "ATS", 15)>
<CFSET val=StructInsert(tabpos, "CHF", 16)>
<CFSET val=StructInsert(tabpos, "BGL", 17)>

<cftry>
<cffile action="WRITE" file="#filename#" output="#data#">
<cffile action="WRITE" file="#filename2#" output="Country	Currency Code">
<cfset data="SKU	Product Group	Description	MaxQuantity	DistiDiscount	ResellerDiscount	ISO	ListPrice	DiscountPrice">
<cffile action="WRITE" file="#filename3#" output="SKU	ProductGroup	DistiDiscount	ResellerDiscount	PriceISOCode	ListPrice	DiscountPrice">
<cffile action="WRITE" file="#filename4#" output="SKU	Description	ProductType	ListPrice">
<cffile action="WRITE" file="#filename5#" output="PartnerType	Percent	ProductGroup">
<cffile action="WRITE" file="#filename6#" output="Manufacturer	Percent	ProductGroup">
<cffile action="WRITE" file="#filename7#" output="Percent	ProductGroup">

	<cfcatch type="Any">
		<cfoutput>
			<h1>An error was encountered trying to write to one or more of the files - 
			please make sure the file is not open in another application - i.e. MS Excel.</h1>
			<CF_ABORT>
		</cfoutput>
	</cfcatch>
</cftry>

<cfloop query="TradeUpOutput">
	<cffile action="append" file="#filename4#" output="#SKU#	#Description#	#ProductGroup#	#ListPrice#">
</cfloop>

<cfloop query="TradeUpPartnerDiscount">
	<cffile action="append" file="#filename5#" output="#validvalue#	#score#	#productgroup#">
</cfloop>

<cfloop query="TradeUpManufacturerDiscount">
	<cffile action="append" file="#filename6#" output="#validvalue#	#score#	#productgroup#">
</cfloop>

<cfloop query="TradeUpMaxPercent">
	<cffile action="append" file="#filename7#" output="#score#	#productgroup#">
</cfloop>


<cfset thissku = output.sku>
<cfset thisdistidiscount = output.distidiscount>
<cfset thisresellerdiscount = output.resellerdiscount>
<cfset notefield = "">
<cfif thisdistidiscount - thisresellerdiscount is not 0.05>
	<cfset notefield = "Special discount level for #output.countrydescription#">
</cfif>


<cfset data = "#output.sku#	#output.productgroup#	#output.description#	#evaluate(output.MaxQuantity)#	#output.DistiDiscount#	#output.ResellerDiscount#">

<cfloop query="output">
	<!--- add a line to the check file --->
	<cffile action="append" file="#filename3#" output="#SKU#	#ProductGroup#	#DistiDiscount#	#ResellerDiscount#	#PriceISOCode#	#ListPrice#	#DiscountPrice#">
	<!--- if this is a new sku or a different discount, need to start a new line --->
	<!--- need to identify where to place the currency - can't just assume that the rows go next to each other. --->
	<cfif output.sku is thissku and output.distidiscount is thisdistidiscount and output.resellerdiscount is thisresellerdiscount>
		<!--- continuation of the same line --->
		<!--- set the data --->
	<cfelse>
		<cfoutput> <b>changing to: #htmleditformat(output.sku)# #htmleditformat(output.distidiscount)# #htmleditformat(output.resellerdiscount)#</b><br></cfoutput>
		<!--- firstly, output the contents of the previous line, and clear the data out --->
		<cfloop from="1" to="17" index="i">
			<cfset data=data & "	#arrdata[i]#">
		</cfloop>
		<cfset data = data & "	#notefield#">
		<!--- now append the line to the data file ---->
		<cffile action="append" file="#filename#" output="#data#">
		
		<!--- start of a new line --->
		<cfset data = "#output.sku#	#output.productgroup#	#output.description#	#evaluate(output.MaxQuantity)#	#output.DistiDiscount#	#output.ResellerDiscount#">

		<!--- reset the data --->
		<cfset val = ArrayClear(arrdata)>
		<cfset val = ArraySet(arrdata, 1, 17, 0)>

		<cfset notefield = "">
		<cfset thissku = output.sku>
		<cfset thisdistidiscount = output.distidiscount>
		<cfset thisresellerdiscount = output.resellerdiscount>
		<cfoutput>:::(thisdistidiscount - thisresellerdiscount): #val(thisdistidiscount - thisresellerdiscount)# #output.countrydescription# <br></cfoutput>
		<cfset difference = val(thisdistidiscount) - val(thisresellerdiscount)>
		<cfset check = val(difference) - (0.05)>
		<cfset check2 = 0.05 - val(difference)>
		<cfoutput>
			<h1>difference: #difference#<br>			
			check: #check#
			check2: #check#
			</h1>
		</cfoutput>
		<cfif val(difference) neq val(0.05)>
			<cfoutput>ThisDistiDiscount: #val(thisdistidiscount)# ThisResellerDiscount: #val(ThisResellerDiscount)#... |#evaluate("val(thisdistidiscount)- val(thisresellerdiscount)")#| Is it not equal to 0.05? #evaluate("(val(thisdistidiscount)- val(thisresellerdiscount)) neq val(0.05)")# </cfoutput>
			<cfoutput>setting note to: Special discount level for #output.countrydescription# #val(thisdistidiscount - thisresellerdiscount)#<br> </cfoutput>
			<cfset notefield = "Special discount level for #output.countrydescription#">
		</cfif>
	</cfif>

	<cfif arrdata[structfind(tabpos,"#output.priceisocode#")] is 0>
		<cfset arrdata[structfind(tabpos,"#output.priceisocode#")] = output.listprice>
	</cfif>

	<cfoutput>
	#output.sku#	#output.productgroup#	#output.description#	#evaluate(output.MaxQuantity)#	#output.DistiDiscount#	#output.ResellerDiscount# #output.priceisocode# #output.listprice#<br>
	<br>
	Array looks like this:
	<cfloop from="1" to="17" index="i">
		#arrdata[i]#	
	</cfloop><br>
	Notefield: #htmleditformat(notefield)#<br>
	thisdistidiscount: #htmleditformat(thisdistidiscount)#<br>
	thisresellerdiscount: #htmleditformat(thisresellerdiscount)#<br>
	</cfoutput>
</cfloop>

Also need to create a file with a list of all the countries.

<cfquery name="prodevalcountries" datasource="#application.siteDataSource#">
select distinct
	countrydescription,
	product.priceisocode
from
	product inner join
	country on product.countryid = country.countryid
where
	product.campaignid =  <cf_queryparam value="#campaignid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>
<cfloop query="ProdEvalCountries">
	<cffile action="append" file="#filename2#" output="#CountryDescription#	#priceisocode#">
</cfloop>




