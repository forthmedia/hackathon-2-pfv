<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			CategoryList.cfm
Author:			DJH
Date created:	8 June 2000

Description:	displays the appropriate products for a category - 
				used to be part of orderitems.cfm.

Version history:
1

Amendment history
CPS 2001-03-23	Amendments to ensure orders for points based orders are created correctly
WAB 2001-04-26		WAB moved getProductList to a separate template
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate 
--->

<!--- these only set for update, so need defaults for new--->
<CFPARAM name="currentorderitems" default="">
<CFPARAM name="orderlastupdated" default="">

<!--- do some  rudimentary browser sniffing - don't worry about full versions - v4 checking is done later on --->
<cfparam name="cgi.http_user_agent" default="">
<cfif cgi.http_user_agent contains "MSIE">
	<!--- internet explorer --->
	<cfif cgi.http_user_agent contains "MSIE 4.0">
		<cfset browser= "IE4">
	<cfelseif cgi.http_user_agent contains "MSIE 5">
		<cfset browser = "IE5">	
	</cfif>
<cfelse>
	<!--- navigator --->
	<cfset browser = "Netscape">	
</cfif>


<!--- get promo name --->
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
	select 	* from promotion where promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>


<CFIF listFind(CampaignIDAllowed, getOrderHeader.CampaignID) is 0>
	<P>Sorry, you do not have rights to this Promotion</P>
<CFELSE>


	<!--- 	Decide the id of the person placing the order
	Need to be able to handle both web users placing own orders and telesales using viewer to place other people's orders
	Needs improvement. --->
	
	<CFIF not isdefined("frmPersonID") and not isdefined("frmLocationID") >
	
		<CFIF isdefined("frmEntityType")>
			<CFIF frmEntityType is "person">
				<CFSET frmPersonID = frmCurrentEntityID>
			<CFELSEIF frmEntityType is "location">
				<CFSET frmLocationID = frmCurrentEntityID>
			</CFIF>
		<CFELSE>
			<CFSET frmPersonID = #request.relayCurrentUser.personid#>
		</CFIF>
	</CFIF>
	
	
	<!--- note that this query can return multiple records if a country is in multiple regions --->
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
		select 	
			p.firstname,
			p.lastname,
			p.personid,
			p.locationid,
			l.sitename,
			l.countryid			
		from
			person as p, 
			location as l
		Where
			<CFIF isdefined("frmPersonid")>
				p.personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<cfelseif isdefined("frmLocationid")>
				l.locationid =  <cf_queryparam value="#frmlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			and	p.locationid=l.locationid	
	</CFQUERY>
	
	
	<CFIF getpersondetails.recordcount IS 0>
		Could not find person
		<CF_ABORT>
	</cfif>
	
	
	<CFSET currentcountry=getpersondetails.countryid>
	<CFSET currentlocationid=getpersondetails.locationid>

	<!--- get all active products --->
	<!--- WAB 2001-04-26 moved this query to a separate template --->
	<cfset evaluateMaxQuant = false>
	<CFINCLUDE template = "qryGetProductList.cfm">
	

	
	<script language="JavaScript">
	
		function changeProductGroup1(aproductgroup)
		{
			var now = new Date()
			now = now.getTime()
	
			if (document.mainForm != null)
			{
				document.mainForm.action = "<cfoutput>/orders/orderitemstask.cfm?ShowProductGroup=1&ThisProductGroup=" + escape(productgroup) + "&PromoID=#jsStringFormat(promoid)#</cfoutput>&updatebasket=1&d=" + now
				document.mainForm.submit()
			}
			else
			{
				window.location.href = "<cfoutput>/orders/orderitems.cfm?ShowProductGroup=1&ThisProductGroup=" + escape(aproductgroup) + "&PromoID=#jsStringFormat(promoid)#&d=</cfoutput>" + now 			
			}				
		}			
	
	</script>
	<cf_translate>
	<table border="0" cellpadding="4" cellspacing="4">
	
		<!--- display the groups that are available for the products within this promotion --->
		<cfoutput query="getProductGroupID" group="ProductGroupID">
			<cfif getOrderHeader.TranslateDescription>
				<tr>
					<td><a href="javascript:changeProductGroup1('#ProductGroup#')">phr_Description_ProductGroup_#htmleditformat(ProductGroupID)#</a></td>
					<!--- 
					make sure this is replaced later on - could use related file to upload a
					graphic related to the promotion?
					--->					
<!--- 					<td><a href="javascript:changeProductGroup1('#ProductGroup#')"><img src="select.gif" border="0"></a></td> --->
				</tr>

			<cfelse>
				<tr>
					<td><a href="javascript:changeProductGroup1('#ProductGroup#')">#htmleditformat(ProductGroup)#</a></td>
					<!--- 
					make sure this is replaced later on - could use related file to upload a
					graphic related to the promotion?
					--->					
<!--- 					<td><a href="javascript:changeProductGroup1('#ProductGroup#')"><img src="select.gif" border="0"></a></td> --->
				</tr>
			</cfif>
		</cfoutput>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<CFIF getOrderHeader.ShowPreviousOrdersLink>
		<tr>
			<td><A HREF="displayorderhistory.cfm?frmlocationid=<CFOUTPUT>#htmleditformat(currentlocationid)#&promoID=#htmleditformat(promoID)#</CFOUTPUT>&ShowProductGroupScreen=0">phr_Order_DetailsOfPreviousOrders</A></td>
		</tr>
		</CFIF>
		<CFIF getOrderHeader.ShowTermsConditions>
		<tr>
			<td><a href="javascript:void(window.open('#thisdir#/TermsConditions.cfm?promoid=<cfoutput>#htmleditformat(promoID)#</cfoutput>','','width=600,height=300,status,scrollbars,resizable<cfif browser is "IE4">,menubar</cfif>'))">phr_Order_ViewTermsConditions</a>
			</td>
		</tr>
		</CFIF>
	</table>
	</cf_translate>

</cfif>  <!--- end of rights to this Promotion --->
