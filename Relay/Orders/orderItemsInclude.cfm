<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			/Orders/OrderItems.cfm
Author:			?, AH, DJH
Date created:	?
				17 May 2000

Description:	

Used within the orders system to display a list of products pertinent to a promotion.


Amendment history
wab 1999-06-01 added a non comma delimiter to DisallowedSKUs decause some skus have commas in them
wab 1999-06-01 changed various listcontains to  listfind
dc 1999-09-17 Changed how all order, order items and product tables work for client logic integration.
				 All order and Product queries now use views. Also added formatting for java script total 
				 box along with product prices currency.
2000-05-17 - DJH	Changing the orders system to start using a shopping basket to hold the order items placed.
			Also, products within promo will be broken down to view one category at a time.
2000-12-22 - Assorted changes have been made to get the orders system working with InFocus - latest one is to prevent extra 
space appearing above the table in i.e. which is caused by haveing hidden fields within a table but not within a td
01 Mar 2001  	DAM made mods so that max quantity can be overridden on a location by location basis
CPS 2001-03-23	Amendments to ensure orders for the 'InFocusIncentiveScheme' are created correctly
2001-04-05		WAB		made modifications to DAM mods of 01 Mar 2001 - bug .  Now I loop through the product list first of all setting and evaluating the maxquantity
2001-04-05		CPS		corrected query to get focus level so that it is based on the order locationid rather than the person's cookie, added include of 	SetFocusLevel.cfm
2001-04-05		WAB		corrected problem when editing an order - brought back a blank productgroup
2001-04-26		WAB 	moved getProductList to a separate template 
2001-05-09	CPS		Problems with rounding errors when dealing with 'Points' campaigns 
2002-09-03 	SWJ 	added the query to getOrgID
July/04		SWJ		modified to be an included file so that we can call it from relayTags
2005-03-30	RND		removed variable scope from 'ThisProductGroup'
2008-07-09	AJC	Issue 695: Sony Rewards Gift Catalogue Category drop down broken - Required .value
2009/02/25 SSS    CR-Lex592 Switch of some columns if you are only in a single buyer mode
2009/02/25 SSS    CR-Lex592 Added the ability to allow anybody to see how many points a company has not just registered people
2009/05/11 SSS  cr-sny673 Added a new translation for the new category
2009/05/11 SSS  cr-sny673 moved the points code higher up so that we could use it in the query builder page
2009/05/26 SSS  cr-sny673-1 Added a default param in order to but the affordable select option to the top or bottom of the drop down
2009/07/02 NJH	P-FNL069 - get user info from currentStruct rather than cookie; encrypt hidden fields
2010/11/09 PPB	LID4607 - defend against CurrencyPerPoint=NULL in table RWPointsConversion 
				NB.setting PointsFactor=1 is not a safe fix because the currencyvalue is not necessarily 1, so I display an error (for case where there is no RWPointsConversion row too) 
2010/11/23	WAB	Remove a CFAbort from the code and replace with an if statement.  Allows full borderset to show even if a "No rights" message is displayed
2011/08/16	PPB	REL109 added classes/ids to allow the styling to be customised 
2012-04-18	WAB CASE 427705 altered some JS which still referred to hidden fields "price_...."
Enhancements: 

--->


<!---  --->
<cfparam name="UseParenting" default="0">
<cfparam name="projectID" default="0">
<cfparam name="showProgChampredemption" default="false">
<cfparam name="phraseprefix" default="">
<cfparam name="showallproducts" default="0">
<cfparam name="MinClaimAmount" default="0">
<cfparam name="OrderClaimRestrictions" default="0">
<!--- 2009/03/05 SSS added for pagination - param set in incentiveINI.cfm--->
<cfparam name="NumberPrizesPerPage" default="99">
<cfparam name="prodStartRow" default="1">
<!---Start 2009/05/26 SSS affordable will be at the bottom override is in incentiveINI.cfm--->
<cfparam name="Affordabletop" default="false">
<cfparam name="ShowAffordable" default="false"> <!--- NJH 2009/07/21 P-SNY047 don't want affordable showing on training orders catalogue. --->
<!---END 2009/05/26 SSS affordable will be at the bottom override is in incentiveINI.cfm--->

<!--- GCC TEMP until the Bibby person structure hits live --->
<!--- NJH 2009/07/02 P-FNL069 - get the countryID from the current user --->
<cfscript>
	//thisUsersDetails = application.com.commonQueries.getFullPersonDetails(request.relayCurrentUser.personid);
	//countryID = thisUsersDetails.countryID;
	countryID = request.relayCurrentUser.countryID;
		if (countryID eq 8){ 
		oldLocale = setLocale("French (Standard)");
		}
		else if (countryID eq 4){ 
		oldLocale = setLocale("German (Standard)");
		}
		else if (countryID eq 11){ 
		oldLocale = setLocale("Italian (Standard)");
		}
		else if (countryID eq 13){ 
		oldLocale = setLocale("Dutch (Standard)");
		}		
		else if (countryID eq 6){ 
		oldLocale = setLocale("Spanish (Standard)");
		}
		else {
		oldLocale = setLocale("English (UK)");
		}
</cfscript>

<cfif structKeyExists(form, "ThisProductGroup")>
	<cfset ThisProductGroup = form.ThisProductGroup>
<cfelseif structKeyExists(url, "ThisProductGroup")>
	<cfset ThisProductGroup = url.ThisProductGroup>
<cfelse>
	<cfif isdefined('ThisProductGroup')>
		<cfset form.ThisProductGroup = ThisProductGroup>
	<cfelse>
		<cfset form.ThisProductGroup = "none">
	</cfif>
	<!--- RND 2005-03-30 Commented out <cfset form.ThisProductGroup = variables.ThisProductGroup> --->
</cfif>

<cfif structKeyExists(form, "productGroupSelect") and form.productGroupSelect and structKeyExists(form, "ThisProductGroup1")>
	<cfset ThisProductGroup = form.ThisProductGroup1>
</cfif> 


<!--- Need to check with Will how to decide whether to turn this on or not. --->

<cfparam name="frmInViewer" default="1">

<!--- DJH 2000-12-22 - stop extra space code ---->
<cfparam name="pricefields" default="">
<cfparam name="illustrativepricefields" default="">

				 
<!--- <CFSET maxordval = 1000> --->
<!--- <CFSET maxordno = 3>  --->

<!--- This variable counts the number of previously ordered items --->
<CFSET tmpNoOfPrevOrders = 0>

<cfoutput><SCRIPT type="text/javascript" src="/javascript/dotranslation.js"></script></cfoutput>
<!---Translations--->
<!--- <cfinclude template="OrdersGeneralPhrases.cfm">
 --->
<cfif isdefined("frmpromoid")>
	<CFSET PromoID = frmpromoid>  <!---  provides backwards compatability--->
</cfif>
<Cfparam name="frmPromoID" default="">
<cfparam name="PromoID" default="#frmPromoID#">


<cfif isDefined("promotion")>
	<cfset PromoID = ListGetAt(Promotion, 2)>
</cfif>

<!--- decipher any parameters passed in frmGlobalParameters (for viewer)--->
<!--- NJH 2009/06/30 P-FNL069 - commented out as file is removed due for security reasons
<cfinclude template="\screen\setParameters.cfm"> --->

<cfsetting enablecfoutputonly="Yes"> 

<CFPARAM Name="frmTask" Default="New">
<CFPARAM Name="currentorderid" Default="0">
<CFPARAM name="msg" default="">


<!---
<CFIF frmtask IS "Accept">		 accept a previous quote - just update order status
	<CFINCLUDE Template="ordertask.cfm">
	<CF_ABORT>
</CFIF>
--->

<CFPARAM name="promoid" default="evaluation">

<CFIF frmtask IS "update">
	
	<CFINCLUDE TEMPLATE="qryorderdetails.cfm">
	
	<CFSET currentorderitems=valuelist(getorderitems.productid)>
	<CFSET currentorderitemsparent=valuelist(getorderitems.ParentSKUGroup)>
	<CFSET currentorderitemsSKU_ParentSKUGroup=valuelist(getorderitems.SKU_ParentSKUGroup)>
	<CFSET currentorderitemsquantity=valuelist(getorderitems.quantity)>
	<cfset promoid=getorderheader.promoid>

		<!--- WAB 2001-04-05
		when coming back to edit an order, no product group was displaying.
		lets set the product group to that of the first item in the order
		actually later discovered that the problem had a slightly different cause, but will leave this in as a fallback anyway
		 --->
	<cfif not isDefined("thisproductgroup") and getOrderItems.productGroup is not "">
		<CFPARAM name="thisproductgroup" default="#getOrderItems.productGroup#">

	</cfif>

	
<CFELSE>

	<!--- these only set for update, so need defaults for new--->
	<CFPARAM name="currentorderitems" default="">
	<CFPARAM name="currentorderitemsSKU_ParentSKUGroup" default="">
	<CFPARAM name="currentorderitemsparent" default="">
	<CFPARAM name="orderlastupdated" default="">

	<!--- get promo name --->
	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		select 	
			*
		from 	
			promotion 
		where 	
			promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>

	<!--- 	Decide the id of the person placing the order
	Need to be able to handle both web users placing own orders and telesales using viewer to place other people's orders
	This is a bit of a hack to try and get something to work in the short term. --->

	<CFIF not isdefined("frmPersonID") and not isdefined("frmLocationID") >

		<CFIF isdefined("frmEntityType")>
			<CFIF frmEntityType is "person">
				<CFSET frmPersonID = frmCurrentEntityID>
			<CFELSEIF frmEntityType is "location">
				<CFSET frmLocationID = frmCurrentEntityID>
			</CFIF>
		<CFELSE>
			<!--- NJH 2009/07/02 P-FNL069 replaced with below <CFSET frmPersonID = #request.relayCurrentUser.personid#> --->
			<CFSET frmPersonID = request.relayCurrentUser.personID>
		</CFIF>
	</CFIF>
	<cfif showProgChampredemption>
		<cfset variables.pointsadmin = application.com.flag.getFlagData(flagid=ProgChampTextID,entityID=request.relaycurrentuser.organisationID)>
		<cfif variables.pointsadmin.recordcount NEQ 0 and not variables.pointsadmin.linkedEntityDeleted>
			<cfset frmPersonID = variables.pointsadmin.data>
		</cfif>
	</cfif>
	
	<CFINCLUDE TEMPLATE="SetPartnerLevel.cfm">

</cfif>

<cfparam name="ThisProductGroup" default="None">


<CFSET getOrderHeaderPromoName=replace(getOrderHeader.PromoName,' ','',"ALL")>


<CFSETTING ENABLECFOUTPUTONLY="No"> 

<cfparam name="GoToEid" default="">
<cfparam name="projectID" default="">

<!--- check for rights to this Promotion
	campaign ID from getOrderHeader.campaignID
	CampaignIDAllowed comes from application .cfm
 --->
<CFIF listFind(CampaignIDAllowed, getOrderHeader.CampaignID) is 0>
	<P>Sorry, you do not have rights to <cfoutput>Promotion #htmleditformat(promoid)#. 
		You have rights to view ids #htmleditformat(CampaignIDAllowed)#  and this is ID #htmleditformat(getOrderHeader.CampaignID)#: 
		</cfoutput>
	</P>

<CFELSE>


	<CFSET ZeroPrice = GetOrderHeader.ZeroPrice>
	
	
	<CFSET MaxOrdNo = getorderheader.MaxOrderNo>
	
	
	
	<!---
	A lot more switches will be set to determine what is shown on screen	
	--->
	<cfset ShowProductGroup = GetOrderHeader.ShowProductGroup>	
	<cfset ShowProductGroupHeader = GetOrderHeader.ShowProductGroupHeader>
	<cfset ShowTermsConditions = GetOrderHeader.ShowTermsConditions>
	<cfset ShowPreviousQuotesLink = GetOrderHeader.ShowPreviousQuotesLink>
	<cfset ShowPreviousOrdersLink = GetOrderHeader.ShowPreviousOrdersLink>
	<cfset ShowDeliveryCalculation = GetOrderHeader.ShowDeliveryCalculation>
	<cfset ShowVATCalculation = GetOrderHeader.ShowVATCalculation>
	
	<cfset ShowProductCode = GetOrderHeader.ShowProductCode>
	<cfset ShowProductCodeAsAnchor = GetOrderHeader.ShowProductCodeAsAnchor>
	<cfset ShowPictures = GetOrderHeader.ShowPictures>
	<cfset ShowDescription = GetOrderHeader.ShowDescription>
	<cfset ShowPointsValue = GetOrderHeader.ShowPointsValue>
	<cfset ShowLineTotal = GetOrderHeader.ShowLineTotal>
	<cfset ShowQuantity = GetOrderHeader.ShowQuantity>
	<cfset ShowListPrice = GetOrderHeader.ShowListPrice>
	<cfset ShowIllustrativePrice = GetOrderHeader.ShowIllustrativePrice>
	<cfset ShowDiscountPrice = GetOrderHeader.ShowDiscountPrice>
	<cfset ShowNoInStock = GetOrderHeader.ShowNoInStock>
	<cfset ShowMaxQuantity = GetOrderHeader.ShowMaxQuantity>
	<cfset ShowAvailability = GetOrderHeader.ShowAvailability>
	<cfset ShowPreviouslyOrdered = GetOrderHeader.ShowPreviouslyOrdered>
	
	<cfset ShowPageTotal = GetOrderHeader.ShowPageTotal>
	<cfset ShowDecimals = GetOrderHeader.ShowDecimals>
	<cfset ShowProductGroupScreen = GetOrderHeader.ShowProductGroupScreen>
	
	<cfset UsePoints = GetOrderHeader.UsePoints>
	<cfset PointsDetailURL = GetOrderHeader.PointsDetailURL>
	
	<cfset UseButtons = GetOrderHeader.UseButtons>
	
	<cfset OrderSingleProduct = GetOrderheader.OrderSingleProduct>
	
	<cfset TranslateDescription = GetOrderHeader.TranslateDescription>
	
	<!--- 2007/12/10 - GCC - Trying view access to catalogue --->
	<!--- 2009/02/25 - SSS - CR-Lex592 also switch of columns if you can only place a single order --->
	<cfif portalViewOnly>
		<cfset ShowQuantity = 0>
		<cfset ShowLineTotal = 0>
		<cfset ShowPageTotal = 0>
	</cfif>
	<cfif OrderSingleProduct>
	<cfset ShowLineTotal = 0>
		<cfset ShowPageTotal = 0>
	</cfif>
	
	<!--- the number of columns that will be used are based on the value of the switches above --->
	<cfset NumColumns 	= bitOr(ShowProductCode,ShowProductCodeAsAnchor) 
						+ ShowPictures 
						+ ShowDescription 					
						+ ShowLineTotal
						+ ShowQuantity
						+ ShowListPrice
						+ ShowIllustrativePrice
						+ bitOr(ShowDiscountPrice, ShowPointsValue)
						+ ShowNoInStock
						+ ShowMaxQuantity
						+ ShowAvailability
						+ ShowPreviouslyOrdered>
	
	
	<!--- note that this query can return multiple records if a country is in multiple regions --->
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
		select 	
			p.firstname,
			p.lastname,
			p.personid,
			p.locationid,
			l.sitename,
			l.countryid,
			o.organisationname
		from
			person as p, 
			location as l,
			organisation as o
			
		Where
			<CFIF isdefined("frmPersonid")>
				p.personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<cfelseif isdefined("frmLocationid")>
				l.locationid =  <cf_queryparam value="#frmlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			and	p.locationid=l.locationid	
			and l.organisationID = o.organisationID
	</CFQUERY>
	
	
	<CFIF getpersondetails.recordcount IS 0>
		Could not find person
		<cfexit method="EXITTEMPLATE">
	</cfif>
	
	
	
	<CFSET currentcountry=request.relaycurrentuser.content.showForCountryid>
	<CFSET currentlocationid=getpersondetails.locationid>
	<!--- SSS 2009/05/11 moved this code higher up so that I can use points in the query builder page --->
	<CFIF UsePoints>
	
		<!--- 
		who is allowed to spend the points? 
		I think only the Program Manager is
		
		SWJ 2002-09-03 added the query to getOrgID
		--->
		<cfquery name="getOrgID" datasource="#application.SiteDataSource#">
			select organisationID from location
			WHERE locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<CFSET organisationID = getOrgID.organisationID>
		<!--- NJH 2009/07/02 P-FNL069 replaced with below <cfset variables.personID = request.relayCurrentUser.personid> --->
		<cfset variables.personID = request.relayCurrentUser.personID>
		
		<cfquery name="GetNumberOfOrders" datasource="#application.SiteDataSource#">
			select orderID from Orders
			WHERE personID=#request.relaycurrentuser.personID#
			and orderstatus != 5
		</cfquery>
		
		<cfparam name="BalanceDate" default="#request.requestTime#">
		<cfif balancedate does not contain "{ts">
			<cfset BalanceDate = lsParseDateTime(balancedate)>	
		</cfif>
	
		<!--- <cfparam name="spendIncentivePointsType" default="company"> <!--- passed in via the relay tag --->
		<!--- SSS 2009/02/25 added the ability for anybody to see how many points a company has not just registered people --->
		<cfscript>
		if(not compareNocase(variables.spendIncentivePointsType, "personal") and not portalViewOnly){
			qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,variables.organisationID,variables.personID);
		}
		
		else if(not compareNocase(variables.spendIncentivePointsType, "company")){
			qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,variables.organisationID);
		}
		if (isdefined("ShowPointsforUnregisteredUser") and ShowPointsforUnregisteredUser and portalViewOnly){
			qPointsBalance = application.com.relayIncentive.PointsBalance(balanceDate,variables.organisationID);
		}
		</cfscript> 
		
		<cfset GetTotalPoints.PointsAvailable = qPointsBalance.balance>	--->
		
		<cfset argStruct = {spendIncentivePointsType=spendIncentivePointsType,portalViewOnly=portalViewOnly}>
		<cfif isDefined("ShowPointsforUnregisteredUser")>
			<cfset argStruct.ShowPointsforUnregisteredUser = ShowPointsforUnregisteredUser>
		</cfif>
		<cfset GetTotalPoints.PointsAvailable = application.com.relayIncentive.getPointsBalanceForPortalUser(argumentCollection=argStruct).balance>	
	
	</CFIF>
	
	<!--- get all active products 
	WAB 2001-04-26 moved this query to a separate template --->
	
	<CFINCLUDE template = "qryGetProductList.cfm">
	<!---
			DAM 04 JAN 2001
	
	    	This is how we'll deal with running into pages that try to show a marketing store
			for a specific product where there isn't one.
			
			Perhaps the query should be changed, but we'll look at that later.
			
			So all we're going to do is, if there are no products for this product group
			we'll just show the element
	--->
	
	<CFSET CanShowSpecificGroup=0>
	<CFLOOP query="getProductGroupID">
		<CFIF productgroup EQ ThisProductGroup><CFSET CanShowSpecificGroup=1><CFBREAK></CFIF>
	</CFLOOP>
	
	<!--- <CFIF CanShowSpecificGroup is 0 and #parameterexists(url.elementid)#>
		<CFSET elementID=url.elementid>
		<CFINCLUDE TEMPLATE="../InFocusWebsite/InFocusTemplate.cfm">
	<CF_ABORT>
	</CFIF>
	 --->
	<!----
				Hack stops here DAM 4 Jan 2001
		Although the logic wraps right round the rest of the page
	---->
	
	
	
	 
	<!--- <cfset PointsFactor = 1> --->			 <!--- 2010/11/09 PPB	LID4607  --->
	<cfparam name="showScreen" default="true">
	
	<cfif UsePoints and showScreen>
		<cfif getProductList.recordCount>
			<cfquery name="PointsFactor" datasource="#application.siteDataSource#">
				select
					CurrencyPerPoint
				from
					RWPointsConversion
				where
					PriceISOCode =  <cf_queryparam value="#getProductList.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" > 			
			</cfquery>
			
			<cfif PointsFactor.recordcount is 1 and PointsFactor.CurrencyPerPoint neq ''>
				<cfset PointsFactor = PointsFactor.CurrencyPerPoint>
		 	<cfelse>
				<!--- 2010/11/09 PPB LID4607  --->
				<!---	<cfset PointsFactor = 1>  --->					
				A CurrencyPerPoint Value Has Not Been Set For <cfoutput>#getProductList.PriceISOCode#</cfoutput>
				<cfset showScreen = false>
			</cfif>
		<cfelse>
			There are no products available.
			<cfset showScreen = false>
		</cfif>
	</cfif>
					
	<CFSETTING ENABLECFOUTPUTONLY="No"> 		
	
	<cfif showScreen>
		<cfif ShowProductGroup>
			<SCRIPT type="text/javascript">
		
				function changeProductGroup(aselect)
				{
					if(aselect.name == 'ThisProductGroup1'){
						document.mainForm.productGroupSelect.value = 1;
					}
					
					var now = new Date()
					now = now.getTime()
					if (document.mainForm != null)
					{
						<!--- START: 2008-07-09	AJC	Issue 695: Sony Rewards Gift Catalogue Category drop down broken - Required .value --->
						<!--- reordering takes you back to first page --->
						document.mainForm.action = "<cfoutput>#jsStringFormat(cgi.SCRIPT_NAME)#?eid=#jsStringFormat(eid)#&ShowProductGroup=1&prodStartRow=1&ThisProductGroup=" + escape(aselect.value) + "&PromoID=#jsStringFormat(promoid)#</cfoutput>&updatebasket=1&dummy=" + now
						document.mainForm.frmAction.value='';
						document.mainForm.submit()
					}
					else
					{
						window.location.href = "<cfoutput>#jsStringFormat(cgi.SCRIPT_NAME)#?eid=#jsStringFormat(eid)#</cfoutput>"
					}
				}
				
				function changeProductGroup1(aproductgroup)
				{
					var now = new Date()
					now = now.getTime()
		
					if (document.mainForm != null)
					{
						document.mainForm.action = "<cfoutput>orderitemstask.cfm?ShowProductGroup=1&prodStartRow=1&ThisProductGroup=" + escape(aproductgroup) + "&PromoID=#jsStringFormat(promoid)#</cfoutput>&updatebasket=1&dummy=" + now
						document.mainForm.submit()
					}
					else
					{
						window.location.href = "<cfoutput>orderitems.cfm?ShowProductGroup=1&prodStartRow=1&ThisProductGroup=" + escape(aproductgroup) + "&PromoID=#jsStringFormat(promoid)#&dummy=</cfoutput>" + now
					}				
				}			
		
											
		
			</script>
		
			<!--- if we're using product groups - this will now probably never get loaded - is included in 
			partners appropriate welcome screen...
			--->
			<cfif ThisProductGroup is "None" and ShowProductGroupScreen>
		
				<!--- need to allow the user to choose which initial product they are interested in seeing --->
		
					<table border="0" cellpadding="4" cellspacing="4" >
						<tr>
							<td colspan="2">Please choose from one of the categories below</td>
						</tr>
						<!--- display the groups that are available for the products within this promotion --->
						<cfoutput query="getProductGroupID">
						<tr>
							<td><a href="javascript:changeProductGroup1('#ProductGroup#')">#htmleditformat(ProductGroup)#</a></td>
							<!--- 
							make sure this is replaced later on - could use related file to upload a
							graphic related to the promotion?
							--->					
							<td><a href="javascript:changeProductGroup1('#ProductGroup#')"><img src="select.gif" border="0" alt="#ProductGroup# contains the following products:<cfoutput>#htmleditformat(Description)#</cfoutput>"></a></td>
						</tr>
						</cfoutput>
					</table>
				<CF_ABORT>
			<cfelse>
				<!--- allow the user to see a dropdown navigation doodah later on --->
				<cfset ShowProductGroupNavigation = true>
			</cfif>
		</cfif>
		<cfparam name="ShowProductGroupNavigation" default="false">
		
		
		
		<CFSET currentlocationid=getpersondetails.locationid>
		<CFINCLUDE template="qrydisalloweditems.cfm">
		<!--- find out whether there are any previous transactions --->
		<CFQUERY NAME="getPreviousOrders" datasource="#application.sitedatasource#">
		SELECT 	
			orderid
		FROM
			orders as o
		WHERE
			o.locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and	o.orderstatus  in ( <cf_queryparam value="#realorderstatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFQUERY>
		
		<CFQUERY NAME="getPreviousQuotes" datasource="#application.sitedatasource#">
		SELECT 	
			orderid
		FROM
			orders as o
		WHERE o.locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and	o.orderstatus = 1
		</CFQUERY>
		
		<cfoutput>
		<SCRIPT type="text/javascript">
		// function to load the shopping basket in a new window
			function shoppingBasket()
			{
				var form = document.mainForm
				var re = /^quant_/
				// start the query string off ensuring it's unique so that ie doesn't load a previously cached copy
				var now = new Date()
				now = now.getTime()
				var queryString = "?dummy=" + now + "&promoid=" + form.frmPromoID.value + "&realorderstatuses=#jsStringFormat(realorderstatuses)#&currentlocationid=#jsStringFormat(currentlocationid)#&reorderdisallowedcutoffdate=#jsStringFormat(urlencodedformat(reorderdisallowedcutoffdate))#&frmPersonID=#jsStringFormat(request.relayCurrentUser.personID)#<cfif usepoints>&totalPointsAvailable=#jsStringFormat(GetTotalPoints.PointsAvailable)#</cfif>"
				var tmp_queryString = ""
				// loop through each quant field on the form. Check to see whether a quantity is required - if it is,
				// append the field name to a name value pair string
				for (i = 0; i < form.length; i++)
				{
					if (re.test(form[i].name) && form[i].value > 0)
					{
						// it's a field we are interested in
						tmp_queryString += "&" + form[i].name + "=" + form[i].value
					}
				}
				if(tmp_queryString.length > 0)
				{
					// now open a new window passing all the quant's being used as a parameter
					<cfoutput>
						var newwin = window.open("/orders/shoppingBasket.cfm" + queryString + tmp_queryString,"BASKET","width=800,height=400,scrollbars")
					</cfoutput>
				}
				else
				{
					alert("phr_Order_ShoppingBasketEmpty")
				}
			}
		</script>
		</cfoutput>
		
		<CFSET pageloaded=now()>
		
		
			
		<!--- 
		<cf_head>
		 --->
		<cfoutput>
		<SCRIPT type="text/javascript">
		
		// create a javascript array to hold the various fields which  are used to calculate maxqty etc
		
		var fieldIs
		var fieldWas
		var valueIs
		var pointsAvailable = <cfif isDefined("GetTotalPoints.PointsAvailable") and GetTotalPoints.PointsAvailable gt 0><cfoutput>#jsStringFormat(getTotalPoints.PointsAvailable)#</cfoutput><cfelse>0</cfif>
		var pointsAlertDisplayed = false
		var records = new Array()
		
		function Record(productID, numberAvailable, maxQuantity, previouslyOrdered, listPrice, illustrativePrice, discountPrice, displayable, parentid)
		{
			this.productID = productID;
			this.numberAvailable = numberAvailable;
			this.maxQuantity = maxQuantity;
			this.previouslyOrdered = previouslyOrdered;
			this.listPrice = listPrice;
			this.illustrativePrice = illustrativePrice;
			this.discountPrice = discountPrice;
			this.displayable = displayable;
			this.parentID = parentid
		}
		
		
		// load the array from the query - only when we are on the correct product group or no product groups are used
		// 
		<cfif UseParenting>
		
		<cfelse>
			<CFIF ShowIllustrativePrice>
				toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
			</cfif>
			<cfloop query="GetProductList">
			<!--- 2001-05-09 CPS Ensure that the list price and discount price are rounded to whole points --->
			<!--- 2003-04-02 DAM Added semi colon (;) at the start of each of these statements to ensure they are separated --->
			<!--- 2005-04-15 AJC Added parenting functionality as products can now appear multiple times --->
			<!--- 2005-12-14 WAB if the productgroup chosen isn't available some code further on just shows the first product group, so we need to load the arrays
									had to add in the "or (CanShowSpecificGroup is 0 and getProductlist.ProductGroup is getProductlist.ProductGroup[1])" bit --->
			<!---  2008/01/02 GCC Removed the group filter on the records object so the total is for all items chosen not just the current product group--->
			<CFIF ShowIllustrativePrice>
				IllustrativePrice = application.com.currency.convert(fromValue=#jsStringFormat(DiscountPrice)#, fromCurrency="#jsStringFormat(PriceISOCode)#", toCurrency="#jsStringFormat(toCurrency.countrycurrency)#");
			</cfif>
			
		
			<CFIF UsePoints>																																																																																																							<!--- productID, numberAvailable, maxQuantity, previouslyOrdered, listPrice, illustrativePrice, discountPrice, displayable, parentid --->
				;records[records.length] = new Record(parseInt(#jsStringFormat(productID)#,10), parseInt(#iif(numberavailable gt 0,de(numberavailable),0)#,10), parseInt(#iif(MaxQuantity neq "", MaxQuantity ,0)#,10), parseInt(#iif(PreviouslyOrdered neq "",de(PreviouslyOrdered),0)#,10), parseFloat(#round(val(val(listprice) / PointsFactor))#,10), <CFIF ShowIllustrativePrice> parseFloat(#val(val(IllustrativePrice.tovalue) / PointsFactor)#,10)<cfelse>0</cfif>, parseFloat(#round(val(val(discountprice) / PointsFactor))#,10), <cfif MaxQuantity is 0 or (MaxQuantity gt PreviouslyOrdered)>true<cfelse>false</cfif>,0)
			<CFELSE>
				<!--- 2010/11/09 PPB LID4607  --->
				<!--- 	
				;records[records.length] = new Record(parseInt(#productID#,10), parseInt(#iif(numberavailable gt 0,de(numberavailable),0)#,10), parseInt(#iif(MaxQuantity neq "", MaxQuantity ,0)#,10), parseInt(#iif(PreviouslyOrdered neq "",de(PreviouslyOrdered),0)#,10), parseFloat(#val(val(listprice) / PointsFactor)#,10),<CFIF ShowIllustrativePrice> parseFloat(#val(val(IllustrativePrice.tovalue) / PointsFactor)#,10)<cfelse>0</cfif>, parseFloat(#val(val(discountprice) / PointsFactor)#,10), <cfif MaxQuantity is 0 or (MaxQuantity gt PreviouslyOrdered)>true<cfelse>false</cfif>,0)
				 --->	
		 		;records[records.length] = new Record(parseInt(#jsStringFormat(productID)#,10), parseInt(#iif(numberavailable gt 0,de(numberavailable),0)#,10), parseInt(#iif(MaxQuantity neq "", MaxQuantity ,0)#,10), parseInt(#iif(PreviouslyOrdered neq "",de(PreviouslyOrdered),0)#,10), parseFloat(#jsStringFormat(val(listprice))#,10),<CFIF ShowIllustrativePrice> parseFloat(#jsStringFormat(val(IllustrativePrice.tovalue))#,10)<cfelse>0</cfif>, parseFloat(#jsStringFormat(val(discountprice))#,10), <cfif MaxQuantity is 0 or (MaxQuantity gt PreviouslyOrdered)>true<cfelse>false</cfif>,0)
		 	</CFIF>
			</cfloop>
		</cfif>
		
		// array record locator
		
		function recordFind(aproductID)
		{
			for (var i=0; i<records.length; i++)
			{
				if (records[i].productID == aproductID)
				break
			}
			return i
		}
		
		
		// this function could probably be more elegant but still.
		function recFindNextDisplayable(idx, direction)
		{
			var found = false
			if (direction == "+")
			{
				for (i = idx+1; i < records.length; i++)
				{
					if(records[i].displayable)
					{
						found = true
						break
					}
				}
				
				if(!found)
				{
					// try going from the start - as before it must've got to the end
					for (i=0; i < records.length; i++)
					{
						if(records[i].displayable)
						{
							found = true
							break
						}
					}
				}
			}
			else
			{
				for (i = idx-1; i >= 0; i--)
				{
					if(records[i].displayable)
					{
						found = true
						break
					}		
				}
				if (!found)
				{
					// try going from the end - as before it must've got to the start
					for (i=records.length; i >= 0; i--)
					{
						if(records[i].displayable)
						{
							found = true
							break
						}
					}
				}
			}
			return records[i].productID
		}
		
		
		tmpNoOfItems = 0
		
			function jsSubmit()  {
			
					
			}
		
			function verifyForm() {
				var form = document.mainForm;
				var tmpProceed = 0
				//Call the function to check wether at least ONE check box has been clicked or one value has been typed in a text box
				if (verifyProductSelected()) {
				//check to see if the correct number of points have been used on this transaction
					<cfif OrderClaimRestrictions neq 0>
						<cfif OrderClaimRestrictions GT GetNumberOfOrders.recordcount>
							if (document.mainForm.TotalPrice.value < #jsStringFormat(minclaimamount)#) {
								alert("phr_Order_MinOrderMessage #jsStringFormat(minclaimamount)#") 
								tmpProceed = 1
								}
						</cfif>
					</cfif>
					//Here we check wether there is an order value limit being applied
					<CFIF IsDefined("maxordval")>
						if(totalordervalue>#jsStringFormat(maxordval)#>){
							window.alert("phr_Order_MaxOrdValExceeded #jsStringFormat(maxordval)#")
							tmpProceed = 1
						}
					</CFIF>
					<cfif UsePoints>
						if(!doTotalPoints(true))
						{
							tmpProceed = 1
						}			
					</cfif>
					if(tmpProceed == 0) {
						form.submit()
					}	
				}
				else {
					window.alert("phr_Order_MustChooseOneProduct <CFIF ShowProductGroup>phr_Order_SelectFromAnyProductGroup</cfif> ")
				}
			}
		
		
			//This function checks wether at least one textbox has a value in it
			function verifyProductSelected() {
				var form = document.mainForm;
				for (i=0; i<form.elements.length; i++){
					thiselement = form.elements[i];
					if (thiselement.name.substring(0,5) == 'quant') {
						if (thiselement.value > 0)  {
							return true
						}
					} 
				}	
				return false
			}
		
		
			function chooseProduct(aProductID){
				var form = document.mainForm
				// SSS 2009/02/03 if is is a single order catalogue blank all quantities before another one is added
				if (#jsStringFormat(OrderSingleProduct)# == 1){
					var re = /^quant_/
					
					for (i = 0; i < form.length; i++){
						if (re.test(form[i].name)){
							form[i].value = 0;
						}
					}
				}
				
				var tmpPrice = records[recordFind(aProductID)].discountPrice
				var quantobj = eval('form.quant_' + aProductID + '_0')//alex
				
				quantobj.value = 1
				
				var cont = true
				
				if(tmpPrice >= pointsAvailable){
					if (tmpPrice > pointsAvailable && !pointsAlertDisplayed) alert("phr_Order_ExceededPointsAvailableTotal " + pointsAvailable)
					cont = false
				}
				if (cont){
					form.submit() 
				}
			}
			
			
			
			// function to calculate the total value of items ordered for a line total. also handles the tabulation from
			// one field to the next in both directions, taking into account 'missing' fields.
			function doLineTotal(object, productID, navigate, parentid)
			{
				alphaRE = /[^0-9]/
				if (navigate == null) var navigate = true
				var recidx = recordFind(productID)
				var maxqty = records[recidx].maxQuantity
				var prevord = records[recidx].previouslyOrdered
				var quantobj = eval('object.form.quant_' + productID + '_' + parentid)//alex
				// make sure a number was entered
		
				if (quantobj != null)
				{
					//window.status = "alphaRE.test result: " + (alphaRE.test(quantobj.value)) + " " + quantobj.value
					if(alphaRE.test(quantobj.value))
					{
						alert("phr_Order_MustEnterNumber")
						quantobj.value = 0
						navigate=false
					}
				}
				
				var lineobj = eval('object.form.line_' + productID + '_' + parentid)//alex
				var priceobj = eval('object.form.price_' + productID + '_' + parentid)//alex
					<!--- GCC - breaks in rewards without this --->
						<cfif isdefined('document.mainForm.IllustrativeTotalPrice')>
							var illustrativelineobj = eval('object.form.lineIllustrative_' + productID + '_' + parentid)//alex
							var illustrativepriceobj = eval('object.form.illustrativeprice_' + productID + '_' + parentid)//alex
						</cfif>
				if((quantobj != null) && (lineobj != null) && (priceobj != null))
				{
					if (quantobj.value.length == 0) quantobj.value = 0
					
					if (maxqty > 0)
					{
						if (parseInt(quantobj.value,10) + prevord > maxqty)
						{
							alert("phr_Order_MaximumItemsYouCanOrder " + (maxqty - prevord))
							quantobj.value = valueIs
							quantobj.focus()
							quantobj.select()
							navigate = false
						}
					}
					//window.status="FieldIs: " + fieldIs + " FieldWas: " + fieldWas
					<cfif ShowDecimals>
					lineobj.value = formatNum(parseFloat(priceobj.value, 10) * parseInt(quantobj.value, 10), 2)
					<!--- GCC - breaks in rewards without this --->
						<cfif isdefined('document.mainForm.IllustrativeTotalPrice')>
							illustrativelineobj.value = formatNum(parseFloat(illustrativepriceobj.value, 10) * parseInt(quantobj.value, 10), 2)
						</cfif>
					<cfelse>
					lineobj.value = parseInt(priceobj.value, 10) * parseInt(quantobj.value, 10)
					<!--- GCC - breaks in rewards without this --->
						<cfif isdefined('document.mainForm.IllustrativeTotalPrice')>
							illustrativelineobj.value = parseInt(illustrativepriceobj.value, 10) * parseInt(quantobj.value, 10)
						</cfif>
					</cfif>
					
					//window.status = "navigate is : " + navigate
					if(navigate == true) doNavigate(object)
				}
				void doPageTotal();
				<cfif UsePoints>
				// need to make sure the reward points are not exceeded.
				tmp = void doTotalPoints();
				</cfif>
			}
		
			function doTotalPoints(tryingToConfirm)
			{
				var retval = true
				var form = document.mainForm
				var re = /^quant_/
				var tmpPoints = 0
				if (tryingToConfirm != null)
					pointsAlertDisplayed=false
		
				// loop through each quant field on the form. work out the accumulated price.
				for (var i = 0; i < form.length; i++)
				{
					if (re.test(form[i].name) && form[i].value > 0)
					{
						// it's a field we are interested in
						productID = form[i].name.split('_')[1]
						var tmpPrice = records[recordFind(productID)].discountPrice
						tmpPoints += (parseInt(form[i].value) * tmpPrice)

						if(tmpPoints > pointsAvailable)
						{
							//if (tmpPoints == pointsAvailable && !pointsAlertDisplayed) alert("phr_Order_ReachedPointsAvailableTotal " + pointsAvailable)
							if (tmpPoints > pointsAvailable && !pointsAlertDisplayed) alert("phr_Order_ExceededPointsAvailableTotal " + pointsAvailable)
							pointsAlertDisplayed = true
							retval = false
							break
						}
					}
					if (i+1 == form.length && pointsAlertDisplayed)
						pointsAlertDisplayed = false				
				}
				window.status = "Points: " +tmpPoints
				return retval	
			}
		
		
			function doNavigate(object)
			{
				tmpidx = recordFind(fieldIs)
				if (fieldIs >= fieldWas)
				{
					tmpobj = eval("object.form.quant_" + (recFindNextDisplayable(tmpidx, '+')) + '_0')//alex
					tmpobj.focus()
					tmpobj.select()
				}
				else
				{
					tmpidx = recordFind(fieldWas)					
					tmpobj = eval("object.form.quant_" + (recFindNextDisplayable(tmpidx, '-')) + '_0')//alex
					tmpobj.focus()
					tmpobj.select()
					//window.status = "FieldIs: " + fieldIs + " FieldWas: " + fieldWas + " object: " + tmpobj.name
				}
			}
		
		
			// Added formatting function so numbers look better in text box total.
			function formatNum (expr,decplaces)
			{
				var str=(Math.round(parseFloat(expr) * Math.pow(10,decplaces))).toString()
				while (str.length <= decplaces)
				{
					str="0" + str
				}
				var decpoint = str.length - decplaces
				return str.substring(0,decpoint) + "." + str.substring(decpoint,str.length)
			}	
			
			
			// function to do all stuff necessary when the screen loads...
			// DAM 02-04-2003 removed setting focus to firstquant- didn't remove firstquant
			// incase its used elsewhere.
			function screenLoaded()
			{
				var firstquant = eval('document.mainForm.quant_' + records[0].productID + records[0].parentID)//alex
				doPageTotal();
				return true
			}
		
			function doPageTotal()
			{
				var tot = 0;
				var illtot = 0;
				// go through each displayable element and add up the totals...
				for (i=0; i < records.length; i++)
				{
					
					if (records[i].displayable)
					{
						// set the line total - just in case it didn't work!
						lineobj = eval('document.mainForm.line_' + records[i].productID + '_' + records[i].parentID);//alex
						lineIllustrativeobj = eval('document.mainForm.lineIllustrative_' + records[i].productID + '_' + records[i].parentID);//alex
						qtyobj = eval('document.mainForm.quant_' + records[i].productID + '_' + records[i].parentID);//alex
						if(lineobj != null && qtyobj != null && qtyobj.value !=0)
						{
										
							<cfif showDecimals> 
							lineobj.value = formatNum(parseInt(qtyobj.value,10) * parseFloat(records[i].discountPrice,10),2);
							<cfelse>
							lineobj.value = parseInt(qtyobj.value,10) * parseInt(records[i].discountPrice,10);
							</cfif>
							tot += parseFloat(lineobj.value,10);
						}
						if(lineIllustrativeobj != null && qtyobj != null && qtyobj.value != 0)
						{
							<cfif showDecimals>
							lineIllustrativeobj.value = formatNum(parseInt(qtyobj.value,10) * parseFloat(records[i].illustrativePrice,10),2);
							<cfelse>
							lineIllustrativeobj.value = parseInt(qtyobj.value,10) * parseInt(records[i].illustrativePrice,10);
							</cfif>					
							illtot += parseFloat(lineIllustrativeobj.value,10);
						}
						
					}
				}
				if (document.mainForm.TotalPrice != null)
				{
					<cfif ShowDecimals>
					document.mainForm.TotalPrice.value = formatNum(tot,2)
						<!--- GCC - breaks in rewards without this --->
						<cfif isdefined('document.mainForm.IllustrativeTotalPrice')>
							document.mainForm.IllustrativeTotalPrice.value = formatNum(illtot,2)
						</cfif>
					<cfelse>
					document.mainForm.TotalPrice.value = tot
						<!--- GCC - breaks in rewards without this --->
						<cfif isdefined('document.mainForm.IllustrativeTotalPrice')>
							document.mainForm.IllustrativeTotalPrice.value = illtot
						</cfif>
					</cfif>
				}
			}
		
		
		
			//Display the subwindow when clicking on the photo
			function detail(prid) 
			{
				// uniquify it!
				var now = new Date()
				now = now.getTime()
				
				url = "/orders/ProductDetail.cfm?productid=" + prid + "&c=" + now		
		
				var subwindow = window.open(url,"Detail","height=" + (screen.availHeight * 0.8) + ",width=" + (screen.availWidth * 0.8) + ",toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0")
				
			} 
		
			function editProduct(prid, prgroup) 
			{
				// uniquify it!
				var now = new Date()
				now = now.getTime()
				
				url = "editProduct.cfm?productid=" + prid + "&productgroup=" + escape(prgroup)
		
				var subwindow = window.open(url,"Login","height=700,width=900,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0")
				subwindow.moveTo(62, 33)
				subwindow.focus()		
				
			} 
		
			function setQuantity(productID,quantity,parentid)  
			<!--- WAB function to set the quantity of an item via javascript - under dev 2000-06-07 for reward stuff --->
			{
				var form = document.mainForm										
				object = eval('form.quant_'+productID + '_0')//alex
				object.value = quantity
													
			}
		
		
		
		
		</SCRIPT>
		</cfoutput>	
				
		<!--- Removed SWJ 2004-07-26 <cf_DisplayBorder BorderSet="#getOrderHeader.BorderSet#"> --->
		<CFSETTING enablecfoutputonly="no">
		
		<CFIF msg IS NOT  "">
			<CFOUTPUT>
				#htmleditformat(msg)#
			</cfoutput>
		</cfif>
		
		<CFSET SKUsonpage="">
		<CFSET ProductSKUsonpage="">
		<CFSET skusParentsonpage="">
		<cfset frmNextPage = "">
		<cfif GoToEid is "">
			<cfset mainFormURL="/orders/orderitemstask.cfm">
		<cfelse>
			<cfset mainFormURL="et.cfm?eid=#GoToEid#">
		</cfif>
		
		<!--- NJH 2009/07/02 - P-FNL069 encrypt hidden fields --->
			
		<cfoutput><FORM ACTION="#mainFormURL#" NAME="mainForm" METHOD="POST"></cfoutput>
		<div class="row">
		<div id="marketingStore_intro" class="col-xs-12 col-sm-6">
			<p>
			<input type="hidden" name="dirty" value="0">
			<input type="hidden" name="frmAction" value="OrderInfo">
				
			<cf_encryptHiddenFields remove="false" excludeVariableNamesRegExp="quant_.*">
				
			<CFOUTPUT>
			
			<cfif GoToEid is not "">
				<CF_INPUT type="hidden" name="GoToEid" value="#GoToEid#">
			</cfif>
			<cfif isDefined("ShowPointsforUnregisteredUser")>
				<CF_INPUT TYPE="HIDDEN" NAME="ShowPointsforUnregisteredUser" VALUE="#ShowPointsforUnregisteredUser#">
			</cfif>
			<CF_INPUT TYPE="HIDDEN" NAME="thisEID" VALUE="#eid#">
			<cfif isDefined("spendIncentivePointsType")>
				<CF_INPUT TYPE="HIDDEN" NAME="spendIncentivePointsType" VALUE="#spendIncentivePointsType#">
			</cfif>
			<CF_INPUT TYPE="HIDDEN" NAME="portalViewOnly" VALUE="#portalViewOnly#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#frmtask#">
			<INPUT TYPE="HIDDEN" NAME="frmdisplaymode" VALUE="edit">
			<CF_INPUT TYPE="HIDDEN" NAME="frmPromoID" VALUE="#PromoID#">
				<!--- <INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="#frmGlobalParameters#"> NJH 2009/06/30 P-FNL069 --->
			<CF_INPUT TYPE="HIDDEN" NAME="currentorderid" VALUE="#currentorderid#">
			<CF_INPUT TYPE="HIDDEN" NAME="orderlastupdated" VALUE="#orderlastupdated#">
			<CF_INPUT TYPE="HIDDEN" NAME="pageloaded" VALUE="#pageloaded#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmOrderCurrency" VALUE="#ordercurrency#">
			<INPUT TYPE="HIDDEN" NAME="frmNextPage" VALUE="">
			<CF_INPUT TYPE="HIDDEN" NAME="OrderSingleProduct" VALUE="#OrderSingleProduct#"> <!--- SSS 2009/02/03 --->
			<CF_INPUT TYPE="HIDDEN" NAME="phraseprefix" VALUE="#phraseprefix#">  <!--- NJH 2007/01/18 --->
			<CFIF Isdefined("frmInViewer")>
				<CF_INPUT TYPE="HIDDEN" NAME="frmInViewer" VALUE="#frmInViewer#">
			</CFIF>
			
			 
	
				phr_#htmleditformat(phraseprefix)#Order_CreateOrderFor
				
				</CFOUTPUT>
		
			<!---If more than one possible person display a combo box with choices--->
			<CFIF getPersonDetails.recordCount GT 1>
		
				<SELECT class="form-control" NAME="frmPersonID">							
				<CFOUTPUT query="getPersonDetails">
				<OPTION value ="#personid#">#htmleditformat(firstName)# #htmleditformat(lastName)#
				</cfoutput>
				</SELECT>	
		
			<CFELSE>
			
				<!---Otherwise output directly person details--->
				<CFOUTPUT>
				<b>#htmleditformat(getpersondetails.FirstName)# #htmleditformat(getpersondetails.lastname)#, #htmleditformat(getpersondetails.organisationname)# </b><BR>
				<CF_INPUT TYPE="HIDDEN" NAME="frmPersonid" VALUE="#getpersondetails.personid#">
				</CFOUTPUT>
			
			</cfif>	
			</p>
		
		</div>
		<div class="col-xs-12 col-sm-6">
			<p>
			<cfif showPointsValue>
					<cfoutput>
						<!--- GCC CR-LEX592 need a switch here to display company points instead of available to spend points - must leave pointsAvailable at 0 to stop spending --->
						<b class="balance">phr_Order_PointsAvailable: #htmleditformat(GetTotalPoints.PointsAvailable)#</b>
					</cfoutput>
			<CFELSE>
				&nbsp;
			</cfif>
			</p>
		</div>
		</div>
		<div class="row">
		
			<cfif ShowProductGroupNavigation>
			
			<!--- sny-673 SSS 2009/05/11 Added a new product group so the we can choose afforable stuff --->
			<!---Start sny-673-1 SSS 2009/05/26 Allow the option to be at the top or bottom of the drop down --->
			<cffunction name="insertAffordable">
				<cfargument name="TranslateDescription" type="boolean" default="false" required="true">
				<cfargument name="ProductGroup" type="string" required="true">
				<cfargument name="ThisProductGroup" type="string" required="true">
				<cfargument name="ProductGroupID" type="numeric" required="true">
					
				<cfoutput>
					<cfif arguments.TranslateDescription>
						<option value="AFFORDABLE" <cfif arguments.ThisProductGroup eq "AFFORDABLE">selected</cfif>>phr_Incentive_Affordable</option>
					<cfelse>
						<option value="AFFORDABLE" <cfif arguments.ThisProductGroup eq "AFFORDABLE">selected</cfif>>Affordable Prizes</option>
					</cfif>
				</cfoutput>
			</cffunction>
			
			<cfif AffordableTop>
				<cfset insertRow = "start">
			<cfelse>
				<cfset insertRow = "end">
			</cfif>
			
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">
					<select class="form-control" name="ThisProductGroup" onchange="changeProductGroup(this)">
						<option value="">phr_orders_SelectProductGroup</option>
					<!--- GCC - 2005/10/12 - Check for product group passed existing - if not show first category in the list --->
						<cfset productGroupFound = false>
						<cfif thisProductGroup eq "AFFORDABLE">
							<cfset productGroupFound = true>
						</cfif>
						<!--- NJH 2009/07/21 P-SNY047 - added ShowAffordable as a switch --->
						<cfoutput query="getProductGroupID">
							<cfif insertRow eq "start" and getProductGroupID.currentrow eq 1 and ShowAffordable>
								<cfset x = insertAffordable(TranslateDescription=TranslateDescription,ProductGroup=ProductGroup,ThisProductGroup=ThisProductGroup,ProductGroupID=ProductGroupID)>
							</cfif>
							<cfif TranslateDescription>
		
								<!--- <option value="#ProductGroup#" <cfif ThisProductGroup is ProductGroup>selected</cfif>>phr_Description_ProductGroup_#ProductGroupID# </option> --->
								<!--- SSS 2009/05/11 The translation for the product group I have tacked on will work differently --->
								<option value="#ProductGroup#" <cfif ThisProductGroup is ProductGroup>selected</cfif>>phr_title_ProductGroup_#ProductGroupID#</option>
							<cfelse>
								<option value="#ProductGroup#" <cfif ThisProductGroup is ProductGroup>selected</cfif>>#ProductGroup#</option>
							</cfif>
							<cfif insertRow eq  "end" and getProductGroupID.currentrow eq getProductGroupID.recordcount and ShowAffordable>
								<cfset x = insertAffordable(TranslateDescription=TranslateDescription,ProductGroup=ProductGroup,ThisProductGroup=ThisProductGroup,ProductGroupID=ProductGroupID)>
							</cfif>
							<cfif ThisProductGroup is ProductGroup>
								<cfset productGroupFound = true>
							</cfif>
						</cfoutput>
						<cfif productGroupFound eq false>
							<cfset ThisProductGroup = getProductList.ProductGroup[1]>
						</cfif>
					</select>
				</div>
			</div>
				<!---END sny-673-1 SSS 2009/05/26 Allow the option to be at the top or bottom of the drop down --->
			<div class="col-xs-12 col-sm-6">
				<div class="form-group">		
					<cfif ShowProductGroupHeader>
						<select class="form-control" name="sortSchema" onChange="if(this.selectedIndex)changeProductGroup(this)">
								<option value="">phr_orders_SelectSortOrder</option>
								<option value="DiscountPrice DESC" <cfif structKeyExists(form, "sortSchema") AND not compareNoCase(form.sortSchema, "DiscountPrice DESC")>SELECTED</cfif> >phr_orders_SortValueHighToLow</option>
								<option value="DiscountPrice ASC" <cfif structKeyExists(form, "sortSchema") AND not compareNoCase(form.sortSchema, "DiscountPrice ASC")>SELECTED</cfif> >phr_orders_SortValueLowToHigh</option>
								<option value="description DESC" <cfif structKeyExists(form, "sortSchema") AND not compareNoCase(form.sortSchema, "description DESC")>SELECTED</cfif> >phr_orders_SortProductNameZA</option>
								<option value="description ASC" <cfif structKeyExists(form, "sortSchema") AND not compareNoCase(form.sortSchema, "description ASC")>SELECTED</cfif> >phr_orders_SortProductNameAZ</option>
						</select>
					<cfelse>
						&nbsp;
					</cfif>
				</div>
			</div>
				<!--- MDC - 2007-12-20 this has been removed as it is no longer needed!! --->
				<!--- 
				<cfif not (OrderSingleProduct or portalViewOnly)>
					<td class="label" align="right"><a href="javascript:shoppingBasket()">phr_Order_ViewShoppingBasket</a></td>	
				</cfif> 
				--->
			</div>
			</cfif>
			
			<cfif isdefined("ShowaccepttermsLink")>
				<cfset variables.LoyaltyAccept = application.com.flag.getFlagData(flagid='LoyaltyAccept',entityID=variables.organisationID)>
				<cfif not variables.LoyaltyAccept.recordcount>
				<cfoutput>
					<a href="#request.currentSite.httpProtocol##request.relayCurrentUser.sitedomain#/et.cfm?eid=#tandcEID#">phr_Incentive_Join_Program</a>
				</cfoutput>
				</cfif>
			</cfif>
		
		<!--- nain table starts --->
		<p>
			<cfif OrderSingleProduct and portalViewOnly eq "false">
					<strong>Phr_incentive_singleorder_Message</strong>
			<cfelseif not OrderSingleProduct and portalViewOnly eq "false">
				<cfoutput>phr_#htmleditformat(phraseprefix)#Order_PleaseSelectTheProductsYouWishToPurchase</cfoutput>
			</cfif>
		</p>
		<TABLE id="marketingStore"  data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder redemption">
			<!---Turn record sets into lists, than into arrays than work out the sum in order to know whether to display
			headings for Discount Price, List Price and Number In Stock---> 
			
			<!--- <CFSET DiscountPriceList = ValueList(getproductlist.DiscountPrice)>
			<CFSET ArrayDiscountPrice = ListToArray(DiscountPriceList)>
			<CFSET DiscountPriceDisplay = ArraySum(ArrayDiscountPrice)>
			
			<CFSET ListPriceList = ValueList(getproductlist.ListPrice)>
			<CFSET ArrayListPrice = ListToArray(ListPriceList)>
			<CFSET ListPriceDisplay = ArraySum(ArrayListPrice)> --->
				
			
			<!---  Labels Section  --->
			<cfoutput>
			<thead>
				<TR valign="top" class="topHeadingMultiLineTop">
					
					<CFIF ShowPictures>
						<TH>&nbsp;</Th>
					</CFIF>
					<cfif ShowProductCode or ShowProductCodeAsAnchor>
						<TH>phr_#htmleditformat(phraseprefix)#Order_ProductCode</th>
					</cfif>
					<cfif ShowDescription>
						<TH>phr_#htmleditformat(phraseprefix)#Order_Description</th>
					</cfif>
					<CFIF ShowListPrice>
						<TH>phr_#htmleditformat(phraseprefix)#Order_ListPrice (#htmleditformat(GetProductList.PriceISOCode)#)</th>
					</CFIF>
					<CFIF ShowDiscountPrice>
						<TH>phr_#htmleditformat(phraseprefix)#Order_DiscountPrice (#htmleditformat(GetProductList.PriceISOCode)#)</th>
					<cfelseif ShowPointsValue>
						<TH>phr_#htmleditformat(phraseprefix)#Order_RewardPoints</th>
					</CFIF>
					<CFIF ShowIllustrativePrice>
						<cfscript>
							toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
						</cfscript>
						<TH><cfoutput>phr_#htmleditformat(phraseprefix)#Order_IllustrativePrice * (#htmleditformat(toCurrency.countrycurrency)#)</cfoutput></th>
					</CFIF>
					<cfif ShowAvailability>
						<TH>phr_#htmleditformat(phraseprefix)#Order_Availability</th>
					</cfif>
					<CFIF ShowNoInStock>
						<TH>phr_#htmleditformat(phraseprefix)#Order_NoInStock</th>
					</CFIF>
					<cfif ShowMaxQuantity>
						<TH>phr_#htmleditformat(phraseprefix)#Order_MaxQty</th>
					</cfif>
					<cfif ShowPreviouslyOrdered>
						<TH>phr_#htmleditformat(phraseprefix)#Order_PrevOrd</th>
					</cfif>		
					<CFIF ShowQuantity>
						<TH>phr_#htmleditformat(phraseprefix)#Order_Qty</th>
					</CFIF>
					<CFIF ShowIllustrativePrice>
						<TH>phr_#htmleditformat(phraseprefix)#Order_IllustrativeTotal * (#htmleditformat(toCurrency.countrycurrency)#)</th>
					</CFIF>
					<cfif ShowLineTotal>
						<TH>phr_#htmleditformat(phraseprefix)#Total (#htmleditformat(GetProductList.PriceISOCode)#)</th>
					</cfif>
				</tr>
			</thead>
			</cfoutput>
			
			
			<!--- Detail Section --->
			<tbody>
			<CFIF getproductlist.recordcount is 0>
			
				<TR>
					<TD>
					<CFIF ThisProductGroup NEQ 0>
					There are no products in this product group.
						
					</TD>
					<CFELSE>
					<TD  colspan="5">There are no products set up for this country</td>
				</CFIF>
				</tr>
		
			<CFELSE>
		
				<cfif UseParenting>
					<cfinclude template="DisplayProductsToOrderByParent.cfm">
				<cfelse>
					<cfinclude template="DisplayProductsToOrder.cfm">
					
				</cfif>
				<!--- Summary/Totals Section --->
				
			</CFIF>
		
			<!--- NJH 2009/07/16 P-FNL069 - moved from below so that it could get encrypted --->
			<cfif isDefined("getOrderID.maxorderID")>
				<cfoutput><CF_INPUT TYPE="HIDDEN" NAME="MaxorderID" VALUE="#getOrderID.maxorderID#"></cfoutput>
			</cfif>
			</tbody>
		</cf_encryptHiddenFields>
		</table>
		<cfinclude template="/orders/displayProductPagination.cfm">
		
		
		<CFIF ZeroPrice IS NOT 1 or UsePoints is 1>
			
			<!--- Total Price displayed only if Prices for the Promotion are not 0--->
			
			<cfif ShowPageTotal and not OrderSingleProduct>
				<div class="form-group row">
					<div class="col-sm-6 col-xs-12">
						<label>
						<cfif UsePoints>
							<CFOUTPUT>phr_Order_TotalPoints</CFOUTPUT>
						<cfelse>
							<CFOUTPUT>#htmleditformat(ThisProductGroup)#  - phr_#htmleditformat(phraseprefix)#Order_TotalPrice</CFOUTPUT>
						</cfif>
						</label>
					</div>
					<div class="col-sm-6 col-xs-12">
					<cfif ShowIllustrativePrice>
						<INPUT TYPE="text" NAME="IllustrativeTotalPrice" VALUE="0" size="7" DISABLED class="form-control">
					</cfif>
						<INPUT TYPE="text" NAME="TotalPrice" VALUE="0" size="7" DISABLED class="form-control">
					</div>
				</div>
				<cfif ShowIllustrativePrice>
				<div class="form-group row">
					<div class="col-sm-6 col-xs-12">
						<label>
							<CFOUTPUT>phr_#htmleditformat(phraseprefix)#Order_IllustrativeTotal</CFOUTPUT>
						</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<INPUT TYPE="text" NAME="IllustrativeTotalPrice" VALUE="0" size="7" DISABLED class="form-control">
					</div>
				</div>
				</cfif>
			<cfelse>
				<INPUT TYPE="Hidden" NAME="TotalPrice" VALUE="0">
			</cfif>					
			
		</CFIF>
				<!---- submit section ---->
				
				
					<CFIF tmpNoOfPrevOrders IS getProductList.RecordCount>
						<p>
							<B>All products have been ordered</B>
						</p>	
					<CFELSE>
		
						<cfif ShowProductGroupNavigation>
										<cfif not (OrderSingleProduct or portalViewOnly)>
										<div class="row">
											<div class="col-xs-12 col-sm-6"><cfoutput>phr_#htmleditformat(phraseprefix)#Order_WhatToDoNow1</cfoutput></div>
											<div class="col-xs-12 col-sm-6">
											<!--- NJH 2009/07/21 P-SNY047 - added ShowAffordable --->
											<select class="form-control" name="ThisProductGroup1" onchange="changeProductGroup(this)">
												<cfoutput query="getProductGroupID">
													<cfif insertRow eq "start" and getProductGroupID.currentrow eq 1 and ShowAffordable>
														<cfset x = insertAffordable(TranslateDescription=TranslateDescription,ProductGroup=ProductGroup,ThisProductGroup=ThisProductGroup,ProductGroupID=ProductGroupID)>
													</cfif>
													<cfif TranslateDescription>
													<option value="#ProductGroup#" <cfif ThisProductGroup is ProductGroup>selected</cfif>>phr_Description_ProductGroup_#ProductGroupID#</option>
													<cfelse>
														<option value="#ProductGroup#" <cfif ThisProductGroup is ProductGroup>selected</cfif>>#ProductGroup#</option>
													</cfif>
													<cfif insertRow eq  "end" and getProductGroupID.currentrow eq getProductGroupID.recordcount and ShowAffordable>
														<cfset x = insertAffordable(TranslateDescription=TranslateDescription,ProductGroup=ProductGroup,ThisProductGroup=ThisProductGroup,ProductGroupID=ProductGroupID)>
													</cfif>
												</cfoutput>
											</select></div>							
										</div>
										<p><b>phr_Or</b></p>
											<div class="row">
											<div class="col-xs-12 col-sm-6"><cfoutput><p>phr_#htmleditformat(phraseprefix)#Order_WhatToDoNow2</p></cfoutput></div>
											<div class="col-xs-12 col-sm-6"><A class="btn btn-primary profileBtn" HREF="JavaScript:verifyForm();">phr_Continue</A></div>
											</div>
											<cfif promoid is "Evaluation2000">
											<cfoutput>
											<div class="row">
											<div class="col-xs-12 col-sm-6">phr_Order_RegisteredWithDisti</div>
											<div class="col-xs-12 col-sm-6"><a class="btn btn-primary" href="javascript: void window.open('FocalPointHelp.cfm','POPUP','width=400,height=500,status,resizable')">phr_Order_FocalPointHelp</a></div>
											</div>
											</cfoutput>
											</cfif>
										</cfif>
						<cfelse>
							<p>
								<cfif not OrderSingleProduct><cfif UseButtons><input type="Button" value="phr_Continue" onclick="verifyForm()" class="btn btn-primary OrdersFormbutton"><cfelse><A HREF="JavaScript:verifyForm();">phr_Continue</A></cfif></cfif>
							</p>
						</cfif>
					</CFIF>
			
				<CFIF not UsePoints>
			
					<CFIF getPreviousOrders.recordcount is not 0 and ShowPreviousOrdersLink>
					<p>
						<A class="btn btn-primary profileBtn" HREF="displayorderhistory.cfm?frmlocationid=<CFOUTPUT>#htmleditformat(currentlocationid)#&promoID=#htmleditformat(promoID)#</CFOUTPUT>&ShowProductGroupScreen=0">phr_Order_DetailsOfPreviousOrders</A> </td>
					</p>
					</cfif>
				
					<CFIF getPreviousQuotes.recordcount is not 0 and ShowPreviousQuotesLink>
					<p>
						<A class="btn btn-primary profileBtn" HREF="displayorderhistory.cfm?frmlocationid=<CFOUTPUT>#htmleditformat(currentlocationid)#&promoID=#htmleditformat(promoID)#</CFOUTPUT>&requiredstatus=1&ShowProductGroupScreen=0">phr_Order_DetailsOfOutstandingQuotes</A> </td>
					</p>
					</cfif>
			
				</CFIF>
		
		
		<CFIF ShowIllustrativePrice>
		<p>* <cfoutput>phr_#htmleditformat(phraseprefix)#illustrativedescription</cfoutput></p>
		</CFIF>
		
		<CFOUTPUT>
			<!--- DJH 2000-12-22 - stop extra space code ---->
			<!--- do not delete priceFields - this is important --->
			#application.com.security.sanitiseHTML(pricefields)#
			
			<CF_INPUT TYPE="HIDDEN" NAME="skusonpage" VALUE="#skusonpage#">
			<CF_INPUT TYPE="HIDDEN" NAME="skusParentsonpage" VALUE="#skusParentsonpage#">
		
			<!--- WAB 2001-04-05 added ifs around this - the product group is already set in a select box further up, so doubles up--->
			<cfif not ShowProductGroupNavigation>
			<CF_INPUT TYPE="HIDDEN" NAME="ThisProductGroup" VALUE="#ThisProductGroup#">
			</cfif>
		
			<!---If this promotion requires Questions to be asked with each order--->
			<CFIF #GetProductList.AskQuestions# EQ 1>	
				<CF_INPUT TYPE="HIDDEN" NAME="Questions" VALUE="#GetProductList.AskQuestions#">	
			</CFIF>
			<CF_INPUT type="hidden" name="projectID" value="#projectID#">
			<!--- pass current country and region as rpoduct table depens on country aswell now --->
			<CF_INPUT TYPE="HIDDEN" NAME="currentcountry" VALUE="#currentcountry#">	
			
			<INPUT TYPE="HIDDEN" NAME="productGroupSelect" VALUE="0">
				<!--- NJH 2009/07/16 P-FNL069 - moved hidden field above so that I could encrypt it.
			<cfif isDefined("getOrderID.maxorderID")>
				<INPUT TYPE="HIDDEN" NAME="MaxorderID" VALUE="#getOrderID.maxorderID#">
				</cfif> --->
		
		</cfoutput>
		
		</form>
		
		<!--- make sure all javascript alerts can be translated --->
		<!---
		<cfif isDefined("cookie.Translator") and cookie.translator is not "">
			<b>Translation for the Javascript alerts:</b><br>
			<cfset JSTextPhraseList = ListChangeDelims(JSTextPhraseList, ",","',")>
			<cfloop list="#JSTextPhraseList#" index="aListItem">
				<cfoutput>phr_#aListItem#</cfoutput><br>
			</cfloop>
		</cfif>
		--->
		<!--- if Replication has been set --->
		<cfif isdefined("cboReplicateOrderID")>
			<cfif cboReplicateOrderID is not "">
				<!--- get all order items SKUs --->
				<cfquery name="qryGetOrderItems" datasource="#application.sitedatasource#">
					SELECT SKU,Quantity from OrderItem
					where orderid =  <cf_queryparam value="#form.cboReplicateOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				
				<!--- loop through order items --->
				<cfoutput query="qryGetOrderItems">
					<!--- set the position of the SKU in the list --->
					<cfset productlistposition=listfind(ProductSKUsonpage,  SKU)>
					<!--- if the sku is found --->
					<cfif productlistposition neq 0>
						<!--- set the quantity value for this field --->
						<script>
							document.mainForm.quant_#ListGetAt(SKUsonpage, productlistposition)#jsStringFormat(_)#ListGetAt(SKUsParentsonpage, productlistposition)#.value=#jsStringFormat(Quantity)#					
						</script>
					</cfif>
				</cfoutput>
			</cfif>
		</cfif>
		
		<script>screenLoaded()</script>
	</cfif>
	
	<!--- Removed SWJ 2004-07-26 </cf_DisplayBorder>	 --->
	<!--- 
	 --->
	
</CFIF>	

	

