<!--- �Relayware. All Rights Reserved 2014 --->
<!--- mods:
	WAB 2000-03-13  added an isNull to prodweight, in case no value set in database
	GCC 2006/11/07 added quantity to prodweight calculation
 --->
<!--- work out value of order--->
			<CFQUERY NAME="getordervalue" datasource="#application.sitedatasource#">			
			select * from (
				select 
					sum(quantity * unitdiscountprice) as totalvalue,
					sum(quantity * unitlistprice) as totallistvalue,
					sum(totaldiscountprice) as totalvalue1,
					sum(totallistprice) as totallistvalue1,
					sum(quantity * (isNull(prodweight,0))) as totalWeight
				from vorderitems as i
				where	i.orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and		i.active=1)
			as base where totalvalue is not null
			</CFQUERY>