<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods

CPS 28032001 use new application variable to display status description

--->

<!---Get authorized countries--->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">


<!---Get only the country names for which orders exist for the combo box display--->
<CFQUERY NAME="getCountryCombo" dataSource="#application.siteDataSource#">
	SELECT CountryDescription, CountryID
	FROM Country
	WHERE CountryID IN (SELECT Location.CountryID
						FROM (promotion INNER JOIN Orders ON promotion.PromoID = Orders.promoid) 
						INNER JOIN Location ON Orders.LocationID = Location.LocationID
						<!--- WHERE Orders.OrderStatus IN (#RealOrderStatuses#) --->
						WHERE promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
</CFQUERY>
	
<!---Check if it is a Promotion which requires Questions and pick up Questions (FlagGroupIDs)--->
<CFQUERY NAME="AskQuestions" dataSource="#application.siteDataSource#">
	SELECT * FROM Promotion WHERE CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>




<!---Catch the New User for the Pie Copying--->
<CFSET Usr = #request.relayCurrentUser.usergroupid#>


<!---On the Delete the previously formed gif file by the same user after checking for it's existence--->
<!--- 		<CFDIRECTORY ACTION="LIST" DIRECTORY="#Path#\TempGraphics" FILTER="#Usr#_*.gif" NAME="MyFiles">
		
		<CFIF MyFiles.RecordCount NEQ 0>
			<CFLOOP QUERY="MyFiles">
				<CFIF fileExists("#Path#\TempGraphics\#Name#")>
					<CFFILE ACTION="DELETE" FILE="#Path#\TempGraphics\#Name#">
				</cfif>
			</CFLOOP>
		</CFIF>
 --->	

<!---The results page queries (if we've come through a Promo Link)--->

<!---<CFIF IsDefined("CountryCombo")>--->

	<CFIF Criteria EQ 1>

<!---Products query--->	
		<CFQUERY NAME="getProductAnalysis" dataSource="#application.siteDataSource#">
			SELECT Product.Description, Sum(OrderItem.Quantity) AS Quantity, OrderItem.SKU, 
			Sum(OrderItem.TotalDiscountPrice) AS OrderValue		
			FROM Product, Orders, OrderItem, Location
			WHERE Orders.OrderID = OrderItem.OrderID
			AND Product.ProductID = OrderItem.ProductID
			AND Orders.LocationID = Location.LocationID
			AND Orders.PromoID IN (SELECT PromoID FROM Promotion WHERE CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			AND Orders.OrderStatus  IN ( <cf_queryparam value="#RealOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfif DateFrom IS NOT ''>
			AND Orders.OrderDate >=  <cf_queryparam value="#DateFrom#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
			<cfif DateTo IS NOT ''>
			AND Orders.OrderDate <=  <cf_queryparam value="#DateTo#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF>
			<CFIF #CountryCombo# IS NOT 0>
			and Location.CountryID =  <cf_queryparam value="#CountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
			GROUP BY Product.CampaignID, Product.Description, OrderItem.SKU
			ORDER BY Product.Description
		</CFQUERY>
		
<!--- 		<!---Create a new .gif Pie file, it's name consisting of UserGroupID and TimeStamp)--->
		<CFIF getProductAnalysis.RecordCount NEQ 0>
		<CFOUTPUT>
			<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
			<cf_chart
	           format="flash"
	           chartwidth="450"
	           seriesplacement="default"
	           font="Arial"
	           fontsize="11"
	           showborder="yes"
	           labelformat="number"
	           show3d="yes"
	           tipstyle="mouseOver"
	           tipbgcolor="FFFF00"
	           pieslicestyle="sliced"
	           backgroundcolor="D3D3D3">

				   <cf_chartseries type="pie" query="getProductAnalysis" itemcolumn="SKU" valuecolumn="quantity" paintstyle="shade"></cf_chartseries>
			 </cf_chart>			
		<!---Define the pie chart--->
<!---
 		<CFX_GIFGD ACTION="DRAW_PIE"
			OUTPUT="#path#\TempGraphics\#Usr#_#TimeStamp#.gif"
			LEGENDS="#ValueList(Product.Description)#"
			DATA="#ValueList(Product.Quantity)#"
			SIZE="275"
			MAXROWS="15"
			HEADER="Order Analysis by Products"
			FOOTER= "Order Product Analysis (#DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'hh:mm tt')#)">
 --->		
			</CFOUTPUT>
		</CFIF>
 --->	
	<CFELSEIF Criteria EQ 2>

<!---Status Query--->
		<CFQUERY NAME="QStatus" dataSource="#application.siteDataSource#">
			SELECT Count(Orders.OrderID) AS Quantity, Orders.OrderStatus, 
			sum(isNull(Orders.OrderValue,0)) AS OrderValue
			FROM Orders 
			INNER JOIN Location ON Orders.LocationID = Location.LocationID
			INNER JOIN Promotion ON Orders.PromoID = Promotion.PromoID
			WHERE promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFIF #DateFrom# IS NOT "">
			AND Orders.OrderDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
			</cfif>
			
			<CFIF #DateTo# IS NOT "">
			AND Orders.OrderDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			</CFIF> 
			
			<CFIF #CountryCombo# IS NOT 0>
			and Location.CountryID =  <cf_queryparam value="#CountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
		
			GROUP BY Orders.OrderStatus
			
		</CFQUERY>
				
				<CFSET Statuses = ValueList(QStatus.OrderStatus)>
				<CFSET StatusListLength = ListLen(ValueList(QStatus.OrderStatus))>
				<CFSET StatusNames = ''>
			

				<CFLOOP index="x" FROM="1" TO="#StatusListLength#">
					<!--- CPS 28032001 use new application variable --->	

					<CFSET StatusNames = ListAppend(StatusNames, application.StatusDescriptions[ListGetAt(Statuses, #x#)])>							
<!---				<CFSET StatusNames = ListAppend(StatusNames, ListGetAt(orderstatuslist, ListFind(RealOrderStatuses,ListGetAt(Statuses, #x#))))>				
	 				<CFSET StatusNames = ListAppend(StatusNames, ListGetAt(orderstatuslist, ListGetAt(Statuses, #x#)))> --->

			
				</CFLOOP>
		
		<!---Define the pie chart--->	
		
		<!--- <CFIF QStatus.RecordCount NEQ 0>
			<CFOUTPUT>
			
			<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
			
			<CFX_GIFGD ACTION="DRAW_PIE"
					OUTPUT="#path#\TempGraphics\#Usr#_#TimeStamp#.gif"
					LEGENDS="#StatusNames#"
					DATA="#ValueList(QStatus.Quantity)#"
					SIZE="275"
					MAXROWS="15"
					HEADER="Order Analysis by Status"
					FOOTER= "Order Status Analysis (#DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'hh:mm tt')#)">
			</CFOUTPUT>
		</CFIF> --->	
		
		
		
<!---Questions Query--->
	<CFELSEIF Criteria EQ 3>
	
		<CFQUERY NAME="GetQuestionIDs" dataSource="#application.siteDataSource#">
		SELECT QuestionIDs FROM Promotion 
		WHERE CampaignID =  <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		
		<CFSET QuestionsList = #GetQuestionIDs.QuestionIDs#>
		
		<CFSET QList = ''>
		
		<CFLOOP INDEX="Question" LIST="#QuestionsList#">
		<CFIF Left(Question, 1) IS 'g'>
		 <CFSET QList = ListAppend(QList, RemoveChars(Question, 1, 6))>
		
		</CFIF>
		
		
		</CFLOOP>
		
		
		<CFQUERY NAME="QuestionsReport" dataSource="#application.siteDataSource#">
		SELECT Count(BooleanFlagData.FlagID) AS NumberofAnswers, FlagGroup.Name AS QuestionAsked, Flag.Name AS Answers
		FROM Flag, FlagGroup, BooleanFlagData
		WHERE Flag.FlagGroupID = FlagGroup.FlagGroupID  
		AND  Flag.FlagID = BooleanFlagData.FlagID
		 
		AND Flag.FlagGroupID  IN ( <cf_queryparam value="#QList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		
		
		
		GROUP BY FlagGroup.Name, Flag.Name
		
		
		</CFQUERY>

	<!---Users Query--->
	<CFELSEIF Criteria EQ 4>
		
		<CFQUERY NAME="UserReport" dataSource="#application.siteDataSource#">
		SELECT Count(Orders.OrderID) AS NumberOfOrders, Sum(isNull(Orders.OrderValue,0)) AS TotalOrderValue,
		Orders.CreatedBy AS UserGroupID, UserGroup.Name
		FROM Orders, UserGroup, Location, Promotion
		
		WHERE Orders.CreatedBy = UserGroup.UserGroupID
		AND Orders.LocationID = Location.LocationID
		AND Orders.PromoID = Promotion.PromoID
		AND Promotion.CampaignID  IN ( <cf_queryparam value="#CampaignID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<CFIF DateFrom IS NOT '' AND DateTo IS NOT ''>
			AND Orders.OrderDate >=  <cf_queryparam value="#CreateODBCDate(DateFrom)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND Orders.OrderDate <=  <cf_queryparam value="#CreateODBCDate(DateTo)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfif>
			
		<CFIF #CountryCombo# IS NOT 0>
			and Location.CountryID =  <cf_queryparam value="#CountryCombo#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		GROUP BY UserGroup.Name, Orders.CreatedBy
		ORDER BY UserGroup.Name
		</CFQUERY>
		
		<!---Define the pie chart--->	
		
		<CFOUTPUT>
		
		<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
		
		<!--- <CFX_GIFGD ACTION="DRAW_PIE"
			OUTPUT="#path#\TempGraphics\#Usr#_#TimeStamp#.gif"
			LEGENDS="#ValueList(UserReport.Name)#"
			DATA="#ValueList(UserReport.NumberOfOrders)#"
			SIZE="275"
			MAXROWS="15"
			HEADER="Order Analysis by Status"
			FOOTER= "Order User Analysis (#DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'hh:mm tt')#)">
		--->
		</CFOUTPUT>
		</cfif>
	




<SCRIPT>

function datefields(){
	var form = window.document.eventStats
	dates = form.DateRange[form.DateRange.selectedIndex].value
	var datefrom = dates.substring(0,10)
	var dateto = dates.substring(11,21)
	from = form.DateFrom
	to = form.DateTo
	from.value = datefrom
	to.value = dateto
	}

</SCRIPT>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="SubMenu">Order Statistics</TD>
	</TR>
</TABLE>

<CFFORM NAME="eventStats" ACTION="NewReportOrders.cfm" METHOD="post">

	<!---Display Country Combo--->
	<SELECT NAME="CountryCombo">
		<OPTION VALUE="0">All Countries
		<CFOUTPUT QUERY="getCountryCombo">
			<OPTION VALUE="#CountryID#" <CFIF IsDefined("CountryCombo")><CFIF CountryCombo EQ CountryID>SELECTED</CFIF><CFELSEIF IsDefined("LinkCountryCombo")><CFIF LinkCountryCombo EQ CountryID>SELECTED</CFIF></CFIF>>#htmleditformat(CountryDescription)#</option>
		</CFOUTPUT>
		</SELECT>
	
	&nbsp;&nbsp;&nbsp;
	
	<!---Display Status Combo--->
	<SELECT NAME="Criteria">
		
		<OPTION VALUE="1" <CFIF IsDefined("Criteria")><CFIF Criteria IS 1>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 1>SELECTED</CFIF></CFIF>>By Product
		<OPTION VALUE="2" <CFIF IsDefined("Criteria")><CFIF Criteria IS 2>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 2>SELECTED</CFIF></CFIF>>By Status
		<!---If it is the promotion containg Questions & Answers--->
		<CFIF AskQuestions.AskQuestions EQ 1>
			<OPTION VALUE="3" <CFIF IsDefined("Criteria")><CFIF Criteria IS 3>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 3>SELECTED</CFIF></CFIF>>By Question
		</CFIF>
		<OPTION VALUE="4" <CFIF IsDefined("Criteria")><CFIF Criteria IS 4>SELECTED</CFIF><CFELSEIF IsDefined("LinkStatus")><CFIF LinkStatus IS 4>SELECTED</CFIF></CFIF>>By User
	</SELECT>
	
	&nbsp;&nbsp;
	
	<CFINCLUDE TEMPLATE="DateRange.cfm">
	
	<!---Display Date Fields--->
	<CFOUTPUT>
	From: <INPUT TYPE="text" NAME="DateFrom" size="10" <CFIF IsDefined("DateFrom")>value=#DateFormat(DateFrom, "dd/mm/yyyy")#</CFIF>>
		  <INPUT TYPE="HIDDEN" NAME="DateFrom_eurodate">

	
	&nbsp;&nbsp;
	
	To: <INPUT TYPE="text" NAME="DateTo" size="10" <CFIF IsDefined("DateTo")>value=#DateFormat(DateTo, "dd/mm/yyyy")#</CFIF>>
		<INPUT TYPE="HIDDEN" NAME="DateTo_eurodate">
	&nbsp;&nbsp;
		<CF_INPUT TYPE="HIDDEN" NAME="CampaignID" VALUE="#CampaignID#">
	</CFOUTPUT>
	
	
	<INPUT TYPE="submit" NAME="StatSubmit" VALUE="  Go  ">
	</CFFORM>

<!--- ==============================================================================
       Display Results             
=============================================================================== --->

	<TABLE CELLPADDING="2" CELLSPACING="2">
	
		
		<TR>
			<TD COLSPAN="3"><B><H2>Analysis of orders - <CFOUTPUT>Promotion: <CFIF ListLen(CampaignID) IS 1>#htmleditformat(AskQuestions.PromoName)#<CFELSE>ALL</CFIF></CFOUTPUT></H2></B></TD>
		</TR>
<!--- ==============================================================================
       By product            
=============================================================================== --->
	
	<CFIF Criteria EQ 1 AND getProductAnalysis.RecordCount NEQ 0>
		<TR>
			<Th>Product Description</Th>
			<Th>Items Ordered</Th>
		</TR>
	
		<CFSET TotalQuantity = 0>
		<CFSET TotalOrderValue = 0>
		
		<CFOUTPUT QUERY="getProductAnalysis">
		
		<TR>
<!---Links for the Drill-Down passing parameters for stopping the Combo Boxes--->
			<TD>
				<a href="NewReportOrdersTask.cfm?SKU=#SKU#&CampaignID=#CampaignID#&LinkCountryCombo=#CountryCombo#&DateFrom=#URLEncodedFormat(DateFrom)#&DateTo=#URLEncodedFormat(DateTo)#">#htmleditformat(getProductAnalysis.Description)#</a>
			</TD>
			<TD align="center">#htmleditformat(Quantity)#</TD>
		</TR>
		
		<CFSET TotalQuantity = TotalQuantity + Quantity>
		<CFSET TotalOrderValue = TotalOrderValue + OrderValue>
		</CFOUTPUT>
		
		<CFOUTPUT>
		<TR>
			<TD>
				<a href="NewReportOrdersTask.cfm?CampaignID=#CampaignID#&LinkCountryCombo=#CountryCombo#&All=1">Total:</a>
			</TD>
			<TD align="center"><B>#htmleditformat(TotalQuantity)#</B></TD>
		</TR>
		
		<TR>
			<TD COLSPAN="3"><cf_chart
	           format="flash"
	           chartwidth="450"
	           seriesplacement="default"
	           font="Arial"
	           fontsize="11"
	           showborder="yes"
	           labelformat="number"
	           show3d="yes"
	           tipstyle="mouseOver"
	           tipbgcolor="FFFF00"
	           pieslicestyle="sliced"
	           backgroundcolor="D3D3D3">

				   <cf_chartseries type="pie" query="getProductAnalysis" itemcolumn="SKU" valuecolumn="quantity" paintstyle="shade"></cf_chartseries>
			 </cf_chart></TD>
		</TR>
	
		</CFOUTPUT>
	
<!--- ==============================================================================
       By status            
=============================================================================== --->
	<CFELSEIF Criteria EQ 2 AND QStatus.RecordCount NEQ 0>
		
		<CFIF QStatus.RecordCount NEQ 0>
		
		<TR>
			<Th>Order Status</Th>
			<Th>Number of Orders</Th>
		</TR>
	
		<CFSET TotalQuantity = 0>
		<CFSET TotalOrderValue = 0>
		
		<CFOUTPUT QUERY="QStatus">
			<CFSET Status = OrderStatus>
			<!---Links for the Drill-Down passing parameters for stopping the Combo Boxes--->		
			<TR>
				<TD>
					<a href="NewReportOrdersTask.cfm?OrderStatus=#Status#&LinkCountryCombo=#CountryCombo#&CampaignID=#CampaignID#&DateFrom=#URLEncodedFormat(DateFrom)#&DateTo=#URLEncodedFormat(DateTo)#">#application.StatusDescriptions[status]#</a>
				</TD>
				<TD align="center">#htmleditformat(Quantity)#</TD>
			</TR>
			<CFSET TotalQuantity = TotalQuantity + Quantity>
			<CFSET TotalOrderValue = TotalOrderValue + OrderValue>
		</CFOUTPUT>
		
		<CFOUTPUT>
		<TR>
			<TD>
				<a href="NewReportOrdersTask.cfm?LinkCountryCombo=#CountryCombo#&CampaignID=#CampaignID#&DateFrom=#DateFrom#&DateTo=#DateTo#&All=1">Total:</a>
			</TD>
			<TD align="center"><B>#htmleditformat(TotalQuantity)#</B></TD>
		</TR>
		
		<TR>
			<TD COLSPAN="3"><cf_chart
	           format="flash"
	           chartwidth="450"
	           seriesplacement="default"
	           font="Arial"
	           fontsize="11"
	           showborder="yes"
	           labelformat="number"
	           show3d="yes"
	           tipstyle="mouseOver"
	           tipbgcolor="FFFF00"
	           pieslicestyle="sliced"
	           backgroundcolor="D3D3D3">

				   <cf_chartseries type="pie" query="QStatus" itemcolumn="application.StatusDescriptions[status]" valuecolumn="quantity" paintstyle="shade"></cf_chartseries>
			 </cf_chart></TD>
		</TR>
	
	</CFOUTPUT>
		<TR>
			<TD colspan="3">These figures do not include quotes and canceled orders</TD>
			
		</TR>
	
		<CFELSE>
		
		<TR>
			<TD colspan="3">No orders available. Please try changing Search Criteria.</TD>
			
		</TR>
		
		</CFIF>

<!--- ==============================================================================
       By question            
=============================================================================== --->
	<CFELSEIF Criteria EQ 3 AND QuestionsReport.RecordCount NEQ 0>
	
		<CFSET TotalNumberOfAnswers = 0>
		
		<TR>
			<TH colspan="2" >Question</Th>
			<TH>Number of Answers</TH>
		</TR>

	
		<CFOUTPUT QUERY="QuestionsReport" GROUP="QuestionAsked">
		
		<TR>
			<TD colspan="3"><B>#htmleditformat(QuestionAsked)#</B></TD>
		</TR>
		
			<CFOUTPUT>
		<TR>
			<TD>&nbsp;</TD><TD >#htmleditformat(Answers)#</TD>
			<TD align="center">#htmleditformat(NumberOfAnswers)#</TD>
		</TR>
			</CFOUTPUT>
		
		
		<CFSET TotalNumberOfAnswers = TotalNumberOfAnswers + NumberOfAnswers>
		
		</CFOUTPUT>
		
<!--- ==============================================================================
       By user            
=============================================================================== --->
	<CFELSEIF Criteria EQ 4 AND UserReport.RecordCount NEQ 0>
		
		<TR>
			<TH>User</TH>
			<TH align="center">Number of Orders</TH>
		</TR>
	
		<CFSET TotalOrders = 0>
		<CFSET TotalOrdersValue = 0>
		
		<CFOUTPUT QUERY="UserReport">
		
		<TR>


<!---Links for the Drill-Down passing parameters for stopping the Combo Boxes--->
			<TD>
				<a href="NewReportOrdersTask.cfm?User=#UserGroupID#&CampaignID=#CampaignID#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Criteria#&DateFrom=#URLEncodedFormat(DateFrom)#&DateTo=#URLEncodedFormat(DateTo)#">#htmleditformat(Name)#</a>
			</TD>
			<TD align="center">#htmleditformat(NumberOfOrders)#</TD>
			
			
		</TR>
		
		<CFSET TotalOrders = TotalOrders + NumberOfOrders>
		<CFSET TotalOrdersValue = TotalOrdersValue + TotalOrderValue>
		
		
		</CFOUTPUT>
		
		<CFOUTPUT>
		<TR>
		<TD><a href="NewReportOrdersTask.cfm?CampaignID=#CampaignID#&LinkCountryCombo=#CountryCombo#&LinkStatus=#Criteria#&DateFrom=#DateFrom#&DateTo=#DateTo#&All=1"><B>Total</B>:</A></TD>
		
		
		<TD align="center"><B>#htmleditformat(TotalOrders)#</B></TD>
		
		
	
		</TR>
		
		
		
		<TR>
			<TD COLSPAN="3"><cf_chart
	           format="flash"
	           chartwidth="450"
	           seriesplacement="default"
	           font="Arial"
	           fontsize="11"
	           showborder="yes"
	           labelformat="number"
	           show3d="yes"
	           tipstyle="mouseOver"
	           tipbgcolor="FFFF00"
	           pieslicestyle="sliced"
	           backgroundcolor="D3D3D3">

				   <cf_chartseries type="pie" query="UserReport" itemcolumn="NAME" valuecolumn="NumberOfOrders" paintstyle="shade"></cf_chartseries>
			 </cf_chart></TD>
		</TR>
		</CFOUTPUT>
		
		<TR>
			<TD colspan="3">These figures do not include quotes and canceled orders</TD>
			
		</TR>
		
	<CFELSE>
	
	<TR>
			<TD colspan="3">No Orders available for Reporting. Please try changing Search Criteria.</TD>
			
		</TR>
		
		
	</CFIF>
	
	</TABLE>





		



