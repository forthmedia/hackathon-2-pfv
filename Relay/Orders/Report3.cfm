<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET frmdisplaymode = 'VIEW'>
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
select 	o.* ,
		p.firstname,
		p.lastname,
		l.sitename,
		c.countrydescription
from 	orders as o, location as l, person as p, country as c
where 	o.personid=p.personid
and		o.locationid=l.locationid
and		l.countryid=c.countryid
and 	o.orderid =  <cf_queryparam value="#tmpOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 

</CFQUERY>

<CFSET addresslabels="''">

<!---Alex added this line in order to repair the template, it is only temporary and requires
Will's attention--->
<CFSET maxaddresslines = getOrderHeader.RecordCount>





<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 	*
	FROM vOrderItems
	WHERE	orderid =  <cf_queryparam value="#tmpOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
	AND 	active = 1
</CFQUERY>


<cf_translate>

<br>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Order Details</DIV>
		</TD>
	</TR>
</table>

<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td>
		<CFOUTPUT>
		Order: #htmleditformat(getorderheader.orderid)#  <BR>
		Placed by: #htmleditformat(getorderheader.firstname)# #htmleditformat(getorderheader.lastname)#, #htmleditformat(getorderheader.sitename)# <BR>
		Date Created: #dateformat(getorderheader.created)# <BR>
		Order Status: #listgetat(orderstatuslist,getorderheader.orderstatus)#<BR>
		SSG ID: #htmleditformat(getorderheader.ssgcompanyid)#
		</CFOUTPUT>	
	</td>
</tr>
<TR>
<TD>&nbsp</td>
<TD>Delivery Details</td>
<TD>Invoice Details</td>
</tr>

	<TR>
		<TD>
		Contact Name:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="delContact" VALUE="#getorderheader.delcontact#" SIZE="30" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.delcontact)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="invContact" VALUE="#getorderheader.invcontact#" SIZE="30" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.invcontact)#</cfif></CFOUTPUT></FONT></TD>
	</TR>

	<TR>
		<TD>
		Site Name:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="delSiteName" VALUE="#getorderheader.delSitename#" SIZE="30" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.delsitename)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="invSiteName" VALUE="#getorderheader.invSitename#" SIZE="30" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.invsitename)#</cfif></CFOUTPUT></FONT></TD>
	</TR>


	<CFLOOP index="I" from ="1" to="#maxaddresslines#">
		<CFIF evaluate("getorderheader.labeladdress#I#") is not "notused">

	<TR>
		<TD>
		<CFOUTPUT><CFIF trim(evaluate("getorderheader.labeladdress#I#")) is not "">phr_#evaluate("getorderheader.labeladdress#htmleditformat(I)#")#</CFIF></CFOUTPUT></FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="delAddress#I#" VALUE="#evaluate("getorderheader.deladdress#I#")#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("getorderheader.invaddress#htmleditformat(I)#")#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="invAddress#I#" VALUE="#evaluate("getorderheader.invaddress#I#")#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("getorderheader.deladdress#htmleditformat(I)#")#</cfif></CFOUTPUT></FONT></TD>

	</TR>
		
		
		</CFIF>
	</CFLOOP> 
	
	<TR>
		<TD>
	Contact Phone:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="contactPhone" VALUE="#getorderheader.contactphone#" SIZE="20" MAXLENGTH="20"><CFelse>#HTMLEditFormat(getorderheader.contactphone)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD>
	Contact Fax:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="contactFax" VALUE="#getorderheader.contactfax#" SIZE="20" MAXLENGTH="20"><CFelse>#HTMLEditFormat(getorderheader.contactfax)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD>
	Contact Email:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="contactemail" VALUE="#getorderheader.contactemail#" SIZE="40" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.contactemail)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD>
	Your Purchase Ref:</FONT></TD>
		<TD>
		<CFOUTPUT><CFIF frmdisplaymode IS "edit"><CF_INPUT TYPE="Text" NAME="theirordernumber" VALUE="#getorderheader.theirordernumber#" SIZE="40" MAXLENGTH="50"><CFelse>#HTMLEditFormat(getorderheader.theirordernumber)#</cfif></CFOUTPUT></FONT></TD>
		<TD>
		</TD>
	</TR>
</table>

<BR>

<CFSET Total=0>

<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<TR>
	<td colspan='6'>
	<h2>Item Details</h2>
	</td>
</tr>
<TR>
	<th>SKU   </th>
	<th>Description  </th>
	<th>Quantity </th>
	<th align="right">List Price </th>
	<th align="right">Discount Price </th>
	<th align="right">Total Discount Price </th>
</TR>

<CFOUTPUT query="getorderitems">
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
	<TD>#htmleditformat(SKU)# </TD>
	<TD nowrap>#htmleditformat(Description)# </TD>
	<TD align="right">#htmleditformat(Quantity)# </TD>
	<TD nowrap align="right">(#htmleditformat(PriceISOCode)#) #decimalformat(UnitListPrice)# </TD>
	<TD align="right">#decimalformat(UnitDiscountPrice)# </TD>
	<TD align="right"#decimalformat(TotalDiscountPrice)# </TD>
</TR>

<CFSET Total=Total+val(totaldiscountprice)>

</CFOUTPUT>
</table>


</cf_translate>
