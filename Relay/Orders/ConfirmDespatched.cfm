<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET user = #request.relayCurrentUser.usergroupid#>
<CFSET FreezeDate = request.requestTimeODBC>
<!--- Check if the Cancel Button has been clicked --->
<CFIF IsDefined("btnCancel")>
	<!--- If no checkboxes have been checked then throw user back to previous page --->
	<CFLOCATION URL="Report2.cfm?btnViewStatus=3"addToken="false">
	<CF_ABORT>
</CFIF>

<!--- Update the Order Status to Dispatched --->
<CFQUERY NAME="qryUpdateOrderStatus" datasource="#application.sitedatasource#">
	UPDATE 	Orders 
	SET 	Orders.OrderStatus =  <cf_queryparam value="#constDispatched#" CFSQLTYPE="CF_SQL_INTEGER" > 
	WHERE 	Orders.OrderID  IN ( <cf_queryparam value="#chkDispatch#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>

<!--- Update the Order Status to Dispatched --->
<CFQUERY NAME="qryUpdateOrderStatus" datasource="#application.sitedatasource#">
	UPDATE 	Orders 
	SET 	Orders.OrderStatus =  <cf_queryparam value="#constCancelled#" CFSQLTYPE="CF_SQL_INTEGER" > 
	WHERE 	Orders.OrderID  IN ( <cf_queryparam value="#chkCancel#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<CFQUERY NAME="qryGetOrderID" datasource="#application.sitedatasource#">
	SELECT Orders.OrderID
	FROM Orders 
	WHERE Orders.OrderID IN (#chkDispatch#,#chkCancel# )
</CFQUERY>

<!---  Add records to the History Log--->	
<CFLOOP QUERY="qryGetOrderID">
	<CFQUERY NAME="CreateOrderHistoryRecord" datasource="#application.sitedatasource#"> 
			INSERT INTO OrderHistory
		   (<!--- OrderHistoryID, ---> OrderID,OrderNumber, OrderStatus, CreatedBy, Created)
		   
   			SELECT 	<!--- max(h.OrderHistoryID)+1, --->
					<cf_queryparam value="#qryGetOrderID.OrderID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					'xxx',
					4,
					<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,	
					<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			FROM Orders AS o, OrderHistory AS h
	</CFQUERY>
</CFLOOP>

<!--- Put them back to the first report page and this will show them that the status has changed --->
<CFLOCATION URL="Report1.cfm"addToken="false">