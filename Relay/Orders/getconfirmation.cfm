<!--- �Relayware. All Rights Reserved 2014 --->



<!--- check that order status is still "quote", otherwise can't be edited --->
	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		select 
			o.*,
			p.BorderSet
		from 
			orders as o,
			promotion as p
		where 
			o.orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and p.promoid = o.promoid
	</CFQUERY>

<cf_translate>
<cf_DisplayBorder BorderSet="#getOrderHeader.BorderSet#"> 

	<CFIF request.relayCurrentUser.usergroupid is 84>
		<CFSET thisEditableStatuses = SuperUserEditableOrderStatuses>
	<CFELSE>
		<CFSET thisEditableStatuses = EditableOrderStatuses>
	</CFIF>


	<CFIF  listfind(thisEditableStatuses,getorderheader.orderstatus) IS 0 >
		<CFSET msg="phr_Order_Thisisaconfirmedorderandcannotbeedited">
	
		<CFSET frmdisplaymode="view">
		<CFINCLUDE template="displayorder.cfm">

	<CFELSE>

		<CFIF getOrderHeader.paymentmethod IS "CC">
			<CFSET msg="phr_Order_PleaseConfirmOrderDetailsCorrect_CC">
		<CFELSE>
			<CFSET msg="phr_Order_PleaseConfirmOrderDetailsCorrect">
		</CFIF>

			<SCRIPT type="text/javascript">
			<!--
	
				<!--- put in for viewer - not tested yet--->
				function jsSubmit()  {

					document.mainForm.submit()

				}

			//-->
			</SCRIPT>
		<CFSET frmdisplaymode="view">
		<CFINCLUDE template="displayorder.cfm">
<CFOUTPUT>
		<div class="form-group row">
			<div class="col-xs-12 col-sm-9 col-sm-offset-3">
				<A class="btn btn-primary" HREF="JavaScript:submitForm('','editorder');">phr_Edit</A>
				<A class="btn btn-primary" HREF="JavaScript:submitForm('orderconfirmed','confirmOrder');">phr_Continue</A>
			</div>
		</div>
</cfoutput>
	</CFIF>	
	
	
	
</cf_DisplayBorder> 
</cf_translate>

<!--- <CFIF  not	isdefined("frmInViewer")>

	</TD>
	<CFINCLUDE TEMPLATE="BottomMenuBar.cfm">	
	
</CFIF>
		
 --->			