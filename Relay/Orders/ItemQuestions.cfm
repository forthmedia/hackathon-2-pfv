<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Mods:

WAB  2001-05-30   added enctype to the <FORM>


 --->

<CF_TRANSLATE>

<SCRIPT type="text/javascript">

<!--

	function verify()  {
		msg = verifyInput (); 
		if (msg != '') 	{
			alert(msg) 
		} 
		else 
		{
			document.mainForm.submit()
		} 
	}
	
<CFOUTPUT>
	function editScreen (screenID)  {
			var now = new Date()
			now= now.getTime()
			newWindow = window.open ('//admin//screens//screensList.cfm?frmScreenID=' + screenID, 'PopUp','location=0,resizable=1,scrollbars=1,status=1, width=750,height=550')
			newWindow.focus()
			}
	</CFOUTPUT>
	
//-->
	
</script>


<cf_DisplayBorder BorderSet="#getOrderHeader.BorderSet#">
<cfparam name="thisproductgroup" default="">
<TABLE>
	<TR><TD width="400"><H1><cfoutput>phr_Order_QuestionsPageTitle#htmleditformat(ScreenID)#</H1></cfoutput></td></tr>
	<TR><TD width="400"><B><cfoutput>phr_Order_QuestionsPageSubTitle#htmleditformat(ScreenID)#</cfoutput></B></td></tr>		
	<TR><TD><CFOUTPUT>#htmleditformat(THISPRODUCTGROUP)#</CFOUTPUT></TD></TR>
	<!--- SWJ removed the line below 05-Feb-2001.  When/if it is re-introduced please check 
	that orderid is defined at this point in the screen's process --->
<!--- 	<TR><TD><cfoutput>phr_Order_TransactionID#frmPromoID#: #orderid#</cfoutput></TD></TR> --->
</TABLE>
<TABLE>

<FORM name="mainForm" action="ItemQuestionsTask.cfm" method="post" enctype="multipart/form-data">
<CF_INPUT type="hidden" name="currentorderid" value="#currentorderid#">
<CF_INPUT type="hidden" name="PromoID" value="#frmPromoID#">

<!--- get query with current organisation --->
<cfquery name="organisation" datasource="#application.siteDataSource#">
	select 
		o.* 
	from 
		organisation o 
			inner join 
		person p on o.organisationid = p.organisationid
	where
		p.personid = #request.relaycurrentuser.personid#
</cfquery>

<cfquery name="orderItem" datasource="#application.siteDataSource#">
	select 
		* ,
		itemid as orderitemid
	from 
		orderItem
	where
		ItemID =  <cf_queryparam value="#itemid#" CFSQLTYPE="CF_SQL_INTEGER" >  
</cfquery>



<!--- test that can now handle different flags for different entities (during update) --->

<CF_aSCREEN formName="mainForm">

	<CF_aScreenItem 

		screenid="#screenid#" 
		orderitem=#orderitem# 
		organisation=#organisation# 
		method="edit" > 	

</CF_aSCREEN>
<CFIF isDefined("request.relayCurrentUser.personid") and (request.relaycurrentuser.usergroups) contains "503">
<!--- 	<CFOUTPUT>Balance #HideBalance#</CFOUTPUT> --->
</cfif>

<TR><TD><A HREF="javascript:verify()">phr_Continue</a></td></tr>	
</form>

</table>

<CFIF isDefined("request.relayCurrentUser.personid") and request.relaycurrentuser.usergroups contains "503">
	  <CFOUTPUT>
	  	<A HREF="javascript:editScreen(#screenID#)">Edit This screen</a>
	  </CFOUTPUT>
</cfif>

</CF_DisplayBorder>

 
</CF_TRANSLATE>
