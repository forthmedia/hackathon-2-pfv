<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			updateOrderStatus.cfm	
Author:				NJH
Date started:		2007-04-25
	
Description:		Updates the status of orders that are passed in.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-02-23			SSS			added some email triggers and some merge fileds once a status is set for a email CR -592 loyalty part 2

Possible enhancements:


 --->





<cf_head>
	<cf_title>Update Order Status</cf_title>
</cf_head>


<cf_translate>

<cfparam name="frmOrderIds" type="string" default="">
<cfparam name="SendDiscontinuedEmail" type="boolean" default="false">
<cfparam name="runInPopUp" type="boolean" default="true">
<cfparam name="OrderNumber" default="">
<cfparam name="SKU" default="">
<cfparam name="CompanyName" type="string" default="">

<cfoutput>
	<cfif runInPopUp>
		<SCRIPT type="text/javascript">
			function closePopUp() {
				// if we've made changes, refresh the parent page to reflect the new changes. Otherwise just close the window
				<cfif isDefined("frmSubmit")>
				if(!opener.closed){
					if (opener.location.href.search(/newReportOrdersTask.cfm/i) != -1) {
						// used for newReportOrdersTask which needs a bunch of parameters to be set
						opener.location.href='/orders/newReportOrdersTask.cfm?orderNumber=#jsStringFormat(orderNumber)#&SKU=#jsStringFormat(SKU)#&CompanyName=#jsStringFormat(CompanyName)#';
					} else {
						opener.location.href=opener.location.href;
					}
				}
				</cfif>
				self.close();
			}
		</script>
	</cfif>
</cfoutput>

<cfset request.relayFormDisplayStyle="HTML-table">

<cfif frmOrderIds neq "">
<!--- SSS 2008-04-15 the realOrderStatuses is in the lexmarkuserfiles/code/cftemplates/orders.ini file --->
	<cfquery name="getOrderStatuses" datasource="#application.siteDataSource#">
		select distinct statusID, description from status where 1=1			
			<cfif isDefined("realOrderStatuses") and realOrderStatuses neq "">
				and statusID  in ( <cf_queryparam value="#realOrderStatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
	</cfquery>
	
	<cfform name="updateStatusForm"  method="post">
		<cf_relayFormDisplay>
			<cfif runInPopUp>
				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="<a href='javascript:closePopUp();'>phr_sys_CloseWindow</a><br><br>" fieldname="" label="" valueAlign="right">
			</cfif>
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmOrderIds#" fieldname="frmOrderIds" label="">
			<cf_relayFormElementDisplay relayFormElementType="select" currentValue="#IIF(isDefined('frmOrderStatus'),'frmOrderStatus',DE(''))#" label="phr_order_updateOrderStatusTo" fieldname="frmOrderStatus" query="#getOrderStatuses#" display="description" value="statusID">
			<cf_relayFormElementDisplay relayFormElementType="submit" currentValue="phr_update" fieldname="frmSubmit" label="" spanCols="yes">
	
			<cfif isDefined("frmSubmit")>
				<!--- updates the status of the order --->
				<cfquery name="updateOrderStatus" datasource="#application.siteDataSource#">
					update orders set orderStatus =  <cf_queryparam value="#frmOrderStatus#" CFSQLTYPE="CF_SQL_INTEGER" >  where orderID  in ( <cf_queryparam value="#frmOrderIds#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
				</cfquery>
				<cfoutput>
				<!--- if any of the following status are set set the merge fileds because a email is going to go out --->
				<cfif frmOrderStatus EQ 2016 or frmOrderStatus EQ 2015 or frmOrderStatus EQ 4>
				<!--- this will make all the merge fields ready for any emails that need sending --->
				<cfloop index="IdxOrderID" list="#frmOrderIDs#" delimiters=",">
						<cfquery name="orderdetails" datasource="#application.siteDataSource#">		
							select 	p.firstname, o.shipper, o.shipperReference, o.orderID, pd.sku, pd.description, rwt.DebitAmount,
									o.delsitename, o.deladdress1, o.deladdress2, o.deladdress3, o.deladdress4, o.deladdress5, o.delpostalcode,
									p.personID, org.OrganisationName, org.organisationID
							from 	orders o inner join
									person p on o.personID = p.personID inner join
									organisation org on p.organisationID = org.organisationID inner join
									orderitem oi on o.orderID = oi.orderID inner join
									product pd on oi.productID = pd.productID inner join
									rwtransaction rwt on o.orderID = rwt.OrderID 
							where 	o.orderID =  <cf_queryparam value="#IdxOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						</cfquery>
						<!--- set the points administrator  for the organisation send in a email --->
						<cfset merge = StructNew()>
						<cfset merge.firstname = orderdetails.firstname>
						<cfif orderdetails.shipper EQ "">
							<cfset merge.TrackingWebsite = 'Tracking Website not avaliable'>
						<cfelse>
							<cfset merge.TrackingWebsite = orderdetails.shipper>
						</cfif>
						<cfif orderdetails.shipperReference EQ "">
							<cfset merge.TrackingNumber = 'Tracking Number not avaliable'>
						<cfelse>
							<cfset merge.TrackingNumber = orderdetails.shipperReference>
						</cfif>
						<cfset merge.TransactionNumber = orderdetails.orderID>
						<cfset merge.ProductCode = orderdetails.sku>
						<cfset merge.description = orderdetails.description>
						<cfset merge.Points = orderdetails.DebitAmount>
						<cfset merge.DeliveryAddress = orderdetails.delsitename>
				
						<cfif orderdetails.deladdress1 NEQ "">
							<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.deladdress1>
						</cfif>
						<cfif orderdetails.deladdress2 NEQ "">
							<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.deladdress2>
						</cfif>
						<cfif orderdetails.deladdress3 NEQ "">
							<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.deladdress3>
						</cfif>
						<cfif orderdetails.deladdress4 NEQ "">
							<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.deladdress4>
						</cfif>
						<cfif orderdetails.deladdress5 NEQ "">
							<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.deladdress5>
						</cfif>
						<cfset merge.DeliveryAddress = merge.DeliveryAddress & '<br>' & orderdetails.delpostalcode>
					
						<cfif frmOrderStatus EQ 2016>
							<!--- these varibales are for the emails below only there do not need to be set on the other emails --->
							<cfset variables.pointsadmin = application.com.flag.getFlagData(flagid='LoyaltyPointsAdministratorID',entityID=orderdetails.organisationID)>
							<cfset merge.OrganisationName = orderdetails.OrganisationName>
							<cfset merge.pointsadmin = variables.pointsadmin.data>
							<cfif application.com.email.doesEmailDefExist(emailTextID='LoyaltyOrderDiscontinued')>
								<cfset sendOrderDiscontinuedemail = application.com.email.sendEmail(emailTextID='LoyaltyOrderDiscontinued',personID=orderdetails.personID, cfmailtype="html",mergeStruct = merge)>
							</cfif>
							<!--- need to send this to Fabrice Toubiana personID 410 on live---> 
							<cfif SendDiscontinuedEmail>
								<cfif application.com.email.doesEmailDefExist(emailTextID='LoyaltyOrderDiscontinuedToApprover')>
									<cfset sendOrderDiscontinuedToApproveremail = application.com.email.sendEmail(emailTextID='LoyaltyOrderDiscontinuedToApprover',personID=EmailLoyaltyOrderDiscontinuedToApproverID, cfmailtype="html",mergeStruct = merge)>
								</cfif>
							</cfif>
						<cfelseif frmOrderStatus EQ 2015>
							<cfif application.com.email.doesEmailDefExist(emailTextID='LoyaltyOrderOutofstock')>
								<cfset sendOutofstockemail = application.com.email.sendEmail(emailTextID='LoyaltyOrderOutofstock',personID=orderdetails.personID, cfmailtype="html",mergeStruct = merge)>
							</cfif>
						<cfelseif frmOrderStatus EQ 4>
							<cfif application.com.email.doesEmailDefExist(emailTextID='LoyaltyOrderDispatched')>
								<cfset senddispatchedemail = application.com.email.sendEmail(emailTextID='LoyaltyOrderDispatched',personID=orderdetails.personID, cfmailtype="html",mergeStruct = merge)>
							</cfif>
						</cfif>
						<!--- after you have used the merge structure delete because it is no longer required --->
						<cfset x= StructDelete(Variables, "merge")>
					</cfloop>
				</cfif>
				
				</cfoutput>
				<cfif len(frmOrderIds) eq 1>
					<cf_relayFormElementDisplay relayFormElementType="message" currentValue="phr_order_YourOrderHasSuccessfullyBeenSetTo '#application.StatusDescriptions[frmOrderStatus]#'." fieldname="">
				<cfelse>
					<cf_relayFormElementDisplay relayFormElementType="message" currentValue="phr_order_YourOrdersHaveSuccessfullyBeenSetTo '#application.StatusDescriptions[frmOrderStatus]#'." fieldname="">
				</cfif>
			</cfif>
	
		</cf_relayFormDisplay>
	</cfform>
	
<cfelse>
	<cfoutput>phr_order_NoOrdersHaveBeenSelectedForUpdate</cfoutput>
</cfif>

</cf_translate>



