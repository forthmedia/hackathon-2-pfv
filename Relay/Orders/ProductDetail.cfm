<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
ProductDetail.cfm

Shows full detail of a product

Mods

WAB 2001-04-25   Correct related file bit so that will show more than one pdf if available
				Also took out bit about points balance, crashes because organisationid not set and doesn't appear to be used
NJH	2009/07/06	P-FNL069 - added check if user was logged in.
WAB 2009/09/07 LID 2515 Removed #application. userfilespath#  

 --->

<!--- NJH 2009/07/06 P-FNL069 - added some security checks --->
<cfif request.relayCurrentUser.isLoggedIn>

	<CFQUERY NAME="GetProduct" datasource="#application.siteDataSource#">
		SELECT * 
		from Product 
		where ProductID =  <cf_queryparam value="#ProductID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<cfif GetProduct.recordCount>
		<cfquery name="promo" datasource="#application.siteDataSource#">
		select 
			* 
		from 
			promotion 
		where
			campaignid =  <cf_queryparam value="#getproduct.campaignid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfset PromoID = promo.promoid>
		

		
		<cf_translate>
		
		<cf_head>
			<cf_title>phr_Order_ProductDetail</cf_title>
				<!--- <link href="#request.currentsite. stylesheet#" rel="stylesheet" media="screen,print" type="text/css"> --->
		</cf_head>
		
		<cfset UsePoints = promo.UsePoints>
		<cfset TranslateDescription = promo.TranslateDescription>
		<cfset OrderSingleProduct = promo.OrderSingleProduct>
		
		<BR>
		

		<TABLE align="center">
			
		<CFOUTPUT QUERY="GetProduct">
			
		<cfset filePath = "/content/linkImages/product/#productID#/">
			
		<FORM ACTION="" METHOD="post" name="aproduct">
			<TR>
				<TD colspan="2" align="center">
				<cfif TranslateDescription>
					<b><p>phr_Description_Product_#htmleditformat(ProductID)#</p></b>
				<cfelse>
					<b><p>#htmleditformat(description_defaultTranslation)#</p></b>
				</cfif>
				</TD>
			</TR>
			<TR>
				<TD colspan="2" align="center">
					<!--- <cfif fileexists("#application.paths.content#\ProductPhotoHires\CountryID#countryID#\#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg")>
						<IMG alt="#SKU# Image" src="/content/ProductPhotoHires/CountryID#countryID#/#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.jpg" BORDER=0></A>
					</cfif>  --->
					<cfif fileExists("#ExpandPath(filePath)#\highRes_#productID#.jpg")>
						<img alt="#sku# Image" src="#filePath#/highRes_#productID#.jpg">
					</cfif>
				<!--- <IMG alt="productdescription" src="#GetProductFile.Webhandle#" BORDER=0> --->
				</TD>
			</tr>
			<tr>
				<TD colspan="2" align="center">
				<br><br>
				<cfif TranslateDescription>
					<div align="justify"><p>phr_LongDescription_Product_#htmleditformat(ProductID)#</p></div>
				<cfelse>
					<div align="justify"><p>#htmleditformat(description_defaultTranslation)#</p></div>
				</cfif>
				<br><br>
				</TD>
			</TR>
			<!--- MDC - 2005-07-20  
			Added in below as PDFs have been commented out below and the files are not being loaded
			into related file anymore, therefore needed to be called in the same was as the HiRes images
			--->
			<TR>
				<TD colspan="2" align="center">
					<!--- <cfif fileexists("#application.paths.content#\ProductPDF\#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.pdf")>
						<a href="/content/ProductPDF/#promoID#-#replace(replace(replace(replace(SKU,'&','-','ALL'),'*','-','ALL'),'/','-','ALL'),' ','-','ALL')#.pdf" BORDER=0 target="blank">phr_Order_Download_PDF_DetailFile</A>
					</cfif>  --->
					<cfif fileExists("#ExpandPath(filePath)#\pdf_#productID#.pdf")>
						<a href="#filePath#/pdf_#productID#.pdf" BORDER=0 target="blank">phr_Order_Download_PDF_DetailFile</A>
					</cfif>
				<!--- <IMG alt="productdescription" src="#GetProductFile.Webhandle#" BORDER=0> --->
				</TD>
			</tr>
		<!--- 
			<CF_RelatedFile action="list" 
				query="GetProductFile" 
				entity="#productID#" 
				entitytype="Product" 
				FileCategory="ProductPDF">
		
			<cfif GetProductFile.recordcount is not 0>
			<TR>
		
				<TD colspan="2" align="left">
					phr_Order_Download_DetailFile
				</TD>
			</tr>
			</cfif>
		
		
			<cfloop query="GetProductFile">
			<TR>
		
				<TD colspan="2" align="left">
					<A HREF="#GetProductFile.Webhandle#" TARGET="_blank"><cfif description is not "">#Description#<cfelse>#filename#</cfif></A>
				</TD>
			</tr>
			</cfloop>	 --->
		
		<!--- 	<TR>
				<TD>
				<b>phr_Points</b>
				</TD>
				<TD>
				<b>phr_Order_Qty</b>
				</TD>
			</TR>
		 --->	
			<!--- <TR>
				<TD>
				#val(DiscountPrice / PointsFactor)#
				</TD>
				<cfif not OrderSingleProduct>
					<TD>
					<INPUT TYPE="text" NAME="qnt" size="2" value=""> 
					</TD>
				</cfif>
			<tr>
				<cfif not OrderSingleProduct>
					<td><A HREF="javascript:add(#ProductID#)">phr_Order_AddtoOrder</A></td>
				</cfif>
				<TD>
				
				</TD>
			</TR>
		 --->	
		 
		 	<TR><TD>&nbsp;</TD></TR>
		 	<TR><TD><A HREF="javascript:window.close()">phr_Close</A></TD></TR>
			
			</FORM>
			
			
		 
			
		
			</CFOUTPUT>
		
		</TABLE>
	
		
		
		</cf_translate>
	</cfif>
</cfif>

