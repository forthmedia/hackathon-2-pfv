<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:
NYB 2009-01-26 	8.1 bug 1674 - was throwing an error on checkPermission.admin - as admin didn't exist
 --->

<CFPARAM NAME="current" TYPE="string" DEFAULT="ordersHome.cfm">

<CF_RelayNavMenu pageTitle="Channel Commerce" thisDir="#thisDir#">
	<CFIF findNoCase("catalogues.cfm",SCRIPT_NAME) neq 0>
		<CF_RelayNavMenuItem MenuItemText="List Product Groups" CFTemplate="/report/reportXMLGeneric.cfm?reportName=productGroups" target="mainSub">			
	</CFIF>
	
	
	<CFIF findNoCase("tradeUpHome.cfm",SCRIPT_NAME) neq 0>
	
		<CFQUERY NAME="getPromotion" datasource="#application.siteDataSource#">
			select *
			from promotion where
			promoId = 'TradeUp'
		</CFQUERY>
		<cfset TradeUpcampaignID = getPromotion.campaignID>
	
		<!--- NYB 2009-01-26 8.1 bug 1674 - added "StructKeyExists(checkPermission, "admin") and " to if --->	
		<cfif StructKeyExists(checkPermission, "admin") and checkPermission.admin is not 0>
			<CF_RelayNavMenuItem MenuItemText="Edit Promotion" CFTemplate="../admin/promotions/promotionEditForm.cfm?CampaignID=#TradeUpcampaignID#&newrecord=1" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Edit PopUps" CFTemplate="../admin/promotions/promotionPopUP.cfm?frm_CampaignID=#TradeUpcampaignID#">
			<CF_RelayNavMenuItem MenuItemText="Edit Products" CFTemplate="../orders/updateProducts.cfm?CampaignID=#TradeUpcampaignID#">
		</cfif>
	</cfif>

	
	
</CF_RelayNavMenu>
