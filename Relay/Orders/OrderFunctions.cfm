<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			opportunityFunctions.cfm
Author:				KAP
Date started:		2003-09-03
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version
2007/04/27			NJH 		created new order functions, commented out the old ones
2008/09/25			NYB 		CR-SNY658 - added if statement - same as in NewReportOrdersTask.cfm that hides select boxes - 
								as these functions are available without the select boxes

Possible enhancements:


 --->

<!--- 
	NJH 2007/04/27 commented out as it's not being used. Added the form below containing the order functions.
<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_OpportunityFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

//comTableFunction.functionName = "&nbsp;Opportunity Letter";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "/content/mergeDocs/MSWordLetterTemplate.cfm?personid=2&ignoreExtraneousParameter=";
//comTableFunction.windowFeatures = "width=1024,height=800,toolbar=1,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
//comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Move checked opportunities to a different organisation";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/moveOppToDiffOrg.cfm?frmOpportunityIDs=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Merge checked opportunities";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/mergeOpportunities.cfm?frmOpportunityIDs=";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_Tagging";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_TagAllOpportunitiesOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(true));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_UntagAllOpportunitiesOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(false));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();
</cfscript> --->

<!--- NJH 2011/10/24 - LID 7656 put the if statement around the whole form at the moment until there are more options. At the moment, if this variable
hasn't been defined, you get an orders function dropdown without being able to do anything. So, don't show if the variable hasn't been defined. --->
<cfif isDefined("relaywareManagedCampaignIdList") and relaywareManagedCampaignIdList neq "">
<form name="ordersFunctions">
	<select name="select1" size=1 STYLE="font-family: Arial, Helvetica, sans-serif; font-size: 10px;" onchange="doDropDown(document.ordersFunctions.select1)">
	<option value="" selected>Phr_Sys_OrderFunctions</option>
	<option value=none>------------------
	<CFOUTPUT>
	<option value=none>Phr_Sys_Tagging
	<option value="JavaScript: doChecks('Orders',true)">&nbsp Phr_Sys_TagAllOrdersPage
	<option value="JavaScript: doChecks('Orders',false)">&nbsp Phr_Sys_UnTagAllOrdersPage
	<!--- NYB 2008/09/25 CR-SNY658 - added if statement - same as in NewReportOrdersTask.cfm --->
	<cfif isDefined("relaywareManagedCampaignIdList") and relaywareManagedCampaignIdList neq "">
			<option value="JavaScript: checks=saveChecks('Orders') ; if (checks!='0')  { newWindow = void(openWin( '/orders/updateOrderStatus.cfm?frmOrderIds='+checks<cfif isDefined('OrderNumber')>+'&orderNumber=#htmleditformat(OrderNumber)#'</cfif><cfif isDefined('SKU')>+'&SKU=#htmleditformat(SKU)#'</cfif><cfif isDefined('CompanyName')>+'&CompanyName=#htmleditformat(CompanyName)#'</cfif>,'PopUp','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Order')}">Phr_Sys_updateOrderStatus
			<option value="JavaScript: checks=saveChecks('Orders') ; if (checks!='0')  { newWindow = void(openWin( '/orders/updateShipperInfo.cfm?frmOrderIds='+checks<cfif isDefined('OrderNumber')>+'&orderNumber=#htmleditformat(OrderNumber)#'</cfif>,'PopUp','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Order')}">Phr_Sys_updateOrdershipperinfo
	</cfif>
	</CFOUTPUT>
	</select>
	<cf_selectScrollJS formName="ordersFunctions" selectBoxName="select1" onChangeFunction="doDropDown(document.ordersFunctions.select1)">  
</form>
</cfif>

