<!--- �Relayware. All Rights Reserved 2014 --->
<!--- It does the following:
	Calls up a screen to allow the user to enter deliverydetails etc and confirm the order items
	This is carried out by including displayOrder.cfm with frmDisplayMode = edit
	(note that if the order has already been confirmed, it comes up in read Only mode)
	Also adds the top and bottom borders

WAB 2000-11-30  the 'orderConfirmed' bit of the command submitForm('UpdateHeader,orderconfirmed','confirmOrder') had disappeared whic hmeant that orders that were being confirmed by the user were not being set to status 2 in the database.
CPS 2001-02-23  Display the phrase "phr_Order_PleaseTakeAMomentToCheck" before the 'Save and Continue' button
CPS 2001-05-04  Display the phrase "phr_Order_ChangeItemsInOrder" according to the switch ShowChangeItemsInOrder - 
              This prevents the bug for In Focus orders that use 'dummy' products whereby users hit the 'Back' button after the order was 
			  placed and then hit 'Change Items In Order'. This resulted in the order value defaulting to the value of the dummy order rather
			  than the value stored in the WDDX packet 
17 Jun 05	AJC		Added phraseprefix for Order_PleaseTakeAMomentToCheckNoEdit
 2009/06/23    WAB    RACE
 --->


<!--- check if the template is being called directly - include body tags if so---> 
<cfparam name="showBodyTags" default="false">
<cfif getbasetemplatepath() is GetCurrentTemplatePath()>
	<cfset showBodyTags = true>
</cfif>
<cfif showBodyTags>

<cf_head>
<cf_title>
</cf_title>
</cf_head>
	
</cfif>

<cfsetting enablecfoutputonly="no">

<!--- 
WAB 2009/06/24 removed as part of Race Security
<cfif not (isdefined('parentApplicationInclude') and parentApplicationInclude eq "No")>
	<cfinclude template="application .cfm">
</cfif>
--->
<CFPARAM name="msg" default="">
<CFPARAM name="phrasePrefix" default="">
<CFPARAM name="useButtons" default="true">

<!--- Check wether this page has been called by Eval Orders Programme or Softbanx Report Program --->
<CFIF IsDefined("tmpOrderID")>
	<CFSET currentorderid = tmpOrderID>
</CFIF>
<!--- check that order status is still "quote", otherwise can't be edited, unless called via softbanx --->
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
	select 
		o.*,
		p.PromoID,
		p.BorderSet
	from 
		orders as o,
		promotion as p
	where 
		orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and p.promoid = o.promoid
</CFQUERY>

<cfparam name="promoid" default="#getOrderHeader.PromoID#">

<CFIF IsDefined("transAuthCode") and transAuthCode EQ 0>
	<CFSET msg= "phr_Order_NotAuthorised_#getorderheader.paymentmethod#">
</CFIF>

<cf_translate>
<cf_DisplayBorder> 

<!---
<CFIF  not	isdefined("frmInViewer")>

	<CFINCLUDE TEMPLATE="TopMenuBar.cfm">	
	<TD WIDTH="503" VALIGN="top">
	
</CFIF>
--->

<CFIF request.relayCurrentUser.usergroupid is 84>
	<CFSET thisEditableStatuses = SuperUserEditableOrderStatuses>
<CFELSE>
	<CFSET thisEditableStatuses = EditableOrderStatuses>
</CFIF>

<!--- Test to see if this order is in an editable state.
	thisEditableStatuses is set above an is determined based on some settings in application .cfm
 --->
<CFIF listfind(thisEditableStatuses,getorderheader.orderstatus) IS 0 AND NOT IsDefined("tmpOrderID")>
	<CFSET msg=msg & "This is a confirmed order and cannot be edited<BR>">
	<SCRIPT type="text/javascript">
		<!--- this function is used by the viewer code --->
		function jsSubmit()  {
				document.mainForm.submit()
		}
	</SCRIPT>
	<CFSET frmdisplaymode="view">
	<CFINCLUDE template="displayorder.cfm">
<CFELSE>
	<SCRIPT type="text/javascript">
		<!--- this function is used by the viewer code --->
		function jsSubmit()  {
				document.mainForm.submit()
		}
	</SCRIPT>
	<!--- if the orderstatus is not an editable status  --->
	<CFSET frmdisplaymode="edit">

	<CFINCLUDE template="displayorder.cfm">
	
	<cfoutput>
		<!--- CPS - Display the phrase that tells the user to check the order details and then press one of the
		      two buttons below. This has been added as a result of users creating orders for the 'In Focus' 
			  promotions but failing to 'Save and Continue' the order. As a result the orders are left in the
			  status of 'Quote' --->
				<!--- CPS 2001-05-04 - display the relevant message dependant upon ShowChangeItemsInOrder --->			
				<!--- 2005/06/17 AJC START:P_SNY011_VIP phraseprefix added --->
				<p><B><FONT COLOR="red"><CFIF ShowChangeItemsInOrder>phr_#htmleditformat(phrasePrefix)#Order_PleaseTakeAMomentToCheck<CFELSE>phr_#htmleditformat(phraseprefix)#Order_PleaseTakeAMomentToCheckNoEdit</CFIF></FONT></B></p>
				<!--- 2005/06/17 AJC END:P_SNY011_VIP phraseprefix added --->
				
			<!--- CPS 2001-05-04  Display the phrase "phr_Order_ChangeItemsInOrder" according to the switch ShowChangeItemsInOrder --->
	<div class="form-group row">
		<div class="col-xs-12 col-sm-9 col-sm-offset-3">
			<CFIF ShowChangeItemsInOrder>
				<A class="btn btn-primary profileBtn" HREF="JavaScript:submitForm('UpdateHeader','orderItems');">phr_#htmleditformat(phrasePrefix)#Order_ChangeItemsInOrder</A>	
			</CFIF>
			<CFIF getOrderHeader.showOrderConfirmationScreen is 1>
	

<!---
WAB 2005-01-10 This bit of script didn't seem to work very well, and as it happens I had a standard function that does what is required anyway
			so I have commented it out
			
			I have also moved the functionality into the verifyform function in displayorder.cfm
					<script>
					function getSelectedRadio(buttonGroup) {
					   // returns the array number of the selected radio button or -1 if no button is selected
					   if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
					      for (var i=0; i<buttonGroup.length; i++) {
					         if (buttonGroup[i].checked) {
					            return i
					         }
					      }
					   } else {
					      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
					   }
					   // if we get to this point, no radio button is selected
					   return -1;
					} // Ends the "getSelectedRadio" function
					
					function getSelectedRadioValue(buttonGroup) {
					   // returns the value of the selected radio button or "" if no button is selected
					   var i = getSelectedRadio(buttonGroup);
					   if (i == -1) {
					      return "";
					   } else {
					      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
					         return buttonGroup[i].value;
					      } else { // The button group is just the one button, and it is checked
					         return buttonGroup.value;
					      }
					   }
					} // Ends the "getSelectedRadioValue" function
					</script>
					<!--- to get a confirmation screen we update the details and then go to getConfirmation.cfm --->
					<A HREF="JavaScript:if (document.mainForm.hiddenTotal.value != 0 && getSelectedRadioValue(document.mainForm.PaymentMethod) == 'VP'){alert('Payment Method Error: Please select an alternative Payment Method if Total Due for Payment not zero.');} else {if (verifyForm()==true) {submitForm('UpdateHeader','Getconfirmation')};}">phr_Order_SaveContinue</A>	 


 --->

					<!--- to get a confirmation screen we update the details and then go to getConfirmation.cfm --->
					<A class="btn btn-primary" HREF="JavaScript:if (verifyForm()==true) {submitForm('UpdateHeader','Getconfirmation')};">phr_#htmleditformat(phraseprefix)#Order_SaveContinue</A>	 


				<CFELSE>
					<!--- miss out the confirmation screen so we update the details, do a final check and then go to confirmOrder.cfm --->
					<cfif UseButtons>
					<CF_INPUT type="Button" value="phr_#phraseprefix#Order_SaveContinue" onclick="if (verifyForm()==true) submitForm('OrderConfirmed','confirmOrder');" class="btn btn-primary OrdersFormbutton">
					<cfelse>
					<A class="btn btn-primary profileBtn" HREF="JavaScript:if (verifyForm()==true) {submitForm('OrderConfirmed','confirmOrder')}">phr_#htmleditformat(phraseprefix)#Order_SaveContinue</A>
					</cfif>
				</cfif>
			</div>
		</div>
	</cfoutput>
	
</cfif>


<!--- <CFIF  not	isdefined("frmInViewer")>

	</TD>

	<CFINCLUDE TEMPLATE="BottomMenuBar.cfm">	
	
</CFIF> --->

</cf_DisplayBorder> 
</cf_translate>
<cfif showBodyTags>
	  

</cfif>
 


