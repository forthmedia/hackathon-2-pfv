<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		addOrder4SKU
Author:			SWJ
Date created:	2002-07-29

	Objective - shows the order history for the current userid

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

Enhancement still to do:



--->
<CFIF IsDefined("url.eid")>
	<CFSET eID=url.eid>
</CFIF>
<CFIF not isDefined("prodid")>
	<p>You must provide a prodid to place an order</p>
	<CF_ABORT>
</CFIF>

<cfscript>
	qPersonsProduct = application.com.relayEcommerce.getPersonsProductFromSet(request.relayCurrentUser.personid,prodid);
</cfscript>

<!--- not sure what this is supposed to do
<CFLOOP QUERY="qPersonsProduct">
	<CFSET "#sku#_ProductID"=ProductID>
	<CFSET "#sku#_SKU"=SKU>
	<CFSET "#sku#_Description"=Description>
	<CFSET "#sku#_ListPrice"=ListPrice>
	<CFSET "#sku#_DiscountPrice"=DiscountPrice>
	<CFSET "#sku#_PriceISOCode"=PriceISOCode>
</CFLOOP> --->

<cfset productIDList = valuelist(qPersonsProduct.productID)>
<CFQUERY NAME="getProductDetails" datasource="#application.siteDataSource#">
SELECT promotion.Basecurrency, promotion.PromoID, product.ListPrice, product.PriceISOCode,
    Product.ProductID, ProductGroup.Description, 
    Product.SKU
FROM Product 
	INNER JOIN ProductGroup ON Product.ProductGroupID = ProductGroup.ProductGroupID
     INNER JOIN promotion ON Product.CampaignID = promotion.CampaignID
	 where Product.productID  in ( <cf_queryparam value="#productIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<CFINCLUDE TEMPLATE="/relayware/RelayQueries/qryPersonData.cfm">


<CFPARAM NAME="CURRENTCOUNTRY" DEFAULT="#thiscountry#">
<CFPARAM NAME="CURRENTORDERID" DEFAULT="0">
<CFPARAM NAME="FRMDISPLAYMODE" DEFAULT="edit">
<CFPARAM NAME="FRMINVIEWER" DEFAULT="1">
<CFPARAM NAME="FRMORDERCURRENCY" DEFAULT="#getProductDetails.PriceISOCode#">
<CFPARAM NAME="FRMPROMOID" DEFAULT="#getProductDetails.PromoID#"><!--- needs query --->
<CFPARAM NAME="THISPRODUCTGROUP" DEFAULT="#getProductDetails.Description#">
<CFPARAM NAME="THISPRODUCTGROUP1" DEFAULT="#getProductDetails.Description#">
<CFPARAM NAME="PAGELOADED" DEFAULT="#now()#">
<CFPARAM NAME="FRMTASK" DEFAULT="new">
<CFPARAM NAME="frmPersonID" DEFAULT="#thispersonID#">
<CFPARAM NAME="FRMNEXTPAGE" DEFAULT="">
<CFPARAM NAME="SKUSONPAGE" DEFAULT="#valuelist(qPersonsProduct.productID)#">
<CFPARAM NAME="SKUID" DEFAULT="#qPersonsProduct.productID#">
<cfloop query="getProductDetails">
	<CFPARAM NAME="price_#productID#" DEFAULT="#ListPrice#">
	<CFPARAM NAME="quant_#productID#" DEFAULT="1">
</cfloop>

<CFPARAM name="requiredstatus" default="#RealOrderStatuses#">
<CFPARAM NAME="frmLocationID" DEFAULT="#thisPersonLocID#">

<cfset productIDListLen = listlen(productIDList)>
<CFQUERY NAME="isThereAQuote" datasource="#application.siteDataSource#">
	SELECT o.orderdate,o.lastupdated, o.OrderID,count(itemid) as items FROM Orders o INNER JOIN OrderItem i ON o.orderID = i.orderID
	WHERE o.personid =#request.relayCurrentUser.personid# AND o.orderstatus=1 
		AND o.orderID in (select orderid from orderitem where productid  in ( <cf_queryparam value="#productIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			group by orderid
			having count(*) = #productIDListLen#)
	GROUP BY o.OrderID,o.lastupdated,o.orderdate
</CFQUERY>


<CFIF isDefined("neworder")>
	<cfinclude template="/orders/orderitemstask.cfm">
<CFELSEIF isDefined("orderid")>
	<CFQUERY NAME="Quote" datasource="#application.siteDataSource#">
		SELECT lastupdated FROM Orders WHERE orderid =  <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<CFSET ORDERLASTUPDATED = Quote.lastupdated>
	<CFSET FRMTASK = "update">
	<CFSET CURRENTORDERID=orderid>
	<cfinclude template="/orders/orderitemstask.cfm">
<CFELSE>

	
	<CFIF isThereAQuote.recordcount EQ 1>
		<!--- If there is only one quote live for this person on this product --->
		<CFIF isThereAQuote.items EQ productIDListLen>
			<!--- and there is only one item in the order 
				take the user to edit this order
			--->
	
			<CFSET message="phr_AlreadyOrderAtQuote">
			<CFSET ORDERLASTUPDATED = isThereAQuote.lastupdated>
			<CFSET FRMTASK = "update">
			<CFSET CURRENTORDERID=isThereAQuote.orderid>
			<cfinclude template="/orders/orderitemstask.cfm">
		<CFELSE>
			<!--- and there is more than one item in the order 
				ask the user if they'd like to complete this order, or 
				start a new one.		
			--->
			<!--- <cf_displayBorder> --->
			<CFSETTING enablecfoutputonly="no">
			<cf_translate>			
			<CFOUTPUT>
			<TABLE>
			<TR><TD><div align="justify"><FONT COLOR="red"><B>phr_AlreadyOrderAtQuoteOnePlusItem</B></FONT></div></TD></TR>
			<TR><TD>&nbsp;</TD></TR>
			
			<!--- eid trap has been added to ensure page loads within borderset if current EID exists --->
			<cfif #IsDefined("url.eid")#>
			<TR><TD><A Href="et.cfm?#request.query_string#&orderid=#isThereAQuote.orderid#">phr_CompleteThisOrder</A> - phr_OrderNumber #htmleditformat(isThereAQuote.orderid)# phr_createdon #dateformat(isThereAQuote.orderdate,"dd mmm yyyy")#</TD></TR>
			<TR><TD><A Href="et.cfm?#request.query_string#&neworder=1">phr_GenerateNewOrder</A></TD></TR>
			<cfelse>
			<TR><TD><A Href="/orders/addorder4SKUinclude.cfm?#request.query_string#&orderid=#isThereAQuote.orderid#">phr_CompleteThisOrder</A> - phr_OrderNumber #htmleditformat(isThereAQuote.orderid)# phr_createdon #dateformat(isThereAQuote.orderdate,"dd mmm yyyy")#</TD></TR>
			<TR><TD><A Href="/orders/addorder4SKUinclude.cfm?#request.query_string#&neworder=1">phr_GenerateNewOrder</A></TD></TR>
			</cfif>
			</TABLE>
			</CFOUTPUT>
			</cf_translate>			
			<!--- </CF_DisplayBorder> --->
		</CFIF>
	
	<CFELSEIF isThereAQuote.recordcount GT 1>
		<!--- If there is more than one quote live for this person on this product 
			ask the user to either choose one order or start another
		--->
			<!--- <cf_displayBorder> --->
			<CFSETTING enablecfoutputonly="no">
			<cf_translate>			
			<TABLE>
			<CFOUTPUT><TR><TD><div align="justify"><FONT COLOR="red"><B>phr_AlreadyOrderAtQuoteOnePlusOrder</B></FONT></div></TD></TR></CFOUTPUT>
			<TR><TD>&nbsp;</TD></TR>
			<CFOUTPUT QUERY="isThereAQuote">
			<TR><TD><A Href="addorder4SKUinclude.cfm?#request.query_string#&orderid=#orderid#">phr_CompleteThisOrder</A> - phr_OrderNumber #htmleditformat(orderid)# phr_createdon #dateformat(orderdate,"dd mmm yyyy")#</TD></TR>
			</CFOUTPUT>
			<CFOUTPUT><TR><TD><A Href="addorder4SKUinclude.cfm?#request.query_string#&neworder=1">phr_GenerateNewOrder</A></TD></TR></CFOUTPUT>
			</TABLE>
			</cf_translate>			

			<!--- </CF_DisplayBorder> --->
	<CFELSE>
	<!--- If there are no quotes live for this person on this product 
		generate a new order for this product only
	--->
		<cfinclude template="/orders/orderitemstask.cfm">
	</CFIF>

</CFIF> 



