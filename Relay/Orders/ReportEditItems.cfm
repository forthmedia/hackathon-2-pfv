<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-11-16  DAN PROD2015-364 - products country restrictions

--->

<CFPARAM name="PromoID" default="">
<cfif IsDefined("Form.PromoID")>
	#htmleditformat(PromoID)# = #htmleditformat(Form.PromoID)#>
</cfif>

<!--- Get the Order Details --->
<CFQUERY NAME="getOrderDetails" datasource="#application.sitedatasource#">
	SELECT * 
	FROM Orders 
	WHERE Orders.OrderID =  <cf_queryparam value="#tmpOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
		select 	p.firstname,
				p.lastname,
				p.personid,
				p.locationid,
				l.sitename,
				l.countryid,
				cg.countrygroupid
		from	person as p, location as l, countrygroup as cg
		Where 	p.personid =  <cf_queryparam value="#getOrderDetails.PersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and		p.locationid=l.locationid
		and		l.countryid=cg.countrymemberid
	</CFQUERY>
	
<CFIF getpersondetails.recordcount IS 0>
	Could not find person
	<CF_ABORT>
</CFIF>
	
<CFSET currentcountry=getpersondetails.countryid>
<CFSET currentregion=valuelist(getpersondetails.countrygroupid)>

<cfset PromoID= getOrderdetails.promoid>
	
<!--- get all active products --->
<CFQUERY NAME="getProductList" datasource="#application.sitedatasource#">
<!---SELECT 	PG.Description as productgroup, 
		p.Description, 
		p.listPrice,
		p.Price,
		pp.PromoPrice,
		pp.DiscountPrice,
		prm.PromoName,
		<CFIF dbtype IS "SQL"> 
		UPPER(p.SKU) as SKU,
		<CFELSE>
		UCASE(p.SKU) as SKU,
		</cfif>
		p.productID
FROM ProductInventory AS PG, ProductInventory AS p, productcountry as pc, ProductPromo as pp, Promotion as prm
WHERE pc.Countryid In (0,#currentcountry#,#currentregion#)  
AND pp.promoID = '#PromoID#'--->
<!--- WAB altered 
AND p.ProductID *= pp.ProductID
AND prm.PromoID *= pp.PromoID
 --->
<!---AND p.SKU = pp.SKU
AND prm.PromoID = pp.PromoID
AND p.ParentID=pg.productID
AND	p.sku=pc.sku
AND p.productID not in ( ---><!--- This should exclude all items where there is a country specific item--->
<!---				SELECT p1.productID
				FROM ProductInventory AS p1, productcountry as pc1, ProductInventory AS p2 , productcountry as pc2
				WHERE p1.SKUGroup = p2.SKUGroup
				and p1.sku=pc1.sku
				and p2.sku=pc2.sku
				AND Pc1.Countryid=0 
				AND pc2.Countryid in (#currentcountry#,#currentregion#))
order by p.parentid --->

<!--- Using the ProductList View these days --->
SELECT p.* 
FROM vProductList p
left join vCountryScope vcs on vcs.entity='product' and vcs.entityid = p.productid and vcs.countryid = <cf_queryparam value="#currentcountry#" cfsqltype="CF_SQL_INTEGER">
WHERE 1=1 <!--- Countryid In (0,#currentcountry#,#currentregion#) --->
and (p.hascountryScope = 0 or vcs.countryid is not null)
AND PromoID =  <cf_queryparam value="#PromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
AND ProductID NOT IN (
	SELECT Product.ProductID
	FROM Product INNER JOIN Product AS Product_1 ON Product.SKUGroup = Product_1.SKUGroup
	WHERE Product.CountryID=0 AND Product_1.CountryID In (#currentcountry#,#currentregion#))

Order By ProductGroup
</CFQUERY>


<CFSET currentlocationid=getpersondetails.locationid>

<!--- Get the previous Order SKU's disallowed but not the current order ones --->
<CFQUERY NAME="getPreviousOrderSKUsDisallowed" datasource="#application.sitedatasource#">
SELECT 	i.SKU
FROM 	orderitem as i, orders as o
WHERE	i.orderid=o.orderid
AND		o.locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND		i.OrderID <>  <cf_queryparam value="#tmpOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND		i.active=1
</CFQUERY> 

<CFSET DisallowedSKUs=valuelist(getPreviousOrderSKUsDisallowed.sku)>

<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 	*
	FROM vOrderItems
	WHERE	orderid =  <cf_queryparam value="#tmpOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
	AND 	active = 1
</CFQUERY>

<CFSET currentorderitems=valuelist(getorderitems.productid)>
<CFSET currentorderitemsquantity=valuelist(getorderitems.quantity)>
	
<SCRIPT LANGUAGE="JavaScript">

<!--
	function verifyForm(form) {
		var form = document.thisForm;
		//Call the function to check wether at least ONE check box has been clicked
		if (verifyCheckBoxClicked()) {
				form.submit()
			}
			else {
				window.alert("You must choose a least ONE product")
			}
	}

	//This function checks wether at least one checkbox has been clicked
	function verifyCheckBoxClicked() {
		var form = document.thisForm;
			for (i=0; i<form.chkProduct.length; i++){
				if (form.chkProduct[i].checked=="1") {
					return true
				} 
			}	
			return false
	}

	function doCheck(item,id) {
			// when an item is checked, set quantity to 1, and update grand total
			var form = document.thisForm;
			quantityvariable='quant_'+id;
			
			//I search through all the elements looking for the hidden field with the name referencing this SKU
			//must be another way of doing it, but haven't found it yet
			
			
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == quantityvariable) {
					if (item.checked==true)  {  
						thiselement.value=1
								}else{
						thiselement.value=0
					}
			}
		}
	

		doTotalOrderValue()


	}


	function doTotalOrderValue() {	
		//work out grand total of order and display in text box
		var form = document.thisForm;
		totalordervalue=0
		
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];


			if (thiselement.name.substring(0,5) == 'price') {
				id=thiselement.name.substring(6,20)
				price = thiselement.value
				quantity = eval('form.quant_'+id+'.value')
//				totalordervalue = totalordervalue +	(parseInt(price,10) * parseInt(quantity,10) )
//				previous construction was missing out the decimals!
				totalordervalue = totalordervalue +	(parseInt(price * 100) * parseInt(quantity) /100)
			}
		}

			form.TotalPrice.value=formatNum(totalordervalue,2)
			jsTotalValue=totalordervalue
	
	}
	
		// Added formatting function so numbers look better in text box total.
	function formatNum (expr,decplaces)
	{
		var str=(Math.round(parseFloat(expr) * Math.pow(10,decplaces))).toString()
		while (str.length <= decplaces)
		{
			str="0" + str
		}
		var decpoint = str.length - decplaces
		return str.substring(0,decpoint) + "." + str.substring(decpoint,str.length)
	}	
//-->

</SCRIPT>

<CFSET SKUsonpage="">		

<FORM METHOD="post" NAME="thisForm" ACTION="ReportUpdateItems.cfm">
<TABLE>

<TR>
<TD colspan="4">
<FONT FACE="Arial,Chicago" SIZE="2">This order was created for
<BR>
<FONT FACE="Arial,Chicago" SIZE="2"><CFOUTPUT>#htmleditformat(getpersondetails.FirstName)# #htmleditformat(getpersondetails.lastname)#, #htmleditformat(getpersondetails.sitename)#</CFOUTPUT>
</td>
</tr>
<TR>
<TD colspan="4"><FONT FACE="Arial,Chicago" SIZE="2">Please select the products you wish to purchase

</td>
</tr>



<TR>
<TD>&nbsp&nbsp&nbsp&nbsp</td>
<TD><FONT FACE="Arial,Chicago" SIZE="2"><B>Product Code</b></td>
<TD><FONT FACE="Arial,Chicago" SIZE="2"><B>Description</b></td>

<TD><FONT FACE="Arial,Chicago" SIZE="2" align="right"><B>Price</b></td>

</tr>

<CFOUTPUT query="getproductlist" group="productgroup" >
<TR>
<TD colspan="4"><FONT FACE="Arial,Chicago" SIZE="2"><B>#htmleditformat(ProductGroup)#</b></FONT></td>
</tr>
	<CFOUTPUT>
		<TR>
		<TD>&nbsp;</td>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">#htmleditformat(SKU)#</FONT></td>
		<TD><FONT FACE="Arial,Chicago" SIZE="2">#htmleditformat(Description)#</FONT></td>
		<TD nowrap align="right"><FONT FACE="Arial,Chicago" SIZE="2">(#htmleditformat(PriceISOCode)#) #decimalformat(DiscountPrice)#</FONT></td>
<!---		<cfif #PromoPrice# GT 0>
			<TD align="right"><FONT FACE="Arial,Chicago" SIZE="2">#dollarformat(PromoPrice)# <br> (#PromoName#)</FONT></td>
		<cfelse>
			<TD align="right"><FONT FACE="Arial,Chicago" SIZE="2">#dollarformat(ListPrice)#</FONT></td>
		</cfif> --->
		<CFIF listcontainsnocase(DisallowedSKUs,sku) IS 0>
			<CFIF listfind(currentorderitems,productid)>
				<TD>
					<CF_INPUT TYPE="checkbox" NAME="chkProduct" onclick="doCheck(this,'#productid#');" CHECKED>
				</TD>
			<CFELSE>
				<TD>
					<CF_INPUT TYPE="checkbox" NAME="chkProduct" onclick="doCheck(this,'#productid#');">
				</TD>
			</CFIF>
		<CFELSE>
			<TD>Previously Ordered</td>
		</CFIF>
		<!--- leaving these variables here should ensure that any disallowed items which happen to be on a quote are deleted --->
		<INPUT TYPE="HIDDEN" NAME="quant_#productid#" VALUE="<CFIF listcontains(currentorderitems,productid) is not 0>#listgetat(currentorderitemsquantity,listcontains(currentorderitems,productid))# <CFELSE>0</cfif>">	
		<CF_INPUT TYPE="HIDDEN" NAME="price_#productid#" VALUE="#Discountprice#">	
		<CFSET SKUsonpage=listappend(SKUsonpage,#productid#)>
	</CFOUTPUT>
		</tr>
</cfoutput>
	<TR>
	<TD colspan="4"><FONT FACE="Arial,Chicago" SIZE="2"><B>Total Price</B></td>
	<TD align="right"><INPUT TYPE="text" NAME="TotalPrice" VALUE="0" size="7" DISABLED></td>

	</tr>
 	<TR> 
		<TD colspan="5" align="center">
			<A HREF="JavaScript:verifyForm(this.form);"><IMG SRC="<CFOUTPUT>#htmleditformat(URLRoot)#</cfoutput>/images/buttons/c_continue_e.gif" WIDTH=120 HEIGHT=22 ALT="Continue" BORDER="0"></A>
		</TD>	
	</TR>
</table>


	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="skusonpage" VALUE="#skusonpage#">	
	<CF_INPUT TYPE="HIDDEN" NAME="currentorderid" VALUE="#tmpOrderID#">	
	<CF_INPUT TYPE="HIDDEN" NAME="PromoID" VALUE="#PromoID#">

	<CF_INPUT TYPE="HIDDEN" NAME="currentcountry" VALUE="#currentcountry#">
	<CF_INPUT TYPE="HIDDEN" NAME="currentregion" VALUE="#currentregion#">
	</cfoutput>


<!--- calculates and displays order total on initial load--->
<SCRIPT LANGUAGE="JavaScript">
	doTotalOrderValue();
</SCRIPT>
</FORM>

