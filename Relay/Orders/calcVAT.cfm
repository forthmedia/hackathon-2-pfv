<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Calculate VAT on an Order --->
<!--- takes parameters ordercountry, netValue, VATNumber (optional) --->
<!---  --->

<CFPARAM name="VATNumber" default="">

<CFQUERY NAME="getCountryVATDetails" datasource="#application.sitedatasource#">			 
	select 
		VATRAte, 
		AllowVATReduction, 
		VATFormat 
	from 
		country
	where 
		countryid =  <cf_queryparam value="#orderCountry#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
			 

<CFSET VATrate = getCountryVATDetails.VATRate>
<CFIF not isnumeric(VATRate)>
	<CFSET VATRate=0>
</cfif>

<CFIF getCountryVATDetails.AllowVATReduction is not 0>
	<!--- validate format of vat number --->

	<!--- strip out all non numbers/letters --->
	<CFSET VATNumber = REReplace(VATNumber, "[^0-9^A-Za-z]", "", "ALL")>

	<CFIF #REFind(getCountryVATDetails.VATFormat,VATNumber)# is true>
		<!--- VAT number OK --->
		<CFSET VATrate = 0>
	<CFELSE>	
		<!--- VAT number not OK --->
	</cfif>

<CFELSE>
		<!--- No VAT reduction--->

</CFIF>

<cfif ShowVATCalculation>
	<CFSET TotalVAT = VATRate/100 * #netValue#>
<cfelse>
	<CFSET TotalVAT = 0>
	<cfset VatRate = 0>
</cfif>
			

