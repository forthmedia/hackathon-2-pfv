<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Processes form on displayorder.cfm

Mods
WAB 030200  Removed orderHistory queries, now done as a tigger
WAB 030200  Altered how address details are updated.
CPS 250401  Incorrect points amount being passed to RWCreateTransaction when using WDDX Packet
            This only occurs when editing a 'Quote' order as the value that is passed in is the current order value
			and not the amended figure input by the user.
JC	091204	Manual redirection to WorldPay commented out as considered obsolete
SSS 2008-04-21  Have added a file exists code so that functions can be added for clients specific stuff after a order is
              submitted.
WAB  2009/06/24 RACE
NJH 2009/07/02	P-FNL069 - got user from currentUser structure rather than cookie. Scoped most variables to be form variables, as I could not find
				any other reference to displayOrderTask.cfm other then a form posting to this file in displayOrder.cfm
NYB	2011-03-14	LHID5528
2011/05/06 PPB LID5378: switched request.setPendingStatus to use setting incentive.pointsAboveWhichOrderConfirmationRequired
2011/06/12  NJH	Changes for Sony Security (check points balance before deducting points)
2011/09/12	WAB LID 7737 - Problem with orders having duplicate entries in the rwtransaction table

 --->

<!---
WAB 2009/06/24 removed as part of Race Security

<cfif not (isdefined('parentApplicationInclude') and parentApplicationInclude eq "No")>
	<cfinclude template="application.cfm">
</cfif>
 --->


<CFSET UpdateTime=now()>
<!--- NJH 2009/07/02 P-FNL069  <CFSET user=#request.relayCurrentUser.usergroupid#> --->
<CFSET user=request.relayCurrentUser.personID>

<!--- certain functions not shown to external users, so set up a variable - (test for external user is not very robust) --->
<CFIF user is application.WebUserID>
	<CFSET internalUser=false>
<CFELSE>
	<CFSET internalUser=true>
</cfif>

<CFPARAM name="Msg" default="">
<CFPARAM name="phrasePrefix" default="">

<!--- 2011/09/12
	WAB LID 7737 - Problem with orders having duplicate entries in the rwtransaction table
	Have shown that could have occurred from pressing submit button twice (but would need server to be running slow at the time).
	Have added a lock around this code so that only one submit will be processed at a time
	the order status will have been updated before the second submit is processed
	(could also implement code to detect form being submitted more than once)
 --->

<cflock name="displayOrderTask_#application.applicationname#_#currentOrderID#" timeout = 10 throwOnTimeout=true>

	<!--- get details of existing order --->
	<CFINCLUDE template="qryorderdetails.cfm">

	<cfset ShowDeliveryCalculation = GetOrderHeader.ShowDeliveryCalculation>
	<cfset ShowVATCalculation = GetOrderHeader.ShowVATCalculation>
	<cfset ShowPurchaseOrderRef = GetOrderHeader.ShowPurchaseOrderRef>
	<cfset UsePoints = GetOrderHeader.UsePoints>

	<!--- CPS 240401 Store the current order value in a variable to be passed to RWCreateTransaction --->
	<CFSET variables.TotalOrderValue = GetOrderValue.Totalvalue>

	<CFIF frmtask Contains "UpdateHeader">

		<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
		Select 	l.countryid
		from 	person as p, location as l
		where	p.personid =  <cf_queryparam value="#getOrderHeader.Personid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and		p.locationid=l.locationid
		</CFQUERY>


		<!--- Calculate Shipping Charges and VAT before updating the header --->
		<CFSET OrderCountry = getPersonDetails.CountryID>
		<CFSET ShippingMethod = form.frmShippingMethod>
		<CFSET ShippingMatrix = getorderHeader.shippingMatrixID>
		<CFSET OrderWeight = getorderHeader.OrderWeight>
		<CFSET VATNUmber = frmVATNumber>

		<CFINCLUDE template="calcShipping.cfm">

		<CFSET NetValue = getorderHeader.ordervalue + ShippingCharge>
		<CFINCLUDE template="calcVAT.cfm">	<!--- returns TotalVAT, VATRate --->

		<CFSET totalamount = NetValue + ShippingCharge + TotalVAT >

	 	<CFQUERY NAME="updateHeader" datasource="#application.sitedatasource#">
			Update Orders
			Set
				lastupdated =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				<CFIF isdefined("form.invcontact")>invcontact=<cf_queryparam value="#form.invcontact#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<CFIF isdefined("form.invsitename")>invsitename=<cf_queryparam value="#form.invsitename#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				delcontact =  <cf_queryparam value="#form.delcontact#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				delsitename =  <cf_queryparam value="#form.delsitename#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
	 			<CFLOOP index="I" from="1" to ="5">
					<CFSET tmpAddressLine = ''>
					<CFIF isdefined("form.invaddress#I#")> <CFSET tmpAddressLine = evaluate("form.invaddress#I#")> </CFIF>
					invaddress#I# =  <cf_queryparam value="#tmpAddressLine#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					<CFSET tmpAddressLine = ''>
					<CFIF isdefined("form.deladdress#I#")> <CFSET tmpAddressLine = evaluate("form.deladdress#I#")> </cfif>
					deladdress#I# =  <cf_queryparam value="#tmpAddressLine#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				</cfloop>
				<CFIF isdefined("form.delPostalCode")>delpostalcode = <cf_queryparam value="#form.delPostalCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,</CFIF>
				<CFIF isdefined("form.invPostalCode")>invpostalcode = <cf_queryparam value="#form.invPostalCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,</CFIF>
			 	delcountryid  =  <CFIF isdefined("form.delCountryid")><cf_queryparam value="#form.delCountryid#" CFSQLTYPE="CF_SQL_INTEGER" ><CFELSE>0</CFIF>,
				invcountryid  =  <CFIF isdefined("form.invCountryid")><cf_queryparam value="#form.invCountryid#" CFSQLTYPE="CF_SQL_INTEGER" ><CFELSE>0</CFIF>,
				contactphone =  <cf_queryparam value="#form.contactphone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				contactfax =  <cf_queryparam value="#form.contactfax#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				contactemail =  <cf_queryparam value="#form.contactemail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<cfif isDefined("form.orderNotes")>orderNotes = <cf_queryparam value="#form.orderNotes#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				ordertype =  <cf_queryparam value="#form.frmOrderType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfif not UsePoints>
					,
					<CFIF isDefined("form.theirordernumber")><CFIF form.theirordernumber IS "">theirordernumber='Not Specified',<CFELSE>theirordernumber=<cf_queryparam value="#form.theirordernumber#" CFSQLTYPE="CF_SQL_VARCHAR" >,</CFIF>	</cfif>
					<CFIF isDefined("form.paymentmethod")><CFIF form.paymentmethod IS "">paymentmethod='Not Specified', <CFELSE>paymentmethod=<cf_queryparam value="#form.paymentmethod#" CFSQLTYPE="CF_SQL_VARCHAR" >,</CFIF></cfif>
					<CFIF isDefined("form.frmDistiLocationID") and form.frmDistiLocationID is not "">distiLocationID = <cf_queryparam value="#form.frmDistiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >,</cfif>
					ShippingMethod =  <cf_queryparam value="#form.frmShippingMethod#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					VATNumber =  <cf_queryparam value="#form.frmVATNumber#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					TaxRate =  <cf_queryparam value="#VATRate#" CFSQLTYPE="CF_SQL_decimal" > ,
					ShippingCharge =  <cf_queryparam value="#ShippingCharge#" CFSQLTYPE="CF_SQL_decimal" > ,
					TaxOnOrder =  <cf_queryparam value="#TotalVAT#" CFSQLTYPE="CF_SQL_money" >
				</cfif>
			Where OrderId =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- NJH 2009/07/09 - only update orders that you have access to if on the portal --->
			<cfif not request.relayCurrentUser.isInternal>
				and personID = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</CFQUERY>


		<!---If this is a promotion which requires Questions--->
		 <CF_aScreenUpdate>

		<CFINCLUDE template = "WDDXFlagOrderItemUpdate.cfm">

		<!--- loop over products and set event payment type --->
		<CFQUERY NAME="GetVoucherPaid" datasource="#application.sitedatasource#">
			select efd.flagid, entityid, vouchercode
			from orders o, orderitem oi, eventproducts ep, eventflagdata efd
			where o.orderid =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and o.orderid = oi.orderid
			and oi.productid = ep.productid
			and ep.flagid = efd.flagid
			and efd.entityid = o.personid
			and regstatus <> 'CANCELLED'
		</cfquery>

		<cfif #GetVoucherPaid.recordcount# NEQ 0>
			<cfset voucherpaid = "">
			<cfloop query="GetVoucherPaid">
				<cfif #vouchercode# NEQ "">
					<cfset voucherpaid = "#ListAppend(voucherpaid,flagid)#">
				</cfif>
			</cfloop>
			<!--- set VP payment type --->
			<cfif #voucherpaid# NEQ "">
				<CFQUERY NAME="UpdatePaymentType" datasource="#application.sitedatasource#">
					UPDATE eventflagdata
					SET lastupdated = getdate(),
					paymenttype='VP',
					T_AuthorisationCode = (select DISTINCT(vouchercode) from orderitem where orderid =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >  and vouchercode <> '')
					WHERE
					flagid  IN ( <cf_queryparam value="#voucherpaid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and entityid =  <cf_queryparam value="#GetVoucherPaid.entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and regstatus <> 'CANCELLED'
				</cfquery>
			</cfif>
		</cfif>
			<CFQUERY NAME="GetFlagIDs" datasource="#application.sitedatasource#">
				select efd.flagid
				from eventflagdata efd,eventproducts ep,orderitem oi
				WHERE oi.orderid =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >
				and oi.productid = ep.productid
				and ep.flagid = efd.flagid
				<!--- JC: 2004-12-22 - added IsDefined --->
				<cfif #IsDefined("voucherpaid")# AND #voucherpaid# NEQ "">
					and efd.flagid  NOT IN ( <cf_queryparam value="#voucherpaid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
				and regstatus <> 'CANCELLED'
			</cfquery>
			<cfif #GetFlagIDs.recordcount# NEQ 0>
				<!--- UPDATE eventflagdata with appropriate payment type --->
				<CFQUERY NAME="UpdatePaymentType" datasource="#application.sitedatasource#">
					UPDATE eventflagdata
					SET lastupdated = getdate(),
					paymenttype=<CFIF isDefined("form.paymentmethod")><CFIF form.paymentmethod IS ""><cf_queryparam value="Not Specified" CFSQLTYPE="CF_SQL_VARCHAR" ><CFELSE><cf_queryparam value="#form.paymentmethod#" CFSQLTYPE="CF_SQL_VARCHAR" ></CFIF></cfif>
					WHERE
						(<cfif #IsDefined("voucherpaid")# AND #voucherpaid# NEQ "">flagid NOT  IN ( <cf_queryparam value="#voucherpaid#" CFSQLTYPE="cf_sql_integer"  list="true"> ) and</cfif> flagid  IN ( <cf_queryparam value="#valuelist(GetFlagIDs.flagid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
					and entityid =  <cf_queryparam value="#GetVoucherPaid.entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and regstatus <> 'CANCELLED'
				</cfquery>
			</cfif>
	</CFIF>

	<cfif frmtask contains "orderconfirmed">

		<!--- final check for disallowed products --->
		<CFSET currentlocationid=getorderheader.locationid>
		<CFSET promoid=getorderheader.promoid>
		<CFINCLUDE template="qrydisalloweditems.cfm">

		<CFSET list="">
		<CFLOOP query="getorderitems">
			<CFIF listcontainsnocase(disallowedskus,sku) IS NOT 0>
				<CFSET listappend(list,sku)>
			</CFIF>
		</CFLOOP>

		<CFIF list is not "">
			<CFSET Msg=Msg&"Unfortunately the following items have already been ordered during this promotion<BR>">
			<CFSET Msg=Msg&"Please edit your order<BR>">
			<CFSET Msg=Msg&replace(list,",","<BR>","ALL")>
			<CFINCLUDE template="editorder.cfm">
			<CF_ABORT>
		</CFIF>

	<!---
		DAM 20000405

		There is no need to do any converting here.
		If we are using points we use the @PointsAmount parameter in the sp ececution
		otherwise we use the @CashAmount
	--->

	<!--- 	<cfset PointsFactor = 1>
		<cfif UsePoints>
			<cfquery name="PointsFactor" datasource="#application.siteDataSource#">
				select
					CurrencyPerPoint
				from
					RWPointsConversion
				where
					PriceISOCode = '#GetOrderItems.PriceISOCode#'
			</cfquery>

			<cfif PointsFactor.recordcount is 1>
				<cfset PointsFactor = PointsFactor.CurrencyPerPoint>
			<cfelse>
				<cfset PointsFactor = 1>
			</cfif> --->

			<!---- spend those points as long as they haven't already confirmed the order...
				WAB 2001-01-25 change points type from SU (spend) to AL (allocate)
			--->
			<!---- START 2011-03-14 NYB LHID5528 added PointsUsed and NewID - requires release of new RWCreateTransaction procedure --->
			<cfset PointsUsed = false>
			<cfset NewID=0>
			<cfif UsePoints>

				<cfif GetOrderHeader.OrderStatus is ConstQuote or GetOrderHeader.OrderStatus is ConstPending>
					<!---- 2011-03-14 NYB LHID5528 replaced query with new function - release RelayPLO 1st --->
					<CFSET organisationID = application.com.RelayPLO.getLocDetails(locationid=#getOrderHeader.locationid#).organisationID>
					<cfset PointsUsed = true>

					<!--- check if individual has the points to spend --->
					<cfset argStruct = structNew()>
					<cfif isDefined("spendIncentivePointsType")>
						<cfset argStruct.spendIncentivePointsType = spendIncentivePointsType>
					</cfif>
					<cfif isDefined("portalViewOnly")>
						<cfset argStruct.portalViewOnly=portalViewOnly>
					</cfif>
					<cfif isDefined("ShowPointsforUnregisteredUser")>
						<cfset argStruct.ShowPointsforUnregisteredUser = ShowPointsforUnregisteredUser>
					</cfif>
					<cfset PointsAvailable = application.com.relayIncentive.getPointsBalanceForPortalUser(argumentCollection=argStruct).balance>


					<cfif TotalOrderValue gt PointsAvailable>
						<cfset Msg="Your total order value (#TotalOrderValue#) exceeds the amount of points available (#PointsAvailable#)."> <!--- WAB 2012-03-13 CASE 427147 had #qPointsBalance.balance# here - which wasn't defined --->

					<cfelse>
						<cftry>
							<cfquery name="spendpoints" datasource="#application.sitedatasource#">
								DECLARE @NewID varchar(50)
								exec @NewID = RWCreateTransaction @OrganisationID =  <cf_queryparam value="#OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > , @TXTypeID = 'AL', @PointsAmount =  <cf_queryparam value="#variables.TotalOrderValue#" CFSQLTYPE="CF_SQL_INTEGER" > , @PriceISOCode =  <cf_queryparam value="#GetOrderItems.PriceISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" > , @OrderID =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > , @PersonID = #request.relaycurrentuser.personid#
								select @NewID as NewID
							</cfquery>
							<cfset NewID = spendpoints.NewID>
							<cfcatch type="any">

								<cfset NewID=0>

								<!---
								WAB 2011/04/27 removed this code and replaced with call to single function which records the error and sends an email all in one
								<cfset line = cfcatch.RootCause.TagContext[1].LINE>
								<cfset diagnostics = "<p>Error running exec RWCreateTransaction in Orders/displayordertask.cfm line #line#.</p>">
								<cfset diagnostics = "#diagnostics#<p>Query Error: #cfcatch.queryError#</p><p>SQL: #cfcatch.Sql#</p>">
								<cfset application.com.errorHandler.insertRelayErrorRecord(diagnostics=diagnostics,line=line)>
								<cfset application.com.errorHandler.emailErrorNotification(error=cfcatch,diagnostics=diagnostics,line=line)>
								--->
								<cfset application.com.errorHandler.recordRelayError_Warning(severity="ERROR",type="RWCreateTransaction Error",caughtError=cfcatch,sendEmail=true)>


								<CFSET Msg="Your order can not be processed at this time">
							</cfcatch>
						</cftry>
					</cfif>

				</cfif>
			</CFIF>
			<!---- END 2011-03-14 NYB LHID5528 added PointsUsed and NewID - requires release of new RWCreateTransaction procedure --->

	<!--- SSS 2008/04/18 This code will add in functions that will occur after a order has been submitted --->
	<cfif fileexists("#application.paths.code#\cftemplates\OrderSubmittedFunctions.cfm")>
		<cfinclude template="/code/cftemplates/OrderSubmittedFunctions.cfm">
	</cfif>


	<!--- 	</cfif> --->


		<!--- check whether the order is already confirmed--->

		<!--- set status to confirmed --->
		<!--- comment out if you want to do lots of testing --->
		<!---  probably ought to check that all the totals add up --->


		<!---

			DAM 30 July 2002

			When we come to confirm an order that is paid by CC we need to divert at this point away
			from updating the order to confirmed, report that the payment transaction failed, and
			display the order for edit.

			SWJ 03 September 2002

			However, this does not work below as it always fails and does not confirm
			the order therfore I have a) added a cfparam to set transcode and
			b) changed the test on frmTask to findNoCase

		--->
		<cfparam name="transAuthCode" type="numeric" default="1">
		<!---- 2011-03-14 NYB LHID5528 added PointsUsed and NewID --->
		<CFIF (not PointsUsed or (PointsUsed and NewID gt 0)) and findNoCase("orderconfirmed",frmtask,1) and IsDefined("transAuthCode") and transAuthCode EQ 1>

			<cfset pointsAboveWhichOrderConfirmationRequired = application.com.settings.getSetting('incentive.pointsAboveWhichOrderConfirmationRequired')>

			<CFQUERY NAME="updateHeader" datasource="#application.sitedatasource#">
				Update Orders
				Set
					<cfif getordervalue.totalvalue GTE pointsAboveWhichOrderConfirmationRequired and pointsAboveWhichOrderConfirmationRequired neq "">		<!--- 2011/05/06 PPB LID5378 --->
						OrderStatus =  <cf_queryparam value="#constPending#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					<cfelse>
						OrderStatus =  <cf_queryparam value="#constConfirmed#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					</cfif>
					lastupdated =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >
				Where OrderId =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<!--- GCC Update Event Record if order includes a single event --->
			<cfquery name="getEvent" datasource="#application.sitedatasource#">
				SELECT     efd.FlagID, efd.EntityID, ed.Title, eg.EventName, eg.EventGroupID
				FROM      eventflagdata efd INNER JOIN
		                     EventDetail ed ON ed.FlagID = efd.FlagID INNER JOIN
		                     EventProducts ep ON efd.FlagID = ep.FlagID INNER JOIN
		                     EventGroup eg ON ed.EventGroupID = eg.EventGroupID
				where
					ep.productid in (select productid from orderItem where orderid =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > )
					and efd.entityid in (select personid from orders where orderid =  <cf_queryparam value="#form.currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfquery>
			<cfif getEvent.recordcount eq 1 and getorderheader.paymentmethod eq "CK">
				<cfquery name = "UpdateEventRegistration" datasource="#application.sitedatasource#">
				Update eventFlagData
				Set
					RegStatus = 'RegistrationSubmitted',
					paymentType =  <cf_queryparam value="#getorderheader.paymentmethod#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					paymentReceived = 0,
					T_AuthorisationCode = NULL,
					lastupdated=getdate(),
					lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >
				where
					flagID =  <cf_queryparam value="#getEvent.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityID =  <cf_queryparam value="#getEvent.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfif>
		<CFELSE>
			<CFSET frmnextpage="editorder">
			<CFSET form.frmtask="edit">
			<!--- <CF_DisplayBorder borderset=#application. standardBorderset#> --->
				<CFINCLUDE template="#frmNextPage#.cfm">
			<!--- </CF_DisplayBorder> --->
			<CF_ABORT>
		</CFIF>

		<!---

		JC === Redirection to secure server obsolete - now handled via ...userfiles/worldpay.cfm

		<cfif getorderheader.paymentmethod is "CC" and internalUser is true >

			<CFSET total = getorderHeader.ordervalue + getorderHeader.taxonorder + getorderHeader.shippingcharge>



			<form action="https:#application.SecureOrderPage#" name="secureForm" method="post" >
			<cfoutput>
			<input type="hidden" name="currentorderid" value="#currentorderid#">
			<input type="hidden" name="total" value="#Total#">
			<INPUT TYPE="HIDDEN" NAME="frmpersonid" VALUE="#getorderheader.personid#">
			<INPUT TYPE="HIDDEN" NAME="frmback" VALUE="#request.currentSite.httpProtocol##http_host#/orders/confirmorder.cfm?frmglobalparameters=#frmglobalparameters#">
			<INPUT TYPE="HIDDEN" NAME="frmpartnerarea" VALUE="">
			<INPUT TYPE="HIDDEN" NAME="frmDataSource" VALUE="#application.SiteDataSource#">
			<INPUT TYPE="HIDDEN" NAME="frmTitle" VALUE="Credit Card Payment">
			<INPUT TYPE="HIDDEN" NAME="frmAmountEditable" VALUE="0">
			<INPUT TYPE="HIDDEN" NAME="frmLanguageID" VALUE="#tmpLanguageID#">
			<INPUT TYPE="HIDDEN" NAME="frmCurrency" VALUE="#getorderHeader.orderCurrency#">
			</cfoutput>
			</form>

			<A HREF="javascript:document.secureForm.submit()">submit</A>
			<SCRIPT type="text/javascript">
			window.document.secureForm.submit()
			</SCRIPT>

			<CF_ABORT>

		</CFIF>
		 --->
	</cfif>

</cflock>

<CFIF frmNextPage is "orderitems">
	<CFSET frmtask="update">
</cfif>

<CFIF frmNextPage is not "">
	<CFINCLUDE template="#frmNextPage#.cfm">
</cfif>

