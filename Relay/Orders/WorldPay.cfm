<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Loading World Pay...</cf_title>
</cf_head>

<cf_body ONLOAD="document.WorldPayForm.submit()">
<CFPARAM NAME="elementid" DEFAULT="429">

	<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 
				*
			FROM 
				vOrderItems
			WHERE
				orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND active = 1
	</CFQUERY>
	
	<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
		select 
			Orderid,OrderCurrency,round(OrderValue + taxonorder, 2) as OrderValue,invcountryid,invcontact,invsitename,invaddress1,invaddress2,invaddress3,invaddress4,invaddress5,InvPostalCode,contactemail,p.promoid,contactfax,contactphone,orderdate,personid,locationid,orderstatus,paymentmethod
	
		from 
			orders as o,
			promotion as p
		where 
			orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and p.promoid = o.promoid
	</CFQUERY>
	<CFQUERY NAME="getCountry" datasource="#application.sitedatasource#">
		SELECT isocode from country where countryid =  <cf_queryparam value="#getOrderHeader.invcountryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<CFQUERY NAME="isPurchaseTester" datasource="#application.sitedatasource#">
		SELECT 1 FROM booleanflagdata b
		INNER JOIN flag f ON b.flagid = f.flagid
		WHERE entityid=#request.relayCurrentUser.personid#
		AND f.flagtextid='PurchaseTester'
	</CFQUERY>
	<CFSET nDesc="">
	<CFOUTPUT QUERY="getOrderItems">
		<CFSET nDesc=nDesc & "#Description#<BR>&nbsp;&nbsp;&nbsp;&nbsp;- QTY = #quantity#, PRICE = #totaldiscountprice##priceisocode#<BR>">
	</CFOUTPUT>
	<CFOUTPUT>
		<FORM NAME="WorldPayForm" ACTION="https://select.worldpay.com/wcc/purchase" METHOD="post">

		<CFLOOP LIST="#FIELDNAMES#" INDEX="x">
			<CF_INPUT NAME="M_#x#" VALUE="#evaluate(x)#" TYPE="Hidden">
		</CFLOOP>
		<CFSET nAddress = "">
		<CFLOOP LIST="invsitename,invaddress1,invaddress2,invaddress3,invaddress4,invaddress5" INDEX="x">
			<CFIF evaluate("getOrderHeader.#x#") NEQ "">
				<CFSET nAddress= nAddress & chr(10) & chr(13) & evaluate("getOrderHeader.#x#")>
			</CFIF>
		</CFLOOP>
		<CF_INPUT TYPE="Hidden" NAME="M_DSN" VALUE="#APPLICATION.SITEDATASOURCE#">
		<CF_INPUT TYPE="Hidden" NAME="M_elementid" VALUE="#elementid#">
		<!--- <INPUT TYPE="Hidden" NAME="M_border" VALUE="#application. standardBorderset#"> --->
		<CF_INPUT TYPE="Hidden" NAME="address" VALUE="#nAddress#">
		<CF_INPUT TYPE="Hidden" NAME="tel" VALUE="#getOrderHeader.contactphone#">
		<CF_INPUT TYPE="Hidden" NAME="fax" VALUE="#getOrderHeader.contactfax#">
		<CF_INPUT TYPE="Hidden" NAME="postcode" VALUE="#getOrderHeader.InvPostalCode#">
		<CF_INPUT TYPE="Hidden" NAME="country" VALUE="#getCountry.isocode#">
		<CF_INPUT TYPE="Hidden" NAME="email" VALUE="#getOrderHeader.contactemail#">
		<!--- <INPUT TYPE="Hidden" NAME="instId" VALUE="55980"> --->
		<CF_INPUT TYPE="Hidden" NAME="instId" VALUE="#WorldPayAccountID#">
		<CF_INPUT TYPE="Hidden" NAME="M_user" VALUE="#request.relayCurrentUser.personid#">
		<CF_INPUT TYPE="Hidden" NAME="currency" VALUE="#trim(getOrderHeader.OrderCurrency)#">
		<CF_INPUT TYPE="Hidden" NAME="desc" VALUE="#nDesc#">
		<CF_INPUT TYPE="Hidden" NAME="cartId" VALUE="#getOrderHeader.OrderID#">
		<CF_INPUT TYPE="Hidden" NAME="amount" VALUE="#getOrderHeader.OrderValue#">
		<CFIF isPurchaseTester.recordcount GT 0>

			
			<INPUT TYPE="Hidden" NAME="testMode" VALUE="100">
		<CFELSE>
			<INPUT TYPE="Hidden" NAME="testMode" VALUE="0">
		</CFIF>
		</FORM>
	</CFOUTPUT>





