<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
   1999-02-21  SWJ Modified javascript to call report/wabcommcheck.cfm
--->

<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="edit">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">

	<CFIF findNoCase("reportOrdersPastWeek.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
	</CFIF>
	<CFIF findNoCase("reportOrdersPastWeekwithAddress.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
	</CFIF>
	<CFIF findNoCase("reportOrderswithAddress.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
	</CFIF>
</CF_RelayNavMenu>
