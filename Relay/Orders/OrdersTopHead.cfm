<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
   1999-02-21  SWJ Modified javascript to call report/wabcommcheck.cfm
	2016-06-27	WAB		PROD2016-1334 callRelayRecordManager security.  Table must be encrypted.  (Not even sure that this file used)
--->


<CFPARAM name="frmruninpopup" default=""><!--- wab --->


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top"  CLASS="submenu">
			<cfif isDefined("subsection") AND subsection neq ''> - <cfoutput>#htmleditformat(subsection)#</cfoutput></cfif>
		</TD>

	<cfif isDefined("subsection") AND subsection neq ''>
		<cfoutput>
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<A HREF="../products/catalogues.cfm" Class="Submenu">Catalogues</A>&nbsp;|&nbsp;
			<A HREF="##addProduct" Class="Submenu">Add Product</A>&nbsp;|&nbsp;
			<A HREF="#application.com.security.encryptURL(url = '/templates/callRelayRecordManager.cfm?table=productGroup', singleSession = true)#" Class="Submenu">Product Groups</A>&nbsp;&nbsp;
		</TD>
		</cfoutput>
	</cfif>
	</TR>

</table>
