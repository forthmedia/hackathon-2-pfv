<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Create triggers for creating an order history record when the order status changes
Work OK, except that the CreatedBy could be misleading
also updates the inventory record
--->

<CFQUERY NAME="CreateTrigger" datasource="#application.sitedatasource#"> 
Alter TRIGGER "Orders_UTrig" ON dbo.Orders FOR UPDATE AS

SeT noCount On


--Insert Order History Record
INSERT INTO OrderHistory (orderid,Orderstatus,CreatedBy,Created,username)
SELECT I.OrderID, 
	I.OrderStatus,
	I.lastupdatedby
	,getdate()
	,user_Name()
FROM Inserted as I Join Deleted as D ON I.orderid = D.orderID
WHERE I.OrderStatus <> D.OrderStatus


-- Check for Inventory Update required
Declare @inventory int, @orderid int

	 Select @orderId = OrderID from Inserted
	 select @inventory=inventoryid from promotion where promoid = (select promoid from orders where orderid = @orderid) 


  IF (@inventory <> 0)
	BEGIN
	 declare	 @beforeStatus int, @afterStatus int

		 Select @beforeStatus = OrderStatus from Deleted
		 Select @afterStatus = OrderStatus from Inserted


-- have to admit that I don't cover every single possible case here		 
		IF ((@beforeStatus = 1  or @beforeStatus = 5) and @afterStatus = 2 )


			BEGIN
				Update Inventory set NumberAllocated =  ISNULL(NumberAllocated,0) + Quantity
				From Inventory, OrderItem
				WHERE orderID = @orderID
				AND Inventory.SKU = OrderItem.SKU
				AND InventoryId = @Inventory
			
			END

		IF    ( (@beforeStatus = 2 or @beforeStatus = 3 or @beforeStatus = 6 or @beforeStatus = 7 or @beforeStatus = 8) and (@afterStatus = 5 or @afterStatus = 9)) 
			
			BEGIN

				Update Inventory set NumberAllocated =  ISNULL(NumberAllocated,0) - Quantity
				From Inventory, OrderItem
				WHERE orderID = @orderID
				AND Inventory.SKU = OrderItem.SKU
				AND InventoryId = @Inventory



			END





	END

</cfquery>

<CFQUERY NAME="CreateTrigger" datasource="#application.sitedatasource#"> 
ALTER TRIGGER Orders_ITrig ON Orders FOR INSERT AS

--Insert Order History Record
INSERT INTO OrderHistory (orderid,Orderstatus,CreatedBy,Created,username)
SELECT I.OrderID, 
	I.OrderStatus,
	I.lastupdatedby
	,getdate()
	,user_Name()
FROM Inserted as I 
</cfquery>




<CFQUERY NAME="CreateTrigger" datasource="#application.sitedatasource#"> 
Alter Trigger "Inventory_UTrig"

On dbo.Inventory
For Update
As

     Declare @beforeinventory int, @afterinventory int

	 Select @beforeInventory = NumberInStock from Deleted
	 Select @afterInventory = NumberInStock from Inserted

IF (@beforeInventory <> @afterInventory )

	BEGIN

		Declare @inventory int, @SKU varchar(30)

		SELECT @inventory = inventoryID from Inserted
		SELECT @SKU = SKU from Inserted

		/* calculate the total number of this item which should be allocated to 3Contact Orders */
		Update inventory
		set NumberAllocated = 
			(SELECT sum(oi.quantity) from 
			PROMOTION as p, ORDERS as o, ORDERITEM as oi
			WHERE  p.promoid = o.promoid
			AND	o.orderid = oi.orderid
			AND 	p.inventoryid = inventory.inventoryid    -- @inventory
			AND 	oi.sku = inventory.sku   -- @SKU
			AND      oi.active <>0
			AND	o.orderstatus in (2,6,8) )     /* confirmed,  sent to remote, rejected by remote */
		Where inventoryid = @inventory
		and sku = @SKU


	END
</cfquery>