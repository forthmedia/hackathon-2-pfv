<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	DisplayOrderRelated File		
Author:	CPS		
Date created: 2001-02-01	

	Objective - Displays details of an order that is currently 'Awaiting Quote From 
				In Focus Agency' - statusid 21. Largely cribbed from DislayOrder.cfm.
	
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

CPS 28032001 use new application variable to display status description
CPS 10/Apr/2001 validate that the amount entered is a number 
CPS 29/May/2001 convert the amount of the order into the local currency of the supplier 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
Enhancement still to do:
--->

<CF_TRANSLATE>

<CFPARAM name="frmdisplaymode" default="view">   <!--- can be display,edit --->
<CFPARAM name="msg" default="">
<!--- <CFPARAM name="frmGlobalParameters" default=""> NJH 2009/06/30 P-FNL069 --->

<cfparam name="ShowDistiDiscount" default="no">

<!--- do some  rudimentary browser sniffing - don't worry about full versions - v4 checking is done later on --->
<cfparam name="cgi.http_user_agent" default="">
<cfif cgi.http_user_agent contains "MSIE">
	<!--- internet explorer --->
	<cfif cgi.http_user_agent contains "MSIE 4.0">
		<cfset browser= "IE4">
	<cfelseif cgi.http_user_agent contains "MSIE 5">
		<cfset browser = "IE5">	
	</cfif>
<cfelse>
	<!--- navigator --->
	<cfset browser = "Netscape">	
</cfif>

<!--- need to build in a check - a confirmed order should not be editable --->

<!--- CPS 29/May/2001 Get the country currency for the person viewing the order --->
<CFQUERY NAME="getCurrency" datasource="#application.sitedatasource#">
	SELECT cou.countryCurrency
	  FROM country cou
	      ,location loc
	      ,person per
	 WHERE cou.countryId = loc.countryId	  
	   AND per.locationId = loc.locationId
	   AND per.personId = #request.relayCurrentUser.personid#
</CFQUERY>

<!--- get details of order --->
<CFINCLUDE Template="qryorderdetails.cfm">
<!--- returns queries getorderheader and getorderitems --->


<!--- set up the show variables --->

<cfset ShowProductGroup = GetOrderHeader.ShowProductGroup>	
<cfset ShowProductGroupHeader = GetOrderHeader.ShowProductGroupHeader>
<cfset ShowTermsConditions = GetOrderHeader.ShowTermsConditions>
<cfset ShowPreviousQuotesLink = GetOrderHeader.ShowPreviousQuotesLink>
<cfset ShowPreviousOrdersLink = GetOrderHeader.ShowPreviousOrdersLink>
<cfset ShowDeliveryCalculation = GetOrderHeader.ShowDeliveryCalculation>
<cfset ShowVATCalculation = GetOrderHeader.ShowVATCalculation>

<cfset ShowProductCode = GetOrderHeader.ShowProductCode>
<cfset ShowProductCodeAsAnchor = GetOrderHeader.ShowProductCodeAsAnchor>
<cfset ShowPictures = GetOrderHeader.ShowPictures>
<cfset ShowDescription = GetOrderHeader.ShowDescription>
<cfset ShowPointsValue = GetOrderHeader.ShowPointsValue>
<cfset ShowLineTotal = GetOrderHeader.ShowLineTotal>
<cfset ShowQuantity = GetOrderHeader.ShowQuantity>
<cfset ShowListPrice = GetOrderHeader.ShowListPrice>
<cfset ShowDiscountPrice = GetOrderHeader.ShowDiscountPrice>
<cfset ShowNoInStock = GetOrderHeader.ShowNoInStock>
<cfset ShowMaxQuantity = GetOrderHeader.ShowMaxQuantity>
<cfset ShowPreviouslyOrdered = GetOrderHeader.ShowPreviouslyOrdered>

<cfset ShowPageTotal = GetOrderHeader.ShowPageTotal>
<cfset ShowDecimals = GetOrderHeader.ShowDecimals>
<cfset ShowProductGroupScreen = GetOrderHeader.ShowProductGroupScreen>

<cfset ShowPurchaseOrderRef = getOrderHeader.ShowPurchaseOrderRef>
 
<cfset UsePoints = GetOrderHeader.UsePoints>

<cfset promoid = getOrderHeader.PromoID>

<!--- get address format --->
<CFSET Countryid = #getOrderHeader.countryid#>
<CFSET screenid = 'standardaddressformat'>
<CFSET screenqueryname = "addressFormat">
<CFSET frmmethod = frmdisplaymode>
<CFINCLUDE template="..\screen\qryscreendefinition.cfm">


<!--- certain functions not shown to external users, so set up a variable  --->
<CFIF not request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    UserType is "External" --->
	<CFSET internalUser=false>
<CFELSE>
	<CFSET internalUser=true>
</cfif>

<SCRIPT type="text/javascript">

<!--

// CPS 10/Apr/2001 validate that the amount entered is a number 
<CFINCLUDE TEMPLATE="..\javascript\verifyInteger.js">

 function secure(){
		var form = document.mainForm;
		if (form.PaymentMethod[form.PaymentMethod.selectedIndex].value == "CC"){
			securesubmit();
		}else{
			window.alert("You have chosen not to make a credit card payment at this time");
		}
}

function securesubmit()	{
	var form = document.secureForm;
		// alert(form.action);
		form.submit();
}

function submitForm() {
	var form = document.mainForm;
		msg = ''
// CPS 10/Apr/2001 validate that the amount entered is a number 
	msg = verifyInteger(form.frmAmount.value, 'number', 2);
	if (msg != ''){
		window.alert(msg);
	}
	else
	{
		form.submit();
	}	
}

//-->

</SCRIPT>

<FORM ACTION="OrderStatusRelatedFileTask.cfm" NAME="mainForm" METHOD="POST" enctype="multipart/form-data">
	<CFOUTPUT>
		<CFIF frmDisplayMode is "view"><CFSET tmpTask = ""><CFELSE><CFSET tmpTask = "updateheader"></cfif>
		<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#tmpTask#">
		<CF_INPUT TYPE="HIDDEN" NAME="currentorderid" VALUE="#currentorderid#">
		<CF_INPUT TYPE="HIDDEN" NAME="orderlastupdated" VALUE="#getorderheader.lastupdated#">
		<INPUT TYPE="HIDDEN" NAME="frmNextPage" VALUE="../orders/vieworder.cfm">
		<!--- <INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="#frmGlobalParameters#"> NJH 2009/06/30 P-FNL069 --->
		<cfif isDefined("form.ThisProductGroup")>
		<CF_INPUT type="hidden" name="ThisProductGroup" value="#form.ThisProductGroup#">
		</cfif>
		<CF_INPUT TYPE="HIDDEN" NAME="frmOrderStatus" VALUE="#frmOrderStatus#">	
		<CF_INPUT TYPE="HIDDEN" NAME="frmPromoId" VALUE="#frmPromoId#">				
		<CF_INPUT TYPE="HIDDEN" NAME="frmOrderId" VALUE="#frmOrderId#">			
	</CFOUTPUT>


<!---Set variable for indicating 0 price Promotions--->
	<CFSET ZeroPrice = getOrderHeader.ZeroPrice>

<TABLE>
	<CFIF msg IS NOT "">
	<TR>
		<TD colspan="3">
		<CFOUTPUT>
		#htmleditformat(msg)#
		</cfoutput>
		</TD>
	</TR>
	</cfif>
</table>	

<TABLE>
<CFOUTPUT> 
	<cfif frmdisplaymode IS "edit">
		<tr>
		<td colspan="3" align="left">
			<div align="justify">phr_Order_PleaseTakeAMomentToCheck</div>
		</td>
		</tr>
	</cfif>
	<tr>
	<td colspan="3">
		<table border="0">
			<tr>
				<td><b>phr_Order</b> #htmleditformat(getorderheader.OrderNoPrefix)##htmleditformat(getorderheader.orderid)# </td>
				<td><b>phr_Order_PlacedBy</b> 
				<CFIF getOrderHeader.Createdby IS application.WebUserID>
					#htmleditformat(getorderheader.firstname)# #htmleditformat(getorderheader.lastname)#, #htmleditformat(getorderheader.sitename)#
				<CFELSE>
					#htmleditformat(getOrderHeader.CreatedByName)#
				</cfif>
				</td>
				<td><b>phr_DateCreated</b> #dateformat(getorderheader.created)#</td>
				<!--- CPS 28032001 use new application variable to display status description	--->
 				<td><b>phr_Order_OrderStatus</b> #application.StatusDescriptions[getorderheader.orderstatus]#</td>
<!---  				<td><b>phr_Order_OrderStatus</b> #listgetat(orderstatuslist,listfind(RealOrderStatuses,getorderheader.orderstatus))#</td>				 --->
			</tr>
		</table>
	</td>
	</tr>
</CFOUTPUT>

<tr><td>&nbsp;</td></tr>

<TR>
<TD>&nbsp;</td>

<!--- Delivery Details --->
<TD><CFOUTPUT> <b>phr_Order_DeliveryDetails</b></CFOUTPUT></td>
</tr>



	<TR>
		<TD>
		<!--- Contact Name ---><CFOUTPUT> <b>phr_ContactName</b></CFOUTPUT></TD>
		<TD>
		<CFOUTPUT>#HTMLEditFormat(getorderheader.delcontact)#</CFOUTPUT></TD>
		<TD>
	</TR>

	<TR>
		<TD>
		<CFOUTPUT><b>phr_Order_SiteName</b></CFOUTPUT></TD>
		<TD>
		<CFOUTPUT>#HTMLEditFormat(getorderheader.delsitename)#</CFOUTPUT></TD>
		<TD>
	</TR>
 
 	<CFLOOP query="addressFormat">

	<CFIF listfindNoCase(addressFieldsUsed,fieldTextId) IS NOT 0>
		<CFOUTPUT>

		<TR>
			<TD>
				<CFIF #trim(fieldLabel)# is not ""><b>phr_#htmleditformat(fieldLabel)#</b></cfif>
			</TD>

			<TD>
				<CFIF #fieldTextID# is "Countryid">
					<CFSET thisLineData = getorderheader.delCountryID>
					<CFSET thisLineFormField = "delCountryID">
 					<CFSET Variable.thislineMethod = frmdisplaymode>			
					<CFINCLUDE template="..\screen\showcountrylist.cfm">
				<CFELSE>
					#evaluate("getorderheader.del#htmleditformat(fieldTextID)#")#
				</cfif>
			</TD>

 		</TR>
	  	</CFOUTPUT>
	</cfif>
	</cfloop>
	

	<TR>
		<TD>
	<CFOUTPUT><b>phr_ContactPhone</b></CFOUTPUT></TD>
		<TD>
		<CFOUTPUT>#HTMLEditFormat(getorderheader.contactphone)#</CFOUTPUT></TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD>
		<CFOUTPUT><b>phr_ContactFax</b></CFOUTPUT></TD>
		<TD>
		<CFOUTPUT>#HTMLEditFormat(getorderheader.contactfax)#</CFOUTPUT></TD>
		<TD>
		</TD>
	</TR>
	<TR>
		<TD>
		<CFOUTPUT><b>phr_ContactEmail</b></CFOUTPUT></TD>
		<TD>
		<CFOUTPUT>#HTMLEditFormat(getorderheader.contactemail)#</CFOUTPUT></TD>
		<TD>
		</TD>
	</TR>

<tr>
	<td>&nbsp;</td>
</tr>

<tr valign="top">
	<td colspan="3">

		<br>

		<CFSET Total=0>
		<table>
		
		<TR valign="top">
		<TD><B>SKU   </TD>
		<TD><B><CFOUTPUT> phr_Description</CFOUTPUT></TD>

		<TD align="center"><B><CFOUTPUT>phr_Order_RewardPoints</CFOUTPUT></TD>
		<TD align="center"><B><CFOUTPUT>phr_Order_ListPrice</CFOUTPUT></TD>
		</TR>

 		<CFOUTPUT query="getorderitems">
		
			<CF_INPUT TYPE="hidden" NAME="frmItemId" VALUE="#itemid#">
			<!--- CPS 29/May/2001 - ISOPriceCode taken from the country in which the supplier resides --->
			<CF_INPUT TYPE="hidden" NAME="frmPriceISOCode" VALUE="#getCurrency.countryCurrency#">	
			
			<!--- if the order is being viewed by the distributor, need to display the discount levels as appropriate --->
			<TR valign="top">
			<TD>#htmleditformat(SKU)# </TD>
			<TD>#htmleditformat(Description)# </TD>
  			<TD align="right">#iif(ShowDecimals,DE("#decimalformat(UnitListPrice)#"), DE("#htmleditformat(Int(UnitListPrice))#"))#</TD>

			<CFSTOREDPROC PROCEDURE="RWDoConversion" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PointsAmountIn" VALUE="#UnitListPrice#" NULL="No">
 				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_DECIMAL" DBVARNAME="@CashAmountIn" VALUE="" NULL="Yes">	 
 				<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PriceISOCode" VALUE="#PriceISOCode#" NULL="No">	 				
				<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_DECIMAL" VARIABLE="CashAmount" DBVARNAME="@ConvertedAmount" NULL="No">	
			</CFSTOREDPROC>		
				
  			<TD align="right">#iif(ShowDecimals,DE("#decimalformat(CashAmount)#"), DE("#htmleditformat(Int(CashAmount))#"))#</TD>						
	


			<!--- Show the order item questions per order item. This will call the aScreen 
			custom tag which will get the data that is stored in --->
 			<cfif IsNumeric(Screenid) AND Screenid GT 0>
			
				<!--- CPS The statement was selecting the organisation for user that is logged onto the system
					  who might not be in the same organisation as the person who created the order e.g.
					  the 3Com Account Manager. Amend the query to select the organisation for the person
					  that placed the order --->
				<cfquery name="organisation" datasource="#application.SiteDataSource#">
					select o.* 
					from organisation o inner join 
						person p on o.organisationid = p.organisationid
<!--- 					where p.personid = #request.relayCurrentUser.personid# --->
						where p.personid =  <cf_queryparam value="#getorderheader.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 					
				</cfquery>
				
				<cfquery name="orderItem" datasource="#application.SiteDataSource#">
					select * , itemid as orderitemid
					from vOrderItems
					where ItemID =  <cf_queryparam value="#itemid#" CFSQLTYPE="CF_SQL_INTEGER" >  
				</cfquery>
				<TR valign="top">
					<TD>&nbsp </TD>
					<TD colspan="4">
						<CF_aSCREEN>
							<CF_aScreenItem 
								screenid="#screenid#" 
								orderitem=#orderitem# 
								organisation=#organisation# 
								method="edit"> 	
						</CF_aSCREEN>
					</TD>
				</TR>
			</cfif>
			<TR> 
				<TD>phr_Order_CampaignProposal</TD>
				<TD><INPUT TYPE="text" NAME="frmAmount" SIZE="5" align="right"  ></TD>
			</TR>
		</CFOUTPUT>

		</TABLE>
	</td>
	
</tr>

<CFIF GetOrderHeader.AskQuestions IS NOT 0>
<!--- this has been updated so that instead of having a list of flagids, it will now display a 
a screen which has been defined in ScreenDefinition table.  The name of this screen is stored in the AskQuestions Column--->
	<TR>
		<TD colspan="3">
			<TABLE width="400">
			<TR><TD width="200">&nbsp;</td><TD width="200">&nbsp;</td></tr>
			<CF_aSCreen>
				<CF_aScreenItem
					screenid="#GetOrderHeader.QuestionIDs#"
					ordersid = "#currentorderid#"
					method = "#frmdisplaymode#">
			</CF_aSCreen>
			</TABLE>
		</TD>
	</TR>
</CFIF>

</TABLE>
</FORM>

<cfif isDefined("orderConfirmed") and orderConfirmed and getOrderHeader.ShowClosingMessage>
<div>
<cfoutput>
phr_Order_ClosingMessage#htmleditformat(promoid)#<br>
<a href="../PartnerIndex.cfm">phr_Order_EToolsProgramme</a>
</cfoutput>
</div>
<p>
</p>
</cfif>

</CF_TRANSLATE>




