<!--- �Relayware. All Rights Reserved 2014 --->
<!--- qrydisalloweditems.cfm --->
<!--- query to return list of disallowed products for current location --->
<!--- requires variable currentlocationid --->
<!--- returns disallowedskus --->

<!--- amendment history --->
<!--- wab 1999-06-01 added a non comma delimiter decause some skus have commas in them  --->

<!--- get details of this promotion --->
<CFQUERY NAME="Promo" datasource="#application.sitedatasource#">
select 	MaxOrderNo
from 	promotion 
where 	promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	
</CFQUERY>



<!--- currently only used if the maxorder number is 1 --->
<CFIF Promo.MaxOrderNo is 1>



	<CFQUERY NAME="getPreviousOrderSKUsDisallowed" datasource="#application.sitedatasource#">
	SELECT 	i.SKU
	FROM 	orderitem as i, orders as o
	WHERE	i.orderid=o.orderid
	and		o.locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and		i.active=1
	and		o.orderstatus  in ( <cf_queryparam value="#realorderstatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and		o.orderdate >  <cf_queryparam value="#ReorderDisallowedCutOffDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</CFQUERY>

	<CFSET DisallowedSKUs=valuelist(getPreviousOrderSKUsDisallowed.sku,application.delim1)>

<CFELSE>

	<CFSET DisallowedSKUs="">	

</CFIF>
