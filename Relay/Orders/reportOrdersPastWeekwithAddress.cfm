<!--- �Relayware. All Rights Reserved 2014 --->
<!---  
2012-10-04	WAB		Case 430963 Remove Excel Header 
--->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>eCommerce Orders</cf_title>
	</cf_head>
	
	
	<cfinclude template="OrdersSubTopHead.cfm">

<cfparam name="sortOrder" default="description">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="order_date">

<cfparam name="keyColumnList" type="string" default="company"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="OrgID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getPastOrders" datasource="#application.SiteDataSource#">
select Name, Email, Company, Address_1, Address_2, Address_3, Address_4, Address_5, 
Postcode, Country, Phone, Fax, Order_Date, Description, OrgID, Order_ID 
from vOrderLastWeek
	WHERE 1=1 and Order_Date> getdate()-7			
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfset email=getpastorders.email>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Orders placed within the last week.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getPastOrders#"
	queryName="getPastOrders"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns="OrgID"
	ShowTheseColumns="Order_ID,Name,Description,Order_Date,Company,Address_1,Address_2,Address_3,Address_4,Address_5,Postcode,Country,Phone,Fax,Email"
	
	dateFormat="#dateFormat#"
	FilterSelectFieldList="Company,Country,Order_Date,Description"
	FilterSelectFieldList2="Company,Country,Order_Date,Description"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
>
</cf_translate>



