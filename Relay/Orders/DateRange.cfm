<!--- �Relayware. All Rights Reserved 2014 --->
<!---Include this document at the point where you wish the Combo Box to appear
and use the following code in your QueryCriteria:
WHERE Date(table field) >= #ListGetAt(DateRange, 1)#
AND   Date <= #ListGetAt(DateRange, 2)#--->

<CFSET LastSunday = DateAdd('D', -DatePart('W', Now()), Now())>
<CFSET LastSunday = DateAdd('D', 1, LastSunday)>
<CFSET SundayBefore = DateAdd('D', -7, LastSunday)>
<CFSET FirstOfThisMonth = CreateDate(Year(Now()), Month(Now()), 1)>
<CFSET LastOfLastMonth = DateAdd('D', -1, FirstOfThisMonth)>
<CFSET FirstOfLastMonth = DateAdd('M', -1, FirstOfThisMonth)>
<CFSET FirstDayOfThisYear = CreateDate(Year(Now()), 1, 1)>
<CFSET FirstDayOfLastYear = CreateDate(Year(DateAdd('YYYY', -1, Now())), 1, 1)>
<CFSET LastDayOfLastYear =  CreateDate(Year(DateAdd('YYYY', -1, Now())), 12, 31)>
<CFSET Today = DateAdd('D', 1, Now())>

<!---I am using tri "If" statements here, there must be a better way to do it,
I'll look into it as soon as I have time--->
<CFOUTPUT>

	<SELECT NAME="DateRange" onChange="datefields(this.value)">
		<OPTION VALUE="">Select a Date Range
		<OPTION VALUE="#DateFormat(LastSunday, "dd/mm/yyyy")#,#DateFormat(Today, "dd/mm/yyyy")#,1" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 1> SELECTED</CFIF></CFIF></CFIF>>This Week
		<OPTION VALUE="#DateFormat(SundayBefore, "dd/mm/yyyy")#,#DateFormat(LastSunday, "dd/mm/yyyy")#,2" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 2> SELECTED</CFIF></CFIF></CFIF>>Last Week
		<OPTION VALUE="#DateFormat(FirstOfThisMonth, "dd/mm/yyyy")#,#DateFormat(Today, "dd/mm/yyyy")#,3" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 3> SELECTED</CFIF></CFIF></CFIF>>Month to Date
		<OPTION VALUE="#DateFormat(FirstOfLastMonth, "dd/mm/yyyy")#,#DateFormat(LastOfLastMonth, "dd/mm/yyyy")#,4" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 4> SELECTED</CFIF></CFIF></CFIF>>Previous Month
		<OPTION VALUE="#DateFormat(FirstDayOfThisYear, "dd/mm/yyyy")#,#DateFormat(Today, "dd/mm/yyyy")#,5" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 5> SELECTED</CFIF></CFIF></CFIF>>Year to Date
		<OPTION VALUE="#DateFormat(FirstDayOfLastYear, "dd/mm/yyyy")#,#DateFormat(LastDayOfLastYear, "dd/mm/yyyy")#,6" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 6> SELECTED</CFIF></CFIF></CFIF>>Last Year
		<OPTION VALUE="#DateFormat(Now(), "dd/mm/yyyy")#,#DateFormat(Today, "dd/mm/yyyy")#,7" <CFIF IsDefined("DateRange")><CFIF DateRange IS NOT ''><CFIF ListGetAt(DateRange,3) IS 7> SELECTED</CFIF></CFIF></CFIF>>Today
	</SELECT>

</CFOUTPUT>



