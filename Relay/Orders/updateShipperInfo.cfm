<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			RejectOrder.cfm
Author:				SSS
Date started:		11/Jan/2008
	
Description:			

allows the etering of a reason for a order to be cancelled and the the points for the order to be refunded.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
11-Jan-2008			SSS			Initial version

Possible enhancements:

To be able to refund part of the points not all the points
 --->

<cfif isdefined("CancelOrder")>
	<cfloop index="i" list="#FRMOrderIDs#" delimiters=",">
		<CFQUERY NAME="UpdateOrders" dataSource="#application.siteDataSource#">
		     update orders
			 	set Shipper =  <cf_queryparam value="#frmshippername#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					ShipperReference =  <cf_queryparam value="#frmTrackno#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		     WHERE orderID =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   </CFQUERY>
	</cfloop>
	<script>
     window.close();  
   </script>
<cfelse>
<cf_translate>
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<cfoutput>
		<cfform method="post">
			<cf_relayFormDisplay>
				<CF_relayFormElementDisplay relayFormElementType="MESSAGE" currentValue="phr_Prizes_shippingmessage" spanCols="yes" valueAlign="left">
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmshippername" currentValue="" label="phr_Prizes_shippername" size="40" maxlength="50" tabindex="1">
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmTrackno" currentValue="" label="phr_Prizes_Trackno" size="40" maxlength="50" tabindex="2">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmOrderIds#" fieldname="FRMOrderIDs">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="CancelOrder" currentValue="submitbutton" label="Phr_Prizes_addtracking" spanCols="No" valueAlign="left" class="button">
			</cf_relayFormDisplay>
		</cfform>
	</cfoutput>
</cf_translate>
</cfif>
