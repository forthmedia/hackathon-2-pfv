<!--- �Relayware. All Rights Reserved 2014 --->



<!---Last phase, DELETE and INSERT if names were moved around so that 
the screen could refresh with new data--->
<CFIF IsDefined("Remove")>

	<CFQUERY NAME="RemoveRights" datasource="#application.siteDataSource#">
		DELETE From RecordRights
		WHERE Entity = 'Promotion'
		AND UserGroupID =  <cf_queryparam value="#CurrentUsers#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND RecordID =  <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>


<CFELSEIF IsDefined("Add")>

	<CFQUERY NAME="AddRights" datasource="#application.siteDataSource#">
		INSERT INTO RecordRights(UserGroupID, Entity, RecordID)
		VALUES(<cf_queryparam value="#PossibleUsers#" CFSQLTYPE="CF_SQL_INTEGER" >, 'Promotion', <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" >)
	</CFQUERY>


</CFIF>



<!---Get Promotions for the Combo Box--->
<CFQUERY NAME="Promotions" datasource="#application.siteDataSource#">
	Select CampaignID, PromoID, PromoName
	From promotion
	ORDER BY PromoID
</CFQUERY>

<!---Get Security Type ID for filtering only the people who have rights to Orders--->
<CFQUERY NAME="getSecurityTypeID" datasource="#application.siteDataSource#">
	SELECT SecurityTypeID
	FROM SecurityType
	WHERE ShortName = 'OrderTask'
</CFQUERY>



<SCRIPT>
	function formSubmit(){
	var form = window.document.ComboForm
	
	form.submit()
	}
</SCRIPT>




<FORM ACTION="RecordManager.cfm" METHOD="post" NAME="ComboForm">
	
	
	
	<SELECT NAME="Campaign" onChange="formSubmit()">
	<OPTION VALUE="0">Select a Promotion
	<CFOUTPUT QUERY="Promotions">
	<OPTION VALUE="#CampaignID#" <CFIF IsDefined("Campaign")><CFIF CampaignID is Campaign>SELECTED</CFIF></CFIF>>#htmleditformat(PromoName)#
	</CFOUTPUT>
	</SELECT>
	
	

</FORM>



<!---Second Phase, after a Criteria (eg. Promotion) was selected)--->
<CFIF IsDefined("Campaign")>

<!---Get all the Users enabled for the given Promotion--->
	<CFQUERY NAME="Users" datasource="#application.siteDataSource#">
		SELECT DISTINCT ug.Name, ug.UserGroupID, ug.personid
		FROM UserGroup AS ug, RecordRights AS rr
		WHERE ug.UserGroupID = rr.UserGroupID 
		AND rr.recordid =  <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND entity = 'Promotion'
	</CFQUERY>
	
	
	<CFSET EnabledPeople = ValueList(Users.personid)>
	
	
<!---Get all the Users with Rights on Orders but without rights on this Promotion--->
	<CFQUERY NAME="PossibleUsers" datasource="#application.siteDataSource#">
		SELECT Name, UserGroupID
		FROM UserGroup
		WHERE PersonID IN (SELECT PersonID
      					   FROM RightsGroup
      					   WHERE UserGroupID IN (SELECT r.UsergroupID
             									 FROM Rights AS r, UserGroup AS ug
												 WHERE r.UserGroupID = ug.UsergroupID
             									 AND r.SecurityTypeID =  <cf_queryparam value="#GetSecurityTypeID.SecurityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
												 )<CFIF EnabledPeople IS ''>)</CFIF>
			<CFIF EnabledPeople IS NOT ''>	
				AND personid  NOT IN ( <cf_queryparam value="#EnabledPeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			</CFIF>	
				AND personID IS NOT NULL
	</CFQUERY>
	
	
	
<FORM ACTION="RecordManager.cfm" METHOD="post">
	<TABLE BGCOLOR="#DFDFDF">
		<TR>
			<TD VALIGN="top">
			
			<CFIF Users.RecordCount IS NOT 0>
			
			<!---Display Users with rights for the given Campaign...--->
			<SELECT NAME="CurrentUsers" MULTIPLE <CFIF Users.RecordCount LT 10> SIZE="<CFOUTPUT>#Users.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
				<CFOUTPUT QUERY="Users">
				<OPTION VALUE="#usergroupID#">#htmleditformat(Name)#
				</CFOUTPUT>
			</SELECT>
			<CFELSE>
			No user is enabled for this campaign
			</CFIF>
			</TD>
	
	
			<TD VALIGN="top">
			<!---...and Possible Users (Users with rights to Orders, but not to this Campaign)--->
			<SELECT NAME="PossibleUsers" MULTIPLE <CFIF PossibleUsers.RecordCount LT 10> SIZE="<CFOUTPUT>#PossibleUsers.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
				<CFOUTPUT QUERY="PossibleUsers">
				<OPTION VALUE="#UserGroupID#">#htmleditformat(Name)#
				</CFOUTPUT>
			</SELECT>
			</TD>
		</TR>
		<TR>
			<TD>
			<INPUT TYPE="submit" NAME="Remove" VALUE="Remove Rights">
			</TD>
			<TD>
			<INPUT TYPE="submit" NAME="Add" VALUE="Add Rights">
			</TD>
		</TR>
	</TABLE>
	
	<CFOUTPUT>
	<CF_INPUT TYPE="hidden" NAME="Campaign" VALUE="#Campaign#">
	</CFOUTPUT>
	
	
	
	</FORM>
	
	
	
</CFIF>


