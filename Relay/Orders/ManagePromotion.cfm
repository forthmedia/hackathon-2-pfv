<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			ManagePromotion.cfm
Author:			DJH
Date created:	2nd June 2000

Description:	Allows the various options within a promotion to be managed. 
Also allows new promotions to be added.

Version history:
1

--->




<cf_head>
	<cf_title>Manage Promotions</cf_title>
</cf_head>

<!--- Get a list of the promotions in the system --->

<cfquery name="Promotion" datasource="#application.siteDataSource#">
select
	CampaignID,
	PromoID,
	PromoName,
	BaseCurrency,
	MaxOrderNo,
	FulfilledBy,
	ShippingMatrixID,
	InventoryID,
	AskQuestions,
	QuestionIDs,
	DistributorList,
	Mail,
	ZeroPrice,
	PromoOrderType,
	lastupdatedby,
	ShowProductGroup,
	ShowProductGroupScreen,
	ShowProductGroupHeader,
	ShowTermsConditions,
	ShowLineTotal,
	ShowPageTotal,
	ShowPictures,
	ShowProductCode,
	ShowProductCodeAsAnchor,
	ShowDescription,
	ShowPointsValue,
	ShowQuantity,
	ShowDecimals,
	ShowListPrice,
	ShowDiscountPrice,
	ShowNoInStock,
	ShowMaxQuantity,
	ShowPreviouslyOrdered,
	ShowDistributorList,
	ShowPaymentOptions,
	ShowOrderConfirmationScreen,
	ShowPreviousOrdersLink,
	ShowPreviousQuotesLink,
	ShowGrandTotal,
	ShowVATCalculation,
	ShowDeliveryCalculation,
	ShowDownloadLink,
	RequiresPurchaseRef,
	ConfirmedPhrase,
	OrderNoPrefix,
	Borderset	
from
	Promotion
order by
	PromoID
</cfquery>

<SCRIPT type="text/javascript">

// not too much data is expected, so using javascript to handle will make things nicer
var records = new Array()
// array constructor
function record(CampaignID, PromoID, PromoName, BaseCurrency, MaxOrderNo, FulfilledBy, ShippingMatrixID, InventoryID, AskQuestions, QuestionIDs, DistributorList, Mail, ZeroPrice, PromoOrderType, ShowProductGroup, ShowProductGroupScreen, ShowProductGroupHeader, ShowTermsConditions, ShowLineTotal, ShowPageTotal, ShowPictures, ShowProductCode, ShowProductCodeAsAnchor, ShowDescription, ShowPointsValue, ShowQuantity, ShowDecimals, ShowDiscountPrice, ShowListPrice, ShowNoInStock, ShowMaxQuantity, ShowPreviouslyOrdered, ShowDistributorList, ShowPaymentOptions, ShowOrderConfirmationScreen, ShowPreviousOrdersLink, ShowPreviousQuotesLink, ShowGrandTotal, ShowVATCalculation, ShowDeliveryCalculation, ShowDownloadLink, RequiresPurchaseRef, ConfirmedPhrase, OrderNoPrefix, Borderset)
{
	this.CampaignID
	this.PromoID = PromoID
	this.PromoName = PromoName
	this.BaseCurrency = BaseCurrency
	this.MaxOrderNo = MaxOrderNo
	this.FulfilledBy = FulfilledBy
	this.ShippingMatrixID = ShippingMatrixID
	this.InventoryID = InventoryID
	this.AskQuestions = AskQuestions
	this.QuestionIDs = QuestionIDs
	this.DistributorList = DistributorList
	this.Mail = Mail
	this.ZeroPrice = ZeroPrice
	this.PromoOrderType = PromoOrderType
	this.ShowProductGroup = ShowProductGroup
	this.ShowProductGroupScreen = ShowProductGroupScreen
	this.ShowProductGroupHeader = ShowProductGroupHeader
	this.ShowTermsConditions = ShowTermsConditions
	this.ShowLineTotal = ShowLineTotal
	this.ShowPageTotal = ShowPageTotal
	this.ShowPictures = ShowPictures
	this.ShowProductCode = ShowProductCode
	this.ShowProductCodeAsAnchor = ShowProductCodeAsAnchor
	this.ShowDescription = ShowDescription
	this.ShowPointsValue = ShowPointsValue
	this.ShowQuantity = ShowQuantity
	this.ShowDecimals = ShowDecimals
	this.ShowDiscountPrice = ShowDiscountPrice
	this.ShowListPrice = ShowListPrice
	this.ShowNoInStock = ShowNoInStock
	this.ShowMaxQuantity = ShowMaxQuantity
	this.ShowPreviouslyOrdered = ShowPreviouslyOrdered
	this.ShowDistributorList = ShowDistributorList
	this.ShowPaymentOptions = ShowPaymentOptions
	this.ShowOrderConfirmationScreen = ShowOrderConfirmationScreen
	this.ShowPreviousOrdersLink = ShowPreviousOrdersLink
	this.ShowPreviousQuotesLink = ShowPreviousQuotesLink
	this.ShowGrandTotal = ShowGrandTotal
	this.ShowVATCalculation = ShowVATCalculation
	this.ShowDeliveryCalculation = ShowDeliveryCalculation
	this.ShowDownloadLink = ShowDownloadLink
	this.RequiresPurchaseRef = RequiresPurchaseRef
	this.ConfirmedPhrase = ConfirmedPhrase
	this.OrderNoPrefix = OrderNoPrefix
	this.Borderset = Borderset
}
// load the array
<cfoutput query="Promotion">
records[records.length] = new record(#jsStringFormat(CampaignID)#, "#jsStringFormat(PromoID)#", "#jsStringFormat(PromoName)#", "#jsStringFormat(BaseCurrency)#", #jsStringFormat(MaxOrderNo)#, "#jsStringFormat(FulfilledBy)#", "#jsStringFormat(ShippingMatrixID)#", #jsStringFormat(InventoryID)#, #jsStringFormat(AskQuestions)#, "#jsStringFormat(QuestionIDs)#", "#jsStringFormat(DistributorList)#","#jsStringFormat(Mail)#", #jsStringFormat(ZeroPrice)#, "#jsStringFormat(PromoOrderType)#", #jsStringFormat(ShowProductGroup)#, #jsStringFormat(ShowProductGroupScreen)#, #jsStringFormat(ShowProductGroupHeader)#, #jsStringFormat(ShowTermsConditions)#, #jsStringFormat(ShowLineTotal)#, #jsStringFormat(ShowPageTotal)#, #jsStringFormat(ShowPictures)#, #jsStringFormat(ShowProductCode)#, #jsStringFormat(ShowProductCodeAsAnchor)#, #jsStringFormat(ShowDescription)#, #jsStringFormat(ShowPointsValue)#, #jsStringFormat(ShowQuantity)#, #jsStringFormat(ShowDecimals)#, #jsStringFormat(ShowDiscountPrice)#, #jsStringFormat(ShowListPrice)#, #jsStringFormat(ShowNoInStock)#, #jsStringFormat(ShowMaxQuantity)#, #jsStringFormat(ShowPreviouslyOrdered)#, #jsStringFormat(ShowDistributorList)#, #jsStringFormat(ShowPaymentOptions)#, #jsStringFormat(ShowOrderConfirmationScreen)#, #jsStringFormat(ShowPreviousOrdersLink)#, #jsStringFormat(ShowPreviousQuotesLink)#, #jsStringFormat(ShowGrandTotal)#, #jsStringFormat(ShowVATCalculation)#, #jsStringFormat(ShowDeliveryCalculation)#, #jsStringFormat(ShowDownloadLink)#, #jsStringFormat(RequiresPurchaseRef)#, "#jsStringFormat(ConfirmedPhrase)#", "#jsStringFormat(OrderNoPrefix)#", "#jsStringFormat(Borderset)#")</cfoutput>


// array record locator

function recordFind(aCampaignID)
{
	for (i=0; i<records.length; i++)
	{
		if (records[i].CampaignID == aCampaignID)
		break
	}
	return i
}

function getValueObj(obj)
{
	switch (obj.type)
	{
		case "text":
			vvalue="value"
		break
			
		case "hidden":
			vvalue="value"
		break
		
		case "textarea":
			vvalue="value"
		break
		
		case "checkbox":
			vvalue="checked"
		break
		
		case "radio":
			vvalue="checked"
		break
		
		case "button":
			vvalue="value"
		break
		
		case "select":
			vvalue="selectedIndex"
		break
		
		default:
			vvalue = "value"
		break					
	}
	return vvalue
}



// function to load the data
function loadRecord(aselect)
{
	var f = aselect.form
	if (aselect.selectedIndex > 0)
	{
		
		// two values in the select mean need to offset the index
		var thisrec = (aselect.selectedIndex - 1)
		// load a record 		
		
		// loop round each property in the current record
		for (propertyid in records[thisrec])
		{
			
			// see if the form object exists, and check what type of input it is
			
			thisField = eval("f." + propertyid)
			
			alert(thisField)
			
			if( thisField != null)
			{
				// it exists - see what type it is
				thisFieldValObj = eval(thisField + "." + getValueObj(thisField))
				// assign the value
				alert(records[thisrec][propertyid])
				thisFieldValObj = records[thisrec][propertyid]
			}	
		
		}
		
		
		/*
		
		f.CampaignID.value = records[thisrec].CampaignID
		f.PromoID.value = records[thisrec].PromoID
		f.PromoName.value = records[thisrec].PromoName
		f.BaseCurrency.value = records[thisrec].BaseCurrency
		f.MaxOrderNo.value = records[thisrec].MaxOrderNo
		f.FulfilledBy.value = records[thisrec].FulfilledBy
		f.ShippingMatrixID.value = records[thisrec].ShippingMatrixID
		f.InventoryID.value = records[thisrec].InventoryID
		f.AskQuestions.checked = records[thisrec].AskQuestions
		f.QuestionIDs.value = records[thisrec].QuestionIDs
		f.DistributorList.value = records[thisrec].DistributorList
		f.Mail.value = records[thisrec].Mail
		f.ZeroPrice.checked = records[thisrec].ZeroPrice
		f.PromoOrderType.value = records[thisrec].PromoOrderType
		f.ShowProductGroup.checked = records[thisrec].ShowProductGroup
		f.ShowProductGroupScreen.checked = records[thisrec].ShowProductGroupScreen
		f.ShowProductGroupHeader.checked = records[thisrec].ShowProductGroupHeader
		f.ShowTermsConditions.checked = records[thisrec].ShowTermsConditions
		f.ShowLineTotal.checked = records[thisrec].ShowLineTotal
		f.ShowPageTotal.checked = records[thisrec].ShowPageTotal
		f.ShowPictures.checked = records[thisrec].ShowPictures
		f.ShowProductCode.checked = records[thisrec].ShowProductCode
		f.ShowProductCodeAsAnchor.checked = records[thisrec].ShowProductCodeAsAnchor
		f.ShowDescription.checked = records[thisrec].ShowDescription
		f.ShowPointsValue.checked = records[thisrec].ShowPointsValue
		f.ShowQuantity.checked = records[thisrec].ShowQuantity
		f.ShowDecimals.checked = records[thisrec].ShowDecimals
		f.ShowDiscountPrice.checked = records[thisrec].ShowDiscountPrice
		f.ShowListPrice.checked = records[thisrec].ShowListPrice
		f.ShowNoInStock.checked = records[thisrec].ShowNoInStock
		f.ShowMaxQuantity.checked = records[thisrec].ShowMaxQuantity
		f.ShowPreviouslyOrdered.checked = records[thisrec].ShowPreviouslyOrdered
		
		*/
		
		f.SubmitButton.value = "Update record"
	}
	else
	{
		// blank em out
		blankEm(f)
	}
}

function blankEm(aform)
{
	aform.reset()
	aform.SubmitButton.value = "Add this record"
}

function show_props(obj, objName) 
{
	var result = ""
	for (var i in obj) 
	{
		result += objName + "." +i +"="+obj[i] + "\n"
	}
	return result
}

function enumShow(obj)
{
	alert(show_props(eval(obj),obj))
}

</script>






<div align="center">
<cfparam name="frmTask" default="New">
<form name="mainForm" action="ManagePromotion.cfm" method="post">
<input type="hidden" name="frmTask">
<input type="hidden" name="CampaignID">
<input type="text" name="checker" onchange="enumShow(this.value)">
<table border="0" cellpadding="4" cellspacing="4">
	<tr>
		<td class="label"  colspan="4">
			<b>Select a Promotion</b><br>
			<cfparam name="SelectPromotion" default="">
			<select name="SelectPromotion" onchange="loadRecord(this)">
				<option value="new">Create a new promotion
				<cfoutput query="Promotion">
					<option value="#CampaignID#">#htmleditformat(PromoName)#
				</cfoutput>
			</select>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" colspan="2">
			<b>Promo ID</b>
			<div class="margin">
			This is the unique text value for the promotion.
			</div>
			<div align="right">
				<input type="text" name="PromoID" size="20" maxlength="50">
			</div>
		</td>
		<td class="label" colspan="2">
			<b>Promo Name</b>
			<div class="margin">
			This is the same as the promo ID, with spaces to aid readability.
			</div>
			<div align="right">
				<input type="text" name="PromoName" size="20" maxlength="50">
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" colspan="2">
			<b>Base Currency</b>
			<div class="margin">
			This
			</div>
			<div align="right">
				<input type="text" name="BaseCurrency" size="3" maxlength="3">
			</div>
		</td>
		<td class="label" colspan="2">
			<b>Max Order No</b>
			<div class="margin">
			This is the maximum no of items allowed to be ordered in a promotion. Usually, when set to 1, indicates that 
			only one line item per product is allowed to be ordered.
			</div>
			<div align="right">
				<input type="text" name="MaxOrderNo" size="2" maxlength="5">
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" colspan="2">
			<b>Fulfilled By</b>
			<div class="margin">
				This indicates which external agency will fulfill the order - used where distributors do not get chosen by the 
				customer.
			</div>
			<div align="right">
				<input type="text" name="FulfilledBy" size="5" maxlength="10">
			</div>
		</td>
		<td class="label" colspan="2">
			<b>Shipping Matrix ID</b>
			<div class="margin">
				This is similar to the fulfilled by field.
			</div>
			<div align="right">
				<input type="text" name="ShippingMatrixID" size="5" maxlength="5">
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" colspan="2">
			<b>Inventory ID</b>
			<div class="margin">
				This 
			</div>
			<div align="right">
				<input type="text" name="InventoryID" size="4" maxlength="6">
			</div>
		</td>
		<td class="label" colspan="2">
			<b>Question IDs</b>
			<div class="margin">
				This is the list of question ids to be asked.
			</div>
			<div align="right">
				<input type="text" name="QuestionIDs" size="30" maxlength="150">
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" colspan="2">
			<b>Mail</b>
			<div class="margin">
				This 
			</div>
			<div align="right">
				<input type="text" name="Mail" size="30" maxlength="150">
			</div>
		</td>
		<td class="label" colspan="2">
			<b>Promo Order Type</b>
			<div class="margin">
				This indicates P for P, I for I.
			</div>
			<div align="right">
				<input type="text" name="PromoOrderType" size="1" maxlength="1">
			</div>
		</td>
	</tr>

	<tr><td colspan="4">
	<table align="center">
	<tr>
		<td class="label" width="33" align="center" align="center">
			<input type="checkbox" name="ZeroPrice">
		</td>
		<td class="label">
			<b>Zero Price</b><br>
			This indicates whether the promotion will be zero priced - check if yes.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="AskQuestions">
		</td>
		<td class="label">
			<b>Ask Questions</b><br>
			This indicates whether questions are to be asked. Check for yes.
		</td>
	</tr>		
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowProductGroup">
		</td>	
		<td class="label">
			<b>Show Product Group?</b><br>
			This indicates whether to show one product group at a time or all on screen at once.				
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowProductGroupScreen">
		</td>
		<td class="label">
			<b>Show Product Group Screen</b><br>
			This indicates whether the product group screen will be displayed when a user first chooses
			to view this promotion.				
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowProductGroupHeader">
		</td>
		<td class="label">
			<b>Show Product Group Header</b><br>
			This indicates whether to display the product group header for this promotion.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowTermsConditions">
		</td>
		<td class="label">
			<b>Show Terms &amp; Conditions</b> <br>
			This indicates whether to display the product group header for this promotion.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowLineTotal">
		</td>
		<td class="label">
			<b>Show Line Total</b> <br>
			This indicates whether a line total will be displayed.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowPageTotal">
		</td>
		<td class="label">
			<b>Show Page Total</b> <br>
			This indicates whether a page total should be shown.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowPictures">
		</td>
		<td class="label">
			<b>Show Pictures</b><br>
			This indicates whether to show pictures.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowProductCode">
		</td>
		<td class="label">
			<b>Show Product Code</b> <br>
			This indicates whether to show the product code.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowProductCodeAsAnchor">
		</td>
		<td class="label">
			<b>Show Product Code as Anchor</b> <br>
			This indicates whether to show the product code with click thru to edit functionality.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowDescription">
		</td>
		<td class="label">
			<b>Show Description</b> <br>
			This indicates whether to show the descriptive text.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowPointsValue">
		</td>
		<td class="label">
			<b>Show Points Value</b> <br>
			This indicates whether to show the points value.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowQuantity">
		</td>
		<td class="label">
			<b>Show Quantity</b> <br>
			This indicates whether to show the quantity field.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowDecimals">
		</td>
		<td class="label">
			<b>Show Decimals</b> <br>
			This indicates whether to show decimals
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowDiscountPrice">
		</td>
		<td class="label">
			<b>Show Discount Price</b> <br>
			This indicates whether to show the discount price.				
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowListPrice">
		</td>
		<td class="label">
			<b>Show List Price</b> <br>
			This indicates whether to show the list price.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowNoInStock">
		</td>
		<td class="label">
			<b>Show No In Stock</b> <br>
			This indicates whether to show the number in stock.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowMaxQuantity">
		</td>
		<td class="label">
			<b>Show Max Quantity</b> <br>
			This indicates whether to show the max quantity.
		</td>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowPreviouslyOrdered">
		</td>
		<td class="label">
			<b>Show Previously Ordered</b><br>
			This indicates whether to show the previously ordered.
		</td>
	</tr>
	<tr>
		<td class="label" width="33" align="center">
			<input type="checkbox" name="ShowBorders">
		</td>
		<td class="label">
			<b>Show Borders</b> <br>
			This indicates whether to show borders.
		</td>
	</tr>
	</table>
	</td></tr>
	<tr>
	<td colspan="4" align="center" class="label">
		<input type="submit" name="SubmitButton" value="Add this record">
	</td>
	</tr>
</table>





</form>

</div>



