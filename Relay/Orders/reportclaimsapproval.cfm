<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportClaimsApproval.cfm
Author:				SSS
Date started:		2008-07-18
Description:			

Core report code for relayware orders

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
18-July-2008			SSS		The query should only bring back orders that the approver has access to
09-sep-2008				SSS		I have added a fix to make sure that only vpp orders are shown in the report the fix is to get 
								all orders that do not have a transaction
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:
The orders need some type of ordertype field that can be used to tell the difference between different types of orders.

 --->

<cfinclude template="/code/cftemplates/ordersINI.cfm">
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>eCommerce Orders</cf_title>
	</cf_head>
	
	<cfinclude template="OrdersSubTopHead.cfm">

<cfparam name="sortOrder" default="orderid desc">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="order_date">

<cfparam name="keyColumnList" type="string" default="orderid"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../orders/viewclaim.cfm?currentorderid="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="orderid"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<!--- <cfquery name="getPastOrders" datasource="#application.SiteDataSource#">
select Name, Email, Company, Address_1, Address_2, Address_3, Address_4, Address_5, 
Postcode, Country, Phone, Fax, Order_Date, Description, OrgID, Order_ID 
from vOrderLastWeek
	WHERE 1=1 and Order_Date> getdate()-7		
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery> --->
<CFPARAM name="requiredstatus" default="#RealOrderStatuses#">
<CFQUERY NAME="getPreviousOrdersDetail" DATASOURCE=#application.sitedatasource# DEBUG>
SELECT 	o.orderid,
		o.orderdate,
		o.created,
		o.orderstatus,
		firstname,
		lastname, 
		s.description as orderstatusdescription
		
from orders as o, person as p, status as s, organisation as og
where orderstatus  in ( <cf_queryparam value="#requiredstatus#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
and 	o.personid=p.personid
and 	p.organisationID = og.organisationID 
<!---	2012-07-20 PPB P-SMA001		 and     og.countryID in (#request.relayCurrentUser.countrylist#) ---> <!--- SSS 2008-07-18 access only to people who can see that organisation's country --->
#application.com.rights.getRightsFilterWhereClause(entityType="Orders",alias="og").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->
<!--- and		o.locationid in (#request.relayCurrentUser.locationID#) <!--- NJH 2006-08-18 changed from #frmLocationID# to persons location... not entirely sure if this is what we want--->
 ---><!--- AJC 2006-09-25 P_Lex003 --->
and		o.orderstatus=s.statusid
<cfif isDefined("orderID") and orderID neq "">
and		o.orderID =  <cf_queryparam value="#orderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
<cfelse>
<!---SSS 2008-09-09 This is to distinguish between vpp orders and incentive orders. Vpp order are the only one that should not have transactions if in the future we have other orders that are bolted on that do not have a transaction it will be a lot of work to tell the orders apart.  --->
and 	o.orderID not in (select distinct orderID from RWTransaction where orderID is not null)
</cfif>
<cfif bypersonid is true>
	and o.personid=#request.relaycurrentuser.personid#
</cfif>
</CFQUERY>

<cfquery name="qoqPreviousOrdersDetail" dbtype="query">
SELECT 	orderid,orderdate,created,orderstatus,firstname,lastname,orderstatusdescription
from getPreviousOrdersDetail
where 1=1
<cfset inQoQ = true>
<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
order by <cf_queryObjectName value="#sortorder#">
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Orders placed within the last week.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#qoqPreviousOrdersDetail#"
	queryName="getPreviousOrdersDetail"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns=""
	ShowTheseColumns="orderid,created,orderstatusdescription"
	columnheadinglist="orderid,order_date,order_status"
	dateFormat="created"
	FilterSelectFieldList="created"
	FilterSelectFieldList2="created"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
>
</cf_translate>



