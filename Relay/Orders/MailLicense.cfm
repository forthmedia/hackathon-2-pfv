<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:  WAB 030200  Uses variable Delivery Address to get properly formatted address
	WAB 2001-03-26		altered where the emails come from (variable wasn't defined)
 --->	

<!---Generic Mail Template--->
<CFSET ordertotal = getOrderValue.TotalValue + getOrderHeader.TaxOnOrder + getOrderHeader.ShippingCharge >

<CFSET tab1 = 15>
<CFSET tab2 = 30>
<CFSET LF=Chr(13) & Chr(10)>
<CF_MAIL query="getorderitems"    
		TO="#orderToEmail#" 
		FROM="#application.ordersEmailFrom#"
		CC="#copyAllEmailsToThisAddress#"
		SUBJECT="#application.EmailFromDisplayName# - License Issue"
		Type="HTML"
		>

<STYLE>
  .p{
  font-family : Verdana,Geneva,Arial,Helvetica,sans-serif;
  font-size : 12px;
  font-weight : normal;
  color : 000000;
 }
 .td{
  font-family : Verdana,Geneva,Arial,Helvetica,sans-serif;
  font-size : 12px;
  font-weight : normal;
  color : 000000;
 }
</style>
		

<p class="p">#getorderheader.firstname# #getorderheader.lastname#,</p>

<p class="p">#phr_order_LicenseIssue#</p>

<table>
	<tr>
		<td class="td">Order :</td> <td>#currentorderid#</td>
	</tr>
	<tr>
		<td class="td">License :</td> <td>#License#</td>
	</tr>
	<tr>
		<td class="td">#phr_order_ItemsOrdered#<br></td><td></td>
	</tr>

<CFOUTPUT>
	<tr>
		<td class="td">#phr_order_ProductCode# :</td> 
		<td class="td">#repeatString(" ",max(0,tab2-Len(phr_order_ProductCode)))# #SKU#</td>
	</tr>
	<tr>
		<td class="td">#phr_Description# :</td> 
		<td class="td">#repeatString(" ",max(0,tab2-Len(phr_description)))# #Description#</td>
	</tr>
</CFOUTPUT>
</table>

</CF_MAIL>



