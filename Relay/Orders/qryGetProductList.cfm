<!--- �Relayware. All Rights Reserved 2014 --->
<!--- qryGetProductList.cfm

Author WAB

Date 2001-04-26

Took this query out of main templates because it is used in more than one place


This template gets the product list
checks that they all have the same currency
sets variable ordercurrency
evaluates all maxOrder Quantities


requires:
query getOrderHeader (for promotion details)
#PromoID#
#currentlocationid#
#frmProductIDs#
#evaluateMaxQuant#  (effectively defaults to true)

2008/09/25 GCC chnaged "campaignID = (" to "campaignID in (" for SQL2005 compatibility
2009/05/11 SSS CR-SNY673 Changed the way information is brought back and added changes for the affordable section
2015-11-16 WAB PROD2015-364 - products country restrictions

--->
<cfparam name="cboCountryID" default="">
<cfparam name="productlisttype" default="standard">

<cfif not isDefined("currentCountry")>
	<CFQUERY NAME="getcountry" datasource="#application.sitedatasource#">
		select countryid from location where locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset currentCountry = getcountry.countryid>
</cfif>

<cfif listlen(application.GenuineOrderStatii) eq 0>
	application.GenuineOrderStatii has no values. Please specify which rows in the status table are
	valid and rerun templates/setApplicationVariables.
</cfif>

<!--- get all active products
2001-04-26 WAB altered this query.  Over time it seems to have lost its ability to get country
specific products properly - it was working if a sku group was specified,
but this should only be necessary if the country variants have different SKUs,
SKU group shouldn't be necessary if it is the same SKU with eg. different price
--->
<!--- if the a different country to the standard has been set --->
<cfif cboCountryID is not "">
	<cfset currentCountry=cboCountryID>
</cfif>

<cfif productlisttype is "standard">
<!--- we use ranking to detemine the best match based on whether has been explicity scoped with this country
(as opposed to the country being a member of a countryGroup (region) which is scoped to the entity) --->
	<CFQUERY NAME="getProductList" datasource="#application.sitedatasource#">
	select * from (
		SELECT
			SKUGroup as productSKUGroup,
			*,
	(
		SELECT
			isnull(sum(i.quantity),0)
		FROM
					orderitem as i
						inner join
					orders as o on i.orderid=o.orderid
		WHERE
					prod.SKU = i.SKU
				and prod.PromoID = o.promoid
		and	o.locationid =  <cf_queryparam value="#currentlocationid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and	i.active=1
		and	o.orderstatus  in ( <cf_queryparam value="#application.GenuineOrderStatii#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and	o.orderdate >  <cf_queryparam value="#ReorderDisallowedCutOffDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			) as PreviouslyOrdered,

			dense_rank() over (
						partition by case when isNull(skugroup,'') = '' then convert(varchar,productid) else skugroup end
						order by case when hascountryscope = 0 then 0 when vcs.explicityScopedToThisCountry = 0 then 1 else 2 end desc)
						as rank

		FROM
			vProductList prod
				left join
			vcountryscope vcs on prod.productid = vcs.entityid and vcs.entity = 'product' and vcs.countryid =  <cf_queryparam value="#currentcountry#" CFSQLTYPE="CF_SQL_INTEGER">

		WHERE
			prod.deleted != 1 and prod.active = 1
		<!--- PromoID = '#PromoID#'
		WAB: 2000-12-06 Very stange goings on with this query giving error Process ID 33 attempting to unlock unowned resource PAG: 9:1:205981.
			seemed to work if I changed PromoID = '#PromoID#' for the select statement below!
		--->
		<!--- CPS 2001-03-23 If the promotion makes use of another promotion then select the products for that promotion --->
		<!--- 2008/09/25 GCC chnaged "campaignID = (" to "campaignID in (" for SQL2005 compatibility --->
		and campaignid in (select campaignid from promotion where promoid =  <cf_queryparam value="#getOrderHeader.UsePromoId EQ ''?PromoID:getOrderHeader.UsePromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		<!--- <CFIF getOrderHeader.UsePromoId EQ ''>
			campaignid in (select campaignid from promotion where promoid =  <cf_queryparam value="#PromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		<CFELSE>
			campaignid in (select campaignid from promotion where promoid =  <cf_queryparam value="#getOrderHeader.UsePromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</CFIF> --->
			and (prod.hascountryscope = 0 or vcs.countryid is not null)

		<CFIF isDefined("ProductIDList")>

			and productid  in ( <cf_queryparam value="#ProductIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

		</cfif>
	) as dummy
	where rank = 1
		<!--- sny-673 SSS 2009/05/11 changed this and added for the affordable section discountprice --->
		<cfif isDefined("sortSchema") and len(sortSchema)>
			ORDER BY <cf_queryObjectName value="#sortSchema#">
		<cfelseif compareNoCase(thisProductGroup, "AFFORDABLE") eq 0>
			ORDER BY discountprice desc
		<CFELSE>
			 ORDER BY Description
		</cfif>
	</CFQUERY>

<cfelse>

	<cfscript>
		getProductList=application.com.relayecommerce.getGlobalSKUProductset(#campaignid#,#cboCountryID#);
	</cfscript>

</cfif>

<cfquery name="getProductGroupID" dbtype="query">
	select distinct productgroupid, productgroup
	from getproductlist
	ORDER BY productgroup, productgroupid
</cfquery>

<cfif thisproductGroup eq "none" AND getProductGroupID.recordcount gt 0>
	<cfset thisproductgroup = getProductGroupID.productGroup>
</cfif>

<!--- sny-673 SSS 2009/05/11 changed the query to bring back the correct stuff for new category --->
<cfquery name="getProductsinGroupList" dbtype="query">
	select *
	from getproductlist
	where 1=1
	<cfif compareNoCase(thisProductGroup, "AFFORDABLE") eq 0 and isDefined("GetTotalPoints.PointsAvailable")>
		and discountprice <= #GetTotalPoints.PointsAvailable#
	<cfelse>
		<cfif not showAllProducts>
			and productgroup = '#thisProductGroup#'
		</cfif>
	</cfif>
	<cfif isDefined("sortSchema") and len(sortSchema)>
		ORDER BY <cf_queryObjectName value="#sortSchema#">
	<cfelseif compareNoCase(thisProductGroup, "AFFORDABLE") eq 0>
		ORDER BY discountprice desc
	<cfelse>
		ORDER BY Description
	</cfif>
</cfquery>

<!--- sny-673 SSS 2009/05/11 changed the query to bring back the correct stuff for new category --->
<cfquery name="getProductsNotinGroupList" dbtype="query">
	select *
	from getproductlist
	where 1=1
	<cfif compareNoCase(thisProductGroup, "AFFORDABLE") eq 0 and isDefined("GetTotalPoints.PointsAvailable")>
		and discountprice > #GetTotalPoints.PointsAvailable#
	<cfelse>
		and productgroup <> '#thisProductGroup#'
	</cfif>
	<cfif isDefined("sortSchema") and len(sortSchema)>
		ORDER BY <cf_queryObjectName value="#sortSchema#">
	<cfelseif compareNoCase(thisProductGroup, "AFFORDABLE") eq 0>
		ORDER BY discountprice desc
	<cfelse>
		 ORDER BY Description
	</cfif>
</cfquery>

<!--- I need to check that all products are using the same currency  --->
<!--- don't worry about zero value items --->
<!--- this won't work if the first item happens to be zero value and of a different currency to the rest--->
<CFIF getProductList.RecordCount is not 0>
	<CFSET ordercurrency  = trim(getProductList.PriceISOCode)>
	<CFLOOP query="getProductList">
		<CFIF DiscountPrice is not 0 and trim(PriceISOCode) is not orderCurrency>
			Items not all in same currency. Please <cfoutput><a href="mailto:#application.com.settings.getSetting('emailAddresses.supportEmailAddress')#">contact</a></cfoutput> your administrator.
			<cfset showScreen = false>
		</cfif>
	</cfloop>
<CFELSE>
	<CFSET ordercurrency = "">
</cfif>

<cfexit method="EXITTEMPLATE">

<cfif not isDefined("evaluateMaxQuant") or evaluateMaxQuant is true>
	<!---
		The application .cfm may set the variable MaxQOverride
		If it has this should be used instead of GetProductList.MaxQuantity

	DAM 01 Mar 2001
	--->

	<cfloop query="GetProductList">

		<cfif isDefined("MaxQOverride")>
			<CFSET newMaxQuantity = MaxQOverride>
		<CFELSEif MaxQuantity is "">
			<cfset newMaxQuantity = 0>
		<CFELSEif not isNumeric("MaxQuantity") >
			<CFSET newMaxQuantity = evaluate(MaxQuantity)>
		<cfelse>
			<cfset newMaxQuantity = MaxQuantity>
		</CFIF>

			<cfset x = querySetCell(getProductList,"MaxQuantity",newMaxQuantity,currentrow)>
	</cfloop>


</cfif>



