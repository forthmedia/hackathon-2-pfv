<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods
2001-03-28	CPS		use application variables StatusDescriptions 

--->

<CFCONTENT TYPE="text/tabdelimited">
<CFSET tmpOrderID = 1>
<CFSET frmdisplaymode = 'View'>

<CFQUERY NAME="getOrderIDs" datasource="#application.sitedatasource#">
select 	o.OrderID
from 	orders as o
where 	o.OrderStatus =  <cf_queryparam value="#constConfirmed#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFLOOP QUERY="getOrderIDs">
<CFQUERY NAME="getOrderHeader" datasource="#application.sitedatasource#">
select 	o.* ,
		p.firstname,
		p.lastname,
		l.sitename,
		c.countrydescription
from 	orders as o, location as l, person as p, country as c
where 	o.personid=p.personid
and		o.locationid=l.locationid
and		l.countryid=c.countryid
and 	o.orderid =  <cf_queryparam value="#getOrderIDs.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 

</CFQUERY>

<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 	*
	FROM vOrderItems
	WHERE	orderid =  <cf_queryparam value="#getOrderIDs.orderid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
	AND 	active = 1
</CFQUERY>

<CFOUTPUT>
Order Details
-------------
Order No      : #htmleditformat(getorderheader.orderid)# 
Placed by     : #htmleditformat(getorderheader.firstname)# #htmleditformat(getorderheader.lastname)#, #htmleditformat(getorderheader.sitename)# 
Date Created  : #dateformat(getorderheader.created)# 
Order Status  : #application.StatusDescriptions[getorderheader.orderstatus]#
<!--- Order Status  : #listfind(orderstatuslist,getorderheader.orderstatus)#
 --->
Delivery Details
----------------
Contact Name  : #htmleditformat(getorderheader.delcontact)#
Site Name     : #htmleditformat(getorderheader.delsitename)#
Address 1     : #htmleditformat(getorderheader.deladdress1)#
Address 2     : #htmleditformat(getorderheader.deladdress2)#
Address 3     : #htmleditformat(getorderheader.deladdress3)#
Address 4     : #htmleditformat(getorderheader.deladdress4)#
Address 5     : #htmleditformat(getorderheader.deladdress5)#
Postal Code   : #htmleditformat(getorderheader.delpostalcode)#
Country       : #htmleditformat(getorderheader.delcountry)#
Contact Phone : #htmleditformat(getorderheader.contactphone)#
Contact Fax   : #htmleditformat(getorderheader.contactfax)#
Contact Email : #htmleditformat(getorderheader.contactemail)#
Customer Ref  : #htmleditformat(getorderheader.theirordernumber)#

Invoice Details
---------------
Contact Name  : #htmleditformat(getorderheader.invcontact)#
Site Name     : #htmleditformat(getorderheader.invsitename)#
Address 1     : #htmleditformat(getorderheader.invaddress1)#
Address 2     : #htmleditformat(getorderheader.invaddress2)#
Address 3     : #htmleditformat(getorderheader.invaddress3)#
Address 4     : #htmleditformat(getorderheader.invaddress4)#
Address 5     : #htmleditformat(getorderheader.invaddress5)#
Postal Code   : #htmleditformat(getorderheader.invpostalcode)#
Country       : #htmleditformat(getorderheader.invcountry)#

Item Details
------------
</CFOUTPUT>
<CFOUTPUT query="getorderitems">
	SKU                  : #htmleditformat(SKU)#
	Description          : #LEFT(Description,58)#
	Quantity             : #htmleditformat(Quantity)#
	List Price           : #decimalformat(UnitListPrice)#
	Discount Price       : #decimalformat(UnitPrice)#
	Total Discount Price : #decimalformat(TotalPrice)#
	
	......................................................
</CFOUTPUT>

<CFOUTPUT>

=================================================================================================
=================================================================================================
</CFOUTPUT>
</CFLOOP>
<CFLOCATION URL="AfterDownload.cfm"addToken="false">
