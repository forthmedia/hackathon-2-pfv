<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			displayorderhistory.cfm
Author:			?
Date created:	?

Description:	displays users order history

Version history:
1

Amendment history
--->

<cf_head>

<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput> | Order History</cf_title>

</cf_head>
<CFSETTING ENABLECFOUTPUTONLY="no">
<CFIF IsDefined("url.eid")>
	<CFSET eID=url.eid>
</CFIF>


<CFPARAM name="requiredstatus" default="#RealOrderStatuses#">

<!--- not sure which query to use! --->
<CFQUERY NAME="getPreviousOrders" datasource="#application.sitedatasource#">
SELECT 	orderid,
		orderdate,
		orderstatus,
		firstname,
		lastname, 
		(select count(1) from orderitem as i where i.orderid = o.orderid and active=1) as numberofitems,
		prm.BorderSet		
from 
		orders as o, 
		person as p,
		Promotion as prm	
where 
		orderstatus  in ( <cf_queryparam value="#requiredstatus#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
and 	o.personid=p.personid
and		prm.promoid = o.promoid
and		o.locationid  in ( <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<cfif isDefined("promoid")>
and		o.promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfif>
<cfif isDefined("frmPersonID")>
and		o.personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfif>

order by 
		orderdate desc
</CFQUERY>


<CFQUERY NAME="getPreviousOrdersDetail" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC" DEBUG>
SELECT 	o.orderid,
			orderdate,
			orderstatus,
			firstname,
			lastname, 
			i.SKU,
			description,
			quantity
	from orders as o, person as p, orderitem as i, product as pr
	where orderstatus in (#requiredstatus#)
	and 	o.personid=p.personid
	and		o.locationid in (#frmLocationID#)
	and		o.orderid=i.orderid
	and		i.active = 1
	and 	pr.productid=i.productid
	<cfif isDefined("frmPersonID")>
	and		o.personid = #frmPersonID#
	</cfif>
	order by orderdate desc</CFQUERY>

<CFOUTPUT>

	<SCRIPT type="text/javascript">
	<!--
	
		function jsSubmit()  {
			document.mainForm.submit()
		}
	
		function displayOrder(page,orderid)  {
			form = document.mainForm;
			if (page == "view") {
				form.action = "/orders/displayOrder.cfm"
				form.borders.value = "yes"
			}
			else {
				form.action = "/orders/editOrder.cfm"
			}
			form.currentOrderID.value=orderid;
			form.submit()
		}				
	//-->
	
	</SCRIPT>
</CFOUTPUT>



<cf_translate>
<cf_DisplayBorder BorderSet="#GetPreviousOrders.BorderSet#">
<CFOUTPUT>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="2" CELLPADDING="2">
	<TR>
		<TD COLSPAN="4">
			<CFIF requiredstatus IS "1">
				<H1>phr_Order_OutstandingQuotes</H1>
			<CFELSE>
				<H1>phr_Order_PreviousOrders</H1>
			</CFIF>
		</TD>
	</TR>
</CFOUTPUT>
	<CFIF getPreviousOrdersDetail.recordcount IS NOT 0>
	
		<CFOUTPUT>
			<TR>
				<th><B>phr_Order_OrderNo.</B></th>
				<th><B>phr_Order_OrderDate</B></th>
				<th><B>phr_Order_OrderedBy</B></th>
				<TH>&nbsp;</TH>
			</TR>
		</CFOUTPUT>
		
		<CFOUTPUT query="getPreviousOrdersDetail" group="orderid">	
			<TR>
				<TD class="Label">#htmleditformat(orderid)#</TD>
				<TD class="Label">#dateformat(orderdate)#</TD>
				<TD class="Label">#htmleditformat(firstname)# #htmleditformat(LastName)#</TD>
				<TD class="Label"><A HREF="javascript:displayOrder('<CFIF orderstatus IS 1>edit<CFELSE>view</CFIF>',#htmleditformat(orderid)#)" >phr_Details</A></TD>
			</TR>
			<TR>
				<TD class="midlabel"><B>phr_Quantity</B></TD>
				<TD class="midlabel"><B>SKU</TD>
				<TD class="midlabel"><B>phr_Description<B></TD>
				<TD>&nbsp;</TD>
			</TR>
			<CFOUTPUT>	
				<TR>
					<TD>#htmleditformat(quantity)#</TD>
					<TD>#htmleditformat(SKU)#</TD>
					<TD><cfif GetOrderHeader.TranslateDescription>phr_description_product_#htmleditformat(productid)#<cfelse>#htmleditformat(Description)#</cfif></TD>
					<TD>&nbsp;</TD>
				</TR>
			</CFOUTPUT>
			<TR>
				<TD COLSPAN="4">&nbsp;</TD>
			</TR>
		</CFOUTPUT>	
		<TR>
			<TD COLSPAN="4">&nbsp;</TD>
		</TR>
	
	<CFELSE>
		<TR>
			<TD COLSPAN="4"><CFOUTPUT>phr_Order_NoMatchingTransactionsFound</CFOUTPUT></TD>
		</TR>
	</cfif>


<CFOUTPUT></TABLE></CFOUTPUT>


<CFOUTPUT>
<FORM ACTION="" NAME="mainForm" METHOD="POST">
	<INPUT TYPE="HIDDEN" NAME="currentOrderID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="borders" VALUE="">
	<CFIF isDefined("eid")><CF_INPUT TYPE="hidden" NAME="EID" VALUE="#eid#"></CFIF>
	<CFINCLUDE template="..\screen\_FormVariablesForViewer.cfm">


</FORM></CFOUTPUT>

</cf_DisplayBorder>
</cf_translate>
<CFSETTING ENABLECFOUTPUTONLY="yes">
