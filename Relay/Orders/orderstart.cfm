<!--- �Relayware. All Rights Reserved 2014 --->
<!--- orderstart.cfm --->
<!--- opening page for the order system --->
<!--- allows user to chose a promoid and a person (if not already specified) --->
<!--- if parameters are already specified then goes straight to orderitems.cfm --->
<!--- might also have links for pulling up reports of previous orders --->


<!--- amendments
1201000 WAB	Check for No Campaigns allowed
120100	WAB	javascript verification was wrong
 --->

<CFINCLUDE template = "applicationOrdersIni.cfm">	<!--- need to do this because this page can be included from pages in other directories --->

<CFIF CampaignIDAllowed is "">
	You do not have rights to any campaigns.
	<CF_ABORT>
</cfif>



<CFPARAM name = "frmPersonID"  default="0">
<CFPARAM name = "frmLocationID"  default="0">

<!--- if valid frmPersonID or frmLocationId are passed then use them.  Otherwise look for a currententityid --->
<CFIF frmPersonid is 0 and frmLocationID is 0>
	<CFIF isdefined ("frmEntityType")>
		<CFSET "frm#frmEntityType#ID" = frmCurrentEntityID>
	<CFELSE>
		No entity set
		<CF_ABORT>
	</cfif>
</cfif>

<CFSET frmPersonID = listfirst(frmPersonID)>  <!--- there is a chance that a list of personids has been passed, just take first --->


<CFPARAM name="frmPromoId" default="">

<CFIF frmPromoId is not "" and isdefined("frmPersonID")>
	<!--- can go straight to orderitems.cfm --->
	<CFINCLUDE template="orderitems.cfm">
	<CF_ABORT>

</cfif>


	<!---Get a list of the CampaignIDs the User is entitled to use--->
	<CFQUERY NAME="GetPromos" datasource="#application.siteDataSource#">
		Select CampaignID, PromoID
		From promotion
		Where CampaignID  IN ( <cf_queryparam value="#CampaignIDAllowed#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY PromoID
	</CFQUERY>


	<!--- get list of people at this location --->
	<CFQUERY NAME="getPeople" datasource="#application.sitedatasource#">
		select FirstName, LastName, PersonID, LocationID
		from person
		where locationid =
		<CFIF frmPersonID is not 0>
			(select locationid from person where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		<CFELSEIF frmLocationID is not 0>
			#frmLocationID#
		</cfif>
	</CFQUERY>


	<CFSET thisLocationID = getPeople.LocationID>	


<cf_head>
	<SCRIPT type="text/javascript">
	<!--

	function jsSubmit()  {
	
		return placeOrder()
			
	}

	
	
	function placeOrder()  {
		<!--- verify that personid and promotionid are set and submit form --->
		form= document.mainForm
		msg = ''

			if (form.frmPromoID[form.frmPromoID.selectedIndex].value == '' )  {
				msg += 'Please select a campaign \n'
			}
			if (form.frmPersonID[form.frmPersonID.selectedIndex].value == 0)  {
				msg += 'Please select the Person placing the order\n'
			}

			if (msg != '') {
				alert (msg)   ;
	
			}  else {
				form.submit()	;
			}


	}

	function displayOrderHistory(status)  {
		form = document.mainForm2
				form.requiredStatus.value=status
				form.submit()

	}

	
	//-->

	</SCRIPT>




	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
</cf_head>






<FORM ACTION="../orders/orderItems.cfm" NAME="mainForm" METHOD="POST">
	<CFINCLUDE template="..\screen\_FormVariablesForViewer.cfm">
	
	<TABLE border="0">
	<TR>
		<TD colspan="4"><b>To place an order select a campaign and a person and click the 'Place Order' button</b></td>
	
	</tr>
	
	<tr><td collspan="4" height="12">&nbsp;</td></tr>
	<TR>
		<TD>Campaign</td>
		<TD><SELECT NAME="frmPromoID">
				<OPTION VALUE="">Select a Campaign
			<CFOUTPUT query="getPromos">
				<OPTION VALUE="#promoid#" <CFIF frmpromoid is promoid>Selected</cfif> >#htmleditformat(PromoID)# 
			</cfoutput>
			</SELECT>	
		</td>
	

	
		<TD align="right">Ordered By</td>
		<TD align="right"><SELECT NAME="frmPersonID">
				<OPTION VALUE="" <CFIF  frmPersonID is "0">Selected</cfif>>
			<CFOUTPUT query="getPeople">
				<OPTION VALUE="#PersonId#" <CFIF frmPersonID is personID>Selected</cfif>>#htmleditformat(FirstName)# #htmleditformat(LastName)# 
			</cfoutput>
			</SELECT>	
		</td>
	
	</tr>	
	
	<tr><td collspan="4" height="12">&nbsp;</td></tr>
	
	<TR>
		
	<TD colspan="4" align="right"><a href="javascript:placeOrder()"><b>Place Order</b></a></td>
	
	</tr>

</table>
	
	
</FORM>		
	
<P>	
<P>
<!--- find out whether there are any previous transactions --->
<CFQUERY NAME="getPreviousOrders" datasource="#application.sitedatasource#">
SELECT 	orderid
FROM 	orders as o
WHERE	o.locationid =  <cf_queryparam value="#thisLocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
and		o.orderstatus  in ( <cf_queryparam value="#realorderstatuses#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>

<CFQUERY NAME="getPreviousQuotes" datasource="#application.sitedatasource#">
SELECT 	orderid
FROM 	orders as o
WHERE	o.locationid =  <cf_queryparam value="#thisLocationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
and		o.orderstatus in (1)
</CFQUERY>

<FORM ACTION="../orders/displayorderhistory.cfm" NAME="mainForm2" METHOD="POST">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#thislocationid#">
	<INPUT TYPE="HIDDEN" NAME="requiredStatus" VALUE="">
	<CFINCLUDE template="..\screen\_FormVariablesForViewer.cfm">


<table border="0">
<TR>
	<td width="180">
	
	<CFIF getPreviousOrders.recordcount is not 0>
		<!--- Details of previous orders --->
		<FONT FACE="Arial,Chicago" SIZE="2"><A HREF="javascript:displayOrderHistory('<CFOUTPUT>#htmleditformat(RealOrderStatuses)#</cfoutput>')">Show Report of Previous Orders</A> 

	<CFELSE>
		No Previous Orders
	</cfif>
	</td>

	
	<td align="middle">
	<CFIF getPreviousQuotes.recordcount is not 0>
		<!--- Details of outstanding quotes --->
		<FONT FACE="Arial,Chicago" SIZE="2"><A HREF="javascript:displayOrderHistory('1')">Show Report of Previous Quotes</A><BR> (use to pick up a half completed order) 
	<CFELSE>
		No Previous Quotes	
		
	</cfif>
	</td>	

</tr>
	

	</table>

</FORM>
	



	
	
	

	
	
	
