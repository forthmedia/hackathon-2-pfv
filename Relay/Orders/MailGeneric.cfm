<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:  WAB 030200  Uses variable Delivery Address to get properly formatted address
	WAB 2001-03-26		altered where the emails come from (variable wasn't defined)
		SSS 2007-10-30 Added the group attribute to cfmail so the email goes out correctly
		
	2014-04-25 PPB Case 439307 prefix description with getorderitems.		
 --->	

<!---Generic Mail Template--->
<CFSET ordertotal = getOrderValue.TotalValue + getOrderHeader.TaxOnOrder + getOrderHeader.ShippingCharge >

<!--- have to translate these items in advance, 'cause we need to know their lenghts to get the formatting right --->
<cf_translate phrases="order_OrderNumber,order_NumberOfItems,order_ProductCode,description,order_UnitPoints,order_TotalPoints,quantity,order_DiscountedUnitPrice,order_DiscountedTotalPrice,order_Totaldiscountedvalue,delivery,VAT,Total,order_Tobepaidby,order_PaymentMethod"  />

<CFSET tab1 = 15>
<CFSET tab2 = 30>
<CFSET LF=Chr(13) & Chr(10)>
<CF_MAIL query="caller.getorderitems"  
		group ="orderID"
		TO="#orderToEmail#" 
		CC="#application.supportEmailAddress#"
		BCC="#application.copyAllEmailsToThisAddress#"
		FROM="#application.ordersEmailFrom#"
		SUBJECT="#application.EmailFromDisplayName# - Order No:#currentorderid#"
		><CF_translate>
		#LF#
#getorderheader.firstname# #getorderheader.lastname#,#LF#
#LF#
phr_order_Thankyouforyourorder#LF#
#LF#
#phr_order_OrderNumber# #repeatString(" ",max(0,tab1-Len(phr_order_OrderNumber)))# #currentorderid##LF#
#phr_order_NumberOfItems# #repeatString(" ",max(0,tab1-Len(phr_order_NumberOfItems)))# #getorderitems.recordcount##LF#
#LF#
phr_order_ItemsOrdered#LF#
#LF#
<CFOUTPUT>
#phr_order_ProductCode# #repeatString(" ",max(0,tab2-Len(phr_order_ProductCode)))# #getorderitems.SKU##LF# 
#phr_Description# #repeatString(" ",max(0,tab2-Len(phr_description)))# <cfif GetOrderHeader.TranslateDescription>phr_description_product_#getorderitems.productid#<cfelse>#getorderitems.Description#</cfif>#LF#			<!--- 2014-04-25 PPB Case 439307 prefix description with getorderitems.  --->
#phr_Quantity# #repeatString(" ",max(0,tab2-Len(phr_Quantity)))# #getorderitems.Quantity##LF#
#phr_order_DiscountedUnitPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedUnitPrice)))# #tmpCurrencySymbol# #numberFormat(getorderitems.UnitDiscountPrice,"___,___,___.__")##LF#
#phr_order_DiscountedTotalPrice# #repeatString(" ",max(0,tab2-Len(phr_order_DiscountedTotalPrice)))# #tmpCurrencySymbol# #numberFormat(getorderitems.TotalDiscountPrice,"___,___,___.__")##LF#
#LF#


</CFOUTPUT>#phr_order_Totaldiscountedvalue# #repeatString(" ",max(0,tab2-Len(phr_order_TotalDiscountedValue)))# #tmpCurrencySymbol# #numberFormat(getOrderValue.TotalValue,"___,___,___.__")##LF#
<cfif getOrderHeader.ShowDeliveryCalculation>
#phr_Delivery# #repeatString(" ",max(0,tab2-Len(phr_Delivery)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.shippingCharge,"___,___,___.__")##LF#
</cfif>
<cfif getOrderHeader.ShowVATCalculation>
#phr_VAT# #repeatString(" ",max(0,tab2-Len(phr_VAT)))# #tmpCurrencySymbol# #numberFormat(getOrderHeader.taxOnOrder,"___,___,___.__")##LF#
</cfif>
#phr_Total# #repeatString(" ",max(0,tab2-Len(phr_Total)))# #tmpCurrencySymbol# #numberFormat(orderTotal,"___,___,___.__")##LF#
<cfif not (getOrderHeader.ShowDeliveryCalculation and getOrderHeader.ShowVATCalculation)>phr_Order_DeliveryByDistributor#LF#
</cfif>
<cfif getOrderHeader.ShowPaymentOptions>#phr_order_PaymentMethod##LF#
#phr_order_Tobepaidby# #repeatString(" ",max(0,tab1-Len(phr_order_toBePaidBy)))# #getorderheader.paymentmethod##LF#
#LF#
</cfif>
<CFIF NOT IsDefined("noShipping")>
phr_order_ShippingDetails#LF#
#LF#
#LF#
phr_order_Thisorderwillbedeliveredto:#LF#
#getorderheader.delcontact##LF#
#getorderheader.delsitename##LF#
#DeliveryAddress##LF#
#LF#
#LF#
</CFIF>
phr_order_BillingDetails#LF#
#LF#
#LF#
#getorderheader.invcontact##LF#
#getorderheader.invsitename##LF#
#InvoiceAddress##LF#
#LF#
</CF_translate>
</CF_MAIL>



