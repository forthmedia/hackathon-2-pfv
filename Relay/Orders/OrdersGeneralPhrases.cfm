<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			OrdersGeneralPhrases.cfm
Author:			DJH
Date created:	2000-11-24

Description:	All the "general" phrases used within the orders system

Version history:
1

--->

<cfset GeneralPhrases=
"Points,
Or,
Continue,
Edit,
Total,
Yes,
No,
Description,
Quantity,
Delivery,
VAT,
ContactEmail,
ContactFax,
ContactName,
ContactPhone,
Country,
DateCreated,
Order,
Details,
UpdateRecords,
Refresh,
PE_Choosepaymentmethod,
close,
Region,
Netherlands,
France,
CentralEurope,
Iberia,
Italy,
MCR,
Nordics,
Sweden,
Norway,
Denmark,
Finland,
GB,
UK,
Ireland,
TU_DISPLAYQUOTE_JS_PRINT,
Cheque,
Visa_Mastercard,
WireTransfer,
payment_CC,
payment_WT,
payment_CK
">
