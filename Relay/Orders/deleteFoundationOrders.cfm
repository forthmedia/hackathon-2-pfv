<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			deleteFoundationOrders.cfm
Author:			DJH
Date created:	20 July 2000

Description:	Deletes all orders from Evaluation2000 and TradeUp which belong to Foundation Network.

Version history:
1

--->




<cf_head>
	<cf_title>Delete Foundation Orders</cf_title>
</cf_head>




<cfif not checkpermission.level3>
You're not Authorised!
<CF_ABORT>
</cfif>


<cfif not isDefined("confirmed")>

If you're sure you want to delete the records for Foundation Network from Evaluation2000 / TradeUp, check the boxes and hit
Go.
<form name="aform" action="deleteFoundationOrders.cfm" method="post">
<input type="checkbox" name="confirmed" value="TradeUp"> TradeUp?<br>
<input type="checkbox" name="confirmed" value="Evaluation2000"> Evaluation2000?<br>
<input type="submit" value="Go">
</form>
<cfelse>
	<cfif confirmed contains "TradeUp">
		<cftransaction>
		<cfquery name="countTradeUp" datasource="#application.siteDataSource#">
			select
				*
			from
				TradeUPTransaction
			where
				LocationID = 1
		</cfquery>

		<cfif countTradeUp.recordcount gt 0>
			
			<cfset transactionnos = valuelist(countTradeUp.TransactionNo)>
			
			<cfquery name="deleteTradeUp" datasource="#application.siteDataSource#">
				delete from 
					TradeUpTransaction
				where 
					LocationID = 1
			</cfquery>
	
			<cfquery name="deleteTradeUp" datasource="#application.siteDataSource#">
				delete from
					TradeUpSN
				where
					TransactionNo  in ( <cf_queryparam value="#transactionnos#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			</cfquery>
	
			<cfquery name="deleteTradeUp" datasource="#application.siteDataSource#">
				delete from
					TradeUpGroup
				where
					TransactionNo  in ( <cf_queryparam value="#transactionnos#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			</cfquery>
	
			<cfquery name="deleteTradeUp" datasource="#application.siteDataSource#">
				delete from
					TradeUpItems
				where
					TransactionNo  in ( <cf_queryparam value="#transactionnos#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			</cfquery>
		</cfif>
		<cfoutput>Deleted all #countTradeUp.recordcount# Foundation Network transactions for TradeUp</cfoutput><br>
		</cftransaction>
	</cfif>
	
	<cfif confirmed contains "Evaluation2000">
		<cftransaction>
		<cfquery name="countEvaluation2000" datasource="#application.siteDataSource#">
			select 
				orderid 
			from 
				orders 
			where 
				promoid = 'Evaluation2000' 			
				and LocationID = 1				
		</cfquery>
		<cfif countEvaluation2000.recordcount gt 0>
			<cfquery name="deleteEvaluation2000" datasource="#application.siteDataSource#">
				delete from 
					Orders 
				where 
					promoid = 'Evaluation2000' 			
					and LocationID = 1
			</cfquery>
	
			<cfset orderids = valuelist(countEvaluation2000.orderid)>
	
			<cfquery name="deleteEvaluation2000" datasource="#application.siteDataSource#">
				delete from 
					orderitem 
				where
					orderid  in ( <cf_queryparam value="#orderids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
				
		</cfif>
		<cfoutput>Deleted all #countEvaluation2000.recordcount# Foundation Network transactions for Evaluation2000</cfoutput>
		
		</cftransaction>
	</cfif>
</cfif>



