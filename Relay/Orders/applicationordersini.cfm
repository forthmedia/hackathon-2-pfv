<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2009/06/22

Trying to cut this stuff out of application. cfm and have it included by the security system

 --->

<cfsetting enablecfoutputonly="yes"> 

<!--- 
Mods
WAB 030200  added variable AddressFieldsUsed. Currently limits address fields to the basic ones and ignores the others

SWJ 18Jan2001 added the query which gets the order status

DAM 27 added functionality to determine if there is a Max Quantity Location override

CPS 28032001 set variable realorderstatuses to new application variable GenuineOrderStatii 

NJH 2006/08/18 included ordersparam.cfm which defines and defaults parameters for orders
--->

<cfparam name="portalViewOnly" default="false">

<!--- NJH 2006/08/18 defining and defaulting order parameters --->
<cfinclude template="ordersParam.cfm">
	  
<CFIF IsDefined("url.eid")>
	<CFSET eID=url.eid>
</CFIF>

<CFSET thisdir="/orders">
<!--- CPS 28032001 this code no longer required as the details are stored in application variables --->
<!--- <CFSET orderstatuslist="Quote,Confirmed, Downloaded for processing,Dispatched,Cancelled,Sent to Remote System,Accepted by Remote System,Rejected by Remote System,cancelled by tester,Partial Dispatch,Dispatched">
<CFSET RealOrderStatuses="2,3,4,6,7,8,10,11"> --->


<!---
		The variable form.promogroup needs to be established to make sure that the correct borderset
		is displayed.
		
		this variable can determine whether or not a submenu is displayed in the border set.

DAM 24 march 2001
--->
<CFIF isDefined("url.productid") or isdefined("form.productid") or isdefined("url.frmpromoid") or isdefined("form.frmpromoid")>
	<CFIF isDefined("url.productid")>
		<CFSET tempProduct = url.productid>
	<CFELSEIF isdefined("form.productid")>
		<CFSET tempProduct = form.productid>
	<CFELSEIF isdefined("url.frmpromoid")>
		<CFSET tempProduct = url.frmpromoid>
	<CFELSEIF isdefined("form.frmpromoid")>
		<CFSET tempProduct = form.frmpromoid>
	</CFIF>
	<CFQUERY NAME="PromoGroup" datasource="#application.siteDataSource#">
		SELECT PromotionGroup.Description, 
	           promotion.PromoID
	       FROM promotion 
		   INNER JOIN PromotionGroup ON promotion.promotiongroupid = PromotionGroup.promotiongroupid

			<CFIF isdefined("ORDERID")>
				inner join 
				order on order.promoid=promotion.promoid
			<CFELSE>
				inner join
				product on product.campaignid = promotion.campaignid
			</CFIF>
	       WHERE   
		   	<CFIF isDefined("url.productid") or isdefined("form.productid")> 
	               	product.productid  IN ( <cf_queryparam value="#tempProduct#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFELSE>
					promotion.promoid IN (<cf_queryparam value="#tempProduct#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</CFIF>
	</CFQUERY>
	<CFSET form.promogroup = "#PromoGroup.Description#">
</CFIF>

<!---
		The Products table contains a field that sets a Maximum order quantity against each product
		The requirement has a risen to permit an override of this field's value for all products
		if the Users Location is flagged in the relevant way.
		
		A flag has been set up in the Database which is 

DAM 27 FEb 2001
--->


<cfif isDefined("promoid")>

	<CFQUERY NAME="PromoGroup" datasource="#application.siteDataSource#">
		SELECT     
			PromotionGroup.Description, 
			promotion.PromoID
		FROM         promotion INNER JOIN 
	                      PromotionGroup ON promotion.promotiongroupid = PromotionGroup.promotiongroupid
		WHERE     
			(promotion.PromoID =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</CFQUERY>
	<CFSET form.promogroup = "#PromoGroup.Description#">
	<CFIF promoid EQ "Evaluation2000">
		<CFSET MaxOrderOverrideFlagID=1357>
		<CFQUERY NAME="MaxQ" datasource="#application.siteDataSource#">
			SELECT 
				Data as Override
			FROM
				(SELECT EntityID, Data FROM IntegerFlagData WHERE FlagID =  <cf_queryparam value="#MaxOrderOverrideFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > ) I
					INNER JOIN Location L
					ON I.EntityID =  L.LocationID
						INNER JOIN (SELECT PersonID, LocationID FROM Person P WHERE PersonID = #request.relayCurrentUser.personID#) P
						ON P.LocationID = L.LocationID
		</CFQUERY>
		<CFIF MaxQ.recordcount>
			<CFIF isnumeric("#MaxQ.recordcount#")>
				<CFSET MaxQOverride=MaxQ.Override>
			</CFIF>
		</CFIF>
	</CFIF>
</CFIF>



<!--- CPS 28032001 this code no longer required as the details are stored in application variables via 
     ../templates/setApplicationVariables. However, to save amending lots of templates I have simply 
	 set the variable realorderstatuses to application.genuineorderstatii --->
	  
<!--- <CFQUERY NAME="GetOrderStatii" datasource="#application.siteDataSource#">
	Select statusID, description from status
	ORDER BY statusID
</CFQUERY>
<CFSET orderstatuslist=valueList(GetOrderStatii.description)>
<CFSET RealOrderStatuses=valueList(GetOrderStatii.statusid)> --->

<CFSET RealOrderStatuses=application.GenuineOrderStatii>

<cfset constStatusTextIDs = "Quote,Confirmed,Downloaded,Dispatched,Cancelled,Pending,SentToRemote,AcceptedByRemote,RejectedByRemote">

<cfquery name="getStatusIDs" datasource="#application.siteDataSource#">
	select statusID, statusTextID from status where statusTextID  in ( <cf_queryparam value="#constStatusTextIDs#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
</cfquery>

<cfset constStruct = application.com.structureFunctions.queryToStruct(query=getStatusIDs,key="statusTextID")>

<CFSET constQuote = constStruct.quote.statusID>
<CFSET constConfirmed = constStruct.confirmed.statusID>
<CFSET constDownloaded = constStruct.downloaded.statusID>
<CFSET constDispatched = constStruct.dispatched.statusID>
<CFSET constCancelled = constStruct.cancelled.statusID>
<cfset constPending = constStruct.pending.statusID>

<!--- Used by POS Integration program when sent to client logic --->
<CFSET constSentToRemote = constStruct.sentToRemote.statusID>
<CFSET constOKInRemote = constStruct.acceptedByRemote.statusID>
<CFSET constErrorToRemote = constStruct.rejectedByRemote.statusID>

<CFSET EditableOrderStatuses="#constQuote#,#constPending#">
<CFSET SuperUserEditableOrderStatuses="#constQuote#,#constErrorToRemote#">

<CFSET ReorderDisallowedCutOffDate = createodbcdate(now()-180)>

<!--- Visa/Mastercard,Wire Transfer,Cheque --->
<cfparam name="paymentmethods" default="CC,CK,WT">
<cfparam name="WorldPayAccountID" default="">

<cf_include template="\code\cftemplates\ordersINI.cfm" checkIfExists="true">

<CFSET addressFieldsUsed = "Address1,Address2,Address3,Address4,Address5,PostalCode,Countryid">

<!---Get a list of the CampaignIDs the User is entitled to use--->
<CFQUERY NAME="Promotions" datasource="#application.siteDataSource#">
	Select CampaignID, ZeroPrice 
		From promotion
		<!--- 2007/12/10 - GCC - Trying view access to catalogue --->
		<cfif not portalViewOnly>
		Where CampaignID IN 
		(select recordid --, rg.personid, rg.usergroupid
			from recordrights as rr
			inner join rightsGroup as rg 
				on rr.userGroupid = rg.usergroupid
			where entity = 'promotion' 
			and personid = #request.relayCurrentUser.personID#)
		</cfif>
		ORDER BY PromoID
</CFQUERY>

<CFSET CampaignIDAllowed = ValueList(Promotions.CampaignID)>

<!---Start security project SSS 2009/06/19--->
<cfset checkPermissionlevel2 = application.com.login.checkInternalPermissions ("ordertask","Level2")>
<cfset checkPermissionlevel3 = application.com.login.checkInternalPermissions ("ordertask","Level3")>
<!---finish security project SSS 2009/06/19--->

<!---Set permission for download if the user has level 2 rights for Order Task--->
<CFIF checkPermissionlevel2> <!---security project SSS 2009/06/19<CFIF checkPermission.level2 EQ 1>--->
<!--- WAB changed this to level 3, download is at the moment a bit of a risky function<CFSET Download = 1> --->
</CFIF>

<CFIF checkPermissionlevel3><!---security project SSS 2009/06/19<CFIF checkPermission.level3 EQ 1>--->
	<CFSET Download = 1>
	<CFSET Maintenance = 1>
</CFIF>

<cfset DistiOrderFulfillmentWarning = 7>

<!--- Include application specific files for this area. This allows application specific
	  variables to be set. For example, the organisation id for 3com is only set if the 
	  user belongs to the 'PartnerFocusLevel' group. For other installations such as
	  kingston, the organisation id is simply taken from the person record ---> 	

<!--- <CFINCLUDE TEMPLATE="../_ApplicationSpecificFiles/OrderFile.cfm"> --->

<!--- 
if the promoid has been defined, need to look to see whether "unique" translations are going to be used
--->

<cfset UniqueTrans = "">
<cfif isDefined("promoid")>
	<cfquery name="getcampaignid" datasource="#application.siteDataSource#">
		select
			campaignid,
			uniquetranslations, 
			case when isnull(uniquetranslationSuffix,'') = '' then campaignid else uniquetranslationSuffix end as uniquetranslationSuffix ,
			borderSet			
		from
			promotion
		where
			promoid =  <cf_queryparam value="#promoid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<CFIF getcampaignid.BorderSet NEQ "">
		<CFSET StandardBorderSet = getcampaignid.BorderSet>
	</cfif>	
	
	<cfif getcampaignid.UniqueTranslations eq 1>
		<cfset UniqueTrans = getcampaignid.uniquetranslationSuffix >
	</cfif>


</cfif>

<CFINCLUDE TEMPLATE="../templates/CheckBrowser.cfm">
<cfsetting enablecfoutputonly="no"> 




