<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Capture this moment in time --->
<!---
2015-11-16  DAN PROD2015-364 - products country restrictions

--->
<CFSET FreezeDate = request.requestTimeODBC>

	<CFSET user = request.relayCurrentUser.userGroupId>
	
<!--- get existing order items, will return nothing for new order --->
<CFQUERY NAME="getOrderItems" datasource="#application.sitedatasource#">
	Select 	*
	FROM vOrderItems
	WHERE	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
	AND 	active = 1

</CFQUERY>

<CFSET currentorderitems = valuelist(getorderitems.productid)>


<CFTRANSACTION>
<!--- loop through all SKUs in form --->
<!--- loop through all SKUs in form --->
<CFLOOP list="#SKUsonPage#" index="skuid">

	<CFIF #evaluate("quant_"&skuid)#  IS NOT 0>
		<!--- ie there is a quantity of this item set --->

			<!--- get actual SKU --->
			<CFQUERY NAME="getItemDetails" datasource="#application.sitedatasource#">			
			SELECT p.*
			FROM vProductDetails p
            left join vCountryScope vcs on vcs.entity='product' and vcs.entityid = p.productid and vcs.countryid = <cf_queryparam value="#currentcountry#" cfsqltype="CF_SQL_INTEGER">
			WHERE productid =  <cf_queryparam value="#skuid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND promoID =  <cf_queryparam value="#PromoID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			<!--- AND countryID In (0,#currentcountry#,#currentregion#) --->
            and (p.hascountryScope = 0 or vcs.countryid is not null)
			
			<!---select p.SKU, PromoPrice as price, discountprice, listprice
			from productInventory as p, ProductPromo as pp
			where p.productid=#skuid#
			AND p.SKU = pp.SKU
			AND pp.promoID = '#PromoID#'--->
			</CFQUERY>

			
	  	<CFIF listcontains(currentorderitems, skuid) IS NOT 0>
			<!--- order currently contains this item, need to update record--->	
			
			 <CFQUERY NAME="updateOrderItem" datasource="#application.sitedatasource#">			
			Update OrderItem 
			SET 	quantity =  <cf_queryparam value="#evaluate("quant_"&skuid)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					unitprice =  <cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="cf_sql_float" > ,
					totalprice =  <cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="CF_SQL_money" >  * #evaluate("quant_"&skuid)#,
					ProductID =  <cf_queryparam value="#getitemdetails.ProductID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			where SKU =  <cf_queryparam value="#getitemdetails.sku#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			and orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY> 

		<CFELSE>
			<!--- order doesn't already contains this item, need create new record--->	
			  <CFQUERY NAME="insertOrderItems" datasource="#application.sitedatasource#">			 
				Insert Into OrderItem (orderid, sku, quantity,<!--- unitprice, --->unitdiscountprice,<!--- totalprice, --->totaldiscountprice,unitlistprice, totallistprice, lastupdatedby,lastupdated, createdby,created, Active,ProductID)
				Values ( 	<cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#getitemdetails.sku#" CFSQLTYPE="CF_SQL_VARCHAR" >,					
						<cf_queryparam value="#evaluate("quant_"&skuid)#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="CF_SQL_decimal" >, 
						<cf_queryparam value="#evaluate(getitemdetails.DiscountPrice*evaluate("quant_"&skuid))#" CFSQLTYPE="CF_SQL_decimal" >,
						<cf_queryparam value="#getitemdetails.DiscountPrice#" CFSQLTYPE="CF_SQL_decimal" >,
						<cf_queryparam value="#evaluate(getitemdetails.DiscountPrice*evaluate("quant_"&skuid))#" CFSQLTYPE="CF_SQL_decimal" >,
						<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						1,
						<cf_queryparam value="#getitemdetails.ProductID#" CFSQLTYPE="CF_SQL_INTEGER" >)
				</CFQUERY>
		</CFIF>	
	
	<CFELSE>
		<!--- ie no quantity of this item set --->
		<CFIF listcontains(currentorderitems, skuid) IS NOT 0>
		<!--- need to delete item from order if already exists --->
		<!--- for some reason I decided to just set to inactive rather than delete --->
		
			<CFQUERY NAME="InactivateOrderItem" datasource="#application.sitedatasource#">			
			Update OrderItem 
			SET 	active=0,
					lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			where SKU=(select SKu from productInventory where productid =  <cf_queryparam value="#skuid#" CFSQLTYPE="CF_SQL_INTEGER" > )
			and	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
		</CFIF>	
		
	</cfif>


</cfloop>

<!--- work out value of order--->
<!--- I have ended  up with some redundancy of totals so not sure which way is best  --->
			<CFQUERY NAME="getordervalue" datasource="#application.sitedatasource#">			
			select 
				sum(quantity * unitdiscountprice) as totalvalue,
				sum(quantity * unitlistprice) as totallistvalue,
				sum(totaldiscountprice) as totalvalue1,
				sum(totallistprice) as totallistvalue1
			from orderitem as i
			where	i.orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and		i.active=1
			</CFQUERY>
			
<!--- 			<CFOUTPUT>#getordervalue.totalprice#<BR>
			#getordervalue.totallistprice#<BR>
			#getordervalue.totalprice1#<BR>
			#getordervalue.totallistprice1#<BR>
			</cfoutput>
 --->

<!---  set value and last updated on order header --->
			<CFQUERY NAME="setOrderHeader" datasource="#application.sitedatasource#">			
			Update 	Orders
			SET		orderlistvalue =  <cf_queryparam value="#getordervalue.totallistvalue#" CFSQLTYPE="CF_SQL_money" > ,
					ordervalue =  <cf_queryparam value="#getordervalue.totalvalue#" CFSQLTYPE="cf_sql_numeric" >,
					lastupdatedby =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			Where	orderid =  <cf_queryparam value="#currentorderid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				
			</CFQUERY>
</cftransaction>

<CFLOCATION URL="Report2.cfm?btnViewStatus=3"addToken="false">>