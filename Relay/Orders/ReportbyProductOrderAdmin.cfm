<!--- �Relayware. All Rights Reserved 2014 --->


	<cf_head>
		<cf_title>Products Ordered|By Date</cf_title>
	</cf_head>
	
	

<cfinclude template="/relay/Orders/OrdersTopHead.cfm">
<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="Date">


<cfquery name="getProductsByOrder" datasource="#application.siteDataSource#">
		select per.firstname + ' ' + per.lastname as Name, org.organisationname as Organisation,
o.orderid as Order_ID, oi.quantity as Quantity, p.sku as Sku, p.description as Description, 
'month'= Case
	when MONTH(o.orderdate) = 1 then 'Jan'
	when MONTH(o.orderdate) = 2 then 'Feb'
	when MONTH(o.orderdate) = 3 then 'Mar'
	when MONTH(o.orderdate) = 4 then 'Apr'
	when MONTH(o.orderdate) = 5 then 'May'
	when MONTH(o.orderdate) = 6 then 'Jun'
	when MONTH(o.orderdate) = 7 then 'Jul'
	when MONTH(o.orderdate) = 8 then 'Aug'
	when MONTH(o.orderdate) = 9 then 'Sept'
	when MONTH(o.orderdate) = 10 then 'Oct'
	when MONTH(o.orderdate) = 11 then 'Nov'
	when MONTH(o.orderdate) = 12 then 'Dec'
	End,
'Year' = YEAR(o.orderdate), o.orderdate as Date
from orders o
join orderitem oi on o.orderid = oi.orderid
join product p on p.productid = oi.productid
join person per on per.personid = o.personid
join organisation org on org.organisationid = per.organisationid
where 1=1
		<!--- text search parameters --->
			<cfparam name="form.frmSearchText" type="string" default="">
			<cfparam name="form.frmSearchColumn" type="string" default="">
			<cfparam name="form.frmSearchComparitor" type="string" default="">
			<cfparam name="form.frmSearchColumnDataType" type="string" default="">
			<cfif form["frmSearchText"] neq "">
				<cfswitch expression="#form["frmSearchComparitor"]#">
					<cfcase value="2"> <!--- equals --->
						<cfif form["frmSearchColumnDataType"] eq 1>
							AND #form["frmSearchColumn"]# = #iif( IsNumeric( form["frmSearchText"] ), 'form["frmSearchText"]', 0 )#
						<cfelse>
							AND LOWER( CAST( #form["frmSearchColumn"]# AS VARCHAR(15) ) ) =  <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfif>
					</cfcase>
					<cfcase value="3"> <!--- like --->
						<cfif form["frmSearchColumnDataType"] eq 1>
							AND CAST( #form["frmSearchColumn"]# AS VARCHAR(15) ) LIKE '%' + '#Replace( form["frmSearchText"], "%", "", "all" )#' + '%'
						<cfelse>
							AND CAST( #form["frmSearchColumn"]# AS VARCHAR(255) ) LIKE '%' + '#Replace( form["frmSearchText"], "%", "", "all" )#' + '%'
						</cfif>
					</cfcase>
					<cfcase value="4"> <!--- greater --->
						<cfif form["frmSearchColumnDataType"] eq 1>
							AND #form["frmSearchColumn"]# > #iif( IsNumeric( form["frmSearchText"] ), 'form["frmSearchText"]', 0 )#
						<cfelse>
							AND LOWER( CAST( #form["frmSearchColumn"]# AS VARCHAR(15) ) ) >  <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfif>
					</cfcase>
					<cfcase value="5"> <!--- less --->
						<cfif form["frmSearchColumnDataType"] eq 1>
							AND #form["frmSearchColumn"]# < #iif( IsNumeric( form["frmSearchText"] ), 'form["frmSearchText"]', "2^31" )#
						<cfelse>
							AND LOWER( CAST( #form["frmSearchColumn"]# AS VARCHAR(15) ) ) <  <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfif>
					</cfcase>
					<cfdefaultcase> <!--- none --->
						
					</cfdefaultcase>
				</cfswitch>
			</cfif>
			
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery> 		

	<cfoutput>
	<table width="100%" cellpadding="3">
		<tr>
			<td><strong>Confirmed Orders by Product</strong></td>
		</tr>
		<tr><td>Here you can search for a confirmed order by Order ID.  Select = from the dropdown box, type in the Order ID and click on Go.<br>
		To sort the data click on the column heading.<br>
		</td></tr>
	</table>
	</cfoutput>


	<CF_Translate>
	
	<CF_tableFromQueryObject 
		queryObject="#getProductsByOrder#" 
		numRowsPerPage="400"
		showTheseColumns="Name,Organisation,Order_ID, Quantity, Sku, Description,Month,Year"
		sortOrder="#sortorder#"
		allowColumnSorting="yes"
				
		searchColumnList="o.orderid"
		searchColumnDataTypeList="1"
		searchColumnDisplayList="Order ID"

		>
	
	</CF_Translate>







