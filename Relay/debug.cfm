<!---
Debug.cfm

WAB 2016-01-13

A control panel for controlling debugging settings
Some of these used to be at the top of the errorDetails page


TBD
Could do better if the password is not known - layout a bit kooky
Could automatically store password in setting;  possibly with a condition if it is a dev site which does not share password with other machines accessing database

I feel that it could be expanded to other things, such as control debug logging


WAB 2016-02-24 Ability to switch off translations
WAB 2016-11-04 Ability to decrypt a URL string (current session).  Not a security problem because a) has to be trusted and b) it is not actually a danger to be able to decrypt (being able to encrypt would be).  

--->

<!--- rights to this page are covered by the same rules as for seeing the link to an error.
		i.e. anyone who can see the details of an error is allowed to control debugging settings
 --->
<cfif request.relayCurrentUser.errors.showLink>

	<!--- Deal with Ajax request inline for changing settings --->
	<cfif isDefined("settingName") and isDefined("onOff")>
		<cfset argumentCollection =  {}>
		<cfif isDefined("password")>
			<cfset checkPassword = application.com.cfadmin.checkCFPassword(cfpassword = password)>
			<cfif !checkPassword>
				<cfset result = {'isOK'=false, 'message' = "Incorrect password"}>
				<cfoutput>#serializeJson(result)#</cfoutput>
				<cfexit>
			</cfif>
			<cfset argumentCollection.cfpassword = password>
		</cfif>

		<cfif settingName IS "showdebug">
			<cfset application.com.relaycurrentuser.setShowDebug(debugOn = onOff, argumentcollection = argumentCollection)>
		</cfif>

		<cfif settingName IS "serverdebug">
			<cfset application.com.cfadmin.setDebug(debugOn = onOff, argumentcollection = argumentCollection)>
		</cfif>

		<cfif settingName IS "showErrors">
			<cfset application.com.relayCurrentUser.setShowErrors (showDump = onOff)>
		</cfif>

		<cfif settingName IS "debugQueries">
			<cfset application.com.relayCurrentUser.setqueryParamDebug (QueryParamDebug = onOff)>
		</cfif>

		<cfif settingName IS "showWhitespace">
			<cfset application.com.relayCurrentUser.setShowLeadingWhiteSpace (ShowLeadingWhiteSpace = onOff)>
		</cfif>

		<cfif settingName IS "doNotTranslate">
			<cfset application.com.relayCurrentUser.updateContentSettings(doNotTranslate = onOff)>
		</cfif>

		<!--- 2016-10-24 PPB CF_DUMP this could be extended to handle other/all future TEXT settings --->
		<cfif ListFindNoCase("cf_dump_output,cf_dump_filenameInfix,cf_dump_restrictedRefs,cf_dump_email",settingName) AND StructKeyExists(url,"settingValue")>
			<cfset application.com.relayCurrentUser.setDebugSessionSetting(settingName=settingName, settingValue = url.settingValue)>
		</cfif>

		<cfset result = {'isOK' = true, settingName = settingName, settingValue = url.settingValue, onOff = onoff, 'message' = '' }>
		<cfoutput>#serializeJson(result)#</cfoutput>
		<cfexit>
	</cfif>

	<cfif isDefined("Action")>
		<cfset result = {'isOK' = true}>
		<cfif action is "dumpURL">
			<cfset structDelete (url,"action")>	
			<cfset structDelete (url,"returnFormat")>	
			<cfset structDelete (url,"_cf_noDebug")>				
			<cfset structDelete (url,"_rwRandom")>				
			<cfset structDelete (url,"_rwSessionToken")>				
			<cfsavecontent variable="result.dumpURL">
				<cfdump var="#url#">
			</cfsavecontent>
		</cfif>
		<cfoutput>#serializeJson(result)#</cfoutput>
		<cfexit>

	</cfif>

	<cfset checkPassword = application.com.cfadmin.checkCFPassword()>

	<cfif checkPassword>
		<cfset showdebug = application.com.relaycurrentuser.getShowDebug(checkServerdebug = true)>
		<cfset serverdebug = application.com.cfadmin.getDebug(checkIPaddress = false)>
		<cfset serverdebugthisIPAddress = application.com.cfadmin.getDebug(checkIPaddress = true)>

	<cfelse>

		<cfset showdebug = application.com.globalFunctions.isDebugOn()>
		<cfset serverdebug = showdebug>
		<cfset serverdebugthisIPAddress = showdebug>

	</cfif>

	<cfset showErrors = request.relayCurrentUser.errors.showDump>
	<cfset debugQueries = request.relayCurrentUser.errors.queryParamdebug>
	<cfset showWhitespace = request.relayCurrentUser.errors.showLeadingWhiteSpace>
	<cfset doNotTranslate = request.relayCurrentUser.content.doNotTranslate>

	<cfset cf_dump_output = StructKeyExists(request.relayCurrentUser.errors,"cf_dump_output") ? trim(request.relayCurrentUser.errors.cf_dump_output) : "">
	<cfset cf_dump_filenameInfix = StructKeyExists(request.relayCurrentUser.errors,"cf_dump_filenameInfix") ? trim(request.relayCurrentUser.errors.cf_dump_filenameInfix) : "">
	<cfset cf_dump_restrictedRefs = StructKeyExists(request.relayCurrentUser.errors,"cf_dump_restrictedRefs") ? trim(request.relayCurrentUser.errors.cf_dump_restrictedRefs) : "">
	<cfset cf_dump_email = StructKeyExists(request.relayCurrentUser.errors,"cf_dump_email") ? trim(request.relayCurrentUser.errors.cf_dump_email) : "">


	<script>
	var passwordKnown = <cfoutput>#checkPassword#</cfoutput>;

	changeSetting = function (obj) {

		var parameters = {
			settingName : obj.name
			,settingValue : obj.value
			,onOff : obj.checked
			,returnFormat:'plain'
			}

		if (jQuery(obj).data('requirespassword') && !passwordKnown) {
			parameters.password  = getAndCheckPassword ();
			if (parameters.password == '') {
				return false
			}
		}


		jQuery.get ('/debug.cfm'
					, parameters
					, function (data) {
						if (!data.isOK) {
							alert (data.message)
						}
					  }
					, 'json');

	}

	getAndCheckPassword = function () {
		var passwordValue = jQuery ('#password').val()
		if (passwordValue == '') {
			alert ('Please Enter A password')
		}
		return passwordValue
	}


	decryptURLString = function (urlString) {
		var parameters=urlString.toQueryParams();
			parameters.action="dumpURL"
			parameters.returnFormat='plain'


			jQuery.get ('/debug.cfm'
					, parameters
					, function (data) {
						if (data.isOK) {
							jQuery('#urlDump').html(data.dumpURL)
						}
					  }
					, 'json');
	
		
	}

	</script>

	<cfoutput>
		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="showdebug" >Your Debug</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="showdebug" value = "1" checked="#showdebug#"class="form-control toggle-slider" onchange="changeSetting(this)" data-requirespassword="1" >
			</div>
		</div>

		<cfif !checkPassword>
			<div class="form-group">
				<div class="col-xs-6 col-sm-6 col-md-6">
				   	<label for="password" >Please enter the Cold Fusion Administrator Password to change Debug</label>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6">
					<cf_input type="password" name="password" id = "password" value = "" class="form-control"   >
				</div>
			</div>
		</cfif>
		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="serverdebug" >Server Debug</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="serverdebug" value = "1" checked="#serverdebug#" class="form-control toggle-slider" onchange="changeSetting(this)" data-requiresPassword >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="showErrors" >Show Errors InLine</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="showErrors" value = "1" checked="#showErrors#" class="form-control toggle-slider" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="debugQueries" >Debug Queries</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="debugQueries" value = "1" checked="#debugQueries#" class="form-control toggle-slider" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="debugQueries">Leave Whitespace (Leading Tabs)</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="showWhitespace" value = "1" checked="#showWhitespace#" class="form-control toggle-slider" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="debugQueries">Do Not Translate</label>
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<cf_input type="checkbox" name="doNotTranslate" value = "1" checked="#doNotTranslate#" class="form-control toggle-slider" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">
		   	<label>CF_DUMP settings:</label><br />
		</div>

		<div class="form-group">
			<div class="col-xs-6 col-sm-12 col-md-6">
			   	<label for="cf_dump_output" >Enter "browser" to see the dump on screen OR a custom filespec</label>
			</div>
			<div class="col-xs-6 col-sm-12 col-md-6">
				<cf_input  name="cf_dump_output" id = "cf_dump_output" value = "#cf_dump_output#" class="form-control" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-6 col-sm-12 col-md-6">
			   	<label for="cf_dump_filenameInfix" >Enter a few chars to uniquely identify your debug dump file (eg. your initials)</label>
			</div>
			<div class="col-xs-6 col-sm-12 col-md-6">
				<cf_input  name="cf_dump_filenameInfix" id = "cf_dump_filenameInfix" value = "#cf_dump_filenameInfix#" class="form-control" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-6 col-sm-12 col-md-6">
			   	<label for="cf_dump_restrictedRefs" >Enter the ref(s) of any restricted dumps you wish to see</label>
			</div>
			<div class="col-xs-6 col-sm-12 col-md-6">
				<cf_input  name="cf_dump_restrictedRefs" id = "cf_dump_restrictedRefs" value = "#cf_dump_restrictedRefs#" class="form-control" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-6 col-sm-12 col-md-6">
			   	<label for="cf_dump_email" >Enter your email address if you wish to have the dump result sent to you by email</label>
			</div>
			<div class="col-xs-6 col-sm-12 col-md-6">
				<cf_input  name="cf_dump_email" id = "cf_dump_email" value = "#cf_dump_email#" class="form-control" onchange="changeSetting(this)"  >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-6 col-sm-12 col-md-6">
			   	<label for="cf_dump_filepathnote" >CF_DUMP files for this site are generated in (default)</label>
			</div>
			<div class="col-xs-6 col-sm-12 col-md-6">
				<cf_input  name="cf_dump_filepathnote" value = "#application.paths.temp#" class="form-control" disabled="disabled" >
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="URLString" >Decrypt a URL String or form parameters</label><br />
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<input name="URLString" value="" onchange="decryptURLString(this.value)">
				<div id="urlDump"></div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-9 col-sm-9 col-md-9">
			   	<label for="reloadCFCs" >Reload Changed CFCs  TBD</label><br />
			</div>
			<div class="col-xs-3 col-sm-3 col-md-3">
				<input name="reloadCFCs" value="" onClick="doReload(this.value)">
			</div>
		</div>


		<div class="form-group">
			<div class="col-xs-12 col-sm-12 col-md-12">
			   	<label for="errorDetails" >Latest Errors:</label><br />
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<A href="/errorhandler/errordetails.cfm" target="errorWindow">Globally</a><br />
				<A href="/errorhandler/errordetails.cfm?visitID=#request.relaycurrentuser.visitid#" target="errorWindow">This session</a>
			</div>
		</div>

	</cfoutput>

</cfif>
