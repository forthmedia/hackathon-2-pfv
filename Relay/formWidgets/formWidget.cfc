/**
 * A base class that all widget controllers should extend
 *
 * A form widget should/can be annotated with the following
 * widgetNamePhrase - as a translation phrase that will be used to represent the widget in the UI
 * entityTypeRestriction - a comma seperated list of entityTypeIDs that the widget can be used with. If ommitted can be used by any entity.
 *
 * @author Richard.Tingle
 * @date 25/08/16
 **/
component accessors=true output=true persistent=false {


	public any function init(required string jsTemplate){
		this.jsTemplate=jsTemplate;
		return this;
	}

	public void function onSave(required numeric entityTypeID, required numeric entityID, required struct widgetFormData) hint="called automatically AFTER the main entity has been saved. Returns a struct that will be available to the widget view" output="false" {

	}

	/*
	* On the portal this returning true will move the template from being a line item to being in the
	* button bar, the output template should be one or more buttons really

	public boolean function shouldDisplayInButtonBar(){
		return false;
	}*/

	public string function getLineHTML(){
		return "";
	}

	public struct function getButtonAttributes(){
		return {};
	}

	public void function onLoad(required any entity, required boolean readonly) hint="called during a load of an entity, should return a struct that will be availble to the widget view" output="false" {
		this.entity=arguments.entity;
		this.readonly=arguments.readonly;
		if (this.jsTemplate != '') {
			application.com.request.includeJavascriptOnce(template=this.jsTemplate,translate=true);
		}
	}

	/*public String function getTemplate(){
		return this.jsTemplate;
	}*/
}