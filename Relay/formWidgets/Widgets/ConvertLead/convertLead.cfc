
component accessors=true output=false persistent=false extends="formWidgets.formWidget" entityTypes="lead" {


	public struct function getButtonAttributes(){
		if (this.entity.canBeConverted()) {
			return {id="portalConvertLink", value="phr_Convert"};
		}
		return {};
	}
}