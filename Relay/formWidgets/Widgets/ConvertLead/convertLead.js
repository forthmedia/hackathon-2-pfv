var $_convertLeadButton = '';
var $_leadForm = '';
var $_leadButtonBar = '';

jQuery(document).ready(function() {
	
	var converting = false;

	jQuery('#portalConvertLink').click(function(){
		if(converting == false && !getConvertLeadButton().hasClass("disabled")){
			var form = getLeadForm();
			var formId = form.attr('id');
			$('portalConvertLink').spinner({position:'center'});
			setButtonBarDisabled(true);

			if (form.serialize() == form.data('serialize')){
				convertPortalOpportunity(formId);
			} else {
				/*if (form.get(0).checkValidity()){*/
					submitWithoutRefresh(formId);
					convertPortalOpportunity(formId);
				/*} else {
					//clicking the save button will fail, but will trigger any user messages etc
					jQuery('#saveButton')[0].click();
				}*/
			}
		}
	});
});

function getConvertLeadButton() {
	if ($_convertLeadButton == '') {
		$_convertLeadButton = jQuery('#portalConvertLink');
	}
	return $_convertLeadButton;
}

function getLeadForm() {
	if ($_leadForm == '') {
		$_leadForm = getConvertLeadButton().closest('form');
	}
	return $_leadForm;
}

function getButtonBar() {
	if ($_leadButtonBar == '') {
		$_leadButtonBar = jQuery('.actionContainer');
	}
	return $_leadButtonBar;
}

function setButtonBarDisabled(disabled) {
	getButtonBar().find(":button,:submit").prop('disabled',disabled)
}
		
function convertPortalOpportunity()  {

	if (jQuery('#convertDataForm').length == 0) {
		getLeadForm().append('<div id="convertDataForm"></div>');
	}
	var $_convertDataForm = jQuery('#convertDataForm');
	$_convertDataForm.html("");

	$_convertDataForm.load('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=DisplayConvertLeadDataForm&returnFormat=plain',
		function() {
			var successUIAction=function(message, opportunityViewLink){
				onSuccessfulConvert(opportunityViewLink);//the creation of user messages happens in the webservice, theres no need to process one here
			}
			var failureUIAction=function(errorThrown){
				$('portalConvertLink').removeSpinner();
				getLeadForm().prepend("<div class='errorblock'>" + errorThrown + "</div>");
			}
		
			var onCloseUIAction=function(){if (typeof savingData=='undefined'){
				$('portalConvertLink').removeSpinner();
				setButtonBarDisabled(false);
			}
		}
		openConvertDataForm(successUIAction,failureUIAction,onCloseUIAction);
	});
}


function onSuccessfulConvert(opportunityViewToken){
	if (!isInternal) {
		var returnUrl = typeof elementId != undefined?'/?eid='+elementID+'&opportunityViewToken=' + opportunityViewToken:location.href;
		location.href=returnUrl;
	} else {
		location.href='/lead/leads.cfm?leadId='+jQuery("#leadid").val();
	}
}


function openConvertDataForm(successUIAction,failureUIAction,onCloseUIAction) {

	var convertDataForm = jQuery("#convertDataForm");

	jQuery.fancybox({
		helpers: {
			overlay: {
				locked: true
			}
		},
		autoSize: false,
		width: '60%',
		height: '50%',
		closeEffect : 'none',
		closeSpeed: 'fast',
		type: 'inline',
		href: convertDataForm,
		beforeClose: function(){onCloseUIAction();}
	});
	
	jQuery("#detail").val("Opportunity for " + jQuery("#lead_company").val());

	//add the save button behaviour to the save button
	jQuery('#convertDataForm #frmSubmit').click(function(){saveConvertDataFormAction(this.form,successUIAction,failureUIAction)});
}

function submitWithoutRefresh(formID){
	jQuery.post(window.location.toString(), jQuery('#'+formID).serialize())
}

function saveConvertDataForm(successUIAction, failureUIAction) {

	var dataObj = {};
	dataObj.Names = jQuery("#Names").val();
	dataObj.detail = jQuery("#detail").val();
	dataObj.expectedCloseDate = jQuery("#expectedCloseDate").val();
	dataObj.OppCustomerTypeID = jQuery("#OppCustomerTypeID").val();
	dataObj.stageID = jQuery("#stageID").val();
	dataObj.budget = jQuery("#budget").val();
	dataObj.leadID = jQuery("#leadid").val();

	if (typeof elementID !== 'undefined'){
		dataObj.eid=elementID; //for portal conversions include the eid, we'll need to send this so that the webservice can send back the encrypted link to the created opportunity.
	}
	
	converting = true

	jQuery.ajax(
		{
			type:'post',
			url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=convertLead&returnFormat=json',
			data:dataObj,
			dataType:'json',
			success: function(data,textStatus,jqXHR) {
	   			converting = false;
	   			successUIAction(data.MESSAGE, data.opportunityViewToken);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				converting = false; //2015-10-07	ACPK	PROD2015-106 Fixed bug preventing user from reattempting conversion if previous attempt failed
				failureUIAction(errorThrown);
			}
	});
}


function saveConvertDataFormAction(form, successUIAction, failureUIAction) {

	var validationResult = relayFormValidation(form);

	if (validationResult) {
		savingData = true;
		jQuery.fancybox.close();
		saveConvertDataForm(successUIAction, failureUIAction);
	}
}