/**
 * This is an interface which defines the behaviour of CFCs (which will be
 * within the custom "Code" part of the application). These are called after
 * an API action such as update, delete etc
 *
 * @author Richard.Tingle
 * @date 17/02/15
 **/
interface {
	void function onSuccessfulUpdate(numeric entityID, struct entityDetails);
	void function onFailedUpdate(numeric entityID, struct entityDetails);
	void function onSuccessfulInsert(numeric entityID, struct entityDetails);
	void function onFailedInsert(numeric entityID, struct entityDetails);
	void function onSuccessfulDelete(numeric entityID);
	void function onFailedDelete(numeric entityID);
}