/**
 * An implementation of this class should be passed (if required) to the RelayEntity updateEntityDetails methods.
 * This objects methods are called after all rights checks so can give helpful error messages
 *
 * @author Richard.Tingle
 * @date 30/01/15
 **/

interface {
	array function getRequiredFields();
	boolean function postRightsCheckFunction(required struct entityToCheck);
	string function getFailureMessage();
	string function getFailureError();
}
