<!---
	�Relayware. All Rights Reserved 2014
--->

<cfcomponent displayname="APICustomAbstract" >

	<cffunction name="Init">
		<cfreturn this/>
	</cffunction>

	<cffunction name="get" access="public" output="false">
		<cfset apiResponse = structNew()/>
		<cfset apiResponse.data = "On development"/>
		<cfset apiResponse.statusCode = 403/>
	  	<cfreturn apiResponse/>
	</cffunction>

	<cffunction name="put" access="public" output="false">
		<cfset apiResponse = structNew()/>
		<cfset apiResponse.data = "On development"/>
		<cfset apiResponse.statusCode = 403/>
	  	<cfreturn apiResponse/>
	</cffunction>

	<cffunction name="post" access="public" output="false">
		<cfset apiResponse = structNew()/>
		<cfset apiResponse.data = "On development"/>
		<cfset apiResponse.statusCode = 403/>
	  	<cfreturn apiResponse/>
	</cffunction>

	<cffunction name="delete" access="public" output="false">
		<cfset apiResponse = structNew()/>
		<cfset apiResponse.data = "On development"/>
		<cfset apiResponse.statusCode = 403/>
	  	<cfreturn apiResponse/>
	</cffunction>

</cfcomponent>
