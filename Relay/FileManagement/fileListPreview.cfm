<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		fileListPreview.cfm
Author:			NJH
Date started:	23-01-2013

Description:	Page to show files in preview mode using embedly

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/07/27			NJH			Ticket 445370 - move files to secure location before access to that embedly can do it's thing
2015/07/28			NJH			Add secure option for embedly
Possible enhancements:


 --->

<cfset args = {fileTypeGroupID = fileTypeGroupID,personid = request.relaycurrentuser.personid,returnCountryIDList = true,checkFileExists = true,languageid = IIF(filterByLanguage,de(request.relayCurrentUser.languageid),de("")),orderBy=fileSortOrder}>
<cfif fileTypeID is not "">
	<cfset args.fileTypeID = fileTypeID>
</cfif>
<cfset origFiles = application.com.filemanager.getFilesAPersonHasCountryViewRightsTo(argumentCollection = args)>

<cfset args ={originalFileQuery = origFiles}>
<cfif fileSortOrder neq "">
	<cfset args.overrideSortOrder = fileSortOrder>
</cfif>
<cfset files = application.com.filemanager.getMostSuitedFileFromFamily(argumentCollection = args)>

<cfset embedlyAPIKey = application.com.settings.getSetting("plugins.embedly.key")>

<cf_head>
	<cfoutput>
	<script>
		jQuery('document').ready(function(){
			jQuery('##embedContent ##fileListPreviewTable a.embedlypreview').embedly({
				maxWidth: jQuery('.embedlypreview').width(),
				method: '#EmbedlyDisplayMethod#',
				key: '#embedlyAPIKey#',
				secure: #request.currentSite.isSecure?true:false#
			});
		});
	</script>

	<cfif fileExists(application.paths.code&"\styles\files.css")>
		<style type="text/css" media="screen,print">@import url("/code/styles/files.css");</style>
	</cfif>
	</cfoutput>
</cf_head>

<cfoutput>
	<table id="fileListPreviewTable">
		<cfloop query="files">
			<tr>
				<cfset theFileURL = fileUrl>
				<!--- NJH 2015/07/27 - 445370 to deal with secure files when using embedly. Move the files to the secure location and give embedly the path to the new location. Shouldn't be any security issues here --->
				<!--- NJH 2015/07/31 - removed this for now, as customer who was wanting this didn't actual want this!!! But it may come up again.
				<cfif secure>
					<cfset securedFileDetails = application.com.fileManager.getSecuredFile(fileID=fileID)>
					<cfif securedFileDetails.isOK>
						<cfset theFileURL = securedFileDetails.relativeURL>
					</cfif>
				</cfif> --->
				<td class="fileListPreviewLinkTD"><div><a href="#htmlEditFormat(theFileURL)#" class="noembedly embedlypreview" target="_blank"><cfif name neq "">#htmlEditFormat(name)#<cfelse>#htmlEditFormat(filename)#</cfif></a> <div class="desc">#htmlEditFormat(description)#</div></div></td>
			</tr>
		</cfloop>
	</table>
</cfoutput>