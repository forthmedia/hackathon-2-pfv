<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			filetophead.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/05/13			SSS			added back button for fileDownloadHistory
2016-02-24		WAB 	PROD2015-291  Media Library Visibility.  Alter queries to use new functions for rights
Possible enhancements:


 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="ActionList.cfm">
<CFPARAM NAME="moduleTitle" DEFAULT="File Management">
<CFPARAM NAME="pageTitle" DEFAULT="File Management">

<cfset canAdd = true>
<cfset urlParams = "">
<cfif structKeyExists(url,"fileTypeGroupId")>
	<cfset urlParams = "&fileTypeGroupID=#url.fileTypeGroupId#">
</cfif>

<CF_RelayNavMenu moduleTitle="#moduleTitle#" thisDir="fileManagement" pageTitle="#pageTitle#">
	<CFIF listFindNoCase("fileSearch.cfm,files.cfm,fileGroupFileListing.cfm",listLast(SCRIPT_NAME,"/")) neq 0>
		
		<cfif structKeyExists(url,"fileTypeGroupId") and isNumeric(url.fileTypeGroupId) and url.fileTypeGroupId neq 0>
			<!--- NJH 2013/09/11 we now have rights on file groups.. so, only show an add button if the user has rights to edit the group (ie. add files,etc) --->
			<cfquery name="doesUserHaveEditRights" datasource="#application.siteDataSource#">
				declare @personid int = #request.relayCurrentUser.personID#
				select 1
				from FileTypeGroup ftg
						left join 
					dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileTypeGroup").entityTypeId#,@personid) as rftg on rftg.recordid = ftg.fileTypeGroupid
				where 
					fileTypeGroupID = <cf_queryparam value="#url.fileTypeGroupId#" cfsqltype="cf_sql_integer">
					and
					(ftg.hasRecordRightsBitMask & 2 = 0 OR rftg.level2 <> 0)					
			</cfquery>
			
			<cfset canAdd = doesUserHaveEditRights.recordCount>
		</cfif>
		
		<cfif canAdd>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='files.cfm?editor=yes&add=yes&hideBackButton=false&fileID=0'&urlParams)#">
		</cfif>
	</CFIF>
	
	<CFIF findNoCase("fileGroupAndType.cfm",SCRIPT_NAME) neq 0>
		<cfif isDefined("fileTypeGroupId")>
			<CF_RelayNavMenuItem MenuItemText="Resources" CFTemplate="files.cfm?#urlParams#">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='fileGroupAndType.cfm?editor=yes&add=yes&hideBackButton=false')#">
	</CFIF>
	
	<CFIF listFindNoCase("fileDownloadHistory.cfm,editFileContents.cfm",listLast(SCRIPT_NAME,"/"))>
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="javaScript:history.back()">
	</cfif>
	
</CF_RelayNavMenu>
