<!--- �Relayware. All Rights Reserved 2014 

2016-02-24		WAB 	PROD2015-291  Visibility Project.  Removed filter on available FileTypeGroups - does not make sense - see comments
--->

<cf_title>Resource Search</cf_title>
<cfset application.com.request.setTopHead(pageTitle="Media Resources",topHeadcfm="fileTopHead.cfm")>

<cfparam name="fileTypeGroupID" default="">
<cfparam name="languageID" default="">
<cfparam name="countryID" default="">
<cfparam name="userGroupID" default="">
<cfparam name="uploadedBy" default="0">
<cfparam name="uploadedDateFrom" default="">
<cfparam name="searchString" default="">
<cfparam name="showAdvancedSearch" type="boolean" default="true">

<cfif structKeyExists(url,"search")>
	<cfset fileSearchArgs = structNew()>
	<cfset fileSearchArgs.personID=request.relaycurrentuser.personID>
	<!--- NJH Case 437127 2013/11/01 --->
	<cfif structKeyExists(url,"searchString")>
		<cfset fileSearchArgs.searchString = url.searchString>
	</cfif>

	<cfif structKeyExists(url,"fileTypeGroupID") and url.fileTypeGroupID neq "">
		<cfset fileSearchArgs.fileTypeGroupID = url.fileTypeGroupID>
	</cfif>
	<cfif structKeyExists(url,"languageID") and url.languageID neq "">
		<cfset fileSearchArgs.languageID = url.languageID>
	</cfif>
	<cfif structKeyExists(url,"countryID") and url.countryID neq "">
		<cfset fileSearchArgs.countryIDList = url.countryID>
	</cfif>
	<cfif structKeyExists(url,"userGroupID") and url.userGroupID neq "">
		<cfset fileSearchArgs.userGroupID = url.userGroupID>
	</cfif>
	<cfif structKeyExists(url,"uploadedBy") and url.uploadedBy eq 1>
		<cfset fileSearchArgs.uploadedBy = request.relayCurrentUser.personID>
	</cfif>
	<cfif structKeyExists(url,"uploadedDateFrom") and url.uploadedDateFrom neq "">
		<cfset fileSearchArgs.uploadedFromDate = url.uploadedDateFrom>
	</cfif>
	<cfset fileSearchArgs.uploadedToDate = request.requestTime>
</cfif>


<cfif showAdvancedSearch>
	<cfquery name="GetFileGroupTypes" dataSource="#application.SiteDataSource#">
		select heading, FileTypeGroupID, intro
		from FileTypeGroup ftg
		<!--- 
		2016-02-24		WAB 	PROD2015-291  Visibility Project
		I have removed this filter (but I have updated it to use new functions just in case it needs reinstating - untested)
		a) Does it make any sense to filter file type groups on the internal site?
		   View Permissions are set for portal users, this should not affect content editors looking at or editing files
		b) Actually there is, as yet, not interface for setting view rights at fileTypeGroup level

			declare @personid int = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >

				left join 
			dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileTypeGroup").entityTypeId#,@personid) as rftg on rftg.recordid = ftg.fileTypeGroupid
		where
			rftg.level1 = 1 or ftg.hasRecordRightsBitMask & 1 = 0
			
		--->	
		order by heading
	</cfquery>

	<cfoutput>
		<cf_querySim>
			getUploadDates
			display,value
			Today|#dateFormat(request.requestTime,"medium")#
			Yesterday|#dateAdd("d",-1,dateFormat(request.requestTime,"medium"))#
			in the last 7 days|#dateAdd("d",-7,dateFormat(request.requestTime,"medium"))#
			in the last 30 days|#dateAdd("d",-30,dateFormat(request.requestTime,"medium"))#
			in the last 90 days|#dateAdd("d",-90,dateFormat(request.requestTime,"medium"))#
		</cf_querySim>
	</cfoutput>

	<cfset usersCountries = application.com.commonQueries.getCountries(restrictToUsersCountries=true)>

	<cfquery name="getUserGroupsForFilter" datasource="#application.siteDataSource#">
		select distinct UserGroupID, Name from (
			select ug.usergroupid, ug.name
			from
			files f with (noLock)
			inner join usergroup ug with (noLock) on f.personid = ug.personid
			where ug.userGroupType != 'Personal'
			union all
			select ug2.usergroupid, ug2.name
			from
			files f with (noLock)
			inner join RightsGroup rg with (noLock) on f.personid = rg.personid
			inner join UserGroup ug2 with (noLock) on rg.UsergroupID = ug2.UserGroupID
			where ug2.userGroupType != 'Personal'
			) AS base
		order by name
	</cfquery>

	<cfset languages = application.com.commonQueries.getLanguages(restrictToUsersCountries=true)>
</cfif>

<cfoutput>

	<style>
		.searchheading {}
		.searchheading span { color:##000; font-weight:bold; padding-top:5px; }
		.searchheading span:hover { color:##7fba00 }
		li.advancedsearch {font-weight:bold; display:none; }
		li {padding-bottom:3px;}
		##advancedSearch_toggle {padding-right:4px;}
		##fileGroup {font-weight:normal;padding-left:3px;}
		li.advancedsearch select {width:100%;}
		.withBorder {width:100%;}
	</style>

	<cfif showAdvancedSearch>
	<script>
		jQuery(document).ready(function() {
			<cfif fileTypeGroupID neq "" or uploadedDateFrom neq "" or countryID neq "" or languageID neq "" or userGroupID neq "">
			jQuery('##advancedSearch_toggle').attr('src','/styles/cachedBGImages/level1_arrow_open.gif');
			jQuery('.advancedsearch').show();
			<cfelse>
			jQuery('##advancedSearch_toggle').attr('src','/styles/cachedBGImages/level1_arrow_closed.gif');
			jQuery('.advancedsearch').hide();
			</cfif>
		});
	</script>
	</cfif>

	<div class="row">
		<div class="col-xs-12 col-md-3 col-lg-2">
				<form>
					<div class="grey-box-top">
						<cf_input type="hidden" name="showAdvancedSearch" value="#showAdvancedSearch#">
							<div class="form-group">
								<label class="searchString">Search For:</label>
								<span title="Search resource IDs or text within resource filenames, urls, names, content, types, keywords, families or media groups.">
									<cf_input type="text" placeholder="Search" value="#searchString#" id="searchString" name="searchString" class="searchBoxAlt form-control" maxlength="50">
								</span>
								<cfif showAdvancedSearch>
									<div class="checkbox">
										<label><cf_input type="checkbox" checked="#uploadedBy#" id="uploadedBy" name="uploadedBy" value=1> Only my resources<label>
									</div>
								</cfif>
							</div>
						<cfif showAdvancedSearch>
						<div>
							<div id="mediaGroupHeading" class="searchheading form-group" onClick="if (jQuery('##advancedSearch_toggle').attr('src') == '/styles/cachedBGImages/level1_arrow_open.gif') jQuery('##advancedSearch_toggle').attr('src','/styles/cachedBGImages/level1_arrow_closed.gif'); else jQuery('##advancedSearch_toggle').attr('src','/styles/cachedBGImages/level1_arrow_open.gif');jQuery('.advancedsearch').toggle();">
								<img src="/styles/cachedBGImages/level1_arrow_open.gif" id="advancedSearch_toggle"/>
								<span>Advanced Search</span>
							</div>
							<div class="advancedsearch form-group">
								<label for="fileTypeGroupID" class="searchLabel">Media Group</label>
								<cfif isDefined("FileTypeHeading")>
									<span id="fileGroup">#FileTypeHeading#</span> <cf_input type="hidden" name="fileTypeGroupID" id="fileTypeGroupID" value="#fileTypeGroupID#">
								<cfelse>
									<cf_select name="fileTypeGroupID" id="fileTypeGroupID" query="#GetFileGroupTypes#" display="Heading" value="fileTypeGroupID" nullText="Any Media Group" selected="#fileTypeGroupID#" showNull="true">
								</cfif>
							</div>
							<div class="advancedsearch form-group">
								<label for="uploadedDateFrom" class="searchLabel">Revision</label>
								<cf_select name="uploadedDateFrom" id="uploadedDateFrom" query="#getUploadDates#" display="display" value="value" nullText="Any Time" showNull="true" selected="#uploadedDateFrom#">
							</div>
							<div class="advancedsearch form-group">
								<label for="countryID" class="searchLabel">Country</label>
								<cf_select name="countryID" id="countryID" selected="#countryID#" query="#usersCountries#" display="country" value="countryID" nullText="Any Country" showNull="true">
							</div>
							<div class="advancedsearch form-group">
								<label for="languageID" class="searchLabel">Language</label>
								<cf_select name="languageID" id="languageID" selected="#languageID#" query="#languages#" display="language" value="languageID" nullText="Any Language" showNull="true">
							</div>
							<div class="advancedsearch form-group">
								<label for="userGroupID" class="searchLabel">Viewable By</label>
								<cf_select name="userGroupID" id="userGroupID" selected="#userGroupID#" query="#GetUserGroupsForFilter#" display="Name" value="UserGroupID" nullText="Any Usergroup" showNull="true">
							</div>
						</div>
						<cfelse>
							<cfif isDefined("FileTypeHeading")>
								<cf_input type="hidden" name="FileTypeHeading" value="#FileTypeHeading#">
								<cf_input type="hidden" name="fileEdit" value="#fileEdit#">
							</cfif>
						</cfif>
						<span id="mediaLibrarySearch">
							<cf_input type="submit" value="Search" name="search">
						</span>
					</div>
				</form>
		</div>
		<div class="col-xs-12 col-md-9 col-lg-10">
				<cfif structKeyExists(url,"search")>
					<cf_include template="/fileManagement/files.cfm">
				<cfelse>
					<cfoutput>#application.com.relayUI.message(message="phr_SelectFileFilters",type="info")#</cfoutput>
				</cfif>
		</div>
	</div>
</cfoutput>