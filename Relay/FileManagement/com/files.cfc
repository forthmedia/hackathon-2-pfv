<!---
  --- files
  --- -----
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   22/01/16


WAB 2016-05-24  BF-938 Remove some debug which caused an error (because it referred to the URL structure and there is also a arguments.URL (string) knocking around because it is a field on the files table
  --->
<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="manageFamilyFile" returnType="struct">
		<cfset var result = {isOK=true,message=""}>

		<cftry>
			<cfif not structKeyExists(form,"fileFamilyID")>
				<!--- 2013-04-25	YMA	Case 435852 insert null when file family isnt defined to support foreign keys. --->
				<cfset form.fileFamilyID = ''>
			</cfif>

			<cfif not isNumeric(form.fileFamilyId) and form.fileFamilyID neq "">
				<cfquery name="insertFileFamily" datasource="#application.siteDataSource#">
					if not exists (select 1 from fileFamily where title=<cf_queryparam value="#form.fileFamilyId#" cfsqltype="cf_sql_varchar">)
					insert into fileFamily (title,createdBy,lastUpdatedBy,lastUpdatedByPerson)
					values (<cf_queryparam value="#form.fileFamilyId#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)

					select scope_identity() as fileFamilyId
				</cfquery>

				<cfset form.fileFamilyId = insertFileFamily.fileFamilyId>

			<cfelseif isNumeric(form.fileFamilyId) and form.fileFamilyId neq 0>
				<cfif structKeyExists(form,"editFileFamilyText") and form.editFileFamilyText neq "">
					<cfquery name="updateFileFamily" datasource="#application.siteDataSource#">
						update fileFamily set title=<cf_queryparam value="#form.editFileFamilyText#" cfsqltype="cf_sql_varchar">,
							lastUpdated = #request.relayCurrentUser.userGroupID#,
							lastUpdatedByPerson=#request.relayCurrentUser.personID#
						where fileFamilyId =  <cf_queryparam value="#form.fileFamilyID#" CFSQLTYPE="cf_sql_integer" >
					</cfquery>
				</cfif>
			</cfif>

			<!--- 2013-04-25	YMA	Case 435852 removed default to 0 for filefamilyID --->

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="updateMediaFile" returntype="struct">

		<cfset var result = {isOK=true,message=""}>
		<cfset var getOrigPath = "">
		<cfset var getNewPath = "">
		<cfset var oldFilePath = "">
		<cfset var newFilePath = "">
		<cfset var file = structNew()>
		<cfset var newFileName = "">
		<cfset var newFileNameAndPath = "">
		<cfset var oldFileNameAndPath = "">
		<cfset var zipFile = structNew()>
		<cfset var client_file = "">
		<cfset var updateFileDetails = "">
		<cfset var count = 1>
		<cfset var newFile = false> <!--- whether the file record we are dealing with is a brand new one or not --->
		<cfset var thumbnail = ""> <!--- 2016-10-07 YMA Auto Generate thumbnail icon's for JPEG,JPG,GIF,TIFF,PNG,BMP,PDF files --->

		<cfif form.fileTypeID_orig eq "" or form.fileTypeID_orig eq 0>
			<cfset newFile = true>
		</cfif>

		<cfif not newFile>
			<cfquery name="getOrigPath" datasource="#application.SiteDataSource#">
				select path,secure,autounzip from fileType
				where fileTypeID =  <cf_queryparam value="#form.fileTypeID_orig#" cfsqltype="CF_SQL_INTEGER" >
			</cfquery>

			<cfif getOrigPath.secure>
				<cfset oldFilePath = "#application.paths.secureContent#\#getOrigPath.path#\">
			<cfelse>
				<cfset oldFilePath = "#application.paths.content#\#getOrigPath.Path#\">
			</cfif>
			<cfset newFilePath = oldFilePath>
		</cfif>

		<cfif form.fileTypeID_orig neq arguments.fileTypeID>
			<cfquery name="getNewPath" datasource="#application.SiteDataSource#">
				select secure, path,autounzip from fileType
				where fileTypeID =  <cf_queryparam value="#arguments.fileTypeID#" cfsqltype="CF_SQL_INTEGER" >
			</cfquery>

			<cfif not getNewPath.secure>
				<cfset newFilePath = "#application.paths.content#\#getNewPath.Path#\">
			<cfelse>
				<cfset newFilePath = "#application.paths.secureContent#\#getNewPath.Path#\">
			</cfif>
		</cfif>

		<cfif structKeyExists(form,"mediaFile") and trim(form.mediaFile) neq "">

			<cfset file=application.com.fileManager.uploadFile(fileField="mediaFile", destination=newFilePath)>

			<cfif file.isOK>
				<cfset newFileName = file.serverFile>
				<!--- START DXC Case #440724 --->
				<cfif findnocase("+",newFileName) gt 0>
					<cfset newFileName=replace(newFileName,"+","plus","ALL")>
				</cfif>
				<!--- END DXC Case #440724 --->
				<cfset newFileNameAndPath = newFilePath & newFileName>

				<cfif not newFile>
		 			<cfset oldFileNameAndPath = oldFilePath & arguments.filename>

		 			<!--- if the old file is a zip file, delete the subdirectory --->
					<!--- START: 2014-07-24 NYB Case 439373 - remove until Vice Versa is removed:
					<cfif getOrigPath.autounzip and right(trim(arguments.filename),4) eq ".zip">
						<cfif directoryExists("#newFilePath#\#arguments.fileID#")>
							<!--- START: 2014-04-30 NYB Case 439373 - added if's around delete --->
							<cfif structkeyexists(form,"REPLACE") and LCase(form.REPLACE) eq "replace">
							<cfdirectory action="delete" directory="#newFilePath##arguments.fileID#" recurse="true">
									</cfif>
							<!--- END: 2014-04-30 NYB Case 439373 --->
						</cfif>
					</cfif>
					END: 2014-07-24 NYB Case 439373 --->
				</cfif>

				<!--- START: 2014-07-24 NYB Case 439373 - vice versa workaround --->
				<cfset suffix = "">
				<cfif not newFile and getOrigPath.autounzip and right(trim(arguments.filename),4) eq ".zip" and directoryExists("#newFilePath#\#arguments.fileID#") and structkeyexists(form,"REPLACE") and LCase(form.REPLACE) eq "replace">
					<cfset suffix = CreateUUID()>
				</cfif>
				<!--- END: 2014-07-24 NYB Case 439373 - vice versa workaround --->

				<!--- if the new file is a zip file, extract the files into a subdirectory --->
				<cfif ((not isDefined("getNewPath.autounzip") and getOrigPath.autounzip) or (isDefined("getNewPath.autounzip") and getNewPath.autounzip)) and right(trim(newFileName),4) eq ".zip">
					<cfset zipFile=application.com.fileManager.unzipFile(fileAndPath=newFileNameAndPath,destination="#newFilePath##arguments.fileID##suffix#",overwrite="true",zipfileID = arguments.fileID)>

					<!--- START: 2014-07-24 NYB Case 439373 - vice versa workaround --->
					<cfif not newFile and getOrigPath.autounzip and right(trim(arguments.filename),4) eq ".zip" and directoryExists("#newFilePath#\#arguments.fileID#") and structkeyexists(form,"REPLACE") and LCase(form.REPLACE) eq "replace">
						<cfset origPath = "#newFilePath##arguments.fileID#">
						<cfdirectory directory = "#origPath#" action = "list" listInfo="all" name="oldDirsQry" recurse="yes" sort="directory desc" type="dir">
						<cfdirectory directory = "#origPath##suffix#" action = "list" listInfo="all" name="newDirsQry" recurse="yes" sort="directory desc" type="dir">

						<cfset QueryAddRow(oldDirsQry)>
						<cfset QuerySetCell(oldDirsQry, "DIRECTORY", "#origPath#",oldDirsQry.recordcount)>

						<cfset QueryAddRow(newDirsQry)>
						<cfset QuerySetCell(newDirsQry, "DIRECTORY", "#origPath##suffix#",newDirsQry.recordcount)>

						<cfloop query="oldDirsQry">
							<cfset subPathVar = replace(replace(oldDirsQry.Directory,origPath,""),origPath&"\","")&iif(oldDirsQry.Name neq "",de("\"),"")&oldDirsQry.Name>
							<cfset newPathVar = origPath&suffix&subPathVar>
							<cfset origPathVar = origPath&subPathVar>

							<cfif not DirectoryExists(newPathVar)>
								<cfdirectory directory = "#origPathVar#" action = "delete" recurse = "yes">
							<cfelse>
								<cfdirectory directory = "#newPathVar#" action = "list" listInfo="all" name="newFilesQry" sort="directory asc" type="file">
								<cfdirectory directory = "#origPathVar#" action = "list" listInfo="all" name="origFilesQry" sort="directory asc" type="file">

								<cfloop query="origFilesQry">
									<cfset subFilePathVar = replace(replace(origFilesQry.Directory,origPath,""),origPath&"\","")&"\"&origFilesQry.Name>
									<cfset newFilePathVar = origPath&suffix&subFilePathVar>
									<cfset origFilePathVar = origPath&subFilePathVar>

									<cfif not FileExists(newFilePathVar)>
										<cffile action = "delete" file = "#origFilePathVar#">
									</cfif>
								</cfloop>
		 					</cfif>
						</cfloop>

						<cfset zipFile=application.com.fileManager.unzipFile(fileAndPath=newFileNameAndPath,destination="#newFilePath##arguments.fileID#",overwrite="true",zipfileID = arguments.fileID)>
						<cfdirectory directory = "#newFilePath##arguments.fileID##suffix#" action = "delete" recurse = "yes">
					</cfif>
					<!--- END: 2014-07-24 NYB Case 439373 - vice versa workaround --->


					<cfif not zipFile.isOK>
						<cfset result.message = zipFile.message>
						<cfset result.isOK = false>
					</cfif>
				</cfif>

				<!--- 2013-10-16 NYB Case 437439 added if: --->
				<cfif result.isOK>
					<!--- GCC 2008-02-14 If the file is of type email template read it and save as UTF-8 --->
					<cfif arguments.fileTypeID eq 5>
						<cffile action="read" file="#newFileNameAndPath#" variable="fileText">
						<cffile action="write" file="#newFileNameAndPath#" output="#fileText#" charset="utf-8">
					</cfif>

					<cfif not newFile>
						<cfset client_file = Replace(file.clientFile," ","_","ALL")>
						<cfif arguments.filename eq client_file and oldFileNameAndPath neq newFileNameAndPath>
							<!--- if the file uploaded was a replacement, then rename to original name --->
							<cfset deleteFile = application.com.fileManager.deleteFile(oldFileNameAndPath)>
							<cffile action="rename"	source="#newFileNameAndPath#" destination="#oldFileNameAndPath#">
							<cfset newFileName = file.clientFile>		<!--- 2013-11-26 PPB Case 438017 reset the filename back to original --->

						<cfelseif fileExists(oldFileNameAndPath) and oldFileNameAndPath neq newFileNameAndPath>
						<!--- Then new file has a different name so delete the old file --->
							<cfset deleteFile = application.com.fileManager.deleteFile(oldFileNameAndPath)>
						</cfif>
					</cfif>

					<!--- START: 2016-10-07 YMA Auto Generate thumbnail icon's for JPEG,JPG,GIF,TIFF,PNG,BMP,PDF files --->
					<cfif not structKeyExists(form,"file1") or (structKeyExists(form,"file1") and trim(form.file1) eq "")>
						<cfset var thumbnailSource = newFileNameAndPath>
						<cfif not fileExists(thumbnailSource)>
							<cfset thumbnailSource = oldFileNameAndPath>
						</cfif>

						<cfset var thumbnailDest = "#application.paths.content#\linkimages\files\#arguments.fileID#\">
						<cfset application.com.globalFunctions.CreateDirectoryRecursive(thumbnailDest)>
						<!--- create a thumbnail with cfPDF if document is a PDF --->
						<cfif listfindnocase("PDF",file.serverFileExt)>
							<cfset thumbnail = application.com.relayimage.convertPdfToImage(
								destination=thumbnailDest
								,filename="Thumb_#arguments.fileID#"
								,source="#thumbnailSource#"
								,type="png"
								)>
							<cfset thumbnail = "#thumbnailDest##thumbnail#">
							<cffile action ="rename" source = "#thumbnail#" destination = "#thumbnailDest#\Thumb_#arguments.fileID#.png">

							<cfset thumbnail = application.com.relayimage.resizeImage(
								targetwidth=application.com.settings.getsetting('files.thumbnailImageWidth')
								,targetHeight=application.com.settings.getsetting('files.thumbnailImageWidth')
								,sourcePath=thumbnailDest
								,sourceName="Thumb_#arguments.fileID#.png"
								)>
						</cfif>
						<!--- create a thumbnail with cfimage if document is a JPEG,JPG,GIF,TIFF,PNG,BMP --->
						<cfif listfindnocase("JPEG,JPG,GIF,TIFF,PNG,BMP",file.serverFileExt)>
							<cfset thumbnail = application.com.relayimage.CreateThumbnail(
								targetSize=application.com.settings.getsetting('files.thumbnailImageWidth')
								,targetFilepath=thumbnailDest
								,targetFileName="Thumb_#arguments.fileID#"
								,sourceFile="mediaFile"
								,targetFileExtension="png"
								)>
							<cfset thumbnail = thumbnail.imagePath>
						</cfif>
					</cfif>
					<!--- END: 2016-10-07 YMA Auto Generate thumbnail icon's for JPEG,JPG,GIF,TIFF,PNG,BMP,PDF files --->

					<cfset result.message = "File uploaded successfully. ">

					<cfquery name="updateFileDetails" datasource="#application.siteDataSource#">
						update files set filename=<cf_queryparam value="#newFileName#" cfsqltype="cf_sql_varchar">,
							url=null,
							revision = getDate(),
							lastUpdated=getDate(),
							lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
							lastUpdatedByPerson=#request.relayCurrentUser.personID#
						where fileId =  <cf_queryparam value="#arguments.fileId#" CFSQLTYPE="cf_sql_integer" >
					</cfquery>
				<!--- 2013-10-16 NYB Case 437439 added if: --->
				</cfif>
			<cfelse>
				<cfset result.message = file.message>
				<cfset result.isOK = false>
			</cfif>

		<!--- no file is specified, so it's a delete of the existing file.. url should be populated --->
		<cfelseif arguments.url neq "" and not newFile>
			<cfif oldFilePath neq "" or arguments.filename neq "">
				<!--- if the old file is a zip file, delete the subdirectory --->
				<cfif getOrigPath.autounzip and right(trim(arguments.filename),4) eq ".zip">
					<cfif directoryExists("#newFilePath#\#arguments.fileID#")>
						<!--- START: 2014-04-30 NYB Case 439373 - added if's around delete --->
						<cfif structkeyexists(form,"REPLACE") and LCase(form.REPLACE) eq "replace">
							<cfdirectory action="delete" directory="#newFilePath##arguments.fileID#" recurse="true">
						</cfif>
						<!--- END: 2014-04-30 NYB Case 439373 --->
					</cfif>
				</cfif>
				<cfset deleteFile = application.com.fileManager.deleteFile(oldFilePath & arguments.filename)>

				<cfquery name="updateFileDetails" datasource="#application.siteDataSource#">
					update files set filename=null,
						lastUpdated=getDate(),
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson=#request.relayCurrentUser.personId#
					where fileId =  <cf_queryparam value="#arguments.fileId#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
			</cfif>

		<!--- no file uploaded or url given, but user has changed fileTypeID - move the file to the new directory --->
		<cfelseif not newFile and arguments.filename neq "" and fileExists("#oldFilePath##arguments.filename#") and oldFilePath neq newFilePath>
			<!--- file with name already exists in new directory... --->
			<cfset newFileName =arguments.filename>
			<cfif fileExists("#newFilePath##newFileName#")>
				<cfset newFileNameNoExt = replace(newFileName,"."&listLast(newFileName,"."),"")>
				<cfset newFileExt = listLast(newFileName,".")>

				<cfloop condition="#fileExists(newFilePath&newFileName)#">
					<cfset newFileName = newFileNameNoExt&count&"."&newFileExt>
					<cfset count++>
				</cfloop>
			</cfif>

			<!--- 2013-07-01	YMA	Case 435986 Make sure the new directory exists before we try and move the file there. --->
			<cfif not directoryExists(newFilePath)>
				<cfdirectory action="create" directory="#newFilePath#">
			</cfif>

			<cffile action="move" source="#oldFilePath##arguments.filename#" destination="#newFilePath##newFileName#">
			<!--- if the file was a zip file and the unzipped contents exist, move them to the new directory as well --->
			<cfif directoryExists(oldFilePath&arguments.fileId)>
				<cfdirectory action="rename" directory="#oldFilePath##arguments.fileId#" newdirectory="#newFilePath##arguments.fileId#">
			</cfif>

			<cfif arguments.filename neq newFileName>
				<cfquery name="updateFileDetails" datasource="#application.siteDataSource#">
					update files set filename=<cf_queryparam value="#newFileName#" cfsqltype="cf_sql_varchar">,
						lastUpdated=getDate(),
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson=#request.relayCurrentUser.personID#
					where fileId =  <cf_queryparam value="#arguments.fileId#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
			</cfif>
		</cfif>

		<!--- set the revision date if the url has changed --->
		<cfif form.url_orig neq form.url and form.url neq "">
			<cfquery name="updateFileDetails" datasource="#application.siteDataSource#">
				update files set revision=getDate(),
					lastUpdated=getDate(),
					lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
					lastUpdatedByPerson=#request.relayCurrentUser.personID#
				where fileId =  <cf_queryparam value="#arguments.fileId#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>
		</cfif>

		<cfif not newFile>
			<!--- 2014-01-24	Case 438685 only delete fileFamily if there was originally a value. --->
			<cfif form.fileFamilyID_orig neq form.fileFamilyID and len(form.fileFamilyID_orig)>
				<cfquery name="doFilesExistInOrigFileFamily" datasource="#application.siteDataSource#">
					delete from fileFamily where fileFamilyID =  <cf_queryparam value="#form.fileFamilyID_orig#" CFSQLTYPE="cf_sql_integer" > and not exists (select 1 from files where fileFamilyID =  <cf_queryparam value="#form.fileFamilyID_orig#" CFSQLTYPE="cf_sql_integer" > )
				</cfquery>
			</cfif>
		</cfif>

		<!--- 2013-07-02	YMA	We need to set the file size in dbo.files --->
		<!--- 2013-11-28	IH Case 438043 assign the return structure to a variable and check if "size" is numeric to work around of a bug in getFileInfo --->
		<cfif result.isOk eq true and fileExists("#newFilePath##newFileName#")>
			<cfset fileSize = GetFileInfo("#newFilePath##newFileName#").size>
			<cfif isNumeric(fileSize)>
				<cfquery name="updateFileSize" datasource="#application.siteDataSource#">
					update 	files
					set		fileSize =  <cf_queryparam value="#fileSize#" CFSQLTYPE="cf_sql_integer" >
					where 	fileID =  <cf_queryparam value="#arguments.fileId#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

</cfcomponent>