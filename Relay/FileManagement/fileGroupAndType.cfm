<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Updates:

Date    Initials    What was changed
2015-08-27    DAN    K-1463 - VER001 Unable to create new media group - add check if arg fileTypeID exists
2015/09/11		NJH	JIRA PROD2015-57 - show groups in the listing screen even if no type has been created against the group so that they can still be edited.
2016-02-24		WAB 	PROD2015-291  Media Library Visibility.  Altered queries to use new functions
--->

<cfparam name="sortOrder" default="heading,type">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="entityType" type="string" default="fileGroup">
<cfparam name="showTheseColumns" type="string" default="Heading,Type,Path,PortalSearch,Secure,AutoUnZip">

<cfset getFileGroupAndTypes = "">
<cfset xmlSource = "">

<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<cf_includeJavascriptOnce template="/javascript/openWin.js">


<cffunction name="moveFilesInFileTypeDirectory" returntype="struct">
	<cfset var moveResult = {isOk=true,message=""}>

	<cftry>
        <!--- 2015-08-27    DAN    K-1463 - VER001 Unable to create new media group - add check if arg fileTypeID exists --->
		<cfif structKeyExists(form,"secure_orig") and structKeyExists(arguments,"secure") and form.secure_orig neq arguments.secure and structKeyExists(arguments, "fileTypeID")>
			<cfset application.com.fileManager.checkFileInCorrectLocationSecureOrNot(fileTypeID=arguments.fileTypeID)>
		</cfif>

		<cfcatch>
			<cfset moveResult.isOK = false>
			<cfset moveResult.message = "Problem moving files">
		</cfcatch>
	</cftry>

	<cfreturn moveResult>
</cffunction>


<cfset getValidDeliveryMethods = application.com.fileManager.ValidDeliveryMethods()>

<cfif getValidDeliveryMethods.recordCount gt 1>
	<cfset showTheseColumns = listAppend(showTheseColumns,"DefaultDeliveryMethod")>
</cfif>

<cfif structKeyExists(url,"editor")>

	<cfif entityType eq "fileGroup">
		<cfsavecontent variable="xmlSource">
			<cfoutput>
			<editors>
				<editor id="232" name="thisEditor" entity="fileTypeGroup" title="Media Group Editor">
					<field name="fileTypeGroupID" label="phr_fileTypeGroup_fileTypeGroupID" description="This records unique ID." control="html"></field>
					<field name="heading" label="phr_fileTypeGroup_heading" required="true"/>
					<field name="intro" label="phr_fileTypeGroup_intro" required="true"/>
					<field name="portalSearch" label="phr_fileTypeGroup_portalSearch" control="yorn"/>
					<field name="fileTypes" condition="fileTypeGroupID neq 0" label="phr_fileTypeGroup_fileTypes" control="childRecords" childTable="fileType" showDelete="NumResources eq 0" showColumns="type,path,secure,autounzip,<cfif getValidDeliveryMethods.recordCount gt 1>defaultDeliveryMethod,</cfif>(select count(fileID) from files where filetypeID=e.filetypeId) as NumResources"/>
					<group label="Edit Options">
						<field name="" label="phr_fileTypeGroup_restrictUploadToUserGroups" description="" control="recordRights" permissionLevel="2" suppressUserGroups="false" userGroupTypes="licensed,personal,internal"></field>
					</group>
					<group label="System Fields" name="systemFields">
						<field name="sysCreated"></field>
						<field name="sysLastUpdated"></field>
					</group>
				</editor>
			</editors>
			</cfoutput>
		</cfsavecontent>
	<cfelse>
		<cfsavecontent variable="xmlSource">
			<cfoutput>
				<cfinclude template="/filemanagement/fileTypeEditor.cfm">
			</cfoutput>
		</cfsavecontent>
	</cfif>
<cfelse>

	<cfset topHeadArgs = {topHeadcfm="fileTopHead.cfm",pageTitle="Media Groups"}>
	<cfif isDefined("fileTypeGroupId")>
		<cfset topHeadArgs.fileTypeGroupID = fileTypeGroupId>
	</cfif>
	<cfset application.com.request.setTopHead(argumentCollection=topHeadArgs)>

	<cfquery name="getFileGroupAndTypes" datasource="#application.siteDataSource#">
		declare @personid int = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		select ftg.fileTypeGroupID, heading, type,path,secure,autounzip,defaultDeliveryMethod, fileTypeId, portalSearch
		from 
			fileTypeGroup ftg
				left join 
			fileType ft on ftg.fileTypeGroupID = ft.fileTypeGroupID
				left join 
			dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileTypeGroup").entityTypeId#,@personid) as rftg on rftg.recordid = ftg.fileTypeGroupid
				left join
			dbo.getRecordRightsTableByPerson (#application.com.relayEntity.getEntityType("fileType").entityTypeId#,@personid) as rft on rft.recordid = ft.fileTypeid
		where 1=1
			and
				(ftg.hasRecordRightsBitMask & 2 = 0 OR rftg.level2 <> 0)
				AND
				(ft.fileTypeId is null or ft.hasRecordRightsBitMask & 2 = 0 OR rft.level2 <> 0) <!--- NJH	JIRA PROD2015-57 - added fileTypeId is null --->

			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
			<cfif structkeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
				and ftg.fileTypeGroupID = <cf_queryparam value="#url.fileTypeGroupID#" cfsqltype="cf_sql_integer">
			</cfif>
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="fileGroupAndType"
	keyColumnList="Heading,type"
	keyColumnKeyList=" , "
	keyColumnURLList=" , "
	keyColumnOnClickList="javascript:openNewTab('##heading##'*comma'##heading##'*comma'/fileManagement/fileGroupAndType.cfm?editor=yes&hideBackButton=true&fileTypeGroupId=##fileTypeGroupId##'*comma{reuseTab:true*commaiconClass:'files'});return false;,javascript:openNewTab('##type##'*comma'##type##'*comma'/fileManagement/fileGroupAndType.cfm?editor=yes&hideBackButton=true&showSaveAndAddnew=false&fileTypeId=##fileTypeId##&entityType=fileType'*comma{reuseTab:true*commaiconClass:'files'});return false;"
	showTheseColumns="#showTheseColumns#"
	FilterSelectFieldList="#showTheseColumns#"
	FilterSelectFieldList2="#showTheseColumns#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getFileGroupAndTypes#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	groupByColumns="Heading"
	hideTheseColumns=""
	booleanFormat="secure,autounzip,portalSearch"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_fileType_"
	postSaveFunction = #moveFilesInFileTypeDirectory#
>