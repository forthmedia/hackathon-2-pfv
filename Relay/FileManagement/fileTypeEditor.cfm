<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		fileTypeEditor.cfm	
Author:			NJH  
Date started:	24-04-2013
	
Description:	XML editor for filetypes		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016-03-01 		WAB		PROD2016-509 Added addWatermark option

Possible enhancements:


 --->


<cfif not isDefined("getValidDeliveryMethods")>
	<cfset getValidDeliveryMethods = application.com.fileManager.ValidDeliveryMethods()>
</cfif>


<cfset filesInType = 0>
<cfif isDefined("filetypeID")>
	<cfquery name="getFilesInFileType" datasource="#application.siteDataSource#">
		select count(fileID) as fileCount from files where fileTypeId = <cf_queryparam value="#filetypeID#" cfsqltype="cf_sql_integer">
	</cfquery>
	<cfset filesInType = getFilesInFileType.fileCount>
</cfif>

<editors>
	<editor id="232" name="fileTypeEditor" entity="fileType" title="Resource Type Editor">
		<field name="fileTypeID" label="phr_fileType_fileTypeID" description="This records unique ID." control="html"></field>
		<field name="type" label="phr_fileType_type" required="true"/>
		<field name="path" label="phr_fileType_path" required="true" pattern="^\w[\w\/]*\w$" message="must be a valid file path, containing only letters,numbers, underscores or forward slashes. It must not start or end with a forward slash." validate="regex" <cfif filesInType neq 0>control="html"</cfif>/>
		<field name="secure" label="phr_fileType_secure"/>
		<field name="autounzip" label="phr_fileType_autounzip"/>
		<field name="allowedFiles" label="phr_fileType_allowedFiles" validValues="func:com.fileManager.getAllowedFilesForType()" multiple="true" size="5"/>
		<field name="addWatermark" label="Add Watermark (Secure PDF Files Only)" control="yorn" condition = "currentRecord.secure IS 1 and (currentRecord.allowedFiles is '' OR listFindNocase(currentRecord.allowedFiles,'pdf'))" />
		<cfif getValidDeliveryMethods.recordCount gt 1>
			<field name="defaultDeliveryMethod" label="phr_files_deliveryMethod" validValues="func:com.fileManager.ValidDeliveryMethods()" <cfif isDefined("filetypeID")>displayAs="radio"</cfif>></field>
		</cfif>
		<group label="System Fields" name="systemFields">
			<field name="sysCreated"></field>
			<field name="sysLastUpdated"></field>
		</group>
	</editor>
</editors>