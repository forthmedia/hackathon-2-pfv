<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			editFileContents.cfm
Author:				SWJ
Date started:		2003-04-27

Purpose:	To provide a method for editing files.

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
SWJ: 2006-08-15 Replaced subdir with session.subdir so no need to set using form variable
PPB  2008-09-30 commented out combinedfilecontent debug
NJH 2009/11/23 LHID 2356 - check if file has BOM; if so, re-write file with BOM.
WAB 2010/09/10 added some slashes after application. userfilespath
NYB 2011-01-14 LHID 4903 - made a number of changes to limit attributes passed to cfmodule,
				and to handle file content passed through containing "body onsubmit" and "<textarea>",
				or where the contents is blank
MS  2011-01-20  LID:5330 Fixed the variable name 'CustomInsert' from 'AddCustomInsert' Line 210
WAB 2011/06/01  LID 6769  Security Scan - Prevent Random Files Being Edited
WAB 2012-11-29 CASE 432313 phr_ Phrases inside Files being translated during editing - add DONOTTRANSLATE protection
SB  2015/12/02 Bootstrap and removed tables
Possible enhancements:


 --->
<cf_checkFieldEncryption fieldNames = "filename,subdir,userFilesSection">

<cfparam name="session.subdir" default="">
<cfif isDefined("URL.subdir")>
	<cfset session.subdir = URL.subdir>
</cfif>

<cfparam name="fileName" type="string" default="">
<cfparam name="pagesBack" type="numeric" default="0">
<cfparam name="frmReturnTo" type="string" default="">
<cfparam name="fileTypeID" type="numeric" default="0">
<cfparam name="userFilesSection" type="string" default="content">
<cfparam name="outputContent" type="boolean" default="false">


<cfparam name="showRelayTags" type="boolean" default="false">
<cfset pagesBack = pagesBack+1>
<cfset editOK = 1>
<cfset chooseFile = 0>

<cfset pathToSection = application.paths[userFilesSection]>

<cfif filename eq "" and session.subdir eq "">
	<!---
	WAB 2011/06/01  LID 6769  Security Scan - Prevent Random Files Being Edited
	Too dangerous to show every file on the system
	Just Abort
	 --->
	<CF_ABORT>

<cfelseif filename eq "" and session.subdir neq "">

	<cfset editOK = 0>
	<cfset chooseFile = 1>
	<cfdirectory directory="#pathToSection#\#session.subDir#"
		name="directoryQuery"
		sort="name ASC">
</cfif>
<!--- ==============================================================================
       SAVE CHANGES IF FORM.saveChanges is set to save changes
=============================================================================== --->
<cfif isDefined("form.saveChanges") and form.saveChanges eq "save changes">

	<CFPARAM name="fileText_HTML" default = "">
	<CFPARAM name="fileText_Text" default = "">
	<!--- combining HTML and Text version together into one string - mainly for email templates, but doesn't affect anything else --->
	<cfset combinedfilecontent = application.com.communications.combineHTMLandText(fileText_HTML,fileText_Text)>

	<!---
	<cfoutput>#application.userFilesAbsolutePath##userFilesSection#\#session.subDir#\#fileName#<br>
	***#combinedfilecontent#***</cfoutput>
	--->


	<!---

		NJH 2009/11/23 LHID 2356 - use fileWriter as the problem was that a BOM was not being written. We check now whether a bom exists

	<cffile action="WRITE"
        file="#application.userFilesAbsolutePath##userFilesSection#\#session.subDir#\#fileName#"
        output="#combinedfilecontent#"
        addnewline="No"
		> --->

	<cfscript>
		useBom = application.com.fileWriter.doesFileHaveBOM("#pathToSection#\#session.subDir#\#fileName#");
		myOutFile = createObject('component', '/relay/com/FileWriter').init("#pathToSection#\#session.subDir#\#fileName#", "utf-8", 131072, useBom);
		myOutFile.writeLine(combinedfilecontent);
		myOutFile.close();
	</cfscript>

	<!--- if frmReturnTo is set then go back to that page --->
	<cfif isDefined ("frmReturnTo") and frmReturnTo is not "">
		<CFLOCATION URL="#frmReturnTo#&message=File Saved OK" ADDTOKEN="No">
	</cfif>

</cfif>

<cfif fileName neq "">
<cftry>
	<cfif filetypeid eq application.fileTypeID["E-Mail Template"]>
		<cffile action="READ" file="#pathToSection#\#session.subDir#\#fileName#" variable="fileText" charset="utf-8">
	<cfelse>
		<cffile action="READ" file="#pathToSection#\#session.subDir#\#fileName#" variable="fileText"><!--- 2008/06/25 GCC removed charset="utf-8" - works better working it out for itself --->
	</cfif>

<cfcatch type="Any">
	<cfset fileText = "Could not find #fileName# on this server.  Please check you have the correct filename.">
	<cfset editOK = 0>
</cfcatch>
</cftry>

<!--- decide whether content is HTML or plain text or both
this is primarily for emailmail templates but might as well use here!
 --->
	<CFIF findnocase("</textarea>",filetext) gt 0>
		<cfset filetext = ReplaceNoCase(filetext,"</textarea>", "&lt;/textarea&gt;","all")>
	</CFIF>

	<CFIF findnocase("submit()",filetext) gt 0 or findnocase("<cf",filetext) gt 0 or findnocase("</cf",filetext) gt 0>
		<cfset filetext_TeXT = filetext>
		<cfset filetext_HTML = "">
	<cfelse>
		<cfset content = application.com.communications.extractHTMLandText (filetext)>
		<cfset filetext_HTML = content.html>
		<cfset filetext_TeXT = content.text>
	</cfif>
</cfif>

	<!--- if this filetype is an email template filetype then we sould show the special fields used for email editing --->
	<cfif filetypeid eq application.fileTypeID["E-Mail Template"]>
		<cfset showEmailFields ="true">
		<cfset outputContent ="true">
	<cfelse>
		<cfset showEMailFields ="false">
	</cfif>

<cf_title>Edit File Contents</cf_title>
<cfset application.com.request.setTopHead(topHeadcfm="fileTopHead.cfm")>
<cfoutput>
	<!--- <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR class="Submenu">
			<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Edit #fileName#</TD>
			<TD ALIGN="right" VALIGN="TOP" class="Submenu">
				<a href="javascript:history.go(-#pagesBack#)" class="Submenu">Back</a> &nbsp;&nbsp;
			</TD>
		</TR>
	</TABLE> --->

	<cfif chooseFile>
		<cfset validFileTypes = ".txt,.cfm,.css,.js,.htm,.xml">
		<div id="editFileChooseFile">
			<form method="post" name="choiceForm">
				<div class="form-group">
					<select name="fileChoice" onChange="location.href=document.choiceForm.fileChoice.value" class="form-control">
						<option value="">Please choose the file to edit</option>
						<cfloop query="directoryQuery">
							<!--- only show files in the validFileTypes defined above --->
							<cfif listFindNoCase(validFileTypes,right(name,4)) neq 0 or listFindNoCase(validFileTypes,right(name,3))>
								<option value="#application.com.security.encryptURL('/fileManagement/editfilecontents.cfm?filename=#name#&userFilesSection=#userFilesSection#&pagesBack=#pagesBack#')#">#htmleditformat(name)#</option>
							</cfif>
						</cfloop>
					</select>
				</div>
				<!--- SWJ: 2006-08-15 Replaced subdir with session.subdir so no need to set using form variable
				<input type="hidden" name="subdir" value="#session.subDir#"> --->
			</form>
		</div>
	</cfif>

	<cfif outputContent>
		<div class="editFilePreviewContent">
			<div class="form-group">
				<label>Preview Content</label>
				<!-- StartDoNotTranslate -->#htmleditformat(FileText)#<!-- EndDoNotTranslate -->
			</div>
		</div>
	</cfif>

	<cfif editOK>
		<br>
		<form method="post" name="fileTextForm">
		<h3 id="editFileContentH3">Edit Content</h3>
		<p>Edit this file in the area below.</p>

		<div class="form-group">
			<cf_includeJavascriptOnce template="/ckeditor/ckeditor.js">
			<cf_includeJavascriptOnce template="/ckfinder/ckfinder.js">
			<!-- StartDoNotTranslate -->
			<TextArea name="fileText_HTML" id = "fileText_HTML">#HTMLEDITFORMAT(filetext)#</TEXTAREA>
			<!-- EndDoNotTranslate -->


			<script>
				CKInstanceName = 'fileText_HTML';

				var editor = CKEDITOR.replace( CKInstanceName,
					 {
						<cfif fileExists("#application.paths.relay#/javascript/ckeditor_config_communication.js")>
						 	customConfig : '/javascript/ckeditor_config_communication.js',
						</cfif>
							filebrowserLinkUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Files&relayContext=communication&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
							filebrowserImageUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Images&relayContext=communication&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
							filebrowserFlashUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Flash&relayContext=communication&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
							height:300
							<cfif fileText_HTML eq "">,toolbar_MyToolbar: [],startupMode:'source'</cfif> <!--- if editing text, start up in source mode and remove all buttons from toolbar. --->
					}
					);
					CKFinder.setupCKEditor( editor, {basePath:'/ckfinder/'});

			</script>

			<!--- <cfif filetext_html is not "" and session.subdir neq "borders">
			<tr>
				<TD>
						<cfscript>
					if (showEmailFields) {
							linkStructure = application.com.devEdit.getFilesForDownload();
							custominsert = application.emailMergeFieldsHTML ;
					} else {
						// WAB 2010-11-08 pass these as empty structures not empty strings
						linkStructure = {} ;
						custominsert = {};
					}
						</cfscript>

					<cfset moduleAttributes = {setValue=filetext_HTML,StyleSheet="/code/styles/DefaultPartnerStyles.css",setDeveditPath="/devEdit",setImagePath="/content/MiscImages",setName="fileText",setWidth="600",setHeight="250",SetDocumentType="de_DOC_TYPE_HTML_PAGE",hideSaveButton="1"}>
					<cfif IsStruct(linkStructure) and not StructIsEmpty(linkStructure)>
						<cfset moduleAttributes.AddCustomLink = linkStructure>
					</cfif>
					<cfif IsStruct(custominsert) and not StructIsEmpty(custominsert)>
						<cfset moduleAttributes.AddCustomInsert = CustomInsert>
					</cfif>

					<cftry>
						<cfmodule
							template="/devEdit/class.devedit.cfm"
							attributecollection="#moduleAttributes#" >
						<cfcatch type="any">
							<cfoutput><p>template error=<br/><cfdump var="#cfcatch#"></p></cfoutput>
						</cfcatch>
					</cftry>
				</TD>
			</tr>
		</cfif>

		<cfif filetext_text is not "" or (filetext_text is "" and filetext_html is "")>
			<cf_includejavascriptonce template="\javascript\textAreaControlBar.js">
			<tr>
				<td>
						<cfmodule template="/templates/textAreaControlBar.cfm"
							textAreaName="fileText_Text"
							formname="fileTextForm"
							showRelayTags="#showRelayTags#"
							showClickThrus="false"
						showHTMLTags="true"
							showSpecialFields="#showEmailFields#"
						showFilesForDownload="false">

						<textarea cols="100" rows="27"
							name="fileText_Text"
							ONSELECT="markSelection(this);"
							ONCLICK="markSelection(this);"
							ONKEYUP="markSelection(this);">#FileText_Text#</textarea>
				</td>
			</tr>
		</cfif>			 --->
		</div>
		<input type="submit" name="saveChanges" value="Save Changes" class="btn btn-primary">
			<cf_encryptHiddenFields>
						<CF_INPUT type="hidden" name="fileName" value="#fileName#">
				<!--- SWJ: 2006-08-15 Replaced subdir with session.subdir so no need to set using form variable
				<input type="hidden" name="subdir" value="#session.subDir#"> --->
						<CF_INPUT type="hidden" name="userFilesSection" value="#userFilesSection#">
						<CF_INPUT type="hidden" name="pagesBack" value="#pagesBack#">
						<CF_INPUT type="hidden" name="frmReturnTo" value = "#frmReturnTo#">
			</cf_encryptHiddenFields>
		</form>
	</cfif>
</cfoutput>