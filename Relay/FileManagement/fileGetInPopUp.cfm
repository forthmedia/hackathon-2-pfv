<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-09-06 PPB Case 430058 (2012Summer release) 
when a portal user clicks on a link to a secure file in an email but they are not logged in they are redirected to login and then here so the file loads in a pop-up 

Amendment History:

Date 		Initials 	What was changed

 --->
<cfif structKeyExists(url,"fileId")>
	<cf_includeJavascriptOnce template = "/javascript/openWin.js"> 
	<cfoutput>
	<script language="javascript">
		openWin('#request.currentSite.httpProtocol##request.currentsite.domainandroot#/fileManagement/FileGet.cfm?fileID=#url.fileId#','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
		document.location.href='#request.currentSite.httpProtocol##request.currentsite.domainandroot#';	//having opened up the file in a popup then redirect to home page to avoid being left of the dummy relocator page 
	</script>
	</cfoutput>	
</cfif>


