<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			fileDownloadHistory.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-05-13			SSS			added functionality to show back button on this page

Possible enhancements:


 --->


<!--- <CFIF NOT IsDefined("Cookie.VALID")>
	<CFLOCATION URL="invalidUser.cfm">
	<CF_ABORT>
</CFIF> --->
<CFPARAM NAME="FileName" DEFAULT="">
<CFPARAM NAME="Owner" DEFAULT="">
<!--- <CFINCLUDE TEMPLATE="../styles/DefaultStyles.cfm"> --->
<!--- Query returning search results --->
<!--- MDC 2006-03-21 updated query to bring back Sitename & Country.  I've added these both to the output table. --->
<CFQUERY name="SearchResult" dataSource="#application.SiteDataSource#">
	SELECT FileDownloads.DownloadTime, 
		Files.Name AS FileName, 
		Person.FirstName+' '+Person.LastName AS Downloadedby, 
		Person_1.FirstName+' '+Person_1.LastName AS Owner,
		Person_1.email, location.sitename as Sitename, country.countrydescription as Country
	FROM Files, FileDownloads, Person, Person AS Person_1, location, Country
		Where FileDownloads.PersonID = Person.PersonID 
		And Files.FileID = FileDownloads.FileID 
		And Files.PersonID = Person_1.PersonID
		And Files.FileID =  <cf_queryparam value="#FileID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		And location.locationid = person.locationid
		And Country.countryid = location.countryid
	ORDER BY FileDownloads.DownloadTime DESC;
</CFQUERY>

<CFQUERY NAME="GetFileDatails" DATASOURCE="#application.SiteDataSource#">
	SELECT Files.Name AS FileName, 
	    Person.FirstName+' '+LastName AS Owner
	FROM Files, Person 
	WHERE Files.PersonID = Person.PersonID
	AND Files.FileID =  <cf_queryparam value="#FileID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
<CFSET FileName = #GetFileDatails.FileName#>
<CFSET Owner = #GetFileDatails.Owner#>

<cf_head>
    <cf_title>History of Downloads for #FileName#</cf_title>
</cf_head>
<!--- SSS 2009-05-13 Added back button --->
<cfinclude template="filetophead.cfm">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<CFOUTPUT>
		<TR><TD COLSPAN="2" CLASS="submenu">History of Downloads for #htmleditformat(FileName)#</TD></TR>
		
		<TR><TD COLSPAN="2">This file is owned by #htmleditformat(Owner)#</TD></TR>
	
	</CFOUTPUT>



	<TR bgcolor="cccccc">
	    <TD>Date of download</TD>
	    <TD>Downloaded by</TD>
		<TD>Company</TD>
	    <TD>Country</TD>
	</TR>
	
	<CFOUTPUT query="SearchResult">
		<TR bgcolor="#IIf(CurrentRow Mod 2, DE('ffffff'), DE('ffffcf'))#">
			<TD>#DateFormat(DownloadTime,'dd-mmm-yyyy')# 
			#TimeFormat(DownloadTime,'HH:MM')#</TD>
			<TD><A HREF="mailto:#email#">#htmleditformat(Downloadedby)#</A></TD>
			<TD>#htmleditformat(Sitename)#</TD>
			<TD>#htmleditformat(Country)#</TD>
		</TR>
	</CFOUTPUT>

</TABLE>



