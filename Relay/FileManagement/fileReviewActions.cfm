<!--- �Relayware. All Rights Reserved 2014 --->
<!---

resetFileDates.cfm

Purpose		This file is used to set either an expiry or review date for a file.

Amendment History

Date			By		Description

2006-10-17		NM		Build

--->


<cf_title>Review Actions</cf_title>

<cfparam name="triggerType" default="2">
<cfparam name="action" default="">

<cfif action is "expireFile" and isDefined("fileID")>
	<cfloop list="#fileID#" index="fID">
		<CFQUERY NAME="ExpireFile" DATASOURCE="#application.SiteDataSource#">
			UPDATE files
			SET triggerType = 1,
				triggerDate =  <cf_queryparam value="#createODBCDate(now())#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				lastUpdated = getDate(),
				lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
				lastUpdatedByPerson=#request.relayCurrentUser.personID#
			WHERE FileID =  <cf_queryparam value="#fID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfloop>
	<cfset doRedirect = "filesForReviewReport.cfm">
</cfif>

<cfif action is "setDate" and isDefined("fileID")>
	<cfparam name="triggerType" default="">
	<cfloop list="#fileID#" index="fID">
		<CFQUERY NAME="setDateOnFile" DATASOURCE="#application.SiteDataSource#">
			UPDATE files
			SET triggerType =  <cf_queryparam value="#triggerType#" CFSQLTYPE="CF_SQL_Integer" > ,
				triggerDate =  <cf_queryparam value="#createODBCDate(triggerDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				lastUpdated = getDate(),
				lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
				lastUpdatedByPerson=#request.relayCurrentUser.personID# 
			WHERE FileID =  <cf_queryparam value="#fID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfloop>
	<cfset doRedirect = "">
</cfif>

<cfif action is "setToNever" and isDefined("fileID")>
	<cfloop list="#fileID#" index="fID">
		<CFQUERY NAME="neverExpireFile" DATASOURCE="#application.SiteDataSource#">
			UPDATE files
			SET triggerType = 0,
				triggerDate = NULL,
				lastUpdated = getDate(),
				lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
				lastUpdatedByPerson=#request.relayCurrentUser.personID#
			WHERE FileID =  <cf_queryparam value="#fID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfloop>
	<cfset doRedirect = "filesForReviewReport.cfm">
</cfif>

<cfif not isDefined("fileID")>	
	<cfset message = "No file was specified to ammend.">
<cfelseif isDefined("doRedirect") and doRedirect is not "">
	<cflocation url="#doRedirect#" addtoken="no">
<cfelseif isDefined("doRedirect") and doRedirect is "">
	<SCRIPT type="text/javascript">
		window.opener.location.href = window.opener.location.href;
		window.close();
	</script>
<cfelse>
	<cfform action="/fileManagement/fileReviewActions.cfm" method="post" name="fileForm" ID="fileForm">
		<cfoutput>
			<CF_INPUT type="hidden" name="fileID" value="#fileID#">
			<CF_INPUT type="hidden" name="triggerType" value="#triggerType#">
		</cfoutput>
		<input type="hidden" name="action" value="setDate">
		<cfset showDate = dateAdd("d",request.fileExpireDaysDefault,now())>
		<table>
			<tr>
				<td>phr_sys_Choose_a_date:</td>
				<td><cf_relaydatefield currentvalue="#showDate#" fieldname="triggerDate" thisformname="fileForm" helpText="Click to choose date" anchorname="x"></td>
			</tr>
			<tr>
				<td colspan="2">
					<cfinput type="submit" name="bSubmit" value="phr_sys_set_date">
				</td>
			</tr>
		</table>
	</cfform>
</cfif>