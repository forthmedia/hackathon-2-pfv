<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eServiceFunctions.cfm
Author:				KAP
Date started:		2003-09-03
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_FilesForReviewFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_setAsExpired";
comTableFunction.securityLevel = "";
comTableFunction.url = "/fileManagement/fileReviewActions.cfm?action=expireFile&fileID=";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_setExpiryDate";
comTableFunction.securityLevel = "";
comTableFunction.url = "/fileManagement/fileReviewActions.cfm?triggerType=1&fileID=";
comTableFunction.windowFeatures = "width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_setReviewDate";
comTableFunction.securityLevel = "";
comTableFunction.url = "/fileManagement/fileReviewActions.cfm?triggerType=2&fileID=";
comTableFunction.windowFeatures = "width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_setToNever";
comTableFunction.securityLevel = "";
comTableFunction.url = "/fileManagement/fileReviewActions.cfm?action=setToNever&fileID=";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();
</cfscript>

