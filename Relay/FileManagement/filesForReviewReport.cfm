<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History

Date			By		Description

2008-07-18		NYF		Bug 787 - removed the filesForReview link as it doesn't work, changed link on filename to displya file not reupload file
2012-10-04	WAB		Case 430963 Remove Excel Header
--->

<cf_title>Review Resources</cf_title>
<cfset application.com.request.setTopHead(pageTitle="Review Resources",topHeadcfm="fileTopHead.cfm")>

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Assets For Review</cf_title>
		<cf_includeJavascriptOnce template = "/fileManagement/js/files.js">
		<cf_includeJavascriptOnce template = "/javascript/openWin.js">
		<cf_includeJavascriptOnce template="/javascript/extExtension.js">
	</cf_head>

<cfparam name="sortOrder" default="created">
<cfparam name="numRowsPerPage" default="50"><!---=== set to 50 for LIVE ===--->
<cfparam name="startRow" default="1">

<cfparam name="form.filesIOwn" default="Yes">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="showColList" type="string" default="name,filename,owner,language,triggerDate,download">
<cfparam name="keyColumnList" type="string" default="name,download"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=" , "><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=" , "><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="keyColumnOnClickList" type="string" default="openNewTab('##name##_##fileID##'*comma'##name## (##fileID##)'*comma'/fileManagement/files.cfm?editor=yes&hideBackButton=true&fileID=##fileID##'*comma{reuseTab:true*commaiconClass:'files'});return false;,javascript:getFile(##FileID##);return false;">

<cfparam name="dateFormat" type="string" default="triggerDate"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->


<CFQUERY NAME="getFiles" DATASOURCE="#application.SiteDataSource#">
SELECT DISTINCT f.triggerDate, f.fileid, f.name,
		case when len(isNull(url,'')) > 0 then f.url else f.filename end as filename,
		f.filetypeid, fT.path, l.language,P.FIRSTNAME + ' ' + P.LASTNAME AS owner, fT.type,
		case WHEN F.PERSONID = #request.relayCurrentUser.personid#   THEN 1 ELSE 0 END AS isOwner,
		case when level2 = 1 or f.personID = #request.relayCurrentUser.personID# then '<img src="/images/icons/report_go.png"/>' else '' end as download,
		case when level1 = 1 or f.personID = #request.relayCurrentUser.personID# then 'link' else 'text' end as showNameAs
FROM
	FILES AS F
	INNER JOIN LANGUAGE L ON f.languageid = l.languageid
	INNER JOIN FILETYPE AS FT ON F.FILETYPEID = FT.FILETYPEID
	LEFT JOIN
	vRecordRights rr on rr.entity = 'files' and rr.recordid = fileid and rr.personid <cfif form.filesIOwn is "Yes">=<cfelse><></cfif> #request.relayCurrentUser.personid#
	INNER JOIN PERSON AS p ON P.PERSONID = F.PERSONID
	WHERE f.triggerType = 2 and f.personID <cfif form.filesIOwn is "Yes">=<cfelse><></cfif> #request.relayCurrentUser.personid#
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by f.triggerDate DESC,p.firstname + ' ' + p.lastname
</CFQUERY>


<cfset attributeStruct = structNew()>
<cfif form.filesIOwn is "Yes">
	<cfinclude template="filesForReviewFunctions.cfm">
	<cfset attributeStruct.functionListQuery=comTableFunction.qFunctionList>
</cfif>

	<CF_tableFromQueryObject
		queryObject="#getFiles#"
		queryName="getFiles"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"

		useInclude="true"
		showTheseColumns="#showColList#"
		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOpenInWindowList="no,yes"
		keyColumnOnClickList="#keyColumnOnClickList#"
		columnTranslationPrefix="phr_report_files_"
		columnTranslation="true"

		FilterSelectFieldList=""
		FilterSelectFieldList2=""

		radioFilterLabel="Resources I Own"
		radioFilterDefault="#form.filesIOwn#"
		radioFilterName="filesIOwn"
		radioFilterValues="Yes-No"

		checkBoxFilterName=""
		checkBoxFilterLabel=""
		allowColumnSorting="yes"
		dateFormat="#dateFormat#"

		rowIdentityColumnName="fileID"
		attributeCollection=#attributeStruct#
	>