<!--- �Relayware. All Rights Reserved 2014 --->
<!---

FileGetV2


WAB  2006-02-21

Messing around trying to make the file download experience nicer

The current fileGet requires the page to be reloaded everytime.
I don't think that there is any inherent reason why this should be the case so I am trying it another way

I have basically taken fileGet and replaced the javascript redirects to DisplayFile.cfm and filelinkDialog.cfm with cfincludes and it all seems to work

expects

fileid
elementid (if called from element)  (actually I don't think that it is used)
openwintype


NJH 2008-05-28 In an attempt to consolidate some code, we are no longer using the old displayFile.cfm
		Rather, we are using the code in fileLinkDialog.cfm which I have now renamed to fileDisplay.cfm
WAB 2009-05-27 	get secure to work  LID 2237

2011-12-01		RMB		P-REL101 changes link depending on data in Delivery Method feild
2012-09-12		IH		Case 430243 change getFilesAPersonHasViewRightsTo to getFilesAPersonHasCountryViewRightsTo
2012-11-07 		PPB 	Case 431818 references to variables that didn't exist in error email
2013-04-11 		PPB 	Case 434536 if isUnknownUser force login for access to secure file (previously if logged out with cookie it worked ok)
2013/11/25		NJH		Case 437245 - put output around mail message
2015-07-30		PYW		P-TAT005. Abstract file download notification from fileget.cfm.
--->

<cfsetting  showdebugoutput="No" >      <!--- messes up the very small window if debug is on --->


<cfset application.com.request.allowFramingByAllSites()> <!---449245 - 2016/04/25 GCC allow this page to be in a frame - needed for framed portal functionality--->

<cf_head>
	<cf_title>phr_Download_File</cf_title>
</cf_head>

<CFPARAM name="message"  default= "">

<CFIF NOT IsDefined("fileID")>
	<CFSET message = "No file was specified to download.">
<CFELSE>
	<!--- need to check permissions again or someone could
			just populate a URL --->
<!---
	WAB 2006-02-21

	replaced this query with a call to the cfc which actually checks for rights to view the file

	<CFQUERY NAME="getFile" DATASOURCE="#Application.SiteDataSource#">
		SELECT FileType.Path,
			   Files.Filename,
			   Files.PersonID,
			   FileTypeGroup.heading,
			   files.emailOwnerOnDownload,
			   Person.Email, Person.EmailStatus
		FROM Files
				inner join
			FileType  on Files.FileTypeID = FileType.FileTypeID
				inner join
			 FileTypeGroup on  FileTypeGroup.FileTypeGroupId = FileType.FileTypeGroupId
			 	left join
			person
				on person.personid = files.personid
		WHERE Files.FileID=#fileID#
	</CFQUERY>
		 --->

	<!--- START: 2013-04-11 PPB Case 434536
				If unknown user and the file is secure or has rights, then redirect to the login page
				2013-05-01 WAB altered to take into account rights as well as secure
	 --->
	<cfif request.relaycurrentuser.isUnknownUser>
		<cfif application.com.filemanager.isFileSecureOrHasRights(fileId=#url.fileID#)>
	 		<cflocation url="#application.com.security.encryptURL('/index.cfm?urlrequested=displayfile=true%26fileId=#url.fileID#')#" addtoken="false">
		</cfif>
	</cfif>
	<!--- END: 2013-04-11 PPB Case 434536 --->


	<cfset getfile = application.com.filemanager.getFilesAPersonHasCountryViewRightsTo(personid = #request.relaycurrentuser.personid#, fileid = fileid,checkFileExists = true)>

	<CFPARAM NAME="emailstatus" DEFAULT="#getFile.emailstatus#">
	<CFPARAM NAME="email" DEFAULT="#getFile.email#">
	<CFPARAM NAME="entityTypeID" DEFAULT="">
	<CFPARAM NAME="entityID" DEFAULT="">
	<CFPARAM NAME="eid" DEFAULT="">


	<cfif not isNumeric(entityTypeID)>
		<cfset entityTypeID = "null">
	</cfif>
	<cfif not isNumeric(entityID)>
		<cfset entityID = "null">
	</cfif>
	<cfif not isNumeric(eid)>
		<cfset eid = "null">
	</cfif>

	<CFIF getFile.RecordCount IS 1>
		<!--- WAB 2009-05-27 LID 2237 get file query now has correct path and filename, taking into account the secured setting <CFSET FileFullPath = "#application.paths.content#\#getFile.path#\#getFile.Filename#"> --->
<!--- 		<CFSET FileFullPath = "#application.UserFilesAbsolutePath#\#getFile.FileNameAndPath#"> --->

		<!--- NJH 2013/01/10 2013Roadmap - check if url is populated as well, as we now handle files that point to elsewhere --->
		<CFIF getFile.fileExists or getFile.url neq "">
			<CFIF EmailStatus LTE 0 and getFile.emailOwnerOnDownload is 1>
				<CFSET EmailSent = 1>
<!---
				<CFSET mailmsg = "The file #getFile.Filename# was downloaded from #request.CurrentSite.Title# by #request.relaycurrentuser.fullname#.">
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "FileID: #fileID#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "Filename: #getFile.Filename#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "User: #request.relaycurrentuser.fullname#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "Download Time: #DateFormat(Now(), "mmm-dd-yyyy")# #TimeFormat(Now(),"HH:MM")#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "This is just for your information.">
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "Automated message from #GetFileFromPath(GetTemplatePath())# (Message 1)">
				<CF_MAIL TO="#Email#"
						FROM="#application.AdminPostmaster#"
						SUBJECT="#getFile.Filename# Filedownloaded."><cfoutput>#mailmsg#</cfoutput></CF_MAIL>
--->
				<!--- 2015/07/30 PYW. P-TAT005. Abstract file download notification from fileget.cfm. --->
				<cfset application.com.fileManager.notifyResourceDownload(	emailTo=Email,
																			personid=getFile.ownerid,
																			emailTo=Email,
																			emailFrom=application.AdminPostmaster,
																			filename=getFile.Filename,
																			siteTitle=request.CurrentSite.Title,
																			fullname=request.relaycurrentuser.fullname,
																			fileID=fileID,
																			filePath=GetFileFromPath(GetTemplatePath())
				)>

			<CFELSE>
				<CFSET EmailSent = 0>
			</CFIF>

			<CFSET DownloadTime=request.requestTimeODBC>
			<CFSET FileID=FileID>
			<CFSET EmailSent=EmailSent>
			<CFSET personid = request.relaycurrentuser.personid>

			<cfif request.relayCurrentUser.visitID is not 0>
				<CFQUERY NAME="InsertDownload" DATASOURCE="#Application.SiteDataSource#">
					INSERT INTO FileDownloads
						(DownloadTime,PersonID, EmailSent, FileID, referringEntityTypeID, referringEntityID, referringEID, visitID)
					VALUES
						(<cf_queryparam value="#DownloadTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#EmailSent#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#FileID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER">, <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER">, <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER">, <cf_queryparam value="#request.relayCurrentUser.visitID#" CFSQLTYPE="CF_SQL_INTEGER">)
				</CFQUERY>
			</cfif>
			<!--- WAB 2009-05-27 LID 2237 get secure files to work, fileNameAndPath added to the getFile query and deals with secure--->
			<!--- 	<CFSET FilePath = "/content/#getFile.path#/#getFile.Filename#">		 --->
			<CFSET FilePath = getFile.fileURL>
			<CFSET FileName = getFile.Filename>

			<!--- 2011/12/01 - RMB - P-REL101 - change link depending onDelivery Method --->
			<cfset checkDeliveryMethod = getFile.DeliveryMethod>

			<cfif checkDeliveryMethod IS ''>
		  		<cfset checkDeliveryMethod = 'Local'>
			</cfif>

			<cfif checkDeliveryMethod EQ 'Local'>

				<!--- determines the type of window to open...
				Currently only 3 options --->
				<cfparam name ="openwintype" default = ""> <!--- WAB added default 2005-11-29 --->
				<cfswitch expression="#openwintype#">
					<!--- NJH 2008-05-28 In an attempt to consolidate some code, we are no longer using the old displayFile.cfm
						Rather, we are using the code in fileLinkDialog.cfm which I have now renamed to fileDisplay.cfm --->
					<!--- <cfcase value="2,3">

						<cfinclude template = "displayFile.cfm">
						<!--- <SCRIPT type="text/javascript">
							<CFOUTPUT>
								openWin('/fileManagement/displayfile.cfm?filePath=#urlencodedformat(FilePath)#&fileName=#urlencodedformat(getFile.Filename)#','WinName','width=800,height=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
							</CFOUTPUT>
						</SCRIPT> --->
					</cfcase> --->
					<cfdefaultcase>
						<script>
							if (self.resizeTo) {      // this tests if the method is available
								 <cfoutput>self.resizeTo(#application.com.settings.getSetting('files.maxPopupWidth')#,#application.com.settings.getSetting('files.maxPopupHeight')#);</cfoutput>	<!--- 2013-07-22 PPB removed "settings" from getSetting() param --->
							}
						</script>
						<cfinclude template = "fileDisplay.cfm">
					</cfdefaultcase>
				</cfswitch>

			<cfelse>
				<!--- 2011/12/01 - RMB - P-REL101 - change link depending onDelivery Method --->
				<cfif checkDeliveryMethod EQ 'AkamaiCDN'>

					<script>
						if (self.resizeTo) {      // this tests if the method is available
							 self.resizeTo(550,550);
						}
					</script>

					<cfset UsesUrl = "/#getFile.fileNameAndPath#">
					<!-- Not passing sExtract = UsesExtract & nTime = UsenTime -->
					<cfif getFile.secure EQ 1>
						<!--- 2012-05-18 - RMB - P-REL101 - changed call to "GetAkamaiToken" from "urlauth_gen_url" also now passing in fileID to new function --->
						<cfset GetAkamaiToken = application.com.AkamaiCDN.GetAkamaiToken(sUrl = UsesUrl, fileID = fileID)>
						<cfset AkamaiCDNURL = "#GetAkamaiToken#">
					<cfelse>
						<cfset AkamaiPartOne = application.com.settings.getsetting('files.Akamai.AkamaiPublicRoot')>
						<cfset AkamaiCDNURL = "http://#AkamaiPartOne##UsesUrl#">
					</cfif>

	  					<!--- RMB 2011/12/15 Opens file link in new popup and close old window --->
						<!--- Not working for google chrome
						<script>
							<cfif request.relaycurrentuser.browser.name CONTAINS 'phone' OR
								  request.relaycurrentuser.browser.name CONTAINS 'Blackberry' OR
								  request.relaycurrentuser.browser.name CONTAINS 'Mobile'>
							window.location = '#AkamaiCDNURL#';
							<cfelse>
							window.open('#AkamaiCDNURL#','MyAkamaiWindow','width=550,height=550,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
							window.close();
							</cfif>
						</script>
						--->
					<cfoutput>
		                Downloading content. Please close this window once your download has finished to return to the portal
						<script>
							window.location = "#jsStringFormat(AkamaiCDNURL)#";
						</script>
					</cfoutput>
				<cfelse>
					Delivery Method Not Set
				</cfif>
			</cfif>

		<CFELSE>
			<cfif application.testsite is 0>
				<!--- problem, the file should be there but isn't --->
				<CFSET mailmsg = "A file was listed in the database but could not be found to be uploaded to the user.">
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "FileID: #fileID#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "Filename: #getFile.Filename#" & Chr(13) & Chr(10)>		<!--- 2012-11-07 PPB Case 431818 changed FileFullPath to getFile.Filename --->
				<CFSET mailmsg = mailmsg & "User: #request.relaycurrentuser.personid#" & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "You may want to look into this situation.">
				<CFSET mailmsg = mailmsg & Chr(13) & Chr(10) & Chr(13) & Chr(10)>
				<CFSET mailmsg = mailmsg & "Automated message from #GetFileFromPath(GetTemplatePath())# (Message 1)">
				<CFMAIL TO="#application.AdminEmail#"	<!--- 2012-11-07 PPB Case 431818 added application. --->
						FROM="#application.AdminPostmaster#"
						SUBJECT="#getFile.FileName# Error: File could not be found."><cfoutput>#mailmsg#</cfoutput></CFMAIL>
			</cfif>

			<CFSET message = """#getFile.Filename#""phr_Sys_FileNotFound"><!--- STCR 2012-05-14 P-REL109 Use translatable phrase for " - Sorry, this file could not be found." --->
		</CFIF>
	<CFELSE>
		<!--- do not have permission to download file --->
		<CFSET message = "phr_Sys_DownloadFileNotAuthorized"> <!---2016/01/04 GCC 446927 make into a translation --->
	</CFIF>

</CFIF>

<cfif message is not "">
	<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
</cfif>