﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			files.cfm
Author:				NJH
Date started:		2013/03/19

Description:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2013-05-23			YMA			Case 435422 Added novalidate="true" to FileFamilyID.  Post processing then adds the file family after form is submitted if the value is not numeric.
2013-05-28			YMA			Case 435402 Taken recordHasSomeLevel1Rights into account when choosing if an internal user can download a file or not.
2013-11-26 			PPB 		Case 438017 reset the filename back to original if replacing a file
2013-11-28			IH 			Case 438043 assign the return structure to a variable and check if "size" is numeric to work around of a bug in getFileInfo
2013-04-25			YMA			Case 435852 insert null when file family isnt defined to support foreign keys.
2014-04-30 			NYB 		Case 439373 add option to have contents of zip append or replace current data from that file upload
2014-06-25 			NYB 		CASE 439373 - trying to get around ViceVersa issues
2014-07-24 			NYB 		Case 439373 introduced workaround to vice versa
2014-08-13			AHL			Case 439373 part 4, SS - Reuploading courseware files - adding Purge/Append option - Disabling 'Upload File' - 'Choose File' until the file is properly uploaded.
2014-10-31			DXC			Case #442319 - remove userGroupTypesattribute from phr_files_restrictDownloadToUserGroups
2014-09-24			DXC 		Case #440724 Replace + in filenames with the word plus to work around IIS security "feature"
2014-10-31			DXC			Case #442319 - remove userGroupTypesattribute from phr_files_restrictDownloadToUserGroups
2015-03-20			SB			Changed the width of the thum image for file_list
2015-07-02			YMA			Add sort order to file editor
2015/09/09			NJH			Jira PROD2015-39 - show filename rather than ID when deleting a file.
2016/01/22			NJH			Jira BF-332 - fixed a number of JS issues. Cleaned up the JS... put a lot into the js file. Moved functions into a files.cfc to clean up files.cfm
2016-02-24			WAB 		PROD2015-291  Media Library Visibility.  Implement Extended Rights for files
2016-03-02 			WAB 		CASE 448308 encrypt fileID for deleteFile function
2016/06/17			NJH			PROD2016-1252 - removed fileNameAndPath and replaced call in files.js with jQuery('#nameAndPath_value')
2016-10-11			WAB			PROD2016-2490 Internal UI tabs go funny with hebrew filenames/titles.  Change tab_name (identitifier) so does not contain the file name
Possible enhancements:


 --->

<cf_title>Files</cf_title>

<cfset defaultSortOrder = "fileFamilyName,heading,name">

<cfparam name="fileTypeGroupID" default="">
<cfparam name="getFiles" type="string" default="">
<cfparam name="sortOrder" default="#defaultSortOrder#">
<cfparam name="numRowsPerPage" type="numeric" default="50">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="showTheseColumns" type="string" default="name,heading,fileFamilyName,type,source,fileLanguage,OwnerName,revision,expired,download,deleteFile">
<cfparam name="fileID" type="numeric" default="0">
<cfparam name="fileEdit" type="boolean" default="true">

<cfparam name="filterString" type="string" default="">
<cfset xmlSource = "">
<cfset fileNameAndPath = "">

<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>

	<cfquery name="getFileGroupHeading" datasource="#application.siteDataSource#">
		select heading from fileTypeGroup where fileTypeGroupID=<cf_queryparam value="#url.fileTypeGroupID#" cfsqltype="cf_sql_integer">
	</cfquery>

	<cfset fileGroupHeading = getFileGroupHeading.heading>
</cfif>

<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
	<cfset fileSearchArgs.fileTypeGroupID = url.fileTypeGroupID>
</cfif>

<cf_includeJavascriptOnce template="/fileManagement/js/files.js" translate="true">
<cf_includeJavascriptOnce template="/javascript/openwin.js">


<cfif structKeyExists(url,"editor")>
	<cfset getValidDeliveryMethods = application.com.fileManager.ValidDeliveryMethods()>

	<cfset SortOrderList="select 1 as value,1 as display">
	<cfloop index='i' from='2' to='99'>
		<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
	</cfloop>

	<cfif fileID neq 0>
		<cfset hasRightsToFile = application.com.fileManager.getFilesAPersonHasEditRightsTo(personID=request.relayCurrentUser.personID,fileID=fileID)>
	</cfif>

	<cf_head>
	<script>
		jQuery(document).ready(function() {
			//setFileUrlRequired();
			jQuery('#url').attr('data-isValid',true);
			jQuery('#name').attr('data-isValid',true);

			if (jQuery('#source_File').prop('checked')) {
				jQuery('#url_formgroup').hide();
			} else {
				jQuery('#mediaFileRow_formgroup').hide();
			}

			jQuery('#mediaFile_lineItemDiv :input').attr('disabled', true);

			jQuery.ajax(
		    	{type:'get',
		        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=fileWS&methodName=getFileNameAndPath&returnFormat=plain',
		        	data:{fileID:parentEntityID },  //2014-04-30 NYB Case 439373 changed to use parentEntityID
		        	dataType:'html',
		        	success: function(data,textStatus,jqXHR) {
						jQuery('#nameAndPath_value').html(data.trim());
						jQuery('#mediaFile_lineItemDiv :input').removeAttr('disabled');		// 2014-08-13 AHL Case 439373 part 4, SS - Reuploading courseware files - adding Purge/Append option
					}
				}
			);
		});
	</script>
	</cf_head>


	<cfset fileTypeParams = "sortOrder='heading,type'">
	<cfset fileTypeLabel = "phr_files_groupAndType">
	<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
		<cfset fileTypeParams = "fileTypeGroupID=#url.fileTypeGroupID#">
		<cfset fileTypeLabel = "phr_files_type">
	</cfif>

	<cfquery name="getFileTypeOfFile" datasource="#application.siteDataSource#">
		select fileTypeID from files where fileID=<cf_queryparam value="#fileID#" cfsqltype="cf_sql_integer">
	</cfquery>

	<cfset fileTypeParams = listAppend(fileTypeParams,"restrictToPermissionLevel=2,showFileTypeID=#getFileTypeOfFile.fileTypeID#")>

	<cfset hasFileTaskLevel3 = application.com.login.checkInternalPermissions(securityTask="fileTask",securityLevel="level3")>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="files" title="Resource Editor" readonly="#not(fileID eq 0 or hasRightsToFile.recordCount or hasFileTaskLevel3)#">
				<field name="fileID" label="phr_files_fileID" description="This records unique ID." control="html"></field>
				<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
					<field name="fileTypeGroupID" label="phr_files_fileGroup" description="This records unique ID." control="html">#fileGroupHeading#</field>
				</cfif>
				<!--- 2014-04-30 NYB Case 439373 - added an onChange: --->
				<field name="fileTypeID" label="#fileTypeLabel#" emptyText1="phr_ext_selectavalue" onChange="populateFileName2();" required="true" description="" display="type" value="fileTypeID" <cfif not structKeyExists(url,"fileTypeGroupID") or not isNumeric(url.fileTypeGroupId)>label1="phr_files_fileGroup" name2="fileTypeGroupID" group="heading" displayGroupAs="select"</cfif> query="func:com.fileManager.getFileTypes(#fileTypeParams#)"/>
				<field name="source" label="phr_files_source" list="File,URL" displayAs="radio" control="select" onchange="toggleFileUrlDisplay();" default="file" currentValue="currentRecord.url eq ''?'File':'Url'"></field>
				<lineItem label="phr_files_file" id="mediaFileRow" fieldname="mediaFile">
					<field name="mediaFile" control="file" label="phr_files_file" description="" requiredFunction="return (isItemInCheckboxGroupChecked(jQuery('##source_File').get(0),'File') &amp;&amp; (!jQuery('##filename_lineItemDiv').length || jQuery('##filename_lineItemDiv').html() == ''))" onChange="populateFileName2('file');jQuery('##mediaFile').attr('data-isValid','');" submitFunction="validateFileExtensionOnSubmit" onblur="validateFileExtensionOnBlur();"></field><!--- 2014-04-30 NYB Case 439373 changed populateFileName to populateFileName2 --->
					<field name="filename" label="phr_files_filename" control="html" condition="currentRecord.filename is not ''"/>
					<!--- START: 2014-04-30 NYB Case 439373 - added field: --->
					<lineItem label="phr_files_replace" id="replaceAddRow" fieldname="replaceAddRow">
						<field name="replace" list="Replace,Add" displayAs="radio" control="select" default="Replace" currentValue="Replace"></field>
					</lineItem>
					<!--- END: 2014-04-30 NYB Case 439373 --->
				</lineItem>
				<field name="url" label="phr_files_url" description="" requiredFunction="return (isItemInCheckboxGroupChecked(jQuery('##source_URL').get(0),'URL'))" onChange="jQuery('##url').attr('data-isValid','');populateFileName('url');" onfocus="if (this.value == '') {this.value='http://';}" onblur="validateUrlOnBlur();" submitFunction="validateUrlOnSubmit"></field>
				<field name="name" label="phr_files_name" description="The file name" onChange="jQuery('##name').attr('data-isValid','');" submitFunction="checkFileUniqueInLibraryOnSubmit" onblur="checkFileUniqueInLibraryOnBlur();" required="true"/>
				<field name="nameAndPath" control="html" label="phr_files_nameAndPath"></field>
				<lineItem label="phr_files_fileFamily">
					<field name="fileFamilyID" label="phr_files_fileFamily" novalidate="true" description="" bindfunction="cfc:relay.webservices.fileWS.getFileFamilies({fileTypeID})" bindonload="true" display="display" value="value" nullText="No Family Chosen" allowNewValue="true"></field>
					<field name="editFileFamily" label="" control="html">
						<cfsavecontent variable="editFamilyLink">
							<cfoutput>
								<cfif fileID eq 0 or hasRightsToFile.recordCount or hasFileTaskLevel3>
									<a href="" title="Add File Family" onclick="newSelectValue(jQuery('##fileFamilyID').get(0),'Add File Family');return false;"><img src="/images/icons/add.png"></a> <a href="" title="Edit File family" onclick="editSelectedValue(jQuery('##fileFamilyID').get(0));return false;"><img src="/images/MISC/iconEditPencil.gif"/></a>
								</cfif>
							</cfoutput>
						</cfsavecontent>
						#htmlEditFormat(editFamilyLink)#
					</field>
				</lineItem>
				<!--- 2015/07/02	YMA	Add sort order to file editor --->
				<field name="sortOrder" label="phr_files_sortOrder" description="" control="select" query="#SortOrderList#"></field>
				<field name="description" label="phr_files_description" description="" control="textArea"></field>
				<field name="languageID" label="phr_files_language" description="" query="select languageid,language from language order by language" display="language" value="languageID" default="#request.relayCurrentUser.languageID#"></field>
				<cfif application.com.settings.getSetting("campaigns.displayCampaigns")><field name="campaignID" label="phr_Sys_AssociateCampaign" control="select" value="campaignID" display="campaignName" query="func:com.relayCampaigns.GetCampaigns()" nulltext="Phr_Ext_SelectAValue"/></cfif>
				<group label="Display Options">
					<!--- 2013-09-12	YMA	Move trigger dates onto seperate line for clarity --->
					<field name="publishDate" label="phr_files_publish" description=""></field>
					<lineItem label="phr_files_triggerDate" fieldname="triggerDate">
						<field name="triggerType" label=" " description="" labelAlign="left" displayAs="radio" validValues="0|Phr_file_NeverExpire,1|Phr_file_expireOnDate,2|phr_file_reviewOnDate"></field>
						<field name="triggerDate" label="phr_files_triggerDate" labelAlign="none" description="" requiredFunction="return (isItemInCheckboxGroupChecked(jQuery('##triggerType_1').get(0),1) || isItemInCheckboxGroupChecked(jQuery('##triggerType_2').get(0),2))" required="true"></field>
					</lineItem>
					<field name="" control="file" label="phr_files_thumbnail" description="" acceptType="image" parameters="filePrefix=Thumb_,fileExtension=png,height=#application.com.settings.getsetting('files.thumbnailImageWidth')#,width=#application.com.settings.getsetting('files.thumbnailImageWidth')#"/>
					<field name="" label="phr_files_restrictDownloadToCountries" description="" control="countryScope" width="150"></field>
					<field name="" label="phr_files_restrictDownloadToUserGroups" description="" control="recordRights" suppressUserGroups="false" useExtendedRights="true"></field><!--- 2014-10-31 DXC Case #442319 remove userGroupTypesattribute from phr_files_restrictDownloadToUserGroups --->
					<field name="emailOwnerOnDownload" label="phr_files_emailOwnerOnDownload" description="" control="YorN"></field>
				</group>
				<group label="phr_files_keywords">
					<field name="" flagGroupID="filesKeywords" label="phr_files_keywords" description=""></field>
				</group>
				<group label="Edit Options">
					<field name="" label="phr_files_restrictUploadToUserGroups" description="" control="recordRights" permissionLevel="2" suppressUserGroups="false" userGroupTypes="licensed,personal,internal"></field>
				</group>
				<cfif fileID eq 0>
					<field name="personID" label="phr_files_keywords" control="hidden" default="#request.relayCurrentUser.personID#"></field>
				</cfif>
				<cfif getValidDeliveryMethods.recordCount gt 1>
				<group label="Delivery Options">
					<field name="deliveryMethod" label="phr_files_deliveryMethod" validValues="func:com.fileManager.ValidDeliveryMethods()" displayAs="radio"></field>
				</group>
				</cfif>
				<field name="revision" label="phr_files_revision" description="The file revision" control="html" condition="arguments.entityID is not 0"/>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>

	<!--- Need to show the topHead if this page is not being included by fileSearch, but is called standalone. --->
	<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
		<cfset application.com.request.setTopHead(topHeadcfm="fileTopHead.cfm",pageTitle="#fileGroupHeading# Resources",fileTypeGroupID=fileTypeGroupID)>
	</cfif>
	<cf_includeJavascriptOnce template="/javascript/extExtension.js">

	<cfparam name="fileSearchArgs" default="#structNew()#">
	<cfset fileSearchArgs.returnAdditionalColumns = ",l.language as fileLanguage,case when hasViewRights=1 then '<img src=""/images/icons/report_go.png""/>' else ''end as download,'Download '+f.name as downloadText,case when hasEditRights=1 then '<img src=""/images/misc/iconDeleteCross.gif""/>' else ''end as deleteFile,'Delete '+f.name as deleteText">
	<cfset getFiles = application.com.filemanager.getFilesForFileManagerListing(personID=request.relayCurrentUser.personID,orderby=sortOrder,argumentCollection=fileSearchArgs)>

	<cfset inQOQ=true>

	<cfquery name="getFiles" dbtype="query">
		select * from getFiles
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>

<cfset fileGroupURL = "">
<cfif structKeyExists(url,"fileTypeGroupID") and isNumeric(url.fileTypeGroupID)>
	<cfset fileTypeGroupID = url.fileTypeGroupID>
</cfif>

<cfif isDefined("fileTypeGroupID") and fileTypeGroupID neq 0>
	<cfset fileGroupURL = "&campaignID=#fileTypeGroupID#">
</cfif>

<cfset fileTitleURL="/fileManagement/files.cfm?editor=yes&hideBackButton=true&fileID=##jsStringFormat(fileID)###fileGroupURL#">
<cfset extraTabOptions = "">

<!--- NJH 2013/12/20 - bit of a hack, I will admit...but when opening up userGuides, the loading icon persists, so have decided not to show it. Ideally, need to find out why
	it's hanging around..that is a TODO: --->
<cfif not fileEdit>
	<cfset fileTitleURL="##jsStringFormat(fileNameAndPath)##">
	<cfset extraTabOptions = "*commashowLoadingIcon:false">
</cfif>


<!--- WAB 2016-03-02 CASE 448308 Add encryption of fileID on the delete link --->
<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="files"
	keyColumnList="name,download,deleteFile"
	keyColumnKeyList=" , , "
	keyColumnURLList=" , , "
	keyColumnOnClickList="openNewTab('File_##jsStringFormat(fileID)##'*comma'##jsStringFormat(name)## (##jsStringFormat(fileID)##)'*comma'#fileTitleURL#'*comma{reuseTab:true*commaiconClass:'files'#extraTabOptions#});return false;,javascript:getFile(##jsStringFormat(fileID)##);return false;,javascript:deleteFile('##jsStringFormat(application.com.security.encryptVariableValue(value = fileID*comma name='fileID'))##'*comma'##jsStringFormat(name)##');return false;"
	showTheseColumns="#showTheseColumns#"
	FilterSelectFieldList="name,heading,fileFamilyName,type,source,fileLanguage,OwnerName,expired"
	FilterSelectFieldList2="name,heading,fileFamilyName,type,source,fileLanguage,OwnerName,expired"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getFiles#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	dateFormat="revision"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	booleanFormat="expired"
	preSaveFunction="#application.getObject('filemanagement.com.files').manageFamilyFile#"
	postSaveFunction="#application.getObject('filemanagement.com.files').updateMediaFile#"
	columnTranslationPrefix="phr_report_files_"
	showCellColumnHeadings="true"
	toolTipContentColumnList="fileNameAndPath,downloadText,deleteText"
	toolTipColumnList="name,download,deleteFile"
	rowIdentityColumnName="fileID"
	showTextSearch="false"
	searchTextColumns = "name,fileFamilyName,fileLanguage,type,source,OwnerName,revision,fileID"
	groupByColumns="fileFamilyName"
	allowColumnSorting="true"
	<!--- 2013-04-25	YMA	Case 435852 insert null when file family isnt defined to support foreign keys. --->
	treatEmtpyFieldAsNull="true"
>
