/* �Relayware. All Rights Reserved 2014 */
var urlMessageSpan = 'validateURLMessage';
var nameMessageSpan = 'validateNameMessage';
var fileMessageSpan = 'validateFileMessage';

function populateFileName() {
	var $_name = jQuery('#name');
	
	if ($_name.val() == '') {
		var $_file1 = jQuery('#file1');
		if ($_file1.val() != '') {
	  		var lastSlash // the position of the last slash in the path
			var lastDot // the position of the last dot in the path
			var filename = $_file1.val();
		    lastSlash = filename.lastIndexOf("\\");
			lastDot = filename.lastIndexOf(".");
			$_name.val(filename.substring(lastSlash+1,lastDot));
		} else {
			 $_name.val(jQuery('#url').val());
		}
		
		//when the name has been set, fire the onChange and onblur events...
		$_name.change();
		$_name.blur();
	}
}

function validateUrlOnSubmit() {	
	if (verifyURL('submit') == 'false') {
		return 'Could not find '+ jQuery('#url').val();
	}
}

function validateUrlOnBlur() {
	var $_urlMessageSpan = jQuery('#'+urlMessageSpan);
	var $_url = jQuery('#url');
	
	if ($_url.val() == 'http://') {
		$_url.val('');
	} 
	if ($_url.val() == '') {
		//$('file1').enable();
		
		if ($_urlMessageSpan.length != 0) {
			$_urlMessageSpan.html('');
		}
	} else {
		if ($_urlMessageSpan.length == 0) {
			$_url.after('<span id="'+urlMessageSpan+'"></span>');
		}
		if ($_url.attr('data-isValid') == '') {
			$_urlMessageSpan.html('<img src="/images/icons/loading.gif"/>');
		}
		verifyURL('blur');
	}
}

function verifyURL(validateAt) {
	var result = true;
	var asynchronous = false;
	var $_url = jQuery('#url');
	
	if ($_url.val() != '') {
		var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=fileWS&methodName=testFileURL&returnformat=json';
		var $_urlMessageSpan = jQuery('#'+urlMessageSpan);
		
		if ($_url.attr('data-isValid') == '') {
			// urlMessageSpan will be set in the onblur version.
			if ($_urlMessageSpan.length != 0) {
				$_urlMessageSpan.attr('class','');
			}
			$_url.attr('data-isValid','pending');
		}
		
		if (validateAt == 'blur') {
			asynchronous = true;	
		}
		
		if ($_url.attr('data-isValid') == 'pending') {
			var parameters = {fileurl:$_url.val()};
			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'get',
					parameters: parameters,
					asynchronous: asynchronous,
					onComplete: function (requestObject,JSON) {
						result = requestObject.responseText.evalJSON();
						$_url.attr('data-isValid',result);
	
						if (validateAt == 'blur') {
							if (result) {
								$_urlMessageSpan.html('<img src="/images/icons/accept.png">');
							} else {
								$_urlMessageSpan.html('Oops! Could not find '+ $_url.val());
								$_urlMessageSpan.attr('class','errorblock')
		                    }
						}
	  	            }
			});
		}
	}
	return $_url.attr('data-isValid');
}



function validateFileExtension(validateAt) {
	var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=fileWS&methodName=checkFileExtension&returnformat=json&_cf_nodebug=true'
	var result = true;
	var asynchronous = false;
	
	if (validateAt == 'blur') {
		asynchronous = true;
	}
	
	var $_mediaFile = jQuery('#mediaFile');
	if ($_mediaFile.attr('data-isValid') == '') {
		$_mediaFile.attr('data-isValid','pending');
	}
	
	if ($_mediaFile.val() != '' && $_mediaFile.attr('data-isValid') == 'pending') {
		var parameters = {filename:$_mediaFile.val(),filetypeid:jQuery('#fileTypeID').val()};
		var myAjax = new Ajax.Request(
			page, 
			{
				method: 'get', 
				asynchronous: asynchronous,
				parameters: parameters, 
				onComplete: function (requestObject,JSON) {
					json = requestObject.responseText.evalJSON(true)
					if (!json) {
					} else {
						var $_fileMessageSpan = jQuery('#'+fileMessageSpan);
						if(json.ALLOWED != true) {
							result = false;
							if (validateAt == 'blur') {
								$_fileMessageSpan.html(jQuery('#name').val() + ' '+ phr.sys_file_name_invalid_extension);
								$_fileMessageSpan.attr('class','errorblock');
							}
						} else {
							result = true;
							if (validateAt == 'blur') {
								$_fileMessageSpan.html('<img src="/images/icons/accept.png">');
							}
						}
						$_mediaFile.attr('data-isValid',result);
                   }
               }
           });
	}
	return $_mediaFile.attr('data-isValid');
}

function validateFileExtensionOnSubmit() {

	if (validateFileExtension('submit') == 'false') {
		return phr.sys_file_name_invalid_extension;
	};
}

function validateFileExtensionOnBlur() {
	var $_fileMessageSpan = jQuery('#'+fileMessageSpan);
	var $_mediaFile = jQuery('#mediaFile');
	if ($_fileMessageSpan.length==0) {
		$_mediaFile.after('<span id="'+fileMessageSpan+'"></span>');
	} else {
		if ($_mediaFile.attr('data-isValid') == '') {
			$_fileMessageSpan.html('');
			$_fileMessageSpan.attr('class','');
		}
	}
	
	validateFileExtension('blur');
}

function checkFileUniqueInLibraryOnSubmit() {
	if (checkFileUniqueInLibrary('submit') == 'false') {
		return phr.sys_file_name_already_exists;
	};
}

function checkFileUniqueInLibraryOnBlur() {
	var $_messageSpan = jQuery('#'+nameMessageSpan);
	if ($_messageSpan.length == 0) {
		jQuery('#name').after('<span id="'+nameMessageSpan+'"></span>');
	} else {
		if (jQuery('#name').attr('data-isValid') == '') {
			$_messageSpan.html('');
			$_messageSpan.attr('class','');
		}
	}
	
	checkFileUniqueInLibrary('blur');
}

function checkFileUniqueInLibrary(validateAt) {
	var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=fileWS&methodName=checkFileUnique&returnformat=json&_cf_nodebug=true'
	var result = true;
	var asynchronous = false;
	var $_name = jQuery('#name');
	
	if (validateAt == 'blur') {
		asynchronous = true;
	}
	
	if ($_name.attr('data-isValid') == '') {
		$_name.attr('data-isValid','pending');
	}
	
	if ($_name.val() != '' && $_name.attr('data-isValid') == 'pending') {
		var parameters = {filetitle:$_name.val(),filetypeid:jQuery('#fileTypeID').val(),currentfileid:jQuery('#fileid').val()};
		var myAjax = new Ajax.Request(
			page, 
			{
				method: 'get', 
				asynchronous: asynchronous,
				parameters: parameters, 
				onComplete: function (requestObject,JSON) {
					json = requestObject.responseText.evalJSON(true)
					if (!json) {
					} else {
						var $_nameMessageSpan = jQuery('#'+nameMessageSpan);
						if(json.FILEEXISTS == true) {
							result = false;
							if (validateAt == 'blur') {
								$_nameMessageSpan.html(phr.sys_file_name_already_exists);
								$_nameMessageSpan.attr('class','errorblock');
							}
						} else {
							result = true;
							if (validateAt == 'blur') {
								$_nameMessageSpan.html('<img src="/images/icons/accept.png">');
							}
						}
						$_name.attr('data-isValid',result);
                   }
               }
           });
	}
	return $_name.attr('data-isValid');
}

function getFile(fileID) {
	window.open('/fileManagement/FileGet.cfm?fileID='+fileID,'FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
}

function deleteFile(fileID,filename) {
	if (confirm("Are you sure you want to delete '"+ filename +"'?")) {
		var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=fileWS&methodName=deleteFile&returnformat=json&_cf_nodebug=true'
		
		var myAjax = new Ajax.Request(
			page, 
			{
				method: 'post', 
				asynchronous: false,
				parameters: {fileID:fileID},
				onComplete: function (requestObject,JSON) {
					json = requestObject.responseText.evalJSON(true)

					if(!json.ISOK) {
						alert(json.MESSAGE);
					} else {
						trID = 'fileID='+fileID;
						$(trID).remove();
					}
	           }
	       });
	}
}

/*
function setFileUrlRequired() {
	var urlObj = document.getElementById('url');
	var mediaFileObj = document.getElementById('mediaFile');
	
	if (jQuery('#source_File').prop('checked')) {
		// dont set file to be required if we already have a file associated with the record
		if (!jQuery('#nameAndPath_value').length || jQuery('#nameAndPath_value').html() == '') {
			setValidate(mediaFileObj,true);
		}
		setValidate(urlObj,false);
	} else {
		setValidate(mediaFileObj,false);
		setValidate(urlObj,true);
	}
}*/

function toggleFileUrlDisplay() {
	//setFileUrlRequired();
	jQuery('#mediaFileRow_formgroup').toggle();
	jQuery('#url_formgroup').toggle();
}


function editSelectedValue() {
	if (jQuery('#fileFamilyID option:selected').html() != '') {
		if (!jQuery('#editFileFamilyText').length) {
			jQuery('<input type="text" name="editFileFamilyText" id="editFileFamilyText" placeholder="Enter Family" onblur="setSelectedValue();">').insertAfter('#fileFamilyID');
		}
		jQuery('#editFileFamilyText').val(jQuery('#fileFamilyID option:selected').html());
		jQuery('#fileFamilyID').hide();
		jQuery('#editFileFamilyText').show();
	}
}


function setSelectedValue() {
	if (jQuery.trim(jQuery('#editFileFamilyText').val()) != '') {
		jQuery.trim(jQuery('#fileFamilyID option:selected').html(jQuery('#editFileFamilyText').val()));
	}
	jQuery('#fileFamilyID').show();
	jQuery('#editFileFamilyText').hide();
}

//START:  2014-04-30 NYB Case 439373 added
function populateFileName2 () {
	var fileTypeIDVar = document.getElementById('fileTypeID');
	fileTypeIDVar = fileTypeIDVar.value;
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=fileWS&methodName=getFileTypeDetails&returnFormat=plain',
        	data:{fileTypeID:fileTypeIDVar},
        	dataType:'html',
        	success: function(data,textStatus,jqXHR) {
				fileTypeAutoUnzip = data.trim();  //2014-04-30 NYB Case 439373 added
				var $_replaceAddRow = jQuery('#replaceAddRow_formgroup');
				var $_fileNameAndPath = jQuery('#nameAndPath_value').val();
				if (fileTypeAutoUnzip != 0 && $_fileNameAndPath != "") {
				    	var cFArr = $_fileNameAndPath.split(".");
				    	var cFi = cFArr.length - 1;
				    	var currfileExt = cFArr[cFi].toLowerCase();
				    	var newfileName = jQuery('#mediaFile').val();
				    	var arr = newfileName.split(".");
				    	var i = arr.length - 1;
				    	var newfileExt = arr[i].toLowerCase();
				    	if (currfileExt == "zip" && newfileExt == "zip") {
							$_replaceAddRow.show();
				    	} else {
				    		$_replaceAddRow.hide();
				    	}
				} else {
		    		$_replaceAddRow.hide();
				}
			}
		});
	populateFileName();
}