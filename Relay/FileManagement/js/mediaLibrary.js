	jQuery(document).ready(function() {
		jQuery('#frmClearButton').click(function(){
			jQuery('#fileSearch select option').prop('selected', false);
			jQuery('#fileSearch :input').not(':submit,:button,:hidden,#fileSearch :checkbox, #fileSearch :radio').val('');
			jQuery('#fileSearch :checkbox, #fileSearch :radio').prop('checked',false);
		});
		jQuery('#advButton').click(function(){
			if ( jQuery('.advOnOff').val() == 'in')
				{
					jQuery('.advOnOff').val('out');
					theVal = jQuery('.advOnOff').val();
				}
			else 
				{
					jQuery('.advOnOff').val('in');
				}
		});
		window.onresize=function(){
			var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);			
			if (w <= 768) {
				setCurrentDisplay();
				return;
			}
		};
	});

	function scrollToTop(){
		
		//duration of the top scrolling animation (in ms)
		var scroll_top_duration = 400
		
		  jQuery('body,html').animate({
			scrollTop: jQuery("#searchResults").offset().top ,
			}, scroll_top_duration
		  );
	}
	
	function loadBackToTop(){
		/* -=-=-=-=-=-=--=-=-=-=-=-=--=-=-=
		 AS Back to top 19/08/2016
		-=-=-=-=-=-=--=-=-=-=-=-=--=-=-= */
		// browser window scroll (in pixels) after which the "back to top" link is shown
		var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//grab the "back to top" link
		back_to_top = jQuery('.backToTop');
		//smooth scroll to top
		back_to_top.on('click', function(event){
			event.preventDefault();
			scrollToTop();
		});
	}

	function loadGridColSelector(){
		var jQueryresultsContainer = jQuery('#searchResults');
		var jQuerylistBTN = jQuery('#list'); 
		var jQuerygridBTN = jQuery('#grid');
		// View type change key event listeners
		jQuerylistBTN.on('click', function(event) {
			event.preventDefault();
			setListDisplay(jQueryresultsContainer);
		});
		jQuerygridBTN.on('click', function(event) {
			event.preventDefault();
			setGridDisplay(jQueryresultsContainer);
		});
	}
	
	function setCurrentDisplay(){
		var jQueryresultsContainer = jQuery('#searchResults');
		
		if(jQueryresultsContainer.hasClass('listviewlayout')){
			setListDisplay(jQueryresultsContainer);
		}else{
			setGridDisplay(jQueryresultsContainer);
		}
	}
	
	function setGridDisplay(jQueryresultsContainer){
		jQueryresultsContainer.addClass('gridviewlayout');
		jQueryresultsContainer.removeClass('listviewlayout'); 
		jQuery('.resultsRow').each(function(){
			//when going to grid view, add the bootstrap classes to 'this' from xClass
				jQuery(this).addClass(jQuery(this).attr('xClass').replace(',',''));
				
			jQuery(this).find('.resultContent').each(function(){setGridBSCols(jQuery(this))});
		})
		matchHeight();
	}
	function setListDisplay(jQueryresultsContainer){
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);			
		if (w <= 768) {
			setGridDisplay(jQueryresultsContainer);
			return;
		}
		
		jQueryresultsContainer.removeClass('gridviewlayout'); 
		jQueryresultsContainer.addClass('listviewlayout');
		jQuery('.resultsRow').each(function(){
			//when going to list view, remove the bootstrap classes from 'this'
			jQuery(this).attr('class',function(i, c){
				return c.replace(/(^|\s)col-\S+/g, '');
			});
			
			//jQuery(this).closest('.resultsRow').css('width','auto');
			jQuery(this).find('.resultContent').each(function(){setListBSCols(jQuery(this))});
		})
		unMatchHeight();
		
	}
	
	function setListBSCols(obj){
		//add bootstrap classes from listcols attr
		var listBSCols = obj.attr('listcols');
		obj.addClass('col-xs-'+listBSCols);
	}
	
	function setGridBSCols(obj){
		//remove bootstrap classes
		var listBSCols = obj.attr('listcols');
		obj.removeClass('col-xs-'+listBSCols);
	}
		
	function matchHeight(){
		var maxHeight = 0;
		jQuery(".resultsRow").each(function(){
			if (jQuery(this).height() > maxHeight) { 
				maxHeight = jQuery(this).height(); 
			}
		});
		jQuery(".resultsRow").height(maxHeight);  
	}
		
	function unMatchHeight(){
		jQuery(".resultsRow").each(function(){
			jQuery(".resultsRow").height('auto');  
		});
	}
	