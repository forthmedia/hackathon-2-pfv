<!--- �Relayware. All Rights Reserved 2014 --->
<!---

FileDisplay.cfm  (was fileLinkDialog.cfm)

Purpose		This file is popped up by FileGet.cfm

			It simply provides a link that the user can right click on to get the file.
			This has been tested in IE 6, Netscape 4.5 & 6.1 and shows different text
			depending on the browser.

			NB.  This originally use CFCONTENT but we could not get this to work.

Amendment History

Date			By		Description

07-Nov-2001		CPS		Query back using rightsgroup and not recordrightsgroup
2002-03-25		SWJ		Rewrote it to pop up a window used to download the file
WAB 2006-06-19 removed references to session. browsertype etc and replaced with relaycurrentuser.
NJH 2008/05/28 	In an attempt to consolidate some code, renamed this file fileLinkDialog.cfm to fileDisplay.cfm  and used in filegetv2.cfm
NJH 2008-09-25  elearning 8.1 - if the file is associated with a module (ie. is courseware), then track the user module progress. Insert a new record
				if one doesn't exist, or update the existing one.
WAB 2009/05/27  LID 2237 dealing with secured files.
NJH 2009/07/16	P-FNL069 check that if the variables have come from the url scope, that they have been encrypted.
WAB 2009/09/09  LID 2512 get correct file extension where using secure files
WAB 2009/09/15  implement confirmFieldsHaveBeenEncrypted()
NJH 2010/01/27  LID 2881 - if swf file is secure, get secured file
NJH 2011/02/25	LID 5753 - added a few new file extensions
NYB 2011/06/07	LHID6807 start - replaced 750,550 and 900,400 values passed as window/flash size with maxImgWidth and maxImgHeight
					from fileDetails, derived from new function fileManagement.getFileAttributes
2012-06-20 		PPB 	Case 428792 I have moved functionality from here into eLearningCourses2Level.cfm
STCR2012-07-16	R-REL109 Phase 2 - If the module has no RelayQuiz attached and is set to use SCORM, then include SCORM api frame.
2012-10-10		IH Case 431158 fix pop-up window size
2012/11/21		NJH	 CASE 432093 - set the moduleDiv class when completing a course
2013/01/10		NJH 2013Roadmap - provide support for opening a 'url'
2013-01-11 		PPB	Case 433158 added mp4 to list of file types so that they launch directly rather than displaying a anchor to click
2013/05/23		NJH Case 435293 - make a specific call to terminate the scorm as we cant not always be guaranteed that the scorm will do that
2014/11/05		DXC 442176 - if file is secure, get secured file (Note - reverted below)
2014-11-03		AHL Case 439407 Shields on Modules not Changing Colour as Expected
2015-04-22		WAB CASE 444334 revert 442176.  Secure files were being exposed to non-logged in (but known) user.
2015-06-05 		WAB CASE 444587 Alterations to SCORM courseware Iframe.  Was not showing 100% height
2015-08-05		DXC	Case 445458 - replacing # with %23 in filenames because adaptive were using # in filenames and it was causing problems...
2015-09-22		DXC Case 445924 - Added height:100% style attribute for html selector
2016-10-10		WAB Added some trims to protect against trailing spaces in URL
2016-11-23  DCC case 448581 external url links not passing
2016-09-06              DCC Case 451392 - scorm elearning not opening due to custom file directly accessing page take out sessionKey Check it gets set in the relayScorm included file

Things still to do:
1.  Implement language management (particluarly for messages)

--->
<cfparam name="fileMethod" default="open">
<cfparam name="filePath" type="string" default="">
<cfparam name="fileName" type="string" default="">

<cfparam name="userModuleProgressID" type="numeric" default="0">
<cfparam name="moduleID" type="numeric" default="0">
<cfparam name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
<cfparam name="resizeWindow" type="boolean" default="true">

<cfparam name="maxImgHeight" type="numeric" default="#application.com.settings.getSetting('files.maxPopupHeight')#">
<cfparam name="maxImgWidth" type="numeric" default="#application.com.settings.getSetting('files.maxPopupWidth')#">

<!--- NJH 2009/07/16 P-FNL069 if the variables have come from the url scope, then check that they were encrypted --->
<cfif not application.com.security.confirmFieldsHaveBeenEncrypted("fileMethod,filePath,fileName,userModuleProgressID,moduleID,personID")>
	<CF_ABORT>
</cfif>


<!--- P-FNL069 - if the params weren't passed in, then stop. --->
<cfif filePath eq "" and fileName eq "">
	<CF_ABORT>
</cfif>

<cf_head>
	<cf_title><cfoutput>#fileName#</cfoutput></cf_title>
</cf_head>

<!---
2012-06-20 PPB Case 428792 I have moved this functionality to fire onClick of the 'Take Course Now' link in eLearningCourses2Level.cfm cos it would never be hit here for Akamai-hosted files
I have reproduced the code to insertPersonModuleProgress but made no attempt to do the updateUserModuleProgress (I checked on Lenovo and it didn't seem to be used - not even the last updated date - probably code existes for when a quiz is re-visited and it was copied here)

<!--- NJh 2008/09/24 elearning 8.1 - when viewing courseware for a module, track the user module progress --->
<cfif userModuleProgressID neq 0>
	<cfscript>
		argumentsStruct = structNew();
		argumentsStruct.frmLastVisited=now();
		application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=userModuleProgressID,argumentsStruct=argumentsStruct);
	</cfscript>
<cfelseif moduleID neq 0>
	<cfscript>
		userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=personID);
	</cfscript>
</cfif>

 --->


	<cfoutput>
		<cfif fileMethod eq "open">
			<!--- WAB 2009/09/09 bit below to get the extension did not work because in some instances the filename of a secure file has been passed with the .cfm on it, so will remove that from the filename (which is then used as the link)--->
			<cfif listlen(fileName,".") gte 3 and listlast(fileName,".") is "cfm">
				<cfset fileName = replaceNoCase(filename,".cfm","","ONCE")>
			</cfif>
			<cfset extension = listlast(fileName,".")><!--- WAB 2009/05/27 LID 2237 use the filename rather than the path (which might have .cfm added to it because is is a secure file) --->

		<cfelse>
			<cfset extension = "">
		</cfif>
		<cfset theurl = replace(trim(filePath)," ","%20","ALL")>
		<cfset theurl = replace(theurl,"##","%23","ALL")><!--- DXC Case 445458 --->

		<!--- 2011/06/07 NYB LHID6807 start --->
		<cfif isdefined("fileid")>
			<cfset fileDetails = application.com.fileManager.getFileAttributes(fileID=fileID,maxImgHeight=maxImgHeight,maxImgWidth=maxImgWidth)>
		<cfelse>
			<cfset x = findnocase("Content",filePath)>
			<CFSET physicalLoc="">
			<cfif x neq 0>
				<CFSET physicalLoc = "#application.paths.content##replacenocase(Right(filePath, len(filePath)-(x+6)),'/','\','all')#">
			</cfif>
			<cfset fileDetails = application.com.fileManager.getFileAttributes(physicalLocation=physicalLoc,maxImgHeight=maxImgHeight,maxImgWidth=maxImgWidth)>
		</cfif>

		<cfset passDecidedByCourseware = false>

		<cfif not request.relayCurrentUser.isInternal>
			<cfif isNumeric(userModuleProgressID) and userModuleProgressID neq 0>
				<cfset moduleInfo = application.com.relayElearning.GetModuleFromProgress(userModuleProgressID=userModuleProgressID)>
			<cfelseif isNumeric(moduleID) and moduleID neq 0 and isNumeric(personID)>
				<cfset userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=personID)>
				<cfset moduleInfo = application.com.relayElearning.GetModuleFromProgress(userModuleProgressID=userModuleProgressID)>
			</cfif>

			<cfif isDefined("moduleInfo.passDecidedBy") and moduleInfo.passDecidedBy is "Courseware" and isDefined("moduleInfo.isSCORM") and moduleInfo.isSCORM eq 0>
				<cfset passDecidedByCourseware = true>
			</cfif>
		</cfif>
		<!--- 2011/06/07 NYB LHID6807 end --->

		<style>html{height:100%}</style><!--- dxc case 445924 --->
		<script language="javascript">
			function resizeWindow(newWindowWidth,newWindowHeight) {
				window.resizeTo(newWindowWidth,newWindowHeight);

				if (window.innerWidth == undefined) {
					if (document.documentElement.clientWidth == undefined) {
						if (document.body.clientWidth != undefined) {
							newWindowWidth=newWindowWidth+(newWindowWidth-document.body.clientWidth);
						}
					} else {
						newWindowWidth=newWindowWidth+(newWindowWidth-document.documentElement.clientWidth);
					}
				} else {
					newWindowWidth=newWindowWidth+(newWindowWidth-window.innerWidth);
				}

				if (window.innerHeight== undefined) {
					if (document.documentElement.clientHeight == undefined) {
						if (document.body.clientHeight != undefined) {
							newWindowHeight=newWindowHeight+(newWindowHeight-document.body.clientHeight);
						}
					} else {
						newWindowHeight=newWindowHeight+(newWindowHeight-document.documentElement.clientHeight);
					}
				} else {
					newWindowHeight=newWindowHeight+(newWindowHeight-window.innerHeight);
				}
				window.resizeTo(newWindowWidth,newWindowHeight);
			}
		</script>

		<cfswitch expression="#extension#">

			<cfcase value="swf">
				<cfif passDecidedByCourseware>
					<cfset application.com.relayElearning.setModuleComplete(userModuleProgressId=userModuleProgressId)>
				</cfif>
				<!--- 2011/06/07 NYB LHID6807 start - replaced 750,550
				with #fileDetails.maxImgWidth#,#fileDetails.maxImgHeight#
				&: added additional testing, then a resize, to make sure fileDetails.maxImgWidth & fileDetails.maxImgHeight
				are applied to the content area --->
				<script>
					resizeWindow(#jsStringFormat(fileDetails.maxImgWidth)#,#jsStringFormat(fileDetails.maxImgHeight)#);

					<!--- NJH 2012/11/21 Case 432093 --->
					<cfif passDecidedByCourseware>
						window.opener.refreshModuleStatus(#moduleInfo.moduleID#,'completed');
					</cfif>
				</script>

				<!--- NJH 2010/01/27 LID 2881 - if file is secure, get secured file --->
				<cfif (isDefined("getFile") and getFile.secure)>
					<cfset securedFileResult = application.com.fileManager.getSecuredFile(fileID=fileID)>
					<cfset filePath = securedFileResult.relativeURL>
				</cfif>

				<!--- 2011/06/07 NYB LHID6807 start - replaced 900 & 400 with #fileDetails.maxImgWidth# & #fileDetails.maxImgHeight# --->
				<cfset params = "classid=clsid:d27cdb6e-ae6d-11cf-96b8-444553540000#application.delim1#
					codebase=http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=7,0,0,0#application.delim1#
					width=#fileDetails.maxImgWidth##application.delim1#
					height=#fileDetails.maxImgHeight##application.delim1#
					id=DynamicBanner#application.delim1#
					align=middle#application.delim1#
					quality=high#application.delim1#
					autostart=true#application.delim1#
					loop=false#application.delim1#
					bgcolor=##ffffff#application.delim1#
					name=DynamicBanner#application.delim1#
					allowscriptaccess=sameDomain#application.delim1#
					pluginspage=http://www.macromedia.com/go/getflashplayer#application.delim1#
					type=application/x-shockwave-flash#application.delim1#
					pluginspage=http://www.macromedia.com/go/getflashplayer">
				<!--- 2011/06/07 NYB LHID6807 end --->

				<!--- NJH 2015/08/03 urldecode filepath passed in, as the . in the file extension was being encoded --->
				<cfoutput>#application.com.RelayActiveContent.renderActiveContent(URLDecode(filePath),params)#</cfoutput>
			</cfcase>

			<cfcase value="pdf,doc,docx,txt,htm,html,jpg,jpeg,png,gif,ppt,pps,pptx,ppsx,mp4,m4v,zip,mp3,xml,xls,xlsx">	<!--- 2013-01-11 PPB Case 433158 added mp4 to list ---><!--- 2013/12/12 GCC 437759 added m4v to the list --->
				<cfif passDecidedByCourseware>
					<cfset application.com.relayElearning.setModuleComplete(userModuleProgressId=userModuleProgressId, status="Passed")> <!--- 2014-11-03 AHL Case 439407 Shields on Modules not Changing Colour as Expected --->
				</cfif>
				<script>
					<!--- 2011/06/07 NYB LHID6807 start - replaced 750,550 with #fileDetails.maxImgWidth#,#fileDetails.maxImgHeight# --->
					<cfif resizeWindow>
						<cfif fileDetails.isImageFile>
							resizeWindow(#jsStringFormat(fileDetails.maxImgWidth)#,#jsStringFormat(fileDetails.maxImgHeight)#);
						<cfelse>
							resizeWindow(#jsStringFormat(maxImgWidth)#,#jsStringFormat(maxImgHeight)#);
						</cfif>
					</cfif>

					<!--- BEGIN STCR P-REL109 Phase 2 SCORM API --->
					<cfif userModuleProgressID neq 0>
						<cfset getModuleInfo = application.com.relayElearning.GetModuleFromProgress(userModuleProgressID=userModuleProgressID) />
						<cfif getModuleInfo.recordCount gt 0 and getModuleInfo.isSCORM eq 1 <!---and getModuleInfo.QuizDetailID eq 0 ---> >
							<!--- STCR moved the isSCORM restriction to tump setting only - still load SCORM API even if there is a RelayQuiz --->

							<!--- <cfif isDefined("session.elearning.scorm.SCORMSessionKey")>---> <!--- DCC 2016-09-05 case 451392 problem isDefined("session.elearning.scorm.SCORMSessionKey" breaking filedisplay--->

								<!--- NJH 2012/11/22 Case 431933 - we wanted to refresh the module status when the user finished the course/quiz.. when the course is SCORM, we hand control off to the coursware, so
								 we call the courseware in an iframe so that we still have control of the window, and therefore can fresh the parent page when this window is closed. --->

								<!--- WAB 2015-06-05 CASE 444587 Iframe not showing at 100% height.  Removing the doc Type and adding a 100% height to the body appears to solve the problem, but don't know whether it is a good solution, also won't work if debug is shown  --->
								<cfset request.document.docType = "">
								<cf_body style="margin:0px;padding:0px;overflow:hidden;height:100%">
						 		<cfsetting showdebugoutput="false">

								<cf_includeonce template="/eLearning/relaySCORM.cfm" />

								<!--- Case 435534 NJH 2013/06/03 - make the iframe resizable within the parent window --->
								function createCoursewareIframe() {
								   ifrm = document.createElement('IFRAME');
								   ifrm.setAttribute('id','courseware');
								   ifrm.setAttribute('src', '#jsStringFormat(theurl)#');
								   ifrm.style.width = '100%';
								   ifrm.style.height = '100%';
								   ifrm.style.position = 'absolute';
								   ifrm.style.top = 0;
								   ifrm.style.left = 0;
								   ifrm.style.bottom = 0;
								   ifrm.style.right = 0;
								   document.body.appendChild(ifrm);
								}


								jQuery(window).bind('beforeunload', function() {
									<!--- NJH 2013/05/23 - case 435293 - make a specific call to terminate the scorm as we cant not always be guaranteed that the scorm will do that --->
									API_1484_11.sessionKey = '#jsStringFormat(session.elearning.scorm.SCORMSessionKey)#'; // set the sessionKey as it may not have been set??
									API_1484_11.Terminate();
									window.opener.refreshModuleStatus(#jsStringFormat(moduleInfo.moduleID)#,'',#jsStringFormat(userModuleProgressID)#);
								});

								<cfif isDefined("session.elearning.scorm.currentUserModuleProgressID") and isNumeric(session.elearning.scorm.currentUserModuleProgressID) and session.elearning.scorm.currentUserModuleProgressID gt 0>
									alert('#jsStringFormat(application.com.relayTranslations.translatePhrase(phrase="phr_eLearning_onlyOneSCO"))#');
									jQuery(window).unbind("beforeunload"); <!--- don't call the beforeunload load, as we're not loading the courseware --->
									this.close();
								<cfelse>
									<cfset session.elearning.scorm.currentUserModuleProgressID = userModuleProgressID />
									<cfset session.elearning.scorm.currentModuleID = getModuleInfo.moduleID />
									<cfset session.elearning.scorm.currentFileID = getModuleInfo.fileID />
									createCoursewareIframe();
								</cfif>
							<!---  <cfelse>
							console.log("no session key defined");
								/* Can we reach here without SCORMSessionKey being set? */
							</cfif> DCC CASE 451392 renove restriction closing if --->

						<cfelse>
							<!--- NJH 2012/11/21 Case 432093 --->
							<cfif passDecidedByCourseware>
								window.opener.refreshModuleStatus(#moduleInfo.moduleID#,'completed');
							</cfif>
							<!--- No module or the module is not scorm --->
							window.location.href='#jsStringFormat(theurl)#';
						</cfif>
					<cfelse>
						<!--- No userModuleProgressID --->
						window.location.href='#jsStringFormat(theurl)#';
					</cfif>
				</script>
				<!--- END STCR P-REL109 Phase 2 SCORM API --->
			</cfcase>

<!---
		WAB 2008/06/23 when files displayFile and filelinkdialog were consolidated I noticed that in displayFile fileMethod
						images were dealt with as below (ie they were displayed rather than the window being reloaded)
						i thought that I would save the details here in a comment, just incase it was important
				<cfcase value="jpg,gif,peg,png">
						<img src="#filePath#" alt="" border="0">
				</cfcase>
 --->

			<cfdefaultcase>
				<!--- WAB 2015-04-22 CASE 444334  revert DXC change of 2014/11/05 442176 which was exposing secured files even when person logged out --->
				<!--- NJH 2013/01/10 - 2013 Roadmap - add support for file url.. need some work yet to handle courseware that points to external sites. --->
				<!--- case 437759 NJH 2013/11/12 - check that getFile is defined
					NJH 2015/08/12 - if viewing a you tube video in a fancy box iframe, the regExp below enables it to work...
				 --->
				<cfif isDefined("getFile") and getFile.source eq "url">
				<!--- DCC 2016-11-23 case 448581 added for url files --->
				<cfif passDecidedByCourseware>
					<cfset application.com.relayElearning.setModuleComplete(userModuleProgressId=userModuleProgressId)>
				</cfif>
				<!--- DCC 2016-11-23 added for url files --->
					<script>
						resizeWindow(800,800);
						window.location.href='#trim(filePath)#'<cfif findNoCase("youtube.",filePath)>.replace(new RegExp('watch\\?v=([a-z0-9\_\-]+)(&|\\?)?(.*)', 'i'), 'embed/$1?version=3&$3')</cfif>;
					</script>
				<cfelse>
					<TABLE WIDTH="70%" BORDER="0" CELLSPACING="3" CELLPADDING="3" ALIGN="center">
					<TR>
						<TD>
						<cfif request.relaycurrentuser.browser.type is "MSIE" and request.relaycurrentuser.browser.Version GTE "4" and request.relaycurrentuser.browser.OS is not "MAC">
							<!---  code for InterNet Explorer --->
							Phr_FM_MSIE_downloadtext
						<cfelseif request.relaycurrentuser.browser.Type is "NS" and request.relaycurrentuser.browser.Version GTE "3.02">
							<!---  code for Netscape --->
							Phr_FM_NS_Downloadtext
						<cfelse>
							<!--- all other browsers --->
							Phr_FM_Other_Downloadtext
						</cfif>
						</TD>
					</TR>
					<TR>
						<TD><A HREF="#filePath#"<cfif passDecidedByCourseware> onClick="updateUserModuleProgress(userModuleProgressID=#userModuleProgressID#,moduleID=#moduleID#)"</cfif>>#htmleditformat(fileName)#</A></TD>
					</TR>
					</TABLE>
				</cfif>
				<cfif passDecidedByCourseware>
					<cfoutput>

						<script>
							function updateUserModuleProgress(userModuleProgressID,moduleID) {
								var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false'
								var page = page + '&webservicename=relayElearningWS&methodname=updateModuleStatus'
								var parameters = {userModuleProgressID:userModuleProgressID,moduleID:moduleID}

								var myAjax = new Ajax.Request(
			                        page,
			                        {
			                            method: 'post',
			                            parameters: parameters
			                        });

			                    <!--- NJH 2012/11/21 Case 432093 --->
				                window.opener.refreshModuleStatus(#moduleInfo.moduleID#,'completed');
							}
						</script>
					</cfoutput>
				</cfif>

			</cfdefaultcase>
		</cfswitch>

	</cfoutput>