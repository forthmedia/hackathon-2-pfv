<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="FileTypeHeading" default="ELearning_CourseWare">

<cf_title>#FileTypeHeading#</cf_title>

<cfquery name="getFileTypeGroupID" datasource="#application.siteDataSource#">
	SELECT fileTypeGroupID FROM filetypegroup
		where [heading] =  <cf_queryparam value="#FileTypeHeading#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfif getFileTypeGroupID.recordCount gt 0>
	<cfset fileTypeGroupID = getFileTypeGroupID.fileTypeGroupID>
<cfelse>
	<cfoutput>#htmlEditFormat(FileTypeHeading)# Media Group not found</cfoutput>
</cfif>

<cfset url.search=true>
<cfparam name="url.searchString" default="">
<cfparam name="showAdvancedSearch" default="true">
<cfparam name="fileEdit" default="true">

<cfset url.fileTypeGroupID = fileTypeGroupID>

<cfset application.com.request.setTopHead(topHead="fileTopHead.cfm")>

<cfinclude template="/filemanagement/applicationFileManagerIni.cfm">
<cfinclude template="/filemanagement/fileSearch.cfm">