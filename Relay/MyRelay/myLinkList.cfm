<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	Created by NJH 2006/07/07
	
	parameters: mode (display, delete, add) 
	
	My Links functionality which includes displaying user links, deleting them, and adding to them
	
	Amendment History:
	2008-06-25	NYF 	T10 Project - replaced "#UCase(myLinks.linkType)#" 
					with "#UCase(application.com.relayTranslations.translatePhrase(phrase=myLinks.linkType))#"
					in the <cf_divOpenClose 
	2008/07/18 NJH	T-10 Bug Fix issue 803. Filtered reports to include only thost that were in live modules. Added a new ordering to the qoq.
	2010/08/05	NJH	RW8.3 code clean up... use xml file loaded into application, rather than reading it again.
	2012-07-09 WAB Added support for Jasper Reports
 --->

<cf_translate>

<cfparam name="mode" default="display">

<cfif url.mode eq "delete" and StructKeyExists(url,"linkID")>

	<cfquery name="RemoveFromMyLinks" datasource="#application.sitedatasource#">
		DELETE FROM UserLink WHERE linkID =  <cf_queryparam value="#url.linkID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>

<cfelseif url.mode eq "add">

	
	
</cfif>

<cfset myLinks = application.com.myrelay.getMyLinks(personID=request.relayCurrentUser.personID)>
<!--- <cfset linkURLs = ValueList(myLinks.linkLocation)> --->

<cf_head>
	<cf_title>Report List</cf_title>
	<cf_includejavascriptonce template = "/javascript/openwin.js">	
</cf_head>

<cfset request.relayFormDisplayStyle = "HTML-table">
<cfset tableWidth="100%">
<cf_relayFormDisplay formWidth="#tableWidth#">
	
	<CF_RelayNavMenu pageTitle="phr_sys_myLinks_myLinks" thisDir="myRelay">
		<CF_RelayNavMenuItem htmlFormat="false" description="Hide this panel" MenuItemText="<img src='/images/MISC/close_icon.gif' alt='Hide this panel' border='0'>" CFTemplate="javascript:parent.document.body.cols = '0,*';">
	</CF_RelayNavMenu>
		
	<!--- Display and delete mode --->
	<cfif listfindnocase("display,delete",url.mode)>
		<cfform name="addLinkForm" action="myLinkList.cfm?mode=add" method="POST">
		<!--- <cf_relayFormElementDisplay relayFormElementType="submit" label="" currentValue="phr_sys_myLinks_AddNewLink" fieldName="frmAddLinkSubmit" spanCols="yes"> --->
		
		<cfif myLinks.RecordCount gt 0>
			<cfset prevType = "">
			<cfoutput query="myLinks" group="linkAttribute1">
				<!--- NJH 2012/08/16 - if a jasper report, then the attribute is the module name, which needs translating --->
				<tr id="#myLinks.linkType#" ><th colspan="2"><cfif linkType eq "jasperReport">phr_mod_</cfif>#htmleditformat(linkAttribute1)#</th></tr>
				<cfoutput>
					<TR id="#myLinks.linkType#" <CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
						<td valign="top"><a href="myLinkList.cfm?mode=delete&linkID=#myLinks.linkID#" title="phr_sys_myLinks_RemoveFromMyLinks" class="isFavorite"></a></td>
						<cfswitch expression="#linkType#">
							<cfcase value="jasperReport">
								<cfset theURL = application.com.jasperserver.getJasperServerReportUnitPath() & linkLocation>
							</cfcase>
							<cfdefaultcase>
								<cfset theURL = myLinks.linkLocation>
							</cfdefaultcase>
						</cfswitch>
						<td><A HREF="#theURL#" TARGET="ReportArea" CLASS="smallLink">#htmleditformat(myLinks.linkDescription)#</A></td>
					</tr>
				</cfoutput>
			</cfoutput>
		</cfif>
		</cfform>
	
	<!--- Add mode --->
	<cfelseif url.mode eq "add">
	
	  	<!--- <cfform name="mainForm" action="myLinkList.cfm?mode=add" method="POST">
		
 		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" spanCols="yes" valueAlign="left">
		<CF_TwoSelectsRelated
			QUERY="GetLinkGroups"
			NAME1="frmLinkType"
			NAME2="frmLinkTypeGroup"
			VALUE1="LinkType"
			VALUE2="LinkGroupValue"
			DISPLAY1="LinkTypeName"
			DISPLAY2="LinkGroupDisplay"
			SELECTED1="#IIF(isDefined('form.frmLinkType'),'form.frmLinkType',DE(''))#"
			SELECTED2="#IIF(isDefined('form.frmLinkTypeGroup'),'form.frmLinkTypeGroup',DE(''))#"
			MULTIPLE3="No"
			FORMNAME = "mainForm"
			ONCHANGE = "document.mainForm.submit();">
		</cf_relayFormElementDisplay>
		
<!--- 		<TR> 
      		<TH colspan="2" STYLE="BORDER: D5D5CA 1pt solid;">Name (click for analysis)</TH>
    	</TR> --->
				
		<cfif isDefined("frmLinkTypeGroup")>
			<cfquery name="getGroupNameFromID" dbtype="query">
				select linkGroupDisplay from GetLinkGroups where linkGroupValue = '#frmLinkTypeGroup#'
			</cfquery>
			
			<cfswitch expression="#frmLinkType#">
			
			<!--- Add code here when adding a new link type --->
			
			<!--- PROFILES --->
				<cfcase value="Profile">
					<cfset userCountries = application.com.profileManagement.getCountryGroups()>
					<cfset profileFlags = application.com.profileManagement.getProfileFlagGroups(EntityTypeID=#frmLinkTypeGroup#,UserCountryList=#userCountries#)>
				    <cfoutput query="profileFlags" GROUP="grouping"> 
				    	<cfset thisFlagGroup = FlagGroupID>
				     	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> 
							<CFIF profileFlags.flag_count gt 0>
								<td valign="top">
									<cfif not listFindNoCase(linkURLs,"/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")>&nbsp;<a href="javascript: void(openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#&linkName=#Name#&linkGroup=#getGroupNameFromID.linkGroupDisplay#&linkType=Profile','','width=500,height=100,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
								</td><td>
									<B><A HREF="/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#" target="ReportArea" CLASS="smallLink">#HTMLEditFormat(Name)#</A></B>
								</td>
 							<CFELSE>
								<!--- NJH 2008/07/18 added an extra td so that the names were aligned --->
								<td>&nbsp;</td><td>#HTMLEditFormat(Name)#</td>
						    </CFIF>
						</tr>
			      		<cfoutput> 
				      	<!--- NJH 2008/07/16 Bug Fix T-10 Issue 671 - added 'and flagGroupID neq thisFlagGroup' so that we don't display duplicates--->
				        <CFIF profileFlags.ParentFlagGroupID IS NOT 0 and flagGroupID neq thisFlagGroup>
				          	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> 
								<CFIF profileFlags.flag_count gt 0>
									<td valign="top">
										<cfif not listFindNoCase(linkURLs,"/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#")>&nbsp;<a href="javascript: void(openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#&linkName=#Name#&linkGroup=#getGroupNameFromID.linkGroupDisplay#&linkType=Profile','','width=500,height=100,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
									</td><td>
										<a href="/myRelay/myProfileLinkFrame.cfm?FlagGroupID=#FlagGroupID#" target="ReportArea" class="smallLink">#HTMLEditFormat(Name)#</a></B>
									</td>
 								<CFELSE>
									<!--- NJH 2008/07/18 added an extra td so that the names were aligned --->
									<td>&nbsp;</td><td>#HTMLEditFormat(Name)#</td>
					            </CFIF>
							</tr>
						</cfif>
						</cfoutput>
					</cfoutput>
				</cfcase>
				
			<!--- REPORTS --->
				<cfcase value="report">
					
					<cfoutput query="qryReports" group="grouping">
						<tr><th colspan="2">#htmleditformat(grouping)#</th></tr>
						<cfoutput>
						<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> 
							<td valign="top">
								<cfif not listFindNoCase(linkURLs,"/#qryReports.URL#")>&nbsp;<a href="javascript: void(openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/#qryReports.URL#&linkName=#qryReports.Name#&linkGroup=#frmLinkTypeGroup#&linkType=Report','','width=500,height=100,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
							</td><td>
								<B><A HREF="/#qryReports.URL#" target="ReportArea" CLASS="smallLink">#htmleditformat(qryReports.Name)#</A></B>
							</td>
						</tr>
						</cfoutput>
					</cfoutput>
					
					<cfif ArrayLen(selectedElements) gt 0>
						<cfoutput>
							<tr><th colspan="2">&nbsp;</th></tr>
							<cfloop index="i" from="1" to="#ArrayLen(selectedElements)#" step="1">
								<cfset reportSecurityLevel = "reportTask:Level1">
								<cfif structKeyExists(selectedElements[i].XmlAttributes,"Security")>
								<cfset reportSecurityLevel = selectedElements[i].XmlAttributes.Security>
								</cfif>
								<cfif application.com.login.checkInternalPermissions(listfirst(reportSecurityLevel,":"),listlast(reportSecurityLevel,":"))>
									<TR<CFIF i MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
										<td valign="top">
											<cfif not listFindNoCase(linkURLs,"/report/reportXMLGeneric.cfm?reportName=#selectedElements[i].XmlAttributes.name#")><a href="javascript: void(openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/report/reportXMLGeneric.cfm?reportName=#selectedElements[i].XmlAttributes.Name#&linkName=#selectedElements[i].XmlAttributes.Title#&linkGroup=#frmLinkTypeGroup#&linkType=report','','width=500,height=100,toolbar=no,titlebar=no,resizeable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
										</td><td>
											<A HREF="/report/reportXMLGeneric.cfm?reportName=#selectedElements[i].XmlAttributes.name#" TARGET="ReportArea" CLASS="smallLink">#selectedElements[i].XmlAttributes.title#</A><br>
										</td>
									</tr>
								</cfif>
							</cfloop>
						</cfoutput>
					</cfif>
					
					<!--- client specific reports --->
					<cfif qryClientReports.recordCount gt 0>
						<cfoutput query="qryClientReports" group="grouping">
							<tr><th colspan="2">#htmleditformat(grouping)#</th></tr>
							<cfoutput>
							<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> 
								<td valign="top">
									<cfif not listFindNoCase(linkURLs,"/#qryClientReports.URL#")>&nbsp;<a href="javascript: newWindow = openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/#qryClientReports.URL#&linkName=#qryClientReports.Name#&linkGroup=#frmLinkTypeGroup#&linkType=Report','','width=500,height=100,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no')" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
								</td><td>
									<B><A HREF="/#qryClientReports.URL#" target="ReportArea" CLASS="smallLink">#htmleditformat(qryClientReports.Name)#</A></B>
								</td>
							</tr>
							</cfoutput>
						</cfoutput>
					</cfif>

				</cfcase>
				
				<cfcase value="crosstab">
					<cfoutput query="getReportDefinition">
						<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>> 
							<td valign="top">
								<cfif not listFindNoCase(linkURLs,"/report/crossTabLibrary.cfm?reportCrossTabID=#getReportDefinition.reportCrossTabID#")>&nbsp;<a href="javascript: void(openWin('/myRelay/updateMyLinks.cfm?task=add&linkLocation=/report/crossTabLibrary.cfm?reportCrossTabID=#getReportDefinition.reportCrossTabID#&linkName=#getReportDefinition.reportTitle#&linkGroup=#URLEncodedFormat('Cross Tab')#&linkType=crosstab','','width=500,height=100,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_myLinks_AddToMyLinks"><img src="/images/icons/link_add.png" border="0"/></a><cfelse>&nbsp;</cfif>
							</td><td>
								<B><A HREF="/report/crossTabLibrary.cfm?reportCrossTabID=#getReportDefinition.reportCrossTabID#" target="ReportArea" CLASS="smallLink">#htmleditformat(getReportDefinition.reportTitle)#</A></B>
							</td>
						</tr>
					</cfoutput>
				</cfcase>
				
				<cfdefaultcase>
				</cfdefaultcase>
				
			</cfswitch>
			
		</cfif>
		
		<!--- SWJ: 2005-05-13 This is added to allow customer specific reports to be defined in the file below --->
<!--- 		<cfif fileexists("#application.paths.code#\cftemplates\InternalReportListExtension.cfm")>
			<cfinclude template="/code/cftemplates/InternalReportListExtension.cfm">
		</cfif> --->
		</cfform> --->
		
	</cfif>
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" spanCols="yes">
			<cfoutput><img src="/images/misc/Spacer.gif" width="#tableWidth#" height="25" border="0"></cfoutput>
	</cf_relayFormElementDisplay>
	
	<!--- <cfif url.mode eq "add">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="&nbsp;<br><a href='myLinkList.cfm?mode=display'>phr_sys_myLinks_BackToMyLinks</a>" spanCols="yes">
	</cfif> --->
</cf_relayFormDisplay>
	
</cf_translate>

