<!--- �Relayware. All Rights Reserved 2014 --->

<cf_translate>
<cfif structKeyExists(url,"task") and url.task eq "add">
	<CFIF structKeyExists(url,"linkLocation") and structKeyExists(url,"linkName") and structKeyExists(url,"linkGroup") and structKeyExists(url,"linkType")>
		<cfset application.com.myRelay.addLink (linkType = linkType,Location = linkLocation,Description = linkName,attribute1 =linkGroup )>
		<cfset message = "phr_sys_myLinks_MyLinksUpdatedSuccessfully">
				 
	<cfelse>
		<cfset message="Please pass linkLocation, linkName, linkGroup and linkType when adding a link to 'My Links'">
	</cfif>
</cfif>


<cfoutput>
	<script type="text/javascript">
		function returnToReportList() {
			if(!opener.closed){
				if (opener.location.href == "#request.currentSite.protocolAndDomain#/myRelay/myLinkList.cfm?mode=add"){
					opener.location.href='/myRelay/myLinkList.cfm?mode=add&LinkType=#jsStringFormat(linkType)#&LinkTypeGroup=#jsStringFormat(linkGroup)#';
					
				} else {
					opener.location.href=opener.location.href;
				}
			}
			self.close();
		}
	</script>
</cfoutput>


<div align="center">
	<cfoutput>
		#application.com.security.sanitiseHTML(message)#
			
		<cfform name="mainForm">
			<!--- <cfinput type="button" value="phr_sys_myLinks_ReturnToReportList" name="closeButton" onClick="javascript:if(!opener.closed){opener.location.reload(true)};self.close();"> --->
			<cfinput type="button" value="phr_sys_myLinks_ReturnToReportList" name="closeButton" onClick="returnToReportList();">
	
			<cfinput type="button" value="phr_sys_myLinks_GoToMyLinks" name="myLinksButton" onClick="javascript:if(!opener.closed){opener.parent.document.location.href='myLinkFrame.cfm'};self.close();">
		</cfform>
	</cfoutput>
</div>


</cf_translate>