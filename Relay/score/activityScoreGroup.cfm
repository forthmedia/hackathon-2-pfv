<CFPARAM name="url.recordID" default="NewRecord">
<CFPARAM name="url.active" type="boolean" default="true">

<cfif url.recordID NEQ "NewRecord">
	<cf_checkfieldEncryption fieldnames="recordID">
	

</cfif>

<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">

<cfif url.recordID EQ "NewRecord">
	<cfset toggleActiveInActive="off">
	<cfset application.com.request.setTopHead(pageTitle="phr_score_scoreRuleGrouping", queryString = "",topHeadCfm="/relay/score/activityScoreTopNavWithSave.cfm", backButtonURL="activityScoreGroups.cfm")>
<cfelse>
	<cf_checkfieldEncryption fieldnames="recordID">
	<cfset toggleActiveInActive=url.active EQ "true" ?"inactive":"active">
	<cfset application.com.request.setTopHead(pageTitle="phr_score_scoreRuleGrouping",queryString = "",topHeadCfm="/relay/score/activityScoreTopNavWithSave.cfm", addItemTemplate="activityScoreRule.cfm?ruleGroupID=#url.recordID#",addItemTemplateText="phr_score_addRuleToGroup", toggleActiveInActive=toggleActiveInActive, backButtonURL="activityScoreGroups.cfm")>
</cfif>


<script language="javascript">
	function FormSubmit() {
	    triggerFormSubmit(jQuery('#scoreRuleGrouping'));
	}
	function toggleActiveInActive() {
		<cfset encryptedURL=application.com.security.encryptURL("activityScoreGroup.cfm?recordID=#url.recordID#&active=#NOT url.active#")>
	   	<cfoutput>
			window.location.href = "#encryptedURL#";
		</cfoutput>
	}
</script>


<cfscript>
	capppingPeriodData=application.com.activityScore.getCappingPeriodType();	
		
		
	polQuery=new Query();
	polQuery.setDatasource(application.siteDatasource);
	
	polQueryResult=polQuery.execute(sql="select entitytypeid as datavalue, description as displayValue from flagEntityType where entitytypeid>=0 and entitytypeid<3 order by entitytypeid").getResult();
	
	if (structKeyExists(form,"active")){
		//saving data
	
		if (form.active EQ true and application.com.activityScore.getNumberOfActiveGroupings() GTE application.com.settings.getSetting("activityScoring.MaximumPermittedScoreGroupings")){
			//we're up against the limit, this record can only be active if it already was 	
			
			if (url.recordID EQ "NewRecord"){
				wasAlreadyActive=false;
			}else{
				wasAlreadyActive=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.scoreRuleGrouping, entityID=url.recordID, fieldList="active").recordSet.active;
			}
			if (not wasAlreadyActive){
				application.com.relayUI.setMessage(message="phr_score_limitOfGroupingsReached", type="warning");	
				form.active=false;
			}
		}
			
			
	
	
		if (recordID=="NewRecord"){
			groupingData={entityTypeID=form.scoreCollectionLevel,cappingPeriodTypeTextID=form.cappingPeriodTypeTextID,cappingPeriodResetMonthOfYear=form.cappingPeriodResetMonthOfYear,cappingPeriodResetDayOfMonth=form.cappingPeriodResetDayOfMonth, active=form.active};
		
			//save a new record
			// insertIfExists="true" as these are just containers, they may well be identical to existing records, we don't care'
			recordID=application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID.scoreRuleGrouping, entityDetails=groupingData, insertIfExists="true").entityID;
			form.NewRecord = RecordID; // eg newRecord = 1234  - used by ProcessPhraseUpdateForm
			
			//set redirect headers to get us back into the normal flow
			
			currentLocation=request.script_name;
			pc = getpagecontext().getresponse();
			pc.getresponse().setstatus(303); //redirect without post body
					pc.setHeader("Location",application.com.security.encryptURL("#currentLocation#?recordID="&recordID));
			
			
		}else{
			//update an existing
			//Only translations and active are not locked down
			
			if (form.active EQ false and application.com.activityScore.activityGroupingHasActiveRules(url.recordID)){
				application.com.relayUI.setMessage(message="phr_score_cantDisableGroupingWithActiveRules", type="warning");
			}else{
				groupingData={active=form.active};
				result=application.com.relayEntity.updateEntityDetails(entityID=url.recordID, entityTypeID=application.entityTypeID.scoreRuleGrouping, entityDetails=groupingData);
		
			}
			
		}
		
	}
	
	//hitting the page for display
	groupingData={entityTypeID=0,cappingPeriodTypeTextID="allTime",cappingPeriodResetMonthOfYear=1,cappingPeriodResetDayOfMonth=1, active=1};	
	

	if (url.recordID != "NewRecord"){
		groupDetails=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.scoreRuleGrouping, entityID=url.recordID, fieldList="entityTypeID,cappingPeriodTypeTextID,cappingPeriodResetMonthOfYear,cappingPeriodResetDayOfMonth,active");
		groupingData=groupDetails.recordSet;
	}

	//these are for the initial display of the month and year reset, they may later be hidden/shown
	needsMonthAndYearQuery = new Query();
    needsMonthAndYearQuery.setDBType('query');
    needsMonthAndYearQuery.setAttributes(cappingPeriodData=capppingPeriodData); // needed for QoQ
    needsMonthAndYearQuery.addParam(name='cappingPeriodTypeTextID', value=groupingData.cappingPeriodTypeTextID, cfsqltype='cf_sql_varchar');
    needsMonthAndYearQuery.setSQL('SELECT needsMonthOfYear,needsDayOfMonth FROM cappingPeriodData where type = :cappingPeriodTypeTextID');
    needsMonthAndYearResult = needsMonthAndYearQuery.execute().getResult();



	displayForEdit=!(recordID=="NewRecord");


</cfscript>

<form id="scoreRuleGrouping" action="" method="post">
<cf_relayFormDisplay>
	
	<CF_relayFormElementDisplay relayFormElementType="select" required="true" currentValue="" fieldName="" size="1" value="method" display="display" label="phr_score_scoreRuleGrouping_Name" nulltext="" nullValue="">
		<cf_editPhraseWidget entityID ="#recordID#" required="true" entityType = "scoreRuleGrouping" message= "" phraseTextID="title"  showOtherTranslationsTable=false>
	</cf_relayFormElementDisplay>
	
	<cf_relayFormElementDisplay  relayFormElementType="Text" fieldname="groupingDescription" currentValue="" label="phr_score_scoreRuleGrouping_Description" required="no">
		<cf_editPhraseWidget entityID ="#recordID#" entityType = "scoreRuleGrouping" required="false" message= "" phraseTextID="description" showOtherTranslationsTable=false>
	</cf_relayFormElementDisplay>
	
	<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#groupingData.active#" fieldName="active" label="phr_sys_active"  list="1#application.delim1#phr_yes,0#application.delim1#phr_no" >
	
	<CF_relayFormElementDisplay relayFormElementType="select" disabled="#displayForEdit#" currentValue="#groupingData.entityTypeID#" fieldName="scoreCollectionLevel" size="1" query="#polQueryResult#" label="phr_score_scoreCollectedByEntity" nulltext="" nullValue="">
	
	<CF_relayFormElementDisplay relayFormElementType="select" disabled="#displayForEdit#" currentValue="#groupingData.cappingPeriodTypeTextID#" fieldName="cappingPeriodTypeTextID" size="1" query="#application.com.activityScore.getCappingPeriodType()#" label="phr_score_cappingPeriodType" value="type" display="phrase" nulltext="" nullValue="">
	
	<div id="cappingResetMonthDiv"
		<cfif NOT needsMonthAndYearResult.needsMonthOfYear>
			style="display: none;"
		</cfif>
		>
		<CF_relayFormElementDisplay relayFormElementType="select" disabled="#displayForEdit#" currentValue="#groupingData.cappingPeriodResetMonthOfYear#" fieldName="cappingPeriodResetMonthOfYear" size="1" query="#application.com.dateFunctions.getMonthsOfYear()#" label="phr_score_cappingResetMonth" value="data" display="display" nulltext="" nullValue="">
		<!---><cfset request.rjthit=true>--->
		
	</div>
	<div id="cappingResetDayDiv"
		<cfif NOT needsMonthAndYearResult.needsDayOfMonth>
			style="display: none;"
		</cfif>
		>
		
		<cf_relayFormElementDisplay  relayFormElementType="select" disabled="#displayForEdit#" fieldname="cappingPeriodResetDayOfMonth" currentValue="#groupingData.cappingPeriodResetDayOfMonth#" bindOnLoad="true" bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='datesWS',methodName='getDaysInNonLeapYearMonth',monthIndex={cappingPeriodResetMonthOfYear})" label="phr_score_cappingResetDay" value="data" display="display">
		
	</div>
</cf_relayFormDisplay>
</form>



<cfif displayForEdit>
<!--- Include the rules within the group --->
	<cfoutput>
		<h3>#url.active?'phr_sys_active':'phr_sys_inactive'# phr_score_RulesInGroup</h3>
	</cfoutput>
	<cfparam name="sortOrder" default="name">
	
	<cfset phraseSnippetName = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRule", phraseTextID = "name", nullText="scoreRuleGroupingID", baseTableAlias="scoreRule")>

	<cfparam name="SortOrder" default="name">

	<cfquery name="getRulesInGroup">
		
		select 
			scoreRuleVersionID,
			scoreRule.scoreRuleID,
			#phraseSnippetName.select# as name,
			case 
				when nonStandardMethod is null then CAST (scoreValue AS nVarChar )
				ELSE 'phr_score_customRule'
			end as scoreValue,
			case
				when nonStandardMethod is null then cast(scoreCap  AS nVarChar )
				ELSE 'phr_score_customRule'
			end as scoreCap,
			activityType.description as activityTypeDescription
		
		from scoreRuleVersion	
		join scoreRule on scoreRuleVersion.scoreRuleID=scoreRule.scoreRuleID
		join activityType on activityType.activityTypeID=scoreRuleVersion.activityTypeID
		#preserveSingleQuotes(phraseSnippetName.join)# 
		where validTo is null and scoreRule.scoreRuleGroupingID=<cf_queryparam value = "#recordID#" CFSQLType="CF_SQL_INTEGER">
		and scoreRuleVersion.active = <cf_queryparam value = "#url.active#" CFSQLType="CF_SQL_INTEGER">
		order by <cf_queryObjectName value="#sortOrder#">	
	</cfquery>
	
	
	
	<CF_tableFromQueryObject queryObject="#getRulesInGroup#" 
	showTheseColumns="name,scoreValue,scoreCap,activityTypeDescription"
	ColumnHeadingList="score_scoreRuleGrouping_scoreRuleTitle,score_scorePerActivity,score_scoringCap,score_activityType"
	ColumnTranslationPrefix="phr_"
	HidePageControls="no"
	useInclude = "false"
	numRowsPerPage="400"
	keyColumnList="name"
	keyColumnURLList="activityScoreRule.cfm?ruleGroupID=#url.recordID#&ruleID="
	keyColumnKeyList="scoreRuleID"
	columnTranslation="true">

</cfif>


<script language="javascript">
	jQuery(document).ready(function() {
		

		
		var showHideDateFields=function(){
			<cfloop query="#capppingPeriodData#">
				<CFoutput>
				if (jQuery("##cappingPeriodTypeTextID").val()=== '#type#'){
					
					var monthDiv=jQuery("##cappingResetMonthDiv");
					var dayDiv=jQuery("##cappingResetDayDiv");
					
					<cfif needsMonthOfYear>
						monthDiv.show();
					<cfelse>
						monthDiv.hide();	
					</cfif>
					<cfif needsdayOfMonth>
						dayDiv.show();
					<cfelse>
						dayDiv.hide();	
					</cfif>
				}
				</CFoutput>
				
			</cfloop>
			
		}

		//as only user interactions trigger change events we have to bind to all potential change points
		
		jQuery("#cappingPeriodTypeTextID").change(showHideDateFields);
		jQuery('#cappingPeriodTypeTextID').on('rw:ajaxupdate',showHideDateFields)

		showHideDateFields();
		
	});
		

	
</script>

