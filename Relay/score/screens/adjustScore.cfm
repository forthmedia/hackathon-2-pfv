<cfif request.currentSite.isInternal and structKeyExists(url,"adjustScore") AND url.adjustScore>
	<cf_checkfieldEncryption fieldnames="adjustScore">
	<cfif structKeyExists(form,"adjustmentScore")>
		<cfscript>
			entityDetails={scoreRuleGroupingID=scoreRuleGroupingID, score=adjustmentScore, comment=reasonForAdjustment, effectiveDate=dateOfAdjustment};
			
			if (reportEntityTypeID EQ application.entityTypeID.organisation){
				entityDetails.organisationID=reportEntityID;
			}else if (reportEntityTypeID EQ application.entityTypeID.location){
				entityIDDetails=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.location, entityID=reportEntityID, fieldList="organisationID").recordSet;
				entityDetails.locationID=reportEntityID;
				entityDetails.organisationID=entityIDDetails.organisationID;
			}else if (reportEntityTypeID EQ application.entityTypeID.person){
				entityIDDetails=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.person, entityID=reportEntityID, fieldList="organisationID, locationID").recordSet;
				entityDetails.personID=reportEntityID;
				entityDetails.locationID=entityIDDetails.locationID;	
				entityDetails.organisationID=entityIDDetails.organisationID;	
			}
			application.com.relayEntity.insertEntity(EntityTypeID=application.entityTypeID.scoreManualAdjustment, insertIfExists="true",entityDetails=entityDetails);
		
		</cfscript>
		

		<cfset application.com.relayUI.message(message="phr_adjustmentAdded")>
		<cflocation url = "#script_name#?frmcurrententityid=#url.frmcurrententityid#&frmentitytypeid=#url.frmentitytypeid#&frmentityscreen=#url.frmentityscreen#&frmentityid=#url.frmentityid#&scoreRuleGroupingID=#scoreRuleGroupingID#" addToken = "no" statusCode = "303">
		
		
	<cfelse>
	
		<H3>phr_score_manualScoreAdjustment</H3>
		<form id="scoreadjustment" action="" method="post">
			<cf_relayFormDisplay>
				<CF_relayFormElementDisplay relayFormElementType="numeric" currentValue="" fieldName="adjustmentScore" label="phr_score_adjustScoreBy" required="Yes">
				<CF_relayFormElementDisplay relayFormElementType="Date" currentValue="#now()#" fieldName="dateOfAdjustment" label="phr_score_adjustDate" required="Yes">
				<CF_relayFormElementDisplay relayFormElementType="Text" currentValue="" fieldName="reasonForAdjustment" label="phr_score_adjustReason" required="Yes">
				<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="left">
					<CF_relayFormElement relayFormElementType="Submit" fieldname="submit"  currentValue="phr_Submit">
					<CF_relayFormElement relayFormElementType="Submit" fieldname="cancel" currentValue="phr_Cancel" onclick="history.back(-1); return false;">
				</cf_relayFormElementDisplay>	
					
			</cf_relayFormDisplay>		
				
				
		</form>
		
		
	</cfif>
<cfelse>	
	Adjust Score - Internal Only
</cfif>