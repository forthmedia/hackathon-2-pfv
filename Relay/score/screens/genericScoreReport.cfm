<cfparam name="reportEntityTypeID" type="numeric">
<cfparam name="reportEntityID" type="numeric">

<cf_checkfieldEncryption fieldnames="reportEntityID">
<cf_checkfieldEncryption fieldnames="reportEntityTypeID">

<cfif structKeyExists(url,"scoreRuleGroupingID")>
	<!--- A link in the TFQO table has been clicked--->
	<cfinclude template="genericAccountReport.cfm">

	
<cfelse>	
	
	<cfset phraseSnippetTitle = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRuleGrouping", baseTableAlias="scoreRuleGrouping", phraseTextID = "title", phraseTableAlias="pt1")>
	
	<cfquery name ="scoreReport" datasource="#application.SiteDataSource#">
	
	select scoreRuleGroupingID,title,rolledUp,sum(allTime) as allTime,sum(numberOfActivities) as numberOfActivities,viewAccount from
		(
			select 
				scoreRuleGrouping.scoreRuleGroupingID as scoreRuleGroupingID,
				#phraseSnippetTitle.select# as title,
				case when scoreRuleGrouping.entityTypeID=<cf_queryparam value = "#reportEntityTypeID#" CFSQLType="CF_SQL_INTEGER">  then 'phr_no' else 'phr_yes' end as rolledUp,
			SUM(score) as allTime, 
			count(score) as numberOfActivities,
			'phr_viewDetails' as viewAccount
			from 
			score
			join scoreRuleVersion on score.triggeringScoreRuleVersionID=scoreRuleVersion.ScoreRuleVersionID
			join scoreRule on scoreRule.scoreRuleID=scoreRuleVersion.scoreRuleID
			join scoreRuleGrouping on scoreRule.scoreRuleGroupingID=scoreRuleGrouping.scoreRuleGroupingID
			join activity on score.triggeringActivityID=activity.activityID
			#preserveSingleQuotes(phraseSnippetTitle.join)# 
			<cfif reportEntityTypeID EQ application.entityTypeID.person> 
		        where activity.personID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.location> 
		        where activity.locationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.organisation>
		        where activity.organisationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER"> 
		    <cfelse>
		    	<cfthrow message="entityTypeID #reportEntityTypeID# not supported (only Person organisation and location supported)"> 
		    </cfif> 
		    <cfif structKeyExists(form,"FRMFROMDATE") and form.FRMFROMDATE NEQ "">
		    	and activity.activityDate >= <cf_queryparam value = "#form.FRMFROMDATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    <cfif structKeyExists(form,"FRMTODATE")  and form.FRMTODATE NEQ "">
		    	and activity.activityDate <= <cf_queryparam value = "#form.FRMTODATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    and scoreRuleGrouping.entityTypeID <= <cf_queryparam value = "#reportEntityTypeID#" CFSQLType="CF_SQL_INTEGER"> <!--- Eliminate records which are held on a higher level --->
			group by #phraseSnippetTitle.select#,scoreRuleGrouping.scoreRuleGroupingID, scoreRuleGrouping.entityTypeID
		union all
			select 
				scoreRuleGrouping.scoreRuleGroupingID,
				#phraseSnippetTitle.select# as title,
				case when scoreRuleGrouping.entityTypeID=<cf_queryparam value = "#reportEntityTypeID#" CFSQLType="CF_SQL_INTEGER">  then 'phr_no' else 'phr_yes' end as rolledUp,
				SUM(score) as allTime, 
				count(score) as numberOfActivities,
				'phr_viewDetails' as viewAccount
				from scoreManualAdjustment
				join scoreRuleGrouping on scoreManualAdjustment.scoreRuleGroupingID=scoreRuleGrouping.scoreRuleGroupingID
				#preserveSingleQuotes(phraseSnippetTitle.join)# 
				<cfif reportEntityTypeID EQ application.entityTypeID.person> 
			        where scoreManualAdjustment.personID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
			    <cfelseif reportEntityTypeID EQ application.entityTypeID.location> 
			        where scoreManualAdjustment.locationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
			    <cfelseif reportEntityTypeID EQ application.entityTypeID.organisation>
			        where scoreManualAdjustment.organisationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER"> 
			    <cfelse>
			    	<cfthrow message="entityTypeID #reportEntityTypeID# not supported (only Person organisation and location supported)"> 
			    </cfif> 
			    <cfif structKeyExists(form,"FRMFROMDATE") and form.FRMFROMDATE NEQ "">
			    	and effectiveDate >= <cf_queryparam value = "#form.FRMFROMDATE#" CFSQLType="CF_SQL_TIMESTAMP">
			    </cfif>
			    <cfif structKeyExists(form,"FRMTODATE")  and form.FRMTODATE NEQ "">
			    	and effectiveDate <= <cf_queryparam value = "#form.FRMTODATE#" CFSQLType="CF_SQL_TIMESTAMP">
			    </cfif>
			    
			    and scoreRuleGrouping.entityTypeID <= <cf_queryparam value = "#reportEntityTypeID#" CFSQLType="CF_SQL_INTEGER"> <!--- Eliminate records which are held on a higher level --->
				group by #phraseSnippetTitle.select#,scoreRuleGrouping.scoreRuleGroupingID, scoreRuleGrouping.entityTypeID
		) unioned
		group by scoreRuleGroupingID,title,rolledUp,viewAccount
	</cfquery>

	<cfscript>
		
		keyColumnURLListValue=request.query_string_and_script_name;
		
		//this transers data filtering from one page to annother
		if (structKeyExists(form,"FRMFROMDATE")){
			keyColumnURLListValue &= "&FRMFROMDATE=" & form.FRMFROMDATE;
		}
		if (structKeyExists(form,"FRMTODATE")){
			keyColumnURLListValue &= "&FRMTODATE=" & form.FRMTODATE;
		}
		
		keyColumnURLListValue&="&scoreRuleGroupingID=";
		
	</cfscript>
	
	<CF_tableFromQueryObject queryObject="#scoreReport#" 
		showTheseColumns="title,rolledUp,allTime,numberOfActivities,viewAccount"
		ColumnHeadingList="score_scoreRuleGrouping,score_rolledUp,score_totalScore,score_numberOfActivities, "
		ColumnTranslationPrefix="phr_"
		keyColumnList="viewAccount"
		keyColumnURLList= "#keyColumnURLListValue#"
		keyColumnKeyList="scoreRuleGroupingID"
		dateFilter="activityTime"
		HidePageControls="no"
		useInclude = "false"
		numRowsPerPage="400"
		columnTranslation="true">
	
</cfif>	