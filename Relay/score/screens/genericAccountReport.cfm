<cfparam name="reportEntityTypeID" type="numeric">
<cfparam name="reportEntityID" type="numeric">
<cfparam name="scoreRuleGroupingID" type="numeric">

<!--- This is for maintaining the date filtering between screens --->

<cfif structKeyExists(url,"FRMFROMDATE")>
	<cfset form.FRMFROMDATE=url.FRMFROMDATE>
</cfif>
<cfif structKeyExists(url,"FRMTODATE")>
	<cfset form.FRMTODATE=url.FRMTODATE>
</cfif>



<cfif request.currentSite.isInternal and structKeyExists(url,"adjustScore") AND url.adjustScore and application.com.login.checkInternalPermissions(securityTask = "ScoreTask").level3>
	<cf_checkfieldEncryption fieldnames="adjustScore">
	
	<cfinclude template="adjustScore.cfm">
		
<cfelse>	
		
	<cfset phraseSnippetTitle = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRuleGrouping", baseTableAlias="scoreRuleGrouping", phraseTextID = "title", phraseTableAlias="pt1")>
	
	<cfparam name="SortOrder" default="activityTime desc">
	
	<cfif FindNoCase("desc",SortOrder)>
		<cfset tieBreakerSortOrder="tieBreakerID desc">
	<cfelse>
		<cfset tieBreakerSortOrder="tieBreakerID">
	</cfif>

	
	<cfset dateRangeFieldName="activityTime"> <!--- For the tfqo --->
	
	<cfquery name ="scoreReport" datasource="#application.SiteDataSource#">
		
		<!--- Create a table variable to make the running total slightly less painful (sqlserver 2012 would allow the over keyword which would be better) --->
		declare @scores TABLE 
						   ( 
						   id int IDENTITY(1,1) primary key NOT NULL,
						   activityType nVarChar(1000), 
						   activityTime dateTime, 
						   score int,
						   tieBreakerID int
						   );
		
		insert into @scores(activityType, activityTime, score,tieBreakerID)
			select 
				activityType.activityType as activityType,
				activity.activityTime as activityTime,
				score,
				scoreID as tieBreakerID
			from 
			score
			join scoreRuleVersion on score.triggeringScoreRuleVersionID=scoreRuleVersion.ScoreRuleVersionID
			join scoreRule on scoreRule.scoreRuleID=scoreRuleVersion.scoreRuleID
			join scoreRuleGrouping on scoreRule.scoreRuleGroupingID=scoreRuleGrouping.scoreRuleGroupingID
			join activity on score.triggeringActivityID=activity.activityID
			join activityType on activity.activityTypeID=activityType.activityTypeID
			#preserveSingleQuotes(phraseSnippetTitle.join)# 
			<cfif reportEntityTypeID EQ application.entityTypeID.person> 
		        where activity.personID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.location> 
		        where activity.locationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.organisation>
		        where activity.organisationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER"> 
		    <cfelse>
		    	<cfthrow message="entityTypeID #reportEntityTypeID# not supported (only Person organisation and location supported)"> 
		    </cfif> 
		    
		    <cfif structKeyExists(form,"FRMFROMDATE") and form.FRMFROMDATE NEQ "">
		    	and activity.activityDate >= <cf_queryparam value = "#form.FRMFROMDATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    <cfif structKeyExists(form,"FRMTODATE")  and form.FRMTODATE NEQ "">
		    	and activity.activityDate <= <cf_queryparam value = "#form.FRMTODATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    
			and scoreRuleGrouping.scoreRuleGroupingID = <cf_queryparam value = "#scoreRuleGroupingID#" CFSQLType="CF_SQL_INTEGER"> 
		union all
			select comment as activityType,
			effectiveDate as activityTime,
			score,
			scoreAdjustmentID as tieBreakerID
			from scoreManualAdjustment
			<cfif reportEntityTypeID EQ application.entityTypeID.person> 
		        where personID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.location> 
		        where locationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER">
		    <cfelseif reportEntityTypeID EQ application.entityTypeID.organisation>
		        where organisationID = <cf_queryparam value = "#reportEntityID#" CFSQLType="CF_SQL_INTEGER"> 
		    <cfelse>
		    	<cfthrow message="entityTypeID #reportEntityTypeID# not supported (only Person organisation and location supported)"> 
		    </cfif> 
		    
		    <cfif structKeyExists(form,"FRMFROMDATE") and form.FRMFROMDATE NEQ "">
		    	and effectiveDate >= <cf_queryparam value = "#form.FRMFROMDATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    <cfif structKeyExists(form,"FRMTODATE")  and form.FRMTODATE NEQ "">
		    	and effectiveDate <= <cf_queryparam value = "#form.FRMTODATE#" CFSQLType="CF_SQL_TIMESTAMP">
		    </cfif>
		    and scoreRuleGroupingID = <cf_queryparam value = "#scoreRuleGroupingID#" CFSQLType="CF_SQL_INTEGER">
		order by activityTime,tieBreakerID;
		  
		
		select activityType, activityTime, score, ( select sum(score) from @scores previous where previous.id<=scores.id ) as runningTotal
		from @scores scores
		order by <cf_queryObjectName value="#sortOrder#">, <cf_queryObjectName value="#tieBreakerSortOrder#"> 

	</cfquery>



<cfquery dbtype="query" name="totalQuery">  
    SELECT sum(score) as total
    FROM scoreReport 
</cfquery>


	<cfif request.currentSite.isInternal and application.com.login.checkInternalPermissions(securityTask = "ScoreTask").level3>
		<cfoutput>
			<cfset returnURL="">
			<cfset adjustmentUrl="#script_name#?frmcurrententityid=#url.frmcurrententityid#&frmentitytypeid=#url.frmentitytypeid#&frmentityscreen=#url.frmentityscreen#&frmentityid=#url.frmentityid#&scoreRuleGroupingID=#scoreRuleGroupingID#&adjustScore=true">
			<a href="#application.com.security.encryptURL(adjustmentUrl)#" id="addAdjustment" style="float: right;" class="btn btn-primary">phr_score_addAdjustment</a>
		</cfoutput>
	</cfif>
	
	

	<cf_relayFormDisplay>
		<CF_relayFormElementDisplay disabled="true" relayFormElementType="text" currentValue="" fieldName="" size="1" label="phr_score_scoreRuleGrouping_Name" >
			<cf_editPhraseWidget displayAs="input" disabled="true" entityID ="#scoreRuleGroupingID#" entityType = "scoreRuleGrouping" message= "" phraseTextID="title" showOtherTranslationsTable=false showCurrentLanguageString=false readonly=true showTranslationLink=false>
		</cf_relayFormElementDisplay>
		<cf_relayFormElementDisplay fieldName="numberOfActivities" relayFormElementType="numeric" disabled="true" currentValue="#scoreReport.recordCount#" label="phr_score_numberOfActivities">
		<cf_relayFormElementDisplay fieldName="total" relayFormElementType="numeric" disabled="true" currentValue="#totalQuery.total#" label="phr_score_totalScore">
	</cf_relayFormDisplay>
	
	
	<CF_tableFromQueryObject 
		queryObject="#scoreReport#" 
		queryName="scoreReport"
		showTheseColumns="activityType,activityTime,score,runningTotal"
		ColumnHeadingList="score_activityType,score_activityTime,score_score,score_runningTotal"
		ColumnTranslationPrefix="phr_"
		dateFilter="activityTime"
		dateTimeFormat="activityTime"
		HidePageControls="no"
		useInclude = "false"
		numRowsPerPage="400"
		columnTranslation="true">
		
</cfif>	