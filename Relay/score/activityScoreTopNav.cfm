
<cfparam name="addItemTemplate" type="string" default="">
<cfparam name="addItemTemplateText" type="string" default="phr_Add">
<cfparam name="showSave" type="boolean" default="false">
<cfparam name="toggleActiveInActive" type="string" default="off">
<cfparam name="backButtonURL" type="string" default="">
<cfparam name="backButtonPhrase" type="string" default="phr_back">


<CF_RelayNavMenu pageTitle="" thisDir="Activity Score">
	<cfif showSave>
		<cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="FormSubmit(this);">
	</cfif>
	<cfif addItemTemplate NEQ "">
		<cf_relayNavMenuItem MenuItemText="#addItemTemplateText#" CFTemplate="#application.com.security.encryptURL(addItemTemplate)#" onclick="">
	</cfif>
	<CF_RelayNavMenuItem MenuItemText="phr_score_scoreRuleGroupings" CFTemplate="activityScoreGroups.cfm">
	<CF_RelayNavMenuItem MenuItemText="phr_score_allScoreRules" CFTemplate="allActivityScoreRules.cfm">
	<cfif toggleActiveInActive NEQ "OFF">
		<cfset toggleActiveInActive_text= (toggleActiveInActive EQ "active"?"phr_showactive":"phr_showinactive")>
		<CF_RelayNavMenuItem MenuItemText="#toggleActiveInActive_text#" CFTemplate="##" onclick="toggleActiveInActive();">	
	</cfif>	
	<cfif backButtonURL NEQ "">
		<cf_relayNavMenuItem MenuItemText="#backButtonPhrase#" CFTemplate="#backButtonURL#" onclick="">
	</cfif>
		
</CF_RelayNavMenu>


