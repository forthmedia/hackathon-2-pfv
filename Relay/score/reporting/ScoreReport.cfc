/**
 * An interface to define reporting of score
 * 
 * @author Richard.Tingle
 * @date 22/03/16
 **/
interface {

	/**
	 * Returns the EntityTypeID on which this report is based (will be person, location or organisation)
	 **/
	numeric function getReportingEntityTypeID();

	/**
	 * Returns the score for a particular entity in the period defined by this report.
	 * May include rolled up score depending on the report
	 **/
	numeric function getScoreForEntityID(required numeric entityID);
	
	/**
	 * Returns a query obtained by the requested EntityIDs.
	 * 
	 * requiredEntityIDs is an array of integers. If requiredEntityIDs is ommitted then all entities are returned
	 * 
	 * 
	 * Has columns EntityID, score, if requiredEntityIDs is ommitted 
	 * 
	 **/
	query function getScoreForEntities(Array requiredEntityIDs);
	
	/**
	 * if (for example) an organisations score includes all of its location and person childrens scores
	 **/
	boolean function isARolledUpScore();
	
}