<!---
  --- ScoreReportForGrouping
  --- ----------------------
  --- 
  --- author: Richard.Tingle
  --- date:   27/03/16
  --->
<cfcomponent implements="score.reporting.ScoreReport" accessors="true" output="false" persistent="false">


	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="scoreRuleGroupingID" type="numeric" required="false"  hint ="if not passed it is the total from all groups" />
		<cfargument name="entityTypeIDForReporting" type="numeric" required="true"/>
		<cfargument name="startDate_exclusive" required="false" />
		<cfargument name="endDate_inclusive" required="false" />
		<cfargument name="includeRollup" />
		
		<cfscript>
			if (structKeyExists(arguments,"scoreRuleGroupingID")){
				this.scoreRuleGroupingID=arguments.scoreRuleGroupingID;
			}
			this.entityTypeIDForReporting=arguments.entityTypeIDForReporting;
			if (structKeyExists(arguments,"startDate_exclusive")){
				this.startDate_exclusive=startDate_exclusive;
			}else{
				this.startDate_exclusive=createdatetime(1970,1,1,0,0,0);
			}
			if (structKeyExists(arguments,"endDate_inclusive")){
				this.endDate_inclusive=endDate_inclusive;
			}else{
				this.endDate_inclusive=now();
			}
			if (structKeyExists(arguments,"scoreRuleGroupingID")){
				this.scoreRuleGrouping_entityTypeID=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.scoreRuleGrouping, entityID=arguments.scoreRuleGroupingID, fieldList="entityTypeID").recordSet.entityTypeID;
			}
			if (structKeyExists(arguments,"includeRollup")){
				this.includeRollup=arguments.includeRollup;
			}else{
				if (structKeyExists(this,"scoreRuleGrouping_entityTypeID")){
					this.includeRollup=this.scoreRuleGrouping_entityTypeID eq this.entityTypeIDForReporting;
				}else{
					this.includeRollup=true;
				}
			}
			
			
		</cfscript>
		
		<cfreturn this/>
	</cffunction>

	<cffunction name="isARolledUpScore" access="public" output="false" returntype="boolean">
		<!--- If its not for a particular grouping it intrinsically is rolled up --->
		<cfreturn this.includeRollup>
	</cffunction>

	<cffunction name="getReportingEntityTypeID" access="public" output="false" returntype="numeric" >
		<cfreturn this.entityTypeIDForReporting>
	
	</cffunction>

	<cffunction name="getScoreForEntityID" access="public" output="false" returntype="numeric">
		<cfargument name="entityID" type="numeric" required="true" />
		<cfscript>
			var requestArray=[entityID];
			var result=getScoreForEntities(requestArray);
			
			if (result.recordCount EQ 1){
				return result.score;
			}else{
				return 0;
			}
			
		</cfscript>
		
		
	</cffunction>

	<cffunction name="getScoreForEntities" access="public" output="false" returntype="query">
		<cfargument name="requiredEntityIDs" type="Array" required="false" />
		<cfif structKeyExists(arguments,"requiredEntityIDs")>
			<cfset var requiredEntityIDsList=ArrayToList(requiredEntityIDs)>
		</cfif>
		<cfquery name="applyScores" datasource="#application.sitedatasource#">
			select entityID, sum(score) as score from (
				select 
					 activity.#application.entityType[this.entityTypeIDForReporting].uniqueKey# as entityID,
					 score
				from score
				join activity on score.triggeringActivityID=activity.ActivityID
				join scoreRuleVersion on score.triggeringscoreRuleVersionID=scoreRuleVersion.scoreRuleVersionID
				join scoreRule on scoreRuleVersion.scoreRuleID=scoreRule.scoreRuleID
				join scoreRuleGrouping on scoreRuleGrouping.scoreRuleGroupingID=scoreRule.scoreRuleGroupingID
				where 
					1=1 
					<cfif (structKeyExists(arguments,"scoreRuleGroupingID"))>
						and scoreRule.scoreRuleGroupingID = <cfqueryparam value = "#this.scoreRuleGroupingID#" CFSQLType = "CF_SQL_INTEGER">
					</cfif>
					and activity.activityTime > <cfqueryparam value = "#this.startDate_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP">
					and activity.activityTime <=  <cfqueryparam value = "#this.endDate_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP"> 
					<cfif structKeyExists(arguments,"requiredEntityIDs")>
						and activity.#application.entityType[this.entityTypeIDForReporting].uniqueKey# in ( <cfqueryparam value = "#requiredEntityIDsList#" CFSQLType = "CF_SQL_INTEGER" list="true">)
					</cfif>
					and activity.#application.entityType[this.entityTypeIDForReporting].uniqueKey# is not null
					<cfif not this.includeRollup>
						and scoreRuleGrouping.entityTypeID=<cfqueryparam value = "#this.entityTypeIDForReporting#" CFSQLType = "CF_SQL_INTEGER">
					</cfif>
					
			union all
				select
					#application.entityType[this.entityTypeIDForReporting].uniqueKey# as entityID,
					score
				from scoreManualAdjustment
				join scoreRuleGrouping on scoreRuleGrouping.scoreRuleGroupingID=scoreManualAdjustment.scoreRuleGroupingID
				
				where
					1=1 
					<cfif (structKeyExists(arguments,"scoreRuleGroupingID"))>
						and scoreRuleGroupingID = <cfqueryparam value = "#this.scoreRuleGroupingID#" CFSQLType = "CF_SQL_INTEGER">	
					</cfif>
					<cfif structKeyExists(arguments,"requiredEntityIDs")>
						and #application.entityType[this.entityTypeIDForReporting].uniqueKey# in ( <cfqueryparam value = "#requiredEntityIDsList#" CFSQLType = "CF_SQL_INTEGER" list="true">)
					</cfif>
					and #application.entityType[this.entityTypeIDForReporting].uniqueKey# is not null
					and effectiveDate > <cfqueryparam value = "#this.startDate_exclusive#" CFSQLType = "CF_SQL_TIMESTAMP">
					and effectiveDate <=  <cfqueryparam value = "#this.endDate_inclusive#" CFSQLType = "CF_SQL_TIMESTAMP"> 
					<cfif not this.includeRollup>
						and scoreRuleGrouping.entityTypeID = <cfqueryparam value = "#this.entityTypeIDForReporting#" CFSQLType = "CF_SQL_INTEGER">
					</cfif>
			) unioned
			group by entityID
		</cfquery>
		
		
		<cfreturn applyScores/>
	</cffunction>
</cfcomponent>