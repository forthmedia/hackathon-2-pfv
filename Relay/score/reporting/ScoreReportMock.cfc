/**
 * An interface to define reporting of score
 * 
 * @author Richard.Tingle
 * @date 22/03/16
 **/
component implements="score.reporting.ScoreReport" {

	/**
	 * Returns the EntityTypeID on which this report is based (will be person, location or organisation)
	 **/
	numeric function getReportingEntityTypeID(){
		return application.entityTypeID.person;
	}

	/**
	 * Returns the score for a particular entity in the period defined by this report.
	 * May include rolled up score depending on the report
	 **/
	numeric function getScoreForEntityID(required numeric entityID){
		return 10;
	}
	
	/**
	 * Returns a query obtained by the requested EntityIDs.
	 * 
	 * EntityID, score
	 * 
	 **/
	query function getScoreForEntities(Array requiredEntityIDs){
		var queryMockArray=ArrayNew(1);
		
		queryMockArray[1]={EntityID=1, score=100};
		queryMockArray[2]={EntityID=3, score=200};
		queryMockArray[3]={EntityID=4, score=600};
		queryMockArray[4]={EntityID=15, score=300};
		
		return application.com.structureFunctions.arrayOfStructuresToQuery(theArray=queryMockArray, addArrayIndex=false);
	}
	
	/**
	 * if (for example) an organisations score includes all of its location and person childrens scores
	 **/
	boolean function isARolledUpScore(){
		return true;
	}
	
}