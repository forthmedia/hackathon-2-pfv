<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			activityScore.cfm
Author:				NJH
Date started:		2016/02/22

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->

<cfparam name="url.active" type="boolean" default="true">


<cfset toggleActiveInActive=url.active EQ "true" ?"inactive":"active">
<cfset application.com.request.setTopHead(pageTitle="phr_sys_nav_Activity_Scores", queryString = "",topHeadCfm="/relay/score/activityScoreTopNav.cfm", addItemTemplate="activityScoreGroup.cfm", toggleActiveInActive=toggleActiveInActive)>

<cfparam name="SortOrder" default="title">

<cfset phraseSnippetTitle = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRuleGrouping", phraseTextID = "title", phraseTableAlias="pt1", nullText="scoreRuleGroupingID")>
<cfset phraseSnippetDescription = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRuleGrouping", phraseTextID = "description", phraseTableAlias="pt2")>

<cfquery name="getActivityScores">
		select distinct scoreRuleGroupingID,entityName,
			#phraseSnippetTitle.select# as title,
			#phraseSnippetDescription.select# as description
             from  scoreRuleGrouping
             join schemaTableBase on schemaTableBase.entityTypeID=scoreRuleGrouping.entityTypeID
			 #preserveSingleQuotes(phraseSnippetTitle.join)#  
			 #preserveSingleQuotes(phraseSnippetDescription.join)#  
	where
		1=1
		and scoreRuleGrouping.active = <cf_queryparam value = "#url.active#" CFSQLType="CF_SQL_INTEGER">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cfset pageTitle = "Activity Scores">

<cfset application.com.request.setDocumentH1(text=pageTitle)>

<CF_tableFromQueryObject queryObject="#getActivityScores#" 
	showTheseColumns="title,description,entityName"
	ColumnHeadingList="score_scoreRuleGrouping,description,score_scoreCollectedByEntity"
	ColumnTranslationPrefix="phr_"
	HidePageControls="no"
	useInclude = "false"
	numRowsPerPage="400"
	keyColumnList="title"
	keyColumnURLList="activityScoreGroup.cfm?recordID="
	keyColumnKeyList="scoreRuleGroupingID"
	columnTranslation="true">


<script language="javascript">
	function toggleActiveInActive() {
		<cfset encryptedURL=application.com.security.encryptURL("activityScoreGroups.cfm?active=#NOT url.active#")>
	   	<cfoutput>
			window.location.href = "#encryptedURL#";
		</cfoutput>
	}
</script>
