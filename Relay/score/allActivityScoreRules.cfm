
<CFPARAM name="url.active" type="boolean" default="true">


<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">


<cfset toggleActiveInActive=url.active EQ "true" ?"inactive":"active">
<cfset application.com.request.setTopHead(pageTitle="phr_score_Rules", queryString = "",topHeadCfm="/relay/score/activityScoreTopNav.cfm", toggleActiveInActive=toggleActiveInActive)>



<script language="javascript">
	function toggleActiveInActive() {
		<cfset encryptedURL=application.com.security.encryptURL("allActivityScoreRules.cfm?active=#NOT url.active#")>
	   	<cfoutput>
			window.location.href = "#encryptedURL#";
		</cfoutput>
	}
</script>

<cfset phraseSnippetGroupingTitle = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRuleGrouping", phraseTextID = "title", phraseTableAlias="pt1", baseTableAlias="scoreRuleGrouping")>
<cfset phraseSnippetRuleName = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "scoreRule", phraseTextID = "name", baseTableAlias="scoreRule")>

<cfparam name="SortOrder" default="ruleName">

<cfquery name="getRulesInGroup">
	
	select 
		scoreRuleVersionID,
		scoreRule.scoreRuleID,
		scoreRule.scoreRuleGroupingID,
		#phraseSnippetRuleName.select# as ruleName,
		#phraseSnippetGroupingTitle.select# as groupTitle,
		flagEntityType.Description as relatedEntity,
		'ruleGroupID=' + CAST (scoreRule.scoreRuleGroupingID as nVarChar(100)) + '&ruleID=' + CAST (scoreRule.scoreRuleID as nVarChar(100)) urlSnippet,
		case 
			when nonStandardMethod is null then CAST (scoreValue AS nVarChar )
			ELSE 'phr_score_customRule'
		end as scoreValue,
		case
			when nonStandardMethod is null then cast(scoreCap  AS nVarChar )
			ELSE 'phr_score_customRule'
		end as scoreCap,
		activityType.description as activityTypeDescription
	
	from scoreRuleVersion	
	join scoreRule on scoreRuleVersion.scoreRuleID=scoreRule.scoreRuleID
	join scoreRuleGrouping on scoreRuleGrouping.scoreRuleGroupingID=scoreRule.scoreRuleGroupingID
	join activityType on activityType.activityTypeID=scoreRuleVersion.activityTypeID
	join flagEntityType on scoreRuleGrouping.entityTypeID=flagEntityType.entityTypeID
	#preserveSingleQuotes(phraseSnippetRuleName.join)# 
	#preserveSingleQuotes(phraseSnippetGroupingTitle.join)# 
	where validTo is null
	and scoreRuleVersion.active = <cf_queryparam value = "#url.active#" CFSQLType="CF_SQL_INTEGER">
	order by <cf_queryObjectName value="#sortOrder#">	
</cfquery>


<CF_tableFromQueryObject queryObject="#getRulesInGroup#" 
showTheseColumns="ruleName,relatedEntity,groupTitle,scoreValue,scoreCap,activityTypeDescription"
ColumnHeadingList="score_scoreRuleGrouping_scoreRuleTitle,phr_score_scoreCollectedByEntity,score_scoreRuleGrouping,score_scorePerActivity,score_scoringCap,score_activityType"
ColumnTranslationPrefix="phr_"
HidePageControls="no"
useInclude = "false"
numRowsPerPage="400"
keyColumnList="ruleName"
keyColumnURLList="activityScoreRule.cfm?"
keyColumnKeyList="urlSnippet"
columnTranslation="true">




