<cf_checkfieldEncryption fieldnames="ruleGroupID">

<cfif structKeyExists(url,"ruleID")>
	<cf_checkfieldEncryption fieldnames="ruleID">
</cfif>


<cfset application.com.request.setTopHead(pageTitle="phr_score_Rule", queryString = "",topHeadCfm="/relay/score/activityScoreTopNavWithSave.cfm", backButtonURL=application.com.security.encryptURL("activityScoreGroup.cfm?recordID=#url.ruleGroupID#"),backButtonPhrase="phr_score_backToGrouping")>


<cfparam name="url.ruleID" default="NewRecord">

<cfset savingData=structKeyExists(form,"activityTypeID")>
<cfset newRecord=(url.ruleID EQ "NewRecord")>
<cfset loadingData=not savingData and not newRecord>

<cfset entityTypeIDForscoring=application.com.activityScore.getScoreRuleGrouping(url.ruleGroupID).recordSet.entityTypeID>

<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">

<script language="javascript">
	function FormSubmit() {
	    triggerFormSubmit(jQuery('#scoreRule'));
	}
</script>

<cfscript>
	if (savingData){
		ruleVersionData={
			activityTypeID=form.activityTypeID,
			scoreCalculationMethod=form.scoreCalculationMethod,
			scoreCap=form.scoreCap,
			active=form.active,
			filterDefinition=form.activityFilter
		};

		//prevent active rules in inactive groups
		if (ruleVersionData.active EQ true and application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.scoreRuleGrouping,entityID=ruleGroupID, fieldList="active").recordSet.active EQ false){
			ruleVersionData.active=false;
			application.com.relayUI.setMessage(message="phr_score_rulesCantBeActiveInInactiveGroups", type="warning");
		}



		ruleVersionData.nonStandardParameters="";
		if (scoreCalculationMethod EQ "default"){
			ruleVersionData.scoreValue=form.scorePerActivity;

		}else{
			ruleVersionData.scoreValue="";

			nonStandardFieldsMetadata=application.com.activityScore.getRequiredParametersForActivityScoringMethod(form.scoreCalculationMethod);

			nonStandardParametersStruct={};

			for(i=1;i<=arrayLen(nonStandardFieldsMetadata); i++){
				parameterName=nonStandardFieldsMetadata[i].parameterName;
				nonStandardParametersStruct[parameterName]=form[parameterName];
				ruleVersionData.nonStandardParameters=serializeJson(nonStandardParametersStruct);
			}

		}



		if (newRecord){
			url.ruleID=application.com.activityScore.createNewRule(url.ruleGroupID,ruleVersionData);
			form.newRecord=url.ruleID; //this is for the translation widgets

			//set redirect headers to get us back into the normal flow

			currentLocation=request.script_name;

			pc = getpagecontext().getresponse();
			pc.getresponse().setstatus(303); //redirect without post body
			pc.setHeader("Location",application.com.security.encryptURL("#currentLocation#?ruleID="&url.ruleID & "&ruleGroupID=" & ruleGroupID));
		}else{
			application.com.activityScore.saveNewVersionOfRule(url.ruleID,ruleVersionData);
		}
	}else if (loadingData){
		ruleVersionData=application.com.activityScore.getMostRecentVersionOfRule(url.ruleID);
	}else{
		//empty values for display
		ruleVersionData={activityTypeID="",scoreCalculationMethod="",scoreCap="",active=0,filterDefinition=""};
	}

</cfscript>


<form id="scoreRule" action="" method="post">
	<cf_relayFormDisplay>

		<cf_relayFormElementDisplay  relayFormElementType="Text" fieldname="ruleName" currentValue="" label="phr_score_scoreRuleGrouping_scoreRuleTitle" required="Yes">
			<cf_editPhraseWidget entityID ="#ruleID#" entityType = "scoreRule" required="yes" message= "" phraseTextID="name"  showOtherTranslationsTable=false>
		</cf_relayFormElementDisplay>

		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="moduleTextID" size="1" query="#application.com.activityScore.getActiveModulesWithActivities()#" value="moduleTextID" display="MODULENAME" label="phr_relayware_filterByModule" nulltext="" nullValue="">
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#ruleVersionData.activityTypeID#" fieldName="activityTypeID" bindOnLoad="true" size="1" bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='activityScoreWS',methodName='getActivitiesInModule',moduleTextId={moduleTextID},entityTypeIDForScoring=#entityTypeIDForscoring#)" value="activityTypeID" display="description" label="phr_activity" nulltext="" nullValue="">


		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#ruleVersionData.scoreCalculationMethod#" fieldName="scoreCalculationMethod" bindOnLoad="false" size="1" bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='activityScoreWS',methodName='getScoringMethodsForActivity',activityTypeId={activityTypeID})" value="method" display="display" label="phr_score_scoringMethod" nulltext="" nullValue="">

		<div id="dynamicOptions"> <!---These options may be changed by javascript depending on the value of scoreCalculationMethod--->
		</div>

		<CF_relayFormElementDisplay relayFormElementType="numeric" maxlength="9" currentValue="#ruleVersionData.scoreCap#" fieldName="scoreCap" label="phr_score_scoringCap (phr_score_scoringCap_restrictionPeriod)">

		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#ruleVersionData.active#" fieldName="active" label="phr_sys_active"  list="1#application.delim1#phr_yes,0#application.delim1#phr_no" >

		<!--- As the dynamic options are loaded from the server depending on a selection the current values are stored in a hidden field so they can be repopulated --->
		<cfscript>


			if (!newRecord){
				if (isDefined("ruleVersionData.nonStandardParameters") && len(ruleVersionData.nonStandardParameters)>0){ //defined when this is an existing record
					currentOptionsStruct=deserializeJSON(ruleVersionData.nonStandardParameters);
				}else{
					currentOptionsStruct={"scorePerActivity"=ruleVersionData.scoreValue};
				}
			}else{
				currentOptionsStruct={};
			}


		</cfscript>
		<cfoutput>
			<!--- The data for current values for the dynamic fields --->
			<input type="hidden" id="currentOptionsStruct" value="#htmlEditFormat(SerializeJSON(currentOptionsStruct))#">
			<input type="hidden" id="currentFilter" value="#htmlEditFormat(ruleVersionData.filterDefinition)#">
		</cfoutput>

		<h3>phr_score_FilterPeopleWhoCanRecieveScore</h3>

		<div id="dynamicFilter">
			<!---These options may be changed by javascript depending on the value of scoreCalculationMethod--->
			<!--- will be something like this:
			<cf_filterLocallyAnyEntity name="activityFilter" filteredByTypeID="#application.entityTypeID.person#" currentFilter="">
			--->
		</div>


	</cf_relayFormDisplay>
</form>




<script language="javascript">
	jQuery(document).ready(function() {



		var reloadFilter=function(){

			var activityTypeID=jQuery('#activityTypeID').val();
			var currentFilter=encodeURIComponent(jQuery("#currentFilter").val());
			jQuery('#dynamicFilter').load(

				url='/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=activityScoreWS&methodName=displayFilter&returnFormat=plain&activityTypeID='+activityTypeID + '&filterName=activityFilter&currentFilter='+currentFilter
				);

		}

		var reloadOptionsIntoFields=function(){
			var existingOptions=JSON.parse(jQuery("#currentOptionsStruct").val());

			for (var prop in existingOptions) {
				//refill fields if available
				var possibleField=jQuery('#' +prop);

				if (!(possibleField == null)){
				    possibleField.val(existingOptions[prop]);
				}

		    }

		}

		var reloadOptionsFunction=function() {
//          ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]
            var activityTypeID=jQuery('#activityTypeID').val();
			//before pulling in a new set of options we save all the old data incase the pulled in version is the same as this one
			var dynamicOptionsData=JSON.parse(jQuery("#currentOptionsStruct").val());

			var dynamicOptions=jQuery('#dynamicOptions').find('input');
			for (var i = 0; i < dynamicOptions.length; i++) {
			    var option=jQuery(dynamicOptions[i]);
				var name = option.attr("name");
				var value=option.val();

				dynamicOptionsData[name]=value;
			}

			jQuery("#currentOptionsStruct").val(JSON.stringify(dynamicOptionsData));
//          ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]
		  	var currentScoringMethod=jQuery("#scoreCalculationMethod").val();
		  	jQuery('#dynamicOptions').load('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=activityScoreWS&methodName=displayScoringMethodOptions&scoringMethod=' + currentScoringMethod + '&activityTypeID=' + activityTypeID + '&returnFormat=plain',reloadOptionsIntoFields);
		}

		//as only user interactions trigger change events we have to bind to all potential change points

		jQuery("#scoreCalculationMethod").change(reloadOptionsFunction);
		jQuery('#scoreCalculationMethod').on('rw:ajaxupdate',reloadOptionsFunction)


		//reload filter whether manually changed or changed by a binding function.
		jQuery("#activityTypeID").change(reloadFilter);
		jQuery('#activityTypeID').on('rw:ajaxupdate',reloadFilter);

		reloadOptionsIntoFields();

	});



</script>