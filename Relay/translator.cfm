<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			translator.cfm	
Author:				SWJ
Date started:		2003-02-13
	
Description:			

Simply sets and unsets the translation cookie.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<script>
	function goToEdit () {
	history.go(-1);
	window.location.reload(true);
	}
</script>
<cfif isDefined("cookie.translator") and len(cookie.translator) eq "16">
	<cfoutput>#htmleditformat(len(cookie.translator))#</cfoutput>
	<cfinclude template="/translation/unsetcookie.cfm">
	<br><br><A HREF="javascript:goToEdit()">Return to edit screen</A>
<cfelse>
	<cfinclude template="/translation/setcookie.cfm">
	<br><br><A HREF="javascript:goToEdit();">Return to edit screen</A>
</cfif>
