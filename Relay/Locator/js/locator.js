/* �Relayware. All Rights Reserved 2014 */
// some global variables
// NJH - commented out calls to hideProfileDiv as looks like no longer used.
var scrollBarFadeInOutInitialised = false;
var latlng = new google.maps.LatLng(0, 0);
var map ='';
var defaultViewType = 'list';
var expandFlagGroupDivs = {}; // a structure to hold the list of flagGroup divs to expand (they should contain a # as well - for example, {#723='',#4782=''}
var searchResultsMap = ''; // holds the script to display the map.
var mapLoaded = 0;
	
jQuery(document).keypress(function(event) {
	// track enter key
	var keycode = (event.keyCode?event.keyCode:(event.which ? event.which:event.charCode));

	if (keycode == 13)
		{search();return false;}
    return true;
});

/*pass in the link that was clicked */
function setTabSelected(linkObj) {
	jQuery(linkObj).parents('ul').find('li').removeClass('active');
	jQuery(linkObj).parent().addClass('active');
}

function search() {
	
	jQuery('#locatorResultsData').parent().html('<div id="locatorResultsData"><img src="/images/icons/loading.gif" alt="Loading"/></div>');
	jQuery('#locatorResultsMap').html('');
	
	var viewType = defaultViewType;
	
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayLocatorWS&methodName=getDisplayResults&returnFormat=json',
        	data:jQuery('#searchForm').serialize(),
        	dataType:'json',
        	error: function() {jQuery('#locatorResultsData').parent().html('<div id="locatorResultsData"><div class="error">Error retrieving search results. Please try again.</div></div>');},
        	success: function(data,textStatus,jqXHR) {
        		jQuery("#locatorResultsContainerParent").show();
        		jQuery('#locatorResultsData').parent().html(data.list);
				
				jQuery("#locatorResultsContainer")[0].scrollIntoView();
								
				//jQuery('#locatorResultsMap').html(data.map);
				searchResultsMap = data.map; /* put data into variable initially, as if the map is not rendered right away but put into a div that is hidden, there is a problem  when it is initially rendered */
				
				if (!scrollBarFadeInOutInitialised) {
					//initialiseScrollBarForLocator();
				} else {
					//jQuery('#locatorResults').jScrollPane()
				}
				if (!data.HASRESULTS) {
					jQuery('#frmSendMessage').attr('disabled',true);
				} else {
					jQuery('#frmSendMessage').removeAttr('disabled');
				}

/*
				if (!data.HASRESULTS) {
					jQuery('.viewLinks').hide();
					if (defaultViewType == 'map'){
						viewType = 'list';
					}
				} else {
					jQuery('.viewLinks').show(); //show the 'view search results' links
				}*/
				jQuery('.viewLinks').show(); //show the 'view search results' links
				showViewType(viewType);
				
			}
		});

	return false;
}

function initialiseScrollBarForLocator() {
	//scrollBarFadeInOutInitialistion('#locatorResults');
	//scrollBarFadeInOutInitialised = true;
}

function getGoogleMap(latLongCoords,zoom,mapDivID) {
	
	var myOptions = {
		zoom: zoom,
		panControl:true,
		center: latLongCoords,
		mapTypeId: google.maps.MapTypeId.ROADMAP
		//scrollwheel: false			      
	};
	
	var googleMap = new google.maps.Map(document.getElementById(mapDivID), myOptions);
	return googleMap;
}

/* this function shows a map in the listing view for an individual location */
function viewMap(tableRowID,latitude,longitude) {
	var latlng = new google.maps.LatLng(latitude,longitude);
	var locationMap = '';
	var directionsDisplay = '';
	var marker = '';
	var showMap = true;
	var locationMapDivID = 'map_canvasDiv';
    
	if (jQuery(window).width() < 3000) {
		jQuery("body").append('<div id="tmpMapCtnr" ><div id="'+locationMapDivID+'"></div></div>');
		locationMap = getGoogleMap(latlng,16,locationMapDivID);

		//START: 2014-07-17 NYB Case 439265 added:
		document.getElementById(locationMapDivID).style.height="100%";
		centreLatitude = latitude + .0015;
		locationMap.setCenter({lat: centreLatitude, lng: longitude});
		//END: 2014-07-17 NYB Case 439265 

		marker = new google.maps.Marker({
			position:latlng, 
			map: locationMap
		});

		jQuery.fancybox.open({
		      helpers: {
		         overlay: {
		            locked: true 
		         }
		      },
			autoSize: false,
			width: '85%',
			height: '85%',
			closeEffect : 'none',
			closeSpeed: 'fast',
			href: '#'+locationMapDivID,
			beforeShow: function () {
				google.maps.event.trigger(map, "resize");
			},
			'afterClose': function(){
				setTimeout(
					function() 
					{jQuery("#tmpMapCtnr").remove();}, 500)
				;}
			});
	}else{
		if (jQuery('#map_canvasRow').length) {
			if (jQuery('#'+tableRowID).next().attr('id') == 'map_canvasRow') {
				showMap = false;
			}
			jQuery('#map_canvasRow').remove();
		}
		
		if (showMap) {
		jQuery('#'+tableRowID).after('<tr id="map_canvasRow"><td colspan="4"><div id="'+locationMapDivID+'"></div></td></tr>').slideDown();
				locationMap = getGoogleMap(latlng,16,locationMapDivID);

				marker = new google.maps.Marker({
					position:latlng, 
					map: locationMap
				});
				//directionsDisplay = new google.maps.DirectionsRenderer();
				//directionsDisplay.setMap(map);

				jQuery('.jspContainer').scrollTop(jQuery('#'+tableRowID).offset().top-jQuery('#locatorResults').offset().top);
		}
		
		//2014-07-17 NYB Case 439265 added:
		google.maps.event.trigger(locationMap , 'resize');
		
	}
}


function getSearchCriteria(countryID) {
	
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl',
        	data:{countryID:countryID,method:'callWebService',webserviceName:'relayLocatorWS',methodName:'getLocatorSearchCriteria',returnFormat:'json'},
        	dataType:'json',
        	success: function(data,textStatus,jqXHR) {
				jQuery('#searchCriteriaContainer').html(data.CONTENT);

				if (data.CONTACTRESELLER) {
					jQuery('#contactResellers').show();
				} else {
					jQuery('#contactResellers').hide();
				}
				//hideProfileDiv(); 
				jQuery('#countryID').focus();
			}
		});
	return false;
}


/*
function hideProfileDiv() {
	if (jQuery('.searchItemContainer').length) {
			jQuery('.searchItemContainer:not('+Object.keys(expandFlagGroupDivs).join(",")+')').each(function() {
				toggleFilter(jQuery(this).attr('id'));
			});
	} else {
		setTimeout(hideProfileDiv,500);
	}
}*/

function clearFilters() {
	jQuery('input[type=checkbox]:checked').removeAttr('checked');
	jQuery('input[type=radio]').removeAttr('checked');
}

function setPreFilterCriteria(name,value) {
	if (name == 'flagList') {
		var flagIDArray = value.split(',');
		for (var i = 0; i < flagIDArray.length; i++) {
			jQuery('#'+flagIDArray[i]).prop('checked',true);
		}
	} else {
		jQuery('#'+name).val(value);
	}
}

/*
function toggleFilter(divId) {
	if (jQuery('#'+divId+'_toggle').length > 0) {
		if ($(divId+'_toggle').getAttribute('src') == '/code/borders/images/screenBlueArrowUp.png') {
			$(divId+'_toggle').src='/code/borders/images/screenBlueArrowDown.png';
			expandFlagGroupDivs['#'+divId] = '';
		}
		else { 
			$(divId+'_toggle').src='/code/borders/images/screenBlueArrowUp.png';
			delete expandFlagGroupDivs['#'+divId];
		}
		$(divId).toggle();
	}
}*/


function displayGoogleMap(divID,latitude,longitude,zoom,markerArray) {
	// Build the base map.
	
   	var centerLatLong = new google.maps.LatLng(latitude,longitude);
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();
    var marker, i;
	map = getGoogleMap(centerLatLong,zoom,divID);

	if (markerArray.length > 0) { 
		for (var i = 0; i < markerArray.length; i++) {  
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(markerArray[i].latitude, markerArray[i].longitude),
				//icon: markerArray[i].icon,
				map: map
		  	});
		
			//extend the bounds to include each marker's position
			bounds.extend(marker.position);
		
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
		      		infowindow.setContent(markerArray[i].infoWindowText);
		      		infowindow.open(map, marker);
		    	}
			})(marker, i));
		}
		
		bounds.extend(centerLatLong); // this is to include the user's location in the map so that it will definitely show
		//now fit the map to the newly inclusive bounds
		map.fitBounds(bounds);
	}
	
	//(optional) restore the zoom level after the map is done scaling
	/*var listener = google.maps.event.addListener(map, "idle", function () {
	    map.setZoom(3);
	    google.maps.event.removeListener(listener);
	});*/
}

function showViewType (viewType) {

	if (viewType == 'map') {
		if (jQuery(window).width() < 767) {
			//2014-08-22	RPW	Partner locator - Clicking map in mobile view doesn't do anything
			jQuery('#locatorResultsMap').html("");
			mapLoaded = 0;
			if (typeof locatorResultsMapMobile == 'undefined') {
				jQuery("#locatorResultsMap").after('<div id="tmpMapCtnr" ><div id="locatorResultsMapMobile"></div></div>');
			}
			
			jQuery('#locatorResultsMapMobile').html(searchResultsMap);
				
			/* if the div which the map is in has been hidden, then we need to 're-draw' the map.. calling resize and setZoom seems to do the trick... */ 
			google.maps.event.trigger(map, 'resize');
			map.setZoom(map.getZoom());
			
			jQuery.fancybox.open({
			      helpers: {
			         overlay: {
			            locked: true 
			         }
			      },
				autoSize: false,
				width: '90%',
				height: 'auto',
				closeEffect : 'none',
				closeSpeed: 'fast',
				href: '"#locatorResultsMapMobile',
				'afterClose': function(){
					setTimeout(
						function() 
						{jQuery("#tmpMapCtnr").remove();}, 500)
					;}
				});			

			//2014-08-22	RPW	Partner locator - Clicking map in mobile view doesn't do anything
			jQuery('#locatorResults').hide();
		}else{
			if (typeof locatorResultsMapMobile != 'undefined') {
				jQuery('#locatorResultsMapMobile').html("");
			}
		jQuery('#locatorResultsMap').show();
		
		/* Case if the map has not already been loaded, then set it. 
			There is a problem with google maps that setting the map into a hidden div caused it to not render properly. So, what we do is not display it until someone clicks on the map button
		*/
		if (mapLoaded != 1) {
			jQuery('#locatorResultsMap').html(searchResultsMap);
			mapLoaded = 1;
		}
		
		/* if the div which the map is in has been hidden, then we need to 're-draw' the map.. calling resize and setZoom seems to do the trick... */ 
		google.maps.event.trigger(map, 'resize');
		map.setZoom(map.getZoom());
		
		jQuery('#locatorResults').hide();
		}
	} else {
		jQuery('#locatorResultsMap').hide();
		jQuery('#locatorResults').show();	
		
		//if (!scrollBarFadeInOutInitialised) {
			initialiseScrollBarForLocator();
		//}
	}
}


function sendMessage(useProducts,locatorCountryID) {
	if (useProducts == undefined){
		useProducts = 0;
	}
	if (locatorCountryID == undefined){
		locatorCountryID = 0;
	}
	var locationIDs = [];
	jQuery('input:checked[name="frmLocationIDCheck"]').each(function() {
		locationIDs.push(this.value)
	});

	if (locationIDs == 0) {
		alert(phr.locator_error_no_reseller_selected);
	} else {
		jQuery.fancybox.open({
		      helpers: {
		         overlay: {
		            locked: true 
		         }
		      },
			autoSize: false,
			width: '85%',
			height: 'auto',
			closeEffect : 'none',
			closeSpeed: 'fast',
			type: "iframe",
			href: '/LocatorPublic/LocatorEnterDetails.cfm?locationList=' + locationIDs+'&UseProducts='+useProducts+'&countryid='+locatorCountryID,
			});
		
	}
}

/* this function shows a map in the listing view for an individual location */
function viewMapDetail(latitude,longitude) {
	if(jQuery("#map_canvas").length){
		var locationMapDivID = 'map_canvas';
		var latlng = new google.maps.LatLng(latitude,longitude);
		var directionsDisplay = '';
		var marker = '';
		var locationMap = '';
		
		jQuery("#"+locationMapDivID).css("height","250px");
		
		locationMap = getGoogleMap(latlng,16,locationMapDivID);

		marker = new google.maps.Marker({
			position:latlng,
			map: locationMap
		});
	}
}

function getmoreData() {
	var usenextStart = jQuery('[id=nextStart]').val();
	var searchFormData = jQuery('#searchForm').serialize() + '&getResultsFrom=' + usenextStart;
	var showMeMoreTDRemoveJS = jQuery([id=showMeMoreTD]);
	showMeMoreTDRemoveJS.attr('onclick','');

	var getOrderFlagGroupsByFlagScore = jQuery("[id=orderFlagGroupsByFlagScore]");
	if(getOrderFlagGroupsByFlagScore.length && getOrderFlagGroupsByFlagScore.val() != ""){
		searchFormData += '&orderFlagGroupsByFlagScore=' + getOrderFlagGroupsByFlagScore.val();
	}
	
	jQuery('[id=showMeMoreTD]').html('<div id="locatorResultsLoadingMore"><img src="/images/icons/loading.gif" class="bigloadinggif" alt="Loading"/></div>');
	var viewType = defaultViewType;
	
	jQuery.ajax(
    	{type:'get',
        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayLocatorWS&methodName=getDisplayResults&returnFormat=json',
        	data:searchFormData,
        	dataType:'json',
        	error: function() {jQuery('#locatorResultsData').parent().html('<div id="locatorResultsData"><div class="error">phr_locator_errorRetrievingResults</div></div>');},//2015-01-13	ACPK	Case 442923 Changed error message to translation
        	success: function(data,textStatus,jqXHR) {

				jQuery('[id=locatorResultsLoadingMore]').remove();
				jQuery('[id=showMeMoreTD]').remove();
				jQuery('[id=showMeMore]').remove();
				var OldItem = jQuery('[id=locatorResultsData]');
				OldItem.attr('id','locatorResultsDataOld');
				OldItem.after(data.list);

				if (!scrollBarFadeInOutInitialised) {
					initialiseScrollBarForLocator();
				} else {
					jQuery('#locatorResults').jScrollPane()
				}

			}
		});

	return false;
}