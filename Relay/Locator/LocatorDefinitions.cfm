<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			LocatorDefinitions.cfm
Author:				NYB
Date started:		2011/06/24
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011-10-14			NYB			LHID7283 Rewrote most of it to limit the allocation of a Locator Definition to 1 per country
2011/10/20			NYB			LHID7283 Rewrote again - changed locator to use CountryScope - making locatorDef.CountryGroupid redundant
2015/02/10			NJH			Jira Fifteen-113  filter out the flags to only those with flagTextIds, as the code is expecting flagTextIds. An empty flagText means an empty value which means that the flag can't be assigned
Possible enhancements:

 --->

<cfparam name="getLocatorDefinitions" type="string" default="">
<cfparam name="sortOrder" default="Name">
<cfparam name="editor" default="no">

<cfset xmlSource = "">

<cfif structKeyExists(url,"editor")>
	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="locatorDef" title="phr_locator_LocatorDefinitionsEditor">
				<field name="LocatorID" label="phr_locatorID" control="html"></field>
				<field name="Name" label="phr_locator_Name" required="true"></field>
				<field name="useGeo" label="phr_locator_useGeo" control="YorN"></field>
				<field name="distanceLengths" label="phr_locator_distanceLengths" required="true"></field>
				<field name="useCompany" label="phr_locator_useCompany" control="YorN"></field>
				<!---<field name="useProducts" label="phr_locator_useProducts" control="YorN"></field>--->
				<field name="ContactReseller" label="phr_locator_ContactReseller" control="YorN"></field>
				<field size="10" name="searchCriteriaList" label="phr_locator_searchCritera" control="twoSelects" default="" title="title" validValues="select 'pa.'+cast(f.flagID as varchar) as value, f.name as display,'Profile Attribute ('+cast(flagID as varchar)+'): '+fg.name+' - '+f.name as title from flag f inner join flagGroup fg on f.flagGroupID = fg.flagGroupID where fg.entityTypeID in (1,2) and fg.flagTypeID in (2,3) and f.isSystem=0 and fg.active=1 union select distinct 'p.'+cast(fg.flagGroupID as varchar) as value, fg.name as display, 'Profile ('+cast(fg.flagGroupID as varchar)+'): '+fg.name as title from flagGroup fg inner join flag f on f.flagGroupID = fg.flagGroupID where fg.entityTypeID in (1,2) and fg.flagTypeID in (2,3) and f.isSystem=0 and fg.active=1 order by title,value"></field>
				<field size="10" name="orderByFlagOverride" label="phr_locator_orderByFlag" control="twoSelects" default="" title="title" allowSort="true" validValues="select f.flagTextID as value, f.name as display,'Profile Attribute ('+cast(flagID as varchar)+'): '+fg.name+' - '+f.name as title from flag f inner join flagGroup fg on f.flagGroupID = fg.flagGroupID where fg.entityTypeID in (1,2) and fg.flagTypeID in (2,3) and fg.active=1 and f.isSystem=0 and len(isNull(f.flagTextID,'')) > 0 order by title,value"></field>
				<field size="10" name="requiredFlagList" label="phr_locator_requiredFlags" control="twoSelects" default="orgApproved,locator_yes" title="title" validValues="select f.flagTextID as value, case when f.flagTextID='orgApproved' then 'Organization Approved' when f.flagTextId ='locApproved' then 'Location Approved' else f.name end as display,'Profile Attribute ('+cast(flagID as varchar)+'): '+fg.name+' - '+f.name as title from flag f inner join flagGroup fg on f.flagGroupID = fg.flagGroupID where fg.entityTypeID in (1,2) and fg.flagTypeID in (2,3) and len(isNull(f.flagTextID,'')) > 0 and fg.active=1 and (f.isSystem=0 or f.flagTextID in ('orgApproved','locApproved')) order by title,value"></field>
				<field name="countryScope" label="phr_locator_countryScope" control="countryScope" hideCountryGroups="true"></field>
				<field name="active" label="phr_locator_active" control="YorN"></field>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

<cfelse>

	<cfquery name="getLocatorDefinitions" datasource="#application.siteDataSource#">
		select LocatorID,name,useGeo,active,searchCriteriaList from locatorDef
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
</cfif>


<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="locatorDefinitions"
	keyColumnList="name"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##name##'*comma'##name##'*comma'/locator/locatorDefinitions.cfm?editor=yes&hideBackButton=true&LocatorID=##LocatorID##'*comma{reuseTab:true*commaiconClass:'content'});return false;"
	showTheseColumns="name,active"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getLocatorDefinitions#"
	numRowsPerPage="50"
	startRow="1"
	booleanFormat="active"
	hideTheseColumns=""
	dateFormat="lastUpdated"
	showSaveAndAddNew="true"
	hideBackButton = "true"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_locator_"
>