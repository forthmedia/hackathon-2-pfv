<CF_RelayNavMenu pageTitle="phr_locator_LocatorDefinitionsEditor">
	<CFIF findNoCase("locatorDefinitions.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#application.com.security.encryptURL(url='locatorDefinitions.cfm?editor=yes&add=yes&hideBackButton=false')#">
		 <cfif not isDefined("editor")>
			<CF_RelayNavMenuItem MenuItemText="Export To Excel" CFTemplate="javascript:openAsExceljs()">
		</cfif>
	</cfif>
</CF_RelayNavMenu>