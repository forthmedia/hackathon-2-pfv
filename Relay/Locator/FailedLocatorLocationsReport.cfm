<!--- OVERVIEW:
File name:			FailedLocatorLocationsReport.cfc	
Author:				NYB
Date started:		2014/03/05
	
Description:	

Possible enhancements:

--->

<!--- Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-03-05 	NYB 	Case 438911 created to list locations that have been flagged to show up on Locator but whom the system is unable to update the lat/long for

 --->

<cfparam name="sortOrder" default="LocationID">
<cfparam name="form.FILTERSELECT1" default="countrydescription">
<cfparam name="form.FILTERSELECTVALUES1" default="United States">

<cf_title>Locations that have failed the update co-ordinates process<cf_title>

<cf_RelayNavMenu pageTitle="" thisDir="locator">
	<cf_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs();">
</cf_RelayNavMenu>

<cfset argumentsStruct = {cache = "false",all = "true",limitByCountryRights="true"}>
<cfif structkeyexists(url,"countryid") and isnumeric(url.countryid)>
	<cfset argumentsStruct.countryid = url.countryid>
</cfif>
<cfset argumentsStruct.locatorDefQry = application.com.relayLocator.getLocatorDefinition(argumentCollection=argumentsStruct)>
<cfif application.com.settings.getSetting('versions.locator') neq "" and application.com.settings.getSetting('versions.locator') lte 4> <!--- version 4 and previous has the org approval flag as a required flag. version 5 makes this optional. If that is required, then it will be in the requiredFlagList --->
	<cfset argumentsStruct.filterByFlag = orgApprovalFlagTextID>
</cfif>

<cfset getLocationsQry = application.com.relayLocator.getLocationsRequiringLatLongUpdate(argumentCollection=argumentsStruct,rtnFailuresOnly="true")>

<cfset inQoQ = "true">
<cfquery name="getLocationsQry" dbtype="query">
	select * from getLocationsQry where 1=1 
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#SortOrder#">
</cfquery>

<CF_tableFromQueryObject queryObject="#getLocationsQry#" 
	showTheseColumns="LOCATIONID,SITENAME,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,ADDRESS5,POSTALCODE,COUNTRYDESCRIPTION"
	ColumnHeadingList="LocationID,LocationName,Address1,Address2,Address3,Address4,Address5,PostalCode,Country"
	useInclude="false"
	FilterSelectFieldList="countrydescription"
>