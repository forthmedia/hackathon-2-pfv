<!--- �Relayware. All Rights Reserved 2014 --->
<!--- OVERVIEW:
File name:			LocatorDetail.cfc
Author:				AR & NYB
Date started:		2011-05-25

Description:

Possible enhancements:

--->

<!--- Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011-05-25	NYB		Created from code AR create for Lenovo
2011-10-19	NYB		LHID7381 Rewrote codeAddress() js function to narrow down fields until it finds a match

2013/04/10	YMA		Case 434093 Get Lat/Long from address error with no reason.
2014/10/23  SB		Fix locator to be responsive.
2014/11/12	ACPK	Removed redundant code in codeAddress() js function, which now retrieves and stores Administrative Area Level data
2014/11/13	ACPK	Added jQuery(document).ready() to try/catch which calls latlongCalcWarning on 1st field change
 --->


<cf_translate>

<cfoutput><!--- Google Map functionality. --->

<style>
.longLatButt { margin-top:.5em; }
</style>

	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			<label>Phr_Sys_latitude</label>
		</div>
		<div class="col-xs-12 col-sm-9">

			<CF_INPUT type="text" id="location_latitude" value="#location.latitude#" disabled="disabled">
			<input class="btn btn-primary btn-xs longLatButt" type="button" value="phr_locator_map_get_latlong_from_address" onclick="javascript:codeAddress();">
			<CF_INPUT type="hidden" value="#location.latitude#" name="location_latitude_#location.locationid#" id="location_latitude_#location.locationid#">
			<CF_INPUT type="hidden" value="#location.latitude#" name="location_latitude_#location.locationid#_orig" id="location_latitude_#location.locationid#_orig">

		</div>
	</div>
	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
			<label>Phr_Sys_longitude</label>
		</div>
		<div class="col-xs-12 col-sm-9">
			<CF_INPUT type="text" id="location_longitude" value="#location.longitude#" disabled="disabled">
			<CF_INPUT type="hidden" value="#location.longitude#"name="location_longitude_#location.locationid#" id="location_longitude_#location.locationid#" >
			<CF_INPUT type="hidden" value="#location.longitude#" name="location_longitude_#location.locationid#_orig" id="location_longitude_#location.locationid#_orig">
			<CF_INPUT type="hidden" name="location_latlongsource_#location.locationid#" id="location_latlongsource_#location.locationid#" value = "#location.latlongsource#">
			<CF_INPUT type="hidden" name="location_latlongsource_#location.locationid#_orig" value = "#location.latlongsource#">
			<CF_INPUT type="hidden" name="location_administrativeAreaLevel1_#location.locationid#" value="#location.administrativeAreaLevel1#" >
			<CF_INPUT type="hidden" name="location_administrativeAreaLevel1_#location.locationid#_orig" value="#location.administrativeAreaLevel1#">
			<CF_INPUT type="hidden" name="location_administrativeAreaLevel2_#location.locationid#" value="#location.administrativeAreaLevel2#">
			<CF_INPUT type="hidden" name="location_administrativeAreaLevel2_#location.locationid#_orig" value="#location.administrativeAreaLevel2#">
			<input type="hidden" name="frmLocationfieldlist" value = "Location_latitude,Location_longitude,Location_latlongsource,Location_administrativeAreaLevel1,Location_administrativeAreaLevel2">
			<input class="btn btn-primary btn-xs longLatButt" type="button" value="phr_locator_map_set_latlong_to_map_centre" onclick="javascript:setMapLatLong();">
		</div>
	</div>
	<div>
	<div class="form-group row">
		<div class="col-xs-12 col-sm-3 control-label">
		</div>
		<div class="col-xs-12 col-sm-9">
			<cfif not(isNumeric(location.latitude) and isNumeric(location.longitude))>
				<p>phr_locator_map_set_latlong_explanation</p>
			</cfif>
			<p>phr_locator_map_marker_dragged</p>
			<div id="map_canvas" style="width:100%; height:300px;"></div>
		</div>
		<script type="text/javascript"src="#request.currentSite.httpProtocol#maps.google.com/maps/api/js?sensor=false&language=#request.relaycurrentuser.LANGUAGEISOCODE#"></script>
		<script language="javascript">
		// Build the base map.
		var directionDisplay;
		var directionsService = new google.maps.DirectionsService();
		<cfoutput>
		var latlng = new google.maps.LatLng(#jsStringFormat(val(location.latitude))#,#jsStringFormat(val(location.longitude))#);
		</cfoutput>
		var myOptions = {
			 <cfif isNumeric(location.latitude) and isNumeric(location.longitude)>
		     	zoom: 15,
		     <cfelse>
		     	 zoom: 2,
		     </cfif>
		     center: latlng,
		     mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
		var geocoder = new google.maps.Geocoder();
			<!--- Display a marker with the current lat long. --->
		var marker = null;
		<cfif val(location.latitude) OR val(location.longitude)>
			marker = new google.maps.Marker({
		   			position: latlng,
		   			map: map,
		   			draggable:true,
		   			title: latlng.toString()
			});
			google.maps.event.addListener(marker, 'dragend', setMapToMarkerCenter);
		</cfif>
		// After loading page, make sure any changes to an address register a warning.
		jQuery( document ).ready(function() {
			try{
				document.getElementById("location_Address1_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
				document.getElementById("location_Address2_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
				document.getElementById("location_Address3_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
				document.getElementById("location_Address4_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
				document.getElementById("location_Address5_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
				document.getElementById("location_PostalCode_#jsStringFormat(location.locationid)#").onchange = latlongCalcWarning;
				document.getElementById("location_countryid_#jsStringFormat(location.locationid)#").onchange= latlongCalcWarning;
			}
			catch(err){
				<cfif request.relaycurrentuser.isinternal>
					alert("An error has occurred in assembling the address details.  Error details: "+err);
				</cfif>
			}
		});
		
		function codeAddress() {
			var address = '';
			try {
				var finalStatus = '';
				var finalResults = '';
				//Collect all user-submitted info, then send Geocode API request
			    var address = document.getElementById("location_Address1_#jsStringFormat(location.locationid)#").value + ',' +
			    	document.getElementById("location_Address2_#jsStringFormat(location.locationid)#").value + ',' +
			    	document.getElementById("location_Address3_#jsStringFormat(location.locationid)#").value + ',' +
			    	document.getElementById("location_Address4_#jsStringFormat(location.locationid)#").value + ',' +
			    	document.getElementById("location_Address5_#jsStringFormat(location.locationid)#").value + ',' +
					    	document.getElementById("location_PostalCode_#jsStringFormat(location.locationid)#").value + ',' +
					    	document.getElementById("location_countryid_#jsStringFormat(location.locationid)#").options[document.getElementById("location_countryid_#jsStringFormat(location.locationid)#").selectedIndex].text ;
						geocoder.geocode( { 'address': address}, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								finalStatus = status;
						finalResults = results[0];
						setadministrativeAreaLevels(finalResults.address_components);
						//Place new or move existing marker on Google Map using lat/long of address
						var resultsLocation = finalResults.geometry.location;
					if (finalStatus == google.maps.GeocoderStatus.OK) {
				        map.setCenter(resultsLocation);
				        if (marker != null){
				        	marker.setPosition(resultsLocation);
				        	marker.setTitle (resultsLocation.toString()+ address);
				        } else {
				        	marker = new google.maps.Marker({
					            map: map,
					            position: resultsLocation,
					            draggable:true,
					            title : resultsLocation.toString()+ address
					        });
				        }
						google.maps.event.addListener(marker, 'dragend', setMapToMarkerCenter);
				        map.panTo(resultsLocation);
				    	document.getElementById("location_latitude_#jsStringFormat(location.locationid)#").value = resultsLocation.lat();
				    	document.getElementById("location_latitude").value = resultsLocation.lat();
						document.getElementById("location_longitude_#jsStringFormat(location.locationid)#").value = resultsLocation.lng();
						document.getElementById("location_longitude").value = resultsLocation.lng();
						// Make sure the location is visible on the map
						if 	(map.getZoom()< 14){
							map.setZoom(14);
						}
						}
					} else {
						if (finalStatus == "") {
					       alert("phr_locator_map_address_search_failed: " + status); // 2013/04/10	YMA	Case 434093 Added status to failure details when finalstatus is empty.
						} else {
					        alert("phr_locator_map_address_search_failed: phr_" + finalStatus);
						}
				        // Move out the map if no marker set.
				        if (marker == null){
				        	map.setZoom(2);
				        }
					}
				});
			}
			catch (err){
				alert("Operation requires Address1-4 and Postcode text fields and the Country select field configured on the screen.\n\nError description:"+ err.description);
			}
		}

		function setMapLatLong () {
			var latlng = map.getCenter();
			document.getElementById("location_latitude_#jsStringFormat(location.locationid)#").value = latlng.lat();
			document.getElementById("location_latitude").value = latlng.lat();
			document.getElementById("location_longitude_#jsStringFormat(location.locationid)#").value = latlng.lng();
			document.getElementById("location_longitude").value = latlng.lng();
			document.getElementById("location_latlongsource_#jsStringFormat(location.locationid)#").value = 'GOOGLE';
			geocoder.geocode({'latLng': latlng}, function(results, status) {
    			if (status == google.maps.GeocoderStatus.OK) {
    				setadministrativeAreaLevels(results[0].address_components);
				}
			});
			if(marker == null){
				marker = new google.maps.Marker({
		   			position: latlng,
		   			map: map,
		   			draggable:true,
		   			title: latlng.toString()
				});
				google.maps.event.addListener(marker, 'dragend', setMapToMarkerCenter);
			}
			else {
	        	marker.setPosition(latlng);
	        	marker.setTitle (latlng.toString());

			}
		}

		function setMapToMarkerCenter(){
			if(marker != null){
				map.setCenter(marker.getPosition());
				setMapLatLong ()
			}
		}
		var latLongWarningDisplayed = false;
		function latlongCalcWarning (){
			if (latLongWarningDisplayed == false){
				alert("phr_locator_map_address_changed_recalc_needed");
				latLongWarningDisplayed = true;
			}
		}

		//Finds and stores values of Administrative Area Level 1 and 2 for address
		function setadministrativeAreaLevels(resultsAddressComponents) {
			var administrativeAreaLevelOne = "";
			var administrativeAreaLevelTwo = "";
			for (i = 0; i < resultsAddressComponents.length; i++) {
				if (resultsAddressComponents[i].types[0] == "administrative_area_level_1")	 {
					administrativeAreaLevelOne = resultsAddressComponents[i].long_name;
				}
				if (resultsAddressComponents[i].types[0] == "administrative_area_level_2")	 {
					administrativeAreaLevelTwo = resultsAddressComponents[i].long_name;
				}
			}
			document.getElementById("location_administrativeAreaLevel1_#location.locationid#").value = administrativeAreaLevelOne;
			document.getElementById("location_administrativeAreaLevel2_#location.locationid#").value = administrativeAreaLevelTwo;
		}
	<!--- Old code left for reference
	function resetlatlong() {
		page = '/webservices/relayLocatorWS.cfc?wsdl&method=SetandGetLatlong'
	    page = page + '&locationID=#frmLocationID#'

	    var myAjax = new Ajax.Request(
	    page,
	    {
	    	method: 'post',
	        parameters: 'returnFormat=json&_cf_nodebug=true',
	        evalJSON: 'force',
	        debug : false,
	        asynchronous: false
	     }
	     );
	     //alert(myAjax.transport.responseText);
	     //document.frmCompanySearch.frmShowLanguageID.value = parseInt(myAjax.transport.responseText);
	     document.getElementById('location_latitude_#location.locationid#').value=myAjax.transport.responseText.evalJSON().LATITUDE;
	     document.getElementById('location_longitude_#location.locationid#').value = myAjax.transport.responseText.evalJSON().LONGITUDE;
	     var latlngreset = new google.maps.LatLng(myAjax.transport.responseText.evalJSON().LATITUDE,myAjax.transport.responseText.evalJSON().LONGITUDE);
	     <cfif useGoogleMap>map.panTo(latlngreset);</cfif>
	} --->
	</script>
	</div>
	</div>
</cfoutput>
</cf_translate>
