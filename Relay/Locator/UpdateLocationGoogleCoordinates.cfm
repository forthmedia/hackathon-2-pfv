<!--- �Relayware. All Rights Reserved 2014 --->
<!--- OVERVIEW:
File name:			UpdateLocationGoogleCoordinates.cfc	
Author:				AR & NYB
Date started:		2011/05/25
	
Description:	

Possible enhancements:

--->

<!--- Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011/05/25	NYB		Created from code AR create for Lenovo
2011/10/26	NYB		LHID7282 - have rewritten to use new countryscoping method and RequiredFlag specified as part of LocatorDefinition
2012-10-22	IH		Case 431291 add useCFVersion=false to cf_queryParam in getCountrySummaryQry
2013-08-22 	NYB 	Case 436665 moved WHERE
2013-10-02 	NYB 	Case 435669 turned 3 main queries into relayLocator calls - req'd 1 new relayLocator function
2014-03-05 	NYB 	Case 438911 moving bug fixes made on 2013 into 2014 and eVault Locator, and moving some 2014 changes into 2013 - esp limiting updates to countries the user has rights to

 --->

<cfsetting requestTimeout="1440">
<cfparam name="url.refreshCount" default="0">
<cfparam name="url.countryID" default="">
<cfparam name="url.allLocations" default="">
<cfparam name="orgApprovalFlagTextID" type="string" default="#application.com.settings.getSetting('locator.orgApprovalFlagTextID')#">

<cfset url.refreshCount = url.refreshCount + 1>
<!--- START: 2013-10-02 NYB Case 435669 - replaced 
<cfset currentTemplate = listlast(GetCurrentTemplatePath(),'\')> 
Case 435669 - with: --->
<cfset baseTemplatePath = GetBaseTemplatePath()> 
<cfset currentTemplate = listlast(baseTemplatePath,'\')> 
<cfset currentFolder = ListGetAt(baseTemplatePath,(ListLen(baseTemplatePath,'\')-1),'\')> 
<!--- END: 2013-10-02 NYB Case 435669 --->

<!--- START: 2013-10-02 NYB Case 435669 replaced getLocatorDefQry query with: --->
<!--- 2014-03-05 NYB Case 438911 replaced separate argumentStruct entries to this one line, and added limitByCountryRights: --->
<cfset argumentsStruct = {cache = "false",allLocations=url.allLocations,all = "true",limitByCountryRights="true"}>
<cfif isnumeric(url.countryid)>
	<cfset argumentsStruct.countryid = url.countryid>
</cfif>
<cfset argumentsStruct.locatorDefQry = application.com.relayLocator.getLocatorDefinition(argumentCollection=argumentsStruct)>
<cfif application.com.settings.getSetting('versions.locator') neq "" and application.com.settings.getSetting('versions.locator') lte 4> <!--- version 4 and previous has the org approval flag as a required flag. version 5 makes this optional. If that is required, then it will be in the requiredFlagList --->
	<cfset argumentsStruct.filterByFlag = orgApprovalFlagTextID>
</cfif>
<!--- END: 2013-10-02 NYB Case 435669 --->


<cfif len(url.countryID)>
	<!--- We have a country to work on. --->
	<!--- 2013-10-02 NYB Case 435669 replaced getLocationsQry query with: --->
	<cfset getLocationsQry = application.com.relayLocator.getLocationsRequiringLatLongUpdate(argumentCollection=argumentsStruct)>
	
	<cfoutput query="getLocationsQry">
		<cfset geocodeResult = application.com.relayLocator.geocodeLocation (locationid,application.com.structureFunctions.queryRowToStruct(getLocationsQry,currentRow))>


			<!--- The assumption is that we have over grabbed the data --->
			<cfif geocodeResult.status is "OVER_QUERY_LIMIT">
				<cfif getLocationsQry.recordcount AND getLocationsQry.currentrow EQ 1>
					<cfif url.refreshCount lte 50>
						Trying again....#htmleditformat(url.refreshCount)#
						<script>
						//2013-10-02 NYB Case 435669 - replaced locator with #currentFolder#
						window.location =('/#currentFolder#/#jsStringFormat(currentTemplate)#?refreshCount=#jsStringFormat(url.refreshCount)#&countryID=#jsStringFormat(url.countryID)#&allLocations=#jsStringFormat(url.allLocations)#');
						</script>
					<cfelse>
						We have used up our conversions for the day.
					</cfif>
				<cfelse>
					#getLocationsQry.currentrow-1# processed. phr_Locator_#geocodeResult.Status#
					<cfif getLocationsQry.recordcount>
						<script>
						//2013-10-02 NYB Case 435669 - replaced locator with #currentFolder#
						window.location =('/#currentFolder#/#jsStringFormat(currentTemplate)#?countryID=#jsStringFormat(url.countryID)#&allLocations=#jsStringFormat(url.allLocations)#');
						</script>
					</cfif>
				</cfif>
				<CF_ABORT>
			</cfif>		
		#htmleditformat(SiteName)# (#htmleditformat(locationid)#): phr_Locator_#geocodeResult.Status#<br />
	</cfoutput>
	
</cfif> 


<!--- START: 2013-10-02 NYB Case 435669 relaced query with: --->
<cfset argumentsStruct.getSummary = "true">
<cfset getCountrySummaryQry = application.com.relayLocator.getLocationsRequiringLatLongUpdate(argumentCollection=argumentsStruct)>
<!--- END: 2013-10-02 NYB Case 435669 --->

<!--- Display the set of country and their break down --->
<cfif not len(url.countryID)>
		<cfoutput>
		<h3>Outstanding Locations</h3>
		<!--- START: 2011/10/26 NYB removed as the necessity of which is questionable.  Can be put back if needed ->
		<cfif len(url.allLocations)>
			<p><strong>You are viewing ALL locations.</strong> <a href="#currentTemplate#?allLocations=">&gt;&gt;&gt;&gt;&gt;Switch to LOCATOR ONLY locations.&lt;&lt;&lt;&lt;&lt;</a></p>
		<cfelse>
		<!- END: 2011/10/26 NYB removed as the necessity of which is questionable.  Can be put back if needed --->
			<p>Filtered on <cfif application.com.settings.getSetting('versions.locator') neq "" and application.com.settings.getSetting('versions.locator') lte 4>'#rtrim(application.com.settings.getsetting("locator.orgApprovalFlagTextID"))#' and</cfif> the Required Flags specified in the countries Locator Definition.</p>
			<p>Click required country to process.</p>
		<!--- START: 2011/10/26 NYB removed as the necessity of which is questionable.  Can be put back if needed ->
			<p><strong>You are viewing LOCATOR visible locations.</strong> <a href="#currentTemplate#?allLocations=true">&gt;&gt;&gt;&gt;&gt;Switch to ALL locations.&lt;&lt;&lt;&lt;&lt;</a></p>
		</cfif>
		<!- END: 2011/10/26 NYB removed as the necessity of which is questionable.  Can be put back if needed --->
		<!--- 2014-03-05 NYB Case 438911 removed unnecessary cfoutputs --->
		<a href="#currentTemplate#?countryID=ALL&allLocations=#htmleditformat(url.allLocations)#">Process ALL (#htmleditformat(arraySum(listToArray(valueList(getCountrySummaryQry.totalLeft))))#)</a><br/>
		</cfoutput>
	<table bgcolor="lightgrey">
		<tr bgcolor="lightgrey">
	<cfoutput query="getCountrySummaryQry">
		<td>
		<a href="#currentTemplate#?countryID=#countryID#&allLocations=#url.allLocations#">#htmleditformat(countryDescription)#(#htmleditformat(totalLeft)#)</a><br/>
		</td>
		<cfif NOT getCountrySummaryQry.currentRow mod 5 ></tr><tr bgcolor="lightgrey"></cfif>
	</cfoutput>
	</table>
<cfelse>
	<cfoutput>
		<p>Total left: #htmleditformat(arraySum(listToArray(valueList(getCountrySummaryQry.totalLeft))))#</p></cfoutput>
	<cfif getLocationsQry.recordcount>
		<script>
		<cfoutput>
		//2013-10-02 NYB Case 435669 - replaced locator with #currentFolder#
		window.location =('/#currentFolder#/#jsStringFormat(currentTemplate)#?allLocations=#jsStringFormat(url.allLocations)#');
		</cfoutput>
		</script>
	</cfif>
</cfif>

