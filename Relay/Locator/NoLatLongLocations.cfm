<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		NoLatLongLocations.cfm	
Author:			SSS
Date started:	05-05-2009
	
Description:			
This report shows all locations that are flaged for the locator but do not have a lat long
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-June-2009        SSS         added charset to content tag to enable correct encodeing in spreadsheet
2011-11-04			NYB			LHID8039 - Rewrote majority of file for LocatorV3
2012-10-04	WAB		Case 430963 Remove Excel Header 
2013-10-02 	NYB 	Case 435669 added support to make orgApprovalFlagTextID able to be a loc, org or country flag

Possible enhancements:


 --->

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="orgApprovalFlagTextID" type="string" default="#application.com.settings.getSetting('locator.orgApprovalFlagTextID')#">
<cfparam name="sortOrder" default="countrydescription">
<cfparam name="keyColumnList" type="string" default="organisationname,sitename"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnKeyList" type="string" default="organisationID,locationID"><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="keyColumnOnClickList" type="string" default="javascript:void(openNewTab('Account Details ##organisationID##'*comma'Account Details'*comma'/data/dataframe.cfm?frmsrchOrgID=##organisationID##'*comma''));return false,javascript:void(viewLocation(##locationID##*comma'LocatorInformation'*comma{openInOwnTab:true}))"><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnURLList" type="string" default=" , ">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfif openAsExcel>
	<cfset variable.Filename = "NoLatLong">
		
<cfelse>
<cfoutput>

	<cf_title>Locations with no lat/long entries</cf_title>
	
</cfoutput>
<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
<cf_includeJavascriptOnce template = "/javascript/viewEntity.js">
</cfif>

<cf_title>Locations with no lat/long entries</cf_title>

<cf_RelayNavMenu pageTitle="" thisDir="locator">
	<cf_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs();">
</cf_RelayNavMenu>

<!--- START: 2011-11-04 LHID8039 NYB --->
<!--- 2011-11-04 LHID8039 NYB - added to speed up processing time when filtered by one country --->
<cfset filterByCountry = "">
<cfif not structisempty(form)>
	<cfset formKeys = StructKeyList(form)>
	<cfset numFilters = application.com.globalFunctions.ListContainsCount(formKeys,"FILTERSELECT")>
	<cfif numFilters gt 1>
		<cfif form.FILTERSELECT1 eq "countrydescription">
			<cfset filterByCountry = form.FILTERSELECTVALUES1>
		</cfif>
	</cfif>
</cfif>

<!--- 2011-11-04 LHID8039 NYB - added Query getLocatorDefQry --->
<cfquery name="getLocatorDefQry" datasource="#application.siteDataSource#">
	select cs.countryid,rtrim(ltrim(loc.RequiredFlagList)) as RequiredFlagList 
	from locatordef loc with(nolock) 
	inner join countryscope cs with(nolock) on cs.entityid = loc.locatorid 
	<cfif filterByCountry neq "">
		and cs.countryid in (select countryid from country where countrydescription=<cfqueryparam cfsqltype="CF_SQL_LONGVARCHAR" value="#filterByCountry#">)
	</cfif>
	and cs.countryID in (#request.relayCurrentUser.countryList#)
	and entity='LocatorDef' 
</cfquery>

<!--- 2013-10-02 NYB Case 435669 added: --->
<cfset approvalflagStruct = application.com.flag.getFlagStructure(flagID=orgApprovalFlagTextID)>

<!--- 2011-11-04 LHID8039 NYB - Rewrote Query --->
<cfquery name="getgeocodedata" datasource="#application.siteDataSource#">
	select * from (
		select l.locationID, l.sitename, l.address1, l.address2, l.address3, l.address4, l.address5, l.Latitude, l.longitude, o.organisationID, o.organisationname, c.countrydescription
		from 
		organisation o 
		inner join location l on l.organisationID = o.organisationID and l.Latitude is null and l.longitude is null
		inner join country c on l.countryID = c.countryID and c.countryID in (#request.relayCurrentUser.countryList#)
		<cfif application.com.settings.getSetting('versions.locator') neq "" and application.com.settings.getSetting('versions.locator') lte 4> <!--- version 4 and previous has the org approval flag as a required flag. version 5 makes this optional. If that is required, then it will be in the requiredFlagList --->
			<!--- 2013-10-02 NYB Case 435669 replaced line: --->
			inner join #approvalflagStruct.FLAGTYPE.DATATABLEFULLNAME# b with(nolock) on l.#approvalflagStruct.FLAGGROUP.ENTITYTYPE.UNIQUEKEY# = b.entityID and b.flagid=#approvalflagStruct.FlagID#
		</cfif>
		where 
			(
			<cfset counter = 0>
			<cfset maxrow=0>
			<cfset outerFirstRun = true>
			<cfoutput query="getLocatorDefQry" group="RequiredFlagList">
				<cfset validFlags = StructNew()>
				<!--- construct valid flag list for this locatordef --->
				<cfloop index="i" list="#RequiredFlagList#">
					<cfset flagDetails = application.com.flag.getFlagStructure(flagid=i)>
					<cfif flagDetails.isOK>
						<cfif ListFindNoCase("organisation,location",flagDetails.ENTITYTYPE.TABLENAME) gt 0>
							<cfif not structkeyexists(validFlags,flagDetails.ENTITYTYPE.TABLENAME)>
								<cfset validFlags[flagDetails.ENTITYTYPE.TABLENAME] = StructNew()>
							</cfif>
							<cfset validFlags[flagDetails.ENTITYTYPE.TABLENAME][flagDetails.FLAGTEXTID] = flagDetails.FLAGID>
						</cfif>
					</cfif>
				</cfloop>
				<cfif not StructIsEmpty(validFlags) or len(rtrim(ltrim(RequiredFlagList))) eq 0><!--- ie, if a flag is put in RequiredFlagList that doesn't exist then return no results for those countries --->
					<cfif outerFirstRun><cfset outerFirstRun = false><cfelse>
						or 
					</cfif>
					(
						<cfset innerFirstRun = true>
						<cfset validFlagEntities = StructKeyList(validFlags)>
						<cfloop index="tableName" list="#validFlagEntities#">
							<cfset validFlagTextIDs = StructKeyList(validFlags[tableName])>
							<cfloop index="j" list="#validFlagTextIDs#">
								<cfif innerFirstRun><cfset innerFirstRun = false><cfelse>
									AND
								</cfif>
								<cfset counter++>
									l.#tableName#id in 
									(
										select entityid from BooleanFlagData b#counter# with(nolock) where b#counter#.flagid =  <cf_queryparam value="#validFlags[tableName][j]#" CFSQLTYPE="CF_SQL_INTEGER" > 
									) 
							</cfloop>
						</cfloop>
						<cfif innerFirstRun><cfset innerFirstRun = false><cfelse>
							AND
						</cfif>
						l.countryid in 
							(
							<cfoutput><cfset maxrow++></cfoutput>
							<cfoutput >  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" > <cfif currentrow lt maxrow>,</cfif></cfoutput>
							)
					)
				</cfif>
			</cfoutput>
			)
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	) as data 
	order by <cf_queryObjectName value="#SortOrder#">
</cfquery>
<cfoutput>
<!--- END: 2011-11-04 LHID8039 NYB --->

<CF_tableFromQueryObject queryObject="#getgeocodedata#" 
	showTheseColumns="organisationID,organisationname,locationID,sitename,address1,address2,address3,address4,address5,countrydescription"
	ColumnHeadingList="OrganisationID,Organisation,LocationID,Location,Address1,Address2,address3,address4,address5,Country"
	useInclude="true"
	openAsExcel="#openAsExcel#"
	FilterSelectFieldList="countrydescription"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOnClickList="#keyColumnOnClickList#"
>
</cfoutput>