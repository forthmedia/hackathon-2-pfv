<!--- �Relayware. All Rights Reserved 2014 --->
<cfsilent>
<!--- 
File name:			googleSiteMap.cfm	
Author:				SWJ
Date started:		2006-03-04
	
Description:		This file generates a sitemap that conforms to googles requirements

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<!--- a request scope variable TopElementIDPartnerContent can be defined in RelayINI 
		to override the one in the application scope  --->
<cfif isDefined("request.TopElementIDPartnerContent")>
	<cfset variables.treeTopElementID = request.TopElementIDPartnerContent>
<cfelse>
	<cfset variables.treeTopElementID = application.TopElementIDPartnerContent>
</cfif>
 
<CFPARAM NAME="mapElementID" DEFAULT="#variables.treeTopElementID#">

<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID= variables.treeTopElementID)> 

</cfsilent>

<cfprocessingdirective suppresswhitespace="Yes">
	<!--- <cfcontent type="text/xml; charset=utf-16">
	
	<cfheader name="content-Disposition" value="#request.CurrentSite.Title#-GoogleSiteMap.xml"> --->
	<cfxml variable="urlset">
	<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">
		<cfoutput query="getElements"	>
			<url>
			    <loc>#request.currentSite.httpProtocol##htmleditformat(request.relayCurrentUser.sitedomain)#/?eid=<cfif elementTextID neq "">#htmleditformat(elementTextID)#<cfelse>#htmleditformat(node)#</cfif></loc>
			    <changefreq>weekly</changefreq>
			    <priority>0.5</priority>
		  	</url>
		</cfoutput>
		</urlset>
	</cfxml>
</cfprocessingdirective>
<cffile action="write" addnewline="yes" file="d:\\web\\relay\\#siteDataSource#-GoogleSiteMap.xml" output="#urlset#" fixnewline="yes">
<cfoutput>#htmleditformat(urlset)#</cfoutput>
