<!--- �Relayware. All Rights Reserved 2014 --->
<!---


WAB: controlContentDisplay.cfm

Used to allow a user to control what content is shown on a portal

available to anyone who has  request.relaycurrentuser.content.ShowLinkToControlContent set

am considering another version of this template which  works across different sessions (so that user can be logged in to internal site and control an external site)

2005-10-10 WAB added a reset button
		  WAB	resize window on loading

2006-05-10 added code to allow user to hide the control content link

 --->





<cf_head>
	<cf_title>Control Content</cf_title>
	<cfoutput>
		<cfif fileexists("#application.paths.code#\styles\relayWareStylesV2.css")>
			<LINK REL='stylesheet' HREF='/code/styles/relayWareStylesV2.css'>
		<cfelse>
			<LINK REL='stylesheet' HREF='/Styles/relayWareStylesV2.css'>
		</cfif>
	</cfoutput>

</cf_head>






<cfif request.relayCurrentUser.content.AllowedToSeeLinkToControlContent>


				<!--- do the update --->
				<cfif isDefined("frmTask")>
					<cfif frmTask is "update">
						<cfset application.com.relayCurrentUser.updateContentSettings (
							status = #frmShowContentStatus#,
							countryid		= #frmShowContentForCountryID#,
							ShowTranslationLinks = #iif(frmShowTranslationLinks is 1,de(true),de(false))#,
							contentdate = #frmShowContentDate#,
							showElementIDs = #frmElementIDs#,
							ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = #frmShowLinkToControlContentEvenIfILogOnAsSomeoneElse#,
							ShowLinkToControlContent = #iif(frmShowLinkToControlContent,de(true),de(false))#
						)
						>

						<cfset application.com.relayCurrentUser.setLanguage (frmLanguageID)>

					<cfelse>

						<cfset application.com.relayCurrentUser.updateContentSettings (
								status = 4,
								countryid		= #request.relaycurrentUser.countryid#,
								ShowTranslationLinks = false,
								contentdate = "",
								showElementIDs = "",
								ShowLinkToControlContentEvenIfILogOnAsSomeoneElse = false
							)
							>

							<cfset application.com.relayCurrentUser.setLanguage (request.relaycurrentuser.person.language)>


					</cfif>


					<script>

						window.opener.location.reload();
						window.opener.focus () ;

					</script>
				</cfif>

				<cfif isDefined("frmLogTranslationRequests") or isDefined("session.LogTranslationRequests")>
					<cfif isDefined("frmLogTranslationRequests")>
						<cfset session.LogTranslationRequests = 1>
						<cfset session.LogTranslationAppIdentifier = frmLogTranslationAppIdentifier>
					<cfelse>
						<cfset session.LogTranslationRequests = 0>
					</cfif>


				</cfif>



			<!--- <CFQUERY NAME="Countries" DATASOURCE="#application.SiteDataSource#">
			select
				0 as dataValue,
				'Any Country' as displayValue,
				0 as sortOrder
			Union
			SELECT
				countryid,
				countrydescription,
				case when isoCode is null then 1 else 2 end as sortorder
			FROM
				Country
			Where countrygrouptypeid =1
			Order by
				sortorder, displayValue
			</cfquery> --->

			<!--- Sugar 448650 NJH 2016/03/18 - remove 'Any Country' - use function to return countries --->
			<cfset countries = application.com.relayCountries.getCountriesValidValues(liveCountriesOnly=false)>

			<CFQUERY NAME="Languages" DATASOURCE="#application.SiteDataSource#">
			SELECT
				l.Languageid as dataValue, l.language as displayvalue

			FROM
				Language as l
			order by language
			</cfquery>

			<CFQUERY NAME="Statii" DATASOURCE="#application.SiteDataSource#">
			select '0' datavalue, 'Draft and Live' as displayvalue
			union
			select '4' , 'Live'
			<!--- select statusid as datavalue,
			status as displayvalue from elementstatus	 --->
			</cfquery>


		<cfoutput>



	<cfset user =  request.relayCurrentUser>

		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
			<tr><td colspan=2>As a Content Editor you may preview this Site in different ways.<BR>
			 </td><td>


		<form action="" method="post" name = "myForm">


			<tr><td>Show content of status ..</td><td>

						<CF_displayValidValues
						formFieldName = "frmshowContentStatus"
						validValues="#statii#"
						currentValue="#user.content.Status#"
						showNull = false>

	</td></tr>

	<tr><td>
		Show me the site as it will appear on this date</td><td>

					<cfif isdefined("user.content.Date")>
						<cfset theDate = user.Content.Date>
					<cfelse>
						<cfset thedate = "">
					</cfif>

					<cf_relaydatefield
							currentValue="#theDate#"
							thisFormName="myForm"
							fieldName="frmShowContentDate"
							anchorname = "D"
							showclearlink = yes
							clearLinkText = "<BR>Click to clear (ie.use current date)">
	</td></tr>


	<tr><td>
			Show content in this Language 			</td><td>
				<CF_displayValidValues
						formFieldName = "frmLanguageID"
						validValues="#Languages#"
						currentValue="#user.LanguageID#"
						showNull = false>
	</td></tr><tr><td>
			Show content as if I am in this Country 	</td><td>
			<CF_displayValidValues
						formFieldName = "frmShowContentForCountryID"
						validValues="#Countries#"
						currentValue="#user.Content.ShowForCountryID#"
						showNull = false>
	</td></tr><tr><td>
		Show Translation Links</td><td>
					<CF_displayValidValues
						formFieldName = "frmShowTranslationLinks"
						validValues="0#application.delim1#No,1#application.delim1#Yes"
						currentValue="#iif(user.content.ShowTranslationLinks,de("1"),de("0"))#"
						showNull = false>

	</td></tr>

	<tr><td>
		Show the Control Content Link </td><td>
					<CF_displayValidValues
						formFieldName = "frmShowLinkToControlContent"
						validValues="0#application.delim1#No,1#application.delim1#Yes"
						currentValue="#iif(user.content.ShowLinkToControlContent,de("1"),de("0"))#"
						showNull = false>

	</td></tr>


	<tr><td>
		Remember these settings even if I log on as someone else</td><td>
					<CF_displayValidValues
						formFieldName = "frmShowLinkToControlContentEvenIfILogOnAsSomeoneElse"
						validValues="0#application.delim1#No,1#application.delim1#Yes"
						currentValue="#iif(user.content.ShowLinkToControlContentEvenIfILogOnAsSomeoneElse,de("1"),de("0"))#"
						showNull = false>

	</td></tr>

	<tr><td>
		Allow me to access these pages regardless of rights or status<BR>
		(comma separated list of ids)</td><td>
		<CF_INPUT type="text" name ="frmElementIDs"  value = "#user.content.AlwaysShowTheseElementIDs#">
	</td></tr>


	<tr><td>
		Record all translations used during my session (Advanced)<BR>
		Enter an identifier (needs to be unique)
		</td><td>
		<input type="checkbox" name ="frmLogTranslationRequests"  value = "1" <cfif isdefined("session.logtranslationrequests") and session.logtranslationrequests>checked</cfif>>
		<BR><input type="text" name ="frmLogTranslationAppIdentifier"  value = "<cfif isdefined("session.logtranslationAppIdentifier") >#htmleditformat(session.logtranslationAppIdentifier)#</cfif>">
		<cfif isdefined("session.logtranslationAppIdentifier") >
		<BR><a href="#request.currentSite.httpProtocol##request.currentsite.internaldomain#\translation\analysephraserequest.cfm?frmAppIdentifier=#session.logtranslationAppIdentifier#">View Details of logged items</a></cfif> <!--- going to internal domain to get styles --->
	</td></tr>



	<td><td>
			<input name ="frmTask" type="submit" value = "Update">
	</td></tr>
		</form>



		<form action="" method="post" name = "myForm2">
	<td><td>
			<input name ="frmTask" type="submit" value = "Reset">
	</td></tr>
		</form>


</table>
		</cfoutput>



</cfif>





<!--- WAB 2005-10-10 when the window is being opened, resize it (saves getting the size right in everywhere where windowOpen is called--->
<cfif not isDefined("frmTask")>
	<script>
	if (self.resizeTo) {      // this tests if the method is available
		self.resizeTo(700,400);
	}	else

	{
	}
	</script>
</cfif>



