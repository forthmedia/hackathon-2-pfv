/* �Relayware. All Rights Reserved 2014 */
/*
File name:			callWebService.cfc	
Author:				
Date started:		RJT 2016/07/11
	
Description:			

A file which is used to call other webservices, intended for comunications "within" relayware. This endpoint iself has a single security level
of trusted IPs only and additionally expects a security token

*/
component accessors=true output=false persistent=false extends="webservices.callWebServiceBase" {
	
	
	/**
	 * All calls to thisw service must have a header of authorization as below. Other than that (and an IP restriction to internal IP addressed these services act like 
	 * the non secure versions 
	 * 
	 * Authorization : Bearer 0x109b7ceb8f4029ee3fd55bda24551433d8ac06273ae18ddae3138ea1ed96d3ea4c2ee00204183b5c978b4d385bf540c174c1
	 * 
	 * Bearer Token can be obtained from sql: select value from settings where name = 'security.internalWebserviceToken'
	 */
	remote any function callWebService(required webservicename, required methodname) output="false" {
		var webservicesFolder="WebServicesSecure";
		
		var componentAndMethod = getComponentAndMethodPointer(webservicename = webservicename, methodname = methodname, webservicesFolder=webservicesFolder);
		
		var args = cleanseInputArguments(arguments);
		
		var headers=GetHttpRequestData().Headers;
		
		var result="";
		if (not StructKeyExists(headers,"Authorization")){
			var errorID=application.com.errorHandler.recordRelayError_Warning(Severity="INFORMATION",Message="No Authorization header passed");
			result='{"message":"Forbidden, see logs #errorID#"}';
		}else{
			var validityReport=isValidWebserviceToken(headers.Authorization);
				
			if (validityReport.isOk){
				result = invoke(ComponentAndMethod.component,methodname,args);
			}else{
				result='{"message":"Forbidden, see logs #validityReport.errorID#"}';
			}
		}
		return prepareOutputResult(result);
		
	}
	
	/**
	* Checks if a token is expected for internal communication
	* 
	* token: this will be preappended with "Bearer " so for example "Bearer 0x1f766baffe22c85d9562d70ec03c9312e92414a789f45890d8d6ae758246899e55678a28e3021d663d72c508152fe7ce5f3b"
	*/
	private struct function isValidWebserviceToken(required String token){
		var requiredWebserviceToken="Bearer " & application.com.settings.getSetting("security.internalWebserviceToken");
		
		if (requiredWebserviceToken EQ "Bearer "){
			var errorID=application.com.errorHandler.recordRelayError_Warning(Severity="INFORMATION",Message="webserviceToken setting was missing from settings");
			return {isOk=false,errorID=errorID};
		}
		
		if(requiredWebserviceToken.equals(token)){ //.equals() to make case sensitive
			return {isOk=true};	
		}else{
			var errorID=0;
			if (not token.startsWith("Bearer ")){
				errorID=application.com.errorHandler.recordRelayError_Warning(Severity="INFORMATION",Message="Authorization header did not start with 'Bearer '");
			}else{
				errorID=application.com.errorHandler.recordRelayError_Warning(Severity="INFORMATION",Message='webserviceToken "#token#" was passed as Authorization but that did not match the value held in the database');
			}
			
			
			return {isOk=false,errorID=errorID};
		}
	}
	
}

