/**
 * application
 * 
 * @author Richard.Tingle
 * @date 29/06/16
 **/
component accessors=true output=true persistent=false extends="applicationMain" {


	public void function onRequestStart(scriptName=cgi.script_name, suppressOutput=false) output="true" {
		//do not call application.com.relayCurrentUser.refresh() to make sure we don't pointlessly create expensive session variables
		
		securityObj = application.com.security;
		var securityResult = securityObj.testFileSecurity(scriptname = scriptname, isLoggedIn="false", isInternal=true);
		
		if (!securityResult.isOK){
			application.com.errorHandler.recordRelayError_Warning(Severity="INFORMATION",Message="Call to secure webservices failed security checks", WarningStructure=securityResult);
			include "securityFailure.cfm";
			abort;
		}
	}
	
}