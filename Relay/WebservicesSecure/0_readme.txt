These webservices are intended for internal communication; e.g. for the java application to call.

They run outside of a true session, so don't try to use request.relayCurrentUser etc