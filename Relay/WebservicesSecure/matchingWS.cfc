/**
 * matching
 * 
 * @author Richard.Tingle
 * @date 29/06/16
 **/
component accessors=true output=false persistent=false {
	
	/**
	 * See doesOrganisationExist for style of webservice call
	 **/
	public any function doesPersonExist(required personDetails) returnFormat="json"{
		return application.com.relayEntity.doesPersonExist(personDetails=deserializejson(personDetails));
	}
	
	/**
	 * See doesOrganisationExist for style of webservice call
	 **/
	public any function doesLocationExist(required locationDetails) returnFormat="json"{
		return application.com.relayEntity.doesLocationExist(locationDetails=deserializejson(locationDetails));
	}
	

	/**
	 * Example call to webservice 
	 * 
	 * URL: [DOMAIN]/webservicesSecure/callwebserviceSecure.cfc?wsdl&method=callWebService&webserviceName=matchingWS&methodName=doesOrganisationExist&returnFormat=json&ORGANISATIONDETAILS={"organisationName":"Relayware Inc","countryID":9}
	 * Headers: 
	 * 		Authorization : Bearer 0x109b7ceb8f4029ee3fd55bda24551433d8ac06273ae18ddae3138ea1ed96d3ea4c2ee00204183b5c978b4d385bf540c174c1
	 * 
	 * Bearer Token can be obtained from sql: select value from settings where name = 'security.internalWebserviceToken'
	 **/
	public any function doesOrganisationExist(required organisationDetails) returnFormat="json"{		
		return application.com.relayEntity.doesOrganisationExist(organisationDetails=deserializejson(organisationDetails));
	}
	
	/**
	 * Example call to webservice 
	 * 
	 * URL: [DOMAIN]/webservicesSecure/callwebserviceSecure.cfc?wsdl&method=callWebService&webserviceName=matchingWS&methodName=doesEntityExist&ENTITYTYPEID=0&ENTITYDETAILS={"firstname":"Test"}&COLUMNSTOUSE=firstname&returnFormat=json
	 * Headers: 
	 * 		Authorization : Bearer 0x109b7ceb8f4029ee3fd55bda24551433d8ac06273ae18ddae3138ea1ed96d3ea4c2ee00204183b5c978b4d385bf540c174c1
	 * 
	 * Bearer Token can be obtained from sql: select value from settings where name = 'security.internalWebserviceToken'
	 **/
	public any function doesEntityExist(required numeric entityTypeID, required entityDetails, required columnsToUse) returnFormat="json"{		
		return application.com.relayEntity.doesEntityExist(entityTypeID=entityTypeID,entityDetails=deserializejson(entityDetails), columnsToUse=columnsToUse);
	}
	
}