<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		domainProfiles.cfm	
Author:			NJH  
Date started:	23-03-2011
	
Description:	Apply profiles to domains		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="flagGroupID">
<cfset flagGroupStruct = application.com.flag.getFlagGroupStructure(flagGroupID=flagGroupID)>

<cfoutput>
	<cf_title>Apply profile '#flagGroupStruct.name#' To Domains</cf_title>
</cfoutput>

<cfset entityTableName = flagGroupStruct.entitytype.tablename>
<cfset successDomains = structNew()>
<cfset erroredDomains = structNew()>
		
<cfset domainsQry = application.com.jasperServer.getDomainsQuery(restrictByCurrentUserRights=true,excludePermissionMasks="0,32")> <!--- NJH remove execute only and 'no access' domains... --->

<cfif structKeyExists(form,"entityTables")>
	<cfloop list="#form.entityTables#" index="entityTable">
		<cfset domainID = listFirst(entityTable,"|")>
		
		<cfquery name="getDomainName" dbType="query">
			select label from domainsQry where schemaId=#domainID#
		</cfquery>
		
		<cfset domainName = getDomainName.label>
		<cfset tableID = listLast(entityTable,"|")>
		<cfset result = application.com.jasperServer.addFlagGroupToXML(domainId=domainID,tableID=tableID,flagGroupId=flagGroupID)>
		
		<!--- get the friendly name for the tableID - get the label of the item group that the join tree is in--->
		<cfset getDomainXML = application.com.jasperServer.readResourceXML(id=domainID)>
		<cfif getDomainXML.isOK>
			<cfset domainXML = getDomainXML.xml>
		
			<cfset joinTree = application.com.jasperServer.getJoinTreeForTable(domainXML=domainXML,tableID=tableId)>
			<cfset tableItemGroup = application.com.jasperServer.getItemGroupForTable(domainXML=domainXML,joinTreeID=joinTree,tableId=tableId)>
			<cfif tableItemGroup neq "" and structKeyExists(tableItemGroup.xmlAttributes,"label")>
				<cfset tableId = tableItemGroup.xmlAttributes.label>
			</cfif>
		</cfif>
		
		<cfif result.isOK>
			<cfif not structKeyExists(successDomains,domainName)>
				<cfset successDomains[domainName] = "">
			</cfif>
			<cfset successDomains[domainName] = listAppend(successDomains[domainName],tableID,"|")>
		<cfelse>
			<cfif not structKeyExists(erroredDomains,domainName)>
				<cfset erroredDomains[domainName] = structNew()>
			</cfif>
			<cfset erroredDomains[domainName][tableID] = result.message>
		</cfif>
	</cfloop>
</cfif>

<cfoutput>
	<p>Apply profile '<b>#flagGroupStruct.name#</b>' to the following domains:</p>
	<div class="infoblock">
	Once a profile has been applied to a domain, it cannot be removed.
	</div>
	<br>
	<cfif not structIsEmpty(erroredDomains)>
		<div class="errorblock">
		The following domains did not have the profile successfully applied:<br>
		<cfloop collection="#erroredDomains#" item="domainName">#htmleditformat(domainName)#
			<ul>
				<cfloop collection="#erroredDomains[domainName]#" item="tableID">
				<li>#htmleditformat(tableID)#: #erroredDomains[domainName][tableID]#</li>
				</cfloop>
			</ul>
		</cfloop>
		</div><br>
	</cfif>
	
	<cfif not structIsEmpty(successDomains)>
	<div class="successblock">
		The following domains have had the profile successfully applied:
		<cfloop collection="#successDomains#" item="domain"><br><b>#htmleditformat(domain)#</b>
			<ul>
				<cfloop list="#successDomains[domain]#" index="tableID" delimiters="|">
					<li>
					#htmlEditFormat(tableID)#
					</li>
				</cfloop>
			</ul>
		</cfloop>
	</div>
	</cfif>
</cfoutput>

<cfform id="mainForm"  method="post" onSubmit="if (confirm('Are you sure you want to apply these profiles')){document.getElementById('frmApplyProfiles').disabled=true;return true} else {return false}">
	<cf_relayFormDisplay>
	<tr>
		<th>Domain</th><th>Tables</th><th></th>
	</tr>
	<!--- WAB 2012-05-30 Mods so that does not list tables which are not in join trees--->
 	<cfloop query="domainsQry">
		<cfset getdomainXML = application.com.jasperServer.readResourceXML(id=schemaID)>
		<cfif getDomainXML.isoK>
			<cfset domainXML = getdomainXML.xml>

			<cfset tablesArray = application.com.jasperServer.getEntityTablesFromDomain(domainXML=domainXML,entityTableName=entityTableName)>

			<cfif arrayLen(tablesArray)>
				<cfset firstpass = true>
					<cfloop array="#tablesArray#" index="tableNode">
						<cfset joinTreeTest = application.com.jasperServer.getJoinTreeForTable(domainXML,tableNode.xmlAttributes.id)>

						<cfset tableItemGroup = "">
						<cfif joinTreeTest is not "">
							<cfif firstpass>
								<cfoutput><tr><td colspan="2">#htmleditformat(label)# <cfif len(description)>(#htmleditformat(description)#)</cfif></td><td></td></tr></cfoutput>
								<cfset firstpass = false>
							</cfif>
							<!--- use for a more friendly display of tablenames. Use the item group label that the join tree is in --->
							<cfset tableItemGroup = application.com.jasperServer.getItemGroupForTable(domainXML=domainXML,joinTreeID=joinTreeTest,tableId=tableNode.xmlAttributes.id)>
							<cfset flagGroupApplied = application.com.jasperServer.hasProfileBeenAppliedToDomain(domainXML=domainXML,flagGroupID=flagGroupId,tableID=tableNode.xmlAttributes.id)>
							<cfoutput><tr><td></td><td><cfif tableItemGroup neq "" and structKeyExists(tableItemGroup.xmlAttributes,"label")>#htmlEditFormat(tableItemGroup.xmlAttributes.label)#<cfelse>#htmlEditFormat(tableNode.xmlAttributes.id)#</cfif></td><td><CF_INPUT type="checkbox" value="#domainsQry.schemaID#|#tableNode.xmlAttributes.id#" id="entityTables" name="entityTables" checked="#iif(flagGroupApplied,true,false)#" disabled="#iif(flagGroupApplied,true,false)#"></td></tr>	</cfoutput>
						</cfif>
					</cfloop>

			</cfif>
		</cfif>
	</cfloop>
	
	<tr><td colspan="2"></td><td><input type="submit" value="Apply Profile" id="frmApplyProfiles" name="frmApplyProfiles"></td></tr>
	
	</cf_relayFormDisplay>
</cfform>
