<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB 2011/09 Added a try catch to prevent huge numbers of errors on dev boxes particularly
2012/01/18 - RMB - P-LEN24 - CR066 Added script to set jasperServerLoggedIn once the form sumbits / page in unloaded.
WAB 2012-05-14 CASE 434001  Reconfigured the code for detecting the jasperServerLogin.  Now uses an onload listener on the iframe (using javascript\jasperserverloaddetect.js) which is called from the parent page, not from here
 --->



<cfoutput>
	<cftry>
		<!--- here we delete the user just before they SSO, so that they are current in terms of their roles, etc. This is needed due to a bug
			in the PS code done by Jasperserver, as roles are added but not deleted. --->
		<!--- WAB 2012-10-04 Cae 431042 remove this code, causes havoc.  Will need to deal with roles in another way 
		<cfif application.com.jasperServer.getJasperServerUserIDForCurrentUser() neq "" and not request.relayCurrentUser.jasperServerIsLoggedIn>
			<!--- <cfset application.com.jasperServerWebserviceCaller.deleteUser(user=application.com.jasperServer.getJasperServerUserNameForCurrentUser())> --->
			<cfset application.com.jasperServerWebserviceCaller.deleteUser(user=application.com.jasperServer.getJasperServerUserNameForCurrentUser())>
		</cfif>
		--->
	
		<cfset jasperServerParams = application.com.jasperServer.getJasperServerLoginParamsForCurrentUser()>
		<cfcatch>
			<cfif cfcatch.message is "Object Instantiation Exception.">
				<cfoutput>The Jasperserver Connector is not set up on this server</cfoutput>
				<CF_ABORT>			
			</cfif>
			<cfrethrow>
		</cfcatch>
	</cftry>
	
	<cfparam name="jasperServerURL" type="string" default="#application.com.jasperServer.getJasperServerPath()#/index.htm">

	<cf_body >
	<form name="jasperServerLoginForm" action="#jasperServerUrl#" method="post">
		<CF_INPUT type="hidden" name = "#replace(listfirst(jasperServerParams,"="),"&","")#" value="#listLast(jasperServerParams,"=")#">
	</form>

	<script>
		document.jasperServerLoginForm.submit();
	</script>


</cfoutput>