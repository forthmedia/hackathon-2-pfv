<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Page to List Jasperserver Reports

WAB 2012-07-09 Changed the function which brings back list to use a query (rather than webservice)
			Added support for myLinks and linking reports to Modules
 --->

<cf_includejavascriptonce template = "/javascript/extExtension.js">
<cf_includejavascriptonce template = "/jasperserver/jasperserver.js">

<cfset jasperserverFunctions = createObject ("component","relay.com.jasperServerWebServiceCaller")>

<style>
	ul li{list-style:none;}
	ul,ul ul ,ul li{margin-left:10px;}
	ul li .bullet{padding-left:10px;}
</style>


<cfset reportsQuery = jasperserverFunctions.getReportsQuery()>
<cfset jasperserverFunctions.outputReportsQuery(reportsQuery)>

