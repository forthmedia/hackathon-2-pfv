<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2012-05-14 CASE 434001  Reconfigured the code for detecting the jasperServerLogin.  Now uses an onload listener on the iframe (using javascript\jasperserverloaddetect.js)
2015-02-04 AHL 443498 Page asks to reload when putting a report on it
 --->
<cfparam name="resource" default="report">
<cfparam name="scrolling" default="no">
<cfparam name="request.resourceLoaded" type="numeric" default=0>

<cfset resourceURI = replace(resourceURI," ","_","ALL")>
<cfset ID=replaceNoCase(CreateUUID(),"-","","ALL")>

<cfset request.resourceLoaded++>

<cf_head>
	<cfoutput>
		<script type="text/javascript">
			function on#Resource#Load(ID)
			{
				divObj = document.getElementById('#resource#LoadingDiv'+ID);
				divObj.style.display='none';
			}
		</script>
	</cfoutput>
</cf_head>

<cfif isNumeric("width")>
	<cfset width = "#width#px">
</cfif>
<cfif isNumeric("height")>
	<cfset height = "#height#px">
</cfif>

<cfset iFrameSrc = "/templates/displayJasperServerResource.cfm?resourceURI=#resourceURI#&resource=#resource#">

<cfif (request.resourceLoaded gt 1 and not request.relayCurrentUser.isJasperServerLoginConfirmed) or not request.currentSite.isInternal>		<!---- 2015-02-04 AHL 443498 Page asks to reload when putting a report on it --->
	
	<!--- if we have more than on resource on a page, then create a little time delay before loading any resources after the first one, to
		ensure that we have the correct JasperServer session, rather than creating a new one --->
	
	<cfoutput>
		<script>
			function load#Resource#(ID) {
				if (window.jasperServerLoggedIn) {
					iFrameObj = document.getElementById('iframe'+ID);
					iFrameObj.src='#iFrameSrc#';
				} else {
					load#resource#Refresh(ID);
				}
			}
			
			function load#resource#Refresh(ID) {
				setTimeout( function(){load#Resource#(ID);},1000)
			}
			
			load#resource#Refresh('#jsStringFormat(ID)#')
		</script>
	</cfoutput>
	<!---
	  If we are not logged in (we have no cookies from jaspersoft) we need to initialize them.
	Jasper Report are reloading after our call, which must be a POST (to eneable our session
	param). The result of that is a security conflict which makes the browser to ask to the
	user to re-send the original inputs in the post menthod. In case of being the first request
	we will ask for a home page instead of the report. Hence, the page will not redirect, and
	we will avoid the security exception. The frame will be reload afterwards with the correct
	resutls.
	--->
	<cfset iFrameSrc = (request.resourceLoaded gt 1)?"":"/templates/displayJasperServerResource.cfm?resource=">		<!---- 2015-02-04 AHL 443498 Page asks to reload when putting a report on it --->
</cfif>

<cfset reportLabel = application.com.jasperServer.getReportsQuery(showInternalExternal='external',uri=reportURI).label>
<cfoutput>
	<script>
		jQuery(document).ready(function(){
			jQuery(".showReport").fancybox({
				autoSize: false,
				width: '90%',
				height: '90%',
				type: 'iframe'
			});
		});
	</script>
	<div id="showReport#ID#" class="showReportContainer visible-xs"><a class="showReport" href="#htmleditformat(iFrameSrc)#">#reportLabel#</a></div>
	<div class="hidden-xs" id="#resource#LoadingDiv#ID#" width="100%" align="center">phr_loading#resource#PleaseWait <img src="/images/loading/facebook.gif"></div>
	<iframe class="hidden-xs" id="iframe#htmlEditFormat(ID)#" onload="on#resource#Load('#ID#')" src="#htmleditformat(iFrameSrc)#" width="#htmleditformat(width)#" height="#htmleditformat(height)#" scrolling="#scrolling#" frameborder="no">
	</iframe>

<cfif  request.resourceLoaded eq 1 and (not request.currentSite.isInternal or not request.relayCurrentUser.isJasperServerLoginConfirmed)>		<!---- 2015-02-04 AHL 443498 Page asks to reload when putting a report on it --->
	<cf_includeJavascriptOnce template="/javascript/jasperserverLoadDetect.js">

	<script>
		jasperserverLoadDetect ('iframe#htmlEditFormat(ID)#')
	</script>
</cfif>
</cfoutput>