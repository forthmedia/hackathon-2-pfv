<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				
Date started:			/xx/07
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cfset jasperserverFunctions = createObject ("component","relay.com.jasperServerWebServiceCaller")>
<cfset jasperserverFunctions.initWebService()>



<cfparam name="directory" default = "/organizations/#lcase(application.sitedatasource)#/adhoc/aru ">

<cfset listXML = jasperserverFunctions.list(uristring = "#directory#", wstype="folder")>


<cfset listxmldoc = xmlparse(listXML)>
<!---<cfdump var="#listXMLdoc#">--->
	<cfset reportsXML = xmlsearch(listxmldoc,"//resourceDescriptor[@wsType='reportUnit']") >  
	<cfset foldersXML = xmlsearch(listxmldoc,"//resourceDescriptor[@wsType='folder']") >  


<cfoutput>
	Directory #directory# <BR>

	<table>
		<tr><td><a href="?directory=#reverse(listRest(reverse(directory),"/"))#">Up</a></td></tr>
		<cfif arrayLen(foldersXML) gt 0>
		<tr><th>Sub Folders</th></tr>
	<cfloop index="i" from = "1" to="#arrayLen(foldersXML)#">
		<cfset thisFolder = foldersXML[i]>
		<tr>
			<td>
				<a href="?directory=#thisFolder.xmlattributes.uriString#">#thisFolder.label.xmltext#</a>
			</td>
		</tr>
	</cfloop>
	</cfif>
	
	<tr><th><cfif arrayLen(reportsXML) gt 0>Reports<cfelse> No Reports</cfif>	</th></tr>	
	<cfloop index="i" from = "1" to="#arrayLen(reportsXML)#">
		<cfset thisReport = reportsXML[i]>
		<cfoutput><tr><td>#thisReport.label.xmltext#</td> 	<!--- <td>#thisReport.xmlattributes.name# </td> --->
		<!--- <td><a href="?reportURI=#thisReport.xmlattributes.uristring#">Run</a></td> --->
		<cfset link = "#jasperserverFunctions.getjasperserverpath()#/flow.html?j_username=#request.relaycurrentuser.person.username#&j_password=#request.relaycurrentuser.person.password#&_flowId=viewReportFlow&ParentFolderUri=#directory#&reportUnit=#directory#/#thisReport.xmlattributes.name#&decorate=no">
		<td><a onclick = "openNewTab('Report','#thisReport.label.xmltext#','#link#');return false">Run</a> <a href="#link#" target="_new">New window</a></td>
		</tr></cfoutput>
	
	</cfloop>
	</table>
	
</cfoutput>	
