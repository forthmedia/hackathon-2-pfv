<!--- �Relayware. All Rights Reserved 2014 --->
<!---
setModuleAndGroup.cfm
WAB 2012-07-09
Rather a rough template for linking a JasperServer report to a Relayware Module
Would be nicer done via a nifty piece of Ajax
Doesn't yet deal with 'Groups'

2013/04/26	NJH	2013 Roadmap - add the ability to set defaults to usergroups. Also modified the moduleQry somewhat to take into account the new RW menu xml.
 --->


<cfparam name="reportURI">
<cfparam name="description">

<cfset jasperserverFunctions = application.com.jasperserver>

<cfset currentReportsValue = jasperserverFunctions.getReportsQuery(uri=reportURI)>

<cfif not structKeyExists(form,"module")>
	<cfset form.module = currentReportsValue.module>
	<cfset form.grouping = currentReportsValue.grouping>
</cfif>

<cfset message = "">
<cfset messageType ="success">
<cfset errorMessage = "">

<!--- Do Update--->
<cfif structKeyExists(form,"submit")>

	<cfset message = "The report was successfully assigned.">

	<!--- NJH 2013/04/25 Roadmap 2013  - update preferences --->
	<cfif structKeyExists(form,"userGroupIDs")>
		<cfif form.userGroupIDs eq "" and form.userGroupIDs_orig neq "">
			<cfloop list="#form.userGroupIDs_orig#" index="userGroupId">
				<cfset application.com.myRelay.deleteLink(linktype="jasperReport",location=form.reportURI,entityID=userGroupId,entityTypeID=application.entityTypeID.userGroup)>
			</cfloop>

		<cfelseif form.userGroupIDs neq "">

			<cfloop list="#form.userGroupIDs#" index="userGroupId">
				<cfif not listFind(form.userGroupIDs_orig,userGroupId)>
					<cfset application.com.myRelay.addLink(description=description,linktype="jasperReport",attribute1=form.grouping,moduleTextID=form.module,location=form.reportURI,entityID=userGroupId,entityTypeID=application.entityTypeID.userGroup)>
				</cfif>
			</cfloop>

			<cfloop list="#form.userGroupIDs_orig#" index="userGroupId">
				<cfif not listFind(form.userGroupIDs,userGroupId)>
					<cfset application.com.myRelay.deleteLink(linktype="jasperReport",location=form.reportURI,entityID=userGroupId,entityTypeID=application.entityTypeID.userGroup)>
				</cfif>
			</cfloop>
		</cfif>
	</cfif>

	<cfif currentReportsValue.Module neq form.module>
		<cftry>
			<cfset jasperserverFunctions.setReportParameter(reportID = jasperserverFunctions.getReportIDFromURI(reportURI), parameter="Module" , value = form.module)>

			<!--- update the attribute on all current links, as they will be pointing to the old module --->
			<cfquery name="updateLinks" datasource="#application.siteDataSource#">
				update userLink set moduleTextID=<cf_queryparam value="#module#" CFSQLTYPE="CF_SQL_VARCHAR">
				from userLink u
					inner join userLinkType ult on u.linkTypeID = ult.linkTypeID
				where moduleTextID <> <cf_queryparam value="#form.module#" CFSQLTYPE="CF_SQL_VARCHAR">
					and linkType = 'jasperReport'
					and linkLocation = <cf_queryparam value="#form.reportURI#" CFSQLTYPE="CF_SQL_VARCHAR">
			</cfquery>

			<!--- <cfset message="The report was successfully assigned to phr_mod_#module#."> --->

			<cfcatch>
				<cfset errorMessage = "There was a problem assigning the report to phr_mod_#module#.">
			</cfcatch>
		</cftry>
	</cfif>

	<cfif currentReportsValue.grouping neq form.grouping>
		<cftry>
			<cfset jasperserverFunctions.setReportParameter(reportID = jasperserverFunctions.getReportIDFromURI(reportURI), parameter="grouping" , value = form.grouping)>

			<!--- <cfset message=message&" The report was successfully assigned to the #form.grouping# group."> --->

			<!--- update the attribute on all current links, as they will be pointing to the old grouping --->
			<cfquery name="updateLinks" datasource="#application.siteDataSource#">
				update userLink set linkAttribute1=<cf_queryparam value="#form.grouping#" CFSQLTYPE="CF_SQL_VARCHAR">
				from userLink u
					inner join userLinkType ult on u.linkTypeID = ult.linkTypeID
				where linkAttribute1 <> <cf_queryparam value="#form.grouping#" CFSQLTYPE="CF_SQL_VARCHAR">
					and linkType = 'jasperReport'
					and linkLocation = <cf_queryparam value="#form.reportURI#" CFSQLTYPE="CF_SQL_VARCHAR">
			</cfquery>

			<cfcatch>
				<cfset errorMessage = errorMessage&" There was a problem assigning the report to the #form.grouping# group.">
			</cfcatch>
		</cftry>
	</cfif>
</cfif>

<!--- Get Current Module & Grouping --->
<cfset report = jasperserverFunctions.getReportsQuery(URI = reportURI)>

<!--- Would be ideal to Display Modules and Groups as 2 selects related, but for time being just doing modules --->
<cfset modules = application.com.relayMenu.getActiveModules()>
<cfset moduleStruct = application.com.structureFunctions.queryToStruct(query=modules,key="moduleTextID",columns="moduleTextID,translatedTitle")>
<!--- 2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Access menus through getMenuXMLDoc().  Client extensions dealt with automatically. --->
<cfset reportMenus = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuData/MenuItem/MenuAction[@TabText='Reports']/SubMenuAction",lockName="applicationMenu")>
<!--- <cfset reportMenus = xmlSearch(application.com.relayMenu.getMenuXMLDoc(),"//MenuData/MenuItem/MenuAction[@TabText='Reports']/SubMenuAction")> --->

<cfset moduleGroupQry = queryNew("module,group,moduleDisplay")>
<cfloop array="#reportMenus#" index="menu">
	<cfif structKeyExists(menu.xmlparent.xmlparent.xmlAttributes,"module") and trim(menu.xmlparent.xmlparent.xmlAttributes.module) neq "" and menu.xmlAttributes.group neq "" and structKeyExists(moduleStruct,menu.xmlparent.xmlparent.xmlAttributes.module)>
		<cfset hasPermission = true>
		<cfif structKeyExists(menu.xmlparent.xmlparent.xmlAttributes,"security")>
			<cfset security = menu.xmlparent.xmlparent.xmlAttributes.security>
			<cfset hasPermission = application.com.login.checkInternalPermissions(securityTask=listFirst(security,":"),securityLevel=listLast(security,":"))>
		</cfif>

		<cfif hasPermission>
			<cfset queryAddRow(moduleGroupQry,1)>
			<cfset querySetCell(moduleGroupQry,"module",moduleStruct[menu.xmlparent.xmlparent.xmlAttributes.module].moduleTextID)>
			<cfset querySetCell(moduleGroupQry,"moduleDisplay",'phr_mod_'&moduleStruct[menu.xmlparent.xmlparent.xmlAttributes.module].moduleTextID)>
			<cfset querySetCell(moduleGroupQry,"group",menu.xmlAttributes.group)>
		</cfif>
	</cfif>
</cfloop>

<cfquery name="moduleGroupQry" dbtype="query">
	select distinct * from moduleGroupQry
	where [module] <> '' and [group] <> ''
	order by moduleDisplay
</cfquery>

<script type="text/javascript">
	function refreshReportList() {
		<cfif isDefined("module") or isDefined("grouping")>
		opener.location.href=opener.location.href;
		</cfif>
		self.close();
	}
</script>

<form name="moduleGroupForm" method="post">
	<cf_input type="hidden" name="reportURI" value="#reportURI#">
	<cf_input type="hidden" name="description" value="#description#">

	<cfif errorMessage neq "">
		<cfset message = errorMessage>
		<cfset messageType = "warning">
	</cfif>

	<cfset message = replaceNoCase(message,"The report",report.label)>
	<cfif message neq "">
		<cfset message = application.com.relayUI.message(message=message,type=messageType)>
	</cfif>

	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="HTML" Label ="" spanCols="true" currentValue="#message#">
		<cf_relayFormElementDisplay relayFormElementType="HTML" Label ="Report" currentvalue = "#report.label#">

		<cf_relayFormElementDisplay relayFormElementType="HTML" Label ="Module and Group">
			<CF_TwoSelectsRelated
				QUERY="moduleGroupQry"
				NAME1="module"
				NAME2="grouping"
				VALUE1="module"
				VALUE2="group"
				DISPLAY1="moduleDisplay"
				DISPLAY2="group"
				SELECTED1="#form.module#"
				SELECTED2="#form.grouping#"
				MULTIPLE3="No"
				EmptyText2="No Grouping"
				FORMNAME = "moduleGroupForm"
				HTMLBetween = "<br>"
			>
		</cf_relayFormElementDisplay>

		<cfif application.com.login.checkInternalPermissions(securityTask="ReportDesignerTask",securityLevel="level3")>
			<cfquery name="getUserGroupsSetForReport" datasource="#application.siteDataSource#">
				select distinct entityID as userGroupId
				from entityUserLink eul
					inner join userLink ul on eul.linkId = ul.linkID
					inner join userLinkType ult on ult.linkTypeId = ul.linkTypeId
				where
					entityTypeID =  <cf_queryparam value="#application.entityTypeID.userGroup#" CFSQLTYPE="cf_sql_integer" >
					and linkLocation =  <cf_queryparam value="#reportURI#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and linkType = 'JasperReport'
			</cfquery>

			<cfset selectedUserGroups = valueList(getUserGroupsSetForReport.userGroupID)>
			<cfif selectedUserGroups eq "">
				<cfset selectedUserGroups = 0>
			</cfif>

			<cfset selectedUserGroupQry = application.com.relayUserGroup.getUserGroupsForSelect(userGroupTypesToExclude="personal,external",userGroupIDsToInclude=selectedUserGroups)>
			<cfset nonSelectedUserGroupQry = application.com.relayUserGroup.getUserGroupsForSelect(userGroupTypesToExclude="personal,external",userGroupIDsToExclude=selectedUserGroups)>

			<cf_relayFormElementDisplay relayFormElementType="HTML" Label ="Add to 'My Favourite Reports' For">
				<CF_TwoSelectsComboQuery
				    NAME="potentialUserGroupIDs"
				    NAME2="userGroupIDs"
					doSort="false"
				    WIDTH="150px"
				    FORCEWIDTH="50"
					QUERY1="selectedUserGroupQry"
					QUERY2="nonSelectedUserGroupQry"
					value = "userGroupID"
					display = "name"
				    CAPTION="<FONT SIZE=-1><B>Potential UserGroups:</B></FONT>"
				    CAPTION2="<FONT SIZE=-1><B>Current UserGroups:</B></FONT>"
				>
			</cf_relayFormElementDisplay>

			<cf_input type="hidden" value="#valueList(selectedUserGroupQry.userGroupId)#" name="userGroupIDs_orig">
		</cfif>

		<cf_relayFormElementDisplay relayFormElementType="HTML" Label ="Report" spanCols="true" valueAlign="right">
			<cf_relayFormElement relayFormElementType="submit" Label = "" fieldname="submit">
			<cf_relayFormElement relayFormElementType="button" Label = "close" currentValue="Close" fieldname="close" onClick="refreshReportList();">
		</cf_relayFormElementDisplay>
	</cf_relayFormDisplay>
</form>