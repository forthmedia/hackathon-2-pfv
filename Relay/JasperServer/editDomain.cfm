<!--- �Relayware. All Rights Reserved 2014 --->

<cfset jasperServerFunctions = createObject("component","relay.com.jasperServer")>

<!--- <cfdump var="#application.flagGroup['92']#"><CF_ABORT> --->

<cfif isDefined("url.id")>
	<cfset id = url.id>
<cfelseif isDefined("form.id")	>
	<cfset id = form.id>
</cfif>

<cfif isDefined("id") or structKeyExists(form,"frmUpdateDomainXML")>
	<cfset getXML = jasperServerFunctions.readresourceXML (id)>

	<cfif getXML.isOK>
		<cfif structKeyExists(url,"showDegbug")>
			<cfdump var="#getXML.XML#"><CF_ABORT>
		</cfif>
		<cfset xml = getXML.XML>
		<cfset xmlobj = createobject ("component","/com/xmlfunctions")>
	
		<cfif xml.xmlroot.xmlname is not "schema">
			Not a Domain	
			<cfoutput>#xml.xmlroot.xmlname# </cfoutput>
			<CF_ABORT>
		</cfif>
	</cfif>
	
	<cfset TemporaryFolderdetails = application.com.fileManager.getTemporaryFolderDetails()>
	<cfset filename="domainxml_#id#.xml">
	
</cfif>


<cfif structKeyExists(form,"frmUpdateDomainXML")>

	<cfif structKeyExists(form,"frmDomainFile") and form.frmDomainFile neq "">
		<cfset importXml = application.com.fileManager.uploadFile(fileField="frmDomainFile",nameConflict="overwite",destination="#TemporaryFolderdetails.path#",renameFileTo=filename)>
		<cfif importXML.isOK>
			<cffile action="read" file="#TemporaryFolderdetails.path##filename#" variable="importDomainXML">
			<cfset xml = xmlParse(importDomainXML)>
			<cfset jasperServerFunctions.writeResourceXML(id=id,xml=xml)>
		</cfif>
	<cfelse>
		<cfloop list="#form.fieldnames#" index="formField">
			<!--- Processing form submission --->
			<cfif listFindNoCase("add,delete",listFirst(formField,"_")) and listLen(formField,"_") eq 2>
				<cfset action = listFirst(formField,"_")>
				<cfset flagGroupID = listLast(formField,"_")>
			
				<cfset tablesArray = application.com.jasperServer.getEntityTablesFromDomain(domainId=id,entityTableName=application.com.flag.getFlagGroupStructure(flagGroupID=flagGroupID).entityType.tablename)>
				<cfset tableID = tablesArray[1].xmlAttributes.id>
				<cfif action eq "delete">
					<cfset x = jasperServerFunctions.deleteFlagGroupFromXML(xml=xml,flagGroupID=flagGroupID,domainId=id,tableID=tableID)>
				<cfelseif action eq "add">
					<cfset x = jasperServerFunctions.applyFlagGroupToXML(xml=xml,flagGroupID=flagGroupID,domainId=id,tableID=tableID)>
				</cfif>
				
				<cfif x.isOK>
					 <cfset jasperServerFunctions.writeResourceXML(id=id,xml=x.xml)>     
					 <cfset xml = X.XML>
				<cfelse>
					<cfoutput>Failed to update XML #x.message#</cfoutput>
				</cfif>
			</cfif>
	
		</cfloop>
	</cfif>
<cfelseif structKeyExists(form,"fieldNames") and listContainsNoCase(form.fieldnames,"frmUpdateDomainXMLFromBackup_")>
	<cfset backUpID = listLast(listGetAt(form.fieldnames,listContainsNoCase(form.fieldnames,"frmUpdateDomainXMLFromBackup_")),"_")>
	
	<cfset jasperServerFunctions.writeResourceXMLFromBackup(id=id,backUpId=backUpId)>
</cfif>

<cfif isDefined("id")>
	<cffile action="write" file="#TemporaryFolderdetails.path#\#filename#" output="#toString(jasperServerFunctions.readresourceXML(id).xml)#">
</cfif>


<cfif not isDefined("id") or structKeyExists(form,"frmUpdateDomainXML")>

	<cfset domainsQry = jasperServerFunctions.getDomainsQuery(restrictByCurrentUserRights=true)>
	
	<cf_tableFromQueryObject
		queryObject="#domainsQry#"
		queryName="domainsQry"
		useInclude="false"
		keyColumnList="label"
		keyColumnURLList="editDomain.cfm?id="
		keyColumnKeyList="schemaID"
		showTheseColumns="uri,label"
		groupByColumns="uri"
		hideTheseColumns="uri"
	>
	
	
<cfelse>

		<!--- get All Tables in the domain --->
		<cfset checkForTables = xmlsearch(xml,"//*[local-name()='resources']/*[local-name()='jdbcTable']")>		
 		<cfoutput>
		
		<cfform enctype="multipart/form-data"  method="post">
		<CF_INPUT type="hidden" name="ID" value = "#id#">
		
		<table>
			<tr>
				<td><strong>Tables</strong></td><td colspan=2><strong>Action</strong></td>
			</tr>
			<tr><td></td><td>Add</td><td>Delete</td></tr>	
		<cfloop index="i" from = "1" to="#arrayLen(checkForTables)#">
			<cfset tablename = replaceNoCase(checkForTables[i].xmlattributes.tablename,"dbo.","","ALL")>

			<cfif listLast(checkForTables[i].xmlattributes.id,"_") eq tablename>
			<tr>
				<!--- <td>#checkForTables[i].xmlattributes.id#</td> 
				<th colspan="3">#checkForTables[i].xmlattributes.tablename#</th> --->
				<th colspan="3">#htmleditformat(tablename)#</th>
			</tr>
				<cfif structKeyExists (application.entityTypeid,tablename)>
					<cfquery name="getFlagGroups" datasource = "#application.sitedatasource#">
						select name, flagGroupID from flagGroup where entitytypeid =  <cf_queryparam value="#application.entityTypeid[tablename]#" CFSQLTYPE="CF_SQL_INTEGER" >  and flagTypeID in (2,3,4,5,6)  <!---  2 checkbox, 3 radio --->
					</cfquery>
						<!---<input type="hidden" name="tableID" value = "#checkForTables[i].xmlattributes.id#">
						<cfselect name="flagGroupID" queryposition="below" query="getFlagGroups" value="flagGroupID" display="name" visible="Yes" enabled="Yes" onchange="submit()">
							<option>
						</cfselect> --->
					<cfloop query="getFlagGroups">
					<cfset checkForItems = xmlsearch(xml,"//*[local-name()='itemGroups']/*[local-name()='itemGroup'][@id='set_flaggroup_#flagGroupID#']")>
					
					<cfquery name="getFlagsForFlagGroup" datasource="#application.siteDataSource#">
						select name from flag where flagGroupId =  <cf_queryparam value="#flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
					
					<tr>
						<td title="Flags (#application.com.flag.getflagGroupStructure(flagGroupId=flagGroupId).flagType.datatable#):#valueList(getFlagsForFlagGroup.name)#">#htmleditformat(name)#</td>
						<td><cfif not arrayLen(checkForItems)><CF_INPUT type="checkbox" name="add_#flagGroupID#" value=""/></cfif></td>
						<td><cfif arrayLen(checkForItems)><CF_INPUT type="checkbox" name="delete_#flagGroupID#" value=""/></cfif></td>
					</tr>
					</cfloop>
				</cfif>
			</cfif>
			<tr><td colspan=3>&nbsp;</td></tr>
		</cfloop>
		
		<cfset backUpDomains = application.com.jasperServer.getBackUpResources(id=id)>
		<cfloop query="backUpDomains">
			<cfset backupFileName = "backUpDomainXml_#id#_#backup_id#.xml">
			<cffile action="write" file="#TemporaryFolderdetails.path#\#backupFileName#" output="#toString(jasperServerFunctions.readResourceXML(id=backup_id,isBackUp=true).xml)#">
			<tr>
				<td colspan="2">
					Backup created #htmleditformat(created)#: 
				</td>
				<td>
					<CF_INPUT type="submit" value="Restore from Backup" name="frmUpdateDomainXMLFromBackup_#backup_id#"/> (<a href="#TemporaryFolderdetails.webpath##backupFileName#" target="_blank">View</a>)
				</td>
			</tr>
		</cfloop>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td colspan="3">Import Domain from file: <input type="file" value="" name="frmDomainFile"/></td></tr>
			<tr><td colspan="3"><a href="#TemporaryFolderdetails.webpath##filename#" target="_blank">Export current domain</a></td></tr>
			<tr><td colspan="3"><input type="submit" value="Update" name="frmUpdateDomainXML"/></td></tr>
		</table>
			</cfform>

		</cfoutput>

<!---
		<!--- get All Queries in the domain --->
		<cfset checkForQueries = xmlsearch(xml,"//:jdbcQuery")>		

 		<cfoutput>
		<strong>JDBC Queries</strong>
		<table>
			<tr><td>ID</td><td>QueryName</td></tr>	
		<cfloop index="i" from = "1" to="#arrayLen(checkForQueries)#">
			<tr><td>#checkForQueries[i].xmlattributes.id#</td><td><!--- <cfdump var="#checkForQueries[i]#"> ---></td></tr>	

		
		</cfloop>
		</table>
		</cfoutput>

		<!--- get All Visible Items in the domain --->
		<cfset checkForItems = xmlsearch(xml,"//:items")>		
 		<cfoutput>
		<strong>Displayed Fields</strong>
		<table>
			<tr><td>ID</td><td>Items</td></tr>	
		<cfloop index="i" from = "1" to="#arrayLen(checkForItems)#">
			<cfset itemsNode = checkForItems[i]>
			<cfif itemsNode.xmlparent.xmlname is "itemGroup">
				<tr><td colspan="2">#itemsNode.xmlparent.xmlattributes.label# Group</td><td><!--- <cfdump var="#itemsNode.xmlparent.xmlattributes#"> ---></td></tr>					
			<cfelse>
				<tr><td colspan="2">Default Group</td></tr>
			</cfif>

			<cfloop index="j" from = "1" to = "#arrayLen(itemsNode.xmlchildren)#">
				<cfset itemnode = itemsNode.xmlchildren[j]>
				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>#itemnode.xmlattributes.label#</td><td><!--- <cfdump var="#itemNode.xmlattributes#"> ---></td></tr>					
			</cfloop>

		</cfloop>
		</table>
		</cfoutput>

		
		

	
	

			<PRE>
			<cfoutput>#htmleditformat(tostring(xml))#</cfoutput> 
		 	</PRE>
		
	<cfelse>
		No XML Found
	</cfif>
--->
</cfif>
	



