<!--- �Relayware. All Rights Reserved 2014 --->
<cf_title>Event Details</cf_title>
<!--- 	displays the event details for this event
		requires frmeventFlagTextID to be set on first entry

2014-02-04 NYB Case 437728 removed sanitiseHTML - am now just dumping the html
2014-06-23 SB Removed Tables template so that it works for responsive/bootstrap
 --->

<CFIF NOT isDefined("frmeventFlagTextID") and IsSimpleValue(frmeventFlagTextID)>
	<cfabort showerror="frmEventFlagTextID needs to be passed to eventDetails">
</CFIF>



<CFSET session.eventFlagTextID = frmeventFlagTextID>
<CFQUERY name="getEventDetail" datasource="#application.siteDataSource#">
	Select * from eventDetail
		where flagid = (select flagid from flag
			where flagtextid =  <cf_queryparam value="#frmeventFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
</CFQUERY>

<CFIF getEventDetail.RECORDCOUNT EQ 1>
	<div class="eventDetails">
		<CFOUTPUT query="getEventDetail">
			<div class="row">
				<div class="col-xs-12">
					<p><strong>#htmleditformat(Title)#</strong></p>
					<p>#htmleditformat(Location)#<BR>#dateformat(date,"DD-MMM-YYYY")#</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
				<!--- GCC Removed as superfluous --->
					<!--- <H3>AgRenda</H3> --->
					<!--- 2014-02-04 NYB Case 437728 don't sanitise, removed
					<p>#application.com.security.sanitiseHTML(Agenda)#</p><!--- Note this is HTML--->
					  and replaced with: --->
					<p>#Agenda#</p><!--- Note this is HTML--->
				</div>
			</div>
		</CFOUTPUT>
	</div>
</CFIF>