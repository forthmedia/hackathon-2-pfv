<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
Mods

2001-04-02 WAB added check for confirmationfileid being blank
2008-05-15 GCC changed evetndetails qry to work with eventFlagtextID correctly
WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate - though surely this template not used!
2011-10-05 PPB LID7683 allow suppression of confirmation email
WAB 2013-01-29  P-NET026 Use standard merge code to evaluate email text.  Also supplied backwards compatible merge fields
				Also CFMAIL -> CF_MAIL (and only one of them).  Deal with HTML emails (previously stripped out HTML - appeared to be due to a bug in very old CF version )
 --->
<cfif fileexists("#application.paths.code#\cftemplates\Modules\EventsPublic\MailConfirmation.cfm")>
 	<cfinclude template="/code/cftemplates/Modules/EventsPublic/MailConfirmation.cfm">
<cfelse>

	<CFQUERY NAME="GetPersonDetails" datasource="#application.siteDataSource#">
		SELECT *, p.firstname + ' ' + p.lastname as fullName
		FROM person AS p
		where p.personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<cfquery name="eventdetails" datasource="#application.siteDataSource#">
	select e.*, (select countrydescription 
				from Country 
				where countryid=e.countryid) as countryname,
				eg.eventname as eventname
		from EventDetail as e
		INNER JOIN flag as f ON e.FlagId=f.Flagid
		INNER JOIN EventGroup as eg ON eg.eventgroupid = e.eventgroupid
		where  f.flagid = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery> 
	 
	<!--- Added by MDC 2003-11-25 --->
	<cfset EventID = "#eventdetails.Flagid#">
	<!--- Added by MDC 2005-03-23 --->
	<cfset emailCC = "#eventdetails.emailCC#">
	 <!--- PKP 2008-03-03 TREND BUG 146 where clause changed needed to look for flagid not flagtextid --->
	<cfquery name="eventflagdata" datasource="#application.siteDataSource#">
		select 	* 
		from eventflagdata 
		where flagid =  <cf_queryparam value="#eventdetails.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<!--- Added by MDC 2003-11-25 to get corresponding related File --->
	<cfquery name="GetRelatedConfirmationFile" datasource="#Application.siteDataSource#">
		select FileID from relatedfile where entityid =  <cf_queryparam value="#EventID#" CFSQLTYPE="CF_SQL_INTEGER" >  and FileCategoryID = 1
	</cfquery>
	
	<CFSET mapPath = "">
	<CFSET fileText = "">
	
	<!--- MDC 2003-11-25 This gets the related file rather than the locationfileid from eventdetail --->
	<cfif GetRelatedConfirmationFile.FileID neq "">
		 <CFSET fileid=#GetRelatedConfirmationFile.FileID#>
		 <CFIF fileid is not 0>
			  <!--- <CFINCLUDE template = "/fileManagement/readfile.cfm">
			  <CFSET mapPath = getfile.fullPath> --->
			  <cfset mapPath=application.com.fileManager.getFileList(fileID=fileID).physicalLocation>
		<CFELSE>	
			<CFSET mapPath = "">
		</cfif>
		
	</cfif>
	<!--- get text of confirmation email --->
	<cfif eventdetails.confirmationfileid neq "">
		<CFSET fileid=#eventdetails.confirmationfileid#>
		<!--- WAB added test for no fileid set --->
		<CFIF fileid IS NOT 0>
		<cfset fileText=application.com.fileManager.readFile(fileID=fileid)>
		<cfELSE>
			<CFSET fileText = "">
		</cfif>
	</cfif>	
		
	<CFSET wave= "">
	<CFSET courseContent= "">
	
	<CFIF trim(eventFlagData.wave) is not "">
		<CFSET wave = "phr_#eventFlagData.wave#"> 
	</cfif>
	<CFIF trim(eventFlagData.courseContent) is not "">
		<CFSET courseContent = eventFlagData.courseContent>
	</cfif>
	<!--- course content not translated yet
	 ,'#eventFlagData.courseContent#'
	<CFSET courseContent = evaluate("phr_#eventFlagData.courseContent#")>
	 --->
	<CFSET agenda = eventdetails.agenda>
	<!--- WAB 2005-03-01 this was just set to application.email from (which isn't an email address!) now set to the same address that bulk communications come from
		probably not ideal but ...
	 --->
	<cfset mailfrom = "#application.emailFromDisplayName# <#application.EmailFrom#@#application.mailFromDomain#>" >
	
	<cfquery name="getemailtext" datasource="#application.siteDataSource#">
		select subject, messagetext,SuppressEmail
		from  eventdetailemail as ede, flag
		where flag.flagid=ede.flagid
		and flag.flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		and eventdetailemailtype like 'confirmation'
	</cfquery>
	
		<CFSET messageSubject = "Registration Received">
	<CFIF getEmailText.subject is not "">
		<CFSET messageSubject = getEmailText.subject>
	</cfif>
	
	<cfif getemailtext.recordCount and not getemailtext.SuppressEmail>		<!--- 2011/10/05 PPB LID7683 suppress confirmation email --->
		
		<cfif getemailtext.messagetext is "" and filetext is not "">
			<cfset mailText = fileText>	
		<cfelse>
			<cfset mailText = #getemailtext.messagetext#>
		</cfif>
		
	
		<cfset mergeStruct = {person = GetPersonDetails, event = eventdetails, agenda = eventdetails.agenda, firstname = GetPersonDetails.firstname,lastname=GetPersonDetails.lastname,fullname = GetPersonDetails.fullname, email = GetPersonDetails.email, wave= wave, courseContent = courseContent}>
		<cfset mailText = application.com.relayTranslations.checkForAndEvaluateCFVariables (phraseText = mailtext, mergeStruct = mergeStruct)>
	
	
		<cfset mailAttributes = {TO=GetPersonDetails.email, FROM=mailfrom, SUBJECT=messageSubject, BCC=application.copyAllEmailsToThisAddress}>
		<cfif mappath neq "">
			<cfset mailAttributes.mimeAttach = mappath >
		</cfif>
		<cfif application.com.regexp.isStringHTML(mailText)>
			<cfset mailAttributes.type="HTML">
		</cfif>
			
	
				<CF_MAIL attributeCollection  = #mailAttributes#
				><cfoutput>#mailtext#</cfoutput></CF_MAIL>
		
			<!--- Insert a Date Flag to say when email acknowledgement sent --->
				
			<cfquery name="updateeventdetails" datasource="#application.siteDataSource#">		
				Update 	EventFlagData 
				Set 	ConfirmationSent =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						LastUpdated = getDate(),
						lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personId#" CFSQLTYPE="CF_SQL_INTEGER" >
				WHERE 	flagid =  <cf_queryparam value="#eventDetails.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND 	entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>		
				
				
	
	</cfif>
</cfif>