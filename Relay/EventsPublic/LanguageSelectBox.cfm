<!--- �Relayware. All Rights Reserved 2014 --->
<!--- custom tag for displaying a "select" a language checkbox --->

<!---
If frmLanguageID is set in the calling template then cookie.defaultLanguageID is set to that value

Then:

If cookie.defaultLanguageID is set,
	Just displays a list of languages to choose from.
	The list is the languages defined for that country plus English
	If English is the only language then no select box is shown.


If cookie.defaultLanguageID is not set,
	If a country has no defined language then cookie.defaultLanguageID is set to English (1)
	If a country has one defined language then cookie.defaultLanguageID is set to that Language
	If a country has more than one defined language then it
		a)	displays a message in all the languages for the given country,
		b) displays a drop down of languages and then stops (ie no other content shown).
		c) When the user selects a languge the form is submitted back to the same page


If a new language is chosen then the form submits back to the current page.  No other changes on the page are saved.
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->


<CFPARAM name="attributes.countryid" >
<CFPARAM name="attributes.thisPage" default = "" >
<CFPARAM name="attributes.selectCurrent" default = "true" >
<CFPARAM name="attributes.siteDataSource" default = #application.sitedatasource#>

	<CFSET siteDataSource = attributes.siteDataSource>
	<CFSET currentLanguage = "">

<!--- frmLanguageID has been passed from a previous instance of this form,so set the default languageID --->

<CFIF isdefined("caller.frmLanguageID")>
	<cfset application.com.globalFunctions.cfcookie(NAME="DefaultLanguageID", VALUE="#caller.frmLanguageid#")>
</CFIF>




	<!--- decide what languages this country uses --->
	<CFQUERY NAME="getCountryLanguages" datasource="#application.siteDataSource#">
	select Languageid , language
	from integerMultipleFlagData, language
	where entityid =  <cf_queryparam value="#attributes.CountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	and flagid = (select flagid from flag where flagtextid = 'CountryLanguages')
	and data = languageid
	order by sortorder
	</cfquery>


	<CFIF not isDefined("cookie.DefaultLanguageID")>
		<CFIF getCountryLanguages.RecordCount IS 0 >
			<!--- no languages, set to English --->
			<cfset application.com.globalFunctions.cfcookie(NAME="DefaultLanguageID", VALUE="1")>
		<CFELSEIF getCountryLanguages.RecordCount IS 1 >
			<!--- single language defined, so set to that language --->
			<cfset application.com.globalFunctions.cfcookie(NAME="DefaultLanguageID", VALUE="#getCountryLanguages.languageid#")>
		<CFELSE>

			<!--- multiple language defined, so need to choose --->

			<CFLOOP query="getCountryLanguages">
				<cf_translate languageID = languageid >
					phr_PleaseChooseYourLanguage<P>
				</cf_translate>
			</cfloop>
		</cfif>
	</cfif>



	<CFSET currentLanguageID = "">
	<CFIF isDefined("cookie.DefaultLanguageID")>
		<CFSET currentLanguageID = cookie.DefaultLanguageID>
	</cfif>

	<cf_translate>
	<!---  display a drop down box if there a) multiple languages, or there is one language which is not English (and add English as an option)--->
	<!---  may be this should display a whole list of languages--->
	<CFIF getCountryLanguages.RecordCount GT 1 or getCountryLanguages.languageID is not 1>
		<CFOUTPUT>
		<FORM name="languageForm"  Action= "#attributes.thisPage#" method="post" >
			<!--- create a form of all form variables past to this page --->
			<CFIF isdefined("fieldnames")>
				<CFLOOP INDEX="j" LIST="#fieldnames#">
					<CFIF j is not "frmLanguageID">
					<CF_INPUT type="hidden" name="#j#" value="#Evaluate(j)#">
					</cfif>
				</CFLOOP>
			</cfif>

				<CFLOOP INDEX="i" LIST="#request.query_string#" delimiters="&">
					<CFSET j = listfirst(i,"=")>
					<CFIF j is not "frmLanguageID">
					<CF_INPUT type="hidden" name="#j#" value="#Evaluate(j)#">
					</cfif>
				</CFLOOP>



			<SELECT Name = "frmLanguageID" onchange="changeLanguage()">
					<OPTION value="">phr_PleaseChooseYourLanguage
				<CFLOOP query="getCountryLanguages">
					<OPTION value="#languageid#" <CFIF languageid is currentLanguageID and attributes.selectCurrent>SELECTED</cfif> > #htmleditformat(Language)#
				</cfloop>
				<CFIF listfind(valuelist(getCountryLanguages.languageid), 1) is 0>
					<OPTION value="1" <CFIF 1 is currentLanguageID and attributes.selectCurrent>SELECTED</cfif>>English
				</cfif>
			</select>


		</FORM>
		</cfoutput>
	</CFIF>

	</cf_translate>
	<CFIF not isDefined("cookie.defaultLanguageID")>
		<CF_ABORT>
	</cfif>






