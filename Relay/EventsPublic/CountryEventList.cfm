<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 	displays list of events for current country / eventgroup
		requires session.eventgroup to be set on first entry to the frame set
		requires frmcountryid to be set from previous page
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
 --->


<!--- language code
currently: if frmLanguageID is set, then sets a cookie and continues
otherwise, checks for langauges for this country
 --->

<CFIF IsDefined("frmcountryid")>
	<CFSET countryid=frmcountryid>
</CFIF>
<SCRIPT LANGAUGE="Javascript">	

	function chooseEvent(eventFlagTextId) {
		form = document.thisForm
		form.frmeventFlagTextID.value = eventFlagTextId
		form.submit()
	}
	

	function changeLanguage()  {
		form = document.languageForm
		msg = ''
		if (form.frmLanguageID[form.frmLanguageID.selectedIndex].value == '')  {
			msg += 'Please select a language'
		}
		if (msg == '')  {
			form.submit()
		}else{
			alert (msg)
		}
	}

</SCRIPT>



<CFQUERY NAME="GetEventDetailData" datasource="#application.siteDataSource#">
	Select 	e.countryid, 
			e.eventstatus, 
			e.allocatedstartdate, 
			e.preferredstartdate, 
			e.location, 
			e.venueRoom, 
			c.countrydescription,
			f.flagtextid
	from 	eventDetail as e, country as c, flag as f, countrygroup as cg
	where 	e.eventgroupid =  <cf_queryparam value="#session.eventgroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and 	f.flagid = e.flagid
	and 	c.countryid=e.countryid
	and 	c.countryid = cg.countrymemberid
	and 	cg.countrygroupid =  <cf_queryparam value="#frmcountryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and 	e.eventstatus  like 'Agreed'
	order by c.countrydescription,
			e.countryid, 
			e.eventstatus, 
<!--- 			e.allocatedstartdate,  this field not currently being used--->
			e.preferredstartdate,    
			e.location
</CFQUERY>	

<cf_translate>

<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
</cf_head>


		<CFOUTPUT>
<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=3 WIDTH=620>
	  	<TR>
		  	<TD colspan=2>&nbsp;</TD>
			<TD>phr_Current_Events_#CountryID#</TD>
			<TD>&nbsp;</TD>
		</TR>
		<TR>
		  	<TD colspan=2>&nbsp;</TD>
			<TD>phr_Welcome_Events_#CountryID#</TD>
			<TD>&nbsp;</TD>
		</TR>
		<CFLOOP QUERY="geteventdetaildata">
			<TR>
				<TD>&nbsp;</TD>
				<TD><A HREF="javascript:chooseEvent('#flagTextID#')">phr_Register</A></TD>
				
				<TD><FONT FACE="Arial" SIZE="2" COLOR="000099"><B>#htmleditformat(Location)#</B> <BR>#htmleditformat(venueRoom)#</FONT></TD>
				<TD><FONT FACE="arial" SIZE="2" COLOR="000099"><B>#dateformat(preferredstartdate,"DD-MMM-YY")#</B><BR></FONT></TD>
			</TR> 
		</CFLOOP>
</TABLE>


<FORM NAME="thisForm" METHOD="post" ACTION="agenda.cfm">
	<INPUT type="HIDDEN" name= "frmeventFlagTextID" value="">
	<CF_INPUT type="HIDDEN" name= "frmcountryid" value="#frmcountryid#">  
</FORM>

</CFOUTPUT>



	
</cf_translate>	

	


















