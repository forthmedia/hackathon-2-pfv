<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	Filename: RegUpdate.cfm
	
	update the eventstatus to regsubmitted
	
	Modifications:
	2008-09-26	NJH	relayElearning 8.1 - track the module progress when a student registers for an event. it either adds a record or updates a record.
	2009-09-10	NJH	P-FNL069 - if on the portal and personID doesn't equal that of the current user, abort.
	2012/10/25	NJH Social CR - check if event is shareable.
	2013-02-07 	PPB Case 433635 changes to allow this to be used from eventTaskAction.cfm (gradeStudents)
	  --->

<CFPARAM name="frmRegStatus" default = "RegistrationSubmitted">

<!--- NJH 2009-07-10 P-FNL069 if we're on the portal and the personID isn't of the current user, then abort. --->
<cfif not request.relayCurrentUser.isInternal and frmPersonID neq request.relayCurrentUser.personID>
	<CF_ABORT>
</cfif>


<cfif not structKeyExists(variables,"eventFlagID")>			<!--- 2013-02-07 PPB Case 433635 --->
    <cfquery name="getEventFlagID" datasource="#application.siteDataSource#">
    select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
    </cfquery>
    
    <cfset eventFlagID = getEventFlagID.flagID>
</cfif>														<!--- 2013-02-07 PPB Case 433635 --->


<!--- START 2013-02-07 PPB Case 433635 --->
<cfset argumentsStruct = structNew()>
<cfif StructKeyExists(variables,"moduleStatusId")>
	<cfset argumentsStruct.frmStatusId=moduleStatusId>
</cfif>
<!--- END 2013-02-07 PPB Case 433635 --->

<!--- NJH 2008-09-24 elearning 8.1 - when registering for an event with a module, track the user module progress --->
<cfif isDefined("userModuleProgressID") and (userModuleProgressID neq "" and userModuleProgressID neq 0)>					<!--- 2013-02-07 PPB Case 433635 changed OR to AND --->
	<cfscript>
		argumentsStruct.frmLastVisited=now();
		application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=userModuleProgressID,argumentsStruct=argumentsStruct);
	</cfscript>
<cfelseif isDefined("frmModuleID") and frmModuleID neq "" and frmModuleID neq 0>
	<cfscript>
		userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=frmModuleID,personID=frmPersonID,argumentsStruct=argumentsStruct);				/* START 2013-02-07 PPB Case 433635 */
	</cfscript>
</cfif>

<cfquery name="updateEventdetails" datasource="#application.siteDataSource#">		
IF exists (select * from EventFlagData WHERE flagid = <cf_queryparam value="#eventflagid#" CFSQLTYPE="CF_SQL_INTEGER" > 			<!--- 2013-02-17 PPB Case 433635 remove subselect of eventflagtextid  --->
	AND entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )
   BEGIN 
	Update EventFlagData 
	Set RegStatus =  <cf_queryparam value="#frmRegStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	<cfif isDefined("userModuleProgressID")>
		userModuleProgressID =  <cf_queryparam value="#userModuleProgressID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	</cfif>
	lastUpdated = getDate(),
	lastUpdatedBy =  <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >,
	lastUpdatedByPerson =  <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
	WHERE flagid =  <cf_queryparam value="#eventflagid#" CFSQLTYPE="CF_SQL_INTEGER" > 												<!--- 2013-02-17 PPB Case 433635 remove subselect of eventflagtextid  --->
	AND entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
   END
ELSE
   BEGIN 
	INSERT INTO EventFlagData
	(EntityID, FlagID, FirstName, LastName, SiteName, CountryID, 
		Country, Email, regDate, RegStatus, Wave <cfif isDefined("userModuleProgressID")>,userModuleProgressID</cfif>,created,createdBy,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
		SELECT personid, <cf_queryparam value="#eventflagid#" CFSQLTYPE="cf_sql_integer" >, 							<!--- 2013-02-17 PPB Case 433635 remove subselect of eventflagtextid  --->
			p.FirstName, p.LastName, l.SiteName, c.CountryID, 
			c.CountryDescription, p.email, getDate(), <cf_queryparam value="#frmRegStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >,'A'<cfif isDefined("userModuleProgressID")>,#userModuleProgressID#</cfif>,
			GetDate(), <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >,getDate(), <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		FROM Person AS p, Location AS l, Country AS c
		WHERE p.PersonID =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND p.LocationID = l.LocationID
		AND l.CountryID = c.CountryID
		AND NOT EXISTS (SELECT * FROM EventFlagData WHERE EntityID =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >  AND 
		flagid = <cf_queryparam value="#eventflagid#" CFSQLTYPE="CF_SQL_INTEGER" > )			<!--- 2013-02-17 PPB Case 433635 remove subselect of eventflagtextid  --->
   END
</CFQUERY>

<!--- NJH 2012/10/23 Social CR - check if event is shareable --->
<cfquery name="getEventDetails" datasource="#application.siteDataSource#">
	select title,share from eventDetail where flagID =  <cf_queryparam value="#eventFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.eventDetail.registered") and getEventDetails.share>
	<cfset mergeStruct = structNew()>
	<cfset mergeStruct.event = getEventDetails>

	<cfset application.com.service.addRelayActivity(action="registered",mergeStruct=mergeStruct,performedOnEntityID=eventFlagID,performedOnEntityTypeID=application.entityTypeID["eventDetail"],sendShare=true)>
</cfif>