<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2010/06 Beginning of a system for Semi Automated Configuration Tests

--->
<cfparam name="runAllTests" default="false">
<cfparam name="runTest" default="">
<cfparam name="cfpassword" default="">

<cfset listResult = application.com.systemTests.getAllRowsHTML(runAlltests = runAllTests,runTestsList=runTest,cfpassword = cfpassword)>

<h2>phr_admin_systemtests</h2>

<P><a href="javascript:runAllTests()">Run All Tests</a> </P>

<P>Click on an individual test to run it</P>
<cfoutput>
	<table id="systemTestsTable" class="table table-hover table-stripes">
	#listResult.rowsHTML#
	</table>
</cfoutput>
