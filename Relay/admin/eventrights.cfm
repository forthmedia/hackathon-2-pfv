<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET Levels = "Level 1,Level 2,Level 3">
<CFSET Permissions = "1,3,7">

<CFPARAM name="frmtask" default="eventtask"><!--- wab added this - can now be used (after a fashion) to set rights for things other than  phoning --->


<CFQUERY NAME="getSecurityID" datasource="#application.siteDataSource#">
SELECT c.SecurityTypeID FROM SecurityType AS c
         WHERE c.ShortName =  <cf_queryparam value="#frmtask#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>
<CFSET securityID = getSecurityID.securityTypeID>

<CFIF IsDefined("form.People") AND IsDefined("form.permission")>

	<CFSET newPermission = permission>
	<CFLOOP LIST="#People#" INDEX="theIndex">
		<CFSET oldPermission = ListLast(theIndex, ":")>
		
		<CFIF oldPermission EQ 1 AND newPermission GT 1>
			<CFQUERY NAME="UpdatePermissions" datasource="#application.siteDataSource#">
				INSERT INTO Rights 
				(UserGroupID, SecurityTypeID, CountryID, MenuID, Permission)
					SELECT ug.UserGroupID, <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0, 0, <cf_queryparam value="#newPermission#" CFSQLTYPE="CF_SQL_Integer" >
					FROM UserGroup AS ug
					WHERE ug.PersonID =  <cf_queryparam value="#ListFirst(theIndex, ":")#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
		<CFELSEIF oldPermission GT 1 AND newPermission EQ 1>
			<CFQUERY NAME="DeletePermissions" datasource="#application.siteDataSource#">
				DELETE FROM Rights 
				WHERE SecurityTypeID =  <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND MenuID = 0
				AND Permission > 1
				AND UsergroupID = (SELECT UserGroupID FROM UserGroup WHERE PersonID =  <cf_queryparam value="#ListFirst(theIndex, ":")#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFQUERY>
		<CFELSEIF (oldPermission GT 1 AND newPermission GT 1) AND (oldPermission NEQ newPermission)>
			<CFQUERY NAME="UpdatePermissions" datasource="#application.siteDataSource#">
				UPDATE Rights SET
				Permission =  <cf_queryparam value="#newPermission#" CFSQLTYPE="CF_SQL_Integer" > 
				WHERE UserGroupID = (SELECT UserGroupID FROM UserGroup WHERE PersonID =  <cf_queryparam value="#ListFirst(theIndex, ":")#" CFSQLTYPE="CF_SQL_INTEGER" > )
				AND SecurityTypeID =  <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND MenuID = 0
			</CFQUERY>
 		</CFIF>
	</CFLOOP>
</CFIF>


<CFQUERY NAME="GetEventViewers" datasource="#application.siteDataSource#">
	SELECT p.PersonID, MAX(p.FirstName) AS FirstName, MAX(p.LastName) AS LastName, MAX(r.Permission) AS Permission
	FROM Rights AS r, Person AS p, RightsGroup AS rg
	WHERE r.SecurityTypeID =  <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND r.UsergroupID = rg.UsergroupID
	AND rg.PersonID = p.PersonID
	GROUP BY p.PersonID
	ORDER BY p.LastName
</CFQUERY>




<cf_head>
	<cf_title>Promote Event Viewers</cf_title>
</cf_head>




<P>
<CFIF GetEventViewers.RecordCount GT 0>
	<FORM NAME="ThisForm" ACTION="eventrights.cfm" METHOD="post">
	<CF_INPUT type="HIDDEN" name = "frmTask" value="#frmtask#">
	<TABLE CELLSPACING="2" CELLPADDING="2">
	<TR>
		<TD VALIGN="top" CLASS="Label"><B>Name</B></TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD VALIGN="top" CLASS="Label"><B>Grant</B></TD>
	</TR>
	
	<TR>
		<TD VALIGN="top">
			<SELECT NAME="people" <CFIF GetEventViewers.RecordCount GT 10> SIZE="10" <CFELSE> SIZE="<CFOUTPUT>#GetEventViewers.RecordCount#</CFOUTPUT>" </CFIF> MULTIPLE>
				<CFOUTPUT QUERY="GetEventViewers">
					<OPTION VALUE="#PersonID#:#Permission#">#ListGetAt(Levels, ListFind(Permissions, Permission))# : #htmleditformat(FirstName)# #htmleditformat(LastName)#
				</CFOUTPUT>
			</SELECT>
		</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD VALIGN="top">
			<SELECT NAME="permission" SIZE="1">
				<OPTION VALUE="1">Level 1
				<OPTION VALUE="3">Level 2
				<OPTION VALUE="7">Level 3
			</SELECT>
		</TD>
	</TR>
	<TR ALIGN="center">
		<TD COLSPAN="3" ALIGN="center"><BR><INPUT TYPE="submit" NAME="Save" VALUE="Save"></TD>
	</TR>
	</TABLE>
	</FORM>
<CFELSE>
	Sorry, there are no more users with View rights that can be promoted to Managers.
	<P>Go to the Administration pages to give users View rights.
</CFIF>


