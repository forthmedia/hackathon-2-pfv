<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		ScreensList2.cfm
Author:			various
Date created:	2000

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
19 July 2001		SWJ			Added security Task management and supressed the
								calling of screenDefListEdit
2001-08-12			SWJ			Modified the code to work in screenAdminFrame.cfm
2004-09-05			SWJ			Modified to include deletions and refreshing of screensList2
2006-11-12			SWJ			Added higher security to Script Screens
2008-08-12			NJH			Bug Fix Trend Nabu Issue 831 - display inactive screens, but add 'inactive' to the display value in the dropdown
2008-10-07			WAB			Added link to create a view from a screen  P-SOPHSprint5
2008-10-22			AJC			P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup
2013-04-29			YMA 		Case 434789 Fix spelling of asterisk
2014-07-31			RMC			Split dropdown into two (one for entity type, the second for associated screens) as original list of options was too long.
								Also performed a code clean-up.
2014-08-13			RMC			Added functionality to reload correct form fields based on users previous selection when form on child page is submitted.
2014-08-19			RPW			Error when clicking Edit to drill-down in Screen Builder
29-09-2014 			SB			Added div around for styling
2015-10-01			ACPK		PROD2015-113 Replace right-hand frame with blank template after deleting item
2016/03/23			NJH			JIRA BF-597 - lower case entityType in qoq
--->

<cfset application.com.request.setDocumentH1(text="Screens")>
<!--- this is to delete screenDefinition rows --->
<div id="screenlistContainer">
<cfif structKeyExists(URL,"frmItemID") and isNumeric(frmItemID) and structKeyExists(URL,"action") and action eq "delete">
	<cfquery name="deleteScreenDef" datasource="#application.siteDataSource#">
		DELETE	FROM ScreenDefinition
		WHERE itemid =  <cf_queryparam value="#frmItemID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
</cfif>

<cfif structKeyExists(session,"currentScreenIDForEditing") and structkeyexists(form,'entityType') and form.entityType neq 0 and not structKeyExists(form,"frmScreenID")>
	<cfset form.frmScreenID = session.currentScreenIDForEditing>
</cfif>

<cfif structKeyExists(form,"frmScreenID")>
	<cfset session.currentScreenIDForEditing = form.frmScreenID>
</cfif>

<!--- Save the selected entity type into the session scope --->
<cfif structkeyexists(form,'entityType')>
	<cfset session.entityType = form.entityType>
<cfelseif structkeyexists(session,'entityType')>
	<cfset form.entityType = session.entityType>
</cfif>

<cfparam name="form.frmScreenID" default="0">
<cfparam name="frmShowViewerScreensOnly" default = "0">
<cfparam name="frmShowInactiveScreens" default = "1"> <!--- NJH 2008-08-12 Bug Fix Trend Nabu Issue 831 - changed default from 0 to 1 --->

<!--- get list of all screens --->
<!--- 2014-08-19	RPW Error when clicking Edit to drill-down in Screen Builder --->
<cfquery name="GetScreensList" dataSource="#application.siteDataSource#">
	SELECT screenID, ltrim(rtrim(ScreenName)) AS ScreenName, suite,
	CASE WHEN len(entityType) = 0 THEN 'Other' ELSE entityType END AS entityType,
	CASE WHEN len(entityType) = 0 THEN 'Other Screens' ELSE EntityType END AS screentype,
	sortorder,active,
	CASE WHEN suite = 1 THEN 'Editor' ELSE '' END AS editor,
	screentextid
	FROM screens
	WHERE ScreenID <> 0
	<cfif frmShowInactiveScreens eq 0>
		AND active = 1
	</cfif>
	ORDER by entityType, ScreenName
</cfquery>

<cfloop from="1" to="#GetScreensList.recordcount#" index="i">
	<!--- Replace blank entity types with the relevant text --->
	<cfif not len(GetScreensList.entityType[i])>
		<cfset GetScreensList.entityType[i] = 'Other'>
		<cfset GetScreensList.screenType[i] = 'Other'>
	</cfif>
	<!--- Ensure US spelling of "organisation" ("organization") --->
	<cfif GetScreensList.screenType[i] is 'Organisation'>
		<cfset GetScreensList.screenType[i] = 'Organization'>
	</cfif>
</cfloop>

<!--- Added functionality to retrieve associated entity where screenID passed in URL --->
<cfif isDefined("URL.frmScreenID")>
	<cfquery name="getEntityId" dbtype="query">
		SELECT	entityType
		FROM	GetScreensList
		where
		<cfif not isnumeric(url.frmScreenID)>
			 screentextid =  '#url.frmScreenID#'
		<cfelse>
			 screenid =  #url.frmScreenID#
		</cfif>
	</cfquery>
	<cfif getEntityID.recordcount is 1>
		<cfset form.frmScreenID = url.frmScreenID>
		<cfset form.entityType = getEntityId.entityType>
	</cfif>
	<cfset rc = StructDelete(url, "frmScreenID", "True")>
</cfif>

<!--- Reorder the query into alphabetical order (without this "Other screens" are displayed first) --->
<cfquery name="GetScreensList" dbtype="query">
	SELECT		*, lower(screentype) AS lowerScreenType
	FROM		GetScreensList
	ORDER BY	lowerScreenType
</cfquery>

<!--- Get screens from query where entity has been selected --->
<cfif structkeyexists(form,'entityType')>
	<cfquery name="getEntityScreens" dbtype="query">
		SELECT	*
		FROM	GetScreensList
		WHERE	lower(entityType) = '#lcase(form.entityType)#'
	</cfquery>
</cfif>

<!--- Ensure that the form entries match (user could change entity type whilst screen type is selected) --->
<!--- 2014-08-19	RPW Error when clicking Edit to drill-down in Screen Builder --->
<cfif structkeyexists(form,'entityType') and form.entityType neq 0 and structkeyexists(form,'frmScreenid') and FORM.frmScreenID neq 0>
	<cfquery name="checkEntityScreenMatch" dbtype="query">
		SELECT	*
		FROM	GetScreensList
		WHERE	lower(entityType) = '#lcase(form.entityType)#'
		<!--- AND		screenId = #form.frmScreenid# --->
		<cfif not isnumeric(FORM.frmScreenID)>
			AND screentextid =  <cf_queryparam value="#FORM.frmScreenID#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="true">
		<cfelse>
			AND screenid =  <cf_queryparam value="#FORM.frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</cfquery>
	<cfif not checkEntityScreenMatch.recordcount>
		<cfset form.frmScreenID = 0>
	</cfif>
</cfif>

<!--- check screenid passed is valid and translate a textid if necessary --->
<cfif structkeyexists(form,'entityType') and structkeyexists(form,'frmScreenID')>
	<cfquery name="getScreen" dataSource="#application.siteDataSource#">
		SELECT 	*
		FROM 	screens
		WHERE
		<cfif not isnumeric(FORM.frmScreenID)>
			screentextid =  <cf_queryparam value="#FORM.frmScreenID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		<cfelse>
			screenid =  <cf_queryparam value="#FORM.frmScreenID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
	</cfquery>
	<cfif getScreen.recordcount>
		<!--- Update text for display purposes --->
		<cfif getScreen.viewEdit is 'edit'>
			<cfset getScreen.viewEdit = 'Editable'>
		<cfelseif getScreen.viewEdit is 'view'>
			<cfset getScreen.viewEdit = 'Read only'>
		</cfif>
	</cfif>
</cfif>

<cfif structkeyexists(variables,'getScreen') and getScreen.recordcount>
	<cfquery name="getScreenDef" datasource="#application.siteDataSource#">
		SELECT 		CONVERT(varchar,ItemID) AS itemid, active, required, allColumns, countryid,
			FieldSource, FieldTextID, sortOrder,fieldlabel
		FROM 		screenDefinition
		WHERE 		screenid =  <cf_queryparam value="#getScreen.screenid#" cfsqltype="cf_sql_integer" >
		ORDER BY 	sortorder
	</cfquery>

	<cfquery name="getOtherScreensIncludingThisOne" datasource="#application.siteDataSource#">
		SELECT 		s.screenname, s.screenid
		FROM 		screens s INNER JOIN screenDefinition sd ON s.screenid = sd.screenid
		WHERE 		fieldsource = <cfqueryparam cfsqltype="cf_sql_varchar" value="screen">
		AND 		fieldtextid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#getScreen.screentextid#">,<cfqueryparam cfsqltype="cf_sql_varchar" value="#getScreen.screenid#">)
		ORDER BY 	s.sortorder
	</cfquery>
</cfif>

<cfparam name="frmScreenDefCountry" default = "9">
<cfset blankrecord = "1">

<cf_title>Screen Management</cf_title>
<cfsaveContent variable="headerScript">
<script>
	<cfoutput>
		function editScreenDef(itemID) {
			setSelectedClass(itemID);
			form = document.screenDefEdit#frameIdentifier#;
			form.frmItemId.value = itemID;
			form.submit();
			}

		function newScreenItem(screenid) {
			form = document.screenDefEdit#frameIdentifier#;
			form.frmScreenID.value = screenid;
			form.submit();
			}

		function newScreen() {
			document.screenEdit#frameIdentifier#.submit();
			}

		function listScreen(screenid) {
			form = document.screenList#frameIdentifier#
			form.frmScreenID.value = screenid
			form.submit();
			}

		function setSelectedClass(itemID) {
			jQuery('.selectedRow').removeClass('selectedRow');
			jQuery('##'+itemID+'_row').addClass('selectedRow');
		}
	</cfoutput>
</script>
</cfsaveContent>

<cfhtmlhead text="#headerScript#">

<cf_RelayNavMenu pageTitle="Screens" thisDir="screens">
	<cfif isdefined("getScreen.screenID") and getScreen.screenID neq "" and len(getScreen.entitytype) neq 0>
		<cf_includejavascriptonce template="/javascript/viewentity.js">
		<cf_includejavascriptonce template="/javascript/openwin.js">
		<cf_includejavascriptonce template="/javascript/extExtension.js">
		<cfif listfind("0,1,2",getScreen.entitytypeid) neq 0>
			<cf_RelayNavMenuItem MenuItemText="View" CFTemplate="javascript:void(viewEntity('#getScreen.entitytypeid#',#request.relayCurrentUser["#getScreen.entitytype#ID"]#,'#getScreen.screenID#'));">
		</cfif>
	</cfif>
	<cf_RelayNavMenuItem MenuItemText="New" CFTemplate="javascript:newScreen()">
</cf_RelayNavMenu>



	<cfoutput><form action="screensList.cfm?frameIdentifier=#frameIdentifier#" method="post" target="ScreensList#frameIdentifier#" ></cfoutput>
	<div class="row">
		<div class="col-xs-6 col-sm-6">
			<select name="entityType" onChange="submit()" class="form-control">
				<option value="0">Select an entity type</option>
				<cfoutput query="GetScreensList" group="lowerScreenType">
					<option value="#entityType#"<cfif structkeyexists(form,'entityType') and form.entityType is entityType>SELECTED</cfif>>#htmleditformat(application.com.regExp.proper(inputString=screenType))#</option>
				</cfoutput>
			</select>
		</div>
		<div class="col-xs-6">
			<cfif structkeyexists(variables,'getEntityScreens')>
				<cfif getEntityScreens.recordcount>
					<!--- 2014-08-19	RPW Error when clicking Edit to drill-down in Screen Builder --->
					<cfscript>
						if (!IsNumeric(FORM.frmScreenID) && IsDefined("getScreen.screenid") && getScreen.recordCount==1) {
							variables.selectedScreenId = getScreen.screenid;
						} else {
							variables.selectedScreenId = FORM.frmScreenID;
						}
					</cfscript>
			<select name="frmScreenID" onChange="submit()" class="form-control">
				<option value="0">Select a screen to edit</option>
							<cfoutput query="getEntityScreens">
								<option value="#ScreenID#" <cfif variables.selectedScreenId is screenid>SELECTED</cfif>>&nbsp;&nbsp;#left(ScreenName,50)# (#htmleditformat(screenID)#) <cfif not active>- Inactive</cfif> <cfif suite>* </cfif> <!--- NJH 2008-08-12 Bug Fix Trend Nabu Issue 831 ---></option>
					</cfoutput>
						</select>
						<!--- RMC: 31-July-2014. Line below removed as per instruction from VB --->
						<!--- * Screens marked with an asterisk will show in the account database applications.	<!--- 2013-04-29 YMA Case 434789 Fix spelling of asterisk. ---> --->
				<cfelseif not getEntityScreens.recordcount and form.entityType neq 0>
					Sorry, there are no screens associated with the selected entity type.
				</cfif>
			</cfif>
		</div>
	</div>
	<cfif structkeyexists(variables,'getScreen') and getScreen.recordcount>
		<cfoutput>
		<div id="screensListNav">
			<ul class="list-inline">
				<li>
					<a href="screenEdit.cfm?frmScreenID=#getScreen.screenid#&frameIdentifier=#frameIdentifier#" target="ScreenEdit#frameIdentifier#">Edit Screen Header</a>
				</li>
			<!--- SWJ: 2006-11-12 Added higher security to Script Screens --->
			<cfif application.com.login.checkInternalPermissions ("AdminTask", "Level3")>
			 <li>|</li>
			 <li>
			 	<a href="screenScript.cfm?frmScreenID=#getScreen.screenid#" target="ScreenEdit#frameIdentifier#">Script Screen </a>
			 </li>
			</cfif>
			</ul>
			<!--- WAB 2008-10-06 link to create a view
				NJH 2012/09/17 - removed this link as I'm not aware of anyone using it and I'm a fan of removing functionality that no one uses.. :) Agreed by WAB and VAB.
			 --->

			<!--- <cfif application.com.login.checkInternalPermissions ("AdminTask", "Level3")>
			 | <a href="createScreenView.cfm?frmScreenID=#getScreen.screenid#" target="ScreenEdit#frameIdentifier#">Create View </a>
			</cfif> --->
		</div>
		<div id="screensListType">
			<b>This screen is:</b>
			<cfif len(getScreen.viewEdit)>#htmleditformat(getScreen.viewEdit)# | </cfif>
			<cfif getScreen.active>Active<cfelse>Inactive</cfif>
			 | Entity: <cfif len(getScreen.entityType)>#htmleditformat(getScreen.entityType)#<cfelse>None</cfif>
			<cfif len(getScreen.screenTextID)> | TextID: #htmleditformat(getScreen.screenTextID)#</cfif>
			<cfif getScreen.suite> | Show In Dashboard</cfif>
		</div>
		</cfoutput>
	</cfif>
 	</form>

<cfif structkeyexists(variables,'getScreen') and getScreen.recordcount>
<table class="table table-hover table-striped">

<cfoutput><form action="screenDefEdit.cfm?frameIdentifier=#frameIdentifier#" method="post" name="screenDefEdit#frameIdentifier#" target="ScreenEdit#frameIdentifier#"></cfoutput>
	<cfoutput>
	<input type="Hidden" name="frmItemId" value="new">
	<input type="Hidden" name="frmNextItemId" value="new">
	<CF_INPUT type="Hidden" name="frmScreenid" value="#getScreen.screenid#">
	</cfoutput>
	<tr>
		<td colspan="7">
			Screen Items
		</td>
	</tr>
	<tr>
		<th>Source</th>
		<th>ID</th>
		<th>Active</th>
		<th>Req'd</th>
		<th>CountryID</th>
		<th>Order</th>
		<th></th>
	</tr>
		<cfset fieldsonpage = "">
		<cfoutput query="GetScreenDef">
			<tr<cfif currentrow mod 2 is not 0> class="oddrow"<cfelse> class="evenRow"</cfif> id="#itemID#_row">
				<td valign="TOP"><a href="javascript: editScreenDef(#ItemID#);">#htmleditformat(fieldSource)#</a></td>  <!--- WAB 2005-01-12 added a href to this column as well - sometimes the  --->
				<td><a href="javascript: editScreenDef(#ItemID#);"><cfif fieldSource is "label">#left(htmleditformat(fieldlabel),20)#<cfelseif listfindNoCase("html,value",fieldSource) is not 0 or trim(fieldtextid) is "">#htmleditformat(fieldSource)#<cfelse>#left(fieldTextID,20)#</cfif></a>
					<cfif fieldSource is "SCREEN"><a href="javascript: listScreen('#fieldtextid#')">edit</a></cfif>
				</td>
				<td>#htmleditformat(YesNoFormat(active))#</td>
				<td>#htmleditformat(YesNoFormat(required))#</td>
				<td>#htmleditformat(CountryID)#</td>
				<td>#htmleditformat(sortOrder)#</td>
				<cfif application.com.login.checkInternalPermissions("AdminTask","Level3")>
				<td><a href="screensList.cfm?frmScreenID=#getScreen.screenid#&frmItemID=#ItemID#&action=delete&frameIdentifier=#frameIdentifier#" onClick="parent.ScreenEdit#frameIdentifier#.location='/templates/blank.cfm'"><span class="deleteLink">phr_Delete</span></a></td> <!--- 2015-10-01	ACPK	PROD2015-113 Replace right-hand frame with blank template after deleting item --->
				</cfif>
			</tr>
		</cfoutput>
		<cfset fieldsonpage = listappend(fieldsonpage ,"Countryid")>
		<cfoutput><CF_INPUT type="Hidden" name="frmFieldsOnPage" value="#FieldsOnPage#"></cfoutput>

	<cfif getScreen.screenid neq "">
		<cfoutput>
			<tr>
				<td colspan="7">
					<p><a class="btn btn-primary" href="screenDefEdit.cfm?frmScreenID=#getScreen.screenid#&frmItemID=new&frameIdentifier=#frameIdentifier#" target="ScreenEdit#frameIdentifier#">Add new item</a></p>
				</td>
			</tr>
		</cfoutput>
	</cfif>
</form>

</table>

<!--- is this screen included in any other screens? --->
	<cfif getOtherScreensIncludingThisOne.recordcount is not 0>
		<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="White" class="withBorder">
		<tr><td>This screen is included in the following screens</td></tr>
		<cfoutput query = "getOtherScreensIncludingThisOne">
		<tr><td><a href="javascript: listScreen('#screenid#')">#htmleditformat(screenname)#</td></tr>
		</cfoutput>
		</table>
	</cfif>
</cfif>

	<cfoutput><form action="screenEdit.cfm?frameIdentifier=#frameIdentifier#" method="post" name="screenEdit#frameIdentifier#" target="ScreenEdit#frameIdentifier#"></cfoutput>
		<input type="Hidden" name="frmScreenId" value="-1">
	</form>

	<cfoutput><form action="screensList.cfm" method="get" target="ScreensList#frameIdentifier#" name="screenList#frameIdentifier#"></cfoutput>
		<input type="Hidden" name="frmScreenID" value="">
		<cfoutput><input type="Hidden" name="frameIdentifier" value="#frameIdentifier#"></cfoutput>
	</form>

</div>