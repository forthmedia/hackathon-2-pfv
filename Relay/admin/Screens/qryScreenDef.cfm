<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods:

RND 2005-04-12  Added isCollapsable & isVisible to query
2006-05-19		SWJ		Made span both columns default to true
2008/07/30 	NYF		Bug Fix T-10 Issue 583: Added support for creating country screens
					which were always possible to create, but never supported properly throughout the code

2008-10-24	NYB  	P-SOP007 - added trngUserModuleProgress Profiles & Profile Attributes to Source dropdown
2011/05/31	NYB		REL106 added support for non PLO table fields to getFieldSources query
2015-03-04  WAB		Fix problems if referencedEntityIDList is blank - was being passed into CFQUERYPARAM which was casting it as 0.  Error only appeared if cf_queryparam debugging turned on, but results could have been unpredictable
2016-02-02	WAB		Change a valid value query
 --->

<!--- get all the bits required for editing a screen definition --->
<CFPARAM name="frmScreenDefCountry" default = "0">

<CFQUERY NAME="GetScreen" dataSource="#application.siteDataSource#">
	Select * from screens
	Where 1=1
	<CFIF isdefined("frmscreenid")> and  screenid =  <cf_queryparam value="#frmscreenid#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
	<CFIF isdefined("singleitemid") and isNumeric (singleitemid)>and screenid = (select screenid from screendefinition where itemid =  <cf_queryparam value="#singleitemid#" CFSQLTYPE="CF_SQL_INTEGER" > )</cfif>
</CFQUERY>

<CFSET entityType = getScreen.EntityType>
<CFSET entityTypeID = getScreen.EntityTypeID>
<CFIF entityTypeID is ""><CFSET entityTypeID = 0></CFIF>
<CFSET screenID = getScreen.screenid>

<cfset screenVersion = getScreen.viewer eq "formRenderer"?2:1>

<CFQUERY NAME="GetScreenDef" dataSource="#application.siteDataSource#">
	SELECT 	convert(varchar,ItemID) as itemid,
			screenid,
			SortOrder,
			CountryID,
			FieldSource,
			FieldTextID,
			FieldLabel,
			active,
			Method,
			Translatelabel,
			UseValidValues ,
			lookup,
			required,
			evalstring,
			breakln,
			allcolumns,
			-- IsCollapsable,
--			IsVisible,
			maxLength,
			size,
			TDClass,
			TRClass,
			jsVerify,
			specialformatting,
			condition,
			lastupdated,
			notes
	From ScreenDefinition as sd
	where 1=1
	<CFIF isdefined("frmscreenid")> and screenid =  <cf_queryparam value="#frmscreenid#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
	<CFIF isdefined("singleitemid") and isNUmeric (singleitemid)> and itemid =  <cf_queryparam value="#singleitemid#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
	<CFIF isdefined("singleitemid") and not isNUmeric (singleitemid)> and 1=0 </cfif>

	<CFIF frmScreenDefCountry is not 0>
	AND (
			(countryid = 0 and not exists (select 1 from screendefinition as sd1 where sd.screenid = sd1.screenid and sd.sortorder = sd1.sortorder and sd1.countryid  in ( <cf_queryparam value="#frmScreenDefCountry#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
		OR
			(countryid  in ( <cf_queryparam value="#frmScreenDefCountry#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		)
	</cfif>

	<CFIF isDefined("BlankRecord") or (isdefined("singleitemid") and not isNUmeric (singleitemid))>
	UNION

	SELECT
			'<cfif isdefined("singleitemid")>#singleitemid#<cfelse>new1</cfif>',
			<CFIF isdefined("frmscreenid")>#frmScreenid#<cfelseif isdefined("singleitemid")>(select screenid from screendefinition where itemid =  <cf_queryparam value="#singleItemID#" CFSQLTYPE="CF_SQL_INTEGER" > )<cfelse>0</cfif> ,
			(select ((max(sortOrder)/5)+1) * 5  from screenDefinition where screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" > ),  <!--- WAB 2000-11-12 changed so that always rounded to nearest 5 above current value--->
			0,
			'screen',
			'',
			'',
			1,
			'edit',
			0,
			0,
			0,
			0,
			'',
			0,
		--	1,
		--	0,
			0,
			null,
			null,
			'',
			'',
			'',
			'',
			'',
			0,
			''
	FROM DUAL
	</cfif>
	Order by SortOrder
	</CFQUERY>


<CFQUERY NAME="getScopes" datasource="#application.siteDataSource#">
	SELECT countryid, countrydescription,
	CASE WHEN isnull(isocode,'') = '' THEN 1 ELSE 2 END as sortorder
	from country
	UNION
	Select 0  , 'All', 0
	order by sortorder, countrydescription
</CFQUERY>



<CFQUERY NAME="getPredefinedScreens" datasource="#application.siteDataSource#">
	SELECT ScreenTextID, ScreenName, 2 as sortorder1, sortorder
	From Screens
	Where predefined = 1
	UNION
	SELECT ' ','Select a value',1,0 from dual
	order by sortorder1, sortorder
</CFQUERY>

<CFSET fieldSourcesWithFreeText = "HTML,Text,Include,IncludeRelayFile,IncludeUserFile,Phrase,Set,query">

<!--- 2013-07-10	YMA	entityListAndEditScreen - add support for any entity type. --->
<CFQUERY NAME="entitiesWithFlagsAndScreens" datasource="#application.siteDataSource#">
	select entitytypeID as loopEntityTypeID from schematable
	where screensexist = 1
	and flagsexist = 1
	and uniqueKey is not null
	order by entitytypeID
</CFQUERY>

<cfset referencedTableList = "">
<cfset referencedEntityIDList = "">
<cfif structKeyExists(application.customentities,entitytype)>
	<cfset referencedTableList = application.customentities[entityType].REFERENCEDTABLE>
	<cfset referencedTableList = listappend(referencedTableList,application.com.relayentity.recursiveListRelatedTables(tableName=application.customentities[entityType].REFERENCEDTABLE,filterOnlyScreensAndFlags=true))>

	<cfloop list="#referencedTableList#" index="referencedTable">
		<cfset referencedEntityIDList = listappend(referencedEntityIDList,structFind(application.entityTypeID,referencedTable))>
	</cfloop>
</cfif>

<CFQUERY NAME="getFieldSources" datasource="#application.siteDataSource#">
	SELECT 'Predefined Item (Snippet)' as fieldSourceDescription,
			'Screen' as fieldSource,
			ScreenName as  fieldDescription	,
			ScreenTextID as fieldTextID,
			convert(float,2) as sortorder1,
			ScreenName as sortOrder2
			From Screens
			Where predefined = 1
			AND active = 1
			and ((entitytypeid <=5 and entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ))

 	UNION


	SELECT 'Other Screen' as fieldSourceDescription,
			'Screen' as fieldSource,
			ScreenName as  fieldDescription	,
			ScreenTextID as fieldTextID,
			15	 as sortorder1,
			ScreenName as sortOrder2
			From Screens
			Where predefined = 0
			AND active = 1
			and ((entitytypeid <=5 and entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ))

 	UNION


	SELECT 'Select a Type', '',' ',' ',
	0,
	cast(0 as varchar(1)) as sortOrder2


	UNION

	SELECT distinct 'Special Effect',
			fieldSource,
			fieldtextid,
			fieldtextid,
			60,
			cast(0 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'specialeffect'

	UNION
	SELECT distinct 'Special Effect',
			'SpecialEffect',
			'SectionHeader',
			'SectionHeader',
			60,
			cast(0 as varchar(1)) as sortOrder2

	UNION
	SELECT distinct 'Special Effect',
			'SpecialEffect',
			'SectionFooter',
			'SectionFooter',
			60,
			cast(0 as varchar(1)) as sortOrder2


	UNION

		SELECT  'Fields from main tables',
				' ',
				'',
				'',
				20,
				'0'


 	<CFIF entityTypeID  IS 0 OR listFind(referencedEntityIDList,0)>
	UNION

	SELECT distinct '&nbsp;  Person Field',
			'Person',
			'Choose a field, or enter the name in the box below',
			'',
			21,
			cast(0 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'location'

	UNION

	SELECT distinct '&nbsp;  Person Field',
			fieldSource,
			fieldtextid,
			fieldtextid,
			21,
			cast(1 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'person'

	</cfif>
	<CFIF entityTypeID LE 1 OR listFind(referencedEntityIDList,1)>
	UNION

	SELECT distinct '&nbsp;  Location Field',
			'Location',
			'Choose a field, or enter the name in the box below',
			'',
			24,
			cast(0 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'location'


	UNION

	SELECT distinct '&nbsp;  Location Field',
			fieldSource,
			fieldtextid,
			fieldtextid,
			24,
			cast(1 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'location'


	UNION

	SELECT '&nbsp;  Formatted Address',
		 	'FullAddress',
			'This will output the address' ,
			'null' ,
			24,
			cast(2 as varchar(1)) as sortOrder2


	</CFIF>
	<CFIF entityTypeID LE 2 OR listFind(referencedEntityIDList,2)>
	UNION

	SELECT distinct '&nbsp;  Organisation Field',
			'Organisation',
			'Choose a field, or enter the name in the box below',
			'',
			27,
			cast(0 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'location'

	UNION

	SELECT distinct '&nbsp;  Organisation Field',
			fieldSource,
			fieldtextid,
			fieldtextid,
			27,
			cast(1 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'organisation'

	</CFIF>

	<!--- NYF 2008/07/30 Bug 583: added -> --->
	<CFIF entityTypeID is 3>
	UNION
	SELECT distinct '&nbsp;  Country Field',
			'Country',
			'Choose a field, or enter the name in the box below',
			'',
			28,
			cast(0 as varchar(1)) as sortOrder2
			from screendefinition  --??needed
			where fieldsource = 'location' --??needed
	UNION
	SELECT distinct '&nbsp;  Country Field',
			fieldSource,
			fieldtextid,
			fieldtextid,
			28,
			cast(1 as varchar(1)) as sortOrder2
			from screendefinition
			where fieldsource = 'country'

	</CFIF>
	<!--- <- Bug 583 --->

	<!--- NYB 2011/05/31 REL106 added -> --->
	<!--- 2013-07-10	YMA	entityListAndEditScreen - add support for any entity type. --->
	<CFIF entityTypeID gt 3>

		<cfloop list="#referencedEntityIDList#" index="i">
			<cfif i neq entityTypeID and i gt 3>
				UNION
				select '&nbsp;  '+st.description,
				isc.table_name as fieldSource,
				isc.column_name as fieldTextid,
				isc.column_name as fieldTextid,
				28.#numberFormat(i*1.1,'9')#,
				cast(ordinal_position as varchar(3)) as sortOrder2
				from information_schema.columns isc
				JOIN schematable st on isc.table_name = st.entityName
				where st.entityTypeID = #i#
			</cfif>
		</cfloop>

		UNION
			SELECT distinct
			'&nbsp; #application.entityType[entityTypeID].tablename# Field',
			'#application.entityType[entityTypeID].tablename#',
			'Choose a field, or enter the name in the box below',
			'',
			29,
			cast(0 as varchar(1)) as sortOrder2
			from dual

		UNION
			SELECT distinct
			'&nbsp; #application.entityType[entityTypeID].tablename# Field',
			'#application.entityType[entityTypeID].tablename#',
			isc.column_name as fieldTextid,
			isc.column_name as fieldTextid,
			29,
			isc.column_name as sortOrder2
			from information_schema.columns isc
			JOIN schematable st on isc.table_name = st.entityName
			where st.entityTypeID = #entityTypeID#

	</CFIF>
	<!--- <- NYB 2011/05/31 REL106 --->

 	UNION

		SELECT 'HTML',
			'HTML',
			'Enter HTML in box below',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION

		SELECT 'Include',
			'Include (deprecated)',
			'enter name of file to include below',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION
		SELECT 'Include Core Relay File',
			'IncludeRelayFile',
			'enter path to file below - relative to Relay Directory',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION
		SELECT 'Include Site Specific File',
			'IncludeUserFile',
			'enter path to file below - relative to Content Directory',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION
		SELECT 'Upload a Related File',
			'RelatedFile',
			'Enter Category Below',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2


 	UNION

		SELECT 'Set',
			'Set',
			'Enter a name value pair in the box below',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION
	<cfif screenversion is 1>
		SELECT 'Cold Fusion',
			'CF',
			'CFBreak',
			'CFBreak',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION

		SELECT 'A Cold Fusion Expression to be evaluated',
			'Value',
			'Enter an expression in Evaluate box below',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2


 	UNION

		SELECT 'A Hidden Field',
			'HiddenField',
			'Enter the name below and the Value in the Evaluate Box',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2

 	UNION
</cfif>
		SELECT 'Choose a workflow to run after this screen has been accessed',
			'ScreenPostProcess',
			processDescription + '(' + convert(varchar,processID) + ')',
			convert(varchar,processID),
			80,
			processDescription as sortOrder2
		from ProcessHeader


	UNION

		SELECT 'Text',
			'Text',
			'Enter Text in box below',
			'',
			90,
			cast(0 as varchar(1)) as sortOrder2

	UNION

		SELECT 'Just show a label',
			'Label',
			'Enter the Text in the Label box',
			'-',
			90,
			cast(0 as varchar(1)) as sortOrder2


	UNION

		SELECT 'Phrase',
			'Phrase',
			'Enter Phrase Identifier in box below',
			'',
			100,
			cast(0 as varchar(1)) as sortOrder2


	UNION
	<!--- Profile/Profile Attributes Header --->
		SELECT  'Profile Attributes',
				' ',
				'',
				'',
				33,
				'0'

	UNION

		SELECT  'Profile Attributes (boolean)',
				' ',
				'',
				'',
				36,
				'0'
	UNION

		SELECT  'Profiles',
				' ',
				'',
				'',
				30,
				'0'

	<!--- NYB 2008-10-24 P-SOP007 - replaced -> ---
	<CFLOOP index="i" from = "0 " to =  "10">
	!--- P-SOP007 - with --->
	<!--- <CFLOOP index="i" from = "0 " to =  "12">
		<cfswitch expression="#i#">
			<cfcase value="11"><cfset i = 71></cfcase>
			<cfcase value="12"><cfset i = 47></cfcase>
			<cfdefaultcase></cfdefaultcase>
		</cfswitch> --->

	<!--- 2013-07-10	YMA	entityListAndEditScreen - add support for any entity type. --->
	<CFLOOP Query="entitiesWithFlagsAndScreens">
	<!--- <- P-SOP007 --->
		<cfif (entityTypeID lt 4 and loopEntityTypeID lt 4 and entityTypeID lte loopEntityTypeID) OR (entityTypeID eq loopEntityTypeID) OR  (listFind(referencedEntityIDList,loopEntityTypeID))>
		<cfif not isDefined("loopedentityIDs")>
			<cfset loopedentityIDs = "">
		</cfif>
		<cfset loopedentityIDs = listAppend(loopedentityIDs,loopEntityTypeID)>
		UNION

		SELECT  '&nbsp;   ' + flagEntityType.description + ' Profile Attributes',
				'Flag',
				left(FlagGroup.Name,50) + ' group' as name,
				'',
				33.#numberFormat(loopEntityTypeID*1.1,'9')#,
				flaggroup.name as sortOrder2

		From FlagGroup, flagEntityType
		WHERE
			flagGroup.entityTypeID = flagEntityType.entityTypeID
		AND flagtypeid not in (0,2,3)  <!--- radio and checkboxes  and groups themselves don't have attributes--->
		and ((flaggroup.entitytypeid <=4 and flaggroup.entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (flaggroup.entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) <cfif structKeyExists(application.customentities,"#application.EntityType[entityTypeID].tablename#") and referencedEntityIDList is not ""> or (flaggroup.entitytypeid in (<cf_queryparam value="#referencedEntityIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="yes" >))</cfif>)
		AND flaggroup.entitytypeid =  <cf_queryparam value="#loopEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		and exists (select 1 from flag where flaggroupid = flagGroup.flagGroupID)  <!--- don't display a group if there are no flags --->

		 UNION

		SELECT  '&nbsp;   ' + flagEntityType.description + ' Profile Attributes',
				'Flag',
				' - ' + left(Flag.Name,50) as name,
				case when isNull(FlagTextID,'') <> '' then flagTextID  else convert(varchar,flagid) end,
				33.#numberFormat(loopEntityTypeID*1.1,'9')#,
				flaggroup.name + flag.name as sortOrder2

		From Flag, FlagGroup, flagEntityType
		WHERE flag.flaggroupid = flaggroup.flaggroupid
		AND flagGroup.entityTypeID = flagEntityType.entityTypeID
		<!--- 		AND isnull(FlagTextID,'') <> '' --->
		AND flagtypeid not in (2,3)  <!--- radio and checkboxes --->
		AND flag.active = 1
		and ((flaggroup.entitytypeid <=4 and flaggroup.entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (flaggroup.entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) <cfif structKeyExists(application.customentities,"#application.EntityType[entityTypeID].tablename#") and referencedEntityIDList is not "">or (flaggroup.entitytypeid in (<cf_queryparam value="#referencedEntityIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="yes" >))</cfif>)
		AND flaggroup.entitytypeid =  <cf_queryparam value="#loopEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >

		 UNION

		SELECT  '&nbsp;   ' + flagEntityType.description + ' Profile Attributes (Boolean)',
				'Flag',
				left(FlagGroup.Name,50) + ' group' as name,
				'',
				36.#numberFormat(loopEntityTypeID*1.1,'9')#,
				flaggroup.name  as sortOrder2

		From FlagGroup, flagEntityType
		WHERE
			flagGroup.entityTypeID = flagEntityType.entityTypeID
		AND flagtypeid in (2,3)  <!--- radio and checkboxes --->
		and ((flaggroup.entitytypeid <=4 and flaggroup.entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (flaggroup.entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) <cfif structKeyExists(application.customentities,"#application.EntityType[entityTypeID].tablename#") and referencedEntityIDList is not "">or (flaggroup.entitytypeid in (<cf_queryparam value="#referencedEntityIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="yes" >))</cfif>)
		AND flaggroup.entitytypeid =  <cf_queryparam value="#loopEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >

		 UNION

		SELECT  '&nbsp;   ' + flagEntityType.description + ' Profile Attributes (Boolean)',
				'Flag',
				' - ' + left(Flag.Name,30) as name,
				case when isNull(FlagTextID,'') <> '' then flagTextID  else convert(varchar,flagid) end,
				36.#numberFormat(loopEntityTypeID*1.1,'9')#,
				flaggroup.name + flag.name as sortOrder2

		From Flag, FlagGroup, flagEntityType
		WHERE flag.flaggroupid = flaggroup.flaggroupid
		AND flagGroup.entityTypeID = flagEntityType.entityTypeID
		AND flagtypeid in (2,3)  <!--- radio and checkboxes --->
		AND flag.active = 1
		and ((flaggroup.entitytypeid <=4 and flaggroup.entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (flaggroup.entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) <cfif structKeyExists(application.customentities,"#application.EntityType[entityTypeID].tablename#") and referencedEntityIDList is not "">or (flaggroup.entitytypeid in (<cf_queryparam value="#referencedEntityIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="yes" >))</cfif>)
		AND flaggroup.entitytypeid =  <cf_queryparam value="#loopEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >

	UNION

		SELECT '&nbsp;   ' + flagEntityType.description + ' Profiles',
				'flagGroup',
				left(Name,50) as name,
				case when isNull(FlagGroupTextID,'') <> '' then FlagGroupTextID  else convert(varchar,flaggroupid) end,
				30.#numberFormat(loopEntityTypeID*1.1,'9')#,
				flagGroup.name as sortOrder2
		From FlagGroup, flagEntityType
		WHERE
		<!--- isnull(FlagGroupTextID,'') <> ''		AND  --->
		flaggroup.active = 1
		AND flagGroup.entityTypeID = flagEntityType.entityTypeID
		and ((flaggroup.entitytypeid <=4 and flaggroup.entitytypeid >=  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) or (flaggroup.entitytypeid =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) <cfif structKeyExists(application.customentities,"#application.EntityType[entityTypeID].tablename#") and referencedEntityIDList is not "">or (flaggroup.entitytypeid in (<cf_queryparam value="#referencedEntityIDList#" CFSQLTYPE="CF_SQL_INTEGER" list="yes" >))</cfif>)
		AND flaggroup.entitytypeid =  <cf_queryparam value="#loopEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</cfloop>

	<cfif screenversion is 1>
	UNION

	SELECT 'Run A Query',
		 	'Query',
			'Enter query name as the Identifier and SQL in the Evaluate Box (in \" s) ',
			'',
			80,
			cast(0 as varchar(1)) as sortOrder2
</cfif>



	order by sortOrder1, sortOrder2
</CFQUERY>

<!--- Add screen widgets to query --->
<cfscript>

	formWidgets=application.com.formWidgets.getFormWidgets(entityTypeID);
	startIndex=getFieldSources.recordCount;

	i=1;
	for(key in formWidgets){

		formWidget=formWidgets[key];
		queryAddRow(getFieldSources);
		querySetCell(getFieldSources, "FieldSource", "formWidget", startIndex+i);
		querySetCell(getFieldSources, "FIELDSOURCEDESCRIPTION", "phr_formWidgets", startIndex+i);
		querySetCell(getFieldSources, "FIELDTEXTID", formWidget.widgetIdentifier, startIndex+i);
		querySetCell(getFieldSources, "FIELDDESCRIPTION", application.com.relayTranslations.translateString(formWidget.widgetNamePhrase), startIndex+i);
		querySetCell(getFieldSources, "SORTORDER1", "110", startIndex+i);
		querySetCell(getFieldSources, "SORTORDER2", formWidget.widgetNamePhrase, startIndex+i);
		i++;
	}

</cfscript>
