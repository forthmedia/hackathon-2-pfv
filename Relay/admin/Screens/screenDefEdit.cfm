<!--- �Relayware. All Rights Reserved 2014 --->
<!---

screenDefEdit.cfm

Author  WAB

Purpose:  For editing a single line of a screenDefinition

Date:	back in the mists of time

Mods:

2001-06-15  	WAB		Added some HTMLEditFormats
2001-07-18	SWJ		Changed some of the field lengths and the screen title
2004-09-29 	WAB 	Added hidden as a method
2005-04-12 	RND 	Added isVisible & isCollapsable fields to output
2005-05-23		WAB		Added a few more hints
2007-06-12	WAB		Handled a flag or group not existing (ie not in the memory structure)
2007-11-07       WAB added extra option to noflagname
2008-10-24	NYB  	P-SOP007 - added trngUserModuleProgress to Screens
2010/07/13	WAB 	FNL088 changed references to application. flag Structures to use a function (and replaced .exists with .isOK)
2012/05/22	WAB 	427912  reinstated doTranslation.js which had been removed accidentally (probably by me).  Normally included automatically when needed, but may not be here when outputTranslateLinkIfPhrase() is called
2015/10/15  DAN     Case 446228 - increase the maxlength of evalString field to allow saving longer SQL
2016-02-02	WAB		Change a displayValidValues - so passes in query  rather than a list  - failed because of a comma in a name
2016-11-07	WAB		FormRenderer Alter additionalParameters code to not use cf_evaluateNameValuePairs
					Changes to support Screens V2 - some controlled by switches
			/NJH	Removed some redundant items
	

 --->


	<cf_includeJavascriptOnce template="/javascript/openwin.js">
	<cf_includeJavascriptOnce template="/javascript/doTranslation.js">

<!--- reuseable function to output a translate link next to fields --->
<cffunction name="outputTranslateLinkIfPhraseHTML">
	<cfargument name = "string">
		<!--- the list first handles a field having more than one phrase in it (unlikely but ..)  --->
	<CFLOOP	index = "item" list="#string#" delimiters=" ,">
		<cfif left(item,4) is "phr_"><CFOUTPUT><A href="javascript:doTranslation('#replaceNocase(listfirst(item," "),"phr_","")#')">Translate</A></CFOUTPUT></cfif>
	</CFLOOP>
</cffunction>


<cfset singleitemid = frmitemid>
<cfinclude template = "qryScreenDef.cfm">

<cfset screenid = getscreendef.screenid>

<cfquery name="getScreen" datasource="#application.siteDataSource#">
	Select * from Screens where screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>

<cfif isnumeric (getscreendef.itemid)>
	<cfquery name="getNext" datasource="#application.siteDataSource#">
		select 	itemid, sortorder
		from 	screendefinition
		where 	screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and 	itemid <>  <cf_queryparam value="#getScreenDef.itemid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and 	sortOrder = (	Select min(sortorder)
							from screendefinition
							where screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
							and sortorder >=  <cf_queryparam value="#getScreenDef.SortOrder#" CFSQLTYPE="CF_SQL_INTEGER" >
							and itemid <>  <cf_queryparam value="#getScreenDef.itemid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>

	<cfquery name="getPrevious" datasource="#application.siteDataSource#">
		select 	itemid, sortorder
		from 	screendefinition
		where 	screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and 	itemid <>  <cf_queryparam value="#getScreenDef.itemid#" CFSQLTYPE="CF_SQL_INTEGER" >
		and 	sortOrder = (	Select max(sortorder)
							from screendefinition
							where screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
							and sortorder <=  <cf_queryparam value="#getscreendef.sortorder#" CFSQLTYPE="CF_SQL_INTEGER" >
							and itemid <>  <cf_queryparam value="#getScreenDef.itemid#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfquery>
	<cfelse>
	<cfquery name="getNext" datasource="#application.siteDataSource#">
		select 	max(itemid) as itemid
		from 	screendefinition
		where 	screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	<cfquery name="getprevious" datasource="#application.siteDataSource#">
		select 	max(itemid) as itemid
		from 	screendefinition
		where 	screenid =  <cf_queryparam value="#screenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	</cfif>

<cf_title>Edit Screen Item</cf_title>

<cf_head>
<script>


<!--- <CFINCLUDE template="..\..\screen\jsViewEntity.cfm"> --->


function verifyDefForm ()  {
	form = document.screenDefEdit
	msg = ''
//	if (form.frmScreenName.value == '')  {
//		msg = msg + 'You must enter a name for your screen'
//	}
	if (msg == '')  {
		return true
	}else {
		alert (msg)
		return false
	}
}

function submitDefForm (nextItemId)  {
	form = document.screenDefEdit
	if (nextItemId !=0) {
		form.frmNextItemId.value = nextItemId
	}
	form.submit()
}

function validValues (fieldName)  {
	newWin = window.open ('<CFOUTPUT>#jsStringFormat(jsStringformat(request.currentsite.protocolAndDomain))#</cfoutput>/dataTools/validvalues.cfm?frmFieldName='+fieldName+'&frmCountryID=0','PopUp2','height=500,width=500,scrollbars=1,resizable=1')
	newWin.focus()
}

function isCollapsableToggle () {
	var isCol = document.screenDefEdit.frmIsCollapsable_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.checked;
	var isVis = document.screenDefEdit.frmIsVisible_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.checked;
	if (isCol == true) {
		//document.screenDefEdit.frmIsVisible_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.checked=false;
		document.screenDefEdit.frmIsVisible_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.disabled=false;
	}
	if (isCol == false) {
		document.screenDefEdit.frmIsVisible_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.checked=true;
		document.screenDefEdit.frmIsVisible_<cfoutput>#jsStringFormat(frmitemid)#</cfoutput>.disabled=true;
	}
}
</script>
</cf_head>

<!--- evaluate values of all the name-values pairs in the specialformatting field--->
<cfset extrafields = application.com.structureFunctions.convertNameValuePairStringToStructure(getscreendef.specialformatting,",")>

<cfset parametersStruct = structCopy (extraFields)>

<!--- set default values for all others, saves doing it on an individual basis --->
<cfloop index="thisItem" list="#extraParameterList#">
	<cfif not structKeyExists (parametersStruct,thisItem)>
		<cfset parametersStruct[thisItem] = "">
	</cfif>
</cfloop>

<cfif getscreendef.fieldsource is "Flag">
	<!--- Removed try and catch and replaced with function to get flag structure, will have isOK=false if does not exist --->
		<cfset theFlag = application.com.flag.getFlagStructure(listfirst(getscreendef.fieldTextID ))>   <!--- listfirst rerquired because sometimes more than one flag is listed --->
</cfif>
<cfif getscreendef.fieldsource is "FlagGroup">
		<cfset theFlagGroup = application.com.flag.getFlagGroupStructure(getscreendef.fieldTextID)>
</cfif>

<cfset pageTitle = "Edit #getScreen.screenName# - #left(getScreenDef.FieldSource,30)#.#htmlEditFormat(left(getScreenDef.FieldTextID,30))#">
<cfset ext = "">
<cfif len(pageTitle) gt 75>
	<cfset ext = "...">
</cfif>

<CF_RelayNavMenu pageTitle="#left(pageTitle,75)##ext#" thisDir="screens">
	<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:if (verifyDefForm ()) {submitDefForm(0)}">
	<cfif getnext.recordcount is not 0>
		<CF_RelayNavMenuItem MenuItemText="Next" CFTemplate="javascript:if (verifyDefForm ()) {submitDefForm(#getNext.itemid#)}">
	</cfif>
	<cfif getprevious.recordcount is not 0>
		<CF_RelayNavMenuItem MenuItemText="Previous" CFTemplate="javascript:if (verifyDefForm ()) {submitDefForm(#getPrevious.itemid#)}">
	</cfif>
	<CF_RelayNavMenuItem MenuItemText="New" CFTemplate="javascript:if (verifyDefForm ()) {submitDefForm('new')}">
		<cfif isdefined("getScreen.screenID") and getScreen.screenID neq "" and len(getScreen.entitytype) neq 0>
		<cf_includejavascriptonce template="/javascript/viewentity.js">
		<cf_includejavascriptonce template="/javascript/extExtension.js">
		<cftry>
			<cfset CurrentUserEntityValue = "#request.relayCurrentUser["#getScreen.entitytype#ID"]#">
			<cfcatch type="any">
				<cftry>
					<cfquery name="getAlternativeID" datasource="#application.sitedatasource#">
						SELECT top 1 #getScreen.entitytype#ID as ID FROM #getScreen.entitytype#
					</cfquery>
					<cfset CurrentUserEntityValue = getAlternativeID.ID>
					<cfcatch type="any">
						<cfset CurrentUserEntityValue = 0>
					</cfcatch>
				</cftry>
			</cfcatch>
		</cftry>
		<!--- WAB 2011/03/16 only show this link for person/location/organisation (these are the only ones that viewEntity will work for)--->
		<cfif CurrentUserEntityValue gt 0 and listfind("0,1,2",getScreen.entitytypeid)>
			<CF_RelayNavMenuItem MenuItemText="View" CFTemplate="JavaScript:void(viewEntity('#getScreen.entitytypeid#',#request.relayCurrentUser["#getScreen.entitytype#ID"]#,'#getScreen.screenID#'));">
		</cfif>
	</cfif>
</CF_RelayNavMenu>


<div id="screenDefEditCont">
	<cfoutput>
		<form action="screenDefTask.cfm?frameIdentifier=#frameIdentifier#" method="POST"  name="screenDefEdit">
			<input type="Hidden" name="frmNextPage" value="screenDefEdit.cfm">
			<CF_INPUT type="Hidden" name="frmScreenid" value="#screenid#">
			<CF_INPUT type="Hidden" name="frmItemId" value="#frmitemid#">
	 		<cf_input type="Hidden" name="frmNextItemId" value="#IIF(isNumeric(frmItemId),DE(frmitemID),DE(''))#">
	</cfoutput>
		<cfoutput query="GetScreenDef">
			<cfset fieldsonpage = "">

			<div class="form-group">
				<label>Sort Order</label>
				<cfinclude template="screendef_sortorder.cfm">
			</div>


			<div class="form-group">
				<label>Country</label>
				<cf_displayValidValues formfieldname="frmCountryID_#itemid#"
						displayas = "select"
						validvalues ="#getScopes#"
						displayValueColumn = "countryDescription"
						dataValueColumn = "countryid"
						currentvalue = "#getScreenDef.countryid#"
					>

				<cfset fieldsonpage = listappend(fieldsonpage ,"Countryid")>
			</div>

			<div class="checkbox">
				<label>
					Active
					<CF_INPUT type="checkbox" name="frmActive_#itemid#" value="1" CHECKED="#iif( getscreendef.active is 1,true,false)#">
					<cfset fieldsonpage = listappend(fieldsonpage ,"active")>
				</label>
			</div>

			<cfif not isnumeric(itemid) >

			<div class="form-group">
				<label>Source</label>
				<cfset textbetween = "<label>Identifier</label>">
				<cfinclude template="screendef_sourceAndID.cfm">
			</div>

			<cfelse>

			<div class="form-group">
				<label>Source:</label>
				#htmleditformat(fieldSource)#

				<cfif fieldSource is "flagGroup" and theFlagGroup.isOK>
					<cfset editFlagGroupID = theFlagGroup.flagGroupID>
				<cfelseif fieldSource is "flag" and theFlag.isOK>
					<cfset editFlagGroupID = theFlag.flagGroupID>
				</cfif>
				<cfif isDefined("editFLagGroupID")>
					<a href="javascript:void(openWin('/profileManager/miniProfileFrame.cfm?flagGroupid=#editflagGroupID#','profile','height=500,width=800,menuBar=0,resize=1,toolbar=0,resizeable=1'))" >Edit </a>
				</cfif>
			</div>

			<div class="form-group">
				<label>Identifier</label>
				<!-- StartDoNotTranslate -->
				<cfinclude template="screendef_fieldtextID.cfm">
				<!-- EndDoNotTranslate -->
			</div>
			</cfif>

			<cfif screenVersion eq 1>
			<div id="frmEvalStringDiv" class="form-group">
				<label>Evaluate</label>
				<CF_INPUT class="form-control" type="text" name="frmEvalString_#itemid#" value="#trim(getScreenDef.EvalString)#" maxlength="4000" size = "50"> <!--- 2015/10/15 DAN Case 446228 --->

				<cfset fieldsonpage = listappend(fieldsonpage ,"evalString")>
			</div>
			<cfelse>
				<CF_INPUT type="hidden" name="frmEvalString_#itemid#" value="#trim(getScreenDef.EvalString)#" >
			</cfif>
			<div id="EvalString_specialDisplay"> <!---Used when a UI is provided to give easier entry to the evalScreen RJT --->
			</div>
			<div class="form-group">

				<label>Label</label>
				<CF_INPUT class="form-control" type="text" name="frmFieldLabel_#itemid#" value="#trim(getScreenDef.FieldLabel)#" maxlength="250" size = "50">
				<cfif getscreendef.translatelabel is 1>
					<BR><CF_translate showtranslationlink="true" processEvenIfNested="true" translationLinkCharacter="Translate" trimphrase="20">phr_#htmleditformat(getScreenDef.FieldLabel)# </CF_translate>
				</cfif>

				<cfset fieldsonpage = listappend(fieldsonpage ,"fieldLabel")>
			</div>


			<div class="checkbox">
				<label>
					Translate Label  (link to phrases)
					<CF_INPUT type="checkbox" name="frmTranslateLabel_#itemid#" value="1" CHECKED="#iif( getscreendef.translatelabel is 1,true,false)#">
				</label>
				<cfset fieldsonpage = listappend(fieldsonpage ,"TranslateLabel")>
			</div>

			<cfif screenVersion eq 1>
			<div class="checkbox">
				<label>
					Keep with Next
					<CF_INPUT type="checkbox" name="frmBreakLn_#itemid#" value="1" CHECKED="#iif( getscreendef.breakln is 1,true,false)#"></td>
				</label>
				<cfset fieldsonpage = listappend(fieldsonpage ,"BreakLn")>
			</div>

			<div class="checkbox">
				<label>
					Span both Columns
					<CF_INPUT type="checkbox" name="frmAllColumns_#itemid#" value="1" CHECKED="#iif( getscreendef.allcolumns is 1,true,false)#">
				</label>
				<cfset fieldsonpage = listappend(fieldsonpage ,"AllColumns")>
			</div>
			</cfif>

			<div class="checkbox">
				<label>
					Required
					<CF_INPUT type="checkbox" name="frmRequired_#itemid#" value="1" CHECKED="#iif( getscreendef.required is 1,true,false)#">
				</label>
				<cfset fieldsonpage = listappend(fieldsonpage ,"required")>
			</div>

			<div class="form-group">
				<label>Required Message</label>
				<CF_INPUT class="form-control" type="text" name="requiredMessage_frmparameter_#itemid#" value="#parametersStruct.requiredMessage#" maxlength="100" size = "20">
								#htmleditformat(outputTranslateLinkIfPhraseHTML(parametersStruct.requiredMessage))#
			</div>
			<cfset structDelete (extrafields,"requiredMessage")>

<!--- 			<!--- 2005-04-12 RND - temp inclusion of code during testing --->
			<tr>
				<td class="Label">Content Collapsable</td>
				<td  colspan="2" valign="TOP"><input type="checkbox" name="frmIsCollapsable_#itemid#" onClick="javascript:isCollapsableToggle();" value="1" <cfif getscreendef.IsCollapsable is 1>CHECKED</cfif>>
				<cfset fieldsonpage = listappend(fieldsonpage ,"IsCollapsable")>
			</tr>

			<tr>
				<td class="Label">Content Visible Onload</td>
				<td  colspan="2" valign="TOP"><input type="checkbox" name="frmIsVisible_#itemid#" value="1" <cfif getscreendef.IsVisible is 1>CHECKED</cfif>>
				<cfset fieldsonpage = listappend(fieldsonpage ,"IsVisible")>
			</tr>
 --->
			<cfif screenVersion is 1>
			<div class="form-group">
				<label>Method</label>
				<cf_displayValidValues formfieldname="frmMethod_#itemid#"
				displayas = "select"
				validvalues ="edit,view,hidden"
				currentvalue = "#getScreenDef.method#"
				>

				<cfset fieldsonpage = listappend(fieldsonpage ,"method")>
			</div>
		<cfelse >

			<div class="form-group">
				<label>Read Only</label>
				<cf_displayValidValues formfieldname="frmMethod_#itemid#"
				displayas = "radio"
				validvalues ="edit�No,view�Yes"
				currentvalue = "#getScreenDef.method#"
				onChange = "onMethodChange (this)"
				>
				Or enter an expression to evaluate to true or false
				<CF_INPUT class="form-control" type="text" name="frmMethod_#itemid#" id="frmMethod_#itemid#_Expression" value="#(listfindnocase("edit,view",getScreenDef.method)?'':getScreenDef.method)#" maxlength="250" size = "20" >

				<!--- Script to clear the radios if an expression is entered and viceVersa --->
				<script>
					onMethodChange = function (obj) {
						if (obj.type == 'radio' && obj.checked == true) {
							var expressionField = $ (obj.name + '_Expression')
							expressionField.value = ''
						} else if (obj.type == 'text' && obj.value != '') {
							var radios = document.getElementsByName('frmMethod_#itemid#');
							for (var i = 0; i< radios.length; i++) {
								console.log (radios[i]);
								if (radios[i].type == 'radio') {
									radios[i].checked = false
								}

							}
						}

					}
				</script>
				<cfset fieldsonpage = listappend(fieldsonpage ,"method")>
			</div>
</cfif>


			<cfif getscreendef.fieldsource is "flaggroup">

				<cfif theFlagGroup.isOK and theFlagGroup.flagTypeID is 1> <!--- group --->

				<div class="form-group">
					<label>Show Top Level Profile Name</label>
					<cf_displayValidValues formfieldname="NoTopFlagGroupName_frmparameter_#itemid#"
								displayas = "select"
								validvalues ="0#application.delim1#Show Top Flag Group Name,1#application.delim1#Hide Top Flag Group Name,2#application.delim1#Put Top Flag Group Name in Label Column"
								currentvalue = "#parametersStruct.NoTopFlagGroupName#"
								nullText= "Default (Show)">

					<cfset structDelete (extrafields,"noTopflaggroupname")>
				</div>

				</cfif>

			<div class="form-group">
				<label>Show Profile Name</label>
				<cf_displayValidValues formfieldname="NoFlagGroupName_frmparameter_#itemid#"
							displayas = "select"
							validvalues ="0#application.delim1#Show Flag Group Name,1#application.delim1#Hide Flag Group Name,2#application.delim1#Put Flag Group Name in Label Column"
							currentvalue = "#parametersStruct.NoFlagGroupName#"
							nullText= "Default (Show)">

				<cfset structDelete (extrafields,"noflaggroupname")>
			</div>

			</cfif>

			<cfif getscreendef.fieldsource is "flag" or getscreendef.fieldsource is "flaggroup">
			<!--- <tr><td colspan="3">
				<table border="0" cellspacing="1" cellpadding="3"> --->
				<div class="form-group">
				<!--- <td class="Label" width= "40">&nbsp;</td> --->
				<label>Show Attribute Name(s)</label>


					<cf_displayValidValues formfieldname="NoFlagName_frmparameter_#itemid#"
								displayas = "select"
								validvalues ="0#application.delim1#Show Attribute Name,1#application.delim1#Hide Attribute Name,2#application.delim1#Put Attribute Name in Label Column"
								currentvalue = "#parametersStruct.NoFlagName#"
								nullText= "Default (Show Name)">

					<cfset structDelete (extrafields,"noflagname")>
				</div><!--- </table>	 --->

				<div class="form-group">
					<!--- <td class="Label" width= "40">&nbsp;</td> --->
					<label>Show Attribute Name on own line</label>
					<cf_displayValidValues formfieldname="flagnameonOwnLine_frmparameter_#itemid#"
								displayas = "select"
								validvalues ="0#application.delim1#No,1#application.delim1#Yes"
								currentvalue = "#parametersStruct.flagnameonOwnLine#"
								nullText= "Default (No)">
					<cfset structDelete (extrafields,"flagnameonOwnLine")>
				</div><!--- </table>	 --->

			</cfif>


			<cfif getscreendef.usevalidvalues is 1>
				<!--- display all the parameters required for valid values --->
				<hr size="1">
			</cfif>

			<cfif getscreendef.fieldsource neq "HTML">
			<div class="checkbox">
				<label>
					Use <a href="javascript:validValues('#getScreendef.fieldsource#.#getscreenDef.fieldtextID#')">Valid Values </a>
					<CF_INPUT type="checkbox" name="frmUseValidValues_#itemid#" value="1" CHECKED="#iif( getscreendef.usevalidvalues is 1,true,false)#" onclick = "if (verifyDefForm ()) {submitDefForm(0)}" >
				</label>
				<cfset fieldsonpage = listappend(fieldsonpage ,"useValidValues")>
			</div>
			</cfif>


			<cfif getscreendef.fieldsource is "Flag" and theFlag.isOK and theFlag.isComplexFlag >

			<div class="form-group">
				<label>Display As</label>

				<cf_displayValidValues formfieldname="DisplayAs_frmparameter_#itemid#"
							displayas = "select"
							validvalues ="Grid"
							currentvalue = "#parametersStruct.DisplayAs#"
							allowcurrentvalue = true
							onchange = "">
						<cfset structDelete (extrafields,"DisplayAs")>
			</div>
			<cfelseif getscreendef.fieldsource is "Flag" and theFlag.isOK and theFlag.flagType.name is "radio" >


			<div class="form-group">
				<label>Display As</label>
				<cf_displayValidValues formfieldname="DisplayAs_frmparameter_#itemid#"
							displayas = "select"
							validvalues ="select#application.delim1#Drop Down Box"
							currentvalue = "#parametersStruct.DisplayAs#"
							nullText = "Default (Radio Buttons)"
							onchange = "">
						<cfset structDelete (extrafields,"DisplayAs")>
			</div>


			<cfelseif getscreendef.usevalidvalues is 1>
			<!--- display all the parameters required for valid values --->
			<div class="form-group">
				<label>Display As</label>
				<cf_displayValidValues formfieldname="DisplayAs_frmparameter_#itemid#"
							displayas = "select"
							validvalues ="Select,MultiSelect,Radio,Checkbox,TwoSelects,Hidden"
							currentvalue = "#parametersStruct.DisplayAs#"
							onchange = "javascript:if (verifyDefForm ()) {submitDefForm(0)}">

				<cfset structDelete (extrafields,"DisplayAs")>
			</div>
			<cfelseif getscreendef.fieldsource is "Flag" and theFlag.isOK and theFlag.flagtype.name is "text"   >


			<div class="form-group">
				<label>Display As</label>
				<cf_displayValidValues formfieldname="DisplayAs_frmparameter_#itemid#"
							displayas = "select"
							validvalues ="Memo,Input"
							currentvalue = "#parametersStruct.DisplayAs#"
							nullText = "Default (Input Box)"
							onchange = "">
					<cfset structDelete (extrafields,"DisplayAs")>
			</div>


			</cfif>

			<cfif getscreendef.usevalidvalues is 1>

						<div class="form-group">
							<label>Allow current value</label>

					<cf_displayValidValues formfieldname="AllowCurrentValue_frmparameter_#itemid#"
								displayas = "select"
								currentvalue = "#parametersStruct.allowCurrentValue#"
								allowcurrentvalue = "true"
								validvalues="true#application.delim1#Yes,false#application.delim1#No"
								>
							<cfset structDelete (extrafields,"allowCurrentValue")>

						</div>
					<cfif parametersStruct.displayas is "MultiSelect">
						<div class="form-group">
							<label>List Size (for multi select)</label>
							<cf_displayValidValues formfieldname="listSize_frmparameter_#itemid#"
								displayas = "select"
								validvalues ="1,2,3,4,5,6,7,8,9,10"
								currentvalue = "#parametersStruct.ListSize#"
								>
								<cfset structDelete (extrafields,"ListSize")>
						</div>
					</cfif>
			</cfif>


					<cfif (getscreendef.fieldsource is "FlagGroup" and theFlagGroup.isOK and (theFlagGroup.flagType.name is "radio"))
						or parametersStruct.displayas is "Radio" or parametersStruct.displayas is "select"
					>
					<div class="form-group">
						<label>Show Null Value</label>
						<cf_displayValidValues formfieldname="ShowNull_frmparameter_#itemid#"
									displayas = "select"
									currentvalue = "#parametersStruct.showNull#"
									allowcurrentvalue = "true"
									validvalues="true#application.delim1#Yes,false#application.delim1#No"
									>
						<cfset structDelete (extrafields,"showNull")>
					</div>


					<div class="form-group">
						<!--- <td class="Label">&nbsp;</td>	 --->
						<label>Null Text</label>
						<CF_INPUT type="text" name="NullText_frmparameter_#itemid#" value="#parametersStruct.nullText#" maxlength="25" size = "25">
					</div>
					<cfset structDelete (extrafields,"nulltext")>

					<div class="form-group">
					</cfif>

					<cfif (getscreendef.fieldsource is "FlagGroup" and theFlagGroup.isOK  and (theFlagGroup.flagType.name is "radio" or theFlagGroup.flagType.name is "checkbox"))
						or parametersStruct.displayas is "Radio" or parametersStruct.displayas is "Checkbox"
					>
					<!--- radio/checkbox flags or valid values  --->

						<div class="form-group">
						<!--- <td class="Label">&nbsp;</td> --->
						<label>Number of Columns <BR>(Radio/Checkbox Display)</label>
						<cf_displayValidValues formfieldname="column_frmparameter_#itemid#"
							displayas = "select"
							datasource = #application.sitedatasource#
							currentvalue = "#parametersStruct.column#"
							allowcurrentvalue = "true"
							validvalues="1,2,3,4"
							>
						<cfset structDelete (extrafields,"Column")>

						</div>


						<div class="form-group">
							<label>Limit Number of Checkboxes<BR>Enter Maximum Number allowed</label>

							<CF_INPUT type="text" name="MaxPermitted_frmparameter_#itemid#" value="#parametersStruct.maxpermitted#" maxlength="4" size = "4"> <BR>
							<cfset structDelete (extrafields,"maxPermitted")>
						</div>

						<div class="form-group">
							<label>Message to display if maximum exceeded</label>
							<CF_INPUT type="text" name="MaxPermittedMessage_frmparameter_#itemid#" value="#parametersStruct.maxPermittedMessage#" maxlength="200" size = "50">
							<cfset structDelete (extrafields,"maxPermittedMessage")>
						</div>


					</cfif>


			<cfif getscreendef.usevalidvalues is 1>
				<hr size="1">
			</cfif>



			<cfif parametersStruct.DisplayAs is "grid">
						<div class="form-group">
							<label>>Columns to display in Grid (list)</label>
							<CF_INPUT type="text" name="ColumnList_frmparameter_#itemid#" value="#parametersStruct.ColumnList#" maxlength="200" size = "50">
							<cfif getscreendef.fieldsource is "Flag" and theFlag.isOK and theFlag.isComplexFlag >
								<cfoutput>Choose From #htmleditformat(structKeyList(theFlag.complexflag.fields))#</cfoutput>
							</cfif>
						</div>
							<cfset structDelete (extrafields,"ColumnList")>
						<div class="form-group">
							<label>Headings of Columns (list)</label>
							<CF_INPUT type="text" name="ColumnHeaderList_frmparameter_#itemid#" value="#parametersStruct.ColumnHeaderList#" maxlength="200" size = "50">
								#htmleditformat(outputTranslateLinkIfPhraseHTML(ColumnHeaderList))#
						</div>
						<cfset structDelete (extrafields,"ColumnHeaderList")>

						<div class="form-group">
							<label>Size of Columns (list)</label>
							<CF_INPUT type="text" name="FieldSizeList_frmparameter_#itemid#" value="#parametersStruct.FieldSizeList#" maxlength="200" size = "50">
						</div>
							<cfset structDelete (extrafields,"FieldSizeList")>

						<div class="form-group">
							<label>Show Delete CheckBox</label>

							<cf_displayValidValues formfieldname="ShowDelete_frmparameter_#itemid#"
								displayas = "select"
								currentvalue = "#parametersStruct.ShowDelete#"
								allowcurrentvalue = "true"
								validvalues="1#application.delim1#Yes,0#application.delim1#No"
								nullText = "Default (Yes)"
							>

						</div>
							<cfset structDelete (extrafields,"ShowDelete")>

						<div class="form-group">
							<label>Just Allow Addition of New Rows</label>

							<cf_displayValidValues formfieldname="AddOnly_frmparameter_#itemid#"
								displayas = "select"
								currentvalue = "#parametersStruct.AddOnly#"
								allowcurrentvalue = "false"
								validvalues="1#application.delim1#Yes,0#application.delim1#No"
								nullText = "Default (No)"
							>
						</div>
							<cfset structDelete (extrafields,"AddOnly")>

						<div class="form-group">
							<label>Text to show on Add Row Link</label>
							<CF_INPUT type="text" name="AddRowText_frmparameter_#itemid#" value="#parametersStruct.AddRowText#" maxlength="200" size = "50">
									#htmleditformat(outputTranslateLinkIfPhraseHTML(AddRowText))#
						</div>
						<cfset structDelete (extrafields,"AddRowText")>

						<div class="form-group">
							<label>Number of Blank Rows</label>
							<CF_INPUT type="text" name="numberBlankRows_frmparameter_#itemid#" value="#parametersStruct.numberblankRows#" maxlength="2" size = "4">
						</div>
							<cfset structDelete (extrafields,"numberBlankRows")>

						<div class="form-group">
							<label>Function to check each row</label>
							<CF_INPUT type="text" name="gridRowCheckFunction_frmparameter_#itemid#" value="#parametersStruct.gridRowCheckFunction#" maxlength="400" size = "50">
							<BR>  By default no validation is done on a row.
							<BR>  If a row is required then by default all fields must be filled in.
							<BR>  If a row is required but no validation is to be done then enter: <B>true</B>
							<BR>  For custom validation, enter a javascript style expression using the functions: somethingEntered('fieldname') and isEmail('fieldname')
							<BR>  eg:  somethingEntered('field1') && somethingEntered('field2') && isEmail('field2')
							<BR>  All fieldnames should be referenced in lower case.
							<BR>  Other functions can be added if required
						</div>
							<cfset structDelete (extrafields,"gridRowCheckFunction")>

						<div class="form-group">
							<label>Message to display if row check fails</label>
							<CF_INPUT type="text" name="gridRowCheckMessage_frmparameter_#itemid#" value="#parametersStruct.gridRowCheckMessage#" maxlength="200" size = "50">
								#htmleditformat(outputTranslateLinkIfPhraseHTML(gridRowCheckMessage))#
								<BR>This will default to the same as "RequiredMessage" but usually you will want to specify what needs to be entered on a row.
								The message is appended with the number of the row(s) which failed the check.
						</div>
							<cfset structDelete (extrafields,"gridRowCheckMessage")>

			<cfelseif parametersStruct.DisplayAs is "memo">
						<div class="form-group">
							<label>Width of memo field</label>
							<CF_INPUT type="text" name="cols_frmparameter_#itemid#" value="#parametersStruct.cols#" maxlength="3" size = "10">
						</div>
							<cfset structDelete (extrafields,"cols")>

						<div class="form-group">
							<label>Height of memo field</label>
							<CF_INPUT type="text" name="rows_frmparameter_#itemid#" value="#parametersStruct.rows#" maxlength="3" size = "10">
						</div>
							<cfset structDelete (extrafields,"rows")>



			</cfif>

			<div class="form-group">
				<label>Max Field Length <BR>(Max Characters Allowed)</label>
				<CF_INPUT type="text" name="frmMaxLength_#itemid#" value="#getScreenDef.maxlength#" maxlength="4" size = "4">
				<cfset fieldsonpage = listappend(fieldsonpage ,"MaxLength")>
			</div>
			<div class="form-group">
				<label>Field Size <BR>(size of box on screen)</label>
				<CF_INPUT type="text" name="frmSize_#itemid#" value="#getScreenDef.size#" maxlength="3" size = "3">
				<cfset fieldsonpage = listappend(fieldsonpage ,"size")>
			</div>

			<cfif screenVersion is 1>
			<div class="form-group">
				<label>Verification</label>
				<CF_INPUT type="text" name="frmjsVerify_#itemid#" value="#trim(getScreenDef.jsVerify)#" maxlength="255" size = "50">
				<cfset fieldsonpage = listappend(fieldsonpage ,"jsVerify")>
			</div>
			</cfif>

			<div class="form-group">
				<label>Condition</label>
				<CF_INPUT type="text" name="frmCondition_#itemid#" value="#trim(getScreenDef.condition)#" maxlength="255" size = "50">
				<cfset fieldsonpage = listappend(fieldsonpage ,"Condition")>
			</div>

			<cfif screenVersion is 1>
			<div class="form-group">
				<label>Row Style</label>
				<CF_INPUT type="text" name="frmTRClass_#itemid#" value="#trim(getScreenDef.TRClass)#" maxlength="50" size = "50">
				<cfset fieldsonpage = listappend(fieldsonpage ,"trclass")>
			</div>

			<div class="form-group">
				<label>Cell Style</label>
				<CF_INPUT type="text" name="frmTDClass_#itemid#" value="#trim(getScreenDef.TDClass)#" maxlength="50" size = "50">
				<cfset fieldsonpage = listappend(fieldsonpage ,"TDClass")>
			</div>
			</cfif>

			<div class="form-group">
				<label>Notes</label>
				<Cf_relayFormElement type="textArea" maxLength="1000" fieldname="frmNotes_#itemID#" currentValue="#getScreenDef.notes#">
				<cfset fieldsonpage = listappend(fieldsonpage ,"notes")>
			</div>
			<div class="form-group">
				<label>Additional Parameters </label>

				<table id="undocumentedParamsTable">
					<cf_addUndocumentedParametersToTable
							parameterStructure = #extraFields#
							tableID = "undocumentedParamsTable"
							parameterSuffix = "frmParameter_#itemid#"
							labelText = ""
							AddNewParameterLinkText = "Add Additional Parameter"
					>
				</table>
			</div>



			<cfset fieldsonpage = listappend(fieldsonpage ,"specialFormatting")>

			<CF_INPUT type="Hidden" name="frmFieldsOnPage" value="#FieldsOnPage#">
		</cfoutput>
</form>
<div>


<cfoutput>
<table class="table table-hover table-striped">
	<tr>
		<th colspan = 3><strong>Help Notes</strong></th>
	</tr>
	<cfif "person,location,organisation," contains getscreendef.fieldsource or getscreendef.fieldsource contains "Flag_">
		<tr>
			<td valign=top><strong>Evaluate</strong></td>
			<td colspan = 2>You may use any Coldfusion Function in the evaluate field.  <BR>
				The current value of this item is in variable thisLineData<BR>
				For example  UCASE(thisLineData) will show the data in uppercase <BR>
				Do not use ##s around the function
			</td>
		</tr>
	</cfif>
		<TR><TD valign=top><strong>Label</strong></TD>
			<TD colspan = 2>This label is shown on screen and in the case of a required field, it is used in the javascript which verifies the form. <BR>
			If for some reason you do not want a label to show on screen, but you need something to show when the javascript runs, then put a carat ^ infront of the label <BR>

			There are two variables that you can set in the "other name value pairs" box to alter the text which appears when the javascript verification runs: <BR>
			requiredLabel=SomeText  		- the text appears prefixed by " enter a value for"  (or similar depending upon content) <BR>
			requiredMessage=SomeOtherText   - the text appears by itself

			</TD>
		</TR>

	<cfif getscreendef.fieldsource is "FlagGroup">
		<TR><TD colspan = 3><strong>Other Name Value Pairs</strong></TD></TR>
		<TR><td colspan = 2>Show Profile Name</td>
			<TD >
			If you do not wish the Profile Name to appear then use noFlagGroupName=1<BR>
			(This will affect all further profiles on the page unless you use noFlagGroupName=0)<BR>
			</TD>
		</tr>
		<TR><td>Checkbox Layout</td>
			<TD colspan = 2>
			To display as multiselect enter displayas=multiselect  and size=x  (where x is numbero f rows in the select box)<BR>
			To display a different number of columns of checkboxes enter column=y (default is 3)
			</TD>

		</TR>
		<TR><td>Suppress an Attribute</td>
			<TD colspan = 2>
			To not show one or more attributes within a profile enter suppressFlag=x,y,z   (where x y and z are the ids of the attributes)
			</TD>

		</TR>

	</cfif>

	<cfif getscreendef.fieldsource is "Flag">

		<TR><TD colspan = 3><strong>Other Name Value Pairs</strong></TD></TR>
			<TR><td>Show Attribute Name</td>
			<TD >
			If you do not wish the Attribute Name to appear then use noFlagName=1<BR>
			(This will affect all further attributes on the page unless you use noFlagName=0)
			</TD>
			</TR>

		<cfif theFlag.isOK and theFlag.flagType.datatableFullName is "textFlagData">
			<TR><td><strong>Display as Memo</strong></td>
			<TD >
			To display as a textarea set displayas=Memo <BR>
			</TD>
			</TR>

		</cfif>

	</cfif>

	<cfif getscreendef.fieldsource is "Query">
		<TR><td colspan = 2>Query</td>
			<TD >
			Queries can include variables ##htmleditformat(frmPersonID)##, ##htmleditformat(frmLocationID)##, ##htmleditformat(frmOrganisationID)## <BR>
			Query should be enclosed in double quotes <BR>
			</TD>
		</tr>

	</cfif>


	<cfif getscreendef.fieldsource is "Flag" or getscreendef.fieldsource is "FlagGroup">

	</cfif>


</TABLE>
</cfoutput>