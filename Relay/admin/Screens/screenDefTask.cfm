<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods:

RND 2005-04-12  Added isCollapsable & isVisible to query
WAB 2005-05-24  corrected a bug in above
NJH	2009/01/20	Bug Fix Sony Issue 1474 - escape single quotes in the eval string
RMC 2014/08/13	Code clean up
 --->

<cfset tablerequired = "screendefinition">
<cfinclude template= "..\..\templates\qryGetTableDefinition.cfm">

<cfloop index="thisItem" list="#frmItemId#">
 	<cfparam name = "frmActive_#thisItem#" default = "0">
 	<cfparam name = "frmRequired_#thisItem#" default = "0">
  	<cfparam name = "frmTranslateLabel_#thisItem#" default = "0">
   	<cfparam name = "frmUseValidValues_#thisItem#" default = "0">
   	<cfparam name = "frmAllColumns_#thisItem#" default = "0">
	<cfparam name = "frmIsCollapsable_#thisItem#" default = "0">
	<cfparam name = "frmIsVisible_#thisItem#" default = "0">
   	<cfparam name = "frmBreakLn_#thisItem#" default = "0">
   	<cfparam name = "frmLookup_#thisItem#" default = "0">
   	<cfparam name = "frmSize_#thisItem#" default = "">
   	<cfparam name = "frmMaxLength_#thisItem#" default = "">


	<!--- gather together all the name value pairs --->
 	<cfparam name = "frmSpecialFormatting_#thisItem#" default = "">
	<cfset "frmSpecialFormatting_#thisItem#" = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldSuffix="frmParameter_#thisitem#",fieldPrefix="")>





	<!--- loop through all the possible ones
		if defined then check for a value
		then add to the list --->
<!---
	<CFLOOP index="thisName" list="#extraParameterList#">
		<CFIF isdefined("frm#thisName#_#thisItem#")>
			<CFSET thisValue = evaluate("frm#thisName#_#thisItem#")>
			<CFIF thisValue is not "">
				<CFSET nameValuePairs = listAppend(nameValuePairs,"#thisName#=#thisValue#")>
			</cfif>
		</cfif>
	</cfloop>

	<!--- Now add on any extra ones which are in the free text entry box  --->
		<CFIF nameValuePairs is not "">
			<CFSET  = listAppend(evaluate("frmSpecialFormatting_#thisItem#"),nameValuePairs )>
		</cfif>
	</cfif>

 --->



	<!--- convoluted way of dealing with when there is both a select box and a text field available for input for fieldtextid --->
	<cfif isdefined("frmFieldTextID_#thisItem#")>
		<cfif  trim(evaluate("frmFieldTextID_#thisItem#")) is "">
			<cfif isdefined("frmFieldTextIDExtra_#thisItem#")>
				<cfset "frmFieldTextID_#thisItem#" = evaluate("frmFieldTextIDExtra_#thisItem#")>
			</cfif>
		</cfif>
 	</cfif>


 	<cfif not isnumeric(thisItem) >
		<!--- new Item to add --->
		<!--- check that this line has been filled in --->
		<cfif #trim(evaluate("frmFieldTextID_#thisItem#"))# is not "">
			<cfquery name="insertScreenDef" datasource="#application.siteDataSource#">
				INSERT INTO ScreenDefinition  (
					screenid,
					active,
					sortorder,
					method,
					countryid,
					fieldSource,
					fieldlabel,
					fieldTextID,
					evalstring,
					required,
					allcolumns,
					isCollapsable,
					isVisible,
					breakLn,
					maxLength,
					size,
					<cfif structKeyExists(form,"frmTDClass_#thisItem#")>TDClass,</cfif>
					<cfif structKeyExists(form,"frmTRClass_#thisItem#")>TRClass,</cfif>
					<cfif structKeyExists(form,"frmjsVerify_#thisItem#")>jsVerify,</cfif>
					translateLabel,
					usevalidvalues,
					lookup,
					specialFormatting,
					condition,
					lastupdated,
					lastupdatedby,
					lastupdatedbyPerson,
					notes)
				VALUES(<cf_queryparam value="#frmScreenid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#evaluate("frmActive_#thisItem#")#" CFSQLTYPE="cf_sql_float" >,
					<cf_queryparam value="#evaluate("frmSortOrder_#thisItem#")#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#evaluate("frmMethod_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#evaluate("frmCountryid_#thisItem#")#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cfset tempfieldSource = evaluate("frmFieldSource_#thisItem#")> <!--- this allows cf to automatically escape single quotes --->
					<cf_queryparam value="#tempfieldSource#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cfset templabel = evaluate("frmFieldLabel_#thisItem#")>
					<cf_queryparam value="#templabel#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#evaluate("frmFieldTextID_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >		,
					<!--- '#evaluate("frmEvalString_#thisItem#")#' NJH 2009/01/20 Bug Fix Sony 1474 - changed to below to allow cf to automatically escape single quotes.- not sure why though --->
					<cfset tempEvalString = evaluate("frmEvalString_#thisItem#")>
					<cf_queryparam value="#tempEvalString#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#evaluate("frmRequired_#thisItem#")#" CFSQLTYPE="cf_sql_float" >,
					<cf_queryparam value="#evaluate("frmAllColumns_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmisCollapsable_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmisVisible_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmBreakLn_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cfif evaluate("frmmaxLength_#thisItem#") is not ""><cf_queryparam value="#evaluate("frmmaxLength_#thisItem#")#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>null </cfif>,
					<cfif evaluate("frmsize_#thisItem#") is not ""><cf_queryparam value="#evaluate("frmsize_#thisItem#")#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse>null </cfif>,
					<cfif structKeyExists(form,"frmTDClass_#thisItem#")><cf_queryparam value="#evaluate("frmTDClass_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
					<cfif structKeyExists(form,"frmTRClass_#thisItem#")><cf_queryparam value="#evaluate("frmTRClass_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
					<cfif structKEyExists(form,"frmjsVerify_#thisItem#")><cf_queryparam value="#evaluate("frmjsVerify_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
					<cf_queryparam value="#evaluate("frmTranslateLabel_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmUseValidValues_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmLookup_#thisItem#")#" CFSQLTYPE="CF_SQL_bit" >,
					<cf_queryparam value="#evaluate("frmSpecialFormatting_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<cf_queryparam value="#evaluate("frmCondition_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	 				getDate(),
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#evaluate("frmNotes_#thisItem#")#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				SELECT scope_identity() AS itemid
			</cfquery>

			<cfset "#thisItem#" = insertScreenDef.itemid>
			<cfset form.frmitemid = insertScreenDef.itemid>
		</cfif>



	<cfelseif isdefined("frmDeleteThisItem_#thisItem#") >

		<cfquery name="deleteScreenDef" datasource="#application.siteDataSource#">
			DELETE FROM ScreenDefinition
		WHERE itemid =  <cf_queryparam value="#thisItem#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

	<cfelse>

		<cfquery name="updateScreenDef" datasource="#application.siteDataSource#">
			UPDATE ScreenDefinition
			SET
				<cfloop index="thisField" list="#frmFieldsOnPage#">
					<cfif isdefined("frm#thisField#_#thisitem#")>
						<cfset fieldValue = evaluate("frm#thisField#_#thisitem#")>
						<cfif listfindNoCase (numericfields,thisField) is 0>
						#thisField# =  <cf_queryparam value="#fieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						<cfelse>
						<cfif fieldvalue is "" and listfindNoCase (nullablefields,thisField) is not 0>
						#thisField# = null,
						<cfelse>
						#thisField# = #fieldValue#,
						</cfif>
					</cfif>
					</cfif>

				</cfloop>
			lastUpdated =  getDate() ,
			lastUpdatedBy =	#request.relayCurrentUser.usergroupid#,
			lastUpdatedByPerson =	#request.relayCurrentUser.personID#
		WHERE itemid =  <cf_queryparam value="#thisItem#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfif isDefined("frmLastUpdated_#thisItem#")>
			AND datediff (s,lastupdated, #createODBCDateTime(evaluate("frmLastUpdated_#thisItem#"))#) < 1
		</cfif>
		</cfquery>
	</cfif>

</cfloop>

<CFIF isdefined("frmNextItemID") and frmNextItemID is not "">
	<CFSET frmItemId =  frmNextItemID>
</cfif>

<cfoutput>

<script type="text/javascript">
	<!--
	parent.ScreensList#frameIdentifier#.location.href = '/admin/screens/screensList.cfm?frmScreenID=#frmScreenID#&frameIdentifier=#frameIdentifier#';
	//-->
</script>
</cfoutput>

<cfinclude template="#frmNextPage#">

