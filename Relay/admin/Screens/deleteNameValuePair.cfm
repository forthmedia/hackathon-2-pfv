<!--- �Relayware. All Rights Reserved 2014 --->
<!--- removes a name value pair from a list, given the Name --->
<CFPARAM Name="attributes.list">
<CFPARAM Name="attributes.name">
<CFPARAM Name="attributes.return" default="newList">
<CFPARAM Name="attributes.delim" default=",">
<CFSET delim = attributes.delim>


<cfset "caller.#attributes.return#" = application.com.regexp.removeItemFromNameValuePairString(inputString = attributes.list, itemToDelete = attributes.name , delimiter = delim)>

<!---
replaced with above call which handles quotes WAB 2007-05-14
 <CFSET position = listContainsNoCase(attributes.list,attributes.name & "=",delim)>
<CFIF position is not 0>
	<CFSET "caller.#attributes.return#" = listDeleteAt(attributes.list,position,delim)>
</cfif> --->
