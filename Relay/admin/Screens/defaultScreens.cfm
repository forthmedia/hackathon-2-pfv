<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			defaultScreens.cfm
Author:				WAB
Date started:			2007/12/12

Description:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display
2012-12-5			IH			Case 432341 Check if reportDesigner is enabled before calling getReportsQuery()
2012-12-11			IH			Change moduleTextID for Report Designer to reportManager
2013-01-09			PPB			Case 432341 avoid "frmHomePageDashboardType is undefined" error after save
2014-08-21			RPW			Add to Home Page options to display Activity Stream and Metrics Charts
2014-11-13			ACPK		Moved frmOrganisationTypeID Relay form element further up the page
2016/01/25			NJH			Removed cfform and trs/tds.
2016/01/26			NJH			JIRA BF-367 - changed jquery to hide form elements correctly.
2016-05-04			WAB 		BF-676 Knock on effect of PROD2016-878. Change getScreenList to use argument entityTypeID not entityType.  

Possible enhancements:


This screen is used to select the default screens which is show to each user when they click on a person/location/organisation

There are system wide defaults which can be overridden on a userGroup / organisationtype basis
The values are stored in the preferences table - see screens.cfc for details

--->

<cfset application.com.request.setDocumentH1(text="phr_sys_nav_Home_Page_Options")>
<cfscript>
	defaultStruct = structNew();
	defaultStruct.defaultscreen.person[0] = '' ;
	defaultStruct.defaultscreen.location[0] = '';
	defaultStruct.defaultscreen.organisation[0] = '';
</cfscript>

<cfset homepageStruct = application.com.relayHomePage.getDefaultHPStruct()>

<cfquery name="getPersonalUserGroup" datasource="#application.siteDataSource#">
	select userGroupID from usergroup where personID=<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>

<cfparam name="form.frmusergroupid"  default= "#getPersonalUserGroup.userGroupID#">
<cfparam name="form.frmorganisationtypeid"  default= 0>
<cfparam name="disableOrganisationTypeID"  default= "false">
<cfparam name="disableusergroupid"  default= "false">

<!--- If it gets to confusin to allow different defaults by organisationtype other than at the base level then can be switched off
	<cfif frmUsergroupID is 0>
		<cfset disableOrganisationTypeID = true>
	</cfif>

--->

<cfquery name="userGroups"  datasource = "#application.sitedatasource#">
	select usergroupid, name , personid,
	case when personid is null then 1 else 2 end as sortIndex,
	case when personid is null then 'User Groups' else 'Individual Users' end as optGroup
	from usergroup where usergroupid <> 0
	union
	select 0,'Any User', null, 0 as sortIndex, 'Default' as optGroup
	order by sortIndex, name
</cfquery>

<cfquery name="organisationTypes"  datasource = "#application.sitedatasource#">
	select TypeTextiD, organisationTypeID
	 from organisationType
	union
	select 'Any Type', 0
	order by organisationTypeID
</cfquery>

<cfset DashboardQry = application.com.relayHomePage.DashboardDisplayOptions()>
<!--- <cfset getReports = application.com.jasperServer.getReportsQuery()> 		Case 432341 moved further down --->

<!--- get the record for the current userGroupID --->
<cfquery name="thisUserGroup"  dbtype="query">
	select * from userGroups
	where usergroupID = #frmUserGroupID#
</cfquery>

<cfif thisUserGroup.personid is not "">
<!--- we are dealing with a personal user group, get list of other groups they are a member of --->
	<cfquery name="getUserGroupBelongedTo"  datasource = "#application.sitedatasource#">
	select userGroupid from rightsgroup where personid =  <cf_queryparam value="#thisUserGroup.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset userGroupList = 	valueList(getUserGroupBelongedTo.usergroupid)>
<cfelse>
	<cfset userGroupList = 	frmUserGroupID>
</cfif>

<cfset reportManagerIsActive = application.com.relayMenu.isModuleActive("reportManager")>		<!--- 2013-01-09 PPB Case 432341 --->


<cfif isDefined ("frmUpdate")>
	<cftry>
		<cfset screenSettings = application.com.preferences.getPreferenceDataFromDatabase
		(
		entitytypeid = 68,entityid = frmusergroupid,
		hive = 'datascreens', key = '',
		defaultStructure = defaultStruct
		) >

		<cfset screenSettings.defaultscreen.person[frmOrganisationTypeID] = personscreen>
		<cfset screenSettings.defaultscreen.location[frmOrganisationTypeID] = locationscreen>
		<cfset screenSettings.defaultscreen.organisation[frmOrganisationTypeID] = organisationscreen>


		<cfset application.com.preferences.savePreferenceDataToDataBase1
		(
		entitytypeid = 68,entityid = frmusergroupid,
		hive = 'datascreens', key = '',
		data = screenSettings
		) >

		<cfif reportManagerIsActive>		<!--- 2013-01-09 PPB Case 432341 --->
	<!--- Start - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->
		<cfset reportSettings = application.com.preferences.getPreferenceDataFromDatabase
		(
		entitytypeid = 0,entityid = frmusergroupid,
		hive = 'reportDashboard', key = '',
		defaultStructure = homepageStruct
		) >

		<cfset reportSettings.homepage.dashboardtype = frmHomePageDashboardType>

		<cfif frmHomePageDashboardType eq "FullScreen">

			<cfif structKeyExists(form,"frmDashboardsToDisplay")><cfset reportSettings.homepage.DashboardsToDisplay = form.frmDashboardsToDisplay></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport1")><cfset reportSettings.homepage.FullScreenReport1 = form.frmFullScreenReport1></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport2")><cfset reportSettings.homepage.FullScreenReport2 = form.frmFullScreenReport2></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport3")><cfset reportSettings.homepage.FullScreenReport3 = form.frmFullScreenReport3></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport4")><cfset reportSettings.homepage.FullScreenReport4 = form.frmFullScreenReport4></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport5")><cfset reportSettings.homepage.FullScreenReport5 = form.frmFullScreenReport5></cfif>
			<cfif structKeyExists(form,"frmFullScreenReport6")><cfset reportSettings.homepage.FullScreenReport6 = form.frmFullScreenReport6></cfif>

		</cfif>

	    <cfif frmHomePageDashboardType eq "SplitScreen">

			<cfif structKeyExists(form,"frmDashboardsToDisplay")><cfset reportSettings.homepage.DashboardsToDisplay = form.frmDashboardsToDisplay></cfif>
			<cfif structKeyExists(form,"frmSplitScreenReport1")><cfset reportSettings.homepage.SplitScreenReport1 = form.frmSplitScreenReport1></cfif>
			<cfif structKeyExists(form,"frmSplitScreenReport2")><cfset reportSettings.homepage.SplitScreenReport2 = form.frmSplitScreenReport2></cfif>
			<cfif structKeyExists(form,"frmSplitScreenReport3")><cfset reportSettings.homepage.SplitScreenReport3 = form.frmSplitScreenReport3></cfif>

		</cfif>

		<cfset application.com.preferences.savePreferenceDataToDataBase1
		(
		entitytypeid = 0,entityid = frmusergroupid,
		hive = 'reportDashboard', key = '',
		data = reportSettings
		) >
	<!--- End - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->
		</cfif>		<!--- 2013-01-09 PPB Case 432341 --->

		<cfset message = "Successfully updated user preferences.">
		<cfset messageType = "success">

		<cfcatch>
			<cfset message = "Problem saving user preferences.">
			<cfset messageType = "error">
		</cfcatch>
	</cftry>
</cfif>


<cfset screenList = structNew()>
<cfset currentBestMatches = structNew()>

<cfloop list = "Organisation,Location,Person" index = "entity">
	<cfset screenList[entity] = application.com.screens.getscreenlist (entityTypeID = application.entityTypeID[entity], usergroupid = frmusergroupid)>
	<cfif frmOrganisationTypeID is not 0 >
		<cfset types = frmOrganisationTypeID>
	<cfelse>
		<cfset types = valuelist (organisationTypes.OrganisationTypeID)>
	</cfif>
	<cfloop index="type" list = "#types#">
		<cfset currentBestMatches[type][entity] = application.com.screens.getdefaultScreen(entityType=entity, organisationtypeid = type, usergroupid = frmusergroupid, usergroups = userGroupList )>
	</cfloop>
</cfloop>

	<cfset currentSettings = application.com.preferences.getPreferenceDataFromDatabase
	(
	entitytypeid = 68,entityid = frmusergroupid,
	hive = 'datascreens', key = '',
	defaultStructure = defaultStruct
	) >

<!--- Start - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->
	<cfset dashboardSettings = application.com.preferences.getPreferenceDataFromDatabase
	(
	entitytypeid = 0,entityid = frmusergroupid,
	hive = 'reportDashboard', key = '',
	defaultStructure = homepageStruct
	) >
<!--- End - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->

<cfset required = frmUserGroupID is 0 and frmOrganisationTypeID is 0?true:false>

<cfset DashboardNumberQry = application.com.relayHomePage.DashboardNumberOptions(DashType = dashboardsettings.homepage.dashboardtype)>

<!--- for using within screens --->
		<SCRIPT type='text/javascript'>

	<!--
		function submitPrimaryForm() {
				var form = document.mainForm;

				form.frmUpdate.click()

			}

	//-->

		</SCRIPT>

<form name = "mainForm" id = "mainForm" method="POST" >

	<CF_relayFormDisplay>

		<cfif isDefined("message")>
			<cfoutput>#application.com.relayUI.message(message=message,type=messageType)#</cfoutput>
		</cfif>

		<cfif thisUserGroup.personid is request.relaycurrentuser.personid>
			<cfset introText = "Choose the default screen which will show when you view/edit a Person/Location/Organization ">
		<cfelse>
			<cfset introText = "Choose the default screen which will show when a member of #thisUserGroup.name# views/edits a Person/Location/Organization ">
		</cfif>
		<CF_relayFormElementDisplay relayFormElementType="HTML" CurrentValue="#application.com.relayUI.message(message=introText,type='info',showClose=false)#" label="">

		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#frmUserGroupID#" fieldName="frmUserGroupID" query="#userGroups#" display="name" value="usergroupid" label="User Group" nullValue="" required="yes" onchange="this.form.submit()" group = "optgroup" disabled = #disableusergroupid#>
		<cfif disableusergroupid>
		<CF_INPUT type="hidden" name = "frmUserGroupID" value = "#frmUserGroupID#">
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#frmOrganisationTypeID#" fieldName="frmOrganisationTypeID" query="#organisationTypes#" display="TypeTextID" value="organisationTypeID" label="Organization Type" nullValue=""  onchange="this.form.submit()" required="yes" disabled = #disableOrganisationTypeID#>
		<cfif disableOrganisationTypeID>
			<CF_INPUT type="hidden" name = "frmOrganisationTypeID" value = "#frmOrganisationTypeID#">
		</cfif>

	<!--- Start - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->

		<cfif reportManagerIsActive>	<!--- 2012-12-5	IH	Case 432341 Check if reportManager is enabled before calling getReportsQuery() --->

			<cfset getReports = application.com.jasperServer.getReportsQuery()>		<!--- 2012-12-5	IH	Case 432341 --->
			<cfparam name="dashboardsettings.homepage.dashboardtype" default = "">
			<cfset homePageDashboardType = dashboardSettings.homepage.dashboardtype>

			<!--- 2013-03-05	YMA	START: Case 433813 Use ajax to populate report options based on homepage view --->
			<cf_head>
			<cfoutput>
			<script>
				jQuery(document).ready(function() {
					refreshReportOptions(jQuery('##frmHomePageDashboardType').get(0),#frmusergroupid#);
				});

				function refreshReportOptions(x,frmusergroupid){

						//BF-473 RJT start with a Standard as default in case x.value is none of these
						fullScreenItemDisplay = 'none';
						splitScreenItemDisplay = 'none';

						if(x.value=="FullScreen") {
							fullScreenItemDisplay = 'block';
							splitScreenItemDisplay = 'none';
						}
						if(x.value=="SplitScreen") {
							fullScreenItemDisplay = 'none';
							splitScreenItemDisplay = 'block';
						}
						if(x.value=="Standard") {
							fullScreenItemDisplay = 'none';
							splitScreenItemDisplay = 'none';
						}
						//2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts
						if(x.value=="Advanced") {
							fullScreenItemDisplay = 'none';
							splitScreenItemDisplay = 'none';
						}

						jQuery('div[id^=frmFullScreenReport][id$=_formgroup]').css('display',fullScreenItemDisplay);
						jQuery('div[id^=frmSplitScreenReport][id$=_formgroup]').css('display',splitScreenItemDisplay);
					}
			</script>
			</cfoutput>
			<cf_head>

			<CF_relayFormElementDisplay onChange="javascript:refreshReportOptions(this,#frmusergroupid#);" relayFormElementType="select" fieldname ="frmHomePageDashboardType"  query="#DashboardQry#" value="value" display="display" currentvalue="#dashboardsettings.homepage.dashboardtype#" label = "Home Page View" nullText = "Choose a home page view"  required ="no"  message= "You must select a default Homepage Dashboard Screen">

			<CF_relayFormElementDisplay relayFormElementType="select" fieldname ="frmDashboardsToDisplay" bindFunction="cfc:webservices.settingsWS.DashboardNumberOptions(DashType={frmHomePageDashboardType})" value="value" display="display" currentvalue="#dashboardsettings.homepage.DashboardsToDisplay#" label = "Number of reports to display" nullText = "Choose number of reports to display"  required ="no"  message= "You must select a number of reports to display">

				<cfparam name="dashboardsettings.homepage.FullScreenReport1" default = "">
				<cfparam name="dashboardsettings.homepage.FullScreenReport2" default = "">
				<cfparam name="dashboardsettings.homepage.FullScreenReport3" default = "">
				<cfparam name="dashboardsettings.homepage.FullScreenReport4" default = "">
				<cfparam name="dashboardsettings.homepage.FullScreenReport5" default = "">
				<cfparam name="dashboardsettings.homepage.FullScreenReport6" default = "">

					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport1"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport1#" label = "Full Screen Report (Top Left)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport2"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport2#" label = "Full Screen Report (Top Right)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport3"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport3#" label = "Full Screen Report (Middle Left)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport4"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport4#" label = "Full Screen Report (Middle Right)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport5"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport5#" label = "Full Screen Report (Bottom Left)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmFullScreenReport6"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.FullScreenReport6#" label = "Full Screen Report (Bottom Right)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">

				<cfparam name="dashboardsettings.homepage.SplitScreenReport1" default = "">
				<cfparam name="dashboardsettings.homepage.SplitScreenReport2" default = "">
				<cfparam name="dashboardsettings.homepage.SplitScreenReport3" default = "">

					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmSplitScreenReport1"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.SplitScreenReport1#" label = "Split Screen Report (Top)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmSplitScreenReport2"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.SplitScreenReport2#" label = "Split Screen Report (Middle)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">
					<CF_relayFormElementDisplay  relayFormElementType="select" fieldname ="frmSplitScreenReport3"  query="#getReports#" value="reportURI" display="reportURI" currentvalue="#dashboardsettings.homepage.SplitScreenReport3#" label = "Split Screen Report (Bottom)" nullText = "Choose a Report"  required ="no"  message= "You must select a report">

		</cfif>		<!--- Case 432341 --->
	<!--- End - 2012-01-12			RMB			P-LEN024 - CR066 - Added setting for homescreen display --->

		<cfset tempItemsDone = "">
		<cfloop list = "Organisation,Location,Person" index = "entity">

				<cfparam name="currentSettings.defaultscreen[entity][frmorganisationtypeid]" default = "">

				<cfset screenEntityName = application.com.regExp.proper(inputstring=application.com.relayEntity.getEntityType(entityTypeID=Entity).label)>

				<CF_relayFormElementDisplay relayFormElementType="select" fieldname ="#entity#Screen"  query="#screenlist[entity]#" value="screenid" display="screenname" currentvalue="#currentSettings.defaultscreen[entity][frmorganisationtypeid]#" label = "Default #screenEntityName# Screen" nullText = "Choose a screen"  required = #required#  message= "You must select a default #screenEntityName# Screen ">
						<cfloop index="type" list = "#types#">
							<cfif currentBestMatches[type][entity].screenid is not currentSettings.defaultscreen[entity][frmorganisationtypeid]>
								<cfset thisItemKey = "#currentBestMatches[type][entity].usergroupid#-#currentBestMatches[type][entity].organisationtypeid#-#currentBestMatches[type][entity].screenid#">
								<cfif listfindnocase(tempItemsDone,thisItemKey) is 0 >
									<cfset tempItemsDone = listappend(tempItemsDone, thisItemKey)>
									<CF_relayFormElementDisplay relayFormElementType="html" label="" currentvalue="">

										<cfoutput><cfif thisUserGroup.personid is request.relaycurrentuser.personid>You <cfelse>#htmleditformat(thisUserGroup.name)#</cfif> will see #currentBestMatches[type][entity].screenname#  for Organization Type #currentBestMatches[type][entity].OrganisationType# <cfif currentBestMatches[type][entity].usergroupid is not frmUserGroupID>as a member of #currentBestMatches[type][entity].usergroupname# </cfif><BR></cfoutput>
									</CF_relayFormElementDisplay >

								</cfif>
							</cfif>
						</cfloop>

		</cfloop>

		<CF_relayFormElementDisplay relayFormElementType="submit" label="submit" currentValue = "Save" fieldname ="frmUpdate">

	</CF_relayFormDisplay>

</form>