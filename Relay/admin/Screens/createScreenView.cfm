<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting showdebugoutput = true>
<!--- 

File name:			createScreenView.cfm	
Author:				WAB
Date started:			2008/10/06
	
Description:		Takes a screen and creates a view from it
					Allows user to specify some settings for the view
					Actually created for P-SOPHSprint5 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->


<!--- Get details of screen and its parameter structure --->
<cfquery name="GetScreen" dataSource="#application.siteDataSource#">
select * from screens where screenid =  <cf_queryparam value="#frmScreenid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>
<cfset screenParameterStructure= application.com.regExp.convertNameValuePairStringToStructure (inputString = GetScreen.parameters,delimiter = ",", DotNotationToStructure = true) >
<!--- make sure there is a .view substructure --->
<cfparam name="screenParameterStructure.view" default=#structNew()#>


<!--- These are the default settings 
		note that these need to be the same as the default settings in function createViewFromScreen
--->
<cfset defaultSettings  = {booleanOn="1",booleanOff="0",showAllCheckboxesIndividually="false",viewName="vScreen_#frmScreenID#"}>

<!--- These blank settings are used on the form  when first loaded if not overriden by existing serttings --->
<cfset blankSettings  = {booleanOn="",booleanOff="",showAllCheckboxesIndividually="",viewName=""}>
	


<cfif isDefined("frmCreate")>
	<!--- form has been submitted so collect together all the parameter form fields into one structure --->
	<cfset formParameterStructure = application.com.regexp.CollectFormFieldsIntoStructure(fieldPrefix = "frmParams",includenulls=true)>
 	<cfparam name= "formParameterStructure.showAllCheckboxesIndividually" default = "">   <!--- this required to deal with checkbox not being checked --->


	<cfif isDefined("saveTheseSettings")>
		<!--- take existing params and overwrite with any which come from the form (need this just in case there is one set which is not dealt with on form) --->
		<cfset structAppend (screenParameterStructure.view,formParameterStructure)>

		<!--- make into name value pairs --->
		<cfset parameters = application.com.structurefunctions.convertStructureToNameValuePairs	(struct = screenParameterStructure, recurse= true, includeNulls = false)>


		<!--- update --->
		<cfquery name="updateScreen" dataSource="#application.siteDataSource#">
		update screens set parameters =  <cf_queryparam value="#parameters#" CFSQLTYPE="CF_SQL_VARCHAR" >  where screenid =  <cf_queryparam value="#frmScreenid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
	</cfif>


	<!--- settings to use when creating the view are the defaults merged with any non null items on the form --->
	<cfset settings = application.com.structurefunctions.Structmerge(struct1 = defaultSettings,struct2 = formParameterStructure,mergeToNewStruct = true, dontMergeNulls = true)>

	<cfset createViewResult = application.com.gridsAndViews.createViewFromScreen(
		screenid = frmScreenID,
		viewname = settings.ViewName,
		showAllCheckboxesIndividually=settings.showAllCheckboxesIndividually,
		booleanOn = settings.booleanOn ,
		booleanOff = settings.booleanOff,
		getSettingsFromScreen = false				
	)>

	<!--- rudimentary error handling --->
	<!--- NYB 2009-02-20 Sophos bug:  added "isDefined("createViewResult.isOK") and " --->
	<cfif not isDefined("createViewResult.isOK") or NOT createViewResult.isOK>
		<cfoutput>
		View Not Created <BR>
		<!--- NYB 2009-02-20 Sophos bug:  added if around error --->
		<cfif isDefined("createViewResult.Error")>#createViewResult.Error#</cfif>
		</cfoutput>
	<cfelse>
		
	</cfif>


	<cfset currentSettings = formParameterStructure>

<cfelse>

	<cfset currentSettings = blankSettings>
	<cfset structAppend (currentSettings, screenParameterStructure.view)>

</cfif>



<cfoutput>
<cfform>

	 <cfset request.relayFormDisplayStyle = "html-table">
	<CF_relayFormDisplay>
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmScreenID#" fieldName="frmScreenID" label="">
			<CF_relayFormElementDisplay relayFormElementType="html" label="Create View from Screen:" currentvalue = "" >
			<CF_relayFormElementDisplay relayFormElementType="html" label="#getScreen.ScreenName# (#getScreen.screenid#)" currentvalue = "" >
			<CF_relayFormElementDisplay relayFormElementType="html" label="" currentvalue = "To override default settings (in brackets) enter values below " spancols="yes">

			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmParams_ViewName" currentValue="#currentSettings.ViewName#"  label="Name of View" size = "20" noteText = "(#defaultSettings.viewName#)" NoteTextPosition="right" NoteTextImage="No">				
			<CF_relayFormElementDisplay relayFormElementType="checkbox" currentValue="#currentSettings.showAllCheckboxesIndividually#" value = "true"  fieldName="frmParams_showAllCheckboxesIndividually" label="Create a separate column <BR>for each Checkbox" noteText = '(#iif(defaultSettings.showAllCheckboxesIndividually,de("Yes"),de("No"))#)' NoteTextPosition="right" NoteTextImage="No">

			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmParams_BooleanOn" currentValue="#currentSettings.booleanOn#"  label="Value for Checkbox Set" size = "2" noteText = "(#defaultSettings.booleanOn#)" NoteTextPosition="right" NoteTextImage="No">				
			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmParams_BooleanOff" currentValue="#currentSettings.booleanOff#"  label="Value for Checkbox UnSet" size = "2" noteText = "(#defaultSettings.booleanOff#)" NoteTextPosition="right" NoteTextImage="No">				
			
			<CF_relayFormElementDisplay relayFormElementType="checkbox" currentValue="1" value =""  fieldName="saveTheseSettings" label="Save these settings">
			<CF_relayFormElementDisplay relayFormElementType="submit" currentValue="Submit" fieldName="frmCreate" label="">
	</CF_relayFormDisplay>




</cfform>
</cfoutput>
<CF_ABORT>





Settings

To output checkboxes and radio buttons as individual columns  displayAs=IndividualFlags



