/**
 * Binds changes of the field frmFieldTextID to update the way the evalString is updated, just as a normal entry box or as multientry boxes (originally required for screen widgets)
 */


function updateEvalStringUI(){
	var thisFieldSource=jQuery("[name^=frmFieldSource]").val(); 	
	var fieldTextID=jQuery("[name^=frmFieldTextID]").val(); 
	
	jQuery.get('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=screenWS&methodName=getSpecialEvalStringUI&returnFormat=json&thisFieldSource=' + thisFieldSource + "&fieldTextID=" + fieldTextID , 
		function(data) {
		
			
			var dataStructure=JSON.parse(data)
			
			if(dataStructure.hasSpecialUI){
				jQuery('#EvalString_specialDisplay').html(jQuery(dataStructure.html)); 	
				sendDataFromEvalStringToSpecialUI();
				pullDataIntoFromEvalStringFromSpecialUI(); //first sending then pulling backs aquires any defaults etc
				jQuery('#EvalString_specialDisplay').show();
				jQuery("#frmEvalStringDiv").hide();
				
				
				
				//bind any change to an input with javascript to pull that back into the evalString
				jQuery('#EvalString_specialDisplay :input').on('input', function() { 
					pullDataIntoFromEvalStringFromSpecialUI();
				});

				
			}else{
				jQuery('#EvalString_specialDisplay').hide();
				jQuery("#frmEvalStringDiv").show();
				jQuery('#EvalString_specialDisplay').html(""); 
			}
	
		}
	);

}

/**
 * Moves data from the "real" EvalString field into the UI that has been provided
 */
function sendDataFromEvalStringToSpecialUI(){
	var evalStringValue=jQuery("[name^=frmEvalString]").val();
	
	var parameters = evalStringValue.split(',');
	
	for (var i = 0; i < parameters.length; i++) {
		var parameterKeyValue = parameters[i].split('=');
	    if (parameterKeyValue.length===2){
	    	//try to find a special UI element put this in
	    	jQuery('#EvalString_specialDisplay [name="'+parameterKeyValue[0]+'"]').val(parameterKeyValue[1]);
	    }
	}
			
}

function pullDataIntoFromEvalStringFromSpecialUI(){
	var newEvalString="";
	
	jQuery('#EvalString_specialDisplay :input').each(function( index, element ) {
		if (newEvalString!=""){
			newEvalString+= ",";
		}
		newEvalString+= jQuery(element).attr("name")+"="+jQuery(element).val();
	});
	
	jQuery("[name^=frmEvalString]").val(newEvalString);
}

jQuery( document ).ready(function() {
	
	
	jQuery("[name^=frmFieldTextID]").change(updateEvalStringUI);
	jQuery("[name^=frmFieldTextID]").on('rw:ajaxupdate',updateEvalStringUI);
	
	updateEvalStringUI();
	
});
		
