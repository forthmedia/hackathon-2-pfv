<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2007/11/28   WAB added rights to screens
2012-12-18	PPB	Case 428315 maintain lastUpdatedBy/lastUpdated
2016-01-21	PROD2015-370 replace twoSelects with multi-select
--->

<!--- update / create new screens --->
<CFPARAM  Name= "frmPreDefined" default = "0">
<CFPARAM  Name= "frmActive" default = "0">
<CFPARAM  Name= "frmSuite" default = "0">
<CFPARAM  Name= "frmCopyScreen" default = "">
<CFPARAM  Name= "frmDeleteScreen" default = "">

<CFSET updatetime = request.requesttimeodbc>


	<CFIF frmScreenID is  -1 >
		<!--- this is a new record.  Add a simple record which is then updated using the same code as the regular update --->

		<cfquery name="insertScreen" datasource="#application.siteDataSource#">
		insert into Screens
				(ScreenID,
				screenname,
				scope,
				suite,
				active,
				createdby,
				created,					<!--- 2012-12-18 PPB Case 428315 --->
				lastUpdatedBy,				<!--- 2012-12-18 PPB Case 428315 --->
				lastUpdated,				<!--- 2012-12-18 PPB Case 428315 --->
				lastUpdatedByPerson
				)
		select 	max(screenid) + 1 ,
				'',
				0,
				0,
				1,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 								<!--- 2012-12-18 PPB Case 428315 --->
				<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,															<!--- 2012-12-18 PPB Case 428315 --->
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 								<!--- 2012-12-18 PPB Case 428315 --->
				<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,															<!--- 2012-12-18 PPB Case 428315 --->
				<cf_queryparam value="#request.relayCurrentUser.personId#" CFSQLTYPE="CF_SQL_INTEGER" >
		from screens
		</cfquery>

		<cfquery name="getScreen" datasource="#application.siteDataSource#">
		select * from screens
		where createdby = 	#request.relayCurrentUser.usergroupid#
		AND created =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</CFQUERY>

		<CFSET frmScreenID = getScreen.screenid>
		<cfset form.newScreen = getScreen.screenid>   <!--- bit of a hack to get uusergroup update to work --->

	</CFIF>

	<cfset frmParameters = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldSuffix="Parameter",fieldPrefix="")>

	<cfif frmDeleteScreen is not "1">
		<!--- now do update --->
		<!--- hack to get entitytypeid --->
		<cfquery name="getEntityTypeID" datasource="#application.siteDataSource#">
		select entityTypeID
		from flagEntityType
		where tablename =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>
		<CFSET frmEntityTypeID = getEntityTypeID.entityTypeID>


		<CFIF frmScreenTextID IS ''><CFSET frmScreenTextID = "scr_#frmScreenID#"></cfif>

		<cfquery name="updatescreens" datasource="#application.siteDataSource#">
		update Screens
		SET 	ScreenTextID =  <cf_queryparam value="#frmScreenTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ScreenName =  <cf_queryparam value="#frmScreenName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				active =  <cf_queryparam value="#frmActive#" CFSQLTYPE="cf_sql_float" > ,
				suite =  <cf_queryparam value="#frmSuite#" CFSQLTYPE="CF_SQL_bit" > ,
				sortorder =  <cf_queryparam value="#iif(frmSortOrder is "",0,frmSortOrder)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				entitytype =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				entitytypeid =  <cf_queryparam value="#frmEntityTypeid#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				scope =  <cf_queryparam value="#frmScope#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				viewer =  <cf_queryparam value="#frmviewer eq 'showScreen'?'showScreen':listLast(frmviewer)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				viewedit =  <cf_queryparam value="#frmMethod#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				preinclude =  <cf_queryparam value="#frmPreInclude#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				postinclude =  <cf_queryparam value="#frmPostInclude#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				parameters =  <cf_queryparam value="#frmParameters#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				tableWidth =  <cf_queryparam value="#iif(frmtableWidth is "",0,frmTableWidth)#" CFSQLTYPE="CF_SQL_Integer" > ,
				predefined =  <cf_queryparam value="#frmpredefined#" CFSQLTYPE="CF_SQL_bit" > ,
				securityTask =  <cf_queryparam value="#frmSecurityTask#" CFSQLTYPE="CF_SQL_VARCHAR" >, 													<!--- 2012-12-18 PPB Case 428315 --->
				lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,								<!--- 2012-12-18 PPB Case 428315 --->
				lastUpdated = <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >														<!--- 2012-12-18 PPB Case 428315 --->
		where ScreenID =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="qry_deletescreenOrgTypes" datasource="#application.siteDataSource#">
			delete from screenOrganisationType
			where screenID =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif listfind("0,1,2",frmEntityTypeid) neq 0>
			<cfif structKeyExists (form, "Selected_OrganisationTypeID") and form.Selected_OrganisationTypeID is not "">
				<cfloop index="i" list="#form.Selected_OrganisationTypeID#">
					<cfquery name="qry_deletescreenOrgTypes" datasource="#application.siteDataSource#">
						INSERT INTO screenOrganisationType (screenID,OrganisationTypeID)
						VALUES (<cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >)
					</cfquery>
				</cfloop>
			</cfif>
		</cfif>
	<cfelse>
		<!--- frmDeleteScreen = 1 --->
		<cfquery name="deleteScreen" datasource="#application.siteDataSource#">
		delete
		from	screens
		where 	screenid =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<!--- delete stuff from screen def - doesn't actually matter whether there were any records --->
		<cfquery name="deleteScreen" datasource="#application.siteDataSource#">
		delete
		from	screendefinition
		where 	screenid =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>



	</cfif>

	<CF_UGRecordManagerTask>


<CFIF frmCopyScreen is not "">

<!--- Create a New Screen Based on Current Screen --->


<cfquery name="insertscreenssamedata" datasource="#application.siteDataSource#">
insert into
Screens (ScreenID,
		ScreenTextID,
		ScreenName,
		active,
		suite,
		entitytype,
		entitytypeid,
		sortOrder,
		scope,
		viewer,
		viewedit,
		preinclude,
		postinclude,
		parameters,
		tablewidth,
		predefined,
		createdby,
		created,									<!--- 2012-12-18 PPB Case 428315 --->
		lastUpdatedBy,								<!--- 2012-12-18 PPB Case 428315 --->
		lastUpdated,								<!--- 2012-12-18 PPB Case 428315 --->
		lastUpdatedByPerson)
select (select max(screenid) + 1 from screens),
		ScreenTextID + 'Copy',
		'copy of ' + ScreenName,
		active,
		suite,
		entitytype,
		entitytypeid,
		sortorder,
		scope,
		viewer,
		viewedit,
		preinclude,
		postinclude,
		parameters,
		tablewidth,
		predefined,
		#request.relayCurrentUser.usergroupid#,
		#updateTime#,								<!--- 2012-12-18 PPB Case 428315 --->
		#request.relayCurrentUser.usergroupid#, 	<!--- 2012-12-18 PPB Case 428315 --->
		#updateTime#,								<!--- 2012-12-18 PPB Case 428315 --->
		#request.relayCurrentUser.personID#
from screens where screenid =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>


		<cfquery name="getScreen" datasource="#application.siteDataSource#">
		select * from screens
		where createdby = 	#request.relayCurrentUser.usergroupid#
		AND created =  <cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		</CFQUERY>

		<CFSET newScreenID = getScreen.screenid>

		<cfquery name="qry_Insert_screenOrgTypes" datasource="#application.siteDataSource#">
			INSERT INTO screenOrganisationType (screenID,OrganisationTypeID)
			select <cf_queryparam value="#newScreenID#" CFSQLTYPE="CF_SQL_VARCHAR" >,organisationTypeID from screenOrganisationType where screenID =  <cf_queryparam value="#frmScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="popscreendef" datasource="#application.siteDataSource#">
		insert into ScreenDefinition
				(ScreenID,
				SortOrder,
				CountryID,
				FieldSource,
				FieldTextID,
				Fieldlabel,
				Method,
				Translatelabel,
				active,
				UseValidValues,
				lookup,
				required,
				Allcolumns,
				BreakLn,
				MaxLength,
				size,
				specialformatting,
				TDClass,
				TRClass,
				jsVerify,
				condition,
				lastupdatedby,
				lastupdated
				)
		select
				<cf_queryparam value="#newScreenID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				SortOrder,
				CountryID,
				FieldSource,
				FieldTextID,
				Fieldlabel,
				Method,
				Translatelabel,
				active,
				UseValidValues,
				lookup,
				required,
				Allcolumns,
				BreakLn,
				MaxLength,
				size,
				specialformatting,
				TDClass,
				TRClass,
				jsVerify,
				condition,
				<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#updateTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		from screendefinition
		where screenid =  <cf_queryparam value="#frmScreenid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

<CFSET frmScreenID = newScreenID>


</CFIF>
<cfinclude template="ScreenEdit.cfm">

<cfoutput>
<SCRIPT type="text/javascript">
	<!--
	parent.ScreensList#frameIdentifier#.location.href = '/admin/screens/screensList.cfm?frmScreenID=#frmScreenID#&frameIdentifier=#frameIdentifier#';
	//-->
</script>
</cfoutput>