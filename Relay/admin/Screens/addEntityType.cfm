<!--- �Relayware. All Rights Reserved 2014 --->

<!---
	NJH 2015/01/05	replaced schemaTable description with label
--->

<cfset xmlsource="">

<!--- add the entity type--->
<cfif structKeyExists(form,"save") and form.save eq "yes">
	<cf_transaction action="begin">
	<cftry>
		<cfset result = structNew()>
		<cfset result.isOk = true>
		<cfset result.message = "New entity type '#form.entityName#' added.">

		<cfquery name="checkSchemaTable" datasource="#application.SiteDataSource#">
			select uniqueKey from schemaTable where entityName = <cf_queryparam value="#form.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<!--- does schema table entry exist? --->
		<cfif checkSchemaTable.recordcount gt 0>
			<!--- Yes - check primary key is correct --->
			<cfif checkSchemaTable.uniqueKey eq "#form.entityName#ID">
				<!--- Yes - Modify other fields to make sure the entity is good for flags / screens etc. --->
				<cfquery name="checkSchemaTable" datasource="#application.SiteDataSource#">
					update schemaTableBase
					set flagsexist = 1, screensexist = 1, customEntity = 1
					<cfif structKeyExists(form,"label")>
						,description =  <cf_queryparam value="#form.label#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfif>
					where entityName = <cf_queryparam value="#form.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>
			<cfelse>
				<!--- No - Log error (entity already exists in incorrect format.  Choose another entity or contact DBA)--->
				<cfset result.isOk = false>
				<cfset result.message = "Entity already exists in Schema Table in incorrect format.  Choose another entity name.">
			</cfif>
		<cfelse>
			<!--- No - Create schemaTable entry --->
			<cfset startAtEntityTypeID = 1000>
			<cfquery name="getNewEntityID" datasource="#application.SiteDataSource#">
				select isNull(max(entityTypeID)+1,#startAtEntityTypeID#) as newEntityTypeID from schemaTable where entityTypeID >=  <cf_queryparam value="#startAtEntityTypeID#" CFSQLTYPE="cf_sql_integer" >
			</cfquery>

			<cfquery name="insertIntoSchemaTable" datasource="#application.SiteDataSource#">
				insert into schemaTableBase (entityTypeID,EntityName,description,flagsExist,screensExist,customEntity)
				values 	(<cf_queryparam value="#getNewEntityID.newEntityTypeID#" CFSQLTYPE="CF_SQL_Integer" >,
						<cf_queryparam value="#form.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						<cf_queryparam value="#form.label#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						1,1,1)
			</cfquery>
		</cfif>

		<cfquery name="checkTableDefinition" datasource="#application.SiteDataSource#">
			SELECT 1 FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME=<cf_queryparam value="#form.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfquery name="getForeignKeyID" datasource="#application.SiteDataSource#">
			SELECT uniqueKey, entityName from schematable where entityname = <cf_queryparam value="#form.relatedEntityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<!--- does table definition exist? --->
		<cfif checkTableDefinition.recordcount gt 0>
			<!--- Yes - Are all fields correct --->
			<cfquery name="checkTableColumns" datasource="#application.SiteDataSource#">
				SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME=<cf_queryparam value="#form.entityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset checkTableColumns = application.com.structureFunctions.queryToStruct(query=checkTableColumns, key="column_name")>
			<cfif 	structKeyExists(checkTableColumns,"#form.entityName#ID") and
					structKeyExists(checkTableColumns,"Created") and
					structKeyExists(checkTableColumns,"CreatedBy") and
					structKeyExists(checkTableColumns,"LastUpdated") and
					structKeyExists(checkTableColumns,"LastUpdatedBy") and
					structKeyExists(checkTableColumns,"lastUpdatedByPerson") and
					structKeyExists(checkTableColumns,"#getForeignKeyID.uniqueKey#")>
				<!--- Yes - Log error (Entity already exists and can be used) --->
				<cfset result.isOk = true>
				<cfset result.message = "Entity already exists in correct format.  Re-use this entity or create a new one if you are wanting to store a different set of data.">
			<cfelse>
				<!--- No - Log error (Entity exists and is incorrect format for use in this application. Choose a different entityName)--->
				<cfset result.isOk = false>
				<cfset result.message = "Entity already has a table definition that stores data in an incorrect format.  Choose another entity name.">
			</cfif>
		<cfelse>
			<!--- No - Create entity table --->
			<cfquery name="insertTableDefinition" datasource="#application.SiteDataSource#">
				CREATE TABLE [dbo].[#form.entityName#](
					[#form.entityName#ID] [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_#form.entityName#] PRIMARY KEY,
					[#getForeignKeyID.uniqueKey#] [int] NOT NULL CONSTRAINT [FK_#form.entityName#_#form.relatedEntityName#] FOREIGN KEY REFERENCES #form.relatedEntityName#(#getForeignKeyID.uniqueKey#),
					[CreatedBy] [int] NOT NULL,
					[Created] [datetime] NOT NULL CONSTRAINT [DF_#form.entityName#_created] DEFAULT (getdate()),
					[LastUpdatedBy] [int] NULL ,
					[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_#form.entityName#_lastUpdated] DEFAULT (getdate()),
					[lastUpdatedByPerson] [int] NULL,
					[createdByPerson] [int] NULL
				)

				exec generate_mrAudit @tablename='#form.entityName#'
			</cfquery>
		</cfif>

		<cfcatch>
			<cfset result.isOK = false>
			<cf_transaction action="rollback">

			<cfset errorID = application.com.errorHandler.recordRelayError_Warning(type="XML Editor PostSaveFunction",Severity="error",caughtError=cfcatch)>
			<cfset result.message = "Problem saving record. Refer an administrator to errorID #errorID#.">
		</cfcatch>
	</cftry>

	<cfif not result.isOK>
		<cfset errorMessage = result.message>
		<cfset result.isOK = false>
	<cfelse>
		<cf_transaction action="commit">
		<cfset application.com.applicationVariables.reLoadAllVariables()>
	</cfif>
	</cf_transaction>

	<cfset application.com.relayUI.setMessage(message=result.message,type=IIF(result.isOk,DE('success'),DE('error')))>

	<!--- <cfoutput>
		<script type="text/javascript">
		{
			window.location = '/admin/screens/addEntityType.cfm'
		}
		</script>
	</cfoutput> --->
</cfif>

<cfif structKeyExists(URL,"editor")>

	<script>
		function submitForm()  {
			form = document.editorForm;
			if (form.onsubmit()) {
				form.submit();
			}
		}
	</script>

	<!--- display the editor --->
	<CF_RelayNavMenu pageTitle="addEntityType" thisDir="">
		<CF_RelayNavMenuItem MenuItemText="Save and Return" CFTemplate="javascript:submitForm();">
	</CF_RelayNavMenu>

	<form action="?" method="POST" id="editorForm" name="editorForm">
		<cfset request.relayFormDisplayStyle="HTML-table">
		<cf_relayFormDisplay bindValidationWithListener="false" autoRefreshRequiredClass="true">

			<cf_relayformelementDisplay relayFormElementType="text" fieldName="entityName" label="phr_EntityTableName" currentValue="" required="true" size="40" maxlength="50" pattern = "^[A-Za-z_][A-Za-z0-9_]*$" validate="regex" message = "The table name must start with a letter and be followed only by letters, numbers or underscores.">
			<cf_relayformelementDisplay relayFormElementType="text" fieldName="label" label="phr_entityLabel" currentValue="" required="true" size="40" maxlength="50">

			<cfquery name="validValues" datasource="#application.SiteDataSource#">
				select entityname as datavalue, entityname as displayvalue from (
				select s.entityname from schemaTable s
				where s.entityname in ('Person','Location','Organisation','Country')
				union
				select s.entityname from schemaTable s
				join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME
				JOIN
				(SELECT
					fk.name,
					OBJECT_NAME(fk.parent_object_id) 'Parent table',
					c1.name 'Parent column',
					OBJECT_NAME(fk.referenced_object_id) 'Referencedtable',
					c2.name 'Referenced column'
				FROM
					sys.foreign_keys fk
				INNER JOIN
					sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
				INNER JOIN
					sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
				INNER JOIN
					sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK
				ON s.entityName = FK.[Parent table]
				where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null and s.customEntity = 1) as entities
			</cfquery>
			<cf_relayformelementDisplay relayFormElementType="select" fieldName="relatedEntityName" label="phr_relatedEntityName" CURRENTVALUE="Organisation" query="#validValues#">
			<CF_INPUT type="hidden" name="save" value="yes">

		</cf_relayFormDisplay>
	</form>
<cfelse>
	<!--- display the list --->
	<CF_RelayNavMenu pageTitle="addEntityType" thisDir="">
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="/admin/screens/addEntityType.cfm?editor=yes&add=yes">
	</CF_RelayNavMenu>

	<CFQUERY NAME="getEntities" DATASOURCE="#application.SiteDataSource#">
		select distinct s.entityTypeID,EntityName,label,UniqueKey,referencedtable
		from schemaTable s
		join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME
		JOIN
		(SELECT
			fk.name,
			OBJECT_NAME(fk.parent_object_id) 'Parent table',
			c1.name 'Parent column',
			OBJECT_NAME(fk.referenced_object_id) 'Referencedtable',
			c2.name 'Referenced column'
		FROM
			sys.foreign_keys fk
		INNER JOIN
			sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
		INNER JOIN
			sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
		INNER JOIN
			sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK
		ON s.entityName = FK.[Parent table] and isc.column_name = FK.[referenced column]
		where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null and s.customentity = 1
		order by referencedtable, s.entityName
	</CFQUERY>

	<cfoutput>
		#application.com.relayUI.message(message="You may not edit existing entity types.  Exercise caution when adding new entity types.  A DBA will be required to remove and update any incorrectly added Entities.",type="warning")#
	</cfoutput>

	<!--- <cfif structKeyExists(url,"isOK") and structKeyExists(url,"message")>
		<cfif url.isOK eq true>
			<cfset message = application.com.relayUI.message(message=url.message,type="success")>
		<cfelse>
			<cfset message = application.com.relayUI.message(message=url.message,type="error")>
		</cfif>
		<cfoutput>
			#message#
		</cfoutput>
	</cfif>
	<cfset application.com.relayUI.displayMessage()> --->

	<cf_tablefromqueryobject
		showTheseColumns="entityTypeID,EntityName,label,UniqueKey,referencedtable"
		keyColumnList=""
		keyColumnKeyList=""
		keyColumnOnClickList=""
		keyColumnURLList=""
		columnTranslation="true"
		columnTranslationPrefix="phr_report_customEntity_"
		queryObject="#getEntities#"
		GroupByColumns="referencedtable"
		numRowsPerPage="25"
		useInclude="false"
	>
</cfif>