<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 	screenEdit.cfm
		code to edit a screen
		called from screenAdminFrame.cfm
		expects following variables: 	frmScreenID
		 --->

<!--- Mods:

2001-08-12	SWJ		Modified the code to work in the frame

2007-11-28  WAB added rights to screens
2009-02-09 	NJH	P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
2011-05-31	NYBREL106 added screens to GetScreensentitytype query
2015-12-01	WAB PROD2015-290 Visibility Project - recordRights need to be attached to Screens not screen (screens being the entityname)
2015-12-17  SB Added bootstrap
2016-01-21	PROD2015-370 replace twoSelects with multi-select
2016-05-04	WAB BF-676 Knock on effect of PROD2016-878. Distinguish between entityNames and their labels (organiSation and organiZation).  db always stores UK English, display is US English
 --->

<CFSET methodlist = "edit,view">
<CFSET preIncludeList = "..\screen\persondropdown.cfm#application.delim1#Drop down of person functions (used with list of people on screen)">


<CFPARAM name="basicUser" default=false>

<CFQUERY NAME="getSecurityTasks" datasource="#application.siteDataSource#">
	SELECT DISTINCT c.shortname  <!--- 12 Aug SWJ added DISTINCT because query was returning duplicate records --->
	FROM Rights AS r, RightsGroup AS rg, SecurityType AS c
	WHERE r.SecurityTypeID = c.SecurityTypeID
	AND r.UserGroupID = rg.UserGroupID
	AND rg.PersonID = #request.relayCurrentUser.personid#
</CFQUERY>

<!--- NYB 2011-05-31 REL106 added screens to query --->
<CFQUERY NAME="GetScreensentitytype" datasource="#application.siteDataSource#">
	SELECT DISTINCT description, tablename as entityName FROM flagentitytype
	union
	SELECT DISTINCT  st.label as description, st.entityName from screens
	join schematable st on screens.entitytypeID = st.entitytypeID
	where screens.entitytype is not null and len(screens.entitytype) > 1
	ORDER BY tableName
</CFQUERY>

<CFQUERY NAME="getScopes" datasource="#application.siteDataSource#">
	SELECT countryid, countrydescription,
	CASE WHEN isnull(isocode,'') = '' THEN 1 ELSE 2 END as sortorder
	from country

	UNION

	Select 0  , 'All countries', 0 from dual

	order by sortorder, countrydescription
</CFQUERY>

<cfquery name="qry_get_ScreenOrganisationTypes" datasource="#application.siteDataSource#">
	SELECT ot.organisationTypeID as datavalue,'phr_sys_'+TypeTextID as displayvalue, case when sot.screenid is not null then 1 else 0 end as isSelected
	FROM
		organisationType ot
			LEFT JOIN
		screenOrganisationType sot ON screenID =  <cf_queryparam value="#frmscreenid#" CFSQLTYPE="CF_SQL_INTEGER" > AND ot.organisationTypeID = sot.OrganisationTypeID
	WHERE
		ot.Active=1

	ORDER BY
		sortIndex
</cfquery>


<CFIF frmscreenid is not -1>
	<CFQUERY NAME="GetScreen" datasource="#application.siteDataSource#">
	SELECT * from Screens
	where screenid =  <cf_queryparam value="#frmScreenid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
<CFELSE>
	<CFQUERY NAME="GetScreen" datasource="#application.siteDataSource#">
	SELECT 'newScreen' as ScreenID,
		'' as ScreenTextID,
		'' as ScreenName,
		1 as active,
		1 as suite,
		'' as securityTask,
		'Person' as entitytype,
		0 as sortorder,
		0 as scope,
		'showScreen' as viewer,
		'edit' viewedit,
		'' as preinclude,
		'' as postinclude,
		'' as parameters,
		'' as tablewidth,
		0 as predefined
		from dual
	</CFQUERY>
</cfif>

<cfset screenVersion = getScreen.viewer eq "formRenderer"?2:1>


<cf_title>Edit Screen Header</cf_title>
<!--- <cf_head>
	<cf_title>Edit Screen Header</cf_title> --->

	<cfsavecontent variable="headerScript">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<SCRIPT>
	function verifyForm ()  {
		form = document.screenEdit
		msg = ''

		if (form.frmScreenName.value == '')  {

			msg = msg + 'You must enter a name for your screen'

		}

		if (msg == '')  {
			return true

		}else {

			alert (msg)
			return false
		}


	}

	function submitTheForm ()  {

		submitForm();
	}

	function copyScreen ()  {
		form = document.screenEdit
		form.frmCopyScreen.value = 1

	}

	function deleteScreen ()  {

		form = document.screenEdit

			if (confirm("Are you sure you want to permanently this screen " )) {
						form.frmDeleteScreen.value = 1
	                	form.submit();
	                	return;
					}

	}

	</script>
</cfsavecontent>

<cfhtmlhead text="#headerScript#">
<CF_RelayNavMenu pageTitle="Edit Screen #frmScreenid#" thisDir="screens">
	<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javascript:if (verifyForm ()) {submitTheForm()}">
	<CF_RelayNavMenuItem MenuItemText="Delete" CFTemplate="javascript:if (verifyForm ()) {deleteScreen()}">
	<CF_RelayNavMenuItem MenuItemText="Save and create new screen based on this screen" CFTemplate="javascript:if (verifyForm ()) {copyScreen(); submitTheForm()}">
</CF_RelayNavMenu>

<!--- NJH 2009-02-09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect --->
<CFFORM ACTION="screenEditTask.cfm?frameIdentifier=#frameIdentifier#" METHOD="POST"  NAME="screenEdit">

<div id="screenEditCont">

<!--- Start the form here --->
<CFOUTPUT >
<CF_INPUT type="hidden" name="frmScreenID" value="#frmScreenID#">
<input type="hidden" name="frmCopyScreen" value="">
<input type="hidden" name="frmDeleteScreen" value="">

	<div class="form-group">
    	<label>Screen Id</label>
		<CFIF frmScreenid is "-1">New Screen<CFELSE> #htmleditformat(getScreen.ScreenID)# </cfif>
    </div>
	<CFIF basicUser is false>
	<div class="form-group">
    	<label>Screen Unique Text ID</label>
		<!--- PJP CASE 430569: Changed Char Type to accept underscores --->
		<CF_relayValidatedField
			fieldName="frmScreenTextID"
			currentValue="#trim(getScreen.ScreenTextID)#"
			charType="variableName"
			size="50"
			maxLength="50"
			helpText="Alpha characters only. No spaces."
		>

		<!--- <INPUT type="text" name="frmScreenTextID" value="#trim(getScreen.ScreenTextID)#" maxlength="50" size = "50"> --->
    </div>
	<CFELSE>
		<CF_INPUT type="hidden" name="frmScreenTextID" value="#trim(getScreen.ScreenTextID)#">
	</cfif>
	<div class="form-group">
    	<label>Screen Full Name</label>
		<CF_INPUT type="text" name="frmScreenName" value="#trim(getScreen.ScreenName)#"maxlength="50" size = "50">
    </div>

	<div class="form-group">
		<label>Sort Order</label>
			<select name="frmSortOrder" class="form-control">
				<cfloop index="thisItem" from = "0" to = "100">
					<option value="#thisItem#" <CFIF #thisItem# is getScreen.sortorder>SELECTED</cfif>>#htmleditformat(thisItem)#
				</cfloop>
			</select>
	</div>

	<div class="checkbox">
		<label>
			Active
			<INPUT type="checkbox" name="frmActive" value="1" <CFIF getScreen.active is 1>CHECKED</cfif>>
		</label>
	</div>

	<div class="checkbox">
    	<label>
			Show in Drop Downs
			<INPUT type="checkbox" name="frmSuite" value="1" <CFIF getScreen.suite is 1>CHECKED</cfif>>
		</label>
    </div>

	<div class="form-group">
		<label>Entity Type</label>
		<select name="frmEntityType" size="1" class="form-control">
				<option value="" >Select a type
			<cfloop query="GetScreensentitytype">
				<option value="#entityName#" <CFIF entityName is getScreen.entitytype>SELECTED</cfif>>#htmleditformat(Description)#
			</cfloop>
		</select>
	</div>

	<cfif listfindNoCase("person,location,organisation",getScreen.entitytype) neq 0>
		<cfif  qry_get_ScreenOrganisationTypes.recordcount neq 0>
		<div class="form-group">
			<label>Organisation Type(s)</label>
				<cf_select name = "Selected_OrganisationTypeID" query="#qry_get_ScreenOrganisationTypes#" size = 3 displayas="multiselect">

				<span class="help-block">
					<p>Only required for Person, Organisation, Location Screens</p>
				</span>
		</div>
		<cfelse>
			<input type="hidden" name="Selected_OrganisationTypeID" id="Selected_OrganisationTypeID" value="">
		</cfif>
	</cfif>

	<div class="form-group">
		<label>Country Scope</label>
		<select name="frmScope" size="1" class="form-control">
			<cfloop query="getScopes">
				<option value="#countryid#" <CFIF countryid is getScreen.scope>SELECTED</cfif>>#htmleditformat(CountryDescription)#
			</cfloop>
		</select>
	</div>
	<div class="form-group">
		<label>Security for this screen</label>
		<select name="frmSecurityTask" size="1" class="form-control">
			<option value="">Select a Value
			<cfloop query="getSecurityTasks">
				<option value="#shortname#" <CFIF shortname is getScreen.securityTask>SELECTED</cfif>>#htmleditformat(shortName)#
			</cfloop>
		</select>
	</div>

<cfif screenVersion is 1>
	<div class="form-group">
		<label>Method</label>
		<select name="frmMethod" class="form-control">
			<cfset displayList = 'Editable,Read only'>
			<cfset valList = 'edit,view'>
			<cfloop index="i" from="1" to="#listlen(displayList,',')#">
				<option value="#listgetat(valList,i,",")#" <CFIF listgetat(valList,i,",") is getScreen.viewedit>SELECTED</cfif>>#htmleditformat(listgetat(displayList,i,","))#
			</cfloop>
		</select>
	</div>
<cfelse>
	<div class="form-group">
		<label>Read Only</label>
				<cf_displayValidValues formfieldname="frmMethod"
					displayas = "radio"
					validvalues ="edit�No,view�Yes"
					currentvalue = "#getScreen.viewedit#"
					onChange = "onMethodChange (this)"
				>
				Or enter an expression to evaluate to true or false
				<CF_INPUT class="form-control" type="text" name="frmMethod" id="frmMethod_Expression" value="#(listfindnocase("edit,view",getScreen.viewedit)?'':getScreen.viewedit)#" maxlength="250" size = "20" >
				<!--- Script to clear the radios if an expression is entered and viceVersa --->
				<script>
					onMethodChange = function (obj) {
						if (obj.type == 'radio' && obj.checked == true) {
							var expressionField = $ (obj.name + '_Expression')
							expressionField.value = ''
						} else if (obj.type == 'text' && obj.value != '') {
							var radios = document.getElementsByName('frmMethod');
							for (var i = 0; i< radios.length; i++) {
								console.log (radios[i]);
								if (radios[i].type == 'radio') {
									radios[i].checked = false
								}

							}
						}

					}
				</script>


	</div>
</cfif>

	<CFIF basicUser is false>
	<!---<div class="form-group">
		<label>Viewer</label>

		<cfset itemSelected = false>
		<select name="frmViewer" class="form-control">
			<option value="" <CFIF getScreen.viewer eq "">SELECTED</cfif>>None selected
			<cfloop index="thisItem" list="ShowScreen#application.delim1#Screen,">
				<option value="#thisItem#" <CFIF thisItem is getScreen.viewer>SELECTED<cfset itemSelected = true></cfif>>#thisItem#
			</cfloop>
		</select>
	</div>--->
	<div class="form-group">
		<label>Viewer</label>

		<cf_displayValidValues formfieldname="frmViewer"
					displayas = "radio"
					validvalues ="showScreen#application.delim1#Screens Version 1,#(not getScreen.viewer eq 'showScreen'?getScreen.viewer:'other')##application.delim1#Or enter a file name below"
					currentvalue = "#getScreen.viewer#"
					onChange = "onViewerChange (this)"
				>
		<cf_input class="form-control" type="text" name = "frmViewer" id="frmViewer_Other" value="#(not getScreen.viewer eq 'showScreen'?getScreen.viewer:'')#">

		<script>
			onViewerChange = function (obj,textIDSuffix) {
				if (obj.type == 'radio' && obj.checked == true && obj.id == 'frmViewer_showScreen') {
					var expressionField = $ (obj.name + '_Other')
					expressionField.value = ''
				} else if (obj.type == 'text' && obj.value != '') {
					var radios = document.getElementsByName(obj.name.replace(textIDSuffix,''));
					for (var i = 0; i< radios.length; i++) {
						if (radios[i].type == 'radio') {
							if (radios[i].id == 'frmViewer_showScreen') {
								radios[i].checked = false;
							} else {
								radios[i].checked = true;
							}
						}

					}
				}

			}
		</script>

    </div>
	<CFELSE>
		<CF_INPUT type="Hidden" name="frmViewer" value="#getScreen.viewer#">
	</CFIF>
	<CFIF basicUser is false>
	<div class="form-group">
		<label>Files to preinclude</label>
		<select name="frmPreInclude" Multiple length = "3" class="form-control">
			<option value="">None
			<CFSET fieldValue = getScreen.preInclude>
			<cfloop index="thisItem" list="#preIncludeList#">
				<CFSET findThisItem = listfindNoCase(fieldValue,listFirst(thisItem,"#application.delim1#"))>
				<option value="#listFirst(thisItem,"#application.delim1#")#" <CFIF  findThisItem is not 0><CFSET fieldValue = listDeleteAt(fieldValue,findthisItem)> SELECTED</cfif>>#listlast(thisItem,"#application.delim1#")#
			</cfloop>
		</select><BR><CF_INPUT type="text" name= "frmPreInclude" value = "#fieldValue#" size = "55" maxlength="255">
	</div>
	<div class="form-group">
		<label>Files to postinclude</label>
		<CF_INPUT class="form-control" type="text" name= "frmPostInclude" value = "#getScreen.postInclude#" size = "55" maxlength="255">
	</div>

	<CFELSE>
		<CF_INPUT type="hidden" name= "frmPreInclude" value = "#getScreen.preInclude#">
		<CF_INPUT type="hidden" name= "frmPostInclude" value = "#getScreen.postInclude#">
	</CFIF>
	<div class="form-group">
		<label>Rights to use this Screen<BR>Leave blank for all users</label>
		<CF_UGRecordManager class="form-control" entity="screens" entityid="#getScreen.screenid#" Form="screenEdit" level = "1" method = "edit">
    </div>

	<div class="form-group">
	   	<label>Rights to Edit this screen<BR>(By default editable by creator and administrators)</label>
		<CF_UGRecordManager class="form-control" entity="screens" entityid="#getScreen.screenid#" Form="screenEdit" level = "4" method = "edit">
    </div>

	<CFIF basicUser is false>
	<div class="form-group">
    	<label>Additional Parameters</label>
			<table id="undocumentedParamsTable">
			<cfset parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(getScreen.parameters,",")>
			<cf_addUndocumentedParametersToTable
					parameterStructure = #parametersStruct#
					tableID = "undocumentedParamsTable"
					parameterSuffix = "parameter"
					hideParameter = "partnerPack"
					LabelText = ""
				>
			</table>
		<!--- <CF_INPUT type="text" name="frmParameters" value="#getScreen.Parameters#" maxlength="255" size = "55"> ---></td>
    </div>
	<CFELSE>
		<CF_INPUT type="hidden" name="frmParameters" value="#getScreen.Parameters#">
	</CFIF>
	<div class="form-group">
    	<label>Table Width</label>
		<CF_INPUT type="text" name="frmTableWidth" value="#getScreen.TableWidth#" maxlength="5" size = "5">
    </div>
	<CFIF basicUser is false>
	<div class="checkbox">
		<label>Save as Predefined Item (Snippet)
			<INPUT type="checkbox" name="frmPredefined" value="1" <CFIF getScreen.predefined is 1>CHECKED</cfif>>
		</label>
	</div>
	<CFELSE>
		<CF_INPUT type="hidden" name="frmPredefined" value="#getScreen.predefined#">
	</CFIF>

</cfoutput>
</div>
</cfform>