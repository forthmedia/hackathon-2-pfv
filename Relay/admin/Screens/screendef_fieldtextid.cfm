<!--- �Relayware. All Rights Reserved 2014 --->
<!---

screendef_fieldTextID.cfm


Purpose:  This file is included in both screenDefEdit.cfm and screenDefListEdit.cfm
		it outputs the fieldtextID in a dropdown

WAB  2001-06-15    Added some HTMLEditFormat s


 --->



<CFOUTPUT>
	<input type="hidden" name="frmFieldSource_#itemid#" class="form-control" value="#fieldsource#"> <!--- Make the dynamic options update happy --->
	<CFIF findNoCase (fieldsource,fieldSourcesWithFreeText) IS 0>
		<select name="frmFieldTextID_#itemid#" class="form-control">
			<CFSET currentFieldSource = fieldSource>
			<CFSET currentFieldTextID = fieldTextID>
			<CFSET matchFound = false>
			<OPTION value="">&nbsp;
			<CFLOOP query = "getFieldSources">
				<CFIF fieldSource IS  currentFieldSource>
					<OPTION value="#fieldtextid#" <CFIF fieldTextId is currentFieldTextID>SELECTED<CFSET matchFound = true></cfif>>#fieldDescription#
				</cfif>
			</cfloop>
		</SELECT>
			<CFIF not matchfound and screenVersion is 1>
				<!--- for some reason, this one is not found, so display it anyway --->
				<CF_INPUT type = "Text" Name = "frmFieldTextID_#itemid#" Value="#fieldTextID#" size = "50" maxlength = "1000">
				<p>this value does not appear to be valid</p>
			</CFIF>

	<cfelse>
		<CF_INPUT type = "Text" Name = "frmFieldTextID_#itemid#" Value="#fieldTextID#" size = "50" maxlength = "1000">

	</cfif>


<cfif screenVersion is 2>
	<cf_includejavascriptonce template = "js/evalStringUIUpdates.js">
</cfif>

</cfoutput>
<CFSET fieldsOnPage = listappend(fieldsOnPage ,"fieldTextID")>