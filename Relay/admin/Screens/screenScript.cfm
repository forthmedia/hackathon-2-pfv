<!--- �Relayware. All Rights Reserved 2014 --->
<!---

A rough and ready page to generate a SQL script of a screen for transferring to another site


WAB July 11th 2006

2007-06-06 added @screenid
2015-12-17 SB Bootstrap

--->

<cfparam name = "frmScreenID" type="numeric">

<!--- 2016-05-06 WAB BF-633 Fix problem caused by adding CF_queryParam, doesn't work inside a string, so set useCFVersion=false so that we get type checking without parameter binding --->
<CFQUERY name="script1" datasource="#application.SiteDataSource#" >
 sp_generate_inserts 'screens', @From = 'from screens where screenid =  <cf_queryparam value="#frmscreenid#" CFSQLTYPE="cf_sql_integer" useCFVersion=false>', @ommit_identity = 1
</CFQUERY>
<CFQUERY name="script2" datasource="#application.SiteDataSource#" >
 sp_generate_inserts 'screendefinition', @From = 'from screendefinition where screenid =  <cf_queryparam value="#frmscreenid#" CFSQLTYPE="cf_sql_integer" useCFVersion=false>', @ommit_identity = 1
</CFQUERY>


<cfoutput>
<div id="screenScriptCont">
	<div class="form-group">
		<label>SQL Script For Screen Table:</label>
		<!--- 'scuse nasty layout - don't want blanks at beginning of lines in text area --->
		<textarea cols="80" rows="10" class="form-control">
			declare @screenid int
			select @screenid = #frmscreenid#
			--DELETE from screen where screenid = #frmscreenid#

			<cfloop query="script1">
			<!--- put in some carriageReturns --->
			<cfset script = replace(replace(COMPUTED_COLUMN_1,"VALUES","#chr(10)#VALUES"),"(","#chr(10)#(")>
			<cfset script = replace (script,"(#frmscreenid#,","(@screenid,")>
			#script#

			</cfloop>
		</textarea>
	</div>
	<div class="form-group">
		<label>SQL Script For ScreenDefinition Table:</label>

		<textarea cols="80" rows="40" class="form-control">
			declare @screenid int
			select @screenid = #frmscreenid#

			--DELETE from screenDefintion where screenid = #frmscreenid#

			<cfloop query="script2">
			<cfset script = replace(replace(COMPUTED_COLUMN_1,"VALUES","#chr(10)#VALUES"),"(","#chr(10)#(")>
			<cfset script = replace (script,"(#frmscreenid#,","(@screenid,")>
			#script#

			</cfloop>
		</textarea>
	</div>
</div>
</cfoutput>

