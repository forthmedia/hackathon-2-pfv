<!--- �Relayware. All Rights Reserved 2014 --->
<!-- frames -->

<!--- 	screenAdminFrame.cfm
		code to manage the screen admin screens
		expects following variables: 	frmScreenID
		 --->

<!--- Mods:

2001-08-12	SWJ		Modified the code to work in the frame

 --->		 



<cfif isDefined("frmScreenTextID")>
	<CFQUERY NAME="getMinScreenID" datasource="#application.siteDataSource#">
		select screenid from screens where screenTextID =  <cf_queryparam value="#frmScreenTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
	<cfset frmScreenID = getMinScreenID.screenid>
</cfif> 
 
<CFIF isdefined("frmScreenID")>
	<CFPARAM NAME="frmScreenID" DEFAULT="frmScreenID">
<CFELSE>
	<CFPARAM NAME="frmScreenID" DEFAULT="0">
</CFIF>

<cfset frameIdentifier = replace(rand(),".","","ALL")>
<CFOUTPUT>
<FRAMESET COLS="45%,*" TITLE="Screen Admin">
    <FRAME NAME="ScreensList#frameIdentifier#" SRC="ScreensList.cfm?frmScreenID=#frmScreenID#&frameIdentifier=#frameIdentifier#" MARGINWIDTH="10" MARGINHEIGHT="10" SCROLLING="auto" FRAMEBORDER="1">
	<FRAME NAME="ScreenEdit#frameIdentifier#" SRC="../../templates/blank.cfm" MARGINWIDTH="10" MARGINHEIGHT="10" SCROLLING="auto" FRAMEBORDER="1">
</FRAMESET>
</CFOUTPUT>

