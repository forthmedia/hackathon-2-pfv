<!--- �Relayware. All Rights Reserved 2014 --->

<!---
23-DEC-2010		MS		LID:4982 - Create New Workflow with user entered providerid
18-Dev-2015		SB		Bootstrap
--->
<cfparam name="frm_processID" default="">

<!--- Update any values that were changed or added --->
<CFIF IsDefined("insertRec") and insertRec eq "yes">
	<!--- WAB modifed so can manually assign a number - useful for moving code between servers --->
	<cftransaction>
		<cfquery name="getProcessID" datasource="#application.SiteDataSource#">
		<cfif isDefined("processid") and processid is not "">
			<!--- LID:4982:checking if the processid entered by the user exists --->
			<cfquery name="processIDexists" datasource="#application.SiteDataSource#">
				select processid
				from processHeader
				where	processid =  <cf_queryparam value="#form.processid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<!--- LID:4982:if the processid does not exist add the record--->
			<cfif processIDexists.recordcount eq 0>
				set identity_Insert processHeader On
				insert into processHeader (processid,processDescription)
				values (#htmleditformat(form.processid)#,'#htmleditformat(form.processdescription)#')
				set identity_Insert processHeader Off
			<cfelse>
				<!--- LID:4982:send the user back with an error msg that processid already exists in the DB --->
				<cflocation url="addProcessActions.cfm?processidConflict=true" addtoken="No">
			</cfif>

		<cfelse>
		insert into processHeader (processDescription)
		values ('#htmleditformat(form.processdescription)#')
		</cfif>
		select scope_identity() as processid
		</cfquery>
	</cftransaction>

	<cflocation url="listprocessActions.cfm?frmprocessid=#getProcessID.processID#" addtoken="No">

</CFIF>


<cf_title>Edit Relay Process Actions</cf_title>
<SCRIPT type="text/javascript">
<!--

function save(){
	form = document.mainForm;
	form.insertRec.value = "yes";
	form.submit();
	}
//-->
</script>



<cf_RelayNavMenu pageTitle="Add Workflow" thisDir="admin/processActions">
	<cf_RelayNavMenuItem MenuItemText="phr_sys_save" CFTemplate="javaScript:save();">
</cf_RelayNavMenu>

<cfform action="addProcessActions.cfm" method="POST" enablecab="Yes" name="mainForm" preservedata="Yes">
	<input type="hidden" name="insertRec" value="no">


	<CFIF IsDefined("URL.processidConflict")>
	<div class="errorblock">
		Error: Process ID entered already exists on the system. Please choose a new Process ID

	</div>
	</CFIF>
<div class="grey-box-top">
	<div class="row">
		<div class="form-group">
			<div class="col-xs-12 col-sm-6">
				<label>ID of new workflow (leave blank for automatic assignment of an ID)</label>
				<cfinput type="Text" name="processID"  required="No" size="4" maxlength="4" class="form-control">
			</div>
			<div class="col-xs-12 col-sm-6">
				<label>Descriptive name of the new workflow</label>
				<cfinput type="Text" name="processDescription" message="You must set a Name" required="Yes" size="80" maxlength="250" class="form-control">
			</div>
		</div>
	</div>
</div>
</CFFORM>