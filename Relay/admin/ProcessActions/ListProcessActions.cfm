<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2006-06-07  WAB added editing header
				modified layout (indents) - handling else and blank in expression
				modified adding new step/sequences
2009-11-06	NJH	LID 2147 - pass frmProcessID through on the url
2015-11-27  ACPK    PROD2015-325 Removed duplicate ProcessID variable which was appearing next to select menu
2015-12-18	SB		Bootstrap
--->

<cfif isDefined('URL.StartRow') OR isDefined('Form.StartRow')>
	<cfif isDefined('URL.StartRow')>
		<cfset StartRow=URL.StartRow>
	<cfelse>
		<cfset StartRow=Form.StartRow>
	</cfif>
<cfelse>
	<cfset StartRow=1>
</cfif>
<cfparam name="frmProcessID" type="numeric" default="0">
<cfif frmProcessID neq 0>
	<cfif isdefined("saveHeader")>

			<!--- collect all the parameters together into single field --->
			<cfparam name="form.parameters" default= "">
			<cfset form.parameters = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldPrefix="Parameter",appendTo=form.parameters)>

		<CFQUERY name="updateHeader" datasource="#application.SiteDataSource#" >
			update processHEader
			set processdescription =  <cf_queryparam value="#frmprocessDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			parameters =  <cf_queryparam value="#parameters#" CFSQLTYPE="CF_SQL_VARCHAR" >
			where processid =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

	<cfelseif isDefined("deleteProcess")>
		<cftransaction>
			<CFQUERY name="updateHeader" datasource="#application.SiteDataSource#" >
			delete from processActions where processid =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
			delete from processHeader where processid =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
		</cftransaction>

	</cfif>


	<!--- only call this query to get the  --->
	<CFQUERY name="GetHeader" datasource="#application.SiteDataSource#" >
	select * from processHEader where processid =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFQUERY name="GetRecords" datasource="#application.SiteDataSource#" >
		SELECT pa.*, ja.jumpActionDesc
		FROM processActions pa
		left JOIN processJumpAction ja on pa.jumpAction = ja.jumpAction
		where pa.processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY pa.stepID, pa.sequenceID
	</CFQUERY>

	<cfset PageName="ListProcessActions.cfm">
	<cfset TotalRecords=GetRecords.RecordCount>
	<cfset RecordsPerPage=25>
	<cfset NumberOfPages=Ceiling(TotalRecords/RecordsPerPage)>
	<cfset CurrentPageNumber=Ceiling(StartRow/RecordsPerPage)>
	<cfset EndRow=StartRow+RecordsPerPage-1>

	<cfif GetRecords.recordcount gt 0>
		<cfquery name="getMaxStepID" dbtype="query">
			select max(stepid) as maxStepID from getRecords
		</cfquery>
		<cfset nextStepID = getMaxStepID.maxStepID+10>
	<cfelse>
		<cfset nextStepID = 10>
	</cfif>
</cfif>

<cf_title>List Workflow Processes</cf_title>

<cf_includejavascriptonce template = "/javascript/openWin.js">
<SCRIPT type="text/javascript">
<!--

function testProc(processID,stepID){
	openWin('../../screen/redirector.cfm?frmStepID='+stepID+'&frmProcessID='+processID,'TestProc','width=650,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
	}

function deleteProcessConfirm () {
	return confirm ('Are you sure that you want to delete this process and all its steps')

}
//-->
</script>


<cfinclude template="Header.cfm">

<div class="grey-box-top">
	<cfif isDefined('frmProcessID')>

	</cfif>
	<!--- Get all defined processes --->
	<CFQUERY NAME="GetProcesses" datasource="#application.siteDataSource#">
		select processID, processDescription from ProcessHeader
	</CFQUERY>
	<div class="row">
		<FORM ACTION="listProcessActions.cfm" METHOD="post" name="procActionForm">
			<div class="form-group">
				<div class="col-xs-6">
					<cfif frmProcessID is not 0 and GetHeader.recordcount neq 0><label>Choose workflow process:</label><cfelse><label>Workflow Process ID:</label></cfif>
					<SELECT NAME="frmProcessID" onChange="form.submit()" class="form-control">
						<cfoutput query="getProcesses">
						<OPTION VALUE="#ProcessID#" #IIF(frmProcessID IS ProcessID,DE('SELECTED'),DE(''))#>#htmleditformat(processDescription)# (#htmleditformat(processID)#)</option>
						</cfoutput>
					</SELECT>
				</div>
				<div class="col-xs-2">
					<input type="submit" value="Go" class="btn btn-primary form-control margin-top-25"> <!--- 2015-11-27  ACPK    PROD2015-325 Removed duplicate ProcessID variable which was appearing next to select menu --->
				</div>
			</div>
		</form>
	</div>
</div>



<cfif frmProcessID neq 0 and GetHeader.recordcount neq 0>
	<cfoutput>
	<form method=post>
		<CF_INPUT type="hidden" name="frmProcessid" value="#frmProcessID#">
	<table class="table table-hover table-striped">
		<tr>
			<th colspan="2">Header</th>
		</tr>
		<tr>
			<td>
				<label for="frmprocessDescription">Name of Workflow</label>
			</td>
			<td>
				<CF_INPUT name="frmprocessDescription" id="frmprocessDescription" Type="text" value="#getHeader.processDescription#" size = 100>
			</td>
		</tr>
			<cfset parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(GetHeader.parameters,",")>

			<cfif structcount(parametersStruct)>
				<tr>
					<td><label>Defined Parameters</td>
					<td>
					<cfloop item="item" collection=#parametersStruct#>
						<cfset value = parametersStruct[item]>
						<tr>
							<td>
								<label>#htmleditformat(item)#</label>
							</td>
							<td>
								<CF_INPUT name="parameter_#item#" type=text value = "#value#" size = 50>
							</td>
						</tr>
					</cfloop>
				</cfif>

				<tr>
					<td>
						<label for="parameters">
							Add Additional Parameters <BR>(enter as name value pairs i.e. variable=value,variable=value)
						</label>
					</td>
					<td>
						<input class="form-control" name="parameters" id="parameters" type=text value = "" size = 50>
					</td>
				</tr>



				<tr>
					<td></td>
					<td>
						<input name="saveHeader" type=submit value = "Update" class="btn btn-primary">
						<!--- SWJ: 2006-11-12 Added higher security to Script Screens --->
						<cfif application.com.login.checkInternalPermissions ("AdminTask", "Level3")>
						<input name="deleteProcess" type=submit value = "Delete" onclick="return deleteProcessConfirm()" class="btn btn-primary">
						<input name="scriptProcess" type=submit value = "Create SQL Script"  class="btn btn-primary">
						</cfif>
					</td>
				</tr>

				<cfif isDefined("scriptProcess")>
					<CFQUERY name="script1" datasource="#application.SiteDataSource#" >
					 sp_generate_inserts 'processHeader', @From = "from processheader where processid  =  <cf_queryparam value="#frmProcessid#" CFSQLTYPE="cf_sql_integer" > "
					</CFQUERY>

					<CFQUERY name="script2" datasource="#application.SiteDataSource#" >
					 sp_generate_inserts 'processActions', @From = "from processActions where processid  =  <cf_queryparam value="#frmProcessid#" CFSQLTYPE="cf_sql_integer" > "
					</CFQUERY>
					<tr><td>
						SQL Script:<BR>
						<textarea cols="160" rows="10" class="form-control">-- header
<cfloop query="script1">#COMPUTED_COLUMN_1#

</cfloop>-- details
<cfloop query="script2">#COMPUTED_COLUMN_1#

</cfloop>
						</textarea>
					</td></tr>
				</cfif>

	</table>
	</form>
	</cfoutput>




<cfinclude template="listProcessActions_Insert.cfm">

	<!--- NJH 2009/06/11 LID 2147 - pass frmProcessID through on the url --->
	<cfif NumberOfPages gt 1>
		<cfoutput>
		<table class="table table-hover table-striped">
		<tr>
	    	<td>
				<cfif CurrentPageNumber gt 1>
					<cfset NextStartRow=Variables.StartRow - RecordsPerPage>
					<a href="#PageName#?StartRow=#Variables.NextStartRow#&frmProcessID=#frmProcessID#">Previous Page</a>
				</cfif>
			</td>
	    	<td>
				<cfif CurrentPageNumber lt NumberOfPages>
					<cfset NextStartRow=Variables.StartRow + RecordsPerPage>
					<a href="#PageName#?StartRow=#Variables.NextStartRow#&frmProcessID=#frmProcessID#">Next Page</a>
				</cfif>
			</td>
		</tr>
		<tr>
	    	<td align="center" colspan="2">
				<cfloop index="Counter" from="1" to="#Variables.NumberOfPages#" step="1">
					<cfset NextStartRow=(Counter*Variables.RecordsPerPage)-(Variables.RecordsPerPage-1)>
					&nbsp;
					<cfif Counter eq CurrentPageNumber>
						#Counter#
					<cfelse>
						<a href="#PageName#?StartRow=#Variables.NextStartRow#&frmProcessID=#frmProcessID#">#htmleditformat(Counter)#</a>
					</cfif>
				</cfloop>
			</td>
		</tr>
	</table>
	</cfoutput>
	</cfif>


</cfif>