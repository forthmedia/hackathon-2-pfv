<!--- �Relayware. All Rights Reserved 2014 --->


<!---

a subroutine for ListProcessActions which actually outputs the list of steps
requires query getRecords

taken out of main code WAB JUNE 2006

added ability to just show a single step with the others collapsed. Probably ought to be done with expanding/collapsing divs, but actually for the time being I just output what I need
2015-12-18 SB Bootstrap
--->

<cfparam name ="getRecords" >
<cfparam name ="startRow" default="1">
<cfparam name ="recordsPerPage" default="#max(1,getrecords.recordCount)#">
<cfparam name ="onlyExpandStepID" default="">

<SCRIPT type="text/javascript">
<!--
function testProc(processID,stepID){
	openWin('../../screen/redirector.cfm?frmStepID='+stepID+'&frmProcessID='+processID,'TestProc','width=650,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
	}

function addSequence (stepID) {
	form = document.dummy;
	seqID = form['addSeq_'+stepID].value
	doAction('addstep',stepID,seqID)
}

function addStep () {
	form = document.dummy;
	stepID = form.addStep.value
	doAction('addstep',stepID,10)
}

function doAction(action,stepID,seqID){
	form = document.editForm;
	form.frmAction.value = action;
	form.frmStepID.value = stepID;
	form.frmSequenceID.value = seqID;
	form.submit();
	}

//-->
</script>

	<form name = "dummy">
	<table class="table table-hover table-striped">
		<TR>
			<TH colspan="3">Description</TH>
		</TR>
			<cfset previousJumpExpression = "true">
			 <cfset indent = false>
		<cfoutput query="GetRecords" group="stepID" startrow=#Variables.StartRow# maxrows=#Variables.RecordsPerPage#>

			<tr bgcolor="C0C0C0">
				<td colspan="3">
					<strong>Workflow step: #htmleditformat(stepid)# </strong>
				<cfif not (onlyexpandstepid is "" or onlyExpandStepID is stepid)>
							<a href="javaScript:doAction('editSequence',#StepID#,'')" class="smallLink">Edit</a>
						</td>
					</tr>
				<cfelse>

							<!--- NJH 2011/04/18 LID 6324
							Remove this link as it can't really be tested on it's own. Yan was ok with this.
							| <a href="javaScript:testProc(#frmProcessID#,#stepID#)">Click to test this step</a> --->
						</td>
					</tr>
					<tr>
						<td bgcolor="D3D3D3"><strong>Step Sequence</strong></td>
						<td bgcolor="D3D3D3"><strong>Step Actions</strong> (What happens on each step)</td>
						<td bgcolor="D3D3D3"><strong>Step Text ID</strong></td>
					</tr>
					<cfoutput>
					<cfif currentRow lt GetRecords.recordCount>
						<cfset nextJumpExpression = GetRecords.jumpexpression[currentRow+1]>
					<cfelse>
						<cfset nextJumpExpression = "true">
					</cfif>
					<TR>
						<td>&nbsp;&nbsp;&nbsp;<a href="javaScript:doAction('editSequence',#StepID#,#SequenceID#)" class="smallLink">
							<strong>#htmleditformat(SequenceID)#</strong></a> <cfif not active >(inactive)</cfif>
						</td>
						<td>
							<cfif jumpExpression eq "true">
								<cfset indent = false>
							<cfelseif jumpExpression eq "">

							<cfelseif jumpExpression eq "else">
								<a href="javaScript:doAction('editSequence',#StepID#,#SequenceID#)" class="smallLink">else</a> <BR>&nbsp;&nbsp;&nbsp; <BR><cfset indent = true><cfif nextJumpExpression is "">{<BR></cfif>
							<cfelse>
								<a href="javaScript:doAction('editSequence',#StepID#,#SequenceID#)" class="smallLink">If #htmleditformat(jumpExpression)#</a> <BR> <cfset indent = true><cfif nextJumpExpression is "">{<BR></cfif>
							</cfif>

								<cfif indent>&nbsp;&nbsp;&nbsp;</cfif>
								<a href="javaScript:doAction('editSequence',#StepID#,#SequenceID#)" class="smallLink">
								#htmleditformat(jumpActionDesc)#


							<cfswitch expression="#jumpAction#">
								<cfcase value="message">#htmleditformat(jumpValue)#</cfcase>
								<cfcase value="include">#htmleditformat(jumpName)# </cfcase>
								<cfcase value="setPhrase">#htmleditformat(jumpValue)#</cfcase>
								<cfcase value="screen">#htmleditformat(jumpValue)#</cfcase>
								<cfcase value="sql">#htmleditformat(jumpName)# (#htmleditformat(comment)#)</cfcase>
								<cfcase value="getFlag">#htmleditformat(jumpValue)#</cfcase>
								<cfcase value="newStep">#htmleditformat(nextStepID)#</cfcase>
								<cfcase value="value">#htmleditformat(jumpName)# #htmleditformat(jumpValue)#</cfcase>
								<cfcase value="newProcess"><a href="javascript:{document.procActionForm.frmProcessID.value='#jumpValue#';document.procActionForm.submit()}">#htmleditformat(jumpValue)#</a></cfcase>
								<cfdefaultcase>#htmleditformat(jumpName)# to #htmleditformat(jumpValue)#</cfdefaultcase>
							</cfswitch>

								<cfif nextStepID is not 0 and jumpAction is not "newStep">
									and then go to step #htmleditformat(nextStepID)#
								</cfif>

							<cfset nextSequenceID = SequenceID + 5>
							</a>

							<cfif jumpExpression is "" and nextjumpExpression is not "">
								<BR>
								}
							</cfif>


							<!--- this next section provides links to other system components --->
							<cfswitch expression="#jumpAction#">
								<cfcase value="setPhrase"><CF_translate showtranslationlink="true" processEvenIfNested="true" translationLinkCharacter="Translate" trimphrase="30">phr_#htmleditformat(jumpValue)# </CF_translate></cfcase>
								<cfcase value="screen"><!--- could open the screen frameset ---></cfcase>
								<cfcase value="sql"></cfcase>
								<cfcase value="getFlag"><!--- could link to flag ---></cfcase>
								<cfcase value="newStep"><!--- could anchor to step ---></cfcase>
								<cfcase value="value"></cfcase>
								<cfdefaultcase></cfdefaultcase>
							</cfswitch>
						</td>
						<td>#htmleditformat(processsteptextid)#</td>
					</tr>

						<cfset previousJumpExpression = "jumpExpression">
					</CFOUTPUT>
				<TR>
					<td colspan="3">
						&nbsp;&nbsp;&nbsp;<cfoutput><a href="javaScript:addSequence(#StepID#);" class="smallLink"></cfoutput><strong>Add new action to this workflow step at position</strong></a> <CF_INPUT type="text" name = "addSeq_#StepID#" value = "#nextSequenceID#" size=3>
					</td>
				</tr>

			</cfif>

		</cfoutput>
		<TR class="evenrow">
			<td colspan="3">
				<cfoutput><a href="javaScript:addStep()" class="smallLink"><strong>Add new workflow step at position</strong></a> <CF_INPUT type="text" name = "addStep" value = "#nextStepID#" size=3></cfoutput>
			</td>
		</tr>

	</TABLE>
	</form>

	<form action="processActionEdit.cfm" method="post" name="editForm" id="editForm">
		<input type="hidden" name="frmAction" value="tba">
		<cfoutput><CF_INPUT type="hidden" name="frmProcessID" value="#frmProcessID#"></cfoutput>
		<input type="hidden" name="frmStepID" value="0">
		<input type="hidden" name="frmSequenceID" value="0">
	</form>


