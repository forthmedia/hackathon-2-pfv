<!--- �Relayware. All Rights Reserved 2014 --->


<cfparam name="frm_processID" default="">

<!--- Update any values that were changed or added --->
<CFIF IsDefined("SubmitUpdate")>
	<CFGRIDUPDATE GRID="ProcessList" DATASOURCE="#application.SiteDataSource#" TABLENAME="ProcessActions" DBTYPE="ODBC" KEYONLY="Yes">
</CFIF>

<!--- Get all defined process Actions --->
<CFQUERY NAME="GetProcessActions" datasource="#application.siteDataSource#">
	select Distinct(ProcessID) from ProcessActions
</CFQUERY>

<cf_title>Edit Relay Process Actions</cf_title>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Edit Relay Process Actions</DIV>
		</TD>
	</TR>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
<td>
		<p>Use this screen to set the correct values for the process actions that are 
		used in <CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT>.</p>

	
<cfif frm_processID eq "">
	<p><FORM ACTION="EditProcessActions.cfm" METHOD="POST" ENABLECAB="Yes">
	Process ID
			<SELECT NAME="frm_processID">	
				<cfoutput query="getProcessActions">
				<OPTION VALUE="#processID#">#htmleditformat(processID)#</option>		
				</cfoutput>														
			</SELECT>					
		<input type="submit" value="go">
	</form> </p>

<cfelse>
	<!--- Get all variables scoped by application.  --->
	<CFQUERY NAME="GetThisProcessAction" datasource="#application.siteDataSource#">
		select * from ProcessActions
		where processID =  <cf_queryparam value="#frm_processID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	<CFFORM ACTION="EditProcessActions.cfm" METHOD="POST" ENABLECAB="Yes">
	<p>Process ID
		<SELECT NAME="frm_processID">	
				<cfoutput query="getProcessActions">
				<OPTION VALUE="#processID#" <cfif processID eq frm_processID>SELECTED</cfif>>#htmleditformat(processID)#</option>		
				</cfoutput>														
			</SELECT>					
		<input type="submit" value="go">	</p>	

			<p><cfgrid name="ProcessList" height="320" width="790" query="GetThisProcessAction" insert="Yes" delete="No" sort="Yes" bold="No" italic="No" appendkey="Yes" highlighthref="No" griddataalign="LEFT" gridlines="Yes" rowheaders="No" rowheaderalign="LEFT" rowheaderitalic="No" rowheaderbold="No" colheaders="Yes" colheaderalign="LEFT" colheaderitalic="No" colheaderbold="Yes" bgcolor="White" selectmode="EDIT" picturebar="Yes">
			<cfgridcolumn name="ProcessID" header="Process ID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="yes" display="Yes" type="NUMERIC" headerbold="No" headeritalic="No">	
			<cfgridcolumn name="StepID" header="Step ID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="NUMERIC" headerbold="No" headeritalic="No">
			<cfgridcolumn name="SequenceID" header="Sequence ID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="NUMERIC" headerbold="No" headeritalic="No">
			<cfgridcolumn name="JumpExpression" header="Jump Expression" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			<cfgridcolumn name="JumpAction" header="Jump Action" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			<cfgridcolumn name="JumpName" header="Jump Name" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">		
			<cfgridcolumn name="Jumpvalue" header="Jump Value" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			<cfgridcolumn name="NextstepID" header="Next step ID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="NUMERIC" headerbold="No" headeritalic="No">
			<cfgridcolumn name="NextProcessStepTextID" header="Next Process Step TextID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			<cfgridcolumn name="Comment" header="Comment" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			<cfgridcolumn name="ProcessStepTextID" header="Process Step TextID" headeralign="LEFT" dataalign="LEFT" bold="No" italic="No" select="Yes" display="Yes" type="STRING_NOCASE" headerbold="No" headeritalic="No">
			</CFGRID></p>
			<br>
		<INPUT TYPE="submit" NAME="SubmitUpdate" VALUE="Submit">

	</CFFORM>
</cfif>
</td>
</tr>
</table>


