<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditInclude.cfm used jumpAction in set to Include
	Alsways called from processActionEdit
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	Enhancement still to do:
	2015-12-18 SB Bootstrap
	--->
<cfquery name="getProcesses" datasource="#application.siteDataSource#">
	SELECT processID, processDescription
		FROM processHeader order by processDescription
</cfquery>
<input type="hidden" name="JumpName" value="0">
<div class="form-group">
	<label for="JumpValue">
		Choose process to jump to
	</label>
	<cfselect class="form-control" id="JumpValue" name="JumpValue"
		size="1"
		message="you must specify a process"
		query="getProcesses"
		value="processID"
		display="processDescription"
		selected="#JumpName#"
		required="Yes">
	</cfselect>
</div>
