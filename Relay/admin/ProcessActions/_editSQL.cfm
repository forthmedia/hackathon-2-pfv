<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditDefault.cfm used as a default case for jumpAction
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	Enhancement still to do:
	2015-12-18 SB Bootsrap
	--->
<cfoutput>
	<div class="form-group">
		<label for="JumpName">
			Name of Query
		</label>
		<cfinput type="Text" name="JumpName" id="JumpName" value="#JumpName#" required="No" size="80" maxlength="250" class="form-control">
	</div>
	<!--- Field: ProcessActions.Jumpvalue --->
	<div class="form-group">
		<label for="Jumpvalue">
			SQL code
		</label>
		<cftextarea name="Jumpvalue" id="Jumpvalue" required="No" rows = 10 cols=80 maxlength="2000" class="form-control">
			#htmleditformat(Jumpvalue)#
		</cftextarea>
	</div>
	<!--- Field: ProcessActions.NextstepID --->
</cfoutput>
