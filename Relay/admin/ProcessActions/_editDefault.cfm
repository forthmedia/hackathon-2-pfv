<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditDefault.cfm used as a default case for jumpAction
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	2006-06-07  WAB 				parameterised the labels
	Enhancement still to do:
	2015-12-18  SB  Bootstrap
	--->
<cfparam name="valueLabel" default = "Value">
<cfparam name="nameLabel" default = "Name">
<cfoutput>
	<div class="form-group">
		<label>
			#htmleditformat(NameLabel)#
		</label>
		<cfinput type="Text" name="JumpName" value="#JumpName#" required="No" size="80" maxlength="250" class="form-control">
	</div>
	<div class="form-group">
		<label>
			#htmleditformat(valueLabel)#
		</label>
		<cfinput type="Text" name="Jumpvalue" value="#Jumpvalue#" required="No" size="80" maxlength="2000" class="form-control">
	</div>
</cfoutput>
<cfset valueLabel= "Value">
<cfset nameLabel ="Name">

