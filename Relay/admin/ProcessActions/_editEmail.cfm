<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditScreen.cfm used jumpAction in set to Include
	Alsways called from processActionEdit
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	Enhancement still to do:
	2015-12-18 SB Bootstrap
	--->
<cfquery name="getEmails" datasource="#application.siteDataSource#">
	select emailTextID from emailDef order by emailTextID
</cfquery>
<input type="hidden" name="JumpName" value="">
<div class="form-group">
	<label for="jumpValue">
		Email that will be sent if the above # expression evaluates to true
	</label>
	<cfselect name="jumpValue" id="jumpValue"
		size="1"
		message="you must specify an email"
		query="getEmails"
		value="emailTextID"
		display="emailTextID"
		selected="#JumpValue#"
		required="Yes">
	</cfselect>
</div>

