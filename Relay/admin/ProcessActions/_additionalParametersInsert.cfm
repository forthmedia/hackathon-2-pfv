<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	WAB 2006-06-07
	A basic way of allowing additional parameters to be displayed/added
	can't handle and validation or anything
	My default it will show any items which are already in the parameters field each on an individual line
	two blank boxes are shown for adding more parameters (added as name value pairs)
	Can also feed in a list of parameter names to show by default  #defaultAdditionalParameters#, these are shown first
	and then any other parameters not included in that lise are shown later
	Edits:
	2015-12-18 SB Bootstrap
	--->
<!--- output additional parameters fields --->
<cfparam name="defaultAdditionalParameters" default="">
<cfoutput>
	<cfloop index="item" list="#defaultAdditionalParameters#">
		<div class="form-group">
			<label>
				#htmleditformat(item)#
			</label>
			<cfif structKeyExists(parametersStruct,item)>
				<cfset value=parametersStruct[item]>
				<cfset x = structDelete(parametersStruct,item)>
			<cfelse>
				<cfset value = "">
			</cfif>
			<CF_INPUT name="parameter_#item#" type=text value = "#value#" size="50" class="form-control">
		</div>
	</cfloop>
	<cfif structcount(parametersStruct)>
		<!--- 	<TR>
			<TD valign="top" class="Label">Additional Parameters
			</TD>
			<TD>
			</TD>
			</TR>
			--->
		<cfloop item="item" collection=#parametersStruct#>
			<cfset value = parametersStruct[item]>
			<div class="form-group">
				<label>
					#htmleditformat(item)#
				</label>
				<CF_INPUT name="parameter_#item#" type=text value = "#value#" size="50" class="form-control">
			</div>
		</cfloop>
	</cfif>
	<div class="form-group">
		<label>
			Add Additional Parameters (enter as variable=value)
		</label>
		<input name="parameters" type=text value = "" size = 50 class="form-control">
		<BR>
		<input name="parameters" type=text value = "" size = 50 class="form-control">
	</div>
</cfoutput>
