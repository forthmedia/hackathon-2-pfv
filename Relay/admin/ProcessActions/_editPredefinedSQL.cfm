<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditPredfinedSQL.cfm
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	created by WAB from editSQL.cfm June 2006 after an old idea of SWJ
	wanted to be able to have predefined sql functions and customs ones, so split them out
	Enhancement still to do:
	2015-12-18 SB Bootstrap
	--->
<cfquery name="getProcessSQL" datasource="#application.sitedatasource#">	<!--- 2012-09-19 PPB datasource changed by WAB while investigating Case 430647 --->
	SELECT queryName, SQLStatement, description FROM processSQL order by description
</cfquery>
<CFIF jumpname neq "">
	<cfquery name="getThisSQL" dbtype="query">
		SELECT queryName, SQLStatement, description
		FROM getProcessSQL
		WHERE queryName = '#jumpName#'
		order by queryName
	</cfquery>
</CFIF>
<div class="form-group">
	<label>
		Select query
	</label>
	<cfselect class="form-control" name="JumpName" size="1" query="getProcessSQL" value="queryName" display="Description" selected="#jumpName#">
	</cfselect>
</div>
<CFIF jumpname neq "">
	<!--- if jumpname is defined i.e. we have saved a previous SQL statement show this --->
	<div class="form-group">
		<label>
			SQL Statement
		</label>
		<cfoutput>
			#htmleditformat(getThisSQL.SQLStatement)#
		</cfoutput>
	</div>
	<div class="form-group">
		<label>
			What this does
		</label>
		<cfoutput>
			#htmleditformat(getThisSQL.Description)#
		</cfoutput>
	</div>
</CFIF>
