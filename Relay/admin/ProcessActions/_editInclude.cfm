<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditInclude.cfm used jumpAction in set to Include
	Alsways called from processActionEdit
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	Enhancement still to do:
	2015-12-18  SB Bootstrap
	--->
<!--- <cfquery name="getFiles" datasource="#application.siteDataSource#">
	select fileName,description from
	processIncludeFile order by description
	</cfquery> --->
<div class="form-group">
	<label for="JumpName">
		File that will be included if the above expression is true
	</label>
	<!--- <cfselect name="frmJumpName" size="1" message="you must specify a file to include" query="getFiles" value="fileName" display="description" selected="#JumpName#" required="Yes"></cfselect> --->
	<cfinput type="Text" name="JumpName" value="#JumpName#" required="No" size="80" maxlength="250" class="form-control">
</div>
