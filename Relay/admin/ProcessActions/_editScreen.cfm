<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
_EditScreen.cfm used jumpAction in set to Include

Alsways called from processActionEdit

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012/11/14	YMA		CASE:432007 removed jumpvalue from displaying as a textbox.  Instead displays as a select option when jump value differs from getScreens list.

Enhancement still to do:


 --->

<cfquery name="getScreens" datasource="#application.siteDataSource#">
	select 0 as order1, '' as order2, 'Choose a screen or enter value in box below' as display,'' as thevalue,0,''
	union
	select 1 as order1, screenname as order2, screenName + '(' + convert(varchar,screenid) + ')' as display, convert(varchar,isNull(screenTextID,screenid)) as thevalue , screenid, screenname from screens 
	union
	select 2 as order1, convert(varchar,screenid) as order2 , convert(varchar,screenid)  + ' ' +screenName  as display, convert(varchar,isNull(screenTextID,screenid)) as thevalue ,screenid, screenname from screens order by order1,order2
</cfquery>
<cfquery name="checkValue" dbtype="query">
	select 1 from getScreens where thevalue ='#jumpvalue#' 
</cfquery>

<cfoutput>
<input type="hidden" name="JumpName" value="">
 
<TR>
	<TD valign="top" class="Label">Screen that will be included if the above expression is true
	</TD>
	<TD>
		<!--- 2012/11/14	YMA		CASE:432007 removed jumpvalue from displaying as a textbox.  Instead displays as a select option when jump value differs from getScreens list. --->
		<cfselect name="jumpValue"
          size="1"
          message="you must specify a screen to include"
          query="getScreens"
          value="thevalue"
          display="display"
          selected="#JumpValue#"
          >
			<cfif checkValue.recordcount eq 0>
				<option value="#jumpvalue#" selected="true"> #jumpvalue# </option>
			</cfif>        
        </cfselect>		
		<BR>	
	</TD>
</TR>	
</cfoutput>
