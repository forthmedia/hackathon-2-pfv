<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	2006-06-07  WAB added editing of parameters
	Made various other changes - added deletion, new sequence with any ID, added list of the process at the bottom for easier navigation
	NYB 2009-04-20 Sophos	removed spelling mistake
	NYB 2010-10-18 changed reference to relayServer in queries getJumpActions & getExpressions
	YMA	2012-11-14 Case 432005 modified to get SequenceID from query rather than frm and modified queryparam to use allowSelectStatement="true"
	YMA	2013-03-26 Case 434470 Fix to query breaking when jumpValue is left undefined.
	SB  2015-12-18 Bootstrap
	--->
<cfparam name="nextStepID" type="numeric" default="0">
<cfparam name="form.active" type="numeric" default="0">
<CFIF isDefined('FORM.frmUpdate')>
	<!--- join together any parameters into a list --->
	<cfparam name="form.parameters" default= "">
	<cfloop index="field" list = "#form.fieldnames#">
		<cfif listfirst(field,"_") is "parameter">
			<cfset paramName = listrest(field,"_")>
			<cfset paramvalue = form[field]>
			<cfif paramvalue is not "">
				<cfset quote="">
				<cfif paramvalue contains ",">
					<cfif paramvalue contains '"'>
						<cfset quote = "'">
					<cfelse>
						<cfset quote = '"'>
					</cfif>
				</cfif>
				<cfset form.parameters = listappend(form.parameters,"#paramname#=#quote##paramValue##quote#")>
			</cfif>
		</cfif>
	</cfloop>
	<CFIF FORM.frmUpdate eq "add">
		<cftry>
			<cfinsert datasource="#application.siteDataSource#"
				tablename="ProcessActions"
				dbtype="ODBC"
				formfields="processID,stepID,sequenceID,active,jumpExpression,jumpAction,JumpName,JumpValue,parameters,NextstepID,NextProcessStepTextID,Comment,ProcessStepTextID">
			<cfcatch type="database">
				<cfif cfcatch.detail contains "Violation of PRIMARY KEY">
					<!--- must be a resubmission of the form, could do an update instead, but will work to just continue --->
				<cfelse>
					<cfrethrow>
				</cfif>
			</cfcatch>
		</cftry>
	<cfelseif FORM.frmUpdate eq "update">
		<cfquery name="updateProcessActions" datasource="#application.siteDataSource#">
			update ProcessActions
				set sequenceID = <cf_queryparam value="#form.sequenceID#" CFSQLTYPE="CF_SQL_INTEGER">,
					active = <cf_queryparam value="#form.active#" CFSQLTYPE="CF_SQL_BIT">,
					jumpExpression = <cf_queryparam value="#form.jumpExpression#" CFSQLTYPE="CF_SQL_VARCHAR">,
					jumpAction = <cf_queryparam value="#form.jumpAction#" CFSQLTYPE="CF_SQL_VARCHAR">,
					JumpName = <cf_queryparam value="#form.JumpName#" CFSQLTYPE="CF_SQL_VARCHAR">,
					JumpValue = <cfif structKeyExists(form,"JumpValue")><cf_queryparam value="#form.JumpValue#" CFSQLTYPE="CF_SQL_VARCHAR"><cfelse><cf_queryparam value="null" CFSQLTYPE="CF_SQL_VARCHAR"></cfif>, <!--- 2013/03/26	YMA	CASE: 434470 Fix to query breaking when jumpValue is left undefined. --->
					parameters = <cf_queryparam value="#form.parameters#" CFSQLTYPE="CF_SQL_VARCHAR">,
					NextstepID = <cf_queryparam value="#form.NextstepID#" CFSQLTYPE="CF_SQL_INTEGER">,
					NextProcessStepTextID = <cf_queryparam value="#form.NextProcessStepTextID#" CFSQLTYPE="CF_SQL_VARCHAR">,
					Comment = <cf_queryparam value="#form.Comment#" CFSQLTYPE="CF_SQL_VARCHAR">,
					ProcessStepTextID = <cf_queryparam value="#form.ProcessStepTextID#" CFSQLTYPE="CF_SQL_VARCHAR">
			where processID = <cf_queryparam value="#form.processID#" CFSQLTYPE="CF_SQL_INTEGER">
				and stepID=<cf_queryparam value="#form.stepID#" CFSQLTYPE="CF_SQL_INTEGER">
				and sequenceID = <cf_queryparam value="#form.sequenceID_orig#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>
	<cfelseif FORM.frmUpdate eq "delete">
		<cfquery name="deleteSequence" datasource="#application.siteDataSource#">
		delete from processactions where processid =  <cf_queryparam value="#form.processid#" CFSQLTYPE="CF_SQL_INTEGER" >  and stepid =  <cf_queryparam value="#form.stepid#" CFSQLTYPE="CF_SQL_INTEGER" >  and sequenceid =  <cf_queryparam value="#form.sequenceid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!--- need to go somewhere or do something now!! --->
		<cfinclude template="listprocessactions.cfm">
		<CF_ABORT>
	</cfif>
	<!--- <cflocation url="listprocessActions.cfm?frmprocessid=#processID#" addtoken="No"> --->
</cfif>
<!--- When called the form can be called to do one of three action modes
	editSequence - in this state processID, stepID and sequenceID will all be set
	addStep - in this state processID, stepID will be 10 greater than the last step and sequenceID be set to 1
	addSequence - in this state processID, stepID and sequenceID will all be set
	--->
<cfif isDefined('frmAction') AND trim(frmAction) neq "">
	<cfif isDefined('frmprocessID') AND isDefined('frmStepID') AND isDefined('frmSequenceID')>
	<cfelseif isDefined('processID') AND isDefined('StepID') AND isDefined('SequenceID')>
		<cfset frmprocessID=processID>
		<cfset frmStepID=StepID>
		<cfset frmSequenceID=SequenceID>
	</cfif>
	<cfif frmSequenceID is "">
		<cfset frmSequenceID = "(select min(sequenceID) from processactions where processID = #frmProcessID# and stepID=#frmStepID#)">
	</cfif>
	<cfswitch expression="#frmAction#">
		<cfcase value="editSequence">
			<!--- YMA	2012/11/14 CASE: 432005 modified queryparam to use allowSelectStatement="true" --->
			<cfquery name="getSequence" datasource="#application.siteDataSource#">
					select pa.*,ph.processDescription
						from processActions pa
							inner join processHeader ph on ph.processID = pa.processID
						where pa.processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and pa.stepID =  <cf_queryparam value="#frmStepID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and pa.sequenceID =  <cf_queryparam value="#frmSequenceID#" CFSQLTYPE="CF_SQL_INTEGER" allowSelectStatement="True" >
				</cfquery>
			<cfset saveAction = "update">
			<cfset processDesc = getSequence.processDescription>
			<cfset ProcessID = getSequence.ProcessID>
			<cfset StepID = getSequence.StepID>
			<!--- YMA	2012/11/14 CASE: 432005 modified to get SequenceID from query rather than frm--->
			<cfset SequenceID = getSequence.sequenceid>
			<cfset active= getSequence.active>
			<cfset jumpExpression = getSequence.jumpExpression>
			<cfset jumpAction = getSequence.jumpAction>
			<cfset JumpName = getSequence.JumpName>
			<cfset Parameters = getSequence.parameters>
			<cfset JumpValue = getSequence.JumpValue>
			<cfset NextstepID = getSequence.NextstepID>
			<cfset NextProcessStepTextID = getSequence.NextProcessStepTextID>
			<cfset Comment = getSequence.Comment>
			<cfset ProcessStepTextID = getSequence.ProcessStepTextID>
		</cfcase>
		<cfcase value="addStep">
			<cfquery name="getProcess" datasource="#application.siteDataSource#">
					select processDescription
						from processHeader
						where processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			<cfset saveAction = "add">
			<cfset processDesc = getProcess.processDescription>
			<cfset ProcessID = frmProcessID>
			<cfset StepID = frmStepID>
			<cfset SequenceID = frmSequenceID>
			<cfset active = 1>
			<cfset jumpExpression = "true">
			<cfset jumpAction = "">
			<cfset JumpName = "">
			<cfset JumpValue = "">
			<cfset parameters = "">
			<cfset NextstepID = "">
			<cfset NextProcessStepTextID = "">
			<cfset Comment = "">
			<cfset ProcessStepTextID = "">
		</cfcase>
		<cfcase value="addSequence">
			<cfquery name="getProcess" datasource="#application.siteDataSource#">
					select processDescription
						from processHeader
						where processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			<cfset saveAction = "add">
			<cfset processDesc = getProcess.processDescription>
			<cfset ProcessID = frmProcessID>
			<cfset StepID = frmStepID>
			<cfset SequenceID = frmSequenceID>
			<cfset active = 1>
			<cfset jumpExpression = "true">
			<cfset jumpAction = "">
			<cfset JumpName = "">
			<cfset JumpValue = "">
			<cfset parameters = "">
			<cfset NextstepID = "">
			<cfset NextProcessStepTextID = "">
			<cfset Comment = "">
			<cfset ProcessStepTextID = "">
		</cfcase>
	</cfswitch>
</cfif>
<!--- 2010-10-18 NYB changed reference to relayServer --->
<cfquery name="getJumpActions" datasource="#application.siteDataSource#">
	select * from processJumpAction order by jumpActionDesc
</cfquery>
<!--- 2010-10-18 NYB changed reference to relayServer --->
<cfquery name="getExpressions" datasource="#application.siteDataSource#">
	select JumpExpression,JumpExpressionDesc from processJumpExpression
	order by JumpExpressionDesc
</cfquery>
<cfquery name="getNextSteps" datasource="#application.siteDataSource#">
	select 0 as stepID, 'Progress to next sequence or step' as display, 1 as orderIndex
	union
	select -1 as stepID, 'End Process (under development)' as display, 2 as orderIndex
	union
	select distinct stepID, 'Jump to step '+ convert(varchar(5),stepID), 3 as orderIndex
		from processActions
		where processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
	order by orderIndex, stepID
</cfquery>
<cfquery name="maxStep" datasource="#application.siteDataSource#">
	select isNull(max(stepID),0) as stepid
		from processActions
		where processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
<cfquery name="maxSequence" datasource="#application.siteDataSource#">
	select isNull(max(sequenceID),0) as sequenceid
		from processActions
		where processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >  and stepid =  <cf_queryparam value="#frmstepid#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
<cfset parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(parameters,",")>
<cf_title>
	Edit Workflow Action
</cf_title>
<cfsavecontent variable="headerScript">
	<cfoutput>
<SCRIPT type="text/javascript">
<!--

function doAction2(action){
	form = document.mainForm;
	form.frmUpdate.value = action;
	form.submit();
	}

function viewFullProcess(){
	form = document.mainForm;
	form.action = "listProcessActions.cfm";
	form.submit();
	}

function newStep(newStepID){
	form = document.mainForm;
	newStepID = prompt('Enter new StepID',newStepID)
	form.frmAction.value = "addstep";
	form.stepID.value = newStepID
	form.sequenceID.value = 10;
	console.log(newStepID)
	if (newStepID != null) {
	form.submit();
	}
	}

function newSequence(newSequenceID){
	form = document.mainForm;
	form.frmAction.value = "addSequence";
	newSequenceID = prompt('Enter new SequenceID',newSequenceID)
	// alert(form.stepID.value )
	form.sequenceID.value = newSequenceID;
	if (newSequenceID != null) {
	form.submit();
	}
	}
//-->
</script>
	</cfoutput>
</cfsavecontent>
<cfhtmlhead text="#headerScript#">
<cfset newStepID = round(maxStep.stepID/10+1)*10>
<cfset newSequenceID = round(maxSequence.sequenceID/10+1)*10>
<CF_RelayNavMenu pageTitle="Edit Workflow Action" thisDir="/admin/processActions">
	<CF_RelayNavMenuItem MenuItemText="Show full Process" CFTemplate="javaScript:viewFullProcess()">
	<CF_RelayNavMenuItem MenuItemText="New step" CFTemplate="javaScript:newStep(#newStepID#)">
	<CF_RelayNavMenuItem MenuItemText="New Sequence" CFTemplate="javaScript:newSequence('#newSequenceID#')">
	<CF_RelayNavMenuItem MenuItemText="Delete item" CFTemplate="javaScript:doAction2('Delete')">
	<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javaScript:doAction2('#saveAction#')">
</CF_RelayNavMenu>
<!--- Entry form --->
<cfform action="processActionEdit.cfm" method="POST" name="mainForm"  preservedata="Yes" enablecab="Yes">
	<cfoutput>
		<CF_INPUT type="hidden" name="processID" value="#ProcessID#">
		<CF_INPUT type="hidden" name="frmProcessID" value="#ProcessID#">
		<input type="hidden" name="frmUpdate" value="tba">
		<CF_INPUT type="hidden" name="stepID" value="#StepID#">
		<input type="hidden" name="frmAction" value="editSequence">
		<input type="hidden" name="NextProcessStepTextID" value="">
	</cfoutput>
	<div id="processActionsEditCont">
		<div class="form-group">
			<label>
				Workflow name:
			</label><br />
			<cfoutput>
				#htmleditformat(processDesc)#
			</cfoutput>
		</div>
		<div class="form-group">
			<label>
				Step:
			</label><br />
			<cfoutput>
				#htmleditformat(StepID)#
			</cfoutput>
		</div>
		<div class="checkbox">
			<label>
				Active
				<cfoutput>
					<input type="checkbox" value="1" name = active
					<cfif active>
						checked
					</cfif>
					>
				</cfoutput>
			</label>
		</div>
		<div class="form-group">
			<label>
				Sequence in this step
			</label>
			<cfinput type="Text" name="sequenceID" value="#sequenceID#" message="You must set a sequence" required="Yes" size="30" maxlength="4" class="form-control">
			<CF_INPUT type="hidden" value="#sequenceID#" name = "sequenceID_orig">
		</div>
		<div class="form-group">
			<label for="ProcessStepTextID">
				Process Step Text ID (unique text string given that can be used to call this process step)
			</label>
			<cfinput type="Text" name="ProcessStepTextID" id="ProcessStepTextID" value="#ProcessStepTextID#" required="no" size="80" maxlength="80" class="form-control">
		</div>
		<div class="form-group">
			<label for="exampleExpressions">
				Evaluate this expression
			</label>
			<!--- WAB 2007-05-01 added this script - until then the drop down seems to have had no purpose! --->
			<script>
							function insertExampleExpression() {
								form = document.mainForm;
								if (form.exampleExpressions.value != '') {
									form.jumpExpression.value = form.exampleExpressions.value
								}
								form.exampleExpressions.selectedIndex = 0

							}
							</script>
			<cfselect class="form-control" id="exampleExpressions" name="exampleExpressions" size="1" queryposition="below" query="getExpressions" value="JumpExpression" display="JumpExpressionDesc" selected="#JumpExpression#" visible="Yes" enabled="Yes" onchange="insertExampleExpression()">
				<cfoutput>
					<!--- START:  NYB 2009-04-20 Sophos - replaced "and" with "an" below: --->
					<option value="" selected >
						Choose an example expression or add your own below
					<option value="#JumpExpression#"  >
						Reset
				</cfoutput>
			</cfselect>
			<br /> <cfinput class="form-control" type="Text" name="jumpExpression" value="#JumpExpression#" message="You must set a jump expression" required="Yes" size="80" maxlength="250">
		</div>
		<div class="form-group">
			<label for="jumpAction"> Take this action if the above expression is true</label>
			<cfselect class="form-control" name="jumpAction" id="jumpAction" onChange="javaScript:doAction2('#saveAction#')" message="You must set an Action for this step" query="getJumpActions" value="jumpAction" display="jumpActionDesc" selected="#jumpAction#" required="Yes" queryposition="below"> <option value="" >Please select an action</option> <cfif jumpaction is not "" and not listFindNoCase(valueList(getJumpActions.jumpaction),jumpAction)><cfoutput><option value="#jumpAction#" >#htmleditformat(jumpAction)#</option></cfoutput></cfif> </cfselect>
		</div>
		<!--- This section will include various mini includes which control
			the user interface for each jumpAction chosen --->
		<CFSWITCH expression="#jumpAction#">
			<CFCASE value="setFlag">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="getFlag">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="include">
				<CFINCLUDE template="_editInclude.cfm">
			</CFCASE>
			<CFCASE value="getAppDetails">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="location">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="message">
				<cfset valueLabel = "Message Text">
				<CFINCLUDE template="_editDefault.cfm">
				<cfset defaultAdditionalParameters = "Headline,ContinueButtonLabel,ShowCancelButton,CancelButtonLabel">
				<CFINCLUDE template="_additionalParametersInsert.cfm">
			</CFCASE>
			<CFCASE value="relayTag">
				<cfset valueLabel = "Name of Relay Tag (without chevrons)">
				<CFINCLUDE template="_editDefault.cfm">
				<CFINCLUDE template="_additionalParametersInsert.cfm">
			</CFCASE>
			<CFCASE value="newProcess">
				<CFINCLUDE template="_editProcess.cfm">
			</CFCASE>
			<CFCASE value="newStep">
				<input type="hidden" name="JumpName" value="">
				<input type="hidden" name="JumpValue" value="">
			</CFCASE>
			<CFCASE value="screen">
				<CFINCLUDE template="_editScreen.cfm">
				<cfset defaultAdditionalParameters = "hideRightBorder">
				<CFINCLUDE template="_additionalParametersInsert.cfm">
			</CFCASE>
			<CFCASE value="setPhrase">
				<CFINCLUDE template="_editPhrase.cfm">
			</CFCASE>
			<CFCASE value="submit">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="value">
				<CFINCLUDE template="_editDefault.cfm">
			</CFCASE>
			<CFCASE value="SQL">
				<CFINCLUDE template="_editSQL.cfm">
			</CFCASE>
			<CFCASE value="predefinedSQL">
				<CFINCLUDE template="_editpredefinedSQL.cfm">
			</CFCASE>
			<CFCASE value="sendemail">
				<CFINCLUDE template="_editEmail.cfm">
			</CFCASE>
			<CFCASE value="">
			</CFCASE>
			<CFDEFAULTCASE>
				<CFINCLUDE template="_editDefault.cfm">
			</CFDEFAULTCASE>
		</CFSWITCH>
		<div class="form-group">
			<label for="NextStepID">
				Step jump
			</label>
			<cfselect name="NextStepID" id="NextStepID" size="1" query="getNextSteps" value="stepID" display="display" selected="#NextstepID#" class="form-control">
			</cfselect>
			<!--- <br><cfinput type="Text" name="NextstepID" value="#NextstepID#" message="Must be an integer field" validate="integer" required="No" maxlength="4"> --->
		</div>
		<div class="form-group">
			<label>
				Description or comments
			</label>
			<cfinput class="form-control" type="Text" name="Comment" value="#comment#" required="No" size="80" maxlength="255">
		</div>
	</div>
</CFFORM>
<!--- Now output a listing of the current process with the current step expanded --->
<CFQUERY name="GetRecords" datasource="#application.SiteDataSource#" >
		SELECT pa.*, ja.jumpActionDesc
		FROM processActions pa
		left JOIN processJumpAction ja on pa.jumpAction = ja.jumpAction
		where pa.processID =  <cf_queryparam value="#frmProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY pa.stepID, pa.sequenceID
	</CFQUERY>
<cfset onlyexpandstepid = stepid>
<cfinclude template = "listprocessActions_Insert.cfm">
