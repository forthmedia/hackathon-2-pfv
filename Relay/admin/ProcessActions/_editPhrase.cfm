<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	_EditPhrase.cfm used as a default case for jumpAction
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	Enhancement still to do:
	2015-12-18 SB Bootstrap
	--->
<div class="form-group">
	<label>
		Phrase Text ID you want to set
	</label>
	<cfinput type="Text"
		name="JumpValue"
		value="#JumpValue#"
		message="You must specify a phrase Text ID with no spaces"
		required="Yes"
		size="80"
		maxlength="80">
</div>
<div class="form-group">
	<label>
		Name of variable to put this phrase in
	</label>
	<cfinput type="Text"
		name="JumpName"
		value="#JumpName#"
		message="You must specify a variable to put this phrase in"
		required="Yes"
		size="80"
		maxlength="80">
</div>
