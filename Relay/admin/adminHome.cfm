<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="message" DEFAULT="">

<CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
<CFIF #TRIM(MESSAGE)# IS "">
<!--- only print this table text if no message --->
	<CFOUTPUT>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="grey-box-top">
				<h2>You can delete test communications with this tool</h2>
				<A HREF="/admin/deleteCommunication.cfm" class="btn btn-primary">Delete communications</A>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="grey-box-top">
				<h2>Use this tool to manage related file categories</h2>
				<A HREF="/admin/relatedFileManager.cfm" class="btn btn-primary">Related File Categories</A>
			</div>
		</div>
	</div>
	</cfoutput>
</CFIF>