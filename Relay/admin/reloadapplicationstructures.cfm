<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			reloadApplicationStructures.cfm
Author:				WAB
Date started:			2009/06/25

Description:
A single page for doing reloads of various application structures held in memory
Uses standard named functions on applicationVariables.cfc and introspection to build up a list of all the things which can be reloaded
Includes a template reloadApplicationStructureTask.cfm which does the reload.  This template can be called directly itself

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/01/11   WAB Improvements to cluster feedback
2010/06 	WAB Use new function clustermanagement.isThisSiteClustered
Possible enhancements:


 --->
 <div id="reloadapplicationstructureTable" class="paddingRightLeft">
 <cfparam name="structure" default = "">
 <cfsetting requesttimeout = "300"><!--- WAb 2011/03/10 Needed this for reloading application components on 6 lenovo servers!  Hopefully when we do clustering in a thread or asynchronously we won't need this so long--->
	<cfparam name = "waitForClusterResponse" default = "false">
	<!--- Note that variables applicationVariablesOnj and meta data are set in this include file --->
	<cfsavecontent variable="reloadResultHTML">
		<cfinclude template="reloadApplicationStructuresTask.cfm">
	</cfsavecontent>

<cfparam name = "updateCluster" default = "true"> <!--- this is deliberately after the reloadApplicationStructuresTask.cfm include --->



 	<script>
		function doReload(structure) {
			urlQueryString =  'structure=' + structure

			clusterCheckBoxObj = document.getElementById ('updateClusterCheckBox')
			waitForClusterResponseCheckBoxObj	= document.getElementById ('waitForClusterResponseCheckBox')
			if (clusterCheckBoxObj && clusterCheckBoxObj.checked) {
				urlQueryString += '&updateCluster=true'
			}

			if (waitForClusterResponseCheckBoxObj && waitForClusterResponseCheckBoxObj.checked) {
				urlQueryString += '&waitForClusterResponse=true'
			}





			window.location.search = urlQueryString


		}
	</script>
	<h1>phr_admin_reloadtool</h1>
	<cfset getFunctions = applicationVariablesObj.getReloadFunctions()>
	<cfoutput>
	<table id="reloadApplicationStructure" class="table table-hover table-bordered">
		<tr>
			<th>Choose an item to Reload</th>
		</tr>
		<cfloop array = "#getFunctions.orderedArray#" index="functionName">
		<cfset thisFunction = getFunctions.functions[functionName]>
		<tr>
			<td><a href="javascript:doReload('#replaceNoCase(thisFunction.name,"reload_","")#')"><cfif structKeyExists (thisFunction,"description")>#application.com.security.sanitiseHTML(thisFunction.description)#<cfelse>#application.com.security.sanitiseHTML(thisFunction.name)#</cfif></a></td>
		</tr>
	</cfloop>
	</table>
	</cfoutput>

	<cfif application.com.clustermanagement.isThisSiteClustered()>
		<div class="checkbox">
  			<label>
				<input type="checkbox" id ="updateClusterCheckBox" <cfif updateCluster>checked</cfif>>
				Update All Machines in Cluster
			</label>
		</div>
		<div class="checkbox">
  			<label>
				<input type="checkbox" id ="waitForClusterResponseCheckBox" <cfif waitForClusterResponse>checked</cfif>> Wait For Cluster Response
			</label>
		</div>
	</cfif>


	<cfoutput><BR>#reloadResultHTML#</cfoutput>

</div>
