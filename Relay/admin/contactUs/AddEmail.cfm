<!--- �Relayware. All Rights Reserved 2014 --->

<cf_translate>
<cfoutput>
<cfif isdefined("EditEmail")>
	<cfquery name="addContact" datasource="#application.siteDataSource#">
		INSERT INTO ContactUsContacts (ReasonID, CountryID, Emailaddress)   
   		VALUES (<cf_queryparam value="#ReasonID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmSelectOption#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmAddEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</cfquery>
	<cflocation url="Managedefinition.cfm?contactUsTypeID=#contactUsTypeID#&reasonID=#reasonID#" addtoken="No">
<cfelse>
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<cfoutput>
	<cfform method="post">
		<cf_relayFormDisplay>
			<cfquery name="Getcountry" datasource="#application.sitedatasource#">
				select CountryID, LocalCountryDescription
				from country
			</cfquery>
			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmAddEmail" currentValue="" label="Email" size="40" maxlength="50" tabindex="1" required="yes">
			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmSelectOption" size="1" query="#Getcountry#" display="LocalCountryDescription" value="CountryID" label="Country" required="Yes" nulltext="Phr_chooseACountry" nullValue="">
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="EditEmail" currentValue="submitbutton" label="" spanCols="No" valueAlign="left" class="button">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#contactUsTypeID#" fieldname="contactUsTypeID">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#reasonID#" fieldname="reasonID">
		</cf_relayFormDisplay>
	</cfform>
	</cfoutput>
</cfif>
</cfoutput>
</cf_translate>
