<!--- �Relayware. All Rights Reserved 2014 --->
<cf_translate>
<cfoutput>
<cfset request.relayFormDisplayStyle = "HTML-table">
<cfinclude template="/templates/relayFormJavaScripts.cfm">
<CFIF NOT IsDefined("ContactUsTypeID") and NOT IsDefined("reasonID")> 
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR class="Submenu">
			<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Phr_Sys_Managedatafor</TD>
			<TD ALIGN="right" VALIGN="TOP" class="Submenu">
				<A HREF="AddDefinition.cfm">Phr_Int_contacts_addnew</A>&nbsp;&nbsp;
			</TD>
		</TR>
	</TABLE>
	<cfquery name="GetDefinitions" datasource="#application.sitedatasource#">
		select *
		From ContactUsType
	</cfquery>
	<CF_tableFromQueryObject queryObject="#GetDefinitions#" 
		showTheseColumns="contactustypeID,ContactUsName"
		HidePageControls="yes"
		useInclude = "false"
		numRowsPerPage="400"
		
		keyColumnList="ContactUsName"
		keyColumnURLList="ManageDefinition.cfm?contactustypeID="
		keyColumnKeyList="contactustypeID"
		columnTranslation="true"
		>
		
<cfelseif isdefined("ContactUsTypeID") and NOT IsDefined("reasonID")>
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR class="Submenu">
			<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Phr_Sys_Managedatafor</TD>
			<TD ALIGN="right" VALIGN="TOP" class="Submenu">
				<A HREF="AddReason.cfm?contactUSTypeID=#contactUSTypeID#">Phr_Int_contacts_addnew</A>&nbsp;&nbsp;
			</TD>
		</TR>
	</TABLE>
	<cfquery name="GetReasons" datasource="#application.sitedatasource#">
		select *, 'translate/edit' as link, 'contactUs' + convert(varchar(3), contactUSTypeID) + 'reasonID' + convert (varchar(5), reasonid)  as phraseTextID
		From ContactUsReason
		where contactUSTypeID =  <cf_queryparam value="#contactUSTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<CF_tableFromQueryObject queryObject="#GetReasons#" 
		showTheseColumns="ReasonName,link"
		HidePageControls="yes"
		useInclude = "false"
		numRowsPerPage="400"
		
		keyColumnList="ReasonName,link"
		keyColumnURLList="ManageDefinition.cfm?contactustypeID=#contactustypeID#&reasonID=,/translation/editphrase.cfm?frmphraseid="
		keyColumnKeyList="reasonID,phraseTextID"
		
		keyColumnOpenInWindowList="no,yes"
		OpenWinNameList="TranslateWin"
		OpenWinSettingsList="scrollbars=yes,menubar=no,toolbar=no,resizable=yes,width=780,height=580"
		columnTranslation="true"
		>
		
<cfelse>
	<cfif isdefined("DeleteEmail")>
		<cfquery name="DeleteExistingEmployee" datasource="#application.sitedatasource#">
			DELETE FROM ContactUsContacts
			WHERE ContactEmailsID =  <cf_queryparam value="#ContactEmailsID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cfif>
	<cfif isdefined("EditEmail")>
		<cflocation url="EditEmail.cfm?ContactUstypeID=#ContactUstypeID#&reasonID=#reasonID#&ContactEmailsID=#ContactEmailsID#" addtoken="No">
	</cfif>
	<cfquery name="GetEmails" datasource="#application.sitedatasource#">
		select ReasonName, EmailAddress, LocalCountryDescription, contactEmailsID, c.countryID
		From contactUsType CUType inner join
		ContactUsReason CUReason on CUtype.contactUsTypeID = CUReason.ContactUsTypeID inner join
		contactUsContacts CUContacts on CUReason.ReasonID = CUContacts.ReasonID inner join
		country c on CUContacts.countryID = c.countryID
		where CUType.ContactUsTypeID =  <cf_queryparam value="#ContactUstypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and cureason.reasonID =  <cf_queryparam value="#reasonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR class="Submenu">
			<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">#htmleditformat(GetEmails.ReasonName)#</TD>
			<TD ALIGN="right" VALIGN="TOP" class="Submenu">
				<A HREF="AddEmail.cfm?contactUSTypeID=#contactUSTypeID#&reasonID=#reasonID#">Phr_Int_contacts_addnew</A>&nbsp;&nbsp;
			</TD>
		</TR>
	</TABLE>
	<table class="withborder" cellspacing="9">
	<tr bgcolor="c0c0c0">
		<td>
			phr_Int_contactus_Country
		</td>
		<td>
			phr_Int_contactus_EmailAddress
		</td>
		<td>
			phr_Int_contactus_Functions
		</td>
	</tr>
	<cfloop query="GetEmails">
		<tr>
			<td>
				#htmleditformat(GetEmails.LocalCountryDescription)#
			</td>
			<td>
				#htmleditformat(GetEmails.EmailAddress)#
			</td>
			<td>
				<cfform method="post" name="#GetEmails.contactEmailsID#">
						<CF_relayFormElement relayFormElementType="submit" fieldname="EditEmail" currentValue="phr_Int_contactus_Edit" label="" spanCols="No" valueAlign="right" class="button">
					<cfif GetEmails.countryID NEQ 37>
						<CF_relayFormElement relayFormElementType="submit" fieldname="DeleteEmail" currentValue="phr_Int_contactus_Delete" label="" spanCols="No" valueAlign="left" class="button">
					</cfif>
						<CF_relayFormElement relayFormElementType="hidden" label="" currentvalue="#GetEmails.contactEmailsID#" fieldname="ContactEmailsID">
						<CF_relayFormElement relayFormElementType="hidden" label="" currentvalue="#contactUsTypeID#" fieldname="contactUsTypeID">
						<CF_relayFormElement relayFormElementType="hidden" label="" currentvalue="#reasonID#" fieldname="reasonID">
				</cfform>
			</td>
		</tr>
	</cfloop>
	</table>
</cfif>
</cfoutput>
</cf_translate>