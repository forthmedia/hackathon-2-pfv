<!--- �Relayware. All Rights Reserved 2014 --->
<cf_translate>
<cfoutput>
<cfif isdefined("addreason")>
	<cfquery name="Addreason" datasource="#application.siteDataSource#">
   		INSERT INTO ContactUsReason (ContactUsTypeID, ReasonName)   
   		VALUES (<cf_queryparam value="#contactUsTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmreasonName#" CFSQLTYPE="CF_SQL_VARCHAR" >)
		select scope_identity() as reasonID
	</cfquery>
	<cfset phraseTextID = "contactUs#contactUsTypeID#reasonID#Addreason.reasonID#">
	
	<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID = phraseTextID, phrasetext="#frmreasonName#", languageid = 1, countryid = 0)>
	
	<cfquery name="AddDefaultEmail" datasource="#application.siteDataSource#">
		insert into contactUsContacts (reasonID, CountryID, EmailAddress)
		Values(<cf_queryparam value="#Addreason.reasonID#" CFSQLTYPE="CF_SQL_INTEGER" >, 37, <cf_queryparam value="#frmEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</cfquery>
	
	<cflocation url="Managedefinition.cfm?contactUsTypeID=#contactUsTypeID#" addtoken="No">
<cfelse>
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	<cfform method="post">
		<cf_relayFormDisplay>
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#contactUsTypeID#" fieldname="contactUsTypeID">
			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmreasonName" currentValue="" label="phr_Int_contactus_ReasonName" size="40" maxlength="50" tabindex="1" required="yes">
			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmEmail" currentValue="" label="phr_Email" size="40" maxlength="255" tabindex="1" required="yes">
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="AddReason" currentValue="submitbutton" label="" spanCols="No" valueAlign="left" class="button">
		</cf_relayFormDisplay>
	</cfform>
</cfif>
</cfoutput>
</cf_translate>
