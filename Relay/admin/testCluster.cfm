<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			testCluster.cfm
Author:				WAB
Date started:			2009/09

Description:
reports whether the cluster is operating properly

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-18 	WAB Added cluster statistics

Possible enhancements:


 --->

<cfif isdefined("removeClusterID")>
	<CFSET application.com.clustermanagement.removeClusterInstance(id = removeClusterID)>
</cfif>
<cfparam name="url.showStats" default="false">


<cfset clustertest = application.com.clustermanagement.runfunctiononcluster(functionname="TestMe",includecurrentserver=true,wait=false,timeout=3)>
<cfset clusterQuery = application.com.clustermanagement.getClusterAndHostQuery (includeInactive = true,includecurrentserver=true,includeStats = url.showStats)>
<cfset sessionCount = application.com.clustermanagement.runfunctiononcluster(functionname="getNumberOfActiveSessions",includecurrentserver=true,wait=false,timeout=3)>

 <cfoutput>
<h2>phr_admin_testclustering</h2>
<form >
	<input type="checkbox" name="showStats" value="true" <cfif url.showStats>checked</cfif> onclick="this.form.submit()"> Show Statistics

 <table id="testClusterTable" class="withBorderNoMargin table table-hover">
 <tr>
	<th valign="top">Server Name</th>
	<th valign="top">Instance Name</th>
	<th valign="top">Application Name</th>
	<th valign="top"></th>
	<th valign="top">hosts</th>
	<th valign="top">Result</th>
	<cfif showStats>
		<th valign="top" width="100">Last Request  </th>
		<th valign="top">Last Session Started</th>
		<th valign="top">Active Sessions</th>
		<th valign="top">Sessions Last Hour</th>
		<th valign="top">Sessions Today</th>
		<th valign="top">Sessions Yesterday</th>
		<th valign="top">Sessions Last Week</th>

	</cfif>

	<th valign="top">Remove</th>
</tr>

 </cfoutput>
 <cfoutput query = "clusterQuery" group="id">
 <tr>
 	<td valign="top">
		#htmleditformat(serverName)#
	</td>

 	<td valign="top">
		<a href="http://#instanceIPAddress#<cfif instancePort neq '8300'>:#htmleditformat(instancePort)#</cfif>/cfide/administrator" target="_new">#htmleditformat(instanceName)#</a> (#htmleditformat(instanceIPAddress)#:#htmleditformat(instancePort)#)
		<cfif currentInstance>*</cfif>
	</td>
 	<td valign="top">
		#htmleditformat(CFAPPName)#
	</td>

	<td valign="top">
		<cfset counter = 0>
		<cfset maxToShow = 2>
		<cfif numberHosts gt maxToShow>
			<cfset instanceDivID = "hosts_#replace(instanceName,"-","","ALL")#">
			<cf_divOpenClose divid="#instanceDivID#" startOpen=false opentext="More" CloseText="">
				<cf_divOpenClose_Image>
			</cf_divOpenClose>
		</cfif>
	</td>
	<td valign="top">

		<cfoutput>
			<cfset counter ++>
			<cfif counter is maxToShow + 1>
			<div id="#instanceDivID#" style=#divStyle#>
			</cfif>
			<a href="#request.currentsite.httpprotocol##hostname#/index.cfm?forceInstance=#instanceName#" target="_new">#htmleditformat(hostname)#</a><BR>
		</cfoutput>
		<cfif numberHosts gt maxToShow>
			</div>
		</cfif>
	</td>
 	<td valign="top">
	<cfset showRemoveLink = true>
	<cfif active >
		<cfif structKeyExists (clusterTest,instanceUniqueID)>
		 	<cfset  thisServerresult = clusterTest[instanceUniqueID]>
			<!--- Rather complicated because testme returns a string, but if there is a failure then there is a structure! --->
			<cfif isstruct(thisServerresult) and structKeyExists(thisServerresult,"isOK") and not thisServerresult.isOK>
				<font color="red">Failed</font><BR>
				#htmleditformat(thisServerresult.message)#
				<cfdump var="#thisServerresult#">
			<cfelse>
					<font color="Green">OK</font>
					<cfset showRemoveLink = false>

			</cfif>
		<cfelseif not recentlyUpdated>
			Not polled!	<BR> Not recently updated!
		<cfelse>

		</cfif>

	<cfelse>
		Not Active
	</cfif>
	</td>
	<cfif showStats>
		<td valign="top"><cfif lastRequest is not "">#replace(application.com.datefunctions.DayDiffFromTodayInWords(date=lastRequest,doTime = true)," ","&nbsp","ALL")# </cfif> </td>
		<td valign="top"><cfif lastSessionStarted is not "">#replace(application.com.datefunctions.DayDiffFromTodayInWords(date=lastSessionStarted,doTime = true)," ","&nbsp","ALL")# </cfif> </td>
		<td valign="top" align="right">
			<cfif structKeyExists (sessionCount,instanceUniqueID)>
			 	<cfset  thisServerresult = sessionCount[instanceUniqueID]>
			 	<cfif thisServerresult.isOK>
					#thisServerresult.count#
				</cfif>
			</cfif>
		</td>
		<td valign="top" align="right">#htmleditformat(lasthour)#</td>
		<td valign="top" align="right">#htmleditformat(today)#</td>
		<td valign="top" align="right">#htmleditformat(yesterday)#</td>
		<td valign="top" align="right">#htmleditformat(lastweek)#</td>
	</cfif>
	<td>
		<cfif showRemoveLink>
		<CF_INPUT type="checkbox" name="removeClusterID" value="#id#" onclick="this.form.submit()">
		</cfif>
	</td>

 </tr>

 </cfoutput>
 <cfoutput>
 </table>
<a href="" class="btn btn-primary">Refresh</a>
</form>



 </cfoutput>



