<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		portalSearch.cfm
Author:			IH
Date started:	30-09-2013

Description:	Administer portal search options

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2015-01-22			ACPK		CORE-999 Fixed error in order by section of query

Possible enhancements:

 --->

<cfparam name="qPortalSearch" default="">
<cfparam name="sortOrder" default="sortOrder">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="portalSearchID" type="numeric" default="0">

<cfset xmlSource = "">

<cfif structKeyExists(url,"editor") and isNumeric(portalSearchID)>
	<cfset qSearch=application.com.search.getPortalSearchSources(portalSearchID=portalSearchID)>

	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="233" name="thisEditor" entity="portalSearch" title="Portal Search Options" readonly="false">
				<field name="displayname" required="true" label="phr_portalsearch_displayName"/>
				<field name="collectionName" required="true" label="phr_portalsearch_collectionName"/>
				<field name="searchMethodName" required="true" label="phr_portalsearch_searchMethodName"/>
				<field name="indexMethodName" label="phr_portalsearch_indexMethodName"/>
				<field name="path" label="phr_portalsearch_path"/>
				<field name="languageid" label="phr_portalsearch_language" query="select languageID as value, language as display  from language where language in ('English','Brazilian','French','Russian','Czech','Dutch','German','Thai','Chinese','English','Greek','Japanese','Korean') order by language"/>
				<field name="sortOrder" label="phr_portalsearch_sortOrder"/>
				<field name="active" label="phr_portalsearch_active" default="1"/>
				<lineItem label="phr_portalsearch_lastIndexed">
					<field name="lastIndexed" label="phr_portalsearch_lastIndexed" control="html"></field>
					<field name="refreshLink" control="html">
						<cfif qSearch.collectionName is not "" and qSearch.indexMethodName is not "">
							<cfsavecontent variable="refreshCollection">
								<cfoutput><a href="/scheduled/portalSearchRefresh.cfm?portalSearchID=#portalSearchID#" title="Click to refresh collection (will open a new window)" target="_blank">Refresh</a></cfoutput>
							</cfsavecontent>
							#htmlEditFormat(refreshCollection)#
						</cfif>
					</field>
				</lineItem>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>

	<cfoutput>
	<style>##refreshLink_lineItemDiv {padding-left:25px;}</style>
	</cfoutput>
<cfelse>
	<!--- 2015-01-22 ACPK CORE-999 Fixed error in order by section of query --->
	<cfquery name="qPortalSearch" datasource="#application.siteDataSource#">
		select portalSearchID,sortOrder,displayname,path,searchMethodName,lastIndexed,active
		from portalSearch
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
	<cfset application.com.request.setTopHead(topHeadCfm="portalSearchTopHead.cfm",pageTitle="Messages")>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="portalSearch"
	keyColumnList="displayname"
	keyColumnKeyList="portalSearchID"
	keyColumnURLList="#cgi.script_name#?editor=true&portalSearchID="
	showTheseColumns="sortOrder,displayname,path,searchMethodName,lastIndexed,active"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#qPortalSearch#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	booleanFormat="active"
	hideTheseColumns=""
	FilterSelectFieldList=""
	dateTimeFormat="lastIndexed"
	showSaveAndAddNew="true"
	hideBackButton = "false"
	columnTranslation="true"
	columnTranslationPrefix="phr_portalsearch_">


