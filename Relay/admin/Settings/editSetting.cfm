<!--- �Relayware. All Rights Reserved 2014 --->
<!---
EditSetting.cfm


WAB 2013-03-07 Altered the initialisation of relayformValidation to use new function
2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on
2016-04-05	WAB BF-246 Get UI related to setting variants to work again in the DIV world

 --->

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfset application.com.request.setDocumentH1(text="Phr_Sys_CurrentSettings")>

<cfparam name="getWholeSection" default="true">
<cfparam name="mode" default="">
<cfset settingsObj = application.com.settings>
<cfset xmlFunctions = application.com.xmlfunctions>

<cfif structKeyExists(form,"SettingFieldList")>
	
	<cfscript>
		//RJT PROD2015-633 Because of the changes to multiselects empty settings are absent from the form, this leads to attempting to clear a multiselect being impossible
		settingFieldArray=ListToArray(form.SettingFieldList);
		for(settingsField in settingFieldArray){
			if (!structKeyExists(form,settingsField)){
				form[settingsField]="";
				form.fieldnames=listAppend(form.fieldnames,settingsField);
			}
		}
	
	</cfscript>
	
	<cftry>
	<cfset settingsObj.ProcessSettingFormUpdate()>
		<cfoutput>#application.com.relayUI.message(message="Settings saved.",type="success")#</cfoutput>

		<cfcatch>
			<cfset application.com.errorHandler.recordRelayError_Warning(Severity="ERROR", catch=cfcatch,message="Settings save error")>
			<cfoutput>#application.com.relayUI.message(message="Problems saving settings. #cfcatch.message#",type="error")#</cfoutput>
		</cfcatch>
	</cftry>
</cfif>


<cfif isdefined("variableToEdit") and variableToEdit is not "">

			<cfset request.relayFormDisplayStyle = "HTML-div">
			<cf_includeJavascriptOnce template = "/javascript/checkObject.js">
			<cf_includeJavascriptOnce template = "/javascript/verifyObjectV2.js">
			<cf_includeJavascriptOnce template = "/javascript/rwFormValidation.js" translate="true">

			<cf_includeJavascriptOnce template = "/javascript/cf/cfform.js">


			<script>
				var delim = <cfoutput>'#jsStringFormat(application.delim1)#'</cfoutput>

				function loadVariantRows(obj,variableToEdit) {

					var settingDiv = obj.up('.setting');

					// delete existing variant rows
//					regExp = new RegExp (variableToEdit + '_')
//					deleteRowsByRegularExpression (currentTableObj,regExp)


						page = '/webservices/settingsWS.cfc?wsdl&method=getVariantRows'
						params = 'returnFormat=plain&variableName='+variableToEdit;

						// changed to post to ensure proper refresh
						var myAjax = new Ajax.Request(
							page,
							{
								method: 'post',
								parameters: params,
								debug : false,
								onComplete: function (requestObject,JSON) {
									$(settingDiv).insert ({bottom:requestObject.responseText});
									obj.hide()
//									regExp = new RegExp ('_addNew')
//									deleteRowsByRegularExpression (currentTableObj,regExp)


									}
							});

					return
				}


				function loadAddVariantRow(obj,variableToEdit) {

					var currentDiv = obj.up('.setting-variants-addvariantlink');

						var page = '/webservices/settingsWS.cfc?wsdl&method=getAddVariantRows'
						var params = 'returnFormat=plain&variablename='+variableToEdit;

						// changed to post to ensure proper refresh
						var myAjax = new Ajax.Request(
							page,
							{
								method: 'post',
								parameters: params,
								debug : false,
								onComplete: function (requestObject) {
									$(currentDiv).insert ({after:requestObject.responseText});

									}
							});

					return
				}

				<!--- 2015-05-13	WAB	Some work to support variants better. (During CASE 444527)  --->
				function setFieldName(valueObj,settingField,variant) {
					var variants = $(settingField).name.split('|');
					var name = '';
					var variantFoundInName = false

					for(var i=0; i<variants.length; i++) {
						if (i == 0) {
							fieldName = variants[i];
						} else {
							variantValue = variants[i].split(delim);
							if (variantValue[0] != '')	{
								fieldName = fieldName+'|'+variantValue[0]
								if (variantValue[0] == variant) {
									variantValue[1] = $F(valueObj);
									variantFoundInName = true;
								}
								fieldName = fieldName+delim+variantValue[1]
							}
						}
					}
					if (variantFoundInName != true) {
						fieldName = fieldName+'|'+variant+delim+$F(valueObj);
					}

					$(settingField).name = fieldName;
				}
			</script>

			<form id="settingForm"  method=post noValidate="true" enctype="multipart/form-data">
				<cfoutput><CF_INPUT type="hidden" name="getWholeSection" value="#getWholeSection#"></cfoutput>
				<cfset request.fieldslist = "">
				<cf_relayFormDisplay>
					<!--- <tr><th valign="top" colspan="2">Variable</th><th valign="top">Value [Default]</th></tr> --->

					<cfif mode neq "person">
						<cfset  rows = settingsObj.getEditorRowsForSetting(variablename = variableToEdit,getWholeSection = getWholeSection)>
						<cfoutput>#rows#</cfoutput>
					<cfelse>
						<cfset  rows = settingsObj.getEditorRowsForSetting(variablename = variableToEdit,getWholeSection = getWholeSection,personMode = true,personid = request.relaycurrentuser.personid)>
						<cfoutput>#rows#
						<cfparam name="setSettingForPersonID" default="#request.relayCurrentUser.personID#">
						<CF_INPUT type="hidden" name="setSettingForPersonID" value="#setSettingForPersonID#">
					</cfoutput>
					</cfif>


					<div class="form-group">
						<input class="btn btn-primary" type="submit" value = "Update">
					</div>
					<CF_INPUT type="hidden" name="SettingFieldList"  value = "#request.fieldsList#">
				</cf_relayFormDisplay>
			</form>

			<script>
			initialiseRelayFormValidation($('settingForm'),{applyValidationErrorClass:true,showValidationErrorsInline:true,autoRefreshRequiredClass:true})
			</script>
</cfif>