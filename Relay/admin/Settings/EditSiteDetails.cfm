<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		EditSiteDetals

		WAB May 2006

		PURPOSE

		To Edit the SiteDef table and memory structures

		Used as a test for the relayForm tag

		--------------------------------------------------------------------------
		VERSION HISTORY

		--------------------------------------------------------------------------
		ATTRIBUTES

		Modifications
		2008/07/22		NJH		CR-TND557 Make a subset of fields visible to the client to give them some control over the editing of site details.
								ShowFields is the variable used to display what shows. If it's "All" (set when user has AdminTask:level3) or if it contains
								the fieldname (which will contain a list of fieldnames when the user has sysAdminTask:level3), then display the field.
		2008/11/14		NJH		All Sites Bug Fix Issue 1345 - added a hidden form field to carry the value of the disabled domains, as cfupdate was setting them
								to be an empty string. Also removed the fieldSource value from the text input.


--->
<div id="editSiteDetailsContainer" class="paddingRightLeft">
	<!--- <h2>phr_admin_editsitestructure</h2> --->

<cfset application.com.request.setDocumentH1(text="Sites")>

<cfset TableRequired = "sitedef">
<cfinclude template = "\relay\templates\qryGetTableDefinition.cfm">

<!--- updater --->
<cfif isDefined("sitedeffieldlist")>

	<!--- collect all the parameters together into single field --->
	<cfparam name="form.parameters" default= "">
	<cfset form.parameters = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldPrefix="Parameter",appendTo=form.parameters)>


	<cfupdate datasource="#application.sitedatasource#" tablename="siteDef" dbtype="ODBC" formfields="#sitedeffieldlist#">
</cfif>

<!--- reload into memory if required --->
<cfif isdefined("updateAndReload")>
	<cfset application.com.relaycurrentsite.loadSiteDefinitions()>
	<cfoutput>reloaded</cfoutput>
</cfif>


<!--- Get all sites --->
<CFQUERY NAME="getSiteDefs" datasource="#application.siteDataSource#">
select * from siteDef
</cfquery>

<cfset columns = getSiteDefs.columnList>
<cfset columns = listdeleteat(columns,listfindnocase(columns,"sitedefid"))>
<cfset columns = listdeleteat(columns,listfindnocase(columns,"name"))>
<cfset columns = listappend("siteDefID",columns)>
<cfset columns = listappend("name",columns)>

<cf_tableFromQueryObject
	queryObject ="#getSiteDefs#"
	useInclude = false
	showTheseColumns = "SiteDefID,Name,Title,isInternal,ReciprocalSite"
	keycolumnList = "sitedefid,name"
	keyColumnURLList = "editsitedetails.cfm?frmSiteDefID=thisURLKey,editsitedetails.cfm?frmSiteDefID=thisURLKey"
	keyColumnKeyList = "sitedefid,sitedefid"
>



<cfif isDefined("frmSiteDefID")>
	<h2>phr_editsitedetails_heading2</h2>
	<!--- NJH 2008/07/21 CR-TND557 start --->
	<cfset showFields = "">
	<!--- if the user has adminTask:Level3, show all the fields --->
	<cfif application.com.login.checkInternalPermissions("adminTask","Level3")>
		<cfset showFields = "All">
	<!--- if the user has sysAdminTask:level3, show only certain fields --->
	<cfelseif application.com.login.checkInternalPermissions("sysAdminTask","Level3")>
		<cfset showFields = "SiteOffline,LiveLanguageIDs">
	</cfif>
	<!--- NJH 2008/07/21 CR-TND557 end --->

	<CFQUERY NAME="getSiteDef" datasource="#application.siteDataSource#">
	select * from siteDef where siteDefID =  <cf_queryparam value="#frmSiteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

		<cfset request.relayFormDisplayStyle = "html-table">
		<cf_relayFormDisplay >
		<!--- NJH 2009/03/27 Bug Fix Sony Issue 2017 - added scriptsrc to use js error messages. --->
		<cfform  method="POST" name="mainForm" >

					<CF_relayFormElementDisplay class="form-control" relayFormElementType="hidden" label="" currentValue="#frmSiteDefID#" fieldName="frmSiteDefID">
					<CF_relayFormElementDisplay class="form-control" relayFormElementType="text" label="ID" currentValue="#getSiteDef.SiteDefID#" fieldSource = "siteDef" fieldName="SiteDefID" disabled=true>
					<CF_relayFormElementDisplay class="form-control" relayFormElementType="hidden" label="" currentValue="#getSiteDef.SiteDefID#" fieldName="SiteDefID">


					<CF_relayFormElementDisplay class="form-control" relayFormElementType="hidden" label="" currentValue="#getSiteDef.name#" fieldName="name">
					<CF_relayFormElementDisplay class="form-control" fieldSource = "siteDef" fieldName="Name" relayFormElementType="text" currentValue="#getSiteDef.name#"  label="Name" size=30 disabled=true
							noteText= "This is the name of the site for internal purposes"
					>

					<!--- 	2013-02-25	WAB	alter to use application.environmentLookup --->
					<cfset to = structCount(application.environmentLookup) - 1>
					<cfloop index="testSite" from="0" to="#to#">
						<cfquery name="getSiteDomainDefs" datasource="#application.siteDataSource#">
							select domain + case when mainDomain = 1 then '*' else ''end as domain from siteDefDomain
							where siteDefID =  <cf_queryparam value="#frmSiteDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
								and testSite =  <cf_queryparam value="#testSite#" CFSQLTYPE="CF_SQL_INTEGER" >
							order by domain
						</cfquery>
						<cfif getSiteDomainDefs.recordCount>
							<cfset abbr = application.environmentLookup[testSite].abbr>
							<CF_relayFormElementDisplay class="form-control" fieldname="Domains#abbr#" label="Domains#abbr#" relayFormElementType="HTML" currentValue="#valueList(getSiteDomainDefs.domain,'<br>')#" noteTextposition="bottom" notetext="<span class='required'>#iif(getSiteDomainDefs.recordCount,DE('* indicates the main domain'),DE(''))#</span>">
							<!--- <cfset request.relayForm.fieldlist.sitedef = listAppend(request.relayForm.fieldlist.sitedef,"Domains#abbr#")> --->
						</cfif>
					</cfloop>

			<!--- NJH 2008/07/21 CR-TND557 --->
			<cfif (listFindNoCase(showFields,"unsubscribeHandler")) or (showFields eq "All")>
					<CF_relayFormElementDisplay class="form-control" fieldSource = "siteDef" fieldName="unsubscribeHandler" relayFormElementType="select" currentValue="#getSiteDef.unsubscribeHandler#"  label="Unsubscribe Handler Type"
							noteText= "Type of unsubscribe Handler" noteTextposition="right" list = "element|Go to an Element named 'Unsubscribe Handler',screen|Go to a Screen named 'Unsubscribe Handler',singleclick"  nullText = "Default"
					>
			</cfif>

					<cfloop index = "column" list = "#columns#">
						<cfif listFindNoCase(request.relayForm.fieldlist.sitedef,column) is 0  and column is not "parameters">  <!--- ie don't do if already done above --->

							<cfset ElementType = "text">
							<cfif listfindnocase ("int,bit,smallint,real,decimal,money",siteDefDef.columns[column].type_name) >
								<cfset ElementType = "numeric">
								<cfif listfindnocase ("bit",siteDefDef.columns[column].type_name)>
									<cfset ElementType = "radio">

									<cf_querysim>
									aQuery
									display,value
									Yes|1
									No|0
									</cf_querysim>
								</cfif>
							</cfif>

					<!--- NJH 2008/07/21 CR-TND557 Show only those columns to users that have adminTask:level3 or sysAdminTask:level3
						If it's not visible, set the form field as a hidden field.
					 --->
					<cfif (listFindNoCase(showFields,column)) or (showFields eq "All")>
							<cfif elementType is "radio">
								<CF_relayFormElementDisplay class="form-control" query=#aquery# relayFormElementType="#ElementType#" currentValue="#getSiteDef[column]#" fieldSource = "siteDef" fieldName="#column#" label="#column#" size=100>
							<cfelse>
								<CF_relayFormElementDisplay class="form-control" relayFormElementType="#ElementType#" currentValue="#getSiteDef[column]#" fieldSource = "siteDef" fieldName="#column#" label="#column#" size=100 maxlength = #siteDefDef.columns[column].length#>
							</cfif>
					<cfelse>
						<CF_relayFormElementDisplay class="form-control" relayFormElementType="hidden" currentValue="#getSiteDef[column]#" fieldSource = "siteDef" fieldName="#column#" label="#column#">
					</cfif>


						</cfif>

					</cfloop>


		<!---  Deal with any additional parameters in the parameters field--->
		<cfset parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(getSiteDef.parameters,",")>
		<cfif structcount(parametersStruct)>

					<cfloop item="item" collection=#parametersStruct#>
						<cfset value = parametersStruct[item]>

					<!--- NJH 2008/07/21 CR-TND557 --->
					<cfif (listFindNoCase(showFields,item)) or (showFields eq "All")>
						<CF_relayFormElementDisplay class="form-control" relayFormElementType="text" currentValue="#value#" fieldSource = "" fieldName="parameter_#item#" label="#item#" size=100 maxlength = 100>
					<cfelse>
						<CF_relayFormElementDisplay class="form-control" relayFormElementType="hidden" currentValue="#value#" fieldSource = "" fieldName="parameter_#item#" label="#item#">
					</cfif>
					</cfloop>
		</cfif>

			<!--- NJH 2008/07/21 CR-TND557 --->
			<cfif (listFindNoCase(showFields,"parameters")) or (showFields eq "All")>
						<CF_relayFormElementDisplay class="form-control" relayFormElementType="text" currentValue="" fieldSource = "siteDef" fieldName="parameters" label="Additional Parameters" size=100 maxlength = 100
							noteText = "(enter as variable1=value1,variable2=value2)" noteTextposition="bottom"
						>
			</cfif>


					<cfoutput><CF_INPUT type = "hidden" name = "siteDefFieldList" value = "#request.relayForm.fieldlist.sitedef#">

						<CF_relayFormElementDisplay class="btn btn-primary"  relayFormElementType="submit" fieldSource = "" fieldName="update" currentvalue="Update Database" label="">
						<CF_relayFormElementDisplay class="btn btn-primary" relayFormElementType="submit" fieldSource = "" fieldName="updateAndReload" currentvalue="Update Database and Reload into Memory" label="">

					</cfoutput>
			</cfform>
		</cf_relayFormDisplay>


		<cfset col = "domains#application.environmentLookup[application.testsite].abbr#">
<!--- <cfdump var="#application.siteDefByID[getSiteDef.siteDefID]#"> --->


</cfif>
</div>