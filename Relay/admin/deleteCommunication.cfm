<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Delete Communication</cf_title>
</cf_head>
<!--- <SCRIPT type="text/javascript">
	function submit() {
	var Form = document.mainForm
	form.frmGetRecs.value =
	}
</script>
 --->
<cfquery name="getCommRecs" datasource="#application.SiteDataSource#" maxrows=100>
	select *, title+' - '+convert(varchar,commID) as listTitle
	from communication order by title
</cfquery>


<cfform method="POST" name="mainForm">
	<cfparam name="frmCommID" default="0">
	<div class="form-group">
		<cfselect name="frmCommID" query="getCommRecs" value="commID" display="listTitle" selected="#frmCommID#" class="form-control"></cfselect>
		<!--- <CFINPUT TYPE="Text" NAME="frmCommID" VALUE="#frmCommID#" REQUIRED="No" TABINDEX="1"> --->
	</div>
	<input type="submit" name="frmGetRecs" value="Delete This Communication" class="btn btn-primary">
	<!--- <INPUT TYPE="hidden" NAME="frmDeleteRecs" VALUE=""> --->
</cfform>

<cfif isdefined('frmCommID') and frmcommid neq 0>
	<!--- Delete the comm records if necessary --->
	<cfif isdefined('frmDeleteRecs') and isdefined('frmCommID') and frmcommid neq 0>
		<cfquery name="getCommRecs" datasource="#application.SiteDataSource#">
			select * from communication where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfquery name="getCommSelection" datasource="#application.SiteDataSource#">
			select * from commSelection where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfquery name="getPhoneAction" datasource="#application.SiteDataSource#">
			select * from phoneAction where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfquery name="getCommFlagGroup" datasource="#application.SiteDataSource#">
			select * from commFlagGroup where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<table><tr><td>Records deleted</td></tr></table>
	</cfif>


	<cfquery name="getCommRecs" datasource="#application.SiteDataSource#">
		delete from communication where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfquery name="getCommSelection" datasource="#application.SiteDataSource#">
		delete from commSelection where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfquery name="getPhoneAction" datasource="#application.SiteDataSource#">
		delete from phoneAction where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfquery name="getCommFlagGroup" datasource="#application.SiteDataSource#">
		delete from commFlagGroup where commid =  <cf_queryparam value="#frmCommID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfoutput>
		<table>
			<tr>
				<td>Communication #htmleditformat(frmCommID)# deleted</td>

			</tr>
		</table>
	</cfoutput>
</cfif>



