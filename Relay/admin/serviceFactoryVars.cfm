<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Purpose:	The runtime variables available via ServicPactory

Usage:	

This code came from http://www.rewindlife.com/archives/000069.cfm

They're undocumented functions that Cold Fusion uses internally 
so they may not exist or be accessible in a future version, 
but there's no security risk with them so they're not very likely to change.

getMaxInactiveInterval() returns the timeout set for the current application. 
If you want the server default timeout you can grab it from the runtime service.

The following is most of the information available to you from the runtime service.

UPDATE Ray brought up a very good point. Whenever you call any of the undocumented scope methods on an Application or Session reference it updates the last access timestamp and therefore returns undesirable results for getTimeSinceLastAccess() and other time related functions.

To call the function and not have it update the last access time you have to use reflection, as in this example.

      
<cfapplication name="sessionInfoTester" sessionManagement="true">

<cfset tracker = createObject(
                     "java", 
                     "coldfusion.runtime.SessionTracker")>

<cfset sessions = tracker.getSessionCollection(
                              application.applicationName)>

<cfscript>
   a = arrayNew(1);
   
   sessionClass = a.getClass().forName("coldfusion.runtime.SessionScope");
   timeMethod = sessionClass.getMethod("getTimeSinceLastAccess", a);

</cfscript>   

<cfoutput>
   <cfloop item="loopSession" collection="#sessions#">
      #loopSession#: 
      #timeMethod.invoke(sessions[loopSession], a)#
      <br />
   </cfloop>
</cfoutput>

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">ColdFusion Server Service Factory Vars</TD>
	</TR>
</TABLE>

<cfscript>
  factoryClass = CreateObject("java", "coldfusion.server.ServiceFactory");
  rt = factoryClass.runtimeService;
</cfscript>

<cfoutput>
<pre>
getApplicationMaxTimeout = '#htmleditformat(rt.getApplicationMaxTimeout())#'
getApplicationTimeout = '#htmleditformat(rt.getApplicationTimeout())#'
getAppServer = '#htmleditformat(rt.getAppServer())#'
getComMapperDir = '#htmleditformat(rt.getComMapperDir())#'
getDefaultCharset = '#htmleditformat(rt.getDefaultCharset())#'
getDefaultMailCharset = '#htmleditformat(rt.getDefaultMailCharset())#'
<!--- getMaxQueued = '#rt.getMaxQueued()#' 
getMinRequests = '#rt.getMinRequests()#'
getNumberSimultaneousRequests = '#rt.getNumberSimultaneousRequests()#'--->
getRegistryDir = '#htmleditformat(rt.getRegistryDir())#'
getRequestTimeoutLimit = '#htmleditformat(rt.getRequestTimeoutLimit())#'
getRootDir = '#htmleditformat(rt.getRootDir())#'
getSaveClassFiles = '#htmleditformat(rt.getSaveClassFiles())#'
getSessionMaxTimeout = '#htmleditformat(rt.getSessionMaxTimeout())#'
getSessionTimeout = '#htmleditformat(rt.getSessionTimeout())#'
getSlowRequestLimit = '#htmleditformat(rt.getSlowRequestLimit())#'
getTemplateCacheSize = '#htmleditformat(rt.getTemplateCacheSize())#'
getWhitespace = '#htmleditformat(rt.getWhitespace())#'
isApplicationEnabled = '#htmleditformat(rt.isApplicationEnabled())#'
isCachePaths = '#htmleditformat(rt.isCachePaths())#'
isPureJavaKit = '#htmleditformat(rt.isPureJavaKit())#'
isSessionEnabled = '#htmleditformat(rt.isSessionEnabled())#'
isTrustedCache = '#htmleditformat(rt.isTrustedCache())#'
logSlowRequests = '#htmleditformat(rt.logSlowRequests())#'
timeoutRequests = '#htmleditformat(rt.timeoutRequests())#'
useJ2eeSession = '#htmleditformat(rt.useJ2eeSession())#'
</pre>
</cfoutput>

<cfdump var='#rt.getApplets()#' label='getApplets'>
<cfdump var='#rt.getApplications()#' label='getApplications'>
<cfdump var='#rt.getCfxtags()#' label='getCfxtags'>
<cfdump var='#rt.getCorba()#' label='getCorba'>
<cfdump var='#rt.getCustomtags()#' label='getCustomtags'>
<cfdump var='#rt.getErrors()#' label='getErrors'>
<cfdump var='#rt.getLocking()#' label='getLocking'>
<cfdump var='#rt.getMappings()#' label='getMappings'>
<cfdump var='#rt.getRequestSettings()#' label='getRequestSettings'>
<cfdump var='#rt.getResourceBundle()#' label='getResourceBundle'>





