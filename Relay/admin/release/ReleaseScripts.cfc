<!--- �Relayware. All Rights Reserved 2014 --->
<!---
cfc for running release scripts
is extended by webservices\releaseScripts.cfc


WAB 2013-01-31 modified fileFindAndreplace to optionally bring back an array of matches
NJH 2016/02/11 record when the release script was run and if it was successful or not.
WAB 2016-03-10	Added two new arguments to the findReplace.  escapeRegExp and ignoreWhitespace.  See hints for details
				Also added a subDirectory argument
WAB 2016-06-29	PROD2016-1342  RunAllScripts function.  Changed its name and added support for runAutomatically=false attribute on functions.
				Changed UI so that last result always displayed on screen.  This required me to bring some JS logic around messages and classnames into CF so could be used both when first displaying page and during ajax updates
				Changed a few other function and argument names
 --->

<cfcomponent>

	<cffunction name="runScript" access="remote" returntype="struct" output=false>
		<cfargument name="componentName" required="true">
		<cfargument name="functionName" required="true">

		<cfset var result = {}>
		<cfset var argumentCollection = structCopy (arguments)>
		<cfset structDelete (argumentCollection,"componentname")>
		<cfset structDelete (argumentCollection,"functionname")>

		<cftry>
			<cfinvoke component="#componentName#" method="#functionName#" attributecollection="#argumentCollection#" returnvariable="result">

			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_ColdFusion(error = cfcatch)>
				<cfset result = {isOK = false,message="",error = cfcatch.message}>
			</cfcatch>
		</cftry>

		<cfset var theComponentArray = getComponentArray(name=replaceNoCase(componentName,"releaseScripts.",""))>

		<cfif arrayLen(theComponentArray)>
			<cfset var theComponent = theComponentArray[1]>

			<cfset var releaseScriptMetaData = {functionRevision=1,runOnEachInstance=false,runOnNewDatabase=false,runOnEachUpgrade=false}>
			<cfset var functionMetaDataArray = StructFindValue(theComponent,arguments.functionName,"one")>

			<cfif arrayLen(functionMetaDataArray)>
				<cfset releaseScriptMetaData = functionMetaDataArray[1].owner>
			</cfif>

			<!--- record the running of the release script. If the script has been run once before but has errored, then we allow an update. --->
			<cfquery >
				<cfif not hasReleaseScriptBeenRunSuccessfully(releaseScriptComponent=theComponent,functionName=arguments.functionName)>
					insert into releaseHistory (type,name,revision,lastUpdated,functionRevision,created,coldFusionInstanceID,isOK,metaData,databaseName)
						values
						('CF'
						,<cf_queryparam value="#arguments.functionName#" cfsqltype="cf_sql_varchar">
						,<cf_queryparam value="#application.com.relayCurrentSite.getSiteFriendlyVersionString()#" cfsqltype="cf_sql_varchar">
						,getDate()
						,<cf_queryparam value="#releaseScriptMetaData.functionRevision#" cfsqltype="cf_sql_integer">
						,getDate()
						,<cf_queryparam value="#application.instance.coldFusionInstanceID#" cfsqltype="cf_sql_integer">
						,<cf_queryparam value="#result.isOK#" cfsqltype="cf_sql_bit">
						,<cf_queryparam value="#serializeJson(result)#" cfsqltype="cf_sql_varchar">
						,db_name()
						)
				<cfelse>
					update releaseHistory set
						isOK=<cf_queryparam value="#result.isOk#" cfsqltype="cf_sql_bit">
						,lastUpdated=getDate()
						,revision=<cf_queryparam value="#application.com.relayCurrentSite.getSiteFriendlyVersionString()#" cfsqltype="cf_sql_varchar">
						,metaData = <cf_queryparam value="#serializeJson(result)#" cfsqltype="cf_sql_varchar">
					where name=<cf_queryparam value="#arguments.functionName#" cfsqltype="cf_sql_varchar">
						and type='CF'
						and functionRevision=<cf_queryparam value="#releaseScriptMetaData.functionRevision#" cfsqltype="cf_sql_integer">
						and isOk != 1
						<cfif releaseScriptMetaData.runOnEachInstance>and coldFusionInstanceID = <cf_queryparam value="#application.instance.coldFusionInstanceID#" cfsqltype="cf_sql_integer"></cfif>
						<cfif releaseScriptMetaData.runOnNewDatabase>and databaseName = db_name()</cfif>
						<cfif releaseScriptMetaData.runOnEachUpgrade>and revision = <cf_queryparam value="#application.com.relayCurrentSite.getSiteFriendlyVersionString()#" cfsqltype="cf_sql_varchar"></cfif>
				</cfif>
			</cfquery>

		<cfelse>
			<cfset result = {isOK = false,message="Could not find component #componentName#"}>
		</cfif>
	
		<cfset AddClassEtcToResult(result)>

		<cfreturn result>
	</cffunction>


	<cffunction name="AddClassEtcToResult" hint="Takes the result structure and works out the message and class name to use in the UI.">
		<cfargument name="result" type="struct" required="true">	
		
		<cfset result.messages = []>
		<cfset var messageStruct = {}>

		<cfif result.isOK>
			<cfset var codeUpdated = (structKeyExists(result,"status") and result.status is "codeUpdated")>
			<cfset var devNeededMessage = codeUpdated?'CS Developer needed for committing the following code changes:<BR>':''>
			<cfset messageStruct.className  = codeUpdated?'infoblock':'successblock'>	
			<cfset messageStruct.content = devNeededMessage & (result.message EQ ""?"OK":result.message)>
			<cfset arrayAppend(result.messages,messageStruct)>
		</cfif>	
			
		<cfif not result.isOK>
			<cfset var updateNeeded = (structKeyExists(result,"status") and result.status is "updateNeed")>
			<cfset var devNeededMessage = updateNeeded?'CS Developer needed for code update:<BR>':'Release error! Product Developer needed: <BR>'>
			<cfset messageStruct.className  = updateNeeded?'warningblock':'errorblock'>	
			<cfset messageStruct.content = devNeededMessage & (structKeyExists (result,"error")?result.error:result.message)>				
			<cfset arrayAppend(result.messages,messageStruct)>
		</cfif>	

	</cffunction>
		
	<cffunction name="getComponentArray">
		<cfargument name="componentName" type="string" required="false">
		<cfargument name="activeOnly" type="boolean" default="true">
		<cfargument name="needRunningOnly" type="boolean" default="false">

		<cfset var componentFilter = structKeyExists(arguments,"componentName") and arguments.componentName neq ""?arguments.componentName:"*.cfc">
		<cfset var coreReleaseFiles = "">
		<cfset var clientReleaseFiles = "">

		<cfdirectory name = "coreReleaseFiles" directory = "#application.paths.relay#/releaseScripts"  filter="#componentFilter#">
		<cfdirectory name = "clientReleaseFiles" directory = "#application.paths.code#/releaseScripts" filter="#componentFilter#">

		<cfset var releaseFiles = "">
		<cfquery name="releaseFiles" dbtype="query">
			select * from coreReleaseFiles
			union
			select * from clientReleaseFiles
			order by name
		</cfquery>

		<!---
		get the metadata and run the functions in the cfc
		Sorry, rather nasty am doing code and output at the same time

		   --->
		<cfset var resultArray= arrayNew(1)>
		<cfset var resultStruct= {}>
		<cfloop query = "releaseFiles">
			<cfset var objectName = replace(replace(replace(replace("#directory#\#name#",".cfc",""),application.path,""),application.userfilesabsolutepath,""),"\",".","ALL")>
			<cfset objectName = rereplace(objectName,"\A\.","")>

			<cftry>
				<cfset var object = createObject("component",objectName)>
				<cfcatch>
					<cfoutput>Unable to load object #objectName#</cfoutput>
					<cfdump var="#cfcatch#">
					<CF_ABORT>
				</cfcatch>

			</cftry>

			<cfset var objectMetaData  = getMetaData(object)>

			<!--- Run functions in order specified by an INDEX attribute, this gets me an array that points to the functions in the right order
				also filters out any private functions
			--->
			<cfset var myStruct = structNew()>
			<cfset var functionArrayLen = arrayLen(objectMetaData.functions)>
			<cfset var functionIDX = 0>

			<cfloop index="functionIDX" from="1" to ="#functionArrayLen#">
				<cfset var theFunctionMetaData	= objectMetaData.functions[functionIDX]>
				<cfif (not StructKeyExists (theFunctionMetaData,"access") or theFunctionMetaData.access is "public")>
					<cfset myStruct[functionIDX] = structNew()>
					<cfset myStruct[functionIDX].name = theFunctionMetaData.name>
					<cfset myStruct[functionIDX].index = structKeyExists (theFunctionMetaData,"index")?theFunctionMetaData.index:functionIDX>
				</cfif>
			</cfloop>
			<cfset var FunctionsInOrderArray = structSort(mystruct,"numeric","ASC","index")>

			<cfset var thisObjectResult = structNew()>
			<cfset thisObjectResult.name = objectName>

			<cfset thisObjectResult.index = structKeyExists(objectMetaData,"index")?objectMetaData.index:500>
			<cfset thisObjectResult.active = structKeyExists(objectMetaData,"active")?objectMetaData.active:true>

			<!--- Default these metadata parameters at component level
				runOnNewDatabase - function to be run if database name changes
				runOnEachUpgrade - need to be run on each upgrade
				runOnEachInstance - need to be run on each cold fusion instance
			--->
			<cfset thisObjectResult.runOnNewDatabase = structKeyExists(objectMetaData,"runOnNewDatabase")?objectMetaData.runOnNewDatabase:false>
			<cfset thisObjectResult.runOnEachUpgrade = structKeyExists(objectMetaData,"runOnEachUpgrade")?objectMetaData.runOnEachUpgrade:false>
			<cfset thisObjectResult.runOnEachInstance = structKeyExists(objectMetaData,"runOnEachInstance")?objectMetaData.runOnEachInstance:false>

			<cfif not thisObjectResult.active>
				<cfset thisObjectResult.index = thisObjectResult.index + 1000>
			</cfif>

			<cfif structKeyExists (objectMetaData,"hint") and objectMetaData.hint is not "">
				<cfset thisObjectResult.hint = objectMetaData.hint>
			</cfif>
			<cfset thisObjectResult.functions = arrayNew(1)>
			<cfset var FunctionsInOrderArrayLen = arrayLen(FunctionsInOrderArray)>
			<cfset var orderIDX = 0>


			<cfif activeOnly and not thisObjectResult.active >
				<cfcontinue>
			</cfif>

			<cfloop index="orderIDX" to="#FunctionsInOrderArrayLen#" from = "1" >
				<cfset var theFunctionMetaData	= objectMetaData.functions[FunctionsInOrderArray[orderIDX]]>

				<cfset theFunctionMetaData.functionRevision = structKeyExists(theFunctionMetaData,"functionRevision")?theFunctionMetaData.functionRevision:1><!--- NJH used if function has been modified in some way and needs re-running --->
				<cfset theFunctionMetaData.runAutomatically = structKeyExists(theFunctionMetaData,"runAutomatically")?theFunctionMetaData.runAutomatically:true>

				<!--- Any metadata parameters not specifically set are inheritent from the containing component --->
				<cfset structAppend(theFunctionMetaData,getHeritableMetaData(thisObjectResult),false)>
				<cfset arrayAppend (thisObjectResult.functions, {metaData =  theFunctionMetaData})>				

				<cfset theFunctionMetaData.needsRunning =	doesReleaseScriptNeedRunning(releaseScriptComponent=thisObjectResult,functionName=theFunctionMetaData.name)>
				<cfset theFunctionMetaData.lastRunInfo = getFunctionLastRun(releaseScriptComponent=thisObjectResult,functionName=theFunctionMetaData.name, successful = false)>

				<cfif needRunningOnly and not theFunctionMetaData.needsRunning>
					<cfset arrayDeleteAt (thisObjectResult.functions,arrayLen(thisObjectResult.functions))>
					<cfcontinue>
				</cfif>

			</cfloop>

			<cfset resultStruct[objectName] = thisObjectResult>
		</cfloop>

		<cfset var ComponentsInOrderArray = structSort(resultStruct,"numeric","ASC","index")>

		<!--- only return components that have functions --->
		<cfloop index="orderIDX" from=1 to = #arrayLen(componentsInOrderArray)#>
			<cfif arrayLen(resultStruct[componentsInOrderArray[orderIDX]].functions) gt 0>
				<cfset arrayAppend(resultArray,resultStruct[componentsInOrderArray[orderIDX]])>
			</cfif>
		</cfloop>

		<cfreturn resultArray>

	</cffunction>

	<!--- These are standard functions which can be used by any release script --->

	<!---
	Do search and replace on phrases
	Pass in an array of things to search for

		<cfset searchReplaceArray = Arraynew (1)>
		<cfset searchReplaceArray[1] = {sqllike='%relay_includetemplate%content%',find='(relay_includetemplate.*?template=["'']{0,1})(content)([^\s]*)',replace="\1code\3"}>

	array Item has 3 keys:
		sqlLike = something to go into a like statement to return the correct records
		find = a regular expression to find
		replace = the replace expression

	WAB 2012-03-03 Added entityTypeID argument which can be used to speed up search if appropriate
	 --->

	<cffunction name="updatePhrases">
		<cfargument name="searchReplaceArray" type="array">
		<cfargument name="entityTypeID" type="numeric">
		<cfargument name="whereClause" type="string" default=""> <!--- NJH 2016/06/14 - added for JIRA PROD2016-347 - filter the records that we want to potentially update --->

			<cfset var result = {isOK=true,message="OK",error=""}>
			<cfset var getMatchingPhrases = "">
			<cfset var arrayItem = "">
			<cfset var tempPhraseText = "">
			<cfset var replacementMade = false>
			<cfset var thisSQLContains = "">
			<cfset var thisSQLLike = "">

			<cfquery name="getMatchingPhrases" datasource="#application.sitedatasource#">
			select p.phraseText, p.phraseid, p.ident,
			case when p.entityID <> 0 then p.phraseTextID + '_' + entityname + '_' + convert(varchar,p.entityID) else p.phrasetextID end as phraseIdentifier
			from vphrases p
				inner join schemaTable s on s.entityTypeID = p.entityTypeID
				inner join phrases on phrases.phraseID = p.phraseID
			where
				(1=0
				<cfloop index="arrayItem" array="#searchReplaceArray#">
					<cfif structKeyExists(arrayItem,"sqllike")><!--- WAB 2011/07/08  sql like can be a piped list of ORs, escape double open square brackets  --->
						<cfloop index="thisSQLLike" list = "#arrayItem.sqllike#" delimiters="|">
							<cfset thisSQLLIke = replace(thisSQLLike,"[[","[[][[]","ALL")>
							OR
							p.phraseText  like  <cf_queryparam value="%#thisSQLLike#%" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfloop>
					</cfif>
					<cfif structKeyExists(arrayItem,"sqlcontains")>
						<cfloop index="thisSQLContains" list = "#arrayItem.sqlcontains#" delimiters="|">
							OR
							CONTAINS(phrases.phraseText, '#thisSQLContains#')
						</cfloop>
					</cfif>
				</cfloop>
				)
				<cfif structKeyExists(arguments,"entityTypeID")>
				AND p.entityTypeID = #arguments.entityTypeID#
				</cfif>
				<cfif arguments.whereClause neq "">
				and #preserveSingleQuotes(arguments.whereClause)#
				</cfif>
			</cfquery>

			<cfset result.message = "#getMatchingPhrases.recordCount# phrases found<BR>">

			<cfloop query="getMatchingPhrases">

				<cfset tempPhraseText = phraseText>
				<cfset replacementMade = false>
				<cfloop index="arrayItem" array="#searchReplaceArray#">
					<cfif arrayItem.find is not "">
						<cfset findArray = application.com.regExp.reFindAllOccurrences(arrayItem.find,tempPhraseText)>
							<cfif arrayLen(findArray)>
								<cfset replacementMade = true>
								<cfloop index="foundItem" array="#findArray#">
									<cfset result.message = result.message & "FOUND: " & foundItem.string & "<BR>">
								</cfloop>
								<cfset tempPhraseText = reReplaceNoCase (tempPhraseText,arrayItem.Find,arrayItem.Replace,"ALL")>
							</cfif>
					<cfelse>
						<cfset result.isOK = false>
					 	<cfset result.error = result.error & "#phraseIdentifier# contains #arrayItem.sqlLike#<BR>" >
					</cfif>
				</cfloop>

				<cfif replacementMade>

					<cfquery name="updatePhrase" datasource = "#application.sitedatasource#">
					update phrases set phrasetext =  <cf_queryparam value="#tempPhraseText#" CFSQLTYPE="CF_SQL_VARCHAR" >  where ident =  <cf_queryparam value="#ident#" CFSQLTYPE="CF_SQL_Integer" >
					</cfquery>
				 	<cfset result.message = result.message & "#phraseIdentifier# updated to #htmleditformat(tempPhraseText)# <BR>">

					<cfset application.com.relayTranslations.resetPhraseStructureKey(phraseIdentifier)>
				</cfif>

			</cfloop>



		<cfreturn result>

	</cffunction>


	<!--- WAB 2012-06-15 brought this function from original FNL088 releaseScript file so could be used elsewhere
	The each item in the FindAndReplaceArray is a structure {find="a regexp", replace="a replace expression"}
		It can be created like this
		<cfset  var findAndReplaceArray = ArrayNew(1)>
		<cfset var tempStruct = structNew()>
		<cfset tempStruct = {find="<CFABORT>",replace="<CF_ABORT>"}>
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>
		<cfset tempStruct = {find="<CFFLUSH>",replace="<CF_FLUSH>"}>
		<cfset arrayAppend (findAndReplaceArray,tempStruct)>

		The structure can have an extra key: type. This can take the values variable, path or null
		This allows you to do searches for variable names without worrying about the #s

	--->

	<cffunction name="FileFindAndReplace" hint="Search For and Replace Variables in Code or Content Directories" access="public">
		<cfargument name="FindAndReplaceArray" type="array">
		<cfargument name="filter" default="*.cf*">
		<cfargument name="relativeTo" default="code" hint = "list containing code,content or both or any other key of application.paths">
		<cfargument name="subdirectory" default="" hint = "include the \">
		<cfargument name="returnFoundArray" default="false"> <!--- WAB 2013-01-31 added this parameter to return full details of items found --->
		<cfargument name="escapeRegExp" default = "false"  hint = "Automatically escape the regular expression.  If you want a group for back referencing use \(  \).  Note that this property can be overridden on an individual item basis, just add to structure in findReplaceArray">
		<cfargument name="ignoreWhiteSpace" default = "false" hint = "Replaces all whitespace in your regular expression with \s+.  Note that this property can be overridden on an individual item basis, in findReplaceArray ">

		<cfset var result = {isOK=true,message="",error="",detail=structNew(),status=""}>
		<cfset var thisResult = {}>

		<cfloop list="#arguments.relativeTo#" index="tmpRelativeTo">
			<cfdirectory action="list" directory="#application.paths[tmpRelativeTo]##subdirectory#" filter="#arguments.filter#" recurse="true" name="files" >

			<cfloop query="files">
				<cfif not findNoCase(".svn",directory)>
					<cffile action="read" file="#directory#\#name#" variable="fileContent">
					<cfset shortfilename ='#replace(replaceNoCase(directory,application.paths[tmpRelativeTo],"#tmpRelativeTo#","ONCE"),"\","/","ALL")#/#name#'>  <!--- WAB 2013-06-24 change \ to / to prevent problems when converted to JSON (because Key names are not encoded) --->
					<cfset newFileContent = fileContent>
						<cfloop array="#FindAndReplaceArray#" index="replaceStruct">
								<cfset structAppend(replaceStruct, {escapeRegExp = escapeRegExp, ignoreWhiteSpace = ignoreWhiteSpace},false)>
								<cfset thisResult = replaceVars(content=newfileContent,argumentCollection= replaceStruct)>
								<cfset newFileContent = thisresult.content>
								<cfif not thisResult.isOK>
									<!--- Item found but not auto-replaced --->
									<cfset result.isOK = false>
									<!--- code has been found. This this requires a CS developer to make necessary changes and commit into the repository --->
									<cfset result.status = "updateNeeded">

									<cfif not structKeyExists(thisResult,"found")>
										<cfset result.error = result.error & "<BR>#shortfilename# contains #htmlEditFormat(replaceStruct.find)#">
									<cfelse>
										<cfset result.error = result.error & "<BR>#shortfilename# contains ">

										<cfparam name="result.detail[shortfilename]"  default= #arrayNew(1)#>
										<cfloop array="#thisResult.found#" index="item">
											<cfset arrayAppend(result.detail[shortfilename], item)>
											<cfset result.error = result.error & "<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;#item.string#" >
										</cfloop>

									</cfif>
								</cfif>
						</cfloop>


					<cfif fileContent neq newFileContent>
						<!--- <cfoutput><div style="float:left"><pre>#fileContent#</pre></div><div style="float:left"><pre>#newfileContent#</pre></div></cfoutput> --->
						<cffile action="write" file="#directory#\#name#" output="#newFileContent#" addNewLine="false">
						<cfif not find("#shortfilename# updated.",result.message)>
							<cfset result.message = result.message & "<BR>#shortfilename# updated.">
						</cfif>
						<!--- Code has been updated. This this requires a CS developer to commit the changes into the repository. However, don't set the status if the status has already been set to "updateNeeded" as the updateNeeded should take precedence --->
						<cfif result.status eq "">
							<cfset result.status = "codeUpdated">
						</cfif>
					</cfif>

				</cfif>

			</cfloop>
		</cfloop>

		<cfif result.message is "">
			<cfset result.message = "No Replacements Made">
			<!--- if we found stuff but no changes made, generally an indication that a developer needs to do something. So, set the status to updateNeeded which informs the release team that a CS developer needs to get involved --->
			<!--- <cfif not result.isOk>
				<cfset result.status = "updateNeeded">
			</cfif>
		<cfelse>
			<!--- code has been found. This this requires a CS developer to make necessary changes and commit into the repository --->
			<cfif structKeyExists(result,"error") and result.error contains " contains ">
				<cfset result.status = "updateNeeded">
			<cfelse>
				<!--- code has been updated. This this requires a CS developer to commit the changes into the repository --->
				<cfset result.status = "codeUpdated">
			</cfif> --->
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="replaceVars" access="private" returnType="struct">
		<cfargument name="find">
		<cfargument name="replace">
		<cfargument name="type" default = "">  <!--- variable, path or null.  Allows us to do slightly different regExp replaces --->
		<cfargument name="content">
		<cfargument name="escapeRegExp" default = "false"  hint = "Automatically escape the regular expression.  If you want a group for back referencing use \(  \).">
		<cfargument name="ignoreWhiteSpace" default = "false" hint = "Replaces all whitespace in your regular expression with \s+. ">
		<cfargument name="returnFoundArray" default="false">

		<cfset var newContent="">
		<cfset var startWith = "">
		<cfset var endWith = "">
		<cfset var findRegExp = "">
		<cfset var replaceRegExp = "">
		<cfset var result = {isOK = true,content = content }>
		<cfset var tempargs = {}>


		<cfif escapeRegExp>
			<cfset find = replace(find, "\(","OpenBrackeT","ALL")>
			<cfset find = replace(find, "\)","CloseBrackeT","ALL")>
			<cfset find = application.com.regexp.escapeRegExp(find) >
			<cfset find = replace(find, "OpenBrackeT","(","ALL")>
			<cfset find = replace(find, "CloseBrackeT",")","ALL")>
		</cfif>

		<cfif ignoreWhiteSpace>
			<cfset find = rereplace (trim(find), "\s+","\s*","ALL")>
		</cfif>

		<cfif type is "variable">
			<cfset startWith = "([\(\s\&##,])">
			<cfset endWith = "([\)\s\&##,>])"> <!--- 2011/05/06 added closing chevron--->
		<cfelseif type is "path">
			<cfset startWith = '([\\\/"##])'>
			<cfset endWith = startWith>
		</cfif>

			<cfset findRegExp = "#startWith#(?:#arguments.find#)#endWith#">  <!--- WAB 2011/03/29 was a problem here if the find had |s (ORs), now done better.  bit in middle is a non capturing group --->

		<cfif structKeyExists (arguments,"replace")>
			<cfif returnFoundArray>
				<cfset tempArgs = {regexp = findRegExp, string = content}>
				<cfset result.found = application.com.regExp.javamatch (argumentCollection=tempArgs)>
			</cfif>

			<cfif type is not "">
					<cfset replaceRegExp = "\1#replace#\2">
			<cfelse>
					<cfset replaceRegExp = "#replace#">
			</cfif>

			<cfset result.Content = reReplaceNoCase(arguments.content,findRegExp,replaceRegExp,"ALL")>

		<cfelse>
			<!--- we just do a search on this NJH/WAB 2013/01/28 - changed to use javaMatch to support negative lookahead (???)--->
			<!--- WAB 2013-01-31 returns the find array --->
			<cfset tempArgs = {regexp = findRegExp, string = content}>
			<cfset result.found = application.com.regExp.javamatch (argumentCollection=tempArgs)>

			<cfif arrayLen(result.found) is not 0>
			      <cfset result.isOK = false>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="RunAllScripts" access="remote" output=false>
		<cfargument name="forceList" type="string" default="" hint="list of methods for which to override runAutomatically=false">

		<cfset var needRunningReleaseScripts = getNeedRunningReleaseScripts()>
		<cfset var fileIdx = 0>
		<cfset var functionIDX = 0>
		<cfset var result = {}>


		<cfloop index="fileIdx" from="1" to="#arrayLen(needRunningReleaseScripts)#">
			<cfset var thisReleaseScript = needRunningReleaseScripts[fileIDx]>

			<cfset result[thisReleaseScript.name] = {}>

			<cfloop index="functionIDX" from=1 to = #arrayLen(thisReleaseScript.functions)#>
				<cfset var thisFunction = thisReleaseScript.functions[functionIDX]>
				<cfif thisFunction.metaData.runAutomatically OR (listFindNoCase (forceList,"#thisReleaseScript.name#.#thisFunction.metadata.name#") IS NOT 0)>
					<cfset result[thisReleaseScript.name][thisFunction.metadata.name] = runScript(componentName=thisReleaseScript.name,functionName=thisFunction.metadata.name)>
				<cfelse>
					<cfset result[thisReleaseScript.name][thisFunction.metadata.name] = {isOK = true, message = "Cannot be run automatically"}>
				</cfif>	
			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="getNeedRunningReleaseScripts" access="public" output="false" returnType="array">

		<cfreturn getComponentArray(needRunningOnly=true, activeOnly=true)>

	</cffunction>


	<cffunction name="doesReleaseScriptNeedRunning" output="false" access="public">
		<cfargument name="releaseScriptComponent" type="struct" required="true">
		<cfargument name="functionName" type="string" required="true">

		<cfreturn not hasReleaseScriptBeenRunSuccessfully(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="hasReleaseScriptBeenRunSuccessfully" access="public" output="false" returnType="boolean">
		<cfargument name="releaseScriptComponent" type="struct" required="true">
		<cfargument name="functionName" type="string" required="true">

		<cfset var functionLastRun = getFunctionLastRun(argumentCollection=arguments, successful = true)>

		<cfreturn functionLastRun.recordCount?true:false>
	</cffunction>


	<cffunction name="getFunctionLastRun" access="public" output="false" returnType="query">
		<cfargument name="releaseScriptComponent" type="struct" required="true">
		<cfargument name="functionName" type="string" required="true">
		<cfargument name="successful" type="boolean" default="true">

		<cfset var theReleaseScriptComponent = arguments.releaseScriptComponent>
		<cfset var releaseScriptMetaData = {}>
		<cfset var getFunctionLastRunQry = "">

		<cfset var functionMetaDataArray = StructFindValue(theReleaseScriptComponent,arguments.functionName,"one")>

		<cfif arrayLen(functionMetaDataArray)>
			<cfset releaseScriptMetaData = functionMetaDataArray[1].owner>

			<cfquery name="getFunctionLastRunQry">
				select lastUpdated,metaData
				from releaseHistory
				where name=<cf_queryparam value="#arguments.functionName#" cfsqltype="cf_sql_varchar">
					and type='CF'
					and functionRevision=<cf_queryparam value="#releaseScriptMetaData.functionRevision#" cfsqltype="cf_sql_integer">
					<cfif releaseScriptMetaData.runOnEachInstance>
						and coldFusionInstanceID = <cf_queryparam value="#application.instance.coldFusionInstanceID#" cfsqltype="cf_sql_integer">
					</cfif>
					<cfif releaseScriptMetaData.runOnEachUpgrade>
						and revision = <cf_queryparam value="#application.com.relayCurrentSite.getSiteFriendlyVersionString()#" cfsqltype="cf_sql_varchar">
					</cfif>
					<cfif releaseScriptMetaData.runOnNewDatabase>
						and databaseName = db_name()
					</cfif>
					<cfif successful>
					and isOk=1
					</cfif>
					<cfif not arrayLen(functionMetaDataArray)>
						and 1=0
					</cfif>
				order by lastUpdated Desc	
			</cfquery>

		</cfif>

		<cfreturn getFunctionLastRunQry>

	</cffunction>


	<cffunction name="getHeritableMetaData" access="public" output="false" returnType="struct" hint="Gets The Component Level Meta Data which can be inherited by a function">
		<cfargument name="componentMetaData" type="struct" required="true">

		<cfset var result = {}>

		<cfloop collection = #componentMetaData# item="item">
			<cfif listFindNoCase("runOnEachInstance,runOnNewDatabase,runOnEachUpgrade",item)>
				<cfset result[item] = componentMetaData[item]>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>
</cfcomponent>