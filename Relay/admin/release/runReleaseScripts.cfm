<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			runReleaseScripts.cfm
Author:				WAB
Date started:		2010-10-06

Description:
This template is used to run release scripts
It allows us to run release scripts without having to open up specific files within RACE
Also means that release notes only have to ever reference this single file
The release scripts are functions within cfcs in the relay\releaseScripts or content\releaseScripts directories
Each function should return a structure containing isOK(=true) and message.  Or if there is a failure, isOK=false and error.
If the function has a hint it is displayed
If the function is private it is, naturally, not run
Since the getMetaData returns the functions in a random order, we process them in alphabetical order.  For some release scripts you may need to name the functions appropriately
Can handle more than one release file in the release scripts directory - again all files done alphabetically



Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-02-21 	WAB Added a spinner and a close (X) link to the messages
2014-10-16 	SB Changed the id and classes of the tables for the internal look and feel changes.
2016-01-13 WAB	CFAdmin Password now stored in db so shouldn't need to get password, but keep option of entering it (perhaps on dev boxes where people have own password)
2016/02/11	NJH	Don't show release script if already been run successfully and requires only a single run. Help show what actually needs doing still
WAB 2016-06-29	PROD2016-1342  Changed UI so that last result always displayed on screen.  This required me to bring some JS logic around messages and class inot CF

Possible enhancements:
Improve results layout
Put Try/Catch around each call
Could log when this is run and only allow to be run once
--->


<!---
 Look for cfcs in the releaseScripts directory
--->
<cfset application.com.request.setDocumentH1(text="Release Scripts")>

<cf_includeJavascriptOnce template ="/javascript/prototypeSpinner.js">
<cf_includeJavascriptOnce template ="/javascript/tableManipulation.js">

<!--- Check whether we have a valid cf password.  If we do then there is not need to worry about collecting it --->
<cfset haveCFPassword = application.com.cfadmin.checkCFPassword()>

<cfparam name="showInActive" default="false">
<cfparam name="showRun" default="false">
<cfoutput>
<script>

	var haveCFPassword = #haveCFPassword#;

	function runScript(obj,componentName,functionName,options) {
		var page = '/webservices/runReleaseScripts.cfc?wsdl&returnFormat=json&_cf_nodebug=true&method=runScript'
		var parameters = 'componentName='+componentName+'&functionName=' +functionName
		var table = getParentTable (obj)
		var row = getParentRow (obj)
		var content = ''

		//	delete any old rows
		deleteResultRows (table,componentName,functionName)

		if(options.cfpassword && !haveCFPassword){
			CFPassword = $('CFPassword').value
			if (CFPassword == '') {
				alert ('Please enter the CF Admin Password below')
				return
			}
			parameters += '&CFPassword='+CFPassword
		}

		obj.spinner({position:'left'})

		var myAjax = new Ajax.Request(
				page,
				{
					method: 'post',
					parameters:  parameters,
					asynchronous: true,
					onComplete: function  (requestObject,JSON) {
							obj.removeSpinner()
							var json = requestObject.responseText.evalJSON(true)
							json.MESSAGES.each (function (message) {
								addFullWidthRow (table,row.rowIndex + 1,componentName+functionName+'_success',message.CONTENT,{classname:message.CLASSNAME},0,0);
							});
		 			}
				}
			)

	}

	function deleteResultRows(table,componentName,functionName) {

		var regExp = new RegExp (componentName+functionName)
		deleteRowsByRegularExpression (table,regExp)

	}

	function deleteCurrentRow(obj) {

		var row = getParentRow (obj)
		var table = getParentTable (obj)
		table.deleteRow(row.rowIndex)

	}
</script>

<style>
.clickable {cursor:pointer;}
.warningblock {background-color:orange !important;color:black !important;}
.infoblock {background-color:yellow !important;color:black !important;}
</style>
</cfoutput>

<cfset releaseScriptObject = createObject ("component","releaseScripts")>

<cfif structKeyExists (url,"runAll")>
	<cfset x = releaseScriptObject.RunAllScripts()>
	<cfdump var="#x#">
</cfif>


<cfset functionArgs = {activeOnly=true}>
<cfif showInactive>
	<cfset functionArgs.activeOnly = false>
<cfelse>
	<cfset functionArgs.needRunningOnly= not showRun>
</cfif>
<cfset componentArray = releaseScriptObject.getComponentArray(argumentCollection=functionArgs)>

<h2>All <cfif showRun>Run </cfif><cfif showInactive>Inactive</cfif> phr_admin_releasescripts</h2>
<cfoutput>
<table id="runReleaseScriptsTable" class="table table-hover">
	<cfloop index="fileIdx" from="1" to="#arrayLen(componentArray)#">
		<cfset thisReleaseScript = componentArray[fileIDx]>

			<tr><th <cfif showRun>colspan="3"</cfif>>
			<span title="#application.com.security.sanitiseHTML(thisReleaseScript.Name)# ">
				<cfif structKeyExists (thisReleaseScript,"hint")>
					#application.com.security.sanitiseHTML(thisReleaseScript.hint)#
				<cfelse>
					#application.com.security.sanitiseHTML(thisReleaseScript.Name)#
				</cfif>
			</span>
			</th></tr>
				<cfloop index="functionIDX" from=1 to = #arrayLen(thisReleaseScript.functions)#>
					<cfset thisFunction = thisReleaseScript.functions[functionIDX]>
					<cfset functionName = structKeyExists (thisFunction.metadata,"hint") and thisFunction.metadata.hint is not ""?application.com.security.sanitiseHTML(thisFunction.metadata.hint):application.com.security.sanitiseHTML(thisFunction.metadata.name)>

					<!--- only show release script if it needs to be run (or run again); or perhaps we show that it has been done and don't show it as a link... --->
					<tr>
					
						<td class="runReleaseTD">
						<cfif not thisFunction.metadata.needsRunning>
							#functionName#
						</cfif>	
						<a onclick="runScript(this,'#thisReleaseScript.Name#','#thisFunction.metadata.name#',{cfpassword:<cfif arrayLen(StructFindValue(thisFunction,'cfpassword'))>true<cfelse>false</cfif>})" class="clickable">
							<cfif thisFunction.metadata.needsRunning>
								#functionName#
							<cfelse>
								Re-Run	
							</cfif>	
						</a>
						<cfif not thisFunction.metadata.runAutomatically>
							<BR>
							Note: This script is not run automatically
						</cfif>
						</td>

					</tr>

					<cfif thisFunction.metadata.lastRunInfo.recordCount is not 0>
						<cfif isJSON (thisFunction.metadata.lastRunInfo.metadata)>
							<cfset resultStruct = deserializeJSON(thisFunction.metadata.lastRunInfo.metadata)>
							<cfset releaseScriptObject.AddClassEtcToResult(resultStruct)>
								<cfloop array = #resultStruct.messages# index="message">
									<tr id="#thisReleaseScript.Name##thisFunction.metadata.name#_#message.className#"> 
										<td class="#message.className#">
											<div>Result from  #application.com.dateFunctions.dateTimeFormat(thisFunction.metadata.lastRunInfo.lastupdated)#</div>
											<div class="runReleaseErrorblock" style="float:left">#message.content#</div><div style="float:right"><A onclick="deleteCurrentRow(this);return false">X</A></div>												
										</td>
									</tr>
								</cfloop>
						</cfif>
					</cfif>

				</cfloop>

	</cfloop>

</table>

<cfif not haveCFPassword>
	Cold Fusion Admin Password: <input id="CFPassword" type="password" value=""><br />
</cfif>

<p><a href="?RunAll">Run All Outstanding Release Scripts</a></p>
<p><a href="?showRun=#not(showRun)#">#showRun?"Hide":"Show"# Run Release Scripts</a></p>
<!--- <p><a href="?showInActive=#not(showInActive)#">#showInActive?"Hide":"Show"# Inactive Release Scripts</a></p> --->
</cfoutput>