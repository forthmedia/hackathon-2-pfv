<!--- �Relayware. All Rights Reserved 2014 --->


<!--- 
These calls to cf_cbAppendCriteria generate the WHERE clause (or part of it) to use in your report query in a 
variable called Criteria. If a passed criteria paramter is blank, it is not included.  If no valid paramters were 
passed, Criteria is set to 1 > 0 to return all records.  A block of these calls is generated in the list form; you can 
copy this block and use it here. 
--->

<!---
<cf_cbAppendCriteria FieldName="table.column1" FieldType="INT" Operator="#column1Operator#" Value="#column1Criteria#">
<cf_cbAppendCriteria FieldName="table.column2" FieldType="INT" Operator="#column2Operator#" Value="#column2Criteria#">
--->


<!--- This is an example of a query you might run using the Criteria from the search form. --->

<!---
<cfquery name="ReportQuery"
 datasource = "#application.sitedatasource#">SELECT
		column1,
		column2,
		column1 - column2 * 100 AS calculatedvalue
	FROM
		table
	WHERE
		column3 > 300 AND
		#PreserveSingleQuotes(Criteria)#
</cfquery>
--->


<cf_head>
	<cf_title>CommerceBlocks</cf_title>
	<link rel="stylesheet" href="<cfoutput>#htmleditformat(stylesheet)#</cfoutput>">
</cf_head>



<!--- This is an example of outputting the results of this query. --->

<!---
<table>
<tr>
	<th>Column 1</th>
	<th>Column 2</th>
	<th>Some Calculation</th>
</tr>
<cfoutput query="ReportQuery">
<tr>
	<td>#column1#</td>
	<td>#column2#</td>
	<td>#calculatedvalue#</td>
</tr>
</cfoutput>
</table>
--->

<!--- 
The following body text remains uncommented so people so don't read instructions won't automatically run to tech support when they try to run this template unmodified.

Please delete the following body copy before you properly deploy a DatabaseBlocks report template.
--->

<P>You did not implement _ReportTemplate.cfm correctly.  Please read through the comments in _ReportTemplate.cfm to see how to implement this file correctly.  It is also recommended that you carefully read the DatabaseBlocks documentation.



