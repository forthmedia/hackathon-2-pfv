<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	promotionPopUpNew.cfm
Author:		Brendan Doherty		
Date created:	2001-05-01

	Objective - Promotional New pop up Page.
				Allows users to create a pop up.
				This page doesnt have Detail or language as detail is a phrase and to create new phrases
				we need the pop up ID. So I am doing it in 2 steps
				1. create the pop up then
				2. edit the pop up once we have the popupID and create detail phrase
	
	Rationale - Requested as part of TradeUp enhancements

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:
--->

<!--- this page submits a form to itself so handle any updates --->
<cfif (isDefined('action')) AND (action eq 'new')>	
	<CFQUERY NAME="insertpopup" datasource="#application.siteDataSource#">
		insert into promotionpopup(StrapLine,CampaignID, Startdate, Enddate)
		values(<cf_queryparam value="#frm_StrapLine#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#frm_CampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#CreateODBCDateTime(CreateDate(frm_startyear, frm_startmonth, frm_startday))#" CFSQLTYPE="cf_sql_timestamp" >, <cf_queryparam value="#CreateODBCDateTime(CreateDate(frm_endyear, frm_endmonth, frm_endday))#" CFSQLTYPE="cf_sql_timestamp" >)
	</CFQUERY>	
	
	<CFQUERY NAME="getInsertedpopupID" datasource="#application.siteDataSource#">
		select MAX(popupID) as popupID
		from promotionpopup
	</CFQUERY>	
	
	<CFQUERY NAME="insertTrigger" datasource="#application.siteDataSource#">
		insert into promotionpopuptrigger(PopUpID,PopUpCategoryID, TriggerValue)
		values(<cf_queryparam value="#getInsertedpopupID.popupID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#frm_Category#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#Triggers#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</CFQUERY>

	<cfset frm_popupID = getInsertedpopupID.popupID>
	<cfinclude template="promotionPopUpEdit.cfm">
	<CF_ABORT>
</cfif>



<cf_head>
	<cf_title>Pop Up Promotion Administration</cf_title>

<cfoutput>
<SCRIPT LANGUAGE="JavaScript">
<!--- on Submit make sure form is valid --->
function validateForm() {
	error = false;
	errorMsg = "";
	checkedcategory = false;	
	
	//triggers
	for(i=0;i<document.form1.Triggers.length;i++){
		// remove the netscape spacer
		if (document.form1.Triggers.options[i].value == ''){	
			document.form1.Triggers.options[i] = null				
		}
	}
	if (!document.form1.Triggers.length){
		error = true;
		errorMsg = 'You must select a pop up trigger';			
	}
	
	// the categories
	for(i=0;i<document.form1.frm_category.length;i++){
		if (document.form1.frm_category[i].checked){		
			checkedcategory = true;	
		}
	}
	if (!checkedcategory){
		error = true;
		errorMsg = 'You must select a category';			
	}	
		
	//the dates
	checkDates();
			
	// strapline
	if (document.form1.frm_strapLine.value == ''){
		error = true;
		errorMsg = 'You must enter a strapline';		
	}	
	
	// show message if error
	if(error){
		alert(errorMsg);
	}
	else{
		SelAll(); 
		document.form1.submit();
	}
}

<!--- helper function to check dates --->
function checkDates() {
	if(!checkDate(document.form1.frm_startday.value,document.form1.frm_startmonth.value,document.form1.frm_startyear.value)){
		error = true;
		errorMsg = 'Invalid start date';
	}
	else{
		if(!checkDate(document.form1.frm_endday.value,document.form1.frm_endmonth.value,document.form1.frm_endyear.value)){
			error = true;
			errorMsg = 'Invalid end date';
		}
	}
}

<!--- helper function to check dates --->
function checkDate(day,month,year) {
	
	if(isNaN(day) || isNaN(day) || isNaN(day)){
		error = true;
	}
	
	var myDay = parseInt(day);
	var myMonth =  parseInt(month);
	var myYear =  parseInt(year);
	
	/* Using form values, create a new date object
	which looks like "Wed Jan 1 00:00:00 EST 1975". */
	var myDate = new Date(year,month,day,0,0,0,0);  

	/* If we entered "Feb 31, 1975" in the form, the "new Date()" function
	converts the value to "Mar 3, 1975". Therefore, we compare the day
	in the array with the day we entered into the form. If they match,
	then the date is valid, otherwise, the date is NOT valid. 
	Fri Apr 27 00:00:00 UTC+0100 2001
	*/
	<!--- alert(myDate_array[1] + "!=" +myDayStr ); --->
	if ((myDate.getDate() != myDay) || (myDate.getMonth() != myMonth) || (myDate.getFullYear() != myYear)) {
	  return false;
	  alert(myDate.getDate() + " != " + myDay);
	  alert(myDate.getMonth() + " != " + myMonth);
	  alert(myDate.getFullYear() + " != " + myYear);
	} else {
	  return true;
	}
} 

<!--- used by the 2 selects to move items between them --->
<!--- taken from 2selectsrelated.js --->
function moveFromTo(formName,fromName,toName) {
	fromObject =  eval('document.'+formName +'.' + fromName)
	toObject =  eval('document.'+formName +'.' + toName)

	SelFrom = fromObject.options.selectedIndex;
	SelTo = toObject.options.length+1;

	if ((SelFrom >= 0) && (SelFrom < fromObject.options.length)) {			
		toObject.options.length = toObject.options.length + 1;
		toObject.options[SelTo-1].value = fromObject.options[SelFrom].value;
		toObject.options[SelTo-1].text = fromObject.options[SelFrom].text;
			
		for (i=SelFrom; i<fromObject.options.length-1; i++) {
			fromObject.options[i].value = fromObject.options[i+1].value;
			fromObject.options[i].text = fromObject.options[i+1].text;
		}
		fromObject.options[i] = null
		if (SelFrom > 0) fromObject.options.selectedIndex = SelFrom-1
		else fromObject.options.selectedIndex = 0;
	}		
}


function Move(formName,objectName,moveIndex) {
	thisObject =  eval('document.'+formName +'.' + objectName)

	Sel   = thisObject.options.selectedIndex;
	SelTo = thisObject.options.selectedIndex + moveIndex;
	Last  = thisObject.length
	if ((SelTo >= 0) && (SelTo <= Last-1)) {
		tempvalue  = thisObject.options[Sel].value;
		temptext  = thisObject.options[Sel].text;
		thisObject.options[Sel].value    = thisObject.options[SelTo].value;
		thisObject.options[Sel].text    = thisObject.options[SelTo].text;
		thisObject.options[SelTo].value  = tempvalue;
		thisObject.options[SelTo].text  = temptext;
		thisObject.options.selectedIndex = SelTo;
	}		
}


function SelAll_(formName,objectName) {
	thisForm = eval('document.'+formName) 
	for (thisElement=0; thisElement<thisForm.elements.length; thisElement++) {
		if  ( thisForm.elements[thisElement].name.substr(0,objectName.length) == objectName) {
			if (thisForm.elements[thisElement].type== 'select-multiple') {
				thisObject =  thisForm.elements[thisElement]
				thisObject.multiple = true;
				for (i=0; i<thisObject.options.length; i++) {
					thisObject.options[i].selected = true;
				}
			}
		}																				
	}
}
	
<!--- for back wards compatability --->
function SelAll() {
	SelAll_('form1', 'Triggers');
}
</SCRIPT>
</CFOUTPUT>	
</cf_head>

<!--- get promotion name --->
<CFQUERY NAME="GetPromotion" datasource="#application.siteDataSource#">
	SELECT promoName from Promotion where CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- for each campaign we need to get the categories --->
<!--- categories are set up manually (no user interface) --->
<CFQUERY NAME="Categories" datasource="#application.siteDataSource#">
	Select * 
	from PromotionPopUpCategory
	where CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
		

<br>
<!--- Standard header --->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Create New Pop Up Promotion - <cfoutput>#htmleditformat(GetPromotion.promoName)#</cfoutput></DIV>
		</TD>
	</TR>
	<TR>	
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<cfoutput><a href="promotionPopUp.cfm?frm_CampaignID=#frm_CampaignID#" Class="Submenu">pop up administration</a><font color=white> |</font></cfoutput>
			<A HREF="javascript:self.close();" Class="Submenu">close</A>
		</TD>
	</TR>
</table>
<br>
<form action="promotionPopUpNew.cfm" method="post" name="form1">
<input type="hidden" name="action" value="new">
<cfoutput><CF_INPUT type="hidden" name="frm_campaignID" value="#frm_campaignID#"></cfoutput>
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td>
		StrapLine
	</td>
	<td colspan=3 width="500">
		<input type="text" name="frm_strapLine" size="20" maxlength="20">
	</td>
</tr>
<tr>
	<td>
		From
	</td>
	<td colspan=3>
	
		<cfoutput><CF_INPUT type="text" name="frm_startday" value="#Datepart('d', now())#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_startmonth" value="#Datepart('m', now())#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_startyear" value="#Datepart('yyyy', now())#" size="4" maxlength="4"></cfoutput>
	</td>
</tr>
<tr>
	<td>
		To
	</td>
	<td colspan=3>
		<cfoutput><CF_INPUT type="text" name="frm_endday" value="#Datepart('d', now())#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_endmonth" value="#Datepart('m', now())#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_endyear" value="#Datepart('yyyy', now())#" size="4" maxlength="4"></cfoutput>
	</td>
</tr>
<tr>
	<td>Trigger</td>
	<cfloop query="Categories">
	<td width="50">
		<cfoutput><CF_INPUT type="radio" name="frm_category" value="#PopUpCategoryID#" onClick="init#PopUpCategoryName#('form1','All','Triggers');">#htmleditformat(PopUpCategoryName)#</cfoutput>
	</td>
	</cfloop>
</tr>
</table>
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>

<cfset first = true>
<cfloop query="Categories">	
	<cfif CategoryType eq 'SQL'>
		<!--- Run the SQL it returns popuptrigger --->
		<CFQUERY NAME="All" datasource="#application.siteDataSource#">
			#preservesinglequotes(Categories.CategorySQL)#
		</CFQUERY>			
		<!--- Create a List as its easier for javascript to check against --->
		<cfset amendedcategoryList = "">
		<cfloop query="ALL">
			<cfset amendedcategoryList =  ListAppend(amendedcategoryList,popuptrigger)>
		</cfloop>
	<cfelse>
		<cfset amendedcategoryList = Categories.CategoryList>
	</cfif>
			
	<cfif first>	
		<tr>
			<td colspan=4 align="center">
				<table>
				<tr>
					<td>
						<select name="All" size="6">
						<OPTION VALUE="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
						</SELECT>
					</TD>
					<TD>
						<cfoutput><INPUT TYPE="BUTTON" VALUE=">>" onClick="moveFromTo('form1','All','Triggers')"><BR>
						<INPUT TYPE="BUTTON" VALUE="<<" onClick="moveFromTo('form1','Triggers','All')"></cfoutput>
					</TD>
					<TD>
						<SELECT NAME="Triggers" SIZE="6"  MULTIPLE >
						<OPTION VALUE="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
						</SELECT>
					</td>
				</tr>	
				</table>	
			</td>		
		</tr>	
		<cfset first = 'false'>
	</cfif>	

	<!--- For each category create a javascript function that will change the selects --->
	<script>
	<cfoutput>
		function init#PopUpcategoryName#(formName,fromName,toName){
			thisForm = eval('document.'+formName) 
			fromObject =  eval('document.'+formName +'.' + fromName)
			toObject =  eval('document.'+formName +'.' + toName)			
			
			<!--- populate the from select --->
			fromObject.options.length = #ListLen(amendedcategoryList)#;
			<cfset i = 0>      
			<cfloop index="popuptrigger" list="#amendedcategoryList#">
				fromObject.options[#i#].value = "#popuptrigger#";
				fromObject.options[#i#].text = "#popuptrigger#";
				<cfset i = i + 1>  
			</cfloop>			
       		
			<!--- empty the to select --->
			for (i=0;i<toObject.options.length; i++) {
				toObject.options[i].value = "";
				toObject.options[i].text = "";
			}									
		}
	</script>	
	</cfoutput>
</cfloop>
</tr>
<tr>
<td colspan=4 align="center">
	<input type="button" name="new" value="new" onClick="validateForm();">
</td>
</tr>
</table>
</form>


