<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---
	2002-09-04  SWJ Created

	2009/05/18	NJH	P-SNY069 - pass through promotionGroupTextID if it exists
--->
<CFPARAM NAME="frmruninpopup" DEFAULT="">
<CFPARAM NAME="current" TYPE="string" DEFAULT="promotionList.cfm">
<CFSET LoginRequired=2>
<!---
<CFSET securitylist = "OrderTask:read">
<cfinclude template="/templates/ApplicationDirectorySecurity.cfm">
 --->

<!--- NJH 2009/05/18 P-SNY069 - pass through promotionGroupTextID if it exists --->
<cfset additionalParams = "">
<cfif structKeyExists(url,"promotionGroupTextID")>
	<cfset additionalParams = "promotionGroupTextID=#promotionGroupTextID#">
</cfif>

<CF_RelayNavMenu moduleTitle="" thisDir="promotions">
	<CFIF frmruninpopup IS NOT "True">
		<CF_RelayNavMenuItem MenuItemText="List Promotions" CFTemplate="promotionList.cfm?#additionalParams#" target="mainSub">
		<CFIF not findNoCase("commHeader.cfm",SCRIPT_NAME,1) >
			<CFIF application.com.login.checkinternalpermissions(securityTask="orderTask").add>
				<CF_RelayNavMenuItem MenuItemText="New Promotion" CFTemplate="promotionAddForm.cfm?#additionalParams#" target="mainSub">
			</CFIF>
		</CFIF>

		<CF_RelayNavMenuItem MenuItemText="Promotion Rights" CFTemplate="promotionRights.cfm?#additionalParams#" target="mainSub">
	</cfif>
</CF_RelayNavMenu>



