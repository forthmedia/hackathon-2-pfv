<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	promotionPopUp.cfm
Author:		Brendan Doherty		
Date created:	2001-05-01

	Objective - Promotional pop up administration page.
				Allows users to select a popup then edit or view
	
	Rationale - Requested as part of TradeUp enhancements

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:
--->
<cfparam name="frmlanguage" default="1">

<!--- get the campaign Name  --->
<!--- Only tested with tradeUp so far --->
<CFQUERY NAME="GetPromotion" datasource="#application.siteDataSource#">
	SELECT promoName from Promotion where CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- Get all the popups linked to this campaign --->
<CFQUERY NAME="getPopUps" datasource="#application.siteDataSource#">
	Select popupID, strapline
	from PromotionPopUp popup, Promotion promo
	where popup.CampaignId = promo.CampaignID
	and promo.CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- Standard get language rights --->
<!--- we need this as the user will be able to select a language to view the pop up in --->
<CFQUERY NAME="getLanguageRights" datasource="#application.siteDataSource#">
	SELECT 
		convert(varchar(3),l.Languageid) + '#application.delim1#' + l.language as languages,
		l.language
	FROM 
		Language as l, 
		booleanflagdata as bfd, 
		flag as f, 
		flaggroup as fg
	WHERE 
		fg.flaggrouptextid='LanguageRights'
		and fg.flaggroupid = f.flaggroupid
		and f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#	
	UNION
		SELECT '1#application.delim1#English', 'English'
		Order by 	Language
</CFQUERY>
<CFSET validLanguages = valuelist(getLanguageRights.languages)>		





<cf_head>
	<cf_title>Pop Up Promotion Administration</cf_title>
</cf_head>
	
<script>
<!--- helper function to allow submitting form from link --->
function sendForm(formaction){
	document.forms[0].action = formaction;
	document.forms[0].submit();
}

<!--- preview pop up function opens the preview page in a new window --->
<!--- Should look exactly like the pop up in the partner pages --->
function preview(){
	popupURL = 'showpromotionPopup.cfm';
	popupURL = popupURL + '?frm_campaignID=' + document.forms[0].frm_campaignID.value;
	popupURL = popupURL + '&frm_popupID=' + document.forms[0].frm_popupID.value;
	popupURL = popupURL + '&frmlanguage=' + document.forms[0].frmLanguage.value;

	features = 'scrollbars=no,height=350,width=280,top=100,left=150';
	previewpopUpWin = window.open(popupURL,'previewPopUpWindow',features);
	if (window.focus) {
		previewpopUpWin.focus();
	}
	if (previewpopUpWin.opener == null){
		previewpopUpWin.opener = window;
		previewpopUpWin.opener.name = "PopUp";
	}	 
}
</script>


<br>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Pop Up Promotion Administration - <cfoutput>#htmleditformat(GetPromotion.promoName)#</cfoutput></DIV>
		</TD>
	</TR>
	<TR>	
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<a href="javascript:sendForm('promotionPopUpNew.cfm');" Class="Submenu">new</a><font color=white> |</font>			
			<A HREF="javascript:self.close();" Class="Submenu">close</A>
		</TD>
	</TR>
</table>
<br>
<form action="" method="post">
<cfoutput><CF_INPUT type="hidden" name="frm_campaignID" value="#frm_campaignID#"></cfoutput>
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td width="200">
		<SELECT NAME="frm_popupID">
		<cfloop query="getPopUps">
			<cfoutput><OPTION VALUE="#popupID#">#htmleditformat(strapline)#</option>														</cfoutput>
		</cfloop>
		</SELECT>			
	</td>
	<td>
		<a href="javascript:sendForm('promotionPopUpEdit.cfm');">edit</a>		
	</td>	
</tr>
<tr>

</tr>
<tr>
	<td>
		<cf_displayValidValues 
			formFieldName = "frmLanguage" 
			validValues="#validLanguages#" 
			currentValue="#frmLanguage#"> 	
	</td>
	<td>
		<a href="##" onClick="javascript:preview()">preview</a
	</td>
</tr>
</table>
</form>


