<!--- �Relayware. All Rights Reserved 2014 --->
<!---
DatabaseBlocks(TM)
A module of the CommerceBlocks line of modular ColdFusion development tools
Copyright (c) 2000 by Productivity Enhancement, Inc.
All Rights Reserved.
Development Team: Adam & David Churvis
--->

<cfif Trim(Attributes.value) NEQ "">
	<CFSET Criteria = Attributes.fieldName>
	<CFSET Value = Attributes.value>
	<CFIF Attributes.FieldType is not 'DATETIME'>
		<!--- escape quotes if field type is not datetime --->
		<CFSET Value = Replace( Value, "'", "''", "ALL" )>
	</CFIF>
	
	<CFIF ListFindNoCase( 'CHAR,MEMO', Attributes.FieldType )>
		<CFIF Attributes.operator is 'EQUAL'>       <CFSET Criteria = Criteria & " = '#Value#' ">
		<CFELSEIF Attributes.operator is 'NOT_EQUAL'>   <CFSET Criteria = Criteria & " <> '#Value#' ">
		<CFELSEIF Attributes.operator is 'GREATER_THAN'><CFSET Criteria = Criteria & " > '#Value#' ">
		<CFELSEIF Attributes.operator is 'LESS_THAN'><CFSET Criteria = Criteria & " < '#Value#' ">
		<CFELSEIF Attributes.operator is 'CONTAINS'>        <CFSET Criteria = Criteria & " LIKE '%#Value#%' ">
		<CFELSEIF Attributes.operator is 'DOES_NOT_CONTAIN'>        <CFSET Criteria = Criteria & " LIKE '%#Value#%' ">
		<CFELSEIF Attributes.operator is 'BEGINS_WITH'> <CFSET Criteria = Criteria & " LIKE '#Value#%' ">
		<CFELSEIF Attributes.operator is 'ENDS_WITH'>   <CFSET Criteria = Criteria & " LIKE '%#Value#' ">
		</CFIF>
	
	<CFELSEIF ListFindNoCase( 'DATETIME', Attributes.FieldType)>
		<CFIF Attributes.operator is 'EQUAL'>       <CFSET Criteria = Criteria & " = #CreateODBCDateTime(Value)# ">
		<CFELSEIF Attributes.operator is 'NOT_EQUAL'>   <CFSET Criteria = Criteria & " <> #CreateODBCDateTime(Value)# ">
		<CFELSEIF Attributes.operator is 'GREATER_THAN'><CFSET Criteria = Criteria & " > #CreateODBCDateTime(Value)# ">
		<CFELSEIF Attributes.operator is 'LESS_THAN'><CFSET Criteria = Criteria & " < #CreateODBCDateTime(Value)# ">
		</cfif>
	
	<CFELSEIF ListFindNoCase( 'INT,FLOAT,BIT', Attributes.FieldType )>
		<CFIF Attributes.operator is 'EQUAL'>       <CFSET Criteria = Criteria & " = #Val(Value)# ">
		<CFELSEIF Attributes.operator is 'NOT_EQUAL'>   <CFSET Criteria = Criteria & " <> #Val(Value)# ">
		<CFELSEIF Attributes.operator is 'GREATER_THAN'><CFSET Criteria = Criteria & " > #Val(Value)# ">
		<CFELSEIF Attributes.operator is 'LESS_THAN'><CFSET Criteria = Criteria & " < #Val(Value)# ">
		</CFIF>
	</CFIF>
	
	<CFIF Trim( Caller.Criteria ) is "" OR Trim(Caller.Criteria) EQ "1 > 0">
	  <CFSET Caller.Criteria = Criteria>
	<CFELSE>
	  <CFSET Caller.Criteria = Caller.Criteria & " AND " & Criteria>
	</CFIF>
<cfelse>
	<CFIF Trim( Caller.Criteria ) EQ "" OR Trim(Caller.Criteria) EQ "1 > 0">
	  <CFSET Caller.Criteria = "1 > 0">
	</CFIF>
</cfif>
