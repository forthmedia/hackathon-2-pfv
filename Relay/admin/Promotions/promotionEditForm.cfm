<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		promotionEditForm.cfm	
Author:			??-???-????
Date created:	Adam & David Churvis

	Objective - Edit a promotion
	
	Rationale - 
	
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
01-MAY-2001 Brendan Doherty added a link in the standard header to open a pop up
			Administration page
30-MAY-2001 CPS Include allflag to allow maintenance of flag related data for promotions
            Also added new input fields to reflect additional attributes on the table			
08-May-2005 SWJ added showOrderNotes & showOrderRelatedFiles
18-May-2009	NJH P-SNY069 - changed the action on the form to be promotionList.cfm rather than tabOnly.cfm was the promo has not been edited.
				also changed paths on javascript files
			
Enhancement still to do:
--->

<cfparam name="NewRecord" default="0">

<!--- Maximum number of records in a select menu --->
<cfset MaxInSelect = 25>
<cfquery name="Getpromotion" datasource = "#application.sitedatasource#">
SELECT
p.CampaignID,
p.PromoID,
p.promoName,
p.Basecurrency,
p.MaxOrderNo,
p.Fulfilledby,
p.ShippingMatrixID,
p.InventoryID,
p.AskQuestions,
p.QuestionIDs,
p.DistributorList,
p.Mail,
p.ZeroPrice,
p.PromoOrderType,
p.lastupdatedby,
p.ShowProductGroup,
p.ShowProductGroupScreen,
p.ShowProductGroupHeader,
p.ShowTermsConditions,
p.ShowLineTotal,
p.ShowPageTotal,
p.ShowPictures,
p.ShowProductCode,
p.ShowProductCodeAsAnchor,
p.ShowDescription,
p.ShowPointsValue,
p.ShowQuantity,
p.ShowDecimals,
p.ShowDiscountPrice,
p.ShowListPrice,
p.ShowIllustrativePrice,
p.ShowNoInStock,
p.ShowMaxQuantity,
p.ShowPreviouslyOrdered,
p.ShowDistributorList,
p.ShowPaymentOptions,
p.ShowOrderConfirmationScreen,
p.ShowPreviousOrdersLink,
p.ShowPreviousQuotesLink,
p.ShowGrandTotal,
p.ShowVATCalculation,
p.ShowDeliveryCalculation,
p.ShowDownloadLink,
<!--- 2005-05-08 SWJ --->
p.showOrderNotes,
p.showOrderRelatedFiles,
p.RequiresPurchaseRef,
p.ShowPurchaseOrderRef,
p.ConfirmedPhrase,
p.OrderNoPrefix,
<!--- p.BorderSet AS p__BorderSet, --->
p.ShowClosingMessage,
p.UsePoints,
p.UseButtons,
p.PointsDetailURL,
p.UniqueTranslations,
p.UniqueTranslationSuffix,
p.TranslateDescription,
p.OrderSingleProduct,
p.PromotionGroupId,
p.UsePromoId,
p.ShowChangeItemsInOrder,
<!--- AR 2005-04-25 adding GlobalSKU solution. --->
p.UseGlobalSKU,
p.GlobalSKUDefaultCurrency,
<!--- 2006-05-25 GCC A_784 --->
p.mandatoryContactPhone,
p.accessoryMode
<!--- ,
	b.BorderSet AS b__BorderSet --->
FROM
promotion p <!--- INNER JOIN Borders b
	ON p.BorderSet = b.BorderSet --->

WHERE
	p.CampaignID =  <cf_queryparam value="#Val(CampaignID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<!--- CPS 2001-05-30 removed this code as now handled by include statement to allflag --->
<!--- Get Promotion Flags --->
<!--- Only basic text input --->
<!--- <CFQUERY NAME="PromotionFlagGroup" datasource="#application.siteDataSource#">
select flaggroupID, ft.name 
from flaggroup fg, flagtype ft
where fg.flagtypeID = ft.flagtypeID
and FlagGroupTextID = 'PromotionValueFlags'
</CFQUERY>

<CFQUERY NAME="PromotionFlags" datasource="#application.siteDataSource#">
select a.flagid, b.data as value, a.name
from

(select 
	f.FlagID,
		f.FlagGroupID, 
		f.Name,
		f.Description, 
		f.Active
	
	from 
		flagentitytype fet 
		inner join flaggroup fg 
		on fet.entitytypeid = fg.entitytypeid
		inner join flag f
		on f.flaggroupid = fg.flaggroupid
	where 
		fet.tablename='Promotion') a
left join 
(Select f.flagid, data 
from 
	flag f inner join integerflagdata i
	on f.flagid = i.flagid
	where i.entityid=#Val(CampaignID)#) b
on a.flagid = b.flagid
</CFQUERY> --->

<CFQUERY NAME="getPopUps" datasource="#application.siteDataSource#">
	Select popupID, strapline
	from PromotionPopUp popup, Promotion promo
	where popup.CampaignId = promo.CampaignID
	and promo.CampaignID =  <cf_queryparam value="#Val(CampaignID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<cfif Getpromotion.RecordCount GT 0>
	<cfif CGI.Referer DOES NOT CONTAIN "promotionList.cfm" AND CGI.Referer DOES NOT CONTAIN "promotionSearch.cfm" AND Len(Trim(CGI.Referer)) AND NOT NewRecord>
		<cfparam name="Form.PromoID" default="">
		<cfparam name="Form.promoName" default="">
		<cfparam name="Form.Basecurrency" default="">
		<cfparam name="Form.MaxOrderNo" default="">
		<cfparam name="Form.Fulfilledby" default="">
		<cfparam name="Form.ShippingMatrixID" default="">
		<cfparam name="Form.InventoryID" default="">
		<cfparam name="Form.AskQuestions" default="">
		<cfparam name="Form.QuestionIDs" default="">
		<cfparam name="Form.DistributorList" default="">
		<cfparam name="Form.Mail" default="">
		<cfparam name="Form.ZeroPrice" default="">
		<cfparam name="Form.PromoOrderType" default="">
		<cfparam name="Form.lastupdatedby" default="">
		<cfparam name="Form.ShowProductGroup" default="">
		<cfparam name="Form.ShowProductGroupScreen" default="">
		<cfparam name="Form.ShowProductGroupHeader" default="">
		<cfparam name="Form.ShowTermsConditions" default="">
		<cfparam name="Form.ShowLineTotal" default="">
		<cfparam name="Form.ShowPageTotal" default="">
		<cfparam name="Form.ShowPictures" default="">
		<cfparam name="Form.ShowProductCode" default="">
		<cfparam name="Form.ShowProductCodeAsAnchor" default="">
		<cfparam name="Form.ShowDescription" default="">
		<cfparam name="Form.ShowPointsValue" default="">
		<cfparam name="Form.ShowQuantity" default="">
		<cfparam name="Form.ShowDecimals" default="">
		<cfparam name="Form.ShowDiscountPrice" default="">
		<cfparam name="Form.ShowListPrice" default="">
		<cfparam name="Form.ShowIllustrativePrice" default="">
		<cfparam name="Form.ShowOrderNotes" default="">
		<cfparam name="Form.ShowOrderRelatedFiles" default="">
		<cfparam name="Form.ShowNoInStock" default="">
		<cfparam name="Form.ShowMaxQuantity" default="">
		<cfparam name="Form.ShowPreviouslyOrdered" default="">
		<cfparam name="Form.ShowDistributorList" default="">
		<cfparam name="Form.ShowPaymentOptions" default="">
		<cfparam name="Form.ShowOrderConfirmationScreen" default="">
		<cfparam name="Form.ShowPreviousOrdersLink" default="">
		<cfparam name="Form.ShowPreviousQuotesLink" default="">
		<cfparam name="Form.ShowGrandTotal" default="">
		<cfparam name="Form.ShowVATCalculation" default="">
		<cfparam name="Form.ShowDeliveryCalculation" default="">
		<cfparam name="Form.ShowDownloadLink" default="">
		<cfparam name="Form.RequiresPurchaseRef" default="">
		<cfparam name="Form.ShowPurchaseOrderRef" default="">
		<cfparam name="Form.ConfirmedPhrase" default="">
		<cfparam name="Form.OrderNoPrefix" default="">
		<!--- <cfparam name="Form.p__BorderSet" default=""> --->
		<cfparam name="Form.ShowClosingMessage" default="">
		<cfparam name="Form.UsePoints" default="">
		<cfparam name="Form.UseButtons" default="">
		<cfparam name="Form.PointsDetailURL" default="">
		<cfparam name="Form.UniqueTranslations" default="">
		<cfparam name="Form.UniqueTranslationSuffix" default="">
		<cfparam name="Form.TranslateDescription" default="">
		<cfparam name="Form.OrderSingleProduct" default="">
		<cfparam name="Form.PromotionGroupId" default="">
		<cfparam name="Form.UsePromoId" default="">
		<cfparam name="Form.ShowChangeItemsInOrder" default="">		
		<cfparam name="Form.accessoryMode" default="">			
		<!--- 2006-05-25 GCC A_784 --->
		<cfparam name="Form.mandatoryContactPhone" default="">	

		<cfset Getpromotion.PromoID[1] = Form.PromoID>
		<cfset Getpromotion.promoName[1] = Form.promoName>
		<cfset Getpromotion.Basecurrency[1] = Form.Basecurrency>
		<cfset Getpromotion.MaxOrderNo[1] = Form.MaxOrderNo>
		<cfset Getpromotion.Fulfilledby[1] = Form.Fulfilledby>
		<cfset Getpromotion.ShippingMatrixID[1] = Form.ShippingMatrixID>
		<cfset Getpromotion.InventoryID[1] = Form.InventoryID>
		<cfset Getpromotion.AskQuestions[1] = Form.AskQuestions>
		<cfset Getpromotion.QuestionIDs[1] = Form.QuestionIDs>
		<cfset Getpromotion.DistributorList[1] = Form.DistributorList>
		<cfset Getpromotion.Mail[1] = Form.Mail>
		<cfset Getpromotion.ZeroPrice[1] = Form.ZeroPrice>
		<cfset Getpromotion.PromoOrderType[1] = Form.PromoOrderType>
		<cfset Getpromotion.lastupdatedby[1] = Form.lastupdatedby>
		<cfset Getpromotion.ShowProductGroup[1] = Form.ShowProductGroup>
		<cfset Getpromotion.ShowProductGroupScreen[1] = Form.ShowProductGroupScreen>
		<cfset Getpromotion.ShowProductGroupHeader[1] = Form.ShowProductGroupHeader>
		<cfset Getpromotion.ShowTermsConditions[1] = Form.ShowTermsConditions>
		<cfset Getpromotion.ShowLineTotal[1] = Form.ShowLineTotal>
		<cfset Getpromotion.ShowPageTotal[1] = Form.ShowPageTotal>
		<cfset Getpromotion.ShowPictures[1] = Form.ShowPictures>
		<cfset Getpromotion.ShowProductCode[1] = Form.ShowProductCode>
		<cfset Getpromotion.ShowProductCodeAsAnchor[1] = Form.ShowProductCodeAsAnchor>
		<cfset Getpromotion.ShowDescription[1] = Form.ShowDescription>
		<cfset Getpromotion.ShowPointsValue[1] = Form.ShowPointsValue>
		<cfset Getpromotion.ShowQuantity[1] = Form.ShowQuantity>
		<cfset Getpromotion.ShowDecimals[1] = Form.ShowDecimals>
		<cfset Getpromotion.ShowDiscountPrice[1] = Form.ShowDiscountPrice>
		<cfset Getpromotion.ShowListPrice[1] = Form.ShowListPrice>
		<cfset Getpromotion.ShowIllustrativePrice[1] = Form.ShowIllustrativePrice>
		<cfset Getpromotion.ShowOrderNotes[1] = Form.ShowOrderNotes>
		<cfset Getpromotion.ShowOrderRelatedFiles[1] = Form.ShowOrderRelatedFiles>
		<cfset Getpromotion.ShowNoInStock[1] = Form.ShowNoInStock>
		<cfset Getpromotion.ShowMaxQuantity[1] = Form.ShowMaxQuantity>
		<cfset Getpromotion.ShowPreviouslyOrdered[1] = Form.ShowPreviouslyOrdered>
		<cfset Getpromotion.ShowDistributorList[1] = Form.ShowDistributorList>
		<cfset Getpromotion.ShowPaymentOptions[1] = Form.ShowPaymentOptions>
		<cfset Getpromotion.ShowOrderConfirmationScreen[1] = Form.ShowOrderConfirmationScreen>
		<cfset Getpromotion.ShowPreviousOrdersLink[1] = Form.ShowPreviousOrdersLink>
		<cfset Getpromotion.ShowPreviousQuotesLink[1] = Form.ShowPreviousQuotesLink>
		<cfset Getpromotion.ShowGrandTotal[1] = Form.ShowGrandTotal>
		<cfset Getpromotion.ShowVATCalculation[1] = Form.ShowVATCalculation>
		<cfset Getpromotion.ShowDeliveryCalculation[1] = Form.ShowDeliveryCalculation>
		<cfset Getpromotion.ShowDownloadLink[1] = Form.ShowDownloadLink>
		<cfset Getpromotion.RequiresPurchaseRef[1] = Form.RequiresPurchaseRef>
		<cfset Getpromotion.ShowPurchaseOrderRef[1] = Form.ShowPurchaseOrderRef>
		<cfset Getpromotion.ConfirmedPhrase[1] = Form.ConfirmedPhrase>
		<cfset Getpromotion.OrderNoPrefix[1] = Form.OrderNoPrefix>
		<!--- <cfset Getpromotion.p__BorderSet[1] = Form.p__BorderSet> --->
		<cfset Getpromotion.ShowClosingMessage[1] = Form.ShowClosingMessage>
		<cfset Getpromotion.UsePoints[1] = Form.UsePoints>
		<cfset Getpromotion.UseButtons[1] = Form.UseButtons>
		<cfset Getpromotion.PointsDetailURL[1] = Form.PointsDetailURL>
		<cfset Getpromotion.UniqueTranslations[1] = Form.UniqueTranslations>
		<cfset Getpromotion.UniqueTranslationSuffix[1] = Form.UniqueTranslationSuffix>
		<cfset Getpromotion.TranslateDescription[1] = Form.TranslateDescription>
		<cfset Getpromotion.OrderSingleProduct[1] = Form.OrderSingleProduct>
		<cfset Getpromotion.PromotionGroupId[1] = Form.PromotionGroupId>
		<cfset Getpromotion.UsePromoId[1] = Form.UsePromoId>
		<cfset Getpromotion.ShowChangeItemsInOrder[1] = Form.ShowChangeItemsInOrder>		
		<cfset Getpromotion.accessoryMode[1] = Form.accessoryMode>	
		<!--- 2006-05-25 GCC A_784 --->		
		<cfset Getpromotion.mandatoryContactPhone[1] = Form.mandatoryContactPhone>
	</cfif>

	<!--- <cfif CGI.Referer CONTAINS "promotionList.cfm" OR NOT Len(Trim(CGI.Referer)) OR NewRecord>
		<cfquery name="GetBordersCount" datasource = "#application.sitedatasource#">
		SELECT
			COUNT(*) AS NumBorderss
		FROM
			Borders
		GROUP BY Borderset	
		</cfquery> --->

		<!--- <cfset BordersRecCount = GetBordersCount.NumBorderss>
		<cfquery name="GetBorderss" datasource = "#application.sitedatasource#">
		SELECT
			Distinct(BorderSet)		FROM
			Borders
		<cfif BordersRecCount GT MaxInSelect>
		WHERE
			BorderSet = '#Trim(Getpromotion.p__BorderSet)#'
		</cfif>
		ORDER BY
			BorderSet ASC
		</cfquery>
		<cfset BordersFilterCriteria = GetBorderss.BorderSet>
	<cfelse>
		<cfquery name="GetBorderss" datasource = "#application.sitedatasource#">
		SELECT
			Distinct(BorderSet)		FROM
			Borders
		<cfif IsDefined("BordersFilterCriteria")>
			WHERE
				BorderSet LIKE '#BordersFilterCriteria#%'
		</cfif>
		ORDER BY
			BorderSet ASC
		</cfquery>
		<cfset BordersRecCount = GetBorderss.RecordCount>
	</cfif> --->

	<!--- CPS 2001-05-30 query to populate promotion group --->
	<CFQUERY NAME="GetPromotionGroup" datasource="#application.siteDataSource#">
		SELECT * 
		  FROM PromotionGroup
	</CFQUERY>
	
	<!--- CPS 2001-05-30 query to populate use promotion id --->	
	<CFQUERY NAME="GetOtherPromotion" datasource="#application.siteDataSource#">
		SELECT promoId 
		  FROM Promotion
		 WHERE CampaignID <>  <cf_queryparam value="#Val(CampaignID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
		
</cfif>




<cf_head>
	<cf_title>Promotions</cf_title>

	<!--- NJH 2009-05-20 P-SNY069 - changed paths to not use relative paths --->
	<!--- include validation library --->
	<SCRIPT type="text/javascript" src="/admin/promotions/validate.js"></script>

	<!--- include list library --->
	<SCRIPT type="text/javascript" src="/admin/promotions/listlib.js"></script>

	<cfif Getpromotion.RecordCount EQ 1>
	<SCRIPT type="text/javascript">
	<!--
		//global var; 1 if validateForm does anything, 0 if not
		PerformAction = 1;

		//called when user changes the filter criteria
		function changeFilter(prObj) {
			if(checkFilterCriteria(prObj.value)) {
				PerformAction = 0;
				document.EditForm.action = "promotionEditForm.cfm";
				validateForm();
				document.EditForm.submit();
			}
			else {
				alert("You have specified an invalid criteria.");
				prObj.focus();
				return false;
			}
		}
		

		

		//validates the form using functions in validation library
		function validateForm() {

			if(PerformAction == 1) {
				with(document.EditForm) {
					if(!hasValue(PromoID.value)) {
						alert("Please enter a value for this column.");
						PromoID.focus();
						return false;
					}
					if(!checkInteger(MaxOrderNo.value)) {
						alert("Please enter an integer value for this column.");
						MaxOrderNo.focus();
						return false;
					}
					if(!checkInteger(InventoryID.value)) {
						alert("Please enter an integer value for this column.");
						InventoryID.focus();
						return false;
					}
					if(!checkInteger(lastupdatedby.value)) {
						alert("Please enter an integer value for this column.");
						lastupdatedby.focus();
						return false;
					}
					action = "promotionEditAction.cfm";

				}
			}
			/*with(document.EditForm) {
				if(PerformAction) {
					if(BordersMenu.options[BordersMenu.selectedIndex].value == "") {
						alert("Please choose a Borders.");
						BordersMenu.focus();
						return false;
					}
				}

				p__BorderSet.value = FSBorderSetArray[BordersMenu.options[BordersMenu.selectedIndex].value - 1];
			}*/
			return true;
		}
		<cfif IsDefined("Client.promotion.CampaignIDList")>
		CampaignIDArray = new Array();

		<cfoutput>
		<cfset Index_Num = 0>
		<cfloop index="Index_List" list="#Client.promotion.CampaignIDList#" delimiters="~">
			<cfset Index_Num = Index_Num + 1>
			CampaignIDArray[#Index_Num#] = "#Index_List#";
		</cfloop>
		</cfoutput>

		function ChangeRecord() {
			with(document.NavForm) {
				CampaignID.value = CampaignIDArray[ExpressMenu.options[ExpressMenu.selectedIndex].value];
				submit();
			}
		}
		var jsOnNavArray = new Array();
		var jsOffNavArray = new Array();

		//Set item 1 equal to an image corresponding to the "First" command
		jsOffNavArray[1] = new Image();
		jsOnNavArray[1] = new Image();
		jsOffNavArray[1].src = "Images/Blue_first_off.gif";
		jsOnNavArray[1].src = "Images/Blue_first_on.gif";

		//Set item 2 equal to an image corresponding to the "Previous" command
		jsOffNavArray[2] = new Image();
		jsOnNavArray[2] = new Image();
		jsOffNavArray[2].src = "Images/Blue_previous_off.gif";
		jsOnNavArray[2].src = "Images/Blue_previous_on.gif";

		//Set item 3 equal to an image corresponding to the "Next" command
		jsOffNavArray[3] = new Image();
		jsOnNavArray[3] = new Image();
		jsOffNavArray[3].src = "Images/Blue_next_off.gif";
		jsOnNavArray[3].src = "Images/Blue_next_on.gif";

		//Set item 4 equal to an image corresponding to the "Last" command
		jsOffNavArray[4] = new Image();
		jsOnNavArray[4] = new Image();
		jsOffNavArray[4].src = "Images/Blue_last_off.gif";
		jsOnNavArray[4].src = "Images/Blue_last_on.gif";

		//image onMouseOver= function
		function doMouseOver(jsArrayIndex) {
			//change appropriate image sourcefile
			if(jsArrayIndex == 1) {
				document.images.First.src = jsOnNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 2) {
				document.images.Previous.src = jsOnNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 3) {
				document.images.Next.src = jsOnNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 4) {
				document.images.Last.src = jsOnNavArray[jsArrayIndex].src;
			}
		}

		//image onMouseOut= function
		function doMouseOut(jsArrayIndex) {
			//change appropriate image sourcefile
			if(jsArrayIndex == 1) {
				document.images.First.src = jsOffNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 2) {
				document.images.Previous.src = jsOffNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 3) {
				document.images.Next.src = jsOffNavArray[jsArrayIndex].src;
			}
			else if(jsArrayIndex == 4) {
				document.images.Last.src = jsOffNavArray[jsArrayIndex].src;
			}
		}
		</cfif>
	//-->
	</SCRIPT>
	</cfif>
	<SCRIPT type="text/javascript">
	<!--
		function init() {
			<cfif Getpromotion.RecordCount>
			document.EditForm.PromoID.focus();
			</cfif>
			return;
		}

		function openWin(popupurl){
			popupURL = 'promotionPopUp.cfm?frm_campaignID=<cfoutput>#Val(CampaignID)#</cfoutput>';
			features = 'scrollbars=yes,height=600,width=1000,top=100,left=100';
			popUpWin = window.open(popupURL,'PopUpWindow',features);
			if (window.focus) {
				popUpWin.focus();
			}
			if (popUpWin.opener == null){
				popUpWin.opener = window;
				popUpWin.opener.name = "PopUp";
			}	 
		}
	//-->
	</script>

</cf_head>

<cf_body onLoad="init();">
<CFSET Current = "promotionEditForm.CFM">
<cfinclude template = "promotionsTopHead.cfm">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>	
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<cfif IsDefined("Client.promotion.CampaignIDList") AND ListLen(Client.promotion.CampaignIDList, "~") GT 1>
				<cfset CurrentIndex = ListFind(Client.promotion.CampaignIDList, CampaignID, "~")>
				<cfset PrevIndex = CurrentIndex - 1><cfset NextIndex = CurrentIndex + 1>
				<cfoutput>
				<cfif PrevIndex GT 0>
					<cfset PrevCampaignID = ListGetAt(Client.promotion.CampaignIDList, PrevIndex, "~")>
						<A HREF="promotionEditForm.cfm?CampaignID=#ListFirst(Client.promotion.CampaignIDList, '~')#" Class="Submenu">first</A><font color=white> |</font>
						<A HREF="promotionEditForm.cfm?CampaignID=#PrevCampaignID#" Class="Submenu">previous</A>					
				</cfif>
				<cfif NextIndex LTE ListLen(Client.promotion.CampaignIDList, "~")>
					<cfset NextCampaignID = ListGetAt(Client.promotion.CampaignIDList, NextIndex, "~")>
					<font color=white> |</font><A HREF="promotionEditForm.cfm?CampaignID=#NextCampaignID#" Class="Submenu">next</A><font color=white> |</font>
					<A HREF="promotionEditForm.cfm?CampaignID=#ListLast(Client.promotion.CampaignIDList, '~')#" Class="Submenu">last</A>
				</cfif>
				<cfif IsDefined("Client.promotionCriteria")>
					<font color=white> |</font><a href="promotionList.cfm" Class="Submenu">Back to List</a>
				</cfif>
					<font color=white> |</font><a href="promotionSearch.cfm" Class="Submenu">Back to Search</a><font color=white> |</font>	
					<a href="##" Class="Submenu" onClick="javascript:openWin();">Pop Up Administration</a>	
				</cfoutput>
			</cfif>	
		</TD>
	</TR>
</table>
<br>
<cfif Getpromotion.RecordCount>
<!--- NJH 2009-05-20 P-SNY069 - changed action from tabOnly.cfm --->
<form method="post" action="promotionList.cfm" name="EditForm" onSubmit="return validateForm();">
<cfoutput>
<CF_INPUT type="hidden" name="CampaignID" value="#CampaignID#">

<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td nowrap style="vertical-align: top;">CampaignID&nbsp;&nbsp;</td>
	<td>
		#HTMLEditFormat(Getpromotion.CampaignID)#
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">PromoID&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="PromoID" size="40" maxlength="50" value="#Getpromotion.PromoID#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">promoName&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="promoName" size="40" maxlength="50" value="#Getpromotion.promoName#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Basecurrency&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="Basecurrency" size="5" maxlength="3" value="#Getpromotion.Basecurrency#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">MaxOrderNo&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="MaxOrderNo" size="22" maxlength="20" value="#Getpromotion.MaxOrderNo#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Fulfilledby&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="Fulfilledby" size="12" maxlength="10" value="#Getpromotion.Fulfilledby#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">ShippingMatrixID&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="ShippingMatrixID" size="7" maxlength="5" value="#Getpromotion.ShippingMatrixID#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">InventoryID&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="InventoryID" size="22" maxlength="20" value="#Getpromotion.InventoryID#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="AskQuestions" value="1"<cfif Getpromotion.AskQuestions EQ "1"> checked</cfif>>AskQuestions	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="mandatoryContactPhone" value="1"<cfif Getpromotion.mandatoryContactPhone EQ "1"> checked</cfif>>Mandatory Contact Phone</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">QuestionIDs&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="QuestionIDs" size="40" maxlength="150" value="#Getpromotion.QuestionIDs#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">DistributorList&nbsp;&nbsp;</td>
	<td>
<!--- 		<input TYPE="text" name="DistributorList" size=150 maxlength="4000" value="#HTMLEditFormat(Getpromotion.DistributorList)#"> --->
		<textarea name="DistributorList" rows=5 cols=39 wrap="VIRTUAL">#HTMLEditFormat(Getpromotion.DistributorList)#</textarea>
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Mail&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="Mail" size="40" maxlength="150" value="#Getpromotion.Mail#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ZeroPrice" value="1"<cfif Getpromotion.ZeroPrice EQ "1"> checked</cfif>>ZeroPrice	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">PromoOrderType&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="PromoOrderType" size="3" maxlength="1" value="#Getpromotion.PromoOrderType#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">lastupdatedby&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="lastupdatedby" size="22" maxlength="20" value="#Getpromotion.lastupdatedby#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowProductGroup" value="1"<cfif Getpromotion.ShowProductGroup EQ "1"> checked</cfif>>ShowProductGroup	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowProductGroupScreen" value="1"<cfif Getpromotion.ShowProductGroupScreen EQ "1"> checked</cfif>>ShowProductGroupScreen	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowProductGroupHeader" value="1"<cfif Getpromotion.ShowProductGroupHeader EQ "1"> checked</cfif>>ShowProductGroupHeader	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowTermsConditions" value="1"<cfif Getpromotion.ShowTermsConditions EQ "1"> checked</cfif>>ShowTermsConditions	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowLineTotal" value="1"<cfif Getpromotion.ShowLineTotal EQ "1"> checked</cfif>>ShowLineTotal	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPageTotal" value="1"<cfif Getpromotion.ShowPageTotal EQ "1"> checked</cfif>>ShowPageTotal	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPictures" value="1"<cfif Getpromotion.ShowPictures EQ "1"> checked</cfif>>ShowPictures	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowProductCode" value="1"<cfif Getpromotion.ShowProductCode EQ "1"> checked</cfif>>ShowProductCode	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowProductCodeAsAnchor" value="1"<cfif Getpromotion.ShowProductCodeAsAnchor EQ "1"> checked</cfif>>ShowProductCodeAsAnchor	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDescription" value="1"<cfif Getpromotion.ShowDescription EQ "1"> checked</cfif>>ShowDescription	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPointsValue" value="1"<cfif Getpromotion.ShowPointsValue EQ "1"> checked</cfif>>ShowPointsValue	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowQuantity" value="1"<cfif Getpromotion.ShowQuantity EQ "1"> checked</cfif>>ShowQuantity	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDecimals" value="1"<cfif Getpromotion.ShowDecimals EQ "1"> checked</cfif>>ShowDecimals	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDiscountPrice" value="1"<cfif Getpromotion.ShowDiscountPrice EQ "1"> checked</cfif>>Show Discount Price	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowListPrice" value="1"<cfif Getpromotion.ShowListPrice EQ "1"> checked</cfif>>Show List Price	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowIllustrativePrice" value="1"<cfif Getpromotion.ShowIllustrativePrice EQ "1"> checked</cfif>>Show Illustrative Price<br><em>(This adds a column to the orders pages which contains an illustrative price in the users local currency, derived from the currency lookup table.)</em></td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowNoInStock" value="1"<cfif Getpromotion.ShowNoInStock EQ "1"> checked</cfif>>ShowNoInStock	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowMaxQuantity" value="1"<cfif Getpromotion.ShowMaxQuantity EQ "1"> checked</cfif>>ShowMaxQuantity	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPreviouslyOrdered" value="1"<cfif Getpromotion.ShowPreviouslyOrdered EQ "1"> checked</cfif>>ShowPreviouslyOrdered	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDistributorList" value="1"<cfif Getpromotion.ShowDistributorList EQ "1"> checked</cfif>>ShowDistributorList	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPaymentOptions" value="1"<cfif Getpromotion.ShowPaymentOptions EQ "1"> checked</cfif>>ShowPaymentOptions	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowOrderConfirmationScreen" value="1"<cfif Getpromotion.ShowOrderConfirmationScreen EQ "1"> checked</cfif>>ShowOrderConfirmationScreen	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowOrderNotes" value="1"<cfif Getpromotion.ShowOrderNotes EQ "1"> checked</cfif>>Show Notes Field on Orders screen<br><em>(This can be used for instructions relating to the order)</em></td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowOrderRelatedFiles" value="1"<cfif Getpromotion.ShowOrderRelatedFiles EQ "1"> checked</cfif>>Show Related Files link on orders screen<br><em>(This allows the user to upload files relating to the order)</em></td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPreviousOrdersLink" value="1"<cfif Getpromotion.ShowPreviousOrdersLink EQ "1"> checked</cfif>>ShowPreviousOrdersLink	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPreviousQuotesLink" value="1"<cfif Getpromotion.ShowPreviousQuotesLink EQ "1"> checked</cfif>>ShowPreviousQuotesLink	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowGrandTotal" value="1"<cfif Getpromotion.ShowGrandTotal EQ "1"> checked</cfif>>ShowGrandTotal	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowVATCalculation" value="1"<cfif Getpromotion.ShowVATCalculation EQ "1"> checked</cfif>>ShowVATCalculation	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDeliveryCalculation" value="1"<cfif Getpromotion.ShowDeliveryCalculation EQ "1"> checked</cfif>>ShowDeliveryCalculation	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowDownloadLink" value="1"<cfif Getpromotion.ShowDownloadLink EQ "1"> checked</cfif>>ShowDownloadLink	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="RequiresPurchaseRef" value="1"<cfif Getpromotion.RequiresPurchaseRef EQ "1"> checked</cfif>>RequiresPurchaseRef	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowPurchaseOrderRef" value="1"<cfif Getpromotion.ShowPurchaseOrderRef EQ "1"> checked</cfif>>ShowPurchaseOrderRef	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">ConfirmedPhrase&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="ConfirmedPhrase" size="40" maxlength="50" value="#Getpromotion.ConfirmedPhrase#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">OrderNoPrefix&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="OrderNoPrefix" size="40" maxlength="50" value="#Getpromotion.OrderNoPrefix#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowClosingMessage" value="1"<cfif Getpromotion.ShowClosingMessage EQ "1"> checked</cfif>>ShowClosingMessage	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="UsePoints" value="1"<cfif Getpromotion.UsePoints EQ "1"> checked</cfif>>UsePoints	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">PointsDetailURL&nbsp;&nbsp;</td>
	<td>
		<CF_INPUT type="text" name="PointsDetailURL" size="40" maxlength="100" value="#Getpromotion.PointsDetailURL#">
	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="UniqueTranslations" value="1"<cfif Getpromotion.UniqueTranslations EQ "1"> checked</cfif>>UniqueTranslations	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Unique Translation Suffix (Optional) &nbsp;&nbsp;</td>
	<td>
	<CF_INPUT type="text" name="UniqueTranslationSuffix" size="40" maxlength="100" value="#Getpromotion.UniqueTranslationSuffix#">
	<BR>(Can be used to share translations between promotions)
										
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="TranslateDescription" value="1"<cfif Getpromotion.TranslateDescription EQ "1"> checked</cfif>>TranslateDescription	</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="OrderSingleProduct" value="1"<cfif Getpromotion.OrderSingleProduct EQ "1"> checked</cfif>>OrderSingleProduct</td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="UseButtons" value="1"<cfif Getpromotion.UseButtons EQ "1"> checked</cfif>>Use Buttons<br><em>(This Sets the Promotion to use buttons instead of standard text)</em></td>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Promotion Group&nbsp;&nbsp;</td>
	<td>
		<select name="PromotionGroupId" size="1">
			<option value="">(Choose One:)</option>
				<cfloop query="GetPromotionGroup">
					<option value="#PromotionGroupId#"<cfif getPromotionGroup.PromotionGroupId EQ getPromotion.PromotionGroupId> selected</cfif>>#htmleditformat(Description)#</option>
				</cfloop>
		</select>
</tr>
<tr>
	<td nowrap style="vertical-align: top;">Use Promotion &nbsp;&nbsp;</td>
	<td>
		<select name="UsePromoId" size="1">
			<option value="">(Choose One:)</option>
				<cfloop query="GetOtherPromotion">
					<option value="#PromoId#"<cfif getOtherPromotion.PromoId EQ getPromotion.UsePromoId> selected</cfif>>#htmleditformat(PromoId)#</option>
				</cfloop>
		</select>	
	</TD>	
</tr>
<tr>
	<td nowrap style="vertical-align: top;">&nbsp;&nbsp;</td>
	<td>
		<input type="checkbox" name="ShowChangeItemsInOrder" value="1"<cfif Getpromotion.ShowChangeItemsInOrder EQ "1"> checked</cfif>>ShowChangeItemsInOrder</td>
</tr>
<!--- AR 2005-04-25 Add the UseGlobalSKU functionality. --->
<tr><td nowrap style="vertical-align: top;">Use Global SKU &nbsp;&nbsp;
	<br><em>Once set CANNOT be unset, so make sure you know what you are doing.</em></td>
	<td>
		<input type="checkbox" name="UseGlobalSKU" value="1"<cfif Getpromotion.UseGlobalSKU EQ "1"> checked</cfif>>UseGlobalSKU
	</TD>	</tr>
<tr><td nowrap style="vertical-align: top;">Global Regions (with identified currencies)&nbsp;&nbsp;<br><em>Only used when UseGlobalSKU set.</em> </td>
	<td>
	<cfscript>
		availableRegionsQry = application.com.relayCountries.getRegions(withCurrencies = true);
		selectedRegionsQry = application.com.relayEcommerce.GetPromotionGlobalSKURegions(campaignID = Getpromotion.CampaignID);
		regionList = valuelist(selectedRegionsQry.countryID);
	</cfscript>
	<!--- Get the set of available currencies. --->
		<select name="GlobalSKURegions" size="5" multiple>
			<option value="">(Choose Atleast One)</option>
				<cfloop query="availableRegionsQry">
					<option value="#availableRegionsQry.countryID#"<cfif listfind(regionList,availableRegionsQry.countryID)> selected</cfif>>#htmleditformat(countryDescription)# (#htmleditformat(CountryCurrency)#)</option>
				</cfloop>
		</select>	
	</td>
	
	</tr>
	<!---  --->
	<!--- 
	NJH 2010-10-13 - LID 3896 - removed the Global SKU Default Currency as it's never been used in anger, in Gawain's words
	<tr><td nowrap style="vertical-align: top;">Global SKU Default Currency&nbsp;&nbsp;<br><em>When a country is not contained in any of <br>the regions, then this currency is used.</em> </td>
	<td>
	<cfscript>
		availableCurrencyQry = application.com.currency.getCurrencies();
		if (len(Getpromotion.GlobalSKUDefaultCurrency)){
			defaultGlobalSkuCurrency = Getpromotion.GlobalSKUDefaultCurrency;
		}
		else {
			defaultGlobalSkuCurrency = "USD";		
		}
	</cfscript>
	<!--- Get the set of available currencies. --->
		<select name="GlobalSKUDefaultCurrency">
				<cfloop query="availableCurrencyQry">
					<option value="#availableCurrencyQry.symbol#"<cfif defaultGlobalSkuCurrency eq availableCurrencyQry.symbol> selected</cfif>>#availableCurrencyQry.symbol# : #availableCurrencyQry.name#</option>
				</cfloop>
		</select>	
	</td> --->
	
	</tr>
	<tr><td>Global SKU Accessories</td>
	<td>
		<select name="accessoryMode">
			<option value="1"<cfif Getpromotion.accessoryMode eq 1>selected</cfif>>1st Generation (Products cannot be accessories)</option>
			<option value="2"<cfif Getpromotion.accessoryMode eq 2>selected</cfif>>2nd Generation (Parent Child)</option>
	<!--- 
			<option value="3">3rd generation (Grandparent Parent Child) [not implemented]</option> --->
	
		</select>
	</td></tr>
<!--- Show alert if too many records returned --->
<!--- <cfif GetBorderss.RecordCount GT MaxInSelect AND BordersFilterCriteria NEQ "">
	<SCRIPT type="text/javascript">
	<!--
	alert("Too many records were returned by this filter.  Please try your search again.");
	//-->
	</SCRIPT>
</cfif>

<tr>
	<td>BorderSet&nbsp;&nbsp;</td>
	<td>
		<!--- Display filter criteria field if necessary --->
		<cfif BordersRecCount GT MaxInSelect OR IsDefined("FORM.BordersFilterCriteria")>
			<br>
			<cfparam name="BordersFilterCriteria" default="">
			Filter: <input type="text" size="6" name="BordersFilterCriteria" value="#HTMLEditFormat(BordersFilterCriteria)#" onChange="changeFilter(this);">
		</cfif>
		<!--- <input type="hidden" name="p__BorderSet" value=""> --->
		<select name="BordersMenu" size="1">
			<option value="">(Choose One:)</option>
				<cfif GetBorderss.RecordCount LTE MaxInSelect>
				<cfloop query="GetBorderss">
					<cfset IsCurrentRecord = 0>
					<cfif GetBorderss.BorderSet EQ Getpromotion.p__BorderSet>
						<cfset IsCurrentRecord = 1>
					</cfif>
					<option value="#CurrentRow#"<cfif IsCurrentRecord EQ 1> selected</cfif>>#BorderSet#</option>
				</cfloop>
			</cfif>
		</select>
	</td>
</tr> --->
</cfoutput>
<!--- CPS 2001-05-30 comment out the next bit of code and replace with call to allflag --->
<!--- <cfset flagList = ''>
<cfloop query="PromotionFlags">
<tr>
	<td nowrap style="vertical-align: top;"><cfoutput>#Name#</cfoutput></td>
	<td>
		<cfoutput><input type="text" name="flag#flagID#" value="#value#" size="10"></cfoutput>
	</td>
</tr>	
<cfset flagList = flagList & "," & flagID>
</cfloop>
<cfoutput><input type="hidden" name="flagList" value="#flagList#"></cfoutput> --->

<CFSET thisEntity = #Val(CampaignID)#>
<CFSET flagMethod = "edit">
<CFSET entityType = 11>

<TR>
	<TD>
	 	<CFINCLUDE TEMPLATE="../../flags/allFlag.cfm">
	</TD>
</TR>

<cfoutput>
	<!--- CPS 2001-05-30 hidden fields required by CF_AScreenUpdate --->
	<input type="hidden" name="frmTableList" value="Promotion">
 	<CF_INPUT type="hidden" name="frmCampaignIdList" value="#thisEntity#">	<!--- WAB 2012-04-17 CASE 427343 changed from frmPromotionIdList to frmCampaignIdList.  Promotion table had primary key of campaignID!  The updateData code must have started using the actually primaryKey name rather than the expected one --->
	<input type="hidden" name="frmPromotionFieldList" value="">	
	<CF_INPUT type="hidden" name="frmPromotionFlagList" value="#flagList#">		
<!---
	<input type="hidden" name="flagList" value="#flagList#"> List: #flaglist#<BR> --->

</cfoutput>

<tr>
	<td colspan="2" align="center">
		<input type="submit" value="Update Database">
	</td>
</tr>
</table>
</form>

<!--- <cfif IsDefined("Client.promotion.BorderSetList") AND ListLen(Client.promotion.BorderSetList, "~") GT 1>
	<form name="NavForm" action="promotionEditForm.cfm" method="GET">
	<input type="hidden" name="CampaignID" value="">
	<input type="hidden" name="NewRecord" value="1">
	<cfoutput>
	<table border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td>
			Go To BorderSet:&nbsp;
			<select name="ExpressMenu" size="1" onChange="ChangeRecord();">
				<cfloop index="Index_Num" from="1" to="#ListLen(Client.promotion.CampaignIDList, '~')#">
					<CFIF ListGetAt(Client.promotion.BorderSetList, Index_Num, '~') NEQ 'NoBorder'> 
		 				<option value="#Index_Num#"<cfif ListGetAt(Client.promotion.BorderSetList, Index_Num, '~') EQ Trim(Getpromotion.p__BorderSet)> selected</cfif>>#ListGetAt(Client.promotion.BorderSetList, Index_Num, '~')#</option>				 
					</cfif>
				</cfloop>
			</select>
		</td>
	</tr>
	</table>
	</cfoutput>
</cfif>  ---> 
<cfelse>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			That promotion does not exist.
		</td>
	</tr>
</cfif>



<!-- Promotions, Generated by DatabaseBlocks(TM) -->

