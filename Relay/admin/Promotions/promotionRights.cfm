<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2009-05-19	NJH	P-SNY069 - added promotionGroupTextID filter
 --->

<cfparam name="promotionGroupTextID" type="string" default="">

<!---Last phase, DELETE and INSERT if names were moved around so that 
the screen could refresh with new data--->
<CFIF IsDefined("Remove")>

	<cfif structKeyExists(form,"currentUsers")>
		<CFQUERY NAME="RemoveRights" datasource="#application.siteDataSource#">
			DELETE From RecordRights
			WHERE Entity = 'Promotion'
			AND UserGroupID in( <cf_queryparam value="#form.CurrentUsers#" CFSQLTYPE="CF_SQL_INTEGER" list="true"> )
			AND RecordID =  <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfif>

<CFELSEIF IsDefined("Add")>

	<cfif structKeyExists(form,"PossibleUsers")>
		<CFLOOP index="thisUser" list = "#form.PossibleUsers#">
			<CFQUERY NAME="AddRights" datasource="#application.siteDataSource#">
			INSERT INTO RecordRights(UserGroupID, Entity, RecordID)
			VALUES(<cf_queryparam value="#thisUser#" CFSQLTYPE="CF_SQL_INTEGER" >, 'Promotion', <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</CFQUERY>
		</cfloop>
	</cfif>
</CFIF>



<!---Get Promotions for the Combo Box--->
<CFQUERY NAME="Promotions" datasource="#application.siteDataSource#">
	Select CampaignID, PromoID, PromoName
	From promotion p
	<!--- NJH 2009-05-19 P-SNY069 --->
	<cfif promotionGroupTextID neq "">
		inner join promotionGroup pg with (noLock) on pg.promotionGroupId = p.promotionGroupID and pg.promotionGroupTextId =  <cf_queryparam value="#promotionGroupTextId#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfif>
	ORDER BY PromoID
</CFQUERY>

<!---Get Security Type ID for filtering only the people who have rights to Orders--->
<CFQUERY NAME="getSecurityTypeID" datasource="#application.siteDataSource#">
	SELECT SecurityTypeID
	FROM SecurityType
	WHERE ShortName = 'OrderTask'
</CFQUERY>



<SCRIPT>
	function formSubmit(){
	var form = window.document.ComboForm
	
	form.submit()
	}
</SCRIPT>


<CFSET Current = "promotionRights.CFM">
<cfinclude template = "promotionsTopHead.cfm">

<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td align='center'>
			<FORM METHOD="post" NAME="ComboForm">
				<SELECT NAME="Campaign" onChange="formSubmit()">
				<OPTION VALUE="0">Select a Promotion
				<CFOUTPUT QUERY="Promotions">
				<OPTION VALUE="#CampaignID#" <CFIF IsDefined("Campaign")><CFIF CampaignID is Campaign>SELECTED</CFIF></CFIF>>#htmleditformat(PromoName)#
				</CFOUTPUT>
				</SELECT>
			</FORM>
		</td>
	</tr>

</table>

<!---Second Phase, after a Criteria (eg. Promotion) was selected)--->
<CFIF IsDefined("Campaign")>
	<!---Get all the Users enabled for the given Promotion--->
	<CFQUERY NAME="Users" datasource="#application.siteDataSource#">
		SELECT DISTINCT ug.Name, ug.UserGroupID, isnull(ug.personid, -1) as personID
		FROM UserGroup AS ug, RecordRights AS rr
		WHERE ug.UserGroupID = rr.UserGroupID 
		AND rr.recordid =  <cf_queryparam value="#Campaign#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND entity = 'Promotion'
		order by ug.name
	</CFQUERY>
	
	
	<CFSET EnabledUserGroups = ValueList(Users.userGroupID)>
	
	
<!---Get all the Users with Rights on Orders but without rights on this Promotion--->
	<CFQUERY NAME="PossibleUsers" datasource="#application.siteDataSource#">
		SELECT '&nbsp;&nbsp;' + Name as Name, UserGroupID,5 as sortOrder
		FROM UserGroup
		WHERE PersonID IN (SELECT PersonID
      					   FROM RightsGroup
      					   WHERE UserGroupID IN (SELECT r.UsergroupID
             									 FROM Rights AS r, UserGroup AS ug
												 WHERE r.UserGroupID = ug.UsergroupID
             									 AND r.SecurityTypeID =  <cf_queryparam value="#GetSecurityTypeID.SecurityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
		<CFIF EnabledUserGroups IS NOT ''>	
		AND userGroupid  NOT IN ( <cf_queryparam value="#EnabledUserGroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>	
		AND personID IS NOT NULL


		UNION

		SELECT '&nbsp;&nbsp;' + Name, UserGroupID,2
		FROM 
			UserGroup
		WHERE 
			personid is null
			<CFIF EnabledUserGroups IS NOT ''>	
			AND userGroupid  NOT IN ( <cf_queryparam value="#EnabledUserGroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFIF>	
			



		UNION
			SELECT '----------------',0,3

		UNION
			SELECT 'User Groups',0,1

		UNION
			SELECT 'Individuals',0,4

		ORDER BY SortOrder, name
				
	</CFQUERY>
	
	
	
<FORM METHOD="post">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
		<TR>
			<TD VALIGN="top">
			
			<CFIF Users.RecordCount IS NOT 0>
			
			<!---Display Users with rights for the given Campaign...--->
			<SELECT NAME="CurrentUsers" MULTIPLE <CFIF Users.RecordCount LT 10> SIZE="<CFOUTPUT>#Users.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
				<CFOUTPUT QUERY="Users">
				<OPTION VALUE="#usergroupID#">#application.com.security.sanitiseHTML(Name)#
				</CFOUTPUT>
			</SELECT>
			<CFELSE>
			No user is enabled for this campaign
			</CFIF>
			</TD>
	
	
			<TD VALIGN="top">
			<!---...and Possible Users (Users with rights to Orders, but not to this Campaign)--->
			<SELECT NAME="PossibleUsers" MULTIPLE <CFIF PossibleUsers.RecordCount LT 10> SIZE="<CFOUTPUT>#PossibleUsers.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
				<CFOUTPUT QUERY="PossibleUsers">
				<OPTION VALUE="#UserGroupID#">#application.com.security.sanitiseHTML(Name)#
				</CFOUTPUT>
			</SELECT>
			</TD>
		</TR>
		<TR>
			<TD>
			<INPUT TYPE="submit" NAME="Remove" VALUE="Remove Rights">
			</TD>
			<TD>
			<INPUT TYPE="submit" NAME="Add" VALUE="Add Rights">
			</TD>
		</TR>
	</TABLE>
	
	<CFOUTPUT>
	<CF_INPUT TYPE="hidden" NAME="Campaign" VALUE="#Campaign#">
	</CFOUTPUT>
	
	
	
	</FORM>
	
	
	
</CFIF>




