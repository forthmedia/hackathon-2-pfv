<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	promotionPopUpEdit.cfm
Author:		Brendan Doherty		
Date created:	2001-05-01

	Objective - Promotional pop up edit Page.
				Allows users to edit the pop up details
	
	Rationale - Requested as part of TradeUp enhancements

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-05-09		WAB			I have made the enddate the date entered plus time of 23:59:59.  As it was the last day of a promotion was being missed.  Could have altered the query which brings back the data, but decided to do it at this end.

Enhancement still to do:
--->

<!--- this page submits a form to itself so handle any updates --->
<cfif isDefined('frmTask') and (frmTask eq 'Update')>
	<!--- Update the triggers --->
	<CFQUERY NAME="updateTrigger" datasource="#application.siteDataSource#">
		Update PromotionPopUpTrigger
		set PopUpCategoryID =  <cf_queryparam value="#frm_category#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		TriggerValue = <cfif isDefined('Triggers')>'#Triggers#'<cfelse>''</cfif>
		where PopUpID =  <cf_queryparam value="#frm_popUpID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<!--- Update the Pop up --->
	<CFQUERY NAME="updatePopUp" datasource="#application.siteDataSource#">
		Update PromotionPopUp
		set Startdate = #CreateODBCDateTime(CreateDate(frm_startyear, frm_startmonth, frm_startday))#,
		Enddate = #CreateODBCDateTime(CreateDate(frm_endyear, frm_endmonth, frm_endday) + createTime(23,59,59))#
		where PopUpID =  <cf_queryparam value="#frm_popUpID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>	
	<!--- Update any phrases --->
	<CF_GetPhraseV2	Action="Update">
</cfif>

<!--- get Translations for fields which require them --->
<CFSET translationsRequired = "">
<CFSET translationsRequired = listAppend(translationsRequired,"Detail")>
<CFPARAM name="frmLanguage" default="1">
<CFPARAM name="frmCountry" default="0">

<CFIF translationsRequired is not "">
	<CF_GetPhraseV2	Action="List"
				PhraseList="#translationsRequired#"
				EntityType="promotionPopUp"
				EntityID="#frm_popupID#"
				Language="1"
				CountryID = "0"
				>

	<CF_GetPhraseV2	Action="List"
				PhraseList="#translationsRequired#"
				EntityType="promotionPopUp"
				EntityID="#frm_popupID#"
				Language="#frmLanguage#"
				CountryID = "#frmCountry#"
				>
</cfif>		

<!--- Only display languages the user has rights to --->		
<CFQUERY NAME="getLanguageRights" datasource="#application.siteDataSource#">
	SELECT 
		convert(varchar(3),l.Languageid) + '#application.delim1#' + l.language as languages,
		l.language
	FROM 
		Language as l, 
		booleanflagdata as bfd, 
		flag as f, 
		flaggroup as fg
	WHERE 
		fg.flaggrouptextid='LanguageRights'
		and fg.flaggroupid = f.flaggroupid
		and f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
	UNION
		SELECT '1#application.delim1#English', 'English'
		Order by 	Language
</CFQUERY>
<CFSET validLanguages = valuelist(getLanguageRights.languages)>				


<CFQUERY NAME="GetPromotion" datasource="#application.siteDataSource#">
	SELECT promoName from Promotion where CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFQUERY NAME="getThisPopUp" datasource="#application.siteDataSource#">
	Select * 
	from promotionPopUp
	where PopUpID =  <cf_queryparam value="#frm_popupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="getThisPopUpTrigger" datasource="#application.siteDataSource#">
	Select *
	from promotionPopUpTrigger PopUpTrigger
	where PopUpID =  <cf_queryparam value="#frm_popupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>			

<CFQUERY NAME="Categories" datasource="#application.siteDataSource#">
	Select * 
	from PromotionPopUpCategory
	where CampaignID =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
	
	


<cf_head>
	<cf_title>Pop Up Promotion Administration</cf_title>

<CFOUTPUT>
<SCRIPT LANGUAGE="JavaScript">
<!--- helper function to allow submiting a form by clicking on a link --->
function sendForm(formaction){
	document.forms[0].action = formaction;
	document.forms[0].submit();
}

<!--- on Submit make sure the fields are valid --->
function validateForm() {
	error = false;
	errorMsg = "";
	checkedcategory = false;	
	
	//triggers
	for(i=0;i<document.form1.Triggers.length;i++){
		// remove the netscape spacer
		if (document.form1.Triggers.options[i].value == ''){	
			document.form1.Triggers.options[i] = null				
		}
	}
	if (!document.form1.Triggers.length){
		error = true;
		errorMsg = 'You must select a pop up trigger';			
	}
	
	// the categories
	for(i=0;i<document.form1.frm_category.length;i++){
		if (document.form1.frm_category[i].checked){		
			checkedcategory = true;	
		}
	}
	if (!checkedcategory){
		error = true;
		errorMsg = 'You must select a category';			
	}	
	
	<!--- // the detail
	if (document.form1.#evaluate("fld_Detail_promotionPopUp_#frm_campaignID#_#frmlanguage#_#frmCountry#")#.value == ''){
		error = true;
		errorMsg = 'You must enter pop up promotion detail';		
	} --->
		
	//the dates
	checkDates();
			
	// show message if error
	if(error){
		alert(errorMsg);
	}
	else{
		SelAll(); 
		document.form1.submit();
	}
}

<!--- helper function to check dates --->
function checkDates() {
	if(!checkDate(document.form1.frm_startday.value,document.form1.frm_startmonth.value,document.form1.frm_startyear.value)){
		error = true;
		errorMsg = 'Invalid start date';
	}
	else{
		if(!checkDate(document.form1.frm_endday.value,document.form1.frm_endmonth.value,document.form1.frm_endyear.value)){
			error = true;
			errorMsg = 'Invalid end date';
		}
	}
}

<!--- helper function to check dates --->
function checkDate(day,month,year) {
	
	if(isNaN(day) || isNaN(day) || isNaN(day)){
		error = true;
	}
	

	var myDay = parseInt(day);
	var myMonth =  parseInt(month);
	var myYear =  parseInt(year);
	
	/* Using form values, create a new date object
	which looks like "Wed Jan 1 00:00:00 EST 1975". */
	var myDate = new Date(year,month,day,0,0,0,0);  

	/* If we entered "Feb 31, 1975" in the form, the "new Date()" function
	converts the value to "Mar 3, 1975". Therefore, we compare the day
	in the array with the day we entered into the form. If they match,
	then the date is valid, otherwise, the date is NOT valid. 
	Fri Apr 27 00:00:00 UTC+0100 2001
	*/
	<!--- alert(myDate_array[1] + "!=" +myDayStr ); --->
	if ((myDate.getDate() != myDay) || (myDate.getMonth() != myMonth) || (myDate.getFullYear() != myYear)) {
	  return false;
	  alert(myDate.getDate() + " != " + myDay);
	  alert(myDate.getMonth() + " != " + myMonth);
	  alert(myDate.getFullYear() + " != " + myYear);
	} else {
	  return true;
	}

 
} 

<!---  used by the two selects functions--->
function saveContinue() {
	validateForm();
}		

<!--- USed to move the triggers about --->
<!--- Taken from 2selectrelated.js  --->
function moveFromTo(formName,fromName,toName) {
	fromObject =  eval('document.'+formName +'.' + fromName)
	toObject =  eval('document.'+formName +'.' + toName)
	SelFrom = fromObject.options.selectedIndex;
	SelTo = toObject.options.length+1;

	if ((SelFrom >= 0) && (SelFrom < fromObject.options.length)) {			
		toObject.options.length = toObject.options.length + 1;
		toObject.options[SelTo-1].value = fromObject.options[SelFrom].value;
		toObject.options[SelTo-1].text = fromObject.options[SelFrom].text;
			
		for (i=SelFrom; i<fromObject.options.length-1; i++) {
			fromObject.options[i].value = fromObject.options[i+1].value;
			fromObject.options[i].text = fromObject.options[i+1].text;
		}
		fromObject.options[i] = null
		if (SelFrom > 0) fromObject.options.selectedIndex = SelFrom-1
		else fromObject.options.selectedIndex = 0;
			
	}
	with (document.form1) {
		SelLeft = fromName.options.selectedIndex;
		SelRight = toName.options.length;
		if ((SelLeft >= 0) && (SelLeft < fromName.options.length-1)) {
			toName.options.length = toName.options.length + 1;
			toName.options[SelRight-1].value = fromName.options[SelLeft].value;
			toName.options[SelRight-1].text = fromName.options[SelLeft].text;
	
			for (i=SelLeft; i<fromName.options.length-1; i++) {
				fromName.options[i].value = fromName.options[i+1].value;
				fromName.options[i].text = fromName.options[i+1].text;
			}
			fromName.options[i] = null
			if (SelLeft > 0) fromName.options.selectedIndex = SelLeft-1
			else fromName.options.selectedIndex = 0;
		}
	}
}

function ToLeft() {
	with (document.form1) {
		SelRight = toName.options.selectedIndex;

		if ((SelRight >= 0)  && (toName.options.selectedIndex + 1 < toName.options.length)) {
			fromName.options[fromName.options.length-1].value = toName.options[toName.options.selectedIndex].value;
			fromName.options[fromName.options.length-1].text = toName.options[toName.options.selectedIndex].text;
			fromName.options.length = fromName.options.length + 1;

			for (i=SelRight;i<toName.options.length-1; i++) {
				toName.options[i].value = toName.options[i+1].value;
				toName.options[i].text = toName.options[i+1].text;
			}
			toName.options[i] = null

			if (SelRight > 0) toName.options.selectedIndex = SelRight-1
			else toName.options.selectedIndex = 0;
		}
	}
}

function Move(formName,objectName,moveIndex) {
	thisObject =  eval('document.'+formName +'.' + objectName)

	Sel   = thisObject.options.selectedIndex;
	SelTo = thisObject.options.selectedIndex + moveIndex;
	Last = thisObject.length
	if ((SelTo >= 0) && (SelTo <= Last-1)) {
		tempvalue  = thisObject.options[Sel].value;
		temptext  = thisObject.options[Sel].text;
		thisObject.options[Sel].value    = thisObject.options[SelTo].value;
		thisObject.options[Sel].text    = thisObject.options[SelTo].text;
		thisObject.options[SelTo].value  = tempvalue;
		thisObject.options[SelTo].text  = temptext;
		thisObject.options.selectedIndex = SelTo;
	}		
}
 
function SelAll_(formName,objectName) {
	thisForm = eval('document.'+formName) 
	for (thisElement=0; thisElement<thisForm.elements.length; thisElement++) {
		if  ( thisForm.elements[thisElement].name.substr(0,objectName.length) == objectName) {
			if (thisForm.elements[thisElement].type== 'select-multiple') {
				thisObject =  thisForm.elements[thisElement]
				thisObject.multiple = true;
				for (i=0; i<thisObject.options.length; i++) {
					thisObject.options[i].selected = true;
				}		
			}
		}
	}
}

function SelAll() {
	SelAll_('form1', 'Triggers') 
}

</script>
</CFOUTPUT>
</cf_head>


<br>
<!--- Standard heading --->
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Pop Up Promotion Administration - <cfoutput>#htmleditformat(GetPromotion.promoName)# - #htmleditformat(getThisPopUp.strapLine)#</cfoutput></DIV>
		</TD>
	</TR>
	<TR>	
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<cfoutput><a href="/admin/promotions/promotionPopUp.cfm?frm_CampaignID=#frm_CampaignID#&frm_popupID=#frm_popupID#" Class="Submenu">pop up administration</a><font color=white> |</font></cfoutput>
			<a href="javascript:sendForm('<cfoutput></cfoutput>/admin/promotions/promotionPopUpNew.cfm');" Class="Submenu">new</a><font color=white> |</font>			
			<A HREF="javascript:self.close();" Class="Submenu">close</A>
		</TD>
	</TR>
</table>

<br>
<form action="<cfoutput></cfoutput>/admin/promotions/promotionPopUpEdit.cfm" method="post" name="form1" id="form1">
<cfoutput><CF_INPUT type="hidden" name="frm_campaignID" value="#frm_campaignID#"></cfoutput>
<input type="hidden" name="frmTask" value="update">
<cfoutput><CF_INPUT type="hidden" name="frm_popupID" value="#frm_popupID#"></cfoutput>
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td>
		From
	</td>
	<td colspan="3">
		<cfoutput><CF_INPUT type="text" name="frm_startday" value="#Datepart('d', getThispopUp.startdate)#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_startmonth" value="#Datepart('m', getThispopUp.startdate)#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_startyear" value="#Datepart('yyyy', getThispopUp.startdate)#" size="4" maxlength="4"></cfoutput>
	</td>
</tr>
<tr>
	<td>
		To
	</td>
	<td colspan="3">
		<cfoutput><CF_INPUT type="text" name="frm_endday" value="#Datepart('d', getThispopUp.enddate)#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_endmonth" value="#Datepart('m',getThispopUp.enddate)#" size="2" maxlength="2"><b>/</b><CF_INPUT type="text" name="frm_endyear" value="#Datepart('yyyy', getThispopUp.enddate)#" size="4" maxlength="4"></cfoutput>
	</td>
</tr>
<tr>
	<td>
		Language
	</td>
	<td colspan="3">
		<cf_displayValidValues 
			formFieldName = "frmLanguage" 
			validValues="#validLanguages#" 
			currentValue="#frmLanguage#" 
			onchange="javascript:saveContinue()"> 		
	</td>
</tr>
<tr>
	<td valign="top">
		Detail<br><br>
		<cfoutput>
		<CFIF frmlanguage is not 1>
			<A HREF="/HTMLEditor/HTMLEditPhrase.cfm?HTMLEditAction=updatePhrase&caller=popupEdit&phrasetextid=Detail&entityType=promotionPopUp&entityid=#frm_popupID#&languageid=#frmlanguage#&frm_CampaignID=#frm_CampaignID#">	<IMG SRC="/images/BUTTONS/editor.gif" WIDTH=127 HEIGHT=30 BORDER=0 ALT="Edit as HTML"></A>			
		<cfelse>
			<A HREF="/HTMLEditor/HTMLEditPhrase.cfm?HTMLEditAction=updatePhrase&caller=popupEdit&phrasetextid=Detail&entityType=promotionPopUp&entityid=#frm_popupID#&languageid=1&frm_CampaignID=#frm_CampaignID#">	<IMG SRC="/images/BUTTONS/editor.gif" WIDTH=127 HEIGHT=30 BORDER=0 ALT="Edit as HTML"></A>			
		</cfif>
		</cfoutput>		
	</td>
	<td colspan="3">
		<CFIF frmlanguage is not 1>
			<!--- show the English Translation if translating into another language --->
			<cfoutput>(English: #htmleditFormat(evaluate("phr_Detail_promotionPopUP_#htmleditformat(frm_popupID)#_1_0"))# )<BR></cfoutput>
		</cfif>
		<cfoutput><textarea cols="50" rows="10" name="#evaluate("fld_Detail_promotionPopUP_#frm_popupID#_#frmlanguage#_#frmCountry#")#">#evaluate("phr_Detail_promotionPopUp_#frm_popupID#_#frmlanguage#_#frmCountry#")#</textarea></cfoutput>
	</td>
</tr>
<tr>
	<td>
		Pop Up Trigger
	</td>	
	<cfloop query="Categories">
	<td>
		<cfoutput><CF_INPUT type="radio" name="frm_category" value="#PopUpCategoryID#" onClick="init#PopUpCategoryName#('form1','All','Triggers');" CHECKED="#iif( PopUpCategoryID eq getThisPopUpTrigger.PopUpCategoryID,true,false)#">#htmleditformat(PopUpCategoryName)#</cfoutput>
	</td>
	</cfloop>
</tr>

<cfset first = true>
<cfloop query="Categories">
	<cfif CategoryType eq 'SQL'>
		<!--- Run the SQL it returns popuptrigger --->
		<CFQUERY NAME="All" datasource="#application.siteDataSource#">
			#preservesinglequotes(Categories.CategorySQL)#
		</CFQUERY>			
		<!--- Create a List as its easier for javascript to check against --->
		<cfset amendedcategoryList = "">
		<cfloop query="ALL">
			<cfset amendedcategoryList =  ListAppend(amendedcategoryList,popuptrigger)>
		</cfloop>
	<cfelse>
		<cfset amendedcategoryList = Categories.CategoryList>
	</cfif>
	
	<cfif PopUpCategoryID eq getThisPopUpTrigger.PopUpCategoryID>
		<CFQUERY NAME="Selected" datasource="#application.siteDataSource#">
			Select triggervalue
			from promotionpopuptrigger
			where popupID =  <cf_queryparam value="#frm_popupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>	
		<cfset selectedtriggerList = Selected.triggervalue>
		<!--- remove selected values from the the options --->
		<cfloop index="popuptrigger" list="#selectedtriggerList#">
			<cfif ListFindNoCase(amendedcategoryList, popuptrigger)>
				<cfset amendedcategoryList = ListDeleteAt(amendedcategoryList, ListFindNoCase(amendedcategoryList, popuptrigger))>
			</cfif>
		</cfloop>	
		<tr>		
			<td colspan="4" align="center">
				<table>
				<tr>
					<td>
						<select name="All" size="6">
						<cfloop index="popuptrigger" list="#amendedcategoryList#">
							<cfoutput><OPTION VALUE="#popuptrigger#">#htmleditformat(popuptrigger)#</cfoutput>
						</cfloop>
						</SELECT>
					</TD>
					<TD>
						<cf_INPUT TYPE="BUTTON" VALUE=">>" onClick="moveFromTo('form1','All','Triggers')"><BR>
						<cf_INPUT TYPE="BUTTON" VALUE="<<" onClick="moveFromTo('form1','Triggers','All')">
					</TD>
					<TD>
						<SELECT NAME="Triggers" SIZE="6"  MULTIPLE >
							<cfif ListLen(selectedtriggerList)>
								<cfloop index="trigger" list="#selectedtriggerList#">
									<cfoutput><OPTION VALUE="#trigger#">#htmleditformat(trigger)#</cfoutput>
								</cfloop>	
							<cfelse>
								<OPTION VALUE="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
							</cfif>
						</SELECT>
					</td>
				</tr>	
				</table>	
			</td>		
		</tr>	
		<cfset first = 'false'>	
	<cfelse>
		<cfset selectedtriggerList = "">
	</cfif>
	<script>
	<cfoutput>
		function init#PopUpcategoryName#(formName,fromName,toName){
			thisForm = eval('document.'+formName) 
			fromObject =  eval('document.'+formName +'.' + fromName)
			toObject =  eval('document.'+formName +'.' + toName)			

			if (toObject.options.length > 0){
				alert("You will lose all your selections if you do this");
			}			

			fromObject.options.length = #ListLen(amendedcategoryList)#;
			<cfset i = 0>      
			<cfloop index="popuptrigger" list="#amendedcategoryList#">
				fromObject.options[#i#].value = "#popuptrigger#";
				fromObject.options[#i#].text = "#popuptrigger#";
				<cfset i = i + 1>  
			</cfloop>	
			
			toObject.options.length = #ListLen(selectedtriggerList)#;
			<cfset i = 0>      
			<cfloop index="popuptrigger" list="#selectedtriggerList#">
				toObject.options[#i#].value = "#popuptrigger#";
				toObject.options[#i#].text = "#popuptrigger#";
				<cfset i = i + 1>  
			</cfloop>									
		}
	</script>	
	</cfoutput>
</cfloop>
<cfif first>	
	<tr>
		<td align="center">
			<table>
			<tr>
				<td>
					<select name="All" size="6">
					</SELECT>
				</TD>
				<TD>
					<cfoutput><INPUT TYPE="BUTTON" VALUE=">>" onClick="moveFromTo('form1','All','Triggers')"><BR>
					<INPUT TYPE="BUTTON" VALUE="<<" onClick="moveFromTo('form1','Triggers','All')"></cfoutput>
				</TD>
				<TD>
					<SELECT NAME="Triggers" SIZE="6"  MULTIPLE >
					</SELECT>
				</td>
			</tr>	
			</table>	
		</td>		
	</tr>	
	<cfset first = 'false'>
</cfif>	
<tr>
<td colspan="4" align="center">
	<input type="button" name="frmTask" value="update" onClick="validateForm();">
</td>
</tr>
</table>
</form>


