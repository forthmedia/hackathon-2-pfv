<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:	showpromotionPopUp.cfm
Author:		Brendan Doherty		
Date created:	2001-05-01

	Objective - Promotional pop display page.
				Allows users to previee the pop up window in the choosen language
	
	Rationale - Requested as part of TradeUp enhancements

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:
--->

<!--- get Translations and fields which require them --->
<CFSET translationsRequired = "">
<CFSET translationsRequired = listAppend(translationsRequired,"Detail")>
<CFPARAM name="frmLanguage" default="1">
<CFPARAM name="frmCountry" default="0">
	
<CFIF translationsRequired is not "">
	<CF_GetPhraseV2	Action="List"
				PhraseList="#translationsRequired#"
				EntityType="promotionPopUp"
				EntityID="#frm_popupID#"
				Language="1"
				CountryID = "0"
				>

	<CF_GetPhraseV2	Action="List"
				PhraseList="#translationsRequired#"
				EntityType="promotionPopUp"
				EntityID="#frm_popupID#"
				Language="#frmLanguage#"
				CountryID = "#frmCountry#"
				>
</cfif>		
		
<CFQUERY NAME="getLanguageRights" datasource="#application.siteDataSource#">
	SELECT 
		convert(varchar(3),l.Languageid) + '#application.delim1#' + l.language as languages,
		l.language
	FROM 
		Language as l, 
		booleanflagdata as bfd, 
		flag as f, 
		flaggroup as fg
	WHERE 
		fg.flaggrouptextid='LanguageRights'
		and fg.flaggroupid = f.flaggroupid
		and f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#	
	UNION
		SELECT '1#application.delim1#English', 'English'
		Order by 	Language
</CFQUERY>
<CFSET validLanguages = valuelist(getLanguageRights.languages)>		
		
<CFQUERY NAME="GetCampaign" datasource="#application.siteDataSource#">
	SELECT PromoName
	from promotion
	where CampaignId =  <cf_queryparam value="#frm_campaignID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="GetPopUp" datasource="#application.siteDataSource#">
	SELECT * from PromotionPopUp where popUpID =  <cf_queryparam value="#frm_popupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>



<cf_head>
	<cf_title><cfoutput>#GetCampaign.promoName# - #GetPopUp.StrapLine#</cfoutput></cf_title>
	
<SCRIPT>
<!--- Forces the pop Up to the fron of the users Screen --->
function grabfocus(){
	self.name = "popUpWin";
	self.focus();
	self.setTimeout('grabfocus()', 1000);
}
</SCRIPT>
</cf_head>

<cf_body onload="grabfocus()">
<table width="100%" border="1">
<tr>
	<td height="300" valign="top">
	<cfoutput>#evaluate("phr_Detail_promotionPopUP_#htmleditformat(frm_popupID)#_#htmleditformat(frmlanguage)#_#htmleditformat(frmCountry)#")#</cfoutput>	
	</td>
</tr>
<tr>
	<td align="center">
	<a href="javascript:self.close();">Close Me</a>
	</td>
</tr>
</table>


