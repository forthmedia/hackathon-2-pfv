<!--- �Relayware. All Rights Reserved 2014 --->
<cfif isDefined('URL.StartRow') OR isDefined('Form.StartRow')>
	<cfif isDefined('URL.StartRow')>
		<cfset StartRow=URL.StartRow>
	<cfelse>
		<cfset StartRow=Form.StartRow>
	</cfif>
<cfelse>
	<cfset StartRow=1>
</cfif>

<CFQUERY name="GetRecords" datasource="#application.SiteDataSource#" >
	SELECT *
	FROM Application
</CFQUERY>
<!--- Table cell bg colors.
	  Escape the # sign with another # sign. BGColor1="##ffff99" --->
<cfset BGColor1="##FFFFFF">
<cfset BGColor2="##ffff99">

<cfset PageName="Application_List.cfm">
<cfset TotalRecords=GetRecords.RecordCount>
<cfset RecordsPerPage=25> 
<cfset NumberOfPages=Ceiling(TotalRecords/RecordsPerPage)>
<cfset CurrentPageNumber=Ceiling(StartRow/RecordsPerPage)>
<cfset EndRow=StartRow+RecordsPerPage-1>

<cf_title>Application - List Records</cf_title>

<cfinclude template="Application_Header.cfm">


<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<TR>
		<TH>Application short name</TH>
		<TH>Full name</TH>
		<TH>Login page</TH>
		<TH>ProcessID (click to edit)</TH>
		<TH>Purpose</TH>
	</TR>
	<CFOUTPUT query="GetRecords" startrow="#Variables.StartRow#" maxrows="#Variables.RecordsPerPage#">
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<td>
			<a href="Application_Edit.cfm?RecordID=#Ident#">#htmleditformat(appName)#</a>
		</td>
		<td>
			#htmleditformat(ApplicationFullName)#&nbsp;
		</td>
		<td>
			#htmleditformat(LoginPage)#&nbsp;
		</td>
		<td>
			<a href="../ProcessActions/listProcessActions.cfm?frmProcessID=#ProcessID#">#htmleditformat(ProcessID)#</a>
		</td>
		<td>
			#htmleditformat(Purpose)#&nbsp;
		</td>

	</tr>
</CFOUTPUT>
</TABLE>


<cfif NumberOfPages gt 1>
	<cfoutput>
	<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
    	<td width="150" align="right" nowrap>
			<cfif CurrentPageNumber gt 1>
				<cfset NextStartRow=Variables.StartRow - RecordsPerPage>
				<a href="#PageName#?StartRow=#Variables.NextStartRow#">Previous Page</a>
			</cfif>
		</td>
    	<td width="150" nowrap>
			<cfif CurrentPageNumber lt NumberOfPages>
				<cfset NextStartRow=Variables.StartRow + RecordsPerPage>
				<a href="#PageName#?StartRow=#Variables.NextStartRow#">Next Page</a>
			</cfif>
		</td>
	</tr>
	<tr>
    	<td width="300"  align="center" colspan="2">
			<cfloop index="Counter" from="1" to="#Variables.NumberOfPages#" step="1">
				<cfset NextStartRow=(Counter*Variables.RecordsPerPage)-(Variables.RecordsPerPage-1)>
				&nbsp;
				<cfif Counter eq CurrentPageNumber>
					#Counter#
				<cfelse>
					<a href="#PageName#?StartRow=#Variables.NextStartRow#">#htmleditformat(Counter)#</a>
				</cfif>
			</cfloop>
		</td>
	</tr>
</table>

</cfoutput>
</cfif>