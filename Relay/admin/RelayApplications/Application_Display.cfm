<!--- �Relayware. All Rights Reserved 2014 --->
<cfif  IsDefined('URL.RecordID') OR IsDefined('Form.RecordID')>
	<cfif IsDefined('URL.RecordID')>
		<cfset RecordID=URL.RecordID>
	<cfelse>
		<cfset RecordID=Form.RecordID>
	</cfif>
<cfelse>
	<cfabort showerror="The RecordID must be passed to this template">
</cfif>

<CFQUERY name="GetRecord" datasource="#application.SiteDataSource#" maxRows=1>
	SELECT *
	FROM Application
	WHERE Ident =  <cf_queryparam value="#Variables.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>



<cf_head>
	<cf_title>Application - Display Record</cf_title>
</cf_head>

<cfinclude template="Application_Header.cfm">

<FONT size="+2"><B>Display Record</B></FONT>-
<FONT size="+1">Application</FONT> <BR>
<table><tr><td>&nbsp;</td></tr></table>
<cfoutput query="GetRecord">

<table cellpadding="2">

	
	
	<TR>
	<TD valign="top" align="right"> <b>appName</b> </TD>
    <TD>
	
	
		#htmleditformat(appName)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>ApplicationFullName</b> </TD>
    <TD>
	
	
		#htmleditformat(ApplicationFullName)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginPage</b> </TD>
    <TD>
	
	
		#htmleditformat(LoginPage)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginMessagePhraseTextID</b> </TD>
    <TD>
	
	
		#htmleditformat(LoginMessagePhraseTextID)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>processid</b> </TD>
    <TD>
	
	
		#htmleditformat(processid)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>startstepid</b> </TD>
    <TD>
	
	
		#htmleditformat(startstepid)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>Purpose</b> </TD>
    <TD>
	
	
		#htmleditformat(Purpose)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>DefaultLanguageID</b> </TD>
    <TD>
	
	
		#htmleditformat(DefaultLanguageID)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>DefaultCountryID</b> </TD>
    <TD>
	
	
		#htmleditformat(DefaultCountryID)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>path</b> </TD>
    <TD>
	
	
		#htmleditformat(path)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>allowNewLocs</b> </TD>
    <TD>
	
	
		<CFIF #allowNewLocs# is 1>Yes </CFIF>> 
		<CFIF #allowNewLocs# is 0>No </CFIF>> 
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>allowNewPerson</b> </TD>
    <TD>
	
	
		<CFIF #allowNewPerson# is 1>Yes </CFIF>> 
		<CFIF #allowNewPerson# is 0>No </CFIF>> 
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>PeopleMaintScrn</b> </TD>
    <TD>
	
	
		<CFIF #PeopleMaintScrn# is 1>Yes </CFIF>> 
		<CFIF #PeopleMaintScrn# is 0>No </CFIF>> 
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>exitpage</b> </TD>
    <TD>
	
	
		#htmleditformat(exitpage)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>Parameters</b> </TD>
    <TD>
	
	
		#htmleditformat(Parameters)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>userCookie</b> </TD>
    <TD>
	
	
		#htmleditformat(userCookie)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>live</b> </TD>
    <TD>
	
	
		#htmleditformat(live)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>frmNextPage</b> </TD>
    <TD>
	
	
		#htmleditformat(frmNextPage)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginRequired</b> </TD>
    <TD>
	
	
		#htmleditformat(LoginRequired)#
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right"> <b>StartPage</b> </TD>
    <TD>
	
	
		#htmleditformat(StartPage)#
	
	</TD>
	</TR>
	

	
<tr><td colspan="2" align="center"><a href="Application_Edit.cfm?RecordID=#Ident#"><b>Edit</b></a></td></tr>
</TABLE>
</CFOUTPUT>

<cfinclude template="Application_Footer.cfm">


