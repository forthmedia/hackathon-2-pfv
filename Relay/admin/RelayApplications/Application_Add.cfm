<!--- �Relayware. All Rights Reserved 2014 --->
<cfif IsDefined('Form.Application_Add')>
	<cfquery name="AddRecord" datasource="#application.siteDataSource#">
		INSERT INTO Application
			(appName,ApplicationFullName,LoginPage,LoginMessagePhraseTextID,processid,startstepid,Purpose,DefaultLanguageID,DefaultCountryID,path,allowNewLocs,allowNewPerson,PeopleMaintScrn,exitpage,Parameters,userCookie,live,frmNextPage,LoginRequired,StartPage)
			VALUES(<cf_queryparam value="#Form.appName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.ApplicationFullName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.LoginPage#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.LoginMessagePhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.processid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Form.startstepid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Form.Purpose#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.DefaultLanguageID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Form.DefaultCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Form.path#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.allowNewLocs#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Form.allowNewPerson#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#Form.PeopleMaintScrn#" CFSQLTYPE="CF_SQL_char" >,<cf_queryparam value="#Form.exitpage#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.Parameters#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.userCookie#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.live#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.frmNextPage#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Form.LoginRequired#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#Form.StartPage#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</cfquery>
	<cfset Message=true>
</cfif>

<cf_title>Application - Edit Record</cf_title>

<cfinclude template="Application_Header.cfm">
<!--- not needed? because of new sub menus Brendan--->
<!--- <cfinclude template="Application_Header.cfm"> --->


<FONT size="+2"><B>Add Record</B></FONT>

<CFOUTPUT>
<cfif IsDefined('Message')>
<hr>
The Record has been added.
<hr>
</cfif>

<FORM action="Application_Add.cfm" method="post">
<INPUT type="hidden" name="Application_Add" value="1">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">	
	<TR>
	<TD valign="top" align="right"> <b>appName</b> </TD>
    <TD>
	
		<input type="text" name="appName" size="45" maxLength="25">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>ApplicationFullName</b> </TD>
    <TD>
	
		<input type="text" name="ApplicationFullName" size="45" maxLength="70">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginPage</b> </TD>
    <TD>
	
		<input type="text" name="LoginPage" size="45" maxLength="50">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginMessagePhraseTextID</b> </TD>
    <TD>
	
		<input type="text" name="LoginMessagePhraseTextID" size="45" maxLength="30">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>processid</b> </TD>
    <TD>
	
		<input type="text" name="processid" size="45" maxLength="4">
		
	</TD>
	<INPUT type="hidden" name="processid_integer">
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>startstepid</b> </TD>
    <TD>
	
		<input type="text" name="startstepid" size="45" maxLength="4">
		
	</TD>
	<INPUT type="hidden" name="startstepid_integer">
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>Purpose</b> </TD>
    <TD>
	
		<input type="text" name="Purpose" size="45" maxLength="100">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>DefaultLanguageID</b> </TD>
    <TD>
	
		<input type="text" name="DefaultLanguageID" size="45" maxLength="4">
		
	</TD>
	<INPUT type="hidden" name="DefaultLanguageID_integer">
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>DefaultCountryID</b> </TD>
    <TD>
	
		<input type="text" name="DefaultCountryID" size="45" maxLength="4">
		
	</TD>
	<INPUT type="hidden" name="DefaultCountryID_integer">
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>path</b> </TD>
    <TD>
	
		<input type="text" name="path" size="45" maxLength="100">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>allowNewLocs</b> </TD>
    <TD>
	
		<INPUT type="radio" name="allowNewLocs" value="1"checked> Yes
		<INPUT type="radio" name="allowNewLocs" value="0"> No
	
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>allowNewPerson</b> </TD>
    <TD>
	
		<INPUT type="radio" name="allowNewPerson" value="1"checked> Yes
		<INPUT type="radio" name="allowNewPerson" value="0"> No
	
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>PeopleMaintScrn</b> </TD>
    <TD>
	
		<INPUT type="radio" name="PeopleMaintScrn" value="1"checked> Yes
		<INPUT type="radio" name="PeopleMaintScrn" value="0"> No
	
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>exitpage</b> </TD>
    <TD>
	
		<input type="text" name="exitpage" size="45" maxLength="50">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>Parameters</b> </TD>
    <TD>
	
		<input type="text" name="Parameters" size="45" maxLength="255">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>userCookie</b> </TD>
    <TD>
	
		<input type="text" name="userCookie" size="45" maxLength="20">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>live</b> </TD>
    <TD>
	
		<input type="text" name="live" size="45" maxLength="1">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>frmNextPage</b> </TD>
    <TD>
	
		<input type="text" name="frmNextPage" size="45" maxLength="100">
		
	</TD>
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>LoginRequired</b> </TD>
    <TD>
	
		<input type="text" name="LoginRequired" size="45" maxLength="4">
		
	</TD>
	<INPUT type="hidden" name="LoginRequired_integer">
	</TR>
	


	
	
	
	<TR>
	<TD valign="top" align="right"> <b>StartPage</b> </TD>
    <TD>
	
		<input type="text" name="StartPage" size="45" maxLength="300">
		
	</TD>
	</TR>
	

	
</TABLE>
	
<!--- form buttons --->
<INPUT type="submit" name="btnEdit_OK" value=" Add Record ">

</FORM>
</CFOUTPUT>
<!--- not needed? because of new sub menus Brendan--->
