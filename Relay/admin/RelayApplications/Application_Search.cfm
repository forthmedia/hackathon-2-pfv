<!--- �Relayware. All Rights Reserved 2014 --->
<cfset ShowRecords=false>
<cfif IsDefined('Form.SearchRecords')>
	<cfif Form.SearchRecords IS NOT "">
		<cfquery name="GetRecords" datasource="#application.siteDataSource#">
			SELECT *
			FROM Application
			WHERE 1=2
			 OR Upper(appName)  LIKE  <cf_queryparam value="%DBUpperCaseFunctionName(#Form.SearchRecords#)%" CFSQLTYPE="CF_SQL_VARCHAR" >  OR Upper(DefaultLanguageID)  LIKE  <cf_queryparam value="%DBUpperCaseFunctionName(#Form.SearchRecords#)%" CFSQLTYPE="CF_SQL_VARCHAR" >  OR Upper(DefaultCountryID)  LIKE  <cf_queryparam value="%DBUpperCaseFunctionName(#Form.SearchRecords#)%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfquery>
		<cfset ShowRecords=true>
	</cfif>
</cfif>
<!--- Table cell bg colors.
	  Escape the # sign with another # sign. BGColor1="##ffff99" --->
<cfset BGColor1="##FFFFFF">
<cfset BGColor2="##ffff99">

<cf_title>Application - Search Records</cf_title>
<cfinclude template="Application_Header.cfm">
<!--- not needed? because of new sub menus Brendan--->
<!--- <cfinclude template="Application_Header.cfm"> --->

<FORM ACTION="Application_Search.cfm" METHOD="POST">
<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<TR>
	<td>Search</td>
    <TD><INPUT TYPE="Text" NAME="SearchRecords" SIZE="20"></TD>
</TR>
<TR>
    <TD  colspan="2" ALIGN="CENTER">
		<INPUT TYPE="Submit" NAME="" VALUE="Search">
	</TD>
</TR>
</TABLE>
</FORM>

<cfif ShowRecords>

	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
		<cfoutput>
		There are #GetRecords.RecordCount# records that match <b>#htmleditformat(Form.SearchRecords)#</b>
		</cfoutput>		
		</td>
	</tr>
	<CFOUTPUT query="GetRecords" >
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<td>			
			#htmleditformat(appName)#&nbsp;
				
			#htmleditformat(ApplicationFullName)#&nbsp;
				
			#htmleditformat(LoginPage)#&nbsp;
				
			#htmleditformat(startstepid)#&nbsp;
				
			#htmleditformat(Purpose)#&nbsp;

		</td>
		<td><a href="Application_Display.cfm?RecordID=#Ident#"> Display </a></td>
		<td><a href="Application_Edit.cfm?RecordID=#Ident#"> Edit </a></td>
	</tr>
</CFOUTPUT>
</TABLE>
</cfif>
