<!--- �Relayware. All Rights Reserved 2014 --->
<cfif  IsDefined('URL.RecordID') OR IsDefined('Form.RecordID')>
	<cfif IsDefined('URL.RecordID')>
		<cfset RecordID=URL.RecordID>
	<cfelse>
		<cfset RecordID=Form.RecordID>
	</cfif>
<cfelse>
	<cfabort showerror="The RecordID must be passed to this template">
</cfif>
<cfif IsDefined('Form.Application_Delete')>
	<cfquery name="DeleteRecord" datasource="#application.siteDataSource#">
		DELETE FROM Application
		WHERE Ident =  <cf_queryparam value="#Variables.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfset Deleted=true>
</cfif>


<cfif IsDefined('Form.Application_Edit')>
	<cfquery name="UpdateRecord" datasource="#application.siteDataSource#">
		UPDATE Application
			SET appName =  <cf_queryparam value="#Form.appName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,ApplicationFullName =  <cf_queryparam value="#Form.ApplicationFullName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,LoginPage =  <cf_queryparam value="#Form.LoginPage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,LoginMessagePhraseTextID =  <cf_queryparam value="#Form.LoginMessagePhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,processid =  <cf_queryparam value="#Form.processid#" CFSQLTYPE="CF_SQL_INTEGER" > ,startstepid =  <cf_queryparam value="#Form.startstepid#" CFSQLTYPE="CF_SQL_INTEGER" > ,Purpose =  <cf_queryparam value="#Form.Purpose#" CFSQLTYPE="CF_SQL_VARCHAR" > ,DefaultLanguageID =  <cf_queryparam value="#Form.DefaultLanguageID#" CFSQLTYPE="CF_SQL_INTEGER" > ,DefaultCountryID =  <cf_queryparam value="#Form.DefaultCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ,path =  <cf_queryparam value="#Form.path#" CFSQLTYPE="CF_SQL_VARCHAR" > ,allowNewLocs =  <cf_queryparam value="#Form.allowNewLocs#" CFSQLTYPE="cf_sql_integer" >,allowNewPerson=#Form.allowNewPerson#,PeopleMaintScrn =  <cf_queryparam value="#Form.PeopleMaintScrn#" CFSQLTYPE="CF_SQL_char" > ,exitpage =  <cf_queryparam value="#Form.exitpage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,Parameters =  <cf_queryparam value="#Form.Parameters#" CFSQLTYPE="CF_SQL_VARCHAR" > ,userCookie =  <cf_queryparam value="#Form.userCookie#" CFSQLTYPE="CF_SQL_VARCHAR" > ,live =  <cf_queryparam value="#Form.live#" CFSQLTYPE="CF_SQL_VARCHAR" > ,frmNextPage =  <cf_queryparam value="#Form.frmNextPage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,LoginRequired =  <cf_queryparam value="#Form.LoginRequired#" CFSQLTYPE="CF_SQL_Integer" > ,StartPage =  <cf_queryparam value="#Form.StartPage#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			WHERE Ident =  <cf_queryparam value="#Variables.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfset Updated=true>
</cfif>

<CFQUERY name="GetRecord" datasource="#application.SiteDataSource#" maxRows=1>
	SELECT *
	FROM Application
	WHERE Ident =  <cf_queryparam value="#Variables.RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

	

	



<cf_head>
	<cf_title>Application - Edit Record</cf_title>
	
</cf_head>

<cfinclude template="Application_Header.cfm">


<cfif IsDefined('Updated')>
	<hr>
	The record has been updated.
	<hr>
</cfif>

<cfif IsDefined('Deleted')>
	<hr>
	The record has been deleted.
	<hr>
</cfif>
<cfoutput query="GetRecord">
<FORM action="Application_Edit.cfm" method="post">
<INPUT type="hidden" name="Application_Edit" value="1">

	<CF_INPUT type="hidden" name="RecordID" value="#Variables.RecordID#">



<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<TR>
		<TD valign="top" align="right" class="label"> <b>appName</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="appName" value="#appName#" size="45" maxLength="25">
		
	
	</TD>
	</TR>
	
	<TR>
		<TD valign="top" align="right" class="label"> <b>Application Full Name</b> </TD>
    <TD>
		<CF_INPUT type="text" name="ApplicationFullName" value="#ApplicationFullName#" size="45" maxLength="70">
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Login Page</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="LoginPage" value="#LoginPage#" size="45" maxLength="50">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Login Message Phrase Text ID</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="LoginMessagePhraseTextID" value="#LoginMessagePhraseTextID#" size="45" maxLength="30">
	</TD>
	</TR>
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Processid <a href="../EditProcessActions.cfm?frm_processID=#ProcessID#">(click to edit)</a></b> </TD>
    <TD>
		<CF_INPUT type="text" name="processid" value="#processid#" size="45" maxLength="4">
	</TD>
	<INPUT type="hidden" name="processid_integer">
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Start Step id</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="startstepid" value="#startstepid#" size="45" maxLength="4">
		
	
	</TD>
	<INPUT type="hidden" name="startstepid_integer">
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Purpose</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="Purpose" value="#Purpose#" size="45" maxLength="100">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Default Language ID</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="DefaultLanguageID" value="#DefaultLanguageID#" size="45" maxLength="4">
		
	
	</TD>
	<INPUT type="hidden" name="DefaultLanguageID_integer">
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Default Country ID</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="DefaultCountryID" value="#DefaultCountryID#" size="45" maxLength="4">
		
	
	</TD>
	<INPUT type="hidden" name="DefaultCountryID_integer">
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Path</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="path" value="#path#" size="45" maxLength="100">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Allow New Locations to be added</b> </TD>
    <TD>
	
		<CF_INPUT type="radio" name="allowNewLocs" value="1" checked="#iif( allowNewLocs is 1,true,false)#"> Yes
		<CF_INPUT type="radio" name="allowNewLocs" value="0" checked="#iif( allowNewLocs is 0,true,false)#"> No
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Allow New People to be added</b> </TD>
    <TD>
	
		<CF_INPUT type="radio" name="allowNewPerson" value="1" checked="#iif( allowNewPerson is 1,true,false)#"> Yes
		<CF_INPUT type="radio" name="allowNewPerson" value="0" checked="#iif( allowNewPerson is 0,true,false)#"> No
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>People Maint Screen</b> </TD>
    <TD>
	
		<CF_INPUT type="radio" name="PeopleMaintScrn" value="1" checked="#iif( PeopleMaintScrn is 1,true,false)#"> Yes
		<CF_INPUT type="radio" name="PeopleMaintScrn" value="0" checked="#iif( PeopleMaintScrn is 0,true,false)#"> No
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Exit page</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="exitpage" value="#exitpage#" size="45" maxLength="50">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Parameters</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="Parameters" value="#Parameters#" size="45" maxLength="255">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>User Cookie</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="userCookie" value="#userCookie#" size="45" maxLength="20">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Live</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="live" value="#live#" size="45" maxLength="1">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>frmNextPage</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="frmNextPage" value="#frmNextPage#" size="45" maxLength="100">
		
	
	</TD>
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Login Required</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="LoginRequired" value="#LoginRequired#" size="45" maxLength="4">
		
	
	</TD>
	<INPUT type="hidden" name="LoginRequired_integer">
	</TR>
	


	
	
	<TR>
	<TD valign="top" align="right" class="label"> <b>Start Page</b> </TD>
    <TD>
	
		<CF_INPUT type="text" name="StartPage" value="#StartPage#" size="45" maxLength="300">
		
	
	</TD>
	</TR>
	

	
</TABLE>
	

<INPUT type="submit" name="btnEdit_OK" value="Update Record">


</FORM>
</CFOUTPUT>
<cfif NOT IsDefined('Deleted')>

<p>
<cfoutput>
<script language="JavaScript">
<!--
function confirmDelete() {
		return confirm("Delete Record?")

}
//-->
</SCRIPT>
<form action="Application_Edit.cfm" method="post" onsubmit="return confirmDelete()">
<INPUT type="hidden" name="Application_Delete" value="1">
<CF_INPUT type="hidden" name="RecordID" value="#Variables.RecordID#">
<input type="submit" name="DeleteButton" value="Delete Record">
</form>
</cfoutput>
</p>
</cfif>



