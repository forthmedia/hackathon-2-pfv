<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		UserGroupManagement.cfm
Author:			Alex
Date created:	2000

	Objective - This file sets up some variables and then calls other files which contain
				the full HTM tags (head, body etc.)


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2002-01-14			SWJ			I removed the HTML tags from this as they were
								duplicated within taskManagement.cfm and userManagement.cfm
2009/04/20			NJH			Bug Fix All Sites Issue 2066 - couldn't remove flag from usergroup if it was last flag in the list.
2015-12-01  		WAB 		PROD2015-26 Visibility.   Remove code for processing  FlagManagement.cfm - now done within template
2106-01-29   		WAB   		Remove twoSelects Combo from UserManagement page, form field changes from previousUsers to Users_orig

Enhancement still to do:



--->

<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>

<!---signal that session security structure needs to be rebuilt--->
<cfset application.cachedSecurityVersionNumber = application.cachedSecurityVersionNumber+1>

<cfif IsDefined("UserManagement") OR IsDefined("EditUsers")>
	<!---Form submitted--->
	<cfif IsDefined("EditUsers")>
		<cftransaction>
			<!---Check if some users need to be deleted--->
			<cfparam name="users" default="">
			<cfif structkeyexists(form,'Users_orig')>
				<cfloop index="x" from="1" to="#ListLen(Users_orig)#">
					<cfif ListFind(Users, ListGetAt(Users_orig, x)) EQ 0>
						<!---If yes, delete them--->
						<cfquery name="DeleteUser" datasource="#application.siteDataSource#">
							DELETE from RightsGroup WHERE UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >  AND PersonID =  <cf_queryparam value="#ListGetAt(Users_orig, x)#" cfsqltype="CF_SQL_INTEGER" >
						</cfquery>
					</cfif>
				</cfloop>
			</cfif>
			<cfif structkeyexists(form,'Users')>
				<cfloop index="x" from="1" to="#ListLen(Users)#">
					<cfif ListFind(users_orig, ListGetAt(Users, x)) EQ 0>
						<!---Check that the user is not already a member of the Group (reload)--->
						<cfquery name="CheckUser" datasource="#application.siteDataSource#">
								SELECT 1 from RightsGroup
								WHERE UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >
								AND PersonID =  <cf_queryparam value="#ListGetAt(Users, x)#" cfsqltype="CF_SQL_INTEGER" >
						</cfquery>

						<!---If not add him--->
						<cfif CheckUser.RecordCount EQ 0>
							<cfquery name="AddUser" datasource="#application.siteDataSource#">
									INSERT into RightsGroup(PersonID,UserGroupID,CreatedBy)
									VALUES(<cf_queryparam value="#ListGetAt(Users, x)#" cfsqltype="CF_SQL_INTEGER" >,<cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
							</cfquery>
						</cfif>
					</cfif>
				</cfloop>
			</cfif>
		</cftransaction>
	</cfif>
	<cfinclude template="UserManagement.cfm">
<cfelseif Isdefined("TaskManagement")>
	<cfif IsDefined("SecurityTypeID")>
		<cfloop index="x" from="1" to="#ListLen(SecurityTypeID)#">
			<cfset SecurityType = Evaluate(#ListGetAt(SecurityTypeID, x)#)>
			<cfset VarPermission = ArraySum(ListtoArray(Evaluate("frmPermission_#SecurityType#")))>
			<cfquery name="UpdateRights" datasource="#application.siteDataSource#">
				UPDATE Rights
				SET Permission =  <cf_queryparam value="#VarPermission#" cfsqltype="CF_SQL_Integer" >
				WHERE UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >
				AND SecurityTypeID =  <cf_queryparam value="#SecurityType#" cfsqltype="CF_SQL_INTEGER" >
			</cfquery>
		</cfloop>
	<cfelseif IsDefined("AddTasks")>
		<cfloop index="x" from="1" to="#ListLen(AddTasks)#">
			<!--- <cfquery name="GetMenuID" datasource="#application.siteDataSource#">
				SELECT MenuID from SecurityType WHERE SecurityTypeID =  <cf_queryparam value="#ListGetAt(AddTasks, x)#" cfsqltype="CF_SQL_INTEGER" >
			</cfquery>
			<cfif GetMenuID.MenuID IS "">
				<cfquery name="AddTasks" datasource="#application.siteDataSource#">
					INSERT into Rights(UserGroupID,SecurityTypeID,CountryID,MenuID,Permission)
					select <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >,<cf_queryparam value="#ListGetAt(AddTasks, x)#" cfsqltype="CF_SQL_INTEGER" >,0,0,1
					from rights
					where not exists (select 1 from rights where UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >  and SecurityTypeID =  <cf_queryparam value="#ListGetAt(AddTasks, x)#" cfsqltype="CF_SQL_INTEGER" > )
				</cfquery>
			<cfelse>--->
				<cfquery name="AddTasks" datasource="#application.siteDataSource#">
					if not exists (select 1 from rights where UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >  and SecurityTypeID =  <cf_queryparam value="#ListGetAt(AddTasks, x)#" cfsqltype="CF_SQL_INTEGER" > )
					INSERT into Rights(UserGroupID,SecurityTypeID,CountryID,MenuID,Permission)
					VALUES(<cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >,<cf_queryparam value="#ListGetAt(AddTasks, x)#" cfsqltype="CF_SQL_INTEGER" >,0,0,1)
				</cfquery>
			<!---</cfif>			 --->
		</cfloop>
	<cfelseif IsDefined("RemoveTask")>
		<cfquery name="AddTasks" datasource="#application.siteDataSource#">
			DELETE from Rights
			WHERE SecurityTypeID =  <cf_queryparam value="#RemoveTask#" cfsqltype="CF_SQL_INTEGER" >
			AND UserGroupID =  <cf_queryparam value="#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >
		</cfquery>
	<cfelseif IsDefined("NewSecurityTask")>
		<!---Check for duplicate names--->
		<cfquery name="CheckSecurityType" datasource="#application.siteDataSource#">
			SELECT 1
			from SecurityType
			WHERE Shortname =  <cf_queryparam value="#Trim(name)#" cfsqltype="CF_SQL_VARCHAR" >
		</cfquery>
		<cfif CheckSecurityType.RecordCount EQ 0>
			<cftransaction>
				<cfquery name="AddSecurityType" datasource="#application.siteDataSource#">
					INSERT into SecurityType(Shortname,Description,CountrySpecific)
					VALUES(<cf_queryparam value="#name#" cfsqltype="CF_SQL_VARCHAR" >,<cf_queryparam value="#Description#" cfsqltype="CF_SQL_VARCHAR" >,0)
				</cfquery>

				<cfquery name="GetID" datasource="#application.siteDataSource#">
					Select max(SecurityTypeID) as maxID from SecurityType
				</cfquery>
				<cfset Def = "View,Edit,Add">
				<cfloop index="x" from="1" to="3">
					<cfquery name="AddPermissionDef" datasource="#application.siteDataSource#">
						INSERT into PermissionDefinition(SecurityTypeID, PermissionLevel,Permissionname)
						VALUES(<cf_queryparam value="#GetID.MaxID#" cfsqltype="CF_SQL_INTEGER" >, <cf_queryparam value="#x#" cfsqltype="CF_SQL_Integer" >, <cf_queryparam value="#ListGetAt(Def, x)#" cfsqltype="CF_SQL_VARCHAR" >)
					</cfquery>
				</cfloop>
			</cftransaction>
		<cfelse>
			<cfoutput>
			<script>
				alert("#jsStringFormat(name)# Task has already been registered")
			</script>
			</cfoutput>
		</cfif>
	</cfif>
	<cfinclude template="TaskManagement.cfm">
</cfif>