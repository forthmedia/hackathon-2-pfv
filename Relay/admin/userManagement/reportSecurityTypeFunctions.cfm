<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportSecurityTypeFunctions.cfm	
Author:				NJH
Date started:		2008-06-12
	
Description:		Reports on the available functions for a particular security task and permission level

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 
<!--- <cfparam name="securityTypeID" type="numeric"> --->
<cfparam name="securityType" type="string">
<cfparam name="permissionLevel" type="numeric" default=1>

<cfquery name="getSecurityTypeFunctions" datasource="#application.siteDataSource#">
	select distinct *,
		case 
			when type = 'Report' then 'access to the following reports:'
			when type = 'Function' then 'access to the following functions:'
			when type = 'PrimaryNav' then 'see the following primary navigation:'
			when type = 'SecondaryNav' then 'see the following secondary navigation:'
			else ''
		end as displayTextForType
	from SecurityTypeFunction with (noLock) 
	where securityType =  <cf_queryparam value="#securityType#" CFSQLTYPE="CF_SQL_VARCHAR" >  and permission =  <cf_queryparam value="#permissionLevel#" CFSQLTYPE="CF_SQL_Integer" > 
	order by displayTextForType,description
</cfquery>

<!--- <cfquery name="getSecurityTypeFunctions" datasource="#application.siteDataSource#">
	select distinct stf.*,
		case 
			when type = 'Report' then 'Access to the following reports'
			when type = 'Function' then 'Access to the following functions'
			when type = 'PrimaryNav' then 'See the following primary navigation'
			when type = 'SecondaryNav' then 'See the following secondary navigation'
			else ''
		end as displayTextForType
	from [SecurityTypeFunction] stf inner join securityType st
	on stf.securityType = st.shortName
	where st.securityTypeID=#securityTypeID# and stf.permission = #permissionLevel#
	order by displayTextForType,stf.description
</cfquery> --->





<cf_head>
	<cf_title>Security Type Functions</cf_title>
</cf_head>



<cfif getSecurityTypeFunctions.recordCount gt 0>
	<b><cfoutput>#htmleditformat(securityType)# Level #htmleditformat(permissionLevel)# gives a user rights to the following:</cfoutput></b><br>
<br>
<table width="100%">
	<cfoutput query="getSecurityTypeFunctions" group="type">
	<tr><th>#htmleditformat(displayTextForType)#</th></tr>
	<cfoutput><tr><td>#htmleditformat(Description)#</td></tr></cfoutput>
	<tr><td></td></tr>
	</cfoutput>
</table>
<cfelse>
	<cfoutput>
	There are no security functions currently controlled by #htmleditformat(securityType)# Level #htmleditformat(permissionLevel)#
	</cfoutput>
</cfif>



