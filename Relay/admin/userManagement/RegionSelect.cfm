<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

File:			RegionSelect.cfm
Author:			DJH
Description:	
Displays a select box containing the available country regions
when a user clicks on a region, all the countries in that region will
be selected/deselected as appropriate.

Version history:

1

2008/06/02  WAB reordered list of regions, needed a small mod on the JS at same time

--->

<cfparam name="RegionSelectHeight" type="string" default="20">

<!--- <cfif not isdefined("getcountries.recordcount")>
	<cfinclude template="/templates/qrygetcountries.cfm">
</cfif> --->


<!--- define a query listing all the countries in their respective groups --->
<cfquery name="CountryGroups" datasource="#application.siteDataSource#">
select 
	isnull(cg.CountryGroupId,0) as RegionID,
	isnull(cgn.CountryDescription,'Not in any Region') as Region,
	c.CountryId,
	c.CountryDescription as Country
from 
	Country as c left join 
	(CountryGroup as cg inner join country cgn on cg.CountryGroupId = cgn.CountryId) on c.CountryId = cg.CountryMemberId
	
	<!--- 
	WAB 2008/06/02 
	Modified because got lots of 'Not in any Region' Items caused by orphaned records in the CountryGroup table
	(Country as c left join CountryGroup as cg on c.CountryId = cg.CountryMemberId)
	left join Country as cgn on cg.CountryGroupId = cgn.CountryId
	--->
where
	cgn.IsoCode is null
	and c.IsoCode is not null
	and c.CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
order by 
  	cgn.CountryDescription,  
	cg.CountryGroupId,
	c.CountryId
</cfquery>

<!--- now create a javascript array based on the countrygroupid and the countryid
actually, it will be a cf variable which will get included in the cfhtmlhead below --->

<cfset ArrayCreator="">
<cfoutput query="CountryGroups">
	<cfset ArrayCreator=ArrayCreator & "Region[#int(currentrow-1)#] = new CountryRegion(#RegionId#,#CountryId#)">
	<cfset ArrayCreator=ArrayCreator & "
">	
</cfoutput>

<!--- using cfhtmlhead, place the javascript code in the head --->
<cfhtmlhead text="
<script language=""JavaScript"">
// array to hold a list of regions and countries
Region = new Array()

// constructor for each array element
function CountryRegion(RegionId, CountryId)
{
	this.RegionId = RegionId
	this.CountryId = CountryId
	this.checked = false
}

// cold fusion actually creates the array from the query countrygroups
#ArrayCreator#

// function to select/de-select the various countries

function selector(aselect)
{	
	var selectedValue = aselect.options[aselect.selectedIndex].value
	// as long as they have chosen a valid one.
	if(aselect.selectedIndex > 2)
	{		
		// loop through the array
		for (var ctr=1; ctr < Region.length; ctr++)
		{
			// check to see whether this country falls into the selected region
			if(Region[ctr].RegionId == selectedValue)
			{
				// it does - loop through each checkbox till the right countryid is found and
				// check it if its not checked (based on the checked property in my Region array)
				for (var ctr2 = 0; ctr2 < document.mainForm.insCountryID.length; ctr2++)
				{				
					if (document.mainForm.insCountryID[ctr2].value == Region[ctr].CountryId)
					{						
						//It is a country in the region - check/uncheck it
						document.mainForm.insCountryID[ctr2].checked = !Region[ctr].checked
						break
					}
				}
				// now mark the checked property as its inverse
				Region[ctr].checked = !Region[ctr].checked
			}
//			WAB 2008/06/02 removed when ordering of regions changed
//			if(Region[ctr].RegionId > selectedValue)
//				break
		}
	}
	else
	{
		switch (aselect.selectedIndex)
		{
			case 0 :
			// select all of them
				// first update all the checkboxes
				for (var ctr3 = 0; ctr3 < document.mainForm.insCountryID.length; ctr3++)
				{
					document.mainForm.insCountryID[ctr3].checked = true
				}
				// now update the array
				for (ctr3 = 0; ctr3 < Region.length; ctr3++)
				{
					Region[ctr3].checked = true
				}
			
			break
			
			case 1 :
				// select none of them
				// first update all the checkboxes
				for (var ctr3 = 0; ctr3 < document.mainForm.insCountryID.length; ctr3++)
				{
					document.mainForm.insCountryID[ctr3].checked = false
				}
				// now update the array
				for (ctr3 = 0; ctr3 < Region.length; ctr3++)
				{
					Region[ctr3].checked = false
				}
		
			break		
		}		
	}
	aselect.selectedIndex=-1
}
</script>
">

<select name="RegionSelect" onchange="selector(this)" size="<cfoutput>#htmleditformat(RegionSelectHeight)#</cfoutput>" class="form-control">
	<option value="-77">Select All
	<option value="-88">Select None
	<option value="-99">Select by Region:
	<cfoutput query="CountryGroups" group="RegionId">
		<option value="#RegionID#">&nbsp;&nbsp;&nbsp;&nbsp;#htmleditformat(Region)#
	</cfoutput>
</select>

