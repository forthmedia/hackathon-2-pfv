<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
2008-06-23	NYF	Added UserGroupType - defaults to Other
--->

<cfparam name="defaultUGType" default="Other">

<CFQUERY NAME="getUserGroupTypes" datasource="#application.siteDataSource#">
	SELECT 1 as sortorder, 
		userGroupType 
	FROM UserGroup
	WHERE personid is NULL and userGroupType is not NULL
	<cfif defaultUGType eq "Other">
		and userGroupType ! =  <cf_queryparam value="#defaultUGType#" CFSQLTYPE="CF_SQL_VARCHAR" >
		union
		select 2 as sortorder,'#defaultUGType#'
	</cfif>
	order by sortorder, userGroupType
</CFQUERY>


<cfset application.com.request.setDocumentH1(show=false)>

<cf_title>Phr_UserGroups_SecurityTypeName</cf_title>
<cf_head>
	<script type="text/javascript">
		function goahead(){

			form = window.document.form11
			namebox = form.NewGroupName.value
			uGType = form.frmUserGroupType.value  //NYF 2008-06-23 T10

			var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_";
			var abort = 0;

			for (var i = 0; i < namebox.length; i++) {
			  	if (iChars.indexOf(namebox.charAt(i)) != -1 && abort == 0) {
			  	  alert ("phr_userGroups_SpecialCharacters");
			  	  abort = 1;
			  	}
		  	}

			if(abort == 0){
				if(namebox == ""){
					alert("phr_userGroups_provide_name")
				}
				else{

					var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnFormat=plain&_cf_nodebug=false'
					var page = page + '&webservicename=relayUserGroupWS&methodname=NewUserGroupName&NewGroupName='+ namebox + '&frmUserGroupType='  +uGType
					var parameters = {}

					jQuery.ajax({
						type: "POST",
						url: page,
						data: parameters,
						cache: false,
						success: function (response){
							opener.location.href="UserManagement.cfm?frmUserGroupID="+response;
							window.close();
						}
					});
					return false;
				}
			}
		}
	</script>
</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<b>phr_userGroups_provide_name</b>
		</TD>
	</TR>
</table>
<FORM ACTION="" METHOD="POST" NAME="form11" target="_parent" onSubmit="return goahead();">
<table width="300" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<tr>
	<td>
	phr_userGroups_new_userGroup_name:
	</td>
	<td>
	<INPUT TYPE="Text" NAME="NewGroupName">
	</td>
</tr>

<tr>
	<td>
	User Group Type:
	</td>
	<td>
	<cf_displayValidValues
		formFieldName = "frmUserGroupType"
		showNull="false"
		validValues="#getUserGroupTypes#"
		dataValueColumn = "userGroupType"
		displayValueColumn = "userGroupType"
		currentValue="#defaultUGType#">
	</td>
</tr>

<tr>
	<td align='center' colspan='2'>
		<input type="submit" value="phr_submit">&nbsp;&nbsp;&nbsp;<input type="button" value="phr_close" onClick="javascript:window.close();">
	</td>
</tr>
</table>
</FORM>