<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<CFPARAM NAME="frmNext" DEFAULT="userlist">
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">

<!--- 2012-07-25 PPB P-SMA001 commented out
<cfinclude template="/templates/qrygetcountries.cfm">
--->

	<!--- check that person is Valid and user has rights for
			this person --->
			
		<CFQUERY NAME="checkPersonValid" datasource="#application.siteDataSource#">
			SELECT p.PersonID, p.FirstName, p.LastName
			  FROM Person AS p, Location AS l
			WHERE p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  AND p.Active <> 0 <!---  true --->
			  AND p.LocationID = l.LocationID
			  <!--- 2012-07-25 PPB P-SMA001 commented out
			  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			   --->
			   #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
		</CFQUERY>
		
		<CFIF #checkPersonValid.RecordCount# IS 0>
			<!--- send them all the way to the menu since
					they should never have reached this template
					for this person ID --->
			<CFSET #message# = "The person you selected to update is not currently active or you do not have rights to the record.">
			<CFINCLUDE TEMPLATE="adminmenu.cfm">
			<CF_ABORT>
		</CFIF>

	<!--- check last updated date/time --->
		<CFQUERY NAME="checkPerson" datasource="#application.siteDataSource#">
			SELECT FirstName, LastName, LastUpdated
			FROM Person
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND LastUpdated =  <cf_queryparam value="#insLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFQUERY>
		
		<CFIF #checkPerson.RecordCount# IS 0>
			<CFSET #message# = "The person's record has been changed by another user since you last viewed it.  ">
			<CFINCLUDE TEMPLATE="#frmNext#.cfm">
			<CF_ABORT>
		</CFIF>

	<!--- okay, now remove the person as a user --->
	<CFTRANSACTION>
		<!--- update the person record --->
		<CFQUERY NAME="updPerson" datasource="#application.siteDataSource#">
			UPDATE Person
			   SET Username = '',
			   		Password = '',
					LoginExpires =  <cf_queryparam value="#CreateODBCDate("1-Jan-1992")#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  AND LastUpdated =  <cf_queryparam value="#insLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 			   
		</CFQUERY>


		<!--- remove the association between the
				Person and UserGroups --->
		<CFQUERY NAME="delRightsGroups" datasource="#application.siteDataSource#">
			DELETE FROM RightsGroup
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY>

		<!--- remove the association between the Person's 
				personal user group and countries --->
				
		<CFQUERY NAME="delUGCountry" datasource="#application.siteDataSource#">
			DELETE FROM UserGroupCountry
			WHERE UserGroupID = 
				(SELECT UserGroupID
				   FROM UserGroup
				   WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY>

		
		<!--- delete the Person's specific rights
				(those associated to the person's personal user group)		--->
		<CFQUERY NAME="delRights" datasource="#application.siteDataSource#">
			DELETE FROM Rights
			WHERE UserGroupID = 
				(SELECT UserGroupID
				   FROM UserGroup
				   WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY>

		<!--- delete the Person's selections
				(those with a createdBy of the person's personal user group)		--->
		<CFQUERY NAME="delSelectionsDepend" datasource="#application.siteDataSource#">
			DELETE sg.*, st.*, sc.*
			FROM Selection AS s,
				 SelectionTag AS st,
				 SelectionCriteria AS sc,
				 SelectionGroup AS sg
			WHERE s.SelectionID = st.SelectionID
			  AND s.SelectionID = sc.SelectionID
			  AND s.SelectionID = sg.SelectionID
			  AND s.CreatedBy = 
				(SELECT UserGroupID
				   FROM UserGroup
				   WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY>
		
		<!--- do these is separate queries due to
				referential integrity / relationships --->
<!---	DON't delete selection header since it may relate to a sent
		communications --->
		
		<CFQUERY NAME="delSelections" datasource="#application.siteDataSource#">
			DELETE s.* FROM Selection AS s
			WHERE s.CreatedBy = 
				(SELECT UserGroupID
				   FROM UserGroup
				   WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY> --->
			
		<CFQUERY NAME="updComms" datasource="#application.siteDataSource#">
			UPDATE Communications AS c
			SET c.SelectionID = null
			WHERE EXISTS (SELECT s.SelectionID
							FROM Selections AS s, UserGroup AS ug
						  WHERE s.CreatedBy = ug.UserGroupID
						    AND s.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
							AND s.SelectionID = c.SelectionID)
			AND c.Sent = 0 <!--- false --->
			AND c.SendDate IS null
		</CFQUERY>
			
			
	</CFTRANSACTION>

	<CFSET message = "The user has been removed.">
	<CFINCLUDE TEMPLATE="#frmNext#.cfm">
	<CF_ABORT>
<!--- </CFIF> --->


