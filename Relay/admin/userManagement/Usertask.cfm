<!--- �Relayware. All Rights Reserved 2014 --->
<!--- If person is removed from UserGroup table data/persondedupe.cfm
		will need modifications --->

<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->

<!--- Amendment History

WAB 140100  altered query for checking that record not already updated 
2007-10-08  WAB  make sure that lastupdatedby is set when password is updated
2007-10-29   WAB changes to country rights update query 
2008-06-20	NYF	permission level drop-down was removed from userEdit.cfm, 
			therefore this file was changed to now default permission level to All
2009-12-17  NAS LID 2886 - add userGroupType to be 'Personal'
2010/03/19	NAS LID 3109 - Fix the Username update to enforce Unicode
2010/09/20	NJH	8.3 Removed setting of password here in user management screen as passwords should be set by user themselves.
--->


<CFPARAM NAME="frmNextPage" DEFAULT="useredit.cfm">
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">

<CFSET updatetime = CreateODBCDateTime(Now())>

<!---signal that session security structure needs to be rebuilt--->
<cfset application.cachedSecurityVersionNumber = application.cachedSecurityVersionNumber+1>

<!--- lists all Country ID's to which User has rights --->
<cfinclude template="/templates/qrygetcountries.cfm">


<!--- <CFIF FORM.frmtask IS "update"> --->

	<!--- check that person is Valid and user has rights for
			this person --->
			
		<CFQUERY NAME="checkPersonValid" datasource="#application.siteDataSource#">
			SELECT p.PersonID, p.FirstName, p.LastName
			  FROM Person AS p, Location AS l
			WHERE p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  AND p.Active <> 0 <!--- true --->
			  AND p.LocationID = l.LocationID
			  <!--- 2012-07-25 PPB P-SMA001 commented out
			  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			   --->
			#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
		</CFQUERY>
		
		<CFIF #checkPersonValid.RecordCount# IS 0>
			<!--- send them all the way to the menu since
					they should never have reached this template
					for this person ID --->
			<CFSET #message# = "The person you selected to update is not currently active or you do not have rights to the record.">
			<CFINCLUDE TEMPLATE="adminmenu.cfm">
			<CF_ABORT>
		</CFIF>

	<!--- check that at least one UserGroup has been selected --->
		<CFIF NOT IsDefined("insUserGroupID")>
			<CFSET insUserGroupID = 0>
		</CFIF>
		
		<CFQUERY NAME="checkUserGroups" datasource="#application.siteDataSource#">
			SELECT UserGroupID
			FROM UserGroup
			WHERE UserGroupID  IN ( <cf_queryparam value="#insUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			  AND UserGroupID <> 0 <!--- initial data load group-- no one belongs to it --->
			  AND PersonID IS Null <!--- we only allow non-personal groups here --->
		</CFQUERY>
		
		<CFIF #checkUserGroups.RecordCount# IS 0>
			<CFSET #message# = "You must associate valid user groups to the person.">
			<CFINCLUDE TEMPLATE="useredit.cfm">
			<CF_ABORT>
		</CFIF>


	<!--- check that valid countries exist --->
		<CFIF NOT IsDefined("insCountryID")>
			<CFSET insCountryID = 0>
		</CFIF>

		<CFQUERY NAME="checkCountries" datasource="#application.siteDataSource#">
			SELECT CountryID
			FROM Country
			WHERE CountryID  IN ( <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			  AND ISOCode IS NOT Null  <!--- if ISO then country is not a group --->
		</CFQUERY>
		
		<CFIF #checkCountries.RecordCount# IS 0>
			<CFSET #message# = "You must allow the user access to at least one country.">
			<CFINCLUDE TEMPLATE="useredit.cfm">
			<CF_ABORT>
		</CFIF>
	<!--- check that both passwords are the same and they exist --->
	
		<!--- 
		NJH 2010/09/20 - removed the password setting in this form.
		<CFIF #Compare(frmNewPassword1,frmNewPassword2)# IS NOT 0 OR #Trim(frmNewPassword1)# IS "">
			<CFSET #message# = "The the same password was not entered in twice.">
			<CFINCLUDE TEMPLATE="#frmNextPage#">
			<CF_ABORT>
		</CFIF>
		
		<!--- 2010/08/20 - validate that the new password meets the requirements --->
		<cfset checkLogin = application.com.login.getWhyLoginFailed(username=insUsername,password=frmNewPassword1)>
		<cfif not checkLogin.isOK>
			<cfset message = checkLogin.message>
			<CFINCLUDE TEMPLATE="#frmNextPage#">
			<CF_ABORT>
		</cfif> --->

	<!--- check last updated date/time --->
		<CFQUERY NAME="checkPerson" datasource="#application.siteDataSource#">
			SELECT FirstName, LastName, Password, LastUpdated
			FROM Person
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND DATEDIFF (ss,LastUpdated, #insLastUpdated#) = 0	
<!--- 			AND LastUpdated = #insLastUpdated#  removed this syntax - problem with milliseconds--->
		</CFQUERY>
		
		<CFIF #checkPerson.RecordCount# IS 0>
			<CFSET #message# = "The person's record has been changed by another user since you last viewed it.  ">
			<CFINCLUDE TEMPLATE="#frmNextPage#">
			<CF_ABORT>
		</CFIF>

	
	<!--- ===================================
			all criteria exists and is valid
			so add the record
		  =================================== ---->	
<!--- PKP: 2008-03-31 : add screen procesing file for screen added on useredit.cfm for NCH-FNL008 USER MANAGEMENT ENHANCEMENTS--->
<cf_aScreenUpdate>

	<CFTRANSACTION>
		
		<!--- GCC 2010/09/01 added this to correctly hash users passwords when changed on the internal user management screen
				WAB 2009/07/14 changes to support application.encryptionMethod["relayPassword"] type of encryption
				for backwards compatibilty need to handle all the previous  encryption methods, but ideally replace with just the relayPassword one 
				work out what the encrypted password should be
			--->	
		<!--- 
		<cfif structKeyExists(application,"encryptionMethod") and structKeyExists(application.encryptionMethod,"relayPassword")>
			<cfset insPassword1 = application.com.encryption.encryptString(encryptType="relaypassword",partnerUsername=insUsername,partnerPassword=insPassword1)>
		<cfelseif isdefined('application.passwordEncryptionMethod') and application.passwordEncryptionMethod is not "">
			<cfset insPassword1 = hash(Trim(insPassword1),"#application.passwordEncryptionMethod#")>
		<cfelse>
		<cfparam name="request.hashAlgorithm" default="MD5">
			<cfset insPassword1 = hash(Trim(insPassword1),request.hashAlgorithm)>
		</cfif>
		--->
		
		<CFQUERY NAME="updPerson" datasource="#application.siteDataSource#">
		Update Person
		<!--- 2010/03/19	NAS LID 3109 - Fix the Username update to enforce Unicode --->
		SET Username =  <cf_queryparam value="#insUsername#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<!--- 
			NJH 2010/09/20 - removed setting of password here as part of 8.3. Passwords should be set by partner themselves.
			<CFIF #Compare(frmNewPassword1,checkPerson.Password)# IS NOT 0>
				Password = '#frmNewPassword1#',
				PasswordDate = #FreezeDate#,
			</CFIF> --->
			LoginExpires =  <cf_queryparam value="#insLoginExpires#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			LastUpdated =  <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			lastupdatedby = #request.relayCurrentUser.usergroupID#  <!--- WAB 2007-10-08 , so that appears correct on mods report added lastupdatedby --->
		WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND DATEDIFF (ss,LastUpdated, #insLastUpdated#) = 0	
		</CFQUERY>
		
		<!---2010/12/14 GCC Reinstanted so that users with no password get one otherwise there is no way to send the password out via user management --->
		<cf_createUserNameAndPassword personid = #frmPersonID# resolveDuplicates = true 
				OverWritePassword = false OverWriteUserName = false>
		
		<!--- <cfset changePassResult = application.com.login.changeUserPassword(username=insUsername,currentPassword=checkPerson.Password,newPassword1=form.frmNewPassword1,newPassword2=form.frmNewPassword2)>
 --->
		<!--- get Personal UserGroupID, adding a new UserGroup
				if necessary --->
				
		<CFQUERY NAME="checkUserGroup" datasource="#application.siteDataSource#">
			SELECT * from UserGroup
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		
		
		<CFIF #checkUserGroup.RecordCount# IS NOT 0>
			<CFSET UserGroup = checkUserGroup.UserGroupID>
		<CFELSE>
			<CFQUERY NAME="insUserGroup" datasource="#application.siteDataSource#">
				<!--- 2009-12-17  NAS LID 2886 - add userGroupType to be 'Personal' --->
				INSERT INTO UserGroup (UserGroupID, Name, PersonID, CreatedBy, Created, LastUpdatedBy, LastUpdated, userGroupType)
					SELECT Max(ug.UserGroupID)+1, p.FirstName + ' ' + p.LastName, p.PersonID, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 'Personal'
					from UserGroup AS ug, Person AS p
					WHERE p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					GROUP BY p.FirstName + ' ' + p.LastName, p.PersonID
			</CFQUERY>
			
			<CFQUERY NAME="checkUserGroupAgain" datasource="#application.siteDataSource#">
				SELECT * from UserGroup
				WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
			
			<!--- assume that if insUserGroup didn't rollover, the record was added --->
			
			<CFSET UserGroup = checkUserGroupAgain.UserGroupID>
			
		</CFIF>

				

		<!--- delete from UserGroupCountry all values not tagged
				if new user then this query will do nothing since no
				records in UserGroupCountry --->
		
		<CFQUERY NAME="delUGCountry" datasource="#application.siteDataSource#">
			DELETE FROM UserGroupCountry
			WHERE UserGroupID =  <cf_queryparam value="#Variables.UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
		       AND CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFIF #IsDefined("insCountryID")#>
				AND CountryID  NOT IN ( <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFELSE>
				AND CountryID <> 0
			</CFIF>

		</CFQUERY>

		<CFIF IsDefined("insCountryID")>
			<CFQUERY NAME="insUGCountry" datasource="#application.siteDataSource#">
				INSERT INTO UserGroupCountry (UserGroupID, CountryID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				SELECT <cf_queryparam value="#Variables.UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" >, c.CountryID, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  FROM Country AS c
	 			 WHERE c.CountryID  IN ( <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				   AND c.ISOCode IS NOT Null
				   AND NOT EXISTS (SELECT ugc.UserGroupID
									 FROM UserGroupCountry AS ugc
									WHERE ugc.CountryID  IN ( <cf_queryparam value="#InsCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
									  AND ugc.UserGroupID =  <cf_queryparam value="#Variables.UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
									  AND c.CountryID = ugc.CountryID)

			</CFQUERY>
		</CFIF>

		<!--- maintain RightsGroup --->
		<!--- delete from RightsGroup all values not tagged
				if new user then this query will do nothing since no
				records in UserGroupCountry --->
		
		<CFQUERY NAME="delRightsGroup" datasource="#application.siteDataSource#">
			DELETE FROM RightsGroup
			WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFIF #IsDefined("insUserGroupID")#>
				AND UserGroupID  NOT IN ( <cf_queryparam value="#insUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<CFELSE>
				AND UserGroupID <> 0
			</CFIF>
		</CFQUERY>

		<CFIF IsDefined("insUserGroupID")>
			<CFQUERY NAME="insRightsGroup" datasource="#application.siteDataSource#">
				INSERT INTO RightsGroup (PersonID, UserGroupID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				SELECT DISTINCT <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >, ug.UserGroupID, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  FROM UserGroup AS ug
	 			 WHERE ((ug.UserGroupID  IN ( <cf_queryparam value="#insUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) AND ug.PersonID IS Null) OR ug.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > )
					AND ug.UserGroupID <> 0
				   AND NOT EXISTS (SELECT rg.PersonID
				   			         FROM RightsGroup AS rg
									WHERE rg.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
									  AND rg.UserGroupID = ug.UserGroupID)
			</CFQUERY>
		</CFIF>

		<!--- ====================================
				Maintain Rights
			  ====================================	--->
			  
			<!--- delete all the person's Personal UserGroup records --->
			<!--- 2007-10-29 WAB adjusted so didn't do the record task ones, which are now done  later--->
			<CFQUERY NAME="delRights" datasource="#application.siteDataSource#">
				DELETE FROM Rights
				 WHERE UserGroupID =  <cf_queryparam value="#Variables.UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 AND CountryID <> 0
				AND SecurityTypeID  <> (select SecurityTypeID from  SecurityType where  shortname = 'RecordTask')
			</CFQUERY>
			  
			<!--- now the personal usergroup should be added to the rights
				  table if appropriate (if country specific) --->

<!--- 	WAB 2000-03-01
		Nobbled this query so that does not operate on recordtask
		replaced record task stuff 	with a loop which takes into account the level of rights to each country
		There are no other country specific rights at the moment, but leave query just in case
 --->					   

<CFQUERY NAME="insRights" datasource="#application.siteDataSource#">
				INSERT INTO RIGHTS ( UserGroupID, SecurityTypeID, CountryID, MenuID, Permission )
					SELECT DISTINCT b.UserGroupID, a.SecurityTypeID, d.CountryID, 0, a.Permission
					  FROM  PredefinedUGSecurity AS a,
					  		UserGroupCountry AS d,
							UserGroup AS b,
							RightsGroup AS e,
							SecurityType AS f
					 WHERE e.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					   AND e.UserGroupID = a.UserGroupID
					   AND b.UserGroupID = d.UserGroupID
					   AND a.SecurityTypeID = f.SecurityTypeID
					   AND f.CountrySpecific <> 0 
						AND a.SecurityTypeID  <> (select SecurityTypeID from  SecurityType where  shortname = 'RecordTask')
					   
<!---    					   AND ((e.PersonID = b.PersonID AND a.PersonalGroupOnly <> 0)
							OR (e.UserGroupID = b.UserGroupID AND a.PersonalGroupOnly = 0))
 --->					   

					   AND e.PersonID = b.PersonID
					   AND a.PersonalGroupOnly <> 0
 
 
 <!---	   delete the 2 rows above to update
		the non-personal/non-country specific group rights 
					   AND ((e.PersonID = b.PersonID
								AND a.PersonalGroupOnly = true)
							OR (e.UserGroupID = b.UserGroupID
								AND a.PersonalGroupOnly = false));--->
			</CFQUERY>


			<CFQUERY NAME="delRights" datasource="#application.siteDataSource#">
				DELETE FROM Rights
				 WHERE UserGroupID =  <cf_queryparam value="#Variables.UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
				 AND CountryID <> 0
				AND SecurityTypeID  = (select SecurityTypeID from  SecurityType where  shortname = 'RecordTask')
				 AND CountryID  in ( <cf_queryparam value="#countryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				 AND CountryID  not in ( <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
			  
 <CFLOOP index="thisCountryID" list = "#insCountryID#">
	<!--- 2007-10-29 WAB adjusted this query so that it didn't rely on all the rights being deleted before hand --->
	<!--- NYF 2008-06-20 replaced  #evaluate("insCountryPermission_#thisCountryID#")# with 7 - in all 3 places - defaults permission to All --->
	 <CFQUERY NAME="insRights" datasource="#application.siteDataSource#">
			if exists (select * from rights r inner join usergroup ug on r.usergroupid = ug.usergroupid where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  and countryid =  <cf_queryparam value="#thisCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  and SecurityTypeID = (select SecurityTypeID from  SecurityType where  shortname = 'RecordTask'))
				BEGIN
					update 
						rights set permission = 7
					from	
						rights r 
							inner join 
						usergroup ug on r.usergroupid = ug.usergroupid 
					where 
							personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  
						and countryid =  <cf_queryparam value="#thisCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
						and SecurityTypeID = (select SecurityTypeID from  SecurityType where  shortname = 'RecordTask')
						and permission <> 7
				
				END
			ELSE
				BEGIN
					INSERT INTO RIGHTS ( UserGroupID, SecurityTypeID, CountryID, MenuID, Permission )
					SELECT DISTINCT 
						b.UserGroupID, 
						(select SecurityTypeID from  SecurityType where  shortname = 'RecordTask'), 
						#thisCountryID#, 
						0, 
						7
					from usergroup as b
					where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				
				
				END				

			</CFQUERY>

</cfloop> 


		<!---Maintain Security Tasks and Permissions--->
<!--- wab added isDEfined - Alex please check --->
<CFIF isDefined("SecurityTypeID")	>

		<CFLOOP index="x" FROM="1" TO="#ListLen(SecurityTypeID)#">
		
			
			<CFSET SecurityType = Evaluate(#ListGetAt(SecurityTypeID, x)#)>
			
			<CFIF IsDefined("frmPersonalPermission_#SecurityType#")>
			
				<CFSET VarPermission = ArraySum(ListToArray(Evaluate("frmPersonalPermission_#SecurityType#")))>
				
				<CFQUERY NAME="CheckSecurityType" datasource="#application.siteDataSource#">
					SELECT * from Rights
					WHERE UserGroupID =  <cf_queryparam value="#UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND SecurityTypeID =  <cf_queryparam value="#SecurityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				
				
				<!---If Security Type already present in the table for the given UserGroupID update it--->
				<CFIF CheckSecurityType.RecordCount NEQ 0>
				
					<CFQUERY NAME="UpdatePersonalRights" datasource="#application.siteDataSource#">
						UPDATE Rights
						SET Permission =  <cf_queryparam value="#VarPermission#" CFSQLTYPE="CF_SQL_Integer" > 
						WHERE UserGroupID =  <cf_queryparam value="#UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
						AND SecurityTypeID =  <cf_queryparam value="#SecurityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>	
				
				
				<!---If not insert it--->
				<CFELSE>
				
				
					<CFQUERY NAME="UpdatePersonalRights" datasource="#application.siteDataSource#">
					INSERT INTO RIGHTS(Permission,UserGroupID,SecurityTypeID,CountryID,MenuID)
					VALUES(<cf_queryparam value="#VarPermission#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#UserGroup#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#SecurityType#" CFSQLTYPE="CF_SQL_INTEGER" >,0,0)
					</CFQUERY>	
				
				
				</CFIF>
			
			</CFIF>
			
		</CFLOOP>
	
</CFIF>
	</CFTRANSACTION>

	<CFSET message = "The user profile for #checkPerson.FirstName# #checkPerson.LastName# has been successfully updated.">
	<CFINCLUDE TEMPLATE="#frmNextPage#">
	<CF_ABORT>
<!--- </CFIF> --->






