<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	File name:			ExternalSSO.cfm
	Author:				PKP
	Date started:		2008-02-04
	Purpose:			External logon form
	Usage:
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	11-Nov-2008          SSS        Changed the query for the name dropdown so that it will display the username if is the same as firstname and last name otherwise
	it will display first name and lastname and the username in brackets. Also tried to fix the layout to make it look better when
	displaying.
	2008-11-26			WAB   altered so that handles more than one domain per site
	2008-11-26			WAB   replaced some code with functions in login.cfc and relayCurrentSite.cfc
	2015-11-13          ACPK  Added Bootstrap; Removed tables and inline styling
	2015-11-17          ACPK  Display Country and Name fields on same row; Removed unneeded cf_relayFormElementDisplay element; Present list of domains as unordered list
	Possible enhancements:
	--->

<cfparam name="linksValidForMinutes" default = 10>
<cfparam name="form.selectEntityName" default = "">
<cfparam name="form.selectedEntityName" default = "">
<cfparam name="form.frmCountryID" default = "0">

<cf_includeJavascriptOnce template = "/javascript/protoTypeSpinner.js">
<cf_includeJavascriptOnce template = "/javascript/openWin.js">
<cf_includeCssOnce template="/javascript/lib/jquery/css/themes/smoothness/jquery-ui.css">
<cf_head>
	<script>
		jQuery(function() {
			jQuery('#selectEntityName').autocomplete({
				source: function(request,response,url){
						var searchParam  = request.term;
						var countryID = jQuery('#frmCountryID option:selected').val();
						$('spinner').spinner({position:'center'});
						jQuery.ajax({
							url:'/webservices/callWebService.cfc?method=callWebService&webServiceName=RelayPLOWS&methodName=getEntitiesSearch&returnFormat=plain&entityType=person&countryID=' + countryID + '&term=' + encodeURIComponent(searchParam),
							dataType: "json",
							type:"GET",
							success: function(data){
								if(data.length == 0){
									data[0] = jQuery.parseJSON('{"label":"phr_noResults","value":""}');
								}
								$('spinner').removeSpinner();
								response(jQuery.map(data, function(item){
								return {
									label:item.label,
									value:item.value
									};
								}))
							}
						});
					},
					minLength: 1, // set to 1 so that we can search for personID as well
					select: function( event, ui ) {
						setSelectedEntityID(ui.item.label,ui.item.value);
						return false;
					}
				}).focus(function(){
					/* NJH 2016/06/21 - changed from data('autocomplete');*/
	       			jQuery(this).data("ui-autocomplete").search(jQuery(this).val());
    			});
		});

		function setSelectedEntityID (label,value) {
			if(value != ''){
				jQuery('#selectedEntityName').val(value);
				jQuery('#selectEntityName').val(label);
				jQuery('form[name=EXternalSSO_Country]').submit();
				$('spinner').spinner({position:'center'});
			}
		}
	</script>
</cf_head>
<div id="externalssoContainer">
<cfoutput>
	<!--- <style>
		ul.ui-autocomplete {max-height: 300px; overflow: auto;}
	</style> --->
</cfoutput>

<!--- 2017/01/03 GCC - 453727 - change from c.countryid to l.countryid makes big performance improvement for some schemas (anaplan)
		2017/01/20 GCC further improved ot replace distinct with exists as issue persisted on other schemas (ringcentral) --->
<CFQUERY NAME="getCountries" dataSource="#application.siteDataSource#">
	select countryDescription as country, c.countryID, ISOcode  from country c
	WHERE exists (
		SELECT l.countryid FROM person p INNER join location l ON  p.locationid = l.locationid
		WHERE p.active=1 AND len(p.password) >= 3
		AND l.countryID IN (#request.relayCurrentUser.CountryList#)
		AND l.countryid = c.countryid
		)
	ORDER BY country
</CFQUERY>

	<cfoutput>
		<cfset application.com.request.setTopHead(topHeadCfm="/relay/admin/userManagement/userTopHead.cfm")>
		<div class="marginBig">#application.com.relayUI.message(message="phr_ExternalSSOhowTo",type="info")#</div>

		    <!--- 2015-11-17  ACPK    Display Country and Name fields on same row --->
			<form name="EXternalSSO_Country" id="EXternalSSO_Country" action="#thisdir#/ExternalSSO.cfm" method="post">
		<cf_relayFormDisplay>
				<div class="grey-box-top">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<cf_relayFormElementDisplay relayFormElementtype="select" currentvalue="#form.frmCountryID#" fieldname="frmCountryID" label="Country" query="#getCountries#" display="Country" value="CountryID" nullvalue="" nulltext="Select a Country">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<cf_relayFormElementDisplay required="true" class="searchBox form-control" relayFormElementtype="text" fieldname="selectEntityName" label="Name" currentvalue="#form.selectEntityName#">
							</div>
						</div>
					</div>
				</div>
				<!--- 2015-11-17    ACPK    Removed unneeded cf_relayFormElementDisplay element --->
    			<div style="width: 20px;" id="spinner"></div>
    			<cf_relayFormElementDisplay relayFormElementtype="hidden" fieldname="selectedEntityName" label="selectedEntityName" onChange="this.form.submit()" currentvalue="#form.selectedEntityName#">
	</cf_relayFormDisplay>
			</form>

		<div class="row">
			<div id="externalssoTable" class="col-xs-12">
				<cfif structKeyExists(form,"selectedEntityName") and form.selectedEntityName is not "">
					<cfquery datasource="#application.siteDataSource#" name="getuserdetails">
			SELECT 	username, password , personid
			FROM 	vpeople
			WHERE	personID =  <cf_queryparam value="#form.selectedEntityName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
					<cfif getuserdetails.recordcount gt 0>
						<!--- pkp phrase added message for result information--->
						<p>phr_ExternalSSOresultInformation</p>
						<h2>Select site to login:</h2>
						<!--- WAB 2008-11-26 change query below to function --->
						<cfset getDomainExternalSites = application.com.relayCurrentSite.getExternalSiteDefs()>
						<cfloop query="getDomainExternalSites">
							<!--- ,#application.internal UserDomains# --->
							<!--- 					<cfset encryptedLink =  URLEncodedFormat(replace(application.com.encryption.EncryptStringAES(StringToEncrypt="currentuser=#request.RelaycurrentUser.personid#&dt=#dateAdd("n",linksValidForMinutes,now())#&personid=#getuserdetails.personid#&surl=#site#",Passphrase="D17FgaT2Yhjk8gFi"),"+","||","ALL"))> --->
							<cfif not application.com.login.isPersonAValidUserOnGivenSite(personid = getuserdetails.personid, sitedefid = sitedefid).isValidUser>
								<div class="col-xs-6">#htmleditformat(form.selectEntityName)#</div>
								<div class="col-xs-6">Not a Valid User</div>
							<cfelse>
							    <!--- 2015-11-17  ACPK    Present list of domains as unordered list --->
	    						<ul>
								    <!--- WAB 2008-11-26 added loop --->
									<cfloop index="thisSite" list="#domains#">
										<cfset encryptedLink =   application.com.login.createSSOLink(personid = getuserdetails.personid,siteURL = thisSite)>
										<li>
											<a href="javascript:void(openWin('#request.currentSite.httpProtocol##thisSite#?es=#encryptedLink#','#replace(replace(thisSite,"-","","ALL"),".","","ALL")#',''));">#htmleditformat(thisSite)#</a>
										</li>
									</cfloop>
								</ul>
							</cfif>
						</cfloop>
					</cfif>
				</cfif>
			</div>
		</div>
	</cfoutput>
</div>