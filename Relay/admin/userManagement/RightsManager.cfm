<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		Rights Manager
Author:			Alex Husic/WAB
Date created:	???

	Objective - This is an admin tool designed to set the rights level that people have
				for various tools.


Date Tested:
Tested by:	SWJ

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

260100  WAB  Code only worked if the default permission was 1.
			Altered so that it works better now,  However will
			crash if you reload a page

27 Nov 00 SWJ Change the

2009-02-24   WAB created a Version 2 which atlast used permissions in a way they should be
				still working on the UI
2009-04-17   SSS a mistake that instaed of setting debug as false it was set to debug so boolean evluation was not working correctly
2009-06-24 	 NJH Bug Fix ALl Sites Issue 2388 - removed the 'Select Rights' option from dropdown as it wasn't needed and causing error when it was selected.
2015-9-25	ACPK	PROD2015-48 Dropdown menu should no longer attempt to update permissions when used
2015-09-30	ACPK	PROD2015-101 Use saved value for user rights level to remove dependency on dropdown
2016-02-05  RJT 	BF-458 IE doesn't consider the button clicked if we do a onclick = "this.form.submit()", so on the personal rights update form - using a normal submit instead
2016-03-10	WAB		CASE 448309  Encrypt all variables on a single session basis
Enhancement still to do:



--->

<!--- Amendments

 --->

	<!--- this function gets all the permission info for either a single person or all the people --->
<cffunction name="getPermissions" >
	<cfargument name="securityTypeID">
	<cfargument name="personid" default = 0>


		<CFQUERY NAME="qryGetPermissions" datasource="#application.siteDataSource#">
		SELECT p.PersonID, (p.FirstName) , (p.LastName) AS LastName,
			<cfset allPermissions = "">
			<cfset personalPermissions = "">
			<cfset inheritedPermissions = "">
			<cfloop index="i" from = "1" to = "5">
				Convert(bit,sum(r.permission & power(2,#i# -1))) AS Level#I#,
				Convert(bit,sum(case when isnull(ug.usergroupid,0) <> rg.usergroupid then r.permission & power(2,#i# -1) else 0 end)) AS InheritedLevel#I#,
				Convert(bit,sum(case when isnull(ug.usergroupid,0) = rg.usergroupid then r.permission & power(2,#i# -1) else 0 end)) AS PersonalLevel#I#,
				max(case when isnull(ug.usergroupid,0) <> rg.usergroupid  and r.permission & power(2,#i# -1) <> 0 then rgName.name else null end) as InheritedUserGroupNameForLevel#I#,
				<cfset allPermissions = listAppend (allPermissions, " (Convert(bit,sum(r.permission & power(2,#i#-1))) * power(2,#i#-1))  " ,"+")>
				<cfset inheritedPermissions = listAppend (inheritedPermissions, " (Convert(bit,sum(case when isnull(ug.usergroupid,0) <> rg.usergroupid then r.permission & power(2,#i#-1) else 0 end)) * power(2,#i#-1))  " ,"+")>
				<cfset personalPermissions = listAppend (personalPermissions, " (Convert(bit,sum(case when isnull(ug.usergroupid,0) = rg.usergroupid then r.permission & power(2,#i#-1) else 0 end)) * power(2,#i#-1))  " ,"+")>
			</cfloop>
			#allPermissions# as AllPermissions,
			#inheritedPermissions# as inheritedPermission,
			#personalPermissions# as personalPermission

			FROM 	Person AS p
						inner join
					RightsGroup AS rg on rg.PersonID = p.PersonID
						inner join
					userGroup rgName on rg.usergroupid = rgName.usergroupid
						inner join
					Rights AS r  on r.UsergroupID = rg.UsergroupID
						left join
					userGroup ug on p.personid = ug.personid
			WHERE r.SecurityTypeID =  <cf_queryparam value="#securityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and ug.personid is not null  <!--- this effectively only returns internal users who have their own user group --->
				<cfif personid is not 0>
				and p.personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
			GROUP BY r.securityTypeID,p.PersonID, p.firstname,p.lastname
			ORDER BY p.FirstName
		</CFQUERY>

	<cfreturn qryGetPermissions>

</cffunction>



<cfparam name="debug" default="false"> <!--- SSS 2009-04-17 changed this to false from debug Bug 2116 --->
<cfparam name="addLevel" default = "">
<cfparam name="removeLevel" default = "">

<CFPARAM NAME="frmTask" TYPE="string" DEFAULT="adminTask">
<!--- 2015-09-30	ACPK	PROD2015-101 Use saved value for user rights level to remove dependency on dropdown --->
<cfif structKeyExists(form,"userRightsLevel") and structKeyExists(form,"update")>
	<cf_checkFieldEncryption fieldnames="userRightsLevel" >
	<cfset frmTask = form.userRightsLevel>
</cfif>

<CFQUERY NAME="getSecurityID" datasource="#application.siteDataSource#">
SELECT c.SecurityTypeID FROM SecurityType AS c
         WHERE c.ShortName =  <cf_queryparam value="#frmTask#" CFSQLTYPE="CF_SQL_VARCHAR" >
</CFQUERY>
<CFSET securityID = getSecurityID.securityTypeID>

<!--- 2015-09-25	ACPK	PROD2015-48 Update permissions only when Update button clicked and if people checked --->
<CFIF structKeyExists(form,"People") and structKeyExists(form,"update")>
	
	<CFLOOP LIST="#People#" INDEX="personID">

			<cf_checkFieldEncryption fieldnames="people,addLevel,removeLevel,frmTask" >
			<cfset currentPermissions = getPermissions(securityTypeID = securityID, personid = personID)>

			<!--- need to add all the permissions requested which the person does not already have inherited rights to --->
			<cfset permissionsToAdd = "">
			<cfset permissionsToRemove = "">
			<cfset newPersonalPermission = currentPermissions.personalPermission>

			<cfloop index="thisLevel" list = "#addLevel#">
				<cfif debug><cfoutput>Add Level: #htmleditformat(thisLevel)# <BR></cfoutput></cfif>
				<cfif not currentPermissions["inheritedLevel#thisLevel#"][1] and not currentPermissions["personalLevel#thisLevel#"][1] >
					<!--- doesn't already have this permission inherited or personal so need to add it --->
					<!--- this is a belt and braces check to make sure that this level is not already in currentPersonalPermission --->
					<cfset isThisLevelAlreadyInPermission = bitand(newPersonalPermission , (2 ^ (thisLevel -1)))>
					<cfif debug><cfoutput>Does not have level #thisLevel#. <BR></cfoutput></cfif>
					<cfif debug><cfoutput>#newPersonalPermission#. #isThisLevelAlreadyInPermission#  <BR> </cfoutput></cfif>
					<cfif not isThisLevelAlreadyInPermission>
						<cfset newPersonalPermission = newPersonalPermission  + (2 ^ (thisLevel -1))>
						<cfif debug><cfoutput>Added</cfoutput></cfif>
					</cfif>
				</cfif>
			</cfloop>

 			<cfloop index="thisLevel" list = "#RemoveLevel#">
				<cfif debug><cfoutput>Remove Level: #htmleditformat(thisLevel)# <BR></cfoutput></cfif>
				<cfif currentPermissions["personalLevel#thisLevel#"][1] >
					<cfif debug><cfoutput>Has level #htmleditformat(thisLevel)#. <BR></cfoutput></cfif>
					<cfset isThisLevelAlreadyInPermission = bitand(newPersonalPermission , (2 ^ (thisLevel -1)))>
					<cfif debug><cfoutput>#newPersonalPermission#. #isThisLevelAlreadyInPermission#  <BR> </cfoutput></cfif>
					<cfif isThisLevelAlreadyInPermission>
						<cfset newPersonalPermission = newPersonalPermission  - (2 ^ (thisLevel -1))>
					</cfif>
				</cfif>
			</cfloop>



			<CFQUERY NAME="DeleteAndUpdatePermissions" datasource="#application.siteDataSource#">
				DELETE FROM Rights
				WHERE SecurityTypeID =  <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND MenuID = 0
				<!--- AND Permission > 1 --->
				AND UsergroupID = (SELECT UserGroupID FROM UserGroup WHERE PersonID =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > )

					<cfif newPersonalPermission is not 0>
					INSERT INTO Rights
					(UserGroupID, SecurityTypeID, CountryID, MenuID, Permission)
						SELECT ug.UserGroupID, <cf_queryparam value="#securityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0, 0, <cf_queryparam value="#newPersonalPermission#" CFSQLTYPE="CF_SQL_Integer" >
						FROM UserGroup AS ug
						WHERE ug.PersonID =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfif>
				</CFQUERY>
	</CFLOOP>
	<!---signal that session security structure needs to be rebuilt--->
	<cfset application.cachedSecurityVersionNumber = application.cachedSecurityVersionNumber+1>

</CFIF>


<CFQUERY NAME="GetSecurityTypes" datasource="#application.siteDataSource#">
	Select ShortName from SecurityType
</CFQUERY>



<CFQUERY NAME="GetPermissionNames" datasource="#application.siteDataSource#">
select
case when permissionname in ('View','Edit','Add') then 'Level' + convert(varchar,permissionlevel) else permissionname end as permissionName,
permissionlevel from permissiondefinition pd where pd.securityTypeID =  <cf_queryparam value="#securityid#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<!--- NJH 2012/08/22 - for some reason, the query above has no record count at times... not sure why.. need to ask WAB... in his absence,
	I've stuck this in.. the odd thing is that there are actual permission set for the tasks where record count is 0 and users have them assigned (or inherited from a usergroup)
	I don't understand permissions enough
 --->
<cfif not GetPermissionNames.recordCount>
	<cfquery name="GetPermissionNames" datasource="#application.siteDataSource#">
		select 'Level1' as permissionName,1 as permissionLevel
		union
		select 'Level2' as permissionName,2 as permissionLevel
		union
		select 'Level3' as permissionName,3 as permissionLevel
	</cfquery>
</cfif>

<cfset numberOfPermissions = GetPermissionNames.recordcount>
<cfset permissionNameStructure = application.com.structureFunctions.queryToStruct(query = GetPermissionNames, key = "permissionLevel")>
<cfset Permissions = getPermissions(securityTypeID = securityID)>

<CFSET Current="rightsManager.cfm">
<!--- <CFINCLUDE TEMPLATE="UserTopHead.cfm"> --->
<cfset application.com.request.setTophead(topHeadcfm="/relay/admin/userManagement/userTophead.cfm")>


<CFIF Permissions.RecordCount GT 0>
	<FORM NAME="ThisForm"  METHOD="post">
	<!--- <CFOUTPUT><INPUT TYPE="Hidden" NAME="frmTask" VALUE="#frmTask#"></CFOUTPUT> --->
	<div class="grey-box-top">
		<div class="form-group">
			<label for="frmTask">User rights level:</label>
			<SELECT NAME="frmTask" id="frmTask" class="select.small form-control" onchange="javascript:submit()">
				<!--- NJH 2009-06-24 Bug Fix ALl Sites Issue 2388 - removed as not necessarily needed, and needed to do extra work if user selected it...
				 <OPTION VALUE="0">Select The Rights to Examine</OPTION> --->
				<CFOUTPUT QUERY="GetSecurityTypes">
					<OPTION VALUE="#application.com.security.encryptVariableValue(value = ShortName, name='frmTask', singleSession = true)#"<CFIF shortname EQ frmtask>SELECTED</CFIF>>#htmleditformat(ShortName)#</OPTION>
				</CFOUTPUT>
			</SELECT>
		</div>
	</div>
	<p>In order to change a user's Rights for <CFOUTPUT>#htmleditformat(frmTask)#</CFOUTPUT> simply find the user in the list below, check their current rights level and set a new one.  Each level has a different meaning within the system and can turn on and off different functionality.</p>
	<p>You can find out which usergroup a user is inheriting their rights from by floating over the X.  [Only one item show]</p>
	<p>Rights are only added if the user does not already inherit that right from another userGroup.</p>
	<div id="rightsManager01" class="row">
		<div class="col-xs-12 col-sm-3">
			<div id="rightsManager03" class="grey-box-top">
				<div class="form-group">
					<label for="addLevel">Grant Personal Rights</label>
					<select name="addLevel" id="addLevel" class="form-control" size="#GetPermissionNames.recordcount#" multiple>
						<CFOUTPUT>
						<CFLOOP query = "GetPermissionNames">
						<OPTION VALUE="#application.com.security.encryptVariableValue(value = permissionlevel, name='addLevel', singleSession = true)#">#htmleditformat(PermissionName)#
						</cfloop>
						</cfoutput>
					</SELECT>
				</div>
				<div class="form-group">
					<label for="removeLevel">Remove Personal Rights</label>
					<select name="removeLevel" id="removeLevel" size="#GetPermissionNames.recordcount#" class="form-control" multiple>
						<CFOUTPUT>
						<CFLOOP query = "GetPermissionNames">
						<OPTION VALUE="#application.com.security.encryptVariableValue(value = permissionlevel, name='removeLevel', singleSession = true)#">#htmleditformat(PermissionName)#
						</cfloop>
						</cfoutput>
					</SELECT>
				</div>
				<input type="submit" name = "update" value="Update" class="btn btn-primary"> <!--- BF-458 RJT IE doesn't consider the button clicked if we do a onclick = "this.form.submit()", using a normal submit instead ---> <!--- 2015-09-25	ACPK	PROD2015-48 Changed input type from button to submit --->
			</div>
		</div>

		<div class="col-xs-12 col-sm-9">
<!--- 			<SELECT NAME="people" <CFIF Permissions.RecordCount LT 10> SIZE="10" <CFELSEIF Permissions.RecordCount GT 50> SIZE="50" <CFELSE>SIZE="<CFOUTPUT>#Permissions.RecordCount#</CFOUTPUT>" </CFIF> MULTIPLE>
				<CFOUTPUT QUERY="Permissions">
					<OPTION VALUE="#PersonID#:#AllPermissions#">
					<cfloop from="1" to="#getPermissionNames.recordcount#" index = "I">
						<cfif evaluate("Level#I#")>
							#permissionNameStructure[i].permissionname#
						<cfelse>
							#repeatString("&nbsp;",len(permissionNameStructure[i].permissionname)*2)#
						</cfif>
					</cfloop>
					&nbsp;&nbsp;#FirstName# #LastName#
				</CFOUTPUT>
			</SELECT>
 --->
			<cfoutput>
			<table id="rightsManager02" class="withBorder table table-hover table-striped">
				<tr><th colspan=2>Name</th>
					<th colspan=#htmleditformat(numberOfPermissions)# align="center">Overall Rights</th><th colspan=#htmleditformat(numberOfPermissions)# align="center">Personal Rights</th>
					<th colspan=#htmleditformat(numberOfPermissions)# align="center">Inherited Rights</th>
					<!--- <td width="20">&nbsp</td>
					<td></td> --->
				</tr>
				<tr>
					<td colspan="2" class="grey-table-header"></td>
					<cfloop list="a,b,c" index = "x">
						<cfloop from=1 to=#numberOfPermissions# index="i">
							<td align="center" class="grey-table-header">#permissionNameStructure[i].permissionname#</td>
						</cfloop>
					</cfloop>
				</tr>
			</cfoutput>
			<CFloop QUERY="Permissions">
				<cfoutput>
					<tr>
						<td><CF_INPUT type="checkbox" value="#application.com.security.encryptVariableValue(value = personid, name='people', singleSession = true)#" name="people" levels=""></td>
						<td style="border-right:##dddddd 1px solid;">#htmleditformat(FirstName)# #htmleditformat(LastName)# </td>
							<cfloop list="Level,personalLevel,inheritedLevel" index = "x">
								<cfloop from=1 to=#numberOfPermissions# index="i">
									<cfset value = permissions["#X##I#"][currentrow]>
										<td align="center" <cfif i is numberofpermissions>style="border-right:##dddddd 1px solid;"</cfif>>
											<cfif value><cfif x is "inheritedLevel"><abbr title="#permissions["InheritedUserGroupNameForLevel#I#"][currentrow]#">X</abbr><cfelse>X</cfif><cfelse>&nbsp;</cfif>
										</td>
								</cfloop>
							</cfloop>
					<!--- <cfif currentRow is 1>
						<td></td>
					</cfif> --->
				</tr></cfoutput>
			</cfloop>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<input type="submit" name = "update" value="Update" class="btn btn-primary"> <!--- 2015-09-25	ACPK	PROD2015-48 Changed input type from button to submit --->
		</div>
	</div>
	<!--- 2015-09-30	ACPK	PROD2015-101 Save current value for user rights level to remove dependency on dropdown --->
	<CF_INPUT type="hidden" name="userRightsLevel" value="#application.com.security.encryptVariableValue(value = frmTask, name='userRightsLevel', singleSession = true)#">
	</FORM>
<CFELSE>
	<div id="rightsManager04">
	 <p>Sorry, there are no more users with View rights that can be promoted to a higher level.</p>
	 <p>Go to the Administration pages to give users View rights.</p>
	</div>
</CFIF>