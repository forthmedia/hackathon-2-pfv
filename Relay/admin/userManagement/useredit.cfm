<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History

WAB 2001-03-19 	Altered country rights query to bring back specific rights to coutnry rather than inherited from other usergroups
WAB 2000-03-01  	added stuff to allow record right to be set on a country specific basis
WAB 2000-11-23  	Added code to give correct names for the individual permissions  Apologies if the query is now completely unreadable.
WAB 2005-04-12   	mod to getPerms query o bring back null permissions as well
WAB 2007-01-31	Modified so that only FNL admin users get full list of rights, other people just see the rights that either they have or the edited user has
WAB 2007-10-29   changes to how country rights displayed if currentuser doesn't have rights to a country that the edited user has rights to
WAB 2008-01-30  Had to add full path to the form actions so that could be included within screens
SWJ 2008-03-25	Added userGroupType to the getUserGroups query.  Also removed the call to the
PKP 2008-03-31 	Added order by userGroupType , new usergroup section, comment old usergroup section , amend getIndiv query to add columns for the cfascreen tag
NYF 2008-06-20	T10 Project.  Removed displayValidValueList next to country checkbox, it now defaults permission level to "All" (beging view & edit)
NYF 2008-06-26	T10 Project.  Amended layout of screen area to be more correct HTML - not broken table layout
WAB 2009/01/07  Bug 1579.  javascript verification not being run on included screen - so things like two selects combo not working correctly
NJH 2009/03/11 	P-SNY047 - added cfform as recordManager which seems to get called in a screen below has been changed to use cfselect
NJH 2010/09/20	Removed setting of passwords in the form as part of 8.3
WAB 2011/11/16  Added Encryption to personid passed to sendPassword
WAB 2012-06-30	P-SMA001 Added rudimentary support for Custom Rights Filtering - makes a call to getRightsFilterQuerySnippet() for the personid which returns, among other things, information as to whether customFiltering has been applied
IH	2013/01/09	Add proper labels to checkboxes
GCC 2015-02-15	Updated RW breakout IP address for user rights management - should really be changed for a setting.
WAB 2015-03-18  Changed hard coded RW breakout to use a function
 --->

<CFPARAM NAME="frmStart" DEFAULT="1">
<CFPARAM NAME="message" DEFAULT="">


<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->

<CFIF isdefined("frmEntityType")>
	<!--- page being called in Viewer --->
	<CFPARAM NAME="frmType" DEFAULT="">
	<CFPARAM NAME="frmnameText" DEFAULT="">
	<CFPARAM NAME="frmPersonType" DEFAULT="">
	<CFPARAM NAME="frmcountryId" DEFAULT="">
	<CFPARAM NAME="frmOrgName" DEFAULT="">

	<CFIF frmEntityType is "Person">
		<CFSET frmPersonID = frmCurrentEntityID>
	</cfif>

	<CFSET inViewer = true>

<CFELSE>

	<CFSET inViewer = false>

</cfif>

<!--- lists all Country ID's to which User has rights --->

<!---
<cfinclude template="/templates/qrygetcountries.cfm">
 --->

<!--- decide whether to limit rights to those that the user has--->
<cfif  application.com.security.isIpAddressTrusted () AND application.com.login.checkInternalPermissions("adminTask","Level1")>
	<cfset LimitRights = false>
<cfelse>
	<cfset LimitRights = true>
</cfif>


<!--- SWJ 25-Mar-08 This does not seem to be needed any more
<CFQUERY NAME="getPersonUG" datasource="#application.siteDataSource#">
	SELECT UserGroupID FROM UserGroup WHERE PersonID = #frmPersonID#
</CFQUERY> --->

<!--- WAB altered 2001-03-19
link though usergrou rather than rights group
this gets countries that this user is specifically given access to rather than
those inherited from the user groups they are in
 --->
<CFQUERY NAME="getPersonCountries" datasource="#application.siteDataSource#">
<!---

	SELECT ugc.CountryID
	FROM	UserGroupCountry AS ugc, UserGroup AS ug
	WHERE ug.PersonID = #frmPersonID#
	  AND ugc.UserGroupID = ug.UserGroupID
 --->
	SELECT 	r.CountryID,
			max(r.Permission) as permission
	FROM Rights AS r, UserGroup AS ug
	WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e
							 WHERE e.ShortName = 'RecordTask')
	AND r.UserGroupID = ug.UserGroupID
	AND ug.PersonID =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
	GROUP BY r.CountryID
</CFQUERY>

<CFSET personCountries = ListAppend(ValueList(getPersonCountries.CountryID),"0")>
<CFSET personCountryPermission = ListAppend(ValueList(getPersonCountries.Permission),"0")>

<!--- NB qrygetcountries has a query called getCountries too --->
<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
	<!--- all the countries to which the user has rights --->
SELECT		'1' AS permiss,
 			(select distinct a.Permission from PredefinedUGSecurity AS a, SecurityType AS f where  a.SecurityTypeID = f.SecurityTypeID and f.shortname = 'RecordTask') as defaultPermission,
			c.CountryID,
			c.CountryDescription AS CountryDescription
	FROM Country AS c
	WHERE c.CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	  AND c.ISOCode IS NOT null


 <!--- all the countries to which the person belongs but not in the above list --->
 UNION

	SELECT	'0',
			(select distinct a.Permission from PredefinedUGSecurity AS a, SecurityType AS f where  a.SecurityTypeID = f.SecurityTypeID and f.shortname = 'RecordTask') ,
			c.CountryID,
			c.CountryDescription
	FROM Country AS c
	WHERE c.CountryID  NOT IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND c.CountryID  IN ( <cf_queryparam value="#Variables.personCountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND c.ISOCode IS NOT null
	ORDER BY CountryDescription
</CFQUERY>


<!--- <!--- get the user groups to which the person is associated --->
<CFQUERY NAME="getPersonUserGroups" datasource="#application.siteDataSource#">
	SELECT UserGroupID
	FROM	RightsGroup
	WHERE PersonID = #frmPersonID#
</CFQUERY>

<CFSET personUGs = ListAppend(ValueList(getPersonUserGroups.UserGroupID),"0",",")>


<!--- get all the user groups --->
<CFQUERY NAME="getUserGroups" datasource="#application.siteDataSource#">
	SELECT ug.UserGroupID, ug.Name
	FROM UserGroup AS ug
	WHERE ug.PersonID IS null
	AND ug.UserGroupID <> 0
	ORDER BY ug.Name
</CFQUERY>

<CFSET personUGs = ListAppend(ValueList(getPersonUserGroups.UserGroupID),"0",",")>

 --->

<!--- get the user groups to which the person is associated --->
<CFQUERY NAME="getUserGroups" datasource="#application.siteDataSource#">
	SELECT ug.UserGroupID, ug.Name , userGroupType,
		case when rg.usergroupid is not null then 1 else 0 end as isMember
	FROM UserGroup AS ug
		left join
		rightsgroup rg on ug.usergroupid = rg.usergroupid and rg.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		left join
		rightsgroup rgUser on ug.usergroupid = rgUser.usergroupid and rgUser.PersonID = #request.relaycurrentuser.personid#
	WHERE ug.PersonID IS null
	AND ug.UserGroupID <> 0
	<cfif LimitRights >
		and (rg.usergroupid is not null or rgUser.usergroupid is not null )
	</cfif>

	ORDER BY userGroupType, ug.Name <!--- PKP 2008-03-31 ADDING ORDERBY FOR THE NCH-FNL008 USER MAMANAGEMENT ENHANCEMENTS--->
</CFQUERY>


<!--- WAB P-SMA001 Check whether this user will have any custom rights filtering --->
<!--- <cfset checkRights = application.com.rights.getRightsFilterQuerySnippet(entityType="location",personid = frmPersonID)>	 --->
<cfset checkRights = application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p",personid = frmPersonID)>	<!--- 2012-07-25 PPB P-SMA001 use for filtering too; changed entityType to person --->

<CFQUERY NAME="getIndiv" datasource="#application.siteDataSource#">
	SELECT  p.PersonID,
			p.FirstName,
			p.LastName,
			p.Username,
			p.Password,
			p.PasswordDate,
			p.LoginExpires,
			p.FirstTimeUser,
			p.LastUpdated,
			l.Address1,
			l.Address2,
			l.Address3,
			l.Address4,
			l.Address5,
			l.PostalCode,
			<!--- PKP 2008-03-31 ADDING EXTRA FOR THE CFASCREEN TAG NCH-FNL008 USER MAMANAGEMENT ENHANCEMENTS--->
			l.countryid,
			l.locationid,
			l.organisationid,
			<!--- PKP 2008-03-31 ADDING EXTRA FOR THE CFASCREEN TAG NCH-FNL008 USER MAMANAGEMENT ENHANCEMENTS--->
			c.CountryDescription,
			o.OrganisationName,
			ug.usergroupid
	FROM Person p inner join Location l on p.locationid = l.locationid
	inner join Organisation o on o.organisationid =l.organisationid
	inner join Country c on l.CountryID = c.CountryID
	left outer join usergroup ug on p.personid =ug.personid
	WHERE p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	<!--- 2012-07-25 PPB P-SMA001 commented out
	  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-08-06 PPB P-SMA001 don't use checkRights.WhereClause which relates to the person being edited not the loggedin person --->
	AND p.Active <> 0 <!--- true --->
</CFQUERY>

<CFIF getIndiv.Recordcount IS 0>
	<CFSET message = "The record for the person selected could not be found or is not an active record.">
	<CFINCLUDE TEMPLATE="userFind.cfm">
	<CF_ABORT>
</CFIF>

<!--- check whether login has been locked, password expired etc. --->
<cfset checkLogin = application.com.login.getWhyLoginFailed(Username=getIndiv.Username, password = getIndiv.Password, isInternal = true, passwordEncrypted =true)>


<!--- determine if the user is currently a valid user of the system --->
<CFSET CurrentUser = "yes">

<!--- date diff : date,now is +, then date<now --->

<CFIF (#DateDiff("s",getIndiv.LoginExpires,Now())# GTE 0)
	OR (#Trim(getIndiv.Username)# IS "")
	OR (#Trim(getIndiv.Password)# IS "")
	OR (#Trim(getIndiv.UserGroupID)# IS "")><!---
	removed as this is not enforced
	(#DateDiff("d",getIndiv.PasswordDate,Now())# GT #application.pass ValidFor#)
	OR --->

	<CFSET CurrentUser = "no">
</CFIF>

<CFIF Trim(getIndiv.Username) IS "">
	<CFSET qryUsername = Trim(getIndiv.FirstName) & " " & Trim(getIndiv.LastName)>
	<CFSET qryUsername = Trim(qryUsername)> <!--- get rid of any spaces if either name is blank --->
	<CFSET qryLoginExpires = #DateAdd("yyyy",2,now())#> <!--- WAB added instead of similar lines below	 --->
<CFELSE>
	<CFSET qryUsername = Trim(getIndiv.Username)>
	<CFSET qryPasswordDate = DateFormat(getIndiv.PasswordDate,"dd-mmm-yyyy")>
	<CFSET qryLoginExpires = DateFormat(getIndiv.LoginExpires,"dd-mmm-yyyy")>

</CFIF>


<!--- WAB removed this bit, tended to reset Login Expires of people who had deliberately been expired
	now only done above for completely new users
<CFIF CurrentUser IS "no" OR NOT IsDefined("qryLoginExpires")>

	<!--- <CFSET qryLoginExpires = #DateAdd("yyyy",2,"1-Mar-98")#> --->
	<CFSET qryLoginExpires = #DateAdd("yyyy",2,now())#>
</CFIF>
--->


<CFSET qryPassword = getIndiv.Password>


<CFPARAM NAME="qryUsername" DEFAULT="">
<CFPARAM NAME="qryPassword" DEFAULT="">
<CFPARAM NAME="frmOrgName" DEFAULT="">
<CFPARAM NAME="qryPasswordDate" DEFAULT="#DateAdd('m',3,Now())#">

<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">
<!--
	<!--- forViewer --->
	function submitPrimaryForm () {

		return checkForm()

	}


	function checkForm() {
		var form = document.mainForm;
		var msg = "";
		//check that the form is valid
		//if (form.frmNewPassword1.value != form.frmNewPassword2.value || form.frmNewPassword1.value == "") {
		//	msg += "\n* The same password must be entered twice.";
		//}
		if (form.insUsername.value == "") {
			msg +="\n* You must enter a username.";
		}

			var anyCos = "no";
		var anyGroups = "no";
		nodatagrp = "no";

		<!--- check to see if the following are selected:
				any countries
				any user groups or
				if countries, then usergroup 3 --->

		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == "insCountryID") {
				if (thiselement.checked) {
					anyCos = "yes";
				}
			}
			if (thiselement.name == "insUserGroupID") {
				if (thiselement.checked) {
					anyGroups = "yes";
				}
				if (thiselement.value == 3 && thiselement.checked) {
					nodatagrp = "yes";
				}
			}

		}



		if (anyCos == "no") {
			msg+= "\n* You must select at least one country";
		}


		if (anyGroups == "no") {
			msg+= "\n* You must select at least one user group";
		}

		if (form.insUsername.value == "") {
			msg +="\n* Username is required";
		}

		<!--- WAB 2009/01/07 Bug-1579
			need to call this function if it exists (it is the javascript for the included screen)
		--->
		if (verifyInput) {
			msg += verifyInput()
		}

		if (msg == "") {
			form.submit();
		} else {
			alert("Please correct the following and try again:\n\n" + msg);
		}

	}

	function delUser() {
		var form = document.delUser;
		var msg = "\nAre you sure that you want to delete this user?";
		if (confirm(msg)) {
			form.submit();
		}
		return;
	}

	function sendPassword(personID) {

		page = '/webservices/callWebService.cfc?'
		parameters = 'method=callWebService&webServiceName=user&methodName=sendUserPassword&personID='+personID+'&_cf_nodebug=true&returnFormat=json';

		var myAjax = new Ajax.Request(
			page,
			{
					method: 'post',
					parameters: parameters,
					evalJSON: 'force',
					debug : false,
					asynchronous:false
			}
		)
		if ($('messageText') == undefined) {
			$('userMgmtTable').insert({before:'<div id="messageText" class="successblock"></div>'});
		}

		$('messageText').innerHTML = myAjax.transport.responseText.evalJSON().MESSAGE;
	}

	function resetLock(personID) {
			page = '/webservices/callWebService.cfc?'
		parameters = 'method=callWebService&webServiceName=user&methodName=clearLock&personID='+personID+'&_cf_nodebug=true&returnFormat=json';

		var myAjax = new Ajax.Request(
			page,
			{
					method: 'post',
					parameters: parameters,
					evalJSON: 'force',
					debug : false,
					asynchronous:true,
					onComplete: function (requestObject,JSON) {
							json = requestObject.responseJSON ;
							if ($('messageText') == undefined) {
								$('userMgmtTable').insert({before:'<div id="messageText" class="successblock"></div>'});
							}
							$('messageText').innerHTML = json.MESSAGE}
			}
		)

	}

//-->
</SCRIPT>

</cf_head>


<CFSET Current="userFind.cfm">
<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>

<cfoutput>
#application.com.relayUI.message(message=message,id="messageText")#

<!--- form to pass search criteria to new location --->
<FORM NAME="NewForm" METHOD="POST" ACTION="/admin/usermanagement/userlist.cfm">
	<CF_INPUT TYPE="HIDDEN" NAME="frmType" VALUE="#frmType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNameText" VALUE="#frmNameText#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonType" VALUE="#frmPersonType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="userlist">
</FORM>

<!--- form to pass search criteria to new location --->
<FORM NAME="DelUser" METHOD="POST" ACTION="/admin/usermanagement/userdelete.cfm">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="#getIndiv.PersonID#">
	<CF_INPUT TYPE="HIDDEN" NAME="insLastUpdated" VALUE="#CreateODBCDateTime(getIndiv.LastUpdated)#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmType" VALUE="#frmType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNameText" VALUE="#frmNameText#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonType" VALUE="#frmPersonType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgName" VALUE="#frmOrgName#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="userlist">
</FORM>
</cfoutput>

<!--- NJH 2009/03/11 CR-SNY675 - added cfform as recordManager which seems to get called in a screen below has been changed to use cfselect --->
<FORM NAME="mainForm" ACTION="/admin/usermanagement/usertask.cfm" METHOD=POST>
	<cfoutput> <!--- NJH 2009/03/12 --->
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="#getIndiv.PersonID#">
	<CF_INPUT TYPE="HIDDEN" NAME="insLastUpdated" VALUE="#CreateODBCDateTime(getIndiv.LastUpdated)#">
	<INPUT TYPE="HIDDEN" NAME="frmType" VALUE="all">
	<INPUT TYPE="HIDDEN" NAME="sendPassword" VALUE="no">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNameText" VALUE="#frmNameText#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonType" VALUE="#frmPersonType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgName" VALUE="#frmOrgName#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="userlist">
	<!--- 	<INPUT TYPE="HIDDEN" NAME="insPasswordDate_eurodate" VALUE="You must enter the password date in 'dd-mmm-yyyy' format."> --->
	<INPUT TYPE="HIDDEN" NAME="insLoginExpires_eurodate" VALUE="You must enter the login date in 'dd-mmm-yyyy' format.">
</cfoutput>

	<CFIF inViewer>
		<CFSET frmNextPage="useredit.cfm">
	</cfif>

<cf_includeJavascriptOnce template="/javascript/login.js" translate="true">


<div id="userMgmtTable">
	<CFOUTPUT>
		<cfif not LimitRights>
			<p class="infoblock" id="LimitRights">This view is not filtered to your usergroup rights because you have adminTask level1 and are accessing this site from the Relayware offices</p>
		</cfif>
			<h2>#htmleditformat(getIndiv.FirstName)# #htmleditformat(getIndiv.LastName)#</h2>

			<CFIF #Variables.CurrentUser# IS "yes">
				(currently <I>is</I> a valid user)  <BR>
			<A HREF="javascript:sendPassword('#application.com.security.encryptVariableValue(name="personid", value=frmpersonId)#')" class="btn btn-primary">Send Password</a>

			<CFELSE>
				<p>(currently <I>is not</I> a valid user)</p>
			 </CFIF>
			 <cfif not checkLogin.isOK and checkLogin.reason is "loginLock">
				<p>This account was locked at #application.com.security.sanitiseHTML(application.com.datefunctions.dateTimeFormat(checkLogin.user.loginLastLock))# after #htmleditformat(checkLogin.user.loginAttempt)# failed attempts<BR>
				It will be locked for another #htmleditformat(checkLogin.user.loginLockDuration)# minutes</p>
				<a href="javascript:resetLock(#frmPersonID#)" class="btn btn-primary">reset</a>
			</cfif>
	</CFOUTPUT>

<h2>Login Details</h2>
<div class="form-group">
	<label for="insUsername">Username:</label>
	<CF_INPUT TYPE="TEXT" NAME="insUsername" id="insUsername" VALUE="#qryUsername#" SIZE="30" MAXLENGTH="50">
</div>

<div class="form-group">
	<label for="insLoginExpires">Login Expires:</label>
	<CF_relayDatefield fieldname="insLoginExpires" id="insLoginExpires" currentValue="#DateFormat(qryLoginExpires,'dd-mmm-yyyy')#"></TD>
</div>

<h2>User's Rights by Country</h2>
<table>
<CFSET half = Ceiling(getCountries.RecordCount/2)><!--- rounds up so 1st column may have 1 more item than second --->
	<CFOUTPUT QUERY="getCountries" >

	<!--- does this person have rights to this country
			if so get the permission, otherwise use default permission --->
		<CFSET x = ListFind(personCountries,CountryID)>
		<CFIF x is not 0>
			<CFSET thisPermission = listgetat(personCountryPermission, x)>
		<CFELSE>
			<CFSET thisPermission = defaultPermission>
		</cfif>

		<!--- NYF 2008-06-20 replaced "CurrentRow MOD 2 IS NOT 0" with "CurrentRow MOD 3 IS 1" to give 3 columns ---->
		<CFIF CurrentRow MOD 3 IS 1>
			<TR>
		</CFIF>
		<!--- Show checkbox only if user has permissions --->
		<TD VALIGN="TOP">

				<CFIF Permiss IS 1>
					<CF_INPUT type="hidden" NAME="countryIDList" VALUE="#CountryID#">  <!--- WAB 2007-10-29 needed to add because when a user with limited country rights edited a user with more country rights, countryrights were getting lost --->
					<cfset tempMethod = "edit">
				<cfelse>
					<cfset tempMethod = "view">
				</CFIF>
				<!--- NYF 2008-06-20 T10 Project ->
				<cf_displayValidValues
					formFieldName = "insCountryPermission_#CountryID#"
					validValues = "1#application.delim1#View,3#application.delim1#Update,7#application.delim1#All,2,4,5,6,8,9,15#application.delim1#Partner Rights"
					currentValue = #thisPermission#
					showNull = false
					nullText = " "
					method = #tempmethod#
					>
				<- NYF   --->
				<CF_INPUT TYPE="CHECKBOX" NAME="insCountryID" id="insCountryID_#CountryID#" VALUE="#CountryID#" CHECKED="#iif(ListFind(personCountries,CountryID) IS NOT 0,true,false)#" disabled="#iif( Permiss IS not 1,true,false)#"	><label for="insCountryID_#CountryID#">#htmleditformat(CountryDescription)#</label>
 		     </TD>
		<!--- NYF 2008-06-20 replaced "CurrentRow MOD 2 IS 0" with "CurrentRow MOD 3 IS 0" to give 3 columns ---->
		<CFIF CurrentRow MOD 3 IS 0>
			<!--- *NYF 2008-06-20 T10 Project - added... -> ---->
			<CFIF not isdefined("noRows")>
				<cfset noRows = ceiling(getCountries.recordcount / 3)>
				<td rowspan="#noRows#" valign="top"><cfinclude template="RegionSelect.cfm"></td>
			</cfif>

		</TR>
		</CFIF>

	</CFOUTPUT>

	<!--- NYF 2008-06-20 T10 Project - replaced ->
	<CFIF #getCountries.RecordCount# MOD 2 IS NOT 0 OR #getCountries.RecordCount# IS 0>
		<CFIF #getCountries.RecordCount# IS 0>	<!--- TR never opened --->
			<TR>
		</CFIF>
		<!--- if no records or odd number, add a column before ending row--->
		<TD COLSPAN=2>
			&nbsp;
 		 </TD></TR>
	</CFIF>
	..with.. -> ---->
	<CFIF #getCountries.RecordCount# MOD 3 IS NOT 0 OR #getCountries.RecordCount# IS 0>
		<!--- if no records or odd number, add a column before ending row--->
		<CFIF #getCountries.RecordCount# MOD 3 IS 1>	<!--- TR never opened --->
			<TD>
		<CFELSEIF #getCountries.RecordCount# MOD 3 IS 2>	<!--- TR never opened --->
			<TD COLSPAN=2>
		<CFELSE>
			<TR>
			<TD COLSPAN=3>
		</CFIF>
			&nbsp;
 		 	</TD>
		</TR>
	</CFIF>

	</TABLE>

<!--- WAB P-SMA001 Output a message if user will have custom rights filtering --->
<cfif checkRights.customRightsApplied>
	<h2>Custom Filtering</h2>
	<cfoutput>#checkRights.customRightsMessage#</cfoutput>
</cfif>

<!---- next section --->
<h2>User is a member of the following User Groups</h2>
	<TABLE>
	<CFSET half = Ceiling(getuserGroups.RecordCount/2)><!--- rounds up so 1st column may have 1 more item than second --->
	<!--- if person in country to which the user has no rights,
		do not display checkbox --->
	<cfif limitrights>
		<tr>
			<td colspan="2">
				Showing only User Groups which either You or the User have rights to
			</td>
		</tr>
	</cfif>

	<!--- <cfif getUserGroups.recordcount gt 0>
		<tr>
			<td colspan="2">
				<cf_invertselection form="mainForm" checkbox="insUserGroupID" displayas="anchor">
			</td>
		</tr>
	</cfif> --->
	<!--- PKP 2008-03-31 BEGIN NEW SECTION FOR USERGROUP MANAGEMENT NCH-FNL008 USER MAMANAGEMENT ENHANCEMENTS--->
	<!--- <INPUT TYPE="HIDDEN" NAME="frmnextpage" VALUE="admin/usermanagement/useredit.cfm"> --->
	<CFQUERY NAME="getuserGroupTypes" datasource="#application.siteDataSource#">
		SELECT DISTINCT userGroupType
		FROM UserGroup
	</CFQUERY>

	<cfoutput query="getuserGroupTypes">
	<TR>
		<TD>
			<h3>#htmleditformat(userGroupType)#</h3>
		</TD>
	</TR>

		<cfset rowcounter = 0 >
		<CFloop QUERY="getUserGroups">
		<CFIF getuserGroupTypes.userGroupType EQ getUserGroups.userGroupType>
			<cfset rowcounter = rowcounter + 1>
			<CFIF rowcounter mod 3 is 1>
			<TR>
			</CFIF>
				<TD VALIGN="TOP">
					 <label><CF_INPUT TYPE="CHECKBOX" NAME="insUserGroupID" VALUE="#UserGroupID#" checked="#iif( isMember,true,false)#" >#HTMLEditFormat(Name)#</label>
				</TD>
			<CFIF rowcounter mod 3 is 0>
			</TR>
			</CFIF>
		</CFIF>
	</CFLOOP>

	</cfoutput>

	<!--- <CFloop QUERY="getUserGroups">
		<CFSET PersonPerm = "">

		<!--- <CFIF getUserGroups.SecurityTypeID NEQ "" AND getPersonUG.RecordCount GT 0>

			<CFQUERY NAME="getPersonPerms" datasource="#application.siteDataSource#">
				SELECT Permission AS IndPermission
				FROM Rights
				WHERE UserGroupID = #getPersonUG.UserGroupID#
				AND SecurityTypeID = #getUserGroups.SecurityTypeID#
			</CFQUERY>

			<CFSET PersonPerm = getPersonPerms.IndPermission>
		</CFIF> --->

		<CFIF CurrentRow MOD 2 IS NOT 0>
			<TR>
		</CFIF>

		<TD VALIGN="TOP" ALIGN="Right">
			<!--- <CFOUTPUT>#PersonPerm# - #Permission#</CFOUTPUT> &nbsp;  --->
			<CFOUTPUT><INPUT TYPE="CHECKBOX" NAME="insUserGroupID" VALUE="#UserGroupID#" <cfif isMember>checked</cfif> ></CFOUTPUT>
			</TD>
		<TD VALIGN="TOP" ALIGN="LEFT">
			 <CFOUTPUT>#HTMLEditFormat(Name)#</CFOUTPUT>
 		     </TD>
		<CFIF CurrentRow MOD 2 IS 0>
		</TR>
		</CFIF>
	</CFloop> --->
	<!--- PKP 2008-03-31 END NEW SECTION FOR USERGROUP MANAGEMENT NCH-FNL008 USER MAMANAGEMENT ENHANCEMENTS--->
	<CFIF getUserGroups.RecordCount MOD 2 IS NOT 0 OR getUserGroups.RecordCount IS 0>
		<CFIF getUserGroups.RecordCount IS 0>	<!--- TR never opened --->
			<TR>
		</CFIF>
		<!--- if no records or odd number, add a column before ending row--->
		<TD COLSPAN=2>
			&nbsp;
 		 </TD></TR>
	</CFIF>
	</TABLE>

<!--- pkp to do this stuff--->

<!---- next section --->
<h2>User Edit Screens</h2>

<cfset request.relayFormDisplayStyle = "HTML-div">
<CF_relayFormElementDisplay relayFormElementType="SCREEN" currentvalue="" fieldname="" label="">
	<cf_ascreen formName = "mainForm">
		<cf_ascreenitem screenid="userEditScreen"
				method="edit"
				person="#getIndiv#"
				location="#getIndiv#"
				organisation="#getIndiv#"
		>
	</cf_ascreen>
</CF_relayFormElementDisplay>

<cf_input type="button" value="Save" onClick="JavaScript:checkForm();" class="btn btn-primary">


	</FORM>