<!--- �Relayware. All Rights Reserved 2014 

WAB 2015-12-01  PROD2015-26 Visibility.  Allow UserGroups to be defined using complicated conditions.  Demise of the rightsFlagGroup table
WAB 2017-01-30	RT-212 Minor alteration to conditionSQL update sql 
--->





<CFSET Current="flagManagement.cfm">
<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>


<CFIF NOT IsDefined("frmUserGroupID")>
	<CFLOCATION URL="UserGroupManagement.cfm?UserManagement=1"addToken="false">
<CFELSE>

<cfif structKeyexists (form,"submitButton")>
	<cfif isDefined("condition")>
		<!--- first do some validation 
			can only contain
			fully qualified fields: x.y     \w*\.\w*
			operators <> =					[<>=!]
			numbers							[0-9]
			matched quotes with stuff inside  '[^']*'
		--->
		<!--- must not contain ; --->
	
		<cfset valid = true>
		<cfset validateMessage = "">
	
		<!--- 	This function attempts to validate the SQL by removing anything which is known to be valid.
				If, at the end, anything is left then there must be a problem
				Can't guarantee it is 100% effective, and might cause issues if conditions get very complicated
		--->

		<cffunction name="validateSQL">
			<cfargument name="SQL">

			<cfset var result = {isOK = true}>
			<!--- first remove all comments --->	
			<cfset sql = application.com.regExp.removeComments (string = sql ,type = "SQL",preserveLength = true)>
			<cfset sql = application.com.regExp.removeComments (string = sql,type = "SQLSingleLine",preserveLength = true)>
					
			<!--- then remove anything like xxx.yyy, operators, numbers, anything between quotes --->		
			<cfset sql = reReplaceNocase (sql,"((person|location|organisation|country)\.\w*|[<>=!]|[0-9]|'[^']*'|AND|OR|NOT|LIKE|BETWEEN|dbo.listfind|,)","","ALL")>
			<cfset sql = reReplaceNocase (sql,"\(([^\(]*)\)","\1","ALL")>  <!--- removes matched brackets --->
			<cfset sql = reReplaceNocase (sql,"\(([^\(]*)\)","\1","ALL")>  <!--- removes matched brackets if nested!, should do loop. --->
			
			<cfif trim(sql) is not "">
				<cfset result = {isok = false, invalidCode = trim(sql)}>
			</cfif>
			
			<cfreturn result>
		</cffunction>
	
		<cfset validateSQLResult = validateSQL (condition)>
		<cfif not validateSQLResult.isOK>
			<cfset valid = false>
			<cfset validateMessage = "Invalid SQL. Words '#validateSQLResult.invalidCode#' are not recognised">
		</cfif>
	
		<cfif valid and trim(condition) is not "">
	
			<cfquery name="validateCondition">
			select 
				<cf_queryparam value = "#condition#" cfsqltype="CF_SQL_VARCHAR" > as condition, 
				null as conditionResult,
				cast (null as nvarchar(max)) as validatemessage
			into ##tempCondition
		
			exec testConditions_POL @personid = 0
			
			select * from ##tempCondition where conditionResult is null
			
			</cfquery>
	
			<cfif validateCondition.recordCount is not 0 >
				<cfset valid = false>			
				<cfset validateMessage = validateCondition.validateMessage>			
			</cfif>
	
		</cfif>
	
	
		<cfif valid>
			<!--- WAB 2017-01-30 RT-212 Added OR conditionValidated = 0.
						If for some reason the existing SQL is correct but has been marked invalid we need to do the update and set it to valid even if	the condition had not changed			
						(particular problem after 2016.1.6 upgrade when flag views not being recreated properly and conditions being set to not validation PROD2016-3299)
			 --->
			<cfquery name="doupdate" result="doupdateResult">
				update 	
					userGroup 
				set 
					conditionsql = <cf_queryparam value = "#condition#" cfsqltype="CF_SQL_VARCHAR" >, 
					conditionValidated = 1
				where 
					  usergroupid = <cf_queryparam value = "#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >
					  and (isnull(conditionsql,'') <> <cf_queryparam value = "#condition#" cfsqltype="CF_SQL_VARCHAR" > OR conditionValidated = 0)
			
			</cfquery>

			<cfif doUpdateResult.recordCount is not 0>
				<cfset application.com.relayUI.setMessage (type = "success", message = "Record Saved")>
			</cfif>

		<cfelse>

			<cfset application.com.relayUI.setMessage (type = "error", message = "Invalid Condition:<BR>#condition#<BR><BR>#validateMessage#")>

		</cfif>
		
		
	</cfif>
</cfif>

	
	<SCRIPT>
		function doFlagOnChange(obj){
			if (obj.value == '') {return}

			var form = obj.form
			textArea = form.condition

			/*  If custom condition then we append new item to what is there with an AND 
				otherwise we update
				We tell if it is a custom condition by whether the text area is read only
			*/

			var customCondition = !textArea.getAttribute('readonly') 

			if (customCondition) {
				if (textArea.value != '') {
					textArea.value += String.fromCharCode(10) + ' AND ' + String.fromCharCode(10)
				}
				textArea.value += obj.value
			
			} else {
				textArea.value = obj.value
			}
			
		}
		
		function editCondition(obj){
			var textArea = obj.form.condition
			textArea.removeAttribute('readonly')
			
			/* reset the drop downs */		
			obj.form.flagGroup.selectedIndex = 0
			obj.form.conditionList.selectedIndex = 0
		}

	</SCRIPT>
	
	<!---Group Combo--->
	<CFQUERY NAME="GroupName" datasource="#application.siteDataSource#">
		Select UserGroupID, Name
		FROM UserGroup
		WHERE PersonID IS NULL
		ORDER BY Name
	</CFQUERY>
	
	

	<CFQUERY NAME="StandardConditions" datasource="#application.siteDataSource#">
	select 
		* 
	from
		vFlagConditions
	where 
		entityTypeID in (0,1,2)	 
		and isnull(condition,'') <> ''
	order by
		entityTypeID , flagGroup	
	</CFQUERY>


	<cfset currentValue = "">
	<cfset customCondition = false>

	<cfif frmUserGroupID is not "">
		<CFQUERY NAME="CurrentCondition" datasource="#application.siteDataSource#">
			Select UserGroupID, conditionSQL, conditionValidated
			FROM userGroup
			where userGroupID = <cf_queryparam value = "#frmUserGroupID#" cfsqltype="CF_SQL_INTEGER" >
		</CFQUERY>
	
		<!--- is current condition a standard condition? --->	
		<cfquery name="checkCurrentCondition" dbtype="query">
		select * from standardconditions where condition = '#CurrentCondition.conditionSQL#'
		</cfquery>


		<cfif checkCurrentCondition.recordCount>
			<cfset currentValue = CurrentCondition.conditionSQL>
		<cfelseif CurrentCondition.conditionSQL is not "">
			<cfset customCondition = true>
		</cfif>

	</cfif>
	


	<FORM ACTION="FlagManagement.cfm" METHOD="POST" name="conditionForm">
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">		
	<TR>
		<TD colspan="3" align="center">
			<B>User Group:</B><BR>
			<SELECT ID="Select_frmUserGroupID" NAME="frmUserGroupID" onChange="this.form.submit()">  <!--- WAB 2012-06-11 added an ID to help clive with testing (there were other objects on page with same name)--->
				<CFOUTPUT QUERY="GroupName">
					<OPTION VALUE=#UserGroupID# <CFIF IsDefined("frmUserGroupID")><CFIF UserGroupID EQ #frmUserGroupID#>SELECTED</CFIF></CFIF> >#htmleditformat(Name)#
				</CFOUTPUT>
			</SELECT>
		</TD>
	</TR>
	<TR>
		<TD colspan="3" align="center">
				<B></B><BR>
				<!--- Note capitalisation of conditionList and flagGroup are important to get js to run--->
				<cfoutput>#application.com.relayForms.validValueDisplay (name="conditionList", query = StandardConditions, group="flagGroup", displayGroupAs="select", formname="conditionForm", showNull="true", selected=currentValue, emptyText1 = "Choose Profile", emptyText2= "Choose Condition", onchange="doFlagOnChange(this)", display="description", value="condition")#</cfoutput>
		
				<cfoutput><textarea name = "condition" rows=5 cols = 60 <cfif customCondition is false>readonly="true"</cfif>>#CurrentCondition.conditionSQL#</textarea></cfoutput>
				<cfif customCondition is false>
					<br /><input Type="Button" value="Customize Condition" onclick=editCondition(this)>
				</cfif>
					<br /><INPUT TYPE="submit" name="submitButton" value = "Update">
	
		</TD>
	
	</TR>
	
			
				
	</TABLE>
	
	</FORM>

</CFIF>



