<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2005-10-19   modified so that condition for being a user includes having a personal usergroup
NJH 2009-06-16	LID 2366 - added join to get people in a particular user group. Also tidied up some IF statements with rights and permissions
				as it fell over if rights was not defined.
GCC 2009-07-29  LID 2484 remove validPassFor from query as it is not used in anger and hides valid users that have passwords created before that time interval.
NAS	2010-07-22	LID 3721 User & Security Error - Login would be expired, but would not show person who has been expired.
WAB 2011-11-16  Added Encryption to personid passed to sendPassword
 --->


<cfparam name="frmPersonType" default="users">

<!---If coming from the search engine--->
<CFIF IsDefined("frmRights")>
	<CFIF Trim(frmRights) IS 'All'>
		<CFLOCATION URL="userrightsCrosstab.cfm?frmCountryID=#frmCountryID#"addToken="false">
		<CF_ABORT>
	</CFIF>
</CFIF>

<!---If expiring password--->
<CFIF structKeyExists(FORM,"expirePersonID") and FORM.expirePersonID neq 0>
	<CFQUERY NAME="expirePassword" datasource="#application.SiteDataSource#">
		update person set loginExpires = '01-jan-2000'
				where personID =  <cf_queryparam value="#FORM.expirePersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>

<CFSET displaystart = 1>

<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->

<CFPARAM NAME="qryNameText" DEFAULT="">
<!--- check to see if any criteria exists --->
<CFPARAM NAME="AnyCriteria" DEFAULT="no">
<CFPARAM NAME="message" DEFAULT="">

<CFSET PermissionsList = "View,2,View/Edit,4,5,6,View/Edit/Add">

<!--- 2012-07-25 PPB P-SMA001 commented out
<!--- lists all Country ID's to which User has rights --->
<cfinclude template="/templates/qrygetcountries.cfm">
--->

<CFQUERY NAME="getIndiv" datasource="#application.sitedatasource#">
	SELECT DISTINCT p.PersonID,
			p.FirstName,
			p.LastName,
			p.Active,
			o.OrganisationName,
			c.CountryDescription,
			p.PasswordDate,
			p.loginExpires,
			(select max(loginDate) from usage where personID = p.personid) as lastLoginDate,
			p.userName,
			(select 1 from person where person.personid = p.personid
									AND	person.Password <> ''
									AND	person.Username <> ''
									<!--- AND person.PasswordDate >= #CreateODBCDateTime(DateAdd("d",-application.Pass ValidFor,Now()))#  this ought to be part of the test, but it isn't being enforced so can't be used --->
									<!--- NAS	2010-07-22	LID 3721 User & Security Error - Login would be expired, but would not show person who has been expired. --->
									<CFIF Not IsDefined("FORM.expirePersonID")>
										AND	person.LoginExpires > #CreateODBCDateTime(Now())#
									</cfif>
									AND person.active <>0) as validuser
		<!--- NJH 2009-06-16 LID 2366 --->
		<CFIF (IsDefined("frmPermission") and frmPermission NEQ 0) or (IsDefined("frmRights") and frmRights NEQ 0)>
			<!--- <CFIF frmPermission NEQ 0 OR frmRights NEQ 0> --->
			, r.Permission
			<!--- </CFIF> --->
		</CFIF>
	FROM Person AS p inner join Location AS l on p.LocationID = l.LocationID
		 INNER JOIN Organisation AS o on l.OrganisationID = o.OrganisationID
		 INNER JOIN Country AS c on l.CountryID = c.CountryID

	<cfif (IsDefined("frmPermission") and frmPermission NEQ 0) or (IsDefined("frmRights") and frmRights NEQ 0) or (IsDefined("frmUserGroupID") and frmUserGroupID NEQ 0)>
		inner join rightsGroup rg with (noLock) on rg.personID = p.personID
		<cfif IsDefined("frmUserGroupID") and frmUserGroupID NEQ 0> and rg.userGroupID =  <cf_queryparam value="#frmUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
		<cfif (IsDefined("frmPermission") and frmPermission NEQ 0) or (IsDefined("frmRights") and frmRights NEQ 0)>
			inner join rights r on rg.usergroupID = r.usergroupID
			<cfif (IsDefined("frmPermission") and frmPermission NEQ 0)>
				and r.Permission =  <cf_queryparam value="#frmPermission#" CFSQLTYPE="CF_SQL_Integer" >
			</cfif>
			inner join securityType s on r.securitytypeID = s.securitytypeID
			<cfif (IsDefined("frmRights") and frmRights NEQ 0)>
				 AND s.securitytypeID =  <cf_queryparam value="#frmRights#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfif>
	</cfif>
	<!--- 2012-07-25 PPB P-SMA001 commented out
	WHERE l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	 --->
	WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->

<!--- 	  AND p.Active = true show both but only allow active people to become users --->
	  <CFIF frmCountryID IS NOT 0>
	 	AND l.CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	  </CFIF>
	  <CFIF isDefined("frmOrgName") and Trim(frmOrgName) IS NOT "">
	 	AND o.organisationName  like  <cf_queryparam value="#frmOrgName#%" CFSQLTYPE="CF_SQL_VARCHAR" >
	  </CFIF>
	<CFIF Trim(frmNameText) IS NOT "">
		  <CFIF CompareNoCase(frmType,"First") IS 0>
		  	AND ltrim(rtrim(p.FirstName))  LIKE  <cf_queryparam value="#Trim(frmNameText)#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		  <CFELSEIF CompareNoCase(frmType,"Last") IS 0>
		  	AND ltrim(rtrim(p.LastName))  LIKE  <cf_queryparam value="#Trim(frmNameText)#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		  <CFELSE>
  		    AND {fn CONCAT({fn CONCAT(ltrim(rtrim(p.FirstName)),' ')},ltrim(rtrim(p.LastName)))} =  <cf_queryparam value="#Trim(frmNameText)#" CFSQLTYPE="CF_SQL_VARCHAR" >
<!--- 		    AND p.FirstName & ' ' & p.LastName = '#frmNameText#' --->
	<!--- 	  	AND ({fn CONCAT({fn CONCAT(p.FirstName,' ')},p.LastName)} LIKE '#Trim(frmNameText)#') --->
		  </CFIF>
	</CFIF>
<!--- 	  p.FirstName LIKE '#Trim(frmNameText)#'
   	OR p.LastName LIKE '#Trim(frmNameText)#' --->
	  <CFIF CompareNoCase(frmPersonType,"users") IS 0>
  		AND	p.Password <> ''
		AND	p.Username <> ''
 		<!--- AND p.PasswordDate >= #CreateODBCDateTime(DateAdd("d",-application.pass ValidFor,Now()))#  this ought to be part of the test, but it isn't being enforced so can't be used --->
		<!--- NAS	2010-07-22	LID 3721 User & Security Error - Login would be expired, but would not show person who has been expired. --->
		<CFIF Not IsDefined("FORM.expirePersonID")>
			AND	p.LoginExpires > #CreateODBCDateTime(Now())#
		</cfif>
		<!--- this line added by WAB - need to have a usergroup to be internal users, external users can have passwords --->
		and exists (select 1 from usergroup where usergroup.personid = p.personid)
	  <CFELSEIF CompareNoCase(frmPersonType,"nonusers") IS 0>

		AND (p.Password = ''
		OR	p.Username = ''
		<!---  OR  p.PasswordDate < #CreateODBCDateTime(DateAdd("d",-application.pass ValidFor,Now()))# this ought to be part of the test, but it isn't being enforced so can't be used --->
		OR	p.LoginExpires <= #CreateODBCDateTime(Now())#)
	  </CFIF>

	ORDER BY p.LastName, p.FirstName, o.OrganisationName
</CFQUERY>

<!--- <CFQUERY NAME="getIndivForGrid" datasource="#application.sitedatasource#">
	SELECT DISTINCT p.PersonID,
			p.FirstName,
			p.LastName,
			p.FirstName+', '+p.LastName as fullName,
			p.Active,
			o.OrganisationName,
			c.CountryDescription,
			p.PasswordDate,
			p.loginExpires,
			(select max(loginDate) from usage where personID = p.personid) as lastLoginDate,
			p.userName,
			(select 1 from person where person.personid = p.personid
									AND	person.Password <> ''
									AND	person.Username <> ''
									-- LID 2484 GCC 2009-07-29 AND person.PasswordDate >= #CreateODBCDateTime(DateAdd("d",-application.Pass ValidFor,Now()))# <!--- this ought to be part of the test, but it isn't being enforced so can't be used --->
									AND	person.LoginExpires > #CreateODBCDateTime(Now())#
									AND person.active <>0) as validuser
		<CFIF IsDefined("frmPermission") or IsDefined("frmRights")>
			<CFIF frmPermission NEQ 0 OR frmRights NEQ 0>
			, r.Permission
			</CFIF>
		</CFIF>
	FROM Person AS p inner join Location AS l on p.LocationID = l.LocationID
		 INNER JOIN Organisation AS o on l.OrganisationID = o.OrganisationID
		 INNER JOIN Country AS c on l.CountryID = c.CountryID
	<CFIF IsDefined("frmRights")>
		<CFIF frmRights NEQ 0 OR frmPermission NEQ 0>
		, SecurityType AS s,
		RightsGroup AS rg,
		Rights as r
		</CFIF>
	</CFIF>
	WHERE l.CountryID IN (#Variables.CountryList#) <!--- from qrygetcountries.cfm --->
<!--- 	  AND p.Active = true show both but only allow active people to become users --->
	  <CFIF frmCountryID IS NOT 0>
	 	AND l.CountryID = #frmCountryID#
	  </CFIF>
	  <CFIF isDefined("frmOrgName") and Trim(frmOrgName) IS NOT "">
	 	AND o.organisationName like '#frmOrgName#%'
	  </CFIF>
	<CFIF Trim(frmNameText) IS NOT "">
		  <CFIF CompareNoCase(frmType,"First") IS 0>
		  	AND p.FirstName LIKE '#Trim(frmNameText)#'
		  <CFELSEIF CompareNoCase(frmType,"Last") IS 0>
		  	AND p.LastName LIKE '#Trim(frmNameText)#'
		  <CFELSE>
  		    AND {fn CONCAT({fn CONCAT(p.FirstName,' ')},p.LastName)} = '#frmNameText#'
<!--- 		    AND p.FirstName & ' ' & p.LastName = '#frmNameText#' --->
	<!--- 	  	AND ({fn CONCAT({fn CONCAT(p.FirstName,' ')},p.LastName)} LIKE '#Trim(frmNameText)#') --->
		  </CFIF>
	</CFIF>
<!--- 	  p.FirstName LIKE '#Trim(frmNameText)#'
   	OR p.LastName LIKE '#Trim(frmNameText)#' --->
	  <CFIF CompareNoCase(frmPersonType,"users") IS 0>
  		AND	p.Password <> ''
		AND	p.Username <> ''
 		-- LID 2484 GCC 2009-07-29 AND p.PasswordDate >= #CreateODBCDateTime(DateAdd("d",-application.pass ValidFor,Now()))# <!--- this ought to be part of the test, but it isn't being enforced so can't be used --->
		AND	p.LoginExpires > #CreateODBCDateTime(Now())#
		<!--- this line added by WAB - need to have a usergroup to be internal users, external users can have passwords --->
		and exists (select 1 from usergroup where usergroup.personid = p.personid)
	  <CFELSEIF CompareNoCase(frmPersonType,"nonusers") IS 0>

		AND (p.Password = ''
		OR	p.Username = ''
		-- LID 2484 GCC 2009-07-29 OR  p.PasswordDate < #CreateODBCDateTime(DateAdd("d",-application.pass ValidFor,Now()))# <!--- this ought to be part of the test, but it isn't being enforced so can't be used --->
		OR	p.LoginExpires <= #CreateODBCDateTime(Now())#)
	  </CFIF>
	<CFIF IsDefined("frmRights")>
	  <CFIF frmRights NEQ 0>
		  AND p.personID = rg.personID
		  AND rg.usergroupID = r.usergroupID
		  AND r.securitytypeID = s.securitytypeID
		  AND s.securitytypeID = #frmRights#
	  </CFIF>
	 </CFIF>
<CFIF IsDefined("frmPermission")>
	  <CFIF frmPermission NEQ 0>
		  AND p.personID = rg.personID
		  AND rg.usergroupID = r.usergroupID
		  AND r.securitytypeID = s.securitytypeID
		  AND r.Permission = #frmPermission#
	  </CFIF>
</CFIF>

	ORDER BY p.LastName, p.FirstName, o.OrganisationName
</CFQUERY> --->

<CFIF frmCountryID IS NOT 0>
	<CFQUERY NAME="getCountryName" datasource="#application.siteDataSource#">
		SELECT CountryDescription
		FROM Country
		WHERE CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>


<CFIF getIndiv.RecordCount GT application.showRecords>
	<!--- showrecords in application .cfm
			defines the number of records to display --->

	<!--- define the start record --->
 	<CFIF structKeyExists(FORM,"Forward.x")>
		 <CFSET displaystart = #frmStart# + #application.showRecords#>
 	<CFELSEIF structKeyExists(FORM,"Back.x")>
		 <CFSET displaystart = #frmStart# - #application.showRecords#>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = #frmStart#>
	</CFIF>
</CFIF>


<cf_head>
	<SCRIPT type="text/javascript">
<!--
	function doForm(person){
		var form = document.ThisForm;
		form.frmPersonID.value = person;
		form.sendPassword.value = "no";    // WAB added - deals with using back button to come back to this page
		form.submit();
	}

	/*
	function sendPassword(person){
		var form = document.ThisForm;
		form.frmPersonID.value = person;
		form.sendPassword.value = "yes";
		form.action = 'userList.cfm';
		form.submit();
	}*/

	function expireLogin(personID){
		var form = document.ThisForm;
		form.expirePersonID.value = personID;
		form.action = 'userList.cfm';
		form.submit();
	}

	function sendPassword(personID) {

		page = '/webservices/callWebService.cfc?'
		parameters = 'method=callWebService&webServiceName=user&methodName=sendUserPassword&personID='+personID+'&_cf_nodebug=true&returnFormat=json';

		var myAjax = new Ajax.Request(
			page,
			{
					method: 'post',
					parameters: parameters,
					evalJSON: 'force',
					debug : false,
					asynchronous:false
			}
		)
		jQuery('#messageText').html(message(myAjax.transport.responseText.evalJSON().MESSAGE));
	}

//-->
	</SCRIPT>

</cf_head>

<CFSET Current="userList.cfm">
<cfset application.com.request.setTophead(topHeadCfm="/relay/admin/userManagement/UserTopHead.cfm")>

<cf_includeJavascriptOnce template="/javascript/ui.js">

<div id="message">
<CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>
</div>

<TABLE WIDTH="80%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<TR>
	<TH>SEARCH RESULTS</TH>
</TR>
<TR>
	<TD ALIGN="CENTER" COLSPAN=2>
		<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
		<CFIF CompareNoCase(frmPersonType,"all") IS NOT 0>
			<CFSET AnyCriteria = "yes">
			<TR>
				<TD>
					Limit To:
				</TD>
				<TD>
					<CFIF CompareNoCase(frmPersonType,"users") IS 0>
						Current Users
					<CFELSEIF CompareNoCase(frmPersonType,"nonusers") IS 0>
						Non-users
					<CFELSE>
						<CFOUTPUT>#htmleditformat(frmPersonType)#</CFOUTPUT>
					</CFIF>
				</TD>
			</TR>
		</CFIF>
		<CFIF Trim(frmNameText) IS NOT "">
			<CFSET AnyCriteria = "yes">
			<TR>
				<TD>
					<CFIF CompareNoCase(frmType,"First") IS 0>
						First Name:
					<CFELSEIF CompareNoCase(frmType,"Last") IS 0>
						Last Name:
					<CFELSE>
						Full Name:
					</CFIF>
				</TD>
				<TD>
					<CFOUTPUT>#htmleditformat(frmNameText)#</CFOUTPUT>
				</TD>
			</TR>
		</CFIF>
		<CFIF Trim(frmCountryID) IS NOT 0>
			<CFSET AnyCriteria = "yes">
			<TR>
				<TD>
					Country:
				</TD>
				<TD>
					<CFOUTPUT>#Replace(ValueList(getCountryName.CountryDescription),",",", ","ALL")#</CFOUTPUT>
				</TD>
			</TR>
		</CFIF>
		<CFIF structKeyExists(form,"frmOrgName") and Trim(form.frmOrgName) IS NOT 0>
			<CFSET AnyCriteria = "yes">
			<TR>
				<TD>
					Organisation Name:
				</TD>
				<TD>
					<CFOUTPUT>#htmleditformat(form.frmOrgName)#</CFOUTPUT>
				</TD>
			</TR>
		</CFIF>
		<CFIF IsDefined("frmRights")>
			<CFIF Trim(frmRights) NEQ 0>
				<CFSET AnyCriteria = "yes">
			</CFIF>
		</CFIF>
		<CFIF anyCriteria IS NOT "yes">
			<!--- add a blank row to clean up HTML --->
			<TR>
				<TD>
					&nbsp;
				</TD>
			</TR>
		</CFIF>
		</TABLE>
	</TD>
</TR>
<CFIF anyCriteria IS "yes">
	<TR><TD COLSPAN="2" ALIGN="CENTER"><hr></TD></TR>
	<TR>
		<TD ALIGN="CENTER" COLSPAN=2>
		<CFOUTPUT>People Found: #getIndiv.RecordCount#</CFOUTPUT>
		</TD>
	</TR>
<CFELSE>
	<TR>
		<TD ALIGN="CENTER" COLSPAN=2>
				You did not provide any search criteria.<BR>Please try again.
		</TD>
	</TR>
	</TABLE>
	<CF_ABORT>
</CFIF>
</TABLE>

<P>

<CFIF getIndiv.RecordCount IS 0>
	<p>No individuals were found.</p>
<CFELSE>
<CFOUTPUT>
	<FORM NAME="ThisForm" METHOD="POST" ACTION="useredit.cfm">
	<INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="0">
	<CF_INPUT TYPE="HIDDEN" NAME="frmType" VALUE="#frmType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNameText" VALUE="#frmNameText#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonType" VALUE="#frmPersonType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgName" VALUE="#frmOrgName#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="userfind">
	<input type="hidden" name="sendPassword" value="no">
	<INPUT TYPE="HIDDEN" NAME="expirePersonID" VALUE="0">
</CFOUTPUT>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR class="evenRow">
		<TH VALIGN="BOTTOM">
			<B>Last Name, First Name</B>
		</TH>
		<TH VALIGN="BOTTOM">
			<B>User Name</B>
		</TH>
		<TH VALIGN="BOTTOM">
			<B>Organisation Name</B>
		</TH>
		<TH VALIGN="BOTTOM">
			<B>Country</B>
		</TH>
		<TH VALIGN="BOTTOM">
			<B>Login <BR>Expires</B>
		</TH>
		<TH VALIGN="BOTTOM">
			<B>Last <BR>Logged in</B>
		</TH>
		<!--- NJH 2009-06-16 LID 2366 --->
		<CFIF (IsDefined("frmPermission") and frmPermission NEQ 0) or (IsDefined("frmRights") and frmRights NEQ 0)>
			<!--- <CFIF frmPermission NEQ 0 OR frmRights NEQ 0> --->
				<TH VALIGN="BOTTOM">
				<B>Permission</B>
				</TH>
			<!--- </CFIF> --->
		</cfif>
		<TH ALIGN="CENTER" VALIGN="BOTTOM">
			<B>Edit <BR>Detail*</B>
		</TH>
		<TH ALIGN="CENTER" VALIGN="BOTTOM">
			<B>Send<BR>Password</B>
		</TH>
		<TH ALIGN="CENTER" VALIGN="BOTTOM">
			<B>Expire<BR>Login</B>
		</TH>

	</TR>

	<CFOUTPUT QUERY="getIndiv" STARTROW=#Variables.displaystart# MAXROWS=#application.showRecords#>
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD>
			#HTMLEditFormat(LastName)#, #HTMLEditFormat(FirstName)#
		</TD>
		<TD>
			#HTMLEditFormat(UserName)#
		</TD>
		<TD>
			#HTMLEditFormat(OrganisationName)#
		</TD>
		<TD>
			#htmleditformat(CountryDescription)#
		</TD>
		<TD>
			#dateFormat(LoginExpires,"dd-mmm-yy")# <cfif dateCompare(#LoginExpires#,#now()#) eq -1><strong style="color: Red;">Expired</strong></cfif>
		</TD>
		<TD>
			#dateFormat(lastLoginDate,"dd-mmm-yy")# #timeFormat(lastLoginDate,"HH:MM")#
		</TD>

		<!--- NJH 2009-06-16 LID 2366 --->
		<CFIF (IsDefined("frmPermission") and frmPermission NEQ 0) or (IsDefined("frmRights") and frmRights NEQ 0)>
			<!--- <CFIF frmPermission NEQ 0 OR frmRights NEQ 0> --->
				<TD>
				#ListGetAt(PermissionsList, Permission)#
				</TD>
			<!--- </CFIF> --->
		</cfif>

		<!--- only allow active people to become users --->
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CFIF YesNoFormat(Active) IS "Yes">
				<A HREF="JavaScript:doForm('#PersonID#');"><IMG SRC="/images/MISC/iconEditPencil.gif" ALT="Edit" BORDER=0></A>
			<CFELSE>
				&nbsp;
			</CFIF>
		</TD>

		<!--- only allow valid users to have password sent--->
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CFIF validUser is 1>
				<A HREF="JavaScript:sendPassword('#application.com.security.encryptVariableValue(name="personid", value=personId)#');"><IMG SRC="/images/MISC/iconMail.gif" ALT="Send Password" BORDER=0></A>
			<CFELSE>
				&nbsp;
			</CFIF>

		</TD>

				<!--- only allow valid users to have loginExpired --->
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CFIF validUser is 1 and dateCompare(#LoginExpires#,#now()#) eq 1>
				<A HREF="JavaScript:expireLogin('#PersonID#');"><IMG SRC="/images/MISC/iconDeleteCross.gif" ALT="Expire Login" BORDER=0></A>
			<CFELSE>
				&nbsp;
			</CFIF>

		</TD>

	</TR>
	</CFOUTPUT>
</TABLE>
</FORM>

<CFIF getIndiv.RecordCount GT application.showRecords>
	<!--- if we only show a subset, then give next/previous arrows --->
	<FORM ACTION="userlist.cfm" METHOD="POST">
	<cfoutput>
	<CF_INPUT TYPE="HIDDEN" NAME="frmType" VALUE="#frmType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNameText" VALUE="#frmNameText#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonType" VALUE="#frmPersonType#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgName" VALUE="#frmOrgName#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="userfind">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	<P></cfoutput>
	<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<TR>
		<TD>
			<CFIF Variables.displaystart GT 1>
				<INPUT TYPE="Image" NAME="Back" SRC="/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="">
			</CFIF>
		</TD>
		<TD align='center'>
	 		<CFIF (Variables.displaystart + application.showRecords - 1) LT getIndiv.RecordCount>
				<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
			<CFELSE>
	 			<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getIndiv.RecordCount#</cFOUTPUT>
			</CFIF>
		</TD>
		<TD ALIGN='RIGHT'>
			<CFIF (Variables.displaystart + application.showRecords - 1) LT getIndiv.RecordCount>
				<INPUT TYPE="Image" NAME="Forward" BORDER=0 ALT="Next" class="btn-primary btn">
			</cFIF>
		</TD>
	</TR>
	</TABLE>
	</FORM>
</CFIF>
<table width="80%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
<TR>
<TD>
	* Only active people can be users and their records edited.
</td>
</tr>
</CFIF>

</CENTER>


