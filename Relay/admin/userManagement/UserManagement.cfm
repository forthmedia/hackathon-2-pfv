<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Amendment History
2008/06/27      GCC     Moved to userTopHead.cfm as these functions are used  in usermanagement, usergroupmangement and flagmanagement
2009-10-13  NAS		LID - 2752 	Added code to remove 'Empty String' from list
2010/01/13	NJH		LID 2752	Re-wrote GetAllUsers query
2106-01-29 	WAB   	Remove twoSelects Combo and a manual select (and default frmuserGroupID to blank - easier to pass to cf_select than isdefined)
--->

<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>

<!--- 2009-11-02  NAS		LID - 2752 	Added code to increase timeout for Query "GetUsers" --->
<cfsetting requesttimeout="180" >

<CFSET Current="userManagement.cfm">
<cfparam name="frmuserGroupID" default = "">

<!---Delete a Group--->
<CFIF IsDefined("DeleteGroup")>
	<!---Delete the Rights for that Group--->
	<CFQUERY NAME="InsertUserGroup" datasource="#application.siteDataSource#">
		DELETE from Rights WHERE UserGroupID =  <cf_queryparam value="#DeleteGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<!---Delete the Users in that Group--->
	<CFQUERY NAME="InsertUserGroup" datasource="#application.siteDataSource#">
		DELETE from RightsGroup WHERE UserGroupID =  <cf_queryparam value="#DeleteGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<!---Delete the Group itself--->
	<CFQUERY NAME="InsertUserGroup" datasource="#application.siteDataSource#">
		DELETE from UserGroup WHERE UserGroupID =  <cf_queryparam value="#DeleteGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>


<!---If a particular Group ID is known--->
<CFIF frmUserGroupID is not "">

	<!---Get all available users (whoever has a UserGroupID created in the UserGroup table--->
	<!--- NJH 2010/01/13 LID 2752 replaced the query below with this one, as the in statement got very large..--->
	<CFQUERY NAME="GetAllUsers" datasource="#application.siteDataSource#">
		SELECT
			p.PersonID as dataValue
			, p.fullname AS displayValue
			, case when rg.userGroupID is null then 0 else 1 end as isSelected
		from
			Person as p with (noLock)
				inner join
			UserGroup as ug with (noLock) on p.PersonID = ug.PersonID
				left join
			rightsGroup rg with (noLock) on rg.personID = p.personID and rg.userGroupID =  <cf_queryparam value="#frmUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		where
			p.personid is not Null
			and isNull(p.fullname,'') != ''
			and p.loginExpires > getDate()
		ORDER BY displayValue
	</CFQUERY>
</CFIF>

<!---Group Combo--->
<CFQUERY NAME="GroupName" datasource="#application.siteDataSource#">
	Select UserGroupID as dataValue, Name as displayValue
	FROM UserGroup
	WHERE PersonID IS NULL
	ORDER BY Name
</CFQUERY>

<CFIF IsDefined("DeleteGroup")>
	<cfoutput>#application.com.relayUI.setMessage(message="The user group was deleted!",type="info")#</cfoutput>
</CFIF>



<FORM ACTION="UserManagement.cfm" METHOD="POST" name="myForm">
	<div class="form-group">
		<label for="frmUserGroupID">User Group:</label>
		<cf_select NAME="frmUserGroupID" id="frmUserGroupID" query = "#groupName#" onChange="window.document.myForm.submit()" nulltext = "Please select a User Group" showNull = #((frmUserGroupID is "")?1:0)# selected="#frmUserGroupID#">
	</div>
</FORM>

<CFIF frmUserGroupID is not "">
	<FORM ACTION="UserGroupManagement.cfm" METHOD="POST" NAME="form1" noValidate="true" >
		<cf_select name = "users" query = "#getAllUsers#" multiple = true keeporig=true>
		<INPUT TYPE="Hidden" NAME="EditUsers" VALUE="1">
		<CF_INPUT TYPE="Hidden" NAME="frmUserGroupID" VALUE="#frmUserGroupID#">
		<INPUT TYPE="Submit" NAME="Submit" VALUE="Save" class="btn btn-primary">
	</FORM>
</CFIF>