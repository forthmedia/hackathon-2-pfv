<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		TaskManagement.cfm
Author:			Alex
Date created:	2000

	Objective - This allows users to create and assign tasks to userGroups.  It is called 
				from UserGroupManagement.cfm which sets up the variables this applet
				requires.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2002-01-14			SWJ			Added these notes and fixed a bug in javaScript function securityname.
2001-11-20			SWJ			Changed it to the new look and feel.
2006-03-04			SWJ			Chnaged the styleing of the task select box
2008-06-13			NJH			P-CRL501 - Added help icon which opens new page showing the security type functions associated with a security type.
Enhancement still to do:



--->

<CFSET Current="userManagement.cfm">
<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>

<CFIF NOT IsDefined("frmUserGroupID")>
	<CFLOCATION URL="UserGroupManagement.cfm?UserManagement=1"addToken="false">
<CFELSE>

	<SCRIPT>
		
		function checkform2(){
			form = window.document.form2
			combo = form.AddTasks.options[form.AddTasks.selectedIndex].value
			if (combo == ""){
				alert("Please choose one or more tasks from the list to add")
			}
			else{
				form.submit()
			}
		}
		
		function checkform3(){
		form = window.document.form2
		//combo = form.AddTasks.options[form.AddTasks.selectedIndex].value
		//alert (form.AddTasks.options[form.AddTasks.selectedIndex].value)
		form.submit()
		
		}
		
		function securityname(){
			var winName = "SecurityTypeName.cfm?frmUserGroupID="+<CFOUTPUT>#jsStringFormat(frmUserGroupID)#</CFOUTPUT>;
			var subwindow = window.open(winName,"Name","height=200,width=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0"); 
			subwindow.focus();
		}
		
		
	</SCRIPT>
	
	
	<!--- sort out any missing permission definitions --->

<cfquery name="getMissingDefinitions" datasource="#application.siteDataSource#">
	select st.SecurityTypeID,shortName--,pd.permissionLevel,pd.PermissionName 
	from securityType st left outer join permissionDefinition pd
	on st.SecurityTypeID = pd.SecurityTypeID
	where pd.permissionLevel is null
</cfquery>
<cfset tasksMissingDefs = valueList(getMissingDefinitions.SecurityTypeID)>
<cfloop index="i" list="#tasksMissingDefs#">
	<cfquery name="getMissingDefinitions" datasource="#application.siteDataSource#">
		INSERT INTO [PermissionDefinition]([SecurityTypeID], [PermissionLevel], [PermissionName])
		VALUES(<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >, 1, 'view')
		INSERT INTO [PermissionDefinition]([SecurityTypeID], [PermissionLevel], [PermissionName])
		VALUES(<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >, 2, 'edit')
		INSERT INTO [PermissionDefinition]([SecurityTypeID], [PermissionLevel], [PermissionName])
		VALUES(<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >, 3, 'add')
	</cfquery>

</cfloop>
	
	<cfquery name="GetPerms" datasource="#application.SiteDataSource#">
		select DISTINCT SecurityType.ShortName, 
			SecurityType.SecurityTypeID, 
			Rights.Permission, 
			PermissionDefinition.permissionLevel,
			PermissionDefinition.PermissionName,
			case WHEN Rights.Permission & power (2,PermissionDefinition.permissionLevel-1) = 0 THEN 0 else 1 END as hasPermission,
			UserGroup.Name	
			FROM Rights inner join UserGroup on UserGroup.UserGroupID = Rights.UserGroupID
			inner join SecurityType on Rights.SecurityTypeID = SecurityType.SecurityTypeID
			left outer join PermissionDefinition on SecurityType.SecurityTypeID = PermissionDefinition.SecurityTypeID	
				WHERE 1=1
				AND UserGroup.UserGroupID =  <cf_queryparam value="#frmUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				ORDER BY SecurityType.ShortName , PermissionDefinition.permissionLevel</cfquery>
	
	<CFSET ListOfTasks = ValueList(GetPerms.SecurityTypeID)>
	
	
	
	<CFQUERY NAME="GetTasks" datasource="#application.SiteDataSource#">
		SELECT ug.Name, st.ShortName 
			FROM UserGroup as ug, SecurityType as st, Rights as ri
				WHERE ug.UserGroupID = ri.UserGroupID
				AND st.SecurityTypeID = ri.SecurityTypeID
				AND ri.UserGroupID =  <cf_queryparam value="#frmUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	
	
	<CFQUERY NAME="GetAllTaskActions" datasource="#application.SiteDataSource#">
		SELECT ShortName, SecurityTypeID 
		FROM SecurityType
		WHERE 1=1
		<CFIF ListOfTasks IS NOT "">
		AND SecurityTypeID  NOT IN ( <cf_queryparam value="#ListOfTasks#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>
		ORDER BY ShortName
			
	</CFQUERY> 
	
	<CFSET TaskCount = GetAllTaskActions.RecordCount>
	
	<!---Group Combo--->
	
	<CFQUERY NAME="GroupName" datasource="#application.SiteDataSource#">
		Select UserGroupID, Name
		FROM UserGroup
		WHERE PersonID IS NULL
		ORDER BY Name	
	</CFQUERY>

<table class="table table-striped admin-user-table">
<CFIF IsDefined("frmUserGroupID")>
	<FORM ACTION="TaskManagement.cfm" METHOD="POST" name="myForm">
	<TR>
		<TD class="usergroup-col">
			<h4>User Group:</h4> <br/>
			<select name="frmUserGroupID" onChange="window.document.myForm.submit()" class="form-control">
			<CFOUTPUT QUERY="GroupName">
				<OPTION VALUE=#UserGroupID# <CFIF IsDefined("frmUserGroupID")><CFIF UserGroupID EQ #frmUserGroupID#>SELECTED</CFIF></CFIF> >#htmleditformat(Name)#
			</CFOUTPUT>
			</SELECT>
		</TD>
		<td></td>
	</TR>
	</FORM>
</CFIF>

		<TR>
			<TD valign="top">
				<h4>Available Security Tasks:</h4>				
				<FORM ACTION="UserGroupManagement.cfm" METHOD="POST" NAME="form2">				
					<select name="AddTasks" size="15" class="form-control" multiple>
						<CFOUTPUT QUERY="GetAllTaskActions">
						<OPTION VALUE="#SecurityTypeID#">#htmleditformat(ShortName)#
						</CFOUTPUT>
					</SELECT>					
					
					<A class="btn btn-primary" HREF="javascript:securityname()"><B>Create New Security Task</B></A>
					<INPUT TYPE="BUTTON" VALUE=" &gt; " onClick="javascript:window.document.form2.submit()" class="btn btn-primary"><P>
					<INPUT TYPE="Hidden" NAME="TaskManagement" VALUE="1">
					<CFOUTPUT>
						<CF_INPUT TYPE="Hidden" NAME="frmUserGroupID" VALUE="#frmUserGroupID#">
					</CFOUTPUT>
				</FORM>
			</TD>
			
			<!-- <TD width=40>&nbsp;</TD> -->
				
			<!--- NJH 2008-06-13 P-CRL501 start --->
			<cfajaximport tags="cfwindow">

			<script type="text/javascript">
			
				function showSecurity(securityType,permissionLevel) {
					var currentTime = new Date();
					var ts = currentTime.getSeconds();
					ColdFusion.Window.create('SecurityTypeFunctionsWindow' + ts, 'Security Type Functions',
					'reportSecurityTypeFunctions.cfm?securityType=' + securityType + '&permissionLevel='+permissionLevel,
						{x:100,y:100,height:300,width:360,modal:false,closable:true,
						draggable:true,resizable:true,center:true,initshow:false,
						minheight:200,minwidth:300 });
					ColdFusion.Window.show('SecurityTypeFunctionsWindow' + ts);
				}
			
			</script>
			<!--- NJH 2008-06-13 P-CRL501 end --->
				
				
			<FORM ACTION="UserGroupManagement.cfm" METHOD="POST" NAME="form1">	
				<TD align="left" valign="top">
				
					<TABLE class="table table-striped">
					
						<CFIF GetPerms.RecordCount NEQ 0>
							<TR>
								<TD colspan="3"><h4>Assigned Security Tasks and Permissions:</h4></TD>
							</TR>
						</CFIF>
						
						<CFOUTPUT QUERY="GetPerms" group="ShortName">
							<TR>							
								<TD NOWRAP>
									<CF_INPUT TYPE="Hidden" NAME="SecurityTypeID" VALUE="#SecurityTypeID#">
									<B>#htmleditformat(ShortName)#</B>  
								</td>
								<td>
									<A HREF="UserGroupManagement.cfm?RemoveTask=#SecurityTypeID#&TaskManagement=1&frmUserGroupID=#frmUserGroupID#">Remove</A>
								</td>
								<td>
									<CFOUTPUT>Level#htmleditformat(PermissionLevel)# 
										<a href="javascript:showSecurity('#shortName#',#PermissionLevel#)"><img src="/images/icons/help.png"/></a>
										<CFIF evaluate(2 ^ (PermissionLevel-1)) NEQ 1>
											<CF_INPUT TYPE="Checkbox" NAME="frmPermission_#SecurityTypeID#" VALUE="#evaluate(2 ^ (PermissionLevel-1))#" CHECKED ="#iif( hasPermission IS NOT 0,true,false)#" class="checkbox">
										<CFELSE>
											<!-- <IMG SRC="/images/MISC/flag-yes.gif" WIDTH=15 HEIGHT=15 BORDER=0 ALT=""> -->
											<CF_INPUT TYPE="Checkbox" CHECKED ="CHECKED" class="checkbox" disabled="true">
											<CF_INPUT TYPE="Hidden" NAME="frmPermission_#SecurityTypeID#" VALUE="1">
										</CFIF>
									</CFOUTPUT>
								</td>	
							</TR>						
						</CFOUTPUT>
						<TR>
							<TD colspan="3"><A  class="btn btn-primary" HREF="javascript:window.document.form1.submit()"><B>Submit Permission Changes</B></A></TD>
						</TR>
					</TABLE>
				</TD>	
				
				<CFOUTPUT>
				<INPUT TYPE="Hidden" NAME="TaskManagement" VALUE="1">
				<CF_INPUT TYPE="Hidden" NAME="frmUserGroupID" VALUE="#frmUserGroupID#">
				</CFOUTPUT>
				
			</FORM>	
		</TR>
	</TABLE>



</CFIF>



