<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:commTask
		Permission: Add
---->
<!---
2015-11-22	SB Bootstrap
---->
<!--- the application .cfm checks for read dataTask privleges
--->
<!--- <cfinclude template="/templates/qry getcountries.cfm"> --->
<!--- qrygetcountries creates a variable CountryList --->

<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
	SELECT CountryID, CountryDescription
	FROM Country
   WHERE CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
   ORDER BY CountryDescription
</CFQUERY>

<!---
	NJH 2009-06-16 - LID 2366 changed rights for user groups
<cfquery name="getRights" datasource="#application.siteDataSource#">
	SELECT Shortname, SecuritytypeID
	FROM Securitytype
		ORDER BY SecuritytypeID
</cfquery> --->

<!--- NJH 2009-06-16 --->
<CFQUERY NAME="GroupName" datasource="#application.siteDataSource#">
	Select UserGroupID, Name
	FROM UserGroup
	WHERE PersonID IS NULL
	ORDER BY Name
</CFQUERY>

<CFPARAM NAME="qryNameText" DEFAULT="">
<CFPARAM NAME="qryOrgName" DEFAULT="">

<CFSET Current="userFind.cfm">
<!--- <CFINCLUDE TEMPLATE="UserTopHead.cfm"> --->
<CFPARAM NAME="message" TYPE="string" DEFAULT="">

<CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>
<FORM ACTION="userlist.cfm?mode=debug" METHOD=POST>
	<div class="form-group">
		<label for="frmPersonType">Limit to:</label>
		<SELECT NAME="frmPersonType" id="frmPersonType" class="form-control">
			<OPTION VALUE="all">
			<OPTION VALUE="users" selected> Current Users
			<OPTION VALUE="nonusers"> Non-Users
		</SELECT>
	</div>

	<div class="form-group">
		<label for="frmCountryID">Country:</label>
		<SELECT NAME="frmCountryID" id="frmCountryID" class="form-control">
		<OPTION VALUE="0">
		<CFOUTPUT QUERY="getCountries">
			<OPTION VALUE="#CountryID#"> #HTMLEditFormat(CountryDescription)#</CFOUTPUT>
		</SELECT>
	</div>

<!--- NJH 2009-06-16 - changed rights for user groups
		<label for="frmRights">Rights</label>
		<select name="frmRights">
			<option value="0"></option>
			<option value="All">All Rights</option>
			<cfloop QUERY="getRights">
				<option value="#SecuritytypeID#">#HTMLEditFormat(Shortname)#</option>
			</cfloop>
		</select> --->
	<cfoutput>
	<div class="form-group">
		<label for="frmUserGroupID">User Groups:</label>
		<select name="frmUserGroupID" id="frmUserGroupID" class="form-control">
			<option value="0"></option>
			<cfloop QUERY="GroupName">
				<option value="#UserGroupID#">#HTMLEditFormat(name)#</option>
			</cfloop>
		</select>
	</div>
	</cfoutput>

	<div class="form-group">
		<label for="frmPermission">User Permissions:</label>
		<SELECT NAME="frmPermission" id="frmPermission" class="form-control">
			<OPTION VALUE="0">
			<OPTION VALUE="1">View Only
			<OPTION VALUE="3">View/Edit
			<OPTION VALUE="7">View/Edit/Add
		</SELECT>
	</div>

	<div class="form-group">
			<label for="frmOrgName">Organization Name:</label>
			<CF_INPUT TYPE=TEXT NAME="frmOrgName" id="frmOrgName" VALUE="#qryOrgName#" class="form-control" SIZE="30" MAXLENGTH="30">
	</div>

	<div class="form-group">
		<label for="frmNameText">Person:</label>
		<CF_INPUT TYPE=TEXT NAME="frmNameText" id="frmNameText" VALUE="#qryNameText#" SIZE="30" MAXLENGTH="30" class="form-control">
	</div>

	<div class="form-group">
		<label class="radio-inline">
			<input type="radio" name="frmType" value="First" class="radio"> First name
		</label>


		<label class="radio-inline">
			<input type="radio" name="frmType" value="Last" checked class="radio"> Last name
		</label>

		<label class="radio-inline">
			<input type="radio" name="frmType" value="Full" class="radio"> Full name
		</label>
	</div>

	<cfoutput><INPUT TYPE="submit" NAME="btnSearch" ALT="Search" class="btn btn-primary"></cfoutput>
</FORM>