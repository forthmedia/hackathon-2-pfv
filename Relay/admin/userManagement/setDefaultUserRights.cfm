<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			setDefultUserRights.cfm	
Author:				SWJ
Date started:		2002-11-24
	
Description:		Use this to ensure that a defult set of user rights are set up
					for the site you are on.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancements still to do:

1.  Delete all existing rights for FNL users
2.  Assign all standard user Groups to FNL users (personid 1-5)
3.  Give FNL admin user rights to all countries

 --->



<cf_head>
	<cf_title>Set Default User Rights</cf_title>
</cf_head>


<h1>Check standard User Groups</h1>
<cfloop index="userGroupName" list="Standard User,Advanced User,Administrator,User Manager,Site Configurator,Content Editor,View Records">
	<cfquery name="getMissingUserGroups" datasource="#application.siteDataSource#">
		select usergroupID from userGroup where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<cfquery name="deleteUserGroupRights" datasource="#application.siteDataSource#">
		delete from rights 
		where userGroupID in (select usergroupID 
				from userGroup where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	
	<cfif getMissingUserGroups.recordCount eq 0>
		<cfquery name="insertUsers" datasource="#application.sitedatasource#">
			INSERT INTO [UserGroup](userGroupID, [Name], [PersonID], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated])
			select max(userGroupID)+1,<cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >, null, 100, getDate(), 100, getDate()
			from userGroup where userGroupID < 100
		</cfquery>
		<cfoutput>#htmleditformat(userGroupName)# added<br></cfoutput>
	<cfelse>
		<cfoutput>#htmleditformat(userGroupName)# already present ID = #htmleditformat(getMissingUserGroups.usergroupID)#<br></cfoutput>
	</cfif>
	<cfloop index="FNLUserID" list="1,3">
		<cfquery name="insertUsers" datasource="#application.sitedatasource#">
			INSERT INTO [RightsGroup]([PersonID], [UserGroupID], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated])
				select personid, (select usergroupID 
				from userGroup where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > ), 100, getdate(), 100, getdate() 
				from usergroup where personID =  <cf_queryparam value="#FNLUserID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and not exists (select personid from rightsGroup where personid =  <cf_queryparam value="#FNLUserID#" CFSQLTYPE="CF_SQL_INTEGER" >  
					and userGroupID = (select usergroupID 
				from userGroup where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
		</cfquery>
		<cfoutput>User with personID #htmleditformat(FNLUserID)# has rights to #htmleditformat(userGroupName)#<br></cfoutput>
	</cfloop>
</cfloop>


<h1>Set up correct securityTypes</h1>
<cfquery name="getCurrentSecurityTypes" datasource="#application.sitedatasource#">
select shortname from securityType order by shortname
</cfquery>
<cfset currentSecurityList=valuelist(getCurrentSecurityTypes.shortname)>
<cfoutput>#currentSecurityList#</cfoutput>
<cfset requiredSecurityTypeList="accountTask,actionTask,AdminTask,certifyTask,CommTask,DataTask,DownTask,ElementTask,evalTask,EventTask,FileTask,forecastTask,fundTask,HelpTask,incentiveTask,LeadManagerTask,LocatorTask,NewsletterTask,OrderTask,PasswordTask,PhoneTask,ProdEval,profileTask,QATask,RecordTask,ReportTask,ScreenTask,ServiceTask,SelectTask,TradeUpTask,trainingTask,translateTask,userTask">
<cfloop index="securityType" list="#requiredSecurityTypeList#">
	<cfquery name="getMissingSecurityTypes" datasource="#application.siteDataSource#">
		select securityTypeID from securityType where shortname =  <cf_queryparam value="#securityType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<cfif getMissingSecurityTypes.recordCount eq 0>
		<cfquery name="MissingSecurityType" datasource="#application.sitedatasource#">
			INSERT INTO [SecurityType]([ShortName], [Description], [CountrySpecific])
			VALUES (<cf_queryparam value="#securityType#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#securityType#" CFSQLTYPE="CF_SQL_VARCHAR" >, 0)
		</cfquery>
		<cfoutput>#htmleditformat(securityType)# added<br></cfoutput>
	<cfelse>
		<cfoutput>#htmleditformat(securityType)# already present ID = #htmleditformat(getMissingSecurityTypes.SecurityTypeID)#<br></cfoutput>
	</cfif>
</cfloop>


<h1>Assign standard tasks to User Groups</h1>
<cfset userGroupName="View Records">
<h2><cfoutput>#htmleditformat(userGroupName)# level 1</cfoutput></h2>
<cfloop index="secType" list="certifyTask,commTask,dataTask,eventTask,fileTask,forecastTask,fundTask,incentiveTask,leadManagerTask,locatorTask,orderTask,profileTask,phoneTask,qaTask,recordTask,reportTask,serviceTask,trainingTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,1, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 1<br></cfoutput>
</cfloop>


<cfset userGroupName="standard user">
<h2><cfoutput>#htmleditformat(userGroupName)# level 1</cfoutput></h2>
<cfloop index="secType" list="actionTask,certifyTask,eventTask,fileTask,forecastTask,fundTask,incentiveTask,leadManagerTask,locatorTask,orderTask,qaTask,recordTask,reportTask,serviceTask,trainingTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,1, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 1<br></cfoutput>
</cfloop>


<!--- <cfset userGroupName="standard user">
<h2><cfoutput>#userGroupName# level 3</cfoutput></h2>
<cfloop index="secType" list="actionTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,3, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#userGroupName# assigned #secType# level 3<br></cfoutput>
</cfloop> --->

<cfset userGroupName="standard user">
<h2><cfoutput>#htmleditformat(userGroupName)# level 7</cfoutput></h2>
<cfloop index="secType" list="commTask,selectTask,downTask,dataTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,7, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 7<br></cfoutput>
</cfloop>


<cfset userGroupName="advanced user">
<h2><cfoutput>#htmleditformat(userGroupName)# level 1</cfoutput></h2>
<cfloop index="secType" list="tradeupTask,evalTask,adminTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,1, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 1<br></cfoutput>
</cfloop>


<cfset userGroupName="advanced user">
<h2><cfoutput>#htmleditformat(userGroupName)# level 3</cfoutput></h2>
<cfloop index="secType" list="orderTask,certifyTask,fileTask,eventTask,incentiveTask,leadManagerTask,locatorTask,qaTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,3, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 3<br></cfoutput>
</cfloop>


<cfset userGroupName="advanced user">
<h2><cfoutput>#htmleditformat(userGroupName)# level 7</cfoutput></h2>
<cfloop index="secType" list="profileTask,eventTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,7, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 7<br></cfoutput>
</cfloop>


<cfset userGroupName="Content Editor">
<h2><cfoutput>#htmleditformat(userGroupName)# level 3</cfoutput></h2>
<cfloop index="secType" list="elementTask,translateTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,3, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 3<br></cfoutput>
</cfloop>


<cfset userGroupName="User Manager">
<h2><cfoutput>#htmleditformat(userGroupName)# level 1</cfoutput></h2>
<cfloop index="secType" list="userTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,1, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 7<br></cfoutput>
</cfloop>

<cfset userGroupName="Site Configurator">
<h2><cfoutput>#htmleditformat(userGroupName)# level 3</cfoutput></h2>
<cfloop index="secType" list="adminTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,3, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 3<br></cfoutput>
</cfloop>

<cfset userGroupName="Administrator">
<h2><cfoutput>#htmleditformat(userGroupName)# level 7</cfoutput></h2>
<cfloop index="secType" list="phoneTask,userTask,adminTask,commTask">
	<cfquery name="insertRights" datasource="#application.siteDatasource#">
	INSERT INTO [Rights]([UserGroupID], [SecurityTypeID], [CountryID], [MenuID], [Permission], [createdby], [created], [lastupdatedby], [LastUpdated])
	select ug.usergroupid , (select SecurityTypeID from securityType where shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > ),
	 0,0,7, 100,getdate(), 100, getdate() 
	 from usergroup ug 
	 where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 and not exists (select r.usergroupid 
	 	from rights r inner join usergroup ug on r.usergroupid=ug.usergroupid
		inner join securityType st on st.SecurityTypeID=r.SecurityTypeID
		where name =  <cf_queryparam value="#userGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >  and st.shortname =  <cf_queryparam value="#secType#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</cfquery>
	<cfoutput>#htmleditformat(userGroupName)# assigned #htmleditformat(secType)# level 7<br></cfoutput>
</cfloop>




