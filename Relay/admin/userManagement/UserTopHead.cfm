<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="userFind.cfm">

<cfoutput>
<!---Create a new group and delete a group scripts--->
<SCRIPT>
	function deleteGroup(){
		form = window.document.myForm
		
		list = form.frmUserGroupID.options[form.frmUserGroupID.selectedIndex].text
		group = form.frmUserGroupID.options[form.frmUserGroupID.selectedIndex].value
		
			if(group != ""){
				if(confirm("Please confirm that you want to delete the User Group"+" "+list)){
			
					window.location.href = "UserManagement.cfm?DeleteGroup="+escape(group)+"&GroupName="+escape(list)
			
				} 
			}
			
			else{
			alert("You must first select a group to delete")
			}
		}	
	
	
	function groupName(){
		var subwindow = window.open("/admin/userManagement/NewUserGroupName.cfm","NewGroup","height=200,width=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=0") ; 
		// NJH 2008/05 T10 Bug 7... not sure why the below code is there and it seems to be keeping the new window from loading the page (on all sites as well).
		/*subwindow.location.href="/admin/userManagement/NewUserGroupName.cfm"
		subwindow.location.reload()
		subwindow.focus();*/
	}
</SCRIPT>
</cfoutput>
<CF_RelayNavMenu pageTitle="User Management" thisDir="/admin/userManagement">

	<CFIF findNoCase("userManagement.cfm",SCRIPT_NAME) or findNoCase("UserGroupManagement.cfm",SCRIPT_NAME) or  findNoCase("flagManagement.cfm",SCRIPT_NAME) or  findNoCase("taskManagement.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="New User Group" CFTemplate="javascript:groupName()" target="mainSub">
		
		<CFIF IsDefined("frmUserGroupID")>
			<CF_RelayNavMenuItem MenuItemText="Assign Users" CFTemplate="UserGroupManagement.cfm?UserManagement=1&frmUserGroupID=#frmUserGroupID#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Assign Security Tasks" CFTemplate="UserGroupManagement.cfm?TaskManagement=1&frmUserGroupID=#frmUserGroupID#" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Assign Profiles to Groups" CFTemplate="FlagManagement.cfm?FlagManagement=1&frmUserGroupID=#frmUserGroupID#&frmFlagGroupID=137" target="mainSub">
			<CF_RelayNavMenuItem MenuItemText="Delete User Group" CFTemplate="javascript:deleteGroup()" target="mainSub">
		</cfif>
	</CFIF>
	<CFIF findNoCase("userList.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Search" CFTemplate="userFind.cfm" target="mainSub">
	</CFIF>
</CF_RelayNavMenu>

