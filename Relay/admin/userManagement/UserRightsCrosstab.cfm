<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2009/09/07 LID 2515 Removed #application.user filespath# , implemented getTemporaryFolderdetails()
	.	Don't think template used so not too worried about this
--->

<!--- This but needs to be derived from a query on security type table --->
<CFQUERY NAME="security" datasource="#application.siteDataSource#">
	Select * from securityType
</CFQUERY>

<CFSET tasks = valuelist(security.shortname)>
<cfparam name="frmCountryID" default="0">

<CFQUERY NAME="Permission" datasource="#application.siteDataSource#">
select
<CFLOOP index="thisTask" list="#tasks#">

(	SELECT 	max(permission)
	FROM Rights AS r, RightsGroup AS rg
	WHERE r.SecurityTypeID = (SELECT c.SecurityTypeID FROM SecurityType AS c
         					WHERE c.ShortName =  <cf_queryparam value="#thisTask#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	AND r.UserGroupID = rg.UserGroupID
	AND rg.PersonID = ug.personid) as #thistask#,
</cfloop>
name, countrydescription as region

from usergroup as ug, person AS p, location AS l , countrygroup as cg, country as c
where ug.personid is not null
AND p.locationID = l.locationID
AND ug.personID = p.personID
AND l.countryid = cg.countrymemberid
AND c.countryid = cg.countrygroupid

	<CFIF frmCountryID IS NOT 0>
		 	AND l.CountryID  IN ( <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>

	AND countrydescription not in ('Europe','APR')	<!--- these are "super regions" so tend to just cause duplication  --->
Order by countrydescription, name
</CFQUERY>


<CFPARAM name="DownloadLink" default="Yes">
<cfset TemporaryFolderdetails = application.com.filemanager.getTemporaryFolderDetails()>
<CFPARAM name="DownloadLocalPath" default="#TemporaryFolderdetails.path#userrights.xls">
<CFPARAM name="DownloadWebPath" default="#TemporaryFolderdetails.webpath#userrights.xls">

<CFSET downloadString="">
<CFSET delimiter=chr(9)>
<CFSET newline= chr(10) >

<CFIF DownloadLink is "Yes">
	<!---Delete previous file left by the same user--->
	<CFIF #FileExists(DownloadLocalPath)# IS "Yes">
		<CFFILE ACTION="DELETE" FILE="#DownloadLocalPath#">
	</CFIF>
</cfif>

	<A HREF="<CFOUTPUT>#htmleditformat(DownloadWebPath)#</CFOUTPUT>">Download this table</A>


<!--- This bit creates a single string which can be evaluated to output one line in a single go --->
<CFSET evalstring = '"<TD>##name##</td>'>
<CFSET downloadevalstring = '"##name##'>
<CFLOOP index="thisTask" list="#tasks#">
	<CFSET evalString = evalstring & "<TD>###thisTask###</td>">
	<CFSET downloadevalString = downloadevalstring & "##delimiter##" & "###thisTask###">
</CFLOOP>
<CFSET evalString = evalString & '"'>
<CFSET downloadevalString = downloadevalString & '"'>

<CFSET Current="UserRightsCrossTab.cfm">
<cfset application.com.request.setTopHead(topHeadcfm="userTopHead.cfm")>

<!--- this bit outputs a table --->
<TABLE BORDER="1">
<CFOUTPUT><TR><TD>Name</td><TD>#Replace(tasks,",","</TD><TD>","ALL")#</td></tr></cfoutput>
<CFSET downloadstring = downloadstring & "Name" & delimiter & #Replace(tasks,",",delimiter,"ALL")# & newline>

<CFOUTPUT query="permission" group= "region">
<TR><TD colspan= "#listlen(tasks)#">#htmleditformat(region)#</td></TR>
<CFSET downloadstring = downloadstring & region & newline>
<CFOUTPUT >
<TR>#evaluate("#evalstring#")#</tr>
<CFSET downloadstring = downloadstring & evaluate("#downloadevalstring#") & newline >
</cfoutput>
</cfoutput>
</table>

<CFIF DownloadLink is "Yes">

	<CFFILE ACTION="WRITE" FILE="#DownloadLocalPath#" OUTPUT="#downloadString#">

	<A HREF="<CFOUTPUT>#htmleditformat(DownloadWebPath)#</CFOUTPUT>">Download this table</A>
</CFIF>




