<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayTagManager.cfm
Author:				WAB
Date started:			2007-04-17

Description:
	Used to switch relayTags on and off from default state as set in the relaytagXML

	Having written this there was a suggestion that this should be done via the site specific relayTag.xml file - but anyhow here is this version


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfset application.com.request.setDocumentH1(text="Manage Relay Tags")>
	<cfif isDefined("updateTags")>
		<cfloop index="field" list="#fieldNames#">
			<cfif listFirst(field,"_") is "frmTag">
				<cfset value = evaluate(field)>
				<cfset tagName = listRest(field,"_")>
				<cfquery name="updateTag" datasource = "#application.sitedatasource#">
					<cfif value is "default">
					delete from relayTag where relayTag =  <cf_queryparam value="#tagname#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelse>
					if exists(select 1 from relayTag where relayTag =  <cf_queryparam value="#tagname#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					BEGIN
						update relayTag set Active =  <cf_queryparam value="#value#" CFSQLTYPE="cf_sql_float" >  where relayTag =  <cf_queryparam value="#tagname#" CFSQLTYPE="CF_SQL_VARCHAR" >
					END
						ELSE
					BEGIN
						insert into relayTag  (relayTag, Active) values (<cf_queryparam value="#tagname#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#value#" CFSQLTYPE="cf_sql_float" >)
					END

					</cfif>



				</cfquery>


			</cfif>
		</cfloop>
		<cfset application.com.applicationVariables.updateRelayTagStructure()>
	</cfif>


	<cfset tempQuery = application.com.structureFunctions.structToQuery(application.RelayTagKeyedByTagName)>
	<cfquery name="tagsSorted" dbtype="query">
		select (activeOverridden * 10 ) + active as sorting, * from tempQuery
		order by sorting desc, tag
	</cfquery>


	<cfquery name="tagUsage" datasource = "#application.sitedatasource#" cachedwithin="#createTimeSpan(1,0,0,0)#">
		<!--- rather a hack query to get list of tags in use.  Only finds one per phrase and not if after character 4000!
		a bit slow so cache for a long time
		--->
		select
		distinct
			replace(substring (
				phrasetext ,
				pos + 1,
				charindex(' ',phraseText,pos) -pos -1
				) , '>','') as tag
		from
		(	select 	phraseid, charindex('<relay_',phrasetext) as pos, convert(varchar(4000),phrasetext)+ ' ' as phrasetext
			from vphrases
			where convert(varchar(4000),phrasetext) like '%<relay__%'
		)	 as x
	</cfquery>
	<cfset tagList = valuelist(tagUsage.tag)>



	<form method="post">
	<table id="relayTagManagerTable" class="table table-hover table-striped">
	<tr><th>Tag</th><th>ModuleID</th><th>In Use?</th><th>Switch<BR>On</th><th>Switch<BR>Off</th><th>Return To<BR>Default</th></tr>
	<cfoutput query="tagsSorted" group="sorting">
		<cfif activeOverRidden is "1"><cfset title = "These tags have been specifically switched #iif(active,de("on"),de("off"))#"> <cfelse><cfset title = "These tags are #iif(active,de("on"),de("off"))# by default"></cfif>
		<TR>
			<td colspan="6"><h5>#htmleditformat(title)#</h5></td>
		</tr>



		<cfoutput>
		<TR>
			<td>#htmleditformat(tag)#</td>
			<td>#htmleditformat(moduleid)# <cfif ModuleNotActive is 1>*</cfif></td>
			<td><cfif listfindNoCase(tagList,tag)>In Use</cfif></td>
			<td><cfif not active><CF_INPUT type="radio" name="frmtag_#tag#" value="1"></cfif></td>
			<td><cfif active><CF_INPUT type="radio" name="frmtag_#tag#" value="0"></cfif></td>
			<td><cfif activeOverRidden is "1"><CF_INPUT type="radio" name="frmtag_#tag#" value="Default"></cfif></td>
		</TR>
		</cfoutput>

	</cfoutput>
	</table>
	<p>* Indicates a module which is not in use</p>

	<input Type="submit" name="updateTags" value = "Update" class="btn btn-primary">
	</form>





