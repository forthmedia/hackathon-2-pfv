<!--- 

WAB 2016-03-17	PROD2016-118  Housekeeping Improvements

\admin\housekeepingReport.cfm

				A rudimentary page for reporting on housekeeping tasks
				Also included by /scheduled/housekeeping.cfm

WAB 2016-10-17	PROD2016-2542 Problems when dbTime and CFTime are wildly different.  Use dbTime when calculating dayDiff

 --->
	<cfset houseKeepingObject = application.com.houseKeeping>

	<cfif isdefined("Name")>
		<!--- 	Run a specific Task 
				Note, this is a copy of code in \scheduled\housekeeping.cfm
		--->
		<cfif structKeyExists(url,"instanceID") and url.InstanceId is not application.instance.coldfusionInstanceId>
			<cfset result = houseKeepingObject.callHouseKeepingOnAnotherInstance(houseKeepingMethodName = name, force = true, coldfusionInstanceId = url.instanceid, timeout =30 )>
		<cfelse>
			<cfset result = houseKeepingObject.runHousekeeping(houseKeepingMethodName = name, force = true)>
		</cfif>
		
		<cfoutput>Task #htmlEditFormat(name)# result<br /></cfoutput>
		<cfdump var="#result.results[name]#">
	</cfif>	

	<cfquery name = "housekeeping">
	select * from vhousekeepingLog
	where 
		FilterOnThisColumnToRemoveDuplicateRunOnEachDatabaseItems=1 
		 and (
			runoneach='database' 
			or 
			coldfusionInstanceId = #application.instance.coldfusioninstanceid# 
			or 
			coldfusionInstanceId is null
			or
			isOK = 0
			or
			(runoneach='instance' and nextRunTime < dateAdd(n,-5,getdate()))
			
			)

		and methodName in (<cf_queryparam value = "#houseKeepingObject.getFunctionNamesList()#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
	order by lastRunTime desc
	</cfquery>
	
	<cfset isClustered = application.com.clustermanagement.isThisSiteClustered ()>
	<cfset dbTime = application.com.dateFunctions.getDbTime()>
	
	<cfset application.com.request.setDocumentH1 (text = "Housekeeping Report")>
	<cfoutput>
		

	<table class="withBorder table table-hover table-striped table-bordered table-condensed" data-role="table">
		<thead>
			<th>Name</th>
			<cfif isClustered>
				<th>Runs on Each</th>
				<th>Instance</th>
			</cfif>
			<th>Last Run</th>
			<th>Next Run Due</th>
			<th>Status</th>
			<th>Time Taken (s)</th>
			<th>Result (If Failed)</th>
		</thead>
		<tbody>
		<cfloop query="housekeeping">
			<cfif isJSON (metadata)>
				<cfset metaDataStruct = deserializeJson(metadata)>
			<cfelse>
				<cfset metaDataStruct = {}>
			</cfif>
			<TR>
				<td><a href="?name=#methodname#<cfif runOnEach is "instance" and coldfusionInstanceId is not application.instance.coldfusionInstanceId>&instanceID=#coldfusionInstanceId#</cfif>">#methodname#</a></td>
				<cfif isClustered>
					<td>#runOneach#</td>
					<td>#instance#</td>
				</cfif>			
				<td><cfif isdate(lastRunTime)>#application.com.datefunctions.DayDiffInWords(lastRunTime, dbTime, true)#<cfelse>Not Run Yet</cfif></td>
				<td><cfif isdate(nextRunTime)>#application.com.datefunctions.DayDiffInWords(nextRunTime, dbTime, true)#</cfif></td>
				<td><cfif locked>
						Running
					<cfelse>
						#isOK?"OK":"Failed"#
					</cfif>
				</td>
				<td><cfif structKeyExists (metaDataStruct,"elapsedTimeSeconds")>#metaDataStruct.elapsedTimeSeconds#</cfif></td>
				<td>
					<cfif not isOK and isJSON (metadata)>
						<cfdump var="#metaDataStruct#">
						<cfif structKeyExists (metaDataStruct,"errorid") and request.relaycurrentUser.errors.showLink>
							<a href="#application.com.errorhandler.getURLOfErrorDetailsFile()#?errorID=#metaDataStruct.errorid#" target="errorWindow">Show Error</a>
						</cfif>
					</cfif>
				</td>
			</TR>
		</cfloop>
		</tbody>
	</table>

	<cfif isClustered>
	This report shows <br />
		<ul>
			<li>Latest results for database wide tasks</li>
			<li>Results of Instance tasks on this instance</li>
			<li>Any tasks which have errored</li>
			<li>Any instance tasks which are more than 5 minutes overdue</li>
		</ul>
	</cfif>	

	</cfoutput>