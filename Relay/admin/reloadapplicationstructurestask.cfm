<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reloadApplicationStrcuturesTask.cfm	
Author:				WAB
Date started:			2009/06/25
	
Description:			
Included by reloadApplicationStructure.cfm and called in its own right
See comments in that file


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010/01/11  Changes to feedback received from cluster

Possible enhancements:


 --->

 
  	<cfset applicationVariablesObj = createObject("component","relay.com.applicationVariables")>

	

 	<cfif structure is not "" and structKeyExists (applicationVariablesObj,"reload_#structure#")>

		<cfparam name = "updateCluster" default = "false">  <!--- this is a checkbox so false if not defined --->
		<cfparam name = "waitForClusterResponse" default = "false">

	
		<cftry>
			<cfset result = applicationVariablesObj.doReload(item=structure, waitForClusterResponse = waitForClusterResponse, updateCluster = updateCluster)> 

			<cfcatch>
				<cfset result = structNew()>
				<cfset result.isOK = false>
				<cfset result.message = "Failed: #cfcatch.message#">
			</cfcatch>	
		</cftry>

		<!--- some older functions don't necessarily bring back a nice structure with an isOK, some bring back nothing, some bring back true/false 
			this now dealt with in the doreload function
		--->	
		
			<cfif not result.isOK>
				<cfset class = "errorblock">
			<cfelse>
				<cfset class = "infoblock">
			</cfif>

			<cfoutput>

			<cf_divopenclose divid="feedbackdiv"><cf_divopenclose_image>Feedback</cf_divopenclose_image> </cf_divopenclose>
			<div class="#class#" id="feedbackdiv">
				<table>
					<tr><td valign="top"><cfif structKeyExists (result.functionMetaData,"description")>#application.com.security.sanitiseHTML(result.functionMetaData.description)#<cfelse>#application.com.security.sanitiseHTML(item)#  </cfif></td>
					<tr><td valign="top">This Instance</td>
					<td>
				<cfif structKeyExists (result,"message") and result.message is not "Function Called">			
					#application.com.security.sanitiseHTML(result.message)#					
				<cfelseif result.isOK>
					OK
				<cfelse>
					Failed	
				</cfif>
					</td>
						</tr>
				<cfif updateCluster and structKeyExists (result,"clusterResult")>

					<cfloop item="instance" collection = #result.clusterResult#>
						<cfset instanceResult = result.clusterResult[instance]>
						<tr>
							<td valign="top">#htmleditformat(instance)#</td>
							<td>
									<cfif not instanceResult.isOK><font color = "red"></cfif>
									<cfif structKeyexists(instanceResult,"Message")>
										#application.com.security.sanitiseHTML(instanceResult.message)#
									<cfelseif instanceResult.isOK>
										OK
									</cfif> 
							</td>
						</tr>					
					</cfloop>
				</cfif>
					</table>
					


			</div>
			</cfoutput>
		
	</cfif>

