<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB Complex Flag grid

Outputs a grid of data stored in a WDDX structure  (or possibly in a custom table)

2009/02  In passing corrected an error which occurred in view mode
2013-04-15	WAB	P-MIC002	Added view mode to valid values

 --->

				<cfparam name = "attributes.flagDataQuery" >
				<cfparam name = "attributes.flagID" >
				<cfparam name = "attributes.entityID" >

				<cfset flagDataQuery = attributes.flagDataQuery>
				<cfset flagid = attributes.flagID>
				<cfset entityID = attributes.entityID>

				<cfif not isDefined("attributes.columnList")>
					<cfparam name = "attributes.columnList" default = "#flagDataQuery.columnlist#">
					<cfloop index="exclude" list="identity_,arrayIndex">
						<cfif listfindNocase(attributes.columnList,exclude)>
							<cfset attributes.columnList = listdeleteat(attributes.columnList,listfindNocase(attributes.columnList,exclude))>
						</cfif>
					</cfloop>
					<!--- could exclude various columns here --->
				</cfif>

				<cfparam name = "attributes.columnheaderList" default = "#attributes.columnList#">

				<cfparam name = "attributes.fieldSizeList" default = "#repeatstring("50,",listlen(attributes.columnList))#">
				<cfparam name = "attributes.GRIDVALIDVALUES" default = "">

				<cfparam name = "attributes.method" default = "view">
				<cfparam name = "attributes.addOnly" default = "false">
				<cfparam name = "attributes.addRowText" default = "Add Row">

				<cfparam name = "attributes.showDelete" default = "true">
				<cfparam name = "attributes.showAddRow" default = "true">

				<cfparam name = "attributes.showRowNumber" default = "true">

				<cfparam name = "attributes.numberBlankRows" default = "1">

				<cfparam name = "attributes.showBlankRow" default = "true">

				<cfparam name = "attributes.maxRows" default = "100">   <!--- not implemented --->

				<cfparam name = "attributes.required" default = "0">
				<cfparam name = "attributes.gridRowCheckFunction" default = "">
				<cfparam name = "attributes.gridRowCheckMessage" default = "">



<!---
form field names end up as:

flag_#flagID#_#Identity_#_#thisField#_#entityID#

 --->

				<cfset gridDef = arrayNew(1)>
				<cfset blankCol = structNew()>

				<cfset columnArray = listtoArray (lcase(attributes.columnList))>
				<cfset fieldSizeArray = listtoArray (attributes.fieldSizeList)>
				<cfset headingArray = listtoArray (attributes.columnHeaderList)>

				<cfloop from="1" index="I" to=#arrayLen(columnArray)#>
					<cfset newCol = duplicate(blankCol)>
					<cfset newCol.name = columnArray[I]>
					<cfset newCol.size = fieldSizeArray[I]>
					<cfset newCol.header = headingArray[I]>

					<cfif listfindNoCase(attributes.GRIDVALIDVALUES,newCol.name)>
						<cfset newCol.hasValidValues = true>
						<cfset newCol.validFieldName = 'flag.#attributes.flagID#.#newCol.name#'>
						<cf_getValidValues validFieldName = #newCol.validFieldName# showNull=false>
						<cfset newCol.ValidValueQuery = validvalues>
					<cfelse>
						<cfset newCol.hasValidValues = false>
					</cfif>
					<cfset griddef[i] = newCol>
				</cfloop>

				<cfset tableID = "table_#flagID#_#entityID#">

				<!--- create an array/structure to define a blank row new row, will be outputted as JS , also used to output an initial blank row --->
				<cfset blankrow = "">
				<cfset rowStruct = ArrayNew(1)>
				<cfset ColOffset = 0>
				<cfif attributes.showRowNumber>
					<cfset rowStruct[1] = structNew()>
					<cfset rowStruct[1].content = "ROWNUMBER">
					<cfset blankROw = blankROw & "<td class=flaggrid>#rowStruct[1].content#</td>">
					<cfset ColOffset = 1>
				</cfif>
				<cfloop from="1" index="I" to=#arrayLen(gridDef)#>
					<cfset thisField = gridDef[I].name>
					<cfset thisFieldSize = gridDef[I].size>
					<cfset fieldName = "flag_#flagID#_INDEX_#thisField#_#entityID#">
					<cfset rowStruct[ColOffset+i] = structNew()>
					<cfif not gridDef[I].hasValidValues>
						<cfset rowStruct[ColOffset+i].content = '<input id = "#fieldName#" class=flaggrid name="#fieldName#" Type=text value="" size=#thisFieldSize#><input name="#fieldName#_orig" Type=hidden value="" >'>
					<cfelse>
						<cfsavecontent variable = "validvalueCode_html">
							<cf_displayValidValues formfieldname = #fieldname# validvalues = #gridDef[I].validvaluequery#>
						</cfsavecontent>
						<cfset rowStruct[ColOffset+i].content = validvalueCode_html>
					</cfif>

					<cfset blankROw = blankROw & "<td class=flaggrid>#rowStruct[ColOffset+i].content#</td>">
				</cfloop>
						<cfif attributes.showDelete and attributes.method is "edit" and not attributes.addOnly>
							<cfset blankROw = blankROw & "<td class=flaggrid>&nbsp;</td>">
						</cfif>

				<cf_includejavascriptonce template = "/flags/js/complexFlagGrid.js">

<cfoutput>
				<table id="#tableID#"  cellspacing=0 cellpadding=2 class="flaggrid">
					<tbody>
					<tr class=flaggrid>
						<cfif attributes.showRowNumber>
						<th class=flaggrid>&nbsp;</th>
						</cfif>
						<cfset fieldList = "">
						<cfloop from="1" index="I" to=#arrayLen(gridDef)#>
						<cfset fieldList = listAppend(fieldList,gridDef[I].name)>
						<th class=flaggrid>#gridDef[I].header#</td>
						</cfloop>
						<cfif attributes.showDelete and attributes.method is "edit" and not attributes.addOnly>
						<th class=flaggrid>phr_sys_Delete</td>
						</cfif>
					</tr>

					<CF_INPUT type="hidden" id="#tableID#_fieldList" name="#tableID#_fieldList" value = "#fieldList#">
					<CF_INPUT type="hidden" id="#tableID#_fieldPattern" name="#tableID#_fieldPattern" value = "flag_#flagID#_*ROWINDEX*_*FIELD*_#entityID#">


				<cfloop query="FlagDataQuery">

					<tr id ="#tableID#_row_#Identity_#"  class=flaggrid>

						<cfif attributes.showRowNumber>
						<td class=flaggrid>#htmleditformat(currentRow)#</td>
						</cfif>

						<cfloop from="1" index="I" to=#arrayLen(gridDef)#>
							<cfset thisField = gridDef[I].name>
							<cfset thisFieldSize = gridDef[I].size>
							<cfset fieldName = "flag_#flagID#_#Identity_#_#thisField#_#entityID#">
							<cfset currentvalue = FlagDataQuery[thisField][currentrow]>
							<td class=flaggrid>
								<cfif not gridDef[I].hasValidValues>
									<CF_INPUT class=flaggrid id = "#fieldName#" name="#fieldName#" Type=text value="#currentvalue#" disabled="#iif( attributes.method is "view" or attributes.addOnly,true,false)#" size=#thisFieldSize#>
									<CF_INPUT name="#fieldName#_orig" Type=hidden value="#currentvalue#">
								<cfelse>
									<cf_displayValidValues formfieldname = #fieldname# validvalues = #gridDef[I].validvaluequery# currentvalue = "#currentValue#" method="#attributes.method#">
								</cfif>

							</td>
						</cfloop>


							<cfif attributes.showDelete and attributes.method is "edit" and not attributes.addOnly>
							<td class=flaggrid>
								<CF_INPUT class=flaggrid id ="flag_#flagID#_#Identity_#_deleteitem_#entityID#" name="flag_#flagID#_#Identity_#_deleteitem_#entityID#" Type=checkbox value="1">
								<CF_INPUT name="flag_#flagID#_#Identity_#_deleteitem_#entityID#_orig" Type=hidden >
							</td>
						</cfif>
					</tr>

				</cfloop>


		</tbody>
				</table>

				<script>
					<cfwddx action="cfml2js" input = "#rowStruct#" topLevelVariable = "newRow#tableID#">
					<cfif (flagDataQuery.recordcount is 0 or attributes.numberblankRows) and attributes.method is not "view"><!--- slight problem here because blank wddx structure ends up with 2 rows ---><!--- WAB 2009/02/08 don't show blank rows if in view mode --->
						<cfset blankRows = max(1,min(attributes.numberblankRows,99))>
						addRow('#jsStringFormat(tableID)#',#jsStringFormat(blankRows)#);
					</cfif>
					</script>

					<cfif attributes.showAddRow and attributes.method is "edit">
						<A href="javascript:addRow('#tableID#',1)">#htmleditformat(attributes.addRowText)#</A>
					</cfif>




</cfoutput>


<cfset caller.complexFlagGrid = structNew()>
<cfset caller.complexFlagGrid.javascriptVerification = "">

<cfif attributes.required and attributes.gridRowCheckFunction is "">
	<!--- if this item is required, but no function has been defined for checking a row, then we will just have to say that any row with something in it is good enough --->
	<cfset attributes.gridRowCheckFunction = " true ">

	<!--- alternatively we could say that all columns need to be filled in
		this does mean that every row has to be filled like this
	--->
	<cfloop from="1" index="I" to=#arrayLen(gridDef)#>
		<cfset thisField = gridDef[I].name>
		<cfset attributes.gridRowCheckFunction = attributes.gridRowCheckFunction & " && somethingEntered('#thisField#') ">
	</cfloop>


</cfif>

<cfif attributes.method is not "view"><!--- WAB 2009/02/08 noticed that this code failed in view mode because  attributes.requiredMessage was not defined in view mode--->
	<cfif attributes.gridRowCheckFunction is not "">
		<cfif attributes.gridRowCheckMessage is ""> <!--- if there is no message to show if the row isn't filled in correctly, then we need to take the next best thing --->
			<cfset attributes.gridRowCheckMessage = attributes.requiredMessage & "  Row(s): ">
		</cfif>
		<cfset js = "verifyGridMsg('#tableID#','#replacenocase(attributes.gridRowCheckFunction,"'","\'","ALL")#','#attributes.gridRowCheckMessage#',#attributes.required#,'#attributes.requiredMessage#');">
		<cfset caller.complexFlagGrid.javascriptVerification  = "msg = msg + " & js  & "/* Flag_#flagID#_#entityid# */">
	</cfif>
</cfif>
<!--- <cfoutput><A href="javascript: #js#">Test Verification</A></cfoutput> --->




