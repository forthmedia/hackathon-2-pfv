<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Queries for Setting and Unsetting Flags--->
<!--- Required Parameters
		List of entity ids  frmEntityID - Can be in the form of a select Statement
		List of FlagIds, thisFlagID  or List of FlagTextIDs  frmFlagTextID
		List of Values for each flag frmFlagValue
		optional parameters: frmruninpopup  true or false
							: frmshowfeedback (shown if parameter is defined)
	--->

<!--- amendments 
		WAB 2000-11-29 Altered check rights to use a View (which uses rightsgroups)
		WAB 2010/09/09 removed some queries and replaced with calls to flagStructure
--->

	
<!--- <CFOUTPUT>Debug: <CFIF isDefined("frmFlagID")>#frmflagid#, </CFIF><CFIF isDefined("frmFlagtextID")>#frmflagtextid#, </CFIF>#frmflagvalue#</CFOUTPUT> --->
<CFPARAM name="frmflagright" default="">			<!--- this can be set to "SystemTask" to over write standard rights (ie on special screens) --->
<CFPARAM name="frmruninpopup" default=false>

<cfif isDefined("message")>
	<cfoutput>
		#application.com.relayui.message(message=message,type='success')#
	</cfoutput>
</cfif>
	
<!--- Check if there is list of flag IDs --->	
<CFIF NOT isDefined("frmFlagID")>

	<CFIF NOT isDefined("frmFlagTextID")>
		No FlagIDs Set
	    <CF_ABORT>
	<CFELSE>
	<!--- Convert FlagTextIDs to FlagIDs --->	
	
		
		<CFQUERY NAME="getFlagIDs" datasource="#application.siteDataSource#">
		Select flagID
		From Flag
		Where FlagTextID  in ( <cf_queryparam value="#frmFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</CFQUERY>
	
		<CFSET thisflagID=valuelist(getflagids.flagid)>

	</CFIF>
<CFELSE>

		<CFSET thisflagID=frmFlagID>

</CFIF>

		
<!--- check list of flag ids is same length as value list --->
<CFIF listlen(thisFlagID) IS NOT listlen(frmFlagValue)>
	Flag ids and values do not match	
 	<CF_ABORT>
</CFIF>

<CFSET Freezedate=now()>

<CFLOOP Index="I" FROM="1" TO="#listlen(thisFlagID)#">
	<CFSET flagID = listgetat(thisFlagID,I)>
	<CFSET flagValue = listgetat(frmflagValue,I)>

	<cfset flagStructure = application.com.flag.getFlagStructure(flagid)>
	<!--- 
		<!--- Work out flag details --->
			<CFQUERY NAME="getFlag" datasource="#application.siteDataSource#">
			Select fg.FlagTypeID, fg.EntityTypeID, f.name, fg.flagGroupID,
				flagType.Name as typename,flagType.dataTable as typedata
			from flaggroup as fg 
				inner join flag as f on fg.flaggroupid=f.flaggroupid
				inner join FlagType ft on fg.FlagTypeID = ft.FlagTypeID 
				inner join FlagEntityType fet on fg.EntityTypeID = ft.FlagTypeID 
			Where 
			f.flagid IN(#flagid#)
			
			</CFQUERY>
	 --->

	<!--- 
	NJH 2010/08/09 RW8.3 - replace below with data in flag structures
	<CFSET typeName = #ListGetAt(application.typeList,GetFlag.FlagTypeID)#>
	<CFSET typeData = #ListGetAt(application.typeDataList,GetFlag.FlagTypeID)#> 
	<CFSET entityTable = #ListGetAt(application.typeEntityTable,GetFlag.entityTypeID+1)#> <!--- NJH 2010/08/05 RW8.3 - scoped typeEntitytable --->
	--->
	
	<cfset typeName = flagStructure.flagType.name>
	<cfset typeData = flagStructure.flagType.datatable>
	<cfset entityTable = flagStructure.entityType.tablename>


	<!--- Does user have edit rights?
	must have rights to flaggroup and any parent flaggroup
	--->
	<CFSET flagMethod = "edit">


	<!--- 
		<CFQUERY NAME="getFlagGroup" datasource="#application.siteDataSource#">
		SELECT fg.flaggroupid as flaggroupid, fg.entitytypeid
		from flaggroup as fg, flag as f
		where f.flagid = #flagid#
		and fg.flaggroupid=f.flaggroupid
		</CFQUERY>
	 --->
	<CFSET flaggroupid=flagStructure.flaggroupid>


	<!--- get entity names for feedback --->
	<CFQUERY NAME="getEntities" datasource="#application.siteDataSource#">	
		Select
		<CFIF #entitytable# IS "person">
			Firstname +' '+Lastname as entityname 
		<CFELSEIF #entitytable# IS "Location">
			Sitename as entityname
		<CFELSEIF #entitytable# IS "organisation">
			organisationName as entityname
		</cfif>
		from #entitytable#
		where #entitytable#id  in ( <cf_queryparam value="#frmentityid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
		

	<!--- doesn't do parent 	group access rights --->
	<CFQUERY NAME="getFlagRights" datasource="#application.siteDataSource#">
		SELECT 1
		from flaggroup as fg
		where fg.flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND (	(fg.#flagMethod#AccessRights = 0 AND fg.#flagMethod# <> 0)
				OR
				(fg.#flagMethod#AccessRights <> 0	AND EXISTS (SELECT 1 
																	FROM vFlagGroupRights as fgr
																	WHERE fg.FlagGroupID = fgr.FlagGroupID
																	and fgr.personid = #request.relayCurrentUser.personid#
																	AND fgr.#flagMethod# <> 0)
				OR '#frmFlagRight#' = 'SystemTask'
				)
			)
	</CFQUERY>		

	<CFIF getflagrights.recordcount IS NOT 0>		

		<CFIF typeName IS "checkbox">

			<CFIF flagvalue is 1>   <!--- set flag --->
				<CFQUERY NAME="AddFlags" datasource="#application.sitedatasource#">
					INSERT INTO #typedata#FlagData
					(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
			  		SELECT 
			  		#entitytable#ID, 
			  		<cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
			  		<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
			  		<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
			  		<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
			  		<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					FROM #entitytable#
					WHERE #entitytable#ID  IN ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND #entitytable#ID NOT IN (SELECT entityid from #typedata#flagdata where flagid =  <cf_queryparam value="#flagid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</CFQUERY>
			<CFELSEIF flagvalue is 0>    <!--- unset flag --->
				<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
					Delete From #typedata#FlagData
					WHERE entityid  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND flagid =  <cf_queryparam value="#flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			<CFELSE>
				invalid value
			</CFIF>

		<CFELSEIF typeName IS "Radio">		
		
		
		<CFELSEIF typeName IS "">		
		
		<CFELSE>		
		
		</CFIF>


	<CFELSE>
		<CFOUTPUT>You do not have rights to #flagmethod# this flag (#flagID#)</CFOUTPUT>
	</CFIF>


	<CFIF isdefined("frmshowfeedback")>
		<CENTER>
		<CFOUTPUT>
		<B>Update Flag Value</b><P>
		Flag: <B>#flagStructure.name# </B> 
		<CFIF typeName IS "checkbox" or typeName IS "radio">
			#iif(flagvalue is 0, DE("Unchecked"),DE("Checked"))#
		<CFELSE>
			set to #flagvalue# 
		</CFIF>
		<BR>for the following entities: <BR>
		<P></P>
		#replace(valuelist(getentities.entityname),",","<BR>","ALL")#	
		</cfoutput>
		
		<cfif frmruninpopup IS "True">
		 <P><A HREF="JavaScript: window.close();">Close window</A>
		</CFIF>

		</CENTER>		
	</cfif>

	


</CFLOOP>


		
