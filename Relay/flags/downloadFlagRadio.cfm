<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type 3 = Radio --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
<!--- 2006/06/09 - GCC - leading space stops values such as 1-5 being interpretted as dates by excel 
may need a more intelligent choice as to whether we add the leading space but it will do for now
--->
SELECT
	' ' + F.Name name
FROM Flag F
LEFT JOIN BooleanFlagData BFD ON BFD.FlagID = F.FlagID AND BFD.EntityID = <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
WHERE F.FlagGroupID = <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.name, delim)>
</CFIF>
