<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	Edit Flags of type 2 = Checkbox
	WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
	WAB 2008/04/10  code to allow setting of a checkbox to make other fields required
	WAB 2008/06/02  modified above so that works with select box display
	NYB 2008/08/09  P-SOP003 - added ability to SortOrder different options via orderType.  Accepts: SortOrder, Alpha or Random
	WAB 2008/11/12  Bug 1327
	WAB 2009/02/03  problem with iif statement in requiresValueInOtherFlag code
	IH	2013/01/09	Add proper labels to checkboxes
	2014-01-09 NYB Case 436910 added includeLabelWrapper option to take out the label wrapper
	2014-11-25 AHL Case 442748 Add a SelectAll functionality
	2015-01-14 WAB Case 442748 Modified so that showSelectAll is a boolean and added selectAllText with a default
	2015/09/08 NJH 	Jira PROD2015-30 - enable flags/groups to be displayed/hidden based on flag being selected. Changed 'requiresValueInOtherFlag' to support a list of flagIds
	2015/09/24 NJH	Jira PROD2015-84 - provide support to make boolean groups required as well.
	2015-09-30 WAB 	Add an ID to the SELECT field
	2015-10-13 WAB 	PROD2015-85 Alter <LABEL> tags so do not have nested INPUT, add FOR attribute (SB modified .css)
					in passing removed some redundant code referencing wabstylesloaded!
	2015-10-15 PYW	P-TATA006 BRD 32. Add suppressOtherFlagStruct to supress flag for flag group whose display is dependent on showOtherFlagGroupStruct.
					Add otherDependentFlagsStruct to control flag for flag group whose display is dependent on showOtherFlagGroupStruct and another flag from
					some other flag group.
	2015-11-11 WAB  P-TAT006 BRD32 Add support for two selects control
	2016-07-27 TWR  451062 - Added cfoutput around closing div tag to fix styling bug
--->
<CFSETTING enablecfoutputonly="yes">



<CFPARAM name="column" default="3">
<CFPARAM name="displayas" default="">
<CFPARAM name="size" default="3">
<CFPARAM name="includeLabelWrapper" default="true"><!--- 2014-01-09 NYB Case 436910 added --->
<!--- 2008/01/22 GCC I had to put these in below to get opp edit working on fnl-europe/cfdev for testing. Not sure if this is an issue that needs further investigation or not? --->
<CFPARAM name="flagGroupformatting.maxpermitted" default="0">
<CFPARAM name="flagGroupformatting.noflagname" default="">
<CFPARAM name="flagGroupformatting.displayas" default="">
<CFPARAM name="flagGroupformatting.showSelectAll" default="false">		<!--- 2014-11-25 AHL Case 442748 Add a SelectAll functionality --->
<CFPARAM name="flagGroupformatting.selectAllText" default="phr_Sys_Select_All">
<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options --->
<CFPARAM name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->

<!---	WAB 2008/06/02
Used to collect together any flags which cause other flags to be required
Code to create relevant javascript is called at the end
(this stucture is populated both when doing checkboxes or selects)
--->
<cfset requiresValueInOtherFlagStruct = structNew()>
<cfset requiresValueInOtherFlagGroupStruct = structNew()>
<cfset showOtherFlagStruct = structNew()>
<cfset showOtherFlagGroupStruct = structNew()>
<!--- PYW 2015/10/23 --->
<cfset suppressOtherFlagStruct = structNew()>
<cfset otherDependentFlagsStruct = structNew()>



	<CFINCLUDE TEMPLATE="qryFlag.cfm">

	<CFIF getFlags.RecordCount IS NOT 0>

		<CFSET flagList = flagList & ",#typeName#_#thisFlagGroup#">
		<CFSET fieldName = "#typeName#_#thisFlagGroup#_#thisentity#">

		<CFIF DisplayAs IS "CheckBox" or DisplayAs IS "" >

			<CFSET OrigSelectedFlagID = ''>
			<CFSET c = 0>

			<cfoutput>

			<!--- NJH 2013/04/08 - if on the unsubscribe flag..
				if checking a comm to subscribe to, then unset the  'unsubscribe from all' checkbox; inversely, if checking the 'unsubscribe from all' checkbox, then uncheck all other checkboxes in the group. --->
			<cfif getFlags.flagGrouptextID eq "unsubscribe">
				<script>
					jQuery(document).ready(function() {
						var unsubscribeGroupID = #application.com.flag.getFlagGroupStructure(flagGroupID="unsubscribe").flagGroupID#;
						var unsubscribeID = #application.com.flag.getFlagStructure(flagID="NoCommsTypeALL").flagID#;
						var controlID = '##control__'+unsubscribeGroupID;
						jQuery(controlID).find('input[type=checkbox]').change(function(e) {
							if (jQuery(this).prop('checked')) {
								if (jQuery(this).val() == unsubscribeID) {
									jQuery(controlID).find('input[type=checkbox][value!='+unsubscribeID+']:checked').removeAttr('checked');
								} else {
									jQuery(controlID).find('input[type=checkbox][value='+unsubscribeID+']:checked').removeAttr('checked');
								}
							}
						})
					})
				</script>
			</cfif>



				<!---
				Limit the number of checkboxes a person can click
				use maxPermitted and maxPermittedMessage in screen or flag definition
				2007/08/21 - GCC; rewritten  2007/10  WAB
				--->
			<cf_includejavascriptonce  template="/javascript/cf/wddx.js">

				<cfif flagGroupformatting.maxPermitted  is not 0>
								<script>
								<!--

							function checkFlagGroupMaxPermitted_#thisFlagGroup# (object) {
								checkFlagGroupMaxPermitted (object,#flagGroupformatting.maxPermitted#,'#flagGroupformatting.maxPermittedMessage# #flagGroupformatting.maxPermitted#')
							}

							function checkFlagGroupMaxPermitted (object,maxPermitted, message) {
								flagGroupObject = document.getElementsByName (object.name)
								numberChecked = checkObject (flagGroupObject)
								if (numberChecked > maxPermitted) {
						            object.checked = false
									alert (message )
								}
							}

								//-->
								</script>

				</cfif>


			<cfif request.relayFormDisplayStyle neq "HTML-div"><TABLE BORDER="0" WIDTH="90%" CELLSPACING="0" CELLPADDING="2"></cfif>
			</cfoutput>
				<CFLOOP QUERY="getFlags">

					<CFINCLUDE TEMPLATE="qryFlagData.cfm">

					<CFSET c = c + 1>
					<CFSET widtha = Round((100 / column))>

						<CFIF c MOD column IS 1>
							<cfif request.relayFormDisplayStyle neq "HTML-div"><cfoutput><TR></cfoutput></cfif>
						</CFIF>
							<!--- WAB added entity ID and original values --->
								<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>
									<CFSET OrigSelectedFlagID = ListAppend(OrigSelectedFlagID, #FlagID#)>
								</CFIF>

							<cfoutput>


							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">
							<cfelse>
								<div class="checkbox-inline">
							</cfif>
								<!--- START: 2014-01-09 NYB Case 436910 --->
								<!--- END: 2014-01-09 NYB Case 436910 --->
								<INPUT TYPE="checkbox" class="checkbox" NAME="#fieldname#" ID="#fieldname#_#FlagID#" VALUE="#FlagID#"<CFIF getFlagData.RecordCount IS NOT 0> CHECKED</CFIF>
														<cfif flagformatting.maxPermitted is not 0>onClick="javascript:checkFlagGroupMaxPermitted_#htmleditformat(thisFlagGroup)#(this);"</cfif>>
							<!--- </TD>
							<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%"> --->
							</cfoutput>
								<cfset extraInfoDisplay = "">
								<cfset extraInfoDisplayName = "">
								<cfif includeLabelWrapper>
									<cfoutput><label for="#fieldname#_#FlagID#"></cfoutput>
								</cfif>
								<CFIF flagFormatting.noFlagName IS 0>
									<CFIF trim(namephrasetextid) is not "">
										<cfoutput>phr_#htmleditformat(namephrasetextid)# &nbsp;</cfoutput>
										<cfset extraInfoDisplayName = "<b>phr_#namephrasetextid#</b><br><br>">
									<CFELSE>
										<cfoutput>#htmleditformat(Name)#&nbsp;</cfoutput>
										<cfset extraInfoDisplayName = "<b>#Name#</b><br><br>">
									</cfif>
								<CFELSE>
										<cfoutput>&nbsp;</cfoutput>
								</CFIF>
								<!--- START: 2014-01-09 NYB Case 436910 --->
								<cfif includeLabelWrapper>
									<cfoutput></label></cfoutput>
								</cfif>
								<!--- END: 2014-01-09 NYB Case 436910 --->
								<!--- 2007-02-22 NM exchanges image with title for popupDescription tag --->
								<cfif descriptionPhraseTextID neq "">
									<cfset extraInfoDisplay = "#extraInfoDisplay#phr_#descriptionPhraseTextID#<br><br>">
								</cfif>

								<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default") and request.relayFormDisplayStyle neq "HTML-div">
									<cfset extraInfoDisplay = "#extraInfoDisplay#Last updated by #getFlagData.lastUpdatedByName# on #dateFormat(getFlagData.lastUpdated,'dd-mmm-yy')#">
								</CFIF>
								<cfif extraInfoDisplay neq "">
									<cf_popupDescription textToDisplay="#extraInfoDisplayName##extraInfoDisplay#">
								</cfif>
								<!--- <!--- 2006-03-14 SWJ Added this to show who updated the flag data --->
								<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default") and request.relayFormDisplayStyle neq "HTML-div">
									<span style="font-weight: bold; cursor: pointer;" title="Last updated by #getFlagData.lastUpdatedByName# on #dateFormat(getFlagData.lastUpdated,'dd-mmm-yy')#"> <img src="/images/misc/iconInformation.gif"> </span>
								</CFIF> --->
							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<cfoutput></TD></cfoutput>
							<cfelse>
								<cfoutput></div></cfoutput>
							</cfif>
						<CFIF c MOD column IS 0 and request.relayFormDisplayStyle neq "HTML-div">
							<cfoutput></TR></cfoutput>
						</CFIF>



				</CFLOOP>
				<cfoutput><CF_INPUT TYPE="Hidden" NAME="#fieldname#_orig" VALUE="#OrigSelectedFlagID#"></cfoutput>
				<CFIF c MOD COLUMN IS NOT 0>
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						<cfoutput>
							<!--- WAB 2015-01-14 noticed that this line (which creates an empty td to fill in the blanks, was adding twice the colspan required (because in the old days we had one column for the checkbox and one for the name)  --->
							<TD COLSPAN=#(3-c MOD COLUMN)#>&nbsp;</TD>
						</TR>
						</cfoutput>
					</cfif>
				</CFIF>

				<!--- START 2014-11-25 AHL Case 442748 SelectAll --->
				<cfif flagGroupformatting.showSelectAll>
					<cfoutput>
						<cfif request.relayFormDisplayStyle neq "HTML-div">
							<TR>
								<TD>
						<cfelse>
							<!--- 2015-12-16 WAB BF-83 add support for the #column# attribute --->
							<cfswitch expression=#Column#>
								<cfcase value="1">
									<div class="checkbox col-xs-12 col-sm-12 col-md-12">
								</cfcase>
								<cfcase value="2">
									<div class="checkbox col-xs-12 col-sm-6 col-md-6">
								</cfcase>
								<cfcase value="4">
									<div class="checkbox-inline">
								</cfcase>
								<cfdefaultcase> <!--- 3 columns --->
									<div class="checkbox-inline">
								</cfdefaultcase>

								</cfswitch>

						</cfif>

							<cfif includeLabelWrapper>
								<label>
							</cfif>

								<input type="checkbox"
									onclick="if (jQuery(this).attr('checked'))     jQuery(&quot;input[name='#fieldname#']&quot;).attr('checked','checked'); else     jQuery(&quot;input[name='#fieldname#']&quot;).removeAttr('checked');"
									> #flagGroupformatting.selectAllText#

							<cfif includeLabelWrapper>
								</label>
							</cfif>

						<cfif request.relayFormDisplayStyle neq "HTML-div">
							</TD>
							<cfif column gt 1>
								<TD COLSPAN=#(COLUMN - 1)#>&nbsp;</TD>
							</cfif>
							</TR>
						<cfelse>
							</div>
						</cfif>


					</cfoutput>
				</cfif>
				<!--- END 2014-11-25 AHL Case 442748 SelectAll --->

			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput>
				</TABLE>
				</CFOUTPUT>
			</cfif>


		<CFELSEIF DisplayAs IS "Select" or DisplayAs IS "MultiSelect" or DisplayAs IS "TwoSelects" >

			<!--- WAB 2015-11-11 P-TAT006 BRD32 Add support for two selects control
				 Decided to use getFlagGroupDataAndControls() functionality for both TwoSelects and Plain select.
				 Doesn't sit perfectly within old flag code, but OK (ends up running getFlags which not needed).
			 	Still need to do a loop to deal with the requiresValueInOtherFlag code	 (Not sure that twoSelects will support )

			--->
			<cfset flagGroupformatting.displayAs = displayas>
			<cfset flagGroupformatting.size = size>
			<cfset flagGroupDataAndControl = application.com.flag.getFlagGroupDataAndControls (flagGroupID = thisFlagGroup, entityid = thisentity, additionalFormatting = flagGroupformatting, method="edit" )[1]>
			<cfoutput>#flagGroupDataAndControl.control_html#</cfoutput>

		</CFIF>




		<!---	WAB 2008/06/02 check for requiresValueInOtherFlag--->
		<CFLOOP QUERY="getFlags">
			<cfset flagFormatting = application.com.flag.getflagStructure(flagid).FORMATTINGPARAMETERSSTRUCTURE>
				<cfif structKeyExists (flagFormatting,"requiresValueInOtherFlag")>
					<cfset tempStruct = {requiresValueInOtherFlag=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlag#" index="otherflagID">
						<cfif application.com.flag.doesflagExist(otherflagID)>
							<cfset tempStruct.requiresValueInOtherFlag = listAppend(tempStruct.requiresValueInOtherFlag,otherflagID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlag neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagMessage")?flagFormatting.requiresValueInOtherFlagMessage:"">
						<cfset requiresValueInOtherFlagStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/24 Jira PROD2015-84 - provide support to make boolean groups required as well. --->
				<cfif structKeyExists(flagFormatting,"requiresValueInOtherFlagGroup")>
					<cfset tempStruct = {requiresValueInOtherFlagGroup=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlagGroup#" index="otherflagGroupID">
						<cfif application.com.flag.doesflagGroupExist(otherflagGroupID)>
							<cfset tempStruct.requiresValueInOtherFlagGroup = listAppend(tempStruct.requiresValueInOtherFlagGroup,otherflagGroupID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlagGroup neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagGroupMessage")?flagFormatting.requiresValueInOtherFlagGroupMessage:"">
						<cfset requiresValueInOtherFlagGroupStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/09 show other flags/groups when flag is selected --->
				<cfif structKeyExists (flagFormatting,"showOtherFlagsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagsWhenSelected#" index="otherFlagID">
						 <cfif application.com.flag.doesflagExist(otherFlagId)>
							 <cfif not structKeyExists(showOtherFlagStruct,flagID)>
								 <cfset showOtherFlagStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagStruct[flagID] = listAppend(showOtherFlagStruct[flagID],otherFlagID)>
						</cfif>
					</cfloop>
				</cfif>
				<cfif structKeyExists (flagFormatting,"showOtherFlagGroupsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagGroupsWhenSelected#" index="otherFlagGroupID">
						 <cfif application.com.flag.doesFlagGroupExist(otherFlagGroupID)>
							 <cfif not structKeyExists(showOtherFlagGroupStruct,flagID)>
								 <cfset showOtherFlagGroupStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagGroupStruct[flagID] = listAppend(showOtherFlagGroupStruct[flagID],otherFlagGroupID)>
						</cfif>
					</cfloop>
				</cfif>

		</CFLOOP>


	<CFELSE>

		<cfoutput><P>* No flag data for this Group</cfoutput>

	</CFIF>


<!--- WAB 2008/11/12  Bug 1327
Moved all code to do with JS for requiresValueInOtherFlag into another template
also copied the required code from screens\showscreenelement_flag and _flaggroup into this new template to fix bug 1327
	 --->


<cfinclude template = "jsVerification.cfm">




<!--- reset defaults --->
<CFSET displayas="">
<CFSETTING enablecfoutputonly="no">

