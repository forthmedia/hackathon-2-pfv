<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		SetFlagIDListTask.cfm
Author:			SWJ
Date created:	28 June 2000

Description:	This template is used to set multiple flags for a given 
				base entity (e.g. person, location or organisation).

It currently supports person and location entities and the following flag Types:
FlagTypeID	Name		DataTable
2			Checkbox	boolean
3			Radio		boolean
4			Date		date
5			Text		text
6			Integer		integer

To call this template you need to pass it the following variables
- 	List of entity ids in frmEntityID for which you are going to set the flags 
	the user choses
-	Entity type in frmEntityTypeID where 1 = location and 0 = person

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ	Fixed the delete query to work with the new x-table structure
2001-01-20		WAB  appears as if this template never finished, (no update procedures).  
			Have changed it so that it uses the CF_setflag custom tag
2001-03-22		WAB		modifications so that entityType does not have to be the same as the flagentityType
2005-04-23		WAB		Mod so that update is done by an include (rather than posting to that template)
2007-10-23 		WAB  changed to cf_setflag	to use flag.cfc for updating
2012-10-05 		PPB 	Case 431046 layout issue
--->


<cfset result = structNew()>
<cfloop index="thisField" list="#FieldNames#">

	<cfif (listFirst(thisField,"_") is "flag" or listFirst(thisField,"_") is "radio") and evaluate(thisField) is not "">
	
		<cfif listFirst(thisField,"_") is "flag">
			<cfset flagid = listlast(thisField,"_")>
			<cfset value = evaluate(thisField)>
		<cfelse>
			<cfset flagid = evaluate(thisField)>
			<cfset value = 1>
		</cfif>
	
		<CF_SetFlag 
			flagID="#flagid#"
			entityId = "#frmEntityID#"
			EntityType = "#frmEntityTypeID#"
			FlagEntityType = "#frmFlagEntityTypeID#"
			Value = "#value#"
			giveFeedBack = "true">

		<cfif isDefined("setFlagResult")>
			<cfset structAppend(result,setFlagResult)>
		</cfif>
		

	</cfif>
</cfloop>

				<!--- WAB 2007-10-23 added some feedback when setFlagV2 implemented--->
				<cfoutput>
					
				<table class="infoblock">
					<cfloop collection = #result# item="thisFlag">
						<cfset thisResult = result[thisFlag]>
						<tr><td colspan=3>#htmleditformat(thisResult.Name)#</td></tr>
						<cfif structKeyexists(thisResult,"numberInserted") and thisResult.numberInserted is not 0>
						<tr><td>&nbsp;</td><td>Number Inserted</td><td>#htmleditformat(thisResult.numberInserted)#</td></tr>
						</cfif>
						<cfif structKeyexists(thisResult,"numberUpdated") and thisResult.numberUpdated is not 0 >
						<tr><td>&nbsp;</td><td>Number Updated</td><td>#htmleditformat(thisResult.numberUpdated)#</td></tr>
						</cfif>
						<cfif structKeyexists(thisResult,"numberNotChanged") and thisResult.numberNotChanged is not 0>
						<tr><td>&nbsp;</td><td>Number Not Changed</td><td>#htmleditformat(thisResult.numberNotChanged)#</td></tr>
						</cfif>

						<cfif structKeyexists(thisResult,"numberDeleted")>
						<tr><td>&nbsp;</td><td>Number Deleted</td><td>#htmleditformat(thisResult.numberDeleted)#</td></tr>
						</cfif>
					
					</cfloop>
				</table>
				
				</cfoutput>
				

