<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type decimal --->
<CFINCLUDE template="qryFlag.cfm">

<CFIF getFlags.RecordCount neq 0>

	<CFLOOP query="getFlags">
	
	    <CFSET flagList = flagList & ",#typeName#_#FlagID#">
	    
		<CFOUTPUT>
			<div class="form-group">
				<label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(Name)#</label>
				<CFIF getFlags.useValidValues eq 1>

					<!--- 2005-11-29 WAB changed to multiselect - also needed to update database - selectionField--->
					<cftry>
						<cf_displayValidValues 
								formfieldname = "frmFlag_#typeName#_#FlagID#"
								validfieldname =  "flag_#getFlags.FlagTextID#"
								currentvalue = ""
								listsize = "4"
								multiple = true
								countryid = "#request.relaycurrentuser.countrylist#"	
								>
								<!--- WAB 2009/03/25 added countryid to above.  Means that will bring back all valid values for all countries that the user has rights to --->
		
								<cfcatch>
									<cfif cfcatch.message contains "Valid Value Error">
											<CF_relayValidatedField
											fieldName="frmFlag_#typeName#_#FlagID#"
											currentValue=""
											charType="decimal"
											size="10"
											maxLength="10"
											helpText=""
										>
									<cfelse>
										<cfrethrow>
									</cfif>
								</cfcatch>
					</cftry>

									
									
				<CFELSE>
					<CF_relayValidatedField
						fieldName="frmFlag_#typeName#_#FlagID#"
						currentValue=""
						charType="decimal"
						size="10"
						maxLength="10"
						helpText=""
					>
					<!--- <INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="" SIZE=10 MAXLENGTH=10><BR><CFIF Trim(Description) IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF> --->
				</cfif>
			</div>
		</CFOUTPUT>
	</CFLOOP>

<CFELSE>
	<P>* No flag data for this Group</P>
</CFIF>

