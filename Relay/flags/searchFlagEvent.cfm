<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Searching on the event table (or any complex flag infact)--->
<!--- 250200 WAB added validvalue support --->

<!--- <CFIF request.relayCurrentUser.personid is 999067> --->
<CFSETTING enableCFoutputOnly = "Yes">
<CFINCLUDE TEMPLATE="qryFlag.cfm">

	<!---
			DAM 2000-05-16
			
			Here we need to determine what values are stored for this flag group
			in the temp selection, if any.
			
			See further note below
	
	--->
	

	<CFQUERY NAME="getFlagValues" datasource="#application.siteDataSource#">
		SELECT 
			selectionfield
		FROM
			TempSelectionCriteria
		WHERE
			SelectionID = #request.relayCurrentUser.personid#
	</CFQUERY>
	<CFSET allselectionfield = "">
	<CFOUTPUT QUERY="getFlagValues">
		<CFSET allselectionfield = ListAppend(allselectionfield,#selectionfield#)>
	</CFOUTPUT>
	
<CFIF #getFlags.RecordCount# IS NOT 0>

	<CFLOOP QUERY="getFlags">

		<!--- find out which fields have been set up as possible search fields --->
		<CFQUERY NAME="getSelectionFields" datasource="#application.siteDataSource#">
		select selectionField,  
		substring(selectionField,len(selectionfield) - charindex ('_',reverse(selectionField))+2,100) as fieldName
		from selectionField
		where selectionField  like  <cf_queryparam value="frmFlag_#typeName#_#getFlags.flagid#_%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>


        <CFOUTPUT>
            <div class="form-group">
		        <div class="row">
					<div class="col-xs-4">#HTMLEditFormat(Name)#</div>
					<div class="col-xs-8">
						<div class="checkbox">
							<CFSET fName = "frmFlag_" & #typeName# & "_" & #FlagID#>
							<!---
									Check the check boxes if they are in 
							--->
							<label>
							    <INPUT TYPE="checkbox" NAME="frmFlag_#typeName#_#FlagID#" VALUE="#FlagID#" <CFIF findnocase(fName,allselectionfield,1) GT 0>CHECKED</CFIF> >
				    			All records for this event
							</label>
						</div>
					</div>
				</div>
				<CFSET flagList = flagList & ",#typeName#_#getFlags.FlagID#">
				<CFSET thisflagID = flagid>
				<CFSET thisflagTextID = flagTextId>
				<CFSET thisflagGroupTextID = flagGroupTextId>
				<CFLOOP query="getSelectionFields">
		
					<div class="row">
						<div class="col-xs-4">
							#htmleditformat(fieldName)#
						</div>
						<div class="col-xs-8">
							<!---
								DAM 2000-05-30
								Added field value retrieval so that the current value of the event field in the 
								current selection can be posted to the custom tag DisplayValidValueList 
								--->
							<CFSET thisField = "frmFlag_#typeName#_#thisFlagID#_#fieldName#">
							<CFINCLUDE TEMPLATE="../templates/getThisFieldValue.cfm">
							<cf_displayValidValues FormFieldName="frmFlag_#typeName#_#thisFlagID#_#fieldName#" 
								validFieldName = "flag.#thisflagTextID#.#fieldName#"
								parentValidFieldName = "flagGroup.#thisflagGroupTextID#.#fieldName#"
								multiple=true
								listsize="3"
								currentvalue=#replace(thisfieldvalue,"'","","ALL")#
								>
						</div>
					</div>
	
					<CFSET flagList = flagList & ",#typeName#_#thisFlagID#_#fieldname#">
				</CFLOOP>
            </div>
        </CFOUTPUT>
	</CFLOOP>
							

<CFELSE>

	<CFOUTPUT><P>* No flag data for this Group</P></CFOUTPUT>				

</CFIF>

<CFSETTING enableCFoutputOnly = "No">
<!--- </CFIF> --->

