<!--- �Relayware. All Rights Reserved 2014 --->
<!---

flags\JSVerfication.cfm

WAB 2008/11/12

Purpose
Created during fix of bug 1327

I had identical code in editFlagRadio and editFlagCheckBox so have bunged it in this template
Might in time be extendable to other flag types - although they are a bit different
The required bit replaces code in showScreenElement_flag and showScreenElement_flaggroup

Mods:
WAB 2009/02/03  added support for mix on entityTypes when using requiresValueInOtherFlagStruct
WAB 2009/03/03 added support for other flag being a checkbox/radio
NYB 2009-04-22 AllSites bug 2140

2012-12-17 PPB Case 427152 change hard-coded 'Select' to a phrase
NJH 2015/09/08	Jira PROD2015-30 - enable flags/groups to be displayed/hidden based on flag being selected. What currently will not work is if the flags are set to required at a screen level. To make a visible flag required, the requiresValueInOtherFlag vaue will have to be set
NJH 2015/09/24	Jira PROD2015-84 - provide support to make boolean groups required as well.
 --->

<!--- NYB 2009-04-22 AllSites bug 2140 - added to stop error --->
<cfparam name="javascriptVerification" default="">

<cfset displayOptionsFlagStruct = {}>
<cfset structAppend(displayOptionsFlagStruct,requiresValueInOtherFlagStruct)>
<cfset structAppend(displayOptionsFlagStruct,requiresValueInOtherFlagGroupStruct)>
<cfset structAppend(displayOptionsFlagStruct,showOtherFlagStruct)> <!--- show another flag --->
<cfset structAppend(displayOptionsFlagStruct,showOtherFlagGroupStruct)> <!--- show another group --->

<cfset randCount = 0>
<cfset jsVarName = "">

<cfif structCount(displayOptionsFlagStruct)>
	<cfloop list="requiresValueInOtherFlagStruct,requiresValueInOtherFlagGroupStruct,showOtherFlagStruct,showOtherFlagGroupStruct" index="displayStruct">  <!--- loop through the various display/validation structures. ShowOtherFlagGroupStruct holds flagGroupIDs.. the others hold flagIDs --->
		<cfloop item="flagID" collection="#displayOptionsFlagStruct#">

			<!--- loop through the various types of display/formatting structures, and build the appropriate js  --->
			<cfif structKeyExists(variables[displayStruct],flagID)>
				<cfset thisItem = isStruct(variables[displayStruct][flagID])?duplicate(variables[displayStruct][flagID]):variables[displayStruct][flagID]>

				<cfset otherFlagOrGroupList = isStruct(thisItem)?thisItem[replaceNoCase(displayStruct,"struct","")]:thisItem>

				<!--- WAB 2009/03/03 added support for other flag being a checkbox/radio  used to assumes other flag to be of type Text/Date/Integer
					2009/02/03 WAB used to have to be of SAME ENTITYTYPE, but now no longer the case
						check that correct entity variable exists (won't exist if an org Flag requires a person flag and we are displaying an org screen)
				--->

				<!--- NJH added ability to pass in a list of flagIds or flagGroupIds....whether it's a flag or flagGroupId is based on the current structure that we're processing.. --->
				<cfloop list="#otherFlagOrGroupList#" index="flagOrGroupID">

					<cfif listFindNoCase("showOtherFlagStruct,requiresValueInOtherFlagStruct",displayStruct)>
						<cfset otherFlagOrGroup = application.com.flag.getFlagStructure(flagOrGroupID)>
					<cfelse>
						<cfset otherFlagOrGroup = application.com.flag.getFlagGroupStructure(flagOrGroupID)>
					</cfif>

					<cfif otherFlagOrGroup.entityTypeID is application.com.flag.getFlagStructure(flagID).entityTypeID>
						<cfset otherFlagEntityID = thisEntity>
					<cfelseif isDefined("frm#otherFlagOrGroup.entityType.uniqueKey#")>   <!--- check that variable exists --->
						<cfset otherFlagEntityID = evaluate("frm#otherFlagOrGroup.entityType.uniqueKey#")>
					<cfelse>
						<cfset otherFlagEntityID = "">
					</cfif>

					<cfif otherFlagEntityID is not "">
						<cfset fieldNameOfOtherFlag = "#otherFlagOrGroup.flagType.name#_#structKeyExists(otherFlagOrGroup,'flagID') and otherFlagOrGroup.flagType.dataTable neq 'boolean'?otherFlagOrGroup.flagID:otherFlagOrGroup.flagGroupId#_#otherFlagEntityID#">

						<!--- dealing with fields being required on other fields --->
						<cfif listFindNoCase("requiresValueInOtherFlagStruct,requiresValueInOtherFlagGroupStruct",displayStruct)>
							<cfif variables[displayStruct][flagID].requiresValueInOtherFlagMessage is "">
								<cfset thisItem.requiresValueInOtherFlagMessage = "phr_Screen_EnterAValueFor #otherFlagOrGroup.translatedname#">
							</cfif>

							<!--- WAB 2009/03/03 added support for other flag being a checkbox/radio (slightly odd, but Trend were testing it) --->

							<cfif displayStruct eq "requiresValueInOtherFlagStruct">
								<cfif listfindnocase("radio,checkbox",otherFlagOrGroup.flagType.name)>
									<cfset fieldNameOfOtherFlag = "#otherFlagOrGroup.flagType.name#_#otherFlagOrGroup.flaggroupID#_#otherFlagEntityID#">
									<cfset verifyFunctionSnippet = "!isItemInCheckboxGroupChecked(form.#fieldNameOfOtherFlag#,#otherFlagOrGroup.flagid#)">
								<cfelse>
									<cfset fieldNameOfOtherFlag = "#otherFlagOrGroup.flagType.name#_#otherFlagOrGroup.flagID#_#otherFlagEntityID#">
									<cfset verifyFunctionSnippet = "checkObject(form.#fieldNameOfOtherFlag#) == 0">
								</cfif>
							<cfelse>
								<!--- NJH Jira PROD2015-84 2015/09/24 required groups when flag is selected. Only requires one checkbox to be selected at the moment --->
								<cfset fieldNameOfOtherFlagGroup = "#otherFlagOrGroup.flagType.name#_#otherFlagOrGroup.flagGroupID#_#otherFlagEntityID#">
								<cfset verifyFunctionSnippet = "verifyObjectv2(form.#fieldNameOfOtherFlagGroup#,' #thisItem.requiresValueInOtherFlagMessage#','true')">
							</cfif>

							<!--- If this item is checked and other item exists and other item is empty --->
							<cfset thisJS = "
							/*  if flag #flagId# in group #typeName#_#thisFlagGroup#_#thisentity# (#getFlags.FlagGroupName#) is selected then #fieldNameOfOtherFlag# is required*/
							if (isItemInCheckboxGroupChecked(form.#typeName#_#thisFlagGroup#_#thisentity#,#flagId#) &&  form.#fieldNameOfOtherFlag# && #verifyFunctionSnippet#) {
								msg += '#jsStringFormat(thisItem.requiresValueInOtherFlagMessage)#\n';
							};
							">
							<cfset javascriptVerification = javascriptVerification & thisJS>

						<!--- show another flag/group when value selected --->
						<cfelse>

							<cfset randCount++>

							<!--- other field selector: Could be done in a list of values if a list of flags is passed through, but doing it one by one at the moment
								If we're showing a group, then the selector is the control around the group. Otherwise if not a boolean flag, it shows the name of the flag. If a boolean flag, then add the value as well
							 --->
							<!--- <cfset otherFieldSelector = displayStruct eq "showOtherFlagGroupStruct"?"##control__#flagOrGroupID#":"[name=#fieldNameOfOtherFlag#]#otherFlag.flagType.dataTable eq 'boolean'?'[value=#flagOrGroupID#]':''#"> --->
							<cfif  displayStruct eq "showOtherFlagGroupStruct">
								<cfset otherFieldSelector = "##control__#otherFlagOrGroup.flagGroupID#">
								<cfif otherFlagOrGroup.flagGroupTextId neq "">
									<cfset otherFieldSelector = otherFieldSelector & ",##control__" & otherFlagOrGroup.flagGroupTextId> <!--- looking for both flagGroupID and flagGroupTextID --->
								</cfif>
							<cfelse>
								<cfset otherFieldSelector = "[name=#fieldNameOfOtherFlag#]">
								<cfif otherFlagOrGroup.flagType.dataTable eq "boolean">
									<cfset otherFieldSelector = otherFieldSelector & "[value=#flagOrGroupID#]">
								</cfif>
							</cfif>
							<cfset jsVarName = "$_#typeName#_#thisFlagGroup#_#thisentity#_#flagId#_#randCount#">

							<cfoutput>
								<script>
									var #jsVarName# = jQuery('[name=#typeName#_#thisFlagGroup#_#thisEntity#]');
									if (#jsVarName#.is(':checkbox')) {
										#jsVarName# = jQuery('[name=#typeName#_#thisFlagGroup#_#thisEntity#][value=#flagId#]');
									}

									#jsVarName#.on('change',function (){
										var $_jOtherField = jQuery('#otherFieldSelector#');

										var $_jOtherFieldRow = $_jOtherField.closest('div.form-group');
										if ($_jOtherFieldRow.length == 0) {
											$_jOtherFieldRow = $_jOtherField.closest('tr.screenTR')
										}

										<!--- if current field is a checkbox or radio, pass in the collection... otherwise pass in the object... probably better way of doing this. --->
										var $_input = jQuery('[name=#typeName#_#thisFlagGroup#_#thisentity#]')[0]
										if (jQuery($_input).is(':checkbox') || jQuery($_input).is(':radio')) {
											$_input = document.getElementsByName('#typeName#_#thisFlagGroup#_#thisentity#');
										}

										if (isItemInCheckboxGroupChecked($_input,#flagId#)) {
											$_jOtherFieldRow.show().addClass('dependentProfiles'); <!--- Jira PROD2015-85 - add class to shown profiles to give them a styling handle. May need to add tiering at a later date --->
										} else {
											$_jOtherFieldRow.hide();
											<!--- if we're dealing with hiding a flag group, we have to go through each item in the group and unset it, and fire the change event so that all dependent fields get hidden as well --->
											<cfif  displayStruct eq "showOtherFlagGroupStruct">
											$_jOtherField.find(':input').each(function() {
												switch(this.type) {
													case 'checkbox': case 'radio':
														jQuery(this).prop('checked',false);
														break;
													default:
														if (this.type != 'hidden') {
															jQuery(this).val('');
														}
												}
												jQuery(this).change();
											});
											<cfelse>
											if ($_jOtherField.is(':checkbox') || $_jOtherField.is(':radio')) {
												$_jOtherField.prop('checked',false);
											} else {
												$_jOtherField.val('');
											}
											$_jOtherField.change();
											</cfif>
										}
									})

									jQuery(document).ready(function () {
										#jsVarName#.change();
									})
								</script>
							</cfoutput>
						</cfif>
					</cfif>
				</cfloop>
			</cfif>
		</cfloop>
	</cfloop>

	<cfparam name="request.jsVerificationIncluded" default="0">

	<!--- NJH 2015/09/17 - PROD2015-30 Fire the onchange events for the various inputs when the reset functionality is called --->
	<cfif not request.jsVerificationIncluded and jsVarName neq "">
		<cfoutput>
		<script>
			jQuery(document).ready(function () {
				#jsVarName#.closest('form').find('##screenResetButton').on('click',function(event) {
					event.preventDefault();
					var $_form = jQuery(this).closest('form');
					$_form.trigger('reset');
					$_form.find('input').not(':button,:hidden,:submit').each(function() {
						jQuery(this).change();
					});
				});
			})
		</script>
		</cfoutput>
		<cfset request.jsVerificationIncluded=1>
	</cfif>
</cfif>

<cfif required is not 0 and getFlags.recordCount gt 0 >
	<cfif requiredMessage is "">
		<cfif requiredLabel is  "" >
			<cfif getFlags.recordCount gt 1>
				<cfset thisrequiredLabel = getFlags.FlagGroupName>
			<cfelse><!--- special case for a single flag --->
				<cfset thisrequiredLabel = getFlags.Name>
			</cfif>
		<cfelse>
			<cfset thisrequiredLabel = requiredLabel>
		</cfif>

		<cfif getFlags.recordCount gt 1>
			<cfset thisrequiredMessage = "phr_Sys_Screen_SelectAtLeastOneCheckboxFor #thisrequiredLabel#">
		<cfelse>
			<!--- 2012-12-17 PPB Case 427152 change hard-coded 'Select' to a phrase (perhaps I should have just removed the word) --->
			<cfset thisrequiredMessage = trim("phr_Sys_Screen_Select #thisrequiredLabel#")>
		</cfif>
	<cfelse>
		<cfset thisRequiredMessage = requiredMessage>
	</cfif>

	<CF_INPUT type="HIDDEN" name="#typeName#_#thisFlagGroup#_#thisentity#_required" value="#thisrequiredMessage#">
	<cfif getFlags.recordCount gt 1>
		<CFSET javascriptVerification = javascriptVerification & "
			/* atleast #required# item in #getFlags.FlagGroupName# required*/
			msg = msg + verifyObjectv2(form.#typeName#_#thisFlagGroup#_#thisentity#,'#thisrequiredMessage#','#required#');
		" >
	<cfelse>
		<CFSET javascriptVerification = javascriptVerification & "
			/* #getFlags.Name# must be checked */
			if (! isItemInCheckboxGroupChecked (form.#typeName#_#thisFlagGroup#_#thisentity#, #getFlags.flagid#)) {msg = msg + '#thisrequiredMessage#\n';}" >
	</cfif>
</cfif>