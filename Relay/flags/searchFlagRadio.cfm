<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type 3 = Radio Button 

2016-03-07	WAB	PROD2016-717 Changed checkbox display to multiselect.  

--->
<CFINCLUDE TEMPLATE="qryFlag.cfm">
	
<CFIF getFlags.RecordCount IS NOT 0>

	<CFSET flagList = flagList & ",#typeName#_#thisFlagGroup#">

	<!---
			DAM 2000-05-16
			
			Here we need to determine what values are stored for this flag group
			in the temp selection, if any.
	
	--->
	
	<CFSET flagCriteria = "frmflag_" & "#typeName#_#thisFlagGroup#">

	<CFQUERY NAME="getFlagValues" datasource="#application.siteDataSource#">
		SELECT 
			criteria1
		FROM
			TempSelectionCriteria
		WHERE
			SelectionID = #request.relayCurrentUser.personid#
			AND
			SelectionField =  <cf_queryparam value="#flagCriteria#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>

    <div class="form-group">
		<cf_select name="frmFlag_#typeName#_#thisFlagGroup#" multiple="true" query = #getFlags# value = "flagID" display="name" selected = "#getFlagValues.criteria1#" pluginOptions = #{"selectAll"=false}#>
	</div>
	
<CFELSE>

	<P>* No flag data for this Group</P>

</CFIF>

