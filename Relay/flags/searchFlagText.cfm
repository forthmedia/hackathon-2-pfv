<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type 5 = Text --->

<CFINCLUDE TEMPLATE="qryFlag.cfm">
	
<CFIF #getFlags.RecordCount# IS NOT 0>

		<CFLOOP QUERY="getFlags">
			<CFSET flagList = flagList & ",#typeName#_#FlagID#">
			<CFOUTPUT>
				<div class="form-group">
					<label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(Name)#</label>
			
					<CFIF getFlags.usevalidvalues is 1>
							<!--- WAB  2007/12/03 added catch code (from viewFlagInteger.cfm --->
						<CFSET thisField = "frmFlag_#typeName#_#FlagID#" >
						<CFINCLUDE TEMPLATE="../templates/getThisFieldValue.cfm">
	
						<cftry>
							<cf_displayValidValues 
									formfieldname = "frmFlag_#typeName#_#FlagID#"
									validfieldname =  "flag_#getFlags.FlagTextID#"
									parentValidFieldName = "flagGroup.#getFlags.flagGroupTextID#"
									currentvalue = #trim(replace(thisfieldvalue,"'","","ALL"))#
									multiple = true
									listsize = "4"
									keepOrig = false
									>
	
									<cfcatch>
										<cfif cfcatch.message contains "Valid Value Error">
												<CF_INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="#thisFieldValue#" SIZE=30 MAXLENGTH=50>
												<CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF>
										<cfelse>
											<cfrethrow>
										</cfif>
									</cfcatch>
						</cftry>
							
					<CFELSE>
						<CFSET thisField = "frmFlag_#typeName#_#FlagID#">
						<CFINCLUDE TEMPLATE="../templates/getThisFieldValue.cfm">
						<CF_INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="#thisFieldValue#" SIZE=30 MAXLENGTH=50>
						<CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF>
					</cfif>
                </div>
			</CFOUTPUT>
		</CFLOOP>
<CFELSE>
	<P>* No flag data for this Group</p>
</CFIF>


