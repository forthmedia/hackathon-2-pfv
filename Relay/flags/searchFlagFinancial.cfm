<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type Financial --->
<CFINCLUDE template="qryFlag.cfm">

<CFIF getFlags.RecordCount neq 0>

	<CFLOOP query="getFlags">

		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
		
		<CFOUTPUT>
			<div class="form-group">
				<label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(getFlags.Name)#</label>
				<CFIF getFlags.useValidValues eq 1>

					<cftry>
						<cf_displayValidValues
								formfieldname = "frmFlag_#typeName#_#FlagID#"
								validfieldname =  "flag_#getFlags.FlagTextID#"
								currentvalue = ""
								listsize = "4"
								multiple=true
								countryid = "#request.relaycurrentuser.countrylist#"
								>
		
								<cfcatch>
									<cfif cfcatch.message contains "Valid Value Error">
											<CF_relayValidatedField
											fieldName="frmFlag_#typeName#_#FlagID#"
											currentValue=""
											charType="financial"
											size="20"
											maxLength="20"
											helpText=""
										>
									<cfelse>
										<cfrethrow>
									</cfif>
								</cfcatch>
					</cftry>

				<CFELSE>
					<CF_relayValidatedField
						fieldName="frmFlag_#typeName#_#FlagID#"
						currentValue=""
						charType="financial"
						size="20"
						maxLength="20"
						helpText=""
					>
				</cfif>
			</div>
		</CFOUTPUT>
	</CFLOOP>
	
<CFELSE>
	<P>* No flag data for this Group</P>
</CFIF>

