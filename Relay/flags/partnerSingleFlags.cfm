<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit

	getFlagGroup.FlagGroupID

	--->

<!--- Find Country for this Entity --->
<CFQUERY NAME="getCountry" datasource="#application.siteDataSource#">
	SELECT DISTINCT l.CountryID
	<CFIF entityType IS 0>
	FROM Person As p, Location As l
	WHERE p.PersonID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND p.LocationID = l.LocationID
	<CFELSE>
	FROM Location As l
	WHERE l.LocationID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
</CFQUERY>

<CFSET eCountryID = getCountry.CountryID>
<CFSET EntityCountryList = "0,#eCountryID#">

<!--- Find all Country Groups for this Country --->
<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
SELECT DISTINCT b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE a.CountryID =  <cf_queryparam value="#eCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Entity Country --->
  AND (b.ISOcode IS null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
ORDER BY b.CountryID
</CFQUERY>

<CFIF getCountryGroups.RecordCount IS NOT 0>
	<CFSET EntityCountryList =  EntityCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>

<!--- NJH 2016/09/23 PROD2016-2383 - show flaggroups even if expiry not set --->
<CFQUERY NAME="getFlagGroup" datasource="#application.sitedatasource#">
SELECT DISTINCT FlagGroup.FlagGroupID, FlagTypeID, EntityTypeID, Flag.FlagID,
	flagType.Name as typename,flagType.dataTable as typedata
FROM FlagGroup
	inner join Flag on Flag.FlagGroupID = FlagGroup.FlagGroupID
	inner join 	FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID

WHERE FlagGroup.Active <> 0
AND (FlagGroup.expiry is null or FlagGroup.Expiry > #CreateODBCDateTime(Now())#)
AND FlagGroup.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
AND FlagGroup.Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND FlagGroup.FlagGroupID =  <cf_queryparam value="#getParentFlagGroup.FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
AND Flag.FlagTextID =  <cf_queryparam value="#FieldTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >

</CFQUERY>

<CFIF getFlagGroup.RecordCount IS 1>

	<CFSET thisFlagGroup = #getFlagGroup.FlagGroupID#>
	<CFSET singleFlag = #getFlagGroup.FlagID#>
	<!--- NJH 2010/08/09 Rw8.3 - changed code to use flag structure
		WAB 2010/09/09 replaced with query
	<CFSET typeName = #ListGetAt(application.typeList,getFlagGroup.FlagTypeID)#>
	<CFSET typeData = #ListGetAt(application.typeDataList,getFlagGroup.FlagTypeID)#>
	<cfset typeName = application. flagGroup[getFlagGroup.flagGroupID].flagType.name>
	<cfset typeData = application. flagGroup[getFlagGroup.flagGroupID].flagType.datatable>--->

	<!--- typeName should not be 'Group' but page exists for this case --->
	<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">

<CFELSE>

	Error - no live flag found for name '<CFOUTPUT>#htmleditformat(FieldTextID)#</CFOUTPUT>' with correct scope.

</CFIF>

<!--- Reset singleFlag variable --->
<CFSET singleFlag = 0>








