<!--- �Relayware. All Rights Reserved 2014 --->
<!--- View Flags of type 4 = Date

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing

	2011/09/19 PPB LID6630 use phr_sys_NoData
AXA	2015-05-01 P-ARB001 Case 444498	span tag surrounding dates changed to div because it was not displaying when dates were readonly
WAB 2015-06-22 FIFTEEN-384 Add time support to date flags
--->
<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>
	<!--- START: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput><P><TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2"<!---  BGCOLOR="##CCCCCC" --->></cfoutput>
	</cfif>
	<!--- END: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->
	<CFLOOP QUERY="getFlags">

		<CFINCLUDE TEMPLATE="qryFlagData.cfm">

		<cfparam name="flagFormatting.showTime" default="false">
		<cfif not flagFormatting.showTime>
			<cfset FlagFormatting.timeMask = "">		
		</cfif>
		<!--- If showing time then by default show local time, otherwise show server time (which makes it backwards compatible) --->
		<cfparam name="flagFormatting.showAsLocalTime" default="#flagFormatting.showTime#">


		<CFOUTPUT>
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<TR><CFIF FlagFormatting.NoFlagName is not 1><TD VALIGN=TOP ALIGN=LEFT></cfif>
		<cfelse>
			<CFIF FlagFormatting.NoFlagName is not 1><label class="flagAttributeLabel"></cfif>
		</cfif>

			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
				phr_#htmleditformat(trim(namephrasetextid))#
			<CFELSE>
				#htmleditformat(Name)#
			</cfif>
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<CFIF FlagFormatting.NoFlagName is not 1></TD></CFIF><CFIF FlagFormatting.FlagNameOnOwnLine is 1></TR><TR></CFIF><TD VALIGN=TOP ALIGN=LEFT>
			<CFIF getFlagData.RecordCount IS NOT 0>#application.com.dateFunctions.DateTimeFormat(date = getFlagData.Data, argumentCollection = flagFormatting)#<CFELSE>- phr_sys_NoData -</CFIF>	<BR>
			<CFIF trim(descriptionphrasetextid) is not "">(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)<CFELSE><CFIF #Trim(Description)# IS NOT "">(<I>#htmleditformat(description)#</I>)</cfif></CFIF>
			</TD></TR>
		<cfelse>
			<CFIF FlagFormatting.NoFlagName is not 1></label></cfif>
			<!--- START 2015-05-01 AXA changed from span to div so that dates display --->
			<div class="form-control">
				<CFIF getFlagData.RecordCount IS NOT 0>#application.com.dateFunctions.DateTimeFormat(date = getFlagData.Data, argumentCollection = flagFormatting)#<CFELSE>- phr_sys_NoData -</CFIF>
			</div>
			<!--- END 2015-05-01 AXA changed from span to div so that dates display --->
			<CFIF trim(descriptionphrasetextid) is not "">
				<span class="help-block">phr_#htmleditformat(descriptionphrasetextid)#</span>
			<CFELSE>
				<CFIF #Trim(Description)# IS NOT ""><span class="help-block">#htmleditformat(description)#</span></cfif>
			</CFIF>
		</cfif>
		</CFOUTPUT>

	</CFLOOP>

	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput></TABLE></cfoutput>
	</cfif>

<CFELSE>

	<cfoutput><P>* No flag data for this Group</cfoutput>

</CFIF>


