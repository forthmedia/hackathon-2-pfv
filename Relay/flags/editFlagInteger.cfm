<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Edit Flags of type 6 = Integer 

WAB 2005-05-19 corrected a bug line 43, was testing variable flagformat and then using variable specialflagformatting
	2005-08-24 SWJ tidied up the screen added the labelTDwidth to the label column
WAB 2006-05-30 added new parameters to the displayvalid value list so that by default if there are no available valid values you get a sensible (null) item in the drop down rather than an input box
WAB 2007-02-06 P-FNL041 start to use flagFormatting collection

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
WAB 2010/05/12 LID 3251 
IH	2012-09-18	Case 430433 fix formatting issue with nested tables
WAB 2016-02-02	Add multiple=false to displayValidValues, for clarity
2016-03-09 	WAB	 PROD2016-684 Removed class flagAttributeLabel - should be same as any other label
--->


<CFINCLUDE TEMPLATE="qryFlag.cfm">

	<CF_includeonce template="/templates/relayFormJavaScripts.cfm">

<CFIF getFlags.RecordCount IS NOT 0>
	
	<!-- Start edit FlagInteger-->
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<CFOUTPUT><table width="90%" border="0" cellspacing="0" cellpadding="2"<CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>></cfoutput>
	</cfif>
	<CFLOOP QUERY="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">

		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		
		<cfoutput>
		<cfif request.relayFormDisplayStyle neq "HTML-div"><TR></cfif>
			<CFIF FlagFormatting.NoFlagName eq 0>
				<cfparam name="labelTDwidth" type="numeric" default="300">
				<cfif request.relayFormDisplayStyle neq "HTML-div">
					<td width="#labelTDwidth#" align="left" valign="top">
						#htmleditformat(Name)#
					</td>
				<cfelse>
					<label <!--- class="flagAttributeLabel" --->>#htmleditformat(Name)#</label>
				</cfif>
			</CFIF>	

		<cfif request.relayFormDisplayStyle neq "HTML-div"><td align="left" valign="top"></cfif>
			<CFIF isdefined ("flag_#trim(getFlags.FlagTextID)#_ValidValues")>

  				<cfparam name = "inputOnNoValidValues" default = "false">
				<cfparam name = "noValidValuesText" default = "#application.delim1#No Options Available">
				<CF_DISPLAYvALIDVALUES formfieldname = "#typeName#_#FlagID#_#thisentity#"
						validValues = #evaluate('flag_#getFlags.FlagTextID#_validvalues')#
						currentValue = #getFlagData.Data#
						multiple = false
						inputOnNoValidValues = #inputOnNoValidValues#
						noValidValuesText = "#noValidValuesText#"
						allowCurrentValue = true <!--- WAB 2010/04/12 will now display current value even if not in the validvalues --->
						attributeCollection = "#flagFormatting#"
 						entityID = "#thisEntity#"
						countryid = #variables.countryid# 						
			>

			
			<CFELSE>
			
				
				<CFIF getFlagData.RecordCount IS NOT 0>
					<cfset value = getFlagData.Data>
				<cfelse>
					<cfset value = "">
				</CFIF>

				<!--- WAB 2007-02-21
						max length reduced to 9 to prevent overflow of integer data field 
						could do something cleverer with JS, but this should do	
					WAB 2015-11-13 CASE 446584 Add support for FlagFormatting.maxlength.  
						Note that the default value of FlagFormatting.maxlength (11) allows for a negtive sign and assumes that we do max/min validation 
						Since there is no max/min validation on this field I have stuck with a maximum of 9 digits
				--->
				<CF_relayValidatedField	
						fieldName="#typeName#_#FlagID#_#thisentity#"
						currentValue="#variables.value#"
						charType="numeric"
						size="#min(FlagFormatting.size,9)#"
						maxLength="#min(FlagFormatting.maxlength,9)#"
						helpText=""
					>
					
				<!--- <INPUT TYPE="text" NAME="#typeName#_#FlagID#_#thisentity#" VALUE="##" SIZE=10 MAXLENGTH=10> --->
				<INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>#htmleditformat(getFlagData.Data)#</CFIF>">
			</CFIF>
	
			<CFIF trim(descriptionphrasetextid) is not ""><BR>(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)<CFELSE><CFIF #Trim(helpText)# IS NOT "">(<I>#htmleditformat(helpText)#</I>)</cfif></CFIF>
	
			<!--- <INPUT TYPE="HIDDEN" NAME="#typeName#_#FlagID#_#thisentity#_integer" VALUE="You must enter an integer for flag '#Name#'."> --->
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			</td>
			</TR>
		</cfif>
		</cfoutput>
	</CFLOOP>
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput></table></cfoutput>
	</cfif>
	
					
<CFELSE>

	<cfoutput><P>* No flag data for this Group</cfoutput>

</CFIF>


