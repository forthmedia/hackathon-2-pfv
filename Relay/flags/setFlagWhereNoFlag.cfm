<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:			
Author:			
Date created:	

Description:  This file is still under development however it will set a  boolean flag 
		for an entity when there is no existing flag for that entity id.
		
		It uses a join between two select statements
		
		The selection quesry could be removed and this could be used to set a default flag for
		all entity ids.

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

--->






<!--- This query is currently hard coded --->
<CFQUERY NAME="SetFlagWherNoFlag" datasource="#application.siteDataSource#">
insert into booleanFlagData 
(entityID,FlagID, createdby,created,lastupdatedBy,lastUpdated)

SELECT S.LocationID, 2477, 100, '10-aug-00', 100, '10-aug-00'
FROM (SELECT BooleanFlagData.FlagID, FlagGroup.FlagGroupID, 
    BooleanFlagData.EntityID
FROM BooleanFlagData INNER JOIN
    Flag ON BooleanFlagData.FlagID = Flag.FlagID INNER JOIN
    FlagGroup ON 
    Flag.FlagGroupID = FlagGroup.FlagGroupID
WHERE (FlagGroup.FlagGroupID = 2128)) AS F RIGHT OUTER JOIN
    (SELECT DISTINCT Location.LocationID
FROM Location INNER JOIN
    Person ON 
    Location.LocationID = Person.LocationID INNER JOIN
    SelectionTag ON 
    Person.PersonID = SelectionTag.EntityID
WHERE (SelectionTag.SelectionID = 1)) AS S ON 
    F.EntityID = S.LocationID
WHERE (F.EntityID IS NULL)
</CFQUERY>




