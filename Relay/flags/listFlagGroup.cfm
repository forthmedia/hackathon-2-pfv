<!--- �Relayware. All Rights Reserved 2014 --->
<!--- This program outputs a valuelist for a given flaggroup textid 

This requires #thisFlagGroup# (as a flgGroupTextID) 
and #thisentity# (i.e. EntityID)

It should be called as follows:

		<CFSET FlagGroupTextID = "nPPPartnerType"> 
		<CFSET ThisEntity = #LocationID#>
		
		<CFOUTPUT>#Result#</CFOUTPUT>
		#result# returns None set if there are no flags set
		#result2# returns blank inthis case


--->

<!--- Uncomment this for testing
<CFSET FlagGroupTextID = "JobFunction"> 
<CFSET ThisEntity = 2>
 --->  

<!--- since changing the query to work on fg.FlagGroupTextID IN (#FlagGroupTextID#) I need to ensure that there are quotes around the 
	flaggrouptextid  (ensures backwards compatability and means that doesn;'t have to be called with quotes	--->
<!--- <CFIF left(trim(FlagGroupTextID),1) is not "'">
	<CFSET FlagGroupTextID = "'" & FlagGroupTextID>
</cfif>
 
<CFIF Right(trim(FlagGroupTextID),1) is not "'">
	<CFSET FlagGroupTextID = FlagGroupTextID & "'">
</cfif> --->

 
<CFSET Result = ""> 
<!--- Find the FlagGroup data type --->
<CFQUERY NAME="GetFlagGroup" datasource="#application.siteDataSource#">
	SELECT ft.FlagTypeID, fg.FlagGroupID, ft.datatable, fg.entitytypeid
	FROM FlagGroup as fg, FlagType  as ft
	WHERE fg.FlagTypeID = ft.FlagTypeID
	AND fg.FlagGroupTextID IN (<cf_queryparam value="#FlagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
	
	AND fg.flagtypeid <> 1   <!--- parent --->
	
	UNION

	SELECT ft.FlagTypeID, fg.FlagGroupID, ft.datatable, fg.entitytypeid
	FROM flaggroup as pfg, FlagGroup as fg, FlagType as ft
	WHERE pfg.flaggroupid = fg.parentflaggroupid
	and fg.FlagTypeID = ft.FlagTypeID
	AND pfg.FlagGroupTextID IN (<cf_queryparam value="#FlagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
	AND pfg.flagtypeid = 1   <!--- parent --->

</CFQUERY>

<CFIF GetFlagGroup.recordcount IS 0>
	<CFSET Result = "Error: FlagGroupTextID is wrong">
<CFELSE>

	<CFIF thisentity is "">
		<!--- this parameter not passed, so work out whether a person or location flag --->
		<CFIF getFlagGroup.EntityTypeID IS 0>
			<CFSET thisentity = personid>
		<CFELSEIF getFlagGroup.EntityTypeID IS 1>
			<CFSET thisentity = locationid>
		</CFIF>
	</cfif>

		<CFSET flaggroupidlist = valuelist(GetFlagGroup.FlagGroupID)>	
		<CFSET flaggrouptablelist = valuelist(GetFlagGroup.datatable)>	
		

	<CFLOOP index=x from="1" to="#listlen(flaggroupidlist)#">


		<CFQUERY NAME="GetFlagValues" datasource="#application.siteDataSource#">
		select f.name 
		from flag as f, #listgetat(flaggrouptablelist,x)#flagdata as a
		where a.flagid= f.flagid
		and f.active <> 0
		and f.flaggroupid =  <cf_queryparam value="#listgetat(flaggroupidlist,x)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and entityid =  <cf_queryparam value="#thisentity#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<CFIF getflagvalues.recordcount IS NOT 0>	
			<CFSET result = listappend(result,valuelist(getflagvalues.name))>
		</CFIF>
	</cfloop>	
		
		<CFIF result is not "">
			<CFSET Result = Replace(Result, "," , ", " ,"ALL")>
			<CFSET Result2 =result>
		<CFELSE>
			<CFSET Result = "None Set">
			<CFSET Result2 = "">
		</CFIF>

	
</CFIF>