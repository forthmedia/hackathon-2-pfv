<!--- �Relayware. All Rights Reserved 2014 --->
<!--- View Flags of type 2 = Checkbox

	WAB 2007/01/17 changes to do with flagGroupFormatting Parameters
	AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing
	NYB 2008/08/09  P-SOP003 - added ability to SortOrder different options via orderType.  Accepts: SortOrder, Alpha or Random
	WAB 2011/09/27	P-TND109 change view mode to use disabled controls rather than plain text
	2015-10-13 WAB 	PROD2015-85 Alter <LABEL> tags so do not have nested INPUT, add For attribute (SB modified .css).  Removed double column layout
--->
<cfparam name="column" default="3">
<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options --->
<cfparam name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->




<cfif ( (structKeyExists(flagGroupFormatting,"showFlagsAsListInViewMode") and flagGroupFormatting.showFlagsAsListInViewMode)
		OR
		flagGroupFormatting.DisplayAs IS "Select" or flagGroupFormatting.DisplayAs IS "MultiSelect"
		)
  >   <!--- WAB 2007-06-13 --->
		<cfquery name="getFlagData" datasource="#application.siteDataSource#">
--				SELECT dbo.booleanFlagList (@flagGroupId = #thisFlagGroup#, @entityID = #thisEntity#,default) as data
				SELECT dbo.booleanFlagList (#thisFlagGroup#, #thisEntity#,default) as data
		</cfquery>
		<cfoutput><cfif getFlagData.data is not "">#htmleditformat(getFlagData.data)#<cfelse>NO DATA</cfif></cfoutput>
<cfelse>


	<cfinclude template="qryFlag.cfm">

	<cfif #getFlags.RecordCount# IS NOT 0>

			<CFSET fieldName = "#typeName#_#thisFlagGroup#_#thisentity#">
			<cfset c = 0>
			<!--- START: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->
			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput><BR><TABLE BORDER="0" WIDTH="90%" CELLSPACING="0" CELLPADDING="2"<!---  BGCOLOR="##CCCCCC" --->></cfoutput>
			</cfif>
			<!--- END: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->
			<cfset noneSelected = true>
			<cfloop QUERY="getFlags">
				<cfinclude template="qryFlagData.cfm">
				<cfif getFlagData.RecordCount IS NOT 0 or request.relayFormDisplayStyle neq "HTML-div">
					<cfset noneSelected = false>
					<cfset c = c + 1>
					<CFSET widtha = Round((100 / column))>
					<cfoutput>
						<cfif c MOD column IS 1>
							<cfif request.relayFormDisplayStyle neq "HTML-div"><cfoutput><TR></cfoutput></cfif>
						</cfif>
						<cfif request.relayFormDisplayStyle neq "HTML-div">
								<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">
							<cfelse>
								<!--- 2015-12-16 WAB BF-83 add support for the #column# attribute --->
								<cfswitch expression=#Column#>
									<cfcase value="1">
										<div class="checkbox col-xs-12 col-sm-12 col-md-12">
									</cfcase>
									<cfcase value="2">
										<div class="checkbox col-xs-12 col-sm-6 col-md-6">
									</cfcase>
									<cfcase value="4">
										<div class="checkbox-inline">
									</cfcase>
									<cfdefaultcase> <!--- 3 columns --->
										<div class="checkbox-inline">
									</cfdefaultcase>
								</cfswitch>
							</cfif>

						<input type="checkbox" class="checkbox" <cfif getFlagData.RecordCount IS NOT 0>checked=checked</cfif> disabled id="#fieldname#_#flagid#">
						<!--- <cfif getFlagData.RecordCount IS NOT 0><IMG SRC="/images/misc/flag-yes.gif" WIDTH=15 HEIGHT=15 ALT="Yes" BORDER="0"><cfelse><IMG SRC="/images/misc/flag-no.gif" WIDTH=15 HEIGHT=15 ALT="No" BORDER="0"></cfif> --->
						<label for="#fieldname#_#flagid#">
						<cfif flagFormatting.noFlagName IS 0>
							<cfif trim(namephrasetextid) is not "">
								phr_#htmleditformat(trim(namephrasetextid))# &nbsp;
							<cfelse>
								#htmleditformat(Name)#&nbsp;
							</cfif>
						<cfelse>
								&nbsp;
						</cfif>
							</label>
						<cfif request.relayFormDisplayStyle neq "HTML-div">
							</TD>
							<cfif c MOD column IS 0>
								</TR>
							</cfif>
						<cfelse>
							</div>
						</cfif>
					</cfoutput>
				</cfif>
			</cfloop>
			<cfif noneSelected and not request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput><span class="flagAttributeLabel">- phr_sys_noneSelected -</span></cfoutput>
			</cfif>

			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput><cfif c MOD COLUMN IS NOT 0><TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))#>&nbsp;<BR></TD></TR></cfif></TABLE></cfoutput>
			</cfif>


	<cfelse>

		<cfoutput><P>* No flag data for this Group</cfoutput>

	</cfif>

</cfif>
