<!--- �Relayware. All Rights Reserved 2014 --->
<!--- View Flags of type 5 = Text

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing
2011/09/19 PPB LID6630 use phr_sys_NoData
WAB 2011/09/27	P-TND109 change view mode to use disabled controls rather than plain text
2013-04-24 	NYB 	Case434585 changed layout to use flagGroupFormatting.noFlagTable to control if a table is used - made to be identical to editFlagText.cfm
2015/09/15	NJH	Added Ids to all text fields so that we can get a handle on them, even when in view mode.
--->


<CFINCLUDE TEMPLATE="qryFlag.cfm">

<!--- START: 2013-04-24 NYB Case434585 added --->
<cfparam name="flagGroupFormatting.noFlagName" default = "0">
<cfparam name="flagGroupFormatting.noFlagTable" default = "#flagGroupFormatting.noflagname#">
<cfparam name="FlagNameOnOwnLine" default="0">
<!--- END: 2013-04-24 NYB Case434585 added --->

<CFIF #getFlags.RecordCount# IS NOT 0>
	<!--- START: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->

	<!--- START: 2013-04-24 NYB Case434585 added IF --->
	<cfif not flagGroupFormatting.noFlagTable and request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput><!---<P>---><TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2" <!---  BGCOLOR="##CCCCCC" ---> ></cfoutput>
	</cfif>
	<!--- END: 2013-04-24 NYB Case434585 added IF --->

	<CFLOOP QUERY="getFlags">

		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<!--- START: 2013-04-24 NYB Case434585 added IF's, layout --->
			<cfif not flagGroupFormatting.noFlagTable>
				<CFOUTPUT>
					<TR>
					<CFIF FlagFormatting.NoFlagName is not 1>
						<TD VALIGN=TOP ALIGN=LEFT><!--- <FONT FACE="Arial, Chicago, Helvetica" SIZE="2"> --->
						<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
							phr_#htmleditformat(trim(namephrasetextid))#
						<CFELSE>
							#htmleditformat(Name)#
						</cfif>

						<!--- </FONT> --->
						</TD>
					</CFIF>
					<CFIF FlagFormatting.FlagNameOnOwnLine is 1>
						</TR><TR>
					</CFIF>
					<TD VALIGN=TOP ALIGN=LEFT><!--- <FONT FACE="Arial, Chicago, Helvetica" SIZE="2"> --->
				</CFOUTPUT>
			</cfif>

			<CFOUTPUT>
				<!--- END: 2013-04-24 NYB Case434585 --->

				<cfif FlagFormatting.displayas is "memo">
					<cfparam name="cols" default="40">
					<cfparam name="rows" default="3">
					<textarea readonly cols="#cols#" rows="#rows#" wrap="VIRTUAL" id="#typeName#_#FlagID#_#thisentity#">#HTMLEditFormat(trim(getFlagData.Data))#</textarea>
				<cfelseif FlagFormatting.displayas is "grid">
					<cf_complexflaggrid FlagID = #flagid# flagDataQuery = #getFlagData# entityid = #thisentity# method = "view" attributeCollection = #flagFormatting# >
				<cfelse>
					<!---
						<CFIF getFlagData.RecordCount IS NOT 0>
							<cfif not isDEfined("getFlagData.data")><!--- WAB 2007-01-18 temporary hack for wddx problem --->
								<cfdump var="#getFlagData#">
							<cfelse>
								#getFlagData.Data#
							</cfif>

						<CFELSE>
							- phr_sys_NoData -
						</CFIF>
					 --->
					<cfif not isDEfined("getFlagData.data")><!--- WAB 2007-01-18 temporary hack for wddx problem --->
						<cfdump var="#getFlagData#">
					<cfelse>
						<CF_INPUT type="text" value="#getFlagData.Data#" id="#typeName#_#FlagID#_#thisentity#" SIZE="#flagFormatting.size#" disabled>
					</cfif>
				</CFIF>
				<!--- 2013-04-24 NYB Case434585 added if --->
				<cfif not flagGroupFormatting.noFlagTable><BR></cfif>
				<CFIF trim(descriptionphrasetextid) is not "">(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)<CFELSE><CFIF #Trim(Description)# IS NOT "">(<I>#htmleditformat(description)#</I>)</cfif></CFIF>
				<!--- START: 2013-04-24 NYB Case434585 added if, output etc --->
			</CFOUTPUT>
			<cfif not flagGroupFormatting.noFlagTable>
				<CFOUTPUT><!--- </FONT> ---></TD></TR></CFOUTPUT>
			</cfif>
			<!--- END: AJC 2008/05/13 Trend Nabu Issue 175: Hard coded Screen formatting needs changing --->
		<cfelse>
			<cfoutput>
				<label class="flagAttributeLabel">
				<CFIF FlagFormatting.NoFlagName is not 1>
					<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
						phr_#htmleditformat(trim(namephrasetextid))#
					<CFELSE>
						#htmleditformat(Name)#
					</cfif>
				</CFIF>
				</label>
				<cfif FlagFormatting.displayas is "memo">
					<cfparam name="cols" default="40">
					<cfparam name="rows" default="3">
					<textarea class="form-control" readonly cols="#cols#" rows="#rows#" wrap="VIRTUAL" id="#typeName#_#FlagID#_#thisentity#">#HTMLEditFormat(trim(getFlagData.Data))#</textarea>
				<cfelseif FlagFormatting.displayas is "grid">
					<cf_complexflaggrid class="form-control" FlagID = #flagid# flagDataQuery = #getFlagData# entityid = #thisentity# method = "view" attributeCollection = #flagFormatting# >
				<cfelse>
					<cfif not isDEfined("getFlagData.data")><!--- WAB 2007-01-18 temporary hack for wddx problem --->
						<span class="form-control"><cfdump var="#getFlagData#"></span>
					<cfelse>
						<CF_INPUT class="form-control" type="text" id="#typeName#_#FlagID#_#thisentity#" value="#getFlagData.Data#" SIZE="#flagFormatting.size#" disabled>
					</cfif>
				</CFIF>
			</cfoutput>
		</cfif>
		</CFLOOP>
		<cfif request.relayFormDisplayStyle neq "HTML-div" and not flagGroupFormatting.noFlagTable><cfoutput></TABLE></cfoutput></cfif>
		<!--- END: 2013-04-24 NYB Case434585 added if, output etc --->
<CFELSE>
	<cfoutput><P>* No flag data for this Group</cfoutput>
</CFIF>


