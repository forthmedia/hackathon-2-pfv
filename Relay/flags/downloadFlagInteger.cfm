<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type 6 = integer --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
	SELECT
		CASE ISNULL(F.Name + Char(61) + CONVERT(varchar(15),IFD.Data),' ')
			WHEN ''
				THEN ' '
			ELSE
				ISNULL(F.Name + Char(61) + CONVERT(varchar(15),IFD.Data),' ')
		END
		AS rowdata

	FROM FLAG F
	LEFT JOIN IntegerFlagData IFD ON IFD.FlagID = F.FlagID AND IFD.EntityID = <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
	WHERE F.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.rowdata, delim)>
</CFIF>
