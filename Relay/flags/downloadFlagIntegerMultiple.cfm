<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type  = integerMultiple 

Author: WAB
Date: 2001-02-07

		2007/09/06   WAB added id in brackets after name of entity (if flag joined to an entity)



--->

<cfif not isDefined("getFlagGroup#frmFlagGroupID#")>

	<CFQUERY NAME="getFlagGroup#frmFlagGroupID#" datasource="#application.sitedatasource#">
	select 
		f.name, 
		f.flagid, 
		f.linksToEntityTypeID,
		fet.tablename,
		fet.nameExpression,
		fet.UniqueKey 
	FROM 
		Flag As f
			left outer join
		flagEntityType 	 fet
			on fet.entityTypeID = f.linksToEntityTypeID 
	WHERE 
		f.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by 
		f.orderingIndex, f.name
	</cfquery>

	<cfset getFlagGroup = evaluate("getFlagGroup#frmFlagGroupID#")>
		
<cfelse>

	<cfset getFlagGroup = evaluate("getFlagGroup#frmFlagGroupID#")>
	
</cfif>




<CFSET flagsQuery = "">

<cfloop query="getFlagGroup">

	<CFQUERY NAME="getFlagData" datasource="#application.sitedatasource#">
	<!--- Chr() in Access, Char() in SQL Server--->
	SELECT 
		f.Name,
		<cfif getFlagGroup.linksToEntityTypeID is "">
		fd.Data as data
		<cfelse>
		#preserveSingleQuotes(getFlagGroup.nameExpression)# + ' (' + convert(varchar(10),data) + ')' as data     <!--- WAB 2007/09/06 added the id in brackets next to the name --->
		</cfif>
	FROM 
		Flag As f 
			inner join
		IntegerMultipleFlagData As fd
			on fd.FlagID = f.FlagID
		<cfif getFlagGroup.linksToEntityTypeID is not "">
			inner join
		#getFlagGroup.tablename# as t
			on fd.data = t.#getFlagGroup.uniqueKey#	
		</cfif>	
	WHERE 
		fd.EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and f.flagid =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	<cfif getFLagData.recordCount is not 0>
		<CFSET flagsQuery = listappend(flagsQuery, ValueList(getFlagdATA.data, ','), delim)>
	<cfelse>
		<CFSET flagsQuery = listappend(flagsQuery, " ", delim)><!--- blank is needed otherwise colums go wrong if there is no item with data in whole table --->
	</cfif>
<!--- 	<CFSET flagsQuery = ValueList(getFlags.data, delim)> --->
	
</cfloop>

