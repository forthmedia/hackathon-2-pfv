<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type 2 = Checkbox --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
	SELECT
		CASE ISNULL(f.Name,' ')
			WHEN ''
				THEN ' '
			ELSE
				ISNULL(f.Name,' ')
		END
		AS Name
	FROM Flag As f
	LEFT JOIN BooleanFlagData As fd ON fd.FlagID = f.FlagID AND fd.EntityID = <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
	WHERE f.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.Name, delim)>
</CFIF>
