<!--- �Relayware. All Rights Reserved 2014 --->
<!--- HiddenFlagText --->

<!--- 
Quick addition to the flag system which allows flags to be set/ unset behind the scenes on forms
Only work within the context of a screen definition
Set method = 'hidden' and evalstring = value required

Author: WAB
		2004-10-06 created from hiddenflagcheckbox.cfm 


 --->


<CFSETTING enablecfoutputonly="yes"> 

<CFINCLUDE TEMPLATE="qryFlag.cfm">

<cfloop query="getFlags"> 
	<cfinclude template="qryFlagData.cfm">
	<CFOUTPUT>
	<CFIF thisevalString is not "">
		<CF_INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#" VALUE="#thisevalString#">
		<CF_INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="#getFlagData.Data#">
	<cfelse>
		<!--- this won't have much affect! old and new data the same--->
		<CF_INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#" VALUE="#getFlagData.Data#">
		<CF_INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="#getFlagData.Data#">
	</cfif>
	</cfoutput>
	<cfset flaglist = flaglist & ",#typeName#_#FlagID#">
</cfloop>
	


<CFSETTING enablecfoutputonly="no"> 