<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
IH	2012-09-18	Case 430433 fix formatting issue with nested tables
WAB 2014-01-21 CASE 438662 Code not reacting to displayAs set at flag level. Use FlagFormatting.displayAs rather than plain displayAs
2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Spacing issue between label and texboxes when merging location records
WAB 2015-06-22 FIFTEEN-384 Add time support and local time support to date flags
2016-03-09 	WAB	 PROD2016-684 Added support for divLayout 
--->

<!--- Edit Flags of type 4 = Date --->
<!--- 2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Added labelTDwidth --->
<cfparam name="labelTDwidth" type="numeric" default="300">
<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>

	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput><TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2" BGCOLOR="white"<CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>></cfoutput>
	</cfif>
	
	<CFLOOP QUERY="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
	
		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		
		<CFOUTPUT>
			<cfif request.relayFormDisplayStyle neq "HTML-div"><TR></cfif>
				<CFIF flagFormatting.NoFlagName eq 0>
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						<!--- 2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Added labelTDwidth --->
						<TD VALIGN=TOP ALIGN=LEFT width="#labelTDwidth#">
					<cfelse>
						<label <cfif divLayout is "horizontal">class="col-xs-12 row flagAttributeLabel"</cfif>>
					</cfif>
						<CFIF trim(namephrasetextid) is not "">  
							phr_#htmleditformat(trim(namephrasetextid))#
						<CFELSE>
							#htmleditformat(Name)#
						</cfif>
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						</TD>
					<cfelse>
						</label>
					</cfif>
				</CFIF>
				<CFIF FlagFormatting.FlagNameOnOwnLine is 1>
					<cfif request.relayFormDisplayStyle neq "HTML-div"></TR>
					<TR></cfif>
				</CFIF>
				<cfif request.relayFormDisplayStyle neq "HTML-div"><TD VALIGN=TOP ALIGN=LEFT></cfif>
				
					<CFIF getFlagData.RecordCount IS NOT 0>
						<cfset value = DateFormat(getFlagData.Data)>
					<cfelse>
						<cfset value = "">
					</CFIF>
					
					<!--- WAB 2004-10-13 this bit needs the form name.  I think that we are always being called within
					cf_ascreenitem in which case thisFormName should be set but just in case this is not true then
					default back to flagForm
					WAB 2014-01-21 CASE 438662 Change to use FlagFormatting.displayAs rather than displayAs
					WAB 2015-06-22 FIFTEEN-384 Add time support, pass flagFormatting to cf_relaydatefield for localtime support
					  --->

					<CFPARAM name="thisFormName" default = "flagform">
					<cfparam name="flagFormatting.showTime" default="false">
					<cfparam name="flagFormatting.showAsLocalTime" default="#flagFormatting.showTime#">

					<cfif flagformatting.displayas is "calendar">
						<CF_relayDateField	
								currentValue="#getFlagData.Data#"
								thisFormName="#thisFormName#"
								fieldName="#typeName#_#FlagID#_#thisentity#"
								anchorName="anchor_#FlagID#_#thisentity#"
								attributeCollection = #flagFormatting#	
								helpText=""
							>
					<cfelse>
							<cfparam name="requiredMessage" default = ""> <!--- WAB 2007-02-12 will be defined in used in screens, but just in case called from anywhere else --->
							<cfparam name="requiredLabel" default = "">
							<cfif requiredMessage is "">
								<cfif requiredLabel is "">
									<cfset requiredLabel = "#Name#">
								</cfif>
								<cfset requiredMessage = "phr_Screen_EnterAValueFor #requiredLabel#">
							</cfif>
							
							<CF_relayDateDropDowns
								currentValue="#getFlagData.Data#"
								fieldName="#typeName#_#FlagID#_#thisentity#"
								attributeCollection = "#flagformatting#"
								required = #required#
								requiredMessage = #requiredMessage#
							>
					</cfif>

					<cfif flagFormatting.showTime>
						<CF_relayTimeField	
								currentValue="#getFlagData.Data#"
								fieldName="#typeName#_#FlagID#_#thisentity#"
								attributeCollection = #flagFormatting#	
							>
						
					</cfif>
						
					<!--- <INPUT TYPE="text" NAME="#typeName#_#FlagID#_#thisentity#" VALUE="<CFIF getFlagData.RecordCount IS NOT 0>#DateFormat(getFlagData.Data)#</CFIF>" SIZE=12><BR><FONT SIZE=2>
					<CFIF trim(descriptionphrasetextid) is not ""><CFIF #Trim(evaluate("phr_#descriptionphrasetextid#"))# IS NOT "">(<I>#evaluate("phr_#descriptionphrasetextid#")#</I>)</CFIF><CFELSE><CFIF #Trim(Description)# IS NOT "">(<I>#description#</I>)</cfif></CFIF>
					<INPUT TYPE="HIDDEN" NAME="#typeName#_#FlagID#_#thisentity#_eurodate" VALUE="You must enter the flag '#HTMLEditFormat(Name)#' date in 'dd-mmm-yyyy' format.">
					--->
					<INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>#createODBCDateTime(getFlagData.Data)#</cfif>"> 
				<cfif request.relayFormDisplayStyle neq "HTML-div"></TD>
			</TR>
				</cfif>
		</CFOUTPUT>

	</CFLOOP>
	<cfif request.relayFormDisplayStyle neq "HTML-div"><cfoutput></TABLE></cfoutput></cfif>
					
<CFELSE>

	<cfoutput><P>* No attribute data for this Profile</cfoutput>

</CFIF>


