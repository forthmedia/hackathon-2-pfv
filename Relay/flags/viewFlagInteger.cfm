<!--- �Relayware. All Rights Reserved 2014 --->
<!--- View Flags of type 6 = Integer

	2005-08-24 SWJ tidied up the screen added the labelTDwidth  to the label column and the function to resolve
				the valid values.

	WAB 2007/01/17  changes to do with flagGroupFormatting Parameters

	2011/09/19 PPB LID6630 use phr_sys_NoData
	WAB 2011/09/27	P-TND109 change view mode to use disabled controls rather than plain text
	NJH 2015/09/15	Added Id to input so that we can get a handle on it.
 --->

<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>

	<cfoutput>
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2">
	</cfif>
	<CFLOOP QUERY="getFlags">

		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		<cfparam name="labelTDwidth" type="numeric" default="300">
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<TR>
			<CFIF FlagFormatting.NoFlagName is not 1>
				<td width="#labelTDwidth#" align="left" valign="top">
					<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
						phr_#htmleditformat(trim(namephrasetextid))#
					<CFELSE>
						#htmleditformat(Name)#
					</cfif>
				</td>
			</CFIF>
			<CFIF FlagFormatting.FlagNameOnOwnLine is 1></TR><TR></CFIF>
		<cfelse>
			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
				<label class="flagAttributeLabel">
					phr_#htmleditformat(trim(namephrasetextid))#
				</label>
			<CFELSE>
				<label class="flagAttributeLabel">
					#htmleditformat(Name)#
				</label>
			</cfif>
		</cfif>
		<cfif request.relayFormDisplayStyle neq "HTML-div"><TD VALIGN=TOP ALIGN=LEFT></cfif>
			<input class="form-control" type="text" id="#typeName#_#FlagID#_#thisentity#" disabled value = "<cfif isDefined("getFlagData.data_name")>#htmleditformat(getFlagData.data_Name)#<cfelse>#htmleditformat(getFlagData.data)#</cfif>">
			<!---
			<CFIF getFlagData.RecordCount IS NOT 0>
				<!--- SWJ: 2005-08-24 Added a method to show the actual value for a valid value.  To show this you need to put
 						validValueEntity=person, validValueEntity=organisation or validValueEntity=location it will resolve
					the relevant data and show in the screen
						WAB: 14/0905
						Sorry - I have reworked this to run off the column in the flagtable called linkstoentityTypeID which I think will be better in the long term

					--->
					<cfif isDefined("getFlagData.data_name")>#getFlagData.data_Name#<cfelse>#getFlagData.data#</cfif>

					<!---
					now replaced with code that does this automatically in qryFlagdata/flag.cfc
					<CFIF getFlags.linkstoentityTypeID is not "">
					<cfquery name="getLookUpDetails" datasource="#application.siteDataSource#">
							<cfswitch expression="#getFlags.linkstoentityTypeID#">
									<cfcase value="0">
								select firstname + ' ' + lastname as value from person where personid = #getFlagData.Data#
								</cfcase>
								<cfcase value="1">
								select siteName as value from location where locationid = #getFlagData.Data#
								</cfcase>
								<cfcase value="2">
								select organisationName as value from organisation where organisationid = #getFlagData.Data#
								</cfcase>
								<cfcase value="3">
								select countryDescription as value from country where countryid = #getFlagData.Data#
								</cfcase>
								<cfdefaultcase >
								select #getFlagData.Data# as value
								</cfdefaultcase >
							</cfswitch>
						</cfquery>
						#getLookUpDetails.value#
				<cfelse>
					#getFlagData.Data#
				</cfif>
				 --->
 			<CFELSE>
				- phr_sys_NoData -
			</CFIF>
			--->
			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<BR>
				<CFIF trim(descriptionphrasetextid) is not "">
					(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)
				<CFELSE>
					<CFIF Trim(helpText) IS NOT "">(<I>#htmleditformat(helpText)#</I>)</cfif>
				</CFIF>
				</TD></TR>
			<cfelse>
				<CFIF trim(descriptionphrasetextid) is not "">
					<span class="help-block">phr_#htmleditformat(descriptionphrasetextid)#</span>
				<CFELSE>
					<CFIF #Trim(helpText)# IS NOT ""><span class="help-block">#htmleditformat(helpText)#</span></cfif>
				</CFIF>
			</cfif>

	</CFLOOP>
	<cfif request.relayFormDisplayStyle neq "HTML-div"></TABLE></cfif>
	</cfoutput>

<CFELSE>

	<cfoutput><p>* No profile attributes for this profile</p></cfoutput>

</CFIF>


