<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Edit Flags of type 5 = Text 

WAB 2004-12-08    replaced inclide of valid values with cf_displayvalidvalues
	2007/11/05 WAB  added support for masks
	2008/01/09	WAB	flagFormatting

	WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
	2011-11-28	NYB	LHID8224 changed to call in limitTextArea.cfm instead of limitTextArea.js - to translate js
	IH	2012-09-18	Case 430433 fix formatting issue with nested tables
	2014-11-12 WAB alter charsLeft code on textareas to use P tag and new javascript
2016-03-09 	WAB	 PROD2016-684 Removed class flagAttributeLabel - should be same as any other label
 --->




<!--- <cfset displayas = "memo"> --->

<cfinclude template="qryFlag.cfm">

<cfparam name="flagGroupFormatting.noFlagName" default = "0">
<cfparam name="flagGroupFormatting.noFlagTable" default = "#flagGroupFormatting.noflagname#">
<cfparam name="FlagNameOnOwnLine" default="0">
<!--- <cfparam name="flagGroupFormatting.displayas" default="memo"> --->
<cfparam name="requiredLabel" default="">


	
<cfif getflags.recordcount is not 0>


	<cfif not flagGroupFormatting.noFlagTable and request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput>
			<table width="90%" border="0" cellspacing="0" cellpadding="2"<CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>>
		</cfoutput>
	</cfif>
	
	<cfloop query="getFlags">

	
		<cfif trim(namephrasetextid) is not ""> 
			<cfset flagname = "phr_#trim(namephrasetextid)#">
		<cfelse>
			<cfset flagname = Name>
		</cfif>

	
		<cfset flaglist = flaglist & ",#typeName#_#FlagID#">
		<cfinclude template="qryFlagData.cfm">
		
		<cfif not flagGroupFormatting.noFlagTable>
			<cfoutput>
			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<tr>
				<cfif not flagGroupFormatting.noflagname >  
					<td valign=top align=left>
							#htmleditformat(flagName)#
					</td>
				</cfif>
				
				<cfif flagformatting.flagNameOnOwnline is 1>
					</tr>
					<tr>
				</cfif>
		
					<td <!--- align="right"  --->valign="top"> <!--- WAB removed the align right cause causing problems --->
			<cfelse>
				<!--- <div class="col-xs-12 col-sm-9"> --->
					<cfif not flagGroupFormatting.noflagname >
						<label class="<!--- flagAttributeLabel --->">#htmleditformat(flagName)#</label>
					</cfif>
			</cfif>
			</cfoutput>
		</cfif>
		
			<cfif isdefined ("flag_#trim(getFlags.FlagTextID)#_ValidValues")>
				<!--- set variables and include template to display validvaluelist --->
				<cfset formfield = "#typeName#_#FlagID#_#thisentity#">
				<cfset validvaluelist = evaluate('flag_#getflags.flagtextid#_validvalues')>
				<cfif requiredlabel is ""><cfset requiredLabel = flagname ></cfif>

				<CF_DisplayValidValues
					validvalues = "#validvaluelist#" 
					formFieldName =  "#typeName#_#FlagID#_#thisentity#"
					currentValue = "#valuelist(getFlagData.data)#"
					formname = "#thisFormName#"
					required = "#required#"
					requiredlabel = "#requiredlabel#"
					entityID = "#thisEntity#"
					countryid = #variables.countryid# 
					multiple = false
					displayAs = "#flagFormatting.displayAs#"
					allowCurrentValue = true <!--- WAB 2010/04/12 LID 3251 will now display current value even if not in the validvalues --->						
					attributeCollection = #flagFormatting#
					class="form-control"
					>

<!--- 				<cfinclude template="/screen/showValidValueList.cfm">						
 --->
			<cfelseif flagformatting.displayas is "memo">
				<cfparam name="flagformatting.cols" default="40">							
				<cfparam name="flagformatting.rows" default="3">
				<cfoutput>
				<!--- 	2008/04/18 GCC extended memo type to enable max length attribute to be used 
					 	NJH 2012/02/16 CASE 425074 include js file -
						2014-11-12 WAB alter charsLeft code on textareas to use P tag and new javascript
				--->

				<cf_includeJavascriptOnce template="/javascript/limitTextArea.js" translate="true">
				<textarea class="form-control" name="#typeName#_#FlagID#_#thisentity#"  onclick="doleft(#flagFormatting.maxlength#,this,'#typeName#_#FlagID#_#thisentity#charsleft')" onkeyup="doleft(#flagFormatting.maxlength#,this,'#typeName#_#FlagID#_#thisentity#charsleft')" cols="#flagformatting.cols#" rows="#flagformatting.rows#" wrap="VIRTUAL">#HTMLEditFormat(trim(getFlagData.Data))#</textarea>
				<input type="Hidden" name="#typeName#_#FlagID#_#thisentity#_orig" value="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getflags.flagtextid)#_default")>#htmleditformat(getFlagData.Data)#</cfif>">
				<cfif flagFormatting.maxlength GT 0>
					<p class="help-block" name="#typeName#_#FlagID#_#thisentity#charsleft" id="#typeName#_#FlagID#_#thisentity#charsleft"></p>
				</cfif>
				</cfoutput>
			<cfelseif flagformatting.displayas is "grid">
				<cfset flagList = listdeleteat (flagList,listfindnocase(flagList,"#typeName#_#FlagID#"))>
				<cfset flaglist = flaglist & ",flag_#FlagID#">  <!--- this is needed to get it processed as a complex flag --->

				<CFPARAM name="requiredMessage" default="">
				<cfif  requiredMessage is "">
					<cfset  requiredMessage = "phr_Sys_EnterARowFor #flagName#">
				</cfif>
				<cf_complexflaggrid FlagID = #flagid# flagDataQuery = #getFlagData# entityid = #thisentity# method = "edit" attributeCollection = #flagFormatting# required = "#required#" requiredMessage = "#requiredMessage#">
				<cfif complexflaggrid.javascriptVerification is not "">
					<cfset javascriptVerification = complexflaggrid.javascriptVerification & javascriptVerification >
				</cfif>
				
			<cfelse>
			
				<cfoutput>
				<!--- WAB: 2004-10-27 surely this <br> should not be here  --->
				<!--- 2008-03-27 - Michael Roberts then corrected by WAB 2008-04-11, trendNABU issue 150
					 there was a de(getFlagData.data) here which fell over when the data value had a # in it, so have removed it since didn't seem to be serving much purpose (although possibly it is remnant of an if statement which was supposed to pick up a default) --->
				<cf_input class="form-control" type="text" name="#typeName#_#FlagID#_#thisentity#" value="#getFlagData.Data#" SIZE="#flagFormatting.size#" MAXLENGTH="#flagFormatting.maxlength#" mask="#flagFormatting.mask#"> 
	
				<input type="Hidden" name="#typeName#_#FlagID#_#thisentity#_orig" value="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getflags.flagtextid)#_default")>#htmleditformat(getFlagData.Data)#</cfif>">
				</cfoutput>

			</cfif>
			
			<cfif trim(descriptionphrasetextid) is not "">
				<cfoutput><span class="help-block">phr_#htmleditformat(descriptionphrasetextid)#</span></cfoutput>
			<cfelse>
				<cfif #trim(helpText)# is not "">
					<cfoutput><span class="help-block">#htmleditformat(helpText)#</span></cfoutput>
				</cfif>
			</cfif>
			
		<cfif not flagGroupFormatting.noFlagTable>

			<cfoutput>
			<cfif request.relayFormDisplayStyle neq "HTML-div">
					</td>
				</tr>
			<cfelse>
				<!--- </div> --->
			</cfif>
			</cfoutput>
		</cfif>	
	</cfloop>



			
	<cfif not flagGroupFormatting.noFlagTable and request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput>
			</table>
		</cfoutput>
	</cfif>
<cfelse>

	

</cfif>



