<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit

	getFlagGroup.FlagGroupID

 2006-02-08  WAB made mod to query to bring back phr_ in name if a namephrasetextid defined

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
	--->

<!--- Some Defaults --->



<CFSET EntityCountryList = "0,#variables.CountryID#">
<CFIF countryid is 0 or application.countryGroupsBelongedTo[variables.countryid] IS  "" >
<CFELSE>
	<CFSET EntityCountryList = listappend(EntityCountryList,application.countryGroupsBelongedTo[variables.countryid])>
</CFIF>

<!--- NJH 2016/09/23 PROD2016-2383 - show flaggroups even if expiry not set --->
<CFQUERY NAME="getFlagGroup" datasource="#application.sitedatasource#">
	SELECT FlagGroup.FlagGroupID, flagGroup.FlagTypeID, EntityTypeID, Flag.FlagID, Flag.linksToEntityTypeID,
	case when isNull(flaggroup.namePhrasetextID,'') <> '' then 'phr_' + flaggroup.namePhrasetextID else flaggroup.name end as flagGroupName,  <!--- WAB 2006-02-08 --->
	case when isNull(flag.namePhrasetextID,'') <> '' then 'phr_' + flag.namePhrasetextID else flag.name end as flagName ,
	flagType.Name as typename,flagType.dataTable as typedata
	FROM FlagGroup
		inner join Flag on Flag.FlagGroupID = FlagGroup.FlagGroupID
		inner join FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID
	WHERE FlagGroup.Active <> 0
	AND (FlagGroup.expiry is null or FlagGroup.Expiry > #CreateODBCDateTime(Now())#)
	AND FlagGroup.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND FlagGroup.Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
	AND FlagGroup.FlagGroupID =  <cf_queryparam value="#getParentFlagGroup.FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND Flag.FlagID  IN ( <cf_queryparam value="#valuelist(getParentFlagGroup.FlagID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

</CFQUERY>

<CFIF getFlagGroup.RecordCount IS NOT 0>

	<CFSET thisFlagGroup = getFlagGroup.FlagGroupID>
	<CFSET singleFlag = valuelist(getFlagGroup.FlagID)>
	<!--- NJH 2010/09/08 RW8.3 - removed code below for data in flagStructures
	replaced WAB 2010/09/09
	<CFSET typeName = ListGetAt(application.typeList,getFlagGroup.FlagTypeID)>
	<CFSET typeData = ListGetAt(application.typeDataList,getFlagGroup.FlagTypeID)>
	<cfset typeName = application. flagGroup[getFlagGroup.flagGroupID].flagType.name>
	<cfset typeData = application. flagGroup[getFlagGroup.flagGroupID].flagType.datatable>--->

	<cfset typeName = getFlagGroup.typeName>
	<cfset typeData = getFlagGroup.typeData>

	<cfset flagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(getFlagGroup.FlagGroupID,specialFormattingCollection)>
	<CFPARAM name="flagGroupFormatting.NoFlagGroupName" default="0">
	<CFPARAM name="flagGroupFormatting.NoTopFlagGroupName" default="0">
	<CFPARAM name="flagGroupFormatting.NoFlagName" default="0">
	<CFPARAM name="flagGroupFormatting.FlagNameOnOwnLine" default="0">

	<cfif not request.relayFormDisplayStyle neq "HTML-div">
		<cfif allcolumns eq 0>
			<div <cfif divLayout is "horizontal">class="col-xs-12 col-sm-9"</cfif>>
		<cfelse>
			<div <cfif divLayout is "horizontal">class="col-xs-12"</cfif>>
		</cfif>
	</cfif>
	<!--- typeName should not be 'Group' but page exists for this case --->
	<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">
	<cfif not request.relayFormDisplayStyle neq "HTML-div">
		</div>
	</cfif>
<CFELSE>

	Error - no live flag found for name '<CFOUTPUT>#htmleditformat(thisFieldTextID)#</CFOUTPUT>' with correct scope.

</CFIF>








