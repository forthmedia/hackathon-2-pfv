<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Edit Flags of type 3 = Radio Button

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
WAB 2008/04/10  code to allow setting of a radio to make other fields required
WAB 2008/06/02  modified above so that works with select box display
NYB 2008/08/09  P-SOP003 - added ability to SortOrder different options via orderType.  Accepts: SortOrder, Alpha or Random
WAB 2009/02/03  problem with iif statement in requiresValueInOtherFlag code
IH	2012-09-18	Case 430433 fix formatting issue with nested tables
2015/09/08 NJH 	Jira PROD2015-30 - enable flags/groups to be displayed/hidden based on flag being selected. Changed 'requiresValueInOtherFlag' to support a list of flagIds
2015/09/24 NJH	Jira PROD2015-84 - provide support to make boolean groups required as well.
2015-09-30 WAB 	Add an ID to the SELECT field
2015-10-13 WAB 	PROD2015-85 Alter <LABEL> tags so do not have nested INPUT, add FOR attribute (SB modified .css). Removed double column layout
					in passing removed some redundant code referencing wabstylesloaded!
2015-10-15 PYW	Add suppressOtherFlagStruct to supress flag for flag group whose display is dependent on showOtherFlagGroupStruct.
				Add otherDependentFlagsStruct to control flag for flag group whose display is dependent on showOtherFlagGroupStruct and another flag from
				some other flag group.
2015-11-25  ACPK    BF-15 Fixed alignment of 'Not Specified' Radio button
--->
<CFPARAM name="column" default="3">
<CFPARAM name="required" default="0">
<!--- <CFPARAM name="phr_notspecified" default= "not specified">  <!--- just in case translations haven't been done ---> --->
<CFPARAM name="nullText" default="phr_notspecified">
<CFPARAM name="shownull" default="true">

<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options --->
<CFPARAM name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->


<!---	WAB 2008/06/02
Used to collect together any flags which cause other flags to be required
Code to create relevant javascript is called at the end
(this stucture is populated both when doing checkboxes or selects)
--->
<cfset requiresValueInOtherFlagStruct = structNew()>
<cfset requiresValueInOtherFlagGroupStruct = structNew()>
<cfset showOtherFlagStruct = structNew()>
<cfset showOtherFlagGroupStruct = structNew()>
<!--- PYW 2015/10/15 --->
<cfset suppressOtherFlagStruct = structNew()>
<cfset otherDependentFlagsStruct = structNew()>


<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagList = flagList & ",#typeName#_#thisFlagGroup#">
	<CFSET fieldName = "#typeName#_#thisFlagGroup#_#thisentity#">
	<CFSET OrigSelectedFlagID = '0'>
	<CFSET c = 0>

	<CFIF flagGroupFormatting.DisplayAs IS NOT "Select">
		<CFSET notSpecified = " CHECKED">
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<cfoutput>
				<TABLE BORDER="0" WIDTH="90%" CELLSPACING="0" CELLPADDING="2" BGCOLOR=""<CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>>
			</cfoutput>
		</cfif>
		<CFLOOP QUERY="getFlags">

			<CFINCLUDE TEMPLATE="qryFlagData.cfm">

			<CFIF getFlagData.RecordCount IS NOT 0>
				<CFSET notSpecified = "">
			</CFIF>

			<CFSET c = c + 1>
			<CFSET widtha = Round((100 / column))>

				<CFOUTPUT>
					<!--- KT 1999-11-25 added the or statement to get the <TR> when just one column of radio buttons, as in the Quiz section --->
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						<CFIF (c MOD column IS 1) OR (Column EQ 1)>
							<TR>
						</CFIF>
                        <TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">
					<cfelse>
						<!--- 2015-12-16 WAB BF-83 add support for the #column# attribute --->
						<cfswitch expression=#Column#>
							<cfcase value="1">
								<div class="radio col-xs-12 col-sm-12 col-md-12">
							</cfcase>
							<cfcase value="2">
								<div class="radio col-xs-12 col-sm-6 col-md-6">
							</cfcase>
							<cfcase value="4">
								<div class="radio-inline">
							</cfcase>
							<cfdefaultcase> <!--- 3 columns --->
								<div class="radio-inline">
							</cfdefaultcase>
						</cfswitch>

					</cfif>

						<CF_INPUT TYPE="radio" class="radio" NAME="#fieldName#" VALUE="#FlagID#" CHECKED="#iif( getFlagData.RecordCount IS NOT 0,true,false)#" id="#fieldname#_#flagid#">
						<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>
							<CFSET OrigSelectedFlagID = FlagID>
						</cfif>

						<label for="#fieldname#_#flagid#" >
							<CFIF trim(namephrasetextid) is not "">
								phr_#htmleditformat(trim(namephrasetextid))#&nbsp;
							<CFELSE>
								#htmleditformat(Name)#&nbsp;
							</cfif>
						</label>
					<cfif request.relayFormDisplayStyle neq "HTML-div">
                        </TD>
                        <CFIF c MOD column IS 0>
                            </TR>
                        </CFIF>
					<cfelse>
						</div>
					</cfif>
				</CFOUTPUT>


				<!--- WAB 2008/04/10
					Code to link a checkbox to a Text flag so that checking one makes the other required
 					Note identical to code in editFlagcheckbox and ought to be factorised

					WAB 2008/06/02
					Asked to make this code work for select boxes as well
					Therefore needed to do some sort of factorising, but still not brilliant since still version in editFlagcheckbox
					Structure is populated here and javascript is generated at bottom
				 --->

				<cfif structKeyExists (flagFormatting,"requiresValueInOtherFlag")>
					<cfset tempStruct = {requiresValueInOtherFlag=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlag#" index="otherflagID">
						<cfif application.com.flag.doesflagExist(otherflagID)>
							<cfset tempStruct.requiresValueInOtherFlag = listAppend(tempStruct.requiresValueInOtherFlag,otherflagID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlag neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagMessage")?flagFormatting.requiresValueInOtherFlagMessage:"">
						<cfset requiresValueInOtherFlagStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/24 Jira PROD2015-84 - provide support to make boolean groups required as well. --->
				<cfif structKeyExists(flagFormatting,"requiresValueInOtherFlagGroup")>
					<cfset tempStruct = {requiresValueInOtherFlagGroup=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlagGroup#" index="otherflagGroupID">
						<cfif application.com.flag.doesflagGroupExist(otherflagGroupID)>
							<cfset tempStruct.requiresValueInOtherFlagGroup = listAppend(tempStruct.requiresValueInOtherFlagGroup,otherflagGroupID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlagGroup neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagGroupMessage")?flagFormatting.requiresValueInOtherFlagGroupMessage:"">
						<cfset requiresValueInOtherFlagGroupStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/09 show other flags/groups when flag is selected --->
				<cfif structKeyExists (flagFormatting,"showOtherFlagsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagsWhenSelected#" index="otherFlagID">
						 <cfif application.com.flag.doesflagExist(otherFlagId)>
							 <cfif not structKeyExists(showOtherFlagStruct,flagID)>
								 <cfset showOtherFlagStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagStruct[flagID] = listAppend(showOtherFlagStruct[flagID],otherFlagID)>
						</cfif>
					</cfloop>
				</cfif>
				<cfif structKeyExists (flagFormatting,"showOtherFlagGroupsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagGroupsWhenSelected#" index="otherFlagGroupID">
						 <cfif application.com.flag.doesFlagGroupExist(otherFlagGroupID)>
							 <cfif not structKeyExists(showOtherFlagGroupStruct,flagID)>
								 <cfset showOtherFlagGroupStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagGroupStruct[flagID] = listAppend(showOtherFlagGroupStruct[flagID],otherFlagGroupID)>
						</cfif>
					</cfloop>
				</cfif>
				<!--- Begin PYW 2015/10/15 --->
				<cfif structKeyExists (flagFormatting,"suppressOtherFlagsWhenSelected")>
					<cfloop list="#flagFormatting.suppressOtherFlagsWhenSelected#" index="otherFlagID">
						 <cfif not structKeyExists(suppressOtherFlagStruct,flagID)>
							 <cfset suppressOtherFlagStruct[flagID] = "">
						</cfif>
						<cfset suppressOtherFlagStruct[flagID] = listAppend(suppressOtherFlagStruct[flagID],otherFlagID)>
					</cfloop>
				</cfif>
				<cfif structKeyExists (flagFormatting,"otherDependentFlagsWhenSelected")>
					<cfloop list="#flagFormatting.otherDependentFlagsWhenSelected#" index="otherFlagID">
						 <cfif not structKeyExists(otherDependentFlagsStruct,flagID)>
							 <cfset otherDependentFlagsStruct[flagID] = "">
						</cfif>
						<cfset otherDependentFlagsStruct[flagID] = listAppend(otherDependentFlagsStruct[flagID],otherFlagID)>
					</cfloop>
				</cfif>
				<!--- End PYW 2015/10/15 --->
		</CFLOOP>

		<CFSET c = c + 1>
		<CFOUTPUT>
			<!--- KT 1999-11-25 added the or statement to get the <TR> when just one column of radio buttons, as in the Quiz section --->
			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<CFIF (c MOD column IS 1) OR (Column EQ 1)>
					<TR>
				</CFIF>
			</cfif>
			<CFIF Required is 0 and (showNull is "" or showNull)> <!--- WAB 2006-10-17 added showNull --->
				<cfif request.relayFormDisplayStyle neq "HTML-div">
					<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%"> <!--- 2015-11-25  ACPK    BF-15 Fixed alignment of 'Not Specified' Radio button --->
				<cfelse>
					<div class="radio-inline">
				</cfif>

					<INPUT TYPE="radio" class="radio" NAME="#fieldName#" VALUE="0"#notSpecified# id="#fieldName#_notspecified">
					<label for = "#fieldName#_notspecified">

					#htmleditformat(nullText)# &nbsp;
					</label>
				<cfif request.relayFormDisplayStyle neq "HTML-div">
					</TD>
				<cfelse>
					</div>
				</cfif>
			</cfif>
				<CF_INPUT TYPE="Hidden" NAME="#fieldName#_orig" ID = "#fieldName#_orig" VALUE="#OrigSelectedFlagID#">

			<cfif request.relayFormDisplayStyle neq "HTML-div">
					<CFIF c MOD column IS 0>
						</TR>
					</CFIF>

					<CFIF c MOD COLUMN IS NOT 0>
						<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))#>&nbsp;<BR></TD></TR>
					</CFIF>

				</TABLE>
			</cfif>
		</CFOUTPUT>

	<CFELSE>

		<CFSET notSpecified = " SELECTED">
		<CFOUTPUT>
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<table width="90%" border="0" cellspacing="0" cellpadding="2">
			<tr><td>
		</cfif>
			<SELECT class="form-control" ID="#fieldName#" NAME="#fieldName#" <CFIF isdefined("onChange") and onchange is not "">onchange = "#htmleditformat(onChange)#"</cfif>>
				<!--- 2007/03/22 SAS REmoved 0 from value to ensure you have to choose something --->
				<cfif required>
					<cfset optionValue = "">
				<cfelse>
					<cfset optionValue = "0">
				</cfif>
				<CFIF showNull is true><OPTION VALUE="#optionValue#" >#htmleditformat(nullText)# </cfif>		<!--- </CFOUTPUT> --->
				<CFLOOP QUERY="getFlags">
					<CFINCLUDE TEMPLATE="qryFlagData.cfm">
						<!--- <CFOUTPUT> --->
						<OPTION VALUE="#FlagID#" <CFIF getFlagData.RecordCount IS NOT 0> SELECTED 	<CFSET notSpecified = ""></CFIF>>
							<CFIF trim(namephrasetextid) is not "">
								phr_#htmleditformat(trim(namephrasetextid))#
							<CFELSE>
								#htmleditformat(Name)#
							</cfif>
						<!--- </CFOUTPUT> --->

					<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>
						<CFSET OrigSelectedFlagID = FlagID>
					</cfif>
				<!---	WAB 2008/06/02
					check for requiresValueInOtherFlag
				 --->
				<cfif structKeyExists (flagFormatting,"requiresValueInOtherFlag")>
					<cfset tempStruct = {requiresValueInOtherFlag=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlag#" index="otherflagID">
						<cfif application.com.flag.doesflagExist(otherflagID)>
							<cfset tempStruct.requiresValueInOtherFlag = listAppend(tempStruct.requiresValueInOtherFlag,otherflagID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlag neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagMessage")?flagFormatting.requiresValueInOtherFlagMessage:"">
						<cfset requiresValueInOtherFlagStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/24 Jira PROD2015-84 - provide support to make boolean groups required as well. --->
				<cfif structKeyExists(flagFormatting,"requiresValueInOtherFlagGroup")>
					<cfset tempStruct = {requiresValueInOtherFlagGroup=""}>
					<cfloop list="#flagFormatting.requiresValueInOtherFlagGroup#" index="otherflagGroupID">
						<cfif application.com.flag.doesflagGroupExist(otherflagGroupID)>
							<cfset tempStruct.requiresValueInOtherFlagGroup = listAppend(tempStruct.requiresValueInOtherFlagGroup,otherflagGroupID)>
						</cfif>
					</cfloop>
					<cfif tempStruct.requiresValueInOtherFlagGroup neq "">
						<!--- WAB 2009/02/03  problem with IIF statement if flagFormatting.requiresValueInOtherFlagMessage not defined --->
						<cfset tempStruct.requiresValueInOtherFlagMessage = structKeyExists(flagFormatting,"requiresValueInOtherFlagGroupMessage")?flagFormatting.requiresValueInOtherFlagGroupMessage:"">
						<cfset requiresValueInOtherFlagGroupStruct[flagID] = tempStruct>
					</cfif>
				</cfif>

				<!--- 2015/09/09 show other flags/groups when flag is selected --->
				<cfif structKeyExists (flagFormatting,"showOtherFlagsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagsWhenSelected#" index="otherFlagID">
						 <cfif application.com.flag.doesflagExist(otherFlagId)>
							 <cfif not structKeyExists(showOtherFlagStruct,flagID)>
								 <cfset showOtherFlagStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagStruct[flagID] = listAppend(showOtherFlagStruct[flagID],otherFlagID)>
						</cfif>
					</cfloop>
				</cfif>
				<cfif structKeyExists (flagFormatting,"showOtherFlagGroupsWhenSelected")>
					<cfloop list="#flagFormatting.showOtherFlagGroupsWhenSelected#" index="otherFlagGroupID">
						 <cfif application.com.flag.doesFlagGroupExist(otherFlagGroupID)>
							 <cfif not structKeyExists(showOtherFlagGroupStruct,flagID)>
								 <cfset showOtherFlagGroupStruct[flagID] = "">
							</cfif>
							<cfset showOtherFlagGroupStruct[flagID] = listAppend(showOtherFlagGroupStruct[flagID],otherFlagGroupID)>
						</cfif>
					</cfloop>
				</cfif>
				<!--- Begin PYW 2015/10/15 --->
				<cfif structKeyExists (flagFormatting,"suppressOtherFlagsWhenSelected")>
					<cfloop list="#flagFormatting.suppressOtherFlagsWhenSelected#" index="otherFlagID">
						 <cfif not structKeyExists(suppressOtherFlagStruct,flagID)>
							 <cfset suppressOtherFlagStruct[flagID] = "">
						</cfif>
						<cfset suppressOtherFlagStruct[flagID] = listAppend(suppressOtherFlagStruct[flagID],otherFlagID)>
					</cfloop>
				</cfif>
				<cfif structKeyExists (flagFormatting,"otherDependentFlagsWhenSelected")>
					<cfloop list="#flagFormatting.otherDependentFlagsWhenSelected#" index="otherFlagID">
						 <cfif not structKeyExists(otherDependentFlagsStruct,flagID)>
							 <cfset otherDependentFlagsStruct[flagID] = "">
						</cfif>
						<cfset otherDependentFlagsStruct[flagID] = listAppend(otherDependentFlagsStruct[flagID],otherFlagID)>
					</cfloop>
				</cfif>
				<!--- End PYW 2015/10/15 --->
				</CFLOOP>
			<!--- <CFOUTPUT> --->
			</SELECT><cfif request.relayFormDisplayStyle neq "HTML-div"><BR><BR></cfif>
			<CF_INPUT TYPE="Hidden" NAME="#fieldName#_orig" VALUE="#OrigSelectedFlagID#">
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			</td></tr>
			</table>
		</cfif>
		</CFOUTPUT>
	</CFIF>

<CFELSE>
	<cfoutput><P>* No flag data for this Group</cfoutput>
</CFIF>


<!--- WAB 2008/11/12  Bug 1327
Moved all code to do with JS for requiresValueInOtherFlag into another template
also copied the required code from screens\showscreenelement_flag and _flaggroup into this new template to fix bug 1327
--->
<cfinclude template = "jsVerification.cfm">




<!--- <CFSET Displayas = ""> WAB removed 2004-11-17 put in singleflag.cfm and flaggroup.cfm instead allows setting for a whole flaggroup to be defined in a screen, otherwise it resets itself after the first one --->

