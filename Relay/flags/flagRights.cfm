<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY NAME="getTopLevelFlags" datasource="#application.sitedatasource#">
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId
<CFIF flagMethod IS "edit">, 1 AS edit_view</CFIF>
FROM FlagGroup
WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID = 0
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
<CFIF flagMethod IS "search">
	AND Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<CFELSE>
	AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFIF>
AND ( <!--- removed by WAB - doesn't make sense - creator is likely to just be an administrator, shouldn't have automatic rights FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#OR --->
	 (#tmpFlagMethod#AccessRights = 0 AND FlagGroup.#tmpFlagMethod# <>0)
	OR (#tmpFlagMethod#AccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.#tmpFlagMethod# <>0)))


<CFIF flagMethod IS "edit">
UNION
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId,0
FROM FlagGroup
WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID = 0
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<!--- wab removed to match removal above AND FlagGroup.CreatedBy<>#request.relayCurrentUser.usergroupid# --->
AND ( (ViewingAccessRights = 0 AND FlagGroup.Viewing <>0)
	OR (ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Viewing <>0)))
AND ( (EditAccessRights = 0 AND FlagGroup.Edit = 0)
<!--- 	WAB: I think that this is wrong, needs a NOT EXISTS as below 
	OR (EditAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Edit =0)) --->
		OR (EditAccessRights <> 0
			AND NOT EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
				AND FlagGroupRights.Edit <> 0))

			)
</CFIF>
ORDER BY OrderingIndex, Name
</CFQUERY> 
