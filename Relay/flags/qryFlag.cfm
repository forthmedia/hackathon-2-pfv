<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Edit Flags query used for all Flag types 


Mods:
WAB 240200  Added flaggroup to query to have access to flaggrouptextid
WAB added showselectedfirst functionality
WAB 2007-02-06  P-FNL041 added handling for collection of formattingParameters 
WAB 2008/01/17  changes to do with flagGroupFormatting Parameters 
NYB 2008/08/09  P-SOP003 - added ability to SortOrder different options via orderType.  Accepts: SortOrder, Alpha or Random 
NYB 2009-03-03 Sophos Stargate 2 - fixed singleFlag bug - was throwing an error when you passed one

 --->


<CFPARAM name="showSelectedFirst" default="0">
<CFPARAM name="singleFlag" default="0">
<CFPARAM name="suppressFlag" default="0">

<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options-> --->
<CFPARAM name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->
<cfif not listContains("SortOrder,Alpha,Random", orderType)>
	<cfset orderType="SortOrder">  
</cfif>
<!--- <-P-SOP003 --->	

<!--- 2007-01-18 
	P-FNL041 
	WAB implementing structured way of passing formatting info around 
	we have 4 sources of formatting information
	1.  The specialformatting column of the screen definition - now passed around as specialformattingcollection
	2.  The formattingParameters column of the flag definition   (added by WAB around this time)
	3.  The formattingParameters column of the flagGroup definition   (added by WAB around this time)
	4.  Default values for this this type of flag - previously just set as individual variables in the edit/viewflagxxx.cfm file but will need to put in a structure which I will call flagGroupFormattingDefault

	So we:
	create a flagGroupFormatting structure by
	1. Taking the defaults for the flag group flagGroupFormattingDefault
	 1b .merging in settings from flagGroup.formattingParameters (overwriting any which appear in both)
	2. merging in settings from screendefinition
	
	
	When we loop through the individual flags and call qryFlagData we 
	
	create a flagFormattingStructure by
	1. Copying flagGroupFormatting structure
		If the individual flag has any formatting setting then we
	2.  merge in any settings from flag.formattingParameters into 
	3.  reapply settings from screendefinition
	this gives us flagFormatting structure
	
--->

<cfparam name = "flagGroupFormatting.showSelectedFirst" default= 0>
<cfparam name = "flagGroupFormatting.suppressFlag" default=  0>
<cfparam name = "flagGroupFormatting.noFlagName" default=  0>

<!--- NYB 2009-03-03 Sophos Stargate 2 - added ' and singleFlag IS NOT ""'--->
<cfset singleFlag = ListFirst(singleFlag)>

<CFQUERY NAME="getFlags" datasource="#application.siteDataSource#">
	SELECT f.currentCount, f.Description, f.FlagID, f.FlagTextID, fg.flagtypeid,f.helpText,
	<!--- f.Name,  ---> 
		case when isNull(f.namePhrasetextID,'') <> '' then 'phr_' + f.namePhrasetextID else f.name end as Name ,
		case when isNull(fg.namePhrasetextID,'') <> '' then 'phr_' + fg.namePhrasetextID else fg.name end as FlagGroupName,  
	f.namephrasetextid, f.descriptionphrasetextid, f.usevalidvalues, 
	f.lookup, fg.flagGroupTextID,f.usevalidvalues,f.linkstoentitytypeid,f.wddxstruct,f.formattingParameters,
	<CFIF orderType IS 1>		
		CASE WHEN EXISTS (SELECT 1 from #typeData#FlagData as fd WHERE fd.FlagID = f.FlagID AND EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > ) THEN 1 ELSE 0 END as SelectedIndex
	<CFELSE>
		1 as SelectedIndex
	</cfif>
	FROM Flag as f, FlagGroup as fg
	WHERE f.Active <> 0
	AND fg.FlagGroupID = f.FlagGroupID 
	AND f.FlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<!--- NYB 2009-03-03 Sophos Stargate 2 - added ' and singleFlag IS NOT ""'--->
		<CFIF singleFlag IS NOT 0 and singleFlag IS NOT "">AND f.FlagID  In ( <cf_queryparam value="#singleFlag#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</CFIF>
		<CFIF suppressFlag IS NOT 0 and suppressFlag IS NOT ""><cfif isNumeric(listFirst(suppressFlag))>AND f.FlagID  not In ( <cf_queryparam value="#suppressFlag#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )<cfelse>AND f.FlagTextID  not In ( <cf_queryparam value="#suppressFlag#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )</cfif></CFIF>
	ORDER BY <CFIF orderType IS "Alpha">FLAGGROUPNAME Asc<cfelse>SelectedIndex Desc</cfif>, f.OrderingIndex, f.Name
</CFQUERY>

<!--- P-SOP003 2008/08/09 NYB - added to give a Random display of Attributes/Flags-> --->
<CFIF orderType eq "Random">
	<CFSET qryLen =  getFlags.recordcount> 	
	<CFSET possNums =  ""> 	
	<cfloop index="i" from="1" to="#qryLen#">
		<CFSET possNums =  ListAppend(possNums,i,",")> 
	</cfloop>
	
	<cfloop index="i" from="1" to="#qryLen#">
		<CFSET randNum = ListGetAt(possNums, RandRange(1, (qryLen + 1) - i))>	
		<CFSET chgTest =  QuerySetCell(getFlags, "SelectedIndex", randNum, i)> 
		<CFSET possNums = ListDeleteAt(possNums, ListFind(possNums, randNum))>
	</cfloop>

	<CFQUERY NAME="getFlags" dbtype="query">
		select * from getFlags order by SelectedIndex
	</CFQUERY>
</CFIF>
<!--- <-P-SOP003 --->	

<cfset "getFlags_#thisFlagGroup#" = getFlags>  <!--- WAB 2006-03-01  give this query a unique name so can be referenced in showScreenElement_Flag/FlagGroup to do the javascript verification --->

<!--- reset defaults --->
<CFSET showSelectedFirst = 0>
<CFSET singleFlag = 0>
<CFSET suppressFlag = 0>

