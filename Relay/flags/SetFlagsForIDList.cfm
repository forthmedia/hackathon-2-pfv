<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		SetFlagIDList.cfm
Author:			SWJ
Date created:	28 June 2000

Description:	This template is used to set multiple flags for a given
				base entity (e.g. person, location or organisation)

It currently supports person and location entities and the following flag Types:
FlagTypeID	Name		DataTable
2			Checkbox	boolean
3			Radio		boolean
4			Date		date
5			Text		text
6			Integer		integer

To call this template you need to pass it the following variables
	List of entity ids in frmEntityID for which you are going to set the flags the user choses
	entity type in frmEntityTypeID where 1 = location and 0 = person

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-01-20		WAB  appears as if this template never finished, (no update procedures).  Added ability to set and unset flags
2001-03-22		WAB		modifications so that entityType does not have to be the same as the flagentityType
2001-04-04		WAB		fixed a bug
2005-04-23		WAB		Modified so that can be included in other templates parameters added for heading and detail and showing entitydropdown and where posts to
					Mod so that update is done by an include (rather than posting to that template)
2007-02-07	WAB		Added a cfcatch for valid values which would otherwise bring back errors - eg valid values which are location or organisation specific
2007/03/23	NJH		set entityID to be the list of entity ids so that we could use #entityID# in a valid value query.
					Also added the column entityID in the getEntityData query
2007/06/06	WAB		developed a bit more on the above- validvalue code can now accept countryid and entityid parameters, so now pass these in
2009/09/16	NJH		LID 2610 - changed getEntityData query to use left joins so that data for orgs was returned when the orgs had no people
2011/07/19 	PPB 	LEN024 - ProfileTask permission required
2012/05/11	IH		Case 428211 remove CFPARAM from #frmEntityID#
2015-09-23	ACPK	FIFTEEN-295 Prevent form submission when default value of dropdown menu selected
2015-11-12  ACPK    Replaced table with Bootstrap

Enhancements required:

1.  Needs to support valid value lists on both text and integer flags so when these are set in the flag definition they show
done in searchflaginteger (DONE)
2.  Add logic so that if an optional fgTextID variable is passed it will constrain the
	getFlags query so that only the specified flagGroup is shown.  We use flagTextID as this make the
	logic implmentation independent.  See the call in line 37 of data/organisationDropDown.cfm
	for an example. (DONE)
3.  The styles and layout needs tidying up.  The screen looks a mess. (DONE)
4. 	The default state should in my opinion hide the drop down in line 239 as this does not make sense to show this if you are
    working just on orgs.  Need to discuss this with Maxine.
5.  What does the contune button do - should the save button do the changes?
6.  When this form submits to setFlagIDListTask the feedback is sadly lacking. (DONE)

--->

<cfif refind("[^0-9,]", frmentityid)>  <!--- NJH 2012/08/01 - frmEntityID is passed through Javascript as well, so can't encrypt. If frmentityid is anything but a list of numbers --->
	<cf_checkFieldEncryption fieldNames="frmentityTypeid,frmentityid">
</cfif>

<CFPARAM NAME="frmEntityID" >
<CFPARAM NAME="frmEntityTypeID" >
<CFPARAM NAME="frmFlagEntityTypeID" default = "#frmEntityTypeID#" >
<CFPARAM NAME="fgTextID" default="">
<CFPARAM NAME ="showEntityDropDown" default = "true">
<CFPARAM NAME="heading" default = "" >
<CFPARAM NAME="detail" default = "" >
<CFPARAM NAME="postTo" default = "setFlagsForIDList.cfm" >  <!--- if posts to somewhere else, will need to include  setFlagForIDListTask.cfm --->

<cfif not application.com.login.checkInternalPermissions("ProfileTask","Level2")>		<!--- 2011/07/19 PPB LEN024 the dropdown menu option is not visible but this prevents someone copying the url and hitting the page directly --->
	<CF_ABORT>
</cfif>


<!--- WAB 2012-10-05  Case 431046 Did a cfsavecontent and moved output down so that message generated is below the header --->
<cfif isDefined("frmAction") and frmAction is "update">
	<cfsaveContent variable="feedback_HTML">
		<cfinclude template = "setFlagForIDListTask.cfm">
	</cfsavecontent>
</cfif>

<cf_title>Set Profile Attributes</cf_title>

<SCRIPT type="text/javascript">

<!--
	function verifyThisForm() {
		var form = document.FlagForm;
		var msg = "";
		var found = "no";
	if (msg != "") {
			alert("\nYou must provide the following information for this location:\n\n" + msg);
		} else {

			form.submit();
		}
	}


//-->

</SCRIPT>



<cfset request.relayFormJavaScriptsLoaded = "yes">

<!--- Get USER countries --->
<cfinclude template="/templates/qrygetcountries.cfm">

<CFQUERY NAME="getEntityData" datasource="#application.siteDataSource#">
	SELECT distinct
	<CFSWITCH EXPRESSION="#frmFlagEntityTypeID#">
		<CFCASE VALUE="0">
				p.firstName+' '+p.lastname AS Name, p.personID as entityID, l.countryid
		</CFCASE>
		<CFCASE VALUE="1">
				l.SiteName AS Name, l.locationID as entityID,  l.countryid
		</CFCASE>
		<CFCASE VALUE="2">
				OrganisationName AS Name, o.organisationID as entityID, o.countryid
		</CFCASE>
		<CFdefaultCASE >
				ERROR type #frmFlagEntityTypeID# not supported
		</CFdefaultCASE>

	</CFSWITCH>
			FROM organisation AS o with (noLock)
				#application.com.rights.getRightsFilterQuerySnippet(entityType="organisation",alias="o").join#
				<cfif frmFlagEntityTypeID lt 2>
				inner join location as l with (noLock)  on o.organisationid = l.organisationid
				#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l").join#
				</cfif>
				<cfif frmFlagEntityTypeID eq 0>
				inner join person as p with (noLock)  on l.locationid = p.locationid
				</cfif>
			WHERE 1=1
				AND
	<CFSWITCH EXPRESSION="#frmEntityTypeID#">
		<CFCASE VALUE="0">
			p.personid  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		</cfcase>
		<CFCASE VALUE="1">
			l.locationid  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		</cfcase>
		<CFCASE VALUE="2">
			o.organisationid  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
		</cfcase>
	</cfswitch>
</CFQUERY>

<!--- NJH 2007/03/23 set entityID to be the list of entity ids so that we could use
		#entityID# in a valid value query. Also added the column entityID in the getEntityData query --->
<cfset entityID = valueList(getEntityData.entityID)>

<!--- WAB 2007/06/06 get countries as well  --->
<CFQUERY NAME="getEntityCountries" dbtype="query">
select distinct Countryid from getEntityData
</CFQUERY>
<cfif getentityCountries.recordcount is 1>
	<cfset EntityCountryID = valuelist (getEntityCountries.countryid)>
<cfelse>
	<cfset EntityCountryID = 0>
</cfif>


<!--- Get list of all flags and groups --->


<!--- Find all Country Groups for these Countries --->
<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT CountryGroupID
	 FROM CountryGroup
	WHERE CountryMemberID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
</CFQUERY>

<CFSET UserCountryList = "0,#getCountryGroups.countryGroupID#">

<CFQUERY NAME="getFlags" datasource="#application.siteDataSource#">
	SELECT
		FG.FlagGroupID as ParentFlagGroupID,
		FG.Name AS ParentFlagGroupName,
		fg.OrderingIndex as ParentFlagGroupIndex,
		FG.FlagGroupID as ChildFlaggroupID,
		'' as ChildFlagGroupName,
		0 as childFlagGroupIndex,
		FG.FlagTypeID,
		f.FlagID,
		f.Name,
		f.Description,
		f.OrderingIndex as flagOrderingIndex,
		f.flagTextID,
		f.usevalidvalues,
		f.lookup

	FROM
		FlagGroup AS FG
			inner join
		Flag f on  fg.FlagGroupID = f.FlagGroupID and fg.parentFlagGroupID = 0
	WHERE
		fg.active =1
	AND	F.Active = 1
	AND FG.EntityTypeID =  <cf_queryparam value="#frmFlagEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND fg.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND (
		 (fg.EditAccessRights = 0 AND fg.Edit <>0)
				OR
		 (fg.EditAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights fgr
							WHERE fg.FlagGroupID = fgr.FlagGroupID
							AND fgr.UserID in (#request.relaycurrentuser.usergroups# )
							AND fgr.edit <>0)
		 )
		)
	<cfif len(fgTextID)>
	AND fg.flagGroupTextID  in ( <cf_queryparam value="#fgTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )


	</cfif>
union

	SELECT

		FGP.FlagGroupID,
		FGP.Name ,
		fgp.OrderingIndex,
		FG.FlagGroupID,
		FG.Name AS FlagGroupName,
		fg.OrderingIndex,
		FG.FlagTypeID,

		f.FlagID,
		f.Name,
		f.Description,
		f.OrderingIndex,
		f.flagTextID,
		f.usevalidvalues,
		f.lookup
	FROM
		flaggroup as FGP
			inner join
		FlagGroup AS FG on fgp.flagGroupid = fg.parentFlagGroupID and fgp.parentFlagGroupID = 0
			inner join
		Flag f on  fg.FlagGroupID = f.FlagGroupID
	WHERE
		fg.active =1
	AND	F.Active = 1
	AND FG.EntityTypeID =  <cf_queryparam value="#frmFlagEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND fg.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND fgp.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND (
		 (fg.EditAccessRights = 0 AND fg.Edit <>0)
				OR
		 (fg.EditAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights fgr
							WHERE fg.FlagGroupID = fgr.FlagGroupID
							AND fgr.UserID in (#request.relaycurrentuser.usergroups# )
							AND fgr.edit <>0)
		 )
		)
	AND (
		 (fgp.EditAccessRights = 0 AND fgp.Edit <>0)
				OR
		 (fgp.EditAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights fgr
							WHERE fgp.FlagGroupID = fgr.FlagGroupID
							AND fgr.UserID in (#request.relaycurrentuser.usergroups# )
							AND fgr.edit <>0)
		 )
		)

	<cfif len(fgTextID)>
	AND fg.flagGroupTextID  in ( <cf_queryparam value="#fgTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	</cfif>


	ORDER BY ParentFlagGroupIndex, ParentFlagGroupName, ChildFlagGroupIndex, ChildFlagGroupName, flagOrderingIndex, f.Name
</CFQUERY>

<cfif getEntityData.recordcount is 0 >
	<p>Sorry, no items were selected</p>
<cfelseif getFlags.recordCount is 0 >
	<p>Sorry, there are no flags for that item</p>
<cfelse>

	<cfif heading is not "">
		<cfset pageTitle = heading>
	<cfelse>
	     <cfset pageTitle = "Manage profile values you want to set/unset">
	</cfif>
	<div class="row">
		<div class="col-xs-12">
			<cf_RelayNavMenu pageTitle="#pageTitle#" thisDir="flags">
				<cf_RelayNavMenuItem MenuItemText="Phr_Sys_Save" CFTemplate="JavaScript:verifyThisForm();">
			</cf_RelayNavMenu>
	    </div>
	</div>


	<cfif isDefined("variables.feedback_html")>
		<cfoutput>#feedback_html#</cfoutput>
	</cfif>

		<cfoutput>
			<div class="row">
		       <div class="col-xs-12">
				<cfif detail is not "">#detail#<cfelse><p>Below is a list of profile data.  For each profile you have the option to set or unset
					the current values for the records you selected prior to launching this screen (either individually, in a selection or because they are already flagged).
					Find the value(s) that you want to set/unset.  Choose
					set or unset for one or more values and then click the Save link above. </p><p>When you click Save all of the records you have selected will be
					updated to reflect what you chose.</p>	</cfif>
				</div>
			</div>
		</cfoutput>

	<cfif len(fgTextID) and showEntityDropDown>
		<form class="form-horizontal" name="myForm" method="post" action="setFlagsForIDList.cfm">
		<cfoutput>
				<cf_encryptHiddenFields>
				<CF_INPUT TYPE="HIDDEN" NAME="frmentityid" VALUE="#frmentityid#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmentityTypeid" VALUE="#frmentityTypeid#">
				</cf_encryptHiddenFields>
				<P>The following
					<cf_displayValidValues
						formFieldName = "frmFlagEntityTypeID"
						currentValue = "#frmFlagEntityTypeID#"
						<!--- 2015-09-23	ACPK	FIFTEEN-295 Prevent form submission when default value of dropdown menu selected --->
						onChange = "if (this.value != '') {document.myForm.submit();}"
						validValues = "0-Person,1-Location,2-Organisation"
						delim1 = "-"
					>

				records will have the profile attributes you specify set/unset:</P>
				<p>(Use the select box to choose records related to the list selected to profile)</p>
				<P>
				<cfset maxRows = 25>
				<cfloop query="getEntityData" startrow="1" endrow="#maxRows#">
					#htmleditformat(Name)# <BR>
				</cfloop>
				<cfif getEntityData.RecordCOunt GT maxRows>
					and #evaluate(getEntityData.RecordCOunt - maxrows)# more records ...
				</cfif>
		</cfoutput>
		</form>
	</cfif>


	<cfoutput>
	<FORM CLASS="form-horizontal" ACTION="#postTo#" METHOD="POST" NAME="FlagForm">
			<INPUT TYPE="HIDDEN" NAME="frmaction" VALUE="update">
			<cf_encryptHiddenFields>
			<CF_INPUT TYPE="HIDDEN" NAME="frmentityid" VALUE="#frmentityid#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmentityTypeid" VALUE="#frmentityTypeid#">
			</cf_encryptHiddenFields>
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagEntityTypeid" VALUE="#frmFlagEntityTypeid#">
			<CF_INPUT TYPE="HIDDEN" NAME="fgTextID" VALUE="#fgTextID#">
		</cfoutput>

	<CFOUTPUT QUERY="getFlags" GROUP="ParentFlagGroupName">
		<h3>#htmleditformat(ParentFlagGroupName)#</h3>
		<!--- <tr><td colspan="2">&nbsp;</td></tr> --->
		<CFOUTPUT GROUP="ChildFlagGroupName">		
			<CFIF ChildFlagGroupName is not "">
				<h4>#htmleditformat(ChildFlagGroupName)#</h4>
			</CFIF>
			<CFOUTPUT>
			   <div class="form-group">
	                     <div class="col-xs-12">
	
			<!--- FlagTypeID	Name		DataTable
					2			Checkbox	boolean
					3			Radio		boolean
					4			Date		date
					5			Text		text
					6			Integer		integer
			--->
					<CFSWITCH EXPRESSION=#FlagTypeID#>
					    <cfcase VALUE="2">
					    	<strong>#htmleditformat(Name)#</strong>
					    	<CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF>
					    	<div class="radio"><label><CF_INPUT TYPE="radio" NAME="Flag_#FlagID#" VALUE="1">Set</label></div>
					    	<div class="radio"><label><CF_INPUT TYPE="radio" NAME="Flag_#FlagID#" VALUE="0">Unset</label></div>
					    	<div class="radio"><label><CF_INPUT TYPE="radio" NAME="Flag_#FlagID#" VALUE="">Clear</label></div>
					    </cfcase>
					    <cfcase VALUE="3">
					    	<strong>#htmleditformat(Name)#</strong>
					    	<CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF></td>
					    	<div class="radio"><label><CF_INPUT TYPE="radio" NAME="Radio_#ChildFlagGroupID#" VALUE="#flagid#">This one</label></div>
					    	<div class="checkbox"><label><CF_INPUT TYPE="checkbox" NAME="Flag_#FlagID#" VALUE="0">Unset</label></div>
					    </cfcase>
					    <cfcase VALUE="4">
					    	<label for="Flag_#FlagID#">#htmleditformat(Name)#</label>
					    	<CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF>
					    	<CF_INPUT ID="Flag_#FlagID#" CLASS="form-control" TYPE="text" NAME="Flag_#FlagID#">
					    	<CFIF trim(Description) NEQ ''><I> Use format DD-MMM-YYYY (#htmleditformat(Description)#)</I></CFIF>
					    </cfcase>
					    <cfcase VALUE="5">
					    	<label for="Flag_#FlagID#">#htmleditformat(Name)# </label>
					    	<CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF>
							<CFIF useValidValues eq 1>
							<!--- display validvaluelist --->
								<cftry>
									
									<CF_displayvalidvalues
									    id = "Flag_#FlagID#"
									    class = "form-control"
										formfieldname = "Flag_#FlagID#"
										validfieldname =  "flag_#getFlags.FlagTextID#"
										currentvalue = ""
										entityID = "#entityID#"
										countryID = "#EntityCountryID#"
										>
	
									<cfcatch>
										<cfif cfcatch.message contains "Valid Value Error">
											<cfoutput>Values not available</cfoutput>
										<cfelse>
											<cfrethrow>
										</cfif>
									</cfcatch>
								</cftry>
							<CFELSE>
							    <CF_INPUT ID="Flag_#FlagID#" CLASS="form-control" TYPE="text" NAME="Flag_#FlagID#" >
							    <CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF>
							</cfif>
					    </cfcase>
					    <cfcase VALUE="6">
					    	<label for="Flag_#FlagID#">#htmleditformat(Name)#</label>
	
							<CFIF useValidValues eq 1>
								<!--- display validvaluelist --->
	
									<cftry>
	
										<CF_displayvalidvalues
										    id = "Flag_#FlagID#"
										    class = "form-control"
											formfieldname = "Flag_#FlagID#"
											validfieldname =  "flag_#getFlags.FlagTextID#"
											currentvalue = ""
											entityID = "#entityID#"
											countryID = "#EntityCountryID#"
	
											>
	
										<cfcatch>
											<cfif cfcatch.message contains "Valid Value Error">
												<cfoutput>Values not available</cfoutput>
											<cfelse>
												<cfrethrow>
											</cfif>
										</cfcatch>
									</cftry>
	
	
							<CFELSE>
								<CF_relayValidatedField
								    id = "Flag_#FlagID#"
	                                         class = "form-control"
									fieldName="Flag_#FlagID#"
									currentValue=""
									charType="numeric"
									size="10"
									maxLength="10"
									helpText=""
								>
							</cfif>
	
							<!--- <INPUT TYPE="text" NAME="Flag_#FlagID#"> <CFIF trim(Description) NEQ ''><I>(#Description#)</I></CFIF> --->
	
	
					    </cfcase>
						<CFDEFAULTCASE>
							<p>
	       						<strong>#htmleditformat(Name)#</strong>
	   							<CFIF trim(Description) NEQ ''><I>(#htmleditformat(Description)#)</I></CFIF> - flag type not supported
							</p>
					    </CFDEFAULTCASE>
	
					</CFSWITCH>
					</div>
				</div>
			</CFOUTPUT>
			<cfif FlagTypeID is 3>
				<div class="form-group">
				    <div class="col-xs-12">
				      <div class="radio">
				        <label>
				            <CF_INPUT TYPE="radio" NAME="Radio_#ChildFlagGroupID#" VALUE="">Clear the above
				        </label>
				      </div>
				    </div>
				 </div>
			</cfif>
		</CFOUTPUT>
	</CFOUTPUT>


<!--- SWJ - removed becasue the save function above does the same job
	<CFOUTPUT>
	<P><A HREF="JavaScript:verifyForm();">Continue</A>
	</CFOUTPUT>
 --->
	</FORM>
</cfif>

<cfoutput>
	<CF_includeonce template="/templates/relayFormJavaScripts.cfm">
</cfoutput>
