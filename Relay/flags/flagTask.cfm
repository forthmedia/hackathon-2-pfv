<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Update flag data for this entity --->


<!--- Mods
	wab 2001-04-04 	modification done on 2001-01-02 was wrong - if only part of a flag group was being updated, it would delete all the other flags in the group.  Now corrected
	wab 2001-01-02 	problem if variable of form radio_flaggroupid_entityid is blank (rather than zero) sholdn't happen, but does and I now check for it
	WAB 2000-06-14 	problem with when a date flag field blank, corrected
	WAB 1999-08-06 	for checkboxes, now checks whether #FlagForm#_#frmentityid# is blank (if comming from updatedata the variable is always defined but can be blank)
	WAB 1999-05-15 	All form elements change to include the entityid,
	WAB 1999-05-15 	Checkboxes use values in checkbox_flaggroupid_entityid_orig to decide which parts of a flag group are on display
					made changes so that the flag can be in the form of a flagtextid  ie.  checkbox_partnertype rather than checkbox_90
	WAB 2007-03-20  code to put individual date fields (month/year/day) back together again - moved from customtags\screens\updatedata
	WAB 2008-04-29 	added nvarchar support
	NYB 2009-03-31 	Sophos -	added isDefined("#FlagForm#_#frmentityid#") to if statement in delete sql for radio flags
							to stop the process throwing an error on quiz/questions.cfm
	WAB 2009/10/07 	LID 2732 - update lastupdatedby on radios before doing a delete
							added same to integer multiple
							updated others to use same sort of code
							removed references to cookie.user
	WAB 2010/02/04 	LID 3064
	NJH 2013/01/08	2013Roadmap - added support for decimal and textMultiple flags.
	NJH 2013/03/12 	case 433498	 - added lastupdatedByPerson

	YMA	2013/04/09	Case 434682 - added lastupdatedByPerson (the one that got away)
	RPW	2014-08-11	Create new profile type: "Financial"
	WAB 2015-06-22 	FIFTEEN-384 (adding time to date flags), moved call to combineDateFormFields() to applicationMain
    DAN	2015-12-17  Case 446907 - fix for violation of PRIMARY KEY constraint on inserting to BooleanFlagData
	RJT	2016-01-26	BF-369	TextFlagMultiples had been half moved over to the application.com.flag.getFlagStructure(thisFlag) style but wasn't getting the table in either the new way or the old way
--->


<CFPARAM Name="UpdateTime" Default="#createodbcdatetime(request.requesttime)#">

	<!--- Look through existing flag list, compare to what has been submitted on the form
	and archive deleted flags (not amended text,date,integer) --->
	<CFLOOP INDEX="FlagForm" LIST="#FlagList#">

<!--- 	debug: <cfoutput>#FlagForm# <BR>	<cfif isDefined("#FlagForm#_#frmentityid#")>	#evaluate("#FlagForm#_#frmentityid#")# <BR>	</cfif>	</cfoutput> --->
	<CFIF ListGetAt(FlagForm,1,"_") IS "checkbox">

		<CFSET thisFlagGroup=ListGetAt(FlagForm,2,"_")>
		<cfset flagGroupStructure = application.com.flag.getFlagGroupStructure(thisFlagGroup)>
		<cfset thisFlagGroup = flagGroupStructure.flagGroupID>

		<CFIF frmTask IS "update">

			<!--- only need to delete flags if there were ones set to start with
				ie if #FlagForm#_#frmentityid#_orig is defined and not null

				NJH 2015/06/28 - remove extra commas from list first...There was a bug when a checkbox flaggroup was on screen twice. The orig value was a 'comma', and
					so we want to avoid this and treat it as an empty string as it's essentially no values set for the group
			 --->
		 	<cfif structKeyExists(form,"#FlagForm#_#frmentityid#_orig")>
				<cfset form["#FlagForm#_#frmentityid#_orig"] = application.com.regexp.removeExtraCommasFromList(form['#FlagForm#_#frmentityid#_orig'])>

				<cfif form["#FlagForm#_#frmentityid#_orig"] is not "">

					<!--- set the last updated on the boolean flags, allows trigger to see who has done the deletion
						WAB 2009/10/07 LID 2732, just refactored the code here to be similar to new block below

						WAB 2010/02/04 LID 3064
						Problem when the _orig variable ended up with an extra comms in it eg  ",324,453"
						This can happen when individual checkboxes from the same flag group are place on a screen; and one is originally checked and one is originally not checked
						Odd that error had not occurred before, since I have that scenario on one of my test pages
						So added in a function to remove leading, trailing and double commas
					--->
					<cfsaveContent variable="querySnippet_sql">
						<cfoutput>
							BooleanFlagData AS bfd, Flag as f, FlagGroup as fg
				 	WHERE bfd.EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				 	AND bfd.FlagID = f.FlagID
				 	AND f.FlagGroupID = fg.FlagGroupID
			 		AND fg.FlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
				 	<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#")>
						<CFIF form["#FlagForm#_#frmentityid#"] is not "">
						AND bfd.FlagID NOT IN ( <cf_queryparam value="#form['#FlagForm#_#frmentityid#']#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
						</CFIF>
					</CFIF>
						AND bfd.FlagID IN (<cf_queryparam value="#form['#FlagForm#_#frmentityid#_orig']#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
						</cfoutput>
					</cfsavecontent>

					<!--- and delete flags --->
					<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
					<!--- update lastupdatedby
						NJH case 433498	 - added lastupdatedByPerson
					 --->
					Update BooleanFlagData
						set lastUpdated =  getDate(),
							lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
							lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					FROM
	  				#preserveSingleQuotes(querySnippet_sql)#

					<!--- and do delete --->
					DELETE BooleanFlagData <!--- NJH 2009/11/24 LHID 2732 - added booleanFlagdata --->
					FROM
					#preserveSingleQuotes(querySnippet_sql)#
					</CFQUERY>
				</cfif>
			</cfif>

		</CFIF>

		<!--- Add new flags
			NJH case 433498	 - added lastupdatedByPerson
		--->
		<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#")>
			<CFIF form["#FlagForm#_#frmentityid#"] is not "">
                <!--- START: 2015-12-17 DAN Case 446907 --->
                <cfloop list="#form["#FlagForm#_#frmentityid#"]#" index="itemFlagId">
    				<CFQUERY NAME="AddFlags" datasource="#application.sitedatasource#">
        				INSERT INTO BooleanFlagData
        				(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated, LastUpdatedByPerson)
        				SELECT 
							<cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
							f.FlagID, 
							<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
							<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
							<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
							<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
							<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
        				FROM Flag AS f
        				WHERE f.FlagID = <cf_queryparam value="#itemFlagId#" CFSQLTYPE="CF_SQL_INTEGER">
        				AND NOT EXISTS (SELECT bfd.FlagID FROM BooleanFlagData AS bfd WHERE bfd.FlagID = f.flagID and bfd.EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
    				</CFQUERY>
                </cfloop>
                <!--- END: 2015-12-17 DAN Case 446907 --->
			</CFIF>
		</CFIF>

	<CFELSEIF ListGetAt(FlagForm,1,"_") IS "radio">

		<CFSET thisFlagGroup=ListGetAt(FlagForm,2,"_")>
		<cfset flagGroupStructure = application.com.flag.getFlagGroupStructure(thisFlagGroup)>
		<cfset thisFlagGroup = flagGroupStructure.flagGroupID>

		<CFIF frmTask IS "update">
			<!--- delete the flags not selected
				WAB 2009/10/07 LID 2732 added-in the update of lastupdatedby before the delete in order to get the lastupdatedby recorded in modregister
			--->
			<cfsaveContent variable="querySnippet_sql">
				<cfoutput>
					BooleanFlagData
			WHERE EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND FlagID IN (SELECT bfd.FlagID FROM BooleanFlagData as bfd, Flag, FlagGroup
			WHERE bfd.FlagID = Flag.FlagID
		 	AND Flag.FlagGroupID = FlagGroup.FlagGroupID
	 		AND FlagGroup.FlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- NYB 2009-03-31 Sophos - added isDefined("#FlagForm#_#frmentityid#") to if statement below to stope the process throwing an error on quiz/questions.cfm --->
			<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#") and form["#FlagForm#_#frmentityid#"] is not "" >  <!--- wab added 2001-01-02 --->
				AND bfd.FlagID NOT IN (<cf_queryparam value="#form['#FlagForm#_#frmentityid#']#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
			</CFIF>
			)
				</cfoutput>
			</cfsavecontent>

			<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
			<!--- update lastupdatedby
				NJH case 433498	 - added lastupdatedByPerson
			--->
			Update BooleanFlagData
				set lastUpdated = getdate(),
					lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			FROM
			#preserveSingleQuotes(querySnippet_sql)#

			<!--- and do delete --->
			DELETE
			FROM
			#preserveSingleQuotes(querySnippet_sql)#
			</CFQUERY>

		</CFIF>

		<!--- Add new flags
			NJH case 433498	 - added lastupdatedByPerson
		--->
		<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#")>
			<CFIF form["#FlagForm#_#frmentityid#"] IS NOT "0" and form["#FlagForm#_#frmentityid#"] IS NOT ""> <!--- Not specified 2000-01-02 WAB added is not "", shouldn't happen but does sometimes!--->
				<CFQUERY NAME="AddFlags" datasource="#application.sitedatasource#">
				INSERT INTO BooleanFlagData
				(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated,LastUpdatedByPerson)
				SELECT 
					<cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					f.FlagID, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
					<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			  	FROM Flag AS f
			 	WHERE f.FlagID IN (<cf_queryparam value="#form['#FlagForm#_#frmentityid#']#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
				AND NOT EXISTS (SELECT bfd.FlagID FROM BooleanFlagData AS bfd WHERE bfd.FlagID = f.flagID and bfd.EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</CFQUERY>
			</CFIF>
		</CFIF>


	<CFELSEIF ListGetAt(FlagForm,1,"_") IS "boolean">
		<!--- test of scheme where the value of the form variable is the list of entityids
		which are to have that flag set --->


		<!--- could test for rights here --->
			
			<CFSET thisFlag=ListGetAt(FlagForm,2,"_")>
			<cfset flagStructure = application.com.flag.getFlagStructure(thisFlag)>
			<cfset thisFlag = flagStructure.flagID>


			<!--- if there were any entities originally selected, delete entities which are no longer selected --->


			<!--- it is possible that these flags have been set for entities which are only just being added, therefore
				their ids will be not be numeric.  Need to check for these and then resolve.
			 --->
			<CFSET entitiesSelected = "">
			<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#")>
				<CFSET entityArray = listToArray(form["#FlagForm#_#frmentityid#"])>
				<CFLOOP index="I" from="1" to ="#arrayLen(entityArray)#">
					<CFIF not isNumeric(entityArray[I])>
						<CFSET entitiesSelected = listappend(entitiesSelected,evaluate(entityArray[I]))>
					<CFELSE>
						<CFSET entitiesSelected = listappend(entitiesSelected,entityArray[I])>
					</cfif>
				</cfloop>
			</CFIF>



			<CFIF form["#FlagForm#_#frmentityid#_orig"] is not "">

				<!--- set the last updated on the boolean flags which are to be deleted, allows trigger to see who has done the deletion
					WAB 2009/10/07 altered to use query snippet
				--->
				<cfsaveContent variable="querySnippet_sql">
					<cfoutput>
				  		BooleanFlagData AS bfd
		 		WHERE FlagID = <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
				<CFIF entitiesSelected is not "">
					AND bfd.entityID NOT IN (<cf_queryparam value="#entitiesSelected#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
				</CFIF>
				AND bfd.EntityID IN (<cf_queryparam value="#form['#FlagForm#_#frmentityid#_orig']#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
					</cfoutput>
				</cfsaveContent >


				<!--- NJH case 433498	 - added lastupdatedByPerson --->
				<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
				Update BooleanFlagData
					set lastUpdated =  getDate(),
						lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				FROM
				#preserveSingleQuotes(querySnippet_sql)#

				DELETE
				FROM
				#preserveSingleQuotes(querySnippet_sql)#
				</CFQUERY>


			</CFIF>


		<!--- Add new flags --->

		<CFIF structKeyExists(form,"#FlagForm#_#frmentityid#")>

			<!--- could do it in one query if we worked out that type of entity this flag is for, but decided to do a query.
				NJH case 433498	 - added lastupdatedByPerson
			 --->
			<CFLOOP index="thisEntityID" list = "#entitiesSelected#">
				<CFIF listFind(form["#FlagForm#_#frmentityid#_orig"], thisEntityID) is 0>
					<CFQUERY NAME="AddFlags" datasource="#application.sitedatasource#">
					INSERT INTO BooleanFlagData
						(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated, lastUpdatedByPerson)
					SELECT 
						<cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#Variables.updatetime#" cfsqltype="CF_SQL_TIMESTAMP" >, 
						<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
						WHERE NOT EXISTS (SELECT entityID FROM BooleanFlagData AS bfd WHERE bfd.FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >  and bfd.EntityID =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
					</CFQUERY>
				</CFIF>
			</CFLOOP>
		</CFIF>







	<CFELSEIF ListGetAt(FlagForm,1,"_") contains "Multiple">

		<CFSET thisFlag=ListGetAt(FlagForm,2,"_")>
		
		<cfset flagStructure = application.com.flag.getFlagStructure(thisFlag)>
		<cfset thisFlag = flagStructure.flagID>
		<cfset fullTable=flagStructure.flagType.DATATABLEFULLNAME>

			<CFSET originalValues = form["#FlagForm#_#frmentityid#_orig"]>
			<CFPARAM name='#FlagForm#_#frmentityid#' default = "">
			<CFSET newValues = form["#FlagForm#_#frmentityid#"]>

			<!--- remove any line breaks and spaces --->
			<cfset newValues = reReplace(newValues,chr(10),",","ALL")>
			<cfset newValues = reReplace(newValues,",\s*,",",","ALL")>

				<!--- delete all items which are no longer in the list
				WAB 2009/10/07 altered to do an update first
				--->
				<cfsaveContent variable="querySnippet_sql">
					<cfoutput>
					  		#fullTable#
					WHERE EntityID = <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND FlagID = <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
					<CFIF newValues is not "">
					AND data NOT IN (<cf_queryparam value="#newValues#" CFSQLTYPE="#flagStructure.flagType.cfsqltype#" list="true">)
					</CFIF>
				 	<CFIF originalValues is not "">
					AND data IN (<cf_queryparam value="#originalValues#" CFSQLTYPE="#flagStructure.flagType.cfsqltype#" list="true">)
					</CFIF>
					</cfoutput>
				</cfsaveContent >

			<!--- NJH case 433498	 - added lastupdatedByPerson --->
			<CFQUERY NAME="DeleteFlags" DATASOURCE="#application.sitedatasource#">
				Update #fullTable#
					set lastUpdated =  GetDate(),
						lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				FROM
				#preserveSingleQuotes(querySnippet_sql)#

				DELETE
					FROM
				#preserveSingleQuotes(querySnippet_sql)#
			</CFQUERY>

		<!--- add all items which are now in the list (and weren't before)
			NJH case 433498	 - added lastupdatedByPerson
		--->
				<CFSET counter=0>
			<CFLOOP index="thisData" list="#newValues#">
				<CFSET counter=counter+1>
				<cfset thisData = trim(thisData)>

				<CFIF listfindnocase(originalValues,thisData) is 0 >
					<CFQUERY NAME="AddFlags" DATASOURCE="#application.sitedatasource#">
					INSERT INTO #fullTable#
					(EntityID, FlagID, Data, sortorder, CreatedBy, Created, LastUpdatedBy, LastUpdated, lastUpdatedByPerson)
					SELECT 
						<cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#thisData#" CFSQLTYPE="#flagStructure.flagType.cfsqltype#" >,
						<cf_queryparam value="#counter#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
						<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
						<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE NOT EXISTS (SELECT 1 FROM  #fullTable# WHERE FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >  and EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and data  =  <cf_queryparam value="#thisdata#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					</CFQUERY>
				<CFELSE>
					<!--- these ones need to have the sort order updated (could have deleted and updated) --->
					<CFQUERY NAME="UpdateFlags" DATASOURCE="#application.sitedatasource#">
					UPDATE
						#fullTable#
					set
						sortOrder =  <cf_queryparam value="#counter#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					where
							flagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
						and EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
						and data  =  <cf_queryparam value="#thisData#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</CFQUERY>
				</CFIF>
			</CFLOOP>


	<CFELSEIF ListGetAt(FlagForm,1,"_") IS NOT "Group">

		<CFSET tableType=ListGetAt(FlagForm,1,"_")>
		<CFSET thisFlag=ListGetAt(FlagForm,2,"_")>
		<cfset flagStructure = application.com.flag.getFlagStructure(thisFlag)>
		<cfset thisFlag = flagStructure.flagID>

		<CFIF frmTask IS "update">
			<CFQUERY NAME="FindFlags" datasource="#application.sitedatasource#">
			SELECT FlagID
		  	FROM #tableType#FlagData
		 	WHERE EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		 	AND FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			<CFSET ffRC = FindFlags.RecordCount>
		<CFELSE>
			<CFSET ffRC = 0>
		</CFIF>


		<CFSET tmpData = form["#FlagForm#_#frmentityid#"]>
		<cfscript>
			// 2014-08-11			RPW Create new profile type: "Financial"
			if (tableType=="financial") {
				if (StructKeyExists(form,"#FlagForm#_#frmentityid+1#")) {
					variables.tmpCurrencyISOCode = form["#FlagForm#_#frmentityid+1#"];
				} else {
					variables.tmpCurrencyISOCode = "";
				}
			}
		</cfscript>

		<CFIF ffRC IS 1>

			<CFIF form["#FlagForm#_#frmentityid#"] IS "">
				<CFQUERY NAME="LastUpdated" datasource="#application.sitedatasource#">
					Update #tableType#FlagData
						set lastUpdated =  getDate() ,
							lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
							lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
					WHERE EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>


				<!--- delete which have been reset to null --->
				<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
					DELETE FROM #tableType#FlagData
						WHERE EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>

			<CFELSE>

				<!--- NJH case 433498	 - added lastupdatedByPerson --->
			 	<CFQUERY NAME="UpdateFlag" datasource="#application.sitedatasource#">
				UPDATE #tableType#FlagData 
				SET
					Data = <cf_queryparam value="#tmpData#" CFSQLTYPE="#flagStructure.flagType.cfsqltype#" >,
					<cfif tableType EQ "financial">
						<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
						currencyISOCode = <cf_queryparam cfsqltype="CF_SQL_CHAR" value="#variables.tmpCurrencyISOCode#" maxlength="3" null="#!Len(variables.tmpCurrencyISOCode)#">,
						</cfif>
			 		lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >,
		 			LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		 			lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				WHERE EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND FlagID =  <cf_queryparam value="#thisFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>

			</CFIF>

		<CFELSE>
			<!--- NJH case 433498	 - added lastupdatedByPerson --->
			<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
			<CFIF form["#FlagForm#_#frmentityid#"] IS NOT "">
				<CFQUERY NAME="AddFlag" datasource="#application.sitedatasource#">
				INSERT INTO #tableType#FlagData
				(
					EntityID,
					FlagID,
					Data,
					<cfif tableType EQ "financial">
					currencyISOCode,
					</cfif>
					CreatedBy,
					Created,
					LastUpdatedBy,
					LastUpdated,
					LastUpdatedByPerson
				)
				VALUES (
					<cf_queryparam value="#frmEntityID#" CFSQLTYPE="cf_sql_integer" >, 
					<cf_queryparam value="#thisFlag#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#tmpData#" CFSQLTYPE="#flagStructure.flagType.cfsqltype#" >,
					<cfif tableType EQ "financial">
						<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
						<cf_queryparam cfsqltype="CF_SQL_CHAR" value="#variables.tmpCurrencyISOCode#" maxlength="3" null="#!Len(variables.tmpCurrencyISOCode)#">,
					</cfif>
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="cf_sql_timestamp" >, 
					<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" >, 
					<cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="cf_sql_timestamp" >,
					<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
				)
				</CFQUERY>
			</CFIF>

		</CFIF>

	</CFIF>

	</CFLOOP>


