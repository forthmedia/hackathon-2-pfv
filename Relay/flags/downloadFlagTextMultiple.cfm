<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type  = textMultiple --->

<cfif not isDefined("getFlagGroup#frmFlagGroupID#")>

	<CFQUERY NAME="getFlagGroup#frmFlagGroupID#" datasource="#application.sitedatasource#">
	select 
		f.name, 
		f.flagid, 
		f.linksToEntityTypeID,
		fet.tablename,
		fet.nameExpression,
		fet.UniqueKey 
	FROM 
		Flag As f
			left outer join
		flagEntityType 	 fet
			on fet.entityTypeID = f.linksToEntityTypeID 
	WHERE 
		f.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by 
		f.orderingIndex, f.name
	</cfquery>

	<cfset getFlagGroup = evaluate("getFlagGroup#frmFlagGroupID#")>
		
<cfelse>

	<cfset getFlagGroup = evaluate("getFlagGroup#frmFlagGroupID#")>
	
</cfif>




<CFSET flagsQuery = "">

<cfloop query="getFlagGroup">

	<CFQUERY NAME="getFlagData" datasource="#application.sitedatasource#">
	<!--- Chr() in Access, Char() in SQL Server--->
	SELECT 
		f.Name,
		fd.Data as data
	FROM 
		Flag As f 
			inner join
		TextMultipleFlagData As fd
			on fd.FlagID = f.FlagID
	WHERE 
		fd.EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and f.flagid =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	
	<cfif getFLagData.recordCount is not 0>
		<CFSET flagsQuery = listappend(flagsQuery, ValueList(getFlagdATA.data, ','), delim)>
	<cfelse>
		<CFSET flagsQuery = listappend(flagsQuery, " ", delim)><!--- blank is needed otherwise colums go wrong if there is no item with data in whole table --->
	</cfif>
	
</cfloop>

