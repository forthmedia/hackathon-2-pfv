<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type 6 = Integer 

WAB 2009/03/25 LID 2014 altered valid value display so that when valid values are country specific (eg #countryid# in the select statement) the user sees all values for their countries
			also required a change to customtags\validvalues\qryValidValuec.cfm
WAB 2016-03-09	BF-543 ValidValues not working (needed a dot not a _ in the validfieldname)
--->
<CFINCLUDE template="qryFlag.cfm">

<CFIF getFlags.RecordCount neq 0>

	<CFLOOP query="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
	
		<CFOUTPUT>
			<div class="form-group">
			    <label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(Name)#</label>		
				<!--- <cfoutput>#flag_AccManager_ValidValues# </cfoutput> --->
				<CFIF getFlags.useValidValues eq 1>

					<!--- 2005-11-29 WAB changed to multiselect - also needed to update database - selectionField--->
					<cftry>
						<CF_displayvalidvalues 
								formfieldname = "frmFlag_#typeName#_#FlagID#"
								validfieldname =  "flag.#getFlags.FlagTextID#"
								currentvalue = ""
								listsize = "4"
								multiple = true
								countryid = "#request.relaycurrentuser.countrylist#"	
								>
								<!--- WAB 2009/03/25 added countryid to above.  Means that will bring back all valid values for all countries that the user has rights to --->

								<cfcatch>
									<cfif cfcatch.message contains "Valid Value Error">
											<CF_relayValidatedField
											fieldName="frmFlag_#typeName#_#FlagID#"
											currentValue=""
											charType="numeric"
											size="10"
											maxLength="10"
											helpText=""
										>
									<cfelse>
										<cfrethrow>
									</cfif>
								</cfcatch>
					</cftry>

										
										
				<CFELSE>
					<CF_relayValidatedField
						fieldName="frmFlag_#typeName#_#FlagID#"
						currentValue=""
						charType="numeric"
						size="10"
						maxLength="10"
						helpText=""
					>
					<!--- <INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="" SIZE=10 MAXLENGTH=10><BR><CFIF Trim(Description) IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF> --->
				</cfif>
			</div>
		</CFOUTPUT>
	</CFLOOP>

<CFELSE>
	<P>* No flag data for this Group</P>
</CFIF>

