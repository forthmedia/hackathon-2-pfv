<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB

2006-03-01 

Altered this template so that each flag is brought back in a separate column

Note than doDownload.cfm has to know that text flaggroups will bring back more than one column


 --->
<!--- Download Flags of type 5 = text --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
SELECT replace(isNull(fd.data,' '),char(13) + char(10),'|') as data     <!--- ought to be able to replace carriage returns with something better that excel can handle- but don't know what to use! --->
FROM Flag As f
		left join
	 TextFlagData As fd on fd.FlagID = f.FlagID
	 					and fd.EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
WHERE f.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
order by f.OrderingIndex, f.name
</CFQUERY>

<!--- <CFSET flagsQuery = #ValueList(getFlags.rowdata, ',')#> --->
<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.data, delim)>
</CFIF>


