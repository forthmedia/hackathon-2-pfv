<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type 14 = financial --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
SET CONCAT_NULL_YIELDS_NULL OFF
SELECT
	ISNULL(C.currencySign + CONVERT(NVARCHAR(30),FFD.data),0) AS rowData
FROM Flag F
LEFT JOIN financialFlagData FFD ON F.FlagID = FFD.flagID AND FFD.EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
LEFT JOIN Currency C ON FFD.currencyISOCode = C.currencyISOCode
WHERE F.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
SET CONCAT_NULL_YIELDS_NULL ON
</CFQUERY>

<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.rowdata, delim)>
</CFIF>
