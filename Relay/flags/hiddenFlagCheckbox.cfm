<!--- �Relayware. All Rights Reserved 2014 --->
<!--- HiddenFlagCheckBox 

Quick addition to the flag system which allows flags to be set/ unset behind the scenes on forms
Only work within the context of a screen definition
Set method = 'hidden' and evalstring = 0 or 1

Author: WAB

date 2000-08-02

 --->


<CFSETTING enablecfoutputonly="yes"> 

<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFOUTPUT>
<CFIF evalString is 1>
	<CF_INPUT TYPE="Hidden" NAME="#typeName#_#thisFlagGroup#_#thisentity#" VALUE="#getFlags.FlagID#">
	<CF_INPUT TYPE="Hidden" NAME="#typeName#_#thisFlagGroup#_#thisentity#_orig" VALUE="">
<CFELSE>
	<CF_INPUT TYPE="Hidden" NAME="#typeName#_#thisFlagGroup#_#thisentity#" VALUE="">
	<CF_INPUT TYPE="Hidden" NAME="#typeName#_#thisFlagGroup#_#thisentity#_orig" VALUE="#getFlags.FlagID#">
</cfif>
</cfoutput>

<CFSET flagList = flagList & ",#typeName#_#thisFlagGroup#">



<CFSETTING enablecfoutputonly="no"> 
