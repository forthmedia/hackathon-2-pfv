<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Get USER countries --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<CFQUERY NAME="getCountryGroups" datasource="#application.sitedatasource#">
SELECT DISTINCT b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE (b.ISOcode = null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
ORDER BY b.CountryID
</CFQUERY>

<CFSET UserCountryList = "0,#Variables.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>
	
<!--- <CFSET entityTable = #ListGetAt(application.typeEntityTable,entityType+1)#> --->
<cfset entityTable = application.entityType[entityType].tablename>

<CFQUERY NAME="getEntities" datasource="#application.sitedatasource#">

	SELECT DISTINCT #entityTable#Temp.#entityTable#ID AS eid
	FROM #entityTable#Temp, FlagGroup AS fg, Flag As f
	<CFIF REFind("Radio_|Checkbox_",flagList) IS NOT 0>, BooleanFlagData</CFIF>
	<CFIF REFind("Date_",flagList) IS NOT 0>, DateFlagData</CFIF>
	<CFIF REFind("Text_",flagList) IS NOT 0>, TextFlagData</CFIF>
	<CFIF REFind("Integer_",flagList) IS NOT 0>, IntegerFlagData</CFIF>
	WHERE #entityTable#Temp.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
	AND f.FlagGroupID = fg.FlagGroupID
	AND fg.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND (1=0
	
 	<CFLOOP INDEX="FlagForm" LIST="#flagList#">
	
		<CFSET thisType=ListGetAt(FlagForm,1,"_")>
		<CFIF IsDefined(#FlagForm#)>
			<CFIF #Evaluate('#FlagForm#')# IS NOT "">
			
				<CFIF ListGetAt(FlagForm,1,"_") IS "checkbox">
					OR (#entityTable#Temp.#entityTable#ID=BooleanFlagData.EntityID
				 	AND BooleanFlagData.FlagID = f.FlagID
					AND fg.FlagGroupID =  <cf_queryparam value="#ListGetAt(FlagForm,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND BooleanFlagData.FlagID IN (#Evaluate('#FlagForm#')#))
				<CFELSEIF ListGetAt(FlagForm,1,"_") IS "radio">
					OR (#entityTable#Temp.#entityTable#ID=BooleanFlagData.EntityID
				 	AND BooleanFlagData.FlagID = f.FlagID
				 	AND fg.FlagGroupID =  <cf_queryparam value="#ListGetAt(FlagForm,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND BooleanFlagData.FlagID = #Evaluate('#FlagForm#')#)
				<CFELSEIF ListGetAt(FlagForm,1,"_") IS "integer">
					OR (#entityTable#Temp.#entityTable#ID=#thisType#FlagData.EntityID
				 	AND #thisType#FlagData.FlagID =  <cf_queryparam value="#ListGetAt(FlagForm,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND #thisType#FlagData.FlagID = fg.FlagID
					AND #thisType#FlagData.Data = #Evaluate('#FlagForm#')#)	
				<CFELSEIF ListGetAt(FlagForm,1,"_") IS "date">
					OR (#entityTable#Temp.#entityTable#ID=#thisType#FlagData.EntityID
				 	AND #thisType#FlagData.FlagID =  <cf_queryparam value="#ListGetAt(FlagForm,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND #thisType#FlagData.FlagID = fg.FlagID
					AND #thisType#FlagData.Data = #CreateODBCDate(Evaluate('#FlagForm#'))#)					
				<CFELSEIF ListGetAt(FlagForm,1,"_") IS NOT "0">
					OR (#entityTable#Temp.#entityTable#ID=#thisType#FlagData.EntityID
				 	AND #thisType#FlagData.FlagID =  <cf_queryparam value="#ListGetAt(FlagForm,2,"_")#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND #thisType#FlagData.FlagID = fg.FlagID
					AND #thisType#FlagData.Data LIKE '%#Evaluate('#FlagForm#')#%')					
				</CFIF>
		
			</CFIF>

		</CFIF>
	</CFLOOP>
	)
</CFQUERY>


<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
</cf_head>

<FONT FACE="Arial, Chicago, Helvetica" SIZE="2">


<H3>Flag Tool Demo: search flags</H3>

<P>
<CFIF getEntities.RecordCount IS 0>
	No records matched.
<CFELSE>
	<CFLOOP QUERY="getEntities">
	<CFIF entityType IS 0>
		<CFQUERY NAME="getDetail" datasource="#application.sitedatasource#">
		SELECT FirstName, LastName, PersonID
		FROM PersonTemp
		WHERE PersonID =  <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		<LI><CFOUTPUT>#htmleditformat(eid)# #htmleditformat(getDetail.FirstName)# #htmleditformat(getDetail.LastName)# (#htmleditformat(getDetail.PersonID)#)</CFOUTPUT>
	<CFELSE>
		<CFQUERY NAME="getDetail" datasource="#application.sitedatasource#">
		SELECT SiteName, LocationID
		FROM LocationTemp
		WHERE LocationID =  <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		<LI><CFOUTPUT>#htmleditformat(getDetail.SiteName)# (#htmleditformat(getDetail.LocationID)#)</CFOUTPUT>
	</CFIF>
	</CFLOOP>
</CFIF>

<P>Back to <A HREF="index.cfm">FLAG TOOLS</A>

</FONT>

