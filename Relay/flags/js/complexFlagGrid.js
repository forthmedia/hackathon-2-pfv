/* �Relayware. All Rights Reserved 2014 */



				function addRow(tableID,numberOfRows) {
					newRowArray = eval ('newRow'+tableID)
					table = document.getElementById(tableID)
					var i = 0 ;	var j = 0 ;
					for (j=1;j<=numberOfRows;j++) {
						var row = table.insertRow(table.rows.length);
						rowNumber = table.rows.length -1
						row.className = 'flaggrid';
						newIndex = 'new' + (table.rows.length>=10?"":"0") + table.rows.length   // adds a leading zero
						row.id = tableID + '_row_' + newIndex
						for (i=0;i<newRowArray.length;i++) {
							 newcell=row.insertCell(-1)
							 newcell.className = 'flaggrid';
							 newcell.innerHTML = newRowArray[i].content.replace(/INDEX/g,newIndex).replace(/ROWNUMBER/g,rowNumber)
						}
					}	
						
			  	}




/*
verifyGrid
	used to work out whether the grid has been filled in correctly
	tableID - the grid is in a table this is the id of that table
	rowcheckExpression - a javascript expression which will return true or false when evaluated.  
			It can use the function  somethingEntered('fieldName')  

			
	returns an array
		result.numberOfNonBlankRows   - 
		result.isOK    - are all non blank rows OK
		result.invalidRows array  
			result.invalidRows[i].rowIndex     - index of row []
			result.invalidRows[i].rowNumber    - row of the grid where there was an error
			
*/


	function verifyGrid (tableID,rowCheckExpression) {
		tableObj = document.getElementById(tableID)	
		// create an array with IDs of each row which has our form fields in.   
		var rowInfo = new Array()
		var result = new Array ()
		
		regExpString ='' + tableID + '_row_(.*)'   // each row of the table with data in it has an id of the form  "myTable_row_xxxx".  We used this regExp to extract the xxxx bit - the "rowIndex" as I call it.  This value will occur in each form field on the row
		regExp = new RegExp(regExpString)
		// loop down the table looking for rows with of this form and pop the index into a structure
		for (i=0;i<tableObj.rows.length;i++) {
			thisRow = tableObj.rows[i]
			regExpResultArray = regExp.exec(thisRow.id)
			if (regExpResultArray) {
				temp = new Array
				temp.index = regExpResultArray[1]
				rowInfo[rowInfo.length] = temp
			}
		}


		//  we need to know the name of the fields in the grid.   They are stored in a hidden field with name myTable_fieldList
		// pop fields into an array
		fieldList = document.getElementById(tableID + '_fieldList').value
		fieldArray = fieldList.split(',')

		// also need to know how the form fields can be referenced.  I store a pattern in a hidden field.  
		// It will be something like flag_xxxx_*ROWINDEX*_*FIELD*_yyyy
		// as we loop over the table, *ROWINDEX* and *FIELD* are replaced by the approproate values
		gridFieldPattern = document.getElementById(tableID + '_fieldPattern').value

		// now loop through these rows to see which rows have any data in them
	
		result.numberOfNonBlankRows  = 0
		for (i = 0; i<rowInfo.length ; i++) {
			rowInfo[i].hasData = false
			// check for delete object
			deletedFieldID = gridFieldPattern.replace('*ROWINDEX*',rowInfo[i].index).replace('*FIELD*','deleteitem')	
			obj = document.getElementById(deletedFieldID)
				if (obj && obj.checked) {
					// has the delete checkbox checked - so no data in this row
				} else 
				{
			for (j = 0; j<fieldArray.length ; j++) {
				fieldID = gridFieldPattern.replace('*ROWINDEX*',rowInfo[i].index).replace('*FIELD*',fieldArray[j])		
				obj = document.getElementById(fieldID)
				hasData = checkObject (obj)  // checkObject is a function in checkObject.js  returns 0 (false) if nothing in a field
				if (hasData) {
					result.numberOfNonBlankRows ++
					rowInfo[i].hasData = true ; break
				}
				}
			}
		}


		// now loop through rows with data and check whether they meet the conditions
		result.invalidRows = new Array()
		result.isOK = true

		for (i = 0; i<rowInfo.length ; i++) {	
			if (rowInfo[i].hasData) {
				gridRowIndex = rowInfo[i].index  // this sets a global variable which is used in in
				isRowOK = eval(rowCheckExpression)
				if (!isRowOK) {
					temp = new Array ()
					temp.rowIndex = rowInfo[i].index
					temp.rowNumber = i + 1    // note = this assumes one row for heading
					result.invalidRows[result.invalidRows.length] = temp
					result.isOK = false
				}

			
			}
		}


	return result
	
	}	


	function verifyGridMsg (tableID,rowCheckExpression,rowCheckMessage,required, requiredMessage)
	
	{
	
			result = verifyGrid (tableID,rowCheckExpression)
			if (result.isOK   && result.numberOfNonBlankRows >= required) {
				return ''
			} else if (result.isOK ) {
				return requiredMessage + '\n'
			} else {
				badRows = ''
				for (i = 0; i<result.invalidRows.length ; i++) {	
					separator = (badRows =='') ? ' ' :', '
					badRows = badRows + separator + result.invalidRows[i].rowNumber
				}
				return rowCheckMessage + '   ' + badRows + '\n'
			}
			
			
	}
		
	
	
	// global vars used set in the function above and used in the function below
	var gridRowIndex
	var gridFieldPattern
	
	function somethingEntered (field) {
		fieldID = gridFieldPattern.replace('*ROWINDEX*',gridRowIndex).replace('*FIELD*',field)				
		obj = document.getElementById (fieldID)
		return checkObject (obj)
		
	}
	
	
	function isEmail (field) {
		fieldID = gridFieldPattern.replace('*ROWINDEX*',gridRowIndex).replace('*FIELD*',field)				
		obj = document.getElementById (fieldID)
		return verifyEmailV2 (obj)
		
	}	
	
	function isNumeric (field) {
		fieldID = gridFieldPattern.replace('*ROWINDEX*',gridRowIndex).replace('*FIELD*',field)				
		obj = document.getElementById (fieldID)

		var RegExp = /^(-)?(\d*)(\.?)(\d*)$/;   // believed to do decimals and negatives.  Note that blank will return true, so need to do a separaet test for something in the field
		var result = RegExp.test(obj.value); 
//		alert (obj.value + '  ' + result)
		return result;  		
	}	

	
