<!--- �Relayware. All Rights Reserved 2014 --->
<!--- View Flags of type 3 = Radio Button

	NYB 2008/08/09  P-SOP003 - added ability to sort attributes different options via orderType.  Accepts: SortOrder, Alpha or Random
	WAB 2011/09/27	P-TND109 change view mode to use disabled controls rather than plain text
	PJP 22/10/2012 CASE 429875 deal with select box in view mode
	WAB 2013-04-16 CASE 429875 reverted PJP change and made simpler correction
	2015-10-13 WAB 	PROD2015-85 Alter <LABEL> tags so do not have nested INPUT, add For attribute (SB modified .css). Removed double column layout
    21/01/2016 DAN BF-325 - Issue on radio button in 3 pane view

--->

<CFPARAM name="column" default="3">
<CFPARAM name="displayas" default="radio">
<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options --->
<CFPARAM name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->


<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>

	<CFSET c = 0>
	<CFSET fieldName = "#typeName#_#thisFlagGroup#_#thisentity#">

	<CFIF DisplayAs IS NOT "Select">
		<cfoutput>
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<BR>
			<TABLE BORDER="0" WIDTH="90%" CELLSPACING="0" CELLPADDING="2">

				<CFLOOP QUERY="getFlags">

					<CFINCLUDE TEMPLATE="qryFlagData.cfm">

					<CFSET c = c + 1>
					<CFSET widtha = Round((100 / column))>

					<CFIF c MOD column IS 1>
						<TR>
					</CFIF>
							<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%">
								<input type="radio" <CFIF getFlagData.RecordCount IS NOT 0>checked=checked</cfif> disabled id="#fieldName#_#flagid#">
								<!---
								<CFIF getFlagData.RecordCount IS NOT 0>
									<IMG SRC="/images/misc/flag-yes.gif" WIDTH=15 HEIGHT=15 ALT="Yes" BORDER="0">
								<CFELSE>
									<IMG SRC="/images/misc/flag-no.gif" WIDTH=15 HEIGHT=15 ALT="No" BORDER="0">
								</CFIF>
								--->

							<label for = "#fieldName#_#flagid#">
								<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
									phr_#htmleditformat(trim(namephrasetextid))#&nbsp;
								<CFELSE>
									#htmleditformat(Name)#&nbsp;
								</cfif>
							</label>
							</TD>
					<CFIF c MOD column IS 0>
						</TR>
					</CFIF>

				</CFLOOP>
			<CFIF c MOD COLUMN IS NOT 0><TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))#>&nbsp;<BR></TD></TR></CFIF>
			</TABLE>
		<cfelse>
			<cfset noneSelected = true>
			<CFLOOP QUERY="getFlags">
				<CFINCLUDE TEMPLATE="qryFlagData.cfm">
				<cfif getFlagData.RecordCount IS NOT 0>
				    <cfset noneSelected = false>
                </cfif>
				<!--- 2015-12-16 WAB BF-83 add support for the #column# attribute --->
				<cfswitch expression=#Column#>
					<cfcase value="1">
						<div class="radio col-xs-12 col-sm-12 col-md-12">
					</cfcase>
					<cfcase value="2">
						<div class="radio col-xs-12 col-sm-6 col-md-6">
					</cfcase>
					<cfcase value="4">
						<div class="radio-inline">
					</cfcase>
					<cfdefaultcase> <!--- 3 columns --->
						<div class="radio-inline">
					</cfdefaultcase>
				</cfswitch>

                    <input class="radio" type="radio" <CFIF getFlagData.RecordCount IS NOT 0>checked=checked</cfif> disabled id="#fieldName#_#flagid#">
					<label>
						<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
							phr_#htmleditformat(trim(namephrasetextid))#&nbsp;
						<CFELSE>
							#htmleditformat(Name)#&nbsp;
						</cfif>
					</label>
				</div>

			</CFLOOP>
			<cfif noneSelected and not request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput><span class="flagAttributeLabel">phr_sys_noneSelected</span></cfoutput>
			</cfif>
		</cfif>
		</cfoutput>
	<CFELSE>
		<!--- CASE 429875 drop down showing incorrectly
				PJP 22/10/2012 Added in hard coded select drop down for view only
				WAB 2013-04-16 reverted PJP change (which wasn't readonly) and instead changed entityID to thisEntity in call to getFlagGroupDataAndControls
		  --->
		<cfset flagGroupFormatting.displayAs = "select">
		<cfset flagGroupControl_html =  application.com.flag.getFlagGroupDataAndControls (flaggroupid = thisFlagGroup, entityid = thisEntity,method="view",additionalFormatting=flagGroupFormatting)>
		<cfoutput>#flagGroupControl_html[1].control_html#</cfoutput>
	</CFIF>


<CFELSE>

	<cfoutput><P>* No flag data for this Group</cfoutput>

</CFIF>


