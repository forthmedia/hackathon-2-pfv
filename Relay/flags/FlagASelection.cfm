<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

FlagASelection.cfm


Template called from the selections screen which allows user to flag all the people in a selection

Basically calls setFlagsForIDList.cfm

Saves having to create a query with the selection screens


takes 	frmSelectionID
		frmEntityTypeID

Author:	WAB    2001-03-22
2012/05/30	IH	Case 428006 increase timeout value to accomodate large queries
2012-08-01 	PPB Case 429007 changed <> to != because it was converting to: and status &lt;&gt; 0

 --->
<cfsetting requesttimeout="600">
<CFSET securitylist = "SelectTask:read">
<CFINCLUDE TEMPLATE="../templates/qrycheckperms.cfm">

<cfif checkPermission.RecordCount IS 0>
	Sorry you do not have access to this Function <CF_ABORT>
</cfif>

 
 
<cfparam name = "frmSelectionID">
<cfparam name = "frmFlagEntityTypeID">
 
<!--- 
	create the queries for various entityTypes 
	note that these don't check for active / inactive
	test for countries is done in setFlagsForIDList.cfm
--->

<cfif frmFlagEntityTypeID is 0>
	<CFSET frmEntityID = "Select entityID from selectionTag where selectionid in (#frmSelectionID#) and status != 0"> <!--- 2012-08-01 PPB Case 429007 changed <> to != because it was converting to: and status &lt;&gt; 0 and crashing --->
	
<cfelseif frmFlagEntityTypeID is 1>
	<CFSET frmEntityID = "Select locationID from selectionTag st inner join person p on p.personid = st.entityid where selectionid in (#frmSelectionID#) and status != 0">			<!--- 2012-08-01 PPB Case 429007 changed <> to != because it was converting to: and status &lt;&gt; 0 and crashing --->

<cfelseif frmFlagEntityTypeID is 2>
	<CFSET frmEntityID = "Select OrganisationID from selectionTag st inner join person p on p.personid = st.entityid where selectionid in (#frmSelectionID#) and status != 0">		<!--- 2012-08-01 PPB Case 429007 changed <> to != because it was converting to: and status &lt;&gt; 0 and crashing --->

<cfelse>
	Incorrect entityTypeID <CF_ABORT>
</cfif>

<cfset frmEntityTypeID = frmFlagEntityTypeID>



<cfinclude template = "setFlagsForIDList.cfm">



 
 