<!--- �Relayware. All Rights Reserved 2014 --->
<!--- This program outputs a valuelist for a given flag textid 

This requires #thisFlagTextID (as a flgTextID) 
and #thisentity# (i.e. EntityID)

It should be called as follows:

		<CFSET FlagTextID = "3ComMainContact"> 
		<CFSET ThisEntity = #LocationID#>
		
		<CFOUTPUT>#Result#</CFOUTPUT>
		#result# returns None set if there are no flags set
		#result2# returns blank inthis case


--->

<!--- Uncomment this for testing 
<CFSET FlagTextID = "PartnerType_SBS"> 
<CFSET ThisEntity = 1>
--->

<!--- since changing the query to work on fg.FlagTextID IN (#FlagTextID#) I need to ensure that there are quotes around the 
	flagtextid  (ensures backwards compatability and means that doesn;'t have to be called with quotes	--->
<!--- <CFIF left(trim(FlagTextID),1) is not "'">
	<CFSET FlagTextID = "'" & FlagTextID>
</cfif>
 
<CFIF Right(trim(FlagTextID),1) is not "'">
	<CFSET FlagTextID = FlagTextID & "'">
</cfif> --->

 
<CFSET Result = ""> 
<!--- Find the Flag data type --->
<CFQUERY NAME="GetFlag" datasource="#application.siteDataSource#">
	SELECT ft.FlagTypeID, fg.FlagGroupID, ft.datatable, f.flagid
	FROM FlagGroup as fg, FlagType  as ft, flag as f
	WHERE fg.FlagTypeID = ft.FlagTypeID
	AND f.flaggroupid = fg.flaggroupid
	AND f.FlagTextID IN (<cf_queryparam value="#FlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
	

</CFQUERY>

<CFIF GetFlag.recordcount IS 0>
	<CFSET Result = "Error: FlagTextID is wrong">
<CFELSE>

		<CFSET flagidlist = #valuelist(GetFlag.FlagID)#>	
		<CFSET flagtablelist = #valuelist(GetFlag.datatable)#>	
		

	<CFLOOP index=I from="1" to="#listlen(flagidlist)#">


		<CFQUERY NAME="GetFlagValues" datasource="#application.siteDataSource#">
		select f.name 
		from flag as f, #listgetat(flagtablelist,I)#flagdata as a
		where a.flagid= f.flagid
		and f.active <> 0
		and f.flagid =  <cf_queryparam value="#listgetat(flagidlist,I)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and entityid =  <cf_queryparam value="#thisentity#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<CFIF getflagvalues.recordcount IS NOT 0>	
			<CFSET result = listappend(result,valuelist(getflagvalues.name))>
		</CFIF>
	</cfloop>	
		
		<CFIF result is not "">
			<CFSET Result = Replace(Result, "," , ", " ,"ALL")>
			<CFSET Result2 =result>
		<CFELSE>
			<CFSET Result = "None Set">
			<CFSET Result2 = "">
		</CFIF>

	
</CFIF>