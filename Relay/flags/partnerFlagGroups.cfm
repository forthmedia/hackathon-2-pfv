<!--- �Relayware. All Rights Reserved 2014 --->


<!--- Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view 
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit
	
	getFlagGroup.FlagGroupID

	--->


<!--- Find Country for this Entity --->
<CFQUERY NAME="getCountry" datasource="#application.siteDataSource#">
	SELECT DISTINCT l.CountryID
	<CFIF entityType IS 0>
	FROM Person As p, Location As l
	WHERE p.PersonID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND p.LocationID = l.LocationID
	<CFELSE>
	FROM Location As l
	WHERE l.LocationID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
</CFQUERY>
	
<CFSET eCountryID = getCountry.CountryID>
<CFSET EntityCountryList = "0,#eCountryID#">

<!--- Find all Country Groups for this Country --->
<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
SELECT DISTINCT b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE a.CountryID =  <cf_queryparam value="#eCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Entity Country --->
  AND (b.ISOcode IS null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
ORDER BY b.CountryID
</CFQUERY>

<CFIF getCountryGroups.RecordCount IS NOT 0>
	<CFSET EntityCountryList =  EntityCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>
	
<CFQUERY NAME="getTopLevelFlags" datasource="#application.siteDataSource#">
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description,
CASE WHEN namephrasetextid is null THEN ' ' ELSE namephrasetextid END as namephrasetextid,
CASE WHEN descriptionphrasetextid is null THEN ' ' ELSE descriptionphrasetextid END as descriptionphrasetextid,
flagType.Name as typename,flagType.dataTable as typedata
FROM FlagGroup
		inner join 
		FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID 

WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
AND FlagGroup.FlagGroupID =  <cf_queryparam value="#getFlagGroup.FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
ORDER BY OrderingIndex, Name
</CFQUERY> 



<CFLOOP QUERY="getTopLevelFlags">

<CFSET thisFlagGroup = ID>
<!--- NJH 2010/08/09 RW8.3 - removed code below for data in structures
then WAB 2010-09-09 get from query
<CFSET typeName = #ListGetAt(application.typeList,FlagTypeID)#>
<CFSET typeData = #ListGetAt(application.typeDataList,FlagTypeID)#> 
<cfset typeName = application. flagGroup[ID].flagType.name>
<cfset typeData = application. flagGroup[ID].flagType.datatable>
--->

<P>
<CFOUTPUT>
			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
				phr_#namephrasetextid#
			<CFELSE>
				#HTMLEditFormat(Name)# 
			</cfif>
	
			<CFIF #Trim(Description)# IS NOT "">
				<CFIF trim(descriptionphrasetextid) is not "">  
				(<I>phr_#descriptionphrasetextid#</I>)
				<CFELSE>
				(<I>#HTMLEditFormat(Description)#</I>)
				</cfif>
			</cfif>
</CFOUTPUT>

<!--- <P><CFOUTPUT>#HTMLEditFormat(Name)# <CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF></CFOUTPUT> --->

<CFQUERY NAME="getLowerLevelFlagGroups" datasource="#application.siteDataSource#">
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description,
CASE WHEN namephrasetextid is null THEN ' ' ELSE namephrasetextid END as namephrasetextid,
CASE WHEN descriptionphrasetextid is null THEN ' ' ELSE descriptionphrasetextid END as descriptionphrasetextid,
flagType.Name as typename,flagType.dataTable as typedata
FROM FlagGroup
		inner join 
		FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID 

WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
ORDER BY OrderingIndex, Name
</CFQUERY>
	
	<CFIF #getLowerLevelFlagGroups.RecordCount# IS NOT 0>
		
		<UL><CFLOOP QUERY="getLowerLevelFlagGroups">
		<CFSET thisFlagGroup = ID>
<!--- 
		<CFSET typeName = #ListGetAt(application.typeList,FlagTypeID)#>
		<CFSET typeData = #ListGetAt(application.typeDataList,FlagTypeID)#>
 --->

<P>
<CFOUTPUT>
			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
				phr_#namephrasetextid# 
			<CFELSE>
				#HTMLEditFormat(Name)# 
			</cfif>
	
			<CFIF #Trim(Description)# IS NOT "">
				<CFIF trim(descriptionphrasetextid) is not "">  
				(<I>phr_#descriptionphrasetextid#</I>)
				<CFELSE>
				(<I>#HTMLEditFormat(Description)#</I>)
				</cfif>
			</cfif>
</CFOUTPUT>

<!--- 			<P><CFOUTPUT>#HTMLEditFormat(Name)# <CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF></CFOUTPUT> --->

			<!--- typeName should not be 'Group' but page exists for this case --->
			<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">

		</CFLOOP></UL>
	<CFELSE>
	
		<CFIF #typeName# IS "Group">
			<UL>No flags available to <CFOUTPUT>#htmleditformat(flagmethod)#</CFOUTPUT>.</UL>
		<CFELSE>

			<UL><CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm"></UL>

		</CFIF>

	</CFIF>

</CFLOOP>



