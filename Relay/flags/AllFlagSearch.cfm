<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

AllFlagSeach.cfm


WAB 2005-12-05

Template which calls AllFlag after working out a filter based on recently searched flags


Note that the filter currently only applies to the top level flaggroups, so if any item has been searched in a lower level flag group then all "sibling" flag groups will be shown.  This could be changed by a small change in allFlag.cfm


 --->
<cfparam NAME="frmFilterCriteriaDisplay" DEFAULT="MYRECENT">
 
 
<cfif frmFilterCriteriaDisplay is "myRecent">

	<!--- get recently used flags/groups --->
	<CFQUERY NAME="getRecentFlags" datasource="#application.siteDataSource#">
	select distinct flagGroupID, ParentflagGroupID  from vFlagGroupsUsedInSelections where entityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >  and createdBy = #request.relaycurrentuser.usergroupid#
	</CFQUERY>
	<cfset groupList = listappend (listappend (valuelist (getRecentFlags.flagGroupID), valuelist (getRecentFlags.ParentflagGroupID)),0)>

 
<cfelseIF frmFilterCriteriaDisplay is "Recent">

	<!--- get recently used flags/groups --->
	<CFQUERY NAME="getRecentFlags" datasource="#application.siteDataSource#">
	select distinct flagGroupID, ParentflagGroupID  from vFlagGroupsUsedInSelections where entityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >  
	</CFQUERY>
	<cfset groupList = listappend (listappend (valuelist (getRecentFlags.flagGroupID), valuelist (getRecentFlags.ParentflagGroupID)),0)>

<cfelseIF frmFilterCriteriaDisplay is "All">

	<!--- get all flags/groups - still filtered later to user rights --->
	<cfset groupList = "">

</cfif>

	<cfinclude template = "allflag.cfm"> 