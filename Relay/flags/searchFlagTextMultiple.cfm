<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type = TextMultiple --->
<CFINCLUDE TEMPLATE="qryFlag.cfm">
	
<CFIF getFlags.RecordCount IS NOT 0>

	<CFLOOP QUERY="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
		
		<CFOUTPUT>
			<div class="form-group">
				<label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(Name)#</label>
		
				<CFSET thisField = "frmFlag_#typeName#_#FlagID#" >
				<CFINCLUDE TEMPLATE="../templates/getThisFieldValue.cfm">
				
				<CFIF getFlags.usevalidvalues is 1>
					<!--- WAB 2007-02-28 added a cftry to catch any valid value problems --->
					<cftry>
						<cf_displayValidValues FormFieldName="frmFlag_#typeName#_#FlagID#" 
						validFieldName = "flag.#getFlags.flagTextID#"
						parentValidFieldName = "flagGroup.#getFlags.flagGroupTextID#"
						multiple = true
						listsize="4"
						currentvalue=#trim(replace(thisfieldvalue,"'","","ALL"))#
						keepOrig = false
						countryid = "#request.relaycurrentuser.countrylist#"	
						>
						<!--- WAB 2009/03/25 added countryid to above.  Means that will bring back all valid values for all countries that the user has rights to --->
				
						<cfcatch>
							<cfif cfcatch.message contains "Valid Value Error">
								<CF_INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="#thisFieldValue#" SIZE=30 MAXLENGTH=50>
								<CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF>
							<cfelse>
								<cfrethrow>
							</cfif>
						</cfcatch>
					</cftry>
	
				<CFELSE>
					<CF_INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="#thisFieldValue#" SIZE=30 MAXLENGTH=50>
					<CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF>
				</cfif>
			</div>
		</CFOUTPUT>	

	</CFLOOP>

<CFELSE>

	<P>* No flag data for this Group</P>

</CFIF>




