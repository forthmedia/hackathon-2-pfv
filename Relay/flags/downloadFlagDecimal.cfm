<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Download Flags of type 12 = Decimal --->

<CFQUERY NAME="getFlags" datasource="#application.sitedatasource#">
	SELECT ISNULL(CONVERT(DECIMAL(36,2),DFD.data),0) data
	FROM Flag F
	LEFT JOIN decimalFlagData DFD ON DFD.FlagID = F.FlagID AND DFD.EntityID = <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
	WHERE F.FlagGroupID = <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFSET flagsQuery = "">

<CFIF getFlags.RecordCount IS NOT 0>
	<CFSET flagsQuery = ValueList(getFlags.data, delim)>
</CFIF>
