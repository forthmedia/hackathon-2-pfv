<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
IH	2012-09-18	Case 430433 fix formatting issue with nested tables
 --->

<CFPARAM name="flagfieldsize" default="30">
<CFPARAM name="flagfieldmaxlength" default="50">
<CFPARAM name="FlagNameOnOwnLine" default="0">
<CFPARAM name="displayas" default="">
<CFSET displayas = "Memo">

<CFINCLUDE TEMPLATE="qryFlag.cfm">
	
<CFIF #getFlags.RecordCount# IS NOT 0>

	<cfoutput><P><TABLE WIDTH="90%" BORDER="0" CELLSPACING="0" CELLPADDING="2" BGCOLOR="##00FFFC"<CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>></cfoutput>
	<CFLOOP QUERY="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
	
		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		
		<CFOUTPUT><TR><CFIF FlagFormatting.NoFlagName is not 1>  <TD VALIGN=TOP ALIGN=LEFT>

			<CFIF trim(namephrasetextid) is not ""> 
				phr_#htmleditformat(trim(namephrasetextid))#
			<CFELSE>
				#htmleditformat(Name)#
			</cfif>

		
		</TD></CFIF>
		<CFIF FlagFormatting.FlagNameOnOwnLine is 1></TR><TR></CFIF>

		<TD VALIGN=TOP ALIGN=LEFT><FONT FACE="Arial, Chicago, Helvetica" SIZE="3">

		</CFOUTPUT>
		
		<CFIF isdefined ("flag_#trim(getFlags.FlagTextID)#_ValidValues")>

				<!--- set variables and include template to display validvaluelist --->
				<CFSET formfield = "#typeName#_#FlagID#_#thisentity#">
				<CFSET validValueList = evaluate('flag_#getFlags.FlagTextID#_validvalues')>
				<CFSET currentValue = getFlagData.Data>

				<CFINCLUDE template="..\screen\showValidValueList.cfm">						

		<CFELSEIF displayas is "memo">
				<CFPARAM name="cols" default="40">							
				<CFPARAM name="rows" default="3">
				<cFOUTPUT><TextArea NAME="#typeName#_#FlagID#_#thisentity#" COLS="#cols#" ROWS="#rows#" WRAP="VIRTUAL">#HTMLEditFormat(trim(getFlagData.Data))#</TEXTAREA>
						<INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>#htmleditformat(getFlagData.Data)#</CFIF>"></cFOUTPUT>
		<CFELSE>
			<CFOUTPUT>
			<INPUT TYPE="text" NAME="#typeName#_#FlagID#_#thisentity#" VALUE="<CFIF getFlagData.RecordCount IS NOT 0>#htmleditformat(getFlagData.Data)#</CFIF>" SIZE=#htmleditformat(flagfieldsize)# MAXLENGTH=#htmleditformat(flagfieldmaxlength)#><BR>
			<INPUT TYPE="Hidden" NAME="#typeName#_#FlagID#_#thisentity#_orig" VALUE="<CFIF getFlagData.RecordCount IS NOT 0 and not isdefined("flag_#trim(getFlags.flagtextid)#_default")>#htmleditformat(getFlagData.Data)#</CFIF>">
			</CFOUTPUT>
		</CFIF>

		<cfoutput>
		<FONT SIZE=2><CFIF trim(descriptionphrasetextid) is not "">(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)<CFELSE><CFIF #Trim(Description)# IS NOT "">(<I>#htmleditformat(description)#</I>)</cfif></CFIF></FONT>
		</FONT></TD></TR>
		</cfoutput>
	</CFLOOP><cfoutput></TABLE></cfoutput>

<CFELSE>

	<cfoutput><P>* No flag data for this Group</cfoutput>

</CFIF>
<CFSET Displayas = "">


