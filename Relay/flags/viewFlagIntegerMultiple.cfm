<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Edit Flags of type  = Integer Multiple

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 

	2011/09/19 PPB LID6630 use phr_sys_NoData

--->


<CFINCLUDE TEMPLATE="qryFlag.cfm">


<CFIF getFlags.RecordCount IS NOT 0>

	<cfoutput>
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2">
	</cfif>
	<CFLOOP QUERY="getFlags">
	
		<CFINCLUDE TEMPLATE="qryFlagData.cfm">
		<cfparam name="labelTDwidth" type="numeric" default="300">			
		<cfif request.relayFormDisplayStyle neq "HTML-div">
			<TR>
			<CFIF FlagFormatting.NoFlagName is not 1>
				<td width="#labelTDwidth#" align="left" valign="top">
					<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a flagtextid there won't be any phrases --->
						phr_#htmleditformat(trim(namephrasetextid))#
					<CFELSE>
						#htmleditformat(Name)#
					</cfif>
				</td>
			</CFIF>
			
			<CFIF FlagFormatting.FlagNameOnOwnLine is 1></TR><TR></CFIF>
			
			<TD VALIGN=TOP ALIGN=LEFT>
			<CFIF getFlagData.RecordCount IS NOT 0>
				<cfloop query = "getFlagData">
					<cfif isDefined("getFlagData.data_name")>#htmleditformat(data_Name)#<cfelse>#htmleditformat(data)#</cfif><BR>
				</cfloop>
				
			<CFELSE>
				- phr_sys_NoData -
			</CFIF>
			<BR>
			<CFIF trim(descriptionphrasetextid) is not "">
				(<I>phr_#htmleditformat(descriptionphrasetextid)#</I>)
			<CFELSE>
				<CFIF Trim(Description) IS NOT "">(<I>#htmleditformat(description)#</I>)</cfif>
			</CFIF>
			</TD></TR>
		<cfelse>
			<CFIF FlagFormatting.NoFlagName is not 1>
				<label class="flagAttributeLabel">
					<CFIF trim(namephrasetextid) is not "">
						phr_#htmleditformat(trim(namephrasetextid))#
					<CFELSE>
						#htmleditformat(Name)#
					</cfif>
				</label>
			</CFIF>
			<span class="form-control">
				<CFIF getFlagData.RecordCount IS NOT 0>
					<cfloop query = "getFlagData">
						<cfif isDefined("getFlagData.data_name")>#htmleditformat(data_Name)#<cfelse>#htmleditformat(data)#</cfif>
					</cfloop>
				<CFELSE>
					- phr_sys_NoData -
				</CFIF>
			</span>
			<CFIF trim(descriptionphrasetextid) is not "">
				<span class="help-block">phr_#htmleditformat(descriptionphrasetextid)#
			<CFELSE>
				<CFIF Trim(Description) IS NOT ""><span class="help-block">#htmleditformat(description)#</span></cfif>
			</CFIF>
		</cfif>
		

	</CFLOOP>
	<cfif request.relayFormDisplayStyle neq "HTML-div"></TABLE></cfif>
	</cfoutput>
					
<CFELSE>

	<cfoutput><p>* No profile attributes for this profile</p></cfoutput>

</CFIF>



