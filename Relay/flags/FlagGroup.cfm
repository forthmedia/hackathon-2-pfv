<!--- �Relayware. All Rights Reserved 2014 --->
<!---

flaggroup.cfm

Author  Unknown

Purpose:  For outputting flag group forms.

Date:	back in the mists of time

Mods:

2005-04-12 	RND 	Added param, divs & conditional statements required for collapsible div elements.

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
NYB 2008/08/09  P-SOP003 - added ability to SortOrder different options via orderType.  Accepts: SortOrder, Alpha or Random
NYB 2009-02-27 	bug 1902 - Country Edit screen was throwing an error when you looked at a Country Group
WAB/SSS 2009/03/10   Mods to get the collapsing divs working (with changes to ascreen )
2016-03-09 	WAB	 PROD2016-684 Add support for divLayout
 --->

<CFSETTING enablecfoutputonly="yes">
<!--- Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit

	getFlagGroup.FlagGroupID

	--->
<cfparam name = "specialFormattingCollection" default = "#structNew()#">


<CFPARAM name="pageItemID" default="">
<CFPARAM name="required" default="false">

<!--- P-SOP003 2008/08/09 NYB - added to give further sortOrder options-> --->
<CFPARAM name="orderType" default="SortOrder">  <!--- options:  SortOrder, Alpha, Random --->
<!--- <-P-SOP003 --->





<CFSET EntityCountryList = "0,#variables.CountryID#">
<!--- START:  NYB 2009-02-27 bug 1902 - replaced ---
<CFIF #variables.CountryID# is 0 or application.countryGroupsBelongedTo[#variables.CountryID#] IS  "" >
<CFELSE>
	<CFSET EntityCountryList = listappend(EntityCountryList,application.countryGroupsBelongedTo[#variables.CountryID#])>
</CFIF>
!--- WITH:  NYB 2009-02-27 --->
<cftry>
	<CFIF not (#variables.CountryID# is 0 or application.countryGroupsBelongedTo[#variables.CountryID#] IS  "")>
		<CFSET EntityCountryList = listappend(EntityCountryList,application.countryGroupsBelongedTo[#variables.CountryID#])>
	</CFIF>
	<cfcatch type="any">

	</cfcatch>
</cftry>
<!--- END:  NYB 2009-02-27 --->

<!---  WAB: removed this code 1999-11-07, instead variables countryid and countrygroupids are passed from show screen  --->
<!---
	<CFIF isnumeric(thisentity)>
		<!--- Find Country for this Entity --->
		<CFQUERY NAME="getCountry" datasource="#application.siteDataSource#">
			SELECT DISTINCT l.CountryID
			<CFIF entityType IS 0>
			FROM Person As p, Location As l
			WHERE p.PersonID=	#thisentity#
			AND p.LocationID = l.LocationID
			<CFELSE>
			FROM Location As l
			WHERE l.LocationID=#thisentity#
			</CFIF>
		</CFQUERY>

		<CFSET eCountryID = getCountry.CountryID>

	<CFELSEIF isdefined("location_countryid_default")>

		<CFSET eCountryID = location_countryid_default>

	<CFELSE>

		<CFSET eCountryID = 0>

	</CFIF>

	<CFSET EntityCountryList = "0,#eCountryID#">

	<!--- Find all Country Groups for this Country --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE a.CountryID = #eCountryID# <!--- Entity Country --->
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryID
	</CFQUERY>

	<CFIF getCountryGroups.RecordCount IS NOT 0>
		<CFSET EntityCountryList =  EntityCountryList & "," & ValueList(getCountryGroups.CountryID)>
	</CFIF>

--->

<!--- NJH 2016/09/23 PROD2016-2383 - show flaggroups even if expiry not set --->
<CFQUERY NAME="getTopLevelFlags" datasource="#application.siteDataSource#">
	SELECT DISTINCT FlagGroup.FlagGroupID AS ID, flagGroup.FlagTypeID, OrderingIndex,
<!--- 	, Name, Description,namephrasetextid,descriptionphrasetextid  --->
	case when isNull(flaggroup.namePhrasetextID,'') <> '' then 'phr_' + flaggroup.namePhrasetextID else flaggroup.name end as Name,  <!--- WAB 2006-03-01 --->
	case when isNull(flaggroup.descriptionPhrasetextID,'') <> '' then 'phr_' + flaggroup.descriptionPhrasetextID else flaggroup.description end as description  <!--- WAB 2006-03-01 --->	,
	<!--- WAB 2010/09/07 added flagType Table when application.typelist removed--->
	flagType.name as TypeName,
	flagType.DataTable as TypeData,
	flagGroup.helpText
	FROM FlagGroup
		inner join flagType on flagGroup.flagTypeID = flagType.flagTypeID
	WHERE Active <> 0
	AND (Expiry is null or Expiry > #CreateODBCDateTime(Now())#)
	AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	AND FlagGroup.FlagGroupID =  <cf_queryparam value="#getFlagGroup.FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY OrderingIndex, Name
</CFQUERY>




	<CFSET firstpassTopLevel = true>
<CFLOOP QUERY="getTopLevelFlags">

	<cfset flagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(ID,specialFormattingCollection)>

	<CFPARAM name="flagGroupFormatting.NoFlagGroupName" default="0">
	<CFPARAM name="flagGroupFormatting.NoTopFlagGroupName" default="0">
	<CFPARAM name="flagGroupFormatting.NoFlagName" default="0">
	<CFPARAM name="flagGroupFormatting.FlagNameOnOwnLine" default="0">
	<CFPARAM name="flagGroupFormatting.NoFlagIndent" default="1">
	<CFPARAM name="flagGroupFormatting.Iscollapsible" default="1">
	<CFPARAM name="flagGroupFormatting.IsCollapsed" default="0">



	<CFSET thisFlagGroup = ID>
	<!--- NJH 2010/08/09 Removed code below for data in flag structures
	<CFSET typeName = ListGetAt(application.typeList,FlagTypeID)>
	<CFSET typeData = ListGetAt(application.typeDataList,FlagTypeID)> --->
	<CFSET variables.typeName = typeName>
	<CFSET variables.typeData = typeData>


	<!--- just line break between groups, not at end --->
	<CFIF firstPassTopLevel><CFSET firstPassTopLevel = false><CFELSE><CFOUTPUT><BR></cfoutput></CFIF>

	<CFQUERY NAME="getLowerLevelFlagGroups" datasource="#application.siteDataSource#">
		SELECT DISTINCT FlagGroup.FlagGroupID AS ID, flagGroup.FlagTypeID, OrderingIndex,
		<!--- Name, Description,namephrasetextid,descriptionphrasetextid --->
		case when isNull(flaggroup.namePhrasetextID,'') <> '' then 'phr_' + flaggroup.namePhrasetextID else flaggroup.name end as Name,  <!--- WAB 2006-03-01 --->
		case when isNull(flaggroup.descriptionPhrasetextID,'') <> '' then 'phr_' + flaggroup.descriptionPhrasetextID else flaggroup.description end as description,  <!--- WAB 2006-03-01 --->
		flagType.name as TypeName,
		flagType.DataTable as TypeData,
		flagGroup.helpText
		FROM FlagGroup
		inner join flagType on flagGroup.flagTypeID = flagType.flagTypeID

		WHERE Active <> 0
		AND (expiry is null or Expiry > #CreateODBCDateTime(Now())#)
		AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY OrderingIndex, Name
	</CFQUERY>

	<CFIF flagGroupFormatting.NoTopFlagGroupName is 0 and flagGroupFormatting.NoFlagGroupName is 0>
			<CFOUTPUT>
			<cfif request.relayFormDisplayStyle neq "HTML-div">
				<cfparam name="request.screenArrowUp" default="#request.currentSite.httpProtocol##cgi.SERVER_NAME#/images/MISC/iconDoubleArrowDown.gif">
				<DIV class="flagGroupName">
				<!--- <CFIF getLowerLevelFlagGroups.RecordCount IS 0> ---><cfif flagGroupFormatting.iscollapsible is true><img src="#request.screenArrowUp#" alt="Click to toggle section visibility" width="16" height="15" border="0" onClick="toggleVisibility( 'control_#pageitemid#_#ID#', this );"></cfif><!--- </cfif> --->
					<cfif required><span class="required"></cfif>#htmleditformat(Name)# <cfif required></span></cfif>
				</div>
			<cfelse>
				<!--- NJH 2015/09/10 - added an ID to the div so that we could get a unique handle --->
				<div <!---class="col-xs-12 col-sm-3 control-label"---> id="control__#getFlagGroup.flagGroupTextId neq ''?getFlagGroup.flagGroupTextId:getFlagGroup.flagGroupId#">
					<label<cfif required> class="required"</cfif>><cfif required><span class="required"></cfif>#htmleditformat(Name)# <cfif required></span></cfif></label>
					<CFIF flagGroupFormatting.NoTopFlagGroupName is 0 and flagGroupFormatting.NoFlagGroupName is 0>
						<CFIF Trim(helpText) IS NOT "">
							<CFOUTPUT>
							<span class="help-block">
							#htmleditformat(helpText)#
							</span>
							</CFOUTPUT>
						</CFIF>
					</CFIF>
				</div>
			</cfif>
			</CFOUTPUT>
	</CFIF>

	<cfif request.relayFormDisplayStyle neq "HTML-div"><cfoutput><div id="control_#pageitemid#_#ID#" <cfif flagGroupFormatting.iscollapsible is true><cfif flagGroupFormatting.isCollapsed >style="display:none;"</cfif></cfif>></cfoutput></cfif>

	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<CFIF flagGroupFormatting.NoTopFlagGroupName is 0 and flagGroupFormatting.NoFlagGroupName is 0>
				<CFIF Trim(helpText) IS NOT "">
					<CFOUTPUT>
					<DIV class="flagGroupHelpText">
					(#htmleditformat(helpText)#)
					</div>
					</CFOUTPUT>
				</CFIF>
		</CFIF>
	</cfif>
	<!--- <P><CFOUTPUT>#HTMLEditFormat(Name)# <CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF></CFOUTPUT> --->

	<CFIF getLowerLevelFlagGroups.RecordCount IS NOT 0>

		<CFIF flagGroupFormatting.NoFlagIndent is NOT 1 and request.relayFormDisplayStyle neq "HTML-div"><CFOUTPUT><UL></CFOUTPUT></cfif>

			<CFSET firstpassLowerLevel = true>
		<CFLOOP QUERY="getLowerLevelFlagGroups">

			<CFSET thisFlagGroup = ID>

			<cfset flagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(ID,specialFormattingCollection)>

			<CFPARAM name="flagGroupFormatting.NoFlagGroupName" default="0">
			<CFPARAM name="flagGroupFormatting.NoTopFlagGroupName" default="0">
			<CFPARAM name="flagGroupFormatting.NoFlagName" default="0">
			<CFPARAM name="flagGroupFormatting.FlagNameOnOwnLine" default="0">
			<CFPARAM name="flagGroupFormatting.NoFlagIndent" default="1">
			<CFPARAM name="flagGroupFormatting.Iscollapsible" default="1">
			<CFPARAM name="flagGroupFormatting.IsCollapsed" default="0">


			<!---
			WAB 2010/09/07 removed
			<CFSET typeName = ListGetAt(application.typeList,FlagTypeID)>
			<CFSET typeData = ListGetAt(application.typeDataList,FlagTypeID)>
			--->
			<CFSET variables.typeName = typeName>
			<CFSET variables.typeData = typeData>



			<!--- just line break between groups, not at end --->
			<CFIF firstPassLowerLevel><CFSET firstPassLowerLevel = false><CFELSE><CFOUTPUT><BR></cfoutput></CFIF>

			<CFIF flagGroupFormatting.NoFlagGroupName IS 0>
				<CFOUTPUT>
					<DIV class="flagGroupName">
						<cfif flagGroupFormatting.iscollapsible is true><img src="#request.currentSite.httpProtocol##cgi.SERVER_NAME#/images/MISC/iconDoubleArrowDown.gif" alt="Click to toggle section visibility" width="16" height="15" border="0" onClick="toggleVisibility( 'control_#pageitemid#_#thisFlagGroup#', this );"></cfif>
						<cfif required><span class="required"></cfif>#htmleditformat(Name)# <cfif required></span></cfif>
					</DIV>
				</CFOUTPUT>
			</CFIF>

			<cfif not request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput><div class="flagGroup1"<!--- class="col-xs-12 col-sm-9"--->></cfoutput>
			<cfelse>
				<cfoutput><div id="control_#pageitemid#_#thisFlagGroup#" <cfif #flagGroupFormatting.iscollapsible# is true><cfif flagGroupFormatting.isCollapsed >style="display:none;"</cfif></cfif>></cfoutput>
			</cfif>

			<CFIF flagGroupFormatting.NoFlagGroupName IS 0>
				<CFIF Trim(helpText) IS NOT "">
					<CFOUTPUT><DIV class="flagGroupHelpText">
						(<I>#htmleditformat(helpText)#</I>)
					</DIV></CFOUTPUT>
				</CFIF>
			</CFIF>

			<!--- <P><CFOUTPUT>#Name# <CFIF #Trim(Description)# IS NOT "">(<I>#Description#</I>)</CFIF></CFOUTPUT> --->
			<!--- <cfoutput>#flagMethod#Flag#typeName#.cfm</cfoutput> --->
			<!--- typeName should not be 'Group' but page exists for this case --->

			<cfif not request.relayFormDisplayStyle neq "HTML-div"><cfoutput><div class="flagGroup2"<!---class="col-xs-12 col-sm-9"--->></cfoutput></cfif>
			<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm"><BR><!--- WAB: try <BR> instead <P> --->
			<cfoutput></div></cfoutput>
		</CFLOOP>

		<CFIF flagGroupFormatting.NoFlagIndent is NOT 1 and request.relayFormDisplayStyle neq "HTML-div"><CFOUTPUT></UL></CFOUTPUT></CFIF>
	<CFELSE> <!--- i.e. wasn't a parent flag group  --->
		<CFIF typeName IS "Group">
			<CFOUTPUT>No flags available to #htmleditformat(flagmethod)#.</CFOUTPUT>
		<CFELSE>
			<CFIF flagGroupFormatting.NoFlagIndent is NOT 1><CFOUTPUT><UL></CFOUTPUT></CFIF>
			<!--- XXXX - points to the file that lists the profile attributes --->
			<cfif request.relayFormDisplayStyle eq "HTML-div"><cfoutput><div class="flagGroup3 <cfif divLayout is "horizontal">col-xs-12 col-sm-9</cfif>"></cfoutput></cfif>
			<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">
			<cfif not request.relayFormDisplayStyle neq "HTML-div">
				<cfoutput></div></cfoutput>
			<cfelse>
				<CFIF flagGroupFormatting.NoFlagIndent is NOT 1><CFOUTPUT></UL></CFOUTPUT></CFIF>
			</cfif>
		</CFIF>
	</CFIF>
	<cfif request.relayFormDisplayStyle neq "HTML-div"><CFOUTPUT></div></CFOUTPUT></cfif>
</CFLOOP>


<!--- Reset Defaults --->
<!--- <CFSET NoFlagGroupName ="0">
<CFSET NoTopFlagGroupName = "0">
<CFSET NoFlagName = "0">
<CFSET FlagNameOnOwnLine = "0"> --->
<CFSETTING enablecfoutputonly="no">
<CFSET Displayas = "">

