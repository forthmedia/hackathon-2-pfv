<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Called from data/personDedupeTask and data/locDedupeTask.cfm --->
<!--- Requires frmEntityID, frmDelEntityID and entityType to be defined before here --->

<!--- Dedupe flag data for these entities --->

<!--- Remember that there is a chance that a Location and Person have the same EntityID
so make sure we check that any amendments are only done to Flag data with the correct
Flag Group EntityTypeID --->

			<!--- <CFSET frmEntityID = 2>
			<CFSET frmDelEntityID = 1>
			<CFSET entityType = 0>
			<CFSET updatetime = CreateODBCDate(now())> --->
			
<!--- Archive Flag data about to be deleted or transferred to saved Entity --->
<CFPARAM NAME="archivetime" DEFAULT="#Now()#">
			
<CFLOOP LIST="Boolean,Date,Text,Integer" INDEX="tableType">
	<CFQUERY NAME="ArchiveFlags" datasource="#application.sitedatasource#">
	INSERT INTO x#tableType#FlagData
	(EntityID, FlagID,<CFIF tableType IS NOT "Boolean"> Data,</CFIF> CreatedBy, Created, LastUpdatedBy, LastUpdated, ArchivedBy, Archived)
	SELECT fd.EntityID, fd.FlagID,<CFIF tableType IS NOT "Boolean"> fd.Data,</CFIF> fd.CreatedBy, fd.Created, fd.LastUpdatedBy, fd.LastUpdated, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#archivetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
  	FROM #tableType#FlagData AS fd, Flag as f, FlagGroup as fg
 		WHERE fd.EntityID =  <cf_queryparam value="#frmDelEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
 		AND fd.FlagID = f.FlagID
 		AND f.FlagGroupID = fg.FlagGroupID
 		AND fg.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFLOOP>

<!--- Add flags from Entity being deleted to Entity being saved if they do not already exist--->
			
<CFLOOP LIST="Boolean,Date,Text,Integer" INDEX="tableType">
	<CFQUERY NAME="UpdateFlags" datasource="#application.sitedatasource#">
	UPDATE #tableType#FlagData
	SET EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
	    LastUpdated =  <cf_queryparam value="#archivetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
		LastUpdatedBy = #request.relayCurrentUser.usergroupid#
	WHERE EntityID =  <cf_queryparam value="#frmDelEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND FlagID IN
		(SELECT DISTINCT bfd.FlagID FROM #tableType#FlagData AS bfd, Flag as fa, FlagGroup as fga
		  WHERE bfd.EntityID =  <cf_queryparam value="#frmDelEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND bfd.FlagID = fa.FlagID
 			AND fa.FlagGroupID = fga.FlagGroupID
 			AND fga.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND bfd.FlagID NOT IN
			(SELECT t.FlagID FROM #tableType#FlagData AS t, Flag as f, FlagGroup as fg
			  WHERE t.EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			    AND t.FlagID = f.FlagID
 				AND f.FlagGroupID = fg.FlagGroupID
 				AND fg.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > ) )
	<CFIF tableType IS "Boolean">
	<!--- For radio buttons, we don't want to have two values for the same Flag Group --->
	AND FlagID NOT IN
		(SELECT rf.FlagID FROM Flag as rf, FlagGroup as rfg
		WHERE rf.FlagGroupID = rfg.FlagGroupID
		AND rfg.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND rfg.FlagTypeID = 3 <!--- Radio --->
		AND EXISTS (SELECT 1 FROM BooleanFlagData AS rfd, Flag As rfx WHERE rfd.FlagID = rfx.FlagID
		AND rfd.EntityID =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND rfx.FlagGroupID = rfg.FlagGroupID))
	</CFIF>
	</CFQUERY>
</CFLOOP>

<!--- Delete remaining flag data for deleted Entity --->		
<CFLOOP LIST="Boolean,Date,Text,Integer" INDEX="tableType">
	<CFQUERY NAME="DeleteFlags" datasource="#application.sitedatasource#">
	DELETE FROM #tableType#FlagData
 		WHERE EntityID =  <cf_queryparam value="#frmDelEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
 		AND FlagID IN
	(SELECT DISTINCT fd.FlagID FROM #tableType#FlagData AS fd, Flag as fa, FlagGroup as fga
	  WHERE fd.EntityID =  <cf_queryparam value="#frmDelEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND fd.FlagID = fa.FlagID
 			AND fa.FlagGroupID = fga.FlagGroupID
 			AND fga.EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>
</CFLOOP>




