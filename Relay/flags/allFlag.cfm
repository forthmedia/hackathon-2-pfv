<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			allFlag.cfm
Author:				BOCC
Date started:		1997

Description:

Returns a list of flagGroups with parent = 0 and parent =

Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view | search
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	CPS 	250501 		Set countryid to 0 for promotions entities
	SWJ		2002-03-05	Modified GroupList to work with all flag methods.
	SWJ 	2002-07-14	Modified getCountry query to have a default case for
						entities with no country scope see around line 81
	WAB 	2004-10-12	translations left to be done by cf_translate
	WAB 	2005-05-07		some of the translations

	WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
	NYB 2009-04-22 AllSites bug 2140
	NYB 2009-06-08 - Sophos Support LID 2260 - child flags on flaggroups in groups weren't showing up input boxes - replaced FileExists with cftry around CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm"

	WAB 2010/		Changes during 8.3
	WAB 2011/02/28  LID 5774 Problem with corruption of typeName variable.  Moved where it was set
2015-11-20          ACPK        PROD2015-294 Replaced UL elements and tables with Bootstrap
2016-03-09 	WAB	 PROD2016-684 Added support for divLayout 
Possible enhancements:


 --->
<CFPARAM name="required" default="0">  <!--- WAB 2006-02-10 --->
<CFPARAM name="divLayout" default="">

<!---
		DAM Parameters added 2000-05-15 see note below...
--->
<CFPARAM name="GroupList" default=""> <!--- GroupList is a list of flag groups that you want to limit all flags to return. --->
<CFPARAM name="GroupExceptions" default=""> <!--- GroupExceptions is a list of flag groups that you want to exclude from the returned results --->
<CFPARAM name="IncludeFlagList" default="">
<cfparam name="specialFormattingCollection" default = "#structNew()#">  <!--- usually set when called by screens, but not if called directly --->


<!--- Get USER countries --->
<cfinclude template="/templates/qrygetcountries.cfm">

<!--- <cfoutput>
	<CF_includeonce template="/templates/relayFormJavaScripts.cfm">
</cfoutput> --->
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<CFIF flagMethod IS "search">

	<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

	<!--- Find all Country Groups for this Country --->
	<!--- Note: no User Country rights implemented for site currently --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.sitedatasource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
	ORDER BY b.CountryID
	</CFQUERY>

	<CFSET UserCountryList = "0,#Variables.CountryList#">
	<CFIF getCountryGroups.CountryID IS NOT "">
		<!--- top record (or only record) is not null --->
		<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
	</CFIF>

<CFELSE>

	<CFIF thisEntity IS NOT 0>

		<CFIF "11" CONTAINS entityType>
			<CFSET eCountryId = 0>
		<CFELSE>
		<!--- Find Country for this Entity --->
		<CFQUERY NAME="getCountry" datasource="#application.sitedatasource#">
			SELECT DISTINCT l.CountryID
			<CFIF entityType IS 0>
				FROM Person As p, Location As l
				WHERE p.PersonID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND p.LocationID = l.LocationID
				<!--- 2012-07-25 PPB P-SMA001 commented out
				AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
				 --->
				#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			<CFELSEif entityType is 1>
				FROM Location As l
				WHERE l.LocationID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- 2012-07-25 PPB P-SMA001 commented out
				AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
				 --->
				#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			<CFELSEif entityType is 2>
				FROM organisation As l
				WHERE l.organisationID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- <cfif isDefined("countryScopeOrganisationRecords") and countryScopeOrganisationRecords eq 1> --->
				<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
					<!--- 11 July 2003 modified to show only if countryScopeOrganisationRecords
					is set to 1 in content/CFTemplates/relayINI.cfm --->
					<!--- 2012-07-25 PPB P-SMA001 commented out
					AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
					 --->
					#application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="l").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
				</cfif>
			<CFELSEif entityType is 23>
				FROM Opportunity as l
				WHERE l.opportunityid =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- GCC 2005-05-12 HACK - Probs with external users seeing opps allocated to them
				in countries they don't have rights to --->
				<cfif request.relaycurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType     CompareNoCase(usertype,'internal') eq 0--->
					<!--- 2012-07-25 PPB P-SMA001 commented out
					AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
					 --->
					#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="l").whereClause# 		<!--- 2012-07-25 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->
				</cfif>
			<cfelse><!--- SWJ 2002-07-14 default case added for all other entityType --->
				FROM country l
				where l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
			</CFIF>
		</CFQUERY>

		<CFSET eCountryID = getCountry.CountryID>
		</CFIF>

	<CFELSE>

		<CFSET eCountryID = frmCountryID>

	</CFIF>

	<!--- Find all Country Groups for this Country --->
	<!--- Note: no User Country rights implemented for site currently --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.SiteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE a.CountryID =  <cf_queryparam value="#eCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Entity Country --->
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryID
	</CFQUERY>

	<CFSET EntityCountryList = "0,#eCountryID#">

	<CFIF findCountries.RecordCount GT 0>
		<CFIF getCountryGroups.CountryID IS NOT "">
			<!--- top record (or only record) is not null --->
			<CFSET EntityCountryList = EntityCountryList & "," & ValueList(getCountryGroups.CountryID)>
		</CFIF>
	</CFIF>
</CFIF>

<!--- use tmpflagmethod in the sql queries, these use viewing where everything else uses view --->
<CFSET tmpflagmethod=iif(flagMethod is "view", DE("viewing"),DE("#flagmethod#"))  >

<CFQUERY NAME="getTopLevelFlags" datasource="#application.sitedatasource#">
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, flagGroup.FlagTypeID, OrderingIndex,
	flagGroup.Name, Description, NamePhraseTextId, DescriptionPhraseTextId,
	flagType.Name as typename,flagType.dataTable as typedata
	<CFIF flagMethod IS "edit">, 1 AS edit_view</CFIF>
	FROM FlagGroup
		inner join
		FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID
	WHERE Active <> 0
	AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
	AND ParentFlagGroupID = 0
	AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
	<CFIF flagMethod IS "search">
		AND Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	<CFELSE>
		AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
	AND ( <!--- removed by WAB - doesn't make sense - creator is likely to just be an administrator, shouldn't have automatic rights FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#OR --->
		 (#tmpFlagMethod#AccessRights = 0 AND FlagGroup.#tmpFlagMethod# <>0)
		OR (#tmpFlagMethod#AccessRights <> 0
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
				AND FlagGroupRights.#tmpFlagMethod# <>0)))
	<!---
		Added By DAM 2000-05-15

		New Parameter GroupList

		Permits execution of the page given a list of Group IDs allowing
		only specific Groups to be rendered.
		2002-03-05 SWJ changed the flagMethod="search" limit to allow all flagMethods

		New Parameter GroupExceptions

		The same only this time excludes given list of Group IDs
	--->
	<CFIF isDefined("GroupList") and GroupList IS NOT "">
			AND FlagGroup.FlagGroupID  IN ( <cf_queryparam value="#GroupList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  -- grouplist
	</CFIF>
 	<CFIF flagMethod IS "search" AND GroupExceptions IS NOT "">
			AND FlagGroup.FlagGroupID  NOT IN ( <cf_queryparam value="#GroupExceptions#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>

<CFIF flagMethod IS "edit">
	UNION
	SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagType.FlagTypeID, OrderingIndex, FlagGroup.Name, Description, NamePhraseTextId, DescriptionPhraseTextId,	flagType.Name as typename,flagType.dataTable as typedata,0
		FROM FlagGroup
		inner join
		FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID

		WHERE Active <> 0
		AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
		AND ParentFlagGroupID = 0
		AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<!--- wab removed to match removal above AND FlagGroup.CreatedBy<>#request.relayCurrentUser.usergroupid# --->
		AND ( (ViewingAccessRights = 0 AND FlagGroup.Viewing <>0)
			OR (ViewingAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights
					WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
					AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
					AND FlagGroupRights.Viewing <>0)))
		AND ( (EditAccessRights = 0 AND FlagGroup.Edit = 0)
		<!--- 	WAB: I think that this is wrong, needs a NOT EXISTS as below
			OR (EditAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights
					WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
					AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
					AND FlagGroupRights.Edit =0)) --->
			OR (EditAccessRights <> 0
				AND NOT EXISTS (SELECT 1 FROM FlagGroupRights
					WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
					AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
					AND FlagGroupRights.Edit <> 0))
				)
</CFIF>
ORDER BY OrderingIndex, flagGroup.Name
</CFQUERY>

<CFIF flagMethod IS "edit">

	<CFOUTPUT>	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmEntityID" VALUE="#thisEntity#"></CFOUTPUT>
	<CFSET flagList = "Group_0"> <!--- List of flags displayed --->

<CFELSEIF flagMethod IS "search">

	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="entityType" VALUE="#entityType#"></CFOUTPUT>
	<CFSET flagList = "Group_0"> <!--- List of flags displayed --->

</CFIF>

<CFSET tempMethod = "">

<cf_translate>
<CFLOOP QUERY="getTopLevelFlags">
    <div class="row form-group">
		<CFSET thisFlagGroup = ID>
		<CFIF flagMethod IS "edit">
			<CFSET this_edit_view = edit_view>
		</CFIF>
		<!--- NJH 2010/08/09 RW8.3 Removed code below for data in flag structures
		<CFSET typeName = #ListGetAt(application.typeList,FlagTypeID)#>
		<CFSET typeData = #ListGetAt(application.typeDataList,FlagTypeID)#>
		WAB replaced with fields in query
		<cfset typeName = application. flagGroup[thisFlagGroup].flagType.name>
		<cfset typeData = application. flagGroup[thisFlagGroup].flagType.datatable>
		--->

		<div <cfif divLayout is "horizontal">class="col-xs-4"</cfif>>
			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
				<CFOUTPUT><B>phr_#namephrasetextid#</B> <CFIF #Trim(Description)# IS NOT "">(<I>#Description#</I>)</CFIF></CFOUTPUT>
			<CFELSE>
				<CFOUTPUT><B>#Name#</B> <CFIF #Trim(Description)# IS NOT "">(<I>#Description#</I>)</CFIF></CFOUTPUT>
			</cfif>
		</div>

		<cfset FlagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(thisFlagGroup,specialFormattingCollection)>
		<CFPARAM name="flagGroupFormatting.NoFlagGroupName" default="0">
		<CFPARAM name="flagGroupFormatting.NoTopFlagGroupName" default="0">
		<CFPARAM name="flagGroupFormatting.NoFlagName" default="0">
		<CFPARAM name="flagGroupFormatting.FlagNameOnOwnLine" default="0">

		<CFQUERY NAME="getLowerLevelFlagGroups" datasource="#application.SiteDataSource#">
		SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagType.FlagTypeID, OrderingIndex, FlagGroup.Name, Description, NamePhraseTextId, DescriptionPhraseTextId,
		flagType.Name as typename,flagType.dataTable as typedata
		<CFIF flagMethod IS "edit">, 1 AS lower_edit_view</CFIF>
		FROM FlagGroup
			inner join FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID

		WHERE Active <> 0
		AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
		AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
		<CFIF flagMethod IS "search">
			AND Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<CFELSE>
			AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFIF>
		AND ( <!--- wab removed FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
		OR  --->(#tmpflagMethod#AccessRights = 0 AND FlagGroup.#tmpflagMethod# <> 0)
		OR (#tmpflagMethod#AccessRights <> 0
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups#)
				AND FlagGroupRights.#tmpflagMethod# <> 0)))



			<CFIF flagMethod IS "edit">
			UNION
			SELECT DISTINCT FlagGroup.FlagGroupID, FlagType.FlagTypeID, OrderingIndex, FlagGroup.Name, Description, NamePhraseTextId, DescriptionPhraseTextId, 	flagType.Name as typename,flagType.dataTable as typedata, 0
			FROM FlagGroup
			inner join
			FlagType on flagGroup.FlagTypeID = FlagType.FlagTypeID

			WHERE Active <> 0
			AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
			AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<!--- wab removed to match removal above AND FlagGroup.CreatedBy<>#request.relayCurrentUser.usergroupid# --->
			AND ( (ViewingAccessRights = 0 AND FlagGroup.Viewing <>0)
				OR (ViewingAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
						AND FlagGroupRights.Viewing <>0)))
			AND ( (EditAccessRights = 0 AND FlagGroup.Edit = 0)
			<!--- 	WAB: I think that this is wrong, needs a NOT EXISTS as below
				OR (EditAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
						AND FlagGroupRights.Edit =0)) --->
				OR (EditAccessRights <> 0
					AND NOT EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
						AND FlagGroupRights.Edit  <>0))
						)
			</CFIF>
		ORDER BY OrderingIndex, FlagGroup.Name
		</CFQUERY>

		<CFIF getLowerLevelFlagGroups.RecordCount IS NOT 0>
			 <div class="col-xs-8"></div>
	</div>
			<CFLOOP QUERY="getLowerLevelFlagGroups">
				<CFSET thisFlagGroup = ID>

				<cfset flagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(thisFlagGroup,specialFormattingCollection)>
				<CFPARAM name="flagGroupFormatting.NoFlagGroupName" default="0">
				<CFPARAM name="flagGroupFormatting.NoTopFlagGroupName" default="0">
				<CFPARAM name="flagGroupFormatting.NoFlagName" default="0">
				<CFPARAM name="flagGroupFormatting.FlagNameOnOwnLine" default="0">

				<!--- NJH 2010/08/09 RW8.3 Removed code below for data in flag structures
				<CFSET typeName = #ListGetAt(application.typeList,FlagTypeID)#>
				<CFSET typeData = #ListGetAt(application.typeDataList,FlagTypeID)#>

				<cfset typeName = application. flagGroup[thisFlagGroup].flagType.name>
				<cfset typeData = application. flagGroup[thisFlagGroup].flagType.datatable>
				--->

				<cfset typeName = getLowerLevelFlagGroups.typeName>
				<cfset typeData = getLowerLevelFlagGroups.typeData>

				<div class="row form-group">
					<div <cfif divLayout is "horizontal">class="col-xs-4"</CFIF>><LABEL>
						<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
							<CFOUTPUT><B>phr_#namephrasetextid#</B></CFOUTPUT>
						<CFELSE>
							<CFOUTPUT><B>#Name#</B></CFOUTPUT>
						</cfif>
						</LABEL>
					</div>
					<CFIF trim(descriptionphrasetextid) is not "">
						<CFOUTPUT>(<I>phr_#descriptionphrasetextid#</I>)</CFOUTPUT> <!--- GCC is this now right? Was namephrasetextid --->
					<CFELSE>
						<CFIF #Trim(Description)# IS NOT ""><CFOUTPUT>(<I>#Description#</I>)</CFOUTPUT> </CFIF>
					</CFIF>

					<CFIF flagMethod IS "edit">
						<CFIF lower_edit_view IS NOT 1 or this_edit_view is not 1>
							<CFSET flagMethod = "view">
							<CFSET tempMethod = "edit">
						</CFIF>
					</CFIF>

					<!--- typeName should not be 'Group' but page exists for this case --->
					<!--- NYB 2009-04-22 AllSites bug 2140 - added cfif around Include Template to stop error --->
					<!--- START: NYB 2009-06-08 - Sophos Support LID 2260 - replaced FileExists with cftry as this was no longer working --->
					<div <cfif divLayout is "horizontal">class="col-xs-8"</cfif>>
						<cftry>
							<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">
							<cfcatch type="">
							  <!--- do nothing --->
							</cfcatch>
						</cftry>
					</div>
					<!--- END: NYB 2009-06-08 - Sophos Support LID 2260 --->
					<CFIF tempMethod IS NOT "">
						<CFSET flagMethod = tempMethod>
						<CFSET tempMethod = "">
					</CFIF>
	           </div>
			</CFLOOP>
		<CFELSE>
		    <div class="col-xs-8">

				<cfset typeName = getTopLevelFlags.typeName>
				<cfset typeData = getTopLevelFlags.typeData>

				<CFIF #typeName# IS "Group">
					<p>No flags available to <CFOUTPUT>#flagmethod#</CFOUTPUT>.</p>
				<CFELSE>
					<CFIF flagMethod IS "edit">
						<CFIF this_edit_view IS NOT 1>
							<CFSET flagMethod= "view">
							<CFSET tempMethod = "edit">
						</CFIF>
					</CFIF>
					<!--- START: NYB 2009-06-08 - Sophos Support LID 2260 - added a cftry arouond Template - incase file doesn't exist --->
					<cftry>
					<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">
						<cfcatch type="">
						  <!--- do nothing --->
						</cfcatch>
					</cftry>
					<!--- END: NYB 2009-06-08 - Sophos Support LID 2260 --->
					<CFIF tempMethod IS NOT "">
						<CFSET flagMethod= tempMethod>
						<CFSET tempMethod = "">
					</CFIF>
				</CFIF>
            </div>
    </div>
	   </CFIF>
</CFLOOP>
</cf_translate>
<CFIF flagMethod IS "edit">
	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="flagList" VALUE="#flagList#"></CFOUTPUT>
<CFELSEIF flagMethod IS "search">
	<!--- <CFSET entityTable = #ListGetAt(application.typeEntityTable,entityType+1)#> --->
	<cfset entityTable = application.entityType[entityType].tablename>
	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="flagList#entityTable#" VALUE="#flagList#"></CFOUTPUT>
</CFIF>


