<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Edit Flags of type multiple integer


WAB 2004-18-08	added the form attribute when calling  cf_displayValidValues
WAB 2007-02-06 P-FNL041 start to use flagFormatting collection
WAB 2007-05-09 added entityid and country to validvalue query

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
WAB 2010/05/12 LID 3251
2016-03-09 	WAB	 PROD2016-684 Removed class flagAttributeLabel - should be same as any other label
--->



<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFLOOP QUERY="getFlags">

	<CFINCLUDE TEMPLATE="qryFlagData.cfm">

	<CFOUTPUT>
		<!--- NJH 2016/08/23 JIRA PROD2016-2204 - don't show flag label if already showing. Seems to be the correct way of doing this. stab in the dark! --->
		<CFIF not FlagFormatting.NoFlagName>
			<label class="<!--- flagAttributeLabel --->">
				<CFIF trim(namephrasetextid) is not "">
					phr_#htmleditformat(trim(namephrasetextid))#
				<CFELSE>
					#htmleditformat(Name)#
				</cfif>
			</label>
		</CFIF>
	</CFOUTPUT>


	<CFSET flagid = getflags.flagid>
	<CFSET flagList = flagList & ",#typeName#_#FlagID#">
	<CFPARAM name="displayAs" default= "">
	<CFIF displayAs is "">
		<CFSET displayAs = "twoselects">
	</cfif>
	<CFPARAM name="ListSize" default= "4">

	<CFPARAM name="thisFormName" default = "flagform">



	<CF_DisplayValidValues
		validFieldName = "flag.#getFlags.FlagTextID#"
		formFieldName =  "#typeName#_#FlagID#_#thisentity#"
		currentValue = "#valuelist(getFlagData.data)#"
		displayas = "#displayas#"
		multiple = true
		listSize = "#ListSize#"
		formname = "#thisFormName#"
		allowCurrentValue = true <!--- WAB 2010/04/12 LID 3251 will now display current value even if not in the validvalues --->
		inputOnNoValidValues = false
		noValidValuesText = " #application.delim1#No options available"
		 attributeCollection = "#flagFormatting#"
		EntityID = #thisEntity#
		countryid = #variables.countryid#
		>

	<CFOUTPUT>
		<CFIF trim(descriptionphrasetextid) is not "">
			<span class="help-block">phr_#htmleditformat(descriptionphrasetextid)#</span>
		<CFELSEIF Trim(helpText) IS NOT "">
			<span class="help-block">#htmleditformat(helpText)#</span>
		</CFIF>

		<cfif request.relayFormDisplayStyle neq "HTML-div" and FlagFormatting.FlagNameOnOwnLine is 1>
			<BR>
		</CFIF>
	</CFOUTPUT>






</CFLOOP>

