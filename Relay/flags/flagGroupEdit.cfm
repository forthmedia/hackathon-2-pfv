<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:			flagGroupEdit.cfm
Author:				BOCC
Date created:		Long time ago!

Description:		Edits the 

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ			Changed frmExpiry to use dateAdd
03-Jul-2000			SWJ			Added UseInSearchScreen processing
12-Jul-2000			SWJ			Added Javascript function to prefill flagGroupTextID
03-Aug-2000			DAM			Added TwoSelectRelated Tag to replace the hard coded person and location flag lists
								This required the removal of javasscript which had been used to determine
								what the value of frmParentFlagGroupID parameter should have been.
								There were also two queries which pulled back data for person and location flaggroups
								but this has been replaced by one that brings back all flaggroups
2000-09-15				WAB			Corrected DAM problem when no flaggroups already existed for given entitytype
2000-10-13			WAB			removed Javascript function to prefill flagGroupTextID because it creates invalid ones with spaces, odd characters etc. needs tobe modified and replaced

--->

<!--- the application .cfm checks for read adminTask privleges --->


<CFIF frmtask IS "add">

	<CFSET frmParentFlagGroupID = 0>
	<CFSET frmFlagTypeID = 0>
	<CFSET frmName = "">
	<CFSET frmDescription = "">
	<CFSET frmFlagGroupTextID = "">
	<CFSET frmNotes = "">
	<CFSET frmOrder = 1>
	<CFSET frmExpiry = "#LSDateFormat(DateAdd('yyyy', 4, Now ()), 'dd-mmm-yyyy')#">
	<CFSET frmScope = 0>
	<CFSET frmViewingAccessRights = 0>
	<CFSET frmEditAccessRights = 0>
	<CFSET frmDownloadAccessRights = 0>
	<CFSET frmSearchAccessRights = 0>	
	<CFSET frmEntityType = 0>
	<CFSET frmView = 1>
	<CFSET frmSearch = 0>
	<CFSET frmEdit = 0>
	<CFSET frmDownload = 0>
	<CFSET frmActive = 1>
	<CFSET frmPublicAccess = 0>
	<CFSET frmPartnerAccess = 0>
	<CFSET frmUseInSearchScreen = 0>

	<!--- get the user's countries --->
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

	<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
		SELECT DISTINCT 
			a.CountryID,
			a.CountryDescription
		FROM Country AS a
		WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		AND a.ISOcode IS NOT null
		ORDER BY a.CountryDescription
	</CFQUERY>
	
	<CFQUERY NAME="getCountrygroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT b.CountryDescription AS CountryGroup, b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryDescription
	</CFQUERY>
	
	<CFSET UserCountryList = "0,#Variables.CountryList#">
	<CFIF getCountryGroups.CountryID IS NOT "">
		<!--- top record (or only record) is not null --->
		<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
	</CFIF>

	<CFQUERY NAME="GetEntityTypes" datasource="#application.siteDataSource#">
		SELECT DISTINCT
		flagentitytype.entitytypeid, 
		tablename,
        FlagGroup.FlagGroupID,
        FlagGroup.Name
        FROM flagentitytype left outer join FlagGroup on flagentitytype.entitytypeid = FlagGroup.entitytypeid 
        WHERE 
			1=1
			AND
			ParentFlagGroupID = 0
			AND FlagTypeID = 1
			AND Active <> 0

			AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			AND ( FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
				OR 
					(FlagGroup.EditAccessRights = 0 AND FlagGroup.Edit <> 0)
				OR EXISTS 
						(		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
								WHERE  r.SecurityTypeID = s.SecurityTypeID 
								AND s.ShortName  = 'AdminTask'	
								AND r.usergroupid = rg.usergroupid
								AND rg.PersonID=#request.relayCurrentUser.personid# 
						)
				OR (FlagGroup.EditAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
						AND FlagGroupRights.Edit <> 0)))
			AND NOT EXISTS (SELECT 1 FROM Flag WHERE Flag.FlagGroupID = FlagGroup.FlagGroupID)
			
        ORDER BY 
			flagentitytype.entitytypeid, name
	</CFQUERY>
	
	
	<CFQUERY NAME="getFlagGroupTypes" datasource="#application.siteDataSource#">
	SELECT * FROM FlagType WHERE FlagTypeID <> 1 ORDER BY Name
	</CFQUERY>

<CFELSE>

	<!--- Check if this user has Edit rights for this group as well (as view) --->
	<!--- wab added bit so that adminstrators can do anything --->
	<CFQUERY NAME="checkEdit" datasource="#application.siteDataSource#">
	SELECT 1
	FROM FlagGroup
	WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND ( CreatedBy=#request.relayCurrentUser.usergroupid#
		OR (EditAccessRights = 0 AND Edit <> 0)
		or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID 
						AND s.ShortName  = 'AdminTask'	
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
		OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE EditAccessRights <> 0
                AND FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.personid#
                AND t.Edit <> 0)))
	</CFQUERY>
	
	<CFIF checkEdit.RecordCount IS 0>
		<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
		<CFINCLUDE TEMPLATE="flagGroupList.cfm">
		<CF_ABORT>
	</CFIF>

	<CFQUERY NAME="getFlagGroup" datasource="#application.siteDataSource#">
		SELECT DISTINCT FlagGroup.*, FlagType.Name As type_name
		FROM FlagGroup, FlagType
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND FlagGroup.FlagTypeID = FlagType.FlagTypeID
	</CFQUERY>

	<CFIF getFlagGroup.Scope IS NOT 0>
		<CFQUERY NAME="getScope" datasource="#application.siteDataSource#">
		SELECT CountryDescription From Country WHERE CountryID =  <cf_queryparam value="#getFlagGroup.Scope#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</CFIF>

	<CFIF getFlagGroup.ParentFlagGroupID IS NOT 0>
		<CFQUERY NAME="getParentFlagGroup" datasource="#application.siteDataSource#">
		SELECT Name FROM FlagGroup WHERE FlagGroupID =  <cf_queryparam value="#getFlagGroup.ParentFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</CFIF>

	<CFSET frmParentFlagGroupID = getFlagGroup.ParentFlagGroupID>
	<CFSET frmFlagTypeID = getFlagGroup.FlagTypeID>
	<CFSET frmName = "#getFlagGroup.Name#">
	<CFSET frmDescription = "#getFlagGroup.Description#">
	<CFSET frmFlagGroupTextID = "#getFlagGroup.FlagGroupTextID#">
	<CFSET frmNotes = "#getFlagGroup.Notes#">
	<CFSET frmOrder = getFlagGroup.OrderingIndex>
	<CFSET frmExpiry = #DateFormat(getFlagGroup.Expiry,"DD-MMM-YYYY")#>
	<CFSET frmScope = getFlagGroup.Scope>
	<CFSET frmViewingAccessRights = getFlagGroup.viewingAccessRights>
	<CFSET frmEditAccessRights = getFlagGroup.EditAccessRights>
	<CFSET frmDownloadAccessRights = getFlagGroup.DownloadAccessRights>
	<CFSET frmSearchAccessRights = getFlagGroup.SearchAccessRights>
	<CFSET frmEntityType = getFlagGroup.EntityTypeID>
	<CFSET frmView = getFlagGroup.Viewing>
	<CFSET frmSearch = getFlagGroup.Search>
	<CFSET frmEdit = getFlagGroup.Edit>
	<CFSET frmDownload = getFlagGroup.Download>
	<CFSET frmActive = getFlagGroup.Active>
	<CFSET frmPublicAccess = getFlagGroup.PublicAccess>
	<CFSET frmPartnerAccess = getFlagGroup.PartnerAccess>
	<CFSET frmUseInSearchScreen = getFlagGroup.UseInSearchScreen>

</CFIF>






<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
		
       function verifyForm(task,next) {
                var form = document.FlagGroupForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }

                if (msg != "") {
                        alert("\nYou must provide the following information for this flag group:\n\n" + msg);
                } else {
                	form.submit();
				}
        }
		
       function setParentLists(entity) {
                var form = document.FlagGroupForm;
				if (! form.frmEntityType[entity].checked) {
					form.frmEntityType[entity].click();
				} else {
					switch (entity) {
					case 0 :
						form.frmLParentFlagGroupID.selectedIndex = 1;
						break
					case 1 :
						form.frmPParentFlagGroupID.selectedIndex = 1;
						break
					}
				}
				if (entity == 0) {
					// Person flag group list should be used
					form.frmLParentFlagGroupID.selectedIndex = 0;
				} else {
					// Location flag group list should be used
					form.frmPParentFlagGroupID.selectedIndex = 0;
				}
	   }

	   function setTextID(){
	   		var form = document.FlagGroupForm;
<!--- WAB removed for time being - needs to strip out spaces and invalid characters --->
//			if (form.frmFlagGroupTextID.value == "") {
//				form.frmFlagGroupTextID.value = form.frmName.value
//			}
	   }
//-->

</SCRIPT>
</cf_head>



<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP"><H1>Edit Flag Group</H1></TD>
	<TD ALIGN="RIGHT" VALIGN="TOP">
	<CFIF #checkPermission.AddOkay# GT 0>
	<A HREF="#thisdir#/flagGroupEntityList.cfm" CLASS="SubMenu">Flag Groups</A>
	<A HREF="JavaScript:document.BackForm.submit();" CLASS="SubMenu">Back</A>
	</CFIF>
	</TD>
</TR>
</TABLE>
</CFOUTPUT>

<CENTER>

<CFIF IsDefined('message')>
	
	<CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>

</CFIF>

<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="380">
	
	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B>

	<CFIF frmtask IS "add">
		ADD NEW FLAG GROUP
	<CFELSE>
		FLAG GROUP DETAILS
	</CFIF>

	</B></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2>		
			<CFOUTPUT>Creator can always view, search, edit and download this flag group regardless of rights settings</CFOUTPUT>
		<BR></TD>
	</TR>
	
	<TR><TD COLSPAN="2" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2>
	<CFIF frmtask IS "edit">
		Entity type, parent, flag type and scope cannot be changed after creation.
		You may delete the flag group completely from the previous screen.
	<CFELSE>
		Only flag groups which you have edit and scope rights to, are not children
		and which do not have flags	themselves may be selected as a parent.
	</CFIF>
	<BR></TD>
	</TR>

</TABLE>

<P>
<FORM METHOD="POST" ACTION="flagGroupUpdateTask.cfm" NAME="FlagGroupForm">
<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
<CFIF frmTask IS "edit"><CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#"></CFIF>
<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="">
<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">
<INPUT TYPE="HIDDEN" NAME="frmExpiry_eurodate" VALUE="You must enter the expiry date in 'dd-mmm-yyyy' format.">
<INPUT TYPE="HIDDEN" NAME="frmPublicAccess_integer" VALUE="Field 'Public' must be an integer.">
<INPUT TYPE="HIDDEN" NAME="frmPartnerAccess_integer" VALUE="Field 'Partner Access' must be an integer.">


<TABLE BORDER=0 WIDTH=380>
<TR>
	<TD COLSPAN=3><B>
		Flag Group <CFIF frmtask IS "edit"><CFOUTPUT>'#htmleditformat(frmName)#' </CFOUTPUT></CFIF>Details 
		<BR><BR><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/misc/rule.gif" WIDTH=370 HEIGHT=3 ALT="" BORDER="0"><BR><BR></B>
	</TD>
</TR>
<TR>
	<TD ROWSPAN=7 WIDTH=30><FONT FACE="Arial, Chicago, Helvetica" SIZE=3>&nbsp;</TD>
	<TD VALIGN="TOP">
		Entity Type :
	</TD>
	<TD VALIGN=TOP>
		<CFIF frmtask IS "add">
<!---
		A

		This tag was added to genericise the selection of flag groups
		previously it was hard coded.
		
		
--->
		<CF_TwoSelectsRelated
			QUERY="GetEntityTypes"
			NAME1="FRMENTITYTYPE"
			NAME2="frmParentFlagGroupID"
			DISPLAY1="tablename"
			DISPLAY2="name"
			VALUE1="entitytypeid"
			VALUE2="FlagGroupID"
			FORCEWIDTH1="30"
			FORCEWIDTH2="60"
			SIZE1="1"
			SIZE2="auto"
			AUTOSELECTFIRST="Yes"
			EMPTYTEXT1="(Select Entity Type)"
			EMPTYTEXT2="(Select Flag Group)"
			FORCELENGTH2 = "6">	
		<CFELSE>
			<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmEntityType" VALUE="#frmEntityType#">
			<CFIF frmEntityType IS 0>
				<CF_INPUT TYPE="HIDDEN" NAME="frmPParentFlagGroupID" VALUE="#frmParentFlagGroupID#">
				Person <CFIF frmParentFlagGroupID IS NOT 0>(with parent #htmleditformat(getParentFlagGroup.Name)#)</CFIF>
			<CFELSE>
				<CF_INPUT TYPE="HIDDEN" NAME="frmLParentFlagGroupID" VALUE="#frmParentFlagGroupID#">
				Location <CFIF frmParentFlagGroupID IS NOT 0>(with parent #htmleditformat(getParentFlagGroup.Name)#)</CFIF>
			</CFIF>
			</CFOUTPUT>
		</CFIF>
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		Flag Type :
	</TD>
	<TD VALIGN=TOP>
		<CFIF frmtask IS "add">
			<SELECT NAME="frmFlagTypeID">
				<OPTION VALUE="1"<CFOUTPUT>#IIF(frmFlagTypeID IS 0, DE(" SELECTED"), DE(""))#</CFOUTPUT>> None (Parent)
				<OPTION VALUE="1">-----------------------------------------
				<CFOUTPUT QUERY="getFlagGroupTypes">
					<OPTION VALUE="#FlagTypeID#"#IIF(frmFlagTypeID IS FlagTypeID, DE(" SELECTED"), DE(""))#> #HTMLEditFormat(Name)#
				</CFOUTPUT>
			</SELECT><BR><BR>
		<CFELSE>
			<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTypeID" VALUE="#frmFlagTypeID#">
				<CFIF frmFlagTypeID IS 1>
					None
				<CFELSE>
					#htmleditformat(getFlagGroup.type_name)#
				</CFIF>
			</CFOUTPUT>
		</CFIF>
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		Name :
	</TD>
	<TD VALIGN=TOP>
		<CFOUTPUT><CF_INPUT TYPE="text" NAME="frmName" VALUE="#frmName#" SIZE="40" MAXLENGTH="250" onBlur="javascript:setTextID()"></CFOUTPUT>
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		Description (on screen help):
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="TEXT" NAME="frmDescription" VALUE="#frmDescription#" SIZE="40" MAXLENGTH="60">
	</TD>
</TR>

<TR>
	<TD VALIGN=TOP>
		FlagGroupTextID :
	</TD>
	<TD VALIGN=TOP>
	<CFIF Trim(frmFlagGroupTextID) IS NOT "">
		<CFOUTPUT>#HTMLEditFormat(Trim(frmFlagGroupTextID))#</CFOUTPUT><BR>
		<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupTextID" VALUE="#frmFlagGroupTextID#">
	<CFELSE>
	<CF_INPUT TYPE="TEXT" NAME="frmFlagGroupTextID" VALUE="#frmFlagGroupTextID#" SIZE="28" MAXLENGTH="30" ><BR>
		(not required, but once set cannot be changed)<BR>
	</CFIF>
	</TD>
</TR>

<TR>
	<TD VALIGN=TOP>
		Notes :
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="TEXT" NAME="frmNotes" VALUE="#frmNotes#" SIZE="28" MAXLENGTH="50"><BR><BR>
	</TD>
</TR>
<TR>
	<TD VALIGN="TOP">
		Ordering :
	</TD>
	<TD VALIGN=TOP>
		<SELECT NAME="frmOrder"><CFLOOP INDEX="LoopCount" FROM="1" TO="20">
			<CFOUTPUT>
			<CFIF LoopCount IS frmOrder>
				<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#
			<CFELSE>
				<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
			</CFIF>
			</CFOUTPUT>
		</CFLOOP></SELECT>
		<BR>Flag Groups with the same Ordering Index are sorted alphabetically.
	</TD>
</TR>
</TABLE>

<P><TABLE BORDER=0 WIDTH=380>
<TR>
	<TD COLSPAN=4><B>
		Display Details 
		<BR><BR><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/misc/rule.gif" WIDTH=370 HEIGHT=3 ALT="" BORDER="0"><BR><BR></B>
	</TD>
</TR>
<TR>
	<TD ROWSPAN=11 WIDTH=30>&nbsp;</TD>
	<TD VALIGN=TOP>
		Scope :
	</TD>
	<TD VALIGN=TOP COLSPAN=2>
		<CFIF frmTask IS "Add">
			<SELECT NAME="frmScope">
				<OPTION VALUE="0"<CFOUTPUT>#IIF(frmScope IS "0", DE(" SELECTED"), DE(""))#</CFOUTPUT>> All Countries/Groups
				<CFOUTPUT QUERY="getCountryGroups">
					<OPTION VALUE="#CountryID#"#IIF(frmScope IS CountryID, DE(" SELECTED"), DE(""))#> #HTMLEditFormat(CountryGroup)#
				</CFOUTPUT>
					<OPTION VALUE="0">-----------------------------------------
				<CFOUTPUT QUERY="getCountries">
					<OPTION VALUE="#CountryID#"#IIF(frmScope IS CountryID, DE(" SELECTED"), DE(""))#> #HTMLEditFormat(CountryDescription)#
				</CFOUTPUT>
			</SELECT><BR><BR>
		<CFELSE>
			<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmScope" VALUE="#frmScope#">
			<CFIF frmScope IS NOT 0>
				#htmleditformat(getFlagGroup.scope)#
			<CFELSE>
				All Countries/Groups
			</CFIF>
			</CFOUTPUT>
		</CFIF>
	</TD>
</TR>
<TR>
	<TD VALIGN="TOP" ROWSPAN=2>
		These have user <BR>specific rights
	</TD>
	<TD VALIGN=TOP> 
		<CF_INPUT TYPE="Checkbox" NAME="frmViewingAccessRights" VALUE="1"  CHECKED="#iif(frmViewingAccessRights IS NOT 0,true,false)#"> Viewing
	</TD>
	<TD VALIGN=TOP> 
		<CF_INPUT TYPE="Checkbox" NAME="frmEditAccessRights" VALUE="1"  CHECKED="#iif(frmEditAccessRights IS NOT 0,true,false)#"> Edit
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP> 
		<CF_INPUT TYPE="Checkbox" NAME="frmDownloadAccessRights" VALUE="1"  CHECKED="#iif(frmDownloadAccessRights IS NOT 0,true,false)#"> Download
	</TD>
	<TD VALIGN=TOP> 
		<CF_INPUT TYPE="Checkbox" NAME="frmSearchAccessRights" VALUE="1"  CHECKED="#iif(frmSearchAccessRights IS NOT 0,true,false)#"> Search
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP ROWSPAN=2>
		These are the <BR>standard rights
	</TD>
	<TD VALIGN=TOP>
		&nbsp;&nbsp;&nbsp; <CF_INPUT TYPE="CHECKBOX" NAME="frmView" VALUE="1"  CHECKED="#iif(frmView IS NOT 0,true,false)#"> View
	</TD>
	<TD VALIGN=TOP>
		&nbsp; <CF_INPUT TYPE="CHECKBOX" NAME="frmSearch" VALUE="1"  CHECKED="#iif(frmSearch IS NOT 0,true,false)#"> Search
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		&nbsp;&nbsp;&nbsp; <CF_INPUT TYPE="CHECKBOX" NAME="frmEdit" VALUE="1"  CHECKED="#iif(frmEdit IS NOT 0,true,false)#"> Edit<BR><BR>
	</TD>
	<TD VALIGN=TOP>
		&nbsp; <CF_INPUT TYPE="CHECKBOX" NAME="frmDownload" VALUE="1"  CHECKED="#iif(frmDownload IS NOT 0,true,false)#"> Download<BR><BR>
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		Expires :
	</TD>
	<TD VALIGN=TOP COLSPAN=2>
		<CF_INPUT TYPE="TEXT" NAME="frmExpiry" VALUE="#frmExpiry#" SIZE=11 MAXLENGTH=11> (dd-mmm-yyyy)<BR><BR>
	</TD>
</TR>
<TR>
	<TD VALIGN=TOP>
		Active :
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="RADIO" NAME="frmActive" VALUE="0"  CHECKED="#iif(frmActive IS 0,true,false)#"> No &nbsp;&nbsp;<BR><BR>
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="RADIO" NAME="frmActive" VALUE="1"  CHECKED="#iif(frmActive IS NOT 0,true,false)#"> Yes <BR><BR>
	</TD>
</TR>
<TR>
	<TD VALIGN="TOP">
		Public : <BR>
		Please set to 1 <BR>
		To use in locator
	</TD>
	<TD VALIGN=TOP COLSPAN=2>
		<CF_INPUT TYPE="TEXT" NAME="frmPublicAccess" VALUE="#frmPublicAccess#" SIZE=3 MAXLENGTH=5> (Integer)
	</TD>
</TR>
<TR>
	<TD VALIGN="TOP">
		Partner Access :
	</TD>
	<TD VALIGN=TOP COLSPAN=2>
		<CF_INPUT TYPE="TEXT" NAME="frmPartnerAccess" VALUE="#frmPartnerAccess#" SIZE=3 MAXLENGTH=5> (Integer)
	</TD>
</TR>

<TR>
	<TD VALIGN="TOP">
		Use in main search <BR>screen flag list :
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="RADIO" NAME="frmUseInSearchScreen" VALUE="0"  CHECKED="#iif(frmUseInSearchScreen IS 0,true,false)#"> No &nbsp;&nbsp;<BR>
	</TD>
	<TD VALIGN=TOP>
		<CF_INPUT TYPE="RADIO" NAME="frmUseInSearchScreen" VALUE="1"  CHECKED="#iif(frmUseInSearchScreen IS NOT 0,true,false)#"> Yes <BR>
	</TD>

</TR>

</TABLE>


</FORM>

<CFOUTPUT>

<P><A HREF="JavaScript:verifyForm('#frmTask#','flagGroupShow');">Save and Return</A>
<CFIF #frmTask# IS "add">
 &nbsp; &nbsp;
<A HREF="JavaScript:verifyForm('#frmTask#','flagGroupEdit');">Save and Add New</A>
</CFIF>
</CFOUTPUT>

<FORM METHOD="POST" ACTION="<CFOUTPUT>#htmleditformat(frmBack)#</CFOUTPUT>.cfm" NAME="BackForm">
<INPUT TYPE="HIDDEN" NAME="null" VALUE="0">
<CFIF frmTask IS "edit">
<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
</CFIF>
</FORM>
										
<P>


</CENTER>
</FORM>







