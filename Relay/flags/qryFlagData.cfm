<!--- ©Relayware. All Rights Reserved 2014 --->
<!--- 

Flag data query used for all Flag types 

	Requires typeData eg Char,Date,Integer
and thisEntity eg 999087 - PersonID or LocationID as Long Integer 


WAB 2007-02-06   P-FNL041 added handling for collection of formattingParameters 
WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
NYB 2012-01-11	Case#424703 wrapped entire process around 'if thisFlagStructure.isOK'
WAB 2013-11-21	CASE 438115 
--->

<cfset thisFlagStructure = application.com.flag.getFlagStructure(getFlags.FlagID)>

<cfif thisFlagStructure.isOK>
	<CFIF thisEntity IS 0 or not isnumeric(thisEntity)>
		<CFIF isdefined("flag_#trim(getFlags.flagtextid)#_default")>
			<!--- this allows default flag values to be set for new records, not fully tested --->
			<Cfset theVar = evaluate("flag_#getFlags.flagtextid#_default")>
			<CFQUERY NAME="getFlagData" datasource="#application.siteDataSource#">
				SELECT 	'#evaluate("#theVar#")#' as data
				FROM dual
			</CFQUERY>
		<CFELSE>
			<CFQUERY NAME="getFlagData" datasource="#application.siteDataSource#">
				SELECT 1 as data
				FROM FlagGroup
				WHERE 1 = 0
			</CFQUERY>
		</CFIF>
	<CFELSE>
	
		<!--- NJH 2012/08/06 - CASE 428311 changed to use person name --->
		<cfif getFlags.linkstoentityTypeID is "">
			<CFQUERY NAME="getFlagData" datasource="#application.siteDataSource#">
				SELECT fd.*, 
				case when p.personID is not null then firstname+' '+lastname + ' ('+ name +')' else name end as name,
				case when p.personID is not null then firstname+' '+lastname + ' ('+ name +')' else name end as lastUpdatedByName  <!--- WAB added this alias to make clearer and because the similar flag.cfc function uses name for the entity's name , will remove the name column in due course--->
				<cfif thisFlagStructure.hasMultipleInstances >
				, #typeData#FlagDataID as identity_
				</cfif>
				FROM #typeData#FlagData fd
				left outer join person p on p.personID = fd.lastUpdatedByPerson <!--- 2006-03-14 SWJ added join to display last updated data --->
				left outer join usergroup u on u.userGroupID = fd.lastUpdatedBy
				WHERE FlagID =  <cf_queryparam value="#getFlags.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND EntityID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
		<cfelse>
			<!--- 
				WAB 2006-12-05
				for flags where we know the data is actually an entityid, this function brings back the entityname as data_name
				function could be used to replace the one above in all cases - but would need lots of testing (and might be slower)
			 --->
			<cfset getFlagData = application.com.flag.getFLagData(flagid =getFlags.FlagID, entityID = thisEntity,returnLastUpdatedByName = true, returnEntityDetails = false )>		
		</cfif>
	</CFIF>
		
	<cfif formattingParameters is not "">
		<!--- create flagFormatting structure by merging defaults with flagGroupAndFlag formattingParameters --->
		<cfset flagFormatting = application.com.structureFunctions.structMerge(struct1 = flagGroupFormatting,struct2 = thisFlagStructure.FormattingParametersStructure, mergeToNewStruct = true)>
		<!--- merge in items defined in specialformattingcollection which override any settings  --->
		<cfset flagFormatting = application.com.structureFunctions.structMerge(struct1 = flagFormatting,struct2 = specialformattingcollection)>
	<cfelse>
		<cfset flagFormatting = flagGroupFormatting>
	</cfif>
	
	<cfif (getFlags.useValidValues is 1 or getFlags.lookup is 1) and flagmethod is "edit" >
	<!--- if necessary get a list of valid values --->	
	<!--- 	<CFIF ListContainsNocase(flagformat, "lookupdisplay=") is not 0 >
			<CFSET lookupdisplay = listgetat(listgetat(flagformat, ListContainsNocase(flagformat, "lookupdisplay="))  ,2,"=")>
		<CFELSE>	
			<CFSET lookupdisplay = "">
		</CFIF>	
		<CFSET lookupvalue = "">
		<CFSET fieldname = "flag_#getFlags.FlagTextID#">
		<cfinclude template="/screen/getValidValueList.cfm">	
		 --->
		<cfset EntityId = thisEntity>	 	<!--- WAB added 2006-05-22 don't have any other way of getting the entityid into the validvalues code which will look for caller.entityID--->
		<!--- CASE 438115 WAB 2013-11-21 deal with entityID being non numeric (for new records), 
			 causes an error if validvalues refers to entityID.  Of course the valid values won't make sense for a new record, but at least will now not cause an error --->
		<cf_getValidValues 
			validfieldname = "flag_#getFlags.FlagTextID#"  
			shownull=false 
			countryid = #variables.countryid# 
			EntityID = #iif(isNumeric(thisEntity),de(thisEntity),0)#
			>
		<cfset "flag_#getFlags.FlagTextID#_validvalues" = validvalues>
	
	</CFIF>
		

	<!--- 2008/05/08 GCC this section of code returns the lookup list not the current value. Is it needed because view mode works better without it?! --->
	<cfif (getFlags.lookup is 1) and flagmethod is "view" AND getFlags.linksToEntityTypeID is "">		
		<!--- need to lookup this value --->
		<!--- won't work with boolean flags - ought to check --->
		
		<!--- 			<CFIF ListContainsNocase(flagformat, "lookupdisplay=") is not 0 >
						<CFSET lookupdisplay = listgetat(listgetat(flagformat, ListContainsNocase(flagformat, "lookupdisplay="))  ,2,"=")>
					<CFELSE >	
						<CFSET lookupdisplay = "">
					</CFIF>	
		
					<CFSET lookupvalue = getflagdata.data>
					<CFSET fieldname = "flag_#getFlags.FlagTextID#">
									
					<CFINCLUDE template="..\screen\doLookup.cfm">
		 --->
		<cf_doValidValueLookup 
			validfieldname = "flag_#getFlags.FlagTextID#"
			lookupvalue = #getflagdata.data#
			countryid = #variables.countryid# 
			EntityID = #thisEntity#  <!--- WAB added  2007-02-05--->
		>	

		<!--- WAB 2015-06-23 Very old piece of code which didn't work with dbType=query but will work as a bit of normal SQL so removed the dbType  --->
		<CFQUERY NAME="getFlagData" >
			SELECT 	'#lookupresult#' as data   <!--- note this is the display value, not the actual data.  Shouldn't be a problem since this only used in view mode --->			
		</CFQUERY>
	</CFIF>


	<!--- if there is no record for this flag, then we can return the default value --->
	<CFIF getFlagData.recordcount IS 0 and isdefined("flag_#trim(getFlags.flagtextid)#_default")>
		<Cfset theVar = evaluate("flag_#getFlags.flagtextid#_default")>
		<CFQUERY NAME="getFlagData" datasource="#application.siteDataSource#">
			SELECT 	'#evaluate("#theVar#")#' as data
			FROM dual
		</CFQUERY>
	</cfif>
				
		
	<CFIF getFlags.WDDXStruct is not "">
		<CFSET wddxQuery = application.com.flag.convertFlagWDDXToQuery(getFlagData.data,getFlags.WDDXStruct)>
		<cfquery name = "getFlagData" dbtype="query">
			select wddxQuery.*, 
			'#getFlagData.data#' as data, <!--- WAB 2008/04/28 added this back into the query so that in cases when the screenhasn't been set up properly, the data is still output and the screen does not fall over--->
			arrayindex as identity_ from 
			wddxQuery
		</cfquery> 
	</cfif> 
	
</cfif>

