<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Search Flags of type 4 = Date --->
<CFINCLUDE template="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>
	
		<CFLOOP query="getFlags">
			<CFSET flagList = flagList & ",#typeName#_#FlagID#">
			<CFOUTPUT>
				<div class="form-group">
					<label for="frmFlag_#typeName#_#FlagID#">#HTMLEditFormat(Name)#</label>			
					<!--- <INPUT TYPE="text" NAME="frmFlag_#typeName#_#FlagID#" VALUE="" SIZE=12> --->
					
					<CF_relayDateField	
						currentValue=""
						thisFormName="ThisForm"
						fieldName="frmFlag_#typeName#_#FlagID#"
						anchorName="anchor1X"
						helpText=""
					>
					
					<BR><CFIF #Trim(Description)# IS NOT "">(<I>#HTMLEditFormat(Description)#</I>)</CFIF>
				</div>
			</CFOUTPUT>
		</CFLOOP>
<CFELSE>
<P>* No flag data for this Group</P>
</CFIF>