<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Spacing issue between label and texboxes when merging location records
2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Added labelTDwidth 
2016-03-09 	WAB	 PROD2016-684 Removed class flagAttributeLabel - should be same as any other label
--->
<cfparam name="labelTDwidth" type="numeric" default="300">
<CFINCLUDE TEMPLATE="qryFlag.cfm">

<CFIF getFlags.RecordCount IS NOT 0>

	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput><TABLE BORDER=0 WIDTH="90%" CELLSPACING="0" CELLPADDING="2" <CFIF flagGroupFormatting.NoFlagName is 1> class="hideTableFormatting"</cfif>></cfoutput><!--- 2014-11-24 PPB P-KAS048 removed BGCOLOR="white" --->
	</cfif>
	<CFLOOP QUERY="getFlags">
	
		<CFSET flagList = flagList & ",#typeName#_#FlagID#">
		<cfset flagControls = application.com.flag.getFlagDataAndControls(flagID=FlagID,entityID=thisentity,countryid=variables.countryid,additionalFormatting=specialFormattingCollection,method=flagMethod)>
		
		<CFOUTPUT>
			<cfif request.relayFormDisplayStyle neq "HTML-div"><TR></cfif>
				<CFIF flagControls.flagFormatting.NoFlagName is not 1>
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						<!--- 2015-02-12	RPW	2015 Roadmap FIFTEEN-124 - Added labelTDwidth --->
						<TD VALIGN=TOP ALIGN=LEFT width="#labelTDwidth#">
					<cfelse>
						<label class="<!--- flagAttributeLabel --->">
					</cfif>
						#flagControls.label#
					<cfif request.relayFormDisplayStyle neq "HTML-div">
						</TD>
					<cfelse>
						</label>
					</cfif>
				</CFIF>
				<CFIF flagControls.flagFormatting.FlagNameOnOwnLine is 1 and request.relayFormDisplayStyle neq "HTML-div">
					</TR>
					<TR>
				</CFIF>
				<cfif request.relayFormDisplayStyle neq "HTML-div"><TD VALIGN=TOP ALIGN=LEFT></cfif>
					#flagControls.control_html#
				<cfif request.relayFormDisplayStyle neq "HTML-div">
				</TD>
			</TR>
			</cfif>
		</CFOUTPUT>

	</CFLOOP>
	<cfif request.relayFormDisplayStyle neq "HTML-div">
		<cfoutput></TABLE></cfoutput>
	</cfif>
					
<CFELSE>

	<cfoutput><P>* No attribute data for this Profile</cfoutput>

</cfif>