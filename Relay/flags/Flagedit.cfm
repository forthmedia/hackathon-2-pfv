<!--- �Relayware. All Rights Reserved 2014 --->
<!--- the application .cfm checks for read adminTask privleges --->

<!--- Amendment History
2000-10-13			WAB			removed Javascript function to prefill flagTextID because it creates invalid ones with spaces, odd characters etc. needs tobe modified and replaced
12-Jul-2000			SWJ			Added Javascript function to prefill flagTextID
2000-02-25		WAB		Added code to set use of valid values and lookup
1999-02-20		WAB		Add Entering of FlagTextID

--->


<!--- Check if this user has Edit rights for this group as well (as view) --->
<CFQUERY NAME="checkEdit" datasource="#application.siteDataSource#">
SELECT 1
FROM FlagGroup
WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND (CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (EditAccessRights = 0 AND Edit <> 0)
	OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE EditAccessRights <> 0
                AND FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.personid#
                AND t.Edit <> 0)))
</CFQUERY>

<CFIF checkEdit.RecordCount IS 0>
	<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
	<CFINCLUDE TEMPLATE="flagGroupList.cfm">
	<CF_ABORT>
</CFIF>

<CFIF frmtask IS "add">

	<CFSET frmName = "">
	<CFSET frmHelpText = "">
	<CFSET frmFlagTextID = "">
	<CFSET frmOrder = 1>
	<CFSET frmActive = 1>
	<CFSET frmlookup = 0>
	<CFSET frmuseValidValues = 0>
	<CFSET frmWddxStruct = "">
		
	<CFQUERY NAME="getMaxOrder" datasource="#application.sitedatasource#">
	SELECT max (f.OrderingIndex) AS order_max, flagtypeid
	FROM Flag as f, FlagGroup as fg
	WHERE f.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND f.FlagGroupID = fg.FlagGroupID 
	group by flagtypeid
	</CFQUERY>
	
	<CFIF getMaxOrder.order_max IS NOT "" AND getMaxOrder.order_max LT 19>
		<CFSET frmOrder = getMaxOrder.order_max + 1>
	</CFIF>

	<CFSET frmflagTypeID = getMaxOrder.FlagTypeID>

<CFELSE>

	<CFQUERY NAME="getFlag" datasource="#application.siteDataSource#">
	SELECT f.Name, f.helpText, f.FlagTextID, 	f.OrderingIndex, f.Active, fg.flagTypeID, f.usevalidValues, f.lookup, f.WDDXStruct
	FROM Flag as f, FlagGroup as fg
	WHERE f.FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and f.flaggroupid = fg.flaggroupid
	</CFQUERY>

	<CFSET frmName = getFlag.Name>
	<CFSET frmHelpText = getFlag.helpText>
	<CFSET frmFlagTextID = getFlag.FlagTextID	>
	<CFSET frmOrder = getFlag.OrderingIndex>
	<CFSET frmActive = getFlag.Active>
	<CFSET frmuseValidValues = getFlag.useValidValues>
	<CFSET frmLookup = getFlag.lookup>
	<CFSET frmflagTypeID = getFlag.FlagTypeID>
	<CFSET frmWDDXStruct = getFlag.WDDXStruct>
	
</CFIF>

<CFQUERY NAME="getFlags" datasource="#application.siteDataSource#">
SELECT Name, OrderingIndex
FROM Flag
WHERE Flag.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
ORDER BY OrderingIndex, Name
</CFQUERY>






<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
		
       function verifyForm(task,next) {
                var form = document.FlagForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }
                if (msg != "") {
                        alert("\nYou must provide the following information for this flag:\n\n" + msg);
                } else {
                	form.submit();
				}
        }

        function validValues(fieldName) {

				newWin = window.open ('<CFOUTPUT>#jsstringFormat(request.currentsite.protocolAnddomain)#</cfoutput>/qa/validvalues.cfm?frmFieldName='+fieldName+'&frmCountryID=0','PopUp','height=500,width=500,scrollbars=1,resizable=1')	
				newWin.focus()

		}

	   function setTextID(){
	   		var form = document.FlagForm;
<!--- WAB removed, needs to be changed to strip out invalid characters --->
//			if (form.frmFlagTextID.value == "") {
//				form.frmFlagTextID.value = form.frmName.value
//			}
	   }


//-->

</SCRIPT>
</cf_head>



<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
<TD ALIGN="LEFT" VALIGN="TOP"><IMG SRC="/images/misc/title_admin_e.gif" WIDTH=180 HEIGHT=23 ALT="" BORDER="0"></TD>
<TD ALIGN="RIGHT" VALIGN="TOP">
<CFIF #checkPermission.AddOkay# GT 0>
<A HREF="#thisdir#/flagGroupList.cfm"><IMG SRC="/images/buttons/txt_flaggrp_e.gif" WIDTH=119 HEIGHT=18 ALT="Flag Groups" BORDER="0"></A>
<A HREF="JavaScript:document.BackForm.submit();"><IMG SRC="/images/buttons/txt_back_e.gif" WIDTH=67 HEIGHT=18 ALT="Back" BORDER="0"></A>
</CFIF>
</TD>
</TR>
</TABLE>
</CFOUTPUT>

<CENTER>

<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="90%">
	
	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT COLOR="#FFFFFF"><B>

	<CFIF frmtask IS "add">
		ADD NEW FLAG to '<CFOUTPUT>#UCase(HTMLEditFormat(Trim(flagGroupName)))#</CFOUTPUT>'
	<CFELSE>
		FLAG DETAILS
	</CFIF>

	</B></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER">

		<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
			<TR><TD VALIGN="TOP">
					Help Text:
			</TD>
			<TD VALIGN="TOP">
					<CFOUTPUT>#HTMLEditFormat(Trim(flagGroupHelpText))#</CFOUTPUT>
			</TD></TR>				
			<TR><TD VALIGN="TOP">
					Scope (Entity Type):
			</TD>
			<TD VALIGN="TOP">
					<CFOUTPUT>#HTMLEditFormat(Trim(flagGroupScEn))#</CFOUTPUT>
			</TD></TR>
			<TR><TD VALIGN="TOP">
					Existing Flags (Order/Name):
			</TD>
			<TD VALIGN="TOP">
					<CFIF getFlags.RecordCount IS 0>
						None
					<CFELSE>
						<CFOUTPUT QUERY="getFlags">#htmleditformat(OrderingIndex)# &nbsp; #htmleditformat(Name)#<BR></CFOUTPUT>
					</CFIF>
			</TD></TR>
		</TABLE>

	</TD></TR>
</TABLE>

<P>
<FORM METHOD="POST" ACTION="flagUpdateTask.cfm" NAME="FlagForm">
<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
<CF_INPUT TYPE="HIDDEN" NAME="flagGroupName" VALUE="#flagGroupName#">
<CF_INPUT TYPE="HIDDEN" NAME="flagGroupHelpText" VALUE="#flagGroupHelpText#">
<CF_INPUT TYPE="HIDDEN" NAME="flagGroupScEn" VALUE="#flagGroupScEn#">
<CFIF frmTask IS "edit"><CF_INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="#frmFlagID#"></CFIF>
<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="">
<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">

<TABLE BORDER=0 WIDTH=380>
<TR>
	<TD COLSPAN=3><B>
		Flag <CFIF frmtask IS "edit"><CFOUTPUT>'#htmleditformat(frmName)#' </CFOUTPUT></CFIF>Details 
		<BR><BR><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/misc/rule.gif" WIDTH=370 HEIGHT=3 ALT="" BORDER="0"><BR><BR></B>
	</TD>
</TR>
<TR>
	<TD ROWSPAN=8 WIDTH=30><FONT FACE="Arial, Chicago, Helvetica" SIZE=3>&nbsp;</TD>
	<TD>
		Name :
	</TD>
	<TD>
		<CF_INPUT TYPE="TEXT" NAME="frmName" VALUE="#frmName#" SIZE="25" MAXLENGTH="250" onBlur="javascript:setTextID()">
	</TD>
</TR>
<TR>
	<TD>
		Help Text:<BR><BR>
	</TD>
	<TD>
		<CF_INPUT TYPE="TEXT" NAME="frmHelpText" VALUE="#frmHelpText#" SIZE="25" MAXLENGTH="60"><BR><BR>
	</TD>
</TR>

<TR>
	<TD>
		FlagTextID :  <BR><BR>
	</TD>
	<TD>
	<CFIF Trim(frmFlagTextID) IS NOT "">
		<CFOUTPUT>#HTMLEditFormat(Trim(frmFlagTextID))#</CFOUTPUT><BR>
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTextID" VALUE="#frmFlagTextID#" >
	<CFELSE>
	<CF_INPUT TYPE="TEXT" NAME="frmFlagTextID" VALUE="#frmFlagTextID#" SIZE="28" MAXLENGTH="30" ><BR>
		(not required, but once set cannot be changed)<BR>
	</CFIF>

	</TD>
</TR>

<TR>
	<TD VALIGN="TOP">
		Ordering :
	</TD>
	<TD>
		<SELECT NAME="frmOrder"><CFLOOP INDEX="LoopCount" FROM="1" TO="99">
			<CFOUTPUT>
			<CFIF LoopCount IS frmOrder>
				<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#
			<CFELSE>
				<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
			</CFIF>
			</CFOUTPUT>
		</CFLOOP></SELECT>
		<BR>Flags with the same Ordering Index are sorted alphabetically, so for items like
		categories, give all flags the same Ordering Index.  Items, like days of the week,
		should be ordered 1 to 7.<BR><BR>
	</TD>
</TR>


<CFIF frmflagTypeID is not 2 and frmflagTypeID is not 3>  <!--- ie not radio and checkbox --->
<CFOUTPUT>
<TR>
	<TD>
		<CFIF frmUseValidValues is 1 and frmFlagTextID is not "">
			<A HREF="javascript:validValues ('flag.#frmFlagTextID#')">Uses	 Valid Values</a>
		<CFELSE>
		Use Valid Values	
		</cfif>			
		 :
	</TD>
	<TD>
		<CF_INPUT TYPE="RADIO" NAME="frmUseValidValues" VALUE="0"  CHECKED="#iif(frmUseValidValues IS 0,true,false)#"> No &nbsp;&nbsp;
		<CF_INPUT TYPE="RADIO" NAME="frmUseValidValues" VALUE="1"  CHECKED="#iif(frmUseValidValues IS NOT 0,true,false)#"> Yes 

	</TD>
</TR>
</CFOUTPUT>


<TR>
	<TD >
		Do Lookup :  
	</TD>
	<TD>
		<CF_INPUT TYPE="RADIO" NAME="frmlookup" VALUE="0"  CHECKED="#iif(frmlookup IS 0,true,false)#"> No &nbsp;&nbsp;
		<CF_INPUT TYPE="RADIO" NAME="frmlookup" VALUE="1"  CHECKED="#iif(frmlookup IS NOT 0,true,false)#"> Yes 
		<BR>Select this if the data value stored in this field is different to the value you wish to display
	</TD>
</TR>

</cfif>


<TR>
	<TD>
		Active :
	</TD>
	<TD>
		<CF_INPUT TYPE="RADIO" NAME="frmActive" VALUE="0"  CHECKED="#iif(frmActive IS 0,true,false)#"> No &nbsp;&nbsp;
		<CF_INPUT TYPE="RADIO" NAME="frmActive" VALUE="1"  CHECKED="#iif(frmActive IS NOT 0,true,false)#"> Yes 
	</TD>
</TR>



<TR>
	<TD valign = "top">
		WDDX :
	</TD>
	<TD>

		<CFIF frmWddXStruct is not "">

			This Flag has a WDDX structure with the following fields<BR>
			You may set valid values by clicking on the link <BR>
			Remember that to use the valid values, you must select "use valid values" in the screen definition<BR>
			Check the box to delete fields (be careful incase they are required in any existing screens)<BR>
			Add new fields adding the names in the boxes below <BR>
			<CFWDDX ACTION="WDDX2CFML" INPUT="#frmWddXStruct#" OUTPUT="thisStructure">
	
				<CFLOOP item="thisItem" collection="#thisStructure#">
	
				<CFOUTPUT><CF_INPUT type="checkbox" name ="frmDelWDDX" value="#thisItem#"> <A HREF="javascript:validValues('Flag.#frmFlagTextID#.#thisItem#')">#htmleditformat(thisItem)#</a></cfoutput> <BR>
	
			</cfloop>

		<CFELSE>
			If this is a text flag, you may define a WDDX structure to hold multiple fields of information in.<BR>
			Enter the name of a field to add below. <BR>
			

			

		</cfif>
			<INPUT type="text" name="frmWDDX" value="">

	</TD>
</TR>







</TABLE>


</FORM>

<CFOUTPUT>

<P><A HREF="JavaScript:verifyForm('#frmTask#','#frmBack#');">Save and Return</A>
<A HREF="JavaScript:verifyForm('#frmTask#','flagEditAgain');">Save and return to this record</A>
<CFIF #frmTask# IS "add">
 &nbsp; &nbsp;
<A HREF="JavaScript:verifyForm('#frmTask#','flagEdit');">Save and Add New</A>

</CFIF>
</CFOUTPUT>

<FORM METHOD="POST" ACTION="<CFOUTPUT>#htmleditformat(frmBack)#</CFOUTPUT>.cfm" NAME="BackForm">
<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
</FORM>
										
<P>


</CENTER>
</FORM>


