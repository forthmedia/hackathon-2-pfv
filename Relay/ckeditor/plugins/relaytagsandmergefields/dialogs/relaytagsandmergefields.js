﻿/*
 * Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

(function()
{
	function relayTagDialog( editor, isEdit, relayTagType )
	{

		var generalLabel = editor.lang.common.generalTab;
		return {
			title : 'Relay Field',
			minWidth : 300,
			minHeight : 80,
			contents :
			[
				{
					id : 'info',
					label : generalLabel,
					title : generalLabel,
					elements :
					[
						{
							id : 'text',
							type : 'text',
							style : 'width: 100%;',
							label : 'Relay Tag Details',
							'default' : '',
							required : true,
							validate : CKEDITOR.dialog.validate.notEmpty( 'lang.textMissing' ),
							setup : function( element )
							{
								if ( isEdit )
									this.setValue( element.getText().slice( 2, -2 ) );
							},
							commit : function( element )
							{

								
								var text = CKE_RelayTags.Types[relayTagType].opening + this.getValue() + CKE_RelayTags.Types[relayTagType].closing;
								// The placeholder must be recreated.
								CKEDITOR.plugins.relaytagsandmergefields.createRelayTagSpan( editor, element, text,false,relayTagType );
							},
							onClick : function () {alert('onclick')}
							
						}
						,
						{
							id : 'somehtml',
							type : 'html',
							style : 'width: 100%;',
							html : '<A href="javascript:wab()">zzzz</A> asdasf zsfasfasfsd' , 
							setup : function( element )
							{
/*								alert (this)	
								y = ''
								for (x in this) {
									y = y + ' ' + x
								}
							 	alert ('These are the methods in \'this\' \n ' + y)

								ajax = getRelayTagEditor(relayTagType,element.getText())

								//alert (ajax)
								alert ('This.html: \n' + this.html)

								this.html =  '123123123124124' ;
alert ('this.html set to something else: '  + this.html)

								// alert ('this.getValue: \n' + this.getValue())
								 dialog = this.getDialog()
								 alert ('this.getdialog: \n' + dialog)
								 alert ('dialog: \n' + dialog.innerHTML)

								y = ''
								for (x in dialog) {
									y = y + ' ' + x
								}
							 	alert ('These are the methods in \'dialog\' \n ' + y)

								 

								if ( isEdit ) {
									alert ('setting value1')

									this.html =  '123123123124124' ;
									this.setValue('1213123123')
									this.innerHTML =  '123123123124124' ;
									alert ('done')
								}	
*/

							},


						}	
// 
					]
				}
			],
			onShow : function()
			{
				
				if ( isEdit ) {
					this._element = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag( editor );
				}

				this.setupContent( this._element );
			},
			onOk : function()
			{
				this.commitContent( this._element );
				delete this._element;
			}
		};
	}

	getRelayTagEditor = function (relayTagType,tagText) {
		var url = '/test/webservices/wabtest.cfc?wsdl&method=getrelaytag&returnFormat=plain&_cf_nodebug=true'
		url = url + '&tagType=' + relayTagType + '&tagText=' + tagText
		result = CKEDITOR.ajax.load(url)
	
		return result
			
	
	}
	
	
	wab = function () {
	alert ('wab')
	alert (this)
	}

	CKEDITOR.dialog.add( 'relayTagInsertDialog', function( editor )
		{
			return relayTagDialog( editor,0, 'RelayTag' );
		});

	CKEDITOR.dialog.add( 'relayTagEditDialog', function( editor )
		{
			return relayTagDialog( editor,1,'RelayTag' );
		});

	CKEDITOR.dialog.add( 'relayMergeInsertDialog', function( editor )
		{
			return relayTagDialog( editor,0,'RelayMerge');
		});

	CKEDITOR.dialog.add( 'relayMergeEditDialog', function( editor )
		{
			return relayTagDialog( editor, 1 ,'RelayMerge');
		});

} )();
