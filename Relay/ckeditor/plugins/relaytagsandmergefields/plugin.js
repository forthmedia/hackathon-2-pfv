/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/**
 *
WAB 2011/10/13 added encodeURIComponent() when opening tag editor
WAB 2011/11/18 attempt to prevent infinite escaping of ampersands
NJH 2011/12/05 added delete functionality
WAB 2013/03/22 Fixed a bit of JS which didn't work with latest version of prototype [I was using (for each x in array)  ]
WAB/NJH 2013-05-22 CASE 435044  Problems with IE<=8, when double clicking on a tag


KNOWN issue:
The right click functionality does not work on a relay tag in Chrome due to the fact that the span is not editable. As a result, the focus is lost when it is clicked and we don't have a selection. This appears to be a known issue.
http://ckeditor.com/forums/CKEditor/CKE4.0.1-howto-get-a-valid-selection-on-a-contenteditablefalse-span-in-chrome-ok-in
 */
(function()
{
    var relayTagInsertDialog = {  
            exec:function(editor){  
            	CKEDITOR.plugins.relaytagsandmergefields.relayTagEditorDialog('insert');
            }
    }
    
    var relayTagEditDialog = {  
            exec:function(editor){  
            	CKEDITOR.plugins.relaytagsandmergefields.relayTagEditorDialog('edit');
            }
    }
      
	var relayTagDelete = {  
            exec:function(editor){  

				if (!is_chrome) {
					relaytagspan = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag(editor);
					//in chrome, this relayTagSpan is not defined as we've lost focus due to the span not being editable
					if (relaytagspan) {
						relaytagspan.remove();
					}
				} else {
					alert(isChromeDeleteMessage);
				}
           }  
       }
       
       
 	var relayMergeFunctionDelete = {  
            exec:function(editor){  

				if (!is_chrome) {
					relaytagspan = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag(editor);
					//in chrome, this relayTagSpan is not defined as we've lost focus due to the span not being editable
					if (relaytagspan) {
						relaytagspan.remove();
					}
				} else {
					alert(isChromeDeleteMessage);
				}
           }  
       }
    
    var relayMergeFunctionInsertDialog = {  
            exec:function(editor){  
            	CKEDITOR.plugins.relaytagsandmergefields.relayMergeFunctionEditorDialog('insert');
            }
    }
    
     var relayMergeFunctionEditDialog = {  
            exec:function(editor){  
            	CKEDITOR.plugins.relaytagsandmergefields.relayMergeFunctionEditorDialog('edit');
            }
    }

	CKEDITOR.plugins.add( 'relaytagsandmergefields',
	{


		init : function( editor )
		{


			var command = editor.addCommand( 'relayTagInsertCommand', relayTagInsertDialog);   //new CKEDITOR.dialogCommand( 'relayTagInsertDialog' ) 
			command.modes = { wysiwyg:1, source:1 };   // this allows the button to show in source view - not sure how to get it to insert though! actual
			editor.addCommand( 'relayTagEditCommand', relayTagEditDialog); // new CKEDITOR.dialogCommand( 'relayTagEditDialog' )
			editor.addCommand( 'relayTagDeleteCommand', relayTagDelete);
			editor.addCommand( 'relayMergeInsertCommand', relayMergeFunctionInsertDialog); //new CKEDITOR.dialogCommand( 'relayMergeInsertDialog' ) );
			editor.addCommand( 'relayMergeEditCommand', relayMergeFunctionEditDialog); //new CKEDITOR.dialogCommand( 'relayMergeEditDialog' ) );
			editor.addCommand( 'relayMergeDeleteCommand', relayMergeFunctionDelete);

		

			editor.ui.addButton( 'InsertRelayTag',
			{
				label : 'Insert Relay Tag',
				command :'relayTagInsertCommand',
				icon : this.path + 'RelayTag.gif'
			});

			editor.ui.addButton( 'InsertRelayMerge',
			{
				label : 'Insert Relay Merge',
				command :'relayMergeInsertCommand',
				icon : this.path + 'RelayMerge.gif'
			});

			if ( editor.addMenuItems )
			{
				editor.addMenuGroup( 'relaytagmenugroup', 20 );
				editor.addMenuItems(
					{
						relayTagEditMenu :
						{
							label : 'Edit Relay Tag',
							command : 'relayTagEditCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'relaytag.gif'
						},
						
						relayTagDeleteMenu :
						{
							label : 'Delete Relay Tag',
							command : 'relayTagDeleteCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'relaytag.gif'
						},

						relayTagInsertMenu :
						{
							label : 'Insert Relay Tag',
							command : 'relayTagInsertCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'relaytag.gif'
						},

						relayMergeEditMenu :
						{
							label : 'Edit Merge Field',
							command : 'relayMergeEditCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'RelayMerge.gif'
						},
						
						relayMergeDeleteMenu :
						{
							label : 'Delete Merge Field',
							command : 'relayMergeDeleteCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'RelayMerge.gif'
						},

						relayMergeInsertMenu :
						{
							label : 'Insert Merge Field',
							command : 'relayMergeInsertCommand',
							group : 'relaytagmenugroup',
							order : 1,
							icon : this.path + 'RelayMerge.gif'
						}
						
					} );



				if ( editor.contextMenu )
				{
					editor.contextMenu.addListener( function( element, selection )
						{
							var result = {}
							// NJH 2016/01/27 - if we're dealing with a relayLink, then we don't want to treat this as a merge field/function.
							/*if (element && element.getParent().getName() == 'a') {
								return result;
							}*/

							// if no relay stuff then can show the insert items but turn off the edit
							if ( !element || !element.data( 'ckrelaytag' ) ) {
								if (isCommandInToolbar(editor,'InsertRelayTag')) result.relayTagInsertMenu = CKEDITOR.TRISTATE_ON
								if (isCommandInToolbar(editor,'InsertRelayMerge')) result.relayMergeInsertMenu = CKEDITOR.TRISTATE_ON
							}
								
							if (element.data( 'ckrelaytag' ) == 'RelayTag') {
								result.relayTagEditMenu = CKEDITOR.TRISTATE_ON ;
								result.relayTagDeleteMenu = CKEDITOR.TRISTATE_ON ;
							}	
							else if (element.data( 'ckrelaytag' ) == 'RelayMergeFunction' || element.data( 'ckrelaytag' ) == 'RelayMerge')  {	
								result.relayMergeEditMenu = CKEDITOR.TRISTATE_ON ;
								result.relayMergeDeleteMenu = CKEDITOR.TRISTATE_ON ;
							}

							return result
						} );
				}

			}

			editor.on( 'doubleclick', function( evt )
				{					
					if (getRelaySpan = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag( editor ) ) {
						if (getRelaySpan.data( 'ckrelaytag' ) == 'RelayMergeFunction') {
							relayMergeFunctionEditDialog.exec(editor);
						} else if (getRelaySpan.data( 'ckrelaytag' ) == 'RelayTag'){
						 // evt.data.dialog = 'relayTagEditDialog';  
						    relayTagEditDialog.exec(editor)
						}
					}	
				});

			// Define the class for highlighting the mergefield
			CKEDITOR.addCss(
				'.class_ckrelaytag' +
				'{' +
					'background-color: #ff6666;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
			);

			CKEDITOR.addCss(
				'.class_ckrelaytagend' +
				'{' +
					'background-color: #ff6600;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
			);


			CKEDITOR.addCss(
				'.class_ckrelaymerge' +
				'{' +
					'background-color: #ffAAAA;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
			);

			CKEDITOR.addCss(
				'.class_ckrelaymergefunction' +
				'{' +
					'background-color: #ddAAAA;' +
					( CKEDITOR.env.gecko ? 'cursor: default;' : '' ) +
				'}'
			);

			editor.on( 'contentDom', function()
				{
					editor.editable().on( 'resizestart', function( evt )
						{
							if ( editor.getSelection().getSelectedElement().data( 'ckrelaytag' ) )
								evt.data.preventDefault();
						});
				});
			

			CKEDITOR.dialog.add( 'relayTagInsertDialog', this.path + 'dialogs/relaytagsandmergefields.js' );
			CKEDITOR.dialog.add( 'relayTagEditDialog', this.path + 'dialogs/relaytagsandmergefields.js' );
			CKEDITOR.dialog.add( 'relayMergeInsertDialog', this.path + 'dialogs/relaytagsandmergefields.js' );
			CKEDITOR.dialog.add( 'relayMergeEditDialog', this.path + 'dialogs/relaytagsandmergefields.js' );

		},

		afterInit : function( editor )
		{
			var dataProcessor = editor.dataProcessor,
				dataFilter = dataProcessor && dataProcessor.dataFilter,
				htmlFilter = dataProcessor && dataProcessor.htmlFilter;

			/*
			This is where the content is filtered and can be doctored adding custom content
			For example adding coloured spans around relay tags and merge fields
			*/
			if ( dataFilter )
			{
				dataFilter.addRules(
				{
				
					text : function( text )
					{
						var result = text
						for (var tagType in CKE_RelayTags.Types ) {

							result = result.replace( CKE_RelayTags.Types[tagType].findRegExp, function( match )
								{
									return  CKEDITOR.plugins.relaytagsandmergefields.createRelayTagSpan( editor, null, match, 1,tagType );
								});
						}

						return result


					}
				});
			}


			if ( htmlFilter )
			{
				/*
				This section removes our custom content before code is displayed in source view
				*/
				htmlFilter.addRules(
				{
					elements :
					{
						span : function( element )
						{
							if ( element.attributes && element.attributes[ 'data-ckrelaytag' ] ) {
								// WAB tried to get rid of &amp; here.
								// The technique is valid, but the & is always changed back to &amp; 
								// element.children[0].value = element.children[0].value.replace('&amp;','&')
								delete element.name;
							}
							
						}
					}
				},
				{applyToAll : true});
			}
		}

	});

})();

	var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
	var isChromeDeleteMessage = 'Due to a known issue in third party software, the Delete Relay Tag functionality does not work in Chrome. Please use an alternative method or browser to delete the tag.';
	var isChromeEditMessage = 'Due to a known issue in third party software, the Edit Relay Tag functionality does not work in Chrome. Please use an alternative browser or double click the tag to edit.';
	
	// The object used for all RelayTag operations.
	var CKE_RelayTags = new Object() ;
	
	CKE_RelayTags.Types = new Object()
	TagType = CKE_RelayTags.Types['RelayTag'] = new Object()
		TagType.opening = '{{'
		TagType.closing = '}}'
		TagType.spanclass = 'class_ckrelaytag'
		TagType.spaneditable = 'false'
		TagType.findRegExp = /(\{\{relay_[^\}]+\}\})/i
	
	TagType = CKE_RelayTags.Types['RelayTagEnd'] = new Object()
		TagType.opening = '{{'
		TagType.closing = '}}'
		TagType.spanclass = 'class_ckrelaytagend'
		TagType.spaneditable = 'false'
		TagType.findRegExp = /(\{\{\/relay_[^\}]+\}\})/i

	TagType = CKE_RelayTags.Types['RelayMerge'] = new Object()
		TagType.opening = '[['
		TagType.closing = ']]'
		TagType.spanclass = 'class_ckrelaymerge'
		TagType.spaneditable = 'false'
		TagType.findRegExp  = /(\[\[[^\]]+[^)]\]\])/i   // this finds [[xxxxxx]]  with no )]], will find [[myfunction().name]] 

	TagType = CKE_RelayTags.Types['RelayMergeFunction'] = new Object()
		TagType.opening = '[['
		TagType.closing = ']]'
		TagType.spanclass = 'class_ckrelaymergefunction'
		TagType.spaneditable = 'false'
		TagType.findRegExp  = /(\[\[[^\]]+\)\]\])/i  // this finds [[xxxxxx(yyyyyyyy]]  ie with a )]] 



CKEDITOR.plugins.relaytagsandmergefields =
{

	relayTagEditorDialog : function (action) {

		var relaytagspan = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag(editor)
		var scrollbarAttr = 'no';
		if (relaytagspan == null && is_chrome && action=='edit') {
			alert(isChromeEditMessage);
		} else {
			if (relaytagspan == null) {
				relayTagText = ''
				relayTagType = 'RelayTag'
				path = '/relaytageditors/relaytagpicker.cfm?'
			 
			 } else {
				relayTagText = encodeURIComponent(relaytagspan.getHtml())
				relayTagType = relaytagspan.getAttribute('data-ckrelaytag')
				path = '/relaytageditors/relaytageditor.cfm?'
			 	scrollbarAttr = 'yes';
			 }

//				path = '/ckeditor/_source/plugins/relaytagsandmergefields/dialogs/ck_RelayTagInsert.cfm?'
			var returnFunction = escape("opener.CKEDITOR.plugins.relaytagsandmergefields.insertRelayTag('" + editor.name + "',*TAGTEXTARRAY*,'"+relayTagType+"')")

			/*
			WAB/NJH 2013-05-22 CASE 435044  Problems with IE<=8, when double clicking on a tag
			See comments in relayMergeFunctionEditorDialog()
			*/

			var windowOpenFunction = 
				              function () {
                					window.open(path + "relayTag=" + relayTagText + '&returnFunction=' + returnFunction,'RelayTags','width=600,height=600, resizable=1,scrollbars='+scrollbarAttr);  
				                }

			if (CKEDITOR.env.ie &&  CKEDITOR.env.version <= 8)  {
	            window.setTimeout (windowOpenFunction,800)
			} else {
				windowOpenFunction()
			}
		}
    },
       
	relayMergeFunctionEditorDialog : function(action) {  

		relaytagspan = CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag(editor)
		if (relaytagspan == null && is_chrome && action=='edit') {
			alert(isChromeEditMessage);
		} else {
			if (relaytagspan == null) {
				relayTagText = ''
				relayTagType = 'RelayMergeFunction'
				path = '/relaytageditors/relaytagpicker.cfm?relayInsertType=mergeFields&context='+editor.config.relayContext+'&';
			 
			 } else {
				relayTagText = encodeURIComponent(relaytagspan.getHtml())
				relayTagType = relaytagspan.getAttribute('data-ckrelaytag')
				path = '/relaytageditors/relayFunctionEditor.cfm?relayInsertType=mergeFields&context='+editor.config.relayContext+'&';
			 
			 }

//				path = '/ckeditor/_source/plugins/relaytagsandmergefields/dialogs/ck_RelayTagInsert.cfm?relayInsertType=mergeFields&'
			returnFunction = escape("opener.CKEDITOR.plugins.relaytagsandmergefields.insertRelayTag('" + editor.name + "',*TAGTEXTARRAY*,'"+relayTagType+"')")
	
			// Needed as window is opened up as a modal dialog
			//var dialogArgs = new Object();
			//dialogArgs.CKEDITOR = CKEDITOR;
	        //var x = window.showModalDialog(path + "relayFunction=" + relayTagText + '&returnFunction=' + returnFunction ,dialogArgs,"dialogWidth:750px;dialogHeight:500px;center:yes; resizable: yes; help: no");
			/*
			WAB/NJH 2013-05-22 CASE 435044  Problems with IE<=8, when double clicking on a tag
			If user has cursor away from a tag and then double clicks on a tag, the editor opens, but the cursor does not move to the tag
			This means that when the update happens later it takes place at the wrong location (and does an insert rather than an update)
			Discovered that opening the window after a timeout fixed the problem (rather a hack I know)
			*/
			var windowOpenFunction = 
				              function () {
	            					void(window.open(path + 'relayFunction=' + relayTagText + '&returnFunction=' + returnFunction,'RelayFunctions','width=600,height=600'));
				                }
	
			if (CKEDITOR.env.ie &&  CKEDITOR.env.version <= 8)  {
	            window.setTimeout (windowOpenFunction,800)
			} else {
				windowOpenFunction()
			}
		}
    },
	
	// this is the code which puts a span around a relay tag
	// it is called both from this file and also by the dialog
	createRelayTagSpan : function( editor, oldElement, text, isGet, relayTagType )
	{
	
		if (typeof(relayTagType) == 'undefined') {
			relayTagType = getRelayTagType(text)
			//alert ('Text: \'' + text + '\'  Type: ' + relayTagType)
		}
			var element = new CKEDITOR.dom.element( 'span', editor.document );

			element.setAttributes(
				{
					'data-cke-editable':'true',
					contentEditable		: CKE_RelayTags.Types[relayTagType].spaneditable,
					'data-ckrelaytag'	: relayTagType,
					'class'			: CKE_RelayTags.Types[relayTagType].spanclass
				}
			);

			text = text.replace('&amp;','&') // WAB 2011/11/17 to try and prevent infinite escaping
			text && element.setText( text );

			if ( isGet )
				return element.getOuterHtml();

			if ( oldElement )
			{
				if ( CKEDITOR.env.ie )
				{
					element.insertAfter( oldElement );
					// Some time is required for IE before the element is removed.
					setTimeout( function()
						{
							oldElement.remove();
							element.focus();
						}, 10 );
				}
				else
					element.replace( oldElement );
			}
			else
				editor.insertElement( element );

		return null;
	},

	// this returns the node (a span) around the current relayTag
	getSelectedRelayTag : 	function( editor ) {
								var range = editor.getSelection().getRanges()[ 0 ];
								range.shrink( CKEDITOR.SHRINK_TEXT );
								var node = range.startContainer;
								while( node && !( node.type == CKEDITOR.NODE_ELEMENT && node.data( 'ckrelaytag' ) ) ) {
									node = node.getParent();
								}

								// 2014/03/03	YMA	Case 438988 If the node wasn't found then the range probably didn't include the span of the relaytag wrapper.
								var selectionStartElement = editor.getSelection().getStartElement();
									if(!node && selectionStartElement.getParent().data( 'ckrelaytag' )){
										node = selectionStartElement.getParent();
										while(node.getParent().data( 'ckrelaytag' )){
											node = node.getParent();
										}
									} else if(selectionStartElement.data( 'ckrelaytag' )) {
										node = selectionStartElement;
									}
								
									// NJH 2016/01/27
									if (node && node.getParent().getName() != 'a') {
										return node;
									} else {
										return null;
									}
							}, 
	
	CKE_RelayTags : CKE_RelayTags,
	
	insertRelayTag : function (editorInstanceName,tagTextArray,tagType,requiresEndTag) {

						editor = CKEDITOR.instances[editorInstanceName]
						element = 		CKEDITOR.plugins.relaytagsandmergefields.getSelectedRelayTag(editor)
						tagTextArray.each (
									function (tagText,index) {
										if (index > 0)	{
											//ie there is more than one tag being added at a time so put a space between them (only occurs occasionally)
											editor.insertText(' ' )
										}
										
										CKEDITOR.plugins.relaytagsandmergefields.createRelayTagSpan( editor, element, tagText,false,undefined )  // pass in undefined and the span function will use regExp to work out what sort of span is required
																
										
									});
				
						return true
					}
	
};

isCommandInToolbar  = function (editor,command) {

	var toolbarname = 'toolbar_' + editor.config.toolbar
	var result = false
	var toolbarArray = editor.config[toolbarname]
	for (var i in toolbarArray) {
		for (var j in toolbarArray[i]) {
			if (toolbarArray[i][j] == command) {
				result = true
				break
			}
		}	
	}	
	return result
}

	getRelayTagType = function (text) {
		//alert ('Testing:|'+text+'|')
		var result = ''
		for (var tagType in CKE_RelayTags.Types ) {

			if (CKE_RelayTags.Types[tagType].findRegExp.test(text)) {
				result = tagType;
				break
			}
		}	
		return result
	}