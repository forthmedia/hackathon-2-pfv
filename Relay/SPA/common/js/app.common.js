/**
 * @ngdoc overview
 * @name common
 * @description
 * This module includes the following modules for use in all applications
 *  *  ngAnimate
 *  *  ngSanitize
 *  *  ngMessages
 *  *  ui.grid
 *  *  ui.bootstrap
 *  *  ui.router
 *  *  checklist-model    
 *  *  isteven-multi-select

 */
(function() {
'use strict';

angular.module('common' , [

    // 'ngAnimate',
    // 'ngSanitize',
    // 'ngFileUpload',
    // 'naif.base64',
    // 'ngMessages',
    // 'ui.grid',
    'ui.bootstrap',
    // 'ui.grid.selection',
    // 'ui.grid.exporter',
    // 'ui.grid.autoResize', 
    // 'ui.grid.pagination',
    'ui.router',
    // 'checklist-model',    
    // 'isteven-multi-select',
    // 'MessageCenterModule',
    // 'ngTable',
    // 'ckeditor'

    ])
})();

(function() {
  'use strict';

	angular
		.module('common')
		.directive('fmFileModel' , fileModelDirective)

	fileModelDirective.$inject = [ '$parse' ]

        function fileModelDirective( $parse ) {

        var directive =  {
                require: '?ngModel',
                restrict: 'A',
                link: function(scope, element, attrs , ngModel) {
                    var model = $parse(attrs.fmFileModel);
                    var modelSetter = model.assign;

                    element.bind('change', function(){
                        ngModel.$setViewValue(element[0].files[0].name);
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };

	    return directive;

	  }

})();

(function() {
'use strict';

angular.module('common')
.directive('gridLoading', function () {
    return {
        restrict: 'C',
        require: '^uiGrid',
        link: function ($scope, $elm, $attrs, uiGridCtrl) {
            $scope.grid = uiGridCtrl.grid;
        }
    }
});

})();

(function() {
/**
		custom validator for form inputs
		checks if value is contained in a simple array
		
*/

angular.module('common')
.directive('rwArrayContains', dir )

function dir( ) {
  var directive = {
    require: '?ngModel',
    scope : {
    	rwArrayData : '='
    },
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.rwArrayContains = function(modelValue, viewValue) {
        return _.indexOf( scope.rwArrayData , modelValue ) === -1 
      };
    }
  };
  return directive;	
};


})();

/**
 * @ngdoc filter
 * @name common.filter:xlat
 * @description
 * Filter that translates a phrase
 * @requires common.service:xlatService
 * @example
 * &lt;p&gt;{{ 'phr.phrase' | xlat }}&lt;/p&gt;
 */
(function() {
'use strict';

angular.module('common')
	.filter( 'xlat' , xlatFilter )

	xlatFilter.$inject = [ "common.xlatService"];

	function xlatFilter( xlatService ) {
		return function(label, parameters) {
			label = label.replace('phr.' , '');	
			return xlatService.xlat(label);
		};
	}
})();
/**
 * @ngdoc service
 * @name common.service:languageService
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.languageService' , languageService );

    languageService.$inject = [ '$http' ];

    function languageService( $http ) {
        var service = {
            getLanguages: getLanguages
        };
        return service;

        /**
         * @ngdoc method
         * @name getLanguages
         * @methodOf common.service:languageService
         * @returns {promise} http request
         * @description
         * Makes http __GET__ to /webservices/callwebservice.cfc
         *  * method=callwebservice
         *  * webservicename=api_as_webservice
         *  * returnFormat=JSON
         *  * methodname=getLanguages
         */
        function getLanguages() {
            return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnFormat=JSON&methodname=getLanguages')
                .then(getLanguagesComplete)
                .catch(getLanguagesFailed);

                function getLanguagesComplete(response) {
                    return response.data;
                }

                function getLanguagesFailed(error) {
                    return error.data;
                }
            }

        }
})();
/**
 * @ngdoc service
 * @name common.service:pageService
 * @description
 * This services provides methods which are useful for managing a page object
 * To use this service in controllers inject 'common.pageService'
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.pageService' , pageService );

    pageService.$inject = [ ];

    function pageService(  ) {

        var page = {
            "pageTitle" : "Default Page Title"
        };

        var service = {
            getPage: getPage,
            getPageTitle: getPageTitle,
            setPageTitle: setPageTitle
        };

        return service;

        /**
         * @ngdoc method
         * @name getPage
         * @methodOf common.service:pageService
         * @description
         * Returns a page object containing the following keys
         *  { 
         *      pageTitle : 
         *  }
         * @returns {object} page object
         */
        function getPage() {
            return page;
        }

        /**
         * @ngdoc method
         * @name setPageTitle
         * @methodOf common.service:pageService
         * @description
         * sets title on page object
         * 
         * @param {string} title the page title
         */
        function setPageTitle( title ) {
            page.pageTitle = title;
        }
        /**
         * @ngdoc method
         * @name getPageTitle
         * @methodOf common.service:pageService
         * @description
         * gets title from page objectcommon.service:
         *
         */
        function getPageTitle( ) {
            return page.pageTitle;
        }
    }

})();

/**
 * @ngdoc service
 * @name common.service:phraseService
 */
(function() {
'use strict';

angular.module('common')
    .factory('common.phraseService' , phraseService );

    phraseService.$inject = ['$http' ];

    function phraseService( $http ) {

        var phraseStruct = "";

        var service = {
            getAllPhrases: getAllPhrases,
            getAllEntityPhrases: getAllEntityPhrases,
            getEntityTypePhrases: getEntityTypePhrases,
            getPhraseEntityTypes: getPhraseEntityTypes,
            getPhraseStructure: getPhraseStructure
        };
        return service;

    ////////////

    function getPhraseStructure() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getPhraseStruct')
            .then( function( response ) { phraseStruct = response} )
            .catch( function ( error) { console.log('XHR Failed for getPhraseStructure.' +  error.data);});
    }

    function getAllPhrases() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getAllPhrases')
            .then(getPhrasesComplete)
            .catch(getPhrasesFailed);

            function getPhrasesComplete(response) {
                return response.data;
            }

            function getPhrasesFailed(error) {
                return( error.data );
            }
        }

    function getAllEntityPhrases( entityTypeID) {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getAllEntityPhrases&entityTypeID=' + entityTypeID )
            .then(getPhrasesComplete)
            .catch(getPhrasesFailed);

            function getPhrasesComplete(response) {
                return response.data;
            }

            function getPhrasesFailed(error) {
                return( error.data );
            }
        }

    function getEntityTypePhrases( entityTypeID ) {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getEntityTypePhrases&entityTypeID=' + entityTypeID )
            .then(getEntityTypePhrasesComplete)
            .catch(getEntityTypePhrasesFailed);

            function getEntityTypePhrasesComplete(response) {
                return response.data;
            }

            function getEntityTypePhrasesFailed(error) {
                return( error.data );
            }
        }

    function getPhraseEntityTypes() {
        return $http.get('/webservices/callwebservice.cfc?method=callwebservice&webservicename=api_as_webservice&returnformat=json&methodname=getPhraseEntityTypes')
            .then(getEntityTypesComplete)
            .catch(getEntityTypesFailed);

            function getEntityTypesComplete(response) {
                return response.data;
            }

            function getEntityTypesFailed(error) {
                return( error.data );
            }
        }
    }

})();

(function() {
'use strict';

angular.module('common')
.factory('moment' , momentService )
.factory('_' , lodashService )
.factory('common.utils' , utils )

    momentService.$inject = [ '$window' ];
    lodashService.$inject = [ '$window' ];

    function momentService( $window ) {
    return $window.moment;
    }

    function lodashService( $window ) {
    return $window._;
    }

    function utils(  ) {

        var service = {
            createUUID: createUUID
        };
        return service;

        function createUUID() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
        }
    }

})();

/**
 * @ngdoc service
 * @name common.service:xlatService
 * @requires $window
 * @requires common.service:pageService
 * @description
 * A description of the controller, service or filter
 */
(function () {
    'use strict';

    angular
        .module('common')
        .factory('common.xlatService', xlatService)

    xlatService.$inject = ['$window']

    function xlatService($window) {

        var service = {};
        service.xlat = xlat;

        return service;

        /**
         * @ngdoc method
         * @name xlat
         * @methodOf common.service:xlatService
         * @description
         * Translates a phrase
         *
         * @param {string} label phrase to translate
         * @returns {string} phrase translated phrase
         */
        function xlat(label) {

            if ($window.phr.hasOwnProperty(label)) {
                return $window.phr[label];
            } else {
                return label;
            }
        }
    };

})();