<!--- Application CSS sty;lesheet --->
<link rel="styleSheet" href="assets/css/app.test.css"/>

<div ng-app="test">
    <div ui-view>
</div>

<!--- Common JS code --->

<cf_includejavascriptonce template="/SPA/common/js/vendor.js">
<cf_includejavascriptonce template="/SPA/common/js/app.common.js">

<!--- Application JS code --->

<cf_includejavascriptonce template="/SPA/test/assets/js/app.test.js">

<!--- Application Template Cache --->

<cf_includejavascriptonce template="/SPA/test/assets/js/app.test.templates.js" translate=true>