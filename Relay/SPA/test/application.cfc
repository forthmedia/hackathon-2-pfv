component extends="applicationMain" {

    function onRequestEnd() {

        // overloading onRequestEnd to get rid of default header
        request.document.H1.show = false;
        super.onRequestEnd();

    }

}