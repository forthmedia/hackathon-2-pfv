<!---
 * CKFinder
 * ========
 * http://ckfinder.com
 * Copyright (C) 2007-2011, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying or distribute this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
--->

<cfcomponent displayname="Application" output="false" hint="Pre-page processing for the application" extends="applicationMain">

	Run the pseudo constructor to set up default
	data structures.
	<cfscript>
	// Set up the application.
	/* WAB
	THIS.Name = "CKFinder_Connector";
	THIS.ApplicationTimeout = "#CreateTimeSpan( 0, 2, 0, 0 )#";
	THIS.SessionTimeout = "#CreateTimeSpan(0,0,45,0)#";
	THIS.SessionManagement = true;
	THIS.SetClientCookies = true;
	*/
	THIS.mappings["/CKFinder_Connector"] = getDirectoryFromPath(getCurrentTemplatePath());
	</cfscript>


	<!--- Include the CFC creation proxy. --->
	<cfinclude template="createcfc.udf.cfm" />


	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Pre-page processing for the page request.">
		<!--- WAB --->
		<cfargument name="thePage" >

		<!--- WAB 2012-05-02 2012 change, need to make sure HTML tags are not added by relayware --->
		<cfset super.onRequestStart(thePage=arguments.thePage)>
		<cfset request.document.addHTMLDocumentTags = false>

		<!---
		Store the CreateCFC method in the application
		scope.
		--->
		<cfset APPLICATION.CreateCFC = THIS.CreateCFC />
		<cfset APPLICATION.CFVersion = Left(SERVER.COLDFUSION.PRODUCTVERSION,Find(",",SERVER.COLDFUSION.PRODUCTVERSION)-1) />
		<cfreturn true />
	</cffunction>

</cfcomponent>
