<cfcomponent extends="taffy.core.baseRepresentation" output="false" hint="Representation class that uses CFML server's json and xml serialization functionality to return json/xml data">

	<cfset variables.anythingToXml = application.anythingToXml />
	
	<cffunction
		name="getAsJson"
		output="false"
		taffy:mime="application/json"
		taffy:default="true"
		hint="serializes data as JSON">
			<cfreturn serializeJSON(variables.data) />
	</cffunction>
	
	<cffunction
		name="getAsXML"
		output="false"
		taffy:mime="application/xml">
			<cfreturn variables.anythingToXml.toXml(variables.data) />
	</cffunction>

</cfcomponent>