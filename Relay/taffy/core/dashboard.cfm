<!---
2015/02/26	NJH	Jira Fifteen-195 - changed value of js base url variable

--->

<cfhtmlhead text='<link href="/taffy/core/dashboard.css" rel="stylesheet" type="text/css" />'>

<h1>Relayware REST API</h1>

<p id="toolbar" style="float:left">
	<button type="button" id="showDash" class="active">API Configuration</button>
	<button type="button" id="showDocs">API Documentation</button>
	<!--- <button type="button" id="reloader">Reload API Cache</button> --->
	<!--- <button type="button" id="showMock">Mock Client</button> --->
</p>

<div id="dash">
	<h2>Resources:</h2>
	<table class="withBorder" align="left" cellspacing="1" cellpadding="3" border="0" style="margin: 0 auto">
		<tr>
			<th>Resource</th>
			<th>URI</th>
			<th>Methods</th>
		</tr>
		<cfoutput>
			<cfset variables.resources = listSort(structKeyList(application._taffy.endpoints), "textnocase") />
			<cfset rowNum=0>
			<cfloop list="#variables.resources#" index="resource">
				<cfset currentResource = application._taffy.endpoints[resource] />
				<cfset rowNum++>
				<tr class="#rowNum mod 2?'evenrow':'oddrow'#">
					<td>#currentResource.beanName#</td>
					<td>#currentResource.srcURI#</td>
					<td>#structKeyList(currentResource.methods,"|")#</td>
				</tr>
			</cfloop>
		</cfoutput>
	</table>
	<h2>Implemented Encodings:</h2>
	<ul>
		<cfloop list="#structKeyList(application._taffy.settings.mimeExtensions)#" index="mime">
			<cfoutput><li>#mime# <cfif mime eq application._taffy.settings.defaultMime><em>(default)</em></cfif></li></cfoutput>
		</cfloop>
	</ul>
</div>

<div id="mock">
	<!--- <cfinclude template="mocker.cfm"/> --->
</div>
<div id="docs">
	<cfinclude template="docs.cfm" />
</div>

<script type="text/javascript">
	jQuery(function(){
		var baseurl = '<cfoutput>/api#cgi.path_info#?docs</cfoutput>';
		jQuery("#reloader").click(function(){
			document.location.href = baseurl + '<cfoutput>&#application._taffy.settings.reloadKey#=#application._taffy.settings.reloadPassword#</cfoutput>';
		});
		jQuery("#exportPDF").click(function(){
			document.location.href = baseurl + '&exportPDF';
		});
		//save some selector refs for frequent use
		var m = jQuery("#mock");
		var d = jQuery("#dash");
		var o = jQuery("#docs");
		var b = jQuery("#toolbar button");
		jQuery("#showDash").click(function(){
			b.removeClass("active");
			jQuery(this).addClass("active");
			m.hide();
			o.hide();
			d.show();
		});
		jQuery("#showMock").click(function(){
			b.removeClass("active");
			jQuery(this).addClass("active");
			d.hide();
			o.hide();
			m.show();
		});
		jQuery("#showDocs").click(function(){
			b.removeClass("active");
			jQuery(this).addClass("active");
			m.hide();
			d.hide();
			o.show();
		});
	});
</script>




