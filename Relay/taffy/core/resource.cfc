<cfcomponent hint="base class for taffy REST components">

	<!--- helper functions --->
	<cffunction name="representationOf" access="private" output="false" hint="returns an object capable of serializing the data in a variety of formats">
		<cfargument name="data" required="true" hint="any simple or complex data that should be returned for the request" />
		<cfargument name="customRepresentationClass" type="string" required="false" default="" hint="pass in the dot.notation.cfc.path for your custom representation object" />

		<cfif arguments.customRepresentationClass eq "">
			<cfreturn getRepInstance(application._taffy.settings.defaultRepresentationClass).setData(arguments.data) /> 
		<cfelse>
			<cfreturn getRepInstance(arguments.customRepresentationClass).setData(arguments.data) />
		</cfif>
	</cffunction>

	<cffunction name="noData" access="private" output="false" hint="use this function to return only headers to the consumer, no data">
		<cfreturn createObject("component", application._taffy.settings.defaultRepresentationClass).noData() />
	</cffunction>

	<!---
		function that gets the representation class instance
		-- if the argument is a beanName, the bean is returned from the factory;
		-- otherwise it is assumed to be a cfc path and that cfc instance is returned
	--->
	<cffunction name="getRepInstance" access="private" output="false">
		<cfargument name="repClass" type="string" />
		<cfif application._taffy.factory.containsBean(arguments.repClass)>
			<cfreturn application._taffy.factory.getBean(arguments.repClass) />
		<cfelse>
			<cfreturn createObject("component", arguments.repClass) />
		</cfif>
	</cffunction>


	<cffunction name="logAndReturnDataAndStatusCode" access="private" output="false">
		<cfargument name="data" required="true" type="struct">
		<cfargument name="statusCode" type="numeric" required="true">
		
		<cfset var loggedData = structNew()>
		<!--- NJH 2012/05/02 - I've noticed a strange behaviour that one can get undefined arguments. In this case, we end up with a structure
			(if returning the arguments) that has undefined keys in it. This is not desirable for the user experience but it also breaks
			when we return with a format of xml. So, here we get rid of the keys --->
		<cfset application.com.structureFunctions.removeUndefinedStructKeys(data=arguments.data)>
		
		<cfset loggedData = duplicate(arguments.data)>
		<cfif structKeyExists(loggedData,"recordSet")>
			<cfset structDelete(loggedData,"recordSet")>
		</cfif>
		
		<!--- NJH 2012/03/28 - put the data into the api request log so we have an audit of what was returned to the user --->
		<cfset application.com.relayAPI.updateRequestLog(data=loggedData,statusCode=arguments.statusCode)>
		
		<cfreturn representationOf(arguments.data).withStatus(arguments.statusCode) />
	</cffunction>
	
</cfcomponent>