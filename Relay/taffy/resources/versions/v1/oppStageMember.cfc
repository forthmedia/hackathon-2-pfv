<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/oppStage/{oppStageID}" hint="Accessing and modify a oppStage record.">

	<cfset this.entityTypeID = application.entityTypeID.oppStage>
	
	<cffunction name="get" access="public" output="false" hint="Retrieve a oppStage record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of oppStage field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.oppStageID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
