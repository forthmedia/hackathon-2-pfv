<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/person/{personID}" hint="Access and modify a person record.">

	<cfset this.entityTypeID = application.entityTypeID.person>

	<cffunction name="get" access="public" output="false" hint="Retrieve a person record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of person field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.personID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a person record.">

		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.personID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a person record.">
		<cfargument name="personID" type="numeric" required="true" hint="The personID of the record to update.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.personID,entityTypeID=this.entityTypeID)>
			
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
