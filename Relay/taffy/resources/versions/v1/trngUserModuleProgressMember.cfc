<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/trngUserModuleProgress/{UserModuleProgressID}" hint="Access and modify a trngUserModuleProgress record.">

	<cfset this.entityTypeID = application.entityTypeID.trngUserModuleProgress>

	<cffunction name="get" access="public" output="false" hint="Retrieve a trngUserModuleProgress record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of UserModuleProgress field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.UserModuleProgressID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

		
	<cffunction name="put" access="public" output="false" hint="Update a trngUserModuleProgress record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.UserModuleProgressID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a trngUserModuleProgress record">
		<cfargument name="UserModuleProgressID" type="numeric" required="true" hint="The trngUserModuleProgressID of the record to delete.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.UserModuleProgressID,entityTypeID=this.entityTypeID)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
