<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/countries" hint="Access country records.">

	<cfset this.entityTypeID = application.entityTypeID.country>  

	<cffunction name="get" access="public" output="false" hint="Retrieve country records.">
		<cfargument name="fieldList" type="string" default="" hint="A list of country field names. The default is all fields. Use the entity method to view all retrievable fields.">
		<cfargument name="queryID" type="string" required="false" hint="An ID used where more records have been returned than is allowed by the query batch size setting. Use to retrieve additional records.">
		<cfargument name="filter" type="string" required="false" hint="A filter to reduce the records returned.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeId,argumentCollection=arguments)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>