<!---<!--- �Relayware. All Rights Reserved 2014 --->--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/file/{fileID}" hint="Accessing and modify a files record.">

	<cfset this.entityTypeID = application.entityTypeID.files>


	<cffunction name="put" access="public" output="false" hint="Update a files record.">

		<cfset var apiResponse = application.com.relayApi.upsertFile(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.fileID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a files record.">
		<cfargument name="fileID" type="numeric" required="true" hint="The fileID of the record to delete.">

		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.fileID,entityTypeID=this.entityTypeID)>

		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
