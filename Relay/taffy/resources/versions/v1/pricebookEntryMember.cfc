<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-19	RPW	FIFTEEN-215 - API - [GET] command is not working for [pricebookEntryMember]
--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/pricebookEntry/{pricebookEntryID}" hint="Access and modify a pricebookEntry record.">

	<cfset this.entityTypeID = application.entityTypeID.pricebookEntry>

	<cffunction name="get" access="public" output="false" hint="Retrieve a pricebookEntry record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of pricebookEntry field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<!--- 2015-02-19	RPW	FIFTEEN-215 Replaced argument entityID passed to getEntity with arguments.pricebookEntryID --->
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.pricebookEntryID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a pricebookEntry record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.pricebookEntryID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a pricebookEntry record.">		
		<cfargument name="pricebookEntryID" type="numeric" required="true" hint="The pricebookEntryID of the record to delete.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.pricebookEntryID,entityTypeID=this.entityTypeID)>
				
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
		
	</cffunction>	

</cfcomponent>
