<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/trngModule/{ModuleID}" hint="Access and modify a trngModule record.">

	<cfset this.entityTypeID = application.entityTypeID.trngModule>

	<cffunction name="get" access="public" output="false" hint="Retrieve an trngModule record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of trngModule fields. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.ModuleID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

		
	<cffunction name="put" access="public" output="false" hint="Update a trngModule record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.ModuleID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a trngModule record">
		<cfargument name="ModuleID" type="numeric" required="true" hint="The trngModuleID of the record to delete.">
			
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.ModuleID,entityTypeID=this.entityTypeID)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
