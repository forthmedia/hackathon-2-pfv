<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/opportunity/{opportunityID}" hint="Access and modify an opportunity record.">

	<cfset this.entityTypeID = application.entityTypeID.opportunity>

	<cffunction name="get" access="public" output="false" hint="Retrieve an opportunity record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of opportunity field names. The default is all field. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.opportunityID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>
	
	
	<cffunction name="put" access="public" output="false" hint="Update an opportunity record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.opportunityID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
