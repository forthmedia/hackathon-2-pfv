<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-23	RPW	FIFTEEN-219 - API - [DELETE] command is flagging an organisation for deletion even when the org has an [HQ] location associated with it.
--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/organization/{organizationID}" hint="Access and modify an organization record.">

	<cfset this.entityTypeID = application.entityTypeID.organisation>

	<cffunction name="get" access="public" output="false" hint="Retrieve an organization record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of organization field names. The default is all fields. Use the entity method to view all retrievable fields.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.organizationID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

		
	<cffunction name="put" access="public" output="false" hint="Update an organization record.">

		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.organizationID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete an organization record.">
		<cfargument name="organizationID" type="numeric" required="true" hint="The organizationID of the record to update.">
		
		<!--- 2015-02-23	RPW	FIFTEEN-219 - API - Changed to check organisation, locations and people before deleting. --->
		<cfset var apiResponse = application.com.relayApi.deleteOrganisation(entityid=arguments.organizationID,entityTypeID=this.entityTypeID,datasource=application.SiteDataSource,relayCurrentUserPersonid=request.relayCurrentUser.personid,relayCurrentUserUsergroupid=request.relayCurrentUser.usergroupid)>
			
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>		
	</cffunction>

</cfcomponent>
