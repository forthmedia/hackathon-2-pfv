<!--- �Relayware. All Rights Reserved 2014 --->
<!---
 	Custom endpoints (cfcs) should have methods get, put, post, delete (whichever are to be implemented).
	
	They should be placed in Code/CfTemplates/API.
	
	These methods should return a struct with members:
		* data (which should itself be a struct)
		* statusCode (which should be 200 on success)
--->
<!---Amendment History
Amendment History
2016-01-06	RJT   	BF-174 custom endpoints fail gracefully when no custom endpoint can be found
			

--->

<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/custom/{name}" hint="Custom API names.">

	<cffunction name="get" access="public" output="false" hint="Get verb.">
		<cfscript>
			var endPointHander=getEndpointHandler(name);
			var apiResponse="";
			if (endPointHander.isOk){
				var apiResponse = endPointHander.handler.get(argumentCollection=arguments);
				return representationOf(apiResponse.data).withStatus(apiResponse.statusCode);
			}else{
				var returnStruct={message=endPointHander.message};
				return representationOf(returnStruct);
			}
		</cfscript>	
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Put or update verb.">
	  	<cfscript>
			var endPointHander=getEndpointHandler(name);
			var apiResponse="";
			if (endPointHander.isOk){
				var apiResponse = endPointHander.handler.put(argumentCollection=arguments);
				return representationOf(apiResponse.data).withStatus(apiResponse.statusCode);
			}else{
				var returnStruct={message=endPointHander.message};
				return representationOf(returnStruct);
			}
		</cfscript>	
	  	
	  	
	</cffunction>


	<cffunction name="post" access="public" output="false" hint="Post or insert verb.">
		<cfscript>
			var endPointHander=getEndpointHandler(name);
			var apiResponse="";
			if (endPointHander.isOk){
				var apiResponse = endPointHander.handler.post(argumentCollection=arguments);
				return representationOf(apiResponse.data).withStatus(apiResponse.statusCode);
			}else{
				var returnStruct={message=endPointHander.message};
				return representationOf(returnStruct);
			}
		</cfscript>	
	</cffunction>

	<cffunction name="delete" access="public" output="false" hint="Delete verb.">
		<cfscript>
			var endPointHander=getEndpointHandler(name);
			var apiResponse="";
			if (endPointHander.isOk){
				var apiResponse = endPointHander.handler.delete(argumentCollection=arguments);
				return representationOf(apiResponse.data).withStatus(apiResponse.statusCode);
			}else{
				var returnStruct={message=endPointHander.message};
				return representationOf(returnStruct);
			}
		</cfscript>			
	</cffunction>

	<cffunction name="getEndpointHandler"  access="private" returntype="struct" output="false">
		<cfargument name="handlerName" required="true">
		
		<cfscript>
			var returnStruct={isOk=false, message="", handler=""};
			
			try{
				returnStruct.handler=CreateObject('Component','Code.CfTemplates.API.#handlerName#').Init();
				returnStruct.isOk=true;
				return returnStruct;
			}catch(any e){
				errorId=application.com.errorhandler.recordRelayError_Warning(caughtError=e);
				returnStruct.isOk=false;
				returnStruct.message="No such endpoint or endpoint misconfigured (see more details see error #errorId#)";
			}
		
		
		</cfscript>

		
		<cfreturn returnStruct>
	
	</cffunction>

</cfcomponent>
