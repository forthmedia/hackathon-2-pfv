<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/lead/{leadID}" hint="Access and modify a lead record.">

	<cfset this.entityTypeID = application.entityTypeID.lead>

	<cffunction name="get" access="public" output="false" hint="Retrieve a lead record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of lead field names. The default is all fields. Use the entity method to view all retrievable fields.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.leadID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a lead record.">
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.leadID,method="update", customPostRightsCheck=new leadConvertedChecker())>
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>



</cfcomponent>
