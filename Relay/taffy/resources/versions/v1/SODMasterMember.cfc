<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/SODMaster/{SODMasterID}" hint="Accessing and modify a SODMaster record.">

	<cfset this.entityTypeID = application.entityTypeID.SODMaster>

	<cffunction name="post" access="public" output="false" hint="Insert a SODMaster record.">
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>

		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a SODMaster record.">

		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.SODMasterID,method="update")>

		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a SODMaster record.">
		<cfargument name="SODMasterID" type="numeric" required="true" hint="The SODMasterID of the record to delete.">

		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.SODMasterid,entityTypeID=this.entityTypeID)>

		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
