<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/location/{locationID}" hint="Accessing and modify a location record.">

	<cfset this.entityTypeID = application.entityTypeID.location>
	
	<cffunction name="get" access="public" output="false" hint="Retrieve a location record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of location field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.locationID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

	
	<cffunction name="put" access="public" output="false" hint="Update a location record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.locationID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a location record.">
		<cfargument name="locationID" type="numeric" required="true" hint="The locationID of the record to delete.">		
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.locationid,entityTypeID=this.entityTypeID)>
				
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>	
	</cffunction>	

</cfcomponent>
