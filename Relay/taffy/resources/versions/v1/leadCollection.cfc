<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/leads" hint="Access,insert and modify lead records.">

	<cfset this.entityTypeID = application.entityTypeID.lead>

	<cffunction name="get" access="public" output="false" hint="Retrieve lead records.">
		<cfargument name="fieldList" type="string" default="" hint="A list of lead field names. The default is all fields. Use the entity method to view all retrievable fields.">
		<cfargument name="queryID" type="string" required="false" hint="An ID used where more records have been returned than is allowed by the query batch size setting. Use to retrieve additional records.">
		<cfargument name="filter" type="string" required="false" default="" hint="A filter to reduce the records returned.">

		<!---add on an extra filter to eliminate converted leads (which are supposed to be invisible--->
		<cfset filter=(len(filter) GT 0?filter & " AND ":"") & "convertedLocationID IS NULL AND convertedPersonID IS NULL AND convertedOpportunityID IS NULL">
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeId,argumentCollection=arguments)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="post" access="public" output="false" hint="Insert an lead record.">

		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>

	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update an lead record.">
		<cfargument name="leadID" type="numeric" required="true" hint="The leadID of the record to update.">

		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.leadID,method="update", customPostRightsCheck=new leadConvertedChecker())>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>