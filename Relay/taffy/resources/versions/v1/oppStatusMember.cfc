<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/oppStatus/{oppStatusID}" hint="Accessing and modify a oppStatus record.">

	<cfset this.entityTypeID = application.entityTypeID.oppStatus>
	
	<cffunction name="get" access="public" output="false" hint="Retrieve a oppStatus record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of oppStatus field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.oppStatusID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
