<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-19	RPW	FIFTEEN-214 - API - [GET] command is not working for [pricebookMember]
--->

<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/pricebook/{pricebookID}" hint="Access and modify a pricebook record.">

	<cfset this.entityTypeID = application.entityTypeID.pricebook>

	<cffunction name="get" access="public" output="false" hint="Retrieve a pricebook record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of pricebook field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<!--- 2015-02-19	RPW	FIFTEEN-214 - Changed entityID parameter to use arguments.pricebookID. arguments.entityID is not passed in --->
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.pricebookID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a pricebook record.">
		<cfargument name="pricebookID" type="numeric" required="true" hint="The pricebookID of the record to update.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.pricebookID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a pricebook record.">
		<cfargument name="pricebookID" type="numeric" required="true" hint="The pricebookID of the record to delete.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.pricebookID,entityTypeID=this.entityTypeID)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>		
	</cffunction>

</cfcomponent>
