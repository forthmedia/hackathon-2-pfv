<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/trngModules" hint="Access,insert and modify Training Module records.">

	<cfset this.entityTypeID = application.entityTypeID.trngModule>

	<cffunction name="get" access="public" output="false" hint="Retrieve Training Module records.">
		<cfargument name="fieldList" type="string" default="" hint="The list of opportunity product field names. The default is all fields. Use the entity method to view all retrievable fields.">
		<cfargument name="queryID" type="string" required="false" hint="An ID used where more records have been returned than is allowed by the query batch size setting. Use to retrieve additional records.">
		<cfargument name="filter" type="string" required="false" hint="A filter to reduce the records returned. It must be url-encoded.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeId,argumentCollection=arguments)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="post" access="public" output="false" hint="Insert a Training Module record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	
	</cffunction>
	
	

</cfcomponent>