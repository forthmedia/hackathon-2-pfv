<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-19	RPW	FIFTEEN-216 - API - [GET] command is not working for [productGroupMember]
--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/productGroup/{productGroupID}" hint="Access and modify a productGroup record.">

	<cfset this.entityTypeID = application.entityTypeID.productGroup>

	<cffunction name="get" access="public" output="false" hint="Retrieve a productGroup record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of productGroup field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<!--- 2015-02-19	RPW	FIFTEEN-216 - Changed entityID parameter to use arguments.productGroupID. arguments.entityID is not passed in --->
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.productGroupID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="put" access="public" output="false" hint="Update a productGroup record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.productGroupID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
