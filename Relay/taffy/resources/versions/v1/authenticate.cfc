<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-23	RPW	FIFTEEN-230 - API - unable to refresh available fields
--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/authenticate" hint="API Authentication">

	<cffunction name="post" access="public" output="false" hint="Authenticate a user. The user must be a valid internal user and must be in the API usergroup. Returns a sessionID to be used on all requests requiring authentication.">
		<cfargument name="username" type="string" required="true" hint="A user's username.">
		<cfargument name="password" type="string" required="true" hint="A user's password.">
		
		<!--- 2015-02-23	RPW	FIFTEEN-230 - API - Added struct clear to reset session when user logs in as sessionID is not the same as the actual session. --->
		<cfscript>
			StructClear(session);
		</cfscript>
		<cfset var sessionID="">
		<cfset var authenticateUser = application.com.login.authenticateUserV2(argumentCollection=arguments,showComplexityWidget=false)>
		<cfset var message = authenticateUser.message>
		<cfset var result = {errorCode=""}>
		<cfset var apiResponse = structNew()>
		
		<!--- Need to respond to 
			Expired - user's password has expired
			Unauthorised - isn't set up as an API user
			OK
		 --->
		<cfif authenticateUser.isOK>
			<!--- check if user is in API usergroup --->
			<cfif application.com.login.isPersonInOneOrMoreUserGroups(personID=authenticateUser.personID,userGroups="API")>
				<cfset result.sessionID = application.com.relayAPI.createUserSession(personID=authenticateUser.personID)>
				<!--- <cfset result.personID = authenticateUser.personID> --->
			<cfelse>
				<cfset result.errorCode ="INVALID_API_USER">
			</cfif>
		<cfelse>
			<cfset result.errorCode = "INVALID_LOGIN_CREDENTIALS">
		</cfif>
		
		<cfset apiResponse = application.com.relayApi.prepareApiResponse(result=result,errorCode=result.errorCode)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>
	
</cfcomponent>