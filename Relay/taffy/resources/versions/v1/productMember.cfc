<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/product/{productID}" hint="Access and modify a product record.">

	<cfset this.entityTypeID = application.entityTypeID.product>

	<cffunction name="get" access="public" output="false" hint="Retrieve a product record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of product field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.productID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

	
	<cffunction name="put" access="public" output="false" hint="Update a product record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.productID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a product record.">		
		<cfargument name="productID" type="numeric" required="true" hint="The productID of the record to delete.">			
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.productID,entityTypeID=this.entityTypeID)>		
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>		
	</cffunction>

</cfcomponent>
