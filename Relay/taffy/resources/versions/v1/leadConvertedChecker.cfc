/**
 * Checks that a lead hasn't been converted
 *
 * @author Richard.Tingle
 * @date 30/01/15
 **/
component implements=interfaces.RelayEntityPostRightsCheck accessors=true output=false persistent=false {

	public array function getRequiredFields(){
		var requiredFields=[];
		requiredFields[1]="convertedLocationID";
		requiredFields[2]="convertedPersonID";
		requiredFields[3]="convertedOpportunityID";

		return requiredFields;
	}

	public boolean function postRightsCheckFunction(required struct entityToCheck hint="The function that is run to check that the entity (lead in this case) can be modified"){
		var underlyingQueryResults=entityToCheck.getResult();
		writeDump(var="#underlyingQueryResults#", output="D:/web/cfdump2.txt");

		for (row = 1; row LTE underlyingQueryResults.RecordCount ; row = (row + 1)){

		        // Output the name some values. When doing so, access the
		        // query as if it were a structure of arrays and we want
		        // only get values from this row index.

			var fieldsThatMustBeNull=getRequiredFields();
			for(i=1; i LTE ArrayLen(fieldsThatMustBeNull); i=i+1){
				var index=fieldsThatMustBeNull[i];

				//we require all the converted fields to be null in the database (empty string in cold fusion), if any aren't
				//the lead has been converted and we're not happy'
				if (len(underlyingQueryResults[index][row]) NEQ 0){
					return false; //it's converted, this is bad, return false indicating that we are not happy!
				}

			}


		}

		return true; //we've got the end without anything suggesting a converted lead
	}

	public string function getFailureMessage() output="false" {
		return "Converted leads cannot be updated";
	}

	public string function getFailureError() output="false" {
		return "LEAD_CONVERTED_TO_OPPORTUNITY";
	}

}