<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/pricebooks" hint="Access,insert and modify pricebook records.">

	<cfset this.entityTypeID = application.entityTypeID.pricebook>  

	<cffunction name="get" access="public" output="false" hint="Retrieve pricebook records.">
		<cfargument name="fieldList" type="string" default="" hint="A list of pricebook field names. The default is all fields. Use the entity method to view all retrievable fields.">
		<cfargument name="queryID" type="string" required="false" hint="An ID used where more records have been returned than is allowed by the query batch size setting. Use to retrieve additional records.">
		<cfargument name="filter" type="string" required="false" hint="A filter to reduce the records returned.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeId,argumentCollection=arguments)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="post" access="public" output="false" hint="Insert a pricebook record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	
	</cffunction>
	
	
	<cffunction name="put" access="public" output="false" hint="Update a pricebook record.">
		<cfargument name="pricebookID" type="numeric" required="true" hint="The pricebookID of the record to update.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.pricebookID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a pricebook record.">
		<cfargument name="pricebookID" type="numeric" required="true" hint="The pricebookID of the record to delete.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.pricebookID,entityTypeID=this.entityTypeID)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>		
	</cffunction>	

</cfcomponent>