<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/country/{countryID}" hint="Access a country record.">

	<cfset this.entityTypeID = application.entityTypeID.country>

	<cffunction name="get" access="public" output="false" hint="Retrieve a country record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of country field names. The default is all fields. Use the entity method to view all retrievable fields.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.countryId,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>