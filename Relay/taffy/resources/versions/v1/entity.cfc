<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-19	RPW	FIFTEEN-213 - API - [GET] command is not working for [entity]
--->

<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/entity/{entity}" hint="Describe Relayware Entities">

	<cffunction name="get" access="public" output="false" hint="Retrieve entity meta data.">
		<cfargument name="entity" type="string" required="true" hint="The Relayware entity to describe.">

		<cfset var result = {errorCode=""}>
		<cfset var entityType = arguments.entity>
		<cfset var apiResponse = structNew()>
		<cfset var entityStruct = structNew()>
		<!--- 2015-02-19	RPW	FIFTEEN-213 - Allowed for invalid entities of one character being passed in. --->
		<cfscript>
			var entityName = arguments.entity;
			if (Len(arguments.entity) > 1) {
				var entityName = ucase(left(arguments.entity, 1))&lcase(right(arguments.entity,len(arguments.entity)-1));
			}
		</cfscript>

		<cftry>
			<cfif listFindNoCase("organization,location,person,opportunity,opportunityProduct,lead,oppStage,oppStatus,country,product,productGroup,pricebook,pricebookEntry,productCategory,productFamily,files,SODMaster,trngUserModuleProgress,trngModule",entityType)>

				<cfif entityType eq "organization">
					<cfset entityType = "organisation">
				<!--- <cfelseif entityType eq "oppProduct">
					<cfset entityType = "oppProduct s"> --->
				</cfif>

				<cfset entityStruct = application.com.relayEntity.getEntityMetaData(entityTypeID=entityType,returnApiName=true)>
				<cfset result[entityName] = entityStruct[entityType]>
			<!--- unsupported entity --->
			<cfelse>
				<cfset result.errorCode = "INVALID_ENTITY">
			</cfif>

			<cfcatch>
				<cfset result.errorCode = "QUERY_ERROR">
				<cfset result.errorID = application.com.errorHandler.recordRelayError_Warning(type="relayEntity Error",Severity="Coldfusion",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>


		<cfset apiResponse = application.com.relayApi.prepareApiResponse(result=result,errorCode=result.errorCode)>

		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>