<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/productFamily/{productFamilyID}" hint="Accessing and modify a productFamily record.">

	<cfset this.entityTypeID = application.entityTypeID.productFamily>
	
	<cffunction name="get" access="public" output="false" hint="Retrieve a productFamily record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of productFamily field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.productFamilyID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

	
	<cffunction name="put" access="public" output="false" hint="Update a productFamily record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.productFamilyID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a productFamily record.">
		<cfargument name="productFamilyID" type="numeric" required="true" hint="The productFamilyID of the record to delete.">		
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.productFamilyid,entityTypeID=this.entityTypeID)>
				
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>	
	</cffunction>	

</cfcomponent>
