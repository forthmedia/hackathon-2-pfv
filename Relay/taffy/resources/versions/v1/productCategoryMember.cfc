<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/productCategory/{productCategoryID}" hint="Accessing and modify a productCategory record.">

	<cfset this.entityTypeID = application.entityTypeID.productCategory>
	
	<cffunction name="get" access="public" output="false" hint="Retrieve a productCategory record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of productCategory field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.productCategoryID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

	
	<cffunction name="put" access="public" output="false" hint="Update a productCategory record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.productCategoryID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a productCategory record.">
		<cfargument name="productCategoryID" type="numeric" required="true" hint="The productCategoryID of the record to delete.">		
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.productCategoryid,entityTypeID=this.entityTypeID)>
				
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>	
	</cffunction>	

</cfcomponent>
