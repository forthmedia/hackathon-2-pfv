<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/oppProduct/{oppProductID}" hint="DEPRECATED Use OpportunityProduct instead.">

	<cfset this.entityTypeID = application.entityTypeID.opportunityProduct>

	<cffunction name="get" access="public" output="false" hint="Retrieve an opportunity product record.">
		<cfargument name="fieldList" type="string" default="" hint="A list of oppProduct field names. The default is all fields. Use the entity method to view all retrievable fields.">
		
		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeID,entityID=arguments.entityID,fieldList=arguments.fieldList)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

		
	<cffunction name="put" access="public" output="false" hint="Update an opportunity product record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.oppProductID,method="update")>
		
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete an organization record">
		<cfargument name="oppProductID" type="numeric" required="true" hint="The oppProductID of the record to delete.">
			
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.oppProductID,entityTypeID=this.entityTypeID,harddelete=true)>
		
		<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>

</cfcomponent>
