// ©Relayware. All Rights Reserved 2014
/*

	2016/08/22 RJT2 Created for Jira PROD2016-2198. Phrase API functionality

*/
component extends="taffy.core.resource" taffy_uri="/v1/phrase/{phraseID}" {

	public any function get( ) {

		var phraseData = application.com.phraseService.getPhrase( phraseID = arguments.phraseID );
		var apiResponse = application.com.relayApi.prepareApiResponse( result = phraseData.data , errorCode = phraseData.errorcode );
 	  	return logAndReturnDataAndStatusCode( data = apiResponse.result , statusCode = apiresponse.statuscode);
 	}

	public any function put( ) {

		var phraseData = application.com.phraseService.doPhraseTranslations( phraseObject = arguments , ID = request.relayCurrentUser.personid );
		var apiResponse = application.com.relayApi.prepareApiResponse( result = phraseData.data , errorCode = phraseData.errorcode );
		return logAndReturnDataAndStatusCode( data = apiResponse.result , statusCode = apiResponse.statuscode);
 	}


}
