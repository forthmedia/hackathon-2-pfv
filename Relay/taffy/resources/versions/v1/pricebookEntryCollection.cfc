<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/pricebookEntries" hint="Access and modify organization records.">

	<cfset this.entityTypeID = application.entityTypeID.pricebookEntry>  

	<cffunction name="get" access="public" output="false" hint="Retrieve pricebookEntry records.">
		<cfargument name="fieldList" type="string" default="" hint="A list of pricebookEntry field names. The default is all fields. Use the entity method to view all retrievable fields.">
		<cfargument name="queryID" type="string" required="false" hint="An ID used where more records have been returned than is allowed by the query batch size setting. Use to retrieve additional records.">
		<cfargument name="filter" type="string" required="false" hint="A filter to reduce the records returned.">

		<cfset var apiResponse = application.com.relayApi.getEntity(entityTypeID=this.entityTypeId,argumentCollection=arguments)>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="post" access="public" output="false" hint="Insert a pricebookEntry record.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	
	</cffunction>
	
	
	<cffunction name="put" access="public" output="false" hint="Update a pricebookEntry record.">
		<cfargument name="pricebookEntryID" type="numeric" required="true" hint="The pricebookEntryID of the record to update.">
		
		<cfset var apiResponse = application.com.relayApi.upsertEntity(entityDetails=arguments,entityTypeID=this.entityTypeID,entityID=arguments.pricebookEntryID,method="update")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
	</cffunction>


	<cffunction name="delete" access="public" output="false" hint="Delete a pricebookEntry record.">		
		<cfargument name="pricebookEntryID" type="numeric" required="true" hint="The pricebookEntryID of the record to delete.">
		
		<cfset var apiResponse = application.com.relayApi.deleteEntity(entityid=arguments.pricebookEntryID,entityTypeID=this.entityTypeID)>
				
	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>
		
	</cffunction>	
	
</cfcomponent>