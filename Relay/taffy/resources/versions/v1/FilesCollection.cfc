<!---<!--- �Relayware. All Rights Reserved 2014 --->--->
<cfcomponent extends="taffy.core.resource" taffy_uri="/v1/files" hint="Access,insert and modify files records.">

	<cfset this.entityTypeID = application.entityTypeID.files>

	<cffunction name="post" access="public" output="false" hint="Insert a files record.">

		<cfset var apiResponse = application.com.relayApi.upsertFile(entityDetails=arguments,entityTypeID=this.entityTypeID,method="insert")>

	  	<cfreturn logAndReturnDataAndStatusCode(data=apiResponse.result,statusCode=apiResponse.statusCode)>

	</cffunction>




</cfcomponent>