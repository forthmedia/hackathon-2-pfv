<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="taffy.core.resource" taffy_uri="/versions" hint="Available API Versions">

	<cffunction name="get" access="public" output="false" hint="View available API versions.">
		
		<cfset var versionDirectories = "">
		<cfset var getVersionDirectories = "">
		
		<cfdirectory action="list" directory="#application.paths.relay#\taffy\resources\versions" name="versionDirectories" filter="v*" type="dir" recurse="no" sort="asc">
		
		<cfquery name="getVersionDirectories" dbtype="query">
			select name, '/api/'+name as url from versionDirectories
		</cfquery>
		
		<cfreturn representationOf(getVersionDirectories).withStatus(200) />
	</cffunction>
</cfcomponent>