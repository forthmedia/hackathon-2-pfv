<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		synchLastSuccessfulRunReport.cfm	
Author:			GCC  
Date started:	12-12-2011
	
Description:	Show last run time of succesful definitions. 

Amendment History:

Date 		Initials 	What was changed
2013-04-04 	PPB 		show server time now (for accurate comparison)

Possible enhancements:

User friendly names instead of function name, ability to hide defs when inactive

 --->

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Last_Successful_Run_UTC desc">
<cfparam name="numRowsPerPage" default="50">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfset application.com.request.setTopHead(pageTitle = "Synching Last Successful Run")>
			
<cfquery name="getSalesForceLastRunDates" datasource="#application.siteDataSource#">
	select * from (
		select result as status,s.startTime as lastestScheduledLogStartTime,functionname as Schedule_Name,
			latestDTHwithSTDefID.startTime as Last_Successful_Run_UTC, latestDTHwithSTDefID.dtdID
		from 
			scheduledTaskLog s with (noLock)
			inner join 
				<!--- get the latest result and starttime for this particular scheduledTaskDef --->
				(select max(scheduledTaskLogID) as scheduledTaskLogID from scheduledTaskLog with (noLock) group by scheduledTaskDefID) smax
					on s.scheduledTaskLogID = smax.scheduledTaskLogID
			inner join 
				<!--- get the scheduledTaskDefID, and last function and starttime --->
				(select scheduledTaskDefID,dtd.functionname,dth.startTime,dtd.dtdID
					from scheduledTaskLog s with (noLock)
					inner join DataTransferHistory dth with (noLock) on dth.scheduledTaskLogID = s.scheduledTaskLogID
					inner join DataTransferdefinition dtd with (noLock) on dtd.dtdid = dth.dtdid
					inner join
						<!--- get the latest run time for each salesForce function --->
						(select functionname,max(dth.startTime) as startTime
						from DataTransferdefinition dtd with (noLock) inner join DataTransferHistory dth with (noLock) on dtd.dtdid = dth.dtdid
						where functionName like 'SalesForce%'
						group by functionname
						) latestDTH on latestDTH.functionname = dtd.functionname and latestDTH.starttime = dth.startTime
						) latestDTHwithSTDefID 
					on latestDTHwithSTDefID.scheduledTaskDefID = s.scheduledTaskDefID
	) base
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortorder#">
</cfquery>

<cfloop query="getSalesForceLastRunDates">
	<cftry>
		<cfif isJSON(status)>
			<cfset scheduledTaskResult = deserializeJSON(status)>
		<cfelse>
			<cfwddx action="wddx2cfml" input="#status#" output="scheduledTaskResult">
		</cfif>
		
		<cfcatch>
			<cfset scheduledTaskResult = structNew()>
		</cfcatch>
	</cftry>

	<cfset scheduledTaskMessage = "">
	<cfif structKeyExists(scheduledTaskResult,"message") and scheduledTaskResult.message eq "SalesForce Server Offline">
		<cfset scheduledTaskMessage = "<div class='infoblock'>Running, but SalesForce Server Offline</div>">
	<cfelseif dateDiff("n",Last_Successful_Run_UTC,request.requestTime) gt 120 and dateDiff("n",lastestScheduledLogStartTime,request.requestTime) lt 15>
		<cfset scheduledTaskMessage = "<div class='errorblock'>Not run successfully for more than 2 hours</div>">
	<cfelseif dateDiff("n",Last_Successful_Run_UTC,request.requestTime) gt 120>
		<cfset scheduledTaskMessage = "<div class='infoblock'>Not running</div>">
	<cfelseif (status eq "" and dateDiff("n",lastestScheduledLogStartTime,request.requestTime) lt 15) or structKeyExists(scheduledTaskResult,"isOK") and scheduledTaskResult.isOK>
		<cfset scheduledTaskMessage = "<div class='successblock'>Running</div>">
	</cfif>
	<cfset querySetCell(getSalesForceLastRunDates,"status",scheduledTaskMessage,currentRow)>
</cfloop>

<!--- convert col Last_Successful_Run_UTC to UTC  --->
<cfset getSalesForceLastRunDates = application.com.dateFunctions.convertServerDateColumnToUTC(queryObject=getSalesForceLastRunDates,columnNameList="Last_Successful_Run_UTC")>

<cf_tableFromQueryObject 
	queryObject="#getSalesForceLastRunDates#"
	queryName="getSalesForceLastRunDates"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	datetimeformat="Last_Successful_Run_UTC"
	showAsLocalTime="false"
	showTheseColumns="Schedule_Name,Last_Successful_Run_UTC,Status"
	hideTheseColumns=""
	columnTranslation="false"
	allowColumnSorting="no"
	HidePageControls="true"
	keyColumnList="Schedule_Name"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(Schedule_Name)##'*comma'##jsStringFormat(Schedule_Name)##'*comma'/salesforce/dataTransferTrace.cfm?dtdID=##jsStringFormat(dtdID)##'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;"
>
	<!--- START 2013-04-04 PPB show server time now (for accurate comparison)  --->
	<cfquery name="qryTimeNow" datasource="#application.siteDataSource#">
		SELECT GETDATE() AS TimeNow;
	</cfquery>

	<cfset getTimeNow = application.com.dateFunctions.convertServerDateColumnToUTC(queryObject=qryTimeNow,columnNameList="TimeNow")>

	<p>
	Database Time Now UTC: <cfoutput>#DateFormat(getTimeNow.TimeNow,"mmm d, yyyy")# #TimeFormat(getTimeNow.TimeNow, "HH:mm")#</cfoutput>	
	</p>
	<!--- END 2013-04-04 PPB --->


