<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		recordsToBeSynchedReport.cfm	
Author:			NJH  
Date started:	29-06-2012
	
Description:	A report to show records that would get picked up in the next run. A debugging tool.		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/03/12			NJH			Case 434100 - added structKeyExists around some bits as the function that sets the trace struct is overridden, and not all variables are set
								Also set a timeout. 
2014-07-14 			PPB 		Case 440798 create a relay error for more debug info

Possible enhancements:


 --->

<cfsetting requesttimeout="3600">

<cfquery name="getSalesForceDataTransferID" datasource="#application.siteDataSource#">
	select dtdID from dataTransferDefinition where functionName = 'salesForcePolSynch'
</cfquery>

<cfset dtdID=getSalesForceDataTransferID.dtdId>
<cfset message = "">

<cftry>
	<cfset result = application.com.salesForce.salesForcePolSynch(dtdID=dtdId,dtStartTime=request.requestTime,dryRun=true).traceStruct>
	
	<cfcatch>
		<cfset message = "Error running dry run. #cfcatch.message#">
		<cfset application.com.errorHandler.recordRelayError_Warning(type="Sales Force Dry Run",Severity="error",caughtError=cfcatch)>		<!--- 2014-07-14 PPB Case 440798 create a relay error for more debug info --->
	</cfcatch>
</cftry>

<cfoutput>
	<cfif message eq "">
		<style>
			.sfdfDebugLabel {font-weight:bold;vertical-align:top;}
		</style>
		
		
		<table>
			<tr>
				<th colspan="2">POL Records to be Synched</th>
			</tr>
			<!--- the override verion does not contain these columns --->
			<cfif structKeyExists(result,"modifiedFrom")>
			<tr>
				<td class="sfdfDebugLabel">Modified Dates</td><td>From: #result.modifiedFrom# To: #result.modifiedTo#</td>
			</tr>
			</cfif>
			<tr>
				<td class="sfdfDebugLabel">Records To Be Exported</td><td><cfdump var="#result.exportEntities#"></td>
			</tr>
			<cfif structKeyExists(result,"exportSQL")>
			<tr>
				<td class="sfdfDebugLabel">Export Records SQL</td><td><cfloop list="#result.exportSQL#" index="sql" delimiters="|">#sql#<br><br></cfloop></td>
			</tr>
			</cfif>
			<tr>
				<td class="sfdfDebugLabel">Records To Be Imported</td><td><cfdump var="#result.importObjects#"></td>
			</tr>
			<cfif structKeyExists(result,"importSQL")>
			<tr>
				<td class="sfdfDebugLabel">Import Records SQL</td><td><cfloop list="#result.importSQL#" index="sql" delimiters="|">#sql#<br><br></cfloop></td>
			</tr>
			</cfif>
		</table>
	<cfelse>
		<div class="errorblock">#message#</div>
	</cfif>
</cfoutput>