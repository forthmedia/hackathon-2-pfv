<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			salesForceReportFunctions.cfm
Author:				NJH
Date started:		2010/09/30
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-10-12	IH	Case 428026 added showResumeSynchOnErrorReport

Possible enhancements:


 --->

<cfparam name="showResumeSynch" type="boolean" default="false">
<cfparam name="showSuspendSynch" type="boolean" default="false">
<cfparam name="showDeleteErrors" type="boolean" default="false">
<cfparam name="showPrioritisePending" type="boolean" default="false">
<cfparam name="showResumeSynchOnErrorReport" type="boolean" default="false">

<cfset functionQueryString = application.com.regExp.removeItemFromNameValuePairString(inputString=request.query_string,itemToDelete="action",delimiter="&")>

<cfscript>	
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "SalesForce Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

if (showResumeSynch) {
	comTableFunction.functionName = "Resume Synch";
	comTableFunction.securityLevel = "";
	/* PJP CASE 432119: Changed the method for deleting to use form post method */
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to resume synching the selected entities?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/salesForce/synchSuspendReport.cfm?action=suspend';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (showResumeSynchOnErrorReport) {
	comTableFunction.functionName = "Resume Synch";
	comTableFunction.securityLevel = "";
	/* PJP CASE 432119: Changed the method for deleting to use form post method */
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to resume synching the selected entities?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/salesForce/errorReport.cfm?action=resumesync&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (showSuspendSynch) {
	comTableFunction.functionName = "Suspend Synch";
	comTableFunction.securityLevel = "";
	/* PJP CASE 432119: Changed the method for deleting to use form post method */
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to suspend synching the selected entities?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/salesForce/errorReport.cfm?action=suspend&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (showDeleteErrors) {
	comTableFunction.functionName = "Delete Errors";
	comTableFunction.securityLevel = "";
	/* PJP CASE 432119: Changed the method for deleting to use form post method */
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to delete the selected error records?\nAll related error records will also be deleted.')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/salesForce/errorReport.cfm?action=deleteError&#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (showPrioritisePending) {
	comTableFunction.functionName = "Prioritise Pending";
	comTableFunction.securityLevel = "";
	/* PJP CASE 432119: Changed the method for deleting to use form post method */
	comTableFunction.url = "javascript:if (confirm('Are you sure you want to prioritise the selected pending records?\n')) {document.frmBogusForm.method='post';document.frmBogusForm.action='/salesForce/synchPendingReport.cfm?#urlEncodedFormat(functionQueryString)#';document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}
</cfscript>


