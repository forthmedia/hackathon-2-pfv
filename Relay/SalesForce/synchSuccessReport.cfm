<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		synchSuccessReport.cfm
Author:			NJH
Date started:	23-10-2009

Description:	Show successful synchs.. Currently only for opportunities

Amendment History:

Date 		Initials 	What was changed
2011/01/31	PPB			LID5510 - references to application.flag changed to use application.com.flag.getFlagStructure()
2014-04-02 	PPB 		Case 439295 added requesttimeout and maxRows


Possible enhancements:


 --->

<cfsetting requesttimeout="900">		<!--- 2014-04-02 PPB Case 439295 added requesttimeout --->

<cf_includeJavascriptOnce template="/javascript/viewEntity.js">
<cf_includeJavascriptOnce template="/salesforce/js/salesforce.js">

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="synch_Date desc">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="fromDate" default="">
<cfparam name="toDate" default="">
<cfparam name="entityType" default="">
<cfparam name="direction" default="">
<cfparam name="maxRows" default="5000">		<!--- 2014-04-02 PPB Case 439295 added maxRows --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfif structKeyExists(form,"frmShowAll")>
	<cfset fromDate = "">
	<cfset toDate = "">
</cfif>

<cfset pageTitle = "Connector Success Report">
<cfif entityType neq "">
	<cfset pageTitle = pageTitle & " for #entityType# Records">
</cfif>
<cfif direction eq "export">
	<cfset pageTitle = pageTitle&", Out to Salesforce.com">
<cfelseif direction eq "import">
	<cfset pageTitle = pageTitle&", In from Salesforce.com">
</cfif>
<cfif fromDate neq "">
	<cfset pageTitle = pageTitle&", from #dateFormat(fromDate,"full")#">
</cfif>
<cfif toDate neq "">
	<cfset pageTitle = pageTitle&", to #dateFormat(toDate,"full")#">
	<cfset toDatePlusOneDay = dateAdd("d",1,toDate)>
</cfif>
<cfset application.com.request.setTopHead(pageTitle = pageTitle)>

<cfset objectEntityStruct = application.com.salesForce.putMappedObjectsInTable(entityType=entityType)>

<cfset synchedExportFlags = "0">
<cfset synchedImportFlags = "0">

<cfloop list="#objectEntityStruct.entityList#" index="entity">
	<cfset flagTextPrefix = entity>
	<cfif listFindNoCase("organisation,location,person,opportunity",entity)>
		<cfset flagTextPrefix = left(entity,3)>
	</cfif>

	<cfif direction eq "import" or direction eq "">
		<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_import").isOK>
			<cfset synchedImportFlags = listAppend(synchedImportFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_import").flagID)>
		</cfif>
	</cfif>

	<cfif direction eq "export" or direction eq "">
		<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_export").isOK>
			<cfset synchedExportFlags = listAppend(synchedExportFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_export").flagID)>
		</cfif>
	</cfif>
</cfloop>

<cfquery name="getSalesForceSuccesses">
	select top #maxRows# * from (								<!--- 2014-04-02 PPB Case 439295 added maxRows --->
		select entityTable as Entity,
			v.name,v.entityId as relaywareID,
			<cfif listFindNoCase(objectEntityStruct.entityList,"person")>
				personLocation.sitename as location,
			</cfif>
			<cfif listFindNoCase(objectEntityStruct.entityList,"person") or listFindNoCase(objectEntityStruct.entityList,"location")>
				locationOrganisation.organisationName as organisation,
			</cfif>
			v.crmID as salesForceID,
			bfd.lastUpdated as synch_Date,
			case when bfd.flagID in (#synchedExportFlags#) then 'Export' else 'Import' end as Direction
		from vflagdef f
			inner join booleanflagdata bfd with (noLock) on f.flagid = bfd.flagid
			inner join ##sfEntityMapping sm on sm.entityName = f.entityTable
			inner join vEntityName v on v.entityTypeID = sm.entityTypeID and bfd.entityID = v.entityID
			<cfif listFindNoCase(objectEntityStruct.entityList,"person")>
				left join person person with (noLock) on v.entityID = person.personID and v.entityTypeId = #application.entityTypeID.person#
				left join location personLocation with (noLock) on personLocation.locationID = person.locationID
				left join organisation locationOrganisation with (noLock) on locationOrganisation.organisationID = person.organisationID
			<cfelseif listFindNoCase(objectEntityStruct.entityList,"location")>
				left join location location with (noLock) on v.entityID = location.locationID and v.entityTypeId = #application.entityTypeID.location#
				left join organisation locationOrganisation with (noLock) on locationOrganisation.organisationID = location.organisationID
			</cfif>
		where f.flagid in (#synchedExportFlags#,#synchedImportFlags#)
			<cfif isDate(fromDate)>and bfd.lastUpdated >= <cf_queryparam value="#fromDate#" cfsqltype="cf_sql_timestamp"></cfif>
			<cfif isDate(toDate)>and  bfd.lastUpdated <= <cf_queryparam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timestamp"></cfif>
			<cfif entityType neq "">and <cfif direction eq "import">sm.object<cfelse>sm.entityName</cfif> = <cf_queryparam value="#entityType#" cfsqltype="cf_sql_varchar"> </cfif>
			<cfif structKeyExists(form,"frmSubmit")>and entityTable = <cf_queryparam value="#entityType#" cfsqltype="cf_sql_varchar"> </cfif>

	) base where 1=1
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		<cfif structKeyExists(form,"frmSubmit")>
			and (cast(entityID as varchar) =  <cf_queryparam value="#trim(frmEntityID)#" CFSQLTYPE="CF_SQL_VARCHAR" >  or salesForceID =  <cf_queryparam value="#trim(frmEntityID)#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and entity =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- START 2014-04-02 PPB Case 439295 added maxRows --->
<cfif getSalesForceSuccesses.recordCount eq maxRows>
	<p>This reports only shows the top <cfoutput>#maxRows#</cfoutput> successful records (if the sortorder of the report has not been changed these are the most recently synched records). To find a specific record, enter the Relayware or SalesForce ID in the search below.</p>
</cfif>
<!--- END 2014-04-02 PPB Case 439295 --->

<!--- default the search comparitor to an equal sign --->
<cfif not structKeyExists(form,"frmSearchComparitor") or form.frmSearchComparitor eq "">
	<cfset form.frmSearchComparitor = 2>
</cfif>

<cfset showTheseColumns = "Name,RelaywareID,SalesForceID,Synch_Date">
<cfset keyColumnURLList = " ">
<cfset keyColumnOnClickList = "viewEntityDetails('##Entity##'*comma##relaywareID##);return false;">
<cfset FilterSelectFieldList = "">
<cfset keyColumnList = "Name">

<cfif listFindNoCase(getSalesForceSuccesses.columnList,"location")>
	<cfset showTheseColumns = listInsertAt(showTheseColumns,2,"Location")>
</cfif>
<cfif listFindNoCase(getSalesForceSuccesses.columnList,"organisation")>
	<cfset showTheseColumns = listInsertAt(showTheseColumns,3,"Organisation")>
</cfif>

<cfquery name="getPolSFObjects">
	select object from ##sfEntityMapping sm where sm.entityName in ('organisation','location','person')
</cfquery>

<cfset polEntityTypes = listAppend("organisation,location,person",valueList(getPolSFObjects.object))>

<cfquery name="getProductSFObject">
	select object from ##sfEntityMapping sm where sm.entityName ='product'
</cfquery>

<cfquery name="getPricebookSFObject">
	select object from ##sfEntityMapping sm where sm.entityName ='pricebook'
</cfquery>

<!--- 	<cfif not listFindNoCase(polEntityTypes,entityType)>
		<cfset keyColumnURLList = " ">
		<cfif entityType eq "opportunity">
			<cfset keyColumnOnClickList="javascript:openNewTab('Opportunity Edit ##RelaywareID##'*comma'##Name##'*comma'/leadManager/opportunityEdit.cfm?opportunityid=##RelaywareID##'*comma{iconClass:'leadmanager'});return false;">
		<cfelseif listFindNoCase("product,#getProductSFObject.object[1]#",entityType)>
			<cfset keyColumnOnClickList="javascript:openNewTab('Product Edit ##RelaywareID##'*comma'##Name##'*comma'/products/products.cfm?editor=yes&hideBackButton=true&productID=##RelaywareID##'*comma{iconClass:'products'});return false;">
		<cfelseif listFindNoCase("pricebook,#getPricebookSFObject.object[1]#",entityType)>
			<cfset keyColumnOnClickList="javascript:openNewTab('Pricebook Edit ##RelaywareID##'*comma'##Name##'*comma'##application.com.security.encryptURL('/products/pricebookEdit.cfm?editor=yes&hideBackButton=true&pricebookID='&RelaywareID)##'*comma{iconClass:'products'});return false;">
		<cfelse>
			<cfset keyColumnOnClickList = "">
			<cfset keyColumnList = "">
		</cfif>
	</cfif> --->

<cfif direction eq "">
	<cfset showTheseColumns = listPrepend(showTheseColumns,"Direction")>
	<cfset showTheseColumns = listPrepend(showTheseColumns,"Entity")>
	<cfset FilterSelectFieldList = "Entity,Direction">
</cfif>

<cfset checkBoxFilterName = "">
<cfif fromDate neq "" or toDate neq "">
	<cfset checkBoxFilterName = "frmShowAll">
</cfif>

<cf_tableFromQueryObject
	queryObject="#getSalesForceSuccesses#"
	queryName="getSalesForceSuccesses"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList=#keyColumnURLList#
	keyColumnOnClickList=#keyColumnOnClickList#
	keyColumnKeyList="relaywareID"
	keyColumnOpenInWindowList="no"
 	hideTheseColumns="EntityName"
	showTheseColumns="#showTheseColumns#"
	columnTranslation="false"
	searchColumnList="RelaywareID,SalesForceID,Name"
	allowColumnSorting="yes"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="Remove Date Filter"
	FilterSelectFieldList="#FilterSelectFieldList#"
	FilterSelectFieldList2="#FilterSelectFieldList#"
>