<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		synchSuspendReport.cfm	
Author:			NJH  
Date started:	23-10-2009
	
Description:	Show synchs that have been suspended.. Currently only for opportunities		

Amendment History:

Date 		Initials 	What was changed
2011/01/31	PPB			LID5510 - references to application.flag changed to use application.com.flag.getFlagStructure()
2012/07/27	IH			Case 429177 limit number of records returned to 5000, add filter and search capability
2014-04-02 	PPB 		Case 439295 removed filters on EntityID,Name,SalesForceID

Possible enhancements:


 --->

<cfsetting requesttimeout="300">

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Suspended_Date">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="frmEntity" default="">
<cfparam name="frmSearchString" default="">

<cfif not listFindNoCase("person,location,organisation,opportunity",frmEntity)>
	<cfset frmEntity="">
</cfif>

<cfset frmSearchString=trim(frmSearchString)>

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfset application.com.request.setTopHead(pageTitle = "Synching Suspended")>

<!--- this sets an entity to resume synching --->
<cfif structKeyExists(form,"frmRowIdentity") and listLen(form.frmRowIdentity,",") gt 0>
	<!--- Case 432119 - change url to form --->
	<cfloop list="#form.frmRowIdentity#" delimiters="," index="entity">
		<cfset entityType = listFirst(entity,"|")>
	 	<cfif listFindNoCase("organisation,location,person,opportunity",entityType)>
		 	<cfset application.com.salesForce.flagEntityAsSynching(resetSynchAttempt=true,entityID=listLast(entity,"|"),entityType=entityType)>
		 </cfif>
	</cfloop>
</cfif>
			
<cfquery name="getSalesForceSuspends" datasource="#application.siteDataSource#">
	select distinct entity+'|'+cast(entityID as varchar) as rowIdentity, * 
	into ##tmpSalesForceSuspends
	from (			
		select p.personID as entityID, p.firstname+' '+p.lastname as name, personID,'Person' as entity, crmPerID as salesForceID,perSuspend.created as suspended_Date
			from person p with (noLock) inner join booleanFlagData perSuspend on p.personID = perSuspend.entityID and perSuspend.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID="perSalesForceSynchSuspended").flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		union
		select l.locationID as entityID, l.sitename as name, locationID,'Location' as entity, crmLocID as salesForceID,locSuspend.created as suspended_Date
			from location l with (noLock) inner join booleanFlagData locSuspend on l.locationID = locSuspend.entityID and locSuspend.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID="locSalesForceSynchSuspended").flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		union
		select o.organisationID as entityId, o.organisationName as name, organisationID,'Organisation' as entity, crmOrgID as salesForceID,orgSuspend.created as suspended_Date
			from organisation o inner join booleanFlagData orgSuspend on o.organisationID = orgSuspend.entityID and orgSuspend.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID="orgSalesForceSynchSuspended").flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		union
		select opp.opportunityID as entityId, opp.detail as name, opportunityID,'Opportunity' as entity, crmOppID as salesForceID,oppSuspend.created as suspended_Date
			from opportunity opp inner join booleanFlagData oppSuspend on opp.opportunityID = oppSuspend.entityID and oppSuspend.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure(flagID="oppSalesForceSynchSuspended").flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	) base where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	
	select top 5000 *, (select count(*) from ##tmpSalesForceSuspends) as suspendCount 
	from ##tmpSalesForceSuspends where 1=1
	<cfif frmEntity neq "">and entity = <cf_queryparam value="#frmEntity#" cfsqltype="CF_SQL_VARCHAR"></cfif>
	<cfif frmSearchString neq "">
		and (salesForceID = <cf_queryparam value="#frmSearchString#" cfsqltype="CF_SQL_VARCHAR">
		<cfif isNumeric(frmSearchString)>or entityID = <cf_queryparam value="#frmSearchString#" cfsqltype="CF_SQL_INTEGER"></cfif>
		or name like <cf_queryparam value="%#frmSearchString#%" cfsqltype="CF_SQL_VARCHAR">)		
	</cfif>
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cfquery datasource="#application.siteDataSource#">
	IF object_id('tempdb..##tmpSalesForceSuspends') IS NOT NULL
		DROP TABLE ##tmpSalesForceSuspends
</cfquery>	

 <cf_querySim>
	getObjects
	entity
	Person
	Location
	Organisation
	Opportunity
</cf_querySim>

<cfform name="filterForm" method="post" >
	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="html" currentValue="" label="" fieldname="">
			Entity <cf_relayFormElement relayFormElementType="select" fieldname="frmEntity" currentValue="#frmEntity#" query="#getObjects#" display="entity" value="entity" nullText="Select an entity">
			EntityID, SalesForceID or Name<cf_relayFormElement relayFormElementType="text" fieldname="frmSearchString" currentValue="" label="frmSearchString" size="20">
			<cf_relayFormElement relayFormElementType="submit" fieldname="frmSubmit" currentValue="Search" label="">
			<cf_relayFormElement relayFormElementType="submit" fieldname="frmClearResults" currentValue="Clear" label="">
		</cf_relayFormElementDisplay>
	</cf_relayFormDisplay>
</cfform>
	
<p>There are <cfoutput>#htmleditformat(getSalesForceSuspends.suspendCount)#</cfoutput> suspended records in the database. By default, this reports only shows the latest 5000 suspended records. To find a specific record, enter the Relayware or SalesForce ID in the search above.</p>
	
<cfset showResumeSynch = true>
<cfinclude template="salesForceReportFunctions.cfm">

<cf_tableFromQueryObject 
	queryObject="#getSalesForceSuspends#"
	queryName="getSalesForceSuspends"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"

	showTheseColumns="Entity,EntityID,Name,SalesForceID,Suspended_Date"
	hideTheseColumns="salesForceErrorID"
	columnTranslation="false"

	allowColumnSorting="yes"
	rowIdentityColumnName="rowIdentity"
	functionListQuery="#comTableFunction.qFunctionList#"
	
	FilterSelectFieldList="Entity,Suspended_Date"									<!--- 2014-04-02 PPB Case 439295 removed filters on EntityID,Name,SalesForceID --->
	FilterSelectFieldList2="Entity,Suspended_Date"									<!--- 2014-04-02 PPB Case 439295 removed filters on EntityID,Name,SalesForceID --->
>			