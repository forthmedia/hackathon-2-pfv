<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		synchPendingReport.cfm
Author:			NJH
Date started:	24-05-2011

Description:	A report to view synch pending changes

Amendment History:

Date		Initials 	What was changed
2014-04-16	PPB			Case 439295 avoid cartesian-type join

Possible enhancements:


 --->

<cfsetting requesttimeout="900">									<!--- 2014-04-16 PPB Case 439295 upped the time-out while the hood was up --->

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="prioritise desc,modDate">
<cfparam name="numRowsPerPage" default="100">

<cfparam name="fromDate" default="">
<cfparam name="toDate" default="">
<cfparam name="entityType" default="">
<cfparam name="synchingStatus" default="">
<cfparam name="direction" default="">

<cf_includeJavascriptOnce template="/javascript/viewEntity.js">
<cf_includeJavascriptOnce template="/salesforce/js/salesforce.js">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfif structKeyExists(form,"frmShowAll")>
	<cfset fromDate = "">
	<cfset toDate = "">
</cfif>

<cfset pageTitle = "Connector Pending Report">
<cfif entityType neq "">
	<cfset pageTitle = pageTitle & " for #entityType# Records">
</cfif>
<cfif direction eq "export">
	<cfset pageTitle = pageTitle&", Out to Salesforce.com">
<cfelseif direction eq "import">
	<cfset pageTitle = pageTitle&", In from Salesforce.com">
</cfif>
<cfif fromDate neq "">
	<cfset pageTitle = pageTitle&", from #dateFormat(fromDate,"full")#">
</cfif>
<cfif toDate neq "">
	<cfset pageTitle = pageTitle&", to #dateFormat(toDate,"full")#">
	<cfset toDatePlusOneDay = dateAdd("d",1,toDate)>
</cfif>
<cfset application.com.request.setTopHead(pageTitle = pageTitle)>

<cfset objectEntityStruct = application.com.salesForce.putMappedObjectsInTable(entityType=entityType)>

<!--- this sets an entity to resume synching --->
<cfif structKeyExists(url,"frmRowIdentity") and listLen(url.frmRowIdentity,",") gt 0>
 	<cfloop list="#url.frmRowIdentity#" delimiters="," index="ID">
		 <cfset application.com.salesForce.prioritisePendingRecord(recordID=ID)>
	</cfloop>
</cfif>

<!--- this sets an entity to resume synching --->
<!--- PJP 10/12/2012 CASE: 432119: Added in check yo use form variables if they exist--->
<cfif structKeyExists(form,"frmRowIdentity") and listLen(form.frmRowIdentity,",") gt 0>
 	<cfloop list="#form.frmRowIdentity#" delimiters="," index="ID">
		 <cfset application.com.salesForce.prioritisePendingRecord(recordID=ID)>
	</cfloop>
</cfif>

<cfset synchingFlags = "0">
<cfset flagTextPrefix = "">

<cfloop list="#objectEntityStruct.entityList#" index="entity">
	<cfif listFindNoCase("organisation,location,person,opportunity",entity)>
		<cfset flagTextPrefix = left(entity,3)>
	</cfif>

	<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").isOK>
		<cfset synchingFlags = listAppend(synchingFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").flagID)>
	</cfif>
</cfloop>

<cfquery name="getSynchPendingChanges" datasource="#application.siteDataSource#">
	select *
	from (
			select distinct ID,fieldName,newValue,modDate,sp.opportunityID,Prioritise,
					sp.entityID as relaywareID,
					sp.objectID as SalesForceID,
					sp.object,
					case when sp.direction = 'I' then 'Import' else 'Export' end as direction,
					sm.entityName as entity,
					case <cfif direction neq "export">when sfeImport.name is not null then sfeImport.name</cfif> when v.name is not null then v.name else v2.name end as name,
					case when v.entityID is not null then v.entityid else v2.entityid end as relaywareIDforFunctionArgs
				from
					salesForceSynchPending sp with (noLock)
						inner join ##sfEntityMapping sm on sp.object in (sm.entityName, sm.object)
						left join vEntityName v on v.entityTypeID = sm.entityTypeID and sp.entityID = v.entityID
						left join vEntityName v2 on isnull(sp.objectid,'') <> '' and v.entityTypeID = sm.entityTypeID and sp.objectID = v2.crmID		/* 2014-04-16 PPB/WAB Case 439295 added the isnull test to avoid cartesian-type join  */
						<cfif direction eq "export">left join SalesForceError sfeExport with (noLock) on sfeExport.entityID = sp.entityID and sfeExport.direction = sp.direction and sp.direction='E'</cfif>
						<cfif direction neq "export">left join SalesForceError sfeImport with (noLock) on sfeImport.objectID = sp.objectID and sfeImport.direction = sp.direction and sp.direction='I' and sfeImport.synchAttempt >= <cf_queryparam value="#application.com.settings.getSetting("salesForce.maxSynchAttempts")#" cfsqltype="cf_sql_integer"></cfif>
						<cfif fromDate neq "" or toDate neq "">
						left join vflagdef f on f.entityTable = sm.entityName and f.flagid in (#synchingFlags#)
						left join booleanflagdata bfd with (noLock) on f.flagid = bfd.flagid and bfd.entityID = <cfif direction eq "export">v<cfelse>v2</cfif>.entityID
						</cfif>
				where 1 =1
					<cfif direction eq "export">and sfeExport.errorID is null<cfelseif direction eq "import">and sfeImport.errorID is null</cfif>
					<cfif isDate(fromDate)>and sp.lastUpdated >= <cf_queryparam value="#fromDate#" cfsqltype="cf_sql_timeStamp"> <cfif direction eq "export">and bfd.lastUpdated >= <cf_queryparam value="#fromDate#" cfsqltype="cf_sql_timeStamp"></cfif></cfif>
					<cfif isDate(toDate)>and  sp.lastUpdated <= <cf_queryparam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp"> <cfif direction eq "export">and bfd.lastUpdated <= <cf_queryparam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp"></cfif></cfif>
					<cfif entityType neq "">and sp.object = <cf_queryparam value="#entityType#" cfsqltype="cf_sql_varchar"> </cfif>
					<cfif direction neq "">and sp.direction = <cf_queryparam value="#left(direction,1)#" cfsqltype="cf_sql_varchar"></cfif>
			 ) base
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!---
<cfquery name="getCountOfRecordsInQueue" datasource="#application.siteDataSource#">
	select count(1) as numRecs from salesForceSynchPending with (noLock)
</cfquery>


<p>There are <cfoutput>#htmleditformat(getCountOfRecordsInQueue.numRecs)#</cfoutput> records in the queue. By default, this reports only shows the next 5000 entities to be synched. To find a specific record, enter the Relayware or SalesForce ID in the search above.</p>
 --->

<cfset showTheseColumns = "Name,RelaywareID,SalesforceID,OpportunityID,ModDate,Prioritise">
<cfset FilterSelectFieldList="">

<cfif application.com.settings.getSetting("salesforce.synchLevel") neq "Record">
	<cfset showTheseColumns = "RelaywareID,SalesforceID,OpportunityID,FieldName,NewValue,ModDate,Prioritise">
</cfif>

<cfif direction eq "">
	<cfset showTheseColumns = listPrepend(showTheseColumns,"Object,Direction")>
	<cfset FilterSelectFieldList = listPrepend(FilterSelectFieldList,"Object,Direction")>
</cfif>

<!--- default the search comparitor to an equal sign --->
<cfif not structKeyExists(form,"frmSearchComparitor") or form.frmSearchComparitor eq "">
	<cfset form.frmSearchComparitor = 2>
</cfif>

<cfset showPrioritisePending = true>
<cfinclude template="salesForceReportFunctions.cfm">

<cfset keyColumnURLList = " ">
<cfset keyColumnOnClickList = "viewEntityDetails('##object##'*comma##relaywareID##);return false;">

<cfset checkBoxFilterName = "">
<cfif fromDate neq "" or toDate neq "">
	<cfset checkBoxFilterName = "frmShowAll">
</cfif>

<cf_tableFromQueryObject
		queryObject="#getSynchPendingChanges#"
		queryName="getSynchPendingChanges"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		hideTheseColumns="lastUpdated"
		showTheseColumns="#showTheseColumns#"
		columnTranslation="false"
		rowIdentityColumnName="ID"
		functionListQuery="#comTableFunction.qFunctionList#"
		booleanFormat="Prioritise"
		searchColumnList="RelaywareID,SalesForceID"
		allowColumnSorting="yes"
		keyColumnList="Name"
		keyColumnOnClickList=#keyColumnOnClickList#
		keyColumnURLList=#keyColumnURLList#
		keyColumnKeyList="relaywareID"
		keyColumnOpenInWindowList="no"
		checkBoxFilterName="#checkBoxFilterName#"
		checkBoxFilterLabel="Remove Date Filter"
		FilterSelectFieldList="#FilterSelectFieldList#"
		FilterSelectFieldList2="#FilterSelectFieldList#"
	>