/* �Relayware. All Rights Reserved 2014 */
function viewEntityDetails(entityType,entityID) {
	if (entityType.toLowerCase() == 'person') {
		viewEntity(0,entityID,111,'',{openInOwnTab:true});
	}
	else if (entityType.toLowerCase() == 'location') {
		viewEntity(1,entityID,91,'',{openInOwnTab:true});	
	}
	else if (entityType.toLowerCase() == 'organisation') {
		viewEntity(2,entityID,71,'',{openInOwnTab:true});	
	}
	else if (entityType.toLowerCase() == 'opportunity') {
		openNewTab('Opportunity Edit '+entityID+'','Opportunity '+entityID+'','/leadManager/opportunityEdit.cfm?opportunityid='+entityID+'',{iconClass:'leadmanager'});
	}
	else if (entityType.toLowerCase() == 'product') {
		openNewTab('Product Edit '+entityID+'','Product '+entityID+'','/products/products.cfm?editor=yes&hideBackButton=true&productID='+entityID+'',{iconClass:'products'});
	}
	else if (entityType.toLowerCase() == 'priceboook') {
		openNewTab('Pricebook Edit '+entityID+'','Pricebook '+entityID+'','/products/pricebookEdit.cfm?editor=yes&hideBackButton=true&pricebookID='+entityID+'',{iconClass:'products'});
	}
}