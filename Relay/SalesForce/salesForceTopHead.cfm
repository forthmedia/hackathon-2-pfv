<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="current" type="string" default="index.cfm">
<cfparam name="templateType" type="string" default="list">
<cfparam name="pageTitle" type="string" default="">
<cfparam name="pagesBack" type="string" default="1">
<cfparam name="thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="salesForce">
	<CFIF findNoCase("errorReport.cfm",SCRIPT_NAME,1) or findNoCase("synchPendingReport.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF>
</CF_RelayNavMenu>