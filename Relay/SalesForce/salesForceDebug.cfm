<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="entityID" default=0>
<cfparam name="object" default="opportunity">
<cfparam name="opportunityID" default=0>
<cfparam name="objectID" default="">
<cfparam name="errorID" default="0">

<cfquery name="getErrors" datasource="#application.siteDataSource#">
	select message,dataStruct,fields,lastUpdated,object,entityID,objectID,direction 
	from salesForceError where 1=1
	<cfif entityID neq 0 or objectID neq "">and object =  <cf_queryparam value="#object#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
	<cfif entityID neq 0>and entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
	<cfif objectID neq "">and objectID =  <cf_queryparam value="#objectID#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
	<cfif opportunityID neq 0 and opportunityID neq "">and opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif> <!--- 2012/01/11	MS	added single quotes for opportunityID --->
	<cfif errorID neq 0>and errorID = <cf_queryparam value="#errorID#" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>
	order by lastUpdated desc
</cfquery>

<cfset application.com.request.setTopHead(pageTitle="Data Details for #getErrors.object[1]# #IIF(getErrors.direction[1] eq 'I',DE(getErrors.objectID[1]),DE(getErrors.entityID[1]))# at the time of synching (#getErrors.lastUpdated[1]#)")>

<table class="withBorder">
	<cfloop query="getErrors">
		<cfset detailStruct = {}>
		
		<cfoutput>
			<tr>
				<th colspan="2">&nbsp;</th>
			</tr>
			<tr>
				<td><b>Message:</b></td><td>#application.com.security.sanitiseHTML(message)#</td>
			</tr>
			<cfif fields neq "">
				<td><b>Field:</b></td><td>#htmlEditformat(fields)#</td>
			</cfif>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<cfif not isJson(dataStruct)>
				<cfset errorDataStruct = dataStruct>
			<cfelse>
				<cfset errorDataStruct = DeserializeJSON(dataStruct,false)>
			</cfif>
			
			<cfif isStruct(errorDataStruct)>
				<cfset detailStruct = errorDataStruct>
				
				<cfif structKeyExists(errorDataStruct,"arguments")>
					<cfif structKeyExists(errorDataStruct.arguments,"objects") and isArray(errorDataStruct.arguments.objects) and arrayLen(errorDataStruct.arguments.objects) eq 1>
						<cfset detailStruct = errorDataStruct.arguments.objects[1]>
					</cfif>
				<cfelseif structKeyExists(errorDataStruct,"entityDetails")>
					<cfset detailStruct = errorDataStruct.entityDetails>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">Relayware Details</th>
					</tr>
				<cfelseif structKeyExists(errorDataStruct,"objectDetails")>
					<cfset detailStruct = errorDataStruct.objectDetails>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">SalesForce Details</th>
					</tr>
				</cfif>
			
				<cfif structCount(detailStruct)>
				<tr>
					<th>Field</th>
					<th>Value</th>
				</tr>
				</cfif>
				<cfloop collection="#detailStruct#" item="field">
					<tr><td>#htmlEditformat(field)#</td><td>#htmlEditformat(detailStruct[field])#</td></tr>
				</cfloop>
				
				<cfif structKeyExists(errorDataStruct,"sendSoap")>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2">Soap Packet sent to SalesForce</th>
					</tr>
					<tr>
						<td colspan="2">#htmlEditFormat(errorDataStruct.sendSoap)#</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</cfif>
			</cfif>
		</cfoutput>
	</cfloop>
</table>
