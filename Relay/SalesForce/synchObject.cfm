<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		synchObject.cfm	
Author:			NJH  
Date started:	24-05-2011
	
Description:	A file to test an import/export of an object/entity		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="ID" default=0>
<cfparam name="object" type="string" default="">
<cfparam name="entityType" type="string" default="">

<cfif (object eq "" and entityType eq "") or ID eq 0 or not structKeyExists(url,"modifiedDate")>
	object or entityType must be passed in, an ID as well as the modifiedDate
	
<cfelse>

	<cftry>
		<cfset application.com.salesForce.setSalesForceUserAsCurrentUser()>
		
		<!--- importing an object --->
		<cfif object neq "">
			<cfset application.com.salesForce.logDebugInformation(entityType=object)>
			<cfset application.com.salesForce.logDebugInformation(entityID=ID)>
			
			<cfset importResult = application.com.salesForce.import(object=object,objectID=ID)>
			<cfdump var="#importResult#">
		
		<!--- exporting an entity --->	
		<cfelse>
		
			<cfset application.com.salesForce.logDebugInformation(entityType=entityType)>
			<cfset application.com.salesForce.logDebugInformation(entityID=ID)>
			
			<cfset exportResult = application.com.salesForce.export(entityType=entityType,entityID=ID,modifiedSinceDate=url.modifiedDate)>
			<cfdump var="#exportResult#">
		</cfif>
		
		<cfcatch>
			<cfif structKeyExists(request,"salesForceDebug") and structKeyExists(url,"debug")>
				<cfdump var="#request.salesForceDebug#">
				<cfdump var="#cfcatch#">
			<cfelse>
				<cfrethrow>
			</cfif>
		</cfcatch>
	</cftry>

</cfif>
