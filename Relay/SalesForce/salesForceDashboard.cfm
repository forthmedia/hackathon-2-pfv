<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		salesForceDashboard.cfm	
Author:			NJH  
	
Description:	Dashboard for summary of reports

Amendment History:

Date		Initials 	What was changed
2014-04-24	PPB/GCC		Case 439295 improve performance

Possible enhancements:

 --->

<!--- 
Status:Description

Success: flagged with either an import/export synched flag
Pending: has a record in pending and has the 'Synching' flag set
Failure: has a record in the error and is flagged as synching or suspended, or has a failed related object.
 --->

<cfset objectEntityStruct = application.com.salesForce.putMappedObjectsInTable()>

<cfset showLast24HoursFrom=false>
<cfset showLast24HoursTo=false>

<cfif not StructKeyExists(form,"frmFromDate") or form.frmFromDate eq "">
	<cfset form.frmFromDate = dateAdd("h",-24,request.requestTime)>
	<cfset showLast24HoursFrom=true>
</cfif>
<cfif not StructKeyExists(form,"frmToDate") or form.frmToDate eq "">
	<cfset form.frmToDate = request.requestTime>
	<cfset showLast24HoursTo=true>
</cfif>

<cfset toDatePlusOneDay = dateAdd("d",1,form.frmToDate)>

<cfset showLast24Hours = showLast24HoursFrom and showLast24HoursTo>

<cfset synchedImportFlags = "">
<cfset synchedExportFlags = "">
<cfset suspendedFlags = "">
<cfset synchingFlags = "">

<cfset statusList="Synched_export,Synched_import,SynchSuspended,Synching">
<cfloop list="#objectEntityStruct.entityList#" index="entityType">
	<cfset flagTextPrefix = entityType>
	<cfif listFindNoCase("organisation,location,person,opportunity",entityType)>
		<cfset flagTextPrefix = left(entityType,3)>
	</cfif>
	
	<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_import").isOK>
		<cfset synchedImportFlags = listAppend(synchedImportFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_import").flagID)>
	</cfif>
	<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_export").isOK>
		<cfset synchedExportFlags = listAppend(synchedExportFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynched_export").flagID)>
	</cfif>
	<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynchSuspended").isOK>
		<cfset suspendedFlags = listAppend(suspendedFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynchSuspended").flagID)>
	</cfif>
	<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").isOK>
		<cfset synchingFlags = listAppend(synchingFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").flagID)>
	</cfif>
</cfloop>

<cfquery name="getExportData" datasource="#application.siteDataSource#">
	select 
         entityName as object,
         count (distinct case when flagid  in ( <cf_queryparam value="#synchedExportFlags#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then entityid else null end) as Success,
         count (distinct case when pendingEntity is not null and failedEntity is null and flagId  not in ( <cf_queryparam value="#suspendedFlags#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then pendingEntity else null end) as Pending,
   		 count (distinct case when failedEntity is not null and flagid in (#suspendedFlags#,#synchingFlags#) then failedEntity else null end) as Failure
   from 
	(select entityTable as entityName, f.flagid, bfd.entityid, sp.entityID as pendingEntity, se.entityID as failedEntity
		from vflagdef f 
			left join booleanflagdata bfd on f.flagid = bfd.flagid and bfd.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
			left join salesForceSynchPending sp on sp.object = f.entityTable and bfd.entityID = sp.entityID and sp.direction='E' and sp.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
			left join salesForceError se on se.object = f.entityTable and se.entityID = bfd.entityID and se.direction = 'E' and se.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
		where f.flagid in (#synchedExportFlags#,#suspendedFlags#,#synchingFlags#) 
               union 
                select entityName,null, null, null, null from schemaTable where entityName in (<cf_queryparam value="#objectEntityStruct.entityList#" cfsqltype="cf_sql_varchar" list="true">)) as entities
   group by entityName
</cfquery>


<cfquery name="getImportData" datasource="#application.siteDataSource#">
	select 
		object,
         count (distinct case when flagid  in ( <cf_queryparam value="#synchedImportFlags#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then entityid else null end) as Success,
         count (distinct case when pendingObject is not null and failedObject is null and flagID  not in ( <cf_queryparam value="#suspendedFlags#" CFSQLTYPE="cf_sql_integer"  list="true"> ) then pendingObject else null end) as Pending,
   		 count (distinct case when failedObject != '0' and flagid in (#suspendedFlags#,#synchingFlags#) then failedObject else null end) as Failure
   from 
	(
		select sm.object as object, f.flagid, bfd.entityid, se.objectID as failedObject, null as pendingObject
		from 
			##sfEntityMapping sm
			left join salesForceError se on se.object = sm.object and se.direction = 'I' and se.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
			left join vEntityName v on isnull(se.objectid,'') <> '' and v.entityTypeID = sm.entityTypeID and se.objectID = v.crmID					<!--- 2014-04-16 PPB/WAB Case 439295 added the isnull test to avoid cartesian-type join --->
			left join vflagdef f on sm.entityName = f.entityTable and f.flagid in (#suspendedFlags#)
			left join booleanflagdata bfd on f.flagid = bfd.flagid and bfd.entityId = v.entityId and bfd.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
		union	
		select sm.object as object, f.flagid, bfd.entityid, null as failedObject, sp.objectID as pendingObject
			from 
				##sfEntityMapping sm
				left join salesForceSynchPending sp on sp.object = sm.object and sp.direction='I' and sp.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
				left join vEntityName v on v.entityTypeID = sm.entityTypeID and v.crmID = sp.objectId
				left join vflagdef f on sm.entityName = f.entityTable and f.flagid in (#synchingFlags#)
				left join booleanflagdata bfd on f.flagid = bfd.flagid and bfd.entityId = v.entityId and bfd.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp">
		union
		<!--- START 2014-04-24 PPB/GCC Case 439295 improve performance ---> 
		select sm.object as object, f.flagid, bfd.entityid, null as failedObject, null as pendingObject
		from 
			booleanflagdata bfd 
			inner join vflagdef f on bfd.FlagID = f.flagid and f.flagid in (#synchedImportFlags#) 
			inner join ##sfEntityMapping sm on sm.entityName = f.entityTable
			where bfd.lastUpdated between <cf_queryParam value="#form.frmFromDate#" cfsqltype="cf_sql_timeStamp"> and <cf_queryParam value="#toDatePlusOneDay#" cfsqltype="cf_sql_timeStamp"> 
		<!--- END 2014-04-24 PPB/GCC Case 439295 ---> 
		union 
        select object,null, null, null, null from ##sfEntityMapping
			where entityName in (<cf_queryparam value="#objectEntityStruct.entityList#" cfsqltype="cf_sql_varchar" list="true">)) as entities
   group by object
</cfquery>

<cfquery name="createTempTable" datasource="#application.siteDataSource#">
	if object_id('tempdb..##sfEntityMapping') is not null
	begin
	   drop table ##sfEntityMapping
	end
</cfquery>

<style>
	.statusReport .withBorder {width:300px;}
	.direction {float:left;width:30px;}
	.infoblock, .successblock, .warningblock, .errorblock {padding:3px;}
	.dashboardCell {width:50%;vertical-align:top;padding-bottom:30px;padding-right:20px;padding-left:20px;}
	.statusReport .withBorder td {padding:5px;}
	#dashboard {width:100%;}
	#<cfif showLast24Hours>hours a<cfelse>fromTo</cfif> {font-weight:bold;color:#CC0000;padding-left:3px;padding-right:3px;}
</style>

<table id="dashboard">
	<tr>
		<td colspan="2" style="width:100%;padding:20px;">
			<form name="dateForm" id="dateForm" method="post">
				<span id="fromTo">From</span> <CF_relayDateField currentValue="#form.frmFromDate#" fieldName="frmFromDate" anchorName="createFrom" helpText="" disableToDate=#request.requestTime# enableRange="true">
				<span id="fromTo">To</span> <CF_relayDateField currentValue="#form.frmToDate#" fieldName="frmToDate" anchorName="createTo" helpText="" disableToDate=#request.requestTime# enableRange="true">
				<cf_input type="submit" value="Go" name="frmSubmit">
				or <span id="hours"><a href="">Last 24 hours</a></span>
			</form>
		</td>
	</tr>
	<tr>
		<td class="dashboardCell">
			<div class="direction"><h1>phr_salesforce_Export</h1></div>
			<div class="statusReport">
			<cf_tableFromQueryObject 
				queryObject="#getExportData#"
				queryName="getExportData"
				showTheseColumns="Object,Success,Pending,Failure"
				hideTheseColumns=""
				columnTranslation="false"
				allowColumnSorting="no"
				HidePageControls="true"
				useInclude="false"
				keyColumnList="Success,Pending,Failure"
				keyColumnKeyList=" , , "
				keyColumnURLList=" , , "
				keyColumnOnClickList="javascript:openNewTab('##Object##SuccessExport'*comma'SF Success: ##Object## Export'*comma'/salesforce/synchSuccessReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=export'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;,javascript:openNewTab('##Object##PendingExport'*comma'SF Pending: ##Object## Export'*comma'/salesforce/synchPendingReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&synchingStatus=Synching&direction=export'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;,javascript:openNewTab('##Object##FailureExport'*comma'SF Failures: ##object## Export'*comma'/salesforce/errorReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=export'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;"
			>
			</div>
		</td>
		<td class="dashboardCell">
			<cf_include template="/salesforce/synchLastSuccessfulRunReport.cfm">
		</td>
	</tr>
	<tr>
		<td class="dashboardCell">
			<div class="direction"><h1>phr_salesforce_Import</h1></div>
			<div class="statusReport">
			<cf_tableFromQueryObject 
				queryObject="#getImportData#"
				queryName="getImportData"
				showTheseColumns="Object,Success,Pending,Failure"
				hideTheseColumns=""
				columnTranslation="false"
				allowColumnSorting="no"
				HidePageControls="true"
				useInclude="false"
				keyColumnList="Success,Pending,Failure"
				keyColumnKeyList=" , , "
				keyColumnURLList=" , , "
				keyColumnOnClickList="javascript:openNewTab('##Object##SuccessImport'*comma'SF Success: ##Object## Import'*comma'/salesforce/synchSuccessReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=import'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;,javascript:openNewTab('##Object##PendingImport'*comma'SF Pending: ##object## Import'*comma'/salesforce/synchPendingReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&synchingStatus=Synching&direction=import'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;,javascript:openNewTab('##Object##FailureImport'*comma'SF Failures: ##object## Import'*comma'/salesforce/errorReport.cfm?entityType=##Object##&fromDate=#jsStringFormat(frmFromDate)#&toDate=#jsStringFormat(frmToDate)#&direction=import'*comma{reuseTab:true*commaiconClass:'salesforce'});return false;"
			>
			</div>
		</td>
		<td class="dashboardCell">
			<table class="withBorder">
				<tr>
					<th>Summary Report</th>
				</tr>
				<tr>
					<td>
						<cfquery name="getImportSummmaryData" dbtype="query">
							select sum(success) as success,sum(pending) as pending ,sum(failure) as failure from getImportData
						</cfquery>
						
						<cfquery name="getExportSummaryData" dbtype="query">
							select sum(success) as success,sum(pending) as pending ,sum(failure) as failure from getExportData
						</cfquery>
						
						<cf_translate phrases="phr_salesforce_Export,phr_salesforce_Import"/>
						
						<cfchart format="png">
							<cfchartseries type="bar" seriesLabel="Total Successful" seriesColor="##55B74E">
								<cfchartdata item="#phr_salesforce_Export#" value="#getExportSummaryData.success#">
								<cfchartdata item="#phr_salesforce_Import#" value="#getImportSummmaryData.success#">
							</cfchartseries>
							<cfchartseries type="bar" seriesLabel="Total Pending" seriesColor="##EFDF2D">
								<cfchartdata item="#phr_salesforce_Export#" value="#getExportSummaryData.pending#">
								<cfchartdata item="#phr_salesforce_Import#" value="#getImportSummmaryData.pending#">
							</cfchartseries>
							<cfchartseries type="bar" seriesLabel="Total Failures" seriesColor="##D8000C">
								<cfchartdata item="#phr_salesforce_Export#" value="#getExportSummaryData.failure#">
								<cfchartdata item="#phr_salesforce_Import#" value="#getImportSummmaryData.failure#">
							</cfchartseries>
						</cfchart>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<cfset application.com.request.setTopHead(pageTitle="Salesforce Connector Dashboard")>