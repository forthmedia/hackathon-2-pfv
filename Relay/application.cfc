<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

application.cfc extends applicationMain.cfc

This is done because some of our code needs to use the onRequest method, and some (webservices) can't run with it.
This allows variables in ini files specified in the security structure to be put into local scope
The onrequest method cannot be used with webservice cfc, so they have to have an application.cfc which extends applicationMain.cfc directly

 --->
<cfcomponent EXTENDS="applicationMain" output="false">

		 <cffunction name="onRequest" returnType="void">
	 		<cfargument name="thePage" type="string" required="true"> 

			<cfinclude template="#thePage#">

		</cffunction>  

</cfcomponent>