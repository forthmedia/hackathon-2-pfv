<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			activeContent.cfm	
Author:				NM
Date started:		2006-04-28
	
Description:		Displays active content using the renderActiveContent method.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->
<!--- REQUIRED PARAMS --->

<cf_param name="acPath" default="" label="Full URL to the movie  (e.g. http://...)" required="true"/>
<cf_param label="Width" name="width" default="" required="true"/>
<cf_param label="Height" name="height" default="" required="true"/>
<cf_param name="bgcolor" default="##FFFFFF" label="Background Colour"/>
<cf_param name="align" default="middle" validvalues="left,middle,right"/>
<cf_param name="quality" default="high" validvalues="low,autolow,autohigh,midium,high,best"/>

<cf_param name="scale" default="default" validvalues="default,noborder,exactfit,noscale"/>


<!--- milliseconds for random naming ---> 
<cfset currentMS = getTickCount()>

<!--- DEFAULTS --->
<cfparam name="classid" default="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
<cfparam name="codebase" default="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0">
<cfparam name="id" default="ActiveContent_#currentMS#">
<cfparam name="name" default="ActiveContent_#currentMS#">
<cfparam name="allowscriptaccess" default="sameDomain">
<cfparam name="pluginspage" default="http://www.macromedia.com/go/getflashplayer">
<cfparam name="type" default="application/x-shockwave-flash">
<cfparam name="wmode" default="opaque">
<cfparam name="flashVars" default="">


<cfif acPath eq "" OR width eq "" OR height eq "">
	Error: A Path, Width and Height must be specified. One or more of these currently isn't.
<cfelse>
	
	<cfset params = "classid=#classid##application.delim1#
		codebase=#codebase##application.delim1#
		width=#width##application.delim1#
		height=#height##application.delim1#
		id=#id##application.delim1#
		align=#align##application.delim1#
		quality=#quality##application.delim1#
		bgcolor=#bgcolor##application.delim1#
		name=#name##application.delim1#
		allowscriptaccess=#allowscriptaccess##application.delim1#
		pluginspage=#pluginspage##application.delim1#
		type=#type##application.delim1#
		scale=#scale##application.delim1#
		wmode=#wmode#">
	<cfif flashVars neq "">
		<cfset params = '#params##application.delim1#flashVars="#flashVars#"'>
	</cfif>


	<cfset contentDrawn = application.com.RelayActiveContent.renderActiveContent(acPath,params)>
	
	<cfif isDefined("contentDrawn") and not contentDrawn>
		There was a problem rendering your active content.
	</cfif>
</cfif>