<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		ElementBranchList.cfm	
Author:			GCC
Date started:		2006/01/04
	
Description: This provides navigation to elements x generations below the point you are at in the tree.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

30-Jan-2006			GCC			CR_SNY580 Extended to allow defaultBranchListColumns to be defined and styled


Enhancements still to do:
	Working but needs more genercising when the time permits or other use cases evolve
	Issues arround Sony using this file with the same layout needed for different generations within a tree
	i.e. Show me, children and grandchildren 
	On the child show me and children but lay out the children as the grandchildren in the example above.
	We almost need to work from the bottom up rather than from the top down...

 --->




<!--- work out where to start the navigation from - basically from item which is on the left nav 
		this always seems to be generation 3 - but may need to be parameterised
--->

<!--- <cfset branchTop = application.com.relayElementTree.getElementOfParticularGenerationFromCurrentBranch(
				elementTree = request.currentElementTree, 
				currentelementid = elementid,	
				generation = 3)> --->

<cfparam name="numberofgenerations" default="1">
<cfparam name="numberofgenerationsofchildren" default="1">
<cfparam name="groupGeneration" default="2">
<cfparam name="groupGenerationClickable" default="false">
<cfparam name="branchTop" default="#elementid#">
<cfparam name="showlinkImage" default="true">
<cfparam name="showHeaders" default="true">
<cfparam name="headingGenerationList" default="1,2">
<cfparam name="showCurrentElementChildren" default="true">
<cfparam name="defaultToCurrentElementImage" default="true">
<cfparam name="defaultBranchListColumns" default="3"><!--- either 1,2 or 3 --->

<cfif not(isnumeric(branchTop))>
	<cfswitch expression="#branchTop#">
		<cfcase value="parent">
			<cfscript>
			numberofgenerationsofchildren=1;
			showCurrentElementChildren=false;
			branchTop = application.com.elementtreenav.getNodeParent(elementid);
			theElementid = elementID;
			</cfscript>
		</cfcase>
	</cfswitch>
<cfelse>
	<cfset theElementid = branchTop>
</cfif>


					
<!--- set topOfBtranchID to the page we are on - not calculated --->
	<!--- now get the branch from this point to the current element and its children --->
	<cfset navigationbranch = application.com.relayElementTree.convertTreeToMenuBranch(
				elementTree = request.currentElementTree, 
				topOfBranchID = #branchTop#,
				currentelementid = #theElementid#,
				showCurrentElementChildren = #showCurrentElementChildren# , 
				includeTopElement = true,
				numberofgenerations = #numberofgenerations#,
				numberofgenerationsofchildren = #numberofgenerationsofchildren#)>

	<cfoutput>
		
		<!--- output, this could be done as a function --->						
		<div id="elementBranchListNav">
			<cfset currentgeneration = 1><!---  style="float:left;display:block;position:absolute;left:200px;top:400px;padding:1px;" --->
			<cfset flipflop = 1>
			<div id="elementBranchListNav_gen1">
			<cfif showHeaders>
				<cfscript>
					parentID = application.com.elementtreenav.getNodeParent(elementid);
				</cfscript>
				<div id="elementBranchListHeader">phr_headline_element_#htmleditformat(parentID)#</div>			
			</cfif>		
			<cfloop query = "navigationbranch">
				<!--- switch colours after every 2nd generation --->
				<cfif groupGeneration eq generation>
					<CFIF flipflop MOD 2 IS NOT 0> 
						<cfset theClass="oddRow">
					<CFELSE> 
						<cfset theClass="evenRow">
					</CFIF>
					<cfset flipflop = flipflop + 1>
				</cfif>	
				<!--- HACK to force the same layout for a generation higher in the tree --->
				<cfif groupGeneration eq 1>
					<cfset localgeneration = generation + 1>
				<cfelse>
					<cfset localgeneration = generation>
				</cfif>
				
				<!--- rules to display image at start of a gen2 and close the gen2 wrapper div once all gen3's are done --->
				<cfif localgeneration is not currentgeneration or localgeneration eq groupGeneration>
					<cfif not(localgeneration eq 3 and currentgeneration eq 2) or (localgeneration eq currentgeneration)>
					</div>	
					</cfif>	
				<cfif localgeneration lt currentgeneration>
					</div>
				</cfif>		
					<div id="elementBranchListNav_gen#localgeneration#" <cfif isdefined('theClass')>class="#theClass#"</cfif>>
				</cfif>
				<cfif showlinkImage>
					<cfif localgeneration eq 2>
						<cfset linkImagePath=application.userfilesabsolutepath & "content\elementLinkImages\linkImageSquare-" & node & "-thumb.jpg">
						<div id="elementBranchListNavImage">
							<cfif fileexists(linkImagePath)>
								<img src="/content/elementLinkImages/linkImageSquare-#node#-thumb.jpg" border="0">
							<cfelseif defaultToCurrentElementImage>
								<cfset linkImagePath=application.userfilesabsolutepath & "content\elementLinkImages\linkImageSquare-" & request.currentElement.node & "-thumb.jpg">
								<cfif fileexists(linkImagePath)>
									<img src="/content/elementLinkImages/linkImageSquare-#request.currentElement.node#-thumb.jpg" border="0">
								</cfif>							
							</cfif>
						</div>
					</cfif>
				</cfif>
					<div <cfif node is Elementid>id="elementBranchListNav<cfif localgeneration eq 3>#htmleditformat(defaultBranchListColumns)#Col</cfif>Selected#htmleditformat(localgeneration)#"<cfelse>id="elementBranchListNav<cfif localgeneration eq 3>#htmleditformat(defaultBranchListColumns)#Col</cfif>UnSelected#htmleditformat(localgeneration)#"</cfif> <cfif isdefined('theClass')>class="#htmleditformat(theClass)#"</cfif>><span><cfif groupGenerationClickable eq "false" and localgeneration eq 2>#htmleditformat(headline)#<cfelse><A HREF="#resolvedhref#" target="#resolvedtarget#">#htmleditformat(headline)#</A></cfif></span></div>
				<cfset currentgeneration =localgeneration >
				<!--- close final div --->
				<cfif navigationbranch.recordcount eq currentrow>
					</div>
				</cfif>
			</cfloop>
			</div>
			<cfif showHeaders>
				<div id="elementBranchListPageHeader"><p>phr_headline_element_#htmleditformat(elementid)#:</p></div>
			</cfif>	
		</div>&nbsp;<!--- space stops firefox breaking display when a table follows directly after this point --->
	
	</cfoutput>

