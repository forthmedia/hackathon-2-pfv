<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementPressRelease.cfm
Author:			SWJ based on DAM's code
Date created:	2002-01-21

	Objective - This displays a list of children as press releases
		
	Syntax	  -	Simply place <RELAY_ELEMENTPRESSRELEASELIST> in the calling 
				element.
	
	Parameters - Currently there are no parameters
				
	Return Codes - none

Amendment History:

Date 		Initials 	What was changed
2002-01-21	SWJ			1st version

Enhancement still to do:
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate


--->

<CFINCLUDE TEMPLATE="qryGetChildren.cfm">

<table width="100%" cellspacing="2" cellpadding="2" border="0">

<cf_translate>    
<CFOUTPUT QUERY="GetChildren">

	
	
	<tr>
	    <td width="25%" align="left" valign="top">#htmleditformat(lastUpdated)#</td>
		<td><A href="javascript:openInNewWindow(#GetChildren.id#)">phr_headline_element_#htmleditformat(GetChildren.ID)#</A></td>
	</tr>
</CFOUTPUT>
</cf_translate>
</table>


