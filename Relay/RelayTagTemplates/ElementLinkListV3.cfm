<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementLinkList.cfm
Author:			SWJ based on DAM's code
Date created:	2002-01-21

	Objective - This displays a list of children of the calling element as a URL 				list

	Syntax	  -	Simply place <RELAY_ELEMENTLINKLIST> in the calling element.

Amendment History:

Date 		Initials 	What was changed
2002-01-21	SWJ			Called qryGetChildren as an include
2005-02-11	SWJ			Redesigned to use .css
2006-11-15    WAB 		added support for hidefromlinklist=1 in the parameters of an element
2007-03-14	WAB			now uses showOnSecondaryNavigation
2008/04/23	WAB			numberofGenerations was being passed into the getSecondaryNavigationBranchFromTree functions, but the relaytag editor was using the parameter numberofGenerationsOfChildren
						a search of the db suggests that no sites are using parameter numberOfGenerations, so will go with numberOfGenerationsOfChildren
2011-10-12 	NYB 		LHID7848
2014-01-31	WAB 		removed references to et cfm, can always just use / or /?
10/06/2014	YMA			Responsive re-write bootstrap 12 column layout
13/02/2017	YMA			Remove numberOfGenerations param as it is not being referenced by the code

Enhancement still to do:

--->

<cf_param name="elementLinkListShowSummary" type="boolean" default="No"/>
<cf_param name="linkListDivID" label="Div ID (leave the default unless you have defined a special CSS class)" default="ElementLinkListContent"/>
<cf_param name="elementLinkListShowImage" type="boolean" default="No"/>
<cf_param name="numberOfGenerationsOfChildren" type="numeric" default="1" validValues="1,2"/>
<cf_param name="showCurrentElementChildren" type="boolean" default="No"/>
<cf_param name="linkImageSize" type="numeric" default="80"/>
<cf_param name="showHeaderIcon" type="boolean" default="No"/>
<cf_param name="wrapLinkAround" type="string" default="header" validvalues="All,Header"/> <!--- NJH 2006/08/17 (accepted params:header,all) currently sticks the <a href> around the header or the whole element--->
<cf_param name="positionText" type="string" default="right" validvalues="Right,Left"/> <!--- place the text to the right of the image, or below the image --->
<cf_param name="defaultLinkListColumns" type="numeric" default="2"/>
<cf_param name="headerIconSrc" type="string" default=""/>

<cfparam name="linkImageType" type="string" default="linkImage">

<cfparam name="request.linkIconDir" type="string" default="/code/borders/Images"> <!--- override in relayBordersINI.cfm --->
<cfparam name="request.thumbImageType" default="jpg">

<cfset GetChildren = application.com.relayElementTree.getSecondaryNavigationBranchFromTree(
						elementTree = request.currentElementTree,
						topOfBranchID = request.currentElement.id,
						numberofgenerations = numberofgenerationsOfChildren +1
						)>
						<!---  note that the +1 is required because numberofgenerations includes the parent item as a generation, whereas numberofgenerationsOfChildren does not--->

<!---  --->
<cfset linkListElementColWidthMD = 12/defaultLinkListColumns>
<cfset linkListElementColWidthXS = 12>

<cfoutput>
	<div id="#linkListDivID#" class="noembedly row">
		<cfloop query="GetChildren">
			<!--- 2011-10-12 NYB LHID7848 replaced parameters with GetChildren.parameters[currentrow] - as parameters already exists as a higher scope variable and this is what's getting picked up - not the query column --->
			<cfif GetChildren.parameters[currentrow] contains "hideFromLinkList=1">
				<cfset GetChildren.recordCount = GetChildren.recordCount - 1>
			<cfelse>
				<!--- if the element has been set to isExternalFile then treat the link as an external URL --->
				<cfif isexternalfile eq 1>
					<cfset link = url>
					<cfset theTarget = "_blank">
				<cfelse>
					<cfset linkID = elementTextID neq ""?elementTextID:id>
					<cfset link = "/?eid=" & linkID>
					<cfset theTarget = "_self">
				</cfif>
				<article class="col-md-#linkListElementColWidthMD# col-xs-#linkListElementColWidthXS#" id="#linkListDivID#Element">

				<cfif wrapLinkAround eq "all">
					<a href="#link#" id="#linkListDivID#Link" target="#theTarget#">
				</cfif>
				<cfif elementLinkListShowImage>
					<cfset linkImagePath=application.userfilesabsolutepath & "\content\elementLinkImages\#linkImageType#-" & GetChildren.ID & "-thumb." & request.thumbImageType>
					<cfif fileexists(linkImagePath)>
						<img id="#linkListDivID#Image" src="/content/elementLinkImages/#linkImageType#-#GetChildren.ID#-thumb.#request.thumbImageType#" border=0 width="#linkImageSize#" <cfif positionText eq "right">class="pull-left"</cfif>/>
					<cfelse>
						<cfif showHeaderIcon>
							<img id="#linkListDivID#HeaderIcon" src="#request.linkIconDir#/#headerIconSrc#" border=0 width="#linkImageSize#" <cfif positionText eq "right">class="pull-left"</cfif>/>
						<cfelse>
							<img id="#linkListDivID#Image" src="/images/misc/blank.gif" height=1 border=0 width="#linkImageSize#" <cfif positionText eq "right">class="pull-left"</cfif>/>
						</cfif>
					</cfif>
				</cfif>

				<h2 id="#linkListDivID#Header">
				<cfif wrapLinkAround eq "header">
					<a href="#link#" id="#linkListDivID#Link" target="#theTarget#">
				</cfif>
						phr_headline_element_#htmleditformat(GetChildren.ID)#
				<cfif wrapLinkAround eq "header">
					</a>
				</cfif>
				</h2>

				<cfif elementLinkListShowSummary>
					<p id="#linkListDivID#SummaryText">
						phr_summary_element_#htmleditformat(GetChildren.ID)#
					</p>
				</cfif>

				<cfif wrapLinkAround eq "all">
					</a>
				</cfif>
				</article>
			</cfif>
			<cfif currentrow/defaultLinkListColumns eq ceiling(currentrow/defaultLinkListColumns)>
				<div class="clearfix hidden-xs"></div>
			</cfif>
		</cfloop>
	</div>
</cfoutput>