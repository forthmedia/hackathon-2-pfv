<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			oppotunityPricingSupport.cfm
Author:				NH/SWJ
Date started:		2006-08-16
	
Description:			

called by oppProductList.cfm as a pop up for special price reasons

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->
 
<cfparam name="opportunityID" type="string" default="1">
<cfparam name="form.sortOrder" type="string" default="SKU ASC">
<cfparam name="useProductStatus" default="true">

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricingReason" returnvariable="qOppPricingReason">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
</cfinvoke>

<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
	select * from vLeadListingCalculatedBudget where opportunity_ID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfquery name="getCurrencySign" datasource="#application.SiteDataSource#">
	select currencySign from currency where currencyISOcode =  <cf_queryparam value="#getOppDetails.currency#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfquery name="getOppAccountManager" datasource="#application.SiteDataSource#">
	select vendorAccountManagerPersonID from opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfset csign = #getCurrencySign.currencySign#>

<cfif isDefined("frmRequestPricing")>

	<cfquery name="getSPReasonDescription" dbtype="query">
		select reason from qOppPricingReason where OppPricingReasonID = #frmSPreason#
	</cfquery>

	<cfset userComments = "phr_oppSpecialPriceDesired: "&#csign#&#frmSpecialPrice#&", phr_oppReason: " & #getSPReasonDescription.reason# & ", phr_oppComments: " & #frmComments#>
	<cfset statusHistoryInserted = application.com.oppSpecialPricing.insertOppPricingStatusHistory(opportunityID=#opportunityID#,oppPricingStatusID=3,personID=#getOppAccountManager.vendorAccountManagerPersonID#,comments=#userComments#)>
	
	<cfif statusHistoryInserted>
		<cfif getOppAccountManager.vendorAccountManagerPersonID gt 0>
			<cfset variables.eMailResult = application.com.email.sendEmail(emailTextID="PartnerPricingRequest", personID=#getOppAccountManager.vendorAccountManagerPersonID#)>
			<cfoutput>#variables.eMailResult#</cfoutput>
			<cfif variables.eMailResult neq "Email Sent">
				<cfset message = "phr_oppErrorSendingRequest">
			<cfelse>
				<cfset message = "phr_oppYourRequestHasBeenSent">
			</cfif>
		<cfelse>
			<cfset message = "phr_oppNoAccountManagerExists">
		</cfif>
	<cfelse>
		<cfset message = "phr_oppThereWasAProblemCreatingTheRequest">
	</cfif>
	
<cfelse>

	<!--- this sets the productCatalogueCampaignID variable, which we're using for our promoID --->
	<cfinclude template="/code/cftemplates/opportunityINI.cfm">
	
	<cfscript>
	// create an instance of the opportunity component
	myopportunity = createObject("component","relay.com.opportunity");
	myopportunity.dataSource = application.siteDataSource;
	myopportunity.opportunityID = opportunityID;
	myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");
	myopportunity.countryID = getOppDetails.countryID;
	myopportunity.sortOrder = form["sortOrder"];
	myopportunity.getOppProducts();
	</cfscript>

</cfif>

<cf_translate>


<cf_head>
<cf_title>phr_oppOpportunityPricingSupport</cf_title>

	
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<SCRIPT type="text/javascript">
<!--

	function validateFrmComments() {
		if (document.frmReasonForm.frmComments.value == "") {
			alert ("phr_oppYouMustProvideReasonForSpecialPricing");
			return false;
		}
		else
		{
		document.frmReasonForm.frmSPreasonText.value = document.frmReasonForm.frmSPreason.options[document.frmReasonForm.frmSPreason.selectedIndex].text;
		//document.frmReasonForm.submit();
		return true;
		}
	}

//-->
</script>

</cf_head>


<cfoutput>

<cfset request.relayFormDisplayStyle="HTML-table">
<cf_relayFormDisplay>

<cfform  method="post" name="frmReasonForm" id="frmReasonForm" >
	
	<cfif #getOppAccountManager.vendorAccountManagerPersonID# eq 0>
		<cfset message = "phr_oppNoAccountManagerExists">
	</cfif>
	
	<cfif isDefined("message")>
	
		<cf_relayFormElementDisplay relayFormElementType="MESSAGE" spanCols="yes" currentValue="#message#" label="">
		<cf_relayFormElementDisplay relayFormElementType="button" fieldName="frmClose" label="" currentValue="phr_Close" spanCols="yes" valueAlign="center" onClick="javascript:self.close();">
			
	<cfelse>
	
		<cfset IllustrativePriceToCurrency = application.com.settings.getSetting("leadManager.IllustrativePriceToCurrency")>
	
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#getOppDetails.detail# (#opportunityID#)" label="phr_oppProjectName">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_oppSpecialPricingIntroText" spanCols="yes">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" spanCols="yes" valueAlign="center">
		
				<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
					<cfparam name="theColSpan" default="6">
					<cfparam name="theColSpan2" default="4">
					<cfif structkeyExists(request,"illustrativePriceMethod")>
						<cfset theColSpan = 7>
						<cfset theColSpan2 = 5>
					</cfif>
					<tr>
						<th colspan="#theColSpan#" align="left">
							phr_oppProductSectionHeading
						</th>
					</tr>
					<TR>
						<cfif ShowProductDescription>
						<TH>phr_oppProductDescription</TH>
						</cfif>
						<TH>phr_oppProductSKU</TH>
						<TH>phr_oppProductquantity</TH>
						<cfif useProductStatus>
						<th nowrap>phr_oppProductStatus</th>
						</cfif>
						<TH>phr_oppProductUnitPrice</TH>
						<TH>phr_oppProductSpecialPrice</TH>
						<th nowrap>phr_oppProductSubTotal <cfif structkeyexists(request,"IllustrativePriceFromCurrency")>(#htmleditformat(request.IllustrativePriceFromCurrency)#)</cfif></th>
						<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
						<cfif structkeyExists(request,"illustrativePriceMethod")>						
							<cfswitch expression="#request.illustrativePriceMethod#">
								<cfcase value="2">
									<th>phr_oppProductSubTotal (#htmleditformat(IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
								</cfcase>
							</cfswitch>						
						</cfif>
					</TR>
					
					<CFLOOP QUERY="myopportunity.getOppProducts">
						<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
							<cfif ShowProductDescription>
							<td>#htmleditformat(description)#</td>
							</cfif>
							<td>#htmleditformat(sku)#</td>
							<td align="right">#htmleditformat(quantity)#</td>
							<cfif useProductStatus>
							<td align="right">#htmleditformat(status)#</td>
							</cfif>			
							<td align="right">#htmleditformat(csign)##htmleditformat(unitprice)#</td>
							<td align="right"><cfif specialPrice neq "">#htmleditformat(csign)##htmleditformat(specialPrice)#</cfif></td>
							<td align="right">#htmleditformat(csign)##htmleditformat(subtotal)#</td>
							<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
							<cfif structkeyExists(request,"illustrativePriceMethod")>						
								<cfswitch expression="#request.illustrativePriceMethod#">
									<cfcase value="2">
										<cfscript>
											IllustrativePrice = application.com.currency.convert(
												method = #request.IllustrativePriceMethod#,
												fromValue = subtotal,
												fromCurrency = request.IllustrativePriceFromCurrency,
												toCurrency = IllustrativePriceToCurrency,
												date = getOppDetails.last_Updated
												);
										</cfscript>
										<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
										<td align="right">#htmleditformat(IllustrativePriceOutput)#</td>
									</cfcase>
								</cfswitch>						
							</cfif>
						</TR>
					</CFLOOP>
				</TABLE>

		</cf_relayFormElementDisplay>
		<cf_translate phrases="phr_oppSpecialPriceIsNumericAndIsRequired"/>
		
		<cf_relayFormElementDisplay relayFormElementType="numeric" label="phr_oppSpecialPriceDesired (#getOppDetails.currency#)" currentValue="" fieldname="frmSpecialPrice" required="Yes" message="#phr_oppSpecialPriceIsNumericAndIsRequired#" size="7">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="phr_oppPleaseGiveReasonsForSpecialPricing">
			<cf_relayFormElement relayFormElementType="select" fieldname="frmSPreason" currentValue="" label="" query="#qOppPricingReason#" display="reason" value="OppPricingReasonID">
			<br>
			<cf_relayTextField
					maxChars="4000"
					currentValue=""
					thisFormName="frmReasonForm"
					fieldName="frmComments"
					helpText=""
					required="yes"
			>
		</cf_relayFormElementDisplay>
		<cf_relayFormElementDisplay relayFormElementType="submit" label="" currentValue="phr_oppRequestSpecialPrice" fieldname="frmRequestPricing" spanCols="yes" valueAlign="center">
		
	</cfif>

</cfform>
</cf_relayFormDisplay>
</cfoutput>


 




</cf_translate>



