<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			siteFooter.cfm	
Author:				SWJ
Date created:		07 July 2002

Description:		An include file that produces a site footer in text form containing
					links to the top elements for the site.
					
Pre-requisites:		Must be called within CFOutput tags
					

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate

 --->

<CFSTOREDPROC PROCEDURE="GetElementBranch" DATASOURCE="#application.sitedatasource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#application.TopElementIDPartnerContent#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#frmPersonid#" NULL="No">
	<CFPROCRESULT NAME="relatedLinks" RESULTSET="1">
	<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Depth" VALUE="2" NULL="No">
</CFSTOREDPROC>



<CFSET nCounter=0>
<cf_translate>
<CFLOOP Query="relatedLinks">
	
	<CFSET nCounter=nCounter+1>
		<CFOUTPUT><A HREF="/?eid=#node#" CLASS="footer">phr_headline_element_#htmleditformat(node)#</A>&nbsp;</CFOUTPUT>
		<CFIF nCounter NEQ footerElements.recordcount>&nbsp;</CFIF>
</CFLOOP>
</cf_translate>

