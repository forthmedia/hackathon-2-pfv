<!--- �Relayware. All Rights Reserved 2014 --->
<style>
	div#pollDisplay{
		position:relative; width: 250px; left: 250px;
	}
</style>

<cfparam name="flaggroupid" default="65">
<cfparam name="form.start" default="true">
<cfset getflagGroup = application.com.flag.getFlagGroupStructure(flaggroupid)>
<!--- <cfdump var = "#getflagGroup#">  --->

<cfif form.start>
	<cfscript>
		thisFieldTextID = getFlagGroup.flagGroupTextID;
		personid = request.relayCurrentUser.personid;
		thisLineMethod = "edit";
		SPECIALFORMATTING  = "";
		maxLength = "";
		size="";
		itemid = "";
		IsCollapsable = false;
		IsVisible = true;
		countryID = request.relayCurrentUser.countryID;
	</cfscript>

	<cfset flaglist = "">
	<cfset entitytype = #getflaggroup.entitytypeid#>
	<cfset entitytypename = application.entityType[entitytype].tablename>
	<!--- <cfset entitytypename =  listgetat(application.typeentitytable,getflaggroup.entitytypeid+1)> --->
	<cfset thisentity = evaluate("#entityTypeName#id")>
	<cfset flagmethod = #thislinemethod#>
	<cfset flagformat=specialformatting>	
	<cfset flagfieldmaxlength=maxlength> 
	<cfset flagfieldsize=size>
	
	<cf_translate>
	<div id="pollDisplay">
	<form action="" method="post" name="pollDisplayForm" id="pollDisplayForm">
		<cfinclude template="/flags/FlagGroup.cfm">
		<input type="submit" name="updatePoll" value="phr_ext_poll_PostYourView">
		<input type="hidden" name="start" value="false">
		<cfoutput>
			<CF_INPUT type="hidden" name="frmpersonidlist" value="#request.relayCurrentUser.personid#">
			<input type="hidden" name="frmTableList" value="person">
			<CF_INPUT type="hidden" name="frmPersonFlagList" value="#flagList#">
			<input type="hidden" name="frmpersonfieldlist" value="">
		</cfoutput>
	</form>
	</div>
	</cf_translate>
	
<cfelse>
	<cfif isDefined("form.UpdatePoll")>
		<!--- <cfdump var="#form#"> --->
		
		<cf_ascreenUpdate>
	</cfif>


	<CFQUERY NAME="Query1" datasource="#application.sitedatasource#">
		SELECT Flag.Name, 
			Flag.FlagID, <!--- Flag.orderingIndex, --->
			<!--- countryid, --->
			Count(e.#getFlagGroup.entityType.UniqueKey#) AS Total
			<!--- --Fraction=cast(((Convert(Real, Count(e.#getFlagGroup.entityType.UniqueKey#))/#Total1#)*100) as int) --->
		FROM #getFlagGroup.flagType.dataTableFullName# fdt 
		INNER JOIN Flag ON fdt.FlagID = Flag.FlagID and Flag.FlagGroupID =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		inner JOIN (#getFlagGroup.entityType.tableName# e  
		<cfif getFlagGroup.entityType.tableName is "person">
		inner join location l on l.locationid = e.locationid
		</cfif>
		)
		ON fdt.EntityID = e.#getFlagGroup.entityType.UniqueKey#
		GROUP BY <!--- Flag.orderingIndex,  --->Flag.Name, flag.FlagID<!--- , countryid --->
		ORDER by Flag.name
	</CFQUERY>
	
	<!--- <cfdump var = "#query1#"> --->
	
	<cfparam name="chartTitle" type="string" default="">
	<cfparam name="chartType" type="string" default="Pie">
	<cfparam name="chartheight" type="numeric" default="275">
	<cfparam name="chartwidth" type="numeric" default="550">
	
	<!--- <cfoutput><div style="font-weight: bold;">#chartTitle#</div></br></cfoutput> --->
	<cf_chart
	         format="flash"
	         chartheight="#chartheight#"
	         chartwidth="#chartwidth#"
	         seriesplacement="default"
	         labelformat="number"
	         show3d="yes"
	         tipstyle="mouseOver"
	         pieslicestyle="sliced">
	   	<cf_chartseries
	         type="#chartType#"
	         query="Query1"
	         itemcolumn="name"
	         valuecolumn="Total">
		</cf_chartseries>
	</cf_chart>
</cfif>

