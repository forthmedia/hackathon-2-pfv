<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		PartnerHomePage.cfm
Author:			SWJ
Date created:	/2000

Description:

Date Tested:
Tested by:

Amendment History:

Version 	Date (DD-MMM-YYYY)	Initials 	What was changed
1
2			2001-01-02				WAB		Moved from root to partnerLogin Directory
2.01		2001-01-28 			SWJ		tidied up this document (and renamed from partnerPortalContent)

WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

Enhancements still to do:
--->



<!--- default values --->
<CFPARAM NAME="tablewidth" DEFAULT="width=123">
<CFPARAM NAME="TableBGCOLOR" DEFAULT="00ADAD">
<CFPARAM NAME="TDHeadingClass" DEFAULT="leftSubNavHeading">
<CFPARAM NAME="TDHeadingAlign" DEFAULT="right">
<CFPARAM NAME="TDHeadingHeight" DEFAULT="38">
<CFPARAM NAME="TDClass" DEFAULT="subNavMenuItem">
<CFPARAM NAME="TDAlign" DEFAULT="right">
<CFPARAM NAME="oncolour" DEFAULT="##FF9900">
<CFPARAM NAME="offcolour" DEFAULT="##DADADA">
<CFPARAM NAME="DividerIMG" DEFAULT="1pixel-black-000000.gif">
<CFPARAM NAME="flagTextID" DEFAULT="PartnerHomepage_News_Item">
<!--- <CFOUTPUT>#listfirst(flagTextIDs)#</CFOUTPUT>
<CFLOOP INDEX="i" LIST="flagTextIDs">

	<cfset query = "Get"&#listfirst(i)#>
	<CFQUERY NAME="#query#" DATASOURCE="#application.sitedatasource#">
		SELECT e.ID,isExternalFile,URL
		FROM Element e 
		INNER JOIN BooleanFlagData b ON e.id = b.entityid
		INNER JOIN flag f ON f.flagid = b.flagid
		WHERE f.flagtextid='#i#'
		order by sortorder
	</CFQUERY>
	<cfset #i# = valueList(query.id)>
</CFLOOP> --->
<CFQUERY NAME="GetItems" DATASOURCE="#application.sitedatasource#">
	SELECT e.ID,isExternalFile,URL
	FROM Element e 
	INNER JOIN BooleanFlagData b ON e.id = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	WHERE f.flagtextid =  <cf_queryparam value="#flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	order by sortorder
</CFQUERY>
<cfset itemIDs = valueList(GetItems.id)>


<cfif not isDefined("frmPersonid") and isDefined("request.relayCurrentUser.personid")><cfset frmPersonid=#request.relayCurrentUser.personid#></cfif>

<CFSTOREDPROC PROCEDURE="GetPersonElements" DATASOURCE="#application.sitedatasource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#frmPersonid#" NULL="No">
	<CFPROCRESULT NAME="PersonElements" RESULTSET="1">
</CFSTOREDPROC>
<CFSET PersonElementList = valuelist(PersonElements.id)>

<CFOUTPUT>
	<TABLE BORDER="0" #tablewidth# CELLSPACING="0" CELLPADDING="0" BGCOLOR="#TableBGCOLOR#">
		<TR>
			<cf_translate phrases="img_LatestNewsVertical"/>
			<CFIF not isDefined("phr_img_LatestNewsVertical") and len(phr_img_LatestNewsVertical) neq 1>
 				<TD COLSPAN="2"><IMG SRC="/images/#phr_img_LatestNewsVertical#" ALT="Latest News" BORDER="0"></TD>
			<CFELSE>
				<TD align="#TDHeadingAlign#" HEIGHT="#TDHeadingHeight#" class="#TDHeadingClass#">Latest News</TD>
				<TD align="#TDHeadingAlign#" HEIGHT="#TDHeadingHeight#" class="#TDHeadingClass#">&nbsp;</TD>
			</CFIF>
		</TR>
			<TR><TD COLSPAN="2"><IMG SRC="/images/background/#DividerIMG#" width="100%" HEIGHT="1px"></TD></TR>
		<cfloop query="GetItems">
			<TR ALIGN="right" VALIGN="top" BGCOLOR="#offcolour#" ONMOUSEOVER="this.style.backgroundColor= '#oncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#offcolour#';">	
				<TD ALIGN="#TDAlign#" VALIGN="top" class="#TDClass#">
					<CFIF PersonElementList contains ID>
						<CFIF isExternalFile EQ 1>
							<A class="#TDClass#" TARGET="blank" HREF="#URL#">phr_headline_element_#htmleditformat(ID)#</A>
						<CFELSE>
							<A class="#TDClass#" HREF="/elementTemplate.cfm?elementID=#id#">phr_headline_element_#htmleditformat(ID)#</A>
						</CFIF>
						<br>phr_summary_element_#htmleditformat(ID)#
					</CFIF>
				</TD>
				<TD>&nbsp;</TD>
			</TR>
			<TR><TD COLSPAN="2"><IMG SRC="/images/background/#DividerIMG#" width="100%" HEIGHT="1px"></TD></TR>
		</CFloop>
 		
	</TABLE>
</CFOUTPUT>




