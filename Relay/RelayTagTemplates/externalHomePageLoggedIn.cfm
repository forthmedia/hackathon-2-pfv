<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		PartnerHomePage.cfm
Author:			SWJ
Date created:	/2000

Description:

Date Tested:
Tested by:

Amendment History:

Version 	Date (DD-MMM-YYYY)	Initials 	What was changed
1
2			2001-01-02				WAB		Moved from root to partnerLogin Directory
2.01		2001-01-28 			SWJ		tidied up this document (and renamed from partnerPortalContent)

WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
Enhancements still to do:
--->





<CFQUERY NAME="GetNewsItems" DATASOURCE="#application.sitedatasource#">
	SELECT e.ID 
	FROM Element e 
	INNER JOIN BooleanFlagData b ON e.id = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	WHERE f.flagtextid='PartnerHomepage_News_Item'
</CFQUERY>
<cfset newsIDs = valueList(getNewsItems.id)>

<CFQUERY NAME="GetPromos" DATASOURCE="#application.sitedatasource#">
	SELECT e.ID
	FROM Element e 
	INNER JOIN BooleanFlagData b ON e.id = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	WHERE f.flagtextid='PartnerHomepage_Promo_Item'
</CFQUERY>
<cfset promoIDs = valueList(GetPromos.id)>




<CFOUTPUT>
<cf_translate>

<TABLE WIDTH="440" BORDER="0" CELLSPACING="0" CELLPADDING="2" BGCOLOR="White">
	
	<TR>
		<CFIF isDefined("NewsGIF")>
			<TD><IMG SRC="/content/MiscImages/#newsGIF#" ALT="Latest News" BORDER="0"></TD>
		<CFELSE>
			<TD><H2>Latest News</H2></TD>
		</CFIF>
		
		<td width="15px">&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<CFIF isDefined("promoGIF")>
			<TD><IMG SRC="/content/MiscImages/#promoGIF#" ALT="Latest News" BORDER="0"></TD>
		<CFELSE>
			<TD><H2>Latest Promotions</H2></TD>
		</CFIF>
	</TR>
	
	<TR>
		<TD></TD>
		<td width="15px">&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<TD></TD>
	</TR>

	<TR>	
		<TD width="40%" ALIGN="left" VALIGN="top">
			<cfloop query="GetNewsItems">
				<P><A HREF="/elementTemplate.cfm?elementID=#id#">phr_headline_element_#htmleditformat(ID)#</A>
				<br>phr_summary_element_#htmleditformat(ID)#</P>
			</CFloop>
		</TD>
		<td width="15px">&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<TD width="40%" ALIGN="left" VALIGN="top">
			<cfloop query="getPromos">
				<P><A HREF="/elementTemplate.cfm?elementID=#id#">phr_headline_element_#htmleditformat(ID)#</A>
				<BR>phr_summary_element_#htmleditformat(ID)#</P>
			</CFloop>
		</TD>
	</TR>				

</TABLE>
</cf_translate>
</CFOUTPUT>




