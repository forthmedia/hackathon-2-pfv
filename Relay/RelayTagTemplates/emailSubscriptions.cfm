<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		emailSubscriptions.cfm
Author:			SWJ - based on the form at Macromedia.com
Date created:	2002-05-25

	Objective - The idea is to create a standard relayTagTemplate that we can call
				which provides a simple way of specifying prefernces e.g. HTML vs 
				text emails etc.
				
				I think this should 
		
	Syntax	  -	
	
	Parameters - 
				
	Return Codes - 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

	<FORM action="index.cgi" name="form1" method="POST">
		<TABLE width="83%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD align="left" valign="middle" class="mmbody" height="24" bgcolor="#333399"><B><FONT color="#FFFFFF">&nbsp;&nbsp;Notification Options</FONT></B>
				</TD>
			</TR>
			<TR>
				<TD align="left" valign="top">
					<TABLE border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD><IMG src="/images/pixel.gif" width="50" height="10" alt="">
							</TD>
							<TD><IMG src="/images/pixel.gif" width="312" height="10" alt="">
							</TD>
							<TD><IMG src="/images/pixel.gif" width="10" height="10" alt="">
							</TD>
							<TD><IMG src="/images/pixel.gif" width="312" height="10" alt="">
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD class="mmbody" align="left" valign="top">	Notify 
		                            me when the new Macromedia MX products are available 
		                            for purchase.
							</TD>
							<TD align="left" valign="top">	&nbsp;
							</TD>
							<TD class="itemmed" align="left" valign="top">								
								<INPUT type="checkbox" name="6235" checked value="1">
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD colspan="3" class="separator">	------------------------------------------------------------------------------------------------------------------------------
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD class="mmbody" align="left" valign="top">	Please 
		                            send me news about Macromedia product tutorials, discounts, 
		                            and special events.
							</TD>
							<TD align="left" valign="top">	&nbsp;
							</TD>
							<TD class="itemmed" align="left" valign="top">								
								<INPUT type="checkbox" name="6236" checked value="1">
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD colspan="3" class="separator">	------------------------------------------------------------------------------------------------------------------------------
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD class="mmbody" align="left" valign="top">	I prefer 
		                            to receive e-mail in: *
							</TD>
							<TD align="left" valign="top">	&nbsp;
							</TD>
							<TD class="itemmed" align="left" valign="top">								
								<INPUT type="radio" name="seminarcode" checked value="518">HTML<BR>								
								<INPUT type="radio" name="seminarcode" value="859">HTML with Macromedia Flash (Rich Media)<BR>								
								<INPUT type="radio" name="seminarcode" value="519">Plain Text (ASCII)
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD class="mmbody" align="left" valign="top">	&nbsp;
							</TD>
							<TD align="left" valign="top">	&nbsp;
							</TD>
							<TD class="itemmed" align="left" valign="top">	&nbsp;
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD class="mmbody" align="left" valign="top">	Please 
		                            indicate the language you prefer to receive communications:
							</TD>
							<TD align="left" valign="top">	&nbsp;
							</TD>
							<TD class="itemmed" align="left" valign="top">
								<SELECT name="6233" style="width: 300px;">
									<OPTION value="1">&lt;Select&gt;
									<OPTION value="14" selected>English
									<OPTION value="848">French
									<OPTION value="849">German
									<OPTION value="852">Japanese
									<OPTION value="851">Italian
									<OPTION value="853">Korean
									<OPTION value="854">Portuguese
									<OPTION value="855">Spanish
									<OPTION value="856">Swedish
									<OPTION value="846">Chinese Simplified
									<OPTION value="847">Chinese Traditional
									<OPTION value="860">Other
								</SELECT>
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD colspan="3" class="separator">	------------------------------------------------------------------------------------------------------------------------------
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD colspan="3" align="left" class="mmbodysml">	Note: 
		                            <A href="http://www.macromedia.com/help/privacy.html" target="_blank">Macromedia
                            respects the web and the privacy</A> of those who
                            use it. We do not rent or sell our mailing list to
                            anyone.
							</TD>
						</TR>
						<TR>
							<TD>	&nbsp;
							</TD>
							<TD colspan="3" align="right">								
								<INPUT type="image" border="0" name="SUBMIT_NEXT" src="/uber/images/btn_go_to_download.gif" width="114" height="21" alt="GO TO DOWNLOAD" hspace="5">
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
	</FORM>