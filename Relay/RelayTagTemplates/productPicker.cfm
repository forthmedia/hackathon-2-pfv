<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			productPicker.cfm
Author:				YMA
Date started:		2013/01/07

Purpose:	Display Products on portal.
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-01-30 	WAB Dealt with error in FileTypeGroupID bind (because FileTypeGroupID passed to the webservice was not numeric)
				Also FileTypeID becomes multiSelect
2013-12-17	WAB	CASE 438446 openCategory() could not handle null prodcut category - add some quotes
2014-07-30	AXA Added two tier hierarchy functionality.
2014-08-27	RPW	Can't view High Res product image in product listing
2015-07-28 DAN 445276 - Using RELAY_CATALOGUEASSETS and RELAY_FILE_LIST relaytags on a page results in an error
2015-08-05 DAN K-1314 - allow filtering products by multiple media groups
2015-09-07 DAN K-1314 - rename labels
2015/11/09	NJH	JIRA PROD2015-234 - give option to open the first level on page load.
2016/11/18  VB		SSPS-80  Added parameter fuzzyProductMatch to configure matching assests

Possible enhancements:

 --->
<cf_param label="Product Catalogue" name="productCatalogueCampaignID" validValues="func:com.settings.getValidValuesForSettings(variableName='products.productCatalogueCampaignID')" default="#application.com.settings.getSetting('products.productCatalogueCampaignID')#"/>
<cf_param label="Hierarchy" name="Hierarchy" default="threeTier" validValues="select 'threeTier' as value, 'Three Tier' as display union select 'twoTier' as value, 'Two Tier' as display" required="true"/>
<cf_param label="Link to Training" name="linkToTraining" type="boolean" default="false"/>
<cf_param label="Catalogue: Image width" name="imageWidth" default="80" type="integer"/>
<cf_param label="Catalogue: No Image File" name="noImageFile" default="/images/MISC/no_image.png" type="string"/>
<cf_param label="Allow Fuzzy Match for Product SKU Search" name="fuzzyProductMatch" type="boolean" default="yes"/>
<cf_param label="Catalogue: Show these fields" name="catalogueShowCols" allowSort="true" validValues="SKU,Title,Description,productID,crmProductID,productGroupID,campaignID,minSeats,maxSeats,listPrice,priceISOCode,resellerDiscount,distiDiscount,renewalSKU" default="sku,Title,Description" multiple="true" displayAs="twoSelects"/>
<!--- 2015-09-07 DAN K-1314 - rename labels --->
<cf_param label="Catalogue: Filter by Product Category" name="specifyCatagories" default="" allowSort="true" multiple="true" displayAs="twoSelects" validValues="select title_defaultTranslation as display, productCategoryID as value from productcategory"/>
<cf_param label="Catalogue: Filter by Product Group" name="specifyGroups" default="" allowSort="true" multiple="true" displayAs="twoSelects" validValues="select title_defaultTranslation as display, productGroupID as value from productgroup"/>
<!--- 2015-08-05 DAN K-1314 - allow multiselect for filtering by media group --->
<cf_param label="Media Library: Filter by Media Group" name="productFileTypeGroupIDs" default="0" validValues="select heading as display, fileTypeGroupID as value from fileTypeGroup" nullValue="0" multiple="true" displayAs="twoSelects" SIZE="5"/>

<cf_param label="Asset Display: Show these fields" name="assetsShowCols" allowSort="true" validValues="SKU,Title,Description,productLongDescription,productID,crmProductID,productGroupID,campaignID,minSeats,maxSeats,listPrice,priceISOCode,resellerDiscount,distiDiscount,renewalSKU" default="sku,Title,Description,productLongDescription" multiple="true" displayAs="twoSelects"/>
<cf_param label="Asset Display: Show file size" name="showFileSize" type="boolean" default="true"/>
<cf_param label="Asset Display: Show Thumbnail" name="showThumbnail" type="boolean" default="yes"/>
<cf_param label="Asset Display: Show file last revised date" name="showLastRevisedDate" type="boolean" default="no"/>
<cf_param label="Asset Display: Thumbnail Size" name="thumbnailSize" default="80" type="integer"/>
<cf_param label="Expand on Page Load" name="expandOnPageLoad" type="boolean" default="false"/>  <!--- JIRA PROD2015-234 --->

<cf_includeJavascriptOnce template = "/javascript/productPicker.js">
<!--- 27/03/2015 added jquery rating and ratings.js to support code that seems to be refering functions from these librarries. --->
<cf_includeJavascriptOnce template = "/javascript/lib/jquery/jquery.rating.js">
<cf_includeJavascriptOnce template = "/javascript/ratings.js">
<cf_includeCSSOnce template="/code/styles/productPicker.css">

<cfparam name="showBackLink" default="true">

<cf_head>
<script>
	setBoxClass('rate_content');
	<cfoutput>setEntityTypeID(#application.entitytypeid.product#);</cfoutput>
	jQuery(document).ready(
    function()
    	{
        	// The DOM (document object model) is constructed
        	// We will initialize and run our plugin here
			jQuery('input.star').rating();
			//we add the following to stop jQuery from conflicting with prototype.js
			jQuery.noConflict();

			//2014-08-27	RPW	Can't view High Res product image in product listing
			jQuery("a.productImage").fancybox({
				helpers: {
					overlay: {
						locked: true
    				}
				},
				scrollOutside: true
  			});
    	}
    )
</script>
</cf_head>

<cfif isdefined("productID") and isNumeric(productID)> <!--- 2015-07-28 DAN 445276 --->
	<cfset additionalCols = assetsShowCols>
	<cfif ListFindNoCase(assetsShowCols,'SKU',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'SKU',','))></cfif>
	<cfif ListFindNoCase(assetsShowCols,'title',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'title',','))></cfif>
	<cfif ListFindNoCase(assetsShowCols,'description',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'description',','))></cfif>
	<cfif ListFindNoCase(assetsShowCols,'productLongDescription',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'productLongDescription',','))></cfif>
	<cfset productQry = application.com.relayProduct.getProductSelectorProducts(productCatalogueCampaignID = productCatalogueCampaignID,productID = productID,additionalCols = additionalCols,productCatalogueCampaignID=productCatalogueCampaignID)>
	<cfset encryptedLink = application.com.security.encryptURL(url='/?eid=#request.currentelement.contentID#&postLoadProductID=#productID#',excludeVariableNamesRegExp='eid')>

	<cfoutput>
	<div id="productPickerWrapper" class="row">
	<div class="col-xs-12">
	<cfloop query="productQry">
		<cfif showBackLink>
		<div class="row">
			<div id="back" class="col-xs-12">
				<span class="left">#len(productGroupPortalTitle) ? htmleditformat(productGroupPortalTitle):htmleditformat(PRODUCTGROUPTITLE)#</span><a href="javascript:window.location.href = '#encryptedLink#';" id="backToProductPicker"><span class="right">phr_backToProductPicker</span></a>
			</div>
		</div>
		</cfif>
		<div id="productOverview" class="row">
			<!--- 2014-08-27	RPW	Can't view High Res product image in product listing --->
			<cfscript>
				if (FileExists(productHighResPath)) {
					variables.highResImage = productHighResUrl;
				} else if (FileExists(productThumbnailPath)) {
					variables.highResImage = productThumbnailURL;
				} else {
					variables.highResImage = noImageFile;
				}
			</cfscript>

			<div class="col-xs-12 col-sm-2">
				<cfif FileExists(productThumbnailPath) AND FileExists(productHighResPath)>
					<a href="#variables.highResImage#" class="productImage">
						<img src="#productThumbnailUrl#" style="max-width:#thumbnailSize#px;width:100%;" />
					</a>
				<cfelseif FileExists(productThumbnailPath) AND NOT FileExists(productHighResPath)>
					<img src="#productThumbnailUrl#" style="max-width:#thumbnailSize#px;width:100%;" />
				<cfelse>
					<img src="#noImageFile#" style="max-width:#thumbnailSize#px;width:100%;" />
				</cfif>
			</div>

			<div id="productDetails" class="col-xs-12 col-sm-7">
				<div id="productTitle"><p><cfif ListFindNoCase(assetsShowCols,"SKU")>#SKU#, </cfif><cfif ListFindNoCase(assetsShowCols,"title")>#title#</cfif></p></div>
				<cfif ListFindNoCase(assetsShowCols,"productLongDescription")>
					<div id="productDescription"><p>#productLongDescription#</p></div>
				<cfelseif ListFindNoCase(assetsShowCols,"description")><div id="productDescription">
					<div id="productDescription"><p>#description#</p></div>
				</cfif>

				<cfif len(additionalCols) gt 0>
					<p>
					<cfloop list="#additionalCols#" index = "Col">
						<span class="columnTitle">phr_asset_#Col#: </span><span class="columnValue">#productQry[Col][currentRow]#</span><br/>
					</cfloop>
					</p>
				</cfif>
			</div>
			<div id="productRating" class="col-xs-12 col-sm-3">
				#application.com.relayRating.getRatingsWidget(entityID=productID,entityTypeID=application.entityTypeID.product)#
			</div>
		</div>
		<cfloop list="Assets,Training" index="fileType">
			<cfinclude Template="/products/productPickerFiles.cfm">
		</cfloop>
		<form  name="postLoadProductID" id="postLoadProductID" method="post" novalidate="true" >
			<cf_relayFormElement relayFormElementType="hidden" fieldname="postLoadProductID" currentvalue="#productID#">
		</form>
	</cfloop>
		</div>
	</div>
	</cfoutput>
<cfelse>
	<cfif isdefined("postLoadProductID") and isNumeric(postLoadProductID)> <!--- 2015-07-28 DAN 445276 --->
		<cfset productQry = application.com.relayProduct.getProductSelectorProducts(productID=postLoadProductID,productCatalogueCampaignID=productCatalogueCampaignID)>
	</cfif>
	<div id="categoryHeading"><h2>phr_ProductCatagories</h2></div>
	<div id="productPickerWrapper" class="row">
	<cfif Hierarchy eq 'threeTier'>
		<cfif specifyCatagories neq "">
			<cfscript>
				CatagoryQry = "";
			</cfscript>
			<cfloop index="ListElement" list="#specifyCatagories#">
				<cfset CatagoryQry = application.com.relayProduct.getProductSelectorCategories(productCategoryID=ListElement,productCatalogueCampaignID=productCatalogueCampaignID)>
					<cfquery name="getCatagoryQry" dbtype="query">
						<cfif isDefined("productCatagoryQry")>
						select * from productCatagoryQry
							union all
						</cfif>
						select * from CatagoryQry
					</cfquery>
					<cfset productCatagoryQry = getCatagoryQry>
			</cfloop>
		<cfelse>
		<cfset productCatagoryQry = application.com.relayProduct.getProductSelectorCategories(productCatalogueCampaignID=productCatalogueCampaignID)>
		</cfif>
		<cfoutput query="productCatagoryQry">
			<cfset openCategoryJS = "openCategory(#productCatalogueCampaignID#,'#productCategoryID#',#request.currentelement.contentID#,#imageWidth#,'#noImageFile#','#catalogueShowCols#','#specifyGroups#')">
			<div class="row">
				<div id="categorySelector" class="col-xs-12">
					<div id="categorySelectorClick" class="notSelected" onClick="#openCategoryJS#">
						<div class="col-xs-3 col-sm-1 categorySelectorImg"><img id="categorySelectorImage" src="<cfif #thumbnailurl# neq ''>#thumbnailurl#<cfelse>#noImageFile#</cfif>" width="#imageWidth#px"/></div>
						<div class="col-xs-7 col-sm-10"><h3>#title#</h3></div>
						<div class="col-xs-2 col-sm-1"><span id="ViewAll"><button type="button" class="glyphicon glyphicon-chevron-right productsViewAllBtn" id="productsViewAll#productCategoryID#" /><!--- phr_ViewAll ---></button></span></div>
					</div>
					<div id="groupHeading#productCategoryID#" class="groupHeading" style="display:none"><h3>phr_ProductGroups</h3></div>
					<div id="groupSelector#productCategoryID#" class="groupSelector" style="display:none"></div>
				</div>
			</div>

			<!--- JIRA PROD2015-234 --->
			<cfif expandOnPageLoad and currentRow eq 1>
				<cf_head>
					<script>
						jQuery(document).ready(function(){#openCategoryJS#});
					</script>
				</cf_head>
			</cfif>
		</cfoutput>

		<cfif isdefined("postLoadProductID")>
			<cfoutput>
				<script type="text/javascript">
					window.onload = openCategory(#productCatalogueCampaignID#,'#productQry.productCategoryID#',#request.currentelement.contentID#,#imageWidth#,'#noImageFile#','#catalogueShowCols#','#specifyGroups#',#productQry.productGroupID#);
				</script>
			</cfoutput>
		</cfif>
	<cfelseif Hierarchy eq 'twoTier'>

		<!--- START 2014-07-30	AXA Added two tier hierarchy functionality.  --->
		<cfset productGroupQry = application.com.relayProduct.getProductSelectorGroups()>
		<cfoutput query="productGroupQry">
			<cfset groupTitle = len(portalTitle) ? portalTitle:title>
			<cfset openGroupTierJS ="javascript:openGroupTier2(#productCatalogueCampaignID#,'#productGroupID#',#request.currentelement.contentID#,#imageWidth#,'#noImageFile#','#catalogueShowCols#')">
			<div class="row" id="twoTier">
				<div id="groupSelector#productGroupID#" class="groupSelector col-xs-12">
					<div id="productGroup#productGroupID#" class="productGroup notselected" onClick="#openGroupTierJS#">
						<div class="col-xs-3 col-sm-1 categorySelectorImg"><img id="groupSelectorImage" src="<cfif #thumbnailurl# neq ''>#thumbnailurl#<cfelse>#noImageFile#</cfif>" width="#imageWidth#px"/></div>
						<div class="col-xs-7 col-sm-10"><h3>#htmleditformat(groupTitle)#</h3></div>
						<div class="col-xs-2 col-sm-1">
							<span id="ViewAll">
								<button type="button" class="glyphicon glyphicon-chevron-right productsViewAllBtn" id="productsViewAll#productGroupID#" /><!--- phr_ViewAll ---></button>
							</span>
						</div>
					</div>
				</div>
				<div id="openGroupTier2#productGroupID#" class="noSelected col-xs-12">
				</div>
			</div>

			<!--- JIRA PROD2015-234 --->
			<cfif expandOnPageLoad and currentRow eq 1>
				<cf_head>
					<script>
						jQuery(document).ready(function(){#openGroupTierJS#});
					</script>
				</cf_head>
			</cfif>
		</cfoutput>

		<cfif isdefined("postLoadProductID")>
			<cfoutput>
				<script type="text/javascript">
					jQuery(document).ready(function(){
						openGroupTier2(#productCatalogueCampaignID#,#productQry.productGroupID#,#request.currentelement.contentID#,#imageWidth#,'#noImageFile#','#catalogueShowCols#');
					});
				</script>
			</cfoutput>
		</cfif>
		<!--- END 2014-07-30	AXA Added two tier hierarchy functionality.  --->
	</cfif>
	</div>
	<cfform  name="postLoadProductID" method="post" novalidate="true" >
		<cf_relayFormElement relayFormElementType="hidden" fieldname="postLoadProductID" currentvalue="">
	</cfform>
</cfif>