<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History

Versions	Date			By		Description

1.1			2002-02-28		SWJ		The first version
1.2.		2002-06-19		DAM		Altered so that all files the person has rights to are loaded
									unless filetype or filegrouptype are supplied in which case it
									filters accordingly
1.3			2004-12-15		JC		Addition of language filter to GetResults query.
									Filter only applied if cookie.user exists and
									have assumed that site-level language change sets
									'languageid' variable
1.4			2005-03-18		AJC		Added <cf_translate> code to page... some elements still to be addressed.
1.5			2005-08-26		SWJ		1. Added the call to getThumbnail to display a thumbnail if showThumbnail is true
									2.  Modified the headings so they only show if suppressHeaders=false
									3.  PlainRows now is an option which defaults to true. If set to false the row class will alternate between odd and even.
									4. SuppressDownloadText is now defaulted to true but can be overidden.
									5. SupressFileName is now defaulted to true.
									6. showLastRevisedDate set to tru will show the last revised date
			2006-02-21		WAB		altered the query which checks for file rights to handle case of an edit permission being set but no view permissions
									now calls a .cfc function
									Also messed about with a fileget which does not require the page to reload
			2006-02-27		WAB		attempt to sove problem of file window not getting focus if already open and contains a pdf file!  Made changes to openwin.js and altered the form post to a get
			2006-03-21 		WAB 		removed mention of cookie.partner session
			2007-09-25		SSS			Added 2 switches to allow Grouping and file sizes to be displayed. The switchs are called
										FileListByGroup and showfilesize
			2009/01/23		NJH		Bug Fix All Sites 1559 - Added cfoutput around some of the code
			2010/10/13  	WAB 	removed get TextPhrases and replaced with CF_translate
			2011/05/04 		WAB 	LID 6475 reinstated filterByLanguage
			2012			NJH 	tidied CF_Params by adding validvalues and binding.  Got rid of separate fileListEditor
			2012-04-17		WAB		CASE 426874/427357 The above (NJH) changes have caused problems because fileType has been removed and replaced with fileTypeID, so editor is no longer backwards compatible with existing content.
									RelayTag would be more readable and portable if we were to use names rather than IDs so I wonder whether we should return to names.
									But we already use FileTypeGroupID - so thinking is a bit mixed!
									Have reverted to parameter FileType which stores a Name
			2012/02/27		NJH		CASE 426874: Added fileTypeGroup, so that it was consistent with fileType
			2012-11-09		IH		Case 431859 scope variables within GetFTG query loop
			2012-01-16		YMA		CASE 433105 updated Filelist to exclude expired files from results.
			2013-03-14 		NYB 	Case 434136 added valign param to improve formatting options
			2014-12-05		PPB		Case 442926 option to showDescription for a file
			2014-26-09		DCC		P-KAS031 Kaspersky request to enable flag association with filelist tag
			2015-02-03		AXA 	Case 443532 cleaned up code and removed old if/else for redundant code allow user to pass in paramters to sort query, added if to only sort if paramters were provided
			2015/08/03		NJH		Some code clean-up. Removed old JS functions that were not being called from anywhere. Open files in fancybox modal window
			2016/02/03		RJT		BF-435 Made FileTypeGroupID officially required (was always actually required). Previously would crash if it wasn't passed in, also removed all reference to the nonExistant getFTG query
            2016/10/13      DAN     PROD2016-2515/Case 451636 - fix file list display issue when the file source is not URL and actual file does not physically exists
            2016/11/28		AT      PROD2016-2812 Create drop down filed with available keywords to filter files
			2017-01-09		DAN		PROD2016-2470 - syntax error fix - checkExpires must be numeric

--->

<cf_translate>


<cf_param name="FileTypeGroupID" label="Media Group" required="true" validValues="select heading as display, fileTypeGroupID as value from fileTypeGroup"/> <!--- NJH 2012/02/27 CASE 426874 --->
<cf_param name="FileType" label="Resource Type" default="" nulltext="All Types" relayFormElementType="select" bindFunction="cfc:webservices.fileWS.callMethod(methodName='getFileTypes',fileTypeGroupID={FileTypeGroupID},returnType='query')" display="type" value="Type"/>   <!--- added WAB 20/2/06 --->
<cf_param name="FilterByKeyword" label="phr_filterByKeywordValue" default="" relayFormElementType="select" multiple="true" bindFunction="cfc:webservices.fileWS.callMethod(methodName='getFileKeyWords',returnType='query')" display="DATA" value="DATA"/>
<cfparam name="openwintype" default="1"><!--- NYB 2011-12-02 didn't cf_param because it doesn't seem to be getting used - was used in FileManagement/FileGetv2.cfm but was commented out --->
<cf_param name="FileListName" default="FileList" Label="Phrase Prefix for Custom Translations" />
<cf_param name="AlternativeClick" type="string" default="" Label="Alternative click text"/>
<cf_param name="FileListByGroup" type="string" default=""/> <!--- SSS 25/09/2007 added to allow grouping of files --->
<cf_param name="filterByLanguage" type="boolean" default="No"/>
<!--- 2005-08-31 Added the following params --->
<cf_param name="suppressHeaders" type="boolean" default="yes"/>
<cf_param name="showThumbnail" type="boolean" default="no"/>
<cf_param name="plainRows" type="boolean" default="yes"/>
<cf_param name="SuppressDownloadText" type="boolean" default="yes"/>
<cf_param name="SuppressFileName" type="boolean" default="yes"/>
<cf_param name="showLastRevisedDate" type="boolean" default="no"/>
<cf_param name="showFileSize" type="boolean" default="no"/> <!--- SSS 25/09/2007 Added to turn off/on file sizes --->
<cf_param name="showDescription" type="boolean" default="no"/> <!--- 2014-12-05 PPB add switch for showDescription --->
<!--- START - P-KAS031 DCC 24/9/2014 Associate profiles with files --->
<cf_param name="flagTextIDs" type="string" default="" validValues = "select flag as display, flagID as value from vflagDef where entityTable = 'files' "/>
<!--- END - P-KAS031 DCC 24/9/2014 Associate profiles with files --->

<cfparam name="FiletypeGroupIDList" default=""/>  <!--- WAB 2012-04-17 - no sign of this parameter being used so removed from editor. FileTypeGroupID parameter of getFilesAPersonHasCountryViewRightsTo can support a list of ids (although not through the editor), but I suspect that code would crash later  --->


<cf_param name="icon" default=""/>
<cf_param name="hideNew" type="boolean" default="no"/>

<cf_param name="fileSortOrder" default=""/>

<cf_param name="defaultFileListColumns" default="1" validValues = "1,2,3,4"/>

<cf_param name="valign" default="top"/> <!--- 2013-03-14 NYB Case 434136 added --->




<cfif defaultFileListColumns eq "1" and structKeyExists(request,"defaultFileListColumns")>
	<cfset defaultFileListColumns = request.defaultFileListColumns>
</cfif>

<CFIF structKeyExists(FORM,"fileID") and FORM.fileID neq 0>
	<!--- Changed to avoid multiple calls to the pop up window when <RELAY_FILE_LIST> is called more than once on a page --->
	<CF_INCLUDEONCE TEMPLATE="/fileManagement/fileGet.cfm">
<cfelse>
	<cf_includeJavascriptOnce template = "/javascript/openWin.js">
</CFIF>


<cfset FiletypeGroupIDList=(replace(FiletypeGroupIDList,"|",",","ALL"))>

	<!--- 2012-01-16		YMA		CASE 433105 updated Filelist to exclude expired files from results. --->
	<!--- 2017-01-09	DAN	- syntax error fix - checkExpires must be numeric --->
	<cfset args = {fileTypeGroupID = fileTypeGroupID,personid = request.relaycurrentuser.personid,returnCountryIDList = true,checkFileExists = true,checkExpires = 1,languageid = iif(filterByLanguage,de(request.relayCurrentUser.languageid),de(""))}>
	<cfif fileType is not "" >
		<cfset args.fileType = fileType>
	<cfelseif isDefined("fileTypeID") and fileTypeID is not "">
		<cfset args.fileTypeID = fileTypeID>
	</cfif>
	<!--- START - P-KAS031 DCC 24/9/2014 Associate profiles with files --->
	<cfif flagTextIDs is not "">
		<cfparam name = "args.join" default = "">
		<cfparam name = "args.filter" default = "">
		<cfset args.filter &= " (1=0" >
		<cfloop list = #flagTextIDs# index="flag">
			<cfset flagJoin = application.com.flag.getJoinInfoForFlag (flagid = flag, joinType = "left")>
			<cfset args.join &= " " & flagJoin.join>
			<cfset args.filter &= " OR " & flagJoin.selectfield & " = 1 ">
		</cfloop>
		<cfset args.filter &= " )" >
	</cfif>
	<!--- END - P-KAS031 DCC 24/9/2014 Associate profiles with files --->

	<cfif FilterByKeyword neq "">
        <cfset args.keywords = FilterByKeyword>
	</cfif>

	<!--- <cfset getResults = application.com.filemanager.getFilesAPersonHasViewRightsTo(personid = #request.relaycurrentuser.personid#, fileType = fileType, fileTypeGroupID = fileTypeGroupID, languageid = iif(filterByLanguage EQ "Yes",languageIndex,de("")))>  --->
	<cfset origResults = application.com.filemanager.getFilesAPersonHasCountryViewRightsTo(argumentCollection = args)>

	<cfset args ={originalFileQuery = origResults}>
	<cfif fileSortOrder neq "">
		<cfset args.overrideSortOrder = fileSortOrder >
	</cfif>
		<cfset getResults = application.com.filemanager.getMostSuitedFileFromFamily(argumentCollection = args)>

<!---
	<!--- if results found for the users language move on otherwise loop again to get default files --->
	<cfif getResults.recordcount gt 0>
		<cfbreak>
	</cfif>
</cfloop>
 --->


<CFQUERY NAME="getTypeText" datasource="#application.sitedatasource#">
	SELECT Heading,
			Intro,FileTypeGroupID
	FROM FileTypeGroup
	WHERE 1=1
	<CFIF IsDefined("FiletypeGroupID") AND FiletypeGroupID GT 0>
	AND FileTypeGroupID =  <cf_queryparam value="#FileTypeGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
</CFQUERY>

<CFPARAM NAME="translate" DEFAULT="0">
<cfparam name="hideElementMenuText" default="false">

<!--- NJH 2015/08/03 - load files in fancy box. Remove old JS functions as found nowhere where they were being called. Added a new function (downloadFile) which submits to the iframe
	beforeShow function prevents users from doing a right-click on the file for added file security. Added for Elastica and kept in core until a customer complains.
--->

	<!---
		function getFileV1(fileID) {
			form = document.getFileForm;
			form.fileID.value = fileID;
			form.submit();
		}


		function getFileV2(fileID) {
			openWin (' ','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
			form = document.getFileFormV2;
			form.fileID.value = fileID;
			form.submit();
		}


		getFile = getFileV2


		function drillToFileGroup(filegroupID) {
			form = document.getFileForm;
			form.FileTypeGroupID.value = filegroupID;
			form.submit();
		}
	--->


<!--- 2005-08-31 SWJ Changed the logic so that headers only show if suppressHeaders=false --->
<!--- NJH 2009/01/23 Bug Fix All Sites 1559 - added output tags --->
<cfoutput>
<cfif not SuppressHeaders>
	<cfif hideElementMenuText eq "false">
		<TABLE>
			<TR>
			<CFIF isdefined("elementMenuText")>
				<TD><H2>#htmleditformat(elementMenuText)#</H2></TD>
			<CFELSE>
				<TD><H2>phr_DocumentLibraryTitle</H2></TD>
			</CFIF>
			</TR>
		</TABLE>
	</cfif>
</cfif>

<CFIF (IsDefined("FiletypeGroupID") AND FiletypeGroupID GT 0) OR (IsDefined("DontListOneGroup"))>
	<cfif not SuppressHeaders>
		<TABLE BGCOLOR="White">
			<!--- 2005-08-31 SWJ Changed the logic so that headers only show if suppressHeaders=false --->
				<TR>
				<TD>
				<CFIF translate>
					<CFIF isdefined("AlternativeHeader")>
						phr_#htmleditformat(AlternativeHeader)#
					<CFELSE>
						phr_FileTypeGroup_Intro_#htmleditformat(getTypeText.FileTypeGroupID)#
					</CFIF>
				<CFELSE>
					<CFIF isdefined("AlternativeHeader")>
						#htmleditformat(AlternativeHeader)#
					<CFELSE>
						#HTMLEditFormat(getTypeText.Intro)#
					</CFIF>
				</CFIF>
				</TD>
			</TR>
			<TR>
				<TD>
				<CFIF translate>
					<!--- 2005-08-31 SWJ Changed the logic so that headers only show if suppressHeaders=false --->
					<CFIF not SuppressHeaders and isdefined("AlternativeClick") and AlternativeClick neq "">
						phr_#htmleditformat(AlternativeClick)#
					<CFELSE>
						phr_FileTypeGroup_Click_#htmleditformat(getTypeText.FileTypeGroupID)#
					</CFIF>
				<CFELSE>
					<!--- 2005-08-31 SWJ Changed the logic so that headers only show if suppressHeaders=false --->
					<CFIF not SuppressHeaders and isdefined("AlternativeClick") and AlternativeClick neq "">
						#htmleditformat(AlternativeClick)#
					<CFELSE>
						Click on the underlined #HTMLEditFormat(getTypeText.Heading)# name to download the #HTMLEditFormat(getTypeText.Heading)#.
					</CFIF>
				</CFIF>
				<BR><BR>
				</TD>
			</TR>


			<!--- <CFIF GetFTG.recordcount EQ 1 AND NOT IsDefined("DontListOneGroup")>
			<TR><TD Colspan=2><a href="javascript:document.getFileForm.FileTypeGroupID.value=0;document.getFileForm.submit()">phr_#FileListName#filelistingback</a></TD></TR>
			</CFIF> --->
		</TABLE>
	</cfif>
</CFIF>
</cfoutput>

<!--- 2004-04-01 AR Added code to make sure that the file form is displayed only once. --->
<cfparam name="showFileListForm" default="true">
<cfif showFileListForm>
<cfoutput>
<FORM METHOD="post" NAME="getFileForm" ID="getFileForm">
	<input type="hidden" name="fileID" value="0">
	<CFIF IsDefined("FileTypeGroupID")>
		<CF_INPUT type="Hidden" NAME="FileTypeGroupID" value="#FileTypeGroupID#">
	<CFELSE>
		<input type="Hidden" NAME="FileTypeGroupID" value="0">
	</CFIF>
	<CFIF IsDefined("ElementID")>
		<CF_INPUT type="hidden" name="ElementID" value="#ElementID#">
	</CFIF>
</FORM>

<!--- 2006-02-27 WAB changed from post to get.  Seemed to solve a problem when openwin.js failed to get focus of the new win.  The post would then not work (no fields were passed) but the get does work.  Can't explain it --->
<cfset fileFormV2Action = "/fileManagement/FileGet.cfm">   <!--- removed V2 - fileget.cfm includes V2 --->

<cfif isDefined("isWebServiceCall") and isWebServiceCall>
	<cfset fileFormV2Action = "#fileFormV2Action#?#session.urltoken#">
</cfif>
<!--- <script>alert("#fileFormV2Action#");</script> --->
<FORM METHOD="get" NAME="getFileFormV2" action = "#fileFormV2Action#" target = "FileDownload">
	<input type="hidden" name="fileID" value="0">
	<CF_INPUT type="hidden" name="openWinType" value="#openWinType#">
	<CFIF IsDefined("FileTypeGroupID")>
		<CF_INPUT type="Hidden" NAME="FileTypeGroupID" value="#FileTypeGroupID#">
	<CFELSE>
		<input type="Hidden" NAME="FileTypeGroupID" value="0">
	</CFIF>
	<CFIF IsDefined("ElementID")>
		<CF_INPUT type="hidden" name="ElementID" value="#ElementID#">
	</CFIF>
</FORM>
</cfoutput>
<cfset showFileListForm = false>
</cfif>

<CFIF (IsDefined("FiletypeGroupID") AND FiletypeGroupID GT 0) OR (IsDefined("DontListOneGroup"))>
	<cfset extraCells = 2>
	<cfif not SuppressFileName>
		<cfset extraCells = extraCells + 1>
	</cfif>
	<cfif showLastRevisedDate>
		<cfset extraCells = extraCells + 1>
	</cfif>
	<div class="fileList">
	<!--- 2006/01/04 - GCC - removed this row for thumbnail mode
		  2014/06/20 - SB - Removed Tables and added Bootstrap Classes --->

	<cfif not showThumbnail>

			<cfif not SuppressDownloadText><!--- 2005-08-31 SWJ Changed the logic --->
			<cfoutput>phr_#htmleditformat(FileListName)#DocLib_Name</cfoutput>
			</cfif>

			<cfif not SuppressFileName><!--- 2005-08-31 SWJ Changed the logic --->
				<cfoutput>phr_#htmleditformat(FileListName)#DocLib_FileName</cfoutput>
			</cfif>

			<cfif showLastRevisedDate><!--- 2005-08-31 SWJ Added the logic --->
				<CFIF translate>phr_DocLib_LastRevised<CFELSE>Last revised</CFIF>
			</cfif>

	</cfif>

		<!--- increment to manage row amount --->
		<cfset incr = 1>
		<!--- to ensure tr is only written once --->
		<cfset trWritten = false>

		<cfset colwidth =  int(100/defaultFileListColumns)>
		<!--- new file has been created in order to turn on and off grouping --->
		<!--- START: AXA 2015-02-03 Case 443532 cleaned up code and removed old if/else for redundant code
			  allow user to pass in paramters to sort query, added if to only sort if paramters were provided
		--->
		<cfquery name="GetResults" dbtype="query">
			Select * from GetResults where fileExists=1 OR source='url'
			<!--- START: 2015-02-03	AXA Case 443532 --->
			<cfif len(trim(FileListByGroup)) GT 0>
			order by <cf_queryObjectName value="#FileListByGroup#">
			<cfelseif len(trim(fileSortOrder)) GT 0>
			order by <cf_queryObjectName value="#fileSortOrder#">
			</cfif>
			<!--- END: 2015-02-03	AXA Case 443532 --->
		</cfquery>

		<cfif len(trim(FileListByGroup)) GT 0>
            <cfoutput query="GetResults" group="#FileListByGroup#">
				<div>#htmleditformat(evaluate(FileListByGroup))#</div>
				<cfoutput>
					<cfinclude template = "fileListDisplay.cfm">
				</cfoutput>
			</cfoutput>
		<cfelse>
			<cfoutput query="GetResults">
				<cfinclude template = "fileListDisplay.cfm">
			</cfoutput>
		</cfif>
		<!--- END: AXA 2015-02-03 Case 443532 --->
	</div>
</CFIF>

</cf_translate>