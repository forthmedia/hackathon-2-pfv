<!---
File name:			eventListUnknownUser.cfm
Author:				SWJ
Date started:		2005-02-13

Description:		Displays list of events which can be registered by the unknown User
				a) eventDetails.eventStatus = agreed
				b) eventCountry.countryid = users countryID
				c) eventDetails.AllocatedStartDate/PreferredStartDate = in the future
				d) eventDetails.invitationType = open
				e) if eventDetails.invitationType = invOnly then the user needs to
					be in the eventFlagdata
		requires session.eventgroup to be set on first entry to the frame set
		requires frmcountryid to be set from previous page

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

20.10.04	ICS	- added sort order for events
Jan 05		JC	- added Teams functionality
2005-06-14	AJC - added events per row display factor
2011/05/04 WAB  LID 6433 change session.registerThisEventFlagTextID to a relayCurrentUser key
2011/11/02 NYB  Added CF_PARAMs for automatic editor
2011/11/05 NJH	removed some CF_PARAMs
2012/04/10 WAB	Some cfparams still has extra attributes which needed removing
2015/02/12 SB	Fixed css layout
Possible enhancements:

 --->
<cfset showThisPage = true>  <!--- set to false if we go off to some other page during processing --->
<cfset RegDoneCheckForProducts = false> <!--- this is set to true when the registration is complete and we need to check if a payment needs to be made
											needed a variable 'cause there are two routes to get to this point --->

<cf_include template="\code\cftemplates\eventINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\eventINI.cfm")>
	<cfinclude template="/code/cftemplates/eventINI.cfm">
</cfif> --->
<!---  --->
<!--- This isn't needed if we generate a generic/default eventINI.cfm --->
<cf_param name="showPageHeading" type="boolean" default="No"/>
<cf_param name="showPageWelcomeText" type="boolean" default="No"/>
<cf_param name="noMoreRegistrations" type="boolean" default="No"/>
<cfparam name="URL.pp" default=""><!--- this is to initialise the magic number if it has come in on the URL variable as this is passed onto the next screen in a form variable --->
<cf_param name="hideEventList" default="No" type="boolean"/> <!--- this hides the main eventList itself --->
<cf_param name="hideEventName" default="No" type="boolean"/> <!--- this hides the event name that shows at the top of each event group --->
<cf_param name="hideEventDetailsLink" default="No" type="boolean"/> <!--- this hides the details link that pops up the screen with event details --->
<cfparam name="eventOptionsScreen" default=""> <!--- this hides the details link that pops up the screen with event details --->
<cf_param name="eventsPerRow" default="1"/><!--- this determines the number of events per row, 1 being the default --->
<cf_param name="eventdateformat" default="DD-MMM-YY"/>
<cf_param name="showEventDetail" default="Yes" type="boolean"/>
<cf_param name="showEventVenue" default="Yes" type="boolean"/>
<cf_param name="showEventLocation" default="Yes" type="boolean"/>
<cfparam name="showDataSource" default="database" > <!--- if database then lists columns, otherwise works off a phrase --->
<cf_param name="tableclass" default="eventListTable"/>
<cf_param name="eventGroupID" label="Event Group" default="0" validvalues="select eventGroupId as value,eventName as display from eventGroup order by display" nullText="All"/>

<cf_param name="maxEventRegistrations" type="numeric" default="100000"/>

<CFIF IsDefined("gotoEID")and isDefined("FORM.frmRegister") and FORM.frmRegister eq "yes">
	<!--- WAB 2011/05/04 session information is lost when user goes through registration process, so must store in relayCurrentUser in a persistant key --->
	<cfset application.com.relayCurrentUser.storeUserData(keyname="registerThisEventFlagTextID",data=FORM.eventFlagTextId,keepAcrossUserChanges=true)>

	<!--- if the eventOptions screen isdefined session.setThisFlagTextID may need to be set  --->
	<cfif isDefined("eventOptionsScreen") and len(eventOptionsScreen) neq 0
		and fileexists("#application.paths.content#\event\#eventOptionsScreen#.cfm")>

		<cfinclude template="/content/event/#eventOptionsScreen#.cfm">

		<cfset session.setThisFlagTextID = StructNew()>
		<cfloop query="eventOptions">
			<cfif structKeyExists(FORM,"flag_#eventOptions.flagType#_#eventOptions.flagTextID#")>
				<cfscript>
					structInsert(session.setThisFlagTextID,eventOptions.flagTextID,eventOptions.flagTextID);
				</cfscript>
				<!--- <cfoutput>#eventOptions.flagTextID#</cfoutput> --->
			</cfif>
		</cfloop>
	</cfif>

	<cfoutput>
	<SCRIPT type="text/javascript">
		document.location.href = '/et.cfm?eid=#jsStringFormat(gotoEID)#&pp=#jsStringFormat(FORM.pp)#&#jsStringFormat(session.urltoken)#';
	</script>
	</cfoutput>
</CFIF>


<cfif showThisPage>

	<cfscript>
		qEventListUnknownUser = application.com.relayEvents.getEventListUnknownUser(eventGroupID = eventGroupID);
	</cfscript>

	<cf_translate>

	<cfoutput>
	<cf_includejavascriptonce template = "/javascript/openwin.js">
	<SCRIPT LANGAUGE="Javascript">

		function eventRegister(eventFlagTextId) {
			form = document.registerForm;
			form.eventFlagTextId.value = eventFlagTextId;
			form.submit();
		}

		jQuery(document).ready(function () {
			jQuery('.fancyboxLink').fancybox({
				helpers: {
				   overlay: {
				      locked: true
				   }
				},
				autoSize: true,
				scrolling: 'auto',
				type:'ajax'
			});
		});

	</SCRIPT>
	</cfoutput>

	<cfif qEventListUnknownUser.Recordcount gt 0>

	<cfoutput>
		<div id="#tableclass#">
			<form action="/et.cfm?#request.query_string#&#session.urltoken#" method="post" name="registerForm" id="registerForm">
				<CF_INPUT type="hidden" name="pp" value="#URL.pp#"></cfoutput>
		  	<cfif showPageHeading eq "yes">
				<h1>phr_Current_Events</h1>
			</cfif>
			<cfif showPageWelcomeText eq "yes">
				<p>phr_Welcome_Events</p>
			</cfif>
			<cfif isDefined("message") and message neq "">
				<p class="messagetext"><strong class="messagetext"><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></strong></p>
			</cfif>

			<cfif isdefined('maxEventRegistrations')>
				<CFQUERY NAME="GetEventsRegistered" datasource="#application.sitedatasource#">
				SELECT
					eventflagdata.FlagID
				FROM eventflagdata INNER JOIN
				EventDetail ON eventflagdata.FlagID = EventDetail.FlagID
				WHERE
					(eventflagdata.EntityID = #request.relayCurrentUser.personid#)
					<CFIF IsDefined("EventGroupID") AND EventGroupID GT 0>
			 			AND eventGroupID =  <cf_queryparam value="#EventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFIF>
					and (eventflagdata.regStatus in ('RegistrationStarted','RegistrationSubmitted','Confirmed'))
				</CFQUERY>

				<cfif GetEventsRegistered.Recordcount gt 0 and GetEventsRegistered.Recordcount gte maxEventRegistrations>
					<cfset noMoreRegistrations = "Yes">
				</cfif>

			</cfif>

			<!--- use hideEventList if not set to yes --->
			<cfif hideEventList eq "no">
			<cfoutput query="qEventListUnknownUser" group="eventName">

				<cfif hideEventName eq "no">
					<h2>#htmleditformat(eventName)#</h2>
				</cfif>
				<cfoutput>
					<div class="form-group row">
						<cfscript>
							// If the event detail link is shown set col widths
							if(showEventDetail)
							{
								eventcol1=2;
								eventcol2=6;
								eventcol3=4;
								variables.title=title;
							}
							else
							{
								eventcol1=0;
								eventcol2=8;
								eventcol3=4;
								variables.title="<A HREF=javascript:eventDetails('#flagTextID#')>#title#</A>";
							}
						</cfscript>
						<cfif showEventDetail>
							<div class="col-xs-12 col-sm-#eventcol1#">
								<cfif hideEventDetailsLink eq "no">
									<A class="fancyboxLink" HREF="/eventsPublic/eventDetails.cfm?frmeventFlagTextID=#flagTextID#">phr_Details</A>
								</cfif>
							</div>
						</cfif>
						<cfif showDataSource is "Database">
							<div class="col-xs-12 col-sm-#eventcol2#">
								<p class="detailsTitle">#htmleditformat(variables.title)#</p>
								<cfif showEventLocation>
									<p class="detailsLocation">#htmleditformat(Location)#</p>
								</cfif>
								<cfif showEventVenue>
									<p class="detailsVenueRoom">#htmleditformat(venueRoom)#</p>
								</cfif>
								<cfif not isDefined("hideUnableToAttend") or (isDefined("hideUnableToAttend") and not hideUnableToAttend)>

		                                <!--- changed radio button to checkbox --->
		                                <div class="checkbox">
		                                    <label>
		                                        <input type="checkbox" class="checkbox" name="eventFlagTextID" value="unableToAttendEvent" checked>phr_unableToAttendEvent
		                                    </label>
		                                </div>

								</cfif>
							</div>
							<div class="col-xs-12 col-sm-#eventcol3#">
								<B><CFIF startDate eq "">phr_DateTBC<cfelse>#dateformat(startDate,"#htmleditformat(eventdateformat)#")#</CFIF></B>
							</div>
						<cfelse>
							<div class="col-xs-12 col-sm-#eventcol2+eventcol3#">
								phr_Event_#htmleditformat(flagTextID)#
							</div>
						</cfif>
						</div>
						<div class="form-group row">
							<div class="col-xs-12">
								<div class="col-xs-12 radio">
									<div class="pull-right">
										<CFIF startdate neq "" and regStatus eq "phr_Register" and EstAttendees neq "" and NumberRegistered lt EstAttendees>
											<cfif noMoreRegistrations eq "Yes">
												<label class="pull-right">phr_EV_RegistrationUnavailable</label>
											<cfelse>
												<cfif not listFind(valuelist(GetEventsRegistered.flagID),"#qEventListUnknownUser.flagID#")>
													<a class="btn btn-primary" href="##" onclick="jQuery('input[name=eventFlagTextId]').attr('value','#flagTextID#');jQuery('form##registerForm').submit();">
														phr_register
													</a>
												<cfelse>
													<label class="pull-right">phr_EV_alreadyRegistered</label>
												</cfif>
											</cfif>
										<cfELSEIF startdate neq "" and regStatus eq "Confirmed">
											<label class="pull-right">phr_EV_EventList_Confirmed</label>
										<CFELSEIF startdate neq "" and EstAttendees neq "" and NumberRegistered gte EstAttendees>
											<label class="pull-right">phr_EV_FullyBooked</label>
										</CFIF>
									</div>
								</div>
							</div>
						</div>



						<!--- the eventOptions screen should set up a query object with the additional flags that need to be set  --->
						<cfif isDefined("eventOptionsScreen") and len(eventOptionsScreen) neq 0
							and fileexists("#application.paths.content#\event\#eventOptionsScreen#.cfm")>

							<cfinclude template="/content/event/#eventOptionsScreen#.cfm">

							<!--- this tells the template to process these flags when the screen is submitted --->
							<input type="hidden" name="processEventOptions" value="yes">

							<cfloop query="eventOptions">
								<cfif eventOptions.flagType eq "checkbox">
									<div class="form-group row">
										<div class="col-xs-12 checkbox">
											<label>phr_#htmleditformat(eventOptions.phraseTextID)#<CF_INPUT type="checkbox" class="checkbox" name="flag_#eventOptions.flagType#_#eventOptions.phraseTextID#" value="1"></label>
										</div>
									</div>
								<cfelseif eventOptions.flagType eq "radio">
									<div class="form-group row">
										<div class="col-xs-12 radio">
											<label>phr_#htmleditformat(eventOptions.phraseTextID)#<CF_INPUT type="radio" class="radio" name="registerButton" value="flag_#eventOptions.flagType#_#eventOptions.phraseTextID#"></label>
										</div>
									</div>
								</cfif>
							</cfloop>
						</cfif>

				</cfoutput>
			</CFOUTPUT>
		</cfif>

		<cfoutput>

			<CF_INPUT type="hidden" name="frmPersonID" value="#request.relayCurrentUser.personid#">
			<input type="hidden" name="frmRegister" value="yes">
			<CF_INPUT type="hidden" name="eid" value="#elementID#">
		<CFIF IsDefined("goToEID")>
			<CF_INPUT type="hidden" name="goToEID" value="#goToEID#">
		</CFIF>
			<input type="hidden" name="eventFlagTextId" value="null">
		<CFIF IsDefined("EventGroupID")>
			<CF_INPUT type="hidden" name="EventGroupID" value="#EventGroupID#">
		</CFIF>
	<!--- Not generic - need to find a better way ofhandling these --->
		<CFIF IsDefined("fieldName")>
			<CF_INPUT type="hidden" name="fieldName" value="#fieldName#">
		</CFIF>
		<CFIF IsDefined("fieldValue")>
			<CF_INPUT type="hidden" name="fieldValue" value="#fieldValue#">
		</CFIF>
		</FORM>
	</div>
	</cfoutput>


	<cfelse>
		<p>phr_EV_NoAvailableEvents</p>
	</cfif>

	</cf_translate>
</cfif>

