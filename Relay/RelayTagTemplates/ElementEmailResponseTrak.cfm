<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		elementEmailResponseTrak.cfm
Author:			Simon WJ
Date created:	03 August 2001

	Objective - To trak email submitals.
		
	Syntax	  -	It should be submitted to via a form tag i.e.
    <FORM ACTION="" METHOD="post" NAME="mainForm" TARGET="_blank">
		<INPUT TYPE="hidden" NAME="xxxxFirstName" VALUE="<<<FirstName>>>">
		<INPUT TYPE="hidden" NAME="xxxxLastName" VALUE="<<<LastName>>>">
		<INPUT TYPE="hidden" NAME="xxxxsiteName" VALUE="<<<siteName>>>">
		<INPUT TYPE="hidden" NAME="xxxxEmail" VALUE="<<<email>>>">
		<INPUT type="hidden" name="elementID" value="398">
		<INPUT TYPE="hidden" NAME="xxxxmessage" VALUE="Thanks for your interest.">
		<INPUT TYPE="hidden" NAME="xxxxcommid" VALUE="<<<commid>>>"> - MANDATORY
		<INPUT TYPE="hidden" NAME="xxxxPID" VALUE="<<<personID>>>"> - MANDATORY
		<INPUT TYPE="hidden" NAME="xxxxTelephone" VALUE="<<<telephone>>>">  - MANDATORY
		<INPUT TYPE="hidden" NAME="Subject" VALUE="Network Telecomms Email Interest">  - MANDATORY
		
		Note: the xxxVarname format is used so that variables may or may not be processed in parts of this template. E.g. when we save 
			  this data to the database, we already need 

		You can also add additional fields in the form:
		<input type="checkbox" name="want_to_know_more" value="yes">
		When this template processes them the underscores are replaced by spaces in the email sent to the notification list.
				
	Return Codes - none

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-09-25			SWJ			Upgraded to cope with 
2001-08-03			SWJ			First version created
2001-11-13			SWJ			Added showValues section and the unsubscribe section
2008/06/30     SSS      Added Nvarchar Support to feeback field

Enhancement still to do:

1.	We need to call a template that will insert a contact history record
2.	We need to work out a secure method that traps abuse using magicCheckSum
3.  We need to work out a secure method that will make sure that the form 
	is being posted to by a valid email.
4.	We need to call screen/updateData.cfm with an appropriate follow on form
5.	We need to work out a method of notifying someone via SMS
6.  Need to work out how to show a different screen message per email type.  e.g. for
	a response to an event invite the message says "Thanks for registering"
7.  Needs to capture some of the data in fiednames and add it as a transaction.
8.	

--->

<!--- ***************************************************************
		get the persons details --->
<CFPARAM NAME="saveTime" TYPE="date" DEFAULT="#now()#">
<CFPARAM NAME="informList" TYPE="string" DEFAULT="#application.adminEmail#">
<CFIF isDefined("xxxxCommid") and isDefined("xxxxPID")>
	<!--- <CFINCLUDE TEMPLATE="commDetailTask.cfm"> --->
</CFIF>

<CFPARAM NAME="Subject" TYPE="string" DEFAULT="Marketing Email Response">

<!--- ***************************************************************
		get the persons details --->
<CFIF isDefined('xxxxPID') and isNumeric(xxxxPID)>
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#" DBTYPE="ODBC">
		select p.email, p.firstName, p.lastName, l.siteName
		from person p inner join location l
		on p.locationID = l.locationID
		where personID = #xxxxPID#
	</CFQUERY>
	<CFSET pid = xxxxPID>
</CFIF>

<!--- ***************************************************************
	 set up the email response to the informList --->
<CF_MAIL TO="#informList#"
        FROM="#application.adminEmail#"
		BCC="#application.cBccEmail#"
        SUBJECT="#Subject#"
        TYPE="HTML"
		>
<P><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2"><B>Information from Email Response Tracker</B></FONT></P>
<cfset ResponseDetails = "<B>RESPONSE DETAILS</B><BR><HR WIDTH='60' SIZE='1'>">
<CFIF isDefined('FieldNames')>
	<TABLE>
		<CFLOOP INDEX="i" LIST="#FieldNames#">
		  	<cfif evaluate(i) neq "" and i neq "xxxxsubmit" and i neq "xxxxmessage">
			<!--- suppress these values as we don't want them in the email --->
				<TR>
					<TD><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">#replacenoCase(replaceNoCase(i,"_"," ","ALL"),"xxxx","","ALL")#:</FONT></TD> 
					<TD><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">#replacenoCase(replaceNoCase(evaluate(i),"_"," ","ALL"),"xxxx","","ALL")#</FONT></TD>
				</TR>
				<!--- if i doesn't start with xxxx then add this value to ResponseDetails --->
				<CFIF left(i,4) neq 'xxxx'>
					<cfset temp = "<b>" & replacenoCase(replaceNoCase(i,"_"," ","ALL"),"xxxx","","ALL") &"</B>: "& replacenoCase(replaceNoCase(evaluate(i),"_"," ","ALL"),"xxxx","","ALL") &"<br>" >
					<cfset ResponseDetails = ResponseDetails & temp>
				</CFIF>
			</CFIF>
		</CFLOOP>
	</TABLE>
<CFELSE>
	<FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">
		No fieldNames set<br>
		Their IP address was #REMOTE_ADDR#
	</FONT>
</CFIF>
</CF_MAIL>
		
<!--- ***********************************************************************
The way SMS could work is that it could SMS the owner of this account

<CFMAIL TO="07785268111@bulletinMail.com"
        FROM="info@foundation-network.com"
        SUBJECT="FNL Web enquiry">FNL Web enquiry from 
		</CFMAIL> ---> 


<!--- ***************************************************************
	 output message to show on the screen  --->
<TABLE BORDER="0" WIDTH="100%">
  <TR> 
    <TD> 
      	<P><B>
			<cfif isDefined ('xxxxMessage') and xxxxMessage neq "">
				<CFOUTPUT>#htmleditformat(xxxxMessage)# </CFOUTPUT>
				<CFELSE>Thank you for your response. 
			</CFIF>
			</B>
		</P>
     	<P>Please feel free to browse our site.</P>
    </TD>
  </TR>
  <!--- ************************************************************************
  For testing: set showValue to yes in order to see the fieldName settings ---> 
  <CFIF isDefined ('showValues') and showValues eq "yes">
	  <TR>
	  	<TD HEIGHT="150px" VALIGN="bottom" STYLE="font-size: xx-small; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">
	  		<CFLOOP INDEX="i" LIST="#FieldNames#">
			  	<cfif evaluate(i) neq "" and i neq "xxxxsubmit" and i neq "xxxxmessage">
				<!--- suppress the above values as we don't want to see them in the email --->
					<CFOUTPUT>#replacenoCase(replaceNoCase(i,"_"," ","ALL"),"xxxx","","ALL")#: 
					#replacenoCase(replaceNoCase(evaluate(i),"_"," ","ALL"),"xxxx","","ALL")#<BR></CFOUTPUT>
					
					<!--- if i doesn't start with xxxx then add this value to ResponseDetails --->
					<CFIF left(i,4) neq 'xxxx'>
						<cfset temp = "<b>" & replacenoCase(replaceNoCase(i,"_"," ","ALL"),"xxxx","","ALL") &"</B>: "& replacenoCase(replaceNoCase(evaluate(i),"_"," ","ALL"),"xxxx","","ALL") &"<br>" >
						<cfset ResponseDetails = ResponseDetails & temp>
					</CFIF>
				</CFIF>
			</CFLOOP>
	  	</TD>
	  </TR>
  </CFIF> 
</TABLE>

<!--- ***************************************************************
	 UPDATE COMMDETAIL RECORD: if commid and xxxxPID are set we can 
	 							update commDetail --->
<CFIF isDefined('xxxxCommid') and isNumeric(xxxxCommid) and isDefined('xxxxPID') and isNumeric(xxxxPID)>
	<CFIF isDefined('Unsubscribe_request') and Unsubscribe_request eq "Unsubscribe">
		<CFSET commDetailStatus = 41><!--- 41 = 'Unsubscribe Response' --->
	<CFELSE>
		<CFSET commDetailStatus = 40><!--- 40 = 'Email Marketing Response' --->
	</CFIF>
	<!--- check to see if their is a matching commDetail record --->
	<CFQUERY NAME="checkIDPair" datasource="#application.sitedatasource#" DBTYPE="ODBC">
		select Commid from commDetail where Commid = #xxxxCommid# and PersonID = #xxxxPID#
	</CFQUERY>
	<!--- if matching commDetail record found update it --->
	<CFIF checkIDPair.recordCount eq 1>
		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#" DBTYPE="ODBC">
			Update commdetail 
			  set feedback = N'#ResponseDetails#',
			  commStatusID = #commDetailStatus#,
			  lastUpdated = getDate(),
			  lastUpdatedBy = #xxxxPID#
			  where Commid=#xxxxCommid#
			and PersonID = #xxxxPID#
		</CFQUERY>
	</CFIF>	
	<CFSET pid = xxxxPID>
</CFIF>


<!--- ***************************************************************
		Unsubscribe people who click to be unsubscribed              
		                                                         --->
<CFIF isDefined('Unsubscribe_request') and Unsubscribe_request eq "Unsubscribe" and isDefined('xxxxPID') and isNumeric(xxxxPID)>
	<!--- Find out the unsubscribe flagID --->
	<CFQUERY NAME="getFlagID" datasource="#application.sitedatasource#" DBTYPE="ODBC">
		Select flagID from flag where flagTextID = 'NoCommstypeall'
	</CFQUERY>
	
	<CFQUERY NAME="insertBFD" datasource="#application.sitedatasource#" DBTYPE="ODBC">
		INSERT INTO BooleanFlagData
		(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
		SELECT #xxxxPID#, f.FlagID, #xxxxPID#, #Variables.saveTime#, #xxxxPID#, #Variables.saveTime#
	  	FROM Flag AS f
 		WHERE f.FlagID = #getFlagID.flagID#
		AND NOT EXISTS (SELECT bfd.FlagID FROM BooleanFlagData AS bfd 
			WHERE bfd.FlagID = f.flagID and bfd.EntityID = #xxxxPID#)
	</CFQUERY>
	
	<!--- ***************************************************************
		Send person an email telling them they've been unsubscribed      
		                                                         --->

	<CF_MAIL TO="#getPersonDetails.email#"
        FROM="#application.adminEmail#"
        SUBJECT="Unsubscribe Request - Acknowledged"
        TYPE="HTML"
		>
	You have been unsubscribed from all future emails from #application.clientName#.
	</CF_MAIL>
	
	<CFIF isDefined ('showValues') and showValues eq "yes">
		<TABLE>
		  <TR>
		  	<TD HEIGHT="150px" VALIGN="bottom" STYLE="font-size: xx-small; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">
			Unsubscribe flagID: <CFOUTPUT>#htmleditformat(getFlagID.flagID)#</CFOUTPUT></TD>
		  </TR>
		</TABLE>
	</CFIF>
</CFIF>



