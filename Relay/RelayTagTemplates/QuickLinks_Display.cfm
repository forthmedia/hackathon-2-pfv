<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	QuickLinks_Display.cfm - derived from Sony's SubItemsDisplay.cfm

	NJH 2007/03/13 - added recent searches to the display
	WAB 2007/09/24  - added recent searched to the UL section
WAB 2008/05/12  - changed ULList code to use use elementTextID if available
	SKP 2010/05/05 - added class to H2
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2015-09-14	ACPK	PROD2015-53	Replaced Sony-specific phrases with non-specific equivalents; Implemented functionality of More link; Changed qGetElements to return either all items or user-specified max rows
--->

<!--- include this for tracking clicks --->
<cf_includeJavascriptOnce template="/javascript/linktracker.js">
<cfparam name="headerClass" default="">

<cfif displayType is "block">
	<cfoutput>
		<table cellspacing="0" cellpadding="0" border="0" class="#dataGetMethod#" width="100%">
			<cfif header neq "">
				<tr>
		          <th>#htmleditformat(header)#</th>
		        </tr>
			</cfif>

			<tr>
				<td>
					  <table cellpadding="0" cellspacing="0">
							<!-- level one navigation item -->
							<cfloop query="qGetElements" startrow="1" endrow="#localRows#">
								<!--- 2006/01/12 - GCC check user has rights to the target element with linkBlock - hide if not --->
								<cfset show="true">
								<cfif dataGetMethod eq "linkblock">
	 								<cfscript>
										urlParameters = application.com.regexp.getParametersFromURLString(qGetElements.url);
										if (structKeyExists(urlParameters,"etid")) {
											elementID = structFind(urlParameters,"etid");
											show = application.com.relayElementTree.isElementInATree(elementTree =  request.CurrentElementTree, elementID = elementID);
										}
									</cfscript>
								</cfif>

								<cfif show>
									<!--- wab 2005-10-11 this is the javascript that runs to record the clicks against the link --->
									<cfif isDEfined("elementLinkID")> <!--- this is rather a nasty way of testing whether this query has this column in it - sometimes it does, sometimes it does not --->
										<cfset onClickJavascript = "return onClickLink('elementLink','#elementLinkID#', '#request.relaycurrentuser.personid#', '')">
									<cfelse>
										<cfset onClickJavascript = "">
									</cfif>

									<cfif elementLinkSchema is not "" and elementLinkTypeTextID neq "Element">
										<cfset elementCount = 1>
										<cfset elementLinkParams = "">
										<cfloop list="#elementLinkSchema#" delimiters="-" index="elementLinkVarName">
											<cfset temp = "qGetElements.elementLinkParam" & elementCount & "[" & qGetElements.currentRow & "]">
											<cfset elementLinkVarValue = #evaluate(temp)#>
											<cfset elementLinkParams = elementLinkParams & "&" &  #elementLinkVarName# & "=" & elementLinkVarValue>
											<cfset elementCount = elementCount + 1>
										</cfloop>
									</cfif>
									<cfif isexternalfile eq 1>
										<cfset link = "#qGetElements.url##elementLinkParams#">
									<cfelse>
										<cfif structKeyExists(qGetElements,"node")>
										   	<cfset link = "/?eid=#qGetElements.node##elementLinkParams#">
										</cfif>
									</cfif>
									<cfif dataGetMethod eq "Advert">
										<tr>
											<td><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" class="listLink" onClick="#htmleditformat(onClickJavascript)#" >phr_detail_element_#htmleditformat(linkImage)#</a></td>
										</tr>
									<cfelseif dataGetMethod EQ "updateReminder">
										<cfform action="#CGI.SCRIPT_NAME#?etID=#etID#" method="POST" name="Updateform" >
											<!--- This is used by one of the phrases to display days since update to the user --->
											<cfset form.UpdateDays = application.com.screens.NumberofDaysSincePersonAccessedScreen(screenID=screentocheck,personid=#request.relayCurrentUser.personID#)>
											<tr>
												<td>PHR_HEADLINE_ELEMENT_#qGetElements.node#</td>
											</tr>
											<tr>
												<td>PHR_DETAIL_ELEMENT_#qGetElements.node#</td>
											</tr>
											<tr>
												<td>
													<input type="submit" name="UpdateNow" value="phr_EXT_UpdateNow" class="submitbutton">
												</td>
											</tr>
										</cfform>
									<cfelseif dataGetMethod eq "recentSearches">
										<!--- NJH 2007/03/13 --->
										<tr>
											<cfif useIcons>
												<td>
													<img class="#iconClass#" src="#iconImage#" border=0>
												</td>
											</cfif>
											<td><a href="/?etid=searchResults&frmCriteria=#searchstring#">#left(searchstring,30)#</a></td>
										</tr>
									<cfelse>
										<cfif qGetElements.node neq elementBranchRootID>
											<tr>
											<cfif useIcons>
												<td>
													<img class="#iconClass#" src="#iconImage#" border=0>
												</td>
											</cfif>
												<td>
													<cfif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://"><!--- link out to new window --->
														<A HREF="#link#" CLASS="listLink" target="_blank" onClick="#onClickJavascript#">
													<cfelse><!--- link in to content frame within frameset --->
														<A HREF="#link#<cfif isdefined('getLinkFlagID') and getLinkFlagID neq "">&LFID=#htmleditformat(getLinkFlagID)#</cfif>" target="#htmleditformat(listLinkTarget)#" CLASS="listLink" onClick="#htmleditformat(onClickJavascript)#">
													</cfif>
													<cfif linkImage neq ""> <!--- NJH 2007/04/30 specify the link as an image rather than text--->
														<img class="#linkImageClass#" src="#linkImage#" border=0>
													<cfelseif elementLinkTypeTextID neq "Element">
														#listfirst(qGetElements.additionalDescription,"(")#
													<cfelse>
														phr_headline_element_#htmleditformat(qGetElements.node)#
													</cfif>
													</a>
												</td>
											</tr>
										</cfif>
									</cfif>
								</cfif>
							</cfloop>
						</table>
				  </td>
			</tr>
<!--- 	        <tr>
				<td align="right" valign="bottom">
					<!--- GCC 2005/02/22 - Commented out until it goes somewhere sensible --->
					<!---
					<cfif showMore eq 1><a href="#moreLinkURL#" target="#moreLinkTarget#"><img src="/content/sony1/sony1v1Images/bt_more_#lang#.gif" width="98" height="20" border="0" alt=""></a></cfif>
					--->
				</td>

	        </tr> --->


		</table>
	</cfoutput>

<cfelseif displayType is "list">

	<cfoutput>
		<table cellspacing="0" cellpadding="0" border="0">
			<cfloop query="qGetElements" startrow="1" endrow="#localRows#">
				<!--- wab 2005-10-11 this is the javascript that runs to record the clicks against the link --->
				<cfif isDEfined("elementLinkID")> <!--- this is rather a nasty way of testing whether this query has this column in it - sometimes it does, sometimes it does not --->
					<cfset onClickJavascript = "return onClickLink('elementLink','#elementLinkID#', '#request.relaycurrentuser.personid#', '')">
				<cfelse>
					<cfset onClickJavascript = "">
				</cfif>

				<cfif elementLinkSchema is not "" and elementLinkTypeTextID neq "Element">
					<cfset elementCount = 1>
					<cfset elementLinkParams = "">
					<cfloop list="#elementLinkSchema#" delimiters="-" index="elementLinkVarName">
						<cfset temp = "qGetElements.elementLinkParam" & elementCount & "[" & qGetElements.currentRow & "]">
						<cfset elementLinkVarValue = #evaluate(temp)#>
						<cfset elementLinkParams = elementLinkParams & "&" &  #elementLinkVarName# & "=" & elementLinkVarValue>
						<cfset elementCount = elementCount + 1>
					</cfloop>
				</cfif>
				<cfif isexternalfile eq 1>
				    <cfset link = "#qGetElements.url##elementLinkParams#">
				<cfelse>
					<cfset link = "/?eid=#qGetElements.node##elementLinkParams#">
				</cfif>
				<cfif dataGetMethod eq "Advert">
					<tr>
						<td><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" onClick="#htmleditformat(onClickJavascript)#" class="advertLink">phr_detail_element_#htmleditformat(linkImage)#</a></td>
					</tr>
				<cfelse>
					<cfif qGetElements.node neq elementBranchRootID>
						<tr>
							<td><p class="listLink"><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" class="listLink" onClick="#htmleditformat(onClickJavascript)#"><cfif elementLinkTypeTextID neq "Element">#listfirst(qGetElements.additionalDescription,"(")#<cfelse>phr_headline_element_#htmleditformat(qGetElements.node)#</cfif></a></p></td>
						</tr>
					</cfif>
				</cfif>
			</cfloop>
			<tr>
				<td align="right">
					<cfif showMore eq 1><p class="listLink"><a href="#moreLinkURL#" target="#moreLinkTarget#" class="listLink"><!--- NJH <img src="#spacerImage#" alt="" width="10" height="5" border="0"> ---><strong>Phr_Sony1_More</strong></a></p></cfif>
				 </td>
			</tr>
		</table>
	</cfoutput>

<cfelseif displayType is "ullist"><!--- JvdW 2007/07/23 - Added ul displaytype --->

	<cfoutput>
	<cfparam name="headerClass" default="">
    <cfif header neq "">
		  <h2 class="#htmleditformat(headerClass)#">#htmleditformat(header)#</h2>
		</cfif>

		<ul class="#htmleditformat(quickLinksClass)#">
			<!--- 2015-09-14	ACPK	PROD2015-53 Changed qGetElements to return either all items or user-specified max rows --->
			<cfif maxrows neq 0>
				<cfset totalItems = maxRows>
			<cfelse>
				<cfset totalItems = qGetElements.recordcount>
			</cfif>
			<cfloop query="qGetElements" startrow="1" endrow="#totalItems#">
				<!--- wab 2005-10-11 this is the javascript that runs to record the clicks against the link --->
				<cfif isDEfined("elementLinkID")> <!--- this is rather a nasty way of testing whether this query has this column in it - sometimes it does, sometimes it does not --->
					<cfset onClickJavascript = "return onClickLink('elementLink','#elementLinkID#', '#request.relaycurrentuser.personid#', '')">
				<cfelse>
					<cfset onClickJavascript = "">
				</cfif>

				<cfif elementLinkSchema is not "" and elementLinkTypeTextID neq "Element">
					<cfset elementCount = 1>
					<cfset elementLinkParams = "">
					<cfloop list="#elementLinkSchema#" delimiters="-" index="elementLinkVarName">
						<cfset temp = "qGetElements.elementLinkParam" & elementCount & "[" & qGetElements.currentRow & "]">
						<cfset elementLinkVarValue = #evaluate(temp)#>
						<cfset elementLinkParams = elementLinkParams & "&" &  #elementLinkVarName# & "=" & elementLinkVarValue>
						<cfset elementCount = elementCount + 1>
					</cfloop>
				</cfif>
				<cfif isexternalfile eq 1>
				    <cfset link = "#qGetElements.url##elementLinkParams#">
				<cfelseif dataGetMethod eq "recentSearches">
					<cfset link = "/?etid=searchResults&frmCriteria=#searchstring#">
				<cfelse>
					<!--- 	WAB 2008/05/12 added a test for elementtextIDs existing  --->
					<cfset link = "/?eid=#iif(qGetElements.elementTextID is not '','qGetElements.elementTextID','qGetElements.node')##elementLinkParams#">

				</cfif>


				<cfif dataGetMethod eq "Advert">
					<li><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" onClick="#htmleditformat(onClickJavascript)#" class="advertLink">phr_detail_element_#htmleditformat(linkImage)#</a></li>
				<cfelseif dataGetMethod eq "recentSearches">
					<li><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" onClick="#htmleditformat(onClickJavascript)#" class="listLink">#left(searchstring,30)#	</a></li>
				<cfelse>
					<cfif qGetElements.node neq elementBranchRootID>
						<li><a href="#link#" target="<cfif isexternalfile eq 1>_blank<cfelse>#htmleditformat(listLinkTarget)#</cfif>" class="listLink" onClick="#htmleditformat(onClickJavascript)#"><cfif elementLinkTypeTextID neq "Element">#listfirst(qGetElements.additionalDescription,"(")#<cfelse>phr_headline_element_#htmleditformat(qGetElements.node)#</cfif></a></li>
					</cfif>
				</cfif>
			</cfloop>

			<!--- 2015-09-14	ACPK	PROD2015-53	Implemented functionality of More link --->
			<cfif showMore EQ 1 and (maxrows GT localRows xor maxrows EQ 0) >
				<!--- 2015-09-14	ACPK	PROD2015-53	Replaced Sony-specific phrases with non-specific equivalents --->
				<li id="more"><p class="listLink"><a target="#moreLinkTarget#" class="listLink"><strong>Phr_QL_More</strong></a></p></li>

				<script language="JavaScript" type="text/javascript">
					var elements = document.querySelectorAll("ul.quickLinks li"); //select all items in list

					//on load, hide all list elements other than the first n (where n = #localRows#) and the More link (which is always last)
					function hideElements() {
						for (var i = #localRows#; i < elements.length - 1; i++) {
							elements[i].style.display = 'none';
						}
					}
					hideElements();

					//when the More link is clicked, display all list elements, then hide the More link itself
					function showAllElements() {
						for (var i = 0; i < elements.length; i++) {
							elements[i].style.display = 'list-item';
						}
						document.querySelector("##more").style.display = 'none';
					}
					document.querySelector("##more a").onclick = showAllElements;
				</script>
			</cfif>
		</ul>
	</cfoutput>
</cfif>

