<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History

Versions	Date			By		Description

1.1			2002-02-28		SWJ		The first version 

--->

<!--- this template should never be called directly, it should only be called after
		the following are defined:
		
		FileTypeGroupID (which can be defined as a URL or form variable
		edit_template (this needs to be defined so that FileTypeGroup 
						does not need to be passed as a parameter
						default value is FileEdit.cfm)
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
---->

<CFIF IsDefined("FORM.fileID") and FORM.fileID neq 0>
	<cfinclude template="/fileManagement/fileGet.cfm">
</CFIF>

<CFPARAM NAME="FileTypeGroupID" DEFAULT="5">
<cfparam name="hideFileTableTHrow" default="no">
<cfparam name="hideFileNameColumn" default="no">
<cfparam name="showLastRevisedColumn" default="no">
<cfparam name="showGroupHeading" default="no">

<CFQUERY NAME="GetResults" datasource="#application.sitedatasource#">
SELECT	f.FileID, ftg.heading,ftg.filetypegroupid as headingID,
			f.Name,
			f.Filename,
			f.Revision,
			ft.Type, ft.path,
			f.personID,
			p.FirstName + ' ' + p.LastName AS Fullname,
			SUM((rr.Permission/1) % 2) AS allow_read,
			allow_edit = case when f.personid = #request.relayCurrentUser.personid# then 1 else 0 END
	FROM files f INNER JOIN FileType ft ON ft.FileTypeID = f.FileTypeID
	INNER JOIN fileTypeGroup ftg on ftg.fileTypeGroupID = ft.fileTypeGroupID
	INNER JOIN person p ON p.PersonID = f.PersonID
	INNER JOIN RecordRights rr ON rr.RecordID = f.FileID
	INNER JOIN RightsGroup rg ON rr.UserGroupID = rg.UserGroupID
 	WHERE ft.FileTypeGroupID  in ( <cf_queryparam value="#replace(FiletypeGroupID,"-",",","ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND rr.Entity='files'
	  AND rg.PersonID = #request.relayCurrentUser.personid# 
	GROUP BY f.FileID, ftg.heading, ftg.filetypegroupid, f.Name, f.Filename, f.Revision, ft.Type, ft.path,
		p.FirstName + ' ' + p.LastName, f.personID  
		HAVING SUM((rr.Permission/1) % 2) > 0
		ORDER BY ftg.heading, f.Name
</CFQUERY>
<!--- <CFQUERY NAME="getTypeText" datasource="#application.sitedatasource#">
	SELECT Heading, Intro
	FROM FileTypeGroup
	WHERE FileTypeGroupID = #FileTypeGroupID#
</CFQUERY>
 --->

<SCRIPT type="text/javascript">
	function getFile(fileID) {
		form = document.getFileForm;
		form.fileID.value = fileID;
		form.submit();
	}

</script>

<cf_translate>
<TABLE BORDER="0" CELLSPACING="1" CELLPADDING="3">
<TR><TD colspan="3">&nbsp;</TD></TR>
<cfif hideFileTableTHrow eq "no">
<TR>
	<th>Phr_EFL_Name</th>
	<cfif hideFileNameColumn eq "no"><th>Phr_EFL_FileName</th></cfif>
	<cfif showLastRevisedColumn eq "yes"><th>Phr_EFL_LastRevised</th></cfif>
</TR>
</cfif>
<cfoutput query="GetResults" group="heading">
	<cfif showGroupHeading eq "yes"><tr><td colspan="2"><strong>phr_EFLGroupHeading#htmleditformat(headingID)#</strong></td></tr></cfif>
	<cfoutput>
		<CFSET thisFile = "#application.paths.content#\#GetResults.path#\#FileName#">
		<cfif fileExists(thisfile)>
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<TD><A HREF="javascript:getFile(#FileID#)" CLASS="smallLink">#htmleditformat(Name)#</A></TD>
				<cfif hideFileNameColumn eq "no"><TD>#htmleditformat(FileName)#</TD></cfif>
				<cfif showLastRevisedColumn eq "yes"><TD>#DateFormat(Revision, "dd-mmm-yyyy")#</TD></cfif>
					<!--- <TD>#DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#</TD> --->
			</TR>
		</cfif>
	</cfoutput>
</cfoutput>
</TABLE>
</cf_translate>
<FORM METHOD="post" NAME="getFileForm" ID="getFileForm">
	<input type="hidden" name="fileID" value="0">
</FORM>

