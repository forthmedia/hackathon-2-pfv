<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eventAutoRegisterPerson.cfm	
Author:				SWJ
Date started:		2005-08-26
	


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cf_param name="eventFlagTextID"/>
<cf_param name="phraseTextIDExtension" default=""/><!--- you can make the standard phrase  --->
<cf_param name="noEmailConfirmation" default="no" type="boolean"/>

<cfif not isDefined("variables.eventFlagTextID") or isDefined("URL.help") >
	<p><strong>Required:</strong> The event Auto Register Person tag requires the variable eventID which is the eventFlagTextID of
		the event you want to register a person for.  </p>
	<cfexit method="EXITTAG">
</cfif>

<cfif isDefined("URL.help") and structKeyExists(url,"help")>
	<p><strong>Description:</strong> This is a simple tag that can be added to any landing page and if provided 
	with a magic number via the urlString pp it will attempt to make the person's regStatus = RegistrationSubmitted.</p>
	<p>Optional: noEmailConfirmation can be set to yes if you do not want the standard email to be sent post registration.</p>
	<p>Optional: you can use phraseTextIDExtension extension if you want to vary the standard phrases used in the page.</p>
	<p>Syntax in text editor: <RELAY_EVENTAUTOREGISTERPERSON eventFlagTextID=eventName> </p>     
</cfif>

<!--- 2014/11/14	YMA	CORE-964 I have modified this tag to work for a logged in user by fetching their PP. --->
<cfif request.relaycurrentuser.ISLOGGEDIN gt 0>
	<cfset url.pp = application.com.login.getMagicNumber(entityTypeID=0,entityID = request.relaycurrentuser.personID)>
</cfif>

<CFIF isDefined("URL.pp")>
	<!--- check ID is valid.  MagicCheckSum requires person. --->
 	<CFModule TEMPLATE="/templates/MagicChecksum.cfm" person="#pp#">
	<cfif isDefined ("validCheckSum")and validChecksum EQ "True">
		<cfset regDetails = application.com.relayEvents.updateEventRegStatus(eventFlagTextID=eventFlagTextID,personid=actualPersonID)>
		<cfif isDefined("debug")>
			RegStatus after updateEventStatus <cfdump var="#regDetails#">
		</cfif>
		
		<cfif regDetails.recordCount gt 0 and regDetails.regStatus eq "RegistrationSubmitted">
			<!--- if this person is registered successfully for this event then give them so feedback & send email --->
			<cfoutput>
			<p><strong>#htmleditformat(regDetails.fullname)#</strong></p>
			<p>Phr_EV_ThankYouReg#htmleditformat(phraseTextIDExtension)#</p> 
			<cfif noEmailConfirmation eq "no"><p>Phr_EV_YouWillReceiveConfirmationEmailShortly</p></cfif>
			<p><strong>#htmleditformat(regDetails.eventName)#</strong></p>
			</cfoutput>
		
			<cfif noEmailConfirmation eq "no">
				<cfset frmPersonID = actualPersonID>
				<cfset eventFlagTextID = variables.eventFlagTextID>
		 		<cfinclude template="/eventsPublic/mailConfirmation.cfm">
			</cfif>
			
		<cfelse>
			<p>phr_EV_SorryRegistrationInvitationOnly</p>
		</cfif>
		
	</cfif>
</cfif>

<cfif isDefined("variables.eventFlagTextID") and isDefined("debug")>
	<cfset x = application.com.relayEvents.validateEventFlagTextID(eventFlagTextID=eventFlagTextID)>
	<cfdump var="#x#">
</cfif>
