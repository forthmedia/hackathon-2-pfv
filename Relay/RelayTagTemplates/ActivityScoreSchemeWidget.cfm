
<cf_includeCssonce template="/styles/activityScoreWidget.css">
<cf_includejavascriptonce template="/javascript/activityScoreWidget.js">


<cf_param label="Scheme" name="SchemeID" required="true" validValues="func:com.activityScore.getActiveSchemesForValidValues()" display="title" value="scoreRuleGroupingID"/>
<cf_param label="Show Rank Within Organization" name="showRank" default="true" type="boolean" />

<!--- AS Styling params --->
<cf_param label="Widget Style" name="widgetstyle" default="Classic" validValues="Classic,Ribbon,Rosette" />
<cf_param label="Widget Theme" name="widgettheme" default="Blue" validValues="Blue,Green,Gold,Silver,Bronze,Custom" />
<cf_param label="Theme Colour" name="themecolour" default="#application.com.settings.getSetting('portalStylingOptions.General.primaryColor')#" placeholder="e.g. FF22BC (Only used when Custom theme is selected!)"/>

<cfset classname = "">
<cfset secondarythemecolour = "">
<cfset lightercolour = "">
<cfset darkercolour = "">

<cfif #themecolour# neq "">	
	<cfif left(themecolour,1) neq "##">
		<cfset classname = Insert("a", themecolour, 0) >
		<cfset themecolour = "##" & themecolour>
	<cfelse>
		<cfset classname = Insert("a", RemoveChars(themecolour, 1, 1), 0)>
	</cfif>
	<cfset secondarythemecolour = application.com.colourManipulator.ColorShade(#themecolour#, 50)>
	<cfset lightercolour = application.com.colourManipulator.ColorShade(#themecolour#, 75)>
	<cfset darkercolour = application.com.colourManipulator.ColorShade(#themecolour#, -50)>
	<cfset themecolourrgb = application.com.colourManipulator.HexToRGB(#themecolour#)>
	<cfset secondarycolourrgb = application.com.colourManipulator.HexToRGB(#secondarythemecolour#)>
	<cfset lightercolourrgb = application.com.colourManipulator.HexToRGB(#lightercolour#)>
	<cfset darkercolourrgb = application.com.colourManipulator.HexToRGB(#darkercolour#)>
</cfif>	

<cfif request.relayCurrentUser.ISLOGGEDIN>
	<cfscript>
		schemeReportInOrg=application.com.activityScore.getScoresForEntitiesWithinOrganisationInSchemeByRule(
			schemeID=SchemeID,organisationID=request.relayCurrentUser.organisationID);
		//grab the score of this entity (i.e. person or location) to both display and compare to other entities in the org.
		if(schemeReportInOrg.scoringEntityTypeID EQ application.entityTypeID.person){
			entityID=request.relayCurrentUser.personID;
		}else if (schemeReportInOrg.scoringEntityTypeID EQ application.entityTypeID.location){
			entityID=request.relayCurrentUser.locationID;
		}else if (schemeReportInOrg.scoringEntityTypeID EQ application.entityTypeID.organisation){
			entityID=request.relayCurrentUser.organisationID;
			showRank=false; //showing rank when its at org level doesn't make sense as it'll always be 1 of 1
		}
	</cfscript>
	
	<cfquery dbtype="query" name="scoreOfThisEntity">
		select score
		from schemeReportInOrg.entityLevelReport
		where entityID = #entityID# <!---QoQ so no need for SQL injection protection --->
	</cfquery>
	
	<cfset score=scoreOfThisEntity.score>
	<cfset totalEntities=schemeReportInOrg.entityLevelResult.RecordCount>
	<cfif showRank>
		<cfquery dbtype="query" name="rankOfThisEntity">
			select count(1) as Rank
			from schemeReportInOrg.entityLevelReport
			where 
			score > #scoreOfThisEntity.score# <!---QoQ so no need for SQL injection protection --->
			or entityID = #entityID# 
		</cfquery>
		<cfset rank=rankOfThisEntity.rank>
	</cfif>
<cfelse>
	<cfset score=0>
	<cfset totalEntities=0>
	<cfset rank=0>
</cfif>

<!--- ------------------------------------------------------------------------------------
	USER SPECIFIED STYLING COLOUR
------------------------------------------------------------------------------------ --->
<cfif (#WidgetStyle# eq "Classic") and (#widgettheme# eq "Custom") and (#themecolour# neq "")>
	<cfoutput>		
		<style>
			.Classic.Custom.#classname# .scheme-title,
			.Classic.Custom.#classname# .scheme-score {
				background-color: #themecolour# !important;
			}
		</style>
	</cfoutput>
<cfelseif (#WidgetStyle# eq "Ribbon") and (#widgettheme# eq "Custom") and (#themecolour# neq "")>
	<cfoutput>
		<style>
			.Ribbon.Custom.#classname# .scheme-wrapper{
			background-color: #themecolour# !important;
			box-shadow: 1px 1px 10px #themecolour# !important;}
			.Ribbon.Custom.#classname# .scheme-score h1 {background-color: #secondarythemecolour# !important;}
			.Ribbon.Custom.#classname# .scheme-score h1:before, 
			.Ribbon.Custom.#classname# .scheme-score h1:after {
			border: 1.5em solid #secondarythemecolour# !important;}
			.Ribbon.Custom.#classname# .scheme-score h1 strong:before, 
			.Ribbon.Custom.#classname# .scheme-score h1 strong:after {
			border-color: #themecolour# transparent transparent transparent !important;}
			.Ribbon.Blue .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Blue .scheme-score h1:after {border-right-color: transparent !important;}
			.Ribbon.Green .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Green .scheme-score h1:after {border-right-color: transparent !important;}
			.Ribbon.Gold .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Gold .scheme-score h1:after {border-right-color: transparent !important;}
			.Ribbon.Silver .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Silver .scheme-score h1:after {border-right-color: transparent !important;}
			.Ribbon.Bronze .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Bronze .scheme-score h1:after {border-right-color: transparent !important;}
			.Ribbon.Custom.#classname# .scheme-score h1:before {border-left-color: transparent !important;}
			.Ribbon.Custom.#classname# .scheme-score h1:after {border-right-color: transparent !important;}			
		</style>
	</cfoutput>
<cfelseif (#WidgetStyle# eq "Rosette") and (#widgettheme# eq "Custom") and (#themecolour# neq "")>
	<cfoutput>
		<style>
			.Rosette.Custom.#classname# .strip {background: #themecolour#;}
			.Rosette.Custom.#classname# .strip:after {background: #themecolour#;}
			.Rosette.Custom.#classname#,.Rosette.Custom.#classname# .strip,
			.Rosette.Custom.#classname# .strip:after,
			.Rosette.Custom.#classname# .strip:before {
			  background: #secondarythemecolour#;
			  background: linear-gradient(45deg, transparent 40%, rgba(113, 113, 113, 0.28) 40%, rgba(#secondarycolourrgb#, 0.85) 60%, transparent 60%), linear-gradient(rgba(#themecolourrgb#, 0.6), rgba(#themecolourrgb#, 0.6));
			  background: -webkit-linear-gradient(45deg, transparent 40%, rgba(113, 113, 113, 0.28) 40%, rgba(#secondarycolourrgb#, 0.85) 60%, transparent 60%), -webkit-linear-gradient(rgba(#themecolourrgb#, 0.6), rgba(#themecolourrgb#, 0.6));
			  background: -webkit-gradient(linear, left bottom, right top, color-stop(40%, transparent), color-stop(40%, rgba(113, 113, 113, 0.28)), color-stop(60%, rgba(#secondarycolourrgb#, 0.85)), color-stop(60%, transparent)), -webkit-gradient(linear, left top, left bottom, from(rgba(#themecolourrgb#, 0.6)), to(rgba(#themecolourrgb#, 0.6)));
			  background-size: 4px 4px;}
			.Rosette.Custom.#classname# .strip:before {background: radial-gradient(circle, black, rgba(#darkercolourrgb#, 0.25));}
			.Rosette.Custom.#classname# .scheme-wrapper {
			  background-color: #secondarythemecolour#;
			  background: linear-gradient(90deg, transparent 30%, rgba(#themecolourrgb#, 0.59) 30%, rgba(#secondarycolourrgb#, 0.72) 70%, transparent 70%), linear-gradient(#secondarythemecolour#, #themecolour#);
			  background: -webkit-linear-gradient(0deg, transparent 30%, rgba(#themecolourrgb#, 0.59) 30%, rgba(#secondarycolourrgb#, 0.72) 70%, transparent 70%), -webkit-linear-gradient(#secondarythemecolour#, #themecolour#);
			  background: -webkit-gradient(linear, left top, right top, color-stop(30%, transparent), color-stop(30%, rgba(#themecolourrgb#, 0.59)), color-stop(70%, rgba(#secondarycolourrgb#, 0.72)), color-stop(70%, transparent)), -webkit-gradient(linear, left top, left bottom, from(#secondarythemecolour#), to(#themecolour#));
			  background-size: 3px 3px;
			  box-shadow: 0 0 0 1px rgba(253, 253, 253, 0.26) inset, 
			              0 0 0 3px #secondarythemecolour# inset, 0 0 0 4px #secondarythemecolour# inset, 
			              0 0 2px 5px rgba(#darkercolourrgb#, 0.62) inset, 
			              0 0 5px 0 rgba(0, 0, 0, 0.57), 0 0 40px 5px rgba(0, 0, 0, 0.42);}              
			.Rosette.Custom.#classname# .scheme-wrapper .scheme-score {
			  color: ##fff;
			  color: rgba(255, 255, 255, 0.75);}
			.Rosette.Custom.#classname# .scheme-wrapper .scheme-score:before {
			  color: #themecolour#;
			  color: rgba(#darkercolourrgb#, 0.39);}
			.Rosette.Custom.#classname# .scheme-wrapper .scheme-score:after {
			  color: #secondarythemecolour#;
			  color: rgba(#darkercolourrgb#, 0.29);}
			.Rosette.Custom.#classname# .tail:before,
			.Rosette.Custom.#classname# .tail:after {
			  background-color: #secondarythemecolour#;
			  background-image: linear-gradient(0deg, transparent 20%, rgba(113, 113, 113, 0.28) 20%, rgba(#secondarycolourrgb#, 0.85) 40%, transparent 40%, rgba(113, 113, 113, 0.28) 60%, rgba(#secondarycolourrgb#, 0.85) 80%, transparent 80%), linear-gradient(rgba(#secondarycolourrgb#, 0.6), rgba(#themecolourrgb#, 0.6));
			  background-image: -webkit-linear-gradient(90deg, transparent 20%, rgba(113, 113, 113, 0.28) 20%, rgba(#secondarycolourrgb#, 0.85) 40%, transparent 40%, rgba(113, 113, 113, 0.28) 60%, rgba(#secondarycolourrgb#, 0.85) 80%, transparent 80%), -webkit-linear-gradient(rgba(#secondarycolourrgb#, 0.6), rgba(#themecolourrgb#, 0.6));
			  background-image: -webkit-gradient(linear, left bottom, left top, color-stop(20%, transparent), color-stop(20%, rgba(255, 255, 255, 0.28)), color-stop(40%, rgba(#secondarycolourrgb#, 0.85)), color-stop(40%, transparent), color-stop(60%, rgba(255, 255, 255, 0.28)), color-stop(80%, rgba(#lightercolourrgb#, 0.85)), color-stop(80%, transparent)), -webkit-gradient(linear, left top, left bottom, from(rgba(#secondarycolourrgb#, 0.6)), to(rgba(#themecolourrgb#, 0.6)));
			  background-size: 4px 4px;}
			.Rosette.Custom.#classname# .scheme-title {color: rgba(255, 255, 255, 0.75);}
			.Rosette.Custom.#classname# .scheme-rank--rosette {color: rgba(255, 255, 255, 0.75);}			
		</style>
	</cfoutput>	
</cfif>
<!--- ------------------------------------------------------------------------------------
	END USER SPECIFIED STYLING COLOUR
------------------------------------------------------------------------------------ --->

<cfoutput>
	<div class="scoreSchemeWidget #widgetstyle# #widgettheme# #classname#">
		<!--- ACTIVITY SCHEME WRAPPER --->
		<div class="scheme-wrapper">
			<!--- ACTIVITY SCHEME TITLE --->
			<div class="scheme-title">
				<p>phr_title_scoreRuleGrouping_#SchemeID#</p>	
			</div> 
			<!--- / ACTIVITY SCHEME TITLE --->
			<!--- ACTIVITY SCHEME SCORE --->
			<div class="scheme-score">
				<h1 class="">
					<strong class="">#score#</strong>
				</h1>	
				<!--- ACTIVITY SCHEME RANKING 
							Extra for the rosette style only 
				--->
				<cfif showRank>
					<div class="scheme-rank--rosette">
						<p>phr_Rank: #rank# of #totalEntities#</p>
					</div>
				</cfif>	
				<!--- / ACTIVITY SCHEME RANKING --->				
			</div> 
			<!--- / ACTIVITY SCHEME SCORE --->
			<!--- ACTIVITY SCHEME RANKING --->
			<cfif showRank>
				<div class="scheme-rank">
					<p>phr_Rank: #rank# of #totalEntities#</p>
				</div>
			</cfif>	
			<!--- / ACTIVITY SCHEME RANKING --->
		</div> <!--- / ACTIVITY SCHEME WRAPPER --->	
		<!--- rosette SPECIFIC DIVS DO NOT REMOVE!! --->
    <div class="strip"></div>
    <div class="strip"></div>
    <div class="strip"></div>
    <div class="strip"></div>
    <div class="strip"></div>
    <div class="strip"></div>
    <div class="tail"></div>
	</div>

</cfoutput>