<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			siteSubNav.cfm	
Author:				DAM
Date created:		2002-07-17
	
Description:		This provides a subnavigation list of hyperlinks

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
24-Oct-2004			SWJ			Changed this tag to use the new memory resident translations and cf_translate
07-Dec-2004   		WAB 		Changed getLeftMenuParent to use the getElements query in memory - this gives the correct parents from the current tree
08-Dec-2004			WAB    		the link was using sitedomain which is wrong !!  Replaced with sitedomain
14-Dec-2004			GCC			Added methods to turn off level 1 and 2 navigation -showLevelOneNavigation / showLevelTwoNavigation
14-Dec-2004			GCC			Extended externalFiles to link out to an external site in a new window if the URL begins with http://
2006-05-18			SWJ			Changed DisplayBorderTableBorder param to siteSubNavTableBorder as this conflicted when this is set in border INI
2006-05-18			SWJ			Introduced siteSubNav-Level2-menuTextTD and siteSubNav-Level3-menuTextTD as classes to the table so we can control content using css
2006/06/15			NJH			Added a few level 2 parameters: ShowLevelTwoSpacing, LevelTwoSpacingHeight, LevelTwoMenuVAlign, LevelTwoMenuHeight
2010/10/20			WAB			removed request.use newelements, assume everyone does.  Not sure if this template is used anyway
2013-09-18  		WAB 		File had been removed from 2013 - but turned out still being used by Lexmark (as an include)
Enhancement still to do:
 --->

<!--- default values --->
<CFPARAM NAME="TableBGCOLOR" DEFAULT="00ADAD">
<CFPARAM NAME="siteSubNavTableBorder" DEFAULT="0"><!--- 2006-05-18			SWJ			Changed DisplayBorderTableBorder param to siteSubNavTableBorder as this conflicted when this is set in border INI --->
<CFPARAM name="showHeadingArea" default="yes">
<CFPARAM NAME="TDHeadingClass" DEFAULT="leftSubNavHeading">
<CFPARAM NAME="TDHeadingAlign" DEFAULT="right">
<CFPARAM NAME="TDHeadingHeight" DEFAULT="38">
<cfparam name="checkForChildren" default="No">
<cfparam name="nodeWithChildren" type="numeric" default="1"> <!--- this is set as part of the query run later when check for children eq 1 --->
<cfparam name="LeftWidth" type="numeric" default="123">

<CFPARAM NAME="showLevelOneNavigation" DEFAULT="true">

<CFPARAM NAME="showLevelTwoNavigation" DEFAULT="true">
<CFPARAM NAME="LevelTwoClass" DEFAULT="subNavMenuItem">
<CFPARAM NAME="LevelTwoMenuAlign" DEFAULT="right">
<CFPARAM NAME="LevelTwoMenuVAlign" DEFAULT="top">
<CFPARAM NAME="LevelTwoMenuHeight" DEFAULT="">
<CFPARAM NAME="LevelTwoOncolour" DEFAULT="##FF9900">
<CFPARAM NAME="LevelTwoOffcolour" DEFAULT="##DADADA">
<CFPARAM NAME="DividerIMG" DEFAULT="1pixel-black-000000.gif"> 
<CFPARAM NAME="LevelTwoShowBullet" DEFAULT="no">
<CFPARAM NAME="LevelTwoOnBullet" DEFAULT="onBullet.gif">
<CFPARAM NAME="LevelTwoOffBullet" DEFAULT="offBullet.gif">
<CFPARAM NAME="ShowLevelTwoSpacing" DEFAULT="false">
<CFPARAM NAME="LevelTwoSpacingHeight" DEFAULT="1">

<CFPARAM NAME="showLevelThreeNavigation" DEFAULT="true">
<CFPARAM NAME="LevelThreeClass" DEFAULT="subNavMenuItem">
<CFPARAM NAME="LevelThreeMenuAlign" DEFAULT="right">
<CFPARAM NAME="LevelThreeOncolour" DEFAULT="##FF9900">
<CFPARAM NAME="LevelThreeOffcolour" DEFAULT="##DADADA">
<CFPARAM NAME="DividerIMG" DEFAULT="1pixel-black-000000.gif"> 
<CFPARAM NAME="LevelThreeShowBullet" DEFAULT="no">
<CFPARAM NAME="LevelThreeOnBullet" DEFAULT="onBullet.gif">
<CFPARAM NAME="LevelThreeOffBullet" DEFAULT="offBullet.gif">

<CFPARAM NAME="showLevelFourNavigation" DEFAULT="false">
<!--- 2006/03/28 - GCC - Used to determine which menu type to build --->
<cfparam name="request.menutype" type="string" default="standard">
<!--- 2006/09/12 GCC -failed attempt to reuse this code beyond generation 3 --->
<cfparam name="request.generationOffset" type="numeric" default="3">


<cfif isdefined("getElements")><!--- WAB added 2007-01-16 to deal with not having a current tree--->

	<!--- Get the parent of the current page --->
	<!--- 	2004-12-07 WAB changed to get from the cached tree - rather than from database  --->
	<CFIF IsDefined("caller.HomeSibling") and caller.HomeSibling is true>
		<CFQUERY NAME="getLeftMenuParent" DBTYPE="query">
			select parent, headline 
			from getElements where 1=1
			<cfif isDefined("etid")>
				and upper(elementTextID) = '#ucase(etid)#'
			<cfelse>
				and id = #eid#
			</cfif>
		</CFQUERY>
	<CFELSEIF isDefined("caller.eid") and isNumeric(caller.eid) and caller.eid neq 0>
		<!--- <cfoutput>caller.eid='#caller.eid#'<CF_ABORT></cfoutput> --->
		<CFSET eid = caller.eid>
		<CFQUERY NAME="getLeftMenuParent" DBTYPE="query">
			select parent, headline from getElements where node = #caller.eid#
		</CFQUERY>
	
	<CFELSE>
		<CFQUERY NAME="getEID" DATASOURCE="#application.sitedatasource#">
			select id from element where elementtextid =  <cf_queryparam value="#application.defaultExternalElementETID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
		<CFSET eid = getEID.id>
	</CFIF>
	
	<!--- If the current page doesn't have a parent then revert to the current page --->
		
	<CFIF isdefined("getLeftMenuParent") AND getLeftMenuParent.recordcount NEQ 0>
		<CFIF getLeftMenuParent.parent EQ application.TopElementIDPartnerContent>
			<CFSET LeftTopElement=#eid#>
		<CFELSE>
			<CFSET LeftTopElement=#getLeftMenuParent.parent#>
		</CFIF>
	<CFELSE>
		<CFSET LeftTopElement=#eid#>
	</CFIF> 
	<!--- <cfif isDefined("sectionEID") and sectionEID gt 0>
		<CFSET sectionEID=#sectionEID#>
	<cfelse>
		<CFSET sectionEID=#application.TopElementIDPartnerContent#>
	</cfif> --->
	
	
	<!--- <CFIF IsDefined("application.elementTree")>
		<CFSET getElements=application.elementTree>
	<CFELSE>
		Error : Note for Developers - the sitesubnav.cfm has been called and "caller.getElements" is not scoped.
		<CF_ABORT>
	</CFIF> --->
	
	<!--- this should be moved to display border as it will be required elsewhere --->
	<CFQUERY NAME="getThisElementsSectionEID" DBTYPE="query">
		SELECT distinct sectionEID FROM getElements 
			WHERE parent = #EID# or node = #EID#
			<!--- parent=#LeftTopElement# or node=#LeftTopElement# --->
	</CFQUERY>
	
	<cfif (not structKeyExists(URL,"eid") and not structKeyExists(URL,"etid")) or structkeyExists(request,"ignoreSectionEID") and request.ignoreSectionEID>
	<!--- if the URL vars above don't exist this implies we are outside of the 
				elements structure therefore we should use the default sectionIDs defined in
				Borders_INI --->
		<cfif request.relaycurrentuser.isLoggedIn>
			<cfset thisSectionID = defaultLoggedInSectionEID>
		<cfelse>
			<cfset thisSectionID = defaultExternalSectionEID>
		</cfif>
	<cfelse>
			<cfset thisSectionID = request.currentElement.sectionEID>
	</cfif>
	 
	<cfif request.menutype eq "dTree">
		<cfparam name="request.dTreeGenerations" type="numeric" default="2">
		<cfparam name="request.dTreeTopLoggedOut" type="numeric" default="948">
		<cfparam name="request.dTreeTopLoggedIn" type="numeric" default="923">
		
		<cfif request.relaycurrentuser.isLoggedIn>
			<cfset treeTop = request.dTreeTopLoggedIn>
		<cfelse>
			<cfset treeTop = request.dTreeTopLoggedOut>
		</cfif>
		
		<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(#treeTop#)>
	
		<cfset getElements = application.com.relayElementTree.convertTreeToMenuTree(
	        elementTree = getElements, 
	        currentelementid = 0, 
	        numberofgenerations = #request.dTreeGenerations#+1,<!--- generations we need + top element generation --->
	        showCurrentElementBranch = false, 
	        includeTopElement = true 
	     )> 
		 <!--- needs paramming --->
		<cfset targetFrame = "_self">
		
		<cfset elementTree = getElements>
		<cfset numberOfGenerations = request.dTreeGenerations>
		<cfinclude template="/relay/RelayTagTemplates/elementTreeMenu.cfm">
	
	<cfelse>
	<!--- 2007/02/21 - GCC - If this is empty then the element isn't in the users tree and they will get redirected to landingpage.
	without this HACK the navigation below falls over... --->
	<cfif getThisElementsSectionEID.sectionEID neq "">
		<CFQUERY NAME="leftNavElements" DBTYPE="query">
		<!--- 	SELECT * FROM getElements 
				WHERE sectionEID = #thisSectionID# 
				and showOnMenu = 1 --->
					SELECT url, generation as test,generation - #request.generationOffset# as generation,node,headline,hidden,SHOWONMENU,ISEXTERNALFILE FROM getElements 
				WHERE  
					showOnMenu = 1
					and sectionEID = #getThisElementsSectionEID.sectionEID#
					<!--- sectionEID = #thisSectionID#  --->
				<!--- parent=#LeftTopElement# or node=#LeftTopElement# --->
		</CFQUERY>
		<cfif checkForChildren eq "yes">
			<CFQUERY NAME="getElementsWithChildren" DBTYPE="query">
				SELECT distinct parent FROM getElements 
			</CFQUERY>
			<cfset nodeWithChildren = valueList(getElementsWithChildren.parent)>
		</cfif> 
		<CFIF IsDefined("application.elementTree")>
			<CFQUERY NAME="getMaxMin" DBTYPE="query">
				select max(generation)  as maxi, min(generation) as mini from leftNavElements group by generation
			</CFQUERY>
		<CFELSE>
			<CFQUERY NAME="getMaxMin" DBTYPE="query">
				select max(generation)  as maxi, min(generation) as mini from leftNavElements group by generation
			</CFQUERY>
		</CFIF>
		
		<cf_translate>
		
		<CFOUTPUT>
			<!-- start of subNavTable --->
			<TABLE WIDTH="100%" BORDER="#siteSubNavTableBorder#" CELLSPACING="0" CELLPADDING="0" class="siteSubNavTable">
			<TBODY>
		 	<CFSET MenuItemCount = 0>
			<CFPARAM NAME="DefaultTemplate" DEFAULT="et.cfm">
	
			<CFLOOP Query="leftNavElements">
				<CFIF hidden NEQ 1 and showonmenu NEQ 0>
						<CFIF Generation  EQ getMaxMin.maxi and showHeadingArea is "yes"><!--- i.e. this is a top level element --->
							<cfif showLevelOneNavigation eq true> 
								<TR VALIGN="middle">
									<TD colspan="2" HEIGHT="#TDHeadingHeight#" ALIGN="#TDHeadingAlign#" CLASS="#TDHeadingClass#" >
										<cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelOneSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelOneUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif> <!--- #elementMenuText# --->
									</TD>					
								</TR>
								<TR>
									<TD colspan="2">
										<IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="1px">
									</TD>
								</TR>
							</cfif>
	<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Generation 2 - Level Two                                                         
		++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->					
	
						<CFELSEIF Generation EQ getMaxMin.maxi +1><!--- i.e. this is a lower level element --->
							<cfif showLevelTwoNavigation eq true>
								<CFSET MenuItemCount = MenuItemCount+1>
								<CFIF node eq eid><CFSET oncolour = "#LevelTwoOncolour#"><CFSET offcolour = "#LevelTwoOffcolour#"><CFELSE><CFSET oncolour = "#LevelTwoOncolour#"><CFSET offcolour = "#LevelTwoOffcolour#"></CFIF>
								<CFSET nIMGSize=15>
								<TR ALIGN="#LevelTwoMenuAlign#" VALIGN="#LevelTwoMenuVAlign#" HEIGHT="#LevelTwoMenuHeight#" BGCOLOR="#LevelTwoOffcolour#" ONMOUSEOVER="this.style.backgroundColor= '#LevelTwoOncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#LevelTwoOffcolour#';" class="siteSubNavTR">
									<TD ALIGN="#LevelTwoMenuAlign#" class="siteSubNav-Level2-bulletTD"><CFIF LevelTwoShowBullet eq "yes"><CFIF node eq eid and listFind(nodeWithChildren,node) neq 0><img src="/code/borders/images/#LevelTwoOnBullet#" alt="" border="0" align="left"><cfelse><img src="/code/borders/images/#LevelTwoOffBullet#" border="0"></CFIF></CFIF></TD>
									<TD VALIGN="#LevelTwoMenuVAlign#" class="siteSubNav-Level2-menuTextTD">		
										<CFIF isExternalFile EQ 1>
											<CFIF left(URL, 10) EQ "JavaScript">
												<A HREF="#URL#" CLASS="#LevelTwoClass#"><cfif ((node eq eid) or listfind(displaybordergetthiselement.parentidlist,node) is not 0 )><span class="levelTwoSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelTwoUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											<cfelseif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://">
												<A HREF="#URL#" CLASS="#LevelTwoClass#" target="_blank"><cfif ((node eq eid) or listfind(displaybordergetthiselement.parentidlist,node) is not 0 )><span class="levelTwoSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelTwoUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											<CFELSE>
												<A HREF="/#URL#" CLASS="#LevelTwoClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelTwoSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelTwoUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											</CFIF>
										<CFELSE>
											<A HREF="/#DefaultTemplate#?eid=#node#" CLASS="#LevelTwoClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelTwoSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelTwoUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
										</CFIF>
									</TD>
									<!--- <td>&nbsp;</td> --->
								</TR>
								<!--- NJH 2006/06/15 added a spacer for level 2 menu items --->
	  							<cfif ShowLevelTwoSpacing eq true>
									<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="#LevelTwoSpacingHeight#px"></TD></TR>
								</cfif>
							</cfif>
		<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Generation 3 - Level Three                                                           
		++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->					
						
						<cfif showLevelThreeNavigation eq true and ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )>
								<CFQUERY NAME="leftSubNavElements" DBTYPE="query">
									SELECT * FROM getElements WHERE parent=#node#
								AND showOnMenu = 1  <!--- NJH 2007/01/22  - hide menu items that are set to never show --->
								</CFQUERY>
			
								<CFSET SubMenuItemCount = 1>
								<CFLOOP Query="leftSubNavElements">
									<CFSET SubMenuItemCount = SubMenuItemCount+1>
										<CFIF node eq eid><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"><CFELSE><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#">
										</CFIF>
										<CFSET nIMGSize=15>
										<TR ALIGN="#LevelTwoMenuAlign#" VALIGN="top" BGCOLOR="#LevelThreeOffcolour#" ONMOUSEOVER="this.style.backgroundColor= '#LevelThreeOncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#LevelThreeOffcolour#';">
											
											<TD VALIGN="top"><CFIF LevelThreeShowBullet eq "yes"><CFIF leftSubNavElements.node eq node><img src="/code/borders/images/#LevelThreeOnBullet#" alt="" border="0"><cfelse><img src="/code/borders/images/#LevelThreeOffBullet#" border="0"></CFIF></CFIF></TD>
											<TD class="siteSubNav-Level3-menuTextTD">				
												<CFIF isExternalFile EQ 1>
													<CFIF left(URL, 10) EQ "JavaScript">
														<A HREF="#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													<cfelseif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://">
														<A HREF="#URL#" CLASS="#LevelThreeClass#" target="_blank"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													<CFELSE>
														<A HREF="/#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													</CFIF>
												<CFELSE>
													<A HREF="/#DefaultTemplate#?eid=#node#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
												</CFIF>
											</TD>
										</TR>
										<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#leftwidth#" HEIGHT="1px"></TD></TR>
								</CFLOOP>
							<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="1px"></TD></TR>
						</cfif>
						<!--- </CFIF> --->
		
						<!--- i.e. this is a lower level element --->
		<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Generation 4 - Level Four                                                           
		++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->					
						<!--- HACK!!! --->
						<cfif showLevelFourNavigation eq true and ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )>four<br>
								<!--- changed from #node# --->
								<CFQUERY NAME="leftSubNavElements" DBTYPE="query">
									SELECT * FROM getElements WHERE parent=#eid#
								AND showOnMenu = 1  <!--- NJH 2007/01/22  - select menu items that are set to show --->
								</CFQUERY>
								<!--- try parents --->
								<cfif leftSubNavElements.recordcount eq 0>
									<CFQUERY NAME="leftSubNavElements" DBTYPE="query">
										SELECT * FROM getElements WHERE parent=#request.currentelement.parentID#
									AND showOnMenu = 1  <!--- NJH 2007/01/22  - select menu items that are set to show --->
									</CFQUERY>
								</cfif>
								<!--- try grandparents --->
								<cfif leftSubNavElements.recordcount eq 0 and listlen(request.currentelement.parentIDlist)-1 gt 0>
									<CFQUERY NAME="leftSubNavElements" DBTYPE="query">
										SELECT * FROM getElements WHERE parent=#listgetat(request.currentelement.parentIDlist,listlen(request.currentelement.parentIDlist)-1)# 
									AND showOnMenu = 1  <!--- NJH 2007/01/22  - select menu items that are set to show --->
									</CFQUERY>
								</cfif>
								<CFSET SubMenuItemCount = 1>
								<CFLOOP Query="leftSubNavElements">
									<CFSET SubMenuItemCount = SubMenuItemCount+1>
										<CFIF node eq eid><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"><CFELSE><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#">
										</CFIF>
										<CFSET nIMGSize=15>
										<TR ALIGN="#LevelTwoMenuAlign#" VALIGN="top" BGCOLOR="#LevelThreeOffcolour#" ONMOUSEOVER="this.style.backgroundColor= '#LevelThreeOncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#LevelThreeOffcolour#';">
											
											<TD VALIGN="top"><CFIF LevelThreeShowBullet eq "yes"><CFIF leftSubNavElements.node eq node><img src="/code/borders/images/#LevelThreeOnBullet#" alt="" border="0"><cfelse><img src="/code/borders/images/#LevelThreeOffBullet#" border="0"></CFIF></CFIF></TD>
											<TD>				
												<CFIF isExternalFile EQ 1>
													<CFIF left(URL, 10) EQ "JavaScript">
														<A HREF="#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													<cfelseif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://">
														<A HREF="#URL#" CLASS="#LevelThreeClass#" target="_blank"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													<CFELSE>
														<A HREF="/#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
													</CFIF>
												<CFELSE>
													<A HREF="/#DefaultTemplate#?eid=#node#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
												</CFIF>
											</TD>
										</TR>
										<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#leftwidth#" HEIGHT="1px"></TD></TR>
								</CFLOOP>
							<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="1px"></TD></TR>
						</cfif>	
						<!--- END HACK --->
						
						<CFELSEIF Generation eq 3>
							<CFSET MenuItemCount = MenuItemCount+1>
							<CFIF node eq eid><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"><CFELSE><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"></CFIF>
							<CFSET nIMGSize=15>
							<TR ALIGN="#LevelTwoMenuAlign#" VALIGN="top" BGCOLOR="#LevelThreeOffcolour#" ONMOUSEOVER="this.style.backgroundColor= '#LevelThreeOncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#LevelThreeOffcolour#';">
								
								<TD >
									<CFIF LevelThreeShowBullet eq "yes"><CFIF node eq eid><img src="/code/borders/images/#LevelThreeOnBullet#" border="0"><cfelse><img src="/code/borders/images/#LevelThreeOffBullet#" border="0"></CFIF></CFIF>					
								</td><td>						
									<CFIF isExternalFile EQ 1>
										<CFIF left(URL, 10) EQ "JavaScript">
											<A HREF="#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
										<cfelseif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://">
											<A HREF="#URL#" CLASS="#LevelThreeClass#" target="_blank"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
										<CFELSE>
											<A HREF="/#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
										</CFIF>
									<CFELSE>
										<A HREF="/#DefaultTemplate#?eid=#node#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
									</CFIF>
						
								</TD>
							</TR>
							<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="1px"></TD></TR>
		<!--- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Generation 4 - Level Three                                                           
		++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->					
		 					<cfif showLevelThreeNavigation eq true and ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )>
								<CFQUERY NAME="leftSubNavElements" DBTYPE="query">
									SELECT * FROM getElements WHERE parent=#node#
								AND showOnMenu = 1  <!--- NJH 2007/01/22  - select menu items that are set to show --->
								</CFQUERY>
			
								<CFSET SubMenuItemCount = 1>
								<CFLOOP Query="leftSubNavElements">
									<CFSET SubMenuItemCount = SubMenuItemCount+1>
									<CFIF node eq eid><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"><CFELSE><CFSET oncolour = "#LevelThreeOncolour#"><CFSET offcolour = "#LevelThreeOffcolour#"></CFIF>
									<CFSET nIMGSize=15>
										<TR ALIGN="#LevelTwoMenuAlign#" VALIGN="top" BGCOLOR="#LevelThreeOffcolour#" ONMOUSEOVER="this.style.backgroundColor= '#LevelThreeOncolour#';" ONMOUSEOUT="this.style.backgroundColor = '#LevelThreeOffcolour#';">
									
									<TD>
										<CFIF LevelThreeShowBullet eq "yes"><CFIF leftSubNavElements.node eq eid><img src="/code/borders/images/#LevelThreeOnBullet#" border="0"><cfelse><img src="/code/borders/images/#LevelThreeOffBullet#" border="0"></CFIF></CFIF>					
									</td><td>
										<CFIF isExternalFile EQ 1>
											<CFIF left(URL, 10) EQ "JavaScript">
												<A HREF="#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											<cfelseif left(URL, 7) EQ "http://" or left(URL, 8) EQ "https://">
												<A HREF="#URL#" CLASS="#LevelThreeClass#" target="_blank"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											<CFELSE>
												<A HREF="/#URL#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
											</CFIF>
										<CFELSE>
											<A HREF="/#DefaultTemplate#?eid=#node#" CLASS="#LevelThreeClass#"><cfif ((node eq eid) or listfind(request.currentelement.parentidlist,node) is not 0 )><span class="levelThreeSelected">phr_headline_element_#htmleditformat(node)#</span><cfelse><span class="levelThreeUnselected">phr_headline_element_#htmleditformat(node)#</span></cfif></A>
										</CFIF>
									</TD>
										
									</TR>
									<TR><TD colspan="2"><IMG SRC="/code/borders/images/#DividerIMG#" width="#LeftWidth#" HEIGHT="1px"></TD></TR>
								</CFLOOP>
							</CFIF>
						</CFIF>
					<!--- </CFIF> --->
				</CFIF>
			</CFLOOP>
		</TBODY>
		</table>
		<!--- end of subNavTable --->
		</CFOUTPUT>
		</cf_translate>
	</cfif>
</cfif>
</cfif>

