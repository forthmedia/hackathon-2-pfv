<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			managePeopleList.cfm
Author:				SWJ
Date started:		2005-08-17

Description:		This provides a listing screen with an IFrame which is used to edit the person record

Amendment History:

Date 				Initials 	What was changed
2006-04-25			AJC			Added screenids attribute for the managePeopleEditScreenIframe.cfm page
2007-02-05			WAB 		added check to make sure not used by the unknown user
2007-02-21			SSS			Extended to allow invite colleagues button and approve colleagues button
2007-09-24			SSS			If flag group is passed in a seesion varible is set to pass that into the iframes
2008-05-13			NYF			added changableStatuses & approvalSortOrder parameters and passed these to managePeopleApprovaStatus.cfm include
2008-04-06			NYF			Sophos Bugzilla 2060 - added ability to filter people shown by Flags or Flaggroups using parameters:
								filterByFlagGroups or filterByFlags
2011/07/19			PPB			LEN024 add switch showPersonEditForm to allow profile screen to be used instead of hard-coded form; default="true" Lenovo="false" for manage my colleagues
2011/08/15 			PPB 		REL109 make pop-up for invite colleagues smaller
2014-11-13			AXA 		Case 442623 added ApprovalStatus to list of valid values in columns to show
Possible enhancements:

Attributes:
screenids = list of screenids to be displayed under the edit form in managePeopleEditScreenIframe.cfm

 --->

<!--- 2009/06/16 GCC LID2367 ensure known user and logged in--->
<cfif not request.relayCurrentUser.isUnknownUser and request.relayCurrentUser.isLoggedIn>

	<cf_param name="screenids" default=""/>
	<cf_param name="allowColleagueInvite" default="no" type="boolean"/>
	<cf_param name="allowColleagueApprovals" default="no" type="boolean"/>
	<cf_param name="allowColleagueCreation" default="yes" type="boolean"/>
	<cf_param name="showPersonEditForm" default="yes" type="boolean"/>			<!--- 2011/07/19 PPB LEN024 --->
	<cf_param name="iFrameWidth" default="550"/>
	<cf_param name="inviteEmailTextID" default="sendToFriendEmail"/>
	<cf_param name="sortOrder" label="Sort Order" default="lastname,firstname"/>
	<cf_param name="changableStatuses" default="PerApproved,PerRejected"/>
	<cf_param name="approvalSortOrder" default="approvalStatus,lastname"/>
	<cf_param name="filterByFlagGroups" default=""/>
	<cf_param name="filterByFlags" default=""/>
	<cf_param name="hideFieldsControl" default="" validValues="salutation,FirstName,LastName,officePhone,MobilePhone,email,language,insFax,location" multiple="true" size="5"/>
	<cf_param name="manageMyColleaguesListingsColumnsToShow" default="salutation,firstname,lastname,emailAddress" validValues="salutation,FirstName,LastName,personID,emailAddress,ApprovalStatus" multiple="true" size="5"/> <!--- 2014-11-13 AXA Case 442623 added ApprovalStatus to list of valid values in columns to show --->

	<cf_includejavascriptonce template = "/javascript/openwin.js">

	<div class="form-group row"><div class="col-xs-12">
	<cfif allowColleagueCreation>
		<a href="/relayTagTemplates/managePeopleEditScreenIframe.cfm?personid=0" id="add_new" class="button btn btn-default">
		phr_Ext_Add_new
		</a>
	</cfif>
	<cfif allowColleagueInvite>
		<cfset encryptedUrlVariables = application.com.security.encryptQueryString(queryString="?Emailonly=true&commType=email&EmailtextID=#inviteEmailTextID#")>
		<cfoutput><a href="/relayTagTemplates/sendCommToFriend.cfm#encryptedUrlVariables#" id="InviteFriend" class="button btn btn-primary"></cfoutput>
		phr_Ext_Invite_Colleagues
		</a>
	</cfif>
	<cfif allowColleagueApprovals>
		<cfoutput><a href="/relayTagTemplates/managePeopleApprovalStatus.cfm?ManagePeopleApprovalFlags=#changableStatuses#&sortOrder=#approvalSortOrder#" id="Approve_Colleagues" class="button btn btn-primary"></cfoutput>
		phr_Approve_Colleagues
		</a>
	</cfif>
	</div></div>

	<cf_modalDialog size="large" type="iframe" identifier=".firstname .smallLink, ##InviteFriend, ##Approve_Colleagues, ##add_new" closeSpeed=1>

	<cfif isdefined("JobsFlagGroup")>
		<cfset session.JobsFlagGroup = JobsFlagGroup>
	</cfif>
	<cfset session.managePeopleEditScreenIframeScreenIDs=screenids>
	<cf_include template ="/Code/CFTemplates/customManagePeopleListingIFrame.cfm" onNotExistsTemplate="/relayTagTemplates/managePeopleListingIframe.cfm">
</cfif>