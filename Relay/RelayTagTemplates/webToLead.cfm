<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			webToLead.cfm
Author:				YMA
Date started:		2013-10-22

Purpose:	Web to lead capture form.

Usage:



Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-01-23			AXA			Case 438629 - enable the VAT Number and and Org/Location fields to display
2014-01-24			YMA			Case 438534 - Default Partner Organisation to ID:2 which is normally the owners of the portal and restrict list to only show orgs with locations.
2014-01-31			WAB 		removed references to et cfm, can always just use / or /?
2016/07/28			NJH			JIRA PROD2016-599/351 - moved primary contacts to location level. Deal also with multiple primary contacts. Change the default value of PrimaryContactFlag to PrimaryContacts
2016/09/21			RJT			PROD2016-2295 made corrections originating from the rename from partnerOrganisationID to vendorOrganisationID
Possible enhancements:


 --->

<cf_param name="vendorOrganisationID" label="phr_partner_organisation" description="The vendor organization which to assign newly created leads" required="true" displayvalue="OrganisationName" default="2" value="organisationID" validValues="select organisationname, organisationID from organisation o join organisationtype ot on o.organisationtypeID = ot.organisationTypeID and ot.typetextID = 'AdminCompany' where organisationID in (select organisationID from location)" displayas="select"/> <!---ESZ 2016-02-09 PROD2016-2287--->
<cf_param name="vendorLocationID" label="phr_vendor_location" nullText="phr_Ext_SelectAValue" showNull="true" required="true" default="" relayFormElementType="select" display="SITENAME" value="LOCATIONID" bindonload="true" bindFunction="cfc:webservices.RelayPLOWS.getLocations(actionOrgs={vendorOrganisationID})"/><!---ESZ 2016-02-09 PROD2016-2304--->

<cf_param label="Show Fields (In addition to default fields: firstname,lastName,company,email)" name="showFields" required="false" default="" relayFormElementType="select" WIDTH="300" multiple="true" displayAs="twoSelects" allowSort="true" validValues="countryID,salutation,officePhone,mobilePhone,address1,address2,address3,address4,address5,postalCode,website"/>
<cf_param label="Mandatory Fields (In addition to default fields: firstname,lastName,company,email)" name="mandatoryFields" required="false" default="" relayFormElementType="select" WIDTH="300" multiple="true" displayAs="twoSelects" allowSort="true" validValues="countryID,salutation,officePhone,mobilePhone,address1,address2,address3,address4,address5,postalCode,website"/>

<cf_param label="Set these flags on submit" name="setFlagTextIDs" required="false" default="" relayFormElementType="select" WIDTH="300" multiple="true" displayAs="twoSelects" allowSort="true" validValues="select flagTextID as dataValue, flag as displayValue from vFlagDef where entityTable='lead' and isSystem=0 and dataTable='Boolean' order by flag"/>
<cf_param label="Description of contact history record" name="commDetailFeedback" required="false" default="Web to Lead" type="string" />

<cf_param label="Organization Type" name="orgTypeID" required="true" type="integer" default="2" validValues="select organisationTypeID as value, typeTextID as display from OrganisationType where typeTextID !='AdminCompany' order by TypeTextID"/>

<cf_param label="Campaign" name="campaignID" default="" validValues="func:com.relayCampaigns.getCampaigns()" displayValue="campaignName" dataValue="campaignId"/>

<cf_param label="New Lead Email" name="LeadEmail" type="string" default="LeadAccountManagerNotification" validValues="select emailtextID as value, emailtextID as display from emaildef where active=1 and moduleTextID='LeadManager' order by emailtextID"/>
<cf_param name="showcaptcha" label="Show Captcha" type="boolean" default="false"/>

<cf_param name="PrimaryContactFlag" label="Primary Contact Flag" required="true" type="string" default="PrimaryContacts" validValues="select f.flagtextID as value, f.name as display from flag f join flaggroup fg on f.flaggroupID = fg.flaggroupID where fg.entitytypeID in (1,2) and fg.flagtypeID in (6,8) and f.active = 1 and linksToEntityTypeID = 0 order by f.name"/>

<cf_param label="Return Type" name="returnType" type="string" required="true" default="" validValues="eid¬Return to EID, screenReturn to Screen, workflowReturn to Workflow"/>
<cf_param requiredFunction="return jQuery('[name=returnType] option:selected').val() == 'eid'" label="Return to EID" name="ReturntoEID" type="string" default=""/>
<cf_param requiredFunction="return jQuery('[name=returnType] option:selected').val() == 'screen'" label="Return to Screen" name="ReturntoScreen" default="" validValues="select screenname as display, screenID as value from screens"/>
<cf_param requiredFunction="return jQuery('[name=returnType] option:selected').val() == 'workflow'" label="Return to Workflow" name="ReturntoWorkflow" default="" validValues="select processID as value, processdescription as display from processheader"/>
<!---ESZ Web to Lead Form Configurable fields--->
<cf_param name="UserDefinedScreenTextID" label="Screen ID" type="string" default="" validValues="select ScreenName as display, screenID as value from screens where entityType like 'Lead' order by display "/>

<cfset showFields = listAppend(showFields,"firstname,lastName,company,email")>
<cfset mandatoryFields = listAppend(mandatoryFields,"firstname,lastName,company,email")>
<!--- incase any old configuration added these fields then remove duplicates --->
<cfset showFields = ListRemoveDuplicates(showFields,",",true)>
<cfset mandatoryFields = ListRemoveDuplicates(mandatoryFields,",",true)>

<cfif not request.relaycurrentuser.isloggedin>

	<cfif not isdefined("countryID")>
		<cfset countryID = request.relaycurrentuser.content.showforcountryID>
	</cfif>
        <cfif '#countryID#' LT 1>
            <cfset countryID = 9>
        </cfif>
	<!--- START: 2014-01-23	AXA	Case 438629 - enable the VAT Number and and Org/Location fields to display--->
	<!--- <cfset insCountryID = val(countryID)> --->
	<cfset countryDetails = application.com.commonQueries.getCountry(countryid=countryID)>
	<!--- END: 2014-01-23 AXA Case 438629 - enable the VAT Number and and Org/Location fields to display--->
	<cfset phoneMask = application.com.commonQueries.getCountry(countryid=countryID).telephoneformat>

	<cfset request.relayFormDisplayStyle = "HTML-div">

	<cf_include template="/templates/relayFormJavaScripts.cfm">

	<cfif structKeyExists(form,"save") and (not isDefined("failedCaptcha") or (isDefined("failedCaptcha") and not failedCaptcha))>
			<cfoutput>
				<div id="orgProfileLower">
					<cfif not structKeyExists(form,"address1") or (structKeyExists(form,"address1") and form.address1 eq "")>
						<cfset form.address1 = form.firstName & " " & form.lastName>
					</cfif>

					<cfloop collection="#form#" item="formItem">
						<cfset tempForm['ins' & #formItem#] = form[formItem]>
					</cfloop>
					<cfset structAppend(form,tempForm)>

					<cfset form.inssitename = form.company>

					<!--- Create POL Data --->
					<cfinclude template="/relay/remoteAddTask.cfm">
					<!--- Set current user --->
					<cfif not request.relaycurrentuser.isInternal>
						<cfset application.com.login.initialiseAnExternalUser(thisPersonID)>
					</cfif>

					<cftry>
						<!--- NJH 2016/07/19 JIRA PROD-599 - deal with primary contact at location level as well - work out the entityType --->
						<cfset primaryContactFlagType = application.com.flag.getFlagStructure(flagId=PrimaryContactFlag).entityType.tablename>
						<cfset primaryContacts = application.com.relayPLO.getPrimaryContactsForEntity(entityID=primaryContactFlagType eq "location"?form.vendorLocationID:form.frmOrganisationID,PrimaryContactFlag=PrimaryContactFlag)>
						<cfset sendLeadEmailTo = primaryContacts>

						<cfif primaryContacts eq "">
							<CFQUERY NAME="firstPersonInOrg" DATASOURCE="#application.siteDataSource#">
								select top 1 personID as partnerSalesPersonID
									from person
									where organisationID = <cf_queryparam value="#form.frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
									order by created
							</CFQUERY>
							<cfset sendLeadEmailTo = firstPersonInOrg.partnerSalesPersonID>

						<!--- NJH 2016/07/27 JIRA PROD2016-351 - don't assign primary contacts to lead as there will be multiple primary contacts. Lead is unassigned
						<cfelse>
							<cfset form.partnerSalesPersonID = partnerSalesPersonID.partnerSalesPersonID> --->
						</cfif>

						<!--- if only one primary contact then just assign them as the partner --->
						<cfif listLen(sendLeadEmailTo) eq 1>
							<cfset form.partnerSalesPersonID = sendLeadEmailTo>
						</cfif>

						<cfset form.contactPersonID = request.relayCurrentUser.personId>
						<cfif isDefined("campaignID")>
							<cfset form.campaignID = campaignID>
						</cfif>

						<!--- NJH 2014/07/21 Store the leads in the leads table --->
						<cfset form.progressID = application.lookupID.leadProgressID.OpenNotContacted>
						<cfset form.leadTypeID = application.lookUpID.leadTypeID.Lead>
						<cfset form.sourceID = application.lookUpID.leadSourceID.web>
						<cfset form.convertedPersonID = request.relayCurrentUser.personId>
						<cfset form.convertedLocationID = request.relayCurrentUser.locationID>
						<cfset form.description = commDetailFeedback>

						<cfset result = application.com.relayEntity.insertEntity(entityDetails=form,entityTypeID=application.entityTypeID.lead)>
						<!---ESZ Web to Lead Form Configurable fields--->
						<CFSET form.newLead =result.ENTITYID>
						<cfif result.isOK>
							<cfloop list="#setFlagTextIDs#" index="flagTextID">
								<cfset application.com.flag.setFlagData(flagId=flagTextID,entityID=result.entityID)>
							</cfloop>
						</cfif>

						<cfif application.com.flag.doesFlagExist("DataQualityCategorizationA") and (structKeyExists(form,"insFirstname") or structKeyExists(form,"insLastname")) and structKeyExists(form,"company") and structKeyExists(form,"insEmail")>
							<cfset application.com.flag.setFlagData(flagId="DataQualityCategorizationA",entityID=request.relayCurrentUser.personId)>
						<cfelseif application.com.flag.doesFlagExist("DataQualityCategorizationB") and  (structKeyExists(form,"insFirstname") or structKeyExists(form,"insLastname")) and structKeyExists(form,"insEmail")>
							<cfset application.com.flag.setFlagData(flagId="DataQualityCategorizationB",entityID=request.relayCurrentUser.personId)>
						<cfelseif  application.com.flag.doesFlagExist("DataQualityCategorizationC")>
							<cfset application.com.flag.setFlagData(flagId="DataQualityCategorizationC",entityID=request.relayCurrentUser.personId)>
						</cfif>

						<cf_addCommDetail
							personID="#request.relayCurrentUser.personId#"
							locationID="#thisLocID#"
							commTypeID=11
							feedback="#commDetailFeedback#"
							COMMSTATUSID=3000
						>

                <!---ESZ Web to Lead Form Configurable fields--->
                <cfif UserDefinedScreenTextID neq ""  &&  isDefined("countryID") && "#countryID#" GT 0>
                    <cfif !StructIsEmpty(form)>
                        <cftry>
                            <cf_updateData >
                            <cfcatch>
                                <cfset errorId =
                                    application.com.errorHandler.recordRelayError_Warning(
                                        type="Screen Update",
                                        Severity="error",
                                        caughtError=cfcatch)>
                            </cfcatch>
                        </cftry>
                    </cfif>
                </cfif>

						<cfif result.isok>
							<cfset mergeStruct = {leadID=result.entityID}>

							<!--- as primary contacts can now be multiple, loop over list and send each primary contact the email. --->
							<cfloop list="#sendLeadEmailTo#" index="leadEmailRecipientPersonID">
								<cfset application.com.email.sendEmail(personID=leadEmailRecipientPersonID,emailTextID=leadEmail,mergeStruct=mergeStruct)>
							</cfloop>
						</cfif>
						<cfcatch>
							<cfset application.com.errorHandler.handleError(exception=cfcatch)>
						</cfcatch>
					</cftry>

					<cfswitch expression="#returnType#">
						<cfcase value="eid">
							<cfif isDefined("returnToEID") and returnToEID neq "">
								<script>
									window.location.href = '/?eid=#returnToEID#';
								</script>
							<cfelse>
								<cfoutput>#application.com.relayUI.message(message="You must define returnToEID in the relaytag",type="error")#</cfoutput>
							</cfif>
						</cfcase>
						<cfcase value="screen">
							<cfif isDefined("returnToScreen") and returnToScreen neq "">
								<CFSET frmPersonID = request.relayCurrentUser.personId>
								<CFSET frmCurrentScreenID = returnToScreen>
								<CFINCLUDE template= "/screen/showScreenNew.cfm">
							<cfelse>
								<cfoutput>#application.com.relayUI.message(message="You must define returnToScreen in the relaytag",type="error")#</cfoutput>
							</cfif>
						</cfcase>
						<cfcase value="workflow">
							<cfif isDefined("returnToWorkflow") and returnToWorkflow neq "">
								<CFSET frmPersonID = request.relayCurrentUser.personId>
								<CFSET frmProcessID = returnToWorkflow>
								<cfif structKeyExists(fORM,"addContactSaveButton")>phr_partnerRegRedirectMsg</cfif>
								<a href="/proc.cfm?prid=#returnToWorkflow#&pid=#frmPersonID#&leadID=#result.entityID#&entityType=lead">phr_partnerRegRedirectClickHere</a>
									<form action="/proc.cfm" name="procForm" method="post">
										<cfif isDefined("includeCustomFile") and fileexists("#application.paths.code#\cftemplates\#includeCustomFile#")>
											<cfinclude template="/code/cftemplates/#includeCustomFile#">
										</cfif>
										<input type="hidden" name="pid" value="#frmPersonID#">
										<input type="hidden" name="prid" value="#returnToWorkflow#">
										<input type="hidden" name="leadID" value="#result.entityID#">
										<input type="hidden" name="entityType" value="lead">
									</form>
								<SCRIPT type="text/javascript">
									document.procForm.submit()
								</script>
							<cfelse>
								<cfoutput>#application.com.relayUI.message(message="You must define returnToWorkflow in the relaytag",type="error")#</cfoutput>
							</cfif>
						</cfcase>
					</cfswitch>
				</div>
			</cfoutput>
	<cfelse>
			<cfscript>
			// this query controls the behanvious of each form element
			//NJH 2009/07/08 P-FNL069 - added regexPattern to validate firstname and lastname to accept only alphabetic UTF characters plus space
			qRelayFieldList = QueryNew("fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,sortorder","varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar");
			qRelayFieldListOrdered = QueryNew("fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,sortorder","varchar,varchar,varchar,varchar,varchar,varchar,varchar,varchar");
			// START: 2014-01-23	AXA	Case 438629 - enable the VAT Number and and Org/Location fields to display
			/*newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "inslocationID");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=80");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");*/
			// END: 2014-01-23	AXA	Case 438629 - enable the VAT Number and and Org/Location fields to display
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "salutation");
				temp = QuerySetCell(qRelayFieldList, "type", "validValueList");
				temp = QuerySetCell(qRelayFieldList, "params", "validFieldName=person.salutation");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_Salutation");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "firstName");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_FirstName");
				temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "lastName");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_Ext_LastName");
				temp = QuerySetCell(qRelayFieldList, "regexPattern", application.com.regexp.standardexpressions["NameCharacters"]); // NJH 2009/09/30 LID 2706
/*			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insJobDesc");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "phr_ext_jobDesc");*/
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "email");
				temp = QuerySetCell(qRelayFieldList, "type", "email");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_EmailAddress");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "officePhone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DirectLine");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "mobilePhone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_MobilePhone");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "company");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=80");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_OrgCompanyName");
			/*newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insaka");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_aka");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insVATnumber");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_VATnumber");*/
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "address1");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Address");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "address2");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "address3");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "address4");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_TownCity");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "address5");
				temp = QuerySetCell(qRelayFieldList, "type", "Province");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=50");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_CountyRegionState");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "postalCode");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=10,maxfieldsize=20");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_PostalCodeZip");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "countryID");
				temp = QuerySetCell(qRelayFieldList, "type", "countryList");
				temp = QuerySetCell(qRelayFieldList, "params", "");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Country");
			/*newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insTelephone");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Switchboard");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insFax");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=30,maxfieldsize=30");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_FaxInclCountryCode");
				temp = QuerySetCell(qRelayFieldList, "fieldMask", phoneMask);*/
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "website");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_WebSiteAddress");
			/*newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insUsername");
				temp = QuerySetCell(qRelayFieldList, "type", "textInput");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Username");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insSex");
				temp = QuerySetCell(qRelayFieldList, "type", "gender");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_Gender");
			newRow  = QueryAddRow(qRelayFieldList);
				temp = QuerySetCell(qRelayFieldList, "fieldName", "insDOB");
				temp = QuerySetCell(qRelayFieldList, "type", "dateOfBirth");
				temp = QuerySetCell(qRelayFieldList, "params", "fieldSize=50,maxfieldsize=100");
				temp = QuerySetCell(qRelayFieldList, "defaultFieldLabel", "Phr_Ext_DateOfBirth");*/
			</cfscript>

			<cfoutput>
				<cfif request.relaycurrentuser.isloggedin eq 0>
					<script language="javascript">
						jQuery(function(){
							document.details.submit.disabled = false;
						})
						function validateCaptcha() {

							if ("<cfoutput>#jsStringFormat(showCaptcha)#</cfoutput>" == "false") {
								return true;
							} else {
								userResponse=document.details.frmcaptcha.value;
								captchaResponse=document.details.frmcaptcha_hash.value;

								var timeNow = new Date();
								page = '/WebServices/relaycaptchaWS.cfc?';
								captchaParameters  = 'returnFormat=JSON&method=getcaptchavalidation&time='+timeNow+ '&userResponse='+userResponse+'&captchaResponse='+captchaResponse;

								var myAjax = new Ajax.Request(
									page,
									{
											method: 'get',
											asynchronous: false,
											parameters: captchaParameters,
											evalJSON: 'force',
											debug : false
									}
								)
								var captchaResponse = myAjax.transport.responseText.evalJSON();

								if (captchaResponse) {
									return true;
								} else {

									var timeNow = new Date();
									page = '/webservices/relaycaptchaWS.cfc?';
									updatercaptchaParameters  = 'method=drawCaptcha&captchaerror=true&time='+timeNow;

									var div = 'captchadiv';
									var myAjax = new Ajax.Updater(
										div,
										page,
										{
											method: 'get',
											parameters: updatercaptchaParameters,
											evalJSON: 'force',
											debug : false,
											onComplete: function () {document.details.frmcaptcha.focus();}
										});
									return false;

								}
							}
						}
					</script>
				</cfif>
				<div id="orgProfileLower">
					<form action="#CGI.SCRIPT_NAME#?#request.query_string#" onsubmit="return validateCaptcha();" method="POST" name="details">
					 	<cf_relayFormDisplay class="screenTable">
						<cfset tabindex=1>

						<cfset orderCount = 0>
						<cfloop list="#showfields#" index="i">
							<cfset orderCount ++>
								<cfquery name="qRelayFieldListOrdered" dbtype="query">
								  select fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,sortorder from qRelayFieldListOrdered
								  union all
								  select fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,'#orderCount#' as sortorder
								  from qRelayFieldList
							  where lower(fieldname) = '#lcase(i)#'
								</cfquery>
						</cfloop>
						<!--- 2014-01-23	AXA	Case 438629 - added sortorder to select list  --->
						<cfquery name="qRelayFieldList" dbtype="query">
						  select fieldName,type,helpText,params,defaultFieldLabel,fieldmask,regexPattern,sortorder
						  from qRelayFieldListOrdered
						</cfquery>

						<cfloop query="qRelayFieldList">
							<!--- get any name value pairs defined in params and make them available to this scope --->
							<CF_EvaluateNameValuePair NameValuePairs = "#qRelayFieldList.params#">
						 	<cfif listfindNocase(showFields,qRelayFieldList.fieldName) neq 0>
								<cfif listfindNocase(mandatoryFields,qRelayFieldList.fieldName) neq 0>
									<cfset isMandatory = "Yes">
								<cfelse>
									<cfset isMandatory = "No">
								</cfif>

								<cfif left(defaultFieldLabel,3) is "phr">
									<cfset thisfieldlabel = defaultFieldLabel>
								<cfelseif application.com.relayTranslations.doesPhraseTextIDExist("phr_#qRelayFieldList.fieldName#") and qRelayFieldList.fieldName neq application.com.relayTranslations.translatePhrase(phrase="phr_#qRelayFieldList.fieldName#")>
									<cfset thisFieldLabel="phr_#qRelayFieldList.fieldName#">
								<cfelse>
									<cfset thisfieldlabel = #qRelayFieldList.defaultFieldLabel#>
								</cfif>

								<cfset pattern = "">
								<cfset validate = "">
								<cfif qRelayFieldList.regexPattern neq "">
									<cfset pattern = qRelayFieldList.regexPattern>
									<cfset validate = "regex">
								</cfif>

								<cfswitch expression="#qRelayFieldList.type#">

									<cfcase value="textInput">
										<!--- <cfif fieldName eq "insVATnumber">
											<cfif (application.com.settings.getSetting("plo.hideVATNumberFieldWhenNotMandatory") and isMandatory eq 1) or not application.com.settings.getSetting("plo.hideVATNumberFieldWhenNotMandatory")> <!--- 2014-01-23	AXA	Case 438629 - changed the if statement  --->
												<cfset VATvalidate = "">
												<cfset VATpattern = "">
												<cfif countryDetails.VATformat neq "">
													<cfset VATvalidate = "regular_expression">
													<cfset VATpattern = countryDetails.VATformat>
												</cfif>
												<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#VATvalidate#" pattern="#VATpattern#">
											</cfif>
										<cfelseif fieldName eq "insTelephone" or fieldName eq "insOfficePhone"  or fieldName eq "insMobilePhone" or fieldName eq "insFax" > --->
										<cfif listFindNoCase("officePhone,mobilePhone",fieldName)>
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" Mask="#qRelayFieldList.fieldMask#">
										<cfelse>
											<CF_relayFormElementDisplay relayFormElementType="text" fieldName="#qRelayFieldList.fieldName#" currentValue="" label="#thisfieldlabel#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#" required="#isMandatory#" validate="#validate#" pattern="#pattern#">
										</cfif>
									</cfcase>
									<cfcase value="Province">
										<cfquery name="GetFullNameSwitch" datasource="#application.siteDataSource#">
											select ProvinceAbbreviation
											from country
											where countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
										</cfquery>
										<cfquery name="getProvince" datasource="#application.siteDataSource#">
											select p.name, p.Abbreviation
											from Province p
											where countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
											order by p.name
										</cfquery>
										<cfif getProvince.recordcount EQ 0>
											<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
										<cfelse>
											<cfif GetFullNameSwitch.ProvinceAbbreviation EQ "" or GetFullNameSwitch.ProvinceAbbreviation EQ "false">
												<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="name" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
											<cfelse>
												<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getProvince#" display="Abbreviation" value="Abbreviation" nullText="Phr_Ext_SelectAValue" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
											</cfif>
										</cfif>
									</cfcase>
									<cfcase value="Region">
										<cfquery name="getRegion" datasource="#application.siteDataSource#">
											select r.name
											from Region r
											where countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
											order by r.name
										</cfquery>
										<cfif getRegion.recordcount EQ 0>
											<cf_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" required="yes" size="#fieldSize#" maxlength="#maxFieldSize#" tabindex="#tabIndex#">
										<cfelse>
											<cf_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#getRegion#" display="name" value="name" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
										</cfif>
									</cfcase>

									<cfcase value="validValueList">
										<cfif isDefined("validFieldName")>
											<cf_getValidValues validFieldName = "#validFieldName#" countryID = "#countryID#"> <!--- 2014-01-23	AXA	Case 438629 - added doublequotes around inscountryid  --->
											<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="#qRelayFieldList.fieldName#" label="#thisfieldlabel#" query="#validvalues#" display="displayvalue" value="dataValue" nullText="" nullValue="" size="1" required="#isMandatory#" tabindex="#tabIndex#">
										<cfelse>
											You must define a validFieldName for a validValueList form element type.
										</cfif>
									</cfcase>

									<cfcase value="countryList">
										<cfquery name="getCountries" datasource="#application.siteDataSource#">
											SELECT DISTINCT CountryID, CountryDescription
												  FROM Country
												  where ISOCode is not null
											ORDER BY CountryDescription ASC
										</cfquery>
										<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#countryID#" fieldName="countryID" size="1" query="#getCountries#" display="CountryDescription" value="CountryID" label="phr_Ext_Country" disabled="yes" tabindex="#tabIndex#">
										<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#countryID#" fieldname="countryID">
									</cfcase>

									<cfcase value="email">
										<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" fieldName="email" label="#thisfieldlabel#" validate="email" onValidate="validateEmail" required="#isMandatory#" size="#fieldSize#" maxlength="#maxFieldSize#" personid=0 tabindex="#tabIndex#" orgType="#application.organisationType.endCustomer.organisationTypeID#">	   <!--- WAB 2008/09/24 CR-TND561 Req8 personid required for unique email validation, in this case it is a new record so set to 0--->
									</cfcase>

									<!--- <cfcase value="gender">
										<cf_querySim>
											getStati
											display,value
											Phr_male|M
											Phr_female|F
										</cf_querySim>
										<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="" fieldName="insSex" label="#thisfieldlabel#" query="#getStati#" display="display" value="value" tabindex="#tabIndex#">
									</cfcase>

									<cfcase value="dateOfBirth">
										<cfSaveContent variable="DOBDataHTML">
											<cfselect name="insDOBDay" size="1" message="Enter Your Day of Birth" required="#isMandatory#" tabindex="#tabIndex#">
													<option value="" SELECTED>Day</option>
												<cfloop index="i" from="1" to="31" step="1">
													<option value="#i#" >#htmleditformat(i)#</option>
												</cfloop>
											</cfselect>
											<cfselect name="insDOBMonth" size="1"  message="Enter Your Month of Birth" required="#isMandatory#" tabindex="#tabIndex#">
											<cfset j = 1>
													<option value="" >Month</option>
												<cfloop index="i" list="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec">
													<option value="#j#" >#htmleditformat(i)#</option>
													<cfset j = j + 1>
												</cfloop>
											</cfselect>
											<cfinput type="Text" name="insDOBYear" range="1930,1988" message="Enter Your Year of Birth (between 1930-1988)" required="#isMandatory#" size="4" tabindex="#tabIndex#">
										</cfsavecontent>
										<CF_relayFormElementDisplay relayFormElementType="HTML" label="#thisfieldlabel#" currentValue="#DOBDataHTML#" tabindex="#tabIndex#">
									</cfcase> --->
								</cfswitch>
							</cfif>
						</cfloop>

						<cfif showCaptcha>
							<tr>
								<td colspan="2">Phr_Ext_Captcha_instructions</td>
							</tr>
							<tr>
								<td valign="bottom">&nbsp;</td>
								<td>
								<div id="captchadiv">
									#application.com.commonQueries.drawCaptcha()#
								</div>
								</td>
							</tr>
				 		</cfif>

                <!---ESZ Web to Lead Form Configurable fields--->
                <cfif UserDefinedScreenTextID neq "" &&  isDefined("countryID") && "#countryID#" GT 0>
                    <cf_getrecord
                            entityid = "newLead"
                            entityType="lead"
                            countryid="#countryID#"
                            organisationid="#vendorOrganisationID#"
                            locationid = "#vendorLocationID#">

                    <cfif isDefined("UserDefinedScreenTextID")>
                        <CF_aSCREEN formName = "User-Defined Screen TextID">
                            <CF_aScreenItem
                                    screenid="#UserDefinedScreenTextID#"
                                    lead = #lead#
                                    method="edit"
                                    >
                        </CF_aSCREEN>
                    </cfif>
                </cfif>

                <CF_relayFormElement relayFormElementType="hidden" label="" fieldname="partnerlocationID" currentvalue="#vendorLocationID#">
                <CF_relayFormElement relayFormElementType="hidden" label="" fieldname="partnerorganisationid" currentvalue="#vendorOrganisationID#">

						<cf_encryptHiddenFields>
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="save" currentvalue="phr_orgProfile_submit">
							<!--- <CF_relayFormElement relayFormElementType="hidden" label="" fieldname="oppTypeID" currentvalue="#oppTypeID#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="sourceID" currentvalue="#oppSourceID#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="stageID" currentvalue="#oppStageID#"> --->
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="vendorLocationID" currentvalue="#vendorLocationID#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmOrganisationID" currentvalue="#vendorOrganisationID#">
							<!--- <CF_relayFormElement relayFormElementType="hidden" label="" fieldname="entityID" currentvalue=""> --->
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="countryID" currentvalue="#countryID#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmShowLanguageID" currentvalue="#application.com.relayTranslations.getUsersPreferredLangFromBrowser()#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="insCountryID" currentvalue="#countryID#">
							<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="insOrganisationTypeID" currentvalue="#orgTypeID#">
						</cf_encryptHiddenFields>

						<cfif isDefined("includeCustomFile") and fileexists("#application.paths.code#\cftemplates\#includeCustomFile#")>
							<cfinclude template="/code/cftemplates/#includeCustomFile#">
						</cfif>

						<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="submit" currentValue="phr_orgProfile_submit" label="" align="center">
						</cf_relayFormDisplay>
					</form>
				</div>
		</cfoutput>
	</cfif>
<cfelse>
	<cfoutput>#application.com.relayUI.message(message="This tag must be used on a logged out page.",type="info")#</cfoutput>
</cfif>