<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 


newsletter items
			2010/10/05  WAB         replaced application.userfiles absolutepath & application.files directory with application.paths.content

 --->
 
 
 
<!--- get the tree of items in the newsletter 
		if it has not already been got on this request
--->
<!--- 	<cfquery name="xx" datasource="#application.sitedatasource#" >
	update communication set locked = 0 where commid = 2673
	</cfquery>
 --->
 

<cfparam name = "request.newsletterDefinition" default = "#structNew()#">

<cfparam name="templateParams" default = #structNew()#>
<cfset templateParams.image1.height = 234>
<cfset templateParams.image1.width = 100>
<cfset templateParams.image2.height = 234>
<cfset templateParams.image2.width = 100>

	<cfif not structKeyExists(request.newsletterDefinition,newsletterid)>
			<cfset request.newsletterDefinition[newsletterid] = structNew()>
			
			<cfquery name="fullTree" datasource="#application.sitedatasource#" >
				exec GetElementTree 
					@ElementID =  <cf_queryparam value="#newsletterid#" CFSQLTYPE="CF_SQL_INTEGER" > , 
					@personid=-1,  <!--- tree regardless of rights --->
					@depth = 0,    <!--- full depth --->
					@statusid = -1,	<!--- any status --->
					@date = null   <!--- any date --->
			</cfquery>

<!--- 			<cfset flagQuery = application.com.relayElementTree.getElementFlagData(valuelist(fulltree.node))> --->

			<cfif isDefined("previewItems")>
				<cfquery name="fulltree" dbtype="query" >
					select *
					from fulltree
					where node = #newsletterid# or node in (#previewItems#)
				</cfquery>
			</cfif>

			
			<cfquery name="fulltree" dbtype="query" >
			select node,elementtypeid,headline , sortorder,
				'phr_detail_element_' + contentidAsstring as detail,
				'phr_longsummary_element_' + contentidAsstring as secondaryContent, 
				'phr_longsummaryheading_element_' + contentidAsstring as secondaryContentHeader ,
				'' as image1,
				'' as image2

			from fulltree
			</cfquery>

			<cf_translateQueryColumn query = "#fulltree#" columnname = "detail" >
			<cf_translateQueryColumn query = "#fulltree#" columnname = "headline">			
			<cf_translateQueryColumn query = "#fulltree#" columnname = "secondaryContent">			
			<cf_translateQueryColumn query = "#fulltree#" columnname = "secondaryContentHeader" forcereload = true>			
			

<!--- fill in all the blanks --->
			<cfloop query="fulltree">
				<!--- check for images associated with items --->
				<cfset linkImagePath1=application.userfilesabsolutepath & "content\elementLinkImages\linkImage_1-" & node & "-orig.jpg">
				<cfset linkImagePath2=application.userfilesabsolutepath & "content\elementLinkImages\linkImage_2-" & node & "-orig.jpg">
				<cfif fileexists(linkImagePath1)>
					<cfset imageTag = '<img src="/content/elementLinkImages/linkImage_1-#node#-orig.jpg" border=0 >'>
				<cfelse>
					<!--- placeholder image --->
					<cfset imagename=  "el_" & node & "_image1.png">
					<cfset imagepath=  "\newsletter\collateral\" & imageName>
					<cfset imageFullPath = application.paths.content & imagepath>
					<cfset imageURL = request.currentSite.httpProtocol & sitedomain & "/content" & imagepath>
					<cfset image = application.com.relayImage.createPlaceholderImage(imageHeight=templateParams.image1.height,imageWidth=templateParams.image1.height,imageText="Image 1, Item #node#,  #headline#",imagePath=imageFullPath,overwrite=true)>
					<cfif image eq "Created successfully" or image eq "Image already exists">
						<cfset imageTag = "<img src='#imageURL#'  border=0>">
						<!--- image fail? --->
					<cfelse>
						<cfset imageTag = "">
					</cfif>
				</cfif>
				<cfset querysetcell(fulltree,"image1",imageTag,currentrow)>	


				<cfif fileexists(linkImagePath2)>
					<cfset ImageTag = '<img src="/content/elementLinkImages/linkImage_2-#node#-orig.jpg" border=0 >'>
				<cfelse>
					<!--- placeholder image --->
					<cfset imagename=  "el_" & node & "_image2.png">
					<cfset imagepath=  "\newsletter\collateral\" & imageName>
					<cfset imageFullPath = application.paths.content & imagepath>
					<cfset imageURL = request.currentSite.httpProtocol & sitedomain & "/content" & imagepath>
					<cfset image = application.com.relayImage.createPlaceholderImage(imageHeight=templateParams.image2.height,imageWidth=templateParams.image2.height,imageText="#headline# Image 2, Item #node#, ",imagePath=imageFullPath,overwrite=true)>
					<cfif image eq "Created successfully" or image eq "Image already exists">
						<cfset imageTag = "<img src='#imageURL#'  border=0>">
						<!--- image fail? --->
					<cfelse>
						<cfset imageTag = "">
					</cfif>
				</cfif>
					<cfset querysetcell(fulltree,"image2",imageTag,currentrow)>	

			</cfloop>


			<cfset request.newsletterDefinition[newsletterid].fulltree = fulltree>
			
</cfif>				

			<cfset fullTree = request.newsletterDefinition[newsletterid].fulltree>
		<!--- get personal weightings --->
			<cfset weightingQuery= application.com.relayElementTree.getElementWeightingsForAPerson (elementIdlist = valuelist(fulltree.node),personid = request.commitem.personid)>

			<!--- brings back the newsletter record (ie top of tree) --->
			<cfquery name="header" dbtype="query" maxrows =1 >
			select * from fulltree order by sortorder
			</cfquery>
			
			<cfquery name="weightedTree" dbtype="query">
			select fulltree.* , flagweighting
			from 
			fulltree , weightingquery
			where 
				fulltree.node = weightingQuery.id
			order by weightingquery.flagweighting desc
			</cfquery>
			<cfif weightedTree.recordCOunt lte 1>No items in Newsletter <CF_ABORT></cfif>
			<cfif isDefined("previewItems")>			
				<cfloop condition="weightedTree.recordcount lt 10">
					<cfquery name="weightedTree" dbtype="query"  >
					select * from weightedTree
						union all
					select *  from weightedTree where node <> #newsletterid# 
					</cfquery>
				</cfloop>

			</cfif>
			

			<cfquery name="lead" dbtype="query">
			select * 
			from 
			weightedTree
			where 
				elementTypeid = 55
			order by flagweighting desc
			</cfquery>



			<cfquery name="articles" dbtype="query">
			select * 
			from 
			weightedTree
			where 
				elementTypeid = 52
			order by flagweighting desc
			</cfquery>

			<cfquery name="promotions" dbtype="query">
			select * 
			from 
				weightedTree
			where 
				elementTypeid = 53
			order by flagweighting	desc
			</cfquery>


<!--- 
		reorder based on the current person
 ---> 
 

 
 






<cfoutput>
	<table cellpadding="5" cellspacing="0" width="534">

		<tr><td colspan=2>#header.detail# </td></tr>		
		<!--- 1 lead  stories --->
		<cfloop query="lead" startrow="1" endrow="1" >
			<tr>
				<td>
					#htmleditformat(image1)#
				</td>
				<td valign="top">
					<B>#htmleditformat(headline)#</B>(#htmleditformat(node)#) <BR>
					#detail#
				</td>
			</tr>
		</cfloop>


		<!--- 3 main stories --->
		<cfloop query="articles" startrow="1" endrow="2" >
			<tr>
				<td>
					#htmleditformat(image1)#
				</td>
				<td valign="top">
					<B>#htmleditformat(headline)#</B>(#htmleditformat(node)#) <BR>
					#detail#
				</td>
			</tr>
		</cfloop>
		
		<!--- promotion 1 --->
		<cfloop query="promotions" startrow="1" endrow="1" >
		<tr><td colspan="2" align="center">Promotion Position 1 Element #htmleditformat(node)# <BR>
			#detail#
		</td></tr>
		<tr><td colspan="2" align="center">
		#htmleditformat(image1)# 	</td></tr>
		</cfloop>
				
		<!--- 3 secondary sub stories --->
		<cfloop query="articles" startrow="3" endrow="6" >
			<tr>
				<td valign="top">
					<B>#htmleditformat(secondaryContentHeader)#</B> (#htmleditformat(node)#) <BR>
					#htmleditformat(secondaryContent)#
				</td>
				<td align="right">
					#htmleditformat(image2)#
				</td>
			</tr>
		</cfloop>
		
		<!--- promotion 2 --->
		<cfloop query="promotions" startrow="2" endrow="2" >
		<tr><td colspan="2" align="center">Promotion POsition 2 Element #htmleditformat(node)# <BR> #htmleditformat(image1)#</td></tr>
		</cfloop>
	
	</table>
</cfoutput>


<cfdump var="#weightingQuery#">

