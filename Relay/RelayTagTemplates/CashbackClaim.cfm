<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			incentiveCashbackClaim.cfm - based on incentivePointsClaim.cfm
Author:				NJH
Date started:		2009-03-03
	
Description:		A file based on incentivePointsClaim whereby resellers can claim cashback for selling certain products as set up in a catalogue.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfinclude template="\cashback\applicationCashbackINI.cfm">

<cfset hideRightBorder = "yes">

<cfinclude template="/cashback/CashbackClaim.cfm">
