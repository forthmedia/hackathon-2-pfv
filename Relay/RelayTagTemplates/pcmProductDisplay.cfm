<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayProductDisplay.cfm	
Author:			NJH
Date started:		2006/09/26
	
Description:		This provides.

Usage:			

Amendment History:

Date 		Initials 	What was changed
START: AJC 2008/12/19 Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories
2009/04/01 Issue 2028  SSS        The getProductID query now gets the product for the tree not from a country ISO Code
2012/04/11 	PPB 		Case 427340 

Enhancements still to do:


 --->
 
<cfif structkeyexists(url,"productID") and not isDefined('displayCriteria')>
	<cfset displayCriteria = url.productID>
	
<cfelseif structkeyexists(url,"pn")>

	<cf_include template="\code\cftemplates\pcmINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\pcmINI.cfm")>
	<!--- pcmINI is used to over-ride default global application variables and params --->
	<cf_includeonce template="/code/cftemplates/pcmINI.cfm">
</cfif> --->

	<cfquery name="getProductID" datasource="#application.siteDataSource#">
		select p.productID,pcp.treeID from pcmProduct p 
			inner join pcmProductCategoryProduct pcp on p.productID = pcp.productID
			inner join pcmProductTree t on pcp.treeID = t.treeID
			inner join pcmProductTreeSource ts on t.sourceID = ts.sourceID
		where p.productKey =  <cf_queryparam value="#url.pn#" CFSQLTYPE="cf_sql_varchar" > 
		<!--- BUG START 2028 SSS 2009/04/01 left the pcc key structure just in case it is used the tree now searches on the treeID not the countryCode--->
		<!--- <cfif request.currentSite.name EQ "lime">
			and p.countryCode= 'ME'
		<cfelse> 
			<cfif structkeyexists(url,"pcc")>
				and p.countryCode =  <cf_queryparam value="#url.pcc#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			<cfelse>
				and p.countryCode='#request.countryISOcode#'
			</cfif> 
		</cfif> --->
		<cfif structkeyexists(url,"pcc")>
			and p.countryCode =  <cf_queryparam value="#url.pcc#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<cfelse>
			and t.treeID =  <cf_queryparam value="#Request.pcmtreeid#" CFSQLTYPE="CF_SQL_INTEGER" > 		
		</cfif>
		<!--- BUG END 2028 --->
		<cfif structkeyexists(url,"plc")>
			and p.languageCode =  <cf_queryparam value="#url.plc#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfif>
		<!--- START:2012/04/11 PPB Case 427340 this extra filtering was causing no rows to be found; Alistair says we're not bothered about source (the raw data ie sourceId=1 is ok)	
		<cfif structKeyExists(url,"withdrawn")>
			and ts.sourceID=4
		<cfelse>
			and ts.sourceID=3
		</cfif>
			and ts.sourceGroupID=2 
		END:2012/04/11 PPB Case 427340  --->			
	</cfquery>

	<cfif getProductID.recordCount gt 0>
		<cfset displayCriteria = getProductID.productID[1]>
		<!--- if the language and country is passed, set the treeID, otherwise determine later on what the tree should be based on the base url --->
		<cfif structKeyExists(url,"pcc") and structKeyExists(url,"plc")>
			<cfset pcmTreeID = getProductID.treeID[1]>
		</cfif>
	</cfif>

<cfelseif structKeyExists(url,"pcmCategory") and structKeyExists(url,"plc") and structKeyExists(url,"pcc")>
	<cfset searchMode = "category">
	<cfset displayCriteria = url.pcmCategory>
	
	<cfif not isNumeric(displayCriteria)>
		<cfquery name="getCategoryID" datasource="#application.siteDataSource#">
			select remoteCategoryID, t.treeID from pcmProductCategory pc
				inner join pcmProductTree t on pc.treeID = t.treeID
				inner join pcmProductTreeSource ts on t.sourceID = ts.sourceID
			where replace(pc.categoryName,' ','') =  <cf_queryparam value="#url.pcmCategory#" CFSQLTYPE="CF_SQL_VARCHAR" >  and t.countryCode =  <cf_queryparam value="#url.pcc#" CFSQLTYPE="CF_SQL_VARCHAR" >  and t.languageCode =  <cf_queryparam value="#url.plc#" CFSQLTYPE="CF_SQL_VARCHAR" >  and ts.sourceGroupID=2
			
		</cfquery>
	
		<cfif getCategoryID.recordCount gt 0>
			<cfset displayCriteria = getCategoryID.remoteCategoryID[1]>
			<cfset pcmTreeID = getCategoryID.treeID[1]>
		</cfif>
	</cfif>
</cfif>

<cfif structkeyexists(url,"navID") and not isDefined('pcmTreeID')>
	<cfset pcmTreeID = url.navID>
</cfif>

<cf_param name="pcmTreeID" type="numeric" default="0"/>
<cf_param name="displayMode" type="string" default="simple" validvalues="categoryGrouping,simple,simpleWithIcons"/>
<cf_param name="searchMode" type="string" default="product" validvalues="category,product,range"/>	<!--- search by products, category or by range --->
<cf_param name="showWithdrawnTree" type="boolean" default="no"/>
<cf_param name="displayCriteria" type="string" default="0" description="Holds the id's of whatever is being searched for: categories,products,ranges"/>
<cf_param name="elementsPerRow" type="numeric" default="4"/>
<cf_param name="productTextSectionTypeID" type="numeric" default="1" description="Which product text section type to show"/>	<!--- which product text section type to show --->
<cfparam name="request.searchableRemoteCategoryIDList" type="string" default=""> <!--- set in pcmINI.cfm --->
<cfparam name="request.globalMaskSectionTypeIDList" type="string" default="">
<cf_param name="showBaseModelsOnly" type="boolean" default="yes"/>

<cf_param name="remoteCategoryID" type="numeric" default="0"/>

<cfif fileexists("#application.paths.code#\cftemplates\pcmINI.cfm")>
	<!--- pcmINI is used to over-ride default global application variables and params --->
	<cfinclude template="/code/cftemplates/pcmINI.cfm">
</cfif>

<cfif pcmTreeID eq 0>
	<cfif not isDefined("request.pcmTreeID")>
		<cfoutput>phr_product_NoProductTreeConfiguredForThisSite.</cfoutput>
	<cfelse>
		<cfset pcmTreeID = request.pcmTreeID>
	</cfif>
</cfif>

<cfif pcmTreeID neq 0>		

	<!--- remote category might be set by a user in the front end... It's a 'nicer' param than 'displayCriteria' --->
	<cfif (isDefined("remoteCategoryID") and remoteCategoryID neq 0) and not isDefined("url.displayCriteria")>
		<cfset displayCriteria = remoteCategoryID>
		<cfif searchMode neq "category">
			<cfset searchMode = "category">
		</cfif>
	</cfif>

		<script>
			function viewProductDetails(productID,prodSectionTypeID,prodTreeID,prodDisplayMode,options) {
				options = options||{};
				displayProductForm.displayCriteria.value=productID;
				displayProductForm.productTextSectionTypeID.value=prodSectionTypeID;
				displayProductForm.pcmTreeID.value=prodTreeID;
				displayProductForm.displayMode.value=prodDisplayMode;	
				if (options.prodAction && options.prodAction != "") {
					displayProductForm.action = options.prodAction;
				}		
				displayProductForm.submit();
			}
		</script>

	<!--- 
		A form that submits to itself... used when selecting a product from a category or range
		to view the product details.
	 --->
	<cfform name="displayProductForm"  method="get">
		<cfinput type="hidden" name="pcmTreeID" value="#pcmTreeID#">
		<cfif isDefined("request.currentelement.elementTextID") and request.currentelement.elementTextID neq "">
			<cfinput type="hidden" name="eid" value="#request.currentelement.elementTextID#">
		<cfelseif isDefined("request.currentelement.ID")>
			<cfinput type="hidden" name="eid" value="#request.currentelement.ID#">
		<cfelse>
			<cfinput type="hidden" name="eid" value="">
		</cfif>
		<cfinput type="hidden" name="searchMode" value="product">
		<cfinput type="hidden" name="displayCriteria" value="0">
		<cfinput type="hidden" name="productTextSectionTypeID" value="#productTextSectionTypeID#">
		<cfinput type="hidden" name="displayMode" value="#displayMode#">
		<cfinput type="hidden" name="showWithdrawnTree" value="#showWithdrawnTree#">
	</cfform>
	
	<cfif isDefined("form.searchMode")>
		<cfset searchMode = form.searchMode>
	</cfif>

	<!--- if an ID hasn't been passed, grab the last one so that a product is displayed everytime... --->
	<cfif (displayCriteria is 0 or displayCriteria is "") and isDefined("session.pcmLastSearch")>
		<cfset searchMode = session.pcmLastSearch.searchMode >
		<cfset displayCriteria = session.pcmLastSearch.displayCriteria >
	<cfelse>
		<cfset session.pcmLastSearch = structNew()>
		<cfset session.pcmLastSearch.searchMode = searchMode>
		<cfset session.pcmLastSearch.displayCriteria = displayCriteria>
	</cfif>

	<cfswitch expression="#searchMode#">
	
		<!--- Display product details --->
		<cfcase value="product">
			<cfif not isNumeric(displayCriteria) or displayCriteria eq 0>
				<cfoutput>A valid product ID must be passed.</cfoutput>
				<CF_ABORT>
			</cfif>
			
			<cfif listContains(request.globalMaskSectionTypeIDList,productTextSectionTypeID)>
				<cfset useGlobalMask = true>
			<cfelse>
				<cfset useGlobalMask = false>
			</cfif>
	
			<cfset productMask = application.com.relayProductTree.getProductMask(pcmTreeID,displayCriteria,useGlobalMask)>
			<cfset productDetails = application.com.relayProductTree.getProductDetails(pcmTreeID,displayCriteria,productMask)>
			<!--- <cfset sectionDetails = application.com.relayProductTree.getProductSectionDetails(productID=displayCriteria,maskID=productMask,sectionTypeID=productTextSectionTypeID)> --->
			<cfset productSectionDetails = productDetails.sections>
	
			<cfquery name="sectionDetails" dbtype="query">
				select * from productSectionDetails where sectionTypeID = #productTextSectionTypeID#
			</cfquery>
	
			<cfif isDefined("productDetails")>
				<!--- check if user ProductDisplay file exists. If not, display ??? --->
				<cfif fileExists(application.paths.code & "\cftemplates\ProductDisplay.cfm")>
					<cfinclude template="\code\cftemplates\ProductDisplay.cfm">
				</cfif>
			<cfelse>
				<cfoutput>We're sorry, but no information is available for this product.</cfoutput>
			</cfif>
		</cfcase>
		
		<!--- Display products by category --->
		<cfcase value="category">
			<cfif not isNumeric(displayCriteria)>
				<cfoutput>A valid category ID must be passed.</cfoutput>
				<CF_ABORT>
			</cfif>
			
			<!--- START: AJC 2008/12/19 Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
			<cfset category = application.com.relayProductTree.selectCategory(treeID=pcmTreeID,categoryID=displayCriteria,mode="remote",showDeleted=false)>
			<!--- END: AJC 2008/12/19 Issue 1425: LPMD Feed > CVP AFrica - Was getting Deleted Categories --->
			
			<cfif category.recordCount gt 0>
				<cfset catProdQry = application.com.relayProductTree.selectCategoryProducts(treeID=pcmTreeID,categoryID=category.categoryID,orderby="title",baseModelsOnly=showBaseModelsOnly)>
			
				<cfif catProdQry.recordCount gt 0>
					<!--- check if user categoryProductDisplay file exists. If not, display ??? --->
					<cfif fileExists(application.paths.code & "\cftemplates\categoryProductDisplay.cfm")>
						<cfinclude template="\code\cftemplates\categoryProductDisplay.cfm">
					</cfif>
				</cfif>
			<cfelse>
				<cfoutput>
					We're sorry, but the category you are searching for could not be found in the product tree(#htmleditformat(pcmTreeID)#).
				</cfoutput>
			</cfif>
	
		</cfcase>
		
		
		<!--- Display products by range --->
		<cfcase value="range">
			<cfif displayCriteria eq "">
				<cfoutput>A valid range value must be passed.</cfoutput>
				<CF_ABORT>
			</cfif>
			
			<!--- <cfset productDetails = application.com.relayProductTree.getProductDetails(displayCriteria)> --->
			
			<!--- check if user ProductDisplay file exists. If not, display ??? --->
			<cfif fileExists(application.paths.code & "\cftemplates\rangeProductDisplay.cfm")>
				<cfinclude template="\code\cftemplates\rangeProductDisplay.cfm">
			</cfif>
		</cfcase>
		
	</cfswitch>

</cfif>


