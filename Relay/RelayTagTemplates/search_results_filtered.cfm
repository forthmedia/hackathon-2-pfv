<!--- �Relayware. All Rights Reserved 2015 --->
<!--- Display portal search results where the config can be filtered based on files, Products, Courses and All.
2015/05/11 SB - Creation of relaytag.
2015/09/22 DAN    839 - eFolder fixes for tag RELAY_SEARCH_RESULTS_FILTERED
2015/09/30 DAN    PROD2015-77 - add character limit to the search input

--->
<cf_param label="Search Filter" name="searchfilter" default="All" validValues="All,Files,Products,Courses" />
<cf_param label="Search Filter Target" name="searchfilterTarget" default="Same Page" validValues="Same Page,New Page" />

<CFPARAM NAME="form.maxRows" DEFAULT="25">
<CFPARAM NAME="form.startRow" DEFAULT="1">
<CFPARAM NAME="form.thisPage" DEFAULT="1">
<cfparam name="frmCriteria" default="">
<cfparam name="frmSourceID" default="">

<cfparam name="searchResultsEID" default="searchresults"> <!--- note this the another existing page EID used if target is New Page --->
<cfparam name="searchFilterTargetBlank" default="#request.currentsite.protocolAndDomain#/et.cfm?eid=#searchResultsEID#">

<cfset qSources = application.com.search.getPortalSearchSources(LanguageID=request.relaycurrentuser.languageid)>


<!--- SB Created a switch between the different filters. --->
<cfswitch expression="#searchfilter#">
	<cfcase value="files"><cfset frmSourceID=2></cfcase>
	<cfcase value="products"><cfset frmSourceID=3></cfcase>
	<cfcase value="courses"><cfset frmSourceID=4></cfcase>
	<cfdefaultcase></cfdefaultcase>
</cfswitch>

<cfswitch expression="#searchfilterTarget#">
	<cfcase value="Same Page"><cfset searchFilterTargetBlank = ""></cfcase>
	<cfdefaultcase></cfdefaultcase>
</cfswitch>

<CFIF frmCriteria is not "">
	<cfset qSearchResult = application.com.search.getPortalSearchResults(searchTextString=frmCriteria,portalSearchID=frmSourceID)>

	<cfset application.com.commonqueries.insertTrackSearch (searchtype = "V",searchstring=frmCriteria, personid = request.relayCurrentUser.personID)>
	<cfset stCount = {}>
	<cfquery name="qResult" dbtype="query">
		select count(portalSearchID) as cCount, portalSearchID
		from qSearchResult
		group by portalSearchID
	</cfquery>
	<cfloop query="qResult"><cfset stCount[portalSearchID] = cCount></cfloop>
	<cfset results=true>
<CFELSE>
	<cfset results=false>
</CFIF>

<cf_head>
<!--- <cf_title>phr_sys_search_searchResults</cf_title> --->


<SCRIPT type="text/javascript">
function getSelectedRadio(buttonGroup) {
   // returns the array number of the selected radio button or -1 if no button is selected
   if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   // if we get to this point, no radio button is selected
   return -1;
} // Ends the "getSelectedRadio" function

function getSelectedRadioValue(buttonGroup) {
   // returns the value of the selected radio button or "" if no button is selected
   var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
} // Ends the "getSelectedRadioValue" function
	function searchWindow(nPerson) {
		document.searchForm.submit();
	}
	function getFile(fileID) {
		openWin (' ','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
		form = document.getFileFormV2FilteredSearch;
		form.fileID.value = fileID;
		form.submit();
	}
</SCRIPT>
</cf_head>

<cfset fileFormV2ActionFilteredSearch = "/fileManagement/FileGet.cfm">
<cfif isDefined("isWebServiceCall") and isWebServiceCall>
	<cfset fileFormV2ActionFilteredSearch = "#fileFormV2ActionFilteredSearch#?#session.urltoken#">
</cfif>
<cfoutput>
<FORM METHOD="get" NAME="getFileFormV2FilteredSearch" action = "#fileFormV2ActionFilteredSearch#" target = "FileDownload">
	<input type="hidden" name="fileID" value="0">
	<input type="Hidden" NAME="FileTypeGroupID" value="0">
</FORM>
</cfoutput>

<cfif results and frmCriteria neq "">
	<h1><CFOUTPUT>#qSearchResult.recordcount# <cfif qSearchResult.recordcount is 1>phr_sys_search_ResultFor<cfelse>phr_sys_search_ResultsFor</cfif> "#htmleditformat(frmCriteria)#"</CFOUTPUT></h1>
<cfelse>
	<h1>phr_sys_search_searchResults</h1>
</cfif>

<div class="searchDiv">
	<div class="search-criteria-container">
		<div class="search-criteria" class="form-group">
			<cfoutput>
				<cfform method="POST" action="#searchFilterTargetBlank#" name="searchForm" id="searchForm">
					<cfinput type="hidden" name="StartRow" value="1">
					<div class="form-group row">
						<div class="col-xs-9 col-sm-10 col-md-11">
						<cfinput class="form-control" type="Text" name="frmCriteria" onfocus="this.value=''" value="phr_search_advanced_text" message="phr_sys_search_YouMustIncludeAsearchString" required="Yes" size="20" maxLength="255">
						</div>
						<div class="col-xs-3 col-sm-2 col-md-1">
							<cf_translate>
								<cfinput name="frmSubmitButton" type="submit" value=" phr_Go " id="submitButton" class="btn btn-primary">
							</cf_translate>
						</div>
					</div>
                    <cfif isdefined("frmSourceID")>
                        <cfinput type="hidden" name="frmSourceID" value="#frmSourceID#">
                    </cfif>
					<cfif isdefined("frmpersonid")>
						<cfinput type="hidden" name="frmpersonid" value="#frmpersonid#">
					</cfif>
				</cfform>
			</cfoutput>
		</div>
	</div>

	<cfif structKeyExists(url,"showProductID") and isNumeric(url.showProductID) and structKeyExists(url,"showCampaignID") and isNumeric(url.showCampaignID)>
		<cfset productid = url.showProductID>
		<cfset showBackLink = false>
		<cfset productCatalogueCampaignID = url.showCampaignID>
		<cfinclude template="productPicker.cfm">
	<cfelseif structKeyExists(url,"courseID") and isNumeric(url.courseID)>
		<cfinclude template="eLearningCourses2Level.cfm">
	</cfif>

	<cfif results>

		<cfif !(isDefined("qSearchResult") and qSearchResult.RecordCount eq 0 and isDefined("frmSources") and not structKeyExists(url,"showProductID"))>

			<div class="search-results">
				<CFSET ShowRow = 0>
				<CFOUTPUT query="qSearchResult" maxRows="#Form.MaxRows#" startrow="#form.startrow#">
					<CFIF ShowRow MOD 2 IS NOT 0><cfset nClass="oddrow"><CFELSE><cfset nClass="evenRow"></CFIF>
					<p class="search-results-row #nClass#">
						<cfswitch expression="#type#">
							<cfcase value="files"><cfset nhref = "javascript:getFile(#qSearchResult.key#)"></cfcase>
							<cfcase value="courses"><cfset nhref = application.com.security.encryptQueryString(querystring = path,  removed=false)></cfcase>
							<cfcase value="products"><cfset nhref = cgi.script_name & "?request.CurrentElement.id=#request.CurrentElement.id#&showProductID=#getToken(qSearchResult.key,3,'_')#&showCampaignID=#getToken(qSearchResult.key,4,'_')#"></cfcase>
							<cfdefaultcase><cfset nhref = path></cfdefaultcase>
						</cfswitch>

						<span class="search-results-title">
							<a href="#nhref#"><cfif Category is not "">#htmleditformat(evaluate(Category))# : </cfif>#qSearchResult.Title#</a>
						</span>
						<cfif isdefined("qSearchResult.custom1") and qSearchResult.custom1 is not "" and qSearchResult.custom1 is not qSearchResult.Title>  <!--- 2014-01-14 PPB Case 437761 don't list a 2nd line for qSearchResult.custom1 if it is exactly the same as the Title  --->
							<span class="search-results-custom1">#htmleditformat(qSearchResult.custom1)#</span>
						</cfif>
						<!--- <cfif showCurrentRow>
							<div class="searchResultsCurrentRow">#Form.StartRow + ShowRow#</div>
						</cfif> --->
						<CFSET ShowRow=ShowRow+1>
					</p>
				</CFOUTPUT>
			</div>

			<cfif ceiling(qSearchResult.recordcount/maxrows) gt 1>
				<div class="search-results-pagination">
					<cfset formName="nextpage2">
					<CFOUTPUT>
						<SCRIPT type="text/javascript">
							function submit#jsStringFormat(formName)#(nStRow,nPage){
								nform=document.#jsStringFormat(formName)#;
								nform.StartRow.value = nStRow;
								nform.thisPage.value = nPage;
								nform.submit();
							}
						</script>
						<FORM name="#formName#" action="/?eid=#eid#" method="post">
							<cfif startrow gt 1><a href="javascript:submit#formName#(#((thisPage-2)*maxrows)+1#,#thisPage-1#)">&lt;&lt; Previous Page</a></cfif>
							<cfset nPage = 1>
							<!--- phr_page --->
							<cfloop from=1 to=#qSearchResult.recordCount# step=#maxrows# index="i">
								<cfif thisPage neq nPage><a href="javascript:submit#formName#(#i#,#nPage#)">#htmleditformat(nPage)#</a><cfelse><strong class="currentPage">#htmleditformat(nPage)#</strong></cfif>&nbsp;&nbsp;
								<cfset nPage = nPage+1>
								<cfif nPage gt 20><cfbreak></cfif>
							</cfloop>
							<cfif thisPage lt nPage-1><a href="javascript:submit#formName#(#(thisPage*maxrows)+1#,#thisPage+1#)">Next Page &gt;&gt;</a></cfif>
							<CF_INPUT type="hidden" name="frmCriteria" value="#frmCriteria#">
							<CF_INPUT type="hidden" name="frmSourceID" value="#frmSourceID#">
							<CF_INPUT type="hidden" name="MaxRows" value="#Form.MaxRows#">
							<CF_INPUT type="hidden" name="StartRow" value="#Form.StartRow#">
							<CF_INPUT type="hidden" name="thisPage" value="#form.thisPage#">
							<CFIF isdefined("frmpersonid")>
								<CF_INPUT type="hidden" name="frmpersonid" value="#frmpersonid#">
							</CFIF>
						</FORM>
					</CFOUTPUT>
				</div>
			</cfif>

		<SCRIPT type="text/javascript">
			function newResultsPage(nStRow,nPage){
				nform=document.newPage;
				nform.StartRow.value = nStRow;
				nform.thisPage.value = nPage;
				nform.submit();
			}
		</script>
		</CFIF>
	</cfif>
</div>