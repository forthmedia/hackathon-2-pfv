<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			showAdvert.cfm	
Author:				GCC
Date started:		2005/08/17
	
Purpose:	To display an advert in an element drawn from a selection of adverts stroed in a tree branch.
Each revolve count is independent for each elementBranchRootID.
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
21-JAN-2009			NJH			Bug Fix All Sites Issue 1396 - check for an elementTextID as well, and get the elementBranchRootID based on the elementTextID if it exists.


Possible enhancements:


 --->

<cf_param name="elementBranchRootID" default="" required="true"/>
<cf_param name="advertMethod" validvalues="First,Revolve" required="true"/> 

<!--- NJH 2001/01/21 Bug Fix All Sites Issue 1396 - check for an elementTextID as well--->
 <!--- Usage START --->
	<cfif not isdefined('elementBranchRootID') and not isDefined("elementBranchRootTextID")>
		You must specify an elementBranchRootID or an elementBranchRootTextID. This is the parentElementID of the advert elements.
		<CF_ABORT>
	</cfif>
	
	<cfif not isdefined('request.currentElementTree')>
		This tag needs request.currentElementTree to exist in order to function.
		<CF_ABORT>
	</cfif>

	<cfif not isdefined('advertMethod')>
		This tag needs an advertMethod in order to function.<br>
		Options are:<br>
		first - always displays the first advert it finds for the user<br>
		revolve - rotates through the adverts starting with the first
		<CF_ABORT>
	</cfif>
<!--- Usage END --->

<!--- NJH 2001/01/23 if the elementBranchRootID is not numeric, assume it is a elementBranchRootTextID --->
<cfif isDefined("elementBranchRootID") and not isNumeric(elementBranchRootID)>
	<cfset elementBranchRootTextID = elementBranchRootID>
</cfif>

<!--- NJH 2001/01/21 Bug Fix All Sites Issue 1396 start --->
<!--- get the elementID from a elementTextID. elementBranchRootTextID takes precedence over elementBranchRootID --->
<cfif isDefined("elementBranchRootTextID")>
	<cfquery name="getElementIDFromTextID" dbtype="query">
		select ID from request.currentElementTree where elementTextID = '#elementBranchRootTextID#'
	</cfquery>

	<cfif getElementIDFromTextID.recordCount gt 0>
		<cfset elementBranchRootID = getElementIDFromTextID.ID>
	</cfif>
</cfif>

<!--- ensure that elementBranchRootID is indeed defined. It wouldn't be defined if there was a problem getting it from the elementTextId, although getElementIDFromTextID
	this should never really happen ideally. --->
<cfif not isDefined("elementBranchRootID")>
	You must specify an elementBranchRootID. This is the parentElementID of the advert elements.
	<CF_ABORT>
</cfif>
<!--- NJH 2001/01/21 Bug Fix All Sites Issue 1396 end --->

<cfscript>
	qGetElements = application.com.relayElementTree.getBranchFromTree (elementtree = request.currentElementTree, topofbranchID = elementBranchRootID, returnTopOfBranch = false);
</cfscript>

<cf_param name="startPosition" default="1"/>

<!--- --->
<cf_param name="bannerElementIDList" default=""/>

<cfif listlen(bannerElementIDList) neq 0>

	<cfquery name="qGetElements" dbtype="query">
		select * from qGetElements
		where node in (#bannerElementIDList#)
	</cfquery>

</cfif>

<cfif qGetElements.recordcount gt 0>	
	<!--- revolve - rotates through the adverts starting with the first--->
	<cfif advertMethod eq "revolve">
		<cfset sessionString = "session.revolver" & elementBranchRootID>
		<cfif isdefined('#evaluate(DE("session.revolver#elementBranchRootID#"))#')>
			<cfset sessionVariable = #evaluate(sessionString)#>
		<cfelse>
			<cfif startPosition eq "any" and qGetElements.recordcount gt 1>
				<cfset sessionVariable = randrange(1,qGetElements.recordcount)>
			<cfelse>
				<cfset sessionVariable = startPosition>
			</cfif>			
		</cfif>
		<cflock timeout="1">
			<cfif sessionVariable gte qGetElements.recordcount>
				<cfset "session.revolver#elementBranchRootID#" = 1>
			<cfelse>
				<cfset "session.revolver#elementBranchRootID#" = sessionVariable + 1>
			</cfif>
			<cfset rowNumber = #evaluate(sessionString)#>
		</cflock>
	<!--- first/default - show the first advert visible by this user --->
	<cfelse>
		<cfset rowNumber = 1>
	</cfif>
	<cfloop query="qGetElements" startrow="#rowNumber#" endrow="#rowNumber#">
		<cfset imageref = "phr_detail_element_" & qGetElements.node>
		<cfif left(url,7) eq "http://" or left(url,8) eq "https://">
			<cfset myTarget = "_blank">
		<cfelse>
			<cfset myTarget = "_self">
		</cfif>
		<cf_translate>
			<cfoutput>
				<cfif isExternalFile><a href="#url#" target="#myTarget#">#htmleditformat(imageref)#</a><cfelse>#htmleditformat(imageref)#</cfif>
			</cfoutput>
		</cf_translate>
		<!--- shown one advert. The shows over kids, go home --->
		<cfbreak>		
	</cfloop>
</cfif>
