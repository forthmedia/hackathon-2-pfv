<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		activityStream.cfm	
Author:			NJH  
Date started:	21-12-2012
	
Description:	A users social activity stream		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_includeJavascriptOnce template="/javascript/social.js" translate="true">
<cf_head>
	<style type="text/css" media="screen,print">@import url("/code/styles/social.css");</style>
</cf_head>

<cfoutput>
<div id="followDivContainer">
	<div id="followingHeading">
		<h1>phr_social_Following</h1>
	</div>
	<cfset approved = application.com.flag.getFlagData(entityID=request.relayCurrentUser.personID,flagID='perApproved')>
	<div id="userHeadingDiv">
		#request.relayCurrentUser.picture#
		<div id="name">#htmlEditFormat(request.relayCurrentUser.person.publicProfileName)#</div>
		<div id="about">#htmlEditFormat(request.relayCurrentUser.person.publicProfileAbout)#</div>
		<div id="location">#htmlEditFormat(request.relayCurrentUser.person.publicProfileLocation)#</div>
		<div id="userApproved">Joined #lsDateFormat(approved.lastUpdated)#</div>
	</div>
	<div id="followingSubHeading">
		<h1>phr_social_whoIAmFollowing</h1>
	</div>
	<div id="followDiv">
		<table style="width:100%;">
			<cfset socialFollow = application.com.social.getSocialFollow()>
			<cfloop query="socialFollow">
				<tr class="followUserRow">
					<td class="followUserPicture">
						<img src="#pictureURL#"/>
					</td>
					<td class="followUserContent">
						<div class="followingUsername">#htmlEditFormat(publicProfileName)#</div>
						<div class="followingAbout">#htmlEditFormat(publicProfileAbout)#</div>
						<div class="followingLocation">#htmlEditFormat(publicProfileLocation)#</div>
					</td>
					<td class="followUserLink">
						<cfset action="stop">
						<cfset encryptedEntityID = application.com.security.encryptVariableValue(name="entityID",value=entityID)>
						<a class="#jsStringFormat(action)#Follow_#jsStringFormat(encryptedEntityID)#" id="#htmlEditFormat(action)#" href="" onClick="follow(#jsStringFormat(entityTypeID)#,'#jsStringFormat(encryptedEntityID)#','#jsStringFormat(action)#');return false;">phr_social_#htmlEditFormat(action)#Following</a>
					</td>
				</tr>
			</cfloop>
		</table>
	</div>
</div>
</cfoutput>