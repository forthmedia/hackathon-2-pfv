<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			orderItems.cfm	
Author:				SWJ
Date started:		2004-07-16
	
Description:		This has been introduced so that order items can be included 
					as a Relay Tag
					
					To specify which promoid to use set promoid as a parameter

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-04-13			AJC			Add ability to show all products in a promotion bye category
2011/08/17			PPB			REL109 added div to allow the styling to be customised 

Possible enhancements:


 --->
<cfparam name="THISPRODUCTGROUP" default="">
<cfset hiderightBorder = "yes">
<cfset hideleftBorder = "yes">
<cf_param name="spendIncentivePointsType" default="company" validValues="company,personal"/>
<!---
<cfif not isNumeric(thisProductGroup) and thisProductGroup is not "">
	<p>The Relay Tag ordersSelectOrderItems requires a numeric value for thisProductGroup</p>
<cfelse>--->
	<cf_include template="\code\cftemplates\incentiveINI.cfm" checkIfExists="true">
	<!--- <cfif fileexists("#application.paths.code#\cftemplates\incentiveINI.cfm")>
		<!--- incentiveINI can be used to over-ride default 
			global application variables and params defined in incentiveParams.cfm --->
		<cfinclude template="/code/cftemplates/incentiveINI.cfm">
	</cfif> --->

	<!--- 
	WAB 2010/02/16 RACE removed to make way for new security system

	<cfinclude template="/orders/application .cfm">
	<cfinclude template="/orders/orderItemsInclude.cfm">
	--->

	<div id="marketingStore_screen">
	<cf_IncludeWithCheckPermissionAndIncludes template="/orders/orderItemsInclude.cfm">	<!--- PPB 2010-02-26 corrected syntax error FOR LIGHTHOUSE 3089 ---> 
	</div>

<!---</CFIF>--->
