<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		ElementSiteMap Tag
		
		Created 12 October 2001

		This is a modification of the templates
		
		ElementDrillDown.cfm and Sitemap.cfm

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-Nov-2001			CPS			Amend call to getElementsBranch to include session variable FNLElementStatus
01-Nov-2006             NJH              Excluded elements that the user des not have visibility rights to
--->

<!--- a request scope variable TopElementIDPartnerContent can be defined in RelayINI 
		to override the one in the application scope  --->
<cfif isDefined("request.TopElementIDPartnerContent")>
	<cfset variables.treeTopElementID = request.TopElementIDPartnerContent>
<cfelse>
	<cfset variables.treeTopElementID = application.TopElementIDPartnerContent>
</cfif>
 
<cf_param NAME="mapElementID" DEFAULT="treeTopElementID"/>
<CFPARAM NAME="frmLevelTwoID" DEFAULT=""> <!--- NYB 2011-12-02 didn't cf_param because it doesn't seem to be getting used anywhere --->
<cf_param NAME="frmShowCountryID" DEFAULT="0"/>
<CFPARAM NAME="DefaultTemplate" DEFAULT="et.cfm">

<cfif isDefined("ContentWidth")><cfset siteMapTableWidth=ContentWidth-20><cfelse><cfset siteMapTableWidth="95%"></cfif>
<cf_param NAME="Layout" DEFAULT="list" validvalues="list,tree,table"/>
<cf_param NAME="NODE_IMAGE" DEFAULT="ftp/borders/fnlimages/tree.jpg"/>
<cf_param NAME="USE_HREF" DEFAULT="yes" type="boolean"/>

<!--- NJH 2006/09/15 --->
<cf_param name="showOddEvenRow" type="boolean" default="yes"/>
<cf_param name="useIcons" type="boolean" default="no"/>
<cf_param name="generationIcon" type="string" default="/images/misc/arrow-open.gif"/>

<cfif mapElementID eq "treeTopElementID">
	<cfset mapElementID = "#variables.treeTopElementID#">
</cfif>

<!--- <CFSTOREDPROC PROCEDURE="GetElementBranch" datasource="#application.sitedatasource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#mapElementID#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE=#request.relayCurrentUser.personid# NULL="No">
		<CFPROCRESULT NAME="getElements" RESULTSET="1">
	<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
 	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@Depth" VALUE="0" NULL="No">
	<CFIF IsDefined('session.FNLElementStatus')>
	 	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@Status" VALUE="#session.FNLElementStatus#" NULL="No">
	</CFIF>	
</CFSTOREDPROC> --->
		
<cfset getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID= variables.treeTopElementID)> 

<!--- NJH 2006/10/26 restrict elements shown to be those that are visible --->
<!--- NJH 2008/05/30 All Sites Issue 40 removed the top node (where the parent is also the node) of the element tree in the sitemap --->
<cfquery name="getVisibleElements" dbtype="query">
	select * from getElements where showOnMenu = 1 and parent <> node
</cfquery>

<cfset getElements = getVisibleElements>
	
<!--- getCountries --->
<CFQUERY NAME="getCountries" datasource="#application.sitedatasource#">		
	Select convert(varchar(3), countryID) + '#application.delim1#' + CountryDescription as country
	from Country
	where isocode <> ''
	order by countrydescription
</CFQUERY>

<CFQUERY NAME="GetTopElementDetails" datasource="#application.sitedatasource#">
	select headline, elementtypeID from element where id =  <cf_queryparam value="#elementid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFSWITCH EXPRESSION=#layout#>
	<CFCASE VALUE="List">
		<cfoutput><ul></cfoutput>
			<CFOUTPUT Query="getElements">
			
				<li
					<cfif showOddEvenRow>
						<CFIF CurrentRow MOD 2 IS NOT 0> class="oddRow"<CFELSE> class="evenRow"</CFIF>
					</cfif>
					>
					<span>
						<CFSET IC=3>
							<CFLOOP CONDITION="IC LTE #generation#">&nbsp;&nbsp;
						<CFSET IC=IC+1>
					</CFLOOP>
					
					<!--- NJH 2006/09/15 --->
					<cfif useIcons>
						<cfif isDefined("generation#generation#Icon")>
							<cfset imageSrc = evaluate("generation#generation#Icon")>
							<cfif imageSrc neq "">
								<img src="#imageSrc#" border=0/>
							</cfif>
						<cfelse>
							<img src="#generationIcon#" border=0/>
						</cfif>
					</cfif>
					<A HREF="/#trim(DefaultTemplate)#?eid=#node#">#htmleditformat(Headline)#</A></span>
				</li>
			</CFOUTPUT>
		</ul>
	</CFCASE>	

	<CFCASE VALUE="tree">
	
		<CFQUERY NAME="Generation1" DBTYPE="query">
			SELECT * FROM getElements WHERE Generation=1
		</CFQUERY>
		
		<cfform  method="POST" name="thisForm">
			<CFTREE 
				name="Tree" 
				height="300" 
				width="360" 
				FONT="verdana,geneva,arial,helvetica,sans-serif"
    			FONTSIZE="10"
				NOTSUPPORTED="<B>Your browser needs to support Java to view the Site Map</B>"
			>
				<CFLOOP query="Generation1">
					<!---
						DAM 2001-10-22
						Note that in teh Hrefs the javascript function call is terminated with ; and then the 
						characters // are inserted to ensure that anything added to the uerl query string by
						the CFTREE tag is seen as a javascript comment.
					--->
					<CFIF USE_HREF>
						<CFTREEITEM value="#node#" parent="0" display="#headline#" img="#NODE_IMAGE#" expand="yes" HREF="javascript:formSubmit(#node#);//">
					<CFELSE>
						<CFTREEITEM value="#node#" parent="0" display="#headline#" img="#NODE_IMAGE#" expand="yes">
					</CFIF>
					<CFSET ParentID_1=#node#>
					<CFQUERY NAME="Generation2" DBTYPE="query">
						SELECT * FROM getElements WHERE parent=#ParentID_1# AND ShowOnMenu = 1
					</CFQUERY>	
					<CFLOOP query="Generation2">
						<CFIF USE_HREF>
							<CFTREEITEM value="#node#" parent="#ParentID_1#" display="#headline#" img="#NODE_IMAGE#" expand="no" HREF="javascript:formSubmit(#node#);//">
						<CFELSE>
							<CFTREEITEM value="#node#" parent="#ParentID_1#" display="#headline#" img="#NODE_IMAGE#" expand="no">
						</CFIF>
						<CFSET ParentID_2=#node#>
						<CFQUERY NAME="Generation3" DBTYPE="query">
							SELECT * FROM getElements WHERE parent=#ParentID_2# AND ShowOnMenu = 1
						</CFQUERY>
						<CFLOOP query="Generation3">
							<CFIF USE_HREF>
								<CFTREEITEM value="#node#" parent="#ParentID_2#" display="#headline#" img="#NODE_IMAGE#" expand="no" HREF="javascript:formSubmit(#node#);//">
							<CFELSE>
								<CFTREEITEM value="#node#" parent="#ParentID_2#" display="#headline#" img="#NODE_IMAGE#" expand="no">
							</CFIF>
							<CFSET ParentID_3=#node#>
							<CFQUERY NAME="Generation4" DBTYPE="query">
								SELECT * FROM getElements WHERE parent=#ParentID_3# AND ShowOnMenu = 1
							</CFQUERY>	
							<CFLOOP query="Generation4">
								<CFIF USE_HREF>
									<CFTREEITEM value="#node#" parent="#ParentID_3#" display="#headline#" img="#NODE_IMAGE#" expand="no" HREF="javascript:formSubmit(#node#);//">
								<CFELSE>
									<CFTREEITEM value="#node#" parent="#ParentID_3#" display="#headline#" img="#NODE_IMAGE#" expand="no">
								</CFIF>
							</CFLOOP>
						</CFLOOP>
					</CFLOOP>
				</CFLOOP>
			</CFTREE>
			<CFIF NOT USE_HREF>
				<INPUT type="submit" value="              Click to view selected item              ">
			</CFIF>
		</CFFORM>
	</CFCASE>

	<CFDEFAULTCASE>
		<ul>
		<CFSET counter = 0>
		<CFLOOP QUERY="getElements">
		   <CFSET counter = counter + 1>
		   <CFIF counter EQ 1>
		      <!--- first cell in row --->
		      <li>
		   </CFIF>
		   <span>
		   <CFOUTPUT><A HREF="/#DefaultTemplate#?ElementID=#node#">#htmleditformat(Headline)#</A></cfoutput>
		   </span>
		   <CFIF counter EQ 3>
		      <!--- last cell in row --->
		      </li>
		      <CFSET counter = 0>
		   </CFIF>
		</CFLOOP>
		<CFIF counter NEQ 0>
		   <!--- in case the query's record count isn't a multiple of three --->
		   </li>
		</CFIF>
		</ul>
	</CFDEFAULTCASE>
</CFSWITCH>







