<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eLearningStudentProgress.cfm	
Author:				VAB/SWJ
Date started:		3-Jul-08
	
Description:		This screen provides a student with the information on a given course they are
					"signed-up" to.

Amendment History:

Date (YYYY-MMM-DD)	Initials 	What was changed

2009-Feb-25			SSS			Reworked to show module progress as personCertifications.cfm does certification progress. Currently includes quiz data where applicable but may need further work.
2010-Feb-15			NAS			SNY090 Updates
2010-05-13			AJC			P-SNY095 - Top 10
2010/10/14			NAS			LID4338: eLearning - Student Progress Relaytag includes FNL in the title
2010/11/10			PPB			LID4534: translate column headings
2011/10/27			RMB			LID7261: sorting coloums fixed
2012/02/15			RMB			CASE: 426595 - Added Show_quiz to quary in "GetStudentProgress" in "relayElearning.CFC" and removed in-line quary "qStudentProgressData"
2013-03-25 			PPB 		Case 431081 sending in SortOrder from relaytag
2013-05-01			STCR		CASE 434886 / 434972 - cf_param without self-closing slash was causing XML corruption in RelayTagEditor
2013-07-25			PPB			Case 436072 facility to reset a user's module progress and allow them to retake a quiz 
2014-07-01 			NYB 		Case 440612 changed defaultsortorder from Title_Of_Module to Title_Of_Course then Title_Of_Module

Possible enhancements:
--->

<cfif fileexists("#application.paths.code#\cftemplates\ElearningStudentProgress.cfm")>
 <cfinclude template="/code/cftemplates/ElearningStudentProgress.cfm">
<cfelse>

<!--- START 2013-03-25 PPB Case 431081 now send in defaultSortOrder so that it doesn't take precedence over the TFQO sortorder mechanism once the list has been displayed once --->	
<cf_param name="defaultSortOrder" label="Sort Order" default="Title_Of_Course,Title_Of_Module,Module_Pass_Date"/> <!--- 2014-07-01 NYB Case 440612 changed from Title_Of_Module --->
<cfparam name="sortOrder" default="#defaultSortOrder#">
<cfparam name="allowUpdate" default="false">

<cfscript>
	if(isdefined('frmCurrentEntityid')) {
		studentPersonID = frmCurrentEntityid;
	} else {
		studentPersonID = request.relaycurrentuser.personID;
	}
	qStudentProgressData = application.com.relayElearning.GetStudentProgress(personID=studentPersonID,courseID=0,sortorder=sortorder);
</cfscript>

<cf_translate>
<cf_param name="showTheseColumns" default="Title_Of_Course,Title_Of_Module,Module_Type,Module_Status,Module_Pass_Date,Module_Code,Show_Quiz" validValues="Title_Of_Course,Title_Of_Module,Module_Type,Module_Status,Module_Pass_Date,Module_Code,Show_Quiz" multiple="true" displayAs="twoSelects" allowSort="true"/>
<cf_param name="GroupBy" default="" validValues="Title_Of_Course,Title_Of_Module,Module_Type,Module_Status,Module_Pass_Date,Module_Code,Show_Quiz,Reset_Quiz"/>		<!--- 2013-07-25 PPB Case 436072 add Reset_Quiz --->
<cf_param name="translateColumns" type="boolean" default="true"/>		<!--- 2010/11/10 PPB LID4534 set to true to translate headings --->
<!--- <cfparam name="frmModuleID" default="0"/> NJH this doesn't appear to be used --->
<cf_param name="showNoRecordsFound" label="Show Number of Records Found" default="No" type="boolean"/>
<cf_param name="incTitle" label="Show Page Heading" default="Yes" type="boolean"/>
<cfparam name="keyColumnList" default="Show_quiz"/>
<!--- START 2013-07-25 PPB Case 436072 --->
<cfparam name="keyColumnURLList" default=" "/>
<cfparam name="keyColumnKeyList" default="moduleID"/>
<cfparam name="keyColumnOnClickList" default="javascript:showQuiz(this*comma##userModuleProgressID##);return false;"/>
<cfparam name="keyColumnOpenInWindowList" default="no"/>

<cfif allowUpdate>
	<cfset showTheseColumns = showTheseColumns & ",Reset_Quiz">
	<cfset keyColumnList = keyColumnList & ",Reset_Quiz">
	<cfset keyColumnURLList = keyColumnURLList & ", ">
	<cfset keyColumnKeyList = keyColumnKeyList & ",moduleID">
	<cfset keyColumnOnClickList = keyColumnOnClickList & ",javascript:resetQuiz('##application.com.security.encryptVariableValue(name='userModuleProgressId'*commavalue=userModuleProgressId)##'*comma'##application.com.security.encryptVariableValue(name='lastQuizTakenID'*commavalue=lastQuizTakenID)##');return false">
	<cfset keyColumnOpenInWindowList = keyColumnOpenInWindowList & ",no">
</cfif>
<!--- END 2013-07-25 PPB Case 436072 --->

</cf_translate>

<!--- 2010-05-13 AJC P-SNY095 - Top 10 --->
<cf_include template="\code\cftemplates\elearningINI.cfm" checkIfExists="true">

<!--- NYB 2011-12-02 didn't cf_param because these are only being used in a webservice in SonyUserFiles: --->
<cfparam name="showTop10" default="false" type="boolean">
<cfparam name="top10AvailableCountries" default="">
<cfparam name="availableUserGroups" default="">
<!--- 2010-05-13 AJC P-SNY095 - Top 10 --->		

<cfif qStudentProgressData.recordCount gt 0>

	<!--- Added Show_quiz to quary in "GetStudentProgress" in "relayElearning.CFC" --->
	<!--- <cfquery name="qStudentProgressData" dbType="query">
		select *, '<img src="/images/icons/bullet_arrow_down.png">' as Show_quiz from qStudentProgressData 
	</cfquery> --->
	
	
	<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
	<!--- START: 2010-05-13 AJC P-SNY095 - Top 10 --->
	<cfif request.relayCurrentUser.isInternal>
		<cf_includejavascriptonce template="/javascript/extExtension.js" >
		
		<cf_includeJavascriptOnce template="/elearning/js/elearning.js">
	</cfif>
	<!--- END: 2010-05-13 AJC P-SNY095 - Top 10 --->
	<cfset columnToShowAjaxAt = listFindNoCase(qStudentProgressData.columnList,"userModuleProgressID")>
	
	<script>
		
		<cfoutput>var columnToShowAjaxAt = #jsStringFormat(columnToShowAjaxAt)#</cfoutput>
		function $alternative(element) { 
			if (arguments.length > 1) {
				for (var i = 0, elements = [], length = arguments.length; i < length; i++)
					elements.push($alternative(arguments[i]));
					return elements;
				}
				if (Object.isString(element))
					element = document.getElementById(element);
					return Element.extend(element);
			}

				function showQuiz (obj,id) {

					currentRowObj = getParentOfParticularType (obj,'TR')
					currentTableObj = getParentOfParticularType (obj,'TABLE')

					totalColspan = 0
					for (i=0;i<currentRowObj.cells.length;i++) {
						totalColspan  += currentRowObj.cells[i].colSpan
					}
					
					infoRow =  id + '_info'
					infoRowObj = $alternative(infoRow)
				
					if (!infoRowObj) {
						/*page = '/webservices/relayElearningWS.cfc?wsdl&method=getQuiz'*/
						/*2010-Feb-15			NAS			SNY090 Updates*/
						page = '/webservices/callWebService.cfc?wsdl&method=callWebservice&webservicename=relayElearningWS&methodname=getQuiz'
						<cfoutput>page = page + '&allowUpdate=#allowUpdate#&showTop10=#jsStringFormat(showTop10)#&top10AvailableCountries=#jsStringFormat(top10AvailableCountries)#&availableUserGroups=#jsStringFormat(availableUserGroups)#&userModuleProgressID='+id;</cfoutput>
						<cfif not request.relayCurrentUser.isInternal>
							<cfoutput>page = page + '&eid='+ #jsStringFormat(request.currentElement.id)#;</cfoutput>
						</cfif> 
			
						// changed to post to ensure proper refresh
						var myAjax = new Ajax.Request(
							page, 
							{
								method: 'post', 
								parameters: '', 
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									rowStructure = new Array (2)
									rowStructure.row = new Array (1) 
									rowStructure.cells = new Array (1) 
									rowStructure.cells[0] = new Array (1) 
									rowStructure.cells[0].content  = json.CONTENT 
									rowStructure.cells[0].colspan  = totalColspan //-rowStructure.cells[0].colspan
									rowStructure.cells[0].align  = 'right'
			
									regExp = new RegExp ('_info')
									deleteRowsByRegularExpression (currentTableObj,regExp)
									
									addRowToTable (currentTableObj,rowStructure,obj.parentNode.parentNode.rowIndex + 1,infoRow)
								
									// 2014-06-17	YMA	Re-initialize responsive table on AJAX content
									jQuery(currentTableObj).trigger('create');
									}
							});
					} 
					else
					{
						regExp = new RegExp (infoRow)
						deleteRowsByRegularExpression (currentTableObj,regExp)
					}
					
					return 
				}
				
				function getParentOfParticularType (obj,tagName) {	
					if (obj.parentNode.tagName == tagName) 	{
						return obj.parentNode
					} else {
						return getParentOfParticularType (obj.parentNode,tagName)
					}
				}
</script>
<!--- Start 2010-Feb-15			NAS			SNY090 Updates --->
<!--- 2010/10/14			NAS			LID4338: eLearning - Student Progress Relaytag includes FNL in the title --->
	<cfif not request.relayCurrentUser.isInternal and incTitle>
		<cfoutput>
		<h2>phr_Relay_Elearning_Student_Progress_Title</h2>
		</cfoutput>
	</cfif>
		<!--- Stop 2010-Feb-15			NAS			SNY090 Updates --->

	<cfif showNoRecordsFound>
		<cfset HidePageControls = "false">
	<cfelse>
		<cfset HidePageControls = "true">
	</cfif>
	
	<CF_tableFromQueryObject 
		queryObject="#qStudentProgressData#"
		queryName="qStudentProgressData"
		groupByColumns="#groupBy#"
	 	hideTheseColumns="#groupBy#"
		showTheseColumns="#showTheseColumns#"
		columnTranslation="#translateColumns#"
		ColumnTranslationPrefix="phr_elearning_"
		dateformat="MODULE_PASS_DATE"
		<!--- numberFormat="Credits_Obtained,Num_Modules_To_Pass,Modules_Passed" --->
		useInclude="false"
		keyColumnList="#keyColumnList#"
		<!--- START 2013-07-25 PPB Case 436072 --->
		keyColumnURLList="#keyColumnURLList#"	
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOnClickList="#keyColumnOnClickList#" 
		keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"
		<!--- END 2013-07-25 PPB Case 436072 --->
		rowIdentityColumnName="userModuleProgressID"
		<!---HidePageControls="yes" --->
		sortOrder="#sortOrder#"
		HidePageControls="#HidePageControls#"
		numRowsPerPage="1000"
	>
	


</cfif>
</cfif>
