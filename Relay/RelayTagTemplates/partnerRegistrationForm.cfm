<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			partnerRegistrationForm.cfm
Author:				RPW
Date started:		2014-09-18

Purpose:	To provide a form for adding all of the contact details in one hit.
			This is a copy of
			Y:\clientCodeFiles\RW2014Roadmap2_PartnerCloud\Code\CFTemplates\Modules\Registration\RelayTagTemplates\addContactForm.cfm
			for relay tag RELAY_PARTNERREGISTRATIONFORM

Usage:

introPhraseTextID - this should be set to the phraseTextID to describe some introductory text for this form
formCompletePhraseTextID - this should be set to the phraseTextID to describe the completion text for this form

showCols (optional) This allows you to specify which of the default values listed you
	want to show. 	The order they appear on the screen is controlled by the list.
	The default values are: insSalutation, insFirstName, insLastName, insJobDesc, insEmail,
	insOfficePhone, insMobilePhone, insSiteName, insAddress1, insAddress2, insAddress3,
	insAddress4, insAddress5, insPostalCode, insCountryID
mandatoryCols (optional) This determines which columns are mandatory. The default
	list is as follows: insSalutation, insFirstName, insLastName, insEmail, insSiteName,
	insAddress1, insPostalCode.
frmNext (optional) This specify which template is processed after the form data.
frmDebug (optional) Yes or no - Defaults to No. If set to yes the frmNext form will
	not be processed. Instead a message telling you what was added and updated is shown.
frmReturnMessage (optional) Yes or no. Default is no. Returns a variable containing
	success/failure message.
urlRoot (optional) Defaults to application. urlRoot. This allows you to specify the
	root for this screen allowing you to call it from outside the URL Root path.
flagStructure (optional) Defaults to none. This allows you to specify the profiles
	that will be set and the values they can be set to.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
04/05/2006			AJC			Added partnerRegistrationFormType to allow for short and long forms
04/05/2006			AJC			Added check to see if user is already registered
08/05/2006
18/07/2006			WAB			made a change so that if usecurrentuser is set and is true, then the contact form is skipped
15/01/2007			SSS			added datacaptureMethod to be able to switch from 2step method to incrmental method
15/01/2007			SSS			Added ShowLocationCols,showPersonCols to switch fields on and of for the incrmental method
22/03/2007			SSS			will only show live langauges for people to select from.
10/04/2008			SSS			If the switch skipcompanycheck is passed as true the company check will be by-passed and location selection page will appear first
14/11/2008			AJC	 		Hide Magic Number
30/06/2009			NJH			P-FNL069 - Removed frmGlobalParameters in an effort to secure code
30/09/2009			SSS			To show captcha if it is selected as on
28/04/2010			NJH			P-PAN002 Set the oppListingsEid as a param . This will show once the opportunity has been saved, rather than back to the partnerRegistrationForm.
2010/06/30  		PPB 		P-PAN002 New Opp screen - we want to limit the country dropdown to list just the countries relevant to the reseller
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2016/02/10			NJH			JIRA Prod2015-343 - Added domain matching parameters.
Possible enhancements:


--->

<!--- 04/05/2006 AJC P_Cor001 --->
<cf_param name="dataCaptureMethod" default="2step" label="Data Capture Method" validvalues="2step,Incremental" reloadOnChange="true"/>

<cf_param name="showCols" default="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID,insLocEmail" type="string" label="Show Columns" validValues="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID,insLocEmail" multiple="true" size="5" condition="dataCaptureMethod eq '2step'"/> <!--- 2012-10-29 MS	P-KAS003 Added insLocEmail to the list so that we can store location Email address in the locEmail Column of the location table --->
<!--- params use by dataCaptureMethod=incremental --->
<cf_param name="ShowLocationCols" default = "insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax" label="Show Location Columns" validValues="insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax" multiple="true" size="5" condition="dataCaptureMethod eq 'incremental'"/>
<cf_param name="showPersonCols" default = "insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone" label="Show Person Columns" validValues="insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone" multiple="true" size="5" condition="dataCaptureMethod eq 'incremental'"/>
<cf_param name="showAdditionalCols" default="" label="Show Additional Columns"/>
<cf_param name="mandatoryColsOverride" default="" label="Mandatory Columns Override" description="List of columns that will be mandatory" validValues="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" multiple="true" size="5"/>

<cf_param label="Show Captcha" name = "showCaptcha" type="boolean" default="No"/> <!--- SSS 30/09/2009 to show captcha--->

<cf_param label="Show Introduction Text" name="showIntroText" default="Yes" type="boolean"/>
<cf_param label="Introduction PhraseTextID"  name="introPhraseTextID" type="string" default=""/>
<cf_param label="Form Completed PhraseTextID" name="formCompletePhraseTextID" type="string" default=""/>

<cf_param name="organisationTypeID" type="numeric" default="1" label="Organisation Type" validValues="select organisationTypeID as value, typeTextID as display from organisationType order by typeTextID"/>
<cf_param name="liveLanguage" default="No" label="Show Live Languages" type="boolean"/> <!--- ss will only show live langauges for people to pick from. --->
<cf_param name="CountryGroup" default="No" label="Show Country Groups" type="boolean"/>
<cf_param label="Default Country in Country Dropdown" name="defaultCountryID" default="0" type="numeric"/>
<cf_param label="Restrict To User Organisation Countries"  name="restrictToUserOrgCountries" default="No" type="boolean"/> <!--- PPB 2010/06/30 --->

<cf_paramgroup name="Domain Matching">
<cf_param label="Domain Match" name="domainMatch" default="true" type="boolean" description="Turn on Domain Matching" onChange="toggleDomainMatchFields();"/>
<cf_param label="Restrict Domain Match By Country" name="restrictDomainMatchByCountry" default="true" type="boolean" description="Restrict the domain match by country"/>
<cf_param label="Allow Unknown Domain" name="allowUnknownDomain" default="true" type="boolean" description="Allow users to continue registration if they are not domain matched" onChange="toggleAllowNewAddressField()"/>
<cf_param label="Allow New Address On Domain Match" name="allowNewAddress" default="true" type="boolean" description="Allow adding of new account if user's account is not listed"/>
</cf_paramgroup>

<cf_param label="Opportunity Type" name="oppTypeID" type="numeric" default="1" validValues="select oppTypeId as value, oppType as display from oppType order by oppType"/>  <!--- NJH 2006/10/06  - used to pass to opportunityEdit--->
<cf_param label="Opportunity Listings Element ID" name="oppListingsEID" type="string" default=""/> <!--- NJH 2010/04/28 P-PAN002 - used to redirect to the opportunity listing once the opportunity has been saved. --->

<cf_param label="Hide Direct Line Extension" name="hideDirectLineExtension" default="Yes" type="boolean"/><!--- pkp 22/04/2008 add switch to hide extension text box --->
<!--- START: 14/11/2008	AJC Hide Magic Number --->
<cf_param label="Show Magic Number" name="showMagicNumber" type="boolean" default="Yes"/>

<cf_param label="Show Person Data Only" name="personDataOnly" default="No" type="boolean"/>
<cf_param label="Should User Be Initialised" name="initialiseUser" default="No" type="boolean" description="Enables person to be added and immediately become the currentuser"/>

<!--- Checks if user is registered and asks to confirm add new --->
<cf_param name="checkUserRegistered" default="No" label="Check If User Is Registered" type="boolean"/>
<cf_param name="resendPasswordEID" default="resendpw" label="ResendPassword Element ID"/>

<cf_param name="organisationScreenID" default=""/>
<cf_param name="personScreenID" default=""/>

<cf_param name="skipcompanycheck" default="No" label="Skip Company Check" type="boolean"/>

<!--- 2008-04-04 Michael Roberts - added for Issue 155: Landing page for invite colleague link from email --->
<cfparam name="isMagicNumber" type="boolean" default="no">
<cfset isMagicNumber = "true">
<!--- if checkUserRegistered true is passed --->

<cfif checkUserRegistered>

	<!--- check that checkUserRegisteredFlagID is passed to the tag --->
	<cfif not isDefined("checkUserRegisteredFlagID")>
		phr_partnerRegistrationForm_checkUserRegisteredFlagID_error
		<cfexit method="EXITTEMPLATE">
	</cfif>

	<cfif not request.relayCurrentUser.isUnknownUser
		and	not structKeyExists(form,"addContactSaveButton")
		and not structKeyExists(url,"ignorecontactform")
		and not structKeyExists(url,"usecurrentuser") >   <!--- WAB I think that this may need to be (not isDefined("usecurrentuser") OR usecurrentuser is true) --->

		<cfif application.com.relayElementTree.isElementInATree(elementTree=request.currentelementtree,ElementID=resendPasswordEID) is "false">
			phr_partnerRegistrationForm_resendpw_error
			<cfexit method="EXITTEMPLATE">
		</cfif>

		<CFSET isApproved = application.com.flag.checkBooleanFLagByID(flagid='#checkUserRegisteredFlagID#', entityid = request.relayCurrentUser.personid)>

			<cfoutput>
				<cfif isApproved>
					phr_partnerRegistrationForm_AlreadyRegistered
				<cfelse>
					<p><a href="#cgi.script_name#?eid=#resendPasswordEID#">phr_partnerRegistrationForm_sendpassword</a></p>
				</cfif>
				<p><a href="#request.query_string_and_script_name#&usecurrentuser=false">phr_partnerRegistrationForm_ChoosePerson_RegisterAsSomeElse </a></p>
		</cfoutput>
		<cfexit method="EXITTAG">
	</cfif>
</cfif>
<!--- 04/05/2006 AJC P_Cor001 --->
<!--- set the default parameters which can be superceded by setting params in the element  --->
<!--- short can be suplemented with additional cols using showAdditionalCols --->

<cfif isdefined('partnerRegistrationFormType')>
	<cfswitch expression="#partnerRegistrationFormType#">
		<cfcase value="short">
			<cfset showCols="insFirstName,insLastName,insJobDesc,insEmail,insSiteName,insCountryID">
		</cfcase>
		<cfcase value="NoVATNumber">
			<cfset showCols="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID">
		</cfcase>
	</cfswitch>
</cfif>

 <!--- 2005/07/06 - GCC - showAdditionalCols SUPLEMENTS the default showCols list --->
<cfif isDefined("showAdditionalCols") and len(showAdditionalCols) gt 0>
	<cfloop index="i" list="#showAdditionalCols#" delimiters="-">
		<cfset showCols = listAppend(showcols,i)>
	</cfloop>
</cfif>

<!--- 2005/07/06 - GCC - mandatoryColsOverride REPLACES default mandatory list --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
<cfelse>
	<!--- NJH 2010/10/22 changed to use settings
	<cfset mandatoryCols = "insSalutation,insFirstName,insLastName,insEmail,insSiteName,insAddress1,insPostalCode,insOfficePhone"> --->
	<cfset mandatoryCols = application.com.settings.getSetting("plo.mandatoryCols")>
</cfif>

<cfparam name="frmNext" default="return=this" type="string">
<cfparam name="frmDebug" default="no" type="boolean">
<cfparam name="showForm" default="yes" type="boolean">
<cfparam name="frmReturnMessage" default="no" type="boolean">
<cfparam name="flagStructure" default="" type="string">
<cfparam name="fieldListQuery" default="relayFieldList" type="string">

<cf_param label="Process ID" name="ProcessID" default="0" type="numeric"/>

<!--- END: 14/11/2008	AJC Hide Magic Number --->
<cfif not isDefined("introPhraseTextID")>
	phr_partnerRegistrationForm_introPhraseTextID_error
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("formCompletePhraseTextID")>
		phr_partnerRegistrationForm_formCompletePhraseTextID_error
	<cfexit method="EXITTEMPLATE">
</cfif>


<!--- WAB added 18/7/2006 aims to bypass the contact form and continue--->
<cfif isDefined("usecurrentuser") and  usecurrentuser and not request.relaycurrentuser.isUnknownUser>
	<cfset variables.thisPersonID = request.relayCurrentUser.personid>
	<cfset variables.thisorgID = request.relayCurrentUser.organisationid>
	<cfset form.addContactSaveButton = "yes">

<cfelse>



	<cfif showForm eq "yes"><!--- this is set to no later when a screen is processed --->

		<CFIF showIntroText eq "yes">

			<!--- we don't want to reshow the introPhraseTextID after thisPersonid is set --->
			<cfif Len(introPhraseTextID)>
			<cfoutput><p>phr_#htmleditformat(introPhraseTextID)#</p></cfoutput>
			</cfif>
		</CFIF>
		<!--- 04/05/2006 AJC P_Cor001 --->
		<cfif defaultCountryID is 0>
			<cfif request.relaycurrentuser.isunknownuser is true>
				<cfset defaultCountryID=request.relaycurrentuser.possiblecountryidforunknownuser>
			<cfelse>
				<cfset defaultCountryID=request.relaycurrentuser.countryid>
			</cfif>
		</cfif>
		<!--- pkp 22/04/2008 add switch to hide extension text box --->
		<cf_addPartnerRegistrationDataForm
			showCols = "#showCols#"
			showLocationCols = "#showLocationCols#"
			showPersonCols = "#showPersonCols#"
			mandatoryCols = "#mandatoryCols#"
			defaultCountryID = "#defaultCountryID#"
			frmNext = "#frmNext#"
			frmDebug = "#frmDebug#"
			showIntroText = "no"
			frmReturnMessage = "#frmReturnMessage#"
			flagStructure = "#flagStructure#"
			fieldListQuery = "#fieldListQuery#"
			liveLanguage="#liveLanguage#"
			personDataOnly = "#personDataOnly#"
			initialiseUser= #initialiseUser#
			hideDirectLineExtension = #hideDirectLineExtension#
			showCaptcha=#showCaptcha# <!--- SSS 30/09/2009 Added this as part of showing captcha or not --->
			datacapturemethod=#datacapturemethod#
			countryGroup="#CountryGroup#"
			insOrganisationTypeID=#organisationTypeID#
			skipcompanycheck="#skipcompanycheck#"
			showMagicNumber=#showMagicNumber#
			restrictToUserOrgCountries=#restrictToUserOrgCountries#
			organisationScreenID=#organisationScreenID#
			personScreenID=#personScreenID#
			restrictDomainMatchByCountry=#restrictDomainMatchByCountry#
			allowUnknownDomain=#allowUnknownDomain#
			domainMatch=#domainMatch#
			allowNewAddress=#allowNewAddress#
			>
		</cfif>
</cfif>

<CFIF isDefined("debug") and debug eq "yes">

	<cfoutput><br>frmNext = #htmlEditFormat(frmNext)# #htmlEditFormat(left(frmNext,14))# #htmlEditFormat(mid(frmNext,15,len(frmNext)-14))#
	<br>mandatoryCols = #htmleditformat(mandatoryCols)#
	<cfif isDefined("thisPersonid") and isNumeric(thisPersonid)><br>thispersonid=#htmlEditFormat(thispersonid)#</cfif>
	<cfif isDefined("thisOrgid") and isNumeric(thisOrgid)><br>thisOrgid=#htmlEditFormat(thisOrgid)#</cfif>
	<cfif isDefined("FieldNames")>
		<CFLOOP INDEX="i" LIST="#FieldNames#">#htmlEditFormat(i)#=#htmlEditFormat(evaluate(i))#, </CFLOOP>
	</cfif>
	</cfoutput>
</CFIF>

<cfif isDefined("frmNext") and left(frmNext,14) eq "returnScreenid" and isDefined("thispersonid") and isNumeric(thispersonid)>

	<!--- if thispersonid has been set by the frmNext starts with 'returnScreenid' show a screen --->
	<CFIF not isdefined("m")>
		<CFSET message="">
	</CFIF>

	<CFSET qs=request.query_string>
	<cfif not isDefined("frmNextPage")><cfset frmNextPage = "thisPage"></cfif>
	<CFIF isDefined("frmNextPage") and frmNextPage eq "thisPage">
		<cfset frmNextPage = "/?#qs#&m=1&showform=no">
	</CFIF>

	<CFSET frmPersonID = thispersonid> <!--- set in cf_addRelayContactDataForm --->
	<CFSET frmCurrentScreenID = mid(frmNext,15,len(frmNext)-14)>
	<CFSET debug = "no">
	<CFIF isDefined("debug") and debug eq "yes">
		<cfoutput>frmCurrentScreenID = #htmlEditFormat(frmCurrentScreenID)#<br>frmPersonID = #htmlEditFormat(frmPersonID)#</cfoutput>
	</CFIF>
	<CFINCLUDE template= "/screen/showScreenNew.cfm">

<!--- ==============================================================================
    This section calls redirector
=============================================================================== --->
<cfelseif isDefined("frmNext") and left(frmNext,15) eq "returnProcessid" and isDefined("thispersonid") and isNumeric(thispersonid) and isDefined("Processid") and isNumeric(Processid)>
	<!--- if thispersonid has been set by the frmNext starts with 'returnScreenid' show a screen --->
	<CFIF not isdefined("m")>
		<CFSET message="">
	</CFIF>

	<CFSET qs=request.query_string>
	<cfif not isDefined("frmNextPage")><cfset frmNextPage = "thisPage"></cfif>
	<CFIF isDefined("frmNextPage") and frmNextPage eq "thisPage">
		<cfset frmNextPage = "/?#qs#&m=1&showform=no">
	</CFIF>

	<CFSET frmPersonID = thispersonid> <!--- set in cf_addRelayContactDataForm --->
	<CFSET debug = "no">
	<CFIF isDefined("debug") and debug eq "yes">
		<CFSET debugProc = "yes">
	</CFIF>
	<CFSET frmProcessID = processID>
	<cfset hideScreenBorder = "yes">


	<cflocation url="/proc.cfm?prid=#processID#&pid=#thispersonid#&entityID=#thisOrgID#" addToken="no">

<!--- ==============================================================================
    This section calls opportunityEdit if frmNext is set to returnLeadScreen
=============================================================================== --->
<cfelseif isDefined("frmNext") and frmNext eq "returnLeadScreen" and isDefined("thisOrgID") and isNumeric(thisOrgID)>

	<cfset showForm="no">
	<cfset showIntroText="no">
	<cfset entityID=thisOrgID>
	<cfif isDefined("thisPersonID")>
		<cfset contactPersonID = thisPersonID>
	</cfif>
	<cfset returnURL=application.com.security.encryptURL("/?eid=#oppListingsEID#&orgID=#thisOrgID#&&startingTab=newopportunities")>
	<!--- NYB LHID5122 start --->
	<cfset endCustomerFlagID=application.com.flag.doesFlagExist("EndCustomerOppContacts")>
	<cfif endCustomerFlagID gt 0>
		<cfif not request.relayCurrentUser.isInternal and isDefined("THISPERSONID")>
			<cfset endCustomerFlagID=application.com.flag.setFlagData(flagid="EndCustomerOppContacts",entityID=THISPERSONID,data=request.relayCurrentUser.personID)>
		</cfif>
	</cfif>
	<!--- NYB LHID5122 end --->
	<cfinclude template="/leadManager/opportunityEdit.cfm">

<!--- ==============================================================================
    This section calls element if frmNext is set to element and GoToEID is numeric
=============================================================================== --->
<cfelseif isDefined("frmNext") and listfirst(frmNext,":") eq "eid" and isDefined("thispersonid") and isNumeric(thispersonid)>

	<cflocation url="/?eid=#listlast(frmNext,":")#" addToken="no">
<cfelse>
	<!--- to hide the form when we return --->
	<!--- we have reached the end of the line --->
	<!--- 2008/01/14 GCC If specifically being told not to show the form and we aren't going to a screen / process / opp then show the Thank you phrase --->
	<cfif isDefined("addContactSaveButton") or (isdefined("showForm") and not showform)>
		<cfoutput><cfif not isDefined("hideFormCompletePhraseTextID")><p>#application.com.relaytranslations.translatePhrase(phrase="phr_#htmlEditFormat(formCompletePhraseTextID)#")#</p></cfif></cfoutput>
	</cfif>
</cfif>