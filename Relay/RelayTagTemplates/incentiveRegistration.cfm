<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			incentiveRegistration.cfm
Author:				SWJ
Date started:		2004-07-26

Description:		This is designed for defining a screen that can submit to itself
					and set up an new account.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-07-15 SWJ Added a block that supresses the addContact form if the user is already logged in
2005-09-19	WAB deal with registration if not an unKnown user
2007-08-29	GCC		Added the ability to pass in and set an incentive T & C s flag by flagtextID
2010/09/17	NAS		LID4097: Incentive Registration Bug
2012/04/27	IH		Case 427128 don't log out user when registers someone else for rewards
2012/05/10	WAB		Case 427128 reversed out above change because it broke the regular registratio (which was more important)

Possible enhancements:

we need to test for a screen with the value in #incentiveRegistrationScreenTextID#
we need to test for a flag set up with the value in #incentiveMember#
 --->

<cfif isDefined("URL.help") and structKeyExists(url,"help")>
	<p>Optional: If the variable "IfRegisteredGoToEID" is passed to Incentive Registration tag and
	set to an elementID then the user will be redirected to that screen.  Otherwise they will see a message.</p>
	<p>Optional: eMailTextID (which is defaulted to incentiveRegistrationScreenTextID) is the email text ID of the
	email that gets sent when the user has registered and can be changed
	to a different email Text ID as required.</p>
</cfif>

<cfif not isDefined("variables.incentiveRegistrationScreenTextID") or isDefined("URL.help") >
	<p><strong>Required:</strong> Incentive Registration tag requires the variable incentiveRegistrationScreenTextID which
	should be set to the text ID of the screen you wish to include for the user to complete after they have added their personal details.</p>
	<cfexit method="EXITTAG">
</cfif>

<cfparam name="FORM.startNew" type="boolean" default="yes">
<cfparam name="ignoreContactForm" type="boolean" default="false">
<cfparam name="emailTextID" type="string" default="incentiveRegistrationEmail">
<cfparam name="includeCompleteYourDetailsText" type="boolean" default="true">
<cfparam name="includeTermsConditionsText" type="boolean" default="true">
<cf_PARAM LABEL="Auto approve incentives registration?  Alternative goes through internal approval process." NAME="autoApprove" DEFAULT="false" TYPE="boolean"/>

<cfscript>
	qGetEmailDetails = application.com.email.getEmailDetails(emailTextID);
</cfscript>
<CFIF qGetEmailDetails.recordcount neq 1>
	<p>You must add an email to Workflow/Email with the textID of <cfoutput>#htmleditformat(emailTextID)#</cfoutput></p>
	<cfexit method="EXITTAG">
</CFIF>





<!--- <!--- 2005-07-15 SWJ: Added this check so that if the user is logged into the portal they can simply bypass the addContactForm step --->
 2005-09-19 WAB modified to a more general case for dealing with registering anyone where we have an idea who they are - whether logged in or not
<cfif request.relayCurrentUser.isLoggedin and isNumeric(request.relayCurrentUser.personid)>
	<cfscript>
		variables.ignoreContactForm = true;
		form.startNew = "no";
		variables.thisPersonID = request.relayCurrentUser.personid;
		form.addContactSaveButton = "yes";
	</cfscript>
</cfif>
 --->


<!---

WAB 2005-09-19
If the currentUser is known, (even if not logged in) then we ask the user if they want to register that person
or someone different.  Since we never log the person in (the password is sent by email) there isn't a security risk

We ask if they want to register the currentUser or someone else

 --->

<cfif
	not request.relayCurrentUser.isUnknownUser
		and
	not structKeyExists(form,"addContactSaveButton")
		and
	not structKeyExists(url,"ignorecontactform")
		and
	not structKeyExists(url,"usecurrentuser")
	>  <!--- complicated if statement deals with all other forms which are submitted to this page, only run this code on first entry--->

		<cfset checkRegistration = application.com.relayIncentive.isPersonRegistered(request.relayCurrentUser.personid)>
		<CFSET isApproved = application.com.flag.checkBooleanFLagByID(flagid='ApprovedIncentiveUser', entityid = request.relayCurrentUser.personid)>


	<cf_translate>
	<cfoutput>
			<p>phr_incentive_reg_ChoosePerson_Title</p>

				<cfif checkRegistration.recordCount eq 1 and isApproved>
					<p>phr_incentive_registration_AlreadyRegistered</p>
				<cfelse>
					<p><a class="btn btn-primary" href="#request.query_string_and_script_name#&usecurrentuser=true">phr_incentive_reg_ChoosePerson_Register #htmleditformat(request.relayCurrentUser.fullname)#</a></p>
				</cfif>

				<!---
				Case 433514 - NJH removed this link as no one uses it and it's not really possible to do, as the reg process re-initialises the user which logs them out
					so they can't complete their Ts&Cs, as the form submits to this relay tag which is on a logged in page
				<TR><Td>&nbsp;</TD><td><a href="#request.query_string_and_script_name#&usecurrentuser=false">phr_incentive_reg_ChoosePerson_RegisterAsSomeElse </a> </td></tr> --->
	</cfoutput>
	</cf_translate>
	<cfexit method="EXITTAG">
</cfif>



<!--- code for processing above clicks--->
<cfif structKeyExists(url,"usecurrentuser") and url.usecurrentuser is true and not request.relaycurrentuser.isUnknownUser and 	not structKeyExists(form,"addContactSaveButton") >
	<cfset form.startNew = "no">
	<cfset variables.thisPersonID = request.relayCurrentUser.personid>
	<cfset form.addContactSaveButton = "yes">
	<cfset variables.ignoreContactForm = true>
</cfif>




<cfif ((structKeyExists(FORM,"startNew") and FORM.startNew eq "Yes") or (structKeyExists(FORM,"addContactSaveButton")))
	and not structKeyExists(form, "agreeToTerms") and variables.ignoreContactForm eq false>

	<cfif not structKeyExists(FORM,"addContactSaveButton") and includeCompleteYourDetailsText>
		<p><cfoutput>phr_incentive_registration_CompleteYourDetails</cfoutput></p>
	</cfif>

	<cfparam name="showCols" default="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insVATNumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" type="string">
	<!--- <cfparam name="mandatoryCols" default="insSalutation,insFirstName,insLastName,insEmail,insSiteName,insAddress1,insPostalCode" type="string"> --->
	<cfparam name="mandatoryCols" default="#application.com.settings.getSetting('plo.mandatoryCols')#" type="string">
	<cfparam name="defaultCountryID" default="9" type="numeric">
	<cfparam name="frmNext" default="return=this" type="string">
	<cfparam name="frmDebug" default="no" type="boolean">
	<cfparam name="showForm" default="yes" type="boolean">
	<cfparam name="showIntroText" default="no" type="boolean">
	<cfparam name="frmReturnMessage" default="no" type="boolean">
	<cfparam name="fieldListQuery" default="relayFieldList" type="string">
	<cfparam name="ProcessID" default="0" type="numeric">
	<cfset FORM.startNew = "No">

	<cf_addRelayContactDataForm
		showCols = "#showCols#"
		mandatoryCols = "#mandatoryCols#"
		defaultCountryID = "#defaultCountryID#"
		frmNext = "#frmNext#"
		frmDebug = "#frmDebug#"
		showIntroText = "#showIntroText#"
		URLRoot = ""
		flagStructure = ""
		frmReturnMessage = "#frmReturnMessage#"
		fieldListQuery = "#fieldListQuery#"
		>
</cfif>



<cfif structKeyExists(FORM,"addContactSaveButton") and structKeyExists(variables,"thisPersonID")
	and not structKeyExists(form, "agreeToTerms")>

	<cfset FORM.startNew = "No">
	<cfscript>
		checkRegistration = application.com.relayIncentive.isPersonRegistered(thisPersonID);
	</cfscript>
	<cfif checkRegistration.recordCount eq 1>

		<cfif structKeyExists(variables,"IfRegisteredGoToEID") and 1 eq 2>
			<cflocation url="/?eid=#variables.IfRegisteredGoToEID#" addtoken="No">
		<cfelse>
			<cfoutput>
				<p>phr_incentive_registration_AlreadyRegistered</p>
			</cfoutput>
		</cfif>
		<cfexit method="EXITTAG">
	</cfif>
	<!--- 2010/09/17	NAS		LID4097: Incentive Registration Bug --->
	<cfif not structKeyExists(url,"usecurrentuser") OR (structKeyExists(url,"usecurrentuser") AND url.usecurrentuser is false)>
		<!--- CR_SNY559 - needed for relaycurrentuser initialisation --->
		<cfset x = application.com.login.initialiseAnExternalUser(thisPersonID)>
	</cfif>

	<SCRIPT type="text/javascript">

	<!--

	function verifyForm() {

		var msg = "";
		// first verify the Relay Screen
		var screenValidation = verifyInput ();
		if (screenValidation.length != 0)
			{
			msg += "phr_JS_YouMustCompleteTheFollowing :\n\n";
			msg += screenValidation;
			}
		var form = document.mainForm;

		//check that the declare checkbox has been checked
		if (!form.agreeToTerms.checked)
		{
			msg = msg + 'phr_incentive_JS_MustAgreeToTerms\n'
			alert(msg);
		}

		else
		{
			if (msg.length == 0)
			{
				//Don't seem to be any errors on the form
				form.submit();
			}
			else
			{
				alert(msg);
			}
		}
	}
	//-->

	</SCRIPT>


		<cfoutput>
		<FORM class="form-horizontal" name="mainForm" method="post" action="#cgi.script_name#?#request.query_string#">
			<cfif structKeyExists(variables,"IfRegisteredGoToEID")>
				<CF_INPUT type="hidden" name="gotoEID" value="#IfRegisteredGoToEID#">
			</cfif>
			<CF_INPUT type="hidden" name="newPersonID" value="#variables.thisPersonID#">

			<cfparam name="incentiveRegistrationScreenTextID" default="incentiveRegistrationScreen">

			<cfif includeTermsConditionsText>
				<p>phr_incentive_registration_CompleteYourDetails</p>
			</cfif>

			<cfif len(incentiveRegistrationScreenTextID) gt 0 and structKeyExists(variables, "thisPersonID")>
				<CFQUERY NAME="getUsersPersonDetails" DATASOURCE="#application.siteDataSource#">
					select p.personid, l.locationID, l.countryID, p.lastUpdated
					from person p INNER JOIN location l on p.locationID = l.locationID
					where personID =  <cf_queryparam value="#variables.thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>

				<cf_ascreen formName = "mainForm">
					<cf_ascreenitem screenid="#incentiveRegistrationScreenTextID#"
						method="edit"
						countryID = "#getUsersPersonDetails.countryID#"
						person="#getUsersPersonDetails#"
					>
				</cf_ascreen>
			</cfif>

			<cfif includeCompleteYourDetailsText>
				<p>phr_incentive_TermsConditions</p>
			</cfif>


			<div class="form-group">
				<div class="col-xs-12">
					<div class="col-xs-12 checkbox">
						<label><INPUT class="checkbox" type="checkbox" name="agreeToTerms" value="yes"> phr_incentive_agreeToTerms</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-9 col-sm-offset-3">
					<A class="btn btn-primary" HREF="javascript:verifyForm();">phr_Continue</A>
				</div>
			</div>

		</FORM>
	</cfoutput>

</cfif>

<cfif structKeyExists(form, "agreeToTerms")>
	<cf_aScreenUpdate>
	<cfparam name="goToEID" type="numeric" default="950">
	<cfparam name="incentiveMemberFlagTextID" type="string" default="incentiveMember">
	<cfparam name="incentiveTandCFlagTextID" type="string" default="IncentiveTermsAgreed">

	<cfscript>
		if(autoApprove){
			incentiveAccountID = application.com.relayIncentive.createRWAccount(0,0,FORM.newPersonID);
			incentivePersonAccountID = application.com.relayIncentive.addPersonToRWAccount(incentiveAccountID, FORM.newPersonID);
		}
		// add them to the members flag
		application.com.flag.setBooleanFlag(FORM.newPersonID,incentiveMemberFlagTextID);
		// set the flag which adds the user to the agreed T and Cs flag if it exists
		if (application.com.flag.doesFlagExist(incentiveTandCFlagTextID)) {
		application.com.flag.setBooleanFlag(FORM.NewPersonID, incentiveTandCFlagTextID);
		}
		// application.com.login.createUserNamePassword(FORM.newPersonID);
		application.com.email.sendEmail(emailTextID,FORM.newPersonID);
	</cfscript>
<!---
	WAB removed on devsite 2005-09-19 but seems to have been removed from live site earlier - emai is being sent on lines above
	<CFSET emailTextID = "incentiveRegistrationEmail">
	<CFINCLUDE template = "/genericEmails/eMailSender.cfm">
 --->
 	<cfif structKeyExists(form,"GoToEID")>
		<cflocation url="/?eid=#form.GoToEID#" addtoken="No">
	<cfelse>
		<cfoutput>
			<p>phr_incentive_registration_RegistrationComplete</p>
			<!--- 2006/01/25 - GCC - CR_SNY559 Check for Sony1 Reg here and call if necessary at step 2 --->
			<cfif fileexists("#application.paths.code#\cftemplates\AdditionalRegistrationCheck.cfm")>
				<cfinclude template="/code/cftemplates/AdditionalRegistrationCheck.cfm">
			</cfif>
		</cfoutput>
	</cfif>

</cfif>



<!--- </cf_translate> --->

