<!--- �Relayware. All Rights Reserved 2014 --->
<!--- PersonRoleProfiles 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
WAB 2011/10/10 for MDC, bug found during 8.3.1 BorderSet Upgrade see comment in code
--->




<CFPARAM NAME="nSelect" DEFAULT="p1.sex, p1.firstname, p1.lastname, f.name AS Role,f.flagtextid AS RoleImage, p1.personid AS Image, p1.FirstName + ' ' + p1.LastName AS Name, p1.OfficePhone, p1.MobilePhone, p1.Email">
<CFPARAM Name="nFieldOrder" DEFAULT="Name,OfficePhone,MobilePhone,Email">
<CFPARAM Name="nField" DEFAULT="Name,Office Phone,Mobile Phone,Email">
<CFPARAM NAME="UseMailto" DEFAULT="Email">
<CFPARAM Name="PersonalDescriptions" DEFAULT="">
<CFPARAM Name="Photos" DEFAULT="false">
<CFPARAM Name="DescriptionPlacement" DEFAULT="After">
<CFPARAM Name="Paragraphs" DEFAULT="Open,Close">
<CFPARAM NAME="PhotoBorder" DEFAULT="0">
<CFPARAM NAME="ThisOrgtype" DEFAULT="">
<CFPARAM NAME="ImageDirectory" DEFAULT="">


<CFIF IsDefined("orgtype")>
	<CFQUERY NAME="GetOrgType" DATASOURCE="#application.SiteDataSource#">
		SELECT f.name FROM
		flag f INNER JOIN booleanflagdata b ON f.flagid = b.flagid
		INNER JOIN person p ON p.organisationid = b.entityID
							AND p.personid = #request.relayCurrentUser.personid#
		INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
								AND fg.flaggrouptextid =  <cf_queryparam value="#orgtype#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
	<CFIF GetOrgType.recordcount GT 0>
		<CFSET ThisOrgtype=GetOrgType.name>
	</CFIF>
</CFIF>


<CFIF Not Isdefined("RolesFlagGroup")>
	Need to define a Profile containing Organisation Role Attributes
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="GetOrgRolesPersons" DATASOURCE="#application.SiteDataSource#">
	SELECT #preservesinglequotes(nSelect)# FROM 
		Person p
		INNER JOIN integerFlagData i ON i.entityid = p.organisationid
		INNER JOIN flag f ON f.flagid = i.flagid
		INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
		INNER JOIN person p1 ON p1.personid = i.data
		WHERE p.personid =#request.relayCurrentUser.personid# 
		and fg.flaggrouptextid =  <cf_queryparam value="#RolesFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		ORDER BY f.orderingindex
</CFQUERY>
<CFQUERY NAME="GetRoles" DATASOURCE="#application.SiteDataSource#">
	SELECT f.name,flagtextid FROM 
		flag f INNER JOIN flaggroup fg ON f.flaggroupid = fg.flaggroupid
		WHERE fg.flaggrouptextid =  <cf_queryparam value="#RolesFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<CFSET Phrases = nFieldOrder & ",">
<CFLOOP QUERY="GetOrgRolesPersons">
	<CFIF PersonalDescriptions contains image>
		<CFSET Phrases= Phrases & "PersonalRolesDescription_" & image & ",">
	<CFELSE>
		<CFSET Phrases= Phrases & "OrgRolesDescription_" & RoleImage & ",">
	</CFIF>
	<CFSET Phrases= Phrases & "OrgRoles_" & roleimage & ",">
</CFLOOP>
<CFIF len(phrases) GT 1><CFSET phrases = left(phrases, len(phrases)-1)></CFIF>
<CFSET phrases = phrases & ",OrgRoles_OpeningParagraphtext,OrgRoles_ClosingParagraph,OrgRoles_NoRoleSet">


<cf_translate >


<CFOUTPUT>
<TABLE CELLPADDING="0" CELLSPACING="0" border="0">

	
	<CFIF Paragraphs contains "open">
		<TR><TD colspan="3">phr_OrgRoles_OpeningParagraphtext</TD></TR>
	</CFIF>
	<TR><TD HEIGHT="16">&nbsp;</TD></TR>
<CFLOOP QUERY="GetOrgRolesPersons">
	<CFIF IsDefined("Sex")>
	<CFIF sex IS "F">
		<CFSET Sex1 = "she">
		<CFSET Sex2 = "She">
		<CFSET Sex3 = "her">
		<CFSET Sex4 = "Her">
	<CFELSE>
		<CFSET Sex1 = "he">
		<CFSET Sex2 = "He">
		<CFSET Sex3 = "his">
		<CFSET Sex4 = "His">
	</CFIF>
	</CFIF>
 	<TR>
		<TD colspan="3"><B>phr_OrgRoles_#htmleditformat(roleimage)#</B></TD>
	</TR>
<TR>
	<CFIF Photos IS "true">
		<CFIF fileexists("#application.paths.content#\miscimages\RolesPhoto_#image#.gif")>
			<TD valign="top"><IMG SRC="/content/miscimages/#ImageDirectory#RolesPhoto_#image#.gif"  BORDER = "#PhotoBorder#"></TD>
		<CFELSE>
			<TD valign="top"><IMG SRC="/content/miscimages/RolesPhoto_0.gif"  BORDER = "#PhotoBorder#"></TD>
		</CFIF>
	<TD>&nbsp;</TD>
	</CFIF>
	<TD VALIGN="top">
	<TABLE CELLPADDING="0" CELLSPACING="0" border="0">

	<CFIF DescriptionPlacement IS "Before">
		<CFIF PersonalDescriptions contains image>
			<CFSET desc="phr_PersonalRolesDescription_#image#">
		<CFELSE>
			<CFSET desc="phr_OrgRolesDescription_#roleimage#">
		</CFIF>
		<TR><TD Colspan=3>#htmleditformat(desc)#</TD></TR>
	</CFIF>
	<CFSET nCount = 1>
	<CFLOOP LIST="#nFieldOrder#" INDEX="x">
		<TR>
		<CFIF GetOrgRolesPersons.columnlist contains x>
		<CFSET phrasePart=gettoken(nFieldOrder,ncount,",")>
		<!--- WAB 2011/10/10 this line was doing evaluate(x), but when x was = name it was bringing back variables.name rather than GetOrgRolesPersons.name.  Not sure where variables.name being set, but got around problem by explicitly referencing the query --->
		<TD WIDTH="150px"><B>phr_#htmleditformat(phrasePart)#</B></TD><TD>&nbsp;</TD><TD><CFIF UseMailto IS x><A href="mailto:#GetOrgRolesPersons[x][currentRow]#">#GetOrgRolesPersons[x][currentRow]#</A><CFELSE>#GetOrgRolesPersons[x][currentRow]#</CFIF></TD>
		<CFELSE>
		<TD COLSPAN="3">Bad Column #htmleditformat(x)#</TD>
		</CFIF>	
		</TR>
		<CFSET nCount = nCount + 1>
	</CFLOOP>
	<CFIF DescriptionPlacement IS "After">
		<CFIF PersonalDescriptions contains image>
			<CFSET desc="phr_PersonalRolesDescription_#image#">
		<CFELSE>
			<CFSET desc="phr_OrgRolesDescription_#roleimage#">
		</CFIF>
		<TR><TD Colspan=3>#htmleditformat(desc)#</TD></TR>
	</CFIF>

	<TR><TD colspan="3" HEIGHT="16">&nbsp;</TD></TR>
	</TABLE>
	</TD></TR>
	<TR><TD HEIGHT="16">&nbsp;</TD></TR>
</CFLOOP>
	<CFSET ThisOrgRoles = valuelist(GetOrgRolesPersons.role)>
	<CFIF Paragraphs contains "close">
		<TR><TD colspan="3">phr_OrgRoles_ClosingParagraph</TD></TR>
	</CFIF>
	

</TABLE>
</CFOUTPUT>
</cf_translate >

