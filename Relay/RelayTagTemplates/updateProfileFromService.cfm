<!--- �Relayware. All Rights Reserved 2014 --->

<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia")>

	<cfset perFieldList = "Firstname,Lastname,jobdesc,officePhone,mobilePhone">
	<cfset perFlagList = "TwitterAccount,imAccount">
	
	<cfset message = "">
	<cfset errorMessage = "">
	
	<cfif structKeyExists(url,"message") and url.message eq "linkSuccessful">
		<cfset message = "Your #request.currentSite.name# account has successfully been linked with your LinkedIn account.">
	</cfif>
	
	<cfif structKeyExists(form,"frmUpdateProfile")>
		<cfloop list="#perFieldList#" index="fieldName">
			<cfset personStruct[fieldname] = form[fieldName]>
		</cfloop>
		<cfset application.com.relayEntity.updatePersonDetails(personDetails=personStruct,personID=request.relayCurrentUser.personID)>
		<cfloop list="#perFlagList#" index="fieldName">
			<cfset application.com.flag.setFlagDataForPerson(flagID=fieldname,data=form[fieldname],personID=request.relayCurrentUser.personID)>
		</cfloop>
		<cfset message = "Your profile details have been successfully updated.">
		<cfset application.com.relayCurrentUser.updatePersonalData()>
		
	<cfelseif structKeyExists(form,"frmUnlinkAccount")>
		<cfset application.com.service.unlinkEntityFromService()>
	</cfif>
	
	<cfset entityLinked = application.com.service.hasEntityBeenLinked()>
	<cfif entityLinked.linkEstablished>
		<cfset profileResult = application.com.linkedIn.getProfile()>
		<cfset linkedInProfile = profileResult.profile>
		<cfif not profileResult.isOK>
			<cfset errorMessage = "Unable to access your LinkedIn profile details. Please <a href=""javascript:void(#profileResult.authoriseLink#)"">authorise</a> #request.currentSite.name# to access your LinkedIn account.">
		</cfif>
	</cfif>
	
	<script>
		function linkToLinkedIn() {
			window.open('/social/ServiceAccess.cfm?a=authorise<cfif not entityLinked.linkEstablished>&message=linkSuccessful</cfif>','LinkedInLogin','width=400px,height=280px');
		}
		
		function copy(fieldname) {
			form = document.myProfileForm;
			form[fieldname].value = form[fieldname+'_linkedIn'].value;
		}
	</script>
	
	<cfset fieldList = listAppend(perFieldList,perFlagList)>
	
	<cfset fieldStruct = structNew()>
	<cfloop list="#fieldList#" index="fieldName">
		<cfset fieldStruct[fieldname] = structNew()>
		<cfset fieldStruct[fieldname].fieldName = fieldName>
		<cfswitch expression="#fieldname#">
			<cfcase value="jobdesc"><cfset label="Job Description"></cfcase>
			<cfcase value="officePhone"><cfset label="Telephone"></cfcase>
			<cfcase value="mobilePhone"><cfset label="Mobile"></cfcase>
			<cfcase value="imAccount"><cfset label="IM Account"></cfcase>
			<cfcase value="twitterAccount"><cfset label="Twitter"></cfcase>
			<cfdefaultcase><cfset label=fieldname></cfdefaultcase>
		</cfswitch>
		<cfset fieldStruct[fieldname].label = label>
	</cfloop>
	
	<cfset personDetails = application.com.relayEntity.getEntityStructure(entityID=request.relayCurrentUser.personID,entityTypeID=0,getEntityQueries=true).queries.person>
	
	<cfoutput>
	
	<cfform name="myProfileForm" action="/?eid=#request.currentElement.id#" method="post">
		<cf_relayFormDisplay>
			<cfif entityLinked.linkEstablished>
				<cfif message neq "">
					<tr><td colspan="4" align="center"><span class="success">#application.com.security.sanitiseHTML(message)#</span></td></tr>
				</cfif>
				<cfif errorMessage neq "">
					<tr><td colspan="4" align="center"><span class="warning">#application.com.security.sanitiseHTML(errorMessage)#</span></td></tr>
				</cfif>
				<cfif profileResult.isOK>
					<tr>
						<td colspan="4" align="center"><span style="color:##191842; font-weight:bold;">Your #request.currentSite.name# profile will be updated with your LinkedIn details.</span></td>
					</tr>
				
					<tr>
						<td></td><td><img src="/images/social/LinkedIn_IN_Icon_55px.png"/></td><td></td><td><img src="/images/social/relayware_icon.jpg"/></td>
					</tr>
				</cfif>
				<tr>
					<td></td><cfif profileResult.isOK><td><img src="#linkedInProfile.pictureURL#" style="border:5px solid ##86ACD3";"/></td><td></td></cfif><td><cfif request.relayCurrentUser.person.pictureURL neq ""><img src="#request.relayCurrentUser.person.pictureURL#" style="border:5px solid ##86ACD3;"/></cfif></td>
				</tr>
				
				<cfloop list="#fieldList#" index="fieldName">
				
					<cfif listFindNoCase(perFieldList,fieldname)>
						<cfset value = personDetails[fieldName][1]>
					<cfelse>
						<cfset value = application.com.flag.getFlagDataForCurrentUser(flagID=fieldname).data>
					</cfif>

					<tr>
						<td align="right" valign="top"><b>#fieldStruct[fieldname].label#</b></td>
						<cfif profileResult.isOK>
						<td width="100px;">#htmlEditFormat(linkedInProfile[fieldName])#<CF_INPUT type="hidden" name="#fieldName#_linkedIn" value="#linkedInProfile[fieldName]#" readonly></td>
						<td align="center"><a href="javascript:copy('#fieldName#');"><b>>>></b></a></td>
						</cfif>
						<td valign="top"><CF_INPUT type="text" name="#fieldName#" value="#value#"></td>
					</tr>
				</cfloop>
				
				<tr>
					<td colspan="3"></td><td><input type="submit" id="frmUpdateProfile" name="frmUpdateProfile" value="Update Profile"></td>
				</tr>
				<tr>
					<td colspan="3"></td><td><input type="submit" id="frmUnlinkAccount" name="frmUnlinkAccount" value="Unlink Account"></td>
				</tr>
			<cfelse>
				<tr>
					<td>Link My Account to linkedIn</td><td><input type="button" id="frmLinkAccount" name="frmLinkAccount" value="Link to LinkedIn" onClick="javascript:linkToLinkedIn()"></td>
				</tr>
			</cfif>
		</cf_relayFormDisplay>
	</cfform>
	</cfoutput>
</cfif>