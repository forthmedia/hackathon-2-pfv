<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		Element Contact Us Page 
		
		Version 1
		
		David A McLean Oct 2001


--->

<SCRIPT>

	function FormVerify(){
		var nForm=document.ContactForm;
		var msg = "";
		if (nForm.Person.value == "") {
			msg += "Please enter your name.\r\n";
		}
		if (nForm.Organisation.value == "") {
			msg += "Please enter your organisation.\r\n";
		}
		if (nForm.Email.value == "") {
			msg += "Please enter your Email.\r\n";
		}else{
			if (verifyEmailV2(nForm.Email.value) == false){
				msg += "Please enter a valid Email.\r\n";
			}
		}
		if (msg != ""){
			alert(msg);
		}else{
			nForm.submit();
		}
	}

	function verifyEmailV2(thisValue) {
		NumberFormatRe = /^[\w_\.\-']+@[\w_\.\-']+\.[\w_\.\-']+$/
		return NumberFormatRe.test(thisValue) 
	}
	
</SCRIPT>

<CFIF IsDefined("Update")>

	<CFMAIL TO="info@foundation-network.com" FROM="info@foundation-network.com" SUBJECT="FNL Web inquiry" TYPE="HTML">
	We have received an inquiry from:
	#Person# at #Organisation#
	Entitled:  #subject#
	
	<CFIF isDefined("Via")>They heard from us #via#<CFIF isDefined("othervia")>: #othervia#</CFIF></CFIF>.
	
	Their contact details are:
		Email - #email#
		Telephone - #Telephone#
	
	They made they following additional comments:
		#comments#</CFMAIL>
			
	<TABLE BORDER="0" WIDTH="100%">
	  <TR> 
	    <TD WIDTH="100%" HEIGHT="86"> 
	      	<P ALIGN="left">Thank you for your inquiry.  We will contact you shortly.</P>
	      <P ALIGN="left" CLASS="heading">
		  Relay - "Partner Relationships. Managed.&#153;"</P>
	    </TD>
	  </TR>
	</TABLE>

<CFELSE>

<form method="post" name="ContactForm" onSubmit="return checkfields()">
    <center>
      <table border=0	width=390>
        <tr> 
          <td align=LEFT valign=TOP colspan=2> 
            <p>Please enter your personal details:<br>
              <br>
            </p>
          </td>
        </tr>
        <tr> 
          <td valign=TOP width=90> 
            <p>Name:</p>
          </td>
          <td valign=TOP width=300> 
            <input type="TEXT" name="Person" maxlength=45 size=38>
            </td>
        </tr>
        <tr> 
          <td valign=TOP width=90> 
            <p>Organisation:</p>
          </td>
          <td valign=TOP width=300> 
            <input type="TEXT" name="Organisation" maxlength=45 size=38>
            <br>
            <br>
            </td>
        </tr>
        <tr> 
          <td valign=TOP width=90> 
            <p>Telephone:</p>
          </td>
          <td valign=TOP width=300> 
            <input type="TEXT" name="Telephone" maxlength=25 size=38>
            </td>
        </tr>
        <tr> 
          <td valign=TOP width=90 height="40"> 
            <p>Email:</p>
          </td>
          <td valign=TOP width=300 height="40">
            <input type="TEXT" name="Email" maxlength=45 size=38>
            <br>
            <br>
            </td>
        </tr>
        
        <tr> 
          <td ALIGN="left" valign=TOP width=90> 
            <p>Subject of your inquiry:</p>
          </td>
          <td valign=TOP width=300> 
            <input type="TEXT" name="Subject" value="" maxlength=45 size=38>
            <br>
            <br>
            </td>
        </tr>
        <tr> 
          <td align=LEFT valign=TOP colspan=2> 
            <p>Please can you give us some background on what you are looking for. <br>
              <br>
              <textarea name="Comments" rows=6 cols=50><CFIF IsDefined("CommentInsert")><CFOUTPUT>#CommentInsert#</CFOUTPUT></CFIF></textarea>
              <br>
              <br>
             </p>
          </td>
        </tr>
		<tr> 
          <td valign=TOP width=90> 
            <p>I found your web site via:</p>
          </td>
          <td valign=TOP width=300> 
            <p> 
              <input type="radio" name="Via"  value="Search Engine">
              Search Engine <br>
              <input type="radio" name="Via" value="Publicity">
              Marketing Literature<br>
              <input type="radio" name="Via"  value="Recommendation">
              Recommendation <br>
              <input type="radio" name="Via"  value="Other">
              Other: 
              <input type="TEXT" name="Othervia" value="" maxlength="45" size="28">
              <br>
              <br>
              </p>
          </td>
        </tr>
        <tr> 
          <td COLSPAN="2" ALIGN="center" valign=TOP width=390 >
            <input type="BUTTON" value="Send inquiry" name="SUBMIT" onclick="javascript:FormVerify()">
            <input type="RESET" value="Clear" name="RESET">
</td>
        </tr>
      </table>
    </center>
	<INPUT NAME="Update" 	TYPE="Hidden" VALUE="true"	>
	<INPUT NAME="ElementID" TYPE="Hidden" VALUE="372"	>
  </form>
</CFIF>

