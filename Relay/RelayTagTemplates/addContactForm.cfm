<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			addContactForm.cfm
Author:				SWJ
Date started:		2003-02-15

Purpose:	To provide a form for adding all of the contact details in one hit.

Usage:

introPhraseTextID - this should be set to the phraseTextID to describe some introductory text for this form
formCompletePhraseTextID - this should be set to the phraseTextID to describe the completion text for this form

showCols (optional) This allows you to specify which of the default values listed you
	want to show. 	The order they appear on the screen is controlled by the list.
	The default values are: insSalutation, insFirstName, insLastName, insJobDesc, insEmail,
	insOfficePhone, insMobilePhone, insSiteName, insAddress1, insAddress2, insAddress3,
	insAddress4, insAddress5, insPostalCode, insCountryID
mandatoryCols (optional) This determines which columns are mandatory. The default
	list is as follows: insSalutation, insFirstName, insLastName, insEmail, insSiteName,
	insAddress1, insPostalCode.
frmNext (optional) This specify which template is processed after the form data.
frmDebug (optional) Yes or no - Defaults to No. If set to yes the frmNext form will
	not be processed. Instead a message telling you what was added and updated is shown.
frmReturnMessage (optional) Yes or no. Default is no. Returns a variable containing
	success/failure message.
urlRoot (optional) Defaults to application. urlRoot. This allows you to specify the
	root for this screen allowing you to call it from outside the URL Root path.
flagStructure (optional) Defaults to none. This allows you to specify the profiles
	that will be set and the values they can be set to.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-05-04			AJC			Added addContactFormType to allow for short and long forms
2006-05-04			AJC			Added check to see if user is already registered
2006-05-08
2006-07-18			WAB			made a change so that if usecurrentuser is set and is true, then the contact form is skipped
2007-01-15			SSS			added datacaptureMethod to be able to switch from 2step method to incrmental method
2007-01-15			SSS			Added ShowLocationCols,showPersonCols to switch fields on and of for the incrmental method
2007-03-22			SSS			will only show live langauges for people to select from.
2008-04-10			SSS			If the switch skipcompanycheck is passed as true the company check will be by-passed and location selection page will appear first
2008-11-14			AJC	 		Hide Magic Number
2009/06/30			NJH			P-FNL069 - Removed frmGlobalParameters in an effort to secure code
2009-09-30			SSS			To show captcha if it is selected as on
2010/04/28			NJH			P-PAN002 Set the oppListingsEid as a param . This will show once the opportunity has been saved, rather than back to the addContactForm.
2010/06/30  		PPB 		P-PAN002 New Opp screen - we want to limit the country dropdown to list just the countries relevant to the reseller
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2012-08-10 			PPB 		Case 428651 added comments relating to EndCustomerOppContacts flag
2014-01-31			WAB 		removed references to et cfm, can always just use / or /?
2016/06/13 			NJH			JIRA PROD2016-347 - Added two new attributes. AddNewAddress  - used to toggle the visiblity of the 'addNewLocation' button; showAllLocationsForInvitedPartner - show all the locations of the user's organisation who has done the inviting
2016-06-22			WAB/AJC  	PROD2016-1298 default oppTypeID not getting passed to opportunityEdit.cfm 

Possible enhancements:


--->

<!--- 2006-05-04 AJC P_Cor001 --->
<cf_param name="dataCaptureMethod" default="2step" label="Data Capture Method" validvalues="2step,Incremental" reloadOnChange="true"/>

<cf_param name="showCols" default="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" type="string" label="Show Columns" validValues="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" multiple="true" size="5" condition="dataCaptureMethod eq '2step'"/>
<!--- params use by dataCaptureMethod=incremental --->
<cf_param name="ShowLocationCols" default = "insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax" label="Show Location Columns" validValues="insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax" multiple="true" size="5" condition="dataCaptureMethod eq 'incremental'"/>
<cf_param name="showPersonCols" default = "insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone" label="Show Person Columns" validValues="insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone" multiple="true" size="5" condition="dataCaptureMethod eq 'incremental'"/>
<cf_param name="showAdditionalCols" default="" label="Show Additional Columns"/>
<cf_param name="mandatoryColsOverride" default="" label="Mandatory Columns Override" description="List of columns that will be mandatory" validValues="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" multiple="true" size="5"/>
<cf_param name="addContactFormType" default="" validValues="NoVATNumber,Short" nullText=" " description="The type of contact form to use. Setting this will override the 'Show Columns' value."/>
<cf_param label="Show Required Symbol" name="showRequiredSymbol" type="boolean" default="No"/> <!--- NJH 2006/11/27  --->
<cf_param label="Show Captcha" name = "showCaptcha" type="boolean" default="No"/> <!--- SSS 2009-09-30 to show captcha--->

<cf_param label="Show Introduction Text" name="showIntroText" default="Yes" type="boolean"/>
<cf_param label="Introduction PhraseTextID"  name="introPhraseTextID" type="string" default=""/>
<cf_param label="Form Completed PhraseTextID" name="formCompletePhraseTextID" type="string" default=""/>

<cf_param name="organisationTypeID" type="numeric" default="1" label="Organisation Type" validValues="select organisationTypeID as value, typeTextID as display from organisationType where typeTextID != 'AdminCompany' order by typeTextID"/>
<cf_param name="liveLanguage" default="No" label="Show Live Languages" type="boolean"/> <!--- ss will only show live langauges for people to pick from. --->
<cf_param name="CountryGroup" default="No" label="Show Country Groups" type="boolean"/>
<cf_param label="Default Country in Country Dropdown" name="defaultCountryID" default="0" type="numeric"/>
<cf_param label="Restrict To User Organisation Countries"  name="restrictToUserOrgCountries" default="No" type="boolean"/> <!--- PPB 2010/06/30 --->

<cf_param label="Opportunity Type" name="oppTypeID" type="numeric" default="1" validValues="select oppTypeId as value, oppType as display from oppType order by oppType"/>  <!--- NJH 2006/10/06  - used to pass to opportunityEdit--->
<cf_param label="Opportunity Listings Element ID" name="oppListingsEID" type="string" default=""/> <!--- NJH 2010/04/28 P-PAN002 - used to redirect to the opportunity listing once the opportunity has been saved. --->

<cf_param label="Hide Direct Line Extension" name="hideDirectLineExtension" default="Yes" type="boolean"/><!--- pkp 2008-04-22 add switch to hide extension text box --->
<!--- START: 2008-11-14	AJC Hide Magic Number --->
<cf_param label="Show Magic Number" name="showMagicNumber" type="boolean" default="Yes"/>

<!--- NJH 2016/06/13 JIRA PROD2016-347 - added two params below to pass to add contact form.--->
<cf_param name="showAllLocationsForInvitedPartner" type="boolean" default="no" description="Show all locations at partner organisation for invited partner?" condition="[[getSetting('plo.useLocationAsPrimaryPartnerAccount')]]"/>
<cf_param name="allowNewAddress" type="boolean" default="no" description="Can the partner create a new location if their location is not available?"/>

<cf_param label="Show Person Data Only" name="personDataOnly" default="No" type="boolean"/>
<cf_param label="Should User Be Initialised" name="initialiseUser" default="No" type="boolean" description="Enables person to be added and immediately become the currentuser"/>

<!--- Checks if user is registered and asks to confirm add new --->
<cf_param name="checkUserRegistered" default="No" label="Check If User Is Registered" type="boolean"/>
<cf_param name="resendPasswordEID" default="resendpw" label="ResendPassword Element ID"/>

<cf_param name="skipcompanycheck" default="No" label="Skip Company Check" type="boolean"/>

<!--- 2008-04-04 Michael Roberts - added for Issue 155: Landing page for invite colleague link from email --->
<cfparam name="isMagicNumber" type="boolean" default="no">
<cfset isMagicNumber = "true">
<!--- if checkUserRegistered true is passed --->


<cfif checkUserRegistered>

	<!--- check that checkUserRegisteredFlagID is passed to the tag --->
	<cfif not isDefined("checkUserRegisteredFlagID")>
		phr_addContactForm_checkUserRegisteredFlagID_error
		<cfexit method="EXITTEMPLATE">
	</cfif>

	<cfif not request.relayCurrentUser.isUnknownUser
		and	not structKeyExists(form,"addContactSaveButton")
		and not structKeyExists(url,"ignorecontactform")
		and not structKeyExists(url,"usecurrentuser") >   <!--- WAB I think that this may need to be (not isDefined("usecurrentuser") OR usecurrentuser is true) --->

		<cfif application.com.relayElementTree.isElementInATree(elementTree=request.currentelementtree,ElementID=resendPasswordEID) is "false">
			phr_addContactForm_resendpw_error
			<cfexit method="EXITTEMPLATE">
		</cfif>

		<CFSET isApproved = application.com.flag.checkBooleanFLagByID(flagid='#checkUserRegisteredFlagID#', entityid = request.relayCurrentUser.personid)>

			<cfoutput>
				<cfif isApproved>
					phr_addContactForm_AlreadyRegistered
				<cfelse>
					<p><a href="#cgi.script_name#?eid=#resendPasswordEID#">phr_addContactForm_sendpassword</a></p>
				</cfif>
				<p><a href="#request.query_string_and_script_name#&usecurrentuser=false">phr_addContactForm_ChoosePerson_RegisterAsSomeElse </a></p>
		</cfoutput>
		<cfexit method="EXITTAG">
	</cfif>
</cfif>
<!--- 2006-05-04 AJC P_Cor001 --->
<!--- set the default parameters which can be superceded by setting params in the element  --->
<!--- short can be suplemented with additional cols using showAdditionalCols --->

<cfif isdefined('addContactFormType')>
	<cfswitch expression="#addContactFormType#">
		<cfcase value="short">
			<cfset showCols="insFirstName,insLastName,insJobDesc,insEmail,insSiteName,insCountryID">
		</cfcase>
		<cfcase value="NoVATNumber">
			<cfset showCols="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID">
		</cfcase>
	</cfswitch>
</cfif>

 <!--- 2005/07/06 - GCC - showAdditionalCols SUPLEMENTS the default showCols list --->
<cfif isDefined("showAdditionalCols") and len(showAdditionalCols) gt 0>
	<cfloop index="i" list="#showAdditionalCols#" delimiters="-">
		<cfset showCols = listAppend(showcols,i)>
	</cfloop>
</cfif>

<!--- 2005/07/06 - GCC - mandatoryColsOverride REPLACES default mandatory list --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
<cfelse>
	<!--- NJH 2010/10/22 changed to use settings
	<cfset mandatoryCols = "insSalutation,insFirstName,insLastName,insEmail,insSiteName,insAddress1,insPostalCode,insOfficePhone"> --->
	<cfset mandatoryCols = application.com.settings.getSetting("plo.mandatoryCols")>
</cfif>

<cfparam name="frmNext" default="return=this" type="string">
<cfparam name="frmDebug" default="no" type="boolean">
<cfparam name="showForm" default="yes" type="boolean">
<cfparam name="frmReturnMessage" default="no" type="boolean">
<cfparam name="flagStructure" default="" type="string">
<cfparam name="fieldListQuery" default="relayFieldList" type="string">

<cf_param label="Process ID" name="ProcessID" default="0" type="numeric"/>

<!--- END: 2008-11-14	AJC Hide Magic Number --->
<cfif not isDefined("introPhraseTextID")>
	phr_addContactForm_introPhraseTextID_error
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("formCompletePhraseTextID")>
	phr_addContactForm_formCompletePhraseTextID_error
	<cfexit method="EXITTEMPLATE">
</cfif>


<!--- WAB added 2006-07-18 aims to bypass the contact form and continue--->
<cfif isDefined("usecurrentuser") and  usecurrentuser and not request.relaycurrentuser.isUnknownUser>
	<cfset variables.thisPersonID = request.relayCurrentUser.personid>
	<cfset variables.thisorgID = request.relayCurrentUser.organisationid>
	<cfset form.addContactSaveButton = "yes">

<cfelse>



	<cfif showForm eq "yes"><!--- this is set to no later when a screen is processed --->

		<CFIF showIntroText eq "yes">

			<!--- we don't want to reshow the introPhraseTextID after thisPersonid is set --->
			<cfoutput><p>phr_#htmleditformat(introPhraseTextID)#</p></cfoutput>
		</CFIF>
		<!--- 2006-05-04 AJC P_Cor001 --->
		<cfif defaultCountryID is 0>
			<cfif request.relaycurrentuser.isunknownuser is true>
				<cfset defaultCountryID=request.relaycurrentuser.possiblecountryidforunknownuser>
			<cfelse>
				<cfset defaultCountryID=request.relaycurrentuser.countryid>
			</cfif>
		</cfif>
		<!--- pkp 2008-04-22 add switch to hide extension text box --->
		<cf_addRelayContactDataForm
			showCols = "#showCols#"
			showLocationCols = "#showLocationCols#"
			showPersonCols = "#showPersonCols#"
			mandatoryCols = "#mandatoryCols#"
			defaultCountryID = "#defaultCountryID#"
			frmNext = "#frmNext#"
			frmDebug = "#frmDebug#"
			showIntroText = "no"
			frmReturnMessage = "#frmReturnMessage#"
			flagStructure = "#flagStructure#"
			fieldListQuery = "#fieldListQuery#"
			liveLanguage="#liveLanguage#"
			personDataOnly = "#personDataOnly#"
			initialiseUser= #initialiseUser#
			hideDirectLineExtension = #hideDirectLineExtension#
			showRequiredSymbol=#showRequiredSymbol#
			showCaptcha=#showCaptcha# <!--- SSS 2009-09-30 Added this as part of showing captcha or not --->
			datacapturemethod=#datacapturemethod#
			countryGroup="#CountryGroup#"
			insOrganisationTypeID=#organisationTypeID#
			skipcompanycheck="#skipcompanycheck#"
			showMagicNumber=#showMagicNumber#
			restrictToUserOrgCountries=#restrictToUserOrgCountries#
			allowNewAddress=#allowNewAddress#
			showAllLocationsForInvitedPartner=#showAllLocationsForInvitedPartner#
			>
		</cfif>
</cfif>

<CFIF isDefined("debug") and debug eq "yes">

	<cfoutput><br>frmNext = #htmleditformat(frmNext)# #left(frmNext,14)# #mid(frmNext,15,len(frmNext)-14)#
	<br>mandatoryCols = #htmleditformat(mandatoryCols)#
	<cfif isDefined("thisPersonid") and isNumeric(thisPersonid)><br>thispersonid=#htmleditformat(thispersonid)#</cfif>
	<cfif isDefined("thisOrgid") and isNumeric(thisOrgid)><br>thisOrgid=#htmleditformat(thisOrgid)#</cfif>
	<cfif isDefined("FieldNames")>
		<CFLOOP INDEX="i" LIST="#FieldNames#">#htmleditformat(i)#=#htmleditformat(evaluate(i))#, </CFLOOP>
	</cfif>
	</cfoutput>
</CFIF>

<cfif isDefined("frmNext") and left(frmNext,14) eq "returnScreenid" and isDefined("thispersonid") and isNumeric(thispersonid)>


	<!--- if thispersonid has been set by the frmNext starts with 'returnScreenid' show a screen --->
	<CFIF not isdefined("m")>
		<CFSET message="">
	</CFIF>

	<CFSET qs=request.query_string>
	<cfif not isDefined("frmNextPage")><cfset frmNextPage = "thisPage"></cfif>
	<CFIF isDefined("frmNextPage") and frmNextPage eq "thisPage">
		<cfset frmNextPage = "/?#qs#&m=1&showform=no">
	</CFIF>

	<CFSET frmPersonID = thispersonid> <!--- set in cf_addRelayContactDataForm --->
	<CFSET frmCurrentScreenID = mid(frmNext,15,len(frmNext)-14)>
	<CFSET debug = "no">
	<CFIF isDefined("debug") and debug eq "yes">
		<cfoutput>frmCurrentScreenID = #htmleditformat(frmCurrentScreenID)#<br>frmPersonID = #htmleditformat(frmPersonID)#</cfoutput>
	</CFIF>
	<CFINCLUDE template= "/screen/showScreenNew.cfm">

<!--- ==============================================================================
    This section calls redirector
=============================================================================== --->
<cfelseif isDefined("frmNext") and left(frmNext,15) eq "returnProcessid" and isDefined("thispersonid") and isNumeric(thispersonid) and isDefined("Processid") and isNumeric(Processid)>
	<!--- if thispersonid has been set by the frmNext starts with 'returnScreenid' show a screen --->
	<CFIF not isdefined("m")>
		<CFSET message="">
	</CFIF>

	<CFSET qs=request.query_string>
	<cfif not isDefined("frmNextPage")><cfset frmNextPage = "thisPage"></cfif>
	<CFIF isDefined("frmNextPage") and frmNextPage eq "thisPage">
		<cfset frmNextPage = "/?#qs#&m=1&showform=no">
	</CFIF>

	<CFSET frmPersonID = thispersonid> <!--- set in cf_addRelayContactDataForm --->
	<CFSET debug = "no">
	<CFIF isDefined("debug") and debug eq "yes">
		<CFSET debugProc = "yes">
	</CFIF>
	<CFSET frmProcessID = processID>
<!--- 	<CFSET frmStepID = "1"> --->
	<cfset hideScreenBorder = "yes">
	<!--- NJH 2009/06/30 P-FNL069
	<CFPARAM NAME="frmGlobalParameters" DEFAULT="entityID"> --->
	<!--- <cfinclude template="/screen/redirector.cfm"> --->
	<!--- <cflocation url="/proc.cfm?frmProcessID=#processID#&frmStepID=1&frmPersonID=#thispersonid#" addtoken="No"> --->
	<!--- 	don't put this line back in 'cause if this is used to add a friend, then it effectively logs out an existing user
<cfset application.com.globalFunctions.cfcookie(name="user" value="#thispersonid#-#application.WebUserID#-0")> --->
	<cfoutput>
		<form action="/proc.cfm" name="procForm">
			<CF_INPUT type="hidden" name="pid" value="#thispersonid#">
			<CF_INPUT type="hidden" name="prid" value="#processID#">
<!--- 			<input type="hidden" name="stid" value="1">  WAB removed 2006-07-17  - will start at lowest stepid--->
			<CF_INPUT type="hidden" name="entityID" value="#thisOrgID#">
		</form>
	<SCRIPT type="text/javascript">
		document.procForm.submit()
	</script>
	<a href="/proc.cfm?prid=#processID#&pid=#thispersonid#&entityID=#thisOrgID#">Click here if you are not redirected automatically</a>
	</cfoutput>

<!--- ==============================================================================
    This section calls opportunityEdit if frmNext is set to returnLeadScreen
=============================================================================== --->
<cfelseif isDefined("frmNext") and frmNext eq "returnLeadScreen" and isDefined("thisOrgID") and isNumeric(thisOrgID)>

	<cfset showForm="no">
	<cfset showIntroText="no">
	<cfset entityID=thisOrgID>
	<cfif isDefined("thisPersonID")>
		<cfset contactPersonID = thisPersonID>
	</cfif>
	<cfset returnURL=application.com.security.encryptURL("/?eid=#oppListingsEID#&orgID=#thisOrgID#&&startingTab=newopportunities")>

	<!--- 2012-08-10 PPB Case 428651 now we also set the EndCustomerOppContacts flag in opportunityTask.cfm
	this one is still required for when the adding a NEW opp on the Portal - we want a newly added contact to have the flag so that they appear in the contact dropdown on the opp (ie before we have saved the opp)
	it is not relevant on the internal because we want to associate the reseller sales person (not current user) to the new contact added
	 --->
	<!--- NYB LHID5122 start --->
	<cfset endCustomerFlagID=application.com.flag.doesFlagExist("EndCustomerOppContacts")>
	<cfif endCustomerFlagID gt 0>
		<cfif not request.relayCurrentUser.isInternal and isDefined("THISPERSONID")>
			<cfset endCustomerFlagID=application.com.flag.setFlagData(flagid="EndCustomerOppContacts",entityID=THISPERSONID,data=request.relayCurrentUser.personID)>
		</cfif>
	</cfif>
	<!--- NYB LHID5122 end --->

	<!--- WAB/AJC  PROD2016-1298 opportunityEdit.cfm needs oppTypeID in URL or form scope to use it as a default --->
	<cfset url.oppTypeID = oppTypeID>

	<!--- <cfinclude template="/leadManager/opportunityParams.cfm">
	<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true"> --->
	<cfinclude template="/leadManager/opportunityEdit.cfm">

<!--- ==============================================================================
    This section calls element if frmNext is set to element and GoToEID is numeric
=============================================================================== --->
<cfelseif isDefined("frmNext") and listfirst(frmNext,":") eq "eid" and isDefined("thispersonid") and isNumeric(thispersonid)>

	<cfoutput>
	<script>
		window.location.href = '/?eid=#listlast(frmNext,":")#';
	</script>
	</cfoutput>
<cfelse>
	<!--- to hide the form when we return --->
	<!--- we have reached the end of the line --->
	<!--- 2008/01/14 GCC If specifically being told not to show the form and we aren't going to a screen / process / opp then show the Thank you phrase --->
	<cfif isDefined("addContactSaveButton") or (isdefined("showForm") and not showform)>
		<cfoutput><cfif not isDefined("hideFormCompletePhraseTextID")><p>#application.com.relaytranslations.translatePhrase(phrase="phr_#htmleditformat(formCompletePhraseTextID)#")#</p></cfif></cfoutput>
	</cfif>
</cfif>