<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		getXMLReport.cfm
Author:			Simon
Date created:	2002-05-18

	Objective - loads an XMLReport based on parameters defined in an element
		
	Syntax	  -	Should only be called as part of a relayTagInclude
	
	Parameters - frmreportName - this should be the name of a valid XML report
				 debugProc - this can be set to yes
				
	Return Codes - none 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

<CF_EvaluateNameValuePair
	NameValuePairs = #element_parameters#
>
<CFIF isDefined("debugProc") and debugProc eq "yes">
	<cfoutput>frmReportName = #htmleditformat(frmReportName)#</cfoutput>
</CFIF>

<CFIF IsDefined("frmReportName")>
	<CFSET frmReportName = #frmReportName#>
	<CFIF isDefined("debugProc") and debugProc eq "yes">
		<cfoutput>frmReportName inside IF = #frmReportName#</cfoutput>
	</CFIF>
	<CF_RelayXMLReport
		reportName = "#frmReportName#"
	>

</CFIF>



