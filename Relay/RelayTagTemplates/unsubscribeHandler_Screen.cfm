<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		unsubscribeHandler_Screen.cfm
Author:			WAB
Date created:	May 2006

	Objective - To receive unsubscribe requests and to display a suitable screen where other options can be set
	
		This template can be called as a relaytag
		however it can also be used directly.  
		So that it will work even if an element hasn't been set up for it, it is also called from unsubscribeHandler_Screen_withBorder (which wraps a border around it!)
		
		Note that this code does not have the following feature which was present in an earlier version of unsubscribe code
			Doesn't have ability to email accoutn manager after an unsubscribe

	
	WAB 2007-04-25

	2013-08-07 	PPB 	Case 434787 don't actually set the flag for the fid sent in but check the checkbox and wait for the user to submit
		
2013-08-15	YMA		Case 435739 pass unsubscribe function to showScreenNew.cfm to confirm unsubscription decission.
--->




<cfif not request.relayCurrentUser.isUnknownUser>
		<!--- got to know who they are to do unsubscribe --->

<!--- this could be set when calling the relay tag 
		If this screen does not exist then we display a default screen
--->
		<cf_param name="screenID" default="unsubscribeHandler"/>
		
			<!--- process the update --->
		<cf_updatedata>
		
		
			<cfset request.UnSubscribeupdateDone = false>
		<cfif isDefined("FRMTABLELIST")>	<!--- if this variable is defined then we know that the user has posted the form, so we can display a message of some sort --->
			<cfset request.UnSubscribeupdateDone = true>
		</cfif>
		
			<cfset frmMethod = "edit">
		<cfif request.UnSubscribeupdateDone>
			<cfset frmMethod = "view">
			<cfset frmNoButton = 1>
		</cfif>

 
			
		<!--- This is the flagID which has been passed for setting --->
		<cfif isDefined("fid")>
			<cfset variables.fid = listfirst(fid)> <!--- to fix a bug where to fids got intot the url on tren --->

			<!--- START 2013-08-07 PPB Case 434787 don't actually set the flag but check the checkbox (below) and wait for the user to submit --->
			<!--- 
			<!--- WAB 2007-04-25 - need to be sure that this fid is in the unsubscribe group, otherwise it can be doctored so added this if statement--->
			<cfif application.com.flag.doesflagExist(fid) and application.com.flag.getFlagStructure(variables.fid).flagTextID contains "noComms">
				<cfset application.com.flag.setBooleanFlagByID(entityID = request.relayCurrentUser.Personid, flagid = variables.fid)>
			</cfif>
			 --->
			<!--- END 2013-08-07 PPB Case 434787 --->
		</cfif>
		

		
		
			<!--- we are going to use showscreencode to display a screen, but if there is no screen defined then will make a dummy one
			 --->

		<!--- is there a screen? --->
		<cfquery name="getScreen" datasource="#application.sitedatasource#">
			select * from screens where <cfif isNumeric(screenID)>screenid<cfelse>screentextid </cfif>= '#screenID#'
		</cfquery>


		<cfif getScreen.recordCount is 0 >
			<cfset screendefinition = application.com.screens.createScreenDefinition (fieldsource="text",fieldtextid="phr_UnsubscribeScreenInstructions",allcolumns=1,specialformatting='column=1',method='#frmMethod#',condition='not request.UnSubscribeupdateDone')>
			<cfset screendefinition = application.com.screens.addRowToScreenDefinition (screenDefinition=#screendefinition#,fieldsource="text",fieldtextid="phr_UnsubscribeScreenThankYou",allcolumns=1,specialformatting='column=1',method='#frmMethod#',condition='request.UnSubscribeupdateDone')>
			<cfset screendefinition = application.com.screens.addRowToScreenDefinition (screenDefinition=#screendefinition#, fieldsource="flaggroup",fieldtextid="Unsubscribe",allcolumns=0,specialformatting='column=1,noflaggroupname=1',method='#frmMethod#')>
			<cfset frmCurrentScreenID = "dummyScreen">
			<cfset "screen_dummyscreen_#request.relaycurrentuser.countryid#_#frmMethod#" = screendefinition>
		<cfelse>
			<cfset frmCurrentScreenID = getScreen.screenid  >
		</cfif>

		<cfparam name="frmShowScreenAction" default = "">

		<cfif frmShowScreenAction is "">
			<!--- need to come back to this place, but need to remove the fid from the url string, otherwise it will be set again --->
				<cfset newQueryString= application.com.regExp.removeItemFromNameValuePairString(inputString=request.query_string,itemtodelete="fid",delimiter="&")>
			<cfset frmShowScreenAction = "?#newQueryString#">
		</cfif>


		<cfset frmPersonid = request.relaycurrentuser.personid>

		<cfif isDefined("fid")>
				<CFQUERY NAME="checkValidFlagID" datasource="#application.sitedatasource#">
				Select flagid, flag.description, namephrasetextid as FlagPhrase , lookuplist.itemtext as CommTypeDescription
					from flag 
				left join lookuplist on flag.flagtextid = 'nocommsType'+convert(varchar,lookupid)
				where flagID =  <cf_queryparam value="#FID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>

				<cfif checkvalidFlagID.recordcount eq 1>
						<center><cfoutput>phr_UnsubscribeHandler_NoLongerReceive <BR>#application.com.flag.getFlagStructure(fid).translatedname#</center><BR><BR></cfoutput>
				</cfif>
		</cfif>

		<cfif isDefined("fid")>
			<!--- START 2013-08-07 PPB Case 434787 check the box relating to the fid sent in --->
			<cfset unsubscribeFlagGroupId = application.com.flag.getFlagStructure(flagID=fid).flagGroupId>

			<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('input[name="Checkbox_#unsubscribeFlagGroupId#_#request.relayCurrentUser.personId#"][value="#fid#"]').attr('checked',true);
			});
			</script>
		</cfif>
		<!--- END 2013-08-07 PPB Case 434787 --->

		<!--- 2013-10-10	YMA	Case 435739 re-instate on submit check in such a way that does not break form validation. --->
		<cfset ConfirmationMessage="phr_Unsubscribe_areYouSure">			
		<cfinclude template =	"/screen/showScreenNew.cfm">

<cfelse>
	<!--- unknown user should not be allowed to access this page, but it is required that it can be accessed without logging in --->		
		
</cfif>
