<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		Register Interest In a Program


		A Page for recording interest in a program or something like that.
		This works in two modes:
		a)  when the user is not logged into the external protal (we test 
			partner cookie)
		b) 	when we know who the user is because we they're logged in
		
		Parameters
		
			Required
		a)	CommentInsert - this should describe the program or thing they are 
							registering their interest in.  If they are external this will be 
							used as part of the email that is sent to the sendTo person
			Optional
		b)	sendTo 		  - should be a flagTextID of the flag to check for the correct 
							person's email address.
		c)  feedBackScreenMessage - this should contain the appropriate feedback meesage 
							for the screen after a person has registered their interest
		d) 	programFlagTextID - this should be set to the flagTextID that will be
							set if they register their interest when they are logged in
		
		V 1 SWJ 2002-06-09
		V 1.1 DAM 2002-06-11
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

--->
<!---
		*****************
			INITIALISE
		*****************
--->

<!--- Default values for optional parameters --->
<cfparam name="feedBackScreenMessage" type="string" default="We have recorded your interest.  Thank you.  We will contact you when appropriate.">
<cfparam name="sendToEmail" type="string" default="help@foundation-network.com">
<cfparam name="personalise" type="string" default="no">


<!--- See if we know who the user id --->
<CFIF isDefined("request.relayCurrentUser.personid") AND request.relayCurrentUser.personid NEQ application.unknownPersonid>
	<CFSET frmPersonID=request.relayCurrentUser.personid>
	<CFSET KnownPerson=true>
<CFELSE>
	<CFSET frmPersonID=application.unknownPersonid>
	<CFSET KnownPerson=false>
</CFIF>

<!--- Get the address to send emails to for this person --->
<CFIF KnownPerson>
	<cfquery name="getSendToEmail" datasource="#application.siteDataSource#">
		SELECT i.entityid, p.email FROM person p 
		INNER JOIN integerMultipleFlagData i ON
		p.personID = i.data
		INNER JOIN flag f ON f.flagID=i.flagID
		INNER JOIN location l ON l.countryid = i.entityid or i.entityid=0
		INNER JOIN person pp ON pp.locationid = l.locationid
		WHERE 
		f.flagTextID =  <cf_queryparam value="#sendTo#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND
	  	pp.personid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	  	ORDER BY i.entityid DESC
	</cfquery>
<CFELSE>
	<cfquery name="getSendToEmail" datasource="#application.siteDataSource#">
		SELECT i.entityid, p.email FROM person p 
		INNER JOIN integerMultipleFlagData i ON
		p.personID = i.data
		INNER JOIN flag f ON f.flagID=i.flagID
		WHERE 
		f.flagTextID =  <cf_queryparam value="#sendTo#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND
	  	i.entityid=0
	  	ORDER BY i.entityid DESC
	</cfquery>
</CFIF>

<CFIF getSendToEmail.recordcount GT 0 and getSendToEmail.email IS NOT "">
	<CFSET sendToEmail=getSendToEmail.email>
</CFIF>

<!--- Get information about the program, flagid and name --->
<cfquery name="getProgramDetails" datasource="#application.siteDataSource#" dbtype="ODBC">
select flagid, name from flag where FlagTextID='#programFlagTextID#'
</cfquery>

<cfif getProgramDetails.recordcount neq 1>
	<cfabort showerror="Sorry - we do not have a program defined with that textID">
</cfif>

<!--- Client side verification --->
<SCRIPT>

	function FormVerify(){
		var nForm=document.ContactForm;
		var msg = "";
		if (nForm.Person.value == "") {
			msg += "Please enter your name.\r\n";
		}
		if (nForm.Organisation.value == "") {
			msg += "Please enter your organisation.\r\n";
		}
		if (nForm.Email.value == "") {
			msg += "Please enter your Email.\r\n";
		}else{
			if (verifyEmailV2(nForm.Email.value) == false){
				msg += "Please enter a valid Email.\r\n";
			}
		}
		if (msg != ""){
			alert(msg);
		}else{
			nForm.submit();
		}
	}

	function verifyEmailV2(thisValue) {
		NumberFormatRe = /^[\w_\.\-']+@[\w_\.\-']+\.[\w_\.\-']+$/
		return NumberFormatRe.test(thisValue) 
	}
	
</SCRIPT>



<!---  
		*****************
			FUNCTIONS 
		*****************
--->
<CFIF KnownPerson>

	<!--- We know who they are so send email, set flag and thank you --->

	<CFSET confirm=true>
	<CFSET mailtext="phr_registerinterstmessage_known" >
	<cfquery name="getpersondetails" datasource="#application.siteDataSource#" dbtype="ODBC">
		SELECT PersonID, FirstName, LastName, OrganisationName, Address1, Address2, Address3, Address4, Address5, PostalCode,
		email, officephone
		FROM person p INNER JOIN organisation o ON o.organisationid = p.organisationid
		INNER JOIN location l on l.locationid = p.locationid
		WHERE personid=#frmpersonid#
	</cfquery>
	<CFSET personid=getpersondetails.personid>
	<CFSET FirstName=getpersondetails.FirstName>
	<CFSET LastName=getpersondetails.LastName>
	<CFSET OrganisationName=getpersondetails.OrganisationName>
	<CFSET Address1=getpersondetails.Address1>
	<CFSET Address2=getpersondetails.Address2>
	<CFSET Address3=getpersondetails.Address3>
	<CFSET Address4=getpersondetails.Address4>
	<CFSET Address5=getpersondetails.Address5>
	<CFSET PostalCode=getpersondetails.PostalCode>
	<CFSET email=getpersondetails.email>
	<CFSET officephone=getpersondetails.officephone>
	
	<CFIF Personalise is true>
		<CFSET Address=FirstName & " " & LastName & " of " & OrganisationName>
	</CFIF>

	<CFQUERY NAME="IsFlagSet" DATASOURCE="#application.sitedatasource#">
		SELECT 1 FROM booleanflagdata
		WHERE entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagid =  <cf_queryparam value="#getProgramDetails.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<CFIF IsFlagSet.recordcount EQ 0>
		<CFQUERY NAME="SetFlag" DATASOURCE="#application.sitedatasource#">
			INSERT INTO booleanflagdata (entityid, flagid) VALUES (<cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getProgramDetails.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</CFQUERY>
	</CFIF>
	
<CFELSEIF IsDefined("form.Update") and form.update eq "true">
	
	<!--- We dont know who they are but they just submitted details --->
	
	<CFSET confirm=true>
	<CFSET mailtext="phr_registerinterstmessage_unknown">
	<CFIF Personalise>
		<CFSET Address=Person & " of " & Organisation>
	</CFIF>
<CFELSE>

<!---  
		*********************************************
			We dont know who they are so ask them 
		*********************************************
--->
	<CFSET confirm=false>
	
	<form method="post" name="ContactForm" onSubmit="return checkfields()">
		<CF_INPUT type="hidden" name="Subject" value="#programTextID#">
		<INPUT NAME="Update" TYPE="Hidden" VALUE="true">
	    <center>
	      <table width="390" border="0">
	        <tr> 
	          <td colspan="2" align="left" valign="top"> 
	            <p>Please enter your personal details:<br>
	              <br>
	            </p>
	          </td>
	        </tr>
	        <tr> 
	          <td width="90" valign="top"> 
	            <p>Name:</p>
	          </td>
	          <td width="300" valign="top"> 
	            <input type="TEXT" name="Person" maxlength=45 size=38>
	            </td>
	        </tr>
	        <tr> 
	          <td width="90" valign="top"> 
	            <p>Organisation:</p>
	          </td>
	          <td width="300" valign="top"> 
	            <input type="TEXT" name="Organisation" maxlength=45 size=38>
	            <br>
	            <br>
	            </td>
	        </tr>
	        <tr> 
	          <td width="90" valign="top"> 
	            <p>Telephone:</p>
	          </td>
	          <td valign=TOP width=300> 
	            <input type="TEXT" name="Telephone" maxlength=25 size=38>
	            </td>
	        </tr>
	        <tr> 
	          <td width="90" height="40" valign="top"> 
	            <p>Email:</p>
	          </td>
	          <td valign=TOP width=300 height="40">
	            <input type="TEXT" name="Email" maxlength=45 size=38>
	            <br>
	            <br>
	            </td>
	        </tr>
	        <tr> 
	          <td align=LEFT valign=TOP colspan=2> 
	            <p>Additional comments<br>
	              <br>
	              <textarea name="Comments" rows=6 cols=50><CFOUTPUT>#CommentInsert#</CFOUTPUT></textarea>
	              <br>
	             </p>
	          </td>
	        </tr>
	        <tr> 
	          	<td COLSPAN="2" ALIGN="center" valign=TOP width=390 >
	            	<input type="BUTTON" value="Send inquiry" name="SUBMIT" onclick="javascript:FormVerify()">
	            	<input type="RESET" value="Clear" name="RESET">
				</td>
	        </tr>
	      </table>
	    </center>
		<CFIF isDefined("ElementID")>
			<CFOUTPUT><CF_INPUT type="hidden" value="#ElementID#" name="ElementID"></CFOUTPUT>
		</CFIF>
	  </form>
</CFIF>

<!---  
		*********************************************
			    FINAL CONFIRMATION STEP              
		*********************************************
--->
<CFIF confirm>		
	<cf_mail to="#sendToEmail#"
        from="#application.AdminEmail#"
        subject="#getProgramDetails.name#"
	><cf_translate>#mailtext#</cf_translate>
	</cf_mail>
	<TABLE BORDER="0" WIDTH="100%">
	  <TR> 
	    <TD VALIGN="top" WIDTH="100%" HEIGHT="400"> 
	      	<P>
			<CFOUTPUT>
			<CFIF ISDefined("Address")>#htmleditformat(Address)#<BR></CFIF>
			#htmleditformat(feedBackScreenMessage)#
			</CFOUTPUT>
			</P>
	    </TD>
	  </TR>
	</TABLE>
</CFIF>

