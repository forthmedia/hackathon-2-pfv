<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NYB	2011-05-25	REL106: replaced V2 with V3
WAB 2015-11-27  PROD2015-458/BF-27 Added allowFramingByAllSites()
NJH 2016/02/23	Set the locator version to V5.
--->
<cfset application.com.request.allowFramingByAllSites()>
<cf_include template="\code\cftemplates\customRelayLocator.cfm" onNotExistsTemplate="\relay\relayTagTemplates\relayLocatorV5.cfm" getFieldXML="true">