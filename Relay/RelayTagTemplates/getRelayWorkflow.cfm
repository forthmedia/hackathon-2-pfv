<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		getRelayScreen.cfm
Author:			Simon
Date created:	2002-05-14

	Objective - loads a workflow based on parameters defined in an element
		
	Syntax	  -	Should only be called as part of a relayTagInclude
	
	Parameters - frmProcessID - this can be a textID of a processAction row or the 
					id of a process
				 debugProc - this can be set to yes
				
	Return Codes - none 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

<CFIF isDefined("debugProc") and debugProc eq "yes">
	<cfoutput>
		frmProcessID = <CFIF isDefined("frmProcessID")>#htmleditformat(frmProcessID)#<cfelse>not defined</cfif>
		frmStepID = <CFIF isDefined("frmStepID")>#htmleditformat(frmStepID)#<cfelse>not defined</cfif>
	</cfoutput>
</CFIF>

<CFIF IsDefined("frmProcessID") and isDefined("frmStepID")>
	<CFSET frmProcessID = #frmProcessID#>
	<CFSET debug = "no">
	<CFIF isDefined("debug") and debug eq "yes">
		<cfoutput>frmProcessID inside getRelayWorkflow = #htmleditformat(frmProcessID)#</cfoutput>
	</CFIF>
		<!--- WAB 2006-06-14 CR_SNY595
			removed table and rows around this line because it makes everything wider and ruins the borders
		 --->
		<cfinclude template="/screen/redirector.cfm">
<CFELSE>
	You need to defined an frmProcessID and an frmStepID variable for this element.
</CFIF>


