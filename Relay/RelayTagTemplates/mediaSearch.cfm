<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:			mediaSearch.cfm
Author:				AJC,YMA,AHL
Date started:		2006-11-28

Description:

relay_mediaSearch

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->

<cf_param label="Filter - Available Media Groups" name="filterFileTypeGroups" default=""  multiple="true" size="4" validValues="select filetypegroupid as value, heading as display from filetypegroup where portalSearch=1 and hasRecordRightsBitMask = 0 order by heading"/>
<cf_param label="Filter - Available Profiles" name="filterFlagGroups" default=""  multiple="true" size="4" validValues="select distinct f.FlagGroupTextID as value, f.flagGroup as display from vflagdef f where f.active=1 and f.entitytypeID=#application.entityTypeID.files# and f.datatable = 'boolean' union select 'language' as value, 'phr_mla_Language' as display union select 'fileExtension' as value, 'phr_mla_FileExtension' as display"/>
<cf_param label="Advanced Filter - Available Profiles" name="advancedFilterFlagGroups" default=""  multiple="true" size="4" validValues="select distinct f.FlagGroupTextID as value, f.flagGroup as display from vflagdef f where f.active=1 and f.entitytypeID=#application.entityTypeID.files# and f.datatable = 'boolean' union select 'language' as value, 'phr_mla_Language' as display union select 'fileExtension' as value, 'phr_mla_FileExtension' as display"/>
<cf_param label="Filters display as" name="filtersDisplayAs" default="Radio"  multiple="false" size="4" validValues="Radio,Checkbox,Dropdown"/>


<cf_param label="Show Text Search Box" name="filterShowTextSearchBox" default="true" type="boolean"/>
<cf_param label="Disable List View" name="disableListView" default="false" type="boolean" onChange="toggleListView();"/>
<cf_param label="Default View" name="defaultView" default="List" validvalues="List,Grid" multiple="false"/>
<cf_param label="Data to Show in List View" name="DataShowList" default="Title,Language,PublishDate,ThumbnailResourceImage" validvalues="MediaGroup,ResourceType,FileID,FileExtension,filesize,Source,Title,Description,Language,PublishDate,ThumbnailResourceImage,CreatedDate,Lastupdated" multiple="true"/>
<cf_param label="Data to Show in Grid View" name="DataShowGrid" default="Title,Language,PublishDate,ThumbnailResourceImage" validvalues="MediaGroup,ResourceType,FileID,FileExtension,filesize,Source,Title,Description,Language,PublishDate,ThumbnailResourceImage,CreatedDate,Lastupdated" multiple="true"/>


<cf_param label="Paging Type" name="PagingType" default="Normal" validvalues="Normal,Infinity" multiple="false" onChange="toggleInfinityScrollFields();"/>
<cf_param label="Infinity Scroll Number of Loads" name="scrollFor" default="4" validvalues="1,2,3,4,5,6,7,8,9,10"  multiple="false"/>


<cf_param label="Results Sort Order" name="sortOrder" default="f.name" validvalues="f.name,f.lastUpdated asc,f.lastUpdated desc,f.created asc,f.created desc,f.publishdate asc,f.publishdate desc" multiple="false"/>


<cf_param label="Records per page" name="pageSize" default="24" validvalues="6,12,18,24" multiple="false"/>
<cf_param label="Grid Columns" name="GridColumns" default="3" validvalues="2,3,4,6" multiple="false"/>

<cf_param label="Show Thumbnail Image" name="showThumbnailAsMainImage" default="true" type="boolean"/>
<cfparam name="showMediaGroupAsMainImage" default="true" type="boolean"/> <!---label="Show Media Group Image" --->
<cf_param label="Show File Extension Icon Image" name="showResourceTypeAsMainImage" default="true" type="boolean"/>

<cfparam  name="pathForFileType" default="resourcelibrary/filetype/"/> <!---label="Path for File Extension Icons"--->
<cfparam name="pathForMediaGroup" default="resourcelibrary/mediagroup/"/> <!---label="Path for Media Group Icons" --->
<cf_param label="Only show files tagged with this profile attribute" name="mustHaveFlagTextID" default="" multiple="false" validvalues="select f.FlagTextID as value, f.flag + ' (' + f.flagGroup + ')' as display from vflagdef f where f.active=1 and f.entitytypeID=#application.entityTypeID.files# and f.datatable = 'boolean' order by f.flagGroup, f.flag"/>
<cf_param label="Date Format" name="dateformatmask" default="medium" validvalues="short,medium,long,full" multiple="false"/>

<cfparam name="searchButtonImage" default=""/>

<cfif disableListView>
	<cfset defaultView="Grid">
</cfif>

<CFPARAM NAME="form.startRow" DEFAULT="1">
<cfparam name="frmCriteria" default="">
<cfparam NAME="form.advOnOff" default="">
<cfparam name="url.fileTypeGroupID" default="">
<cfparam NAME="form.fileTypeGroupID" default="#url.fileTypeGroupID#">

<cf_includeJavascriptOnce template="/javascript/lib/jquery/spinner.js">
<cf_includeJavascriptOnce template="/javascript/jquery.sticky-kit.min.js">
<cfif PagingType neq "Normal">
	<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscroll.js">
</cfif>
<cf_includeJavascriptOnce template="FileManagement/js/mediaLibrary.js">
<cf_includeCssOnce template="FileManagement/styles/mediaLibrary.css">

<cfparam name="selectedFlagTextIDs" default="">
<cfparam name="selectedLanguages" default="">
<cfparam name="selectedExtensions" default="">
<cfset mergedFileflagGroups = ListRemoveDuplicates(listAppend(filterFlagGroups,advancedFilterFlagGroups),',',true)>

<cfif mergedFileflagGroups neq "">
	<cfloop list="#mergedFileflagGroups#" index="flagGroupTextID">
		<cfif structKeyExists(form,"filterflaggroup_#flagGroupTextID#") and form["filterflaggroup_#flagGroupTextID#"] neq "">
			<cfset selectedFlagTextIDs = listAppend(selectedFlagTextIDs,form["filterflaggroup_#flagGroupTextID#"])>
		</cfif>
		<cfif structKeyExists(form,flagGroupTextID) and form[flagGroupTextID] neq "" and flagGroupTextID eq "language">
			<cfset selectedLanguages = listAppend(selectedLanguages,form[flagGroupTextID])>
		</cfif>
		<cfif structKeyExists(form,flagGroupTextID) and form[flagGroupTextID] neq "" and flagGroupTextID eq "fileExtension">
			<cfset selectedExtensions = listAppend(selectedExtensions,form[flagGroupTextID])>
		</cfif>
	</cfloop>
</cfif>

<cf_head>
  <SCRIPT type="text/javascript">
    function searchWindow(nPerson) {
      document.searchForm.submit();
    }
    function getFile(fileID) {
      openWin (' ','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
      form = document.getFileFormV2Search; <!--- 2015-09-14 DAN 1486 P-TAT005 --->
      form.fileID.value = fileID;
      form.submit();
    }
  </SCRIPT>
</cf_head>

<cfset fileFormV2ActionSearch = "/fileManagement/FileGet.cfm">
<cfif isDefined("isWebServiceCall") and isWebServiceCall>
    <cfset fileFormV2ActionSearch = "#fileFormV2ActionSearch#?#session.urltoken#">
</cfif>
<cfoutput>
  <FORM METHOD="get" NAME="getFileFormV2Search" action = "#fileFormV2ActionSearch#" target = "FileDownload">
    <input type="hidden" name="fileID" value="0">
    <input type="Hidden" NAME="FileTypeGroupID" value="0">
  </FORM>
</cfoutput>


<!--- =============================================================================
      SEARCH BOX
============================================================================== --->
<cfif not (not filterShowTextSearchBox and advancedFilterFlagGroups eq "" and filterFlagGroups eq "" and filterFileTypeGroups eq "")>
	<div class="searchElement" id="searchElementsBox">
	  <div class="">
		<!--- WRAPPER ROW --->
		<div id="filterSearch" class="">
		  <!--- WRAPPER COLUMN --->
		  <div class="wrapperCol">
			<cfoutput>
			  <!--- MAIN FORM ELEMENT --->
			  <cfform method="POST" action="#cgi.script_name#?eid=#eid###searchResults" name="searchForm" id="fileSearch" class="form-horizontal">
				<cfinput type="hidden" name="StartRow" value="1" class="form-control">

				<!--- SEARCH TEXT INPUT FIELD --->
				<div class="form-group">
				  <div class="TextSearchBox">
					<cfif filterShowTextSearchBox>
					  <cfinput class="form-control" type="Text" name="frmCriteria" value="#frmCriteria#" required="no" size="20" maxLength="255" placeholder="Search">
					  <!--- onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" --->
					</cfif>
				  </div>
				</div> <!--- / SEARCH TEXT INPUT FIELD --->

				<!--- MEDIA GROUPS --->
				<cfif filterFileTypeGroups neq "">
				  <cfset qry_get_filterFileTypeGroups = application.com.filemanager.getFileTypeGroups(filetypegroupids=filterFileTypeGroups) />
				  <cfif qry_get_filterFileTypeGroups.recordcount neq 0>
					<div class="mediaGroup borderBottom form-group">
					  <div class="col-md-2 col-sm-2 col-xs-3 filterMediaGroupTitle">
						<label>phr_mediaSearch_Filter_Media_Groups:</label>
					  </div>
						<cfif filtersDisplayAs eq "Dropdown">				<!--- 2016-08-05 PPB P-MIC001 Case 451195 --->
						  <div class="col-md-10 col-sm-10 col-xs-9 filterMediaGroupField">
							<div class="filterOption">
							  <select name="fileTypeGroupID" class="form-control">
								<option value="">
								  phr_mla_please_select
								</option>
								<cfloop query="qry_get_filterFileTypeGroups">
								  <option value="#filetypegroupID#" <cfif listfind(form.fileTypeGroupID,fileTypeGroupID,",")>selected</cfif>>
									#heading#
								  </option>
								</cfloop>
							  </select>
							</div>
						  </div>
						<cfelseif filtersDisplayAs eq "Radio">				<!--- 2016-08-05 PPB P-MIC001 Case 451195 --->
						  <div class="col-md-10 col-sm-10 col-xs-9 filterMediaGroupField">
							<cfloop query="qry_get_filterFileTypeGroups">
							  <div class="filterOption">
								<div class="filterfield">
								  <div class="radio">
									<label  for="#filetypegroupID#">
									  <input type="radio" id="#filetypegroupID#" name="fileTypeGroupID" value="#filetypegroupID#" <cfif listfind(form.fileTypeGroupID,fileTypeGroupID,",")>checked</cfif> /> #heading#
									</label>
								  </div>

								</div>
							  </div>
							</cfloop>
						  </div>
						<cfelse>
						  <div class="col-md-10 col-sm-10 col-xs-9 filterMediaGroupField">
							<cfloop query="qry_get_filterFileTypeGroups">
							  <div class="filterOption">
								<div class="checkbox">
								  <label for="#filetypegroupID#">
									<input type="checkbox" id="#filetypegroupID#" name="fileTypeGroupID" value="#filetypegroupID#" <cfif listfind(form.fileTypeGroupID,fileTypeGroupID,",")>checked</cfif> /> #heading#
								  </label>
								</div>
							  </div>
							</cfloop>
						  </div>
						</cfif>
					</div>
				  </cfif>
				</cfif> <!--- / MEDIA GROUPS --->
				<cfif filterFlagGroups neq "">
				  <cfloop list="#filterFlagGroups#" index="fgID">
					  <cfif listFind(advancedFilterFlagGroups,fgID) eq 0>
					  		#application.com.mediaSearch.getFilterViewById(filtersDisplayAs, fgID, selectedFlagTextIDs, selectedLanguages, selectedExtensions)#
					  </cfif>
				  </cfloop>
				</cfif>
				<!--- ==================================================
					  ADVANCED SEARCH
				================================================== --->
				<cfif  advancedFilterFlagGroups neq "">
				  <div class="advancedSearch">
					<div id="filter-panel" class="collapse #form.advOnOff# filter-panel">
					 <div class="">
					  <cfloop list="#advancedFilterFlagGroups#" index="fgID">
						  #application.com.mediaSearch.getFilterViewById(filtersDisplayAs, fgID, selectedFlagTextIDs, selectedLanguages, selectedExtensions)#
					  </cfloop>
					</div>
				  </div>
				  </div>
				</cfif> <!--- / ADVANCED SEARCH --->

				<cfif isdefined("frmpersonid")>
				  <cfinput type="hidden" name="frmpersonid" value="#frmpersonid#">
				</cfif>

				<!--- SEARCH BUTTONS --->
				<div class="searchbuttons form-group">
				  <cfif searchButtonImage neq "">
					<cfinput name="frmSubmitButton" type="Image" src="#searchButtonImage#" id="submitButton">
				  <cfelse>
					<cf_translate>
					  <cfinput name="frmSubmitButton" type="submit" value=" phr_mla_Go " id="submitButton" class="btn btn-primary">
					  <button type="button" class="btn btn-secondary" id="frmClearButton">
						phr_mla_clear
					  </button>
					</cf_translate>
				  </cfif>
				  <cfif advancedFilterFlagGroups neq "">
					<button type="button" id="advButton" class="btn btn-secondary" data-toggle="collapse" data-target="##filter-panel">
					  <span class="glyphicon glyphicon-cog"></span> phr_mla_advanced_search
					</button>
					<input type="hidden" class="advOnOff" name="advOnOff" value="#form.advOnOff#">
				  </cfif>
				</div> <!--- / SEARCH BUTTONS --->
			  </cfform> <!--- / MAIN FORM ELEMENT --->
			</cfoutput>
		  </div><!---  / WRAPPER COLUMN --->
		</div> <!--- / WRAPPER ROW--->
	  </div> <!--- / Container --->
	</div> <!--- / SEARCH BOX --->
</cfif>
<div class="searchResults" id="searchResults">

	<div id="search-results" class=" list-group">
			<cfset selectedFlagTextIDs = listAppend(selectedFlagTextIDs,mustHaveFlagTextID)>
			<cfset showFileArgs['searchTextString'] = frmCriteria>
			<cfset showFileArgs['pageSize'] = pageSize>
			<cfset showFileArgs['fileTypeGroupID'] = form.fileTypeGroupID>
			<cfset showFileArgs['flagTextIDs'] = selectedFlagTextIDs>
			<cfset showFileArgs['languages'] = selectedLanguages>
			<cfset showFileArgs['fileExtensions'] = selectedExtensions>
			<cfset showFileArgs['DataShowList'] = DataShowList>
			<cfset showFileArgs['DataShowGrid'] = DataShowGrid>
			<cfset showFileArgs['showThumbnailAsMainImage'] = showThumbnailAsMainImage>
			<cfset showFileArgs['showMediaGroupAsMainImage'] = showMediaGroupAsMainImage>
			<cfset showFileArgs['showResourceTypeAsMainImage'] = showResourceTypeAsMainImage>
			<cfset showFileArgs['GridColumns'] = GridColumns>
			<cfset showFileArgs['dateformatmask'] = dateformatmask>
			<cfset showFileArgs['pathForMediaGroup'] = pathForMediaGroup>
			<cfset showFileArgs['pathForFileType'] = pathForFileType>
			<cfset showFileArgs['PagingType'] = PagingType>
			<cfset showFileArgs['pathForFileType'] = pathForFileType>
			<cfset showFileArgs['scrollFor'] = scrollFor>
			<cfset showFileArgs['defaultView'] = defaultView>
			<cfset showFileArgs['sortOrder'] = sortOrder>
			<cfset showFileArgs['checkExpires'] = 1>
			<cfset showFileArgs['disableListView'] = disableListView>
			<Cfset showFileURL = application.com.mediaSearch.getShowMoreMediaFilesUrl(argumentCollection=showFileArgs)>

	</div>
</div>
		<cfoutput>
			<script>
				jQuery(function(jQuery) {
					jQuery(document).ready(function(){
						jQuery( "input" ).checkboxradio();
						jQuery('##search-results').spinner();
						jQuery('##search-results').load('#showFileURL#', function(){
							<cfif PagingType neq "Normal">
								initialiseMLAJscroll();
							</cfif>
							jQuery(this).removeSpinner();
							loadGridColSelector();

							var jQueryresultsContainer = jQuery('##searchResults');
							<cfif defaultView neq "Grid">
								setListDisplay(jQueryresultsContainer);
							<cfelse>
								setGridDisplay(jQueryresultsContainer);
							</cfif>
						jQuery('##resultsTitle').stick_in_parent();
						});
					});
				});

				function loadMLAPage(showFileURL){
					jQuery('##search-results').load(encodeURI(showFileURL), function(){
						<cfif PagingType neq "Normal">
							initialiseMLAJscroll();
						</cfif>
						setCurrentDisplay();
						loadGridColSelector();
						jQuery('##resultsTitle').stick_in_parent();
						scrollToTop();
					});
				}

				<cfif PagingType neq "Normal">
					function initialiseMLAJscroll() {
						jQuery('##search-results').removeData();
						jQuery('##search-results').jscroll({
							loadingHtml: '<img src="/images/icons/loading.gif" alt="Loading"/>',
							autoTriggerUntil: #scrollFor#,
							nextSelector:'##showMoreMLA'
						});
					}
				</cfif>
			</script>
		</cfoutput>