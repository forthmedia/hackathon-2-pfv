<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			businessPlan.cfm
Author:				RMC
Date started:		2014/08/04

Purpose:			Display Business Plan entities in a tabbed layout within the portal and internal applications if called on a screen.
					Used entityListAndEditScreen.cfm as basis of this Relay tag development.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-11-21	RPW	CORE-913 Business Planning Screen on Internal is Very badly styled
2015-03-06 			ACPK 		FIFTEEN-179 Reduced width of addNewButton div to reposition Add New button
2016-02-02	WAB		Changes to make sure that twoSelectsCombos are displayed when needed
					Changed nullValue of entityType to -1 (and added support for any nullvalue in rwFormValidation.js)
2016-03-09	WAB		Make sure that Editor uses a single layout and sticks to it.  DivLayout defaults to Stacked but can be Horizontal 
2016-10-11  YB      Case 451783 - Added new phrase to translate the addNew button

Possible enhancements:
 --->

<!--- 2014-11-21	RPW	CORE-913 Added js files to deal with tabs on internal --->
<cfif request.relaycurrentuser.isInternal>
	<cf_includeCSSOnce template ="/code/styles/brand.css">
	<cf_includeCSSOnce template ="/javascript/bootstrap/css/bootstrap.min.css">
	<cf_includeJavascriptOnce template= "/javascript/bootstrap/js/bootstrap.js">
	<cf_includeJavascriptOnce template="/javascript/bordersGeneral.js"/>
	<cf_includeJavascriptOnce template= "/javascript/lib/jquery/jquery.cookie.js">
</cfif>

<cf_param label="Entity Type" name="entityType" required="true" default="8" nullValue="-1" validvalues="select entitytypeID as datavalue, description as displayvalue from (select s.description, s.entitytypeID from schemaTable s where s.entityname in ('Person','Location','Organisation','Country') union select s.description, s.entitytypeID from schemaTable s join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME JOIN (SELECT fk.name, OBJECT_NAME(fk.parent_object_id) 'Parent table', c1.name 'Parent column', OBJECT_NAME(fk.referenced_object_id) 'Referencedtable', c2.name 'Referenced column' FROM sys.foreign_keys fk INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id INNER JOIN sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id INNER JOIN	sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK ON s.entityName = FK.[Parent table] where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null and s.customEntity = 1) as entities" />

<cf_param label="Listing: Show these columns" name="showTheseColumns" required="true" default="" bindOnLoad="true" relayFormElementType="select" width="300" multiple = "true" displayas="twoselects" allowSort="true"  display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>
<cf_param label="Listing: Date format these columns" name="dateFormat" default="" bindOnLoad="true" relayFormElementType="select" width="300" multiple = "true" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType},forDateFormat=true)"/>
<cf_param label="Listing: Show these tabs" name="showTheseTabs" required="true" nullText="phr_Ext_SelectAValue" showNull="true" bindOnLoad="true" default="" relayFormElementType="select" multiple = "true" displayas="twoselects" allowSort="true" width="300" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityScreens(entityTypeID={entityType})"/>

<cf_param label="Listing: Choose the click column" name="clickColumn" required="true" nullText="phr_Ext_SelectAValue" showNull="true" default="" bindOnLoad="true" relayFormElementType="select" width="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>
<cf_param label="Listing: Group by this column" name="GroupByColumns" required="false" nullText="phr_Ext_SelectAValue" showNull="true" bindOnLoad="true" default="" relayFormElementType="select" width="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>

<cf_param label="Editor: Screen" name="frmCurrentScreenTextID" nullText="phr_Ext_SelectAValue" showNull="true" required="true" default="" bindOnLoad="true" relayFormElementType="select" width="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityScreens(entityTypeID={entityType})"/>
<cf_param label="Editor: Post save workflow" name="processID" nullText="phr_Ext_SelectAValue" showNull="true" required="false" default="" displayAs="select" validValues="select processID as value, processDescription as display from processheader" />

<cf_param label="Editor: Form Layout" name="divLayout" default="Stacked" validValues="Stacked,Horizontal"/> 

<cfparam name="returnToEID" default="">
<cfparam name="returnToScreen" default="">

<!--- Get details of the screens for the tab headings --->
<cfquery name="getScreenData" datasource="#application.SiteDataSource#">
	SELECT 	*
	FROM 	Screens
	WHERE 	screentextid IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#showTheseTabs#" list="true">)
	AND 	entityTypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#entityType#">
</cfquery>

<!--- Business plans must have a summary screen --->
<cfif not ListFindNoCase(showTheseTabs,'BusinessPlanSummary',',')>
	<!--- Check that the summary screen exists --->
	<cfquery name="checkSummaryScreen" dbtype="query">
		SELECT ScreenID FROM getScreenData WHERE screenTextID = 'BusinessPlanSummary'
	</cfquery>
	<!--- Add it to the list of tabs to display --->
	<cfif checkSummaryScreen.recordcount>
		<cfset showTheseTabs = listprepend(showTheseTabs,"BusinessPlanSummary")>
	</cfif>
</cfif>

<!--- Function to return the name of a given screen --->
<cffunction name="fnctGetScreenName" returntype="string" access="public">
	<!--- One required argument --->
	<cfargument name="screenTextId" type="any" required="true">

  	<!--- Get screen name for this tab --->
  	<cfquery name="getScreenName" dbtype="query">
		SELECT	ScreenName
		FROM 	getScreenData
		WHERE	ScreenTextID = '#arguments.screenTextId#'
	</cfquery>

	<cfreturn getScreenName.ScreenName />
</cffunction>

<cfif structKeyExists(url,"eid")>
	<cfparam name="listingEID" default="#url.eid#">
<cfelse>
	<cfparam name="listingEID" default="">
</cfif>

<cfquery name="schemaTableDetails" datasource="#application.SiteDataSource#">
	SELECT * FROM schematable WHERE entitytypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#entityType#">
</cfquery>

<cfquery name="customEntityKeys" datasource="#application.SiteDataSource#">
	SELECT
		OBJECT_NAME(fk.parent_object_id) 'customEntity',
		c1.name 'customEntityUniqueKey',
		OBJECT_NAME(fk.referenced_object_id) 'relatedTable',
		c2.name 'relatedTableUniqueKey'
	FROM
		sys.foreign_keys fk
	INNER JOIN
		sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
	INNER JOIN
		sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
	INNER JOIN
		sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
	WHERE OBJECT_NAME(fk.parent_object_id) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#schemaTableDetails.entityName#">
</cfquery>

<!--- A new record has just been added or an existing record has been saved/submitted --->
<cfif structKeyExists(form,"saveBtn") or structkeyexists(form,"submitBtn")>
	<cfif structKeyExists(form,"saveBtn")>
		<cfset returnMessage = "phr_entityTypeScreen_#schemaTableDetails.entityname#_Saved">
	<cfelseif structkeyexists(form,"submitBtn")>
		<cfset returnMessage = "phr_entityTypeScreen_#schemaTableDetails.entityname#_Submitted">
	</cfif>

	<cfset keyCheck = evaluate('#schemaTableDetails.entityname#ID')>
	<cfif (isdefined("keyCheck") AND not isnumeric(keyCheck))>
		<cfquery name="insertEntity" datasource="#application.SiteDataSource#">
			INSERT INTO
			  #schemaTableDetails.entityname#
			(
				#customEntityKeys.relatedTableUniqueKey#,
				CreatedByPerson,
				Created,
				LastUpdatedByPerson,
				LastUpdated,
				createdby,
				lastupdatedby
			)
			VALUES
			(
			 	<cfqueryparam cfsqltype="cf_sql_integer" value="#frmcurrentEntityID#">,
				<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,
				GETDATE(),
				<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,
				GETDATE(),
				<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="cf_sql_integer" >)
			SELECT SCOPE_IDENTITY() as newofID
		</cfquery>

		<cfset form[schemaTableDetails.uniqueKey] = insertEntity.newofID>
		<cfset form.new = form[schemaTableDetails.uniqueKey]>


	</cfif>

	<cf_updatedata>

	<cfset application.com.relayUI.setMessage(message=returnMessage)>

	<!--- Process "Submit" form action  --->
	<cfif structkeyexists(form,"submitBtn")>
		<!--- Plug into approvals engine --->
		<cfif application.com.flag.doesFlagExist('businessPlanSubmitted')>
			<cfset application.com.flag.setFlagData(flagId='businessPlanSubmitted',entityID=form[schemaTableDetails.uniqueKey])>
		</cfif>
		<cfset result.approvalDetails = application.com.approvalEngine.approvalRuleHandler(mode='request', entityTypeID=entityType, approvalStatusRelatedEntityID=form[schemaTableDetails.uniqueKey]) />

		<cfset businessPlanData=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.businessPlan, entityID=form[schemaTableDetails.uniqueKey], fieldList="PlanTitle")>
		<cfset mergeStruct={}>

		<cfif businessPlanData.isOk>
			<cfset mergeStruct.businessplanname=businessPlanData.recordSet.data>
		</cfif>

		<cfset application.com.email.sendEmail(emailTextID='BusinessPlanSubmittedEmail', personID=request.relayCurrentUser.personID, entityID=form[schemaTableDetails.uniqueKey], mergeStruct=mergeStruct)>
	</cfif>

	<cfif request.relaycurrentuser.isInternal gt 0>
		<!--- Redirect back to listing with final message --->
		<cfset url["#customEntityKeys.RELATEDTABLEUNIQUEKEY#"] = form.frmCurrentEntityID>
		<!--- <cfset url.message = "#returnMessage#"> --->
		<cfset url.uniqueKey = form.frmCurrentEntityID>
		<cfset structDelete(url,"editor")>

		<!--- After visiting an sub custom entity and trying to return back to the POL custom entity listing we don't know who we were looking at anymore so get this from LASTLISTINGSCREEN --->
		<cfif returnToScreen neq "" and (structKeyExists(form,"returnScreenLoaded") and form.returnScreenLoaded)>
			<cfset relocateParams= listFirst(url.LASTLISTINGSCREEN,'-')>
			<cfset relocateUrl="#request.currentsite.PROTOCOLANDDOMAIN#/data/businessPlan.cfm?frmEntityScreen=#listGetAt(relocateParams,1,"|")#&frmcurrentEntityID=#listGetAt(relocateParams,2,"|")#&frmentitytypeid=#listGetAt(relocateParams,3,"|")#">
			<cfoutput><script>{window.location = "#relocateUrl#";}</script></cfoutput>
		</cfif>

		<!--- if returnToScreen is defined we've got a listing on an editor so turn the editor back on and set that we've loaded the return screen in preperation for our final save. --->
		<cfif returnToScreen neq "" and returnToScreen neq frmcurrentscreentextID>
			<cfset frmcurrentscreentextID = returnToScreen>
			<cfset url.editor = "yes">
			<cfif not (structKeyExists(form,"returnScreenLoaded") and form.returnScreenLoaded)>
				<cfset form.returnScreenLoaded = true>
			</cfif>
		</cfif>
	<cfelse>
		<cfif isDefined("processID") and isNumeric(processID) and processID neq 0>
			<!--- Redirect to a process --->
			<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/proc.cfm?prid=#processID#&entityID=#form[schemaTableDetails.uniqueKey]#&eid=#url.eid#")>
		<cfelse>
			<!--- Redirect back to listing with final message --->
			<cfif returnToEID eq "">
				<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#url.eid#&uniqueKey=#form.frmCurrentEntityID#&frmcurrententityID=#form.frmCurrentEntityID#")>
			<cfelse>
				<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#returnToEID#&editor=yes&uniqueKey=#form.frmCurrentEntityID#&countryID=#form.countryID#&frmcurrententityID=#form.frmCurrentEntityID#")>
			</cfif>
		</cfif>
		<cfoutput><script type="text/javascript">{window.location = "#returnURL#";}</script></cfoutput>
	</cfif>
</cfif>

<!--- editor --->
<cfif structKeyExists(url,"editor") and url.editor eq "yes">
		<cfset url.editor = "false">
		<cfset structdelete(form,"SUBMITBTN")>

		<!--- Determine submission status for enabling/disabling submit button --->
		<cfset disableSubmitButton = false>
		<cfif structkeyexists(url,'uniqueKey') and isnumeric(url.uniquekey)>
			<!--- <cfset flagStruct = application.com.flag.getFlagStructure(flagID='businessPlanSubmitted')> --->
			<cfset isSubmitted = application.com.flag.getFlagData(flagID='businessPlanSubmitted',entityId=url.uniqueKey)>
			<!--- Retrieve approval status --->
			<cfif isSubmitted.recordcount>
				<!--- Business plan submitted, so cannot resubmit --->
				<cfset disableSubmitButton = true>
			</cfif>
		</cfif>

		<cfform method="post" name="#schemaTableDetails.entityname#" enctype="multipart/form-data">
		<cfoutput>
			<CF_relayFormDisplay class="#divLayout is 'horizontal'?'form-horizontal':''#">
				<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
				<cfif request.relaycurrentuser.isInternal>
					<tr><td>
				</cfif>
				<CF_ascreen formname="#schemaTableDetails.entityname#">
					<ul class="nav nav-tabs" role="tablist" id="businessPlanTabs">
					  	<cfloop from="1" to="#listlen(showTheseTabs,',')#" index="tab">
						  	<cfset tabClass = "">
						  	<cfset thisTab = listgetat(showTheseTabs,tab,",")>
							<cfset screenName = fnctGetScreenName(thisTab)>
							<!--- Get the approval status of this section and update the tab class accordingly --->
							<cfset thisName = replacenocase(thisTab,"BusinessPlan","")& "Approval">
							<cfquery name="getFlagGroupId" datasource="#application.SiteDataSource#">
								SELECT flagGroupID FROM flagGroup WHERE flagGroupTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#thisName#">
							</cfquery>
							<cfif getFlagGroupId.recordcount and isnumeric(url.uniqueKey)>
								<cfset approvalStatus = application.com.flag.getFlagGroupData(entityID=url.uniqueKey, flagGroupID=getFlagGroupId.flagGroupId)>
								<cfif approvalStatus.recordcount>
									<cfset tabClass = "businessPlan" & replacenocase(approvalStatus.flagName," ","","all")>
								</cfif>
							</cfif>
							<li id="businessPlanTabHeader#tab#"<cfif tab is 1> class="active"</cfif>><a href="##businessPlanTab#tab#" role="tab" data-toggle="tab" class="#tabClass#">#screenName#</a></li>
						</cfloop>
					</ul>
					<!--- Container div for tabs --->
					<div class="tab-content">
					<!--- Start: For each tab that is to be displayed... --->
					<cfloop list="#showTheseTabs#" index="listItem" delimiters=",">

						<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
						<cfif request.relaycurrentuser.isInternal>
							<cfparam name="tabNum" type="numeric" default="1">
							<div class="tab-pane<cfif tabNum is 1> active</cfif>" id="businessPlanTab#tabNum#">
							<table class="withBorder"  width="100%" border="0"  cellspacing="1" cellpadding="3" align="center">
						</cfif>


						<cfset params["#schemaTableDetails.uniqueKey#"] = uniqueKey>
						<cfset params["#schemaTableDetails.uniqueKey#list"] = uniqueKey>
						<cfset params["countryID"] = countryID>
						<cfset params["screenid"] = listItem>
						<cfset params.divLayout = divLayout>

						<!--- 2013-11-06	YMA	Get recursively get entityIDs for all related entities ---->
						<cfset entityDetails = application.com.relayPLO.getEntityStructure(entityTypeID =schemaTableDetails.ENTITYTYPEID, entityID = iif(isnumeric(uniqueKey),uniqueKey,0),getEntityQueries=true)>

						<cfset childEntityID = uniqueKey>
						<cfif isNumeric(childEntityID)>
							<cfset childentity = entityDetails.queries[schemaTableDetails.entityname]>

							<cfquery dbtype="query" name="childEntityID">
								SELECT 	#application.entityType[application.customentities[schemaTableDetails.entityname].REFERENCEDENTITYTYPEID].uniqueKey# AS childEntityID
								FROM 	childentity
							</cfquery>

							<cfset childEntityID = childEntityID.childEntityID>
						</cfif>

						<cfif not structKeyExists(entitydetails.id,"Person") or not structKeyExists(entitydetails.id,"Location") or not structKeyExists(entitydetails.id,"Organisation")>
							<cfset childEntityList = application.com.relayentity.recursiveListRelatedTables(tablename=schemaTableDetails.entityname,filterOnlyScreensAndFlags=true)>
							<cfset lastEntityTable = schemaTableDetails.entityname>
							<cfset lastEntityID = url.frmcurrententityID>
							<cfset entitydetails2 = application.com.relayPLO.getEntityStructure(entityTypeID = application.entitytypeID[lastEntityTable], entityID = iif(isnumeric(lastEntityID),lastEntityID,0),getEntityQueries=true)>
							<cfloop list="#childEntityList#" index="i">
								<cfset childentity = entitydetails2.queries[lastEntityTable]>

								<cfquery dbtype="query" name="childEntityID">
									SELECT 	#application.entityType[application.customentities[lastEntityTable].REFERENCEDENTITYTYPEID].uniqueKey# AS childEntityID
									FROM 	childentity
								</cfquery>

								<cfset childEntityID = iif(isnumeric(childEntityID.childEntityID),childEntityID.childEntityID,lastEntityID)>

								<cfset entityDetails2 = application.com.relayPLO.getEntityStructure(entityTypeID = application.entitytypeID[i], entityID = iif(isnumeric(childEntityID),childEntityID,0),getEntityQueries=true)>

								<cfloop collection="#entitydetails2.queries#" item="i">
									<cfset entitydetails.queries[i] = entitydetails2.queries[i]>
									<cfset lastEntityTable = i>
								</cfloop>
								<cfloop collection="#entitydetails2.id#" item="i">
									<cfset entitydetails.id[i] = entitydetails2.id[i]>
									<cfset lastEntityTypeID = i>
									<cfset lastEntityID = entitydetails2.id[i]>
								</cfloop>

								<cfif structKeyExists(entitydetails.id,"Person") or structKeyExists(entitydetails.id,"Location") or structKeyExists(entitydetails.id,"Organisation")>
									<cfbreak>
								</cfif>
							</cfloop>
						</cfif>

						<cfloop collection="#entityDetails.id#" item="i">
							<cfif not isNumeric(i)>
								<cfset fieldvalue = entityDetails.id[i]>
								<cfset tablequery = application.com.structureFunctions.queryRowToStruct(entityDetails.queries[i],1)>
								<cfif not isNumeric(fieldvalue)>
									<cfset fieldvalue = "new">
									<cfset tablequery["#application.entitytype[application.entitytypeID[i]].uniqueKey#"] = fieldvalue>
								</cfif>
								<cfset params["#i#"] = tablequery>
								<cfset params["#application.entitytype[application.entitytypeID[i]].uniqueKey#"] = fieldvalue>
							</cfif>
						</cfloop>

						<cfif structKeyExists(url,"#customEntityKeys.RELATEDTABLEUNIQUEKEY#")>
							<cfset params["#customEntityKeys.RELATEDTABLEUNIQUEKEY#"] = evaluate("url.#customEntityKeys.RELATEDTABLEUNIQUEKEY#")>
							<cfset structdelete(params,"#schemaTableDetails.uniqueKey#")>
							<cfset structdelete(params,"#schemaTableDetails.uniqueKey#list")>
						</cfif>
						<cfif structKeyExists(url,"FIELDTEXTID")>
							<cfset params["FIELDTEXTID"] = url.FIELDTEXTID>
						</cfif>

						<!--- Output the screen in a "tabbed" div --->
						<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
						<cfparam name="tabNum" type="numeric" default="1">
							<cfif NOT request.relaycurrentuser.isInternal>
							<div class="tab-pane<cfif tabNum is 1> active</cfif>" id="businessPlanTab#tabNum#">
							</cfif>
							<cfset params['method'] = "edit">

							<cfif isNumeric(uniqueKey) and ((application.com.flag.doesFlagExist('businessPlanApproved') and application.com.flag.getFlagData(flagID='businessPlanApproved', entityID=#uniqueKey#).recordcount gt 0)
									or
									(application.com.flag.doesFlagExist('businessPlanRejected') and application.com.flag.getFlagData(flagID='businessPlanRejected', entityID=#uniqueKey#).recordcount gt 0)
									)>
								<cfset params['method'] = "view">
							</cfif>


							<CF_ascreenitem attributecollection="#params#">

							<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="left">

							<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
							<cfif request.relaycurrentuser.isInternal><p style="line-height:3.0em;"></cfif>
							<cfif tabNum gt 1>
								<!--- Display "Previous" link --->
								<a class="btn btn-primary btnPrevious">&laquo; Previous</a>&nbsp;
							</cfif>

							<cfif tabNum lt listlen(showTheseTabs,",")>
								<!--- Display "Next" link --->
								<a class="btn btn-primary btnNext">Next &raquo;</a>
							</cfif>
							<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
							<cfif request.relaycurrentuser.isInternal></p></cfif>
							</cf_relayFormElementDisplay>
							<!--- "Save" & "Submit" buttons --->
							<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="left">
								<!--- 2013-09-19	YMA	Case 437096 need CF_TRANSLATE to get the phrase to show when its not already translated --->
								<cf_translate>
									<cf_relayFormElement class="saveBtnBusinessPlan" relayFormElementType="submit" fieldname="saveBtn" currentValue="phr_entityTypeScreen_#schemaTableDetails.entityName#_SaveBtn" label="" onClick="JavaScript:verifyForm();"  disabled="#iif(params['method'] eq 'view',DE('true'),DE('false'))#">
									<!--- "Submit" button not displayed to approvers, and only on the "Summary" screen --->
									<cfif listItem is 'BusinessPlanSummary'>
										<cf_relayFormElement class="" relayFormElementType="submit" fieldname="submitBtn" currentValue="phr_entityTypeScreen_#schemaTableDetails.entityName#_Save" disabled="#iif(disableSubmitButton,DE('true'),DE('false'))#" label="" onClick="JavaScript:verifyForm();">
									</cfif>
									<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
									<cfif request.relaycurrentuser.isInternal>
										<cf_relayFormElement relayFormElementType="button" fieldname="cancelBtn" currentValue="phr_Cancel" label="" onClick="history.back(-1);">
									</cfif>
								</cf_translate>
								<cfif listItem is 'BusinessPlanSummary'>
									<cf_relayFormElement relayFormElementType="hidden" fieldname="frmEntityType" currentValue="#customEntityKeys.relatedtable#">
									<cf_relayFormElement relayFormElementType="hidden" fieldname="frmCurrentEntityID" currentValue="#url.frmcurrententityID#">
									<cf_relayFormElement relayFormElementType="hidden" fieldname="#schemaTableDetails.uniqueKey#" currentValue="#uniqueKey#">
									<cf_relayFormElement relayFormElementType="hidden" fieldname="countryID" currentValue="#countryID#">
									<cfif request.relaycurrentuser.isInternal gt 0>
										<cfif structKeyExists(form,"returnScreenLoaded")>
											<cf_relayFormElement relayFormElementType="hidden" fieldname="returnScreenLoaded" currentValue="#form.returnScreenLoaded#">
											<cfset returnToScreen="">
										</cfif>
										<cf_relayFormElement relayFormElementType="hidden" fieldname="clickColumn" currentValue="#clickColumn#">
										<cf_relayFormElement relayFormElementType="hidden" fieldname="showTheseColumns" currentValue="#showTheseColumns#">
										<cf_relayFormElement relayFormElementType="hidden" fieldname="dateFormat" currentValue="#dateFormat#">
										<cf_relayFormElement relayFormElementType="hidden" fieldname="GroupByColumns" currentValue="#GroupByColumns#">
										<cf_relayFormElement relayFormElementType="hidden" fieldname="showTheseTabs" currentValue="#showTheseTabs#">
									</cfif>
								</cfif>
							</cf_relayFormElementDisplay>

							<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
							<cfif NOT request.relaycurrentuser.isInternal>
							</div>
							</cfif>
						<cfset tabNum = incrementvalue(tabNum)>
						<!--- End: for each tab --->
						<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
						<cfif request.relaycurrentuser.isInternal>
							</table>
							</div>
						</cfif>
					</cfloop>
					</div>


				</CF_ascreen>
				<!--- 2014-11-21	RPW	CORE-913 Added formatting to deal with tabs on internal --->
				<cfif request.relaycurrentuser.isInternal>
					</td></tr>
				</cfif>
			</CF_relayFormDisplay>
		</cfoutput>
		</cfform>

<!--- List --->
<cfelse>
	<cfset flagJoinInfo = structNew()>
	<cfset tableColumns = "">
	<cfset flagColumns = "">
	<cfset uniqueKeyList = "uniqueKey">
	<cfset clickList="">
	<cfset random = randrange(100000,0)>

	<cfif request.relaycurrentuser.isinternal gt 0>
		<cfoutput>
			<!--- 2014-11-21	RPW	CORE-913 Fixed function to remove row on delete --->
			<script type="text/javascript">
			deleteRow#random# = function(uniqueKey){
			if (confirm("phr_areYouSureYouWantToDelete #schemaTableDetails.uniqueKey# "+uniqueKey+"?") == true) {
				var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=relayScreens&methodName=deleteEntityRow&returnformat=json&_cf_nodebug=true'

				var myAjax = new Ajax.Request(
					page,
					{
						method: 'post',
						asynchronous: false,
						parameters: {entityID:uniqueKey,entityName:'#schemaTableDetails.entityName#',uniqueKey:'#schemaTableDetails.uniqueKey#',entityTypeID:'#entityType#'},
						onComplete: function (requestObject,JSON) {
							json = requestObject.responseText.evalJSON(true)
							if(!json.ISOK) {
								alert(json.MESSAGE);
							} else {
								trID = 'uniqueKey='+uniqueKey;
								$(trID).remove();
							}
			           }
			       });
				}
			}
			</script>
		</cfoutput>
	</cfif>

	<!--- When on internal partner dashboard get current entity relationship to Organisation, Country and Location to submit to editor form. --->
	<cfif structKeyExists(url,"uniqueKey") and structKeyExists(url,"countryID")>
		<cfset currentcountryID = url.countryID>
		<cfset Evaluate("#customEntityKeys.relatedTableUniqueKey# = url.uniquekey")>
	<cfelse>

		<cfif structKeyExists(form,"FRMENTITYTYPE") and structKeyExists(URL,"FRMCURRENTENTITYID") and listFindNoCase("organisation,person,location",form.FRMENTITYTYPE )>

			<cfset entityStructure = application.com.relayplo.getEntityStructure(entityTypeID=url.FRMENTITYTYPEID,entityID=url.FRMCURRENTENTITYID)>

			<cfset Evaluate("#customEntityKeys.relatedTableUniqueKey# = entityStructure.id.#customEntityKeys.relatedTable#") >

			<cfset currentcountryID = entityStructure.countryID>

			<cfquery name="getEntityTypeID"  datasource="#application.SiteDataSource#">
				SELECT entityTypeID FROM schematable WHERE entityname = <cfqueryparam cfsqltype="cf_sql_varchar" value="#customEntityKeys.relatedTable#">
			</cfquery>
			<cfset url.FRMENTITYTYPEID = getEntityTypeID.entityTypeID>
		<cfelse>
			<cfif structKeyExists(request.relaycurrentuser,"#customEntityKeys.relatedTableUniqueKey#")>
				<cfset Evaluate("#customEntityKeys.relatedTableUniqueKey# = request.relaycurrentuser.#customEntityKeys.relatedTableUniqueKey#") >
			<cfelse>
				<cfset Evaluate("#customEntityKeys.relatedTableUniqueKey# = url.uniquekey")>
			</cfif>
		</cfif>
	</cfif>

	<!--- prepare list columns --->
	<cfloop list="#showTheseColumns#" index="i">
		<cfif listLast(i,'.') eq "lastUpdatedBy"><cfset showTheseColumns = REReplace(showTheseColumns,i,"LastUpdatedByName")></cfif>
		<cfif listLast(i,'.') eq "createdBy"><cfset showTheseColumns = REReplace(showTheseColumns,i,"createdByName")></cfif>
		<cfif CompareNoCase("#left(i,5)#","flag_") eq 0>
			<cfset structInsert(flagJoinInfo,i,application.com.flag.getJoinInfoForFlag(flagID=right(i,len(i)-len(left(i,5))),entityTableAlias="#schemaTableDetails.entityName#"))>
			<cfset flagColumns= ListAppend(flagColumns, right(i,len(i)-len(left(i,5))))>
		<cfelse>
			<cfset tableColumns = ListAppend(tableColumns, listLast(i,'.'))>
			<cfset showTheseColumns = REReplace(showTheseColumns,i,listLast(i,'.'))>
			<cfset dateFormat = REReplace(dateFormat,i,listLast(i,'.'))>
			<cfset GroupByColumns = REReplace(GroupByColumns,i,listLast(i,'.'))>
			<cfset clickColumn = REReplace(clickColumn,i,listLast(i,'.'))>
		</cfif>
	</cfloop>

	<cfset tableColumns = replacenocase(tableColumns,"UpdatedByName","UpdatedBy","one")>
	<cfset tableColumns = replacenocase(tableColumns,"createdByName","createdBy","one")>
	<cfset listPos = listfind(tableColumns,"del",",")>
	<cfif listPos>
		<cfset tableColumns = listdeleteat(tableColumns,listPos,",")>
	</cfif>

	<!--- prepare list query
		NJH 2014/07/18 - removed the #schemaTableDetails.entityName#.* from the query and replaced with a looping over the table column names, as a TFQO doesn't seem to find columns when there is a *
	--->

	<cfsavecontent variable="query">
		<cfoutput>
			SELECT #schemaTableDetails.uniqueKey# as uniqueKey,
				<cfloop list="#tableColumns#" index="columnName">#schemaTableDetails.entityName#.#columnName#,</cfloop>
				lastUpdatedBy.name as lastUpdatedByName,
				createdBy.name as createdByName

				<cfloop collection="#flagJoinInfo#" item="i">
					,#flagJoinInfo[i].selectField# as [#flagJoinInfo[i].alias#]
				</cfloop>
			FROM #schemaTableDetails.entityName#
				<cfloop collection="#flagJoinInfo#" item="i">
					#flagJoinInfo[i].join#
				</cfloop>
			LEFT join vEntityName lastUpdatedBy on #schemaTableDetails.entityName#.lastupdatedbyperson = lastUpdatedBy.entityID AND lastUpdatedBy.entityTypeID=#application.entityTypeID.person#
			join vEntityName createdBy on #schemaTableDetails.entityName#.createdbyperson = createdBy.entityID AND createdBy.entityTypeID=#application.entityTypeID.person#
			WHERE #schemaTableDetails.entityName#.#customEntityKeys.relatedTableUniqueKey# = <cfif isNumeric("#evaluate(customEntityKeys.relatedTableUniqueKey)#")>#evaluate(customEntityKeys.relatedTableUniqueKey)#<cfelse>0</cfif>
		</cfoutput>
	</cfsavecontent>

	<cfquery name="getEntityData" datasource="#application.SiteDataSource#">
		#preservesinglequotes(query)#
	</cfquery>

	<!--- if internal add the delete column --->
	<cfif request.relaycurrentuser.isinternal gt 0>
		<cfquery name="getEntityData" dbtype="query">
			SELECT *, 'phr_delete' as del
			FROM getEntityData
		</cfquery>
	</cfif>

	<!--- Figure out which columns to booleanformat from the flag type and table column data types --->
	<cfquery name="booleanFormatColumns"  datasource="#application.SiteDataSource#">
		SELECT 	column_name as col from INFORMATION_SCHEMA.COLUMNS
		WHERE 	data_type = 'bit'
		AND	 	table_Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#schemaTableDetails.entityName#">
		AND 	column_name in (<cf_queryparam value="#tableColumns#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
		<cfif isDefined("flagColumns") and flagColumns neq "">
			UNION ALL
			SELECT 'flag_' + cast(f.flagID as varchar) as col FROM flag f
				JOIN flaggroup fg on f.flaggroupID = fg.flaggroupID
					AND f.flagID in (<cf_queryparam value="#flagColumns#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
					AND fg.flagtypeID in (<cfqueryparam cfsqltype="cf_sql_integer" value="2,3" list="true">)
		</cfif>
	</cfquery>


	<cfset urlList="">
	<cfset clickList="">
	<cfif isNumeric("#evaluate(customEntityKeys.relatedTableUniqueKey)#")>
		<cfif request.relaycurrentuser.isinternal gt 0>
			<cfif listLen(clickColumn) eq 1>
				<cfset showTheseColumns = listAppend(showTheseColumns,"del")>
				<cfset clickColumn = listAppend(clickColumn,"del")>
				<cfset uniqueKeyList = listAppend(uniqueKeyList,"uniqueKey")>
			</cfif>

			<cfset urlList = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/relaytagtemplates/businessPlan.cfm?editor=yes&frmCurrentScreenTextID=#frmCurrentScreenTextID#&frmcurrententityID=#evaluate(customEntityKeys.relatedTableUniqueKey)#&countryID=#currentcountryID#&entityType=#entityType#&returnToScreen=#returnToScreen#&clickColumn=#clickColumn#&showTheseColumns=#showTheseColumns#&showTheseTabs=#showTheseTabs#&dateFormat=#dateFormat#&GroupByColumns=#GroupByColumns#")>
			<cfif returnToScreen neq "">
				<cfset urllist = urllist & '&returnToEntityTypeID=' & url.entityType>
			</cfif>
			<cfif structKeyExists(url,"lastListingScreen")>
				<cfset urllist = urllist & '&lastListingScreen=' & listAppend(url.lastListingScreen,"#frmCurrentScreenTextID#|#evaluate(customEntityKeys.relatedTableUniqueKey)#|#schemaTableDetails.entityTypeID#","-")>
			<cfelse>
				<cfset urllist = urllist & '&lastListingScreen=' & FORM.THISSCREENTEXTID & '|' & evaluate(customEntityKeys.relatedTableUniqueKey) & '|' & URL.FRMENTITYTYPEID>
			</cfif>
			<cfset urllist = urllist & '&uniqueKey='>
			<cfset urlList = listAppend(urlList,"##")>

			<cfset clickList = " ,javascript:deleteRow#random#(##uniqueKey##)">
		<cfelse>
			<cfset urlList = "#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#listingEID#&editor=yes&add=no&frmCurrentScreenTextID=#frmCurrentScreenTextID#&frmcurrententityID=#evaluate(customEntityKeys.relatedTableUniqueKey)#&countryID=#request.relaycurrentuser.organisation.countryID#">
			<cfset urllist = urllist & '&uniqueKey='>
			<cfset clickList = " ">
		</cfif>

		<cfoutput>
		<!--- 2015-03-06 ACPK FIFTEEN-179 Reduced width of addNewButton div to reposition Add New button
			  2016-01-04 SB BF-108 - Fixed HTML which had inline styling. --->

		<div class="addNewButton">
			<a class="btn btn-primary" href="#listFirst(urlList)#new">phr_newBusinessPlan_addNew</a>  <!---YB - Case 451783 - Added new phrase to translate the addNew button --->
		</div>

		</cfoutput>

		<cf_tablefromqueryobject
			rowIdentityColumnName="uniqueKey"
			keyColumnList="#clickColumn#"
			keyColumnKeyList="#uniqueKeyList#"
			keyColumnURLList="#urlList#"
			keyColumnOnClickList="#clickList#"
			showTheseColumns="#showTheseColumns#"
			useInclude="false"
			queryObject="#getEntityData#"
			HidePageControls="true"
			dateFormat="#dateFormat#"
			numRowsPerPage="#getEntityData.recordcount#"
			columnTranslation="true"
			columnTranslationPrefix="phr_report_entityTypeScreen_"
			booleanFormat="#ValueList(booleanFormatColumns.col)#"
			GroupByColumns="#GroupByColumns#"
			noForm="true"
		>
	<cfelse>
		<cfoutput>
			<div id="saveRecordBeforeAdding">
				<p>phr_saveRecordBeforeAddingSubEntity</p>
			</div>
		</cfoutput>
	</cfif>

</cfif>