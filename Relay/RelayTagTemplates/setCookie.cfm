<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			setCookie.cfm
Author:				SWJ
Date started:		2006-01-30

Description:		The idea of this relayTag is to provide a method to set a cookie.
					You give the cookie a name and it is automatically set to the
					value of 1 when the customer visits the site.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

Add a variable to specify the persistence of the cookie

 --->

<cfif not isDefined("cookieName")>
	<p>You must provide the variable cookieName.</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif isDefined("cookieName") and isDefined("cookieValue")>
	<cfset application.com.globalFunctions.cfcookie(name="#cookieName#", value="#cookieValue#")>
<cfelseif isDefined("cookieName")>
	<cfset application.com.globalFunctions.cfcookie(name="#cookieName#", value="1")>
</cfif>