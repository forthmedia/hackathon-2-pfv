<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			entityListAndEditorScreen.cfm
Author:				YMA
Date started:		2013/07/22

Purpose:	Display Custom Entities on the portal and internal if called on a screen.
			You can use this to display custom entities that belong to a custom entity by including this tag on a screen that displays in
			the editor screen of the first custom entity to show the listing of the sub entity. You will also need a hidden EID that holds
			the editor for the sub entity.  You will need to pass in the hidden parameter: returnToEID and returnToScreen
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-08-20			YMA			Case 436441 Two select params no longer need valid values defined and can work just on Bind functions.
2013-11-20			YMA			Case 438096 make createdby and lastupdated by show the persons name
2014-08-08 			YMA 		remove generated column names which are not passed through from the tag. fixed js delete to remove table row after db delete.
2014-08-18			NJH			Removed dateFormat param. Derive that from DB. Also clean up a bit of code. Need to get rid of evaluates!!!
2014-09-30			AXA 		Fixed sorting issue.  added sortorder parameter and removed the noForm attribute from TFQO
2014-10-15			NJH 		P-NEX001 changed joins to person table to use vEntity to bring back data on deleted persons and when id=0
2014-11-10			AXA 		P-NEX001 added numRows variable to turn on pagination for large datasets.  added allowcolumnsorting flag to TFQO to fix Session Expired error.
2015-01-21			AHL			Case 443209  Add check for url.uniqueKey existing
2015/03/04			NJH			Jira Fifteen 267 - work out numberFormat columns and their corresponding format masks for financial flagGroups in particular.
2015-03-06			RPW			FIFTEEN-267 - Custom Entities - Added ability to format financial flags
2015-11-25          YMA         BF-22 Prevent error when submitting to a workflow on update of an existing record. Setting entityID to a form variable that is also set when new record added instead of add new recor query result.
2016-03-11			WAB			Set relayformDisplay classes to "" (for stacked layout)
Possible enhancements:
 --->

<cf_param label="Entity Type" name="entityType" required="true" default="0" nullValue="-1" validvalues="select entitytypeID as datavalue, description as displayvalue from (select s.description, s.entitytypeID from schemaTable s where s.entityname in ('Person','Location','Organisation','Country') union select s.description, s.entitytypeID from schemaTable s join INFORMATION_SCHEMA.COLUMNS isc on s.entityname = isc.TABLE_NAME JOIN (SELECT fk.name, OBJECT_NAME(fk.parent_object_id) 'Parent table',	c1.name 'Parent column', OBJECT_NAME(fk.referenced_object_id) 'Referencedtable', c2.name 'Referenced column' FROM sys.foreign_keys fk INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id INNER JOIN sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id INNER JOIN	sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id) as FK ON s.entityName = FK.[Parent table] where s.screensExist = 1 and s.flagsExist = 1 and s.uniqueKey is not null and s.customEntity = 1) as entities" />

<cf_param label="Listing: Show these columns" name="showTheseColumns" required="true" default="" bindOnLoad="true" relayFormElementType="select" WIDTH="300" multiple="true" displayAs="twoSelects" allowSort="true" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>
<!--- <cfparam label="Listing: Date format these columns" name="dateFormat" default="" bindOnLoad="true" relayFormElementType="select" WIDTH="300" multiple="true" displayAs="twoSelects" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType},forDateFormat=true)"/> --->
<cf_param label="Listing: Choose the click column" name="clickColumn" required="true" nullText="phr_Ext_SelectAValue" showNull="true" default="" bindOnLoad="true" relayFormElementType="select" WIDTH="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>
<cf_param label="Listing: Group by this column" name="GroupByColumns" required="false" nullText="phr_Ext_SelectAValue" showNull="true" bindOnLoad="true" default="" relayFormElementType="select" WIDTH="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityListAndEditScreenFields(entityTypeID={entityType})"/>
<cf_param label="Listing: Number of rows to display per page" name="numRowsPerPage" required="false" type="string" default="25" /> <!--- 2014-11-10 AXA P-NEX001 Added for pagination for large datasets --->
<cf_param label="Listing: Default Sort Order" name="sortOrder" type="AlphaNumericListWithSpace" required="false" default="" allowNull="true" /> <!--- 2014-09-30 AXA P-NEX001 Added sortOrder with regEx checking to prevent SQL injection --->

<cf_param label="Editor: Screen" name="frmCurrentScreenTextID" nullText="phr_Ext_SelectAValue" showNull="true" required="true" default="" bindOnLoad="true" relayFormElementType="select" WIDTH="300" displayas="select" display="displayvalue" value="datavalue" bindFunction="cfc:webservices.relayScreens.entityScreens(entityTypeID={entityType})"/>
<cf_param label="Editor: Post save workflow" name="processID" nullText="phr_Ext_SelectAValue" showNull="true" required="false" default="" displayAs="select" validValues="select processID as value, processDescription as display from processheader" />

<cfparam name="returnToEID" default="">
<cfparam name="returnToScreen" default="">
<cfparam name="addnewPhraseTextID" default="AddNew">
<CFPARAM name="divLayout" default = "stacked"> <!--- or horizontal --->



<cfif structKeyExists(url,"eid")>
	<cfparam name="listingEID" default="#url.eid#">
<cfelse>
	<cfparam name="listingEID" default="">
</cfif>

<cfset schemaTableDetails = application.com.relayEntity.getEntityType(entityTypeID=entityType)>

<cfquery name="customEntityKeys" datasource="#application.SiteDataSource#">
	SELECT
		OBJECT_NAME(fk.parent_object_id) 'customEntity',
		c1.name 'customEntityUniqueKey',
		OBJECT_NAME(fk.referenced_object_id) 'relatedTable',
		c2.name 'relatedTableUniqueKey'
	FROM
		sys.foreign_keys fk
	INNER JOIN
		sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
	INNER JOIN
		sys.columns c1 ON fkc.parent_column_id = c1.column_id AND fkc.parent_object_id = c1.object_id
	INNER JOIN
		sys.columns c2 ON fkc.referenced_column_id = c2.column_id AND fkc.referenced_object_id = c2.object_id
	WHERE OBJECT_NAME(fk.parent_object_id)  =  <cf_queryparam value="#schemaTableDetails.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfquery>

<!--- A new record has just been added or an existing record has been saved --->
<cfif structKeyExists(form,"submitBtn")>

	<cfset returnMessage = "phr_entityTypeScreen_#schemaTableDetails.tableName#_Saved">
	<cfset keyCheck = evaluate('#schemaTableDetails.uniqueKey#')>

	<cfif (ISDEFINED("keyCheck") AND not ISNUMERIC(keyCheck))>
		<!--- 2014/10/16	YMA	modify query to insert new fields. --->
		<CFQUERY NAME="insertEntity" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO
		  #schemaTableDetails.tableName#
		(
			#customEntityKeys.relatedTableUniqueKey#,
			CreatedByPerson,
			Created,
			LastUpdatedByPerson,
			LastUpdated,
			createdby,
			lastupdatedby
		)
		VALUES
		(
		 	<cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,
			GETDATE(),
			<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,
			GETDATE(),
			<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="cf_sql_integer" >)

		SELECT SCOPE_IDENTITY() as newofID
		</CFQUERY>

		<cfset form[schemaTableDetails.uniqueKey] = insertEntity.newofID>
		<cfset form.new = form[schemaTableDetails.uniqueKey]>
		<cfset returnMessage = "phr_entityTypeScreen_#schemaTableDetails.tableName#_Submitted">
	</cfif>

	<cf_updatedata>

	<cfset result.approvalDetails = application.com.approvalEngine.approvalRuleHandler(mode='request', entityTypeID=entityType, approvalStatusRelatedEntityID=form[schemaTableDetails.uniqueKey]) />

	<cfset application.com.relayUI.setMessage(message=returnMessage)>

	<cfif request.relaycurrentuser.isInternal gt 0>
		<!--- Redirect back to listing with final message --->
		<cfset url[customEntityKeys.RELATEDTABLEUNIQUEKEY] = form.frmCurrentEntityID>
		<!--- <cfset url.message = returnMessage> --->
		<cfset url.uniqueKey = form.frmCurrentEntityID>
		<cfset structDelete(url,"editor")>

		<!--- After visiting an sub custom entity and trying to return back to the POL custom entity listing we don't know who we were looking at anymore so get this from LASTLISTINGSCREEN --->
		<cfif returnToScreen neq "" and (structKeyExists(form,"returnScreenLoaded") and form.returnScreenLoaded)>
			<cfset relocateParams= listFirst(url.LASTLISTINGSCREEN,'-')>
			<cfset relocateUrl="#request.currentsite.PROTOCOLANDDOMAIN#/data/entityFrameScreens.cfm?frmEntityScreen=#listGetAt(relocateParams,1,"|")#&frmcurrentEntityID=#listGetAt(relocateParams,2,"|")#&frmentitytypeid=#listGetAt(relocateParams,3,"|")#">
			<cfoutput>
			<script>
			{
				window.location = "#relocateUrl#";
			}
			</script>
			</cfoutput>
		</cfif>

		<!--- if returnToScreen is defined we've got a listing on an editor so turn the editor back on and set that we've loaded the return screen in preperation for our final save. --->
		<cfif returnToScreen neq "" and returnToScreen neq frmcurrentscreentextID>
			<cfset frmcurrentscreentextID = returnToScreen>
			<cfset url.editor = "yes">
			<cfif not (structKeyExists(form,"returnScreenLoaded") and form.returnScreenLoaded)>
				<cfset form.returnScreenLoaded = true>
			</cfif>
		</cfif>
	<cfelse>
		<cfif isDefined("processID") and isNumeric(processID) and processID neq 0>
			<!--- Redirect to a process --->
			<!--- 2015-11-25 YMA BF-22 Prevent error when submitting to a workflow on update of an existing record. Setting entityID to a form variable that is also set when new record added instead of add new recor query result. --->
			<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/proc.cfm?prid=#processID#&entityID=#form[schemaTableDetails.uniqueKey]#&eid=#url.eid#&entityType=#entityType#")>
		<cfelse>
			<!--- Redirect back to listing with final message --->
			<cfif returnToEID eq "">
				<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#url.eid#&uniqueKey=#form.frmCurrentEntityID#&frmcurrententityID=#form.frmCurrentEntityID#")>
			<cfelse>
				<cfset returnURL = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#returnToEID#&editor=yes&uniqueKey=#form.frmCurrentEntityID#&countryID=#form.countryID#&frmcurrententityID=#form.frmCurrentEntityID#")>
			</cfif>
		</cfif>
		<cfoutput>
			<script type="text/javascript">
			{
				window.location = "#returnURL#";
			}
			</script>
		</cfoutput>
	</cfif>
</cfif>

<!--- editor --->
<cfif structKeyExists(url,"editor") and url.editor eq "yes">
		<cfset url.editor = "false">
		<cfset structdelete(form,"SUBMITBTN")>

		<cfform method="post" name="#schemaTableDetails.tableName#" enctype="multipart/form-data">
		<cfoutput>
			<CF_relayFormDisplay class="#(divLayout is "horizontal"?"form-horizontal":"")#">
				<CF_ascreen formname="#schemaTableDetails.tableName#" >

					<cfset params["#schemaTableDetails.uniqueKey#"] = uniqueKey>
					<cfset params["#schemaTableDetails.uniqueKey#list"] = uniqueKey>
					<cfset params["countryID"] = countryID>
					<cfset params["screenid"] = frmCurrentScreenTextID>

					<!--- 2013-11-06	YMA	Get recursively get entityIDs for all related entities ---->
					<cfset entityDetails = application.com.relayPLO.getEntityStructure(entityTypeID =schemaTableDetails.ENTITYTYPEID, entityID = iif(isnumeric(uniqueKey),uniqueKey,0),getEntityQueries=true)>

					<cfset childEntityID = uniqueKey>
					<cfif isNumeric(childEntityID)>
						<cfset childentity = entityDetails.queries[schemaTableDetails.tableName]>
						<cfquery dbtype="query" name="childEntityID">
							select #application.entityType[application.customentities[schemaTableDetails.tableName].REFERENCEDENTITYTYPEID].uniqueKey# as childEntityID
							from childentity
						</cfquery>
						<cfset childEntityID = childEntityID.childEntityID>
					</cfif>

					<cfif not structKeyExists(entitydetails.id,"Person") or not structKeyExists(entitydetails.id,"Location") or not structKeyExists(entitydetails.id,"Organisation")>
						<Cfset childEntityList = application.com.relayentity.recursiveListRelatedTables(tablename=schemaTableDetails.tableName,filterOnlyScreensAndFlags=true)>
						<cfset lastEntityTable = schemaTableDetails.tableName>
						<cfset lastEntityID = url.frmcurrententityID>
						<Cfset entitydetails2 = application.com.relayPLO.getEntityStructure(entityTypeID = application.entitytypeID[lastEntityTable], entityID = iif(isnumeric(lastEntityID),lastEntityID,0),getEntityQueries=true)>
						<cfloop list="#childEntityList#" index="i">
							<cfset childentity = entitydetails2.queries[lastEntityTable]>

							<cfquery dbtype="query" name="childEntityID">
								select #application.entityType[application.customentities[lastEntityTable].REFERENCEDENTITYTYPEID].uniqueKey# as childEntityID
								from childentity
							</cfquery>

							<cfset childEntityID = iif(isnumeric(childEntityID.childEntityID),childEntityID.childEntityID,lastEntityID)>

							<cfset entityDetails2 = application.com.relayPLO.getEntityStructure(entityTypeID = application.entitytypeID[i], entityID = iif(isnumeric(childEntityID),childEntityID,0),getEntityQueries=true)>

							<cfloop collection="#entitydetails2.queries#" item="i">
								<cfset entitydetails.queries[i] = entitydetails2.queries[i]>
								<cfset lastEntityTable = #i#>
							</cfloop>
							<cfloop collection="#entitydetails2.id#" item="i">
								<cfset entitydetails.id[i] = entitydetails2.id[i]>
								<cfset lastEntityTypeID = #i#>
								<cfset lastEntityID = entitydetails2.id[i]>
							</cfloop>

							<cfif structKeyExists(entitydetails.id,"Person") or structKeyExists(entitydetails.id,"Location") or structKeyExists(entitydetails.id,"Organisation")>
								<Cfbreak>
							</cfif>
						</cfloop>
					</cfif>

					<cfloop collection="#entityDetails.id#" item="i">
						<cfif not isNumeric(i)>
							<cfset fieldvalue = entityDetails.id[i]>
							<cfset tablequery = application.com.structureFunctions.queryRowToStruct(entityDetails.queries[i],1)>
							<cfif not isNumeric(fieldvalue)>
								<cfset fieldvalue = "new">
								<cfset tablequery["#application.entitytype[application.entitytypeID[i]].uniqueKey#"] = fieldvalue>
							</cfif>
							<cfset params[i] = tablequery>
							<cfset params["#application.entitytype[application.entitytypeID[i]].uniqueKey#"] = fieldvalue>
						</cfif>
					</cfloop>

					<cfif structKeyExists(url,customEntityKeys.RELATEDTABLEUNIQUEKEY)>
						<cfset params[customEntityKeys.RELATEDTABLEUNIQUEKEY] = url[customEntityKeys.RELATEDTABLEUNIQUEKEY]>
						<cfset structdelete(params,schemaTableDetails.uniqueKey)>
						<cfset structdelete(params,"#schemaTableDetails.uniqueKey#list")>
					</cfif>
					<cfif structKeyExists(url,"FIELDTEXTID")>
						<cfset params["FIELDTEXTID"] = url.FIELDTEXTID>
					</cfif>

					<CF_ascreenitem attributecollection="#params#" method="edit" divLayout = #divLayout#>

					<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="left">
						<!--- 2013-09-19	YMA	Case 437096 need CF_TRANSLATE to get the phrase to show when its not already translated --->
						<!--- <cf_relayFormElement relayFormElementType="submit" fieldname="submitBtn" currentValue="phr_entityTypeScreen_#schemaTableDetails.tableName#_Save" label=""> --->
						<!--- RT-279 NJH 2017/02/16 - changed to cfinput so that we could pass in validate="submitonce" which means that form is submitted only once --->
						<cfinput type="submit" name="submitBtn" value="phr_entityTypeScreen_#schemaTableDetails.tableName#_Save" validate="submitonce" class="btn btn-primary">

						<cf_relayFormElement relayFormElementType="hidden" fieldname="frmEntityType" currentValue="#customEntityKeys.relatedtable#">
						<cf_relayFormElement relayFormElementType="hidden" fieldname="frmCurrentEntityID" currentValue="#url.frmcurrententityID#">
						<cf_relayFormElement relayFormElementType="hidden" fieldname="#schemaTableDetails.uniqueKey#" currentValue="#uniqueKey#">
						<cf_relayFormElement relayFormElementType="hidden" fieldname="countryID" currentValue="#countryID#">
						<cfif request.relaycurrentuser.isInternal gt 0>
							<cfif structKeyExists(form,"returnScreenLoaded")>
								<cf_relayFormElement relayFormElementType="hidden" fieldname="returnScreenLoaded" currentValue="#form.returnScreenLoaded#">
								<cfset returnToScreen="">
							</cfif>
							<cf_relayFormElement relayFormElementType="hidden" fieldname="clickColumn" currentValue="#clickColumn#">
							<!--- START: YMA 2014-08-08 remove generated column names which are not passed through from the tag--->
							<cfif listfindnocase(showTheseColumns,"LastUpdatedByName")>
								<cfset showtheseColumns=rereplace(showTheseColumns,"LastUpdatedByName","LastUpdatedBy")>
							</cfif>
							<cfif listfindnocase(showTheseColumns,"createdByName")>
								<cfset showtheseColumns=rereplace(showTheseColumns,"createdByName","createdBy")>
							</cfif>
							<cfif listfindnocase(showTheseColumns,"del")>
								<cfset showtheseColumns=rereplace(showTheseColumns,"del","")>
							</cfif>
							<!--- END: YMA 2014-08-08 remove generated column names which are not passed through from the tag--->
							<cf_relayFormElement relayFormElementType="hidden" fieldname="showTheseColumns" currentValue="#showTheseColumns#">
							<!--- <cf_relayFormElement relayFormElementType="hidden" fieldname="dateFormat" currentValue="#dateFormat#"> --->
							<cf_relayFormElement relayFormElementType="hidden" fieldname="GroupByColumns" currentValue="#GroupByColumns#">
						</cfif>
					</cf_relayFormElementDisplay>

				</CF_ascreen>
			</CF_relayFormDisplay>
		</cfoutput>
		</cfform>

<!--- List --->
<cfelse>
	<cfset flagJoinInfo = structNew()>
	<cfset tableColumns = "">
	<cfset flagColumns = "">
	<cfset uniqueKeyList = "uniqueKey">
	<cfset clickList="">
	<cfset random = randrange(100000,0)>

	<cfif request.relaycurrentuser.isinternal gt 0>
		<cfif StructKeyExists(form,"showTheseColumns")><cfset showTheseColumns = form.showTheseColumns></cfif> <!--- YMA 2014-08-08 remove generated column names which are not passed through from the tag--->
		<cfoutput>
			<script type="text/javascript">
			deleteRow#random# = function(uniqueKey){
			if (confirm("phr_areYouSureYouWantToDelete"+uniqueKey+"?") == true) {
				var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webservicename=relayScreens&methodName=deleteEntityRow&returnformat=json&_cf_nodebug=true'

				var myAjax = new Ajax.Request(
					page,
					{
						method: 'post',
						asynchronous: false,
						parameters: {entityID:uniqueKey,entityName:'#schemaTableDetails.tableName#',uniqueKey:'#schemaTableDetails.uniqueKey#',entityTypeID:'#entityType#'},
						onComplete: function (requestObject,JSON) {
							json = requestObject.responseText.evalJSON(true)
							if(!json.ISOK) {
								alert(json.MESSAGE);
							} else {
								// YMA 2014-08-08 fixed bug, so after deleting record from DB the row is removed from the html table
								trID = 'uniqueKey='+uniqueKey;
								$(trID).remove();
							}
			           }
			       });
				}
			}
			</script>
		</cfoutput>
	</cfif>

	<!--- When on internal partner dashboard get current entity relationship to Organisation, Country and Location to submit to editor form. --->
	<cfif structKeyExists(url,"uniqueKey") and structKeyExists(url,"countryID")>
		<cfset currentcountryID = url.countryID>
		<cfset variables[customEntityKeys.relatedTableUniqueKey] = url.uniqueKey>
	<cfelse>
		<cfif structKeyExists(form,"FRMENTITYTYPE") and structKeyExists(URL,"FRMCURRENTENTITYID") and listFindNoCase("organisation,person,location",form.FRMENTITYTYPE )>

			<cfset entityStructure = application.com.relayplo.getEntityStructure(entityTypeID=url.FRMENTITYTYPEID,entityID=url.FRMCURRENTENTITYID)>
			<cfset variables[customEntityKeys.relatedTableUniqueKey] = entityStructure.id[customEntityKeys.relatedTable]>

			<cfset currentcountryID = entityStructure.countryID>
			<cfquery name="getEntityTypeID"  datasource="#application.SiteDataSource#">
				select entityTypeID from schematable where entityname  =  <cf_queryparam value="#customEntityKeys.relatedTable#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
			<cfset url.FRMENTITYTYPEID = getEntityTypeID.entityTypeID>
		<cfelse>

			<!--- 2015-01-21 AHL Case 443209 Add check for url.uniqueKey existing --->
			<cfif structKeyExists(request.relaycurrentuser,"#customEntityKeys.relatedTableUniqueKey#")>
				<cfset variables[customEntityKeys.relatedTableUniqueKey] = request.relaycurrentuser[customEntityKeys.relatedTableUniqueKey]>
			<cfelseif structKeyExists(url,"uniqueKey")>
				<cfset variables[customEntityKeys.relatedTableUniqueKey] =  url.uniqueKey>
			<cfelse>
				<cfset variables[customEntityKeys.relatedTableUniqueKey] =  0>
			</cfif>
		</cfif>
	</cfif>

	<cfset numberFormatMask = "">

	<!--- prepare list columns --->
	<cfloop list="#showTheseColumns#" index="i">
		<cfif listLast(i,'.') eq "lastUpdatedBy"><cfset showTheseColumns = REReplace(showTheseColumns,i,"LastUpdatedByName")></cfif>
		<cfif listLast(i,'.') eq "createdBy"><cfset showTheseColumns = REReplace(showTheseColumns,i,"createdByName")></cfif>
		<!--- 2014/10/16	YMA	support new fields being chosen. --->
		<cfif listLast(i,'.') eq "lastUpdatedByPerson"><cfset showTheseColumns = REReplace(showTheseColumns,i,"LastUpdatedByName")></cfif>
		<cfif listLast(i,'.') eq "createdByPerson"><cfset showTheseColumns = REReplace(showTheseColumns,i,"createdByName")></cfif>
		<cfset showTheseColumns = ListRemoveDuplicates(showTheseColumns,",",true)>
		<cfif CompareNoCase(left(i,5),"flag_") eq 0>
			<cfset flagID = right(i,len(i)-len(left(i,5)))>
			<cfset structInsert(flagJoinInfo,i,application.com.flag.getJoinInfoForFlag(flagID=flagID,entityTableAlias=schemaTableDetails.tableName))>
			<cfset flagColumns= ListAppend(flagColumns, right(i,len(i)-len(left(i,5))))>
			<cfset flagStruct = application.com.flag.getFlagStructure(flagId=flagID)>
			<cfset numberFormatMask = listAppend(numberFormatMask,"0.0")>
		<cfelse>
			<cfset tableColumns = ListAppend(tableColumns, listLast(i,'.'))>
			<cfset showTheseColumns = REReplace(showTheseColumns,i,listLast(i,'.'))>
			<!--- <cfset dateFormat = REReplace(dateFormat,i,listLast(i,'.'))> --->
			<cfset GroupByColumns = REReplace(GroupByColumns,i,listLast(i,'.'))>
			<cfset clickColumn = REReplace(clickColumn,i,listLast(i,'.'))>
		</cfif>
	</cfloop>
	<!--- prepare list query
		NJH 2014/07/18 - removed the #schemaTableDetails.tableName#.* from the query and replaced with a looping over the table column names, as a TFQO doesn't seem to find columns when there is a *
	--->
	<cfsavecontent variable="query">
		<cfoutput>
			select #schemaTableDetails.uniqueKey# as uniqueKey,
				<cfloop list="#tableColumns#" index="columnName">#schemaTableDetails.tableName#.#columnName#,</cfloop>
                                <!--- START 2014-10-15 NJH P-NEX001 changed joins to person table to use vEntity to bring back data on deleted persons and when id=0 --->
				lastUpdatedBy.name as lastUpdatedByName,
				createdBy.name as createdByName
				<!--- END 2014-10-15 NJH P-NEX001 changed joins to person table to use vEntity to bring back data on deleted persons and when id=0 --->
				<cfloop collection="#flagJoinInfo#" item="i">
					,#flagJoinInfo[i].selectField# as [#flagJoinInfo[i].alias#]
					<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - added data for user defined currency --->
					<cfif StructKeyExists(flagJoinInfo[i],"selectFieldB")>
						,#flagJoinInfo[i].selectFieldB# as [#flagJoinInfo[i].alias#B]
					</cfif>
				</cfloop>
			from #schemaTableDetails.tableName#
				<cfloop collection="#flagJoinInfo#" item="i">
					#flagJoinInfo[i].join#
				</cfloop>
			<!--- START 2014-10-15 NJH P-NEX001 changed joins to person table to use vEntity to bring back data on deleted persons and when id=0 --->
			<!--- doing a left join because sometimes updates against an entity are unknown and are therefore marked against personID 0 --->
			LEFT join vEntityName lastUpdatedBy on #schemaTableDetails.tableName#.lastupdatedbyperson = lastUpdatedBy.entityID AND lastUpdatedBy.entityTypeID=#application.entityTypeID.person#
			<!--- 2014/10/16	YMA	now using createdbyperson. --->
			join vEntityName createdBy on #schemaTableDetails.tableName#.createdbyperson = createdBy.entityID AND createdBy.entityTypeID=#application.entityTypeID.person#
			<!--- END 2014-10-15 NJH P-NEX001 changed joins to person table to use vEntity to bring back data on deleted persons and when id=0 --->
			where #schemaTableDetails.tableName#.#customEntityKeys.relatedTableUniqueKey# = <cfif isNumeric(variables[customEntityKeys.relatedTableUniqueKey])>#variables[customEntityKeys.relatedTableUniqueKey]#<cfelse>0</cfif>
		</cfoutput>
	</cfsavecontent>

	<cfquery name="getEntityData" datasource="#application.SiteDataSource#">
		#preservesinglequotes(query)#
		<cfif sortorder is not "">
			order by <cf_queryObjectName value="#sortorder#"> <!--- 2014-09-30 AXA Added sortOrder --->
		</cfif>
	</cfquery>

	<!--- if internal add the delete column --->
	<cfif #request.relaycurrentuser.isinternal# gt 0>
		<cfquery name="getEntityData" dbtype="query">
			select *, 'phr_delete' as del
			from getEntityData
		</cfquery>
	</cfif>


	<!--- Figure out which columns to booleanformat from the flag type and table column data types --->
	<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - changed flagTypeID 14 to financial --->
	<cfquery name="getFormatColumns"  datasource="#application.SiteDataSource#">
		select column_name as col, data_type, numeric_scale as decimalPlaces from INFORMATION_SCHEMA.COLUMNS
			where data_type in ('bit','datetime','numeric')
			and table_Name =  <cf_queryparam value="#schemaTableDetails.tableName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and column_name in (<cf_queryparam value="#tableColumns#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
		<cfif isDefined("flagColumns") and flagColumns neq "">
			union all
			select 'flag_' + cast(f.flagID as varchar) as col, case when fg.flagTypeID in (2,3) then 'bit' when fg.flagTypeID = 14 then 'financial' else 'datetime' end as data_type, fg.flagTypeDecimalPlaces as decimalPlaces
			from flag f
				join flaggroup fg on f.flaggroupID = fg.flaggroupID
					and f.flagID in (<cf_queryparam value="#flagColumns#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
					and fg.flagtypeID in (2,3,4,14)
		</cfif>
	</cfquery>

	<cfquery name="booleanFormatColumns" dbtype="query">
		select * from getFormatColumns where data_type='bit'
	</cfquery>

	<cfquery name="dateTimeFormatColumns" dbtype="query">
		select * from getFormatColumns where data_type='datetime'
	</cfquery>

	<!--- NJH 2015/03/04 Jira Fifteen 267 - work out numberFormat columns and their corresponding format masks --->
	<cfquery name="currencyFormatColumns" dbtype="query">
		select * from getFormatColumns where data_type in ('numeric','currency')
	</cfquery>

	<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags --->
	<cfquery name="financialFormatColumns" dbtype="query">
		select * from getFormatColumns where data_type in ('financial')
	</cfquery>

	<cfset numberFormatMaskList = "">
	<cfloop query="currencyFormatColumns">
		<cfset numberFormatMask = "*comma_-_" & #IIF(decimalPlaces gt 0,DE("."),DE(""))# & repeatstring("_",decimalPlaces)>
		<Cfset numberFormatMaskList = listAppend(numberFormatMaskList,numberFormatMask)>
	</cfloop>

	<cfset urlList="">
	<cfset clickList="">
	<cfif isNumeric(variables[customEntityKeys.relatedTableUniqueKey])>
		<cfif request.relaycurrentuser.isinternal gt 0>
			<!--- START: YMA 2014-08-08 remove generated column names which are not passed through from the tag--->
			<cfif listLen(clickColumn) eq 1 or not listfind(showthesecolumns,"del")>
				<cfset showTheseColumns = listAppend(showTheseColumns,"del")>
				<cfif not listfind(clickColumn,"del")>
					<cfset clickColumn = listAppend(clickColumn,"del")>
				</cfif>
				<cfif not listfind(uniqueKeyList,"uniqueKey")>
					<cfset uniqueKeyList = listAppend(uniqueKeyList,"uniqueKey")>
				</cfif>
			</cfif>
			<!--- END: YMA 2014-08-08 remove generated column names which are not passed through from the tag--->
			<cfset urlList = application.com.security.encryptURL(url="#request.currentsite.PROTOCOLANDDOMAIN#/relaytagtemplates/entitylistandeditscreen.cfm?editor=yes&frmCurrentScreenTextID=#frmCurrentScreenTextID#&frmcurrententityID=#variables[customEntityKeys.relatedTableUniqueKey]#&countryID=#currentcountryID#&entityType=#entityType#&returnToScreen=#returnToScreen#&clickColumn=#clickColumn#&showTheseColumns=#showTheseColumns#&GroupByColumns=#GroupByColumns#")>
			<cfif returnToScreen neq "">
				<cfset urllist = urllist & '&returnToEntityTypeID=' & url.entityType>
			</cfif>
			<cfif structKeyExists(url,"lastListingScreen")>
				<cfset urllist = urllist & '&lastListingScreen=' & listAppend(url.lastListingScreen,"#frmCurrentScreenTextID#|#variables[customEntityKeys.relatedTableUniqueKey]#|#schemaTableDetails.entityTypeID#","-")>
			<cfelse>
				<cfset urllist = urllist & '&lastListingScreen=' & FORM.THISSCREENTEXTID & '|' & variables[customEntityKeys.relatedTableUniqueKey] & '|' & URL.FRMENTITYTYPEID>
			</cfif>
			<cfset urllist = urllist & '&uniqueKey='>
			<cfset urlList = listAppend(urlList,"##")>

			<cfset clickList = " ,javascript:deleteRow#random#(##uniqueKey##)">
		<cfelse>
			<cfset urlList = "#request.currentsite.PROTOCOLANDDOMAIN#/?eid=#listingEID#&editor=yes&add=no&frmCurrentScreenTextID=#frmCurrentScreenTextID#&frmcurrententityID=#variables[customEntityKeys.relatedTableUniqueKey]#&countryID=#request.relaycurrentuser.organisation.countryID#">
			<cfset urllist = urllist & '&uniqueKey='>
			<cfset clickList = " ">
		</cfif>


		<cfoutput>
			<div style="display:block;width:100%; margin:0 auto;">
				<div class="addNewButton" style="text-align:right; margin:0 auto;">
					<a class="button" href="#listFirst(urlList)#new">phr_#addnewPhraseTextID#</a>
				</div>
			</div>
		</cfoutput>


		<!--- 2014-09-30 AXA Removed NoForm attribute --->
		<!--- 2014-11-10 AXA changed numrowsperpage value to variable and added columnsorting flag and sortorder param --->
		<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags --->
		<cf_tablefromqueryobject
			rowIdentityColumnName="uniqueKey"
			keyColumnList="#clickColumn#"
			keyColumnKeyList="#uniqueKeyList#"
			keyColumnURLList="#urlList#"
			keyColumnOnClickList="#clickList#"
			showTheseColumns="#showTheseColumns#"
			useInclude="false"
			queryObject="#getEntityData#"
			numberFormat="#valueList(currencyFormatColumns.col)#"
			numberFormatMask="#numberFormatMaskList#"
			financialFormat="#valueList(financialFormatColumns.col)#"
			dateTimeFormat="#valueList(dateTimeFormatColumns.col)#"
			numRowsPerPage="#numRowsPerPage#"
			columnTranslation="true"
			columnTranslationPrefix="phr_report_entityTypeScreen_"
			booleanFormat="#ValueList(booleanFormatColumns.col)#"
			GroupByColumns="#GroupByColumns#"

			allowColumnSorting="true"
			sortOrder = "#SORTORDER#"
		>

	<cfelse>
		<cfoutput>
			<div style="display:block;width:100%; margin:0 auto;">
				<p>phr_saveRecordBeforeAddingSubEntity</p>
			</div>
		</cfoutput>
	</cfif>

<!--- 	<cfif StructKeyExists(URL,"message") and url.message neq "">
		<cfoutput>
			<cf_translate>
			<script type="text/javascript">
				alert('#jsStringFormat(url.message)#');
			</script>
			</cf_translate>
		</cfoutput>
		<cfset structDelete(url,"message")>
	</cfif> --->
</cfif>