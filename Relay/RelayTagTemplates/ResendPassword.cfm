<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			resendPassword.cfm
Author:				SWJ
Date started:		2002

Description:

Optional variables that can be set in element parameters

GoToEID = By default this form will return the external login.  Set GoToEID
		  to another eid that you want to go to rather that the default.
		  This will set the form variable URLRequested to eid=#goToEID#

fromAddress = the email address this email should be coming from (defaults to application.supportEmail)

thisApplicationName = the name of the application or microsite that this email is about
			defaults to request.CurrentSite.Title

thisSiteName = the application they will be taken back to defaults to #request.CurrentSite.Title#"

thisDomain = the application they will be taken back to defaults to #request.CurrentSite.Title#"

phraseprefix = a text string that will precede the existing phrasetextIDs

Amendment History:

Date (DD-MMM-YYYY)	Initials	CODE 							What was changed

2005-03-23			AJC			P_SNY001: Forgotten Password	If the approvalflag is passed then it will check if the organisation has this flag as true. If not a error message will appear
																Also you can pass sendEmail and speciy the email template to use in application.com.email.sendEmail.

2005/05/06			MDC			P_SNY001: Forgotten Password	Changed the file to use cf_translate.
																Added in PhrasePrefix so that the translations can be site specific.

2006/02/06			WAB											Allow prepopulation with users email address if we know who they are

2006/03/17			GCC			CR_SNY588 set password			Send link to change password and capture user entered password after they follow the link.

2006/11/20			WAB											removed a line which was preventing password being sent if someone else had already logged into this machine
2007/03/16			NJH											added id's to the input fields so we could get better hooks for styling
2007/09/19			WAB			dealing with corrupted links see comments
2007-09-26			AJC			P-SNY041 DealerNet Sony1 Integration   1) Allow multiple approvalflags to be passed
																	   2) ChangePassword attribute added which will generate a new pw for the person
																	   3) useUsername attribute added. If this is set then it will ask for username not email address
																	   4) IF per and org approval flags passed then it will check both
2008-07-04			NYF  		bug 546
2008/07/09                      GCC             added relocate to stop relocation if email send failed and display message instead
2008-11-10                      WAB             Problem with change password not picking up the encryption.  Fixed to look like changepassword.cfm
2009-04-03			NYB			Sophos Stargate	added styling, and ability to display email address instead of username on Enter new password screen
2009/07/01			NJH			P-FNL069 - encrypt/decrypt hidden form fields
2009/08/28			NJH			LID 2567 - used named parameters for decrypt function and put bent pipes back in.
2010/06/29			NJH			P-FNL069 - added new login authentication code.
2013-09-24 			NYB 		Case 436181 replaced phr_CP_Password_Link_Sent with a linkSentText param
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2014-01-09 			NYB 		Case 438275 changed search around username to replace opening quote with closing quote and closing quote with opening quote
2016/06/14			NJH			JIRA PROD2016-595 - removed org/per approval flag parameters, and replace with call to isPersonAValidUserOnTheCurrentSite which will deal with that and more. Cleaned up existing code a little. Needs an overhaul really.
2016-06-29			WAB			During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Should not be necessary ('private variable')
Possible enhancements:


 --->


<!--- Get Phrases --->

<cf_param name="ChangePassword" default="no" type="boolean"/>
<cf_param name="useUsername" default="no" type="boolean"/>
<cf_param name="displayDetail" default="username"/>
<!--- <cfparam name="orgApprovalFlag" default="">
<cfparam name="perApprovalFlag" default=""> --->
<cf_param name="confirmSendEmail" default="no" type="boolean"/>
<cf_param name="sendEmail" default=""/>
<cf_param name="relocate" default="yes" type="boolean"/>
<!--- <cfparam name="suppressOutput" default="false">  ---><!--- suppress output.. just set a message --->
<cfparam name="linkSentText" default="#request.relayCurrentUser.isInternal?'phr_CP_Password_Link_Sent':'phr_Ext_Password_Link_Sent'#"/>  <!--- 2013-09-24 NYB Case 436181 added --->

<!--- <cfif linkSentText eq "">
	<cfset linkSentText = request.relayCurrentUser.isInternal?"phr_CP_Password_Link_Sent":"phr_Ext_Password_Link_Sent">
</cfif> --->

<cfset phraseprefix = "">
<cfset suppressOutput = request.relayCurrentUser.isInternal?true:false>

<!--- NJH 2010/07/01 P-FNL069 - check if password encryption is turned on for backwards compatibility. If the password is already encrypted,
	then we need to tell changeUserPassword and getWhyLoginFailed, as that will encrypt the password when comparing the password with what is
	in the database
 --->
<cfif structKeyExists(application,"encryptionMethod") and structKeyExists(application.encryptionMethod,"relayPassword")>
	<cfset passwordEncrypted = true>
<cfelseif structKeyExists(application,"passwordEncryptionMethod") and application.passwordEncryptionMethod is not "">
	<cfset passwordEncrypted = true>
<cfelse>
	<cfset passwordEncrypted = false>
</cfif>


<!--- 2006/03/16 - GCC - CR_SNY588 Change Password --->
<cfif structkeyexists(form,"Action") and form.Action eq "setPassword">
	<!--- 2016-06-29	WAB	During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Did not seem to be serving much purpose.  Even if field was encrypted, it will have been decrypted and put in form scope automatically  
	<cfif structKeyExists(request .encryption .encryptedFields,"personID")>
		<cfset form.personID = request .encryption .encryptedFields.personID>
	</cfif>
	--->

	<!--- AJC P-SNY041 Relay Global Encrypted Passwords --->
	<cfquery name="qry_get_personUsername" datasource="#application.sitedatasource#">
		select username from person
		where personID =  <cf_queryparam value="#form.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfset changePassResult = application.com.login.changeUserPassword(username=qry_get_personUsername.username,currentPassword=form.oldPassword,newPassword1=form.frmNewPassword1,newPassword2=form.frmNewPassword2,passwordEncrypted=passwordEncrypted)>

	<cfoutput>
		<cfset message = changePassResult.message>
		<cfif not suppressOutput>
		<div class="CP_message">#htmleditformat(changePassResult.message)#</div>
		</cfif>

		<!--- the pwe (password expired) variable is passed in when the user is sent to this page because their password has expired. --->
		<cfif structKeyExists(url,"pwe") and url.pwe and changePassResult.isOK and not request.relayCurrentUser.isLoggedIn>
			<div class="CP_message">phr_login_passwordSuccessfullyChanged.</div>
		<!--- if we've changed password through the 'Resend password' link, then show the normal password changed message, as that will have a link to login --->
		<cfelseif changePassResult.isOK>
			<cfset message = "phr_password_successfullyChanged">
			<cfif not suppressOutput>
				<div class="CP_message">#message#</div>
			</cfif>
		</cfif>
	</cfoutput>
</cfif>

<!--- NJH 2010/06/29 P-FNL069 we don't want to go down here if we've changed the password successfully --->
<cfif not isDefined("changePassResult") or isDefined("changePassResult") and not changePassResult.isOK>

	<cfif application.com.settings.getSetting("security.passwordGenerationMethod") eq "user" and structkeyexists(URL,"spl") and url.spl is not "">  <!--- WAb added is ot "" after some spurious blank links turned up --->

		<!--- could be set in the relayINI --->
		<cfparam name="request.maxLinkAgeInDays" default="28">
		<!--- deconstruct the params --->
		<!--- WAB 2007/09/19 had incident where the spl got a '.' at the end of it, solved by checking the last character and chopping off if not valid, am going to leave it in release code	--->
		<cfif not refindnocase("[0-9a-f]",right(url.spl,1))>
			<cfset url.spl = left(url.spl,len(url.spl)-1)>
		</cfif>

		<!--- WAB 2007/09/19   any error in this section assumed to be a bad link , no displays message rather than falling over --->
		<cftry>
			<!--- NJH 2009/08/28 LID 2567 - used named parameters and put bent pipes back in. --->
			<cfset variableSet = application.com.encryption.decryptString(string=url.spl)>
			<cfset link = {PersonID = listFirst(variableSet,application.delim1), CreatedDate = listgetat(variableSet,2,application.delim1),password = listgetat(variableSet,3,application.delim1), OK = true, tooOld = false,NolongerExists = false}>

			<!--- work out if it is still a valid link --->
			<cfif datediff("d",link.CreatedDate,now()) gt request.maxLinkAgeInDays>
				<cfset link.tooOld=true>
				<cfset link.ok= False>
			</cfif>
			<!--- work out if the user exists and the password matches that on the system --->
			<cfquery name="checkLinkDetails" datasource="#application.sitedatasource#">
				<!--- NYB 2009-04-03 - Sophos Bugzilla 1741 - added "firstname, firstname+' '+lastname as [Name],email" to query --->
				Select personID,username, firstname, firstname+' '+lastname as [Name],email from person
				where personID =  <cf_queryparam value="#link.PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  and password =  <cf_queryparam value="#link.Password#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfif checkLinkDetails.recordcount neq 1>
				<cfset link.NolongerExists="true">
				<cfset link.ok=False>
			</cfif>

			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_Warning (Type="Password Link Error",message="url.spl = #url.spl#",caughtError=cfcatch)>
				<cfset link = {OK = false,nolongerExists=false,tooOld=false}>  <!--- WAB 2012-05-29 added keys to link structure.  If the decryption errored (corrupted link) then was falling over later on--->
			</cfcatch>
		</cftry>


		<cfoutput>
			<cfif link.ok>
				<cfset message = "">
				<cfset loginReasonStruct = application.com.login.getWhyLoginFailed(username=checkLinkDetails.username,password=link.Password,passwordEncrypted=passwordEncrypted)>

				<!--- NJH 2010/06/29 P-FNL069 if the password is about to expire or has expired, then show the message. The 'pwe' variable is set when we are
					directed here from et.cfm where the password is expired.. it won't appear when a partner is simply changing their password --->
				<cfif (loginReasonStruct.user.passwordExpired or loginReasonStruct.user.passwordExpiryWarning) and structKeyExists(url,"pwe")>
					<cfset mergeStruct = {passwordexpiresindays = loginReasonStruct.user.passwordExpiresIndays}>
					<cfset message = application.com.relaytranslations.translatePhrase(phrase=loginReasonStruct.message,mergeStruct = mergeStruct)>
				</cfif>

				<cfif not request.relayCurrentUser.isInternal>
					<cfinclude template="/templates/changePWForm.cfm">
				</cfif>

			<cfelse>
				<cfif link.NoLongerExists>
					<cfset message = "phr_CP_Link_No_Longer_Valid">
				<cfelseif link.tooOld>
					<cfset message = "phr_CP_Link_Expired">
				<cfelse>
					<cfset message = "phr_CP_Link_Corrupted">
				</cfif>

				<cfif not suppressOutput>
					<b>#message#</b>
				</cfif>

				<cfif not suppressOutput>
					<CFIF not request.relayCurrentUser.isInternal>
						<a href="/?eid=#eid#">
					<CFELSE>
						<a href="/resendPW.cfm">
					</CFIF>
					phr_ClickHere
					</a>
					phr_CP_To_Generate_A_New_password_link
				</cfif>
			</cfif>
		</cfoutput>
	<cfelse>

			<CFIF not IsDefined("Partneremail") and not IsDefined("Partnerusername")>
				<cfif not request.relayCurrentUser.isInternal>
			<!--- If there is no partneremail data display a form to prompt the user for it --->
				<cfoutput>


						<!--- LID 4832 NJH 2011/01/05 - this page can be called from defaultLoginV3, which has it's own form already. If we're in a form, we don't want to write out another form --->
						<!--- NJH 2016/06/17 - inLoginForm doesn't seem to exist anymore
						<cfset outputForm = true>
						<cfif isDefined("inLoginForm") and inLoginForm>
							<cfset outputForm=false>
						</cfif>

						<cfset shrtTst = useUsername>

						<cfif not shrtTst>
							<cf_includeJavascriptOnce template="/javascript/verifyEmail.js">
						</cfif>

						<script>
							function validateForm() {
								<cfif not shrtTst>
								validEmailMsg = verifyEmail(document.getElementById('Partneremail'));
								if (validEmailMsg != '') {
									alert('phr_changePassword_email');
									return false;
								}
								<cfelse>
								if (document.getElementById('Partnerusername').value == '') {
									alert('phr_changePassword_username');
									return false;
								}
								</cfif>
								return true;
							}
						</script> --->


						<!--- <cfif outputForm> --->
							<FORM NAME="LoginForm" METHOD="post">
						<!--- </cfif> --->
							<cf_relayFormDisplay class="">
							<cfset request.relayForm.useRelayValidation = true>

							<p class="title">phr_ResendPW_PleaseEnterYour#iif(useUsername,de("UserName"),de("EmailAddress"))#</p>
							<!--- <div class="form-group row">
								<cfif shrtTst>
									<div class="col-xs-12 col-sm-3 control-label required">
										<label for="Partnerusername" class="required">phr_#htmleditformat(phraseprefix)#ResendPW_#iif(shrtTst,de("UserName"),de("EmailAddress"))#</label>
									</div>
									<div class="col-xs-12 col-sm-9">
										<INPUT TYPE="Text" NAME="Partnerusername" size="30" value = "" id="Partnerusername" class="form-control">
									</div>
								<cfelse>


									<div class="col-xs-12 col-sm-3 control-label required">
										<label for="Partneremail" class="required">phr_#htmleditformat(phraseprefix)#ResendPW_#iif(shrtTst,de("UserName"),de("EmailAddress"))#</label>
									</div>
									<div class="col-xs-12 col-sm-9">
										<INPUT TYPE="Text" NAME="Partneremail" size="30" value = "" id="Partneremail" class="form-control">
									</div>

								</cfif>
							</div> --->

							<cfif useUsername>
								<cf_relayFormElementDisplay type="text" fieldname="Partnerusername" currentValue="" required="true" label="phr_ResendPW_UserName">
							<cfelse>
								<cf_relayFormElementDisplay type="text" fieldname="Partneremail" currentValue="" validate="email" required="true" label="phr_ResendPW_EmailAddress">
							</cfif>

							<!--- <div id="ResendPWRow form-group row">
								<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
									<cfif not request.currentSite.isInternal>
										<CF_INPUT type="submit" value="phr_continue" class="submitbutton button btn btn-primary"><!--- <A HREF="javascript:document.GetEmail.submit()">#phr_#phraseprefix#Continue#</A> --->
									<cfelse>
										<!--- LID 4832 NJH 2011/01/05 - changed the styling to match the login page for the internal system --->
										<span id="loginLinks">
											<a href="javascript:document.LoginForm.submit();" class="loginLink button btn btn-primary">phr_continue</a>
										</span>
									</cfif>
								</div>
							</div> --->
							<cf_relayFormElementDisplay type="submit" fieldname="frmSubmit" currentValue="phr_continue" class="submitbutton button btn btn-primary">

							<cfif isDefined ("goToEID")>
								<!--- NJH 2009/07/01 P-FNL069 - encrypt hidden form fields --->
								<cf_encryptHiddenFields>
								<CF_INPUT type="hidden" name="URLRequested" value="eid=#goToEID#">
								<cf_encryptHiddenFields/>
							</cfif>
							</cf_relayFormDisplay>
						<!--- <cfif outputForm> --->
						</FORM>
						<!--- </cfif> --->


				</cfoutput>
				</cfif>
			<CFELSE>

				<!---
						Otherwise if partneremail is present process it
				--->

				<!--- Verify Email Address --->
				<!--- PKP : 2008-01-31 : DETERMINE INTERNAL OR EXTERNAL USER--->
				<CFIF not request.relayCurrentUser.isInternal>

					<cfset allowInactiveUser = application.com.settings.getSetting("security.external.login")>

					<!--- P-SNY041 - fix so that it sends to only the first person it finds i.e. if there are multiple person with same email --->
					<cfquery name="CheckEmail" datasource="#application.sitedatasource#" maxrows=1>
						SELECT personid,organisationID FROM person
					WHERE
					<cfif useUsername>
					username =  <cf_queryparam value="#partnerusername#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelse>
					<!--- START: 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
					(
					email =  <cf_queryparam value="#replace(partneremail,chr(96),chr(39),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >
					or
					email =  <cf_queryparam value="#replace(partneremail,chr(39),chr(96),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >
					)
					<!--- END: 2014-01-09 NYB Case 438275 --->
					</cfif>
						and len(password) > 1
			<!--- 		WAB 2006/11/20 removed this check - doesn't make any sense at all.  Prevents someone using the resend password function after someone else has logged into a machine
						<cfif not request.relaycurrentuser.isUnknownUser>
							and personID = #request.relayCurrentUser.personID#
					</cfif>
			 --->
			 			and personid <>  <cf_queryparam value="#application.unknownpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >    <!--- WAB 2007-06-21 to prevent unknown user being send a password accidentally - seemed to happen on Trend, although can't explain how email address could have been guessed!--->
			 			<!--- START: AJC 2008-09-26	CR-TND561 - Req 4 - Inactive User - Switch in external login for allow inactive person --->
						<cfif allowInactiveUser is false> <!--- was application.settings.login. Allow InactiveUser --->
							and active = 1
						</cfif>
						<!--- END AJC 2008-09-26	CR-TND561 - Req 4 - Inactive User - Switch in external login for allow inactive person --->
					</CFQUERY>
				<CFELSE>
					<!--- P-SNY041 - fix so that it sends to only the first person it finds i.e. if there are multiple person with same email --->
					<cfquery name="CheckEmail" datasource="#application.sitedatasource#" maxrows=1>
						SELECT P.personid, p.firstname, p.lastname, l.sitename, p.username, p.password
						FROM person p
						inner join  UserGroup ug on UG.personid = P.personid
						inner join location l on l.locationid = p.locationid
						<!--- START: 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
						WHERE
						(
						P.email =  <cf_queryparam value="#replace(partneremail,chr(96),chr(39),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >   <!--- 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
						or
						P.email =  <cf_queryparam value="#replace(partneremail,chr(39),chr(96),'all')#" CFSQLTYPE="CF_SQL_VARCHAR" >   <!--- 2014-01-09 NYB Case 438275 added replace of opening quote with closing quote --->
						)
						<!--- END: 2014-01-09 NYB Case 438275 --->
						  AND p.Active <> 0 <!--- true --->
						  AND p.Password <> ''
						  AND p.LoginExpires >= #CreateODBCDateTime(Now())#
					</CFQUERY>
				</CFIF>

				<CFIF CheckEmail.recordcount EQ 1>
					<!--- P-SNY041 - Loops through approval flag list and if a false is set the email is not set --->
					<!--- START AJC 2007-09-26 P-SNY041 --->

					<!--- if the organisation has to be approved to send out the
					password and also the approvalFlag has been set --->
					<!--- <cfset confirmSendEmail=true>
					<cfif orgApprovalFlag is not "">
						<!--- set send email to false, if a flag is found then it will be set to true --->
						<cfset confirmSendEmail=false>
						<cfloop list="#orgApprovalFlag#" index="orgAppFlag">
							<cfif application.com.flag.checkBooleanFlagByID(CheckEmail.organisationID,orgAppFlag) >
								<cfset confirmSendEmail=true>
							</cfif>
						</cfloop>
						<!--- <!--- if is approved --->
						<cfif application.com.flag.checkBooleanFlagByID(CheckEmail.organisationID,orgApprovalFlag) >
							<cfset confirmSendEmail=true>
						<!--- if is not approved --->
						<cfelse>
							<cfset confirmSendEmail=false>
						</cfif> --->
					</cfif>

					<cfif perApprovalFlag is not "" and confirmSendEmail>

						<cfset confirmSendEmail=false>
						<cfloop list="#perApprovalFlag#" index="perAppFlag">
							<cfif application.com.flag.checkBooleanFlagByID(CheckEmail.personID,perAppFlag)>
								<cfset confirmSendEmail=true>
							</cfif>
						</cfloop>
						<!--- <!--- if is approved --->
						<cfif application.com.flag.checkBooleanFlagByID(CheckEmail.personID,perApprovalFlag) >
							<cfset confirmSendEmail=true>
						<!--- if is not approved --->
						<cfelse>
							<cfset confirmSendEmail=false>
						</cfif> --->
					<cfelse>
						<cfset confirmSendEmail=true>
					</cfif> --->

					<!--- NJH 2016/06/14 JIRA PROD2016-595 - send email if person is a valid user on the site. No need for organisation/person/location approval flags to be passed in --->
					<cfset validUserCheck = application.com.login.isPersonAValidUserOnTheCurrentSite(personID=CheckEmail.personID)>
     					<cfset confirmSendEmail = validUserCheck.isValidUser or (structKeyExists (validUserCheck,"FailETID") and validUserCheck.FailETID  neq 'LoginFailure' ) >
					
					<!--- END AJC 2007-09-26 P-SNY041 --->
					<!--- if an email template has been passed and it is ok to send the password --->
					<cfif sendEmail is not "" and confirmSendEmail>
						<cfif application.com.settings.getSetting("security.passwordGenerationMethod") eq "user">
							<!--- START AJC 2007-09-26 P-SNY041 --->
							<cfscript>
								application.com.login.createUserNamePassword(personID=CheckEmail.personID,OverWritePassword=changePassword);
							</cfscript>
							<!--- END AJC 2007-09-26 P-SNY041 --->
							<cfset application.com.email.sendEmail(sendEmail,CheckEmail.PersonID)>
							<!--- 2013-09-24 NYB Case 436181 changed to linkSentText: --->
							<cfset message = linkSentText>
						<cfelse>
							<!--- Process Resend PW --->
							<cfparam name="frmUserName" default="">

							<cf_createUserNameAndPassword
								personid = "#CheckEmail.personID#"
								resolveDuplicates = false
								OverWritePassword = false
								OverWriteUserName = false
								UserName = "#frmUserName#"
							>
							<CFIF UserNameAndPassword.OK>
								<cfset application.com.email.sendEmail(sendEmail,CheckEmail.PersonID)>
							</CFIF>
						</cfif>

						<cfoutput>#linkSentText#</cfoutput>

					<!--- if an email template has not been passed and is ok to send the password --->
					<cfelseif sendEmail is "" and confirmSendEmail>

						<!--- Process Resend PW --->
						<cfparam name="frmUserName" default="">

						<cf_createUserNameAndPassword
							personid = "#CheckEmail.personID#"
							resolveDuplicates = false
							OverWritePassword = false
							OverWriteUserName = false
							UserName = "#frmUserName#"
						>

						<CFIF not UserNameAndPassword.OK >
							<!---If user name is not Unique --->
							<cfoutput>#linkSentText#</cfoutput>
						<CFELSE>

							<cfparam name="fromAddress" default="#application.com.settings.getSetting('emails.supportemailaddress')#">
							<!--- <cfparam name="thisApplicationName" default="#request.CurrentSite.Title#">
							<cfparam name="thisSiteName" default="#request.CurrentSite.Title#"> --->

							<cfif application.com.settings.getSetting("security.passwordGenerationMethod") eq "user">
								<!--- PKP : 2008-01-31 : CHECK TO SEE INTERNAL OR EXTERNAL USERS TO SELECT CORRECT EMAIL --->

								<CFIF request.relayCurrentUser.isInternal>
									<cfparam name="resendEmailTextID" default="SendInternalUserPassword">  <!---2008/07/09 GCC & NYF Standardised from ResendPasswordEmailInternal --->
								<CFELSE>
									<cfparam name="resendEmailTextID" default="ResendPasswordEmail"> <!---2008/07/09 GCC & NYF Standardised defaultChangePasswordLinkEmail --->
								</CFIF>

								<cfscript>
									//we need to ensure the password is changed at this point to 'disable' the account
									application.com.login.createUserNamePassword(personID=checkEmail.personID);
									sendResult = application.com.email.sendEmail(resendEmailTextID,checkEmail.personID);
								</cfscript>
								<cfoutput>
									<!--- nasty... --->
									<cfif sendResult eq "You have not set up an email with the textID #resendEmailTextID#" or sendResult eq "Email Failed">
										<cfset relocate="false">
										#htmleditformat(sendResult)#
									<cfelse>
										<!--- 2013-09-24 NYB Case 436181 changed to linkSentText: --->
										<cfset message = linkSentText>
										<!--- NJH 2012/10/04 - put this cfif around the output as otherwise the login screen gets the message twice
											If we're going to relocate anyways, then there is not much sense in output this here.
										--->
										<cfif not relocate>
											<cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
										</cfif>
									</cfif>
								</cfoutput>

							<cfelse>
								<CF_MAIL TO="#UserNameAndPassword.email#"
										BCC="#application.com.settings.getSetting('emailAddresses.copyallemailstothisaddress')#"
										FROM="#fromAddress#"
										SUBJECT="Phr_incentiveUserNameEmailsubjectresendPassword"
										type="html">
										<cf_translate>
											<p>Phr_incentiveUserNameEmailBodyResendPassword</p>


											<p>
											Phr_LoginScreen_Username: #UserNameAndPassword.username#<BR>
											Phr_LoginScreen_Password: #UserNameAndPassword.password#
											</p>

											<p>
											<!--- WAB 2009/01/20 mod here URL root not defined on Sony, replaced with currentSite.Domainandroot--->
										<A HREF="#request.currentSite.Domainandroot#/Index.cfm">#request.CurrentSite.Title#</A></p>
										</cf_translate>
									</CF_MAIL>

									<!--- 2013-09-24 NYB Case 436181 changed to linkSentText: --->
									<cfoutput>#linkSentText#</cfoutput>

							</cfif>
						</CFIF>

					<!--- if the organisation is not approved (confirmSendEmail is false) --->
					<cfelse>
						<cfoutput><p>#linkSentText#</p></cfoutput>

					</cfif>

					<CFIF not request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    <CFIF UserType is "External"> --->

						<cfif structKeyExists(FORM,"URLRequested")>
							<cfoutput>
								<SCRIPT type="text/javascript">
									window.location.href = '/?#jsStringFormat(FORM.URLRequested)#';
								</script>
							</cfoutput>
						</cfif>
					</CFIF>
				<!--- found multiple users at this email address --->
				<CFELSEIF CheckEmail.recordcount GT 1>
					<!--- 2013-09-24 NYB Case 436181 changed to linkSentText: --->
					<cfset message = linkSentText>
				<!--- if user not found --->
				<CFELSE>
					<!--- 2013-09-24 NYB Case 436181 changed to linkSentText: --->
					<cfset message = linkSentText>
					<cfif not suppressOutput>
						<cfoutput><p>#application.com.security.sanitiseHTML(message)#</p></cfoutput>
					</cfif>
				</CFIF>
			</CFIF>

	</cfif>
</cfif>