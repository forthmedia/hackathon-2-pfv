<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			conditionalPhrase.cfm
Author:				SWJ
Date started:		2005-08-22

Description:		The idea of this relayTag is to provide a method to show alternative
					phrases based on a logic check.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/08/14			NJH			Case 445248 - Changed the delimiters for the validvalues from #application.delim1# to |. The application.delim1 var was not getting evaluated.


Possible enhancements:

Allow multiple conditions and lists of vars effectively a loop of decisions

decide what options to support

 --->

<cf_param label="Condition Type" name="conditionType" required="yes" validValues="userInAUserGroup|Current user in one of User Group(s) below,customCondition|Custom condition" showNull="True" reloadOnChange="true"/>
<cf_param label="Condition Parameters" name="ConditionParameters" required="yes" condition="conditionType is 'userInAUserGroup'" validValues="cfc:com.relayUserGroup.getUserGroupsForSelect(userGroupTypes='visibility,external')" value="usergroupid" display="name" multiple="true" />
<cf_param label="Condition Parameters" name="ConditionParameters" required="yes" condition="conditionType is 'customCondition'"/>
<cf_param label="Text if condition is true" name="firstPhrase" required="yes" condition="conditionType is not ''"/>
<cf_param label="Text if condition is false" name="secondPhrase" required="yes" condition="conditionType is not ''"/>


<cfif not isDefined("conditionType")>
	<p>You must provide the parameter conditionType to this tag</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("ConditionParameters")><!--- probably should be optional defaulting to true --->
	<p>You must provide the parameter parameters to this tag</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("firstPhrase")>
	<p>You must provide the parameter firstPhrase to this tag</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("secondPhrase")>
	<p>You must provide the parameter secondPhrase to this tag</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfset result = false>

<cfswitch expression = "#conditionType#">
	<cfcase value="userInAUserGroup">
		<cfloop index = "userGroup" list = "#ConditionParameters#">
			<cfif listFindNoCase (request.relayCurrentUser.usergroups,usergroup) is not 0>
				<cfset result = true>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfcase>
	<cfcase value="customCondition">
		<cfset result = evaluate(ConditionParameters)>
	</cfcase>
</cfswitch>

<cfif result>
	<cfoutput>#htmleditformat(firstPhrase)#</cfoutput>
<cfelseif secondPhrase is not "">   <!--- WAB added check for "" - sometimes only want phrase to show on true condition --->
	<cfoutput>#htmleditformat(secondPhrase)#</cfoutput>
</cfif>