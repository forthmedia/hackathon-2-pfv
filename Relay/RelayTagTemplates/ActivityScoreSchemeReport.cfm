<cf_param label="Scheme" name="SchemeID" required="true" validValues="func:com.activityScore.getActiveSchemesForValidValues()" display="title" value="scoreRuleGroupingID"/>
<cf_param label="Time Period To Show Data For" name="timePeriod" default="allTime" required="true" validValues="func:com.activityScore.getScoreDisplayOptionsForValidValues()" display="display" value="data" />

<cfscript>
	//as this is highly specific to this tag it is calculated here rjt
	
	scheme=application.com.activityScore.getScoreRuleGrouping(schemeID).recordSet;
	
	if (scheme.cappingPeriodTypeTextID == "allTime"){
		timePeriod="allTime";
	}
	
	switch(timePeriod){
		case "allTime":
			startDate_exclusive=CreateDateTime(1970,1,1,0,0,0);
			end_inclusive=now();
			nextCapReset=application.com.activityScore.getTimeCappingPeriodContainingDate(scoreRuleGroupingID=SchemeID, date=now()).end_inclusive;
			break;
		case "Current":
			cappingPeriod=application.com.activityScore.getTimeCappingPeriodContainingDate(scoreRuleGroupingID=SchemeID, date=now());
			startDate_exclusive=cappingPeriod.start_exclusive;
			end_inclusive=cappingPeriod.end_inclusive;
			nextCapReset=end_inclusive;
			break;
		case "Previous":
			cappingPeriodCurrent=application.com.activityScore.getTimeCappingPeriodContainingDate(scoreRuleGroupingID=SchemeID, date=now());
			cappingPeriodPrevious=application.com.activityScore.getTimeCappingPeriodContainingDate(scoreRuleGroupingID=SchemeID, date=DateAdd("d", -1,cappingPeriodCurrent.start_exclusive));
			startDate_exclusive=cappingPeriodPrevious.start_exclusive;
			end_inclusive=cappingPeriodPrevious.end_inclusive;
			nextCapReset=end_inclusive;
			break;
		default:
			throw (message="Time period #timePeriod# is unexpected");	
			
	}
	
	scheme=application.com.activityScore.getScoreRuleGrouping(schemeID).recordSet;
	
	entityTypeIDToUse=scheme.entityTypeID;
	
	if (entityTypeIDToUse EQ application.entityTypeID.person){
		entityID=request.relayCurrentUser.personID;
	}else if( entityTypeIDToUse EQ application.entityTypeID.location){
		entityID=request.relayCurrentUser.locationID;
	}else if(  entityTypeIDToUse EQ application.entityTypeID.organisation){
		entityID=request.relayCurrentUser.organisationID;
	}else{ 
		throw(message="#entityTypeID# is not a supported entity type ID");
	}
	
	scoreReport=application.com.activityScore.getScoresForEntityInSchemeByRule(entityID=entityID,entityTypeID=entityTypeIDToUse,schemeID=SchemeID,startDate_exclusive=startDate_exclusive, endDate_inclusive=end_inclusive,includeCurrentRulesEvenIfEmpty="true");

</cfscript>

<cfquery dbtype="query" name="sumQoQ">
	select sum(CurrentScore) as totalScore
	from scoreReport
</cfquery>	

<cfoutput>
	<div class="container">
	  <div class="row">
	     <div class="col-md-6 col-sm-12">
			<h3>#scheme.TITLE_TRANSLATION#</h3>
			<h4>phr_total: <cfif sumQoQ.totalScore EQ "">0<cfelse>#sumQoQ.totalScore#</cfif></h4>
		 </div>
		<div class="col-md-6 col-sm-12">
			<cfif timePeriod EQ "Current" OR timePeriod EQ "Previous">
				<p>phr_scoreCalculatedFrom: #DateFormat(startDate_exclusive,"dd-mmm-yyyy")#</p>
			</cfif>
			<cfif timePeriod EQ "Previous">
				<p>phr_scoreCalculatedTo: #DateFormat(end_inclusive,"dd-mmm-yyyy")#</p>
			</cfif>
			<cfif (timePeriod EQ "Current" OR  timePeriod EQ "AllTime") AND scheme.cappingPeriodTypeTextID NEQ "AllTime">
				<p>phr_NextLimitResetDate: #DateFormat(nextCapReset,"dd-mmm-yyyy")#</p>
			</cfif>
		</div>
	  </div>
	</div>
    <CF_tableFromQueryObject queryObject="#scoreReport#" 
	showTheseColumns="SCORERULENAME,activityTypeDescription,ACTIVITIESCOMPLETED,pointsPerActivity,CURRENTSCORE,scoreCap"
	ColumnHeadingList="score_scoreRuleGrouping_scoreRuleTitle,Activity,score_numberOfActivities,score_scorePerActivity,score_score,score_scoringCap"
	ColumnTranslationPrefix="phr_"
	HidePageControls="no"
	useInclude = "false"
	numRowsPerPage="400"
	columnTranslation="true">
</cfoutput>


