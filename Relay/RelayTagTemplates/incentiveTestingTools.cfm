<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			incentiveTestingTools.cfm	
Author:				AH
Date started:		2004-09-07
	
Description:		This is designed for resetting accounts during testing.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfscript>
	if(isDefined("URL.resetCompanyAccount")){
		comQry = application.com.commonQueries;
		comQry.personID = request.relayCurrentUser.personid;
		comQry.dataSource = application.siteDataSource;
		comQry.getPersonDetails();
		application.com.relayIncentive.deletePointsFromCompanyAccount(comQry.PersonOrgID);
	}
	else if(isDefined("URL.resetAccount")){
		application.com.relayIncentive.deletePointsFromAccount(request.relayCurrentUser.personid);
	}
	else if(isDefined("URL.unregister")){
		application.com.relayIncentive.unregisterUser(request.relayCurrentUser.personid);
	}
	else if(isDefined("URL.username")){
		userID = application.com.commonQueries.getPersonID(form.username);
		if(len(userID)){
			pNumber = application.com.commonQueries.getP(variables.userID);
		}
		else{
			pNumber = "";
		}
	}
	else if(structKeyExists(form, "statusid")  ){
		// session.
	}
</cfscript>

<cfif isDefined("URL.resetCompanyAccount")>
	<SCRIPT type="text/javascript">
		alert("Your Incentive company account was successfully reset!");
	</script>
</cfif>

<cfif isDefined("URL.resetAccount")>
	<SCRIPT type="text/javascript">
		alert("Your Incentive account was successfully reset!");
	</script>
</cfif>

<cfif isDefined("URL.unregister")>
	<SCRIPT type="text/javascript">
		alert("Your Incentive account was unregistered!");
	</script>
</cfif>

<cfoutput>
RESET THE ACCOUNT OF THE ORGANISATION YOU BELONG TO:<BR>
<a href="/?eid=#url.eid#&resetCompanyAccount=1">Reset My Company Account to zero</a>
<p></p>
RESET YOUR OWN PERSONAL ACCOUNT:<BR>
<a href="/?eid=#url.eid#&resetAccount=1">Reset My Own Account to zero</a>
<p></p>
THIS LINK WILL ONLY UNREGISTER YOUR OWN ACCOUNT:<BR>
<a href="/?eid=#url.eid#&unregister=1">Unregister Me</a>

<p></p>
YOU CAN FIND OUT ANYONE'S P NUMBER:
<form name="form1" action="/?eid=#url.eid#"  method="post">
Enter Username here: <input name="username" type="text">
</form>
<a href="javascript:document.form1.submit();">And click here to obtain the user's p number</a>
<p></p>
<cfif structKeyExists(form, "username") >
	<cfif len(variables.pNumber)>
		<font color="##FF0000">THE P NUMBER IS: #htmleditformat(variables.pNumber)#</font>
	<cfelse>
		<font color="##FF0000">We are sorry, but the passed username is not found in the database.</font>
	</cfif>
</cfif>

</cfoutput>


<cfinclude template="/relay/relayTagTemplates/portalTestingTools.cfm">
