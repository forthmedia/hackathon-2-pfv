<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			incentivePointsClaim.cfm
Author:				SWJ
Date started:		2004-07-16
	
Description:		This has been introduced so that incentive points claim can be included 
					both from incentive directory and as a Relay Tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 <cfscript>
 	//WORK OUT PERSONID
	if (structKeyExists(url, "personID")){
		personID = url.personID;
	}
	if (structKeyExists(form, "personID")){
		personID = form.personID;
	}
	//work out through cookie
	if(not structKeyExists(variables, "personID")){
		personID = request.relayCurrentUser.Personid;
	}
 </cfscript>
 
 
 <!---first check if the person is registered with the program---->
 <cfset personRegistration = application.com.relayIncentive.isPersonRegistered(personID)>

<!--- NJH 2010/08/16 LID 3846 was incentiveIni... but params includes the INI file and methodToClaimIncentivePoints is param'd in the params file --->
<!--- Now returns structure incentiveSettings. --->
<cf_include template="\incentive\incentiveParams.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\incentiveINI.cfm")>
	<!--- incentiveINI can be used to over-ride default 
		global application variables and params defined in incentiveParams.cfm --->
	<cfinclude template="/code/cftemplates/incentiveINI.cfm">
</cfif> --->

<!--- 2007/12/10 - GCC - Trying view access to statement for non points administrator --->
<cfparam name="permitNonRegisteredUsersToViewStatement" default="false">
<!--- <cfif not personRegistration.recordCount>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfexit method="exittemplate">
</cfif> --->
<cfif not personRegistration.recordCount and not permitNonRegisteredUsersToViewStatement>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfexit method="exittemplate">
</cfif>

<cfset hideRightBorder = "yes">

<!--- <cfinclude template="/incentive/application .cfm"> --->
<cfif incentiveSettings.methodToClaimIncentivePoints eq "product">
	<cfinclude template="/incentive/incentivePointsClaimByProductFamily.cfm">
<cfelseif incentiveSettings.methodToClaimIncentivePoints eq "amount">
	<cfinclude template="/incentive/incentivePointsClaimByAmount.cfm">
</cfif>
