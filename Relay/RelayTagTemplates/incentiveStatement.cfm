<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			incentiveStatement.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 <!---first check if the person is registered with the program---->
<cfset personRegistration = application.com.relayIncentive.isPersonRegistered(request.relayCurrentUser.personid)>
<cfif not personRegistration.recordCount>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfexit method="exittemplate">
</cfif>

<cfset hiderightBorder = "yes">
<cfset url.statementType = "company">

<cfif structKeyExists(url,"includeTemplate") and not compareNoCase(URL.includeTemplate, "sd")>
	<cfinclude template="/incentive/statementDetail.cfm">
<cfelse>
	<cfinclude template="/incentive/statementInclude.cfm">
</cfif>