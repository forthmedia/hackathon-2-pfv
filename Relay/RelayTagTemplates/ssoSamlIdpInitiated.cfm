<cfif structKeyExists(form,"rejectSendDataToThirdParty")>
	<cflocation url="/" AddToken = false >
</cfif>

<cfscript>
	validatedDataTransferConsentGiven=false;
	//Process any data transfer authoriations before the page starts properly
	if (structKeyExists(form,"acceptSendDataToThirdParty")){
		//we have recieved a command to authorise the client for this user, we need to make sure the consent token checks out
		//this prevents form scope manipulation being used to
		consentToken=createObject("component","singleSignOn.SAML.IDP.SAMLConsentToken").init(token=form.consentTokenString);
		consentToken.pullFromDatabase();
		if (consentToken.isValidToken() AND consentToken.getPersonID() eq request.relayCurrentUser.personID){
			attributes.relayState=consentToken.getRelayState();
			attributes.ServiceProvider=consentToken.getClientIdentifier();
			attributes.inResponseTo=consentToken.getInResponseTo();

			validatedDataTransferConsentGiven=true;

		}else{
			//we'll end up back on the consent page, log the issue
			errorStruct=structNew();
			errorStruct.requestedPerson=request.relayCurrentUser.personID;
			errorStruct.authorisedPerson=consentToken.getPersonID();
			errorStruct.explanation="acceptSendDataToThirdParty was present in the form scope but the consent token was not valid or for the right user. This could be a case of the user going back to an old page or could be a (failed) hacking attempt.";

			application.com.errorHandler.recordRelayError_Warning(type="SAMLAuthenticationError",Severity="error",WarningStructure=errorStruct);


		}
	}

</cfscript>


<cf_param label="Relay State" name="ATTRIBUTES.relayState" default=""/>

<cf_param label="ServiceProvider" name="ATTRIBUTES.ServiceProvider" validValues="func:singleSignOn.SAML.IDP.ServiceProviderManager.getValidValuesForServiceProvider()" display="displayValue" value="dataValue"/>

<cfparam name="ATTRIBUTES.inResponseTo" default=""/> <!--- For when this is used as part of the non idpInitiated SSO --->



<cfscript>

	//we need to establish consent for the specified person's data to be passed to a third party


	certificateManager=new singleSignOn.SAML.IDP.CertificateManager();

	serviceProvider=new singleSignOn.SAML.IDP.SAMLServiceProvider(serviceProviderIdentifier=trim(ATTRIBUTES.serviceProvider));
	existsInDatabase=serviceProvider.pullFromDatabase();
</cfscript>

<cfif not existsInDatabase>
	<CFset errorID=application.com.errorHandler.recordRelayError_Warning(Severity="ERROR", message="Service provider (#ATTRIBUTES.serviceProvider#) did not exist in the database")>

	<p>phr_saml_Third_Party_Not_Recognised <cfoutput>(#errorID#)</cfoutput></p>

<cfelse>

	<cfscript>
		if (validatedDataTransferConsentGiven){
			serviceProvider.setAuthorisedToAccessDataForUser(request.relayCurrentUser.personID);
		}
	</cfscript>
	
	
	<cfset requireConsent=application.com.settings.getSetting('SAML.IDP.requireUserConsentForDataTransfer')>
	
	<cfif not requireConsent or serviceProvider.isAuthorisedToAccessDataForUser(request.relayCurrentUser.personID)>
	
		<cfscript>
		assertionConsumerURI=serviceProvider.getAssertionConsumerURL();
		serviceProviderEntityID=serviceProvider.getServiceProviderIdentifier();
	
	
		nameID=serviceProvider.getNameIDForAPerson(request.relayCurrentUser.personID);
		issuerString=cgi.SERVER_NAME;
		audianceURI=serviceProviderEntityID;
		password = certificateManager.getCertificateStorePassword();
		certificateAliasName = serviceProvider.getCertificateToUse();
		fileName =certificateManager.getCertificateStoreLocation();
		allowedTimeMismatch_minutes=application.com.settings.getSetting('SAML.IDP.allowedTimeMismatch_minutes');
	
		certificateExpiry=certificateManager.getCertificateExpiry(certificateAliasName);
	
		if (DateDiff("d", now(), certificateExpiry)<application.com.settings.getSetting("SAML.IDP.CertificateExpiryWarning_days")){
			serviceProvider.sendWarningAboutCertificateExpireyIfNeccissary();
		}
	
		//no attributes
		assertionAttributes=serviceProvider.getAttributesForAPerson(request.relayCurrentUser.personID);
		//assertionAttributes=createObject("java", "java.util.HashMap").init();
		//assertionAttributes.put("SomeKey", "someName");
	
	
		creator=new singleSignOn.SAML.IDP.SignedResponseCreator(issuerString,fileName,password,certificateAliasName);
	
	
		response=creator.createSignedResponse(ATTRIBUTES.inResponseTo, nameID, assertionConsumerURI, audianceURI, allowedTimeMismatch_minutes,assertionAttributes);
	
		</cfscript>
	
	
	
	
		<cfoutput>
			<cfset type=structKeyExists(url,"test")?"text":"hidden">
	
			<form action="#assertionConsumerURI#" method="post" id="SAML_SP_POST">
				<input type="#type#" name="SAMLResponse" value="#toBase64(response)#"/>
				<input type="#type#" name="RelayState" value="#ATTRIBUTES.relayState#">
			</form>
		</cfoutput>
	
	
		<cfif NOT structKeyExists(url,"test")>
			<script>
				jQuery(document).ready( function () {
					document.forms['SAML_SP_POST'].submit();
				});
			</script>
		</cfif>
	
	<cfelse>
		<!--- We attempt to obtain permission --->
		<cfscript>
			consentToken=createObject("component","singleSignOn.SAML.IDP.SAMLConsentToken").init(validity_seconds=600);
			consentToken.autogenerateToken();
			consentToken.setPersonID(request.relayCurrentUser.personID);
			consentToken.setClientIdentifier(ATTRIBUTES.ServiceProvider);
			consentToken.setRelayState(ATTRIBUTES.relayState);
			consentToken.setInResponseTo(ATTRIBUTES.inResponseTo);
			consentToken.persistToDatabase();
		</cfscript>
	
		<cfset agreeToDataTransferPage=application.com.settings.getSetting("OAuth.AuthenticationServer.authorisePage")>
		<cfset clientName=serviceProvider.getServiceProviderName()>
		<cfif agreeToDataTransferPage eq "">
			<cfmodule template="/singleSignOn/common/authenticate_authorise.cfm" clientName="#serviceProvider.getServiceProviderName()#" consentToken="#consentToken#" >
		<cfelse>
			<cfmodule template="#agreeToDataTransferPage#" clientName="#serviceProvider.getServiceProviderName()#" consentToken="#consentToken#" >
		</cfif>
	
	</cfif>
</cfif>