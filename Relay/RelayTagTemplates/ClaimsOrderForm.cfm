<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ClaimsOrderForm.cfm
Author:				SSS
Date started:		2008/07/23
Description:			

Core code claims that are made

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
23-July-2008			SSS			once a claim is first submitted a email should be sent to all approvers that are able to approve the claim
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Possible enhancements:


 --->
<cfparam name="template" default="/code/cftemplates/claimsOrderForm.cfm">

<cfif fileexists("#application.UserFilesAbsolutePath##replace(template,"/","\","ALL")#")>
	<cfinclude template="/code/cftemplates/ordersINI.cfm">
	<cf_param label="Go To Element ID" name="gotoeid" default=""/>
	<cfparam name="variables.orderid" default="newrecord">
	<cfparam name="form.orderid" default="newrecord">
	<cfparam name="form.invsitename" default="">
	<cfparam name="form.invaddress1" default="">
	<cfparam name="form.invaddress2" default="">
	<cfparam name="form.invaddress3" default="">
	<cfparam name="form.invaddress4" default="">
	<cfparam name="form.invaddress5" default="">
	<cfparam name="form.invpostalcode" default="">
	<cfparam name="form.invcontact" default="">
	<cfparam name="form.invcontactphone" default="">
	<cfparam name="form.invcontactfax" default="">
	<cfparam name="form.invcontactemail" default="">
	<cfparam name="form.delcontact" default="">
	<cfparam name="form.ordernotes" default="">
	<cfparam name="form.ordernumber" default="">
	<cfparam name="form.orderdate" default="">
	<cfparam name="form.shippeddate" default="">
	<cfparam name="form.delsitename" default="">
	<cfparam name="form.deladdress1" default="">
	<cfparam name="form.deladdress2" default="">
	<cfparam name="form.deladdress3" default="">
	<cfparam name="form.deladdress4" default="">
	<cfparam name="form.deladdress5" default="">
	<cfparam name="form.delpostalcode" default="">
	<cfparam name="form.frmpromoid" default="1">
	<cfparam name="form.frmpersonid" default="#request.relaycurrentuser.personid#">
	<cfparam name="form.frmTask" default="">
	<cfparam name="promoid" default="1">
	<cfparam name="variables.frmTask" default="New">
	<cfparam name="variables.invsitename" default="">
	<cfparam name="variables.invaddress1" default="">
	<cfparam name="variables.invaddress2" default="">
	<cfparam name="variables.invaddress3" default="">
	<cfparam name="variables.invaddress4" default="">
	<cfparam name="variables.invaddress5" default="">
	<cfparam name="variables.invpostalcode" default="">
	<cfparam name="variables.invCountryID" default="#request.relaycurrentuser.countryid#">
	<cfparam name="variables.invcontact" default="">
	<cfparam name="variables.contactphone" default="">
	<cfparam name="variables.contactfax" default="">
	<cfparam name="variables.contactemail" default="">
	<cfparam name="variables.delcontact" default="">
	<cfparam name="variables.ordernotes" default="">
	<cfparam name="variables.ordernumber" default="">
	<cfparam name="variables.orderdate" default="">
	<cfparam name="variables.shippeddate" default="">
	<cfparam name="variables.delsitename" default="">
	<cfparam name="variables.deladdress1" default="">
	<cfparam name="variables.deladdress2" default="">
	<cfparam name="variables.deladdress3" default="">
	<cfparam name="variables.deladdress4" default="">
	<cfparam name="variables.deladdress5" default="">
	<cfparam name="variables.delpostalcode" default="">
	<cfparam name="variables.delCountryID" default="#request.relaycurrentuser.countryid#">
	<cfparam name="variables.frmpersonid" default="#request.relaycurrentuser.personid#">
	<cfparam name="variables.frmpromoid" default="1">
	<cfparam name="variables.productserialid" default="0">
	<cfparam name="variables.productgroupid" default="0">
	<cfparam name="variables.TheirOrderNumber" default="">
	<cfparam name="variables.approverNotes" default="">
	<cfparam name="form.orderstatus" default="2001">
	<cfparam name="variables.orderstatus" default="">
	<cfparam name="promoid" default="1">
	<cfparam name="form.currentOrderID" default="0">
	<cfparam name="variables.currentOrderID" default="0">
	<cfparam name="url.currentOrderID" default="0">

	<cfif form.currentOrderID is not 0>
		<cfset variables.currentOrderID=form.currentOrderID>
	</cfif>
	<cfif url.currentOrderID is not 0>
		<cfset variables.currentOrderID=url.currentOrderID>
	</cfif>
	
	<cfif isdefined("url.eid")>
		<cfset variables.formprocesspage="/?eid=#url.eid#">
	<cfelse>
		<cfset variables.formprocesspage="viewclaim.cfm">
	</cfif>
	
	<cfif isdefined("gotoeid")>
		<cfset variables.goto="/?eid=#gotoeid#">
	<cfelse>
		<cfset variables.goto="reportclaimsapproval.cfm">
	</cfif>
	
	<cfif form.frmTask is "New">
		<!--- <cfset var qry_get_PersonDetails = "">
		<cfset var qry_add_order = ""> --->
		
		<CFSET freezedate=now()>
		<cfoutput><CFSET user="#request.relaycurrentuser.personid#"></cfoutput>
		
		<!--- Get persondetails including the Country ID needed for OrderItems and the product table --->
		<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
			Select 	l.*, p.*, c.countrydescription as country, c.countryid
			from person as p, location as l, country as c
			where
			p.personid =  <cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >  and p.locationid=l.locationid
			and	l.countryid=c.countryid
		</CFQUERY>
		
		<cfif isdate(form.orderdate) is false>
			<cfset variables.orderdate=createodbcdatetime(now())>
		<cfelse>
			<cfset variables.orderdate=form.orderdate>
		</cfif>
		<cfif isdate(form.shippeddate) is false>
			<cfset variables.shippeddate=createodbcdatetime(now())>
		<cfelse>
			<cfset variables.shippeddate=form.shippeddate>
		</cfif>
		
		<cftransaction>
		
			<CFQUERY NAME="getOrderID" datasource="#application.sitedatasource#">
				Select MAX(orderID+1) as orderID
				from Orders
			</CFQUERY>
			<cfif getOrderID.orderID is "">
				<cfset variables.orderid=1>
			<cfelse>
				<cfset variables.orderid=getOrderID.orderID>
			</cfif>
		
			<CFQUERY NAME="qry_add_order" datasource="#application.sitedatasource#"> 
				INSERT Into Orders
				(OrderID,OrderNumber,Orderdate,LocationID,PersonID,OrderStatus, CreatedBy,Created,LastUpdatedBy,LastUpdated,delcontact,delsitename,<CFLOOP index="i" from="1" to="5">deladdress#i#,</CFLOOP>delpostalcode,delcountryid,contactphone,contactfax,contactemail,invcontact,invsitename,<CFLOOP index="i" from="1" to="5">invaddress#i#,</CFLOOP>invpostalcode,invcountryid,promoid,ordertype,ordernotes,shippeddate)
				VALUES (<cf_queryparam value="#variables.orderID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#form.ordernumber#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#variables.orderdate#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#request.relaycurrentuser.locationid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#form.orderstatus#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#user#" CFSQLTYPE="cf_sql_float" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#user#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#form.delcontact#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delsitename#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delAddress1#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delAddress2#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delAddress3#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delAddress4#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delAddress5#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delpostalcode#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.delcountryid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#form.contactphone#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.contactfax#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.contactemail#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invcontact#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invsitename#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invAddress1#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invAddress2#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invAddress3#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invAddress4#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invAddress5#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invpostalcode#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.invcountryid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#form.frmPromoId#" CFSQLTYPE="CF_SQL_VARCHAR" >,'c',<cf_queryparam value="#form.ordernotes#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#variables.shippeddate#" CFSQLTYPE="cf_sql_timestamp" >)
			</CFQUERY>
			
		</cftransaction>
		
		<cfloop index="i" list="#form.orderitemIds#">
			<cfif i is not 0 or i is not "">
				<CFQUERY NAME="getItemDetails" datasource="#application.sitedatasource#">			
					SELECT * FROM Product
					WHERE productid =  <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				<cfset quantity=evaluate("form.quantity#i#")>
				<CFQUERY NAME="insertOrderItems" datasource="#application.sitedatasource#">			 
					Insert Into OrderItem(orderid,sku,quantity,lastupdatedby,lastupdated,createdby,created,Active,ProductID,ParentSKUGroup)
					Values (<cf_queryparam value="#getOrderID.orderID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getitemdetails.sku#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#quantity#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,1,<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getitemdetails.SKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				</CFQUERY>
			</cfif>
		</cfloop>
		<cfset form.newrecord=getOrderID.orderID>
		<cf_updatedata>
		<!--- 2008/09/30 GCC moved out to a client specific file --->
		<cfif fileexists("#application.paths.code#\cftemplates\PostClaimOrderSubmit.cfm")>
			<cfinclude template="/code/cftemplates/PostClaimOrderSubmit.cfm">
		</cfif>
		<cflocation url="#variables.goto#"addToken="false">
	<cfelseif form.frmTask is "Update">
		
		<CFSET freezedate=now()>
		<cfoutput><CFSET user="#request.relaycurrentuser.personid#"></cfoutput>
		
		<cfif isdate(form.orderdate) is false>
			<cfset variables.orderdate=createodbcdatetime(now())>
		<cfelse>
			<cfset variables.orderdate=createodbcdatetime(CreateDate(year(form.orderdate), month(form.orderdate), day(form.orderdate)))>
		</cfif>
		<cfif isdate(form.shippeddate) is false>
			<cfset variables.shippeddate=createodbcdatetime(now())>
		<cfelse>
			<cfset variables.shippeddate=createodbcdatetime(CreateDate(year(form.shippeddate), month(form.shippeddate), day(form.shippeddate)))>
		</cfif>
		
		<CFQUERY NAME="qry_update_order" datasource="#application.sitedatasource#"> 
				Update Orders
				Set OrderNumber =  <cf_queryparam value="#form.ordernumber#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				Orderdate =  <cf_queryparam value="#variables.orderdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				shippeddate =  <cf_queryparam value="#variables.shippeddate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				LocationID=#request.relaycurrentuser.locationid#,
				PersonID =  <cf_queryparam value="#form.frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				OrderStatus =  <cf_queryparam value="#form.orderstatus#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				LastUpdatedBy =  <cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				LastUpdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				delcontact =  <cf_queryparam value="#form.delcontact#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				delsitename =  <cf_queryparam value="#form.delsitename#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<CFLOOP index="i" from="1" to="5">deladdress#i#='#evaluate("form.delAddress#i#")#',</CFLOOP>
				delpostalcode =  <cf_queryparam value="#form.delpostalcode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				delcountryid =  <cf_queryparam value="#form.delcountryid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				contactphone =  <cf_queryparam value="#form.contactphone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				contactfax =  <cf_queryparam value="#form.contactfax#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				contactemail =  <cf_queryparam value="#form.contactemail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				invcontact =  <cf_queryparam value="#form.invcontact#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				invsitename =  <cf_queryparam value="#form.invsitename#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<CFLOOP index="i" from="1" to="5">invaddress#i#='#evaluate("form.invAddress#i#")#',</CFLOOP>
				invpostalcode =  <cf_queryparam value="#form.invpostalcode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				invcountryid =  <cf_queryparam value="#form.invcountryid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				promoid =  <cf_queryparam value="#form.frmPromoId#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ordernotes =  <cf_queryparam value="#form.ordernotes#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				Where OrderID =  <cf_queryparam value="#form.currentOrderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<cfif ListSort(form.existingproducts, "numeric") is not ListSort(form.OrderItemIDs, "numeric")>
			<cfset productsToAdd="">
			<cfset productsToDelete="">
			<cfloop list="#form.OrderItemIDs#" index="i">
				<cfif listfind(form.existingproducts,  i )>
					<cfset productsToAdd=listappend(productsToAdd,i)>
				</cfif>
			</cfloop>
			<cfloop list="#form.existingproducts#" index="j">
				<cfif listfind(form.OrderItemIDs,  j )>
					<cfset productsToDelete=listappend(productsToDelete,j)>
				</cfif>
			</cfloop>
			
			<cfloop list="#productsToAdd#" index="k">
				<CFQUERY NAME="getItemDetails" datasource="#application.sitedatasource#">			
					SELECT * FROM Product
					WHERE productid =  <cf_queryparam value="#k#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				<cfset quantity=evaluate("form.quantity#k#")>
				<CFQUERY NAME="insertOrderItems" datasource="#application.sitedatasource#">			 
					Insert Into OrderItem(orderid,sku,quantity,lastupdatedby,lastupdated,createdby,created,Active,ProductID,ParentSKUGroup)
					Values (<cf_queryparam value="#form.currentOrderID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getitemdetails.sku#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#quantity#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#user#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,1,<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#getitemdetails.SKUGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				</CFQUERY>
			</cfloop>
			
			<cfloop list="#productsToDelete#" index="l">
				<CFQUERY NAME="qry_update_order" datasource="#application.sitedatasource#"> 
					delete from orderitem
					where OrderID =  <cf_queryparam value="#form.currentOrderID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
					productid =  <cf_queryparam value="#l#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			</cfloop>
			
		</cfif>
		<cf_updatedata>
		
		<cflocation url="#variables.goto#"addToken="false">
	</cfif>
	<cfif variables.currentOrderID is not 0>

		<cfset variables.orderID=variables.currentOrderID>
		
		<cfquery name="qry_get_order" datasource="#application.sitedatasource#">
			select * from orders where orderid =  <cf_queryparam value="#variables.orderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="qry_get_orderitems" datasource="#application.sitedatasource#">
			select orderitem.quantity, orderitem.orderID, orderitem.productid, product.description from orderitem,product 
			where orderitem.orderid =  <cf_queryparam value="#variables.orderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and orderitem.productid=product.productid
		</cfquery>
		
		<cfoutput query="qry_get_order">
		
			<cfif listfind(EditableOrderStatusList,orderstatus)>
				<cfset variables.frmTask="Update">
			<cfelse>
				<cfset variables.frmTask="View">
			</cfif>
			<cfset variables.contactname="#invsitename#">
			<cfset variables.contactphone="#invaddress1#">
			<cfset variables.contactfax="#invaddress2#">
			<cfset variables.invsitename="#invsitename#">
			<cfset variables.invaddress1="#invaddress1#">
			<cfset variables.invaddress2="#invaddress2#">
			<cfset variables.invaddress3="#invaddress3#">
			<cfset variables.invaddress4="#invaddress4#">
			<cfset variables.invaddress5="#invaddress5#">
			<cfset variables.invpostalcode="#invpostalcode#">
			<cfset variables.invCountryID="#invcountryid#">
			<cfset variables.invcontact="#invcontact#">
			<cfset variables.contactphone="#contactphone#">
			<cfset variables.contactfax="#contactfax#">
			<cfset variables.contactemail="#contactemail#">
			<cfset variables.delcontact="#delcontact#">
			<cfset variables.ordernotes="#ordernotes#">
			<cfset variables.ordernumber="#ordernumber#">
			<cfset variables.orderdate="#dateformat(orderdate,"DD/MM/YYYY")#">
			<cfset variables.shippeddate="#dateformat(shippeddate,"DD/MM/YYYY")#">
			<cfset variables.delsitename="#delsitename#">
			<cfset variables.deladdress1="#deladdress1#">
			<cfset variables.deladdress2="#deladdress2#">
			<cfset variables.deladdress3="#deladdress3#">
			<cfset variables.deladdress4="#deladdress4#">
			<cfset variables.deladdress5="#deladdress5#">
			<cfset variables.delpostalcode="#delpostalcode#">
			<cfset variables.delCountryID="#delcountryid#">
			<cfset variables.frmpersonid="#personid#">
			<cfset variables.frmpromoid="#promoid#">
			<cfset variables.currentOrderID="#orderid#">
			<cfset variables.orderstatus="#orderstatus#">
			<cfset "variables.orders_lastupdated_#orderid#"="">
			<cfset variables.TheirOrderNumber="#TheirOrderNumber#">
			<cfset variables.approverNotes="#approverNotes#">
		</cfoutput>
		<cfset variables.OrderItemsArray=ArrayNew(2)>
		<cfset variables.OrderItemsArrayCounter=1>
		
		<cfoutput query="qry_get_orderitems">
			<cfif variables.OrderItemsArrayCounter is 1>
				<CFQUERY NAME="getProdDetails" datasource="#application.sitedatasource#">			
					SELECT Product.productgroupid, productgroupname FROM Product,ProductGroup
					WHERE Product.productgroupid=ProductGroup.productgroupid and productid =  <cf_queryparam value="#productid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
				<cfset variables.productgroupid=getProdDetails.productgroupid>
				<cfset variables.productserialid=getProdDetails.productgroupname>
			</cfif>
			<cfscript>
				variables.OrderItemsArray[#variables.OrderItemsArrayCounter#][1]=description;
				variables.OrderItemsArray[#variables.OrderItemsArrayCounter#][2]=productid;
				variables.OrderItemsArray[#variables.OrderItemsArrayCounter#][3]=quantity;
				variables.OrderItemsArrayCounter=variables.OrderItemsArrayCounter+1;
			</cfscript>
			
		</cfoutput>
	<cfelse>
		<cfset variables.orderstatus="#NewQuoteStatus#">
	</cfif>
	
	<cfinclude template="/code/cftemplates/ClaimsOrderForm.cfm">
<cfelse>
	phr_error_claims_notemplate
</cfif>
	

<!--- processing --->

