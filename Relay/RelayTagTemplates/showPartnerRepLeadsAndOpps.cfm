<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			showPartnerRepLeadsAndOpps.cfm
Author:				SWJ
Date started:		2003-01-30

Description:		This is a Relay tag that calls reportActiveOpportunities

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/06/24   WAB    RACE
2015/09/30	NJH		PROD2015-35 - added param for column filters

Possible enhancements:


 --->
<!--- any of the above variables can be over ridden in the INI file --->

<cf_param label="Show Opportunity Types" name="showOppTypes" type="string" default="" validValues="select oppTypeTextId as value, oppType as display from oppType order by display" nullText="All"/>
<!--- 2012-06-19 RMB P-LEX070 Added opportunity_id and oppTypeText to Show columns validValues list--->
<!--- /* P-PSI001 - RMB - 2013-06-21 - Added 'OppStatus' AND 'OppType' AND 'partner_Sales_Person' */ --->
<cf_param label="Show Columns" name="showColumns" default="Detail,Last_Updated,Stage" validValues="opportunity_id,Account,Stage,Detail,Expected_Close_Date,Overall_Customer_Budget,Calculated_Budget,Last_Updated,Account_Manager,Currency,StatusDescription,OppCloseMonth,OppCloseYear,oppTypeText,OppStatus,OppType,partner_Sales_Person" multiple="true" displayAs="twoSelects" allowSort="true"/>
<cf_param label="Filter by" name="filterColumns" default="Stage" validValues="Account,Stage,Partner_Sales_Person,Account_Manager" multiple="true" displayAs="twoSelects"/>
<cf_param name="dateFilter" default="Expected_Close_Date" validValues="Expected_Close_Date,Last_Updated"/>
<cf_param label="Translate Columns" name="columnTranslation" type="boolean" default="Yes"/>
<!--- 2014/10/10	YMA		Partner cloud  demo project - requirement to filter opp list to only show forecast opportunities --->
<cf_param label="Only show forecast opportunities" name="filterToForecast" type="boolean" default="false"/>

<!--- 2015-01-14 ACPK CORE-973 Added showAllPartnerLeads parameter--->
<cf_param label="Show all partner leads" name="showAllPartnerLeads" type="boolean" default="false"/>

<cfset leadOppPartnerShowTheseColumns = showColumns>
<cfset filterSelectFieldList = filterColumns>
<cf_includeWithCheckPermissionAndIncludes template = "/leadManager/LeadAndOpplistPartner.cfm">