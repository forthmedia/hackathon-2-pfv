<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			incentiveSelectPrizes.cfm	
Author:				SWJ
Date started:		2004-07-16
	
Description:		This has been introduced so that order items can be included 
					as a Relay Tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/11/23		WAB		remove a cfabort and replace with an IF Allows full borderset to show even if a "No rights" message is displayed
2012-08-23		IH		Case 426222 change to cf_param

Possible enhancements:

orders/orderitems.cfm?ShowProductGroup=2&ThisProductGroup=Promotional%20Items&PromoID=IncentiveCatalogue
 --->
 

 <!---first check if the person is registered with the program---->
<cfset personRegistration = application.com.relayIncentive.isPersonRegistered(request.relayCurrentUser.personID)>

<cf_include template="\code\cftemplates\incentiveINI.cfm" checkIfExists="true">

<!--- 2007/12/10 - GCC - Trying view access to catalogue --->
<cfparam name="portalViewOnly" default="false">
<cf_param name="permitNonRegisteredUsersToViewCatalogue" label="Permit Non Registered Users To View Catalogue" type="boolean" default="No" />
<cf_param name="promoid" label="Promo ID" default="incentivePrizes" validValues=" select PromoID as value, promoName as display from promotion order by promoName" />
<cf_param name="spendIncentivePointsType" label="Spend Incentive Points Type" default="Company" validvalues="Personal,Company" />

<cfparam name="goToEID" default="#request.currentElement.id#">

<cfset OKToInclude = true>
<cfif not personRegistration.recordCount and permitNonRegisteredUsersToViewCatalogue>
	<cfset portalViewOnly = "True">
<cfelseif not personRegistration.recordCount>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfset OKToInclude = false>
</cfif>

<cfif OKToInclude>
	<!--- Version using the security system --->
	<cfset fileToInclude = "/relay/orders/orderItemsInclude.cfm">
	
	<cfif structKeyExists(form,"frmaction")>
		<cfif form.frmaction eq "OrderInfo">
			<cfset fileToInclude = "/relay/orders/orderItemsTask.cfm">
		<cfelseif form.frmaction eq "DisplayOrder">
			<cfset fileToInclude = "/relay/orders/displayordertask.cfm">
		</cfif>
	</cfif>
	<cf_IncludeWithCheckPermissionAndIncludes template = "#fileToInclude#">	 
</cfif>	

