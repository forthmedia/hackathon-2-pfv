<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		branchLinkList.cfm
Author:			YMA
Date created:	2016-11-07

	Objective - This displays a list of children of the specified topBranchID

	Syntax	  -	Simply place <RELAY_BRANCHLINKLIST> in the calling element.

Amendment History:

Date 		Initials 	What was changed

Enhancement still to do:

--->

<!--- Listing Definition --->
<cf_param label="Page ID to show results for (if left blank the current page will be used)" name="topBranchID" type="numeric" default=""/>
<cf_param label="Limit number of results returned" name="numChildren" type="numeric" default="99"/>
<cf_param label="Position of text in relation to the image" name="positionText" type="string" default="right" validvalues="Right,Left"/> <!--- place the text to the right of the image, or below the image --->
<cf_param label="Number of columns to display" name="defaultLinkListColumns" type="numeric" default="2" validValues="1,2,3,4,6,12"/>
<cf_param label="Listing sort order" name="sortOrder" default="sortorder asc" validValues="sortorder asc,sortorder desc,name asc,name desc,lastupdated asc,lastupdated desc" multiple="false"/>
<cf_param label="Show page summary" name="elementLinkListShowSummary" type="boolean" default="No"/>
<!--- image definition --->
<cf_param label="Show page image" name="elementLinkListShowImage" type="boolean" default="No"/>
<cf_param label="Image size (px)" name="linkImageSize" type="numeric" default="80"/>

<!--- style definition --->
<cf_param name="linkListDivID" label="Div ID (leave the default unless you have defined a special CSS class)" default="BranchLinkListContent"/>

<!--- Parameters used in old elementLinkList Relaytag, retained for Backwards compatibility. --->
<cfparam name="linkImageType" type="string" default="linkImage">
<cfparam name="request.thumbImageType" default="jpg">

<!--- hidden from Relaytag editor.  Not sure why we would want  to display more than 1 generation. --->
<cfparam name="numberOfGenerationsOfChildren" type="numeric" default="1" />

<cfif topBranchID eq 0>
	<cfset topBranchID = request.currentElement.id>
</cfif>

<cfset GetChildren = application.com.relayElementTree.getSecondaryNavigationBranchFromTree(
						elementTree = request.currentElementTree,
						topOfBranchID = topBranchID,
						numberofgenerations = numberofgenerationsOfChildren +1,
						sortOrder = sortOrder
						)>
						<!---  note that the +1 is required because numberofgenerations includes the parent item as a generation, whereas numberofgenerationsOfChildren does not--->
						
<!--- Set the bootstrap columns.  For mobile devices we show only one column --->
<cfset linkListElementColWidthMD = 12/defaultLinkListColumns>
<cfset linkListElementColWidthXS = 12>

<cfoutput>

	<div id="#linkListDivID#" class="active parent-page dynamic-homepage-container">
		<div class="row section">
			<cfloop query="GetChildren" startrow=1 endrow="#numChildren#">

				<cfset elementCustomParameters = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = GetChildren.PARAMETERS,delimiter = ",") >

				<cfif GetChildren.parameters[currentrow] contains "hideFromLinkList=1">
					<cfset GetChildren.recordCount = GetChildren.recordCount - 1>
				<cfelse>
					<!--- if the element has been set to isExternalFile then treat the link as an external URL --->
					<cfif isexternalfile eq 1>
						<cfset link = url>
						<cfset theTarget = "_blank">
					<cfelse>
						<cfset link = "/?eid=" & id>
						<cfset theTarget = "_self">
					</cfif>
					<div class="col-xs-12 col-sm-#linkListElementColWidthMD# col-md-#linkListElementColWidthMD# col-lg-#linkListElementColWidthMD#">
						<div class="dynamic-box type2">
							<cfif elementLinkListShowImage>
								<div class="image pull-#positionText eq "right"?"right":"left"#">
									<!--- YMA check if uploaded file in original format exists first, then use thumb images secondary. --->
									<cfset linkImagerelative = "content\elementLinkImages\" & GetChildren.ID & "_orignalFile.png">
									<cfset linkImagePath=application.userfilesabsolutepath & linkImagerelative>
									<cfif not fileexists(linkImagePath)>
										<cfset linkImagerelative = "content\elementLinkImages\" & GetChildren.ID & "_orignalFile.jpg">
										<cfset linkImagePath=application.userfilesabsolutepath & linkImagerelative>
									</cfif>
									<cfif not fileexists(linkImagePath)>
										<cfset linkImagerelative = "content\elementLinkImages\" & GetChildren.ID & "_orignalFile.jpeg">
										<cfset linkImagePath=application.userfilesabsolutepath & linkImagerelative>
									</cfif>
									<cfif not fileexists(linkImagePath)>
										<cfset linkImagerelative = "content\elementLinkImages\#linkImageType#-" & GetChildren.ID & "-orig." & request.thumbImageType>
										<cfset linkImagePath=application.userfilesabsolutepath & linkImagerelative>
									</cfif>
									<cfif fileexists(linkImagePath)>
										<img src="#linkImagerelative#" border=0 width="#linkImageSize#" />
									<cfelse>
										<img src="/images/misc/blank.gif" height=1 border=0 width="#linkImageSize#" />
									</cfif>
								</div>
							</cfif>
							<div class="title">
								<h2>
									<a href="#link#">
										phr_headline_element_#htmleditformat(GetChildren.ID)#
									</a>
								</h2>
								<cfif elementLinkListShowSummary>
									<p class="summary">
										phr_summary_element_#htmleditformat(GetChildren.ID)#
									</p>
								</cfif>
							</div>
						</div>
					</div>
				</cfif>
			</cfloop>
		</div>
	</div>
</cfoutput>