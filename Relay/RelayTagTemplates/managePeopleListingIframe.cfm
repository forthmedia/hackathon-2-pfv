<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			managePeopleListingIFrame.cfm
Author:				Unknown
Date started:		Unknown

Description:		This is used to provide a page for listing people records

Amendment History:

Date 				Initials 	What was changed
2006-04-25			AJC			Add mailto:email on the list
2007-02-05			WAB 		added check to make sure not used by the unknown user
2008-05-15			NYF 		removed 'active=1' filter from the GetPeople query
2008-07-17			AJC			Issue 808: Show All Records not working on CompanyAdmin
2008-04-06			NYF		Sophos - added ability to filter people shown by Flags or Flaggroups using parameters:
						filterByFlagGroups or filterByFlags
2010-03-02			PPB 		P-TND096 - reinstated 'active=1' filter on the query
2011-07-19			PPB			LEN024 add switch showPersonEditForm to allow profile screen to be used instead of hard-coded form; default="true" Lenovo="false" for manage my colleagues
2013-01-09 			PPB 		Case 432648 changed "mailto:=" to changed "mailto:"
2014-11-13			AXA 		Case 442623 added ApprovalStatus to list of valid values in columns to show
2015-11-03			VSN 		Case 446429 Removing addition of user edit iFrame if the hideFieldsControl option is set to false
2016/06/17			NJH			JIRA PROD2016-425 - filter people by locationID if relayware is 'location aware'.
Possible enhancements:


 --->

<!---START: 2013-05-09	MS		P-LEN062 -CR087 - Add extra columns to the listing so we add a hook to access a customised version of this file --->
<!---
	NJH 2016/06/17 - moved this functionality to calling template.. Don't think it's needed anyway as Lenovo are no longer with us and an option has been provided to pass in column list.
	<cfif fileexists("#application.paths.code#\CFTemplates\customManagePeopleListingIFrame.cfm")>
	<cfinclude template ="/Code/CFTemplates/customManagePeopleListingIFrame.cfm">
<cfelse> --->


<!--- 2009-06-16 GCC LID2367 ensure known user and logged in--->
<cfif not request.relayCurrentUser.isUnknownUser and request.relayCurrentUser.isLoggedIn>


	<cfparam name="sortOrder" type="string" default="lastname">
	<cfparam name="numRowsPerPage" default="200">
	<cfparam name="filterByFlagGroups" default="">
	<cfparam name="filterByFlags" default="">
	<cfparam name="showPersonEditForm" default="true" type="boolean">		<!--- 2011-07-19 PPB LEN024 added showPersonEditForm --->
	<cfparam name="hideFieldsControl" default="" type="string"/>

	<cfparam name="showTheseColumns" default="salutation,firstname,lastname,emailAddress"> <!--- 2014-11-13	AXA Case 442623 default columns to display in listing--->
	<cfif len(trim(manageMyColleaguesListingsColumnsToShow)) GT 0>
		<cfset showTheseColumns = manageMyColleaguesListingsColumnsToShow /> <!--- 2014-11-13 AXA if user selected custom columns to display use those instead--->
	</cfif>

	<cfset filterByEntityTypeUniqueKey = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"locationID":"organisationID">

	<CFQUERY NAME="GetPeople" datasource="#application.siteDataSource#">
		Select p.personid, p.firstname, p.lastname, p.email as emailAddress, p.salutation, '#hideFieldsControl#' AS 'hideFieldsControl','#showPersonEditForm#' as showPersonEditForm
		<cfif filterByFlagGroups neq "">, 'phr_' + flag.Namephrasetextid AS ApprovalStatus</cfif>
		from person as p
		<cfif filterByFlagGroups neq "" or filterByFlags neq "">
			INNER JOIN BooleanFlagData b WITH (NOLOCK) ON p.personID = b.entityID
			INNER JOIN Flag WITH (NOLOCK) ON b.FlagID = Flag.FlagID
			<cfif filterByFlags neq "">
				AND Flag.FlagTextID in (<cf_queryparam value="#filterByFlags#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>
			<cfif filterByFlagGroups neq "">
				INNER JOIN FlagGroup WITH (NOLOCK) ON Flag.FlagGroupID = FlagGroup.FlagGroupID AND FlagGroup.FlagGroupTextID in (<cf_queryparam value="#filterByFlagGroups#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			</cfif>
		</cfif>
		where
			p.#filterByEntityTypeUniqueKey# = #request.relayCurrentUser[filterByEntityTypeUniqueKey]#
		<cfif not request.relaycurrentuser.isInternal>AND p.personid <>  <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>
	    and p.active = 1
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>

<!---VSN Case 446429 Begin--->
	<cfset peopleQueryKeyList = "firstname,emailAddress">
	<cfset peopleQueryUrlList = "/relayTagTemplates/managePeopleEditScreenIframe.cfm?personid=|showPersonEditForm=|hideFieldsControl=,mailto:">
	<cfif showPersonEditForm eq "No">
		<cfset peopleQueryKeyList="emailAddress">
		<cfset peopleQueryUrlList="mailto:">
	</cfif>
<!---VSN Case 446429 End--->

	<cfif IsQuery(GetPeople)>
		<CF_tableFromQueryObject queryObject="#GetPeople#"

		HidePageControls="no"
		useInclude = "false"

		keyColumnList="#peopleQueryKeyList#"<!---VSN Case 446429 using paramater now--->
		keyColumnURLList="#peopleQueryUrlList#"
		keyColumnKeyList="personid|showPersonEditForm|hideFieldsControl,emailAddress"
		showTheseColumns="#showTheseColumns#"
		columnTranslation="true"
		ColumnTranslationPrefix="phr_Ext_"
		numRowsPerPage ="#numRowsPerPage#"
		sortorder="#sortOrder#">
	</cfif>

</cfif>

<!--- </cfif> --->
