<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ordersProductGroups.cfm	
Author:				SWJ
Date started:		2003-04-24
	
Description:		This tag calls orders/catagoryList for a given promoid.
					It simply includes orders/CategoryList.cfm and displays a list of 
					product Groups.  Is can be used to initiate an ordering
					process as 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

NOTES:

2005-04-13	AJC	have added showallproducts default param to categorylist. if set to 1 a link
				appears to show all products

 --->
<cfparam name="showallproducts" default="0">
<cfif not isDefined("promoid")> 
	<p><strong>Missing parameter promoid.</strong>  <br>You must define the parameter "promoid" in the paramter field of this page.  
	<br><br>Do this by editing the page in the Relay Content Editor and adding the string
	promoid=validPromoID.  <br><br>A list of valid PromoIDs can be found by selection 
	Promotions in the eCommerce Section of the Relay internal application.</p> 
	<cfexit method="EXITTEMPLATE">
</cfif>

<TABLE width="70%">
      <tr>
		<td align="center">
			<!--- 
			WAB 2010-02-16 RACE removed to make way for new security system
			<cfinclude template="/orders/application .cfm">
			<cfinclude template="/orders/CategoryList.cfm">
			--->
				
				<cf_IncludeWithCheckPermissionAndIncludes template="/orders/CategoryList.cfm">	 <!--- PPB 2010-02-26 corrected syntax error FOR LIGHTHOUSE 3089 --->	
		</td>
	  </tr>
</table>

