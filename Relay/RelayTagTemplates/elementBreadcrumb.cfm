<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementBreadCrumb.cfm
Author:			WAB
Date started:		2006/05/08

Description: Outputs a breadcrumb trail to the current element

Usage:

Amendment History:

This code was originally in elementTemplate.cfm.  Moved here when elementTemplateV2 was developed.
Still called from elementTemplateV2 , but could in theory be used as a relayTag (note that it is a custom tag so would need the custom tag functions of  executerelaytag to have been released)

Date (DD-MMM-YYYY)	Initials 	What was changed



Enhancements still to do:



 --->

				<!--- new breadcrumb system which handled the Sony1 left nav.  Basically the top node in the left nav now points to the home page, so cam be included in the breadcrumb --->
				<!--- NJH 2008/07/15 Bug Fix T-10 Issue 731. Check if request.currentElement.node is not an empty string as this will be the case when the user doesn't have access to the node --->
				<cfquery name="getBreadCrumb" dbtype="query">
					select * from request.currentElementTree
					where (node in (#request.currentElement.parentIDList#) <cfif request.currentElement.node neq "">or node = #request.currentElement.node#</cfif>)
					<cfif request.currentsite.elementTreeDef.hideTheseEIDsFromBreadcrumb is not "">and (node not in (#request.currentsite.elementTreeDef.hideTheseEIDsFromBreadcrumb#))</cfif>
				</cfquery>
					<!--- request.hideTheseEIDsFromBreadcrumb is a comma list of eid's that you can add to
							relayINI.cfm which defines those EIDs that you do not want to show in the breadcrumb --->

				<cfoutput>
				<cfparam name="attributes.breadcrumbDivID" default="breadcrumb-trail">
				<cfparam name="attributes.breadcrumbShowUL" default="yes">
				<cfparam name="attributes.breadcrumbSeperator" default="">

				<div id="#attributes.breadcrumbDivID#">
					<cfif attributes.breadcrumbShowUL EQ "yes">
						<ul>
							<cfloop query="getBreadCrumb">
								<cfif currentRow eq (getBreadCrumb.recordCount)>
									<li><a href="#resolvedhref#" class="endLink">#htmleditformat(headline)#</a></li>
								<cfelse>
									<li><a href="#resolvedhref#">#htmleditformat(headline)#</a></li>
								</cfif>
							</cfloop>
						</ul>
					<cfelse>
						<cfloop query="getBreadCrumb">
							<cfif currentRow eq (getBreadCrumb.recordCount)>
								<a href="#resolvedhref#">#htmleditformat(headline)#</a>
							<cfelse>
								<a href="#resolvedhref#">#htmleditformat(headline)#</a> #htmleditformat(attributes.breadcrumbSeperator)#
							</cfif>
						</cfloop>
					</cfif>
				</div>
				</cfoutput>


				<!---
				some old breadcrumb code

						<cfquery name="getBreadCrumbTrail" datasource="#application.SiteDataSource#">
									exec getElementParents @ElementID=#elementid# , @topid = #request.breadcrumbTrailTopEID#, @personid=#request.relayCurrentUser.personID#, @permission = 1, @countryid = '#request.relayCurrentUser.countryid#'
								</cfquery>

								<cfoutput>
								<div id="breadcrumb-trail">
									<!--- request.hideTheseEIDsFromBreadcrumb is a comma list of eid's that you can add to
											relayINI.cfm which defines those EIDs that you do not want to show in the breadcrumb --->
									<ul>
										<li><a href="/?etid=#request.HomePageETID#">phr_home</a></li>
										<cfloop query="getBreadCrumbTrail"><!--- and getBreadCrumbTrail.elementID neq variables.elementid --->
											<cfif currentRow gt 1
												and listfindNoCase(request.hideTheseEIDsFromBreadcrumb,getBreadCrumbTrail.elementID) eq 0>
												<cfif getBreadCrumbTrail.currentRow eq (getBreadCrumbTrail.recordCount)><!--- recordcount - 1 --->
													<li><a href="/?eid=#getBreadCrumbTrail.elementID#" class="endLink">phr_headline_element_#getBreadCrumbTrail.elementID#</a></li>
												<cfelse>
													<li><a href="/?eid=#getBreadCrumbTrail.elementID#">phr_headline_element_#getBreadCrumbTrail.elementID#</a></li>
												</cfif>
											</cfif>
										</cfloop>
									</ul>
								</div>
								</cfoutput>
				 --->

