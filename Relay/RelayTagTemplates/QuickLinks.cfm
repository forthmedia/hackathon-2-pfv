<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	QuickLinks.cfm - Derived from Sony's SubItems.cfm
	
	NJH 2007/03/13 - added recent searches to the display
	NJH 2007/05/01 - added linkImage param so that we can set a link to be an image rather than text
				   - added maxRows param to determine how many intelligent links to bring back. Brings back all if set to 0
WAB 2008/05/12   Added elementtextID to all the queries so that links can be done by elementtextID if it is not blank
	NYB 2009/02/02 - Trend Support bug 1714 - added 'with (nolock)' to query 
 2012/03/01 PPB Case 426925 duplicate rows in the What's New 
--->

<cfset loginbullet = '<img src="/content/sony1/sony1V1Images/redBullet.gif" alt="" border="0" align="top">'>

<cfparam name="quickLinksClass" default="quickLinks">
<cfparam name="displayType" default="list">
<cfparam name="variables.eid" default="#request.currentElement.node#">   <!--- WAB 2006-01-31 changed this from default = 0, and put in variables scope. #request.currentElement.node# now a better way of knowing what the current element is, and guaranteed to be numeric  --->
<cfparam name="elementBranchRootID" default="#variables.eid#">
<cfparam name="header" default="">
<cfparam name="headerPhrase" default="">
<cfparam name="lang" default="en">
<cfparam name="listLinkTarget" default="_self">
<cfparam name="moreLinkURL" default="">
<cfparam name="moreLinkTarget" default="_self">
<cfparam name="rows" default="all">
<cfparam name="showMore" default="0">
<cfparam name="dataGetMethod" default="standard">
<cfparam name="elementLinkTypeTextID" default="Element">
<cfparam name="elementLinkParams" default="">
<cfparam name="elementLinkSchema" default="">
<cfparam name="useIcons" default="false">
<cfparam name="iconImage" default="/content/MiscImages/shim.gif">
<cfparam name="iconClass" default="#dataGetMethod#Icon">
<cfparam name="elementFlagID" default="WhatsNewElementFlag">
<cfparam name="elementParentEID" default="#request.currentElement.id#">
<cfparam name="linkImage" type="string" default=""> <!--- NJH 2007/04/30 --->
<cfparam name="linkImageClass" type="string" default=""> <!--- NJH 2007/04/30 --->
<cfparam name="maxRows" type="numeric" default=0> <!--- NJH 2007/04/30 --->

<cfset elementRightsList = valuelist(request.currentElementTree.node)>

<cfswitch expression="#dataGetMethod#">
	<cfcase value = "linkBlock">
		<cfscript>
				qGetElements = application.com.relayElementTree.getBranchFromTree (elementtree = request.currentElementTree, topofbranchID = elementBranchRootID);
		</cfscript>
	</cfcase>
	<cfcase value = "intelligentLinks">
		<!--- 2005/06/17 - GCC - Still an issue with auto 2 way return links to mull...  --->
			<!--- points to remember 
			1. Always assumes the unioned section is linking back to an element only.
			
			 --->
			 <!--- NJH 2007/04/30 added top #maxRows# to be able to get 1 or more promotions 
WAB 2008/05/12   added elementtextID
--->
			<cfquery name = "qGetElements" datasource="#application.sitedatasource#">
			SELECT 
			<cfif maxRows neq 0>top #maxRows#</cfif>
			* FROM (
				SELECT     ltrim(URL) as URL,isExternalFile, elementLinkID, hostElementID AS node, element.elementTextID, lastUpdated, elementLinkSchema.elementLinkSchema, elementLinkParam1, elementLinkParam2, elementLinkParam3, additionalDescription, elementLinkTypeTextID
				FROM         elementLink LEFT OUTER JOIN
				elementLinkSchema ON elementLink.elementLinkSchemaID = elementLinkSchema.elementLinkSchemaID
				inner join element on elementLink.hostElementID = element.id
				WHERE     (elementID =  <cf_queryparam value="#variables.eid#" CFSQLTYPE="CF_SQL_INTEGER" > ) and elementLinkTypeTextID <> 'Advert'
				AND hostElementID  in ( <cf_queryparam value="#elementRightsList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				UNION
				SELECT     ltrim(URL) as URL,isExternalFile, elementLinkID,elementID AS node,element.elementTextID, lastUpdated, elementLinkSchema.elementLinkSchema, elementLinkParam1, elementLinkParam2, elementLinkParam3, additionalDescription, 'Element' as elementLinkTypeTextID
				FROM         elementLink LEFT OUTER JOIN
				elementLinkSchema ON elementLink.elementLinkSchemaID = elementLinkSchema.elementLinkSchemaID
				inner join element on elementLink.elementID = element.id
				WHERE     (hostelementID =  <cf_queryparam value="#variables.eid#" CFSQLTYPE="CF_SQL_INTEGER" > ) and elementLinkTypeTextID <> 'Advert'
				AND elementID  in ( <cf_queryparam value="#elementRightsList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<cfif isdefined('productID') and productID eq "" and  isdefined('nodeID') and nodeID eq "">
				<!--- root of a tree - no links --->
					AND (entityID = 0)
				<cfelseif isdefined('productID') and productID neq "">
					AND (entityID =  <cf_queryparam value="#productID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				<cfelseif isdefined('nodeID') and nodeID neq "">
					AND (entityID =  <cf_queryparam value="#nodeID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</cfif>
			) as base
			order by lastUpdated desc <!--- NJH 2007/05/01 order by lastUpdated date of element - this may need to change in the future --->
			</cfquery>
	</cfcase>
	<cfcase value = "whatsNew">
		<cfset qGetElements = application.com.relayElementTree.getElementsFlaggedWithParticularFlag(elementtree = request.currentElementTree,flagid=#elementFlagID#,demoteVisitedElements = true)>
		
		<!--- START: 2012/03/01 PPB Case 426925 Lexmark was getting duplicate rows in the What's new list cos they can have the same page with different parents in the elementTree --->
		<cfquery name="qGetElements" dbtype="query">
			SELECT DISTINCT ID,CONTENTID,CONTENTIDASSTRING,HEADLINE,RESOLVEDHREF,ISEXTERNALFILE,URL,ELEMENTTEXTID,NODE 
			FROM qGetElements
			ORDER BY visited , overallweighting DESC  
			<!--- 
			getElementsFlaggedWithParticularFlag orders with a column "sortorder ASC" on the end but by using it re-introduces the duplicate rows; 
			if duplicates re-appear it is worth trying to remove "overallweighting" from the sort order in case the same issue applies;
			if change to the order are not acceptable then don't use a qoqs to remove the dups but perhaps skip thro the rows;
			because demoteVisitedElements=true is sent into getElementsFlaggedWithParticularFlag (above) then we can hardcode "visited" into the sortorder
			--->
		</cfquery>
		<!--- END: 2012/03/01 PPB Case 426925 --->
	</cfcase>
	<cfcase value = "Advert">
	<!---  WAB 2008/05/12   added elementtextID  --->
		<cfquery name = "qGetElements" datasource="#application.sitedatasource#">
			SELECT     0 AS isExternalFile, elementLinkID,elementLink.entityID AS linkImage, elementLink.hostElementID AS node, e.elementTextID, elementLinkSchema.elementLinkSchema, 
			                      elementLink.elementLinkParam1, elementLink.elementLinkParam2, elementLink.elementLinkParam3, elementLink.additionalDescription, 
			                      elementLink.elementLinkTypeTextID
			FROM         elementLink LEFT OUTER JOIN
			                      countryScope ON elementLink.entityID = countryScope.entityid AND countryScope.entity = N'element' LEFT OUTER JOIN
			                      elementLinkSchema ON elementLink.elementLinkSchemaID = elementLinkSchema.elementLinkSchemaID
						 INNER JOIN element e on e.id = elementlink.entityID
			WHERE     (elementLink.elementID =  <cf_queryparam value="#variables.eid#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (elementLink.elementLinkTypeTextID = 'Advert') AND (countryScope.countryid = #request.relaycurrentuser.content.showForCountryID# OR
			                      countryScope.countryid IS NULL)
								  and elementLink.hostElementID  in ( <cf_queryparam value="#elementRightsList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						and e.statusid = 4
		</cfquery>
	</cfcase>
	
	<!--- NJH 2007/03/13 - added recent searches--->
	<!--- NYB 2009/02/02 - bug 1714 - added with (nolock) --->
	<cfcase value="recentSearches">
		<cfquery name="qGetElements" datasource="#application.sitedatasource#">
			select 0 AS isExternalFile, a.searchstring from (
				select top 5 searchstring,max(searchdate) as searchDate
				from tracksearch  with (nolock)
				where searchtype='V' and personid=#request.relaycurrentuser.personid#
				and searchstring <>''
				group by searchstring
				order by max(searchdate) desc)a 
			order by searchstring
		</cfquery>	
	</cfcase>
	
	<cfcase value = "updateReminder">
		<cfif not isdefined("MessageBranchID")>
			You must specify MessageBranchID. It needs to be set to the ID of the element whose children hold the reminder messages.
			<CF_ABORT>
		</cfif>
		<cfif not isdefined("etID")>
			You must specify etID. This must be the element that holds the screen that requires updating.
			<CF_ABORT>
		</cfif>
		<cfscript>
			qGetElements = application.com.relayElementTree.getBranchFromTree (elementtree = request.currentElementTree, topofbranchID = MessageBranchID, returnTopOfBranch = false);
		</cfscript>
		<!--- revolves around messages in the element tree --->
		
		<cfset sessionString = "session.MessageRevolver" & MessageBranchID>
		<cfif isdefined('#evaluate(DE("session.MessageRevolver#MessageBranchID#"))#')>
			<cfset sessionVariable = #evaluate(sessionString)#>
		<cfelse>
			<cfif startPosition eq "any" and qGetElements.recordcount gt 1>
				<cfset sessionVariable = randrange(1,qGetElements.recordcount)>
			<cfelse>
				<cfset sessionVariable = startPosition>
			</cfif>			
		</cfif>
		
		<cflock timeout="1">
			<cfif sessionVariable gte qGetElements.recordcount>
				<cfset "session.MessageRevolver#MessageBranchID#" = 1>
			<cfelse>
				<cfset "session.MessageRevolver#MessageBranchID#" = sessionVariable + 1>
			</cfif>
			<cfset rowNumber = #evaluate(sessionString)#>
		</cflock>
		<!--- first/default - show the first advert visible by this user --->
		<cfif qGetElements.recordcount gt 0>
			<cfquery name="qGetElements" dbtype="query">
				select *
				from qGetElements
				where ContentID = #qGetElements.id[rowNumber]#
			</cfquery>
		</cfif>
	</cfcase>
	
	
	<cfdefaultcase>
		<cfscript>
			qGetElements = application.com.relayElementTree.getBranchFromTree (elementtree = request.currentElementTree, topofbranchID = elementBranchRootID);
		</cfscript>
	</cfdefaultcase>
</cfswitch> 

<cfif qGetElements.recordcount gt 0>
	<!--- do we show the More button? --->
	<cfif rows eq "All">
		<!--- 2005-05-05 SWJ modified showMore to hide the more button if all URL variable is set --->
		<cfset showMore = 0>
	<cfelse>
		<cfif qGetElements.recordcount gt rows>
			<cfset showMore = 1>
		<cfelse>
			<cfset showMore = 0>
		</cfif>
	</cfif>

	<!---strip underscores from header--->
	<!--- NJH 2007/03/12 commented this out as wasn't sure what why it was there and it's removing the '_' for translations --->
	<!--- <cfif len(header) gt 0>
		<cfset header = replace(#header#,"_","	","ALL")>
	</cfif> ---> 
	
	<!--- SWJ: 2005-06-24 added the HeaderPhrase logic to supercede the header --->
	<cfif len(headerPhrase) gt 0>
		<cfset header = headerPhrase>
	</cfif>

	<cfscript>
		//parameter to control how many subitems are displayed in list
		if(rows eq "all")
			localRows=qGetElements.recordcount;
		else
			localRows=rows+1;
	</cfscript>

	<cfinclude template="QuickLinks_Display.cfm">

</cfif>
