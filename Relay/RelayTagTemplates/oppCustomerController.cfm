<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		oppCustomerController.cfm

Amendment History:
2013-07-30	YMA	Fixed to use the oppTypeTextId as showOppTypes rather than the OppType which can be translated to cause errors
2014-03-10 PPB Case 439121 clicking Add New from within a CustomerController tab should create opp of the relevant OppType
2014-09-29	AXA Fixed tabs to display for Leads if it has been specified as an OppType
2014-11-04	YMA I believe we should be showing this for renewals not leads as leads show in customernewopportunities.cfm
2015-09-11	ACPK	PROD2015-56 Removed unused variables, passed showOppColumns into customerNewOpportunities.cfm as showColumns
--->



<!--- 2013-07-30	YMA	Fixed to use the oppTypeTextId as showOppTypes rather than the OppType which can be translated to cause errors --->
<cfquery name="showOppTypesDefaults" datasource="#application.sitedatasource#">
	select 'SalesHistory' as value, 'SalesHistory' as display union select oppTypeTextID as value, oppType as display from oppType union select 'Contacts' as value, 'Contacts' as display
</cfquery>

<cfparam name="orgID" default="0"/>
<cfparam name="message" default="">
<cf_param name="Height" default="500"/>
<cf_param name="Width" default="$('innerContentColumns').getDimensions().width-10"/>
<cfparam name="customerName" default="">
<cf_param label="Show 'Add New End Customer' Button" name="ShowAddNewEndCustButton" default="Yes" type="boolean"/>
<cf_param label="Show Tabs" name="showOppTypes" allowSort="true" multiple="true" displayAs="twoSelects" default="#ValueList(showOppTypesDefaults.value)#" validValues="select 'SalesHistory' as value, 'SalesHistory' as display union select oppTypeTextID as value, oppType as display from oppType union select 'Contacts' as value, 'Contacts' as display"/>

<cfset useOppProductTotals = application.com.settings.getSetting("leadManager.products.useOppProductTotals")>
<cf_param label="Show Opportunity Columns" name="showOppColumns" multiple="true" displayAs="twoSelects"  default="OpportunityDescription,TotalValue,ExpectedCloseDate,EditButton" validValues="expectedCloseDate,actualCloseDate,opportunityDescription,opportunityStage,currency,EditButton,closeMonth,closeYear,TotalValue" allowSort="true"/>

<cf_include template="/code/CFTemplates/opportunityINI.cfm" checkIfExists="true">

<cfparam name="session.customerOrgID" default="#orgID#">

<cfif structkeyexists(url,"orgid")>
	<cfset session.customerOrgID = url.orgID>
	<cfquery name="qCustomerName" datasource="#application.sitedatasource#">
		SELECT OrganisationName FROM organisation WHERE organisationID =  <cf_queryparam value="#session.customerOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset CustomerName = qCustomerName.organisationName>
</cfif>

<!--- START: 2012/08/09	MS		P-KAS002 	If the the argument showAddNewEndCustButton is not passed or set in the RELAY_OPPCUSTOMERLIST RelayTag we default it to true for backward compatibility using cf_param--->
<cfif structkeyexists(url,"SHOWADDNEWENDCUSTBUTTON")>
	<cfset SHOWADDNEWENDCUSTBUTTON=url.SHOWADDNEWENDCUSTBUTTON>
</cfif>
<!--- END: 2012/08/09	MS	P-KAS002 --->
<cfoutput><h2><span class="customerActivity">phr_Customer_Activity</span> <span class="customerName">#customerName#</span></h2>

<!---PJP 19/12/2012: CASE 431613: Added in extra param for start tab --->
<cf_param label="Start on tab" name="starttab" default="SalesHistory" type="string" validValues="select 'SalesHistory' as value, 'SalesHistory' as display union select oppType as value, oppTypeTextID as display from oppType union select 'Contacts' as value, 'Contacts' as display"/>

<cfif structKeyExists(url,"starttab")>
	<cfset starttab = url.starttab>
</cfif>

<cfif listFind(showOppTypes,starttab,",") eq 0>
	<cfoutput>#application.com.relayUI.message(message="You have chosen a hidden tab as the starting tab.  Please re-configure this page and choose a valid tab to remove this error.",type="error")#</cfoutput>
</cfif>

<!--- 2015-09-11	ACPK	PROD2015-56 Removed unused variables --->
<cfset messageDisplayed = false>

<!--- Nav tabs --->
<ul class="nav nav-tabs" role="tablist">
<cfloop list=#showOppTypes# index="oppType">
	<li<cfif starttab eq oppType> class="active"</cfif>><a href="###oppType#" role="tab" data-toggle="tab">phr_opp_CustomerController_#oppType#</a></li>
</cfloop>
</ul>

<!--- Tab panes --->
<div class="tab-content custCntrlTabs">
	<cfloop list=#showOppTypes# index="oppType">
		<div class="tab-pane<cfif starttab eq oppType> active</cfif>" id="#oppType#">
			<img src="/images/icons/loading.gif">
			<cfif not listfind("SalesHistory,Contacts",oppType)>
				<cfset messageDisplayed = true>
			</cfif>
		</div>
	</cfloop>
</div>
<script>
	var loadedPages = {};

	jQuery('.nav-tabs a').click(function (e) {
		e.preventDefault()
		var targetFrame = jQuery(this).attr("href")
		var target = targetFrame.substr(1);

		if(typeof(loadedPages[target]) == 'undefined'){
			if(target == "SalesHistory"){
				jQuery(targetFrame).load("/templates/customerSalesHistory.cfm?inIFrame=1")
			}else if (target == "Contacts"){
				jQuery(targetFrame).load("/templates/customerContacts.cfm?inIFrame=1")
			}else if (target == 'Renewal'){ // 2014-09-29	AXA Fixed tabs to display for Leads if it has been specified as an OppType
											// 2014-11-04	YMA I believe we should be showing this for renewals not leads as leads show in customernewopportunities.cfm
				jQuery(targetFrame).load("/templates/customerRenewals.cfm?inIFrame=1")
			}else{ //2015-09-11	ACPK	PROD2015-56 Passed showOppColumns into customerNewOpportunities.cfm as showColumns
				jQuery(targetFrame).load('/templates/customerNewOpportunities.cfm?inIFrame=1#iif(messageDisplayed neq true,de("&message=#message#"),de(""))#&ShowAddNewEndCustButton=#ShowAddNewEndCustButton#&oppType=' + target + '&showColumns=#showOppColumns#')
			}
			loadedPages[target] = true;
		}
	})

	jQuery(document).ready(function(){
		if('#starttab#' == "SalesHistory"){
			jQuery('###starttab#').load("/templates/customerSalesHistory.cfm?inIFrame=1")
		}else if ('#starttab#' == "Contacts"){
			jQuery('###starttab#').load("/templates/customerContacts.cfm?inIFrame=1")
		}else if ('#starttab#' == 'Renewal'){ 	// 2014-09-29	AXA Fixed tabs to display for Leads if it has been specified as an OppType
												// 2014-11-04	YMA I believe we should be showing this for renewals not leads as leads show in customernewopportunities.cfm
			jQuery('###starttab#').load("/templates/customerRenewals.cfm?inIFrame=1")
		}else{ //2015-09-11	ACPK	PROD2015-56 Passed showOppColumns into customerNewOpportunities.cfm as showColumns
			jQuery('###starttab#').load('/templates/customerNewOpportunities.cfm?inIFrame=1#iif(messageDisplayed neq true,de("&message=#message#"),de(""))#&ShowAddNewEndCustButton=#ShowAddNewEndCustButton#&oppType=#starttab#&showColumns=#showOppColumns#')
		}
		loadedPages['#starttab#'] = true;
	});
</script>

</cfoutput>
