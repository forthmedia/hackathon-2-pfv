<!--- �Relayware. All Rights Reserved 2014 --->
<!--- IF you are here because of <RELAY_SUGGESTIONS> depending on the client it may be calling FeedbackEmailForm.cfm instead --->
<!--- 
File name:			Suggestions.cfm	
Author:				???
Date created:		??/??/??
	
Description:		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-03-24			AJC			Genericised the tag. Now accepts various parameters listed below and
								also uses the phrases table. Also bulit in the ability for an option 
								in the drop down box to go to a specific email address.
2005-02-16			GCC			Extended to email multiple people "-" delimited within the pipe delimitation
2006-09-22			GCC			Fixed to ensure blank fields and unknown company for 404 users
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2010/11/09 			PPB 		LID4605: added the closing "/" on a cf_translate tag; change also done to FeedbackEmailFormV2.cfm  

Enhancement still to do:
Suggest building ability to add more fields to the form by passing a parameter list.
--->
 
<!--- Accepted Parameters
NAME			TYPE	VALUES		DESCRIPTION	
formname		string	any/null	used to prefix phrases so as multiple instances of the tag can
									be used in a site.
subjectPrefix	string	any/null	Prefix of the subject of the email that gets sent
selectOptions	int		1+			Number of options in the select box. The name of option is called as
									phr_#formname#Option#i# where i is a number produced from a loop with the max
									being the selectOptions value
EmailPersonIDs	string	list		A list of personIDs to relate to the options in the select box.
									Allows 1 or a number matching the selectOptions value e.g. if selectOptions
									is 3 the the list can be either 1 or 3 personIDs not 2 or 4+. If 1 is entered
									then all selections go to that email. If no personID is entered then
									the email goes to application.supportEmailName.

END Accepted Parameters
--->
<cf_translate>
<cfparam name="formname" default="StdForm">
<cfparam name="SubjectPrefix" default="New Message">
<cfparam name="selectoptions" default="1">
<cfparam name="EmailPersonIDs" default="">

<cfif isDefined("form.update") and form.update is "true">

	
	<!--- If the number of personids for the form is eq to the number of options or is lte 1 --->
	<cfif listlen(EmailPersonIDs,"|") eq selectoptions or listlen(EmailPersonIDs,"|") lte 1>
		<cfoutput>
			<H1>phr_#htmleditformat(formname)#Feedbacksubmitted</H1>
			<p>phr_#htmleditformat(formname)#Thankyouforyourinput</p>
		</cfoutput>
		<!--- If the list of personids is greater than 0 --->
		<cfif listlen(EmailPersonIDs,"|") gt 0>
			<!--- if there are multiple emails addresses. i.e. 1 email address for each dropdown option --->
			<cfif listlen(EmailPersonIDs,"|") gt 1>
				<!--- set the personid of the relevant selected option --->
				<cfset variables.receiverPersonID=listgetat(EmailPersonIDs,form.frmSelectOption,"|")>
			<cfelseif listlen(EmailPersonIDs,"|") eq 1>
			<!--- if 1 person gets all emails from this form --->
				<cfset variables.receiverPersonID=EmailPersonIDs>
			</cfif>
			<!--- get the details of the person(s) who will receive this email --->	
			<CFQUERY NAME="GetReceiverDetails" DATASOURCE="#application.sitedatasource#">
				Select firstname+' '+lastname as EmailName, personid, email, locationid ,organisation.organisationname, person.organisationid 
				from person,organisation where 
				person.organisationid=organisation.organisationid and
				personid  in ( <cf_queryparam value="#replace(variables.receiverPersonID,"-",",","ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
 			<!--- set the to email address --->
			<cfif GetReceiverDetails.RecordCount neq 0>
				<cfset variables.toaddress="">
				<cfloop query="GetReceiverDetails">
					<cfif variables.toaddress eq "">
						<cfset variables.toaddress = "#GetReceiverDetails.EmailName# <#GetReceiverDetails.email#>">
					<cfelse>
						<cfset variables.toaddress = variables.toaddress & ",#GetReceiverDetails.EmailName# <#GetReceiverDetails.email#>">
					</cfif>
				</cfloop>
			<cfelse>
				<cfset variables.toaddress="#application.supportEmailName# <#application.supportEmailAddress#>">
			</cfif>
		<cfelse>
			<!--- if no personids set then use the support email as default --->
			<cfset variables.toaddress="#application.supportEmailName# <#application.supportEmailAddress#>">
		</cfif>
		
		<CFQUERY NAME="GetPartnerDetails" DATASOURCE="#application.sitedatasource#" maxrows="1">
			Select firstname, lastname, personid, email, locationid ,organisation.organisationname, person.organisationid 
			from person,organisation where 
			person.organisationid=organisation.organisationid and
			personid = #request.relaycurrentuser.personID#
			and personID <> 404
		</CFQUERY>
		
		<cfif GetPartnerDetails.recordcount neq 0>
			<cfset variables.organisationname=GetPartnerDetails.organisationname>
		<cfelse>
			<cfset variables.organisationname="Unknown">
		</cfif>
		
		<CF_addCommDetail
			personID 	 = "#request.relayCurrentUser.personid#"
			locationID   = "#request.relayCurrentUser.locationID#"
			commTypeID	 = 11
			commReasonID = 2501
			commStatusID = 3002
			body	 = "#frmMessageBody#"
		>
		
		<cfset variables.phrases="#formname#subjectprefix">
		<cf_translate phrases="#variables.phrases#" />								<!--- 2010/11/09 PPB LID4605 added the closing "/" on the cf_translate tag --->
		<cfset variables.subjectprefix=evaluate("phr_#formname#subjectprefix")>
		
		<CF_MAIL TO="#variables.toaddress#"
			FROM="#application.adminPostmaster#"
			SUBJECT="#variables.subjectprefix# - #frmSubject#"
			BCC="#application.cBccEmail#">
Sender Name: #form.frmtoName#
Senders Email Address:  #form.frmtoEmail#
Organisation:  #variables.organisationname#
	
Date:  #dateformat(DateAdded,"dd-mmm-yyyy")# #timeformat(DateAdded,"HH:mm")#
	
Message Type:  #frmSelectOption#
	
Subject:  #frmSubject#
	
Details:
#frmMessageBody#
</CF_MAIL>
	<!--- If the number of personids for the form is not eq to the number of options and is gt 1 --->
	<cfelse>
		ERROR - you must Specify the correct number of EmailPersonIDs to use with this form.<br>
		This can either be 0(will use support default) EmailPersonIDs, 1 EmailPersonIDs or an equal number to the number of selectdropdown options.
	</cfif>
<CFELSE>

<!--- Entry form --->
	<cfparam name="variables.toName" default="">
	<cfparam name="variables.toEmail" default="">
	
	<cfif isDefined("request.relayCurrentUser.personid")>
		<cfif request.relaycurrentuser.personID is not 404>
			<CFQUERY NAME="GetPartnerDetails" DATASOURCE="#application.sitedatasource#">
				Select firstname, lastname, personid, email, locationid ,organisation.organisationname, person.organisationid 
				from person,organisation where 
				person.organisationid=organisation.organisationid and
				personid = #request.relaycurrentuser.personID#
			</CFQUERY>
			<cfoutput query="GetPartnerDetails">
				<cfset variables.toName="#firstname# #lastname#">
				<cfset variables.toEmail="#email#">
			</cfoutput>
		</cfif>
	</cfif>
	
	<cfoutput>
		<FORM method="post">
			<INPUT NAME="Update" TYPE="Hidden" VALUE="true">
			<CF_INPUT type="HIDDEN" name="DateAdded" value="#CreateODBCDateTime(Now())#">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="">
				<tr>
					<td>
						<cfif selectoptions gt 1>
						<p>phr_#htmleditformat(formname)#DropDownTitle</p>		
						<p><SELECT NAME="frmSelectOption">
							<cfloop index="i" from="1" to="#selectoptions#" step="1">
								<OPTION VALUE="phr_#formname#Option#i#">phr_#htmleditformat(formname)#Option#htmleditformat(i)#</OPTION>
							</cfloop>
							<!---<OPTION VALUE="phr_generalsuggestion">phr_generalsuggestion</OPTION>
							<OPTION VALUE="phr_sitefeedback">phr_sitefeedback</OPTION>
							 <OPTION VALUE="#phr_complaint#">#phr_complaint#</OPTION>
							<OPTION VALUE="#phr_help#">#phr_help#</OPTION> --->
						</SELECT></p>
						<cfelse>
							<CF_INPUT type="hidden" name="frmSelectOption" value="phr_#formname#defaultoption">
						</cfif>
						<p>phr_#htmleditformat(formname)#Name</p>
						<p><CF_INPUT type="text" size="40" name="frmtoName" maxLength="255" value="#variables.toName#"></p>
						<p>phr_#htmleditformat(formname)#Email</p>
						<p><CF_INPUT type="text" size="40" name="frmtoEmail" maxLength="255" value="#variables.toEmail#"></p>
						<p>phr_#htmleditformat(formname)#SubjectTitle</p>
						<p><INPUT type="text" size="40" name="frmSubject" maxLength="80"></p>
					    <p>phr_#htmleditformat(formname)#MessageDetailTitle</p>
						<p><TEXTAREA name="frmMessageBody" cols=40 rows=5></TEXTAREA></p>
						<p><CF_INPUT type="submit" value="phr_#formname#submitbutton"></p>
					</td>
				</tr>
			</table>
		</FORM>
	</CFOUTPUT>
</CFIF>
</cf_translate>

