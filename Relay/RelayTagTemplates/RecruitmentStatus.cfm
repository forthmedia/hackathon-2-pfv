<!--- �Relayware. All Rights Reserved 2014 --->

<CFQUERY NAME="GetRP" DATASOURCE="#application.sitedatasource#">
	SELECT distinct fg.FlagGroupID,  fg.name
	FROM Person p 
	INNER JOIN FlagGroup fg
	ON p.Organisationid = fg.EntityMemberID
	AND p.personid =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			INNER JOIN flaggroup fg1
		ON fg.parentflaggroupid =  fg1.flaggroupid
		and fg1.flaggrouptextid='RecruitmentProgram'
</CFQUERY>

<CFIF IsDefined("form.Country")>
	<CFSET thisCountry = form.Country>
<CFELSE>
	<CFSET thisCountry = 0>
</CFIF>

<FORM NAME="FG_Form" METHOD="Post">
	<TABLE>
<CFIF GetRP.recordcount EQ 0>
	<!--- We safely assume at this point that the user is an Administrator 
	So we are going to set up a select Box that lists all the Recruitment 
	programs and show one at a time with access to all.
	--->
	<CFQUERY NAME="GetFGList" DATASOURCE="#application.sitedatasource#">
		SELECT fg.flaggroupid, fg.name FROM flaggroup fg
		INNER JOIN flaggroup fg1
		ON fg.parentflaggroupid =  fg1.flaggroupid
		and fg1.flaggrouptextid='RecruitmentProgram'
		and fg.active = 1
	</CFQUERY>
	<CFIF IsDefined("form.FG")>
		<CFSET thisReport = form.FG>
	<CFELSE>
		<CFSET thisReport = GetFGList.FlagGroupID>
	</CFIF>


	<TR><TD valign="top"><DIV align=right>Please select a Recruitment Report to view :</DIV></TD>
	<TD>
	
	<SELECT NAME="FG" onchange="Javascript:document.FG_Form.submit()">
		<CFOUTPUT QUERY="GetFGList">
			<OPTION VALUE="#flaggroupid#"<CFIF thisReport EQ flaggroupid> SELECTED </CFIF>>#htmleditformat(name)#
		</CFOUTPUT>
	</SELECT>
	</TD></TR>


	<CFSET flaggroupid=thisReport>
<CFELSE>
	<!--- We safely assume at this point that the user is a client 
	So we can show just this 1 flaggroupid 
	we show the country selection to both sets of user
	--->

	
	<CFSET flaggroupid=GetRP.FlagGroupID>
</CFIF>
	<CFQUERY NAME="GetCountries" DATASOURCE="#application.sitedatasource#">
		SELECT DISTINCT c.countryID, c.CountryDescription
		FROM flag f
		INNER JOIN booleanflagdata b ON b.flagid = f.flagid AND f.flaggroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		INNER JOIN organisation o ON o.organisationid = b.entityid
		INNER JOIN country c ON c.countryID = o.countryID
		ORDER BY c.CountryDescription
	</CFQUERY>
	<TR><TD valign="top"><DIV align=right>Please select a Country to filter by :</DIV></TD>
	<TD>
	<SELECT NAME="Country" onchange="Javascript:document.FG_Form.submit()">
		<OPTION VALUE="0" <CFIF thisCountry EQ 0> SELECTED </CFIF>>All Countries
		<CFOUTPUT QUERY="GetCountries">
			<OPTION VALUE="#countryID#"<CFIF thisCountry EQ countryID> SELECTED </CFIF>>#htmleditformat(CountryDescription)#
		</CFOUTPUT>
	</SELECT>
		<TR><TD valign="top"><DIV align=right>Please select a Recruitment Report to view :</DIV></TD>
	<TD>
	<CFIF IsDefined("form.FG")>
		<CFSET thisReport = form.FG>
	<CFELSE>
		<CFSET thisReport = GetRP.FlagGroupID>
	</CFIF>
	<SELECT NAME="FG" onchange="Javascript:document.FG_Form.submit()">
		<CFOUTPUT QUERY="GetRP">
			<OPTION VALUE="#flaggroupid#"<CFIF thisReport EQ flaggroupid> SELECTED </CFIF>>#htmleditformat(name)#
		</CFOUTPUT>
	</SELECT>
	</TD></TR>
	<CFOUTPUT>
	<CF_INPUT NAME="ElementID" TYPE="Hidden" VALUE="#elementid#">
	<INPUT NAME="FlagID" TYPE="Hidden" VALUE="0">
	</CFOUTPUT>
	
	</TD>
	</TR>
	</TABLE>
</FORM>

<cfinclude template="/report/profiles/FlagGroupCounts.cfm">