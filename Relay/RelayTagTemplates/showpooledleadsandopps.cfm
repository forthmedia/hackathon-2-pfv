<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			showPooledLeadsAndOpps.cfm	
Author:				AJC
Date started:		2008-07-01
	
Description:		This is a Relay tag that displays and allows assignment of Pooled leads to partners
					Original requirement from P-SOP001 (Sophos)

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<!--- any of the above variables can be over ridden in the INI file --->

<cf_param name="showAllPartnerLeads" type="boolean" default="no"/>
<cf_param name="filterSelect" type="string" default=""/>
<cf_param name="FilterSelectValues" type="string" default=""/>

<!--- 
WAB 2009/06/24 Removed as part of RACE Security
<cfinclude template="/leadManager/application .cfm">
<cfinclude template="/leadManager/LeadAndOpplistPool.cfm">

 ---> 
 <cf_includeWithCheckPermissionAndIncludes template = "/leadManager/LeadAndOpplistPool.cfm" debug="false">

