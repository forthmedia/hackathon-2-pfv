<!--- ©Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relay\RelayTagTemplates\RelatedPersonListing.cfm
Author:				NYB
Date started:		2008-11-19
	
Description:		This all specified columns in the People table where their personid is in flags specified for their usergroup 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
19 Feb 2009			NYB			rewrote the GetPeople query as it wasn't working properly or effectiently
25 Feb 2009			NYB			Sophos Stargate 2 - added the ability to based off country or location flags 

 --->

<cf_param name="FlagSource" default="AccManager,AccountTeam"/> <!--- must be of the same EntityType --->
<cf_param name="Columns" default="Fullname,JobDescription,eMail,Phone"/>
<cf_param name="defaultSortOrder" label="Sort Order" default="FlagSource,FirstName,LastName"/>
<cfparam name="sortOrder" default="#defaultSortOrder#">

<cf_param name="Format" default="table" validvalues="list,table"/> <!--- alt: list --->

<!--- START: NYB 2009-02-25 - Sophos Stargate 2 - added --->
<CFQUERY NAME="getFlagType" DATASOURCE="#application.siteDataSource#">
	select fet.uniqueKey,fg.flagTypeID from flaggroup fg 
	inner join flag f on fg.flaggroupid=f.flaggroupid and f.flagtextid =  <cf_queryparam value="#ListFirst(FlagSource)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	inner join flagEntityType fet on fg.EntityTypeid=fet.EntityTypeid 
</CFQUERY>
<cfset EntityTypeID = getFlagType.uniqueKey>

<cfif not request.relayCurrentUser.isUnknownUser>
	<!--- START: NYB 2009-02-25 - Sophos Stargate 2 - added --->
	<cfset PartnerIdentifier = evaluate("request.relaycurrentuser.#EntityTypeID#")>
	<cfif EntityTypeID eq "CountryID">
		<cfset PartnerIdentifier = ListAppend(PartnerIdentifier,application.CountryGroupsBelongedTo[PartnerIdentifier])>
	</cfif>
	<!--- END: NYB 2009-02-25 --->

	<CFQUERY NAME="GetPeople" datasource="#application.siteDataSource#">
		Select fs.FlagSource as FlagSource, p.Firstname + ' ' + p.Lastname as FullName, p.JobDesc as JobDescription, p.OfficePhone as Phone, p.* 
		from person p 
			inner join (integerFlagData i 
				inner join flag f on i.flagid=f.flagid 
				inner join flaggroup fg on f.flaggroupid=fg.flaggroupid and f.flagtextid in (<cf_queryparam value="#flagSource#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)) on p.personid=i.data 
			inner join (select 0 as FlagSource, '0' as FlagSourceName 
			<cfloop index="i" from="1" to="#listlen(FlagSource)#">
				union select #i# as FlagSource, <cf_queryparam value="#ListGetAt(FlagSource,i)#" CFSQLTYPE="CF_SQL_VARCHAR" list="true"> as FlagSourceName
			</cfloop>
			) as fs on fs.FlagSourceName=f.flagTextID
		<!--- NYB 2009-02-25 - Sophos Stargate 2 - replaced "entityid=#request.relaycurrentuser.organisationid#" with "entityid in (#PartnerIdentifier#)": --->
		where i.entityid  in ( <cf_queryparam value="#PartnerIdentifier#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  
			union
		Select fs.FlagSource as FlagSource, p.Firstname + ' ' + p.Lastname as FullName, p.JobDesc as JobDescription, p.OfficePhone as Phone, p.* 
			from person p 
			inner join (integerMultipleFlagData im 
				inner join flag f on im.flagid=f.flagid 
				inner join flaggroup fg on f.flaggroupid=fg.flaggroupid and f.flagtextid in (<cf_queryparam value="#flagSource#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)
			) on p.personid=im.data 
			inner join (select 0 as FlagSource, '0' as FlagSourceName 
			<cfloop index="i" from="1" to="#listlen(FlagSource)#">
				union select #i# as FlagSource, <cf_queryparam value="#ListGetAt(FlagSource,i)#" CFSQLTYPE="CF_SQL_VARCHAR" list="true"> as FlagSourceName
	</cfloop>
			) as fs on fs.FlagSourceName=f.flagTextID
		<!--- NYB 2009-02-25 - Sophos Stargate 2 - replaced "entityid=#request.relaycurrentuser.organisationid#" with "entityid in (#PartnerIdentifier#)": --->
		where im.entityid  in ( <cf_queryparam value="#PartnerIdentifier#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )  
		order by <cf_queryObjectName value="#SortOrder#">
	</CFQUERY>

	<cfif Format eq "List">
		<cfparam name="ColumnDelimiter" default=",">
		<cfparam name="RecordDelimiter" default=";">
		<cfset recordList = "">
		<cfoutput query="GetPeople">
			<cfset recordEntry = "">
			<cfloop index="col" list="#Columns#">
				<cfset colName = evaluate(col)>
				<cfif col eq "email">
					<cfset colName = "<a href='mailto:#colName#'>#colName#</a>">
				</cfif>
				<cfset recordEntry = ListAppend(recordEntry, colName, "#application.delim1#")>
			</cfloop>
			<cfset recordList = ListAppend(recordList, recordEntry, "^")>
		</cfoutput>
		<cfoutput>#replace(replace(recordList,"#application.delim1#",ColumnDelimiter,"all"),"^",RecordDelimiter,"all")#</cfoutput>
	<cfelse>
		<div class="RelatedPersonListing">
			<cfif IsQuery(GetPeople)>
				<CF_tableFromQueryObject queryObject="#GetPeople#" 
				HidePageControls="yes"
				useInclude = "false"
				
				keyColumnList="email"
				keyColumnURLList="mailto:"
				keyColumnKeyList="email"
				keyColumnTargetList="_blank"
				
				showTheseColumns="#Columns#"
				columnTranslation="true"
				ColumnTranslationPrefix="phr_Ext_"
				numRowsPerPage ="200"
				sortorder="#SortOrder#">
			</cfif>
		</div>
	</cfif>
	
</cfif>
