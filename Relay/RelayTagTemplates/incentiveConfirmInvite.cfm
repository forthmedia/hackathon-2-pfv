<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			incentiveConfirmInvite.cfm
Author:				AH/SWJ
Date started:		2004-08-25

Purpose:	To provide a form for verifying a user with their magic number .

Usage:	this is used as a Relay Tag

frmNext (optional) This specify which template is processed after the form data.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
derived from externaluservereification.cfm
2005-04-15   WAB		changed the delimiter in the partner session cookie from - to |  .  - does not work 'cause the date always has - in it as well
						Infact the whole concept of a login level isn't used at the moment, but I'm preserving the code
2005-06-13	WAB 	this code didn't work when being called from remote.cfm

2006-04-11	WAB		changed to use standardised login code

2007-08-29	GCC		Added the ability to pass in and set an incentive T & C s flag by flagtextID
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Possible enhancements	:


 --->


<CFPARAM name="message" default="">
<CFPARAM name="messagePhrase" default="">
<CFPARAM name="startNew" default="yes">



<cf_translate>


<cfif not structKeyExists(variables,"GoToEID")>
	<p>The External User Verification Relay tag requires the variable GoToEID which
	should be set to the elementID of the screen to go to if the user authenticates.</p>
	<CF_ABORT>
</cfif>


<cfscript>
	if(not request.relaycurrentuser.isUnknownUser){
		personRegistration = application.com.relayIncentive.isPersonRegistered(request.relaycurrentuser.personid);
	}
</cfscript>

<cfif not request.relaycurrentuser.isUnknownUser and personRegistration.recordCount eq 1>

	<!--- if the user is already registered then treat them here --->
	<cfif structKeyExists(variables,"IfRegisteredGoToEID") and 1 eq 2>
		<cflocation url="/?eid=#variables.IfRegisteredGoToEID#" addtoken="No">
	<cfelse>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
		<TR>
			<td valign="top">
			<cf_translate>
					phr_incentive_registration_AlreadyRegistered
					<!--- According to our records you have already registered for Sony rewards.
					Please <a href="?eid=952">click here</a> to login or contact the Sony rewards help desk on
					... --->
			</cf_translate>
			</td>
		</TR>
	</TABLE>
	</cfif>
	<CF_ABORT>
</cfif>


<cfscript>
	if(structKeyExists(form,"pNumber")){
		authenticated = application.com.login.autoAuthenticateUser(listFirstP=listFirst(pNumber,"-"),listLastP=listLast(pNumber,"-"),lastname=lastname);
		if(authenticated.recordcount) {
			personRegistration = application.com.relayIncentive.isPersonRegistered(authenticated.personid);
		} else {
			startnew = "yes";
		}
	}
</cfscript>

<!--- <cfif structKeyExists(variables, "authenticatedFailed")>

	<CF_ABORT>
</cfif> --->

<cfif structKeyExists(variables, "authenticated") and authenticated.recordCount EQ 1>
	<cfset loginBullet = '<img src="/code/borders/images/Sony-Bullet-transparent.gif" border="0">'>
	<cfset startNew = "no">
	<cfoutput>
	<cfif personRegistration.recordCount eq 1>
		<!--- if the user is already registered then treat them here --->
		<cfif structKeyExists(variables,"IfRegisteredGoToEID") and 1 eq 2>
			<cflocation url="/?eid=#variables.IfRegisteredGoToEID#" addtoken="No">
		<cfelse>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
			<TR>
				<td valign="top">
					<!--- phr_incentive_registration_AlreadyRegistered --->
					According to our records you have already registered for Sony rewards.
					Please <a href="/?eid=952">click here</a> to login or contact the Sony rewards help desk on
					...
				</td>
			</TR>
		</TABLE>
		</cfif>
		<CF_ABORT>
	</cfif>
	<SCRIPT type="text/javascript">

	<!--

	function verifyForm() {

		var msg = "";
		// first verify the Relay Screen
		var screenValidation = verifyInput ();
		if (screenValidation.length != 0)
			{
			msg += "phr_JS_YouMustCompleteTheFollowing :\n\n";
			msg += screenValidation;
			}
		var form = document.mainForm;

		//check that the declare checkbox has been checked
		if (!form.agreeToTerms.checked)
		{
			msg = msg + 'phr_incentive_JS_MustAgreeToTerms\n'
			alert(msg);
		}

		else
		{
			if (msg.length == 0)
			{
				//Don't seem to be any errors on the form
				form.submit();
			}
			else
			{
				alert(msg);
			}
		}
	}
	//-->

	</SCRIPT>




	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center">
		<FORM name="mainForm" method="post" action="#cgi.script_name#?#request.query_string#">
			<cfif structKeyExists(variables,"IfRegisteredGoToEID")>
				<CF_INPUT type="hidden" name="gotoEID" value="#IfRegisteredGoToEID#">
			</cfif>
			<CF_INPUT type="hidden" name="newPersonID" value="#authenticated.personid#">

			<cfparam name="incentiveRegistrationScreenTextID" default="">

			<tr><td colspan="2">phr_incentive_registration_CompleteYourDetails</td></tr>

			<cfif len(incentiveRegistrationScreenTextID) gt 0 and structKeyExists(authenticated, "personid")>
				<CFQUERY NAME="getUsersPersonDetails" DATASOURCE="#application.siteDataSource#">
					select p.*, l.locationID, l.countryID, p.lastUpdated
					from person p INNER JOIN location l on p.locationID = l.locationID
					where personID =  <cf_queryparam value="#authenticated.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>

				<tr>
					<td valign="top">
						<cf_ascreen formName = "mainForm">
							<cf_ascreenitem screenid="#incentiveRegistrationScreenTextID#"
								method="edit"
								countryID = "#getUsersPersonDetails.countryID#"
								person= "#getUsersPersonDetails#"
							>
						</cf_ascreen>
					</td>
				</tr>
			</cfif>

			<TR>
				<td colspan="2" valign="top">
					<INPUT type="checkbox" name="agreeToTerms" value="yes"><B>phr_incentive_agreeToTerms</B>&nbsp;&nbsp;<br>
					#htmleditformat(loginBullet)#&nbsp;phr_incentive_TermsConditions<!--- <a href="/?eid=1046" target="_blank" class="relink"><B>phr_incentive_viewTerms</B></a> --->&nbsp;&nbsp;
				</td>
			</TR>

			<TR>
				<td colspan="2" align="right" valign="top">
					#htmleditformat(loginBullet)#&nbsp;<A HREF="javascript:verifyForm();" class="relink"><B>phr_Continue</b></A>
				</td>
			</TR>

		</FORM>
	</TABLE>

	</cfoutput>

</cfif>



<cfif structKeyExists(FORM, "agreeToTerms")>
	<cf_ascreenUpdate>
	<cfset startNew = "no">
	<cfset level=2>

	<cfparam name="variables.incentiveMemberFlagTextID" type="string" default="approvedIncentiveUser">
	<cfparam name="variables.incentiveTandCFlagTextID" type="string" default="IncentiveTermsAgreed">

	<cfparam name="emailTextID" type="string" default="incentiveUserNameEmail">

	<!--- check the email is set up otherwise we'll get an error later on --->
	<cfscript>
		qGetEmailDetails = application.com.email.getEmailDetails(emailTextID);
	</cfscript>

	<CFIF qGetEmailDetails.recordcount neq 1>

		<cfoutput><p>You must add an email to Workflow/Email with the textID of #htmleditformat(emailTextID)#</p></cfoutput>
		<CF_ABORT>
	</CFIF>

	<cfscript>
		// set the flag which adds the user to the correct user group
		application.com.flag.setBooleanFlag(FORM.NewPersonID, variables.incentiveMemberFlagTextID);
		// set the flag which adds the user to the agreed T and Cs flag if it exists
		if (application.com.flag.doesFlagExist(variables.incentiveTandCFlagTextID)) {
		application.com.flag.setBooleanFlag(FORM.NewPersonID, variables.incentiveTandCFlagTextID);
		}
		// send the user an email with their user name and password
		incentiveAccountID = application.com.relayIncentive.createRWAccount(0,0,FORM.newPersonID);
		incentivePersonAccountID = application.com.relayIncentive.addPersonToRWAccount(incentiveAccountID, FORM.newPersonID);
		// add them to the members flag
		// application.com.flag.setBooleanFlag(FORM.newPersonID,incentiveMemberFlagTextID);
		application.com.login.createUserNamePassword(FORM.NewPersonID);
		application.com.email.sendEmail(emailTextID,FORM.NewPersonID);

		//2005/08/09 GCC Check for registration promotions - need persons countryID - registration date is now
		getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(FORM.NewPersonID);
		request.promotionDate = "registrationDate";
		application.com.relayIncentive.getActivePromotions(countryID = getFullPersonDetails.countryID, personID = getFullPersonDetails.personID, triggerPoint = 'registration',	nowDate = createODBCDateTime(now()));
	</cfscript>



	<!--- now log them into the internal site --->
	<cfset application.com.login.externalUserLoginProcess (FORM.NewPersonID)>

		<!---
		WAB 2006-04-11 replaced with line above

	<!---Set partner's session cookie (TimeStamp and level of login)--->
	<cfset application.com.globalFunctions.cfcookie(NAME="PARTNERSESSION" VALUE="#Now()#-#variables.level#")>
	<!--- Dynamically recreates their user groups. External user groups are controlled by flags that a user has set. --->
	<CF_createUserGroups personid = "#FORM.NewPersonID#">
	<!--- Get all the usergroups this person is a member of --->
	<cfscript>
		getUserGroups = application.com.login.getUserGroups(FORM.NewPersonID);
		application.com.login.insertUsage(FORM.NewPersonID);
	</cfscript>
	<!---and set the User Cookie--->
	<CFCOOKIE NAME="USER" VALUE="#FORM.NewPersonID#-#application.WebUserID#-#valuelist(getUserGroups.userGroupID)#"
			EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#">

	<cfscript>
				//initialize user session structure
		application.com.login.initUserSession(FORM.NewPersonID);
	</cfscript>
		 --->

	<cfoutput>
	<script>
		window.location.href = '/?eid=#jsStringFormat(gotoEID)#';
	</script>
	</cfoutput>
</cfif>

<cfif startNew eq "yes">

<!--- WAB - problem when this page called from remote.cfm
		the p number is passed through on the query string
		when it hits et.cfm, et.cfm sees the p number and uses it to do a login and then takes the user off somewhere else rather than coming back to here

	so I am going to remove p number from the querystring

	also there is no eid on the querystring so doesn't know where to come back to so lets add it on
	elementID is passed in by executerelaytag


--->
<cfset modifiedquerystring = rereplaceNoCase(request.query_string,"&p=[0-9]*-[0-9]*","","ALL")>
<cfif modifiedquerystring does not contain "eid=" and modifiedquerystring does not contain "etid=">
	<cfset modifiedquerystring = modifiedquerystring & "&eid=" & elementid>
</cfif>



	<CFPARAM NAME="Level" DEFAULT="2">
	<cfoutput>
	<SCRIPT type="text/javascript" src="/javascript/verifyPNumber.js"></script>
	<SCRIPT type="text/javascript">

		function submitform(){

			form = window.document.myForm2;

			if (form.pNumber.value == ""){
				alert("phr_LoginScreen_JS_PleaseEnterYourPNumber ");
				form.pNumber.focus();
				return
			}

			else if (! verifyPNumber(form.pNumber)){
					alert("phr_LoginScreen_JS_PleaseEnterPNumberInCorrectFormat");
					form.pNumber.focus();
					return
			}

			else if (form.lastname.value == ""){
				alert("phr_LoginScreen_JS_PleaseEnterLastName");
				form.lastname.focus();
				return
			}
			else {
				form.submit();
			}
		}
	</SCRIPT>
	</cfoutput>

	<cfoutput>

	phr_incentive_ConfirmParticipationIntroText

	<TABLE ALIGN="center" border=0>
	<FORM ACTION="/?#modifiedquerystring#" METHOD="post" NAME="myForm2" ID="myForm2" >
		<INPUT TYPE="Hidden" NAME="reLogin" VALUE=""><!--- forces reauthentication and ignores existing cookies --->


		<CFIF structKeyExists(variables, "authenticated") and authenticated.recordCount NEQ 1>
			<TR>
				<TD colspan="2" align="left">
					<P STYLE="color: Red;">
						<B>phr_userVerification_JS_InvalidVerificationDetails</B>
					</p>
				</TD>
		   	</TR>
		</CFIF>



		<TR>
			<TD><P><B>phr_incentive_MagicNumber</B></P></TD>
			<TD ALIGN="left"><INPUT TYPE="text" NAME="pNumber" <cfif structKeyExists(URL, "pp")>value="#htmleditformat(URL.pp)#"</cfif> ></TD>
		</TR>

		<TR>
			<TD><P><B>phr_incentive_LastName </B></P></TD>
			<TD ALIGN="left"><INPUT TYPE="text" NAME="lastname"></TD>
		</TR>

		<TR>
			<TD>&nbsp;</TD>
			<TD align="left">
				<input type="button" onClick="javascript:submitform()" value="phr_continue" class="button">
			</TD>

		</TR>
		</FORM>

	</TABLE>

	phr_incentive_ConfirmParticipationSubText <br>
</cfoutput>

</cfif>

</cf_translate>

