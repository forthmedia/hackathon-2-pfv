<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

	WAB 2010/04/21

	relayTag   RELAY_TABS
	
	used to create an Ext tab panel within a portal page
	
	attributes:
		Title_1   - Tab title as displayed
		URL_1		- URL to display
		Name_1		- Name of tab (is defaulted but needed if you want to refer to tab in JS 
		
		(and so on up to 10)
		
		debug   -  by default debug is suppressed on the tabs to prevent a horizontal scroll bar 
		startingTab - can pass either a name or a number 
		height
		width
	
Amendments
2012-05-10 PPB P-MIC001 if there is already a "?" in the URL cos we have added _cf_nodebug append a "&" before the urlVariables not a 2nd "?"
	
--->

	<!--- PJP 20/12/2012: CASE 431613: Change to use isdefined on url.startingtab instead of cfparam --->
	<cfif isdefined('url.startingTab') and not isdefined('attributes.startingTab')>
		<cfset attributes.startingTab = url.startingTab>
	</cfif>

	<cf_param name="attributes.startingTab" default="1"/>  
	<!---END CASE 431613 --->
	<cf_param name="attributes.debug" default="No" type="boolean"/> <!--- need atleast one attribute so that this tag is called as a custom tag --->
	<cf_param name="attributes.title_1" default="" required="true"/>
	<cf_param name="attributes.URL_1" default="" required="true"/>
	<cf_param name="attributes.Name_1" default="" required="true"/>	
	<cf_param name="attributes.Active_1" default="Yes" type="boolean"/>	
	<cf_param name="attributes.title_2" default=""/>
	<cf_param name="attributes.URL_2" default=""/>
	<cf_param name="attributes.Name_2" default=""/>	
	<cf_param name="attributes.Active_2" default="Yes" type="boolean"/>	
	<cf_param name="attributes.title_3" default=""/>
	<cf_param name="attributes.URL_3" default=""/>
	<cf_param name="attributes.Name_3" default=""/>	
	<cf_param name="attributes.Active_3" default="Yes" type="boolean"/>	
	<cf_param name="attributes.panelHeight" default = "$('contentWrapper').getDimensions().height - 50"/>	 
	<cf_param name="attributes.width" default = "$('innerContentColumns').getDimensions().width - 10"/>
	<cf_param name="attributes.stylesheet" default=""/>
	<cf_param name="attributes.urlAttributes" default="#structNew()#"/> <!--- pass in url variables to called templates --->
		 
		<cfset tabArray = arrayNew(1)>
		<cfloop index="i" from="1" to = "10">
			<cfif structKeyExists (attributes, "Title_#i#") and attributes["Title_#i#"] is not "">
				<cfset tabArray[i] = {title=attributes["Title_#i#"],options="",active=true}>
				<cfif structKeyExists (attributes,"Active_#i#") and not attributes["Active_#i#"]>
					<cfset tabArray[i].active = false>
				</cfif>
				<cfset tabArray[i].url = attributes["URL_#i#"]>
				<cfif structKeyExists (attributes,"Name_#i#") and attributes["Name_#i#"] is not "">
					<cfset tabArray[i].name = reReplaceNoCase(attributes["Name_#i#"],"[^0-9a-zA-Z_]","","ALL")>
				<cfelse>
					<cfset tabArray[i].name = "Tab#i#">
				</cfif>
				
				<cfif left(tabArray[i].url,4) is "phr_" >
					<cfset tabArray[i].options = "useIFrame:false">
				<cfelse>
					<cfset tabArray[i].url= application.com.security.encryptURL(tabArray[i].url)>
					<cfif not attributes.debug>
						<!--- hides CF debug in the tabs, to prevent a horizontal scroll bar --->
						<cfset tabArray[i].url =  tabArray[i].url & iif(listLen(tabArray[i].url,"?") gt 1,de("&"),de("?")) & "_cf_nodebug=true"> 
					</cfif>
				</cfif>	
				
					
			<cfelse>
				<cfset tabArray[i] = {active=false}>
			</cfif>
		</cfloop>
		

<cfif arrayLen(tabArray) is 0>
	<cfoutput>No tabs Defined</cfoutput>
<cfelse>
	
		<cfoutput>
			<cfhtmlhead text='<link rel="stylesheet" type="text/css" href="/javascript/ext/resources/css/core.css" />'>
			<cfhtmlhead text='<link rel="stylesheet" type="text/css" href="/javascript/ext/resources/css/tabs.css" />'>
			<cfif fileexists("#application.paths.code#\styles\tabs.css")>
				<cfhtmlhead text='<link rel="stylesheet" type="text/css" href="/code/styles/tabs.css" />'>
			</cfif>
		</cfoutput>
		
		
		<cf_includeJavascriptOnce template = "/javascript/ext/adapter/ext/ext-base.js">
		<cf_includeJavascriptOnce template = "/javascript/ext/ext-all.js">
		<cf_includeJavascriptOnce template = "/javascript/ext/docs/resources/TabCloseMenu.js">
		<cf_includeJavascriptOnce template = "/javascript/ext/DDTabPanel.js">

		<cfset urlVariables = application.com.structurefunctions.convertStructureToNameValuePairs(struct=attributes.urlAttributes,delimiter = "&", equalssign = "=")>

		<div id="renderTo">

		</div>
		

		 <script>
		<cfoutput>
		var tabPanelWidth = #attributes.width#
		var tabPanelHeight = #attributes.panelHeight#
		var renderToDiv = 'renderTo'
		</cfoutput>

				<cf_translate encode="javascript">
				<cfoutput>
					function openStartingTabs () {
					<cfloop index="i" from="1" to="#arrayLen(tabArray)#">
						<cfif tabArray[i].active>
						  <!--- 2012/05/10 PPB P-MIC001 if there is already a "?" in the URL cos we have added _cf_nodebug above append a "&" before the urlVariables not a 2nd "?"   --->
						  tab#jsStringFormat(i)# = Ext.ux.addTab ('#tabArray[i].Name#','#tabArray[i].Title#','#tabArray[i].URL##iif(listLen(tabArray[i].URL,"?") gt 1,de("&"),de("?"))##jsStringFormat(urlVariables)#',{closeable:false<cfif tabArray[i].options is not "">,#tabArray[i].options#</cfif>})				 
						</cfif>
					</cfloop>
				
					
					Ext.ux.showTabByName('<cfif isNumeric(attributes.startingTab)>#tabArray[attributes.startingTab].Name#<cfelse>#jsStringFormat(attributes.startingTab)#</cfif>')
					
					}
				</cfoutput>
				</cf_translate>
			</script>
			<script type="text/javascript" src="/javascript/extTabPanel.js"></script>		
</cfif>

