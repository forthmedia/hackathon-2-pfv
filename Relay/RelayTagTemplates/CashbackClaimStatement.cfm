<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		CashbackClaimStatement.cfm	
Author:			NJH  
Date started:	05-03-2009
	
Description:	View cashback claim statement		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/11/06 			NJH			P-SNY086 - added additionalReference column as a child column

Possible enhancements:


 --->
<cfinclude template="\cashback\applicationCashbackINI.cfm">

<cfset viewAsStatement = true>
<cfset keyColumnList = "_">
<cfset keyColumnURLList = " ">
<cfset keyColumnKeyList = " ">
<cfset keyColumnOnClickList = " ">
<cfset dateFormat = "ApprovalRejectionDate,Created">
<cfset claimsReport_showTheseMasterColumns = "CashbackClaimID,Credit_Amount,Created,TranslatedStatus,ApprovalRejectionDate,_"> <!--- NJH 2009/05/12 CR-SNY675-1 added cashbackClaimID --->
<cfset claimsReport_showTheseChildColumns = "ProductGroup,Product,Quantity">
<cfset keyColumnOpenInWindowList = "no">
<cfset keyColumnOnClickList = "showCashbackClaimItems(this*comma##CashbackClaimID##);return false">

<cfparam name="claimsReport_showTheseChildColumns" type="string" default="ProductGroup,Product,Quantity,AdditionalReference"> <!--- NJH 2009/11/06 P-SNY086 --->

<cfinclude template="/Cashback/viewCashbackClaims.cfm">
