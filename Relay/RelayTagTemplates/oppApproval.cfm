<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	oppApproval.cfm 
	
	This template will return a list of opportunities that are awaiting approval 
	

	Parameters:
	
	Author: Peter Barron 2010-04-19

Amendment History:
2010-05-21	AJC	LID 3442 Change approvals to call the same relaytag instead of another file
2010-06-24	PPB now moved the Approval stage into oppEdit such that if the designated approver clicks 'Place Order' it is auto approved
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Potential Enhancements

PPB - I have introduced a oppStage AwaitingApproval - this could be used to filter this list 
--->

<cf_param name="startRow" default="1"/>
<cf_param name="numRowsPerPage" default="100"/>
<cf_param name="defaultSortOrder" label="Sort Order" default="oppExpectedCloseDate"/>
<cfparam name="sortOrder" default="#defaultSortOrder#">
		
<cf_param name="returnMessage" default=""/>
<cf_param name="showTheseColumns" multiple="true" displayAs="twoSelects" default="Customer,Contact,Description,Total Value,Expected Close,ViewButton" validvalues="Customer,Contact,Description,Total Value,Expected Close,ViewButton" allowSort="true"/>

<cfset ColumnHeadingList = ReplaceList(showTheseColumns,"ViewButton","Blank")>
<cfset showTheseColumns = ReplaceList(showTheseColumns,"Customer,Contact,Description,Total Value,Expected Close","CustomerOrganisationName,ContactName,Description,TotalValue,oppExpectedCloseDate")>


<!--- PPB 2010-07-23 moved query to cfc to be able to re-use it  --->
<cfset qryOpportunities=application.com.opportunity.getOpportunitiesAwaitingApproval()>

<cfoutput>
	<script>
		function viewOpp (params) {
			parent.location.href='/?eid=oppEdit&' + params
		}
	</script>
</cfoutput>


<cfset returnURL = "#cgi.script_name#?#request.query_string#">

<cfif qryOpportunities.recordCount gt 0>
	<CF_tableFromQueryObject  
		queryObject="#qryOpportunities#"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		openAsExcel = "false"
		numRowsPerPage="#numRowsPerPage#"
		totalTheseColumns=""
		GroupByColumns=""
		ColumnTranslation="true"
	    ColumnTranslationPrefix="phr_"
		ColumnHeadingList="#ColumnHeadingList#"
		showTheseColumns="#showTheseColumns#"
		FilterSelectFieldList=""
		FilterSelectFieldList2=""
		hideTheseColumns=""
		numberFormat=""
		dateFormat="oppExpectedCloseDate"
		showCellColumnHeadings="no"
		keyColumnURLList=" "
		keyColumnOnClickList="viewOpp('##application.com.security.encryptQueryString('returnURL=#urlencodedformat(returnURL)#&opportunityid=##opportunityid##&approveIt=1')##')"
		keyColumnKeyList="opportunityID"
		keyColumnList="ViewButton"
		keyColumnOpenInWindowList="no"
		OpenWinSettingsList="" 
		useInclude = "false" 
		allowColumnSorting = "true"
		PriceFromCurrencyColumn="currency"
		currencyFormat="TotalValue"
		KeyColumnLinkDisplayList="button"
	>
<cfelse>
	phr_opp_NoOpportunitiesForApproval
</cfif>