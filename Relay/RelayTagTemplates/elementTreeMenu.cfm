<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB: 2006-11-14 had to change dtree object variable from d to dtreeMenu.  Some other bit of third party javascript was also using d as a variable

2013-09-18  WAB File had been removed from 2013 - but turned out still being used by Lexmark (as an include)
 --->

<cfparam name="elementTree">
<cfparam name="request.dtreePlusIcon" default="/images/dtree/dtree_expand.gif">
<cfparam name="request.dtreeMinusIcon" default="/images/dtree/dtree_shrink.gif">

<!--- 
removed this QoQ because need to use the order which is passed in  (although the top element needs to be at the top)

<cfquery name="getElements" dbtype="query">
	SELECT * FROM elementTree
	ORDER BY SORTORDER   
</cfquery>
 --->
<cfset getelements = elementTree>


<!--- do this so that we can escape any quotes --->
<cf_translateQueryColumn query = #getElements# columnName = "headline">


<cf_includeJavascriptOnce template="/javascript/dtreeMenu.js">
<cfoutput>	

	<cfif isdefined('DtreeStyleSheet')>
    	<cfhtmlhead text='<link type="text/css" rel="stylesheet" href="/code/styles/#DtreeStyleSheet#.css" />'>		
	<cfelse>
    	<cfhtmlhead text='<link type="text/css" rel="stylesheet" href="/code/styles/DefaultDtreeMenu.css" />'>  	
	</cfif>
		
	<cfparam name="request.useIcons" default="false">
	
	<!--- 2006/08/03 GCC & NJH we looked at using showIcons but that looked painful 
	so we have bodged this in for now... needs revisiting--->
	<cfparam name="request.dTreeShowChildIcon" default="false">
	<cfparam name="request.dTreeChildIconImage" default="/code/borders/altioImages/sub_section_arrow.gif">
	<cfparam name="request.forceTopLevelIcon" default="false">
</cfoutput>


<SCRIPT type="text/javascript">
	
	dTreeMenu = new dTree('dTreeMenu');
	dTreeMenu.config.inOrder=true; 
	dTreeMenu.config.useCookies=false; 		
	dTreeMenu.config.useIcons=<cfoutput>#jsStringFormat(request.useIcons)#</cfoutput>; 		
	dTreeMenu.config.treeTop=<cfoutput>#jsStringFormat(treeTop)#</cfoutput>; 
	dTreeMenu.config.forceTopLevelIcon=<cfoutput>#jsStringFormat(request.forceTopLevelIcon)#</cfoutput>; 
	dTreeMenu.config.useLines = false ;
	dTreeMenu.config.folderLinks =true ;	
	dTreeMenu.config.closeSameLevel	=true ;
	dTreeMenu.config.useSelection	=true ;
	dTreeMenu.config.treeMenu = true			
	dTreeMenu.icon.nlPlus		= "<cfoutput>#jsStringFormat(request.dtreePlusIcon)#</cfoutput>"
	dTreeMenu.icon.nlMinus		= "<cfoutput>#jsStringFormat(request.dtreeMinusIcon)#</cfoutput>"

	<cfset folderIcon = "">

	<cfset suppressChildrenOfGeneration = 99>	<!--- used to suppress items from navigation using value in parameters "hideChildrenFromMainNavigation=1" --->
	<CFLoop Query="getElements" >
			<cfif resolvedTarget eq "">
				<cfset resolvedTarget = targetFrame>
			</cfif>
			<cfif generation lte suppressChildrenOfGeneration >			

					<cfif parameters contains "hideFromV3=1">
						<cfset suppressChildrenOfGeneration = generation>  <!--- this means that loop will continue until we get back to another item of this generation --->
					<cfelse>

							<cfoutput>dTreeMenu.add('#jsStringFormat(node)#','<cfif currentrow is 1>-1<cfelse>#jsStringFormat(parentID)#</cfif>','<cfif generation gt 2 and request.dTreeShowChildIcon><img src="#request.dTreeChildIconImage#" class="folderIcon">#replace(headline,"'","\'","ALL")#<cfelse>#replace(headline,"'","\'","ALL")#</cfif>','#jsStringFormat(resolvedhref)#','','#jsStringFormat(resolvedTarget)#','#jsStringFormat(foldericon)#','','');
							</cfoutput>
					</cfif>
								
					<cfif parameters contains "hideChildrenFromMainNavigation=1">
						<cfset suppressChildrenOfGeneration = generation>  <!--- this means that loop will continue until we get back to another item of this generation --->
					<cfelse>
						<cfset suppressChildrenOfGeneration = 99>
					</cfif>
			</cfif>
	</CFLoop>

	document.write(dTreeMenu);
</script>
<cfif targetFrame eq "_self">
	<cfoutput>
		<cfset MenusToOpen = "#request.currentElement.parentIDlist#,#request.currentElement.node#">
		<script>
			dTreeMenu.openToLowestRefInList('#jsStringFormat(MenusToOpen)#')
		</script>
	</cfoutput>
</cfif>


