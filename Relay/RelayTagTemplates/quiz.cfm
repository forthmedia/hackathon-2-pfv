<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		quiz.cfm
Author:			SWJ
Date started:	22-05-2008

Description:	RelayTag to display and launch all relayQuizzes

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-10-13	NYB		Sophos. 	added QuizDetailID param - as clients were adding this to the relaytag but it wasn't doing anything
					Sophos-2. 	added OrderMethod to RelayTag
2008-10-21	NYB  	Sophos Bug	changed QuizDetailID to QuizID in javascript
								changed cfparam QuizDetailID to string
2011-09-16	NYB		P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
						added params stringencyMode & showIncorrectQuestionsOnCompletion
2015-08-21  VSN     445557 - translate the quiz link

Possible enhancements:


 --->

<cf_param label="Quiz" name="QuizDetailID" type="string" default="0" validvalues="select quizDetailId as value, 'phr_quizDetail_title_'+cast(quizDetailId as varchar) as display from quizDetail order by display" nullText="All"/>
<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/>
<cf_param NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#"/><!--- NYB 2011-09-15 P-SNY106 --->
<cf_param NAME="showIncorrectQuestionsOnCompletion" DEFAULT="No" type="boolean"/><!--- NYB 2011-09-15 P-SNY106 --->

<cf_includejavascriptonce template = "/javascript/openwin.js">

<!--- get the information about all quizzes --->
<!--- 2008-10-13 NYB - added if --->
<cfif QuizDetailID gt 0>
	<cfset quizzes = application.com.relayQuiz.getQuizzes(QuizDetailID=QuizDetailID)>
<cfelse>
<cfset quizzes = application.com.relayQuiz.getQuizzes()>
</cfif>

<cfoutput query="quizzes">
	<!--- START: 2011-09-16 NYB P-SNY106 - added encryptURL call then use the encryptedLink instead in the js call --->
		<cfset encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#')>
        <!--- 2015-08-21  VSN     445557 - translate the quiz link --->
		<p><a href="javascript:void(openWin('#encryptedLink#','MyWindow','width=600,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'));">phr_elearning_StartQuiz #htmleditformat(QuizName)#</a></p>
	<!--- END: 2011-09-16 NYB P-SNY106 --->
</cfoutput>
