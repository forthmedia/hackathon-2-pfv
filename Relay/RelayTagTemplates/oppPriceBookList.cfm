<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	oppPriceBookList.cfm 
	
	This template will return a list of pricebooks for the organisation of the logged-in person and provide a button to allow them to download a pricebook to excel
	

	Parameters:
	
	Author: Peter Barron 2010-04-06

Amendment History:


--->

<!--- 
originally the download button was built up in the SQL ie.
'<input type="button" onClick="javascript:location.href=''       /LeadManager/oppPriceBookExport.cfm?pricebookID=' + CAST(Pricebook.pricebookID AS varchar)  + '&Country=' + Country.CountryDescription + '&Currency=' + Pricebook.Currency + '         ''" value="Download"/>' AS DownloadButton
 but when I needed to add encryption using application.com.security.encryptURL() the syntax becomes awkward so I have switched to using the 'keyColumn' arguments of TFQO which does the encryption automatically
--->
<cf_param name="startRow" type="numeric" default="1"/>
<cf_param name="numRowsPerPage" type="numeric" default="100"/>

<cfset vendorOrganisationId = application.com.settings.getSetting("theClient.clientOrganisationId")>
<cfset pricebookCampaignID = application.com.settings.getSetting("leadManager.pricebookCampaignID")>

<cfset userIsTier1 = application.com.flag.isFlagSetForPerson(flagId="Tier1Tier2DistributorTier1",personId=#request.relaycurrentuser.personId#)>
<cfset userIsTier2 = application.com.flag.isFlagSetForPerson(flagId="Tier1Tier2DistributorTier2",personId=#request.relaycurrentuser.personId#)>
<cfset userIsDisti = application.com.flag.isFlagSetForPerson(flagId="Tier1Tier2DistributorDistri",personId=#request.relaycurrentuser.personId#)>

<cfif userIsTier2>
	<cfset distiOrganisationId = application.com.flag.getFlagData(flagId="AssignDistributorOrgID",entityID=request.relaycurrentuser.OrganisationID).data>
	
	<cfif distiOrganisationId eq "">
		<cfset distiOrganisationId = 0> 
	</cfif>
</cfif>

<cfif userIsTier1 OR userIsTier2 OR userIsDisti>	<!--- one of them should be set --->	

	<cfquery name="getOppPriceBooks" datasource="#application.siteDataSource#">
		SELECT     Pricebook.pricebookID, Pricebook.Name AS PricebookName, Pricebook.countryID, Pricebook.currency, Pricebook.startdate, Pricebook.enddate, Organisation.organisationName, Country.ISOCode, Country.CountryDescription, 
					'phr_Download' AS DownloadButton
		FROM         Pricebook INNER JOIN
	                 Country ON Pricebook.countryID = Country.CountryID INNER JOIN
	                 Organisation ON Pricebook.OrganisationID = Organisation.OrganisationID
		WHERE 		(Pricebook.endDate IS NULL OR Pricebook.endDate >= GETDATE())
		AND 		(Pricebook.deleted != 1)
		AND 		(Pricebook.campaignID =  <cf_queryparam value="#pricebookCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		<!--- 2012-07-20 PPB P-SMA001		AND    		(Pricebook.CountryID IN (#request.relaycurrentuser.CountryList#)) --->
		#application.com.rights.getRightsFilterWhereClause(entityType="Pricebook").whereClause#			<!--- 2012-07-20 PPB P-SMA001 --->
		<cfif userIsTier1>
			AND Pricebook.OrganisationID =  <cf_queryparam value="#vendorOrganisationId#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfif>
	
		<cfif userIsTier2>
			AND Pricebook.OrganisationID =  <cf_queryparam value="#distiOrganisationId#" CFSQLTYPE="CF_SQL_INTEGER" >  
		</cfif>
	
		<cfif userIsDisti>
			AND (Pricebook.OrganisationID =  <cf_queryparam value="#vendorOrganisationId#" CFSQLTYPE="CF_SQL_INTEGER" >  OR Pricebook.OrganisationID = #request.relaycurrentuser.OrganisationID#)
		</cfif>
	</cfquery>

	<cfoutput>
	<script>
		function downloadPricebook (params) {
			//we want to open it in a window so we don't lose the underlying page; keep the window small cos there is no output required 
			openWin('/templates/oppPriceBookExport.cfm?' + params, 'winname','toolbar=no,width=100,height=100,location=no')
		}
	</script>
	</cfoutput>

	
	<cfif getOppPriceBooks.recordCount gt 0>
		<CF_tableFromQueryObject  
			queryObject="#getOppPriceBooks#"
			sortOrder = "PricebookName,CountryDescription,currency,startdate"
			startRow = "#startRow#"
			openAsExcel = "false"
			numRowsPerPage="#numRowsPerPage#"
			totalTheseColumns=""
			GroupByColumns=""
			ColumnTranslation="true"
		    ColumnTranslationPrefix="phr_"
			ColumnHeadingList="Name,Country,Currency,Start,End,blank"
			showTheseColumns="PricebookName,CountryDescription,currency,startdate,enddate,DownloadButton"
			FilterSelectFieldList=""
			FilterSelectFieldList2=""
			hideTheseColumns=""
			numberFormat=""
			currencyformat=""
			dateFormat="startdate,enddate"
			showCellColumnHeadings="no" 
			keyColumnURLList=" "
			keyColumnKeyList=""
			keyColumnList="DownloadButton"
			keyColumnOnClickList="downloadPricebook('##application.com.security.encryptQueryString('pricebookID=##pricebookID##&Organisation=##organisationName##&Country=##CountryDescription##&Currency=##Currency##')##')"
			KeyColumnLinkDisplayList="button"	
			keyColumnOpenInWindowList="no"
			OpenWinSettingsList="" 
			useInclude = "false" 
			allowColumnSorting = "false"
		>
	<cfelse>
		<br/>
		<span class="infoblock">phr_opp_NoPricebooksToList</span>
	</cfif>
	
</cfif>
