<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			conditionalRedirect.cfm	
Author:				SWJ
Date started:		2006-01-30
	
Description:		The idea of this relayTag is to provide a baisc method to test a variable
					in a scope and if it true the redirect the page to another element.
					

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	2006-07-17		WAB			added handling of flags (Flag becomes a 'scope')
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Possible enhancements:
	
Allow multiple conditions and lists of vars effectively a loop of decisions

decide what options to support

 --->

<cf_param label="Variable to Check" name="variable" required="yes"/>
<cf_param label="Scope of the Variable" name="scopeToCheck" validvalues="flag,expression,form,url,cookie,session"/>
<cf_param label="Redirect to Element ID" name="goToEID" required="yes"/>

<cfif not isDefined("scopeToCheck")><!--- probably should be optional defaulting to true --->
	<p>You must provide the scope of the variable to this tag.  I.e. Cookie, URL, request or Form</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("variable")>
	<p>You must provide the variableName to test for the existence of</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("goToEID")>
	<p>You must provide the eid that this should be redirected to if true</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfset doRedirect = false>

<cfswitch expression= #scopeToCheck#>
	<cfcase value="flag" >
		<cfset flag  = application.com.flag.getFlagStructure(variable)>
		<cfif listfind("2,3",flag.flagType.flagtypeid) is 0 >
			<cfoutput>#variable# is not a boolean flag</cfoutput>
		</cfif>
		<cfif listfind("0,1,2",flag.entityType.entitytypeid) is 0>
			<cfoutput>#flag.name# not POL</cfoutput>
		</cfif>

		<cfset doRedirect = application.com.flag.checkBooleanFlagByID(flagid = variable,entityid = request.relaycurrentuser[flag.entityType.uniquekey])>
	</cfcase>
	
	<!--- not developed yet - needs to test result against a value 
	<cfcase value="currentUser">
		<cfif structKeyExists(request.relayCurrentUser,variable)>			
		</cfif>
	</cfcase>
	 --->
	 
	<cfcase value="expression" >
		<!--- for complicated cases (eg where more than one thing is to be tested) --->
		<!---  don't allow application.com functions to be accessed --->
		<cfset  variable = replaceNoCase(variable,'application.com',"","ALL")>						
		<!--- alias this cfc so all methods can be called --->
		<cfset fn = application.com.redirectorAllowedFunctions>   
		<!--- another alias --->
		<cfset  user =  request.relayCurrentUser>
		
		<cftry>
			<cfset doRedirect = evaluate(variable)>			

			<cfcatch>
				<cfdump var="#cfcatch#"><!--- if there is an error just continue (result will be false) output an error message in comments, could send an email somewhere --->			
				<cfoutput><!--  conditionalredirect error #htmleditformat(variable)# <!--- #cfcatch. ---> --></cfoutput>
			</cfcatch>
		</cftry>

	</cfcase>

	<cfdefaultcase>
		<cfif structKeyExists(scopeToCheck,variable)>
			<cfset doRedirect= true>
		</cfif> 
	</cfdefaultcase>
</cfswitch>


<cfif doRedirect>
	<cflocation url="/?eid=#goToEID#" addToken="false">
</cfif>