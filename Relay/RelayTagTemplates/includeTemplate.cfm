<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="Yes" >
<!--- Used to call a UserFilesPath template 

Usage: 

--->

<cf_param name="template" type="string" default=""/>

<CFIF template neq "">
	<CFIF isDefined("showDebug") and showDebug eq "yes">
	<CFOUTPUT>
		<table border="2">
		<tr>
		<td><p>
			<h2>RELAY_INCLUDETEMPLATE information</h2>
			
				<CFIF isDefined("SCRIPT_NAME")>Called from: #htmleditformat(request.query_string_and_script_name)#<br></CFIF>
					Cannot find template #htmleditformat(cfcatch.missingFileName)#.<BR>
				<CFIF isDefined("CGI.HTTP_REFERER")>Referrer: #htmleditformat(CGI.HTTP_REFERER)#</CFIF>
			
			Template parameter currently defined as : #htmleditformat(template)#
		</p></td>
		</tr>
		</table>
		</CFOUTPUT>
	<cfelse>
		<CFTRY>
			<cfsetting enablecfoutputonly="no" >
			<cfinclude template="/#template#">
			<cfsetting enablecfoutputonly="Yes" >
			<cfcatch type="MissingInclude">
				<CFOUTPUT>
				<h2>RELAY_INCLUDETEMPLATE information</h2>
					<strong>Cannot find:</strong> #htmleditformat(cfcatch.missingFileName)#<BR><br>
					<CFIF isDefined("SCRIPT_NAME")><strong>Called within:</strong> #htmleditformat(request.query_string_and_script_name)#<br></CFIF>
					<CFIF isDefined("CGI.HTTP_REFERER")><strong>Referrer:</strong> #htmleditformat(CGI.HTTP_REFERER)#</CFIF>
				</CFOUTPUT>
			</cfcatch>
			<cfcatch type="Any">
				<cfrethrow>
			</cfcatch>
		</CFTRY>
	</CFIF>
<CFELSE>
	<CFOUTPUT>
	<table border="2">
		<tr>
		<td><p>
				No template has been defined to call.<BR><br>
				<CFIF isDefined("SCRIPT_NAME")><strong>Called within:</strong> #htmleditformat(request.query_string_and_script_name)#<br></CFIF>
				<CFIF isDefined("CGI.HTTP_REFERER")><strong>Referrer:</strong> #htmleditformat(CGI.HTTP_REFERER)#</CFIF>
		</p></td>
		</tr>
	</table>
	</CFOUTPUT>
</CFIF>

<cfsetting enablecfoutputonly="No" >