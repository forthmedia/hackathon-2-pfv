<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			ordersLoginForm.cfm
Author:				SWJ
Date started:		1-Sep-04

Description:		RelayTag used to provide an automated login and registration process
					when a user has selected a productID they want to order.  It works
					in concert with addOrder4SKU.cfm

					calling the tag:
					this should be called using the following example URL string
					et.cfm?etid=orderReg&prodid=1223

					Required variables:
					orderReg = elementTextID for the element that this tag is added to

					optional variables:


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?


Possible enhancements:


 --->

<cf_param name="emailTextID" type="string" default="ordersUserNameEmail"/>
<!--- check the email is set up otherwise we'll get an error later on --->
<cfscript>
	qGetEmailDetails = application.com.email.getEmailDetails(emailTextID);
</cfscript>

<CFIF qGetEmailDetails.recordcount neq 1>

	<cfoutput><p>You must add an email to Workflow/Email with the textID of #htmleditformat(emailTextID)#</p></cfoutput>
	<CF_ABORT>
</CFIF>

<cfif structKeyExists(cookie,"partnersession")>
	...<cfoutput>#htmleditformat(url.prodid)#</cfoutput>
	<cfif isdefined("prodid")>
		<!--- <cflocation url="/orders/addorder4SKU.cfm?prodid=#URL.prodid#&eid=2032" addtoken="No"> --->
		<cfoutput>
		<script>
			//alert("hi");
			window.location.href = '/orders/addorder4SKUinclude.cfm?prodid=#jsStringFormat(prodid)#';
		</script>
		</cfoutput>
	</cfif>
</cfif>



<CFPARAM name="message" default="">
<CFPARAM name="messagePhrase" default="">
<cfparam name="FORM.startNew" type="boolean" default="yes">


<cf_translate>

<CFPARAM NAME="Level" DEFAULT="2">


<cfoutput>
<cfif (structKeyExists(FORM,"ordersLoginUserType") and FORM.ordersLoginUserType eq "newUser")
	or structKeyExists(FORM,"addContactSaveButton")>
	<cfif not structKeyExists(FORM,"addContactSaveButton")>
		phr_orders_LoginForm_CompleteYourDetails
	</cfif>

	<cfparam name="showCols" default="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID" type="string">
	<!--- <cfparam name="mandatoryCols" default="insSalutation,insFirstName,insLastName,insEmail,insSiteName,insAddress1,insPostalCode" type="string"> --->
	<cfparam name="mandatoryCols" default="#application.com.settings.getSetting('plo.mandatoryCols')#" type="string">
	<cfparam name="defaultCountryID" default="9" type="numeric">
	<cfparam name="frmNext" default="return=this" type="string">
	<cfparam name="frmDebug" default="no" type="boolean">
	<cfparam name="showForm" default="yes" type="boolean">
	<cfparam name="showIntroText" default="no" type="boolean">
	<cfparam name="frmReturnMessage" default="no" type="boolean">
<!--- 	<cfparam name="fieldListQuery" default="relayFieldList" type="string"> --->
	<cfparam name="ProcessID" default="0" type="numeric">
	<cfset FORM.startNew = "No">

	<cf_addRelayContactDataForm
		showCols = "#showCols#"
		mandatoryCols = "#mandatoryCols#"
		defaultCountryID = "#defaultCountryID#"
		frmNext = "#frmNext#"
		frmDebug = "#frmDebug#"
		showIntroText = "#showIntroText#"
		URLRoot = ""
		flagStructure = ""
		frmReturnMessage = "#frmReturnMessage#"
		personDataOnly = 0
		<!--- fieldListQuery = "#fieldListQuery#" --->
		>
</cfif>

<cfif structKeyExists(FORM,"addContactSaveButton")>
	<!---  --->
	<cfset FORM.startNew = "No">
	<cfparam name="portalMemberFlagTextID" type="string" default="portalMember">

	<cfscript>
	// add them to the members flag
	application.com.flag.setBooleanFlag(thisPersonID,portalMemberFlagTextID);
	application.com.login.createUserNamePassword(thisPersonID);
	application.com.email.sendEmail(emailTextID,thisPersonID);
	</cfscript>

	<cfset application.com.globalFunctions.cfcookie(NAME="PARTNERSESSION", VALUE="#Now()#-#variables.level#")>
	<!--- This next step dynamically recreates the partners user group membership.
	Partner user groups are controlled by the flags that a partner has set. --->
	<CF_createUserGroups personid = "#thisPersonID#">
	<!---Get all the usergroups this person is a member of--->
	<cfscript>
		getUserGroups = application.com.login.getUserGroups(thisPersonID);
		application.com.login.insertUsage(thisPersonID);
	</cfscript>
	<!---and set the User Cookie--->
	<cfset application.com.globalFunctions.cfcookie(NAME="USER", VALUE="#thisPersonID#-#application.WebUserID#-#valuelist(getUserGroups.userGroupID)#", EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#")>

	<cfoutput>
		<script>
			//alert("hi");
			window.location.href = '/orders/addorder4SKU.cfm?prodid=#jsStringFormat(prodid)#';
		</script>
	</cfoutput>
</cfif>

<cfif structKeyExists(FORM,"startNew") and FORM.startNew eq "Yes">
<SCRIPT>
	function submitform(ordersLoginUserType){
		if (ordersLoginUserType == "existingUser") {
			form = window.document.myForm2;
			if (form.partnerusername.value == ""){
				alert("<cfoutput>phr_LoginScreen_JS_PleaseEnterUserName</cfoutput>");
				form.partnerusername.focus();
				return
			}
			else if (form.partnerpassword.value == ""){
				alert("<cfoutput>phr_LoginScreen_JS_PleaseEnterPassword</cfoutput>");
				form.partnerpassword.focus();
				return
			}
			else {
				form.submit();
			}
		}
		else
		{
			form = window.document.myForm22;
			//form.action = "<cfoutput>/?etid=orderReg<cfif len(request.query_string) gt 0>&#jsStringFormat(request.query_string)#</cfif></cfoutput>";
			//alert("hi");
			form.submit();
		}
	}
</SCRIPT>


<CFIF isDefined("request.relayCurrentUser.personid")>
	<CFQUERY NAME="getUsername" DATASOURCE="#application.sitedatasource#">
		SELECT Username
		FROM Person
		WHERE PersonID = #request.relayCurrentUser.personid#
	</CFQUERY>
</CFIF>

<cfscript>
if (structKeyExists(cookie,"user") and request.relayCurrentUser.personid neq application.UnknownPersonID )
	{
	existingUserChecked = "checked";
	newUserChecked = "";
	}
else
	{
	newUserChecked = "checked";
	existingUserChecked = "";
	}
</cfscript>

<TABLE ALIGN="center" border=0>
<FORM ACTION="/?#request.query_string#" METHOD="post" NAME="myForm2" ID="myForm2" >
	<INPUT TYPE="Hidden" NAME="reLogin" VALUE=""><!--- forces reauthentication and ignores existing cookies --->
	<INPUT TYPE="Hidden" NAME="startNew" VALUE="No">
	<CF_INPUT TYPE="Hidden" NAME="urlRequested" VALUE="etid=orderReg&prodid=#url.prodid#">
	<TR>
		<TD COLSPAN="2">
			<P>phr_orders_LoginForm_Instructions</P>
		</TD>
	</TR>
	<CFIF IsDefined("Fault")>
		<TR>
			<TD colspan="2" align="left">
				<P STYLE="color: Red;">
					<B>phr_orders_LoginForm_InvalidUsername</B>
				</p>
			</TD>
	   	</TR>
	</CFIF>
	<TR>
 		<TD colspan="2" align="left">
			<P><input type="radio" name="ordersLoginUserType" value="existingUser" #existingUserChecked#>phr_orders_LoginForm_IAmAnExistingUser</P>
		</TD>
     </TR>

	<TR>
		<TD><P>phr_orders_LoginForm_Username</P></TD>
		<TD ALIGN="left"><INPUT TYPE="text" NAME="partnerusername" <CFIF isDefined("request.relayCurrentUser.personid")><CFIF getUsername.RecordCount EQ 1>value="#htmleditformat(getUsername.Username)#"</CFIF></CFIF> ></TD>
	</TR>

	<TR>
		<TD><P>phr_orders_LoginForm_Password</P></TD>
		<TD ALIGN="left"><INPUT TYPE="password" NAME="partnerpassword"></TD>
	</TR>

	<CFIF Isdefined("AppName") and appname is not "" and appname is not "default">
		<CF_INPUT TYPE="Hidden" NAME="Level" VALUE="#Level#">
		<CF_INPUT TYPE="Hidden" NAME="AppName" VALUE="#AppName#">
	</CFIF>

	<cfif isDefined("urlRequested")>
		<CF_INPUT TYPE="Hidden" NAME="urlRequested" VALUE="#urlRequested#">
		<CF_INPUT TYPE="Hidden" NAME="Level" VALUE="#Level#">
	</cfif>
	<TR>
		<TD>&nbsp;</TD>
		<TD align="left">
			<input type="button" onClick="javascript:submitform('existingUser')" value="phr_Continue" class="button">
		</TD>
     </TR>
	</FORM>
	<FORM ACTION="/?#request.query_string#" METHOD="post" NAME="myForm22" ID="myForm22" >
	<TR>
	 	<TD colspan="2" align="left" height="10">&nbsp;</TD></tr><tr>
 		<TD colspan="2" align="left">
			<input type="radio" name="ordersLoginUserType" value="newUser" onclick="javascript:submitform('newUser')">phr_orders_LoginForm_IAmANewUser</P>
		</TD>
	</TR>
	</FORM>

	<!--- Check whether an element is defined in this element tree
	 to show resend password link or not --->
	<CFQUERY NAME="IsResendOK" DATASOURCE="#application.sitedatasource#">
		SELECT 1 FROM element WHERE elementtextid='resendpw'
	</CFQUERY>

	<CFIF IsResendOK.recordcount GT 0>
		<TR>
			<TD COLSPAN="2">
				<p>&nbsp;  </p>
				<p><A HREF="/?etid=resendpw">phr_LoginScreen_resendPassword</A></p>
			</TD>
		</TR>
	</CFIF>
</TABLE>
</cfif>

</cfoutput>

</cf_translate>


