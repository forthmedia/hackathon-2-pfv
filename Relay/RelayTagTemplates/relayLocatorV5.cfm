﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!--- Amendment History:
2014-03-28	NYB		Case 439188 Altered the locator so it doesn't fall over when a countryid is passed that there is no LocatorDefinition setup for, in this case the drop down will include a Please Select A Country option that it defaults to
2016/04/07	NJH		Jira BR-636 - removed sensor from google api call as it was deprecated
--->


<cfparam name="countryID" default="#request.relayCurrentUser.content.showForCountryID#">
<cf_param label="Hide Search Profiles by Default" name="hideSearchProfiles" default="yes" type="boolean"/>
<cf_param label="Default View Type" name="defaultViewType" default="List" validValues="List,Map"/>

<cf_head>
	<!--- <style type="text/css" media="screen,print">@import url("/javascript/css/jquery.jscrollpane.css");</style>
	<style type="text/css" media="screen,print">@import url("/code/styles/locator.css");</style> --->

	<cfoutput><script type="text/javascript" src="https://maps.google.com/maps/api/js?language=#request.relaycurrentuser.LANGUAGEISOCODE#"></script></cfoutput>
</cf_head>

<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery.jscrollpane.css">
<cf_includeCssOnce template="/code/styles/locator.css">

<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.mousewheel.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscrollpane.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/scroll-startstop.events.jquery.js">
<cf_includejavascriptOnce template="/javascript/lib/scrollbar.js">
<cf_includejavascriptOnce template="/locator/js/locator.js" translate="true">
<cf_includejavascriptOnce template="/javascript/openWin.js">
<cf_includejavascriptOnce template ="/javascript/checkBoxFunctions.js" >

<!--- setting/getting some filters that may be passed through via the url --->
<cfif structKeyExists(url,"country")>
	<cfif isNumeric(url.country)>
		<cfset countryID = url.country>
	<cfelseif structKeyExists(application.countryIDLookupFromIsoCodeStr,url.country)>
		<cfset countryID = application.countryIDLookupFromIsoCodeStr[url.country]>
	</cfif>
</cfif>

<cfset getLocatorDef = application.com.relayLocator.getLocatorDefinition(countryId=countryId,cache=false)>
<!--- 2014-03-28 NYB Case 439188 moved:
<cfset profileDisplayStruct = application.com.relayLocator.getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList=getLocatorDef.searchCriteriaList)>
--->
<cfset preFilterFlags = "">
<cfset preFilter = false>
<cfset expandFlagGroupDivs = "">
<cfset availableFilters = "company,location,distance">
<!--- START: 2014-03-28 NYB Case 439188 added:--->
<cfset contactReseller = false>
<cf_modalDialog type="iframe" identifier=".viewProfileLink" size="large">

<cfif getLocatorDef.recordcount gt 0>
	<cfset profileDisplayStruct = application.com.relayLocator.getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList=getLocatorDef.searchCriteriaList)>
	<cfset contactReseller = getLocatorDef.contactReseller>
	<!--- END: 2014-03-28 NYB Case 439188 added:--->
	<!--- get rid of unwanted url variables if we're not using geo or the company search, as we pass in the url structure through to the 'getDisplayResults' function later on... --->
	<cfloop list="#availableFilters#" index="filter">
		<cfif structKeyExists(url,filter) and url[filter] neq "">
			<cfif listFindNoCase("location,distance",filter) and not getLocatorDef.useGeo>
				<cfset structDelete(url,filter)>
			<cfelseif filter eq "company" and not getLocatorDef.useCompany>
				<cfset structDelete(url,filter)>
			</cfif>
		</cfif>
	</cfloop>

	<!--- work out whether we are pre-filtering/search the locator --->
	<cfloop collection="#url#" item="urlVar">
		<cfif listFindNoCase("location,company",urlVar)>
			<cfset prefilter = true>
		<cfelse>
			<!--- dealing with potential profiles --->
			<cfset flagGroupStruct = application.com.flag.getFlagGroupStructure(flagGroupID=urlVar)>
			<cfloop list="#url[urlVar]#" index="flagTextID">
				<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagTextID)>
				<cfif flagGroupStruct.isOK and flagStruct.isOK and flagGroupStruct.flagGroupID eq flagStruct.flagGroupID and listFind(profileDisplayStruct.flagIDList,flagStruct.flagID)>
					<cfset preFilterFlags = listAppend(preFilterFlags,flagStruct.flagID)>
					<cfset expandFlagGroupDivs = listAppend(expandFlagGroupDivs,"##"&flagStruct.flagGroupID)>
					<cfset prefilter = true>
				</cfif>
			</cfloop>
		</cfif>
	</cfloop>
<!--- 2014-03-28 NYB Case 439188 added:--->
</cfif>

<cfif prefilter>
	<cfset display_html = application.com.relayLocator.getDisplayResults(countryID=countryID,flagIDList=preFilterFlags,argumentCollection=url)>
</cfif>

<cfoutput>
<div id="locatorContainer" class="noembedly">
<aside class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div id="searchCriteriaContainer"></div>
</aside>
<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopaddingright">
	<div id="locatorResultsContainer">
		<div id="locatorResultsText">
			<h2 id="headingDiv">phr_locator_SearchResults</h2>
			<ul class="viewLinks nav nav-tabs" <cfif not prefilter>style="display:none;"</cfif>>
				<cfloop list="list,map" index="viewType">
				<cfset UseText = "LocatorView_#viewType#">
				<li <cfif viewType eq "list">class="active"</cfif>><a href="" id="#htmlEditFormat(viewType)#viewType" class="btn btn-sm btn-primary" onClick="setTabSelected(this);showViewType('#jsStringFormat(viewType)#');return false;">phr_#UseText#</a></li><!--- RMB - P-KAS024 - changed to transltion for View Link (Map & List) --->
				</cfloop>
			</ul>
		</div>
		<div id="locatorResults" <cfif defaultViewType neq "list" and prefilter>style="display:none;"</cfif>>
			<cfif prefilter>
				#display_html.list#
			<cfelse>
			<div id="locatorResultsData">
				<div>
					<p>phr_locator_description</p>
			    </div>
			</div>
			</cfif>
		</div>
		<!--- 2014-03-28 NYB Case 439188 removed getLocatorDef.:--->
		<div id="contactResellers" <cfif not contactReseller>style="display:none;"</cfif>>
			phr_locator_SelectTheResellersYouWishToContact</br>
			<cf_input type="button" name="frmSendMessage" value="phr_locator_sendmessage" class="btn btn-primary " onClick="javascript:sendMessage();" disabled="#IIF(prefilter,DE('false'),DE('true'))#">
		</div>
	</div>
<div id="locatorResultsMap" <cfif defaultViewType neq "map" and prefilter>style="display:none;"</cfif>>
	<cfif prefilter>
		#display_html.map#
	</cfif>
</div>
</section>


<!--- This is done so that this can be used in javascript.. javascript doesn't recognise yes/no, but rather true/false --->
<cfset hideSearchProfiles = IIF(hideSearchProfiles,DE('true'),DE('false'))>
<cfset flagGroupJSStruct = "">
<cfloop list="#expandFlagGroupDivs#" index="flagGroupDiv">
	<cfset flagGroupJSStruct = listAppend(flagGroupJSStruct,"'###flagGroupDiv#':''")>
</cfloop>

<cfif not hideSearchProfiles and not preFilter>
	<cfloop list="#profileDisplayStruct.sortedFlagGroupAndFlagList#" index="ID">
		<cfset flagGroupJSStruct = listAppend(flagGroupJSStruct,"'###listLast(ID,'|')#':''")>
	</cfloop>
</cfif>

<!--- if we have some prefilters coming through, then don't hide them when we first load the page. After that, if we switch countries, for example, then we hide them
	if the hideSearchProfiles variable is set to true --->
<script>
	getSearchCriteria(#jsStringFormat(countryID)#);
	defaultViewType = '#jsStringFormat(lcase(defaultViewType))#';
	expandFlagGroupDivs = {#flagGroupJSStruct#}; <!---  can't JSStringFormat as we need to keep the single quotes...'structure of divIDs...(including the #)  for example {#145:'',#946:''}--->
</script>
</div>
</cfoutput>

<!--- if some filters have been passed in via the url --->
<cfif preFilter>
	<cfoutput>
		<script>
			<cfif defaultViewType eq "list">initialiseScrollBarForLocator();</cfif>

			function setPreFilters() {
				if (jQuery('.searchItemContainer').length) {
					<cfif preFilterFlags neq "">
					setPreFilterCriteria('flagList','#jsStringFormat(preFilterFlags)#');
					</cfif>
					<cfloop list="#availableFilters#" index="filter">
						<cfif structKeyExists(url,filter) and url[filter] neq "">
						setPreFilterCriteria('#jsStringFormat(filter)#','#jsStringFormat(url[filter])#');
						</cfif>
					</cfloop>
				} else {
					setTimeout(setPreFilters,500);
				}
			}

			setPreFilters();
		</script>
	</cfoutput>
</cfif>