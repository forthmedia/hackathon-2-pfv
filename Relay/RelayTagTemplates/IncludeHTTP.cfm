<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		>>--------------------------<<
			IncludeHTTP Relay Tag
		>>--------------------------<<

		Written By David A McLean May 2002
		
		This Relay Tag will perform an Http Get/Post on a given URL and return content
		to an Element Detail Phrase.
		
		It expects to find the Given URL in the Element parameters column set to
		a variable in the caller scope named "element_parameters".
		
		Supported Element Parameters
--->

<CFIF Not IsDefined("element_parameters")>
	<p style="font-weight: bold;">Problem - no element parameter variable defined for IncludeHTTP</p>
	<ol>
		<li>Add the following string the paramaters section: IncludeFile=http://URLString where URLString is the corrent path to the data you wish to include.</li>
		<li>If you want to append URL variables to the calling string then you must end the URLString in a ?.</li>
		<li>You can also include a file to do special site specific functions by adding the parameter SpecialRequest=fileName.cfm. This file should be placed in the content/CFTemplates directory. (See User Files Directory Tree below.)</li>
		<li>Use showURL=yes to see the URL that Relay is getting the data from.</li>
		<li>Use replaceIDList=1-2-3 where is a hypendelimted list of ContentString relaceIDs.  This improves processing speed.</li>
	</ol>
	<cfexit method="EXITTEMPLATE">
</CFIF>


<CFIF Not IsDefined("IncludeFile")>
	<p style="font-weight: bold;">Problem - no IncludeFile variable</p>
	<ol>
		<li>Add the following string the paramaters section: IncludeFile=http://URLString where URLString is the corrent path to the data you wish to include.</li>
		<li>If you want to append URL variables to the calling string then you must end the URLString in a ?.</li>
		<li>You can also include a file to do special site specific functions by adding the parameter SpecialRequest=fileName.cfm. This file should be placed in the content/CFTemplates directory. (See User Files Directory Tree below.)</li>
		<li>Use showURL=yes to see the URL that Relay is getting the data from.</li>
		<li>Use replaceIDList=1-2-3 where is a hypen delimted list of ContentString relaceIDs.  This improves processing speed.</li>
	</ol>

	<cfexit method="EXITTEMPLATE">
</CFIF>

<CFIF IsDefined("SpecialRequest")>
	<cfinclude template="/content/CFMTemplates/#SpecialRequest#">
<CFELSE>
	<CFHTTP URL="#IncludeFile#" METHOD="GET"></CFHTTP>
	<CFSET TheContent=cfhttp.fileContent>
</CFIF>



<CFIF IsDefined("Patt_Begin") AND IsDefined("Patt_End")>
	<CFSET x = findnoCase(Patt_Begin,TheContent,1) + Len(Patt_Begin)>
	<CFSET y = findnoCase(trim(Patt_End),TheContent,x)>
	<CFIF x LT y>
		<CFSET TheFinalContent=Mid(TheContent, x, y-x)>
	<CFELSE>
		<CFSET TheFinalContent="x=#x# y=#y# Patt_Begin=#HTMLEditFormat(Patt_Begin)# Patt_End=#HTMLEditFormat(Patt_End)# x <BR>#TheContent#">
	</CFIF>
<CFELSE>
	<CFSET TheFinalContent=TheContent>
</CFIF>

<!--- output the final content --->
<CFIF TheFinalContent neq "Connection Failure">
	<CFOUTPUT>	
	<cfif isDefined("showURL") and isContentEditor.recordcount GT 0>
		<p>Relay Included data from the following URL: <br><a href="#IncludeFile#" target="_blank">#htmleditformat(IncludeFile)#</a></p>
		<p><cfif isDefined("replaceIDList")> replaceIDList = #htmleditformat(replaceIDList)#</cfif></p>
	</cfif>
	#htmleditformat(TheFinalContent)#
	</CFOUTPUT>
</CFIF>


