<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			actionListExternal.cfm	
Author:				SWJ
Date started:		2004-04-02
	
Purpose:	To provide a listing screen for actions.

Usage:	



Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 
<!--- set the default parameters which can be superceded by setting params in the element  --->
<cfparam name="projectID" default="" type="string">
<!--- st = screenType l=listing, e=edit --->
<cfparam name="st" default="l" type="string">
<cfparam name="fieldListQuery" default="relayFieldList" type="string">
<CFSET SRCHPERSONID=request.relayCurrentUser.personid>
<CFSET SRCHDATETYPE="Started">
<CFSET SRCHACTIONLIVE="on">
<cfset hideRightBorder="yes">
<cfparam name="PortalID" default="0">

<!--- <cfif not isNumeric("SRCHPERSONID")>
	Relay_ActionListExternal tag requires you to be logged in to review actions.
	<cfexit method="EXITTEMPLATE">
</cfif>

<cfif not isDefined("formCompletePhraseTextID")>
	Relay_AddContactForm tag requires a value for formCompletePhraseTextID. Please define this in the element paramaters.
	<cfexit method="EXITTEMPLATE">
</cfif> --->

<!--- Added switch to determine whether actions are for portal vieww are not  --->

<CFIF PortalID EQ 1>

	<cfparam name="thisDir" default="#cf_template_path#"><!--- temp path setting --->
	<cfif st eq "l">
		<cfinclude template="/code/cftemplates/_ActionList.CFM"> 
	<cfelseif st eq "e">
		<CFINCLUDE TEMPLATE="#appfiles#/actionEdit.CFM">
	<cfelseif st eq "a">
		<CFINCLUDE TEMPLATE="#appfiles#/addQuickAction.CFM">
	</cfif>

<cfelse> <!-- Relay view ---> 

<cfparam name="thisDir" default="actions">
	<cfif st eq "l">
		<cfinclude template="/actions/_ActionList.CFM"> 
	<cfelseif st eq "e">
		<cfinclude template="/actions/actionEdit.CFM">
	</cfif>

</CFIF> 

<CFSET current ="ActionList">


