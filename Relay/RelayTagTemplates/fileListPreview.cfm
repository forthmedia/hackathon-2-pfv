<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		fileListPreview.cfm
Author:			NJH
Date started:	23-01-2013

Description:	Page to show files in preview mode using embedly

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_param name="FileTypeGroupID" label="Media Group" required="true" validValues="select heading as display, fileTypeGroupID as value from fileTypeGroup"/>
<cf_param name="FileTypeID" label="Resource Type" default="" nulltext="All Types" relayFormElementType="select" bindFunction="cfc:webservices.fileWS.callMethod(methodName='getFileTypes',fileTypeGroupID={FileTypeGroupID},returnType='query')" display="type" value="FileTypeID"/>
<cf_param name="fileSortOrder" multiple="true" displayAs="twoSelects" validValues="name,fileURL" default="name" size="3" allowSort="true"/>

<cfset embedlyDisplayMethod = "replaceParent">
<cfset filterByLanguage = false>

<cfinclude template="\fileManagement\fileListPreview.cfm">