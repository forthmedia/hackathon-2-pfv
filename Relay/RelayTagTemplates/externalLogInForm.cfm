<!--- �Relayware. All Rights Reserved 2014 
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
--->


<cfparam name="message" default="">
<cfparam name="messagePhrase" default="">
<cfparam name="phraseprefix" default="">
<cfparam name="eidPostLogin" default="">
<cfparam name="securityProfileID" default="">
<cfparam name="showWelcomeTitle" type="boolean" default="no">
<cfparam name="showPleaseLogin" type="boolean" default="no">
<cfparam name="showRegisterLink" type="boolean" default="yes">
<cfparam name="showResendLink" type="boolean" default="yes">
<cfparam name="submitImage" type="string" default="">
<cfparam name="showSubmitPhrase" type="boolean" default="yes">
<cfparam name="showRememberMe" type="boolean" default="no">
<cfparam name="useDynamicProfilechecks" type="boolean" default="false">
<cf_translate>

<CFPARAM NAME="Level" DEFAULT="2">

<cfset usernameErrorMessage = "phr_#phraseprefix#LoginScreen_JS_PleaseEnterUserName">
<cfset passwordErrorMessage = "phr_#phraseprefix#LoginScreen_JS_PleaseEnterPassword">

<cf_translate phrases="#usernameErrorMessage#,#passwordErrorMessage#"/>

<!--- <SCRIPT>
	function submitform(){
		form = window.document.loginForm;
		if (form.partnerusername.value == ""){
			alert("<cfoutput>phr_#phraseprefix#LoginScreen_JS_PleaseEnterUserName</cfoutput>");
			form.partnerusername.focus();
			return
		}
		else if (form.partnerpassword.value == ""){
			alert("<cfoutput>phr_#phraseprefix#LoginScreen_JS_PleaseEnterPassword</cfoutput>");
			form.partnerpassword.focus();
			return
		}
		else { 
			form.submit();
		}
	}
</SCRIPT> --->
		
<!--- WAB added 2007-01-15 for Trend  --->
<cfif structKeyExists(URL, "EPL")><cfset eidPostLogin = URL.EPL></cfif>
		
<cfif request.relayCurrentUser.isunknownuser>
	<cfset username = "">
<cfelse>
	<cfset username = request.relayCurrentUser.person.username>
</cfif>



<cfoutput>

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfform action="/?#request.query_string#" method="post" name="loginForm" id="loginForm" >	
	<cf_relayFormElement relayFormElementType="Hidden" fieldname="reLogin" currentValue="" label=""><!--- forces reauthentication and ignores existing cookies --->
	
<cf_relayFormDisplay class="phr_#phraseprefix#loginTable">
	<cfif showWelcomeTitle>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="phr_#phraseprefix#LoginScreen_WelcomeTitle" spanCols="yes" valueAlign="center">
	</cfif>
	<cfif IsDefined("request.Fault")>
		<cf_relayFormElementDisplay relayFormElementType="MESSAGE" fieldname="" label="" currentValue="phr_#phraseprefix#LoginScreen_JS_InvalidUsername" spanCols="yes" valueAlign="center">
	</cfif>
	
	<cfif showPleaseLogin>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="phr_#phraseprefix#LoginScreen_PleaseLogin" spanCols="yes" valueAlign="center">
	</cfif>
	
	
	<cf_relayFormElementDisplay relayFormElementType="text" fieldname="partnerusername" label="phr_#phraseprefix#LoginScreen_Username" currentValue="#username#" required="yes" size="15" message="#request.translations[usernameErrorMessage]#">

	<cf_relayFormElementDisplay relayFormElementType="password" fieldname="partnerpassword" label="phr_#phraseprefix#LoginScreen_Password" currentValue="" required="yes" size="15" message="#request.translations[passwordErrorMessage]#">
	
	<CFIF Isdefined("AppName") and appname is not "" and appname is not "default">
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="Level" currentValue="#Level#" label="">
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="AppName" currentValue="#AppName#" label="">
	</CFIF>
	
	<cfif isDefined("urlRequested")>
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="urlRequested" currentValue="#urlRequested#" label="">
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="Level" currentValue="#Level#" label="">
	<cfelseif isDefined("eidPostLogin")>  <!--- WAb added this 2005-03-15 copied from a sony login --->
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="urlRequested" currentValue="eid=#eidPostLogin#" label="">
	</cfif>
	<cfif isdefined("securityProfileID") and securityProfileID neq "">
		<cf_relayFormElement relayFormElementType="Hidden" fieldName="securityProfileID" currentValue="#securityProfileID#" label="">
	</cfif>
 	<cfif isDefined("submitImage") and submitImage neq "">
		<cfif showSubmitPhrase>
			<tr>
				<td colspan="2" class="loginButton"><span>phr_#htmleditformat(phraseprefix)#LoginScreen_Submit</span> <cf_relayFormElement relayFormElementType="image" fieldName="loginButton" label="" currentValue="" onClick="submitForm();" imageSrc="#submitImage#"></td>
			</tr>
		<cfelse>
			<cf_relayFormElementDisplay relayFormElementType="image" trID="loginSubmit" fieldName="loginButton" label="" currentValue="" onClick="loginForm.submit();" imageSrc="#submitImage#" spanCols="yes" valueAlign="right">
		</cfif>
 	<cfelse>
		<cf_relayFormElementDisplay relayFormElementType="submit" trID="loginSubmit" fieldname="loginButton" label="" currentValue="phr_#phraseprefix#LoginScreen_Login">	
	</cfif>

	<cfif showRememberMe>
		<cfif structkeyExists(cookie,"dejavu") and cookie.dejavu eq request.relaycurrentuser.personID>
			<cfset rememberMeValue = 1>
		<cfelse>
			<cfset rememberMeValue = 0>
		</cfif>
		<cf_relayFormElementDisplay relayFormElementType="checkbox" trID="rememberMe" fieldname="setRememberMe" label="phr_Remember_Me" currentValue="#rememberMeValue#" value="1" valueAlign="left">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="setRememberMe_Exists" currentValue="true">
	</cfif>

	<cfif showRegisterLink>
		<cf_relayFormElementDisplay relayFormElementType="HTML" label="phr_#phraseprefix#LoginScreen_NewUser" currentValue="">
			<A HREF="javascript:window.location.href='/partners/login.cfm?appname=PartnerPassword&x=#timeformat(now(),'mmss')#'<CFIF IsDefined('appname')>+'&nextappname=#htmleditformat(appname)#'</CFIF>">phr_#htmleditformat(phraseprefix)#LoginScreen_RegisterForPassword</A>
		</cf_relayFormElementDisplay>
<!--- 		<TR>
			<TD COLSPAN="2">
				<!--- 2001-02-22 - CPS: added a non breaking space after the <A> tag as this was making the anchor inoperable in Netscape --->
				<P>phr_#phraseprefix#LoginScreen_NewUser&nbsp;</P>
				<P><A HREF="javascript:window.location.href='#request.currentSite.httpProtocol##request.relayCurrentUser.sitedomainAndRoot#/partners/login.cfm?appname=PartnerPassword&x=#timeformat(now(),"mmss")#'<CFIF IsDefined('appname')>+'&nextappname=#appname#'</CFIF>">phr_#phraseprefix#LoginScreen_RegisterForPassword</A></P>
			</TD>
			</P>
		</TR>  --->
	</cfif>

	<!--- DAM 27 Aug 2002 Determine whether to show resend password link or not --->
	<cfquery name="IsResendOK" datasource="#application.sitedatasource#">
		SELECT 1 FROM element WHERE elementtextid =  <cf_queryparam value="#phraseprefix#resendpw" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	
	<cfif IsResendOK.recordcount GT 0 and showResendLink>
		<cf_relayFormElementDisplay relayFormElementType="HTML" label="phr_#phraseprefix#LoginScreen_ResendPassword" currentValue="<a href='/elementTemplate.cfm?etid=#phraseprefix#resendpw'>phr_#htmleditformat(phraseprefix)#LoginScreen_resendPassword</a>">
<!--- 		<TR>
			<TD COLSPAN="2">
				<p>&nbsp;  </p>
				<p><A HREF="/elementTemplate.cfm?etid=#phraseprefix#resendpw">phr_#phraseprefix#LoginScreen_resendPassword</A></p>
			</TD>
		</TR> --->
	</CFIF>
</cf_relayFormDisplay>

</cfform>
	
</cfoutput>			
	
</cf_translate>

