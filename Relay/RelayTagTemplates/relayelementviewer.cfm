<!--- �Relayware. All Rights Reserved 2014 --->
<cf_param name="flagID" type="string" default=""/>
<cf_param name="treeTop" type="numeric" default="-1"/>
<cf_param name="displayMode" type="string" default="linkList" validvalues="DropDown,LinkList,Accordion"/> <!--- 2012-10-19 PPB Case 431263 typo correction ---> 
<cf_param name="showLinkIcon" type="boolean" default="no"/>
<cf_param name="linkIconSrc" type="string" default="blank.gif"/>
<cf_param name="showContentHeadline" type="boolean" default="yes"/>
<cf_param name="contentHeadlineClass" type="string" default=""/>
<cf_param name="numberofgenerations" type="numeric" default="2"/>
<cf_param name="numberofgenerationsofchildren" type="numeric" default="2"/>
<cf_param name="displayEID" type="numeric" default="0"/>
<cf_param name="elementIndex" type="numeric" default="1"/>
<cfparam name="request.linkIconDir" type="string" default="/code/borders/Images"> <!--- override in relayBordersINI.cfm --->

<cf_translate>
<!--- initialising a variable that we use later on determining whether the user have rights to view the specified eid? --->
<cfset hasRightsToElement = true>

<cfif treeTop eq "-1">
	<cfset treeTop = request.currentElement.id>
</cfif>

<cfset qGetChildren = application.com.relayElementTree.convertTreeToBranch(
						elementTree = request.currentElementTree, 
						topOfBranchID = treeTop,
						currentelementid = treeTop,
						showCurrentElementChildren = true , 
						numberofgenerations = numberofgenerations,
						numberofgenerationsofchildren = numberofgenerationsofchildren)>	
						

<cfif flagID neq "">
	<cfif application.com.flag.doesFlagExist(flagID=#flagID#)>
		<cfset qGetChildrenByFlag = application.com.relayElementTree.getElementsFlaggedWithParticularFlag(elementtree = qGetChildren,flagid=#application.com.flag.getFlagStructure(flagID).flagID#<!--- ,demoteVisitedElements = true --->)>
	<cfelse>
		<cfoutput>'#flagID#' is not a valid flag id.</cfoutput>
		<cfreturn>
	</cfif>
<cfelse>
	<cfset qGetChildrenByFlag = qGetChildren>
</cfif>
<cfset siblings = ValueList(qGetChildrenByFlag.node)>

<cfif not isDefined("url.displayEID") and displayEID eq 0>
	<cfif qGetChildrenByFlag.recordCount gt 0>
		<cfif qGetChildrenByFlag.recordCount gte elementIndex>
			<cfset displayEID =  qGetChildrenByFlag.ID[elementIndex]>
		<cfelse>
			<cfset displayEID =  qGetChildrenByFlag.ID[1]>
		</cfif>
	<cfelse>
		<cfset displayEID = request.currentElement.id>
	</cfif>
<cfelseif isDefined("url.displayEID")>
	<cfset displayEID = url.displayEID>
</cfif>

<cfset childrenIDList = valueList(qGetChildrenByFlag.ID)>
<!--- are we allowed to see this eid?? If not, set it to be the parent--->
<cfif not listContains(childrenIDList,displayEID)>
	<cfset hasRightsToElement = false>
</cfif>


<cfswitch expression="#displayMode#">

	<cfcase value="accordion">
		
		<cfset queryAddColumn(qGetChildren,"detail",arrayNew(1))>
		<cfloop query = "qGetChildren">
			<cfset querySetCell(qGetChildren,"detail","phr_detail_element_"&qGetChildren["id"][currentrow],currentrow)>
		</cfloop>
		<cf_translateQueryColumn query = "#qGetChildren#" columnname = "detail" >
		<cf_translateQueryColumn query = "#qGetChildren#" columnname = "headline">			

		<cf_executeRelayTag>
			<cf_ricoAccordion
				ContentQuery = #qGetChildren#
				TitleColumn = "headline"
				DetailColumn = "detail"
				width = 750
				height = "400"
			>	
		</cf_executeRelayTag>
	
	</cfcase>
	
		
	<cfdefaultcase>
	
<script>
	function submitForm(goToEid){
		if (goToEid == null) {
			<cfif displayMode eq "dropdown">
				var element = elementForm.elementHeadlines;
				goToEid = element.options[element.selectedIndex].value;
			</cfif>
		}
		elementForm.displayEID.value = goToEid;
		elementForm.submit();
	}
</script>

<cfform name="elementForm" action="/" method="get">
	<cfinput type="hidden" name="displayEID" value="">
	<cfinput type="hidden" name="eid" value="#request.currentElement.id#">
	
	<cfif displayMode eq "linkList">
		<cfoutput>
			<cfif qGetChildrenByFlag.recordCount gt 1>
				<table width="100%" cellpadding=0 cellspacing=0 border=0 class="elementViewerTable">
					<cfloop query="qGetChildrenByFlag">
					<tr>
						<td <cfif ID eq displayEID>class="active"</cfif>>
							<a href="##" onClick="submitForm(#ID#);">
							
							<cfif showLinkIcon>
								<cfif linkIconSrc neq "">
									<img src="#request.linkIconDir#/#linkIconSrc#">
								</cfif>
							</cfif>
							
							#htmleditformat(Headline)#</a>
						</td>
					</tr>
					</cfloop>
				</table>
			</cfif>
		</cfoutput>
		
	<cfelseif displayMode eq "dropDown">
		<cfif qGetChildrenByFlag.recordCount gt 1>
			<table width="100%" cellpadding=0 cellspacing=0 border=0 class="elementViewerTable">
				<tr>
					<td>
					<cfselect name="elementHeadlines" query="qGetChildrenByFlag" display="Headline" value="ID" selected="#displayEID#" onChange="submitForm();"/>
					</td>
				</tr>
			</table>
		</cfif>
	
	<cfelseif displayMode eq "none">
	
	</cfif>
	
</cfform>

<!--- only show content --->
		<cfif displayEID neq request.currentElement.id and hasRightsToElement>
	<cfoutput>
		<table width="100%" cellpadding=0 cellspacing=0 border=0>
			<cfif showContentHeadline>
				<tr>
					<td colspan="2" class="#contentHeadlineClass#">phr_headline_element_#htmleditformat(displayEID)#</td>
				</tr>
			</cfif>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
							<!--- WAB 2006-11-27 added in some special checks to make sure that an elementViewer is not included within an elementviewer --->
							<cf_translate phrases = "phr_detail_element_#displayEID#" onNullShowPhraseTextID=false />
							<cfset detail = request.translations["phr_detail_element_#displayEID#"]>
							<cfset re="<(RELAY_ElementViewer.*?)>" >
							<cfset detail = rereplacenocase (detail,re,"","ALL")>
							
					<cf_executeRelayTag>
								<cfoutput>
									#detail#
								</cfoutput>
					</cf_executeRelayTag>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<cfif listLen(siblings) gt 0>
				<cfset index = listFind(siblings,displayEID)>
				<cfif index neq 0>
					<tr>
						<td align="left">
			 				<cfif listFirst(siblings) neq displayEID>
								<cfset previousEID = listGetAt(siblings,index-1)>
								<a onClick="submitForm(#previousEID#);"><div class="pageNavigationDiv"><< phr_element_previous</div></a>
							</cfif>
						</td>
						<td align="right">
			 				<cfif listLast(siblings) neq displayEID>
								<cfset nextEID = listGetAt(siblings,index+1)>
								<a onClick="submitForm(#nextEID#);"><div class="pageNavigationDiv">phr_element_next >></div></a>
							</cfif>
						</td>
					</tr>
				</cfif>
			</cfif>
		</table>
	</cfoutput>
		<cfelse>
			<cfif not hasRightsToElement>
				<cfoutput>phr_element_YouDoNotHaveRightsToViewElement 'phr_headline_element_#htmleditformat(displayEID)#' (eid #htmleditformat(displayEID)#)</cfoutput>
			</cfif>
</cfif>

	</cfdefaultcase>	

</cfswitch>
</cf_translate>
