<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		dashboard.cfm	
Author:			NJH  
Date started:	01-12-2010
	
Description:	Template to show JasperServer dashboards		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/02/04	YMA		Closed iframe.  This was preventing rest of page content from rendering as it was left un-closed.
2013/02/04	YMA		Displaying more than 1 report or dashboard on a page means only 1 loading message gets removed on report load.  Changed to reference a unique ID to handle this.

Possible enhancements:


 --->

<cf_param label="Dashboard" name="dashboardURI" validValues="func:com.jasperServer.getDashboards(showInternalExternal='External')" display="dashboardURI" value="dashboardURI"/> <!--- NOTE: this URI is case sensitive!!!! --->
<cf_param label="Height" name="height" type="string" default="600"/>
<cf_param label="Width" name="width" type="string" default="100%"/>

<cfset resourceURI = dashboardURI>
<cfset resource = "dashboard">

<cf_include template="/jasperServer/loadJasperServerResource.cfm">