<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	DistributorList.cfm

	This template will return a list of distis defined in the database using
	a view called vDistiList.  This view creates a list of distributors and
	data related to them in text flags.  It is called in qryGetDistributors early
	on in the template.

	The template checks to see if the current user belongs to an organisation
	with 'tied' distis.  These are defined as a text string in a text flag record.

	Parameters:
		frmCountryID (optional): this will constrain the list to the coutries
		defined in frmCountryID.
		frmOrgIDList (optional): this will constrain the list to the orgs
		defined in frmOrgIDList.  This is used when a reseller has a defined set of
		distis they deal with exclusively.

Amendment History:

16.3.01		WAB		removed some spacers
07-Mar-2001	CPS 	Ensure that organisationId and OrganisationName are displayed for the supplier/Distributor
2002-01-25	SWJ 	Added check to see if we have a defined list if distis for
					the partner viewing this page and the pass this to
					qryGetDistributors via frmOrgIDList to reduce the list shown
2002-01-31	SWJ		Modified the code to show a disti logo from the FNL library if its there and show a country flag
2002-02-15	SWJ		Modified code to manage distributor type
2002-03-19	SWJ		Add 'DistiList_IntroPara' as a phrase so you can put an introductory para into the screen

--->

<!--- check the DistiIDList textFlagData for a list of assigned distis --->
<cfquery name="checkAssignedDistis" datasource="#application.sitedatasource#" dbtype="ODBC">
	select data from textflagData
	where entityID=(select organisationID from person where personID=<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">)
	and flagID=(select flagID from flag where flagTextID='DistiIDList')
</cfquery>
<!--- if you find any set frmOrgIDList to that list.  This will be used by qryGetDistributors --->
<cfif isDefined('checkAssignedDistis.Data') and checkAssignedDistis.Data neq "">
	<cfset frmOrgIDList = #checkAssignedDistis.Data#>
</cfif>

<CFQUERY NAME="getUserCountry" datasource="#application.sitedatasource#" DBTYPE="ODBC">
select l.countryID
	from person p inner join location l on p.locationID=l.locationID
	where personID=<cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="cf_sql_integer">
</CFQUERY>

<cfif isDefined('getUserCountry.countryID') and getUserCountry.countryID neq "">
	<cfset currentCountryID = #getUserCountry.countryID#>
	<CFSET frmCountryIDList=currentCountryID>
</cfif>


<cfinclude template="/templates/qryGetDistributors.cfm">

<!--- get a distinct list of distiTypes and create phrases for translation --->
<CFQUERY NAME="GetDistiTypes" DBTYPE="query">
	select distinct distiType from getDistis
</CFQUERY>
<cfset distiPhrase = listqualify(valuelist(getDistiTypes.distiType),"'")>


<cf_translate>
<TABLE WIDTH="450" BORDER="0" CELLSPACING="0" CELLPADDING="2">
	<TR>
		<!--- <TD WIDTH="40" HEIGHT="40">&nbsp;</TD> --->
		<TD COLSPAN="3" NOWRAP STYLE="margin-left: 25px;"><H1>phr_DistiList_PageTitle</H1></TD>
	</TR>
	<CFOUTPUT QUERY="getDistis" group="countryDescription">
	  <tr>
	  	<!--- <TD WIDTH="40">&nbsp;</TD> --->
	    	<TD COLSPAN="3" NOWRAP STYLE="margin-left: 25px;">
	        	<div align="left"><img src="/images/flags/#ISOCode#FlagAnim.gif"><!--- <B>#CountryDescription#</B> ---></div>
	      	</TD>
	  </tr>
	  </CFOUTPUT>
	  	<TR>
			<TD COLSPAN="3" STYLE="margin-left: 25px;">phr_DistiList_IntroPara</TD>
		</TR>



<CFOUTPUT QUERY="GetDistis" GROUP="distiType" GROUPCASESENSITIVE="Yes">
	<CFIF getDistiTypes.distiType neq "">
		<TR>
			<!--- <TD WIDTH="40">&nbsp;</TD> --->
			<TD HEIGHT="25px" COLSPAN="3" VALIGN="bottom" NOWRAP STYLE="margin-left: 25px; text-decoration: underline;"><BR><BR><b>#evaluate("phr_#htmleditformat(DistiType)#")#</b></TD>
		</TR>
	</cfif>
	  <cfoutput>
	  	<CFSET logoFilePath=replace("#application.path#\..\images\orgLogos\#logofile#","/","\")>

	    <tr>
			<TD WIDTH="40">&nbsp;</TD>
			<TD WIDTH="100px">
				<cfif logofile neq "" and fileExists(logoFilePath)>
					<img src="/images/MISC/spacer.gif" WIDTH="1" HEIGHT="50" BORDER="0"><A HREF="#SpecificURL#" TARGET="_blank"><IMG SRC="/images/orgLogos/#logofile#" ALT="#OrganisationName#" BORDER="0"></A>
				<cfelse>
					<img src="/images/MISC/spacer.gif" WIDTH="100" HEIGHT="50" BORDER="0">
				</cfif>
			</TD>
			<TD VALIGN="bottom">
				<CFIF SpecificURL NEQ "">
					<!--- <A HREF="#SpecificURL#" TARGET="_blank">#OrganisationName#</A>&nbsp; --->&nbsp;&nbsp;Tel: #htmleditformat(Telephone)#<!---  - Fax: #Fax# --->
				<CFELSE>
					#htmleditformat(OrganisationName)#&nbsp; Tel: #htmleditformat(Telephone)#<!---  - Fax: #Fax# --->
				</CFIF>
			</TD>
	    </tr>
		<TR><TD WIDTH="40">&nbsp;</TD><TD COLSPAN="2" ALIGN="left"><HR ALIGN="left" WIDTH="100%" SIZE="1"></TD></TR>
	  </cfoutput>
</cfoutput>
</table>
<cf_translate>
<p>&nbsp;</p>
<p>&nbsp;</p>

