<!--- �Relayware. All Rights Reserved 2014 --->

<!--- PersonRoleProfiles 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->




<CFPARAM NAME="nSelect" DEFAULT="p1.sex, p1.JobDesc as JobDescription, p1.firstname, p1.lastname, f.name AS Role,f.flagtextid AS RoleImage, p1.personid AS Image, p1.FirstName + ' ' + p1.LastName AS Name, p1.OfficePhone, p1.MobilePhone, p1.Email">
<CFPARAM Name="nFieldOrder" DEFAULT="Name,JobDescription,OfficePhone,Email">
<CFPARAM Name="nField" DEFAULT="Name,Job Description,Office Phone,Email">
<CFPARAM NAME="UseMailto" DEFAULT="Email">
<CFPARAM Name="PersonalDescriptions" DEFAULT="">
<CFPARAM Name="Photos" DEFAULT="false">
<CFPARAM Name="DescriptionPlacement" DEFAULT="After">
<CFPARAM Name="Paragraphs" DEFAULT="Open,Close">
<CFPARAM NAME="PhotoBorder" DEFAULT="1">
<CFPARAM NAME="ThisOrgtype" DEFAULT="">
<CFPARAM NAME="ImageDirectory" DEFAULT="">
<CFPARAM NAME="Precedence" DEFAULT="">

<!---
	DAM 17 July 2002
	Some validation of parameter dependancies required

--->


<CFIF IsDefined("orgtype")>
	<CFQUERY NAME="GetOrgType" DATASOURCE="#application.SiteDataSource#">
		SELECT f.name FROM
		flag f INNER JOIN booleanflagdata b ON f.flagid = b.flagid
		INNER JOIN person p ON p.organisationid = b.entityID
							AND p.personid = #request.relayCurrentUser.personid#
		INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid
								AND fg.flaggrouptextid =  <cf_queryparam value="#orgtype#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
	<CFIF GetOrgType.recordcount GT 0>
		<CFSET ThisOrgtype=GetOrgType.name>
	</CFIF>
</CFIF>

<CFOUTPUT><SCRIPT type="text/javascript" SRC="/javascript/dotranslation.js"></SCRIPT></CFOUTPUT>

<CFIF Not Isdefined("RolesFlagGroup")>
	Need to define a Profile containing Organisation Role Attributes
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="GetCountryRolesPersons" DATASOURCE="#application.SiteDataSource#">
	SELECT 1,#preservesinglequotes(nSelect)# FROM
	Person p1 INNER JOIN booleanflagdata b ON p1.personid = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid 
	INNER JOIN flaggroup pfg ON pfg.flaggroupid = fg.parentflaggroupid 
							AND pfg.flaggrouptextid =  <cf_queryparam value="#RolesFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	INNER JOIN Location o ON o.countryid = fg.scope
	INNER JOIN person p ON p.Locationid = o.Locationid
						AND p.personid =  #request.relayCurrentUser.personid#
	ORDER BY p1.lastname
</CFQUERY>
<CFIF GetCountryRolesPersons.recordcount EQ 0>
	<CFQUERY NAME="GetCountryRolesPersons" DATASOURCE="#application.SiteDataSource#">
		SELECT 1,#preservesinglequotes(nSelect)# FROM
		Person p1 INNER JOIN booleanflagdata b ON p1.personid = b.entityid
		INNER JOIN flag f ON f.flagid = b.flagid
		INNER JOIN flaggroup fg ON fg.flaggroupid = f.flaggroupid 
		INNER JOIN flaggroup pfg ON pfg.flaggroupid = fg.parentflaggroupid 
								AND pfg.flaggrouptextid =  <cf_queryparam value="#RolesFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		INNER JOIN countrygroup cg ON cg.countrygroupid = fg.scope
		INNER JOIN location o ON o.countryid = cg.countrymemberid
		
		INNER JOIN person p ON p.locationid = o.locationid
							AND p.personid =  #request.relayCurrentUser.personid#
		ORDER BY p1.lastname
	</CFQUERY>
</CFIF>
<CFSET personList=valuelist(GetCountryRolesPersons.image)>

<CFIF precedence IS NOT "">
	<CFSET n=1>
	<CFLOOP LIST="#precedence#" INDEX="x" DELIMITERS=";">
		<CFSET y = listFind(personList,x)>
		<CFIF y GT 0>
			<CFSET tempList=listdeleteat(personList,y,",")>
			<CFIF n GT listlen(tempList)>
				<CFSET personList = listappend(templist,x,",")>
			<CFELSE>
				<CFSET personList = listinsertat(tempList,n,x,",")>
			</CFIF>
			<CFSET n=n+1>
		</CFIF>
	</CFLOOP>
</CFIF>


<cf_translate>
<TABLE CELLPADDING="0" CELLSPACING="0" border="0">
<CFOUTPUT>
	
		<TR><TD colspan="3">phr_CountryRoles_#htmleditformat(GetCountryRolesPersons.roleimage)#</TD></TR>
	<CFIF Paragraphs contains "open">
		<TR><TD colspan="3">phr_CountryRoles_OpeningParagraphtext</TD></TR>
	</CFIF>
	<TR><TD HEIGHT="16">&nbsp;</TD></TR>
<CFLOOP LIST="#personList#" INDEX="x">
	<CFQUERY NAME="thisPerson" DBTYPE="query">
		SELECT * FROM GetCountryRolesPersons WHERE image = #x#
	</CFQUERY>
	<CFIF IsDefined("thisPerson.Sex")>
	<CFIF thisPerson.sex IS "F">
		<CFSET Sex1 = "she">
		<CFSET Sex2 = "She">
		<CFSET Sex3 = "her">
		<CFSET Sex4 = "Her">
	<CFELSE>
		<CFSET Sex1 = "he">
		<CFSET Sex2 = "He">
		<CFSET Sex3 = "his">
		<CFSET Sex4 = "His">
	</CFIF>
	</CFIF>

<TR>
	<CFIF Photos IS "true">
		<CFIF fileexists("#application.paths.content#\miscimages\RolesPhoto_#thisPerson.image#.gif")>
			<TD valign="top"><IMG SRC="/content/miscimages/#ImageDirectory#RolesPhoto_#thisPerson.image#.gif"  BORDER = "#PhotoBorder#"></TD>
		<CFELSE>
			<TD valign="top"><IMG SRC="/content/miscimages/RolesPhoto_0.gif"  BORDER = "#PhotoBorder#"></TD>
		</CFIF>
	<TD>&nbsp;</TD>
	</CFIF>
	<TD VALIGN="top">
	<TABLE CELLPADDING="0" CELLSPACING="0" border="0">

	<CFIF DescriptionPlacement IS "Before">

		<TR><TD Colspan=3>phr_PersonalRolesDescription_#htmleditformat(thisPerson.image)#</TD></TR>
	</CFIF>
	<CFSET nCount = 1>
	<CFLOOP LIST="#nFieldOrder#" INDEX="x">
		<TR>
		<CFIF thisPerson.columnlist contains x>
		<TD><B>#evaluate("phr_#gettoken(nField,ncount,',')#")#</B></TD><TD>&nbsp;</TD><TD><CFIF UseMailto IS x><A href="mailto:#evaluate('thisPerson.#x#')#">#evaluate("thisPerson.#htmleditformat(x)#")#</A><CFELSE>#evaluate("thisPerson.#htmleditformat(x)#")#</CFIF></TD>
		<CFELSE>
		<TD COLSPAN="3">Bad Column #htmleditformat(thisPerson.x)#</TD>
		</CFIF>	
		</TR>
		<CFSET nCount = nCount + 1>
	</CFLOOP>
	<CFIF DescriptionPlacement IS "After">

		<TR><TD Colspan=3>phr_PersonalRolesDescription_#thisPerson.image#</TD></TR>
	</CFIF>

	<TR><TD HEIGHT="16">&nbsp;</TD></TR>
	</TABLE>
	</TD></TR>
	<TR><TD HEIGHT="16">&nbsp;</TD></TR>
</CFLOOP>

	<CFIF Paragraphs contains "close">
		<TR><TD colspan="3">phr_CountryRoles_ClosingParagraph</TD></TR>
	</CFIF>
	
</CFOUTPUT>
</TABLE>
</cf_translate>

