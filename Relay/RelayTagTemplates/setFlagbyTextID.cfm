<!--- �Relayware. All Rights Reserved 2014 --->
<!--- SWJ 2005-07-13: if set flag is false then the flagValues will simply be deleted 

WAB 2007-02-05 added check for existence of the flag
--->
<cf_param name="flagTextID" default="" label="FlagID: a '-' (hyphen) delimited list of flagIDs"/>
<cf_param name="unsetFlag" default="no" type="boolean"/>
<cfparam name="debug" default="false">

<cfif flagTextID neq "">
	<cfscript>
		listlength = listLen(flagTextID,"-");
		for(i=1; i lte listlength; i=i+1) {		
			
			theFlagID = listGetAt(flagTextID,i,"-") ;

			if (application.com.flag.doesflagexist(theFlagID)) {
				y = application.com.flag.unsetBooleanFlag(entityID=request.relayCurrentUser.personid,flagID=theFlagID);
				if (debug) {writeOutput (theFlagID & ' has been unset<br>');}
				if (not unsetFlag){
					x = application.com.flag.setBooleanFlag(entityID=request.relayCurrentUser.personid,flagTextID=theFlagID);
					if (debug) {writeOutput (theFlagID & ' has been set');}
				}

			} else {
				
				writeoutput ("#theFlagID# does not exist") ;

			}	

				
		}
	</cfscript>

<cfelse>
	You must define the variable FlagTextID for this page.  This can be a hyphen delimited list of flagTextIDs e.g. approved-approvalLevel1.
</cfif>
