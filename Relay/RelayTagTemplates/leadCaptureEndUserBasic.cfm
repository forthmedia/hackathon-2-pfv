<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			endUserLeadCapture.cfm	
Author:				SWJ
Date started:		2005-08-30
	
Description:		This is used to capture a simple lead which is put into the 
					lead table structure.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

20-Jun-2006		NJH		CR_SNY593 Added send email functionality if displayType was 'selectEmail'. 
						Provided ability to add screen to the lead form.
						Added the form wrapper tags.
						
17-apr-2007		SSS		Created a function that will allow a email to be sent about the lead to a person. 
						This can be enabled or disabled via a switch.
2009/07/17		NJH		P-FNL069 - encrypt url variables to entityRelatedFilePopup call.


Possible enhancements:


 --->
 
<cfparam name="leadTypeID" type="numeric" default="1">
<cfparam name="styleSheet" type="string" default="display">
<cfparam name="sendLeadEmail" type="boolean" default="0">
<cfparam name="bccEmailAddress" type="string" default="">

<cf_translate> 

<cfif not isDefined("variables.leadTypeID") or isDefined("URL.help") >
	<p><strong>Required:</strong> leadTypeID - this controls how the lead will display and work. </p>
	<cfexit method="EXITTAG">
</cfif>

<cfif isDefined("URL.help") and structKeyExists(url,"help")>
	<p><strong>Description:</strong> This is used to capture a simple lead which is put into the lead table structure.</p>
	<p>Optional: noEmailConfirmation can be set to yes if you do not want the standard email to be sent post registration.</p>
	<p>Optional: you can use phraseTextIDExtension extension if you want to vary the standard phrases used in the page.</p>
	<p>Syntax in text editor would be: <RELAY_ENDUSERLEADCAPTURE leadTypeID=1> </p>     
</cfif>

<CFQUERY NAME="LeadTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT * from leadType WHERE leadTypeID =  <cf_queryparam value="#variables.leadTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT countryID, countryDescription from country where ISOCode is not null order by countryDescription
</CFQUERY>

<cfif isDefined("form.Company") and isDefined("form.saveLead") >

	<cftransaction>
	<!--- GCC - cfmail here - send to reseller & cc passed in email (or stored in lead config? - my pref) & relayhelpcc from application.emailfrom--->
	<cfinsert datasource="#application.sitedatasource#" tablename="lead" formfields="#form.insertFieldList#">
		<cfquery name="getMax" datasource="#application.SiteDataSource#">
			select max(leadID) as leadID from lead
		</cfquery>
		
		<cfset maxLeadID = #getMax.leadID#>
	</cftransaction>
	
	<cfif leadTypes.useRelatedFiles>
		<cfoutput>
		<p>phr_leadRelatedFilesIntroText#variables.LeadTypeID#</p>		
			<!--- NJH 2009/07/17 P-FNL069 encrypt url variables --->
			<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityType=lead&entityID=#getMax.leadID#")>	
			<p><a href="javascript:openWin('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','MyWindow','width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');">phr_ClickToAddFiles</a></p>
		</cfoutput>
	</cfif>
	
	<cfif isdefined('LeadCaptureThankYouText')>
		<cfoutput>
		<p>#htmleditformat(LeadCaptureThankYouText)#</p>
		</cfoutput>
	<cfelse>
		<p>phr_ThisIsTheTextToShowWhenSubmited</p>
	</cfif>
	<cfif isdefined('leadCaptureScreenID')>
		<cf_updateData> <!--- does the screen update    --->
	</cfif>
	
	<cfquery name="getPersonDets" datasource="#application.SiteDataSource#">
		select * from person where personid = #request.relayCurrentUser.PersonID#
	</cfquery>
	<cfquery name="getLocationDets" datasource="#application.SiteDataSource#">
		select * from location where locationid=#request.relayCurrentUser.locationID#
	</cfquery>
	<cfquery name="getOrgDets" datasource="#application.SiteDataSource#">
		select * from organisation where organisationid=#request.relayCurrentUser.organisationID#
	</cfquery>
	
	<cfset toList = "">
	<cfloop index="emailToIndex" from="1" to="#form.customFieldCount#">
		<cfset field = "form.emailTo" & emailToIndex>
		<cfif isDefined(#field#)>		
			<cfquery name="getEmail" datasource="#application.SiteDataSource#">
				select #evaluate(field)# as emailAddress, ccEmailAddress from lead inner join leadtype on lead.leadtypeID = leadtype.leadtypeID where leadID = #maxLeadID#
			</cfquery>
			
			<!--- the custom field could contain a list of email addresses, so we need to get the individual addresses to validate them --->
			<cfloop list="#getEmail.emailAddress#" index="emailAddress" delimiters=",">
				<cfif  IsValid("email",#emailAddress#)>
					<cfif listfindnocase(toList,#emailAddress#) eq 0>
						<cfset toList = toList & "," & #emailAddress#>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
	</cfloop>
	
	<cfif toList neq "" or sendleadEmail>   <!--- if there are emails to send, create the email --->
		<cfquery name="getLeadDets" datasource="#application.SiteDataSource#">
			SELECT     Lead.LeadID, Country.CountryDescription, Lead.Company, Lead.salutation, Lead.FirstName, Lead.LastName, Lead.Address1, Lead.Address2, Lead.Address3, Lead.Address4, Lead.Address5, 
	                      Lead.PostalCode, Lead.Address7, Lead.Address8, Lead.Address9, Lead.Town, Lead.Telephone, Lead.Fax, Lead.PrefPartner, Lead.Email, 
	                      Lead.TransactionNo, Lead.Source, Lead.custom1, Lead.custom2, Lead.custom3, Lead.custom4
			FROM       Lead INNER JOIN
	                      Country ON Lead.CountryID = Country.CountryID
			where leadID =  <cf_queryparam value="#maxLeadID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<!--- the queryname.columnlist returns an  alphabetical list. We want the order in which it was written--->
		<cfset columnList = "LeadID,CountryDescription,Company,salutation,FirstName,LastName,Address1,Address2,Address3,Address4,Address5,Address7,Address8,Address9,Town,PostalCode,Telephone,Fax,PrefPartner,Email,TransactionNo,Source,custom1,custom2,custom3,custom4">
		<cfset leadDetails = "">
		
		<!--- retreiving the data that was just inserted into the db to send in an email. 
			  We only append a column to the email body if it contains a value --->
		<cfloop list="#columnList#" index="column" delimiters=",">
			<cfif #column# neq "CountryDescription">
				<cfquery name="getLeadDetsColumn" datasource="#application.SiteDataSource#">
					select #column# from lead where leadID =  <cf_queryparam value="#maxLeadID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
			<cfelse>
				<cfquery name="getLeadDetsColumn" datasource="#application.SiteDataSource#">
					select #column# as country from country where countryID =  <cf_queryparam value="#form.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				<cfset column = "country">	<!--- so that we can use the phrase text id phr_country for translation --->
			</cfif>
			<cfset leadColumn = "getLeadDetsColumn." & #column#>
			<cfif listfindnocase("custom1,custom2,custom3,custom4",#column#)>
				<cfset columnLabel="phr_LeadCapture_"&#column#&#variables.LeadTypeID#>
			<cfelse>
				<cfset columnLabel="phr_ext_"&#column#>
			</cfif>
			<cf_translate phrases="#columnLabel#"/>
	 		<cfif (getLeadDetsColumn.RecordCount eq 1) and (#evaluate(leadColumn)# neq "")>
				<cfset leadDetails = #leadDetails# & "<tr><td valign='top'>" & #evaluate(columnLabel)# & "&nbsp;&nbsp;</td><td>" & #evaluate(leadColumn)#  & "</td></tr><tr><td>&nbsp;</td></tr>">
			</cfif>
		</cfloop>

		
		<!--- setting the email subject --->
		<cfif isDefined('leadCaptureEmailSubject')>
			<cf_translate phrases="#leadCaptureEmailSubject#"/>
			<cfset emailSubject = "#evaluate(leadCaptureEmailSubject)#">
		<cfelse>
			<cfset emailSubject = "phr_LeadNotificationFor_LeadType"&#variables.leadTypeID#>
			<cf_translate phrases="#emailSubject#"/>
			<cfset emailSubject = "#evaluate(emailSubject)#">
		</cfif>
		
		
		<!--- 2007-10-10 WAB  
				SSS had taken a full copy of the email below in order to deal with SendLeadEmail = 1
				I think we only need one copy of the email and just change the toAddress and CC Addresses
		--->
		<cfif toList eq "" and sendleadEmail>
			<!--- add current user's email to the toList (which is actually blank ) --->
			<cfset tolist = listappend(toList,"#request.relayCurrentUser.fullname# <#request.relayCurrentUser.person.Email#>")>
			<cfset ccList = "">
		<cfelse>
			<!--- otherwise add current user's email to the ccList  --->
			<cfset ccList = "#getEmail.ccEmailAddress#">
			<cfset ccList = listappend(ccList, "#request.relayCurrentUser.fullname# <#request.relayCurrentUser.person.Email#>")>			
		</cfif>

		 	<cfmail
				to="#toList#"
				cc="#ccList#"
				bcc="#application.bccemail#"
				from="#application.emailFromDisplayName# <#application.EmailFrom#@#application.mailFromDomain#>"
				subject="#emailSubject#"
				type="html">
				<BASE href="#request.currentSite.httpProtocol##siteDomain#">
				<style>
				<!--- NJH 2009/01/05 - Bug Fix All Sites Issue 1561 - use the stylesheet in request.currentSite. stylesheet
				<cfset userFilesStyleSheetPath = application. UserFilesAbsolutePath & "\code\borders\" & request.DisplayBorder.BorderSet & "_stylesheet_" & styleSheet & ".css">
				<cfif fileExists(userFilesStyleSheetPath)>
					<cfinclude template="/code/borders/#request.DisplayBorder.BorderSet#_styleSheet_#styleSheet#.css">
				<cfelse>
					<cfinclude template= "/code/styles/DefaultPartnerStyles.css">
				</cfif> --->
					<!--- <cfinclude template= "#request.currentSite. stylesheet#"> --->
				</style>
				
				<div id="borderContentArea" align="Left" class="borderContentAreaCustomised" >
				<div id="bordercontentDiv" class="bordercontentDivCustomised"  style="height:auto;overflow:hidden;">		
				<table width="100%" border=0 cellpadding=0 cellspacing=0 class="withBorder">
				<cf_translate>
				<cfif isDefined('LeadCaptureEmailIntro')>
					<tr><td colspan="2">
					#LeadCaptureEmailIntro#
					</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
				</cfif>
				#leadDetails#
	
				<cfif isDefined('leadCaptureScreenID')>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td colspan="2">
					<table width="100%" border=0 cellpadding=0 cellspacing=0>
					<cf_ascreen formName = "endUserLeadForm">
						<cf_ascreenitem screenid="#leadCaptureScreenID#"	
								method="view" 
								person="#getPersonDets#" 
								location="#getLocationDets#"
								organisation="#getOrgDets#"
						>
					</cf_ascreen>
					</table>
					</td></tr>
				</cfif>
				</cf_translate>
				</table>
				</div>
				</div>
			</cfmail>


	</cfif>
	

	<!---  SSS code removed by WAB
	
		<cfif tolist neq "">
		 	<cfmail
				to="#toList#"
				cc="#getEmail.ccEmailAddress#,#request.relayCurrentUser.person.Email#"
				bcc="#application.bccemail#"
				from="#application.emailFromDisplayName# <#application.EmailFrom#@#application.mailFromDomain#>"
				subject="#emailSubject#"
				type="html">
				<BASE href="#request.currentSite.httpProtocol##siteDomain#">
				<style>
				<cfset userFilesStyleSheetPath = application. UserFilesAbsolutePath & "\code\borders\" & request.DisplayBorder.BorderSet & "_stylesheet_" & styleSheet & ".css">
				<cfif fileExists(userFilesStyleSheetPath)>
					<cfinclude template="/code/borders/#request.DisplayBorder.BorderSet#_styleSheet_#styleSheet#.css">
				<cfelse>
					<cfinclude template= "/code/styles/DefaultPartnerStyles.css">
				</cfif>
				</style>
				
				<div id="borderContentArea" align="Left" class="borderContentAreaCustomised" >
				<div id="bordercontentDiv" class="bordercontentDivCustomised"  style="height:auto;overflow:hidden;">		
				<table width="100%" border=0 cellpadding=0 cellspacing=0 class="withBorder">
				<cf_translate>
				<cfif isDefined('LeadCaptureEmailIntro')>
					<tr><td colspan="2">
					#LeadCaptureEmailIntro#
					</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
				</cfif>
				#leadDetails#
	
				<cfif isDefined('leadCaptureScreenID')>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td colspan="2">
					<table width="100%" border=0 cellpadding=0 cellspacing=0>
					<cf_ascreen formName = "endUserLeadForm">
						<cf_ascreenitem screenid="#leadCaptureScreenID#"	
								method="view" 
								person="#getPersonDets#" 
								location="#getLocationDets#"
								organisation="#getOrgDets#"
						>
					</cf_ascreen>
					</table>
					</td></tr>
				</cfif>
				</cf_translate>
				</table>
				</div>
				</div>
			</cfmail>
		</cfif>
		<!---This Email will only be sent if sendLeadEmail is set to true--->
		<cfif sendleadEmail>
			<cfmail
				to="#request.relayCurrentUser.fullname# <#request.relayCurrentUser.person.Email#>"
				bcc="#bccEmailAddress#"
				from="#application.emailFromDisplayName# <#application.EmailFrom#@#application.mailFromDomain#>"
				subject="#emailSubject#"
				type="html">
				<BASE href="#request.currentSite.httpProtocol##siteDomain#">
				
				<style>
				<cfset userFilesStyleSheetPath = application. UserFilesAbsolutePath & "\code\borders\" & request.DisplayBorder.BorderSet & "_stylesheet_" & styleSheet & ".css">
				<cfif fileExists(userFilesStyleSheetPath)>
					<cfinclude template="/code/borders/#request.DisplayBorder.BorderSet#_styleSheet_#styleSheet#.css">
				<cfelse>
					<cfinclude template= "/code/styles/DefaultPartnerStyles.css">
				</cfif>
				</style>
				
				<div id="borderContentArea" align="Left" class="borderContentAreaCustomised" >
				<div id="bordercontentDiv" class="bordercontentDivCustomised"  style="height:auto;overflow:hidden;">		
				<table width="550" border=0 cellpadding=0 cellspacing=0 class="withBorder">
				<cf_translate>
				<cfif isDefined('LeadCaptureEmailIntro')>
					<tr><td colspan="2">
					#LeadCaptureEmailIntro#
					
					<br><br>
					phr_leadCapture_LeadCreatedBy <b>#request.relayCurrentUser.fullname#</b> phr_companyname <b>#request.RelayCurrentUser.ORGANISATION.ORGANISATIONNAME#</b>
					</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
				</cfif>
				#leadDetails#<br><br>
				
				<cfif isDefined('leadCaptureScreenID')>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td colspan="2">
					<table width="100%" border=0 cellpadding=0 cellspacing=0>
					<cf_ascreen formName = "endUserLeadForm">
						<cf_ascreenitem screenid="#leadCaptureScreenID#"	
								method="view" 
								person="#getPersonDets#" 
								location="#getLocationDets#"
								organisation="#getOrgDets#"
						>
					</cf_ascreen>
					</table>
					</td></tr>
				</cfif>
				</cf_translate>
				</table>
				</div>
				</div>
			</cfmail>
		</cfif>

	 --->
	
	<cfif isdefined("debug")>
		<cfdump var="#form#">
	</cfif>
<cfelse>

	<cfquery name="getPersonDets" datasource="#application.SiteDataSource#">
		select * from person where personid = #request.relayCurrentUser.PersonID#
	</cfquery>
	<cfquery name="getLocationDets" datasource="#application.SiteDataSource#">
		select * from location where locationid=#request.relayCurrentUser.locationID#
	</cfquery>
	<cfquery name="getOrgDets" datasource="#application.SiteDataSource#">
		select * from organisation where organisationid=#request.relayCurrentUser.organisationID#
	</cfquery>

	<cfoutput>




<!--- save the content for the xml to define the editor --->
<!--- <cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="232" name="thisEditor" entity="lead" title="Lead Capture">
<!--- 		<field name="leadTypeID" label="Request ID" description="This records unique ID." control="hidden"></field> --->
		<field name="company" label="phr_EndUserCompanyName" description=""></field>
		<field name="custom1" label="phr_QntyCMS20" description="" control="numeric"></field>
		<field name="custom2" label="phr_DistributorName" description=""></field>
		<!--- <field name="quarter" label="Quarter" control="SELECT" description="Account this request Relates to." validvalues = "1,2,3,4"></field>
		<field name="year" label="Year" control="SELECT" description="Account this request Relates to." validvalues = "#listOfYears#"></field> --->
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

<CF_RelayXMLEditor
	editorName = "thisEditor"
	xmlSourceVar = "#xmlSource#"
	add="yes"
	thisEmailAddress = "relayhelp@foundation-network.com"
	hideBackButton = true
> --->
	<cf_translate phrases="phr_ext_Company"/>
	
	<cfset request.relayFormDisplayStyle = "HTML-table">
	
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
	
	<cfform  method="POST" name="endUserLeadForm" > 
		<cf_relayFormElement relayFormElementType="hidden" fieldname="insertFieldList" currentValue="leadTypeID,company,custom1,custom2,custom3,custom4,countryID,createdBy" label="">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="LeadTypeID" currentValue="#LeadTypeID#" label="">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="createdBy" currentValue="#request.relayCurrentUser.personID#" label="">

		<!--- <cfif listFindNoCase(LeadTypes.fieldsToShow, "tradeUpFields")>
		<TR>
			<TD align="right">
			<B>phr_TU_EndUserDetails_ProjectName</B>
			</TD>
			<TD>
			#getTransactionDetails.TransactionName#
			</TD>
		</TR>
		</cfif> --->
		<cf_relayFormDisplay>
		<cfif isdefined('LeadCaptureIntroText')>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#LeadCaptureIntroText#" label="" spanCols="yes">
		</cfif>
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="<B>phr_EndUserDetails</B>" label="" spanCols="yes">
		
		<cfif isDefined('HideCompanyField') and #HideCompanyField# eq "yes">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="Company" currentValue="">
		<cfelse>
			<cf_relayFormElementDisplay relayFormElementType="text" fieldname="Company" label="#phr_ext_Company#" currentValue="" required="yes" size="25" maxlength="100">
		</cfif>
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="countryid" label="phr_ext_Country" currentValue="#request.relayCurrentUser.countryID#" query="#getCountries#" display="CountryDescription" value="countryID" required="yes">
		<!--- GCC - 2005/09/30 - format is fieldName-DisplayType-flagtextid 
		displayType is optional - default is text input
		flagtextid must be declared if displayType 'Select' is used to determine which valid values to offer as a choice
		--->
		<cfloop from="1" to="#listlen(LeadTypes.fieldsToShow,",")#" step="1" index="xCount">
			<cfset customDetail = listgetat(LeadTypes.fieldsToShow,xCount,",")>
			<cfif listlen(customDetail,"-") gt 1>
				<cfset fieldType=listgetat(customDetail,2,"-")>
			<cfelse>
				<cfset fieldType="default">
			</cfif>
			<cfset customName = "custom" & #xCount#>
				<cf_relayFormElementDisplay relayFormElementType="HTML" label="phr_LeadCapture_#customName##variables.LeadTypeID#" currentValue="" fieldname="" required="yes">
				<cfset customLabel="phr_LeadCapture_"&#customName#&#variables.LeadTypeID#>
				<cf_translate phrases = "#customLabel#"/>
				<!--- need to check against customxfieldtype --->
				<cfswitch expression="#fieldType#">
				
					<cfcase value="select">
						<cfset validFieldName = listlast(customDetail,"-")>
						<cf_displayValidValues 
						validFieldName = "#validFieldName#" <!--- passed to look for an unrelated list : the data is only going to the lead table anyway --->
						parentValidFieldName = "flag.#validFieldName#"<!--- passed to look for a flag valid value if an unrelated list isn't found --->
						formFieldName =  "#customName#"
						currentValue = ""
						displayas = "select"
						listSize = "1"
						formname = "endUserLeadForm"
						required="1"
						requiredLabel= "#evaluate(customLabel)#"
						useCFForm = "yes"
						>
						<cfinput type="hidden" name="#customName#_required" id="#customName#_required" value="phr_LeadCapture_#customName##variables.LeadTypeID# Phr_Sys_formValidation_Req">
					</cfcase>
					<!--- enable email address capture and let the receiver know which field(s) to use --->
					<cfcase value="selectemail">
						<cfset validFieldName = listlast(customDetail,"-")>
						<cf_displayValidValues 
						validFieldName = "#validFieldName#" 
						formFieldName =  "#customName#"
						currentValue = ""
						displayas = "select"
						listSize = "1"
						formname = "endUserLeadForm"
						required="1"
						requiredLabel= "#evaluate(customLabel)#"
						useCFForm = "yes"
						>
						<cfset emailName = "emailTo" & #xCount#>
						<cfinput type="hidden" name="#emailName#" value="#customName#">
						<cfinput type="hidden" name="#customName#_required" id="#customName#_required" value="phr_LeadCapture_#customName##variables.LeadTypeID# Phr_Sys_formValidation_Req">
					</cfcase>
					<cfcase value="textArea">
						<cf_relayFormElement relayFormElementType="textArea" fieldname="#customName#" label="" currentValue="" required="yes" cols="50" rows="10" maxChars="500" message="#evaluate(customLabel)# #request.translations.Phr_Sys_formValidation_Req#">
					</cfcase>
					<cfdefaultcase>
						<cf_relayFormElement relayFormElementType="text" fieldname="#customName#" label="" currentValue="" required="yes" size="25" maxlength="50" message="#evaluate(customLabel)# #request.translations.Phr_Sys_formValidation_Req#">
					</cfdefaultcase>				
				</cfswitch>
			</cf_relayFormElementDisplay>
		</cfloop>
		<cf_relayFormElement relayFormElementType="hidden" fieldName="customFieldCount" currentValue="#listlen(LeadTypes.fieldsToShow,",")#">
		
		<!--- <cfif listFindNoCase(LeadTypes.fieldsToShow, "personDetails")>
		<TR>
			<TD align="right">
				phr_TU_EndUserDetails_Salutation
			</TD>
			<TD>
				#Lead.Salutation#
			</TD>
		</TR>
		
		
		<TR>
			<TD align="right">
				phr_TU_EndUserDetails_FirstName
			</TD>
			<TD>
				#Lead.FirstName#
			</TD>
		</TR>
	
		<TR>
			<TD align="right">
				phr_TU_EndUserDetails_LastName
			</TD>
			<TD>
				#Lead.LastName#
			</TD>
		</TR>
	
		<TR>
			<TD align="right">
				phr_TU_EndUserDetails_Email
			</TD>
			<TD>
				#Lead.email#
			</TD>
		</TR>
		
		
		<TR>
			<TD align="right">
				phr_TU_EndUserDetails_Telephone
			</TD>
			<TD>
				#Lead.Telephone#
			</TD>
		</TR>
		</cfif> --->
		
		<cfif isdefined('leadCaptureScreenID')>
			<cf_relayFormElementDisplay relayFormElementType="SCREEN" currentValue="">
				<cf_ascreen formName = "endUserLeadForm">
					<cf_ascreenitem screenid="#leadCaptureScreenID#"	
							method="edit" 
							person="#getPersonDets#" 
							location="#getLocationDets#"
							organisation="#getOrgDets#"
					>
				</cf_ascreen>	
			</cf_relayFormElementDisplay>
		</cfif>
		<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="saveLead" currentValue="phr_save" label="" spanCols="yes" valueAlign="center">

	</cf_relayFormDisplay>
</cfform>
</cfoutput>

</cfif>
</cf_translate>

