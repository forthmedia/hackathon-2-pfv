<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			manageEventTeamMembers.cfm
Author:				SWJ
Date started:		2005-08-18

Description:		This is a relay tag designed to provide a method to allow people

Behaviour:  By default it will show all events that have a maxTeamSize > 0

parameters:
	eventTextID = this will filter to only allow the specified event to show

	eventGroup

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-04-26			AJC			Added currenteventid code so that the team drop down is related to that event
2006-05-02			AJC			Added Screen code and processing
2006-05-02			AJC			Added hidden variables to tidy process
2006-05-02			AJC			Whilst here, changed text to translations
2006-05-02			AJC			Added Team Player report
2016-01-29			WAB 		Removed call to Sel All Function

Still To Do
===========
Needs a parameter to specify which event text ID to show
Should the user have to be registered for that event or not?  Probably should also
need to contorl max team size
need to control whether a person can have more than one team for a given event
need to think about making a team name unique by flagID & personID
need an edit team name function
get an error when you add the same person to the team members

Possible enhancements:

 --->
<cfparam name="variables.currentTeamID" default="0">
<cfparam name="variables.currentEventID" default="0">
<cfparam name="sortOrder" default="lastname">
<cfif isDefined("form.saveTeamMembers") and isDefined("form.EventTeamID")>
	<!--- clear out all team members --->
	<cfquery name="deleteTeamMember" datasource="#application.SiteDataSource#">
		delete from [EventTeamPerson] where [EventTeamID] =  <cf_queryparam value="#form.EventTeamID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfif isDefined("form.ExistingTeamMembers")>
		<cfloop index="i" list="#form.ExistingTeamMembers#">
			<!--- add teamMembers for all of the ids in #form.ExistingTeamMembers# --->
			<cfquery name="getMyTeams" datasource="#application.SiteDataSource#">
				INSERT INTO [EventTeamPerson]([PersonID], [EventTeamID])
					VALUES(<cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >,<cfqueryParam value="#form.EventTeamID#" cfsqltype="CF_SQL_INTEGER">)
			</cfquery>
		</cfloop>
	</cfif>
	<!--- 2006-05-02 AJC CHW001 - Added updatedate code for team screen flags --->
	<cfinclude template="/screen/updatedata.cfm">

</cfif>

<!--- add new team if an event has been selected --->
<cfif isDefined("form.addTeam")>
	<cfif form.eventFlagID gt 0>
		<cfquery name="addTeams" datasource="#application.SiteDataSource#">
			INSERT INTO [EventTeam]([FlagID], [EventTeamName], [personID])
			VALUES( <cf_queryparam value="#form.eventFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cfqueryparam value="#form.eventTeamName#" cfsqltype="CF_SQL_VARCHAR" maxlength="25">
			, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >)
		</cfquery>
	<cfelse>
		phr_Please_select_an_Event_First
	</cfif>

	<cfquery name="getNewTeam" datasource="#application.SiteDataSource#">
		select eventTeamID from eventTeam
		where flagID =  <cf_queryparam value="#form.eventFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and eventTeamName =  <cf_queryparam value="#form.eventTeamName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and personID = #request.relayCurrentUser.personID#
	</cfquery>

	<cfset form.EventTeamID = getNewTeam.eventTeamID>
</cfif>
<cfoutput>
<cfscript>
	getEventsWithTeams = "";
	getEventsWithTeams=application.com.relayEvents.getEventList(
		filterByCountryIDList=request.relaycurrentuser.countrylist,
		filterByUserGroupids=request.relaycurrentuser.usergroups,
		limitToTeamEvents = true
		);
</cfscript>
</cfoutput>
<!--- <cfquery name="dddddd" datasource="#application.SiteDataSource#">
	select e.flagID, e.location from eventDetail as e
	inner join flag f on f.flagid = e.flagid
	inner join eventCountry ec ON f.flagID = ec.flagID
	where maxTeamSize > 0
	and ec.countryID=#request.relaycurrentuser.countryid#

	and e.flagid in (select ed.flagID
		from eventDetail ed
		INNER JOIN eventflagData efd ON ed.flagID = efd.flagID
		where efd.entityID = #request.relaycurrentuser.personid#)

	order by location
</cfquery>
 --->
<!--- if we've got at least 1 team then we can start --->
<cfif getEventsWithTeams.recordCount gt 0>
	<!--- 2006-04-26 AJC P_CHW001 EventFlagID overrides variables.currentEventID --->
	<cfif isDefined("form.eventFlagID")>
		<cfset variables.currentEventID = form.eventFlagID>
		<!---<cfset form.EventTeamID = variables.currentEventID>--->
	</cfif>

	<!--- 2006-04-26 AJC P_CHW001 Added Flagid filter --->
	<cfquery name="getMyTeams" datasource="#application.SiteDataSource#">
		SELECT [EventTeamID], [FlagID], [EventTeamName] FROM [EventTeam]
		where personid = #request.relayCurrentUser.personid# and FlagID =  <cf_queryparam value="#variables.currentEventID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<!--- if form.EventTeamID has been set then override the variables.currentTeamID
		else set it to the firstRecord in the query --->
	<cfif isDefined("form.EventTeamID")>
		<cfset variables.currentTeamID = form.EventTeamID>
	<!--- <cfelseif getMyTeams.recordCount gt 0>
		<cfset variables.currentTeamID = getMyTeams.EventTeamID[1]> --->
	</cfif>

	<div id="manageEventTeamMembersTeamForm">

		<cfform  method="POST" name="chooseTeamForm" id="chooseTeamForm">
		<table>
			<tr>
				<td>phr_Choose_Event:</td><td>
			<!--- 2006-04-26 AJC P_CHW001 Added onchange and selected attributes --->
			<cfselect name="eventFlagID" size="1" queryposition="below" query="getEventsWithTeams" value="flagID" display="title" selected="#variables.currentEventID#" visible="Yes" enabled="Yes" onchange="document.chooseTeamForm.submit()">
				<option value="0" <cfif variables.currentEventID is "0">selected</cfif>>phr_Please_Select</option>
			</cfselect>
				</td>
			</tr>
			<cfif getMyTeams.recordCount gt 0>
			<tr>
				<td>phr_Choose_Team:</td><td>
				<cfselect name="eventTeamID" size="1" queryposition="below" query="getMyTeams" value="EventTeamID" display="EventTeamName" selected="#variables.currentTeamID#" visible="Yes" enabled="Yes" onchange="document.chooseTeamForm.submit()">
				<!--- 2006-04-26 AJC P_CHW001 Added default option --->
					<option value="">phr_Please_Select</option>
				</cfselect>
				</td>
			</tr>
			</cfif>
			<tr>
				<td>phr_Create_new_team:</td><td><cfinput type="Text" name="eventTeamName" required="Yes">
			<input type="submit" name="addTeam" value="phr_Add_Team">
				</td>
			</tr>
		</table>
		</cfform>
	</div>
	<!--- if the user has selected their team then we are good to go --->
	<cfif getMyTeams.recordCount gt 0 and variables.currentTeamID is not 0>
		<CFPARAM NAME="caption1" DEFAULT="Potential Team Members">
		<CFPARAM NAME="caption2" DEFAULT="Current Team Members">
		<CFQUERY NAME="GetPotentialTeamMembers" DATASOURCE="#Application.SiteDataSource#">
			select lastname + ', '+ firstname as column2, personID as column1
			from person
			where organisationID = #request.relayCurrentUser.organisationID#
				and personid not in (select p.personID
						from person p
						inner join EventTeamPerson et on p.personID = et.personid
						where organisationID = #request.relayCurrentUser.organisationID#
						and eventTeamID =  <cf_queryparam value="#variables.currentTeamID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			ORDER BY lastname
		</CFQUERY>

		<CFQUERY NAME="GetExistingTeamMembers" DATASOURCE="#Application.SiteDataSource#">
			select lastname + ', '+ firstname as column2, p.personID as column1, email
			from person p
			inner join EventTeamPerson et on p.personID = et.personid
			where <!--- organisationID = #request.relayCurrentUser.organisationID#
			and  --->eventTeamID =  <cf_queryparam value="#variables.currentTeamID#" CFSQLTYPE="cf_sql_integer" >
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
		</CFQUERY>



		<!--- get all the groups of eventTeam flags
		<CFQUERY NAME="personFlags" DATASOURCE="#application.SiteDataSource#">
		select * from flagGroup where entityTypeID = #application.entityTypeID["person"]# and parentflaggroupid = 0
		</CFQUERY>

		<cfset flagjoins = application.com.flag.getJoinInfoForListOfFlagGroups(valueList(eventTeamFlags.flagGroupID))>
		<CFQUERY NAME="getEvents" DATASOURCE="#application.SiteDataSource#">
			select
					<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
						#flagjoins[i].selectField# as  #flagjoins[i].alias# ,
					</cfloop>
					e.*
			from vEventTeamNumbers e
						<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
							#flagjoins[i].join#
						</cfloop>
			where 1=1
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
		</CFQUERY  >

		<!--- get names of the flags/groups for doing column names --->
		<cfset flagcolumnNameList = "">
		<cfset flagcolumnAliasList = "">
		<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
			<cfset flagcolumnNameList = listappend(flagcolumnNameList,flagjoins[i].name)>
			<cfset flagcolumnAliasList = listappend(flagcolumnAliasList,flagjoins[i].alias)>
		</cfloop>
		--->



		<cfform  method="POST" name="teamAddForm" id="teamAddForm"
			onSubmit="document.teamAddForm.eventTeamID.value = document.chooseTeamForm.eventTeamID.value">
			<CF_TwoSelectsComboQuery
			    NAME="PotentialTeamMembers"
			    NAME2="ExistingTeamMembers"
			    SIZE="6"
			    WIDTH="150px"
			    FORCEWIDTH="50"
				QUERY2="GetPotentialTeamMembers"
				QUERY1="GetExistingTeamMembers"
			   	CAPTION="<B>#htmleditformat(caption1)#</B>"
			    CAPTION2="<B>#htmleditformat(caption2)#</B>"
				UP="No"
				DOWN="No"
				FORMNAME="teamAddForm">


			<cfif isDefined("screenid")>
				<table border=0>
				<CF_aSCREEN formName="teamAddForm">
 						<!---  these lines are just for debuggin - use them to create your own screen to try out
								<cfset screendefinition = application.com.screens.createScreenDefinition (fieldsource="flaggroup",fieldtextid=568,allcolumns=1)>
								<cf_ascreenitem screenid = "dummy" screenquery = screendefinition eventTeamID=#variables.currentTeamID#  countryid = 0 METHOD="edit">
							 --->
					<CF_aSCREENITEM SCREENID="#screenid#" eventTeamID=#variables.currentTeamID#  countryid = 0 METHOD="edit">
				</CF_aSCREEN>
				</table>
			</cfif>

			<cfoutput>
				<CF_INPUT type="hidden" name="eventTeamID" value="#variables.currentTeamID#">
				<!--- 2006-05-02 AJC CHW001 - Added hidden eventflagid (The current Event) --->
				<CF_INPUT type="hidden" name="eventFlagID" value="#variables.currentEventID#">
			</cfoutput>
			<input type="submit" name="SaveTeamMembers" value="phr_Save">

		</cfform>

		<!--- 2006-05-02 AJC Added team player report --->
		<cf_translate>
			<cfif IsQuery(GetExistingTeamMembers)>
			Phr_TeamSelectedReport
			<CF_tableFromQueryObject queryObject="#GetExistingTeamMembers#"
				HidePageControls="yes"
				useInclude = "false"
				keyColumnList="email"
				keyColumnURLList="mailto:"
				keyColumnKeyList="email"
				keyColumnTargetList="_blank"
				showTheseColumns="column2,email"
				ColumnHeadingList="Name,Email"
				sortOrder="column2"
				tableAlign="left"
				allowColumnSorting="no">
			</cfif>
		</cf_translate>

	</cfif>

<cfelse>
	<p>phr_no_events</p>
</cfif>
