<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementLinkList.cfm
Author:			SWJ based on DAM's code
Date created:	2002-01-21

	Objective - This displays a list of children of the calling element as a URL 				list
		
	Syntax	  -	Simply place <RELAY_ELEMENTLINKLIST> in the calling element.

Amendment History:

Date 		Initials 	What was changed
2002-01-21	SWJ			Called qryGetChildren as an include
2005-02-11	SWJ			Redesigned to use .css
2005-10-24	WAB 		Changed to use convertTreeToMenuBranch
Enhancement still to do:

--->

<cfparam name="elementLinkListShowSummary" default="no">
<cfparam name="linkListDivID" default="ElementLinkListContent">
<cfparam name="elementLinkListShowImage" default="no">
<cfparam name="numberofgenerationsofchildren" type="numeric" default="1">

<cfparam name="variables.foundimage" default="false">

<cfset GetChildren = application.com.relayElementTree.convertTreeToMenuBranch(
						elementTree = request.currentElementTree, 
						topOfBranchID = RelayTag.ElementID,
						currentelementid = RelayTag.ElementID,
						showCurrentElementChildren = true , 
						numberofgenerations = 0,
						numberofgenerationsofchildren =numberofgenerationsofchildren)>
						
<cf_translate nulltext="">
<cfoutput>
<div id="#linkListDivID#">
</cfoutput>
	
	<cfoutput query="GetChildren">
		<!--- if the element has been set to isExternalFile then treat the link as an external URL --->
		<cfif isexternalfile eq 1>
			<!---2005/04/08 - GCC horrible hack for Sony --->
			<cfif isdefined('session.BVsessionToken') and FindNoCase("ChannelId",url) gt 0>
				<cfset link = "#url##session.BVsessionToken#">
			<cfelse>
				<cfset link = "#url#">
			</cfif>
		<cfelse>
			<cfset link = "/?eid=" & #id#>
		</cfif>
		
		<!--- this is displayed as a unordered list so that you can easily style it using a stylesheet --->
		
		
		
		<!--- <ul>
			<li>
				<div style="display:table;">
					<cfif elementLinkListShowImage eq "yes">
						<div style="float:left; width:400px;>
							phr_headline_element_#GetChildren.ID#_image
						</div>
					</cfif>
					<div style="float:right; width:330px;">
						<a href="#link#">phr_headline_element_#GetChildren.ID#</a>
						<cfif elementLinkListShowSummary eq "yes">
							phr_summary_element_#GetChildren.ID#
						</cfif>
					</div>	
				</div>
			</li>
		</ul> --->
		<cfif generation is 2 and numberofgenerationsofchildren gt 1>
			<div id="#linkListDivID#Header"><ul><li>phr_headline_element_#htmleditformat(GetChildren.ID)# </li></ul></div>
		<cfelse>
			<ul><li>
					<cfif elementLinkListShowImage eq "yes">
						<div style="float:left; width:90px; font-size:11px;">
							<cfset linkImagePath=application.userfilesabsolutepath & "content\elementLinkImages\linkImage-" & GetChildren.ID & "-thumb.jpg">
							<cfif fileexists(linkImagePath)>
								<img src="/content/elementLinkImages/linkImage-#GetChildren.ID#-thumb.jpg" border=0 width="80">
								<cfset variables.foundimage=true>
							</cfif>		
							<cfif variables.foundimage is false>
								<img src="/Content/ElementLinkImages/blank.gif"><!--- linkImage-#GetChildren.ID#.#ListGetAt(variables.availablefiletypes, variables.loopcounter)# --->
							</cfif>
						</div>
						<div style="float:right; width:402px;">
					<cfelse>
						<div style="width:510px;">
					</cfif>

						<a href="#link#">phr_headline_element_#htmleditformat(GetChildren.ID)#</a>
						<cfif elementLinkListShowSummary eq "yes">
							<br/>phr_summary_element_#htmleditformat(GetChildren.ID)#
						</cfif>
					</div>
			</li></ul>
		</cfif>
		
	</cfoutput>
</div>

</cf_translate>
