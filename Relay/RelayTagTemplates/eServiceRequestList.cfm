<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 
<cf_translate>

<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="232" name="thisEditor" entity="actions" title="Service Request">
		<field name="actionID" label="ID" description="This requests unique ID" control="hidden"></field>
<!--- 		<field name="organisationID" label="Account" control="SELECT" description="Account this request Relates to." query = "SELECT organisationID AS Value, Account AS Display FROM vFundAccounts order by account"></field>
 --->		<cfif isDefined("actionID") and actionID gt 0>
<!--- 			<field name="PersonID" label="Contact" control="SELECT" description="Account this request Relates to." query = "SELECT personID AS Value, firstname +' '+lastname AS Display FROM person where organisationID = 1 order by firstname"></field> --->
		</cfif>
		<field name="description" label="Desciption" description="Account this request Relates to"></field>
		<field name="requestedBy" label="Requestor" description="Who is requesting this change"></field>
		<field name="deadline" label="Required by" description="When this is required by"></field>
		<!--- <field name="priority" label="Priority" control="SELECT" description="Priority of this Request" validvalues = "1,2,3,4"></field> --->
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

<cfif isDefined("editor") and editor eq "yes" and isDefined("add") and add eq "yes">
	<CF_RelayXMLEditor
		editorName = "thisEditor"
		xmlSourceVar = "#xmlSource#"
		add="yes"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>
<cfelseif isDefined("editor") and editor eq "yes">
	<CF_RelayXMLEditor
		xmlSourceVar = "#xmlSource#"
		editorName = "thisEditor"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>

<cfelse>

<cfparam name="sortOrder" default="Deadline asc, priority DESC">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="filterBy" default="">
<cfparam name="dateFormat" default="deadline">
<cfparam name="FORM.hideComplete" default="Yes">

	<cfquery name="vServiceRequestList" datasource="#application.SiteDataSource#">
		select *
		from vServiceRequestList
		     WHERE 1=1  
		AND ((entityTypeID = 0 and entityID = #request.relayCurrentUser.personid#))
		OR
		(entityTypeID = 2 and entityID = 
		(select organisationID FROM person where personid = #request.relayCurrentUser.personid#))
		and hidecomplete =  <cf_queryparam value="#FORM.hideComplete#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">	
	</cfquery>

		<cfset qs = request.query_string & "&editor=yes&actionID=">
	
	<CF_tableFromQueryObject 
		queryObject="#vServiceRequestList#"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		
		keyColumnList="description"
		keyColumnURLList="#cgi.SCRIPT_NAME#?#QS#"
		keyColumnKeyList="ID"
		
		useInclude = "true"
		
		FilterSelectFieldList="Status,Owner,Requestor,Type,Priority"
		FilterSelectFieldList2="Status,Owner,Requestor,Type,Priority"

		radioFilterLabel="Hide complete?"
		radioFilterDefault="No"
		radioFilterName="hideComplete"
		radioFilterValues="Yes-No"

		
		showTheseColumns="ID,Description,Deadline,Owner,Status,Type,Priority"
		
		dateFormat="#dateFormat#"
		allowColumnSorting="yes"
	>

</cfif>	
</cf_translate>