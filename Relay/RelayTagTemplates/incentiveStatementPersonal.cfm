<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			incentiveStatement.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/03/12    WAB   LID 1963

Possible enhancements:


 --->

 <!---first check if the person is registered with the program---->
<cfset personRegistration = application.com.relayIncentive.isPersonRegistered(request.relayCurrentUser.personID)>

<!--- WAB 2009/03/12
LID 1963
Template falling over because variable paramed in incentive\incentiveParams.cfm was not being 
Decided to include the application .cfm which includes this file and the incentiveINI if required

So removed this code block
<cfif fileexists("#application.paths.code#\cftemplates\incentiveINI.cfm")>
	<!--- incentiveINI can be used to over-ride default 
		global application variables and params defined in incentiveParams.cfm --->
	<cfinclude template="/code/cftemplates/incentiveINI.cfm">
</cfif>

 --->

<!--- <cfinclude template="\incentive\application .cfm"> --->


<!--- 2007/12/10 - GCC - Trying view access to statement for non points administrator --->
<cfparam name="permitNonRegisteredUsersToViewStatement" default="false">
<!--- <cfif not personRegistration.recordCount>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfexit method="exittemplate">
</cfif> --->
<cfif not personRegistration.recordCount and permitNonRegisteredUsersToViewStatement>
	<cfif isdefined("incentivePointsAdministratorFlagTextID")>
		<cfset pcData = application.com.flag.getFlagData(flagid=incentivePointsAdministratorFlagTextID,entityid = request.relaycurrentuser.organisationid)>
		<cfif pcData.recordcount eq 1>
			<cfset variables.personID=pcData.data>
		<cfelse>
			phr_Incentive_shared_youNeedToBeRegistered
			<cfexit method="exittemplate">
		</cfif>
	<cfelse>
		You must define incentivePointsAdministratorFlagTextID when using permitNonRegisteredUsersToViewStatement = "true".
		<cfexit method="exittemplate">
	</cfif>
<cfelseif not personRegistration.recordCount>
	phr_Incentive_shared_youNeedToBeRegistered
	<cfexit method="exittemplate">
</cfif>

<cfset hiderightBorder = "yes">
<cfset url.statementType = "personal">

<div align="center">
<!--- WAB 2010/11/08 removed versioning, now all done in the included files --->
<cfif isDefined("url.includeTemplate") and not compareNoCase(URL.includeTemplate, "sd")>
	<cfinclude template="/incentive/statementDetail.cfm">
<cfelse>
	<cfinclude template="/incentive/statementInclude.cfm">
</cfif>
</div>