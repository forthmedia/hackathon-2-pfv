<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		mailReadAdHoc.cfm
Author:			SWJ
Date created:	2002-03-27

	Objective - This manages ad hoc mail read processing.  When an email is sent out
				via QuickMailClient.cfm a img link is embedded in the outbound mail which 
				calls this template.  When it is called it adds a commDetailHistory 
				record to the database.
		
	Syntax	  -	it passes cdid and pid which are used to set the commDetailHistory
	
	Parameters - cdid 
				
	Return Codes - Please describe the return codes are returned to let you know certain 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->


<!--- web bug 

called with: /calltemplate.cfm?template=relayTagTemplates/mailreadadHoc.cfm&cid=#addedCommDetailID#&pid=#frmPersonID#

--->
<CFIF isDefined('cdid') and isDefined('pid') and isNumeric(pid) and isNumeric(cdid) and pid neq 0 and cdid neq 0>
	
	<!--- ***************************************************************
	 UPDATE COMMDETAIL RECORD: if cdid and PID are set we can update commDetail --->
	<CFQUERY NAME="getCommStatusID" datasource="#application.sitedatasource#">
		select commStatusID from commDetailstatus 
		where name = 'Email Read'
	</CFQUERY>	

	<!--- if there is no commDetailstatus record defined for 'Email Read'  we should add one --->
	<CFIF getCommStatusID.recordCount neq 1>
		<CFQUERY NAME="insertStatus" datasource="#application.sitedatasource#">
			INSERT INTO [rem].[dbo].[CommDetailStatus](CommStatusID,[Name], [Description], [StatsReport], [StatsReportOrder], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated])
			values (42,'Email Read', 'Web Bug Email Receipt', 'Emails sent (receipt confirmed)', 
			20, 100, getDate(), 100, getDate())		
		</CFQUERY>	
		<CFQUERY NAME="getCommStatusID" datasource="#application.sitedatasource#">
			select commStatusID from commDetailstatus 
			where name = 'Email Read'
		</CFQUERY>	
	</CFIF>
	
	<!--- check to see if their is a matching commDetail record --->
	<CFQUERY NAME="checkIDPair" datasource="#application.sitedatasource#">
		select Commid from commDetail where commDetailID =  <cf_queryparam value="#cdid#" CFSQLTYPE="CF_SQL_INTEGER" >  and PersonID =  <cf_queryparam value="#pID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<!--- if matching commDetail record found update it --->
	<CFIF checkIDPair.recordCount eq 1>
		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#">
			Update commdetail 
			  set feedback = N'Email Read',
			  commStatusID =  <cf_queryparam value="#getCommStatusID.commStatusID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			  lastUpdated = getDate(),
			  lastUpdatedBy =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			  where commDetailID =  <cf_queryparam value="#cdid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and PersonID =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</CFIF>	
</CFIF>
