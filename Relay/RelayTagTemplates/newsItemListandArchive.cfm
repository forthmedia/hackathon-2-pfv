<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementLinkList.cfm
Author:			SWJ based on DAM's code
Date created:	2002-01-21

	Objective - This displays a list of children of the calling element as a URL
				list
		
	Syntax	  -	Simply place <RELAY_ELEMENTLINKLIST> in the calling 
				element.
	
	Parameters - Currently there are no parameters
				
	Return Codes - none

Amendment History:

Date 		Initials 	What was changed
2002-01-21	SWJ			Called qryGetChildren as an include
WAB 2010/10/13  removed cf_get phrase  and replaced with CF_translate
Enhancement still to do:



--->

<CFINCLUDE TEMPLATE="qryGetChildren.cfm">
<Cf_translate>
<Table CELLPADDING="0" CELLSPACING="0">
	<TR>
	<TD HEIGHT="16">&nbsp;</TD>
	</TR>
<CFOUTPUT QUERY="GetChildren">

	
	<CFIF #isExternalFile# EQ 1>
		<CFSET link = "#url#">
	<CFELSE>
		<CFSET link = "/ElementTemplate.cfm?ElementID=" & #id#>
	</CFIF>
	<TR>
	<TD><A HREF = "#link#">phr_headline_element_#htmleditformat(GetChildren.ID)#</A></TD>
	</TR>
	<TR>
	<TD>phr_summary_element_#htmleditformat(GetChildren.ID)#</TD>
	</TR>
		<TR>
	<TD HEIGHT="16">&nbsp;</TD>
	</TR>
</CFOUTPUT>
</TABLE>
</Cf_translate>

