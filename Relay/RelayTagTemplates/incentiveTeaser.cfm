<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 2005/11/03 - GCC - CR_SNY554 Rewards Banner teaser 
	2006/06/13  WAB changed link to the rewards site to go straight to the register page with usecurrentuser set

--->
<cfoutput>
	<cfif request.relayCurrentUser.isLoggedIn>
	<cfscript>
		mn = application.com.login.getMagicNumber(request.relayCurrentUser.personid);
		encryptedMagicNumber = application.com.encryption.encryptMagicNumber(mn);
		if (cgi.SERVER_NAME eq "dev-sony1" )
			sonyRewardsURL = "192.168.0.44/sony";
		else if (cgi.HTTP_HOST eq "dev.sony1.net")
			sonyRewardsURL = "partnerdev.foundation-network.com/sony";
		else
			sonyRewardsURL = "www.SonyRewards.net";
		personRegistrationDetails = application.com.relayIncentive.isPersonRegistered(request.RelayCurrentUser.personid);
	</cfscript>							
		<cfif personRegistrationDetails.recordcount eq 1>
			<cfset linkURL = request.currentSite.httpProtocol & sonyRewardsURL & "/index.cfm?tepid=" & encryptedMagicNumber>
		<cfelse>
			<cfset seidNameValuePair = application.COM.relayElementTree.createSEIDQueryString (linktype="tracked",elementid = 950,personid = request.relayCurrentUser.personid)>
			<cfset linkURL = request.currentSite.httpProtocol & sonyRewardsURL & "/index.cfm?" & urlencodedformat("usecurrentuser=true&" & seidNameValuePair)>
		</cfif>
	<cfelse>
		<cfset linkURL = "http://www.SonyRewards.net/">
	</cfif>
	
	<!--- Sony Exception for French/ German Users --->
	<cfset countryExclusion = "false">
	<cfif listfind("4,8",request.relaycurrentuser.countryID)>
		<cfquery name="hasCompanyAccount" datasource="#application.sitedatasource#">
			SELECT     RWPersonAccount.personID
			FROM         RWPersonAccount INNER JOIN
                      RWCompanyAccount ON RWPersonAccount.AccountID = RWCompanyAccount.AccountID
			WHERE     (RWCompanyAccount.OrganisationID =
                          (SELECT     organisationid
                            FROM          person
                            WHERE      personid = #request.relaycurrentuser.personid#)) AND (RWPersonAccount.AccountDeleted = 0) AND (RWCompanyAccount.AccountDeleted = 0)
		</cfquery>
		<cfif hasCompanyAccount.recordcount neq 0>
			<cfset account = valuelist(hasCompanyAccount.personid)>
			<cfif listfind(account,request.relaycurrentuser.personid) eq 0>
				<cfset countryExclusion = "true">
				<cfset showTeaser = "false">
			</cfif>
		</cfif>
		
	</cfif>	
	
	<!--- Set domain is uesd for full image paths within the swf.
	Application name is used to instantiate application variables inside the flashRemotingFunctions CFC.
	Would be lovely to be pulling this from a site/domain structure... --->
	<cfif application.testSite eq 2>
		<!--- dev --->
		<cfif CGI.HTTP_HOST eq "dev.sony1.net">
			<cfset thisSiteDomain = "partnerdev.foundation-network.com">
		<cfelse>
			<cfset thisSiteDomain = "192.168.0.44">
		</cfif>
		<cfset applicationName = "Sony1">
	<cfelse>
		<!--- test/live --->
		<cfset thisSiteDomain = "ws1.sony1.net"> <!--- WAB 2011/02/02 LID 5328 changed from www.sonyrewards.net because it could not deal with https --->
		<cfset applicationName = "sony">
	</cfif>
	<!--- productgroups to use - set up as request variable perhaps? --->
	<cfset teaserProductGroupIDs = "44,45,51,47,50">
	
	<cfscript>
		isIncentiveUser = application.com.relayIncentive.isIncentiveUser(personID = request.relaycurrentuser.personid);
	</cfscript>
	
	<!--- is registered and no country restriction --->
	<cfif isIncentiveUser eq "true" and countryExclusion eq "false">
								
		<!--- get balance: pre-calculated for performance - used in first frame--->
		<!--- work out if the phrase 'with your points' or 'with more points' is needed --->
		<cfscript>
			balanceQry = application.com.relayIncentive.PointsBalance(datepassed = now(), organisationid = request.relaycurrentuser.organisationid, personid = request.relaycurrentuser.personid);
			canPurchaseProducts = application.com.relayIncentive.canPurchaseTeaserProducts(balance = balanceQry.balance, currentcountry = request.relaycurrentuser.countryid, teaserProductGroupIDs = teaserProductGroupIDs);
		</cfscript>
		
		<!--- translate phrases for use within the flash movie and set them --->
		<cf_translate phrases ="incentive_YouHave,incentive_Points,incentive_WithYourPoints,incentive_WithMorePoints,incentive_ClicktoSpend,incentive_ClicktoBrowse"></cf_translate>
		<cfset WelcomeText = phr_incentive_YouHave & " " & balanceQry.balance & " " & phr_incentive_Points>
		
		<cfif canPurchaseProducts eq "true">
			<cfset YouCouldBuy = phr_incentive_WithYourPoints>
			<cfset ClickToSpend = phr_incentive_ClicktoSpend>
		<cfelse>
			<cfset YouCouldBuy = phr_incentive_WithMorePoints>
			<cfset ClickToSpend = phr_incentive_ClicktoBrowse>
		</cfif>
		<cfset showTeaser = "true">
	
	<!--- non rewards user that isn't at an already a registered company --->
	<cfelseif isIncentiveUser eq "false" and countryExclusion eq "false">
		<cf_translate phrases ="incentive_RegisterIntro,incentive_claimThesePrizes,incentive_ClicktoRegister"></cf_translate>
		<cfset WelcomeText = phr_incentive_RegisterIntro>
		<cfset YouCouldBuy = phr_incentive_claimThesePrizes>
		<cfset ClickToSpend = phr_incentive_ClicktoRegister>
		<cfset balanceQry.balance = 105000>
		<cfset showTeaser = "true">
	<cfelseif countryExclusion eq "true">
		<cfset showTeaser = "false">
	<!--- non rewards users see old link for now--->
	<cfelse>
		<cfset showTeaser = "false">
		<p><cfinclude template="/content/sony1/sonyRewardsLink.cfm"></p>
	</cfif>
	
	
	<cfif showTeaser>
	<table cellspacing="0" cellpadding="0" border="0">
	<tr><td>
	<!--- debugging dump --->
	<!--- <cfif request.relaycurrentuser.personid eq 81711>
	 	<cfoutput>
			<cfinvoke component="relay.WebServices.flashRemotingFunctions"
				method="getIncentiveTeaserDetails" returnvariable="getIncentiveTeaserImages">
				<cfinvokeargument name="applicationName" value="#applicationName#">
				<cfinvokeargument name="siteDomain" value="#thisSiteDomain#">
			  	<cfinvokeargument name="balance" value="#balanceQry.balance#">	 
				<cfinvokeargument name="currentCountry" value="#request.relaycurrentuser.countryid#">
				<cfinvokeargument name="teaserProductGroupIDs" value="#teaserProductGroupIDs#"> 
				<cfinvokeargument name="callType" value="inline">
			</cfinvoke>
		</cfoutput> 
		<cfdump var="#getIncentiveTeaserImages#"> 
		<!--- <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0" width="205" height="115" id="RewardsBanner205" align="middle">
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="movie" value="#request.currentSite.httpProtocol##thisSiteDomain#/Flash/incentiveTeaserV2.swf" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="ffffff" />
			<param name="flashvars" VALUE="<cfif isdefined('playBeforeReturn')>playBeforeReturn=#playBeforeReturn#&</cfif>WelcomeText=#urlencodedformat(WelcomeText)#&YouCouldBuy=#urlencodedformat(YouCouldBuy)#&ClickToSpend=#urlencodedformat(ClickToSpend)#&logoUrl=#request.currentSite.httpProtocol##thisSiteDomain#/code/borders/rewardsBorderImages/rewards_logo.jpg&bgcolor=ffffff&gatewayURL=#request.currentSite.httpProtocol##thisSiteDomain#/flashservices/gateway/&teaserProductGroupIDs=#teaserProductGroupIDs#&siteDomain=#thisSiteDomain#&applicationName=#applicationname#&balance=#balanceQry.balance#&currentCountry=#request.relaycurrentuser.countryID#&linkURL=#linkURL#"> 
			<embed src="#request.currentSite.httpProtocol##thisSiteDomain#/flash/incentiveTeaserV2.swf" 
				quality="high" 
				bgcolor="ffffff" 
				FLASHVARS="<cfif isdefined('playBeforeReturn')>playBeforeReturn=#playBeforeReturn#&</cfif>WelcomeText=#urlencodedformat(WelcomeText)#&YouCouldBuy=#urlencodedformat(YouCouldBuy)#&ClickToSpend=#urlencodedformat(ClickToSpend)#&logoUrl=#request.currentSite.httpProtocol##thisSiteDomain#/code/borders/rewardsBorderImages/rewards_logo.jpg&bgcolor=ffffff&gatewayURL=#request.currentSite.httpProtocol##thisSiteDomain#/flashservices/gateway/&teaserProductGroupIDs=#teaserProductGroupIDs#&siteDomain=#thisSiteDomain#&applicationName=#applicationName#&balance=#balanceQry.balance#&currentCountry=#request.relaycurrentuser.countryID#&linkURL=#linkURL#" 
				width="205" height="115" 
				name="RewardsBanner205" 
				align="middle" 
				allowScriptAccess="sameDomain" 
				type="application/x-shockwave-flash" 
				pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object>  --->
	</cfif> --->
	
	<cfparam name="flashVarsList" default="">
	
	<cfif isdefined('playBeforeReturn')>
		<cfset flashVarsList ="playBeforeReturn=" & playBeforeReturn & "&">
	</cfif>
		<cfset flashVarsList = flashVarsList & "WelcomeText=" & urlencodedformat(WelcomeText) & "&YouCouldBuy=" & urlencodedformat(YouCouldBuy) & "&ClickToSpend=" & urlencodedformat(ClickToSpend) & "&logoUrl=" & request.currentSite.httpProtocol & thisSiteDomain & "/code/borders/rewardsBorderImages/rewards_logo.jpg&bgcolor=ffffff&gatewayURL=" & request.currentSite.httpProtocol & thisSiteDomain & "/flashservices/gateway/&teaserProductGroupIDs=" & teaserProductGroupIDs & "&siteDomain=" & thisSiteDomain & "&applicationName=" & applicationName & "&balance=" &balanceQry.balance & "&currentCountry=" & request.relaycurrentuser.countryID & "&linkURL=" & linkURL>
		
	<cfset params = "classid=clsid:d27cdb6e-ae6d-11cf-96b8-444553540000#application.delim1#
	codebase=http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0#application.delim1#
	width=205#application.delim1#
	height=115#application.delim1#
	id=RewardsBanner205#application.delim1#
	align=middle#application.delim1#
	quality=high#application.delim1#
	bgcolor=##ffffff#application.delim1#
	name=RewardsBanner205#application.delim1#
	FLASHVARS=#flashVarsList##application.delim1#
	allowscriptaccess=sameDomain#application.delim1#
	pluginspage=http://www.macromedia.com/go/getflashplayer#application.delim1#
	type=application/x-shockwave-flash#application.delim1#
	pluginspage=http://www.macromedia.com/go/getflashplayer">

	 <cfset contentDrawn = application.com.RelayActiveContent.renderActiveContent("#request.currentSite.httpProtocol##thisSiteDomain#/Flash/incentiveTeaserV2.swf",params)>

	
	</td></tr>
	<tr><td height="10">&nbsp;</td></tr>
	</table>
	</cfif>
	
</cfoutput>

							

