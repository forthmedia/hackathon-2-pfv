<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		report.cfm	
Author:			NJH  
Date started:	2011/02/28
	
Description:	Template to show JasperServer reports

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/02/04	YMA		Closed iframe.  This was preventing rest of page content from rendering as it was left un-closed.
2013/02/04	YMA		Displaying more than 1 report or dashboard on a page means only 1 loading message gets removed on report load.  Changed to reference a unique ID to handle this.

Possible enhancements:


 --->

<cf_param label="Report" name="reportURI" type="string" validvalues="func:com.jasperServer.getReportsQuery(showInternalExternal='external')" display="reportURI" value="reportURI"/> <!--- NOTE: this URI is case sensitive!!!! --->
<cf_param name="height" type="string" default="600"/>
<cf_param name="width" type="string" default="100%"/>
<!---PJP 20/11/2012: CASE 432088: Added in scrolling param for iframe --->
<cf_param name="scrolling" type="string" default="no" validvalues="yes,no,auto" shownull="false"/>

<cfset resourceURI = reportURI>
<cfset resource = "report">

<cf_include template="/jasperServer/loadJasperServerResource.cfm">