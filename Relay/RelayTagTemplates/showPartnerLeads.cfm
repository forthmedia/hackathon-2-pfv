<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			showPartnerLeads.cfm
Author:				SWJ
Date started:		2015-01-20

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/09/25          DAN         PROD2016-2387 - filter leads by type (new tag parameter)

Possible enhancements:


 --->

<cf_param label="Show Add Button" name="showAddButton" default="false" type="boolean"/>
<cf_param label="Show Location Listing" name="showLocLevel" default="false" type="boolean"/>
<cf_param name="leadTypeID" default="-1" label="Lead Type" validValues="func:com.relayForms.getLookupList(fieldname='leadTypeID')" display="translatedItemText" value="LookupID" />
<cf_param name="showTheseColumns" label="Show These Columns" multiple="true" displayAs="twoSelects" allowSort="true" type="string" validValues="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created" default="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created"/>

<cfif application.com.settings.getSetting("versions.leadScreen") eq 1>
	<cf_includeWithCheckPermissionAndIncludes template = "/lead/LeadListPartner.cfm">
<cfelse>
	<cfset variables.showAccountLeads=true>
	<cf_include template="\relay\relayTagTemplates\leadsV2.cfm">
</cfif>