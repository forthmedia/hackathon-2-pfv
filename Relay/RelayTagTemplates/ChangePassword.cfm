<!--- �Relayware. All Rights Reserved 2014 --->
<!---PassNew.cfm

2006-12-12 WAB added translations to JS - changePasswordPhrases.txt
2007/02/26 NJH  - added set password functionaliy, in addition to change password. If checkExistingPW is set to false,
	we don't ask the user to enter their current password. However, we can only do this if they are not an unknown user.
2007-10-08  WAB  make sure that lastupdatedby is set when password is updated
2008-02-07	AJC	changed query to check password to authenticateUser. This can then handle encryption situations.
2008-06-27	AJC	Issue 378: Portal - Change password screen doesn't work
2008/07/09 	NYF 	bug 547 - removed <center><b><i> tags around #Message#
2011/01/05	NJH		LID 5004 - re-worked this page to use the new 8.3 password security measures.
2011/09/09	WAB   removed reference to request.loginlockamount
2012/02/09  PPB		CASE 425258 styling only
2014-07-02 NYB Case 440570 don't redirect unless the result is ok

--->
<cf_param label="Send Confirmation Email" name="sendConfimationEmail" type="boolean" default="No"/>
<cf_param label="Email" name="emailTextID" default="ChangePasswordEmail" validvalues="select eMailTextID as value, eMailTextID as display from emaildef"/>
<cf_param label="Check Existing Password" name="checkExistingPW" type="boolean" default="Yes"/>  <!--- NJH 2007/02/26 --->
<!--- START P-SNY041 DealerNet Sony1 Integration AJC 2007-10-29--->
<!--- <cfparam name="forcePasswordFlagID" default="0"/>
<cfparam name="postRedirectionURL" default=""/>--->
<!--- END P-SNY041 DealerNet Sony1 Integration AJC 2007-10-29--->

<cfif not checkExistingPW and request.relayCurrentUser.isUnknownUser>
	<cfoutput>phr_Ext_SetPword_AnUnknownUserCannotSetPassword</cfoutput>
	<CF_ABORT>
</cfif>

<cfset phrasePrefix = not checkExistingPW?"SetPword":"ChangePword">

<CFIF structKeyExists(FORM,"frmChangePassword") and frmChangePassword and structKeyExists(FORM,"oldPassword") and structKeyExists(FORM,"frmNewPassword1")>
	<!--- check that password is valid --->
	<cfparam name="changePassword" default="false">

	<cfset getResults = application.com.login.authenticateUser(partnerUsername=request.relaycurrentuser.person.username,partnerPassword=FORM.oldPassword)>

	<!--- WAB 2011/09/09 removed reference to request.loginlockAmount --->
	<cfset changePassword = Getresults.RecordCount eq 0?false:true>

	<!--- if password valid, then check that it hasn't expired --->
	<CFIF changePassword is false>
		<CFSET Message = "phr_Ext_#phrasePrefix#_CurrentPasswordIsInCorrect">
	<CFELSE>

		<!--- password was correct, now check to see if it has expired; update relayCurrentUser structure reflecting the new password--->
		<cfset result = application.com.login.updatePassword(frmUsername=request.relayCurrentUser.person.username,frmPassword=form.oldPassword,frmNewPassword1=form.frmNewPassword1,frmNewPassword2=form.frmNewPassword2)>
		<cfset application.com.relayCurrentUser.updatePersonalData()>

		<cfset message = result.isOK?"phr_Ext_#phrasePrefix#_PasswordSuccessfullyChanged":result.message>

		<cfif sendConfimationEmail>
			<cfset application.com.email.sendemail(emailTextID=emailTextID, personID=request.relaycurrentuser.personID)>
		</cfif>
		<!--- P-SNY041 DealerNet Sony1 Integration AJC 2007-10-29--->
		<!--- <cfif forcePasswordFlagID neq 0>
			<cfset application.com.flag.unsetBooleanFlag(entityID=request.relaycurrentuser.personID,flagID=forcePasswordFlagID)>
		</cfif>

		<cfif len(postRedirectionURL) neq 0 and result.isOK><!--- 2014-07-02 NYB Case 440570 don't redirect unless the result is ok --->
			<!--- WAB 2011/09/15 for Sony1 we were in a frame set, but since we do not control it we want to redirect to a page within our own frame, added a test for the parant frame being in the same domain--->
			<cfoutput>
				<script>
					if (parent.frames.length != 0) {
					  // loaded in frames
					  // check whether the parent frame is in the same domain or not.  If not same domain then we need to redirect in our own child frame
					  //  will error if parent frame is in another domain, so catch that error
					  try {
						  parentHostName = window.parent.location.hostname
						  sameDomain = true
					  }
					  catch(err){
						  sameDomain = false
					  }

						if (sameDomain) {
					  window.parent.location = '#jsStringFormat(postRedirectionURL)#';
						}else {
					  	window.location = '#jsStringFormat(postRedirectionURL)#';
						}
					}
					else {
					  // not loaded frames
					  window.location = '#jsStringFormat(postRedirectionURL)#';
					}
				</script>
			</cfoutput>
		</cfif> --->
		<!--- END P-SNY041 DealerNet Sony1 Integration AJC 2007-10-29--->
	</CFIF>
</CFIF>

<!--- if we are in a process and the form has been submitted only process the submission - display nothing! --->
<CFIF not (structKeyExists(request,"processAction") and structKeyExists(FORM,"frmChangePassword") and frmChangePassword is "Yes" and structKeyExists(FORM,"oldPassword") and structKeyExists(FORM,"frmNewPassword1"))>

	<cf_includeJavascriptOnce template="/javascript/login.js" translate="true">

	<CFPARAM NAME="Message" DEFAULT="">

	<cfoutput>
	<SCRIPT type="text/javascript">
	<!--

	function validate () {
		form=$('LoginForm')

		if (!validatePassword()) {
			return false;
		} else {
			form.frmChangePassword.value = 'yes';
			return true;
		}
	}

	//-->
	</SCRIPT>

	<CFIF Not isDefined("NoTitle")>
		<H2>phr_Ext_#htmleditformat(phrasePrefix)#_ChangeYourPassword</H2>
	</CFIF>

	<FORM METHOD="POST" id="LoginForm" name="LoginForm" onSubmit="return validate();">
		<cf_relayFormDisplay class="">
		<INPUT TYPE="hidden" NAME="frmChangePassword" VALUE="no">
		#application.com.relayUI.setMessage(message=message)#

		<p id="changePasswordIntroPhrase">phr_Ext_#htmleditformat(phrasePrefix)#_Intro</p>
			<cfif checkExistingPW>
				<cf_relayFormElementDisplay type="password" label="phr_Ext_#htmleditformat(phrasePrefix)#_CurrentPassword" fieldname="oldPassword" required="true" currentValue="">
			<cfelse>
				<!--- populate the old password with their current password, instead of getting the user themselves to enter it --->
				<CF_INPUT TYPE="hidden" NAME="oldPassword" VALUE="#request.relayCurrentUser.person.password#">
			</cfif>

			<cf_relayFormElementDisplay type="password" label="phr_Ext_#htmleditformat(phrasePrefix)#_NewPassword" fieldname="frmNewPassword1" currentValue="" onKeyUp="validatePasswordStrength(this)" required="true">

			<cf_relayFormElementDisplay type="password" label="phr_Ext_#htmleditformat(phrasePrefix)#_NewPasswordAgain" fieldname="frmNewPassword2" currentValue="" required="true">
			<div class="form-group" id="NewPasswordRow3">
				<div id="passwordStrengthIndicator">#application.com.login.checkPasswordComplexity(password="").fullDescription#</div>
			</div>

		<CF_INPUT type="hidden" name="sendConfimationEmail" value="#sendConfimationEmail#">
		<CF_INPUT type="hidden" name="emailTextID" value="#emailTextID#">

		<div id="changePasswordSubmitButtonDiv">
			<CF_INPUT TYPE="submit" VALUE="phr_Ext_#phrasePrefix#_ChangePassword" class="button btn btn-primary">
		</div>
		 <cfif structKeyExists(request,"processAction")>
	         <CF_INPUT type="HIDDEN" name="frmProcessID" value="#request.processAction.ProcessID#|#request.processAction.nextStepID#">
	    	 <CF_INPUT type="HIDDEN" name="frmProcessUUID" value="#request.processAction.UUID#">
				<!--- this make sure that if this on an element then it doesn't show a continue button, since we have our own button here  --->
			 <cfset request.processAction.continueButton.show = false>
	 	 </cfif>

		</cf_relayFormDisplay>
	</FORM>
	</cfoutput>
</cfif>