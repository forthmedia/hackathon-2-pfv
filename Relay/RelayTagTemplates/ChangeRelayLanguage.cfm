<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ChangeRelayLanguage.cfm	
Author:				SWJ
Date started:		2004-09-25
	
Description:		This simple form is added to a page and then allows
					the user to change their language.
	
	This tag accepts two optional parameters which can be set as follows:
	
	<cfset showTheseLanguageIDs = "1-2-3-4">
	<cfset showToThesePersonIDs = "ALL">

	
	When submitted the form variable is tested for in et.cfm and their language is reset as below:
					
	<CFSET currentLanguageID = FORM.frmShowLanguageID>
	<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID" VALUE="#FORM.frmShowLanguageID#")>
	<cfset application.com.globalFunctions.cfcookie(NAME="language" VALUE="#getSelectedLanguage.currentLanguage#")>											


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-04-26			SSS			Changed the showTheseLanguageIDs so that it works out what langagues are associated with your country and only diplays them

2007/12/11			WAB			changed so that the default for showTheseLanguageIDs is now request.relaycurrentsite.livelanguageids
2016-02-03	WAB Mass replace of displayValidValues using lists to use queries

Possible enhancements:

We could set the users person record when they are logged in.


 --->
<cfparam name="showToThesePersonIDs" default= "ALL">

<cfif (structKeyExists(request.currentSite,"restrictToCountryLanguageList") and request.currentSite.restrictToCountryLanguageList eq "true")>
	<cfset showTheseLanguageIDs = application.com.relayTranslations.getCountryLanguages(#request.relaycurrentuser.content.showforcountryid#)>
<cfelse>
	<cfif isDefined("showTheseLanguageIDs")>
		<cfset showTheseLanguageIDs = REPLACE(showTheseLanguageIDs,"-",",","all")>
	<cfelse>
		<cfset showTheseLanguageIDs = request.currentsite.LiveLanguageIDs>  <!--- WAB 2007/12/11 changed from application. --->
	</cfif>
</cfif> 

<cfif listlen(showTheseLanguageIDs) GT 1>
	<cfif showToThesePersonIDs eq "ALL" or (structKeyExists(COOKIE,"USER") and listFind(showToThesePersonIDs,request.relayCurrentUser.personid))>
	
		<cfparam name="request.currentLanguageID" type="numeric" default="0"> 
		 
		<CFQUERY NAME="getLiveLanguages" DATASOURCE="#application.siteDataSource#">		
		Select LanguageID,  localLanguageName 
		from Language where languageID  in ( <cf_queryparam value="#showTheseLanguageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		order by language
		</CFQUERY>
		
		
		<cfoutput>
		<!--- 2005/08/11 GCC Sony1 Frameset workarround --->
		<cfif isDefined("request.alternativePortalPath")>
			<cfset formAction = request.alternativePortalPath & "?eeid=" & eid>
			<cfset formTarget = "_top">
		<cfelse>
			<cfset formAction = cgi.SCRIPT_NAME & "?" & request.query_string>
			<cfset formTarget = "_self">
		</cfif>
		<form action="#formAction#" target="#formTarget#" method="post" name="ChangeRelayLanguageForm">
			<cf_displayValidValues 
				formFieldName="frmShowLanguageID"	 
				validValues="#getLiveLanguages#"
				dataValueColumn = "languageID"
				displayValueColumn = "localLanguageName"
				nullText="Change Language"
				currentValue="#request.currentLanguageID#"
				onchange = "this.form.submit()"
				>
		</form>
		
		</cfoutput>
	</cfif>
</cfif>


