
<cf_param label="Banners" name="listBanner" title="hint" default="" displayas="twoselects" multiple="true" display="name" value="id" allowSort="true" 
validValues="SELECT b.bannerid as id, b.title as name, 'phr_available_for ' + dbo.getRecordRightsList (b.bannerid,'banner',1,1) as hint FROM banner b WITH (NoLOCK)" />

<cf_param label="Full width banner?" default="No" type="boolean" name="fullWidthBanner" />
<cf_param label="Banner height (in pixels)" name="bannerHeight" type="string" default="" placeholder="Valid values are numbers followed by px" required="true" />

<cfset validatedBannerHeight = application.com.banner.hasPixle(#bannerHeight#) />

<cf_include template="/banner/banner.cfm"> 
