<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		addOrder4SKU
Author:			SWJ
Date created:	2002-07-29

Description:		This has been introduced so that addOrder4SKUInclude can be included 
					as a Relay Tag
					
					To specify which promoid to use set promoid as a parameter


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/06/23   WAB   RACE

Enhancement still to do:



--->
<CFIF not isDefined("prodid")>
	<p>AddOrders for SKU requires the variable prodid to place an order</p>
	<CF_ABORT>
</CFIF>

<!--- 
WAB 2009/06/23 RACE removed to make way for new security system
<!---Gcc bodge - pease forgive me--->
<cfset parentApplicationInclude = "No">
<!--- <cfset hiderightBorder = "yes">
<cfset hideleftBorder = "yes"> --->
<cfinclude template="/orders/application .cfm">
<cfif isDefined("showstandardborderset") and showstandardborderset eq "No">
	<cfinclude template="/orders/addOrder4SKUInclude.cfm">
<cfelse>
	<cfinclude template="/orders/addOrder4SKU.cfm">
</cfif>--->

<!--- Version using the security system --->
<cfif isDefined("showstandardborderset") and showstandardborderset eq "No">
	<cfset fileToInclude = "/orders/addOrder4SKUInclude.cfm">
<cfelse>
		<cfset fileToInclude = "/orders/addOrder4SKU.cfm">
</cfif>

<cf_IncludeWithCheckPermissionAndIncludes.cfm template = "#fileToInclude#">	 

