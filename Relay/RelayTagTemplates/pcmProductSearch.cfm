<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayproductSearchForm.cfm	
Author:			NJH
Date started:		2006/09/26
	
Description:		This provides.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007/05/10			AJC			CR-LEX528 Product Range Order
2007-12-10			SSS			have added another display mode to display in vertical or horizontal.
2012/03/15			IH			Changing Product Sort order to show BSD products at the top

Enhancements still to do:


 --->

<cf_translate>
<cfparam name="attributes.pcmTreeID" type="numeric" default=0>
<cfparam name="attributes.displayMode" type="string" default="simple"> <!--- currently have 'simple, simpleWithIcons, and categoryGrouping' --->
<cfparam name="attributes.LayoutMode" type="string" default="vertical"> <!--- currently have 'horizontal, vertical' --->
<cfparam name="attributes.goToEid" type="string" default="pcmProductDisplay">
<cfparam name="attributes.showWithdrawnTree" type="boolean" default="false">
<cfparam name="attributes.showProductsWithAccessories" type="boolean" default="false">
<cfparam name="request.searchableRemoteCategoryIDList" type="string" default=""> <!--- set in pcmINI.cfm --->
<cfparam name="request.goToProductTextSectionTypeIDList" type="string" default="1">	<!--- default is the overview --->
<cfparam name="request.goToWithdrawnProductTextSectionTypeIDList" type="string" default="1"> <!--- default is the overview --->
<cfparam name="attributes.showBaseModelsOnly" type="boolean" default="true">

<cfparam name="selectPhrase" type="string" default="phr_product_search_selectAprinter">

<cfset displayMode = attributes.displayMode>
<cfset showWithdrawnTree = attributes.showWithdrawnTree>
<cfset gotoEID = attributes.gotoEID>
<cfset showProductsWithAccessories = attributes.showProductsWithAccessories>
<cfset showBaseModelsOnly = attributes.showBaseModelsOnly>

<cf_include template="\code\cftemplates\pcmINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\pcmINI.cfm")>
	<!--- pcmINI is used to over-ride default global application variables and params --->
	<cfinclude template="/code/cftemplates/pcmINI.cfm">
</cfif> --->

<cfset pcmTreeID = attributes.pcmTreeID>
<cfif pcmTreeID eq 0>
	<cfif not isDefined("request.pcmTreeID")>
		<cfoutput>phr_product_NoProductTreeConfiguredForThisSite.</cfoutput>	
	<cfelse>
		<cfset pcmTreeID = request.pcmTreeID>
	</cfif>
</cfif>

<cfif pcmTreeID neq 0>

	<!--- if a searchMode is set in pcmINI.cfm, use it to decide which dropdowns to display; otherwise display all --->
	<cfif isDefined("request.#displayMode#SearchMode")>
		<cfset searchDisplayList = evaluate("request.#displayMode#SearchMode")>
	<cfelse>
		<cfset searchDisplayList = "byProduct,byCategory,byRange">
	</cfif>
	
	<!--- resetting earlier search criteria --->
	<cfif isDefined("searchMode")>
		<cfswitch expression="#searchMode#">
			<cfcase value="product">
				<cfset searchByCategory = "">
				<cfset searchByRange = "">
			</cfcase>
			<cfcase value="category">
				<cfset searchByProduct = "">
				<cfset searchByRange = "">
			</cfcase>
			<cfcase value="range">
				<cfset searchByProduct = "">
				<cfset searchByCategory = "">
			</cfcase>
		</cfswitch>
	</cfif>
	
	
	<cfoutput>
	<script>
	
		<cfif displayMode neq "categoryGrouping">
		
			function submit#jsStringFormat(displayMode)#Form(searchBy,dropDownName) {
				//var tempSearchMode = document.getElementById('searchMode');
				#jsStringFormat(displayMode)#ProductSearchForm.searchMode.value = searchBy;
				//tempSearchMode.value = searchBy;
				
				var SearchCriteria = document.getElementById(dropDownName);
				#jsStringFormat(displayMode)#ProductSearchForm.displayCriteria.value = SearchCriteria.options[SearchCriteria.selectedIndex].value;
				#jsStringFormat(displayMode)#ProductSearchForm.submit();
			}
			
		<cfelse>
		
			// we pass the product Text Section ID as the text section to display
			function submitCategoryGroupingForm(searchBy,prodTextSectionTypeID,dropDownName) {
				var tempSearchMode = document.getElementById('searchMode');
				tempSearchMode.value = searchBy;
				
				var tempProdTextSectionTypeID = document.getElementById('productTextSectionTypeID');
				tempProdTextSectionTypeID.value = prodTextSectionTypeID;
				
				var SearchCriteria = document.getElementById(dropDownName);
				#jsStringFormat(displayMode)#ProductSearchForm.displayCriteria.value = SearchCriteria.options[SearchCriteria.selectedIndex].value;
				#jsStringFormat(displayMode)#ProductSearchForm.submit();
			}
			
		</cfif>
	</script>
	
	
	<table width="100%" border=0 class="#displayMode#ProductSearchTable#attributes.LayoutMode#">
	</cfoutput>
		<cfform name="#displayMode#ProductSearchForm" action="/" method="get">
			<cfinput type="hidden" name="pcmTreeID" value="#pcmTreeID#">
			<cfinput type="hidden" name="eid" value="#goToEid#">
			<cfinput type="hidden" name="displayMode" value="#displayMode#">
			<cfinput type="hidden" name="searchMode" value="product">
			<cfinput type="hidden" name="showWithdrawnTree" value="#showWithdrawnTree#">
			<cfinput type="hidden" name="displayCriteria" value="">
		
			<!--- if we're not displaying a categoryGrouping product search --->
			<cfif displayMode neq "categoryGrouping">
				
				<cfif listContainsNoCase(searchDisplayList,"byProduct")>
					<cfquery name="getProducts" datasource="#application.siteDataSource#">
						select top 1 sourceID,'' as productID, 'phr_product_search_byProduct' as title, 1 as sortOrder from PCMproduct
						union
						select p.sourceID,p.productID, title, 2 as sortOrder from PCMproduct p inner join PCMproductCategoryProduct pcp
						on p.productID = pcp.productID inner join PCMproductCategory pc
						on pcp.categoryID = pc.categoryID and pcp.treeID = pc.TreeID 
						<cfif showBaseModelsOnly>
							inner join PCMlexSearchBaseModels baseModels on baseModels.productKey = p.productKey
						</cfif>
						where (pcp.processStatus <> 'SUPPRESSED')
							and pc.treeID =  <cf_queryparam value="#pcmTreeID#" CFSQLTYPE="CF_SQL_INTEGER" >  
							--and baseModel is null
<!--- 						<cfif request.searchableRemoteCategoryIDList neq "">
							and pc.remoteCategoryID in (#request.searchableRemoteCategoryIDList#)
						</cfif> --->
						<!--- 2012/03/15 IH Changing Product Sort order to show BSD products at the top --->
						order by sortOrder, sourceID desc, title
					</cfquery>
				</cfif>
				<cfif listContainsNoCase(searchDisplayList,"byCategory")>
					  	<cfquery name="getCategories" datasource="#application.siteDataSource#">
						select top 1 '' as remoteCategoryID, 'phr_product_search_byTechnology' as categoryName,'phr_product_search_byTechnology' as categoryDisplayName, 1 as sequence, 1 as sortOrder from PCMproductCategory
						union
						select pc.remoteCategoryID, pc.categoryName, 'phr_category_'+replace(replace(replace(replace(replace(pc.categoryName,' ',''),'/',''),',',''),'-',''),'&','') as categoryDisplayName, isNull(pcs.sequence,0), 2 as sortOrder from PCMproductCategory pc
						left join pcmProductCategorySequence pcs on pc.remoteCategoryID = pcs.remoteCategoryID
						inner join pcmProductCategoryProduct pcp on pc.categoryID = pcp.categoryID
						inner join pcmProduct p on pcp.productID = p.productID 
						<cfif showBaseModelsOnly>
							inner join PCMlexSearchBaseModels baseModels on baseModels.productKey = p.productKey
						</cfif>
						where pc.parentCategoryID is null
						and pc.treeID =  <cf_queryparam value="#pcmTreeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and pcp.processStatus <> 'SUPPRESSED'
<!--- 						<cfif request.searchableRemoteCategoryIDList neq "">
							and remoteCategoryID in (#request.searchableRemoteCategoryIDList#)
						</cfif> --->
						order by sortOrder,sequence,categoryName
					</cfquery>
				</cfif>
				
				<cfif listContainsNoCase(searchDisplayList,"byRange")>
					<cfquery name="getRanges" datasource="#application.siteDataSource#">
						select top 1 '' as priceRangeValue, 'phr_product_search_byRange' as priceRangeDisplay, 1 as sortOrder, 0 as sequence from PCMproduct
						union
						select distinct prsc.priceRange as priceRangeValue, 'phr_product_range'+prsc.priceRange as priceRangeDisplay, 2 as sortOrder,prs.sequence from PCMlexPriceRangeSearchCriteria prsc inner join PCMproduct p
						on prsc.productKey = p.productKey right outer join PCMlexPriceRangeSequence prs
						on prsc.priceRange = prs.priceRange inner join PCMproductCategoryProduct pcp
						on pcp.productID = p.productID inner join PCMproductCategory pc
						on pcp.categoryID = pc.categoryID and pcp.treeID = pc.TreeID
						<cfif showBaseModelsOnly>
							inner join PCMlexSearchBaseModels baseModels on baseModels.productKey = p.productKey
						</cfif>
						where pc.treeID =  <cf_queryparam value="#pcmTreeID#" CFSQLTYPE="CF_SQL_INTEGER" >  
						and pcp.processStatus <> 'SUPPRESSED'
<!--- 						<cfif request.searchableRemoteCategoryIDList neq "">
							and pc.remoteCategoryID in (#request.searchableRemoteCategoryIDList#)
						</cfif> --->
						order by sortOrder,<!---NYB bug7948 removed: prs.--->sequence
					</cfquery>
				</cfif>
				<cfif attributes.LayoutMode eq "vertical">
					<cfif listContainsNoCase(searchDisplayList,"byProduct")>
						<tr class="byProductRow">
						<cfif displayMode eq "simpleWithIcons">
							<td align="center">
								<cfif not fileexists("#request.productImagesDirectory#\#request.byProductImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byProductImage,request.productImagesDirectory,request.byProductImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byProductImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byProductImage#" alt="phr_product_search_byProduct" border="0"></cfoutput>
								</cfif>
							</td>
						</cfif>
							<td align="center">
								<cfselect query="getProducts" id="#displayMode#searchByProduct" name="#displayMode#searchByProduct" display="title" value="productID" selected="#IIF(isDefined('#displayMode#searchByProduct'),'#displayMode#searchByProduct',DE(''))#" onChange="submit#displayMode#Form('Product','#displayMode#searchByProduct');"></cfselect>
							</td>
						</tr>
					</cfif>
					
					<cfif listContainsNoCase(searchDisplayList,"byCategory")>	
						<tr class="byCategoryRow">
						<cfif displayMode eq "simpleWithIcons">
							<td align="center">
								<cfif not fileexists("#request.productImagesDirectory#\#request.byCategoryImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byCategoryImage,request.productImagesDirectory,request.byCategoryImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byCategoryImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byCategoryImage#" alt="phr_product_search_byTechnology" border="0"></cfoutput>
								</cfif>
							</td>
						</cfif>
							<td align="center">
								<cfselect query="getCategories" id="#displayMode#searchByCategory" name="#displayMode#searchByCategory" display="categoryDisplayName" value="remoteCategoryID" selected="#IIF(isDefined('#displayMode#searchByCategory'),'#displayMode#searchByCategory',DE(''))#" onChange="submit#displayMode#Form('Category','#displayMode#searchByCategory');"></cfselect>
							</td>
						</tr>
					</cfif>
					
					<cfif listContainsNoCase(searchDisplayList,"byRange")>
						<tr class="byRangeRow">
						<cfif displayMode eq "simpleWithIcons">
							<td align="center">
								<cfif not fileexists("#request.productImagesDirectory#\#request.byRangeImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byRangeImage,request.productImagesDirectory,request.byRangeImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byRangeImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byRangeImage#" alt="phr_product_search_byRange" border="0"></cfoutput>
								</cfif>
							</td>
						</cfif>
							<td align="center">
								<cfselect query="getRanges" id="#displayMode#searchByRange" name="#displayMode#searchByRange" display="priceRangeDisplay" value="priceRangeValue" selected="#IIF(isDefined('#displayMode#searchByRange'),'#displayMode#searchByRange',DE(''))#" onChange="submit#displayMode#Form('Range','#displayMode#searchByRange');"></cfselect>
							</td>
						</tr>
					</cfif>
				<cfelse>
					<tr class="productdropdowns">
					<td class="empty">
						&nbsp;
					</td>
					<cfif listContainsNoCase(searchDisplayList,"byProduct")>
							<td align="center" class="Products">
						<cfif displayMode eq "simpleWithIcons">
								<cfif not fileexists("#request.productImagesDirectory#\#request.byProductImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byProductImage,request.productImagesDirectory,request.byProductImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byProductImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byProductImage#" alt="phr_product_search_byProduct" border="0"></cfoutput>
								</cfif>
							
						</cfif>
								<cfselect query="getProducts" id="#displayMode#searchByProduct" name="#displayMode#searchByProduct" display="title" value="productID" selected="#IIF(isDefined('#displayMode#searchByProduct'),'#displayMode#searchByProduct',DE(''))#" onChange="submit#displayMode#Form('Product','#displayMode#searchByProduct');"></cfselect>
							</td>
					</cfif>
					<td class="empty">
						&nbsp;
					</td>
					<cfif listContainsNoCase(searchDisplayList,"byCategory")>	
						<td align="center" class="Categories">
						<cfif displayMode eq "simpleWithIcons">
								<cfif not fileexists("#request.productImagesDirectory#\#request.byCategoryImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byCategoryImage,request.productImagesDirectory,request.byCategoryImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byCategoryImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byCategoryImage#" alt="phr_product_search_byTechnology" border="0"></cfoutput>
								</cfif>
						</cfif>
								<cfselect query="getCategories" id="#displayMode#searchByCategory" name="#displayMode#searchByCategory" display="categoryDisplayName" value="remoteCategoryID" selected="#IIF(isDefined('#displayMode#searchByCategory'),'#displayMode#searchByCategory',DE(''))#" onChange="submit#displayMode#Form('Category','#displayMode#searchByCategory');"></cfselect>
							</td>
					</cfif>
					<td class="empty">
						&nbsp;
					</td>
					<cfif listContainsNoCase(searchDisplayList,"byRange")>
						<td align="center" class="Ranges">
						<cfif displayMode eq "simpleWithIcons">
							
								<cfif not fileexists("#request.productImagesDirectory#\#request.byRangeImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,request.byRangeImage,request.productImagesDirectory,request.byRangeImage,true,"thumbnail",70,70)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#request.byRangeImage#")>
									<cfoutput><img src="#request.productImagesDirectoryURL#/#request.byRangeImage#" alt="phr_product_search_byRange" border="0"></cfoutput>
								</cfif>
						</cfif>
								<cfselect query="getRanges" id="#displayMode#searchByRange" name="#displayMode#searchByRange" display="priceRangeDisplay" value="priceRangeValue" selected="#IIF(isDefined('#displayMode#searchByRange'),'#displayMode#searchByRange',DE(''))#" onChange="submit#displayMode#Form('Range','#displayMode#searchByRange');"></cfselect>
							</td>
					</cfif>
					<td class="empty">
						&nbsp;
					</td>
				</tr>
				</cfif>
			
			<cfelse>
			
				<cfinput type="hidden" name="productTextSectionTypeID" value="">
				<!--- we're displaying a categoryGrouping product search --->
				
				<cfquery name="getCategoriesWithProducts" datasource="#application.siteDataSource#">
					<!--- AJC 2007/05/10 CR-LEX528 Product Range Order --->
					select distinct pc.categoryID, pc.remoteCategoryID,categoryName, localCategoryImageProductID,p.productID, p.title, pcs.sequence from PCMproduct p
					<cfif showProductsWithAccessories>
						inner join pcmproductText pt on p.productID = pt.productID
						inner join pcmProductTextSectionAttributes ptsa on ptsa.sectionAttributeID = pt.sectionAttributeID
						inner join pcmProductTextSection pts on pts.sectionID = ptsa.sectionID
						inner join pcmProduct accessory on pt.textData  = accessory.productkey
						inner join pcmProductCategoryProduct apcp on apcp.productID = accessory.productID
					</cfif>
					inner join pcmProductCategoryProduct pcp on p.productID = pcp.productID
					inner join pcmProductCategory pc on pcp.categoryID = pc.categoryID and pcp.treeID = pc.TreeID 
					<!--- AJC 2007/05/10 CR-LEX528 Product Range Order --->
					inner join pcmProductCategorySequence pcs on pc.remotecategoryID = pcs.remotecategoryID 
					<cfif showBaseModelsOnly>
						inner join PCMlexSearchBaseModels baseModels on baseModels.productKey = p.productKey
					</cfif>
					where 1=1
						and pc.treeID =  <cf_queryparam value="#pcmTreeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						--and p.baseModel is null
<!--- 			NJH 2006/11/16 not really sure if we need this...
				<cfif showWithdrawnTree>
						and pcp.processStatus = 'suppressed'
					<cfelse> --->
						and pcp.processStatus <> 'suppressed'
					<!--- </cfif> --->
<!--- 					<cfif request.searchableRemoteCategoryIDList neq "">
						and pc.remoteCategoryID in (#request.searchableRemoteCategoryIDList#)
					</cfif> --->
					<cfif showProductsWithAccessories>
						and pts.sectionTypeID = 5
						and pt.textDataCount=1
<!--- 						<cfif showWithdrawnTree>
							and apcp.processStatus = 'suppressed'
						<cfelse> --->
							and apcp.processStatus <> 'suppressed'
						<!--- </cfif> --->
					</cfif>
					<!--- AJC 2007/05/10 CR-LEX528 Product Range Order --->
					order by pcs.sequence, categoryName, p.title
				</cfquery>
				
				<cfif getCategoriesWithProducts.recordCount gt 0>
					<cfoutput query="getCategoriesWithProducts" group="remoteCategoryID">
						<tr>
							<td class="categoryTitle" colspan="2">
								phr_product_remoteCategory#htmleditformat(remoteCategoryID)#
							</td>
						</tr>
					
						<tr>
							<td class="categoryImage">
								 <cftry> 
									<cfscript>
										treeSuccess = application.com.relayProductTree.setupNavigation(treeID = pcmTreeID);
									</cfscript>
									
								 	<cfcatch type="lock">
										<p>Please wait while the tree is updated. This can take up to 1 minute...</p>
										<script type="text/javascript" language="javascript"> 
											var reloadTimer = null;
											window.onload = function()
											{
											    setReloadTime(5); // In this example we'll use 5 seconds.
											}
											function setReloadTime(secs) 
											{
											    if (arguments.length == 1) {
											        if (reloadTimer) clearTimeout(reloadTimer);
											        reloadTimer = setTimeout("setReloadTime()", Math.ceil(parseFloat(secs) * 1000));
											    }
											    else {
											        location.reload();
											    }
											}
										</script> 
										<CF_ABORT> 
									</cfcatch> 
								</cftry>
								
								<cfset baseTree = application.navstruct.navigation["#PCMtreeID#"]>
	
								<cfset noderesults = structFindValue(baseTree,categoryID,"all")>
								<cfloop from="1" to="#arrayLen(noderesults)#" index="nodeidx">
									<cfif structKeyExists(noderesults[#nodeidx#].owner,"categoryID") AND noderesults[nodeidx].owner.categoryID eq categoryID>
										<cfset currentNode = noderesults[nodeidx].owner>
									</cfif>	
								</cfloop>
								
								<cfscript>
									productThumbnailURLdata = application.com.relayProductTree.getLocalCategoryImage(
										treeID = pcmTreeID, nodeID = categoryID);
									if (len(productThumbnailURLdata.URL) eq 0 and isDefined("currentNode")) { 
									productThumbnailURLdata = application.com.relayProductTree.getThumbnailImageUrl(
										treeID = pcmTreeID,
										node = currentNode);
									}									
								</cfscript>
								
								<cfif fileExists("#request.productCategoryImagesDirectory#\categoryImage_#pcmTreeID#_#categoryID#.png")>
									<img src="#request.productCategoryImagesDirectoryURL#/categoryImage_#pcmTreeID#_#categoryID#.png" alt="#categoryName#" border="0" hspace="0" vspace="0">
								<cfelseif len(productThumbnailURLdata.url) and not find(".bmp",productThumbnailURLdata.url)>
									<cfset nImage = application.com.relayImage.getImageComponent()>
									<cfif not fileexists("#request.ProductImagesDirectory#\#productThumbnailURLdata.productID#tn.png")>		
										<cfscript>
											nImage.readimageFromURL("#request.remoteImagePath#/#productThumbnailURLdata.URL#");
											nImage.blur(nImage.getWidth()/350);
											if (nImage.getWidth()GT nImage.getHeight()){
											nImage.scaleWidth(request.productThumbsize);	
											}	else {nImage.scaleHeight(request.productThumbsize);}
															 
											nImage.writeImage("#request.productImagesDirectory#\#productThumbnailURLdata.productID#tn.png","png");
											nImage.readimage("#request.productImagesDirectory#\#productThumbnailURLdata.productID#tn.png");
											height = nImage.getHeight();
										</cfscript>
										<img src="#request.productImagesDirectoryURL#/#productThumbnailURLdata.productID#tn.png" alt="#categoryName#" border="0" hspace="0" vspace="0">
									<cfelse>
										<cfscript>
											nImage.readimage("#request.productImagesDirectory#\#productThumbnailURLdata.productID#tn.png");
											height = nImage.getHeight();
										</cfscript>
										<img src="#request.productImagesDirectoryURL#/#productThumbnailURLdata.productID#tn.png" alt="#categoryName#" border="0" hspace="0" vspace="0">
									</cfif>
<!--- 								<cfelse>
									phr_product_noImage --->
								</cfif>
							</td>
							
							<td>
								
								
								<!--- PRODUCT TEXT SECTIONS that the user is taken to when selecting a product--->
								<cfif not showWithdrawnTree>
									<cfset sectionTypeIDList = request.goToProductTextSectionTypeIDList>
								<cfelse>
									<cfset sectionTypeIDList = request.goToWithdrawnProductTextSectionTypeIDList>
								</cfif>
	
								<table width="50%" cellpadding=0 cellspacing=0 border=0>
									<cfloop list="#sectionTypeIDList#" index="sectionTypeID">
										<tr>
										
											<cfif not showWithdrawnTree>
												<cfset selectPhrase = "phr_product_search_select"&sectionTypeID>
											<cfelse>
												<cfset selectPhrase = "phr_product_search_selectWithdrawn"&sectionTypeID>
											</cfif>
	
											<cfquery name="getProducts" dbType="query">
												select cast(0 as integer) as productID, '#selectPhrase#' as title, 1 as sortOrder from getCategoriesWithProducts
												union
												select productID, title, 2 as sortOrder from getCategoriesWithProducts 
													where categoryID = #categoryID#
												order by sortOrder, title
											</cfquery>
											
											<td>
												<cfselect query="getProducts" id="searchCategoryGrouping#getCategoriesWithProducts.categoryID##sectionTypeID#" name="searchCategoryGrouping#sectionTypeID##sectionTypeID#" display="title" value="productID" onChange="submitCategoryGroupingForm('Product','#sectionTypeID#','searchCategoryGrouping#getCategoriesWithProducts.categoryID##sectionTypeID#');"></cfselect>
											</td>
										</tr>
									</cfloop> 
								</table>
							</td>
						</tr>
						<tr>
							<td class="spacer" colspan="2">
								&nbsp;
							</td>
						</tr>
					
				</cfoutput>
			<cfelse>
				We're sorry, but there are currently no categories that have products containing supplies information.
			</cfif>
		
			</cfif>
		</cfform>
	</table>

</cfif>

</cf_translate>