<!--- �Relayware. All Rights Reserved 2014 --->
<!--- IF you are here because of <RELAY_SUGGESTIONS> depending on the client it may be calling Suggestions.cfm instead --->
<!---
File name:			FeedbackEmailForm.cfm
Author:				???
Date created:		??/??/??

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
24/03/2005			AJC			Genericised the tag. Now accepts various parameters listed below and
								also uses the phrases table. Also bulit in the ability for an option
								in the drop down box to go to a specific email address.
16/02/2005			GCC			Extended to email multiple people "-" delimited within the pipe delimitation
2010/09/17			NAS			LID3859: Message type ID not being translated
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2010/11/09 			PPB 		LID4605: added the closing "/" on a cf_translate tag; change also done to Suggestions.cfm
2011/10/07			NJH			LID 7842: if contactUsTypeID is not passed in, then don't go to the contactUsReason table to get the reason.
2015/06/09			NJH			Kanban 360: make name and email required. Added email validation on email field.
Enhancement still to do:
Suggest building ability to add more fields to the form by passing a parameter list.
--->

<!--- Accepted Parameters
NAME			TYPE	VALUES		DESCRIPTION
formname		string	any/null	used to prefix phrases so as multiple instances of the tag can
									be used in a site.
subjectPrefix	string	any/null	Prefix of the subject of the email that gets sent
selectOptions	int		1+			Number of options in the select box. The name of option is called as
									phr_#formname#Option#i# where i is a number produced from a loop with the max
									being the selectOptions value
EmailPersonIDs	string	list		A list of personIDs to relate to the options in the select box.
									Allows 1 or a number matching the selectOptions value e.g. if selectOptions
									is 3 the the list can be either 1 or 3 personIDs not 2 or 4+. If 1 is entered
									then all selections go to that email. If no personID is entered then
									the email goes to application.supportEmailName.
receiverflagtextID string any/null	If this is passed and a EmailPersonIDs is not passed then it gets the local(country)
									users for the email to go to

END Accepted Parameters
--->

<cf_translate>
<cf_param name="formname" default="StdForm" label="Phrase Prefix" description="Used to prefix phrases so as multiple instances of the tag can be used in a site."/>
<cf_param name="SubjectPrefix" default="New Message" label="Email Subject Prefix" description="Prefix of the subject of the email that gets sent"/>
<cf_param name="selectOptions" default="1" label="Select Options" description="Number of options in the select box.  The name of option is called as phr_[formname]Option[i]"/>
<cf_param name="EmailPersonIds" default="" description="A list of personIDs to relate to the options in the select box, should be the same number as is set by the selectOptions"/>
<cfparam name="receiverflagtextID" default="">
<cf_param name="contactUsTypeID" default="0" validvalues="select ContactUstypeID as value, ContactUsName as display from ContactUstype"/>
<cf_param name="introText" default="" description="Optional text that can be put at the top of the form"/>
<cfparam name="tableClass" default="withBorder">


<cffunction name="getToEmailAddress" access="private">

	<cfargument name="frmSelectOption" required="yes" type="string">
	<cfargument name="contactUsTypeID" required="yes" type="string">
	<cfargument name="countryList" required="yes" type="string">

	<cfset var GetReceiverDetails = "">

	<cfquery name="GetReceiverDetails" datasource="#application.siteDataSource#">
		select CUContacts.EmailAddress as Email
		from ContactUstype CUtype inner join
		ContactUSReason CUreason on CUtype.ContactUsTypeID = CUreason.ContactUsTypeID inner join
		ContactUsContacts CUContacts on CUreason.ReasonID = CUContacts.ReasonID
		where CUContacts.CountryID  in ( <cf_queryparam value="#arguments.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and CUContacts.reasonID =  <cf_queryparam value="#arguments.frmSelectOption#" CFSQLTYPE="CF_SQL_INTEGER" >
		and CUreason.ContactUsTypeID =  <cf_queryparam value="#arguments.contactUsTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfreturn GetReceiverDetails>

</cffunction>

<cfset request.relayFormDisplayStyle = "HTML-div">
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfif structKeyExists(form,"update") and form.update is "true">


	<!--- If the number of personids for the form is eq to the number of options or is lte 1 --->
	<cfif listlen(EmailPersonIDs,"|") eq selectoptions or listlen(EmailPersonIDs,"|") lte 1 or ContactUsTypeID NEQ 0>
		<cfoutput>
			<H3>phr_#htmleditformat(formname)#Feedbacksubmitted</H3>
			<p>phr_#htmleditformat(formname)#Thankyouforyourinput</p>
		</cfoutput>
		<cfif ContactUsTypeID NEQ 0>
			<cfset GetReceiverDetails = getToEmailAddress(frmSelectOption=frmSelectOption,contactUsTypeID=contactUsTypeID,countryList=request.relaycurrentuser.countryID)>
			<cfset variables.toaddress = "#GetReceiverDetails.email#">
		<cfif GetReceiverDetails.recordcount EQ 0 or GetReceiverDetails.Email EQ "">
			<cfquery name="GetCountryGroup" datasource="#application.siteDataSource#">
				SELECT     CountryGroup.CountryGroupID
				FROM       CountryGroup INNER JOIN
               	Country ON CountryGroup.CountryGroupID = Country.CountryID
				WHERE     (CountryGroup.CountryMemberID = #request.relaycurrentuser.countryID#) AND (NOT (CountryGroup.CountryGroupID IN (#request.relaycurrentuser.countryID#, 37)))
			</cfquery>
			<cfset countrygroups = "">
			<cfif GetCountryGroup.recordcount GT 0>
				<cfloop query="GetCountryGroup">
					<cfset countrygroups = listappend(countrygroups, CountryGroupID)>
				</cfloop>

				<cfset GetReceiverDetails = getToEmailAddress(frmSelectOption=frmSelectOption,contactUsTypeID=contactUsTypeID,countryList=countryGroups)>
			<cfset variables.toaddress = "">
			<cfloop query="GetReceiverDetails">
				<cfif GetReceiverDetails.Email NEQ "">
					<cfset variables.toaddress = listappend(toaddress, "#GetReceiverDetails.email#")>
				</cfif>
			</cfloop>
			</cfif>
		</cfif>
		<cfif GetReceiverDetails.recordcount EQ 0 or len(variables.toaddress) EQ 0>
			<cfset GetReceiverDetails = getToEmailAddress(frmSelectOption=frmSelectOption,contactUsTypeID=contactUsTypeID,countryList=37)>
		</cfif>
			<cfif GetReceiverDetails.recordcount EQ 1>
				<cfset variables.toaddress = "#GetReceiverDetails.email#">
			</cfif>
		<cfelse>
		<!--- If the list of personids is greater than 0 --->
		<cfif listlen(EmailPersonIDs,"|") gt 0>
			<!--- if there are multiple emails addresses. i.e. 1 email address for each dropdown option --->
			<cfif listlen(EmailPersonIDs,"|") gt 1>
				<!--- set the personid of the relevant selected option --->
				<cfset variables.receiverPersonID=listgetat(EmailPersonIDs,form.frmSelectOption,"|")>
			<cfelseif listlen(EmailPersonIDs,"|") eq 1>
			<!--- if 1 person gets all emails from this form --->
				<cfset variables.receiverPersonID = EmailPersonIDs>
			</cfif>
			<!--- get the details of the person who will receive this email --->
			<cfset variables.numberlist = "">
			<cfset variables.letterlist = "">
			<cfloop index="IndexEmail" list="#variables.receiverPersonID#">
				<cfif isnumeric(IndexEmail)>
					<cfset variables.numberlist = listappend(variables.numberlist, IndexEmail)>
				<cfelse>
					<cfset variables.letterlist = listappend(variables.letterlist, IndexEmail)>
				</cfif>
			</cfloop>
			<CFQUERY NAME="GetReceiverDetails" DATASOURCE="#application.sitedatasource#">
				Select email
				from person,organisation
				where person.organisationid=organisation.organisationid
				and personid  in ( <cf_queryparam value="#replace(variables.numberlist,"-",",","ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
 			<!--- set the to email address(es) --->
			<cfif GetReceiverDetails.RecordCount neq 0>
				<cfset variables.toaddress="">
				<cfloop query="GetReceiverDetails">
					<cfif variables.toaddress eq "">
						<cfset variables.toaddress = "#GetReceiverDetails.email#">
					<cfelse>
						<cfset variables.toaddress = variables.toaddress & ",#GetReceiverDetails.email#">
					</cfif>
				</cfloop>
			<cfelse>
				<cfset variables.toaddress="#application.supportEmailName# <#application.supportEmailAddress#>">
			</cfif>
			<cfif isdefined("variables.letterlist")>
				<cfif variables.letterlist NEQ "">
					<cfset variables.toaddress = variables.toaddress & ",#variables.letterlist#">
				</cfif>
			</cfif>
		<cfelse>
			<cfoutput>#formname#FeedbackReceiver</cfoutput>
			<cfquery name="getSendToEmail" datasource="#application.siteDataSource#">
				SELECT i.entityid, p.personid, p.email FROM person p
				INNER JOIN integerMultipleFlagData i ON
				p.personID = i.data
				INNER JOIN flag f ON f.flagID=i.flagID
				INNER JOIN location l ON l.countryid = i.entityid or i.entityid=0
				INNER JOIN person pp ON pp.locationid = l.locationid
				WHERE
				f.flagTextID =  <cf_queryparam value="#formname#Receiver" CFSQLTYPE="CF_SQL_VARCHAR" >
				AND
			  	l.countryid = #request.relaycurrentuser.countryID#
			  	ORDER BY i.entityid DESC
			</cfquery><!---  --->
			<!--- if no personids set then use the support email as default --->
			<cfset variables.toaddress="#application.supportEmailName# <#application.supportEmailAddress#>">
		</cfif>
		</cfif>

		<CFQUERY NAME="GetPartnerDetails" DATASOURCE="#application.sitedatasource#" maxrows="1">
			Select firstname, lastname, personid, email, locationid ,organisation.organisationname, person.organisationid
			from person,organisation where
			person.organisationid=organisation.organisationid and
			personid = #request.relayCurrentUser.personid#
		</CFQUERY>

		<cfif GetPartnerDetails.recordcount neq 0>
			<cfset variables.organisationname=GetPartnerDetails.organisationname>
		<cfelse>
			<cfset variables.organisationname="Unknown">
		</cfif>

		<CF_addCommDetail
			personID 	 = "#request.relayCurrentUser.personid#"
			locationID   = "#request.relayCurrentUser.locationID#"
			commTypeID	 = 11
			commReasonID = 2501
			commStatusID = 3002
			body	 = "#frmMessageBody#"
		>


		<cf_translate phrases="#formname#subjectprefix" />						<!--- 2010/11/09 PPB LID4605 added the closing "/" on the cf_translate tag --->
		<cfset variables.subjectprefix=evaluate("phr_#formname#subjectprefix")>

		<!--- 2010/09/17			NAS			LID3859: Message type ID not being translated --->
		<cfif ContactUsTypeID neq 0>
			<CFQUERY NAME="getReasonName" DATASOURCE="#application.sitedatasource#">
				select reasonname
				from  ContactUsReason
				where reasonID =  <cf_queryparam value="#frmSelectOption#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<cfset reason = getReasonName.reasonname>
		<cfelse>
			<cfset reason = frmSelectOption>
		</cfif>

		<!---TO="#variables.toaddress#" --->
		<CF_MAIL TO="#variables.toaddress#"
			FROM="#application.adminPostmaster#"
			SUBJECT="#variables.subjectprefix# - #frmSubject#"
			BCC="#application.bccemail#">
			<cfoutput>
Sender Name: #form.frmtoName#
Senders Email Address:  #form.frmtoEmail#
Organisation:  #variables.organisationname#

Date:  #dateformat(DateAdded,"dd-mmm-yyyy")# #timeformat(DateAdded,"HH:mm")#

<!--- 2010/09/17			NAS			LID3859: Message type ID not being translated --->
Message Type:  #reason#

Subject:  #frmSubject#

Details:
#frmMessageBody#
			</cfoutput>
</CF_MAIL>
	<!--- If the number of personids for the form is not eq to the number of options and is gt 1 --->
	<cfelse>
		ERROR - you must Specify the correct number of EmailPersonIDs to use with this form.<br>
		This can either be 0(will use support default) EmailPersonIDs, 1 EmailPersonIDs or an equal number to the number of selectdropdown options.
	</cfif>
<CFELSE>

<!--- Entry form --->
	<cfparam name="variables.toName" default="">
	<cfparam name="variables.toEmail" default="">
	<cfparam name="variables.tableClass" default="withBorder">

	<cfif isDefined("request.relayCurrentUser.personid")>
		<cfif request.relayCurrentUser.personid is not "">
			<cfif request.relayCurrentUser.personid is not 404>
				<CFQUERY NAME="GetPartnerDetails" DATASOURCE="#application.sitedatasource#">
					Select firstname, lastname, personid, email, locationid ,organisation.organisationname, person.organisationid
					from person,organisation where
					person.organisationid=organisation.organisationid and
					personid = #request.relayCurrentUser.personid#
				</CFQUERY>
				<cfoutput query="GetPartnerDetails">
					<cfset variables.toName="#firstname# #lastname#">
					<cfset variables.toEmail="#email#">
				</cfoutput>
			</cfif>
		</cfif>
	</cfif>
	<cfoutput>
		<cfform method="post">
			<cf_relayFormDisplay class="#tableClass#">
				<cfif introText neq "">
					<CF_relayFormElementDisplay relayFormElementType="HTML" fieldName="" spanCols="yes" currentValue="#introText#" label="">
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="true" fieldname="Update">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#contactUsTypeID#" fieldname="contactUsTypeID">
				<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#CreateODBCDateTime(request.requestTime)#" fieldname="DateAdded">

				<cfif ContactUsTypeID neq 0>
					<CFQUERY NAME="getSuggestionID" DATASOURCE="#application.sitedatasource#">
						select *
						from ContactUsType inner join ContactUsReason
						on ContactUsType.ContactUsTypeID = ContactUsReason.contactUsTypeID
						where contactUsType.ContactUsTypeID =  <cf_queryparam value="#contactUsTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>
					<cf_querySim>
						getSuggestion
						display,value<cfloop query="getSuggestionID">
						phr_contactUs#htmleditformat(ContactUsTypeID)#reasonID#htmleditformat(reasonID)#|#htmleditformat(reasonID)#</cfloop>
					</cf_querySim>

					<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmSelectOption" size="1" query="#getSuggestion#" display="display" value="value" label="phr_#formname#DropDownTitle" required="Yes" nulltext="Phr_Ext_contactus_ChooseSuggestion" nullValue="">
				<cfelse>
					<cf_querySim>
						getSuggestion
						display,value<cfloop index="i" from="1" to="#selectoptions#" step="1">
						phr_#htmleditformat(formname)#Option#htmleditformat(i)#|phr_#htmleditformat(formname)#Option#htmleditformat(i)#</cfloop>
					</cf_querySim>
					<cfif selectoptions gt 1>
						<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmSelectOption" size="1" query="#getSuggestion#" display="display" value="value" label="phr_#formname#DropDownTitle" required="Yes" nulltext="Phr_Ext_contactus_ChooseSuggestion" nullValue="">
					<cfelse>
						<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="phr_#formname#defaultoption" fieldname="frmSelectOption">
					</cfif>
				</cfif>
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmtoName" currentValue="#variables.toName#" label="phr_#formname#Name" size="40" maxlength="255" tabindex="1" required="yes">
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmtoEmail" currentValue="#variables.toEmail#" label="phr_#formname#Email" size="40" maxlength="255" tabindex="2" required="yes" onValidate="validateEmail">
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="frmSubject" currentValue="" label="phr_#formname#SubjectTitle" size="40" maxlength="80" tabindex="3" required="no">
				<CF_relayFormElementDisplay relayFormElementType="textArea" fieldname="frmMessageBody" currentValue="" label="phr_#formname#MessageDetailTitle" cols="50" rows="10" maxLength="4000">
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="addSuggestionButton" currentValue="phr_#formname#submitbutton" label="" spanCols="No" valueAlign="left" class="button">
			</cf_relayFormDisplay>
		</cfform>
	</CFOUTPUT>
</CFIF>
</cf_translate>