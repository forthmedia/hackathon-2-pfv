<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		PartnerHomePage.cfm
Author:			SWJ
Date created:	/2000

Description:

Date Tested:
Tested by:

Amendment History:

Version 	Date (DD-MMM-YYYY)	Initials 	What was changed
1
2			2001-01-02				WAB		Moved from root to partnerLogin Directory
2.01		2001-01-28 			SWJ		tidied up this document (and renamed from partnerPortalContent)
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Enhancements still to do:
--->


<CFSETTING SHOWDEBUGOUTPUT="no">

<!--- 2005-04-06 MDC - Added in the cfif below to account for using displaybordersV5.
In DisplayBorder personElementList is the variable being passed, where as in displayborderV5
caller.personElementList is being passed. Both display borders are currently being used. --->

<cfif isDefined("caller.personElementList") and caller.personElementList neq "">
	<cfset personElementlist = caller.personElementList>
</cfif> 

<cfparam name="PersonElementList" default="0">
<cfparam name="TableOrientation" type="string" default="Horizontal">
<cfparam name="contentWidth" default="470">

<CFQUERY NAME="GetNewsItems" DATASOURCE="#application.sitedatasource#">
	SELECT e.ID,isExternalFile,URL
	FROM Element e 
	INNER JOIN BooleanFlagData b ON e.id = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	WHERE f.flagtextid='PartnerHomepage_News_Item'
	<cfif PersonElementList neq 0>
		and e.id  in ( <cf_queryparam value="#PersonElementList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	order by sortorder
</CFQUERY>
<cfset newsIDs = valueList(getNewsItems.id)>

<CFQUERY NAME="GetPromos" DATASOURCE="#application.sitedatasource#">
	SELECT e.ID,isExternalFile,URL
	FROM Element e 
	INNER JOIN BooleanFlagData b ON e.id = b.entityid
	INNER JOIN flag f ON f.flagid = b.flagid
	WHERE f.flagtextid='PartnerHomepage_Promo_Item'
	<cfif PersonElementList neq 0>
		and e.id  in ( <cf_queryparam value="#PersonElementList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	order by sortorder
</CFQUERY>
<cfset promoIDs = valueList(GetPromos.id)>
<!--- <cfdump var="#getVisibleElements#"> <cfexit method="EXITTEMPLATE">--->
<!--- <CFSTOREDPROC PROCEDURE="GetPersonElements" DATASOURCE="#application.sitedatasource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE="#frmPersonid#" NULL="No">
	<CFPROCRESULT NAME="PersonElements" RESULTSET="1">
</CFSTOREDPROC>
<CFSET PersonElementList = valuelist(PersonElements.id)> --->


<CFOUTPUT>

<CFIF TableOrientation eq "Vertical">
	<cfset tabwidth1 = contentWidth * 0.45>
	<cfset tabwidth2 = contentWidth * 0.1>
	<TABLE WIDTH="100%" BORDER=0" CELLSPACING="2" CELLPADDING="0" BGCOLOR="">
		<tr valign="top">
			<td valign="top" class="NewsPromoTitle">Phr_LatestNewsText</td>
		</tr>
		<TR>	
			<TD VALIGN="top">
				<table>
				<cfloop query="GetNewsItems">
						<CFIF GetNewsItems.currentRow MOD 2 IS 1><TR><cfelse><td width="#tabwidth2#" valign="top">&nbsp;&nbsp;</TD></CFIF>
							<td width="#tabwidth1#" valign="top">
								<CFIF isExternalFile EQ 1>
									<A TARGET="blank" HREF="#URL#" class="NewsPromoLink">phr_headline_element_#htmleditformat(ID)#</A>
								<CFELSE>
									<A HREF="/?eid=#id#" class="NewsPromoLink">phr_headline_element_#htmleditformat(ID)#</A>
								</CFIF>
								<br>phr_summary_element_#htmleditformat(ID)# <CFIF isExternalFile EQ 1>
									<A TARGET="blank" HREF="#URL#" class="NewsPromoLink">&gt;</A></A>
								<CFELSE>
									<A HREF="/?eid=#id#" class="NewsPromoLink">&gt;</A></A>
								</CFIF>
							</TD>
						<CFIF GetNewsItems.currentRow MOD 2 IS 0></TR></CFIF>
				</CFloop>
				</table>
			</TD>
		</TR>
		
		<TR>
			<td>&nbsp;</td>
		</TR>
		
		<TR>
			<TD class="NewsPromoTitle">Phr_LatestPromotionsText</TD>
		</TR>
		
		<TR>
			<TD VALIGN="top">
				<table>
				<cfloop query="getPromos">
						<CFIF getPromos.currentRow MOD 2 IS 1><TR><cfelse><td width="#tabwidth2#" valign="top">&nbsp;&nbsp;</td></CFIF>
							<td width="#tabwidth1#" valign="top">
							<CFIF isExternalFile EQ 1>
								<A TARGET="blank" HREF="#URL#" class="NewsPromoLink">phr_headline_element_#htmleditformat(ID)#</A>
							<CFELSE>
								<A HREF="/?eid=#id#" class="NewsPromoLink">phr_headline_element_#htmleditformat(ID)#</A>
							</CFIF>
							<br>phr_summary_element_#htmleditformat(ID)# <CFIF isExternalFile EQ 1>
								<A TARGET="blank" HREF="#URL#" class="NewsPromoLink">&gt;</A></A>
							<CFELSE>
								<A HREF="/?eid=#id#" class="NewsPromoLink">&gt;</A></A>
							</CFIF>
						</td>
						<CFIF getPromos.currentRow MOD 2 IS 0></TR></CFIF>
				</CFloop>
				</table>
			</TD>
		</TR>				
	
	</TABLE>

<CFELSE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="2" BGCOLOR="">
	
	<TR>
	<TD>&nbsp;</TD>
		<CFIF isDefined("phr_img_LatestNews")>
			<TD><IMG SRC="/content/MiscImages/#phr_img_LatestNews#" ALT="Latest News" BORDER="0"></TD>
		<CFELSE>
			<TD><H2>Latest News</H2></TD>
		</CFIF>
		
		<td width="15px">&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<CFIF isDefined("phr_img_LatestPromotions")>
			<TD><IMG SRC="/content/MiscImages/#phr_img_LatestPromotions#" ALT="Latest News" BORDER="0"></TD>
		<CFELSE>
			<TD><H2>Latest Promotions</H2></TD>
		</CFIF>
	</TR>
	
	<TR>
		<TD>&nbsp;</TD><TD>&nbsp;</TD>
		<td >&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<TD>&nbsp;</TD>
	</TR>

	<TR>	
	<TD>&nbsp;</TD>
		<TD width="40%" ALIGN="left" VALIGN="top">
			<cfloop query="GetNewsItems">
				<P>
				<CFIF isExternalFile EQ 1>
					<A TARGET="blank" HREF="#URL#">phr_headline_element_#htmleditformat(ID)#</A>
				<CFELSE>
					<A HREF="/?eid=#id#">phr_headline_element_#htmleditformat(ID)#</A>
				</CFIF>
				<br>phr_summary_element_#htmleditformat(ID)#</P>
			</CFloop>
		</TD>
		<td width="15px">&nbsp;</td>
		<td width="15px" style="border-left-width: 1px; margin-left: 20px; border-style: solid; border-color: navy; margin-right: 20px; border-bottom-width: 0; border-right-width: 0; border-top-width: 0;">&nbsp;</td>
		<TD width="40%" ALIGN="left" VALIGN="top">
			<cfloop query="getPromos">
				<P>
				<CFIF isExternalFile EQ 1>
					<A TARGET="blank" HREF="#URL#">phr_headline_element_#htmleditformat(ID)#</A>
				<CFELSE>
					<A HREF="/?eid=#id#">phr_headline_element_#htmleditformat(ID)#</A>
				</CFIF>
				<BR>phr_summary_element_#htmleditformat(ID)#</P>
			</CFloop>
		</TD>
	</TR>				

</TABLE>
</CFIF>
</CFOUTPUT>