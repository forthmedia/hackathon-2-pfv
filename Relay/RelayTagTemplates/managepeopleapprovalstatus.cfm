<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			managePeopleApprovalStatus.cfm
Author:				GCC
Date started:			2007-02-21

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-05-13			NYF	& GCC	extensive changes - see Release Notes\bugs trend nabu\bug316.doc
2008-06-02			NYF			Changed the 'GetPeople' query to use frmOrganisationID & frmPersonID, and add defaults to these - current user - to allow this module to be used in the internal [org] Approval Process
2008-07-04			NYF			Changed frmPersonID in cfparam and 'GetPeople' query to CurrentUser - to stop user whose status is changed becoming frmPersonid and consequently disappearing from the list
2008-07-17			AJC			Issue 809: Cannot view more than 50 people on Approval tab in CompanyAdmin
2010-11-16			NAS			LID4781: Issues with untranslatable fields
2016-03-31			RJT			BF-572 Reorganise query so people without any approval status still appear in the manage my collegues list
2016/06/17			NJH			JIRA PROD2016-425 - if at location level, then only show people at person's location.
2016/07/07			RJT			PROD2016-1354 Allowed the organisationID to be grabbed from both frmOrganisationID and OrganisationID as both seem to be being used
Possible enhancements:

Enable mulitple people to be approved at once - this does depend on the approval process being simple - i.e. not asking a lot of per person questions.

 --->

<!--- 2009-06-16 GCC LID2367 ensure known user and logged in--->
<cfif not request.relayCurrentUser.isUnknownUser and request.relayCurrentUser.isLoggedIn>

	<cfparam name="RegApprovalPrefix" default="">
	<cfparam name="sortOrder" type="string" default="approvalStatus,lastname">
	<cfparam name="ManagePeopleApprovalFlags" default="PerApproved,PerRejected">
	<cfparam name="numRowsPerPage" default="200">

	<cfset filterByEntityTypeUniqueKey = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount")?"locationID":"organisationID">

	<!---RJT Grab the organisationID from assorted possible locations --->
	<!--- NJH 2016/11/16 JIRA PROD2742 - default the filter variable to current user values --->
	<cfset variables[filterByEntityTypeUniqueKey]=request.relaycurrentuser[filterByEntityTypeUniqueKey]>
	<cfif request.relayCurrentUser.isInternal>
		<cfif structKeyExists(variables,"frm#filterByEntityTypeUniqueKey#")>
			<cfset variables[filterByEntityTypeUniqueKey]=variables["frm"&filterByEntityTypeUniqueKey]>  <!---When initially hitting the new accounts Approvals approving an organisation then its child people --->
		<cfelseif structKeyExists(url,filterByEntityTypeUniqueKey)>
			<cfset variables[filterByEntityTypeUniqueKey]=url[filterByEntityTypeUniqueKey]> <!--- PROD2016-1354 When hitting the new accounts Approvals approving an organisation then its child people after approving (i.e. hitting it for the second time)  --->
		</cfif>
	</cfif>


	<cffunction name="updatePersonActiveStatus" access="private">

		<cfargument name="personid" required="Yes" type="numeric">
		<cfargument name="activate" required="Yes" type="boolean" default="0">

		<cfset var qDeactivatePeople = "">
		<cfset var qGetPerson = "">

		<!--- setting update dates to be personid because the EBA has no usergroup as an external user... this will report wierd --->
		<CFQUERY NAME="qDeactivatePeople" datasource="#application.siteDataSource#">
			update person
			set active =  <cf_queryparam value="#arguments.activate#" CFSQLTYPE="cf_sql_float" > ,
			lastupdatedby = #request.relaycurrentuser.personid#,
			lastupdated = getdate()
			where personid =  <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFQUERY NAME="qGetPerson" datasource="#application.siteDataSource#">
			select isnull(firstname,'') + ' ' + isnull(lastname,'') as fullname
			from Person
			where personid =  <cf_queryparam value="#arguments.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<cfreturn qGetPerson>
	</cffunction>

	<cfif not request.relaycurrentuser.isinternal>
		<cf_displayBorder
			noborders=true
		>
	</cfif>

	<h1>phr_Approve_Colleagues</h1>
	<cfif structKeyExists(URL, "frmRowIdentity")>
		<p>
		<cfloop list=#URL.frmRowIdentity# index="frmPersonID">
			<!--- 2009-06-16 GCC LID2367 final check to ensure only applying changes to people in the currentusers organisation --->
			<cfset getEntityDetails = application.com.relayplo.getEntityStructure(entityTypeID = 0,entityid = frmPersonID,getEntityQueries=false)>
	 		<cfset rights = application.com.rights.getPolrights(entityStruct = getEntityDetails)>
	 		<cfset rightsOK = (rights.recordcount is 0 or rights.updateOkay is 0)?false:true>

			<!--- 2009-07-29 GCC LID2367 this tag is sometimes used internally as part of the approvals process - allow internals to approve full stop --->
			<cfif (getEntityDetails.ID[replace(filterByEntityTypeUniqueKey,"ID","")] eq request.relaycurrentuser[filterByEntityTypeUniqueKey] and rightsOK) or request.relaycurrentuser.isInternal>
				 <cfscript>
					application.com.flag.setbooleanflag(entityID=frmPersonID,flagtextid=action,deleteOtherRadiosInGroup="true");
					if (action contains "Approv") {
						user = application.com.login.createUserNamePassword(personid=frmPersonID);
					}
					user = updatePersonActiveStatus(personID=frmPersonID,activate=1);
					writeoutput(user.fullname & " phr_action_#action#<br>");
				 </cfscript>

				<cfset notifyChange="false">
				<cf_include template="/code/cftemplates/RegistrationApprovalPer.cfm" onNotExistsTemplate="/approvals/RegistrationApprovalPer.cfm">
			<cfelse>
				<cfoutput>#htmleditformat(getEntityDetails.entityName)# phr_Sys_not phr_action_#htmleditformat(action)#<br></cfoutput>
			</cfif>
		</cfloop>
		</p>
	</cfif>

	<cfscript>
		comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

		comTableFunction.functionName = "Phr_Sys_ApprovalFunctions";
		comTableFunction.securityLevel = "";
		comTableFunction.url = "";
		comTableFunction.windowFeatures = "";
		comTableFunction.functionListAddRow();

		comTableFunction.functionName = "--------------------";
		comTableFunction.securityLevel = "";
		comTableFunction.url = "";
		comTableFunction.windowFeatures = "";
		comTableFunction.functionListAddRow();
	</cfscript>

	<cfloop list="#ManagePeopleApprovalFlags#" index="flagTextID">
		<cfscript>
			comTableFunction.functionName = "&nbsp;phr_sys_#flagTextID#CheckedPeople";
			comTableFunction.securityLevel = "";
			comTableFunction.url = "/relaytagtemplates/managePeopleApprovalStatus.cfm?sortorder=#sortorder#&ManagePeopleApprovalFlags=#ManagePeopleApprovalFlags#&action=#flagTextID#&#filterByEntityTypeUniqueKey#=#variables[filterByEntityTypeUniqueKey]#&frmRowIdentity=";
			comTableFunction.windowFeatures = "";
			comTableFunction.functionListAddRow();
		</cfscript>
	</cfloop>

	<CFQUERY NAME="GetPeople">
		<!---
		RJT BF-572
		This was previously a single query with inner joins, however this missed people with no approval status as at BF-572.
		Equally a left join misses people with any other boolean flag set (i think due to te 3 levels of left join).
		The following seems like the simplest way to express the concept, but could probably be colapsed down into one query
		if performance required it
		--->

		DECLARE @peopleApprovalStatus TABLE
		(
			personID int,
			FirstName nVarChar(max),
			LastName nVarChar(max),
			EmailAddress nVarChar(max),
			Salutation nVarChar(max),
			ApprovalStatus nVarChar(max) default 'phr_noApprovalStatus'
		)

		insert into @peopleApprovalStatus(personID,FirstName,LastName,EmailAddress,Salutation)
		select p.PersonID, p.FirstName, p.LastName, p.Email, p.Salutation AS EmailAddress
		from person p
		WHERE p.#filterByEntityTypeUniqueKey# = <cf_queryparam value="#variables[filterByEntityTypeUniqueKey]#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfif not request.relaycurrentuser.isInternal>AND p.personid <>  <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" > </cfif>

		update peopleApprovalStatus
		set ApprovalStatus='phr_' + flag.Namephrasetextid
		FROM flag
		inner JOIN BooleanFlagData ON flag.FlagID = BooleanFlagData.FlagID
		inner JOIN @peopleApprovalStatus peopleApprovalStatus on BooleanFlagData.EntityID=peopleApprovalStatus.personID
		inner JOIN FlagGroup ON flag.FlagGroupID = FlagGroup.FlagGroupID and FlagGroup.FlagGroupTextID =  'PerApprovalStatus'

		select personID,FirstName,LastName,EmailAddress,Salutation,ApprovalStatus from @peopleApprovalStatus
		order by <cf_queryObjectName value="#sortOrder#">
	</CFQUERY>

	<cfif IsQuery(GetPeople) and getpeople.recordcount gt 0>
		<CF_tableFromQueryObject queryObject="#GetPeople#"
		HidePageControls="no"
		numRowsPerPage ="#numRowsPerPage#"
		useInclude = "false"
		rowIdentityColumnName="personID"
		functionListQuery="#comTableFunction.qFunctionList#"
		showTheseColumns="salutation,firstname,lastname,EmailAddress,ApprovalStatus"
		columnTranslation="true"
		ColumnTranslationPrefix="phr_Ext_"
		sortorder="#sortOrder#">
	<cfelse>
		phr_sys_NobodyToApprove
	</cfif>
</cfif>