<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY NAME="GetChildren" datasource="#application.sitedatasource#">
	SELECT DISTINCT e.headline, e.headline, e.url, e.id, e.isexternalfile,e.lastUpdated, e.sortorder
	FROM 
       (SELECT recordid, 
				usergroupid 
	 			FROM 	recordrights 
	 			WHERE 	entity = 'ELEMENT') AS rr 
        	RIGHT JOIN Element AS e
        	ON rr.recordid = e.id
        	LEFT JOIN UserGroup 
       	ON rr.usergroupid = UserGroup.UserGroupID
	LEFT JOIN rightsgroup 
	ON UserGroup.UserGroupID = rightsgroup.UserGroupID
	WHERE 
			(
			e.ParentID =  <cf_queryparam value="#RelayTag.ElementID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		AND 
			e.isLive=1
		AND 
			e.statusid=4
		AND 
			rightsgroup.PersonID =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND
			(EXISTS (SELECT 1
                               FROM countryScope csc
                              WHERE csc.entity = 'Element' 
                                AND csc.entityId = e.id
                                AND csc.countryId IN (SELECT cgr.countrygroupid
							FROM person per
						  INNER JOIN location loc ON loc.locationId = per.locationId
					          INNER JOIN countrygroup cgr ON cgr.countrymemberid = loc.countryid
						       WHERE per.personId =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )))
			)
		OR
			(
			e.ParentID =  <cf_queryparam value="#RelayTag.ElementID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		AND 
			e.isLive=1
		AND 
			e.statusid=4
		AND 
			rr.usergroupid Is Null
		AND	NOT		EXISTS (SELECT 1
	                         FROM countryScope csc
                                WHERE csc.entity = 'Element' 
                                  AND csc.entityId = e.id)
			)
		OR
			(
			e.ParentID =  <cf_queryparam value="#RelayTag.ElementID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		AND 
			e.isLive=1
		AND 
			e.statusid=4
		AND 
			rr.usergroupid Is Null
		AND		(EXISTS (SELECT 1
                               FROM countryScope csc
                              WHERE csc.entity = 'Element' 
                                AND csc.entityId = e.id
                                AND csc.countryId IN (SELECT cgr.countrygroupid
							FROM person per
						  INNER JOIN location loc ON loc.locationId = per.locationId
					          INNER JOIN countrygroup cgr ON cgr.countrymemberid = loc.countryid
						       WHERE per.personId =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )))
			)
		OR
			(
			e.ParentID =  <cf_queryparam value="#RelayTag.ElementID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		AND 
			e.isLive=1
		AND 
			e.statusid=4
		AND 
			rightsgroup.PersonID =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND
			NOT EXISTS (SELECT 1
	                         FROM countryScope csc
                                WHERE csc.entity = 'Element' 
                                  AND csc.entityId = e.id)
			)
	Order by e.sortorder
</CFQUERY>
