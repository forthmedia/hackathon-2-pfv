<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ordersPrevious.cfm	
Author:				SWJ
Date created:		07 July 2002

Description:		This tag lists the previous orders for this person

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

	2006/08/18 		NJH			included template ordersParam.cfm to define params that we might need for orders.
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate	

Enhancement still to do:


 --->
<!--- NJH 2006/08/18 added so we can view a single order --->
<cf_param name="orderID" default=""/>
<cfparam name="bypersonid" default="yes" type="boolean">  <!--- NJH 2011/08/04 - added as byPersonId not defined.. not sure where it gets set.. perhaps this is a security loophole??? --->
<cf_param label="ElementID to View Order" name="displaylinkeid" default=""/>

<cf_include template="\code\cftemplates\ordersINI.cfm" checkIfExists="true">

<CFPARAM name="requiredstatus" default="#RealOrderStatuses#">
<!--- AJC 2006-09-25 P_Lex003 --->
<!--- not sure which query to use! 
<CFQUERY NAME="getPreviousOrders" DATASOURCE=#application.sitedatasource# DEBUG>
SELECT 	orderid,
		orderdate,
		orderstatus,
		firstname,
		lastname, 
		(select count(1) from orderitem as i where i.orderid = o.orderid and active=1) as numberofitems,
		prm.BorderSet		
from 
		orders as o, 
		person as p,
		Promotion as prm	
where 
		orderstatus in (#requiredstatus#)
and 	o.personid=p.personid
and		prm.promoid = o.promoid
and		o.locationid in (#request.relayCurrentUser.locationID#)   <!--- NJH 2006/08/18 changed from #frmLocationID# to persons location... not entirely sure if this is what we want--->
<cfif isDefined("orderID") and orderID neq "">
and		o.orderID = #orderID#
</cfif>
<cfif isDefined("promoid")>
and		o.promoid = '#promoid#'
</cfif>
<cfif bypersonid is true>
	and p.personid=#request.relaycurrentuser.personid#
</cfif>
order by 
		orderdate desc
</CFQUERY>--->

<CFQUERY NAME="getPreviousOrdersDetail" DATASOURCE=#application.sitedatasource# DEBUG>
SELECT 	o.orderid,
		o.orderdate,
		o.created,
		o.orderstatus,
		o.ordernumber,
		firstname,
		lastname, 
		i.SKU,
		pr.description,
		i.quantity,
		s.description as orderstatusdescription
		
from orders as o
INNER JOIN person p ON o.personid=p.personid
LEFT JOIN (orderitem i 
INNER JOIN product pr ON pr.productid=i.productid) ON o.orderid=i.orderid and i.active = 1
INNER JOIN status s ON o.orderstatus=s.statusid
where orderstatus  in ( <cf_queryparam value="#requiredstatus#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
and		o.locationid in (#request.relayCurrentUser.locationID#) <!--- NJH 2006/08/18 changed from #frmLocationID# to persons location... not entirely sure if this is what we want--->
<!--- and		o.orderid=i.orderid AJC - created left join incase order is saved and no orderitems present--->
<!--- AJC 2006-09-25 P_Lex003 --->
<cfif isDefined("orderID") and orderID neq "">
and		o.orderID =  <cf_queryparam value="#orderID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfif>
<cfif bypersonid is true>
	and o.personid=#request.relaycurrentuser.personid#
</cfif>
order by o.created desc
</CFQUERY>


<SCRIPT type="text/javascript">
	<!--

		<!--- put in for viewer - not tested yet--->

		function jsSubmit()  {
				document.mainForm.submit()
		}

		function displayOrder (page, orderid)  {
			form = document.mainForm;
			<cfif displaylinkeid is not "">
				form.action = <cfoutput>page;</cfoutput>
			<cfelse>
				form.action = <cfoutput>'/orders/'+page;</cfoutput>
			</cfif>
			
			form.currentOrderID.value=orderid;
			
			form.submit();
		}				

	//-->
</SCRIPT>

<cfform name="mainForm" method="post">
		<cfinput type="hidden" value="" name="currentOrderID">
</cfform>
<cf_translate>
<TABLE width="100%" class="withborder">
	<cfif showtitle>
	<TR>
		<TD colspan = "3" class="midlabel"><B>
			<CFOUTPUT> 
				<CFIF requiredstatus IS "1">
					phr_Order_OutstandingQuotes
				<CFELSE>
					phr_Order_PreviousOrders
				</CFIF>
			</CFOUTPUT>
		</TD>
	</TR>
	</cfif>
	<CFIF getPreviousOrdersDetail.recordcount IS NOT 0>
		<cfif showOrderItems is false>
			<TR>
				<cfif showorderid><TD class="label"><strong>phr_Order_OrderID</strong></TD></cfif>
				<cfif showordernumber><TD class="label"><strong>phr_Order_Claim_Reference</strong></TD></cfif>
				<cfif showorderdate><TD class="label"><strong>phr_Order_OrderDate</strong></TD></cfif>
				<cfif showcreateddate><TD class="label"><strong>phr_Order_CreatedDate</strong></TD></cfif>
				<cfif showordercontactname><TD class="label"><strong>phr_Order_OrderedBy</strong></TD></cfif>
				<cfif showorderstatus><TD class="label"><strong>phr_Order_status</strong></TD></cfif>
				<cfif showorderdetailslink><td>&nbsp;</td></cfif>
			</TR>
		</cfif>
		<CFOUTPUT query="getPreviousOrdersDetail" group="orderid">
			<cfif showOrderItems>
				<TR>
					<cfif showorderid><TD class="label"><strong>phr_Order_OrderID</strong></TD></cfif>
					<cfif showordernumber><TD class="label"><strong>phr_Order_Claim_Reference</strong></TD></cfif>
					<cfif showorderdate><TD class="label"><strong>phr_Order_OrderDate</strong></TD></cfif>
					<cfif showcreateddate><TD class="label"><strong>phr_Order_CreatedDate</strong></TD></cfif>
					<cfif showordercontactname><TD class="label"><strong>phr_Order_OrderedBy</strong></TD></cfif>
					<cfif showorderstatus><TD class="label"><strong>phr_Order_status</strong></TD></cfif>
					<cfif showorderdetailslink><td>&nbsp;</td></cfif>
				</TR>
			</cfif>
			<TR>
				<cfif showorderid><TD class="label">#htmleditformat(orderid)#</TD></cfif>
				<cfif showordernumber><TD class="label">#htmleditformat(ordernumber)#</TD></cfif>
				<cfif showorderdate><TD class="label">#dateformat(orderdate)#</TD></cfif>
				<cfif showcreateddate><TD class="label">#dateformat(created)#</TD></cfif>
				<cfif showordercontactname><TD class="label">#htmleditformat(firstname)# #htmleditformat(LastName)#</TD></cfif>
				<cfif showorderstatus><TD class="label">#htmleditformat(orderstatusdescription)#</TD></cfif>
				<cfif showorderdetailslink>
					<cfif displaylinkeid is not "">
						<TD><A HREF="javascript:displayOrder('/?eid=#displaylinkeid#&currentorderid=#orderid#',#orderid#)" >#phr_Details#</A></TD>
					<cfelse>
						<TD><A HREF="javascript:displayOrder('<CFIF orderstatus IS 1>editorder.cfm<CFELSE>vieworder.cfm</CFIF>',#htmleditformat(orderid)#)" >phr_Details</A></TD>
					</cfif>
				</cfif>
			</TR>
			<!--- AJC 2006-09-25 P_Lex003 --->
			<cfif showOrderItems>
				<TR>
					<TD class="midlabel"><B>phr_Quantity</B></TD>
					<TD class="midlabel"><B>SKU</TD>
					<TD class="midlabel"><B>phr_Description<B></TD>
				</TR>
				<CFOUTPUT>	
					<TR>
						<TD>#htmleditformat(quantity)#</TD>
						<TD>#htmleditformat(SKU)#</TD>
						<TD>#htmleditformat(Description)#</TD>
					</TR>
				</CFOUTPUT>
			</cfif>
			<!--- AJC 2006-09-25 P_Lex003  --->
			<!--- <TR>
				<TD>&nbsp;</TD>
			</TR> --->
		</CFOUTPUT>
		<!--- AJC 2006-09-25 P_Lex003  --->
		<!--- <TR>
			<TD>&nbsp;</TD>
		</TR> --->
	<CFELSE>
		<TR>
			<TD><CFOUTPUT>phr_Order_NoMatchingTransactionsFound</CFOUTPUT></TD>
		</TR>
	</cfif>
</TABLE>
</cf_translate>


