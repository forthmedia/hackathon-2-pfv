<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		elearningCertificationRegistration.cfm	
Author:			NJH  
Date started:	26-08-2008
	
Description:	eLearning certification registration for the portal 		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-DEC-2013		AXA		Case 437312: added cf_param tag to display sorting options
19-JUL-2016		GCC		448887 - updated form name to be dynamic to enable tag to be called multiple time son the same page
Possible enhancements:


 --->
<cf_param label="Certification TypeID" name="certificationTypeID" type="numeric" default="0" validValues="select description as display, certificationTypeId as value from trngCertificationType order by display" nullText="All"/>
<!--- 03-DEC-2013 AXA Case 437312: added cf_param tag to display sorting options --->
<cf_param label="Sort By" name="SortOrder" type="string" default="Code,Description" validValues="Code, Code Desc, Title, Title Desc, Description, Description Desc, SortOrder, SortOrder Desc" nullText="None"/>
<cf_param label="formName" name="formname" type="string" default="mainForm">	
<cfparam name="frmRunInPopUp" type="boolean" default="no">



<cfform name="#formName##CertificationTypeID#"  method="post">
	<cfinclude template="\elearning\certificationRegistration.cfm">
</cfform>