<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	oppCustomerList.cfm

	This template will return a list of end customers of the logged-in person at a given reseller
	OR all the end customers of the given reseller if the logged-in person is the Primary Contact or Partner Lead Manager

	we want to list the customer orgs and just the opp details of the next opp that is expected to close OR the next asset that is due for renewal if the customer doesn't have any relevant opportunities

	Parameters:

	Author: Peter Barron 2010-03-20

Amendment History:
2012-01-20	NYB	Case#425042 fixed fact reportShowTheseColumns was ending up with more entries then reportColumnHeadingList
2012-08-09	MS	P-KAS002 	Users can pass the the argument showAddNewEndCustButton set as false in the RELAY_OPPCUSTOMERLIST RelayTag to Hide or Show The Add new Button on My Opportunities Tab under my customers on Portal
2012-09-19 	PPB Case 430388 filter to ALL locations of the partners organisation
2014-09-29  AXA set TextID as value for Opportunity Types valid values.  Was not passing through correctly.
2015/09/30	NJH	JIRA PROD2015-35 - added TFQO text search
2015/11/03	RJT Made consistent with other filtered TFQO; now always displays TFQO as may be filtered (so temporarily showing nothing)
2016/04/27	NJH Sugar 448757 - performance enhancements - changed in statement to join. Still more queries to work on, but this was a big improvement on kaspersky
2016/12/12	NJH JIRA PROD2016-2937 - remove the reseller OrgID parameter. Now done in rights. Added rights sql to opportunities query
Possible Enhancements:
--->

<cfparam name="startRow" default="1"/>
<cf_param name="numRowsPerPage" default="100"/>
<cf_param name="defaultSortOrder" label="Sort Order" default="OrganisationName"/>
<cfparam name="sortOrder" default="#defaultSortOrder#">
<cf_param name="reportShowTheseColumns" default="Customer,Contact,NextExpectedCloseValue,NextExpectedCloseDate,ViewButton" validvalues="Customer,Contact,NextExpectedCloseValue,NextExpectedCloseDate,ViewButton" multiple="true" displayAs="twoSelects" allowSort="true"/>
<cf_param name="dateFilter" default="NextExpectedCloseDate" validValues="NextExpectedCloseDate"/>
<cfparam name="reportCurrencyFormat" default="NextExpectedCloseValue"/>
<cf_param label="Show Opportunity Types" name="showOppTypes" default="Deals,Renewal"  multiple="true" size="4" validValues="select oppTypeTextID as value, oppType as display from oppType order by oppTypeTextID"/><!--- 2014-09-29  AXA set TextID as value for Opportunity Types valid values.  Was not passing through correctly. --->

<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">

<cfif isdefined('useOppProductTotals') and not useOppProductTotals>
	<!--- NYB 2012-01-20 Case#425042 removed entry for reportColumnHeadingList - this is dealt with in the ReplaceList below --->
	<cfif listfind(reportShowTheseColumns,"NextExpectedCloseValue",",") gt 0>
		<cfset reportShowTheseColumns = listDeleteAt(reportShowTheseColumns,listfind(reportShowTheseColumns,"NextExpectedCloseValue",","),",")>
	</cfif>
	<cfif listfind(reportCurrencyFormat,"NextExpectedCloseValue",",") gt 0>
		<cfset reportCurrencyFormat = listDeleteAt(reportCurrencyFormat,listfind(reportCurrencyFormat,"NextExpectedCloseValue",","),",")>
	</cfif>
</cfif>

<!--- START: NYB 2012-01-20 Case#425042 moved to be after above if --->
<cfset reportColumnHeadingList = ReplaceList(reportShowTheseColumns,"ViewButton","Blank")>
<cfset reportShowTheseColumns = ReplaceList(reportShowTheseColumns,"Customer,Contact","OrganisationName,ContactName")>
<!--- END: NYB 2012-01-20 Case#425042 --->

 <!---
	we only want to list customers which have either live opportunities or assets (when a customer is listed we will always click thro to some detail)
	as there are complex filters used on the 'New Opporunities' and 'Sales History' (ie. Assets), to keep the filtering in one place only we call the same methods to get lists of relevant opportunities/assets and then output a row per customer based on the content of these 2 lists
  --->

	<cfset opportunitiesToOutput = false>		<!--- i use variable for these cos we may not have a recordset to test recordcount against --->
	<cfset assetsToOutput = false>
	<cfset useWholeWordsOnly = false>

	<cfset qryResellerOpportunities = application.com.opportunity.getCustomerControllerOpportunities(opportunityTypes=showOppTypes)>

	<!--- resellerOrgID needs to be dealt with for assets.... --->
	<cfset qryResellerAssets = application.com.asset.getAssets(resellerOrgID=request.relaycurrentuser.organisationID,includeRenewed=true)>

	<cfif qryResellerOpportunities.recordcount gt 0>
		<cfset oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",Alias="o")>
		<cfset oppRightsJoin = oppRightsQuerySnippet.join>
		<cfset oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

		<!--- this query defines a subquery in the FROM clause to determine the the opportunity with the lowest expected close date for each customer and then links back to that opportunity in the main query to get the rest of the information relating to the customer; the DISTINCT is necessary cos there may be 2+ opps with the same ExpectedCloseDate

			NJH 2016/04/27 Sugar 448757 - performance enhancements - changed in statement to join
		--->
		<cfquery name="getOppCustomersWithOpportunities" datasource="#application.siteDataSource#">
			SELECT DISTINCT org.OrganisationID, org.OrganisationName, Person.FirstName + ' ' + Person.LastName AS ContactName,
			o2.MinExpectedCloseDate AS NextExpectedCloseDate,
			(SELECT totalValue FROM vOppValue WHERE opportunityID = o.opportunityID) AS NextExpectedCloseValue,
			o.currency AS NextExpectedCloseCurrency,
			'phr_View' AS viewButton
			FROM  (SELECT entityID, MIN(expectedCloseDate) AS MinExpectedCloseDate FROM Opportunity WHERE (opportunityID IN (<cfoutput>#ValueList(qryResellerOpportunities.opportunityID)#</cfoutput>)) GROUP BY entityID) AS o2
			INNER JOIN Opportunity o ON o2.entityID=o.entityID AND o2.MinExpectedCloseDate = o.ExpectedCloseDate
			INNER JOIN organisation org ON org.OrganisationID = o2.entityid
			LEFT OUTER JOIN Person ON o.contactPersonID = Person.PersonID
			#preserveSingleQuotes(oppRightsJoin)#
			WHERE 1=1
			#preserveSingleQuotes(oppRightsWhereClause)#
		</cfquery>
		<cfif getOppCustomersWithOpportunities.recordcount gt 0>
			<cfset opportunitiesToOutput = true>
		</cfif>
	</cfif>

	<cfif qryResellerAssets.recordcount gt 0>							<!--- defence until the first asset (for any customer) is added --->
		<!--- get all the customers with assets (but not necessarily with any opportunities) --->
		<cfquery name="getOppCustomersWithAssets" datasource="#application.siteDataSource#">
			SELECT DISTINCT org.OrganisationID, org.OrganisationName, Person.FirstName + ' ' + Person.LastName AS ContactName,
			A2.MinEndDate AS NextExpectedCloseDate,
			A.Price AS NextExpectedCloseValue,
			A.currencyISOCode AS NextExpectedCloseCurrency,
			'phr_View' AS viewButton
			FROM  (SELECT entityID, MIN(endDate) AS MinEndDate FROM Asset WHERE (assetID IN (<cfoutput>#ValueList(qryResellerAssets.assetID)#</cfoutput>)) GROUP BY entityID) AS A2
			INNER JOIN Asset A ON A2.entityID=A.entityID AND A2.MinEndDate = A.EndDate
			INNER JOIN organisation AS org ON A2.entityID = org.OrganisationID
			LEFT OUTER JOIN Person ON A.contactPersonID = Person.PersonID
			WHERE 1=1
			and (a.partnerEntityID = #request.relaycurrentuser.organisationID#)	<!--- this is needed so that the link with the subquery doesn't reintroduce all opps for the entity --->
		</cfquery>
		<cfif getOppCustomersWithAssets.recordcount gt 0>
			<cfset assetsToOutput = true>
		</cfif>
	</cfif>

	<cfif opportunitiesToOutput OR assetsToOutput>
		<cfset inQoQ = true>
		<!--- get a DISTINCT list all the customers with opportunities OR assets --->
		<cfquery name="getOppCustomers" dbtype="query">
			<cfif opportunitiesToOutput>
				SELECT OrganisationName,ContactName,sum(NextExpectedCloseValue) as NextExpectedCloseValue,NextExpectedCloseDate,ViewButton,NextExpectedCloseCurrency,
					OrganisationID
				FROM getOppCustomersWithOpportunities
				where 1=1
					<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
				GROUP BY OrganisationName,ContactName,NextExpectedCloseDate,ViewButton,NextExpectedCloseCurrency,OrganisationID
			</cfif>
			<cfif opportunitiesToOutput AND assetsToOutput>
				UNION
			</cfif>
			<cfif assetsToOutput gt 0>
				SELECT OrganisationName,ContactName,sum(NextExpectedCloseValue) as NextExpectedCloseValue,NextExpectedCloseDate,ViewButton,NextExpectedCloseCurrency,
					OrganisationID
				FROM getOppCustomersWithAssets
				where 1=1
				<cfif opportunitiesToOutput>
					and organisationId NOT IN (#ValueList(getOppCustomersWithOpportunities.organisationID)#)
				</cfif>
				<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
				GROUP BY OrganisationName,ContactName,NextExpectedCloseDate,ViewButton,NextExpectedCloseCurrency,OrganisationID
			</cfif>
			ORDER BY <cf_queryObjectName value="#sortOrder#">
		</cfquery>
	<cfelse>
		<cfset getOppCustomers=QueryNew("OrganisationName,ContactName,NextExpectedCloseValue,NextExpectedCloseDate,ViewButton,NextExpectedCloseCurrency","varchar,varchar,date,date,bit,varchar")>
	</cfif>

	<cfoutput>
	<script>
		function selectOrg (params) {
			parent.location.href='/?eid=CustomerController&' + params
		}
	</script>
	</cfoutput>

	<cfif not ((opportunitiesToOutput OR assetsToOutput) and getOppCustomers.recordCount gt 0)>
		phr_opp_NoCustomersToList
	</cfif>


	<CF_tableFromQueryObject
		queryObject="#getOppCustomers#"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		openAsExcel = "false"
		numRowsPerPage="#numRowsPerPage#"
		totalTheseColumns=""
		GroupByColumns=""
		ColumnTranslation="true"
	    ColumnTranslationPrefix="phr_opp_"
		ColumnHeadingList="#reportColumnHeadingList#"
		showTheseColumns="#reportShowTheseColumns#"
		FilterSelectFieldList=""
		FilterSelectFieldList2=""
		hideTheseColumns=""
		numberFormat=""
		dateFormat="NextExpectedCloseDate"
		showCellColumnHeadings="no"
		keyColumnURLList=" "
		keyColumnOnClickList="selectOrg('##application.com.security.encryptQueryString('startingTab=2&orgID=##OrganisationID##')##')"
		keyColumnKeyList=""
		keyColumnList="ViewButton"
		keyColumnOpenInWindowList="no"
		OpenWinSettingsList=""
		useInclude = "false"
		allowColumnSorting = "true"
		currencyFormat="#reportCurrencyFormat#"
		KeyColumnLinkDisplayList="button"
		CurrencyISOColumn="NextExpectedCloseCurrency"
		showTextSearch="true"
		searchTextColumns="OrganisationName,ContactName,NextExpectedCloseValue,NextExpectedCloseDate"
		dateFilter="#listFindNoCase(reportShowTheseColumns,dateFilter)?dateFilter:''#"
	>

