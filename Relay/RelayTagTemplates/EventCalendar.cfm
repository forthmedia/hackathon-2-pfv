<!--- �Relayware. All Rights Reserved 2016 --->
<!---
Amendment History:
Date (DD-MMM-YYYY)  Initials    What was changed
10/02/2016  DAN PROD2015-289 - custom to core - bring widget from eFolder
25/02/2016  DAN PROD2015-289 - further fixes and improvements to calendar widgets
2016-03-02	WAB BF-226	Event Record Rights now stored against Eventdetail not Event entity
2016-10-11  DAN PROD2016-2500/Case 451953 - fix to highlight all of the event days in the calendar widget

--->

<cf_param name="eventwidget_targetPageId" type="string" default="events" label="Please provide text or numeric ID of the destination page" required="true"/>

<cfparam name="eventwidget_MonthNames" type="string" default="phr_sys_January,phr_sys_February,phr_sys_March,phr_sys_April,phr_sys_May,phr_sys_June,phr_sys_July,phr_sys_August,phr_sys_September,phr_sys_October,phr_sys_November,phr_sys_December" />
<!--- TODO: ideally we would also want to translate individual days but for now just keep it consistent with the rest of the portal --->
<cfparam name="eventwidget_DayNames" type="string" default="Su,Mo,Tu,We,Th,Fr,Sa" />
<cfparam name="eventwidget_StartDay" type="string" default="Su" />

<cf_head>
    <cfoutput>
        <cf_includecssonce template="/styles/EventWidget.css" includePortalcss="true">
        <cf_includejavascriptonce template="/javascript/EventWidget.js" translate="true" checkIfExists="true">
    </cfoutput>
</cf_head>

<cfif NOT StructKeyExists(request, 'eventWidgetID')>
	<cfset request.eventWidgetID = 0>
</cfif>

<cfset request.eventWidgetID++>

<CFQUERY NAME="GetEventDetailData" datasource="#application.sitedatasource#">
Select 	e.countryid,
	(select regStatus from EventFlagData WHERE flagid = e.flagID
		AND entityid = #request.relayCurrentUser.personID#) as regStatus,
	COUNT(efd.entityid) as NumberRegistered,
	eg.eventName,
	e.title,
	e.eventstatus,
	isnull(e.AllocatedStartDate, e.PreferredStartDate) as startDate,
    isnull(e.allocatedEndDate, e.PreferredEndDate) as endDate,
	e.location,
	e.venueRoom,
	e.SortOrder,
	f.flagtextid,
	e.EstAttendees as EstAttendees,
	ef.Sex,
	ef.fieldValue,
	ef.MinDateofBirth,
	ef.MaxDateofBirth,
	<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
	e.hideRegisterForEventBtn,
	e.hideUnRegisterForEventBtn
	<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
from eventDetail as e
inner join eventGroup eg on e.eventGroupID = eg.eventGroupID
inner join flag f on f.flagid = e.flagid
inner join eventCountry ec ON f.flagID = ec.flagID
left outer join eventFilters ef on e.FlagID = ef.FlagID
left outer join eventflagdata efd on efd.flagid = e.flagid and efd.regstatus IN('RegistrationSubmitted','Confirmed') 	<!--- 2013-01-15 PPB Case 433273 was regstatus  <> 'cancelled' --->
left join recordRights r on r.recordID = e.flagID and r.entity = 'EventDetail'
where 1=1
<CFIF IsDefined("EventGroupID") AND EventGroupID GT 0>
 AND e.eventGroupID =  <cf_queryparam value="#EventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFIF>
<!--- NJH 2008/09/23 elearning 8.1 - added moduleID as a possible filter --->
<CFIF IsDefined("moduleID") AND moduleID neq 0 and moduleID neq "">
 AND e.moduleID =  <cf_queryparam value="#moduleID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFIF>
and (r.userGroupID in (#request.relayCurrentUser.userGroups#) or r.recordID is null)
and ec.countryID =  <cf_queryparam value="#request.relayCurrentUser.content.showForCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
and e.flagid in (select ed.flagID
	from eventDetail ed
	INNER JOIN eventflagData efd ON ed.flagID = efd.flagID
	where efd.entityID = #request.relayCurrentUser.personID# <!--- #request.relayCurrentUser.personid#  --->
	and ed.invitationType = 'invOnly'
	UNION
	select ed.flagID
	from eventDetail ed
	where ed.invitationType = 'open')
<!---
	and (e.eventstatus = 'Agreed')
	WAB - removed this line and replaced with below
	This allows full events to show if the person is already entered  (confirmed or not)
	GCC - 2005/02/15 - Changed to hide initial events and correct a logic error
	--->
 and ((e.eventstatus = 'Agreed') or (e.flagid in (select distinct flagid from eventflagdata where regstatus in ('RegistrationStarted','RegistrationSubmitted','confirmed') and eventstatus <> 'initial' and entityid = #request.relayCurrentUser.personid#)))

and dateDiff(day, getDate(), isnull(e.AllocatedStartDate, e.PreferredStartDate)) >= 0
<CFIF IsDefined("sex")>
	and (ef.sex is null or ef.sex =  <cf_queryparam value="#sex#" CFSQLTYPE="CF_SQL_VARCHAR" > )
</CFIF>
<CFIF IsDefined("fieldValue")>
	and (ef.fieldValue is null or (ef.fieldValue =  <cf_queryparam value="#fieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
</CFIF>
<CFIF IsDefined("DateOfBirth")>
	and convert(datetime,'#DateOfBirth#',111) Between isnull(ef.maxDateOfBirth,'1766/10/14') and isnull(ef.minDateOfBirth,'2149/07/20')
</CFIF>
group by e.CountryID, e.FlagID, eg.EventName, e.title, e.EventStatus, e.AllocatedStartDate, e.PreferredStartDate, e.AllocatedEndDate, e.PreferredEndDate, e.Location,
                     e.VenueRoom, f.FlagTextID, e.EstAttendees, ef.Sex, ef.MinDateofBirth, ef.MaxDateofBirth, ef.fieldValue, e.SortOrder
<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
,e.hideRegisterForEventBtn
,e.hideUnRegisterForEventBtn
<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
order by  isnull(e.AllocatedStartDate, e.PreferredStartDate),eg.eventName, e.SortOrder
</CFQUERY>

<cfoutput>

<script>
var calID = #request.eventWidgetID#;
var targetPageId = '#eventwidget_targetPageId#';
var calClass = 'Big';
var dayNames = '#eventwidget_DayNames#';
var startDay = '#eventwidget_StartDay#';
var monthNames = '#eventwidget_MonthNames#';
var dayNumbers = '01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31';
var theDate = new Date();
var fromDate = new Date(theDate.getFullYear(), theDate.getMonth() - 1, (1-1));
var toDate = new Date(theDate.getFullYear(), (theDate.getMonth() + 6), (1-1));
var useFromDate = false;
var useToDate = false;
var showAllCells = true;
var dayFunctionName = 'ProcessThisDaysEvents';
var monthFunctionName = '';

jQuery(document).ready(function(){
	pureCalContainer(calID, calClass, theDate, dayNames, monthNames, dayNumbers, startDay, showAllCells, fromDate, toDate, useFromDate, useToDate, dayFunctionName);
});

var calData = #SerializeJSON(GetEventDetailData)#
var TheStartDateLoc = jQuery.inArray("STARTDATE", calData.COLUMNS)
var TheEndDateLoc = jQuery.inArray("ENDDATE", calData.COLUMNS)

function ProcessThisDaysEvents(TheDate, TheCell){
    var isEventDay = false;

    for (var i = 0; i < calData.DATA.length; i++) {
		var eventStartDate = new Date(calData.DATA[i][TheStartDateLoc]);
		var eventEndDate = new Date(calData.DATA[i][TheEndDateLoc]);
        
		var currentDate = new Date(TheDate);		
		if (currentDate <= eventEndDate && currentDate >= eventStartDate) {
		   isEventDay = true;
		}
    }

	if (isEventDay) {
		var EffectThisCell = jQuery('[CalCell="' + TheCell + '"]');
		var GrabDay = EffectThisCell.html();
		EffectThisCell.html('');
		var PassDate = new Date(TheDate);
		var divEventView = jQuery( '<div />' );
		var ThisLink = "window.open('/et.cfm?eid=" + targetPageId + "&EventYear=" + PassDate.getFullYear() + "&EventMonth=" + (PassDate.getMonth()+1) + "&EventDay=" + PassDate.getDate() + "')";
		jQuery(divEventView).addClass('DateHasEvent DateHasEvent' + calClass).html(GrabDay).attr("onclick", "" + ThisLink + "");
		EffectThisCell.append(divEventView)
	}
}
</script>

<div id="CalWidget#request.eventWidgetID#" class="CalWidget">#request.eventWidgetID#</div>
</cfoutput>
