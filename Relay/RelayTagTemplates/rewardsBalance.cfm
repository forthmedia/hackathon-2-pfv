<!--- �Relayware. All Rights Reserved 2014 --->
<cfsilent>
<!---  
relaytemplate for putting into an communication and getting the rewards balance

WAB 2005-02-09


is passed: 
request.commItem     is a strucure giving details of the current item in the communication
					fields include  
						.personid, 
						.organisationid, 
						.commid
						.emailType   HTML or TEXT
					
may return variables in a structure of form
request.rewardsBalance

make sure that cfsilent is used so that text cfmails work ok with 

SSS 2009-02-24   CRlex592 Loyalty Made so that an accounts data can be displayed with out a commItem structure existsing.
				As defualt the current users orgID and personID will be used if the structure commItem does not Exist.

--->
<CFPARAM name="AccountType"  default= "person">
<CFPARAM name="outputValue"  default = true>
<cfsetting enablecfoutputonly="yes">	
<cfscript>
	if (structKeyExists(request,"commItem")) {
		variables.rewardPersonID = Request.commItem.PersonID;
		variables.rewardOrganisationID = Request.commItem.OrganisationID;
	}else{
		variables.rewardPersonID = Request.RelayCurrentUser.PersonID;
		variables.rewardOrganisationID = request.RelayCurrentUser.OrganisationID;
	}
	endOfCurrentMonthBalanceDate = createdate(year(now()), month(now()), daysinmonth(now()));
	if (AccountType is "person") {
		qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,variables.rewardOrganisationID,variables.rewardPersonID);
	} else {
		qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,variables.rewardOrganisationID);
	}
</cfscript>
<cfset theBalance = NumberFormat(qPointsBalance.balance,"999999999")>
<cfif theBalance is "">
	<cfset theBalance = 0>
</cfif>
<cfset request.rewardsBalance.value = theBalance>
</cfsilent><cfif outputValue><cfoutput>#htmleditformat(theBalance)#</cfoutput></cfif>
<cfsetting enablecfoutputonly="no">	