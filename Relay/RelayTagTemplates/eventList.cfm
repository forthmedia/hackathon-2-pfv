<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			eventList.cfm
Author:				SWJ
Date started:

Description:		Displays list of events for current country / eventgroup which are
				a) eventDetails.eventStatus = agreed
				b) eventCountry.countryid = users countryID
				c) eventDetails.AllocatedStartDate/PreferredStartDate = in the future
				d) eventDetails.invitationType = open
				e) if eventDetails.invitationType = invOnly then the user needs to
					be in the eventFlagdata
		requires session.eventgroup to be set on first entry to the frame set
		requires frmcountryid to be set from previous page

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

20.10.04	ICS	- added sort order for events
Jan 05		JC	- added Teams functionality
2005-09-07	WAB	- changed a reference to #eid# to #elementid# .  eid isn't always defined, but elementid is always there when executing relay tags
2006-03-21   WAb removed references to cookie.partner session
2008/09/23	NJH	 - elearning 8.1 - added moduleID as a possible filter and included the stylesheet as the eventList was being opened in a pop-up.
			I also had to change some of the links that were pointing to an elementID as no elementID is defined if the page is run in a pop-up.

2008/10/24	NJH	Sophos RelayWare Set Up Bug Fix Issue 912 - Once a user has already registered again, then don't display links to register. It looks like there
			were supposed to be different states of registration, but it doesn't look like this is actually functional at the moment.
2009/09/23	NJH LID 2659
2012/05/18 	PPB Case 428214 if a user starts entering event questions and backs out don't sent an email and don't lock them out of the event
2012-11-19 	IH	Case 427101 allow users to register on the day of the event
2013-01-15 	PPB Case 433273 changed the statuses which are used to dictate whether a person has registered to an event
2013-01-25 	RMB Case 433471 moved cfoutput outside form
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2014-04-03 	PPB Case 439308 avoid multiple rows if the current user is a member of more than one of the usergroups that have visibility to the event
2015-06-12 AHL Case 444959 Event registration issue. UserGroup filter
10/02/2016  DAN PROD2015-289 - update to allow filtering by date passed via url from event calendar widget
2016/03/01	NJH	removed the custom editor and added eventGroupId and moduleID in here.
2016-03-02	WAB BF-226	Event Record Rights now stored against Eventdetail not Event entity
2016-07-18  DAN Case 450867 - fix GetEventDetailData query to deal with scenario when no user groups available
2016-11-10  DAN PROD2016-2500 - when event date specified (i.e. click from calendar widget) show all the events that covers the clicked date and are available to register for

Possible enhancements:


 --->
<!--- NJH 2008/09/23 relay elearning 8.1 - this relay tag is called on the portal in an openWin() so need to include the stylesheet.. --->
<!--- PJP 21/11/2012: CASE 432171: This tag can be called many times on one page meaning multiple hidden forms with the same name. Need to add in form name counter --->

<cf_param name="eventGroupID" label="EventGroup" default="0" display="eventName" value="eventGroupID" validvalues="select distinct ed.eventGroupID, eg.eventName from eventGroup eg with (noLock) inner join eventDetail ed on ed.eventGroupID = eg.eventGroupID order by eventName"/>
<cf_param name="moduleID" label="Module" default="0" display="title" value="moduleID" validvalues="select distinct ed.moduleID, 'phr_title_TrngModule_' + convert(VarChar,tm.moduleID) as title from trngModule tm with (noLock) inner join eventDetail ed on ed.moduleID = tm.moduleID order by title"/>

<cfparam name="request.eventlistcount" default="0">
<cfset request.eventlistcount = request.eventlistcount +1>

<cfset showThisPage = true>  <!--- set to false if we go off to some other page during processing --->
<cfset RegDoneCheckForProducts = false> <!--- this is set to true when the registration is complete and we need to check if a payment needs to be made
											needed a variable 'cause there are two routes to get to this point --->

<cf_include template="\code\cftemplates\eventINI.cfm" checkIfExists="true">

<cf_param name="showPageHeading" type="boolean" default="No"/>
<cf_param name="showPageWelcomeText" type="boolean" default="No"/>
<cf_param name="noMoreRegistrations" type="boolean" default="No"/>
<cf_param name="hideEventName" type="boolean" default="No"/>

<!--- This isn't needed if we generate a generic/default eventINI.cfm --->
<cf_param name="maxEventRegistrations" type="numeric" default="100000"/>

	<cfif structKeyExists(Form,"frmTableList")> <!--- This tells us that we are coming from a screen of questions --->
		<cf_ascreenupdate>
		<cfset frmRegStatus = "RegistrationSubmitted">
		<!--- Validate Team Fields if necessary --->

		<cfinclude template="/eventsPublic/mailConfirmation.cfm">	<!--- 2012/05/18 PPB Case 428214 now that we've filled in the additional questions, send the email --->

		<cfif IsDefined("teamname")>
			<!--- <cfinclude template="/code/cftemplates/EventTeamRegistration.cfm"> --->
			<cfinclude template="/eventsPublic/regUpdateTeams.cfm">
		<cfelse>
			<cfinclude template="/eventsPublic/regUpdate.cfm">
		</cfif>

			<cfset message="Phr_EV_ThankYouReg">
			<cfset RegDoneCheckForProducts = true>

	<CFELSEIF isDefined("frmRegister") and frmRegister eq "yes" and request.relaycurrentuser.isLoggedIn and not structKeyExists(request,"postOnceOnly")><!---PJP CASE: 431482: Only submit form once, stops duplicate emails --->

		<cfparam name="request.postOnceOnly" default="1"><!---PJP CASE: 431482: Only submit form once, stops duplicate emails --->

		<CFQUERY NAME="GetEventDetail" datasource="#application.sitedatasource#">
			select * from eventdetail where flagid  = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>

		<!--- are them some additional questions to ask?
		Need additional criteria here - screenID gt 0 both visits
		- 2nd visit (after the additional questions screen) should be regsubmitted
		--->
		<cfif GetEventDetail.registrationScreenid is not 0>
			<!--- if there are then, the regstatus --->
			<cfset frmRegStatus = "RegistrationStarted">
		<cfelse>
			<!--- this value is actually cfparam'ed in regupdate.cfm  but this may make it more readable--->
			<cfset frmRegStatus = "RegistrationSubmitted">
			<cfset message="Phr_EV_ThankYouReg">
			<cfset RegDoneCheckForProducts = true>
		</cfif>

		<cfset frmModuleID = GetEventDetail.moduleID>

		<!--- if a moduleID is passed in but is not the same moduleID as the event.. shouldn't ever happen, but trapping it if it does --->
		<cfif ((isDefined("moduleID") and moduleID NEQ 0) and moduleID neq frmModuleID)>
			Error in event registration. Please contact an administrator.
			<cfscript>
				errorStructure={Message="ModuleID (#moduleID#) and frmModuleID (#frmModuleID#) were not the same",moduleID=moduleID,frmModuleID=frmModuleID};
				errorID=application.com.errorHandler.recordRelayError_Warning(Severity="Error", message=errorStructure.message, WarningStructure=errorStructure);
			</cfscript>
				<cfoutput>ErrorID: #errorID#</cfoutput>
			<CF_ABORT>
		</cfif>


		<cfinclude template="/eventsPublic/regUpdate.cfm">

		<!--- 2005/03/24 - GCC - Changed to do the following: if you are going to another screen or you are going to a product then don't email,
		otherwise do.
		 --->
		<cfquery name="checkEventProducts" datasource="#application.sitedatasource#">
			SELECT     ProductID AS ProdID
			FROM         EventProducts
			WHERE     (FlagID = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
		</cfquery>
		<cfif GetEventDetail.registrationScreenid is 0 AND checkEventProducts.recordcount eq 0>		<!--- 2012/05/18 PPB Case 428214 changed OR to AND --->
			<cfinclude template="/eventsPublic/mailConfirmation.cfm">
		</cfif>

		<!--- If we are carrying on it is off to additional questions --->
		<cfif GetEventDetail.registrationScreenid is not 0 >

			<cfset frmcurrentscreenid = GetEventDetail.registrationScreenid>
			<cfset frmShowScreenAction	= "">  <!--- will submit back to this page --->
			<cfset frmParametersToHideOnForm = "EventFlagTextID">
			<cfinclude template="/screen/showscreennew.cfm">
			<cfset showThisPage= false>
		<cfelse>
			<cfset RegDoneCheckForProducts = true>

		</cfif>

	<!--- START: 2013-08-01	MS 	P-KAS026 Process Even UNREGISTER --->
	<CFELSEIF isDefined("frmRegister") and frmRegister eq "no" and frmUnRegister eq "yes" and request.relaycurrentuser.isLoggedIn and not structKeyExists(request,"postOnceOnly")><!---PJP CASE: 431482: Only submit form once, stops duplicate emails --->

		<cfparam name="request.postOnceOnly" default="1">

		<CFQUERY NAME="GetEventDetail" datasource="#application.sitedatasource#">
			select flagID from eventdetail where flagid  = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>

		<!--- Delete Entry of Person Registration from the EventFlagData table --->
		<CFQUERY NAME="qryUnRegisterFromEvent" datasource="#application.sitedatasource#">
			delete from EventFlagData where flagID =  <cf_queryparam value="#GetEventDetail.flagID#" CFSQLTYPE="cf_sql_integer" > and EntityID  =  <cf_queryparam value="#FORM.FRMPERSONID#" CFSQLTYPE="CF_SQL_Integer" >
		</cfquery>

	<!--- END: 	 2013-08-01	MS 	P-KAS026 Process Even UNREGISTER --->
	<CFELSEIF isDefined("frmRegister") and frmRegister eq "yes">

		<cfset message="Phr_EV_MustLogIn">

	</CFIF>



<!--- after registration need to check whether there is a product associated with the event
if so we need to go to the orders system
 --->
<cfif RegDoneCheckForProducts>

		<CFQUERY NAME="GetEventDetail" datasource="#application.sitedatasource#">
			select * from eventdetail where flagid  = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfquery>
		<!--- Not pretty but it gets the job done for now... --->

		<cfquery name="getProdIDForEvent" datasource="#application.sitedatasource#">
			SELECT     ProductID AS ProdID
			FROM         EventProducts
			WHERE     (FlagID = (select flagid from flag where flagtextid =  <cf_queryparam value="#eventflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
		</cfquery>

		<cfif getProdIDForEvent.recordcount is not 0 and fileexists("#application.paths.code#\cftemplates\EventProductFilter_#GetEventDetail.eventgroupID#.cfm")>
			<cfinclude template="/code/cftemplates/EventProductFilter_#GetEventDetail.eventgroupID#.cfm">
		</cfif>

		<cfif getProdIDForEvent.recordcount is not 0>

			<cfif not isdefined('goToEID')>
				<p><strong>Required:</strong> GoToEID is a mandatory parameter for this tag when used with subsequent pages which
	should be set in the element parameters.</p>
				<CF_ABORT>
			</cfif>

			<cfoutput>

				<script>
				location.href="/?eid=#jsStringFormat(goToEID)#&prodid=#jsStringFormat(valuelist(getProdIDForEvent.ProdID))#";
				</script>
			</cfoutput>
			<cfset showThisPage= false>
		<cfelse>
			<cfset message="Phr_EV_ThankYouReg">
		</cfif>


</cfif>


<cfif showThisPage>

	<!--- <cfquery name="getUsersCountry" datasource="#application.sitedatasource#">
		select p.personID, l.countryID from location l
		INNER JOIN person p on l.locationID=p.locationID
		WHERE personID = #request.relayCurrentUser.personid#
	</cfquery>

	<CFSET frmCountryid=getUsersCountry.countryid> --->
	<cfset frmCountryID = request.relayCurrentUser.countryID>

	<CFQUERY NAME="GetEventDetailData" datasource="#application.sitedatasource#">
	Select 	e.countryid,
		(select regStatus from EventFlagData WHERE flagid = e.flagID
			AND entityid = #request.relayCurrentUser.personID#) as regStatus,
		COUNT(efd.entityid) as NumberRegistered,
		eg.eventName,
		e.title,
		e.eventstatus,
		isnull(e.AllocatedStartDate, e.PreferredStartDate) as startDate,
		e.allocatedenddate,
		e.location,
		e.venueRoom,
		e.SortOrder,
		f.flagtextid,
		e.EstAttendees as EstAttendees,
		ef.Sex,
		ef.fieldValue,
		ef.MinDateofBirth,
		ef.MaxDateofBirth,
		<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
		<!--- START: 2014-02-26	Case 438996 - adding comment just to get only these lines moved across --->
		case when e.hideRegisterForEventBtn is null then 0 else e.hideRegisterForEventBtn end as hideRegisterForEventBtn,
		case when e.hideUnRegisterForEventBtn is null then 0 else e.hideUnRegisterForEventBtn end as hideUnRegisterForEventBtn
		<!--- END  : 2014-02-26	Case 438996 --->
		<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
	from eventDetail as e
	inner join eventGroup eg on e.eventGroupID = eg.eventGroupID
	inner join flag f on f.flagid = e.flagid
	inner join eventCountry ec ON f.flagID = ec.flagID
	left outer join eventFilters ef on e.FlagID = ef.FlagID
	left outer join eventflagdata efd on efd.flagid = e.flagid and efd.regstatus IN('RegistrationSubmitted','Confirmed') 	<!--- 2013-01-15 PPB Case 433273 was regstatus  <> 'cancelled' --->
	left join vRecordRights r on r.recordID = e.flagID and r.entity = 'Event' and r.personId = #request.relayCurrentUser.personID#			<!--- 2014-04-03 PPB Case 439308 replace with join to the view --->
	where 1=1
	<CFIF EventGroupID GT 0>
	 AND e.eventGroupID =  <cf_queryparam value="#EventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
	<!--- START 2015-06-12 AHL Case 444959 Event registration issue. UserGroup filter --->
    <cfif structKeyExists(request.relayCurrentUser, "userGroups") AND request.relayCurrentUser.userGroups NEQ "">
	AND (		EXISTS (SELECT rr.recordID FROM recordRights rr with(noLock) WHERE rr.entity = 'eventDetail' and rr.recordid = e.flagID AND rr.usergroupid in (#request.relayCurrentUser.userGroups#) )
			OR NOT EXISTS (SELECT rr.recordID FROM recordRights rr with(noLock) WHERE rr.entity like 'eventDetail' and rr.recordid = e.flagID)
		)
    <cfelse>
    AND NOT EXISTS (SELECT rr.recordID FROM recordRights rr with(noLock) WHERE rr.entity like 'eventDetail' and rr.recordid = e.flagID)
    </cfif>
	<!--- END 2015-06-12 AHL Case 444959 --->
	<!--- NJH 2008/09/23 elearning 8.1 - added moduleID as a possible filter --->
	<CFIF moduleID neq 0 and moduleID neq "">
	 AND e.moduleID =  <cf_queryparam value="#moduleID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
	and ec.countryID =  <cf_queryparam value="#frmcountryid#" CFSQLTYPE="CF_SQL_INTEGER" >
	and e.flagid in (select ed.flagID
		from eventDetail ed
		INNER JOIN eventflagData efd ON ed.flagID = efd.flagID
		where efd.entityID = #request.relayCurrentUser.personID# <!--- #request.relayCurrentUser.personid#  --->
		and ed.invitationType = 'invOnly'
		UNION
		select ed.flagID
		from eventDetail ed
		where ed.invitationType = 'open')
	<!---
		and (e.eventstatus = 'Agreed')
		WAB - removed this line and replaced with below
		This allows full events to show if the person is already entered  (confirmed or not)
		GCC - 2005/02/15 - Changed to hide initial events and correct a logic error
		--->
	 and ((e.eventstatus = 'Agreed') or (e.flagid in (select distinct flagid from eventflagdata where regstatus in ('RegistrationStarted','RegistrationSubmitted','confirmed') and eventstatus <> 'initial' and entityid = #request.relayCurrentUser.personid#)))
    <CFIF IsDefined("url.Event")>
        and isnull(e.AllocatedStartDate, e.PreferredStartDate) <= <cf_queryparam value="#url.Event#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
        and dateDiff(day, getDate(), isnull(e.AllocatedStartDate, e.PreferredStartDate)) >= 0
        and isnull(e.allocatedEndDate, e.PreferredEndDate) >= <cf_queryparam value="#url.Event#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
    <CFELSE>
        and dateDiff(day, getDate(), isnull(e.AllocatedStartDate, e.PreferredStartDate)) >= 0
    </CFIF>
	<CFIF IsDefined("sex")>
		and (ef.sex is null or ef.sex =  <cf_queryparam value="#sex#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</CFIF>
	<CFIF IsDefined("fieldValue")>
		and (ef.fieldValue is null or (ef.fieldValue =  <cf_queryparam value="#fieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" > ))
	</CFIF>
	<CFIF IsDefined("DateOfBirth")>
		and convert(datetime,'#DateOfBirth#',111) Between isnull(ef.maxDateOfBirth,'1766/10/14') and isnull(ef.minDateOfBirth,'2149/07/20')
	</CFIF>
	group by e.CountryID, e.FlagID, eg.EventName, e.title, e.EventStatus, e.AllocatedStartDate, e.PreferredStartDate, e.AllocatedEndDate, e.Location,
                      e.VenueRoom, f.FlagTextID, e.EstAttendees, ef.Sex, ef.MinDateofBirth, ef.MaxDateofBirth, ef.fieldValue, e.SortOrder
	<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
	,e.hideRegisterForEventBtn
	,e.hideUnRegisterForEventBtn
	<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
	order by  eg.eventName, e.SortOrder, isnull(e.AllocatedStartDate, e.PreferredStartDate)
	</CFQUERY>

	<cf_includejavascriptonce template = "/javascript/openwin.js">
	<cfoutput>
	<!---START PJP 21/11/2012: CASE 432171: --->
	<script LANGAUGE="Javascript">

		function eventRegister#request.eventlistcount#(eventFlagTextId) {
			form = document.registerForm#request.eventlistcount#;
			form.eventFlagTextId.value = eventFlagTextId;
			form.submit();
		}

		<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
		function eventUnRegister#request.eventlistcount#(eventFlagTextId) {
			form = document.registerForm#request.eventlistcount#;
			form.eventFlagTextId.value = eventFlagTextId;
			form.frmUnRegister.value = 'yes';
			form.frmRegister.value = 'no';
			form.submit();
		}
		<!--- END: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->

		jQuery(document).ready(function() {

			jQuery('.eventDetails').fancybox({
				autoSize: false,
				   helpers: {
			         overlay: {
			            locked: true
			         }
			      },
				type: "iframe",
				width: '90%',
				height: 'auto'
			});
		});
	</script>

	<!--- END PJP 21/11/2012: CASE 432171: --->
	</cfoutput>

	<cfif GetEventDetailData.Recordcount gt 0>
	<div class="EventDetailsListing row">
		<div class="EventDetailsListing col-xs-12 col-sm-12 col-md-12">
		  	<cfif showPageHeading eq "yes">
				<h1>phr_Current_Events</h1>
			</cfif>
			<cfif showPageWelcomeText eq "yes">
				<p>phr_Welcome_Events</p>
			</cfif>
			<cfif isDefined("message") and message neq "">
				<cfset application.com.relayUI.message(message=message)>
			</cfif>
			<cfif isdefined('maxEventRegistrations')>
				<CFQUERY NAME="GetEventsRegistered" datasource="#application.sitedatasource#">
				SELECT
					eventflagdata.FlagID
				FROM eventflagdata INNER JOIN
				EventDetail ON eventflagdata.FlagID = EventDetail.FlagID
				WHERE
					(eventflagdata.EntityID = #request.relayCurrentUser.personID#<!--- #request.relayCurrentUser.personid# --->)
					<CFIF EventGroupID GT 0>
			 			AND eventGroupID =  <cf_queryparam value="#EventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFIF>
					and (eventflagdata.regStatus in ('RegistrationStarted','RegistrationSubmitted','Confirmed'))
				</CFQUERY>

				<cfif GetEventsRegistered.Recordcount gt 0 and GetEventsRegistered.Recordcount gte maxEventRegistrations>
					<cfset noMoreRegistrations = "Yes">
				</cfif>
			</cfif>

			<!--- NJH 2008/09/25 elearning 8.1 this relay tag can be called as a popup - if so, then an eid is not defined. --->
			<cfif isDefined("elementID")>
				<cfset baseURL = "/?eid=#elementid#">
			<cfelse>
				<cfset baseURL = "/relayTagTemplates/eventList.cfm?">
			</cfif>

			<!--- pass a userModuleProgress ID if it's defined --->
			<cfif isDefined("userModuleProgressID")>
				<cfset baseURL = baseURL & "&userModuleProgressID=#userModuleProgressID#">
			</cfif>

			<cfoutput query="geteventdetaildata" group="eventName">
				<cfif hideEventName eq "no">
					<h1>#htmleditformat(eventName)#</h1>
				</cfif>
				<cfoutput>
				<div class="row"><!--- PJP 21/11/2012: CASE 432171:

				javascript:eventDetails#request.eventlistcount#('#flagTextID#')

				 --->

					<div class="eventDetailsLink col-xs-12 col-sm-2 col-md-2  col-lg-1"><a href="/eventsPublic/eventDetails.cfm?frmeventFlagTextID=#flagTextID#" id="ed_#request.eventlistcount#" class="eventDetails btn btn-primary">phr_Details</a></div><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
					<div class="EventDetailsDetails col-xs-12 col-sm-5 col-md-6 col-lg-7">#htmleditformat(title)#<BR>#htmleditformat(Location)#  <br>#htmleditformat(venueRoom)#</div>
					<div class=" col-xs-12 col-sm-2 col-md-2 col-lg-2"><CFIF startDate eq "">phr_DateTBC<cfelse>#dateformat(startDate,"DD-MMM-YY")#</CFIF></div>
					<div class="eventDetailsStatus col-xs-12 col-sm-3 col-md-2 col-lg-2">
						<CFIF startdate neq "" and regStatus eq "" and request.relaycurrentuser.isLoggedIn and EstAttendees neq "" and NumberRegistered lt EstAttendees>
							<cfif noMoreRegistrations eq "Yes">
								phr_EV_RegistrationUnavailable
							<cfelse><!--- PJP 21/11/2012: CASE 432171: --->
								<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
								<cfif not hideRegisterForEventBtn>
								<A HREF="javascript:eventRegister#request.eventlistcount#('#flagTextID#')" class="eventRegister btn btn-primary">phr_Register</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
							</cfif>
								<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
							</cfif>
						<CFELSEIF startdate neq "" and regStatus eq ""and EstAttendees neq "" and NumberRegistered lt EstAttendees>
							<cfif noMoreRegistrations eq "Yes">
								phr_EV_RegistrationUnavailable
							<cfelse><!--- PJP 21/11/2012: CASE 432171: --->
								<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
								<cfif not hideRegisterForEventBtn>
								<A HREF="javascript:eventRegister#request.eventlistcount#('#flagTextID#')" class="eventRegister btn btn-primary">phr_Register</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
							</cfif>
								<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
							</cfif>
						<CFELSEIF startdate neq "" and regStatus eq "To Be Invited" and EstAttendees neq "" and NumberRegistered lt EstAttendees>
							<cfif noMoreRegistrations eq "Yes">
								phr_EV_RegistrationUnavailable
							<cfelse><!--- PJP 21/11/2012: CASE 432171: --->
								<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
								<cfif not hideRegisterForEventBtn>
								<A HREF="javascript:eventRegister#request.eventlistcount#('#flagTextID#')" class="eventRegister btn btn-primary">phr_Register</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
							</cfif>
								<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
							</cfif>
						<CFELSEIF startdate neq "" and regStatus eq "Invited" and EstAttendees neq "" and NumberRegistered lt EstAttendees>
							<cfif noMoreRegistrations eq "Yes">
								phr_EV_RegistrationUnavailable
							<cfelse><!--- PJP 21/11/2012: CASE 432171: --->
								<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
								<cfif not hideRegisterForEventBtn>
								<A HREF="javascript:eventRegister#request.eventlistcount#('#flagTextID#')" class="eventRegister btn btn-primary">phr_Register</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
							</cfif>
								<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
							</cfif>
						<CFELSEIF startdate neq "" and regStatus eq "Cancelled" and EstAttendees neq "" and NumberRegistered lt EstAttendees>
							<cfif noMoreRegistrations eq "Yes">
								phr_EV_RegistrationUnavailable
							<cfelse><!--- PJP 21/11/2012: CASE 432171: --->
								<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
								<cfif not hideRegisterForEventBtn>
								<A HREF="javascript:eventRegister#request.eventlistcount#('#flagTextID#')" class="eventRegister btn btn-primary">phr_Register</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
							</cfif>
								<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
							</cfif>
						<cfELSEIF startdate neq "" and regStatus eq "Confirmed">
							<b>phr_EV_EventList_Confirmed</b>
						<!--- LID 2659 NJH 2009/09/23 - don't show fully booked if the person has been registered. --->
						<CFELSEIF startdate neq "" and EstAttendees neq "" and NumberRegistered gte EstAttendees and not listFindNoCase("RegistrationSubmitted,RegistrationStarted",regStatus)>
							phr_EV_FullyBooked
						<!--- Not paid yet - need to link out to payment here? --->
						<cfELSEIF startdate neq "" and regStatus eq "RegistrationSubmitted">
							<!--- NJH 2008/10/24 Sophos RelayWare Set Up Bug Fix Issue 912 - commented out the links for now, as there is no additional
							functionality other than emailing a confirmation. I'm guessing it was supposed to do something with payments???  --->
							<!--- <a href="#baseURL#&frmRegister=Yes&eventFlagTextID=#flagTextID#"> --->phr_EV_EventList_Registered<!--- </a> --->
						<!--- Not completed additional questions yet --->
						<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
						<cfif hideUnRegisterForEventBtn eq 1>
							phr_EV_EventList_Registered
						<cfelse>
							<A HREF="javascript:eventUnRegister#request.eventlistcount#('#flagTextID#')" class="eventUnRegister btn btn-primary">phr_UnRegister</A><!--- 438512 - 2014-02-10 - Missing Code - RMB - P-KAS026 - 2013-09-13 - Added CLSS TO "A HREF" for PORTAL styling --->
						</cfif>
						<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
						<cfELSEIF startdate neq "" and regStatus eq "RegistrationStarted">
							<!--- NJH 2008/10/24 Sophos RelayWare Set Up Bug Fix Issue 912 - commented out the links for now, as there is no additional
							functionality other than emailing a confirmation. I'm not sure what it should be doing....  --->
							<!--- 2012/05/18 PPB Case 428214 re-introduced the anchor around the phrase below so that if there is a screen of additional questions after the user clicked "register" and the user backed out of answering them they can get back in again to continue  --->
							<a href="#baseURL#&frmRegister=Yes&eventFlagTextID=#flagTextID#">phr_EV_EventList_RegistrationInProgress</a>

						</CFIF>
					</div>
				</div>
				</cfoutput>
			</CFOUTPUT>
	</div>
	</div>
	<!--- PJP 21/11/2012: CASE 432171: --->
	<cfoutput>	<!--- 2013-01-25 RMB Case 433471 moved cfoutput outside form --->
	<FORM name="registerForm#request.eventlistcount#" method="post">
			<CF_INPUT type="hidden" name="frmPersonID" value="#request.relayCurrentUser.personID#"> <!--- request.relayCurrentUser.personid --->
			<input type="hidden" name="frmRegister" value="yes">
			<input type="hidden" name="frmUnRegister" value="no"><!--- 2013-08-01	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			<!--- NJH 2008/09/25 relayElearning 8.1 called as a popup and not in an element --->
			<cfif isDefined("elementID")>
				<CF_INPUT type="hidden" name="eid" value="#elementID#">
			</cfif>
		<CFIF IsDefined("frmNext")>
			<CF_INPUT type="hidden" name="frmNext" value="#frmNext#">
		</CFIF>
			<input type="hidden" name="eventFlagTextId" value="null">

			<CF_INPUT type="hidden" name="EventGroupID" value="#EventGroupID#">

	<!--- Not generic - need to find a better way ofhandling these --->
		<CFIF IsDefined("fieldName")>
			<CF_INPUT type="hidden" name="fieldName" value="#fieldName#">
		</CFIF>
		<CFIF IsDefined("fieldValue")>
			<CF_INPUT type="hidden" name="fieldValue" value="#fieldValue#">
		</CFIF>
	</FORM>
	</cfoutput>		<!--- 2013-01-25 RMB Case 433471 moved cfoutput outside form --->

	<cfelse>
		phr_EV_NoAvailableEvents
	</cfif>
</cfif>
