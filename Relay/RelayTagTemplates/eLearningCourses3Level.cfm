﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		elearningCourses3Level.cfm
Author:			NJH
Date started:	2009-02-12 - bas

Description:	Based on Sophos eLearning Relay Tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-07-16			NYB			CR-SNY047 - added CourseSortOrder and ModuleSortOrder to param options
2009-07-23			NJH			P-FNL069 - encrypted url variables and accept only encrypted course and moduleID's.
2010-02-18			NAS			LID3099 add ',resizable=1' to make the popup resizable in ie.
2010-08-06			NJH			RW8.3 - moved call to elearningParams as it's not longer needed with new settings.
2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses
2010-09-22 			PPB 		P-LEN022  (LID 4052) use the local language/country file that is in the same fileFamily as the file associated with the module
2010-10-08			NAS			LID4300: Courses not launching correctly
2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive)
2011-02-25			NJH			LID 5753 - Moved the functionality to get the correct file in the file family into the getCourseLauncherURL function
2011-04-21			MS			P-LEN-30 CSAT (CR-031) Added hooks to implement custom star ratings, published date diaplay for course and modules
2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes
2011-09-06 			PPB 		LID7679: country scoping of modules
2011-09-16 			NYB 		P-SNY106
								Renamed from ElearningCoursesV2.cfm to ElearningCourses.cfm
2012-04-18			WAB			Renamed from ElearningCourses.cfm to ElearningCourses3Level.cfm to distinguish from  ElearningCourses.cfm/ElearningCourses2Level.cfm
2012-06-20 			PPB 		Case 428792 now fire off insertPersonModuleProgress from an onClick event (so it works for Akamai files and remove it from fileDisplay.cfm
2012-07-05 			STCR 		P-REL109 Phase 2 - pass personid to getCourseLauncherUrl(), so that userModuleProgress records get generated for CourseWare
2012-07-27			Englex		P-REL109 Phase 2 - Module Prerequisites
2012-12-06 			PPB 		Case 432403 moved cfinclude eLearningModuleDetailAppendage.cfm inside <cfif getModuleInfo.recordCount> because it relies on getModuleInfo.ModuleID existing
2013-04-10 			NYB 		Case 434164 added elearning.useModuleCountryScoping as default
2013-04-30			STCR		CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date
2013-05-28			STCR		CASE 435368 - do not return Courses before their Activation Date, unless previewing content for a specific date
2013-11-08 			PPB 		Case 437665 defend against fileid=0
2014-01-31			WAB 		removed references to et cfm, can always just use / or /?
2014-04-14			YMA			Case 435844 Make startquiz button able to be translatable
2014-07-23			PPB 		Case 441102 commented out a line of CF which was displaying on the portal which was attempting to display ratings widget(unnecessarily)
 --->

<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/><!--- question order --->
<cf_param name="ShowBackLinks" type="boolean" default="Yes"/>
<!--- START:  NYB 2009-07-16 CR-SNY047 - added: --->
<cf_param name="CourseSortOrder" type="string" default="publishedDate DESC,sortOrder,Title_Of_Course"/>
<cf_param name="ModuleSortOrder" type="string" default="sortOrder,Title_of_Module"/>
<!--- END:  2009-07-16 CR-SNY047 --->
<!--- START: 2011-09-16 NYB P-SNY106 - added params: --->
<cf_param NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#" validValues="Standard,Strict"/><!--- NYB 2011-09-15 P-SNY106 --->
<cf_param NAME="showIncorrectQuestionsOnCompletion" DEFAULT="No" type="boolean"/><!--- NYB 2011-09-15 P-SNY106 --->
<!--- END: 2011-09-16 NYB P-SNY106 --->
<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<cf_PARAM LABEL="Review answers link in quiz complete page" NAME="reviewAnswersLink" DEFAULT="false" TYPE="boolean"/>

<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
<cfparam name="available" type="Boolean" default="1">

<cf_param name="useModuleCountryScoping" default="#application.com.settings.getSetting('elearning.useModuleCountryScoping')#" type="boolean"/><!--- 2011-09-06 PPB LID7679 ---><!--- 2013-04-10 NYB Case434164 added default --->
<cf_param name="showModulesScopedToAllCountries" default="Yes" type="boolean"/>		<!--- 2011-09-06 PPB LID7679 done for lenovo so Japanese don't have to see non-Japanese All Countries modules --->

<!--- 2014-04-14	YMA	Case 435844 Make startquiz button able to be translatable --->
<cf_param label="Show quiz name in quiz link" name="showQuizName" default="yes" type="boolean"/>

<cf_param name="showThumbNail" type="Boolean" default="true"/>	<!--- will be false --->
<cfparam name="thumbNailWidth" type="numeric" default="90">

<!--- 2013-04-30 STCR CASE 432193 - ignore Modules before their Activation Date, unless previewing content for a specific date --->
<cfif isDefined("request.relaycurrentuser.content.date") and isDate(request.relaycurrentuser.content.date)>
	<cfset variables.searchFromActivationDate = request.relaycurrentuser.content.date />
<cfelse>
	<cfset variables.searchFromActivationDate = request.requestTime />
</cfif>

<!--- NJH 2009-07-23 P-FNL069 if the variables have come from the url scope, then check that they were encrypted --->
<cf_checkFieldEncryption fieldNames = "courseID,moduleID">

<cf_includeonce template="/eLearning/relaySCORM.cfm" />
<cf_includejavascriptonce template = "/javascript/ratings.js"><!--- 2014/10/29 GCC added for setBoxClass to work below --->
<cf_includeJavascriptOnce template="/elearning/js/elearning.js">

<script>
	setBoxClass('rate_content');
	<cfoutput>setEntityTypeID(#application.entitytypeid.trngModule#);</cfoutput>
	jQuery(document).ready(
    function()
    	{
        	// The DOM (document object model) is constructed
        	// We will initialize and run our plugin here
        jQuery('input.star').rating();
        //we add the following to stop jQuery from conflicting with prototype.js
		jQuery.noConflict();

    	}
    )

</script>

	<cfif structKeyExists(url,"courseID")>
		<cfset courseID = url.courseID>
		<cfif structKeyExists(url,"moduleid")>
			<!--- START: SHOW QUIZ & NOTES --->
			<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
			<!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date --->
			<cfset getModuleInfo = application.com.relayElearning.GetTrainingModulesData(courseID=courseID,ModuleID=ModuleID,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=ModuleSortOrder,availableModules=available,useModuleCountryScoping=useModuleCountryScoping,showModulesScopedToAllCountries=showModulesScopedToAllCountries,fromActivationDate=variables.searchFromActivationDate)>

			<div class="moduleOutline">
				<div class="outer">

				<cfif getModuleInfo.recordCount>
					<cfset courseTitle = getModuleInfo.Title_of_Course>
					<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
					<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
					<!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date --->
					<cfset qryTrainingModules = application.com.relayElearning.GetTrainingModulesData(moduleID=ModuleID,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=ModuleSortOrder,availableModules=available,useModuleCountryScoping=useModuleCountryScoping,showModulesScopedToAllCountries=showModulesScopedToAllCountries,fromActivationDate=variables.searchFromActivationDate)>
					<!--- START	2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes  --->
					<div class="backTop">
						<cfoutput>
							<!--- NJH 2009-07-23 P-FNL069 --->
							<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring="?eid=#url.eid#&courseID=#courseID#&CourseSortOrder=#CourseSortOrder#",remove=false)>

							<cfif ShowBackLinks><p class="back"><a href="/#encryptedUrlVariables#">phr_elearning_backToModules</a></p></cfif>
						</cfoutput>
					</div>
					<!--- STOP 2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes  --->
					<div class="inner last">
						<cfset imagePath = "/content/linkImages/trngModule/#moduleID#/">
						<cfset imageFileName = "Thumb_#moduleID#.png">

						<h2 class="innerlastH2">phr_eLearning_QuizCoureware_Heading</h2>
						<cfoutput>
							<h3 class="innerlastH1">#htmleditformat(courseTitle)#</h3>
							<div class="text">
							<cfif not FileExists("#ExpandPath(imagePath)##imageFileName#")>
								<cfset imagePath = "/images/MISC/">
								<cfset imageFileName = "spacer.gif">
							</cfif>
							<cfif showThumbNail>
								<img class="pull-left" src="#imagePath##imageFileName#" width="#thumbNailWidth#">
							</cfif>
							<!--- START 2011-04-21	MS	CSAT (CR-31) INSERT CUSTOMISATION FOR LENOVO --->
							<cfif fileexists("#application.paths.code#\cftemplates\eLearningModuleDetailHeading.cfm")>
								<cfinclude template="/code/cftemplates/eLearningModuleDetailHeading.cfm">
							<CFELSE>
								<h3 class="innerlastH2">#htmleditformat(getModuleInfo.title_of_module)#</h3>
							</CFIF>
							<!--- END 2011-04-21	MS	CSAT (CR-31) --->
							<p><B>phr_eLearning_DatePublished</B>
							<CFIF getModuleInfo.publishedDate eq "">
								phr_eLearning_noPubDt
							<CFELSE>
								#LSDateFormat(getModuleInfo.publishedDate, "mmm dd, yyyy")#
							</CFIF>
							</p>
							<p>phr_description_trngModule_#htmleditformat(moduleID)#</p>
							</cfoutput>
						<ul class="list-inline links">

						<cfif val(qryTrainingModules.fileID) neq 0 or qryTrainingModules.FileName neq "">	<!--- 2013-11-08 PPB Case 437665 changed condition use val() to defend against fileid=0  --->

							<!--- LID 5753 NJH 2011-02-24 - moved this code into getCourseLauncherUrl... --->
							<!--- 2010-09-22 PPB P-LEN022 get the local language/country file and if it exists launch it directly cos we don't need to use the CourseLauncher --->
							<!--- <cfset localVersionFileID = application.com.relayElearning.getLocalVersionFileID(qryTrainingModules.fileID)>
							<cfif localVersionFileID neq 0>
								<!--- START 2010-10-08			NAS			LID4300: Courses not launching correctly --->
								<cfset fileId = localVersionFileID>
								<cfset getModuleDetails = application.com.relayElearning.getModuleDetailsQry(moduleID=moduleID)>
								<cfset filePath="/content/#getModuleDetails.path#">
								<cfset courseLauncherUrl=application.com.security.encryptURL(url = "/fileManagement/fileDisplay.cfm??filePath=#filePath#/#fileID#/#getModuleDetails.AICCFilename#&fileName=#getModuleDetails.AICCFilename#", dontRemoveRegExp="filename")>
								<!--- STOP 2010-10-08			NAS			LID4300: Courses not launching correctly --->
							<cfelse> --->

							<cfset courseLauncherUrl = application.com.relayElearning.getCourseLauncherUrl(moduleID=ModuleID,personID=request.relaycurrentuser.personid)><!--- 2012-07-05 P-REL109 Phase 2 STCR included personid --->
							<cfset fileId = qryTrainingModules.fileID>

							<!--- </cfif> --->
							<cfif courseLauncherUrl neq "">
								<cf_includeJavascriptOnce template="/javascript/openWin.js">
								<cfoutput>
									<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID="Courseware#fileID#_Description")>
										<cfset courseDescription = "phr_Courseware#fileID#_Description">
									<cfelse>
										<cfset courseDescription = " "/>
									</cfif>

									<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID="Courseware#fileID#_Title")>
										<cfset courseTitle = "phr_Courseware#fileID#_Title">
									<cfelse>
										<cfset courseTitle = "phr_downloadTheNotes"/>
									</cfif>
									<!--- 2010-02-18			NAS			LID3099 add ',resizable=1' to make the popup resizable in ie. --->
									<!--- 2012-06-20 PPB Case 428792 now fire off insertPersonModuleProgress from an onClick event and remove it from fileDisplay.cfm --->
									<li class="downloadNotes"><a class="btn btn-primary" href="javascript:void(openWin('#courseLauncherUrl#','CoursewareContent#ModuleID#','width=#application.com.settings.getSetting('files.maxPopupWidth')#,height=#application.com.settings.getSetting('files.maxPopupHeight')#,scrollbars=no,status=no,address=no,resizable=1'));" onClick="upsertUserModuleProgress(moduleID=#moduleID#,personId=#request.relayCurrentUser.personId#)" align="left">#htmleditformat(courseTitle)#</a>#htmleditformat(courseDescription)#</li>
								</cfoutput>
							<cfelse>
								<li>phr_elearning_CourseContentFileDoesNotExist.</li>
							</cfif>
						</cfif>

						<cfset qryGetQuizzes = application.com.relayQuiz.getQuizzes(moduleID=ModuleID)>
						<cfset getModuleScreens = application.com.relayElearning.getModuleScreens(moduleID=ModuleID)>
						<cfset getModuleProgressScreens = application.com.relayElearning.getTrngUserModuleProgress(ModuleID=ModuleID,PersonID=frmPersonID)>

						<cfset refresh = 0>
						<cfif getModuleScreens.recordcount gt 0 and getModuleProgressScreens.recordcount eq 0>
							<cfset refresh = 1>
						</cfif>

						<cfif qryGetQuizzes.recordcount gt 0>
							<cfoutput>

							<cf_modalDialog type="iframe" identifier=".takeQuiz a" size="large">
							<cf_includejavascriptonce template = "/javascript/openwin.js">
							<script>
								function refresh() {
									window.location.reload(false);
								}
								function startQuiz(encryptedLink) {
									if (#jsStringFormat(refresh)# == 1) {
										setTimeout("refresh()",2*1000);
									}
									openWin(encryptedLink,'Quiz','width=600,height=750,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
								}

								<!---
								2012-06-20 PPB Case 428792 now fire off insertPersonModuleProgress from an onClick event and remove it from fileDisplay.cfm
								despite the function name upsert... this currently just inserts
								 --->
								function upsertUserModuleProgress(moduleId,personId) {
									var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=true'
									var page = page + '&webservicename=relayElearningWS&methodname=insertUserModuleProgress'
									var parameters = {moduleId:moduleId,personId:personId}

									var myAjax = new Ajax.Request(
										                    page,
										                    {
										                        method: 'post',
										                        parameters: parameters
										                    });
									<!--- NJH 2012/11/21 Case 432093 --->
									refreshModuleStatus(moduleID,'started');
								}
							</script>
							</cfoutput>
							<cfoutput query="qryGetQuizzes">
								<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID="Quiz#QuizDetailID#_Description")>
									<cfset translatedPhr = "phr_Quiz#QuizDetailID#_Description">
								<cfelse>
									<cfset translatedPhr = " ">
								</cfif>
								<!--- 2014-04-14	YMA	Case 435844 Make startquiz button able to be translatable --->
								<cfif showQuizName>
									<cfset nameToDisplay = QuizName>
								<cfelse>
									<cfset nameToDisplay = "phr_takeTheQuiz">
								</cfif>
								<!--- START: 2011-09-16 NYB P-SNY106 - added encryptURL call then use the encryptedLink instead in the js call --->
								<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
								<cfset encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
								<li class="takeQuiz"><a class="btn btn-primary" href="#encryptedLink#">#htmleditformat(nameToDisplay)#</a><br/>#htmleditformat(translatedPhr)#</li>
								<!--- END: 2011-09-16 NYB P-SNY106 --->
							</cfoutput>
						</cfif>
						</ul>

						<!--- START: 2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR031 CSAT (Ratings) --->
						<cfif application.com.settings.getSetting("elearning.useModuleRating")>
							<cf_includejavascriptonce template = "/javascript/lib/jquery/jquery.rating.js">
							<cf_includejavascriptonce template = "/javascript/ratings.js">

							<!--- <cf_htmlhead text = "<link href='/javascript/css/jquery.rating.css' rel='stylesheet' media='screen,print' type='text/css'>"> --->
							<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery.rating.css">

							<cfset getRatings = application.com.relayRating.getRatings(entityID=getModuleInfo.moduleID,entitytypeID=application.entitytypeid.trngModule) />

							<cfoutput>
								#application.com.relayRating.getRatingsWidget(entityID=getModuleInfo.moduleID,entityTypeID=application.entityTypeID.trngModule)#
							</cfoutput>
						</cfif>
						<!--- END: 2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR031 CSAT (Ratings) --->
						</div>
						<div class="innerBottom"></div>
				    </div>

				<!--- 2012-12-06 PPB Case 432403 moved this include inside the CFIF because it relies on getModuleInfo.ModuleID existing --->
				<!--- START 2011-04-21	MS	CSAT (CR-31) INSERT CUSTOMISATION FOR LENOVO --->
					<cfif fileexists("#application.paths.code#\cftemplates\eLearningModuleDetailAppendage.cfm")>
						<cfinclude template="/code/cftemplates/eLearningModuleDetailAppendage.cfm">
					</CFIF>
				<!--- END 2011-04-21	MS	CSAT (CR-31) --->

				<cfelse>
					phr_elearning_InsufficientRightsToViewModule.
				</cfif>


			<cfoutput>
						<div class="outerBottom"></div>
					</div>
					<!--- NJH 2009-07-23 P-FNL069 --->
					<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring="?eid=#url.eid#&courseID=#courseID#&CourseSortOrder=#CourseSortOrder#",remove=false)>

					<cfif ShowBackLinks><p class="back"><a href="/#encryptedUrlVariables#">phr_elearning_backToModules</a></p></cfif>
				</div>
			</cfoutput>
			<!--- END: SHOW QUIZ & NOTES --->

		<cfelse>
			<!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date --->
			<cfset getModuleInfo = application.com.relayElearning.GetTrainingModulesData(courseID=courseID,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=ModuleSortOrder,availableModules=available,useModuleCountryScoping=useModuleCountryScoping,showModulesScopedToAllCountries=showModulesScopedToAllCountries,fromActivationDate=variables.searchFromActivationDate)>
			<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
			<cfset getCourseData = application.com.relayElearning.GetTrainingCoursesData(courseID=courseID,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=CourseSortOrder,useModuleCountryScoping=useModuleCountryScoping,showModulesScopedToAllCountries=showModulesScopedToAllCountries,fromActivationDate=variables.searchFromActivationDate)>

			<!--- START		MS		2011-04-18	CSAT(CR-31) Add a hook to display custom content for Lenovo --->
			<cfif fileexists("#application.paths.code#\cftemplates\eLearningModuleTitles.cfm")>
				<cfinclude template="/code/cftemplates/eLearningModuleTitles.cfm">
			<CFELSE><!--- SHOW Non-Custom Content if Cutom Lenovo Content is not available --->
			<div class="modules">
				<!--- START	2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes  --->
				<div class="backTop">
					<cfoutput>
						<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring = "?eid=#url.eid#&CourseSortOrder=#CourseSortOrder#",  removed=false)>
						<cfif ShowBackLinks><p class="back"><a href="/#encryptedUrlVariables#">phr_elearning_backToCourses</a></p></cfif>
					</cfoutput>
				</div>
				<!--- STOP	2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes  --->
				<h2 class="modulesH2">phr_eLearning_Modules_Heading</h2>
				<cfif getCourseData.recordcount GT 0><cfoutput><h3 class="modulesH3">#htmleditformat(getCourseData.Title_of_Course)#</h3></cfoutput></cfif>
				<div class="outer">

				<cfif getCourseData.recordCount gt 0>
					<cfset courseTitle = getCourseData.Title_of_Course>
					<cfset courseDescription = getCourseData.Description>

					<cfif getModuleInfo.recordcount gt 0>
						<cfoutput query="getModuleInfo">
							<cfset moduleDivClass = "inner" />
							<cfif currentRow eq getModuleInfo.recordCount>
								<cfset moduleDivClass="inner last">
							</cfif>
							<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring = "?eid=#url.eid#&courseID=#courseID#&moduleId=#moduleID#&CourseSortOrder=#CourseSortOrder#&ModuleSortOrder=#ModuleSortOrder#", remove=false)>

							<cfset imagePath = "/content/linkImages/trngModule/#moduleID#/">
							<cfset imageFileName = "Thumb_#moduleID#.png">

							<cfif not FileExists("#ExpandPath(imagePath)##imageFileName#")>
								<cfset imagePath = "/images/MISC/">
								<cfset imageFileName = "spacer.gif">
							</cfif>

							<!--- NJH 2012/11/21 Case 432093 - added ID on moduleDiv --->
							<div class="#moduleDivClass#" id="module#moduleID#div">

								<cfif showThumbNail>
									<img class="pull-left" src="#imagePath##imageFileName#" width="#thumbNailWidth#">
								</cfif>
								<h3><a href="/#encryptedUrlVariables#">phr_title_trngModule_#htmleditformat(moduleID)#</a></h3>
								<p>phr_description_trngModule_#htmleditformat(moduleID)#</p>
								<!--- <cfif not disabled> ---><p class="action"><a href="/#encryptedUrlVariables#">phr_elearning_accessModule</a></p><!--- </cfif> --->
								<div class="innerBottom"></div>
	    					</div>
						</cfoutput>
					<cfelse>
						phr_eLearning_NoModulesAvailable
					</cfif>
				<cfelse>
					phr_elearning_InsufficientRightsToViewCourse.
				</cfif>
			</CFIF><!--- End 	MS		2011-04-18		CSAT(CR-31) --->
					<div class="outerBottom"></div>
				</div>

			<!--- END: SHOW MODULES --->
			<cfoutput>
					<!--- NJH 2009-07-23 P-FNL069  --->
					<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring = "?eid=#url.eid#&CourseSortOrder=#CourseSortOrder#",  removed=false)>

					<cfif ShowBackLinks><p class="back"><a href="/#encryptedUrlVariables#">phr_elearning_backToCourses</a></p></cfif>
				</div>
			</cfoutput>
		</cfif>

	<cfelse>
		<!--- START: SHOW COURSES --->
		<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
		<!--- SKP 2010-09-01 LEN002 - add course filtering --->
		<cfparam name="solutionArea" default="">
		<cfparam name="userLevel" default="">
		<cfparam name="series" default="">

		<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses --->
		<cfparam name="active" default="1">
			<!--- START 2011-04-21	MS	CSAT (CR-31) Course List with Averages Sortable --->
			<cfif fileexists("#application.paths.code#\cftemplates\eLearningCourseTitles.cfm")>
				<cfinclude template="/code/cftemplates/eLearningCourseTitles.cfm">
			<CFELSE>
			<!--- If custom file does not exist show standard content --->
				<cfset coursesQry = application.com.relayElearning.GetTrainingCoursesData(solutionArea=solutionArea,series=series,userLevel=userLevel,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=CourseSortOrder,active=active,useModuleCountryScoping=useModuleCountryScoping,showModulesScopedToAllCountries=showModulesScopedToAllCountries,fromActivationDate=variables.searchFromActivationDate)>

				<div class="courses">
					<h2 class="coursesH2">phr_elearning_courses_heading</h2>
					<div class="outer">
					<cfif coursesQry.recordCount>

						<cfoutput query="coursesQry">
							<cfset courseDivClass="inner">
							<cfif currentRow eq coursesQry.recordCount>
								<cfset courseDivClass="inner last">
							</cfif>
							<cfset imagePath = "/content/linkImages/trngCourse/#courseID#/">
							<cfset imageFileName = "Thumb_#courseID#.png">

							<cfif not FileExists("#ExpandPath(imagePath)##imageFileName#")>
								<cfset imagePath = "/images/MISC/">
								<cfset imageFileName = "spacer.gif">
							</cfif>
							<!--- NJH 2009-07-23 P-FNL069  --->
							<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?eid=#url.eid#&courseID=#courseID#&CourseSortOrder=#CourseSortOrder#")>
							<div class="#courseDivClass#">
								<cfif showThumbNail>
									<img class="pull-left" src="#imagePath##imageFileName#" width="#thumbNailWidth#">
								</cfif>
								<h3><a href="/#encryptedUrlVariables#">#htmleditformat(Title_of_Course)#</a></h3>
								<p>#htmleditformat(Description)# </p>
								<p class="action"><a href="/#encryptedUrlVariables#">phr_elearning_accessCourse</a></p>
								<div class="innerBottom"></div>
							</div>
						</cfoutput>
					<cfelse>
						phr_elearning_ThereAreNoCoursesAvailable.
					</cfif>
					<div class="outerBottom"></div>
				</div>
			</CFIF><!--- END: 2011-04-21	MS	CSAT (CR-31) Course List with Averages Sortable --->

		<!--- END: SHOW COURSES --->
	</cfif>
