<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			CJMTemplates.cfm
Author:				YMA
Date started:		2014-12-08

Description:		web service calls for CJM.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2015-11-10          ACPK        PROD2015-24 Added loadEmailForm function
2015-11-30          ACPK        PROD2015-463 Refresh current tab after closing template to show name/description change; removed unneeded code; improved error message
2016-08-01          DAN         Case 450985 - add option to hide email button on the customized templates tab

Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->
<cf_param label="Choose the first tab to display" name="starttab" default="AvailableTemplates" validvalues="AvailableTemplates,CustomizedTemplates"/>
<cf_param label="Include template description?" name="showDescription" default="true" type="boolean"/>
<cf_param label="Limit to certain template types?" name="templateTypeIDs" default="" nullText="phr_Ext_SelectAValue" showNull="true" validvalues="select cjmtemplateType as displayvalue, cjmtemplatetypeID as datavalue from cjmtemplatetype"/>
<cf_param label="Available template display sort order" name="sortOrder" default="lastUpdated" validvalues="cjmTitle,cjmDescription,lastUpdated,CJMTemplateType"/>
<cf_param label="Allow to email customized templates?" name="showEmailButton" default="true" type="boolean"/>
<cfparam name="includeCustomSave" default="false" type="boolean"/>

<!--- Tab Navigation --->
<ul class="nav nav-tabs" role="tablist">
	<li<cfif starttab eq "AvailableTemplates"> class="active"</cfif>><a href="#AvailableTemplates" role="tab" data-toggle="tab">phr_CJM_AvailableTemplates</a></li>
	<li<cfif starttab eq "CustomizedTemplates"> class="active"</cfif>><a href="#CustomizedTemplates" role="tab" data-toggle="tab">phr_CJM_CustomizedTemplates</a></li>
</ul>

<!--- Tab panes --->
<div class="tab-content CJMTemplateTabs">
		<div class="tab-pane<cfif starttab eq "AvailableTemplates"> active</cfif>" id="AvailableTemplates">
			<img src="/images/icons/loading.gif">
		</div>
		<div class="tab-pane<cfif starttab eq "CustomizedTemplates"> active</cfif>" id="CustomizedTemplates">
			<img src="/images/icons/loading.gif">
		</div>
</div>

<script>
	jQuery('.nav-tabs a').click(function (e) {
		e.preventDefault()
		var targetFrame = jQuery(this).attr("href")
		var target = targetFrame.substr(1);

		loadTab(target,targetFrame)
	})
	<cfoutput>
		jQuery(document).ready(function(){
			loadTab('#starttab#','###starttab#')
		});
	</cfoutput>

	//checks to see if browser is IE8 or below
	function isNotIE8OrBelow() {
		if (window.navigator.userAgent.indexOf("MSIE") > 0 && !document.addEventListener) { //NB: addEventListener not supported by IE until IE9+
			return false;
		} else {
			return true;
		}
	}
	//checks to see if device screen too small
	function isNotDesktop() {
		var minDesktopWidth = 992; //bootstrap defines width of desktop as 992px or greater
		if (screen.width < minDesktopWidth) {
			return false;
		} else {
			return true;
		}
	}

	loadTab = function (target,targetFrame) {
	    <!--- load the templateDesigner --->
	    // postURL is the webservice that is being called
	    var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=plain&_cf_nodebug=false';
	    // extend postURL with relevant parameters that describe the webservice
	    postURL += '&webservicename=relayCJMWS&methodname=get' + target;
	    <!--- CHECK IF BROWSER/DEVICE IS SUPPORTED --->
	    var browserSupported = isNotIE8OrBelow();
	    var deviceSupported = isNotDesktop();
	    postURL += '&browserSupported=' + browserSupported + '&deviceSupported=' + deviceSupported;
	    <cfoutput>postURL += '&showEmailButton=#showEmailButton#'</cfoutput>
		<cfif templateTypeIDs neq "">
			<cfoutput>postURL += '&templateTypeIDs=#templateTypeIDs#&sortOrder=#sortOrder#'</cfoutput>
		</cfif>
	    // call the jQuery Ajax to process the webservice
	    var result = jQuery.ajax({
           url: postURL,
           type:'get'

	    }).done(function(data){
           jQuery(targetFrame).html(data);
           jQuery('#'+target).trigger('create');

	    }).fail(function(data){
	       console.log("Error loading " + target + " tab");
	    });
     }

	<!--- NJH 2016/01/29 - this shoudl be using cf_modalDialog! --->
	loadEditor = function (argArray) {
		<cfoutput>var href='/cobranding/cjmTemplateEditor.cfm?includeCustomSave=#includeCustomSave#';</cfoutput>
		if(typeof argArray.cjmTemplateID !== 'undefined'){
			href += '&cjmTemplateID=' + argArray.cjmTemplateID;
		}
		if(typeof argArray.cjmCustomizedTemplateID !== 'undefined'){
			href += '&cjmCustomizedTemplateID=' + argArray.cjmCustomizedTemplateID;
		}

		jQuery.fancybox.open({
		      helpers: {
		         overlay: {
		            locked: true
		         }
		      },
			autoSize: false,
			width: '85%',
			height: 'auto',
			closeEffect : 'none',
			closeSpeed: 'fast',
			type: "iframe",
			href: href,
			afterLoad: function(links, index){
				iframeWindow = jQuery(links.wrap[0]).find('iframe')[0].contentWindow
				if(iframeWindow.canvas !== undefined){
					iframeWindow.adjustZoom(iframeWindow.canvasWidth)
				}

			},
			<!--- 2015-11-30     ACPK    PROD2015-463 Refresh current tab after closing template to show name/description change --->
			afterClose: function(){
				var targetFrame = jQuery("ul.nav-tabs > li.active > a").attr("href")
                var target = targetFrame.substr(1);
				loadTab(target,targetFrame);
			}
		});
	}

    <!--- 2015-11-10  ACPK    PROD2015-24 Opens the Email Template form in a fancybox, for sending a customized template (see relayCJMWS.cfc)
		NJH 2016/01/29 - this shoudl be using cf_modalDialog!
	 --->
	loadEmailForm = function (cjmCustomizedTemplateID) {
		var href='/cobranding/CJMTemplateEmailForm.cfm?&cjmCustomizedTemplateID=' + cjmCustomizedTemplateID;

		jQuery.fancybox.open({
		      helpers: {
		         overlay: {
		            locked: true
		         }
		      },
			autoSize: false,
			width: '85%',
			height: 'auto',
			closeEffect : 'none',
			closeSpeed: 'fast',
			type: "iframe",
			href: href
		});
	}
</script>