<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		mailBox.cfm
Author:			NJH
Date started:	2013/01/25

Description:	used for My Mailbox on the portal

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

 --->

<cf_param name="showTheseColumns" type="string" multiple="true" displayAs="twoSelects" validvalues="Title,TypeText,SendDate,StatusText" default="Title,TypeText,SendDate,StatusText" allowSort="true"/>
<cf_param name="numRowsPerPage" type="numeric" default="6"/>
<cfparam name="sortOrder" type="string" default="DateSent desc">
<cfparam name="startRow" type="numeric" default="1">

<cfset qryComms = application.com.communications.getCommsSentForPerson(personId=request.relayCurrentUser.personId,commTypeIDs=2,sortOrder=sortOrder)>

<cfif qryComms.recordcount gt 0>
	<cfoutput>
		<script>
			jQuery(document).ready(function(){
				jQuery(".smallLink").fancybox({
				   helpers: {
			         overlay: {
			            locked: true
			         }
			      },
					autoSize: false,
					width: 'auto',
					height: 'auto',
					type: 'ajax'
				});
			});
		</script>
	</cfoutput>

	<CF_tableFromQueryObject
		queryObject="#qryComms#"
		queryName="qryComms"
		showTheseColumns="#showTheseColumns#"
		dateformat="SendDate"
		useInclude="false"
		keyColumnList="Title"
		keyColumnOnClickList = " "
		keyColumnURLList="/communicate/viewCommunication.cfm?personID=#request.relayCurrentUser.personID#&commId="
		keyColumnKeyList="commID"
		sortOrder="#sortOrder#"
		allowColumnSorting="yes"
		numRowsPerPage = "#numRowsPerPage#"
		columnTranslation="true"
		columnTranslationPrefix="phr_comm_"
		keyColumnTargetList="_blank"
		startRow="#startRow#"
		id="mailBox"
	>

<cfelse>
	<div align="center">phr_comm_noCommunicationsHaveBeenSent</div>
</cfif>