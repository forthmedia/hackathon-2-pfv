<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		externalLink.cfm
Author:			SWJ
Date created:	2000-07-12

	Objective - to track clickThrus
		
	Syntax	  -	example in Javascript newWindow = window.open ('/newsletter/externalLink.cfm?entity=1&entityid=' + elementid + '&personid=' + form.frmPersonId.value + '&URL=' + escape(URL), 'PopUp')
				externalLink.cfm?eid=1&etype=file&pid=<<<pid>>>&goToURL=www.microsoft.com
	Parameters - eType: table name of the entity you are tracking 
				eID: the id of the entity
				pID: the id of the person clicking thru
				goToURL: the fully qualified URL
				
	Return Codes - none 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2007/11/13 deal with https
2011-02-02 	NYB 	LHID5321 added entityTrackingToLocLength to limit size of URL query was trying to insert into the entityTracking table to no larger than the table will allow. 
2012-09-04	PJP  	Case 430418
2012-01-29 	WAB 	CASE 433406 remove htmlEditFormating the gotoURL - accidentally added by CASE 430318 (when a body onload became a javascript redirect), in particular makes ampersands go wrong.  Also possibly needs change in mergeFunctions.trackedLink() 

Enhancement still to do:

--->

<!--- used to add record to entity tracking and relocate to a url --->
<CFPARAM name="saveURL" default="true">
<CFIF not isDefined("eID")>
	<CFABORT SHOWERROR="You must define an entityID">
</CFIF>
<CFIF not isDefined("eType")>
	<CFSET eType = "element">
</CFIF>
<CFIF not isDefined("pID")>
	<CFABORT SHOWERROR="You must define an personID">
</CFIF>
<CFPARAM NAME="goToURL" DEFAULT="">

<!--- START: NYB 2011-02-09 LHID5321 - thought this would be a nice touch: --->
<CFIF etype eq "files">
	<cfoutput><p>File loading...</p></cfoutput>
<cfelse>
	<cfoutput><p>Page loading...</p></cfoutput>
</CFIF>
<!--- END: NYB 2011-02-09 LHID5321 - thought this would be a nice touch --->

<!--- WAb 2007/11/13 needed some change to deal with https 
	check for https before removing the protocol
--->

<cfparam name = "httpProtocol" default = "http">
<!--- remove http:// from gotoURL if it is present --->
<cfif reFindNoCase ("\Ahttps:\/\/",gotoURL)>
	<cfset httpProtocol = "https">
	<cfset gotoURL = reReplaceNoCase(gotoURL,"\Ahttps:\/\/","")>
<cfelse>
	<cfset gotoURL = reReplaceNoCase(gotoURL,"\Ahttp:\/\/","")>
</cfif>

<!--- if the eType is files --->
<cfif etype eq "files">
	<CFQUERY NAME="getPath" DATASOURCE="#application.sitedatasource#">
		SELECT FileType.Path,
			   Files.Filename,
			   Files.PersonID,
			   FileTypeGroup.heading,
			   files.emailOwnerOnDownload
		FROM Files, FileType, FileTypeGroup
		WHERE Files.FileID =  <cf_queryparam value="#eid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		  AND Files.FileTypeID = FileType.FileTypeID
		  AND FileTypeGroup.FileTypeGroupId = FileType.FileTypeGroupId
	</CFQUERY>
	<!--- default goToURL to filename & Path --->
	<CFset goToURL="#getPath.path#/#getPath.filename#">
</cfif>

<!--- NYB 2011-02-02 LHID5321 added entityTrackingToLocLength to limit size of URL query was trying to insert into the table to no larger than the table will allow. 
have only added this check here because it *should* only be external urls this could ever be a problem for
--->
<cfset entityTrackingToLocLength = application.com.dbTools.getColumnSize(tableName="entityTracking",columnName="toLoc")>
<cfif StructIsEmpty(entityTrackingToLocLength)>
	<cfset entityTrackingToLocLength = 200>
<cfelse>
	<cfset entityTrackingToLocLength = entityTrackingToLocLength.toLoc>
</cfif>

<cfset entityTrackingArgs = {entityID=eid,entityTypeID=application.EntityTypeID['#eType#'],personID=pid,visitID=0}>
<cfif saveURL>
	<cfset entityTrackingArgs.goToUrl = goToURL>
</cfif>
<cfif isDefined("cid")>
	<cfset entityTrackingArgs.commID = cid>
<cfelseif isDefined("commid")>
	<cfset entityTrackingArgs.commID = commID>
</cfif>
<cfif structKeyExists(request,"relayCurrentUser") and structKeyExists(request.relayCurrentUser,"visitID")>
	<cfset entityTrackingArgs.visitID = request.relayCurrentUser.visitID>
</cfif>
<cfset application.com.entityFunctions.addEntityTracking(argumentCollection=entityTrackingArgs)>

<!--- WAB 2013-04-30 Add cf_queryparam (for N varchar support) --->
<!--- <CFQUERY NAME="addEntityTracking" datasource="#application.sitedatasource#">
	insert into entityTracking (entityID, entityTypeID, Personid,toLoc, created, commid)
	select #eid#,
		#application.EntityTypeID['#eType#']#  <!--- (select entityTypeId from flagEntityType where tablename='#eType#') --->
		,#pid#,
		<CFIF saveURL>
			<cf_queryParam value="#left(goToURL,entityTrackingToLocLength)#" cfsqltype="CF_SQL_VARCHAR">,
		<CFELSE>
		null,
		</cfif>
		#now()#,
		<CFIF isDefined("cid")>
			#cid#
		<CFELSEIF isDefined("commid")>
			#commid#
		<CFELSE>
			null
		</cfif>
</CFQUERY> --->

<CFOUTPUT>
<!--- 2002-11-07 SWJ not sure we should have any HTML or BODY tags in this template<cf_head>
<cf_title>#request.CurrentSite.Title#</cf_title>

</cf_head>
 --->
<!--- PJP 2012-09-04: Case 430418: Added in request.noSession test so redirect works on no sessions --->
<CFIF etype neq "files">
	<!--- 2012-01-29 WAB CASE 433406 remove htmlEditFormating the gotoURL, in particular makes ampersand go wrong --->
	<cfset locationUrl = "#httpProtocol#://#goToURL#">
	<!--- <cf_body onLoad="window.focus();window.location.href='#htmleditformat(httpProtocol)#://#htmleditformat(goToURL)#';"> --->
<CFELSE>
	<!--- WAB 2011/01/12 changed httpProtocol to protocolAndDomain --->
	<cfset locationUrl = "/content/#htmleditformat(getPath.path)#/#htmleditformat(getPath.Filename)#">
	<!--- <cf_body onLoad="window.focus();window.location.href='/content/#htmleditformat(getPath.path)#/#htmleditformat(getPath.Filename)#';">  --->
</CFIF>

<cfif structKeyExists(request,"noSession") and request.noSession>
	<script>
		window.location.href='#jsStringFormat(locationURL)#';
	</script>
<cfelse>
	<cf_body onLoad="window.focus();window.location.href='#locationURL#';">
</cfif>
<!---  --->

</cfoutput>


	
	
	

