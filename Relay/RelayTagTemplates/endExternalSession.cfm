<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:    endExternalSession.cfm
Author:     SWJ
Date created: 30 April 2002

  Objective - log person out
    
  Syntax    - None
  
  Parameters - None required
        
  Return Codes - None 

Amendment History:

Date (DD-MMM-YYYY)  Initials  What was changed
2010-03-31      NAS     LID 3169 - Remember me cookie prevents portal log out 
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Enhancement still to do:



--->

<!---<CFIF isdefined("cookie.partner session")>
  <cfset application.com.globalFunctions.cfcookie(NAME="Partner SESSION" VALUE="" EXPIRES="NOW")>
</cfif>

<CFOUTPUT>
  <FORM name="endEnternalSessionForm" action="#SCRIPT_NAME#?#request.query_string#"></FORM>
</CFOUTPUT>

<script>document.endEnternalSessionForm.submit()</script> --->

<!--- START   2010-03-31      NAS     LID 3169 - Remember me cookie prevents portal log out  --->
<cfscript>
  application.com.login.unSetRememberMeCookie();
</cfscript>
<!--- END   2010-03-31      NAS     LID 3169 - Remember me cookie prevents portal log out  --->

<cfoutput>
  <script>
    document.location.href = "/?logout=1";
  </script>
</cfoutput>
