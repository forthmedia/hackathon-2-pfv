<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		elearningTrainingPathAssignment.cfm
Author:			YMA
Date started:	2014-04-23

Description:	For Primary contacts to assign a training path for their colleagues to follow.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


 --->


<cf_param name="sortOrder" label="Training Path Sort By" validvalues="TrainingPath,created" default="trainingPath" />
<cf_param name="orderBy" label="Training Path Sort Direction" validvalues="Asc,Desc" default="Asc" />

<!--- only show for training contacts --->
<!---<cfset isPrimaryContact = IIF(application.com.flag.doesFlagExist(flagID='KeyContacts Primary') and application.com.flag.getFlagData(flagID="KeyContacts Primary",data=request.relaycurrentUser.personID).recordCount gt 0,true,false)>--->
<cfset isPrimaryContact = application.com.relayPLO.isPersonAPrimaryContact()>
<cfset isTrainingContact = IIF(application.com.relayElearning.getTrainingContact(organisationID=request.relaycurrentUser.organisationID,personID=request.relaycurrentUser.personID) eq request.relaycurrentUser.personID,true,false)>

<cfif isPrimaryContact or isTrainingContact>
	<cfset sortOrder = sortOrder & " " & orderBy>

	<cfset availableTrainingPath = application.com.trainingPath.getTrainingPathListForUsers(sortOrder=sortOrder,entityID=request.relaycurrentuser.organisationID,entityType="organisation")>

	<cfquery dbtype="query" name="distinctAvailableTrainingPath">
		select distinct TrainingPathId, TrainingPath
		from availableTrainingPath
		where ignoreAssignmentRules = 0
	</cfquery>

	<cfif distinctAvailableTrainingPath.recordcount gt 0>

		<cf_includeJavascriptOnce template="/elearning/js/elearning.js" translate="true">
		<cf_includeJavascriptOnce template="/javascript/prototypeSpinner.js" translate="true">

		<cf_modalDialog identifier=".trainingPathInfo" type="ajax">
		<script>
			jQuery(document).ready(function() {
				trainingPath.init();
			});
		</script>

		<div class="trainingPath 2">
		<cfloop query="distinctAvailableTrainingPath">

			<cfquery dbtype="query" name="availablePeopleForTrainingPath">
				select 0 selected, personID, fullname
				from availableTrainingPath
				where trainingPathID in (#TrainingPathid#)
				and trainingPathPersonID is null
				union
				select 1 selected, personID, fullname
				from availableTrainingPath
				where TrainingPathID in (#TrainingPathid#)
				and trainingPathPersonID is not null
				order by fullname
			</cfquery>

			<cfquery dbtype="query" name="peopleAssignedTrainingPath">
				select 1 selected, personID, fullname
				from availableTrainingPath
				where trainingPathID in (#trainingPathID#)
				and trainingPathPersonID is not null
			</cfquery>

			<cfoutput>
				<div class="trainingPathResult" id="trainingPathResult#TrainingPathID#"></div>
				<div class="trainingPathRow form-group">
					<div class="trainingPathName">
						<p>
							<a class="trainingPathInfo btn btn-primary" href="/webservices/callWebservice.cfc?wsdl&method=callWebService&webservicename=relayElearningWS&methodName=getTrainingPathInfo&returnFormat=plain&TrainingPathId=#TrainingPathId#">#htmlEditFormat(TrainingPath)#</a>
						</p>
					</div>
					<div class="selectBox">
						<cf_relayFormElement fieldname="frmAssignPeopleToTrainingPath#TrainingPathid#"  relayFormElementType="select" multiple="true" currentValue="#valueList(peopleAssignedTrainingPath.personID)#" size="10" display="fullname" value="personID" query="#availablePeopleForTrainingPath#">
					</div>
					<div class="submit">
						<cfset dataAttributes = {trainingPathID=TrainingPathid}>
						<p><cf_relayFormElement relayFormElementType="button" currentValue="phr_elearning_TrainingPath_assignTrainingPath" fieldname="frmAssignTrainingPath#TrainingPathid#" dataAttributes="#dataAttributes#" label=""></p>
					</div>
				</div>
			</cfoutput>
		</cfloop>
		</div>

	<cfelse>
		phr_elearning_TrainingPath_noTrainingPathAvailable
	</cfif>
<cfelse>
	phr_elearning_TrainingPath_notTrainingContact
</cfif>