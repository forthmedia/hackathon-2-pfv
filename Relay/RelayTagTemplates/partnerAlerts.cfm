<!--- �Relayware. All Rights Reserved 2014 --->
<cfset resellerOrgID = request.relaycurrentUser.organisationID>
<cfset approvals 	= application.com.partnerAlerts.getApprovals(resellerOrgID=resellerOrgID)>
<cfset renewals  	= application.com.partnerAlerts.getRenewals(resellerOrgID=resellerOrgID)>

<!--- 
<cfset opportunities = application.com.partnerAlerts.getOpportunities(resellerOrgID=resellerOrgID)>
 --->
<!--- 
<cfset opportunities = application.com.opportunity.getCustomerControllerOpportunities(resellerOrgID=resellerOrgID,opportunityTypes='ALL',showCurrentMonth=true)>
 --->

<!--- PPB 2010/08/02 switched opportunities to work with listPartnerOpportunities instead of getCustomerControllerOpportunities --->
<cfset opportunities = application.com.opportunity.listPartnerOpportunities(opportunityView='vLeadListingCalculatedBudget',sortOrder='currency',partnerSalesPersonID=#request.relayCurrentUser.personID#,showCurrentMonth=true)>
<cfset pipeline = application.com.opportunity.listPartnerOpportunities(opportunityView='vLeadListingCalculatedBudget',sortOrder='currency',partnerSalesPersonID=#request.relayCurrentUser.personID#,filterType='PIPELINE',showCurrentMonth=true)>
<cfset forecast = application.com.opportunity.listPartnerOpportunities(opportunityView='vLeadListingCalculatedBudget',sortOrder='currency',partnerSalesPersonID=#request.relayCurrentUser.personID#,filterType='FORECAST',showCurrentMonth=true)>

<!--- 
<cfset pipeline = application.com.opportunity.getCustomerControllerOpportunities(resellerOrgID=resellerOrgID,opportunityTypes='PIPELINE',showCurrentMonth=true)>
<cfset forecast = application.com.opportunity.getCustomerControllerOpportunities(resellerOrgID=resellerOrgID,opportunityTypes='FORECAST',showCurrentMonth=true)>
 --->
<!--- 
<cfset pipeline		= application.com.partnerAlerts.getOpportunities(resellerOrgID=resellerOrgID,filterType='pipeline',sortorder='currency')>
<cfset forecast		= application.com.partnerAlerts.getOpportunities(resellerOrgID=resellerOrgID,filterType='forecast',sortorder='currency')>
 --->
<!--- 
<cfset pipeline		= application.com.partnerAlerts.getOppProductsForGivenDate(resellerOrgID=resellerOrgID,l_pipeline=1,sortorder='currency')>
<cfset forecast		= application.com.partnerAlerts.getOppProductsForGivenDate(resellerOrgID=resellerOrgID,l_pipeline=0,sortorder='currency')>
 --->


<cfset currentUserIsApprover = application.com.flag.isFlagSetForCurrentUser("PartnerOrderingManager")>


<cfset spacing="&nbsp;&nbsp;">
<cfoutput>
	<table width="630" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td>
		    <cfif currentUserIsApprover>
			    <p><strong>phr_opp_number_of_pending_approvals:</strong></p>
				<ul>
				  <li>
					  phr_opp_number_of_orders_awaiting_your_approval:#htmleditformat(spacing)#<a href="?eid=OrderApproval">#htmleditformat(approvals)#</a>
				  </li>
				</ul>
		    </cfif>
			<p><strong>phr_opp_no_of_renewals_alarms: </strong></p>
			  <ul>
				  <li>
			  			phr_opp_number_renewals_that_expire_this_month: #htmleditformat(spacing)#<a href="?eid=MyRenewals&showCurrentMonth=true">#htmleditformat(renewals)#</a>
			  	  </li>
			 </ul>
			<p><strong>phr_opp_open_opportunities:</strong></p>
			 <ul>
			 	<li>
				 	phr_opp_number_opportunites_that_close_this_month: #htmleditformat(spacing)#<a href="?eid=MyOpportunities&showCurrentMonth=true">#opportunities.recordcount#</a>
			 	</li>
			</ul>
			<p><strong>phr_opp_your_pipeline: </strong></p>
			<ul>
				<li>
			    	phr_opp_pipeline_for_current_month: #htmleditformat(spacing)#<a href="?eid=MyOpportunities&showCurrentMonth=true&filterType=PIPELINE">#htmleditformat(application.com.partnerAlerts.getOppValueByCurrencyString(pipeline))#</a>
				</li>
			</ul>
			<p><strong>phr_opp_your_forecast: </strong></p>
			<ul>
				<li>
			    	phr_opp_forecast_for_current_month: #htmleditformat(spacing)#<a href="?eid=MyOpportunities&showCurrentMonth=true&filterType=FORECAST">#htmleditformat(application.com.partnerAlerts.getOppValueByCurrencyString(forecast))#</a>
				</li>
			</ul>
		 </td>
		</tr>
	</table>
</cfoutput>

