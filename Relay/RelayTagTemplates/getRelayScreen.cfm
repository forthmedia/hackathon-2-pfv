<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		getRelayScreen.cfm
Author:			Simon
Date created:	2002-05-14

	Objective - loads a screen based on parameters defined in an element
		
	Syntax	  -	Should only be called as part of a relayTagInclude
	
	Parameters - CurrentScreenID
				
	Return Codes - none 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:


Mods:
WAB 2005-05-24  changed code so that encPersonID (an encrytped personid) can be passed into the code otherwise defaults to the users personid as previously

WAB 2006-05-23 P_CHW001 needed to be able to use this template to add a new location.  Had to be modified to handle this.

WAB 2007-02-07  took out the ability to add a new organisation from this screen because it crashes later in the process.  
			Can still add new locations to the existing organisation
			And would also allow the unknown user to add a new organisation if frmOrganisationid = newOrg, frmLocationid=newLoc, frmPersonid=newPer and organisation_countryid_default = 0 were passed in as parameters of the tag
			
NJH 2008-08-18	CR-SNY651 - changed hard-coded references to http to a variable that gets set to http if cgi.HTTPS is not "on"; else https
2013-07-09 	NYB 		Case 435968
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

--->

<cf_param name="frmCurrentScreenID" type="string" label="The screen you wish to display" default="" validValues="select screenID as dataValue, ltrim(rtrim(ScreenName)) as displayValue from screens where screenID <> 0 and active=1 order by entityType, ScreenName"/>
<cf_param name="goToEID" type="string" default="" label="The page you would like to go to after the screen has been processed"/>
<cfparam name="frmNextPage" type="string" default="thisPage"/>
<cf_param name="frmMethod" type="string" default="Edit" label="Display Method" validValues="select 'edit' as dataValue, 'Edit' as displayValue union select 'view' as dataValue, 'View' as displayValue"/>
<cf_param name="frmNoButton" type="boolean" default="No" label="Hide Button"/>
<cf_param name="frmDontShowrequired" type="boolean" default="Yes" label="Show Required Markers"/>
<cf_param name="nextScreenid" label="Next Screen" default="" validvalues="select screenID as dataValue, ltrim(rtrim(ScreenName)) as displayValue from screens where screenID <> 0 and active=1 order by entityType, ScreenName" nullText=" - "/>
<cf_param name="frmParametersToHideOnForm" label="Parameters To Hide On Form" default=""/>

<CFIF not isdefined("m")>
	<CFSET message="">
</CFIF>

<CFSET qs=request.QUERY_STRING>
<CFIF frmNextPage eq "thisPage">
	<cfset frmNextPage = "/?#qs#&m=1">
</CFIF>
<cfif goToEID neq "">
	<cfset frmNextPage = "/?eid=#goToEID#">
</cfif>

<CFIF frmCurrentScreenID neq "">
	<cfif isDefined("encPersonID")>
		<CFSET frmPersonID = application.com.encryption.decryptID(encPersonID)>
	<cfelse>
		<!--- WAB 2006-05-23 made some changes so that this code can be used for a user to edit other sites within the same organisation
				not actually sure whether this is the right thing to do, needs some sort of security I would have thought
				<cfset frmPersonid = request.relaycurrentUser.personid>
				
				to add a new location, would need to be called with frmLocationID = new and frmPersonid = 0
				
		 --->
		<cfif not request.relayCurrentUser.isUnknownUser>
			<cfset frmOrganisationid  = request.relayCurrentUser.organisationid>
			<CFparam name="frmLocationid" default = #request.relayCurrentUser.locationid#>
			<CFparam name="frmCountryID" default = #request.relayCurrentUser.location.countryid#><!--- 2013-07-09 NYB Case 435968 added --->
			<CFparam name="frmPersonID" default = #request.relayCurrentUser.PersonID#>
			<cfif isdefined("LocationID") and LocationID neq "" and isnumeric(LocationID)>
				<cfset frmLocationID = LocationID>
			</cfif>
			<cfif isdefined("PersonID") and PersonID neq "" and isnumeric(PersonID)>
				<cfset frmPersonID = PersonID>
			</cfif>
		<cfelse>
			<!--- This has never been tested - 
			don't actually think that unknown user ever uses this tag (relayaddcontact is used instead), but may be in the future it could happen
			2007-02-07 infact it always errors in getDefaultvalues.cfm because countryid is not set. would need <cfset organisation_countryid_default = 0> 
			I think for security reasons that it shouldn't allow adding new organisations etc. unless it is specifically allowed by passing in a parameter
			Therefore I am going to cut out this bit
			--->
			<!--- 			
			<cfset frmOrganisationid  = "newOrg">
			<cfset frmLocationid  = "newLoc">
			<cfset frmPersonid  = "newPer">
			 --->		
		</cfif>
	</cfif>
	
	<cfif isDefined("frmPersonID") and frmPersonID IS NOT application.unknownpersonid>  <!--- WAB 2007-02-07 added to prevent editing of the unknown user record --->
		<CFSET frmCurrentScreenID = frmCurrentScreenID>
		<CFSET frmNextPage = frmNextPage>
		<cfparam name="debug" default="no">
		<CFIF debug eq "yes">
			<cfoutput>frmCurrentScreenID = #htmleditformat(frmCurrentScreenID)#<br>frmPersonID = #htmleditformat(frmPersonID)#</cfoutput>
		</CFIF>
		<CFINCLUDE template= "/screen/showScreenNew.cfm">
	</cfif>
</CFIF>
