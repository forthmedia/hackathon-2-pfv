﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		elearningCourses2Level.cfm
Author:			NJH
Date started:	2009-02-12 - bas

Description:	Based on Sophos eLearning Relay Tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-07-16			NYB			CR-SNY047 - added CourseSortOrder and ModuleSortOrder to param options
2009-07-23			NJH			P-FNL069 - encrypted url variables and accept only encrypted course and moduleID's.
2010-02-18			NAS			LID3099 add ',resizable=1' to make the popup resizable in ie.
2010-08-06			NJH			RW8.3 - moved call to elearningParams as it's not longer needed with new settings.
2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses
2010-09-22 			PPB 		P-LEN022  (LID 4052) use the local language/country file that is in the same fileFamily as the file associated with the module
2010-10-08			NAS			LID4300: Courses not launching correctly
2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive)
2011-02-25			NJH			LID 5753 - Moved the functionality to get the correct file in the file family into the getCourseLauncherURL function
2011-04-21			MS			P-LEN-30 CSAT (CR-031) Added hooks to implement custom star ratings, published date diaplay for course and modules
2011-07-28			NAS			LID7318: E-Learning - Look and Feel Core Changes
2011-09-16 			NYB 		P-SNY106
2011/11/23 			PPB 		CR-LEN061 added image
2012-04-18			WAB			Renamed from ElearningCoursesV3.cfm to ElearningCourses2Level.cfm to distinguish from  ElearningCoursesV2.cfm/ElearningCourses3Level.cfm
2012-04-18			WAB			CASE 426194 Added Published Date (from elearningCoursesV2) sorry no special styling
2012-06-20 			PPB 		Case 428792 now fire off insertPersonModuleProgress from an onClick event (so it works for Akamai files and remove it from fileDisplay.cfm)
2012-07-05 			STCR 		P-REL109 Phase 2 - pass personid to getCourseLauncherUrl(), so that userModuleProgress records get generated for CourseWare
2012-07-17			Englex		P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR031 CSAT (Ratings)
2012-07-27			Englex		P-REL109 Phase 2 - Module Prerequisites
2013-04-10 			NYB 		Case 434164 added elearning.useModuleCountryScoping as default
2013-04-30			STCR		CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date
2013-05-01			STCR		CASE 434886 / 434972 - cf_param without self-closing slash was causing XML corruption in RelayTagEditor
2013-05-28			STCR		CASE 435368 - do not return Courses before their Activation Date, unless previewing content for a specific date
2013-07-02			PPB			Case 435750 allow html tags in course title and description
2013-11-07			NYB 		Case 437616 added useModuleCountryScoping to parameters passed
2014-01-31			WAB 		removed references to et cfm, can always just use / or /?
2014-06-17			REH		Case 439511 custom template for level two (2 click process) module and course title content
2014-08-27			REH		Case 441146 Truncate Description on two tier relaytag to height of image
2016-11-30			GCC		Case 452618 Added ability to have multiple versions of the tag on the same page filterd by solutionarea and/or series and/or level and added per tag optional heading.
 --->
<cf_param name="courseListHeading" type="string" default="" label="Optional Heading for the course list"/>
<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/><!--- question order --->
<cf_param name="ShowBackLinks" type="boolean" default="yes"/>
<!--- START:  NYB 2009-07-16 CR-SNY047 - added: --->
<cf_param name="CourseSortOrder" type="string" default="publishedDate DESC,sortOrder,Title_Of_Course"/>
<cf_param name="ModuleSortOrder" type="string" default="sortOrder,Title_of_Module"/>
<!--- END:  2009-07-16 CR-SNY047 --->
<!--- START: 2011-09-16 NYB P-SNY106 - added params: --->
<cf_param NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#" validValues="Standard,Strict"/>
<cf_param NAME="showIncorrectQuestionsOnCompletion" DEFAULT="No" type="boolean"/>
<!--- END: 2011-09-16 NYB P-SNY106 --->
<!--- 2014/04/14	YMA	Case 434303 Test Feedback to users --->
<cf_PARAM LABEL="Review answers link in quiz complete page" NAME="reviewAnswersLink" DEFAULT="false" TYPE="boolean"/>

<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
<cfparam name="available" type="Boolean" default="1">

<!--- 2013-04-10 NYB Case434164 added default --->
<cf_param name="useModuleCountryScoping" default="#application.com.settings.getSetting('elearning.useModuleCountryScoping')#" type="boolean"/>

<!--- 2011/11/23 PPB CR-LEN061 added image --->
<cf_param name="showThumbNail" type="Boolean" default="false"/>	<!--- will be false --->
<cfparam name="thumbNailWidth" type="numeric" default="90">

<!--- 2013-04-30 STCR CASE 432193 - ignore Modules before their Activation Date, unless previewing content for a specific date --->
<cfif isDefined("request.relaycurrentuser.content.date") and isDate(request.relaycurrentuser.content.date)>
	<cfset variables.searchFromActivationDate = request.relaycurrentuser.content.date />
<cfelse>
	<cfset variables.searchFromActivationDate = request.requestTime />
</cfif>

<!--- NJH 2009/07/23 P-FNL069 if the variables have come from the url scope, then check that they were encrypted --->
<cf_checkFieldEncryption fieldNames = "courseID,moduleID">

<!--- START 2014-08-27 REH Case 441146 Truncate Description on two tier relaytag to height of image --->
<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.expander.js">
<cf_head>
	<script>
		function setExpander(level) {
			jQuery(level).expander({
			    slicePoint:       370,               // default is 100
			    expandText:       'phr_social_more', // default is 'read more'
			    userCollapseText: 'phr_social_less', // default is 'read less'
			    preserveWords:true
			  });
		}
		jQuery(document).ready(function() {
			setExpander('div.expandable div.expanding');
		});

	</script>
</cf_head>
<!--- END 2014-08-22 REH Case 441146 --->

<cf_includeJavascriptOnce template="/elearning/js/elearning.js">
<cf_includeonce template="/eLearning/relaySCORM.cfm" />

<cfif application.com.settings.getSetting("elearning.useModuleRating")>
	<cf_includejavascriptonce template = "/javascript/lib/jquery/jquery.rating.js">
	<cf_includejavascriptonce template = "/javascript/ratings.js">
	<script>
		setBoxClass('rate_content');
		<cfoutput>setEntityTypeID(#application.entitytypeid.trngModule#);</cfoutput>
		jQuery(document).ready(
	    function()
	    	{
	        	// The DOM (document object model) is constructed
	        	// We will initialize and run our plugin here
	        jQuery('input.star').rating();
	        //we add the following to stop jQuery from conflicting with prototype.js
			jQuery.noConflict();

	    	}
	    )
	</script>
</cfif>
<!--- 2012-06-22 GCC changed from url. so that content editors can force a course plain text as tag attributes - will still block form and url scoped variables if unencrypted.--->
	<cfparam name="solutionArea" default="">
	<cfparam name="userLevel" default="">
	<cfparam name="series" default="">
	<cfif isdefined("courseID")>

		<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
		<cfset getCourseData = application.com.relayElearning.GetTrainingCoursesData(courseID=courseID,solutionarea=solutionArea,userlevel=userlevel,series=series,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=CourseSortOrder,fromActivationDate=variables.searchFromActivationDate)>
		<cfif getCourseData.recordcount gt 0><!--- stops multiple includes of this tag on the same page each displaying the modules after click through--->

			<!--- START: SHOW MODULES --->
			<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
			<!--- 2010-11-11			NAS			LID4683: Modules - can not make a module unavailable (inactive) --->
			<!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date, unless previewing content for a specific date --->
			<cfset getModuleInfo = application.com.relayElearning.GetTrainingModulesData(courseID=courseID,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=ModuleSortOrder,availableModules=available,PersonID=request.relayCurrentUser.personID,fromActivationDate=variables.searchFromActivationDate,useModuleCountryScoping=useModuleCountryScoping,showExpired=false)>  <!--- 2013-11-07 NYB Case 437616 added useModuleCountryScoping to parameters passed --->


			<!--- START		MS		2011-04-18	CSAT(CR-31) Add a hook to display custom content for Lenovo --->
			<!--- 2014-06-17 REH Case 439511 custom template for level two module title content --->
			<cfif fileexists("#application.paths.code#\cftemplates\eLearningModuleTitles2Level.cfm")>
				<cfinclude template="/code/cftemplates/eLearningModuleTitles2Level.cfm">
			<CFELSE><!--- SHOW Non-Custom Content if Cutom Lenovo Content is not available --->
				<div class="modules">
					<cfif courseListHeading neq "">
						<cfoutput><h2>#courseListHeading#</h2></cfoutput>
					</cfif>
					<cfif getCourseData.recordcount GT 0>
						<cfset form.Course_Title =getCourseData.Title_of_Course>
						<cfoutput><h2>phr_elearning_course_modules_heading</h2></cfoutput>
					</cfif>
					<div class="outer">
						<cfif getCourseData.recordCount gt 0>
							<cfset courseTitle = getCourseData.Title_of_Course>
							<cfset courseDescription = getCourseData.Description>

							<cfif getModuleInfo.recordcount gt 0>

								<cfoutput>
								<cf_includejavascriptonce template = "/javascript/openwin.js">
								<cf_modalDialog type="iframe" size="large" identifier=".StartQuiz a">
								<script>
									function startQuiz(encryptedLink) {
										openWin(encryptedLink,'MyWindow','width=600,height=750,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
									}

									<!---
									2012-06-20 PPB Case 428792 now fire off insertPersonModuleProgress from an onClick event and remove it from fileDisplay.cfm
									despite the function name upsert... this currently just inserts
									--->
									function upsertUserModuleProgress(moduleId,personId) {
										var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=true'
										var page = page + '&webservicename=relayElearningWS&methodname=insertUserModuleProgress'
										var parameters = {moduleId:moduleId,personId:personId}

										var myAjax = new Ajax.Request(
										                        page,
										                        {
										                            method: 'post',
										                            parameters: parameters
										                        });
										<!--- NJH 2012/11/21 Case 432093 --->
										refreshModuleStatus(moduleID,'started');
									}
								</script>
								</cfoutput>

								<cfoutput query="getModuleInfo">
									<cfset courseLauncherUrl = application.com.relayElearning.getCourseLauncherUrl(moduleID=ModuleID,personID=request.relaycurrentuser.personid)><!--- 2012-07-05 P-REL109 Phase 2 STCR included personid --->

									<cfset moduleDivClass="inner">
									<!--- NYB 2011-09-01 removed, didn't see the point of and couldn't find anywhere where .last is being styled up differenctly
									<cfif currentRow eq getModuleInfo.recordCount>
										<cfset moduleDivClass="inner last">
									</cfif>
									--->
									<!--- NJH 2009-07-23 P-FNL069  --->
									<cfset moduleDivClass = moduleDivClass & " " & module_completion_status>

									<!--- START: 2011/11/23 PPB CR-LEN061 added thumbnail images --->
									<cfset imagePath = "/content/linkImages/trngModule/">
									<cfset imageFileName = "#getModuleInfo.moduleId#/Thumb_#getModuleInfo.moduleId#.png"> <!--- 2014/08/07 AHL Case 441145. Updating new file structure /linkImages/trngModule/module ID/ Thumb_moduleID--->

									<cfif not FileExists("#ExpandPath(imagePath)##imageFileName#")>
										<cfset imageFileName = "Thumb_Module_Default.png">
									</cfif>
									<!--- END: 2011/11/23 PPB CR-LEN061 --->

									<!--- NJH 2012/11/21 Case 432093 - added ID on moduleDiv --->
									<div class="#moduleDivClass#" id="module#moduleID#div">
										<!--- START: 2011/11/23 PPB CR-LEN061 added thumbnail image; I used a table instead of floating div to avoid a long description wrapping under the image --->
										<!--- <table>
											<tr> --->
												<cfif showThumbNail>
													<!--- <td> --->
														<img class="pull-left" src="#imagePath##imageFileName#" width="#thumbNailWidth#">
													<!--- </td> --->
												</cfif>
												<!--- <td> --->
													<div class="text">
														<h3 id="Title" class="Title">phr_title_trngModule_#htmleditformat(moduleID)#</h3>
														<p><B>phr_eLearning_DatePublished</B>
															<CFIF getModuleInfo.publishedDate eq "">phr_eLearning_noPubDt<CFELSE>#LSDateFormat(getModuleInfo.publishedDate, "mmm dd, yyyy")#</CFIF>
														</p>

														<!--- START 2014-08-27 REH Case 441146 Truncate Description on two tier relaytag to height of image --->
														<div id="Description" class="Description">
															<div class="expandable">
																<div class="expanding">phr_description_trngModule_#htmleditformat(moduleID)#</div>
															</div>
														</div>
														<!--- END 2014-08-27 REH Case 441146 --->
													</div>
													<!--- START: 2012-07-27 - Englex - P-REL109 Phase 2 - Module Prerequisites --->
													<ul class="list-inline links">
														<li id="OpenStudy" class="OpenStudy">
															<!--- 2012-06-20 PPB Case 428792 now fire off insertPersonModuleProgress from an onClick event and remove it from fileDisplay.cfm --->

															<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
																					Courseware can display when a fileID is not set if the coursewareTypeID
																					is not default as it is likely a 3rd party API --->
															<cfif fileid neq "" and courseLauncherUrl neq ""
																or (courseLauncherUrl neq ""
																	and
																	coursewareTypeID neq application.com.relayForms.getDefaultLookupListForField(fieldname='coursewareTypeID').lookupID
																)
															>
																<a class="btn btn-primary" href="javascript:void(openWin('#courseLauncherUrl#','CoursewareContent#ModuleID#','width=#application.com.settings.getSetting('files.maxPopupWidth')#,height=#application.com.settings.getSetting('files.maxPopupHeight')#,scrollbars=no,status=no,address=no,resizable=1'));" onClick="upsertUserModuleProgress(moduleID=#moduleID#,personId=#request.relayCurrentUser.personId#)" align="left">phr_downloadTheNotes</a>
															</cfif>
														</li>
														<li id="StartQuiz" class="StartQuiz">
															<cfif QuizDetailID gt 0 and quizActive>
																<!--- 2014-04-14	YMA	Case 434303 Test Feedback to users --->
																<cfset encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#&reviewAnswersLink=#reviewAnswersLink#')>
															    <a class="btn btn-primary" href="#encryptedLink#">phr_takeTheQuiz</a>
															<cfelse>&nbsp;
															</cfif>
														</li>
													</ul>

													<!--- START: 2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR031 CSAT (Ratings) --->
													<cfif application.com.settings.getSetting("elearning.useModuleRating")>

														<!--- <cf_htmlhead text = "<link href='/javascript/css/jquery.rating.css' rel='stylesheet' media='screen,print' type='text/css'>"> --->
														<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery.rating.css">
														<cfset getRatings = application.com.relayRating.getRatings(entityID=getModuleInfo.moduleID,entitytypeID=application.entitytypeid.trngModule) />

															#application.com.relayRating.getRatingsWidget(entityID=getModuleInfo.moduleID,entityTypeID=application.entityTypeID.trngModule)#

														<!--- <div class="ratings">
															<!--- this ratings div has javascript in it, so am not sanitising it --->
															<div id="starRatings">
																#application.com.relayElearning.getRatingsDiv(moduleID=getModuleInfo.ModuleID)#
															</div>
															<div class="rate" id="box#getModuleInfo.ModuleID#">
																<div class="rate_content" id="subBox#getModuleInfo.ModuleID#">

																	<div id="RatingSubmit">

																		 <form name="contact" id="starForm#getModuleInfo.ModuleID#" action="" arg="#getModuleInfo.ModuleID#">
																			 <div id="stars-wrapper#getModuleInfo.ModuleID#">
																			<input type="hidden" name = "moduleid" id = "moduleid" value = "#getModuleInfo.ModuleID#">
																		   <fieldset>
																			   phr_eLearning_rateThisModule<BR>
																				<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=1 id="#getModuleInfo.ModuleID#1">
																				<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=2 id="#getModuleInfo.ModuleID#2">
																				<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=3 id="#getModuleInfo.ModuleID#3">
																				<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=4 id="#getModuleInfo.ModuleID#4">
																				<INPUT TYPE=RADIO NAME="entityScore" class="star" VALUE=5 id="#getModuleInfo.ModuleID#5">
																		     <br /><br />
																		    <!--- <input type="button" name="submit" class="button" id="submit_btn" value="phr_eLearning_RateIt" onclick="submitRatings2(this);" /> --->
																		   </fieldset>
																		   </div><!--- End of DIV stars-wrapper1 --->
																		 </form>
																	</div>



																</div>
															</div>
														</div> --->
													</cfif>
													<!--- END: 2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR031 CSAT (Ratings) --->

												<!--- </td>
											</tr>
										</table> --->
										<!--- END: 2011/11/23 PPB CR-LEN061 --->
									</div>

<!---
									<div class="#moduleDivClass#">

										<div class="text">
											<div id="Title" class="Title">phr_title_trngModule_#htmleditformat(moduleID)#</div>
											<div id="Description" class="Description">phr_description_trngModule_#htmleditformat(moduleID)#</div>
										</div>
										<div class="links">
											<div id="OpenStudy" class="OpenStudy">
											<cfif fileid neq ""><a href="javascript:void(openWin('#courseLauncherUrl#','CoursewareContent#ModuleID#','width=760,height=420,scrollbars=no,status=no,address=no,resizable=1'));" align="left">phr_downloadTheNotes</a></cfif>
											</div>
											<div id="StartQuiz" class="StartQuiz">
											<cfif QuizDetailID gt 0>
												<!--- START: 2011-09-16 NYB P-SNY106 - added encryptURL call then use the encryptedLink instead in the js call --->
												<cfset encryptedLink = application.com.security.encryptURL('/Quiz/Start.cfm?QuizDetailID=#QuizDetailID#&OrderMethod=#OrderMethod#&stringencyMode=#stringencyMode#&showIncorrectQuestionsOnCompletion=#showIncorrectQuestionsOnCompletion#')>
											    <a href="javascript:startQuiz('#encryptedLink#')">phr_takeTheQuiz</a>
												<!--- END: 2011-09-16 NYB P-SNY106 --->
											</cfif>
											</div>
										</div>
									</div>
 --->

								</cfoutput>
							<cfelse>
								phr_eLearning_NoModulesAvailable
							</cfif>
						<cfelse>
							phr_elearning_InsufficientRightsToViewCourse.
						</cfif>
					</div>
					</CFIF><!--- End 	MS		2011-04-18		CSAT(CR-31) --->

				<!--- END: SHOW MODULES --->
				<cfoutput>
					<!--- NJH 2009-07-23 P-FNL069  --->
					<cfset encryptedUrlVariables = application.com.security.encryptQueryString(querystring = "?eid=#url.eid#&CourseSortOrder=#CourseSortOrder#",  removed=false)>
					<cfif ShowBackLinks><p class="back"><a class="btn btn-primary" href="/#encryptedUrlVariables#"><i class="fa fa-arrow-left"></i> phr_elearning_backToCourses</a></p></cfif>
				</div>
			</cfoutput>
		</cfif>
	<cfelse>
		<!--- START: SHOW COURSES --->
		<!--- NYB 2009-07-16 CR-SNY047 - added SortOrder to params passed --->
		<!--- SKP 2010-09-01 LEN002 - add course filtering --->
		<!--- 2010-09-15			NAS			P-LEN022 add 'Active' to only retrieve active courses --->
		<cfparam name="active" default="1">
			<!--- START 2011-04-21	MS	CSAT (CR-31) Course List with Averages Sortable --->
			<!--- 2014-06-17 REH Case 439511 custom template for level two course title content --->
			<cfif fileexists("#application.paths.code#\cftemplates\eLearningCourseTitles2Level.cfm")>
				<cfinclude template="/code/cftemplates/eLearningCourseTitles2Level.cfm">
			<CFELSE>
				<!--- If custom file does not exist show standard content --->
				<!--- 2013-04-10 NYB Case 434164 added useModuleCountryScoping --->
				<!--- NJH 2016/12/16 JIRA PROD2016-PROD2016-2967 - don't show expired modules --->
				<cfset coursesQry = application.com.relayElearning.GetTrainingCoursesDataV2(solutionArea=solutionArea,series=series,userLevel=userLevel,visibleForPersonID=request.relayCurrentUser.personID,SortOrder=CourseSortOrder,active=active,personID=request.relayCurrentUser.personID,useModuleCountryScoping=useModuleCountryScoping,fromActivationDate=variables.searchFromActivationDate,showExpired=false)>

				<div class="courses">
					<cfif courseListHeading neq "">
						<cfoutput><h2>#courseListHeading#</h2></cfoutput>
					</cfif>
					<h2>phr_elearning_courses_heading</h2>
					<div class="outer">
					<cfif coursesQry.recordCount>
						<cfoutput query="coursesQry">
							<cfset courseDivClass="inner">
							<!--- NYB 2011-09-01 removed, didn't see the point of and couldn't find anywhere where .last is being styled up differenctly
							<cfif currentRow eq coursesQry.recordCount>
								<cfset courseDivClass="inner last">
							</cfif>
							--->
							<!--- NJH 2009-07-23 P-FNL069  --->
							<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?eid=#url.eid#&courseID=#courseID#&CourseSortOrder=#CourseSortOrder#")>
							<cfif num_modules_in_course gt 0 and num_modules_attempted gt 0>
								<cfif num_modules_in_course eq num_modules_completed>
									<cfset courseDivClass = courseDivClass & " completed">
								<cfelse>
									<cfset courseDivClass = courseDivClass & " started">
								</cfif>
							<cfelse>
								<cfset courseDivClass = courseDivClass & " initial">
							</cfif>

							<!--- START: 2011/11/23 PPB CR-LEN061 added thumbnail images --->
							<cfset imagePath = "/content/linkImages/trngCourse/#coursesQry.courseId#/">
							<cfset imageFileName = "Thumb_#coursesQry.courseId#.png">

							<cfif not FileExists("#ExpandPath(imagePath)##imageFileName#")>
								<cfset imagePath = "/images/MISC/">
								<cfset imageFileName = "spacer.gif">
							</cfif>
							<!--- END: 2011/11/23 PPB CR-LEN061 --->

							<div class="#courseDivClass#">
								<!--- START: 2011/11/23 PPB CR-LEN061 added thumbnail image; I used a table instead of floating div to avoid a long description wrapping under the image --->
								<!--- <table>
									<tr> --->
										<cfif showThumbNail>
											<!--- <td> --->
												<img class="pull-left" src="#imagePath##imageFileName#" width="#thumbNailWidth#">
											<!--- </td> --->
										</cfif>
										<!--- <td> --->
								<h3 id="Title" class="Title"><a href="/#encryptedUrlVariables#" class="Title"><span class="Title">#Title_of_Course#</span></a></h3>		<!--- 2013-07-02 PPB Case 435750 removed htmleditformat so html displays --->
								<p>
									<B>phr_eLearning_DatePublished</B>
									<CFIF publishedDate eq "">phr_eLearning_noPubDt<CFELSE>#LSDateFormat(publishedDate, "mmm dd, yyyy")#</CFIF>
								</p>
								<p id="Description" class="Description">#Description#</p>		<!--- 2013-07-02 PPB Case 435750 removed htmleditformat so html displays --->
										<!--- </td>
									</tr>
								</table> --->
								<!--- END: 2011/11/23 PPB CR-LEN061 --->
							</div>
						</cfoutput>
					<cfelse>
						<p>phr_elearning_ThereAreNoCoursesAvailable.</p>
					</cfif>
				</div>
			</CFIF><!--- END: 2011-04-21	MS	CSAT (CR-31) Course List with Averages Sortable --->

		<!--- END: SHOW COURSES --->
	</cfif>