<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		elearningCertificationRegistration.cfm	
Author:			NJH  
Date started:	26-08-2008
	
Description:	eLearning certification registration for the portal 		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-06-03          NYB         Sop010
2011-03-04 			NYB 		LHID3717 & 4828; removed expandFor and all associated support code
								because P-FNL069 change made to relay/relayElearningWS.cfc on 2009/07/06
								has broken the showModules dropdown link and rendered this feature unsupportable.
2011-09-16 			NYB 		P-SNY106
2011-09-16 			NYB 		P-SNY106
2013-04-16			STCR		CASE 432328 Question OrderMethod
2013-06-26			NYB			CASE 434909 SortOrder code now moved to \elearning\personCertifications.cfm and deals with page being showed on a person or an organisation basis
2014-01-31			WAB 		removed references to et cfm, can always just use / or /?

Possible enhancements:

 --->

<cfparam name="moduleID" type="numeric" default="0">
<cfparam name="mode" type="string" default="view" >

<cf_param NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#" validValues="Standard,Strict"/>
<cf_param NAME="showIncorrectQuestionsOnCompletion" DEFAULT="No" type="boolean"/>
<!--- 2013-05-16 NYB Case 434909 - removed from here and put in the file being called 
<cfparam name="defaultSortOrder" label="Sort Order" default="Code,Description,Registration_Date,Status"/>
<cfparam name="sortOrder" default="Code,Description,Registration_Date,Status"/>
--->

<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/><!--- question order --->
<cfparam name="goToElement" default="currentPage">

<cfif goToElement eq "currentPage">
	<cfset goToElement = request.currentElement.id>
</cfif>

<cfif moduleID eq 0>
	<cfset goToUrl = "/?eid=#goToElement#">
	<!--- 2013-05-16 NYB Case 434909 - removed  
	<cfset formName="mainForm">
	--->
	<cf_param name="validCertificationsOnly" default="Yes" type="boolean"/>
	
	<cf_includeonce template="/eLearning/relaySCORM.cfm" />
		
	<!--- 2013-05-16 NYB Case 434909 - removed  
	<cfform name="#formName#"  method="post" >
	--->
	<!--- 2013-06-26 NYB Merge changes - added getFieldXML="true" and changed to cf_include from cfinclude: --->
	<cf_include template="\elearning\personCertifications.cfm" getFieldXML="true"/>
	<!--- 2013-05-16 NYB Case 434909 - removed  
	</cfform>
	--->
</cfif>