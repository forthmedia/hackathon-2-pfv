<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		IncentiveInstantReward.cfm
Author:			SSS
Date created:	2008/11/07

	Objective - To allocate points to a incentive account
	
	Rationale - To allow the entering of prromotion points
	
	Syntax	  -	This will be a relay tag that will be loaded into a screen
	
	Parameters 	TBA


Date Tested:
Tested by:

Amendment History:

(DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:

--->
<cfparam name="request.relayFormDisplayStyle" default="HTML-table">
<cfparam name="attributes.PromoID" default="26">

<!--- run this query to make sure the company has a accunt they can give the points to before entering the code --->
<CFQUERY NAME="GetCompanyAccount" datasource="#application.siteDataSource#">
	select *
		from organisation o inner join
		rwcompanyaccount rwca on o.organisationID = rwca.organisationID
		where rwca.organisationID = #request.relaycurrentuser.organisationID#
</CFQUERY>
<!--- This error block is for when a user has entered a code into the system --->
<cfif isdefined("CodeEntered")>
 	<CFQUERY NAME="CheckCode" datasource="#application.siteDataSource#">
		select *
		from InstantReward
		where code =  <cf_queryparam value="#form.CodeEntered#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>
	<cfif CheckCode.recordcount EQ 0>
	<!--- no code in the system matches the code entered so we assume it was entered incorrectly --->
		<cfset errorCodephrase = "phr_InstantReward_IncorrectCode">
	<cfelseif CheckCode.RwPromotionID NEQ attributes.PromoID>
	<!--- If a code is entered and it does not relate to the promotion page there on this error will be incountered --->
		<cfset errorCodephrase = "phr_InstantReward_WrongPromotion"> 
	<cfelseif CheckCode.claimDate NEQ "">
		<!--- if the voucher has already been redeemed you are not allowed to reenetr it --->
		<cfset errorCodephrase = "phr_InstantReward_CodeRedeemed">
	<cfelse>
		<!--- if all the errors are by passed then give the points to the client--->
		<!--- accrue the points --->
		 <cfset Variables.TransactionID = application.com.relayIncentive.RWAccruePoints(
										AccountId= GetCompanyAccount.AccountID,
										AccruedDate= CreateODBCDateTime(request.requesttime),
										PointsAmount= CheckCode.value,
										PersonID= request.relaycurrentuser.personID)>
		
		<!--- add the transaction that is to be added as part of this points allocation --->
		<cfset Variables.ItemTransaction = application.com.relayIncentive.AddRWTransactionItems(
										RWTransactionID=Variables.transactionID,
                                        ItemID=0,
                                        quantity=1,
                                        points= CheckCode.value,
                                        endUserCompanyName=GetCompanyAccount.organisationname,
                                        pointsType='PP',
                                        RWpromotionID=attributes.PromoID)>
		
		<!--- This will submit the claim and allocate the points --->								
		<cfset Variables.totalPointsClaimed = application.com.relayIncentive.submitClaim(
                                        RWTransactionID = Variables.transactionID,
                                        RWTransactionTypeID = "AC",
                                        distiID = 0)>
		
		<!--- Add the details of the transaction to the InstantRewards table --->
		<CFQUERY NAME="UpdateInstantRewardCode" DATASOURCE="#application.SiteDataSource#">
			Update InstantReward
			set ClaimDate = #CreateODBCDateTime(request.requesttime)#,
			ClaimedBy = #request.relaycurrentuser.personID#,
			RwTransactionID =  <cf_queryparam value="#Variables.TransactionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			where code =  <cf_queryparam value="#form.CodeEntered#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY> 
		<cfset Successphrase ="phr_InstantReward_PointsAllocated">
		<cfset Variables.totalPointsClaimed= CheckCode.value>
		
	</cfif> 
	
<cfelse>
	<!--- Get the promotion to check if it is valid --->
	<CFQUERY NAME="getpromotion" datasource="#application.siteDataSource#">
		select *
		from RWPromotion
		where RWPromotionID =  <cf_queryparam value="#attributes.promoID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<!--- this is a error block for when a person just enters a promotion before a code has been entered --->
	<cfif GetCompanyAccount.recordcount EQ 0>
	<!--- if there is no company account to add points to there is no need to go any further --->
		<cfset errorphrase ="phr_InstantReward_noaccount">
	<cfelseif attributes.promoID EQ 0>
	<!--- this error is if there is a setup problem with the promotion --->
		<cfset errorphrase ="phr_InstantReward_relaytag">
	<cfelseif getpromotion.recordcount EQ 0>
	<!--- This error if the promotion does not exist another setup problem --->
		<cfset errorphrase ="phr_InstantReward_ErrorNoPromo">
	<cfelseif Datediff('d', request.requesttime, getpromotion.startdate) GT 0>
	<!--- user problem if the promotion has not yet started --->
		<cfset errorPhrase ="phr_InstantReward_PromotionNotStrated">
	<cfelseif Datediff('d', request.requesttime, getpromotion.Enddate) LT 0>
		<!--- user problem if the promotion has expired --->
		<cfset errorPhrase ="phr_InstantReward_PromotionHasEnded">
	</cfif>
</cfif>

<cf_translate>
<cfform action="#CGI.SCRIPT_NAME#?#request.query_string#" method="POST" name="details" >

<cf_relayFormDisplay>
	<cfif not isdefined("errorPhrase")>
		<CF_relayFormElementDisplay relayFormElementType="html" spanCols="yes" valueAlign="left" fieldname="" label="" currentValue="<strong>phr_InstantReward_InstantStartMessage</strong>">
		<CF_relayFormElementDisplay relayFormElementType="text" fieldName="CodeEntered" currentValue="" label="phr_InstantReward_enter_promotion_code" size="50" maxlength="100" tabindex="1" required="yes">
		<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="SubmitCode" currentValue="phr_InstantReward_submit_Code" label="" spanCols="No" valueAlign="left" class="button">
		<cfif isdefined("Successphrase")>
			<CF_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="<strong>#htmleditformat(Variables.totalPointsClaimed)# #htmleditformat(Successphrase)#</strong>">
		</cfif>
		<cfif isdefined("errorCodephrase")>
			<CF_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="<strong>#htmleditformat(errorCodephrase)#</strong>">
		</cfif>
	<cfelse>
		<CF_relayFormElementDisplay relayFormElementType="message" fieldname="" label="" currentValue="<strong>#htmleditformat(errorphrase)#</strong>">
	</cfif>
</cf_relayFormDisplay>
phr_InstantReward_cardimg
</cfform>
</cf_translate>
