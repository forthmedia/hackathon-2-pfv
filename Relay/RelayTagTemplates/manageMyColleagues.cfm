<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			manageMyColleagues.cfm
Author:				SWJ
Date started:		2003-04-03

Description:		This is used to provide a page for managing a list of records

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2003-05-08	SWJ	I added phone and mobile to the editable field list.  To add a new field simply
			- add it to the query get people
			- add it to the variable personfieldsonpage (line 162?)
			- insert a text control and hidden as per the main block further down the page.
			- if you want a label you will need to add it to the translation fields

Possible enhancements:


 --->
<!--- <CFPARAM name="frmlanguageid" default="#iif(isdefined('cookie.defaultlanguageid'),DE(cookie.defaultlanguageid),DE('1'))#"> --->
<!--- 2009/06/16 GCC LID2367 ensure known user and logged in--->
<cfif not request.relayCurrentUser.isUnknownUser and request.relayCurrentUser.isLoggedIn>


<cfif isDefined("doUpdate") and doUpdate eq "yes">
	<cfinclude template="/screen/updatedata.cfm">
</cfif>

<CFPARAM name="frmLanguageID" default="1">
<CFSET listofvalidjobfunctions="MainContact,ManagingDirector,FinancialManager,marketingManager,SalesContact,ServiceContact,PurchaseContact">

<!--- locid must be passed to this template --->
<!--- 2009/06/16 GCC LID2367 always make sure only done for current users organisation --->
<cfset orgID = request.relayCurrentUser.organisationID>
<!--- <cfif not isDefined("orgID")>
	<CFQUERY NAME="GetThisPersonsOrgID" datasource="#application.siteDataSource#">
		Select organisationID from person where personid=#request.relayCurrentUser.personid#
	</CFQUERY>
	<cfset orgID = GetThisPersonsOrgID.organisationID>
</cfif> --->

<!--- ==============================================================================
      CHECK THE NUMBER OF LOCATIONS FOR THIS ORGID
=============================================================================== --->
<CFQUERY NAME="getLocsForThisOrg" datasource="#application.siteDataSource#">
	select locationID, sitename+' ('+address4+' '+postalcode+')' as locdetails
		from location where organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>


<cfif getLocsForThisOrg.recordCount gt 1>
	<!--- if there are more that one location for this org put a drop down up
			so the user can select from the list --->
	<cfif not isDefined("form.frmLocID")>
		<cfset form.frmLocid = getLocsForThisOrg.locationID[1]>
	</cfif>
	<cfform method="post" name="locForm">
		<p style="font-weight: bold;">Select which site</p>
		<!--- the screen will re-fresh when the user selects from the drop down --->
		<cfselect name="frmLocID" query="getLocsforThisOrg" value="locationID" display="locdetails" selected="#form.frmLociD#" onChange="document.locForm.submit()">	</cfselect>
	</cfform>
<cfelse>
	<cfset form.frmLocid = getLocsForThisOrg.locationID>
</cfif>

<!--- locid must be passed to this template --->
<cfif not isDefined("form.frmLocID")>
	<p>You must define form.frmLocID to use this template</p>
	<cfexit method="EXITTEMPLATE">
</cfif>

<CFPARAM name="locationid" default="#frmLocID#">
<!--- get details of location--->
<CFQUERY NAME="getLocation" datasource="#application.siteDataSource#">
	select * from location where locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFPARAM NAME="Locname" DEFAULT="#GetLocation.sitename#">


<!--- get list of possible languages--->
<CFQUERY NAME="GetLangList" datasource="#application.siteDataSource#">
	select language from language order by language
</CFQUERY>

<!--- get name of current language --->
<CFQUERY NAME="GetLanguage" datasource="#application.siteDataSource#">
	SELECT language from language where LanguageID =  <cf_queryparam value="#frmlanguageID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>


<!--- get list of people at this location --->
<CFQUERY NAME="GetPeople" datasource="#application.siteDataSource#">
	Select p.personid, p.firstname, p.lastname, p.email, p.salutation, p.lastupdated, p.officePhone,p.mobilephone
	from person as p
	where p.locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
	and active = 1
	order by lastname
</CFQUERY>

<CFQUERY NAME="getalljobfunctions" datasource="#application.siteDataSource#">
	select f.name, f.flagid, f.namephrasetextid
	from flag as f, flaggroup as fg
	where f.flaggroupid= fg.flaggroupid
	and fg.flaggrouptextid='JobFunction'
	and f.active = 1
	and fg.active = 1
	and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	order by f.name
</CFQUERY>


<CFSET addoredit=getpeople.recordcount is 0?"phr_addpeoplefor":"phr_editaddpeoplefor">

<SCRIPT type="text/javascript">

<!--
	function verifyForm() {
		var form = document.frmmain;
		var msg = "";

		var userchecked = false
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmUser') {
					if (thiselement.checked==true)  {
						userchecked = true
								}
			}
		}

		if (msg != "") {
				alert("\n<CFOUTPUT>phr_Pleaseprovidethefollowinginformation</CFOUTPUT>:\n\n" + msg);
		} else {
			form.submit();
		}
	}
//-->

</SCRIPT>


<CFSET personfieldsonpage="Person_Email,Person_FirstName,Person_LastName,Person_Salutation,Person_OfficePhone,person_MobilePhone">
<CFSET personidsonpage="">

<CFOUTPUT>
<FORM METHOD="POST" NAME="frmmain">

<CF_INPUT TYPE="Hidden" NAME="LocationID" VALUE="#LocationID#">
<CF_INPUT TYPE="Hidden" NAME="frmLocationID" VALUE="#LocationID#">
<INPUT TYPE="Hidden" NAME="doUpdate" VALUE="yes">
<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#createODBCDateTime(now())#">
<CFIF isdefined("frmProcessID")>
<CF_INPUT TYPE="HIDDEN" NAME="frmProcessID" VALUE="#frmProcessID#">
<CF_INPUT TYPE="HIDDEN" NAME="frmStepID" VALUE="#NextStepID#">
</cfif>


<TABLE>
	<TR><td colspan="3"><H3>#htmleditformat(Locname)#: #htmleditformat(AddorEdit)# </H3></td></TR>
</TABLE>
		<CFIF isdefined("email")><CF_INPUT TYPE="Hidden" NAME="email" VALUE="#email#"></CFIF>

<table border="0" cellspacing="3" cellpadding="0" class="withBorder">
	<CFIF getpeople.recordcount is not 0>
	<TR><td colspan="3"><B>phr_editthefollowingpeople:</b></td></tr>
	</cfif>

	<CFLOOP query="getpeople">
		<CFSET personidsonpage=listappend(personidsonpage,personid)>

		<!--- get list of all job functions, with field showing if this person has this job function. ordered with selected job functions first--->
		<CFQUERY NAME="getalljobfunctionsordered" datasource="#application.siteDataSource#">
			select f.name, f.flagid, f.namephrasetextid,
			case when exists (select * from booleanflagdata as bfd where entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >  and bfd.flagid=f.flagid) THEN 1 ELSE 0 END	as selected
			from flag as f, flaggroup as fg
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by selected DESC, f.orderingindex
		</CFQUERY>


		<!--- list of job function flags for this person  --->
		<CFQUERY NAME="getentityjobfunctions" datasource="#application.siteDataSource#">
			select f.flagid
			from flag as f, flaggroup as fg, booleanflagdata as bfd
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and bfd.flagid=f.flagid
			and bfd.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by f.orderingindex
		</cfquery>

		<TR>
			<CF_INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">

			<td valign="top" nowrap>
				<table>
					<tr>
						<td><strong>phr_salutation</strong></td>  <td><strong>phr_firstname</strong></td>
						<td><strong>phr_lastname</strong></td><br/>
					</tr>
						<tr>
						<td>
							<CF_INPUT TYPE="Text"  name="Person_Salutation_#Personid#" value="#trim(salutation)#" size="10" class="narrow">
							<CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#">
						</td>
						<td>
			 				<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_FirstName_#Personid#" SIZE="15" VALUE="#trim(firstname)#">
							<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="#trim(firstname)#">
						</td>
						<td>
							<CF_INPUT TYPE="Text"  NAME="Person_LastName_#Personid#" SIZE="15" VALUE="#trim(lastname)#">
							<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="#trim(Lastname)#">
						</td>
					</tr>

					<tr><td><strong>phr_officePhone</strong></td><td colspan="2"><strong>phr_MobilePhone</strong></td></tr>
					<tr>
						<td>
							<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_OfficePhone_#Personid#" SIZE="15" VALUE="#trim(officePhone)#">
							<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" VALUE="#trim(officePhone)#">
						</td>

						<td>
							<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_MobilePhone_#Personid#" SIZE="15" VALUE="#trim(mobilePhone)#">
							<CF_INPUT TYPE="Hidden" NAME="Person_MobilePhone_#Personid#_orig" VALUE="#trim(mobilePhone)#">
						</td>
					</tr>
					<tr><td colspan="3"><strong>phr_emailAddress*</strong></td></tr>
					<tr>
						<td colspan="3">
							<CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" SIZE="30" VALUE="#trim(email)#">
							<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="#trim(email)#">
						</td>
					</tr>
				</table>
			</TD>

			<TD>
			<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=#max(getentityjobfunctions.recordcount+1,3)#>
				<OPTION VALUE="0" #IIF(getentityjobfunctions.recordcount is 0, DE(" SELECTED"), DE(""))#>phr_NoJobFunction</OPTION>
				<CFloop QUERY="getalljobfunctionsordered">
					<OPTION VALUE="#Flagid#" #IIF(listfindnocase(valuelist(getentityjobfunctions.flagid),flagid) is not 0, DE(" SELECTED"), DE(""))#>#HTMLEditFormat("phr_#htmleditformat(namephrasetextid)#")#</OPTION>
				</CFloop>
			</SELECT>
			</TD>

			<INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="#IIF(getentityjobfunctions.recordcount is not 0, DE("#valuelist(getentityjobfunctions.flagid)#"), DE("0") )#">

		</TR>

		<tr>
			<td colspan="2"><hr width="100%" size="1"></td>
		</tr>
	</cfloop>
	</table>
<!--- ==============================================================================
       ADD PERSON SECTION
=============================================================================== --->

	<table border="0" cellspacing="3" cellpadding="0" class="withBorder">
	<CFSET firstname="">
	<CFSET lastname="">
	<CFSET email="">
	<CFSET salutation="">
	<CFSET lastupdated="">
	<TR><td colspan="3"><B>phr_YouMayAddNewPeopleBelow:</b></td></tr>

	<TR>
		<TD>phr_language:</TD>

		<td colspan="3">
			<SELECT NAME="insLanguage">
				<cfloop QUERY="GetLangList">
				<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
				</cfloop>
			</SELECT>
		</td>
	</TR>


<!--- 	<CFLOOP index="I" from="1" to="2"> --->
	<CFSET personid="new">
	<CFSET personidsonpage=listappend(personidsonpage,personid)>
	<CF_INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">
	<CF_INPUT TYPE="HIDDEN" NAME="person_locationid_#personid#" VALUE="#locationid#">			<!--- this tells the task which location the person is to be added to --->
<!--- 	<TR><TD WIDTH="85">&nbsp;</TD>
		<TD>#phr_language#:</TD>
		<TD>
			<SELECT NAME="insLanguage">
				<cfloop QUERY="GetLangList">
					<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
				</cfloop>
			</SELECT>
		</TD>
	</TR>
 --->
	<tr>
		<td valign="top" nowrap>
				<table>
					<tr>
						<td><strong>phr_salutation</strong></td>
						<td><strong>phr_firstname</strong></td>
						<td><strong>phr_lastname</strong></td><br/>
					</tr>
						<tr>
						<td>
							<CF_INPUT TYPE="Text" name="Person_Salutation_#Personid#" size="10" class="narrow">
							<CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig">
						</td>
						<td>
			 				<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_FirstName_#Personid#" SIZE="15">
							<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig">
						</td>
						<td>
							<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_LastName_#Personid#" SIZE="15">
							<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig">
						</td>
					</tr>

					<tr><td><strong>phr_officePhone</strong></td><td colspan="2"><strong>phr_MobilePhone</strong></td></tr>
					<tr>
						<td>
							<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_OfficePhone_#Personid#" SIZE="15">
							<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig">
						</td>

						<td>
							<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_MobilePhone_#Personid#" SIZE="15">
							<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig">
						</td>
					</tr>
					<tr><td colspan="3"><strong>phr_emailAddress*</strong></td></tr>
					<tr>
						<td colspan="3">
							<CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" SIZE="30">
							<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig">
						</td>
					</tr>
				</table>
		</TD>
<!--- 		<INPUT TYPE="Text"  class="narrow" name="Person_Salutation_#Personid#" size="10" >
			<INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="">
			<INPUT TYPE="Text"  class="narrow" NAME="Person_FirstName_#Personid#" SIZE="15">
			<INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="">

			<INPUT TYPE="Text" class="narrow" NAME="Person_LastName_#Personid#" SIZE="15">
			<INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="">
			<br>
			<input type="text" name="Person_OfficePhone_#Personid#" size="15" class="narrow" title="Direct Line">
			<INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" VALUE="">

			<INPUT TYPE="Text"  class="narrow" NAME="Person_MobilePhone_#Personid#" SIZE="15" VALUE="">
			<INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" VALUE="">

			<br>#phr_emailAddress#* <INPUT TYPE="Text" NAME="Person_email_#Personid#" VALUE="">
			<INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="">
 --->


		<TD>
			<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=3>
				<OPTION VALUE="0" SELECTED>phr_NoJobFunction</OPTION>
				<cfloop QUERY="getalljobfunctions">
					<OPTION VALUE="#Flagid#" >#HTMLEditFormat("phr_#htmleditformat(namephrasetextid)#")#</OPTION>
				</cfloop>
			</SELECT>
			<CF_INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="0">
		</TD>

	</TR>

	<TR>
		<td colspan="3">* phr_RequiredFields
		</td>
	</TR>


	<TR>

		<td colspan="3">
			<input type="button" value="phr_save" onClick="javascript:verifyForm()">
		</td>
	</TR>
</table>

<!--- ==============================================================================
      MANDATORY HIDDENS USED BY UPDATEDATE.CFM
=============================================================================== --->
<INPUT TYPE="Hidden" NAME="frmtablelist" VALUE="person">
<CF_INPUT TYPE="Hidden" NAME="frmpersonfieldlist" VALUE="#personfieldsonpage#">
<CF_INPUT TYPE="Hidden" NAME="frmpersonidlist" VALUE="#personidsonpage#">
<INPUT TYPE="Hidden" NAME="frmpersonflaglist" VALUE="checkbox_jobfunction">


</FORM>

</cfoutput>
</cfif>