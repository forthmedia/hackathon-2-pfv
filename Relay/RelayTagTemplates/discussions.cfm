<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			discussions.cfm
Author:				IH
Date started:		2013-01-24

Purpose:	Add discussions to any pages.

Usage:	Only use as a realaytag

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2013-05-16			YMA			Case 434412 Added more button and limited initial view of how much text shows.
2014-02-04			YMA			Case 435798 re-write to use scrolling pagination
2015-06-25			WAB			Removed undocumented change of 2015-05-21 GCC which added a variable request.discussionAdmin and then referenced it in webservices.discussions.cfc
								This would not work when the webservice called by ajax.
								Replaced with a function in the cfc

Possible enhancements:
JavaScript block should be moved to an external file.
External ext2attr file should be moved to an external file.
--->


<cfsetting enablecfoutputonly="true">

<!--- <cf_htmlhead text='<link rel="stylesheet" href="/code/styles/discussions.css" />'>
<cf_htmlhead text='<link rel="stylesheet" href="/javascript/css/jquery.jscrollpane.css" />'> --->

<cf_includeCssOnce template="/code/styles/discussions.css">
<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery.jscrollpane.css">

<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.expander.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscroll.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.mousewheel.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery.jscrollpane.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/scroll-startstop.events.jquery.js">
<cf_includejavascriptOnce template="/javascript/lib/scrollbar.js">
<cf_includejavascriptOnce template="/javascript/discussions.js">

<cf_head>
<cfoutput>
	<script>
		function setExpander(level) {
			jQuery(level).expander({
			    slicePoint:       100,  // default is 100
			    expandPrefix:     ' ...', // default is '... '
			    expandText:       'phr_social_more', // default is 'read more'
			    collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
			    widow: 4,
			    userCollapseText: 'phr_social_less',  // default is 'read less'
			    preserveWords:true
			  });
		}
		// you can override default options globally, so they apply to every .expander() call
		//jQuery.expander.defaults.slicePoint = 120;

		var fancyBoxInitiated = false;
		jQuery(document).ready(function() {
			if (jQuery(window).width() > 767) {
				jQuery('##discussions').addClass('discussions');
				jQuery('##discussionGroups').addClass('discussionGroups');
				jQuery('##discussionComments').addClass('discussionComments');
				jQuery('##discussionMessages').addClass('discussionMessages');

			}else{
				intiateFancyBox()
				fancyBoxInitiated = true;
			}
			setExpander('div.expandable div.expanding');

			jQuery(window).resize(function(){
				if(jQuery(window).width() <= 767 && !fancyBoxInitiated ){
					intiateFancyBox()
					fancyBoxInitiated = true;
				}
			});
		});

		intiateFancyBox = function () {
			jQuery(".addCommentMessage").fancybox({
			   helpers: {
		         overlay: {
		            locked: true
		         }
		      },
				autoSize: false,
				width: '90%',
				height: '90%',
				closeEffect : 'none',
				closeSpeed: 'fast',
				beforeLoad: function(){
					jQuery('##addMessage').toggleClass('hidden-xs')
				},
				afterClose: function(){
					jQuery('##addMessage').toggleClass('hidden-xs')
				}
			});
			jQuery(".openSearchBox").fancybox({
			   helpers: {
		         overlay: {
		            locked: true
		         }
		      },
				autoSize: true,
				width: 'auto',
				height: 'auto',
				closeEffect : 'none',
				closeSpeed: 'fast',
				beforeLoad: function(){
					jQuery('##searchBox').toggleClass('hidden-xs')
				},
				afterClose: function(){
					jQuery('##searchBox').toggleClass('hidden-xs')
				}
			});
		}
	</script>
</cfoutput>
</cf_head>

<cf_param name="attributes.discussionGroupIDs" type="string" label="Communities" size="5" multiple="true" default="" validValues="select discussionGroupID as value, title as display from discussionGroup order by title" />
<cf_param name="attributes.recordsPerPage" type="integer" label="Records per page" default="5"/>
<cfparam name="attributes.showVote" default="false" />

<!--- ext2attr --->
<cfif not structKeyExists(variables,"attributes")>
	<cfset attributes = {}>
</cfif>
<cfloop collection="#form#" item="i">
	<cfif not structKeyExists(attributes,i)>
		<cfset attributes[i]=trim(form[i])>
	</cfif>
</cfloop>
<cfloop collection="#url#" item="i">
	<cfif not structKeyExists(attributes,i)>
		<cfset attributes[i]=trim(url[i])>
	</cfif>
</cfloop>
<!--- END ext2attr --->

<cfif isNumeric(attributes.discussionGroupIDs)><cfset attributes.discussionGroupID=attributes.discussionGroupIDs></cfif>

<cfparam name="attributes.action" default="">
<cfparam name="attributes.discussion" default="">
<cfparam name="attributes.callmethod" default="">
<cfparam name="attributes.title" default="">
<cfparam name="attributes.discussionMessageID" default="">
<cfparam name="attributes.discussionGroupID" default="">
<cfparam name="attributes.entityCommentID" default="">
<cfparam name="attributes.searchString" default="">

<cfset addMessageOnPageEdit=false>
<cfset addCommentOnPageEdit=false>

<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.autosize-min.js" />
<cf_includeJavascriptOnce template="/javascript/social.js"  translate="true"/>

<cfswitch expression="#attributes.callmethod#">

	<cfcase value="saveDiscussionMessage">
		<cfinvoke component="webservices.discussions" method="saveDiscussionMessage" returnVariable="returnVar" argumentCollection="#attributes#"></cfinvoke>
	</cfcase>

	<cfcase value="saveDiscussionComment">
		<cfset attributes.entityTypeID = application.entityTypeID.discussionMessage>
		<cfset attributes.entityID = attributes.discussionMessageID>
		<cfinvoke component="webservices.socialWS" method="saveEntityComment" returnVariable="returnVar" argumentCollection="#attributes#"></cfinvoke>
		<cfset successMessage = "Comment added.">
	</cfcase>

	<cfcase value="deleteDiscussionMessage">
		<cfinvoke component="webservices.discussions" method="deleteDiscussionMessage" returnVariable="deleted" discussionMessageID="#attributes.discussionMessageID#"></cfinvoke>
		<cfif deleted>
			<cfset attributes.discussionMessageID = "">
			<cfset successMessage = "phr_discussion_message_deleted">
		<cfelse>
			<cfset errorMessage = "phr_discussion_message_delete_error">
		</cfif>
	</cfcase>

	<cfcase value="deleteEntityComment">
		<cfinvoke component="webservices.socialWS" method="deleteEntityComment" returnVariable="deleted" entityCommentID="#attributes.entityCommentID#"></cfinvoke>
		<cfif deleted>
			<cfset successMessage = "phr_discussion_comment_deleted">
		<cfelse>
			<cfset errorMessage = "phr_discussion_comment_delete_error">
		</cfif>
	</cfcase>

</cfswitch>


<cfoutput><div id="discussions"></cfoutput>

<cfsavecontent variable="searchBox">
	<cfoutput>
		<form action="#cgi.script_name#">
			<input type="hidden" name="eid" value="#htmlEditFormat(attributes.eid)#">
			<input type="hidden" name="discussionGroupID" value="#htmlEditFormat(attributes.discussionGroupID)#">
			<input type="hidden" name="discussionMessageID" value="#htmlEditFormat(attributes.discussionMessageID)#">
			<div id="searchbar">
				<input type="text" size="22" maxlength="255" name="searchString" onblur="this.value=(this.value=='') ? 'phr_discussion_search' : this.value;" onfocus="this.value=(this.value=='phr_discussion_search') ? '' : this.value;" value="#htmlEditFormat(iif(attributes.searchString neq '',de(attributes.searchString),de('phr_discussion_search')))#">
			</div>
		</form>
	</cfoutput>
</cfsavecontent>


<cfif isNumeric(attributes.discussionGroupID)>
	<cfinvoke component="webservices.discussions" method="getDiscussionGroup" returnVariable="qGroup" discussionGroupIDs="#attributes.discussionGroupID#" cacheQuery="false"></cfinvoke>
	<cfoutput>
		<div class="groupheader">
			<h4 class="col-xs-7 pull-left">
				<cfif isNumeric(attributes.discussionMessageID)><a href="#cgi.script_name#?eid=#attributes.eid#&discussionGroupID=#attributes.discussionGroupID#" class="inherit">#htmlEditFormat(qGroup.recordset.title)#</a><cfelse>#htmlEditFormat(qGroup.recordset.title)#</cfif>
			</h4>
			<span id="searchBox"class="text-right searchBox pull-right hidden-xs col-xs-12 col-sm-5">#variables.searchBox#</span>
			<div class="<cfif not isNumeric(attributes.discussionGroupIDs)>col-xs-5<cfelse>col-xs-12</cfif> text-right visible-xs">
				<h4>
					<A href="##searchBox" class="openSearchBox visible-xs"><span class="glyphicon glyphicon-search"></span></a>
					<A href="##addMessage" class="addCommentMessage visible-xs"><span class="glyphicon glyphicon-edit"></span></a>
				</h4>
			</div>
			<cfif not isNumeric(attributes.discussionGroupIDs)><span class="col-xs-7"><a href="#cgi.script_name#?eid=#attributes.eid#">phr_discussion_view_all</a></span></cfif>

		</div>
	</cfoutput>
<cfelse>
	<cfoutput>
		<div class="groupheader">
			<h4 class="col-xs-7 pull-left">phr_discussion_label</h4>
			<div class="col-xs-5 text-right visible-xs">
				<h4>
					<A href="##searchBox" class="openSearchBox visible-xs"><span class="glyphicon glyphicon-search"></span></a>
				</h4>
			</div>
			<span id="searchBox" class="text-right searchBox pull-right hidden-xs col-xs-12 col-sm-5">#variables.searchBox#</span>
		</div>
		<div id="discussionGroups">
			<img id="discussionGroupsData" src="/images/icons/loading.gif" alt="Loading"/>
		</div>
		<script>
			showDiscussionGroups(false,'#jsStringFormat(attributes.searchString)#',0,#attributes.recordsPerPage#,#attributes.eid#,'#attributes.discussionGroupIDs#');
		</script>
	</cfoutput>
</cfif>


<cfif isNumeric(attributes.discussionGroupID)>
	<cfif structKeyExists(variables,"successMessage")>
		<cfoutput>#application.com.relayui.message(variables.successMessage)#</cfoutput>
	<cfelseif structKeyExists(variables,"errorMessage")>
		<cfoutput>#application.com.relayui.message(variables.errorMessage,'error')#</cfoutput>
	</cfif>
	<cfif isNumeric(attributes.discussionMessageID)>
		<cfset thisSearchString = "">
	<cfelse>
		<cfset thisSearchString = attributes.searchString>
	</cfif>
	<cfoutput>
		<div id="discussionMessages<cfif isNumeric(attributes.discussionMessageID)>InComments</cfif>">
			<img id="discussionMessagesData" src="/images/icons/loading.gif" alt="Loading"/>
		</div>
		<script>
			showDiscussionMessages(false,'#jsStringFormat(attributes.searchString)#',0,#attributes.recordsPerPage#,#attributes.eid#,'#attributes.discussionGroupID#','#attributes.discussionMessageID#',#attributes.showVote#);
		</script>
	</cfoutput>
</cfif>


<cfif isNumeric(attributes.discussionMessageID)>
	<cfoutput>
		<div id="discussionComments">
			<img id="discussionCommentsData" src="/images/icons/loading.gif" alt="Loading"/>
		</div>
		<script>
			showDiscussionComments(false,'#jsStringFormat(attributes.searchString)#',0,#attributes.recordsPerPage#,#attributes.eid#,'#attributes.discussionMessageID#','#attributes.discussionGroupID#');
		</script>
		<a name="commentForm"></a>
		<div id="addMessage" class="comment hidden-xs">
			<div class="photo hidden-xs">
				<img src="<cfif request.relayCurrentUser.person.pictureURL is not "">#request.relayCurrentUser.person.pictureURL#<cfelse>/images/social/icon_no_photo_80x80.png</cfif>" width="80" height="80" onerror="this.onerror=null;this.src='/images/social/icon_no_photo_80x80.png'">
			</div>
			<div class="nameandcompany hidden-xs"><div class="name">#htmlEditFormat(request.relayCurrentUser.fullname)#</div><div class="company">#htmlEditFormat(request.relayCurrentUser.organisation.organisationName)#</div></div>
			<div class="body">
				<form action="" method="post" onsubmit="return checkCommentForm(this)">

					<input type="hidden" name="discussionMessageID" value="#htmlEditFormat(attributes.discussionMessageID)#">
					<input type="hidden" name="callmethod" value="saveDiscussionComment">
					<input type="hidden" name="searchString" value="">
					<div class="form-group">
						<textarea name="comment" id="discussionComment" class="inherit form-control" onfocus="this.value=(this.value=='phr_discussion_enter_comment') ? '' : this.value;" onblur="this.value=(this.value=='') ? 'phr_discussion_enter_comment' : this.value;">phr_discussion_enter_comment</textarea>
					</div>
					<div class="form-group">
						<input type="submit" value="phr_discussion_add_comment" class="button btn btn-primary">
					</div>
				</form>
			</div>
		</div>
	</cfoutput>

<cfelseif isNumeric(attributes.discussionGroupID) and attributes.discussionGroupID gt 0>
	<cfoutput>
	<cfif structKeyExists(variables,"qGetMessage") and isQuery(qGetMessage) and qGetMessage.recordCount><div class="separator">phr_discussion_start</div></cfif>
	<div id="addMessage" class="message hidden-xs" style="height:auto">
		<div class="photo hidden-xs">
			<img src="<cfif request.relayCurrentUser.person.pictureURL is not "">#request.relayCurrentUser.person.pictureURL#<cfelse>/images/social/icon_no_photo_80x80.png</cfif>" width="80" height="80" onerror="this.onerror=null;this.src='/images/social/icon_no_photo_80x80.png'">
		</div>
		<div class="nameandcompany hidden-xs"><div class="name">#htmlEditFormat(request.relayCurrentUser.fullname)#</div><div class="company">#htmlEditFormat(request.relayCurrentUser.organisation.organisationName)#</div></div>
		<div class="body">
			<form action="" method="post" onsubmit="return checkMessageForm(this)">
				<input type="hidden" name="callmethod" value="saveDiscussionMessage">
				<input type="hidden" name="searchString" value="">
				<div class="form-group">
					<input class="inherit form-control" type="text" name="title" maxlength="255" value="phr_discussion_enter_message_title" onfocus="this.value=(this.value=='phr_discussion_enter_message_title') ? '' : this.value;" onblur="this.value=(this.value=='') ? 'phr_discussion_enter_message_title' : this.value;">
				</div>
				<div class="form-group">
					<textarea data-role="none" class="inherit form-control" name="message" onfocus="this.value=(this.value=='phr_discussion_enter_message') ? '' : this.value;" onblur="this.value=(this.value=='') ? 'phr_discussion_enter_message' : this.value;">phr_discussion_enter_message</textarea>
				</div>
				<div class="form-group">
					<input type="submit" value="phr_discussion_start_discussion" class="button btn btn-primary">
				</div>
			</form>
		</div>
	</div>
	</cfoutput>
</cfif>

<cfoutput></div></cfoutput>

<cffunction name="getFormattedDate" access="private">
	<cfargument name="date" required="true">
	<cfset var diff = dateDiff("m",now(),date)>
	<cfset var formattedDate = "">
	<cfif diff lt 60>
		<cfset formattedDate = "#diff# mins ago">
	<cfelseif diff lt 24*60>
		<cfset formattedDate = "#int(diff/60)# hour#iif(diff gte 120,de('s'),'')# ago">
	<cfelse>
		<cfset formattedDate = "#int(diff/60*24)# day#iif(diff gte 60*48,de('s'),'')# ago">
	</cfif>
</cffunction>

<cfsavecontent variable="jsBlock">
<cfoutput>
<!--- This JS block should be moved to an external file. The one ColdFusion variable (attributes.discussionGroupID) it uses should be set in the "discussions" div using HTML5 syntax (data-discussionGroupID) --->
<script language="javascript" type="text/javascript">

	function checkCommentForm(form){
		if(trim(form.comment.value) == '' || trim(form.comment.value) == 'phr_discussion_enter_comment'){
			alert('Please enter your comment.');
			return false;
		}
		return true
	}

	function checkMessageForm(form){
		if(trim(form.title.value) == '' || trim(form.title.value) == 'Enter message title...'){
			alert('Please enter message title.');
			return false;
		}
		if(trim(form.message.value) == '' || trim(form.message.value) == 'Enter message...'){
			alert('Please enter message.');
			return false;
		}
		return true;
	}

	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}

	jQuery(document).ready(function (){
			jQuery('##discussions textarea').autosize({append: "\n"});
		}
	);

	function deleteComment(entityCommentID){
		if(confirm('phr_discussion_delete_conf_comment')) {
			jQuery('<form/>', {'method': 'post', 'target': '_top', 'id': 'deleteForm'})
			.append(jQuery('<input>', {'name': 'entityCommentID', 'value': entityCommentID, 'type': 'hidden'}))
			.append(jQuery('<input>', {'name': 'callmethod', 'value': 'deleteEntityComment', 'type': 'hidden'}))
			.append(jQuery('<input>', {'name': '_rwsessiontoken', 'value': _rw.sessiontoken, 'type': 'hidden'}))
			.appendTo('body').submit();
		}
		return false;
	}

	function deleteMessage(discussionMessageID){
		if(confirm('phr_discussion_delete_conf_message')) {
			jQuery('<form/>', {'method': 'post', 'target': '_top', 'id': 'deleteForm'})
			.append(jQuery('<input>', {'name': 'discussionMessageID', 'value': discussionMessageID, 'type': 'hidden'}))
			.append(jQuery('<input>', {'name': 'callmethod', 'value': 'deleteDiscussionMessage', 'type': 'hidden'}))
			.append(jQuery('<input>', {'name': '_rwsessiontoken', 'value': _rw.sessiontoken, 'type': 'hidden'}))
			.appendTo('body').submit();
		}
		return false;
	}

</script>
</cfoutput>
</cfsavecontent>
<cf_htmlhead text="#jsBlock#">




