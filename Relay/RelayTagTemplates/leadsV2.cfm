<cf_param name="leadTypeID" default="-1" label="Lead Type" validValues="func:com.relayForms.getLookupList(fieldname='leadTypeID',excludeTextID='distributedLead,partner')" display="translatedItemText" value="LookupID" />
<cf_param name="showTheseColumns" label="Show These Columns" multiple="true" displayAs="twoSelects" allowSort="true" type="string" validValues="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created" default="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created"/>
<cf_param name="filterColumns" default="Country,leadApprovalStatus" validValues="Country,LeadApprovalStatus" multiple="true" />
<cf_param name="showOpportunityOnConversion" description="Show Opportunity On Conversion (if portal conversion is active)" type="boolean" default="true"/>

<cfparam name="variables.showAddButton" default="true"> <!--- set in 'show company leads' --->
<cfset variables.returnUrl = "/?eid=#request.currentElement.id#">

<cfif showOpportunityOnConversion>
	<cfif structKeyExists(url,"opportunityViewToken")>
		<cfscript>
			//we've recieved a token to view an opportunity (that was just converted by webservice). We redirect to view it
			//This has to be a 2 stage process to get the encrption right
			token=new com.tokens.token(tokenTypeTextID="opportunityViewToken",token=url.opportunityViewToken);
			token.pullFromDatabase();

			if(token.isValidToken()) {
				location(application.com.security.encryptURL('#variables.returnURL#&frmTask=edit&opportunityid=#token.get("opportunityID")#'), false, 307);
			} else {
				location(variables.returnURL, false, 307);
			}
		</cfscript>

	<!--- include opp relay tag --->
	<cfelseif structKeyExists(url,"opportunityid")>
		<!---Displaying a lead that was just converted to an opportunity --->
		<cfinclude template="/RelayTagTemplates/showPartnerRepLeadsAndOpps.cfm">
	</cfif>
</cfif>

<!--- LIST/EDIT --->
<cfif structKeyExists(url,"leadID") and (url.leadId eq 0 or application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="leadId")) and application.com.rights.doesUserHaveRightsForEntity(entityType="lead",entityID=url.leadID,permission="edit")>
	<cfset application.com.leads.updatePartnerViewedDate(leadID=url.leadID,partnerSalesPersonId=request.relayCurrentUser.personID)>
<cfelse>
	<cfif variables.showAddButton>
		<cf_input type="button" value="phr_lead_Add" onClick="addLead();">
	</cfif>
	<cfset keyColumnOnCLickList = "javascript:editLead('##application.com.security.encryptVariableValue(name='leadID'*commavalue=leadID)##');return false">
</cfif>

<cf_include template="/relay/lead/leadsV2.cfm">