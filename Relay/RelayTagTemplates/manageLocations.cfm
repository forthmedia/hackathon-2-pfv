<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			manageLocations.cfm
Author:
Date started:

Description:

Amendment History:

Date 		Initials 	What was changed
2008-08-18	PPB			formatted the address in the dropdown box (done for NABU 970 so the address is the same format as other dropdowns on their bespoke Locations screen)
2010/04/12	NJH			P-PAN002 added the showEditLocBtn as a param
2012-06-27 	PPB 		Case 428374 'addnew' to phrase
2013-07-09 	NYB 		Case 435968
2015-01-23	ACPK		Case 442638 Modified query to exclude inactive location records
2016-03-10	WAB			Add class="" to relayformDisplay so shows stacked
Possible enhancements:
--->

<cf_param name="frmcurrentscreenid" default="newlocation"/>
<cf_param name="showEditLocBtn" type="boolean" default="yes"/>	<!--- NJH 2010/04/12 P-PAN002 --->

<cfif structKeyExists(form,"AddNewLocation") or structKeyExists(form,"EditExistingLocation")>
	<cfif structKeyExists(form,"AddNewLocation")>
	<!--- START: 2013-07-09 NYB Case 435968 added/edited --->
		<cfset headerText = "Sys_AddLocation">
		<cfset frmLocationID = "new">
	<cfelse>
		<cfset headerText = "Sys_EditLocation">
	</cfif>

	<cf_head>
	<cfoutput>
		<script>
			function refreshFormOnChange(newCountryField) {
				var formVar = document.getElementById("mainForm");
				var textFields = new Array();
				var origVals = new Array();
				var counter = 0;

				for (var i=0;i<formVar.elements.length;i++)
				  {
				  	if(formVar.elements[i].type == "text") {
				  		if(typeof formVar.elements[i].id != 'undefined') {


							textFields[counter] = formVar.elements[i].id;
							origVals[counter] = formVar.elements[i].value;
				  			counter = counter+1;
				  		}
				  	}
				  }

				page = '/webservices/relayWS.cfc?wsdl&method=refreshScreen&returnFormat=JSON&_cf_nodebug=true'
				parameters = 'frmcurrentscreenid=#frmcurrentscreenid#&frmLocationID=#frmLocationID#&LOCATION_ORGANISATIONID_DEFAULT=#url.LOCATION_ORGANISATIONID_DEFAULT#&location_countryid_default='+newCountryField.value +'&frmCountryID='+newCountryField.value
				div = 'locationFormDiv'
				var myAjax = new Ajax.Updater(
					div,
					page,
					{
						method: 'get',
						parameters: parameters ,
						evalJSON: 'force',
						debug : false,
						onComplete: function () {
							for (var j=0;j<textFields.length;j++)
							  {
								formEmt = document.getElementById(textFields[j]);
								if(typeof formEmt != 'undefined') {
									formEmt.value = origVals[j];
								}
							  }
				  		}

					});
			}
		</script>
			</cfoutput>
		</cf_head>

		<h2>phr_#headerText#</h2>
		<div id="locationFormDiv">
		<cfinclude template="/RelayTagTemplates/getrelayscreen.cfm">
		</div>
<cfelse>

	<!--- ==============================================================================
	      CHECK THE NUMBER OF LOCATIONS FOR THIS ORGID
	=============================================================================== --->


<!---ORIGINAL PPB:2008-08-18
	<CFQUERY NAME="getLocsForThisOrg" datasource="#application.siteDataSource#">
		select locationID, sitename+' ('+address1+', '+address4+' '+postalcode+')' as locdetails
			from location where organisationID = #request.relaycurrentuser.organisationid#
			order by sitename, address1, address4, postalcode
	</CFQUERY>

--->
	<!--- 2015-01-23	ACPK	Case 442638 Modified query to exclude inactive location records (active = 0) --->
	<CFQUERY NAME="getLocsForThisOrg" datasource="#application.siteDataSource#" >
		select locationID, sitename, address1, address4, address5, postalcode, Country.ISOCode as CountryCode, null as locdetails, location.CountryID
			from location INNER JOIN Country ON Location.CountryID = Country.CountryID
			where organisationID = #request.relaycurrentuser.organisationid#
			and active = 1
			order by sitename, address1, address4, postalcode
	</CFQUERY>

<!---
	Issue 970: PPB 2008-08-18
	rather than implementing awkward formatting of the address in sql, i loop the query result and format row by row and writing it back to the reserved query field "locdetails" for more-readable CF;

	equivalent address formatting SQL (from NABU valid values): ....., case when len(l.sitename) > 20 then left(l.sitename,20)+'..' else l.sitename end +' ('+ case when len(l.address1) > 15 then left(l.address1,15)+'..' else l.address1 end + ', ' + case when len(l.address4) > 10 then left(l.address4,10)+'..' else l.address4 end +', '+ case when len(l.address5) > 2 then left(l.address5,2)+'..' else l.address5 end  +' '+ case when len(l.postalcode) > 10 then left(l.postalcode,10)+'..' else l.postalcode end + ', '+left(c.isocode,2)+')' as displayValue .....
--->

	<cfloop query="getLocsForThisOrg">

		<cfif (len(sitename) gt 20) >
			<cfset formattedAddress = left(sitename,20) & "..">
		<cfelse>
			<cfset formattedAddress = sitename>
		</cfif>
		<cfset formattedAddress = formattedAddress & " ">		<!--- PPB 2008-10-13 issue 970 removed comma --->

		<cfif (len(address1) gt 15) >
			<cfset formattedAddress = formattedAddress & "(" & left(address1,15) & "..">
		<cfelse>
			<cfset formattedAddress = formattedAddress & "(" & address1>
		</cfif>
		<cfset formattedAddress = formattedAddress & ", ">

		<cfif (len(address4) gt 10) >
			<cfset formattedAddress = formattedAddress & left(address4,10) & "..">
			<cfelse>
			<cfset formattedAddress = formattedAddress & address4>
			</cfif>
			<cfset formattedAddress = formattedAddress & ", ">

		<cfif (len(address5) gt 2) >
			<cfset formattedAddress = formattedAddress & left(address5,2) & "..">
			<cfelse>
			<cfset formattedAddress = formattedAddress & address5>
		</cfif>
		<cfset formattedAddress = formattedAddress & " ">

		<cfif len(postalcode) gt 10>
			<cfset formattedAddress = formattedAddress & left(postalcode,10) & "..">
		<cfelse>
			<cfset formattedAddress = formattedAddress & postalcode>
		</cfif>
		<cfset formattedAddress = formattedAddress & ", ">

		<cfset formattedAddress = formattedAddress & left(CountryCode,2) & ")">

		<cfset getLocsForThisOrg.locdetails = formattedAddress>
	</cfloop>

	<form action="?frmcurrentscreenid=newlocation&eid=manageLocations&frmPersonID=0&location_organisationid_default=#request.relaycurrentuser.organisationid#&location_countryid_default=#request.relaycurrentuser.countryid#" method="POST" name="details" >
		<cf_relayFormDisplay class="">
			<!--- select a location --->
<!--- 		<cfquery name="getLocsForThisOrg" datasource="#application.siteDataSource#">
		select LocationID value, '' as display, * from location
			where location.organisationID = #request.relaycurrentuser.organisationid#
			order by sitename
	 	</cfquery>
		<cfloop query = "getLocsForThisOrg">
			<!--- get a formatted address and pop it into the query, ready to be used by the radio button displayer --->
			<cfset formattedAddress = application.com.screens.evaluateAddressFormat(location= getLocsForThisOrg,row=currentrow,separator="<BR>",finalcharacter="<BR>&nbsp;",showcountry=true)>
			<cfset querySetCell(getLocsForThisOrg,"display", formattedAddress, currentrow)>
		</cfloop>
			<CF_relayFormElement relayFormElementType="radio" currentValue="" fieldName="frmLocationID" label="location" query = "#getLocsForThisOrg#" display="display" value="value" required="yes" noteText="Please select a location"> --->
			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmLocationID" size="1" query="#getLocsForThisOrg#" display="locdetails" value="locationID" label="phr_MangeLocation_Address" required="Yes" nulltext="" nullValue="">

			<cfif showEditLocBtn>
				<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="EditExistingLocation" currentValue="phr_Sys_EditLocation" label="" spanCols="No" class="button">
			</cfif>
		</cf_relayFormDisplay>
	</form>
	<form action="?frmcurrentscreenid=newlocation&eid=manageLocations&frmPersonID=0&location_organisationid_default=#request.relaycurrentuser.organisationid#&location_countryid_default=#request.relaycurrentuser.countryid#" method="POST" name="details1" >
		<!---
		<A HREF="javascript:details1.submit();">phr_Sys_AddLocation</A>
		<input type="hidden" name="addnewlocation">
		--->
		<input type="hidden" name="frmLocationID" value="phr_sys_new">	<!--- 2012-06-27 PPB Case 428374 change from addnew to phrase --->
		<cf_relayFormDisplay class="">
			<CF_relayFormElementdisplay relayFormElementType="submit" fieldname="addnewlocation" currentValue="phr_Sys_AddLocation" label="" spanCols="yes" valueAlign="right" class="button">
		</cf_relayFormDisplay>
	</form>
</cfif>