<!--- �Relayware. All Rights Reserved 2014 --->
<!---

sendCommToFriend.cfm

Author:  SWJ

Date:  2000-12-22

Purpose
	to send a queued communication to a friend or colleague


Setting it up:

You need to add this tag to the element using the content editor.
Amendments

WAB 2005-05-10  Added some parameters so that text can be changed a bit
			  Bit rudimentary, but will get lifestyle going
			  Doesn't check whether email has actually been sent.  Could for example be an invalid email or unsubscribed and it wouldn't tell the user.  This could be fixed with a new version of tskcommsend which returns a structure

SSS 2007-02-22 	Added the ability to do a short form that will allow a user to send a invite friend email to a list of emails
				This function does not add anything to the database all it does is send a email.

NYB 2008-04-23 Sophos - if sending email from internal portal and MailToPersonID wasn't passed get suitable contact from orgID - Primary Contact, if 1 exists, else, the first person added
SSS 2009-05-12 Trend Bug 2143 released the sophos change
SSS 2009-06-03 LH 2302 we forgot that this template is also used on the extrnal portal change will set the persons organisation ID to the place you will invite to.
NJH 2009-07-17	P-FNL069 encrypt the hidden fields if on the portal. Also check for encrypted token if EmailTextID, c or frmCommID are passed in as url variables.
NAS 2009-10-30	LID - 2785 Make 'SEND' Translateable
NJH 2010-02-02 	LID 2906 - changed delimiter in email list from "," to ";"
2012/04/13	IH	Case 427058 formatted confirmation page
2012/12/17	NJH 2013 Roadmap - check email for blacklisting.
2013/04/11	YMA	Case 434160 Microfocus don't want this new OK button so we need to be able to hide it
 --->

<cfif not request.relaycurrentuser.isinternal>
	<cf_displayBorder
		noborders=true
	>
</cfif>

<cfparam name="Emailonly" default="false"><!--- only displays capture for email address and does not create person in the db --->
<cfparam name="EmailtoExistingPerson" default="false"><!--- if you wish to send a separate email to a person already in relay --->
<cfparam name="commType" default="comm"><!--- sends a comm or email --->
<cfparam name="EmailtextID" default="sendToFriendEmail">

<!--- NJH 2009-07-16 P-FNL069 in an effort to reduce spamming.. if the variables have come from the url scope, then check that they were encrypted
--->
<cf_checkFieldEncryption fieldNames = "c,frmCommID,emailTextID">

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cfparam name="MailToPersonID" default="#request.relayCurrentUser.Personid#">

<!--- START:  NYB 2008-04-23 Sophos added - if sending email from internal portal get orgID --->
<!---<cfparam name="PrimaryContactFlag" default="KeyContacts Primary"> NJH 2016/07/19 - removed as could not find it being used. --->

<cfif request.relayCurrentUser.isInternal and(isDefined("FRMCURRENTENTITYID") or isDefined("FRMENTITYID"))
		and isDefined("FRMENTITYTYPEID") and FRMENTITYTYPEID eq 2
		and MailToPersonID eq request.relayCurrentUser.Personid>
	<cfif isDefined("FRMCURRENTENTITYID")>
		<cfset orgID = FRMCURRENTENTITYID>
	<cfelse>
		<cfset orgID = FRMENTITYID>
	</cfif>
<!--- START LH 2302 SSS 2009-06-03 If the stuff above does not exist just set orgID to the persons organistaion --->
<cfelse>
	<cfset orgID = request.relaycurrentuser.ORGANISATIONID>
</cfif>
<!--- END LH 2302 SSS 2009-06-03 If the stuff above does not exist just set orgID to the persons organistaion --->
<!--- END:  NYB 2008-04-23 Sophos --->

<cfset request.relayFormDisplayStyle = "html-div">

<cfif commType neq "email">
	<cfif not isDefined("frmCommID")>
		<cfif not isDefined("c")>
				Phr_Ext_sendEmail_ErrorMessageComm
			<!--- <cfoutput>#c#</cfoutput> --->
			<CF_ABORT>
		<cfelse>
			<cfset frmCommID = c>
		</cfif>
	</cfif>
<cfelse>
	<cfif not isDefined("EmailtextID")>
		Phr_Ext_sendEmail_ErrorMessageEmailtext
		<CF_ABORT>
	</cfif>
	<cfset defexists = application.com.email.doesEmailDefExist(emailtextid=EmailtextID)>
	<cfif defexists EQ "false">
		Phr_Ext_sendEmail_def_error
		<CF_ABORT>
	</cfif>
</cfif>

<cfparam name = "IntroductionText" default = "Phr_SendCommToFriend_IntroductionText">
<cfparam name = "showFeedBack" default = "false">
<cfparam name = "ResultText" default = "Email has been sent">

<!--- ==============================================================================
      Process the sending only
=============================================================================== --->
<cfif isdefined("SendEmails") and commtype EQ "email">
	<!---Start 2009-07-08 p-fnl069 SSS took this out because we can make sure there are no duplicates in the list and change delim at the same time--->
	<!---<cfset newemails = ListChangeDelims(Emailaddress, ",",";")>--->
	<cfset newemails = application.com.globalFunctions.removeDuplicates(type="list",data=Emailaddress,delimiter=";")>
	<cfset newemails = application.com.globalFunctions.removeDuplicates(type="list",data=newemails,delimiter=",")>
	<!---End 2009-07-08 p-fnl069 SSS took this out because we can make sure there are no duplicates in the list and change delim at the same time--->
	<cfoutput>
	<cfset emailstatus ="">
	<!--- p-fnl069 2009-07-08 SSS This will be a list of email address we have successfully sent a email to --->
	<cfset EmailSentList="">

	<!--- START:	2012/05/10 	MS	Adding id to table and moving the success msgs out of the table--->
	<h1>phr_Ext_Invite_Colleagues</h1>
	<p>phr_Ext_Invite_Colleagues_Sent_Details</p>

<table id="tfqo_aReport" data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withborder" cellspacing="1" cellpadding="3" border="0" align="center" style="margin: 0pt auto;">
	<thead>
		<tr>
			<th id="Invite_Friend_email" valign="top" columnname="Invite_Friend_email">
				phr_Invite_Friend_email
			</th>
			<th id="Invite_Friend_result" valign="top" columnname="Invite_Friend_result">
			phr_Invite_Friend_result
			</th>
		</tr>
	</thead>
	<!--- END:	2012/05/10 	MS	--->

	<!--- NJH 2010/02/02 LID 2906 - changed delimiter from "," to ";" --->

	<!--- 2012/05/10 MS	Setting counter to check and set alternate row styling --->
	<cfset CURRENTROW=0>
	<cfloop index="myIndex" list="#newemails#" delimiters=",;">
		<cfset CURRENTROW++>
		<cfset currentclass=listGetAt('oddrow,evenRow',CURRENTROW mod 2+1)>

	<cfset myIndex = trim(myIndex)>
	<cfif application.com.email.isValidEmail(myIndex)>
		<cfquery name="checkPerson" datasource="#application.siteDataSource#">
			select personid from person where email =  <cf_queryparam value="#myIndex#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif checkPerson.recordcount EQ 0>
			<cfset EmailError =  application.com.email.sendEmail(emailtextid=EmailtextID,personid=request.relaycurrentuser.personid,recipientAlternativeEmailAddress=myIndex)>

			<cfif comparenocase(EmailError, "Email failed") EQ 0>
				<cfset emailMessage = "Phr_Ext_sendEmail_failed">
				<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_sendEmail_failed</td></tr> --->
			<cfelse>
				<cfset emailMessage = "Phr_Ext_Email_Successful">
				<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_Email_Successful</td></tr> --->
			</cfif>
		<cfelse>
			<cfif EmailtoExistingPerson>
				<cfif checkPerson.recordcount EQ 1>
					<!--- NJH 2012/12/17 - 2013 Roadmap Item 17 - check email for blacklisting. Should this be checked in the sendemail function?? --->
					<cfif not application.com.email.isEmailBlackListed(emailAddress=myIndex)>
						<cfset EmailError =  application.com.email.sendEmail(emailtextid=EmailExistingtextID,personid=checkPerson.personID,recipientAlternativeEmailAddress=myIndex)>
					<cfelse>
						<!--- maybe need a blacklisted message?? --->
						<cfset emailError = "Email failed">
					</cfif>
				<cfelse>
					<!--- 2009-05-20 GCC this can never be the case!--->
					<cfset EmailError =  application.com.email.sendEmail(emailtextid=EmailtextID,personid=#checkPerson.personID#,recipientAlternativeEmailAddress=#myIndex#)>
				</cfif>

				<cfif comparenocase(EmailError, "Email failed") EQ 0>
					<cfset emailMessage = "Phr_Ext_sendEmail_failed">
					<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_sendEmail_failed</td></tr> --->
				<cfelse>
					<cfset emailMessage = "Phr_Ext_Email_Successful">
					<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_Email_Successful</td></tr> --->
				</cfif>
			<cfelse>
				<cfset emailMessage = "Phr_Ext_Email_Existing">
				<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_Email_Existing</td></tr> --->
			</cfif>
		</cfif>
	<cfelse>
		<cfset emailMessage = "Phr_Ext_Invalid_Email">
		<!--- <tr class="#currentclass#"><td>#myindex#</td><td>Phr_Ext_Invalid_Email</td></tr> --->
	</cfif>
	<tr class="#currentclass#"><td>#myindex#</td><td>#emailMessage#</td></tr>
	</cfloop>
	</table>

	</cfoutput>
	<CF_ABORT>
</cfif>

<!--- ==============================================================================
      Process the adding & sending
=============================================================================== --->
<cfif structKeyExists(form,"btnAddandSend") and form.btnAddandSend>
	<cfquery name="checkPerson" datasource="#application.siteDataSource#">
		select personid from person where email =  <cf_queryparam value="#insEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfquery>

	<cfif checkPerson.recordCount eq 0>
		<!--- First add them --->
		<cfset frmNext="noRedirect">
		<cfset frmReturnMessage = "no">
		<cfinclude template="/remoteAddTask.cfm">
		<cfset frmPersonID = thisPersonID>
	<cfelse>
		<cfset frmPersonID = checkPerson.personid>
	</cfif>

		<cfmodule template = "/communicate/populateCommDetail.cfm"
			commid = #frmCommID#
			personids = #frmPersonID#
			resend = true
		>


		<cfmodule template = "/communicate/send/tskcommsend.cfm"
			CommID = "#frmCommID#"
			personids = "#frmpersonid#"
			showprocessingcode = false
			sendconfirmation = false
			resend = true       <!--- seems sensible to send it again if requested --->
		>

		<CFOUTPUT>
		<cfif showFeedback>
		#resultstring#	<P>
		</cfif>
		#resultText#
		</CFOUTPUT>

		<CF_ABORT>

</cfif>

<!--- ==============================================================================
      show the form
=============================================================================== --->
<cfoutput>

<cfif not emailonly>
<table>
	<form method="post" name="FormY" id="FormY">
		<!--- NJH 2009-07-16 P-FNL069 encrypt hidden fields if on the portal. We can't do this on the internal system at the moment as this file is called in a screen
			which also has a form state variable.. Needs further thinking about. But for now, this will lock down the portal.
		--->
		<cfsavecontent variable="hiddenFields_html">
		<CF_INPUT type="hidden" name="commType" value="#commType#"><br>
		<input type="hidden" name="btnAddandSend" value="yes">
		<CF_INPUT type="hidden" name="frmCommID" value="#frmCommID#">
		<input type="hidden" name="insCountryID" value="9">
		</cfsavecontent>

		<cfif request.relayCurrentUser.isInternal>
			#hiddenFields_html#
		<cfelse>
			<cf_encryptHiddenFields>
				#hiddenFields_html#
			</cf_encryptHiddenFields>
		</cfif>

		<cfif isDefined("frmPersonID")>
			<tr><td colspan=2><hr width="100%" size="2"></td></tr>
		</cfif>
		<tr>
			<td colspan=2>
				#htmleditformat(IntroductionText)#
			</td>
		</tr>

		<tr>
			<td class="label">phr_Ext_FirstName:</td>
			<td colspan="5"><cf_input type="Text" name="insFirstName" message="You must enter a first name" required="Yes" size="25" maxlength="24">
			</td>
		</tr>
		<tr>
			<td class="label">phr_Ext_LastName:</td>
			<td colspan="5"><cf_input type="Text" name="insLastName" message="You must enter a last name" required="Yes" size="25" maxlength="25"></td>
		</tr>
		<tr>
			<td class="label">
			Ext_sendEmail_company:</td>
			<td colspan="5">
			<input type="Text" name="insSiteName" value="" size="25" maxlength="50"></td>
		</tr>
		<tr>
			<td class="label">phr_Ext_Emailaddress:</td>
			<td colspan="5"><cf_input type="Text" name="insEmail" message="You must supply an email address" required="Yes" size="25" maxlength="50"></td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<!--- NAS 2009-10-30	LID - 2785 Make 'SEND' Translateable --->
				<input type="submit" name="sendonly" value=" phr_Sys_Send ">
		   	</td>
		</tr>
		</form>
	</table>
<cfelse>
	<form method="post" name="FormY" id="FormY">
		<cf_relayFormDisplay class="">
			<CF_relayFormElementDisplay relayFormElementType="HTML" label="" currentvalue="<h1>phr_Ext_Invite_Colleagues</h1>#IntroductionText#" fieldname="" spanCols="yes">

			<!--- NJH 2009-07-17 P-FNL069 - encrypt hidden fields if on the portal --->
			<cfsavecontent variable="hiddenFields_html">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#commType#" fieldname="commType">
			<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="yes" fieldname="btnAddandSend">
			</cfsavecontent>
			<cfif request.relayCurrentUser.isInternal>
				#hiddenFields_html#
			<cfelse>
				<cf_encryptHiddenFields>
					#hiddenFields_html#
				</cf_encryptHiddenFields>
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="Emailaddress" currentValue="" label="phr_Ext_Emailaddress" cols="30" rows="5" maxLength="500" maxChars="" tabindex="8" required="yes">
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="SendEmails" currentValue="phr_Send" label="" spanCols="No" valueAlign="left">
		</cf_relayformdisplay>
	</form>
</cfif>

</cfoutput>
<!--- 	</cfif>
<cfelse>
	<!--- this is the first time we are entering the form --->
	<cfoutput>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center">
			<cfform method="post" name="FormX">
				<input type="hidden" name="btnSend" value="yes">
				<input type="hidden" name="frmCommID" value="#frmCommID#">
				<tr>
					<td align="center" colspan=2>
						<cfinput type="Text" name="frmEmail" message="You must enter an email address" required="Yes" size="35" maxlength="80">
					</td>
				</tr>

				<tr>
					<td colspan="2" align="center">
						<a href="javascript:document.FormX.submit()">#phr_continue#</a>
				   	</td>
				</tr>
				</cfform>
		</table>
	</cfoutput>
</cfif>
 --->
<cfif isDefined("debug") and debug eq "yes">
	<cfoutput>DEBUG sendCommToFriend.cfm commid = #htmleditformat(frmCommID)#</cfoutput>
</cfif>
