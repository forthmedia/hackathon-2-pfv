<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eventRegPageFinal.cfm	
Author:				SWJ
Date started:		2005-02-18
	
Description:		This page is the final page in the registration process.  It pairs 
					with eventListUknownUser, as in that screen registerThisEventFlagTextID
					is setup. This will contain the eventTextID of the event that the user 
					is egistering for.
					
					It is also detects when certain boolean flags are to be set via a structure 
					and if session.setThisFlagTextID has any keys then we need to 
					examine them and set the flags.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2011/05/04 WAB LID 6433 When unknown user is registered the session is cleared.  Therefore session.registerThisEventFlagTextID is lost.  Now stored in relayCurrentuser using com.relayCurrentUser.storeUserData()


Possible enhancements:


 --->

<!--- NYB 2011-12-02 didn't cf_param because it doesn't seem to be getting used --->
<cfparam name="regStatus" default="RegistrationSubmitted" >

<cf_param name="phraseTextIDExtension" default=""/><!--- you can make the standard phrase  --->
<cf_param name="noEmailConfirmation" default="no" type="boolean"/>

<cf_translate>

<div id="eventListTable">

<!--- the assumption is that session.registerThisEventFlagTextID will have been set by eventListUnknownUser.cf --->
<cfif structKeyExists(request.relayCurrentUser,"registerThisEventFlagTextID") and not request.relaycurrentuser.isUnknownUser>
	<cfset registerThisEventFlagTextID = request.relayCurrentUser.registerThisEventFlagTextID>
	<!--- <script>
			// first get the details for this event
			qEventDetails = application.com.relayEvents.getEventDetails(session.registerThisEventFlagTextID);
			// then register this personID
			application.com.relayEvents.updateEventRegStatus(session.registerThisEventFlagTextID,request.relayCurrentUser.personID);
			}
	</script> --->
	
	<!--- assuming we have a value in session.registerThisEventFlagTextID then register the event --->
	<cfif len(registerThisEventFlagTextID) gt 0 and compareNoCase(registerThisEventFlagTextID,"unableToAttendEvent") neq 0>
		<cfinvoke component="relay.com.relayEvents" 
			method="getEventDetails"
			eventFlagTextID="#registerThisEventFlagTextID#" 
			returnvariable="qEventDetails">
		</cfinvoke>
		
		<cfinvoke component="relay.com.relayEvents" 
			method="updateEventRegStatus"
			eventFlagTextID="#registerThisEventFlagTextID#"
			personID="#request.relayCurrentUser.personID#" 
			returnvariable="regDetails">
		</cfinvoke>
	
		<cfif noEmailConfirmation eq "no">
		
			<cfset frmPersonID = request.relayCurrentUser.personID>
			<cfset eventFlagTextID = registerThisEventFlagTextID>
	 		<cfinclude template="/eventsPublic/mailConfirmation.cfm">
		
		</cfif>
		
		<cfoutput>
			<p>
				<strong>#htmleditformat(regDetails.fullname)#</strong>
			</p>
			<p>
				Phr_EV_ThankYouReg#htmleditformat(phraseTextIDExtension)# <cfif noEmailConfirmation eq "no">Phr_EV_YouWillReceiveConfirmationEmailShortly</cfif>
			</p>
			<p>
				<strong>#htmleditformat(regDetails.eventName)#</strong>
			</p>
		</cfoutput>
	</cfif>
	
	<cfif len(registerThisEventFlagTextID) gt 0 and compareNoCase(registerThisEventFlagTextID,"unableToAttendEvent") eq 0>
		<cfoutput><p>Phr_Event_UnabletoAttendEvent#htmleditformat(phraseTextIDExtension)#</p></cfoutput>
	</cfif>
	
	<!--- if session.setThisFlagTextID has any keys then we need to examine them and insert them --->
	<cfif structKeyExists(session,"setThisFlagTextID") and listLen(structkeylist(session.setThisFlagTextID)) gt 0>
		<p>phr_youCheckedTheFollowing</p>
		<cfoutput>
		<ul>
		<cfloop list="#structkeylist(session.setThisFlagTextID)#" index="xyz">
			<cfscript>
				application.com.flag.setBooleanFlag(request.relayCurrentUser.personID,session["setThisFlagTextID"]["#xyz#"]);
			</cfscript>
			<li>phr_#htmleditformat(xyz)#</li>
		</cfloop>
		</ul>
		</cfoutput>
	</cfif>

<cfelse>
	<cfoutput>
	<!--- this will fire if the session has timed out --->
		<p>Phr_EventRegistrationTimeOut#htmleditformat(phraseTextIDExtension)#</p>
	</cfoutput>
</cfif>
</div>
</cf_translate>
