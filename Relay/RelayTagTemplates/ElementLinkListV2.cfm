<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		ElementLinkList.cfm
Author:			SWJ based on DAM's code
Date created:	2002-01-21

	Objective - This displays a list of children of the calling element as a URL 				list
		
	Syntax	  -	Simply place <RELAY_ELEMENTLINKLIST> in the calling element.

Amendment History:

Date 		Initials 	What was changed
2002-01-21	SWJ			Called qryGetChildren as an include
2005-02-11	SWJ			Redesigned to use .css
2006-11-15    WAB 		added support for hidefromlinklist=1 in the parameters of an element
Enhancement still to do:

--->

<cfparam name="elementLinkListShowSummary" default="no">
<cfparam name="linkListDivID" default="ElementLinkListContent">
<cfparam name="elementLinkListShowImage" default="no">
<cfparam name="numberofgenerations" type="numeric" default="0">
<cfparam name="numberofgenerationsofchildren" type="numeric" default="1">
<cfparam name="showCurrentElementChildren" type="boolean" default="false">
<!--- <cfinclude template="qryGetChildren.cfm"> --->

<cfset GetChildren = application.com.relayElementTree.convertTReeToMenuBranch(
						elementTree = request.currentElementTree, 
						topOfBranchID = RelayTag.ElementID,
						currentelementid = RelayTag.ElementID,
						showCurrentElementChildren = true , 
						numberofgenerations = 1,
						numberofgenerationsofchildren =numberofgenerationsofchildren)>
						
<cf_translate>
<cfoutput>
<div id="#linkListDivID#">
</cfoutput>
	
	<cfoutput query="GetChildren">
		<cfif parameters contains "hideFromLinkList=1">
		<cfelse>
		<!--- if the element has been set to isExternalFile then treat the link as an external URL --->
		<cfif isexternalfile eq 1>
			<cfset link = "#url#">
		<cfelse>
			<cfset link = "/?eid=" & #id#>
		</cfif>
		
		<!--- this is displayed as a unordered list so that you can easily style it using a stylesheet --->
		<ul>
			<li>
			<cfif elementLinkListShowImage eq "yes">
				<cfset linkImagePath=application.userfilesabsolutepath & "content\elementLinkImages\linkImage-" & GetChildren.ID & "-thumb.jpg">
				<cfif fileexists(linkImagePath)>
					<img src="/content/elementLinkImages/linkImage-#GetChildren.ID#-thumb.jpg" border=0 width="80">
				</cfif>				
			</cfif>
			<a href="#link#">phr_headline_element_#htmleditformat(GetChildren.ID)#</a>
		<cfif elementLinkListShowSummary eq "yes">
			<dd>phr_summary_element_#htmleditformat(GetChildren.ID)#</dd>
		</cfif></li>
		</ul>
		</cfif>	
	</cfoutput>

</div>

</cf_translate>
