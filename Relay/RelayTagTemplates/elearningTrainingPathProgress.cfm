<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			elearningTrainingPathProgress.cfm
Author:				YMA
Date started:		2014/05/01
Description:		Display persons progress through courses belonging to TrainingPaths they are assigned to.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

2014-07-11			YMA			Case 440931: Remove 'Group by:' dropdown from {{ RELAY_ELEARNING_TrainingPath_PROGRESS }}
2015-11-24			IH			P-TAT006 BRD 31. Add showExired
 --->

<cfparam name="frmEntityTypeID" type="numeric" default="0">   <!--- this page is only currently used on portal so not sure why we have these variables... not really needed!! ---->
<cfparam name="frmCurrentEntityID" type="numeric" default="0">
<cfparam name="GroupBy" default="">
<cfparam NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#">


<cf_param label="Course sort order" name="courseSortOrder" type="string" default="title_of_course" validvalues="TrainingPath,Title_of_course,publisheddate"/>
<cf_param label="Module sort order" name="moduleSortOrder" type="string" default="Title_Of_Module" validvalues="Module_Type,Title_Of_Module,publishedDate,Module_Code"/>
<cf_param label="Show these cols" multiple="true" displayAs="twoSelects" name="showTheseColumns" allowSort="true" type="string" validvalues="TrainingPath,Title_of_course,publisheddate,Num_modules_in_course,num_modules_attempted,num_modules_completed" default="TrainingPath,Title_of_course,num_modules_attempted,num_modules_completed"/>
<cf_param name="numRowsPerPage" type="numeric" default="500"/>
<cf_param label="Quiz question order method" name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/>
<cf_param label="Show incorrect questions on quiz completion" NAME="showIncorrectQuestionsOnCompletion" DEFAULT="false" validValues="true,false"/>
<cf_param label="Show review answers link on quiz completion" NAME="reviewAnswersLink" DEFAULT="true"  validValues="true,false"/>

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfif structKeyExists(form,"frmPersonID")>
	<cfset personID = frmPersonID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 0>
	<cfset personID = frmCurrentEntityID>
<cfelseif not request.relayCurrentUser.isInternal>
	<cfset personID = request.relayCurrentUser.personID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 2>
	<cfset personID = 0>
</cfif>

<!--- 2015-11-24 IH P-TAT006 BRD 31. Add showExired --->
<cfset getData = application.com.relayElearning.GetTrainingCoursesDataV2(joinTrainingPaths=true,personid=personID,sortorder=courseSortOrder,active=true,showExpired=false)>

<cfif listfind(listappend(application.com.globalFunctions.ListMinusList(showTheseColumns,"Title_Of_Course,level"),GroupBy),courseSortOrder) eq 0>
	<cfset courseSortOrder = listfirst(application.com.globalFunctions.ListMinusList(showTheseColumns,"Title_Of_Course,level"))>
</cfif>

<cfquery name="getData" dbType="query">
	select distinct cast(trainingPathID as varchar) +'_'+ cast(courseID as varchar) as trngPathID_courseID,
	<cfif groupBy neq "">
		#listappend(showTheseColumns,GroupBy)#, courseID,
	<cfelse>
		#showTheseColumns#, courseID,
	</cfif>
	'<img src="/images/icons/bullet_arrow_down.png">' as Show_Modules
	 from getData
	 order by <cf_queryObjectName value="#courseSortOrder#">
</cfquery>

<cfparam name="disableShowAllLink" default="#getData.recordcount lte 1000?false:true#"/>

<cfif getData.recordCount gt 0>

	<cf_modalDialog size="large" type="iframe" identifier=".takeQuizURL">

	<cfoutput>
	<script>
		var variableStruct = {personId:'#jsStringFormat(application.com.security.encryptVariableValue(name="personID",value=personID))#',
								orderMethod:'#jsStringFormat(OrderMethod)#',
								moduleSortOrder:'#jsStringFormat(moduleSortOrder)#',
								stringencyMode:'#jsStringFormat(stringencyMode)#',
								showIncorrectQuestionsOnCompletion:'#jsStringFormat(showIncorrectQuestionsOnCompletion)#',
								reviewAnswersLink:'#jsStringFormat(reviewAnswersLink)#',
								showTheseColumns:'#jsStringFormat(showTheseColumns)#'
								<!--- <cfif not request.relayCurrentUser.isInternal>, eid:'#jsStringFormat(request.currentElement.id)#'</cfif> --->
							};

		jQuery(document).ready(function() {
			trainingModules.init(variableStruct);
		});

		function refreshModuleDiv() {trainingModules.refreshModuleDivInTrainingPath();}
	</script>
	</cfoutput>

	<cfset showTheseColumns = listAppend(showTheseColumns,"show_modules")>

	<cf_includeJavascriptOnce template="/elearning/js/elearning.js">

	<CF_tableFromQueryObject
		queryObject="#getData#"
		queryName="getData"
		groupByColumns="#groupBy#"
	 	hideTheseColumns="#groupBy#"
		showTheseColumns="#showTheseColumns#"
		columnTranslation="true"
		ColumnTranslationPrefix="phr_TrainingPath_"
		dateformat="publisheddate"
		numberFormat="Num_modules_in_course,num_modules_attempted,num_modules_completed"
		useInclude="false"
		keyColumnList="Show_Modules"
		keyColumnURLList="##"
		keyColumnKeyList="courseID"
		keyColumnOnClickList=" " <!--- javascript:showModules(this*comma##courseID##);return false --->
		keyColumnOpenInWindowList="no"
		HidePageControls="no"
		allowColumnSorting="no"
		sortOrder="#courseSortOrder#"
		numRowsPerPage = "#numRowsPerPage#"
		disableShowAllLink="#disableShowAllLink#"
		rowIdentityColumnName="trngPathID_courseID"
	>
<cfelse>
	<cfoutput><div>phr_elearning_no_TrainingPaths_assigned</div></cfoutput>
</cfif>