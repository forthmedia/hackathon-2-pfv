<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		ElementChildrenFAQs.cfm
Author:			SWJ
Date created:	2005-05-30

Description:	Draws a FAQList from child elements of the current element

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


--->

<CFPARAM NAME="ElementType" DEFAULT="FAQ">
<CFPARAM NAME="Title" DEFAULT="Frequently Asked Questions">
<cfparam name="FAQTreeTopID" type="numeric" default="#eid#">

<cfquery name="getElements" datasource="#application.sitedatasource#" >
	-- #application.cachedContentVersionNumber# (this variable is used to flush the cache when the content is changed)
	exec GetElementBranchV2 @ElementID =  <cf_queryparam value="#FAQTreeTopID#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid=#request.relayCurrentUser.personid#, @statusid = 4, @Depth=99
</cfquery>


<CFQUERY NAME="GetTreeData" DBTYPE="query">
	SELECT node AS ItemID,  
		Parent AS ParentItemID,
		Name as description,
		RecordRights as thisRights,
		elementTypeID,
		status,
		statusid,
		haschildren
	FROM getElements
	Order by sortorder,ParentItemID, ItemID 
</CFQUERY>

<!--- need to get this item translated now so that relay tags can be run 
<cf_translate phrases = "headline,Detail" entitytype="element" entityid="10"/>--->

<CF_Translate>
<cfoutput>
<STYLE>
P {	margin-left : 15px;
	margin-right : 15px;}
</STYLE>

<script>
	function tree_toggleTarget ( target ) {
		if ( target.style.display == 'none' )	target.style.display = '' ;
		else target.style.display = 'none' ;
	}
</script>

</cfoutput>

<!--- <cfdump var="#getTreeData#"> --->
<CFOUTPUT Query="GetTreeData" group="ParentItemID">
	<cfif ParentItemID eq FAQTreeTopID>
		<cfset parentID = 0>
	<cfelse>
		<cfset parentID = parentItemID>
	
		<div id="parent_#parentItemID#" style="font: 12px Arial, Helvetica, sans-serif; color: Black; cursor:pointer;">
			<p><a href onClick="javascript:tree_toggleTarget(child_#parentItemID#,'open');return(false);"><img src="/images/MISC/iconHelpSmall.gif" alt="" border="0"> phr_headline_element_#htmleditformat(parentItemID)#</a></p>
		</div>	
		<div id="child_#parentItemID#" style="display: none;cursor:pointer">
	</cfif>
	<CFOUTPUT>
		<cfif getTreeData.hasChildren eq 0>
		<div id="question_#ItemID#" style="padding-left: 20px;">
			<p><a href onClick="javascript:tree_toggleTarget(answer_#ItemID#,'open');return(false);"><img src="/images/MISC/iconHelpSmall.gif" alt="" border="0"> phr_headline_element_#htmleditformat(ItemID)#</a></p>
		</div>	
		<div id="answer_#ItemID#" style="display: none; padding-left: 20px;">
	    	<p>phr_detail_element_#htmleditformat(ItemID)#</p>
		</div>
		</cfif>
	</CFOUTPUT>
	<cfif ParentItemID neq FAQTreeTopID>
		</div>
	</cfif>
</CFOUTPUT>


</CF_Translate>
