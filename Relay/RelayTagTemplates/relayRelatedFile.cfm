<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayRelatedFile.cfm
Author:				GCC
Date started:		2008/06/30

Description: First cut attempt to wrap the related file custom tag in a relaytag for ease of config setup.

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed
2010/02/03			NJH			P-LEX044	Added switches to show/hide various columns
2012/11/02			NJH			Case 431417 - added a bind of fileCategory and tidied up some other cf_params

Possible enhancements: Needs extending to all entity types - currently only organisation, location or people


 --->

<cfparam name="Entity" default="">
<cf_param name="EntityType" default="organisation" validValues="Organisation,Location,Person"/>
<cf_param name="FileCategory" default="" relayFormElementType="select" multiple="true" bindFunction="cfc:webservices.relatedFileWS.getRelatedFileCategory(fileCategoryEntity={EntityType})" display="fileCategory" value="fileCategory" size="6"/>
<cf_param name="Action" default="List,Put" validValues="List,Put" multiple="true" size="3"/>
<cf_param name="DescriptionLength" default="Short" validvalues="Long,Short"/>
<cf_param name="ReplaceAllowed" default="yes" type="boolean"/>

<!--- NJH 2010/02/03 P-LEX044  - added switches --->
<cf_param name="showLanguage" default="yes" type="boolean"/>
<cf_param name="showFileCategoryDescription" default="yes" type="boolean"/>
<cf_param name="showFileDescription" default="yes" type="boolean"/>
<cf_param name="showUploadedDate" default="yes" type="boolean"/>

<cf_param name="NoUpdate" default="no" type="boolean"/>
<cfparam name="webhandle" default="webhandle"/>
<cfparam name="filehandle" default="filehandle"/>
<cfparam name="includehandle" default="includehandle"/>
<cfparam name="ftphandle" default="ftphandle"/>
<cfparam name="dbdatasource" default="#application.sitedatasource#">

<cfif FileCategory eq "">
	You must specify a fileCategory
	<cfexit>
</cfif>

<cfswitch expression="#entitytype#">
	<cfcase value="Organisation">
		<cfset entity =request.relayCurrentuser.OrganisationID>
	</cfcase>
	<cfcase value="Location">
		<cfset entity =request.relayCurrentuser.locationID>
	</cfcase>
	<cfcase value="Person">
		<cfset entity =request.relayCurrentuser.personID>
	</cfcase>
	<cfdefaultcase>
		Unrecognised entityType.
		<cfexit>
	</cfdefaultcase>
</cfswitch>

<!--- NJH 2010/02/03 P-LEX044  - added switches --->
<cf_relatedfile
	action="#action#"
	entity="#Entity#"
	entitytype="#EntityType#"
	filecategory="#FileCategory#"
	NoUpdate="#NoUpdate#"
	replaceallowed="#replaceallowed#"
	DescriptionLength="#DescriptionLength#"
	showLanguage="#showLanguage#"
	showFileCategoryDescription="#showFileCategoryDescription#"
	showFileDescription="#showFileDescription#"
	showUploadedDate="#showUploadedDate#"
>

