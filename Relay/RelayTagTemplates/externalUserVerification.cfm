<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			externalUserVerification.cfm
Author:				AH
Date started:		2004-08-25

Purpose:	To provide a form for verifying a user with their magic number .

Usage:	this is used as a Relay Tag

frmNext (optional) This specify which template is processed after the form data.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<CFPARAM name="message" default="">
<CFPARAM name="messagePhrase" default="">



<cfif not structKeyExists(variables,"GoToEID")>
	The External User Verification Relay tag requires the variable GoToEID which
	should be set to the elementID of the screen to go to if the user authenticates.
	<cfexit method="EXITTAG">
</cfif>


<cfscript>
	if(structKeyExists(form,"pNumber")){
		authenticated = application.com.login.autoAuthenticateUser(listFirstP=listFirst(pNumber,"-"),listLastP=listLast(pNumber,"-"),lastname=lastname);
	}
</cfscript>

<cfif structKeyExists(variables, "authenticated") and authenticated.recordCount EQ 1>
	<cfset level=2>


<!--- 	<cfscript>
		// this section is used for testing various module specific functions
		if (structKeyExists(variables,"module") and module eq "incentiveManager") {
		//and set the flag which adds it to the right user group
		application.com.flag.setBooleanFlag(authenticated.PersonID, "approvedIncentiveUser");
		// send the user an email with their user name and password
		}
	</cfscript> --->

	<!--- gcc test --->
	<cfscript>
			// AR 17-MAR-2005 Profile checks if a user can login. The structure returns a set behaviours which determine what the login process should do.
			profileSuccess = structNew();
			if (isdefined("form.securityProfileID")){
					profileSuccess = application.com.login.applySecurityProfile (personID = authenticated.personID,securityProfileID = form.securityProfileID);
			}
	</cfscript>

	<!---Set partner's session cookie (TimeStamp and level of login)--->
	<cfset application.com.globalFunctions.cfcookie(NAME="PARTNERSESSION", VALUE="#Now()#-#variables.level#")>
	<!--- This next step dynamically recreates the partners user group membership.
	Partner user groups are controlled by the flags that a partner has set. --->
	<CF_createUserGroups personid = "#authenticated.personID#">
	<!---Get all the usergroups this person is a member of--->
	<cfscript>
		getUserGroups = application.com.login.getUserGroups(authenticated.personID);
		application.com.login.insertUsage(authenticated.personID);
	</cfscript>
	<!---and set the User Cookie--->
	<CFCOOKIE NAME="USER" VALUE="#authenticated.PersonID#-#application.WebUserID#-#valuelist(getUserGroups.userGroupID)#"
			EXPIRES="#application.com.globalFunctions.getCookieExpiryDays()#">

	<cfscript>
		//initialize user session structure--->
		application.com.login.initUserSession(request.relayCurrentUser.personId);
	</cfscript>



	<cfoutput>
	<script>
		window.location.href = '/?eid=#jsStringFormat(gotoEID)#';
	</script>
	</cfoutput>
<cfelse>
<cf_translate>

<CFPARAM NAME="Level" DEFAULT="2">

<SCRIPT>
	function submitform(){

		form = window.document.myForm2;
		if (form.pNumber.value == ""){
			alert("<cfoutput>phr_LoginScreen_JS_PleaseEnterUserName</cfoutput>");
			form.pNumber.focus();
			return
		}
		else if (form.lastname.value == ""){
			alert("<cfoutput>phr_LoginScreen_JS_PleaseEnterPassword</cfoutput>");
			form.surname.focus();
			return
		}
		else {
			form.submit();
		}
	}
</SCRIPT>


<cfoutput>
<TABLE ALIGN="center" border=0>
<FORM ACTION="/?#request.query_string#" METHOD="post" NAME="myForm2" ID="myForm2" >
	<INPUT TYPE="Hidden" NAME="reLogin" VALUE=""><!--- forces reauthentication and ignores existing cookies --->
	<cfif isdefined("securityProfileID")>
		<CF_INPUT TYPE="Hidden" NAME="securityProfileID" VALUE="#securityProfileID#">
	</cfif>
	<CFIF structKeyExists(variables, "authenticated") and authenticated.recordCount NEQ 1>
		<TR>
			<TD colspan="2" align="left">
				<P STYLE="color: Red;">
					<B>phr_userVerification_JS_InvalidVerificationDetails</B>
				</p>
			</TD>
	   	</TR>
	</CFIF>

	<TR>
		<TD><P><B>phr_userVerification_Number</B></P></TD>
		<TD ALIGN="left"><INPUT TYPE="text" NAME="pNumber" <cfif structKeyExists(URL, "pp")>value="#htmleditformat(URL.pp)#"</cfif> ></TD>
	</TR>

	<TR>
		<TD><P><B>phr_userVerification_Surname </B></P></TD>
		<TD ALIGN="left"><INPUT TYPE="text" NAME="lastname"></TD>
	</TR>

	<TR>
		<TD>&nbsp;</TD>
		<TD align="left">
			<input type="button" onClick="javascript:submitform()" value="phr_continue" class="button">
		</TD>

	</TR>
	</FORM>

</TABLE>

</cfoutput>

</cf_translate>

</cfif>