<!--- �Relayware. All Rights Reserved 2014 --->
<!--- SiteMap.cfm --->

<!--- Displays an exploded view of a newsletter edition
	shows all section/article headlines whether live or not
 --->
 
<!---
expects frmNewletterID
		frmLevelTwoID
 --->
<CFPARAM NAME="ElementID" DEFAULT="#application.TopElementIDPartnerContent#">
<CFPARAM NAME="frmLevelTwoID" DEFAULT=""> 
<CFPARAM NAME="frmShowCountryID" DEFAULT="0">
<CFPARAM NAME="DefaultTemplate" DEFAULT="PartnerLogin/elementTemplate.cfm">

<CFSTOREDPROC PROCEDURE="GetElementBranch" datasource="#application.sitedatasource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@ElementID" VALUE="#ElementID#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@personid" VALUE=#request.relayCurrentUser.personid# NULL="No">
		<CFPROCRESULT NAME="getElements" RESULTSET="1">
	<CFPROCPARAM TYPE="Out" CFSQLTYPE="CF_SQL_VARCHAR" VARIABLE="ErrorText" DBVARNAME="@FNLErrorParam" NULL="Yes">
</CFSTOREDPROC>
	

<!--- getCountries --->
<CFQUERY NAME="getCountries" datasource="#application.sitedatasource#" DEBUG>		
	Select convert(varchar(3), countryID) + '#application.delim1#' + CountryDescription as country
	from Country
	where isocode <> ''
	order by countrydescription
</CFQUERY>

<CFQUERY NAME="GetTopElementDetails" datasource="#application.sitedatasource#" DBTYPE="ODBC">
	select headline, elementtypeID from element where id = #elementid#
</CFQUERY>





<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">
			<DIV CLASS="Heading">Site Map</DIV>
		</TD>
	</TR>
</table>

<CFSWITCH EXPRESSION=#layout#>
	<CFCASE VALUE="List">
		<table width="450" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
			<CFOUTPUT Query="getElements">
			
				<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
					<TD>
						<CFSET IC=3>
							<CFLOOP CONDITION="IC LTE #generation#">&nbsp;
						<CFSET IC=IC+1>
					</CFLOOP><A HREF="/#DefaultTemplate#?ElementID=#node#">#htmleditformat(Headline)#</A></TD>
				</TR>
			</CFOUTPUT>
		</TABLE>
	</CFCASE>
	
	<CFCASE VALUE="tree">
	
		<CFFORM action="SiteMap.cfm" method="POST" target="BODY">
			<CFTREE name="Tree" height="200" width="230" >
			<CFTREEITEM value="0" display="SiteMap" expand="yes">
			<CFSET CurGroup1 = "">
			<CFLOOP query="getElements">
				<CFIF CurGroup1 is not parent>
					<CFTREEITEM value="#parent#" parent="0" display="#parent#" img="Folder" expand="no">
					<CFSET CurGroup1 = parent>
					<CFSET CurGroup2 = ''>
				</CFIF>
				<CFTREEITEM value="#headline#" parent="#parent#" display="#headline#" img="Document" expand="no">
			</CFLOOP>
			</CFTREE>
			<P>
			<INPUT type="Submit" value="Display">
		</CFFORM>
	</CFCASE>
	
	
	<CFDEFAULTCASE>
		<TABLE>
		<CFSET counter = 0>
		<CFLOOP QUERY="getElements">
		   <CFSET counter = counter + 1>
		   <CFIF counter EQ 1>
		      <!--- first cell in row --->
		      <TR>
		   </CFIF>
		   <TD>
		   <CFOUTPUT><A HREF="/#DefaultTemplate#?ElementID=#node#">#htmleditformat(Headline)#</A></cfoutput>
		   </TD>
		   <CFIF counter EQ 3>
		      <!--- last cell in row --->
		      </TR>
		      <CFSET counter = 0>
		   </CFIF>
		</CFLOOP>
		<CFIF counter NEQ 0>
		   <!--- in case the query's record count isn't a multiple of three --->
		   </TR>
		</CFIF>
		</TABLE>
	</CFDEFAULTCASE>
</CFSWITCH>





