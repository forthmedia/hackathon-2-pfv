<!--- �Relayware. All Rights Reserved 2014 --->
<!---
SSS 	2007-09-25 	This file was created in order to display contents of file library and allow Grouping.
WAB 	2009/05/27 	LID 2237 dealing with secured files, use new field getResults.filenameandpath which has the .cfm added to it if necessary
NAS 	2010/03/22 	LID 3151 Add the cfparam 'thumbnailSize' so that unknown file type like 'wmf' do not cause an error
WAb 	2010/01/31 	fileExists column now exists on the query, so use instead of doing own test
NYB 	2013-03-14	Case 434136 added valign param to improve formatting options

NJH 2013/01/10	2013Roadmap - if file is url, don't display file size.

2013-07-02	YMA		Case 435915 Make sure thumbnailsize parameter actually effects the size of thumbnails but not icons
2014-06-22	SB		Changed to bootstrap/responsive
2014-12-05	PPB		Case 442926 option to showDescription for a file
2014-11-17	DXC		Case 442321 - was displaying a row in table regardless of whether ((source eq "file" and fileExists) or source eq "url") was true. Moved logic inside this conditional
2015/0803	NJH		Bit of a file cleanup. Factorised some code. Still needs work but better than it was. Files now loaded in fancybox, so removed 'getFile' and set the link to /fileManagement/FileGet.cfm. Open some documents in google viewer
					for a nicer user experience.
2015/09/03	NJH		By request, open files of type url in new window.
2016-05-18 	WAB		BR-713/CASE 448770 fix problems displaying MP4 files
2017-01-10	DAN		PROD2016-2880 - fix for thumnailing secure files on display, also support for TIFFs and BMPs

--->

<!--- NAS 2010/03/22 LID 3151 Add the cfparam 'thumbnailSize' so that unknown file type like 'wmf' do not cause an error --->
<cfparam name="thumbnailSize" default="20">
<cfparam name="productid" default="">
<cf_param name="valign" default="top"/> <!--- 2013-03-14 NYB Case 434136 added --->

<cfset modalIdentifierClass = "fileLink">
<cf_includeJavascriptOnce template='/javascript/fileDownload.js'>

<cf_modalDialog size="large" identifier=".#modalIdentifierClass#" hideOnContentClick="false" locked="true" scrollOutside="false" type="iframe" beforeShow="function() {jQuery('iframe[id^=\'fancybox-frame\']').contents().find('body').attr('oncontextmenu','return false;');}"/>

<cfoutput>
	
	<cfif incr eq 1 and not trWritten>
		<cfset trWritten = true>
		<div class="boxRow clearfix">
	</cfif>

	<cfif (GetResults.source eq "file" and fileExists) or GetResults.source eq "url">
		<!--- 2005-08-31 SWJ Added the logic to show thumbNails.
		 2006-01-27 NM Added ability to specify icon for all filetypes.
		 2006-04-03 NM Added ability to specify image file as link for file. --->
		<!--- CHECK FOR IMAGE FILE TO DISPLAY IF AN ICON DOES NOT EXIST --->
		<!--- 2013-06-28	YMA	Case 435915 We need to look inside the entity folder for the image --->
		<cfset fileExtension = GetResults.source eq "file"?listLast(fileName,"."):"">
		<cfset modal = true>
		<cfset urlParams = "fileID=#fileID#">
		
		<cfif isNumeric(productid)>
			<!--- <cfset getFileLink="javascript:getFile(#FileID#,#application.entityTypeID.product#,#productid#)">
			<cfset getFileLink = "/fileManagement/FileGet.cfm?fileID=#fileID#&entityTypeID=#application.entityTypeID.product#&entityID=#productID#&eid=#request.currentElement.ID#"> --->
			<cfif isdefined('request.currentElement.ID')> <!--- GCC 2016/02/16 GCC case 448050 / HF1427 / RT-299 --->
				<cfset urlParams = urlParams& "&entityTypeID=#application.entityTypeID.product#&entityID=#productID#&eid=#request.currentElement.ID#">
			<cfelse>
				<cfset urlParams = urlParams& "&entityTypeID=#application.entityTypeID.product#&entityID=#productID#"> 
			</cfif>
   		</cfif>

		<!--- <cfset getFileLink="javascript:getFile(#FileID#)"> NJH 2015/08/03  - nicer user experience. Remove the new window and open files in fancy box or download them directly to browser. Probably some more
		work involved, but hopefully this will be the start. Instigated by some Elastica work.
		WAB 2016-05-18 BR-713/CASE 448770 remove mp4 from the list of download file extensions --->

		<cfset getFileLink = "#request.currentSite.protocolAndDomain#/fileManagement/FileGet.cfm?#urlParams#">
		<!--- list of extensions that will be downloaded --->
		<cfset downloadableFileExtensions = "zip,mp3,m4v,doc,docx,xlsx,xls,ppt,pptx"> 

		<!--- view office documents in google viewer for a nice experience if the file is not secure --->
		<!--- NJH 2015/08/04 - removed this for now as google seems to be throwing in some js/css files that is throwing fancy box out... Needs to be looked at. I also think that it may need to get into fileManagement/fileDisplay.cfm anyways.... --->
				
					<!--- <cfif listFindNoCase("doc,docx,xlsx,xls,ppt,pptx", fileExtension) and not secure>	

						 <cfif secure>
							<cfset securedFileDetails = application.com.fileManager.getSecuredFile(fileID=fileID)>
							<cfif securedFileDetails.isOK>
								<cfset getFileLink = securedFileDetails.relativeURL>
							</cfif>
						</cfif> 
						<cfset getFileLink = "http#request.currentSite.isSecure?'s':''#://docs.google.com/gview?url=#getFileLink#&embedded=true">
                        <cfset modal = true> --->

					<cfif listFindNoCase(downloadableFileExtensions, fileExtension) or GetResults.source eq "url">
						<cfset getFileLink = GetResults.source neq "url"?"javascript:downloadFile('#getFileLink#')":"javascript:void(openWin('#getFileLink#','#jsStringFormat(getResults.name)#'));">
						<cfset modal=false>
					</cfif>

					<cfset fileClass = modal?modalIdentifierClass:"">
					<cfset setWidth = false>

					<cfif showThumbnail>
						<cfif fileExists("#application.paths.content#\linkImages\files\#FileID#\thumb_#FileID#.png")>
							<cfset imageSrc = "/Content/linkImages/files/#FileID#/thumb_#FileID#.png">
							<cfset setWidth = "true">
						<cfelseif icon neq "" and fileExists("#application.paths.content#\fileIconImages\#icon#.gif")>
							<cfset imageSrc = "/Content/fileIconImages/#icon#.gif">
						<cfelseif icon neq "" and fileExists("#application.Path#\fileManagement\iconImages\#icon#.gif")> <!--- is this still valid?? wouldn't have thought icons would be in this directory. They shouldn't be!! --->
							<cfset imageSrc = "/fileManagement/iconImages/#icon#.gif">
						<cfelseif listFindNoCase("jpg,jpeg,gif,png,bmp,tiff", fileExtension)>
							<cfparam name="flush" default="0">
							<cfscript>
								thisSourceFile = "/" & GetResults.path & "/" & FileName;
								thumbnail = application.com.relayThumbnailDisplay.getThumbnail(sourceImageURL=thisSourceFile,size=thumbnailSize,flush=flush,secure=secure);
							</cfscript>
							<cfset imageSrc = thumbnail>
							<cfset setWidth = true>
						<cfelseif listFindNoCase("pdf,doc,xls,ppt,zip,docx,xlsx,pptx,txt", fileExtension)>
							<cfset imageFile = "#fileExtension#.gif">
							<cfif fileExtension eq "txt">
								<cfset imageFile = "txt.png">
							</cfif>
							<cfset imageSrc = "/images/MISC/#imageFile#">
						<cfelse>
							<cfset imageSrc = "/images/MISC/spacer.gif">
							<cfset setWidth = true>
						</cfif>
					</cfif>


					<!---
					<cfset hasImageFile = fileExists("#application.paths.content#\linkImages\files\#FileID#\thumb_#FileID#.png")>
					<cfif showThumbnail AND hasImageFile>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<div class="fileId col-xs-12 col-sm-2 01">
							<img src="/Content/linkImages/files/#FileID#/thumb_#FileID#.png" alt="" width="#thumbnailSize#" border="0">
						</div>
						<cfset incr = incr + 1>
					<cfelseif showThumbnail and icon neq "" and fileExists("#application.paths.content#\fileIconImages\#icon#.gif")>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<div class="fileId col-xs-12 col-sm-2 02">
							<A class="#fileClass#" HREF="#getFileLink#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#"><img src="/Content/fileIconImages/#icon#.gif" border="0"></a>
						</div>
						<cfset incr = incr + 1>
					<cfelseif showThumbnail and icon neq "" and fileExists("#application.Path#\fileManagement\iconImages\#icon#.gif")>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<div class="fileId col-xs-12 col-sm-2 03">
							<A class="#fileClass#" HREF="#getFileLink#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#"><img src="/fileManagement/iconImages/#icon#.gif" border="0"></a>
						</div>
						<cfset incr = incr + 1>
					<cfelseif showThumbnail and (listFindNoCase("jpg,gif,png", right(fileName,3)) gt 0 or right(filename,4) eq "jpeg")>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<div class="fileId col-xs-12 col-sm-2 04">
							<!--- must pass flush=1 to redraw after thumbsize change --->
							<cfparam name="flush" default="0">
							<cfscript>
								thisSourceFile = "/content/" & GetResults.path & "/" & FileName;
								//writeOutput(thisSourceFile);
								thumbnail = application.com.relayThumbnailDisplay.getThumbnail(sourceImageURL=thisSourceFile,size=thumbnailSize,flush=flush);
							</cfscript>
							<A class="#fileClass#" HREF="#getFileLink#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#"><img src="#thumbnail#" alt="" width="#thumbnailSize#" border="0"></a>
						</div>
						<cfset incr = incr + 1>
					<cfelseif showThumbnail and listFindNoCase("pdf,doc,xls,ppt,zip,docx,xlsx,pptx,txt", listLast(fileName,".")) gt 0>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<cfset imgSrc = "#listLast(fileName,".")#.gif">
						<cfif listLast(fileName,".") eq "txt">
							<cfset imgSrc = "txt.png">
						</cfif>
						<div class="fileId col-xs-1 col-sm-2 05">
							<A HREF="#getFileLink#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#"><img src="/images/MISC/#imgSrc#" border="0"></a>
						</div>
						<cfset incr = incr + 1>
					<cfelseif showThumbnail>
						<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
						<div class="fileId col-xs-12 col-sm-2 06">
							<img src="/images/MISC/spacer.gif" alt="" width="#thumbnailSize#" height="#evaluate(thumbnailSize/2)#" border="0">
						</div>
						<cfset incr = incr + 1>
					<cfelse>
						<cfset incr = incr + 1>
					</cfif> --->



					<cfset incr = incr + 1>
					<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
					
					<!--- CONTENT BOX --->
					<div class="boxContentBox" style="width: #colWidth#%">
						
						<cfif showThumbnail>
							<div class="thumbnailBox">
								<A class="#fileClass#" HREF="#getFileLink#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#">
									<img src="#imageSrc#" border="0" <cfif setWidth>width="#thumbnailSize#"</cfif>>
								</a>
							</div>
						</cfif>					

						<span>
							<A HREF="#getFileLink#" CLASS="smallLink #fileClass#" title="phr_DocLib_LastRevised #DateFormat(Revision, "dd-mmm-yyyy")# #TimeFormat(Revision, "HH:mm:ss")#">
							#htmleditformat(getResults.Name)#
							</A>
							<cfif showfilesize and source eq "file"> 
								(#htmleditformat(filesize)# KB)
							</cfif>
						</span>
						
						<!--- START 2014-12-05 PPB Case 442926  --->
						<cfif StructKeyExists(variables,'showDescription') and showDescription>
					    <p id="fileDescription">#description#</p>
				    </cfif>
						<!--- END 2014-12-05 PPB Case 442926  --->

						<cfif not SuppressFileName>
							<!--- 2013-03-14 NYB Case 434136 changed "top" to "#valign#": --->
							<div class="filename">
								<span>#htmleditformat(FileName)#</span>
							</div>
						</cfif>

						<cfif showLastRevisedDate><!--- 2005-08-31 SWJ Added the logic --->
							<div class="revisedDate">#DateFormat(Revision, "dd-mmm-yyyy")#</div>
						</cfif>
					</div> <!--- / CONTENT BOX --->


					<!--- START 2014-11-17	DXC		Case 442321  Moved logic for preventing empty cells into section under conditional for fileExists or isURL above--->
					<cfif currentRow eq recordCount and incr lte defaultFileListColumns>
						<cfset fromItem = incr - 1>
						<cfset noEmptyCells = ((defaultFileListColumns+1) - incr) * (extraCells)>
						<!--- <cfset noEmptyCells = ((defaultFileListColumns+1) - incr) * (extraCells+1)>  16-11-2007 JvdW: Removed +1 to prevent superfluous td to display--->
						<!--- <td>incr:#incr#<br>defaultFileListColumns:#defaultFileListColumns#<br>noEmptyCells:#noEmpytCells#<br>extraCells:#extraCells#</td> --->
						<cfloop from="1" to="#noEmptyCells#" index="X">
							<cfif x mod 2 eq 0>
								<div width="#colwidth#%">&nbsp;</div>
							<cfelse>
								<div>&nbsp;</div>
							</cfif>
							<cfset incr = incr + 1>
						</cfloop>
					</cfif>
					<cfif incr gt defaultFileListColumns>
						<cfset incr = 1>
						<cfset trWritten = false>
						</div>
					</cfif>
					<!--- END 2014-11-17	DXC		Case 442321  Moved logic for preventing empty cells into section under conditional for fileExists or isURL above --->
				</cfif>
</cfoutput>
