<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	A relay tag which can be used for setting a session variable

	WAB July 2006
	
	requires 
		name
		value
	
	WAB 2012-11-19 CASE 432097 Code to check whether relaytag is a custom tag was failing, so someone had implemented a work around in this tag.	
			Failing code now fixed so work around can be removed				
--->


<cf_param name="attributes.name" required="true">
<cf_param name="attributes.value" required="true">


<cfset "session.#attributes.Name#" = attributes.Value>

<cfif isdefined("debug") or isdefined("attributes.debug")>
	<cfoutput>session.#htmleditformat(attributes.Name)# set to #session[attributes.Name]#</cfoutput> 
</cfif>






