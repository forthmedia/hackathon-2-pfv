<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		elearningPersonCertificates.cfm	
Author:			AJC  
Date started:	2012/07/17
	
Description:	Wrapper for certificate printing 		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->
<cf_include template="\elearning\printPDFCertificates.cfm" getFieldXML="true" >       <!--- 2015-10-14 PPB P-KAS064 Certificate Printing use cf_include and added getFieldXML attribute so the parameters are picked up --->