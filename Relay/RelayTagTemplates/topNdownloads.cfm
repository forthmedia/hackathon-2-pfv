<!--- �Relayware. All Rights Reserved 2015 --->
<!--- topNdownloads.cfm
Relaytag used to create widget that displays list of most downloaded files

2015-02-02	ACPK	CRARB001 Implemented first iteration of Top Downloaded Files widget for Arbor
2015-02-05	ACPK	CRARB001 Updated links to always open in new window --->

<!--- include JavaScript template used for opening links in new window --->
<cf_includeJavascriptOnce template = "/javascript/openWin.js">
<!--- submit total number of items in list (default = 5) --->
<cf_param name="norequested" label="Number of items" type="integer" default="5" />
<!--- submit file type group IDs to include (default: none) --->
<cf_param name="FileTypeGroupID" label="Media group" default="0" validValues="SELECT heading AS display, fileTypeGroupID AS value FROM fileTypeGroup" multiple="true" displayAs="twoSelects"/>
<!--- submit file type IDs to exclude (default: none) --->
<!--- <cfparam name="filetypeids" default="0" /> --->
<!--- submit earliest date of activities to include (default: none)--->
<cf_param name="fromDate" label="From" RelayFormElementType="date" default="" />
<!--- submit latest date of activities to include (default: none)--->
<cf_param name="toDate" label="To" RelayFormElementType="date" default="" />
<!--- submit number of previous days to include (default: none)--->
<cf_param name="daysDuration" label="Duration (in days)" type="integer" default="0" />
<!--- submit phrase prefix to use in header --->
<cf_param name="phraseprefix" label="Phrase Prefix" type="string" default=""/>

<!--- return records of N most popular downloads --->
<cfset topNdownloads = application.com.fileManager.gettopNdownloads(norequested=#norequested#,FileTypeGroupID=#FileTypeGroupID#,fromDate=#fromDate#,toDate=#toDate#,daysDuration=#daysDuration#)>

<!--- present above results as numbered list of hyperlinks --->
<section id="topDownloads">
	<cfoutput><h3>phr_#htmleditformat(phraseprefix)#top_downloads_title</h3></cfoutput> <!--- heading of widget --->
	<ol>
 		<cfoutput query="topNdownloads" group="fileID">
	   		<cfset downlink = application.com.security.encryptURL('/fileManagement/fileGet.cfm?fileID=#fileID#&personID=#request.relayCurrentUser.personId#')> <!--- URL of file --->
			<li>
				<!--- open file as new window --->
				<a href="#downlink#" onclick="openWin(this.href,'FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'); return false;">#topNdownloads.name#</a> <!--- Use filename as hyperlink text (NB: assumption this will always exist) --->
			</li>
		</cfoutput>
	</ol>
</section>
