<cf_param requiredFunction="return jQuery('[name=returnType] option:selected').val() == 'workflow'" label="Return to Workflow" name="ReturntoWorkflow" default="" validValues="select processID as value, processdescription as display from processheader"/>
<cf_param label="authorisationServer" name="authorisationServer" validValues="func:com.oauth2.getValidServers(activeOnly='true')" display="serverName" value="clientIdentifier"/>

<CF_OAuth2AuthRequestLaunchPost OAuth2ID="#authorisationServer#" autolaunch="true" beginWorkflow="#ReturntoWorkflow#">