﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---

NJH 2006/09/27 - based on sony1ConfirmParticipation
WAB 2008-04-30    Someone chnaged the form name to myForm3 to prevent clash with myForm2 in a border set, but didn't release to Lexmark Site
					I came along to fix the bug and decided to change myForm3 to pNumberLoginForm o prevent future errors
2008-11-05 AJC P-SOP1 - Hide P Number
 --->

<cfsilent>

<cf_param name="startNew" default="yes" type="boolean"/>
<cfparam name="level" default="2">
<cfparam name="pp" default="">
<cf_param name="phraseprefix" default=""/>
<cf_param name="goToEID" default=""/>
<cf_param name="showRegisterLink" type="boolean" default="no"/>
<cf_param name="registerEID" default=""/>
<cf_param name="processID" type="numeric" default="0"/>
<cf_param name="stepID" type="numeric" default="0"/>
<!--- START: 2008-11-05 AJC P-SOP1 - Hide P Number --->
<cf_param name="showPNumber" type="boolean" default="yes"/>
<!--- END: 2008-11-05 AJC P-SOP1 - Hide P Number --->
<cf_param name="message" default=""/>
<cfparam name="messagePhrase" default="">

<!--- WAB added 2005-11-30 - basically if we already know who they are even if not logged in (probably from a comm click thru), we can populate the pNUmber for them --->
<cfif pp is "" and not request.relaycurrentuser.isUnKnownUser>
	<cfset pp = request.relaycurrentuser.pNUmber>
</cfif>

</cfsilent>

<cfif showRegisterLink and registerEID eq "">
	<cfoutput>registerEID is a parameter that must be passed.</cfoutput>
	<CF_ABORT>
</cfif>

<cfif goToEID eq "" and processID eq "0">
	<cfoutput>goToEID or processID must be passed as a parameter</cfoutput>
	<CF_ABORT>
</cfif>

<cf_translate>
<SCRIPT type="text/javascript">
	function verifyMainForm() {

		var msg = "";
		// first verify the Relay Screen
		var screenValidation = verifyInput ();
		if (screenValidation.length != 0)
			{
			msg += "phr_JS_YouMustCompleteTheFollowing :\n\n";
			msg += screenValidation;
			}
		var form = document.mainForm;

		if (msg.length == 0)
		{
			//Don't seem to be any errors on the form
			form.submit();
		}
		else
		{
			alert(msg);
		}
	}
</SCRIPT>

<CFOUTPUT>

	<div id="#phraseprefix#LoginForm" class="#phraseprefix#RootDiv">

	 <cfscript>
		if(structKeyExists(form,"pNumber") and structKeyExists(form,"lastname")){
			if (listLen(form.pNumber,"-") is 2 and len(form.lastname) gt 0) {
				checkLogin = application.com.login.autoAuthenticateUser(listFirstP=listFirst(pNumber,"-"),listLastP=listLast(pNumber,"-"),lastname=form.lastname);
				if (checkLogin.recordcount is 1) {
					//  WAB 2006-06-17 shouldn't log them in, change to just initialising
					// application.com.login.externalUserLoginProcess (CheckLogin.personid) ;
					application.com.login.initialiseAnExternalUser(CheckLogin.personid) ;
					form.addContactSaveButton = true ;  // this is a complete hack so that the sony1registration-stage2.cfm process will think that the person has just entered the registration process

				} else {
					request.fault = true;
				}
			} else {
				request.fault = true;
			}
		}
	</cfscript>


	<!-- LOGIN FORM -->
		<!--- display login form if
			1)  this is the first time hitting the page [form.action or form.Pnumber won't be defined]
			2)  there has been a login fault  - request.fault will have been set above
		--->
	<cfif (structKeyExists(request,"Fault") and request.Fault) or (not structKeyExists(form,"action") and not structKeyExists(form,"Pnumber"))  >

		<cf_includejavascriptonce template = "/javascript/verifyPNumber.js">

		<SCRIPT type="text/javascript">
			<!--
			function loginFormSubmit(){
				form = document.forms["#phraseprefix#pNumberLoginForm"];
				if (form.pNumber.value == ""){
					alert("phr_LoginScreen_JS_PleaseEnterYourPNumber");
					form.pNumber.focus();
					return
				}
				else if (!verifyPNumber(form.pNumber)){
					alert("phr_LoginScreen_JS_PleaseEnterPNumberInCorrectFormat");
					form.pNumber.focus();
					return
				}
				else if (form.lastname.value == ""){
					alert("phr_LoginScreen_JS_PleaseEnterLastName");
					form.lastname.focus();
					return
				}
				else {
					form.submit();
				}
			}
			//-->
		</script>

		<form action="/?#request.query_string#" method="post" name="#phraseprefix#pNumberLoginForm" id="#phraseprefix#pNumberLoginForm" >
			<cf_relayFormElement relayFormElementType="hidden" fieldName="reLogin" currentValue=""><!--- forces reauthentication and ignores existing cookies --->
			<cf_relayFormDisplay class="#phraseprefix#LoginStatusBox">
				<h3>
						phr_#htmleditformat(phraseprefix)#Confirm_LoginScreen_Title
				</h3>
				<CFIF structKeyExists(request,"Fault") and request.Fault>
					<p>phr_LoginScreen_JS_InvalidUsername</p>
				<cfelse>
					<p><b>phr_#htmleditformat(phraseprefix)#Confirm_LoginScreen_PleaseLogin</b></p>
				</CFIF>
				<div class="form-horizontal">
				<!--- START: 2008-11-05 AJC P-SOP1 - Hide P Number --->
				<cfif showPNumber>
						<div class="form-group row">
							<div class="col-xs-12 col-sm-3 control-label">
						<label for="pNumber">phr_#htmleditformat(phraseprefix)#Confirm_LoginScreen_IDCode</label>
							</div>
							<div class="col-xs-12 col-sm-9">
						<cf_relayFormElement relayFormElementType="text" fieldName="pNumber" currentValue="#pp#" size="20">
							</div>
						</div>
				<cfelse>
					<cf_relayFormElement relayFormElementType="hidden" fieldName="pNumber" currentValue="#pp#">
				</cfif>
				<!--- END: 2008-11-05 AJC P-SOP1 - Hide P Number --->
					<div class="form-group row">
						<div class="col-xs-12 col-sm-3 control-label">
						<label for="lastname">phr_#htmleditformat(phraseprefix)#Confirm_LoginScreen_Password</label>
						</div>
						<div class="col-xs-12 col-sm-9">
						<cf_relayFormElement relayFormElementType="text" fieldName="lastname" currentValue="" size="20">
						</div>
					</div>
					<div class="form-group row">
						<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
						<cf_relayFormElement relayFormElementType="button" fieldName="CmdSUBMIT" currentValue="phr_SUBMIT" class="#phraseprefix#submitbutton" onclick="javascript:loginFormSubmit()">
						</div>
					</div>

				<cfif showRegisterLink>
						<div class="form-group row">
							<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
								<a href="/?etid=#registerEID#&action=add" class="btn btn-primary kill-span"><span>phr_Register</span></a>
							</div>
						</div>
				</cfif>

				<CFIF Isdefined("AppName") and appname is not "" and appname is not "default">
					<cf_relayFormElement relayFormElementType="hidden" fieldName="Level" currentValue="#Level#">
					<cf_relayFormElement relayFormElementType="hidden" fieldName="AppName" currentValue="#AppName#">
				</CFIF>

				<cfif isDefined("urlRequested")>
					<cf_relayFormElement relayFormElementType="hidden" fieldName="urlRequested" currentValue="#urlRequested#">
					<cf_relayFormElement relayFormElementType="hidden" fieldName="Level" currentValue="#Level#">
				</cfif>
				</div>
			</cf_relayFormDisplay>
		</form>
	<cfelse>
		<cfif processID neq "0">
			<cfset frmProcessID = processID>
			<cfset frmStepID = stepID>
			<cfset frmPersonID = request.relayCurrentUser.personID>
			<cfinclude template="/proc.cfm">
		<cfelse>
			<cflocation url="/?eid=#goToEID#"addToken="false">
		</cfif>
	</cfif>
</div>

</cfoutput>

</cf_translate>

