<!---
2015-02-05	RPW	Reverted Company Leads Relay Tag Changes
2015-03-10	RPW Added phrases for buttons
2015-03-20	RPW Added ability to include custom JavaScript
2015-06-12	MS	P-CML001 Kanban 957 Ability to Hide Add new record button by setting a param hideAddbtn using RW Content Manager
2015/09/11	PYW #445888 Change showCurrentUsersCountriesOnly value to a variable
2015/09/30	NJH	PROD2015-35 - added param for column filters and date filter
2015-10-19	ACPK	PROD2015-212 Changed userRightsCountriesOnly to cf_param, default to false
2015/11/12 	RJT 	PROD2015-374 - Allowed leads to be converted on portal
2015-11-30  SB		Bootstrap
2016-02-11	RJT	PROD2015-374 Portal lead conversion - display converted lead after conversion
2016-09-25  DAN PROD2016-2387 - filter leads by type (new tag parameter)
--->

<cf_param name="userRightsCountriesOnly" type="boolean" default="false" />
<cf_param name="filterColumns" default="Country,leadApprovalStatus" validValues="Country,LeadApprovalStatus" multiple="true" />
<cf_param name="dateFilter" default="" validValues="Created"/>
<cf_param name="showOpportunityOnConversion" description="Show Opportunity On Conversion (if portal conversion is active)" type="boolean" default="true"/>
<cf_param name="leadTypeID" default="-1" label="Lead Type" validValues="func:com.relayForms.getLookupList(fieldname='leadTypeID')" display="translatedItemText" value="LookupID" />
<cf_param name="showTheseColumns" label="Show These Columns" multiple="true" displayAs="twoSelects" allowSort="true" type="string" validValues="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created" default="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created"/>

<cfif structKeyExists(url,"opportunityViewToken")>
	<cfscript>
		//we've recieved a token to view an opportunity (that was just converted by webservice). We redirect to view it
		//This has to be a 2 stage process to get the encrption right
		token=new com.tokens.token(tokenTypeTextID="opportunityViewToken",token=url.opportunityViewToken);
		token.pullFromDatabase();

		if(token.isValidToken()){
			location(application.com.security.encryptURL('/index.cfm?eid=#request.currentElement.id#&frmTask=edit&opportunityid=#token.get("opportunityID")#'), false, 307);
		}else{
			location("/?eid=#request.currentElement.id#", false, 307);
		}


	</cfscript>
<cfelseif showOpportunityOnConversion and structKeyExists(url,"opportunityid")>
	<!---Displaying a lead that was just converted to an opportunity --->
	<cfset returnURL="/?eid=#request.currentElement.id#"> <!---We very much do not what to be going back to the referer when we save, so we just stay on the same page when saving  --->
	<cfinclude template="/RelayTagTemplates/showPartnerRepLeadsAndOpps.cfm">
<cfelse>

	<cfparam name="leadID" type="numeric" default=0>
	<cfset defaultHideAddbtn=request.relayCurrentUser.isInternal?NOT application.com.settings.getSetting("leadmanager.addLeadOnInternal") : NOT application.com.settings.getSetting("leadmanager.addLeadOnExternal")>
	<cfparam name="hideAddbtn" type="boolean" default='#defaultHideAddbtn#'><!--- 2015-06-12	MS	P-CML001 Kanban 957 Ability to Hide Add new record button by setting a param hideAddbtn using RW Content Manager --->
	<!--- PYW #445888 Change showCurrentUsersCountriesOnly value to a variable --->
	<!--- 2015-10-19	ACPK	PROD2015-212 Changed userRightsCountriesOnly to cf_param, default to false --->



	<cfset filterColumnList = filterColumns>

	<cf_head>
		<cfoutput>
			<style>
				##frmRowIdentity{
					position: relative;
				}
				##actionContainer{
					float:right
				}
				##save{
					float:left;
					padding-right:10px
				}
				##delete{
					float:right
				}
			</style>

			<script type="text/javascript">
				var eid=#request.currentElement.id#;

				function editLead(leadID) {
					location.href = location.href+'&editor=yes&add=no&leadID='+leadID;
				}

				function addNew() {
					location.href = location.href+'&editor=yes&add=yes';
				}

				function onSuccessfulConvert(opportunityViewToken){
					<cfif showOpportunityOnConversion>
						<!---We redirect with a token instructing the page to display the opportunity --->
						location.href='/?eid=#request.currentElement.id#&opportunityViewToken=' + opportunityViewToken;

					<cfelse>
						backToList();
					</cfif>
				}

				function backToList() {
					location.href = '/?eid=#request.currentElement.id#';
				}

				function preSubmit(btn) {
					form = document.editorForm;

					//2014-10-06	RPW		Lead screen not saving and displaying message - added editor=yes
					//2014-04-08	MS		We also need to pass in ADD=YES to make sure that the code knows what to do oe either show a listing screen or the edit form
					jQuery(form).attr("action","/?eid=#request.currentElement.id#&editor=yes&add=yes")

					FormSubmit(btn);
				}

				runCustomFunction = function(){}

			</script>
		</cfoutput>
		<!--- 2015-03-20	RPW Added ability to include custom JavaScript --->
		<cfif NOT request.relayCurrentUser.isInternal AND fileExists("#application.paths.code#\cftemplates\Modules\leads\javascript.cfm")>
			<cfinclude template="/code/cftemplates/Modules/leads/javascript.cfm">
		</cfif>
	</cf_head>


	<cfif structKeyExists(url,"leadID")>
		<cfset leadID=url.leadID>
	</cfif>
	<cfif structKeyExists(form,"leadID")>
		<cfset leadID=form.leadID>
	</cfif>


	<cf_includeJavascriptOnce template ="/lead/js/leadPortal.js">

	<!--- 2015-03-10	RPW Added phrases for buttons --->
	<cfif NOT structKeyExists(url,"editor") AND NOT hideAddbtn><!--- 2015-06-12	MS	P-CML001 Kanban 957 Ability to Hide Add new record button by setting a param hideAddbtn using RW Content Manager --->
		<cf_input type="button" value="phr_lead_Add" onClick="addNew();">
	<cfelseif structKeyExists(url,"editor")>
		<cf_input type="button" value="phr_lead_Back" onClick="backToList();">
	</cfif>

	<cfif leadID NEQ 0 and application.com.leads.leadIsConverted(leadID)>
		<!--- Pretty much the only way this will display is via a saved link --->
		<p>phr_leadApprovalStatusID_Converted</p>
	<cfelseif not structKeyExists(url,"editor")>
		<!---we display the list --->

		<cf_include template="/lead/leads.cfm">

	<cfelse>
		<cf_include template="/lead/leads.cfm">
		<cfoutput>
			<div id="actionContainer">
				<div id="save" class="form-group">
					<cf_input type="button" id="saveButton" value="phr_Save" onClick="runCustomFunction();preSubmit(this);">
					<cfif NOT structKeyExists(url,'add') or url.add EQ "no">
						<!--- only convert existing leads--->

						<cfif application.com.settings.getSetting("leadManager.convertLeadOnPortal")  AND application.com.leads.leadCanBeConverted(leadID) AND application.com.leads.personCanConvertLead()>
							<cf_input type="button" id="portalConvertLink" value="phr_Convert">
						</cfif>
						<cf_input id="deleteLink" type="button" value="phr_delete">
					</cfif>
				</div>
			</div>
		</cfoutput>
	</cfif>
</cfif>


