<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			portalTestingTools.cfm	
Author:				SWJ
Date started:		
	
Description:		This is designed for resetting accounts during testing.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfoutput>

<p>Session.FullPersonDetails is currently: </p>
	<cfif isDefined("session.FullPersonDetails")>
		<cfdump var ="#session.FullPersonDetails#">
	<cfelse>
	Not defined
	</cfif>

<p>Personal FlagID to set on login: 
	<cfif isDefined("session.setThisPersonFlagOnLogin")>
	#htmleditformat(session.setThisPersonFlagOnLogin)#
	<cfelse>
	Not defined
	</cfif>
</p>


</cfoutput>
