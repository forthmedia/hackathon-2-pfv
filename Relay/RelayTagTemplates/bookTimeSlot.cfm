<!--- �Relayware. All Rights Reserved 2014 

2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
--->
<cfparam name="bookingBuffer" type="numeric" default="0"><!--- the number of hours from current hour until the next booking slot --->
<cfparam name="currentDay" type="date" default="#now()#">
<cfparam name="resourceID" type="numeric" default="0">

<CFIF Not IsDefined("prodID")>
	You must pass prodID to bookTimeSlot.cfm
	<cfexit method="EXITTEMPLATE">
</CFIF>

<CFIF Not IsDefined("orderID")>
	You must pass orderID to bookTimeSlot.cfm
	<cfexit method="EXITTEMPLATE">
</CFIF>

<cfquery name="getResources" datasource="#application.siteDataSource#">
	select r.resourceID,resourceName 
		from resource r 
		INNER JOIN resourceProduct rp ON r.resourceID = rp.resourceID
	and active = 1 <!--- SWJ added to temporarily supress booking inactive resources --->
	where rp.productID =  <cf_queryparam value="#prodID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by resourceName
</cfquery>
<cfif isDefined("resourceID") and resourceID eq 0>
	<cfset resourceID = getResources.resourceID[1]>
</cfif>

<cfswitch expression="#dayofweek(currentDay)#">
	<cfcase value="1"><!--- Sunday --->
		<cfset startDate = dateadd("d",1,currentDay)>
		<cfset endDate = dateadd("d",5,currentDay)>
	</cfcase>
	<cfcase value="2"><!--- Monday --->
		<cfset startDate = currentDay>
		<cfset endDate = dateadd("d",4,currentDay)>
	</cfcase>
	<cfcase value="3"><!--- Tuesday --->
		<cfset startDate = dateadd("d",-1,currentDay)>
		<cfset endDate = dateadd("d",3,currentDay)>
	</cfcase>
	<cfcase value="4"><!--- Wednesday --->
		<cfset startDate = dateadd("d",-2,currentDay)>
		<cfset endDate = dateadd("d",2,currentDay)>
	</cfcase>
	<cfcase value="5"><!--- Thursday --->
		<cfset startDate = dateadd("d",-3,currentDay)>
		<cfset endDate = dateadd("d",1,currentDay)>
	</cfcase>
	<cfcase value="6"><!--- Friday --->
		<cfset startDate = dateadd("d",-4,currentDay)>
		<cfset endDate = currentDay>
	</cfcase>
	<cfcase value="7"><!--- Saturday --->
		<cfset startDate = dateadd("d",-5,currentDay)>
		<cfset endDate = dateadd("d",6,currentDay)>
	</cfcase>
</cfswitch>

<cfif isDefined("frmDate") and isDate(frmDate) and isDefined("frmTime") and isNumeric(frmTime)>
	<!--- <cfoutput>frmDate=#frmDate#  frmTime=#frmTime#</cfoutput> --->
	<cfquery name="getResourceDetails" datasource="#application.siteDataSource#">
		select resourceID,resourceName from resource 
		where resourceID =  <cf_queryparam value="#resourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>

	<cfquery name="getPersonDetails" datasource="#application.siteDataSource#">
		select firstname, lastname, email from person where personid = #request.relayCurrentUser.personid#
	</cfquery>

	<cfif isDefined("frmAction") and frmAction eq "book">
		<cfquery name="insertBooking" datasource="#application.siteDataSource#">
			INSERT INTO resourceTimeSlot([personID], [timeSlotDate], [timeSlot], resourceID, orderid)
			VALUES (<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#frmDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#frmTime#" CFSQLTYPE="CF_SQL_tinyint" >, <cf_queryparam value="#resourceID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#orderid#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</cfquery>
		<cf_mail to="#getPersonDetails.email#" 
			from="#application.salesEmail#" 
			subject="#application.clientName# eLabs Equipment Booking Confirmation" 
			bcc="#bccemail#">
Dear #getPersonDetails.firstname#,

This email confirms your booking on #getResourceDetails.resourceName# on #frmDate# #frmTime#.



		</cf_mail>

	</cfif>
	<cfif isDefined("frmAction") and frmAction eq "cancel">
		<cfquery name="cancelBooking" datasource="#application.siteDataSource#">
			Delete from resourceTimeSlot
			where personid=#request.relayCurrentUser.personid# and  timeSlotDate =  <cf_queryparam value="#frmDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
			and timeSlot =  <cf_queryparam value="#frmTime#" CFSQLTYPE="CF_SQL_tinyint" >  and resourceID =  <cf_queryparam value="#resourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cf_mail to="#getPersonDetails.email#" 
			from="#application.salesEmail#" 
			subject="#application.clientName# eLabs Equipment Booking Cancelation" 
			bcc="#bccemail#">
Dear #getPersonDetails.firstname#,

This email confirms that your booking on #getResourceDetails.resourceName#
on #frmDate# between #frmTime#:00 and #evaluate(frmTime + 1)#:00 has been cancelled.</cf_mail>
	
</cfif>

</cfif>

<cfquery name="getTimeSlots" datasource="#application.siteDataSource#">
	select distinct personID,timeSlotDate,timeSlot from resourceTimeSlot 
	where timeSlotDate > '#dateFormat(dateAdd("d",-1,startDate),"dd-mmm-yy")#' 
		and timeSlotDate < '#dateFormat(dateAdd("d",1,endDate),"dd-mmm-yy")#'
		and resourceID =  <cf_queryparam value="#resourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<script>
function bookSlot(startDate,thisTime){
	var form = document.thisForm;
	form.frmDate.value = startDate;
	form.frmTime.value = thisTime;
	form.frmAction.value = "book";
	form.resourceID.value = document.bookTimeSlotResourceForm.resourceID.value;
	form.submit();
}

function cancelSlot(startDate,thisTime){
	var form = document.thisForm;
	form.frmDate.value = startDate;
	form.frmTime.value = thisTime;
	form.frmAction.value = "cancel";
	form.resourceID.value = document.bookTimeSlotResourceForm.resourceID.value;
	form.submit();
}

function goWeek(thisDate){
	var form = document.thisForm;
	form.currentDay.value = thisDate;
	form.resourceID.value = document.bookTimeSlotResourceForm.resourceID.value;
	form.submit();
}


</script>
<style>
	.smallText{
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-size : 9px;
	}
	.smallHeading{
		font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
		font-size : 10px;
		font-weight : bold;
		color : Gray;
	}
</style>

<cfform name="bookTimeSlotResourceForm">
<p>If the time slot you want is not available try selecting another resource from the drop down below.</p>
	<p>Resource: <cfselect name="resourceID" size="1" query="getResources" value="resourceID" display="resourceName" selected="#resourceID#" onChange="submit()"></cfselect></p>

</cfform> 
<cfoutput><a href="/?etid=eLabLauncher&prodid=#prodID#&orderID=#orderID#">Back to Launch Page</a><br></cfoutput>
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="3">
		<!--- <tr><td colspan="6"><cfoutput>#dateDiff("d",now(),startDate)#</cfoutput></td></tr> --->
		<tr>
			<cfif dateDiff("d",now(),startDate) gt 1>
				<cfset goBackDate = dateAdd("d",-7,startDate)>
				<cfset goToDate = dateAdd("d",3,endDate)>
				<cfoutput>
					<td colspan="3"><a href="javaScript:goWeek('#dateFormat(goBackDate,"dd-mmm-yy")#')">&lt;&lt;&lt; Previous week</a></td>
					<td colspan="3" align="right"><a href="javaScript:goWeek('#dateFormat(goToDate,"dd-mmm-yy")#')">Next week &gt;&gt;&gt;</a></td>
				</cfoutput>
			<cfelse>
				<cfset goToDate = dateAdd("d",3,endDate)>
				<cfoutput><td colspan="6" align="right"><a href="javaScript:goWeek('#dateFormat(goToDate,"dd-mmm-yy")#')">Next week &gt;&gt;&gt;</a></td></cfoutput>
			</cfif>
		</tr>
		<TR><CFOUTPUT>
				<th class="smallHeading">GMT Time slot</th>
				<cfset loopDate = startDate>
				<CFLOOP index="i" from="1" to="5" step="1">
					<th class="smallHeading">#dateFormat(loopDate,"dd-mmm-yy")#<br>#htmleditformat(DayOfWeekAsString(dayofweek(loopDate)))#</th>
					<cfset loopDate = dateadd("d",1,loopDate)>
				</CFLOOP>
			</CFOUTPUT>
		</TR>
		<CFLOOP index="h" from="8" to="17" step="1">
			<TR>
			<CFOUTPUT>
				<th class="smallHeading">#htmleditformat(h)#:00-#evaluate(h+1)#:00</th>
				<cfset loopDate = startDate>
				<CFLOOP index="i" from="1" to="5" step="1">
					<td align="center"><!--- #dateDiff("h",now(),CreateDateTime(year(loopDate),month(loopDate),day(loopDate),h,00,00))# ---> <!--- #CreateDateTime(year(now()),month(now()),day(now()),h,00,00)# ---><!---#dateDiff("h",now(),loopDate)# #h# #CreateDateTime(year(now()),month(now()),day(now()),hour(dateadd("h",bookingBuffer,now())),00,00)# #dateCompare(loopdate,bookedDate,"d")# --->
					<cfif dateDiff("h",now(),CreateDateTime(year(loopDate),month(loopDate),day(loopDate),h,00,00)) gte bookingBuffer> 
						<cfquery name="checkThisDate" dbtype="query">
						select personid, timeSlotDate from getTimeSlots 
							where timeSlotDate= '#dateformat(loopdate,"yyyy-mm-dd")# 00:00:00.000' 
							and timeSlot = #h#
						</cfquery><!--- #checkThisDate.personid# #checkThisDate.timeSlotDate#--->
						<cfif checkThisDate.recordCount gt 0><!--- i.e. they are at the same position in the array --->
							<cfif checkThisDate.personid eq #request.relayCurrentUser.personid#>
								<a href="javascript:cancelSlot('#dateformat(loopdate,"dd-mmm-yy")#','#h#')">Cancel</a>
							<cfelse>
								Booked
							</cfif>
						<cfelse>
							<a href="javascript:bookSlot('#dateformat(loopdate,"dd-mmm-yy")#','#h#')">Book</a>
						</cfif>
					</cfif>&nbsp;</td>
					<cfset loopDate = dateadd("d",1,loopDate)>
				</CFLOOP>
			</CFOUTPUT>
			</TR>
		</CFLOOP>
	</TABLE>
<cfoutput>
	<form method="post" name="thisForm">
		<CF_INPUT type="hidden" name="frmPersonID" value="#request.relayCurrentUser.personid#">
		<input type="hidden" name="frmDate" value="">
		<input type="hidden" name="frmTime" value="">
		<input type="hidden" name="frmAction" value="">
		<input type="hidden" name="resourceID" value="">
		<CF_INPUT type="hidden" name="currentDay" value="#currentDay#">
	</form>
</cfoutput>

