<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			managePeopleEditScreenIFrame.cfm
Author:				SWJ
Date started:		2005-08-17

Description:		This is used to provide a page for managing a list of records

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-04-25			AJC			Add div to hid or display page
2006-04-25			AJC			Fixed people list reload
2007-02-05			WAB 		added check to make sure not used by the unknown user
2007-09-24			SSS			Job function Group allows the passing of a flaggroup of job functions
2008/10/03			NJH			Bug Fix T-10 Issue 932 - changed salutation to be a dropdown
2008/10/03			NJH			Came across another bug.. The person listing screen that calls this page returns all active and non-active people.
								However, the query that returns this person's details checks if they're active and so no records were getting returned. Removed the check.
2009/01/30			NJH			Bug Fix All Sites Issue 1709 - changed currentValue and currentRecord from getlanguage.language to the person's language
2009/04/16			WAB 		When a custom screen was included we ended up with the updatedata hidden fields being duplicated,
2009-06-02 			NYB 		LHID2300 - translation of Salutation different in popup to that displayed on label - have made them the same
2009/07/21			NJH			P-FNL069 - only check for rights if personID is not 0; Otherwise set rightsOK to be false.
2009/09/23			WAB		LID 2663.  Check whether person has rights to add new people - use same function as update data
						Also limit the location drop down based on whether person has rights to all locations within organisation
						Might be slightly academic since most clients give the main contact rights to the whole organisation
						Note that haven't disabled the add new location button even if user only has 'ownlocation' rights - again probably academic
2009/09/25			NJH			LID 2663 - changed a bit of the functionality. Originally, if personID was "", then we went through the whole page
								and drew the div, but set the style of the div to hidden. Errors occured though, as some of the rights variable were set
								only if personID was not "". I've essentially not done anything if personID is "", but only run through the code if personID is not "".
2010/04/21			NJH			P-PAN002 - allow adding of people that are not in your organisation. This is done by giving temporary rights to the person for the organisation.
								It was introduced by the Panda project as they wanted partners to be able to add end customers for their opportunities.
								Added encrypt hidden fields and relayHeaderFooter tag.
2011/07/19			PPB			LEN024 add switch showPersonEditForm to allow profile screen to be used instead of hard-coded form; default="true" Lenovo="false" for manage my colleagues
2011/08/10 			PPB 		LID7031 toggle the 'add new end customer' header
2012-03-27 			PPB 		P-LEX070 showJobFunction and showAddLocationLink settings
2013-03-13			NJH			Case 433840
2014-01-31			WAB 	removed references to et cfm, can always just use / or /?
2014-10-30			AHL			Case 442292 CF_relayFormDisplay was out of the cfform
2015/04/21			NJH			Pass organisation and location records into the screen
2015/06/10			NJH			Pass in email validation attributes if unique email validation is on. Also apply a phone mask
2016-03-10			WAB			Add blank class to submit buttons to prevent default form-control class
14/04/2016          DAN         Case 448829 - make title field non-mandatory
Possible enhancements:


 --->

<!--- <CFPARAM name="frmlanguageid" default="#iif(isdefined('cookie.defaultlanguageid'),DE(cookie.defaultlanguageid),DE('1'))#"> --->
<!--- GCC 2009/06/23 P-FNL069 Security Update --->
<cfif not request.relayCurrentUser.isUnknownUser and request.relayCurrentUser.isLoggedIn>
<cfif not request.relaycurrentuser.isinternal>
	<cf_displayBorder
		noborders=true
	>
</cfif>
	<cf_includejavascriptonce template = "/javascript/openWin.js">

	<!--- P_CHW001 - Moved from further down and default set to null --->
	<CFPARAM name="url.personid" default="">
	<CFPARAM name="url.screenids" default="">
	<!--- <CFPARAM name="frmLanguageID" default="1"> --->
	<CFPARAM name="frmLanguage" default="English">
	<cfparam name="listofvalidjobfunctions" type="string" default="MainContact,ManagingDirector,FinancialManager,marketingManager,SalesContact,ServiceContact,PurchaseContact">

	<cfparam name="showPersonEditForm" default="true">		<!--- 2011/07/19 PPB LEN024 added showPersonEditForm --->

	<cfparam name="showJobFunction" default="#not (application.com.settings.getSetting('AddContact.showJobFunction') eq 0)#">	<!--- 2012/03/27 PPB P-LEX070 showJobFunction; I use the "not eq 0" construction so if the custom setting doesn't exist in XML (ie equals empty string) the default is true  --->

	<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl --->
	<cfparam name="hideFieldsControl" default="" type="string"/>

<!--- 2007/10/18 SSS check if already quoted before quoting
		2012-01-14 WAB - list must now be unqualified for cf_queryparam, so remove any qualifiers
--->
	<cfif left(listofvalidjobfunctions,1) eq "'">
		<cfset listofvalidjobfunctions = replace(listofvalidjobfunctions,"'","","all")>
	</cfif>

	<!--- NJH 2009/07/21 P-FNL069 initialise some vars --->
	<cfset rightsOK = true>
	<cfif structKeyExists(variables,"giveTemporaryRights") and variables.giveTemporaryRights and structKeyExists(variables,"tempOrganisationID")>
		<cfset entityOrgID = variables.tempOrganisationID>
	<cfelse>
		<cfset entityOrgID = request.relayCurrentUser.organisationID>
	</cfif>

	<!--- 2006-04-25 AJC P_CHW001 - Hides page on section load, shows on add or edit --->

	<!--- NJH 2009/09/25 LID 2663
		it would appear that if url.personID is  "", then the whole div is hidden. However, we were getting errors as code below references
		some variables that get set when url.personID is not "". I therefore have simply only put an if around this whole block, so that if
		url.personID is "", then dont' do anything.
	--->
	<!--- NJH 2009/09/25 LID 2663 commented out this if statement
	<cfif url.personid is "">
		<style>
			#ContentDisplayArea{visibility:hidden;}
		</style>
	<cfelse> --->

	<cfif url.personID is not "">
		<style>
			#ContentDisplayArea{visibility:show;}
		</style>

	<!--- GCC 2009/06/23 P-FNL069 Security Update --->
		<!--- NJH 2009/07/21 P-FNL069 moved this into if clause --->
		<!--- WAB 2009/09/23 LID 2663 check ownrecord rights--->

		<!--- NJH 2010/04/22 P-PAN002 removed this function and replaced it with getPOLRights for the organisation.
		<cfset getOwnRecordTaskRights = application.com.login.checkInternalPermissions('ownRecordTask')> --->

		<cfset entityStruct = application.com.relayPLO.getEntityStructure(entityID=entityOrgID,entityTypeID=2)>
		<cfset orgRights = application.com.rights.getPOLRights(entityTypeID=2,entityStruct=entityStruct,entityID=entityOrgID)>

		<cfif url.personID neq 0>
			<cfset getEntityDetails = application.com.relayplo.getEntityStructure(entityTypeID = 0,entityid = url.personid,getEntityQueries=false)>
			<cfset rights = application.com.rights.getPolrights(entityStruct = getEntityDetails)>
			<CFIF (rights.recordcount is 0 or rights.updateOkay is 0) >
			       <CFSET rightsOK = false>
			<CFELSE>
			       <CFSET rightsOK = true>
			</cfif>
			<cfset entityOrgID = getEntityDetails.ID.organisation> <!--- NJH 2009/07/21 P-FNL069 --->
		<cfelse>
			<!--- WAB 2009/09/23 LID 2663 check ownrecord rights
				need to check whether person has rights to edit own organisation records
				may also just have rights to just own location or own country, this would limit the location drop down
				P-PAN002 2010/04/21 NJH - changed from getOwnRecordTaskRights to orgRights
			--->
			<cfif orgRights.recordCount is 0 or not orgRights.ownLocationPeople>
				<cfset rightsOK = false>
			</cfif>

		</cfif>

	<!--- </cfif> NJH 2009/09/25 LID 2663 commented out --->
		<cfif ((structKeyExists(variables,"giveTemporaryRights") and variables.giveTemporaryRights and structKeyExists(variables,"tempOrganisationID")) or entityOrgID eq request.relaycurrentuser.organisationID) and rightsOK>
		<div id="ContentDisplayArea">
		<cfif isDefined("URL.personid") and isNumeric(URL.personID)>
			<cfset variables.personid = URL.personID>
		<!--- 2006-04-25 AJC P_CHW001 - Added due to cfparam now being blank --->
		<cfelse>
			<cfset variables.personid = 0>
		</cfif>

		<cfif isDefined("doUpdate") and doUpdate eq "yes">
			<!--- <CFINCLUDE TEMPLATE="/screen/updatedata.cfm"> --->
			<cf_updateData>

			<cfif isdefined("message")>
			<cfoutput>
			<p class="messageText">#application.com.security.sanitiseHTML(message)#</p>
			</cfoutput>
			</cfif>
			<script>
				// 2006-04-25 AJC P_CHW001 - Added to reload the people list, deleted non working code to refresh current frame which is not required
				// checks iframe exists
				if(parent.frames.peopleList)
				{
					// reloads people list
					parent.frames.peopleList.location.reload();
				}
			</script>
		</cfif>

		<!--- 2006-04-25 AJC P_CHW001 - moved cfparams further up the code --->
		<!--- locid must be passed to this template --->
		<!--- GCC 2009/06/23 P-FNL069 Security Update --->
		<!--- NJH 2010/04/21 P-PAN002 - give temporary rights for an organisation that may not be the current users. It's a bit of a loophole, but
			not one that we are going to advertise....  --->
		<cfif structKeyExists(variables,"giveTemporaryRights") and variables.giveTemporaryRights and structKeyExists(variables,"tempOrganisationID")>
			<cfset orgID = variables.tempOrganisationID>
		<cfelse>
			<cfset orgID = request.relayCurrentUser.organisationID>
		</cfif>


		<!--- ==============================================================================
		      CHECK THE NUMBER OF LOCATIONS FOR THIS ORGID
		=============================================================================== --->

		<!--- P-PAN002 2010/04/21 NJH - changed from getOwnRecordTaskRights to orgRights --->
		<CFQUERY NAME="getLocsForThisOrg" datasource="#application.siteDataSource#">
			select
				locationID,
				sitename+' ('+address4+' '+postalcode+')'  as locdetails
			from
				location
			where
				organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- WAB 2009/09/23 LID 2663 limit locs depending upon users rights --->
				<cfif not orgRights.ownOrganisationPeople>
					<!--- not rights to whole org, so need to limit to own country --->
					and location.countryid = #request.relaycurrentuser.countryid#
				</cfif>
				<cfif not orgRights.ownCountryPeople>
					<!--- not rights to own country, so need to limit to own location --->
					and location.locationid = #request.relaycurrentuser.locationid#
				</cfif>


				order by sitename
		</CFQUERY>

		<cf_translate>
		<div class="managePeopleList" id="managePeopleList">
		<cfif getLocsForThisOrg.recordCount gt 0>
			<!--- if there are more that one location for this org put a drop down up
					so the user can select from the list --->
			<cfif not isDefined("form.frmLocationid")>
				<cfset form.frmLocationid = getLocsForThisOrg.locationID[1]>
			</cfif>
			<!--- <cfform method="post" name="locForm">
				<!--- <p style="font-weight: bold;">Select which site</p> --->
				<!--- the screen will re-fresh when the user selects from the drop down --->
				<cfselect name="PERSON_Locationid_#variables.personid#" query="getLocsforThisOrg" value="locationID" display="locdetails" selected="#form.frmLocationid#" onChange="document.locForm.submit()">	</cfselect>
				<!--- 2006-04-25 AJC P_CHW001 - moved cfparams further up the code --->
				<!--- 2006-04-25 AJC P_CHW001 - Add new Location --->
				<cfoutput>
					<SCRIPT type="text/javascript">
						function newLocation()
						<!--hide
						{
							openWin('/screen/showscreennew.cfm?location_organisationid_default=#request.relaycurrentuser.organisationid#&frmlocationid=new&frmcurrentscreenid=baselocationrecord&location_countryid_default=#request.relaycurrentuser.countryid#&frmnextpage=/templates/windowopenreload_windowclose.cfm','mywindow','width=550,height=500,scrollbars=yes,resizable=yes');
						}
						//-->
					</script>
					<input type="hidden" name="frmpersonidlist" value="#variables.personid#">
					<input type="hidden" name="doUpdate" value="yes">
					<a href="javascript:newLocation()">phr_managePeopleEditScreenIframe_addlocation</a>
				</cfoutput>
			</cfform> --->
		<cfelse>
			<cfset form.frmLocationid = 0>
		</cfif>

		<!--- locid must be passed to this template --->
		<cfif not isDefined("form.frmLocationid")>
			<p>You must define form.frmLocationid to use this template</p>
			<cfexit method="EXITTEMPLATE">
		</cfif>

		<CFPARAM name="locationid" default="#frmLocationid#">
		<!--- get details of location--->
		<CFQUERY NAME="getLocation" datasource="#application.siteDataSource#">
			select * from location where locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfset thisCountryID = 0>
		<cfset thisSiteName = "">

		<cfif getLocation.recordCount>
			<cfset thisCountryID = getLocation.countryID[1]>
			<cfset thisSiteName = getLocation.sitename[1]>
		<cfelse>
			<cfquery name="getOrganisation" datasource="#application.siteDataSource#">
				select countryID,organisationName from organisation where organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfif getOrganisation.recordCount>
				<cfset thisCountryID = getOrganisation.countryID[1]>
				<cfset thisSiteName = getOrganisation.organisationName[1]>
			</cfif>
		</cfif>

		<CFPARAM NAME="Locname" DEFAULT="#thisSiteName#">


		<!--- get list of possible languages--->
		<!--- NJH 2009/01/30 Commented out as all calls to this query are commented out .--->
	<!--- 	<CFQUERY NAME="GetLangList" datasource="#application.siteDataSource#">
			select language from language order by language
		</CFQUERY> --->

		<!--- get name of current language --->
		<!--- NJH 2009/01/30 Bug Fix All Sites Issue 1709 Commented out as not needed. language comes from person table --->
		<!--- <CFQUERY NAME="GetLanguage" datasource="#application.siteDataSource#">
			SELECT language from language where LanguageID = #frmlanguageID#
		</CFQUERY> --->


		<!--- get list of people at this location
			NJH 2008/10/03... the query that gets all the people in the listing screen returns all people, active and non-active, so I've changed this query
			to be the same as otherwise you're clicking a person in the listing screen to get their details and you get a blank record
		--->
		<CFQUERY NAME="getPerson" datasource="#application.siteDataSource#">
			<!---  2011/07/19 PPB LEN024 added p.jobDesc, p.faxPhone; I was tempted to SELECT * for future-proofing but a little more risky --->
			Select <!---p.personid, p.firstname, p.lastname, p.email, p.salutation, p.lastupdated, p.officePhone,p.mobilephone,p.locationid, p.language, p.organisationID, p.jobDesc, p.faxPhone--->
				*
			from person as p
			where personID =  <cf_queryparam value="#variables.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
			--and active = 1
			order by lastname
		</CFQUERY>

		<CFQUERY NAME="getOrganisation" datasource="#application.siteDataSource#">
			select * from organisation where organisationid =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

<cfif showJobFunction>
		<!--- query looks for a passed in flaggroupID and or flagtextID --->
		<CFQUERY NAME="getalljobfunctions" datasource="#application.siteDataSource#">
			select f.name, f.flagid, f.namephrasetextid, ft.name as flagtype, fg.flaggroupID, case when isNull(f.namephrasetextid,'') <> '' then f.name else 'phr_' + f.namephrasetextid end as translatedName
			from flag as f, flaggroup as fg, flagtype as ft
			where f.flaggroupid= fg.flaggroupid
			and fg.flagtypeID = ft.flagtypeID
			<cfif isdefined("session.JobsFlagGroup")>
				<cfif isnumeric(session.JobsFlagGroup)>
					and fg.flaggroupID =  <cf_queryparam value="#session.JobsFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
					and fg.flaggrouptextid =  <cf_queryparam value="#session.JobsFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
			<cfelse>
				and fg.flaggrouptextid='JobFunction'
				and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			</cfif>
			and f.active = 1
			and fg.active = 1
			order by f.name
		</CFQUERY>
		<!--- the ID of the flag to be changed is set here --->
		<cfset flagtypeGrouptoset = #getalljobfunctions.flagtype# & '_' & #getalljobfunctions.flaggroupID#>
</cfif>
		<CFSET addoredit=iif(getPerson.recordcount is 0,DE("phr_addpeoplefor"),DE("phr_editaddpeoplefor"))>

		<SCRIPT type="text/javascript">

		<!--
			function doTranslation(phraseid) {
			//-----------------

			newWindow = window.open( '../report/utilities/language_detail.cfm?frmphraseid='+phraseid, 'translator','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'  )
			newWindow.focus()
			}



		//-->

		</SCRIPT>

		<!---
		function verifyForm() {
				var form = document.frmmain;
				var msg = "";

				var userchecked = false
				for (j=0; j<form.elements.length; j++){
					thiselement = form.elements[j];
					if (thiselement.name == 'frmUser') {
							if (thiselement.checked==true)  {
								userchecked = true
										}
					}
				}



				if (msg != "") {
						alert("\n<CFOUTPUT>phr_Pleaseprovidethefollowinginformation</CFOUTPUT>:\n\n" + msg);

				} else {
					form.submit();
				}
			}
		 --->

		<!--- NJH 2009/01/30 Bug Fix All Sites Issue 1709 - Added language to fieldnames --->
		<CFSET personfieldsonpage="">

		<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
		<cfif showPersonEditForm>	<!--- 2011/07/19 PPB LEN024 added showPersonEditForm condition --->


			<cfif ListContainsNoCase(hideFieldsControl, "salutation") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "Person_Salutation")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "FirstName") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "Person_FirstName")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "LastName") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "Person_LastName")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "officePhone") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "Person_OfficePhone")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "MobilePhone") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "person_MobilePhone")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "email") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "Person_Email")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "language") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "person_language")>
			</cfif>

			<cfif ListContainsNoCase(hideFieldsControl, "location") EQ 0>
				<CFSET personfieldsonpage = ListAppend(personfieldsonpage, "person_locationid")>
			</cfif>
			<!--- 2011/07/19 PPB LEN024 fields defined here appear on the edit form (the profile screen included in the manage my colleagues ) as read-only; hence I prevent it happening --->
		</cfif>

		<CFSET personidList="">
		<CFOUTPUT>
		<!--- ACTION="/partners/AddPersonListTask.cfm" --->

		<!--- NJH 2015/06/10 - added for unique email validation --->
		<cfset emailAttributes = {}>
		<cfif application.com.relayPLO.isUniqueEmailValidationOn().isOn>
			<cfset emailAttributes = {personid = isNumeric(Personid)?personID:0, orgTypeID=getOrganisation.OrganisationTypeId,uniqueEmail=true,uniqueEmailMessage="phr_sys_formValidation_UniqueEmailRequired"}>
		</cfif>

		<CFFORM METHOD="POST" NAME="frmmain"> <!--- 2014-10-30 AHL Case 442292 CF_relayFormDisplay was out of the cfform --->
		<CF_relayFormDisplay class="">

		<CF_aSCREEN formName="frmmain" >
		<cf_encryptHiddenFields>
			<!---
				WAB 2009/04/16
				These variables (and personIDList are picked up by cf_ascreen and output as hidden fields at the end of the form.
				replaces previous method of just outputting hidden fields because this led to duplicates when a custom screen was included on the page
			--->

			<cfset tableList = "person">
			<cfset personfieldList = personfieldsonpage>
			<cfif isdefined("session.JobsFlagGroup")>
				<cfset personflagList = flagtypeGrouptoset>
			<cfelse>
				<cfset personflagList = "checkbox_jobfunction">
			</cfif>



		<CF_INPUT TYPE="Hidden" NAME="LocationID" VALUE="#LocationID#">
		<!--- <INPUT TYPE="Hidden" NAME="frmLocationID" VALUE="#LocationID#"> --->
		<INPUT TYPE="Hidden" NAME="doUpdate" VALUE="yes">
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#createODBCDateTime(now())#">
		<CFIF isdefined("frmProcessID")>
		<CF_INPUT TYPE="HIDDEN" NAME="frmProcessID" VALUE="#frmProcessID#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmStepID" VALUE="#NextStepID#">
		</cfif>


		<!---

		<TABLE>
			<TR><td colspan="3"><H3>#Locname#: #AddorEdit# </H3></td></TR>
		</TABLE> --->
				<CFIF isdefined("email")><CF_INPUT TYPE="Hidden" NAME="email" VALUE="#email#"></CFIF>

		<!--- <table border="0" cellspacing="3" cellpadding="0">
				<TH>#phr_Salutation#*</th>
				<th>#phr_FirstName#*</th>
				<TH>#phr_LastName#*</TH>
				<!--- <TH>#phr_emailAddress#*</TH> --->
				<TH>#phr_JobFunction#</TH>
				<!--- <TH>#phr_WhichRecordIsYours#</TH> --->
		</table> --->

		<cfif showPersonEditForm>		<!--- 2011/08/10 PPB LID7031 toggle the header based on this param too; while at it use a phase --->
			<h1>
				<cfif personID eq 0 or personID eq "">
					<cfif application.com.relayTranslations.doesPhraseTextIDExist('sys_addEndCustomerContact')>	<!--- check if phrase exists to protect against existing users not having it --->
						phr_sys_addEndCustomerContact
					<cfelse>
						Add New End Customer Contact
					</cfif>
				<cfelse>
					phr_sys_editEndCustomerContact
				</cfif>
			</h1>
		</cfif>
			<CFLOOP query="getPerson">
				<div id="managePeopleEditScreenIframe-1">
				<CFSET personidList=listappend(personidList,personid)>
				<cfset "person_lastupdated_#personid#" = lastupdated>	<!--- nasty hack to get ascreen to output the lastupdated hidden field --->

				<cfif showJobFunction>
					<!--- get list of all job functions, with field showing if this person has this job function. ordered with selected job functions first--->
					<CFQUERY NAME="getalljobfunctionsordered" datasource="#application.siteDataSource#">
						select f.name, f.flagid, case when isNull(f.namephrasetextid,'') <> '' then f.name else 'phr_' + f.namephrasetextid end as translatedName,
						case when exists (select * from booleanflagdata as bfd where entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >  and bfd.flagid=f.flagid) THEN 1 ELSE 0 END	as selected
						from flag as f, flaggroup as fg
						where f.flaggroupid= fg.flaggroupid
						<cfif isdefined("session.JobsFlagGroup")>
							<cfif isnumeric(session.JobsFlagGroup)>
								and fg.flaggroupID =  <cf_queryparam value="#session.JobsFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" >
							<cfelse>
								and fg.flaggrouptextid =  <cf_queryparam value="#session.JobsFlagGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfif>
						<cfelse>
							and fg.flaggrouptextid='JobFunction'
							and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
						</cfif>
						and f.active = 1
						and fg.active = 1
						order by selected DESC, f.orderingindex
					</CFQUERY>
				</cfif>

				<cfif showPersonEditForm>	<!--- 2011/07/19 PPB LEN024 added showPersonEditForm as a switchable option default="true" Lenovo="false" for manage my colleagues --->

					<cfif ListContainsNoCase(hideFieldsControl, "salutation") EQ 0>
						<cf_relayFormElementDisplay type="html" label="phr_ext_salutation" required=false>
								<cf_displayValidValues
									validFieldName = "person.salutation"
									formFieldName =  "Person_Salutation_#Personid#"
									displayas = "select"
									currentValue = #trim(salutation)#
									countryID = #thisCountryID#
									entityID = #personID#
									useCFFORM = true
									required = "false" <!--- Case 448829 --->
									>
								<CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#">
						</cf_relayFormElementDisplay>
					</cfif>

					<cfif ListContainsNoCase(hideFieldsControl, "FirstName") EQ 0>
						<div class="form-group">
							<label class="required">phr_firstname</label>
			 				<CF_relayFormElement class="narrow" relayFormElementType="text" currentValue="#trim(firstname)#" fieldName="Person_FirstName_#Personid#" size="15" label="phr_firstname" required="yes">
							<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="#trim(firstname)#">
						</div>
					</cfif>

					<cfif ListContainsNoCase(hideFieldsControl, "LastName") EQ 0>
						<div class="form-group">
							<label class="required">phr_lastname</label>
							<CF_relayFormElement class="narrow" relayFormElementType="text" currentValue="#trim(lastname)#" fieldName="Person_LastName_#Personid#" size="15" label="phr_lastname" required="yes">
							<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="#trim(Lastname)#">
						</div>
					</cfif>
					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - END --->

					<!--- PJP 14/12/2012 CASE: 431075: If lexmark, makes sure we use domain country id for valid value list translation --->
					<!--- START: 2013-03-13 NJH Case 433840 changed:--->
					<cfif structKeyExists(application,"siteDomainStruct") and structKeyExists(application.siteDomainStruct,cgi.http_host)>
						<cfset thisDomain = application.siteDomainStruct[cgi.http_host]>
						<cfif structKeyExists(thisDomain,"countryID")>
					<!--- END: 2013-03-13 NJH Case 433840 --->
							<cfset thisCountryID =  application.siteDomainStruct[cgi.http_host].countryid>
						</cfif>
					</cfif>

					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
					<cfif ListContainsNoCase(hideFieldsControl, "officePhone") EQ 0>
						<div class="form-group">
							<label>phr_officePhone</label>
							<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" VALUE="#trim(officePhone)#">
							<cfset phoneMask = application.com.commonQueries.getCountry(countryid=thisCountryID).telephoneformat>
							<CF_relayFormElement relayFormElementType="hidden" label="" currentvalue="#phonemask#" fieldname="phonemask">
							<CF_relayFormElement relayFormElementType="text" currentValue="#trim(officePhone)#" fieldName="Person_OfficePhone_#Personid#" size="15" mask="#phonemask#" noTR="START">
						</div>
					</cfif>

					<cfif ListContainsNoCase(hideFieldsControl, "MobilePhone") EQ 0>
						<div class="form-group">
							<label>phr_MobilePhone</label>
							<CF_relayFormElement relayFormElementType="text" FIELDNAME="Person_MobilePhone_#Personid#" SIZE="15" currentValue="#trim(mobilePhone)#" mask="#phonemask#">
							<CF_INPUT TYPE="Hidden" NAME="Person_MobilePhone_#Personid#_orig" VALUE="#trim(mobilePhone)#">
						</div>
					</cfif>
					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - END --->
					<cfif showJobFunction>
						<div class="form-group">
							<label>phr_JobFunction</label>
							<cfset attribs = {formfieldNAME="#flagtypeGrouptoset#_#personid#", multiple = true, validvalues = getalljobfunctionsordered,  dataValueColumn = "flagid", displayValueColumn = "translatedName", selectedColumn = "selected", keepOrig = true }>
							<cfif isdefined("session.JobsFlagGroup")>
								<cfif getalljobfunctions.flagtype EQ "radio">
									<cfset attribs.multiple = false>
								<cfelse>
								</cfif>
							<cfelse>
								<cfset attribs.formfieldname = "checkbox_jobfunction_#personid#">
							</cfif>
							<cf_displayValidValues attributeCollection = #attribs#>
						</div>
					</cfif>

					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
					<cfif ListContainsNoCase(hideFieldsControl, "email") EQ 0>
						<div class="form-group">
							<label class="required">phr_emailAddress</label>
							<CF_relayFormElement relayFormElementType="text" currentValue="#trim(email)#" fieldName="Person_email_#Personid#" size="50" validate="email" label="phr_emailAddress" onValidate="validateEmail" required="yes" attributeCollection=#emailAttributes#>
							<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="#trim(email)#">
						</div>
					</cfif>

					<cfif ListContainsNoCase(hideFieldsControl, "language") EQ 0>
						<div class="form-group">
							<label>phr_language</label>
							<!--- NJH 2008/04/30 Trend NABU bug 166 Portal - redue available list of valid languages --->
							<!--- NJH 2009/01/30 Bug Fix All Sites Issue 1709 - changed currentValue and currentRecord from getlanguage.language to language as well as changing the person fieldname from insLanguage--->
								<CF_DISPLAYVALIDVALUES
									FORMFIELDNAME="Person_language_#Personid#"
									validFieldName="person.language"
									CURRENTVALUE="#trim(Language)#"
									currentRecord = "#trim(Language)#"
									method="edit"
									>
							 <!--- <SELECT NAME="insLanguage">
									<cfloop QUERY="GetLangList">
									<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
									</cfloop>
								</SELECT> --->
						</div>
					</cfif>
					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - END --->

					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
					<cfif ListContainsNoCase(hideFieldsControl, "location") EQ 0>
						<div class="form-group">
							<label class="required">phr_location</label>
							<cf_translate phrases= "phr_Sys_SelectLocation"/>
							<cfselect class="form-control" name="PERSON_Locationid_#variables.personid#" query="getLocsforThisOrg" value="locationID" display="locdetails" selected="#Locationid#" required="yes" message="#phr_Sys_SelectLocation#">	</cfselect>
							<!--- 2006-04-25 AJC P_CHW001 - moved cfparams further up the code --->
							<!--- 2006-04-25 AJC P_CHW001 - Add new Location --->
							<cfoutput>

								<SCRIPT type="text/javascript">
									function newLocation()
									<!--hide
									{
										<cfif request.currentSite.isInternal>
											var url = '/relayTagTemplates/getRelayScreen.cfm?frmnextpage=/templates/windowopenreload_windowclose.cfm&frmLocationID=new&frmPersonID=0&frmcurrentscreenid=baselocationrecord';
										<cfelse>
											var url = '/?eid=AddNewLocation';
										</cfif>
										openWin(url+'&location_organisationid_default=#entityOrgID#&location_countryid_default=#request.relaycurrentuser.countryid#','mywindow','width=550,height=500,scrollbars=yes,resizable=yes');
									}
									//-->
								</script>
								<CF_INPUT TYPE="Hidden" NAME="PERSON_Locationid_#Personid#_orig" VALUE="#trim(Locationid)#">
								<a class="newLocation" id="addNewLocationLink" href="javascript:newLocation()">phr_managePeopleEditScreenIframe_addlocation</a>
							</cfoutput>
						</div>
					</cfif>
					<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
				<cfelse>
					<CF_INPUT TYPE="Hidden" NAME="PERSON_Locationid_#variables.personid#" VALUE="#trim(Locationid)#">
					<CF_INPUT TYPE="Hidden" NAME="PERSON_Locationid_#Personid#_orig" VALUE="#trim(Locationid)#">
				</cfif>

				<!--- 2006-04-25 AJC P_CHW001 Insert screens into page from list of screenids --->

				<cfif structKeyExists(session, "managePeopleEditScreenIframeScreenIDs") and session.managePeopleEditScreenIframeScreenIDs is not "">
					<cfloop index="i" list="#session.managePeopleEditScreenIframeScreenIDs#">
					<!--- <CF_aSCREEN formName="frmmain">   WAB 2009/04/16 moved a_screen tag to surround the whole page --->
   						<CF_aSCREENITEM SCREENID="#i#"
										person=#getPerson#
										location=#getLocation#
										organisation=#getOrganisation#
										PERSONID=#PERSONID#
										countryid = #thisCountryID#
										METHOD="edit"
										organisationID=#getPerson.organisationID#
										locationID=#getPerson.locationID#>
  					<!--- </CF_aSCREEN> --->
					</cfloop>
				</cfif>
					<cf_relayFormElement relayFormElementType="submit" fieldname="" currentValue="phr_save" label="" class="">
				</div>
			</cfloop>

		<!--- ==============================================================================
		       ADD PERSON SECTION
		=============================================================================== --->
		<cfif getPerson.recordCount eq 0>
			<div id="managePeopleEditScreenIframe-2">


				<CFSET firstname="">
				<CFSET lastname="">
				<CFSET email="">
				<CFSET salutation="">
				<CFSET lastupdated="">
				<cfset language="">


				<!--- 	<CFLOOP index="I" from="1" to="2"> --->
				<CFSET personid="new">
				<CFSET personidList=listappend(personidList,personid)>
				<!--- 2009/04/16 nasty hack to get ascreen to output the lastupdated hidden field
				rather than output a hidden field, set a variable which aScreen picksup
				<INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">
				--->
				<cfset "person_lastupdated_#personid#" = lastupdated>

				<!--- <INPUT TYPE="HIDDEN" NAME="person_locationid_#personid#" VALUE="#locationid#">	 --->		<!--- this tells the task which location the person is to be added to --->
				<!--- 	<TR><TD WIDTH="85">&nbsp;</TD>
						<TD>#phr_language#:</TD>
						<TD>
							<SELECT NAME="insLanguage">
								<cfloop QUERY="GetLangList">
									<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
								</cfloop>
							</SELECT>
						</TD>
					</TR>
				 --->

				<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - START --->
				<cfif ListContainsNoCase(hideFieldsControl, "salutation") EQ 0>
					<div class="form-group">
						<label class="required">phr_ext_salutation</label>
						<!--- NJH 2008/10/03 - Bug Fix T-10 Issue 932 - changed salutation to be a dropdown and made it required--->
						<cf_translate phrases= "phr_sys_salutation"/>
						<cf_displayValidValues
							validFieldName = "person.salutation"
							formFieldName =  "Person_Salutation_#Personid#"
							displayas = "select"
							currentValue = #trim(salutation)#
							countryID = #thisCountryID#
							entityID = 0
							useCFFORM = true
							required = "true"
							requiredLabel="#Phr_Sys_Salutation#"
							>
						<!--- <INPUT TYPE="Text"  class="narrow" name="Person_Salutation_#Personid#" size="10" class="narrow"> --->
						<CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" value="">
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "FirstName") EQ 0>
					<div class="form-group">
						<label class="required">phr_firstname</label>
		 				<CF_relayFormElement class="narrow" relayFormElementType="text" currentValue="#trim(firstname)#" fieldName="Person_FirstName_#Personid#" size="15" label="phr_firstname" required="yes">
						<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" value="">
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "LastName") EQ 0>
					<div class="form-group">
						<label class="required">phr_lastname</label>
						<CF_relayFormElement class="narrow" relayFormElementType="text" currentValue="#trim(lastname)#" fieldName="Person_LastName_#Personid#" size="15" label="phr_lastname" required="yes">
						<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" value="">
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "officePhone") EQ 0>
					<div class="form-group">
						<label>phr_officePhone</label>
						<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_OfficePhone_#Personid#" SIZE="15">
						<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" value="">
						<cfset phoneMask = application.com.commonQueries.getCountry(countryid=thisCountryID).telephoneformat>
						<CF_relayFormElement relayFormElementType="hidden" label="" currentvalue="#phonemask#" fieldname="phonemask">
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "MobilePhone") EQ 0>
					<div class="form-group">
						<label>phr_MobilePhone</label>
						<CF_INPUT TYPE="Text"  class="narrow" NAME="Person_MobilePhone_#Personid#" SIZE="15" mask="#phonemask#">
						<CF_INPUT TYPE="Hidden" NAME="Person_OfficePhone_#Personid#_orig" value="">
					</div>
				</cfif>
				<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - END --->
				<cfif showJobFunction>
					<div class="form-group">
						<label>phr_JobFunction</label>
						<cfset attribs = {formfieldNAME="#flagtypeGrouptoset#_#personid#", multiple = true, validvalues = getalljobfunctions,  dataValueColumn = "flagid", displayValueColumn = "translatedName", nulltext = "phr_NoJobFunction", keepOrig = true }>
						<cfif isdefined("session.JobsFlagGroup")>
							<cfif getalljobfunctions.flagtype EQ "radio">
								<cfset attribs.multiple = false>
							<cfelse>
							</cfif>
						<cfelse>
							<cfset attribs.formfieldname = "checkbox_jobfunction_#personid#">
						</cfif>
						<cf_displayValidValues attributeCollection = #attribs#>
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "email") EQ 0>
					<div class="form-group">
						<label class="required">phr_emailAddress</label>
						<CF_relayFormElement relayFormElementType="text" currentValue="#trim(email)#" fieldName="Person_email_#Personid#" size="50" validate="email" label="phr_emailAddress" onValidate="validateEmail" required="yes" attributeCollection=#emailAttributes#>
						<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="#trim(email)#">
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "language") EQ 0>
					<div class="form-group">
						<label>phr_language</label>
						<!--- NJH 2008/04/30 Trend NABU bug 166 Portal - redue available list of valid languages --->
						<!--- NJH 2009/01/30 Bug Fix All Sites Issue 1709 - changed currentValue and currentRecord from getlanguage.language to language as changing the person fieldname from insLanguage--->
						<CF_DISPLAYVALIDVALUES
							FORMFIELDNAME="Person_language_#Personid#"
							validFieldName="person.language"
							CURRENTVALUE="#trim(Language)#"
							currentRecord = "#trim(Language)#"
							method="edit"
							>
						<!--- <SELECT NAME="insLanguage">
							<cfloop QUERY="GetLangList">
							<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
							</cfloop>
						</SELECT> --->
					</div>
				</cfif>

				<cfif ListContainsNoCase(hideFieldsControl, "location") EQ 0>
					<div class="form-group">
						<label class="required">phr_location</label>
						<cf_translate phrases= "phr_Sys_SelectLocation"/>
						<cfselect class="form-control" name="PERSON_Locationid_#variables.personid#" query="getLocsforThisOrg" value="locationID" display="locdetails" selected="#Locationid#" required="yes" message="#phr_Sys_SelectLocation#">	</cfselect>
						<!--- 2006-04-25 AJC P_CHW001 - moved cfparams further up the code --->
						<!--- 2006-04-25 AJC P_CHW001 - Add new Location --->
						<cfoutput>
							<SCRIPT type="text/javascript">
								function newLocation()
								<!--hide
								{
									<cfif request.currentSite.isInternal>
										var url = '/relayTagTemplates/getRelayScreen.cfm?frmnextpage=/templates/windowopenreload_windowclose.cfm&frmLocationID=new&frmPersonID=0&frmcurrentscreenid=baselocationrecord';
									<cfelse>
										var url = '/?eid=AddNewLocation';
									</cfif>
									openWin(url+'&location_organisationid_default=#entityOrgID#&location_countryid_default=#request.relaycurrentuser.countryid#','mywindow','width=550,height=500,scrollbars=yes,resizable=yes');
								}
								//-->
							</script>
							<CF_INPUT TYPE="Hidden" NAME="PERSON_Locationid_#Personid#_orig" VALUE="#trim(Locationid)#">
							<cfif not (application.com.settings.getSetting('AddContact.showAddLocationLink') eq 0)>			<!--- 2012/03/27 PPB P-LEX070 showAddLocationLink setting --->
							<a class="newLocation" id="addNewLocationLink" href="javascript:newLocation()">phr_managePeopleEditScreenIframe_addlocation</a>
							</cfif>
						</cfoutput>
					</div>
				</cfif>
				<!--- 2013-06-20 - RMB - 435783 - Added hideFieldsControl - END --->

				<div class="form-group">
						<cf_relayFormElement relayFormElementType="submit" fieldname="" currentValue="phr_save" label="">
				</div>
			</div>
		</cfif>
		</cf_encryptHiddenFields>
		<!---
		WAB 2009/04/16 replaced these hidden fields with variables which are picked up by cf_ascreen
		<!--- ==============================================================================
		      MANDATORY HIDDENS USED BY UPDATEDATA.CFM
		=============================================================================== --->

		<INPUT TYPE="Hidden" NAME="frmtablelist" VALUE="person">
		<INPUT TYPE="Hidden" NAME="frmpersonfieldlist" VALUE="#personfieldsonpage#">
		<INPUT TYPE="Hidden" NAME="frmpersonidlist" VALUE="#personidsonpage#">
		<cfif isdefined("session.JobsFlagGroup")>
			<INPUT TYPE="Hidden" NAME="frmpersonflaglist" VALUE="#flagtypeGrouptoset#">
		<cfelse>
			<INPUT TYPE="Hidden" NAME="frmpersonflaglist" VALUE="checkbox_jobfunction">
		</cfif>
		 --->
		</CF_aSCREEN>

		</CF_relayFormDisplay> <!--- 2014-10-30 AHL Case 442292 CF_relayFormDisplay was out of the cfform --->
		</CFFORM>
		</cfoutput>
		</div> <!---2014-11-05 Case 442292 Div not concording with cfif --->
		</cf_translate>
		</div>
		</cfif> <!--- NJH 2009/09/25 LID 2663 added this if.. was originally above --->
	</cfif>

</cfif>
