<!--- �Relayware. All Rights Reserved 2014 --->
<!--- FILE DETAILS
relay\RelayTagTemplates\fundRequests.cfm
 --->

<!--- AMENDMENT HISTORY
NYB 2009-09-21 - LHID2623 - amended code to allow for using etids - not just numeric eids
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2015-08-07  DAN     update to make tag editor working
 --->

<cfparam name="fundRequestID" type="numeric" default=0>
<cfparam name="fundRequestActivityID" type="numeric" default=0>
<cfparam name="mode" type="string" default="view">
<!--- NYB 2009-09-21 - LHID2623 - removed type="numeric" to allow for using etids instead --->
<cfparam name="eid" default="0">

<cfif structKeyExists(form,"mode")>
	<cfset mode = form.mode>
<cfelseif structKeyExists(url,"mode")>
	<cfset mode = url.mode>
</cfif>

<cfinclude template="/Funds/applicationFundsINI.cfm">

<cfif request.relayCurrentUser.isLoggedIn>
	<cfif isdefined("url.eid")>
		<cfset processPage="/?eid=#url.eid#&">
	<cfelse>
		<cfif fundRequestActivityID eq 0>
			<cfset processPage = "/funds/fundRequests.cfm">
		<cfelse>
			<cfset processPage = "/funds/fundRequestActivities.cfm?">
		</cfif> 
	</cfif>

	<cfif isDefined("frmAddActivity") or isDefined("frmEditActivity") or isDefined("frmSaveAddNewActivity")>
		<cfinclude template="/funds/fundRequestActivities.cfm">
	<cfelseif fundRequestID eq 0 and mode eq "view">
		<cf_include template="/funds/fundRequests.cfm" getFieldXML="true">
	<cfelse>
		<cfinclude template="/funds/fundRequestActivities.cfm">
	</cfif>
</cfif>

