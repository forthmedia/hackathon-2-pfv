<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Mods
		2009-06-26		SSS		changed the query so it will only return locations that are flaged as show in locator

--->

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">

	<!--- Get the Google Maps key from the INI file --->
	<cf_include template="\code\cftemplates\locatorINI.cfm" checkIfExists="true">
	<!--- <cfif fileexists("#application.paths.code#\cftemplates\locatorINI.cfm")>
		<!--- locator INI is used to over-ride default global application variables and params --->
		<cfinclude template="/code/cftemplates/locatorINI.cfm">
	</cfif> --->
	
	<!--- <cfset oldSiteDataSource = application.siteDataSource>
	<cfset application.siteDataSource = "LenovoLive"> --->

	<cfparam name="addressFound" type="boolean" default=0>
	<cfparam name="LocationId" type="string" default="0">
	<cfparam name="mapAccuracy" type="numeric" default=4>
	
	<!--- <cfinclude template="/LenovoUserFiles/code/cftemplates/locatorINI.cfm"> --->
	<!--- <cfset googleMapAPIKey = "ABQIAAAAtAK_dNWvlvIhCG_1ZyLokhTPt3MLbR2tUAF2_6JL90hydX8JlRRePyjq90stQcqRY6wvHqySfY13-w"> --->
		
	<!--- Initialise flag integer to determine if latitude and longitude found in sql query for locationId passed --->
	<cfset addressFound = 0>
	
	<cfoutput><script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=#googleMapAPIKey#" type="text/javascript"></script></cfoutput>

	<!--- Get the Latitude & Longitude for the passed in LocationId from the database --->
	<!--- Security Project SSS changed this query so that it will only bring back the location if it is flaged as show in locator ---> 
	<cfquery name="getLatAndLong" datasource="#application.siteDataSource#">
		SELECT
			Latitude,
			Longitude,
			SiteName,
			Address1,
			Address2,
			Address3,
			Address4 AS City,
			Address5 AS State,
			PostalCode,
			ISOCode AS CountryCode,
			Telephone,
			AKA
		FROM 
			Location
			INNER JOIN Organisation ON Organisation.OrganisationId = Location.OrganisationId
			INNER JOIN Country ON Country.CountryId = Location.CountryId
			inner join booleanflagdata fd on location.locationid = fd.entityid 
			inner join flag f on f.flagid=fd.flagid and f.flagtextid = 'locator_yes'
		WHERE LocationId =  <cf_queryparam value="#LocationId#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cf_translate>

	<cf_head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

<!--- 		<cfif not getLatAndLong.recordCount>
			No Company location found in the database!
		</cfif> --->

		<cfoutput query="getLatAndLong">

			<cfset returnStruct = application.com.relayGeo.Geocode(address1=#getLatAndLong.address1#, address2=#getLatAndLong.address2#, City=#getLatAndLong.City#, State=#getLatAndLong.State#, PostalCode=#getLatAndLong.PostalCode#, CountryCode=#getLatAndLong.CountryCode#, googleMapKey=googleMapAPIKey)>

			<!--- <cfdump var="#returnStruct#"><br> --->

<!--- 			<br>
			address1 = #getLatAndLong.address1#<br>
			address2 = #getLatAndLong.address2#<br>
			City = #getLatAndLong.City#<br>
			State = #getLatAndLong.State#<br>
			PostalCode = #getLatAndLong.PostalCode#<br>
			CountryCode = #getLatAndLong.CountryCode#<br><br> --->
	
			<!--- Only do map if a record found in the database --->
			<cfif not getLatAndLong.recordCount>
				Company location not found in the database!
			<cfelse>

				<!--- Only do map if function returned a Lat & Long --->
				<cfif returnStruct.hasGeoCode and returnStruct.Accuracy lte mapAccuracy>
					<!--- Latitude and Longitude found for LocationId passed to set integer flag to 1 --->
					<cfset addressFound = 1>
	
					<cf_title>#getLatAndLong.SiteName#</cf_title>
					
				    <noscript>
				        <img src="Sorry2.gif">
				    </noscript>
					
				    <style type="text/css">
				        .bubble {
						    width:          350px;
						    max-width:      350px;
						    text-align:     left;
						    font-family:    verdana,helvetica,arial,sans-serif;
						    font-size:      11px;
						    font-weight:    normal;
					    }
				
					    .bubble h1 {
						    font-size:      11px;
						    margin-bottom:  4px;
						    font-weight:    bolder;
					    }
				
					    .bubble h2 {
						    font-size:      11px;
						    margin-bottom:  4px;
						    font-weight:    bolder;
				            font-style:italic;
					    }
				    </style>

					<script language="javascript" type="text/javascript">
					
						function onLoad() 
						{	//<![CDATA[
							if (GBrowserIsCompatible()) 
							{
							
								//var point = new GPoint(#jsStringFormat(getLatAndLong.Latitude)#, #jsStringFormat(getLatAndLong.Longitude)#);
								var point = new GLatLng(#jsStringFormat(returnStruct.lat)#,#jsStringFormat(returnStruct.lon)#);
								var map = new GMap(document.getElementById("map"));
								map.addControl(new GLargeMapControl());
								map.addControl(new GMapTypeControl());
								
								map.setMapType(G_NORMAL_MAP);
								//map.setMapType(G_HYBRID_TYPE);
								//map.setMapType(G_SATELLITE_MAP);
								
								map.centerAndZoom(point, 2);
								
								var marker = new GMarker(point);
								
								// NJH 2008-05-16 Bug Fix Lenovo 83 escaped the single quotes
								// Formats the address for the Map Pin Bubble, seen onclicking the pin
								var info = '<div class=\"bubble\"><h1>#replace(getLatAndLong.SiteName,"'","\'","ALL")# <cfif len(trim(#getLatAndLong.AKA#)) NEQ 0>(#replace(getLatAndLong.AKA,"'","\'","ALL")#)</cfif></h1>' +
								'<cfif len(trim(#getLatAndLong.Address1#))>#replace(getLatAndLong.Address1,"'","\'","ALL")#</cfif>' +
								'<cfif len(trim(#getLatAndLong.City#))><br>#replace(getLatAndLong.City,"'","\'","ALL")#</cfif>' +
								'<cfif len(trim(#getLatAndLong.State#))><br>#replace(getLatAndLong.State,"'","\'","ALL")#</cfif>' +
								'<cfif len(trim(#getLatAndLong.PostalCode#))><br>#replace(getLatAndLong.PostalCode,"'","\'","ALL")#</cfif>' +
								'<br><cfif len(getLatAndLong.Telephone) gt 0 >Tel: #jsStringFormat(getLatAndLong.Telephone)#</cfif></div>';

								//GEvent.addListener(marker, "click", function() {
								GEvent.addListener(marker, "mouseover", function() {
									marker.openInfoWindowHtml(info);
								});
								
								map.addOverlay(marker);
								marker.openInfoWindowHtml(info);
							} else {
								document.getElementById("map").innerHTML = "<img src='Sorry.gif' />";
							}
							//]]>
						}
					
					</script>
				
				<!--- <cfelse>
					phr_GoogleMap_UnableToMapResellerBasedOnAvailableAddrInfo. --->
				</cfif>
			
			</cfif>
		<!--- <link href="#request.currentsite. stylesheet#" rel="stylesheet" media="screen,print" type="text/css"> --->
		</cfoutput>
	</cf_head>
	
	<cfif addressFound NEQ 0>
	    <cf_body onload="onLoad()">
	        <div id="map" style="width: 640px; height: 480px;"></div>

			<form>
				<input type=button value="phr_GoogleMap_CloseMap" onClick="javascript:window.close();">
			</form>

	    
	<cfelse>
		
			<!--- No LocationId so notify user map cannot be displayed --->
			phr_GoogleMap_UnableToMapResellerOnAvailAddrInfo.
		
	</cfif>

	</cf_translate>
	
	<!--- <cfset application.siteDataSource = oldSiteDataSource> --->
	

