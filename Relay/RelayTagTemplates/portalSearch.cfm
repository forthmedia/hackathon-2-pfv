<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Display Portal Search Results based on /RelayTagTemplates/textSearchResultsV3.cfm 
	adjusted to portal search changes by Imre Haraszti
--->
<!---
Amendment History
2015/09/14    DAN    1486 P-TAT005 fixes for issue with Search RelayTag
2015/09/30    DAN    PROD2015-77 - add character limit to the search input

--->
<cf_param name="showCurrentRow" default="false" type="boolean"/>
<cf_param name="elementIcon" default="searchPage_icon.gif"/>
<cf_param name="searchButtonImage" default=""/>
<cf_param name="request.breadcrumbTrailTopEID" default="#request.currentsite.elementtreedef.topID#"/>
<cf_param name="request.hideTheseEIDsFromBreadcrumb" default="#request.currentsite.elementtreedef.hideTheseEIDsFromBreadcrumb#"/>
<cf_param name="request.productImagesDirectory" default="productImages"/>
<!--- <cfparam label="Thumbnail Size" name="thumbsize" default="90"> --->
<!--- <cfparam name="showProductBreadcrumb" type="boolean" default="No"> --->
<!--- <cfparam name="showBreadCrumbTrail" default="Yes" type="boolean"> --->
<!--- <cfparam name="breadCrumbTrailFormat" default="arrow"> ---><!--- ul,div,arrow --->

<CFPARAM NAME="form.maxRows" DEFAULT="25">
<CFPARAM NAME="form.startRow" DEFAULT="1">
<CFPARAM NAME="form.thisPage" DEFAULT="1">
<cfparam name="frmCriteria" default="">
<cfparam name="frmSourceID" default="">

<cfset qSources = application.com.search.getPortalSearchSources(LanguageID=request.relaycurrentuser.languageid)> 
 
<CFIF frmCriteria is not "">	
	<cfset qSearchResult = application.com.search.getPortalSearchResults(searchTextString=frmCriteria,portalSearchID=frmSourceID)>
			
	<cfset application.com.commonqueries.insertTrackSearch (searchtype = "V",searchstring=frmCriteria, personid = request.relayCurrentUser.personID)>
	<cfset stCount = {}>
	<cfquery name="qResult" dbtype="query">
		select count(portalSearchID) as cCount, portalSearchID 
		from qSearchResult 
		group by portalSearchID
	</cfquery>
	<cfloop query="qResult"><cfset stCount[portalSearchID] = cCount></cfloop>	
	<cfset results=true>
<CFELSE>
	<cfset results=false>
</CFIF>

<cf_head>
<!--- 2015-09-14 DAN    1486 P-TAT005 - remove overwritting the current page title --->
<!--- <cf_title>phr_sys_search_searchResults</cf_title> --->

<SCRIPT type="text/javascript">
function getSelectedRadio(buttonGroup) {
   // returns the array number of the selected radio button or -1 if no button is selected
   if (buttonGroup[0]) { // if the button group is an array (one button is not an array)
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            return i
         }
      }
   } else {
      if (buttonGroup.checked) { return 0; } // if the one button is checked, return zero
   }
   // if we get to this point, no radio button is selected
   return -1;
} // Ends the "getSelectedRadio" function

function getSelectedRadioValue(buttonGroup) {
   // returns the value of the selected radio button or "" if no button is selected
   var i = getSelectedRadio(buttonGroup);
   if (i == -1) {
      return "";
   } else {
      if (buttonGroup[i]) { // Make sure the button group is an array (not just one button)
         return buttonGroup[i].value;
      } else { // The button group is just the one button, and it is checked
         return buttonGroup.value;
      }
   }
} // Ends the "getSelectedRadioValue" function
	function searchWindow(nPerson) {
		document.searchForm.submit();
	}
	function getFile(fileID) {
		openWin (' ','FileDownload','width=370,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')
		form = document.getFileFormV2Search; <!--- 2015-09-14 DAN    1486 P-TAT005 --->
		form.fileID.value = fileID;
		form.submit();
	}
</SCRIPT>
</cf_head>

<!--- START 2015-09-14 DAN    1486 P-TAT005 - change var names to prevent conflicting with the filelist tag --->
<cfset fileFormV2ActionSearch = "/fileManagement/FileGet.cfm"> 
<cfif isDefined("isWebServiceCall") and isWebServiceCall>
	<cfset fileFormV2ActionSearch = "#fileFormV2ActionSearch#?#session.urltoken#">
</cfif>
<cfoutput>
<FORM METHOD="get" NAME="getFileFormV2Search" action = "#fileFormV2ActionSearch#" target = "FileDownload">
	<input type="hidden" name="fileID" value="0">
	<input type="Hidden" NAME="FileTypeGroupID" value="0">
</FORM>
</cfoutput>
<!--- END 2015-09-14 DAN    1486 P-TAT005 --->

<cfif results and frmCriteria neq "">	
	<h1><CFOUTPUT>#qSearchResult.recordcount# <cfif qSearchResult.recordcount is 1>phr_sys_search_ResultFor<cfelse>phr_sys_search_ResultsFor</cfif> "#htmleditformat(frmCriteria)#"</CFOUTPUT></h1>
<cfelse>
	<h1>phr_sys_search_searchResults</h1>
</cfif>

<div class="searchDiv">
	<div class="search-criteria-container">
		<div class="search-criteria" class="form-group">
			<cfoutput>
				<cfform method="POST" action="#cgi.script_name#?eid=#eid#" name="searchForm" id="searchForm">
					<cfinput type="hidden" name="StartRow" value="1">
					<div class="form-group row">
						<div class="col-md-6 col-sm-9 col-xs-9">
						<cfinput class="form-control" type="Text" name="frmCriteria" value="#frmCriteria#" message="phr_sys_search_YouMustIncludeAsearchString" required="Yes" size="20" maxLength="255"> <!---  onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" ---> 
						</div>
						<div class="col-md-6 col-sm-3 col-xs-3">
						<cfif searchButtonImage neq "">
							<cfinput name="frmSubmitButton" type="Image" src="#searchButtonImage#" id="submitButton">
						<cfelse>
							<cf_translate>
								<cfinput name="frmSubmitButton" type="submit" value=" phr_Go " id="submitButton" class="btn btn-primary">
							</cf_translate>
						</cfif>
						</div>
					</div>
					<cfif results and qSources.recordCount and qSearchResult.recordCount>
					<div class="form-group row">
						<div class="col-xs-12">
							<ul  class="list-inline">
								<li>Filter: <cfif listLen(frmSourceID) is 1><a href="#cgi.script_name#?eid=#eid#&frmCriteria=#htmlEditFormat(frmCriteria)#">phr_sys_clear_filter</a></cfif></li>
								<cfloop query="qSources">
									<li><cfif structKeyExists(stCount,portalSearchID) and stCount[portalSearchID]><a href="#cgi.script_name#?eid=#eid#&frmCriteria=#htmlEditFormat(frmCriteria)#&frmSourceID=#portalSearchID#">#displayName# (#stCount[portalSearchID]#)</a></li></cfif>
								</cfloop>
							</ul>
						</div>			
					</div>
					</cfif>
					<cfif isdefined("frmpersonid")>
						<cfinput type="hidden" name="frmpersonid" value="#frmpersonid#">
					</cfif>
				</cfform>
			</cfoutput>
		</div>
	</div>

	<cfif structKeyExists(url,"showProductID") and isNumeric(url.showProductID) and structKeyExists(url,"showCampaignID") and isNumeric(url.showCampaignID)>
		<cfset productid = url.showProductID>	
		<cfset showBackLink = false>	
		<cfset productCatalogueCampaignID = url.showCampaignID>
		<cfinclude template="productPicker.cfm">
	<cfelseif structKeyExists(url,"courseID") and isNumeric(url.courseID)>
		<cfinclude template="eLearningCourses2Level.cfm">
	</cfif>
	
	<cfif results>
							
		<cfif !(isDefined("qSearchResult") and qSearchResult.RecordCount eq 0 and isDefined("frmSources") and not structKeyExists(url,"showProductID"))>			
			
			<div class="search-results">
				<CFSET ShowRow = 0>
				<CFOUTPUT query="qSearchResult" maxRows="#Form.MaxRows#" startrow="#form.startrow#">
					<!--- 2013/02/19	YMA		2013 Roadmap Re-Style Verity Search - Fix search results to show odd and even rows.--->
					<CFIF ShowRow MOD 2 IS NOT 0><cfset nClass="oddrow"><CFELSE><cfset nClass="evenRow"></CFIF>
					<p class="search-results-row #nClass#">						
									
						<cfswitch expression="#type#">
							<cfcase value="files"><cfset nhref = "javascript:getFile(#qSearchResult.key#)"></cfcase>
							<cfcase value="courses"><cfset nhref = application.com.security.encryptQueryString(querystring = path,  removed=false)></cfcase>	
							<cfcase value="products"><cfset nhref = cgi.script_name & "?eid=#eid#&showProductID=#getToken(qSearchResult.key,3,'_')#&showCampaignID=#getToken(qSearchResult.key,4,'_')#"></cfcase>													
							<cfdefaultcase><cfset nhref = path></cfdefaultcase>						
						</cfswitch>						
															
						<!--- <td valign="top" align="center" class="search-results-image">
						
						<A href="#nhref#">
						
						<cfif type eq "elements">
							<img src="/images/icons/#elementIcon#" border="0">
							
						<cfelseif type eq "database">
							<cf_include template="\code\cftemplates\pcmINI.cfm" checkIfExists="true">
							
							<cfset productID = gettoken(key,3,'_')>
								
							<cfif structkeyexists(request,"productThumbsize")>
								<cfset thumbsize = request.productThumbsize>
							</cfif>
								
							<!--- <cfset product = application.com.relayProductTree.newProduct()>
							<cfset product.productID=productID> --->
							
							<cfif structKeyExists(request,"pcmTreeID")>
								<cfscript>
									prodHeaderQry = application.com.relayProductTree.getProductHeader(treeID=request.pcmTreeID,productID=productID);				
								</cfscript>
								
								<cfset productImage = prodHeaderQry.productID&"tn.png">
								
								<cfif not fileexists("#request.productImagesDirectory#\#productImage#")>
									<cfset application.com.relayImage.getRemoteImage(request.remoteImagePath,prodHeaderQry.thumbnailURL,request.productImagesDirectory,productImage,true,"thumbnail",thumbsize,thumbsize)>
								</cfif>
								<cfif fileexists("#request.productImagesDirectory#\#productImage#")>
									<img src="#request.productImagesDirectoryURL#/#productImage#" alt="#prodHeaderQry.Title#" border="0">
								<!--- <cfelse>
									<img src="/images/misc/blank.gif" width="80" height="80"> --->
								</cfif>
							<cfelseif collection contains "Course">							
								<cfif gettoken(key,2,'_') is "m">
									<cfset imgSrc = "/content/linkImages/trngModule/#keyPath#/Thumb_#keyPath#.png">
								<cfelse>
									<cfset imgSrc = "/content/linkImages/trngCourse/#keyPath#/Thumb_#keyPath#.png">
								</cfif>													
								<cfif fileExists(expandPath(imgSrc))>											
									<img src="#imgSrc#" width="80" height="80" />
								</cfif>
								#imgSrc#														
							</cfif>							
						</cfif>
							</A>
						</td> --->
							
						<span class="search-results-title">
							<a href="#nhref#"><cfif Category is not "">#htmleditformat(evaluate(Category))# : </cfif>#qSearchResult.Title#</a>
						</span>
						<cfif isdefined("qSearchResult.custom1") and qSearchResult.custom1 is not "" and qSearchResult.custom1 is not qSearchResult.Title>  <!--- 2014-01-14 PPB Case 437761 don't list a 2nd line for qSearchResult.custom1 if it is exactly the same as the Title  --->
							<span class="search-results-custom1">#htmleditformat(qSearchResult.custom1)#</span>
						</cfif>
						<!--- 
						<cfif showBreadCrumbTrail>						
							<cfif listFind("files,courses,pages",type) eq "files">								
								<cfset ElementID = gettoken(key,1,'_')>			
								<cfquery name="getBreadCrumbTrail" datasource="#application.SiteDataSource#">
									exec getElementParents @ElementID =  <cf_queryparam value="#ElementID#" CFSQLTYPE="CF_SQL_INTEGER" >  , @topid =  <cf_queryparam value="#request.breadcrumbTrailTopEID#" CFSQLTYPE="CF_SQL_INTEGER" > , @personid=#request.relayCurrentUser.personID#, @permission = 1, @countryid = '#request.relayCurrentUser.countryid#'
								</cfquery>
								
								<div class="search-breadcrumb-container">
								<!--- request.hideTheseEIDsFromBreadcrumb is a comma list of eid's that you can add to 
										relayINI.cfm which defines those EIDs that you do not want to show in the breadcrumb --->
									<cfif breadCrumbTrailFormat eq "ul">
										<ul>
									</cfif>
									<!--- <li><a href="/?etid=#request.HomePageETID#">phr_home</a></li> --->
									<cfloop query="getBreadCrumbTrail"><!--- and getBreadCrumbTrail.elementID neq variables.elementid --->
										<cfif currentRow gt 1 and getBreadCrumbTrail.elementID neq variables.ElementID
											and listfindNoCase(request.hideTheseEIDsFromBreadcrumb,getBreadCrumbTrail.ElementID) eq 0>
											<cfif breadCrumbTrailFormat eq "ul">
												<li>
											<cfelseif breadCrumbTrailFormat eq "div">
												<div>
											</cfif>
												<a href="/?eid=#getBreadCrumbTrail.ElementID#"<cfif getBreadCrumbTrail.currentRow eq (getBreadCrumbTrail.recordCount)> class="endLink"</cfif>>phr_headline_element_#htmleditformat(getBreadCrumbTrail.ElementID)#</a>
											<cfif breadCrumbTrailFormat eq "ul">
												</li>
											<cfelseif breadCrumbTrailFormat eq "div">
												</div>
											<cfelseif breadCrumbTrailFormat eq "arrow" and getBreadCrumbTrail.currentRow lt (getBreadCrumbTrail.recordCount - 1)>
												&gt;
											</cfif>
										</cfif>
									</cfloop>
									<cfif breadCrumbTrailFormat eq "ul">
										</ul>
									</cfif>
								</div>			

							<cfelseif type eq "products" and showProductBreadcrumb>
								<cfset productKey = qSearchResult["key"][currentrow]>
							  	<cfset navid = gettoken(productKey,1,'_')>
								<cfset nodeid = gettoken(productKey,2,'_')>
								<cfset productid = gettoken(productKey,3,'_')>
								<!--- SEMI-HACK separate breadcrumb just for product search to resolve url setting issues in the cfoutput loop, variable name conflicts and target differences for links --->
								<cfinclude template = "TextSearchResult_pcmBreadcrumb.cfm">
							 </cfif>
						</cfif>	 
						--->				
					<cfif showCurrentRow>
						<div class="searchResultsCurrentRow">#Form.StartRow + ShowRow#</div>
					</cfif>
					<CFSET ShowRow=ShowRow+1>
				</p>	
				</CFOUTPUT>
			</div>
			
			<cfif ceiling(qSearchResult.recordcount/maxrows) gt 1>
				<div class="search-results-pagination">
					<cfset formName="nextpage2">
					<CFOUTPUT>
						<SCRIPT type="text/javascript">
							function submit#jsStringFormat(formName)#(nStRow,nPage){
								nform=document.#jsStringFormat(formName)#;
								nform.StartRow.value = nStRow;
								nform.thisPage.value = nPage;
								nform.submit();
							}
						</script>
						<FORM name="#formName#" action="/?eid=#eid#" method="post">
							<cfif startrow gt 1><a href="javascript:submit#formName#(#((thisPage-2)*maxrows)+1#,#thisPage-1#)">&lt;&lt; Previous Page</a></cfif>
							<cfset nPage = 1>
							<!--- phr_page --->
							<cfloop from=1 to=#qSearchResult.recordCount# step=#maxrows# index="i">
								<cfif thisPage neq nPage><a href="javascript:submit#formName#(#i#,#nPage#)">#htmleditformat(nPage)#</a><cfelse><strong class="currentPage">#htmleditformat(nPage)#</strong></cfif>&nbsp;&nbsp;
								<cfset nPage = nPage+1>
								<cfif nPage gt 20><cfbreak></cfif>
							</cfloop>
							<cfif thisPage lt nPage-1><a href="javascript:submit#formName#(#(thisPage*maxrows)+1#,#thisPage+1#)">Next Page &gt;&gt;</a></cfif>
							<CF_INPUT type="hidden" name="frmCriteria" value="#frmCriteria#">
							<CF_INPUT type="hidden" name="frmSourceID" value="#frmSourceID#">
							<CF_INPUT type="hidden" name="MaxRows" value="#Form.MaxRows#">
							<CF_INPUT type="hidden" name="StartRow" value="#Form.StartRow#">
							<CF_INPUT type="hidden" name="thisPage" value="#form.thisPage#">
							<CFIF isdefined("frmpersonid")>
								<CF_INPUT type="hidden" name="frmpersonid" value="#frmpersonid#">
							</CFIF>		
						</FORM>
					</CFOUTPUT>
				</div>
			</cfif>
			
		<SCRIPT type="text/javascript">
			function newResultsPage(nStRow,nPage){
				nform=document.newPage;
				nform.StartRow.value = nStRow;
				nform.thisPage.value = nPage;
				nform.submit();
			}
		</script>
		</CFIF>
	</cfif>
</div>