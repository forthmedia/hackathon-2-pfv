<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		unsubscribeHandler.cfm
Author:			Simon WJ
Date created:	06 August 2002

	Objective - To trak unsubscribe requests.
		
	Syntax	  -	It should be submitted to via a form tag i.e.
    <FORM ACTION="http://www.foundation-network.com/relayware.fnl" METHOD="post" NAME="mainForm" TARGET="_blank">
		<INPUT TYPE="hidden" NAME="xxxxFirstName" VALUE="<<<FirstName>>>">
		<INPUT TYPE="hidden" NAME="xxxxLastName" VALUE="<<<LastName>>>">
		<INPUT TYPE="hidden" NAME="xxxxsiteName" VALUE="<<<siteName>>>">
		<INPUT TYPE="hidden" NAME="xxxxEmail" VALUE="<<<email>>>">
		<INPUT type="hidden" name="elementID" value="398">
		<INPUT TYPE="hidden" NAME="xxxxmessage" VALUE="Thanks for your interest.">
		<INPUT TYPE="hidden" NAME="CID" VALUE="<<<commid>>>"> - MANDATORY
		<INPUT TYPE="hidden" NAME="PID" VALUE="<<<personID>>>"> - MANDATORY
		<INPUT TYPE="hidden" NAME="xxxxTelephone" VALUE="<<<telephone>>>">  - MANDATORY
		<INPUT TYPE="hidden" NAME="Subject" VALUE="Network Telecomms Email Interest">  - MANDATORY
	
		Note: the xxxVarname format is used so that variables may or may not be processed in parts of this template. E.g. when we save 
			  this data to the database, we already need 

		You can also add additional fields in the form:
		<input type="checkbox" name="want_to_know_more" value="yes">
		When this template processes them the underscores are replaced by spaces in the email sent to the notification list.
				
	Return Codes - none

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-09-25			SWJ			Upgraded to cope with 
2001-08-03			SWJ			First version created
2001-11-13			SWJ			Added showValues section and the unsubscribe section
2002-10-04			SWJ			Add a method of specifying a flagid in a URL variable 
								using FID.  If the 
2005-05-20			DAM			Added logic to switch inform email to account manager only or add it 
								to the list of people to be informed

2006-06-01			WAB			cf_translated
Enhancement still to do:

1.	call a template that will insert a contact history record
2.	add a secure method that traps abuse using magicCheckSum
3.  work out a secure method that will make sure that the form 
	is being posted to by a valid email.
4.	work out a method of notifying someone via SMS

--->
<!--- 11th May 03 MDC
	Added to send out Email to partner in their language --->
	<cfquery name="EmailLanguage" datasource="#application.sitedatasource#">
		select l.languageid, l.buttonID
		from language l
		join person p on p.language = l.language
		where p.personid =  <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<CFIF isNumeric(EmailLanguage.languageid)>
		<CFSET thisPageLanguageID=EmailLanguage.languageid>
	<CFELSE>
		<CFSET thisPageLanguageID=1>
	</CFIF>
	<CFSET thisPageButtonID=EmailLanguage.buttonID>


<cf_translate phrases = "UnsubReqAck,UnsubRequest" />	
<!--- ***************************************************************
		get the persons details --->
<CFPARAM NAME="saveTime" TYPE="date" DEFAULT="#now()#">
<CFPARAM NAME="informAccMgr" TYPE="string" DEFAULT="ommit"><!--- DAM 2005-05-20 This can have one of three values 'ommit', 'replace' or 'add' --->
<CFPARAM NAME="informList" TYPE="string" DEFAULT="#application.supportEmailAddress#">
<CFPARAM NAME="Subject" TYPE="string" DEFAULT="#phr_UnsubRequest#">
<cfparam name="xxxxMessage" default="">
<cfparam name="thisFlagDescription" default="phr_NoUnsubTookPlace">
<!--- DAM 2005-05-20 request.unsubInformAccMgr can be set in the relayINI to determine the outcome here. 
		That only happens if it is 'replace' or 'add'.
--->
<cfif isdefined("request.unsubInformAccMgr") and (request.unsubInformAccMgr is "replace" or request.unsubInformAccMgr is "add")>
	<cfset informAccMgr = request.unsubInformAccMgr>
	<!--- we'll need to get the account manager's email address --->
	<CFQUERY NAME="getPersonsAccManagerEmail" DATASOURCE="#application.sitedataSource#">
		select p.email
		from integerflagdata i
		join person p on p.personid = i.data
		join flag f on f.flagid = i.flagid
		join person p1 on p1.organisationid = i.entityid
		where p1.personid=#request.relaycurrentuser.personid# and f.flagtextid = 'AccManager'
	</CFQUERY>
	<!--- there shouldn't be more than one account manager per person but lets make sure that
		doesn't break it anyway --->
	<cfset accMgrEmail = valuelist(getPersonsAccManagerEmail.email,";")>
</cfif>
<!--- Alter the inform list accordingly --->
<cfif informAccMgr is "replace" and getPersonsAccManagerEmail.recordcount gt 0>
	<cfset informList = accMgrEmail>
<cfelseif informAccMgr is "add" and getPersonsAccManagerEmail.recordcount gt 0>
	<cfset informList = informList & ";" & accMgrEmail>
</cfif>
<!--- ***************************************************************
		get the persons details --->
<CFIF isDefined('PID') and isNumeric(PID)>
	<CFQUERY NAME="getPersonDetails" datasource="#application.sitedatasource#">
		select p.email, p.firstName, p.lastName, o.organisationName
		from person p inner join organisation o
		on p.organisationID = o.organisationID
		where personID =  <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>



<!--- ***************************************************************
		Unsubscribe people who click to be unsubscribed              
		                                                         --->
<CFIF isDefined('Unsubscribe_request') and Unsubscribe_request eq "Unsubscribe" and isDefined('PID') and isNumeric(PID)>
	<!--- Work out the unsubscribe flagID --->
	<cfset thisFlagID = "">
	<!--- First find out if they are trying to unsubscribe from a specific flagID --->
	<CFIF isDefined('FID') and isNumeric(FID)>
		<!--- WAB altered query to pick up the text of the communication type in case there isn't a flagphrasetextid defined --->
		<CFQUERY NAME="checkValidFlagID" datasource="#application.sitedatasource#">
			Select flagid, flag.description, namephrasetextid as FlagPhrase , lookuplist.itemtext as CommTypeDescription
			from flag 
			left join lookuplist on flag.flagtextid = 'nocommsType'+convert(varchar,lookupid)
			where flagID =  <cf_queryparam value="#FID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		<cfif checkvalidFlagID.recordcount eq 1>
			<cfset thisFlagID = checkvalidFlagID.flagID>
			<cfif checkvalidFlagID.FlagPhrase is not "">
				<cfset thisFlagDescription = "phr_#checkvalidFlagID.FlagPhrase#">
			<cfelse>
				<cfset thisFlagDescription = "phr_UnsubFrom #checkvalidFlagID.CommTypeDescription#">
			</cfif>			
	
		<cfelse>
			<!--- if we did not find a valid flagID use unsubscribe from all --->
			<CFQUERY NAME="getFlagID" datasource="#application.sitedatasource#">
				Select flagID from flag where flagTextID = 'NoCommstypeall'
			</CFQUERY>
			<cfset thisFlagID = getFlagID.flagID>
			<cfset thisFlagDescription = "phr_UnsubAllEmails">	
		</cfif>
	<cfelse>
		<!--- If not set them to be unsubscribed from all emails ---> 
		<CFQUERY NAME="getFlagID" datasource="#application.sitedatasource#">
			Select flagID from flag where flagTextID = 'NoCommstypeall'
		</CFQUERY>
		<cfset thisFlagID = getFlagID.flagID>
		<cfset thisFlagDescription = "phr_UnsubAllEmails">
	</CFIF>
	
		<!--- set the unsubscribe flag so that comms will be surpressed --->
	<CFQUERY NAME="insertBFD" datasource="#application.sitedatasource#">
		INSERT INTO BooleanFlagData
		(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated)
		SELECT <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" >, f.FlagID, <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#Variables.saveTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#Variables.saveTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	  	FROM Flag AS f
 		WHERE f.FlagID =  <cf_queryparam value="#thisFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND NOT EXISTS (SELECT bfd.FlagID FROM BooleanFlagData AS bfd 
			WHERE bfd.FlagID = f.flagID and bfd.EntityID =  <cf_queryparam value="#PID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>
	
	<!--- ***************************************************************
		Send person an email telling them they've been unsubscribed      
		                                                         --->
	
	<CF_MAIL TO="#getPersonDetails.email#"
        FROM="#application.EmailFrom#"
        SUBJECT="#request.CurrentSite.Title# #Phr_UnsubReqAck#"
		BCC="#application.cBccEmail#"
        TYPE="HTML"
		>
	<cf_translate>
	<FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">
	<P>Phr_AsReqYouHaveBeen #thisFlagDescription# 
	Phr_UnsubFrom #application.clientName#.
 
	phr_ReSubScribe #request.CurrentSite.Title#.
	</P>
	</font>
	</cf_translate>
	</CF_MAIL>
	
	<CFIF isDefined ('showValues') and showValues eq "yes">
		<TABLE>
		  <TR>
		  	<TD HEIGHT="150px" VALIGN="bottom" STYLE="font-size: xx-small; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">
			<CFOUTPUT>Phr_UnsubFlagID #htmleditformat(thisFlagID)# - #htmleditformat(thisFlagDescription)#</CFOUTPUT></TD>
		  </TR>
		</TABLE>
	</CFIF>
</CFIF>

<!--- ***************************************************************
	 set up the email response to the informList --->
<CFMAIL TO="#informList#"
        FROM="#application.EmailFrom#"
		BCC="#application.cBccEmail#"
        SUBJECT="#request.CurrentSite.Title# #Subject#"
        TYPE="HTML"
		>
<P><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2"><B>Information from the Relay Unsubscribe Handler</B></FONT></P>
<cfset ResponseDetails = "<STRONG>Unsubscribe request handled</STRONG>">
<CFIF isDefined("getPersonDetails.recordCount") and getPersonDetails.recordCount gt 0>
	<TABLE>
		<TR>
			<TD>		
			<FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">
				<P>The following person has been #thisFlagDescription# from #application.clientName#<br>
				<CFLOOP QUERY="getPersonDetails">
				Name: #firstname# #lastname#<br>
				Company: #organisationName#<BR>
				Email: #email#<BR>
				
				</CFLOOP>
				<BR>
				Their IP address was #REMOTE_ADDR#
				<BR><BR>
				They have been emailed a response saying they will not receive any more emails from #application.clientName# and a flag
				has been set against their profile which will supress them from any further emails.
				</P>
			</FONT>
			</TD>
		</TR>	
	</TABLE>
<CFELSE>
	<FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">
		The following person has been #thisFlagDescription# from #application.clientName#<br>
		Their IP address was #REMOTE_ADDR#
	</FONT>
</CFIF>
</CFMAIL>
		
<cf_translate>
<!--- ***************************************************************
	 output message to show on the screen  --->
<TABLE BORDER="0" WIDTH="100%">
  <TR> 
    <TD> 
      	<P><B>
			<cfif isDefined ('xxxxMessage') and xxxxMessage neq "">
				<CFOUTPUT>#htmleditformat(xxxxMessage)# </CFOUTPUT>
				<CFELSE><cfoutput>phr_AsReqYouHaveBeen #htmleditformat(thisFlagDescription)#</cfoutput>. 
			</CFIF>
			</B>
		</P>
     	
    </TD>
  </TR>
</TABLE>
</cf_translate>
<!--- ***************************************************************
	 UPDATE COMMDETAIL RECORD: if commid and PID are set we can 
	 							update commDetail --->
<!--- 
2002-11-22:  Removed from here as this is now done in remote.cfm by using updateCommDetail.cfm

<CFIF isDefined('CID') and isNumeric(CID) and isDefined('PID') and isNumeric(PID)>
	<CFSET commDetailStatus = 41><!--- 41 = 'Unsubscribe Response' --->
	<!--- check to see if their is a matching commDetail record --->
	<CFQUERY NAME="checkIDPair" datasource="#application.sitedatasource#">
		select Commid from commDetail where Commid = #CID# and PersonID = #PID#
	</CFQUERY>
	<!--- if matching commDetail record found update it --->
	<CFIF checkIDPair.recordCount eq 1>
		<CFQUERY NAME="UpdateCommDetailRecord" datasource="#application.sitedatasource#">
			Update commdetail 
			  set feedback = '#thisFlagDescription#',
			  commStatusID = #commDetailStatus#,
			  lastUpdated = getDate(),
			  lastUpdatedBy = #PID#
			  where Commid=#CID#
			and PersonID = #PID#
		</CFQUERY>
	</CFIF>	
	<CFSET pid = PID>
</CFIF> --->

