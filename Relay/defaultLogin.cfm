<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2012-06-26 PPB Case 428684 don't attempt to login if the user presses enter while on the 'Forgotten Login Details' page

--->

<cfparam name="message" default = "&nbsp;">
<cfparam name="resendPW" default = "false">


<cf_includeJavascriptOnce template="/javascript/login.js" translate="true">

<cfif resendPW>
	<cf_includeJavascriptOnce template="/javascript/verifyEmail.js">
</cfif>

<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput> - Login</cf_title>

	<link href="/styles/login.css" rel="stylesheet" media="screen,print" type="text/css">
	<script type="text/javascript">
		<!--
		<cfif (request.relaycurrentuser.isunknownuser or request.relaycurrentuser.person.username is "") and not resendPW>
			function setfocus() { document.LoginForm.frmUsername.focus();  document.LoginForm.frmUsername.select();  }
		<cfelse>
			function setfocus() {if ($('frmPassword')) {document.LoginForm.frmPassword.focus();} }
		</cfif>
		//-->
	</script>
</cf_head>

<cf_body onload="setfocus();">
<!--- SB - Removed as it was causing conflicts --->
<!--- <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300italic,400,400italic,600,600italic" rel="stylesheet" type="text/css"> --->
<script type="text/javascript">
	document.body.onkeypress = function (event) {

	      	event = event || window.event;

			if (event.keyCode == 13) {
				if ($('loginLinks').style.display == '') {
					if ((event.target || event.srcElement).name == 'frmUsername' || (event.target || event.srcElement).name == 'frmPassword') {
	            		doLogin();
	            	} else if ((event.target || event.srcElement).name == 'partneremail') {
	            	   //nb if the user hasn't focus in the email box then this won't fire
	            	   javascript:document.LoginForm.submit();
	             	}
	            } else if ($('changePasswordLinks') != undefined && $('changePasswordLinks').style.display == '') {
	            	updatePassword();
	            } else if ($('newPasswordLink') != undefined){
	            	updatePasswordWithOrig();
	            }

	           	if (navigator.appName == 'Microsoft Internet Explorer') {
	            	event.keyCode = 0
	            }
			}
		}

	function sendPassword() {
		if (isEmailAddressValid($('Partneremail').value)) {
			document.LoginForm.submit();
		} else {
			alert('Please enter a valid email address')
		}
	}
</script>
<!--- 2012-06-26 PPB Case 428684 don't attempt to login if the user presses enter while on the 'Forgotten Login Details' page   --->
<!--- RMB - 2014-07-17 - P-RIL001 - Added setting to turn of autocomplete on login form --->
<cfset isAutoCompleteOn = application.com.settings.getSetting("security.internalsites.autoCompleteForUAndP")>

<cfoutput>
<form ID="LoginForm" <cfif NOT isAutoCompleteOn>autocomplete="off"</cfif> Class="LoginForm" NAME="LoginForm"  METHOD="POST" TARGET="" action="/index.cfm">

	<input type="hidden" id="resendPW" name="resendPW" value="false">
	<div id="outerDiv">
		<div id="loginDiv" class="row">
			<div id="loginLeftCol" class="col-xs-12 col-sm-6">
				<div id="loginOuterDiv">
					<div id="loginInputDiv">
						<cfif resendPW>
							<div id="innerPWDiv">
								<h1>phr_Resend_Password</h1>
								<span id="UsernameRow">
									<label for="Partneremail">Email Address</label>
									<INPUT TYPE="Text" NAME="Partneremail" size="30" value = "" id="Partneremail" class="form-control">
								</span>
								<div>
									<span id="message"><span id="loginMessage">Please enter your email address</span></span>
									<span id="loginLinks"><a href="javascript:sendPassword();" class="loginLink btn btn-primary">Send Password</a></span>
								</div>
							</div>
						<cfelse>
							<cfif structKeyExists(form,"partnerEmail") or structKeyExists(url,"spl") or structKeyExists(form,"oldPassword")>
								<!--- <cfset suppressOutput = true> --->
								<cfinclude template="/resendPWDefault.cfm">
							</cfif>

							<cfset showChangePassword = false>

							<cfif structKeyExists(url,"spl") and isDefined("link") and link.ok or (isDefined("changePassResult") and not changePassResult.isOK)>
								<cfset showChangePassword = true>

								<cfset oldPassword = "">
								<Cfset personID = 0>
								<cfif isDefined("link")>
									<cfset oldPassword = link.password>
									<cfset personID = link.personID>
								<cfelseif structKeyExists(form,"action") and form.action eq "setPassword">
									<cfset oldPassword = form.oldPassword>
									<cfset personID = form.personID>
								</cfif>

								<cf_encryptHiddenFields>
									<cfoutput>
										<CF_INPUT type="hidden" name="action" value="setPassword">
										<CF_INPUT type="hidden" name="personID" value="#personID#">
										<CF_INPUT type="hidden" name="oldPassword" value="#oldPassword#">
									</cfoutput>
								</cf_encryptHiddenFields>
							</cfif>

							<cfset loginDisplay = "">
							<cfset passwordDisplay = 'style="display:none"'>
							<cfif showChangePassword>
								<cfset loginDisplay = 'style="display:none"'>
								<cfset passwordDisplay = "">
							</cfif>

							<div id="loginInnerDiv">
								<h1>Sign In</h1>
								<div id="UsernameRow" class="form-group" #loginDisplay#>
									<label for="frmUsername">Username</label>
									<CF_INPUT TYPE="TEXT" <!---placeholder="Username"---> class="form-control" ID="frmUsername" NAME="frmUsername" VALUE="#IIF(NOT request.relayCurrentUser.isUnknownUser,DE(request.relayCurrentUser.person.username),DE(''))#" MAXLENGTH=50>
								</div>
								<div id="PasswordRow" class="form-group" #loginDisplay#>
									<label for="frmPassword">Password</label>
									<INPUT TYPE="PASSWORD" <!---placeholder="Password"---> ID="frmPassword" class="form-control" NAME="frmPassword" VALUE="" MAXLENGTH="50" AUTOCOMPLETE="off" onKeyUp="if ($('loginMessage').innerHTML != 'phr_login_attemptingToLogin') {$('loginMessage').innerHTML='&nbsp;';};return false;">
								</div>
								<div id="NewPasswordRow1" class="form-group" #passwordDisplay#>
									<label for="frmNewPassword1">New Password</label>
									<INPUT TYPE="PASSWORD" <!---placeholder="New Password"---> ID="frmNewPassword1" class="form-control" NAME="frmNewPassword1" VALUE="" MAXLENGTH="50" AUTOCOMPLETE="off" onKeyUp="validatePasswordStrength(this)" oncopy="return false;">
								</div>
								<div id="NewPasswordRow2" class="form-group" #passwordDisplay#>
									<label for="frmNewPassword2">Repeat New Password</label>
									<INPUT TYPE="PASSWORD" <!---placeholder="Repeat New Password"---> ID="frmNewPassword2" class="form-control" NAME="frmNewPassword2" VALUE="" MAXLENGTH="50" AUTOCOMPLETE="off">
								</div>
								<div>
									<div id="message">
										<span id="loginMessage">#application.com.security.sanitiseHTML(message)#</span>
									</div>
									<!--- regular login button --->
									<div id="loginLinks" #loginDisplay#>
										<a href="javascript:doLogin();" class="loginLink btn btn-primary">Sign in to Partner Cloud</a>
									</div>
									<!--- we get this if the users password is expired --->
									<cfif not showChangePassword>
										<div id="changePasswordLinks" #passwordDisplay#>
											<div><a href="javascript:updatePassword();" class="loginLink"><img src="/images/login/update.png"></a></div>
											<div><a id="changePasswordContinueLink" href="javascript:window.location.href = removeUnwantedURLParameters(window.location.href);" class="loginLink"><img src="/images/login/login_button.png"></a></div>
										</div>
									<!--- we get this if the user is changing their password (from a link in an email) --->
									<cfelse>
										<div id="newPasswordLink">
											<a href="javascript:updatePasswordWithOrig();" class="loginLink"><img src="/images/login/update.png"></a>
										</div>
									</cfif>
								</div>
								<div id="NewPasswordRow3" #passwordDisplay#>
									<div id="passwordStrengthIndicator"><cfoutput>#application.com.login.checkPasswordComplexity(password="").fullDescription#</cfoutput></div>
								</div>
							</div>
						</cfif>
					</div>
				</div>
				<cfif not resendPW>
					<div id="forgottenPW" #loginDisplay#>
						<p><a href="##" onClick="$('resendPW').value=true;$('LoginForm').submit();" class="loginForgottenLink">phr_login_forgottenLoginDetails</a></p>
					</div>
				</cfif>
			</div>
			<div id="loginRightCol" class="col-sm-6 hidden-xs">
				<img src="/images/login/partnerUp.png">
			</div>
		</div>
		<footer id="loginFooter" role="footer">
			<ul>
				<li id="footerLogo"><a href="http://www.relayware.com"><img src="/images/login/logo.png"></a></li>
			</ul>
		</footer>
	</div>
	<div id="loginCopyright">
		<p>Copyright &copy; <cfoutput>#DateFormat(Now(),"yyyy")#</cfoutput> relayware.com, inc. All rights reserved.</p>
	</div>
</FORM>
</cfoutput>