<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>
	<cffunction name="test">
		<cfargument name="content">
		<cfargument name="type">		

			<cfset var local = {}>
			<cfset var result = {isOK = true, linenumber = 0}>

			<cfif type is "cfm">
				<CFSET CONTENT = "<CFIF FALSE>#CONTENT#</CFIF>">
			</cfif>	
 			<cfset content = application.com.regExp.replaceHTMLTagsInString(inputString = content,htmltag="cf_abort|cfabort|cfinclude|cflocation|cf_include|cf_includejavascriptonce",hasendtag=false,replaceExp="",preserveLineNumbers=true,preserveLength = false)>
			<cfset content = rereplaceNocase(content,".abortIt\(\)","","ALL")>
			<cfset content = rereplaceNocase(content,'<cffunction name="initialise">.*?</cffunction>',"","ALL")>

			


		<cffile action="write" file="#application.paths.temp#/tmpCompileTest.#type#" output="#content#">				
	
		<cftry>
			
			<cfsavecontent variable="local.x">
				<cfif type is "cfm">
					<cfinclude template = "\temp\tmpCompileTest.cfm">
				<cfelse>
					<cfset createObject("component","temp.tmpCompileTest")>
				</cfif>
			</cfsavecontent>	
		
			<cfcatch>
				<cfif cfcatch.type is "template">
					<cfset result.isOK = false>
					<cfif structKeyExists (cfcatch,"detail")>
						<cfset result.message = cfcatch.detail>
					<cfelse>
						<cfset result.message = cfcatch.message>
					</cfif>
					
					<cfif structKeyExists (cfcatch,"line")>
						<cfset result.lineNumber = cfcatch.line>
					<cfelseif structKeyExists (cfcatch,"tagContext")>
						<cfset lines = application.com.errorHandler.getErrorFileAndLineFromTagContext(cfcatch.tagContext)>
						<cfset result.lineNumber = lines.line>
					</cfif>	
				<cfelse>
	
				</cfif>
				
			</cfcatch>
		</cftry>
		
		<cfset structDelete(local,"x")>
		<cfsetting requestTimeOut="3600"> <!--- reset request timeout incase doctored --->

		<cfreturn result>	
	</cffunction>

</cfcomponent>