<!--- �Relayware. All Rights Reserved 2014 --->
<!---
This file is an attempt to take all the places where preserveSingleQuotes has been found
and look back in the file or function for the code which generated the SQL and then run codecop against that

Not sure whether it has ever worked, been used

 --->

<cfinclude template="replacementFunctions.cfm">
<cfset form.autoreplace = true>
<cfset form.frmaction = "psq">
<cfset form.realrun= 1>


<cfquery name="getpsq" datasource = "#application.sitedatasource#">
select * from securitypreservesinglequotes
where isnull(filename,'') <> ''
/*
and
(itemsfound is null
or itemsfound <> itemsfixed
)
*/
</cfquery>


<cfloop query="getpsq">
	<!--- open file --->
	<cfset filePath = expandPath_(filename)>
	<cffile action = "read" file="#filePath#" variable="fileContent">

	<cfset result = {content = fileContent,updated=false,output="",itemsfound=0,itemsfixed=0}>

	<!--- get function if necessary --->
	<cfif functionname is not "">
		<cfset regexp = '<cffunction.*?name="#functionname#".*?>(.*?)</cffunction>'>
		<cfset groupnames = {1="innerHTML"}>
		<cfset x = application.com.regExp.refindalloccurrences(regexp,filecontent,groupnames)>
		<cfset contentToSearch  = x[1].innerHTML>
	<cfelse>
		<cfset contentToSearch = fileContent>

	</cfif>

	<!--- look for set or cfcontent --->
	<cfset regexp = '(<(cfsavecontent).*?variable\s*=\s*"#variablename#".*?>)(.*?)</cfsavecontent>'>
	<cfset groupnames = {1="openingTag",2="tagname",3="innerHTML"}>
	<cfset cfsavecontentfind = application.com.regExp.refindalloccurrences(reg_Expression=regexp,string=contentToSearch,groupnames=groupnames,getLineNumbers = true)>



	<!--- pass to doquery --->
	<cfloop array="#cfsavecontentFind#" index="cfsavecontent">
		<cfset cfsavecontent.attributes = {}>
		<cfset thisresult = processCFQUERY(thisObject=cfsavecontent,inputoutput=structNew(),identifier="#filename#")>


			<cfloop collection="#thisResult#" item="key">
				<cfif isBoolean (thisResult[key])>
					<cfset result[key] = result[key] OR thisResult[key]>
				<cfelseif isNumeric (thisResult[key])>
					<cfset result[key] = result[key] + thisResult[key]>
				<cfelseif key is not "content">
					<cfset result[key] = result[key] & thisResult[key]>
				</cfif>
			</cfloop>

		<cfif thisResult.updated is true>
			<cfset result.content = replace(result.content,cfsavecontent.string,thisResult.content)>
			<cfset result.updated = true>
		</cfif>


	</cfloop>

	<!--- update file --->
	<cfif result.updated>
		<cffile action="write" output="#Result.Content#" file="#FilePath#">
	</cfif>
	<!--- do some logging --->
			<cfquery name="log" datasource = "#application.sitedatasource#" debug="false">
			update securityPreserveSingleQuotes
			set itemsfound =  <cf_queryparam value="#result.itemsfound#" CFSQLTYPE="cf_sql_integer" >,
				itemsfixed =  <cf_queryparam value="#result.itemsfixed#" CFSQLTYPE="cf_sql_integer" >
			where filename =  <cf_queryparam value="#filename#" CFSQLTYPE="CF_SQL_VARCHAR" >
				functionname =  <cf_queryparam value="#functionname#" CFSQLTYPE="CF_SQL_VARCHAR" >
				variablename  =  <cf_queryparam value="#variablename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>


	<table>
		<cfoutput>
			#result.output#
		</cfoutput>

	</table>


</cfloop>