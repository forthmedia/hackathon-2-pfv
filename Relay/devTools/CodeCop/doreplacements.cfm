<!--- �Relayware. All Rights Reserved 2014 --->
<cf_nodebug>
<cfinclude template="replacementFunctions.cfm">

<cfsetting showdebugoutput ="true" requestTimeOut="3600" enablecfoutputonly="true">
<cfparam name="form.inSituReplace" default = "0">
<cfparam name="form.AutoReplace" default = "1">
<cfparam name="form.Recurse" default = "false">
<cfparam name="form.update" default = "">
<cfparam name="form.frmaction" default = "cfquery">
<cfparam name="form._cf_nodebug" default = "true">
<cfparam name="form.updateFunctionArguments" default = "false">
<cfparam name="form.processingMode" default = ""> <!--- JustUnProcessed,JustPartProcessed,ReprocessAll, --->
<!--- <cfparam name="form.justdoPartProcessedFiles" default = "false">
<cfparam name="form.JustUnProcessedFiles" default = "false">
<cfparam name="form.reprocessAllFiles" default = "false"> --->

<cfset debug = "0">
<cfparam name="directoryPath"  default = "/devtools/codecop/exampleReplacements">
<cfparam name="maxFiles"  default = "10">
<cfset filefilter = "*.cf*">
<cfset filecounter = 0>


<cfif refindnocase("([A-Z]:)|\A(\\\\)",directoryPath)>
	<cfset expandedPath = directoryPath>
<cfelse>
	<cfset expandedPath = expandPath_(directoryPath)>
</cfif>

<cfif expandedpath contains ".">
	<cfset parsepath = application.com.regexp.getFileDetailsFromPath(expandedpath)>
	<cfset expandedpath = parsepath.path>
	<cfset filefilter = parsepath.fullfilename>
</cfif>

	
<cfif expandedPath  contains "/test/">
	<cfset form.inSituReplace = 0>
</cfif>

<cfset functions = []>
<cfset functions[1] = {name="Misc Replaces",functionPointer= MiscReplaces, tagname="", hasEndTag = true,processFunctionsSeparately = false}>
<cfset functions[2] = {name="CF_Abort",functionPointer= ProcessCFABORT, tagname="", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[3] = {name = "CF_QueryParam",functionPointer= ProcessCFQuery, tagname="CFQUERY", hasEndTag = true,processFunctionsSeparately = true}>
<cfset functions[4] = {name = "CF_QueryParam_Insert",functionPointer= processCFQueryInsert, tagname="CFQUERY", hasEndTag = true,processFunctionsSeparately = true}>
<cfset functions[5] = {name = "CF_QueryParam_OtherStuff",functionPointer= processCFQueryLookForObjectsAsCFVariables, tagname="CFQUERY", hasEndTag = true,processFunctionsSeparately = true}>
<cfset functions[6] = {name = "CF_INPUT",functionPointer= replaceHTMLTagWithCustomTag, tagname="INPUT", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[7] = {name = "CF_A",functionPointer= replaceHTMLTagWithCustomTag, tagname="A", processFunctionsSeparately = false, hasEndTag = true}>
<cfset functions[8] = {name = "CF_FRAME",functionPointer= replaceHTMLTagWithCustomTag, tagname="FRAME", processFunctionsSeparately = false, hasEndTag = true}>
<cfset functions[9] = {name = "CF_IFRAME",functionPointer= replaceHTMLTagWithCustomTag, tagname="IFRAME", processFunctionsSeparately = false, hasEndTag = true}>
<cfset functions[10] = {name = "HTMLEDITFORMAT",functionPointer= processHTMLEDITFORMAT, tagname="", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[11] = {name = "jsStringFormat",functionPointer= processSCRIPT, tagname="SCRIPT", hasEndTag = true,processFunctionsSeparately = false}>
<cfset functions[12] = {name = "CF_IMG",functionPointer= replaceHTMLTagWithCustomTag, tagname="IMG", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[13] = {name = "CFSAVECONTENT",functionPointer= ProcessCFSAVECONTENT, tagname="CFSAVECONTENT", hasEndTag = true,processFunctionsSeparately = false}>
<cfset functions[14] = {name = "CFLOCATION",functionPointer= ProcessCFLOCATION, tagname="CFLOCATION", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[15] = {name = "Test Compile",functionPointer= testCompile, tagname="", hasEndTag = false,processFunctionsSeparately = false}>
<cfset functions[16] = {name = "Add CopyRight",functionPointer= addCopyRight, tagname="", hasEndTag = false,processFunctionsSeparately = false, fileFilter="\.(cf.|js|xml|html?|css)", excludeFileFilter="(\\cf_gantt20\\|\\mxAjax\\|\\poiutility\\|\\jquery\\|\\ckeditor[0-9]*\\|\\ckfinder\\|prototype.*?\.js|\\customTags\\date.cfm|\\javascript\\[a-zA-Z]*\\|\\javascript\\cfform[a-zA-Z]*|\\taffy\\(anythingtoxml|core)\\|jquery.*?\.js|\\code\\borders\\script\\|superfish.css)"}>

<cfset FunctionList = "">
<cfloop index="i" from = "1" to =#arrayLen(functions)# >
	<cfset functionList = listAppend(functionList,"#i##application.delim1##functions[i].name#")>
</cfloop>




<cfoutput>
<form method="post">
	<cf_sessionTokenField>	
	Path <CF_INPUT type="text" name="directoryPath" value="#directoryPath#" ><br />
	<input type="checkbox" value="true" name="Recurse" <cfif Recurse is 1>checked</cfif> > Recurse<br />
	<input type="checkbox" value="1" name="autoReplace" <cfif autoreplace is 1>checked</cfif> > Auto Replace (replaces are done automatically either in situ or in a new file; alternative is that all replaces are show to user for confirmation first - no supported by all processes))<br />
	<input type="checkbox" value="1" name="inSituReplace" <cfif inSituReplace is 1>checked</cfif> > In Situ Replace (otherwise creates file in codeCopResults directory [a sibling of Relay])<br />
	<input type="checkbox" value="true" name="_cf_nodebug" <cfif _cf_nodebug is true>checked</cfif> > Hide Debug<br />
	<input type="checkbox" value="true" name="updateFunctionArguments" <cfif updateFunctionArguments is true>checked</cfif> > updateFunctionArguments<br />
	
	<cfset validValues = "JustUnProcessed,JustPartProcessed,ReprocessAll">
	<cf_displayValidValues formFieldName="processingMode" validvalues="#validvalues#" currentValue="#processingMode#" displayAs="radio" columns="1" nullText="Process Unprocessed and Part Processed Files" length=8>
	<!--- 
	<input type="radio" value="JustPartProcessed," name="processingMode" <cfif justdoPartProcessedFiles is true>checked</cfif> > Just Re-Process part processed files<br />
	<input type="radio" value="JustUnProcessed" name="processingMode" <cfif JustUnProcessedFiles is true>checked</cfif> > Ignore part processed files<br />
	<input type="radio" value="ReprocessAll" name="processingMode" <cfif reprocessAllFiles is true>checked</cfif> > Re-process all files<br />
	 --->
	
	
	Max Files<CF_INPUT type="text" name="maxFiles" value="#maxfiles#"><br />
	Replace Type<cf_displayvalidvalues validvalues="#functionList#" formfieldname="frmaction" currentvalue="#form.frmaction#" displayAs="multiselect" listsize=15 shownull=false><br />

	<input type="submit">

</cfoutput>


	<cfif cgi.REQUEST_METHOD is "POST">

	
		<cfif processingMode is not "JustPartProcessed" >
			<cfdirectory name="files" action="list" directory="#expandedPath#" recurse="#form.recurse#" type="file" filter="#filefilter#"> 
	
			<cfquery name="files" dbtype="query" >
			select  directory + '\' + name as filePath from files
			where lower(directory) not like '%.svn%'
			and lower(directory) not like '%dummy%'
			</cfquery>

		<cfelse>
			<cfset files = getPartProcessedFiles (action = frmaction, maxFiles = maxFiles)>
		</cfif>
		
		<cfif files.recordcount is 0>
			<cfoutput>No Files Found in #expandedPath#</cfoutput>
		<cfelse>

			
			<cfset result = iterateFileQueryRunningActions(fileQuery=files,actionList= frmAction, maxFiles = maxFiles)>
			
			
			<cfoutput>
			<table>
				#result.output#
			</table>
			</cfoutput>
			
		
		

		</cfif>
	</cfif>

	<cfoutput>
	<input type="submit">
	</form>
	</cfoutput>

