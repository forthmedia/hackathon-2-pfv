<!--- �Relayware. All Rights Reserved 2014 --->
<!---
***************
ALL BELOW IS FUNCTIONS
***************
 --->
<cffunction name="testCompile">
	<cfargument name="content">
	<cfargument name="identifier" >


	<cfset var result = {updated = false,content = content, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var local = {}>
	<cfset var testResult = "">
	<cfset local.testObj = createObject("component","testCompile")>
	<cfset testResult = local.testObj.test (content = content, type = listLast(identifier,'.'))>

	<cfset structDelete (local,"testObj")>

	<cfif not testResult.isOK>
		<cfset result.updated = false>
		<cfset result.itemsFound = 1>
		<cfset result.itemsFixed = 0>
		<cfset result.output = "<TR><TD colspan=5></TD><TD>#testResult.message#</TD></TR>">
		<cfset removeIssues (filename=identifier)>
		<cfset LogIssue (filename=identifier,lineNumber = testResult.linenumber,oldstring ="", message = testResult.message)>
	<cfelse>
		<cfset removeIssues (filename=identifier)>
	</cfif>

	<cfreturn result>
</cffunction>


<cffunction name="miscReplaces">
	<cfargument name="content">
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = content, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var searchReplaceArray	= arrayNew(1)>
	<cfset var searchReplace	= "">

	<cfset searchReplaceArray[1] = {search ='listfirst\(cookie\.user,"-"\)', replace = 'request.relayCurrentUser.personid'}>
	<cfset searchReplaceArray[2] = {search ='listgetat\(cookie\.user,1,"-"\)', replace = 'request.relayCurrentUser.personid'}>
	<cfset searchReplaceArray[3] = {search ='listgetat\(cookie\.user,2,"-"\)', replace = 'request.relayCurrentUser.usergroupid'}>
	<cfset searchReplaceArray[4] = {search ='isDefined\("Cookie\.USER"\)', replace = 'isDefined("request.relayCurrentUser.personid")'}>

	<cfloop array="#searchReplaceArray#" index="searchReplace">
		<cfset result.content = reReplaceNoCase(result.content,searchReplace.search,searchReplace.replace,"ALL")>
	</cfloop>

	<cfif result.Content is not content>
		<cfset result.updated = true>
		<cfset result.itemsFound = 1>
		<cfset result.itemsFixed = 1>
		<cfset result.output = "<TR><TD colspan=5></TD><TD>Misc Replacements Made</TD></TR>">
	</cfif>

	<cfreturn result>
</cffunction>


<cffunction name="ProcessCFABORT">
	<cfargument name="content">
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = content, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var regExp = "<CFABORT\s*>">

	<cfif refindNocase(regExp,content)>

		<cfset result.content = reReplaceNoCase(content,regExp,"<CF_ABORT>","ALL")>

		<cfset result.updated = true>
		<cfset result.itemsFound = 1>
		<cfset result.itemsFixed = 1>
		<cfset result.output = "<TR><TD colspan=5></TD><TD>CFABORTs updated</TD></TR>">
	</cfif>

	<cfreturn result>
</cffunction>

<cffunction name="ProcessCFLOCATION">
	<cfargument name="thisObject">
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>

	<cfif not structKeyExists(thisObject.attributes,"addToken")>
		<cfset result.content = reReplaceNoCase(result.content,">",'addToken="false">')>
		<cfset result.updated = true>
		<cfset result.itemsFound = 1>
		<cfset result.itemsFixed = 1>
	<cfelseif thisObject.attributes.addToken>
		<cfset result.content = reReplaceNoCase(result.content,"addToken=.*?(?=[ >])",'addToken="false"')>
		<cfset result.updated = true>
		<cfset result.itemsFound = 1>
		<cfset result.itemsFixed = 1>

	</cfif>

	<cfreturn result>
</cffunction>



<cfset regExps = {}>
<cfset regExps.CFVariable.regExp = "##([A-Za-z0-9_.\(\)]+)##">
<cfset regExps.CFVariable.groupNames = {1 = "innerText"}>

<cffunction name="processSCRIPT">
	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>

	<cfset content = thisObject.innerHTML>
	<!--- WAB 2013-09-02 remove JS comments - could be problematical in some circumstances,  --->
	<cfset content = application.com.regExp.removeComments (String  = content, Type="JSSingleLine", preserveLineNumbers = true,preserveLength = true)>

	<cfset content = application.com.regExp.removeAllComments (String  = content, preserveLineNumbers = true,preserveLength = true)>
	<cfset content = application.com.regExp.replaceHTMLTagsInString(inputString = content,htmltag="cfquery|script|style|cfmail|cf_mail|cf_query|cfscript|cfsavecontent",hasendtag=true,replaceExp="",preserveLineNumbers=true,preserveLength = true)>
	<!--- need to inspect cfsavecontent at a later date --->
	<cfset content = application.com.regExp.replaceHTMLTagsInString(inputString = content,htmltag="(cf.*?|cfset|cfloop|frame|tr|td|table|input|a|img|div|select|option|form)",hasendtag=false,replaceExp="",preserveLineNumbers=true,preserveLength = true)>


	<cfset matches = application.com.regExp.reFindAllOccurrences(reg_expression = regExps.CFVariable.regExp,string = content, groupnames=regExps.CFVariable.groupnames,getlinenumbers = true,usejavaMatch=true)>

	<cfloop array=#matches# index="thisMatch">
		<cfif isVariableSafe(thismatch.string,"script")>
			<!--- OK is a safe variable--->
		<cfelseif refindNoCase("\A(.*XML.*|.*HTML.*|.*JSON.*|.*_JS##)\Z",thismatch.string)>
			<!--- Don't do any variable with HTML or XML in the name--->

		<cfelse>

			<cfset lookForJSStringFormat = application.com.regExp.refindAllOccurrences ("jsStringFormat\((.*)\)",thisMatch.innerText)>
			<cfset hasJsStringFormat = false>
			<cfset variableName = thisMatch.innerText>
			<cfset safelyParamed = false>

			<cfif arrayLen(lookForJSStringFormat)>
				<cfset hasJsStringFormat = true>
				<cfset variableName = lookForJSStringFormat[1][2]>
			</cfif>

			<cfset currentLineNumber = thisObject.lineNumber + thisMatch.lineNumber -1>

			<!--- has this variable been param'ed as a number or variablename --->
			<cfif
						structKeyExists (inputOutput.cfparamStruct,variableName)
					and structKeyExists (inputOutput.cfparamStruct[variableName],"type")
					and listfindnocase("integer,float,numeric,variablename", inputOutput.cfparamStruct[variableName].type) IS NOT 0
					and inputOutput.cfparamStruct[variableName].position lt (thisObject.position + thismatch.position)>
				<cfset safelyParamed = true>
			</cfif>

			<cfif not safelyParamed>

				<!--- Is there a quote somewhere on the line before this item, if not then jsStringFormat will not work --->
				<cfset currentLine = getCurrentLine(content,thisMatch.position)>

				<cfif refind("['""]",left(currentLine.line,currentLine.positionOnLine)) is 0>
					<cfset result.itemsFound ++>
					<cfset result.output = result.output & '<tr><td colspand="3"></td><td>#currentLineNumber#</td><td>#htmleditformat(thisMatch.string)#</td><td>#htmleditformat(mid(currentLine.line,max(1,currentLine.positionOnLine - 50),thisMatch.length + 100))#<font color="red">Not in a Javascript String.  Needs dealing with manually</font></td></tr>'>

				<cfelseif not hasJsStringFormat>

					<cfset result.itemsFound ++>
					<cfset result.output = result.output & '<tr><td colspand="3"></td><td>#currentLineNumber#</td><td>#htmleditformat(thisMatch.string)#</td><td>#htmleditformat(mid(content,max(1,thisMatch.position - 50),thisMatch.length + 100))#</td></tr>'>
					<cfset newString = "##jsStringFormat(#thisMatch.innerText#)##">
					<cfset LogReplacement (filename=identifier,lineNumber = thisMatch.linenumber,oldstring = thisMatch.string, newString = newString)>
					<cfset result.content = application.com.globalfunctions.rwreplacenocase(string = result.content,substring1 = thisMatch.string,substring2 = newString,scope="one",start = thisMatch.position - 2)>
					<cfset result.itemsFixed ++>
					<cfset result.updated = true>


				</cfif>






			</cfif>


		</cfif>
	</cfloop>


	<cfreturn result>
</cffunction>


<cffunction name="processCFSAVECONTENT">
	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfif structKeyExists(thisObject.attributes,"variable")>
		<cfset variablename = thisObject.attributes.variable>
	<cfelse>
		<cfset variablename = "variable Attribute Not set">
	</cfif>

	<cfset tempRequestAction = request.action>

	<cfif variableName contains "HTML">
		<!--- have to pass in the innerHTML to this function - otherwise it all gets discarded since in a cfsavecontent --->
		<cfset request.action = "HTMLEDITFORMAT" >
		<CFSET result = processHTMLEDITFORMAT(content = thisObject.innerHTML,inputoutput = inputoutput, identifier = identifier, linenumber = thisObject.linenumber )>
		<cfif result.updated>
			<cfset result.content = replace(thisObject.string,thisObject.innerHTML,result.content)>
		</cfif>

	<CFELSEIF variablename contains "SQL">
		<!--- can just pass the whole object in here --->
		<cfset request.action = "CF_QUERYPARAM" >
		<cfset thisObject.name = variablename>
		<CFSET result = processCFQUERY(thisobject = thisObject,inputoutput = inputoutput, identifier = identifier)>

	<CFELSEIF variablename contains "JS">

	<CFELSEIF variablename contains "XML" or variablename contains "soap" >

	<CFELSEIF variablename contains "css">


	<CFELSE>
		<cfset result.output = result.output & '<tr><td colspand="3"></td><td>#thisObject.linenumber#</td><td></td><td>CFSAVECONTENT #variablename# type not known</td></tr>'>
		<cfset LogIssue (filename=identifier,lineNumber = thisObject.linenumber,oldstring = "",newstring="",message="CFSAVECONTENT #variablename#")>

	</cfif>

	<cfset request.action = tempRequestAction  >


	<cfreturn result>

</cffunction>


<cffunction name="processHTMLEDITFORMAT">

	<cfargument name="content">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >
	<cfargument name="lineNumber" default = 1>


	<cfset var result = {updated = false,content = content, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var groupNames = '' />
	<cfset var matches = '' />
	<cfset var newString = '' />
	<cfset var thisMatch = '' />

	<!--- remove all cfquery,cf_,  --->
	<cf_crashProtection identifier="#identifier#" comment = 'removecomments'>
		<cfset content = application.com.regExp.removeAllComments (String  = content, preserveLineNumbers = true,preserveLength = true)>
	</cf_crashProtection>

	<cf_crashProtection identifier="#identifier#" comment = 'ReplaceHMTL'>
		<cfset content = application.com.regExp.replaceHTMLTagsInString(inputString = content,htmltag="cfquery|script|style|cfmail|cf_mail|cf_query|cfscript|cfsavecontent|object|textarea|cfxml",hasendtag=true,replaceExp="",preserveLineNumbers=true,preserveLength = true)>
	</cf_crashProtection>


	<cf_crashProtection identifier="#identifier#" comment = 'CFSET'>
		<!--- special look at cfset, to prevent problems if value being set has a > in it --->
		<cfset content = application.com.regExp.reReplaceWithBlank(InputString = content,reg_expression = "<cfset.*?=\s*([""']).*?\1\s*>")>
	</cf_crashProtection>

	<cf_crashProtection identifier="#identifier#" comment = 'ReplaceHTML No Closing'>
		<!--- need to inspect cfsavecontent at a later date --->
		<cfset content = application.com.regExp.replaceHTMLTagsInString(inputString = content,htmltag="(cf.*?|cfset|cfloop|frame|tr|td|table|input|a|img|div|select|option|form|span)",hasendtag=false,replaceExp="",preserveLineNumbers=true,preserveLength = true, useSophisticatedRegExp = true)>
	</cf_crashProtection>

	<cfset matches = application.com.regExp.reFindAllOccurrences(reg_expression = regExps.CFVariable.regExp,string = content, groupnames=regExps.CFVariable.groupnames,getlinenumbers = true,usejavaMatch=true)>

	<cfset result.output = result.output & '<tr><td colspand="3">#identifier#</td><td></td></tr>'>

	<cfloop from=#arrayLen(matches)# to="1" step="-1" index="matchIndex"> <!--- have to loop backwards so that increase in length doesn't cause our positions to be wrong --->
		<cfset thisMatch = matches[matchIndex]>
		<cfif isVariableSafe(thismatch.string,"html")>
			<!--- OK has htmleditformat--->
		<cfelseif refindNoCase("\A(.*WDDX.*|.*XML.*|.*HTML.*|.*CSS.*|##phr_.*)\Z",thismatch.string)>
			<!--- Don't do any variable with HTML or XML in the name,or a phrase --->

			<cfelseif refindNoCase("\A##(ncontent|.*\.control|elementDetail|header.detail|detail|DisplayBorderGetThisElement.translations.headline|.*soap.*|thisTag.generatedContent|npostmarkup|attributes.notetext)##\Z",thismatch.string)>
			<!--- these are ones which look as if they might have htmltags in them --->

		<cfelse>

				<cfset result.itemsFound ++>
				<cfset result.output = result.output & '<tr><td colspand="3"></td><td>#thisMatch.linenumber#</td><td>#htmleditformat(thisMatch.string)#</td><td>#htmleditformat(mid(content,max(1,thisMatch.position - 50),thisMatch.length + 100))#</td></tr>'>
				<cfset newString = "##htmleditformat(#thisMatch.innerText#)##">
				<cfset LogReplacement (filename=identifier,lineNumber = arguments.linenumber + thisMatch.linenumber -1,oldstring = thisMatch.string, newString = newString)>
				<cfset result.content = application.com.globalfunctions.rwreplacenocase(string = result.content,substring1 = thisMatch.string,substring2 = newString,scope="one",start = thisMatch.position - 2)>
				<cfset result.itemsFixed ++>
				<cfset result.updated = true>

		</cfif>
	</cfloop>

	<!---
	<cfset content = application.com.regExp.AddLineNumbers(string= content)>
	<cfoutput>

	<PRE>
		#htmleditformat(content)#
	</PRE>
	</cfoutput>
	 --->

	<cfreturn result>

</cffunction>



<cffunction name="replaceHTMLTagWithCustomTag">
	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0,issueLineNumbers = structNew()}>
	<cfset var tagName = thisObject.tagName>  <!--- could be passed as an argument, but this will preserve the case --->
	<cfset var hasEndTag = false>  <!--- updated to correct value below --->
	<cfset var openingTag = thisObject.openingTag>
	<cfset var tagCase = getCase(openingTag)>
	<cfset var findifs = {}>
	<cfset var failureMessage = "">
	<cfset var okToUpdate = '' />
	<cfset var cfifregexp = '' />
	<cfset var cfifgroupnames = '' />
	<cfset var iifregexp = '' />
	<cfset var iifgroupnames = '' />
	<cfset var found = '' />
	<cfset var tempCondition = '' />
	<cfset var condition = '' />
	<cfset var testObjectAgain = '' />
	<cfset var cfifOrIif = '' />
	<cfset var key = '' />

	<cfif structKeyExists (thisObject,"endTag")>
		<cfset hasEndTag = true>
	</cfif>

	<cfif refindNoCase("##.*##",thisObject.openingTag)>
		<!--- opening and closing cfoutputs can be removed --->
		<cfset openingTag = rereplacenocase(openingTag,"<cfoutput>|</cfoutput>","","ALL")>

		<cfset result.itemsFound ++>
		<cfset okToUpdate = true>



		<cfif refindnocase("<cfif|##iif",openingTag)>

			<!--- look for iif and cfif inside the tag, these need to be converted  --->
			<cfset cfifregexp = "<cfif(.*?)>([^=]*)=?(.*?)</cfif>">
			<cfset cfifgroupnames = {1="condition",2="attribute",3="attributeValue"}>
			<cfset findifs.cfif = application.com.regexp.refindalloccurrences(reg_expression = cfifregexp,string= openingTag,groupnames=cfifgroupnames,usejavaMatch=true)>

			<cfset iifregexp = "##iif\(((?:\((?:\(.*?\)|.)*?\)|.)*?),(.*?),(.*?)\)##">
			<cfset iifgroupnames = {1="condition",2="true",3="false"}>
			<cfset findifs.iif = application.com.regexp.refindalloccurrences(reg_expression = iifregexp,string= openingTag,groupnames=iifgroupnames,usejavaMatch=true)>

			<cfif arrayLen(findifs.cfif) or arraylen(findifs.iif)>

				<cfloop collection = "#findifs#" item="cfifOrIif">
					<cfloop array="#findifs[cfifOrIif]#" index = "found">

							<cfif cfifOrIif is "iif">
								<cfset found.attribute = rereplacenocase(found.true,"de\s*\(\s*[""']+\s*(.*?)\s*[""']+\s*\)","\1")>
								<cfset found.attribute = rereplacenocase(found.ATTRIBUTE,"[""']+(.*?)[""']+","\1")>
							</cfif>

							<cfif refindNoCase("checked|disabled",found.attribute)>
								<!--- need to replace any ## around variables in condition.  Eg  #iif(#x# is 1,true,false) is not valid code--->
								<cfset tempCondition = rereplace(found.condition,"##","","ALL")>
								<cfif find(" ",found.condition) is 0>
									<!--- no spaces, single condition, can just be the attribute value plus some ## (may not need the )--->
									<cfset condition = '"###tempCondition###"'>
								<cfelse>
									<cfset condition = '"##iif(#tempCondition#,true,false)##"'>
								</cfif>
								<cfset openingTag = replace(openingTag,found.string,"#found.attribute#=#condition#")>
							<cfelse>
								<cfset okToUpdate = false>
								<cfset failureMessage = "contains cfif or iif">
							</cfif>
					</cfloop>

				</cfloop>


			<cfelse>
				<cfset okToUpdate = false>
				<cfset failureMessage = "couldn't match cfif">
			</cfif>
		</cfif>

		<!--- check again for <cf in the content --->
		<cfif refindnocase("<cf",openingTag)>
				<cfset okToUpdate = false>
				<cfset failureMessage = "has <cf inside tag">
		</cfif>

		<!--- I can't deal with stuff of the form <INPUT  #allattributesinasinglevariable#>
		look through all the attributes checking that non of the names have # in them
		however this code gets tripped up by things like value="#form["frmWhereClauseList"]#" because this is parsed as value="#form#"  & frmWhereClauseList"]#"
		so I am going to temporarily replace [" and "] with something else
		--->
		<cfset tempcontent = openingTag>
		<cfset tempContent = rereplace(tempcontent ,'\["(.*?)"\]',"['\1']","ALL" )>
		<cfset tempContent = rereplace(tempcontent ,'\("' , "(" ,"ALL" )>
		<cfset tempContent = rereplace(tempcontent ,'"\)' , ")" ,"ALL" )>
		<cfset tempContent = rereplace(tempcontent , '",' , "," ,"ALL" )>
		<cfset tempContent = rereplace(tempcontent , ',"' , "," ,"ALL" )>

		<cfset var findArgs = {inputstring = tempContent ,htmltag=TagName,hasendtag=false,getLineNumbers = false,ignorecomments=false}>
		<cfset testObjectAgain = application.com.regexp.findHTMLTagsInString(argumentCollection = findArgs)>

		<cfif arrayLen(testObjectAgain) is 0>
			<cfset result.output = "<TR><TD colspan=5></TD><td >#thisObject.linenumber# #htmleditformat(thisObject.string)#</td><td >#htmleditformat(result.content)#</td><td>Updated</td></TR>">
			<cfoutput>

			</cfoutput>
			<cfset okToUpdate = false>

		<CFELSE>

			<cfset updateRequired = false>

			<cfloop collection="#testObjectAgain[1].attributes#" item="key">
				<cfset value = testObjectAgain[1].attributes[key]>
				<cfif refindnocase("##iif",key)>
					<cfset okToUpdate = false>
					<cfset failureMessage = "found attribute name containing iif - #key#">
					<cfbreak>
				<cfelseif refindnocase("##",key)>
					<cfset okToUpdate = false>
					<cfset failureMessage = "found attribute name containing hash ## - #key#">
					<cfbreak>

				<cfelse>

					<!--- look at all the cfvariables --->
					<!--- get rid of any htmleditformats --->
					<cfset value = reReplaceNoCase(value,"htmlEditFormat\((.*?)\)","\1","ALL")>
					<cfset cfvariablematches = application.com.regExp.reFindAllOccurrences(reg_expression = regExps.CFVariable.regExp,string = value, groupnames=regExps.CFVariable.groupnames,getlinenumbers = false,usejavaMatch=true)>

					<cfloop array=#cfvariablematches# index="thisMatch">
						<cfif isVariableSafe(thismatch.string,"html")>
							<!--- OK has htmleditformat--->
						<cfelseif refindNoCase("\A(.*XML.*|.*HTML.*|.*JSON.*|.*_JS##)\Z",thismatch.string)>
							<!--- Don't do any variable with HTML or XML in the name--->

						<cfelse>
							<cfset updateRequired = true>
							<cfbreak>
						</cfif>
					</cfloop>

				</cfif>
			</cfloop>

		</cfif>


		<cfif okToUpdate>
			<cfif updateRequired>
				<cfset result.itemsFixed ++ >
				<!--- needs to be cf_ version of the tag --->
				<!--- get rid of any htmleditformats --->
				<cfset openingTag = reReplaceNoCase(openingTag,"htmlEditFormat\((.*?)\)","\1","ALL")>
				<cfset result.content = replaceNoCase(openingTag,"<" & tagName,setCase("<CF_" & Tagname,tagCase))>
				<cfif hasEndTag>
					<cfset result.content = result.content & thisObject.innerHTML & setCase("</CF_" & Tagname & ">",tagCase)>
				</cfif>

				<cfset result.updated = true>
				<cfset LogReplacement (filename=identifier,lineNumber = thisObject.linenumber,oldstring =thisObject.string, newString = result.content )>
				<cfset result.output = "<TR><TD colspan=5></TD><td >#thisObject.linenumber# #htmleditformat(thisObject.string)#</td><td >#htmleditformat(result.content)#</td><td>Updated</td></TR>">
			<cfelse>
				<!--- safe variable, no update required --->
			</cfif>
		<cfelse>
			<cfset LogIssue (filename=identifier,lineNumber = thisObject.linenumber,oldstring =thisObject.string, message = "Cannot be updated")>
			<cfset result.output = "<TR><TD colspan=5></TD><td >#thisObject.linenumber# #htmleditformat(thisObject.string)#</td><td ></td><td><font color='red'>Cannot be updated</font> #failuremessage#</td></TR>">
			<cfset result.issueLineNumbers[thisObject.linenumber] = "">
		</cfif>


	<cfelse>
		<!--- No update required <cfset result.output = "<TR><TD colspan=3></TD><td >#thisObject.linenumber# #htmleditformat(thisObject.string)#</td><td></td><td >No Update Required</td></TR>"> --->
	</cfif>

	<cfreturn result>

</cffunction>



<cffunction name="processCFQuery">
	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

			<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>
			<cfset var queryNameOutput = false>
			<cfset var output = "">
			<cfset var tempoutput = "">
			<cfset var thisMatch = "">
			<cfset var updateIdentifier = "">
			<cfset var conditionresult = "">
			<cfset var queryContent = "">
			<cfset var queryParam = "">
			<cfset var regExp = "">
			<cfset var groupNames = "">
			<cfset var findMatches = "">
			<cfset var guessDataTypeResult = ""							>
			<cfset var useQueryParamFunction = '' />
			<cfset var argumentStruct = '' />
			<cfset var findCFSET = '' />
			<cfset var thisCFSet = '' />
			<cfset var thisResult = '' />
			<cfset var getWhereClause = '' />
			<cfset var regExps = '' />
			<cfset var doctoredCFVariable = '' />
			<cfset var escapesinglequotesRegExp = '' />
			<cfset var preservesinglequotesResult = '' />
			<cfset var commaOrSpace = '' />
			<cfset var replacementString = '' />
			<cfset var key = '' />
			<cfset var regExpItem = '' />


			<cfif thisObject.tagName is "cfset">
				<cfset useQueryParamFunction = true>
			<cfelse>
				<cfset useQueryParamFunction = false>
			</cfif>

			<cfif thisObject.tagName is "CFQUERY" and structKeyExists(thisObject.attributes,"dbtype")>
				<!--- don't worry about QoQ --->
				<cfreturn result>
			</cfif>

					<cfif structKeyExists (inputOutput,"argumentArray")>
						<cfset argumentStruct = arrayToStruct(array = inputOutput.argumentArray, key="ATTRIBUTES",KEY2="NAME")>
					<cfelse>
						<cfset argumentStruct = {}>
					</cfif>

					<!--- look for any cfsets with equals signs inside the query - only deals with double quoted and can't handle escaped double quote --->
					<cfset regExp = "(<(cfset)\s*(\S*)\s*=\s*([""]))([^""]*?=[^""]*?)(("")\s*>)">
					<cfset groupnames = {1="openingTag",2="tagName",3="variablename",4="quote",5="innerHTML",6="endTag",7="endquote"}>

					<cfset findCFSET = application.com.regexp.refindalloccurrences(reg_expression = regexp,string =thisObject.innerhtml,groupnames = groupnames,usejavaMatch=true)>

						<cfloop array="#findCFSET#" index="thisCFSet">
							<cfset thisCFSet.lineNumber = thisObject.linenumber>
							<cfset thisCFSet.attributes.name = thisCFSet.variablename>
							<cfset thisResult = processCFQuery (thisObject = thisCFSet,inputoutput =inputoutput,identifier = identifier )>
							<cfloop collection="#thisResult#" item="key">
								<cfif isBoolean (thisResult[key])>
									<cfset result[key] = result[key] OR thisResult[key]>
								<cfelseif isNumeric (thisResult[key])>
									<cfset result[key] = result[key] + thisResult[key]>
								<cfelseif key is not "content">
									<cfset result[key] = result[key] & thisResult[key]>
								</cfif>
							</cfloop>

							<cfif thisResult.updated is true>
								<cfset result.content = replace(result.content,thisCFSet.string,thisResult.content)>
								<cfset result.updated = true>
							</cfif>

						</cfloop>


					<!---
					No don't worry about the where clause, do it everywhere'
					find last where clause
					<cfset groupNames = {1 = "clause"}>
					<cfset regExp = "\swhere\s((?:(?!where)[\s\S])*)\Z">
					<cfset getWhereClause = application.com.regexp.refindalloccurrences(reg_Expression = regExp, string = application.com.regExp.removeAllComments(thisObject.innerhtml), groupnames = groupnames)>

					<cfif arrayLen(getWhereClause)>
					 --->

						<!--- started with just one regexp, but eventually broke out into two, one hardcoded to find expressions in single quotes

						Next character can't be
							.  - suggests a cf variable used as a table

						--->
						<cfset regExps = {}>
						<cfset regExps.int.groupNames = {1 = "openingWhiteSpace", 2 = "sqlvariable",3="beforeoperator",4="operator",5="afterOperator",6="bracket",7="N",8="quote",9="cfexpression",10="cfvariable",11="endquote",12="beforeEndBracket",13="endbracket",14="nextcharacter"}>
						<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
						<cfset regExps.int.regexp = "(?x)  							## enable commenting
												(\s|\A|,)  							## start with a space or a comma
												([A-Z0-9_()\.]+) 						## a sql variable (or maybe a function plus a variable)
												(\s*)  								## optional space
												(\sNOT\s*IN|\sIN|\sBETWEEN.*?AND|\sBETWEEN|<>|>|<|>=|<=|>|=)  ## an operator
												(\s*)  								## optional space
												(\()?  								## optional brackets
												\s*		  							## optional space
												()  								## not used (N in the varchar version)
												()  								## not used (quote in the varchar version)
												(\##([^\##]*?)\##)  						## a CF variable
												()?  								## not used (quote in the varchar version)
												(?:(\s*?)(\)))?  						## optional bracket (with optional preceeding spaces)
												([^.])  							## any character.  Not .
												">

						<cfset regExps.varchar.groupNames = {1 = "openingWhiteSpace", 2 = "sqlvariable",3="beforeoperator",4="operator",5="afterOperator",6="bracket",7="N",8="quote",9="cfexpression",10="cfvariable",11="endquote",12="beforeEndBracket",13="endbracket",14="nextcharacter"}>
						<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
						<cfset regExps.varchar.regexp = "(?x)  								## enable commenting
														(\s|\A|,)    						## start with a space or a comma
														([A-Z0-9_()\.]+)  					## a sql variable (or maybe a function plus a variable)
														(\s*)								## optional space
														(\sNOT\s*IN|\sIN|\sNOT\s*LIKE|\sLIKE|\sBETWEEN.*?AND|\sBETWEEN|<>|>|<|>=|<=|>|=)     ## an operator
														(\s*)								## optional space
														(\()?								## optional brackets
														\s*									## optional space
														(N)?								## optional N
														(')									## a quote
														([^']*?\##(.*?)\##[^']*?)			## a CF variable
														(')									## a quote
														(?:(\s*?)(\)))?  						## optional bracket (with optional preceeding spaces)
														([^.])  							## any character.  Not .
														">


						<cfset queryContent = result.content>
						<cfset queryContent = replace(queryContent,thisObject.openingTag,"")> <!--- getting rid of opening tag like this is needed when we are trying to process a cfset --->
						<cfset queryContent = removeSafeStuffFromQuery (queryContent)>

					<cfloop collection = #regExps# item="regExpItem">


						<cfset findMatches = application.com.regexp.refindalloccurrences(reg_Expression = regExps[regExpItem].regExp, string = queryContent, groupnames = regExps[regExpItem].groupnames,getLineNumbers = true,usejavaMatch=false)>

<!---  <pre>
<cfoutput>#htmleditformat(querycontent)#
</cfoutput>
</pre>
<cfdump var="#findmatches#"> --->
						<cfloop array = "#findMatches#" index="thisMatch">
							<cfset queryContent = replace (queryContent,thisMatch.string,"")>
							<cfset thisMatch.linenumber = thisObject.linenumber + thisMatch.linenumber -1 >

							<!--- sometimes a closing bracket is picked up when there hasn't been an opening bracket - deal
							commented out for moment - causes other problems
							<cfif thisMatch.endBracket is not "" and thisMatch.Bracket is "">
								<cfset thisMatch.string = replace(thisMatch.string,"#thisMatch.beforeEndBracket##thisMatch.endBracket##thisMatch.nextcharacter#","#thisMatch.beforeEndBracket##thisMatch.nextcharacter#")>
								<cfset thisMatch.endBracket = "">
							</cfif>
							--->

							<!--- the final character is not actually wanted as part of the match string - needed for something else. So remove last character from string --->
							<cfif thisMatch.nextCharacter is not "">
								<cfset thisMatch.string = left (thisMatch.string,len(thisMatch.string) -1)>
							</cfif>

							<cfset updateIdentifier = makeSafeUpdateIdentifier(Identifier & ":"  & thisMatch.string)>
							<cfset conditionresult = {cfvariable = thisMatch.cfvariable,cfexpression = thisMatch.cfexpression,string = thisMatch.string,sqldatatype="",list = false,needsqueryParam = true, isargument = false, argumentdatatype = "",autoReplaceOK = true, updated = false,message = ""}>
							<cfset doctoredCFVariable = replacenocase(thisMatch.cfvariable,"arguments.","","all")>

 							<cfif structKeyExists(argumentStruct,doctoredCFVariable)>
								<cfset conditionresult.isargument = true>
								<cfif structKeyExists(argumentStruct[doctoredCFVariable],"TYPE")>
									<CFSET conditionresult.argumentdatatype = argumentStruct[doctoredCFVariable].TYPE>
								</cfif>
								<cfif structKeyExists(argumentStruct[doctoredCFVariable],"default") and argumentStruct[doctoredCFVariable].default is "">
									<!--- can't do anything with a default of "" so set isargument to false --->
									<cfset conditionresult.isargument = false>
								</cfif>
							</cfif>


							<cfset escapesinglequotesRegExp = 'escapesinglequotes\((.*?)\)'> <!--- specific to relayplo.cfc, needs to be removed --->
							<cfset conditionresult.cfexpression = rereplacenocase(conditionresult.cfexpression,escapesinglequotesRegExp,"\1")>


							<cfset guessDataTypeResult = guessDataType (sqlvariable = thisMatch.sqlvariable, cfexpression = conditionresult.cfexpression, quote = thisMatch.quote ) >
							<cfset conditionresult.sqldatatype = guessDataTypeResult.sqldatatype>
							<cfset conditionresult.cfexpression = guessDataTypeResult.cfexpression>

							<!--- check whether here is an IN statement - in which case we need a list --->
							<cfif thisMatch.operator contains "in">
								<cfset conditionresult.list = true>
							</cfif>

							<!--- a few special cases where we do not need query param at all --->
							<cfif isVariableSafe(thisMatch.cfvariable)>
								<cfset conditionresult.needsqueryParam = false>
							<cfelseif isVariableSafelyTyped(thisMatch.cfvariable,argumentStruct)>
								<cfset conditionresult.needsqueryParam = false>
							</cfif>


							<cfif refindNoCase("preserveSingleQuotes(.*)",thisMatch.cfvariable)>
								<!--- special case for preservesinglequotes --->
								<cfset preservesinglequotesResult = logPreserveSingleQuotes (identifier = identifier,variablename = conditionresult.cfexpression,linenumber = thisMatch.linenumber)>
								<cfif preservesinglequotesResult.done>
									<cfset conditionresult.needsqueryParam = false>
									<cfset conditionresult.message = "Preserve Single Quotes dealt with">
								<cfelseif preservesinglequotesResult.replaceWith is not "">
									<cfset conditionresult.needsqueryParam = true>
									<cfset conditionresult.cfexpression = preservesinglequotesResult.replaceWith>
									<cfset conditionresult.message = "Preserve Single Quotes replacement">
									<!--- take a look at what has been returned to get correct datatype etc (may replace a quotedvaluelist ) --->
									<cfset guessDataTypeResult = guessDataType (sqlvariable = thisMatch.sqlvariable, cfexpression = conditionresult.cfexpression, quote = thisMatch.quote ) >
									<cfset conditionresult.sqldatatype = guessDataTypeResult.sqldatatype>
									<cfset conditionresult.cfexpression = guessDataTypeResult.cfexpression>

								<cfelse>
									<!--- doesn't acually need query param but for time being need to show these items on screen --->
									<cfset conditionresult.needsqueryParam = true>
	 								<cfset conditionresult.sqldatatype = "">
									<cfset conditionresult.message = "Preserve Single Quotes">
								</cfif>

							<cfelseif refindNoCase("application.com.security.queryparam(.*)",thisMatch.cfvariable)>
								<!--- any variable wrapped with queryParam is OK --->
								<cfset conditionresult.needsqueryParam = false>
 								<cfset conditionresult.sqldatatype = "">

							<cfelseif refindNoCase(".*_sql",thisMatch.cfvariable)>
								<!--- variable names ending in _SQL are snippets of SQL (probably should have preserve single quotes actually) --->
								<cfset conditionresult.needsqueryParam = false>
 								<cfset conditionresult.sqldatatype = "">

							<cfelseif thisMatch.quote is not thisMatch.endquote>
								<!--- if start and end quotes do not match--->
								<cfset conditionresult.needsqueryParam = true>
 								<cfset conditionresult.sqldatatype = "">
								<cfset conditionresult.message = "Start and End Quotes do not match">

							<cfelseif thisMatch.Bracket is "("  AND thisMatch.endbracket is not ")">
								<!--- if start and end brackets do not match--->
								<cfset conditionresult.needsqueryParam = true>
 								<cfset conditionresult.sqldatatype = "">
								<cfset conditionresult.message = "Start and End Brackets do not match">

							<cfelseif thisMatch.nextCharacter is not "" and not refindNoCase("[ ,\n\t\r\)"";<]",thisMatch.nextCharacter)>
								<!--- if the next character is not , or space then something odd going on - we will allow < as well --->
								<cfset conditionresult.needsqueryParam = true>
 								<cfset conditionresult.sqldatatype = "">
								<cfset conditionresult.message = "Something odd going on!.  Next character is #thisMatch.nextCharacter# #asc(thisMatch.nextCharacter)#">
							</cfif>


							<!--- if the cfvariable is a date or integer and is a function argument
								i) 	if argument has the correct datatype then don't need cfqueyrparam
							 	ii) if argument does not have the correct datatype then suggest adding it!
							--->

							<cfif conditionresult.isargument and (conditionresult.argumentdatatype is "numeric" or conditionresult.argumentdatatype is "date")>
								<cfset conditionresult.needsqueryParam = false>
							<cfelseif conditionResult.isArgument and conditionResult.argumentdatatype is "" and (conditionResult.sqldatatype is not "CF_SQL_VARCHAR" ) and not conditionresult.list
										and not listfind(form.update,updateIdentifier)
										and not form.updateFunctionArguments
											>

								<cfset conditionresult.needsqueryParam = true>
 								<cfset conditionresult.sqldatatype = "">
								<cfset conditionresult.message = "Try an argument datatype">
							</cfif>

							<!--- query param needed so do the update --->
							<cfif conditionresult.needsQueryParam>
								<cfset result.itemsFound ++>

								<cfif conditionResult.isArgument and conditionResult.argumentdatatype is "" and (conditionResult.sqldatatype is not "CF_SQL_VARCHAR" ) and not conditionresult.list and not form.updateFunctionArguments>
									<cfset conditionResult.autoReplaceOK = false>
								</cfif>

								<cfset conditionresult.checkbox = '<input type="checkbox" name="update" value="#updateIdentifier#">'>

								<cfif ((structKeyExists(form,"update") and listfindnocase(form.update,updateIdentifier)) OR structKeyExists(form,"dataType_#updateIdentifier#")) and form["dataType_#updateIdentifier#"] is not "">
										<cfset conditionresult.sqldatatype = listfirst(form["dataType_#updateIdentifier#"])>
										<cfset conditionResult.autoReplaceOK = true>
								</cfif>

								<cfif conditionresult.sqldatatype is not "">

									<cfif not useQueryParamFunction>
										<cfset queryParam = '<cf_queryparam '>
										<cfset commaOrSpace = " ">
									<cfelse>
										<cfset queryParam = '##application.com.security.queryparam('>
										<cfset commaOrSpace = ",">
									</cfif>
									<cfset queryParam = queryParam  & 'value="#conditionresult.cfexpression#"#commaOrSpace#CFSQLTYPE="#conditionresult.sqldatatype#" '>
									<cfif conditionresult.list>
										<cfset queryParam = queryParam & '#commaOrSpace#list="true"'>
									</cfif>
									<cfif not useQueryParamFunction>
										<cfset queryParam = queryParam & '>'>
									<cfelse>
										<cfset queryParam = queryParam & ')##'>
									</cfif>

									<cfset conditionresult.wholereplacement = "#thisMatch.openingWhiteSpace##thisMatch.sqlvariable# #thisMatch.operator# #thisMatch.bracket# #queryParam# #thisMatch.endBracket#">


									<cfif (structKeyExists(form,"update") and listfindnocase(form.update,updateIdentifier))
										or (form.autoReplace and conditionResult.autoReplaceOK)>

										<!--- do update --->
										<cfset replacementString = conditionresult.wholereplacement>

										<cfif not useQueryParamFunction>
											<!--- leave out comment
											<cfset replacementString = replacementString & "<!--- securityupdate #replace(conditionresult.string,"#thisMatch.beforeoperator##thisMatch.operator##thisMatch.afteroperator#","#thisMatch.beforeoperator#|#thisMatch.operator#|#thisMatch.afteroperator#")#---> ">
											--->
										</cfif>

										<!--- upadte content with new string, sometimes end up with a leading space on conditionresult.string which isn't really there (say a tag has been removed) so need to trim - but hope don end up with substring problems --->
										<cfset oldContent = result.content>
										<cfset result.content = replace(result.content,trim(conditionresult.string),trim(replacementString))>  <!--- disrupt the comment with || around the operator so does not show up on new searches (although this process does remove comments) --->

										<cfif oldContent is result.content>
											<cfoutput>
													Update Failed: #updateIdentifier#
											<pre>

											#htmleditformat(result.content)#
											Replace: #htmleditformat(trim(conditionresult.string))#
											With: #htmleditformat(trim(replacementString))#
											</pre>
												<cfdump var="#conditionresult#">
												<cfdump var="#thismatch#">
											</cfoutput>

										<cfelse>
											<cfset result.updated = true>
											<cfset conditionresult.updated = true>
											<cfset LogReplacement (filename=identifier,lineNumber = thisMatch.linenumber,oldstring =conditionresult.string, newString = conditionresult.wholereplacement )>
											<cfset result.itemsFixed ++>
										</cfif>
									</cfif>
								<cfelse>

										<cfset LogIssue (filename=identifier,lineNumber = thisMatch.linenumber,oldstring =conditionresult.string, message = conditionresult.message)>

								</cfif>


								<cfsavecontent variable  = "tempOutput">
								<cfoutput>
<!---
								<cfoutput>#htmleditformat(result.content)# <BR><BR></cfoutput>
 --->
								<cfif not queryNameOutput >
									<cfset queryNameOutput = true>
									<tr><td colspan="3">&nbsp</td><td colspan="8">Query: <cfif structKeyExists(thisObject.attributes,"name")>#thisObject.attributes.name#<cfelse>Unnamed</cfif>.  Line:
									#thisObject.linenumber#</td></tr>
								</cfif>

								<tr>
									<td colspan="4">&nbsp</td>
									<td>
										<cfif structKeyExists(conditionresult,"checkbox")>
											#conditionresult.checkbox#
										</cfif>
									</td>

									<td>
										<strong>#htmleditformat(conditionresult.string)#<br /></strong>
									</td>

									<td>
										<cfif structKeyExists (conditionresult,"wholereplacement")>
											<strong>#htmleditformat(conditionresult.wholereplacement)#<br /></strong>
										</cfif>
									</td>
									<td>
										<cfif conditionresult.updated >
										Updated<br />
											<!--- <cfdump var="#thismatch#"><cfabort>  --->

										<cfelse>
											<cfif conditionresult.sqldatatype is "" AND conditionresult.message is "">
													<strong>What DataType?</strong>

											</cfif>
												<strong>#conditionresult.message#</strong>

											<cfif (conditionresult.sqldatatype is "" AND conditionresult.message is "") OR conditionresult.message is "try an argument datatype">
													<select name = "dataType_#updateIdentifier#">
														<option/>
														<option   value="cf_sql_integer">cf_sql_integer</option>
														<option   value="cf_sql_bit">cf_sql_bit</option>
														<option value="cf_sql_timestamp">cf_sql_timestamp</option>
														<option value="cf_sql_numeric">cf_sql_numeric</option>
													</select>
											</cfif>
										</cfif>

									</td>

								</tr>
								</cfoutput>
								</cfsavecontent>
								<cfset result.output = result.output & tempOutput>
							</cfif>

						</cfloop>
					</cfloop>

	<cfreturn result>
</cffunction>


<cffunction name="removeSafeStuffFromQuery">
	<cfargument name="queryContent">

	<cfset var tagPlaceHolder = "< >">
	<cfset queryContent = application.com.regExp.removeAllComments(queryContent)>
	<cfset queryContent = rereplace(queryContent,"--.*?\n"," ","ALL")>
	<cfset queryContent = rereplace(queryContent,"##application.com.security.queryparam\(.*?\)##","  ","ALL")>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfquery",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cf_",hasendtag=false,replaceExp=tagPlaceHolder)> <!--- specifically cf_dateRangeRetrieve--->
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfset",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfswitch",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfcase",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfcase",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfif",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfif",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfELSE",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfloop",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfloop",hasendtag=false,replaceExp=tagPlaceHolder)>
	<cfset queryContent = rereplacenocase(queryContent,"select\s*scope_identity\(\)\s*as\s*[a-zA-Z0-9_]*","")>

	<cfreturn queryContent>

</cffunction>


<cffunction name="processCFQueryInsert">
	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

			<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>
			<cfset var queryNameOutput = false>
			<cfset var output = "">
			<cfset var tempoutput = "">
			<cfset var thisMatch = "">
			<cfset var updateIdentifier = "">
			<cfset var conditionresult = "">
			<cfset var queryContent = "">
			<cfset var queryParam = "">
			<cfset var regExp = "">
			<cfset var groupNames = "">
			<cfset var findMatches = "">
			<cfset var guessDataTypeResult = ""							>
			<cfset var useQueryParamFunction = '' />
			<cfset var argumentStruct = '' />
			<cfset var regExps = '' />
			<cfset var findInserts = '' />
			<cfset var columnArray = '' />
			<cfset var insertArray = '' />
			<cfset var findLeftRegExp = '' />
			<cfset var findLeftGroupNames = '' />
			<cfset var findLeftArray = '' />
			<cfset var insertExpression = '' />
			<cfset var findCFVariableRegExp = '' />
			<cfset var findCFVariableGroupNames = '' />
			<cfset var findCFVariableArray = '' />
			<cfset var thisVariable = '' />
			<cfset var preservesinglequotesResult = '' />
			<cfset var commaOrSpace = '' />
			<cfset var replacementString = '' />
			<cfset var thisInsert = '' />
			<cfset var columnIndex = '' />
			<cfset var regExpItem = '' />

			<cfif thisObject.tagName is "cfset">
				<cfset useQueryParamFunction = true>
			<cfelse>
				<cfset useQueryParamFunction = false>
			</cfif>

			<cfif thisObject.tagName is "CFQUERY" and structKeyExists(thisObject.attributes,"dbtype")>
				<!--- don't worry about QoQ --->
				<cfreturn result>
			</cfif>

					<cfif structKeyExists (inputOutput,"argumentArray")>
						<cfset argumentStruct = arrayToStruct(array = inputOutput.argumentArray, key="ATTRIBUTES",KEY2="NAME")>
					<cfelse>
						<cfset argumentStruct = {}>
					</cfif>

					<!--- look for
					insert into table ( , , , )
					values ( , , , )


					insert into table ()
					select
					from
					 --->

						<!--- started with just one regexp, but eventually broke out into two, one hardcoded to find expressions in single quotes --->
						<cfset regExps = {}>
						<cfset regExps.values.groupNames = {1 = "tablename", 2 = "columnList",3="insertList",4="4"}>
						<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
<!--- 					<cfset regExps.values.regExp	= "insert\s*into\s*(.*?)\s*(\((?:\(.*?\)|.)*?\))\s*values\s*(\((?:\((?:\(.*?\)|.)*?\)|.)*?\))\s*">   --->
						<cfset regExps.values.regExp	= "insert\s*(?:into)?\s*(.*?)\s*(\(.*?\))\s*values\s*(\(.*\))\s*(\Z|select|end|;)">

						<cfset regExps.select.groupNames = {1 = "tablename", 2 = "columnList",3="insertList",4="4"}>
						<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
<!--- 						<cfset regExps.select.regExp	= "insert\s*into\s*(.*?)\s*\(((?:\(.*?\)|.)*?)\)\s*select\s*(.*?)(from|\Z)"> --->
						<cfset regExps.select.regExp = "(?x)  							## enable commenting
														insert\s*						## insert
														(?:into)?\s*					## optional into	
														(.*?)\s*						## the table name
														\((.*?)\)\s*					## open brackets, the column list, close brackets
														(?:\(\s*)?select\s*				## optional open bracket before select 
														(.*?)							## the insert list
														(from|\Z)">


					<!--- 	<cfset regExps.bb.groupNames = {1 = "tablename", 2 = "columnList",3="insertList"}>
						<!--- NOTE if adding a new group name, check the numbering of backreferences in the regExp --->
						<cfset regExps.bb.regExp	= "insert\s*into\s*(.*)\s(\(.*?)\)\s*values\s*\((.*)\)\s*"> --->


						<cfset queryContent = thisobject.innerhtml>

					<cfloop collection = #regExps# item="regExpItem">

						<cfset queryContent = replace(queryContent,thisObject.openingTag,"")> <!--- getting rid of opening tag like this is needed when we are trying to process a cfset --->

						<cfset queryContent = application.com.regExp.removeAllComments(queryContent)>
						<cfset queryContent = rereplace(queryContent,"--.*?\n"," ","ALL")>
						<cfset queryContent = rereplacenocase(queryContent,"<CF_QUERYPARAM.*?>"," cfqueryparam placeholder  ","ALL")>
						<cfset queryContent = rereplace(queryContent,"##application.com.security.queryparam\(.*?\)##"," queryparam placeholder ","ALL")>
<!---
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfquery",hasendtag=false,replaceExp="   ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cf_",hasendtag=false,replaceExp="   ")> <!--- specifically cf_dateRangeRetrieve--->
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfset",hasendtag=false,replaceExp="    ")>
--->
						<!--- this may have a bad effect, can't remember why tags commented out below--->
						<cfset queryContent = removeSafeStuffFromQuery(queryContent)>
<!---
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfswitch",hasendtag=false,replaceExp="  ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfcase",hasendtag=false,replaceExp="  ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfcase",hasendtag=false,replaceExp="  ")>

						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfif",hasendtag=false,replaceExp="  ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfif",hasendtag=false,replaceExp="  ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfELSE",hasendtag=false,replaceExp="  ")>

						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="cfloop",hasendtag=false,replaceExp="  ")>
						<cfset queryContent = application.com.regExp.replaceHTMLTagsInString(inputstring = queryContent,htmltag="/cfloop",hasendtag=false,replaceExp="  ")>
 --->

						<cfset findInserts = application.com.regexp.refindalloccurrences(reg_Expression = regExps[regExpItem].regExp, string = queryContent, groupnames = regExps[regExpItem].groupnames,getLineNumbers = true,usejavaMatch=true)>

<!---  <pre>
<cfoutput>#htmleditformat(querycontent)#
</cfoutput>
</pre>
<cfdump var="#findinserts#"> --->

<!---
<cfoutput>#identifier#<br /></cfoutput>
<cfif identifier is "\relay\com\relayFundManager.cfc|createFundAccount|assignBudgets">
	<cfdump var="#findInserts#">
	<cfoutput>
	<pre>#querycontent#
	</pre>
	</cfoutput>
</cfif>
--->

						<cfloop array = "#findInserts#" index="thisInsert">

							<!--- remove this item from queryContent so not picked up by next regexp --->
							<cfset queryContent = replace(querycontent, thisInsert.string,"	")>

							<!--- only need to proces if there any cf variables in string .  Exlude is the select statement is scope_Identity--->
							<cfif refind("##",thisInsert.insertList) and not refind("scope_identity",thisInsert.insertList)>

								<!--- break up column and insert lists into separate columns --->
								<cfset regExp = "(,|\(|\A)(\s*)((?:\((?:\(.*?\)|.)*?\)|.)*?)(\s*)(?=,|\)|\Z)">
								<cfset groupnames={1 = "openingcomma",2 = "openingWhiteSpace",3="expression",4 = "closingWhiteSpace",5="closingComma"}>
								<cfset columnArray = application.com.regexp.refindalloccurrences(reg_Expression = regExp, string = thisInsert.columnlist, usejavaMatch=true, groupnames= groupnames)>
								<cfset insertArray = application.com.regexp.refindalloccurrences(reg_Expression = regExp, string = thisInsert.insertlist, usejavaMatch=true, groupnames= groupnames)>


								<cfif arrayLen(columnArray) is not arrayLen(insertArray)>
									<cfoutput><div class="warningBlock">Non Matching Arrays <BR>
									#identifier#. Line #thisObject.linenumber#<br />
									<table>
									<tr>
										<td>
											#reFindAllOccurrencesArrayToList(columnArray,"expression","<BR>")#<br />
										</td>
										<td>
											#reFindAllOccurrencesArrayToList(insertArray,"expression","<BR>")#<br />
										</td>
									</tr>
									</table>

									<br />
									</div>
									</cfoutput>
									<cfset LogIssue (filename=identifier,lineNumber = thisObject.linenumber,oldstring = thisInsert.insertList, message = "Non Matching arrays")>


							</cfif>



									<cfloop index="columnIndex" from="1" to = "#arrayLen(insertArray)#">

										<cfset conditionresult = {cfvariable = "",cfexpression = "",string = insertArray[columnIndex].expression,sqldatatype="",list = false,needsqueryParam = true, isargument = false, argumentdatatype = "",autoReplaceOK = true, updated = false,message = ""}>

											<!--- check for a left--->
										<cfset findLeftRegExp = "\A\s*left\((.*),([0-9]*)\)\s*\Z">
										<cfset findLeftGroupNames = {1="expression",2="maxlength"}>
										<cfset findLeftArray = application.com.regexp.refindalloccurrences(reg_Expression = findLeftRegExp, string = conditionresult.string, groupnames = findLeftGroupNames,getLineNumbers = false,usejavaMatch=true)>

										<cfif arrayLen(findLeftArray)>
											<cfset insertExpression = findLeftArray[1].expression>
											<cfset conditionresult.maxLength = findLeftArray[1].maxlength>
										<cfelse>
											<cfset insertExpression = conditionresult.string>
											<cfset conditionresult.maxLength = 0>
										</cfif>

										<cfset findCFVariableRegExp = "([^N'])(N)?(')?(##(.*?)##[^']*)(')?([^'])">
										<cfset findCFVariableGroupNames = {1="openingCharacter",2="N",3="quote",4="cfexpression",5="cfvariable",6="endquote",7="closingCharacter"}>
										<cfset closingCharacter = (columnIndex is arrayLen(insertArray)) ? ")" : ","> <!--- this character not picked up by regExp (because needed for susequent match) but will be a comma except for the last item.  Need to add it back to guarantee proper replacement --->
										<cfset findCFVariableArray = application.com.regexp.refindalloccurrences(reg_Expression = findCFVariableRegExp, string = insertArray[columnINdex].string & closingCharacter, groupnames = findCFVariableGroupNames,getLineNumbers = false,usejavaMatch=true)>
										<cfif arrayLen(findCFVariableArray)>

											<cfset thisMatch = findCFVariableArray[1]>
											<cfset thisMatch.lineNumber = thisInsert.linenumber>
											<!--- remove aliases (such as #abcd# AS XYZ)which work in plain SQL but won't here --->
											<cfset thisMatch.cfexpression = rtrim(ltrim(rereplacenocase(thisMatch.cfexpression," AS .*?\Z","","all")))>

											<cfset thisMatch.doctoredCF = replacenocase(thisMatch.cfexpression,"arguments.","","all")>

											<cfif arrayLen(columnArray) is arrayLen(insertArray)>
												<cfset thisMatch.sqlVariable = columnArray[columnIndex].expression>
											<cfelse>
												<cfset thisMatch.sqlVariable = "">
											</cfif>

										<cfset updateIdentifier = makeSafeUpdateIdentifier(Identifier & ":"  & thisMatch.string)>

											<cfset conditionresult.needsqueryParam = true>

											<cfif isVariableSafe(thisMatch.cfvariable)>
												<cfset conditionresult.needsqueryParam = false>
											<cfelseif isVariableSafelyTyped(thisMatch.cfvariable,argumentStruct)>
												<cfset conditionresult.needsqueryParam = false>
											</cfif>
			

											<cfif structKeyExists(argumentStruct,thisMatch.doctoredCF)>
												<cfset conditionresult.isargument = true>
												<cfif structKeyExists(argumentStruct[thisMatch.doctoredCF],"TYPE")>
													<CFSET thisVariable.argumentdatatype = argumentStruct[thisMatch.doctoredCF].TYPE>
												</cfif>
											</cfif>

											<cfset guessDataTypeResult = guessDataType (sqlvariable = thisMatch.sqlVariable, cfexpression = thisMatch.cfexpression, quote = thisMatch.quote ) >
											<cfset conditionresult.sqldatatype = guessDataTypeResult.sqldatatype>
											<cfset conditionresult.cfexpression = guessDataTypeResult.cfexpression>
											<cfset conditionresult.ISARGUMENT = FALSE>

											<!--- a few special cases where we do not need query param at all
											<cfif refindNoCase("request\.relayCurrentUser\.|dateadd\(",thisMatch.cfvariable)>
												<cfset conditionresult.needsqueryParam = false>
											<cfelseif refindNoCase("createODBCDateTime(.*)",thisMatch.cfvariable)>
												<cfset conditionresult.needsqueryParam = false>
											</cfif>
											--->



										<cfif ((structKeyExists(form,"update") and listfindnocase(form.update,updateIdentifier)) OR structKeyExists(form,"dataType_#updateIdentifier#")) and form["dataType_#updateIdentifier#"] is not "">
											<cfset conditionresult.sqldatatype = listfirst(form["dataType_#updateIdentifier#"])>
											<cfset conditionResult.autoReplaceOK = true>
										</cfif>


										<!--- MAY not need any of this for inserts


										<!--- if the cfvariable is a date or integer and is a function argument
											i) 	if argument has the correct datatype then don't need cfqueyrparam
										 	ii) if argument does not have the correct datatype then suggest adding it!
										--->

										<cfif conditionresult.isargument and (conditionresult.argumentdatatype is "numeric" or conditionresult.argumentdatatype is "date")>
											<cfset conditionresult.needsqueryParam = false>
										<cfelseif conditionResult.isArgument and conditionResult.argumentdatatype is "" and (conditionResult.sqldatatype is not "CF_SQL_VARCHAR" ) and not conditionresult.list
													and not listfind(form.update,updateIdentifier)
													and not form.updateFunctionArguments
														>

											<cfset conditionresult.needsqueryParam = true>
			 								<cfset conditionresult.sqldatatype = "">
											<cfset conditionresult.message = "Try an argument datatype">
										</cfif>

										 --->

										<cfif refindnocase("<CF",conditionresult.cfexpression)>
											<cfset conditionresult.needsqueryParam = true>
			 								<cfset conditionresult.sqldatatype = "">
											<cfset conditionresult.message = "Can't deal with cf tag ">
										</cfif>



										<!--- query param needed so do the update --->
										<cfif conditionresult.needsQueryParam>

											<cfset result.itemsFound ++>

											<cfif conditionResult.isArgument and conditionResult.argumentdatatype is "" and (conditionResult.sqldatatype is not "CF_SQL_VARCHAR" ) and not form.updateFunctionArguments>
												<cfset conditionResult.autoReplaceOK = false>
											</cfif>

											<cfset conditionresult.checkbox = '<input type="checkbox" name="update" value="#updateIdentifier#">'>

											<cfif conditionresult.sqldatatype is not "">
												<cfif not useQueryParamFunction>
													<cfset queryParam = '<cf_queryparam '>
													<cfset commaOrSpace = " ">
												<cfelse>
													<cfset queryParam = '##application.com.security.queryparam('>
													<cfset commaOrSpace = ",">
												</cfif>
												<cfset queryParam = queryParam  & 'value="#conditionresult.cfexpression#"#commaOrSpace#CFSQLTYPE="#conditionresult.sqldatatype#" '>
												<cfif conditionresult.maxLength is not 0>
													<cfset queryParam = queryParam & '#commaOrSpace#maxlength="#conditionresult.maxLength#"'>
												</cfif>

												<cfif not useQueryParamFunction>
													<cfset queryParam = queryParam & '>'>
												<cfelse>
													<cfset queryParam = queryParam & ')##'>
												</cfif>

												<cfset conditionresult.wholereplacement = "#queryParam#">

												<cfif (structKeyExists(form,"update") and listfindnocase(form.update,updateIdentifier))
													or (form.autoReplace and conditionResult.autoReplaceOK)>
													<!--- do update --->
													<cfset replacementString = conditionresult.wholereplacement>

													<cfif not useQueryParamFunction>
														<!--- leave out comment
														<cfset replacementString = replacementString & "<!--- securityupdate #replace(conditionresult.string,"#thisMatch.beforeoperator##thisMatch.operator##thisMatch.afteroperator#","#thisMatch.beforeoperator#|#thisMatch.operator#|#thisMatch.afteroperator#")#---> ">
														--->
													</cfif>
													<cfset result.content = replace(result.content,thisMatch.string,thisMatch.openingCharacter & replacementString & thisMatch.closingCharacter)>  <!--- disrupt the comment with || around the operator so does not show up on new searches (although this process does remove comments) --->
													<cfset result.updated = true>
													<cfset conditionresult.updated = true>
													<cfset LogReplacement (filename=identifier,lineNumber = thisMatch.linenumber,oldstring =conditionresult.string, newString = conditionresult.wholereplacement )>
													<cfset result.itemsFixed ++>
												</cfif>

											<cfelse>

												<cfset LogIssue (filename=identifier,lineNumber = thisObject.linenumber,oldstring =  thisMatch.sqlVariable &  ' : ' & conditionresult.string, message = conditionresult.message)>


											</cfif>






											<cfsavecontent variable  = "tempOutput">
											<cfoutput>
			<!---
											<cfoutput>#htmleditformat(result.content)# <BR><BR></cfoutput>
			 --->
											<cfif not queryNameOutput >
												<cfset queryNameOutput = true>
												<tr><td colspan="3">&nbsp</td><td colspan="8">Query: <cfif structKeyExists(thisObject.attributes,"name")>#thisObject.attributes.name#<cfelse>Unnamed</cfif>.  Line:
												#thisObject.linenumber#</td></tr>
											</cfif>

											<tr>
												<td colspan="4">&nbsp</td>
												<td>
													<cfif structKeyExists(conditionresult,"checkbox")>
														#conditionresult.checkbox#
													</cfif>
												</td>

												<td>
													<strong>#htmleditformat(conditionresult.string)#<br /></strong>
												</td>

												<td>
													<cfif structKeyExists (conditionresult,"wholereplacement")>
														<strong>#htmleditformat(conditionresult.wholereplacement)#<br /></strong>
													</cfif>
												</td>
												<td>
													<cfif conditionresult.updated >
													Updated<br />
														<!--- <cfdump var="#thismatch#"><cfabort>  --->

													</cfif>
													<cfif conditionresult.sqldatatype is "" AND conditionresult.message is "">
															<strong>What DataType?</strong>
															<select name = "dataType_#updateIdentifier#">
																<option/>
																<option   value="cf_sql_integer">cf_sql_integer</option>
																<option   value="cf_sql_bitr">cf_sql_bit</option>
																<option value="cf_sql_timestamp">cf_sql_timestamp</option>
																<option value="cf_sql_numeric">cf_sql_numeric</option>
															</select>
													</cfif>
														<strong>#conditionresult.message#</strong>


												</td>

											</tr>
											</cfoutput>
											</cfsavecontent>
											<cfset result.output = result.output & tempOutput>
										</cfif>



										</cfif>



									</cfloop>



								</cfif>


						</cfloop>
					</cfloop>


	<cfreturn result>
</cffunction>


<cffunction name="processCFQueryLookForObjectsAsCFVariables">

	<cfargument name="thisObject">
	<cfargument name="inputOutput" >
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = thisObject.string, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var item = "">

	<cfif thisObject.tagName is "CFQUERY" and structKeyExists(thisObject.attributes,"dbtype")>
		<!--- don't worry about QoQ --->
		<cfreturn result>
	</cfif>

	<cfset queryContent = thisobject.innerhtml>
	<cfset queryContent = removeSafeStuffFromQuery (queryContent)>

	<cfif structKeyExists (inputOutput,"argumentArray")>
		<cfset argumentStruct = arrayToStruct(array = inputOutput.argumentArray, key="ATTRIBUTES",KEY2="NAME")>
	<cfelse>
		<cfset argumentStruct = {}>
	</cfif>


	<cfset var  findRemainingVariables = application.com.regExp.reFindAllOccurrences(reg_expression = regExps.CFVariable.regExp,string = queryContent, groupnames=regExps.CFVariable.groupnames,getlinenumbers = true,usejavaMatch=true)>
	<cfset var dangerousFieldList = "">
	<!--- Loop bacwards though variables ignoring ones which are safe --->
	<cfset var i = 0>

	<cfloop index="i" from = "#arrayLen(findRemainingVariables)#" to="1" step = "-1">
		<cfset item = findRemainingVariables[i]>
		<cfif isVariableSafe(item.innerText)>
			<!--- OK --->
		<cfelseif refindNoCase("preserveSingleQuotes",item.innerText)>
			<!--- OK --->
		<cfelseif isVariableSafelyTyped(item.innerText,argumentStruct)>	
			<!--- OK --->
		<cfelse>
			<cfset openFont = "">
			<cfset closeFont = "">
			<cfif refindNoCase("url\.|form\.|cgi\.", item.innerText)>
				<cfset openFont = "<font color='red'>">
				<cfset closeFont = "</font>">
			</cfif>
			<cfset dangerousFieldList &= openfont & item.innerText & closefont & "<BR>">
			<cfset result.content = application.com.regExp.reReplaceNoCaseStartingAt (result.content,"(##" & item.innerText & "##)",'<cf_queryObjectName value="\1">',"ONE",item.position)>
			<cfset result.updated = true>
		</cfif>
	</cfloop>

	<cfif dangerousFieldList is not "">
		<cfoutput>
		<br />
		======================<br />
		#identifier#<br />
		#thisObject.lineNumber#
		<pre>
			#queryContent#
		</pre>
		Dangerous Fields
		#dangerousFieldList#
		</cfoutput>
	</cfif>
	<cfreturn result>

</cffunction>


<cffunction name="isVariableSafelyTyped">
	<cfargument name = "cfvariable">
	<cfargument name = "argumentStruct">

	<cfset var result = false >
	<cfset cfvariable = replacenocase(cfvariable,"arguments.","","all")>

	<cfif structKeyExists(argumentStruct,cfvariable)>
		<cfif structKeyExists(argumentStruct[cfvariable],"TYPE")>
			<cfif listFind("numeric,date",argumentStruct[cfvariable].TYPE)>
				<cfset var result = true >
			</cfif>
		</cfif>
	</cfif>

	<cfreturn result>
</cffunction>


<cffunction name="guessDataType">
	<cfargument name = "sqlvariable">
	<cfargument name = "cfexpression">
	<cfargument name = "quote">

	<cfset var sqlVariableNoAlias = "">
	<cfset var doctoredCFVariable = "">
	<cfset var result = {sqldatatype  = "",cfexpression = cfexpression}>
	<cfset var listqualifyRegExp = 'listqualify\((.*?),("''"|'''''''')\)'>	<!--- actually "'"|'''' without the escaping --->
	<cfset var quotedvaluelistRegExp = 'quotedValueList\((.*?)\)'>



		<cfset doctoredCFVariable = replacenocase(replace(cfexpression,"##","","ALL"),"arguments.","","all")>
		<cfset SQLVariableNoAlias = rereplace(listlast(sqlvariable,"."),"[\[\]]","","all")> <!--- remove Alias AND square brackets--->

		<!--- Guess a dataType based on name of SQL column OR CF Variable--->
		<cfif quote is not "">
			<cfset result.sqldatatype = "CF_SQL_VARCHAR">
		<cfelseif getDataTypeFromKnownDataTypesStructure(cfexpression) is not "">
			<cfset result.sqldatatype = getDataTypeFromKnownDataTypesStructure(doctoredCFVariable)>
		<cfelseif getDataTypeFromKnownDataTypesStructure(SQLVariableNoAlias) is not "">
			<cfset result.sqldatatype = getDataTypeFromKnownDataTypesStructure(SQLVariableNoAlias)>
		<cfelseif refindnocase(listqualifyRegExp,cfexpression) >
			<cfset result.sqldatatype = "CF_SQL_VARCHAR">
			<cfset result.cfexpression = rereplacenocase(cfexpression,listqualifyRegExp,"\1")>
		<cfelseif refindNoCase(quotedvaluelistRegExp,cfexpression)>
			<cfset result.sqldatatype = "CF_SQL_VARCHAR">
			<cfset result.cfexpression = rereplacenocase(cfexpression,quotedvaluelistRegExp,"\1")>
		<cfelse>
			<cfset result.sqldatatype = getColumnDataTypeFromDB(SQLVariable)>
		</cfif>

		<cfreturn result>
</cffunction>

<cffunction name="getKnownDataTypeStructure">

	<cfif not structKeyExists (variables,"knownDataTypes")>

		<cfset path = application.com.regexp.getFileDetailsFromPath(gettemplatepath()).path & "/knownDataTypes.txt">

		<cffile action="read" file = #path# variable="dataTypes">

		<cfset dataTypes = rereplace (dataTypes,"\r\n",chr(10),"ALL")>
		<cfset linesArray = listToArray (dataTypes,chr(10))>
		<cfset variables.knownDataTypes = {}>

		<cfloop array = "#linesArray#" index="item">
			<cfset variables.knownDataTypes ["\A" & listfirst(item,":") & "\Z"] = listlast(item,":")>
		</cfloop>

	</cfif>

	<cfreturn variables.knownDataTypes>

</cffunction>

<!---
Note that this is generally for distinguishing between numeric and dates.  Varchar are distinguished by the quote
--->
<cffunction name = "getDataTypeFromKnownDataTypesStructure">
	<cfargument name="cfexpression">

	<cfset  var doctoredCFVariable = replacenocase(replace(cfexpression,"##","","ALL"),"arguments.","","all")>

	<cfset var dataType = "">

	<cfset var dataTypesStruct = getKnownDataTypeStructure()>

	<cfloop collection="#dataTypesStruct#" item="regExp">
		<cfif refindNocase(regExp,doctoredCFVariable)>
			<cfset dataType = dataTypesStruct[regExp]>
			<cfset dataType = "cf_sql_" & dataType>
			<cfbreak>
		</cfif>
	</cfloop>


	<cfreturn dataType>

</cffunction>


<cffunction name="runFunctionOnEveryTagInContent">
	<cfargument name="content">
	<cfargument name="tagname">
	<cfargument name="hasEndTag" default = "true">
	<cfargument name="functionPointer">
	<cfargument name="identifier">
	<cfargument name="inputOutput" default="#structNew()#">
	<cfargument name="startLineNumber" default="0">

	<cfset var result = {}>
	<cfset var thisResult = "">
	<cfset var thisObject = "">
	<cfset var objectArray = "">
	<cfset var useComplicatedRegExp = '' />
	<cfset var cfsetPositionArray = '' />
	<cfset var thisIdentifier = '' />
	<cfset var key = '' />

	<!--- any << and >> screw up the regExp to search for Tags
			So escape them now and un escape them at end
	--->
	<cfset content= ReplaceNoCase(content," << ","*&lt;&lt;*","ALL")>
	<cfset content= ReplaceNoCase(content," >> ","*&gt;&gt;*","ALL")>
	<cfset result = {updated = false,content = content,output="",itemsFound = 0,itemsfixed =0}>


	<cfset useComplicatedRegExp = true>

	<cfif listfindNoCase("/relay/screen/showscreenelement.cfm",replace(identifier,"\","/","ALL"))>
		<cfset useComplicatedRegExp = false>
	</cfif>

<cf_log file="wabreplace" text = "#request.Action# objectArray try">
<cfset findArgs = {inputstring = content,htmltag=listfirst(tagname),hasendtag=listfirst(hasEndTag),getLineNumbers = true,ignorecomments=true,useSophisticatedRegExp=useComplicatedRegExp}>
<cfset objectArray = application.com.regexp.findHTMLTagsInString(argumentCollection = findArgs)>
<cf_log file="wabreplace" text = "#request.Action# objectArray got">

	<cfset cfsetPositionArray = getCFSETPositionArray (content)>

	<cfloop array="#objectArray#" index="thisObject">
		<cfif application.com.regexp.isPositionInsidePositionArray(cfsetPositionArray,thisobject.position)>
			<!--- inside a cfset tag, don't process --->
		<cfelse>

			<cfif listfirst(tagname) is "cffunction">
				<cfset findArgs = {inputstring = thisObject.string,htmltag="cfargument",hasendtag=false,getLineNumbers = false,ignorecomments=true}>
				<cfset inputOutput.argumentArray = application.com.regexp.findHTMLTagsInString(argumentCollection = findArgs)>
	
				<cfset findArgs = {inputstring = thisObject.string,htmltag="cfparam",hasendtag=false,getLineNumbers = false,ignorecomments=true}>
				<cfset cfparamArray = application.com.regexp.findHTMLTagsInString(argumentCollection = findArgs)>	
				<cfset arrayAppend(inputOutput.argumentArray,cfparamArray,true)>
			</cfif>

			<cfset thisObject.lineNumber = thisObject.lineNumber + startLineNumber - IIF(startLineNumber IS NOT 0 ,1,0)>
			<cfif structKeyExists (thisObject.attributes,"name")>
				<cfset thisIdentifier = thisObject.attributes.name>
			<cfelse>
				<cfset thisIdentifier = "Unnamed #thisObject.length#" >
			</cfif>

			<cfset functionIdentifier = listappend(identifier,thisIdentifier,"|")>
			<cfif listLen(tagname) gt 1>
				<cfset thisResult = runFunctionOnEveryTagInContent(content = thisObject.string, functionPOinter = functionPOinter,tagname=listrest(tagname), hasendtag=listrest(hasEndTag),  inputOutput = inputOutput, identifier = functionIdentifier,startLineNumber = thisObject.lineNumber)>
			<cfelse>
				<cfset thisResult = functionPointer(thisObject = thisObject, inputOutput = inputOutput, identifier = functionIdentifier)>
			</cfif>

			<cfloop collection="#thisResult#" item="key">
				<cfif isNumeric (thisResult[key])>
					<cfset result[key] = result[key] + thisResult[key]>
				<cfelseif isBoolean (thisResult[key])>
					<cfset result[key] = result[key] OR thisResult[key]>
				<cfelseif isStruct (thisResult[key])>
					<cfparam name="result[key]" default = #structNew()#>
					<cfset  structAppend(result[key],thisResult[key])>
				<cfelseif key is not "content">
					<cfset result[key] = result[key] & thisResult[key]>
				</cfif>
			</cfloop>

			<cfif thisResult.updated is true>
				<cfset oldContent = result.content>
				<cfset result.content = replace(result.content,thisObject.string,thisResult.content)>

				<cfif oldContent is result.content>
					<cfoutput>
							<font color="red">Update Failed</font>: #functionIdentifier#
					<pre>
						#thisResult.content#
					</pre>
					</cfoutput>

				</cfif>

<!---
				<cfoutput><pre>#htmleditformat(result.content)#<br /></pre></cfoutput>
				<cfoutput><pre>#htmleditformat(thisObject.string)#<br /></pre></cfoutput>
				<cfoutput><pre>#thisResult.content#<br /></pre></cfoutput>
 --->

				<cfset result.updated = true>
			</cfif>

		</cfif>


	</cfloop>

	<!--- unescape the << and >> --->
	<cfset result.content= ReplaceNoCase(result.content,"*&lt;&lt;*"," << ","ALL")>
	<cfset result.content= ReplaceNoCase(result.content,"*&gt;&gt;*"," >> ","ALL")>


	<cfreturn result>

</cffunction>


<cffunction name="iterateFileQueryRunningActions">
	<cfargument name="fileQuery">
	<cfargument name="inputOutput" default="#structNew()#">
	<cfargument name="ActionList">
	<cfargument name="maxFiles" default="0">
	<!---
	functionPointer = functionDefinition.functionPointer, tagName=functionDefinition.tagname, hasendTag=functionDefinition.hasEndTag, maxFiles = maxFiles,processFunctionsSeparately = functionDefinition.processFunctionsSeparately
	<cfargument name="functionPointer">
	<cfargument name="maxFiles">
	<cfargument name="processFunctionsSeparately"  default="false" >
	<cfargument name="tagName">
	<cfargument name="hasEndTag">
	--->

	<cfset var fileCounter = 0 >
	<cfset var result = {output = ""}>
	<cfset var fileResult = "">
	<cfset var relativeFilePath = "">
	<cfset var fileContent = "">
	<cfset var functionPointer = "">
	<cfset var fileNameOutput =false>
	<cfset var thisFileProcessed = '' />
	<cfset var functionDef = '' />
	<cfset var newFilePath = '' />
	<cfset var parsepath = '' />
	<cfset var resultsDir = '' />
	<cfset var actionID = '' />
	<cfset var log = '' />
	<cfset var cffileAttributes = {}/>


	<cfloop query = "fileQuery">


		<cfset fileNameOutput =false>

		<cfset analysePathResult = analysePath (filepath)>

		<cfset relativeFilePath = analysePathResult.relativePath>

		<cfset thisFileProcessed = false>

			<cfset readFilePath = filepath>
			<cfset writeFilePath = readFilePath>

				<cfif inSituReplace is 0>
					<cfset parsepath = application.com.regexp.getFileDetailsFromPath(relativeFilePath)>

					<cfset resultsDir = analysePathResult.rootpath & "/codeCopResults/" & parsepath.path>
					<cfset writeFilePath = "#resultsDir#/#parsepath.fullfilename#">
				</cfif>

		<cfif fileExists(readFilePath)>
			<cfset cffileAttributes = {action = "read", file="#readFilePath#", variable="fileContent"}>

			<cfif listLast(readFilePath,".") eq "xml">
				<cfset cffileAttributes.charset="utf-8">
			</cfif>
			<cffile attributeCollection=#cffileAttributes#>

			<!--- get all the cfparams which have patterns or types --->
				<cfset findArgs = {inputstring = fileContent,htmltag="cfparam",hasendtag=false,getLineNumbers = false,ignorecomments=true}>
				<cfset cfparamArray = application.com.regexp.findHTMLTagsInString(argumentCollection = findArgs)>

				<cfset cfparamStruct = {}>
				<cfloop array=#cfparamArray# index="cfparam">
					<cfset cfparamStruct[cfparam.attributes.name] = cfparam.attributes>
					<cfset cfparamStruct[cfparam.attributes.name].position = cfparam.position>
				</cfloop>
				<cfset inputOutput.cfparamStruct = cfparamStruct>

		</cfif>

		<cfloop list="#actionList#" index="actionID">
			<cfset functionDef = functions[actionID]>
			<cfset request.action  = functionDef.name>

			<cfparam name="functionDef.fileFilter" default="\.cf.\Z">
			<cfparam name="functionDef.excludefileFilter" default="\A\Z">

			<CFSET testFileName = refindnocase(functionDef.fileFilter,relativeFilePath)>
			<CFSET testExcludeFileName = refindnocase(functionDef.excludeFileFilter,relativeFilePath)>

			<cfset skip = skipfile(relativeFilePath,request.Action)>

			<cfif testFileName is not 0 AND testExcludeFileName is 0 AND fileExists(readFilePath) and (form.processingMode is "ReprocessAll" or doesFileNeedProcessing(relativeFilePath,request.Action))>
				<cfif not skip.skip>
					<cfset thisFileProcessed = true>

					<cf_log file="wabreplace" text = "#request.Action# #relativeFilePath#">

					<cfset functionPointer = functionDef.functionPointer>
					<cfif functionDef.tagname is "">
						<cfset fileResult = functionPointer(content = filecontent, identifier = relativeFilePath)>
					<cfelseif functionDef.processFunctionsSeparately and filepath contains ".cfc">
						<cfset fileResult = runFunctionOnEveryTagInContent(content = filecontent,tagname=listappend("cffunction",functionDef.tagname), hasEndTag = listappend("true",functionDef.hasendtag), functionPointer = functionDef.functionPointer , identifier = relativeFilePath, inputOutput = inputOutput)>
					<cfelse>
						<cfset fileResult = runFunctionOnEveryTagInContent(content = filecontent,tagname=functionDef.tagname, hasEndTag = functionDef.hasendtag,functionPointer = functionDef.functionPointer ,identifier=relativeFilePath,inputOutput = inputOutput)>
					</cfif>

					<cfif fileResult.output is not "" or fileResult.updated>
						<cfif not fileNameOutput>
							<cfset fileNameOutput = true>
							<cfset result.output = result.output  & '<tr><td colspan="10" >#relativeFilePath#</td></tr>' >
						</cfif>

						<cfset result.output = result.output  &  fileResult.output>
					</cfif>
					<cfif fileResult.updated>
						<cfif inSituReplace is 0 and not directoryExists(resultsDir)>
							<cfdirectory action="create" directory="#resultsDir#">
						</cfif>

						<cfset cffileAttributes = {action="write", output="#fileResult.Content#", file="#writeFilePath#", addNewLine=false}>
						<cfif listLast(writeFilePath,".") eq "xml">
							<cfset cffileAttributes.charset="utf-8">
						</cfif>

						<cffile attributeCollection=#cffileAttributes#>

						<cfset result.output = result.output  & '<tr><td colspan="10">File Updated on Disk</td></tr>' >
						<cfset fileContent = fileResult.Content>  <!--- ready for next loop --->
					</cfif>


					<cfquery name="log" datasource = "#application.sitedatasource#">
					insert into codeCop.dbo._securityProject_File
					(fileName ,action,liveRun,itemsFound,itemsFixed,created)
					values ('#relativeFilePath#','#request.action#',#form.inSituReplace#,#fileResult.itemsFound#,#fileResult.itemsfixed#,getdate())

					<cfif fileResult.itemsFound is 0>
						DELETE FROM codeCop.dbo._securityProject_ISSUE WHERE FILENAME  like  <cf_queryparam value="#relativeFilePath#%" CFSQLTYPE="CF_SQL_VARCHAR" > AND ACTION =  <cf_queryparam value="#request.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelseif structKeyExists (fileResult,"issueLineNumbers") and structCount(fileResult.issueLineNumbers) is not 0>
						DELETE FROM codeCop.dbo._securityProject_ISSUE WHERE FILENAME  like  <cf_queryparam value="#relativeFilePath#%" CFSQLTYPE="CF_SQL_VARCHAR" > AND ACTION =  <cf_queryparam value="#request.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and lineNumber  not in ( <cf_queryparam value="#structKeyList(fileResult.issueLineNumbers)#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
					</cfif>


					</cfquery>

					<cfif maxFiles is not 0 and fileCounter GTE maxFiles>
						<cfbreak>
					</cfif>

				<cfelse>
					<cfset result.output = result.output  & '<tr><td colspan="10" >#relativeFilePath#  -   SKIPPED #skip.reason# </td></tr>' >
				</cfif>
			<cfelse>

				<cfif NOT (testFileName is not 0)>
					<cfset debugreason = "filtered out by filename/extension">
				<cfelseif NOT (testExcludeFileName is 0) >
					<cfset debugreason = "excluded from this process">
				<cfelseif NOT  (fileExists(readFilePath)) >
					<cfset debugreason = "does not exist">
				<cfelseif not (form.processingMode is "ReprocessAll" or doesFileNeedProcessing(relativeFilePath,request.Action)) >
					<cfset debugreason = "Already processed">
				</cfif>
				<!---
				<cfset result.output = result.output  & '<tr><td colspan="10" >#relativeFilePath#  -  not processed  #debugReason# </td></tr>' >
				--->

			</cfif>
		</cfloop>

		<cfif thisFileProcessed>
			<cfset filecounter ++>
		</cfif>

	</cfloop>

		<cfoutput><BR>#filecounter# files processed</cfoutput>
	<cfreturn result>

</cffunction>

<cffunction name="skipFile">
	<cfargument name="filePath">
	<cfargument name="action">

	<cfset var testskipFile = "">
	<cfset var result = {skip = false, reason = ""}>

	<cfquery name="testskipFile" datasource = "#application.sitedatasource#">
	select * from codeCop.dbo._securityProject_FileDoNotProcess
	where  '#filepath#' like filename
	and active = 1
	and (action =  <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" > or action = '*' or action is null)
	</cfquery>

	<cfif testskipFile.recordCount>
		<cfset result.skip = true>
		<cfset result.reason = testskipFile.comment>
	</cfif>

	<cfreturn result>

</cffunction>


<cffunction name="doesFileNeedProcessing">
	<cfargument name="filepath">
	<cfargument name="action">

	<cfset var result = true>
	<cfset var testFile = "">


	<cfif filePath contains "devTools" and not form.DIRECTORYPATH contains "devTools" >
		<cfreturn false>
	</cfif>

	<cfquery name="testFile" datasource = "#application.sitedatasource#" debug="false">
	select top 1 * from codeCop.dbo._securityProject_File
	where action =  <cf_queryparam value="#action#" CFSQLTYPE="CF_SQL_VARCHAR" >
	and filename =  <cf_queryparam value="#filepath#" CFSQLTYPE="CF_SQL_VARCHAR" >
	order by created desc
	</cfquery>



	<cfif form.processingMode is not "JustUnProcessed">
		<cfif testFile.recordcount is 1 and testFile.itemsFound IS 0 >
			<cfset result = false>
		</cfif>
	<cfelse>
		<!--- ignore part processed, so don't do if any entry in db --->
		<cfif testFile.recordcount>
			<cfset result = false>
		</cfif>
	</cfif>

	<cfreturn result>
</cffunction>

<cffunction name="expandPath_">
	<cfargument name="directorypath">

	<cfset var expandedpath = "">
	<cfif mid(directorypath,2,10) is "customtags">
		<cfset expandedPath = application.paths.core & directoryPath>
	<cfelse>
		<cfset expandedPath = expandPath(directoryPath)>
	</cfif>

	<cfreturn expandedPath>
</cffunction>

<cffunction name="getPartProcessedFiles">
	<cfargument name="action">
	<cfargument name="filePath">
	<cfargument name="maxFiles">

<cfset var thisAction = '' />
<cfset var getfiles = '' />

	<cfquery name="getfiles" datasource = "#application.sitedatasource#" >
		select
			<cfif structKeyExists (arguments,"maxFiles")>TOP #maxFiles#</cfif>
			f.filename as filepath
		from codeCop.dbo._securityProject_file f
		inner join
			(
			select filename, action, MAX(created) created
			from codeCop.dbo._securityProject_file
			where action in (
			<cfloop index="thisAction" list="#arguments.action#">
				'#functions[thisaction].name#',
			</cfloop>
			'dummy!')
			group by filename, action
			) as latest
		on latest.fileName = f.fileName and latest.created = f.created and latest.action = f.action
		where itemsfound <> itemsfixed
	</cfquery>

	<cfloop query="getFiles">
		<cfset querysetcell(getFiles,"filepath",expandpath_(filepath),currentrow)>
	</cfloop>
	<cfreturn getfiles>

</cffunction>

<cffunction name="arrayToStruct">
 <cfargument name="array">
 <cfargument name="key">
 <cfargument name="key2">

	<cfset var result = {} />
	<cfset var thisItem = '' />

	<cfloop array="#array#" index="thisItem">
		<cfset result[thisItem[key][KEY2]] = thisItem[KEY]>
	</cfloop>
	<cfreturn result>
</cffunction>


<cffunction name="logPreserveSingleQuotes">
	 <cfargument name="identifier">
	 <cfargument name="variablename">
	 <cfargument name="linenumber">

	 	<Cfset var logAndGet = '' />>
	 		<cfset var find = application.com.regexp.refindAlloccurrences(reg_expression = "##preserveSingleQuotes\(.*?\)##",string = variablename,usejavaMatch=true)>
	 		<cfset var variablename_ = find[1].string>

			<cfquery name="logAndGet" datasource = "#application.sitedatasource#" debug="false">
			if not exists (
			select 1 from codeCop.dbo._securityProject_PreserveSingleQuotes
			where identifier =  <cf_queryparam value="#identifier#" CFSQLTYPE="CF_SQL_VARCHAR" > and
				variableinquery =  <cf_queryparam value="#variablename_#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and linenumber  =  <cf_queryparam value="#linenumber#" CFSQLTYPE="CF_SQL_Integer" >
			) BEGIN

			insert into
			codeCop.dbo._securityProject_PreserveSingleQuotes
			(identifier ,	variableinquery ,linenumber,itemsfixed, itemsfound )
			values(	'#identifier#' ,'#variablename_#',#linenumber#,null,null)
			END

			select
				replaceVariableInQuery as replacewith ,
				case when itemsfound is not null and itemsfound = itemsfixed then 1 else 0 end as done

			from codeCop.dbo._securityProject_PreserveSingleQuotes
			where identifier =  <cf_queryparam value="#identifier#" CFSQLTYPE="CF_SQL_VARCHAR" > and
				variableinquery =  <cf_queryparam value="#variablename_#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and linenumber =  <cf_queryparam value="#linenumber#" CFSQLTYPE="CF_SQL_Integer" >

			</cfquery>

		<cfreturn logAndGet>

	<cfreturn result>
</cffunction>


<cffunction name="getCFSETPositionArray">
	 <cfargument name="string">
	<cfset var regExp = "<cfset\s*\S*\s*=\s*(['""]).*?(\1)\s*>">

	<cfset var result = application.com.regexp.refindAllOccurrences (regexp, string)>

	<cfreturn result>
</cffunction>


<cffunction name="getColumnDataTypeFromDB">
	 <cfargument name="SQLExpression">


<cfset var columnname = '' />
<cfset var sqltable = '' />
<cfset var getColumnDataType = '' />
<cfset var getcfsqlType = '' />
<cfset var getSimpleType = '' />
	<cfset var result = "">
	<cfset SQLExpression = rereplace(SQLExpression,"[\[\],\(\)]","","all")>
	<cfset columnname = listlast(SQLExpression,".")> <!--- remove Alias AND square brackets--->
	<cfset sqltable = "">
	<cfif listlen (SQLExpression,".") is 2>
		<cfset sqltable = listFirst (SQLExpression,".")>
	</cfif>


		<cfif sqlTable is not "">

			<cfquery name="getColumnDataType" datasource = "#application.sitedatasource#">
			select distinct data_type, '' as simpleType, '' as cfsqltype
			from information_schema.columns
			where column_name =  <cf_queryparam value="#columnname#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and table_name =  <cf_queryparam value="#sqltable#" CFSQLTYPE="CF_SQL_VARCHAR" > and table_name not like 'dataload%'
			</cfquery>

		</cfif>

		<cfif sqlTable is "" or getColumnDataType.recordCOunt is 0>

			<cfquery name="getColumnDataType" datasource = "#application.sitedatasource#">
			select distinct data_type, '' as simpleType, '' as cfsqltype
			from information_schema.columns
			where column_name =  <cf_queryparam value="#columnname#" CFSQLTYPE="CF_SQL_VARCHAR" > and table_name not like 'dataload%'
			</cfquery>

		</cfif>

		<cfloop query="getColumnDataType">
			<cfset querysetcell(getColumnDataType,'simpleType',application.dataTypeLookup[getColumnDataType.data_Type].simple,currentrow)>
			<cfset querysetcell(getColumnDataType,'cfsqlType',application.dataTypeLookup[getColumnDataType.data_Type].cfsql,currentrow)>
		</cfloop>

		<cfquery dbtype="query" name="getcfsqlType" >
			select distinct cfsqlType from getColumnDataType
		</cfquery>

		<cfif getcfsqlType.recordcount is 1>
			<cfset result = getcfsqlType.cfsqltype>
		<cfelse>
			<cfquery dbtype="query" name="getSimpleType" >
				select distinct simpleType from getColumnDataType
			</cfquery>

			<cfif getSimpleType.recordcount is 1>
				<!--- all the same simple type --->
				<cfswitch expression = #getSimpleType.simpletype#>
					<cfcase value = "numeric">
						<cfset result = "cf_sql_float">
					</cfcase>
					<cfcase value = "text">
						<cfset result = "cf_sql_varchar">
					</cfcase>
					<cfcase value = "date">
						<cfset result = "cf_sql_timestamp">
					</cfcase>

				</cfswitch>
			</cfif>

		</cfif>




	<cfreturn result>

</cffunction>


<cffunction name = "isVariableSafe">
	<cfargument name="cfexpression">
	<cfargument name="Type" default = "" hint="html,script">

	<cfset var result = false>
	<cfset var doctoredCFVariable = replacenocase(replace(cfexpression,"##","","ALL"),"arguments.","","all")>
	<cfset var regExp = "">

	<cfset var safeVariablesStructure = getSafeVariableStructure()>

	<cfloop collection="#safeVariablesStructure#" item="regExp">
		<cfif refindNocase(regExp,doctoredCFVariable)>
			<cfset result = true>
			<cfbreak>
		</cfif>
	</cfloop>

	<cfif type is "html">
		<cfif refindNocase("htmlEditFormat",doctoredCFVariable)>
			<cfset result = true>
		</cfif>
	</cfif>

	<cfreturn result>

</cffunction>

<cffunction name="getSafeVariableStructure">

	<cfargument name="extraItems" default = "">

	<cfset var path = "">
	<cfset var regExp = "">
	<cfset var result = extraItems>

	<cfif not structKeyExists (variables,"SafeVariablesStructure")>
		<cfset path = application.com.regexp.getFileDetailsFromPath(gettemplatepath()).path & "/safeVariablenames.txt">

		<cffile action="read" file = #path# variable="safeVariables">
		<cfset safeVariables = rereplace(safeVariables,"\r\n",chr(10),"ALL")>

		<cfset variables.SafeVariablesStructure = {}>
		<cfset linesArray = listToArray (safeVariables,chr(10))>

		<cfloop array = "#linesArray#" index="item">
			<cfset variables.SafeVariablesStructure ["\A" & item & "\Z"] ="">
		</cfloop>


	</cfif>


	<cfreturn variables.SafeVariablesStructure>

</cffunction>

<cffunction name="LogReplacement">
		<cfargument name="filename">
		<cfargument name="lineNumber">
		<cfargument name="oldString">
		<cfargument name="newString">

		<cfset var log = "">

		<cfquery name="log" datasource = "#application.sitedatasource#">
		insert into codeCop.dbo._securityProject_Replacement
		(fileName ,LineNumber,action,liveRun,OriginalCode,NewCode,created)
		values ('#arguments.filename#',#arguments.lineNumber#,'#request.action#',#form.inSituReplace#,'#left(arguments.oldString,500)#','#left(arguments.newString,500)#',getdate())

		delete from codeCop.dbo._securityProject_Issue
		where filename =  <cf_queryparam value="#listfirst(arguments.filename,"|")#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and linenumber =  <cf_queryparam value="#arguments.lineNumber#" CFSQLTYPE="CF_SQL_Integer" >
		and action  =  <cf_queryparam value="#request.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn>

</cffunction>


<cffunction name="LogIssue">
		<cfargument name="filename">
		<cfargument name="lineNumber">
		<cfargument name="oldString">
		<cfargument name="message">

		<cfset var log = "">

		<cfquery name="log" datasource = "#application.sitedatasource#">
		<cfset originalCode = left(arguments.oldString,500)>
		if not exists (
			select 1 from codeCop.dbo._securityProject_Issue
			where filename =  <cf_queryparam value="#arguments.filename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and linenumber =  <cf_queryparam value="#arguments.lineNumber#" CFSQLTYPE="CF_SQL_Integer" >
			and action =  <cf_queryparam value="#request.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and OriginalCode  =  <cf_queryparam value="#originalCode#" CFSQLTYPE="CF_SQL_VARCHAR" > )
		BEGIN

			insert into codeCop.dbo._securityProject_Issue
			(fileName ,LineNumber,action,liveRun,OriginalCode,Message,created)
			values ('#arguments.filename#',#arguments.lineNumber#,'#request.action#',#form.inSituReplace#,'#originalCode#','#arguments.message#',getdate())

		END
		</cfquery>

		<cfreturn>

</cffunction>

<cffunction name="RemoveIssues">
		<cfargument name="filename">

		<cfquery name="log" datasource = "#application.sitedatasource#">
			delete
			from codeCop.dbo._securityProject_Issue
			where filename =  <cf_queryparam value="#arguments.filename#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and action  =  <cf_queryparam value="#request.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn>

</cffunction>

<cffunction name="getCase">
		<cfargument name="string">

		<cfset var firstChar = left(string,1)>
		<cfif compare (firstChar,uCase(firstChar)) is 0>
			<cfset result = "upper">
		<cfelse>
			<cfset result = "lower">
		</cfif>

		<cfreturn result>
</cffunction>

<cffunction name="setCase">
		<cfargument name="string">
		<cfargument name="case" default="upper">

		<cfset var result = string>

		<cfif arguments.case is "upper">
			<cfset result = ucase(string)>
		<cfelse>
			<cfset result = lcase(string)>
		</cfif>

		<cfreturn result>
</cffunction>

<cffunction name="findPositionOfBeginningAndEndOfLine">
		<cfargument name="string">
		<cfargument name="currentPosition">

		<cfset var result = {}>

		<!--- Note - not checked for being 1 character out yet! --->
		<cfset result.end = refind ("(\n|\Z)",string,currentPosition)>
		<cfset result.begin = len(string) - refind ("(\n|\Z)",reverse(string),len(string) - currentPosition) + 2>

		<cfreturn result>
</cffunction>

<cffunction name="getCurrentLine">
		<cfargument name="string">
		<cfargument name="currentPosition">
		<!--- Note - not checked for being 1 character out yet! --->
		<cfset var result = findPositionOfBeginningAndEndOfLine(argumentCollection = arguments)>
		<cfset result.positionOnline = currentPosition - result.begin>
		<cfset result.line = mid(string,result.begin,result.end - result.begin)>

		<cfreturn result>
</cffunction>


<cffunction name="addCopyRight">
	<cfargument name="content">
	<cfargument name="identifier" >

	<cfset var result = {updated = false,content = arguments.content, output = "",itemsfound = 0,itemsFixed = 0}>
	<cfset var local = {}>
	<cfset var testResult = "">
	<cfset var fileExtension = listLast(arguments.identifier,".")>
	<cfset var startComment = "">
	<cfset var endComment = "">
	<cfset var copyRightString = "#chr(169)#Relayware. All Rights Reserved">
	<cfset var thisYear = dateFormat(now(),"yyyy")>
	<cfset var lastYear = dateFormat(dateAdd("yyyy",-1,now()),"yyyy")>
	<cfset var endCommentRegExp = "">
	<cfset var startCommentRegExp = "">
	<cfset var processingInstructionRegExp = "<\?.*?\?>">
	<cfset var processingInstruction = "">

	<cfif listFindNoCase("cfc,cfm",fileExtension)>
		<cfset startComment = "<!---">
		<cfset endComment = "--->">
	<cfelseif listFindNoCase("xml,html,htm",fileExtension)>
		<cfset startComment = "<!--">
		<cfset endComment = "-->">
	<cfelseif listFindNoCase("js,css",fileExtension)>
		<cfset startComment = "/*">
		<cfset endComment = "*/">
	</cfif>

	<cfset startCommentRegExp = replace(replace(startComment,"/","\/"),"*","\*")>
	<cfset endCommentRegExp = replace(replace(endComment,"/","\/"),"*","\*")>

	<cfif thisYear eq 2013>
		<cfset thisYear = 2014>
		<cfset lastYear = 2013>
	</cfif>

	<!--- has not yet been set... set it --->
	<cfif refindNocase(copyRightString,content) IS 0>
		<!--- if xml document, we want to put the comment after the processing instruction, which is enclosed within <? and ?> --->
		<cfif fileExtension eq "xml" and reFindNoCase(processingInstructionRegExp&"\r\n",content)>
			<cfset processingInstruction = application.com.regExp.refindSingleOccurrence(reg_expression=processingInstructionRegExp&"\r\n",string=result.content)>
			<cfset result.content = reReplaceNoCase(result.content,processingInstructionRegExp&"\r\n",processingInstruction[1].string&startComment& " #copyRightString# #thisYear# "&endComment & chr(13)&chr(10))>
		<cfelse>
			<cfset result.content = startComment& " #copyRightString# #thisYear# "&endComment & chr(13)&chr(10)& result.content>
		</cfif>
		<cfset result.updated = true>

	<!--- it has been found, but the year may be out of date --->
	<cfelseif refindNocase(startCommentRegExp & ".*?#copyRightString#\s*#lastYear#.*?" & endCommentRegExp,content)>
		<cfset result.content = reReplaceNoCase(result.content,"#copyRightString#\s*#lastYear#","#copyRightString# #thisYear#")>
		<cfset result.updated = true>
	</cfif>

	<cfreturn result>
</cffunction>

<cffunction name="convertAbsolutePathToRelayPath" output=false>
	<cfargument name="filepath">
	<cfset var result = filepath>
		<!--- read file file--->
		<cfset result = replacenocase(result,application.paths.core,'')>
		<cfif structKeyexists (application.paths,"test")>
			<cfset result = replacenocase(result,application.paths.test,'\test')>
		</cfif>
		<cfset result = replacenocase(result,application.paths.code,'\code')>

		<cfif refind ("\A[A-Z]:",result)>
			<!--- still an absolute path, must be outside of current relay directory --->
			<cfset result = reReplaceNoCase (result,".*?((\\relay)|(\\code))","\1")>
		</cfif>

	<cfreturn result>
</cffunction>

<cffunction name="analysePath" output=false>
	<cfargument name="filepath">

	<cfset var result = {relativePath="",rootPath = ""}>

	<cfset result.relativePath = reReplaceNoCase (filepath,".*?((\\relay)|(\\code)|(\\test)|(\\customtags))\\","\1\")>
	<cfset result.rootPath = replace (filePath,result.relativePath,"")>

	<cfreturn result>
</cffunction>


<cffunction name="reFindAllOccurrencesArrayToList">
	<cfargument name="array">
	<cfargument name="key" default ="string">
	<cfargument name="delimiter" default =",">

	<cfset var result = "">

	<cfloop array="#array#" index="item">
		<cfif result is not "">
			<cfset result &= delimiter>
		</cfif>
		<cfset result &= htmleditformat(item[key])>
	</cfloop>


	<cfreturn result>
</cffunction>


<cffunction name="makeSafeUpdateIdentifier">
	<cfargument name="string">

	<cfreturn rereplacenocase(string,"[^A-Za-z0-9_]","_","all")>
</cffunction>
