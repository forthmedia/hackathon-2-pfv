<!--- �Relayware. All Rights Reserved 2014 --->

<!---
An attempt to scan our code base looking for files which are never called from anywhere

 --->

<cf_nodebug>

<!--- Initialise --->
<cfquery name="INITIALISE" datasource="#application.sitedatasource#">
IF NOT EXISTS (SELECT 1 FROM codeCop.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AllFiles')
BEGIN

	CREATE TABLE codeCop.[dbo].[allFiles](
		[fileID] int identity (1,1),
		[filename] [varchar](250) NULL,
		[scanned] [bit] NOT NULL default 0,
		[knownGood] [bit] NOT NULL default 0
	) ON [PRIMARY]


END


IF NOT EXISTS (SELECT 1 FROM codeCop.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'fileLink')
BEGIN

	CREATE TABLE codeCop.[dbo].[fileLink](
		fromFileID int,
		toFileID int
						
	) ON [PRIMARY]



END


</cfquery>





<!--- 
	On first run
	put list of all files into db
	columns filename, called, scanned
 --->
<cfquery name="check" datasource="#application.sitedatasource#">
select count(*) as theCount from codeCop.dbo.allFiles
</cfquery>
<cfif check.theCount is 0>
	<cfdirectory action="list" name="dir" directory="#application.paths.core#" recurse="yes" filter="*.cf*|*.xml">
	<cfloop query="dir">
		<cfif not directory contains ".svn" and not directory contains "_support">

			<cfquery name="insert" datasource="#application.sitedatasource#">
			insert into codeCop.dbo.allFiles
			(filename)
			values (<cf_queryparam value="#convertAbsolutePathToRelayPath(directory & "\" & name)#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</cfquery>

		</cfif>

	</cfloop>

		<cfquery >
		delete from allfiles where filename like '%codecop%'
		delete from allfiles where filename like '%testbox%'
		delete from allfiles where filename like '%examples%'
		delete from allfiles where filename like '%wirebox%'
		</cfquery>
</cfif>



<!---
Force the "Head" files to be scanned.
(eg index.cfm,menuitems.xml)
 --->

		<cfquery name="update" datasource="#application.sitedatasource#">
		update codeCop.dbo.allFiles
		set knownGood =1
		where filename  in (
			'\relay\index.cfm'
			,'\relayware\XML\RelayMenuItems.xml'
			,'\relayware\XML\RelayTags.xml'
			,'\Relay\WebServices\relaywebservices.cfc'
			,'\Relay\WebServices\relayws.cfc'
			,'\relayware\XML\configToolsList.xml'
			,'\Relay\amIUp.cfm'
			,'\Relay\application.cfc'
			,'\Relay\applicationMAIN.cfc'
			,'\Relay\debug.cfm'
			,'\relay\homepage\homepage.cfm'

			
			)
		</cfquery>



<!---
TODOTODO
get list of file called from database

select * from screendefinition where fieldtextid like '%cfm%'
select * from processactions where jumpname like '%cfm%'
select * from phraseText from phrases where phraseText like '%cfm%'
--->


<!---
get query of all files which are called but not scanned files
 --->
		<cfquery name="filesToScan" datasource="#application.sitedatasource#">
		select *
		from codeCop.dbo.allFiles
		where scanned = 0
			and autoadded = 0
--			and filename > (select max(filename) from codeCop.dbo.allFiles where scanned = 1)
		order by filename	
		</cfquery>


	<cfloop query="filesToScan">


		<!---  --->
		<cfif filename contains "contentFromDB" >

		</cfif>

		<cfset currentDir = reverse(listrest(reverse(filename),"\"))>

		<!--- read file, strip comments --->
		<cfset absPath = convertRelayPathToAbsolutePath(filename)>
		<cffile action="read" file=#abspath# variable="content">

		<cfset content = application.com.regexp.removeallcomments(content)>


		<!--- RegExp look for .cfm  and cf_--->
		<cfset args = {reg_expression = "[^a-zA-Z0-9_##.\/\\-]([a-zA-Z0-9_##.\/\\-]*?\.cfm)", string = content}>
		<cfset linksArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

		<cfset args = {reg_expression = "<CF_(.*?)[^a-zA-Z0-9_]", string = content}>
		<cfset customTagsArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

		<cfset args = {reg_expression = "application\.com\.([a-zA-Z0-9_]*?)\.", string = content}>
		<cfset cfcArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

		<cfset args = {reg_expression = "createobject\(""component"",""(.*?)""\)", string = content}>
		<cfset createobjectArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

		<cfset args = {reg_expression = "webservicename=([A-Za-x0-9_]*)", string = content}>
		<cfset callWebserviceArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

		<cfset args = {reg_expression = "[^a-zA-Z0-9_##.\/\\-]([a-zA-Z0-9_##.\/\\-]*?\.js)""", string = content}>
		<cfset jsArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>


		<cfset filesFound = []>

		<cfloop array="#linksArray#" index="link">
			<cfset linkFound = link[2]>
			<cfset originalLinkFound = linkFound>
			<cfset linkFound = replace(linkFound,"/","\","ALL")>
			<cfset linkFound = replace(linkFound,"\\","\","ALL")>

			<cfset linkFound = rereplaceNoCase(linkFound,"##application\.paths\.(.*?)##","\\\1")>
			<cfset linkFound = rereplaceNoCase(linkFound,"##(thisDir|attributes.thisdir)##",currentDir)>
			<cfset linkFound = rereplaceNoCase(linkFound,"##(request.currentSite.httpProtocol|cgi.http_host|request.currentsite.domainandroot)##","","ALL")>



			<cfif refindNoCase("\A\\(code|relay|content)",linkFound)>
				<!--- already in for \relay, \code, \content
				--->
				<cfset fileFound = linkFound>

			<cfelseif left(linkFound,1) is "\">
				<!--- absolute, so relative to relay --->
				<cfset fileFound = "\relay" & linkFound>
			<cfelseif filename is "\relayware\XML\RelayMenuItemsv3.xml">
				<!--- special case --->
				<cfset fileFound = "\relay\" & linkFound>
			<cfelseif filename is "\relayware\XML\RelayTags.xml">
				<!--- special case --->
				<cfset fileFound = "\relay\relayTagTemplates\" & linkFound>

			<cfelse>
				<!--- relative to current --->
				<cfset fileFound = currentDir & "\"&linkFound>
			</cfif>

			<cfset fileFound = rereplace(fileFound,"\\[^\\]*?\\\.\.","")> <!--- get rid of \xxxx\.. --->
			<cfset fileFound = rereplace(fileFound,"\\[^\\]*?\\\.\.","")> <!--- get rid of \xxxx\.. --->

			<cfset arrayAppend (filesFound,fileFound)>

		</cfloop>


		<cfloop array="#customTagsArray#" index="customTag">
			<cfset customTagFound = customTag[2]>

			<cfset fileFound = '#currentDir#\#customTagFound#.cfm'>
			<!--- check for file in current directory --->
			<cfquery name="check" datasource="#application.sitedatasource#">
			select *
			from codeCop.dbo.allFiles
			where filename   like  <cf_queryparam value="#fileFound#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfif not check.recordcount>
				<cfset fileFound = "\customtags%\#customTagFound#.cfm">
				<cfquery name="check" datasource="#application.sitedatasource#">
				select *
				from codeCop.dbo.allFiles
				where filename   like  <cf_queryparam value="#fileFound#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>
	
				<cfif check.recordcount>
					<cfset fileFound = check.filename>				
				</cfif>

			</cfif>

			<cfset arrayAppend (filesFound,fileFound)>

		</cfloop>


		<cfloop array="#cfcArray#" index="link">
			<cfset cfcFound = link[2]>

			<cfset fileFound = "\relay\com\#cfcfound#.cfc">

			<cfset arrayAppend (filesFound,fileFound)>

		</cfloop>

		<cfloop array="#callWebserviceArray#" index="link">
			<cfset cfcFound = link[2]>

			<cfset fileFound = "\relay\webservices\#cfcfound#.cfc">

			<cfset arrayAppend (filesFound,fileFound)>

		</cfloop>


		<cfloop array="#createobjectArray#" index="link">
			<cfset cfcFound = link[2]>

			<cfset cfcfound = replace(cfcfound,".","\","ALL")>

			<cfif listlen(cfcfound,"\") is 1>
				<cfset fileFound = "#currentdir#\#cfcfound#.cfc">
			<cfelseif refindNoCase("\A\\(code|relay|content)",cfcfound)>
				<!--- already in for \relay, \code, \content
				--->
				<cfset fileFound = cfcfound>

			<cfelse>
				<cfset fileFound = "\relay\#cfcfound#.cfc">
			</cfif>

			<cfset arrayAppend (filesFound,fileFound)>

		</cfloop>


		<cfloop array="#jsArray#" index="link">
			<cfset jsLink = link[2]>
			<cfset jsLink = "\relay\#jsLink#">
			<cfset jsLink = replace(jsLink,"/","\","ALL")>
			<cfset jsLink = replace(jsLink,"\\","\","ALL")>
			<cfif fileExists (application.paths.core & jsLink)>
				<cfset arrayAppend (filesFound,jsLink)>
			</cfif>

		</cfloop>


		<cfloop array="#filesFound#" index="fileFound">
			<cfset regExp = "(.*)##(.*?)##(.*)">
			<cfset find = application.com.regExp.refindAllOccurrences(regExp,fileFound, {1 = "before" , 2 = "variable", 3 = "after" })>
			<cfif arrayLen(find)>
				<cfset x = {
					 dashboardType = ['Advanced','FullScreen','SplitScreen','Standard']
					,"arguments\connectorType" = ['salesforce']
				}>
				<cfif structKeyExists (x,find[1].variable)>
					<cfloop array="#x[find[1].variable]#" index="sub">
						<cfset testFile = reReplaceNoCase (fileFound,regExp,"\1#sub#\3")>
						<cfif fileExists (expandPath (testFile))>
							<cfset insertLink (fileID, testFile)>
						</cfif>
					</cfloop>

				<cfelse>
					<cfoutput>#fileFound#<br /></cfoutput>
					<cfset insertLink (fileID, fileFound)>
				</cfif>
				
				
			
			<cfelse>
				<cfset insertLink (fileID, fileFound)>
			</cfif>
			
		</cfloop>	



		<cfquery name="filesToScan" datasource="#application.sitedatasource#">
		update 
			codeCop.dbo.allFiles
		set scanned = 1	
		where fileid = #fileid#

		</cfquery>

	</cfloop>


	<cfquery name="updateGood" datasource="#application.sitedatasource#">
	declare @count int = 1
	
	while @count <> 0
	BEGIN
		update codeCop.dbo.allfiles set derivedgood = 1
		from 
			codeCop.dbo.vfileLink fl 
				inner join 
			codeCop.dbo.allfiles af on fileid = tofileid	
		where (fl.knowngood = 1	or fl.derivedGood = 1)
		and af.derivedgood = 0
	
		set @count = @@rowcount
	END
	</cfquery>


<cfoutput>Files Linked To But Apparently Not Used</cfoutput>
			<cfquery name="notFound" datasource="#application.sitedatasource#">
				select distinct top 200  [from] from codeCop.dbo.vfilelink where not (_to_knowngood = 1	or _to_derivedGood = 1)			
			</cfquery>

<cfdump var="#notFound#">



<!--- search for .cfm --->
<!--- convert relative to absolute --->
<!--- update called column of db --->

<!--- search for cf_ --->
<!--- look for file in db under local directory --->
<!--- look for file in customtags --->



<cffunction name="convertAbsolutePathToRelayPath" output=false>
	<cfargument name="filepath">
	<cfset var result = filepath>
		<!--- read file file--->
		<cfset result = replacenocase(result,application.paths.core,'')>
		<cfset result = replacenocase(result,application.paths.test,'\test')>
		<cfset result = replacenocase(result,application.paths.code,'\code')>

	<cfreturn result>
</cffunction>

	<cffunction name="convertRelayPathToAbsolutePath" output=false>
	<cfargument name="relaypath">

		<cfset result = application.paths[listfirst(relaypath,"\")] & "\" &listrest(relaypath,"\")>

	<cfreturn result>
</cffunction>




<cffunction name="lookAtFile">
	<cfargument name="fileName">
	<cfargument name="level">

	<cfset var thisfile  = fileName>
	<cfset var thisfilePath  = "">
	<cfset var thisfileDirectory  = "">
	<cfset var linkFile = "">
	<cfset var fileNameToLookForInStructure = "">
	<cfset var filecontents= "">
	<cfset var linksarray = "">
	<cfset var args = "">

	<cfset thisfile = replaceNoCase(thisfile ,"\","/","ALL") >
	<cfif not listcontainsnocase("content,code,customTags,relay",listfirst(thisFile,"/"))>
		<cfset thisFile = "relay/" & Thisfile>
	</cfif>


	<cfset thisFile = replace(thisFile,"//","/","ALL")>

	<!--- try and replace /xx/..  with nothing --->
	<cfset thisFile = reReplaceNoCase (thisFile,"\/.*?\/\.\.","","ALL")>

	<!--- remove leading /s --->
	<cfset thisFile = reReplaceNoCase (thisFile,"\A\/","","ALL")>

	<cfif thisFile contains "##">
		<cftry>
			<cfset thisFile = evaluate ("'#thisFile#'")>
			<cfcatch>
			</cfcatch>
		</cftry>
	</cfif>

	<cfset fileNameToLookForInStructure = thisFile>
	<cfif listfirst(fileNameToLookForInStructure,"/") is "customtags" >
		<cfset fileNameToLookForInStructure = listfirst(fileNameToLookForInStructure,"/") & "/" & listlast(fileNameToLookForInStructure,"/")>
	</cfif>

	<cfif level gt 20>
		<cfoutput>
			Level 20 reached  - #fileNameToLookForInStructure# , #arguments.filename# <BR>
		</cfoutput>
		<cfreturn>
	</cfif>
	<!--- mark that page as OK --->
	<cfif structKeyExists (application.allfiles,fileNameToLookForInStructure)>
		<cfif application.allfiles[fileNameToLookForInStructure].found>
			<!--- <cfoutput>#thisFile# already done</cfoutput> --->
			<!--- already done --->
			<cfreturn >
		</cfif>
		<cfset application.allfiles[fileNameToLookForInStructure].found = true>
			 <cfoutput>Looking at file #thisfile#<BR></cfoutput>
	<cfelse>
		<cfoutput>#thisFile# Not Found<BR></cfoutput>
		<!--- not found, return --->
		<cfreturn>
	</cfif>


	<cfset thisfileDirectory = reverse(listrest(reverse(thisFile),"/"))>


	<!--- ReadFile --->
	<cffile action="read" file="#application.allfiles[fileNameToLookForInStructure].actualpath#" variable = "fileContent">
	<!--- remove comments --->
	<cftry>
	<cfset fileContent = application.com.regexp.removeallcomments(filecontent)>
		<cfcatch>
			<cfdump var="#cfcatch#">
		</cfcatch>
	</cftry>
	<!--- RegExp look for .cfm  and cf_--->
	<cfset args = {reg_expression = "[^a-zA-Z0-9_##.\/\\]([a-zA-Z0-9_##.\/\\]*?\.cfm)", string = filecontent}>
	<cftry>
		<cfset linksArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>
		<cfcatch>
			<cfdump var="#cfcatch#">
		</cfcatch>
	</cftry>
	<cfoutput><ul></cfoutput>
	<cfloop index="link" array="#linksarray#">
		<cfset linkFile = link[2]>
		<cfoutput>Link: #linkFile#<BR></cfoutput>
		<!--- if starts with a / then relative to root so leave--->
		<!--- if no slash then relative to current location --->

		<cfset linkfile = replace(linkfile,"\/","/","ALL")>  <!--- javascript escapes --->
		<cfif left(linkfile,1) is not "/">
			<cfset linkFile = thisFileDirectory & "/" & linkfile>
		</cfif>

		<cfif linkFile is not fileNameToLookForInStructure>

			<cfoutput><li></cfoutput>
			<cfset lookAtFile (linkfile,level + 1)>
			<cfoutput></li></cfoutput>
		</cfif>
	</cfloop>
	<cfoutput></ul></cfoutput>

	<cfset args = {reg_expression = "<CF_(.*?)[^a-zA-Z0-9_]", string = filecontent}>
	<cfset customTagsArray = application.com.regExp.reFindAllOccurrences(argumentCollection = args )>

	<ul>
	<cfloop index="link" array="#customTagsArray#">
		<cfset linkFile = link[2]>
		<cfoutput>Custom Tag: #linkFile#<BR></cfoutput>
		<!--- if starts with a / then relative to root so leave--->
		<!--- if no slash then relative to current location --->

		<cfset linkFile = "customtags/" & linkfile & ".cfm">

		<cfif linkFile is not fileNameToLookForInStructure>
			<li>
			<cfset lookAtFile (linkfile,level + 1)>
			</li>
		</cfif>
	</cfloop>
	</ul>




</cffunction>


<!--- look for these cfm in the list of all files and mark as OK --->

<!--- recurse on each cfm.  If you come across a file which has already been marked as OK then stop --->


<cffunction name="insertLink">
	<cfargument name="fromFileID" type=numeric>
	<cfargument name="ToFileName">

	<cfquery >
		declare @fromFileID int = #fromFileID#
				, @toFileName varchar(max)= '#toFileName#'
				,@toFileID int
				
		
		select @toFileID = fileID from codecop.dbo.allfiles where filename = @toFileName

		if @toFileID is null
		BEGIN
			insert into codecop.dbo.allFiles (filename, autoadded) values (@toFileName,1)
			set @toFileID = scope_identity()
		END

		if not exists (select 1 from codecop.dbo.filelink where fromFileID = @fromFileID and toFileID = @toFileID)
			insert into codecop.dbo.filelink (fromFileID, toFileID)	values (@fromFileID, @toFileID)
	
	</cfquery>


</cffunction>