<!--- �Relayware. All Rights Reserved 2014 --->

<!---
	Sometimes when regular expression searches go wrong they can crash the system without giving a stack trace
	This makes it very difficult to track down where the error has occurred, and it keeps happening again and again
	By wrapping this tag around offending processes we can automatically exclude a file which causes crashes from the next run

--->

<cfparam name="attributes.action" default="#request.action#">

<cfif thisTag.executionMode is "start">


		<cfquery name="insert" datasource="#application.sitedatasource#">
		insert into codecop.dbo._securityProject_filedonotprocess (filename,action,comment)
		values (<cf_queryparam value="#listfirst(attributes.identifier,"|")#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#attributes.action#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="crash test #attributes.comment#" CFSQLTYPE="cf_sql_varchar" >)
		</cfquery>


<cfelse>

		<cfquery name="insert" datasource="#application.sitedatasource#">
		delete from codecop.dbo._securityProject_filedonotprocess
		where
		filename =  <cf_queryparam value="#listfirst(attributes.identifier,"|")#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and action =  <cf_queryparam value="#attributes.action#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>


</cfif>