<cfsetting enablecfoutputonly="true">
<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="frmAddCheckPoint" default="1">
<cfif frmAddCheckPoint is "">
	<cfset frmAddCheckPoint = 0>
</cfif>


<cfset svnCFC = createObject ("component","svn")>

<cfset coreDirectories = svnCFC.getCoreDirectories()>

<!--- Set the core directory to use--->
<cfparam name="CoreDirectory" default="#application.paths.Core#">
<cfparam name="CoreDirectory_Manual" default="">
<cfif CoreDirectory_Manual is not "">
	<cfif not directoryExists (CoreDirectory_Manual)>
		<cfoutput>#application.com.relayui.message("#CoreDirectory_Manual# does not exist","error")#</cfoutput>
	<cfelse>
		<cfset Coredirectory = CoreDirectory_Manual>
	</cfif>
	
</cfif>

<cfset pathToSQL = "#replace (CoreDirectory,"/","\","ALL" )#\_support\sql\relay">

<cfset directoryArray = ["\updates"]>

<cfif not directoryExists (pathToSQL & directoryArray[1])>
	<cfset directoryArray = ["\3. update schema","\5. update data"]>
</cfif>


<cfset Overallsvn = svncfc.getDirectorySVN(pathToSQL).Overallsvn>


<!--- Get all the .sql files, put into a query with a few extra columns --->
<cfset firstPass = true>
<cfloop array="#directoryArray#" index="directory">
	<cfdirectory name="tmpScripts" directory="#pathToSQL##directory#" filter="*.sql" sort="directory + '\' +name asc" recurse="true">
	<cfquery name="scripts" dbtype="query">
		<cfif not firstPass>
		select * from scripts
		UNION
		</cfif>
		select * ,directory + '\' + name as ordering, '' as relativePath
		 from tmpscripts
		order by ordering
	</cfquery>
	<cfset firstPass = false>
</cfloop>


<cfloop query = "scripts">
	<cfset querySetCell(scripts,"relativePath", "#replacenocase(directory & "\" & name,pathToSQL & "\","")#", currentRow)>
</cfloop>

<cfset scripts = svnCFC.applySVNInfoToFileQuery (scripts)>


<cfif isdefined("ReleaseScriptsRun")>
	<cf_querySim>
	ScriptsRun
	Type,Name,Revision,Date
	<cfoutput>#replaceNocase(ReleaseScriptsRun,chr(9),"|","ALL")#</cfoutput>
	</cf_querySim>

	<cfquery name="ScriptsOutOfDate" dbtype="query">
	select scripts.relativePath , scripts.lastChangedRevision , scriptsRun.Revision from 
	ScriptsRun, scripts
	where
	scripts.relativePath = scriptsRun.name
	and scripts.lastChangedRevision > scriptsRun.Revision

	union 

	select scripts.relativePath , scripts.lastChangedRevision , 0 from 
	scripts
	where
	scripts.relativePath not in (#quotedValueList(scriptsRun.name)#)
	</cfquery>
	
	<cfset frmreleaseScriptname = valueList(scriptsOutOfDate.relativePath)>

</cfif>




<cfparam name = "frmreleaseScriptname" default  ="">
<cfif isdefined("frmreleaseScriptList")>
	<cfset frmreleaseScriptname = replacenocase (frmreleaseScriptList,chr(13) & chr(10),",","ALL")>
	<cfset frmreleaseScriptname = replacenocase (frmreleaseScriptName,"/","\","ALL")>
</cfif>




<cffunction name="runReleaseScript">
	<cfargument name = "scriptName">
		<cfset result = "">
		<cfset error = false>			
		<cffile action="read" file="#pathToSQL#/#scriptname#" variable="fileContent" charset="UTF-8">
			<!--- CCF can't handle GOs in queries, so have to break up into lots of individual ones 
			(?m) turns on multiline mode so that ^ matches the beginning of a line
			--->
			<cfset sqlArray = application.com.regExp.refindalloccurrences ("(?m)(\A|^GO)(.*?)(?=(\Z|^GO))",fileContent)>

			<cfloop index="arrayItem" array="#sqlArray#">
				<cfset theSQL = arrayItem[3]>
				<cfif rereplace(theSQL,"\W","","ALL") is not ""> <!--- replaces all non word characters with blank, prevents blnak queries running --->
					<cftry>
						<cfquery name="x" datasource = "#application.siteDataSource#">
						#preserveSingleQuotes(theSQL)#
						</cfquery>
						<cfcatch>
							<cfset error = true>			
							<cfset result = result &"
								Query Failed<BR>
								#cfcatch.detail#<BR>
								#theSQL# <BR>
							">	
						</cfcatch>
					</cftry>
				</cfif>	
			</cfloop>
		<cfif error is false>
			<cfset result = "OK">
		</cfif>	
	<cfreturn result>	
</cffunction>





<!--- Get the distinct revision numbers for populating a drop down --->
<cfquery name="distinctRevisions" dbtype="query">
	select distinct lastChangedRevision, lastChangedDateString  
	from scripts 
	where inSVN = 1
	order by lastChangedRevision desc
</cfquery>

<cfparam name="fromRevision" default="">

<!--- Filter by revision if required --->
<cfif fromRevision is not "" >
	<cfquery name="scriptsFiltered" dbtype="query">
		select * from scripts 
		where
			lastChangedRevision >= #fromRevision#
	</cfquery>
<cfelse>
	<cfset scriptsFiltered = scripts>
</cfif>


<!--- Now output the list of scripts --->
<cf_includeJavascriptOnce template = "/javascript/checkBoxfunctions.js">

<!--- Do not translate any of this page, some SQL scripts have phr_ in them --->
<!-- StartDoNotTranslate -->
<cfoutput>
<form method="post">
Choose which Directory to retrieve the SQL code from<br />
<cf_select name="coredirectory" query="#coreDirectories#" selected="#Coredirectory#" value="directory" display="directory" allowCurrentValue="true"><br />
Or enter the path to a core directory<br />
<input name="coredirectory_Manual" type="Text" size="50"><br />

This Directory is at svn #Overallsvn#
<cfif Overallsvn LT distinctRevisions.lastChangedRevision>
	<cfoutput>#application.com.relayUI.message ("Files Not at same revision","Error")#</cfoutput>
</cfif>

</cfoutput>

<cfoutput>
	<BR>Show Scripts Changed Since Revision<br />
	<cf_select name="fromRevision" query="#duplicate(distinctRevisions)#" 	selected="#fromRevision#" value="lastChangedRevision" display="lastChangedRevision" nullText="From" showNull=true group="lastchangeddatestring">
	<input name="" type="submit" value = "Go">
	<br />
	<cf_input type="checkBox" name="frmAddCheckPoint" value="1" checked="#iif(frmAddCheckPoint,true,false)#">Add CheckPoint Code
	<input type="hidden" name="frmAddCheckPoint" value="">


<table>
	<th><input type="checkBox" onclick = "toggleCheckboxes (this.form,'frmreleasescriptname',/./,this.checked)"></th><th>Script</th><th>Result</th>

</cfoutput>
<cfloop query="scriptsFiltered" group="directory">
	<cfset directoryPath = replacenocase(directory,pathToSQL,"")>
	<cfif directoryPath is "">
		<cfset directoryPath = "\">
		<cfset regExp = "^[^\\]*$">
	<cfelse>
		<cfset regExp = right(directoryPath,len(directoryPath) -1)>
		<cfset regExp  = replace(regExp,"\","\\","ALL" )>
	</cfif>



	<cfoutput><tr><Td colspan="2"><CF_INPUT type="checkBox" onclick = "toggleCheckboxes (this.form,'frmreleasescriptname',/#regExp#/,this.checked)"><b>#htmleditformat(directoryPath)#</b></Td></tr>	</cfoutput>
	
	<cfloop>
	<!--- Actually run the script here.  OK I agree, rather nasty doing it in the middle of some output--->
	<cfif listFindNoCase(frmreleaseScriptName,relativePath) and isdefined("Run")>
		<cfset scriptresult = runReleaseScript(relativePath)>
	<cfelse>
		<cfset scriptresult = "">
	</cfif>
	
	
		<cfoutput>
	<tr>
		<td>
			&nbsp;&nbsp;
		</cfoutput>
			<cfif inSVN>
				<CF_INPUT type="checkbox" name="frmreleasescriptname" value="#relativePath#" checked="#iif( listFindNoCase(frmreleaseScriptName,relativePath),true,false)#">
			</cfif>
		<cfoutput>
		</td>
		<td>
			#htmleditformat(name)# (<cfif inSVN>#lastChangedRevision#<cfelse>Not in SVN</cfif>)
		</td>
		<td>
			<cfif scriptresult is not ""><pre>#htmleditformat(scriptresult)#</pre></cfif>
		</td>	
	</tr>
	</cfoutput>
	</cfloop>
</cfloop>
<cfoutput>
</table>
<input name="Run" type="submit" value = "Run Queries">
<input name="" type="submit" value = "Generate Script">
</form>
</cfoutput>

<!--- 
Generate the script for all the chosen sql files
--->
<cfsetting enablecfoutputonly="true">
<cfSaveContent variable="fullScript">
<cfoutput>
/* 
Script Generated #application.com.datefunctions.dateTimeformat(date = now(), showIcon=false)#
<cfif fromRevision is not "">From Revision #fromRevision#</cfif>
To Revision #distinctRevisions.lastChangedRevision# (latest)
<cfif frmreleaseScriptName is "">
All Eligible Scripts
#replace(valueList(scriptsFiltered.relativePath),",",chr(10),"ALL")#
<cfelse>
#replace(frmreleaseScriptName,",",chr(10),"ALL")#
</cfif>
*/
</cfoutput>
<cfset regExp = "(?<!CheckPoint\*/)(\r\nGO)(?:\r\n|\Z)(?!/\*Start CheckPoint)">
<cfset regExpObject = createObject("java", "java.util.regex.Pattern")>

<cfset Counter = 0>
<cfloop query="scriptsFiltered">
	<cfif inSVN AND (frmreleaseScriptName is "" or listFindNoCase(frmreleaseScriptName,relativePath))>
		<cffile action="read" file="#directory#/#name#" variable="fileContent" charset="UTF-8">
		<cfif frmAddCheckPoint>
			<cfset pattern = regExpObject.compile("" &  regexp,0 )>
			<cfset match = pattern.matcher(filecontent)>
	
			<cfloop condition="match.find() and counter lt 1200">
				<cfset counter ++>
				<cfset insertString = "#chr(13)##chr(10)#/*Start CheckPoint*/ Print ('Checkpoint #Counter# ' + convert(varchar,getdate(),108)) /*End CheckPoint*/#chr(13)##chr(10)#GO#chr(13)##chr(10)#">
				<cfset  filecontent= match.replaceFirst("$1#insertString#")>
				<cfset match = pattern.matcher(filecontent)>
			</cfloop>
		
		</cfif>
		
		<cfoutput>	
		#chr(10)#Print 'Start of #name#.  Revision #lastChangedRevision#'
		#chr(10)#Print ('         ' + convert(varchar,getdate(),108))
		#chr(10)#GO#chr(10)#
		#chr(10)##fileContent#
		#chr(10)#GO#chr(10)#Print 'End of #name#'
		#chr(10)#Print ('         ' + convert(varchar,getdate(),108))
		#chr(10)#GO#chr(10)#

		exec upsertReleaseHistory 'sql','#relativePath#',#lastChangedRevision#
		</cfoutput>		
	</cfif>	
</cfloop>

</cfSaveContent>

<!--- IF very long then save as a file and give a link --->
	<cfset fileName = "releaseScript.txt">
	<cfset tempFolder = application.com.fileManager.getTemporaryFolderDetails()>
	<cfset absolutePath = tempFolder.path & "/" & fileName>
	<cfset webPath = tempFolder.webpath & fileName>
	<cffile action="write" file="#absolutePath#" output="#fullScript#"> 
	<cfset scriptWebLink = '<a href="#webPath#" target="_newWindow">Link</a>'>


<cfsetting enablecfoutputonly="false">


<!--- 
Generate a script for analysing which scripts haven't been run
 --->
<cfset firstPass = true>
<cfsetting enablecfoutputonly="true">	
<cfSaveContent variable="latestScriptsAnalysis">
<cfoutput>
declare @latestScripts as Table (name varchar(200), revision int)
Insert into @latestScripts (name, revision)
</cfoutput>
<cfloop query="scripts">
	<cfif inSVN>
	<cfif firstPass>
		<cfset firstPass = false>
	<cfelse>	
		<cfoutput> Union #chr(10)#</cfoutput>
	</cfif>
	<cfoutput>select '#relativePath#'	, #lastChangedRevision# #chr(10)#</cfoutput>
	</cfif>	
</cfloop>
<cfoutput>
select l.name, l.revision as LatestRevision, rh.revision currentRevision from @latestScripts l left join releaseHistory rh on rh.type='sql' and l.name= rh.name
where l.revision > isnull(rh.revision,0)
</cfoutput>
</cfSaveContent>
<cfsetting enablecfoutputonly="false">	




<cfoutput>

ALL QUERIES JOINED TOGETHER<BR>
	#scriptWebLink#<br />
<cfif  len(fullScript) LT 1000000 >
	<textarea rows = "20" cols="120">
	#fullScript#
	</textarea>

<cfelse>

</cfif>



<BR>
<BR>
Paste a list of SQL files here to generate a combined script<br />
<form method="Post">
<textarea name="frmreleaseScriptList" rows = "10" cols="120"><cfif frmreleaseScriptName is not "" ><cfoutput>#replaceNoCase(frmreleaseScriptName,",",chr(10),"ALL")#</cfoutput></cfif>
</textarea>
<BR>
<input type="submit" value = "Generate Script">
</form>



<br /><br />
RELEASE HISTORY
<br />
EITHER: Run this SQL to find out which scripts have not been run on your database<br />
<textarea rows = "5" cols="120">
#latestScriptsAnalysis#
</textarea>


<br />OR: Paste the result of the query <BR><span style="font-family:courier">Select * from releaseHistory</span><br /> into this box, to find out which scripts need running<br />
<form method="Post">
<textarea name="ReleaseScriptsRun" rows = "5" cols="120"></textarea>
<BR>
<input type="submit" value = "Analyse">
</form>

<BR>
</cfoutput>

<!-- EndDoNotTranslate -->



