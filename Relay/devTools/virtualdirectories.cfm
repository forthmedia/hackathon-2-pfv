<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

WAB 2010/12/06
Tool for changing virtual directories for your current site
For dev site only
 --->

<cfoutput>
Use this tool to change the IIS root and virtual directories. <BR>
This allows you to quickly change which database your dev site is pointing to <BR>
This, in turn, means that you do not have to create an IIS Site for every database, you can have one set (or at least a small number) of URLs and switch them from database to database<BR>
Choose a value from the drop down (these are siblings of the current mappings you have)<BR>
Or enter a full path to Relay, Code or Content in the field to the right<BR>
After any change you should do a ?flush=1<BR>

</cfoutput>

<cfif isdefined("updatevdir")>
	<cfloop list="relay,code,content" index="vdir">
		<cfif form["vdir_#vdir#_manual"] is not "">
			<cfset form["vdir_#vdir#"] = form["vdir_#vdir#_manual"]>
		</cfif>
		<cfif directoryExists (form["vdir_#vdir#"])>
			<CFset res = application.com.iisadmin.setVirtualDirectories(VDIR = iif(vdir is "relay","",de(vdir)), PHYSICALPath = form["vdir_#vdir#"])>
			<cfif not res.isOK>
				<cfoutput><div class="errorblock">#res.string#</div></cfoutput>
			</cfif>
		<cfelse>
			<cfoutput><div class="warning">Directory #form["vdir_#htmleditformat(vdir)#"]# does not exist</div></cfoutput>			
		</cfif>
	</cfloop>
</cfif>


<cfset x = application.com.iisadmin.getVirtualDirectoriesForThisSite()>

<cfoutput>
<form method="post">
<table>
	<tr>
		<th>Virtual Directory</th>
		<th>Select Path</th>
		<th>Notes</th>
		<th>Manually set the Path <BR>(You have to include the /code, /content or /relay)</th>
	</tr>

	<cfloop array="#x#" index="vdir">
		<cfset physicalPath = vdir.xmlattributes.physicalpath>
		<cfset vdirNoSlash = #replace(vdir.xmlattributes.path,"/","","ONE")#>
		<cfif vdirNoSlash is "">
			<cfset vdirNoSlash = "relay">
		</cfif>

		<cfif listfindnocase("code,content,relay",vdirNoSlash)>
			<cfset twoDirectoriesUp = reverse(listrest(listrest(reverse(vdir.xmlattributes.physicalpath),"\"),"\"))>

			<!--- get a directory by going up a few levels and looking for directories with same name --->
			<cfdirectory action="list" directory="#twoDirectoriesUp#" name="theDir" type="dir">
		
			<tr>
				<td valign="top">#vdir.xmlattributes.path# <cfif vdir.xmlattributes.path is "/">(Relay)</cfif></td><td>
				#htmleditformat(twoDirectoriesUp)#/<select name="vdir_#vdirNoSlash#">
					<cfloop query="theDir">
						<cfset path = "#directory#\#name#\#vdirNoSlash#">
						<cfif directoryExists(path)>
						<option value = "#path#" <cfif path is #vdir.xmlattributes.physicalpath#>selected</cfif>>#htmleditformat(name)#\#htmleditformat(vdirNoSlash)# 
						</cfif>
					</cfloop>
				</select>
					<cfif vdirNoSlash is "content">
						<cfset xmlResult = application.com.xmlfunctions.readAndParseXMLFile ("#vdir.xmlattributes.physicalpath#/../xml/datasource.xml")>
						<cfif xmlResult.isOK>
							<BR>Current datasource: (#xmlResult.xml.datasource.xmltext#)
						</cfif>
					</cfif>
		
				</td>
				<td>
					<cfif vdirNoSlash is "Relay">
						This is the location of your Relay Directory <BR>as checked out from source control
					<cfelseif vdirNoSlash is "code">
						This is the location of the Client Specific Code directories <BR>as checked out from source control
					<cfelseif vdirNoSlash is "content">
						This is the location of the Client Specific Content directories.  <BR>These are usually on a share such as <BR>\\fnl-dev1\web\CLIENTCONTENTMATCHEDTODATABASE <BR>
						The name of the directory gives a clue as to which database your datasource needs to connect to.<BR>
						The datasource name used is in brackets
						
					</cfif>
				</td>
				<td><CF_INPUT type="text" name="vdir_#vdirNoSlash#_manual" value="" size=30></td>
		
		
			</tr>
		</cfif>
	</cfloop>
</table>
<input name="updatevdir" type="submit" value="Update">
</form>
</cfoutput>

