<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>

<cfset zeroArray = [0]>
<cfset blankArray = [""]>

<cffunction name="applySVNInfoToFileQuery">
	<cfargument name="fileQuery">
	
	<cfif not listFindNoCase (fileQuery.columnList,"lastChangedRevision")>
		<cfset queryAddColumn (fileQuery,"lastChangedRevision","integer",zeroArray)>
	</cfif>
	<cfif not listFindNoCase (fileQuery.columnList,"lastChangedDateString")>
		<cfset queryAddColumn (fileQuery,"lastChangedDateString","varchar",blankArray)>
	</cfif>
	<cfif not listFindNoCase (fileQuery.columnList,"lastChangedDate")>
		<cfset queryAddColumn (fileQuery,"lastChangedDate","date",zeroArray)>
	</cfif>
	<cfif not listFindNoCase (fileQuery.columnList,"inSVN")>
		<cfset queryAddColumn (fileQuery,"inSVN","integer",zeroArray)>
	</cfif>


	<cfquery name="directories" dbtype="query">
		select distinct directory from fileQuery
	</cfquery>

	<cfset svnProperties = {}>
	<cfset entryArrayPositions = {name=1,type=2,lastChangedDate = 9,lastChangedRevision=10,size=33}>

	<cfloop query="directories">
			<cfset svnProperties[directory] = {}>
			<cfset svnFileName = "#directories.directory#/.svn/entries">
			<cfif fileExists (svnFileName)>
				<cffile action="read" file="#svnFileName#" variable="svn">
				
				<cfset svn = rereplace(svn,"(\n)(?=\n)","\1 ","ALL")>
				<cfset entriesArray = listtoarray(svn,chr(12))>
				
				<cfloop array="#entriesArray#" index="entry">
					<cfset entryArray = listtoarray(entry,chr(10))>
					<cftry>
						<cfif arrayLen(entryArray) gte entryArrayPositions["size"] and entryArray[entryArrayPositions["type"]] is "file">
							<cfset props = {
										name=entryArray[entryArrayPositions["name"]],
										lastChangedRevision=entryArray[entryArrayPositions["lastChangedRevision"]],
										lastChangedDate=application.com.datefunctions.extractDateTimeFromUTCString(entryArray[entryArrayPositions["lastChangeddate"]]).utcdatetime
										}>
							<cfset svnProperties[directory][props.name] = props>
						</cfif>
						<cfcatch>
							Problem with <cfoutput>#directories.directory#</cfoutput>
							<cfdump var="#entryArray#"><cfabort>
						</cfcatch>
					</cftry>	
				</cfloop>
			</cfif>
			
	</cfloop>

	<!--- Add revision details to the query of scripts --->
	<cfloop query = "fileQuery">
		<cfif structKeyExists (svnProperties[directory],name)>
			<cfset querySetCell(fileQuery,"lastChangedRevision", svnProperties[directory][name].lastChangedRevision , currentRow)>
			<cfset querySetCell(fileQuery,"lastChangedDate", svnProperties[directory][name].lastChangedDate , currentRow)>
			<cfset querySetCell(fileQuery,"lastChangedDateString", dateFormat(svnProperties[directory][name].lastChangedDate,"dd/mm/yy") , currentRow)>
			<cfset querySetCell(fileQuery,"inSvn", 1 , currentRow)>		
		<cfelse>
			<cfset querySetCell(fileQuery,"inSvn", 0 , currentRow)>		
		</cfif>	
	</cfloop>
	

	<cfreturn fileQuery>	

</cffunction>


<cffunction name="getCoreDirectories">
	<!--- Get the different code versions which are in your web directory, for populating a drop down --->
	<cfset coreDirectories = queryNew("directory")>
	<cfdirectory action="list" directory="#application.paths.web#" name="dirs"  >
	
	<cfloop query="dirs">
		<cfif type is "dir">
			<cfdirectory action="list" directory="#directory#\#name#" name="sdirs" filter="_support" >
			<cfif sdirs.recordCount>
				<cfset queryAddRow(coreDirectories)>
				<cfset querySetCell(coreDirectories,"directory",sdirs.directory,coreDirectories.recordCount)>
			</cfif>
		</cfif>	
	</cfloop>	
	
	<cfreturn coreDirectories>
</cffunction>


<cffunction name="getDirectorySVN">
	<cfargument name="directory">

	<!--- Get the SVN of the whole Directory Structure --->
	<cffile action="read" file="#directory#/.svn/entries" variable="svn">
	<cfset svn = rereplace(svn,"(\n)(?=\n)","\1 ","ALL")>
	<cfset entriesArray = listtoarray(svn,chr(12))>
	<cfset entryArray = listToArray(entriesArray[1],chr(10))>
	<cfset result = {Overallsvn = entryArray[4]}>
	<cfreturn result >
	
</cffunction>


</cfcomponent>	