<!--- �Relayware. All Rights Reserved 2014 --->
<cfset svnCFC = createObject ("component","svn")>

<cfset coreDirectories = svnCFC.getCoreDirectories()>

<!--- Set the core directory to use--->
<cfparam name="CoreDirectory" default="#application.paths.Core#">
<cfparam name="CoreDirectory_Manual" default="">
<cfif CoreDirectory_Manual is not "">
	<cfif not directoryExists (CoreDirectory_Manual)>
		<cfoutput>#application.com.relayui.message("#CoreDirectory_Manual# does not exist","error")#</cfoutput>
	<cfelse>
		<cfset Coredirectory = CoreDirectory_Manual>
	</cfif>
	
</cfif>

<cfset Overallsvn = svncfc.getDirectorySVN(coreDirectory).Overallsvn>



<cfdirectory action="list" directory="#coreDirectory#" name="files" filter="*.cfm|*.cfc|*.dll|*.xml|*.js|*.htm*|*.txt" recurse = true>

<cfset svnCFC = createObject ("component","svn")>

<cfset files = svnCFC.applySVNInfoToFileQuery (files)>


<!--- Get the distinct revision numbers for populating a drop down --->
<cfquery name="distinctRevisions" dbtype="query">
	select distinct lastChangedRevision, lastChangedDateString  
	from files 
	where inSVN = 1
	order by lastChangedRevision desc
</cfquery>

<cfparam name="fromRevision" default="">


<form method="post">
Choose which Directory to Analyse<br />
<cf_select name="coredirectory" query="#coreDirectories#" selected="#Coredirectory#" value="directory" display="directory" allowCurrentValue="true"><br />
Or enter the path to a core directory<br />
<input name="coredirectory_Manual" type="Text" size="50"><br />

<cfoutput>This Directory is at svn #Overallsvn#<br /></cfoutput>
<cfif Overallsvn LT distinctRevisions.lastChangedRevision>
	<cfoutput>#application.com.relayUI.message ("Files Not at same revision","Error")#</cfoutput>
</cfif>




<BR>Find Files Changes since Revision<br />
<cf_select name="fromRevision" query="#duplicate(distinctRevisions)#" 	selected="#fromRevision#" value="lastChangedRevision" display="lastChangedRevision" nullText="From" showNull=true group="lastchangeddatestring">
<input name="" type="submit" value = "Go">

</form>


<cfif fromRevision is not "" >

	<cfquery name="files" dbtype="query">
		select * from files 
		where
			lastChangedRevision >= #fromRevision#
	</cfquery>
	

	Files Changed<br />

<textArea rows="20" cols="80"><cfsetting enablecfoutputonly="true">	
	<cfloop query = "files">
		<cfset x = replacenocase(directory & "\" & name,coreDirectory & "\","")>
		<cfoutput>#x##chr(10)#</cfoutput>
	</cfloop>
<cfsetting enablecfoutputonly="false">	
</textArea>

</cfif>
