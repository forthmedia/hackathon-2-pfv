<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			errorSummary.cfm
Author:				SWJ
Date started:		2005

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-08-26 SWJ Added the PeopleWithErrors query
2007-06-01 WAB  modified to take account of the relayerrorFix table / status
2010-01-11	NJH LID 2915 - htmlEditFormat output the template name brought back by the query
2012-10-05 WAB MAde some changes to take account of the new columns in relayError, and the fact that we don't use the relayErrorFix table at the moment
2015-11-18 SB Removed tables code to bring in Bootstrap
Possible enhancements:


 --->


<cfparam name="URL.SendEmail" type="boolean" default="false">

<cfquery name="PeopleWithErrors" datasource="#application.sitedatasource#">
select Firstname +' '+ lastname as Full_name, count(*) as Errors
	from relayError re
	left outer join vPeople vp ON vp.personid = re.personid
	where errorDateTime > dateadd(d,-14,getDate())
	group by Firstname +' '+ lastname
	HAVING count(*) > 1
			ORDER By errors Desc
</cfquery>



 <cfquery name="recentErrors" datasource="#application.sitedatasource#">
 	select
		source, type ,filename,
		case when charindex ('?', template) <> 0 then left(template,charindex ('?', template)-1) else template end as template,
		 linenumber, max(relayErrorID) as relayErrorID, count (1) as totalcount,
count ( case when errorDateTime > dateadd(hh,-24,getDate()) then 1 else null end) as last_24_hrs,
count ( case when errorDateTime > dateadd(hh,-48,getDate()) then 1 else null end) as last_48_hrs,
count ( case when errorDateTime > dateadd(hh,-96,getDate()) then 1 else null end) as last_96_hrs,
count ( case when errorDateTime > dateadd(d,-7,getDate()) then 1 else null end) as this_Week

	from
		relayError re

	where errorDateTime > dateadd(d,-7,getDate())
	group by source, type, filename,
	case when charindex ('?', template) <> 0 then left(template,charindex ('?', template)-1) else template end,
	linenumber
order by last_24_hrs desc

</cfquery>




<div class="row">
	<div class="col-xs-6">
		<p>Relay Recent Error Summary</p>
	</div>
	<div class="col-xs-6 submenu">
		<ul class="list-inline pull-right">
			<li><A HREF="errorList.cfm" Class="Submenu">Full List</A></li>
			<li>|</li>
			<li><A HREF="errorSummary.cfm?SendEmail=true" Class="Submenu">Email summary</A></li>
			<li>|</li>
			<li><A HREF="updateErrorTypes.cfm" Class="Submenu">Update Error Types</A></li>
			<li>|</li>
			<li><A HREF="javascript:history.go(-1);" Class="Submenu">Back</A></li>
	</div>
</div>

<cfsaveContent variable="reportContent_html">
	<cfoutput>
		<cfif fileexists("#application.paths.content#\styles\relayWareStyles.css")>
			<cfhtmlhead text='<LINK REL="stylesheet" HREF="/code/styles/relayWareStyles.css">'>
		<cfelse>
			<cfhtmlhead text='<LINK REL="stylesheet" HREF="/Styles/relayWareStyles.css">'>
		</cfif>
	</cfoutput>
	<h2>Errors - Last 7 days</h2>
	<CF_tableFromQueryObject queryObject="#recentErrors#"
		HidePageControls="no"
		useInclude = "false"

		keyColumnList="Template"
		keyColumnURLList="/errorHandler/errorDetails.cfm?errorID="
		keyColumnKeyList="relayErrorID"

		showTheseColumns="source,type,Template,Last_24_hrs,Last_48_hrs,Last_96_hrs,This_Week"
		sortorder="last_24_hrs Desc"
		showTotals="yes">


	<h2>People who've experienced more than 1 Error in the Last 14 days</h2>

	<CF_tableFromQueryObject queryObject="#PeopleWithErrors#"
		HidePageControls="no"
		useInclude = "false"
		showTheseColumns="Full_name,Errors"
		sortorder="errors Desc">

	<!--- <h2>Error details last 14 days</h2> --->


</cfsavecontent>


<cfoutput>#reportContent_html#</cfoutput>

<cfif URL.SendEmail>
	<cfmail to="errorAdmin@foundation-network.com"
        from="#application.AdminPostmaster#"
        subject="#request.CurrentSite.Title# Error Summary"
        type="HTML">
#reportContent_html#
	</cfmail>
</cfif>
