<!--- 
File name:			TestRelaywareAPIConnection.cfm	
Author:				PPB
Date started:		2014-09-24
	
Description:		Test the connection to the Relayware API 


Amendment History:

Possible enhancements:

Offer a dropdown of users in the API group for the user to select one to avoid having to edit this file 

 --->

<cfif not StructKeyExists(url,"username") OR not StructKeyExists(url,"password")>
	<br/>
	&nbsp;&nbsp;You must enter a 2 url parameters to use this utility: username and password. The user should be a RW user in the API user group.
	
	<br/><br/>
	&nbsp;&nbsp;eg. http://thecustomername.relayware.com/utilities/TestRelaywareAPIConnection.cfm?username=Locator Api&password=pass123

<cfelse>

	<cfhttp url="#request.currentSite.httpProtocol##cgi.http_host#/api/v1/authenticate" method="post" result="apiResult">
		<cfhttpparam type="formfield" name="username" value="#url.username#">
		<cfhttpparam type="formfield" name="password" value="#url.password#">
	</cfhttp>

	<cfdump var="#apiResult#">

	<br/><br/>
	&nbsp;&nbsp;If you see StatusCode: 200 Ok in the response above, the call has been successful
	
</cfif>



