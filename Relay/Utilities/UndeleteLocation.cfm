<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Very Rough template for un-deleting a Location

WAB 2000-11-29
2013-12-19 		NYB 	Case 436226 changed file to handle processing of multiple ids
2015-11-18      SB		Removed tables for Bootstrap

--->

<cf_title>Undelete Locations</cf_title>

<cfset subsection = "Undelete Location">
	<!--- START: 2013-12-19 NYB Case 436226 - rewrote pretty much the entire file --->
	<form method = "post" enctype="multipart/form-data">
		<div class="div-floatleft-47">
			<div class="grey-box-top">
				<div class="form-group">
					<label for="frmLocationID">Enter the ids of the Locations to be undeleted</label>
					<INPUT type="text" id="frmLocationID" name="frmLocationID" length="8" class="form-control">
				</div>
			</div>
		</div>
		<div id="undeleteLocationOr" class="div-floatleft-6 text-align-center">
			<h4>or</h4>
		</div>
		<div class="div-floatleft-47">
			<div class="grey-box-top">
				<div class="form-group">
					<label for="dataForLoading">Upload csv file (containing locationids only)</label>
					<input type="file" id="dataForLoading" name="dataForLoading" size="50">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<INPUT type= "submit" id="frmSubmit" class="btn btn-primary">
			</div>
		</div>
	</form>

	<cfset unDelete = Querynew("LocationID","Integer")>
	<cfset unDelete2 = Querynew("LocationID","Integer")>

	<cfif structkeyexists(form,"dataForLoading") and form.dataForLoading neq "">
		<cfset dateV = now()>
		<cfset tableName = "undeleteLocations#Year(dateV)##Month(dateV)##Day(dateV)##Hour(dateV)##Minute(dateV)##Second(dateV)#">
		<cfset dataFileName = "#tableName#.csv">
		<cfset fileDetails = application.com.fileManager.uploadFile(fileField="dataForLoading",destination="#application.paths.CONTENT#\UndeleteRequests\",renameFileTo=dataFileName,nameConflict="overwrite",accept="text/*,application/octet-stream,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",weight=6000)>
		<cfif fileDetails.isok>
			<cfset fieldNames = arraynew(1)>
			<cfset x = ArrayAppend(fieldNames, "LocationID")>
			<CFSET urladdress = 'http#IIf(CGI.https is "on", DE("s"), DE(""))#://' & CGI.SERVER_NAME & "/content/UndeleteRequests/" & dataFileName>
			<cfhttp url="#urladdress#" firstrowasheaders="no" method="GET" resolveurl="Yes" throwOnError="Yes" name="LocIDs" columns="LocationID" />

		<cftry>
				<CFQUERY NAME="unDelete" DATASOURCE=#application.sitedatasource#>
				DECLARE @DataTable AS UndeleteLocationsTableType
				<cfloop query="LocIDs">
					INSERT INTO @DataTable(LocationID) values (#LocationID#)
				</cfloop>
				exec undeleteLocationsFromTable @UndeleteLocationsTable=@DataTable
			</CFQUERY>

			<cfcatch>
				<cfoutput><br /><br />There has been an error undeleting locations from the uploaded file: #cfcatch.message#<br /></cfoutput>
			</cfcatch>
		</cftry>


		</cfif>
	</cfif>

	<CFIF isDefined("frmLocationID") and len(frmLocationID) gt 0>
		<cftry>
			<CFQUERY NAME="checkLocationDel" DATASOURCE=#application.sitedatasource#>
				select LocationID from LocationDel where LocationID in (<cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" list="yes">)
			</CFQUERY>

			<CFQUERY NAME="unDelete2" DATASOURCE=#application.sitedatasource#>
				exec undeleteLocations @locationid = '#valuelist(checkLocationDel.LocationID)#'
			</CFQUERY>

			<cfcatch>
				<cfoutput><br /><br />There has been an error undeleting locations specified: #cfcatch.message#<br /></cfoutput>
			</cfcatch>
		</cftry>
	</cfif>

	<cfquery name="unDelete" dbtype="query">
		select * from unDelete
		union
		select * from unDelete2
	</cfquery>

	<cfif unDelete.recordcount gt 0><cfoutput><br />The following #unDelete.recordcount# LocationID#iif(unDelete.recordcount gt 1,de("s were"),de(" was"))# successfully undeleted</cfoutput></cfif>
	<cftable query = "unDelete"
	    startRow = "1" colSpacing = "3" HTMLTable colHeaders border>
	    <cfcol header = "<b>LocationID</b>"
	        align = "Left"
	        text="#LocationID#">
	</cftable>
	<!--- END: 2013-12-19 NYB Case 436226 --->