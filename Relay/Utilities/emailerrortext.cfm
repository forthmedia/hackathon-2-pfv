<!--- �Relayware. All Rights Reserved 2014 --->
<!-- William's Code for view and amending Email Error Text data-->

<!---   Used to add an item to the EmailErrorText Table  --->
<CFIF isDefined("add")>
	<CFQUERY name="AddItemToEmailErrorText" datasource="#application.sitedatasource#">
        INSERT INTO  EmailPhrases(Phrase, active,Action, CommStatus, feedback, subjectLineOnly)
        VALUES(<cf_queryparam value="#Phrase#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#active#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Action#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#CommStatus#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#feedback#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#subjectLineOnly#" CFSQLTYPE="CF_SQL_VARCHAR" >)
	</CFQUERY>   
	<cfset message ="Email Phrase Added: #Phrase# ">              
</CFIF>
<CFIF isDefined("update")>
	<CFQUERY name="updateItemToEmailErrorText" datasource="#application.sitedatasource#">
        UPDATE  EmailPhrases
		 	SET Phrase =  <cf_queryparam value="#Phrase#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				active =  <cf_queryparam value="#active#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				Action =  <cf_queryparam value="#Action#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				CommStatus =  <cf_queryparam value="#CommStatus#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				feedback =  <cf_queryparam value="#feedback#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				subjectLineOnly =  <cf_queryparam value="#subjectLineOnly#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				sortorder =  <cf_queryparam value="#sortorder#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			WHERE phraseid =  <cf_queryparam value="#phraseid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>   
	<cfset message ="Email Phrase Updated: #Phrase# ">              
</CFIF>


<CFQUERY NAME="ListErrorText" datasource="#application.sitedatasource#">
	<CFIF isDefined("Addnew")>
		SELECT '' as sortorder,'' as phrase, '' as active,'' as Action, '' as CommStatus, '' as feedback, '' as subjectLineOnly, '' as flagtextid 
	<CFELSE>
		SELECT sortorder,Phraseid,Phrase, active,Action, CommStatus, feedback, subjectLineOnly, flagtextid 
		FROM EmailPhrases 
		where isNull(phrase,'') <> ''
		and isNull(action,'') <> ''
		and active =1
	</CFIF>
	<CFIF isdefined("Editphraseid")>
		and phraseid =  <cf_queryparam value="#Editphraseid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
	<CFIF not isDefined("Addnew")>
		order by sortorder
	</CFIF>
</CFQUERY>



<cfset current = "emailErrorText.cfm">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<cfif isDefined('message') and message neq "">
<tr>
	<td><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></td>
</tr>
</cfif>
<CFIF isdefined("Editphraseid")><CFSET cols=2><CFELSE><CFSET cols=7></CFIF>
<tr>
	<CFOUTPUT><th colspan="#cols#">Returned Email Processing</th></CFOUTPUT>
</tr>
<tr>
	<CFOUTPUT><td colspan="#cols#">The following phrases are checked for in returned emails</td></CFOUTPUT>
</tr>
<CFIF NOT isdefined("Editphraseid") AND NOT IsDefined("Addnew")>
<tr><th>Phrase</th><th>Action</th><th>Comm Status</th><th>Feedback</th><th>Subject Line Only</th><th>Active</th><th>Sort Order</th></tr>
</CFIF>
<CFSET nCount=1>
<cfoutput query="ListErrorText" >
	<CFIF isdefined("Editphraseid") OR IsDefined("Addnew")>
	<FORM NAME="Update" METHOD="post">
		<TR><TD>Phrase</TD><TD><CF_INPUT TYPE="text" NAME="Phrase" VALUE="#Phrase#"></TD></TR>
		<TR><TD>Action</TD><TD><CF_INPUT TYPE="text" NAME="Action" VALUE="#Action#"></TD></TR>
		<TR><TD>Comm Status</TD><TD><CF_INPUT TYPE="text" NAME="CommStatus" VALUE="#CommStatus#"></TD></TR>
		<TR><TD>Feedback</TD><TD><CF_INPUT TYPE="text" NAME="Feedback" VALUE="#Feedback#"></TD></TR>
		<TR><TD>Subject Line Only</TD><TD><CF_INPUT TYPE="text" NAME="SubjectLineOnly" VALUE="#SubjectLineOnly#"></TD></TR>
		<TR><TD>Active</TD><TD><CF_INPUT TYPE="text" NAME="Active" VALUE="#Active#"></TD></TR>
		<TR><TD>SortOrder</TD><TD><CF_INPUT TYPE="text" NAME="SortOrder" VALUE="#SortOrder#"></TD></TR>
		<CFIF isDefined("Addnew")>
			<TR><TD colspan=2><INPUT TYPE="Submit" NAME="Add" VALUE="Add"></TD></TR>
		<CFELSE>
			<TR><TD colspan=2><INPUT TYPE="Submit" NAME="Update" VALUE="Update"></TD></TR>
			<CF_INPUT TYPE="hidden" NAME="phraseid" VALUE="#phraseid#">
		</CFIF>
	</FORM>
	<CFELSE>
		<CFIF nCount mod 2 EQ 0>
			<CFSET nRow="evenRow">
		<CFELSE>
			<CFSET nRow="oddRow">
		</CFIF>
		<tr class="#nRow#"><td><A href="eMailErrorText.cfm&Editphraseid=#PhraseID#">#htmleditformat(Phrase)#</A></td><td>#htmleditformat(Action)#</td><td>#htmleditformat(CommStatus)#</td><td>#htmleditformat(Feedback)#</td><td>#htmleditformat(SubjectLineOnly)#</td><td>#htmleditformat(Active)#</td><td>#htmleditformat(sortorder)#</td></tr>
		<CFSET nCount= nCount + 1>

	</CFIF>
</cfoutput>

<!--- <tr>
	<td>Add a new phrase to list by typing it in the box below</td>
</tr>
<tr>
	<td><form method="POST">
			<input type="Text" name="NewErrorText">
			<input type="Submit" name="SubmitNewErrorText" value="Add">
		</form>
	</td>
</tr> --->
</table>




