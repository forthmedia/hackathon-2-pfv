<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			populateCalendarData.cfm	
Author:				SWJ
Date started:		2007-04-20
	
Description:		The beginnings of a utility to populate the calendar tables

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

Set a starting year
test what's already populated so we don't create duplicates

 --->



<cf_head>
	<cf_title>Setup Calendar Data</cf_title>
</cf_head>


<<cfif isdefined("form.calendarID") and isdefined("form.startingMonth")>

	<cfinvoke component="relay.com.relayCalendarFunctions" method="populateCalendarQuarters" returnvariable="qryCalendarQuarters">
		<cfinvokeArgument name="calendarID" value="#form.calendarID#">
		<cfinvokeArgument name="startingMonth" value="#form.startingMonth#">
		<cfif structKeyExists(form,"clearPreviousEntries")>
			<cfinvokeArgument name="clearPreviousEntries" value="true">
		</cfif>
	</cfinvoke>
	
	<cfinvoke component="relay.com.relayCalendarFunctions" method="getCalendarQuarters" returnvariable="qryCalendarQuarters"/>
	
	<cfdump var="#qryCalendarQuarters#">	
</cfif>

<cfform name="thisForm" method="post">
	<fieldset>
		<legend class="outer-legend">Populate Calendar Data</legend>
		<label for="calendarID">Calendar ID</label>
			<cfinput name="calendarID" value="1" tooltip="Enter the calendar ID" required="true" validate="integer">
		<label for="startingMonth">Starting Month</label>
			<cfinput name="startingMonth" value="1" tooltip="Enter the month number of the starting month of the year" required="true" validate="integer">
		<label for="clearPreviousEntries">Clear Down Previous Calendar Entries</label>
			<cfinput type="checkbox" name="clearPreviousEntries">
		<input type="submit">
	</fieldset>
</cfform>



