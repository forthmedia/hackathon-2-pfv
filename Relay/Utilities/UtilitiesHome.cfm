<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2009-02-24 		NJH		Bug Fix Demo Issue 1684 - removed the system check link as it's very old and not used anymore
2009-09-14 		NYB		LHID2630 - removed "Change flag settings for different partner types" - looks like this link has existed since v6 but the file it points to has never existed
2010-12-23		MS		LID:5182 - Fixed Broken link
2011-04-14 		NYB		LHID6271: added Undelete Organisation
2013-12-19 		NYB 	Case 436226 - pluralised the entity names, the files now handle processing of multiple ids

--->

<CFQUERY NAME="getTranslationRights" DATASOURCE="#application.SiteDataSource#">
	select bfd.entityid
	from booleanflagdata as bfd, flag as f , flaggroup as fg
	WHERE fg.flaggrouptextid='LanguageRights'
	and fg.flaggroupid=f.flaggroupid
	and f.flagid=bfd.flagid
	and bfd.entityid= #request.relayCurrentUser.personid#
</CFQUERY>

<cfset unmergeOrgTasks = "RWAdminTask:Level3;RWAdmin:Level3">
<cfset hasUnmergeRights = "false">

<cfloop list="#unmergeOrgTasks#" delimiters=";" index="i">
	<cfif application.com.login.checkInternalPermissions(listfirst(i,":"),listlast(i,":")) and not hasUnmergeRights>
		<cfset hasUnmergeRights = "true">
	</cfif>
</cfloop>

<CFOUTPUT>
		<!--- START: NYB 2009-09-14 - LHID2630 - removed.  Looks like this link has existed since v6 but the file it points to has never existed ---
		<strong>Internal Utilities and Reports</strong><BR>
		<UL>
			<li><a href="../admin/selectpartnergroup.cfm">Change flag settings for different partner types</a>
		</UL>
		!--- END: NYB 2009-09-14 - LHID2630 --->
		<!---2015-06-30 SB Removed tables and added list styling --->
		<div id="utilitiesHome">
			<ul id="settingsTable1">
				<li class="settings-level-0"><A HREF="/utilities/ErrorSummary.cfm">View Relay Errors</A></li>
				<!--- START: 2013-12-19 NYB Case 436226 - pluralised the entity names --->
				<li class="settings-level-0"><A HREF="/utilities/UndeleteLocation.cfm">Undelete Locations</A></li>
				<li class="settings-level-0"><A HREF="/utilities/UndeletePerson.cfm">Undelete People</A></li>
				<li class="settings-level-0"><A HREF="/utilities/UndeleteOrganisation.cfm">Undelete Organisations</A></li>
				<!--- END: 2013-12-19 NYB Case 436226 --->
				<cfif hasUnmergeRights>
				<li class="settings-level-0"><A HREF="/utilities/UndedupeOrganisation.cfm">Undedupe Organisation</A></li>
				</cfif>
				<li class="settings-level-0"><A HREF="/utilities/scheduledTaskMonitorReport.cfm">View Scheduled Tasks</A></li>
			</ul>
		</div>
		</CFOUTPUT>
