<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="true">
<!---
createUserNamePassword.cfm

Author  WAB

Date 2000-03-08

Purpose

Can be called with a list of personids to create usernames and passwords

SWJ 2003-02-20 Added the layout and ability to pass in a selectionID


 --->
<cfset application.com.request.setTophead(topHeadCfm="")>
<cfoutput><h2>User Names and Passwords</h2></cfoutput>

<cfif isDefined("frmSelectionID")>
	<CFQUERY NAME="getRecords" datasource="#application.siteDataSource#">
	SELECT p.PersonID
		FROM dbo.organisation o INNER JOIN
	         dbo.Location l ON o.OrganisationID = l.OrganisationID INNER JOIN
	         dbo.Selection s INNER JOIN
	         dbo.SelectionTag st ON s.SelectionID = st.SelectionID INNER JOIN
	         dbo.Person p ON st.EntityID = p.PersonID INNER JOIN
	         dbo.SelectionGroup sg ON s.SelectionID = sg.SelectionID
			inner join RightsGroup rg on rg.usergroupid = sg.usergroupid
			 ON l.LocationID = p.LocationID INNER JOIN
	         dbo.Country c ON l.CountryID = c.CountryID
		WHERE s.SelectionID  in ( <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		  AND (len(rtrim(p.username)) = 0  or len(rtrim(p.password)) = 0)
		  AND s.Tablename = 'Person'
		  AND p.active = 1
		  AND rg.personid = #request.relayCurrentUser.personid#
		  AND c.CountryID IN (SELECT r.CountryID
			  FROM Rights AS r
			  		inner join
				   RightsGroup AS rg on rg.UserGroupID = r.UserGroupID
				    inner join
				   UserGroup AS ug on rg.PersonID = ug.PersonID
				    inner join
				   Selection AS s on ug.UserGroupID = s.CreatedBy

			 WHERE s.SelectionID  in ( <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			   AND r.Permission > 0
			   AND r.SecurityTypeID =
						(SELECT e.SecurityTypeID
						 FROM SecurityType AS e
						 WHERE e.ShortName = 'RecordTask'))
	</cfquery>
	<cfset frmPersonID = valuelist(getRecords.personid)>
	<cfset relayCounter = 0>

	<cfif getRecords.recordCount gt 0>
		<cfsetting requesttimeout="#getRecords.recordCount#"  showdebugoutput=false>

		<cfoutput><p>User Names and passwords were created for: </p></cfoutput>
		<cfloop list="#frmPersonID#" index="personID">
			<!--- <cf_createUserNameAndPassword
				personid = "#personID#"
				resolveDuplicates = true
				OverWritePassword = false
				OverWriteUserName = false
			> --->
			<cfscript>
				getUserName = application.com.login.createUserNamePassword(personID);
			</cfscript>
			<cfset relayCounter = relayCounter + 1>
			<cfoutput>#getUserName.fullname# <br/>
			</cfoutput>
		</cfloop>

	<cfelse>
		<cfoutput><p>No User Names to be created</p></cfoutput>
	</cfif>


<cfelseif isDefined("frmPersonID")>
	<cfoutput><p>User name and passwords were created for: </p></cfoutput>
	<cfloop list="#frmPersonID#" index="personID">
		<!--- <cf_createUserNameAndPassword
			personid = "#personID#"
			resolveDuplicates = true
			OverWritePassword = false
			OverWriteUserName = false
		> --->
		<cfscript>
			getUserName = application.com.login.createUserNamePassword(personID);
		</cfscript>
		<cfoutput>#htmleditformat(getUserName.fullname)#<br/></cfoutput>
	</cfloop>
<cfelse>
	<cfoutput><p>You must provide either frmPersonID or frmSelectionID for this to work</p></cfoutput>
</cfif>
