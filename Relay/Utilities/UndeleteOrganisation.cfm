<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2011-04-14 		NYB		LHID6271: created
2011-05-13		NYB		LHID6552: changed to now also unflag org's flagged for deletion
2013-12-19 		NYB 	Case 436226 changed file to handle processing of multiple ids
2015-11-19		SB		Bootstrap
--->

<cf_title>Undelete Organisations</cf_title><!--- 2013-12-19 NYB Case 436226 --->

<cfset subsection = "Undelete Organisation">

	<!--- START: 2013-12-19 NYB Case 436226 - moved form from below --->
	<form method = "post">
		<div class="form-group">
			<label for="frmOrganisationIDs">Enter the ids of the Organisations to be undeleted</label>
			<INPUT type = "text" id="frmOrganisationIDs" name="frmOrganisationIDs" length="8" class="form-control">
		</div>
		<INPUT type= "submit" id="frmSubmit" class="btn btn-primary">
	</form>
	<!--- END: 2013-12-19 NYB Case 436226 --->

	<CFIF isDefined("frmOrganisationIDs")><!--- 2013-12-19 NYB Case 436226 - changed to frmOrganisationIDs --->
		<cfloop list="#frmOrganisationIDs#" index="frmOrganisationID"><!--- 2013-12-19 NYB Case 436226 - added loop --->
			<cfif isnumeric(frmOrganisationID)>
				<CFQUERY NAME="getOrganisation" DATASOURCE=#application.sitedatasource#>
					select * from OrganisationDel where OrganisationID = <cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no">
				</CFQUERY>
				<CFQUERY NAME="getDateDeleted" DATASOURCE=#application.sitedatasource# maxrows="1">
					select ModDate from modregister where action='OD' and recordid=<cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no"> order by id desc
				</CFQUERY>
			</cfif>

			<cfif isnumeric(frmOrganisationID) and getOrganisation.recordcount gt 0 AND getDateDeleted.recordcount gt 0>
				<cfset DeletedDate = getDateDeleted.ModDate>

				<cftransaction action="begin">
				<cftry>
					<CFQUERY NAME="unDelete" DATASOURCE=#application.sitedatasource#>
						exec undeleteOrganisation @OrganisationID = <cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no">
					</CFQUERY>

					<CFQUERY NAME="getOrganisation" DATASOURCE=#application.sitedatasource#>
						select * from Organisation where OrganisationID = <cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no">
					</CFQUERY>

					<cfif getOrganisation.recordCount gt 0>
						<CFQUERY NAME="getLocations" DATASOURCE=#application.sitedatasource#>
							select locationid from locationdel where organisationid=<cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no"> and locationid in (
								select recordid from modregister
								where moddate < dateadd(mi,10,cast(
								<cfqueryparam value = #DeletedDate# CFSQLType = "cf_sql_timestamp" null = "no">
								 as datetime))
								and moddate > dateadd(mi,-10,cast(
								<cfqueryparam value = #DeletedDate# CFSQLType = "cf_sql_timestamp" null = "no">
								 as datetime))
								and action='LD'
							)
						</CFQUERY>

						<cfoutput query="getLocations">
							<CFQUERY NAME="unDelete" DATASOURCE=#application.sitedatasource#>
								exec undeleteLocation @locationid = <cfqueryparam value = #Locationid# CFSQLType = "cf_sql_INTEGER" null = "no">
							</CFQUERY>
						</cfoutput>

						<!--- 2013-12-19 NYB Case 436226 - added frmOrganisationID --->
						<cfoutput>#htmleditformat(getOrganisation.OrganisationName)# (#frmOrganisationID#) undeleted successfully.<br /></cfoutput>

						<cftransaction action="commit"/>
					<cfelse>
						<!--- 2013-12-19 NYB Case 436226 added frmOrganisationID --->
						<cfoutput>Organisation #frmOrganisationID# not undeleted<br /></cfoutput>
						<cftransaction action="rollback"/>
					</cfif>
					<cfcatch type="any">
						<cftransaction action="rollback"/>
						<!--- 2013-12-19 NYB Case 436226 added frmOrganisationID --->
						<cfoutput>Organisation #frmOrganisationID# not undeleted.  An error occured during the process.<br /></cfoutput>
					</cfcatch>
				</cftry>
			<cfelse>
				<cfif isnumeric(frmOrganisationID) and application.com.flag.checkBooleanFlagByID(entityid=frmOrganisationID,flagid="DeleteOrganisation")>
					<CFQUERY NAME="getOrganisation" DATASOURCE=#application.sitedatasource#>
						select * from Organisation where OrganisationID = <cfqueryparam value = #frmOrganisationID# CFSQLType = "cf_sql_INTEGER" null = "no">
					</CFQUERY>
					<cfset application.com.flag.unsetBooleanFlag(entityid=frmOrganisationID,flagid="DeleteOrganisation")>
					<!--- 2013-12-19 NYB Case 436226 added frmOrganisationID --->
					<cfoutput>#getOrganisation.OrganisationName# (#frmOrganisationID#) has been unflagged for deletion<br /></cfoutput>
				<cfelse>
					<!--- 2013-12-19 NYB Case 436226 changed text --->
					<cfoutput><p>No Organisation with an id of #frmOrganisationID# found to undelete</p></cfoutput>
				</cfif>
			</cfif>

			<cf_flush><!--- 2013-12-19 NYB Case 436226 added --->

		</cfloop><!--- 2013-12-19 NYB Case 436226 added loop --->

	<!--- START: 2013-12-19 NYB Case 436226 moved and removed:
	<cfelse>
		<form method = "post">
		Enter the id of the Organisation to be undeleted <BR>
		<INPUT type = "text" name="frmOrganisationID" length = "8"><BR>
		<INPUT type= "submit" id="frmSubmit">
		</form>
	END: 2013-12-19 NYB Case 436226  --->

	</cfif>