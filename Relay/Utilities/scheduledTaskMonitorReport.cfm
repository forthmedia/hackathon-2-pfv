<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		scheduledTaskMonitorReport.cfm	
Author:			NJH  
Date started:	08-01-2009
	
Description:	A display of the scheduled task monitoring		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cf_translate>

<cfif request.relayCurrentUser.isLoggedIn>

	<!--- query the DB for some of the scheduled task information. Part of the information such as the interval, the task start time, etc come from the
		Cold Fusion admin scheduled task file.
	 --->
	<cfquery name="getScheduledTaskData" datasource="#application.siteDataSource#">
		select Name,emailClient as Send_Notification_Email,
			isNull(clientSupportEmail,<cfif isDefined("application.scheduledTaskSupportEmail")>'#application.scheduledTaskSupportEmail#'<cfelse>''</cfif>) as Support_Email_Address, url,
			(select max(startTime) from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID) as last_run,
			(select top 1 case when errorID is null then 0 else errorID end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID order by startTime desc) as last_errorID,
			(select top 1 case when errorID is null then 1 else 0 end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID order by startTime desc) as last_run_successful,
			(select case when count(*) = 0 then '-' else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-1,GetDate())) as executions_last_24_hours,
			(select case when count(*) = 0 then '-' else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-2,GetDate())) as Executions_Last_2_days,
			(select case when count(*) = 0 then '-' else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-7,GetDate())) as Executions_Last_7_days,
			(select case when count(*) = 0 then '-' else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-30,GetDate())) as Executions_Last_30_days,
			(select case when count(errorID) = 0 then null else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-1,GetDate()) and errorID is not null) as errors_last_24_hours,
			(select case when count(errorID) = 0 then null else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-2,GetDate()) and errorID is not null) as errors_Last_2_days,
			(select case when count(errorID) = 0 then null else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-7,GetDate()) and errorID is not null) as errors_Last_7_days,
			(select case when count(errorID) = 0 then null else cast(count(*) as varchar) end from scheduledTaskLog stl with (noLock) where stl.scheduledTaskDefID = std.scheduledTaskDefID and startTime >= dateAdd(day,-30,GetDate()) and errorID is not null) as errors_Last_30_days,
			null as paused,
			null as interval,
			null as start_Time,
			null as end_Time,
			null as next_expected_run
		from scheduledTaskDefinition std with (noLock)
	</cfquery>
	
	<cfset cfAdminTasksQry = application.com.relayAdmin.getCFScheduledTasks()>
	
	<cfloop query="getScheduledTaskData">
		<cfset templateURL=replace(getScheduledTaskData.url[currentRow],"http://","")>
		<cfset templateURL=replace(templateURL,"https://","")>	
		<cfset templateURL = replace(templateURL,left(templateURL,find("/",templateURL)),"")>
	
		<cfquery name="getCFScheduleData" dbtype="query">
			select distinct * from cfAdminTasksQry where url like '%#templateURL#'
		</cfquery>
		
		<!--- the only link between the scheduled task in the file and the scheduled task in the DB is the script name and parameters of 
			the page getting called --->
		<cfloop query="getCFScheduleData">
			<cfset urlHostName = replace(getCFScheduleData.url,"http://","")>
			<cfset urlHostName = replace(urlHostName,"https://","")>
			<cfset urlHostName = left(urlHostName,find("/",urlHostName)-1)>
		
			<cfif structKeyExists(application.siteDefIndex,urlHostName)>
				<cfset currentDBRow = getScheduledTaskData.currentRow>
				<cfset getScheduledTaskData.paused[currentDBRow]=getCFScheduleData.paused>
				<cfset getScheduledTaskData.interval[currentDBRow]=getCFScheduleData.interval>
				<cfset getScheduledTaskData.start_Time[currentDBRow]=getCFScheduleData.start_Time>
				<cfset getScheduledTaskData.end_Time[currentDBRow]=getCFScheduleData.end_Time>
				
				<!--- calculate the next expected run based on the last run, time and the interval --->
				<!--- the interval is in minutes --->
				<cfif isNumeric(getCFScheduleData.interval)>
					<!--- if the schedule has finished running for the day, then it's scheduled to run again tomorrow --->
					<cfif dateDiff("s",getScheduledTaskData.last_run,request.requestTime)>
						<cfset getScheduledTaskData.next_expected_run[currentDBRow]= "#dateFormat(dateAdd("d",1,request.requestTime),"yyyy-mm-dd")# #getScheduledTaskData.start_time#">
					<!--- otherwise it's at the next interval --->
					<cfelse>
						<cfset getScheduledTaskData.next_expected_run[currentDBRow]= dateAdd("n",getCFScheduleData.interval,getScheduledTaskData.last_run)>
					</cfif>
				<!--- if it's daily then it will be the next day of when it was last run. --->
				<cfelseif getCFScheduleData.interval eq "Daily">
					<cfset getScheduledTaskData.next_expected_run[currentDBRow]= dateAdd("d",1,getScheduledTaskData.last_run)>
				<!--- if it's only run once and it hasn't run yet --->
				<cfelseif getCFScheduleData.interval eq "Once">
					<cfif getScheduledTaskData.last_run eq "">
						<cfset getScheduledTaskData.next_expected_run[currentDBRow] = "#dateFormat(getCFScheduleData.start_date,"yyyy-mm-dd")# #getScheduledTaskData.start_time#">
					<!--- otherwise it's finished running --->
					<cfelse>
						<cfset getScheduledTaskData.next_expected_run[currentDBRow] = "Finished">
					</cfif>
				</cfif>
				
				<cfif isNumeric(getCFScheduleData.interval)>
					<cfset getScheduledTaskData.interval[currentDBRow] = "#getScheduledTaskData.interval[currentDBRow]# minutes">
				</cfif>
				
				<cfbreak>
			</cfif>
		</cfloop>
	</cfloop>
	
	<cf_tableFromQueryObject
		queryObject="#getScheduledTaskData#"
		useInclude="false"
		showTheseColumns="Name,Interval,Start_Time,End_Time,Paused,Last_Run,Last_Run_Successful,Next_Expected_Run,Executions_Last_24_Hours,Errors_Last_24_Hours,Executions_Last_2_days,Errors_Last_2_Days,Executions_Last_7_days,Errors_Last_7_Days,Send_Notification_Email,Support_Email_Address"
		booleanFormat="Send_Notification_Email,Last_Run_Successful,Paused"
		dateTimeFormat="Last_Run,Next_Expected_Run"
		keyColumnList="Errors_Last_24_Hours,Errors_Last_2_Days,Errors_Last_7_Days,Errors_Last_30_Days"
		keyColumnURLList="/errorHandler/errorDetails.cfm?errorID=,/errorHandler/errorDetails.cfm?errorID=,/errorHandler/errorDetails.cfm?errorID=,/errorHandler/errorDetails.cfm?errorID="
		keyColumnKeyList="last_errorID,last_errorID,last_errorID,last_errorID"
		keyColumnOpenInWindowList="yes,yes,yes,yes"
	>

</cfif>





</cf_translate>
