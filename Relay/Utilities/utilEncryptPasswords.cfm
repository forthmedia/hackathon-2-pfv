<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Filename: utilEncryptPassword.cfm

Notes: This utility is to convert the password on the person table from plain text to encrypted 
It uses the Security.Encryption Methods.relaypassword.method setting so set it BEFORE running (typically "passwordHash")
You can send in a url param rowsToProcess to test a couple first or do a bigger batch 
On FTF environment it took roughly 4.3 mins per 5000 rows
--->

<cfparam name="rowsToProcess" default="10000">
<cfsetting requesttimeout="36000">	<!--- 10 hours --->

<cf_relayHeaderFooter>


<cfoutput>
<cfset StartTime=now()>
Start Time: #StartTime#<br />
</cfoutput>

<cfdump var="Started #now()#" label="debug" output="d:\web\encryptPasswordErrors.html" format="html">


<cfquery name="qryPersons" datasource="#application.siteDataSource#">
	SELECT TOP #rowsToProcess# personId, username, password 
	FROM PERSON 
	WHERE LEN(PASSWORD) <> 32 AND LEN(PASSWORD) <> 0
	ORDER BY LastUpdated DESC
</cfquery>

<cfloop query="qryPersons">
	<cfset encryptedPassword = application.com.login.encryptRelayPassword(username=qryPersons.UserName,password=qryPersons.Password)>
	<cftry>
		<cfquery name="qryUpdatePerson" datasource="#application.siteDataSource#">
			UPDATE person SET password =  <cf_queryparam value="#encryptedPassword#" CFSQLTYPE="CF_SQL_VARCHAR" > WHERE personid =  <cf_queryparam value="#qryPersons.personId#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>
		
		<cfcatch type="any">
			<cfset errorString = "personid=#qryPersons.personId# ">
			<cfif StructKeyExists(cfcatch,"detail")>
				<cfset errorString = errorString & #cfcatch.detail#>
			</cfif>
			<cfdump var="#errorString#" label="debug" output="d:\web\encryptPasswordErrors.html" format="html">
		</cfcatch>
	</cftry>
</cfloop>

<cfoutput>
	Processed #qryPersons.recordcount# rows<br />

	<cfset EndTime=now()>

	End Time: #EndTime#<br />
	Elapsed Time (mins) = #(EndTime - StartTime)*60*24#<br /><br />
	
	Check file d:\web\encryptPasswordErrors.html for errors; please delete this file after use.<br />
</cfoutput>


</cf_relayHeaderFooter>