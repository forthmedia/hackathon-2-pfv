<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Template to display what an address will look like for any particular country
either pass in a locationid (or list)
or pass in a list of countries 

 --->

<cfif isdefined("locationid")> 
	<cfquery name="myQuery" datasource="#application.sitedatasource#">
			select * from location where locationid  in ( <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>
	
	
	<cfoutput query = "myQuery">
		<B>#application.CountryName[countryid]#</B> <BR>
		
		#application.com.screens.evaluateAddressFormat (location = myQuery)#<BR><BR>
		#application.com.screens.evaluateAddressFormat (location = myQuery, separator = "<BR>", finalcharacter="")#<BR><BR>
		
	</cfoutput>

<cfelse>

	<cfquery name="myQuery" datasource="#application.sitedatasource#">
		select 'Address1' as address1,'Address2' as address2,'Address3' as address3,'Town' as address4,'Region' as address5,'postalcode' as postalcode,
				'Address6' as address6,'Address7' as address7,'Address8' as address8,'Address9' as address9,
				'nAddress1' as naddress1,'nAddress2' as naddress2,'nAddress3' as naddress3,'nTown' as naddress4,'nRegion' as naddress5
	</cfquery>
	
	<cfquery name="countries" datasource="#application.sitedatasource#" >
	select * from country where 
	isnull(isocode,'') <> ''
	<cfif isDefined("countryid")>
	and countryid  in ( <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	</cfquery>
	
	
	<cfoutput query = "countries" maxrows=40>
		<B>#htmleditformat(countrydescription)#</B> <BR>
		
		#application.com.screens.evaluateAddressFormat (location = myQuery, countryid = countryid)#<BR><BR>
		#application.com.screens.evaluateAddressFormat (location = myQuery, countryid = countryid, separator = "<BR>", finalcharacter="")#<BR><BR>
		
	</cfoutput>
	
	
</cfif>
 
 
