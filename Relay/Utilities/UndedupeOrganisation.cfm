<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Very Rough template for undeduping an organisation

NYB 2010-12-16
WAB 2014-10-07   CASE 442010  Deal with conflict with triggers designed to keep person.organisationid and location.organisationid in Synch
--->
<cfsetting requesttimeout="9600">

<cf_title>Undedupe Organisation</cf_title>

<cfset message = "">
<cf_relayFormDisplay>
		<CFIF isDefined("frmOrgID")>
			<CFQUERY NAME="getDedupes" DATASOURCE="#application.sitedatasource#">
				select distinct ArchivedID from dedupeAudit
				where [newId] =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >  and archivedid ! =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and RecordType = 'Organisation'
			</CFQUERY>

			<cfif isDefined("url.debug")>
				<p>getDedupes=<br/><cfdump var="#getDedupes#"></p>
			</cfif>

			<cfif getDedupes.recordCount>
				<cftransaction>
					<cftry>
						<cfloop query = "getDedupes">
								<!--- undelete Org - recovers deleted flags from modregister as part of procedure --->
								<CFQUERY NAME="unDelete" DATASOURCE=#application.sitedatasource#>
									exec undeleteorganisation @organisationid =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</CFQUERY>

								<CFQUERY NAME="getOrganisation" DATASOURCE=#application.sitedatasource#>
									select * from Organisation where organisationid =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</CFQUERY>

								<cfif getOrganisation.recordCount is not 0>

								<cfquery name="getdeleteDate" DATASOURCE=#application.sitedatasource#>
									select	max(moddate) as dupeCreated --max in case previously been deleted
								  	from modregister m
									where action='OD'
									and recordid =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</cfquery>

									<!--- get all non flag changes made --->
									<CFQUERY NAME="otherModChanges" DATASOURCE="#application.sitedatasource#">
										select med.tableName,med.FieldName,mr.*
										from modregister mr
										inner join modentitydef med on mr.modentityid=med.ModEntityID
										where
										modDate >= DATEADD (mi, -60, '#getDeleteDate.dupeCreated#')
											<!--- PJP 10/12/2012 CASE: 432397: Changed the comment to use coldfusion comment tags, i think this was causing an issue?
											(
											select min(modDate) from modregister where action='LM' and newVal =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and oldVal =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_VARCHAR" >
											and modDate > DATEADD (mi, -60, '#getDeleteDate.dupeCreated#')
											)
											--->
										and
										modDate <=  <cf_queryparam value="#getDeleteDate.dupeCreated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
											<!--- PJP 10/12/2012 CASE: 432397: Changed the comment to use coldfusion comment tags, i think this was causing an issue?
											(
											select min(modDate) from modregister where action='OD' and RecordID =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_INTEGER" >
											and modDate > DATEADD (mi, -60, '#getDeleteDate.dupeCreated#')
											)
											--->
										and ((newVal =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and OldVal =  <cf_queryparam value="#ArchivedID#" CFSQLTYPE="CF_SQL_VARCHAR" > )
										or (action='FA' and RecordID =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
										order by
										<!--- WAB 2014-10-07   CASE 442010  Deal with conflict with triggers designed to keep person.organisationid and location.organisationid in Synch
											This order by makes usure that location.organisationID is updated first, the triggers will then automatically update person.organisationid
										--->
											  case when tableName = 'location' and fieldname = 'organisationid' then 0 else 1 end,
											modDate
									</CFQUERY>

									<cfif isDefined("url.debug")>
										<p>otherModChanges=<br/><cfdump var="#otherModChanges#"></p>
									</cfif>

									<cfloop query = "otherModChanges">
										<cfif action eq "FA">
											<CFSET application.com.flag.deleteFlagData(flagId=FLAGID,entityid=frmOrgID)>
										<cfelse>
											<CFQUERY NAME="unDelete" DATASOURCE=#application.sitedatasource#>
												update #tableName#
												set #FieldName# =  <cf_queryparam value="#OldVal#" CFSQLTYPE="CF_SQL_VARCHAR" >
												where #tableName#id =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >
												and #FieldName# <>  <cf_queryparam value="#OldVal#" CFSQLTYPE="CF_SQL_VARCHAR" >
											</CFQUERY>
										</cfif>
									</cfloop>
								</cfif>
						</cfloop>

						<cftransaction action="commit" />

						<cfset message = "Organisation id:#frmOrgID# has been processed.  The organisations with the following ids have successfully been removed:  #ValueList(getDedupes.ArchivedID)#.">

						<cfcatch type="any">
							<cftransaction action="rollback" />
								<cfdump var="#cfcatch#">
							<cfset message = "Error occured in process">
						</cfcatch>
					</cftry>
				</cftransaction>
			<cfelse>
				<cfset message = "No dupes found for organisationID #frmOrgID#">
			</cfif>
		</cfif>


		<cfform name="undedupeOrg"  method="post">
			<cf_relayFormElementDisplay relayFormElementType="message" fieldname="message" currentValue="#message#" label="">
			<cf_relayFormElementDisplay relayFormElementType="text" required="true" label="Organisation IDs from which to have dupes removed" fieldname="frmOrgID" currentValue="" length="8">
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSubmit" currentValue="" label="">
		</cfform>


</cf_relayFormDisplay>