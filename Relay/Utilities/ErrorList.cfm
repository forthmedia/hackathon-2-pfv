<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		errorList.cfm
Author:			SWJ
Date created:	

Description:	Lists the usage of the system in the last 30 days

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-12-02			AJC			Cross site scripting attack fix
--->

<CFSET REPORTBEGINDATE=NOW()-30>
<CFSET REPORTENDDATE=NOW()>
<CFSET MINLOGINS=3>
<CFPARAM NAME="frmSortOrder" DEFAULT="UserName ASC">
<CFPARAM NAME="frmTask" DEFAULT="analysis">


<CFIF frmTask eq "analysis">
	<CFQUERY NAME="GetErrors" DATASOURCE="#application.SiteDataSource#">
		select Template, count(*)as numbOcc,max(errorDateTime) as mostRecentOcc,min(errorDateTime) as firstOcc
		from relayError 
		group by template
		ORDER By mostRecentOcc Desc
	</CFQUERY>
	<CFQUERY NAME="GetTotalErrors" DATASOURCE="#application.SiteDataSource#">
		select count(*)as totalOcc,max(errorDateTime) as mostRecentOcc,min(errorDateTime) as firstOcc
		from relayError
	</CFQUERY>
<CFELSE>
	<CFQUERY NAME="GetErrors" DATASOURCE="#application.SiteDataSource#">
		SELECT * 
		FROM vRelayErrors
		Where 1=1
		<CFIF isDefined("frmID")> and RelayErrorID =  <cf_queryparam value="#frmID#" CFSQLTYPE="CF_SQL_INTEGER" > </CFIF>
		<CFIF isDefined("frmTemplate")> and template =  <cf_queryparam value="#frmTemplate#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>
		ORDER By ErrorDateTime Desc
	</CFQUERY>
</CFIF>

<CFIF frmTask eq "list">
	<CFQUERY NAME="GetErrorInstances" DATASOURCE="#application.SiteDataSource#">
	select left(diagnostics,400) as errorInstance,count(*)as numbOcc,max(errorDateTime) as mostRecentOcc,min(errorDateTime) as firstOcc
		from relayError 
	Where 1=1
		<CFIF isDefined("frmTemplate")> and template =  <cf_queryparam value="#frmTemplate#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>
		group by left(diagnostics,400)
		ORDER By mostRecentOcc Desc
	</CFQUERY>
</CFIF>


<cf_title>Error List</cf_title>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
			Relay Error Tracking
		</TD>
		
			<TD ALIGN="right" VALIGN="top" CLASS="submenu">
				<A HREF="errorSummary.cfm" Class="Submenu">Error Summary</A> | 
				<CFIF frmTask eq "list" or frmTask eq "detail">
				<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
				</CFIF>
			</TD>
		
	</TR>
</table>

<CFIF frmTask EQ "Analysis">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH>Template</TH>
		<TH>Number<br>of occurrences</TH>
		<TH>Most recent occurrence</TH>
		<TH>First Occurrence</TH>
	</TR>
	<CFOUTPUT QUERY="GetErrors">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<!--- START: AJC 2008-12-02 - Cross site scripting attack fix --->
			<TD><A HREF="/utilities/ErrorList.cfm?frmTemplate=#htmleditformat(template)#&frmTask=list">#htmleditformat(Template)#</A></TD>
		 	<!--- END: AJC 2008-12-02 - Cross site scripting attack fix --->
		 	<TD ALIGN="CENTER">#htmleditformat(numbOcc)#</TD>
			<TD>#dateformat(mostRecentOcc,"dd-mmm-yy")# (#timeformat(mostRecentOcc,"HH:MM")#)</TD>
			<TD>#dateformat(firstOcc,"dd-mmm-yy")# (#timeformat(firstOcc,"HH:MM")#)</TD>
		</TR>	
	</CFOUTPUT>
	<CFOUTPUT QUERY="GetTotalErrors">
		<TR>
			<TH><B>Summary</B></TH>
		 	<TH><B>#htmleditformat(totalOcc)#</B></TH>
			<TH><B>#dateformat(mostRecentOcc,"dd-mmm-yy")# (#timeformat(mostRecentOcc,"HH:MM")#)</B></TH>
			<TH><B>#dateformat(firstOcc,"dd-mmm-yy")# (#timeformat(firstOcc,"HH:MM")#)</B></TH>
		</TR>	
	</CFOUTPUT>
</TABLE>

<CFELSEIF frmTask EQ "List">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TH WIDTH="150">Username <br>company</TH>
		<TH>Date</TH>
		<TH>Template</TH>
		<TH>Referer</TH>
	</TR>
	<CFOUTPUT QUERY="GetErrors">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD WIDTH="100" VALIGN="top"><A HREF="/utilities/ErrorList.cfm?frmID=#RelayErrorID#&frmTask=Detail"><CFIF firstName eq "">Unknown User<CFELSE>#htmleditformat(firstName)# #htmleditformat(LastName)# <br>#htmleditformat(sitename)#</CFIF></A></TD>
		 	<TD ALIGN="CENTER" VALIGN="top">#dateformat(ErrorDateTime,"dd-mmm-yy")# (#timeformat(ErrorDateTime,"HH:MM")#)</TD>
			<TD WIDTH="400">#left(htmleditformat(diagnostics),400)#</TD>
			<TD VALIGN="top">#htmleditformat(Referer)#</TD>
		</TR>	
	</CFOUTPUT>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD colspan="4"></TD>
	</TR>
	<TR>
		<TH WIDTH="150">Error instance</TH>
		<TH>Count</TH>
		<TH>Most recent occurrence</TH>
		<TH>First Occurrence</TH>
	</TR>
	<CFOUTPUT QUERY="GetErrorInstances">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<!--- START: AJC 2008-12-02 - Cross site scripting attack fix --->
			<TD WIDTH="500" VALIGN="top">#htmleditformat(errorInstance)#</TD>
			<!--- END: AJC 2008-12-02 - Cross site scripting attack fix --->
		 	<TD ALIGN="CENTER"VALIGN="top">#htmleditformat(numbOcc)#</TD>
			<TD VALIGN="top">#dateformat(mostRecentOcc,"dd-mmm-yy")# (#timeformat(mostRecentOcc,"HH:MM")#)</TD>
			<TD VALIGN="top">#dateformat(firstOcc,"dd-mmm-yy")# (#timeformat(firstOcc,"HH:MM")#)</TD>
		</TR>	
	</CFOUTPUT>

</TABLE>

<CFELSEIF frmTask EQ "Detail">
<TABLE>
	<CFOUTPUT QUERY="GetErrors">
	<TR><TD CLASS="label">Person</TD><TD><CFIF firstName eq "">Unknown User<CFELSE>#htmleditformat(firstName)# #htmleditformat(LastName)# (#htmleditformat(sitename)#)</CFIF></TD></TR>
	<TR><TD CLASS="label">Date/Time</TD><TD>#dateformat(ErrorDateTime,"dd-mmm-yy")# (#timeformat(ErrorDateTime,"HH:MM")#)</TD></TR>
	<TR><TD CLASS="label">RemoteAddress</TD><TD>#htmleditformat(RemoteAddress)#</TD></TR>
	<TR><TD CLASS="label">Template</TD><TD>#htmleditformat(Template)#</TD></TR>
	<TR><TD CLASS="label">Referer</TD><TD>#htmleditformat(Referer)#</TD></TR>
	<TR><TD CLASS="label">Browser</TD><TD>#htmleditformat(Browser)#</TD></TR>
	<TR><TD COLSPAN="2" CLASS="label">Diagnostics</TD></TR>
	<!--- START: AJC 2008-12-02 - Cross site scripting attack fix --->
	<TR><TD COLSPAN="2">#htmleditformat(Diagnostics)#</TD></TR>
	<!--- END: AJC 2008-12-02 - Cross site scripting attack fix --->
	</CFOUTPUT>
</TABLE>
</CFIF>






