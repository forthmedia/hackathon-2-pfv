<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

deleteFlaggedPeople.cfm

Template to run the deleteFlaggedPeople stored procedure 
brings back information on who has been deleted, and more importantly on who hasn't been deleted and why.

designed to be run either by a user or as a scheduled process 

If run as a scheduled process, it should send an email to support if there are any 'suspicious deletions' which need looking after

Note that I imagine the stored procedure running on a SQL scheduled process on a regular basis (say hourly) 
and this template just running once per day in order to notify support of any deletion problems

WAB 2004-10-25
2008-03-19 altered to use deleteFlaggedOrganisations sp

NJH 2008-10-15 CR-TND562 put the content of the email into a variable so that it could be logged into the scheduled task log
WAB 2010-10-12 Removed references to application.internalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalInternalDomain()
WAB 2011-01-27 Altered so that large organisation batches could be deleted - needed to alter sp to bring back name of user
--->

<CFPARAM name="scheduledProcess" default = "false">
<CFPARAM name="test" default = "0">
<CFPARAM name="overRideDateString" default = "">
<CFPARAM name="overRideDateStringOrganisation" default = "">
<CFPARAM name="timelimit" default = "1">

<cf_ascreenupdate> 


<CFSTOREDPROC PROCEDURE="deleteFlaggedPeople" DATASOURCE="#application.siteDataSource#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INT" DBVARNAME="@TimeLimit" VALUE="#timelimit#" null="no">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INT" DBVARNAME="@Test" VALUE="#test#" null="no">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_varchar" DBVARNAME="@overRideDatestring" VALUE="#overRideDateString#" >   
			
			<CFPROCRESULT NAME="Deleted" RESULTSET="1"> 
	 		<CFPROCRESULT NAME="NotDeleted" RESULTSET="2"> 
			<CFPROCRESULT NAME="SuspiciousDeletions" RESULTSET="3"> 

</CFSTOREDPROC>			


	<CFSTOREDPROC PROCEDURE="DeleteFlaggedOrganisations" DATASOURCE="#application.siteDataSource#" >
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INT" DBVARNAME="@TimeLimit" VALUE="#timelimit#" null="no">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INT" DBVARNAME="@Test" VALUE="#test#" null="no">
			<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_varchar" DBVARNAME="@overRideDatestring" VALUE="#overRideDateStringOrganisation#" >   
			
			<CFPROCRESULT NAME="LocsDeleted" RESULTSET="1"> 
	 		<CFPROCRESULT NAME="OrgsDeleted" RESULTSET="2"> 
			<CFPROCRESULT NAME="OrgsNotdeleted" RESULTSET="3"> 
	
	</CFSTOREDPROC>		

	<cfquery name="LargeOrganisationBatches" DBTYPE="QUERY">
	SELECT LASTUPDATED,lastupdatedby, deletedbyname, COUNT(1) as organisationsDeleted
	FROM ORGSNOTDELETED
	WHERE REASON = 'Too Many Organisations in Request' 
	GROUP BY LASTUPDATED,lastupdatedby, deletedbyname
	</cfquery>

		

<cfquery name="getOrgDeleteFlag" datasource="#application.sitedatasource#">
select * from flag where flagtextid = 'deleteOrganisation'
</cfquery>


<cfquery name="getDeleteFlag" datasource="#application.sitedatasource#">
select * from flag where flagtextid = 'deletePerson'
</cfquery>

<cfoutput>

<form action="" method="post" name="myForm" >
<CF_INPUT type="hidden" name="frmPersonFlagList" value="checkbox_#getDeleteFlag.flaggroupid#">
<input type="hidden" name="frmTableList" value="person">
<input type="hidden" name="frmPersonFieldList" value="">

<TABLE>
		
		<cfif deleted.recordcount is not 0>
			<TR><TD colspan="4"><B>The following records <cfif test is not 1>have been<cfelse>will be</cfif> deleted </B></TD></TR>
			<CFLOOP query = "deleted">
				<TR><td></td><TD>#htmleditformat(personid)#</TD><td>#htmleditformat(firstname)# #htmleditformat(lastname)#</td></TR>
			</CFLOOP>
		</cfif>
		<cfif notdeleted.recordcount is not 0>
			<TR><TD colspan="4"><B>The following people <cfif test is not 1>have not been<cfelse>will not be</cfif> deleted </B></TD></TR>
			<CFLOOP query = "notdeleted">
				<TR><td><CF_INPUT type="checkbox" name="checkbox_#getDeleteFlag.flaggroupid#_#personid#" value="#getdeleteflag.flagid#" checked></td>
				<CF_INPUT type="hidden" name="checkbox_#getDeleteFlag.flaggroupid#_#personid#_orig" value="#getdeleteflag.flagid#" ><CF_INPUT type="hidden" name="frmPersonIDList" value = "#personid#">
				<TD>#htmleditformat(personid)#</TD><td>#htmleditformat(firstname)# #htmleditformat(lastname)#</td><TD>#htmleditformat(reason)#</TD><TD>#htmleditformat(extendedInfo)#</TD></TR>
			</CFLOOP>
		<TR><TD COLSPAN="4"><input type=submit VALUE="Uncheck checkboxes and click here to remove person from deletion list"></td></tr>
		<TR><TD COLSPAN="4"></td></tr>		
		</cfif>
		
		<cfif SuspiciousDeletions.recordcount is not 0>
			<TR><TD colspan="4"><B>The following batches are very large and have not been deleted</B></TD></TR>		
			<TR><TD colspan="4">If you are happy to delete these batches, click on the link</TD></TR>		
			<CFLOOP query = "SuspiciousDeletions">
				<TR>
				<TD>#htmleditformat(peopleDeleted)# by #htmleditformat(deletedBy)# </td><TD>#htmleditformat(whendeleted)#</TD><TD><A HREF="/utilities/deleteFlaggedPeople.cfm?overRideDateString=#whendeleted#">delete</A></TD></TR>
			</cfloop>	
		</cfif>
		
</TABLE>	
</form>		
</cfoutput>
	

<cfif getOrgDeleteFlag.recordcount gt 0>


<cfoutput>

	<form action="" method="post" name="myForm" >
	<CF_INPUT type="hidden" name="frmOrganisationFlagList" value="checkbox_#getOrgDeleteFlag.flaggroupid#">
	<input type="hidden" name="frmTableList" value="organisation">
	<input type="hidden" name="frmOrganisationFieldList" value="">
	
	<TABLE>
			
			<cfif OrgsDeleted.recordcount is not 0>
				<TR><TD colspan="4"><B>The following Organisations <cfif test is not 1>have been<cfelse>will be</cfif> deleted </B></TD></TR>
				<CFLOOP query = "OrgsDeleted">
					<TR><td></td><TD>#htmleditformat(organisationID)#</TD><td>#htmleditformat(organisationname)#</td></TR>
				</CFLOOP>
			</cfif>
</cfoutput>
			<cfif OrgsNotdeleted.recordcount is not 0>
				<TR><TD colspan="4"><B>The following records <cfif test is not 1>have not been<cfelse>will not be</cfif> deleted </B></TD></TR>
				<CFoutput query = "OrgsNotdeleted" group="organisationid">
					<TR><td><CF_INPUT type="checkbox" name="checkbox_#getOrgDeleteFlag.flaggroupid#_#organisationID#" value="#getOrgDeleteFlag.flagid#" checked></td>
					<CF_INPUT type="hidden" name="checkbox_#getOrgDeleteFlag.flaggroupid#_#organisationID#_orig" value="#getOrgDeleteFlag.flagid#" ><CF_INPUT type="hidden" name="frmOrganisationIDList" value = "#organisationID#">
					<TD>#htmleditformat(organisationID)#</TD><td>#htmleditformat(organisationname)#</td><TD><cfoutput>#htmleditformat(reason)#<BR></cfoutput></TD></TR>
				</CFOUTpUT>
			<TR><TD COLSPAN="4"><input type=submit VALUE="Uncheck checkboxes and click here to remove Organisations from deletion list"></td></tr>
			<TR><TD COLSPAN="4"></td></tr>		
			</cfif>
<cfoutput>
		<cfif LargeOrganisationBatches.recordcount is not 0>
			<TR><TD colspan="4"><B>The following batches are very large and have not been deleted</B></TD></TR>		
			<TR><TD colspan="4">If you are happy to delete these batches, click on the link</TD></TR>		
			<CFLOOP query = "LargeOrganisationBatches">
				<TR>
				<TD>#htmleditformat(organisationsDeleted)# by #htmleditformat(deletedByName)# </td><TD>#htmleditformat(lastupdated)#</TD><TD><A HREF="/utilities/deleteFlaggedPeople.cfm?overRideDateStringOrganisation=#lastupdated#">delete</A></TD></TR>
			</cfloop>	
		</cfif>
</cfoutput>			

			

	
	<!--- 		<cfoutput>#x.y#
			
			<BR>#x.lastupdated#
			<BR>#x.x#
			</cfoutput>		
	 --->		
	
	
	</TABLE>	
	</form>	

</cfif>
	



<cfif scheduledProcess and (notDeleted.recordcount is not 0  or (getOrgDeleteFlag.recordcount gt 0 and OrgsNotdeleted.recordcount is not 0))>
	
	<!--- NJH 2008-10-15 CR-TND562 put the content into a variable so that it could be logged into the scheduled task log --->
	<cfsavecontent variable="mailBody_html">
		<cfoutput>
			<p>The following entities have been flagged for deletion but have not been deleted 	
			Please check!</p>
			<p>goto <A href="#application.com.relayCurrentSite.getReciprocalInternalProtocolAndDomain()#/utilities/deleteFlaggedPeople.cfm">#htmleditformat(application.com.relayCurrentSite.getReciprocalInternalProtocolAndDomain())#/utilities/deleteFlaggedPeople.cfm</A> for more info</p>
			
			<cfif notDeleted.recordcount is not 0>
				<cfloop query="notDeleted">#htmleditformat(personid)#  #htmleditformat(firstname)# #htmleditformat(lastname)#  #htmleditformat(reason)# #htmleditformat(extendedInfo)#<br></cfloop>
			</cfif>
			
			<cfif SuspiciousDeletions.recordcount is not 0>
				<br>The following batches are suspicious and have not been deleted<br><br>
				<CFLOOP query = "SuspiciousDeletions">#htmleditformat(peopleDeleted)# by #htmleditformat(deletedBy)# #htmleditformat(whendeleted)#<br></cfloop>
			</cfif>
			
			<cfif getOrgDeleteFlag.recordcount gt 0 and OrgsNotdeleted.recordcount is not 0>
					<CFLOOP query = "OrgsNotdeleted">#htmleditformat(organisationid)#  #htmleditformat(organisationname)#  #htmleditformat(reason)#<br></cfloop>
				</cfif>
		</cfoutput>
	</cfsavecontent>

	<cf_mail type="html"
		to="#application.helpalertmailbox#" 
		from="#application.AdminPostmaster#" 
		subject="#request.CurrentSite.Title# Deletion Problem" >
		<cfoutput>
		#mailBody_html#
		</cfoutput>
	<!--- <p>The following entities have been flagged for deletion but have not been deleted 	
Please check!</p>
<p>goto <A href="#request.currentSite.httpProtocol##listfirst(application. internal userdomains)#/utilities/deleteFlaggedPeople.cfm">#request.currentSite.httpProtocol##listfirst(application.internaluserdomains)#/utilities/deleteFlaggedPeople.cfm</A> for more info</p>

<cfif notDeleted.recordcount is not 0>
		<cfoutput>#personid#  #firstname# #lastname#  #reason#<br></cfoutput>
<cfif SuspiciousDeletions.recordcount is not 0>
<br>The following batches are suspicious and have not been deleted<br><br>
<CFLOOP query = "SuspiciousDeletions">#peopleDeleted# by #deletedBy# #whendeleted#<br>
			</cfloop>
		</cfif>
</cfif>
<cfif getOrgDeleteFlag.recordcount gt 0 and OrgsNotdeleted.recordcount is not 0>
		<CFLOOP query = "OrgsNotdeleted">#organisationid#  #organisationname#  #reason#<br></cfloop>
		</cfif> --->
</cf_mail>

</cfif>

