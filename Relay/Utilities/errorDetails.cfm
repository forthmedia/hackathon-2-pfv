<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			errorSummary.cfm	
Author:				SWJ
Date started:		2005
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-08-26 SWJ Added the PeopleWithErrors query

Possible enhancements:


 --->

<cfparam name="URL.SendEmail" type="boolean" default="false">


<CFQUERY NAME="GetErrorInstances" DATASOURCE="#application.SiteDataSource#">
	select template,left(diagnostics,400) as error_Instance,
		count(*) as Total_Occurances,
		max(errorDateTime) as Most_Recent_Occurrance,min(errorDateTime) as First_Occurance
		from relayError 
	Where template in (select distinct template 
			from relayError where relayErrorFixID is NULL)
			<CFIF isDefined("frmTemplate")> and template =  <cf_queryparam value="#frmTemplate#" CFSQLTYPE="CF_SQL_VARCHAR" > </CFIF>
			<CFIF isDefined("frmID")> and RelayErrorID =  <cf_queryparam value="#frmID#" CFSQLTYPE="CF_SQL_INTEGER" > </CFIF>
		group by template,left(diagnostics,400)
		ORDER By template,Most_Recent_Occurrance Desc
</CFQUERY>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top" CLASS="submenu">
			Relay Recent Error Summary
		</TD>
		<TD ALIGN="right" VALIGN="top" CLASS="submenu">
			<A HREF="errorSummary.cfm" Class="Submenu">Error Summary</A> | 
			<A HREF="errorDetails.cfm?SendEmail=true" Class="Submenu">Send email</A> |
			<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>
		</TD>
	</TR>
</table>

<cfsaveContent variable="reportContent_html">
	<cfoutput>
		<cfif fileexists("#application.paths.content#\styles\relayWareStyles.css")>
			<cfhtmlhead text='<LINK REL="stylesheet" HREF="/code/styles/relayWareStyles.css">'>
		<cfelse>
			<cfhtmlhead text='<LINK REL="stylesheet" HREF="/Styles/relayWareStyles.css">'>
		</cfif>
	</cfoutput>
	
	<h2>Error details this month</h2>
	<CF_tableFromQueryObject queryObject="#GetErrorInstances#" 
		
		HidePageControls="no"
		useInclude = "false"
		showTheseColumns="Template,Error_Instance,Total_Occurances,Most_Recent_Occurrance,First_Occurance"
		sortorder="most_Recent_Occurance Desc">

</cfsavecontent>	


<cfoutput>#reportContent_html#</cfoutput>

<cfif URL.SendEmail>
	<cfmail to="errorAdmin@foundation-network.com"
        from="#application.AdminPostmaster#"
        subject="#request.CurrentSite.Title# Error Summary"
        type="HTML">
#reportContent_html#
	</cfmail>	
</cfif>