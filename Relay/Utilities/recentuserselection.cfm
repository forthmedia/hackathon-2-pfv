<!--- �Relayware. All Rights Reserved 2014 --->
<!--- updates selection of recent 3Com Users --->






<cfquery name="recentUsers" datasource="#application.sitedatasource#">
select distinct us.personid
from usage as us, person as p, location as l
where us.personid = p.personid
and p.locationid = l.locationid
and p.active <> 0
and l.active <> 0
and ltrim(p.password) <> ''
and  p.LoginExpires >= #CreateODBCDateTime(Now())#
and logindate > #CreateODBCDateTime(Now()-90)#
</CFQUERY>

<CFSET freezedate = #CreateODBCDateTime(Now())#>


<cfquery name="getSelection" datasource="#application.sitedatasource#">
Select selectionid 
from selection
where selectiontextid = 'recentusers'
</CFQUERY>

<cfquery name="appendtoSelectionTag" datasource="#application.sitedatasource#">
INSERT INTO SelectionTag (SelectionID,EntityID,Status, CreatedBy,Created,LastUpdatedBy,LastUpdated) 
				 SELECT distinct <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" >,	
	 					p.personid,	
						1,	
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,	
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,		
						<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
						<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					FROM 	usage as us, person as p, location as l
					where 	us.personid = p.personid
							and p.locationid = l.locationid
							and p.active <> 0
							and l.active <> 0
							and ltrim(p.password) <> ''
							and  p.LoginExpires >= #CreateODBCDateTime(Now())#
							and logindate > #CreateODBCDateTime(Now()-90)#
					
							AND p.personid not IN(select entityid from selectiontag where selectionid =  <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" > )

</CFQUERY>


<cfquery name="updateSelectionTag" datasource="#application.sitedatasource#">
Update SelectionTag 
	set status = 1
	where 	selectionid =  <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and status = 0
	and		entityid in (select distinct us.personid
							from usage as us, person as p, location as l
							where us.personid = p.personid
							and p.locationid = l.locationid
							and p.active <> 0
							and l.active <> 0
							and ltrim(p.password) <> ''
							and  p.LoginExpires >= #CreateODBCDateTime(Now())#
							and logindate > #CreateODBCDateTime(Now()-90)#
							)	

</CFQUERY>



<cfquery name="removeFromSelectionTag" datasource="#application.sitedatasource#">
Update SelectionTag 
	set status = 0 
	where 	selectionid =  <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and		entityid not in (select distinct us.personid
							from usage as us, person as p, location as l
							where us.personid = p.personid
							and p.locationid = l.locationid
							and p.active <> 0
							and l.active <> 0
							and ltrim(p.password) <> ''
							and  p.LoginExpires >= #CreateODBCDateTime(Now())#
							and logindate > #CreateODBCDateTime(Now()-90)#
							)	

</CFQUERY>

<cfquery name="UpdateHeader" datasource="#application.sitedatasource#">
	Update 	selection
	set   	lastupdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > , 
			lastupdatedby = #request.relayCurrentUser.usergroupid#
	where 	selectionid =  <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<cfquery name="docount" datasource="#application.sitedatasource#">
	select count(entityid) as numberofpeople
	from selectiontag
	where selectionid =  <cf_queryparam value="#getSelection.SelectionId#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and status <> 0
</CFQUERY>








<CFOUTPUT>Done:  #htmleditformat(doCount.numberofpeople)# users in selection</cfoutput>	



