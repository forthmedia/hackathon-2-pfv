<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-06-01		WAB added javascript item

Possible enhancements:


 --->


<cfquery name="reCreateErrorTypes" datasource="#application.siteDataSource#">
	truncate table relayErrorType
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (1, 'Other','',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (2, 'Log file full','log file for database % is full',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (3, 'Data truncation','String or binary data would be truncated',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (4, 'Violation of PRIMARY KEY','Violation of PRIMARY KEY constraint',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (5, 'Request timeout','The request has exceeded the allowable time limit Tag',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (6, 'Lock timeout other','A timeout occurred while attempting to lock',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (7, 'Lock timeout on FullTree','A timeout occurred while attempting to lock FullTree',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (8, 'Lock timeout on RelayCurrentUser','A timeout occurred while attempting to lock RelayCurrentUser',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (9, 'Lock timeout on BuildNavigationTree','A timeout occurred while attempting to lock BuildNavigationTree',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (10, 'Lock timeout on Session scope','A timeout occurred while attempting to lock session scope',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (11, 'Deadlock victim','deadlock victim',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (12, 'invalid XML character','invalid XML character',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (13, 'CFML structure missing an element','Element % is undefined in a CFML structure',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (14, 'Database syntax error','Incorrect syntax near the keyword',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (15, 'Missing Template','Missing template',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])  /* this is set when javascript error is recorded*/
     VALUES (16, 'Javascript Error','',0,0)
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])  
     VALUES (17, 'Database object missing','[SQLServer]Invalid object name',0,0)

	 /*
INSERT INTO [relayErrorType] ([relayErrorTypeID],[errorType],[matchString],[escalate],[autoSuppressDuplicates])
     VALUES (16, '','',0,0)
*/
</cfquery>

<!--- Now we want to load the data we've just populated --->
<cfquery name="getErrorTypes" datasource="#application.siteDataSource#">
	select * from relayErrorType where len(matchString) > 1 order by relayErrorTypeID
</cfquery>

<cfoutput>
<!--- next we loop over each record and do an update --->
<cfloop query="getErrorTypes">
	<cfquery name="updateErrorTypes" datasource="#application.siteDataSource#">
		update relayError set relayErrorTypeID =  <cf_queryparam value="#relayErrorTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where diagnostics  like  <cf_queryparam value="%#matchString#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		and relayErrorTypeID is Null
	</cfquery>
	<p>#htmleditformat(errorType)# error types updated</p>
</cfloop>
</cfoutput>
<!--- finally set all other errors to other --->
<cfquery name="updateErrorTypes" datasource="#application.siteDataSource#">
	update relayError set relayErrorTypeID = 1
	where relayErrorTypeID is Null
</cfquery>

<h2>Updates complete</h2>


