<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History   --->
<!---	1999-01-25  WAB	Added code to allow to run in a popup window - leaves out all the buttons on the top menu
   1999-02-21  SWJ Modified javascript to call report/commCheck.cfm
--->
<cfparam name="attributes.pageTitle" default="">
<cfparam name="attributes.thisDir" default="Utilities">

<CFPARAM name="frmruninpopup" default=""><!--- wab --->

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<CF_RelayNavMenuItem MenuItemText="Utilities" CFTemplate="/utilities/UtilitiesHome.cfm">
	<cfif findNoCase("emailErrorText.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Add New" CFTemplate="/utilities/emailerrortext.cfm&addnew=1">
	</cfif>
</CF_RelayNavMenu>