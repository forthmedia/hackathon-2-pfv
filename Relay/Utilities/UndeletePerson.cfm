<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Very Rough template for un-deleting a Person

WAB 2000-11-29
RMB	2011/11/01  	LID2831		 Added extra checks to ensure a restored person has a location record
                                 It will ask to restore location, it will also ask to restore any other person(s) that have been deleted attached to location being recovered

2013-03-24		YMA		Case 434607 updated to check organisationID from location in case the org was merged since the user was deleted.
2013-12-19 		NYB 	Case 436226 changed file to handle processing of multiple ids
2015-11-18		SB		Bootstrap

--->

<cfset subsection = "Undelete Person">
<cf_title>Undelete People</cf_title><!--- 2013-12-19 NYB Case 436226 --->

<cfparam name="PassDoNotRecoverPersons" default="0">
<cfparam name="othpersonCount" default="0">

<cfsavecontent variable="personUseForm_html">
	<form method = "post">
	<!--- 2013-12-19 NYB Case 436226
		  2015-11-18 SB Bootstrap --->
	<div class="form-group">
		<label for="frmPersonIDs">Enter the ids of the people to be undeleted</label>
		<!--- 2013-12-19 NYB Case 436226 changed frmPersonID to frmPersonIDs--->
		<INPUT type="text" name="frmPersonIDs" id="frmPersonIDs" value="<CFIF isDefined("frmPersonIDs")><cfoutput>#htmleditformat(frmPersonIDs)#</cfoutput></cfif>" length="8" class="form-control">
		<INPUT type="hidden" name="PassDoNotRecoverPersons" id="PassDoNotRecoverPersons" value="1">
	</div>
	<INPUT type= "submit" id="frmSubmit" class="btn btn-primary">
	</form>
</cfsavecontent>
<cfset displayformhtml = true><!--- 2013-12-19 NYB Case 436226 added --->

		<!--- START: 2013-12-19 NYB Case 436226 added --->
		<CFIF isDefined("frmPersonIDs") and len(frmPersonIDs) gt 0>
			<cfset frmPersonIDs = application.com.globalFunctions.RemoveUnwantedTypeFromList(frmPersonIDs,"Numeric")>
			<CFIF isDefined("frmPersonIDs") and len(frmPersonIDs) eq 0>
				<cfset personUseForm_html = personUseForm_html&"<p>Invalid characters supplied</p>">
			</CFIF>
		</CFIF>
		<!--- END: 2013-12-19 NYB Case 436226 --->

		<!--- 2013-12-19 NYB Case 436226 changed to frmPersonIDs and added len condition --->
		<CFIF isDefined("frmPersonIDs") and len(frmPersonIDs) gt 0>

			<!--- START: 2013-12-19 NYB Case 436226 changed query to use list --->
			<CFQUERY NAME="checkPersonDel" DATASOURCE=#application.sitedatasource#>
				select * from PersonDel where PersonID in (<cf_queryparam value="#frmPersonids#" CFSQLTYPE="CF_SQL_INTEGER" list="yes">)
			</CFQUERY>
			<!--- END: 2013-12-19 NYB Case 436226 --->

			<!--- START: 2013-12-19 NYB Case 436226 added --->
			<cfset unfound = application.com.globalFunctions.ListMinusList(frmPersonIDs,ValueList(checkPersonDel.personid))>
			<cfloop list="#unfound#" index="perID">
				<cfset personUseForm_html = personUseForm_html&"Person Id <cfoutput>#htmleditformat(perID)#</cfoutput> supplied is not in the deleted set<br>">
			</cfloop>

			<cfloop query="checkPersonDel">

				<cfset frmPersonID = personid>
			<!--- END: 2013-12-19 NYB Case 436226 --->

				<!--- RMB	2011/10/31  	LID2831		Start Check Level 1	 Checks to ensure Person has been deleted first  --->

					<!--- 2013-03-24	YMA	Case 434607 updated to check organisationID from location in case the org was merged since the user was deleted. --->
					<!--- START: 2013-12-19 NYB Case 436226 removed checkPersonDel.--->
					<CFQUERY NAME="checkOrganisationDel" DATASOURCE=#application.sitedatasource#>
						select * from OrganisationDel where OrganisationID in
						(select organisationID from location where locationID = <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
						union
						select organisationID from locationdel where locationID = <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >)
	   				</CFQUERY>
	   				<!--- END: 2013-12-19 NYB Case 436226 --->

					<!--- RMB	2011/10/31  	LID2831		Start Check Level 2	 Checks to ensure Organisation has been NOT been deleted first  --->
					<cfif checkOrganisationDel.recordCount is 0>
						<!--- START: 2013-12-19 NYB Case 436226 added --->
						<cfif listlen(frmPersonIDs) gt 1>
							<cfset frmLocationID = LocationID>
						</cfif>
						<!--- END: 2013-12-19 NYB Case 436226 --->

						<CFIF isDefined("frmLocationID") and len(frmLocationID) gt 0><!--- 2013-12-19 NYB Case 436226 added len requirement --->
						<!--- START: 2013-12-19 NYB Case 436226 added --->
							<CFQUERY NAME="getLocation" DATASOURCE=#application.sitedatasource#>
								select * from LocationDel where Locationid =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
							</CFQUERY>
							<cfif getLocation.recordcount gt 0>
						<!--- END: 2013-12-19 NYB Case 436226 --->
								<CFQUERY NAME="unDeleteLocation" DATASOURCE=#application.sitedatasource#>
									exec undeleteLocation @locationid =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > , @DoNotRecoverPersons = #PassDoNotRecoverPersons#
								</CFQUERY>
							</cfif><!--- 2013-12-19 NYB Case 436226 added --->

							<CFQUERY NAME="getLocation" DATASOURCE=#application.sitedatasource#>
								select * from Location where Locationid =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
							</CFQUERY>

							<cfif getLocation.recordCount is not 0>
								<!--- 2013-12-19 NYB Case 436226 changed--->
								<cfset personUseForm_html = personUseForm_html & "Site #htmleditformat(getLocation.sitename)# undeleted successfully<br />">
							<cfelse>
								<!--- 2013-12-19 NYB Case 436226 changed --->
								<cfset personUseForm_html = personUseForm_html & "Location not undeleted<br />">
							</cfif>
						</CFIF>

						<!--- START: 2013-12-19 NYB Case 436226 removed checkPersonDel. from query--->
						<CFQUERY NAME="checkLocationDel" DATASOURCE=#application.sitedatasource#>
							select * from LocationDel where LocationID =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</CFQUERY>
						<!--- END: 2013-12-19 NYB Case 436226 --->

						<!--- START: 2013-12-19 NYB Case 436226 removed checkPersonDel. from query--->
						<cfif listlen(frmPersonIDs) eq 1>
							<CFQUERY NAME="getOtherPerson" DATASOURCE=#application.sitedatasource#>
								select * from PersonDel where LocationID =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND PersonID ! =  <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
							</CFQUERY>
						</cfif>
						<!--- END: 2013-12-19 NYB Case 436226 --->

						<!--- RMB	2011/10/31  	LID2831		Start Check Level 3	 Checks to ensure Location has been NOT been deleted first  --->
						<cfif checkLocationDel.recordCount is 0 or listlen(frmPersonIDs) gt 1>
						<!--- 2013-12-19 NYB Case 436226 --->

							<CFIF PassDoNotRecoverPersons EQ 1>
								<CFQUERY NAME="unDeletePerson" DATASOURCE=#application.sitedatasource#>
									exec undeletePerson @personid =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
								</CFQUERY>
							</cfif>

							<CFQUERY NAME="getPerson" DATASOURCE=#application.sitedatasource#>
								select * from Person where personid =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
							</CFQUERY>

							<cfif getPerson.recordCount is not 0>
								<!--- 2013-12-19 NYB Case 436226 changed--->
								<cfset personUseForm_html = personUseForm_html & "User #htmleditformat(getPerson.firstname)# #htmleditformat(getPerson.lastname)# undeleted successfully<br />">
							<cfelse>
								<!--- 2013-12-19 NYB Case 436226 changed --->
								<cfset personUseForm_html = personUseForm_html & "Person not undeleted<br />">
							</cfif>

							<cfif PassDoNotRecoverPersons EQ 0 AND othpersonCount GT 0>

								<!--- START: 2013-12-19 NYB Case 436226 removed checkPersonDel. from query--->
								<CFQUERY NAME="getAllPersons" DATASOURCE=#application.sitedatasource#>
									select * from Person where LocationID =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND PersonID ! =  <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
								</CFQUERY>

								<!--- START: 2013-12-19 NYB Case 436226 wrapped in savecontent to display later --->
								<cfsavecontent variable="personUseForm_html_supp">
									<br />These persons have also been restored
									<CF_tableFromQueryObject
										queryObject="#getAllPersons#"
										queryName="getAllPersons"
										showthesecolumns="PersonID,Salutation,FirstName,LastName"
										useInclude="false"
										HidePageControls="yes"
										allowColumnSorting="no">
								</cfsavecontent>
								<!--- END: 2013-12-19 NYB Case 436226 --->

								<!--- 2013-12-19 NYB Case 436226 added --->
								<cfset personUseForm_html = personUseForm_html & "<br />" & personUseForm_html_supp>

							</cfif>

						<cfelse>

							<cfset displayformhtml = false><!--- 2013-12-19 NYB Case 436226 added --->

							Error this persons Location is in the deleted set<br />
							<CF_tableFromQueryObject
								queryObject="#checkPersonDel#"
								queryName="checkPersonDel"
								showthesecolumns="PersonID,Salutation,FirstName,LastName"
								useInclude="false"
								HidePageControls="yes"
								allowColumnSorting="no">

							This function will restore this Location for you<br />
							<CF_tableFromQueryObject
								queryObject="#checkLocationDel#"
								queryName="checkLocationDel"
								showthesecolumns="LocationID,SiteName"
								hidethesecolumns=""
								useInclude="false"
								HidePageControls="yes"
								allowColumnSorting="no">

							<cfif getOtherPerson.recordCount GT 0>

								Do you also wish to restore <cfoutput>#getOtherPerson.recordCount#</cfoutput> person with the same location?<br />
								<CF_tableFromQueryObject
									queryObject="#getOtherPerson#"
									queryName="getOtherPerson"
									showthesecolumns="PersonID,Salutation,FirstName,LastName"
									useInclude="false"
									HidePageControls="yes"
									allowColumnSorting="no">

							</cfif>

							<cfsavecontent variable="personlocationUseForm_html">
								<form method = "post">

								<cfif getOtherPerson.recordCount GT 0>
									<div class="radio">
										<label>
											Yes (Restore all persons) <input type="radio" name="PassDoNotRecoverPersons" value="0" checked="yes"></label>									<!--- 2013-12-19 NYB Case 436226 changed: --->
									/ <label>No (Only restore <cfoutput>#htmleditformat(FirstName)# #htmleditformat(LastName)#</cfoutput>) <input type="radio" name="PassDoNotRecoverPersons" id="PassDoNotRecoverPersons" value="1"></label>
									</div>
								<cfelse>
	  								<input type="hidden" name="PassDoNotRecoverPersons" id="PassDoNotRecoverPersons" value="0">
								</cfif>

									<!--- START: 2013-12-19 NYB Case 436226 frmPersonID to frmPersonIDs--->
									<INPUT type = "hidden" id="frmPersonIDs" name="frmPersonIDs" value="<CFIF isDefined("frmPersonIDs")><cfoutput>#htmleditformat(frmPersonIDs)#</cfoutput></cfif>">
									<CF_INPUT type = "hidden" id="frmLocationID" name="frmLocationID" value="#LocationID#">
									<!--- END: 2013-12-19 NYB Case 436226 --->
									<CF_INPUT type = "hidden" id="othpersonCount" name="othpersonCount" value="#getOtherPerson.recordCount#">
									<INPUT type= "submit" id="frmSubmit" class="btn btn-primary">
								</form>
							</cfsavecontent>

							<cfoutput>
								#personlocationUseForm_html#
							</cfoutput>

		  				</cfif>
						<!--- RMB	2011/10/31  	LID2831		END Check Level 3	 Checks to ensure Location has been NOT been deleted first  --->

					<cfelse>
						<!--- START: 2013-12-19 NYB Case 436226 - REPLACED: ---
						<cfoutput>
							Error Organisation Id <cfoutput>#htmleditformat(checkPersonDel.OrganisationID)#</cfoutput> supplied is in the deleted set<br>
							Please restore that Organisation Id <cfoutput>#htmleditformat(checkPersonDel.OrganisationID)#</cfoutput> first<br>
							<!---#personUseForm#--->
						</cfoutput>
						!--- WITH: 2013-12-19 NYB Case 436226 --->
						<cfset personUseForm_html = personUseForm_html & "Error Organisation Id <cfoutput>#htmleditformat(OrganisationID)#</cfoutput> supplied is in the deleted set<br>">
						<cfset personUseForm_html = personUseForm_html & "Please restore that Organisation Id <cfoutput>#htmleditformat(OrganisationID)#</cfoutput> first<br>">
						<!--- END: 2013-12-19 NYB Case 436226 --->
	  				</cfif>
					<!--- RMB	2011/10/31  	LID2831		END Check Level 2	 Checks to ensure Organisation has been NOT been deleted first  --->

				<!--- RMB	2011/10/31  	LID2831		END Check Level 1	 Checks to ensure person has been deleted first  --->
			</cfloop><!--- 2013-12-19 NYB Case 436226 added --->

			<!--- START: 2013-12-19 NYB Case 436226 - REMOVED: ---
			<cfelse>
				Error Person Id <cfoutput>#htmleditformat(frmPersonID)#</cfoutput> supplied is not in the deleted set<br>
				<cfoutput>
				#personUseForm_html#
				</cfoutput>
			</cfif>
			!--- END: 2013-12-19 NYB Case 436226 --->

		<!--- 2013-12-19 NYB Case 436226 REMOVED:
		<cfelse>
		--->

		</cfif>

		<!--- START: 2013-12-19 NYB Case 436226 added if --->
		<cfif displayformhtml>
			<cfoutput>
			#personUseForm_html#
			</cfoutput>
		</cfif>
		<!--- END: 2013-12-19 NYB Case 436226 --->
