<cfcomponent output="false">
<cffunction name="getBanner" output="false" returnType="query">
	<cfargument name="bannerIds" type="string" required="true">
	<cfset var phraseSnippetHeading = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "banner", phraseTextID = "Heading", nullText="''", baseTableAlias="b")>
	<cfset var phraseSnippetParagraph = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "banner", phraseTextID = "Paragraph", nullText="''", baseTableAlias="b")>
	<cfset var phraseSnippetButton = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "banner", phraseTextID = "Button", nullText="''", baseTableAlias="b")>
	<cfset var qryBanners = "">
	<!--- START 2016-08-16 AHL Sorting order based on the argument list --->
	<cfquery name="qryBanners" >
		SELECT
			DISTINCT
			b.bannerID,
			b.title,
			b.targetUrl as targetUrl,
			#phraseSnippetHeading.select# as heading,
			#phraseSnippetParagraph.select# as paragraph,
			#phraseSnippetButton.select# as button,
			b.cssClasses,
			b.backgroundFileID,
			b.containerPadding,
			b.headingFontColor,
			b.paragraphFontColor,
			X.sortorder
		  FROM banner b WITH (NOLOCK)
			#preserveSingleQuotes(phraseSnippetHeading.join)#
			#preserveSingleQuotes(phraseSnippetParagraph.join)#
			#preserveSingleQuotes(phraseSnippetButton.join)#
		  INNER JOIN (VALUES
				<cfset idx = 0>
				<cfif arguments.bannerIds eq "">
					(0,0)
				<cfelse>
					<cfloop list='#arguments.bannerIds#' index = "id">
						(#id#,#idx#)
						<cfset idx++>
						<cfif idx lt ListLen(arguments.bannerIds)>
						,
						</cfif>
					</cfloop>
				</cfif>
				) AS X(id,sortorder) ON X.id = b.bannerID
   		LEFT JOIN  dbo.getRecordRightsTableByPerson (563,<cfqueryparam cfsqltype="cf_sql_integer" value='#request.relaycurrentuser.personID#'/>) AS re
     			ON re.recordid = b.bannerID

   			WHERE
   			(
        		    b.hasRecordRightsBitMask & 1 = 0     /* this record has no Level1 rights applied to it (so everyone can see the record) (note that for Level2 rights change the & 1 to a & 2, etc) */
            	   OR
           		 re.level1 = 1                                     /* this person has level1 rights to this record */
      		)
		  ORDER BY X.sortorder
		<!--- END 2016-08-16 AHL Sorting order based on the argument list --->
	</cfquery>

    <Cfreturn qryBanners>

    </cffunction>

		<cffunction name="getBannersQuery" returnType="query" description = "for tfqo for editing the banners" hint = "for tfqo for editing the banners">
			<cfargument name="sortorder" type="string" default="title"/>
			<cfset var qryData = ""/>

			<cfquery name="qryData" datasource="#application.siteDataSource#">
				SELECT [bannerID]
						,[title]
						,[targetUrl]
						,[heading_defaultTranslation]
						,[paragraph_defaultTranslation]
						,[button_defaultTranslation]
						,[cssClasses]
						,[backgroundFileID]
						,[containerPadding]
						,[headingFontColor]
						,[paragraphFontColor]
						,[lastUpdatedByPerson]
						,[createdByPerson]
						,[CreatedBy]
						,[Created]
						,[LastUpdatedBy]
						,[LastUpdated]
						, 'phr_delete' as [delete]
					FROM [dbo].[banner]
				ORDER BY <cf_queryObjectName value="#arguments.sortOrder#">
			</cfquery>

			<cfreturn qryData/>

		</cffunction>


		<!--- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			A Sarabi 30/09/2016
			If the given string doesnt have 'px' at the end of it then its will be added!
		~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ --->
		<cffunction name="hasPixle" returnType="string">
			<cfargument name="bannerHeight" type="string" required="true">

			<cfset var returnVal ="600px">

			<cfif right(bannerHeight,2) neq "px">
				<cfif IsNumeric(bannerHeight)>
					<cfset returnVal = Insert("px", bannerHeight, Len(bannerHeight)) />
				</cfif>
			</cfif>

			<cfreturn returnVal>
		</cffunction>

		<cffunction name = "getEditorXML" returnType = "string"  description = "" hint = "">

					<cfset var xmlSource = ""/>

					<!--- const --->
					<cfset var sqlBackgrounds = "SELECT
							f.FileID
							,f.name
						FROM files f WITH (NoLock)
						INNER JOIN fileType ft WITH (NoLock) ON ft.fileTypeID = f.fileTypeID
						INNER JOIN fileTypeGroup ftg WITH (NoLock) ON ft.filetypegroupid = ftg.filetypegroupid AND ftg.heading like 'banner'"/>

					<cfsavecontent variable="xmlSource">
						<cfoutput>
						<editors>
							<editor id="007" name="thisEditor" entity="banner" title="Banner Editor">
								<field name="bannerID" label="phr_banner_bannerID" description="This records unique ID." control="html"></field>
								<field name="title" label="phr_banner_title" description="Title for internal use" required="true"></field>
								<field name="backgroundFileID" label="phr_banner_backgroundFileID" nullText="phr_Ext_SelectAValue" required="true" NoteTextImage="false" NoteTextPosition="right" NoteText="phr_banner_backgroundFile_noteText" description="phr_banner_backgroundFile_description" control="select" displayas="select" display="name" value="FileID" validValues="#sqlBackgrounds#"></field>
								<field name="" CONTROL="TRANSLATION" label="phr_banner_heading" Parameters="phraseTextID=heading" showOtherTranslationsTable="false" placeholder="placeholder test"></field>
								<field name="headingFontColor" id="headingcolour" class="jscolor" label="phr_banner_headingFontColor" description="phr_banner_headingFontColor_description" placeholder="thing bla bla"></field>
								<field name="" CONTROL="TRANSLATION" label="phr_banner_paragraph" description=""  Parameters="phraseTextID=paragraph,displayAs=TextArea" showOtherTranslationsTable="false" placeholder="test test test"></field>
								<field name="paragraphFontColor" id="paragraphcolour" class="jscolor" label="phr_banner_paragraphFontColor" description="phr_banner_paragraphFontColor_description"></field>
								<field name="" CONTROL="TRANSLATION" label="phr_banner_button" Parameters="phraseTextID=button" showOtherTranslationsTable="false"></field>
								<field name="targetUrl" label="phr_banner_url (if this is an external URL then the http:// or the https:// must be included!)" description="Destination URL after clicking on the banner" maxlength="249" ></field>
								<!--- AS 06/10/2016 Removed from out of the box
								<field name="cssClasses" label="phr_banner_cssClasses" description="Advanced configuration. CSS class for the container" maxlength="249" ></field>
								--->
								<field name="" control="recordRights" label="phr_banner_WhoShouldSeeThisModule"
										userGroupTypes = "External,Internal,Licensed,Personal,Other"
										permissionLevel="1"
										singleUserGroupTypeOnly = "No"></field>

								<group label="System Fields" name="systemFields">
									<field name="sysCreated"></field>
									<field name="sysLastUpdated"></field>
									<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
								</group>
							</editor>

						</editors>
						</cfoutput>
					</cfsavecontent>

					<cfreturn xmlSource/>
		</cffunction>



</cfcomponent>