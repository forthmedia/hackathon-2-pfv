<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2004-11-15   WAB sorted out unicode characters not being sent correctly
2005-01-19	  WAB added test for email containing HTML
2005-02-28		WAB needed to get country specific emails so needed to use cf_translate
			have therefore had to chanage all the variables that can be included in emails into form variables so that the get into the custom tag
2005-07-26		WAB needed to get correct language to send an email in - start using contry.defautllanguageid
2005-08-08   AJC	Allowed multiple personid's to be passed to sendemail function
2005-08-26 SWJ: Added isValidEmail function
2006-03-16	WAB:	added a pointer request.emailmerge = application.com.emailmerge so that email merge functions can be embedded in phrases and evaluated
2006-12-05 	WAB: needed to be able to send emails which were not of the country of the recipient, but rather of another country stored somewhere else.  Can pass in a countryid to override use of location.country and languageid to override language.  requires cf_translate to be released at same time
2007-05-08	WAB  hacked in a method to allow the toAddress, ccAddress and bccAddresses to be entered as a pointer to a phrase
2007-06-18 WAB allow emaildefID (ie numeric key on the table) to be passed in emailtextID variable
2007-06-26 WAB allow query set in parameters to be run (can reference #personid# or #entityID# if passed in  (for example from relatedFileupdatesubroutine)
			WAB added concept of the mergestructure
2007/10/03	WAB	countrydescription aliased to country in query (previously form.country set to countrydescription
2008/01/29	WAB	added recipientAlternativePersonID   (actually looks as if didn't do properly, mod made 2008/04/23)
2008/04/24	WAB	added ability to define TOAddress via a query - use select email from ...
2008/06/19	WAB mergeStruct now has query called location in it (which is actually the whole getPersonDetails record ).  Used for calling outputAddress function
2008/06/25	WAB mergeStruct now has variable called ADDRESS which outputs an address with <BR> separators
2008/11/05 WAB  put the custom queries into their own structure, two bits must be released together
2009/01/29	WAB Work on redirecting emails on Test Servers - Set of functions added
2009/01/29	WAB sendEmail converted to use CF_MAIL
2009/02/24 SSS you can make emails inactive but you are still able to send a test if a email is inactive.
2009/02/27 NJH  Bug Fix Sony Issue 1913 - added remainder of address fields for personDetails as SA had address7 in the addressScreen which didn't exist in the query.
2011/01/  WAB Added code to enable bounceback processing on these emails
2011/04/19 WAB LID 6349  Problem with above code, was updating the wrong commdetail record (incorrect use of scope_identity())
2010/05/05 WAB LID 6501 Changes to get some mergeFunctions working properly
2010/05/05 WAB Also noticed that test emails probably failing
2011/05/09	MS	P-Len030 CR-018	sendEmail to send out attachments
2011/05/24 WAB	LID 6640 did some varing
2011/07/19	NAS	P-LEX053
2011/10/12	WAB added request level caching to getCurrentEmailRedirectionParameters()
2012/03/26	IH	Case 427195 Don't send out emails to inactive people
2012/07/05	IH	Case 428806 If email sender and recepient is not the same set recepient locale based on defaultLocale in country table
2012-10-29 	WAB Case 431506.  do a Duplicate() on mergeStruct in generateEmailContent(). sendEmail was being called in a loop and mergeStruct was getting contaminated (perhaps the faut )
2012-11-07	IH	Case 431724 add ; as email address delimiter, fix bug in failed email handling
2012-11-21	WAB Case 432201 sendEmail() fails if emailAddress is blank.  Now log attempt correctly.
2013-07-02 	PPB Case 435789 added join on country in getAlternativeRecipientEmail()
2013-07-03	WAB/MS	P-KAS022 Added ability to send emails to personID=0 without falling over.  Note that these emails are not logged
2013-01-10	WAB CASE 437262 fix problems when sending an email from  a housekeeping task (when relaycurrentuser is not completely defined)
2013-11-21	WAB	2013RoadMap2 Item 25.  Changes to contact history.  System Emails now recorded in  commdetail with own commTypeID (System Email - 50)
			Now record generated content in commDetailContent table
			Store details of entities which an email is related to (by looking at the mergeStruct)
			Now log emails to personID=0 (since now can have a relationship to another entity)
2013-12-17 	Record the fact that an email has been sent to someone other than the PersonID - in case need to be filtered out at some stage
			Also store the from address (in the metadata field)
2014-07-01 PPB Case 438224 add option to only send an email if one hasn't already been sent
2014-09-17 	WAB CASE 441711 alter translateEmailAddressList to protect against leading/trailing spaces on the address.
2015-03-09	AXA CASE 444047 fixed bug in calculateemailcontent
2015-10-13  DAN PROD2015-160 - helper function to get emailDefID by given emailTextID
2015-10-29  DAN PROD2015-281 - support for case 446369 - add method to check if email related to the specific entity has been sent to the specified person,
            this can be used as standalone OR as built-in in sendEmail if you pass optional params "sendIfNotPreviouslySent" and "relatedEntityId"
2015-10-30  DAN PROD2015-281 - add commdetail entityType to the query (since entityIDs are not unique) and also allow to specify related entity name to check only for relatedEntityTypeId
2015-11-02  DAN PROD2015-281 - remove additional args and use existing mergeStruct instead to get relatedEntityId/typeId
2015-11-02	ACPK	PROD2015-281 Added structCount(email.relatedEntityIDs)
2016-01-19	WAB BF-307 alter insert into Commdetail.email to take 200 characters
20/01/2016  DAN 447651 - update the old regexp for e-mail validation so its consistence (see cfform.js) and supports new TLDs
2017-01-11	DBB RT-21: Fixed Activity stream storing message incorrectly.
2017-01-24 	WAB RT-188 Evaluation of toAddress failing resulting in emails going to 1000s of people
--->

<cfcomponent displayname="Email component" hint="Manages email functions">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             getEmailDetails
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getEmailDetails" access="public" returntype="struct">
		<cfargument name="emailTextID" type="string" required="yes">

		<cfset var result = "">
		<cfset var getEmailDetailsQry = "">
		<cfset var commid = "">
		<cfset var queryName = "">
		<cfset var parameterName = "">

		<CFQUERY NAME="getEmailDetailsQry" DATASOURCE="#application.siteDataSource#">
			select emailDefID, emailTextID, type,addresstype,toaddress,ccaddress,bccaddress,includetemplate,parameters,fromaddress,commid,active,
				<!--- WAB 2010/10/20 converted to entity phrases and this was the quickest way of upgrading--->
				'subject_emaildef_' + convert(varchar,emailDefID) as subjectPhraseTextID,
				'Body_emaildef_' + convert(varchar,emailDefID) as bodyPhraseTextID,
				sendMessage,messageUrl,messageImage,
				'messageTitle_emaildef_' + convert(varchar,emailDefID) as messageTitlePhraseTextID,
				'messageText_emaildef_' + convert(varchar,emailDefID) as messageTextPhraseTextID
			 from emailDef where <cfif isNumeric(arguments.emailTextID)>emailDefID<cfelse>emailTextID</cfif> = '#arguments.emailTextID#'
		</CFQUERY>

		<!--- WAB 2007-05-16 link to communications table for recording sends --->
		<cfif getEmailDetailsQry.recordcount is not 0 and getEmailDetailsQry.commid is "">
			<cfset commid = updateEmailDefCommID(getEmailDetailsQry.emaildefID)>
			<cfset querysetcell (getEmailDetailsQry,"commid",#commid#,1)>
		</cfif>


		<!--- make it a structure so that we can put parameters into a substructure (can't store a structure in a query in cf6) --->
		<cfset result = application.com.structureFunctions.queryRowToStruct(query=getEmailDetailsQry,row=1)>
		<cfset result.recordCount = getEmailDetailsQry.recordcount>
		<cfset result.parametersStruct = application.com.regexp.convertNameValuePairStringToStructure(result.parameters,",")>
		<cfset result.customQueries = structNew()>

		<!--- WAB 2008/11/05 put the custom queries into their own structure --->
			<cfloop item="parameterName" collection = "#result.parametersStruct#">
				<cfif listfirst(parameterName,"_") is "query">
					<cfset queryName = listlast(parameterName,"_")>
					<cfset result.customQueries[queryName] = result.parametersStruct[parameterName]>
					<cfset structDelete (result.parametersStruct,parameterName)>
				</cfif>
			</cfloop>

		<cfreturn result>

	</cffunction>

	<!--- START: 2010-06-11 AJC - P-FNL090 Email Scheduler --->
	<cffunction name="getEmailDef" access="public" returntype="query">
		<cfargument name="emailTextID" type="string" required="false" default="">

		<cfset var getEmailDetailsQry = "">

		<cfquery name="getEmailDetailsQry" datasource="#application.siteDataSource#">
			select * from emailDef
			<cfif emailTextID neq "">
				 where <cfif isNumeric(arguments.emailTextID)>emailDefID<cfelse>emailTextID</cfif> = '#arguments.emailTextID#'
			</cfif>
		</cfquery>

		<cfreturn getEmailDetailsQry>

	</cffunction>
	<!--- END: 2010-06-11 AJC - P-FNL090 Email Scheduler --->

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             doesEmailDefExist
			 WAB 2007-02-21
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="doesEmailDefExist" access="public" returntype="boolean" hint="">
		<cfargument name="emailTextID" type="string" required="yes">

		<cfset var getEmailDetails = "">

		<CFQUERY NAME="getEmailDetails" DATASOURCE="#application.siteDataSource#">
			select * from emailDef where <cfif isNumeric(arguments.emailTextID)>emailDefID<cfelse>emailTextID</cfif> = '#arguments.emailTextID#'
		</CFQUERY>

		<cfreturn iif(getEmailDetails.recordcount is 0,false,true)>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             sendEmail
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="sendEmail" access="public" returntype="string" hint="Sends an email of emailTextID to personid">
		<cfargument name="emailTextID" type="string" required="yes">
		<cfargument name="personID" type="string" required="yes">
		<cfargument name="cfmailType" type="string" default="text">
		<cfargument name="bccopy" type="string" default="" hint="addtional blind copy emails">
		<cfargument name="ccAddress" type="string" default="" hint="addtional cc address">  <!--- NJH 2007/09/10 pass an address to cc the email to --->
		<cfargument name="recipientAlternativeEmailAddress" type="string" default="" hint="email personalised to PersonID but sent to a different email address">
		<cfargument name="recipientAlternativePersonID" type="string" default="" hint="email personalised to arguments.PersonID but sent to different person - recipientAlternativePersonID">
		<cfargument name="CountryID" type="string" DEFAULT="">  <!--- for special cases when need to send email of a country which is not the person's actual country  added for Lexmark--->
		<cfargument name="LanguageID" type="string" DEFAULT="">  <!--- for special cases when need to send email in a different language--->
		<cfargument name="test" type="boolean" DEFAULT="false">
		<cfargument name="entityID"  DEFAULT="0">
		<cfargument name="attachments" Required="No" DEFAULT="#querynew('empty')#" Type="Query"> <!--- 2011/05/09	MS	P-Len030 CR-018	enable attachments --->
		<cfargument name="sendIfNotPreviouslySent" type="boolean" DEFAULT="false"> 	<!--- 2014-07-01 PPB Case 438224 --->
		<cfargument name="mergeStruct" type="struct" default="#structNew()#" required="false"><!---ESZ 16/09/2016 PROD2016-379 Standard Emails - The 'preview' button is not working.--->
        <!--- 2015-10-29 DAN - note the existing arg above "sendIfNotPreviouslySent" has "NOT" in its name so "false" means "true" so by default it allows to send email without checking if already did (which seems OK for the default behaviour) --->

        <cfset var failToAddress = "">
		<cfset var mailFailToDomain = '' />
		<cfset var statusid = '' />
		<cfset var returnMessage = '' />
		<cfset var insertCommDetail = '' />
		<cfset var updateCommDetail = '' />
        <cfset var relatedEntityId = 0>
		<cfset var relatedEntityTypeID = -1> <!--- there is an entityType = 0 --->
		<cfset var insertMessageSent = "">

        <!--- START: PROD2015-281 / case 446369 --->
        <cfset var email = calculateEmailContent(argumentCollection = arguments)>

        <cfif structKeyExists(email, "relatedEntityIDs") and structCount(email.relatedEntityIDs) neq 0>
            <cfset relatedEntityTypeId = StructKeyArray(email.relatedEntityIDs)[1]>
            <cfset relatedEntityId = email.relatedEntityIDs[relatedEntityTypeId]>
        </cfif>
        <!--- END: PROD2015-281 / case 446369 --->

        <!--- START 2014-07-01 PPB Case 438224 --->
        <cfif arguments.sendIfNotPreviouslySent and hasEmailBeenSent(emailTextId=arguments.emailTextID,personId=arguments.personID,relatedEntityId=relatedEntityId,relatedEntityTypeId=relatedEntityTypeId)> <!--- 2015-10-29 DAN PROD2015-281 - support for case 446369 --->
            <cfset returnMessage = "Email Sent Previously" />
        </cfif>
        <!--- END 2014-07-01 PPB Case 438224 --->



		<cfif returnMessage eq "">					<!--- 2014-07-01 PPB Case 438224 --->

<!---
		An idea here - could always send the test to the current user
			<cfif test>
				<cfset email.toAddress =  "Test - Would go to " & email.toAddress   & " <" & request.relayCurrentUser.person.email & ">">
			</cfif>
--->

			<cfif email.isOK>

				<cfif not test  >
					<!--- log sending, need to do an insert before sending so that we have the commdetailid for the failToAddress --->
					<CFQUERY NAME="insertCommDetail" DATASOURCE="#application.siteDataSource#">
					insert into Commdetail
					(commid, locationid, personid, commformlid, commtypeid, fax, feedback, commstatusid, datesent, lastupdated,lastupdatedby, test)
					values
					(<cf_queryparam value="#email.commid#" CFSQLTYPE="CF_SQL_Integer" >,
					<cf_queryparam value="#iif(email.person.locationid is not "",de(email.person.locationid),0)#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#iif(email.person.personid is not "",de(email.person.personid),0)#" CFSQLTYPE="cf_sql_integer" >,
					2,
					50,
					<cf_queryparam value="#email.toAddress#" CFSQLTYPE="CF_SQL_VARCHAR"  maxlength="1500">
					,'',0,null,getdate(),
					<cf_queryparam value="#request.relaycurrentuser.personid#" CFSQLTYPE="cf_sql_integer" >,
					<cf_queryparam value="#iif(test,1,0)#" CFSQLTYPE="cf_sql_integer" >)
					select scope_identity() as commdetailID  <!--- WAB LID 5867 scope_identity() was getting the wrong identity value (from commdetailhistory) --->
					</CFQUERY>

					<cfset mailFailToDomain = application.com.settings.getSetting("communications.mailFailToDomain")>
					<cfset failToAddress = "BB#application.instance.Databaseid#-#email.CommID#-#email.person.personid#-@#mailFailToDomain#">
				</cfif>

				<!--- WAB 2012-11-12 moved test for blank email address down, so that attempts to send to blank email address are logged (and to prevent an error because we were trying to update a commdetail record which had not been created) --->
				<CFIF email.toAddress neq "">

					<!---
						WAB 2012-09-10 (but lost and not committed until 2012-11-12)
						Moved this <CFTRY> from higher up (before insertCommdetail)
						If for some reason the insert into commDetail failed we were catching and ignoring the error, but then the updateCommDetail query was failing further down
					--->
					<cftry>

						<!--- 2009/01/29	WAB  convert to CF_MAIL --->
						<!--- NJH 2009/02/15 Bug Fix All Sites 1820 - added cfoutput around email body --->
						<cf_mail to="#email.toAddress#"
					        from="#email.fromAddress#"
					        subject="#email.Subject#"
					        cc="#email.ccAddress#"
					        bcc="#email.bccAddress#"
							type = "#email.cfmailType#"
							><cfoutput>#email.Body#</cfoutput>
							<cfif failToAddress is not ""><cf_mailparam name="Return-Path" value="#failToAddress#"></cfif>
							<!--- START: 2011/05/09	MS	P-Len030 CR-018	IF we need to attach a file in this email --->
								<CFLOOP query="attachments">
									<cf_mailparam file="#attachments.path#">
								</CFLOOP>
							<!--- END: 2011/05/09	MS	P-Len030 CR-018 --->
							</cf_mail>

						<cfset statusid = 2>
						<cfset returnMessage = "Email Sent">

					<cfcatch type="Any">
						<cfset statusid = -2>
						<cfset returnMessage = "Email failed">
					</cfcatch>
					</cftry>


				<cfelse>
					<cfset statusid = -2>
					<cfset returnMessage = "Email failed - Blank Address">
				</CFIF>

				<cfif not test  >
					<!--- log sending (or lack of it)--->
					<!--- 2012-03-07 WAB Case427030.  This query was outside the IF email.toAddress neq "", so crashed when the email was blank and we tried to update a non existed commdetail record (well #insertCommDetail.commdetailID# did not exist)--->

					<!--- WAB 2013-12-17 indicate if the email wasn't actually sent to the Person it is concerned with --->
					<cfset var metaData = {from=email.fromAddress}>
					<cfif email.NotSentToMergedPerson>
						<cfset metaData.NotSentToMergedPerson = true>
					</cfif>



					<CFQUERY NAME="updateCommDetail" DATASOURCE="#application.siteDataSource#">
						update Commdetail
						set
						commStatusID =  <cf_queryparam value="#statusid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						feedback =  <cf_queryparam value="#returnMessage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							dateSent = getdate()
						where commdetailid =  <cf_queryparam value="#insertCommDetail.commdetailID#" CFSQLTYPE="CF_SQL_INTEGER" >

						insert into commDetailContent
						(commdetailID, subject,body,metadata)
						values (<cf_queryparam value="#insertCommDetail.commdetailID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#email.Subject#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#email.body#" cfsqltype="cf_sql_varchar">,<cf_queryparam value="#serializeJSON(metaData)#" cfsqltype="cf_sql_varchar">)


						<cfloop collection = #email.relatedEntityIDs# item="relatedEntityTypeID">
						insert into relatedEntity
						(entityTypeID, entityID, relatedEntityTypeID, relatedEntityID)
						values
						(	<cf_queryparam value="#application.entityTypeID['commdetail']#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#insertCommDetail.commdetailID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#relatedEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							<cf_queryparam value="#email.relatedEntityIDs[relatedEntityTypeID]#" CFSQLTYPE="CF_SQL_INTEGER" >
						)
						</cfloop>

					</CFQUERY>

					<cfif statusid eq 2 and email.sendToPersonID neq 0 and email.sendToPersonID neq "" and email.sendMessage>
						<cfquery name="insertMessageSent" datasource="#application.siteDataSource#">
							insert into messageSent (text,title,url,image,personID)
							values (<cf_queryparam value="#email.messageText#" cfsqltype="cf_sql_varchar">,
									<cf_queryparam value="#email.messageTitle#" cfsqltype="cf_sql_varchar">,
									<cf_queryparam value="#email.messageUrl#" cfsqltype="cf_sql_varchar">,
									<cf_queryparam value="#email.messageImage#" cfsqltype="cf_sql_varchar">,
									<cf_queryparam value="#email.sendToPersonID#" cfsqltype="cf_sql_integer">
									)
						</cfquery>
					</cfif>
				</cfif>



			<cfelse>
				<cfset returnMessage = email.error>
			</cfif>

		</cfif>				<!--- 2014-07-01 PPB Case 438224 --->

		<cfreturn returnMessage>

	</cffunction>


	<cffunction name="getRelatedEntityIDsFromMergeStruct" access="public" returntype="struct" hint="">
		<cfargument name="mergeStruct" type="struct" DEFAULT="#structNew()#">

		<!---
		pulls out keys looking like xxxxID from the merge structure and converts xxxx to an entityTypeID
		--->
		<cfscript>
			var result = {};
			var regExp = '\A(.*)id\Z';
			var groupNames = {1="entity"} ;

			structEach (mergeStruct, function (key,value) {
				var match = application.com.regExp.reFindAllOccurrences(regExp,key,groupNames) ;
				if (arrayLen(match) and structKeyExists (application.entityTypeID,match[1].entity)) {
					result[application.entityTypeID[match[1].entity]] = value;
				}
			}
			);

		</cfscript>

		<cfreturn result>
	</cffunction>

	<!--- pulled all of this code out of sendEmail so that we can do for example an on screen display of what is going to happen
		2012-10-29 WAB 431506 use a copy of ARGUMENTS.mergeStruct so we don't contaminate the mergeStruct in calling functions
	--->
	<!--- 2014/09/08	YMA	now we get recipient details before person details as it is possible to make an email about an alternative recipient by passing in
						emailDetails.addressType = "personIDQuery" --->
	<cffunction name="calculateEmailContent" access="public" returntype="struct" hint="merges the content and works out who it is going to">

		<cfargument name="emailTextID" type="string" required="yes">
		<cfargument name="personID" type="string" required="yes">
		<cfargument name="cfmailType" type="string" default="text">
		<cfargument name="bccopy" type="string" default="" hint="addtional blind copy emails">
		<cfargument name="ccAddress" type="string" default="" hint="addtional cc address">  <!--- NJH 2007/09/10 pass an address to cc the email to --->
		<cfargument name="recipientAlternativeEmailAddress" type="string" default="" hint="email personalised to PersonID but sent to a different email address">
		<cfargument name="recipientAlternativePersonID" type="string" default="" hint="email personalised to arguments.PersonID but sent to different person - recipientAlternativePersonID">
		<cfargument name="CountryID" type="string" DEFAULT="">  <!--- for special cases when need to send email of a country which is not the person's actual country  added for Lexmark--->
		<cfargument name="LanguageID" type="string" DEFAULT="">  <!--- for special cases when need to send email in a different language--->
		<cfargument name="test" type="boolean" DEFAULT="false">
		<cfargument name="entityID" DEFAULT="0">
		<cfargument name="mergeStruct" type="struct" DEFAULT="#structNew()#">

		<cfset var result = structNew()>
		<cfset var getPersonDetails = "">
		<cfset var emailDetails = getEmailDetails(arguments.emailTextID)>
		<cfset var emailQuery = '' />
		<cfset var phrases = '' />
		<cfset var query = '' />
		<cfset var logMergeErrors = '' />
		<cfset var key = '' />
		<cfset var queryName = '' />
		<cfset var x = '' />
		<cfset var getAlternativeRecipientEmail = '' />
		<cfset var GetEmailAddress = queryNew("") />
		<cfset var tempQuery = '' />
		<cfset arguments.mergeStruct.test = arguments.test> <!--- NJH 2016/09/20 JIRA PROD2016-379 - pass in test, so that we get a dummy entityID in the merge field --->
		<cfset var tmpMergeStruct = Duplicate(mergeStruct) />
        <cfset var localePos = 0>

		<cfset result.isOK = true>
		<cfset result.commid = emailDetails.commid>
		<cfset result.sendToPersonID = 0>

		<!---
		 if recipientAlternativeEmailAddress passed in, then can't send personID

		 if recipientAlternativePersonID, use as recpient..

		 personID usable if not 0
		 --->
		<cfif arguments.recipientAlternativeEmailAddress eq "">
			<cfif arguments.recipientAlternativePersonID neq "">
				<cfset result.sendToPersonID = arguments.recipientAlternativePersonID>
			<cfelseif arguments.personID neq "">
				<cfset result.sendToPersonID = arguments.personID>
			</cfif>
		</cfif>

		<!--- SWJ: added this check to make sure we get back exactly 1 email --->
		<cfif emailDetails.recordCount neq 1>
			<cfset result.isOK = false>
			<cfset result.Error = "You have not set up an email with the textID #emailTextID#">
			<cfreturn result>
		<cfelseif (not emailDetails.active) and (not test)> <!--- SSS 2009/02/24 Emails are now allowed to be set to inactive but you can still send a test if you like--->
			<cfset result.isOK = false>
			<cfset result.Error = "Email not active">
			<cfreturn result>
		<cfelseif arguments.personid is "">  <!--- WAB added 2007-06-06, occasionally blank personids are passed in (Trend in this case)--->
			<cfset result.isOK = false>
			<cfset result.Error = "Email failed - Invalid Person">
			<cfreturn result>
		</cfif>

			<!---  work out email addresses based on details in emailDef  --->
			<cfif recipientAlternativeEmailAddress neq "">
					<!--- specific email address passed into function --->
					<cfset result.toaddress = recipientAlternativeEmailAddress>
			<cfelse>

				<!--- NJH/WAB 2012/03/07 CASE 426905:
					Re-wrote a bit of the below code.. Essentially, we use the country and language of the recipientAlternativePersonID
					to translate the email into. If this hasn't been passed through, but a query is used to grab the people to send the email to,
					then we use the first person in the list as the recipientAlternativePersonID.

					The default it to use the country and language of the person that the email is about.
				 --->

				<cfif recipientAlternativePersonID neq "" or left(ltrim(emailDetails.toAddress),7) is "select ">
					<cfif recipientAlternativePersonID eq ""  and left(ltrim(emailDetails.toAddress),7) is "select ">
					<!--- email def defines a query which is run to bring back an emailaddress --->
					<!--- 	NJH 2016/09/20 JIRA PROD2016-379 replace the evaluate with a checkForAnEvaluate
							WAB 2017-01-24 RT-188 Knock on effect of above. Emails refering to #personid# not merging correctly (evaluate would have picked up arguments.personid, but personid is not in the merge struct).
								The merge system replaced #personid# with PERSONID which is sometimes a valid column in the query so the query doesn't fail but can bring back multiple results.
								Add personID to the merge struct (without overwrite)
								If the query errors during evaluation we will not send email - to prevent emails going to incorrect addresses
					--->
					<cfset arguments.mergeStruct.personID = arguments.personid>					
					<cfset var mergeObject = createObject ("component","com.mergeFunctions")>
					<cfset mergeObject.initialiseMergeStruct (mergeStruct = mergeStruct)>

					<cfset var emailQueryMergeResult = mergeObject.checkForAndEvaluateCFVariables_ReturningStructure(phrasetext = emailDetails.toAddress, mergeStruct = arguments.mergeStruct)>

					<cfif arrayLen(emailQueryMergeResult.errorArray) is 0>
						<cfset emailQuery = emailQueryMergeResult.phraseText>
					<cfelse>
						<cfset result.isOK = false>
						<cfset result.Error = "Email failed - Merge Error - See Error #emailQueryMergeResult.errorID#">
						<cfreturn result>
					</cfif>

					<cftry>
					<CFQUERY name="GetEmailAddress" datasource="#application.SiteDataSource#">
						#preservesinglequotes(emailQuery)#
					</CFQUERY>

						<cfcatch>
							<!--- if we're testing the sending of an email, don't throw an error --->
							<cfif not arguments.test>
								<cfrethrow>
							</cfif>
						</cfcatch>
					</cftry>
					<cfif listfindnocase(getEmailAddress.columnList,"email")>
						<cfset result.toAddress = valueList(GetEmailAddress.email,";")> 	<!--- WAB 2008/10/15 added value list, can bring back as many addresses as required --->
					</cfif>
					      <!--- if personID is in the query, then set the first person to be the alternative person recipient --->
					      <cfif listfindnocase(getEmailAddress.columnList,"personid")>
					            <cfif emailDetails.addressType is "personIDQuery">
						            <cfset arguments.PersonID = GetEmailAddress.personid>
						        <cfelse>
						            <cfset arguments.recipientAlternativePersonID = GetEmailAddress.personid>
						        </cfif>
					      </cfif>
					      <cfset result.sendToPersonID = GetEmailAddress.personid><!---DBB RT-21--->
					</cfif>

					<cfif recipientAlternativePersonID neq "" >
					      <CFQUERY name="getAlternativeRecipientEmail" datasource="#application.SiteDataSource#">
					      select email, lang.languageid, l.countryid, c.defaultLocale
					      from person p
					            join location l on p.locationid = l.locationid
					            join country c on c.countryid = l.countryid					<!--- 2013-07-02 PPB Case 435789 added join on country --->
					            left join language lang on p.language = lang.language
					      where personid =  <cf_queryparam value="#recipientAlternativePersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					      </CFQUERY>

					      <cfif getAlternativeRecipientEmail.languageid is not "">
					            <cfset result.translationCountryID = getAlternativeRecipientEmail.countryID>
					            <cfset result.translationLanguageID = getAlternativeRecipientEmail.LanguageID>
					      </cfif>
					      <cfset result.toaddress = getAlternativeRecipientEmail.email>
					      <cfset result.defaultLocale = getAlternativeRecipientEmail.defaultLocale>
					</cfif>
				</cfif>
			</cfif>

			<CFQUERY name="GetPersonDetails" datasource="#application.SiteDataSource#">
				SELECT
					Country.EmailFrom,
					Person.FirstName,
					Person.Title,
					Person.Username,
					Person.Password,
					Lang.LanguageId,
					Person.Salutation,
					Person.PersonId,
					Person.LastName,
					Person.Email,
					Person.Active,
					Person.OfficePhone AS DirectLine,	<!--- 2008-03-04 - Michael Roberts - Issue 149 - Added --->
					Loc.CountryId,
					Loc.LocationId,
					Loc.SiteName,
					Loc.Address1,
					Loc.Address2,
					Loc.Address3,
					Loc.Address4,
					Loc.Address5,		<!--- 2008-06 WAB, MR below managed to remove Address5 so other emails using it aren't working, so added back --->
					Loc.Address5 AS State,	<!--- 2008-03-04 - Michael Roberts - Issue 149 - Added --->
					Loc.PostalCode,
					Loc.Address6, <!--- NJH 2009/02/27 - Bug Fix Sony Issue 1913 - added remainder of address fields --->
					Loc.Address7,
					Loc.Address8,
					Loc.Address9,
					Loc.Address10,
					Loc.nAddress1,
					Loc.nAddress2,
					Loc.nAddress3,
					Loc.nAddress4,
					Loc.nAddress5,
					CountryDescription AS Country,
					Country.defaultLocale,
					CASE
						WHEN Lang.LanguageId IS NOT NULL THEN CONVERT(varchar,Lang.LanguageId)
						WHEN Country.DefaultLanguageId IS NOT NULL THEN Country.DefaultLanguageId
						ELSE '1'
					END AS LanguageToUse,   <!--- WAB 2007-04-25 note that country.defaultLanguageid can be a comma separated list - hence the need to varchar things.  not need to listfirst when being used --->
					Org.OrganisationName,
					Org.OrganisationID,
					Org.VatNumber,			<!--- 2011/07/19	NAS	P-LEX053 --->
					<!---
					WAB removed 2008/04/09 and replaced with sql which added the http instead of the loop below
					Org.OrgURL AS OrgURL	<!--- 2008-03-04 - Michael Roberts - Issue 149 - Added as this is a link in the email required ---> --->
					case when orgurl <> '' and left (ltrim(orgurl),4) <> 'http' then 'http://' + orgurl else ltrim(orgurl) end as OrgURL,
					Country.defaultlocale
				FROM
					Country
					INNER JOIN Location Loc ON Country.CountryID = Loc.CountryID
					INNER JOIN Person ON Loc.LocationID = Person.LocationID
					LEFT JOIN Language Lang ON Lang.Language = Person.Language
					INNER JOIN Organisation Org ON Org.OrganisationId = Person.OrganisationId
				WHERE Person.PersonId  IN ( <cf_queryparam value="#arguments.PersonId#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>

			<cfset result.defaultLocale = "">
			<cfif arguments.personid is not request.relayCurrentUser.personid>
				<cfset result.defaultLocale = GetPersonDetails.defaultLocale>
			</cfif>

			<cfset result.Person.Locationid = GetPersonDetails.locationid>
			<cfset result.Person.Personid = GetPersonDetails.personid>

			<cfif GetPersonDetails.personid is "" and recipientAlternativeEmailAddress is "" and arguments.personID neq 0>  <!--- 2013-07-03	WAB/MS	P-KAS022 Send Emails to Personid=0 ---> <!--- WAB added 2007-05-15, occasionally deleted personids are passed in and fail.  However 2008/07/09 if we have an alternativerecipientaddress then doesn't matter --->
				<cfset result.isOK = false>
				<cfset result.Error = "Email failed - Invalid Person">

				<cfreturn result>
			</cfif>

			<cfif getpersondetails.personID is request.relaycurrentuser.personid>
				<cfset result.translationCountryID = arguments.countryID>
				<cfset result.translationLanguageID = arguments.LanguageID>
			<cfelseif getpersondetails.personID is "">    <!--- WAb 2008/07/09 to deal with no personid and an alternativeRecipientEmail address --->
				<cfset result.translationCountryID = request.relaycurrentuser.countryID>
				<cfset result.translationLanguageID = request.relaycurrentuser.LanguageID>
			<cfelse>
				<!--- email not being sent to the current user, so use the language calculated in the query above unless overridden by arguments--->
				<cfset result.translationCountryID = iif(arguments.countryID is not "",arguments.countryID,GetPersonDetails.countryid)>
				<cfset result.translationLanguageID = iif(arguments.LanguageID is not "",arguments.LanguageID,listfirst(GetPersonDetails.LanguageToUse))>
			</cfif>

			<!--- 2014/11/14	YMA	We don't want to change the toAddress in result if it was already set by arguments.recipientAlternativeEmailAddress --->
			<cfif arguments.recipientAlternativeEmailAddress eq "">
				<!--- AXA CASE 444047 fixed bug in calculateemailcontent --->
				<cfif emailDetails.addressType is "literal" and emailDetails.toAddress is not "" AND NOT(left(ltrim(emailDetails.toAddress),7) is "select ")>
					<!--- email def defines an email address (or a phrase containing one) --->
					<cfset result.toAddress = translateEmailAddressList(Addresslist = emailDetails.toAddress, countryid = result.translationCountryID, language=result.translationLanguageID )>
				<cfelse>
					<!--- START: AXA CASE 444047 fixed bug in calculateemailcontent --->
					<cfif NOT structKeyExists(result,'toAddress') OR len(trim(result.toAddress)) EQ 0>
						<!--- 2012/03/26	IH	Case 427195 Don't send out emails to inactive people --->
						<cfif Getpersondetails.Active is 1>
							<CFSET result.toAddress = Getpersondetails.email>
						<cfelse>
							<cfset result.isOK = false>
							<cfset result.Error = "Email failed - Inactive Person">
							<cfreturn result>
						</cfif>
					</cfif>
					<!--- END: AXA CASE 444047 fixed bug in calculateemailcontent --->
				</cfif>
			</cfif>

			<cfset result.ccAddress = translateEmailAddressList(addresslist = emailDetails.ccAddress, countryid = result.translationCountryID, language=result.translationLanguageID )>
			<cfset result.ccAddress  = listappend(result.ccAddress,arguments.ccAddress,",")> <!--- NJH 2007/09/10 append email address passed through as an argument --->

			<cfset result.bccAddress = translateEmailAddressList(addresslist = emailDetails.bccAddress, countryid = result.translationCountryID, language=result.translationLanguageID )>
			<cfset result.bccAddress = listappend(result.bccAddress,bcCopy,",")>
			<cfset result.bccAddress = listappend(result.bccAddress,application.copyallemailstothisaddress,",")>

			<!--- WAB 2012-09-19 default the from address to the same as we use in communications --->
			<cfif emailDetails.fromAddress is "">
				<cfset result.fromAddress = "#application.EmailFromDisplayName# <#application.emailFrom#@#application.mailfromdomain#>">
			<cfelse>
				<CF_EvaledString stringtoeval=#emailDetails.fromAddress#>
				<CFSET result.fromAddress = EvaledString>
				<cfset result.fromAddress = translateEmailAddressList(addresslist = result.fromAddress, countryid = result.translationCountryID, language=result.translationLanguageID )>
			</cfif>

			<cfset form.toaddress = result.toAddress>

			<cfif GetPersonDetails.recordcount><!--- 2013-07-03	WAB/MS	P-KAS022 Send Emails to Personid=0 --->

				<cfset tmpMergeStruct.recipient = application.com.structureFunctions.queryRowToStruct(getPersonDetails,1)>
				<!--- WAB 2008/06/25 add a field called address which is a preformatted address on separate line (for more flexibilty use func.outputaddress() ) --->
				<cfset tmpMergeStruct.recipient.address = application.com.screens.evaluateAddressFormat(location = getpersondetails,showCountry= true,separator="<BR>",finalcharacter="")>
				<cfset tmpMergeStruct.recipient.magicnumber= application.com.LOGIN.getMagicNumber(Getpersondetails.personid)>
				<cfset tmpMergeStruct.recipient.orgmagicnumber= application.com.LOGIN.getMagicNumber(entityID=Getpersondetails.OrganisationID,entityTypeID=2)>
				<!--- WAB 2013-01-10 CASE 437262 when email sent as a housekeeping task there is not a relayCurrentUser, so will test for it
						- a bit nasty but will have to do - shouldn't really be using pNumber these days anyway
				 --->
				<cfif structKeyExists (request.relaycurrentuser,"pNumber")>
					<cfset tmpMergeStruct.sendermagicnumber= request.relaycurrentuser.pNumber>
				</cfif>
				<cfset tmpMergeStruct.entityID= entityID>

				<!--- for backwards compatability all the items in the recipient key need to be available without the "recipient." --->
				<!--- 2009/05/20 GCC LID 2143 use passed in merge as pref to default merge --->
				<cfloop item="key" collection = #tmpMergeStruct.recipient#>
					<cfif not structKeyExists(tmpMergeStruct,key)>
						<cfset tmpMergeStruct[key] = tmpMergeStruct.recipient[key]>
					</cfif>
				</cfloop>

			</cfif><!--- 2013-07-03	WAB/MS	P-KAS022 Send Emails to Personid=0 --->


			<!--- WAB 2008/06/19 this allows us to pass location query into the address formatting function func.outputaddress
				WAB 2012-10-05 this is no longer needed because merge.cfc will generate the location query automatically if required.  And was causing problems because merge.getLocation() could not deal with a location query which did not contain all the location fields
			<CFSET tmpMergeStruct.Location =  GetPersonDetails>
			--->

			<!--- append message phrases here... don't append if not sending message --->
			<CFSET phrases = emailDetails.bodyPhraseTextID & "," & emailDetails.subjectPhraseTextID>
			<cfif emailDetails.sendMessage>
				<cfset phrases = listAppend(phrases,emailDetails.messageTitlePhraseTextID)>
				<cfset phrases = listAppend(phrases,emailDetails.messageTextPhraseTextID)>
			</cfif>

			<!--- WAB can now run a query specific to the email def, allowed to reference #entityID#
				parameter name can be Query (in which case fields are referenced as query.) or in form query_myQuery (in which case fields are referenced as myQuery.)
				WAB 2008/11/05 put the custom queries into their own structure so moved some of this code to the eMailDetailsFunction (which must be released at same time)
			--->
			<cfloop item="queryName" collection = "#emailDetails.customQueries#">
					<cfset query = application.com.relayTranslations.checkForAndEvaluateCFVariables(phrasetext = emailDetails.customQueries[queryName], entityId = entityID)>
					<CFQUERY name="tempQuery" datasource="#application.SiteDataSource#">
						#query#
					</CFQUERY>

					<cfset tmpMergeStruct[queryName] = tempQuery>
			</cfloop>

			<cfset request.emailmerge = application.com.emailmerge>   <!--- rather a hack way of getting access to these functions, really ought to be combined with functionsAllowedInPhrases.cfc --->
			<cfset request.commitem = tmpMergeStruct>
			<cfset request.commitem.personid = personid>  <!--- WAB 2007/11/21 this is a bit of a hack!  emailmerge.cfc was designed for use with comms and expects to be passed request.commitem.personid.  One day will replace with a proper object--->

			<!--- WAB for new merge system --->
			<cfset tmpMergeStruct.context = "emailDef">
			<cfset tmpMergeStruct.comm = emailDetails>
			<cfset tmpMergeStruct.comm.test = arguments.test>
			<cfset tmpMergeStruct.comm.portalURL = application.com.relaycurrentsite.getReciprocalExternalProtocolAndDomain()>

			<!--- set locale --->
			<cfif result.defaultLocale is not "">
				<cfset localePos=listFindNoCase(Server.Coldfusion.SupportedLocales,result.defaultLocale)>
				<cfif localePos>
					<cfset result.defaultLocale = listgetat(server.ColdFusion.SupportedLocales,localePos)>
                    <cftry>
    					<cfset request.oldLocale = setlocale(result.defaultLocale)>
                    <cfcatch type="any">
                    </cfcatch>
                    </cftry>
				<cfelse>
					<cfset result.defaultLocale="">
				</cfif>
			</cfif>

			<!--- WAB  2005-07-26  problem getting correct language for the person
			if the person is the current user , then leave cf_translate to do its stuff, otherwise pass in the person's language, or the default language for the country
			--->
			<cfset logMergeErrors = Not(test)>  <!--- ie if it is a test then don't send the warning emails --->
			<cf_translate phrases = "#phrases#"  countryid = #result.translationCountryID# language=#result.translationLanguageID# mergeStruct=#tmpMergeStruct# exactmatch = false logMergeErrors= #logMergeErrors# / >

			<cfif result.defaultLocale is not "" and structKeyExists(request,"oldLocale")>
                <cftry>
    				<cfset setlocale(request.oldLocale)>
                <cfcatch type="any">
                </cfcatch>
                </cftry>
			</cfif>

			<CFSET result.Body=request.translations[trim(emailDetails.bodyPhraseTextID)]>
			<CFSET result.Subject=request.translations[trim(emailDetails.subjectPhraseTextID)]>

			<cfset result.sendMessage = emailDetails.sendMessage>
			<cfif emailDetails.sendMessage>
				<cfset result.messageTitle = request.translations[trim(emailDetails.messageTitlePhraseTextID)]>
				<cfset result.messageText = request.translations[trim(emailDetails.messageTextPhraseTextID)]>
				<cfset result.messageUrl = application.com.relayTranslations.checkForAndEvaluateCFVariables(phrasetext = emailDetails.messageUrl, entityId = entityID, mergeStruct=tmpMergeStruct)>
				<cfset result.messageImage = emailDetails.messageImage>
			</cfif>

			<CFIF isDefined("emailDetails.includeTemplate") and emailDetails.includeTemplate neq "">
				<CFINCLUDE TEMPLATE="#emailDetails.includeTemplate#">
			</CFIF>

			<!--- if the content contains <HTML then must be an HTML email
				Added by WAB 2005-01-19
			--->
			<cfif result.Body contains "<HTML" or result.Body contains "<BR>"> <!--- wab added <BR> --->
				<cfset result.cfmailType = "HTML">
			<cfelse>
				<cfset result.cfmailType = cfmailType>
			</cfif>

			<!--- check whether email is being sent to the actual person given by the personid --->
			<cfif result.toaddress  IS NOT GetPersonDetails.email AND GetPersonDetails.recordCount is not 0>
				<cfset result.NotSentToMergedPerson = true>
			<cfelse>
				<cfset result.NotSentToMergedPerson = false>
			</cfif>

			<cfset result.relatedEntityIDs = getRelatedEntityIDsFromMergeStruct(mergeStruct)>

			<cfreturn result>

	</cffunction>

	<cffunction NAME="translateEmailAddressList" access="private" returns="string">
		<cfargument name="addresslist" required = true>
		<cfargument name="language" required = true>
		<cfargument name="countryid" required = true>

		<cfset var result = "">
		<cfset var address = "">
		<!--- WAB 2013-01-10 CASE 437262 Need to set context to emailDef, otherwise get problems in merge.cfc if an email is sent by a housekeeping task because looks for relayCurrentUser.person which is not in existence --->
		<cfset var tmpMergeStruct = {context = "emailDef"}>

		<cfloop list="#addresslist#" index="address">
			<!--- WAB 2014-09-17 CASE 441711, protect against leading/trailing spaces on the address --->
			<cfset address = trim(Address)>
			<cfif Address does not contain "@" and Address is not "" and Address does not contain " ">   <!--- need regexp here --->
				<cf_translate phrases = "#Address#"  countryid = #CountryID# language=#Language# exactmatch = false onNullShowPhraseTextID = true/>
				<cfset result = listappend(result,request.translations[address])>
			<cfelse>
				<cfset result = listappend(result,address)>
			</cfif>

		</cfloop>

		<cfreturn result>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	     SWJ:    2005-08-26 Added isValidEmail function
		 Usage:  <cfif application.com.email.isValidEmail(emailAddress)>
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="isValidEmail" access="public" returntype="boolean" hint="Checks that the email string passed is a valid email address.">
		<cfargument name="emailAddress" type="string" required="yes">

        <!--- 20/01/2016 DAN 447651 --->
        <cfreturn REFindNoCase("^[a-zA-Z_0-9-'\+~]+(\.[a-zA-Z_0-9-'\+~]+)*@([a-zA-Z_0-9-]+\.)+[a-zA-Z]{2,}$", arguments.emailAddress)>
	</cffunction>


	<cffunction name="updateEmailDefCommID" access="public" returntype="numeric" >
		<cfargument name="emailDefID" type="string" required="yes">
		<cfset var emailDetails="">
		<cfset var updateEmaildef="">
		<cfset var commid="">

		<CFQUERY NAME="emailDetails" DATASOURCE="#application.siteDataSource#">
			SELECT
				e.commid AS CommID,
				c.commid AS comm_commid,
				e.emailTextID
			FROM
				emailDef e
				LEFT JOIN communication c ON e.commid = c.commid
			WHERE
				emailDefID =  <cf_queryparam value="#arguments.emailDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfif emailDetails.recordCount is 0>
			<cfreturn 0>
		<cfelseif emailDetails.commid is not "">
			<cfreturn emailDetails.commid>
		<cfelse>
			<!--- 2013-08-15	YMA		Case 436313 frmDisplayName is now refered to as frmcommFromDisplayNameID. --->
			<cfset commid = application.com.communications.insertCommunication(
			frmProjectRef = "", frmCampaignID = 0, frmTitle = emailDetails.emailtextid,
			frmDescription = "", frmReplyTo = "", frmfromAddress = "", frmcommFromDisplayNameID = "",
			frmCommTypeLID = 98, frmCommFormLID = 0, frmtrackemail = 0, frmNotes="")>
	<!--- note: commformlid needs to be fixed --->
			<CFQUERY NAME="updateEmaildef" DATASOURCE="#application.siteDataSource#">
				UPDATE emaildef
				SET commid =  <cf_queryparam value="#commid#" CFSQLTYPE="CF_SQL_INTEGER" >
				WHERE emailDefID =  <cf_queryparam value="#arguments.emailDefID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<cfreturn commid>
		</cfif>

	</cffunction>


<!---
***************************
WAB 2009/01/29
This section deals with doctoring email addresses on non live sites so that all emails all go to a particular email address

Use in conjunction with tag customtags\mail\mail.cfm

These functions broken out so that they can be use on their own.  For example to predict what is going to happen to a given email address

***************************
 --->


	<cffunction name=	"doEmailsNeedToBeRedirected" access="public" returntype="boolean" >

		<cfreturn application.testSite>

	</cffunction>


	<cffunction name="getCurrentEmailRedirectionParameters" access="public" returntype="struct" >

		<!---
			WAB 2011/10/12 added caching of this structure in requestScope
			Was worried that might be causing memory problems when sending huge comms (of course only accessed on test sites)
		--->

		<cfset var result = structNew()>
		<cfset var defaultEmailRedirectionParameters = "">

		<cfif not structKeyExists(request,"CurrentEmailRedirectionParameters")>

			<cfset defaultEmailRedirectionParameters = getDefaultEmailRedirectionParameters()>

			<!--- regular expression which defines the allowed addresses
				@foundation-network.com is always allowed, and any others can be set in an application variable
				session based ones can be added as well - not sure whether this overrides the default @foundation-network.com, and the application settings or is appended to them
			--->
			<cfif structKeyExists(session,"emailRedirection") and structKeyExists(session.emailRedirection,"allowedEmailAddressRegExp") <!--- and session.emailRedirection.allowedEmailAddressRegExp is not "" --->>
				<cfset result.allowedEmailAddressRegExp = session.emailRedirection.allowedEmailAddressRegExp>
				<cfset result.hasSessionallowedEmailAddressRegExp = true>
			<cfelse>
				<cfset result.allowedEmailAddressRegExp = defaultEmailRedirectionParameters.permanentAllowedEmailAddressRegExp>
				<cfif defaultEmailRedirectionParameters.customerAllowedEmailAddressRegExp is not ""> <!--- Wab 2011/05/02 removed application.testSite is not 2, this now done in getDefaultEmailRedirectionParameters()--->
					<cfset result.allowedEmailAddressRegExp = listAppend(result.allowedEmailAddressRegExp,defaultEmailRedirectionParameters.customerAllowedEmailAddressRegExp,"|")>
				</cfif>

				<cfset result.hasSessionallowedEmailAddressRegExp = false>
			</cfif>


			<!--- email address to send all emails to defaults to devHelp2@foundation-network.com, but can be replaced at application or session level --->
			<cfif structKeyExists(session,"emailRedirection") and structKeyExists(session.emailRedirection,"replacementEmailAddress") and session.emailRedirection.replacementEmailAddress is not "">
				<cfset result.replacementEmailAddress = session.emailRedirection.replacementEmailAddress>
				<cfset result.hasSessionreplacementEmailAddress = true>
			<cfelse>
				<!--- Not that the replacementEmailAddress can be different if we are on a Relayware breakout, but this is dealt with in the getDefaultEmailRedirectionParameters function --->
				<cfset result.replacementEmailAddress = defaultEmailRedirectionParameters.replacementEmailAddress>
				<cfset result.hasSessionreplacementEmailAddress = false>
			</cfif>

			<cfset request.CurrentEmailRedirectionParameters = result>
		<cfelse>
			<cfset result = request.CurrentEmailRedirectionParameters >
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="updateSessionReplacementEmailAddress" access="public" returntype="boolean">
		<cfargument name="ReplacementEmailAddress" type="string" required="yes">

		<cfset var result = true>

		<cfif ReplacementEmailAddress is "" >
			<cfif structKeyExists (session, "emailRedirection")>
				<cfset structDelete (session.emailRedirection,"replacementEmailAddress")>
			</cfif>
		<cfelseif ReplacementEmailAddress is getDefaultEmailRedirectionParameters().ReplacementEmailAddress>
			<!--- this is the same as the default value (application variable) so don't set session variable --->

		<cfelseif isValidEmail(ReplacementEmailAddress)>
			<cfset session.emailRedirection.replacementEmailAddress = ReplacementEmailAddress>
		<cfelse>
			<!--- Invalid Email --->
			<cfset result = false>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="addSessionallowedEmailAddressRegExp" access="public" returntype="boolean">
		<cfargument name="RegExp" type="string" required="yes">

		<cfset var result = true>
		<cfset var defaultEmailRedirectionParameters = getDefaultEmailRedirectionParameters()>

		<cfif len(RegExp) lt 4 >  <!--- regExp must be longer than 3 characters, prevents user entering something like '@a' which would allow thousands of emails through --->
			<cfset result = false>
		<cfelseif not structKeyExists (session,"emailRedirection") or not structKeyExists(session.emailRedirection,"allowedEmailAddressRegExp") >
			<!--- first item to be added to list, start off by taking the application version into the session variable, adding in fnl and then add our new regExp --->
			<cfset session.emailRedirection.allowedEmailAddressRegExp = listAppend (listAppend(defaultEmailRedirectionParameters.permanentAllowedEmailAddressRegExp, defaultEmailRedirectionParameters.customerAllowedEmailAddressRegExp,"|"),RegExp,"|")>
		<cfelseif listfindnocase (session.emailRedirection.allowedEmailAddressRegExp,RegExp,"|") is 0>
			<!--- not already in list so add --->
			<cfset session.emailRedirection.allowedEmailAddressRegExp = listAppend (session.emailRedirection.allowedEmailAddressRegExp,RegExp,"|")>
		<cfelse>
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="removeSessionallowedEmailAddressRegExp" access="public" returntype="boolean">
		<cfargument name="RegExpList" type="string" required="yes">

		<cfset var result = true>
		<cfset var defaultEmailRedirectionParameters = getDefaultEmailRedirectionParameters()>
		<cfset var position = '' />
		<cfset var regExp = '' />

		<cfif not structKeyExists (session,"emailRedirection") or not structKeyExists(session.emailRedirection,"allowedEmailAddressRegExp")>
			<!--- session structure does not exist and trying to remove items from it
				must be trying to remove default items, so we are going to have to create the session variable with the default items in it and then remove the appropriate one
			 --->
			<cfparam name="session.emailRedirection" default = "#structNew()#">
			<cfset session.emailRedirection.allowedEmailAddressRegExp = listAppend(defaultEmailRedirectionParameters.permanentAllowedEmailAddressRegExp, defaultEmailRedirectionParameters.customerAllowedEmailAddressRegExp,"|")>
		</cfif>

			<cfloop index="regExp" list ="#regExpList#">
				<cfset position = listFindNoCase (session.emailRedirection.allowedEmailAddressRegExp,regExp,"|")>
				<cfif position is not 0>
					<cfset session.emailRedirection.allowedEmailAddressRegExp = listDeleteAt(session.emailRedirection.allowedEmailAddressRegExp,position,"|")>
				</cfif>
			</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="restoreDefaultAllowedEmailAddressRegExp" access="public" returntype="boolean">
		<cfset structDelete (session.emailRedirection,"allowedEmailAddressRegExp")>
		<cfreturn true>
	</cffunction>


	<cffunction name="getDefaultEmailRedirectionParameters" access="public" returntype="struct">

		<cfset var result = application.com.settings.getSetting("emailRedirection")>
		<!--- If we are on a relayware breakout then have we have a different replacementEmailAddress--->
		<cfif application.com.security.isIpAddressRelayWareInc()>
			<cfset result.replacementEmailAddress = result.replacementEmailAddress_RelayWareBreakOut>
		</cfif>
		<!---
			On a test site we don't take account of the customerAllowedEmailAddressRegExp [would be better dealt with by settings which were testsite specific, but I haveb't coded that yet!]
			WAB 2011/05/02 added this in here, testsite was previously being tested elsewhere in a later procedure but better to do it at lower level
		--->
		<cfif application.testSite is 2>
			<cfset result.customerAllowedEmailAddressRegExp = "">
		</cfif>
		<cfreturn result>

	</cffunction>


	<cffunction name="doctorEmailAddressListForRedirection" access="public" returntype="string" >
		<cfargument name="emailAddressList" type="string" required="yes">

			<cfset var newAddressList  = "">
			<cfset var thisAddress  = "">
			<cfset var parameters  = getCurrentEmailRedirectionParameters()>
			<cfset var newAddress = '' />

				<cfloop index = "thisAddress" list = #emailAddressList# delimiters=",;">
					<cfif parameters.AllowedEmailAddressRegExp is not "" and  reFindNoCase(parameters.AllowedEmailAddressRegExp,thisAddress)>
						<cfset newAddressList = listappend(newAddressList,thisAddress)>
					<cfelse>
						<cfset newAddress = replace (replace (replace (thisAddress,"@"," @ ","ALL"),">","]","ALL"),"<","[","ALL")>
						<cfset newAddress = newAddress & "<" & parameters.replacementEmailAddress  & ">">
						<cfset newAddressList = listappend(newAddressList,newAddress)>
					</cfif>
				</cfloop>

		<cfreturn newAddressList>

	</cffunction>

	<!--- NJH 2012/12/17 - 2013 Roadmap --->
	<cffunction name="isEmailBlackListed" access="public" returnType="boolean">
		<cfargument name="emailAddress" type="string" required="true">

		<cfset var emailFieldStruct = application.com.relayEntity.getTableFieldStructure(tablename="person",fieldname="email")>

		<cfif emailFieldStruct.hasBlackList and reFindNoCase(valueList(emailFieldStruct.blackList.query.data,"|"),arguments.emailAddress)>
			<cfreturn true>
		</cfif>

		<cfreturn false>
	</cffunction>

	<cffunction name="getEmailStyles" access="public" returnType="query">
		<cfset var returnVar = queryNew('name')>
		<!--- <cfset var thisDirectory = "#application.userfilesabsolutepath#\Content\styles\emailstyles\">

		<cfif directoryExists(thisDirectory)>
			<cfdirectory action="list" directory="#thisDirectory#" name="returnVar" recurse="no" filter="*.css">
		</cfif> --->
		<!--- NJH 2013/09/20 2013Roadmap2 sprint 1- altered to use file library.. --->
		<cfset var fileTypeID = application.com.filemanager.getFileTypes(type="Communications",fileTypeGroup="stylesheets").fileTypeID[1]>

		<!--- NJH 2016/10/14 - JIRA PROD2016-2406  - alias the column names--->
		<cfreturn application.com.fileManager.getFilesAPersonHasViewRightsTo(returnColumns="f.filename,f.name",fileTypeID=fileTypeID,orderby="f.name")>
	</cffunction>

	<!--- 2013-06-21	YMA	Calculate the content of an email definition by its module. See ModuleEntityStruct currently defined at the beginning of this file. --->
	<cffunction name="testEmail" access="public" returnType="struct">
		<cfargument name="emailDefID" type="numeric" required=true>
		<cfargument name="frmCountryID" type="numeric" default=#request.relaycurrentuser.languageid#>
		<cfargument name="frmLanguageID" type="numeric" default=#request.relaycurrentuser.countryid#>
		<cfargument name="frmShowContent" default="0"  required=true>

		<!--- 2013/11/26	YMA	Case 438024 pass in variable 'test' so email content gets calculated when email is set to inactive. --->
        <!---ESZ 16/09/2016 PROD2016-379 Standard Emails - The 'preview' button is not working.--->
		<cfreturn calculateEmailContent(emailTextID=emaildefID,personid=request.relaycurrentuser.personid, languageid = frmLanguageID, countryid = frmCountryid, test=true, mergeStruct = getEntityMergeStruct(arguments.emailDefID))>
	</cffunction>

	<!--- 2014-07-01 PPB Case 438224 check if an email has already been sent --->
	<cffunction name="getLastEmailSent" access="public" returnType="struct" hint="Returns a structure containing the date the most recent email was sent" output="false">
		<cfargument name="personID" type="numeric" required="yes">
		<cfargument name="emailTextID" type="string" required="yes">

		<cfset var qryEmailHistory = "">
		<cfset var returnStruct = StructNew()>

		<cfquery name="qryEmailHistory" datasource="#application.siteDataSource#">
			SELECT MAX(cd.DateSent) AS DateSent
			FROM commdetail cd WITH (nolock)
			INNER JOIN Communication c WITH (nolock) ON c.CommId=cd.CommId AND c.Title =  <cf_queryparam value="#arguments.emailTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			WHERE cd.personId = '#arguments.personID#'
		</cfquery>

		<cfset returnStruct.DateSent = qryEmailHistory.DateSent>

		<cfreturn returnStruct>
	</cffunction>

    <!--- 2015-10-13  DAN PROD2015-160 --->
    <cffunction name="getEmailDefIDByTextID" access="public" returntype="numeric" hint="Returns emaildefID for the given emailTextID or 0 if email def not found">
        <cfargument name="emailTextID" type="string" required="yes">

		<cfset var qEmailDefIDByTextID = "">
        <CFQUERY NAME="qEmailDefIDByTextID" DATASOURCE="#application.siteDataSource#">
            select emailDefID from emailDef where emailTextID = <cf_queryparam value="#trim(arguments.emailTextID)#" CFSQLTYPE="cf_sql_varchar">
        </CFQUERY>

        <cfif qEmailDefIDByTextID.recordcount gt 0>
            <!--- it should only be one record but in case there are existing duplicates --->
            <cfreturn qEmailDefIDByTextID.emailDefID[1]>
        </cfif>

        <cfreturn 0>
    </cffunction>

    <!--- start: 2015-10-29 DAN PROD2015-281 - support for case 446369 --->
    <cffunction name="hasEmailBeenSent" access="public" returntype="boolean" hint="Returns true if email has been sent to the specified personId based on given emailTextID and optionally relatedEntityId.">
        <cfargument name="emailTextId" type="string" required="yes">
        <cfargument name="personId" type="numeric" required="yes">
        <cfargument name="relatedEntityId" type="numeric" default="0"> <!--- this is optional so we can also check if just email has been sent regardless the entity --->
        <cfargument name="relatedEntityTypeId" type="numeric" default="-1"> <!--- there is an entityType = 0 --->

		<cfset var qhasEmailBeenSent = "">
        <cfquery name="qhasEmailBeenSent" datasource="#application.siteDataSource#">
            SELECT 1 from commdetail cd inner join communication c on c.commId=cd.commId
            <cfif arguments.relatedEntityId GT 0>
                inner join relatedEntity re on re.entityID=cd.commDetailId
            </cfif>
                where c.title = <cf_queryparam value="#trim(arguments.emailTextId)#" CFSQLTYPE="cf_sql_varchar">
                and cd.personId = <cf_queryparam value="#arguments.personId#" CFSQLTYPE="cf_sql_integer">
            <cfif arguments.relatedEntityId GT 0>
                and re.entityTypeId = <cf_queryparam value="#application.entityTypeID.commdetail#" CFSQLTYPE="cf_sql_integer">
                and re.relatedEntityId = <cf_queryparam value="#arguments.relatedEntityId#" CFSQLTYPE="cf_sql_integer">
                and re.relatedEntityTypeId = <cf_queryparam value="#arguments.relatedEntityTypeId#" CFSQLTYPE="cf_sql_integer">
            </cfif>
        </cfquery>

        <cfreturn qhasEmailBeenSent.recordCount GT 0>
    </cffunction>
    <!--- end: 2015-10-29 DAN PROD2015-281 - support for case 446369 --->

	<cffunction name="convertEmailToUnicode" access="public" output="false" hint="Convert an email domain that has been converted by the browser to punycode back to unicode">
       <cfargument name = "email" type="string" required="false">

       <cfset var result = email>

       <cfif listLen (arguments.email,"@") is 2>
			<cfset var domain = listLast(arguments.email,"@")>
			<cfset var unicodeDomain = CreateObject( "java", "java.net.IDN" ).toUnicode(domain) />
			<cfset result = listFirst(arguments.email,"@") & "@" & unicodeDomain>
       </cfif>

       <cfreturn result>
</cffunction>

<!---ESZ 16/09/2016 PROD2016-379 Standard Emails - The 'preview' button is not working.--->
	<cffunction  name="getEntityMergeStruct" access="public" returnType="struct" hint="Returns a merge structure with various entityIds that are associated with a given module">
		<cfargument name="emailDefID" type="numeric" required=true>

		<cfset var mergeStruct = {}>
		<cfset var getMaxEntityID = "">
		<cfset var getEntityName = "">

		<cfset var moduleID = getEmailDef(emailDefID).moduleTextID>
		<cfif moduleID neq "">
			<cfset var module = application.com.relayMenu.getActiveModules(moduleID=moduleID)>
			<cfquery name="getEntityName">
				  select uniqueKey,entityName
				  from schemaTable s
				  	inner join relayModuleEntityType me on me.entityTypeID = s.entityTypeID
				  	inner join relayModule m on m.moduleID = me.relayModuleID
				  where m.moduleTextID = <cf_queryparam value="#module.moduleTextID#" cfsqltype="cf_sql_varchar">
			</cfquery>

			<cfloop query="getEntityName">
				<cfquery name="getMaxEntityID">
                    select max(#getEntityName.uniqueKey#) as ID from #getEntityName.entityName#
				</cfquery>
				<cfset mergeStruct[getEntityName.uniqueKey] = getMaxEntityID.ID>
			</cfloop>

		</cfif>
		<cfreturn mergeStruct>
	</cffunction>

</cfcomponent>
