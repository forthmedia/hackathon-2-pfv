<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2009/04/27 look for relaytageditors using CF_PARAMXML
WAB 2011/11/09 relayTagEditor from cf_param was not working properly
WAB 2011/11/16 mods to addMergeFieldToQuery() - then needed to modify getmergefunctions()
WAB 2011/12/05 Lots of work to generate automatic editors
WAB 2012-11-19 CASE 432097 code to check whether relaytag is a custom tag was failing
WAB 2013-12-11 CASE 438377 Fixed error in when updateRelayTagStructure when finds a cf_include for which onNotExistsTemplate did not exist
WAB 2015-03-25 CASE 444232 RelayTags failing to load - QoQ problem with dataType of moduleNotActive
NJH 2016/01/05 Jira BF-34 - re-write code so that we get the proper name of the merge fields to insert, regardless of the number of levels involved.
--->

<cfcomponent displayname="Relay Tag Functions" hint="Contains a number of commonly used Relay Tag Functions">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          Adds ModuleName to query containing moduleID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="updateRelayTagStructure">
		<cfargument name="applicationScope" default="#application#">


		<cfset var relayTagXMLUserFiles = '' />
		<cfset var newXML = '' />
		<cfset var relayTagsOnList = '' />
		<cfset var relayTagsOffList = '' />
		<cfset var counter = '' />
		<cfset var member = '' />
		<cfset var thisTag = '' />
		<cfset var moduleDetail = '' />
		<cfset var tagPathRelay = '' />
		<cfset var tagPathUserFiles = '' />
		<cfset var thefile = '' />
		<cfset var testEditorName = '' />
		<cfset var tempRelayTagKeyedByTagName = '' />
		<cfset var structureFunctions = createObject("component","/relay/com/structureFunctions")>
		<cfset var tempQuery = '' />
		<cfset var relayTagXML = '' />
		<cfset var tempRelayTagStruct = '' />
		<cfset var i = '' />
		<cfset var relayTagsOn = '' />
		<cfset var relayTagsOff = '' />
		<cfset var activeRelayTags = '' />
		<cfset var theFileContent = ''>
		<cfset var thisInclude = ''>
		<cfset var incTemplate = ''>

		<cftry>
				<cffile action="read"
	  			file="#applicationScope.paths.relayware#\xml\relayTags.xml"
	  			variable="relayTagXML">

				<cfif fileexists("#applicationScope.paths.abovecode#\xml\relayTags.xml")>
					<cffile action="read"
		  			file="#applicationScope.paths.abovecode#\xml\relayTags.xml"
		  			variable="relayTagXMLUserFiles">
				<cfelse>
					<cfset relayTagXMLUserFiles = "">
				</cfif>

				<cfcatch type="Any">Relay Tag XML file not found</cfcatch>
		</cftry>


		<!--- amazingly this seems to work to join the two xml files together!! WAB --->
		<cfset newXML = "<dummy>
						#relayTagXML#
						#relayTagXMLUserFiles#
						</dummy>">
		<cfscript>
		  	relayTagXML = XmlParse("#newXML#");
			var relayTagArray = XmlSearch(relayTagXML, "//RelayTags/RelayTag");
		</cfscript>


		<!--- get list of tags which are switched on / off --->
		<cfquery name="relayTagsOn" datasource = "#applicationScope.sitedatasource#">
		select relayTag from relayTag where active = 1
		</cfquery>
		<cfset relayTagsOnList = valueList (relayTagsOn.relayTag)>
		<cfquery name="relayTagsOff" datasource = "#applicationScope.sitedatasource#">
		select relayTag from relayTag where active = 0
		</cfquery>
		<cfset relayTagsOffList = valueList (relayTagsOff.relayTag)>

		<cfscript>
			// Create Custom Insert Strucure    - this is used by devEdit
			tempRelayTagStruct = StructNew();
			tempRelayTagKeyedByTagName = StructNew();
			counter = 0;
		</cfscript>

		<cfloop index = "i" from= "1" to = #arrayLen(relayTagArray)#>
			<cfset counter = counter+1>
			<cfset member = "insert_" & counter>

			<cfset thisTag = duplicate(relayTagArray[I].XmlAttributes)>
			<cfset thisTag.active = evaluate(thisTag.active)>
			<cfset thisTag.activeDefault = thisTag.active>

			<!--- WAB 2015-03-25 CASE 444232 Default ModuleNotActive to 0, otherwise get random dataType problems when doing QoQ with moduleNotActive != 1   --->
			<cfset thisTag.ModuleNotActive = 0>
			<!--- if this tag is module specific then only active if module is in list of LiveRelayApps, or it --->
			<!--- <CFIF thisTag.ModuleID is not 0 and listFindNocase (application. LiveRelayApps, thisTag.ModuleID) is 0> --->
			<CFIF thisTag.ModuleID is not 0 and not applicationScope.com.relayMenu.isModuleActive(moduleID=thisTag.ModuleID,applicationScope = applicationScope)>
				<cfset thisTag.active = 0>
				<cfset thisTag.ModuleNotActive = 1>
			<cfelse>

			<!--- active/inactive setting can be overridden by value in database --->
				<cfset thisTag.activeOverridden = 0>
			<cfif listfindnocase(relayTagsOnList,thisTag.Tag)>
				<cfset thisTag.active = 1>
				<cfset thisTag.activeOverridden = 1>
			<cfelseif listfindnocase(relayTagsOffList,thisTag.Tag)>
				<cfset thisTag.active = 0>
				<cfset thisTag.activeOverridden = 1>
			</cfif>
			</cfif>

			<cfset thisTag.tagWithChevrons = "<" & thisTag.tag & ">">
			<cfset thisTag.editorPath = "">
			<cfset thisTag.editorXML = "">
			<cfset thisTag.isCustomTag = false>
			<cfset	thisTag.editorExists = "0">
			<cfset thisTag.editorPath = "/relayTagEditors/relayTagEditor.cfm">
			<cfset thisTag.editorInsertPath = "">

			<cfset 	thisTag.InsertName = thisTag.Name>
			<cfset 	thisTag.InsertHTMLCode = "<span contenteditable='false' unselectable='on'>" & replace(replace(thisTag.tagWithChevrons,"<","::","ALL"),">",":::","ALL") & "</span>">
			<cfset 	thisTag.CleanHTMLCode = trim(thisTag.tagWithChevrons)>


			<!--- check whether file exists and read in the content for later use --->
			<cfset tagPathRelay =  "/relay/RelayTagTemplates/#thisTag.CFM#">
			<cfset tagPathUserFiles =  "/code/cftemplates/RelayTagTemplates/#thisTag.CFM#">

			<cfif fileExists(applicationScope.paths.abovecode & tagPathUserFiles)>
				<cfset thisTag.tagPath = tagPathUserFiles>
				<cffile action="READ" file="#applicationScope.paths.abovecode##tagPathUserFiles#" variable="theFileContent">
				<cfset thisTag.fileExists = true>
			<cfelseif fileExists(expandpath(tagPathRelay))>
				<cfset thisTag.tagPath = tagPathRelay>
				<cffile action="READ" file=#expandpath(tagPathRelay)# variable="theFileContent">
				<cfset thisTag.fileExists = true>
			<cfelse>
				<cfset theFileContent = "">
				<cfset thisTag.fileExists = false>
			</cfif>

			<!--- look for a custom editor (same name as tag with "Editor" Added --->
			<cfset testEditorName = "\relayTagEditors\" & left(thisTag.cfm,(len(thisTag.cfm)-4)) & "Editor.cfm" >

			<cfif (fileExists(applicationScope.path & testEditorName))>
					<cfset thisTag.editorExists = "1">
					<cfset thisTag.editorInsertPath = testEditorName>
			<cfelse>

				<cfif thisTag.fileExists>
					<!---
						We are going to look through the file for cf_param tags which define the editor
						We are also going to look at some included files
						These must be included with <cf_include template = "xxx" getFieldXML="true">
					--->

						<!--- this loop looks for included templates in the filecontent and adds their contents to the overall filecontent --->
		 				<cfset var includeFound = "true">
						<cfset counter = 1>
						<cfloop condition="includeFound and counter lt 10">
							<cfset includeFound = "false">
							<cfset counter = counter + 1>
							<cfset var argumentCollection ={inputstring =theFileContent, htmltag="cf_include",hasendtag=false}>
							<cfset var findXMLInclude = application.com.regExp.findHTMLTagsInString(argumentCollection = argumentCollection)>
							<cfloop index="thisInclude" array="#findXMLInclude#">
		 						<cfif structKeyExists (thisInclude.attributes,"getFieldXML")>
		 							<!--- NJH 2013/03/25 - look for template and onNotExistsTemplate which could be set
		 								WAB CASE 438377 2013-12-11 Fixed error when onNotExistsTemplate did not exist
		 							--->
		 							<cfloop list="template,onNotExistsTemplate" index="incTemplate">
										<cfif structKeyExists (thisInclude.attributes,incTemplate)>
											<cfset var templatePath = thisInclude.attributes[incTemplate]>
											<!--- NJH a relay tag template can include a setting in the cf_include file.. so, try and figure out what the var is..
												chances are is that it is a setting (version)... might be a better way of doing this... --->
											<cfset var firstPos = find("##",templatePath)>
											<cfif firstPos>
												<cfset var secondPos = find("##",templatePath,firstPos+1)>
												<cfset var cfvarString = mid(templatePath,firstPos,secondPos+1-firstPos)>
												<cfset var cfvar = evaluate(cfvarString)>
												<cfset templatePath = replace(templatePath,cfvarString,cfvar)>
											</cfif>
											<cfset testPath = expandPath(templatePath)>
											<cfset filePath ="">
											<cfif fileExists(testPath)>
												<cfset filePath = testPath>
												<cfbreak>
											</cfif>
										</cfif>
									</cfloop>

									<cfif filePath neq "">
										<cfset testPath = application.paths.relay & "/relayTagTemplates/" & templatePath>
										<cfif fileExists(testPath)>
											<cfset filePath = testPath>
										</cfif>
									</cfif>
									<cfset var includeFileContents = "Not Found">
									<cfif filePath neq "">
										<cffile action="READ" file="#filePath#" variable="includeFileContents">
										<cfset includefound = true>
									</cfif>
									<cfset theFileContent = replacenocase(theFileContent,thisInclude.string,includeFileContents,"ONE")>
								</cfif>
							</cfloop>
						</cfloop>

						<!--- we now make some XML from all the --->
						<cfset thisTag.EditorXML = getEditorXMLFromFileContent (content= theFileContent)>
				</cfif>

				<cfif thisTag.EditorXML is not "">
					<cfset thisTag.editorExists = "1">
				<cfelse>
					<cfset thisTag.editorExists = "0">
				</cfif>

			</cfif>

			<!---  WAB decide whether relaytags are customs tags --->
			<!--- 2012-11-19 WAB CASE 432097, was looking for  name="attributes. in the wrong variable, so we were never finding custom tags--->
			<cfif findNoCase('name="attributes.', theFileContent)>
				<cfset thisTag.isCustomTag = true>
			</cfif>

			<cfset tempRelayTagStruct[member] = thistag >
			<cfset tempRelayTagKeyedByTagName[thisTag.tag] = tempRelayTagStruct[member]>


		</cfloop>

		<cfset arguments.applicationScope.relayTagStruct = tempRelayTagStruct>
		<cfset arguments.applicationScope.RelayTagKeyedByTagName = tempRelayTagKeyedByTagName>
		<cfset tempQuery = structureFunctions.structToQuery(tempRelayTagKeyedByTagName)>  <!--- WAB - I think that a query might be more useful for displaying the picker and things --->

		<cfquery name = "activeRelayTags" dbtype="query">
		select * from tempQuery where active = 1 and moduleNotActive != 1 order by name
		</cfquery>

		<cfset arguments.applicationScope.activeRelayTagQuery = activeRelayTags>

	</cffunction>


	<cffunction name="addModuleNameToQuery" access="public" hint="Returns a query with module names.">
		<cfargument name="qryWithModuleID" required="yes" type="query">
		<cfset var qryReturn = queryNew(qryWithModuleID.columnList)>
		<cfset var findModuleID = 0>
		<cfset var moduleDetail = "">
		<cfset var thisModuleName = "">
		<cfset var defaultModuleID = arrayNew(1)>
		<cfset var defaultModuleName = arrayNew(1)>
		<cfset var col="">

		<cfif not isDefined("arguments.qryWithModuleID.moduleID")>
			<cfscript>
				queryAddColumn(qryReturn,"moduleID",defaultModuleID);
			</cfscript>
		</cfif>
		<cfscript>
			queryAddColumn(qryReturn,"moduleName",defaultModuleName);
		</cfscript>

		<cfloop query="arguments.qryWithModuleID">
			<cfscript>
				queryAddRow(qryReturn);
			</cfscript>
			<cfloop list="#columnList#" index="col">
				<cfscript>
					querySetCell(qryReturn,col,evaluate(col));
				</cfscript>
			</cfloop>

			<cfif not isDefined("moduleID")>
				<cfscript>
					querySetCell(qryReturn,"moduleID",0);
				</cfscript>
				<cfset findModuleID = 0>
			<cfelse>
				<cfset findModuleID = moduleID>
			</cfif>
			<cfset moduleDetail = XmlSearch(application.com.relayMenu.getMenuXMLDoc(), "//MenuItem[@id=#findModuleID#]/@ItemName")>
			<cfif arrayLen(moduleDetail) gt 0>
				<cfset thisModuleName = application.com.relayTranslations.translatePhrase("phr_#replace(LTrim(moduleDetail[1].xmlValue),' ','_','ALL')#")>
			<cfelse>
				<cfset thisModuleName = application.com.relayTranslations.translatePhrase("phr_general")>
			</cfif>
			<cfscript>
				querySetCell(qryReturn,"moduleName",thisModuleName);
			</cfscript>
		</cfloop>

		<cfreturn qryReturn>

	</cffunction>

	<cffunction name="getEditorXMLFromFileContent" returns="string">
		<cfargument name="content" required="yes" type="string">

		<cfset var result = "">
		<cfset var temp= "">
		<cfset var findXMLParam  = "">
		<cfset var regExpFunctions = createObject("component","/relay/com/regExp")>
		<cfset var argumentCollection = {}>
		<cfset var paramArguments = {}>
		<cfset var groups = {}>
		<cfset var param = "">
		<cfset var paramAttributes = {}>

		<!--- This regExp finds <cf_param /> tags --->
		<cfset var cf_ParamRe = "<cf_param .*?/>|<cf_paramgroup.*?>|</cf_paramgroup.*?>">

				<cfset argumentCollection = {reg_expression = cf_ParamRe, string = content}> <!--- use argument collection to prevent content appearing in debug --->
				<cfset findXMLParam  = regExpFunctions.refindAllOccurrences(argumentCollection = argumentCollection)>
				<cfif arrayLen(findXMLParam)>
					<cfset result= "<editor>">
						<cfloop array="#findXMLParam#" index="Param">
							<cfset var topAndTailedString = right(left(Param.string, len(Param.string)-1),len(Param.string)-2)>
							<cfset topAndTailedString = replace(topAndTailedString,">","&gt;","ALL")>
							<cfset topAndTailedString = replace(topAndTailedString,"<","&lt;","ALL")>
							<cfset result = result & "<" & topAndTailedString & ">">
						</cfloop>
					<cfset result = result & "</editor>">
				</cfif>
				<cfset result = replacenocase(replacenocase(result,"cf_paramgroup","fieldgroup","ALL"),"cf_param","field","ALL")>
				<!--- TODO possibly ought to check that the XML is valid here --->

		<cfreturn result>

	</cffunction>

	<!--- WAB 2013-07-04 Minor mod to bring back parameter array - used for matching up unnamed parameters --->
	<cffunction name="getEditorXMLFromCFCFunction" returns="struct">
		<cfargument name="cfc" required="yes" >
		<cfargument name="functionname" required="yes" type="string">

			<cfset var metadata = getmetadata(cfc)>
			<cfset var result = {editorXML = "", description="",functionExists = false}>
			<cfset var find= "">
			<cfset var parametersArray = "">
			<cfset var thisParam = "">
			<cfset var tempXML = "">

			<!--- Look for name - does rather rely on this name not being the value of any other parameter in the cfc
				if causes a problem then could be adjusted
			 --->

			<cfset find = structFindValue(metadata,functionname,"all")>

			<cfif arrayLen(find) gt 0>
				<cfset result.functionExists = true>
				<cfset result.parametersArray =  find[1].owner.parameters>
				<cfloop array="#result.parametersArray#" index="thisParam">
					<cfif not structKeyExists (thisParam,"showInEditor") or thisParam.showInEditor>
						<cfset tempXML = tempXML&"<field #application.com.structureFunctions.convertStructureToNameValuePairs(struct=thisParam,delimiter=" ",Qualifier='"',encode="ISO")#/>">
					</cfif>
				</cfloop>
				<cfset result.editorXML = "<editor>#tempXML#</editor>">
			<cfelse>

			</cfif>

			<cfreturn result>
	</cffunction>


	<cffunction name="createStandardRelayFormElementAttributeStructureFromTagAttributes">
		<cfargument name="attributeStruct">

		<!--- this call does all the standard conversions
		2014-10-14	WAB	Core-856 Add options so that booleans are stored as Yes/No
		--->
		<cfset var attributeCollection = application.com.relayforms.createStandardRelayFormElementAttributeStructure (attributeStruct = attributeStruct, options = {BooleanStoredAs="YesNo"})>
		<cfif structKeyExists (attributeCollection,"reloadOnChange")>
			<cfset attributeCollection.onChange = "doReload(this.form)">
			<cfset structDelete(attributeCollection,"reloadOnChange")>
		</cfif>

		<cfreturn attributeCollection >
	</cffunction>


	<cffunction name="getRelayMetaDataForCFC" returns="struct">
		<cfargument name="cfc" required="yes" >

			<cfset var metadata = getmetadata(cfc)>
			<cfset var result = structNew()>
			<cfset var find = '' />
			<cfset var propertyArray = '' />
			<cfset var thisPropertyArray = '' />
			<cfset var i = '' />
			<cfset var j = '' />

			<cfset find = structFindKey(metadata,"properties","all")>

			<cfif arrayLen(find) gt 0>
				<cfset propertyArray = arrayNew(1)>
				<cfloop index="i" from="1" to = "#arrayLen(find)#">
					<cfset thisPropertyArray = find[i].value>
					<cfloop index="j" from="1" to = "#arrayLen(thisPropertyArray)#">
						<cfset propertyArray[arrayLen(propertyArray)+1] = thisPropertyArray[j]>
					</cfloop>
				</cfloop>
				<cfset result.properties = PropertyArray>
			<cfelse>

			</cfif>

			<cfreturn result>
	</cffunction>


	<cffunction name="getMergeFunctions" access="public" returntype="query">
		<cfargument name="context" type="string" default=""> 	<!---  WAB 2013-07-02 added support for Context being a list --->
		<cfargument name="entityTypeID" type="string" default="">

		<cfset var mergeFieldsQry = queryNew("description,insertName,editorExists,tag,cleanHTMLCode,editorPath,moduleName,moduleID,entityTypeID,derivedField","varchar,varchar,bit,varchar,varchar,varchar,varchar,integer,varchar,bit")>
		<cfset var currentRow=0>
		<cfset var mergeFunctionsArray = getArrayOfFunctionsInCFC(cfc = application.com.relayTranslations.getMergeObject(),access="remote,public")>
		<cfset var description = "">
		<cfset var hasEditor=false>
		<cfset var editorPath = "">
		<cfset var thisEntityType = arguments.entityTypeID>
		<cfset var moduleID = 0>
		<cfset var moduleName = "">
		<cfset var mergeFunction = '' />
		<cfset var param = '' />

		<cfloop array="#mergeFunctionsArray#" index="mergeFunction">

			<!--- if the function is active and it is for a module that is on. --->
			<cfif (not structKeyExists(mergeFunction,"active") or mergeFunction.active) and (not structKeyExists(mergeFunction,"moduleID") or application.com.relayMenu.isModuleActive(mergeFunction.moduleID) or mergeFunction.moduleID eq 0)>
				<cfif (arguments.entityTypeID eq "" or not structKeyExists(mergeFunction,"entityTypeID") or mergeFunction.entityTypeID eq arguments.entityTypeID)
						and (arguments.context eq "" or not structKeyExists(mergeFunction,"context") or application.com.globalFunctions.ListFindNoCaseWithList(mergeFunction.context,arguments.context))>
					<cfset currentRow++>
					<cfset description=mergeFunction.name>
					<cfif structKeyExists(mergeFunction,"description")>
						<cfset description=mergeFunction.description>
					</cfif>

					<cfset hasEditor=0>

					<cfloop array="#mergeFunction.parameters#" index="param">
						<cfif not StructKeyExists (param,"showInEditor") or param.showInEditor is "true">
							<cfset hasEditor=1>
						</cfif>
					</cfloop>

					<cfset editorPath = "">
					<cfif hasEditor>
						<cfset editorPath = "/relayTagEditors/relayFunctionEditor.cfm?addNew=yes">
					</cfif>

					<cfset moduleName = "General">
					<cfset moduleID = 0>
					<cfif structKeyExists(mergeFunction,"moduleID") and mergeFunction.moduleID neq 0>
						<cfset moduleID = mergeFunction.moduleID>
						<cfset moduleName = application.com.relayMenu.getModuleName(moduleID=moduleID)>
					</cfif>

					<cfset queryAddRow(mergeFieldsQry)>
					<cfset querySetCell(mergeFieldsQry,"description",description,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"insertName",mergeFunction.name,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"editorExists",hasEditor,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"tag",mergeFunction.name,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"cleanHTMLCode","[["&mergeFunction.name&"()]]",currentRow)>
					<cfset querySetCell(mergeFieldsQry,"editorPath",editorPath,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"moduleName",moduleName,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"moduleID",moduleID,currentRow)>
					<cfset querySetCell(mergeFieldsQry,"derivedField",0,currentRow)> <!--- not needed but needs to be same as mergeFieldsQuery --->

					<cfset thisEntityType = arguments.entityTypeID>
					<cfif structKeyExists(mergeFunction,"entityTypeID")>
						<cfset thisEntityType = mergeFunction.entityTypeID>
					</cfif>
					<cfif thisEntityType neq "">
						<cfset querySetCell(mergeFieldsQry,"entityTypeID",thisEntityType,currentRow)>
					</cfif>

				</cfif>
			</cfif>
		</cfloop>

		<cfreturn mergeFieldsQry>
	</cffunction>


	<cffunction name="getMergeFields" access="public">
		<cfargument name="context" type="string" default=""> <!---  WAB 2013-07-02 added support for Context being a list --->
		<cfargument name="entityTypeID" type="string" default="">

		<cfset var mergeFieldsQry = queryNew("description,insertName,editorExists,tag,cleanHTMLCode,editorPath,moduleName,moduleID,entityTypeID,derivedField","varchar,varchar,bit,varchar,varchar,varchar,varchar,integer,varchar,bit")>
		<cfset var mergeFieldsStruct = structNew()>
		<cfset var mergeFieldSetArray = arrayNew(1)>
		<cfset var fieldSetAttributes = structNew()>
		<cfset var fieldSet = '' />

		<cfset mergeFieldsStruct = application.com.xmlFunctions.readAndParseXMLFile(fileNameAndPath="#application.paths.relayware#\xml\mergeFields.xml")>
		<cfset mergeFieldSetArray = xmlSearch(mergeFieldsStruct.xml,"//fields/fieldset")>

		<cfloop array="#mergeFieldSetArray#" index="fieldSet">
			<cfset fieldSetAttributes = fieldSet.xmlAttributes>
			<cfif (arguments.entityTypeID eq "" or fieldSetAttributes.entityTypeID eq arguments.entityTypeID)
					and (arguments.context eq "" or not structKeyExists(fieldSetAttributes,"context") or application.com.globalFunctions.ListFindNoCaseWithList(fieldSetAttributes.context,arguments.context)
					 and (not structKeyExists(fieldSetAttributes,"moduleID") or fieldSetAttributes.moduleID is 0 or application.com.relayMenu.isModuleActive(fieldSetAttributes.moduleID) or fieldSetAttributes.moduleID eq 0)
				 )>
				<!--- we've found an active module, so loop over the children --->
				<cfset mergeFieldsQry = addMergeFieldToQuery(mergeFieldsQry=mergeFieldsQry,xmlNodeArray=fieldSet.xmlChildren,entityTypeID=fieldSetAttributes.entityTypeID)>
			</cfif>
		</cfloop>

		<cfif arguments.context eq "emailDef">
			<!--- NJH 2014/08/15 Add support for custom entities in merge fields. They are very basic. We don't show createdBy and we're exposing lastUpdatedByPerson as lastUpdatedBy --->
			<cfquery name="local.getCustomEntities">
				select cast(entityTypeID as varchar) as entityTypeID,
					case when len(isNull(description,'')) = 0 then case when c.column_name = 'lastUpdatedByPerson' then 'lastUpdatedBy' else c.column_name end else description end as description,
					cast (0 as bit) as editorExists,'General' as moduleName, 0 as moduleID,
					case when c.column_name = 'lastUpdatedByPerson' then 'lastUpdatedBy' else c.column_name end as insertName,
					case when c.column_name = 'lastUpdatedByPerson' then 'lastUpdatedBy' else c.column_name end as tag,
					'[['+ s.entityName+'.'+case when c.column_name = 'lastUpdatedByPerson' then 'lastUpdatedBy' else c.column_name end +']]'as cleanHTMLCode,
					'' as editorPath, 0 as derivedField
				from schemaTable s
					inner join information_schema.columns c on s.entityName = c.table_name
				where customEntity = 1
					and c.column_name not in ('createdBy','lastUpdatedBy')
					<cfif arguments.entityTypeID neq "">
						and s.entityTypeID = <cf_queryparam value="#arguments.entityTypeID#" cfsqltype="cf_sql_integer">
					</cfif>
			</cfquery>

			<cfquery dbtype="query" name="mergeFieldsQry">
				select #mergeFieldsQry.columnList# from [local].getCustomEntities
				union
				select #mergeFieldsQry.columnList# from mergeFieldsQry
			</cfquery>
		</cfif>

		<cfreturn mergeFieldsQry>
	</cffunction>

	<!--- WAB 2011/11/16 added a derived field attribute/column, allowed me to generate a query automatically  --->
	<cffunction name="addMergeFieldToQuery" access="private" returntype="query">
		<cfargument name="mergeFieldsQry" type="query">
		<cfargument name="xmlNodeArray" type="array">
		<cfargument name="entityTypeID" type="string" default="">

		<cfset var description="">
		<cfset var moduleName = "">
		<cfset var moduleID = 0>
		<cfset var name = "">
		<cfset var derivedField= false>
		<cfset var thisRow = 0>
		<cfset var insertCode = "">
		<cfset var mergeField = '' />

		<!--- we've found an active module, so loop over the children --->
		<cfloop array=#xmlNodeArray# index="mergeField">
			<cfif not structKeyExists(mergeField.xmlAttributes,"active") or mergeField.xmlAttributes.active>
				<cfif arrayLen(mergeField.xmlChildren)>
					<cfset arguments.mergeFieldsQry = addMergeFieldToQuery(mergeFieldsQry=arguments.mergeFieldsQry,xmlNodeArray=mergeField.xmlChildren,entityTypeID=arguments.entityTypeID)>
				<cfelse>
					<cfset thisRow = mergeFieldsQry.recordCount+1>
					<cfset name = mergeField.xmlAttributes.name>
					<cfset description = name>
					<cfif structKeyExists(mergeField.xmlAttributes,"description")>
						<cfset description = mergeField.xmlAttributes.description>
					</cfif>

					<cfif mergeField.xmlParent.xmlName neq "fieldSet" and structKeyExists(mergeField.xmlParent.xmlAttributes,"name") and mergeField.xmlParent.xmlAttributes.name neq "">
						<cfset name = mergeField.xmlParent.xmlAttributes.name &"."&name>
					</cfif>
					<cfif arguments.entityTypeID eq "">
						<cfset insertCode = name>
					<cfelse>
						<!--- NJH 2012/10/25 Social CR - display the entity type friendly name for the merge field..so, module.title rather than trngModule.title
							NJH 2016/01/05 Jira BF-34 - re-write code so that we get the proper name of the merge fields to insert, regardless of the number of levels involved.
						--->
						<cfset var node = mergeField>
						<cfset insertCode = "">
						<cfloop condition="node.xmlname neq 'fieldset'">
							<cfset insertCode = listPrePend(insertCode,node.xmlAttributes.name,".")>
							<cfif structKeyExists(node,"xmlParent")>
								<cfset node = node.xmlParent>
							</cfif>
						</cfloop>
						<!--- if at the top, see if we have a name. If so, use it. Otherwise assume it is going to be the entity table name --->
						<cfset insertCode = listPrePend(insertCode,structKeyExists(node.xmlAttributes,"name")?node.xmlAttributes.name:application.entityType[arguments.entityTypeID].tablename,".")>
					</cfif>

					<cfset moduleName = "General">
					<cfset moduleID = 0>
					<cfif structKeyExists(mergeField.xmlAttributes,"moduleID") and mergeField.xmlAttributes.moduleID neq 0>
						<cfset moduleID = mergeField.xmlAttributes.moduleID>
						<cfset moduleName = application.com.relayMenu.getModuleName(moduleID=moduleID)>
					</cfif>

					<cfset derivedField= false>
					<cfif structKeyExists (mergeField.xmlAttributes,"derivedField")>
						<cfset derivedField = mergeField.xmlAttributes.derivedField>
					</cfif>


					<cfset queryAddRow(mergeFieldsQry)>
					<cfset querySetCell(mergeFieldsQry,"description",description,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"insertName",name,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"editorExists",0,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"tag",name,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"cleanHTMLCode","[["&insertCode&"]]",thisRow)>
					<cfset querySetCell(mergeFieldsQry,"editorPath","",thisRow)>
					<cfif arguments.entityTypeID neq "">
						<cfset querySetCell(mergeFieldsQry,"entityTypeID",arguments.entityTypeID,thisRow)>
					</cfif>
					<cfset querySetCell(mergeFieldsQry,"moduleName",moduleName,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"moduleID",moduleID,thisRow)>
					<cfset querySetCell(mergeFieldsQry,"derivedField",iif(derivedField,1,0),thisRow)>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn mergeFieldsQry>
	</cffunction>


	<cffunction name="getMergeFieldsAndFunctions" returntype="query">
		<cfargument name="context" type="string" default="">
		<cfargument name="entityTypeID" type="string" default="">

		<cfset var mergeFieldsQry = getMergeFields(argumentCollection=arguments)>
		<cfset var mergeFunctionsQry = getMergeFunctions(argumentCollection=arguments)>
		<cfset var mergeFieldsAndFunctionsQry = "">
		<cfset var columnList = mergeFieldsQry.columnList>

		<cfquery name="mergeFieldsAndFunctionsQry" dbtype="query">
			select #columnList#, lower(insertName) as lowerInsertName from mergeFieldsQry
			union
			select #columnList#, lower(insertName) as lowerInsertName from mergeFunctionsQry
			order by moduleID,entityTypeID,lowerInsertName
		</cfquery>

		<cfreturn mergeFieldsAndFunctionsQry>
	</cffunction>


	<!--- returns an array of all the functions in a cfc, deals with extended cfcs which have lots of layers of functions --->
	<cffunction name="getArrayOfFunctionsInCFC">
		<cfargument name="cfc">
		<cfargument name="access" default=""> <!--- public,remote,private --->

		<cfset var metaData = getMetaData(cfc)>
		<cfset var result = arrayNew(1)>
		<cfset var functions = '' />
		<cfset var function = '' />
		<cfset var find = structFindKey(metadata,"functions","all")>

		<cfloop array=#find# index="functions">
			<cfloop array=#functions.value# index="function">
				<cfif access is "" or (structKeyExists(function,"access") and listfindnocase(access,function.access))>
					<cfset arrayAppend(result,function)>
				</cfif>
			</cfloop>
		</cfloop>

		<cfreturn result>
	</cffunction>


	<cffunction name="getSetting" access="private" returnType="numeric">
		<cfargument name="variableName" type="string" required="true">

		<cfreturn iif(application.com.settings.getSetting(variableName=arguments.variableName),1,0)>
	</cffunction>


	<cffunction name="isRelayTagInElement" returnType="boolean">
		<cfargument name="relayTagName" type="string" required="true">

		<cfset var checkTagInElement = "">

		<cfquery name="checkTagInElement" datasource="#application.siteDataSource#">
			select 1 from phrases p inner join phraseList pl on pl.phraseID=p.phraseID
			where pl.entityTypeID =  <cf_queryparam value="#application.entityTypeID.element#" CFSQLTYPE="cf_sql_integer" >
				and pl.phraseTextID='detail'
				and CONTAINS(p.phraseText, '<#arguments.relayTagName#')
		</cfquery>

		<cfreturn checkTagInElement.recordCount>
	</cffunction>

</cfcomponent>

