<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			FundRequestType.cfc
Author:				AJC
Date started:		2011-03-15
	
Description:		For the MDF section. Contains
functions related to Fund Request Type.	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->
<cfcomponent>

	<cffunction access="public" name="getFundRequestType" hint="Returns Fund Request Type information">
		<cfargument name="sortOrder" type="string" default="sortorder">
		<cfscript>
			var qry_get_FundRequestType = "";
			var FundRequestTypePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "FundRequestType", phraseTextID = "title");
		</cfscript>
		
		<cfquery name="qry_get_FundRequestType" datasource="#application.siteDataSource#">
			select FundRequestTypeID
			,#preservesinglequotes(FundRequestTypePhraseQuerySnippets.select)# as FundRequestType
			,FundRequestTypeTextID
			,active
			,sortorder
			,startdate,
			enddate
			from FundRequestType
				#preservesinglequotes(FundRequestTypePhraseQuerySnippets.join)# 
			order by #arguments.sortorder#
		</cfquery>
		
		<cfreturn qry_get_FundRequestType>
	</cffunction>
	
	<cffunction access="public" name="getFundRequestTypeForDropDown" hint="Returns lenovo Co-Funders for populating a dropdown" validValues="true">
		
		<cfscript>
			var qry_get_FundRequestType = "";
			var FundRequestTypePhraseQuerySnippets = "";
		</cfscript>
		<cfset FundRequestTypePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "FundRequestType", phraseTextID = "title")>
		<cfquery name="qry_get_FundRequestType" datasource="#application.siteDataSource#">
			select FundRequestTypeID as dataValue ,<!--- 'title_lenovoFundCoFundingEntity_' + convert (varchar, lenovoFundCoFundingEntityID) ---> #preservesinglequotes(FundRequestTypePhraseQuerySnippets.select)# as displayValue
			 from FundRequestType
			 #preservesinglequotes(FundRequestTypePhraseQuerySnippets.join)# 
			 where 
			 (
			 	(getdate() between startdate and enddate) 
			 	or (startdate is null and enddate is null)
			 )
			 
			 and active = 1 order by sortorder
		</cfquery>
		
		<cfreturn qry_get_FundRequestType>
	</cffunction>

</cfcomponent>
