<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			tableFunction.cfc
Author:				KAP
Date started:		2003-09-02
	
Description:			

creates query for functionList attribute of tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-Sep-2003			KAP			Initial version

Possible enhancements:

NOTES!!!
Any passed parameters get set as form variables not url/QueryString!!!


 --->

<cfcomponent displayname="functionList" hint="Creates functionList">

	<cfparam name="this.qfunctionList"  type="query" default="#QueryNew( "functionID,functionName,securityLevel,url,windowFeatures" )#">
	<cfparam name="this.functionID"     type="string" default="">
	<cfparam name="this.functionName"   type="string" default="">
	<cfparam name="this.securityLevel"  type="string" default="">
	<cfparam name="this.url"            type="string" default="">
	<cfparam name="this.windowFeatures" type="string" default="">

	<cffunction access="public" name="functionListAddRow" hint="Adds row to query object this.qfunctionList">
		<cfscript>
		QueryAddRow( this.qFunctionList );
		QuerySetCell( this.qFunctionList, "functionID",     iif( this.functionID neq "", "this.functionID", "this.qfunctionList.RecordCount" ) );
		QuerySetCell( this.qFunctionList, "functionName",   this.functionName );
		QuerySetCell( this.qFunctionList, "securityLevel",  this.securityLevel );
		QuerySetCell( this.qFunctionList, "url",            this.url );
		QuerySetCell( this.qFunctionList, "windowFeatures", this.windowFeatures );
		</cfscript>
	</cffunction>
<!--- 
	<cffunction access="public" name="functionList" hint="Returns a query object in this.qfunctionList">

		<cfscript>
		QueryAddRow( this.qFunctionList, 6 );
		QuerySetCell( this.qFunctionList, "functionID", 1, 1 );
		QuerySetCell( this.qFunctionList, "functionName", "Function", 1 );
		QuerySetCell( this.qFunctionList, "securityLevel", "", 1 );
		QuerySetCell( this.qFunctionList, "url", "", 1 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "", 1 );
		
		QuerySetCell( this.qFunctionList, "functionID", 2, 2 );
		QuerySetCell( this.qFunctionList, "functionName", "f1", 2 );
		QuerySetCell( this.qFunctionList, "securityLevel", "", 2 );
		QuerySetCell( this.qFunctionList, "url", "u1?rowIdentityList=", 2 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "", 2 );
		
		QuerySetCell( this.qFunctionList, "functionID", 3, 3 );
		QuerySetCell( this.qFunctionList, "functionName", "f2", 3 );
		QuerySetCell( this.qFunctionList, "securityLevel", "Approval(3)", 3 );
		QuerySetCell( this.qFunctionList, "url", "u2?rowIdentityList=", 3 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "left=100,top=100,height=600,width=800", 3 );
		
		QuerySetCell( this.qFunctionList, "functionID", 4, 4 );
		QuerySetCell( this.qFunctionList, "functionName", "---------------------", 4 );
		QuerySetCell( this.qFunctionList, "securityLevel", "", 4 );
		QuerySetCell( this.qFunctionList, "url", "", 4 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "", 4 );
		
		QuerySetCell( this.qFunctionList, "functionID", 5, 5 );
		QuerySetCell( this.qFunctionList, "functionName", "f3", 5 );
		QuerySetCell( this.qFunctionList, "securityLevel", "", 5 );
		QuerySetCell( this.qFunctionList, "url", "u3?rowIdentityList=", 5 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "left=100,top=100,height=600,width=800", 5 );
		
		QuerySetCell( this.qFunctionList, "functionID", 6, 6 );
		QuerySetCell( this.qFunctionList, "functionName", "f4", 6 );
		QuerySetCell( this.qFunctionList, "securityLevel", "", 6 );
		QuerySetCell( this.qFunctionList, "url", "u4?rowIdentityList=", 6 );
		QuerySetCell( this.qFunctionList, "windowFeatures", "left=100,top=100,height=600,width=800,directories=no,hotkeys=no,location=yes,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no", 6 );

		this.functionID = 7;
		this.functionName = "*f5";
		this.securityLevel = "";
		this.url = "javascript:alert();//";
		this.windowFeatures = "left=100,top=100,height=600,width=800,directories=no,hotkeys=no,location=yes,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no";
		functionListAddRow();
		</cfscript>
	
	</cffunction>
 --->
</cfcomponent>

