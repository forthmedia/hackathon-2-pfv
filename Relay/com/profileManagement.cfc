<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Profile Management" hint="Provide functions or managing components">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getAllFlagTypes" hint="returns the flagType for a given frmEntityTypeID">
		<cfargument name="dataSource" type="string" required="true">
		<cfquery name="getAllFlagTypes" datasource="#dataSource#" cachedwithin="#createTimeSpan(0,6,0,0)#">
			select * from flagEntityType
		</cfquery>
		<cfreturn getAllFlagTypes>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFlagType" hint="returns the flagType for a given frmEntityTypeID">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="entityTypeID" type="numeric" required="yes">
		<cfquery name="getFlagType" datasource="#dataSource#">
			select tableName from flagEntityType
			where entityTypeID = #entityTypeID#
		</cfquery>
		<cfreturn getFlagType.tableName>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns Profile Attribute Info                    
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProfileAttributeType" output="false" 
		returntype="query" hint="Returns a query object containing profile attribute (flag) type details based on a flagID">
		<cfargument name="dataSource" type="string" required="yes">
		<cfargument name="flagID" type="numeric" required="yes">
		<!--- this uses an updated version of the vFlagDef view --->
		<cfquery name="getProfileAttributeType" datasource="#dataSource#">
			SELECT FlagGroupID, flagGrouptextID, FlagID, flagtextID, 
			   DataTable, 
			   EntityTypeID, 
			   FlagGroup, Flag, 
			   DataType, Protected, 
			   EntityTable, flagTypeID,dataTablefullname
			FROM vFlagDef
			where flagID = #flagID#
		</cfquery>
		<cfif getProfileAttributeType.recordCount gt 0>
			<cfreturn getProfileAttributeType>
		<cfelse>
			<cfreturn "Invalid flagID">
		</cfif>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProfileAttributeValue" hint="Returns data given a flagID and an entityID">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="flagID" type="numeric" required="yes">
		<cfargument name="entityID" type="numeric" required="yes">
		
		<!--- get the details for this flagID --->
		<cfinvoke method="getProfileAttributeType" returnvariable="qGetProfileAttributeType">
			<cfinvokeargument name="flagID" value="#flagID#">
			<cfinvokeargument name="dataSource" value="#dataSource#">
		</cfinvoke>

		<cfif isQuery(qGetProfileAttributeType)>
			<cfset tablename = qGetProfileAttributeType.dataTablefullname>
			<cfswitch expression="#tablename#">
				<cfcase value="booleanFlagData">
					<cfquery name="getProfileAttributeValue" datasource="#dataSource#">
						select name as data from booleanFlagData b
							inner join flag f ON f.FlagID = b.flagID
							where entityID = #entityID# 
							and f.flagID = #flagID#
					</cfquery>
					<cfset ProfileAttributeValue = getProfileAttributeValue.data>
				</cfcase>
				
				<cfcase value="integerFlagData,textFlagData">
					<cfquery name="getProfileAttributeValue" datasource="#dataSource#">
						select data from #tablename# b
							inner join flag f ON f.FlagID = b.flagID
							where entityID = #entityID# 
							and f.flagID = #flagID#
					</cfquery>
					<cfset ProfileAttributeValue = getProfileAttributeValue.data>
				</cfcase>
				
				<cfcase value="dateFlagData">
					<cfquery name="getProfileAttributeValue" datasource="#dataSource#">
						select data from #tablename# b
							inner join flag f ON f.FlagID = b.flagID
							where entityID = #entityID# 
							and f.flagID = #flagID#
					</cfquery>
					<cfset ProfileAttributeValue = dateFormat(getProfileAttributeValue.data,"dd-mmm-yyyy")>
				</cfcase>
	
				<cfdefaultcase>
					<cfset ProfileAttributeValue = "Profile type is not supported">
				</cfdefaultcase>
			</cfswitch>
		<cfelse>
			<cfset ProfileAttributeValue = "FlagID invalid">
		</cfif>
		<cfreturn ProfileAttributeValue>
	</cffunction>
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Get Flag Groups
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getFlagGroups" hint="Get Flag groups based on the passed entity Type ID">
		<cfargument name="entityTypeID" type="numeric" required="yes">
		
		<CFQUERY NAME="getFlagGroups" DATASOURCE="#application.SiteDataSource#">
			SELECT fg.flagGroupID, fg.name
			FROM flagGroup fg
			
			WHERE fg.entityTypeID = (SELECT distinct entityTypeID from flagGroup where flagGroupID =  #arguments.entityTypeID#)
		</CFQUERY>
		
		<cfreturn getFlagGroups>
	
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Create New Flag
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="createNewFlag" hint="Adds new flag to the system">
		<cfargument name="flagGroupID" type="numeric" required="yes">
		<cfargument name="flagTextID" type="string" required="yes">
		<cfargument name="name" type="string" required="yes">
		
		<cfset var newOrder = "">
		
		<cftransaction>
		
		<CFQUERY NAME="getMaxOrder" DATASOURCE="#application.SiteDataSource#">
			SELECT max(orderingIndex) as maxOrder
			FROM Flag
			WHERE flagGroupID = #arguments.flagGroupID#
		</CFQUERY>
		
		<cfif len(getMaxOrder.maxOrder)>
			<cfset newOrder = getMaxOrder.maxOrder+1>
		<cfelse>
			<cfset newOrder = 1>
		</cfif>
		
		<CFQUERY NAME="AddFlag" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Flag
			(FlagGroupID, Name, 
			FlagTextID, OrderingIndex, 
			Active, CreatedBy, Created, LastUpdatedBy, LastUpdated)
			VALUES
			(<cf_queryparam value="#arguments.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#replace(arguments.name,Chr(34),"`","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
			<cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#newOrder#" CFSQLTYPE="cf_sql_float" >, 1, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, 
			getDate(), <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, getDate())
		</CFQUERY>
		
		<CFQUERY NAME="getFlagID" DATASOURCE="#application.SiteDataSource#">
			SELECT max(FlagID) as newFlagID
			FROM Flag
		</CFQUERY>
		
		<cfset flagID = getFlagID.newFlagID>
		
		</cftransaction>
	
		<cfreturn flagID>
	
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
			 
			 WAB altered so that can bring back non live flags (otherwise impossible to add the first flag for a type!)
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	
	<cffunction access="public" name="GetEntityTypes" hint="Returns a query object containing live EntityID">
		<cfargument name="justReturnLiveTypes" type="boolean" default="false">		
		<cfscript>
			var GetEntityTypes = "";
		</cfscript>

		<!--- 2010/04/14 GCC changed order by flagEntityType.tablename to order by tablename becuase SQL2005 throws an error - worked with 2000--->
		<CFQUERY NAME="GetEntityTypes" DATASOURCE="#application.SiteDataSource#">
			SELECT distinct flagEntityType.entitytypeid, upper(left(TableName,1)) + right(tablename, len(tablename)-1) as tablename, flagEntityType.description, case when FlagGroup.flaggroupid is null then 0 else 1 end as isLive
			FROM flagEntityType 
				left JOIN FlagGroup ON flagEntityType.entitytypeid = FlagGroup.EntityTypeID 
			<cfif justReturnLiveTypes>
				where FlagGroup.flaggroupid is not null 
			</cfif>
			ORDER BY 
			case when FlagGroup.flaggroupid is null then 0 else 1 end desc,
			tablename
		</CFQUERY>
		
		<cfreturn GetEntityTypes>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getCountryGroups" hint="Return all Country Groups for this Country">
		<cfscript>
			var UserCountryList = "";
			var newOrder = "";
		</cfscript>
		
		<!--- Get USER countries --->
		<cfinclude template="/templates/qrygetcountries.cfm">

		<CFQUERY NAME="getCountryGroups" datasource="#application.sitedatasource#">
			SELECT DISTINCT b.CountryID
				 FROM Country AS a, Country AS b, CountryGroup AS c
				WHERE (b.ISOcode IS null OR b.ISOcode = '')
				  AND c.CountryGroupID = b.CountryID
				  AND c.CountryMemberID = a.CountryID
				  AND a.CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
			ORDER BY b.CountryID
		</CFQUERY>
		
		<CFSET UserCountryList = "0,#CountryList#">
		<CFIF getCountryGroups.CountryID IS NOT "">
			<!--- top record (or only record) is not null --->
			<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
		</CFIF>
		
		<cfreturn UserCountryList>
	</cffunction>	
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateProfileCounts" hint="Updates flag.currentCount column for a flagGroupID">
		<cfargument name="flagGroupID" type="numeric" required="yes">
		<cfscript>
			var updateProfileCounts = "";
		</cfscript>
		
		<CFQUERY NAME="updateProfileCounts" datasource="#application.sitedatasource#">
			Update f set f.CurrentCountDate = GetDate(),
				f.CurrentCount = fc.CurrentCount
			From Flag f 
				Join (select FlagID, count(EntityID) as CurrentCount from booleanFlagData 
						where flagID in (Select flagID from flag where flagGroupID = #arguments.flagGroupID#)
					group by FlagID) as fc 
				on f.FlagID = fc.FlagID 
			where f.flagGroupID = #arguments.flagGroupID#		
		</CFQUERY>
				
	</cffunction>	
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             function description                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getProfileFlagGroups" hint="Returns a query object containing flag groups">
		<cfargument name="searchPhrase" type="string" required="yes" default="">
		<cfargument name="EntityTypeID" type="string" required="yes" default="99">
		<cfargument name="discardEntityTypeIDs" type="string" required="yes" default="0,2">
		<cfargument name="UserCountryList" type="string" required="yes" default="0">
		<cfscript>
			var getProfileFlagGroups = "";
		</cfscript>

		<CFQUERY NAME="getProfileFlagGroups" datasource="#application.sitedatasource#">
		SELECT 
		FlagGroup.FlagGroupID AS Grouping,
		FlagGroup.FlagGroupID,
		FlagGroup.EntityTypeID,
		FlagGroup.FlagGroupTextID,
		FlagGroup.FlagTypeID,
		FlagGroup.Active,
		FlagGroup.Name,
		FlagGroup.description,
		FlagGroup.ParentFlagGroupID,
		FlagGroup.ViewingAccessRights,
		FlagGroup.EditAccessRights,
		FlagGroup.DownloadAccessRights,
		FlagGroup.SearchAccessRights,
		FlagGroup.OrderingIndex AS parent_order,
		0 AS child_order,
		(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count,
		
		(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
		(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
			OR (s.EditAccessRights = 0 AND s.Edit <> 0)
			or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
			WHERE  r.SecurityTypeID = s.SecurityTypeID 
			AND s.ShortName  = 'AdminTask'	
			AND r.usergroupid = rg.usergroupid
			AND rg.PersonID=#request.relayCurrentUser.personid# )
			OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
		                AND s.FlagGroupID = t.FlagGroupID
		                AND t.UserID=#request.relayCurrentUser.usergroupid#
		                AND t.Edit <> 0)))) AS edit_count
		FROM FlagGroup
		
		<!--- added by DF --->
		INNER JOIN FlagType ft ON ft.flagTypeID = flagGroup.flagTypeID
		<cfif isDefined("arguments.searchPhrase") and arguments.searchPhrase neq "">
		INNER JOIN Flag ON Flag.FlagGroupID = FlagGroup.FlagGroupID
		</cfif>
		
		WHERE ParentFlagGroupID = 0
		
		
		<cfif compareNoCase(arguments.EntityTypeID,"OTHER") eq 0>
			AND FlagGroup.EntityTypeID  not in ( <cf_queryparam value="#arguments.discardEntityTypeIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<cfelse>
			AND FlagGroup.EntityTypeID =  <cf_queryparam value="#arguments.EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		
		
		<!--- Added by SWJ to constrain the flag list a bit --->
		AND FlagGroup.Scope  IN ( <cf_queryparam value="#arguments.UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
		AND flagGroup.active = 1
		AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
			or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
								WHERE  r.SecurityTypeID = s.SecurityTypeID 
								AND s.ShortName  = 'AdminTask'	
								AND r.usergroupid = rg.usergroupid
								AND rg.PersonID=#request.relayCurrentUser.personid# )
			OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
			OR (FlagGroup.ViewingAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights
					WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
					AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
					AND FlagGroupRights.Viewing <> 0)))
		
		<!---added by DF --->		
		<cfif isDefined("arguments.searchPhrase") and arguments.searchPhrase neq "">
			AND (FlagGroup.FlagGroupTextID  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >  or FlagGroup.Name  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >  or Flag.Name  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >  or Flag.FlagTextID  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" > )
		</cfif>
				
		UNION
		SELECT 
		FlagGroup.ParentFlagGroupID,
		FlagGroup.FlagGroupID,
		FlagGroup.EntityTypeID, 
		FlagGroup.FlagGroupTextID,
		FlagGroup.FlagTypeID,
		FlagGroup.Active,
		FlagGroup.Name,
		FlagGroup.description,
		FlagGroup.ParentFlagGroupID,
		FlagGroup.ViewingAccessRights,
		FlagGroup.EditAccessRights,
		FlagGroup.DownloadAccessRights,
		FlagGroup.SearchAccessRights,
		p.OrderingIndex,
		FlagGroup.OrderingIndex,
		(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID),
		
		(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
		(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
			OR (s.EditAccessRights = 0 AND s.Edit <> 0)
			or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID 
						AND s.ShortName  = 'AdminTask'	
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )	
		OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
		                AND s.FlagGroupID = t.FlagGroupID
		                AND t.UserID=#request.relayCurrentUser.usergroupid#
		                AND t.Edit <> 0))))
		FROM FlagGroup, FlagGroup as p
		WHERE FlagGroup.ParentFlagGroupID <> 0
		AND p.ParentFlagGroupID = 0
		AND flagGroup.active = 1
		
		
		<cfif compareNoCase(arguments.EntityTypeID,"OTHER") eq 0>
			AND FlagGroup.EntityTypeID  not in ( <cf_queryparam value="#arguments.discardEntityTypeIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<cfelse>
			AND FlagGroup.EntityTypeID =  <cf_queryparam value="#arguments.EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		
		
		<!--- Added by SWJ to constrain the flag list a bit --->
		AND p.FlagGroupID = FlagGroup.ParentFlagGroupID
		AND FlagGroup.Scope  IN ( <cf_queryparam value="#arguments.UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
		AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
			OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
			or exists (	SELECT 1 FROM RightsGroup as rg, UserGroup as ug 
								WHERE rg.UserGroupID = ug.UserGroupID
								AND rg.PersonID=#request.relayCurrentUser.personid# AND ug.Name='Admin-Standard'  )
			or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
								WHERE  r.SecurityTypeID = s.SecurityTypeID 
								AND s.ShortName  = 'AdminTask'	
								AND r.usergroupid = rg.usergroupid
								AND rg.PersonID=#request.relayCurrentUser.personid# )	
			OR (FlagGroup.ViewingAccessRights <> 0
				AND EXISTS (SELECT 1 FROM FlagGroupRights
					WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
					AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
					AND FlagGroupRights.Viewing <> 0)))
		
		<!--- Added By DF --->
		
		<cfif isDefined("arguments.searchPhrase") and arguments.searchPhrase neq "">
			AND ( FlagGroup.FlagGroupTextID  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >  or FlagGroup.Name  like  <cf_queryparam value="%#replaceNoCase(arguments.searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >  )
		</cfif>
		
		ORDER BY parent_order, grouping, child_order, flagGroup.Name   <!--- DF added flagGroup.Name was just 'Name' --->
		</CFQUERY>

		<cfreturn getProfileFlagGroups>
	</cffunction>	
	
</cfcomponent>

