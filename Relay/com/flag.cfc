<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:	flag.cfc

Template with various flag things in it
eg: createFlagGroup
	createFlag


WAB 2004-07-28
WAB 2005-01-18    made checkBooleanFlagByID take a flagtextid

WAB  2005-07-18   added a couple of <cfset var  ...  s to  getJoinInfo flag
WAB  2005-10-17 added functions doesCurrentUserHaveRightsToEditFlagGroupDefinition and doesCurrentUserHaveRightsToEditFlagDefinition
WAB 2005-10-17 added createdBy to flag and flaggroup structures
WAB 2007-02-13   added some locking to updateflagstructure  and updateflagGroupstructure  -appeared as if the old version could go wrong and delete the whole structure
WAB 2007-11-06 isFlagTextIDUnique function brought from webservices\misc.cfc
WAB 2008/04/29   added nvarchar support to setflag function
NJH 2008/06/02 altered setbooleanflag to be able to use requestTime as the date
WAB 2008/10/21  added automatic creation of views based on flagGroups
WAB 2009/01/14  CR-TND556 changed createJoinForFlagGroup to do radios quicker in download
WAB 2009/01/30 Mod in updateFlagStructure - seee comment
WAB 2009/02/03	altered evaluateFlagExpression so that words TRUE and FALSE are understood and not taken as flagNames
WAB 2009/02/03  change to flagRights code to make sure that session cache is updated when flaggroups are updated.
				Note ideally needs uploading at a quiet time!
				Needs flag structures  reloading at same time (reset application variables)
				Possibly every session needs structDelete(session,"FlagGroupRights") run on it (tricky)
WAB 2009/02/18  Mod to getFlagData to indicate if a linked entity no longer exists
WAB 2009/02/25 	added a left to the insert of name and description in addFlag function
NJH	2009/05/28	P-SNY063 - new function to copy flags from one entity to another.
WAB 2009/06/08 LID 2335, createFlagGroupDefaultTranslation was defaulting to flag_ rather than flagGroup_
WAB 2009/09/16 	LID 2572   Make sure that autocreated phrasetextids are valid and unique
NJH 2009/09/21 	LHID 2616
NJH 2009/09/24	LHID 2680
NYB 2009-10-02 	LHID2716 - added cftry/catch in IsFlagSetForPerson as was throwing an error when a flag id was passed for a flagid which doesn't exist
NJH 2009/10/13	P-FNL079 - a little bug fix. Added alias to table to get rid of an ambigious columns in a number of queries such as get/set flag data. The opportunity table had both entityID and flagId columns.
NJH/WAB 2010/02/02	LID2828 - added argument to createFlag and createFlagGroup that allows us to not update the memory structure. Used in bulk uploading
				to improve performace. Also now expect a list of flag(group)Ids in updateFlag(group)Structure so that we can update the structure for a list of ids, rather than
				just a single one.
WAB 2010/02/02	Added function getFlagGroupStructure and getFlagStructure to beused instead of accessing the application variables directly
WAB 2010/03/24 	Alter doesflagGroupExist to handle flagGroupTextIDs (in case of grou not existing)
WAB 2010/06 	Brought in some functions underdevelopment for outputting flag controls
NAS 2010/06/23 	P-CLS002 - Amended function to receive FlagGroupTextID
NAS 2010/09/29 	LID4188: added an if to cater for Date data on SetFlagData
NJH 2010/12/24 	Added deleteFlagGroupData function
PPB 2011/06/14 	REL106 - add showInConditionalEmails
PPB 2011/09/02 	LID5916 return only active flags from getFlagGroupData
WAB 2011/10	P-TND109 Changes to getFlagData and getFlagDataAndControls to deal with entities which don't exist yet
WAB 2011/10/17	Fix parts of getFlagDataAndControls which had never been used (Memo)
2011-11-28	NYB	LHID8224 changed to call in limitTextArea.cfm instead of limitTextArea.js - to translate js
2012-01-10  WAB make updateFlagStructure cluster aware with applicationScope
WAB 2012-02-22	Add date flag support to getFlagDataAndControls - still need to do work on required and view mode
2012/03/07 	PPB P-REL115 API new fn getFlagQuerySnippets
2012/03/12 	PPB P-AVD001 - added CFOUTPUT around for date flag
2012/05/03	IH	Case 427913 set flagGroupID to 0 when it doesn't exist
2012/05/16 	PPB LEX070 added CFOUTPUTs around "required" field for date flag
2012-05-29 	PPB P-MIC001 added sortorder defaults for getFlagData/getFlagGroupData
2012-08-16 	PPB Case 430059 pass thro "required" attribute for integer flags
2012-11-19 	WAB CASE 431750. pass through required (and other formatting parameters) for date flags (calendar).
2012/12/04	NJH  Roadmap2013 - support for multiple text flags
2012-11-27 	WAB CASE 432266 problem in setFlagData() when integer data had decimal point
2013/01/08	NJH 2013Roadmap - added support for decimal and textMultiple flags.. return flagFormatting parameters on getFlagDataAndControl
2012/01/	WAB added EntityTableAlias argument to getJoinInfoForFlag
2013-02-25 	PPB Case 431614 allow textarea to be disabled/readonly
2013-03-12 	NYB/NJH Case 433498 added lastupdated fields to flags in general (lastUpdated,lastUpdateByperson)
2013-03-26 	WAB	P-AVD005 getFlafDataAndControl - added the required attribute to a single checkbox
2013-09-24	NJH Added flagGroupDoesNotExist and flagDoesNotExist application structures to speed up whether a flagGroup/flag exists.
2013-10-10	WAB CASE 437400 Fix Problem saving checkbox groups if flagTextID contains an underscore. getFlagGroupDataAndControls() was not converting attributes.flagGroupID to a numeric flagGrouptextID when generating the fieldName
2013-10-15 	WAB	CASE 437370 Allow checkboxes to be shown as twoSelectCombo
2013-11-11 	WAB	CASE 436913 (Xirrus Opportunities) Add Support for Time Field on date flags
2013-11-	WAB 2013RoadMap2 Item 25 ContactHistory. Altered getJoinInfoForListOfFlags/Flaggroups so that any additional arguments are passed through ( I needed a specific alias).
2014-01-14 NYB Case 438608 added translatedname to query rtnd by getFlagGroupFlags
2014-02-25 	WAB CASE 439006 Added view mode for date flags.
2014-07-04  REH case 440842 getFlagDataAndControls update, value to be displayed can obtained from defaultValue (if provided) and only if number of records returned from flagData is zero
2014-07-10 	PPB Case 440579 added optional EntityName argument to doesFlagGroupExist and doesFlagExist
2014-07-04  REH case 440961 related to change made for 440842, needed to create a variable to hold a list to accomodate use of valuelist
2014-08-18 REH Case 441300 added order by using orderingIndex to getFlagGroupFlags query
2014-08-11  RPW Create new profile type: "Financial"
2014-09-08	RPW	Financial Flag - Error in selections and downloads
2014-09-24 WAB CASE 440960 processFlagForm was not deleting flags when data value blank (hidden error on TextFlags but error thrown on integerFlags)
2014-11-12 WAB alter charsLeft code on textareas to use P tag and new javascript (and removed from multiText flags)
2014-10-21 AHL Case 442156 Unable to access: Opportunity Invalid data ' for CFSQLTYPE CF_SQL_INTEGER.
2014-12-16 WAB CASE 442889 Add support for size and maxlength attributes on decimal flags
2015-01-20 WAB CASE 442889 problem with previous fix, needed to default size and maxlength for integer flags
2015-02-12 AXA P-AER001 use translated name for required label and pass in required attribute correctly
2015-02-23	RPW	FIFTEEN-217 - API - [GET] command is not working for [oppProductCollection]
2015/02/25 NJH  Jira Fifteen-223 treat an empty data, where data is expected, as a delete in setflagdata
2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags
2015-05-01 AXA Created new method to do pass in decimal formatting to cf_input for financial flags that are readonly.  Called it from getFlagDataAndControls and added some display logic.
2015-05-06	NJH	return empty array for getFlagGroupDataAndControls if flagGroup doesn't exist
2015-05-05	NJH Add ID and required to textAreas. Required was not working.
2015-06-22	WAB Added support for dot notation in flagformatting structure (converted to structure of structure).  So now the same as flagGroup formatting
2015-06-22 	WAB FIFTEEN-384 Add support for localTime in DateFlags	Removed call to combineDateFormFields() now done in applicationMain
2015/09/24	NJH	Check uniqueness of flagGroupTextIds against the entity columns as well.
2015-10-06	WAB Jira PROD2015-41 Added support for range attribute on numeric flags
2015-10-28  DAN add helper methods dedicated for getting the flagId by flagTextId and also to find booleanFlagData for the given entity (support for Case 446369)
2015-10-29  DAN update getFlagIdByFlagTextId to use existing getFlagStructure method under the hood and remove getBooleanFlagDataForEntity as there is existing method checkBooleanFlagByID
2015-11-13	RJT Sugar 446563 - Removed dependancy on relaycurrentuser for functions that don't explicitly reference the current user
2015-12-02    ACPK    BF-39 Fixed bug caused when LinkedEntityNameExpression is empty string
2016-01-10 	WAB createFlagTypeAndFlagEntityTypeStructures() - change query to use information_schema tables to get information about flag tables
				add	cfsqlType to flagTypeStructure
2016/02/16	NJH	JIRA PROD2015-589 - set the flagTextID to flagGroupTextID + flagname if flagTextID is not set in createFlag function.
2016-03-01 	WAB When adding a flag, need to remove flagtextID as well as flagid from flagDoesNotExist structure
2016/03/16	NJH modified getFlagSnippets function so that text, date and integer flags all use the same query snippet... had to include date as well
2016/03/31	RJT	BF-570 Where an automatically generated text ID is entirely filtered out (e.g. is kanji characters) a random (but garanteed unique) string is used instead
2016-06-08	WAB BF-973 getFlagQuerySnippets() not supporting all flagTypes
2016-09-21 WAB	PROD2016-2349 'Select A Value' appearing as item in text/integermultipleflag multiselects
2016/09/27	NJH		JIRA PROD2016-2383 - removed flagGroup expiry
2016-10-05	WAB	PROD2016-2433 getFlagDataAndControl().  Issue with validValues when entityID is not numeric (ie a new record).  Only occurred for textMultiple flags
2016-12-07	WAB	PROD2016-2682 FormRenderer  Added functions to blow metaDataCache.  Also removed some other unused functionality (mainly concerned with loading whole flag structures in one go)
2016/12/13	NJH		JIRA PROD2016-PROD2016-2957 - added createdBy/lastUpdatedBy person fields
2017-01-20	WAB	RT-20	Performance Problems Kaspersky API. Altered getJoinInforForFlagGroup to use the new indexed vRadioFlagGroupData view to improve performance
--->

<!---
Contains Functions:
	checkBooleanFlagByID			-> returns: boolean
	convertFlagWDDXToQuery
	convertFlagWDDXToStruct
	createFlag
	createFlagGroup
	createFlagGroupNameDefaultTranslation
	createFlagNameDefaultTranslation
	createFlagStructure
	createFlagTypeAndFlagEntityTypeStructures
	deleteFlagData
	doesCurrentUserHaveRightsToEditFlagDefinition
	doesCurrentUserHaveRightsToEditFlagGroupDefinition
	doesCurrentUserHaveRightsToFlag
	doesCurrentUserHaveRightsToFlagGroup
	doesFlagExist
	doesFlagGroupExist
	evaluateFlagExpression
	getBooleanFlagList
	getDefaultFlagGroupFormattingParameters
	getFlagData			-> returnType: "query"
	getFlagDataAndControls			-> returnType: "query"
	getFlagDataForCurrentUser
	getFlagDataForPerson
	getFlagEntityType			-> returnType: "query"
	getFlagFormattingParameters
	getFlagGroupChildGroups
	getFlagGroupData			-> returnType: "query"
	getFlagGroupDataForCurrentUser			-> returnType: "query"
	getFlagGroupDataForPerson			-> returnType: "query"
	getFlagGroupFlags
	getFlagGroupFormattingParameters
	getFlagGroupRights
	getJoinInfoForFlag
	getJoinInfoForFlagGroup
	getJoinInfoForListOfFlagGroups
	getJoinInfoForListOfFlags
	isFlagGroupTextIDUnique
	isFlagSetForCurrentUser:  returns boolean, based on isFlagSetForPerson
	isFlagSetForPerson:  returns boolean, based on
	isFlagTextIDUnique
	isTextIDValid
	mergeFlagGroupAndFlagFormattingParameters - boolean
	setBooleanFlag
	setBooleanFlagByID
	setFlagData
	setFlagDataForPerson
	unsetBooleanFlag
	updateFlagGroupStructure
	updateFlagStructure
	validValueDisplay

 --->

<cfcomponent displayname="Various Methods for Relay Flags" hint="">
	<cffunction access="public" name="createFlagStructure" output="no" >
		<cfargument name="applicationScope" default="#application#">
		<!--- creates a structure allowing lookup of flagid from a flagtextid --->
		<!--- superceded by updateflagstructure --->
		<cfscript>
			var getFlags = '';
			var flagidStruct = structnew() ;
		</cfscript>

		<CFQUERY NAME="getFlags" DATASOURCE="#applicationScope.SiteDataSource#">
		select flagid, flagtextid from flag
		</cfquery>

		<cfloop query="getFlags">
			<cfif flagtextid is not "">
				<cfset flagidStruct[flagtextid] = flagid>
			</cfif>
			<cfset flagidStruct[flagid] = flagid>
		</cfloop>

		<cfset arguments.applicationScope.flagid  = flagidStruct>

	</cffunction>


	<cffunction access="public" name="createFlagTypeAndFlagEntityTypeStructures" output="yes">
			<cfargument name="applicationScope" default="#application#">

		<cfset var getFlagTypes = "">
		<cfset var getFlagEntityTypes = "">
		<cfset var tempFlagdataTable = arrayNew(1)>
		<cfset var tempFlagTypeID = structNew()>
		<cfset var tempFlagType = structNew()>		<!--- WAB 2005-03-02 store all info on flagtypes--->
		<cfset var tempRowStruct = "">
		<cfset var column = '' />
		<cfset var testTable = '' />


		<cfset var structureFunctionsObj =  CreateObject("component","relay.com.structureFunctions")>

		<!--- flag variables--->
				<CFQUERY NAME="getFlagTypes" DATASOURCE="#applicationScope.siteDataSource#">
					Select name, FLAGTYPEID, datatablefullname,
					CASE WHEN isNull(DataTable,'') <> '' THEN dataTable ELSE 'ERROR' END as DataTable
					from flagType
					order by flagtypeid
				</CFQUERY>

				<CFQUERY NAME="getFlagEntityTypes" DATASOURCE="#applicationScope.siteDataSource#">
					Select tablename, EntityTypeid
					from flagEntityType
					order by EntityTypeid
				</CFQUERY>



				<!---
					NJH 2010/08/09 Rw8.3 - removed this application variables.. used data from flag structures instead.
				<CFSET application.typelist = valuelist(getFlagTypes.name)>
				<CFSET application.typeDatalist = valuelist(getFlagTypes.DataTable)>
				<CFSET application.typeEntityTable = valuelist(getFlagEntityTypes.TableName)> --->


				<cfset structdelete (applicationScope,"flagType")>

				<!--- WAB
				2001-03-19
				trying out having the full name of the flagTables stored in the database
				allows us to have tables which don't have to have the flagdata in the name
				WAB 2016-01-10 Change query to use information_schema tables and work out the cfsqldatatype
				 --->

				<cfloop query="getFlagTypes">
					<cfset tempFlagdataTable[flagtypeid] = dataTableFullName>
					<cfset tempFlagTypeID[name] = flagTypeID>
					<cfset tempRowStruct = structNew()>
					<cfloop index="column" list = "#getFlagTypes.columnlist#">
						<cfset tempRowStruct[column] = getFlagTypes[column][currentrow]>
					</cfloop>
						<cfif flagTypeID is not 1><!--- 1 is group --->
							<CFQUERY NAME="testTable" DATASOURCE="#applicationScope.siteDataSource#">
							select
								  tableExists = case when exists (select 1 from information_schema.tables where table_name = <cf_queryparam value="#dataTableFullName#" CFSQLTYPE="CF_SQL_VARCHAR" >) then 1 else 0 end
								, hasDataColumn = case when exists (select 1 from information_schema.columns where table_name = <cf_queryparam value="#dataTableFullName#" CFSQLTYPE="CF_SQL_VARCHAR" > and column_name = 'data') then 1 else 0 end
								, hasSortIndexColumn = case when exists (select 1 from information_schema.columns where table_name = <cf_queryparam value="#dataTableFullName#" CFSQLTYPE="CF_SQL_VARCHAR" > and column_name = 'sortorder') then 1 else 0 end
								, sqldataType = (select data_Type from information_schema.columns where table_name = <cf_queryparam value="#dataTableFullName#" CFSQLTYPE="CF_SQL_VARCHAR" > and column_name = 'data')
							</CFQUERY>
							<cfset tempRowStruct.hasSortIndexColumn= testTable.hasSortIndexColumn>
							<cfset tempRowStruct.hasDataColumn = testTable.hasDataColumn>
							<cfset tempRowStruct.sqldataType = testTable.sqldataType>
							<cfif testTable.hasDataColumn>
								<cfset tempRowStruct.cfsqlType = applicationScope.dataTypeLookup[testTable.sqldataType].cfsql>
							</cfif>
						</cfif>
						<cfset tempRowStruct.DefaultFormattingParameters = getDefaultFlagGroupFormattingParameters(name)>
					<cfset tempFlagType[FlagTypeID] =temprowstruct >

				</cfloop>


				<cfset arguments.applicationScope.flagdataTable = tempFlagdataTable>
				<cfset arguments.applicationScope.flagTypeID = tempFlagTypeID>

				<!--- I merge the temporary structure into the existing app variable (rather than just doing an =) so that pointers to it in the flag group structure are maintained--->
				<cfif structKeyExists (applicationScope,"flagType")>
					<cfset structureFunctionsObj.structMerge (struct1 = applicationScope.FlagType, struct2 = tempFlagType)>
					<!--- note that the above won't delete default parameters which are removed completely - need to null them (or reload from scratch) --->
				<cfelse>
					<cfset arguments.applicationScope.FlagType = tempFlagType>		<!--- WAB 2005-03-02 store all info on flagtypes--->
				</cfif>

	</cffunction>


		<!--- WAB 2005-09-28 added cluster handling --->
	<cffunction name="updateFlagGroupStructure" access="public" output="no">
		<cfargument name="flaggroupid" type="string" required = "true" hint = "A list of flagGroupIDs or flagGroupTextIDs ">
		<cfargument name="updateCluster" type="string" required="no" default="true">

		<cfset var flagGroupItem = "">
		<cfset var getFlagGroups = "">
		<cfset var getFlags = "">
		<cfset var structureFunctionsObj =  CreateObject("component","relay.com.structureFunctions")>
		<cfset var regExpobject =  CreateObject("component","relay.com.regexp")>
		<cfset var tempflaggroupStruct = structNew()>
		<cfset var column = "">
		<cfset var updateFlagFormattingParameters= false>
		<cfset var flaggrouptextid = '' />
		<cfset var getChildGroups = '' />
		<cfset var entityType_forBlowingCache = '' />
		<cfset var fieldNames_forBlowingCache = '' />

		<!--- creates an application structure giving all the information required about a flaggroup
		(actually it is a structure of structures)

		can be referenced in the form application.flaggroup[flaggroupid].field   eg. application.flaggroup[27].name
							or 			application.flaggroup[flaggroupid][field]  eg. application.flaggroup[27]["name"]
		also keyed on flaggrouptextid   eg. application.flaggroup["myBooleanflaggroup"]["name"]

		 --->
		<cflock name = "#application.applicationname#_updateFlagGroupStructure_#flagGroupid#" timeout = "5">


		<!--- 2014-09-08	RPW	Financial Flag - Error in selections and downloads --->
		<CFQUERY NAME="getFlagGroups" DATASOURCE="#application.SiteDataSource#">
		select
			fg.name,
			case when isNull(fg.NamePhraseTextID,'') = '' then fg.Name else 'phr_'+fg.NamePhraseTextID end as translatedName,
			fg.helpText,
			fg.description,
			case when isNull(fg.DescriptionPhraseTextID,'') = '' then fg.Description else 'phr_'+fg.DescriptionPhraseTextID end as translatedDescription,
			case when fg.parentflaggroupid <> 0 then (select active from flaggroup where flaggroupid = fg.parentflaggroupid) & fg.active else fg.active end as Active,
			fg.parentflaggroupid,fg.flaggroupid, fg.flaggrouptextid, fg.flagtypeid, fg.entitytypeid,
			case when fg.parentflaggroupid <> 0 then (select  viewingaccessrights from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.viewingaccessrights  else fg.viewingaccessrights end as viewingaccessrights,
			case when fg.parentflaggroupid <> 0 then (select  editaccessrights from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.editaccessrights  else fg.editaccessrights end as editaccessrights,
			case when fg.parentflaggroupid <> 0 then (select  searchaccessrights from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.searchaccessrights  else fg.searchaccessrights end as searchaccessrights,
			case when fg.parentflaggroupid <> 0 then (select  downloadaccessrights from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.downloadaccessrights  else fg.downloadaccessrights end as downloadaccessrights,
			case when fg.parentflaggroupid <> 0 then (select  viewing from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.viewing  else fg.viewing end as viewing,
			case when fg.parentflaggroupid <> 0 then (select  edit from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.edit  else fg.edit end as edit,
			case when fg.parentflaggroupid <> 0 then (select  search from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.search  else fg.search end as search,
			case when fg.parentflaggroupid <> 0 then (select  download from flaggroup where flaggroupid = fg.parentflaggroupid) | fg.download  else fg.download end as download,
			fg.EntityTypeID,
			fg.createdby,
			formattingParameters,
			getdate() as structureLastUpdated,   <!--- WAB 2009-02-04 to allow blowing of session.flagRights structure --->
			1 as isOK,   <!--- WAB 2010-02-02 - when a flag group is requested but does not exist then we set isOK to false, so need an isok = true here  --->
--			ft.name as FlagType,
--			ft.dataTable as DataTable,
--			ft.DataTableFullName as DataTableFullName,
--			fet.tablename as EntityType,
--			fet.EntityTypeID as EntityTypeID,
--			fet.tableName as entitytablename,
--			fet.NameExpression as entitynameExpression,
--			fet.uniqueKey as entityuniquekey,
			fg.flagGroupTextID as apiName,
			'flagGroup' as fieldType,
			fg.FlagTypeDecimalPlaces
		 from
			flaggroup fg
				inner join
			flagtype ft on fg.flagtypeid = ft.flagtypeid
				inner join
			flagentitytype fet on fet.entitytypeid = fg.entitytypeid

			<cfif arguments.flaggroupid is not "">
				where
				<!--- WAB/NJH 2010-02-02 LID 2828 - give ability to pass in a list of flagIds --->
				<cfif isNumeric (listFirst(arguments.flaggroupid))>
					fg.flaggroupid  in ( <cf_queryparam value="#arguments.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<cfelse>
					fg.flaggrouptextid  in ( <cf_queryparam value="#arguments.flaggroupid#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				</cfif>
			</cfif>
		</cfquery>



<!--- if no flaggroupid set then we are updating the whole structure
	I considered clearing out the old structure, and creating a new one, but there will be flags which have pointers into this structure,
	so I can't just recreate keys, I have to overwrite the existing ones.
	There is a danger since a flaggroup which has been deleted will continue to exist.  I could do a check for this but haven't,
	and actually don't think that there will be much of a problem
 --->


		<!--- convert to a structure of structures --->
		<cfloop query="getFlagGroups">

			<cfset flagGroupItem = structNew()>


			<cfloop index="column" list = "#getflagGroups.columnlist#">
				<cfset flagGroupItem[column] = getflagGroups[column][currentrow]>
			</cfloop>

				<cfset flagGroupItem.entityType = application.entityType[entityTypeID]>
				<cfset flagGroupItem.FlagType = application.flagType[flagTypeID]>

			<!--- get ids of child flags or child flaggroups
			<cfif flagTypeID is 1><!--- group --->
				<CFQUERY NAME="getChildGroups" DATASOURCE="#application.SiteDataSource#">
				select flaggroupid from flaggroup where parentFlagGroupID = #flaggroupid# order by orderingIndex
				</cfQUERY>
				<!--- <cfset tempFlagGroupStruct[getFlagGroups.flagGroupid].childFlagGroups> --->
			</cfif>
			--->

				<!--- 2007-01-18 WAB added structured way of passing formatting info around --->

				<cfset flagGroupItem.FormattingParametersStructure = regExpobject.convertNameValuePairStringToStructure(inputString = flagGroupItem.FormattingParameters,delimiter = ",", dotnotationtoStructure = true)>  <!--- WAb 2008-10-21 added handling of dot notation --->

				<!--- if the formatting parameters have changed then need to update a bit of the flag structure as well --->
				<cfset updateFlagFormattingParameters = false>
				<cfif structkeyExists (application.flaggroup,getFlagGroups.flaggroupid) and flagGroupItem.FormattingParameters is not application.flaggroup[getFlagGroups.flaggroupid].FormattingParameters>
					<cfset updateFlagFormattingParameters = true>
				</cfif>

				<!--- I merge the temporary structure into the existing app variable (rather than just doing an =) so that pointers to it in the flag group structure are maintained--->
				<cfif structKeyExists (application.flaggroup,getFlagGroups.flaggroupid)>
					<cfset structureFunctionsObj.structMerge (application.flaggroup[getFlagGroups.flaggroupid], flagGroupItem)>
					<!--- actually this means that if something is removed from FormattingParametersStructure then it doesn't disappear, so I'm going to overwrite this anyway --->
					<cfset application.flaggroup[getFlagGroups.flaggroupid].FormattingParametersStructure = flagGroupItem.FormattingParametersStructure >
				<cfelse>
					<cfset application.flaggroup[getFlagGroups.flaggroupid] = flagGroupItem >
				</cfif>

				<cfset structDelete(application.flagGroupDoesNotExist,flagGroupID)>

				<cfif getFlagGroups.flagGrouptextid is not "">
					<cfset application.flaggroup[getFlagGroups.flaggrouptextid] =  flagGroupItem>
					<!--- WAB 2016-03-01 Need to remove flagGroupTextID as well as flagGroupid from flagGroupDoesNotExist structure --->
					<cfset structDelete(application.flagGroupDoesNotExist,getFlagGroups.flaggrouptextid)>
				</cfif>

				<!--- if the formatting parameters have changed then update the underlying flag structure as well --->
				<cfif updateFlagFormattingParameters>

					<CFQUERY NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
						select * from flag where flagGroupID =  <cf_queryparam value="#getFlagGroups.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>
					<cfloop query = "getFlags">
						<cfif structKeyExists (application.flag,flagid)>
							<cfset mergeFlagGroupAndFlagFormattingParameters (application.flag[flagid])>
						</cfif>
					</cfloop>

				</cfif>

			<!---
			NJH 2017/01/09 PROD2016-3198 need to blow cache for all flagGroups because flag rights are on the group
			<cfif flagGroupItem.FlagType.dataTable is "boolean"> --->
				<cfset entityType_forBlowingCache = flagGroupItem.entityType.tableName>
				<cfset fieldNames_forBlowingCache = listAppend(fieldNames_forBlowingCache,flagGroupItem.flagGrouptextID)>
			<!--- </cfif> --->

		</cfloop>


		<cfif getFlagGroups.recordcount is 0 and flagGroupid is not "">
			<!--- this must be a flagGroup which has been deleted, so delete it from the structure also delete reference by flagGrouptextid --->
			<cfif structkeyexists (application.flagGroup,flagGroupid)>
				<cfset entityType_forBlowingCache = application.flagGroup[flagGroupid].entityType.tableName>
				<cfset fieldNames_forBlowingCache = application.flagGroup[flagGroupid].flagGroupTextID>

				<cfset flaggrouptextid = application.flagGroup[flagGroupid].flagGrouptextid>
				<cfset  structdelete (application.flagGroup,flagGroupid)>
				<cfif flagGrouptextid is not "">
					<cfset  structdelete (application.flagGroup,flagGrouptextid)>
				</cfif>
			</cfif>
			<cfset application.flagGroupDoesNotExist[flagGroupID] = "">
		</cfif>




</cflock>


		<cfif updateCluster and structKeyExists(application,"com")> <!--- WAB 2008-01-09 had to add the structKeyExists so that flags could be loaded during boot before com is loaded --->
			<cfset application.com.clusterManagement.updateCluster(updatemethod="updateFlagGroupStructure",flagGroupID = #flagGroupID#)>

			<!--- Blow new metadata cache.  Strictly speaking only needed for booleanFlagGroups
				NJH 2017/01/09 PROD2016-3198 need to blow cache for all flagGroups because flag rights are on the group --->
			<cfif entityType_forBlowingCache is not "">
				<cfset application.com.entities.blowCache (entityType = entityType_forBlowingCache, fieldNames = fieldNames_forBlowingCache)>
			</cfif>
		</cfif>

	<cfreturn true>

</cffunction>


	<cffunction access="public" name="updateFlagStructure" output="yes">
		<cfargument name="flagid" type="string" required = true>
		<cfargument name="updateCluster" type="string" required="no" default="true">
		<cfargument name="applicationScope" default="#application#">

		<cfset var tempFlagStruct = '' />
		<cfset var flagItem = '' />
		<cfset var defaultStructure = '' />
		<cfset var regExpobject = '' />
		<cfset var x = '' />
		<cfset var parentFlagGroupID = '' />
		<cfset var flagtextid = '' />
		<cfset var column = '' />
		<cfset var getFlags = '' />

		<cfset var entityType_forBlowingCache = '' />
		<cfset var fieldNames_forBlowingCache = '' />

		<!--- creates an application structure giving all the information required about a flag
		(actually it is a structure of structures)

		can be referenced in the form application.flag[flagid].field   eg. application.flag[27].name
							or 			application.flag[flagid][field]  eg. application.flag[27]["name"]
		also keyed on flagtextid   eg. application.flag["myBooleanFlag"]["name"]

		 --->
		<cflock name = "#applicationScope.applicationname#_updateFlagStructure_#flagid#" timeout = "5">


		<CFQUERY NAME="getFlags" DATASOURCE="#applicationScope.SiteDataSource#">
		select
			f.flaggroupid,
			f.flagid, f.flagtextid,
			f.name,
			case when isNull(f.NamePhraseTextID,'') = '' then f.Name else 'phr_'+f.NamePhraseTextID end as translatedName,
			f.description,
			f.helpText,
			case when isNull(f.DescriptionPhraseTextID,'') = '' then f.Description else 'phr_'+f.DescriptionPhraseTextID end as translatedDescription,
			f.usevalidvalues,
			f.lookup,
			f.linkstoentityTypeID,
			f.SubentityTypeID,
			f.hasmultipleinstances,
			f.wddxstruct,
			fg.EntityTypeID as EntityTypeID,
			fg.flagtypeid as flagtypeid,
			case when fg.parentflaggroupid <> 0 then (select active from flaggroup where flaggroupid = fg.parentflaggroupid) & fg.active & f.active else fg.active & f.active end as flagActive,
			f.createdby,
			f.formattingParameters	,
			getdate() as structureLastUpdated,   <!--- WAB 2009-02-04 to allow blowing of session.flagRights structure --->
			1 as isOK,  <!--- WAB 2010-02-02 - when a flag is requested but does not exist then we set isOK to false, so need an isok = true here  --->
--			ft.name as FlagType,
--			ft.DataTableFullName as DataTableFullName,
--			fet.tablename as EntityType,
--			fet.EntityTypeID as EntityTypeID,
			f.flagtextid as apiName,
			'flag' as fieldType,  <!--- NJH 2012/03/26 added apiName --->
			fg.FlagTypeCurrency,	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
			fg.FlagTypeDecimalPlaces	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->

		 from
			flaggroup fg
				inner join
			flag f on fg.flaggroupid = f.flaggroupid
				inner join
			flagtype ft on fg.flagtypeid = ft.flagtypeid
				inner join
			flagentitytype fet on fet.entitytypeid = fg.entitytypeid

			where
			<cfif isNumeric (listfirst(arguments.flagid))>
				f.flagid IN (#arguments.flagid#)
			<cfelse>
				f.flagtextid IN (#listqualify(arguments.flagid,"'")#)
			</cfif>

		</cfquery>


<!--- if no flagid set then we are updating the whole structure and we need to clear out the old application structure
		if there is a flagid set then we are just updating the existing stucture
 --->

<!--- convert to a structure of structures --->
<cfloop query="getFlags">

	<cfset flagItem = structNew()>

	<cfloop index="column" list = "#getflags.columnlist#">
		<cfset flagItem[column] = getflags[column][currentrow]>
	</cfloop>

		<cfset flagItem.entityType = applicationScope.entityType[entityTypeID]>
		<cfset flagItem.FlagType = applicationScope.flagType[flagTypeID]>

		<cfset flagItem.Flaggroup = getFlagGroupStructure(flaggroupid)>

		<cfset flagItem.isComplexFlag = false>
		<cfif getFlags.wddxstruct is not "">  <!--- WAB 2006-10-18 added complex flag stuff --->
			<CFWDDX ACTION="WDDX2CFML" INPUT="#getFlags.wddxstruct#" OUTPUT="defaultStructure">
			<cfif isArray(defaultStructure)>
				<cfset defaultStructure = defaultStructure[1]>
				<cfset flagItem.complexFlag.multiplerows = true>
			<cfelse>
				<cfset flagItem.complexFlag.multiplerows = false>
			</cfif>
			<cfset flagItem.isComplexFlag = true>
			<cfset flagItem.complexFlag.fields = defaultStructure>

		</cfif>


			<cfset regExpobject =  CreateObject("component","relay.com.regexp")>   <!--- application structure not available during boot--->

			<!--- 2007-01-18 WAB added structured way of passing formatting info around --->
			<cfset flagItem.FormattingParametersStructure = regExpobject.convertNameValuePairStringToStructure(inputString = flagItem.FormattingParameters, delimiter = ",", dotnotationtoStructure = true)> <!--- WAB 2015-06-22 added handling of dot notation (which has been in flaggroups for years)--->
			<!--- merge in the flagGroup parameters as well--->
			<cfset  mergeFlagGroupAndFlagFormattingParameters (flagItem)>



		<!--- WAB 2016-03-01 Need to remove flagtextID as well as flagid from flagDoesNotExist structure --->
		<cfset arguments.applicationScope.flag[getFlags.flagid] = flagItem>

		<cfset structDelete(applicationScope.flagDoesNotExist,flagID)>
		<cfif getFlags.flagtextid is not "">
			<cfset arguments.applicationScope.flag[getFlags.flagtextid] = flagItem>
			<cfset structDelete(applicationScope.flagDoesNotExist,getFlags.flagtextid)>
		</cfif>


		<cfset entityType_forBlowingCache = flagItem.entityType.tablename />
		<cfset fieldNames_forBlowingCache = listAppend(fieldNames_forBlowingCache, flagItem.flagTextID) />

</cfloop>



<cfif getFlags.recordcount is 0 and flagid is not "">
	<!--- this must be a flag which has been deleted, so delete it from the structure also delete reference by flagtextid --->
	<cfif structkeyexists (applicationScope.flag,flagid)>
		<cfset flagtextid = applicationScope.flag[flagid].flagtextid>
		<cfset entityType_forBlowingCache = applicationScope.flag[flagid].entityType.tablename />
		<cfset fieldNames_forBlowingCache = flagtextid />

		<cfset x = structdelete (applicationScope.flag,flagid)>
		<cfif flagtextid is not "">
			<cfset x = structdelete (applicationScope.flag,flagtextid)>
		</cfif>
	</cfif>
	<cfset arguments.applicationScope.flagDoesNotExist[flagid] = "">
</cfif>

</cflock>


		<cfif updateCluster and structKeyExists(applicationScope,"com")>  <!--- WAB 2008-01-09 had to add the structKeyExists so that flags could be loaded during boot before com is loaded --->
			<cfset arguments.applicationScope.com.clusterManagement.updateCluster(updatemethod="updateFlagStructure",flagID = #flagID#)>
			<cfset arguments.applicationScope.com.entities.blowCache (entityType = entityType_forBlowingCache, fieldnames = fieldNames_forBlowingCache)>
		</cfif>

	<cfreturn true>
</cffunction>


	<cffunction access="public" name="getDefaultFlagGroupFormattingParameters" returnType="struct">
		<cfargument name="typeName" type="string" required="true">

			<cfscript>
				var tempStruct = structNew() ;
				tempStruct.displayas = "" ;

				switch (typename) {

					case "checkbox" : {
						tempStruct.column = 3;   // number of columns in display
						tempStruct.size = 4;   // if displayed as multiselect, number of items
						tempStruct.maxPermitted = 0;
						tempStruct.maxPermittedMessage = 'phr_Validation_Max_Choices' ;
						break;
					}

					case "date" : {
						tempStruct.displayas = "calendar" ;
						break;
					}

					case "radio" : {
						break;
					}

					// NJH 2012/12/04 Roadmap2013 - support for decimal flags
					case "decimal" : {
						tempStruct.FlagNameOnOwnLine = 0;
						tempStruct.NoFlagName = 0;
						break;
					}

					// 2014-08-11  RPW Create new profile type: "Financial"
					case "financial" : {
						tempStruct.FlagNameOnOwnLine = 0;
						tempStruct.NoFlagName = 0;
						break;
					}

					case "text" : {
						tempStruct.size = 30;
						tempStruct.Maxlength = 50;
						tempStruct.FlagNameOnOwnLine = 0;
						tempStruct.mask = '';

						break;
					}

					// NJH 2012/12/04 Roadmap2013 - support for multiple text flags
					case "textmultiple" : {
						tempStruct.size = 5;
						tempStruct.Maxlength = 100;
						tempStruct.FlagNameOnOwnLine = 0;
						tempStruct.cols = 50;
						tempStruct.rows = 5;
						tempStruct.mask = '';
						tempStruct.NoFlagName = 0;
						break;
					}


				}

			</cfscript>

			<!---
				WAB 2015-10-06 Jira PROD2015-41
				work out the default max/min value for integer/finanical fields etc
				rather a large looking query to work out something simple!
				It is actually a copy of code in com.applicationVariables.getTableInfoStructure() getFieldInfo
				Removed manually set size and lengths in the code above
			--->
			<cfset var getSizeAndRange = "">
			<cfquery name="getSizeAndRange">
			declare @ten DECIMAL(38) = 10, @two DECIMAL(38) = 2
			select
			ft.name,
			case
				when c.numeric_precision is not null then
					c.numeric_precision +
					case when data_Type = 'tinyint' then 0 else 1 end +   /* add one for the negative sign (except tinyint) */
					case when c.numeric_Scale = 0 then 0 else 1 end			/* add one for the decimal point */
				else
					isNull(c.character_Maximum_Length,0)
				end
			as MaxLength,
			case
				when data_Type like 'tinyint' then
					'0'
				when data_Type like '%int' then
					CONVERT(VARCHAR (40), - (power (@two,(8*st.length)-1) ) )
				when numeric_precision is not null then
					CONVERT(VARCHAR (40), -(POWER (@ten , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (@ten,NUMERIC_SCALE) -1) ELSE '' END

				ELSE
					NULL

				end
			as [min],
			case
				when data_Type like 'tinyint' then
					CONVERT(VARCHAR (40), power (@two,(8*st.length)) -1 )
				when data_Type like '%int' then
					CONVERT(VARCHAR (40), power (@two,(8*st.length)-1) -1 )
				when numeric_precision is not null then
					CONVERT(VARCHAR (40), (POWER (@ten , (NUMERIC_PRECISION - NUMERIC_SCALE)) -1))  + CASE WHEN NUMERIC_SCALE IS NOT NULL THEN '.' + CONVERT(VARCHAR (40), POWER (@ten,NUMERIC_SCALE) -1) ELSE '' END
				end
			as [max]

			from
				information_schema.columns c
					inner join
				flagType ft on ft.dataTablefullName = table_name
					inner join
				sysTypes st on c.data_type = st.name
			where column_name = 'data' and numeric_precision is not null and ft.name = '#typeName#'
			</cfquery>

			<cfif getSizeAndRange.recordCount>
				<cfset tempStruct.max = getSizeAndRange.max>
				<cfset tempStruct.min = getSizeAndRange.min>
				<cfset tempStruct.range = "#getSizeAndRange.min#,#getSizeAndRange.max#">
				<cfset tempStruct.size = getSizeAndRange.maxLength>
				<cfset tempStruct.maxLength = getSizeAndRange.maxLength>
			</cfif>

		<cfreturn tempStruct>

	</cffunction>



	<cffunction access="public" name="mergeFlagGroupAndFlagFormattingParameters" returnType="boolean">
		<cfargument name="flagStructure" type="struct" required="true">

		<cfset var structureFunctionsObj =  CreateObject("component","relay.com.structureFunctions")>
		<cfset var x = structureFunctionsObj.structMerge(struct1 = flagStructure.flagGroup.FormattingParametersStructure,struct2 = flagStructure.FormattingParametersStructure, mergeToNewStruct=true )>

		<cfset arguments.flagStructure.FlagGroupAndFlagFormattingParametersStructure = x>

		<cfreturn true >


	</cffunction>

<!---
	WAB 2007/01/17
	returns a structure of all the formatting parameters for a flagGroup, merged with an additional structure which can be passed in
	so combines defaults for the type of flag, with settings from the individual flaggroup and with settings for use in this particular instance
 --->

	<cffunction access="public" name="getFlagGroupFormattingParameters" returnType="struct">
		<cfargument name="flaggroupid" type="numeric" required="true">
		<cfargument name="additionalParameterStructure" type="struct" default="#structNew()#">	<!--- for example from screens special formatting --->

		<cfset var flagGroupFormattingDefault = "">
		<cfset var flagGroupFormatting = "">
		<cfset var flagGroupStructure = getFlagGroupStructure(FlagGroupID)>

		<cfset flagGroupFormattingDefault = structCopy(flagGroupStructure.flagType.defaultFormattingParameters)>
		<!--- create flagGroupFormatting structure by merging defaults with flagGroup formattingParameters --->
		<cfset flagGroupFormatting = application.com.structureFunctions.structMerge(struct1 = flagGroupFormattingDefault,struct2 = flagGroupStructure.formattingParametersStructure, mergeToNewStruct = true)>
		<!--- then merge in items defined in additionalParameterStructure to override any default settings --->
		<cfset flagGroupFormatting = application.com.structureFunctions.structMerge(flagGroupFormatting,additionalParameterStructure)>

		<cfreturn 	flagGroupFormatting>

	</cffunction>


	<cffunction access="public" name="getFlagFormattingParameters" returnType="struct">
		<cfargument name="flagid" type="numeric" required="true">
		<cfargument name="additionalParameterStructure" type="struct" default="#structNew()#">

		<cfset var thisFlagStructure = getFlagStructure(FlagID)>

		<!--- create flagFormatting structure by merging defaults with flagGroupAndFlag formattingParameters --->
		<cfset var flagFormatting = application.com.structureFunctions.structMerge(struct1 = thisFlagStructure.flagType.defaultFormattingParameters,struct2 = thisFlagStructure.FlagGroupAndFlagFormattingParametersStructure, mergeToNewStruct = true)>
		<!--- merge in items defined in specialformattingcollection which override any settings  --->
		<cfset flagFormatting = application.com.structureFunctions.structMerge(struct1 = flagFormatting,struct2 = additionalParameterStructure)>


		<cfreturn flagFormatting >

	</cffunction>


	<!---
	WAB 2010/02/02
	Up until now the the flagGroupStructure has always been fully populated in memory
	However am worried about the time it is taking to load the structure at start up, therefore have created this function which would have to replace direct calls to application.flagGroup
	Will be a problem retro fitting this function everywhere.
	However, boot time is a particular problem with Sophos who are using lots of quiz flags.
	As a start I Think that it may be possible to specifically exclude quiz flags from the boot up memory struture and retrofit this function everywhere that quiz flags are used
	 --->
	<cffunction access="public" name="getFlagGroupStructure" returnType="struct" hint="returns flagGroup Structure, loading if necessary" output="false">
		<cfargument name="flagGroupID" type="string" required="true">

		<cfset var result = "">

		<cfif structKeyExists (application.flagGroup,flagGroupID)>
			<!--- if already in memory then return --->
			<cfset result = application.flagGroup[flagGroupID]>
		<cfelse>
			<!--- if not already in memory then try and load--->
			<cfset updateFlagGroupStructure(flagGroupID = flagGroupID, updateCluster = false)>
			<cfif structKeyExists (application.flagGroup,flagGroupID)>
				<!--- if it is now in memory then return --->
				<cfset result = application.flagGroup[flagGroupID]>
			<cfelse>
				<!--- if it still not in memory then the flagGroup cannot exist
				--->
				<!--- 2012/05/03	IH	Case 427913 set flagGroupID to 0 when it doesn't exist --->
				<cfset result = {isOK=0,flagGroupID=0}>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- NJH 2010/08/10 RW8.3 - added isOk=true to result if it was ok --->
	<cffunction access="public" name="getFlagStructure" returnType="struct" hint="returns flag Structure, loading if necessary" output="false">
		<cfargument name="flagID" type="string" required="true">

		<cfset var result = "">

		<cfif structKeyExists (application.flag,flagID)>
			<!--- if already in memory then return --->
			<cfset result = application.flag[flagID]>
			<cfset result.isOK = true>
		<cfelse>
			<!--- if not already in memory then try and load--->
			<cfset updateFlagStructure(flagID = flagID, updateCluster = false)>
			<cfif structKeyExists (application.flag,flagID)>
				<!--- if it is now in memory then return --->
				<cfset result = application.flag[flagID]>
				<cfset result.isOK = true>
			<cfelse>
				<!--- if it still not in memory then the flagGroup cannot exist
					we need to return an isOK = true in the other instances, maybe can be put automatically into the structure
				--->
				<cfset result = {isOK=false}>
			</cfif>
		</cfif>

		<cfreturn result>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Create a FlagGroup
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

<!---

WAB 2010/09/20  LID 4111  Alter so that name can be longer than the size of the field.
							The long name will end up in the translation

 --->

	<cffunction access="public" name="createFlagGroup" returnType="numeric"
		hint="Creates a flagGroup" output="no">

		<cfargument name="flagType" type="string" required="true">
		<cfargument name="Name" type="string" required="true">
		<cfargument name="NamePhraseTextID" type="string" default="">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="helpText" type="string" required="false" default="">
		<cfargument name="Description" type="string" required="false" default="">
		<cfargument name="flaggrouptextID" type="string" required="false" default="">
		<cfargument name="Notes" type="string" default="">
		<cfargument name="Parent" type="string" required="false" default=0>
		<cfargument name="order" type="numeric" required="false" default=0>
		<cfargument name="scope" type="numeric" required="false" default=0>
		<cfargument name="ViewingAccessRights" type="numeric" required="false" default=0>
		<cfargument name="EditAccessRights" type="numeric" required="false" default=0>
		<cfargument name="searchAccessRights" type="numeric" required="false" default=0>
		<cfargument name="downloadAccessRights" type="numeric" required="false" default=0>
		<cfargument name="view" type="numeric" required="false" default=1>
		<cfargument name="edit" type="numeric" required="false" default=1>
		<cfargument name="search" type="numeric" required="false" default=1>
		<cfargument name="download" type="numeric" required="false" default=1>
		<cfargument name="useinsearchscreen" type="numeric" required="false" default=1>
		<!--- <cfargument name="expiry" type="string" required="false" default="#DateAdd('yyyy', 10, Now ())#"> --->
		<cfargument name="active" type="numeric" required="false" default=1>
		<cfargument name="createdby" type="numeric" required="false" default="#request.relayCurrentUser.usergroupid#">
		<cfargument name="updatetime" type="string" required="false" default="#now()#">
		<cfargument name="createDefaultTranslationName" type="boolean" required="false" default="false">
		<cfargument name="createDefaultTranslationLanguage" type="numeric" required="false" default="1">
		<cfargument name="EntityMemberLive" type="numeric" required="false" default="0">
		<cfargument name="EntityMemberID" type="numeric" required="false" default="0">
		<cfargument name="FormattingParameters" type="string" required="false" default="">
		<cfargument name="updateMemoryStructure" type="boolean" required="false" default="true">  <!--- WAB/NJH 2010/02/02 LID 2828 to improve performance on bulk loading we do this at the very end --->
		<cfargument name="FlagTypeCurrency" type="string" required="false" default="">	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
		<cfargument name="FlagTypeDecimalPlaces" type="string" required="false" default="">	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
		<cfargument name="createdbyPerson" type="numeric" required="false" default="#request.relayCurrentUser.personID#">

		<cfset var flagTypeID = '' />
		<cfset var EntityTypeID = '' />
		<cfset var checkFlagGroup = '' />
		<cfset var getMaxOrder = '' />
		<cfset var AddFlagGroup = '' />

 		<cfif not isNumeric(flagType)>
			<cfset flagTypeID = application.flagTypeID[flagType]>
		<cfelse>
			<cfset flagTypeID = flagtype>
		</cfif>

		<cfif not isNumeric(EntityType)>
			<cfset EntityTypeID = application.EntityTypeID[EntityType]>
		<cfelse>
			<cfset EntityTypeID = Entitytype>
		</cfif>

		<!--- check if already exists --->
		<CFQUERY NAME="checkFlagGroup" DATASOURCE="#application.SiteDataSource#">
		 select flaggroupid from flaggroup
		 where
		 	(flaggrouptextid =  <cf_queryparam value="#flaggrouptextid#" CFSQLTYPE="CF_SQL_VARCHAR" >  and flaggrouptextid <> '')
				or
			(name =  <cf_queryparam value="#name#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and ParentFlagGroupID = <cfif parent is 0 or parent is "">0<cfelse>#getFlagGroupStructure(parent).flaggroupid#</cfif>
			and EntityTypeID =  <cf_queryparam value="#EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and flagTypeID =  <cf_queryparam value="#flagTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			)
		</cfquery>


		<cfif checkFlagGroup.recordcount is not 0>
			<cfset updateFlagGroupStructure(checkFlagGroup.flaggroupid)>
			<cfreturn checkFlagGroup.flaggroupid>

		<!--- NJH 2015/09/24 - check against the entity columns as well --->
		<cfelseif not isFlagGroupTextIDUnique(flagGroupTextID=arguments.flagGroupTextID,entityType=arguments.entityType)>
			<cfreturn 0>
		<cfelse>

			<!--- if ordering index is not set, the add at the end of the list --->
			<cfif order is 0>
				<CFQUERY NAME="getMaxOrder" DATASOURCE="#application.SiteDataSource#">
				select isnull(Max(orderingIndex),0) +1 as maxorder from flaggroup where parentFlagGroupid = <cfif parent is 0 or parent is "">0<cfelse>#getFlagGroupStructure(parent).flaggroupid#</cfif>
				</cfquery>
				<cfset arguments.order = getMaxOrder.maxOrder>
			</cfif>

			<CFQUERY NAME="AddFlagGroup" DATASOURCE="#application.SiteDataSource#">
			<!--- WAB 2010/09/20 LID 4111 truncate name to max length
				For bulk loading
			--->
			declare @lengthOfNamefield int
			select @lengthOfNamefield = character_maximum_Length from information_schema.columns where table_name = 'flagGroup' and column_name = 'name'

			INSERT INTO FlagGroup
			(ParentFlagGroupID,
			FlagTypeID,
			EntityTypeID,
			Name,
			NamePhrasetextID,
			helpText,
			Description,
			EntityMemberLive, EntityMemberID,
			FlagGroupTextID,
			Notes,
			OrderingIndex,
			Scope,
			ViewingAccessRights, EditAccessRights, SearchAccessRights, DownloadAccessRights,
			Viewing, Search, Edit, Download,
			useInSearchScreen,
			FormattingParameters,
			FlagTypeCurrency,	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
			FlagTypeDecimalPlaces,	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
			<!--- Expiry, ---> Active, CreatedBy, Created, LastUpdatedBy, LastUpdated,createdByPerson,lastUpdatedByPerson)
			VALUES
			(<cfif parent is 0 or parent is "" >0<cfelse><cf_queryparam value="#getFlagGroupStructure(parent).flaggroupid#" CFSQLTYPE="CF_SQL_Integer" ></cfif>,
			<cf_queryparam value="#FlagTypeID#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#EntityTypeID#" CFSQLTYPE="cf_sql_integer" >,
			left(<cf_queryparam value="#Replace(name,Chr(34),"`","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" >,@lengthOfNamefield),
			<cf_queryparam value="#NamePhrasetextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#arguments.helpText#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#Description#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#EntityMemberLive#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#EntityMemberID#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#replace(flaggrouptextID," ","","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#Notes#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam value="#order#" CFSQLTYPE="cf_sql_float" >,
			<cf_queryparam value="#Scope#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#ViewingAccessRights#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#EditAccessRights#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#SearchAccessRights#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#DownloadAccessRights#" CFSQLTYPE="CF_SQL_bit" >,
			<cf_queryparam value="#View#" CFSQLTYPE="cf_sql_float" >, <cf_queryparam value="#Search#" CFSQLTYPE="cf_sql_float" >, <cf_queryparam value="#Edit#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Download#" CFSQLTYPE="cf_sql_float" >,
			<cf_queryparam value="#UseInSearchScreen#" CFSQLTYPE="CF_SQL_bit" >,
			<cf_queryparam value="#FormattingParameters#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			<cf_queryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.FlagTypeCurrency#" maxlength="3" null="#!Len(arguments.FlagTypeCurrency)#">,	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
			<cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.FlagTypeDecimalPlaces#" null="#!Len(arguments.FlagTypeDecimalPlaces)#">,	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
			<!--- <cf_queryparam value="#CreateODBCDate(Expiry)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, --->
			<cf_queryparam value="#Active#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#createdby#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#updateTime#" CFSQLTYPE="cf_sql_timestamp" >,
			<cf_queryparam value="#createdby#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#updateTime#" CFSQLTYPE="cf_sql_timestamp" >,
			<cf_queryparam value="#arguments.createdByPerson#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#arguments.createdByPerson#" CFSQLTYPE="cf_sql_integer" >)

			select scope_identity() as FlagGroupID
			</CFQUERY>

			<cfif createDefaultTranslationName>
				<cfset createFlagGroupNameDefaultTranslation (flagGroupid = AddFlagGroup.flaggroupid, name = name, flaggroupTextID = flagGroupTextID, namePhrasetextID = namePhrasetextID, languageid = createDefaultTranslationLanguage)>
			</cfif>

			<!--- NJH 2010/02/02 LID2828 only update if we ask it to --->
			<cfif arguments.updateMemoryStructure>
				<cfset updateFlagGroupStructure(AddFlagGroup.flaggroupid)>
			</cfif>

			<cfreturn AddFlagGroup.flaggroupid>

		</cfif>

	</cffunction>


	<cffunction access="public" name="createFlag" returnType="numeric"
		hint="Creates a flag" output="yes">

		<!--- Wab 2008-04-15 added protected field to query --->

		<cfargument name="flagGroup" type="string" required="true" >
		<cfargument name="Name" type="string" required="true">
		<cfargument name="flagType" type="string" required="true"> <!--- used to confirm that is in the group expected --->
		<cfargument name="helpText" type="string" required="false" default="">
		<cfargument name="Description" type="string" required="false" default="">
		<cfargument name="flagtextID" type="string" required="false" default="">
		<cfargument name="namePhraseTextID" type="string" required="false" default="">
		<cfargument name="order" type="numeric" required="false" default=0>
		<cfargument name="score" type="numeric" required="false" default=0>
		<cfargument name="active" type="numeric" required="false" default=1>
		<cfargument name="usevalidvalues" type="numeric" required="false" default=0>
		<cfargument name="protected" type="numeric" required="false" default=0>
		<cfargument name="lookup" type="numeric" required="false" default=0>
		<cfargument name="linkstoentityType" type="string" required="false" default="">
		<cfargument name="wddx" type="string" required="false" default="">
		<cfargument name="formattingParameters" type="string" required="false" default="">
		<cfargument name="createdby" type="numeric" required="false" default="#request.relayCurrentUser.usergroupid#">
		<cfargument name="updatetime" type="string" required="false" default="#now()#">
		<cfargument name="createDefaultTranslationName" type="boolean" required="false" default="false">
		<cfargument name="createDefaultTranslationLanguage" type="numeric" required="false" default="1">
		<cfargument name="updateMemoryStructure" type="boolean" required="false" default="true">  <!--- WAB/NJH 2010/02/02 LID 2828 to improve performance on bulk loading we do this at the very end --->
		<cfargument name="ShowInConditionalEmails" type="boolean" required="false" default="false">  <!--- 2011/06/14 PPB REL106 - add showInConditionalEmails --->
		<cfargument name="createdbyPerson" type="numeric" required="false" default="#request.relayCurrentUser.personID#">


		<cfset var getFlagGroup = '' />
		<cfset var flagGroupID = '' />
		<cfset var flagTypeID = '' />
		<cfset var linkstoEntityTypeID = '' />
		<cfset var checkFlag = '' />
		<cfset var getMaxOrder = '' />
		<cfset var AddFlag = '' />

		<cfif not  doesFlagGroupExist(#flagGroup#)>
			<cfoutput>Invalid Flag Group</cfoutput>
			<cfreturn 0>
		<cfelse >
			<cfset getFlagGroup = getFlagGroupStructure(flagGroup)>
			<cfset flagGroupID = getFlagGroup.FlagGroupID>
		</cfif>



		<!--- check flagType passed against the type of the group --->
		<cfif not isNumeric(flagType)>
			<cfset flagTypeID = application.flagTypeID[flagType]>
		<cfelse>
			<cfset flagTypeID = flagtype>
		</cfif>
		<cfif flagTypeID is not getFlagGroup.flagTypeID>
			<!--- LID 2616 NJH 2009/09/21 - added .name to flagType structure --->
			<cfoutput>Flag Group #getFlagGroup.name# is not of type #application.flagType[flagTypeID].name# </cfoutput>
			<cfreturn 0>
		</cfif>

		<cfif not isNumeric(arguments.ShowInConditionalEmails)>
			<cfif arguments.ShowInConditionalEmails>
				<cfset arguments.ShowInConditionalEmails = 1>
			<cfelse>
				<cfset arguments.ShowInConditionalEmails = 0>
			</cfif>
		</cfif>

		<!---  convert linkstoentityType to numeric--->
		<cfif linkstoEntityType is "" or linkstoEntityType is "null">
			<cfset linkstoEntityTypeID = "null">
		<cfelseif not isNumeric(linkstoEntityType)>
			<cfset linkstoEntityTypeID  = application.EntityTypeID[linkstoEntityType]>
		<cfelse>
			<cfset linkstoEntityTypeID  = linkstoEntityType>
		</cfif>

		<cfset arguments.Name = Replace(arguments.Name,Chr(34),"`","ALL")>

		<!--- JIRA PROD2015-589  - NJH 2016/02/16 - set flagText if not set. This assumes/hopes that the flagGroupTextID has been set. Perhaps this assumption needs to be handled a bit better--->
		<cfif arguments.flagTextID eq "">
			<cfset var prefix=getFlagGroup.flagGroupTextID&"_">
			<cfset arguments.flagTextId = application.com.regExp.makeSafeTextID(inputString=replace(prefix&arguments.name," ","_","ALL"))>
			<!---BF-570 RJT flag text ID just _ when using kanji characters etc --->
			<cfif arguments.flagTextId eq prefix>
				<cfloop index="local.i" from="1" to="5">
			        <!--- Random character in range A-Z --->
			        <cfset arguments.flagTextId=arguments.flagTextId & Chr(RandRange(65, 90)) >
			    </cfloop>
			</cfif>
			<!--- If the flag text ID isn't unique (it almost certainly is) keep appending characters at random' --->
			<cfscript>
				while (isFlagTextIDUnique(flagTextId=arguments.flagTextId,entityType=getFlagGroup.entityType.tablename,flagTypeID=flagTypeID) EQ False) {
					arguments.flagTextId=arguments.flagTextId & Chr(RandRange(65, 90));
				}
			</cfscript>


		</cfif>

		<!--- check if already exists --->
		<CFQUERY NAME="checkFlag" DATASOURCE="#application.SiteDataSource#">
		 select flagid from flag
		 where
		 	(flagtextid =  <cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" >  and flagtextid <> '')
				or
			(name =  <cf_queryparam value="#name#" CFSQLTYPE="CF_SQL_VARCHAR" >
			and FlagGroupID =  <cf_queryparam value="#FlagGroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
			)
		</cfquery>


		<cfif checkFlag.recordcount is not 0>

			<cfset updateFlagStructure(checkFlag.flagid)>

			<cfreturn checkFlag.flagid>

		<cfelse>



			<!--- if ordering index is not set, the add at the end of the list --->
			<cfif order is 0>
				<CFQUERY NAME="getMaxOrder" DATASOURCE="#application.SiteDataSource#">
				select isNUll(Max(orderingIndex),0) +1 as maxorder from flag where FlagGroupid =  <cf_queryparam value="#flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<cfset arguments.order = getMaxOrder.maxOrder>
			</cfif>


			<!--- Add new flag --->
			<!--- Wab 2009/02/25 added a left to the name and description--->
			<!--- 2011/06/14 PPB REL106 - add showInConditionalEmails --->
			<CFQUERY NAME="AddFlag" DATASOURCE="#application.SiteDataSource#">
				<!--- WAB 2010/09/20 LID 4111 truncate name to max length
					For bulk loading
				--->
				declare @lengthOfNamefield int
				select @lengthOfNamefield = character_maximum_Length from information_schema.columns where table_name = 'flag' and column_name = 'name'


				INSERT INTO Flag
				(FlagGroupID, Name, Description, helpText,
				FlagTextID, namePhraseTextID, OrderingIndex, score,
				Active, usevalidvalues, protected, lookup, WDDXStruct, formattingParameters,
				linkstoentityTypeID,ShowInConditionalEmails,
				CreatedBy, Created, LastUpdatedBy, LastUpdated,createdByPerson,lastUpdatedByPerson)
				VALUES
				(<cf_queryparam value="#FlagGroupID#" CFSQLTYPE="cf_sql_integer" >,
				left(<cf_queryparam value="#Replace(Name,Chr(34),"`","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" >,@lengthOfNameField), <cf_queryparam value="#left(Description,60)#" CFSQLTYPE="CF_SQL_VARCHAR" >,  <cf_queryparam value="#left(helpText,100)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#FlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<cf_queryparam value="#namePhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#Order#" CFSQLTYPE="cf_sql_float" >, <cf_queryparam value="#Score#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#Active#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#usevalidValues#" CFSQLTYPE="CF_SQL_bit" >,
				<cf_queryparam value="#protected#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#lookup#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#WDDX#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#formattingParameters#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#linkstoentityTypeID#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#arguments.ShowInConditionalEmails#" CFSQLTYPE="CF_SQL_bit" >,
				<cf_queryparam value="#createdby#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#updatetime#" CFSQLTYPE="cf_sql_timestamp" >, <cf_queryparam value="#createdby#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#updatetime#" CFSQLTYPE="cf_sql_timestamp" >,
				<cf_queryparam value="#arguments.createdByPerson#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#arguments.createdByPerson#" CFSQLTYPE="cf_sql_integer" >)

				select scope_identity() as FlagID
			</CFQUERY>

			<cfif createDefaultTranslationName>
				<cfset createFlagNameDefaultTranslation (flagid = AddFlag.flagid, name = name, flagTextID = flagTextID, namePhrasetextID = namePhrasetextID, languageid = createDefaultTranslationLanguage)>
			</cfif>

			<!--- NJH 2010/02/02 LID2828 only update if we ask it to --->
			<cfif arguments.updateMemoryStructure>
				<cfset updateFlagStructure(AddFlag.flagid)>
			</cfif>

			<cfreturn AddFlag.flagid>

		</cfif>

	</cffunction>


	<cffunction access="public" name="createFlagNameDefaultTranslation" >
		<cfargument name="flagID" type="string" required="true" >
		<cfargument name="Name" type="string" required="true">
		<cfargument name="flagtextID" type="string" required="false" default="">
		<cfargument name="namePhraseTextID" type="string" required="false" default="">
		<cfargument name="Languageid" type="numeric" required="false" default="1">
		<cfargument name="countryid" type="numeric" required="false" default="0">

			<cfset var modifiedNamePhrasetextID = NamePhrasetextID>
			<cfset var updateFlag = '' />

			<!--- WAB 2009/09/15 LID 2572 problem if phrasetextID ended up in form xxx_yyy_123 because looks like an entity translation and everything gets confused.  So if it matches this pattern then remove the _s--->
			<cfif NamePhraseTextID is "">
				<cfset modifiedNamePhrasetextID = "flag_"&iif(FlagTextID is "",de(flagID),de(FlagTextID))>
			</cfif>

				<cfset modifiedNamePhrasetextID = application.com.relayTranslations.createValidUniquePhraseTextID(modifiedNamePhrasetextID)>

				<cfif modifiedNamePhrasetextID is not NamePhrasetextID>

				<CFQUERY NAME="updateFlag" DATASOURCE="#application.SiteDataSource#">
					update flag set namephraseTextID = '#modifiedNamePhrasetextID#' where flagid = #flagID#
				</CFQUERY>

			</cfif>

			<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID =modifiedNamePhrasetextID, phrasetext= name, languageid = Languageid, countryid = countryid )>

			<cfreturn  modifiedNamePhraseTextID >

	</cffunction>


	<cffunction access="public" name="createFlagGroupNameDefaultTranslation" >
		<cfargument name="flagGroupID" type="string" required="true" >
		<cfargument name="Name" type="string" required="true">
		<cfargument name="flagGroupTextID" type="string" required="false" default="">
		<cfargument name="namePhraseTextID" type="string" required="false" default="">
		<cfargument name="Languageid" type="numeric" required="false" default="1">
		<cfargument name="countryid" type="numeric" required="false" default="0">

			<cfset var modifiedNamePhraseTextID = NamePhrasetextID>
			<cfset var updateFlagGroup = '' />

			<!--- WAB 2009/09/15 LID 2572  problem if phrasetextID ended up in form xxx_yyy_123 because looks like an entity translation and everything gets confused.  So if it matches this pattern then remove the _s--->
			<cfif NamePhraseTextID is "">
				<!--- WAB 2009/06/08 LID 2237, was defaulting to flag_ rather than flagGroup_  --->
				<cfset modifiedNamePhraseTextID = "flagGroup_"&iif(FlagGroupTextID is  "",de(flagGroupID),de(FlagGroupTextID))>

			</cfif>

				<cfset modifiedNamePhraseTextID = application.com.relayTranslations.createValidUniquePhraseTextID(modifiedNamePhraseTextID)>

				<cfif modifiedNamePhraseTextID is not NamePhrasetextID>
					<CFQUERY NAME="updateFlagGroup" DATASOURCE="#application.SiteDataSource#">
					update flagGroup set namephraseTextID = '#modifiedNamePhraseTextID#' where flagGroupid = #flagGroupID#
				</CFQUERY>
			</cfif>

			<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(phraseTextID =modifiedNamePhraseTextID, phrasetext= name, languageid = languageid, countryid = countryid )>
			<cfreturn  modifiedNamePhraseTextID >

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Sets a boolean flag
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setBooleanFlag" hint="Sets a boolean type flag on the system" output="no">

		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="flagTextID" required="yes" type="string">
		<cfargument name="deleteOtherRadiosInGroup" default="false">
		<cfargument name="useRequestTime" default="false"> <!--- NJH 2008/06/02 needed to mark flags as being in a batch --->
		<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
		<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">



		<cfscript>
			var setBooleanFlag = "";
			var snippet_sql= "";
			var flagStructure = getFlagStructure(flagTextID) ;
			var dateSQL = "getDate()";
		</cfscript>

		<cfif useRequestTime>
			<cfset dateSQL = "#request.requestTimeODBC#">
		</cfif>

		<cfif flagStructure.isOk>
			<CFQUERY NAME="setBooleanFlag" DATASOURCE="#application.SiteDataSource#">
				<!---
				WAB 2008/01/23 added code to delete existing radio values in a radio group if the deleteOtherRadiosInGroup argument is passed
				NJH case 433498	 - added lastupdatedByPerson
				 --->

				<cfif flagStructure.flagType.name is "radio" and deleteOtherRadiosInGroup>
					<!--- need to do an update before the delete, use this snippet to make sure I use the same query --->
					<cfset snippet_sql = "
						from
						booleanflagData bfd
							inner join
						flag f on f.flagid = bfd.flagid
					where
						f.flagGroupID =  #application.com.security.queryparam(value="#flagStructure.flagGroupID#",CFSQLTYPE="CF_SQL_INTEGER" )#
						and f.flagid <>  #application.com.security.queryparam(value="#flagStructure.flagID#",CFSQLTYPE="CF_SQL_INTEGER" )#
						and entityID = #application.com.security.queryparam(value="#arguments.entityID#",CFSQLTYPE="CF_SQL_INTEGER" )#
					">

					update booleanflagdata
						set lastupdated =  <cf_queryparam value="#dateSQL#" CFSQLTYPE="CF_SQL_timestamp" > ,
							lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >

					#snippet_sql#

					delete 	booleanflagdata
					#snippet_sql#
				</cfif>
				INSERT INTO [BooleanFlagData]([EntityID], [FlagID], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated], [LastUpdatedByPerson])
				select  <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						flagID,
						<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#dateSQL#" CFSQLTYPE="CF_SQL_timestamp" >,
						<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#dateSQL#" CFSQLTYPE="CF_SQL_timestamp" >,
						<cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				FROM flag
				where flag.flagid =  <cf_queryparam value="#flagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND not exists (select * from BooleanFlagData bfd
					INNER JOIN flag f ON bfd.flagID = f.flagID
					WHERE entityID = <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND f.flagid =  <cf_queryparam value="#flagStructure.flagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</cfquery>
		</cfif>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        UnSets a boolean flag
		2005-07-17 SWJ: significantly upgraded this function so that it now is quite useful
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="unsetBooleanFlag" hint="unSets a boolean type flag on the system.  FlagID can be flagTextID." output="no">

		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="flagID" required="yes" type="string">
		<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
		<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">

		<cfscript>
			var unsetBooleanFlag = "";
			var flag = getFlagStructure(arguments.flagID);
			var FlagTest = flag.flagType.name;
		</cfscript>

		<cfswitch expression="#FlagTest#">

			<cfcase value="checkBox">
				<!--- WAB 2006-09-25 added the update of lastupdated- this means that the mod register records the change properly
						NJH case 433498	 - added lastupdatedByPerson
				--->
				<CFQUERY NAME="unsetBooleanFlag" DATASOURCE="#application.SiteDataSource#">
					update booleanflagdata
					set
						lastupdated = getdate(),
						lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					where flagid =  <cf_queryparam value="#flag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityid = <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >

					delete from booleanflagdata
					where flagid =  <cf_queryparam value="#flag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityid = #entityid#
				</cfquery>
				<cfreturn "checkbox">
			</cfcase>

			<!--- SWJ: I know this is not a boolean flag, but it works any way! --->
			<cfcase value="text">
				<CFQUERY NAME="unsetBooleanFlag" DATASOURCE="#application.SiteDataSource#">
					update textflagdata set lastupdated = getdate(), lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >, lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					where flagid =  <cf_queryparam value="#flag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityid = <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >

					delete from textFlagData
					where flagid =  <cf_queryparam value="#flag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and entityid = <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<cfreturn "text">
			</cfcase>

			<cfcase value="radio">
				<CFQUERY NAME="unsetBooleanFlag" DATASOURCE="#application.SiteDataSource#">
					update booleanflagdata
					set
						lastupdated = getdate(),
						lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					where
						entityid = <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and flagID in (
						Select flagID from flag f
						where f.flagGroupID =  <cf_queryparam value="#flag.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
						)

					delete from booleanflagdata
					where entityID = <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
					and flagID in (
						Select flagID from flag f
						where f.flagGroupID =  <cf_queryparam value="#flag.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
						)
				</cfquery>
				<cfreturn "radio">
			</cfcase>

		</cfswitch>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Sets a boolean flag based on flagID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="setBooleanFlagByID" hint="Sets a boolean type flag on the system" output="no">

		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="flagID" required="yes" type="numeric">
		<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
		<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">



		<cfscript>
			var setBooleanFlagByID = "";
		</cfscript>

		<!--- NJH case 433498	 - added lastupdatedByPerson --->
		<CFQUERY NAME="setBooleanFlagByID" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO [BooleanFlagData]([EntityID], [FlagID], [CreatedBy], [Created], [LastUpdatedBy], [LastUpdated],[LastUpdatedByPerson])
			select 	<cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					flagID,
					<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					getDate(),
					<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					getdate(),
					<cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
			FROM flag
			where flagID = <cf_queryparam value="#arguments.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND not exists (select * from BooleanFlagData bfd
				INNER JOIN flag f ON bfd.flagID = f.flagID
				WHERE entityID = <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				AND f.flagID = <cf_queryparam value="#arguments.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >)
		</cfquery>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Checks a boolean flag based on flagID
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="checkBooleanFlagByID" access="public" returntype="boolean" hint="Checks if a boolean flagID is set or not for entityID">

		<cfargument name="entityID" required="yes" type="numeric">
		<cfargument name="flagID" required="yes" type="string">


		<cfscript>
			var qCheckBooleanFlagByID = "";
		</cfscript>

		<CFQUERY NAME="qCheckBooleanFlagByID" DATASOURCE="#application.SiteDataSource#">
				select entityID
					from
						flag f
							inner join
						BooleanFlagData bfd
							on f.flagid = bfd.flagid
				WHERE entityID = #arguments.entityID#
				AND
				<cfif isNumeric(arguments.flagID)>
					f.flagID =  <cf_queryparam value="#arguments.flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
					f.flagtextID =  <cf_queryparam value="#arguments.flagID#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
		</cfquery>

		<cfif qCheckBooleanFlagByID.recordCount eq 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>

	</cffunction>



<!---
This function checks boolean flags for the current user.
Doesn't matter whether the flag is person,location or organisation
 --->
	<cffunction name = "isFlagSetForCurrentUser" output=NO>
		<cfargument name="flagID" required="yes" type="string">

		<cfreturn isFlagSetForPerson (flagid = flagid, personid = request.relaycurrentuser.personid)>

	</cffunction>


	<cffunction name = "getFlagDataForCurrentUser" output=NO>
		<cfargument name="flagID" required="yes" type="string">

		<cfreturn getFlagDataForPerson (flagid = flagid, personid = request.relaycurrentuser.personid,argumentCollection = arguments)>
	</cffunction>


	<cffunction name="getFlagDataForPerson" returnType="query" output="no">
		<cfargument name="flagID" required="yes" type="string">
		<cfargument name="PersonID" required="yes" type="string">

		<cfset var flag  = getFlagStructure(flagid)>
		<cfset var theEntityID = '' />
		<cfset var person = '' />

		<cfif listfind("0,1,2",flag.entityType.entitytypeid) is 0>
			<cfoutput>#flag.name# not POL</cfoutput><cfreturn false>
		</cfif>

		<cfif structKeyExists(request,"relaycurrentuser") and structKeyExists(request.relaycurrentuser,"personid") and  personid is request.relaycurrentuser.personid>
			<cfset theEntityID = request.relaycurrentuser[flag.entityType.uniquekey]>
		<cfelse>
			<cfquery name = "person" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
			select personid, locationid, organisationid from person where personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset theEntityID = person[flag.entityType.uniquekey][1]>
		</cfif>

		<cfreturn getFlagData (entityID = theEntityID,argumentCollection = arguments)>

	</cffunction>


	<cffunction name = "getFlagGroupDataForCurrentUser" output=NO>
		<cfargument name="flagGroupID" required="yes" type="string">
		<cfargument name="returnNulls" type="boolean" default="false">

		<cfreturn getFlagGroupDataForPerson (argumentCollection=arguments, personid = request.relaycurrentuser.personid)>

	</cffunction>

	<cffunction name="getFlagGroupDataForPerson" output="no">
		<cfargument name="flaggroupID" required="yes" type="string">
		<cfargument name="PersonID" required="yes" type="string">

		<cfset var flaggroup  = getFlagGroupStructure(flaggroupid)>
		<cfset var theEntityID = 0>
		<cfset var person = "">

		<cfif flaggroup.isOK is 0>
			<cfoutput>#flaggroupid# does not exist</cfoutput><cfreturn false>
		</cfif>

		<cfif listfind("0,1,2",flaggroup.entityType.entitytypeid) is 0>
			<cfoutput>#flaggroup.name# not POL</cfoutput><cfreturn false>
		</cfif>

		<cfif structKeyExists(request,"relaycurrentuser") and structKeyExists(request.relaycurrentuser,"personid") and  personid is request.relaycurrentuser.personid>
			<cfset theEntityID = request.relaycurrentuser[flaggroup.entityType.uniquekey]>
		<cfelseif IsNumeric(personid) AND personid NEQ 0 > <!--- 2014-10-21 AHL Case 442156 Unable to access: Opportunity Invalid data ' for CFSQLTYPE CF_SQL_INTEGER. --->
			<cfquery name = "person" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
			select personid, locationid, organisationid from person where personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfif person.recordCount>
				<cfset theEntityID = person[flaggroup.entityType.uniquekey][1]>
			</cfif>
		</cfif>

		<cfreturn getFlagGroupData (entityID = theEntityID,argumentCollection = arguments)>

	</cffunction>


	<cffunction name="setFlagDataForPerson" output="no">
		<cfargument name="flagID" required="yes" type="string">
		<cfargument name="PersonID" required="yes" type="string">


		<cfset var flag  = getFlagStructure(flagid)>
		<cfset var theEntityID = '' />
		<cfset var person = '' />

		<cfif listfind("0,1,2",flag.entityType.entitytypeid) is 0>
			<cfoutput>#flag.name# not POL</cfoutput><cfreturn false>
		</cfif>

		<cfif structKeyExists(request,"relaycurrentuser") and structKeyExists(request.relaycurrentuser,"personid") and  personid is request.relaycurrentuser.personid>
			<cfset theEntityID = request.relaycurrentuser[flag.entityType.uniquekey]>
		<cfelse>
			<cfquery name = "person" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
			select personid, locationid, organisationid from person where personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset theEntityID = person[flag.entityType.uniquekey][1]>
		</cfif>

		<cfreturn setFlagData (entityID = theEntityID,argumentCollection = arguments)>

	</cffunction>


<!---
This function checks boolean flags for any personid.
Doesn't matter whether the flag is person,location or organisation

WAB 2007-02-05 added ability to handle integer flags which contain organisation, location or personids.  tests whether the stored id matches the given person
	for other integer flags it just returns the data (in quotes).
	This does mean that the name of the function is no longer quite right and it can return stuff other than boolean

 --->

 	<cffunction name = "isFlagSetForPerson"  output=no>
		<cfargument name="flagID" required="yes" type="string">
		<cfargument name="PersonID" required="yes" type="string">

		<!--- START: NYB 2009-10-02 LHID2716 - added cftry/catch to catch flags that don't exist
			WAB 2010-09-15 changed to use getflagstructure and isOK
		--->
		<cfset var theEntityID = '' />
		<cfset var LinkedEntityID = '' />
		<cfset var flagData = '' />
		<cfset var person = '' />
		<cfset var flag  = getFlagStructure(flagid)>

		<cfif not flag.isOK>
			<cfreturn false>
		</cfif>

		<cfif listfind("0,1,2",flag.entityType.entitytypeid) is 0>
			<cfoutput>#flag.name# not POL</cfoutput>
			<cfreturn false>
		</cfif>

		<!--- 2006/11/10 - GCC - BUG??? request.relaycurrentuser is not defined at this point when a security profile is applied to a remember me login --->
		<cfif structKeyExists(request,"relaycurrentuser") and structKeyExists(request.relaycurrentuser,"personid") and  personid is request.relaycurrentuser.personid>
			<cfset theEntityID = request.relaycurrentuser[flag.entityType.uniquekey]>
			<cfif flag.linksToEntityTypeID is not "">
				<cfset LinkedEntityID = request.relaycurrentuser[application.entityType[flag.linksToEntityTypeID].uniquekey]>s
			</cfif>
		<cfelse>
			<cfquery name = "person" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
			select personid, locationid, organisationid from person where personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfif person.recordcount eq 0>
				<cfoutput>#personID# does not exist</cfoutput>
				<cfreturn false>
			</cfif>

			<cfset theEntityID = person[flag.entityType.uniquekey][1]>
			<cfif flag.linksToEntityTypeID is not "">
				<cfset LinkedEntityID = person[application.entityType[flag.linksToEntityTypeID].uniquekey][1]>
			</cfif>
		</cfif>

		<cfif listfind("6",flag.flagType.flagtypeid) >  <!--- integer --->
				<cfset flagData = application.com.flag.getFlagData(flagid = flagid,entityid = theEntityID)>
				<cfif flag.linksToEntityTypeID is not "">
					<cfif flagData.data is LinkedEntityID>
						<cfreturn true>
					<cfelse>
						<cfreturn false>
					</cfif>
				<cfelse>
						<cfreturn "'" & flagData.data & "'" >    <!--- need to return in quotes to allow for CF evaluation  --->
				</cfif>
		<cfelseif listfind("2,3",flag.flagType.flagtypeid) >
			<cfreturn application.com.flag.checkBooleanFlagByID(flagid = flagid,entityid = theEntityID)>
		<cfelse>
			<cfoutput>#flag.name# is not of correct type</cfoutput><cfreturn false>
		</cfif>

	</cffunction>


<!---
function which takes an expression made up of names of flags and evaluates it (ie returns true or false) for a given person
The expression might look like:
	"(perApproved and orgApproved) OR (IncentiveUser)"

	The expression can include  brackets, AND, OR, NOT, comma (which is interpreted as AND for backwards compatability), ! , | (pipe single or double == OR), & (single or double == AND)

	(went a bit overboard allowing !, | and &)
	WAB 2007-02-05
	can now deal with integer flags
		either 	integer flags where the data is an entityid - automatically check whether this entityid refers to the personid passed in
		or 		other integer flags can do CF expressions   eg:   NumberOfEmployees GT '100'    [note that the 100 is in quotes]


	WAB 2009/02/03	altered so that words TRUE and FALSE are understood and not taken as flagNames
 --->

 	<cffunction name = "evaluateFlagExpression" return="boolean" output=no>
		<cfargument name="expression" required="yes" type="string">
		<cfargument name="personid" required="yes" type="numeric">
			<cfset var item = "">
			<cfset var re = "">
			<cfset var thisFlagResult = "">
			<cfset var flagsDone = "">   <!--- just used to prevent checking a flag more than once if used more than one in the expression--->
			<cfset var resultExpression = expression>
			<cfset var operators = "AND,OR,NOT,IS,GT,LT,GTE,LTE,TRUE,FALSE">
			<cfset var bracketsRe= "AND,OR,NOT,IS,GT,LT,GTE,LTE">

			<cfloop index = "item" list= #expression# delimiters="&(|,) !">
				<cfif refindNoCase("\A['""].*['""]\Z",item) is not 0>
					<!--- this re looks for items which are in quotes (single or double)
						- these are constants rather than flags and can be ignored
					--->
				<cfelseif item contains "user.">
					<cfif personid is request.relaycurrentuser.personid>
						<cfset resultExpression = ReplaceNoCase(resultExpression,"user.","request.relayCurrentUser.","ONE")>
					<cfelse>
						<cfoutput>This function currently only supported for currentUser</cfoutput><CF_ABORT>
					</cfif>

				<cfelseif listfindnocase (operators,item) is 0 and listfindNoCase(flagsDone,item) is 0 >
					<cfset flagsDone = listAppend (flagsDone,item)>
					<cfset re = "(\A|[&\(\)\|, !])(#item#)([&\(\)\|, !]|\Z)">
					<cfset thisFlagResult = isFlagSetForPerson(flagid = item, personid = #personid#)>
					<cfset resultExpression = reReplaceNoCase(resultExpression,re,"\1#thisFlagResult#\3","ALL")>
				</cfif>

			</cfloop>

			<cfset resultExpression = replaceNoCase(resultExpression,","," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression,"&&"," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression,"&"," AND ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "||" ," OR ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "|" ," OR ","ALL")>
			<cfset resultExpression = replaceNoCase(resultExpression, "!" ," NOT  ","ALL")>
		<!--- <cfoutput>resultExpression=[#evaluate(resultExpression)#]</cfoutput><CF_ABORT> --->
		<cfreturn evaluate(resultExpression)>

	</cffunction>




	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          Gets structure of items set in a radio or checkbox group
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getBooleanFlagList" access="public" returntype="struct" hint="Brings back structure containing lists of items set in either a radio or checkbox group , flagids, flagtextids and names" output="no">
		<!--- 2005-03-31 WAb added return variable of the whole query with the flagdata in it --->
		<cfargument name="entityID" required="yes" type="string">
		<cfargument name="flagGroupID" required="yes" type="string">

		<cfset var returnStruct = structNew()>
		<cfset var getFlags = '' />

		<cfquery NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
			SELECT f.name, f.flagtextid, f.flagid, fd.* FROM booleanflagdata fd
				INNER JOIN flag f ON f.flagid = fd.flagid AND f.flaggroupid =  <cf_queryparam value="#getFlagGroupstructure(flaggroupid).flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
				WHERE fd.entityID =  <cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				ORDER BY f.orderingIndex
		</cfquery>


		<cfset returnStruct.flagIDs = valuelist(getFlags.flagid)>
		<cfset returnStruct.flagTextIDs = valuelist(getFlags.flagTextid)>
		<cfset returnStruct.Names = valuelist(getFlags.name)>
		<cfset returnStruct.data = getFlags>


		<cfreturn returnStruct>

	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Functions to manipulate flag content.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
<cffunction name="setFlagData">
	<cfargument name="flagId" required="Yes">
	<cfargument name="entityID" required="Yes" > <!--- type="numeric" can now be a list of entityids --->
	<cfargument name="data" required="no">
	<cfargument name="sortorder" required="no" default="0" type="numeric">
	<cfargument name="returnNumberNotAffected" default="false">
	<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
	<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">


	<cfset var checkForValueQry = "">
	<cfset var updateValueQry = "">
	<cfset var insertValueQry = "">

	<!--- 2006-03-21 WAB modified to deal with tables other than integer multi.  Not tested --->

	<cfset var theFlag = getFlagStructure(arguments.flagid)>
	<cfset var result = structNew()>

	<cfif theflag.flagType.hasDataColumn>
		<cfif not structKeyExists(arguments,"data")>
			<cfoutput>Function SetFlagData - Data Value not supplied</cfoutput><CF_ABORT>
		<!--- NJH 2015/02/25 Jira Fifteen-223 treat an empty data, where data is expected, as a delete --->
		<cfelseif arguments.data eq "">
			<cfreturn deleteFlagData(argumentCollection=arguments)>
		</cfif>
	</cfif>


	<cfquery NAME="updateValueQry" DATASOURCE="#application.SiteDataSource#">
		declare @numberUpdated int, @numberInserted int, @numberNotChanged int
		select @numberUpdated = 0, @numberInserted = 0 , @numberNotChanged  = 0


		<cfif returnNumberNotAffected>
				select @numberNotChanged = count(1)
				from
					<cf_queryobjectname value="#theFlag.flagGroup.entityType.TABLENAME#"> e
						inner join
					<cf_queryobjectname value="#theFlag.flagType.datatablefullname#"> fd on <cf_queryobjectname value="e.#theFlag.flagGroup.entityType.uniqueKey#"> = fd.entityid and fd.flagid =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
				where
					<cf_queryobjectname value="#theFlag.flagGroup.entityType.uniqueKey#">  in ( <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
					<cfif theflag.flagType.hasSortIndexColumn >
						AND data <>  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelseif theflag.flagType.hasDataColumn>
						AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >  COLLATE SQL_Latin1_General_Cp1_CS_AS
					</cfif>
		</cfif>

		<!--- P-FNL079 NJH 2009/10/13 - added fd. to the flagID column as flagID was ambiguous when the tablename was opportunity
				NJH case 433498	 - added lastupdatedByPerson
		--->
		<cfif theflag.flagType.hasDataColumn > <!--- flags without a data column (ie booleans, don't have an update) --->
				update <cf_queryobjectname value="#theFlag.flagType.datatablefullname#">
				set
				<cfif theflag.flagType.hasSortIndexColumn>sortorder = #arguments.sortorder#,	</cfif>
				<!--- NAS 2010/09/29 LID4188: added an if to cater for Date data on SetFlagData --->
				data = <cf_queryparam value="#arguments.data#" CFSQLTYPE="#theflag.flagType.cfsqlType#" >,
				<cfif structKeyExists(arguments,"currencyISOCode") and arguments.currencyISOCode neq "">currencyISOCode= <cf_queryparam value="#arguments.currencyISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif><!--- YMA 2015/07/06 Support for saving financial flag data --->
				lastupdatedBy = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				lastupdated = getDate(),
				lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				from
					#theFlag.flagGroup.entityType.TABLENAME# e
						INNER join
					#theFlag.flagType.datatablefullname# fd on e.#theFlag.flagGroup.entityType.uniqueKey#= fd.entityid and fd.flagid =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >  <cfif theflag.flagType.hasSortIndexColumn >AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >  </cfif>
				where fd.flagID =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >  AND
					#theFlag.flagGroup.entityType.uniqueKey#  in ( <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> )
					<cfif theflag.flagType.hasSortIndexColumn>
						AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelse>
					<!--- NAS 2010/09/29 LID4188: added an if to cater for Date data on SetFlagData --->
						AND data <> <cf_queryparam value="#arguments.data#" CFSQLTYPE="#theflag.flagType.cfsqlType#" >  <!--- only update if data has changed --->
					</cfif>

				select @numberUpdated = @@rowcount
		</cfif>

		<cfif theflag.flagType.name is "radio">
			<!--- any other radio buttons in the group can be updated to be this radio button,
			(otherwise would have to do delete followed by insert)
			but need to change the created date and by (added WAB 2007/01/23
			NJH case 433498	 - added lastupdatedByPerson
			--->

			update booleanflagdata
			set
				flagid =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				lastupdatedBy =<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				lastupdated = getDate(),
				createdBy =#request.relayCurrentUser.usergroupid#,
				created = getDate(),
				lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >

			from booleanflagdata bfd1
				inner join flag f1	on bfd1.flagid = f1.flagid
				inner join flag f2  on f1.flaggroupid = f2.flaggroupid and f1.flagid <> f2.flagid
			where bfd1.entityid  in ( <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement="true"> ) and f2.flagid =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >

			select @numberUpdated = @@rowcount

		</cfif>

				INSERT INTO
				#theFlag.flagType.datatablefullname# (
					flagId,
					entityID,
					<cfif theflag.flagType.hasDataColumn >data,</cfif>
					<cfif theflag.flagType.hasSortIndexColumn>sortorder,</cfif>
					<cfif structKeyExists(arguments,"currencyISOCode")>currencyISOCode,</cfif><!--- YMA 2015/07/06 Support for saving financial flag data --->
					createdby,
					created,
					lastupdatedby,
					lastupdated,
					lastUpdatedByPerson)
				select
					<cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryobjectname value="#theFlag.flagGroup.entityType.uniqueKey#"  >,
					<cfif theflag.flagType.hasDataColumn ><cf_queryparam value="#arguments.data#" CFSQLTYPE="#theflag.flagType.cfsqlType#" >,</cfif>
					<cfif theflag.flagType.hasSortIndexColumn><cf_queryparam value="#arguments.sortorder#" CFSQLTYPE="cf_sql_integer" >,</cfif>
					<cfif structKeyExists(arguments,"currencyISOCode")><cf_queryparam value="#arguments.currencyISOCode#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif><!--- YMA 2015/07/06 Support for saving financial flag data --->
					<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					getDate(),
					<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					getDate(),
					<cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >

				from
					#theFlag.flagGroup.entityType.TABLENAME# e
						left join
					#theFlag.flagType.datatablefullname# fd on e.#theFlag.flagGroup.entityType.uniqueKey#= fd.entityid and fd.flagid =  <cf_queryparam value="#theFlag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >  <cfif theflag.flagType.hasSortIndexColumn >AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >  </cfif>
				where
					<!--- WAB 2016-02-02 changed this back to a cf_queryparam (from cfqueryparam) with an allowSelectStatement. (used by seltFlagForIDList) Don't know why removed, but was in one of my commits! --->
					#theFlag.flagGroup.entityType.uniqueKey#  in ( <cf_queryparam value="#arguments.entityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true" allowSelectStatement=true> )
					and fd.flagid is null

					select @numberInserted = @@rowcount

				select @numberUpdated as numberUpdated ,@numberInserted as numberInserted, @numberNotChanged as numberNotChanged
	</cfquery>

	<cfset result.numberupdated = updateValueQry.numberupdated >
	<cfset result.numberInserted = updateValueQry.numberInserted >
	<cfif returnNumberNotAffected>
		<cfset result.numberNotChanged = updateValueQry.numberNotChanged >
	</cfif>

	<cfreturn result>


 </cffunction>




<!---
	This function returns the set of ids of entities
	with a particular flag set
	- really designed for boolean flag data, but would work with other flags
	if you pass in data you get the records where "data" matches the data

	WAB 2011/10 Reworked so would work with entityID = 0 (for brand new records)

--->

<cffunction name="getFlagData" returnType="query" output="no">
	<cfargument name="flagId" required="Yes" type="string">
	<cfargument name="data" required="no" default="">  <!--- used to match value in data column --->
	<cfargument name="entityid" required="no" type="numeric"  >  	 <!--- type = "numeric" --->
	<cfargument name="filter" required="no" default=""> <!--- SQL clause to filter by--->
	<cfargument name="sortorder" required="no" default="OrderingIndex">		<!--- 2012/05/29 PPB P-MIC001 added OrderingIndex --->
	<cfargument name="additionalColumns" required="no" default="">
	<cfargument name="returnLastUpdatedByName" required="no" default="false">
	<cfargument name="returnEntityDetails" required="no" default="true">
	<cfargument name="returnNulls" required="no" default="false">

	<cfset var qGetFlagData = "">
	<cfset var NameExpression = "">
	<cfset var LinkedEntityType = "">
	<cfset var LinkedEntityNameExpression = 	"">
	<cfset var entityType = "">
	<cfset var theFlag = getFlagStructure(FlagID)>
	<cfset var leftOrInner = "Inner">
	<cfif returnNUlls>
		<cfset leftOrInner = "Left">
	</cfif>


	<!--- WAB 2010/09/20 Added code to return something if flagid is invalid.  Will most likely cause an error further down stream
	perhaps need to have columns called flagid and entityid - not sure
	--->
	<cfif not theFlag.isOK>
		<cfset qGetFlagData = queryNew('flagid,entityid,data')>
		<cfreturn qGetFlagData>
	</cfif>

	<cfset entityType = theFlag.entityType>

	<cfif theFlag.linksToEntityTypeID is not "">
		<cfset LinkedEntityType = application.EntityType[theFlag.linksToEntityTypeID]>
		<cfset LinkedEntityNameExpression = replacenocase(LinkedEntityType.nameExpression,"lastname","e2.lastname")>
	</cfif>
	<cfset NameExpression = replacenocase(EntityType.nameExpression,"lastname","e.lastname")>

	<CFQUERY NAME="qGetFlagData" DATASOURCE="#application.SiteDataSource#">
			select
				fd.entityid,
				<cfif returnEntityDetails>
					e.#entityType.uniquekey#,
					<cfif NameExpression is not "">
					e.#preservesinglequotes(NameExpression)# as name ,
					</cfif>
				</cfif>
				<!--- hack to bring back data column - ought to know whether it exists --->
				<cfif theFlag.flagType.hasDataColumn >
					data,
				<cfelseif "2,3" contains theFlag.flagType.flagtypeid >
					case when fd.flagid is not null then 1 else 0 end as isset,
				</cfif>
				<cfif theFlag.linksToEntityTypeID is not "">
				    <!--- 2015-12-02    ACPK    BF-39 Fixed bug caused when LinkedEntityNameExpression is empty string --->
					<cfif LinkedEntityNameExpression is not "">
					   isNull(e2.#preservesinglequotes(LinkedEntityNameExpression)#,fd.data) as data_name ,
					<cfelse>
					   fd.data as data_name ,
					</cfif>
					<!--- WAB 2009/02/18 added a column to tell you if the linked entity is doesn't actually exist  --->
					case when e2.#preservesinglequotes(LinkedEntityType.uniqueKey)# is null then 1 else 0 end as linkedEntityDeleted ,
				</cfif>
				<cfif theFlag.hasMultipleInstances >
				, #theFlag.flagType.datatablefullname#ID as identity_
				</cfif>

				<cfif additionalColumns is not "">
				#preserveSingleQuotes(additionalColumns)#,
				</cfif>
				f.flagid,
				<cfif returnLastUpdatedByName>
				usergroup.name as lastUpdatedByName,
				</cfif>
				fd.lastUpdated

			from
				<cfif returnEntityDetails>
				#entityType.tablename# e
					JOIN
				</cfif>
				flag f <cfif returnEntityDetails>on 1=1</cfif>
					#leftOrInner# JOIN
				#theFlag.flagType.datatablefullname# fd  on f.flagid = fd.flagid <cfif returnEntityDetails >and fd.entityid = e.#entityType.uniquekey#<cfelseif structKeyExists (arguments,"entityid")> and fd.entityid = #arguments.entityid#</cfif>
				<cfif returnLastUpdatedByName>
					left outer join userGroup on userGroup.usergroupID = fd.lastUpdatedBy
				</cfif>
				<cfif theFlag.linksToEntityTypeID is not "">
					left join <cf_queryObjectName value="#LinkedEntityType.tablename#"> e2 on fd.data = e2.#preservesinglequotes(LinkedEntityType.uniqueKey)#
				</cfif>

			where f.flagId =  <cf_queryparam value="#theFlag.flagId#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- NJH 2009/10/29 P-FNL079 - added table alias --->
			<cfif data is not "">and data =  <cf_queryparam value="#data#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
			<cfif filter is not "">and #preserveSingleQuotes(filter)#</cfif>
			<cfif structKeyExists (arguments,"entityid") and returnEntityDetails>
				and e.#entityType.uniquekey# in (#entityid#) <!--- if we are returning entity details, and hence might also have a left join, we need to apply the filter to the entitytable, otherwise needs to be the flagdata table  --->
			</cfif>
			<cfif sortorder is not "">
			order by <cf_queryObjectName value="#sortorder#">
			<cfelseif theFlag.flagType.hasSortIndexColumn>
			order by sortOrder asc
			</cfif>

		</cfquery>

	<cfreturn qGetFlagData>
</cffunction>


<!---
	WAB 2011.02.07
	function for setting all data in a boolean flag group in one go
	code originally in flagTask.cfm
 --->
<cffunction name="setFlagGroupData" output="false">
	<cfargument name="flagGroupID" required="Yes">
	<cfargument name="entityID" required="Yes" > <!--- type="numeric" can now be a list of entityids --->
	<cfargument name="flagIDList" required="Yes">
	<cfargument name="flagIDList_Orig" > <!--- this allows us to update subsets of flags in a group by passing the list of original flagids set - if not passed in it is assumed that all flags are being displayed and all values not in flagIDList will be delete --->
	<cfargument name="updateTime" default = "getDate()">  <!--- ie the SQL time --->
	<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
	<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">


	<cfset var result = {numberDeleted = 0, numberInserted = 0}>
	<cfset var querySnippetSQL = '' />
	<cfset var deleteResult = '' />
	<cfset var insertResult = '' />

	<cfif entityID is 0>
		<cfreturn result>
	</cfif>

	<cfset arguments.flagGroupID = getFlagGroupStructure(arguments.flagGroupID).flagGroupID>


	<!--- delete flags no longer set --->
			<cfif not structKeyExists (arguments,"flagIDList_Orig") or flagIDList_Orig is not "">
					<!--- set the last updated on the boolean flags, allows trigger to see who has done the deletion
						WAB 2009/10/07 LID 2732, just refactored the code here to be similar to new block below

						WAB 2010/02/04 LID 3064
						Problem when the _orig variable ended up with an extra comms in it eg  ",324,453"
						This can happen when individual checkboxes from the same flag group are place on a screen; and one is originally checked and one is originally not checked
						Odd that error had not occurred before, since I have that scenario on one of my test pages
						So added in a function to remove leading, trailing and double commas
					--->
					<cfsaveContent variable="querySnippetSQL">
						<cfoutput>
						BooleanFlagData AS bfd
						inner join Flag as f on bfd.FlagID = f.FlagID
				 	WHERE bfd.EntityID = #EntityID#
			 		AND f.FlagGroupID = #flagGroupID#
				 	<CFIF flagIDList is not "">
						AND bfd.FlagID NOT IN ( #flagIDList# )
					</CFIF>
						<cfif structKeyExists (arguments,"flagIDList_Orig")>
							<cfif flagIDList_Orig is not "">
							AND bfd.FlagID IN (#application.com.regexp.removeExtraCommasFromList(flagIDList_Orig)#)
							</cfif>
						</cfif>
						</cfoutput>
					</cfsavecontent>


					<!--- and delete flags
						NJH case 433498	 - added lastupdatedByPerson
					--->
					<CFQUERY result = "deleteResult" datasource="#application.sitedatasource#">
					<!--- update lastupdatedby --->
					Update BooleanFlagData
					set lastupdated = getdate() ,
						lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					FROM
	  				#preserveSingleQuotes(querySnippetSQL)#

					<!--- and do delete --->
					DELETE BooleanFlagData <!--- NJH 2009/11/24 LHID 2732 - added booleanFlagdata --->
					FROM
					#preserveSingleQuotes(querySnippetSQL)#
					</CFQUERY>
					<cfset result.numberdeleted = deleteResult.recordcount>

			</CFIF>

			<!--- add new flags not already set
				NJH case 433498	 - added lastupdatedByPerson
			--->
			<CFIF flagIDList is not "">
					<CFQUERY result = "insertResult" datasource="#application.sitedatasource#">
					INSERT INTO BooleanFlagData
					(EntityID, FlagID, CreatedBy, Created, LastUpdatedBy, LastUpdated,lastUpdatedByPerson)
					SELECT
						<cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						f.FlagID,
						<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				  	FROM Flag AS f
			 		WHERE f.FlagID  IN ( <cf_queryparam value="#flagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND NOT EXISTS (SELECT bfd.FlagID FROM BooleanFlagData AS bfd WHERE bfd.FlagID = f.flagID and bfd.EntityID =  <cf_queryparam value="#EntityID#" CFSQLTYPE="cf_sql_integer" > )
					</CFQUERY>
					<cfset result.numberInserted = insertResult.recordcount>
			</CFIF>




	<cfreturn result>


 </cffunction>




<cffunction name="convertFlagWDDXToStruct">
	<cfargument name="WDDXData" required="yes" >
	<cfargument name="DefaultWDDX" required="yes" >

	<cfset var defaultStructure = '' />
	<cfset var x = '' />
	<cfset var currentStructure = '' />
	<cfset var i = '' />
	<cfset var thisItem = '' />

				<!--- get the default structure from the flag table --->
				<CFWDDX ACTION="WDDX2CFML" INPUT="#DefaultWDDX#" OUTPUT="defaultStructure">

							<CFIF WDDXData is not "">
								<!--- get existing structure --->
								<cftry>
									<CFWDDX ACTION="WDDX2CFML" INPUT="#WDDXData#" OUTPUT="currentStructure">
									<cfcatch>
										<cfoutput>corrupted wddx #WDDXData#</cfoutput>
										<cfreturn defaultStructure>
									</cfcatch>
								</cftry>

								<cfif isArray(defaultStructure)>
									<cfset defaultStructure = defaultStructure[1]>
									<cfif not isArray(currentStructure)>  <!--- if the default structre has been changed to an array then need to update the current structure to be an array --->
										<cfset x = arrayNew(1)>
										<cfset x[1] = currentStructure >
										<cfset currentStructure = x>
									<cfelseif arrayLen(currentStructure) is 0>
										<cfset currentStructure = defaultStructure>
									</cfif>
								</cfif>

								<!--- ought to check that all the fields in the default structure are
									contained in the stored structure this will allow the structure to be updated --->
									<CFLOOP item="thisItem" collection="#defaultStructure#">
										<cfif isArray(currentStructure)>
											<CFLOOP index="i" from="1" to ="#arrayLen(currentStructure)#">
													<CFIF not structKeyExists(currentStructure[i], thisItem)>
														<CFSET currentStructure[i][thisItem] = defaultStructure[thisItem]>
													</CFIF>
											</CFLOOP>
										<cfelse>
											<CFIF not structKeyExists(currentStructure, thisItem)>
													<CFSET currentStructure[thisItem] = defaultStructure[thisItem]>
											</CFIF>
										</cfif>
									</CFLOOP>
								<CFReturn currentStructure>
							<CFELSE>
								<CFReturn defaultStructure>
							</cfif>

</cffunction>


<cffunction name="convertFlagWDDXToQuery">
	<cfargument name="WDDXData" required="yes" >
	<cfargument name="DefaultWDDX" required="yes" >

	<cfset var theStructure = convertFlagWDDXToStruct (WDDXData=WDDXData,DefaultWDDX=DefaultWDDX)>
	<cfset var result  = "">
	<cfset var queryContainsRecords = "">
	<cfset var field = '' />

	<cfif isArray(theStructure)>
		<cfset result = application.com.structureFunctions.arrayOfStructuresToQuery (theStructure)>
	<cfelse>
		<cfset theStructure.arrayIndex = 1>   <!--- to add an array index column to the query --->
		<cfset result = application.com.structureFunctions.StructureToQuery (theStructure)>
	</cfif>
	<!--- if the query only has one record, check to see if it has any data in it.  May be completely blank (because a blank wddx structure still returns one record), in which case we should just return an empty query --->
	<cfif result.recordCOunt is 1 >
		<cfset queryContainsRecords = false>
		<cfloop list = "#result.columnList#" index="field">
			<cfif result[field][1] is not "" and field is not "arrayIndex">
				<cfset queryContainsRecords = true>
				<cfbreak>
			</cfif>
		</cfloop>
		<cfif not queryContainsRecords >
			<cfquery name="result" dbtype="query">
			select * from result where 1 = 0
			</cfquery>
		</cfif>
	</cfif>
	<cfreturn result>
</cffunction>


<!---
gets all the data for a flaggroup for a given entity, or by data value

mods:
WAB 2006-09-19 added ability to bring flags without data, added isSet column for boolean data
 --->
<cffunction name="getFlagGroupData" returnType="query" output="no">
	<cfargument name="flagGroupId" required="Yes" type="string">
	<cfargument name="data" required="no" default="">  <!--- used to match value in data column --->
	<cfargument name="entityid" required="no" default = "0" >
	<cfargument name="filter" required="no" default=""> <!--- SQL clause to filter by--->
	<cfargument name="sortorder" required="no" default="OrderingIndex">		<!--- 2012/05/29 PPB P-MIC001 added OrderingIndex --->
	<cfargument name="additionalColumns" required="no" default="">
	<cfargument name="returnLastUpdatedByName" required="no" default="false">
	<cfargument name="returnEntityDetails" required="no" default="true">
	<cfargument name="returnNulls" required="no" default="false">
	<cfargument name="returnActiveFlagsOnly" required="no" default="false">	<!--- 2011/09/02 PPB LID5916 only active flags required --->

	<cfset var qGetFlagGroupData = "">
	<cfset var theFlagGroup = getFlagGroupStructure(FlagGroupID)>
	<cfset var entityType = theFlagGroup.entityType>
	<cfset var NameExpression = replacenocase(EntityType.nameExpression,"lastname","e.lastname")>
	<cfset var leftOrInner = "Inner">
	<cfif returnNUlls>
		<cfset leftOrInner = "Left">
	</cfif>


	<CFQUERY NAME="qGetFlagGroupData" DATASOURCE="#application.SiteDataSource#">
					select
				fd.entityid,
				<cfif returnEntityDetails>
					e.#entityType.uniquekey#,
					<cfif NameExpression is not "">
					e.#preservesinglequotes(NameExpression)# as name ,
					</cfif>
				</cfif>
				f.flagid,
				f.name as flagname,
				case when isNull(f.namePhrasetextID,'') <> '' then 'phr_' + f.namePhrasetextID else f.name end as TranslatedFlagName,
				<!--- hack to bring back data column - ought to know whether it exists --->
				<cfif theFlagGroup.flagType.hasDataColumn >
					data,
				<cfelseif "2,3" contains theFlagGroup.flagtypeid >
					case when fd.flagid is not null then 1 else 0 end as isset,
				</cfif>
				<cfif additionalColumns is not "">
					#preserveSingleQuotes(additionalColumns)#,
				</cfif>
				<cfif returnLastUpdatedByName>
				usergroup.name as lastUpdatedByName,
				</cfif>
				f.flagTextID ,
				fd.lastUpdated

			from
				<cfif returnEntityDetails>
				#entityType.tablename# e
					JOIN
				</cfif>
				flag f  <cfif returnEntityDetails> on 1=1</cfif>
					#leftOrInner# JOIN
				#theFlagGroup.flagType.datatablefullname# fd  ON fd.flagid = f.flagid <cfif returnEntityDetails> and fd.entityid = e.#entityType.uniquekey#<cfelseif structKeyExists (arguments,"entityid")>and fd.entityid  in ( <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>

				<cfif returnLastUpdatedByName>
					left outer join userGroup on userGroup.usergroupID = fd.lastUpdatedBy
				</cfif>

			where
			f.flagGroupId =  <cf_queryparam value="#theFlagGroup.flagGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfif data is not "">and data =  <cf_queryparam value="#data#" CFSQLTYPE="CF_SQL_VARCHAR" > </cfif>
			<cfif filter is not "">and #preserveSingleQuotes(filter)#</cfif>
			<cfif structKeyExists (arguments,"entityid") and returnEntityDetails>and e.#entityType.uniquekey#  in ( <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif> <!--- NJH 2009/10/29 P-FNL079 - added table alias --->
			<cfif returnActiveFlagsOnly> and f.active=1 </cfif>			<!--- 2011/09/02 PPB LID5916 only active flags required --->
			<cfif sortorder is not "">
			order by <cf_queryObjectName value="#sortorder#">
			</cfif>

		</cfquery>

	<cfreturn qGetFlagGroupData>
</cffunction>




<cffunction name="deleteFlagData" output="yes">
	<cfargument name="flagId" required="Yes" type="string">
	<cfargument name="entityid" required="no" default = "0" >
	<cfargument name="data" required="no" default="">  <!--- used to match value in data column --->
	<cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
	<cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">


	<!---
		WAB 2006-03-21 modified so that should work against any flag table
		originally hard coded for integerMulti--->

	<cfset var deleteQry = '' />
	<cfset var theFlag = getFlagStructure(arguments.flagid)>
	<cfset var result = structNew()>

	<!--- NJH case 433498	 - added lastupdatedByPerson --->
	<cfquery name="deleteQry"  DATASOURCE="#application.SiteDataSource#">

		update #theFlag.flagType.datatablefullname#
			set lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				lastUpdated = getDate(),
				lastUpdatedByPerson = <cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		where
			flagID =  <cf_queryparam value="#theflag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- PJP CASE 430551:  Added in allowSelectStatement to queryparam --->
			AND entityID  IN ( <cf_queryparam value="#arguments.entityid#" CFSQLTYPE="CF_SQL_INTEGER" allowSelectStatement="true" list="true"> )
			<cfif len(arguments.data)>
			AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>

		delete from <cf_queryObjectName value="#theFlag.flagType.datatablefullname#"> where
			flagID =  <cf_queryparam value="#theflag.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- PJP CASE 430551:  Added in allowSelectStatement to queryparam --->
			AND entityID  IN ( <cf_queryparam value="#arguments.entityid#" CFSQLTYPE="CF_SQL_INTEGER" allowSelectStatement="true" list="true"> )
			<cfif len(arguments.data)>
			AND data =  <cf_queryparam value="#arguments.data#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
		select @@rowcount as numberDeleted

	</cfquery>

	<cfset result.numberDeleted = deleteQry.numberDeleted>

	<cfreturn result>

</cffunction>

<!---
WAB 2005-06-21

getJoinInfoForFlag

returns structure with joins required for building a query on this flag

(only works for flags with a single instance)

WAB 2013-01   Added entityTableAlias argument to allow for non-standard aliasing
 --->

<cffunction name="getJoinInfoForFlag" returntype="struct" output="no">
	<cfargument name="flagID" required="Yes" type="string">
	<cfargument name="joinType"  type="string" default = "Left">
	<cfargument name="returnLinkedEntity"  type="boolean" default = "true">
	<cfargument name="entityTableAlias"  type="string" >
	<cfargument name="LinkedEntityAlias"  type="string" default = "">

		<cfset var result = structNew()>
		<cfset var theFlagSTructure = getFlagStructure(flagid)>
		<cfset var dataTableName = theFlagSTructure.flagType.datatablefullname>
		<cfset var entityType = theFlagSTructure.entityType.tablename>
		<cfset var theflagid = theFlagSTructure.flagid>  <!--- this resolves flagid in case it was passed as a textid --->

		<cfif not structKeyExists (arguments,"entityTableAlias")>
			<cfset arguments.entityTableAlias = left(entitytype,1)>
		</cfif>

		<cfset result.tableAlias = "Flag_#theflagid#">
		<cfset result.alias = "Flag_#theflagid#">
		<cfset result.name = theFlagSTructure.name>

		<cfset result.join = "#chr(10)# #joinType# join #dataTableName#  as #result.tableAlias#  WITH (NOLOCK) on #arguments.entityTableAlias#.#theFlagSTructure.entityType.uniquekey# = #result.tableAlias#.entityid and #result.tableAlias#.flagid = #theflagid# ">

		<cfif theFlagSTructure.flagType.name is "radio" or theFlagSTructure.flagType.name  is "checkbox">
			<cfset result.selectField = "case when #result.tableAlias#.flagid is null then 0 else 1 end">
		<cfelse>
			<cfset result.selectField = " #result.tablealias#.data ">
		</cfif>
		<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added data for financial flags user defined currency --->
		<cfif theFlagSTructure.flagType.name is "financial">
			<cfset result.selectFieldB = " #result.tablealias#.currencyISOCode ">
		</cfif>

		<cfif theFlagStructure.linksToEntityTypeID is not "" and returnLinkedEntity>
			<cfif arguments.linkedentityAlias is "">
				<cfset arguments.linkedentityAlias = "#result.tableAlias#_ent">
			</cfif>
			<!---  might want a switch of some sort here--->
			<cfset result.join = result.join & " left join #application.entityType[theFlagStructure.linksToEntityTypeID].tablename# as #linkedentityAlias# with(nolock) on #linkedentityAlias#.#application.entityType[theFlagStructure.linksToEntityTypeID].uniquekey#  = #result.tableAlias#.data" >
			<cfset result.selectFieldRaw =  result.selectField>
			<cfset result.selectField = " isNull(#linkedentityAlias#.#application.entityType[theFlagStructure.linksToEntityTypeID].nameExpression#,#result.tablealias#.data) ">
			<cfset result.selectField = replaceNoCase (result.selectField, " lastName", " #linkedentityAlias#.LastName")>
		</cfif>

		<cfreturn result>

</cffunction>

<!---
WAB 2009/01/14  CR-TND556 changed createJoinForFlagGroup to do radios quicker in download
--->

<cffunction name="getJoinInfoForFlagGroup" returntype="array" output="no">
	<cfargument name="flagGroupID" required="Yes" type="string">
	<cfargument name="showValueForFlag" type="string" default="name"> <!--- NJH 2012/06/07 API project --->
	<cfargument name="entityTableAlias"  type="string" >

		<cfset var result = ArrayNew(1)>
		<cfset var theFlagGroupStructure = getFlagGroupStructure(flaggroupid)>
		<cfset var dataTableName = theFlagGroupStructure.flagType.datatablefullname>
		<cfset var entityType = theFlagGroupStructure.entityType.tablename>
		<cfset var theflaggroupid = theFlagGroupStructure.flaggroupid>  <!--- this resolves flaggroupid in case it was passed as a textid --->
		<cfset var getFlagGroups = "">
		<cfset var joinInfo = "">
		<cfset var i = '' />
		<cfset var getflags = '' />
		<cfset var showValue = "default">

		<cfif not structKeyExists (arguments,"entityTableAlias")>
			<cfset arguments.entityTableAlias = left(entitytype,1)>
		</cfif>


		<cfif theFlagGroupSTructure.flagType.name is "checkbox">

			<cfif arguments.showValueForFlag eq "flagTextID">
				<cfset showValue = "'flagTextID'">
			</cfif>

			<cfset result[1] = structNew()>
			<cfset result[1].tableAlias = "FlagGroup_#theflagGroupid#">
			<cfset result[1].alias = "FlagGroup_#theflagGroupid#">
			<cfset result[1].name = theFlagGroupSTructure.name>
			<cfset result[1].join	= "" >
			<cfset result[1].selectFieldRaw = "(dbo.BooleanFlagNumericList(#theflaggroupid#,#arguments.entityTableAlias#.#ENTITYtype#id))">
			<cfset result[1].selectField = "(dbo.BooleanFlagList(#theflaggroupid#,#arguments.entityTableAlias#.#ENTITYtype#id,#showValue#))">

		<cfelseif theFlagGroupSTructure.flagType.name is "radio" >
			<!--- WAB 2009/01/14 changed code for radio so that does not use booleanflaglist function (slow) and replaced with a left join (fast).  Can be done because know that only ever zero or one result will come back
				2017-01-20	WAB	RT-20 Altered getJoinInforForFlagGroup to use the new indexed vRadioFlagGroupData view to improve performance
			 --->
			<cfset result[1] = structNew()>
			<cfset result[1].tableAlias = "FlagGroup_#theflagGroupid#">
			<cfset result[1].alias = "FlagGroup_#theflagGroupid#">
			<cfset result[1].name = theFlagGroupSTructure.name>
			<!--- 2015-02-23	RPW	FIFTEEN-217 - Replaced #ENTITYtype#id with #theFlagGroupStructure.entityType.UniqueKey# as some tables do not have the primary key named in this way. --->
			<cfset result[1].join = "#chr(10)# left join vRadioFlagGroupData #result[1].tableAlias# WITH (NOEXPAND) ON #result[1].tableAlias#.flaggroupid = #theFlagGroupID# and #result[1].tableAlias#.entityid = #arguments.entityTableAlias#.#theFlagGroupStructure.entityType.UniqueKey#"> 
			<cfset result[1].selectFieldRaw = "#result[1].tableAlias#.flagID">
			<cfset result[1].selectField = "#result[1].tableAlias#.#arguments.showValueForFlag#">

		<cfelseif theFlagGroupSTructure.flagType.name is "group">

			<cfquery  name = "getflaggroups" datasource = "#application.sitedatasource#">
			select * from flaggroup where parentflaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfloop query="getFlagGroups">

				<cfset joinInfo = getJoinInfoForFlagGroup(flagGroupID = getFlagGroups.FlagGroupID,entityTableAlias = arguments.entityTableAlias)>
				<cfloop index = "i" from = 1 to = #arrayLen(joinInfo)#>
					<cfset arrayappend(result,joinInfo[i])>
				</cfloop>
			</cfloop>



		<cfelse >

			<!--- get join info for all the flags in group --->
			<cfquery  name = "getflags" datasource = "#application.sitedatasource#">
			select * from flag where flaggroupid =  <cf_queryparam value="#theFlagGroupSTructure.flaggroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfloop query="getFlags">
				<cfset arrayappend(result,getJoinInfoForFlag(flagID = FlagID,entityTableAlias = arguments.entityTableAlias))>
			</cfloop>


		</cfif>

		<cfreturn result>
</cffunction>


<cffunction name="getJoinInfoForListOfFlags" returntype="array">
	<cfargument name="flagList" required="Yes" type="string">
	<cfset var result = arrayNew(1)>
	<cfset var theFlag = "">

	<cfloop index="theFlag" list = "#flagList#">
		<cfif doesFlagExist(theFlag)>
			<cfset arrayappend(result,getJoinInfoForFlag(flagid = theFlag, argumentCollection = arguments))>
		</cfif>
	</cfloop>

	<cfreturn result>

</cffunction>


<cffunction name="getJoinInfoForListOfFlagGroups" returntype="array">
	<cfargument name="flagGroupList" required="Yes" type="string">
	<cfset var result = arrayNew(1)>
	<cfset var theFlag = "">
	<cfset var joinInfo = '' />
	<cfset var theFlagGroup = '' />
	<cfset var i = '' />

	<cfloop index="theFlagGroup" list = "#flagGroupList#">
		<cfset joinInfo = getJoinInfoForFlagGroup(flagGroupID = theFlagGroup , argumentCollection = arguments)>
		<cfloop index = "i" from = 1 to = #arrayLen(joinInfo)#>
			<cfset arrayappend(result,joinInfo[i])>
		</cfloop>

	</cfloop>

	<cfreturn result>

</cffunction>



<cffunction name="getFlagGroupChildGroups" returntype="query">
	<cfargument name="ParentFlagGroupId" required="Yes" type="numeric">
	<cfset var FlagGroupChildrenQry = '' />
	<!--- Return the set of ChildFlagroups. --->
	<CFQUERY NAME="FlagGroupChildrenQry" DATASOURCE="#application.SiteDataSource#">
		select FlagGroupTextId,name from FlagGroup where ParentFlagGroupId = #arguments.ParentFlagGroupId#
	</CFQUERY>
	<cfreturn FlagGroupChildrenQry>
</cffunction>

<!--- NAS 2010/06/23 P-CLS002 - Amended function to receive FlagGroupTextID --->
<cffunction name="getFlagGroupFlags" returntype="query">
	<cfargument name="FlagGroupId" required="Yes" type="string">
	<cfargument name="cachedWithin" type="date" default="#createTimeSpan(0,0,0,0)#">
	<cfargument name="systemFlags" required="false" type="boolean">
	<cfargument name="active" required="false" type="boolean">

	<!--- Return the set of ChildFlagroups. --->
	<cfset var FlagGroupFlagsQry = "">

	<CFQUERY NAME="FlagGroupFlagsQry" DATASOURCE="#application.SiteDataSource#" cachedWithin="#arguments.cachedWithin#">
		select
		<!--- START: 2014-01-14 NYB Case 438608 added translatedName --->
		case when isNull(NamePhraseTextID,'') = '' then Name else case when NamePhraseTextID like 'phr_%' then NamePhraseTextID else 'phr_'+NamePhraseTextID end end as translatedName,
		<!--- END: 2014-01-14 NYB Case 438608 --->
		*  from Flag where FlagGroupId =  <cf_queryparam value="#getFlagGroupStructure(flaggroupid).flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfif structKeyExists(arguments,"systemFlags")>
			and isSystem <cfif not arguments.systemFlags>!=<cfelse>=</cfif> 1
		</cfif>
		<cfif structKeyExists(arguments,"active")>and active = 1</cfif>
		ORDER BY orderingIndex <!--- 2014-08-18 REH Case 441300 added order by using orderingIndex --->
	</CFQUERY>
	<cfreturn FlagGroupFlagsQry>
</cffunction>


<!--- START 2014-07-10 PPB Case 440579 added EntityName argument --->
<cffunction name="doesFlagExist" returntype="numeric" hint ="Finds out whether a flag exists" output="no">
	<cfargument name="FlagId" required="Yes" type="string">
	<cfargument name="EntityName" required="No" type="string">

	<cfset var returnValue = 0>
	<cfset var flagStructure = "">
	<cfparam name="application.flagDoesNotExist" default="#structNew()#">

	<cfif Not structKeyExists(application.flagDoesNotExist,arguments.flagID)>
          <cfset flagStructure = getFlagStructure(arguments.flagID)>
          <cfif flagStructure.isOK>
 				<cfif (Not StructKeyExists(arguments,"EntityName")) OR (StructKeyExists(flagStructure.EntityType,"TableName") and arguments.EntityName eq flagStructure.EntityType.TableName)>
					<cfset returnValue = flagStructure.flagid >
				</cfif>
          <cfelse>
                <cfset application.flagDoesNotExist[arguments.flagID] = "">
          </cfif>
    </cfif>

	<cfreturn returnValue>
</cffunction>
<!--- END 2014-07-10 PPB Case 440579 --->

<!--- START 2014-07-10 PPB Case 440579 added EntityName argument --->
<cffunction name="doesFlagGroupExist" returntype="numeric" hint ="Finds out whether a flag group exists" output="YES">
	<cfargument name="FlagGroupId" required="Yes" type="string">
	<cfargument name="EntityName" required="No" type="string">

	<cfset var returnValue = 0>
	<cfset var flagGroupStructure = "">
    <cfparam name="application.flagGroupDoesNotExist" default="#structNew()#">

    <cfif Not structKeyExists(application.flagGroupDoesNotExist,arguments.flagGroupID)>
          <cfset flagGroupStructure = getFlagGroupStructure(arguments.FlagGroupId)>
          <cfif flagGroupStructure.isOK>
				<cfif (Not StructKeyExists(arguments,"EntityName")) OR (StructKeyExists(flagGroupStructure.EntityType,"TableName") and arguments.EntityName eq flagGroupStructure.EntityType.TableName)>
		 			<cfset returnValue = flagGroupStructure.flagGroupid >
				</cfif>
          <cfelse>
                <cfset application.flagGroupDoesNotExist[arguments.flagGroupID] = "">
          </cfif>
    </cfif>

	<cfreturn returnValue>
</cffunction>
<!--- END 2014-07-10 PPB Case 440579 --->



<!---

	functions to decide whether someone has rights to update a flaggroup or flag definition  (ie not the data)
	WAB 2005-10-17   to sort out problems with the profile editor


 --->


<cffunction name="doesCurrentUserHaveRightsToEditFlagGroupDefinition" returntype="boolean" output="no">
	<cfargument name="FlagGroupId" required="Yes" type="string">
		<!--- user has rights either if they are the creator of the flaggroup or
			they are an administrator

			The rules could be updated in the future, but atleast there is now a function where it casn be done
			2006-10-03  - WAB added test based on recordrights for the flaggroup

		 --->

		<cfset var flagGroupStructure = getFlagGroupStructure(FlagGroupId)>
		<cfif flagGroupStructure.isOK>
			<cfif (flagGroupStructure.createdBy is request.relaycurrentuser.usergroupid  <!--- currentUser is creator --->
					or
				application.com.login.checkInternalPermissions("adminTask","level1")	<!--- currentUser is an administrator --->
					or
				application.com.rights.getRecordrights(entity="flaggroup",entityid=flagGroupStructure.flaggroupid,personid = request.relaycurrentuser.personid,level=5)		<!--- currentUser has been given specific rights to this flaggroup--->

				) >

				<cfreturn 	true>
			<cfelse>
				<cfreturn 	false>
			</cfif>

		<cfelse>
			<cfreturn 	false>
		</cfif>





</cffunction>



<cffunction name="doesCurrentUserHaveRightsToEditFlagDefinition" returntype="boolean" output="no">
	<cfargument name="FlagId" required="Yes" type="string">

	<cfset var flagStructure = getFlagStructure(FlagId)>
	<cfif flagStructure.isOK>
		<cfreturn 	doesCurrentUserHaveRightsToEditFlagGroupDefinition (flagStructure.flaggroupid)>
	<cfelse>
		<!--- should be OK now using getFlagStructure
		<cfset updateFlagStructure(FlagId)>  <!--- sometimes the application.flag structure goes funny - do an update just in case it really does exist--->
		--->
		<cfreturn 	false>
	</cfif>

</cffunction>


<cffunction name="doesCurrentUserHaveRightsToFlag" returntype="boolean" output = no>
	<cfargument name="FlagId" required="Yes" type="string">
	<cfargument name="Type" required="Yes" type="string">		<!--- view,edit,search --->

	<cfset var thisFlagID = doesFlagExist(flagid)><!--- gets numericID - returns 0 ie false if not exists --->

	<cfif not thisFlagID>
		<cfreturn true>  <!--- return true - some other part of the program can pick up that it is an invalid ID --->
	</cfif>

	<cfreturn 	doesCurrentUserHaveRightsToFlagGroup(flagGroupID = getFlagStructure(flagID).flaggroupid, type = type)>
</cffunction>

<cffunction name="doesCurrentUserHaveRightsToFlagGroup" returntype="boolean" output = no>
	<cfargument name="FlagGroupId" required="Yes" type="string">
	<cfargument name="Type" required="Yes" type="string">		<!--- view,edit,search --->

	<cfset var FlagGroupStructure = getFlagGroupStructure(flagGroupid)><!--- gets numericID - returns 0 ie false if not exists --->
	<cfset var rightsQuery = '' />

	<cfparam name="session.FlagGroupRights" default = "#structNew()#">


	<cfif not FlagGroupStructure.isok>
		<cfreturn true>  <!--- return true - some other part of the program can pick up that it is an invalid ID --->
	</cfif>

	<cfif not structKeyExists(session.FlagGroupRights,FlagGroupStructure.flagGroupID)
		or
		session.FlagGroupRights[FlagGroupStructure.flagGroupID].LastUpdated LT FlagGroupStructure.structureLastUpdated
	>
		<cfset session.FlagGroupRights[FlagGroupStructure.flagGroupID] = 	getFlagGroupRights (flagGroupID = FlagGroupId, personid = request.relayCurrentUser.personid)>

	</cfif>

	<cfset rightsQuery = session.FlagGroupRights[FlagGroupStructure.flagGroupID]>
	<cfif arguments.type is "view"><cfset arguments.type="viewing"></cfif>
	<cfreturn rightsQuery[type][1]>
</cffunction>


<cffunction name="getFlagGroupRights" returntype="query" output = no>
	<cfargument name="FlagGroupId" required="Yes" type="string">
	<cfargument name="PersonID" required="Yes" type="numeric">
	<cfset var FlagGroupRights = "">
	<cfset var FlagGroupStructure = getFlagGroupStructure(arguments.flagGroupID)>
	<cfset var parentRights = '' />

	<!--- get rights to this flagGroup --->
	<CFQUERY NAME="FlagGroupRights" DATASOURCE="#application.SiteDataSource#">
		select
		FG.FLAGgROUPID,
--		ViewingAccessRights,

		case 	when ViewingAccessRights = 0 and FG.viewing = 1 then 1
			WHEN ViewingAccessRights = 1 AND SUM(CONVERT(INT,FGR.VIEWING)) > 0 THEN 1
			ELSE 0 END AS VIEWING
		,
		case 	when eDITAccessRights = 0 and FG.Edit = 1 then 1
			WHEN eDITAccessRights = 1 AND SUM(CONVERT(INT,FGR.edit)) > 0 THEN 1
			ELSE 0 END AS Edit,
		getdate() as lastupdated


		from
			flagGroup FG
				left join
			(flaggroupRights FGR
				inner join
			rightsgroup rg
				on rg.usergroupid = fgr.userid
				AND personid = #arguments.personid#
			)
				on fg.flaggroupid = fgr.flaggroupid

		WHERE
			FG.FLAGgROUPID =  <cf_queryparam value="#FlagGroupStructure.flagGroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
		GROUP BY FG.FLAGGROUPID,
		FG.ViewingAccessRights, FG.Viewing,
		FG.EditAccessRights, FG.Edit
	</CFQUERY>
	<!--- if there is a parent flaggroup then we need to combine the rights to that as well --->
	<cfif FlagGroupStructure.parentFlagGroupid is not 0>
		<cfset parentRights = getFlagGroupRights (flagGroupID = FlagGroupStructure.parentFlagGroupid, personid = personid)>

		<!--- using sql to AND the results together --->
		<CFQUERY NAME="FlagGroupRights" DATASOURCE="#application.SiteDataSource#">
		select #FlagGroupStructure.flagGroupID# as flagGroupID,
			#parentRights.viewing# & #flagGrouprights.viewing# as viewing,
			#parentRights.edit# & #flagGrouprights.edit# as edit,
			#createodbcdatetime(flagGrouprights.lastupdated)# as lastupdated
		</CFQUERY>

	</cfif>
	<cfreturn flagGroupRights>
</cffunction>



<!---
WAB 2007-06-11 checks whether a flagTextID is Unique
if flagid is passed as well then doesn't count itself! (usually used when creating a new flagTextID so flagid is 0)
if flagtextid is "" then returns true anyway
NJH 2016/12/13 JIRA PROD2016-2957 - check if flagTextID is unique against the flagGroupTextID for boolean flags.
 --->
	<cffunction access="remote" name="isFlagTextIDUnique"  returntype="boolean">
		<cfargument name="FlagTextID" type="string" required="true">
		<cfargument name="FlagID" type="numeric" default=0>
		<cfargument name="entityType" type="string" required="true"> <!--- NJH 2016/12/14 - check against the entity columns as well --->
		<cfargument name="flagTypeID" type="numeric" required="true">

			<cfset var entitytable = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).tablename>
			<cfset var get = '' />
			<cfset var result = "true">

			<cfif trim(arguments.FlagTextID) is not "">
				<cfquery name = "get" datasource="#application.sitedatasource#">
				select 1
				from
					flag
				where flagtextid =  <cf_queryparam value="#arguments.flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfif flagid is not 0 >
				and flagid <> #flagid#
				</cfif>

				<cfif listFindNoCase("2,3",arguments.flagTypeID)>
				union
				select 1 from flagGroup
				where flagGroupTextID =  <cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and flagTypeID in (2,3)

				<cfelse>
				union
				select 1 from information_schema.columns
					where table_name = <cf_queryparam value="#entitytable#" cfsqltype="cf_sql_varchar">
						and column_name= <cf_queryparam value="#arguments.flagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				</cfquery>

				<cfif get.recordcount is not 0>
					<cfset result = "false">
				</cfif>
			</cfif>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2015/09/24 check uniqueness of flagGroupTextId against entity column names as well, so that every field against an entitytype is unique --->
	<cffunction access="remote" name="isFlagGroupTextIDUnique"  returntype="boolean">
		<cfargument name="FlagGroupTextID" type="string" required="true">
		<cfargument name="FlagGroupID" type="numeric" default=0>
		<cfargument name="entityType" type="string" required="true"> <!--- NJH 2015/09/24 - check against the entity columns as well --->

			<cfset var get = '' />
			<cfset var result = "true">

			<cfset var entitytable = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).tablename>

			<!--- AJC SONYDNS1 - was trim(flagtexID) and caused error as not passed --->
			<cfif trim(FlagGroupTextID) is not "">
				<cfquery name = "get" datasource="#application.sitedatasource#">
				select 1
				from
					flaggroup
				where flaggrouptextid =  <cf_queryparam value="#flaggrouptextid#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfif flaggroupid is not 0 >
				and flaggroupid <> #flaggroupid#
				</cfif>
				union
				select 1 from information_schema.columns where table_name = <cf_queryparam value="#entitytable#" cfsqltype="cf_sql_varchar">
					and column_name= <cf_queryparam value="#flaggrouptextid#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfif get.recordcount is not 0>
					<cfset result = "false">
				</cfif>
			</cfif>

		<cfreturn result>
	</cffunction>

	<!---
	rules - must be a valid variable name

	 --->
	<cffunction access="remote" name="isTextIDValid"  returntype="boolean">
		<cfargument name="TextID" type="string" required="true">
			<cfset var result = true>
	 		<cfset var regExp = "\A[a-zA-Z][a-zA-Z0-9_]*\Z">

			<cfif refindNoCase(regExp,TextID) is 0 >
				<cfset result = false>
			</cfif>


			<cfreturn result>
	</cffunction>





	<!--- NJH 2009/05/28 P-SNY063 --->
	<cffunction name="copyEntityFlags" access="public" hint="Copies flags from one entity to another of the same type">
		<cfargument name="entityTypeID" type="numeric" required="true">
		<cfargument name="fromEntityID" type="numeric" required="true">
		<cfargument name="toEntityID" type="numeric" required="true">
		<cfargument name="flagGroupsToExclude" type="string" default="">

		<cfscript>
			var argumentStruct = structNew();
			var getFlagGroupsForEntityType = "";
			var flagGroupData = '';
			var column = '' ;
		</cfscript>

		<cfquery name="getFlagGroupsForEntityType" datasource="#application.siteDataSource#">
			select flagGroupID from flagGroup where entityTypeID = #arguments.entityTypeID#
				and flagTypeID > 1
				<cfif arguments.flagGroupsToExclude neq "">
					and flagGroupID  not in ( <cf_queryparam value="#arguments.flagGroupsToExclude#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</cfif>
		</cfquery>

		<cfloop query="getFlagGroupsForEntityType">
			<cfset flagGroupData = getFlagGroupData(entityID=arguments.fromEntityID,flagGroupID=flagGroupID)>
			<cfif flagGroupData.flagID neq "">
				<cfloop list="#flagGroupData.columnList#" index="column">
					<cfset argumentStruct[column] = evaluate("flagGroupData."&column)>
				</cfloop>
				<cfset argumentStruct.entityID = arguments.toEntityID>
				<cfset setFlagData(argumentCollection=argumentStruct)>
			</cfif>
		</cfloop>
	</cffunction>


	<cffunction name="loadApplicationStructures">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="updateCluster" default="false">

		<cfset createFlagTypeAndFlagEntityTypeStructures(applicationScope = applicationScope)>
		<cfset createFlagStructure(applicationScope = applicationScope)>
<!---
	2010/09 WAB see what happens if we don't pre-load
	<cfset updateFlagAndGroupStructure(updateCluster = updateCluster)> --->
		<cfset arguments.applicationScope.flag = structNew()>
		<cfset arguments.applicationScope.flagGroup = structNew()>
		<cfset arguments.applicationScope.flagGroupDoesNotExist = structNew()>
		<cfset arguments.applicationScope.flagDoesNotExist = structNew()>

		<cfreturn applicationScope.sitedatasource> <!--- not sure why we return that! --->

	</cffunction>


<!--- William try out some ideas for easier display of flags
	NJH 2012/12/04 Roadmap2013 - support for multiple text flags - returned a type of array of flag controls
--->
	<cffunction access="public" name="getFlagGroupDataAndControls" returnType="array">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="entityID" type="string" required="true">
		<cfargument name="formEntityID" type="string" default="#entityID#">		<!--- for new record where we don't have an entityid then need to pass in entityid = 0 and formEntityID can be something else (such as newRecord), when form is processed formEntityID is replaced with the actual entityid--->
		<cfargument name="method" type="string" default="view">
		<cfargument name="additionalFormatting" type="struct" default="#structNew()#">

		<cfset var result = {isok = true,control_html = "",label=""}>
		<cfset var argumentCollection = {}>
		<cfset var flagGroupStructure = getFlagGroupStructure(flagGroupID)>

		<!--- NJH 2015/05/06 - return empty array element if not ok --->
		<cfif not flagGroupStructure.isOK>
			<cfset result.isOK = false >
			<cfreturn [result]>
		</cfif>

		<cfset var typeName = flagGroupStructure.flagType.name>
		<cfset var entityTypeID = flagGroupStructure.entityType.entityTypeId>
		<cfset var flagGroupFormatting = "">
		<cfset var flagControls = arrayNew(1)>
		<cfset var flagIDs = 0>

		<!--- get data for the flagGroup --->
		<!--- WAB 2011/10 changes here so will work with entities which don't exist yet --->
		<cfset argumentCollection = {flagGroupID = flagGroupID, entityID = entityID,returnnulls=true,filter="f.active = 1"}>
		<cfif not isNumeric(EntityID) or EntityID is 0>
			<cfset argumentCollection.returnEntityDetails = false>
			<cfset argumentCollection.entityid = 0>
		</cfif>

		<!--- make controls --->

		<cfset result.typename = typename>


		<cfif listFindNoCase("checkbox,radio",typename)>

			<cfset result.flagGroupData = getFlagGroupData(argumentCollection = argumentCollection)>

			<!--- merge in formatting information for this flaggroup --->
			<cfset flagGroupFormatting = application.com.flag.getFlagGroupFormattingParameters(flagGroupStructure.flagGroupID,additionalFormatting)>
			<cfif structKeyExists (arguments,"required")>
				<cfset flagGroupFormatting.required  = arguments.required>
			<cfelse>
				<cfparam name="flagGroupFormatting.required" default="0">
			</cfif>

			<cfparam name="flagGroupFormatting.displayAs" default="#typename#">
			<cfparam name="flagGroupFormatting.required" default="0">
			<!--- Make sure that displayAs is valid for the type of flag--->
			<cfif typename is "checkbox" and listFindNoCase ("select",flagGroupFormatting.displayAs)> <!--- during CASE 437370, allow displayAs=select for checkboxes, but needs to be converted to multiSelect--->
				<cfset flagGroupFormatting.displayAs = "multiselect">
			<cfelseif typename is "checkbox" and not listFindNoCase ("checkbox,multiselect,twoSelects",flagGroupFormatting.displayAs)> <!--- CASE 437370, allow twoSelects --->
				<cfset flagGroupFormatting.displayAs = "checkBox">
			<cfelseif typename is "radio" and not listFindNoCase ("radio,select",flagGroupFormatting.displayAs)>
				<cfset flagGroupFormatting.displayAs = "radio">
			</cfif>

			<cfif typename is "radio">
				<cfset flagGroupFormatting.showNull = true>
			</cfif>

			<cfset result.fieldroot = "#typename#_#flagGroupStructure.FlagGroupID#">  <!--- 2013-10-10 WAB CASE 437400 Fix here, use flagGroupStructure.FlagGroupID rather than attributes.flagGroupID --->
			<cfset result.fieldname = result.fieldroot & "_#arguments.formEntityID#">
			<cfset result.label = flagGroupStructure.translatedName>
			<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=flagGroupFormatting,query=result.flagGroupData, value = "flagid", display = "translatedFlagName", name="#result.fieldName#", selectedColumn="isSET",entityTypeID=entityTypeID,entityID=arguments.entityID,keepOrig=true,label=result.label,method=method)>
			<cfif method is "edit">
				<cfset result.control_html = result.control_html & '<INPUT type="hidden" name="frm#flagGroupStructure.entityType.tablename#FlagList" value="#result.fieldroot#">'>
			</cfif>

			<cfset flagControls[1] = result>

		<cfelse>
			<!--- NJH 2012/12/04 Roadmap2013 - support for multiple text flags --->
			<cfset flagIDs = getFlagGroupFlags(flagGroupID=arguments.flagGroupID,active=true)>
			<cfset flagIDs = valueList(flagIDs.flagID)>
			<cfset var flagid = "">
			<cfloop list="#flagIDs#" index="flagID">
				<cfset arrayAppend(flagControls,getFlagDataAndControls(flagId=flagID,entityID=arguments.entityID,method=arguments.method,additionalFormatting=arguments.additionalFormatting))> <!--- NJH 2015/09/23 - pass through additionalFormatting through to flags --->
			</cfloop>
		</cfif>

		<cfreturn flagControls>
	</cffunction>

	<!--- 2016-10-05	WAB	PROD2016-2433 Issue with validValues when entityID is not numeric (ie a new record).  Only seemed to occur for textMultiple flags but have fixed all places to use argumentCollection.entityID (which = 0 for non numeric arguments.entityid) --->
	<cffunction access="public" name="getFlagDataAndControls">
		<cfargument name="flagID" type="string" required="true">
		<cfargument name="entityID" type="string" required="true">
		<cfargument name="formEntityID" type="string" default="#entityID#">		<!--- for new record where we don't have an entityid then need to pass in entityid = 0 and formEntityID can be something else (such as newRecord), when form is processed formEntityID is replaced with the actual entityid--->
		<cfargument name="method" type="string" default="edit">
		<cfargument name="additionalFormatting" type="struct" default="#structNew()#">
		<cfargument name="countryid" type="string" default="0">		 <!--- for validvalues, not sure whether should be 0 or blank --->

		<cfset var result = {isok = true,control_html = "",label=""}>
		<cfset var argumentCollection = {}>
		<cfset var args = {}>
		<cfset var flagStructure = getFlagStructure(flagID)>
		<cfset var typeName = "">
		<cfset var fieldNameAndRoot = '' />
		<cfset var validValues = '' />
		<cfset var validvaluelist = '' />
		<cfset var validValueQuery = '' />
		<cfset var requiredLabel = '' />
		<cfset var flagList = '' />
		<cfset var tempFlagData = '' />
		<cfset var requiredMessage = '' />
		<cfset var javascriptVerification = '' />
		<cfset var attributeStruct = structNew()>
		<cfset var origValue = '' />
		<cfset var charType="numeric">
		<cfset var result_defaultValue = '' />

		<cfif not flagStructure.isOK>
			<cfset result.label = "Flag #flagid# not found">
			<cfset result.isOK = false >
			<cfreturn result>
		</cfif>


		<cfset fieldNameAndRoot = getFieldNameAndRootForFlag (flagStructure = flagStructure, entityID = formEntityID)>
		<cfset result.fieldName = fieldNameAndRoot.name>
		<cfset result.fieldRoot = fieldNameAndRoot.root>
		<cfset typeName = flagStructure.flagType.name>
		<cfset result.typename = typeName>
		<!--- get data for the flag --->
		<!--- WAB 2011/10 changes here so will work with entities which don't exist yet --->
		<cfset argumentCollection = {flagID = flagID, entityID = entityID,returnnulls=true,filter="",returnnulls = false}>
		<cfif not isNumeric(entityID)>
			<cfset argumentCollection.returnEntityDetails = false>
			<cfset argumentCollection.entityid = 0>
		</cfif>
		<cfscript>
			// 2014-08-11  RPW Create new profile type: "Financial"
			if (TypeName=="financial") {
				argumentCollection.additionalColumns = "currencyISOCode";
			}
		</cfscript>
		<cfset result.flagData = getFlagData(argumentCollection = argumentCollection)>

		<!--- 2014-07-04 REH case 440842 value to be displayed can obtained from defaultValue (if provided) and only if number of records returned from flagData is zero --->
		<cfif result.flagData.recordcount eq 0 and structKeyExists(additionalFormatting,"defaultValue") and len(additionalFormatting.defaultValue)>
			<cfset result_defaultValue = application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=additionalFormatting.defaultValue)>
			<!--- 2014-07-04  REH case 440961 related to change made for 440842, change logic and now adding a dummy record to the original query (result.flagData) to hold the defaultValue if one is provided  --->
			<cfscript>
			    if(len(trim(result_defaultValue)) and structKeyExists(result.flagData,'data')){
			    	queryAddRow(result.flagData, 1);
				    querySetCell(result.flagData, "data", result_defaultValue, 1);
			    }
			</cfscript>
		</cfif>

		<cfset result.flagFormatting = application.com.flag.getFlagFormattingParameters(flagStructure.flagID,additionalFormatting)>
		<cfset result.label = flagStructure.translatedName>
		<cfif structKeyExists (arguments,"required") >
			<cfset result.flagFormatting.required  = arguments.required>
		<cfelseif structKeyExists(arguments.additionalFormatting,"required")>	<!--- 2012-08-16 PPB Case 430059 added required attribute from additionalFormatting if it exists --->
			<cfset result.flagFormatting.required  = arguments.additionalFormatting.required>
		<cfelse>
			<cfparam name="result.flagFormatting.required" default="0">
		</cfif>
		<cfparam name="result.flagFormatting.class" default="">
		<cfif not request.relayFormDisplayStyle neq "HTML-div">
			<cfif structKeyExists(result.flagformatting,"class")>
				<cfset result.flagformatting.class = result.flagformatting.class & " form-control">
			<cfelse>
				<cfset result.flagformatting.class = "form-control">
			</cfif>
		</cfif>
		<!--- make controls --->

		<cfif TypeName is "checkbox">
			<cfparam name="result.flagFormatting.displayAs" default="checkbox">
			<!--- single checkbox --->

			<cfsaveContent variable="result.control_html"><cfoutput>
				<!--- 	WAB 2013-03-26 P-AVD005 added the required attribute
						WAB 2015-04-20 Added Label attribute (used by rwFormValidation)
				--->
				<cf_input type="checkbox" name="#result.fieldname#" class="#result.flagFormatting.class#" disabled="#IIF(method is 'edit',false,true)#" id="#result.fieldname#" value="#flagStructure.flagID#" checked="#result.flagData.recordcount#" required="#result.flagFormatting.required#" label="#result.label#">
				<!--- _Required Hidden Field --->
				<cfif result.flagFormatting.required >
					<CF_INPUT type="Hidden" name="#result.fieldname#_required" label="#flagStructure.translatedName#" value="#flagStructure.translatedName# phr_sys_formvalidation_req">
				</cfif>
				<cfif method is "edit">
					<cf_input type="hidden" name="#result.fieldname#_orig" value="#result.flagData.flagID#">
				</cfif>
			</cfoutput></cfsaveContent>
			<cfset result.label = "<label for='#result.fieldname#'>#htmleditformat(result.label)#</label>">


		<cfelseif typeName is "text">

			<cfif flagStructure.usevalidvalues>
				<cfset validValues = application.com.relayForms.getValidValues (fieldName="flag_#flagStructure.flagtextid#",countryid = countryid,entityid = argumentCollection.entityID)>
				<cfif result.flagFormatting.displayAs is ""><cfset result.flagFormatting.displayAs = "Select"></cfif>
				<!--- START AXA 2015-02-12 P-AER001 use translated name for required label and pass in required attribute correctly --->
				<cfif requiredlabel is ""><cfset requiredLabel = flagStructure.translatedName ></cfif>
				<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=result.flagFormatting,query=validvalues,   value = "dataValue", display = "displayValue", name="#result.fieldName#", selected = result.flagData.data, keepOrig=true,label=result.label,method=method,required = result.flagFormatting.required,requiredlabel = requiredlabel,shownull = true)>
				<!--- END AXA 2015-02-12 P-AER001 use translated name for required label and pass in required attribute correctly--->
			<cfelseif isdefined ("flag_#flagStructure.FlagTextID#_ValidValues")>   <!--- this case comes from the old flag code, but unlikely to work within a cfc function --->
					<cfset validvaluelist = evaluate('flag_#flagStructure.flagtextid#_validvalues')>
					<cfset validValueQuery = createQueryFromValidValueList (list = validvaluelist,valuecolumn="dataValue",displayColumn="displayValue")>

					<cfif requiredlabel is ""><cfset requiredLabel = flagname ></cfif>
					<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=result.flagFormatting,query=validvalueQuery, name="#result.fieldName#", selected=result.flagData.data,entityID=arguments.entityID,keepOrig=true,label=result.label,method=method,required = required,requiredlabel = requiredlabel)>

			<cfelseif result.flagFormatting.displayas is "memo">
					<cfparam name="result.flagFormatting.cols" default="40">
					<cfparam name="result.flagFormatting.rows" default="3">
					<!--- 2008/04/18 GCC extended memo type to enable max length attribute to be used --->
					<!--- NJH 2012/02/16 CASE 425074 include js file --->
					<cf_includeJavascriptOnce template="/javascript/limitTextArea.js" translate="true">
					<!--- 2014-11-12 WAB alter charsLeft code to use P and some new javascript --->
					<cfset args = {
							id = result.fieldname,
							name=result.fieldname,
							onclick="doleft(#result.flagFormatting.maxlength#,this,this.name + 'charsleft')",
							onkeyup="doleft(#result.flagFormatting.maxlength#,this,this.name + 'charsleft')",
							cols="#result.flagFormatting.cols#" ,
							rows="#result.flagFormatting.rows#",
							class="#result.flagFormatting.class#",
							wrap="VIRTUAL",
							required=result.flagFormatting.required
								}>
					<!--- 2013-02-25 PPB Case 431614 allow textarea to be disabled/readonly 2012-03-12 WAB converted to use an argument collection --->
							<cfif method neq 'edit'></cfif>
							<!--- 2013-07-17 YMA Case 436223 flagGormatting exists in the Result struct --->
							<cfif StructKeyExists(result.flagformatting,"disabled") and result.flagformatting.disabled><cfset args.disabled="true" > </cfif>
							<cfif StructKeyExists(result.flagformatting,"readonly") and result.flagformatting.readonly><cfset args.readonly="true" > </cfif> <!--- NJH 2015/04/23 - add readonly if it exists --->
							<cfif args.required><cfset args.message = "#result.label# phr_sys_formvalidation_req"></cfif>
					<cfsavecontent variable="result.control_html">
					<cfoutput>
						<textarea  #application.com.relayForms.convertTagAttributeStructToValidAttributeList(attributeStruct=args,tagName="textArea")#>#HTMLEditFormat(trim(result.flagData.data))#</textarea>
						<!--- NJH 2015/05/12 - kanbanise 580 - removed the IIF statement and replaced with below. Removes the need for the DE which seemed to error when input string contained characters like ~ --->
						<cf_input type="Hidden" name="#result.fieldname#_orig" value="#result.FlagData.RecordCount IS NOT 0 and not isdefined('flag_#trim(flagStructure.flagtextid)#_default')?result.flagData.data:''#">
						<cfif result.flagFormatting.maxlength GT 0>
							<p name="#typeName#_#FlagID#_#entityID#charsleft" id="#typeName#_#FlagID#_#entityID#charsleft"></p>
						</cfif>
					</cfoutput>
					</cfsavecontent>
			<cfelseif result.flagFormatting.displayas is "grid">
					<cfset flagList = listdeleteat (flagList,listfindnocase(flagList,"#typeName#_#FlagID#"))>
					<cfset flaglist = flaglist & ",flag_#FlagID#">  <!--- this is needed to get it processed as a complex flag --->

					<CFPARAM name="requiredMessage" default="">
					<cfif  requiredMessage is "">
						<cfset  requiredMessage = "phr_Sys_EnterARowFor #flagName#">
					</cfif>
					<cfsavecontent variable="result.control_html">
						<cf_complexflaggrid FlagID = #flagid# flagDataQuery = #result.FlagData# entityid = #entityID# method = "edit" attributeCollection = #result.flagFormatting# required = "#required#" requiredMessage = "#requiredMessage#">
					</cfsavecontent>
					<cfif complexflaggrid.javascriptVerification is not "">
						<cfset javascriptVerification = complexflaggrid.javascriptVerification & javascriptVerification >
					</cfif>

			<cfelse>

					<cfsavecontent variable="result.control_html">
					<cfoutput>
					<!--- The Field --->
					<cfif structKeyExists(result.flagFormatting,"mask") and result.flagFormatting.mask is not "">
						<cfset attributeStruct.onkeyup="mask_onValueChanged();">
						<cfset attributeStruct.onfocus="mask_onSetFocus(this, '#result.flagFormatting.mask#');">
						<cfset attributeStruct.onblur="mask_onKillFocus();">
					</cfif>
					<cfif result.flagFormatting.required>
						<cfset attributeStruct.required="1">
						<cfset attributeStruct.message = "#result.label# phr_sys_formvalidation_req">
					</cfif>
					<!--- WAB 2015-04-20 Added Label attribute (used by rwFormValidation) --->
					<cf_input class="#result.flagFormatting.class#" type="text" id = "#result.fieldname#" name="#result.fieldname#" disabled="#IIF(method is 'edit',false,true)#" value="#htmlEditFormat(result.FlagData.Data)#" SIZE="#result.flagFormatting.size#" MAXLENGTH="#result.flagFormatting.maxlength#" attributeCollection=#attributeStruct# label="#result.label#">
					<cfif method is "edit">
						<!--- _Required Hidden Field --->
						<cfif result.flagFormatting.required >
							<CF_INPUT type="Hidden" name="#result.fieldname#_required" label="#flagStructure.translatedName#" value="#flagStructure.translatedName# phr_sys_formvalidation_req">
						</cfif>
						<!--- The original Value _orig --->
						<cf_input type="Hidden" name="#result.fieldname#_orig" value="#result.flagData.data#">
					</cfif>
					<!--- mask initialisation Code --->
					<cfif structKeyExists (result.flagFormatting,"mask") and result.flagFormatting.mask is not "">
						<cf_includejavascriptonce template="/javascript/masks.js">
						<script>$('#typeName#_#FlagID#_#entityid#').focus()</script>
					</cfif>

					</cfoutput>
					</cfsavecontent>

			</cfif>

		<cfelseif listFindNoCase("integer,decimal,financial",typeName)>	<!--- 2014-08-11  RPW Create new profile type: "Financial" --->

			<cfif flagStructure.usevalidvalues>
				<cfset validValues = application.com.relayForms.getValidValues (fieldName="flag_#flagStructure.flagtextid#",countryid = countryid,entityid = argumentCollection.entityID)>
				<cfif result.flagFormatting.displayAs is ""><cfset result.flagFormatting.displayAs = "Select"></cfif>
				<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=result.flagFormatting,query=validvalues,   value = "dataValue", display = "displayValue", name="#result.fieldName#", selected = result.flagData.data, keepOrig=true,label=result.label,method=method,shownull = true)>

			<CFELSE>

				<cfinclude template="/templates/relayFormJavascripts.cfm">
				<cfscript>
					// 2014-08-11  RPW Create new profile type: "Financial"
					var extendedParameters = {};
					switch(typeName)
					{
						case "decimal":
							charType = "decimal";
							break;
						case "financial":
							charType = "financial";
							extendedParameters.currencySign = application.com.screens.getCurrencies(currencyISOCode=flagStructure.FlagTypeCurrency).currencySign[1];
							extendedParameters.FlagTypeDecimalPlaces = flagStructure.FlagTypeDecimalPlaces;
							extendedParameters.FlagTypeCurrency = flagStructure.FlagTypeCurrency;
							extendedParameters.countryId = arguments.countryId;
							extendedParameters.currencyISOCode = result.flagData.currencyISOCode;
							break;
						default:
							charType = "numeric";
							break;
					}
				</cfscript>
				<cfsavecontent variable="result.control_html">
					<!--- 	WAB 2014-12-16 CASE 442889 Add support for size and maxlength attributes
							WAB 2015-10-06 Jira PROD2015-41 Added support of range attribute (which all numeric flags should have by default)
					--->
					<cfif method is "edit">
						<!--- 2012-08-16 PPB Case 430059 added required attribute --->
						<!--- 2014-08-11  RPW Create new profile type: "Financial" --->
						<CF_relayValidatedField
							fieldName="#result.fieldname#"
							currentValue="#result.flagData.data#"
							charType="#charType#"
							size="#result.flagFormatting.size#"
							maxLength="#result.flagFormatting.maxLength#"
							helpText=""
							required =  "#result.flagFormatting.required#"
							requiredMessage = "#flagStructure.translatedName# phr_sys_formvalidation_req"
							label = #flagStructure.translatedName#
							extendedParameters = #extendedParameters#
							range = #result.flagFormatting.range#
						>
							<cfoutput><CF_INPUT type="Hidden" name="#result.fieldname#_orig" value="#result.flagData.data#"></cfoutput>

							<!--- 2012-08-16 PPB Case 430059 added _Required Hidden Field (not actually used so commented out but left in because the re-write may use the _required convention)
							<cfif result.flagFormatting.required >
								<cfoutput><CF_INPUT type="Hidden" name="#result.fieldname#_required" label="#flagStructure.translatedName#" value="#flagStructure.translatedName# phr_sys_formvalidation_req"></cfoutput>
							</cfif>
 							--->

						<!--- WAB 2007-02-21
							max length reduced to 9 to prevent overflow of integer data field
							could do something cleverer with JS, but this should do
							--->
					<cfelse>
						<!--- TBD, put in nice field --->
						<!--- 2015-05-01 AXA added extended parameters to do pass in decimal formatting to cf_input --->

						<cfif compareNoCase(charType,'financial') EQ 0>
							<!--- START 2015-05-01 AXA copied over code from RelayValidatedField to apply field formatting and display elements to Financial flags and decimal fields --->
							<!--- 2015/12/15 GCC 446885 changed to pass in currency as well - needed for UD - user defined currencies --->
							<cfset variables.financialHTMLTag = formatFinancialFlagDataHTML(flagValue=result.flagData.data,flagCurrency=result.flagData.currencyISOCode, flagStructure=flagStructure, fieldName=result.fieldname, countryID=arguments.countryId)>
							<cfif FindNoCase(charType,"financial") AND request.relayFormDisplayStyle EQ "HTML-div"><div></cfif>
							<cfoutput>#variables.financialHTMLTag.fieldPrefix#</cfoutput>
							<cfoutput><cfif FindNoCase(charType,"financial") AND request.relayFormDisplayStyle EQ "HTML-div"><div class="form-group col-lg-#variables.financialHTMLTag.inputWidth#"></cfif></cfoutput>
							<!--- END 2015-05-01 AXA copied over code from RelayValidatedField to apply field formatting and display elements to Financial flags and decimal fields --->
							<cfoutput><cf_input type="text" disabled=true class="form-control #variables.financialHTMLTag.class#" value = "#variables.financialHTMLTag.currentValue#"></cfoutput>
							<cfif FindNoCase(charType,"financial") AND request.relayFormDisplayStyle EQ "HTML-div"></div></div></cfif>
						<cfelse>
							<cfoutput><cf_input type="text" class="form-control" disabled=true value = "#result.flagData.data#" charType="#charType#" extendedParameters = #extendedParameters#></cfoutput>
						</cfif>
					</cfif>
				</cfsavecontent >



			</CFIF>

		<!--- 	WAB 2016-09-21 PROD2016-2349 'Select A Value' appearing in MultiSelects, set showNull = false on both integer and text multiple --->
		<cfelseif typeName eq "integermultiple">

			<cfset validValues = application.com.relayForms.getValidValues (fieldName="flag_#flagStructure.flagtextid#", countryid = countryid, entityid = argumentCollection.entityID )>
			<cfif result.flagFormatting.displayAs is ""><cfset result.flagFormatting.displayAs = "twoselects"></cfif>
			<cfif not structKeyExists (result.flagFormatting,"ListSize") or result.flagFormatting.ListSize is ""><cfset result.flagFormatting.ListSize = 4></cfif>
			<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=result.flagFormatting,query=validvalues,   value = "dataValue", display = "displayValue", name="#result.fieldName#", selected = valuelist(result.flagData.data), keepOrig=true,label=result.label,method=method,shownull = false, required = result.flagFormatting.required)>

		<!--- NJH 2012/12/04 Roadmap2013 - support for multiple text flags --->
		<cfelseif typeName eq "textmultiple">
			<cfif flagStructure.useValidValues>
				<cfset validValues = application.com.relayForms.getValidValues (fieldName="flag_#flagStructure.flagtextid#", countryid = countryid, entityid = argumentCollection.entityID )>
				<cfif result.flagFormatting.displayAs is ""><cfset result.flagFormatting.displayAs = "twoselects"></cfif>
				<cfset tempFlagData = result.flagData>  <!--- 'cause can't do valueList(result.flagData.data) --->
				<cfif not structKeyExists (result.flagFormatting,"ListSize") or result.flagFormatting.ListSize is ""><cfset result.flagFormatting.ListSize = 4></cfif>
				<cfset result.control_html  = application.com.relayforms.validValueDisplay (argumentcollection=result.flagFormatting,query=validvalues,   value = "dataValue", display = "displayValue", name="#result.fieldName#", selected = valuelist(tempFlagData.data), keepOrig=true,label=result.label,method=method, shownull = false, required = result.flagFormatting.required)>
			<cfelse>
				<cfsavecontent variable="result.control_html">
					<cfoutput>
						<!--- WAB 2014-11-12 took out the charsLeft javascript because it wasn't working (the js had been changed) and there wasn't an element to show the message in anyway
											could have added the <p> tag but infact for a text multiple, the total number of characters is not important - it is the number of characters per line (or between commas)
						 --->
						<textarea class="#result.flagFormatting.class#" name="#result.fieldname#" <cfif arguments.method eq "view">readonly</cfif> cols="#result.flagFormatting.cols#" rows="#result.flagFormatting.rows#" wrap="VIRTUAL">#HTMLEditFormat(trim(valueList(result.FlagData.Data)))#</textarea>
						<cfif arguments.method neq "view"><cf_input type="Hidden" name="#result.fieldname#_orig" value="#HTMLEditFormat(trim(valueList(result.FlagData.Data)))#"></cfif>
					</cfoutput>
				</cfsavecontent>
			</cfif>


		<cfelseif typeName is "date">
			<!--- WAB 2012-02-22 added date controls
				WAB 2012-11-19 CASE 431750.  Added attributeCollection - required parameter was not getting passed through
				WAB 2014-02-25 CASE 439006 (During Safenet implementation) Added view mode for the calendar.  The relayDateDropDowns tag doesn't support it yet
				WAB 2015-06-22 FIFTEEN-384 Add local time support to date flags
			--->

			<cfif method is "view">
				<cfset result.flagFormatting.readonly = true>
			</cfif>

			<cfparam name="result.flagFormatting.showTime" default="false">
			<cfparam name="result.flagFormatting.showAsLocalTime" default="#result.flagFormatting.showTime#">

			<cfsavecontent variable="result.control_html">
					<cfif result.flagFormatting.displayas is "calendar">
						<CF_relayDateField
								currentValue="#result.flagData.data#"
								fieldName="#result.fieldname#"
								anchorName="anchor_#result.fieldname#"
								helpText=""
								attributeCollection = #result.flagFormatting#
							>

							<cfif method is "edit">
								<!--- _Required Hidden Field --->
								<cfif result.flagFormatting.required >
									<cfoutput><CF_INPUT type="Hidden" name="#result.fieldname#_required" label="#flagStructure.translatedName#" value="#flagStructure.translatedName# phr_sys_formvalidation_req"></cfoutput>
								</cfif>
							</cfif>

					<cfelse>
							<!--- may need to deal with required message --->
							<CF_relayDateDropDowns
							currentValue="#result.flagData.data#"
							fieldName="#result.fieldname#"
							attributeCollection = "#result.flagFormatting#"
							>
					</cfif>

					<!--- 2013-11-11 CASE 436913 (Xirrus Opportunities) Add Support for Time Field --->
					<cfif result.flagFormatting.showTime>
						<cf_relaytimefield
							currentValue="#result.flagData.data#"
							fieldName="#result.fieldname#"
							attributeCollection = #result.flagFormatting#
						>
					</cfif>


					<cfif method is "edit">
						<!--- The original Value _orig --->
						<!--- START: 2012/03/12 PPB P-AVD001 the _orig variable needed CFOUTPUT tag around it; I split out the IIF to avoid syntax issue --->
						<cfif (result.FlagData.RecordCount IS NOT 0 and not isdefined('flag_#trim(flagstructure.flagtextid)#_default'))>
							<cfset origValue = createODBCDateTime(result.flagData.data)>
						<cfelse>
							<cfset origValue = "">
						</cfif>
						<cf_input type="Hidden" name="#result.fieldname#_orig" value="#origValue#">
						<!--- END: 2012/03/12 PPB P-AVD001 --->
					</cfif>
			</cfsavecontent>

		</cfif>

		<cfif method is "edit">
			<cfset result.control_html = result.control_html & '<INPUT type="hidden" name="frm#flagStructure.entityType.tablename#FlagList" value="#result.fieldroot#">'>
		</cfif>


		<cfreturn result>
		</cffunction>

		<cffunction name="getFieldNameAndRootForFlag" output="no">
			<cfargument name="flagStructure" >
			<cfargument name="entityID" >

			<cfset var result = {type = flagStructure.flagType.name}>

			<cfif listfindnocase("radio,checkbox",result.type)>
				<cfset result.root = "#result.type#_#flagStructure.FlagGroupID#">
			<cfelse>
				<cfset result.root = "#result.type#_#flagStructure.FlagID#">
			</cfif>

			<cfset result.name = result.root & "_#arguments.EntityID#">

			<cfreturn result>

		</cffunction>


		<cffunction name="deleteFlagGroupData" output="yes">
		      <cfargument name="flagGroupId" required="Yes" type="string">
		      <cfargument name="entityid" required="no" default = "0" >
			  <cfargument name="updatedbyPersonID" type="numeric" default="#isDefined('request.relaycurrentuser.personID')?request.relaycurrentuser.personID:404#">
			  <cfargument name="updatedbyUserGroupID" type="numeric" default="#isDefined('request.relaycurrentuser.usergroupid')?request.relaycurrentuser.usergroupid:404#">


		      <cfset var theFlagGroup = getFlagGroupStructure(arguments.flaggroupid)>

		      <!--- NJH case 433498	 - added lastupdatedByPerson --->
		      <cfquery DATASOURCE="#application.SiteDataSource#">
		            update #theFlagGroup.flagType.datatablefullname#
		           		set lastupdatedby = <cf_queryparam value="#arguments.updatedbyUserGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		           			lastUpdated = getDate(),
		           			LastUpdatedByPerson =<cf_queryparam value="#arguments.updatedbyPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		            from #theFlagGroup.flagType.datatablefullname# fd
		                  inner join flag f on f.flagid = fd.flagid
		            where
		                  f.flagGroupID =  <cf_queryparam value="#theflagGroup.flagGroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
		                  AND entityID  IN ( <cf_queryparam value="#arguments.entityid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

		            Delete
					#theFlagGroup.flagType.datatablefullname#
					from #theFlagGroup.flagType.datatablefullname# fd
		                  inner join flag f on f.flagid = fd.flagid
		            where
		                  f.flagGroupID =  <cf_queryparam value="#theflagGroup.flagGroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
		                  AND entityID  IN ( <cf_queryparam value="#arguments.entityid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		      </cfquery>


		</cffunction>

	<cffunction access="public" name="processFlagForm">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="string" required="true">
		<cfargument name="formEntityID" type="string" default="#entityID#">	<!--- for new records when the form is created before the id was known --->

		<cfset var flagFieldRoot = "">
		<cfset var flagFieldName = "">
		<cfset var flagFieldType = "">
		<cfset var UpdateTime = createodbcdatetime(request.requesttime)>
		<cfset var flagFieldName_Orig = '' />
		<cfset var flagValue = '' />
		<cfset var flagValue_orig = '' />
		<cfset var thisFlagID = '' />
		<cfset var flagStructure = '' />
		<cfset var flagtablename = '' />
		<cfset var counter = '' />
		<cfset var flagType = '' />
		<cfset var thisDate = '' />
		<cfset var tmpData = '' />
		<cfset var thisData = '' />
		<cfset var DeleteFlags = '' />
		<cfset var AddFlags = '' />
		<cfset var UpdateFlags = '' />
		<cfset var querySnippetSQL = '' />
		<cfset var dataType = "">
		<cfset var currencyISOCodeEntityID = "">
		<cfset var currencyISOCode = "">

		<cfif structKeyExists (form,"frm#entityType#FlagList")>

			<!--- loop through flags on page for this entity--->
			<CFLOOP index="flagFieldRoot" list="#form["frm#entityType#FlagList"]#">
				<cfset flagFieldName = flagFieldRoot & "_#formEntityID#">
				<cfset flagFieldName_Orig = flagFieldName & "_orig">
				<cfset flagFieldType = listfirst(flagFieldName,"_")>

				<CFIF flagFieldType is NOT "Flag">
					<!--- regular flags --->
					<CFPARAM name="form[flagFieldName]" default="">  <!--- if nothing selected then the field will not be defined --->

					<CFIF structKeyExists(form,"#flagFieldName#_orig")>
						<cfset flagValue = form[flagFieldName]>
						<cfset flagValue_orig = form[flagFieldName_orig]>

						<!---
						WAB 2015-06-22 During FIFTEEN-384 (adding time to date flags)
						This now moved to applicationMain.doctorFormFields
						<cfif flagFieldType is "date">
							<cfset flagValue = application.com.dateFunctions.combineDateFormFields(baseVariableName = "#flagFieldName#", scope="form")>
						</cfif>
						--->
						<CFIF compare(flagValue, flagValue_orig) IS NOT 0>

							<CFIF flagFieldType IS "checkbox" or flagFieldType IS "radio">

								<cfset setFlagGroupData(flagGroupID = ListGetAt(flagFieldName,2,"_"),entityid = entityID, flagIDList = flagValue, flagIDList_Orig = flagValue_orig )>

							<!--- NJH 2012/12/04 Roadmap2013 - support for multiple text flags --->
							<CFELSEIF listFindNoCase("IntegerMultiple,textMultiple",flagFieldType)>

								<!--- remove any line breaks and spaces --->
								<cfset flagValue = reReplace(flagValue,chr(10),",","ALL")>
								<cfset flagValue = reReplace(flagValue,",\s*,",",","ALL")>

								<cfset flagStructure = application.com.flag.getflagstructure(ListGetAt(flagFieldName,2,"_"))>
								<CFSET thisFlagID=flagStructure.flagID>
								<cfset flagtablename = flagStructure.flagType.datatablefullname>
								<!--- delete all items which are no longer in the list
								WAB 2009/10/07 altered to do an update first
								--->

								<cfsaveContent variable="querySnippetSQL">
									<cfoutput>
										#flagtablename#
											WHERE EntityID = <cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
											AND FlagID = <cf_queryparam value="#thisFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
											<CFIF flagValue is not "">
											AND data NOT IN (<cf_queryparam value="#flagValue#" CFSQLTYPE="#flagStructure.flagType.cfsqlType#" list="true">)
											</CFIF>
										 	<CFIF flagValue_orig is not "">
											AND data IN (<cf_queryparam value="#flagValue_orig#" CFSQLTYPE="#flagStructure.flagType.cfsqlType#" list="true">)
											</CFIF>
									</cfoutput>
								</cfsaveContent >

								<!--- NJH case 433498	 - added lastupdatedByPerson --->
								<CFQUERY NAME="DeleteFlags" DATASOURCE="#application.sitedatasource#">
									Update <cf_queryObjectName value="#flagtablename#">
										set lastUpdated = getdate(),
											lastUpdatedBy = #request.relayCurrentUser.usergroupid#,
											lastUpdatedByPerson = #request.relayCurrentUser.personID#
									FROM
									#preserveSingleQuotes(querySnippetSQL)#

									DELETE
										FROM
									#preserveSingleQuotes(querySnippetSQL)#
								</CFQUERY>

								<!--- add all items which are now in the list (and weren't before)--->
								<CFSET counter=0>


								<CFLOOP index="thisData" list="#flagValue#" >
									<CFSET counter=counter+1>
									<cfset thisData  = trim(thisData)>

									<cfif thisData neq "">
										<CFIF listfindnocase(flagValue_orig,thisData) is 0 >
											<CFQUERY NAME="AddFlags" DATASOURCE="#application.sitedatasource#">
											INSERT INTO <cf_queryObjectName value="#flagtablename#">
											(EntityID, FlagID, Data, sortorder, CreatedBy, Created, LastUpdatedBy, LastUpdated,LastUpdatedByPerson)
											SELECT
												<cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
												<cf_queryparam value="#thisFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
												<cf_queryparam value="#thisData#" CFSQLTYPE="#flagStructure.flagType.cfsqlType#" >,
												<cf_queryparam value="#counter#" CFSQLTYPE="CF_SQL_INTEGER" >,
												<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
												<cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
												<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
												<cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
												<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
											WHERE NOT EXISTS (
																SELECT 1 FROM  <cf_queryObjectName value="#flagtablename#">
																WHERE 	FlagID =  <cf_queryparam value="#thisFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
																		and EntityID =  <cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
																		and data = <cf_queryparam value="#thisData#" CFSQLTYPE="#flagStructure.flagType.cfsqlType#" >)
											</CFQUERY>
										<CFELSE>
											<!--- these ones need to have the sort order updated (could have deleted and updated) --->
											<CFQUERY NAME="UpdateFlags" DATASOURCE="#application.sitedatasource#">
											UPDATE
												<cf_queryObjectName value="#flagtablename#">
											set
												sortOrder =  <cf_queryparam value="#counter#" CFSQLTYPE="cf_sql_integer" >,
												lastupdatedby = #request.relayCurrentUser.usergroupid#,
												lastUpdated =  <cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
												lastupdatedbyPerson = #request.relayCurrentUser.personID#
											where
													flagID =  <cf_queryparam value="#thisFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
												and EntityID =  <cf_queryparam value="#EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
												and data = <cf_queryparam value="#thisData#" CFSQLTYPE="#flagStructure.flagType.cfsqlType#">
											</CFQUERY>
										</CFIF>
									</cfif>
								</CFLOOP>


								<CFELSEIF flagFieldType IS NOT "Group">

									<CFSET thisFlagID=ListGetAt(flagFieldName,2,"_")>
									<!--- 2014-09-24 WAB CASE 440960 we were not dealing with deleting flags when flagvalue blank (hidden error on TextFlags but error thrown on integerFlags) --->
									<cfif flagValue is not "">
										<!--- YMA 2015/07/06 Support for saving financial flag data --->
										<cfif flagFieldType IS "Financial">
											<cfif isnumeric(formEntityID)>
												<cfset currencyISOCodeEntityID = formEntityID + 1>
											<cfelse>
												<cfset currencyISOCodeEntityID = "1">
											</cfif>
											<cfif structKeyExists(form,flagFieldRoot & "_#currencyISOCodeEntityID#")>
												<cfset currencyISOCode = form[flagFieldRoot & "_#currencyISOCodeEntityID#"]>
											</cfif>
											<cfset setFlagData (flagid = thisflagid,entityid = entityid, data = flagValue, currencyISOCode = currencyISOCode)>
										<cfelse>
											<cfset setFlagData (flagid = thisflagid,entityid = entityid, data = flagValue)>
										</cfif>
									<cfelse>
										<cfset deleteFlagData (flagid = thisflagid,entityid = entityid)>
									</cfif>

								</CFIF>


						</cfif>

					</cfif>

				<CFELSE>
						<!--- **************************************
								complex flags
							 ************************************** --->

					<CFINCLUDE template="updateData_complexFlags.cfm">


				</cfif><!--- end of complex flags --->


			</cfloop><!--- end of loop through flags --->

		</cfif>

	</cffunction>

	<!--- 	2012/03/07 PPB P-REL115 API
			2015-11-12 AHL P-ANA001 Missing Unique ID initialization

			Note that getJoinInfoForFlag() and getJoinInfoForFlagGroup() do similar things and this function ought probably call those functions (which have more compact joins)
	--->
	<cffunction access="public" name="getFlagQuerySnippets" hint="Gets a join and a select statement for building queries which link the Flag Tables." returns="struct">
		<cfargument name="textIDList" required="true">	 			<!--- list of flagTextIds and/or flagGroupTextIds --->
		<cfargument name="uniqueKeyAliasList" required="false">
		<!---
			we MAY need to send in a list of aliases (which are specific to the calling program) corresponding to the list of flags
			if the textIDList = "orgFlag1,persFlag1,locFlag1,persFlag2" the uniqueKeyAliasList may be "o,p,l,p"

			IMPORTANT: use preserveSingleQuotes in the join of the calling programs CFQUERY eg. #preserveSingleQuotes(flagQuerySnippets.join)#
		 --->

		<cfset var listItem = "">
		<cfset var loopCount = 0>
		<cfset var flagType = "">
		<cfset var uniqueKeyPrefix = "">
		<cfset var flagGroupStruct = "">
		<cfset var flagStruct = "">
		<cfset var uniqueKey = "">
		<cfset var result = structNew()>
		<cfset result.select = "">
		<cfset result.join = "">

		<cfloop list="#textIDList#" index="listItem">
			<cfset loopCount = loopCount + 1>

			<cfset flagType = "">
			<cfset uniqueKeyPrefix = "">

			<cfset flagGroupStruct = application.com.flag.getFlagGroupStructure('#listItem#')>

			<cfif flagGroupStruct.isOk>
				<cfset flagType = flagGroupStruct.flagType.name>
				<cfset uniqueKey = uniqueKeyPrefix & application.flagGroup['#listItem#'].entityType.uniqueKey>
			<cfelse>
				<cfset flagStruct = application.com.flag.getFlagStructure('#listItem#')>
				<cfif flagStruct.isOk>
					<cfset flagType = flagStruct.flagGroup.flagType.name>

					<!--- check if the uniquekey to be used (eg "organisationId") has a table alias sent in (eg "o") to prepare a prefix (eg "o.") --->
					<cfif StructKeyExists(arguments,"uniqueKeyAliasList")>
						<cfset uniqueKeyPrefix = ListGetAt(uniqueKeyAliasList,loopCount)>
						<cfif uniqueKeyPrefix neq "">
							<cfset uniqueKeyPrefix = uniqueKeyPrefix & ".">
						</cfif>
					</cfif>
					<cfset  uniqueKey = uniqueKeyPrefix & application.flag['#listItem#'].entityType.uniqueKey>
				</cfif>
			</cfif>

			<cfswitch expression="#flagType#">
				<cfcase value="Checkbox">
					<cfset result.select = result.select & ", CASE WHEN (bfd_#listItem#.entityID IS NULL) THEN 0 ELSE 1 END AS #listItem#" >
					<cfset result.join = result.join  & " left outer join (booleanFlagData bfd_#listItem# with (noLock) inner join flag f_#listItem# with (noLock) on f_#listItem#.flagId = bfd_#listItem#.flagId and f_#listItem#.flagTextId = '#listItem#') on bfd_#listItem#.entityID = #uniqueKey#">
				</cfcase>

				<cfcase value="Radio">
					<cfset result.select = result.select & ", f_#listItem#.flagTextId AS #listItem#" >
					<cfset result.join = result.join  & " left outer join (booleanFlagData bfd_#listItem# with (noLock) inner join flag f_#listItem# with (noLock) on f_#listItem#.flagId = bfd_#listItem#.flagId inner join flaggroup fg_#listItem# with (noLock) on fg_#listItem#.flagGroupId = f_#listItem#.flagGroupId and fg_#listItem#.flagGroupTextId = '#listItem#') on bfd_#listItem#.entityID = #uniqueKey#">
				</cfcase>

				<!--- 	NJH 2016/03/16 - modified so that text, date and integer flags all use the same query snippet... had to include date as well.
						WAB 2016-06-08	BF-973 modified to be just defaultcase so picks up decimal flags etc as well.
										originally this was a cfif statement with a cfelse.  Not sure where that went, but we are effectively back to it now
										May give slightly odd query with multiple flags
				--->
				<cfdefaultcase>
					<cfset result.select = result.select & ", fd_#listItem#.data AS #listItem#" >
					<cfset result.join = result.join  & " left outer join (#flagType#FlagData fd_#listItem# with (noLock) inner join flag f_#listItem# with (noLock) on f_#listItem#.flagId = fd_#listItem#.flagId and f_#listItem#.flagTextId = '#listItem#') on fd_#listItem#.entityID = #uniqueKey#">
				</cfdefaultcase>

			</cfswitch>

		</cfloop>

		<cfreturn result>

	</cffunction>

	<!--- 2015-03-06	RPW	FIFTEEN-267 - Custom Entities - Added ability to format financial flags - Functions FormatFlagData and FormatFlagDataFinancial --->
	<cfscript>
		public string function FormatFlagData
		(
			required numeric flagID,
			required string flagValue,
			string secondaryData=""
		)
		{
			var returnValue = arguments.flagValue;

			if (Len(returnValue)) {

				var flagStructure = application.com.flag.getFlagStructure(flagID=arguments.flagID);
				switch(flagStructure.flagType.name) {
					case "Financial":
						returnValue = FormatFlagDataFinancial(
							flagStructure=flagStructure,
							flagValue=arguments.flagValue,
							secondaryData=arguments.secondaryData
						);
						break;
				}

			}

			return returnValue;

		}


		public string function FormatFlagDataFinancial
		(
			required struct flagStructure,
			required string flagValue,
			string secondaryData=""
		)
		{

			var returnValue = arguments.flagValue;

			var currency = arguments.flagStructure.flagTypeCurrency;
			if (Len(arguments.secondaryData)) {
				currency = arguments.secondaryData;
			}
			var getCurrency = application.com.currency.getCurrencyByISOCode(currency);
			var currencySign = getCurrency.currencySign[1];

			if (IsNumeric(arguments.flagValue)) {

				if (IsDefined("arguments.flagStructure.FlagTypeDecimalPlaces") && IsNumeric(arguments.flagStructure.FlagTypeDecimalPlaces)) {
					var decimalPlaces = arguments.flagStructure.FlagTypeDecimalPlaces;
				} else {
					var decimalPlaces = 0;
				}

				var mask = "";
				if (decimalPlaces) {

					mask &= ".";

					for (var p=0;p < decimalPlaces;p++) {
						mask &= "_";
					}

					returnValue = NumberFormat(arguments.flagValue,mask);

				} else {

					returnValue = Round(arguments.flagValue);

				}

				returnValue = currencySign & returnValue;
			}

			return returnValue;
		}
	</cfscript>
	<!--- 2015/12/15 GCC 446885 changed to pass in currency as well - needed for UD - user defined currencies --->
	<cffunction name="formatFinancialFlagDataHTML" access="public" hint="Apply number formatting to financial field flag and generates a currency display" output="false" returntype="struct">
		<cfargument name="flagValue" type="string" required="yes" />
		<cfargument name="flagCurrency" type="string" required="yes" />
		<cfargument name="flagStructure" type="struct" required="yes" />
		<cfargument name="fieldName" type="string" required="true" />
		<cfargument name="countryId" type="numeric" required="false" default="0" />
		<cfset var result = {} />
		<cfset result.fieldPrefix = "" />
		<cfset result.currentValue = arguments.flagValue />
		<cfscript>
		local.mask = "";
		local.currencySign = application.com.screens.getCurrencies(currencyISOCode=arguments.flagStructure.FlagTypeCurrency).currencySign[1];


		if (structKeyExists(local,"currencySign")) {
			if (request.relayFormDisplayStyle == "HTML-div") {
				result.fieldPrefix = '<div class="form-group col-lg-2"><input disabled type="text" name="dummyFinancial" class="form-control" value="' & local.currencySign & '" /></div>';
			} else {
				result.fieldPrefix = local.currencySign;
			}
		}

		result.class = " right";
		result.inputWidth = 10;

		//2014-09-08	RPW	Financial Flag - Error in selections and downloads
		if (IsDefined("arguments.flagStructure.FlagTypeDecimalPlaces") && IsNumeric(arguments.flagStructure.FlagTypeDecimalPlaces)) {
			local.decimalPlaces = arguments.flagStructure.FlagTypeDecimalPlaces;
		} else {
			local.decimalPlaces = 0;
		}

		if (local.decimalPlaces) {
			local.pattern = "^[0-9]+(\.\d{1," & local.decimalPlaces & "})?$";

			if (IsNumeric(arguments.flagValue)) {
				local.mask &= ".";

				for (local.p=0;local.p < local.decimalPlaces;local.p++) {
					local.mask &= "_";
				}

				result.currentValue = NumberFormat(arguments.flagValue,local.mask);
			}

		} else {

			if (IsNumeric(arguments.flagValue)) {
				result.currentValue = Round(attributes.flagValue);
			}
		}

		if (IsDefined("arguments.flagStructure.FlagTypeCurrency")) {
			if (arguments.flagStructure.FlagTypeCurrency=="UD") {

				result.inputWidth = 8;
				// 2015/12/15 GCC 446885 changed to pass in currency as well - needed for UD - user defined currencies
				if (Len(arguments.flagCurrency)) {
					local.defaultCurrency = arguments.flagCurrency;
				} else {
					local.countryISOCodeList = application.com.relayCountries.getCountryDetails(countryID=arguments.countryId).CountryCurrency;
					local.defaultCurrency = ListFirst(local.countryISOCodeList);
				}

				local.getCurrencies = application.com.screens.getCurrencies();

				local.fieldNameIdent = ListLast(arguments.fieldName,"_");

				if (IsNumeric(local.fieldNameIdent)) {
					local.fieldNameIdent++;
				} else {
					local.fieldNameIdent = 1;
				}

				local.preSelectName = ListDeleteAt(arguments.fieldName,3,"_") & "_" & local.fieldNameIdent;

				if (request.relayFormDisplayStyle == "HTML-div") {
					result.fieldPrefix = '<div class="form-group col-lg-5">';
				} else {
					result.fieldPrefix = "";
				}

				result.fieldPrefix &= '<select name="' & local.preSelectName & '" class="form-control">';

				for (local.c=1;local.c <= local.getCurrencies.recordCount;local.c++) {
					local.sel = "";
					if (local.getCurrencies.currencyISOCode[local.c]==local.defaultCurrency) {
						local.sel = " selected";
					}
					result.fieldPrefix &= '<option value="' & local.getCurrencies.currencyISOCode[local.c] & '"' & local.sel & '>' & local.getCurrencies.currencySign[local.c] & ' ' & local.getCurrencies.currencyName[local.c] & '</option>';
				}

				result.fieldPrefix &= "</select>";

				if (request.relayFormDisplayStyle == "HTML-div") {
					result.fieldPrefix &= '</div>';
				}
			}
		}


		</cfscript>
		<cfreturn result />
	</cffunction>

    <!--- start: 446369 --->
    <cffunction name="getFlagIdByFlagTextId" returntype="numeric" hint ="Returns flag id for the given flagTextId or 0 if flag not found" output="no">
        <cfargument name="flagTextId" required="Yes" type="string">

        <cfset var flagStruct = getFlagStructure(flagId = arguments.flagTextId)>
        <cfif flagStruct.isOK and structKeyexists(flagStruct, "flagID")>
            <cfreturn flagStruct.flagID>
        </cfif>

        <cfreturn 0>
    </cffunction>
    <!--- end: 446369 --->


</cfcomponent>


