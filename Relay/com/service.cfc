﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		service.cfc	
Author:			NJH  
Date started:	26-10-2012
	
Description:	Service Component to provide social interactions between RW and LinkedIn and Facebook		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012/10/15			NJH			Social CR - Added share image url.
2013/02/20			NJH			Item task 13 - pass through entityId and entityTypeID to the getProfile function, as we can now link non-person entityTypes (such as twitterAccounts)
2014-11-04			AHL			Case 441864 Activity Stream Issues
2016-06-29			WAB			During PROD2016-876.  Removed reference to request .encryption .encryptedfields.  Should not be necessary ('private variable')

Possible enhancements:


 --->


<cfcomponent output="false">

	<cffunction name="getService" access="public" returnType="struct">
		<cfargument name="serviceID" type="string" required="true">
		
		<cfset var result = {name="",serviceID=0,serviceTextID=""}>
		<cfset var getServiceID = "">
		
		<cfquery name="getServiceID" datasource="#application.siteDataSource#">
			select serviceID,serviceTextID,name,consumerKey,consumerSecret from service where 
			<cfif not isNumeric(arguments.serviceID)>serviceTextID = '#arguments.serviceID#'
			<cfelse>serviceID = #arguments.serviceID#
			</cfif>
		</cfquery>
		
		<cfset result.serviceID = getServiceID.serviceID>
		<cfset result.name = getServiceID.name>
		<cfset result.serviceTextID = getServiceID.serviceTextID>
		<cfset result.consumerKey = getServiceID.consumerKey>
		<cfset result.consumerSecret = getServiceID.consumerSecret>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="isServiceAvailableForContext" access="public">
		
	</cffunction>
	
	
	<cffunction name="linkEntityToService" access="public" result="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="0">
		<cfargument name="serviceEntityID" type="string" required="false">
		
		<cfset var thisService = getService(arguments.serviceID)>
		<cfset var thisServiceEntityID =  "">
		<cfset var result = {isOK=true,message="Link Established",ID=""}>
		<cfset var thisServiceID = thisService.serviceID>
		<cfset var createLink = "">
		<cfset var entityProfile = application.com[thisService.serviceTextID].getProfile(entityID=arguments.entityID,entityTypeID=arguments.entityTypeID,fieldlist="id,public-profile-url,picture-url").profile>	
		<cfset var mergeStruct  = {service=thisService.serviceTextID}>
		<cfset var updatePersonPictureURL = "">
		
		<cfif structKeyExists(arguments,"serviceEntityID")>
			<cfset thisServiceEntityID = arguments.serviceEntityID>
		<cfelse>
			<cfset thisServiceEntityID = entityProfile.ID>												
		</cfif>
		
		<cfif thisServiceEntityID neq "">
			<cfquery name="createLink" datasource="#application.siteDataSource#">
				if not exists(select 1 from serviceEntity where entityID=#arguments.entityID# and entityTypeID=#arguments.entityTypeID# and serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > )
					insert into serviceEntity (entityID,entityTypeID,serviceID,serviceEntityID,publicProfileURL,createdBy,lastUpdatedBy)
						values (#arguments.entityID#,#arguments.entityTypeID#,#thisServiceID#,'#thisServiceEntityID#','<cfif structKeyExists(entityProfile,"publicProfileURL")>#entityProfile.publicProfileURL#</cfif>',#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#)
				else
					update serviceEntity set
						serviceEntityID =  <cf_queryparam value="#thisServiceEntityID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
						<cfif structKeyExists(entityProfile,"publicProfileURL")>publicProfileURL='#entityProfile.publicProfileURL#',</cfif>
						lastUpdated=GetDate(),
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#
					where entityID=#arguments.entityID# and entityTypeID=#arguments.entityTypeID# and serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			
			<cfif thisService.serviceTextID eq "linkedIn" and arguments.entityTypeID eq 0>
				<cfset application.com.relayCurrentUser.setPersonImage(pictureUrl=entityProfile.pictureURL)>
			</cfif>
			<cfset result.Id = thisServiceEntityID>
			
			<cftry>
				<cfif not request.relayCurrentUser.isInternal>
					
					<cfif structKeyExists(form,"frmDoNotShareLink#thisService.serviceTextID#Account")>
						<cfset application.com.flag.setBooleanFlag(flagTextID="optOutShare#thisService.serviceTextID#_accountMgt",entityID=request.relayCurrentUser.personID)>
					</cfif>
					
					<!--- <cfif application.com.settings.getSetting("socialMedia.share.person.linked")> --->
						<cfset application.com.service.addRelayActivity(action="linked",performedOnEntityID=arguments.entityID,performedOnEntityTypeID=arguments.entityTypeID,mergeStruct=mergeStruct,performedByEntityID=arguments.entityID,sendShare=true)>
					<!--- </cfif> --->
				</cfif>
				
				<cfcatch>
					<!--- <cfset result.isOK = false>
					<cfset result.message = "Unable to Establish Link"> --->
				</cfcatch>
			</cftry>
			
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Unable to Establish Link">
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="unlinkEntityFromService" access="public" result="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="0">
		
		<cfset var thisService = getService(arguments.serviceID)>
		<cfset var deleteLink = "">
		<cfset var deleteAccessToken = "">
		
		<cfquery name="deleteLink" datasource="#application.siteDataSource#">
			delete from serviceEntity where entityID=#arguments.entityID# and entityTypeID=#arguments.entityTypeID# and serviceID =  <cf_queryparam value="#thisService.serviceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif arguments.entityTypeID eq 0>
			<cfquery name="deleteAccessToken" datasource="#application.siteDataSource#">
				delete from serviceToken where entityID=#arguments.entityID# and entityTypeID=#arguments.entityTypeID# and serviceID =  <cf_queryparam value="#thisService.serviceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
		
	</cffunction>
	
	
	<cffunction name="hasEntityBeenLinked" access="public" result="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="0">
		
		<cfset var result = {isOK=true,message="Link Established",ID="",linkEstablished=true}>
		<cfset var thisService = getService(arguments.serviceID)>
		<cfset var doesLinkExist = "">
		
		<cfquery name="doesLinkExist" datasource="#application.siteDataSource#">
			select serviceEntityID from serviceEntity 
			where 
				entityID=#arguments.entityID# and 
				serviceID =  <cf_queryparam value="#thisService.serviceID#" CFSQLTYPE="CF_SQL_INTEGER" >  and 
				entityTypeID=#arguments.entityTypeID#
		</cfquery>
		
		<cfif doesLinkExist.recordCount>
			<cfset result.ID = doesLinkExist.serviceEntityID>
			<!--- <cfset result.linkEstablished = true>
			<cfif not doesLinkExist.linked>
				<cfset result.message = "#application.entityType[arguments.entityTypeID].tablename# has been unlinked from their #thisService.serviceTextID# account.">
			</cfif> --->
		<cfelse>
			<cfset result.message = "Link Not Established">
			<cfset result.linkEstablished = false>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getEntityServiceID" access="public" result="string" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="entityID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default="#application.entityTypeID.person#">
		
		<cfreturn hasEntityBeenLinked(argumentCollection=arguments).ID>
	</cffunction>
	
	
	<cffunction name="doesEntityExistWithServiceEntityID" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="ServiceEntityID" type="string" required="true">
		<cfargument name="entityTypeID" type="numeric" required="false">
		
		<cfset var result = {isOK=true,message="Link Established",entityID=0,entityTypeID=0,entityExists=true}>
		<cfset var thisServiceID = getService(arguments.serviceID).serviceID>
		<cfset var doesLinkExist = "">
		
		<cfquery name="doesLinkExist" datasource="#application.siteDataSource#">
			select entityID,entityTypeID from serviceEntity 
				where serviceEntityID =  <cf_queryparam value="#arguments.ServiceEntityID#" CFSQLTYPE="CF_SQL_VARCHAR" >  
				and serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				<cfif structKeyExists(arguments,"entityTypeID")> and entityTypeID=#arguments.entityTypeID#</cfif>
		</cfquery>
		
		<cfif doesLinkExist.recordCount>
			<cfset result.entityID = doesLinkExist.entityID>
			<cfset result.entityTypeID = doesLinkExist.entityTypeID>
		<cfelse>
			<cfset result.message = "Link Not Established">
			<cfset result.entityExists = false>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getServiceEntity" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="ServiceEntityID" type="string" required="true">
		
		<cfset var result = {isOK=true,serviceEntityID=0,entityID=0,publicProfileURL=""}>
		<cfset var thisServiceID = getService(arguments.serviceID).serviceID>
		<cfset var doesLinkExist = "">
		
		<cfquery name="local.getServiceEntityQry" datasource="#application.siteDataSource#">
			select serviceEntityID,entityID,publicProfileURL 
			from serviceEntity where 
				serviceEntityID =  <cf_queryparam value="#arguments.ServiceEntityID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and 
				serviceID =  <cf_queryparam value="#thisServiceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif local.getServiceEntityQry.recordCount>
			<cfset result.serviceEntityID = local.getServiceEntityQry.serviceEntityID>
			<cfset result.entityID = local.getServiceEntityQry.entityID>
			<cfset result.publicProfileURL = local.getServiceEntityQry.publicProfileURL>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAuthoriseRequestString" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="callbackURL" type="string" required="false">
		<cfargument name="callbackParams" type="struct" default="#structNew()#">
		
		<cfset var thisService = getService(arguments.serviceID)>
		<cfreturn application.com[thisService.serviceTextID].getAuthoriseRequestString(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getAuthenticateRequestString" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="callbackURL" type="string" required="false">
		<cfargument name="callbackParams" type="struct" default="#structNew()#">
		
		<cfset var thisService = getService(arguments.serviceID)>
		<cfreturn application.com[thisService.serviceTextID].getAuthenticateRequestString(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="authenticateUserViaService" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		
		<cfset var thisService = getService(arguments.serviceID)>
		
		<cfset var serviceEntityID = application.com[thisService.serviceTextID].getProfile(fieldlist="id").profile.ID> <!--- the user's facebook,twitter or LinkedIn ID --->
		<cfset var result = {isOK=false,validUser=false,personID=0,serviceEntityID=serviceEntityID}>
		
		<cfset var thisPersonID = application.com.service.doesEntityExistWithServiceEntityID(serviceID=arguments.serviceID,serviceEntityID=serviceEntityID,entityTypeID=0).entityID>
		<cfif thisPersonID neq 0 and application.com.service.hasEntityBeenLinked(serviceID=arguments.serviceID,entityID=thisPersonID).linkEstablished>
			<cfset result.validUser = true>
			<cfset result.personID = thisPersonID>
			<cfset result.isOK = true>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getProfile" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" required="true">
		<cfargument name="fieldlist" type="string" required="false">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="entityTypeID" type="numeric" required="false">
		
		<cfset var thisService = getService(arguments.serviceID)>

		<cfreturn application.com[thisService.serviceTextID].getProfile(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="getConnections" access="public" returnType="struct" output="false">
		<cfargument name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="serviceEntityID" type="string" required="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		<cfargument name="excludeNonRelaywarePeople" type="boolean" default="false">
		
		<cfset var thisService = getService(arguments.serviceID)>
		<cfset var result = {isOK=true,person=queryNew("ID,fullname,personID")}>
		
		<cfif not structKeyExists(arguments,"serviceEntityID")>
			<cfset arguments.serviceEntityID = application.com.service.getEntityServiceID(serviceID=thisService.serviceTextID,entityID=arguments.personID)>
		<cfelse>
			<cfset arguments.personID = application.com.service.getServiceEntity(serviceEntityID=arguments.serviceEntityID,serviceID=thisService.serviceTextID).entityID>
		</cfif>
		
		<cfif hasEntityBeenLinked(entityID=arguments.personID,entityTypeID=application.entityTypeID.person,serviceID=arguments.serviceID).linkEstablished and application.com.settings.getSetting("socialMedia.enableSocialMedia")>
			<cfif structKeyExists(application.com[thisService.serviceTextID],"getConnections")>
				<cfset result = application.com[thisService.serviceTextID].getConnections(argumentCollection=arguments)>
				
				<cfif arguments.excludeNonRelaywarePeople>
					<cfquery name="result.person" dbtype="query">
						select * from result.person where personID <> 0
					</cfquery>
				</cfif>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getAccessToken" access="public" returnType="any" output="false">
		<cfargument name="serviceID" type="string" default="linkedIn">
		
		<cfset var thisService = getService(arguments.serviceID)>
		
		<cfreturn application.com[thisService.serviceTextID].getAccessToken(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="handleCallback" access="public" returnType="struct" output="false">
		<cfargument name="serviceID" type="string" required="true">
		
		<cfset var result = structNew()>
		<cfset var accessToken = "">
		<cfset var authenticateUser = false>
		<cfset var isOK = true>
		<cfset var thisPersonID = "">
		
		<!--- we need to delete an existing accessToken if we're dealing with a new authentication. This was discovered in testing
			where I was trying to link accounts without actually logging in to RW. So, try and link and account. Delete the link and try again.. It was storing
			and using the old access token. --->
		<cfif structKeyExists(session,"accessToken")>
			<cfset structDelete(session,"accessToken")>
		</cfif>
		
		<!--- linkedIn authentication --->
		<cfif structKeyExists(url,"oauth_token") and structKeyExists(url,"oauth_verifier") and listFindNoCase("linkedIn,twitter",arguments.serviceID)>
			<cfset accessToken = CreateObject("component", "oauth.oauthtoken").getAccessToken(requestTokenKey=url.oauth_token,serviceID=arguments.serviceID)>
			
		<!--- a code variable on the url is a facebook authentication --->
		<!--- NJH Case 436757 changes --->
		<cfelseif structKeyExists(url,"code")  and listFindNoCase("linkedIn,facebook",arguments.serviceID)>			
			<cfset accessToken = application.com[arguments.serviceId].getAccessToken(code=url.code)>
			
		<cfelseif structKeyExists(form,"frmSignInWithService") and application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="frmServiceEntityID")>
			<cfset thisPersonID = application.com.service.doesEntityExistWithServiceEntityID(serviceID=arguments.serviceID,serviceEntityID=frmServiceEntityID,entityTypeID=0).entityID>
			<cfset accessToken = CreateObject("component", "oauth.oauthtoken").getAccessToken(serviceID=arguments.serviceID,entityId=thisPersonID)>
		</cfif>
		
		<cfif isObject(accessToken) and not accessToken.isEmpty()>
			<cfset authenticateUser=true>
		<cfelse>
			<cfset isOK = false>
		</cfif>
		
		<cfset result.accessToken = accessToken>
		<cfset result.authenticateUser = authenticateUser>
		<cfset result.isOK = isOK>
		
		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="getShareLink" returntype="string" output="false" access="public">
		<cfargument name="contentType" type="string" required="true">
		<cfargument name="urlTitle" type="string" default="link">
		<cfargument name="summary" type="string" default="summary">
		<cfargument name="shareUrl" type="string" default="#request.currentSite.protocolAndDomain#/#cgi.script_name#?#cgi.query_string#">
		
		<cfset var link = "">

		<!--- we don't want to show the share button if the user has not linked their accounts. --->
		<cfif (hasEntityBeenLinked().linkEstablished or not request.relayCurrentUser.isLoggedIn) and application.com.settings.getSetting("socialMedia.enableSocialMedia")>
				<cfset link = '<a href="javascript:void(window.open(''http://www.linkedin.com/shareArticle?mini=true&url=#urlEncodedFormat(arguments.shareUrl)#'',''Share#arguments.contentType#'',''width=550px,height=420px''))"><img src="/images/social/linkedin-share.jpg"></a>'>
		</cfif>
		
		<cfreturn link>
	</cffunction>
	
	
	<!--- <cffunction name="shortenURL" returnType="string" output="false" access="public">
		<cfargument name="url" type="string" required="true">
		
		<cfset var result = structNew()>
		<cfset var shortURL = arguments.url>
		<cfset var urlShortener = application.com.settings.getSetting("socialMedia.urlShortener")>
		
		<!--- TODO: need to add protocol to the url if one doesn't exist --->
		<!--- bit.ly has 3 return formats: json,xml and text... am using text at the moment as I'm not sure we need any of the other fields returned... but can easily switch to xml if need be. --->
		
		<cfif urlShortener.login neq "" and urlShortener.apiKey neq "">
			<cfhttp url="https://api-ssl.bitly.com/v3/shorten?login=#urlShortener.login#&apiKey=#urlShortener.apiKey#&longUrl=#urlEncodedFormat(trim(arguments.url))#&format=txt" method="get" result="result">
			
			<cfif result.responseHeader.status_code eq "200">
				<cfset shortURL = result.fileContent>
			</cfif>
		</cfif>
		
		<cfreturn shortURL>
	</cffunction> --->
	
	
	<!--- this function is used to take a string and shorten any links that have been found --->
	<!--- <cffunction name="setStringForShare" returnType="string" output="false" access="public">
		<cfargument name="shareString" type="string" required="true">
		
		<cfset var resultString = arguments.shareString>
		<cfset var shortenedUrls = "">
		<cfset var longUrl = "">
		<cfset var tagStruct = structNew()>
		<cfset var shortenedUrl = "">
		<cfset var groupNames = {1="protocol",2="domainAndQueryString"}>
		<cfset var hrefStruct = structNew()>

		<cfset var regExpResult = application.com.regexp.findHTMLTagsInString(htmlTag="a",hasEndTag=true,inputString=arguments.shareString)>
	
		<!--- here we look for anchors,extract the hrefs and bit.ly them, and then remove the html anchor code, replacing it with the new bit.ly link --->
		<cfloop array=#regExpResult# index="tagStruct">
			<cfset longUrl = tagStruct.attributes.href>
			
			<!--- don't shorten the url again if it exists twice in the input string. Probably not likely to happen, but trying not to waste calls to bit.ly --->
			<cfif not listFindNoCase(shortenedUrls,longUrl,application.delim1)>
				<cfset shortenedUrl = application.com.social.shortenURL(url=longUrl)>
				<cfset shortenedUrls = listAppend(shortenedUrls,longUrl,application.delim1)>
				<cfset resultString = replace(resultString,tagStruct.string,shortenedUrl,"ALL")>
			</cfif>
		</cfloop>
		
		<!--- here we just look for hrefs and bit.ly them --->
		<cfset regExpResult = application.com.regExp.refindAllOccurrences(string=arguments.shareString,reg_expression="(http://|www\.)([^\s]+)",groupNames=groupNames)>
		<cfloop array=#regExpResult# index="hrefStruct">
			<cfset longUrl = hrefStruct.string>
			
			<!--- don't shorten the url again if it exists twice in the input string. Probably not likely to happen, but trying not to waste calls to bit.ly --->
			<cfif not listFindNoCase(shortenedUrls,longUrl,application.delim1)>
				<cfset shortenedUrl = application.com.social.shortenURL(url=longUrl)>
				<cfset shortenedUrls = listAppend(shortenedUrls,longUrl,application.delim1)>
				<cfset resultString = replace(resultString,longUrl,shortenedUrl,"ALL")>
			</cfif>
		</cfloop>
		
		<cfreturn resultString>
	</cffunction> --->
	
	
	<cffunction name="getShareText" access="public" returnType="string" output="false">
		<cfargument name="action" type="string" required="true">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		<cfargument name="performedOnEntityID" type="numeric" required="true">
		
		<cfset var defaultComment = application.com.relayTranslations.translatePhrase(phrase="phr_share_#arguments.event#",mergeStruct=arguments.mergeStruct)>
	
		<cfreturn defaultComment>
		<!--- <cfset var mergeVarsArray = application.com.regExp.refindAllOccurrences(reg_expression=application.com.relayTranslations.regExp.SquareBrackets,string=defaultComment,groupNames=groupNames)> --->

	</cffunction>
	
	
	<!--- for real time sharing...used primarily on the portal --->
	<cffunction name="sendAutomatedShare" access="public" returnType="struct" output="false">
		<cfargument name="event" type="string" required="true">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		<cfargument name="entityIdList" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default=0>
		
		<cfreturn share(comment=getShareText(argumentCollection=arguments))>
	</cffunction>
	
	
	<!--- loop over the activities and share them if they have not already been shared and they are meant to be shared. --->
	<cffunction name="sendActivityShares" access="public" output="false">
		<cfargument name="activityID" type="numeric" required="false">
		<cfargument name="numActivitiesToShare" type="numeric" default="20">
		
		<cfset var getActivities = "">
		<cfset var shareResult = structNew()>
		<cfset var serviceID = 0>
		<cfset var phrasePrefix = "">
		<!--- <cfset var urlTitlePhrase = "">
		<cfset var urlTitle = ""> --->
		<cfset var urlImage = "">
		
		<!--- at the moment, only personal activites can be shared. --->
		<cfquery name="getActivities" datasource="#application.siteDataSource#">
			select top #arguments.numActivitiesToShare# ra.ID as activityID,shareText,performedByEntityID,performedByEntityTypeID,performedOnEntityTypeID,[action],url,urlTitle
			from relayActivity ra
				left join relayActivityShare ras on ra.ID = ras.relayActivityID
			where ras.ID is null
				and ra.performedByEntityTypeID = 0
				<cfif structKeyExists(arguments,"activityID")>and ra.ID = #arguments.activityID#</cfif>
				and ra.created >= dateAdd(day,-1,getDate())
				order by ra.created
		</cfquery>
		
		<cfloop query="getActivities">
			<cfset phrasePrefix = "phr_share_#application.entityType[performedOnEntityTypeID].tablename##action#">
			<!--- <cfset urlTitlePhrase = "#phrasePrefix#_urlTitle">
			<cfset urlTitle = application.com.relayTranslations.translatePhrase(phrase=urlTitlePhrase,evaluateVariables=true)> --->
			<cfif fileExists(application.paths.content&"\social\#application.entityType[performedOnEntityTypeID].tablename##action#.jpg")>
				<cfset urlImage = application.com.relayCurrentSite.getExternalSiteURL()&"/content/social/#application.entityType[performedOnEntityTypeID].tablename##action#.jpg">
			</cfif>
			
			<cfset shareResult = share(comment=shareText,url=url,urlTitle=urlTitle,urlImage=urlImage,entityID=performedByEntityID,entityTypeID=performedByEntityTypeID,performedOnEntityTypeID=performedOnEntityTypeID)>

			<cfloop collection="#shareResult#" item="serviceID">
				<cfquery name="local.logActivityShare" datasource="#application.siteDataSource#">
					insert into relayActivityShare (serviceID,relayActivityID,shared,createdBy)
					values (<cf_queryparam value="#getService(serviceID=serviceID).serviceID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#activityID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#IIF(shareResult[serviceID].isOK,1,0)#" CFSQLTYPE="CF_SQL_bit" >,<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >)
				</cfquery>
			</cfloop>
		</cfloop>

	</cffunction>
	
	
	<cffunction name="share" access="public" returnType="struct" output="false">
		<cfargument name="comment" type="string" required="true">
		<cfargument name="url" type="string" required="false" default="">
		<cfargument name="urlTitle" type="string" required="false" default="">
		<cfargument name="urlImage" type="string" required="false" default="">
		<cfargument name="entityId" type="numeric" default="#request.relayCurrentUser.personID#">
		<cfargument name="entityTypeID" type="numeric" default=0>
		<cfargument name="performedOnEntityTypeID" type="numeric" required="false">
		
		<cfset var result = structNew()>
		<cfset var serviceID = "">
		<cfset var shareString = arguments.comment>
		<cfset var socialMedia = application.com.settings.getSetting("socialMedia")>
		<cfset var shareResult = structNew()>
		<cfset var tablename= "">
		<cfset var sendShare= true>
		
		<cfif structKeyExists(arguments,"performedOnEntityTypeID") and arguments.entityTypeID eq 0>
			<cfset tablename = application.entityType[arguments.performedOnEntityTypeID].tablename>
		</cfif>

		<cfif socialMedia.enableSocialMedia>
			<cfloop list="#socialMedia.services#" index="serviceID">
				
				<cfif tablename eq "eventDetail" and arguments.entityTypeID eq 0 and application.com.flag.isFlagSetForPerson(personID=arguments.entityID,flagId="optOutShare#serviceID#_Events")>		<!--- 2012/04/18 PPB P_REL112 flag was optOutAutomatedSharing_event  --->
					<cfset sendShare = false>
				<cfelseif listFindNoCase("trngModule,trngCertification",tablename) and arguments.entityTypeID eq 0 and application.com.flag.isFlagSetForPerson(personID=arguments.entityID,flagId="optOutShare#serviceID#_Training")>		<!--- 2012/04/18 PPB P_REL112 flag was optOutAutomatedSharing_training  --->
					<cfset sendShare = false>
				<cfelseif tablename eq "person" and arguments.entityTypeID eq 0 and application.com.flag.isFlagSetForPerson(personID=arguments.entityID,flagId="optOutShare#serviceID#_accountMgt")>
					<cfset sendShare = false>
				</cfif>
				
				<cfset var entityLinked = hasEntityBeenLinked(entityID=arguments.entityID,entityTypeID=arguments.entityTypeID,serviceID=serviceID)>
				<cfset shareResult = {isOk=true,message=""}>
				
				<!--- need to check whether user has opted out for the automated share for this event --->
				<cfif entityLinked.linkEstablished and sendShare>
					<cftry>
						<cfset shareResult = application.com[serviceID].share(argumentCollection=arguments)>
						<cfcatch>
							<cfset shareResult.isOK = false>
							<cfset shareResult.message = cfcatch.message>
						</cfcatch>
					</cftry>
				<cfelse>
					<cfset shareResult.isOK = false>
					<cfset shareResult.message = entityLinked.message>
				</cfif>
				
				<cfset result["#serviceID#"] = shareResult>
			</cfloop>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="addRelayActivity" access="public" output="false">
		<cfargument name="action" type="string" required="true">
		<cfargument name="siteID" type="string" default="#request.currentSite.siteDefId#">
		<cfargument name="performedOnEntityID" type="numeric" required="true">
		<cfargument name="performedOnEntityTypeID" type="numeric" required="true">
		<cfargument name="performedByEntityID" type="string" default="#request.relayCurrentUser.personId#"> <!--- can be a list of entityIDs --->
		<cfargument name="performedByEntityTypeID" type="numeric" default="#application.entityTypeID.person#">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		<cfargument name="sendShare" type="boolean" default="false"> <!--- whether we share it immediately --->
		<cfargument name="created" type="date" default="#createODBCDateTime(request.requestTime)#"> <!--- this can be passed through.. for example, using the element go-live date, which won't necessarily be the current date --->
		<cfargument name="share" type="boolean" default="true"> <!--- whether we want to share it at all with social media --->
		<cfargument name="url" type="string" default="">
		<cfargument name="text" type="string" default="">
		<cfargument name="title" type="string" default="">
		<cfargument name="insertIfExists" type="boolean" default="false">
		
		<cfset var insertActivityRecord = "">
		<cfset var entityName = application.entityType[arguments.performedOnEntityTypeID].tablename>
		<cfset var performedOnUniqueKey = application.entityType[arguments.performedOnEntityTypeID].uniqueKey>		<!--- 2014-11-04 AHL Case 441864 Activity Stream Issues --->
		<cfset var phrasePrefix = "phr_share_#entityName##arguments.action#">
		<cfset var shareTextPhrase = "#phrasePrefix#_text">
		<cfset var urlPhrase = "#phrasePrefix#_url">
		<cfset var titlePhrase = "#phrasePrefix#_title">
		<cfset var shareText = arguments.text>
		<cfset var shareUrl = arguments.url>
		<cfset var shareTitle = arguments.title>
		<cfset var canShare = false>
		<cfset var entityID = 0>
		<cfset var databaseID = application.instance.databaseID>
		
		<!--- START 2014-11-04 AHL Case 441864 Activity Stream Issues --->
		<cfif not structKeyExists(arguments.mergeStruct,performedOnUniqueKey)>
			<cfset arguments.mergeStruct[performedOnUniqueKey] = arguments.performedOnEntityID>
		</cfif>
		<!--- END 2014-11-04 AHL Case 441864 Activity Stream Issues --->

		<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia") or not application.com.settings.getSetting("socialMedia.share.#entityName#.#arguments.action#")>
			<cfset arguments.share = false>
			<cfset arguments.sendShare = false>
		</cfif>
		
		<cfif shareText eq "">
			<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=replaceNoCase(shareTextPhrase,"phr_",""))>
				<cfset shareText = application.com.relayTranslations.translatePhrase(phrase=shareTextPhrase,evaluateVariables=true,mergeStruct=arguments.mergeStruct)>
			</cfif>
		</cfif>
		<cfif shareUrl eq "">
			<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=replaceNoCase(urlPhrase,"phr_",""))>
				<cfset shareUrl = application.com.relayTranslations.translatePhrase(phrase=urlPhrase,evaluateVariables=true,mergeStruct=arguments.mergeStruct)>
			</cfif>
		</cfif>
		<cfif shareTitle eq "">
			<cfif application.com.relayTranslations.doesPhraseTextIDExist(phraseTextID=replaceNoCase(titlePhrase,"phr_",""))>
				<cfset shareTitle = application.com.relayTranslations.translatePhrase(phrase=titlePhrase,evaluateVariables=true,mergeStruct=arguments.mergeStruct)>
			</cfif>
		</cfif>

		<cfif shareText neq "">
			<cfset shareText = application.com.social.shortenUrlInText(text=shareText)>
		</cfif>

		<!--- an issue here if the site hasn't registered yet with heartbeat.. DatabaseId is then an empty string. So, setting it to 0.. we may have to revisit then the databaseIDs
			that are 0 and then doing an update. --->		
		<cfif databaseID eq "">
			<cfset databaseID = 0>
		</cfif>

		<!--- if this is just a single individual performing an action and we've been asked to share it... --->
		<cfif arguments.sendShare and listLen(performedByEntityID) eq 1>
			<cfset canShare = true>
		</cfif>
		
		<cfquery name="insertActivityRecord" datasource="#application.siteDataSource#">
			<cfloop list="#arguments.performedByEntityID#" index="entityID">
				<cfif not arguments.insertIfExists>
				if not exists (select 1 from relayActivity where performedOnEntityID=#arguments.performedOnEntityID# and performedOnEntityTypeID=#arguments.performedOnEntityTypeID# and siteID =  <cf_queryparam value="#arguments.siteID#" CFSQLTYPE="CF_SQL_INTEGER" >  and [action] =  <cf_queryparam value="#arguments.action#" CFSQLTYPE="CF_SQL_VARCHAR" >  and performedByEntityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >  and performedByEntityTypeID=#arguments.performedByEntityTypeID# and databaseID =  <cf_queryparam value="#databaseID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				begin
				</cfif>
				insert into relayActivity (performedOnEntityID,performedOnEntityTypeID,performedByEntityID,performedByEntityTypeID,shareText,siteID,[action],databaseID,created,createdBy,lastUpdatedBy,url,share,urlTitle) values
					(#arguments.performedOnEntityID#,#arguments.performedOnEntityTypeID#,#entityID#,#arguments.performedByEntityTypeID#,N'#shareText#',#arguments.siteID#,'#arguments.action#',#databaseID#,#arguments.created#,#request.relayCurrentUser.userGroupID#,#request.relayCurrentUser.userGroupID#,'#shareUrl#',#IIF(arguments.share,1,0)#,N'#shareTitle#')
				<cfif canShare>
					select scope_identity() as activityID
				</cfif>
				<cfif not arguments.insertIfExists>
				end
				else
				select 0 as activityID
				</cfif>
			</cfloop>
		</cfquery>

		<cfif canShare and isNumeric(insertActivityRecord.activityID) and insertActivityRecord.activityID neq 0>
			<cfset sendActivityShares(activityID=insertActivityRecord.activityID)>
		</cfif>
	</cffunction>
	

</cfcomponent>