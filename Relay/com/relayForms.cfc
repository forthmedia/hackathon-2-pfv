<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2011/11/30  WAB created function createStandardRelayFormElementAttributeStructure
2012-01-23	NYB	Case#424434 changed createJavascriptBind function so selects fields filled from binds now hold onto their value except when the value is no longer available
Added cfinvoke call to "#componentPointer#" in Function "runValidValueFunction"
2012-03-15	PPB reverted change done for Case 424434 cos it didn't work for opps (multiple bound dropdowns) - WAB to re-code asap
2012-03-20	IH	Case: 424412 set isSelected to 0 to prevent selections highlighting
2012-01-30 	WAB displayValidValues() add support for a different null value
2013-02-06 	WAB runValidValueFunction() Added support for unnamed arguments in validvalue functions
2013-08-15  IH Case 436506 remove "required" attribute from checkboxes as it caused the JS validation on the deal registration form to stop working
2013-08-20	YMA	Case 436441 Added support for Binds to populate two selects selected and list boxes.
2013-10-15 	WAB	CASE 437370 Fix problem when displaying flags as twoSelectsCombo
2014-08-12 	AXA fixed label displays on radio buttons
2014-10-14	WAB	changes to createStandardRelayFormElementAttributeStructure() to deal with storage of booleans (either as 0/1 or Yes/No)
2014-10-20 	WAB getValidValuesFromString(). Added support for mergeStruct
2014-11-11	NJH added dataAttributes to displayvalidvalues, to allow extra attributes. Replaces customAttribute essentially.
2015-03-18 	WAB JIRA Fifteen-303 reinstated <labels> when outputting responsive checkbox/radio groups (and removed from cf_input at the same time)
2015-05-13	NJH	Changes to support Nutanix requests. Added function to get default lookupValue for lookupfield. Uncommented a line that Will thought we may not need anymore.
2015-07-01	WAB CASE Fifteen-395 (related to Case 444625). Some select boxes get two lots of null text. Alter getValidValuesFromString()
2015-04-01	WAB Added function convertFieldNameForValidValueTable()
2015-07-02	WAB Knock on effect of parts of CASE Fifteen-395.  When showing a optgroups of a select box using TwoSelectsRelated, a random 'Select A Value' was output if nulltext was defined
2015-11-30  SB  Bootstrap
2015-12-01 	WAB Added Support for jQuery multi-select plugin (During PROD2015-290 Visibility Project.)
2016-01-20	WAB PROD2015-370 jQuery multi-select plugin enabled by default, and made to work properly with Ajax
2016-01-29	WAB A few more multiselect changes, added permanent hidden field (so field still exists when no items are selected), make size =1 when plugin used
2016-02-01	WAB And then removed the permananent hidden field again - causes JS problems.  Will have to rely on the _orig field, so that is now output by default
				Convert cf_displayValidValues to use code in this cfc
2016-02-02	WAB	Added custom event when bind changes a field
				Add data-nullvalue so we can have non blank null values in select boxes (useful for binding where argument has to be numeric)
				Change name of multiple-select js file
				Change layout of radio/checkboxes to inline (and fix problem of all radios being required)
2016-02-09	RJT	BF-477 For the multiselect options we explicityly setting width as empty to prevent the multi select javascript from (sometimes incorrectly - e.g. when tab isn't visible') resizing it. Its right to begin with so this is good
2016-02-11 	WAB BF-449 multiselect not working in settings due to dots in the fieldnames.  Added an escape function.
2016-02-12  SB	PROD2015-532 Fixed the radio button layout
2016/02/19	NJH	Added some valid attributes of input to support unique email validation.
2016/02/26	NJH added tagAttribtues for radio and checbox, as their valid attributes are a much smaller list.
2016-04-06	WAB	BF-629 Title attribute on checkboxes and radios being output wrongly
2016-10-11	WAB	New version of convertTagAttributeStructToValidAttributeList which is about 10x faster.  Uses structKeyExists instead of a regExp for most of the attributes and then a simple regExp for the last few
2016-11-11	WAB	PROD2016-2731 Performance Improvements to convertTagAttributeStructToValidAttributeList()
2016-11-14	WAB	Radio/Checkbox output gets onChange events added
2016-11-23	DBB PROD2016-2816 - Selections -all fields - valid values error Fixed.
2016-12-05	atrunov fix bug PROD2016-2785 (reason of this bug - date mask attribute was not generated for date input field)
2016-11		WAB FormRenderer Automatically add data-textid atttribute if textif column found in query
2016-11		WAB FormRenderer Some mods to the output of checkboxes to support conditional display and onEvents better
2017/01/11	NJH JIRA PROD2016-2299 - fix the bind function for a two selects. Ripped out all the old code related to two selects as it turns out that we can get away with the same code used for a normal select.
2017-02-13	IH Jira PROD2016-3201 add "blacklist" to global attributes
2017-02-22	WAB PROD2016-3479 createStandardRelayFormElementAttributeStructure() Add support for bind functions to settings
--->

<cfcomponent >

	<cfset this.tagAttributeLists = {byTagName = {
									  select = "autofocus|form|size|multiple|disabled|autofocus|required|width"
									, input = "accept|accessKey|align|alt|autocomplete|autofocus|checked|dirname|disabled|form|height|ismap|list|mask|max|maxlength|min|multiple|pattern|placeholder|readonly|required|size|src|step|type|usemap|value|width|personID|orgTypeID|uniqueEmail|uniqueEmailMessage"
									, textArea = "autofocus|disabled|form|maxLength|placeholder|readonly|required|rows|cols|wrap"
									, anchor = "href|hreflang|media|rel|target|type"
									, img = "src|ismap|width|height|crossorigin|alt"
									, form = "method|action|enctype|noValidate|autocomplete|accept-charset"

								}
								, byType = {
									  radio = "checked|disabled|value|required|type|form|autofocus"
								}
								, global = {
									  standard = "on.*|accesskey|class|contenteditable|contextmenu|dir|draggable|dropzone|hidden|id|lang|name|spellcheck|style|tabIndex|title|translate|blacklist|data-.*"
									, rw = "dateMask|requiredMessage|message|submitFunction|requiredFunction|label|onvalidate|validate|validateat|range|novalidate|price|quantity|productID"   										 <!--- price,quantity and productID are used for special pricing on the portal --->
									, RegExp = "\Aon.*|data-.*"
								}}>

	<cfset this.tagAttributeLists.byType.checkbox = this.tagAttributeLists.byType.radio>

	<cfset this.tagAttributeStructures = {byTagName = {}, byType = {}, global = {}}>

	<cfset variables.structureFunctions = createObject ("component","com.structureFunctions")> <!--- boot dependency issues, can't load from application.com --->
	<cfloop collection = "#this.tagAttributeLists.byTagName#" item="local.item">
		<cfset this.tagAttributeStructures.byTagName [local.item] = variables.structureFunctions.listToStruct (this.tagAttributeLists.byTagName [local.item],"|")>
	</cfloop>

	<cfloop collection = "#this.tagAttributeLists.byType#" item="local.item">
		<cfset this.tagAttributeStructures.byType [local.item] = variables.structureFunctions.listToStruct (this.tagAttributeLists.byType [local.item],"|")>
	</cfloop>

	<cfset this.tagAttributeStructures.global = variables.structureFunctions.listToStruct (listappend(this.tagAttributeLists.global.standard,this.tagAttributeLists.global.rw,"|") ,"|")>


	<!--- WAB/NJH 2010/09/29 created this function to use CF binding without the needs for CFFORM
		currently just does select boxes populated by cfcs
		can handle named or unnamed cfc arguments
		Could be extended (probably)
		Added handling for a selected item when bindOnLoad set
		WAB 2011/02/03 added support for defaultSelected

		2013-08-20	YMA	Case 436441 Added support for Binds to populate two selects selected and list boxes.

	--->

	<cffunction name="createJavascriptBind" output="false">
		<cfargument name="bind">  <!--- coldFusion bind expression --->
		<cfargument name="name"> 	<!--- element name --->
		<cfargument name="type">	<!--- element Type (SELECT) --->
		<cfargument name="value" default="">	<!--- name of the column in query that give option value --->
		<cfargument name="display" default=""> <!--- name of the column in query that give option text --->
		<cfargument name="displayas">
		<cfargument name="bindOnLoad" default="false">
		<cfargument name="formName" default="">  <!--- not usually required, calculated using JS --->
		<cfargument name="selected" default="">
		<cfargument name="showNull" default="true">
		<cfargument name="nullText" default="phr_ext_select">

		<cfset var onChangeParameterList = '' >
		<cfset var bindExpressionParameterList = '' >
		<cfset var regExpCFCArguments = '' >
		<cfset var regExpCFCArgumentsGroups = '' >
		<cfset var cfcArgumentsArray = '' >
		<cfset var useNamesFromCFCMetadata = '' >
		<cfset var obj = '' >
		<cfset var cfcMetaData = '' >
		<cfset var cfcFunction = '' >
		<cfset var cfcFunctionParametersArray = '' >
		<cfset var groups = '' >
		<cfset var curlyRegExp = '' >
		<cfset var counter = '' >
		<cfset var thisCFCParameterName = '' >
		<cfset var searchForParametersWithCurlyBrackets = '' >
		<cfset var thisBindParam = '' >
		<cfset var thisCFCParam = '' >
		<cfset var webServiceCall = '' >
		<cfset var webServiceCallList = ''>
		<cfset var randomNumber = '' >
		<cfset var cfcArgument = '' >
		<cfset var result = {script_js="",scriptWithOnLoad_js=""}>
		<cfset var bindStringRegExpGroups = {1="TYPE",2="PATHANDMETHOD",3="Parameters"}>
		<cfset var bindStringRegExp = "(.*?):(.*?)\((.*?)\)"><!--- this parses a string like cfc:relay.webservices.wabtest.usergroup({xx},{y2},'name') --->
		<cfset var bindAttributes = "">
		<cfset var bindStringReFindResult = application.com.regexp.reFindAllOccurrences(bindStringRegExp,arguments.bind,bindStringRegExpGroups)>
		<cfset var bindToParams = "">
		<cfset var selectFunction = "">
		<cfset var jsFunctionName = "">
		<cfset var bindFromFieldName = "">

		<cfif arguments.type is not "select">
			<cfreturn BindError("Binding to #arguments.type# fields is not supported")>
		</cfif>

		<cfif not arrayLen(bindStringReFindResult )>
			<cfreturn BindError("Could not parse bind function: #name#: #bind#")>
		</cfif>

			<cfset bindAttributes = bindStringReFindResult[1]>

			<cfif bindAttributes.type is not "cfc">
					<cfreturn BindError("#bindAttributes.type# binding  is unsupported")>
			</cfif>

				<cfset bindAttributes.bindToName = arguments.name>
				<cfset bindAttributes.path = reverse(listRest(reverse(bindAttributes.PATHANDMETHOD),"."))>
				<cfset bindAttributes.path = replaceNoCase (bindAttributes.path ,"relay.","","ONE")>
				<cfset bindAttributes.method = listLast (bindAttributes.PATHANDMETHOD,".")>
				<cfset bindAttributes.slashedPath = "/" & replace (bindAttributes.path,".","/","ALL") & ".cfc">

				<!--- Loop though all parameters,
					ones with curly brackets need built into the first parameter
				 --->
				<cfset onChangeParameterList = "">
				<cfset bindExpressionParameterList = "">

			 	<cfset regExpCFCArguments = "(?:\A|,)(.*?)(?:=*)([^=]+?)(?=\Z|,)">  <!--- list looks like xx={aa},yy='bb'  or {aa},'bb' --->
				<cfset regExpCFCArgumentsGroups = {1="name",2="value"}>

				<cfset cfcArgumentsArray = application.com.regexp.reFindAllOccurrences(regExpCFCArguments,bindAttributes.parameters,regExpCFCArgumentsGroups)>

				<!--- if any of the parameters do not have names we have to get the names directly from the cfc
					if some are named and some are not the throw an error (actually only if first is named and one of the others isn't)
				--->
				<cfif arrayLen(cfcArgumentsArray) and cfcArgumentsArray[1].name is "">
					<cfset useNamesFromCFCMetadata = true>
					<cfset obj = createObject("component",bindAttributes.path)>
					<cfset cfcMetaData = getMetaData(obj )>
					<cfset cfcFunction = StructFindValue( cfcMetaData, bindattributes.method )>
					<cfset cfcFunctionParametersArray =  cfcFunction [1].owner.parameters>
				<cfelse>
					<cfset useNamesFromCFCMetadata = false>
				</cfif>

				<cfset groups = {1="FieldName"}>
				<cfset curlyRegExp = "\{(.*)\}">

				<cfset counter = 0>

				<cfloop index="cfcArgument" array = "#cfcArgumentsArray#">
					<cfset counter ++ >
					<cfif useNamesFromCFCMetadata>
						 <cfset thisCFCParameterName = cfcFunctionParametersArray[counter].name>
					<cfelseif cfcArgument.name is "">
						<cfreturn BindError("Bind function must use all named or all unnamed parameters")>
					<cfelse>
						<cfset thisCFCParameterName = cfcArgument.name>
					</cfif>
					<cfset searchForParametersWithCurlyBrackets = application.com.regexp.reFindAllOccurrences(curlyRegExp,cfcArgument.value,groups)>

					<cfif arrayLen(searchForParametersWithCurlyBrackets)>
						<cfset thisBindParam = "['#searchForParametersWithCurlyBrackets[1].fieldname#',formName,'change']">
						<cfset onChangeParameterList = listappend (onChangeParameterList,thisBindParam)>
						 <cfset thisCFCParam = "['#thisCFCParameterName#','#searchForParametersWithCurlyBrackets[1].fieldname#',formName,'value']">
					<cfelse>
						 <cfset thisCFCParam = "['#thisCFCParameterName#',#cfcArgument.value#]">
					</cfif>
					<cfset bindExpressionParameterList = listappend (bindExpressionParameterList,thisCFCParam)>
				</cfloop>

				<cfif showNull and structKeyExists(arguments,"nullText")>
					<cfset bindToParams="'nullText':'#arguments.nullText#'">
				</cfif>

				<!--- NJH 2016/12/20 - JIRA PROD2016-3006
					Here we are adding a current value argument to the webservice so that we can keep the selected value should it not be present in the return.
					Once the webservice has been called once, we then remove the currentValue as an argument as otherwise it would always be returned. This is why we've got the orig bind expression parameter list.
					This is used to reset the bindExpr variable later on (after the initial bind has been done)
				 --->
				<cfset bindExpressionParameterListOrig = bindExpressionParameterList>
				<cfset bindExpressionParameterList = listappend (bindExpressionParameterList,"['currentValue','#arguments.selected#']")>

				<cfset randomNumber = replace(rand(),".","","ALL")>
				<cfset bindAttributes.bindToNameSelect = bindAttributes.bindToName>
				<!--- NJH 2017/01/11 JIRA PROD2016-2299 - everything uses the same webservice call, whether a two selects or not. Have to change the bindToName if a two-selects as the select has "list_" appended to the name --->
				<cfif arguments.type is "select" and structKeyExists(arguments,"displayas") and arguments.displayas eq "twoselects">
					<!--- the first select in a two selects has the name List_#name# - this is the select that we are binding to --->
					<cfset bindAttributes.bindToNameSelect = 'List_' & bindAttributes.bindToName >
					<!--- <cfset webServiceCall = "'bindTo':'List_#bindAttributes.bindToName#','bindToAttr':'value','#bindAttributes.type#':'#bindAttributes.slashedPath#','cfcFunction':'#bindAttributes.method#','bindToParams':{#bindToParams#},'bindExpr':[ #bindExpressionParameterList#]"> --->
				</cfif>
				<!--- <cfelse> --->
				<cfset webServiceCall = "'bindTo':'#bindAttributes.bindToNameSelect#','bindToAttr':'value','#bindAttributes.type#':'#bindAttributes.slashedPath#','cfcFunction':'#bindAttributes.method#','bindToParams':{#bindToParams#},'bindExpr':[ #bindExpressionParameterList# ]">
				<!--- </cfif> --->

				<cfset jsFunctionName = "_relay_cf_bind_init_#randomNumber#">


				<cf_includeJavascriptOnce template = "/javascript/cf/ajax/messages/cfmessage.js">
				<cf_includeJavascriptOnce template = "/javascript/cf/ajax/package/cfajax.js">

				<cfsaveContent variable = "result.script_js">
					<cfoutput>
					<script type="text/javascript" id="script_#randomNumber#">
					<!--- NJH 2017/01/11 JIRA PROD2016-2299 - removed all this code as not working and not necessary.
						<cfif arguments.type is "select" and structKeyExists(arguments,"displayas") and arguments.displayas eq "twoselects">

						//2014/01/17	YMA	Clean up any duplicate values in the select box created by the bind on the list box.  This is also fired by moveFromTo() in twoselectscombo.js
						jQuery("###bindAttributes.bindToNameSelect#").change(function(){
							var seen = {};
							jQuery('###bindAttributes.bindToNameSelect#').children('option').each(function() {
							    var txt = jQuery(this).text();
							    if (seen[txt])
							        jQuery(this).remove();
							    else
							        seen[txt] = true;
							});
							updateHiddenField('#bindAttributes.bindToNameSelect#')
						});

						//2014/01/17	YMA	Add any extra already selected values to the bind functions selected list.
						function gatherSelectedFunction#randomNumber#(preselected) {

							var selected = preselected +','+ jQuery("###bindAttributes.bindToNameSelect#").find("option").map(function() {
							    if(jQuery.inArray(jQuery(this).attr('value'),preselected.replace(/,\s+/g, ',').split(',')) < 0){
							    	return jQuery(this).attr('value');
							    }
							}).get();

							selected = selected.replace(/(^,)|(,$)/g, "");
							return selected
						}
					</cfif> --->

					<cf_includeJavascriptOnce template = "/javascript/fnlajax.js">

					 #jsFunctionName#=function()
					 {
						<cfif arguments.formName is "">
						var formName = $('script_#randomNumber#').up('form').getAttribute('name');  <!--- WAB 2013-05-20 changed from .name to .getAttribute - otherwise falls over if there is a form element called "name" --->
						<cfelse>
						var formName = '#arguments.formName#'
						</cfif>
						<cfif arguments.type is "select">

 							<!--- <cfif not structKeyExists(arguments,"displayAs") or not arguments.displayas eq "twoselects"> --->
								ColdFusion.Ajax.initSelect('#bindAttributes.bindToNameSelect#','#ucase(arguments.value)#','#ucase(arguments.display)#');
 							<!--- </cfif> --->
							<!---
								Since CF does not handle selecting an item after the bind we have had to do our own
								we have to wait for the CF bind to run
								We have hacked a way to do this.
								We add an onChange listener, CF automatically calls any onchange events which have a property _cf_bindhandler set to true
								This gets called automatically after the bind, but we need to then disable it.  Not having a function to hand for removing a listener I have set an attribute on the select box which I test

								WAB 2015-04-30 Problems getting initial values selected.
								Seemed to be caused by a combination of timing issues and problems if bindOnLoad was set on a field which was also being affected by another field doing its bind on load
								The initial value would get selected correctly but then be unselected
								Changed approach and decided that if the items in a select box were updated then any selected items shodl be preserved (if they appeared in the updated select)
								I wrapped the call to ColdFusion.Bind.cfcBindHandler in some of my own code which gets the selected values before the bind and then sets them afterwards (if the items still exist).
								Unfortunately I couldn't do the setting after the call to cfcBindHandler, because by that point it can have triggered other binds to take place using the wrong value for this field
								I therefore had to add callback functionality to the cfcBindHandler.
								Maybe there was an easier way!
							--->

							var extendedBindHandler = function () {
									/* get currently selected values */
									var object = $(arguments[1].bindTo)
									var currentValue = $F(object)

									<!---  NJH 2016/12/20 - JIRA PROD2016-3006 - once the intial bind has been done, then revert back to the original parameter list (ie. the list without the current value argument) --->
									if (object.getAttribute('initialBindDone')) {
										arguments[1].bindExpr = [ #bindExpressionParameterListOrig# ];
									 }

									/* set a callback function to preserve those values */
									arguments[1].bindToParams.callback = function () {

											/* NJH 2017/01/11 JIRA PROD2016-2299 - when a call is made, refresh the 'selected' options on the right dropdown */
											if (object.getAttribute('data-twoSelects')) {
												var hiddenSelectName = object.name.replace('List_','')
												var moveToSelectName = hiddenSelectName+'_select';
												jQuery('##'+moveToSelectName).empty();
												updateHiddenField(hiddenSelectName);
											}


											/* This deals with setting an intial value */
											if (!object.getAttribute('initialBindDone')) {
												setSelected (object,'#arguments.selected#',true)  // the true sets this as the defaultSelected so that the isDirty function works
												object.setAttribute('initialBindDone',1)
												/* for a two-selects, this will move all the selected options over to the relevant dropdown */
												if (object.getAttribute('data-twoSelects')) {
													moveFromTo(object, object.name, moveToSelectName,false);
												}
											}

										/* this deals with preserving the previously selected value */
										if (currentValue != '') {
											setSelected (object,currentValue,false)
										}

										/* fire an event which can be picked up by plugins etc.  Need to switch to jQuery here! */
										jQuery(object).trigger('rw:ajaxupdate')

									}
									/* call the CF library passing all arguments */
									ColdFusion.Bind.cfcBindHandler.apply (this,arguments)
							}
							/* NJH 2015/05/13 - found out it was needed so reinstated it */
							extendedBindHandler._cf_bindhandler=true;

						</cfif>
						<!--- NJH the space between the square bracket is important as otherwise we get a double square bracket and there is an attempt made to translate it. --->
							ColdFusion.Bind.register([ #onChangeParameterList# ],
														{#webServiceCall#},
														extendedBindHandler,
														#arguments.bindOnLoad#);

						<!--- NJH 2017/01/11 JIRA PROD2016-2299 - remove below. Everything uses the same initSelect and register calls.
						<cfif arguments.type is "select" and structKeyExists(arguments,"displayas") and arguments.displayas eq "twoselects">
							ColdFusion.Ajax.initSelect('List_#bindAttributes.bindToName#','#ucase(arguments.value)#','#ucase(arguments.display)#');
							ColdFusion.Bind.register([ #onChangeParameterList# ],{#webServiceCallList#}, extendedBindHandler,#arguments.bindOnLoad#);
						</cfif>
 --->
						};
					</cfoutput>
				</cfsaveContent>
				<cfset result.scriptWithOnLoad_js =result.script_js & "ColdFusion.Event.registerOnLoad(#jsFunctionName#);</script>">
				<!--- for debuggin adds a link <cfset result.scriptWithOnLoad = result.scriptWithOnLoad & "<A href='javascript:#jsFunctionName#()'>Test Bind</A>"> --->

				<cfset result.script_js =result.script_js & "#jsFunctionName#();</script>">
				<cfset request.relayForm.useCFAjax = true >


		<cfreturn result>

	</cffunction>

	<cffunction name="BindError" output="false">
		<cfargument name="ErrorMessage">

		<cfset var result  = {script_JS = "<script>alert('#jsStringFormat(errorMessage)#')</script>"}>
		<cfset result.scriptWithOnLoad_JS =result.script_js>

		<cfreturn result>
	</cffunction>



		<!--- Code cut from the valid value custom tags
			allows display of validvalues as either selects or checkboxes

		 WAB 2012-01-30 add support for a different null value

		 2013-08-20	YMA	Case 436441 Added support for Binds to populate two selects selected and list boxes.
		 WAB 2015-12-01 	Added Support for jQuery multi-select plugin (During PROD2015-290 Visibility Project.)
		 				Added a selectedAtTop attribute
		WAB 2016-01- 	Lots of changes to do multiselects with a plugin
						Output _orig field by default on multiselects - best way of testing for select box which is on page but not selected
		2016-03-09	WAB Change default null text for multiselect (used as placeholder in the plugin)
		WAB 2016-03-10 Prompted by CASE 448309, added an automatic encryption function
		2016-04-06	WAB	BF-629 Title attribute displaying wrongly on radios/checkbox (name of field rather than value appearing) and on Selects the name of the field used to get the title was displaying as the title of the <select>
		WAB 2016-11-28 PROD2016-2831 TwoSelects not displaying from XMLEditor even if allowSort is set
		--->


	<cffunction access="public" name="validValueDisplay" output="false" returns="string">
			<cfargument name="query" type="query" required="false">
			<cfargument name="name" type="string" required="true">  <!--- form field name --->
			<cfargument name="ID" type="string" default="#arguments.name#">  <!--- form field id--->
			<cfargument name="Label" type="string" default="#arguments.name#">  <!--- might be used for an automatic required message--->
			<cfargument name="value" type="string" default="dataValue">
			<cfargument name="display" type="string" default="displayValue">
			<cfargument name="selected" type="string" default="">	<!--- list of items which are selected.  Can use column on query (arguments.selectedColumn) instead--->
			<cfargument name="selectedColumn" type="string" default="isSelected"> <!--- name of the (boolean) column in the query which indicates whether the item is selected or not (can use list of values (arguments.selected) instead)--->
			<cfargument name="group" type="string" default="">
			<cfargument name="method" type="string" default="edit">
			<cfargument name="multiple" type="boolean" default="false"> <!--- WAB 2016-01-25 added support for multiple attribute and use it to default the displayAs - a bit more standard --->
			<cfargument name="displayAs" type="string" default="#(multiple)?'multiselect':'select'#"> <!--- select,radio,checkbox,multiselect --->
			<cfargument name="column" type="numeric" default="3">  <!--- Number of columns for radio/checkbox --->
			<cfargument name="showNull" type="boolean" default="false"> <!--- Does a select box have a null value --->
			<cfargument name="NullText" type="string" default="#(multiple)?'phr_sys_Select_Values': (displayAs eq 'radio'?'phr_sys_NoneSelected':'phr_Ext_SelectAValue')#">
			<cfargument name="NullValue" type="string" default=""> <!--- WAB 2012-01-30 add support for a different null value, wanted for a productPicker relayTagEditor.  Note that if set to something other than "" then any required checking will fail --->
			<cfargument name="onchange" type="string" default="">
			<cfargument name="allowNewValue" type="boolean" default="false"> <!--- allows user to add a new value to the select box --->
			<cfargument name="newValueText" type="string" default="Add A new Value">
			<cfargument name="newValuePopUpText" type="string" default="#arguments.newValueText#">
			<cfargument name="allowCurrentValue" type="boolean" default="false">
			<cfargument name="passThrough" type="string" default="">
			<cfargument name="required" type="boolean" default="false">
			<cfargument name="requiredMessage" type="string" required="false">
			<cfargument name="size" type="numeric" default="#iif(displayAs is 'multiselect',3,iif(displayAs is 'twoSelects',10,1))#"> <!--- number of items in a multiselect/TwoSelects --->
			<cfargument name="keepOrig" Default = "false"> <!--- outputs the current value as a hidden field with the suffix _orig --->
			<cfargument name="bind" default="" >
			<cfargument name="bindAttribute" default="" >
			<cfargument name="bindOnLoad" default="false" >
			<cfargument name="title" type="string" default="#arguments.display#">
			<cfargument name="displayGroupAs" type="string" default=""> <!--- NJH 2012/12/10 2013Roadmap - show optGroup as two selects related --->
			<cfargument name="allowSort" type="boolean" default="false">
			<cfargument name="selectedAtTop" type="numeric" default="#arguments.size#" hint="Put selected items at top if there are more than this number of items (0 = off)">
			<cfargument name="pluginOptions" type="struct" default="#structNew()#">

			<cfargument name="customAttribute" default="" >    <!--- TBD!! need to think about this --->
			<cfargument name="dataAttributes" type="string" default="" hint="A list of columns in the incoming query that hold data attributes for the select">

			<cfargument name="encrypt" type="boolean" default="false">
			<cfargument name="encryptName" type="string" default="#name#">
			<cfargument name="encryptSingleSession" type="string" default="true">

			<cfset var result_html = "">
			<cfset var selectedQuotedIfNecessary = "">
			<cfset var c = 0>
			<cfset var widtha = 0>
			<cfset var widthb = 0>
			<cfset var getSelected= "">
			<cfset var valueSelected = false>
			<cfset var disabled = false />
			<cfset var disabledAttribute = "" />
			<cfset var bindresult_js = '' />
			<cfset var selectedQuery = '' />
			<cfset var notselectedQuery = '' />
			<cfset var argStruct = structNew()>
			<cfset var twoSelectsCaption1 = "Available Choices">
			<cfset var twoSelectsCaption2 = "Selected Choices">
			<cfset var twoSelectsWidth = 150>
			<cfset var attrStruct = structNew()>
			<cfset var foundInList = 0>
			<cfset var sortOrderAdded = false>
			<cfset var inputArgs = ''>
			<cfset var selectedList = ''>
			<cfset var data = ''>

			<!--- Two selects Combo not used unless specifically requested by setting arguments.useJqueryMultiSelect=false or (WAB 2016-11-28 PROD2016-2831) if allowSort is true --->
			<cfif displayAs is "twoSelects" and not ( (StructKeyExists (arguments,"useJqueryMultiSelect") and arguments.useJqueryMultiSelect is false) OR allowSort is true )>
				<cfset arguments.displayAs = "multiSelect">
			</cfif>

			<cfif not structKeyExists (arguments,"query") and bind is "" >
				You must supply either a query or a bind function <CF_ABORT>
			</cfif>

			<!---  WAB 2016-01-15, having added multiple argument, make sure that it is in synch with the displayAs --->
			<cfif listFindNoCase ("multiselect,checkbox", arguments.displayAs) and not arguments.multiple >
				<cfset arguments.multiple = true>
			</cfif>
			<!---  WAB 2016-02-02, deal with displayAs being blank --->
			<cfif arguments.displayAs is "">
				<cfset arguments.displayAs = (arguments.multiple)?'multiSelect':'select'>
			</cfif>

			<cfset var dataValueColumnEncryptedIfNecessary = value>

			<cfif structKeyExists (arguments,"query")>
				<cfif not listfindnocase(arguments.query.columnList,selectedColumn)>
				<!---
					if there isn't a selected Column on the query we will create one and populate it from the values of #selected#
					does mean having to loop through the query an extra time,
					but makes code below much neater if we only have one way of defining selected
					WAB 2011/10/13 did have a QoQ here, but was losing the order of the query
				 --->

					<cfset queryAddColumn(arguments.query,selectedColumn,"bit",arrayNew(1))>

					<cfloop query="arguments.query">
						<cfset foundInList = listfindnocase(arguments.selected,arguments.query[value][currentRow])>
						<cfset querySetCell(arguments.query,selectedColumn,iif(foundInList,1,0),currentrow)>
					</cfloop>

				<cfelseif keepOrig and selected is "">
					<!--- if we need to output the orginal list of values then we need to calculate it from the isSelected column --->
					<cfquery name="getSelected" dbtype=Query>
					select [#value#] as dataValue
					from arguments.query
					where [#selectedColumn#] = 1
					</cfquery>
					<cfset arguments.selected = valueList (getSelected.dataValue)>

				</cfif>

				<!--- if lots of items in a multiselect then we can put selected ones at top --->
				<cfif arguments.multiple and selectedAtTop is not 0 and arguments.query.recordcount gt selectedAtTop >
					<cfquery name="arguments.query" dbtype=Query>
					select * from
					arguments.query
					order by <cf_queryObjectName value="#selectedColumn#"> desc
					</cfquery>
				</cfif>

				<cfif arguments.encrypt>
					<cfset var securityObj = application.com.security>
					<cfset var dataValueColumnEncryptedIfNecessary = "encryptedValue">
					<cfset queryAddColumn (arguments.query,dataValueColumnEncryptedIfNecessary,"varchar",arrayNew(1))>
					<cfloop query="arguments.query">
						<cfset querySetCell(arguments.query,dataValueColumnEncryptedIfNecessary,securityObj.encryptVariableValue (name=arguments.encryptName,value=arguments.query[value][currentRow],singleSession=encryptSingleSession)	,currentrow)>
					</cfloop>
				</cfif>

				<!--- NJH 2013/02/06 - sometimes title was getting passed through, although perhaps it wasn't meant to. For example, from opportunityDomain.xml there is a title
					which has nothing to do with the query... so check here if title is actually a valid field. --->
				<cfif not listFindNoCase(arguments.query.columnList,arguments.title)>
					<cfset arguments.title = "">
				</cfif>

				<!--- WAB 2016-11  FormRenderer Automatically look for a column named textID in query.  If found then output as data-textid atttribute --->
				<cfif listFindNoCase(arguments.query.columnList,"textID") and not listFindNoCase (arguments.dataAttributes,"TextID")>
					<cfset arguments.dataAttributes = listAppend(arguments.dataAttributes,"textID")>
				</cfif>

			</cfif>


			<cfif method is "View">
				<cfset disabled = true>
				<cfset disabledAttribute = "disabled">
				<cfset arguments.name = ""> <!--- prevents a submit deleting the data --->
				<!--- NJH 2015/07/02 - any disabled input does not get processed. Added id back so that I could get handle on input.<cfset arguments.id = ""> ---> <!--- ditto (well prevents confusion if happened to have view and dit versions on same page and try to do validation) --->
			</cfif>


			<cfsavecontent variable="result_html">
				<cfswitch expression=#arguments.displayAs#>
					<CFCASE value = "radio,checkbox">

						<CFSET c = 0>
						<CFSET widtha = Round((100 / column)-(10/column))>
						<CFSET widthb = Round((100 - (column * widtha))/column)>

						<cfif arguments.query.recordcount>

							<cfset var fieldArgs = {name = arguments.name, id = arguments.id, type=arguments.displayAs}>

							<cfif request.relayFormDisplayStyle neq "HTML-div">
								<CFOUTPUT><TABLE class="booleanflag" id="#arguments.name#_table"></CFOUTPUT>

								<CFLOOP query="arguments.query">
									<cfset inputArgs = structCopy (fieldArgs)>

									<cfif inputArgs.id eq inputArgs.name>
										<cfset inputArgs.id = inputArgs.name & "_" & arguments.query[value][currentRow]>
									</cfif>

									<!--- 2016-04-06	WAB	BF-629 Title attribute on checkboxes and radios being output wrongly
														get value from query  --->
									<cfif arguments.title is not "">
										<cfset inputArgs.title = arguments.query[arguments.title][currentRow]>
									</cfif>

									<!--- 2013-08-15  IH Case 436506 remove "required" attribute from checkboxes as it caused the JS validation on the deal registration form to stop working --->
									<cfif arguments.displayAs is "checkbox">
										<cfset structDelete(inputArgs,"required")>
									</cfif>
									<CFSET c = c + 1>
									<CFIF c MOD column IS 1><TR></CFIF>
									<!--- NJH if radio input is reaonly, then disable any inputs that are not checked --->
									<cfif structKeyExists(arguments,"readonly") and arguments.readonly>
										<cfset disabled = not arguments.query[selectedColumn][currentRow]?true:false>
									</cfif>
									<cfoutput>

									<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="#widthb#%"><cf_INPUT attributeCollection = #inputArgs# VALUE="#arguments.query[dataValueColumnEncryptedIfNecessary][currentRow]#" CHECKED=#arguments.query[selectedColumn][currentRow]# disabled="#disabled#" > </TD>
									<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="#widtha#%"><label for="#inputArgs.id#" class="label">#arguments.query[display][currentRow]#</label></TD>
									<CFIF c MOD column IS 0></TR></CFIF>
									</cfoutput>
								</CFLOOP>

								<CFOUTPUT>
									<CFIF c MOD COLUMN IS NOT 0>
										<TD COLSPAN=#Evaluate(2*(3-c MOD COLUMN))# >&nbsp;<BR></TD></TR>
									</CFIF>
									</TABLE>
								</CFOUTPUT>

								<cfif arguments.required and method is "edit">
									<cfoutput><input type="hidden" name="#name#_required" label="#label#" value="#Label# <cfif arguments.required gt 1>#arguments.required# </cfif>phr_sys_formvalidation_req" required="#arguments.required#" ></cfoutput>
								</cfif>

							<cfelse>

								<CFOUTPUT>
									<div class="validation-group">
										<div class="form-group">
								</CFOUTPUT>


								<CFLOOP query="arguments.query">
									<cfset inputArgs = structCopy (fieldArgs)>

									<cfif inputArgs.id eq inputArgs.name>
										<cfset inputArgs.id = inputArgs.name & "_" & arguments.query[value][currentRow]>
									</cfif>

									<!--- 2016-04-06	WAB	BF-629 Title attribute on checkboxes and radios being output wrongly
														get value from query  --->
									<cfif arguments.title is not "">
										<cfset inputArgs.title = arguments.query[arguments.title][currentRow]>
									</cfif>

									<cfoutput>
									<!--- SB 12/02/2016 The class radio-inline or checkbox-inline should be on the label
										NJH 2016/02/26 - commented out includeKeysRegExp as it wasn't being used. cf_input actually deletes it from the structure
										NJH/RT 2016/03/03 - added data-role=none to fix radio button issue. jQuery Mobile.css was messing it up on portal. This prevents it from happening
									 --->
									<!--- <div class="#radioOrCheckBox#-inline" for="#inputArgs.id#"> --->
									<label class="#arguments.displayas#-inline" for="#inputArgs.id#">
										<cf_input label="#arguments.query[display][currentRow]#" class="#arguments.displayAs#" TYPE="#arguments.displayAs#" VALUE="#arguments.query[dataValueColumnEncryptedIfNecessary][currentRow]#" CHECKED=#arguments.query[selectedColumn][currentRow]# disabled="#disabled#" attributeCollection = #inputArgs# data-role="none" > #arguments.query[display][currentRow]#
									</label>
									<!--- </div> --->
									</cfoutput>
								</CFLOOP>

									<!--- NJH 2017/01/30 JIRA PROD2016-3317 - if show null, then output a 'not selected' option --->
									<cfif arguments.displayAs eq "radio" and arguments.showNull>
										<cfoutput>
										<cfset inputArgs = structCopy (fieldArgs)>
										<cfset inputArgs.id = inputArgs.name & "_">
										<cfset inputArgs.title = arguments.NullText>
										<label class="radio-inline" for="#inputArgs.name#_">
											<cf_input label="#arguments.NullText#" class="radio" TYPE="radio" VALUE="" CHECKED=#arguments.selected eq ''?1:0# disabled="#disabled#" attributeCollection = #inputArgs# data-role="none" > #arguments.NullText#
										</label>
										</cfoutput>
									</cfif>

									<cfoutput>
										</div>
									<cfif arguments.required and method is "edit">
									<input type="hidden" name="#name#_required" label="#label#" value="#Label# <cfif arguments.required gt 1>#arguments.required# </cfif>phr_sys_formvalidation_req" required="#arguments.required#" >
								</cfif>
								</div>
							</CFOUTPUT>

							</cfif>

							<!---  WAB 2016-11 FormRenderer Output a hidden field with name of form name_checkbox to indicate that there is a set of checkboxes on the page --->
							<cfset inputArgs = duplicate (arguments)>  <!--- has to be duplicate, struct copy does not seem to do what is expected --->
							<cfset inputArgs.name = arguments.name & "_" & arguments.displayAs >
							<cfset inputArgs.id = inputArgs.name >
							<cf_INPUT TYPE="hidden" VALUE="" attributeCollection = #inputArgs# >

						</cfif>

					</CFCASE>

					<CFCASE value = "select,multiselect">
						<!--- WAB 2016-01-29 changed structCopy to duplicate because structCopy seems to bring arguments through by reference --->
						<cfset argStruct = duplicate (arguments)>

						<!--- WAB 2015-07-02 reordered code so that displayGroupAs eq select section was separate from subsequent code.  #nulltext# was appearing randomly in the output due to all sort of nasty if statements - this is much neater
						--->
						<cfif arguments.displayGroupAs eq "select">
							<!--- NJH 2012/12/10 - display the optGroup as a two selects related --->
							<cf_twoSelectsRelated value1=#arguments.group# value2=#arguments.value# name1=#arguments.group# name2=#arguments.name# display1=#arguments.group# display2=#arguments.display# query="arguments.query" HTMLBetween="<br>" selected2=#arguments.selected# attributeCollection=#arguments#>
						<cfelse>

							<CFIF arguments.multiple>
								<cfset var defaultpluginOptions = {
																	"multipleSelect" = true
																	, "placeholder" = nullText
																	, "filter" = true
																	, "width"=" "
																	, "selectAllText" = 'phr_multiselect_selectAll'
										        					, "allSelected" = 'phr_multiselect_AllSelected'
															        , "countSelected" = 'phr_multiselect_XoFYSelected'
															        , "noMatchesFound" = 'phr_multiselect_multiselect_noMatchesFound'
																	}> <!---BF-477 RJT By explicityly setting width as empty we prevent the multi select javascript from (sometimes incorrectly) resizing it. Its right to begin with so this is good --->
								<cfset structAppend (arguments.pluginOptions,defaultpluginOptions,false)>

								<!--- WAB 2016-01-29 If we are going to use the multiple select might as well have list size of 1, makes for much less flicker --->
								<cfif arguments.pluginOptions.multipleSelect>
									<cfset argStruct.size = 1>
								</cfif>

							<cfelse>
								<cfset structDelete (argStruct,"multiple")>
								<cfset argStruct.size = 1>

							</CFIF>

							<!--- WAB BF-629 The title argument is used for displaying title on individual options, not a title for the whole select box, so need to delete title from the structure passed into the <select> tag --->
							<cfset structDelete (argStruct, "title")>

							<CFOUTPUT>
								<SELECT class="form-control"
										#convertTagAttributeStructToValidAttributeList(attributeStruct = argStruct,tagName="select")#
											<cfif allowNewValue is 1>onChange = "javascript:checkAddNewValueToSelectBox(this)"</cfif>
											#passthrough#
											#disabledAttribute#
											data-nullvalue = "#NullValue#"
										>
							</CFOUTPUT>

							<CFIF showNull is true >  <!--- WAB: did have and displayas is not "MultiSelect", but can't remember exactly why and don't think it is right --->
								<CFPARAM name = "arguments.nullText" default="phr_Ext_SelectAValue"> <!--- 2009-10-27	NAS LID - 2737 Mod code to make 'Select a value' translateable in drop down lists. --->
								<CFOUTPUT><OPTION VALUE="#NullValue#" >#nullText#</OPTION></CFOUTPUT>
							</CFIF>

							<CFIF allowNewValue is 1 and arguments.newValueText neq "">
								<CFOUTPUT><OPTION VALUE="" addnewValue>#NewValueText#</OPTION></CFOUTPUT>
							</CFIF>

							<cfif structKeyExists (arguments,"query")>

								<cfif arguments.group is "">
									<CFLOOP query="arguments.query" >
										<CFOUTPUT><OPTION VALUE="#htmleditformat(trim(arguments.query[dataValueColumnEncryptedIfNecessary][currentRow]))#" <cfif arguments.title neq "">title="#arguments.query[title][currentRow]#"</cfif> <CFIF arguments.query[selectedColumn][currentRow] > SELECTED="SELECTED" <cfset valueSelected = true></CFIF> <cfif customAttribute is not "">#customAttribute#=#arguments.query[customattribute][currentRow]#</cfif> <cfloop list="#arguments.dataAttributes#" index="data">data-#data#="#arguments.query[data][currentRow]#"</cfloop>>#arguments.query[display][currentRow]#</OPTION></CFOUTPUT>
									</CFLOOP>
								<cfelse>
									<cfoutput query="arguments.query" group="#group#">
										<optgroup label="#arguments.query[group][currentRow]#">
										<CFOUTPUT><OPTION VALUE="#htmleditformat(trim(arguments.query[dataValueColumnEncryptedIfNecessary][currentRow]))#" <cfif arguments.title neq "">title="#arguments.query[title][currentRow]#"</cfif> <CFIF arguments.query[selectedColumn][currentRow] > SELECTED="SELECTED" <cfset valueSelected = true></CFIF> <cfif customAttribute is not "">#customAttribute#=#arguments.query[customattribute][currentRow]#</cfif>>#arguments.query[display][currentRow]#</OPTION></CFOUTPUT>
										</optgroup>
									</cfoutput>

								</cfif>

							</cfif>

							<cfif allowCurrentValue and valueSelected is false and selected is not "">
								<cfoutput><option value = "#htmleditformat(selected)#" selected="SELECTED">#htmleditformat(selected)#</cfoutput>
							</cfif>

							<cfif arguments.displayGroupAs neq "select">
								<CFOUTPUT></SELECT></cfoutput>
							</cfif>


							<cfif arguments.bind is not "">

								<cfset bindresult_js = createJavascriptBind ( bind = arguments.bind
																			, name=arguments.name
																			, type="select"
																			, value = arguments.value
																			, display = arguments.display
																			, selected = arguments.selected
																			, nullText = arguments.nullText
																			, bindOnLoad = arguments.bindOnLoad
																			, showNull = arguments.showNull)>

								<cfoutput>#bindresult_js.scriptWithOnLoad_js#</cfoutput>

							</cfif>

							<cfif arguments.multiple and pluginOptions.multipleSelect>
								<!---  Include plugin code and initialise,
									note that we wrap the existing control in a validation-group div - so that any validation is displayed in correct place
									also add a listener for the underlying control being updated
									WAB 2016-02-11 BF-449 had to escape the id because in settings the ids have dots in them
									--->
								<cf_includeCssOnce template = "/javascript/lib/multiple-select/multiple-select.css">
								<cf_includeJavascriptOnce template = "/javascript/lib/multiple-select/multiple-select.js"
									callback = "
											jQuery ('select###escapeIDForjQuery(arguments.id)#')
												.wrap( '<div class=""validation-group""></div>' )
												.multipleSelect(#serializeJSON(pluginOptions)#)
												.on ('rw:ajaxupdate', function (event) {jQuery(event.target).multipleSelect('refresh')});
											"
									synchronousCallback = true
								>

							</cfif>

							<CFIF allowNewValue is 1>
								<cfoutput>
								<cf_translate encode="javascript">
								<cf_includejavascriptonce template = "/javascript/newSelectValue.js">
								<script>
									function checkAddNewValueToSelectBox(thisObject) {
										if (thisObject[thisObject.selectedIndex].getAttribute('addnewvalue') != null) {
											newSelectValue (thisObject,'#NewValuePopUpText#')
										}
									}
								</script>
								</cf_translate>
								</cfoutput>
							</cfif>
						</cfif>
					</CFCASE>

					<CFCASE value ="twoSelects">
						<cfif structKeyExists(arguments,"query")> <!--- This is a twoSelectsCombo with a query rather than a bind --->
							<!--- NJH 2013/06/06 - found that (in the relay tag editor), the order wasn't getting displayed properly if the 'allowSort' was set to true. It was defaulting
								to alphabetical order, or rather the order of the original query. Only doing this currently if sorting is allowed. --->
							<cfif not (structKeyExists(arguments,"query") and listfindnocase(arguments.query.columnList,"sortOrder")) and arguments.allowSort>
								<cfset queryAddColumn(arguments.query,"sortOrder","integer",arrayNew(1))>
								<cfset sortOrderAdded = true>
							</cfif>

							<cfif sortOrderAdded>
								<cfloop query="arguments.query">
									<cfset foundInList = listfindnocase(arguments.selected,arguments.query[value][currentRow])>
									<cfset querySetCell(arguments.query,"sortOrder",iif(foundInList,de(foundInList),0),currentrow)>
								</cfloop>
							</cfif>

							<!--- 	2012/03/20	IH	Case: 424412 set isSelected to 0 to prevent selections highlighting
									2013-10-15	WAB CASE 437370 use #selectedColumn# instead of isSelected
							 --->
							<cfquery name="selectedQuery" dbtype="query">
							select 0 as isSelected,* from arguments.query where #selectedColumn# = 1
							<cfif structKeyExists(arguments,"query") and structKeyExists(arguments.query,"columnlist") and listfindnocase(arguments.query.columnList,"sortOrder")>order by sortOrder</cfif>
							</cfquery>
							<cfif selectedQuery.recordcount gt 0>
								<cfloop query = "selectedQuery">
									<cfset selectedList = ListAppend(selectedList, evaluate(arguments.value))>
								</cfloop>
								<cfset arguments.selected = selectedList>
							</cfif>

							<cfquery name="notselectedQuery" dbtype="query">
							select * from arguments.query where #selectedColumn# = 0
							</cfquery>

							<cfif method is "edit">

								<cfif structKeyExists(arguments,"caption1")>
									<cfset twoSelectsCaption1 = arguments.caption1>
								</cfif>
								<cfif structKeyExists(arguments,"caption2")>
									<cfset twoSelectsCaption2 = arguments.caption2>
								</cfif>
								<cfif structKeyExists(arguments,"width")>
									<cfset twoSelectsWidth = arguments.width>
								</cfif>

								<CF_TwoSelectsComboQuery
								    NAME="List_#Name#"
								    NAME2="#Name#"
								    SIZE="#size#"
								    WIDTH="#twoSelectsWidth#px"
								    FORCEWIDTH="50"
									QUERY1="selectedQuery"
									QUERY2="notselectedQuery"
									value = #value#
									display = #display#
								    CAPTION="<FONT SIZE=-1><B>#twoSelectsCaption1#:</B></FONT>"
								    CAPTION2="<FONT SIZE=-1><B>#twoSelectsCaption2#:</B></FONT>"
									UP="#arguments.allowSort#"
									DOWN="#arguments.allowSort#"
									label= #label#
									required = #required#
									title = "#arguments.title#"
									onchange = #arguments.onChange#
									useJqueryMultiSelect = false
								>


							<!--- 2013-07-15 YMA Support Bind function with Two Selects. --->
							<cfif bind is not "">

								<cfset bindresult_js = createJavascriptBind (bind=bind,name=name,type="select",value=value,display=display,selected = selected,nullText=nullText,bindOnLoad = bindOnLoad,showNull=showNull,displayas=arguments.displayAs)>
								<cfoutput>#bindresult_js.scriptWithOnLoad_js#</cfoutput>
							</cfif>
							<cfelse>
								<cfoutput query="selectedQuery">
									#selectedQuery[display][currentRow]# <BR>
								</cfoutput>
							</cfif>
						<cfelse>
							<!--- This is a twoSelectsCombo with a bind --->
							<cfif method is "edit">
								<cfset selectedQuery = queryNew("isSelected,#value#,#display#","bit,Integer,VarChar")>

								<cfquery name="notselectedQuery" dbtype="query">
									select * from selectedQuery where isselected = 0
								</cfquery>
								<cfif selectedQuery.recordcount gt 0>
									<cfloop query = "selectedQuery">
										<cfset selectedList = ListAppend(selectedList, evaluate(arguments.value))>
									</cfloop>
									<cfset selected = selectedList>
								</cfif>

								<cfif structKeyExists(arguments,"caption1")>
									<cfset twoSelectsCaption1 = arguments.caption1>
								</cfif>
								<cfif structKeyExists(arguments,"caption2")>
									<cfset twoSelectsCaption2 = arguments.caption2>
								</cfif>
								<cfif structKeyExists(arguments,"width")>
									<cfset twoSelectsWidth = arguments.width>
								</cfif>

								<CF_TwoSelectsComboQuery
								    NAME="List_#Name#"
								    NAME2="#Name#"
								    SIZE="#size#"
								    WIDTH="#twoSelectsWidth#px"
								    FORCEWIDTH="50"
									QUERY1="#selectedQuery#"
									QUERY2="#notselectedQuery#"
									value = #value#
									display = #display#
								    CAPTION="<FONT SIZE=-1><B>#twoSelectsCaption1#:</B></FONT>"
								    CAPTION2="<FONT SIZE=-1><B>#twoSelectsCaption2#:</B></FONT>"
									UP="#arguments.allowSort#"
									DOWN="#arguments.allowSort#"
									label= #label#
									required = #required#
									title = "#arguments.title#"
								>

								<cfset bindresult_js = createJavascriptBind (bind=bind,name=name,type="select",value=value,display=display,selected = selected,nullText=nullText,bindOnLoad = bindOnLoad,showNull=showNull,displayas=arguments.displayAs)>
								<cfoutput>#bindresult_js.scriptWithOnLoad_js#</cfoutput>
							</cfif>
						</cfif>
					</CFCASE>

					<cfdefaultcase>
						<cfoutput>DisplayAs = '#arguments.displayAs#' not supported</cfoutput>
					</cfdefaultcase>

				</cfswitch>

				<cfif keepOrig and method is not "View">
					<cfoutput><CF_INPUT type="hidden" name="#name#_orig" value="#selected#"></cfoutput>
				</cfif>

			</cfsavecontent>

		<cfreturn result_html>

	</cffunction>

	<cffunction name="structCopyByRegExp" output="false">
		<cfargument name="struct">
		<cfargument name="regExp">

		<cfset var key = "">
		<cfset var result = {}>

		<cfloop collection=#struct# item="key">
			<cfif refindNoCase(regExp,key)>
				<cfset result[key] = struct[key]>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<!--- bastardised from a custom tag.  Does not deal with invalid values or doing a lookup --->
	<cffunction name="getValidValues" output="false">
		<cfargument name="fieldName" required = "true">
		<cfargument name="parentFieldName" default = "">
		<cfargument name="countryid" default = "0"> <!---DBB PROD2016-2816 - Removed type="numeric"---><!--- NJH 2016/07/28 JIRA PROD2016-1504 - typed countryID and entityID to prevent any sort of SQL injection --->
		<cfargument name="entityid" default = "0" type="numeric">
		<cf_param name="countryid" type="numericList"><!---DBB PROD2016-2816--->

		<cfset var queryString = "">
		<cfset var ValidValueFunctionResult = "">
		<cfset var validValueQuery = getValidValueQueryOrStatementFromDB (fieldname = convertFieldNameForValidValueTable(FieldName), countryid = countryid)>
		<cfset var tempArgs = "">

		<CFIF validValueQuery.recordCount IS 0 and parentFieldName is not "">
			<cfset validValueQuery = getValidValueQueryOrStatementFromDB (fieldname = convertFieldNameForValidValueTable(ParentFieldName), countryid = countryid)>
		</cfif>


		<CFIF validValueQuery.displayvalue is "*lookup">

			<!--- valid values come from a query--->
			<CFSET queryString = listFirst(ValidValueQuery.datavalue,"|") >

			<cftry>
				<CFSET queryString = evaluate('"#queryString#"')>

				<!---  runs another query getvalidvalues--->
				<cFQUERY NAME="validValueQuery" DATASOURcE=#application.sitedataSource# >
				#preservesinglequotes(queryString)#
				</CFQUERY>

				<cfcatch>  <!--- WAB 2007/11/13 moved this catch to include the cfquery as well as the evaluate --->
					<cfsetting enablecfoutputonly="no" > <!--- to reverse out the "yes" at beginning of tag --->
					<cfthrow message = "Valid Value Error: #cfcatch.message# . Field: #FieldName#"  >
				</cfcatch>
			</cftry>

		<cfelseif validValueQuery.displayvalue is "func">
			<cfset tempArgs = structCopy(arguments)>
			<cfset structDelete (tempArgs,"fieldName")>
			<cfset structDelete (tempArgs,"parentFieldName")>

			<cfset ValidValueFunctionResult = runValidValueFunction (function = ValidValueQuery.datavalue, argumentCollection = tempArgs)>
			<cfif ValidValueFunctionResult.isOK >
				<cfset validValueQuery = ValidValueFunctionResult.query>
			<cfelse>
				<cfoutput>Problem Getting Valid Values #ValidValueFunctionResult.message#</cfoutput>	<CF_ABORT>
			</cfif>

		</CFIF>


		<cfreturn validValueQuery>


	</cffunction>

	<!--- WAB 2014-10-20 Added support for passing a merge structure to this function  (an NJH request) --->
	<cffunction name="getValidValuesFromString" output="false">
		<cfargument name="string" required ="true">
		<cfargument name="excludeValues" default="">
		<cfargument name="displayColumn" default="displayValue">
		<cfargument name="valuecolumn" default="dataValue">
		<cfargument name="mergeStruct" default="#structNew()#">

		<cfset var result = "">
		<cfset var tmpresult = "">

		<cfif listFirst(string,":") is "func" or listFirst(string,":") is "cfc" >
			<cfset tmpresult = runValidValueFunction (function = listRest(string,":"), argumentCollection = arguments)>
			<cfset result = tmpresult.query>
		<cfelseif left(ltrim(string),7) is "select ">
			<!--- hack to deal with some very old valid values which have two queries joined by a |  The second part will refer to LOOKUPVALUE, we only need the first part --->
			<cfset var evaluatedString = application.com.relayTranslations.checkForAndEvaluateCFVariables (phraseText = string,mergeStruct = arguments.mergeStruct)>
			<cfif refindnocase ("\|.*lookup",evaluatedString)>
				<cfset evaluatedString = listFirst (evaluatedString,"|")>
			</cfif>
			<cfquery name = "result" datasource = "#application.siteDataSource#">
			#preserveSingleQuotes(evaluatedString)#
			</cfquery>
		<cfelseif listLen(arguments.string,".") eq 2>
			<!--- 2015-07-01	WAB CASE CASE Fifteen-395 (related to Case 444625). Some select boxes get two lots of null text
								could have added a showNull = false to call to cf_getValidValues, but better to use the function version which we have here
					2016-02-01	WAB	Consolidating code from cf_displayValidValues - pass through mergeStruct (usually containing countryid and entityid)
			--->
			<cfset result = getValidValues (fieldname = arguments.string, argumentCollection = mergeStruct)>
		<cfelse>
			<!--- must be a list --->
			<cfset result= createQueryFromValidValueList (list = string,valueColumn=arguments.valueColumn,displayColumn=arguments.displayColumn)>
		</cfif>

		<cfif excludevalues is not "">
			<!--- TODO This does assume the name of the dataValue column --->
			<cfquery name = "result" dbtype = "query">
			select * from result where dataValue not in (#preserveSingleQuotes(excludevalues)#)
			</cfquery>

		</cfif>

		<cfreturn result>

	</cffunction>


	<!---
	WAB 2013-02-06 Added support for unnamed arguments in validvalue functions
	 --->
	<cffunction name="runValidValueFunction" output="false">
		<cfargument name="function" >
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">

		<cfset var result = {isOK=true,query=queryNew("dummy")}>
		<cfset var functionStruct = getFunctionPointerFromString(function=arguments.function,mergeStruct=arguments.mergeStruct)>
		<cfset var validValueFunction = "">
		<cfset var argumentValue = ''>

		<cfif functionStruct.isOK>
			<cfif structKeyExists(functionStruct.metadata,"validvalues")>

				<!--- if any extra arguments have been passed in the merge these in (we could delete the "function" argument) --->
				<cfset structappend (functionStruct.args,arguments)>
				<cfset structappend (functionStruct.args,arguments.mergeStruct,false)>

				<!--- NJH 2013/03/25 Case 434369: use cfinvoke because if a method is calling another method within the cfc, it will find it. There are scoping issues when running it as below. --->
				<cfif isObject(functionStruct.componentPointer)>
					<cfinvoke component="#functionStruct.componentPointer#" argumentcollection="#functionStruct.args#" method="#functionStruct.method#" returnvariable="result.query">

				<!--- use this if function is in request scope --->
				<cfelse>
					<cfset validValueFunction = functionStruct.functionPointer>
					<cfset result.query = validValueFunction(argumentCollection=functionStruct.args)>
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message = "Your valid value function must have the attribute validValues">
			</cfif>
		<cfelse>
			<cfset result = functionStruct>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getFunctionPointerFromString" returnType="struct" output="false">
		<cfargument name="function" >
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">

		<cfset var groups ={1="component",2="method",3="argumentList"}>
		<cfset var regexp = "(.+?)\.([^\.]+?)(?:\Z|\((.*?)\))">
		<cfset var result = {isOK=true}>
		<cfset var functionPointer = "">
		<cfset var refindResult = "">
		<cfset var functionMetadata = "">
		<cfset var args = structNew()>
		<cfset var componentName = "">
		<cfset var componentPointer = "">
		<cfset var argumentList = "">
		<cfset var argumentValue = ''>
		<cfset var i = 0>

		<cfset var theFunction = application.com.relayTranslations.checkForAndEvaluateCFVariables (phraseText = arguments.function,mergeStruct = arguments.mergeStruct)>
		<cfset refindResult = application.com.regexp.refindAllOccurrences(regexp,theFunction,groups)>

		<cfif arrayLen(refindResult)>
			<cfset componentName = refindResult[1].component>
			<cfif listfirst(componentName,".") is "request" and listlen(componentName,".") is 1>
				<!--- ie request.myFunction () --->
				<cfset functionPointer = request[refindResult[1].method]>
			<cfelse>
				<!--- NJH 2015/12/22 - add ability to pass in the getObject function --->
				<cfif left(componentName,10) is "getObject(">
					<cfset var componentPathArray = application.com.regexp.refindAllOccurrences("\((.+?)\)",componentName,{1="componentPath"})>
					<cfif arrayLen(componentPathArray)>
						<!--- replace any single or double quotes --->
						<cfset var componentPath = replace(replace(componentPathArray[1].componentPath,"'","","ALL"),'"','','ALL')>
						<cfset componentPointer = application.getObject(componentPath)>
					</cfif>
				<cfelseif listfirst(componentName,".") is "com" or listlen(componentName,".") is 1>
					<cfset componentPointer = application.com[listlast(componentName,".")]>
				<cfelse>
					<cfset componentPointer = createObject("component",componentName)>
				</cfif>

				<cfset functionPointer = componentPointer[refindResult[1].method]>
			</cfif>

			<cfset functionMetadata = getmetadata(functionPointer)>

			<!--- if the function has been defined with any arguments then pop these in a structure --->
			<cfset argumentList = refindResult[1].argumentList>
			<!--- rather rough test for whether the arguments are named or not --->
			<cfif argumentList contains "=">
				<!--- named arguments, can just convert straight to argument structure --->
				<cfset args = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = argumentList,delimiter=",")>
			<cfelseif argumentList is not "" >
				<!--- Unnamed arguments  (added WAB 2013-02-06 )
				We need to loop through the argumentList and the function metadata array of parameters at the same time and build up the args collection --->
				<cfloop list="#argumentList#" index="argumentValue">
					<cfset i ++>
					<cfset args[functionMetadata.parameters[i].name] = argumentValue>
				</cfloop>
			</cfif>

			<cfset result.functionPointer = functionPointer>
			<cfset result.args = args>
			<cfset result.metaData = functionMetadata>
			<cfset result.componentPointer = componentPointer>
			<cfset result.method = refindResult[1].method>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Unable to extract function from string">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getValidValueQueryOrStatementFromDB" output="false">
		<cfargument name="fieldName" required = "true">
		<cfargument name="countryid" default = "0">

		<cfset var regionList = '' />
		<cfset var Countryandregionlist = '' />
		<cfset var getValidValues = '' />

		<CFIF countryid is not 0 and isnumeric(countryid ) >
			<cftry>
				<CFSET regionList = application.CountryGroupsbelongedTo[countryid]>
				<cfcatch type="any">
					<CFSET regionList = countryid>
				</cfcatch>
			</cftry>
		<CFELSE>
			<CFSET regionList = "">
		</cfif>

		<CFSET Countryandregionlist = countryid & iif(regionlist is not "",DE(",#regionList#"),DE(""))>


			<cFQUERY NAME="getValidValues" DATASOURcE=#application.sitedataSource# >
			select
				validvalue as displayvalue,
				CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
				sortorder
			 from validfieldvalues
			where fieldname  in ( <cf_queryparam value="#FieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			AND countryID = 0
			<CFIF countryandregionlist IS NOT 0>
			AND NOT EXISTS (SELEcT 1 FROM validfieldvalues
					WHERE fieldname  in ( <cf_queryparam value="#FieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					and countryID  in ( <cf_queryparam value="#countryandregionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
			</CFIF>

			<CFIF regionlist is not "">
			UNION

			select
				validvalue,
				CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
				sortorder
			from validfieldvalues
			where fieldname  in ( <cf_queryparam value="#FieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			AND countryID  in ( <cf_queryparam value="#regionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				AND NOT EXISTS (SELEcT 1 FROM validfieldvalues
								WHERE fieldname  in ( <cf_queryparam value="#FieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
								AND countryID in ( <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))

			</CFIF>
			UNION

			select
				validvalue,
				CASE WHEN isNull(datavalue,'') = '' Then ValidValue ELSE dataValue END as DataValue,
				sortorder
			from validfieldvalues
			where fieldname  in ( <cf_queryparam value="#FieldName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			AND countryID  in ( <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )   <!--- WAB 2009/03/25 to allow list of countrids to be passed, changed from = to in --->

			order by sortorder, validvalue

			</CFQUERY>

		<cfreturn getValidValues>
	</cffunction>

	<!--- 	WAB2016-02-02 modified to take delim1 as an argument, there seem to be a few places still which use other delimiters and they now all call this function
							Default set to application.delim1 and | since that it what the existing code had
	--->
	<cffunction name="createQueryFromValidValueList" output="false">
		<cfargument name="list">
		<cfargument name="displayColumn" default="display">
		<cfargument name="valueColumn" default="value">
		<cfargument name="delim1" default="#application.delim1#|">

		<!--- create a query out of the list --->
		<cfset var resultQuery = "">
		<cfset var item  = "">
		<cfset var value = "">
		<cfset var display = "">
		<cfset var count = 0>

		<cfset var valueDataType = "varchar">
		<!--- WAB 2016-02-02 added an extra check for the last item in the list being numeric as well, before assuming whole list is numeric
					If you get an error here, you really need to be dealing in queries rather than lists!
		--->
		<cfif not refind("[^0-9]",listfirst(listfirst(list),delim1)) and not refind("[^0-9]",listfirst(listlast(list),delim1))>
			<cfset valueDataType = "integer">
		</cfif>

		<cfset resultQuery = queryNew("#valueColumn#,#displayColumn#","#valueDataType#,varchar")>

		<cfloop index="item" list = "#arguments.list#">
			<cfset value = listfirst(item,delim1)>
			<cfset display = listlast(item,delim1)>
			<cfset count = count + 1>
			<cfset queryaddrow(resultQuery,1)>
			<cfset querysetcell(resultQuery,valueColumn,value,count)>
			<cfset querysetcell(resultQuery,displayColumn,display,count)>
		</cfloop>
		<cfreturn resultQuery>

	</cffunction>


	<!---
	WAB 2015-04-01
		Fieldnames are stored in the validvalues table in the format tableName.FieldName (or flagGroup.flagName)
		Within screens they are named with underscores tableName_FieldName  flagGroup_flagName
		Previously the valid value code replaced all underscores with dots.  But this fails if the flagname has underscores in it.
		This regExp replaces the first underscore with a dot but not if there is already a dot.
	--->

	<cffunction name="convertFieldNameForValidValueTable" hint="" output="false">
		<cfargument name="fieldName">

		<cfreturn reReplace (fieldName,"\A([^.]*?)_","\1.")>
	</cffunction>


	<!--- Functions copied from my reports work.  Useful when trying to resolve dataValues to displayValues (when displaying valid vlalue data in a read only mode))--->
	<cffunction name="getMetaDataForColumn" output="false">
		<cfargument name="queryObject">
		<cfargument name = "columnName">

		<cfset var metadataArray = getMetaData(queryObject)>
		<cfset var i = 0>
		<cfset var result = structNew()>

		<cfloop index="i" from = "1" to="#arrayLen(metaDataArray)#">
			<cfif metaDataArray[i].name is columnName>
				<cfset result = metaDataArray[i]>
				<cfbreak>
			</cfif>
		</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction name="doesColumnNeedQuotes" output="false">
		<cfargument name="queryObject">
		<cfargument name = "columnName">

		<cfset var metaData = getMetaDataForColumn (queryObject,columnName)>
		<cfset var result = true>
		<cftry>
			<cfif not structKeyExists (metaData,"typename")>
				<cfset result = true>
			<cfelseif listfindnocase("int,int identity,datetime,integer", metaData.typename)>
				<cfset result = false>
			</cfif>

			<cfcatch>
				<!---
				<cfoutput>relayForms.doesColumnNeedQuote(). Error getting column metadata from column #columnName#</cfoutput>
				<cfdump var="#getmetaData(queryObject)#">
				<cfdump var="#queryObject#">
				<CF_ABORT>
				--->
				<cfset result >
			</cfcatch>
		</cftry>
		<cfreturn result>

	</cffunction>


	<!---
		 WAB 2011/11/29
		 This bit of code was originally in settings
		 It took all the XML attributes of a setting and generated an attribute collection suitable for passing to relayFormElement
		Now pulled out to be used in other places (such as relayTagEditors)
		WAB 2014-10-14 Alteration to deal with having booleans stored as either Yes/No (for eg relayTags) or 0/1 (for eg settings)
			Also removed references to applicationScope which I can't think are needed

	--->
	<cffunction name="createStandardRelayFormElementAttributeStructure" output="false">
		<cfargument name="attributeStruct">
		<cfargument name="options" default="#structNew()#"> <!--- Options structure, created for handling the booleanStoredAs parameter, but could be used for other things --->

			<cfset var result = attributeStruct>
			<cfset var tempValueList = '' />
			<cfset var theQuery = '' />
			<cfset var getDefault = '' />

			<cfset var defaultOptions = {booleanStoredAs='OneZero'}>  <!--- or YesNo --->
			<cfset structAppend(arguments.options,defaultOptions,false)>

				<cfif structKeyExists(result,"type")>
					<cfset result.dataType = result.type>
					<cfset structDelete (result,"type")>
				</cfif>

				<cfif structKeyExists(result,"control")>
					<cfset result.relayFormElementType = result.control>
					<cfset structDelete (result,"control")>
				<cfelseif not structKeyExists(result,"relayFormElementType")>
					<cfset result.relayFormElementType = "Text">
				</cfif>

				<cfif not structKeyExists(result,"fieldname")>
					<cfset result.fieldname = replaceNoCase(result.name,"attributes.","")>
					<cfset structDelete (result,"name")>
				</cfif>

				<cfif not structKeyExists(result,"label")>
					<cfset result.label = application.com.regExp.camelCaseToSpaces(result.fieldname)>
				</cfif>

				<cfif not structKeyExists(result,"noteTextPosition")>
					<cfset result.noteTextPosition = "right">
				</cfif>
				<cfif not structKeyExists(result,"maxLength")>
					<cfset result.maxLength = "4000">
				</cfif>

				<cfif structKeyExists(result,"dataType") >
					<cfif listfindnocase("bit,boolean",result.dataType) >
						<cfswitch expression="#arguments.options.booleanStoredAs#">
							<cfcase value="OneZero">
								<cfparam name="arguments.options.booleanValidValues" default="1#application.delim1#Yes,0#application.delim1#No">
							</cfcase>
							<cfcase value="YesNo">
								<cfparam name="arguments.options.booleanValidValues" default="Yes,No">
							</cfcase>
						</cfswitch>
						<cfset result.ValidValues   = options.booleanValidValues>
						<cfset result.relayFormElementType = "select">

						<cfset result.displayAs = "radio">
						<cfset result.column = 2> <!--- for display valid values --->
						<cfset result.showNull = false>
					<cfelseif listfindnocase("integer,numeric",result.dataType)>
						<cfset result.validate  = "integer">
					<cfelseif listfindnocase("NumericList",result.dataType)>
						<cfset result.SubmitFunction  = "validateNumericList">
					</cfif>
				</cfif>

				<cfif structKeyExists(result,"validValues")>
					<cfset result.query = application.com.relayForms.getValidValuesFromString(string = result.validValues)>
					<cfset result.relayFormElementType = "select">
					<cfset structDelete (result,"validValues")>


					<cfif structKeyExists(result,"displayValue")>
						<cfset result.display=result.displayValue>
						<cfset structDelete (result,"displayValue")>
					<cfelse>
						<!--- check that displayValue defaults to something which is in the query --->
						<cfif listfindnocase(result.query.columnList,"display")>
								<cfset result.display="display">
						<cfelseif listfindnocase(result.query.columnList,"displayvalue")>
								<cfset result.display="displayvalue">
						</cfif>
					</cfif>
					<cfif structKeyExists(result,"dataValue")>
						<cfset result.value=result.dataValue>
						<cfset structDelete (result,"dataValue")>
					<cfelse>
						<!--- check that dataValue defaults to something which is in the query --->
						<cfif listfindnocase(result.query.columnList,"value")>
								<cfset result.value="value">
						<cfelseif listfindnocase(result.query.columnList,"datavalue")>
								<cfset result.value="datavalue">
						</cfif>

					</cfif>
				</cfif>

				<!--- WAB 2017-02-22	PROD2016-3479  Add support for bind functions to settings--->
				<cfif structKeyExists(result,"bindFunction")>
					<cfset result.relayFormElementType = "select">
				</cfif>	

				<!--- WAB 2016-02-02 some adjustment in this area to try to make sure that multiple and displayAs are in synch
						And to deal with places where we need to force the use of twoselectscombo - eg when ordering is needed
				 --->
				<cfif result.relayFormElementType is "select">

					<cfparam name = "result.multiple" default="false">
					<cfparam name = "result.displayAs" default="select">

					<cfif listfindNoCase ("multiselect,twoselects,checkbox",result.displayAs) and not result.multiple>
						<cfset result.multiple = true>
					</cfif>

					<!--- WAB 2016-11-28 PROD2016-2831 I have been able to remove this line because validValueDisplay() now reacts correctly to allowSort=true anyway.
										Not sure why I didn't do that in the first place
					<cfif result.multiple and structKeyExists (result,"allowSort") and result.allowSort>
						<cfset result.usejQueryMultiSelect = false>
					</cfif>
					 --->

					<cfparam name="result.showNull"  default = #(result.multiple)?false:true#>
					<!--- in some parts of the system (relaySelectField) NullText is king, so is shown even if showNull is false, so will only param it if showNull is true --->
					<cfif result.showNull>
						<cfparam name="result.NullText"  default = "phr_Ext_SelectAValue">
					</cfif>
				</cfif>



				<cfif structKeyExists (result,"query") and structKeyExists (result,"default") and result.default is not "">

					<cftry>

						<cfif application.com.relayforms.doesColumnNeedQuotes(result.query,result.value)>
							<cfset tempValueList = listQualify(lcase(result.default),"'")>
						<cfelse>
							<cfset tempValueList = result.default>
						</cfif>
							<cfset theQuery = result.query>
							<cfquery name ="getDefault" dbType="Query">
							select #result.display# as displayValue from
							 theQuery
							where LOWER([#result.value#])  in (#preserveSingleQuotes(tempValueList)#)
							</cfquery>
						<cfif getDefault.recordcount>
							<cfset result.default = valueList(getDefault.displayValue)>
						</cfif>

					<cfcatch>
						<!--- we are just going to have to ignore errors
						value will be returned as result
						--->

					</cfcatch>
				</cftry>
				</cfif>

				<cfif structKeyExists(result,"required")>
					<cfif listFindNoCase ("NO,FALSE", result.required)>
						<cfset result.required = 0>
					<cfelseif listFindNoCase ("YES,TRUE", result.required)>
						<cfset result.required = 1>
					</cfif>
				</cfif>

		<cfreturn result>
	</cffunction>


	<!--- WAB 2016-11-10 PROD2016-2731 New version of convertTagAttributeStructToValidAttributeList, much (10x) faster
			Old version and unit test in _Support\tests\unit\com\relayForms\convertTagAttributeStructToValidAttributeList.cfc
 --->
	<cffunction name="convertTagAttributeStructToValidAttributeList" returntype="string" output="false">
		<cfargument name="attributeStruct" type="struct" required="true">
		<cfargument name="tagName" type="string" default="input">

		<cfset var tagNameAttributes = this.tagAttributeStructures.byTagName[arguments.tagName]>
		<!--- NJH 2016/02/26 - check for type specific attributes --->
		<cfif structKeyExists(arguments.attributeStruct,"type") and arguments.attributeStruct.type neq "" and structKeyExists(this.tagAttributeStructures.byType,arguments.attributeStruct.type)>
			<cfset tagNameAttributes = this.tagAttributeStructures.byType[arguments.attributeStruct.type]>
		</cfif>

		<cfloop list="disabled,readonly,multiple,required,checked" index="attr">
			<cfif structKeyExists(arguments.attributeStruct,attr)>
				<cfif isBoolean(arguments.attributeStruct[attr]) and not arguments.attributeStruct[attr]>
					<cfset structDelete(arguments.attributeStruct,attr)>
				<cfelseif attr neq "required">
					<cfset arguments.attributeStruct[attr] = attr>
				</cfif>
			</cfif>
		</cfloop>

		<cfset local.quote = '"'>
		<cfset local.list = "">
		<cfloop collection=#attributeStruct# item="local.key">
			<cfif structKeyExists(attributeStruct,local.key)>
				<cfset local.value = attributeStruct[local.key]>
				<!--- NJH 2017/01/30 - PROD2016-3317 - allow an attribute of 'value' to be output even if it has an empty string --->
				<cfif (structKeyExists (this.tagAttributeStructures.global,local.key) OR structKeyExists (tagNameAttributes,local.key)  OR refindNoCase(this.tagAttributeLists.global.regExp,local.key) neq 0)   and isSimpleValue(local.value) and (local.value neq "" or local.key eq "value")>
					<cfset local.list = ListAppend(local.list, lcase(local.key) & "=" & quote & encodeForHTMLAttribute(local.value) & quote," ")>
				</cfif>
			</cfif>
		</cfloop>

		<cfreturn local.list>
	</cffunction>


		<!--- 2014-09-10	RPW		Add Lead Screen on Portal --->
	<cffunction name="getLookupList" access="public" output="false" returntype="query" ValidValues="true">
		<cfargument name="fieldName" type="string" required="false" default="">
		<cfargument name="textID"  type="string" required="false" default="">
		<cfargument name="lookupID"  type="string" required="false" default="0">
		<!---<cfargument name="textIDList" type="string" required="false" default="">--->
		<cfargument name="getDefault" type="boolean" required="false">
		<cfargument name="excludeTextId" type="string" required="false" default="">

		<cfset var qLookupList = queryNew('') />

		<cfquery name="qLookupList">
			SELECT
				lookupID,
				fieldName,
				itemText,
				lookUpTextID,
				'phr_' + FieldName + '_' + lookUpTextID translatedItemText
			FROM lookupList
			WHERE isLive = 1
			<cfif len(trim(arguments.fieldName)) GT 0>
				AND fieldName = <cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.fieldName#">
			</cfif>
			<cfif len(trim(arguments.textID)) GT 0>
				AND lookupTextID IN (<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.textID#" list="true">)
			</cfif>
			<cfif ListLen(arguments.excludeTextId) GT 0>
				AND lookupTextID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.excludeTextId#" list="true">)
			</cfif>
			<cfif val(trim(arguments.lookupID)) GT 0>
				AND lookupID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.lookupID#">
			</cfif>
			<cfif structKeyExists(arguments,"getDefault") and arguments.getDefault>
				AND isDefault = <cf_queryparam cfsqltype="cf_sql_bit" value="#arguments.getDefault#">
			</cfif>
			ORDER BY sortOrder,lookupID
		</cfquery>
		<cfreturn qLookupList>
	</cffunction>

	<!--- NJH 2015/05/12 - create function to get default lookup value --->
	<cffunction name="getDefaultLookupListForField" output="false" returnType="query">
		<cfargument name="fieldName" type="string" required="true">

		<cfreturn getLookupList(fieldname=arguments.fieldname,getDefault=true)>
	</cffunction>


	<!--- WAB 2016-02-11 BF-449 --->
	<cffunction name="escapeIDForjQuery" output="false" returnType="string" hint="Escapes . HASH [ ] with \\ so jQuery does not treat them as CSS notation">
		<cfargument name="string" type="string" required="true">
			<!--- My regExp replace failed - \\\\\1 just did not work, so have had to do in two passes!
				 would actually probably be better as a js function replace( /(:|\.|\[|\]|,)/g, "\\$1" )
			--->
			<cfset var result = reReplaceNoCase ( string, "(:|\.|\[|\]|,)", "\\\1","ALL" )>
			<cfset result = reReplaceNoCase ( result, "(\\(:|\.|\[|\]|,))", "\\\1","ALL" )>
		<cfreturn result>
	</cffunction>

</cfcomponent>