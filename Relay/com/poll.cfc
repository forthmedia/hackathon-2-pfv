<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Polling component" hint="Manages poll functions">
	<cfset datasource="relaydb">
	<cffunction name="getQuestions" access="remote" returntype="query">
		<cfargument name="dataSource" type="string" required="true">
		<cfquery name="pollRecords" datasource="#dataSource#">
			SELECT * FROM pollQuestion 
		</cfquery>
		<cfreturn pollRecords>
	</cffunction>
	
	<cffunction name="getChoices" access="remote" returntype="query">
		<cfargument name="dataSource" type="string" required="true">
		<cfargument name="questionID" type="numeric" required="true">
		<cfquery name="choiceRecords" datasource="#dataSource#">
			SELECT * FROM pollChoice WHERE pollQuestionID = #questionID#
		</cfquery>
		<cfreturn choiceRecords>
	</cffunction>
	
	<cffunction name="checkResponse" access="remote" returntype="query">
		<cfargument name="choiceID" type="any" required="true">
		<CFSCRIPT>
			if(IsArray(choiceID)){
				for(i = 1; i LTE ArrayLen(choiceID); i = i + 1){
					returnObj = addResponse(choiceID[i]); 		
				}
			} else {
				returnObj = addResponse(choiceID);
			}
			return returnObj;
		</CFSCRIPT>
	</cffunction>
	
	<cffunction name="addResponse" access="private" returntype="query">
		<cfargument name="choiceID" type="numeric" required="true">
		<!--- <cfargument name="dataSource" type="string" required="true"> --->
		<cfquery name="responseRecord" datasource="relaydb">
			INSERT INTO pollResponse(pollChoiceID, IPAddress)
			VALUES (<cf_queryparam value="#choiceID#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#CGI.REMOTE_ADDR#" CFSQLTYPE="CF_SQL_VARCHAR" >)
		</cfquery>
		<cfreturn responseRecord>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Get Question counts                            
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="functionName" returnType="query"
		hint="description">
		<cfargument name="tableName" type="string" required="true">
		<cfargument name="dataSource" type="string" required="true">
		
		<cfquery name="functionName" datasource="#dataSource#">
			SELECT pollQuestionText, pollChoiceText,count(distinct ipAddress) from pollResponse pr
			RIGHT OUTER JOIN pollChoice pc ON pr.pollchoiceID = pc.pollChoiceID
			INNER JOIN pollQuestion pq ON pc.pollQuestionID = pq.pollQuestionID
			group by pollQuestionText, pollChoiceText		
		</cfquery>
		<cfreturn functionName>
	</cffunction>	

</cfcomponent>