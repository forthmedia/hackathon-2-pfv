<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Relay Scorecard" hint="Provides functions used in the scorecard.">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 
	<cffunction access="public" name="getScorecardDef" hint="Returns the scorecard definition" returntype="query">
		<cfargument name="scorecardDefID" type="numeric" required="yes">
		
		<cfscript>
			var qryGetScorecardDef = "";
		</cfscript>
		
		<cfquery name="qryGetScorecardDef" datasource="#application.siteDataSource#">
			select * from scorecardDef where scorecardDefID = #arguments.scorecardDefID#
		</cfquery>

		<cfreturn qryGetScorecardDef>
	</cffunction>
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                                                                                                             
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
 
	<cffunction access="public" name="getScorecardSectionDef" hint="Returns the scorecard section definition" returntype="query">
		<cfargument name="sectionDefID" type="numeric" required="yes">
		<cfargument name="scorecardDefID" type="numeric" required="yes">
		
		<cfscript>
			var qryGetScorecardSectionDef = "";
		</cfscript>
		
		<cfquery name="qryGetScorecardSectionDef" datasource="#application.siteDataSource#">
			select * from scorecardSectionDef where sectionDefID = #arguments.sectionDefID#
				and scorecardDefID = #arguments.scorecardDefID#
		</cfquery>

		<cfreturn qryGetScorecardSectionDef>
	</cffunction>

</cfcomponent>
