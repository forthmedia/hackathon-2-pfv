/**
 * saml
 * 
 * @author Richard.Tingle
 * @date 19/04/16
 * 
 * 2016/06/10	RJT	PROD2016-435 Added method to get definitions for other sites as SAML SP goes to the portal
 **/
component accessors=true output=false persistent=false {


	public any function getRelaywareAsSPConfigForCurrentSite() output="false" {

		var protocol=application.com.settings.getSetting("SAML.SP.requireHTTPS") ? "https://" : "http://";
		return application.javaloader.create("singleSignOn.samlserviceprovider.RelaywareAsSPConfiguration").init(
		protocol & cgi.server_name,
		protocol & cgi.server_name & "/singleSignOn/SAML/SP/assertionConsumer.cfm" );
	}
	
	public any function getRelaywareAsSPConfigForADomain(required string siteDomain) output="false" {
		var domain=lcase(arguments.siteDomain); //RJT standardise to lower case for consistency
		
		var domainURL="";
		if (application.com.relaycurrentSite.isSiteSecure(domain)){
			domainURL="https://#domain#";
		}else{
			domainURL="http://#domain#";
		}
		return application.javaloader.create("singleSignOn.samlserviceprovider.RelaywareAsSPConfiguration").init(
		domainURL,
		domainURL & "/singleSignOn/SAML/SP/assertionConsumer.cfm" );
	}
	
	public void function checkAndWarnIfIsSecureMisconfigured(){
		//i'm using cgi.HTTP_REFERER as a best guess at the current domain that should remain intact behind a load balancer
		if (FindNoCase("http", cgi.HTTP_REFERER) NEQ 0 ){ //check if cgi.HTTP_REFERER available at all, if not available just don't warn
			//request.currentSite and request.protocalAndDomain all trust isSecure so have to use cgi
			var isSecureByDomain=FindNoCase("https", cgi.HTTP_REFERER) ? 1 : 0;
			var isSecureBySetting=application.com.relaycurrentSite.isSiteSecure(request.currentSite.DOMAIN);
				
				
			var misConfiguredIsSecure = isSecureByDomain NEQ isSecureBySetting;
		
			if (misConfiguredIsSecure){
				var warningPhrase=isSecureBySetting ? 'phr_SAML_misconfigured_isSecure_setAsSecure' : 'phr_SAML_misconfigured_isSecure_setAsNonSecure';
				application.com.relayui.setMessage(message=warningPhrase & " phr_SAML_misconfigured_fixInstructions", type="error", closeAfter="0");	
			}
		}
	}
	
	public query function getAllIdentityProviders(boolean activeOnly=false, String sortOrder="identityProviderName")  validvalues="true"{
		var idpQuery=new Query();
		idpQuery.setDatasource(application.sitedatasource);
		idpQuery.addParam(name="activeOnly",value=arguments.activeOnly,cfsqltype="cf_sql_bit"); 
		
		var sqlSafe_identityProviderName=application.com.security.queryObjectName(arguments.sortOrder);
		
		var result = idpQuery.execute(sql="select SAMLIdentityProviderID, identityProviderName, identityProviderEntityID,authnRequestURL "&
											"from SAMLIdentityProvider "&
											"where (active=1 or :activeOnly = 0) " &
											"order by #sqlSafe_identityProviderName# " 
											); 
											
		return result.getResult();
		
	}
	
}