<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		salesForce.cfc	
Author:			NJH  
Date started:	09-09-2010
	
Description:	SalesForce functions		

Amendment History:

Date 		Initials 	What was changed
2010-09-09	NJH			LID 3980 - removed errors for errors table that were no longer errors.. primarily with account manager not being set, when it had been set
2011-01-31	PPB			LID5510 - references to application.flag changed to use application.com.flag.getFlagStructure()
2010-10-10			NYB			Created function getSalesForceUserID which imports ID for a user from Salesforce.  Not sure if this should have been incorportated into function getSalesForceIDForEntity in some way
2011-01-05			NYB			Fixed importing of opp products
2012-08-15	NJH			CASE 429891: check that the flagTextID and flagGroupTextID neq empty string before putting them in the xml search string.
2012-10-03	WAB		changed attribute dontSendEvaluationErrorWarningEmail to logMergeErrors (and NOT'ed)
2012/11/27	NJH/WAB 	CASE 432014 Fix problem when person moves organisation
2012/12/03	NJH/WAB 	CASE 432014 Further work to fix this problem
2012/12/12	NJH			Removed the check around whether value was 0. Let 0's get by in the convert object, and deal with them where needed (ie. before exporting an entity with ID 0). Also store data as JSON in SFerrorLog.
2012-12-19 	PPB 		Case 432692 search error string for "500" rather than eq "500 Internal Server Error" (in case eg "500 Server Error" is returned) 
2013-01-23	PPB			Case 433103 additional error reporting for 500 errors
2013-01-24	PPB			Case 433103 don't attempt to process an object that isn't mapped (in this case organisation)
2013-03-13 	NJH 		Case 434168 introduce throwErrorOn500Response
2013-03-19 	PPB 		Case 434331 belt and braces (on top of fix 429891 which was released in conjunction with this)
2013-03-20 	PPB 		Case 434378 don't build queryString until we know mappedObjectStruct.objectColumnList is good
2013-04-09 	PPB 		Case 434168 don't send email if throwErrorOn500Response=false
2013-05-07 	PPB 		Case 434976 create error if the RW account manager doesn't have a crmPerId (fix removed from core 2013-10-24 should be Lenovo only)
2013-07-05 	PPB 		Case 436049 throw error if import failed
2013-07-15 	PPB 		Case 436080 replaced function deleteSalesForceIDForEntity with deleteSalesForceIDForEntityV2 to cope with bulk deletes
2013-08-08 	PPB			Case 435688 prevent making an entry into salesforceerror causing an error
2013-08-21 	PPB         Case 436649 add option for mapping attribute datatype="numeric"
2013/08/14 	NJH			Case 435632 - factorised function for creating soap packets for the create/update methods. If the SF object has a length in its metadata, use it to retrieve the 'length' leftmost characters
2013-09-12 	PPB 		Case 436891 doesEntityExist() returns as a failure when processing location and processorder="location,person" and location exists and person doesn't
2013-10-14 	PPB 		Case 437176 rejig the way errors are inserted
2013-11-21 	WAB 	CASE 438130 Modify call to getSetting to put less load on the settings system
2014-02-05 	PPB 		Case 438759 give additional error info
2014-02-26 	PPB 		Case 439001 excludeRecordsFlaggedForDeletion when calling doesEntityExist
2014/04/29	NJH			Case 439537 - added cflock around the xmlsearch
2014-04-29 	PPB 		Case 439537 added var
2014-05-12 	PPB 		Case 439347 use structKeyExists() before structCount()'s
2014-05-22 	PPB 		Case 440416 changed argument default to false in deleteSalesForceIDForEntityV2
2014-05-22 	LAH			Case 440431 changed to outer join to pick up rows that are not in RW
2014-05-30	PPB			Case 440240 released a line of code done by MS for P-EVA003 marked 2014-04-30 - bug causing crmOppId='0'
2014-06-05 	PPB 		Case 440430 error logging
2014-09-19 	PPB 		Case 441579 switched if logic for tmpObjectID
2014-10-02	NJH			fixes to getSynchPendingRecords, for bulk queue processing.  reverted some changes from 440431 since threshhold was being ignored which would have failed on large numbers of records 1000+
2014-10-03	AXA			switched outer join to inner join for queued records to use threshhold number.
2014-11-04	AHL			Case 442476 Lead Synch Jammed
2015-02-02	AHL		Case 443724 Max Sync Attempts (MSA)
Possible enhancements:


 --->
 
 
<cfcomponent name="SalesForce" extends="com.salesforceCore">
	<cfset this.m_entityType = ""/>	<!--- 2015-02-02 AHL Case 443724 Max Sync Attempts. This is a member variable (field OO) used to have a track of the entity synching. This should be modified only at the time that the entity is known, and reset later --->


	<cffunction name="Send" access="public" output="false" returntype="Struct" hint="Runs a SalesForceAPI method">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object on which the operation is run">
		<cfargument name="method" type="String" required="true" hint="The SalesForce method to call">
		<cfargument name="objectID" type="String" required="false" hint="The Id of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="entityID" type="numeric" required="false" hint="The related entityID of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="name" type="string" required="false" hint="The name of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="opportunityID" type="string" required="false" default="" hint="The associated opportunityID. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="role" type="string" required="false" default="" hint="The role of the object. This is used for logging purposes"> <!--- used for error logging purposes --->		<!--- 2014-06-05 PPB Case 440430 add default to avoid an [undefined struct element] in errorStruct which prevented a logSFError --->
		<cfargument name="ignoreBulkProtection" type="boolean" default="false"> <!--- for pricebook import, we want to ignore the bulk protection put in place as we want all the records --->
		<cfargument name="throwErrorOn500Response" type="boolean" default="false"> <!--- if we get a 500 from salesforce, throw an error or continue processing. THis is only used when we do an initial query --->  <!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->
		
		<cfreturn application.com.salesforceAPI.send(argumentCollection=arguments)>
	</cffunction>
	

	<cffunction name="runMappingFunction" access="private" output="false" hint="Runs a function as listed in the mapping XML.">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		<cfargument name="functionName" type="string" required="true" hint="The name of the function to run. Will currently be passed in as func:com.salesForce....">
		
		<cfscript>
			var returnValue = "";
			var thisFunction = arguments.functionName;
			var result = "";
			var columnKey = "";
			var functionArray = "";
		</cfscript>
		
		<cfif left(thisFunction,5) eq "func:">
			<cfset thisFunction = right(thisFunction,len(thisFunction)-5)>
		</cfif>
		
		<cfif listFirst(thisFunction,".") eq "com">
			<cfset thisFunction = right(thisFunction,len(thisFunction)-4)>
		</cfif>
		
		<!--- set any merge variables that may be needed --->
		<cfif application.com.relaytranslations.checkForCfVariables(thisFunction)>
			<cfset thisFunction=application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=thisFunction,mergeStruct=arguments.mergeStruct,logMergeErrors=false)>
		</cfif>
		
		<cftry>
			<cfset functionArray = application.com.regExp.getFunctionCallFromString(thisFunction)>
			<cfif arrayLen(functionArray) neq 1>
				<cfthrow message="Function #thisFunction# does not exist">
			<cfelse>
				<cfinvoke component=#application.com[functionArray[1].component]# method=#functionArray[1].method# argumentCollection=#functionArray[1].argumentStruct# returnvariable="result">
			</cfif>
			
			<!--- maybe a bit dirty, but am unsure of how else to do it. This is for backwards compatibility to get the pricebook ID
				value="func:com.opportunity.getOppPricebook(opportunityId='[[merge.entityID]]',getVendorPricebookOnly=true).crmPricebookID_orig"
				
				The function needs to return the crmPricebookID_orig value rather than the actual query
				TODO: might need to use a regular expression to extract this from the string..
			 --->
			<cfif find(").",arguments.functionName)>
				<cfset columnKey = listLast(arguments.functionName,").")>
				<cfif columnKey neq "" and columnKey neq arguments.functionName>
					<cfset result = result[columnKey]>
				</cfif>
			</cfif>

			<cfcatch>
				<cfmail to="#application.com.settings.getSetting('salesForce.adminEmail')#" from="errors@relayware.com" type="HTML" subject="Error in running function on #request.currentSite.domainAndRoot#">
					<cfdump var="#thisFunction#"><cfdump var="#functionArray#"><cfdump var="#cfcatch#">
				</cfmail>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce runMappingFunction",Severity="error",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="setDefaultValues" access="private" returntype="struct" output="false" hint="Set default values for a given Relayware entity or SalesForce object">
		<cfargument name="objectDetails" type="struct" required="true" hint="Structure to hold the Relayware entity or SalesForce object details">
		<cfargument name="object" type="string" required="true" hint="The name of the Relayware entity or SalesForce object">
		<cfargument name="direction" type="string" default="RW2SF">
		<cfargument name="entityID" type="numeric" default=0>
		<cfargument name="objectID" type="string" default="">
		<cfargument name="mergeStruct" type="struct" default="#structNew()#">
		
		<cfscript>
			var mappedObjectStruct = getMappedObject(object=arguments.object,direction=arguments.direction);
			var mappedObjectList = mappedObjectStruct.objectList;
			var currentObject = "";
			var node = "";
			var mappingArray = arrayNew(1);
			var mappingAttributes =structNew();
			var defaultValue = "";
			var mappedObjectName = "object";
			var mappedColumnName = "objectColumn";
			var table="table";
			var column="column";
			var currentColumn = "";
			var com = application.com;
		</cfscript>

		<cfif arguments.direction neq "RW2SF">
			<cfset mappedObjectList = mappedObjectStruct.tableList>
			<cfset mappedObjectName = "table">
			<cfset mappedColumnName = "column">
			<cfset table="object">
			<cfset column="objectColumn">
		</cfif>
		
		<!--- this will pass on the entityID to the flag function and other useful bits of data that we may need/want --->
		<cfset arguments.mergeStruct["entityID"] = arguments.entityID>
		<cfset arguments.mergeStruct["objectID"] = arguments.objectID>
		<cfset arguments.mergeStruct["direction"] = arguments.direction>
		<cfset structAppend(arguments.mergeStruct,arguments.objectDetails)>

		<!--- setting column values that are default values.. The mapping document will have the tablename and column fields empty, but will have a value --->
		<!--- if SF2RW, then mappedObjectList = tablelist; otherwise it is the object --->
	 	<cfloop list="#mappedObjectList#" index="currentObject">
	 		<cflock name="SalesforceXMLSearch" timeout="3">
				<cfset mappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[@#mappedObjectName#='#currentObject#'][@#table#=''][@#column#='']")>
			</cflock>
			
			<cfloop array="#mappingArray#" index="node">
				<cfset mappingAttributes = node.xmlAttributes>
				
				<!--- don't set values where we have a flagID.. flags are set a little later on --->
				<cfif structKeyExists(mappingAttributes,"value") and not structKeyExists(mappingAttributes,"flagId") and not structKeyExists(mappingAttributes,"flagGroupID")>
					<cfset defaultValue = mappingAttributes.value>
					<cfset currentColumn = mappingAttributes[mappedColumnName]>
					
					<cfif application.com.relaytranslations.checkForCfVariables(defaultValue)>
						<cfset defaultValue=application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=defaultValue, mergeStruct = arguments.mergeStruct)>
					</cfif>
					
					<cfif listFirst(defaultValue,":") eq "func">
						<cfset defaultValue = runMappingFunction(functionName=defaultValue)>
					</cfif>
					<cfset arguments.objectDetails[currentColumn] = defaultValue>
				</cfif>
			</cfloop>
		</cfloop>
		<cfreturn arguments.objectDetails>
	</cffunction>


	<cffunction name="getMappedObject" access="public" hint="Returns the corresponding table/object for a given object/table" returntype="struct" output="false">
		<cfargument name="object" type="string" required="true" hint="Can be an object or table name, depending on the direction">
		<cfargument name="direction" type="string" default="RW2SF" hint="Can be RW2SF or SF2RW">
		<cfargument name="role" type="string" default="" hint="An opportunityContactRole role">
		
		<cfscript>
			var processOrderList = "";
			var node = "";
			var mappedArray = arrayNew(1);
			var entity = "";
			var objectName = "object";
			var tableName = "";
			var IdMappingArray = arrayNew(1);
			var thisObject = "";
			var objectColumnList = "";
			var entityColumnList = "";
			var tableList = "";
			var objectList = "";
			var flagList = "";
			var roleSearch = "";
			var structKey = "";
			var thisEntity = "";
			var flagGroupQry = "";
			var objectTableName = "table";
			var synchedObjectColumnList = "";
			var synchedEntityColumnList = "";
			var synchedFlagList = "";
		</cfscript>
		
		<cfif arguments.direction eq "RW2SF">
			<cfset objectName = "table">
			<cfset objectTableName = "object">
		</cfif>
		
		<cfif not structKeyExists(application,"sfMappingXml")>
			<cfset loadSFMappingsXML()>
		</cfif>
		
		<!--- a structure to hold some mapping conversions based on the xml --->
		<cfif not structKeyExists(request,"mappedObjectInfo")>
			<cfset request.mappedObjectInfo = structNew()>
		</cfif>
		
		<!--- if a role has been passed through (in the case of opportunityContactRole or partner), then use that as part of the search and
			as part of the structure key --->
		<cfif structKeyExists(arguments,"role") and arguments.role neq "">
			<cfset roleSearch="[translate(@role,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.role)#']">
		</cfif>

		<cfset structKey = "#arguments.object##arguments.role#-#arguments.direction#">
		
		<!--- do the processing only if the request variable does not exist --->
		<cfif not structKeyExists(request.mappedObjectInfo,structKey)>
		
			<cfset request.mappedObjectInfo[structKey] = structNew()>
			<!--- get the mappings for the given entity or object --->
			<cflock name="SalesforceXMLSearch" timeout="3">
				<cfset mappedArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@#objectName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.object)#']#roleSearch#")>
			</cflock>
	
			<cfloop array="#mappedArray#" index="node">
				<cfif not listFindNoCase(processOrderList,node.xmlAttributes[objectTableName]) and node.xmlAttributes[objectTableName] neq "">
					<cfset processOrderList = listAppend(processOrderList,node.xmlAttributes[objectTableName])>
					<cfif arguments.direction eq "RW2SF">
						<cfset objectList = listAppend(objectList,node.xmlAttributes[objectTableName])>
					<cfelse>
						<cfset tableList = listAppend(tableList,node.xmlAttributes[objectTableName])>
					</cfif>
				</cfif>
				<cfif node.xmlAttributes.objectColumn neq "">
					<cfif arguments.direction eq "RW2SF">
						<cfset objectColumnList = listAppend(objectColumnList,node.xmlAttributes.objectColumn)>
						<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "rw2sf">
							<cfset synchedObjectColumnList = listAppend(synchedObjectColumnList,node.xmlAttributes.objectColumn)>
						</cfif>
					<cfelse>
						<cfif node.xmlAttributes.column neq "">
							<cfset entityColumnList = listAppend(entityColumnList,node.xmlAttributes.column)>
							<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "sf2rw">
								<cfset synchedEntityColumnList = listAppend(synchedEntityColumnList,node.xmlAttributes.column)>
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfloop>
			
			<!--- we have an object that isn't a direct mapping, such as a contact that can map to both a location and a person --->
			<cfif listLen(processOrderList) gt 1>
				<cfif arguments.direction eq "SF2RW">
					<cfif arguments.object eq "contact">
						<!--- if we're mapping a contact, we want to process the person last --->
						<cfif listFindNoCase(processOrderList,"person")>
							<cfset processOrderList = listDeleteAt(processOrderList,listFindNoCase(processOrderList,"person"))>
							<cfset processOrderList = listAppend(processOrderList,"person")>
						</cfif>
					</cfif>
				<cfelse>
					<cfif arguments.object eq "opportunity">
						<!--- if we're mapping an opportunity, we want to process the opportunity first --->
						<cfif listFindNoCase(processOrderList,"opportunity")>
							<cfset processOrderList = listDeleteAt(processOrderList,listFindNoCase(processOrderList,"opportunity"))>
							<cfset processOrderList = listPrepend(processOrderList,"opportunity")>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			
			<!--- we want to get the main object/entity that the incoming object/entity maps to. This is based by finding the related entity where
				the object ID maps to --->
			<cfif arguments.direction eq "RW2SF">
				<cfloop list="#processOrderList#" index="thisObject">
					<cflock name="SalesforceXMLSearch" timeout="3">
						<cfset IdMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(thisObject)#'][translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='id']")>
					</cflock>
					
					<cfif arrayLen(IdMappingArray) eq 1>
						<cfset entity = IdMappingArray[1].xmlAttributes.object>
						<cfbreak>
					</cfif>
				</cfloop>
			<cfelse>
				<cflock name="SalesforceXMLSearch" timeout="3">
					<cfset IdMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.object)#'][translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='id']")>
				</cflock>
				
				<cfif arrayLen(IdMappingArray) eq 1>
					<cfset entity = IdMappingArray[1].xmlAttributes.table>
				</cfif>
			</cfif>
			
			<cfloop list="#processOrderList#" index="thisEntity">
				<cflock name="SalesforceXMLSearch" timeout="3">
					<cfset mappedArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@#objectTableName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(thisEntity)#']")>
				</cflock>
						
				<cfloop array="#mappedArray#" index="node">
					<!--- <cfif direction eq "SF2RW"> --->
						<cfif not listFindNoCase(objectList,node.xmlAttributes.object) and node.xmlAttributes.object neq "">
							<cfset objectList = listAppend(objectList,node.xmlAttributes.object)>
						</cfif>
						<cfif node.xmlAttributes.objectColumn neq "" and not listFindNoCase(objectColumnList,node.xmlAttributes.objectColumn)>
							<cfset objectColumnList = listAppend(objectColumnList,node.xmlAttributes.objectColumn)>
							<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "rw2sf">
								<cfset synchedObjectColumnList = listAppend(synchedObjectColumnList,node.xmlAttributes.objectColumn)>
							</cfif>
						</cfif>
					<!--- <cfelse> --->
						<cfif not listFindNoCase(tableList,node.xmlAttributes.table) and node.xmlAttributes.table neq "">
							<cfset tableList = listAppend(tableList,node.xmlAttributes.table)>
						</cfif>
						<cfif node.xmlAttributes.column neq "" and not listFindNoCase(entityColumnList,node.xmlAttributes.column)>
							<cfset entityColumnList = listAppend(entityColumnList,node.xmlAttributes.column)>
							<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "sf2rw">
								<cfset synchedEntityColumnList = listAppend(synchedEntityColumnList,node.xmlAttributes.column)>
							</cfif>
						</cfif>
					<!--- </cfif> --->
				</cfloop>
			</cfloop>

			<!--- this could be in the case of a partner and opportunityContactRole --->
			<cfif objectList eq "">
				<cfset objectList = arguments.object>
			</cfif>
			
			<cfif arguments.direction eq "RW2SF">
				<cfset tablename=arguments.object>
			<cfelse>
				<cfset tablename = entity>
			</cfif>

			<!--- get any associated flags - this will be used for getting any changes from the mod register --->
			<cflock name="SalesforceXMLSearch" timeout="3">
				<cfset mappedArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@#objectTableName#,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(entity)#'][@flagID or @flagGroupID]")>
			</cflock>

			<cfloop array="#mappedArray#" index="node">
				<cfif structKeyExists(node.xmlAttributes,"flagID") and node.xmlAttributes.flagID neq "">
					<cfset flagList = listAppend(flagList,application.com.flag.getFlagStructure(flagID=node.xmlAttributes.flagID).flagID)>
					<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "sf2rw">
						<cfset synchedFlagList = listAppend(synchedFlagList,application.com.flag.getFlagStructure(flagID=node.xmlAttributes.flagID).flagID)>
					</cfif>
				<cfelseif structKeyExists(node.xmlAttributes,"flagGroupID") and node.xmlAttributes.flagGroupID neq "">
					<cfset flagGroupQry = application.com.flag.getFlagGroupFlags(flagGroupID=node.xmlAttributes.flagGroupID,cachedWithin=createTimeSpan(0,0,2,0))>
					<cfset flagList = listAppend(flagList,valueList(flagGroupQry.flagID))>
					<cfset request.sfFlagsInFlagGroup[application.com.flag.getFlagGroupStructure(flagGroupID=node.xmlAttributes.flagGroupID).flagGroupID] = valueList(flagGroupQry.flagID)>
					<cfif not structKeyExists(node.xmlAttributes,"direction") or node.xmlAttributes.direction neq "sf2rw">
						<cfset synchedFlagList = listAppend(synchedFlagList,valueList(flagGroupQry.flagID))>
					</cfif>
				</cfif>
			</cfloop>


			<!--- bit of a special case for opportunity Line Items as they are not directly linked to opportunities, but we need to process them
				along with processing an opportunity if they exist in the XML --->
			<cfif listFindNoCase(processOrderList,"opportunity")>
				<cflock name="SalesforceXMLSearch" timeout="3">
					<cfset mappedArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='opportunitylineitem']")>
				</cflock>
				
				<cfif arrayLen(mappedArray) gt 0>
					<cfif arguments.direction eq "RW2SF">
						<cfset processOrderList = listAppend(processOrderList,"opportunityLineItem")>
						<cfset objectList = listAppend(objectList,"opportunityLineItem")>
					<cfelse>
						<cfset objectList = listAppend(objectList,"opportunityLineItem")>
					</cfif>
				</cfif>
			</cfif>
	
			<cfset request.mappedObjectInfo[structKey].entity = entity> <!--- the main related object. (for example, a contact entity will be person as that holds the contact's ID) --->
			<cfset request.mappedObjectInfo[structKey].processOrder = processOrderList> <!--- the order in which to process the entities/objects. 
									For example, if we pass in a contact which maps to a location and person, then the list
									will have location and then person as we need to process locations first. Order is important! --->
			<cfset request.mappedObjectInfo[structKey].entityColumnList = entityColumnList> <!--- the list of rw table columns used for a given entity --->
			<cfset request.mappedObjectInfo[structKey].objectColumnList = objectColumnList> <!--- the list of object columns used for a given object --->
			<cfset request.mappedObjectInfo[structKey].objectList = objectList> <!--- an unordered list of mapped objects for a given entity (for example, a contact could have location,person or person,location) --->
			<cfset request.mappedObjectInfo[structKey].tableList = tableList> <!--- an unordered list of mapped entities for a given object --->
			<cfset request.mappedObjectInfo[structKey].flagList = flagList> <!--- the list of flags used in the mappings. If a flag group has been specified in the mapping, this will hold the flagIDs in the flagGroup --->
			<cfset request.mappedObjectInfo[structKey].synchedEntityColumnList = synchedEntityColumnList>
			<cfset request.mappedObjectInfo[structKey].synchedObjectColumnList = synchedObjectColumnList>
			<cfset request.mappedObjectInfo[structKey].synchedFlagList = synchedFlagList>
		</cfif>
		
		<cfreturn request.mappedObjectInfo[structKey]>
	</cffunction>


	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	        Returns the person details for this user from SalesForce
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSalesForceUserID" access="private" returntype="struct" hint="Returns the account manager for a given entity" output="false">
		<cfargument name="personid" type="numeric" required="false">
		<cfargument name="firstname" type="string" required="false">
		<cfargument name="lastname" type="string" required="false">
		<cfargument name="email" type="string" required="false">

		<cfscript>
			var userStruct = structNew();
			var salesforceID = 0;
		</cfscript>
		
		<cfset userStruct.isOK = "false">

		<cfif structKeyExists(arguments,"personid")>
			<cfset userStruct.PersonDetails = application.com.RelayPLO.getPersonDetails(personID=arguments.personID)>
			<cfif userStruct.PersonDetails.recordcount gt 0>
				<cfset userStruct.firstname = userStruct.PersonDetails.firstname>
				<cfset userStruct.lastname = userStruct.PersonDetails.lastname>
				<cfset userStruct.email = userStruct.PersonDetails.email>
			<cfelse>
				<cfset userStruct.message = "PersonID passed not found on Relayware">
			</cfif>
			<cfset StructDelete(userStruct, "PersonDetails")>
		<cfelse>
			<cfif structKeyExists(arguments,"firstname") and structKeyExists(arguments,"lastname") and structKeyExists(arguments,"email")>
				<cfset userStruct.firstname = arguments.firstname>
				<cfset userStruct.lastname = arguments.lastname>
				<cfset userStruct.email = arguments.email>
			<cfelse>
				<cfthrow message="Error in calling function getSalesForceUserID.  Function requires parameters:  PersonID(numeric); or Firstname, Lastname and Email (string).">			
				<cfabort>
			</cfif>
		</cfif>

		<!--- everything is ok up till this point --->
		<cfif not structKeyExists(userStruct,"message")>

			<cfif not structKeyExists(request,"sfUserIDs")>
				<cfset request.sfUserIDs = structNew()>
			</cfif>

			<!--- trim these, as spaces prove to be troublesome in debugging why an account manager is not getting found. --->
			<cfset userStruct.firstname = trim(userStruct.firstname)>
			<cfset userStruct.lastname = trim(userStruct.lastname)>
			<cfset userStruct.email = trim(userStruct.email)>
			
			<cfif not structKeyExists(request.sfUserIDs,"#userStruct.firstname##userStruct.lastname##userStruct.email#")>
				<!--- <cfset getUserStruct = send(object="user",method="query",queryString="select ID from user where firstname='#userStruct.firstname#' and lastname='#userStruct.lastname#' and email='#userStruct.email#' and isActive = TRUE")> --->
				<cfset salesForceID = doesSFObjectExist(object="user",objectDetails=userStruct)>
				<cfif salesForceID neq 0>
					<cfset request.sfUserIDs["#userStruct.firstname##userStruct.lastname##userStruct.email#"] = salesForceID>
				<cfelse>
					<cfset userStruct.message = "User #userStruct.firstname# #userStruct.lastname# with email '#userStruct.email#' not found on SalesForce.">
				</cfif>
			</cfif>
			
			<cfif structKeyExists(request.sfUserIDs,"#userStruct.firstname##userStruct.lastname##userStruct.email#")>
				<cfset userStruct.ID = request.sfUserIDs["#userStruct.firstname##userStruct.lastname##userStruct.email#"]>
				<cfset userStruct.isOK = true>
			</cfif>
		</cfif>

		<cfreturn userStruct>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	        Returns the personid for this user from Relayware using their 
	        firstname,lastname and email (and assuming orgid=2), retrieved from SalesForce 
	        using their SalesForceID.  If not found return struct: personid=0 and success=false
	        
	        NJH 2013 Roadmap Release 2 - item 35 - create account managers if they don't exist. Changes some functionality in that we now store the UserID on the person table in the crmPerId column.
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getRelaywarePersonID" access="public" returntype="struct" hint="Returns the personid for this user from Relayware" output="false">
		<cfargument name="SalesForceID" type="string" required="true">
		<cfargument name="object" type="string" default="User">

		<cfscript>
			var result = {success=true,message="",personID=0};
			var getUserStruct = "";
			var entityExistsStruct = "";
			var getCountryID = "";
			var personDetailStruct = {crmPerID = arguments.salesForceID};
			var getPersonAtAdminCompany = "";
			var getAdminOrgs = "";
			var orgStruct = structNew();
			var organisationID = 0;
			var orgCreated = false;
			var locStruct = structNew();
			var locationID = 0;
			var userStruct = structNew();
			var insertResult = structNew();
		</cfscript>
		
		<cfif not structKeyExists(request,"accManagerIDs")>
			<cfset request.accManagerIDs = structNew()>
		</cfif>
		
		<cfif not structKeyExists(request.accManagerIDs,arguments.salesForceID)>
		
			<cfquery name="getPersonAtAdminCompany" datasource="#application.siteDataSource#">
				select personID from person p
					inner join organisation o on p.organisationID = o.organisationID and o.organisationTypeID = #application.organisationType["AdminCompany"].organisationTypeID#
				where crmPerID = <cf_queryparam value="#arguments.salesForceID#" cfsqltype="cf_sql_varchar">
			</cfquery>
			
			<cfif getPersonAtAdminCompany.recordCount>
				<cfset result.personID = getPersonAtAdminCompany.personID[1]>
			<cfelse>
				<cfif arguments.object eq "User">
					<cfset getUserStruct = send(object="#arguments.object#",method="query",queryString="select ID,firstname,lastname,email,country,companyName,postalCode,street,city from #arguments.object# where ID='#arguments.SalesForceID#'",ignoreBulkProtection=true)>
				</cfif>
				<cfif getUserStruct.success>
					<cfif listFindNoCase(getUserStruct.response.columnList,"ID") and getUserStruct.response.recordcount eq 1>
						<cfset userStruct = {firstname = getUserStruct.response.firstname[1],lastname = getUserStruct.response.lastname[1],email = getUserStruct.response.email[1],country = getUserStruct.response.country[1]}>
						
						<cfquery name="getAdminOrgs" datasource="#application.siteDataSource#">
							select organisationID from organisation where organisationTypeID=#application.organisationType["AdminCompany"].organisationTypeID#
						</cfquery>
						
						<cfset userStruct.organisationID = valueList(getAdminOrgs.organisationID)>

						<cftry>
							<cfset entityExistsStruct = application.com.relayEntity.doesPersonExist(PersonDetails=userStruct,columnsToUse="firstname,lastname,email,organisationID")>
							<cfcatch>
								<cfset result.success=false>
								<cfset result.message=cfcatch.message>
							</cfcatch>
						</cftry>
						
						<cfif structKeyExists(entityExistsStruct,"entityID") and entityExistsStruct.entityID GT 0>
							<cfset result.personid = entityExistsStruct.entityID>
							
							<!--- set the salesForceID against the person in RW --->
							<cfset application.com.relayEntity.updatePersonDetails(personID=result.personid,personDetails=personDetailStruct)>
						<cfelse>
							
							<!--- Roadmap 2 Item 35 - Create an account manager if they don't exist --->
							<cfquery name="getCountryID" datasource="#application.siteDataSource#">
								select c.countryId from country c
									left join countryAlternatives ca on ca.countryID = c.countryID
								where (
									c.r1CountryID=<cf_queryparam value="#trim(getUserStruct.response.country[1])#" cfsqltype="cf_sql_varchar"> or
									c.countryDescription=<cf_queryparam value="#trim(getUserStruct.response.country[1])#" cfsqltype="cf_sql_varchar"> or
									c.isoCode=<cf_queryparam value="#trim(getUserStruct.response.country[1])#" cfsqltype="cf_sql_varchar"> or
									c.localCountryDescription=<cf_queryparam value="#trim(getUserStruct.response.country[1])#" cfsqltype="cf_sql_varchar"> or
									ca.alternativeCountryDescription = <cf_queryparam value="#trim(getUserStruct.response.country[1])#" cfsqltype="cf_sql_varchar">
								)
							</cfquery>
							
							<cfif getCountryID.recordCount>
								<cfset orgStruct = {organisationName=getUserStruct.response.companyName[1],companyName=getUserStruct.response.companyName[1],organisationTypeID=application.organisationType["AdminCompany"].organisationTypeID,countryID = getCountryID.countryID,vatNumber=""}>
								<cfset orgStruct.matchName = application.com.dbtools.getOrganisationMatchName(argumentCollection = orgStruct)>
								<!--- not using doesOrganisationExist at the moment as organisationType is not being used as part of match names... including it at this point is going to open a can of worms --->
								<cfset organisationID = application.com.relayEntity.doesEntityExist(entityDetails=orgStruct,entityTypeID=application.entityTypeID.organisation,columnsToUse="matchName,countryID,organisationTypeID").entityID>

								<!--- if the org does not exist, insert the org --->
								<cfif organisationID eq 0>
									<cfset insertResult = application.com.relayEntity.insertOrganisation(organisationDetails=orgStruct,insertIfExists=true)> <!--- we've already checked if the org exists, and we know it doesn't, so set insertIfExists to true --->
									<cfset organisationID = insertResult.entityID>
									<cfset orgCreated = true>
									<cfset result.message = insertResult.message>
								</cfif>
								
								<cfif organisationID neq 0>
									<cfset locStruct = {sitename=getUserStruct.response.companyName[1],address1=getUserStruct.response.street[1],address4=getUserStruct.response.city[1],postalCode=getUserStruct.response.postalCode[1],countryID = getCountryID.countryID,organisationID=organisationID}>
									<cfif not orgCreated>
										<cfset locationID = application.com.relayEntity.doesLocationExist(locationDetails = locStruct).entityID>
									</cfif>
									
									<!--- if the location does not exist, create it --->
									<cfif locationID eq 0>
										<cfset insertResult = application.com.relayEntity.insertLocation(locationDetails=locStruct,insertIfExists=true)> <!--- we've already checked if the loc exists, and we know it doesn't, so set insertIfExists to true --->
										<cfset locationID = insertResult.entityID>
										<cfset result.message = insertResult.message>
									</cfif>
									
									<cfif locationID neq 0>
										<cfset userStruct.organisationID=organisationID>
										<cfset userStruct.locationID=locationID>
										<cfset userStruct.crmPerID = arguments.salesForceID>
										<cfset insertResult = application.com.relayEntity.insertPerson(personDetails=userStruct,insertIfExists=true)>
										<cfset result.personID = insertResult.entityID>
										<cfset result.message = insertResult.message>
										
										<cfif result.message eq "NON_UNIQUE_EMAIL">
											<cfset result.message = "Could not add person #userStruct.firstname# #userStruct.lastname# with email #userStruct.email# as the email address is not unique.">
										</cfif>
										
										<cfif result.personID neq 0>
											<!--- NJH 2014/04/16 - case 439429 - give the user basic internal user rights --->
											<cfset application.com.login.setPersonAsInternalUser(personID=result.personID,loginExpiresDate=dateAdd("d",-1,now()))>
											<!--- flag new user as an accountManager and put them in temp user group --->
											<cfset application.com.flag.setFlagData(flagId=application.com.flag.getFlagStructure(flagID="isAccountManager").flagID,entityID=result.personID)>
											
											<cfset application.com.relayUserGroup.addPersonToUserGroup(userGroup="SalesForceProxyUser",personID=result.personID)>
										</cfif>
									</cfif>	
								</cfif>
							<cfelse>
								<cfset result.message = "Could not find country #getUserStruct.response.country[1]# for #arguments.object# #userStruct.firstname# #userStruct.lastname#">
							</cfif>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			
			<cfif result.personID neq 0>
				<cfset request.accManagerIDs[arguments.salesForceID] = result.personID>
			</cfif>
		<cfelse>
			<cfset result.personID = request.accManagerIDs[arguments.salesForceID]>
		</cfif>

		<cfif result.personID eq 0>
			<cfset result.success = false>
			<cfif result.message eq "">
				<cfset result.message = "#arguments.object# #arguments.object# #userStruct.firstname# #userStruct.lastname# with email #userStruct.email# does not exist on Relayware">
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>


	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	        Returns the personid for the user in Relayware set to be the default 
	        Account Manager/Owner - when the Owner on Salesforce doesn't exist on Relayware
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getDefaultOwner" access="private" returntype="numeric" hint="Returns the default account manager for a given country" output="false">
		<cfargument name="Country" type="string" required="true">

		<cfscript>
			var getDefaultOwner = "";
		</cfscript>

		<cfquery name="getDefaultOwner" datasource="#application.sitedatasource#">
			select top 1 fd.Data,par.countryID
			from flag f 
			inner join integerflagdata fd on f.flagid=fd.flagid 
			inner join country par on fd.entityid=par.countryid 
			inner join CountryGroup cg on par.countryid=cg.CountryGroupid 
			inner join country c on cg.CountryMemberid=c.Countryid 
			where f.flagtextid='SFDCDefaultOwner' and 
			(c.ISOCode='#arguments.Country#' or c.CountryDescription='#arguments.Country#' or c.LocalCountryDescription='#arguments.Country#' or c.R1CountryID='#arguments.Country#' or par.countryID=37)
			order by par.countryGroupTypeID asc,len(isnull(par.CountryCurrency,0)) desc
		</cfquery>

		<cfif getDefaultOwner.recordcount eq 1>
			<cfreturn getDefaultOwner.Data>
		<cfelse>
			<cfreturn 0>
		</cfif>
	</cffunction>

	
	<cffunction name="getAccountManagerForEntity" access="private" returntype="struct" hint="Returns the account manager for a given entity" output="false">
		<cfargument name="entityType" type="string" required="false">
		<cfargument name="entityID" type="numeric" required="false">
		<cfargument name="object" type="string" required="false">
		<cfargument name="opportunityID" type="numeric" required="false">
		<cfargument name="numAttempts" type="numeric" default=1>
		
		<cfscript>
			var uniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey;
			var getAccountManagerDetails = "";
			var getUserStruct = structNew();
			var result = {message="",objectOwnerID=""};
			var mergeStruct = structNew();
			var messageTextId = "";
			var message = "";
			var getOrganisationDetails = "";
			var getVendorAccountManager = "";
			var objectOwnerID = "";
		</cfscript>
		
		<cfif not (structKeyExists(arguments,"entityType") or structKeyExists(arguments,"entityID") or structKeyExists(arguments,"object")) and not structKeyExists(arguments,"opportunityID")>
			<cfset result.message = "An opportunityId, or an entityID and a entityTypeId, must be passed in.">
			<cfreturn result>
		</cfif>
		
		<cfif listFindNoCase("organisation,location,person,opportunity,oppProducts",arguments.entityType)>
			<cfif listFindNoCase("organisation,location,person",arguments.entityType)>
				<cfquery name="getOrganisationDetails" datasource="#application.siteDataSource#">
					select e.organisationID, o.organisationName, ot.typeTextID from #arguments.entityType# e with (noLock)
						inner join organisation o with (noLock) on e.organisationID = o.organisationID
						inner join organisationType ot on o.organisationTypeID = ot.organisationTypeID
					where e.#uniqueKey# = #arguments.entityID#
				</cfquery>

				<cfif getOrganisationDetails.recordCount gt 0>
					<cfset getAccountManagerDetails = application.com.commonQueries.getAccManager(entityID=getOrganisationDetails.organisationID,entityType="organisation")>
					
					<cfif getAccountManagerDetails.recordCount eq 0>
						<cfif getOrganisationDetails.typeTextID eq "EndCustomer">
							<cfset objectOwnerID = application.SFDCAPI.userID>
						<cfelse>
							<cfset mergeStruct = {organisationName = getOrganisationDetails.organisationName,organisationId = getOrganisationDetails.organisationID}>
							<cfset messageTextId = "phr_salesForceError_AccManagerIsNotSet">
						</cfif>
					</cfif>
				</cfif>
				
			<cfelseif listFindNoCase("opportunity,oppProducts",arguments.entityType)>
				<cfquery name="getVendorAccountManager" datasource="#application.siteDataSource#">
					select distinct o.opportunityid,o.vendorAccountManagerPersonID as ID, o.detail
					from opportunity o with (nolock) 
					left join oppProducts op with (nolock) on o.opportunityid=op.opportunityid
					where o.#uniqueKey# = #arguments.entityID#
				</cfquery>
				<cfif getVendorAccountManager.ID[1] neq 0>
					<cfset getAccountManagerDetails = application.com.RelayPLO.getPersonDetails(personID=getVendorAccountManager.ID[1])>
				<cfelse>
					<cfset mergeStruct = {opportunityDetail = getVendorAccountManager.detail,opportunityID = arguments.entityID}>
					<cfset messageTextId = "phr_salesForceError_OppAccManagerIsNotSet">
				</cfif>
			</cfif>
			
			<cfif objectOwnerID eq "" and messageTextId eq "" and isQuery(getAccountManagerDetails) and getAccountManagerDetails.recordCount gt 0>
				<cfset getUserStruct = getSalesForceUserID(firstname=getAccountManagerDetails.firstname,lastname=getAccountManagerDetails.lastname,email=getAccountManagerDetails.email)>
				<cfif getUserStruct.isOK>
					<cfset objectOwnerID = getUserStruct.ID>	
				<cfelse>
					<cfset mergeStruct = {firstname=getAccountManagerDetails.firstname,lastname=getAccountManagerDetails.lastname,email=getAccountManagerDetails.email}>
					<cfset messageTextId = "phr_salesForceError_CouldNotFindAccManagerOnSalesForce">
				</cfif>		
			</cfif>
		</cfif>

		<cfset result.objectOwnerID = objectOwnerID>
		
		<cfif message eq "" and messageTextId neq "">
			<!--- Need to translate the phrase, as this will get stored in the salesForceError table, and salesForceError is not 
				currently a RW entity where we could use entity translations --->
			<cfset message = application.com.relayTranslations.translatePhrase(phrase=messageTextId,mergeStruct=mergeStruct)>
		</cfif>
		
		<cfset result.message = trim(message)>

		<cfreturn result>
	</cffunction>
	
	
	<!--- returns the salesForceID for a given entity --->
	<cffunction name="getSalesForceIDForEntity" returntype="string" access="public" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		
		<cfscript>
			var salesForceID = 0;
			var getEntitySalesForceID = "";
			var foreignKeyInfo = getIDMappingInfo(entityType=arguments.entityType);
			var uniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey;
		</cfscript>
		
		<cfquery name="getEntitySalesForceID" datasource="#application.siteDataSource#">
			select 
			<cfif foreignKeyInfo.table eq "flag">
				isNull(#foreignKeyInfo.flagID#.data,0)
			<cfelse>
				isNull(#foreignKeyInfo.sfIDColumn#,0)
			</cfif>
			as salesForceID
			from #arguments.entityType#
			where #uniqueKey# = #arguments.entityID#
		</cfquery>
		
		<cfif getEntitySalesForceID.recordCount eq 1 and len(getEntitySalesForceID.salesForceID) gt 0>
			<cfset salesForceID = getEntitySalesForceID.salesForceID>
		</cfif>
		
		<cfreturn salesForceID>
	</cffunction>
	
	
	<!--- function to set the salesForceID for a given entity --->
	<cffunction name="setSalesForceIDForEntity" access="public" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="salesForceID" type="string" required="true">
		
		<cfscript>
			var foreignKeyInfo = getIDMappingInfo(entityType=arguments.entityType);
			var setSalesForceID = "";
			var uniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey;
		</cfscript>
		
		<cfif foreignKeyInfo.table eq "flag">
			<cfset application.com.flag.setFlagData(flagID=foreignKeyInfo.flagID,entityID=arguments.entityID,data=arguments.salesForceID)>
		<cfelse>
			<cfquery name="setSalesForceID" datasource="#application.siteDataSource#">
				update #arguments.entityType# 
					set #foreignKeyInfo.sfIDColumn# = '#arguments.salesForceID#',
						lastUpdated='#request.requestTime#',
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson = #request.relayCurrentUser.personID#
				where #uniqueKey# = #arguments.entityID#
			</cfquery>
		</cfif>
		
	</cffunction>
	
	
	<cffunction name="flagEntityAsSynched" access="public" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="direction" type="string" default="import">

		<cfset var flagTextID = "SalesForceSynched_#arguments.direction#">
		<cfset var prefix = left(arguments.entityType,3)>
		<cfset var uniqueKey = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey>
		<cfset var getRelatedRecordTypes = "">
		<cfset var relatedEntityType = "">
		<cfset var getMappedInfo = getIDMappingInfo(entityType=arguments.entityType)>
		<cfset var objectEntityStruct = putMappedObjectsInTable(entityType=entityType)>
		<cfset var setRelatedRecordsToPending = "">
		<cfset var flagStruct = "">
		<cfset var fromStatement = "">
		
		<cfif listFindNoCase("pricebook,pricebookEntry,product",arguments.entityType)>
			<cfset prefix = arguments.entityType>
		</cfif>
		<cfset flagTextID = prefix&flagTextID>

		<!--- if the entity is already flagged as synched, delete and then recreate so that we get the latest Synched time in the success report--->
		<cfset application.com.flag.deleteFlagData(entityID=arguments.entityID,flagId=application.com.flag.getFlagStructure(flagTextID).flagID)>
		<cfset application.com.flag.setBooleanFlag(entityID=arguments.entityID,flagTextID=flagTextID,deleteOtherRadiosInGroup=true)>

		<!--- NJH 2013 Roadmap V2 enchancements. when an object which has caused others objects to fail has successfully synched, then set the related objects to synching and prioritise them in the pending queue --->
		<cfquery name="getRelatedRecordTypes" datasource="#application.siteDataSource#">
			select distinct relatedObject,relatedObjectID from salesForceRelatedError sfre
				inner join ##sfEntityMapping sm on sfre.object = sm.<cfif arguments.direction eq "export">entityName<cfelse>object</cfif> and sm.entityName='#arguments.entityType#'
				inner join #arguments.entityType# e on sfre.objectID = e.<cfif arguments.direction eq "export">#uniqueKey#<cfelse>#getMappedInfo.sfIdColumn#</cfif>
			where e.#uniqueKey#='#arguments.entityID#'
		</cfquery>
		
		<cfloop query="getRelatedRecordTypes">
			<cfset relatedEntityType = relatedObject>
			<cfif not isNumeric(relatedObjectID)>
				<cfset relatedEntityType = getMappedObject(object=relatedObject,direction="SF2RW").entity>
			</cfif>
			
			<cfset getMappedInfo = getIDMappingInfo(entityType=relatedEntityType)>
			<cfset flagStruct = application.com.flag.getFlagStructure(flagID=left(relatedEntityType,3)&"SalesForceSynching")>

			<cfif flagStruct.isOK>
				<!--- flag related records as pending where they are not pending, set them as priority in the pending queue, and reset the synch attempt (which means that an incoming record is set back to pending) --->
				<cfquery name="setRelatedRecordsToPending" datasource="#application.siteDataSource#">
					update booleanFlagData set flagID = #flagStruct.flagID#,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID#,lastUpdatedByPerson=#request.relayCurrentUser.personID#
					from salesForceRelatedError sfre
						inner join #relatedEntityType# e on <cfif arguments.direction eq "export">sfre.relatedObject='#relatedEntityType#' and sfre.relatedObjectID=e.#getMappedInfo.entityIDColumn#<cfelse>sfre.relatedObject ='#relatedObject#' and sfre.relatedObjectID=e.#getMappedInfo.sfIdColumn#</cfif>
						inner join booleanFlagData bfd on bfd.entityID = e.#getMappedInfo.entityIDColumn#
						inner join flag f on f.flagID = bfd.flagID and f.flagGroupID = #flagStruct.flagGroupID#
					where sfre.object=<cfif arguments.direction eq "export">'#arguments.entityType#'<cfelse>'#objectEntityStruct[arguments.entityType]#'</cfif>
						and sfre.relatedObjectID = '#relatedObjectID#'
						and bfd.flagID != #flagStruct.flagID#
					
					update salesForceSynchPending set prioritise=1,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID#
					where isNull(prioritise,0) =0
						and object = '#relatedObject#'
						and <cfif not isNumeric(relatedObjectID)>objectID<cfelse>entityID</cfif>='#relatedObjectID#'
						and direction = '#left(arguments.direction,1)#'
						
					update salesForceError set synchAttempt=0,lastUpdated=getDate()
					where object = '#relatedObject#'
						and <cfif not isNumeric(relatedObjectID)>objectID<cfelse>entityID</cfif>='#relatedObjectID#'
						and direction = '#left(arguments.direction,1)#'
						and synchAttempt > 0
				</cfquery>
			</cfif>	
		</cfloop>

	</cffunction>
	
	
	<cffunction name="flagEntityAsSuspended" access="public" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityType" type="string" required="true">

		<cfset application.com.flag.setBooleanFlag(entityID=arguments.entityID,flagTextID="#left(arguments.entityType,3)#SalesForceSynchSuspended",deleteOtherRadiosInGroup=true)>
	</cffunction>
	
	
	<cffunction name="flagEntityAsSynching" access="public" output="false">
		<cfargument name="entityID" type="numeric" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="resetSynchAttempt" type="boolean" default="false">
		
		<cfscript>
			var resetAttempt = "";
		</cfscript>

		<cfset application.com.flag.setBooleanFlag(entityID=arguments.entityID,flagTextID="#left(arguments.entityType,3)#SalesForceSynching",deleteOtherRadiosInGroup=true)>
		
		<cfif arguments.resetSynchAttempt>
			<cfquery name="resetAttempt" datasource="#application.siteDataSource#">
				update salesForceError set synchAttempt=0 where object='#arguments.entityType#' and entityID=#arguments.entityID#
			</cfquery>
		</cfif>
	</cffunction>
	
	
	<!--- function to delete the salesForceID for a given entity --->
	<!--- 
		START 2013-07-15 PPB Case 436080 replaced function deleteSalesForceIDForEntity with deleteSalesForceIDForEntityV2 
		at time of replacing no customer code was calling this function using entityID argument so I have not supported it in V2 (hence I have left V1 in place for now)
	--->
	<!---
	<cffunction name="deleteSalesForceIDForEntity" access="private" hint="Removes the SalesForceID for deleted Objects" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" default=0>
		<cfargument name="objectIDList" type="string" required="false">
		
		<cfscript>
			var foreignKeyInfo = getIDMappingInfo(entityType=arguments.entityType);
			var deleteSalesForceID = "";
			var entityIDList = 0;
			var getEntityIDs = "";
			var whereClause = "1=0";
			var objectList = "";
		</cfscript>
		
		<cfif arguments.entityID neq 0>

			<cfset whereClause = "#application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey# = #arguments.entityID#">
			<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)>
				<cfset entityIDList = arguments.entityID>
			</cfif>
		
		<!--- this is a bulk delete, where we are given the salesforceIds --->
		<cfelseif structKeyExists(arguments,"objectIDList") and arguments.objectIDList neq "">
			
			<cfset objectList = listQualify(arguments.objectIDList,"'")>
			<cfset whereClause = "#foreignKeyInfo.sfIDColumn# in (#objectList#)">
			<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)>
				<cfquery name="getEntityIDs" datasource="#application.siteDataSource#">
					select #application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey# as entityID from #arguments.entityType#
						where #foreignKeyInfo.sfIDColumn# in (#preserveSingleQuotes(objectList)#)
				</cfquery>
				
				<cfif getEntityIDs.recordCount>
					<cfset entityIDList = valueList(getEntityIDs.entityID)>
				</cfif>
			</cfif>
		</cfif>
		
		<cfquery name="deleteSalesForceID" datasource="#application.siteDataSource#">
			update #arguments.entityType# set #foreignKeyInfo.sfIDColumn# = null,lastUpdated=GetDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID# 
				where #preserveSingleQuotes(whereClause)#
			
			<!--- delete references to the opportunity products if the opportunity has been deleted on SalesForce --->
			<cfif arguments.entityType eq "opportunity">
			update oppProducts set crmOppProductID = null,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID# 
				where opportunityID in (#entityIdList#)
			</cfif>
		</cfquery>
		
		<!--- deleting the salesForce synching flag for core entities --->
		<cfif entityIdList neq 0>
			<cfset application.com.flag.deleteFlagGroupData(flagGroupID=application.com.flag.getFlagStructure(flagID="#left(arguments.entityType,3)#SalesForceSynched").flagGroupID,entityID=entityIDList)>
		</cfif>

	</cffunction>
	--->
	<!--- END 2013-07-15 PPB Case 436080 --->
	

	<!---
		START 2013-07-15 PPB Case 436080 
		the original function deleteSalesForceIDForEntity had issues because it used a list of objectId which could grow very long (if there had been bulk deletes on SF) 
		deleteSalesForceIDForEntityV2 receives a query of SF objectIds from which a temp table of RW entityIds is created for use in subqueries; for convenience the temp table is loaded into a query 
	 --->
	<cffunction name="deleteSalesForceIDForEntityV2" access="private" hint="Removes the SalesForceID for deleted Objects" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="qrySFObjectsToDelete" type="query" required="false">
		<cfargument name="tableSFObjectsToDelete" type="string" required="true">
		<cfargument name="flagForDeletion" type="boolean" default="false">			<!--- 2014-05-22 PPB Case 440416 changed default to false --->
		
		<cfscript>
			var foreignKeyInfo = application.com.salesforce.getIDMappingInfo(entityType=arguments.entityType);
			var entityUniqueKey = "";
			var deleteSalesForceID = "";
			var whereClause = "";
			var deleteSynchingFlag = "";
			var deleteFlagID = "";
			var setFlagToMarkForDeletion = "";
		</cfscript>

		<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)>				<!--- is this reqd? should other entityTypes be allowed in and access any of this function? --->
			<!--- delete temp table just in case there was an error during processing last time --->	
			<cfset entityUniqueKey = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey>
			
			<!--- for backwards compatibility.. in case anyone is using this function and passing in a query - put the query into a table --->
			<cfif structKeyExists(arguments,"qrySFObjectsToDelete")>
				<cf_qoqToDB query="#arguments.qrySFObjectsToDelete#" name="#arguments.tableSFObjectsToDelete#" columnList="ID">
			</cfif>
			
			<cfsavecontent variable="whereClause">
				<cfoutput>
					from #arguments.tableSFObjectsToDelete# s
					inner join #arguments.entityType# e on e.#foreignKeyInfo.sfIDColumn# = s.ID  
					inner join booleanFlagData bfd on bfd.entityID = e.#entityUniqueKey#
					inner join flag f on bfd.flagID = f.flagID and f.flagGroupID = #application.com.flag.getFlagStructure(flagID="#left(arguments.entityType,3)#SalesForceSynched_export").flagGroupID#
				</cfoutput>
			</cfsavecontent>
			
			<!--- deleting the salesForce synching flag for core entities --->
			<cfquery name="deleteSynchingFlag" datasource="#application.siteDataSource#">
				update booleanFlagData set lastUpdated=getDate(),
					lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
					lastUpdatedByPerson=#request.relayCurrentUser.personID#
				#preserveSingleQuotes(whereClause)#
				
				delete booleanFlagData
				#preserveSingleQuotes(whereClause)#
			</cfquery>
			
			
			<cfif listFindNoCase("organisation,location,person",arguments.entityType) and arguments.flagForDeletion>
				<cfset deleteFlagID = application.com.flag.getFlagStructure(flagID="Delete#arguments.entityType#").flagID>
					
				<cfquery name="setFlagToMarkForDeletion" datasource="#application.siteDataSource#">
					insert into booleanFlagdata (flagId,entityID,createdBy,lastUpdatedBy,lastUpdatedByPerson)
					select #deleteFlagID#,e.#entityUniqueKey#,#request.relayCurrentUser.userGroupId#,#request.relayCurrentUser.userGroupId#,#request.relayCurrentUser.personId#
					from #arguments.tableSFObjectsToDelete# s 
						inner join #arguments.entityType# e on e.#foreignKeyInfo.sfIDColumn# = s.ID
						left join booleanFlagData bfd on bfd.entityID = e.#entityUniqueKey# and bfd.flagID = #deleteFlagID#
					where bfd.flagId is null
				</cfquery>
			</cfif>
			
			<!--- as a final step, remove the CRMID from the core records --->
			<cfquery name="deleteSalesForceID" datasource="#application.siteDataSource#">
				update #arguments.entityType# set #foreignKeyInfo.sfIDColumn# = null,
					lastUpdated=GetDate(),
					lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
					lastUpdatedByPerson=#request.relayCurrentUser.personID#
				from #arguments.tableSFObjectsToDelete# s
					inner join #arguments.entityType# e on e.#foreignKeyInfo.sfIDColumn# = s.ID
				
				<!--- delete references to the opportunity products if the opportunity has been deleted on SalesForce --->
				<cfif arguments.entityType eq "opportunity">
					update oppProducts set crmOppProductID = null,
						lastUpdated=getDate(),
						lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
						lastUpdatedByPerson=#request.relayCurrentUser.personID#
					from #arguments.tableSFObjectsToDelete# s 
						inner join opportunity o on o.#foreignKeyInfo.sfIDColumn# = s.ID
						inner join oppProducts op on op.opportunityID = o.opportunityID 
				</cfif>
			</cfquery>
			
		</cfif>
	</cffunction>
	<!--- END 2013-07-15 PPB Case 436080 --->
		
	<cffunction name="scoreAndMergeMatchedEntities" access="private" hint="Scores and puts on merge queue entities that matched to incoming object details" returntype="struct" output="false">
		<cfargument name="entityType" type="string" required="true" hint="Should be organisation,person or location">
		<cfargument name="entityIDList" type="string" required="true" hint="The list of IDs to score">
		
		<cfscript>
			var IDtoKeep = 0;
			var IDtoDelete = 0;
			var result = {isOK=true};
			var functionArgs = structNew();
			var polScores = "";
			var mergeResultStruct = structNew();
			var dedupeFunction = application.com.dedupe["score#arguments.entityType#"];
		</cfscript>
		
		<cfset functionArgs["#arguments.entityType#IDList"] = arguments.entityIDList>
		<cfset polScores = dedupeFunction(argumentCollection=functionArgs)>

		<cfloop query="polScores">
			<cfif currentRow eq 1>
				<cfset IDtoKeep = entityID>
			<cfelse>
				<cfset IDToDelete = entityID>
				<cfset mergeResultStruct = application.com.dedupe.addMergeOnlyDupe(IDToKeep=IDToKeep,IDToDelete=IDToDelete,entityType=arguments.entityType)>
				<cfif not mergeResultStruct.isOK>
					<cfset result.isOk = false>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfset result.entityID = IDToKeep>
		<cfreturn result>
	</cffunction>
	
	
	<!--- returns a structure with some mapping information for the IDs.. ie. columns that hold the foreign key ID 
		NJH 2014/02/26 CASE 439003 - check the arrayLength of the xmlSearch and initialise the result structure with keys that we're expecting
	--->
	<cffunction name="getIDMappingInfo" access="public" returnType="struct" output="false">
		<cfargument name="entityType" type="string" required="true">
		
		<cfscript>
			var result = {entityIDColumn="",sfIDColumn="",table=""};
			var IdMappingArray = arrayNew(1);
			var mappedObjectInfo = getMappedObject(object=arguments.entityType);
			var object = mappedObjectInfo.entity;
			var foreignKeyInfo = structNew();
		</cfscript>

		<!--- TODO: put this in memory so we don't keep searching xml --->
		<cflock name="SalesforceXMLSearch" timeout="3">
			<cfset IdMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(object)#'][translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='id']")>
		</cflock>
		
		<cfif arrayLen(IdMappingArray)>
			<cfset foreignKeyInfo = IdMappingArray[1].xmlAttributes>
			
			<cfset result.entityIDColumn = application.entityType[application.entityTypeID[arguments.entityType]].uniqueKey>
			<!--- cater for lead import --->
			<cfif structKeyExists(request,"sfOppTypeTextID") and entityType neq "opportunity">
            	<cfset result.sfIDColumn = "crmLeadID">
			<cfelse>
				<cfset result.sfIDColumn = foreignKeyInfo.column>
			</cfif>
			<cfset result.table = foreignKeyInfo.table>
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="logSFError" access="public" hint="Records an error" returnType="numeric" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="method" type="string" required="true">
		<cfargument name="message" type="string" required="true">
		<cfargument name="statusCode" type="string" required="false" default="">
		<cfargument name="entityID" type="numeric" required="false" default=0>
		<cfargument name="objectID" type="string" required="false" default=0>
		<cfargument name="fields" type="string" required="false" default="">
		<cfargument name="name" type="string" required="false" default="">
		<cfargument name="direction" type="string" required="true" hint="Is either E for export or I for import"> <!--- should be E or I --->
		<cfargument name="opportunityID" type="string" required="false" default="">
		<cfargument name="dataStruct" type="any" required="false" default="">
		<cfargument name="sendAlertEmail" type="boolean" required="false" default="false">
		<cfargument name="role" type="string" required="false" default="">
		<cfargument name="relatedObjectID" type="string" required="false" default="">
		<cfargument name="relatedObject" type="string" required="false" default="opportunity">
		
		<cfscript>
			var insertSalesForceError = "";
			var mappedObject = structNew();
			var entity = "";
			var entityMappingStruct = structNew();
			var structFindValueArray = "";
			var subjectLine = "";
			/* WAB 2013-11-21 CASE 438130 removed getSetting("salesForce"), and make call for individual settings.  Was getting settings errors */
			var maxSFSynchAttempts = application.com.settings.getSetting("salesForce.maxSynchAttempts");
			var salesForceAdminEmail = application.com.settings.getSetting("salesForce.AdminEmail");
			var loggingName = arguments.name;
			var recordType = "";
			var relatedObjectIdIsNull = false;
			var primaryObjectIDValue = arguments.entityID;
			var primaryObjectIDColumn = "entityID";
			var objectIDIsNull = false;
			var entityIDIsNull = false;
		</cfscript>

		<!--- if we're exporting, then we want to get the RW name for the SF object --->
		<cfif arguments.direction eq "E">
			
			<cfloop list="organisation,location,person,oppProducts" index="entity">
				<cfset mappedObject = application.com.salesForce.getMappedObject(object=entity,direction='RW2SF')>
				<!--- START 2015-02-02 AHL Case 443724 Max Sync Attempts. --->
				<cfif trim(this.m_entityType) neq "" and
					(trim(mappedObject.tableList) eq "" or ListLen(mappedObject.tableList,application.delim1) gt 1) >
					<cfset arguments.object = this.m_entityType />
					<cfbreak>
				</cfif>
				<!--- END 2015-02-02 AHL Case 443724 Max Sync Attempts. --->
				<cfif not (entity eq "location" and listSort(mappedObject.tableList,"text") eq "location,person")>
					<cfset entityMappingStruct[entity] = mappedObject.entity>	
				</cfif>
			</cfloop>
			
			<cfset structFindValueArray = structFindValue(entityMappingStruct,arguments.object)>
			<cfif arrayLen(structFindValueArray) gt 0>
				<cfset arguments.object = structFindValueArray[1].key>
			</cfif>
		</cfif>
		
		<!--- belt and braces for any code that I may have missed.. If an opportunityID has been passed in, then use that as the relatedObjectId --->
		<cfif (arguments.relatedObjectId eq "" or arguments.relatedObjectId eq 0) and (arguments.opportunityID neq 0 and arguments.opportunityID neq "")>
			<cfset arguments.relatedObjectID = arguments.opportunityID>
			<cfset arguments.relatedObject = "opportunity">
		</cfif>
		
		<!--- we store the role field in the name - this will be applicable only for those salesForce objects that have roles, such as partner and opportunityContactRole --->
		<cfif loggingName eq "" and arguments.role neq "">
			<cfset loggingName = arguments.role>
		</cfif>
		
		<cfif not isValid("String",arguments.dataStruct)>
			<cfset arguments.dataStruct = SerializeJSON(arguments.dataStruct,"true")>
		</cfif>
		
		<cfif arguments.relatedObjectId eq "" or arguments.relatedObjectId eq 0>
			<cfset relatedObjectIdIsNull = true>
		</cfif>
		
		<cfif arguments.objectID eq "" or arguments.objectID eq 0>
			<cfset objectIDIsNull = true>
			<cfset arguments.objectID = "">
		</cfif>
		
		<cfif arguments.entityID eq "" or arguments.entityID eq 0>
			<cfset entityIDIsNull = true>
			<cfset arguments.entityID = 0>
		</cfif>
		
		<cfif direction eq "I">
			<cfset primaryObjectIDColumn = "objectID">
			<cfset primaryObjectIDValue = arguments.objectID>
		</cfif>
		
		<!--- NJH 2010-09-09 LID 3980 - don't match on message, but update it so that we have the most recent message showing and not historical ones 
			NJH 2010-10-01 - also removed matching on name, as an name could change, which would create a new error record.
		--->
		<!--- START 2013-10-14 PPB Case 437176 rejig the way SF errors are inserted to avoid rows with synchAttempt=0 --->		
		<cfquery name="insertSalesForceError" datasource="#application.siteDataSource#">
			
			declare @errorId int

			select @errorId = errorID from salesForceError where 
				object='#arguments.object#' and
				<!--- opportunityID=<cf_queryparam value="#arguments.opportunityID#" cfsqltype="cf_sql_varchar"/> and  --->
				method='#arguments.method#' and
				isNull(statusCode,'') = <cf_queryparam value="#arguments.statusCode#" cfsqltype="cf_sql_varchar"/> and 
				isNull(entityID,0) = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer"/> and 
				isNull(objectID,'') = <cf_queryparam value="#arguments.objectID#" cfsqltype="cf_sql_varchar"/> and
				isNull(fields,'') = <cf_queryparam value="#arguments.fields#" cfsqltype="cf_sql_varchar"/> and 
				direction = '#arguments.direction#'
				<cfif arguments.role neq "">and isNull(name,'') = <cf_queryparam value="#loggingName#" cfsqltype="cf_sql_varchar"/></cfif>

			IF ISNULL(@errorId,0) = 0 
				BEGIN
					insert into salesForceError 
						(object,opportunityID,method,message,statusCode,entityID,objectID,fields,name,direction,dataStruct,synchAttempt)
					values (
						<!--- 2013-08-08 PPB Case 435688 added left 500 to avoid changing db (in a rare case it's set to cfcatch.message+cfcatch.detail); agreed by GCC  --->	
						<!--- 2014-06-05 PPB Case 440430 added left 100 to loggingName  --->	
						'#arguments.object#',<cf_queryparam value="#arguments.opportunityID#" cfsqltype="cf_sql_varchar" null="#relatedObjectIdIsNull#" useCFVersion="false"/>,
						'#arguments.method#',
						'#Left(arguments.message,500)#',
						<cf_queryparam value="#arguments.statusCode#" cfsqltype="cf_sql_varchar" null="#IIF(arguments.statusCode eq '',DE('true'),DE('false'))#" useCFVersion="false"/>,
						<cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer" null="#entityIDIsNull#"/>,
						<cf_queryparam value="#arguments.objectID#" cfsqltype="cf_sql_varchar" null="#objectIDIsNull#"/>,
						<cf_queryparam value="#arguments.fields#" cfsqltype="cf_sql_varchar" null="#IIF(arguments.fields eq '',DE('true'),DE('false'))#" useCFVersion="false"/>,
						<cf_queryparam value="#Left(loggingName,100)#" cfsqltype="cf_sql_varchar" null="#IIF(loggingName eq '',DE('true'),DE('false'))#" useCFVersion="false"/>,
						'#arguments.direction#','#arguments.dataStruct#',1
					);
					
					SELECT 1 AS synchAttempt, SCOPE_IDENTITY() AS sfErrorID
				END	
			ELSE
				BEGIN
					<!--- 2013-08-08 PPB Case 435688 added left 500 to avoid changing db (in a rare case it's set to cfcatch.message+cfcatch.detail); agreed by GCC  --->	
					update salesForceError set lastUpdated = '#request.requestTime#',
						<cfif loggingName neq "">name='#loggingName#',</cfif>
						message='#Left(arguments.message,500)#',
						dataStruct='#arguments.dataStruct#',
						synchAttempt=synchAttempt+1
					where errorID=@errorID

					select synchAttempt, @errorID as sfErrorID from salesForceError where errorID = @errorID;
				END
			
			<!--- if the related object has been passed in, then add a record in the salesforceRelatedError table and insert a row in the salesForceError table for the related Object if it doesn't already have an error --->
			<cfif not relatedObjectIdIsNull>
			
				<!--- sometimes the relatedObject info coming through is the same as the primary object (usually when dealing with opportunities). If this is the case, then don't put in a related error message--->
				<cfif arguments.object neq arguments.relatedObject and primaryObjectIDValue neq arguments.relatedObjectID>
					if not exists (select 1 from salesForceRelatedError where object = '#arguments.object#' and objectID='#primaryObjectIDValue#' and relatedObject='#arguments.relatedObject#' and relatedObjectId='#arguments.relatedObjectID#')
						insert into salesForceRelatedError (object,objectID,relatedObject,relatedObjectID) values ('#arguments.object#','#primaryObjectIDValue#','#arguments.relatedObject#','#arguments.relatedObjectID#')
						
					if not exists (select 1 from salesForceError where object = '#arguments.relatedObject#' and #primaryObjectIDColumn#='#arguments.relatedObjectID#' and direction='#arguments.direction#')
						insert into salesForceError (object,#primaryObjectIDColumn#,direction,message,method) values ('#arguments.relatedObject#','#arguments.relatedObjectID#','#arguments.direction#','Problem with Related Record','')
				</cfif>	
			<!--- if the object has a 'Problem with related record' record, then delete it as it's no longer needed --->
			<cfelse>
			
				delete from salesForceError where object='#arguments.object#' and #primaryObjectIDColumn#='#primaryObjectIDValue#' and direction='#arguments.direction#' and message='Problem with Related Record'
			</cfif>
			
		</cfquery>
		<!--- END 2013-10-14 PPB Case 437176 --->
		
		<!--- if we're exporting, then we stop the synching for the object and mail the admin
			otherwise, we continue trying to synch, but fire off email every x times.
			
			We have to multiply by 1000000000 as there seem to be some rounding issues when doing the division. Although it would appear to be a whole number, it's not.
			
			Also added a check so that if we're not dealing with the primary object, then don't send an email. This is for the case when we're trying to export 100 people at a given org, and the org can't go
			across because the account manager hasn't been set, for example. In this case, we don't want to send 100 emails about the organisation.
		 --->
		<cfif ((insertSalesForceError.synchAttempt gte maxSFSynchAttempts and (arguments.direction eq "E" or arguments.entityID neq 0))
				or (arguments.direction eq "I" and insertSalesForceError.synchAttempt gt 1 and isValid("integer",int((log(insertSalesForceError.synchAttempt)/log(maxSFSynchAttempts))*1000000000)/1000000000))
				or arguments.sendAlertEmail)
			and (not structKeyExists(request,"salesForceDebug") or (structKeyExists(request.salesForceDebug,"baseEntity") and request.salesForceDebug.baseEntity eq arguments.object))>
			
			<cfset subjectLine = "Maximum Attempts Reached Synching #arguments.object# on #request.currentSite.domainAndRoot# in Relayware/SalesForce Synchronisation">
			<cfif arguments.sendAlertEmail>
				<cfset subjectLine="Error Occurred in Relayware/SalesForce Synchronisation on #request.currentSite.domainAndRoot# That Requires Attention">
			</cfif>
		
			<cfmail to="#salesForceAdminEmail#" from="#application.com.settings.getSetting('emailAddresses.supportEmail')#" subject="#subjectLine#" type="html" priority="urgent">
				
				The following transaction has reached #insertSalesForceError.synchAttempt# <cfif arguments.direction eq "I">import<cfelse>export</cfif> attempts at #request.requestTime#.<br>
				<br>
				<cfif arguments.direction eq "E">
					<cfif arguments.opportunityID neq "" and arguments.opportunityID neq 0>
				Opportunity #arguments.opportunityID# has been suspended from synching.<br>
					<cfelseif listFindNoCase("organisation,location,person",arguments.object)>
				#arguments.object# with #arguments.object#ID #arguments.entityID# has been suspended from synching.<br>
					</cfif>
				</cfif>
				<br>
				<cfif arguments.object neq "">The object is: #arguments.object#<br></cfif>
				<cfif arguments.entityID neq 0 and arguments.entityID neq "">The Relayware #arguments.object#ID is: #arguments.entityID#<br></cfif>
				<cfif arguments.objectID neq 0 and arguments.objectID neq "">The SalesForce objectID is: #arguments.objectID#<br></cfif>
				<cfif not relatedObjectIdIsNull and arguments.object neq "opportunity">The associated opportunity is: #arguments.opportunityID#<br></cfif>
				<cfif arguments.fields neq "">The problem field is: #arguments.fields#<br></cfif>
				<cfif arguments.method neq "">The method the process was trying to run is: #arguments.method#<br></cfif>
				<br>
				The error message is:#arguments.message#<br>
				<br>
				Please resolve the issue to ensure a successful transaction<cfif (arguments.opportunityID neq "" and arguments.opportunityID neq 0) or (listFindNoCase("organisation,location,person",arguments.object))> and flag the record to resume synching</cfif>.<br>
				<br>
				Regards,<br>
				<br>
				RelayWare/SalesForce Synch 
			</cfmail>
			
			<cfif arguments.entityID neq 0>
				<!--- if we are attempting to export an opportunity, then suspend it after max attempts. Flagging the opportunity
					 as suspended , rather than the individual object that the error may be occurring on. ie. there may be a problem
					 with the partnerSalesPersonId, but we don't want to flag the person as suspended...  --->
				<cfif arguments.opportunityID neq "" and arguments.opportunityID neq 0>
					<cfset flagEntityAsSuspended(entityID=arguments.opportunityID,entityType="opportunity")>
				
				<!--- otherwise, if it's a person,location or organisation object, then set the suspend flag --->
				<cfelse>
					<cfif listFindNoCase("organisation,location,person",arguments.object)>
						<cfset recordType = arguments.object>
					<cfelseif listFindNoCase("organisation,location,person",getMappedObject(object=arguments.object,direction="SF2RW").entity)>
						<cfset recordType = getMappedObject(object=arguments.object,direction="SF2RW").entity>
					</cfif>
					<cfif recordType neq "">
						<cfset flagEntityAsSuspended(entityID=arguments.entityID,entityType=recordType)>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		
		<cfreturn insertSalesForceError.sfErrorID>
	</cffunction>
	
	
	<cffunction name="deleteLogEntries" access="public" hint="Deletes any log entries for a given object or entity" output="false">
		<cfargument name="object" type="string" required="false"> <!--- must be passed in if error Id is not passed in --->
		<cfargument name="entityID" type="numeric" required="false" default=0>
		<cfargument name="objectID" type="string" required="false" default=0>
		<cfargument name="method" type="string" required="false" default="">
		<cfargument name="message" type="string" required="false">
		<cfargument name="statusCode" type="string" required="false">
		<cfargument name="direction" type="string" required="false" hint="Is either E for export or I for import"> <!--- should be E or I --->
		<cfargument name="role" type="string" required="false">
		<cfargument name="errorID" type="numeric" required="false">
		
		<cfscript>
			var deleteLogs = "";
			var mappedObject = "";
			var structFindValueArray = "";
			var entityMappingStruct = structNew();
			var entity = "";
			var relatedObjectID = "";
		</cfscript>
		
		<cfif not structKeyExists(arguments,"errorID")>
			<cfif structKeyExists(arguments,"object")>
				<!--- if we're exporting, then we want to get the RW name for the SF object --->
				<cfif structKeyExists(arguments,"direction") and arguments.direction eq "E">
					
					<cfif arguments.method eq "" and arguments.entityID eq 0>
						<cfthrow message="Either a message or an entityID must be passed to select the log entries to delete.">
					</cfif>
					
					<cfset relatedObjectID = arguments.entityID>
					
					<cfloop list="organisation,location,person,oppProducts" index="entity">
						<cfset mappedObject = application.com.salesForce.getMappedObject(object=entity,direction='RW2SF')>
						<cfif not (entity eq "location" and listSort(mappedObject.tableList,"text") eq "location,person")>
							<cfset entityMappingStruct[entity] = mappedObject.entity>	
						</cfif>
					</cfloop>
					
					<cfset structFindValueArray = structFindValue(entityMappingStruct,arguments.object)>
					<cfif arrayLen(structFindValueArray) gt 0>
						<cfset arguments.object = structFindValueArray[1].key>
					</cfif>
					
				<cfelseif structKeyExists(arguments,"direction") and arguments.direction eq "I">
					<!--- if we're importing  --->
					<cfif arguments.method eq "" and arguments.objectID eq 0>
						<cfthrow message="Either a message or an objectID must be passed to select the log entries to delete.">
					</cfif>
					
					<cfset relatedObjectID = arguments.objectID>
					
				<cfelseif not structKeyExists(arguments,"direction")>
					<cfif (not structKeyExists(arguments,"message") or arguments.message eq "") and arguments.objectID eq 0 and arguments.entityId eq 0>
						<cfthrow message="Either a message, an objectID or an entityID must be passed to select the log entries to delete.">
					</cfif>
				</cfif>
			<cfelse>
				<cfthrow message="An object and either a message, an objectID or an entityID must be passed to select the log entries to delete.">
			</cfif>
		</cfif>
		
		<cfquery name="deleteLogs" datasource="#application.siteDataSource#">
			<!--- remove any related records when deleting errors for primary object in the error table--->
			<cfif structKeyExists(arguments,"errorID") or (structKeyExists(arguments,"object") and relatedObjectID neq "" and arguments.method eq "" and not structKeyExists(arguments,"message") and not structKeyExists(arguments,"statusCode") and not structKeyExists(arguments,"role"))>
				delete salesForceRelatedError 
				from salesForceRelatedError sfer inner join salesForceError sfe
					on sfer.relatedObject=sfe.object and sfer.relatedObjectId=case when sfe.direction = 'I' then sfe.objectID else cast(sfe.entityID as varchar(20)) end
				where
					<cfif structKeyExists(arguments,"errorID")>
						sfe.errorID = #arguments.errorID#
					<cfelse>
						sfer.object='#arguments.object#' and sfer.objectID = '#relatedObjectID#'
					</cfif>
			</cfif>
			
			delete from salesForceError where 1=1
			<!--- if an opportunity has been created, then delete all records associated with that opportunity, as sometimes
				these will be errors that may not necessarily get deleted otherwise. If an opportunity has gone across, then we can
				assume that it has correctly exported all that it needed
			--->
		<!--- 
			NJH 2010-10-03 - commented this out as this is now handled by a trigger when the oppSynchFlag is set.
			<cfif arguments.object eq "opportunity" and structKeyExists(arguments,"entityID") and arguments.method eq "create">
				opportunityID=#arguments.entityID#
		<cfelse> --->
				<cfif structKeyExists(arguments,"object")>and object='#arguments.object#'</cfif>
				<cfif arguments.method neq "">and method = '#arguments.method#'</cfif>
				<cfif arguments.entityID neq 0> and entityID = #arguments.entityID#</cfif>
				<cfif arguments.objectID neq 0> and objectID = '#arguments.objectID#'</cfif>
				<cfif structKeyExists(arguments,"message")> and message = '#arguments.message#'</cfif>
				<cfif structKeyExists(arguments,"statusCode")> and statusCode = '#arguments.statusCode#'</cfif>
				<cfif structKeyExists(arguments,"role")> and name = '#arguments.role#'</cfif>
				<cfif structKeyExists(arguments,"errorID")> and errorID = #arguments.errorID#</cfif>
		<!--- </cfif> --->
		</cfquery>

	</cffunction>
	
	
	<cffunction name="synchOpportunityStage" access="public" hint="Brings over any new opportunity stages set on Sales Force" output="false">
		
		<cfscript>
			var methodCallResult = structNew();
			var resultQuery = "";
			var insertNewOpportunityStage = "";
			var mappedObjectStruct = getMappedObject(object="opportunityStage",direction="SF2RW");
			var queryString = "";		/* 2013-03-20 PPB Case 434378 don't build until we know mappedObjectStruct.objectColumnList is good */
		</cfscript>
		
		<cfif StructKeyExists(mappedObjectStruct,"objectColumnList") and mappedObjectStruct.objectColumnList neq "">		<!--- 2013-03-20 PPB Case 434378 --->
			<cfset queryString = "select #mappedObjectStruct.objectColumnList# from opportunityStage">						<!--- 2013-03-20 PPB Case 434378 --->
			<cfset methodCallResult = send(object="opportunityStage",method="query",queryString=queryString,ignoreBulkProtection=true)>
			
			<cfif methodCallResult.success>
				<cfset resultQuery = methodCallResult.response>
				
				<cfif resultQuery.recordCount gt 0 and listFindNoCase(resultQuery.columnList,"masterLabel")>
				
					<cf_qoqToDB query="#resultQuery#" name="##SfOppStages">
					
					<cfquery name="insertNewOpportunityStage">
						insert into oppStage (probability,opportunityStage,liveStatus,stageTextID,sortOrder,status)
						select case when defaultProbability ='' then defaultProbability else 0 end, masterLabel,isActive,left(replace(masterLabel,' ',''),30),case when s.sortOrder = '' then 0 else s.sortOrder end,left(masterLabel,40)
						from ##SfOppStages s
							left join oppStage os on os.status = left(s.masterLabel,40)
						where os.status is null
					</cfquery>
				</cfif>
			</cfif>
		</cfif>

	</cffunction>
	
	
	<!--- 
		This function will assign account managers to opportunities that have just come over from sales force. These account managers
		are people flagged with 'isSalesOperationsAdmin' and come from orgs that have the same name as the vendor organisation (ie. org 2)
		Right now, an opporuntity is assigned to the salesOpAdmin who is in the same country as the end customer. If no saleOpAdmin exists for that
		given country, then it is assigned to the person at organisation 2.	
	 --->
	<cffunction name="assignAccountManagerForNewlyCreatedOpportunities" access="public" output="false">
		<cfargument name="createdSince" type="date" required="true">
		<cfargument name="sendSingleEmailPerAccManager" type="boolean" default="true">
		
		<cfscript>
			var getCreatedOpportunities = "";
			var getVendorDetails = "";
			var getSalesOpAdmins = "";
			var salesOpAdminOpps = "";
			var salesOpAdminPersonID = 0;
			var salesOpAdminStruct = structNew();
			var countryID = 0;
			var getOpportunitiesForCountry = "";
			var getOpportunitiesWithoutSalesOpAdmin = "";
			var emailMergeStruct = structNew();
			var opportunityID = 0;
			var oppStruct = structNew();
		</cfscript>
		
		<cfquery name="getCreatedOpportunities" datasource="#application.siteDataSource#">
			select distinct opp.countryID,opp.opportunityID, opp.detail 
			from modRegister mr with (noLock)
				inner join modEntityDef med with (noLock) on mr.modEntityID = med.modEntityID
				inner join opportunity opp with (noLock) on mr.recordID = opp.opportunityID
			where med.tableName = 'opportunity'
				and med.fieldName = 'Whole Record'
				and mr.action = 'OPPA'
				and mr.modDate >= '#arguments.createdSince#'
				and actionByCF = #request.relayCurrentUser.userGroupID#
		</cfquery>
		
		<cfquery name="getVendorDetails" datasource="#application.siteDataSource#">
			select organisationName,countryID from organisation o with (noLock) 
				inner join organisationType ot with (noLock) on o.organisationTypeID = ot.organisationTypeID
			where organisationID=2 and typeTextID='AdminCompany'
		</cfquery>
		
		<cfquery name="getSalesOpAdmins" datasource="#application.siteDataSource#">
			select p.personID, p.firstname, p.lastname,p.email,p.organisationID,o.countryID 
			from organisation o with (noLock)
				inner join organisationType ot with (noLock) on o.organisationTypeID = ot.organisationTypeID
				inner join person p with (noLock) on p.organisationID = o.organisationID
				inner join #application.com.flag.getFlagStructure("SalesOperationsAdmin").flagType.dataTableFullName# isSalesOperationsAdmin on isSalesOperationsAdmin.entityID = o.organisationID and isSalesOperationsAdmin.flagID = #application.com.flag.getFlagStructure("SalesOperationsAdmin").flagID# and isSalesOperationsAdmin.data = p.personID
			where typeTextID='AdminCompany'
				and organisationName = '#getVendorDetails.organisationName#'
		</cfquery>
		
		<!--- only continue if a person exists at org 2 --->
		<cfif listFind(valueList(getSalesOpAdmins.organisationID),"2")>
				 
			<cfset salesOpAdminStruct = application.com.structureFunctions.queryToStruct(query=getSalesOpAdmins,key="countryID")>
			
			<!--- get the opportunities for a given sales Op Admin --->
			<cfloop collection="#salesOpAdminStruct#" item="countryID">
				<cfquery name="getOpportunitiesForCountry" dbtype="query">
					select * from getCreatedOpportunities where countryID = #countryID#
				</cfquery>
				
				<cfset salesOpAdminStruct[countryID]["opportunities"] = valueList(getOpportunitiesForCountry.opportunityID)>
			</cfloop>
			
			<cfquery name="getOpportunitiesWithoutSalesOpAdmin" dbtype="query">
				select * from getCreatedOpportunities where countryID not in (#structKeyList(salesOpAdminStruct)#)
			</cfquery>
			
			<cfset salesOpAdminStruct[getVendorDetails.countryID]["opportunities"] = salesOpAdminStruct[getVendorDetails.countryID]["opportunities"] & valueList(getOpportunitiesWithoutSalesOpAdmin.opportunityID)>
			
			
			<cfloop collection="#salesOpAdminStruct#" item="countryID">
			
				<cfset salesOpAdminOpps = salesOpAdminStruct[countryID]["opportunities"]>
				<cfset salesOpAdminPersonID = salesOpAdminStruct[countryID]["personID"]>
				
				<cfif salesOpAdminOpps neq "">
				
					<cfloop list="#salesOpAdminOpps#" index="opportunityID">
						<cfset oppStruct = structNew()>
						<cfset oppStruct.vendorAccountManagerPersonID = salesOpAdminPersonID>
						<cfset application.com.relayEntity.updateOpportunityDetails(opportunityID=opportunityID,opportunityDetails=oppStruct)>
					</cfloop>
					
					<cfset emailMergeStruct = structNew()>
					
					<!--- if we're sending one email to the sales op admin with all the opportunities --->
					<cfif arguments.sendSingleEmailPerAccManager>
						<cfset emailMergeStruct.opportunities = salesOpAdminOpps>
						<cfset application.com.email.sendEmail(emailTextID='NewSalesForceOpportunityEmail',personID=salesOpAdminPersonID,mergeStruct=emailMergeStruct)>
					<cfelse>
					
						<!--- or we send a single email per opportunity assigned --->
						<cfloop list="#salesOpAdminOpps#" index="opportunityID">
							<cfset application.com.email.sendEmail(emailTextID='NewSalesForceOpportunityEmail',personID=salesOpAdminPersonID,mergeStruct=emailMergeStruct)>
						</cfloop>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>
		
	</cffunction>
	
	
	<cffunction name="UpdateDeletedObjects" access="public" returnType="struct" hint="Removes the salesForceID from objects that have been deleted on SalesForce" output="false">
		<cfargument name="entityTypes" type="string" default="opportunity">
		<cfargument name="deleteRWReferences" type="boolean" default="true"> <!--- there may be times when we just want to get the items without deleting the ties to RW --->
		
		<cfscript>
			var methodCallResult = structNew();
			var deleteQry = "";
			var entityID = 0;
			var mappedObject = structNew();
			var entityType = "";
			var contactLocationPerson = false;
			var contactProcessed = false;
			var deleteOrphanedPendingRecords = "";
			var deleteOrphanedErrorRecords = "";
			var result = {isOk=true};
			var deletedIDsTableName = "##deletedSalesForceIDs#replace(CreateUUID(),'-','','ALL')#";
			var deleteTmpTable = "";
		</cfscript>

		<cfloop list="#arguments.entityTypes#" index="entityType">
			
			<cfset mappedObject = getMappedObject(object=entityType)>
			<cfif structKeyExists(mappedObject,"entity") and mappedObject.entity neq "">			<!--- 2013-01-24 PPB Case 433103 don't attempt to process an object that isn't mapped (in this case organisation) --->
			    <cfif listFindNoCase("location,person",entityType) and listSort(mappedObject.tableList,"text") eq "location,person">
				    <cfset contactLocationPerson = true>
				    <cfset entityType = "person">
			    </cfif>
    
			    <!--- if we're processing a location or a person which both map to a contact and one has already been done (ie. the contacts have been processed)
				    then don't bother doing it again 
			    --->
			    <cfif not contactProcessed or mappedObject.entity neq "contact">
				    <cfset methodCallResult = send(method="getDeleted", object=mappedObject.entity, startDate=request.utcStartDateTime, endDate=request.utcEndDateTime,throwErrorOn500Response=true)> <!--- throw an error because we don't want to continue. This function is very critical for things like merges --->
				    <cfset deleteQry = methodCallResult.response>
				    
				    <cfif arguments.deleteRWReferences>
					    <cfif methodCallResult.success and listFindNoCase(deleteQry.columnList,"ID")>
					    
					    	<cf_qoqToDB query="#deleteQry#" name="#deletedIDsTableName#" columnList="ID">
					    	
					    	<!--- the name of the temp table that holds the delete Salesforce IDs. The table gets created in the deleteSalesForceIDForEntityV2 function --->
							<cfset deleteSalesForceIDForEntityV2(entityType=entityType,tableSFObjectsToDelete=deletedIDsTableName)>						<!--- 2013-07-15 PPB Case 436080 call to V2 sending query --->
							 
						    <!--- if an SF object has been deleted, then it will error if we attempt to get the record when processing a pending record --->
						    <cfquery name="deleteOrphanedPendingRecords" datasource="#application.siteDataSource#">
							    delete salesForceSynchPending 
							    from salesForceSynchPending sp
							    	inner join #deletedIDsTableName# s on sp.objectID = s.ID and object='#mappedObject.entity#'
						    </cfquery>
						    
						    <!--- delete error records where objects no longer exist on SF --->
						    <cfquery name="deleteOrphanedErrorRecords" datasource="#application.siteDataSource#">
							    delete from salesForceError 
							    from salesForceSynchPending sp
							    	inner join #deletedIDsTableName# s on sp.objectID = s.ID and object='#mappedObject.entity#'
						    </cfquery>
					    </cfif>
					    
					    <!--- if an RW entity has been deleted, then it will error if we attempt to get the record when processing a pending record --->
					    <cfquery name="deleteOrphanedPendingRecords" datasource="#application.siteDataSource#">
						    delete salesForceSynchPending
						    from salesForceSynchPending sp 
						    	inner join #entityType#Del d on d.#application.entityType[application.entityTypeID[entityType]].uniqueKey# = sp.entityID and sp.object = '#entityType#'
					    </cfquery>
					    
					    <!--- delete error records where objects no longer exist on RW --->
					    <cfquery name="deleteOrphanedErrorRecords" datasource="#application.siteDataSource#">
						    delete salesForceError
						    from salesForceError se 
						    	inner join #entityType#Del d on d.#application.entityType[application.entityTypeID[entityType]].uniqueKey# = se.entityID and se.object = '#entityType#'
						    
						   <!--- 2013 Roadmap Release 2 --->	
						    delete salesForceRelatedError
						    from salesForceRelatedError sre
						    	inner join #entityType#Del d on cast(d.#application.entityType[application.entityTypeID[entityType]].uniqueKey# as varchar(20)) = sre.relatedObjectID and sre.relatedObject = '#entityType#'
					    </cfquery>
				    </cfif>
				    
				    <cfif contactLocationPerson>
					    <cfset contactProcessed = true>
				    </cfif>
			    </cfif>
			</cfif>			<!--- 2013-01-24 PPB Case 433103 --->
		</cfloop>
		
		<!--- a bit of clean up--->
		<cfquery name="deleteTmpTable" datasource="#application.siteDataSource#">
	   		if object_id('tempdb..#deletedIDsTableName#') is not null
			begin
			   drop table #deletedIDsTableName#
			end
		</cfquery>
		
		<cfset result.deleteQuery = methodCallResult.response>
		<cfreturn result>

	</cffunction>
	
	
	<cffunction name="ObjectEntitySynch" access="private" returntype="struct" output="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="entityTypeList" type="string" required="true" default="organisation,location,person">
		<cfargument name="sfOpportunityID" required="false"> <!--- this will be used to get opportunity products for a given SF opportunity --->
		<cfargument name="rwOpportunityID" required="false"> <!--- this will be used to get opportunity products for a given RW opportunity --->
		<cfargument name="dtStartTime" type="date" required="true"> <!--- the start time of the current run --->
		<cfargument name="dryRun" type="boolean" required="false" default="false"> <!--- run in dryrun mode --->
		
		<cfscript>
			var getModifiedEntities = queryNew("entityType,entityID,lastModDate,entityCrmID");
			var result = {isOk = true};
			var mappedObject = structNew();
			var relatedObjectStruct = structNew();
			var getAllModifiedEntities = queryNew("entityID,entity","integer,varchar");
			var entityType = "";
			var sfModQuery = "";
			var argStruct = structNew();
			var synchArgStruct = structNew();
			var objArgStruct = structNew();
			var synchResult = structNew();
			var runTrace = application.com.settings.getSetting("salesForce.runTrace");
			var getAllModifiedObjects = queryNew("ID,object","varchar,varchar");
			var getAllFlaggedEntities = queryNew("entityID,entity","integer,varchar");
			var flaggedEntities = "";
			var getModifiedEntitiesStruct = structNew();
			var exportSql = "";
			var importSql = "";
		</cfscript>
		
		<!--- indicates that this is a synch, rather than just an import or export --->
		<cfset request.sfSynch = true>

		
		<!--- setting some variables should this function not be called as a part of the normal scheduled task (ie from the 'get next records to be synched' report, which runs with 'dryRun' equal to true) --->
		<cfif not structKeyExists(request,"utcEndDateTime")>
			<cfset request.utcEndDateTime = createODBCDateTime(arguments.dtStartTime)>
		</cfif>

		<cfif not structKeyExists(request,"utcStartDateTime")>
			<cfset request.utcStartDateTime =application.com.dataTransfers.getLastDataTransferRun(dtdId=arguments.dtdID,lastSuccessfulRun=true,convertToUTC=true)>
		</cfif>
		
		<cfloop list="#arguments.entityTypeList#" index="entityType">
			<cfif (listFindNoCase("organisation,location,person",entityType) and application.com.settings.getSetting("salesForce.ploSynchType") neq "None") or not (listFindNoCase("organisation,location,person",entityType))>
				
				<cfset request.synchLevel = application.com.settings.getSetting("salesForce.synchLevel")>
				<!--- if we're in here because of opportunity products, then set the field level to 'Record'. We don't do field level on opportunity products.
					We'll only be here in this situation when opportunities are set to 'Field level' synching. --->
				<cfif entityType eq "oppProducts">
					<cfset request.synchLevel = "Record">
				</cfif>
				
				<cfset mappedObject = getMappedObject(object=entityType)>
				
				<!--- this might be the case when we are doing a parent/child scenario and we aren't mapping an organisation --->
				<cfif mappedObject.entity neq "">
					<cfset putSFDCObjectIntoMemory(object=mappedObject.entity)>
					
					<cfif not structKeyExists(url,"processPendingData") and entityType neq "oppProducts">
						<cfset flaggedEntities = flagModifiedEntitiesAsSynching(entityTypeList=mappedObject.tableList,modifiedFrom=request.utcStartDateTime,modifiedTo=request.utcEndDateTime)>
					</cfif>
					
					<cfset argStruct = {entityTypeList = mappedObject.tableList,modifiedFrom=request.utcStartDateTime,modifiedTo=request.utcEndDateTime}>
					<cfif structKeyExists(arguments,"rwOpportunityID")>
						<cfset argStruct.opportunityID=arguments.rwOpportunityID>
					</cfif>
					
					<cfset getModifiedEntitiesStruct = getEntitiesFlaggedAsSynching(argumentCollection=argStruct)>
					<cfset getModifiedEntities = getModifiedEntitiesStruct.recordSet>
	
					<cfset objArgStruct = {object=mappedObject.entity,modifiedFrom=request.utcStartDateTime,modifiedTo=request.utcEndDateTime}>
					<cfif structKeyExists(arguments,"sfOpportunityID")>
						<cfset objArgStruct.opportunityID=arguments.sfOpportunityID>
					</cfif>
					<cfset relatedObjectStruct = getModifiedObjectsFromSalesForce(argumentCollection=objArgStruct)>
					<cfset sfModQuery = relatedObjectStruct.response>
					
					<cfquery name="getAllModifiedEntities" dbtype="query">
						select entityID,entity from getModifiedEntities
						union
						select entityID,entity from getAllModifiedEntities
						order by entity
					</cfquery>
					
					<cfif runTrace or arguments.dryRun>
						<cfif isQuery(flaggedEntities)>
							<cfquery name="getAllFlaggedEntities" dbtype="query">
								select entityID,entity from flaggedEntities
								union
								select entityID,entity from getAllFlaggedEntities
								order by entity
							</cfquery>
						</cfif>
					
						<cfquery name="getAllModifiedObjects" dbtype="query">
							select ID,object from sfModQuery
							union
							select ID,object from getAllModifiedObjects
							order by object
						</cfquery>
						
						<cfset exportSql = listAppend(exportSql,getModifiedEntitiesStruct.sql,"|")>
						<cfset importSql = listAppend(importSql,relatedObjectStruct.arguments.queryString,"|")>
					</cfif>
					
					<cfset logDebugInformation(sfModificationsQry=sfModQuery,rwModificationsQry=getModifiedEntities,method="SalesForcePOLSynch")>
					
					<cfif structKeyExists(url,"showDebug")>
						<cfdump var="#sfModQuery#">
						<cfdump var="#getModifiedEntities#">
					</cfif>
					
					<cfif not arguments.dryRun>
						<cfset logDebugInformation(sfModificationsQry=sfModQuery,rwModificationsQry=getModifiedEntities,method="SalesForcePOLSynch")>
					
						<cfif relatedObjectStruct.success>
							<cfif structKeyExists(arguments,"rwOpportunityID")>
								<cfset synchArgStruct.rwOpportunityID=arguments.rwOpportunityID>
							</cfif>
							<cfif structKeyExists(arguments,"sfOpportunityID")>
								<cfset synchArgStruct.sfOpportunityID=arguments.sfOpportunityID>
							</cfif>
							<cfset synchResult = FieldLevelSynch(sfModQuery=sfModQuery,rwModQuery=getModifiedEntities,entityType=entityType,lastSuccessfulRun=request.utcStartDateTime,argumentCollection=synchArgStruct)>
						</cfif>
					
						<cfif entityType eq "oppProducts">
							<cfset request.synchLevel = application.com.settings.getSetting("salesForce.synchLevel")>
						</cfif>
					</cfif>
					<!--- if we're processing a location, and it maps to a contact, then we will be grabbing both the person and location data in one go, and so
						we don't need to process the person data --->
					<cfif entityType eq "location" and listSort(mappedObject.tableList,"text") eq "location,person">
						<cfbreak>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		
		<cfif runTrace or arguments.dryRun>
			<cfset result.traceStruct = {modifiedFrom=request.utcStartDateTime,modifiedTo=request.utcEndDateTime,flaggedEntities = getAllFlaggedEntities,exportEntities = getAllModifiedEntities,exportSql = exportSql,importObjects = getAllModifiedObjects,importSql = importSql}>
		</cfif>

		<cfset result.dataQuery = getAllModifiedEntities>
		<cfreturn result>
	</cffunction>
	
	
	<!--- NJH 2009-10-21 P-FNL079 - function to synch pol data between relayware and SalesForce --->
	<cffunction name="SalesForcePOLSynch" access="public" hint="Synchs relayware POL data with SalesForce" returntype="struct" output="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">
		<cfargument name="dryRun" type="boolean" required="false" default="false">
		
		<cfreturn ObjectEntitySynch(dtdID=arguments.dtdID,entityTypeList="organisation,location,person",dtStartTime=arguments.dtStartTime,dryRun=arguments.dryRun)>
	</cffunction>
	
	
	<!--- NJH 2009-10-21 P-FNL079 - function to synch pol data between relayware and SalesForce --->
	<cffunction name="SalesForceOppSynch" access="public" hint="Synchs relayware opportunity data with SalesForce" returntype="struct" output="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">
		
		<cfreturn ObjectEntitySynch(dtdID=arguments.dtdID,entityTypeList="opportunity",dtStartTime=arguments.dtStartTime)>
	</cffunction>
	
	<!--- START:	2013-08-21	MS	P-EVA001 New function SalesForceLeadSynch for the new RW Opps to SFDC Lead Synch functionality --->
	<cffunction name="SalesForceLeadSynch" access="public" hint="Synchs relayware opportunity data with SalesForce" returntype="struct" output="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn ObjectEntitySynch(dtdID=arguments.dtdID,entityTypeList="opportunity",dtStartTime=arguments.dtStartTime)><!--- 2013-08-21 	MS	P-EVA001 New argument called entityTypeID which would hold the Opportunity Type ID for this new RW Opps to SFDC Lead Synch functionality --->
	</cffunction>
	<!--- END:  	2013-08-21	MS	P-EVA001 New function SalesForceLeadSynch for the new RW Opps to SFDC Lead Synch functionality --->
	
	<!--- START:	2013-03-21	MS	P-EVT003 New function SalesForceCustObjSynch to synch custom standalone objects form SFDC to RW for Evault --->
	<cffunction name="SalesForceCustObjSynch" access="public" hint="Synchs relayware opportunity data with SalesForce" returntype="struct" output="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">
		
		<cfreturn ObjectEntitySynch(dtdID=arguments.dtdID,entityTypeList=arguments.exportEntities,dtStartTime=arguments.dtStartTime)>
	</cffunction>
	<!--- END:  	2013-03-21	MS	P-EVT003 New function SalesForceCustObjSynch to synch custom standalone objects form SFDC to RW for Evault --->
	
	<cffunction name="FieldLevelSynch" access="private" output="false" returntype="struct">
		<cfargument name="sfModQuery" type="query" required="true">
		<cfargument name="rwModQuery" type="query" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="lastSuccessfulRun" type="date" required="true">
		<cfargument name="sfOpportunityID" required="false"> <!--- this will be used to get opportunity products for a given SF opportunity --->
		<cfargument name="rwOpportunityID" required="false"> <!--- this will be used to get opportunity products for a given RW opportunity --->
		
		<cfscript>
			var currentEntityID = 0;
			var result = {isOk = true,rwThresholdReached=false,sfThresholdReached=false};
			var mappedObject = getMappedObject(object=arguments.entityType);
			var hasRecordBeenModifiedInSF = "";
			var polSynchType = application.com.settings.getSetting("salesForce.ploSynchType");
			var exportEntityStruct = structNew();
			var importObjectStruct = structNew();
			var exportEntity = false;
			var importObject = false;
			var previousSfID = "";
			var thisObjectID = "";
			var baseEntity = "";
			var mappedFieldName = structNew();
			var tableFieldname = "";
			var objectFieldName = "";
			var threshold = application.com.settings.getSetting("salesForce.bulkUpdate.threshold");
			var fieldMappingArray = arrayNew(1);
			var foundInUpdateTable = false;
			var fieldList = "";
			var retrieveResult = structNew();
			var importResult = structNew();
			var exportResult = structNew();
			var getNumSFRecords = "";
			var getNumRWRecords = "";
			var getRecordsModifiedInSF = "";
			var hasRecordBeenModifiedInRW = "";
			var importObjectID = "";
			var exportEntityID = "";
			var entityKey = "";
			var thisEntityID = "";
			var objectField = "";
			var deleteSynchPendingRecord = "";
			var entityFieldName = "";
			var objectStruct = structNew();
			var getEntityChanges = "";
			var getObjectChanges = "";
			var argStruct = structNew();
			var getOrgType = "";
			var triggerPoint = "";
			var flagOrFlagGroup = "";
			var objectKey = "";
			var pendingArgStruct = structNew();
			var entitySynchMessage = "";
			var entitySynchResult = structNew();
		</cfscript>

		<cfset baseEntity = getMappedObject(object=mappedObject.entity,direction="SF2RW").entity>
					
		<cfif listFindNoCase(arguments.sfModQuery.columnList,"#mappedObject.entity#ID")>
			<cfquery name="getNumSFRecords" dbtype="query">
				select count (distinct #mappedObject.entity#ID) as numRecords from arguments.sfModQuery
			</cfquery>
		</cfif>
		
		<cfquery name="getNumRWRecords" dbtype="query">
			select count (distinct entityID) as numRecords from arguments.rwModQuery
		</cfquery>

		<cfset importObjectStruct[mappedObject.entity] = structNew()>
		<cfset exportEntityStruct[baseEntity] = structNew()>
		
		<!--- First export changes made on Relayware to SalesForce 
			- if the number of records is less than the threshold, then continue processing --->
		<cfif getNumRWRecords.numRecords lte threshold>
			
			<cfloop query="arguments.rwModQuery">
				<cftry>
					<cfset exportEntity = false>
					<cfset importObject = false>
					 
					<!--- if some records have been modified on SalesForce, check if the current prm record is in the list; otherwise, just export it. --->
					<cfif ((not structKeyExists(url,"processPendingData") and listFindNoCase(arguments.sfModQuery.columnList,"#mappedObject.entity#ID")) or (structKeyExists(url,"processPendingData") and arguments.sfModQuery.recordCount gt 0)) and entityCrmID neq "">
					
						<cfset objectFieldName = getMappedField(object=mappedObject.entity,table=baseEntity,column=fieldName,flagID=IIF(left(fieldname,8) eq "AnyFlag-",DE(newVal),DE("")))>
						<cfset foundInUpdateTable = false>
						
						<!--- this query is cached to improve performance.. for example, when a checkbox flag has many flags selected/deleted, then this will run once for every flag as 
							each flagID is a different record. So, only need to call it once really... 3 minutes is an arbitrary number 
							Removed the cachedWithin because if synch runs less than 3 minute intervals, we get strange results	
						--->
							
						<cfquery name="hasRecordBeenModifiedInSF" dbType="query" maxrows="1">
							select * from arguments.sfModQuery where #mappedObject.entity#ID = '#entityCrmID#' <cfif request.synchLevel eq "field">and lower(field)='#lcase(objectFieldName)#'</cfif>
						</cfquery>
						
						<!--- we have to check the update table if the record is sitting in there, because it's possible that it's not in the SalesForce query --->
						<cfif hasRecordBeenModifiedInSF.recordCount eq 0 and structKeyExists(url,"processPendingData")>
							
							<cfquery name="hasRecordBeenModifiedInSF" datasource="#application.siteDataSource#">
								select top 1 objectID as accountID,newValue,fieldName as field,modDate as lastModifiedDate
									from SalesForceSynchPending with (noLock)
								where objectID = '#entityCrmID#' and object='#mappedObject.entity#' <cfif request.synchLevel eq "field">and fieldName='#objectFieldName#'</cfif> 
							</cfquery>
							
							<cfset foundInUpdateTable = true>
						</cfif>
						
						<!--- have to loop over each field and check to see which field was last edited rather than the last edited on the record --->
						<cfif hasRecordBeenModifiedInSF.recordCount eq 1>
							<cfif listFindNoCase(arguments.sfModQuery.columnList,"processed")>
								<cfset querySetCell(arguments.sfModQuery,"processed",1,hasRecordBeenModifiedInSF.rowNum)>
							</cfif>
							<!--- if the record has not been modified in SalesForce or if it was, but it was made before the change in Relayware, then export the change to SalesForce --->
							<cfif dateCompare(modDate,hasRecordBeenModifiedInSF.lastModifiedDate) neq -1>
								<cfset exportEntity = true>
							<cfelse>
								<cfset importObject = true>
							</cfif>
						<cfelse>
							<cfset exportEntity = true>
						</cfif>
					<cfelse>
						<cfset exportEntity = true>
					</cfif>
					
					<!--- if we're going to be exporting a Relayware entity --->
					<cfif exportEntity>
						<cfset entityKey = entityId>
						<cfif entityCrmID neq "">
							<cfset entityKey = entityID&application.delim1&entityCrmID>
						</cfif>
						<cfif not structKeyExists(exportEntityStruct[baseEntity],entityKey)>
							<cfset exportEntityStruct[baseEntity][entityKey] = structNew()>
						</cfif>
						<!--- START 2015-02-02 AHL Case 443724 Max Sync Attempts. Tracking entity synching. --->
						<cfset exportEntityStruct[baseEntity][entityKey]["entityName"] = arguments.rwModQuery.entity />
						<!--- START 2015-02-02 AHL Case 443724 Max Sync Attempts. --->
					
						
						<!--- if it's a flag, we build up a list of flagIDs, which eventually gets passed to the setFlagData function --->
						<cfif left(fieldName,8) eq "AnyFlag-">
							<cfif not structKeyExists(exportEntityStruct[baseEntity][entityKey],"AnyFlag")>
								<cfset exportEntityStruct[baseEntity][entityKey]["AnyFlag"] = "">
							</cfif>
							<cfset exportEntityStruct[baseEntity][entityKey]["AnyFlag"] = listAppend(exportEntityStruct[baseEntity][entityKey]["AnyFlag"],newVal)>
						<cfelseif fieldName neq "Whole Record">
							<cfset exportEntityStruct[baseEntity][entityKey][fieldname] = newVal>
						</cfif>
						
					<!--- otherwise if we're going to be importing a SalesForce object --->
					<cfelseif importObject>
						<cfset objectKey = entityCrmID&application.delim1&entityID>
						<cfif not structKeyExists(importObjectStruct[mappedObject.entity],objectKey)>
							<cfset importObjectStruct[mappedObject.entity][objectKey] = structNew()>
						</cfif>
						
						<cfif request.synchLevel eq "field">
							<cfset importObjectStruct[mappedObject.entity][objectKey][objectFieldName] = hasRecordBeenModifiedInSF.newValue>
						</cfif>
					</cfif>
					
					<cfcatch>
						<cfif not structKeyExists(url,"processPendingData")>
							<cfset pendingArgStruct = structNew()>
							<cfif  structKeyExists(arguments,"rwOpportunityID")>
								<cfset pendingArgStruct.opportunityID = arguments.rwOpportunityID>
							</cfif>
							<cfset insertPendingRecord(object=arguments.rwModQuery.entity[currentRow],modifiedDate=arguments.rwModQuery.modDate[currentRow],newValue=arguments.rwModQuery.newVal[currentRow],field=arguments.rwModQuery.fieldname[currentRow],objectID=arguments.rwModQuery.entityCRMID[currentRow],entityID=arguments.rwModQuery.entityID[currentRow],argumentCollection=pendingArgStruct)>
						</cfif>
						<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce PolSynch #baseEntity#",Severity="error",catch=cfcatch,WarningStructure=application.com.structureFunctions.queryRowToStruct(query=arguments.rwModQuery,row=currentRow))>
					</cfcatch>
				</cftry>
				
			</cfloop>
			
		<cfelse>
		
			<!--- if the number of records returned is over the threshold, then insert the records into the holding table if they don't
				yet exist. If they do, them update the newValue and moddate fields  --->
			<cfset pendingArgStruct = structNew()>
			<cfif  structKeyExists(arguments,"rwOpportunityID")>
				<cfset pendingArgStruct.opportunityID = arguments.rwOpportunityID>
			</cfif>
			<cfset insertPendingRecords(pendingRecordsQry=arguments.rwModQuery,objectColumn="entity",objectIDColumn="entityCrmID",baseEntity=baseEntity,argumentCollection=pendingArgStruct)>
		</cfif>
		
		<!--- process the salesforce updates --->
		<cfif listFindNoCase(arguments.sfModQuery.columnList,"#mappedObject.entity#ID")>
			<!--- if we are below the threshold and have salesforce records to process --->
			<cfif getNumSFRecords.numRecords lte threshold and getNumSFRecords.numRecords gt 0>

				<!--- ignore any processed records...processed will be 1 if a RW record has superceded this SF change in the RW loop above --->
				<cfquery name="getRecordsModifiedInSF" dbType="query">
					select * from arguments.sfModQuery <cfif listFindNoCase(arguments.sfModQuery.columnList,"processed")>where processed = 0</cfif>
					order by #mappedObject.entity#ID
				</cfquery>
				
				<cfset previousSfID = "">
			
				<!--- Import any changes from SalesForce to Relayware --->
				<cfloop query="getRecordsModifiedInSF">
					<cftry>
						<!--- <cfset thisObjectID = evaluate("#mappedObject.entity#ID")> --->
						<cfset thisObjectID = getRecordsModifiedInSF["#mappedObject.entity#ID"][currentRow]>
						
						<cfset tableFieldName = getMappedField(object=mappedObject.entity,table=baseEntity,objectColumn=field)>
						
						<!--- if we're looking at a new object, see if it's ok to update the entity --->
						<cfif previousSfID neq thisObjectID>
							<cfset logDebugInformation(entityType=mappedObject.entity)>
							<cfif entityID neq "">
								<cfset currentEntityID = entityID> <!--- we can get an entity from the query --->
							<cfelse>
								<cfset currentEntityID = application.com.salesForce.doesEntityExist(object=mappedObject.entity,objectID=thisObjectID,useRWMatchingRules=false).entityID>
							</cfif>
							
							<cfset entitySynchMessage = "">
							
							<!--- it's a new record or for some reason changes have been made but the object doesn't exist on Relayware... it could also be
								that a new record was created and then modified, and we've only picked up the modifications --->
							<cfif ((request.synchLevel eq "field" and field eq "Whole Record") or currentEntityID eq 0) or field eq "Deleted">
								<cfif (listFindNoCase("organisation,location,person",arguments.entityType) and polSynchType eq "Full") or not listFindNoCase("organisation,location,person",entityType)>
									<cfset argStruct = {object = mappedObject.entity,objectID = thisObjectID,objectEntityID = currentEntityID}>
									<cfif request.synchLevel neq "field" and not structKeyExists(url,"processPendingData")>
										<cfset argStruct.objectDetails = application.com.structureFunctions.queryRowToStruct(query=getRecordsModifiedInSF,row=currentRow)>
									</cfif>
									<cfif field eq "Deleted">
										<cfset argStruct.deleteEntity = true>
									</cfif>
									<cfset importResult = import(argumentCollection=argStruct)>

									<cfif not importResult.isOK and not structKeyExists(url,"processPendingData")>
										<cfset pendingArgStruct = structNew()>
										<cfif structKeyExists(arguments,"sfOpportunityID")>
											<cfset pendingArgStruct.opportunityID = arguments.sfOpportunityID>
										</cfif>
										<cfset insertPendingRecord(object=mappedObject.entity,modifiedDate=lastModifiedDate,newValue=newValue,field=field,objectID=thisObjectID,argumentCollection=pendingArgStruct)>
									</cfif>
								</cfif>
								<cfset exportEntity = false>
								
							<cfelse>
								<cfset exportEntity = false>
								
								<!--- if we have an existing entity and we're doing an update and the entity is flagged as synching or if it hasn't matched but we're doing a full pol synch.. ie. creating new people on RW from SF. --->
								<cfif (((currentEntityID neq 0 and polSynchType eq "Update" and application.com.flag.checkBooleanFlagByID(entityId=currentEntityID,flagID="#left(baseEntity,3)#SalesForceSynching")) or polSynchType eq "Full") and listFindNoCase("organisation,location,person",baseEntity)) or not listFindNoCase("organisation,location,person",baseEntity)>
								
									<!--- find out if the object (based on RW trigger points) is available for importing. (ie. if it was ok to export, then it will be ok to import) --->
									<cfset entitySynchResult = canEntityBeSynched(recordType=baseEntity,recordID=currentEntityID)>
									<cfset exportEntity = entitySynchResult.canSynch>
									<cfset entitySynchMessage = entitySynchResult.message>
								</cfif>
							</cfif>
						</cfif>
						<cfset previousSfID = thisObjectID>
							
						<cfif exportEntity>
							<cfset importObject = true>
							
							<cfset deleteLogEntries(object=mappedObject.entity,objectID=thisObjectID,entityID=currentEntityID,method="ObjectImport",direction="I")>
							
							<!--- if we're processing holding data or if we've just put RW records into holding table as it exceeded the threshold, then we need to see if it's been modified in Relayware in the past. We do this by looking at the 
								holding table for Relayware data. If we're not processing holding data, then we can skip this, because the we process and compare live RW
								data first... and we won't be processing any records here that have already been compared. --->
							<cfif structKeyExists(url,"processPendingData") or getNumRWRecords.numRecords gt threshold>

								<cfquery name="hasRecordBeenModifiedInRW" datasource="#application.siteDataSource#">
									select * from SalesForceSynchPending with (noLock) where object='#baseEntity#' and objectID='#thisObjectID#'
										<cfif request.synchLevel eq "field">
											<cfset flagOrFlagGroup = listFirst(tableFieldName,".")>
											<cfif listFindNoCase("flag,flagGroup",flagOrFlagGroup)>and fieldName like 'AnyFlag-%' and newValue <cfif flagOrFlagGroup eq "flagGroup"> in (#request.sfFlagsInFlagGroup[listLast(tableFieldName,".")]#) <cfelse> = #listLast(tableFieldName,".")#</cfif>
											<cfelse>and fieldName='#tableFieldName#'
											</cfif>
										</cfif>
								</cfquery>
								
								<cfif hasRecordBeenModifiedInRW.recordCount>
									<!--- if the record has been modified in Relayware before the change in SalesForce, then import the change to SalesForce --->
									<cfif dateCompare(lastModifiedDate,hasRecordBeenModifiedInRW.modDate) eq -1>
										<cfset importObject = false>
									</cfif>
								</cfif>
							</cfif>
							
							<!--- building the import/export structures --->
							<cfif importObject>
								<cfset objectKey = thisObjectID>
								<cfif currentEntityId neq 0>
									<cfset objectKey = thisObjectID&application.delim1&currentEntityId>
								</cfif>
								<cfif not structKeyExists(importObjectStruct[mappedObject.entity],objectKey)>
									<cfset importObjectStruct[mappedObject.entity][objectKey] = structNew()>
								</cfif>
								<!--- a place holder if not doing field synching --->
								<cfif request.synchLevel eq "field">						
									<cfset importObjectStruct[mappedObject.entity][objectKey][field] = getRecordsModifiedInSF.newValue>
								</cfif>
							<cfelse>
								<!--- if the record exists in the pending table and is waiting to be exported, then lets export it...
									It may be that it just got put into the pending table because it was in a batch of records that was over the threshold.
									I'm not sure whether we should export this change here and now, or wait until the object gets picked up to go across.
									This has the potential of more SFDC calls, as we could potentially be exporting a field at a time rather than the whole object should the 
									object have other changes that need to go... we could also just delete the current record that we're processing so that we do not process it again
								 --->
								 <cfset entityKey = hasRecordBeenModifiedInRW.entityID&application.delim1&hasRecordBeenModifiedInRW.objectID>

								<cfif not structKeyExists(exportEntityStruct[baseEntity],entityKey)>
									<cfset exportEntityStruct[baseEntity][entityKey] = structNew()>
								</cfif>
								<cfset exportEntityStruct[baseEntity][entityKey][hasRecordBeenModifiedInRW.fieldname] = hasRecordBeenModifiedInRW.newValue>
							</cfif>
							
						<!--- CASE 426374: we'll be in here if approval is a trigger point and the entity is not approved or if org type is set to never synch.. In this case, we want to throw an error 
							We really are only going to be here if a record has been unapproved or if the org type has changed while sitting in the bulk queue...
						--->
						<cfelseif entitySynchMessage neq "" and structKeyExists(url,"processPendingData")>
							<cfset logSFError(object=mappedObject.entity,objectID=thisObjectID,entityID=currentEntityID,method="ObjectImport",message=entitySynchMessage,fields="",direction="I")>
						</cfif>
					
						<cfcatch>
							<cfif not structKeyExists(url,"processPendingData")>
								<cfset pendingArgStruct = structNew()>
								<cfif  structKeyExists(arguments,"sfOpportunityID")>
									<cfset pendingArgStruct.opportunityID = arguments.sfOpportunityID>
								</cfif>
								<cfset insertPendingRecord(object=mappedObject.entity,modifiedDate=getRecordsModifiedInSF.lastModifiedDate[currentRow],newValue=getRecordsModifiedInSF.newValue[currentRow],field=getRecordsModifiedInSF.field[currentRow],objectID=thisObjectID,argumentCollection=pendingArgStruct)>
							</cfif>
							
							<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce PolSynch #mappedObject.entity#",Severity="error",catch=cfcatch,WarningStructure=application.com.structureFunctions.queryRowToStruct(query=getRecordsModifiedInSF,row=currentRow))>
						</cfcatch>
					</cftry>
				</cfloop>

			<cfelse>
			
				<!--- if we're over the threshold, put the records into a holding table. --->
				<cfset pendingArgStruct = structNew()>
				<cfif  structKeyExists(arguments,"sfOpportunityID")>
					<cfset pendingArgStruct.opportunityID = arguments.sfOpportunityID>
				</cfif>
				
				<cfset insertPendingRecords(pendingRecordsQry=arguments.sfModQuery,objectColumn="object",objectIDColumn="#mappedObject.entity#ID",modDateColumn="lastModifiedDate",fieldNameColumn="field",newValueColumn="newValue",baseEntity=baseEntity,argumentCollection=pendingArgStruct)>
			</cfif>
		</cfif>

		<!--- Logging which entity we are now currently processing --->
		<cfset logDebugInformation(entityType=baseEntity)>
		
		<cfloop collection="#exportEntityStruct[baseEntity]#" item="exportEntityID">						
			<!--- if an entity on RW has a CRMID, then the exportEntityID variable will look like 'entityID|objectID' so that we can delete any related object records--->
			<cfset thisEntityID = listFirst(exportEntityID,application.delim1)>
			<cfset thisObjectID = "">
			<cfif listLen(exportEntityID,application.delim1) eq 2>
				<cfset thisObjectID = listLast(exportEntityID,application.delim1)>
			</cfif>
			
			<cfset argStruct = {entityType=baseEntity,entityID=thisEntityID,entityObjectID = thisObjectID}>
			<cfif structKeyExists(exportEntityStruct[baseEntity][exportEntityID],"Deleted")>
				<cfset argStruct.deleteObject = true>
			<cfelse>
				<!--- if we're looking at field changed and it has a salesForceID, then just export the changes; otherwise we export the whole record --->
				<cfif request.synchLevel eq "field" and thisObjectID neq "" and structCount(exportEntityStruct[baseEntity][exportEntityID])>
					<cfset argStruct.entityDetails=exportEntityStruct[baseEntity][exportEntityID]>
				</cfif>
			</cfif>
			
			<cfset exportResult.isOK = true>
			<!--- START 2015-02-02 AHL Case 443724 Max Sync Attempts. --->
			<!--- Through this ugly hack I make sure that the log receive the entity --->
			<cfif StructKeyExists(exportEntityStruct[baseEntity][exportEntityID],'entityName')>
				<cfset this.m_entityType = exportEntityStruct[baseEntity][exportEntityID].entityName />
			</cfif>
			<!--- END 2015-02-02 AHL Case 443724 Max Sync Attempts. --->
			<cftry>
				<cfset exportResult = export(argumentCollection=argStruct)>
				<cfcatch>
					<cfset exportResult.isOK = false>
				</cfcatch>
			</cftry>
			<cfset this.m_entityType = "" /><!--- 2015-02-02 AHL Case 443724 Max Sync Attempts. --->

			<cfif not exportResult.isOK and not structKeyExists(url,"processPendingData")>
				<cfquery name="getEntityChanges" dbtype="query">
					select * from arguments.rwModQuery where lower(entity)='#lcase(baseEntity)#' and entityID=#thisEntityID#
				</cfquery>

				<cfset pendingArgStruct = structNew()>
				<cfif  structKeyExists(arguments,"rwOpportunityID")>
					<cfset pendingArgStruct.opportunityID = arguments.rwOpportunityID>
				</cfif>
				<cfset insertPendingRecords(pendingRecordsQry=getEntityChanges,objectColumn="entity",objectIDColumn="entityCrmID",baseEntity=baseEntity,argumentCollection=pendingArgStruct)>
			</cfif>
		</cfloop>
		
		<!--- Logging which object we are now currently processing --->
		<cfset logDebugInformation(entityType=mappedObject.entity)>
		
		<cfloop collection="#importObjectStruct[mappedObject.entity]#" item="importObjectID">
		
			<!--- if an entity on RW has a CRMID, then the exportEntityID variable will look like 'entityID|objectID' so that we can delete any related object records--->
			<cfset thisObjectID = listFirst(importObjectID,application.delim1)>
			<cfset thisEntityID = 0>
			<cfif listLen(importObjectID,application.delim1) eq 2>
				<cfset thisEntityID = listLast(importObjectID,application.delim1)>
			</cfif>
			
			<cfif request.synchLevel eq "field">
				<!--- loop through fields and if any are picklist or large text, retrieve the values. THis is because the history table
					only records the fact that they have been changed and doesn't hold what value they currently are. So, here we have to go
					and retrieve their current values
				 --->
				<cfset fieldList = "">
				<cfloop collection="#importObjectStruct[mappedObject.entity][importObjectID]#" item="objectField">
					<cfif not listFindNoCase("Whole Record,Opportunity Products",objectField) and listFindNoCase("multipicklist,textArea",application.sfObjects[mappedObject.entity][objectField].type)>
						<cfset fieldList = listAppend(fieldList,objectField)>
					</cfif>
				</cfloop>
				
				<cfif fieldList neq "">
					<cfset retrieveResult = send(method="retrieve",fieldlist=fieldList,object=mappedObject.entity,ids=thisObjectID)>
					<cfif retrieveResult.isOK and listFindNoCase(retrieveResult.response.columnList,"ID")>
						<cfloop list="#fieldList#" index="objectFieldName">
							<cfset importObjectStruct[mappedObject.entity][importObjectID][objectFieldName] = retrieveResult.response[objectFieldName][1]>
						</cfloop>
					</cfif>
				</cfif>
			
			<!--- if we're processing table records and not getting pending records, then we have all the data, so get it and pass it through 
				if we have an entityID, that means we are doing an update so only get synched pending fields, not the whole list.
			--->
			<cfelseif not structKeyExists(url,"processPendingData")>
				<cfif thisEntityID eq 0>
					<cfset fieldList = getMappedObject(object=mappedObject.entity,direction="SF2RW").objectColumnList>
				<cfelse>
					<cfset fieldList = getMappedObject(object=mappedObject.entity,direction="SF2RW").synchedObjectColumnList>
				</cfif>
				<cfset fieldList = application.com.salesforceAPI.setObjectFieldsForQuery(object=mappedObject.entity,fieldlist=fieldList,useLabel=false)>
				
				<cfset importObjectStruct[mappedObject.entity][importObjectID] = structNew()>			

				<!--- NJH 2012/02/20 CASE 426374: we should never get into the catch here, but if we do, we now just retrieve all the object details when we do the export --->
				<cfif not structKeyExists(request,"salesForceResultsExceedsThreshold") or not request.salesForceResultsExceedsThreshold>
					<cftry>
						<cfquery name="hasRecordBeenModifiedInSF" dbType="query" maxrows="1">
							select #fieldList# from arguments.sfModQuery where #mappedObject.entity#ID = '#thisObjectID#'
						</cfquery>
						
						<cfset importObjectStruct[mappedObject.entity][importObjectID] = application.com.structureFunctions.queryRowToStruct(query=hasRecordBeenModifiedInSF,row=1)>
						<cfcatch>
						</cfcatch>
					</cftry>	
				</cfif>
			</cfif>
			
			<cfset importResult.isOK = true>
			<cftry>
				<cfset argStruct = {object = mappedObject.entity,objectID = thisObjectID,objectEntityID = thisEntityID}>
				<cfif structCount(importObjectStruct[mappedObject.entity][importObjectID])>
					<cfset argStruct.objectDetails = importObjectStruct[mappedObject.entity][importObjectID]>
				</cfif>
				<cfset bSuccess = StructAppend(importResult,import(argumentCollection=argStruct),true)> <!--- 2014-11-04 AHL Case 442476 Lead Synch Jammed --->
				<cfcatch>
					<!--- START 2013-07-05 PPB Case 436049 throw error if import failed (eg if oppProductsDel table is out of sync with oppProducts) --->
					<cfset opportunityID = "">
					<cfif mappedObject.entity eq "opportunity">
						<cfset opportunityID = thisEntityID>		<!--- I'm sending thisEntityID instead of thisObjectID because it is numeric (to avoid an error when the record is automatically set to suspended after X retries) --->
					</cfif>
					<cfset logSFError(opportunityID=opportunityID,object=mappedObject.entity,entityID=thisEntityID,method="FieldLevelSynch",message="Import failed",direction="I",dataStruct=argStruct)>		<!--- 2014-02-05 PPB Case 438759 add dataStruct --->
					
					<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce Import Failed",Severity="error",catch=cfcatch,WarningStructure=argStruct)>   	<!--- 2014-02-05 PPB Case 438759 log a relay error --->	
					<!--- END 2013-07-05 PPB Case 436049 --->
					<cfset importResult.isOK = false>
				</cfcatch>
			</cftry>
				
			<cfif not importResult.isOK and not structKeyExists(url,"processPendingData")>
				<cfquery name="getObjectChanges" dbtype="query">
					select * from arguments.sfModQuery where #mappedObject.entity#ID='#thisObjectID#'
				</cfquery>
				
				<cfset pendingArgStruct = structNew()>
				<cfif  structKeyExists(arguments,"sfOpportunityID")>
					<cfset pendingArgStruct.opportunityID = arguments.sfOpportunityID>
				</cfif>

				<cfset insertPendingRecords(pendingRecordsQry=getObjectChanges,objectColumn="object",objectIDColumn="#mappedObject.entity#ID",modDateColumn="lastModifiedDate",newValueColumn="newValue",fieldnameColumn="field",baseEntity=mappedObject.entity,argumentCollection=pendingArgStruct)>
			</cfif>
		</cfloop>
		
		<cfreturn result>
				
	</cffunction>
	
	
	<!--- NJH 2009-10-21 P-FNL079 - function to import objects from sales force --->
	<cffunction name="SalesForceImport" access="public" hint="Imports SalesForce objects into Relayware" returntype="struct" output="false">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="object" type="string" default="opportunity">
		<cfargument name="dtStartTime" type="date" required="true">
		
		<cfscript>
			var result = {isOK="true",message=""};
			var methodCallResult = structNew();
			var mappedObjectStruct = getMappedObject(object=arguments.object,direction="SF2RW");
			var objectStruct = structNew();
			var lastSuccessfulRun = application.com.dataTransfers.getLastDataTransferRun(dtdId=arguments.dtdID,lastSuccessfulRun=true,convertToUTC=true);
			var entityExistsStruct = structNew();
			var importObject = true;
			var threshold =  application.com.settings.getSetting("salesForce.bulkUpdate.threshold");
			var polData = false;
			var entityID = 0;
			var importResult = structNew();
			var sfModQuery = "";
			var thisObjectID = "";
			var getDistinctObjects = "";
			var objectField = "";
			var getObjectChanges = "";
			var argStruct = structNew();
			var fieldList = "";
			var retrieveResult = structNew();
			var rwModQuery = queryNew("entityID");
		</cfscript>
		
		<cfset logDebugInformation(entityType=arguments.object)>
		
		<cfif listFindNoCase("organisation,location,person",mappedObjectStruct.entity)>
			<cfset polData = true>
		</cfif>
		
		<cfset request.synchLevel = "Record">
		
		<cfset methodCallResult = getModifiedObjectsFromSalesForce(object=arguments.object,modifiedFrom=lastSuccessfulRun,polData=polData,modifiedTo=application.com.dateFunctions.convertServerDateToUTC(arguments.dtStartTime))>
		<cfset sfModQuery = methodCallResult.response>

		<!--- log the current query that we're working with --->
		<cfset logDebugInformation(sfModificationsQry=sfModQuery,method="SalesForceImport",entityType=arguments.object)>
		
		<cfif methodCallResult.success>
			<cfset fieldLevelSynch(rwModQuery=rwModQuery,sfModQuery=sfModQuery,entityType=mappedObjectStruct.entity,lastSuccessfulRun=lastSuccessfulRun)>
		<cfelse>
			<cfset result.isOK = false>
			<cfset result.message = "Error querying SalesForce for object #arguments.object#. #methodCallResult.message#">
		</cfif>

		<cfreturn result>
	</cffunction>
	
	
	<cffunction name="SalesForceExport" access="public" hint="Exports relayware entities to SalesForce" returntype="query" output="false">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="entityType" type="string" default="opportunity">
		<cfargument name="dtStartTime" type="date" required="true">
		
		<cfscript>
			var getModifiedEntities = "";
			var lastSuccessfulRun = application.com.dataTransfers.getLastDataTransferRun(dtdId=arguments.dtdID,lastSuccessfulRun=true,convertToUTC=true);
			var threshold =  application.com.settings.getSetting("salesForce.bulkUpdate.threshold");
			var exportResult = structNew();
			var mappedObject = getMappedObject(object=entityType);
			var previousEntityID = "";
			var entityStruct = structNew();
			var exportEntity = false;
			var getDistinctEntities = "";
			var getEntityChanges = "";
			var argStruct = structNew();
			var containsRelatedData = false;
			var sfModQuery = queryNew("tmp");
			var dtStartTimeInUTC = application.com.dateFunctions.convertServerDateToUTC(arguments.dtStartTime);
		</cfscript>
		
		<cfset logDebugInformation(entityType=arguments.entityType)>
		
		<cfset request.synchLevel = "Record">

		<cfif not structKeyExists(url,"processPendingData")>
			<cfset application.com.salesForce.flagModifiedEntitiesAsSynching(entityTypeList=mappedObject.tableList,modifiedFrom=lastSuccessfulRun,modifiedTo=dtStartTimeInUTC)>
		</cfif>
		<cfset getModifiedEntities = getEntitiesFlaggedAsSynching(entityTypeList=mappedObject.tableList,modifiedFrom=lastSuccessfulRun,modifiedTo=dtStartTimeInUTC).recordSet>
		
		<!--- logging the query and entityType that we are currently processing --->
		<cfset logDebugInformation(rwModificationsQry=getModifiedEntities,method="SalesForceExport",entityType=arguments.entityType)>
		
		<cfset fieldLevelSynch(rwModQuery=getModifiedEntities,sfModQuery=sfModQuery,entityType=arguments.entityType,lastSuccessfulRun=lastSuccessfulRun)>
		
		<cfreturn getModifiedEntities>
	</cffunction>
	
	
	<cffunction name="setMappedField" access="private" returnType="string" hint="Builds a structure in memory with field level mappings" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectColumn" type="string" required="false">
		<cfargument name="table" type="string" required="true">
		<cfargument name="column" type="string" required="false">
		<cfargument name="flagID" type="string" default="">
		
		<cfscript>
			var fieldMappingArray = "";
			var flagStruct = structNew();
			var fieldName = "";
		</cfscript>
		
		<cfif structKeyExists(arguments,"objectColumn")>
			<cfset fieldName = arguments.objectColumn>
		<cfelseif structKeyExists(arguments,"object")>
			<cfset fieldName = arguments.object>
		</cfif>
		
		<!--- These 3 field names are not in the mapping, but are common in both modification queries. --->
		<cfif listFindNoCase("Opportunity Products,Deleted,Whole Record",fieldName)>
			<cfreturn fieldName>
		</cfif>
		
		<cfif not structKeyExists(request,"sfMappedFieldName")>
			<cfset request.sfMappedFieldName = structNew()>
		</cfif>
		
		<cfif structKeyExists(arguments,"objectColumn")>
			<cfif not structKeyExists(request.sfMappedFieldName,"sf.#arguments.object#.#arguments.objectColumn#")>
				<cflock name="SalesforceXMLSearch" timeout="3">
					<cfset fieldMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.object)#'][translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.objectColumn)#']")>
				</cflock>
				
				<cfif arrayLen(fieldMappingArray)>
					<cfif fieldMappingArray[1].xmlAttributes.column neq "">
						<cfset request.sfMappedFieldName["sf.#arguments.object#.#arguments.objectColumn#"] = fieldMappingArray[1].xmlAttributes.column>
					<cfelse>
						<cfif structKeyExists(fieldMappingArray[1].xmlAttributes,"flagID")>
							<cfset request.sfMappedFieldName["sf.#arguments.object#.#arguments.objectColumn#"] = "flag.#application.com.flag.getFlagStructure(flagID=fieldMappingArray[1].xmlAttributes.flagID).flagID#">
						<cfelseif structKeyExists(fieldMappingArray[1].xmlAttributes,"flagGroupID")>
							<cfset request.sfMappedFieldName["sf.#arguments.object#.#arguments.objectColumn#"] = "flagGroup.#application.com.flag.getFlagGroupStructure(flagGroupID=fieldMappingArray[1].xmlAttributes.flagGroupID).flagGroupID#">
						</cfif>
					</cfif>
				</cfif>
			</cfif>
			
			<cfif structKeyExists(request.sfMappedFieldName,"sf.#arguments.object#.#arguments.objectColumn#")>
				<cfreturn request.sfMappedFieldName["sf.#arguments.object#.#arguments.objectColumn#"]>
			</cfif>
		</cfif>
					
		<cfif structKeyExists(arguments,"column")>
		
			<!--- get the salesForce column name for a given Relayware field - if it's a flag, the flagID is stored as the column name --->
			<cfif left(arguments.column,8) eq "AnyFlag-" and arguments.flagID neq "">
			
				<cfset flagStruct = application.com.flag.getFlagStructure(flagID=arguments.flagID)>
				<cflock name="SalesforceXMLSearch" timeout="3">
					<cfset fieldMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.table)#'][@flagID='#arguments.flagID#' or translate(@flagID,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(flagStruct.flagTextID)#' or @flagGroupID='#flagStruct.flagGroup.flagGroupID#' or translate(@flagGroupID,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(flagStruct.flagGroup.flagGroupTextID)#']")>
				</cflock>
				
				<cfif arrayLen(fieldMappingArray)>
					<cfif structKeyExists(fieldMappingArray[1].xmlAttributes,"flagID")>
						<cfif not structKeyExists(request.sfMappedFieldName,"rw.#arguments.table#.flag.#flagStruct.flagID#")>
							<cfset request.sfMappedFieldName["rw.#arguments.table#.flag.#flagStruct.flagID#"] = fieldMappingArray[1].xmlAttributes.objectColumn>
							<cfset request.sfMappedFieldName["sf.#arguments.object#.#fieldMappingArray[1].xmlAttributes.objectColumn#"] = "flag.#flagStruct.flagID#">
						</cfif>
						
						<cfreturn request.sfMappedFieldName["rw.#arguments.table#.flag.#flagStruct.flagID#"]>
						
					<cfelseif structKeyExists(fieldMappingArray[1].xmlAttributes,"flagGroupID")>
						<cfif not structKeyExists(request.sfMappedFieldName,"rw.#arguments.table#.flag.#arguments.flagID#")>
							<cfset request.sfMappedFieldName["rw.#arguments.table#.flagGroup.#flagStruct.flagGroupID#"] = fieldMappingArray[1].xmlAttributes.objectColumn>
							<cfset request.sfMappedFieldName["sf.#arguments.object#.#fieldMappingArray[1].xmlAttributes.objectColumn#"] = "flagGroup.#flagStruct.flagGroup.flagGroupID#">
						</cfif>
						
						<cfreturn request.sfMappedFieldName["rw.#arguments.table#.flagGroup.#flagStruct.flagGroupID#"]>
					</cfif>
				</cfif>
			<cfelse>
				<cfif not structKeyExists(request.sfMappedFieldName,"rw.#arguments.table#.#arguments.column#")>
					<!--- when searching for a rw mapping, then search both the object and table as the table could be a location in xml, but it is passed through as person, as that is the base object --->
					<cflock name="SalesforceXMLSearch" timeout="3">
						<cfset fieldMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@table,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.table)#' or translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.object)#'][translate(@column,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.column)#']")>
					</cflock>
					
					<cfif arrayLen(fieldMappingArray) gt 0>
						<cfset request.sfMappedFieldName["rw.#arguments.table#.#arguments.column#"] = fieldMappingArray[1].xmlAttributes.objectColumn>
						<cfset request.sfMappedFieldName["sf.#arguments.object#.#fieldMappingArray[1].xmlAttributes.objectColumn#"] = arguments.column>
					</cfif>
				</cfif>
				<cfif structKeyExists(request.sfMappedFieldName,"rw.#arguments.table#.#arguments.column#")>
					<cfreturn request.sfMappedFieldName["rw.#arguments.table#.#arguments.column#"]>
				</cfif>
			</cfif>
		</cfif>
		
		<cfreturn "">
	</cffunction>
	
	
	<cffunction name="getMappedField" access="private" returntype="string" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objectColumn" type="string" required="false">
		<cfargument name="table" type="string" required="true">
		<cfargument name="column" type="string" required="false">
		<cfargument name="flagID" type="string" default="">
		
		<cfreturn setMappedField(argumentCollection=arguments)>
	</cffunction>
	
	
	<cffunction name="insertPendingRecord" access="private" output="false" hint="Insert a single pending record">
		<cfargument name="field" type="string" required="true">
		<cfargument name="newValue" type="string" required="true">
		<cfargument name="modifiedDate" type="date" required="true">
		<cfargument name="objectID" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="false" default=0>
		<cfargument name="object" type="string" required="true">
		<cfargument name="opportunityID" required="false">
		
		<cfscript>
			var insertIntoSynchPending = "";
			var argStruct = structNew();
			var thisOpportunityID = "";
			var argPendingStruct = structNew();
			var recordID = "";
			var objectIDIsNull = false;
			var entityIDIsNull = false;
		</cfscript>
		
		<cfif structKeyExists(arguments,"opportunityID")>
			<cfset thisOpportunityID = arguments.opportunityID>
		</cfif>
		
		<cfif arguments.entityID eq 0 or arguments.entityID eq "">
			<cfset recordID = arguments.objectID>
			<cfset entityIDIsNull = true>
		<cfelse>
			<cfset recordID = arguments.entityID>
		</cfif>
		
		<cfif arguments.objectID eq "" or arguments.objectID eq 0>
			<cfset objectIDIsNull = true>
		</cfif>
		
		<cfif canEntityBeSynched(recordType=arguments.object,recordID=recordID).canSynch>
		
			<!--- if no entityID is passed, then it's an import --->
			<cfquery name="insertIntoSynchPending" dataSource="#application.siteDataSource#">
				declare @ID int
				select @ID = ID from SalesForceSynchPending with (noLock) where fieldname='#arguments.field#' and object='#arguments.object#'
					<cfif entityIDIsNull>
						and objectID='#arguments.objectId#' and entityID is null
					<cfelse>
						and entityID=#arguments.entityID#
					</cfif>
				
				if (@ID is not null)
					update SalesForceSynchPending set newValue=N'#arguments.newValue#',modDate='#arguments.modifiedDate#',lastUpdated='#request.requestTime#',lastUpdatedBy=#request.relayCurrentUser.userGroupId# <cfif structKeyExists(arguments,"opportunityID")>,opportunityID='#arguments.opportunityID#'</cfif> where ID = @ID
				else
					insert into SalesForceSynchPending (<cfif not objectIDIsNull>objectID,</cfif><cfif not entityIDIsNull>entityID,</cfif>object,fieldname,newValue,modDate,createdBy,lastUpdatedBy<cfif structKeyExists(arguments,"opportunityID")>,opportunityID</cfif>,direction)
					values (<cfif not objectIDIsNull>'#arguments.objectId#',</cfif><cfif not entityIDIsNull>#arguments.entityID#,</cfif><cf_queryparam value="#arguments.object#" cfsqltype="cf_sql_varchar" null="#objectIDIsNull#"/>,'#arguments.field#',N'#arguments.newValue#','#arguments.modifiedDate#',#request.relayCurrentUser.userGroupId#,#request.relayCurrentUser.userGroupId#<cfif structKeyExists(arguments,"opportunityID")>,'#arguments.opportunityID#'</cfif>,<cfif entityIDIsNull>'I'<cfelse>'E'</cfif>) 
			</cfquery>
			
			<!--- if we've just inserted a pending record for an opportunity product, then we want to put in a record for the opportunity, as the product won't get picked up for 
				synching (by bulk update) on its own, but it needs an opportunity record. The opportunity record will get picked up, and then it checks to see whether it has any products that need going across --->
			<cfif listFindNoCase("oppProducts,opportunityLineItem",arguments.object) and structKeyExists(arguments,"opportunityID")>
				<cfset argPendingStruct = {object = "opportunity",modifiedDate = arguments.modifiedDate,newValue = "",field = "Opportunity Products"}>
				<cfif isNumeric(arguments.opportunityID)>
					<cfset argPendingStruct.entityID = arguments.opportunityID>
				<cfelse>
					<cfset argPendingStruct.objectID = arguments.opportunityID>
				</cfif>
				<cfset insertPendingRecord(argumentCollection=argPendingStruct)>
			</cfif>
		</cfif>
		
	</cffunction>
	
	
	<cffunction name="insertPendingRecords" access="private" output="false" hint="Bulk insert pending records. Used for performance reasons">
		<cfargument name="pendingRecordsQry" type="query" required="true">
		<cfargument name="entityIDColumn" type="string" default="entityID">
		<cfargument name="objectIDColumn" type="string" default="objectID">
		<cfargument name="objectColumn" type="string" default="object">
		<cfargument name="fieldNameColumn" type="string" default="fieldName">
		<cfargument name="newValueColumn" type="string" default="newVal">
		<cfargument name="modDateColumn" type="string" default="modDate">
		<cfargument name="opportunityID" required="false">
		<cfargument name="baseEntity" type="string" required="true">
		
		<cfscript>
			var createTempTable = "";
			var insertIntoTempTable = "";
			var updateExistingSynchPending = "";
			var insertNewSynchPending = "";
			var startRow = 0;
			var endRow = 0;
			var counter = 1;
			var count = 1;
			var processNumRecords = 1000;
			var tmpEntityID = 0;
			var tmpObjectID = "";
			var getObjectType = "";
			var argPendingStruct = structNew();
			var triggerPoints = application.com.settings.getSetting("salesForce.synchTriggerPoints.plo");
			var updateErrorSynchAttempt = "";
		</cfscript>
		
		<cfif arguments.pendingRecordsQry.recordCount>
			<cfquery name="createTempTable" datasource="#application.siteDataSource#">
				if object_id('tempdb..##PendingRecords') is not null
				begin
				   drop table ##PendingRecords
				end
				select entityID,objectID,object,fieldname,newValue,modDate,opportunityID,direction into ##PendingRecords from salesForceSynchPending with (noLock) where 1=0
			</cfquery>
			
			<!--- TODO: might need to work out the logic a bit better here --->
			<cfset counter = (arguments.pendingRecordsQry.recordCount\processNumRecords)+1>
			<cfloop from="1" to="#counter#" step="1" index="count">
				<cfset startRow = endRow+1>
				<cfif count neq counter>
					<cfset endRow = count*processNumRecords>
				<cfelse>
					<cfset endRow = startRow-1+(arguments.pendingRecordsQry.recordCount mod processNumRecords)>
				</cfif>

				<!--- NJH 2012-02-20 CASE 426374: don't evaluate the column names, as it downgrades performance considerably.. instead, reference it like a structure --->
				<cfif startRow lt arguments.pendingRecordsQry.recordCount or arguments.pendingRecordsQry.recordCount eq 1>
					<cfquery name="insertIntoTempTable" dataSource="#application.siteDataSource#">
						insert into ##PendingRecords (entityID,objectID,object,fieldname,newValue,modDate<cfif structKeyExists(arguments,"opportunityID")>,opportunityID</cfif>,direction)
						<cfloop query="arguments.pendingRecordsQry" startRow="#startRow#" endrow="#endRow#">
							<cfset tmpEntityID = arguments.pendingRecordsQry[arguments.entityIDColumn][currentRow]>
							<cfset tmpObjectID = arguments.pendingRecordsQry[arguments.objectIDColumn][currentRow]>
							select <cfif tmpEntityID eq "" or tmpEntityID eq 0>null<cfelse>#tmpEntityID#</cfif>,<cfif tmpObjectID eq "" or tmpObjectID eq 0>'#arguments.pendingRecordsQry[arguments.objectIDColumn][currentRow]#'<cfelse>null</cfif>,'#arguments.pendingRecordsQry[arguments.objectColumn][currentRow]#','#arguments.pendingRecordsQry[arguments.fieldNameColumn][currentRow]#','#arguments.pendingRecordsQry[arguments.newValueColumn][currentRow]#','#arguments.pendingRecordsQry[arguments.modDateColumn][currentRow]#'<cfif structKeyExists(arguments,"opportunityID")>,'#opportunityID#'</cfif>,<cfif tmpEntityID eq "" or tmpEntityID eq 0>'I'<cfelse>'E'</cfif>
							<cfif currentRow lt endRow>union</cfif>
						</cfloop>
					</cfquery>
				</cfif>
			</cfloop>
			
			<cfquery name="updateExistingSynchPending" dataSource="#application.siteDataSource#">
				update salesForceSynchPending set newValue = p.newValue, lastUpdatedBy=#request.relayCurrentUser.userGroupId#, lastUpdated = '#request.requestTime#'
					from salesForceSynchPending s with (noLock) inner join ##PendingRecords p on s.fieldName = p.fieldName and s.object = p.object and isNull(s.objectID,'') = isNull(p.objectID,'') and isNull(s.entityID,0) = isNull(p.entityID,0)
			</cfquery>
			
			<!--- when importing objects, we look at the synch attempt to 'suspend' it. If we've made a change and the object needs re-importing, then we want to reset the synch attempt so that the record will get picked up for synching again--->
			<cfquery name="updateErrorSynchAttempt" dataSource="#application.siteDataSource#">
				update salesForceError set synchAttempt = 0, lastUpdated = '#request.requestTime#'
					from salesForceError s with (noLock) inner join ##PendingRecords p on s.object = p.object and isNull(s.objectID,'') = isNull(p.objectID,'') and s.direction = 'I' and (isNull(s.entityID,0) = 0 or len(s.entityID)=0)
			</cfquery>
			
			<!--- CASE 426374: added the inner join trigger points so that we only insert into pending table those records that are eligible for synching
				Case 437308: NJH 2013/10/28 - changed f.name to f.flagTextID as Lenovo changed the flagName from approved to active. FlagTextID should have been used anyways to get the approval flag for a given entity
			 --->
			<cftry>
			<cfquery name="insertNewSynchPending" dataSource="#application.siteDataSource#">
				insert into salesForceSynchPending (entityID,objectID,object,fieldname,newValue,modDate,createdBy,lastUpdatedBy <cfif structKeyExists(arguments,"opportunityID")>,opportunityID</cfif>,direction)
				select distinct p.entityID,p.objectID,p.object,p.fieldname, p.newValue,p.modDate,#request.relayCurrentUser.userGroupId#,#request.relayCurrentUser.userGroupId# <cfif structKeyExists(arguments,"opportunityID")>,'#opportunityID#'</cfif>,p.direction
					from ##PendingRecords p 
						left join salesForceSynchPending s with (noLock) on s.fieldName = p.fieldName and s.object = p.object and isNull(s.objectID,'') = isNull(p.objectID,'') and isNull(s.entityID,0) = isNull(p.entityID,0) and s.direction = p.direction
						
						<cfif listFindNoCase("organisation,person",arguments.baseEntity)>
							left join (
								select ot.typeTextID, #left(arguments.baseEntity,1)#.#arguments.baseEntity#ID as entityID, crm#left(arguments.baseEntity,3)#ID as objectID,
									case 
										<cfloop collection="#triggerPoints#" item="orgType">
											<cfif triggerPoints[orgType] neq "Never">when ot.typeTextID='#orgType#'
												<cfif triggerPoints[orgType] eq "Approval"> and f.flagTextID like '%Approved' then 1<cfelse> then 1</cfif>
											</cfif>
										</cfloop>
										else 0 end as synch
									from organisation o with (noLock) inner join organisationType ot with (noLock) on o.organisationTypeID = ot.organisationTypeID
								<cfif arguments.baseEntity eq "person">
									inner join person p with (noLock) on p.organisationID = o.organisationID
								</cfif>
									inner join booleanFlagData bfd with (noLock) on bfd.entityId = #left(arguments.baseEntity,1)#.#arguments.baseEntity#ID
									inner join flag f with (noLock) on f.flagID = bfd.flagID and f.flagGroupID = #application.com.flag.getFlagGroupStructure(flagGroupId="#left(arguments.baseEntity,3)#ApprovalStatus").flagGroupId#
							) appStatus on (appStatus.entityID = p.entityID or appStatus.objectID = p.objectID)
						</cfif>
				where s.ID is null
				<!--- if synching is null or 1, then we can synch them across. Null is a new object coming across --->
				<cfif listFindNoCase("organisation,person",arguments.baseEntity)>
					and isNull(appStatus.synch,1) <> 0
				</cfif>
			</cfquery>
			
				<cfcatch>
					
					<cfquery name="getPendingObjectRecords" datasource="#application.siteDataSource#">
						select objectID,count(objectID) from ##PendingRecords
						group by objectId
					</cfquery>
					
					<cfquery name="getPendingEntityRecords" datasource="#application.siteDataSource#">
						select entityID,count(entityID) from ##PendingRecords
						group by entityID
					</cfquery>
					
					<cfmail to="nathaniel.hogeboom@relayware.com" from="errors@relayware.com" subject="Error encountered inserting pending records on #request.currentSite.domainAndRoot#" type="html" priority="urgent">
						<br>
						<b>Arguments</b><br>
						<cfdump var="#arguments#"><br>
						<b>Pending Object Records</b>
						<br>
						<cfdump var="#getPendingObjectRecords#"><br>
						<br>
						<b>Pending Entity Records</b>
						<br>
						<cfdump var="#getPendingEntityRecords#"><br>
						<br>
						<b>Error dump</b><br>
						<cfif structKeyExists(cfcatch,"sql")>SQL: <cfdump var="#cfcatch.sql#"><br></cfif>
						Message:<cfdump var="#cfcatch.message#"><br>
						Detail:<cfdump var="#cfcatch.detail#"><br>
						TagContext:<cfdump var="#cfcatch.tagContext#"><br>
						<br>
						Regards,<br>
						<br>
						RelayWare/SalesForce Synch
					</cfmail>
					
					<cfset application.com.errorHandler.recordRelayError_Warning(type="SalesForce InsertPendingRecords",Severity="error",catch=cfcatch)>
					<cfabort>
				</cfcatch>
			</cftry>
			
			<cfif structKeyExists(arguments,"opportunityID")>
			
				<cfquery name="getObjectType" dbtype="query" maxrows="1">
					select #arguments.objectColumn# as object,#arguments.modDateColumn# as modifiedDate 
					from arguments.pendingRecordsQry order by #arguments.modDateColumn# desc
				</cfquery>
				
				<!--- if we've just inserted a pending record for an opportunity product, then we want to put in a record for the opportunity, as the product won't get picked up for 
					synching (by bulk update) on its own, but it needs an opportunity record. The opportunity record will get picked up, and then it checks to see whether it has any products that need going across --->
				<cfif listFindNoCase("oppProducts,opportunityLineItem",getObjectType.object[1])>
					<cfset argPendingStruct = {object = "opportunity",modifiedDate = getObjectType.modifiedDate[1],newValue = "",field = "Opportunity Products"}>
					<cfif isNumeric(arguments.opportunityID)>
						<cfset argPendingStruct.entityID = arguments.opportunityID>
						<cfset argPendingStruct.objectID = "">
					<cfelse>
						<cfset argPendingStruct.objectID = arguments.opportunityID>
					</cfif>
					<cfset insertPendingRecord(argumentCollection=argPendingStruct)>
				</cfif>
			</cfif>
		</cfif>

	</cffunction>


	<cffunction name="loadSFMappingsXML" access="public" hint="Loads the mappings xml into memory" output="false">
		
		<cfscript>
			var filePathAndName="#application.paths.relayware#\xml\sfMappings.xml";
			var mappingXmlFile = "";
		</cfscript>
		
		<cfif not structKeyExists(request,"sfMappingXML")>
			<cfif structKeyExists(request,"sfOppTypeTextID") and fileExists("#application.paths.abovecode#\xml\sfMappings#request.sfOppTypeTextID#.xml")>
				<cfset filePathAndName="#application.paths.abovecode#\xml\sfMappings#request.sfOppTypeTextID#.xml">
			<cfelseif fileExists("#application.paths.abovecode#\xml\sfMappings.xml")>
				<cfset filePathAndName="#application.paths.abovecode#\xml\sfMappings.xml">
			</cfif>
		
			<cffile action="read" file="#filePathAndName#" variable="mappingXmlFile">
			<cfset request.sfMappingXML = xmlParse(mappingXmlFile)>
		</cfif>
		
	</cffunction>
	
		
	<cffunction name="getMappingXML" access="public" output="false" returnType="xml">
	
		<cfif structKeyExists(request,"sfMappingXML")>
			<cfreturn request.sfMappingXML>
		<cfelse>
			<cfreturn application.sfMappingXML>
		</cfif>
	</cffunction>
	
	
	<cffunction name="putSFDCObjectIntoMemory" access="public" output="false" hint="Puts object details into memory">
		<cfargument name="object" type="string" required="true" hint="The SalesForce object name">
		
		<cfscript>
			var mappingsResult = structNew();
		</cfscript>
		
		<cfif not structKeyExists(application,"sfObjects")>
			<cfset application.sfObjects = structNew()>
		</cfif>
		
		<cfif structKeyExists(application,"sfObjects") and not structKeyExists(application.sfObjects,arguments.object)>
			<cfset mappingsResult = send(object=arguments.object,method="describeSObjects")>
			<cfif mappingsResult.success>
				<cfset application.sfObjects[arguments.object] = application.com.structureFunctions.queryToStruct(query=mappingsResult.response,key="name")>
			<cfelse>
				<cfif isSimpleValue(mappingsResult.response) and reFindNoCase("INVALID_TYPE",mappingsResult.response)>
					<!--- <cfreturn false> --->
				</cfif>
			</cfif>
		</cfif>

	</cffunction>
	
	
	<cffunction name="processUpdatedAssets" access="public" hint="Processes any assets that have been updated" output="false">
		<cfargument name="updatedSince" type="date" required="true">
		
		<cfscript>
			var getUpdatedAssets = "";
			var opportunityDetailsStruct = structNew();
		</cfscript>
		
		<cfquery name="getUpdatedAssets" datasource="#application.siteDataSource#">
			select opportunityId, endDate, partnerSalesPersonID from asset a inner join 
				opportunity opp with (noLock) on a.renewalOppId = opp.opportunityID inner join
				modRegister mr with (noLock) on mr.recordID = a.assetID inner join
				modEntityDef med with (noLock) on mr.ModEntityID = med.ModEntityID
			where mr.modDate >= '#arguments.updatedSince#'
				and med.TableName = 'asset'
				and mr.Action = 'ASSM'
				and med.FieldName = 'EndDate'
		</cfquery>
		
		<cfloop query="getUpdatedAssets">
			<cfset opportunityDetailsStruct={expectedCloseDate=endDate}>
			<cfset application.com.relayEntity.updateOpportunity(opportunityID=opportuntiyID,opportunityDetails=opportunityDetailsStruct)>
			<cfset application.com.email.sendEmail(personId=partnerSalesPersonID,emailTextID="AssetRenewal")>
		</cfloop>
		
	</cffunction>


	<cffunction name="getSynchPendingRecords" access="public" returntype="query" hint="Returns records in the synch pending table for either RW or SF entities/objects" output="false">
		<cfargument name="entityType" type="string" required="false" hint="Can be a list of RW entities">
		<cfargument name="object" type="string" required="false" hint="A salesForce object">
		
		<cfscript>
			var getPendingRecords = queryNew("entityID,objectId,entity,fieldName,newValue,modDate");
			var processNumPendingRecords =  application.com.settings.getSetting("salesForce.bulkUpdate.processNumPendingRecords");
			var rwEntityType = "";
			var mappedObject = "";
			var foreignKeyInfo = "";
			var uniqueKey = "";
			var suspendedFlagID = ""; // 2014-10-02	NJH Case 442548	fixes to getSynchPendingRecords
		</cfscript>
		
		<cfif structKeyExists(arguments,"entityType") or structKeyExists(arguments,"object")>
		
			<cfif structKeyExists(arguments,"entityType") and arguments.entityType neq "">
			
				<cfset rwEntityType = arguments.entityType>	
				<cfif listSort(arguments.entityType,"text") eq "location,person">
					<cfset rwEntityType = "person">
				</cfif>
				<!--- START: 2014-10-02	NJH	 Case 442548		fixes to getSynchPendingRecords 
					suspended flagid added to check flagtextid's for all allowed entities
				--->
				<cfset suspendedFlagID = "#rwEntityType#SalesForceSynchSuspended">
				<cfif listFindNoCase("organisation,location,person,opportunity",rwEntityType)>
					<cfset suspendedFlagID = "#left(rwEntityType,3)#SalesForceSynchSuspended">
				</cfif>
				<!--- END: 2014-10-02	NJH	 Case 442548		fixes to getSynchPendingRecords --->
				<!--- Getting pending RW records - only get records that are flagged as synching --->
				<cfquery name="getPendingRecords" datasource="#application.siteDataSource#">
					if object_id('tempdb..##sfEntityRecords') is not null
					begin
					   drop table ##sfEntityRecords
					end

					select top #processNumPendingRecords# sfsp.entityID as entityID into ##sfEntityRecords
					from SalesForceSynchPending sfsp with (noLock)
					<!--- START: 2013-12-04		MS		P-EVA001 Case. 438182 When we now have 2 Process Pending Scheduled Tasks for Opps, SalesForceProcessPendingOppSynch and SalesForceProcessPendingLeadSynch. So we need to update the query to only pick up specific Opps based on their OppTypeIDs which is descided using the value set in request.sfOppTypeTextID variable --->
					<cfif arguments.ENTITYTYPE eq "opportunity">
						INNER JOIN opportunity o with (noLock) on sfsp.entityID = o.opportunityID
					</cfif>
					<!--- END  : 2013-12-04		MS		P-EVA001 Case. 438182 When we now have 2 Process Pending Scheduled Tasks for Opps, SalesForceProcessPendingOppSynch and SalesForceProcessPendingLeadSynch. So we need to update the query to only pick up specific Opps based on their OppTypeIDs which is descided using the value set in request.sfOppTypeTextID variable --->
						left join booleanFlagData bfd with (noLock) on bfd.entityID = sfsp.entityID and bfd.flagId = #application.com.flag.getFlagStructure(flagID=suspendedFlagID).flagID# <!--- 2014-10-02	NJH  Case 442548	fixes to getSynchPendingRecords--->
					where (object <cfif listLen(arguments.entityType) gt 1>in (#listQualify(arguments.entityType,"'")#)<cfelse>= '#arguments.entityType#'</cfif> <!--- <cfif arguments.entityType eq "opportunity">or (sfsp.opportunityID is not null and isNumeric(sfsp.opportunityID) = 1)</cfif> --->)
						and bfd.entityID is null
						and sfsp.entityID is not null and len(ltrim(rtrim(sfsp.entityID))) > 0
						<cfif arguments.entityType eq "oppProducts">and isNumeric(sfsp.opportunityID) = 1</cfif>
					<!--- START: 2013-12-04		MS		P-EVA001 Case. 438182 When we now have 2 Process Pending Scheduled Tasks for Opps, SalesForceProcessPendingOppSynch and SalesForceProcessPendingLeadSynch. So we need to update the query to only pick up specific Opps based on their OppTypeIDs which is descided using the value set in request.sfOppTypeTextID variable --->
					<cfif arguments.entityType eq "opportunity">
						and	o.OppTypeID <cfif structKeyExists(request,"sfOppTypeTextID") and request.sfOppTypeTextID eq "Leads">=<cfelse>!=</cfif> 2
					</cfif>
					<!--- END  : 2013-12-04		MS		P-EVA001 Case. 438182 When we now have 2 Process Pending Scheduled Tasks for Opps, SalesForceProcessPendingOppSynch and SalesForceProcessPendingLeadSynch. So we need to update the query to only pick up specific Opps based on their OppTypeIDs which is descided using the value set in request.sfOppTypeTextID variable --->
					group by sfsp.entityID,prioritise
					order by prioritise desc,min(modDate)
			
					select distinct sfsp.entityID,sfsp.objectId as entityCRMID, sfsp.object as entity,sfsp.fieldName,sfsp.newValue as newVal,sfsp.modDate, sfsp.prioritise
					from SalesForceSynchPending sfsp with (noLock)
						inner join ##sfEntityRecords s on s.entityID = sfsp.entityID			<!--- 2014-10-02 NJH	 Case 442548 reverted change from Case 440431 --->
					where object <cfif listLen(arguments.entityType) gt 1>in (#listQualify(arguments.entityType,"'")#)<cfelse>= '#arguments.entityType#'</cfif>
					order by sfsp.prioritise desc,sfsp.entityID,sfsp.modDate
				</cfquery>
			
			<cfelseif structKeyExists(arguments,"object") and arguments.object neq "">
			
				<cfset mappedObject = getMappedObject(object=arguments.object,direction="SF2RW")>
				<cfset foreignKeyInfo = getIDMappingInfo(entityType=mappedObject.entity)>
				<cfset uniqueKey = application.entityType[application.entityTypeID[mappedObject.entity]].uniqueKey>
				<!--- START: 2014-10-02	NJH	 Case 442548		fixes to getSynchPendingRecords 
					suspended flagid added to check flagtextid's for all allowed entities
				--->
				<cfset suspendedFlagID = "#mappedObject.entity#SalesForceSynchSuspended">
				<cfif listFindNoCase("organisation,location,person,opportunity",mappedObject.entity)>
					<cfset suspendedFlagID = "#left(mappedObject.entity,3)#SalesForceSynchSuspended">
				</cfif>
				<!--- END: 2014-10-02	NJH	 Case 442548		fixes to getSynchPendingRecords --->
				<cfquery name="getPendingRecords" datasource="#application.siteDataSource#">
					if object_id('tempdb..##sfObjectRecords') is not null
					begin
					   drop table ##sfObjectRecords
					end

					select top #processNumPendingRecords# sfsp.objectID as objectID into ##sfObjectRecords
					from SalesForceSynchPending sfsp with (noLock)
						left join #mappedObject.entity# e with (noLock) on e.#foreignKeyInfo.sfIDColumn# = sfsp.objectID
						left join booleanFlagData bfd with (noLock) on e.#uniqueKey# = bfd.entityID and bfd.flagID = #application.com.flag.getFlagStructure(flagID=suspendedFlagID).flagID# <!--- 2014-10-02	NJH Case 442548	fixes to getSynchPendingRecords--->
						left join salesForceError sfe with (noLock) on sfe.objectID = sfsp.objectID
					where (sfsp.object = '#arguments.object#')
						and ((len(ltrim(rtrim(sfsp.entityID))) = 0 or sfsp.entityID is null or sfsp.entityID=0) <cfif arguments.object eq "opportunityLineItem"> or (isNumeric(sfsp.opportunityID) = 0 and sfsp.opportunityID is not null)</cfif>)
						and bfd.entityID is null
						and (len(ltrim(rtrim(sfsp.objectID))) > 0 or sfsp.objectID is not null)
						and isNull(sfe.synchAttempt,0) < #application.com.settings.getSetting("salesForce.maxSynchAttempts")#
					group by sfsp.objectID,prioritise
					order by prioritise desc,min(modDate)
					
					select distinct sfsp.objectID as #arguments.object#ID,sfsp.newValue,sfsp.fieldName as field,sfsp.modDate as lastModifiedDate,sfsp.prioritise,sfsp.entityID,sfsp.objectID as ID, '#arguments.object#' as object
					from SalesForceSynchPending sfsp with (noLock)
						inner join ##sfObjectRecords s on s.objectID = sfsp.objectID			<!--- 2014-10-03 AXA	copied from NJH Case 442548 changes. reverted change from Case 440431 --->		
					where object = '#arguments.object#'
						and ((len(ltrim(rtrim(entityID))) = 0 or entityID is null) <cfif arguments.object eq "opportunityLineItem"> or (isNumeric(sfsp.opportunityID) = 0 and sfsp.opportunityID is not null)</cfif>)
					order by sfsp.prioritise desc,sfsp.objectID,sfsp.modDate
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn getPendingRecords>
	</cffunction>
	
	
	<cffunction name="setOppPartner" access="private" hint="Sets the partner for an incoming opportunity" output="false">
		<cfargument name="entityStruct" type="struct" required="true" hint="Beware! This will modify the original structure that is passed in. Passed by reference!">
		
		<cfscript>
			var argEntityStruct = arguments.entityStruct;
			var primaryContactData = "";
			var orgHQ = "";
			var isPersonAtOrganisation = "";
			var getLocationCountry = "";
		</cfscript>
		
		<!--- The partnerLocationID field contains an organisationID at this point. We need to grab the locationID. We do this by:
			1. If there is no reseller contact, we grab the location HQ at the org and set it to the primary contact
			2. If there is a reseller contact, we see if they are at the same org as the partnerLocationId.
				a)If so, they are the contact and their location is set to the reseller location
				b)If not, the primay contact is set to be the person and their location is used.
		--->
		<cfif structKeyExists(argEntityStruct.opportunity,"partnerLocationID")>
			<cfif argEntityStruct.opportunity.partnerLocationID neq "" and argEntityStruct.opportunity.partnerLocationID neq 0>
				
				<cfset primaryContactData = application.com.flag.getFlagData(flagid="keyContactsPrimary",entityID=argEntityStruct.opportunity.partnerLocationID,additionalColumns="locationId")>
				
				<!--- a primary contact may always be set when created on RW, but is not the case when an account/contact has come from SF... in this case,we get the last person updated?? --->
				<cfif primaryContactData.recordCount eq 0>
					<cfquery name="primaryContactData" datasource="#application.siteDataSource#">
						select top 1 personID as data, locationID from person where organisationID=#argEntityStruct.opportunity.partnerLocationID#
						order by lastUpdated desc
					</cfquery>
				</cfif>

				<cfif not (structKeyExists(argEntityStruct.opportunity,"partnerSalesPersonID") and argEntityStruct.opportunity.partnerSalesPersonID neq "" and argEntityStruct.opportunity.partnerSalesPersonID neq 0)>
					<cfset orgHQ = application.com.flag.getFlagData(flagID="HQ",entityID=argEntityStruct.opportunity.partnerLocationID)>
					<cfif orgHQ.data neq "">
						<cfset argEntityStruct.opportunity.partnerLocationID = orgHQ.data>
					<cfelse>
						<cfset argEntityStruct.opportunity.partnerLocationID = primaryContactData.locationID>
					</cfif>
					<cfset argEntityStruct.opportunity.partnerSalesPersonID = primaryContactData.data>
				<cfelse>	
					<!--- is the contact at the same organisation? --->
					<cfquery name="isPersonAtOrganisation" datasource="#application.siteDataSource#">
						select locationId from person with (noLock)
						where personID = #argEntityStruct.opportunity.partnerSalesPersonID#
							and organisationID = #argEntityStruct.opportunity.partnerLocationID#
					</cfquery>
				
					<!--- if the person is not at the partner's organisation, then we get the location by getting the one flagged as the organisation HQ --->
					<cfif isPersonAtOrganisation.recordCount>
						<cfset argEntityStruct.opportunity.partnerLocationID = isPersonAtOrganisation.locationID>
					<cfelse>
						<cfset argEntityStruct.opportunity.partnerLocationID = primaryContactData.locationID>
						<cfset argEntityStruct.opportunity.partnerSalesPersonID = primaryContactData.data>
					</cfif>
				</cfif>
				
				<!--- set the country of the opportunity to be that of the partner's country --->
				<cfif not structKeyExists(argEntityStruct.opportunity,"countryID") or argEntityStruct.opportunity.countryID eq 0 or argEntityStruct.opportunity.countryID eq "">
					<cfquery name="getLocationCountry" datasource="#application.siteDataSource#">
						select countryId from location with (noLock) where locationID = #argEntityStruct.opportunity.partnerLocationID#
					</cfquery>
					<cfset argEntityStruct.opportunity.countryID = getLocationCountry.countryID[1]>
				</cfif>
				
			<!--- if it's an empty string, then we just set it to 0, as it should be numeric --->
			<cfelse>
				<cfset argEntityStruct.opportunity.partnerLocationID = 0>
			</cfif>
		</cfif>
		
	</cffunction>


	<cffunction name="convertDateToSalesForceFormat" access="public" returntype="String" output="false">
		<cfargument name="dateToConvert" required="true" type="date">
		<cfargument name="inUTC" default="true" type="boolean">
		
		<cfreturn "#dateFormat(arguments.dateToConvert,'yyyy-MM-dd')#T#timeFormat(arguments.dateToConvert,'HH:mm:ss')##IIF(arguments.inUTC,DE('Z'),DE(''))#">
	</cffunction>
	
	
	<cffunction name="getProductFromPricebookEntry" access="public" returnType="numeric" hint="Used in sfmappings document to get the product for a given pricebookEntry" validValues="true" output="false">
		<cfargument name="pricebookEntryID" type="numeric" required="true">
		<cfargument name="direction" type="string" default="SF2RW">
		
		<cfset var productID=0>
		<cfset var getProductID = "">
		
		<cfif arguments.direction eq "SF2RW">
			<cfquery name="getProductID" datasource="#application.siteDataSource#">
				select productID from pricebookEntry where pricebookEntryID=#arguments.pricebookEntryID#
			</cfquery>
			
			<cfif getProductID.recordCount>
				<cfset productId = getProductID.productID>
			</cfif>
		</cfif>

		<cfreturn productID>
	</cffunction>
	
	
	<cffunction name="logDebugInformation" access="public" hint="Should be called by most functions to collect debug information" output="false">
		<cfargument name="entityType" type="string" required="false">
		<cfargument name="method" type="string" required="false">
		<cfargument name="entityID" type="string" required="false">
		<cfargument name="sfModificationsQry" type="query" required="false">
		<cfargument name="rwModificationsQry" type="query" required="false">
		<cfargument name="additionalDetails" type="Any" default="">
		<cfargument name="direction" type="string" required="false">
		
		<cfscript>
			var tempStruct = structNew();
		</cfscript>
		
		<cfif structKeyExists(url,"debug")>
			<cfif not structKeyExists(request,"salesForceDebug")>
				<cfset request.salesForceDebug = structNew()>
				<cfset request.salesForceDebug.history = structNew()>
				<cfset request.salesForceDebug.timeStarted = now()>
			</cfif>
			
			<!--- the query of SalesForce modifications that we are processing --->
			<cfif structKeyExists(arguments,"sfModificationsQry")>
				<cfset request.salesForceDebug.sfModificationsQry = arguments.sfModificationsQry>
			</cfif>
			
			<!--- the query of Relayware modifications that we are processing --->
			<cfif structKeyExists(arguments,"rwModificationsQry")>
				<cfset request.salesForceDebug.rwModificationsQry = arguments.rwModificationsQry>
			</cfif>
			
			<cfif structKeyExists(arguments,"direction")>
				<cfset request.salesForceDebug.direction = arguments.direction>
			</cfif>
			
			<!--- if only an entitytype has been passed through, then this will indicate what the current entity it is that
				we are processing.. For example, we could be exporting an opportunity, but to export the opportunity, we need
				to export an organisation and person. These will come through but we will know that they are not the primary object but
				rather related data.
			--->
			<cfif structKeyExists(arguments,"entityType") and not structKeyExists(arguments,"entityID")>
				<cfset request.salesForceDebug.baseEntity = arguments.entityType>
			</cfif>
			
			<cfif structKeyExists(arguments,"entityType") and structKeyExists(arguments,"entityID")>
				<!--- if we're dealing with the primary object/entity --->
				<cfif arguments.entityType eq request.salesForceDebug.baseEntity>
					<cfif not structKeyExists(request.salesForceDebug,arguments.entityType)>
						<cfset request.salesForceDebug[arguments.entityType] = structNew()>
						<cfset request.salesForceDebug[arguments.entityType].ID = arguments.entityID>
						<cfset request.salesForceDebug.debugURL = "//#request.currentSite.domainAndRoot#/salesForce/synchObject.cfm?ID=#arguments.entityID#&#IIF(isNumeric(arguments.entityID),DE('entityType'),DE('object'))#=#arguments.entityType#">
					</cfif>
					<cfif structKeyExists(arguments,"method")>
						<cfset request.salesForceDebug[arguments.entityType][arguments.method&"-"&timeFormat(now(),"hh:mm:ss:ll")] = arguments.additionalDetails>
					</cfif>

				<cfelse>
				
					<!--- this is a structure of related data..  --->
					<cfif not structKeyExists(request.salesForceDebug[request.salesForceDebug.baseEntity],arguments.entityType)>
						<cfset request.salesForceDebug[request.salesForceDebug.baseEntity][arguments.entityType] = structNew()>
						<cfset request.salesForceDebug[request.salesForceDebug.baseEntity][arguments.entityType].ID = arguments.entityID>
					</cfif>
					<cfif structKeyExists(arguments,"method")>
						<cfset request.salesForceDebug[request.salesForceDebug.baseEntity][arguments.entityType][arguments.method&"-"&timeFormat(now(),"hh:mm:ss:ll")] = arguments.additionalDetails>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		
	</cffunction>
	
	
	<cffunction name="clearDebugInformation" type="private" hint="Clears the debug information if an export/import for a given entity/object has not fallen over" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="string" required="true">
		
		<cfscript>
			var method = "";
		</cfscript>

		<cfif structKeyExists(request,"salesForceDebug") and structKeyExists(request.salesForceDebug,arguments.entityType) and request.salesForceDebug[arguments.entityType].ID eq arguments.entityID>
		
			<cfset structDelete(request.salesForceDebug,arguments.entityType)>
			
			<cfif isNumeric(arguments.entityID)>
				<cfset method = "export">
			<cfelse>
				<cfset method = "import">
			</cfif>
			
			<cfif not structKeyExists(request.salesForceDebug.history,method)>
				<cfset request.salesForceDebug.history[method] = structNew()>
			</cfif>
			<cfif not structKeyExists(request.salesForceDebug.history[method],request.salesForceDebug.baseEntity)>
				<cfset request.salesForceDebug.history[method][request.salesForceDebug.baseEntity] = "">
			</cfif>
			
			<!--- keep a list of processed entities.. Note: these ones may not have successfully gone across, but we have successfully processed them --->
			<cfset request.salesForceDebug.history[method][request.salesForceDebug.baseEntity]= listAppend(request.salesForceDebug.history[method][request.salesForceDebug.baseEntity],arguments.entityID&"(#timeFormat(now(),'hh:mm:ss:ll')#)")>
		</cfif>

	</cffunction>
	
	
	<cffunction name="getSalesForceUser" type="public" hint="Gets the SalesForce User" returntype="query" output="false">
		
		<cfscript>
			var getSalesForceUserID = "";
		</cfscript>
		
		<cfquery name="getSalesForceUserID" datasource="#application.siteDataSource#">
			select p.personID, ug.userGroupID from person p with (noLock)
				inner join userGroup ug with (noLock) on p.personID = ug.personID
			where p.firstname='SalesForce' and p.lastname='API'
				and ug.name = 'SalesForce API'
		</cfquery>
		
		<cfreturn getSalesForceUserID>
	</cffunction>
	
	
	<cffunction name="setSalesForceUserAsCurrentUser" type="public" hint="Sets the current user as the salesForce API user" output="false">
		
		<cfscript>
			var getSalesForceUserID = getSalesForceUser();
		</cfscript>
		
		<cfif getSalesForceUserID.recordCount eq 0>
			<cfthrow message = "Could not find SalesForce user with firstname of 'SalesForce' and a lastname of 'API' in the 'SalesForce API' usergroup.">
		</cfif>
			
		<cfset request.relayCurrentUser.personID = getSalesForceUserID.personID>
		<cfset request.relayCurrentUser.userGroupID = getSalesForceUserID.userGroupID>
		
	</cffunction>
	
	
	<cffunction name="getHistoryObject" access="public" returnType="string" output="false">
		<cfargument name="object" type="string" required="true">
		
		<cfset var historyObject = arguments.object&"History">
		
		<cfif arguments.object eq "opportunity">
			<cfset historyObject = "opportunityFieldHistory">
		<cfelseif not listFindNoCase("account,contact",arguments.object)>
			<cfset historyObject = arguments.object>
		</cfif>
		
		<cfreturn historyObject>
	</cffunction>
	
	
	<cffunction name="prioritisePendingRecord" access="public" hint="Prioritises a pending record" output="false">
		<cfargument name="recordID" type="numeric" require="true">
		
		<cfset var prioritisePendingRecordQry = "">
		
		<cfquery name="prioritisePendingRecordQry" datasource="#application.siteDataSource#">
			update salesForceSynchPending set prioritise=1,lastUpdated=getDate(),lastUpdatedBy=#request.relayCurrentUser.userGroupID# 
			where isNull(prioritise,0) != 1 and ID=#arguments.recordID#
		</cfquery>
	</cffunction>
	
	
	<!--- a function to determine whether an entity/object can be synched or not based on the trigger points --->
	<cffunction name="canEntityBeSynched" access="private" returnType="struct" output="false">
		<cfargument name="recordType" type="string" required="true">
		<cfargument name="recordID" type="string" required="true">
		
		<cfset var result = {canSynch=true,message=""}>
		<cfset var baseEntity = "">
		<cfset var canSynch = true>
		<cfset var getOrgType = "">
		<cfset var triggerPoint = "">
		
		<cfif listFindNoCase("organisation,person",arguments.recordType)>
			<cfset baseEntity = arguments.recordType>
		<cfelseif listFindNoCase("organisation,person",getMappedObject(object=arguments.recordType,direction="SF2RW").entity)>
			<cfset baseEntity = getMappedObject(object=arguments.recordType,direction="SF2RW").entity>
		</cfif>
		
		<cfif listFindNoCase("organisation,person",baseEntity)>
			<cfquery name="getOrgType" datasource="#application.siteDataSource#">
				select ot.typeTextID, bfd.flagID, <cfif baseEntity eq "person">p.firstname + ' '+p.lastname<cfelse>o.organisationName</cfif> as name
				from organisation o with (noLock) inner join organisationType ot with (noLock)
					on o.organisationTypeID = ot.organisationTypeID
					<cfif baseEntity eq "person">inner join person p with (noLock) on p.organisationID = o.organisationID</cfif>
					left join 
						(booleanFlagData bfd with (noLock) inner join flag f with (noLock)on bfd.flagID = f.flagID and f.flagTextID like '%#left(baseEntity,3)#Approved'
							inner join flagGroup fg on fg.flagGroupID = f.flagGroupID and fg.entityTypeID = #application.entityTypeID[baseEntity]#)
						on bfd.entityID = #baseEntity#ID
				where <cfif isNumeric(arguments.recordID)>#baseEntity#ID=#arguments.recordID#<cfelse>crm#left(baseEntity,3)#ID='#arguments.recordID#'</cfif>
			</cfquery>
			
			<cfif getOrgType.recordCount>
				<cfset triggerPoint = application.com.settings.getSetting("salesForce.synchTriggerPoints.plo.#getOrgType.typeTextID#")>
				
				<cfif not ((triggerPoint eq "Approval" and getOrgType.flagID neq "") or (triggerPoint eq "Creation"))>
					<cfset canSynch = false>
					<cfif triggerPoint eq "Approval" and getOrgType.flagID eq "">
						<cfset result.message = "#getOrgType.name# is not approved.">
					<cfelseif triggerPoint eq "Never">
						<cfset result.message = "#getOrgType.name# is of type '#getOrgType.typeTextID#' and is set to never synch.">
					</cfif>
				</cfif>
			</cfif>
		</cfif>

		<cfset result.canSynch = canSynch>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="putMappedObjectsInTable" access="public" returnType="struct" output="false">
		<cfargument name="entityType" type="string" default="">
		
		<cfset var objectList = "">
		<cfset var entityList = "">
		
		<cflock name="SalesforceXMLSearch" timeout="3">
			<cfset var IdMappingArray = xmlSearch(getMappingXML(),"//mappings/mapping[translate(@objectColumn,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='id']")>
		</cflock>
		
		<cfset var xmlNode = "">
		<cfset var result = {entityList="",objectList=""}>
		<cfset var rwEntity = "">
		<cfset var sfObject = "">
		
		<cfloop array="#IdMappingArray#" index="xmlNode">
			<cfset sfObject = xmlNode.xmlAttributes.object>
			<cfset rwEntity = xmlNode.xmlAttributes.table>
			
			<cfif (entityType neq "" and (arguments.entityType eq rwEntity or arguments.entityType eq sfObject)) or arguments.entityType eq "">
				<cfset objectList = listAppend(objectList,sfObject)>
				<cfset entityList = listAppend(entityList,rwEntity)>
				<cfif not structKeyExists(result,sfObject)>
					<cfset result[sfObject] = "">
				</cfif>
				<cfset result[sfObject] = listAppend(result[sfObject],rwEntity)>
				
				<cfif not structKeyExists(result,rwEntity)>
					<cfset result[rwEntity] = "">
				</cfif>
				<cfset result[rwEntity] = listAppend(result[rwEntity],sfObject)>
			</cfif>
		</cfloop>
		
		<cfset var count = 1>			<!--- 2014-04-29 PPB Case 439537 added var --->
		
		<cfquery name="createTempTable" datasource="#application.siteDataSource#">
			if object_id('tempdb..##sfEntityMapping') is not null
			begin
			   drop table ##sfEntityMapping
			end
			
			select * into ##sfEntityMapping from (
			<cfloop list="#entityList#" index="entity">select '#entity#' as entityName, #application.entityTypeID[entity]# as entityTypeID,'#ucase(left(application.com.salesForce.getMappedObject(object=entity).entity,1))##lcase(right(application.com.salesForce.getMappedObject(object=entity).entity,len(application.com.salesForce.getMappedObject(object=entity).entity)-1))#' as object
				<cfif count neq listLen(entityList)>union</cfif>
				<cfset count++>
			</cfloop>
			) base 
		</cfquery>
		
		<cfset result.entityList=entityList>
		<cfset result.objectList = objectList>
		
		<cfreturn result>
		
	</cffunction>
	
	
	<cffunction name="getEntitySynchingStatus" access="public" returnType="struct" output="false">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityID" type="numeric" required="true">
		
		<cfset var statusList="Synched_Import,Synched_Export,SynchSuspended,Synching">
		<cfset var flagTextPrefix = arguments.entityType>
		<cfset var flagTextID = "">
		<cfset var flagStruct = {}>
		<cfset var result = {status="",statusImage=""}>
		<cfset var imgStruct = {Synched_Export="green",Synched_Import="green",SynchSuspended="red",Synching="orange"}>
		<cfset var synchStatus = "">
		<cfset var doesErrorRecordExist = "">
		<cfset var statusFound = false>
		<cfset var getSynchingDirection = "">
		<cfset var direction = "">
		<cfset var getMappedInfo = {}>
		
		<cfif arguments.entityID neq 0>
			<cfif listFindNoCase("organisation,location,person,opportunity",arguments.entityType)>
				<cfset flagTextPrefix = left(arguments.entityType,3)>
			</cfif>
			
			<cfloop list="#statusList#" index="synchStatus">
				<cfset flagTextID = flagTextPrefix&"Salesforce"&synchStatus>
				<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagTextID)>
				<cfif flagStruct.isOK>
					<cfset flagSet = application.com.flag.getFlagData(entityID=arguments.entityID,flagID=flagStruct.flagID)>
					<cfif flagSet.recordCount>
						<cfset statusFound = true>
						<cfbreak>
					</cfif>
				</cfif>
			</cfloop>
			
			<cfif listFindNoCase("Synching,SynchSuspended",synchStatus)>
				<cfset getMappedInfo = application.com.salesForce.getIDMappingInfo(entityType=arguments.entityType)>
				
				<cfif getMappedInfo.table eq arguments.entityType>
					<cfquery name="doesErrorRecordExist" datasource="#application.siteDataSource#">
						select top 1 message,case when direction ='I' then 'Import' else 'Export' end as direction from salesForceError s with (noLock)
							inner join #arguments.entityType# e with (noLock) on (s.entityID = e.#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# and s.object=<cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar">) or s.objectId=#getMappedInfo.sfIDColumn#
						where e.#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer">
						order by s.lastUpdated desc
					</cfquery>
					
					<cfif doesErrorRecordExist.recordCount>
						<cfset synchStatus = "SynchSuspended">
						<cfset direction=doesErrorRecordExist.direction[1]>
					<cfelse>
						<cfquery name="getSynchingDirection" datasource="#application.siteDataSource#">
							select top 1 case when direction ='I' then 'Import' else 'Export' end as direction from salesForceSynchPending s with (noLock)
								inner join #arguments.entityType# e with (noLock) on (s.entityID = e.#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# and s.object=<cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar">) or s.objectId=#getMappedInfo.sfIDColumn#
							where e.#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer">
							order by s.lastUpdated desc
						</cfquery>
						
						<cfif doesErrorRecordExist.recordCount>
							<cfset direction=getSynchingDirection.direction[1]>
						</cfif>
					</cfif>
				</cfif>
			<cfelseif listFindNoCase("Synched_Export,Synched_Import",synchStatus)>
				<cfset direction = listLast(synchStatus,"_")>
			</cfif>
			
			<cfif statusFound>
				<cfset result.status = flagStruct.name& " ("& direction & " " & dateFormat(flagSet.lastUpdated,"dd-mmm-yyyy") & "  " & timeFormat(flagSet.lastUpdated,"HH:mm:ss") &")">
				<cfif synchStatus eq "SynchSuspended" and doesErrorRecordExist.recordCount>
					<cfset result.status = result.status & chr(13)&chr(10)& doesErrorRecordExist.message[1]>
				</cfif>
				<cfset result.statusImage = "/images/misc/#imgStruct[synchStatus]#dot.gif">
			</cfif>
		</cfif>
		
		<cfreturn result>
	</cffunction>
	
	<cffunction name="getSalesForceErrorRecords" access="public" returnType="query" output="false">
		<cfargument name="entityType" type="string" default="">
		<cfargument name="fromDate" default="">
		<cfargument name="toDate" default="">
		<cfargument name="synchingStatus" type="string" default="">
		<cfargument name="direction" type="string" default="">
		<cfargument name="sortOrder" type="string" default="last_Updated desc">
		<cfargument name="relatedRecordID" type="string" required="false">
		<cfargument name="relatedObject" type="string" default="opportunity">
		
		<cfset var statusList="SynchSuspended,Synching">
		<cfset var suspendedFlags = "0">
		<cfset var synchingFlags = "0">
		<cfset var objectEntityStruct = application.com.salesForce.putMappedObjectsInTable(entityType=arguments.entityType)>
		<cfset var entity = "">
		<cfset var getSalesForceErrors = "">
		<cfset var nameExpression = "">
		<cfset var primaryObjectIDColumn = "entityID">
		
		<cfloop list="#objectEntityStruct.entityList#" index="entity">
			<cfset flagTextPrefix = entity>
			<cfif listFindNoCase("organisation,location,person,opportunity",entity)>
				<cfset flagTextPrefix = left(entity,3)>
			</cfif>
	
			<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynchSuspended").isOK>
				<cfset suspendedFlags = listAppend(suspendedFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynchSuspended").flagID)>
			</cfif>
			<cfif application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").isOK>
				<cfset synchingFlags = listAppend(synchingFlags,application.com.flag.getFlagStructure(flagID=flagTextPrefix&"SalesforceSynching").flagID)>
			</cfif>
		</cfloop>
		
		<!--- we're wanting to get records related to a given object. at the moment, it's records related to an opportunity --->
		<cfif structKeyExists(arguments,"relatedRecordID")>
			<cfif not isNumeric(arguments.relatedRecordId)>
				<cfset primaryObjectIDColumn = "objectID">
			</cfif>
		</cfif>

		<!--- 2013-07-18	YMA	Case 436222 The delete function will not run when no object is passed into rowIdentity so pass in unknown when no object recorded. --->
		<cfquery name="getSalesForceErrors" datasource="#application.siteDataSource#">
			select distinct object,message,salesforceID,last_Updated, attempts,rowIdentity,name,viewDetails,relaywareID,
				case when isNull(hasRelatedRecords,0) > 0 then '<img src="/images/icons/bullet_arrow_down.png">' else '' end as Related_Records,
				'View' as View_Details,
				current_status,
				case when direction='Import' then salesForceID else relaywareID end as functionObjectId,errorID
			from (
				select sfe.object as object,case when sfe.ObjectID = '0' then null else sfe.objectID end as salesForceID,
					sfe.lastUpdated as last_updated,synchAttempt as attempts,
					case 
						--when len(sfe.opportunityID) > 0 and direction='E' then 'Opportunity|'+cast(sfe.opportunityID as varchar)
						when isNull(sfe.object,'') != '' then sfe.object+'|'+cast(isNull(sfe.entityID,'') as varchar)
						else 'Unknown|'+cast(isNull(sfe.entityID,'') as varchar)
					end + '|'+cast(errorID as varchar) as rowIdentity,
					errorID,
					case
						when len(sfe.name) > 0 then sfe.name
						when v.name is not null then v.name else v2.name
					end as Name,
					case 
						when sfe.entityID <> 0 and sfe.object in ('person','organisation','location','opportunity','product','pricebook') then 'View' else ''
					end
					as ViewDetails,
					message,
					case when isNull(sfe.entityID,0) = 0 and direction = 'I' then null else convert(varchar,sfe.entityID) end as relaywareID,
					case 
						when direction = 'I' then 'Import' else 'Export' 
					end as direction,
					case
						when bfd.flagID in (#synchingFlags#) then 'Synching'
						when bfd.flagID in (#suspendedFlags#) then 'Suspended'
						when sfe.errorID is not null and sfe.synchAttempt >=  <cf_queryparam value="#application.com.settings.getSetting("salesForce.maxSynchAttempts")#" CFSQLTYPE="CF_SQL_Integer" >  then 'Suspended'
						else 'Synching' 
					end as current_Status,
					<cfif not structKeyExists(arguments,"relatedRecordID")>(select count(1) from salesForceRelatedError where relatedObject=sfe.object and relatedObjectId = case when sfe.direction='I' then sfe.objectId else cast(sfe.entityId as varchar) end)<cfelse>0</cfif> as hasRelatedRecords
				from salesForceError sfe with (noLock)
					<cfif structKeyExists(arguments,"relatedRecordID")>
						inner join salesForceRelatedError sfer on sfer.object = sfe.object and sfer.objectId = sfe.#primaryObjectIDColumn# and sfer.relatedObjectId = <cf_queryparam value="#arguments.relatedRecordID#" cfsqltype="cf_sql_varchar">  and sfer.relatedObject = <cf_queryparam value="#arguments.relatedObject#" cfsqltype="cf_sql_varchar">
					</cfif>
					inner join ##sfEntityMapping sm on (sm.entityName = <cfif not structKeyExists(arguments,"relatedRecordID")>sfe.object<cfelse>sfer.relatedObject</cfif> and sfe.direction = 'E') or (sm.object = <cfif not structKeyExists(arguments,"relatedRecordID")>sfe.object<cfelse>sfer.relatedObject</cfif> and sfe.direction = 'I')
					left join vEntityName v on v.entityTypeID = sm.entityTypeID and cast(v.entityID as varchar) = <cfif not structKeyExists(arguments,"relatedRecordID")>sfe.entityID<cfelse>sfer.objectID</cfif>
					left join vEntityName v2 on v.entityTypeID = sm.entityTypeID and v2.crmID = sfe.objectID
					left join 
						(vflagdef f inner join booleanflagdata bfd with (noLock) on f.flagid = bfd.flagid and f.flagID in (#suspendedFlags#,#synchingFlags#))
						on <cfif not structKeyExists(arguments,"relatedRecordID")>f.entityTable = sfe.object and sfe.entityID=bfd.entityID<cfelse>f.entityTable = sfer.relatedObject and sfe.opportunityID= cast(bfd.entityID as varchar)</cfif> 
					
				where 1=1
					<cfif isDate(arguments.fromDate)>and sfe.lastUpdated >= <cf_queryparam value="#arguments.fromDate#" cfsqltype="cf_sql_timestamp"></cfif>
					<cfif isDate(arguments.toDate)>and sfe.lastUpdated <= <cf_queryparam value="#arguments.toDate#" cfsqltype="cf_sql_timestamp"></cfif>
					<cfif arguments.direction neq "">and Direction = <cf_queryparam value="#left(arguments.Direction,1)#" cfsqltype="cf_sql_varchar"></cfif>
					<cfif arguments.entityType neq "" and not structKeyExists(arguments,"relatedRecordID")>and sfe.object = <cf_queryparam value="#arguments.entityType#" cfsqltype="cf_sql_varchar"></cfif>
			) base where 1=1
			
			<cfif arguments.synchingStatus neq "">and Synching_Status = <cf_queryparam value="#arguments.synchingStatus#" cfsqltype="cf_sql_varchar" list="true"></cfif> 
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			order by #arguments.sortOrder#
		</cfquery>
		
		<cfreturn getSalesForceErrors>
		
	</cffunction>
</cfcomponent>