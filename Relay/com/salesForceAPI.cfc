<!--- �Relayware. All Rights Reserved 2014 --->

<!---
File name:		salesForce.cfc
Author:
Date started:

Description:	SalesForce functions

Amendment History:

Date 		Initials 	What was changed

2014-10-13	AHL		Case 442038 Opportunity not sync. Apostrophe SOQL Escape Characters
2015-05-27  DXC 444699 added check for and call to function override_assignmentRuleHeaderUseDefaultRule()
2015-11-06  DAN Case 445281/444699 - no code changes, just marking the line for release which is missing
2015-12-09  DAN Case 445281 - pass the arguments to override_assignmentRuleHeaderUseDefaultRule()
2016-01-13  DAN Case 446364 - add EmailHeader to the SOAP headers so emails can be triggered from SalesForce

Possible enhancements:


 --->

<cfcomponent name="SalesForceAPI">

	<cffunction name="createSFDCSession" access="private" hint="Starts a new SFDC session" output="false">

		<cfset var loginAttempt = structNew()>
		<cfset var userStruct = application.com.settings.getSetting("salesForce.apiUser")>

		<cfif userStruct.username eq "" or userStruct.password eq "">
			<cfthrow message="FAIL: SFDC Username and Password not set.">
		</cfif>

		<cfset loginAttempt = send(object="", method="login", username=userStruct.username,password=userStruct.password)>

		<cfif loginAttempt.Success>
			<cfset application.SFDCAPI.serverUrl = loginAttempt.response.serverUrl[1]>
			<cfset application.SFDCAPI.sessionID = loginAttempt.response.sessionID[1]>
			<cfset application.SFDCAPI.userID = loginAttempt.response.userID[1]>
		<cfelse>
			<cfthrow message="Unable to login with SalesForce API" detail="#loginAttempt.response#">
		</cfif>
	</cffunction>

	<!--- ********************************************************************************************
	this is the master function that calls all of the methods to build a SOAP request and handle the SOAP response
	 ******************************************************************************************** --->
	<cffunction name="Send" access="public" output="false" returntype="Struct" hint="Runs a SalesForce method">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object on which the operation is run">
		<cfargument name="method" type="String" required="true" hint="The SalesForce method to call">
		<cfargument name="objectID" type="String" required="false" hint="The Id of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="entityID" type="numeric" required="false" hint="The related entityID of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="name" type="string" required="false" hint="The name of the object. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="opportunityID" type="string" required="false" default="" hint="The associated opportunityID. This is used for logging purposes"> <!--- used for error logging purposes --->
		<cfargument name="role" type="string" required="false" default="" hint="The role of the object. This is used for logging purposes"> <!--- used for error logging purposes --->		<!--- 2014-06-05 PPB Case 440430 add default to avoid an [undefined struct element] in errorStruct which prevented a logSFError --->
		<cfargument name="ignoreBulkProtection" type="boolean" default="false"> <!--- for pricebook import, we want to ignore the bulk protection put in place as we want all the records --->
		<cfargument name="throwErrorOn500Response" type="boolean" default="false"> <!--- if we get a 500 from salesforce, throw an error or continue processing. THis is only used when we do an initial query --->  <!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->

		<cfscript>
			var sendResult="";
			var startSoap="";
			var endSoap="";
			var sendSOAP="";
			var methodCall="";
			var urlToCall = "";
			var loginUrl = "https://login.salesforce.com/services/Soap/u/22.0";
			var result={success=false,response="",errorCode=""};
			var errorStruct = structNew();
			var oldSessionID = "";
			var sendResultResponse = "";
			var sendResponseArray = arrayNew(1);
			var responseFault = "";
			var parsedResponse = "";
			var queryMoreArgs = structNew();
			var unionQueryResult = "";
			var queryMoreResult = structNew();
			var logStruct = structNew();
			var deleteErrorArgs = structNew();
			var whereClauseArray = "";
			var whereClause = "";
			var assignmentRuleHeaderUseDefaultRule = 'true'; // START: 2014-11-19        MS  P-WEB002 Use a variable to set the boolean value sent for attribute useDefaultRule for the AssignmentRuleHeader in the SOAP
			// START: 13/01/2016 Case 446364
			var triggerAutoResponseEmail = application.com.settings.getSetting("connector.salesforce.defaults.emailHeader.triggerAutoResponseEmail");
			var triggerOtherEmail = application.com.settings.getSetting("connector.salesforce.defaults.emailHeader.triggerOtherEmail");
			var triggerUserEmail = application.com.settings.getSetting("connector.salesforce.defaults.emailHeader.triggerUserEmail");
			// END: 13/01/2016 Case 446364
			
			arguments.direction = "E";
			if (listFindNoCase("query,queryMore,queryAll,getUpdated,getDeleted,retrieve",arguments.method)) {
				arguments.direction = "I";
			}
			structAppend(errorStruct,arguments); //pass in the arguments as part of the error struct
		</cfscript>

		<cfif application.testSite eq 1><!---sandbox access--->
			<cfset loginURL = "https://test.salesforce.com/services/Soap/u/22.0">
		</cfif>

        <!--- START: 2014-11-19     MS  P-WEB002 Use a variable to set the boolean value sent for attribute useDefaultRule for the AssignmentRuleHeader in the SOAP --->
        <cfif structKeyExists(request,"sfOppTypeTextID") and request.sfOppTypeTextID neq "">
            <cfset assignmentRuleHeaderUseDefaultRule = 'false'>
        </cfif>
        <!--- END  : 2014-11-19     MS  P-WEB002 Use a variable to set the boolean value sent for attribute useDefaultRule for the AssignmentRuleHeader in the SOAP --->

        <!--- start DXC 444699 - if the function application.com.salesforce.override_assignmentRuleHeaderUseDefaultRule exists, use it to override AssignmentRuleHeader without further core changes--->
        <cfif structKeyExists(application.com.salesforce,"override_assignmentRuleHeaderUseDefaultRule")>
            <!--- some clients want to override assignmentRuleHeaderUseDefaultRule. create a function in custom salesforce.cfc for this. it should return true/false/default --->
            <cfset oARHUDR=application.com.salesforce.override_assignmentRuleHeaderUseDefaultRule(argumentcollection="#arguments#")> <!--- 2015-12-09  DAN Case 445281 --->
            <cfif oARHUDR neq "default"><!--- note: if this function returns default then it won't override --->
                <cfset assignmentRuleHeaderUseDefaultRule = oARHUDR>
            </cfif>
        </cfif>
        <!--- end DXC 444699 --->

		<cfif arguments.method eq "login">
			<cfset urlToCall = loginUrl>
		<cfelse>
			<!--- check if session exists. Create one if not --->
			<cfif not structKeyExists(application,"SFDCAPI") or not structKeyExists(application.SFDCAPI,"sessionID") or application.SFDCAPI.sessionID eq "">
				<cfset createSFDCSession()>
			</cfif>
			<cfset urlToCall = application.SFDCAPI.serverUrl>
		</cfif>

		<!--- NJH 2012/02/20 CASE 426374: a protection against bulk update protection when in record level mode.. we can't handle a large amount of wide records
			so, here we check if the number of records we're going to be retrieving is over the threshold or not by getting a count.
			If we're going to be over the threshold, then we only get the ID and lastModified date as these are the only columns that we are interested in as it's
			only these two columns that get stored.
		--->
		<cfif not arguments.ignoreBulkProtection and application.com.settings.getSetting("salesForce.synchLevel") eq "Record" and arguments.method eq "query" and left(arguments.queryString,13) neq "select count(">
            <cfset whereClauseArray = application.com.regExp.refindSingleOccurrence(" where ",arguments.queryString)>
			<cfif arrayLen(whereClauseArray)>
				<cfset whereClause = right(arguments.queryString,len(arguments.queryString)-whereClauseArray[1].position)>
			</cfif>

			<cfset countResult = send(object=arguments.object,method="query",queryString="select count() from #arguments.object# #whereClause#")>

			<cfset request.salesForceResultsExceedsThreshold = false>

			<cfif countResult.isOK>
				<cfif listFindNoCase(countResult.response.columnList,"size") and countResult.response.size[1] gt application.com.settings.getSetting("salesForce.bulkUpdate.threshold")>
					<cfset arguments.queryString = "select ID,lastModifiedDate from #arguments.object# #whereClause#">
					<cfset request.salesForceResultsExceedsThreshold = true>
				</cfif>
			</cfif>
		</cfif>

		<cfoutput>
			<cfsavecontent variable="startSoap"><?xml version="1.0" encoding="UTF-8"?>
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
			xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<soapenv:Header>
			<cfif arguments.method eq "login"><m:LoginScopeHeader xmlns:m="urn:partner.soap.sforce.com">
			<m:organizationId></m:organizationId>
			</m:LoginScopeHeader>
			<m:CallOptions xmlns:m="urn:partner.soap.sforce.com">
			<m:client></m:client>
			</m:CallOptions>
			<cfelse><ns1:SessionHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
			<ns2:sessionId xmlns:ns2="urn:partner.soap.sforce.com">#application.SFDCAPI.sessionID#</ns2:sessionId>
			</ns1:SessionHeader>
			<ns1:AssignmentRuleHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
			<ns2:useDefaultRule xmlns:ns2="urn:partner.soap.sforce.com">#assignmentRuleHeaderUseDefaultRule#</ns2:useDefaultRule> <!--- 2014-11-19   MS  P-WEB002 Use a variable to set the boolean value sent for attribute useDefaultRule for the AssignmentRuleHeader in the SOAP ---> <!--- 2015-11-06 DAN 445281/444699 ---> 
			</ns1:AssignmentRuleHeader>
            <!--- START: 13/01/2016 Case 446364 --->
            <ns1:EmailHeader soapenv:mustUnderstand="0" xmlns:ns1="urn:partner.soap.sforce.com">
                <ns2:triggerAutoResponseEmail xmlns:ns2="urn:partner.soap.sforce.com">#triggerAutoResponseEmail#</ns2:triggerAutoResponseEmail>
                <ns2:triggerOtherEmail xmlns:ns2="urn:partner.soap.sforce.com">#triggerOtherEmail#</ns2:triggerOtherEmail>
                <ns2:triggerUserEmail xmlns:ns2="urn:partner.soap.sforce.com">#triggerUserEmail#</ns2:triggerUserEmail>
            </ns1:EmailHeader>
            <!--- END: 13/01/2016 Case 446364 --->
            </cfif>
			</soapenv:Header>
			<soapenv:Body>
			<#arguments.method# xmlns="urn:partner.soap.sforce.com">
			</cfsavecontent>

			<cfsavecontent variable="endSoap">
			</#arguments.method#>
			</soapenv:Body>
			</soapenv:Envelope></cfsavecontent>
		</cfoutput>

		<!--- check function for method exists --->
		<cftry>
			<cfif structKeyExists(variables,arguments.method)>
				<cfset methodCall = variables[arguments.method]>
			<cfelse>
				<cfoutput>Method #arguments.method# does not exist.</cfoutput><cf_abort>
			</cfif>

	 		<cfcatch type="any">
				<cfoutput>Method #arguments.method# does not exist.</cfoutput><cf_abort>
			</cfcatch>
		</cftry>

		<!--- call function for method - function will deal with different object specific behaviours - will need extending to be passed additional params
			using arguments collection forvariables like querystring for queries to execute etc. --->
		<cftry>
			<cfset sendSoap = methodCall(argumentcollection=arguments)><!--- object=arguments.object,method=arguments.method --->
			<cfset sendSoap = startSoap & sendSoap & endSoap>

		 	<cfcatch type="any">
				<cfoutput>Method #arguments.method# against #arguments.object#  failed.</cfoutput>
				<cfdump var="#cfcatch#">

				<cfif structKeyExists(url,"debug")>
					<cfrethrow>
				</cfif>
				<cfabort>
			</cfcatch>
		</cftry>

		<cfhttp url="#urlToCall#" method="post" timeout="60" result="sendResult">
			<cfhttpparam type="xml" value="#trim(sendSoap)#" />
			<cfhttpparam type="header" name="SOAPAction" value="#arguments.method#" />
		</cfhttp>

		<cfset result.errorCode=sendResult.statusCode>

		<!--- if we get an error because our session has expired, then we probably need to login and then try again
			I have removed the 'and method neq "login" from the if statement, as when a username/password has changed, the session
			needs resetting and the method is login.
		 --->
		<cfif (Find("500",sendResult.statuscode) gt 0) and arguments.method neq "login">		<!--- 2013-01-23 PPB Case 433103 additional trap for 500 errors --->
			<cfset sendResultResponse = xmlparse(sendResult.fileContent)>
			<cfset sendResponseArray=xmlSearch(sendResultResponse,"//soapenv:Fault")>
			<cfset responseFault = sendResponseArray[1].faultstring.XmlText>

			<!--- if we have an invalid session, recreate session and try call again --->
			<cfif reFindNoCase("INVALID_SESSION_ID|INVALID_OPERATION_WITH_EXPIRED_PASSWORD",responseFault)>
				<cfif structKeyExists(application.SFDCAPI,"sessionID")>
					<cfset oldSessionID = application.SFDCAPI.sessionID>
				</cfif>
				<cfset createSFDCSession()>

				<!--- replace the old Session ID with  the new Session ID --->
				<cfset sendSoap = replace(sendSoap,oldSessionID,application.SFDCAPI.sessionID)>

				<cfhttp url="#urlToCall#" method="post" timeout="60" result="sendResult">
					<cfhttpparam type="xml" value="#trim(sendSoap)#" />
					<cfhttpparam type="header" name="SOAPAction" value="#arguments.method#" />
				</cfhttp>
			</cfif>
		</cfif>

		<cfset result.errorCode=sendResult.statusCode>

		<cfset logStruct = {sendSoap = trim(sendSoap),arguments = arguments}>
		<cfset errorStruct.dataStruct = duplicate(logStruct)>

		<cfif structKeyExists(arguments,"objectID")>
			<cfset logStruct.response = sendResult>
			<cfset application.com.salesForce.logDebugInformation(entityID=arguments.objectID,entityType=arguments.object,method="send",additionalDetails=logStruct)>
		</cfif>

		<!--- http request succeeeded --->
		<cfif sendResult.statuscode eq "200 OK">
			<cfset result.success = "true">

			<!--- NJH 2010-09-09 LID 3980 - remove any 500 Internal Server Error messages for this object if we now have a successful call --->
			<cfset deleteErrorArgs = duplicate(arguments)>
			<cfset deleteErrorArgs.statusCode = "500 Internal Server Error">
			<cfset application.com.salesForce.deleteLogEntries(argumentCollection=deleteErrorArgs)>

			<cfset sendResultResponse = xmlparse(sendResult.fileContent)>

			<!--- this is where we should invoke an XML parser and build a result structure in CF derived from the XML response... --->
			<cfset parsedResponse = parseSFDCResponse(sendResultResponse,arguments.method)>

			<cfset result.response = parsedResponse.resultQry>
			<cfset result.success = parsedResponse.isOK>

			<!--- if we've run a query and there are still more records to retrieve, run the query function again until all records have been
				retreived. Consolidate the responses into a single query response --->
			<cfif listFindNoCase("query,queryMore",arguments.method) and structKeyExists(parsedResponse,"isDone")>
				<cfif not parsedResponse.isDone>
					<cfset queryMoreArgs = duplicate(arguments)>
					<cfset queryMoreArgs.method = "queryMore">
					<cfset queryMoreArgs.queryLocator = parsedResponse.queryLocator>

					<cfset queryMoreResult = send(argumentCollection=queryMoreArgs)>

					<cfquery name="unionQueryResult" dbtype="query">
						select * from result.response
						union
						select * from queryMoreResult.response
					</cfquery>

					<cfset result.response = unionQueryResult>
				</cfif>
			</cfif>

			<!--- if there were errors in the call, then log them --->
			<cfif not result.success>
				<cfloop query="parsedResponse.resultQry"> <!--- this will be an error query --->
					<cfif not success>
						<cfset structAppend(errorStruct,application.com.structureFunctions.queryRowToStruct(query=parsedResponse.resultQry,row=currentRow))>
						<cfset application.com.salesForce.logSFError(argumentCollection=errorStruct)>
					</cfif>
				</cfloop>
			<cfelse>

				<cfif listFindNoCase(parsedResponse.resultQry.columnList,"success")>
					<!--- there may not have been errors in the call, but for some reason SalesForce may have been unable to do an update or create, for example --->
					<cfloop query="parsedResponse.resultQry">  <!--- this will be a query with ids that may not have had success --->
						<cfif not success>
							<cfset errorStruct.message = "Sales Force unable to #arguments.method# #arguments.object# with ID #arguments.objectID#">
							<cfif arguments.entityID neq 0>
								<cfset errorStruct.message = errorStruct.message & " (#arguments.entityID#)">
							</cfif>
							<cfset application.com.salesForce.logSFError(argumentCollection=errorStruct)>
							<cfset result.success=false>
						<cfelse>
							<!--- delete any log entries for this objectID if we've successfully updated/created --->
							<cfset application.com.salesForce.deleteLogEntries(argumentCollection=arguments)>
						</cfif>
					</cfloop>
				</cfif>
			</cfif>

		<!--- Service returned an application error --->
		<cfelseif Find("500",sendResult.statuscode) gt 0>							<!--- 2012-12-19 PPB Case 432692 search error string for "500" rather than eq "500 Internal Server Error" (in case eg "500 Server Error" is returned) --->
			<cfset sendResultResponse = xmlparse(sendResult.fileContent)>
			<cfset result.response = sendResultResponse>
			<cfset sendResponseArray=xmlSearch(result.response,"//soapenv:Fault")><!--- : ensures we search in all contexts not just top level in the SOAP envelope --->
			<cfset result.response = sendResponseArray[1].faultstring.XmlText>

			<cfset errorStruct.message = result.response>
			<cfset errorStruct.statusCode = sendResult.statuscode>
			<cfset errorStruct.sendAlertEmail = true>
			<cfset application.com.salesForce.logSFError(argumentCollection=errorStruct)>

			<!--- START 2013-01-23 PPB Case 433103 send debug info to devs and rethrow --->
			<cfset result.success = false>

			<cfif arguments.throwErrorOn500Response>		<!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->
				<cfmail to="nathaniel.hogeboom@relayware.com,peter.barron@relayware.com,mustafa.shairani@relayware.com,jenny.mcnamara@relayware.com" from="errors@relayware.com" subject="SalesForce Connector 500 Error" type="html" priority="urgent">
					<cfdump var="Site=#request.currentsite.title#"></br></br>
					<Cfdump var="#arguments#" label="arguments">
					<cfdump var="#sendsoap#" label="Send Soap"><br>
					<cfdump var="#sendResult#" label="Send Result">
				</cfmail>

				<cfthrow message="Unable to complete process due to a 500 Internal server error from Salesforce. '#errorStruct.message#'">
			</cfif>											<!--- 2013-03-13 NJH Case 434168 throwErrorOn500Response --->

			<!--- END 2013-01-23 PPB Case 433103 --->

		<!--- http request failed --->
		<cfelse>
			<cfset result.response=sendResult.errorDetail>
		</cfif>
		<cfset result.arguments = arguments>

		<cfset result.isOk = result.success>

		<cfreturn result>

	</cffunction>

	<cffunction name="login" access="private" hint="Creates the SOAP body for the login method" output="false">
		<cfargument name="UserName" type="String" required="true" hint="The SalesForce username">
		<cfargument name="password" type="String" required="true" hint="The SalesForce password">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
			<username>#arguments.UserName#</username>
			<password>#arguments.password#</password>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="queryMore" access="private" hint="Creates the SOAP body for the queryMore method" output="false">
		<cfargument name="queryLocator" type="string" required="true" hint="The SalesForce queryLocator">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
			<queryLocator>#arguments.queryLocator#</queryLocator>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use - methods are
	 ******************************************************************************************** --->
	<cffunction name="query" access="private" hint="Creates the SOAP body for the query method" output="false">
		<cfargument name="queryString" type="String" required="true" hint="The SalesForce query string">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<queryString><![CDATA[#arguments.queryString#]]></queryString>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="queryAll" access="private" hint="Creates the SOAP body for the queryAll method" output="false">
		<cfargument name="queryString" type="String" required="true" hint="The SalesForce query string">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<queryString><![CDATA[#arguments.queryString#]]></queryString>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="getUpdated" access="private" hint="Creates the SOAP body for the getUpdated method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name.">
		<cfargument name="startDate" type="String" required="true" hint="The SalesForce startDate">
		<cfargument name="endDate" type="String" required="true" hint="The SalesForce endDate">

		<cfset var soap="">
		<cfset var updatedStartDate = convertDateToSalesForceFormat(dateToConvert=arguments.startDate)>
		<cfset var updatedEndDate = convertDateToSalesForceFormat(dateToConvert=arguments.endDate)>

		<cfoutput>
				<cfsavecontent variable="soap">
					<sObjectType>#arguments.object#</sObjectType>
					<startDate>#updatedStartDate#</startDate>
					<endDate>#updatedEndDate#</endDate>
				</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="getDeleted" access="private" hint="Creates the SOAP body for the getDeleted method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="startDate" type="String" required="true" hint="The SalesForce startDate">
		<cfargument name="endDate" type="String" required="true" hint="The SalesForce endDate">

		<cfset var soap="">
		<cfset var deletedStartDate = application.com.salesForce.convertDateToSalesForceFormat(dateToConvert=arguments.startDate)>
		<cfset var deletedEndDate = application.com.salesForce.convertDateToSalesForceFormat(dateToConvert=arguments.endDate)>

		<cfoutput>
			<cfsavecontent variable="soap">
				<sObjectType>#arguments.object#</sObjectType>
				<startDate>#deletedStartDate#</startDate>
				<endDate>#deletedEndDate#</endDate>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
	<cffunction name="describeSObjects" access="private" hint="Creates the SOAP body for the describeSObjects method" output="false">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">

		<cfset var soap="">

		<cfoutput>
			<cfsavecontent variable="soap">
				<sObjectType>#arguments.object#</sObjectType>
			</cfsavecontent>
		</cfoutput>

	<cfreturn soap>

	</cffunction>

	<!--- ********************************************************************************************
	this is a method function that builds the SOAP body for send to use
	 ******************************************************************************************** --->
 	<cffunction name="retrieve" access="private" output="false" hint="Creates the SOAP body for the retrieve method">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="fieldList" type="String" default="" hint="The SalesForce list of fields to retreive">
		<cfargument name="ids" type="String" required="true" hint="The SalesForce list of ids for which to retrieve data">

		<cfscript>
			var soap="";
			var id="";
			var thisObject = "";
			var objectName = "";
			var retrieveFieldList = "";
			var retrieveFieldName = "";
			var fieldName = "";
			var cleanedFieldList = "";
			var node = "";
			var objectList = arguments.object;
			var mappedArray = "";
		</cfscript>

		<!--- if no fields have been passed, then by default we get all the fields for the object that we have mapped. --->
		<cfif arguments.fieldList eq "">
			<cfset mappedArray = xmlSearch(application.sfMappingXml,"//mappings/mapping[translate(@object,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.object)#']/@objectColumn")>
			<cfloop array=#mappedArray# index="node">
				<cfset arguments.fieldList = listAppend(arguments.fieldList,node.xmlValue)>
			</cfloop>
		</cfif>

		<cfif arguments.object eq "opportunity">
			<cfset objectList = arguments.object> <!--- TO DO: Need to double check this is really what we want to do --->
		</cfif>

		<cfset retrieveFieldList = arguments.fieldList>

		<!--- When exporting an opportunity, we need the ownerID - Not sure this is going to work.. have put it in here for now --->
		<cfif arguments.object eq "opportunity" and not listFindNoCase(retrieveFieldList,"ownerID")>
			<cfset retrieveFieldList = listAppend(retrieveFieldList,"ownerID")>
		</cfif>

		<!--- get the sfObject struct for the mapped objects if they don't already exist --->
		<cfloop list="#objectList#" index="thisObject">
			<cfset application.com.salesForce.putSFDCObjectIntoMemory(object=thisObject)>
		</cfloop>

		<!--- if the list of fieldnames isn't in the core object, then it's more than likely a field that's in the secondary object. So, to get this field,
			we use the base objects query field to get the additional fields.

			For example:
				product maps to product2 and baseProduct__c
				if we want to get duration for the product, it's actually a baseProduct__c field. So, we need to get this field via the product2 baseProduct__r field.
				It ends up looking like this... baseProduct__r.duration
			 --->
		<cfloop list="#objectList#" index="thisObject">
			<cfloop collection="#application.sfObjects[thisObject]#" item="fieldName">

				<!--- only add the field if it's in our list of fieldnames to get --->
				<cfif listFindNoCase(retrieveFieldList,fieldName)>
					<cfset retrieveFieldName = fieldName>

					<cfset retrieveFieldName = setObjectFieldsForQuery(object=thisObject,fieldlist=fieldName)>

					<cfif ((thisObject neq arguments.object and listFindNoCase(arguments.fieldList,fieldName)) and (not structKeyExists(application.sfObjects[arguments.object],fieldName))) or (listValueCountNoCase(arguments.fieldList,fieldName) gt 1)>
						<cfset objectName = thisObject>
						<cfif right(objectName,3) eq "__c">
							<cfset objectName = replaceNoCase(objectName,"__c","__r")>
						</cfif>

						<cfset retrieveFieldName = "#objectName#.#retrieveFieldName#">
					</cfif>

					<cfif not listFindNoCase(cleanedFieldList,retrieveFieldName)>
						<cfset cleanedFieldList = listAppend(cleanedFieldList,retrieveFieldName)>
					</cfif>
				</cfif>
			</cfloop>
		</cfloop>

		<cfoutput>
			<cfsavecontent variable="soap">
				<fieldList>#cleanedFieldList#</fieldList>
				<sObjectType>#arguments.object#</sObjectType>
				<cfloop list="#arguments.ids#" index="id" delimiters=","><ids>#id#</ids></cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="update" access="private" hint="Performs updates on objects. The objects are passed through in an array and must be of the same type" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="objects" type="array" required="true" hint="An array of objects">

		<cfreturn buildCreateUpdateSoapPacket(method="update",argumentCollection=arguments)>

	</cffunction>

	<cffunction name="process" access="private" output="false" hint="Performs a SalesForce process request">
		<cfargument name="processRequests" type="array" required="true" hint="An array of process requests">
		<cfargument name="processType" type="string" default="ProcessSubmitRequest" hint="Can be ProcessSubmitRequest or ProcessWorkitemRequest">

		<cfscript>
			var soap="";
			var node="";
		</cfscript>

		<cfoutput>
			<cfsavecontent variable="soap">
				<cfloop array="#arguments.processRequests#" index="node">
					<actions xsi:type="urn:#arguments.processType#">
						<objectId>#node.objectID#</objectId>
						<cfif structKeyExists(node,"nextApproverIds")><nextApproverIds>#node.nextApproverIds#</nextApproverIds></cfif>
						<cfif structKeyExists(node,"comments")><comments>#node.comments#</comments></cfif>
					</actions>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="upsert" access="private" output="false" hint="Performs insert/updates on objects. The objects are passed through in an array and must be of the same type. Objects are mapped based on a custom field">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="objects" type="array" required="true" hint="An array of objects">

		<cfscript>
			var soap="";
			var node="";
			var fieldName = "";
		</cfscript>

		<!--- perhaps need to filter out any unwanted fieldnames, or perhaps fieldnames that are not updateable or createable --->
		<cfoutput>
			<cfsavecontent variable="soap">
				<!--- start per object --->
				<cfloop array="#arguments.objects#" index="node">
					<sObject>
					<type xmlns="urn:sobject.partner.soap.sforce.com">#arguments.Object#</type>
					<cfloop collection="#node#" item="fieldName">
					<#fieldname#><![CDATA[#node[fieldName]#]]></#fieldname#></cfloop>
					</sObject>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>

	</cffunction>

	<cffunction name="create" access="private" output="false" hint="Creates the SOAP body for the create method">
	 	<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="objects" type="array" required="true" hint="An array of objects">

		<cfreturn buildCreateUpdateSoapPacket(method="create",argumentCollection=arguments)>
	</cffunction>

	<cffunction name="setObjectFieldsForQuery" access="public" returntype="string" output="false">
		<cfargument name="object" type="string" required="true">
		<cfargument name="fieldlist" type="string" required="true">
		<cfargument name="useLabel" type="boolean" default="true">

		<cfscript>
			var retrieveFieldName = "";
			var retrieveFieldList = "";
			var fieldName = "";
		</cfscript>

		<!--- This function will return a list of fields used for a query. In the case where a field is a picklist, we want to
			get the description of the picklist and not the value (I think!!!). I have just discovered the 'toLabel' function and would have
			made life easier if I had known about it, as it would have meant not having to work with random numbers, such as 62 for Spain --->
		<cfset application.com.salesforce.putSFDCObjectIntoMemory(object=arguments.object)>

		<cfloop list="#arguments.fieldList#" index="fieldName">
			<cfif structKeyExists(application.sfObjects[arguments.object],fieldName)>
				<cfset retrieveFieldName = fieldName>
				<cfif application.sfObjects[arguments.object][fieldName].type eq "picklist" and arguments.useLabel>
					<cfset retrieveFieldName = "toLabel(#fieldName#)">
				</cfif>
				<cfset retrieveFieldList = listAppend(retrieveFieldList,retrieveFieldName)>
			</cfif>
		</cfloop>

		<cfreturn retrieveFieldList>
	</cffunction>

	<!--- NJH 2013/08/14 Case 435632 - factorised function for creating soap packets for the create/update methods. If the SF object has a length in its metadata, use it to retrieve the 'length' leftmost characters --->
	<cffunction name="buildCreateUpdateSoapPacket" access="private" hint="Builds the SOAP packet for the create/update methods">
		<cfargument name="method" type="string" default="create">
		<cfargument name="object" type="String" required="true" hint="The SalesForce object name">
		<cfargument name="objects" type="array" required="true" hint="An array of objects">

		<cfscript>
			var soap="";
			var node="";
			var fieldName = "";
			var isNillable = false;
			var nillableFieldList = "";
			var fieldValue = "";
			var addFieldToXml = true;
		</cfscript>

		<cfset application.com.salesForce.putSFDCObjectIntoMemory(object=arguments.object)>

		<!--- filter out any unwanted fieldnames, or fieldnames that are not createable/updateable --->
		<cfoutput>
			<cfsavecontent variable="soap">
				<!--- start per object --->
				<cfloop array="#arguments.objects#" index="node">
					<cfset nillableFieldList = "">
					<cfif structKeyExists(node,"fieldsToNull")>
						<cfset nillableFieldList = node.fieldsToNull>
						<cfset structDelete(node,"fieldsToNull")>
					</cfif>
					<sObject>
					<type xmlns="urn:sobject.partner.soap.sforce.com">#arguments.Object#</type>
					<cfloop collection="#node#" item="fieldName">
						<cfset isNillable = false>
						<cfset addFieldToXml = false>
						<cfif listFindNoCase(nillableFieldList,fieldName)>
							<cfif structKeyExists(application.sfObjects[arguments.object],fieldname) and application.sfObjects[arguments.object][fieldName].nillable>
								<cfset isNillable = true>
							</cfif>
						</cfif>
						<cfset fieldValue = node[fieldName]>
						<!--- NJH 2013/08/14 Case 435632 --->
						<cfif (structKeyExists(application.sfObjects[object],fieldname) and application.sfObjects[object][fieldName].length neq 0)>
							<cfset fieldValue = left(fieldValue,application.sfObjects[object][fieldName].length)>
						</cfif>
						<cfif not isNillable>
							<cfif arguments.method eq "create" and structKeyExists(application.sfObjects[object],fieldname) and application.sfObjects[object][fieldName].createable>
								<cfset addFieldToXml = true>
							<cfelseif arguments.method eq "update" and (fieldName eq "ID" or (structKeyExists(application.sfObjects[arguments.object],fieldname) and application.sfObjects[arguments.object][fieldName].updateable))>
								<cfset addFieldToXml = true>
							</cfif>
						</cfif>
						<cfif addFieldToXml><#fieldname#><![CDATA[#fieldValue#]]></#fieldname#><cfelseif isNillable><fieldsToNull>#fieldName#</fieldsToNull></cfif></cfloop>
					</sObject>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>
	</cffunction>

	<cffunction name="delete" access="private" output="false" hint="Creates the SOAP body for the delete method">
	 	<cfargument name="ids" type="String" required="true" hint="The SalesForce list of ids which to delete">

		<cfscript>
			var soap="";
			var deletedObjId="";
		</cfscript>

		<cfoutput>
			<cfsavecontent variable="soap">
				<cfloop list="#arguments.ids#" index="deletedObjId">
					<ids>#deletedObjId#</ids>
				</cfloop>
			</cfsavecontent>
		</cfoutput>

		<cfreturn soap>
	</cffunction>

 	<cffunction name="parseSFDCResponse" access="private" hint="Parses the response of a SOAP call to SFDC" output="false">
		<cfargument name="response" type="XML" required="true" hint="The response XML as received from SalesForce">
		<cfargument name="method" type="string" required="true" hint="The SalesForce method that was called">

		<cfscript>
			var resultXML = "";
			var searchItem = ""; // the item to look for in the incoming xml
			var result = {isOK = true};
			var cleanedResponse = "";
			var isDoneArray = arrayNew(1);
			var queryLocatorArray = arrayNew(1);
		</cfscript>

		<cfswitch expression="#arguments.method#">
			<cfcase value="getUpdated">
				<cfset searchItem = "ids">
			</cfcase>
			<cfcase value="getDeleted">
				<cfset searchItem = "deletedRecords">
			</cfcase>
			<cfcase value="describeSObjects">
				<cfset searchItem = "fields">
			</cfcase>
			<cfcase value="query,queryMore,queryAll">
				<cfset searchItem = "records">
			</cfcase>
			<cfdefaultcase>
				<cfset searchItem = "">
			</cfdefaultcase>
		</cfswitch>

		<cfif searchItem neq "">
			<cfset searchItem = "/*[local-name()='#searchItem#']">
		</cfif>

		<!--- have to replace type (lower case) with object type, as SalesForce appends the type to the response, which is confused with fields
			that are called type, such as for an account  --->
		<cfset cleanedResponse = replace(arguments.response,"sf:type","sf:objectType","ALL")>

		<!--- first find out if the response contains any errors --->
		<cfset resultXML = xmlSearch(cleanedResponse,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']/*[local-name()='errors']")>
		<cfif arrayLen(resultXML) gt 0>
			<cfset arguments.method = "errors">
			<cfset result.isOk = false>
		<cfelse>
			<cfset resultXML = xmlSearch(cleanedResponse,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']#searchItem#")>

			<!--- if a query was sent, and no results were returned, then we don't get records back, but some fields saying that the query successfully
				brought back nothing. --->
			<cfif arrayLen(resultXML) eq 0 and listFindNoCase("query,queryAll",arguments.method)>
				<cfset resultXML = xmlSearch(cleanedResponse,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']")>
			</cfif>
		</cfif>

		<!--- if we have done a query, we may not get all the rows returned in one batch... in this case, we do a queryMore call, but
			it expects a queryLocator Id, which is what we're getting here. --->
		<cfif listFindNoCase("query,queryMore,queryAll",arguments.method)>
			<cfset isDoneArray = xmlSearch(cleanedResponse,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']/*[local-name()='done']")>
			<cfif arrayLen(isDoneArray) eq 1>
				<cfset result.isDone = isDoneArray[1].xmlText>
				<cfif not result.isDone>
					<cfset queryLocatorArray = xmlSearch(cleanedResponse,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']/*[local-name()='queryLocator']")>
					<cfif arrayLen(queryLocatorArray) eq 1>
						<cfset result.queryLocator = queryLocatorArray[1].xmlText>
					</cfif>
				</cfif>
			</cfif>
		</cfif>

		<cfset result.resultQry = convertResponseToQuery(responseArray=resultXML,method=arguments.method,searchItem=searchItem)>

		<cfreturn result>
	</cffunction>

	<cffunction name="convertResponseToQuery" access="private" returntype="query" hint="Parses the response of a SOAP call to SFDC and builds a query from it" output="false">
		<cfargument name="responseArray" type="array" required="true">
		<cfargument name="method" type="string" required="true">
		<cfargument name="parentXmlName" type="string" default="">
		<cfargument name="searchItem" type="string" default="">

		<cfscript>
			var resultQry = queryNew("success");
			var arrayLength = arrayLen(arguments.responseArray);
			var idx = "";
			var objectColumnList = "";
			var childrenArray = arrayNew(1);
			var childrenArrayLength = 0;
			var childArrayIdx = 0;
			var nameSpace = "";
			var childQryStruct = structNew();
			var baseColumn = "";
			var childColumn = "";
			var columnArray = arrayNew(1);
			var childQry = "";
			var childQryArray = arrayNew(1);
			var columnValue = "";
			var columnName = "";
			var newColumnName = "";
			var searchParentXMLName = "";
			var dataTypeList = "";
			var count = 0;
			var pickListArray = arrayNew(1);
			var xmlNode = "";
		</cfscript>

		<cfset queryAddRow(resultQry)>
		<cfset querySetCell(resultQry,"success",1)>

		<cfswitch expression="#arguments.method#">

			<!--- these methods just returns a list of ids of the given object --->
			<cfcase value="errors">
				<cfset objectColumnList = "success,fields,message,statusCode">
				<cfset resultQry = queryNew(objectColumnList)>

				<cfloop from="1" to=#arrayLength# index="idx">
					<cfset queryAddRow(resultQry)>
					<cfloop list="#objectColumnList#" index="columnName">
						<cfif columnName eq "success">
							<cfset querySetCell(resultQry,columnName,0)>
						</cfif>
						<cfif structKeyExists(arguments.responseArray[idx],columnName)>
							<cfset querySetCell(resultQry,columnName,arguments.responseArray[idx][columnName].xmlText)>
						</cfif>
					</cfloop>
				</cfloop>
			</cfcase>

			<!--- these methods just returns a list of ids of the given object --->
			<cfcase value="getUpdated">
				<cfset resultQry = queryNew("id")>

				<cfloop from="1" to=#arrayLength# index="idx">
					<cfset queryAddRow(resultQry)>
					<cfset querySetCell(resultQry,"id",arguments.responseArray[idx].xmlText)>
				</cfloop>
			</cfcase>

			<!--- just wanting to return a list of field names and their types for a given object. Could extend it to return other information
				as well, but this is all we need for now --->
			<cfcase value="describeSObjects">
				<cfset objectColumnList = "name,type,nillable,precision,scale,length,custom,updateable,createable">
				<cfset resultQry = queryNew(objectColumnList)>

				<cfloop from="1" to=#arrayLength# index="idx">
					<cfset queryAddRow(resultQry)>
					<cfloop list="#objectColumnList#" index="columnName">

						<cfset querySetCell(resultQry,columnName,arguments.responseArray[idx][columnName].xmlText)>
					</cfloop>
				</cfloop>
			</cfcase>

			<!--- query,queryMore,update,create,retrieve methods --->
			<cfdefaultcase>

				<!--- get the column names dynamically for the result query --->
				<cfif arrayLen(arguments.responseArray) gt 0>
					<cfset childrenArray = arguments.responseArray[1].xmlChildren>

					<!--- building the column list --->
					<cfif objectColumnList eq "">
						<cfset childrenArrayLength = arrayLen(childrenArray)>
						<cfif childrenArrayLength gt 0>
							<cfset nameSpace = childrenArray[1].xmlNsPrefix>
						<cfif nameSpace neq "">
							<cfset nameSpace = nameSpace&":">
						</cfif>

						<cfloop array="#childrenArray#" index="xmlNode">
							<cfset columnName = xmlNode.xmlname>
							<cfif nameSpace neq "">
								<cfset columnName = replace(xmlNode.xmlname,nameSpace,"")>
							</cfif>

							<!--- we found out that responses can contain query objects. We are happy to get the data if we are getting child-parent information,
								but not parent-child info. Put the response queries into a structure, and we will 'join' the queries below. If this is the case,
								then don't add a column to the result query for this particular column. --->
							<cfif isArray(xmlNode.xmlChildren) and arrayLen(xmlNode.xmlChildren) gt 0 and listFindNoCase("retrieve,query",arguments.method)>
								<cfset searchParentXMLName = arguments.parentXmlName&IIF(arguments.parentXmlName neq "",DE('/'),'')&xmlNode.xmlname>
								<cfset childQryArray = xmlSearch(xmlNode,"//*[local-name()='#arguments.method#Response']/*[local-name()='result']/#arguments.searchItem#/#searchParentXMLName#/")>
								<cfset childQryStruct[columnName]=convertResponseToQuery(responseArray=childQryArray,method=arguments.method,parentXmlName=searchParentXMLName,searchItem=arguments.searchItem)>
							<cfelseif not listFindNoCase(objectColumnList,columnName)>
								<cfset objectColumnList = listAppend(objectColumnList,columnName)>
							</cfif>
						</cfloop>
					</cfif>
				</cfif>

					<!--- build the result query. Loop through every column name and set its type to be varchar --->
					<cfset dataTypeList = "">
					<cfloop from=1 to="#listLen(objectColumnList)#" index="count">
						<cfset dataTypeList = listAppend(dataTypeList,"varchar")>
					</cfloop>
					<cfset resultQry = queryNew(objectColumnList,dataTypeList)>

					<cfif not (arrayLength eq 1 and listlen(objectColumnList) eq 0)>
						<cfloop array="#arguments.responseArray#" index="xmlNode">
							<cfset queryAddRow(resultQry)>
							<cfloop list="#objectColumnList#" index="columnName">
								<cfif structKeyExists(xmlNode,nameSpace&columnName)>
									<cfset querySetCell(resultQry,columnName,xmlNode[nameSpace&columnName].xmlText)>
								</cfif>
							</cfloop>
						</cfloop>
					</cfif>

					<!--- Loop through the structure of queries --->
					<cfloop collection="#childQryStruct#" item="baseColumn">
						<!--- if the child query has the same number of rows as the base query, then add them --->
						<cfif childQryStruct[baseColumn].recordCount eq resultQry.recordCount>
							<cfloop list="#childQryStruct[baseColumn].columnList#" index="childColumn">
								<cfset newColumnName = baseColumn&"_"&childColumn>
								<cfset columnArray = arrayNew(1)>
								<cfset childQry = childQryStruct[baseColumn]>
								<!--- build an array that holds the column values... --->
								<cfloop query="childQry">
									<!--- if no value is passed, then don't set it; will turn out as null; otherwise this causes problems when some of the text is
										numbers and then an empty string is added. --->
									<cfif childQry[childColumn][currentRow] neq "">
										<cfset columnArray[currentRow] = childQry[childColumn][currentRow]>
									</cfif>
								</cfloop>
								<!--- add the column to the base query --->
								<cfset queryAddColumn(resultQry,newColumnName,"varchar",columnArray)>
							</cfloop>
						</cfif>
					</cfloop>
				</cfif>
			</cfdefaultcase>

		</cfswitch>

		<cfreturn resultQry>
	</cffunction>

	<!--- 2014-10-13	AHL		Case 442038 Opportunity not sync. Apostrophe SOQL Escape Characters --->
	<!--- This function escapes SOLQ Characters --->
	<cffunction name="escapeSoql" access="public" hint="Escapes SOQL characters" output="false">
		<cfargument name="strToEscape" type="string" required="true" hint="String to be escaped">

		<cfscript>
			var result = "";
		</cfscript>

		<cfset result = reReplace (strToEscape,"([""'\\])","\\\1","ALL")>

		<cfreturn result>

	</cffunction>
	<!--- 2014-10-13	AHL		Case 442038 Opportunity not sync. Apostrophe SOQL Escape Characters --->
</cfcomponent>