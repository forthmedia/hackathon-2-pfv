<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="relayKeyStats" hint="Manages the updating and displaying of Relay Statistics">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query                      
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateStandardKeyStats" hint="Runs a number of queries that update the key stats table.">
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		<cfset timeRun = now()>
		
		<!--- to control which countries you will generate key stats on change request.currentSite.liveCountryIDs in relayVars --->
		<cfparam name="request.currentSite.liveCountryIDs" default="9">

		<cfquery name="checkKeyStatsDefExists" datasource="#application.siteDataSource#">
			select * from dbo.sysobjects where id = object_id(N'[dbo].[keyStatsDef]') 
		</cfquery>
		
		<cfif checkKeyStatsDefExists.recordCount eq 0>
			<!--- if not create it --->
			<cfinvoke method="initialiseKeyStatsDef"></cfinvoke>
		</cfif>
			
		
		<!--- get the keyStatsDef clauses--->
		<cfquery name="getKeyStatsDef" datasource="#dataSource#">
			select * from keyStatsDef
		</cfquery>
		 
		<cfoutput>
			<cfloop query="getKeyStatsDef">
				<cfquery name="insertKeyStats" datasource="#dataSource#">
					INSERT INTO [relayKeyStats]([description], [path], [value], [lastUpdated], [countryID], [userGroup])
					select <cf_queryparam value="#getKeyStatsDef.statistic#" CFSQLTYPE="CF_SQL_VARCHAR" >,'', count(*), <cf_queryparam value="#timeRun#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,0,'any' #preservesingleQuotes(getKeyStatsDef.allCountryWhereClause)#
				</cfquery>
			</cfloop>
		
			<cfloop index="i" list="#request.currentSite.liveCountryIDs#">
				<cfloop query="getKeyStatsDef">
					<cfquery name="insertKeyStats" datasource="#dataSource#">
						INSERT INTO [relayKeyStats]([description], [path], [value], [lastUpdated], [countryID], [userGroup])
						select <cf_queryparam value="#getKeyStatsDef.statistic#" CFSQLTYPE="CF_SQL_VARCHAR" >,'', count(*), <cf_queryparam value="#timeRun#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#i#" CFSQLTYPE="CF_SQL_INTEGER" >,'any' #preservesingleQuotes(getKeyStatsDef.CountryWhereClause)# #i#
					</cfquery>
				</cfloop>
			</cfloop>
		</cfoutput>
		
		<cfreturn "Key stats refreshed for all countryIDs 0,#request.currentSite.liveCountryIDs#">
	</cffunction>


	<cffunction name="initialiseKeyStatsDef" access="private" hint="Sets up keyStatsDef and some basic data">
	
		<cfargument name="datasource" type="string" default="#application.siteDataSource#">
		
		<cfquery name="createKeyStatsDef" datasource="#dataSource#">
			create table keyStatsDef
			(keyStatsDefID int IDENTITY (1, 1) NOT NULL ,
			statistic [varchar] (50) NOT NULL,
			allCountryWhereClause [varchar] (4000) NOT NULL,
			CountryWhereClause [varchar] (4000) NOT NULL)
			
			INSERT INTO [keyStatsDef]([statistic], [allCountryWhereClause], [CountryWhereClause])
			VALUES('Phr_Sys_People_with_valid_emails', 'from person where email like ''%@%.%''', 'from person p INNER JOIN location l on p.locationID = l.locationID where email like ''%@%.%'' AND l.countryID =')
			
			INSERT INTO [keyStatsDef]([statistic], [allCountryWhereClause], [CountryWhereClause])
			VALUES('phr_Sys_People_in_account_database', 'from person', 'from person p INNER JOIN location l on p.locationID = l.locationID where l.countryID =')
			
			INSERT INTO [keyStatsDef]([statistic], [allCountryWhereClause], [CountryWhereClause])
			VALUES('phr_Sys_Organisations_in_account_database', 'from organisation', 'from organisation where countryID =')
		</cfquery>
	
	</cffunction>
 
</cfcomponent>
