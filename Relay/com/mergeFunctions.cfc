<!--- �Relayware. All Rights Reserved 2014 --->
<!---

These are all stub functions which call other functions in the application.com scope.
	This allows us to control which functions are available for use within phrases
	- any expression containing application.com is rejected, but we can alias this component as say fn.


	WAB 2008/06/19 added outputAddress function
	NYB	2009/02/19 Sophos Stargate 2
	WAB/NJH 2009/03/03 Lighthouse Issue All Sites 1739 - added function to get the url of an external site.
	NYB	2009/03/05 Sophos Stargate 2
	NYB 2009/06/11 removed IsFlagSet function - func in redirectorAllowedFunctions.cfc - which extends this file
	NJH	2010/03/23	P-PAN002 - added encryptURL function

	WAB 2010/10  8.3
		MAJOR CHANGES Created mergeFunctions.cfc out of functionsAllowedInPhrases, redirectorAllowedFunction.cfc and emailMerge.cfc
		This file extends a core merge.cfc (which has all the functions for doing the actual merging)
		Still work to be done so that functions can be listed nicely in content editor
		Different functions may shw in particular contexts

	NYB 2011-03-09	changed functions IsUserInUsergroup and IsPersonInUsergroup
	WAB 2010/05/05 LID 6501 Realised that I had not really integrated the functions from emailMerge.cfc in properly
	WAB 2011/11/18 problem with unsubscribe functions having same name as variables - renamed functions
				seemed to be confusion as to whether communication.siteURL or communication.linksToSiteURL - came down on side of latter, since that is field in db
	WAB 2012-03-14  Implement omniture tracking in a new way
	WAB 2012-01-29 CASE 433406 alter trackedLink() to remove any htmlentities added to URL by CKEDITOR
	WAB 2013-04-	During comms work, change all references to Comm.linktoSiteURL to Comm.linktoSiteURLWithProtocol
	WAB 2013-07-02 Switched on some functions to appear in the function picker for text based emails.  Context = TextCommunication.  Needed to add support for passing in a list of contexts when retrieving merge functions so that I could get items of either communication OR textCommunication context
	WAB 2013-07-04 Removed omniture functions, now all injected into mergeObject at run time by communication.cfc
	WAB	2014-09-03	Merging of Non POL Flags (reworking some recent YMA changes to showProfileValue and showProfileAttributeValue).
	NJH	2014-12-17	Added encryptVariableValue as it's a nice function to have and it may be needed for Evault in the future.
	 WAB 2015-01-28 ShowProfileAttributeValue () Added valueList() to deal with return from 'multiple' flags
 --->

<cfcomponent extends="merge" cache="false">


	<cffunction name = "URLParamsForLinkToElement" return="string" output="no">
		<cfargument name="elementID" required="yes" type="string">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">
		<cfreturn application.COM.relayElementTree.createSEIDQueryString (linktype="tracked",elementid = elementID,personid = arguments.personid)>

	</cffunction>


	<cffunction name = "if" return="string" output="no" >
		<cfargument name="condition" required="yes" type="string">
		<cfargument name="trueResult" required="yes" type="string">
		<cfargument name="falseResult" required="yes" type="string">

		<cfif condition>
			<cfreturn trueResult>
		<cfelse>
			<cfreturn falseResult>
		</cfif>

	</cffunction>

	<cffunction name="SalesRepDetails" return="string" output="yes">
		<cfargument name="flagTextID" required="yes" default = "AccSalesRep" type="string">
		<cfargument name="personID" default="#getPersonid()#" showInEditor = "false">


		<cfset var getSalesRep="">

		<cfif not application.com.flag.doesFlagExist(flagID='#arguments.flagTextID#')>
			<cfreturn "">
		<cfelse>
			<!--- 2007/03/01 GCC needs a lot fo work - hacked in for Lenovo - can work with integer and integer multiple but not a lot else I guess --->
			<cfquery name="getSalesRep" datasource="#application.sitedatasource#">
				SELECT   top 1  person.firstname,person.lastname,person.email
				FROM         Person Person_1 INNER JOIN
		                      #application.com.flag.getFlagStructure(arguments.flagTextID).FlagType.DataTableFullName# fd ON Person_1.OrganisationID = fd.entityid INNER JOIN
		                      flag ON fd.flagid = flag.FlagID LEFT OUTER JOIN
		                      Person ON fd.data = Person.PersonID
				WHERE     (Flag.FlagTextID =  <cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ) AND (Person_1.PersonID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="cf_sql_integer" > )
				order by person.firstname,person.lastname,person.email,person.personid
			</cfquery>
			<cfif getSalesRep.recordcount eq 0>
				<cfreturn "">
			<cfelse>
				<cfreturn "#getSalesRep.firstname# #getSalesRep.lastname# #getSalesRep.email#">
			</cfif>
		</cfif>
	</cffunction>

	<cffunction name="dump" return="string" output="yes">
		<cfargument name="var" required="yes" >
		<cfset var result_html = "">

		<cfsavecontent variable = "result_html">
			<cfdump var="#var#">
		</cfsavecontent>

		<cfreturn result_html>
	</cffunction>

	<!--- 2008/06/19 WAB testing outputting of addresses in emails
		2012-05-15 TBD Note, does not work in Comms because not all the location fields are available automatically
	--->
	<cffunction name="OutputAddress" access="public" description="Formatted Address" output="false">
		<!--- NJH 2011/08/03 - apparently this function takes these arguments, but they don't seem to do anything so commenting out for now.
		<cfargument name="separator" type="string" default=",">
		<cfargument name="showCountry" type="boolean" default="true">
		<cfargument name="finalCharacter" type="string" default="."> --->

		<cfreturn application.com.screens.evaluateAddressFormat (location = getLocation(),argumentCollection = arguments)>
	</cffunction>

	<!--- 2009/02/19 NYB Sophos Stargate 2
		WAB Better to use showProfileValue() which automatically deals with getting the right POL entityID

	--->
	<cffunction name="getFlagData">
		<cfargument name="FlagID" required="yes" >
		<cfargument name="EntityID" required="yes" >
		<cfreturn application.com.flag.getFlagData(flagId = FlagID,entityid=EntityID).data>
	</cffunction>

	<!--- 2009/06/11 NYB - removed IsFlagSet function - func in and included by redirectorAllowedFunctions.cfc --->

	<!--- WAB/NJH 2009/03/03 Lighthouse Issue All Sites 1739 --->
	<cffunction name="getExternalSiteURL">
		<cfargument name="site" default =  "">
		<!--- <cfargument name="country" default =  "">	 --->

		<cfreturn application.com.relayCurrentSite.getExternalSiteURL(argumentCollection = arguments)>


	</cffunction>

	<!--- NJH P-PAN002 2010/03/23 --->
	<cffunction name="encryptURL">
		<cfreturn application.com.security.encryptURL(argumentcollection=arguments)>
	</cffunction>




	<!---
	***************************************************
		These functions from redirectorAllowedFunctions
	***************************************************
	--->


	<!--- From redirector required functions --->
	<cffunction name = "isFlagSet" return="boolean" output=no>
		<cfargument name="flagID" required="yes" type="string">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">

		<cfreturn application.com.flag.isFlagSetForPerson(flagid = flagid,personid = arguments.personid)>

	</cffunction>

	<!--- WAB 2007/12/04 really for bringing back a list of boolean flags,  --->
	<cffunction name = "getFlagList" return="boolean" output=no>
		<cfargument name="flagGroupID" required="yes" type="string">
		<cfargument name="column" default="flagTextID">   <!--- could be flagid or name --->
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">

		<cfset var result = "">
		<cfset var tmp = application.com.flag.getFlagGroupDataForPerson(flaggroupid = flaggroupid,personid=arguments.personid)>

		<cfswitch expression="#column#">
			<cfcase value="flagTextID">
				<cfset result = valueList (tmp.flagTextID)>
			</cfcase>
			<cfcase value="flagID">
				<cfset result = valueList (tmp.flagID)>
			</cfcase>
			<cfcase value="Name">
				<cfset result = valueList (tmp.Name)>
			</cfcase>

		</cfswitch>


		<cfreturn result >

	</cffunction>


	<cffunction name = "isPersonIncentiveRegistered" return="boolean" output=no moduleID="32">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false"
					>

		<cfif application.com.relayIncentive.isPersonRegistered(personid = arguments.personid).recordcount is 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>

	<!--- this is the existing function.  Any references to it ought to be replaced with one above --->
	<cffunction name = "isUserIncentiveRegistered" return="boolean" output=no moduleID="32">
		<cfreturn isPersonIncentiveRegistered(personid = request.relaycurrentuser.personid)>
	</cffunction>


	<cffunction name = "doesUserHaveRights" return="boolean" output=no>
		<cfargument name="task" required="yes" type="string">
		<cfargument name="level" required="yes" type="string">

		<cfif isNumeric (arguments.level)><cfset arguments.level = "Level" & arguments.level> </cfif>
		<cfreturn application.com.login.checkInternalPermissions(securityTask = task, securityLevel = level)>

	</cffunction>

	<!--- 2008-11-12 NYB P-SOP010 - added InUsergroup function --->
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        function description
		NYB 2011-03-09 rewrote
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name = "IsPersonInUsergroup">
		<cfargument name="Usergroup" required="yes" type="string">
		<cfargument name="PersonID" default="#getPersonid()#" showInEditor = "false">
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="true">
		<cfset var IntUsergroups = application.com.login.isPersonInOneOrMoreUserGroups(UserGroups=arguments.Usergroup,argumentCollection=arguments)>
		<cfreturn IntUsergroups>
	</cffunction>

	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		this is the existing function.  Any references to it ought to be replaced with one above
		NYB 2011-03-09 rewrote
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name = "IsUserInUsergroup">
		<cfargument name="Usergroup" required="yes" type="string">
		<cfargument name="PersonID" default="#getPersonid()#" showInEditor = "false">
   		<cfargument name="IncludePersonUserGroups" type="boolean" default="true">
		<cfset var IntUsergroups = application.com.login.isPersonInOneOrMoreUserGroups(UserGroups=arguments.Usergroup,argumentCollection=arguments)>
		<cfreturn IntUsergroups>
	</cffunction>


	<!---
	************************************
		These functions from emailMerge
	************************************
	--->
	<cffunction name="createRemoteCFMLink" return="string" output="no">
		<cfargument name="personid" type="numeric" default = "#getPersonID()#" showInEditor = "false">
		<cfargument name="locationid" type="numeric" default = "#getLocationID()#" showInEditor = "false">
		<cfargument name="siteURL" type="string" default="#getPartnerPortalURL()#" >
		<cfargument name="test" type="boolean" default=#getCommunication().test#>
		<cfargument name="commid" type="numeric" default=#getCommunication().commID#>
		<cfargument name="useSession" type="boolean" default="false"> <!--- NJH 2011/08/08 added so that RelayCommunicationLink function as otherwise request.currentsite was undefined when previewing a comm... maybe a better way of doing this.. --->

		<cfset var result =  "#SiteURL#/#iif(not arguments.useSession,de("webservices/nosession/"),de(""))#remote.cfm?#iif(arguments.test,de("t=1&"),de(""))#&c=#CommID#&p=#createMagicNumber(personid = personid, locationid = locationid)#" >

		<cfreturn result>

	</cffunction>


	<!--- removed from the merge picker as you can now use person.magicNumer --->
	<cffunction name="createMagicNumber" return="string" output="no">
		<cfargument name="personid" type="numeric" default="#getPersonID()#" showInEditor = "false">
		<cfargument name="locationid" type="numeric" default="#getLocationID()#" showInEditor = "false">

		<cfset var result = personid & '-' & (bitand(personid,1023) * bitand(locationid,1023))>

		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="URLParameterForUnrestrictedLinkToSecurePage" return="string" output="no" context="emailDef">
		<cfargument name="ElementID" type="string" required="true" hint="the elementid of the page to go to">
		<cfargument name="personid" type="numeric" default="#getPersonID()#" showInEditor = "false">
		<cfargument name="commid" type="numeric" default="#getCommunication().commID#" showInEditor = "false">
		<cfargument name="days" type="numeric" default="30">


		<cfset var theString = application.com.relayelementTree.createSEIDQueryString (linktype = "unrestricted",elementid = elementid, personid = personid, commid = commid, days=days)>
		<cfreturn theString>

	</cffunction>

	<!---
	This creates a link to a page which usually requires a login.
	Clicking on this link allows the user to go to this page (and its parents) without logging in
	 --->

	<cffunction name="SecurePageLink" return="string" output="no" Description="Unrestricted Link to a Secure Page" access="Public" context="TextCommunication">

		<cfargument name="ElementID" type="string" required="true">
		<cfargument name="Days" type="numeric" default="30">
		<cfargument name="additionalParameters" type="string" default="" label="Additional Parameters">

		<cfset var result = "">
		<cfset var extraParams = "">

		<cfset var comm = getCommunication()>
		<cfset var seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "unrestricted",elementid = elementid, personid = getPersonid(), commid = comm.commid, days=days, test = comm.test)>	<!--- WAB LID 2361 added test parameter ---><!--- NJH 2009/10/02 LID 2725 --->

		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters>
		</cfif>

		<cfset result = "#Comm.linktoSiteURLWithProtocol#?#seidQueryString##extraParams#">

		<cfset result = AddCustomTrackingToURL(result)>

		<cfreturn result>

	</cffunction>

	<!--- this function very old and here only for backwards compatibility --->
	<cffunction access="public" name="RelayPageLink" return="string" output="no" active = "false" >

		<cfargument name="elementid" type="string" required="true">
		<cfargument name="additionalParameters" type="string" default="">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">
		<cfargument name="locationid" default="#getLocationid()#" showInEditor = "false">

		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">

		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters>
		</cfif>

		<cfset theString = "&a=e&e=#elementID#">

		<cfset result = createRemoteCFMLink(personid = personid,locationid = locationid) & theString  & extraParams>

		<cfreturn result>

	</cffunction>


	<!--- WAB 2011/06 left this for backwards compatibility, but just uses PortalPageLink --->
	<cffunction access="public" name="RelayPageLinkV2" description="Link to a Page on the Portal"  return="string" output="no" active="false">
		<cfreturn portalPageLink (argumentCollection = arguments)>
	</cffunction>

	<!--- NJH 2011/08/04 - added to support relayElement in the links dialog. --->
	<cffunction name="relayElement" returntype="string" output="false">
		<cfargument name="Elementid" label="Page ID" type="string" required="true">
		<cfargument name="AdditionalParameters" label="Additional URL Parameters" type="string" default="">

		<cfreturn portalPageLink (argumentCollection = arguments)>
	</cffunction>

	<!--- this version by passes remote.cfm and goes straight to the portal--->
	<cffunction name="PortalPageLink" description="Link to a Page on the Portal"  return="string" output="no" access="Public" context="TextCommunication">

		<cfargument name="Elementid" label="Page ID" type="string" required="true">
		<cfargument name="AdditionalParameters" label="Additional URL Parameters" type="string" default="">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">


		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">
		<cfset var comm = getCommunication()>
		<cfset var seidQueryString = "">

		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters>
		</cfif>

<!--- 		<cfset theString = "p=#request.CommItem.magicNumber#&cid=#request.thisComm.commid#&eid=#elementID#">
		<cfset result = "#request.currentSite.httpprotocol##request.thisComm.linkToSiteURL#?#theString##extraParams#">
 --->
		<cfset seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "tracked",elementid = elementid, personid = getPersonID(), commid = Comm.commid,test=comm.test)>  <!--- WAB LID 2361 added test parameter --->
		<cfset result = "#Comm.linktoSiteURLWithProtocol#?#seidQueryString##application.com.security.encryptQueryString(extraParams)#"> <!--- AXA 2014-10-14 Brought in WAB changes from partnerportal, to encrypt querystrings for portal urls --->

		<cfset result = AddCustomTrackingToURL (result)>

		<cfreturn result>
	</cffunction>



	<!--- WAB 2011/11/23 --->
	<cffunction access="public" name="RelayFile" return="string" output="no" description="Download File Link" context="TextCommunication">

		<cfargument name="fileID" type="string" required="true" validvalues="func:com.filemanager.getFilesAPersonHasViewRightsTo(filetypegroupid = 0,returnColumns='f.fileID as [value],f.name as display')"> <!--- Case 436732 --->
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">
		<cfargument name="locationid" default="#getLocationid()#" showInEditor = "false">


		<cfset var result = "">
		<cfset var theString = "">

		<cfif context IS "communication">
			<cfset theString = "&a=d&f=#fileID#">
			<cfset result = createRemoteCFMLink(personid = personid,locationid = locationid,usesession=true) & theString>
		<cfelse>
			<cfset result = "/filemanagement/fileget.cfm?fileid=#fileID#">
		</cfif>


		<cfreturn result>

	</cffunction>



	<!--- 2009-01-05 NYB CR-LEX585 - added function:

		2010/05/17  WAB This function does not seem to work
		because there is not a linkType = AutoLogin in function createSEIDQueryString
		Since the function appears Lexmark specific, and I cannot find any Lexmark phrases with text AutoLoginLink in them , I am not overly worried!
	--->
	<!--- NJH 2011/08/04 - removed as no one seems to know what it does and Lexmark aren't using it anymore.. could possibly be re-instated later, but it tidies this .cfc up with functions that are no longer being used.
	<cffunction name="AutoLoginLink" return="string" output="no">
		<cfargument name="ElementID" type="string" required="true">
		<cfargument name="AdditionalParameters" type="string" label="Additional Parameters" default="">
		<cfargument name="PersonID" default="#getPersonid()#" showInEditor = "false">

		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">
		<cfset var comm = getCommunication()>


		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters>
		</cfif>


		<cfset var seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "autologin",elementid = elementid, personid = getPersonid(), commid = comm.commid, test = comm.test)>	<!--- WAB LID 2361 added test parameter ---><!--- NJH 2009/10/02 LID 2725 --->
		<cfset result = "#Comm.linktoSiteURLWithProtocol#?#seidQueryString##extraParams#">

		<cfreturn result>
	</cffunction>
--->


	<!--- output value of a flagGroup for person/location/org
		WAB 2014-09-04 add support for flags of any entity (reworking some recent YMA changes).
		Have acutually also added support for [[entity.flagGroupTextID]] notation, so this function should be required less often.
	--->

	<cffunction access="public" name="showProfileValue" description="Show Profile/FlagGroup Value" return="string" output="no">
		<cfargument name="ProfileID" type="string" required="true" label="Profile" validValues="select case when len(flagGroupTextId) = 0 then cast(flagGroupID as varchar) else flagGroupTextId end as value, name as display from flagGroup">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false" > <!--- note that this parameter does not make sense for non POL flagGroups--->

		<cfset var flagQuery = "">
		<cfset var flagGroup  = "">
		<cfset var result = "">
		<cfset var entityID = 0>


		<cfif not application.com.flag.doesFlagGroupExist (profileID)>
			<cfthrow message = "Flag Group does not exist:  #htmleditformat(profileID)#" extendedInfo = "#htmleditformat(profileID)#">
		</cfif>

		<cfset flagGroup  = application.com.flag.getFlagGroupStructure(profileID)>
		<cfif flagGroup.isOK>

			<cfif listfindNocase ("person,location,organisation",flaggroup.entityType.tablename) is not 0 and personid is not 0>
				<!--- 	This function was orginally only designed for POL data
						A specific personID could be passed in and the locationid/organisationid would be calculated from it, so need to continue to support this
				--->
				<cfquery name = "flagQuery" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
				select dbo.BooleanFlagList (#flagGroup.flagGroupID#,#flaggroup.entityType.uniquekey#,default) as flagList from person where personid  =  <cf_queryparam value="#personid#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
			<cfelse>
				<!--- for any other entityType we use getEntityID () to get the correct entityid from the merge structure --->
				<cfset entityID = getEntityID (flagGroup.entityType.tableName)>
				<cfquery name = "flagQuery" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
				select dbo.BooleanFlagList (#flagGroup.flagGroupID#,#flaggroup.entityType.uniquekey#,default) as flagList from #flaggroup.entityType.tablename# where #flaggroup.entityType.uniqueKey# =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>

			</cfif>

			<cfset result = flagQuery.flagList>

		</cfif>

		<cfreturn result>

	</cffunction>

		<!--- output value of a flag or person/location/org
			WAB 2014-09-04 add support for flags of any entity (reworking some recent YMA changes).
			Have acutually also added support for [[entity.flagGroupTextID]] notation, so this function should be required less often.
		--->
	<cffunction access="public" name="showProfileAttributeValue" return="string" output="no">
		<cfargument name="ProfileAttributeID" type="string" required="true" label="Profile Attribute" validValues="select case when len(isnull(flagTextId,'')) = 0 then cast(flagID as varchar) else flagTextId end as value, left(flag,100) as display from vflagdef where entityTypeid in (0,1,2)">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false" label="Unique Person ID">

		<cfset var result = "">
		<cfset var temp = "">
		<cfset var flag  = "">

		<cfif not application.com.flag.doesFlagExist (profileAttributeID)>
			<cfthrow message = "Flag does not exist:  #htmleditformat(profileAttributeID)#" extendedInfo = "#htmleditformat(profileAttributeID)#">
		</cfif>

		<cfset flag  = application.com.flag.getFlagStructure(profileAttributeID)>

		<cfif flag.isOK>
			<!--- WAB 2014-09-03 extend functionality to deal with flags for any entity --->
			<!--- 	This function was orginally only designed for POL data and a specific personID could be passed in
					Not sure whether this feature was used much, but probably needs to be kept in
			--->
			<cfif listfindNocase ("person,location,organisation",flag.entityType.tablename) is not 0 and personid is not 0>

				<!--- Special function which deals with POL data --->
				<cfset temp = application.com.flag.getFlagDataForPerson (personid = personid, flagid = flag.flagid)>
			<cfelse>
				<!--- for any other entityType we use getEntityID () to get the correct entityid from the merge structure --->
				<cfset var entityID = getEntityID (flag.entityType.tableName)>

				<cfif entityID is not 0>
					<cfset temp = application.com.flag.getFlagData (entityid = entityid, flagid = flag.flagid)>
				<cfelse>
					<cfreturn "">
				</cfif>

			</cfif>

			<!--- WAB 2015-01-28 Added valueList() because if the flag is 'multiple' then we get more than one row --->
			<cfif isdefined("temp.data_name")>
				<cfset result = valueList(temp.data_name)>
			<cfelseif isdefined("temp.data")>
				<cfset result = valueList(temp.data)>
			<cfelseif temp.recordcount is not 0>
				<cfset result = true>   <!--- not sure what best to bring back here - ideally for booleans should use showProfileValue --->
			<cfelse>
				<cfset result = false>
			</cfif>

		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="TrackedLink" return="string" output="no" description="Tracked Link" access="public" context="TextCommunication">
		<!--- adds correct stuff to end of a remote.cfm link to go to a url--->
		<cfargument name="URL" type="string" required="true">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">
		<cfargument name="locationid" type="numeric" default = "#getLocationid()#" showInEditor = "false">
		<cfargument name="siteURL" type="string" default="#getCommunication().linktoSiteURLWithProtocol#" showInEditor = "false"> <!--- need a getportal function here --->
		<cfargument name="test" type="boolean" default="#getCommunication().test#" showInEditor = "false">
		<cfargument name="commid" type="numeric" default="#getCommunication().commid#" showInEditor = "false">

		<!--- WAB 2012-01-29 CASE 433406 add call to unHTMLEditFormat() to remove any htmlentities from the URL.  These will get added by CKEDITOR (especially changing & to &amp;)--->
		<cfset var theString = "&a=g&u=#urlencodedformat(AddCustomTrackingToURL(application.com.regexp.unHTMLEditFormat(arguments.url)))#">
		<cfset var result = createRemoteCFMLink(personid = personid,locationid = locationid,siteURL=siteURL,test=test,commid = commid) & theString>

		<cfreturn result>

	</cffunction>

	<!--- this is a placeholder function which is overwritten during the communications process --->
	<cffunction name="AddCustomTrackingToURL" return="string" output="no">
		<cfargument name="URL" required="true">
		<cfreturn url >
	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             2006/03/16 - GCC - CR_SNY588 Set Password generatePasswordLink
			Modified by WAB so that can be used in a comm (needs to look for request.CommItem.personid)
	  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" context="emailDef" name="generatePasswordLink" hint="Creates an encrypted string to add to a link to enable a user to set/reset their password" output="no">
		<cfargument name="PersonID" type="numeric" default="#getPersonID()#">
		<cfargument name="Reset" type="boolean" default="true">

		<cfset var generateRandomPassword ="">
		<cfset var getFullPersonDetails ="">
		<cfset var link ="">
		<cfset var encryptedLink = "">

		<cfif isDefined("request.CommItem")>
			<cfset arguments.personid = request.CommItem.personid>
		<cfelseif arguments.personid is "">
			<cfoutput>Function generatePasswordLink: PersonID required</cfoutput><CF_ABORT><!--- actuall this will never be outputted because output is OFF - which it must be --->
		</cfif>


		<cfif arguments.reset>
			<cfset generateRandomPassword = application.com.login.createUserNamePassword(personID=arguments.personID)>
		</cfif>
		<cfset getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID=arguments.personID)>
		<cfset link = arguments.personID & "#application.delim1#" & now() & "#application.delim1#" & getFullPersonDetails.password>
		<!--- 2008-02-21 AJC P-SNY041 encryption functionality extension --->
		<cfset encryptedLink = application.com.encryption.encryptString(string=link)>
		<cfreturn encryptedLink>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            PKP: 2008-02-01 : REWORK GENERATE PASSWORD LINK FOR INTERNAL USERS
							adding site to link that is to be encrypted

			WAB 2011/05/17 The URL encrypted into this link is actually an external URL, rather than an internal URL, so not sure how it is ever used.
			I have changed it to an internal URL
	  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="generateInternalPasswordLink" hint="Creates an encrypted string to add to a link to enable a user to set/reset their password" output="no">
		<cfargument name="PersonID" default="#getPersonid()#">
		<cfargument name="Reset" type="boolean" default="true">

		<cfset var generateRandomPassword ="">
		<cfset var getFullPersonDetails ="">
		<cfset var link ="">
		<cfset var encryptedLink = "">

		<cfif arguments.reset>
			<cfset generateRandomPassword = application.com.login.createUserNamePassword(personID=arguments.personID)>
		</cfif>
		<cfset getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID=arguments.personID)>
		<cfset link = arguments.personID & "#application.delim1#" & now() & "#application.delim1#" & getFullPersonDetails.password& "#application.delim1#" & application.com.relayCurrentSite.getReciprocalInternalDomain()>
		<cfset encryptedLink = application.com.encryption.encryptString(string=link)>
		<cfreturn encryptedLink>
	</cffunction>


	<!--- is this function still used?? - removed it from the merge function picker... NJH 2011/08/04 --->
	<cffunction name="redirectURL" hint="URL setsup a redirect link for a email" output="no">
		<cfargument name="templatepassed" type="string" default = "">
		<cfargument name="urlparams" Type="string" default = "">

		<!---
		This function needs to be deprecated
		<cfargument name="action" type="string" default="i">
		<cfargument name="personid" default="#getPersonid()#">

		<cfset var magicnumber = application.com.login.getMagicNumber(entityID=#personid#,entityTypeID=0)>
		<cfset var encodedtargeturl = templatepassed & '?' & urlparams>

		<cfset var urlpassed = request.currentSite.httpprotocol & request.currentsite.internaldomain & '/remote.cfm?' & 'a=' & action & '&p=' & magicnumber & '&relocateTo='&urlencodedformat(encodedtargeturl)>


		<cfreturn urlpassed>
		--->

		<cfreturn getStartingTemplateLink (page = templatePassed,queryString = urlparams)>
	</cffunction>


	<cffunction name="unsubscribeLink" description="Link to Unsubscribe From All Communications" output="no" access="public" context="TextCommunication">

		<cfset var theFlagStructure = application.com.flag.getFlagStructure("nocommstypeAll")>
		<cfset var theString = "&a=u&fid=#iif(theFlagStructure.isOK,de(theFlagStructure.flagID),de(''))#">
		<cfset var result = createRemoteCFMLink(useSession=true) & theString>

		<cfreturn result>

	</cffunction>

	<!--- NJH 2011/08/03 - added for the 'unsubscribe' link... left unsubscribeLink for backwards compatibility
		WAB 2011/11/16 - removed, actually causes an error because variables.unsubscribe exists
						- we will need to update any phrases containing [[unsubscribe (just a few on Lenovo)

	<cffunction name="unsubscribe" description="Link to Unsubscribe From All Communications" access="package" showInEditor = "false" output="no">

		<cfreturn unsubscribeLink()>

	</cffunction>
	--->

	<!---
		NJH 2011/08/03 - added for the unsubscribeThisType link, which looks for a function.
		set as access as package so that it is found in the method, but not shown in the merge functions list, (show functions with access="public")
		WAB 2011/11/16 changed name from unsubscribeThisType so did not clash with variable of same name
		--->
	<cffunction name="unsubscribeThisTypeLink" description="Link to Unsubscribe From This Type of Communication" output="no" access="public" context="TextCommunication">
		<cfargument name="commtypelid" type="numeric" default=#getCommunication().commtypelid# showInEditor = "false">

		<cfset var theFlagStructure = application.com.flag.getFlagStructure("nocommstype#arguments.commtypelid#")>
		<cfset var theString = "&a=u&fid=#iif(theFlagStructure.isOK,de(theFlagStructure.flagID),de(''))#">
		<cfset var result = createRemoteCFMLink(useSession=true) & theString>

		<cfreturn result>

	</cffunction>

	<!--- function added for the resend password link in the link dialog --->
	<cffunction name="resendPassword" description="Link to reset a user's password" output="no">
		<cfargument name="PersonID" type="numeric" default="#getPersonID()#">
		<cfargument name="Reset" type="boolean" default="true">
		<cfargument name="ElementID" type="string" default="resend_password">

		<cfreturn "#getPartnerPortalURL()#/index.cfm?eid=#arguments.elementID#&spl=#generatePasswordLink(argumentCollection=arguments)#">

	</cffunction>


	<cffunction name="getStartingTemplateLink" description="Create SSO Link to Specific Page on Internal Site" output="no">
		<cfargument name="Page" required="true">
		<cfargument name="QueryString" default = "">

		<cfreturn application.com.login.createStartingTemplateLink(startingTemplate=page,personid = getPersonid(),queryString = queryString )>

	</cffunction>


	<cffunction access="public" name="setLocalVariable" output="no" context="element">
		<cfargument name="Name" type="string" size="30" >
		<cfargument name="Value" type="string" size="30">

		<cfset variables[name] = value>
		<cfset variables.merge[name] = value>

		<cfreturn "">

	</cffunction>


	<cffunction name="getPartnerPortalName" output="no">
		<cfargument name="Site" default =  "">	  <!--- name of the site, or sitedefid, or if blank we just get the first external site --->

		<cfreturn application.com.relaycurrentSite.getExternalSite(site=site).title>

	</cffunction>

	<cffunction name="getInternalSiteName" output="no">
		<cfargument name="Site" default =  "">	  <!--- usually only one internal site in fact so slightly irrelevant --->

		<cfreturn application.com.relaycurrentSite.getInternalSite(site=site).title>

	</cffunction>

	<cffunction name="getSiteName" description="Returns the name of the current site" output="no">
		<cfreturn request.currentSite.Name>
	</cffunction>

	<cffunction name="getPartnerPortalURL" output="no">
		<cfargument name="Site" default =  "">	  <!--- name of the site, or sitedefid, or if blank we just get the first external site --->

		<cfset var result = "">

		<cfif structKeyExists (variables,"communication")>
			<cfset result = variables.communication.linktoSiteURLWithProtocol>
		<cfelse>
			<cfset result = application.com.relaycurrentSite.getExternalSite(site=site).protocolAndDomain>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getInternalSiteURL" output="no">
		<cfargument name="Site" default =  "">

		<cfreturn application.com.relaycurrentSite.getInternalSite(site=site).protocolAndDomain>

	</cffunction>

	<cffunction name="getSiteURL" hint="Gets URL of current site (includes http protocol)" output="no">

		<cfreturn request.currentSite.protocolAndDomain>

	</cffunction>

	<!--- START: 2010/11/11 AJC P-LEN022 CR011 Add view online link function --->
	<cffunction name="RelayCommunicationLink" return="string" output="no" description = "Link to View Online Version of Communication" access="Public" context="TextCommunication">
		<cfargument name="personid" default="#getPersonid()#" showInEditor = "false">
		<cfargument name="locationid" type="numeric" default = "#getLocationid()#" showInEditor = "false">
		<cfargument name="siteURL" type="string" default="#getCommunication().linktoSiteURLWithProtocol#" showInEditor = "false"> <!--- need a getportal function here --->
		<cfargument name="test" type="boolean" default="#getCommunication().test#" showInEditor = "false">
		<cfargument name="commid" type="numeric" default="#getCommunication().commid#" showInEditor = "false">

		<cfset var theString = "&a=c">

		<cfset var result = createRemoteCFMLink(personid = personid,locationid = locationid,siteURL=siteURL,test=test,commid = commid, useSession=true) & theString >

		<cfreturn result>

	</cffunction>
	<!--- END: 2010/11/11 AJC P-LEN022 CR011 Add view online link function --->


	<cffunction name="getSetting" access="public" output="false" returnType="string" context="element">  <!--- WAB 2013-05-15 CASE 434088 switched off from displaying in Comms, can't think of a use and confusing to users --->
		<cfargument name="setting" required="true" type="string">

		<cfreturn application.com.settings.getSettingForMergeField(variableName=arguments.setting)>
	</cffunction>


	<!--- WAB 2013-02-26 Added this function (code comes from relayTags\relay_rewardsBalance)
		Probably not used in earnest anywhere, but problems appeared in testing
	--->
	<cffunction name="rewardsBalance"  output="false">
		<cfargument name="AccountType"  default= "person">
		<cfargument name="personid" default="#getPersonid()#">
		<cfargument name="organisationid" default="#getOrganisationid()#">

		<cfset qPointsBalance = 0>
		<cfset var endOfCurrentMonthBalanceDate = createdate(year(now()), month(now()), daysinmonth(now()))>
		
		<cfif (AccountType is "person")>
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,OrganisationID,PersonID)>
		<cfelse>
			<cfset qPointsBalance = application.com.relayIncentive.PointsBalance(endOfCurrentMonthBalanceDate,OrganisationID)>
		</cfif>

		<cfset var result = NumberFormat(qPointsBalance.balance,"9")>
		<cfif result is "">
			<cfset result = 0>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="encryptVariableValue" access="public" output="false" returnType="string" hint="Function to encrypt a variable value">
		<cfargument name="name" required="true" type="string" hint="The name of the variable">
		<cfargument name="value" required="true" type="string" hint="The value of the variable">

		<cfreturn application.com.security.encryptVariableValue(argumentCollection = arguments)>
	</cffunction>
	
	<!--- 2015/02/23	YMA	Approval Engine, give a way to bring in the current approvers response --->
	<cffunction access="public" name="getCurrentApproversReason" description="Show the reason text the latest aprover gave during an approval engine approval" return="string" output="no">
		<cfargument name="approvalEngineID" required="true" label="Approval Engine" validValues="select distinct approvalEngineID as value, title as display from vApprovalEngine" showInEditor = "true">
		
		<cfset var currentApproversReason  = "">
		<cfset var result = "">
		<cfset var approvalEngineEntity = "">
		
		<cfquery name = "approvalEngineEntity" DATASOURCE="#application.SiteDataSource#">
			select distinct approvalEngineEntityType as entityType from vapprovalengine where approvalEngineID  =  <cf_queryparam value="#arguments.approvalEngineID#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>
		
		<cfif approvalEngineEntity.recordcount gt 0>
			<cfquery name = "currentApproversReason" DATASOURCE="#application.SiteDataSource#">
				select top 1 reason from approvalstatus where approvalengineID  =  <cf_queryparam value="#arguments.approvalEngineID#" CFSQLTYPE="cf_sql_integer" > and entityID =  <cf_queryparam value="#getEntityID(entityType=approvalEngineEntity.entityType)#" CFSQLTYPE="CF_SQL_INTEGER" >  order by created desc
			</cfquery>
			
			<cfif currentApproversReason.recordcount gt 0>
				<cfset result = currentApproversReason.reason>
			</cfif>
		</cfif>
		
		<cfreturn result>

	</cffunction>
	
</cfcomponent>

