<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		jasperServer.cfc
Author:			NJH
Date started:	15-02-2012

Description:	Functions for Jasper server

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012/01/18 - RMB - P-LEN24 - CR066 Added validvalues = true and VAR'ed the cfset's in "getReportsQuery"
2012/02/15			NJH			CASE 426198: When looking for a join tree to apply the profile changes to, search for nodes of type jdbcTable and jdbcQuery
2012/02/23			NJH			CASE 426198: xmlFormat xml names and changed the looking for a join tree to look for tablename followed by a '.'
2012/05/30			WAB			Jasperserver 451 changes.  Remove all spaces from the joinstring XML
2012-08-21 			PPB 		Case 428959 query for integer flags needed attention
2012-10-01 			PPB 		Case 430511 explicitly qualify the "path" to the function (ie application.com.jasperServer.) was reqd in getDashboards() so done elsewhere too
2012-10-04  		WAB			CASE 431042 delete roles which user no longer has
2012-10-30  		WAB			Removed lots of unnecessary htmleditformats
2012-10-30  		WAB			CASE 431515 fix a join getting the case wrong
2012/11/28			YMA			CASE 432282 applyFlagGroupToXML() now casts integer as integer when not validvalue list
2013-11-20			YMA			CASE 437921 Fix ambiguous column name error
2014-01-22 			WAB 		CASE 438676 fix http/https issues when behind the loadbalancer
2014-08-05			REH			CASE 441207 defined correct data type for dates
2014-12-19			AHL			Case 442967 Phrases not being Resolved in Report Designer
2015-01-16			AHL			Case 442666 Opportunity ID not displaying in Reports
2015-11-02          DAN         Case 446246 - fix for report designer errors, use java "float" type
2016-01-26 			WAB			BF-382 Change algorithm for generating report server url
2016-10-21			atrunov		Case https://relayware.atlassian.net/browse/SSPS-49, integrated User Group Rule for fund account into existing functionality
2016-10-18			WAB			PROD2016-1356 Minor change adding some queryparams to getReportsQuery() and some other queries.  
								Simultaneous major change is actually to the performance of dbo.getFolderPermissions function (see jasperserver repository)

Possible enhancements:

--->


<cfcomponent>

	<cfset principalObjectClass = {user = "com.jaspersoft.jasperserver.api.metadata.user.domain.impl.hibernate.RepoUser"}>


	<cffunction access="public" name="initialise" output="yes">
		<cfargument name="applicationScope" default="#application#">

		<cfset var tentantID = getTenantID(applicationScope = applicationScope)>
		<cfset var getClientName = "">
		<cfset var updateTenantName = "">
		<cfset var isReportManagerOn = "">

		<cfquery name="getClientName" datasource="#applicationScope.siteDataSource#">
			select value from settings where name = 'theClient.clientName'
		</cfquery>

		<cfquery name="isReportManagerOn" datasource="#applicationScope.siteDataSource#">
			select 1 from relaymodule where moduleTextID = 'ReportManager' and active = 1
		</cfquery>

		<cfif getClientName.recordCount and isReportManagerOn.recordCount>
			<cfquery name="updateTenantName" datasource="jasperServer">
				update jiTenant set tenantName =  <cfqueryparam value="#getClientName.value#" CFSQLTYPE="CF_SQL_VARCHAR" > <!--- WAb 2012-07-06 changed to use CFQUERYPARAM rather than CF_QUERYPARAM.  This function sometimes called remotely when application scope not available and cf_queryparam breaks --->
				where tenantName = tenantID
					and tenantID =  <cfqueryparam value="#tentantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
		</cfif>
	</cffunction>


	<cffunction name="getDomainXML" access="public">
		<cfargument name="domainID" type="numeric" required="false">
		<cfargument name="domainXML" type="xml" required="false">

		<cfset var thisDomainXML = "">
		<cfset var domainXMLStruct = structNew()>

		<cfif not structKeyExists(arguments,"domainXML") and structKeyExists(arguments,"domainID")>
			<cfset domainXMLStruct = readResourceXML(id=arguments.domainID)>
			<cfif structKeyExists(domainXMLStruct,"xml")>
				<cfset thisDomainXML = domainXMLStruct.xml>
			</cfif>
		<cfelseif structKeyExists(arguments,"domainXML")>
			<cfset thisDomainXML = arguments.domainXML>
		</cfif>

		<cfreturn thisDomainXML>
	</cffunction>


	<cffunction name="getEntityTablesFromDomain" access="public">
		<cfargument name="domainID" type="numeric" required="false">
		<cfargument name="domainXML" type="xml" required="false">
		<cfargument name="entityTableName" type="string" required="true">
		<cfargument name="tableID" type="string" required="false">

		<cfset var thisDomainXML = "">
		<cfset var tablesArray = arrayNew(1)>
		<cfset var tableSearchString = "not (contains(@id,'JoinTree'))">

		<cfset thisDomainXML = getDomainXML(argumentCollection=arguments)>

		<cfif thisDomainXML neq "">
			<cfif structKeyExists(arguments,"tableID") and arguments.tableId neq "">
				<cfset tableSearchString = "(@id = '#arguments.tableID#')">
			</cfif>
			<cfset tablesArray = xmlsearch (thisDomainXML,"//*[local-name()='jdbcTable'][translate(@tableName, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='dbo.#lcase(arguments.entityTableName)#' and #tableSearchString#]")>
		</cfif>

		<cfreturn tablesArray>

	</cffunction>


	<cffunction name="getJoinTreeForTable" access="public">
		<cfargument name="domainXML" type="xml" required="true">
		<cfargument name="tableID" type="string" required="true">

		<!--- NJH 2012-02-15 CASE 426198: search both jdbcQuery or jdbcTable nodes --->
		<cfset var findFieldNode = xmlsearch (arguments.domainXML,"//*[local-name()='jdbcTable']/*[local-name()='fieldList']/*[local-name()='field'][contains(translate(@id, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'#lcase(arguments.tableID)#.')] | //*[local-name()='jdbcQuery']/*[local-name()='fieldList']/*[local-name()='field'][contains(translate(@id, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'#lcase(arguments.tableID)#.')]")>
		<cfif arrayLen(findFieldNode)>
			<cfreturn getJoinTreeFromFieldNode(fieldNode=findFieldNode[1])>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>


	<cffunction name="getItemGroupForTable" access="public" hint="Returns the itemGroup that a particular tableId is in. Returns the first found.">
		<cfargument name="domainXML" type="xml" required="true">
		<cfargument name="tableID" type="string" required="true">
		<cfargument name="joinTreeID" type="string" required="true">

		<cfset var itemGroupsArray = xmlsearch(arguments.domainXML,"//*[local-name()='itemGroups']/*[local-name()='itemGroup']/*[local-name()='items']/*[local-name()='item'][contains(translate(@resourceId,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'#lcase(arguments.joinTreeID)#.#lcase(arguments.tableID)#.')]")>

		<cfif arrayLen(itemGroupsArray)>
			<cfreturn itemGroupsArray[1].xmlParent.xmlParent>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>


	<cffunction name="getItemGroupForJoinTree" access="public" hint="Returns the first item group with a resource Id of the given joinTree.">
		<cfargument name="domainXML" type="xml" required="true">
		<cfargument name="joinTreeID" type="string" required="true">

		<cfset var itemGroupArray = xmlsearch(arguments.domainXML,"//*[local-name()='itemGroups']/*[local-name()='itemGroup'][@resourceId='#arguments.joinTreeID#']")>
		<cfif arrayLen(itemGroupArray)>
			<cfreturn itemGroupArray[1]>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>

	<cffunction name="getJoinTreeFromFieldNode" access="public">
		<cfargument name="fieldNode" type="xml" required="true">

		<cfreturn arguments.fieldNode.xmlParent.xmlParent.xmlAttributes.id>
	</cffunction>


	<cffunction name="getDataSourceForTable" access="public">
		<cfargument name="domainXML" type="xml" required="true">
		<cfargument name="tableID" type="string" required="true">

		<cfset var findFieldNode = "">

		<cfset var jdbcTableNode = xmlsearch(arguments.domainXML,"//*[local-name()='jdbcTable'][@id='#arguments.tableID#']")>
		<cfif arrayLen(jdbcTableNode)>
			<cfreturn jdbcTableNode[1].xmlAttributes.datasourceID>
		<cfelse>
			<cfreturn "">
		</cfif>
	</cffunction>


	<cffunction name="hasProfileBeenAppliedToDomain" type="public" returntype="boolean">
		<cfargument name="domainID" type="numeric" required="false">
		<cfargument name="domainXML" type="xml" required="false">
		<cfargument name="flagGroupID" type="numeric" required="true">
		<cfargument name="tableId" type="string" required="true">

		<cfset var thisDomainXML = getDomainXML(argumentCollection=arguments)>
		<cfset var flagGroupTableAlias = "">
		<cfset var checkForFlagGroupID = "">

		<cfif thisDomainXML neq "">
			<cfset flagGroupTableAlias = getFlagGroupTextIDForDomain(flagGroupID=arguments.flagGroupId,tableID=tableID)>

			<cfset checkForFlagGroupID = xmlsearch(thisDomainXML,"//*[local-name()='itemGroups']/*[local-name()='itemGroup'][@id='set_#flagGroupTableAlias#']")>
			<cfif arrayLen(checkForFlagGroupID) neq 0>
				<cfreturn true>
			</cfif>
		</cfif>

		<cfreturn false>
	</cffunction>


	<cffunction name="readResourceXML">
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="isBackUp" type="boolean" default="false">

		<cfset var result = {isOK = true,message=""}>
		<cfset var getResource = "">
		<cfset var bytearray = "">
		<cfset var xmlString = "">

		<cfif not arguments.isBackUp>
			<cfquery name="getResource" datasource="jasperserver">
				select * from
				jiFileResource
				where id = #arguments.id#
			</cfquery>
		<cfelse>
			<cfquery name="getResource" datasource="jasperserver">
				select * from
				jiFileResourceBackUp
				where backup_id = #arguments.id#
			</cfquery>
		</cfif>

		<cfif getResource.recordcount is not 0>
			<cftry>
				<cfset bytearray = getResource.data>
				<cfset xmlString = CharsetEncode(bytearray,"utf-8")>
				<cfset result.xml = xmlparse (xmlString)>
				<cfcatch>
					<cfset result.isok = false>
					<cfset result.message = cfcatch.message&" "&cfcatch.detail>
				</cfcatch>
			</cftry>
		<cfelse>
			<cfset result.isok = false>
			<cfset result.message = "The resource does not exist">
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="writeResourceXML">
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="XML" type="xml" required="true">

		<cfset var xmlString = toString(arguments.XML)>
		<cfset var binaryArray = charsetDecode(xmlstring,"utf-8")>
		<cfset var hexString = BinaryEncode(binaryArray,"hex")>
		<cfset var updateResource = "">

		<!--- backups are stored in jiFileResourcebackUp via a trigger --->
		<cftry>
			<cftransaction action="begin">
			<cfquery name="updateResource" datasource="jasperserver">
				update
				jifileresource
				set data = 0x#hexString#
				where id = #arguments.id#
			</cfquery>

			<cftransaction action="commit">
			<cfcatch>
				<cftransaction action="rollback">
			</cfcatch>
		</cftry>

		<cfreturn true>
	</cffunction>


	<cffunction name="writeResourceXMLFromBackup">
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="backupId" type="numeric" required="true">

		<cfset var updateResource = "">

		<!--- backups are stored in jiFileResourcebackUp via a trigger --->
		<cftry>
			<cftransaction action="begin">

			<cfquery name="updateResource" datasource="jasperserver">
				if exists(select 1 from jiFileResourcebackUp where backUp_id=#arguments.backUpId#)
				update
					jifileresource
						set data = b.data
					from jifileresource f
						inner join jiFileResourcebackUp b on f.id = b.id
					where
						b.backUp_id = #arguments.backUpID#
						and f.id = #arguments.id#
			</cfquery>

			<cftransaction action="commit">
			<cfcatch>
				<cftransaction action="rollback">
			</cfcatch>
		</cftry>

		<cfreturn true>
	</cffunction>

	<cffunction name="getReportsQuery" access="public" returntype="query" hint="Returns a query of reports" validvalues="true">
		<cfargument name="restrictByCurrentUserRights" type="boolean" default="true">
		<cfargument name="showInternalExternal" type="string" default="" hint="Get internal or external dashboards">
		<cfargument name="module" type="string" default="">
		<cfargument name="URI" type="string" default=""> <!--- to filter by URI --->
		<cfargument name="orderby" type="string" default="[module],grouping">

		<cfset var tenantURI = application.com.jasperServer.getTenantRecord().tenantFolderUri>		<!--- 2012-10-01 PPB Case 430511 explicitly qualify the "path" to the function (ie application.com.jasperServer.) --->
		<cfset var reports = queryNew("Module,hasModule,Id,label,name,parentURI,reportURI,_module,grouping")>
		<cfset var userID = getJasperServerUserIDForCurrentUser()>
		<cfset var resourceTypes = "">

		<cfif arguments.showInternalExternal eq "external">
			<cfset resourceTypes = "portal_resources">
		<cfelseif arguments.showInternalExternal eq "internal">
			<cfset resourceTypes = "internal_resources">
		</cfif>

		<!---NJH Case 436800 2013/09/04 - put a try/catch so that if jasperserver is not set up, select boxes will still work but will be empty.. --->
		<cftry>
			<cfquery name="reports" datasource ="jasperserver">
			select
				isNull(_module,'NoModule') as Module,
				case when _module is null then 0 else 1 end as hasModule,
				*
			from (
				select ru.id as id, r.label as label, r.name,folder.uri as parentURI,
					replace(folder.uri,<cf_queryparam value="#tenantURI#" CFSQLTYPE="CF_SQL_VARCHAR" >,'')+'/'+r.name as reportURI,
						case
							when dbo.getParameterValue(r.description,'module',' ') is not null then dbo.getParameterValue(r.description,'module',' ')
							when folder.uri like '%/event_manager%' then 'Events'
							when folder.uri like '%/incentive_manager%' then 'Incentive'
							when folder.uri like '%/communications_manager%' then 'Communicate'
							when folder.uri like '%/training_manager%' then 'eLearning'
							when folder.uri like '%/fund_manager%' then 'Funds'
							when folder.uri like '%/opportunity_manager%' then 'LeadManager'
							when folder.uri like '%/Scorecard_Manager%' then 'Scorecard'
							when folder.uri like '%/Sales_Out_Data%' then 'SalesOutData'
							when folder.uri like '%/file_library%' or folder.uri like '%/Partner_Accounts%' or folder.uri like '%/Partner_Portal%' or folder.uri like '%/System_Activity%' then 'Content'
							else null end as _Module,
					dbo.getParameterValue(r.description,'grouping',' ') as grouping
				from JIReportUnit ru
					inner join JIResource r on ru.id=r.id
					left outer join JIDataDefinerUnit dd on ru.id=dd.id
					left outer join JIAdhocReportUnit ahRU on ru.id=ahRU.id
					inner join JIResourceFolder folder on r.parent_folder=folder.id
					<cfif arguments.restrictByCurrentUserRights>
						inner join dbo.getFolderPermissions(<cf_queryparam value="#userID#" CFSQLTYPE="CF_SQL_INTEGER" >) p on p.uri = folder.uri and allowed=1
					</cfif>
				where
				folder.uri  like  <cf_queryparam value="#tenantURI#%/#resourceTypes#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				and folder.hidden=0
				and folder.name != 'topics'
				<cfif URI is not "">
				and replace(folder.uri,<cf_queryparam value="#tenantURI#" CFSQLTYPE="CF_SQL_VARCHAR" >,'')+'/'+r.name =  <cf_queryparam value="#URI#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				) as x
				where 1=1
				<cfif arguments.module is not "">
				and (_module =  <cf_queryparam value="#arguments.module#" CFSQLTYPE="CF_SQL_VARCHAR" > )
				</cfif>
				order by
					<cf_queryObjectName value="#orderby#">
			</cfquery>

			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer",Severity="error",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>

		<cfreturn reports>
	</cffunction>

	<cffunction name="getReportIDFromURI" >
		<cfargument name="reportURI">

		<cfreturn getReportsQuery(uri = reportURI).id>
	</cffunction>

	<!--- Outputs List Of reports from Query returned by getReportsQuery --->
	<cffunction name="outputReportsQuery" >
		<cfargument name="reportsQuery">

		<cfset var class ="">
		<cfset var functionname = "">

		<cf_includeJavascriptOnce template = "/javascript/openWin.js">
		<cf_includeJavascriptOnce template = "/javascript/myRelay.js">
		<cf_includejavascriptonce template="/javascript/extExtension.js" >


		<UL>
		<cfoutput query="arguments.reportsQuery" group="module">
			<LI><b>phr_mod_#htmleditformat(replace(module,"'","","ALL"))#</b>
				<UL>
				<cfoutput group="grouping">
					<LI><b>#htmleditformat(grouping)#</b>
					<UL>
					<cfoutput>
						<LI>
							<cfset class="notIsFavorite">
							<cfset functionName = "addToFavourites">
							<cfif application.com.myRelay.isLinkInMyLinks(linkType="jasperReport",location=reportURI)>
								<cfset class="isFavorite">
								<cfset functionName = "removeFromFavourites">
							</cfif>
							<a class="#class#" href="##" linkType="jasperReport" linkAttribute1="#module#" linkLocation="#reportURI#" linkName="#label#" onClick="#functionName#(this);" title="phr_sys_myLinks_AddToMyLinks"></a>
							<!--- <cfif not hasModule> --->
							<a href="javascript: void(openWin('/jasperserver/setModuleAndGroup.cfm?reportURI=#reportURI#&description=#urlEncodedFormat(label)#','#JSStringFormat(label)#','width=570,height=370,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_reports_SetModule"><img src="/images/icons/layout_edit.png" border="0"/></a>
							<!--- <cfelse>&nbsp;&nbsp;&nbsp;
							</cfif> --->
							<a href="javascript:void(openNewTab('#JSStringFormat(label)#','#JSStringFormat(label)#','#getJasperServerReportUnitPath()##reportURI#',{showLoadingIcon:true,iconClass:'#lcase(module)#',tabTip:'#jsStringFormat(label)#'}));">#htmleditformat(label)#</a>
						</LI>
					</cfoutput>
					</UL>
					</LI>
				</cfoutput>
				</UL>
			</LI>
		</cfoutput>
		</UL>

	</cffunction>

	<cffunction name="setReportParameter" access="public" returntype="boolean" hint="sets a module attribute on a report">
		<cfargument name="reportID" type="string" required="false">
		<cfargument name="parameter" type="string" required="false">
		<cfargument name="value" type="string" required="false">

		<cfset var setModule="">

		<cfquery name="setModule" datasource ="jasperserver">
		declare @parameter varchar(50),@newValue varchar(50), @oldValue varchar(50)
		select @newValue =  <cf_queryparam value="#value#" CFSQLTYPE="CF_SQL_VARCHAR" > , @parameter =  <cf_queryparam value="#parameter#" CFSQLTYPE="CF_SQL_VARCHAR" >

		select
			@oldValue = dbo.getParameterValue(description,@parameter,' ')
		from
			JIResource
		where id =  <cf_queryparam value="#reportID#" CFSQLTYPE="CF_SQL_INTEGER" >


		IF @oldValue is Null
			Update JIresource Set description = isNull(description,'') + '  ' + @parameter + '="' + @newValue + '"' 		where id =  <cf_queryparam value="#reportID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ELSE
			Update JIresource Set description = replace (isNull(description,''), @parameter + '="' + @oldValue + '"', @parameter + '="' + @newValue + '"') 		where id =  <cf_queryparam value="#reportID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn true>
	</cffunction>

	<cffunction name="getDomainsQuery" access="public" returntype="query" hint="Returns a query of domains">
		<cfargument name="domainName" type="string" required="false">
		<cfargument name="domainID" type="string" required="false">
		<cfargument name="restrictByCurrentUserRights" type="boolean" default="false">
		<cfargument name="excludePermissionMasks" type="string" default=""> <!--- NJH - list of permission masks to exclude. It is an attempt to filter out domains that are execte only.... I don't think it's the best method, but it appears to work at the moment --->

		<cfset var tenantURI = application.com.jasperServer.getTenantRecord().tenantFolderUri>		<!--- 2012-10-01 PPB Case 430511 explicitly qualify the "path" to the function (ie application.com.jasperServer.) --->
		<cfset var domainsQry = "">
		<cfset var getAccessibleDomains = "">
		<cfset var userID = getJasperServerUserIDForCurrentUser()>

		<cfquery name="domainsQry" datasource="jasperserver">
			select distinct d.id,r.name,r.label,r.version,r.description, fr.id as schemaID,
				folder.uri
			from jidomaindatasource d inner join jiresource r
				on d.id = r.id inner join jifileresource fr
				on d.[schema_id] = fr.id inner join jiresourcefolder folder
				on r.parent_folder = folder.id
				<cfif arguments.restrictByCurrentUserRights>
					inner join dbo.getFolderPermissions(<cf_queryparam value="#userID#" CFSQLTYPE="CF_SQL_INTEGER" >) p on p.uri = folder.uri
				</cfif>
				<cfif arguments.excludePermissionMasks neq "">
					left join vobjectPermission v on v.uri = folder.uri+'/'+r.name and v.userId=<cf_queryparam value="#userID#" CFSQLTYPE="CF_SQL_INTEGER">
				</cfif>
			where
				folder.uri  like  <cf_queryparam value="#tenantURI#/%" CFSQLTYPE="CF_SQL_VARCHAR" >
				and folder.uri not like '%org_template%'
				and folder.hidden=0
				<cfif structKeyExists(arguments,"domainName")>and r.name  in ( <cf_queryparam value="#arguments.domainName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )</cfif>
				<cfif structKeyExists(arguments,"domainID")>and fr.id  in ( <cf_queryparam value="#arguments.domainID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				<cfif arguments.excludePermissionMasks neq "">
				and (v.permissionMask is null or v.permissionMask not in (<cf_queryparam value="#arguments.excludePermissionMasks#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">))
				</cfif>
		</cfquery>

		<cfreturn domainsQry>
	</cffunction>


	<cffunction name="deleteFlagGroupFromXML">
		<cfargument name="XML" type="xml" required="true">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="domainID" type="numeric" required="true">

		<cfset var deleteFlagGroupDomain = "">
		<cfset var result = {isOK = true,message=""}>
		<cfset var domainTextID=getDomainsQuery(domainID=arguments.domainID).name>

		<cftry>
			<cfset result = applyFlagGroupToXML(xml=arguments.xml,flagGroupID=arguments.flagGroupID,delete = true)>
			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message & " "&cfcatch.detail>
			</cfcatch>
		</cftry>

		<cfif result.isOK>
			<cfquery name="deleteFlagGroupDomain" datasource="#application.siteDataSource#">
				delete from flagGroupDomain where flagGroupId =  <cf_queryparam value="#arguments.flagGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >
					and domainTextID =  <cf_queryparam value="#domainTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="applyFlagGroupsToDomains" access="public" hint="Look at the flagGroupDomain table and apply flagGroups to the domain. Used after a domain has been overwritten." ret>
		<cfargument name="flagGroupID" type="numeric" required="false">
		<cfargument name="refreshFlagGroup" type="boolean" default="true">

		<cfset var getDomainsForFlagGroup = "">
		<cfset var domainID = 0>
		<cfset var domainXML = structNew()>
		<cfset var result = {isOK = true,message=""}>
		<cfset var addFlagGroup = "">
		<cfset var success = true>
		<cfset var message = "">
		<cfset var successMessage = "Successfully applied the following profiles to the following domains:<br>">
		<cfset var failureMessage = "Failed to apply the following profiles to the following domains:<br>">

		<cfquery name="getDomainsForFlagGroup" datasource="#application.siteDataSource#">
			select fgd.flagGroupID,fgd.domainTextID,fg.flagGroupTextId,fgd.domainTableID
			from flagGroupDomain fgd
				inner join flagGroup fg on fgd.flagGroupID = fg.flagGroupID
			where 1=1
			<cfif structKeyExists(arguments,"flagGroupId")>and fgd.flagGroupID = #arguments.flagGroupID#</cfif>
		</cfquery>

		<cfloop query="getDomainsForFlagGroup">
			<cfset success = false>
			<cfset domainID = getDomainsQuery(domainName=domainTextID).schemaID>

			<cftry>
				<cfif domainID neq "">
					<cfset domainXML = readResourceXML(id=domainID)>
					<cfif domainXML.isOK>
						<cfset addFlagGroup = addFlagGroupToXML(domainXML=domainXML.xml,flagGroupID=flagGroupID,domainID=domainID,refreshFlagGroup=arguments.refreshFlagGroup,tableId=domainTableID)>
						<cfset success = addFlagGroup.isOK>
						<cfset message = addFlagGroup.message>
					<cfelse>
						<cfset message = "Unable to read domain resource xml.">
					</cfif>
				<cfelse>
					<cfset message = "Unable to find domain resource xml for domain '#domainTextID#'.">
				</cfif>

				<cfcatch>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="JasperServer Apply Profiles to Domains",Severity="error",catch=cfcatch,WarningStructure=arguments)>
					<cfset message = "Unable to apply profile #flagGroupID# to domain '#domainTextID#'.">
				</cfcatch>
			</cftry>

			<cfif not success>
				<cfset result.isOK = false>
				<cfset failureMessage = failureMessage&"#flagGroupTextId# - #domainTextID# (#domainTableID#) "&IIF(message neq "",DE("(#message#)"),DE(""))&"<br>">
			<cfelse>
				<cfset successMessage = successMessage&"#flagGroupTextId# - #domainTextID# (#domainTableID#)<br>">
			</cfif>
		</cfloop>

		<cfif getDomainsForFlagGroup.recordCount>
			<cfset result.message = successMessage &"<br><br>"&failureMessage>
		<cfelse>
			<cfset result.message = "No profile customisations to apply.">
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="addFlagGroupToXML" hint="Adds a flagGroup to a domainXML" returntype="struct">
		<cfargument name="domainXML" type="xml" required="false">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="domainID" type="numeric" required="true">
		<cfargument name="refreshFlagGroup" type="boolean" default="true">
		<cfargument name="tableID" type="string" required="true">

		<cfset var addFlagGroupDomain = "">
		<cfset var result = {isOK = true}>
		<cfset var domainTextID=getDomainsQuery(domainID=arguments.domainID).name>
		<cfset var thisDomainXML = "">
		<cfset var resourceXML = "">
		<cfset var success=true>
		<cfset var addProfileToDomain = "">
		<cfset var warningStruct = structNew()>

		<cfset thisDomainXML = getDomainXML(argumentCollection=arguments)>

		<cfif isXML(thisDomainXML)>
			<cftry>
				<cfset result = applyFlagGroupToXML(xml=thisDomainXML,flagGroupID=arguments.flagGroupID,refreshFlagGroup=arguments.refreshFlagGroup,tableId=arguments.tableId)>
				<cfif result.isOk and structKeyExists(result,"xml")>
					<cfset success = writeResourceXML(id=arguments.domainID,xml=result.xml)>

					<cfif success>

						<cfquery name="addProfileToDomain" datasource="#application.siteDataSource#">
							if not exists(select 1 from flagGroupDomain where flagGroupId =  <cf_queryparam value="#arguments.flagGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >  and domainTextId =  <cf_queryparam value="#domainTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and domainTableId =  <cf_queryparam value="#arguments.tableId#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							insert into flagGroupDomain(flagGroupID,domainTextId,domainTableID,createdBy,lastUpdatedBy)
								values (<cf_queryparam value="#arguments.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#domainTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.tableID#" CFSQLTYPE="CF_SQL_VARCHAR" >,#request.relayCurrentUser.userGroupId#,#request.relayCurrentUser.userGroupId#)

							update flagGroupDomain set lastUpdatedBy=#request.relayCurrentUser.userGroupId#,lastUpdated=getDate()
								where flagGroupId =  <cf_queryparam value="#arguments.flagGroupId#" CFSQLTYPE="CF_SQL_INTEGER" >  and domainTextId =  <cf_queryparam value="#domainTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and domainTableId =  <cf_queryparam value="#arguments.tableId#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfquery>
					</cfif>
				</cfif>

				<cfcatch>
					<cfset result.isOk = false>
					<cfset result.message = cfcatch.message & " "&cfcatch.detail>
					<cfif application.testSite eq 2>
						<Cfdump var="#cfcatch#">
						<CF_ABORT>
					</cfif>
					<!--- <cfset warningStruct = duplicate(arguments)>
					<cfif not structKeyExists(arguments,"domainXML")>
						<cfset warningStruct.domainXML = thisDomainXML>
					</cfif>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Report Designer addFlagGroupToXML",Severity="error",catch=cfcatch,WarningStructure=warningStruct)> --->
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>

	</cffunction>


	<cffunction name="getFlagGroupTextIDForDomain" access="public" hint="Returns the FlagGroup Alias used in domains">
		<cfargument name="flagGroupID" type="numeric" required="true">
		<cfargument name="tableID" type="string" required="false">

		<cfset var flagGroupStruct = application.com.flag.getFlagGroupStructure(arguments.flagGroupID)>
		<cfset var flagGroupTextID = flagGroupStruct.flagGroupTextID >

		<cfif flagGroupTextID is "">
			<cfset flagGroupTextID = "flagGroup_#flagGroupStruct.flagGroupID#">
		</cfif>

		<cfif structKeyExists(arguments,"tableId")>
			<cfset flagGroupTextID = "#arguments.tableID#_#flagGroupTextID#">
		</cfif>

		<cfreturn xmlFormat(flagGroupTextID)>
	</cffunction>

	<cffunction name="applyFlagGroupToXML" hint="Adds or deletes a flagGroup to/from the domainXML">
		<cfargument name="XML" type="xml" required="true">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="delete" type="boolean" default="false">
		<cfargument name="refreshFlagGroup" type="boolean" default="false">
		<cfargument name="tableID" type="string" required="true">

			<cfset var result = {isOK = true,flagGroupExists=false,message=""}>

			<cfset var flagGroupStruct = application.com.flag.getFlagGroupStructure(arguments.flagGroupID)>
			<cfset var flagGroupTextID = flagGroupStruct.flagGroupTextID>
			<cfset var flagGroupTableAlias = "">
			<cfset var entityTableName = flagGroupStruct.entitytype.tablename>
			<cfset var entityUniqueKey = flagGroupStruct.entitytype.uniqueKey>
			<cfset var entityTableJSID = "">
			<cfset var entityUniqueKeyJSID = "">
			<cfset var findEntityTable = "">
			<cfset var findEntityColumn = "">
			<cfset var jdbcQuery = "">
			<cfset var fields = "">
			<cfset var joinedDataSetRef = "">
			<cfset var itemGroup = "">
			<cfset var xmlnsuri = xml.xmlroot.xmlnsuri>
			<cfset var XMLSnippets = structNew()>
			<cfset var checkForFlagGroupID = "">
			<cfset var tableSearchString = "">
			<cfset var datasourceID = "">
			<cfset var joinTreeID = "JoinTree_1">
			<cfset var itemGroupNode = "">
			<cfset var flagGroupJoinInfoArray = arrayNew(1)>
			<cfset var jdbcTableFindArray = arrayNew(1)>
			<cfset var thisNode = "">
			<cfset var checkForJoin = "">
			<cfset var checkForField = "">
			<cfset var checkForJdbcQuery = "">
			<cfset var checkForItem = "">
			<cfset var idx = 0>
			<cfset var i=0>
			<cfset var flagsForFlagGroup = "">
			<cfset var itemGroupTableName = trim(application.com.regExp.camelCaseToSpaces(string=replace(replace(arguments.tableID,"tbl_",""),"dbo_","")))> <!--- NJH 2012/02/15 CASE 426198: Create a readable item group name based on the table coming through --->
			<cfset var flagGroupName = xmlFormat(flagGroupStruct.name)>
			<cfset var flagGroupItemSetPrefix = "P Org"> <!--- to set profiles apart. They prefix the flagGroup name --->
			<cfset var thisTableId = "">
			<cfset var joinTreeItemGroupNode = '' />
			<cfset var javaLangType = "">

			<!--- find the table and column which we are going to join the flag to
				since the XML is case sensitive	have to get the exact case, can't just work it out
			 --->
			<cfset flagGroupTableAlias = getFlagGroupTextIDForDomain(flagGroupID=arguments.flagGroupId,tableID=arguments.tableID)>

			<cfset findEntityTable = getEntityTablesFromDomain(domainXML=arguments.xml,entityTableName=entityTableName,tableID=arguments.tableID)>

			<cfif not arguments.delete and not arguments.refreshFlagGroup>
				<!--- if the flagGroup has been applied to the domain xml, don't re-apply it --->
				<cfif hasProfileBeenAppliedToDomain(domainXML=arguments.xml,flagGroupId=arguments.flagGroupId,tableID=arguments.tableID)>
					<cfset result.message = "FlagGroupID #flagGroupStruct.flagGroupID# already exists in XML">
					<cfset result.flagGroupExists = true>
					<cfreturn result>
				</cfif>
			<cfelseif arguments.refreshFlagGroup and hasProfileBeenAppliedToDomain(domainXML=arguments.xml,flagGroupId=arguments.flagGroupId,tableID=arguments.tableID)>
				<cfset applyFlagGroupToXML(delete=true,refreshFlagGroup=false,xml=arguments.xml,flagGroupID=arguments.flagGroupID,tableID=arguments.tableID)>
			</cfif>

			<!--- Now find the column to join to
				WAB 2012-10-30 CASE 431515 modified the xpath, so only searches from current node (findEntityTable[1]), previously was searching whole document
			--->
			<cfset findEntityColumn = xmlsearch (findEntityTable[1],".//*[local-name()='field'][translate(@id, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='#lcase(entityUniqueKey)#']")>
			<cfif not arrayLen(findEntityColumn)>
				<cfset result.isOK = false>
				<cfset result.message = "Column #entityUniqueKey# not found in table #entityTableName# in XML">
				<cfreturn result>
			</cfif>

			<cfset entityTableJSID = findEntityTable[1].xmlattributes.id>
			<cfset entityUniqueKeyJSID = findEntityColumn[1].xmlattributes.id>
			<cfset datasourceID = getDataSourceForTable(domainXML=arguments.xml,tableID=arguments.tableId)>
			<cfset joinTreeID = getJoinTreeForTable(domainXML=arguments.xml,tableID=arguments.tableId)>

			<cfif flagGroupStruct.flagType.name is "radio">
				<!--- this to be added to the resources node --->
				<cfxml variable="XMLSnippets.jdbcQuery">
				<root <cfoutput>xmlns="#xmlnsuri#"</cfoutput>>
					<cfoutput><jdbcQuery datasourceId="#datasourceID#" id="#flagGroupTableAlias#" ></cfoutput>
						<fieldList>
					 		<field id="entityid" type="java.lang.Integer"/>
							<field id="flagid" type="java.lang.Integer"/>
							<field id="name" type="java.lang.String"/>
							<field id="LastUpdated" type="java.sql.Timestamp"/>
							<field id="Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains ---> <!--- Case 446246 --->
						</fieldList>
						<query>select f.name, f.flagid, bfd.entityid, bfd.LastUpdated, ISNULL(f.Score,0) as Score from booleanflagdata bfd inner join flag f on bfd.flagid = f.flagid where f.flaggroupid = <cfoutput>#flagGroupStruct.flagGroupID#</cfoutput>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</query>
					 </jdbcQuery>
				</root>
				</cfxml>

				<!--- these go as children of the fieldlist node  in the jdbctable with id of JoinTree_1--->
				<cfxml variable="XMLSnippets.fields">
					<cfoutput>
					<fieldlist>
				 		<field id="#flagGroupTableAlias#.entityid" type="java.lang.Integer"/>
						<field id="#flagGroupTableAlias#.flagid" type="java.lang.Integer"/>
						<field id="#flagGroupTableAlias#.name" type="java.lang.String"/>
						<field id="#flagGroupTableAlias#.LastUpdated" type="java.sql.Timestamp"/>
						<field id="#flagGroupTableAlias#.Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
					 </fieldlist>
					 </cfoutput>
				</cfxml>


				<!--- goes under a jdbctable --->
				<cfxml variable="XMLSnippets.joinedDataSetRef">
				<cfoutput>
				<root>
					<joinedDataSetRef test="#flagGroupTableAlias#">
						<joinString>left outer join #flagGroupTableAlias# #flagGroupTableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #flagGroupTableAlias#.entityid)</joinString>
				 	</joinedDataSetRef>
				</root>
				</cfoutput>
				</cfxml>

				<!--- these items go under the schema node - Display Tab --->
				<cfxml variable="XMLSnippets.itemGroup">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
				<itemGroups>
					<itemGroup description="Flag Group #flagGroupName#" label = "#flagGroupItemSetPrefix# #flagGroupName#" id="set_#flagGroupTableAlias#" resourceId="#joinTreeID#">
						<items>
							<item description="#flagGroupName#" id="#flagGroupTableAlias#_name" label="#flagGroupName#" labelId="" resourceId="#joinTreeID#.#flagGroupTableAlias#.name"/>
							<item description="#flagGroupName# Last Updated" descriptionId="" id="#flagGroupTableAlias#_LastUpdated" label="#flagGroupName# Last Updated" labelId="" resourceId="#joinTreeID#.#flagGroupTableAlias#.LastUpdated"/>
							<item description="#flagGroupName# Score" descriptionId="" id="#flagGroupTableAlias#_Score" label="#flagGroupName# Score" labelId="" resourceId="#joinTreeID#.#flagGroupTableAlias#.Score"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
					 	</items>
					</itemGroup>
				</itemGroups>
				</root>
				</cfoutput>
				</cfxml>

			<!--- START: 2013/12/13	YMA	Case 438402 show lastupdated date on checkbox values --->
			<cfelseif flagGroupStruct.flagType.name is "checkbox">

				<cfquery name="flagsForFlagGroup" datasource="#application.siteDatasource#">
					select flagId, 'flag_'+cast(flagID as varchar)+'_table' as flagTableAlias, 'flag_'+cast(flagID as varchar) as flagAlias,name
					from flag where flagGroupID =  <cf_queryparam value="#flagGroupStruct.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<!--- this to be added to the resources node --->
				<cfxml variable="XMLSnippets.jdbcQuery">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
					<jdbcQuery datasourceId="#datasourceID#" id="#flagGroupTableAlias#" >
						<fieldList>
							<field id="#flagGroupTableAlias#_names" type="java.lang.String"/>
							<field id="#entityUniqueKey#" type="java.lang.Integer" />
						</fieldList>
						<query>select cast(dbo.booleanflaglist(#flagGroupStruct.flagGroupID#,#entityUniqueKey#,'flagName')as varchar(200)) as #flagGroupTableAlias#_names, #entityUniqueKey# from #entityTableName# <!--- 2014-12-19 AHL Case 442967 Phrases not being Resolved in Report Designer --->
						</query>
					</jdbcQuery>
					<cfloop query="flagsForFlagGroup">
					<jdbcQuery datasourceId="#datasourceID#" id="#arguments.tableId#_#flagTableAlias#">
					 	<fieldList>
					 		<!--- 2013-09-18	YMA	Case 437055 should not add a description to the field.  Causes validation errors on jasperserver domain --->
					 		<!--- 2013-11-20	YMA	Case 437921 Fix ambiguous column name errror when applying profile to opp domain --->
							<field id="#arguments.tableID#_#flagAlias#" type="java.lang.String"/>
							<field id="#entityUniqueKey#" type="java.lang.Integer" />
							<field id="#arguments.tableID#_#flagAlias#_LastUpdated" type="java.sql.Timestamp"/>
							<field id="#arguments.tableID#_#flagAlias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</fieldList>
						<query>select case when booleanflagdata.flagid is null then 'No' else 'Yes' end as #arguments.tableID#_#flagAlias#, booleanflagdata.LastUpdated as #arguments.tableID#_#flagAlias#_LastUpdated,(SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #flagID#) as #arguments.tableID#_#flagAlias#_Score, #entityUniqueKey# from #entityTableName# left join booleanflagdata on booleanflagdata.entityid = #entityUniqueKey# and booleanflagdata.flagid = #flagID#	<!--- 2015-01-16 AHL Case 442666 Opportunity ID not displaying in Reports --->  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</query>
					</jdbcQuery>
					 </cfloop>
				</root>
				</cfoutput>
				</cfxml>

				<!--- these go as children of the fieldlist node  in the jdbctable
					NJH 2012/02/15 CASE 426198: changed the id from #flagGroupTableAlias#._names to #flagGroupTableAlias#.#flagGroupTableAlias#_names
				--->
				<cfxml variable="XMLSnippets.fields">
					<cfoutput>
	 					<fieldlist>
							<field id="#flagGroupTableAlias#.#flagGroupTableAlias#_names" type="java.lang.String" />
	      					<field id="#flagGroupTableAlias#.#entityUniqueKey#" type="java.lang.Integer" />
							<cfloop query="flagsForFlagGroup">
							<field id="#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#" type="java.lang.String" />
							<field id="#arguments.tableId#_#flagTableAlias#.#entityUniqueKey#" type="java.lang.Integer" />
							<field id="#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_LastUpdated" type="java.sql.Timestamp"/>
							<field id="#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains ---> <!--- Case 446246 --->
							</cfloop>
						 </fieldlist>
					 </cfoutput>
				</cfxml>


				<!--- goes under a jdbctable  --->
				<cfxml variable="XMLSnippets.joinedDataSetRef">
					<cfoutput>
					<root>
					<joinedDataSetRef test="#flagGroupTableAlias#">   <!--- the test attribute is used to search the existing xml for this join, but is removed before we merge it into the xml --->
						<joinString>join #flagGroupTableAlias# #flagGroupTableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #flagGroupTableAlias#.#entityUniqueKey#)</joinString>
					</joinedDataSetRef>
					<cfloop query="flagsForFlagGroup">
					<joinedDataSetRef test="#arguments.tableId#_#flagTableAlias#">
						<joinString>join #arguments.tableId#_#flagTableAlias# #arguments.tableId#_#flagTableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #arguments.tableId#_#flagTableAlias#.#entityUniqueKey#)</joinString>
				 	</joinedDataSetRef>
					</cfloop>
					</root>

					</cfoutput>
				</cfxml>

				<!--- these items go under the schema node --->
				<cfxml variable="XMLSnippets.itemGroup">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
				<itemGroups>
					<itemGroup description="Flag Group #flagGroupName#" label = "#flagGroupItemSetPrefix# #flagGroupName#" id="set_#flagGroupTableAlias#" resourceId="#joinTreeID#">
						<items>
							<item description="#flagGroupName#" id="#flagGroupTableAlias#_names" label="#flagGroupName# List" labelId="" resourceId="#joinTreeID#.#flagGroupTableAlias#.#flagGroupTableAlias#_names"/>
							<cfloop query="flagsForFlagGroup">
							<item description="#xmlFormat(name)#" id="#arguments.tableId#_#flagAlias#" label="#xmlFormat(name)#" labelId="" resourceId="#joinTreeID#.#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#"/>
							<item description="#xmlFormat(name)# Last Updated" id="#arguments.tableId#_#flagAlias#_LastUpdated" label="#xmlFormat(name)# Last Updated" labelId="" resourceId="#joinTreeID#.#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_LastUpdated"/>
							<item description="#xmlFormat(name)# Score" id="#arguments.tableId#_#flagAlias#_Score" label="#xmlFormat(name)# Score" labelId="" resourceId="#joinTreeID#.#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_Score"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
					 	</items>
					</itemGroup>
				</itemGroups>
				</root>
				</cfoutput>
				</cfxml>
			<!--- END: 2013/12/13	YMA	Case 438402 show lastupdated date on checkbox values --->

			<cfelseif listFindNoCase("integer,decimal",flagGroupStruct.flagType.name)>
				<cfif flagGroupStruct.flagType.name is "integer">
					<cfset javaLangType = "java.lang.Integer">
				<cfelse>
					<cfset javaLangType = "java.lang.Float">
				</cfif>
				<cfset flagGroupJoinInfoArray = application.com.flag.getJoinInfoForFlagGroup(flagGroupID=flagGroupStruct.flagGroupID)>

				<!--- this to be added to the resources node --->
				<cfxml variable="XMLSnippets.jdbcQuery">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
					<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
					<cfset local.flagID = replacenocase(flagGroupJoinInfoArray[idx].alias,"Flag_","")/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
					<jdbcQuery datasourceId="#datasourceID#" id="#flagGroupJoinInfoArray[idx].tableAlias#" >
						<fieldList>
							<!--- 2012/11/28	YMA		CASE: 432282 decide if integer profile should be displayed as String or Integer based on if selectfieldraw exists--->
							<cfif structKeyExists(flagGroupJoinInfoArray[idx],"selectfieldraw")>
							<field id="#flagGroupJoinInfoArray[idx].alias#" type="java.lang.String"/>
							<cfelse>
								<field id="#flagGroupJoinInfoArray[idx].alias#" type="#javaLangType#"/>
							</cfif>
							<field id="#entityUniqueKeyJSID#" type="java.lang.Integer" />
							<field id="#flagGroupJoinInfoArray[idx].alias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</fieldList>
						<!--- 2012-08-21 PPB Case 428959 integerFlagData was linking to itself
						<query>select cast(#flagGroupJoinInfoArray[idx].selectField# as varchar(200)) as #flagGroupJoinInfoArray[idx].alias#, #entityUniqueKeyJSID# from integerFlagData #flagGroupJoinInfoArray[idx].tableAlias# #flagGroupJoinInfoArray[idx].join#
						</query>
 						--->
						<!--- 2012-08-21 PPB Case 428959 link the entity table to integerFlagData; note: it is ok to use the LEFT 1 of the entity table as the alias here because that is how flag.getJoinInfoForFlag() does it
							  it seems that we don't need to link to the entity table except that flagGroupJoinInfoArray[idx].join refers to eg. o.organisationID (perhaps we shouldn't call getJoinInfoForFlagGroup() and build up the join
							 note: flagGroupJoinInfoArray.selectfieldraw = eg Flag_9999.data
						--->
						<!--- 2012/11/28	YMA		CASE: 432282 decide if integer profile should be displayed as String or Integer based on if selectfieldraw exists--->
						<cfif structKeyExists(flagGroupJoinInfoArray[idx],"selectfieldraw")>
							<query>select cast(#flagGroupJoinInfoArray[idx].selectField# as varchar(200)) as #flagGroupJoinInfoArray[idx].alias#, #left(flagGroupStruct.entityType.tableName,1)#.#flagGroupStruct.entityType.UniqueKey#, (SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #local.flagID#) as #flagGroupJoinInfoArray[idx].alias#_Score from #flagGroupStruct.entityType.tableName# AS #left(flagGroupStruct.entityType.tableName,1)# #flagGroupJoinInfoArray[idx].join# where #flagGroupJoinInfoArray[idx].selectfieldraw# IS NOT NULL  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						<cfelse>
							<query>select #flagGroupJoinInfoArray[idx].selectField# as #flagGroupJoinInfoArray[idx].alias#, #left(flagGroupStruct.entityType.tableName,1)#.#flagGroupStruct.entityType.UniqueKey#, (SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #local.flagID#) as #flagGroupJoinInfoArray[idx].alias#_Score from #flagGroupStruct.entityType.tableName# AS #left(flagGroupStruct.entityType.tableName,1)# #flagGroupJoinInfoArray[idx].join# where #flagGroupJoinInfoArray[idx].selectField# IS NOT NULL  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</cfif>
						</query>
					</jdbcQuery>
					</cfloop>
				</root>
				</cfoutput>
				</cfxml>

				<!--- these go as children of the fieldlist node  in the jdbctable --->
				<cfxml variable="XMLSnippets.fields">
					<cfoutput>
	 					<fieldlist>
							<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
								<!--- 2012/11/28	YMA		CASE: 432282 decide if integer profile should be displayed as String or Integer based on if selectfieldraw exists--->
								<cfif structKeyExists(flagGroupJoinInfoArray[idx],"selectfieldraw")>
									<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#" type="java.lang.String"/>
								<cfelse>
									<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#" type="#javaLangType#"/>
								</cfif>
							<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#entityUniqueKeyJSID#" type="java.lang.Integer" />
							<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#_Score" type="java.lang.Float" />  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
						 </fieldlist>
					 </cfoutput>
				</cfxml>


				<!--- goes under a jdbctable with id JoinTree_1 --->
				<cfxml variable="XMLSnippets.joinedDataSetRef">
					<cfoutput>
					<root>
					<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
					<joinedDataSetRef test="#flagGroupJoinInfoArray[idx].tableAlias#">
						<joinString>left outer join #flagGroupJoinInfoArray[idx].tableAlias# #flagGroupJoinInfoArray[idx].tableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #flagGroupJoinInfoArray[idx].tableAlias#.#entityUniqueKeyJSID#)</joinString>
				 	</joinedDataSetRef>
					</cfloop>
					</root>

					</cfoutput>
				</cfxml>

				<!--- these items go under the schema node --->
				<cfxml variable="XMLSnippets.itemGroup">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
				<itemGroups>
					<itemGroup description="Flag Group #flagGroupName#" label = "#flagGroupItemSetPrefix# #flagGroupName#" id="set_#flagGroupTableAlias#" resourceId="#joinTreeID#">
						<items>
							<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
							<item description="#flagGroupJoinInfoArray[idx].name#" id="#flagGroupJoinInfoArray[idx].alias#" label="#flagGroupJoinInfoArray[idx].name#" labelId="" resourceId="#joinTreeID#.#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#"/>
							<item description="#flagGroupJoinInfoArray[idx].name# Score" id="#flagGroupJoinInfoArray[idx].alias#_Score" label="#flagGroupJoinInfoArray[idx].name# Score" labelId="" resourceId="#joinTreeID#.#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#_Score"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
					 	</items>
					</itemGroup>
				</itemGroups>
				</root>
				</cfoutput>
				</cfxml>

			<cfelseif listFindNoCase("text,date",flagGroupStruct.flagType.name)>

				<cfquery name="flagsForFlagGroup" datasource="#application.siteDatasource#">
					select flagId, 'flag_'+cast(flagID as varchar)+'_table' as flagTableAlias, 'flag_'+cast(flagID as varchar) as flagAlias,name
					from flag where flagGroupID =  <cf_queryparam value="#flagGroupStruct.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<!--- this to be added to the resources node --->
				<cfxml variable="XMLSnippets.jdbcQuery">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
					<cfloop query="flagsForFlagGroup">
					<jdbcQuery datasourceId="#datasourceID#" id="#arguments.tableId#_#flagTableAlias#" >
						<fieldList>
							<!--- 2014-08-05 REH CASE 441207 defined correct data type for dates --->
							<field id="#arguments.tableID#_#flagAlias#" type="#flagGroupStruct.flagType.name eq 'date'? 'java.sql.Timestamp' : 'java.lang.String'#"/>
							<field id="#entityUniqueKeyJSID#" type="java.lang.Integer" />
							<field id="#arguments.tableID#_#flagAlias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</fieldList>
						<query>select data as #arguments.tableID#_#flagAlias#, entityID as #entityUniqueKeyJSID#,(SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #flagID#) as #arguments.tableID#_#flagAlias#_Score from #flagGroupStruct.flagType.dataTableFullName# where flagid = #flagID#  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						</query>
					</jdbcQuery>
					</cfloop>
				</root>
				</cfoutput>
				</cfxml>

				<!--- these go as children of the fieldlist node  in the jdbctable --->
				<cfxml variable="XMLSnippets.fields">
					<cfoutput>
	 					<fieldlist>
							<cfloop query="flagsForFlagGroup">
							<!--- 2014-08-05 REH CASE 441207 defined correct data type for dates --->
							<field id="#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#" type="#flagGroupStruct.flagType.name eq 'date'? 'java.sql.Timestamp' : 'java.lang.String'#" />
							<field id="#arguments.tableId#_#flagTableAlias#.#entityUniqueKeyJSID#" type="java.lang.Integer" />
							<field id="#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
						 </fieldlist>
					 </cfoutput>
				</cfxml>


				<!--- goes under a jdbctable with id JoinTree_1 --->
				<cfxml variable="XMLSnippets.joinedDataSetRef">
					<cfoutput>
					<root>
					<cfloop query="flagsForFlagGroup">
					<joinedDataSetRef test="#arguments.tableId#_#flagTableAlias#">
						<joinString>left outer join #arguments.tableId#_#flagTableAlias# #arguments.tableId#_#flagTableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #arguments.tableId#_#flagTableAlias#.#entityUniqueKeyJSID#)</joinString>
				 	</joinedDataSetRef>
					</cfloop>
					</root>

					</cfoutput>
				</cfxml>

				<!--- these items go under the schema node --->
				<cfxml variable="XMLSnippets.itemGroup">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
				<itemGroups>
					<itemGroup description="Flag Group #flagGroupName#" label = "#flagGroupItemSetPrefix# #flagGroupName#" id="set_#flagGroupTableAlias#" resourceId="#joinTreeID#">
						<items>
							<cfloop query="flagsForFlagGroup">
							<item description="#xmlFormat(name)#" id="#arguments.tableID#_#flagAlias#" label="#xmlFormat(name)#" labelId="" resourceId="#joinTreeID#.#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#"/>
							<item description="#xmlFormat(name)# Score" descriptionId="" id="#flagGroupTableAlias#_Score" label="#flagGroupName# Score" labelId="" resourceId="#joinTreeID#.#arguments.tableId#_#flagTableAlias#.#arguments.tableID#_#flagAlias#_Score"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
					 	</items>
					</itemGroup>
				</itemGroups>
				</root>
				</cfoutput>
				</cfxml>

			<cfelseif listFindNoCase("TextMultiple,IntegerMultiple",flagGroupStruct.flagType.name)><!--- 2013/3/14	YMA	Add ability to connect Multipleflagdata to domains --->

				<cfset flagGroupJoinInfoArray = application.com.flag.getJoinInfoForFlagGroup(flagGroupID=flagGroupStruct.flagGroupID)>
				<!--- this to be added to the resources node --->
				<cfxml variable="XMLSnippets.jdbcQuery">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
					<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
						<cfset local.flagID = replacenocase(flagGroupJoinInfoArray[idx].alias,"Flag_","")/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
						<jdbcQuery datasourceId="#datasourceID#" id="#flagGroupJoinInfoArray[idx].tableAlias#" >
							<fieldList>
								<field id="#flagGroupJoinInfoArray[idx].alias#" type="java.lang.String"/>
								<field id="#entityUniqueKeyJSID#" type="java.lang.String" />
								<field id="#flagGroupJoinInfoArray[idx].alias#_Score" type="java.lang.Float"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</fieldList>
							<cfif structKeyExists(flagGroupJoinInfoArray[idx],"selectfieldraw")>
								<query>select LEFT(data, LEN(data) - 1) as #flagGroupJoinInfoArray[idx].alias#, entityID as #entityUniqueKeyJSID#, (SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #local.flagID#) as #flagGroupJoinInfoArray[idx].alias#_Score from ( SELECT CAT.entityID, CAST( (SELECT cast( #flagGroupJoinInfoArray[idx].selectfield# as nvarchar(200)) + ', ' AS [data()] FROM #flagGroupStruct.entityType.tableName# O #flagGroupJoinInfoArray[idx].join# WHERE #flagGroupJoinInfoArray[idx].tableAlias#.flagID = CAT.flagID and #flagGroupJoinInfoArray[idx].tableAlias#.entityID = CAT.entityID FOR XML PATH('')) AS NVARCHAR(500) ) AS data FROM #flagGroupStruct.flagType.dataTableFullName# CAT where CAT.flagid = #replacenocase(flagGroupJoinInfoArray[idx].alias,"Flag_","")# group by CAT.entityID, CAT.flagid ) #flagGroupStruct.flagType.dataTableFullName#  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
								</query>
							<cfelse>
								<query>select LEFT(data, LEN(data) - 1) as #flagGroupJoinInfoArray[idx].alias#, entityID as #entityUniqueKeyJSID#, (SELECT ISNULL(f.Score,0) as Score FROM Flag f WHERE f.flagid = #local.flagID#) as #flagGroupJoinInfoArray[idx].alias#_Score from (SELECT CAT.entityID, CAST( (SELECT cast(SUB.data as nvarchar(500)) + ', ' AS [data()] FROM #flagGroupStruct.flagType.dataTableFullName# SUB WHERE SUB.flagID = CAT.flagID and SUB.entityID = CAT.entityID FOR XML PATH('')) AS NVARCHAR(500)) AS data FROM  #flagGroupStruct.flagType.dataTableFullName# CAT where CAT.flagid = #replacenocase(flagGroupJoinInfoArray[idx].alias,"Flag_","")# group by CAT.entityID, CAT.flagid) #flagGroupStruct.flagType.dataTableFullName#  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
								</query>
			</cfif>
						</jdbcQuery>
					</cfloop>
				</root>
				</cfoutput>
				</cfxml>

				<!--- these go as children of the fieldlist node  in the jdbctable --->
				<cfxml variable="XMLSnippets.fields">
					<cfoutput>
	 					<fieldlist>
							<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
							<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#" type="java.lang.String" />
							<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#entityUniqueKeyJSID#" type="java.lang.String" />
							<field id="#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#_Score" type="java.lang.Float" />  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
						 </fieldlist>
					 </cfoutput>
				</cfxml>

				<!--- goes under a jdbctable with id JoinTree_1 --->
				<cfxml variable="XMLSnippets.joinedDataSetRef">
					<cfoutput>
					<root>
						<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
						<joinedDataSetRef test="#flagGroupJoinInfoArray[idx].tableAlias#">
							<joinString>left outer join #flagGroupJoinInfoArray[idx].tableAlias# #flagGroupJoinInfoArray[idx].tableAlias# on (#entityTableJSID#.#entityUniqueKeyJSID#  == #flagGroupJoinInfoArray[idx].tableAlias#.#entityUniqueKeyJSID#)</joinString>
					 	</joinedDataSetRef>
						</cfloop>
					</root>

					</cfoutput>
				</cfxml>

				<!--- these items go under the schema node --->
				<cfxml variable="XMLSnippets.itemGroup">
				<cfoutput>
				<root xmlns="#xmlnsuri#">
				<itemGroups>
					<itemGroup description="Flag Group #flagGroupName#" label = "#flagGroupItemSetPrefix# #flagGroupName#" id="set_#flagGroupTableAlias#" resourceId="#joinTreeID#">
						<items>
							<cfloop from=1 to=#arrayLen(flagGroupJoinInfoArray)# index="idx">
							<item description="#flagGroupJoinInfoArray[idx].name#" id="#flagGroupJoinInfoArray[idx].alias#" label="#flagGroupJoinInfoArray[idx].name#" labelId="" resourceId="#joinTreeID#.#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#"/>
							<item description="#flagGroupJoinInfoArray[idx].name# Score" id="#flagGroupJoinInfoArray[idx].alias#_Score" label="#flagGroupJoinInfoArray[idx].name# Score" labelId="" resourceId="#joinTreeID#.#flagGroupJoinInfoArray[idx].tableAlias#.#flagGroupJoinInfoArray[idx].alias#_Score"/>  <!--- 2015-07-23 AHL Adding Score to the profile report domains --->
							</cfloop>
					 	</items>
					</itemGroup>
				</itemGroups>
				</root>
				</cfoutput>
				</cfxml>

			</cfif>

			<!--- NJH 2012/02/15 CASE 426198: search both jdbcQuery or jdbcTable nodes --->
			<cfset jdbcTableFindArray = xmlsearch (arguments.xml,"//*[local-name()='jdbcTable'][@id='#joinTreeID#'] | //*[local-name()='jdbcQuery'][@id='#joinTreeID#']")> <!--- [@tableName='dbo.#flagGroupStruct.entityType.tablename#'] --->

			<!--- Check if we already have this join in the domain; if exists, delete and re-add --->
			<cfloop index="i" from = 1 to = #arrayLen(XMLSnippets.joinedDataSetRef.root.xmlchildren)#>
				<cfset thisNode = XMLSnippets.joinedDataSetRef.root.xmlchildren[i]>
				<cfset checkForJoin = xmlsearch(arguments.xml,"//*[local-name()='joinedDataSetRef']/*[local-name()='joinString'][ contains(., '#thisNode.xmlattributes.test#' ) ]")>
				<cfif arrayLen(checkForJoin) gt 0>
					<cfset application.com.xmlFunctions.deleteNode(checkForJoin[1].XMLparent)>
				</cfif>

				<!--- TODO: Need to work out how to create a join tree if one doesn't exist.. as applying a profile will/should be creating a join tree --->
				<cfif not arguments.delete and joinTreeID neq "">
					<cfset structDelete (thisNode.xmlattributes,"test")>
					<cfset application.com.xmlFunctions.XmlAppend (jdbcTableFindArray[1].joinedDataSetList,thisNode,true)>
				</cfif>

			</cfloop>

			<!--- Add fields--->
			<cfloop index="i" from = 1 to = #arrayLen(XMLSnippets.fields.fieldlist.xmlchildren)#>
				<cfset checkForField = xmlsearch(jdbcTableFindArray[1],"//*[local-name()='field'][@id='#XMLSnippets.fields.fieldlist.xmlchildren[i].xmlattributes.id#']")>

				<cfif arrayLen(checkForField)>
					<cfset application.com.xmlFunctions.deleteNodes(checkForField)>
				</cfif>
				<cfif not delete>
					<cfset application.com.xmlFunctions.XmlAppend(jdbcTableFindArray[1].fieldList,XMLSnippets.fields.fieldlist.xmlchildren[i],true)>
				</cfif>
			</cfloop>

			<!--- Add Query (derived Table), delete if already exists --->
			<cfloop index="i" from = 1 to = #arrayLen(XMLSnippets.jdbcQuery.root.xmlchildren)#>

				<cfset checkForJdbcQuery = xmlsearch(xml,"//*[local-name()='jdbcQuery'][@id='#XMLSnippets.jdbcQuery.root.xmlchildren[i].xmlattributes.id#']")>
				<cfif arrayLen(checkForJdbcQuery) gt 0>
					<cfset application.com.xmlFunctions.deleteNodes(checkForJdbcQuery)>
				</cfif>
				<cfif not delete>
					<cfset application.com.xmlFunctions.XmlAppend (xml.schema.resources,XMLSnippets.jdbcQuery.root.xmlchildren[i],true)>
				</cfif>
			</cfloop>

			<!--- add the item groups - display --->
			<cfset checkForItem = xmlsearch(xml,"//*[local-name()='itemGroup'][@id='#XMLSnippets.itemGroup.root.itemGroups.itemgroup.xmlattributes.id#']")>

			<cfif arrayLen(checkForItem) gt 0>
				<cfset application.com.xmlFunctions.deleteNodes(checkForItem)>
			</cfif>
			<cfif not delete>
				<!--- create new itemgroups node on top level.. non exists at this point --->
				<cfif not structKeyExists(xml.schema,"itemgroups")>
					<cfset application.com.xmlFunctions.XmlAppend(xml.schema,XMLSnippets.itemgroup.root.itemgroups,true)>
				<cfelse>

					<!--- here we're getting the itemgroup that the table exists in. The itemgroup parent is joinTree+tablename. The item group must
						contain a field from this table for the table to be found in the item group and therefore placed in the appropriate spot. --->
					<cfset itemGroupNode = getItemGroupForTable(domainXML=arguments.xml,tableId=arguments.tableID,joinTreeId=joinTreeID)>

					<!--- if itemGroup node wasn't found, add to root item group with the same jointree Id.--->
					<cfif itemGroupNode eq "">
						<cfset joinTreeItemGroupNode = getItemGroupForJoinTree(domainXML=arguments.xml,joinTreeId=joinTreeID)>

						<!--- if itemGroup for the join tree node wasn't found, just add to root item group --->
						<cfif joinTreeItemGroupNode neq "">
							<!--- if the itemGroup has itemgroups, add a new item group under item groups --->
							<cfif structKeyExists(joinTreeItemGroupNode,"itemGroups")>
								<cfset application.com.xmlFunctions.XmlAppend(xml.schema.itemgroups.itemgroup.itemGroups,XMLSnippets.itemgroup.root.itemgroups.itemgroup,true)>
							<cfelse>
								<!--- otherwise add item groups --->
								<cfset application.com.xmlFunctions.XmlAppend(joinTreeItemGroupNode,XMLSnippets.itemgroup.root.itemgroups,true)>
							</cfif>
						<cfelse>
							<cfset application.com.xmlFunctions.XmlAppend(xml.schema.itemgroups,XMLSnippets.itemgroup.root.itemgroups.itemgroup,true)>
						</cfif>
					<cfelse>
						<!--- if an itemGroups node exists under itemGroup, add the new profile here; otherwise, add a new itemGroups node --->
						<cfif structKeyExists(itemGroupNode,"itemGroups")>
							<cfset application.com.xmlFunctions.XmlAppend(itemGroupNode.itemGroups,XMLSnippets.itemgroup.root.itemgroups.itemgroup,true)>
						<cfelse>
							<cfset application.com.xmlFunctions.XmlAppend(itemGroupNode,XMLSnippets.itemgroup.root.itemgroups,true)>
						</cfif>
					</cfif>
				</cfif>
			</cfif>

			<cfset result.xml = xml>

			<cfreturn result>

	</cffunction>


	<cffunction name="getJasperServerLoginParamsForCurrentUser" output="false">
		<cfargument name="newUser" type="boolean" default="false">
		<cfargument name="currentUser" default="#request.relayCurrentUser#">
		<cfargument name="refreshUser" type="boolean" default="true">

		<cfset var result = "">
		<cfset var userGroupList = "">
		<cfset var jasperSecurityObj = "">

		<!---<cfset var isPrimaryContact = IIF(application.com.flag.getFlagData(flagID="KeyContacts Primary",data=currentUser.personID).recordCount gt 0,true,false)>--->
		<cfset var isPrimaryContact = application.com.relayPLO.isPersonAPrimaryContact(personID=currentUser.personID)>
		<cfset var isTrainingContact = IIF(application.com.flag.getFlagData(flagID="KeyContactsTraining",data=currentUser.personID).recordCount gt 0,true,false)>
		<cfset var isLeadContact = IIF(application.com.flag.getFlagData(flagID="PrimaryLeadContact",data=currentUser.personID).recordCount gt 0,true,false)>
		<cfset var isFundManager = application.com.relayFundManager.checkIfUserIsManager(personID = currentUser.personID)>
		<cfset var isPortalLeadUser = IIF(application.com.flag.getFlagData(flagID="PortalLeadUser",entityID=currentUser.personID).recordCount gt 0,true,false)>

		<!--- this is the variable used for the domain security.
			Possible values are:
			Country - filter by country rights - should only be for internal user
			Organisation - show only organisation records - for primary contact, etc. on the portal
			Person - show only personal records - default for portal user
		 --->
		<cfset var filter = "Country">
		<cfset var user = getJasperServerUserNameForCurrentUser()>
		<cfset var homeURI = "/organizations/#getTenantID()#/Home/#user#_home">
		<cfset var tenantID = getTenantID()>
		<cfset var username = getJasperServerUserNameForCurrentUser()>

		<!---
			On the very first occasion a user accesses jasperserver we don't have a user id, so we can't add attributes
			So if no user id then we need to do a dummmy request
		 --->

		<cfif not arguments.newUser and getJasperServerUserIDForCurrentUser() is "" and arguments.refreshUser>
			<cfset initialiseJasperServerUser()>
		</cfif>

		<!--- <cfif arguments.refreshUser>
			<cfset application.com.jasperServerWebServiceCaller.putUser(username=user)>
		</cfif> --->
		<cfset userGroupList = getUserGroupList()>


		<cfscript>
		if (request.relayCurrentUser.isLoggedIn) {
			jasperSecurityObj = CreateObject( "java", "com.jaspersoft.ps.relayware.security.ParamUtil" ) ;
			jasperSecurityObj.init(
				javacast("string", username),
				request.relaycurrentuser.person.password,
				javacast("string", request.relaycurrentuser.personid),
				javacast("string", request.relaycurrentuser.countryList),
				userGroupList,
				tenantID);
			result = jasperSecurityObj.getURLParam();
			result = rereplaceNoCase(result, "\A&","","ONE") ;

			if (getJasperServerUserIDForCurrentUser() neq "") {
				if (not newUser) {
					AddProfileAttributeForCurrentUser (attrname = "countryID", attrValue = request.relaycurrentuser.countryid );
					AddProfileAttributeForCurrentUser (attrname = "languageID", attrValue = request.relaycurrentuser.languageid );
					if (request.relayCurrentUser.isInternal) {
						AddProfileAttributeForCurrentUser (attrname = "countryIDlist", attrValue = request.relaycurrentuser.countryList );  // just taking first few for time being since the field is only 255 long
					} else {
						AddProfileAttributeForCurrentUser (attrname = "countryIDlist", attrValue = request.relaycurrentuser.countryid);
					}
					AddProfileAttributeForCurrentUser (attrname = "personID", attrValue = request.relaycurrentuser.personID);
					AddProfileAttributeForCurrentUser (attrname = "isInternal", attrValue = request.relaycurrentuser.isInternal);
					AddProfileAttributeForCurrentUser (attrname = "locationID", attrValue = request.relaycurrentuser.locationID);
					AddProfileAttributeForCurrentUser (attrname = "organisationID", attrValue = request.relaycurrentuser.organisationID);
					if (not request.relayCurrentUser.isInternal) {
						if (isPrimaryContact or isTrainingContact or isLeadContact or isFundManager or isPortalLeadUser) {
							filter = "Organisation";
						} else {
							filter = "Person";
						}
					}
					AddProfileAttributeForCurrentUser (attrname = "filter", attrValue = filter);
					/* I thought this was how to deal with lists, but it is not! AddProfileAttributeForCurrentUser (attrname = "countryids", attrValue = request.relaycurrentuser.countryList,isList=true );  */
				}
			}
		}
		</cfscript>

		<cfif not arguments.newUser and getJasperServerUserIDForCurrentUser() neq "" and arguments.refreshUser>

			<cfif not request.relayCurrentUser.jasperServerIsLoggedIn>
				<!--- WAB 2012-10-04  CASE 431042 --->
				<cfset deleteOldRoles (userGroupList = userGroupList, username = username,  tenantID = tenantID )>

				<!--- fixing a Kerstin issue.. after some re-work, the SSO no longer assigned the correct permissions on the users home folder.. So, doing it here via the webservice --->
				<!--- NJH 2013/09/26 - removed as Steve fixed the original problem. Still would like to have old roles deleted by the SSO.....
				<cfset application.com.jasperServerWebServiceCaller.setPermission(uriString=homeURI,permissionMask=30,user=user)>
				<cfset application.com.jasperServerWebServiceCaller.setPermission(uriString=homeURI,permissionMask=0,role="ROLE_USER")> --->

			</cfif>

			<cfset application.com.relayCurrentUser.setJasperServerIsLoggedIn(isLoggedIn=true)>
			<cfset updateUserFolderName()>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!---
	WAB 2012-010-04 cut out this code from the above function
	 --->
	<cffunction name="getUserGroupList" output="false" returntype="string">

		<cfset var securityModule = "">
		<cfset var hasPermission = false>
		<cfset var module = "">
		<cfset var securityModuleTaskAndID = "">
		<cfset var userGroupList = "">

		<!--- This list actually uses bent pipe as its sub-delimiter, but tends to get corrupted and #application.delim1# is pain to read, so use - and then replace --->
		<cfset var securityModuleList = "file-content,element-Content,reportDesigner-ReportManager,comm-Communicate,data-Content,elearning-eLearning,event-Events,fund-Funds,incentive-Incentive,leadManager-LeadManager,sod-SalesOutData"> <!--- "fnladmin,data,element,file,approvals,scorecard,order,elearning,comm,event,fund,incentive,leadManager,locator,profile,salesForce,sod"> --->

			<!--- we're currently assuming that every internal user gets read/write/delete permissions based on having one of these security tasks assigned
			Case Matters!!! for usergroups. Everything should be in lower case!! The will map to roles in applicationContext-security-relayware.xml, so that xml will need changing
			if these roles change.
		 --->
		<cfif request.currentSite.isInternal>
			<cfset userGroupList="internal"> <!--- the internal home folder is based on this being set --->

			<cfif application.com.login.checkInternalPermissions("rwAdminTask","Level1")>
				<cfset userGroupList = listAppend(userGroupList,"rwadmintask")>
			</cfif>

			<cfloop list="#securityModuleList#" index="securityModuleTaskAndID">
				<cfset securityModule = listFirst(securityModuleTaskAndID,"-")>
				<cfset module = listLast(securityModuleTaskAndID,"-")>
				<cfset hasPermission = false>
				<cfif securityModule eq "reportDesigner">
					<cfif application.com.login.checkInternalPermissions("#securityModule#task","Level3")>
						<cfset securityModule = "admin">
						<cfset hasPermission = true>
					<cfelseif application.com.login.checkInternalPermissions("#securityModule#task","Level2")>
						<cfset securityModule = "reportDesignerWrite">
						<cfset hasPermission = true>
					</cfif>
				<cfelse>
					<cfif application.com.login.checkInternalPermissions("#securityModule#task","Level1") and application.com.relayMenu.isModuleActive(moduleId=module)>
						<cfset hasPermission = true>
					</cfif>
				</cfif>

				<cfif hasPermission>
					<cfset userGroupList = listAppend(userGroupList,lcase(securityModule)&"task")>

					<!--- JS roles can't have a one to many mapping with RW roles. So, have to break up datatask into 3 separate tasks.. datatask,datatask_partner and datatask_system --->
					<cfif securityModule eq "data">
						<cfset userGroupList = listAppend(userGroupList,"datatask_partner,datatask_system")>
					</cfif>
				</cfif>
			</cfloop>
		<cfelse>
			<cfset userGroupList = "portal">
		</cfif>

		<cfreturn userGroupList>

	</cffunction>

	<!--- WAB 2012-10-04  CASE 431042
	Need to be able to delete roles in jasperserver if user no longer has rights
	Ought to be done at Jasperserver end, but Kerstins code does not do this job, so we are having to do it in the db
	This group of functions converts relay tasks to jasperroles and then deletes old ones from the database
	 --->
	<cffunction name="getRelayToJSRoleMappingStruct">

		<cfset var result = {}>
		<cfset var getXML  = application.com.xmlfunctions.readAndParseXMLFile("#application.paths.relayware#/xml/RelayToJasperServerRoleMapping.XML")>
		<cfset var entry = "">

		<cfset var entryArray = xmlsearch(getXML.xml,"//map/entry")>

		<cfloop array="#entryArray#" index="entry">
			<cfset result [replacenocase(entry.key.value.xmltext,"ROLE_","")] = entry.value.xmltext>
		</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction name="convertUserGroupListToJSRoleList">
		<cfargument name="usergroupList" required="true">

		<cfset var ug = "">
		<cfset var result = "">
		<cfset var mapping = getRelayToJSRoleMappingStruct()>

		<cfloop list="#userGroupList#" index="ug">
			<cfif structKeyExists (mapping,ug)>
				<cfset result = listAppend(result,mapping[ug])>
			</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>

	<cffunction name="DeleteOldRoles">
		<cfargument name="usergroupList" required="true">
		<cfargument name="tenantID" required="true">
		<cfargument name="username" required="true">


		<cfset var rolelist = application.com.jasperserver.convertUserGroupListToJSRoleList(usergrouplist)>

		<cfquery datasource ="jasperserver">
			delete
				jiuserrole
			from
				jirole r
					inner join
				jiuserrole ur  on r.id = ur.roleid
					inner join
				jiuser u on u.id = userid
					inner join
				jitenant t on t.id = u.tenantid
			where
					u.username =  <cf_queryparam value="#arguments.username#" CFSQLTYPE="CF_SQL_VARCHAR" >  and t.tenantid =  <cf_queryparam value="#arguments.tenantid#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and rolename not in ('role_user',#listQualify(roleList,"'")#)

		</cfquery>


	</cffunction>



	<cffunction name="initialiseJasperServerUser" output="false" returntype="struct">

		<cfset var theUrl = "#getJasperServerPath()#/?">
		<cfset var params = getJasperServerLoginParamsForCurrentUser (newUser = true)>
		<cfset var result = {isOk=true,message=""}>
		<cfset var cfhttpResult = "">

		<cfhttp url="#theURL##params#" method="GET" result="cfhttpResult" timeout="30">  <!--- when we have an initial hit and have to create the organisation, it seems to timeout with 10 sec. Have increated it to 30 --->

		<cfif getJasperServerUserIDForCurrentUser() is "">
			<cfif structKeyExists(cfhttpResult,"errorDetail") and cfhttpResult.errorDetail neq "">
				<cfset result.message = cfhttpResult.errorDetail>
			<cfelse>
				<cfset result.message = "The initialise webservice call was made ok, but the user still doesn't exist. It may be an indication that the jasperserver datasource is not pointing to the correct database.">
			</cfif>
			<cfset result.isOK = false>
		<cfelse>
			<cfset updateUserFolderName()>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getJasperServerWebServicePassword" output="false">
		<cfreturn "$F0nD4t10N-n3Tw0Rk##R314yW4r3!">
		<!--- <cfreturn application.com.settings.getSetting("reportManager.webServicePassword")> --->
	</cffunction>

	<cffunction name="getJasperServerPath" output="false">

		<!--- for all non-dev sites, stick 'reports' in front of the url to obtain the jasperserver url used. Remove "wwww" if it exists --->
		<!--- WAB 2014-01-22 Behind the load balance we cannot use cgi.SERVER_PORT_SECURE to test for secure --->
		<cfset var jasperServerVersion = "52">

		<cfif application.testSite neq 2>
			<!--- WAB 2016-01-26 BF-382 Change report domain so that aaaa.bbbb.com goes to aaaa.reports.bbbb.com rather than reports.aaaa.bbbb.com
					Allows all clients to run off same relayware certificate
			 --->
			<cfreturn request.currentSite.httpProtocol&reReplace(cgi.HTTP_HOST,"(\A[^.]+\.)","\1reports.")&":"&IIF(request.currentSite.isSecure,DE("8443"),DE("18080"))&"/jasperserver#jasperServerVersion#">
		<cfelse>
			<!--- if on the dev 1 environment, point to the dev1 repository which is the 'masterRepository' (ie. the repository used to configure what goes into version control) --->
			<cfif cgi.HTTP_HOST eq "dev1-relayint">
				<cfreturn "http://rw-dev1:18080/jasperserver#jasperServerVersion#">
			<!--- all other dev environments should be pointing to the dev jasper server environment, which is currently on dev5 --->
			<cfelse>
				<cfreturn "http://rw-dev5:8080/jasperserver#jasperServerVersion#">
			</cfif>
		</cfif>
	</cffunction>

	<!--- 2013/01/25	YMA		CASE 433364 changed viewReportFlow to viewAdhocReportFlow as filters were buggy using previouse method. --->
	<cffunction name="getJasperServerReportUnitPath" output="false" hint="URL for accessing a report Unit - just put the URI on the end">
		<cfreturn "#application.com.jasperServer.getjasperserverpath()#/flow.html?_flowId=viewAdhocReportFlow&decorate=no&reportUnit=">
	</cffunction>



	<cffunction name="getTenantID" output="false">
		<cfargument name="applicationScope" default="#application#">

		<cfset var getDatabaseID = "">
		<cfset var tenantID = "">

		<!--- if we're on the dev1 site, the set the tenantId to be 'relaywaremaster'.. this is the repository used to configure what goes into version control. The tenant name is import as the build scripts
			look for relaywaremaster --->
		<cfif applicationScope.testSite eq 2 and listfindnocase("dev1-relayint,dev14-2013int,dev14-2013ext,dev14-2012int,dev14-2012ext,dev6-relayware83Int,dev2-roadmapInt,dev2-roadmapExt",cgi.HTTP_HOST)>
			<cfset tenantID = "relaywareMaster">
		<cfelse>
			<cfquery name= "getDatabaseID" datasource = "#applicationScope.sitedatasource#">
				select db_name() as databaseID
			</cfquery>

			<cfset tenantID = lcase(rereplace(getDatabaseID.databaseid,"[^A-Za-z0-9]","","ALL"))>
		</cfif>

		<cfreturn tenantID>
	</cffunction>


	<cffunction name="getTenantRecord">

		<cfset var getTenant = "">
		<cfset var tenantID = getTenantID()>

		<cfquery name= "getTenant" datasource ="jasperserver">
			select * from
			jiTenant
			where tenantid =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn getTenant>
	</cffunction>


	<cffunction name="getJasperServerUserNameForCurrentUser" output="false" >
		<cfargument name="currentUser" default = "#request.relayCurrentUser#">

		<cfreturn getJasperServerUserNameForUser(personID=currentUser.personID,isInternal=currentUser.isInternal)>
	</cffunction>


	<cffunction name="getJasperServerUserNameForUser" output="false" >
		<cfargument name="personId" type="numeric" require="true">
		<cfargument name="isInternal" type="boolean" default="true">

		<cfreturn arguments.personId&"_"& iif(arguments.isInternal,DE("int"),DE("ext"))>
	</cffunction>


	<cffunction name="getJasperServerUserIDForCurrentUser" output="false">
		<cfargument name="currentUser" default = "#request.relayCurrentUser#">

		<cfreturn getJasperServerUserIDForUser(personId=currentUser.personID,isInternal=currentUser.isInternal)>
	</cffunction>


	<cffunction name="getJasperServerUserIDForUser" output="false">
		<cfargument name="personID" type="numeric" required="true">
		<cfargument name="isInternal" type="boolean" default="true">

		<cfset var tenantID = getTenantID()>
		<cfset var getUserid = "">
		<cfset var username = getJasperServerUserNameForUser(personId=arguments.personId,isInternal=arguments.isInternal)>

		<cfquery name= "getUserid" datasource ="jasperserver">
			select u.id from
			jiUser u inner join JITenant t on u.tenantid = t.id
			where
				username =  <cf_queryparam value="#username#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and t.tenantid =  <cf_queryparam value="#tenantID#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfreturn getUserid.id>
	</cffunction>


	<cffunction name="AddProfileAttributeForCurrentUser" output="false">
		<cfargument name="attrName" required="true">
		<cfargument name="attrValue" required="true">
		<cfargument name="isList" default="false">

		<cfreturn AddProfileAttribute (attrName = attrName,attrValue = attrValue, userID = getJasperServerUserIDForCurrentUser(), isList = isList)>
	</cffunction>


	<cffunction name="AddProfileAttribute" output="false">
		<cfargument name="attrName" required="true">
		<cfargument name="attrValue" required="true">
		<cfargument name="userID" required="true">
		<cfargument name="isList" default="false">

		<cfset var thisValue = "">
		<cfset var AddAttribute = "">

		<cfif not arguments.isList>
			<cfquery name= "AddAttribute" datasource ="jasperserver">
			if exists (select 1 from JIProfileAttribute
						where principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
								and attrName =  <cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >
								and principalObjectID  =  <cf_queryparam value="#arguments.userID#" CFSQLTYPE="cf_sql_integer" >
						)
				BEGIN
					update JIProfileAttribute
					set attrValue =  <cf_queryparam value="#arguments.attrValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
					where
						principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and attrName =  <cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and principalObjectID  =  <cf_queryparam value="#arguments.userID#" CFSQLTYPE="cf_sql_integer" >

				END
			ELSE
				BEGIN
					insert into
					JIProfileAttribute
						(principalObjectClass,principalObjectID, attrName, attrValue)
					values (<cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.userID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.attrValue#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				END
			</cfquery>

		<cfelse>
			<!--- isList --->
			<cfquery name= "AddAttribute" datasource ="jasperserver">
			delete from
			JIProfileAttribute
					where principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and attrName =  <cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and principalObjectID  =  <cf_queryparam value="#arguments.userID#" CFSQLTYPE="cf_sql_integer" >
						and attrValue  not in ( <cf_queryparam value="#arguments.attrValue#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				<cfloop index="thisValue" list="#attrValue#">
					if not exists (select 1 from JIProfileAttribute
									where principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
										and attrName =  <cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >
										and principalObjectID  =  <cf_queryparam value="#arguments.userID#" CFSQLTYPE="cf_sql_integer" >
										and attrValue =  <cf_queryparam value="#thisValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
										)
						BEGIN
							insert into
								JIProfileAttribute
									(principalObjectClass,principalObjectID, attrName, attrValue)
							values (<cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#arguments.userID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#arguments.attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#thisValue#" CFSQLTYPE="CF_SQL_VARCHAR" >)
						END
				</cfloop>
			</cfquery>
		</cfif>

		<cfreturn true>
	</cffunction>


	<cffunction name="getProfileAttributesForCurrentUser" output="false">
			<cfset var getAttributes = "">
			<cfquery name= "getAttributes" datasource ="jasperserver">
			select attrName, attrValue from JIProfileAttribute
			where
						principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and principalObjectID =  <cf_queryparam value="#getJasperServerUserIDForCurrentUser()#" CFSQLTYPE="CF_SQL_INTEGER" >

			</cfquery>
		<cfreturn getAttributes>
	</cffunction>


	<cffunction name="getProfileAttributeForCurrentUser" output="false">
		<cfargument name="attrName" required="true">

			<cfset var getAttribute = "">
			<cfquery name= "getAttribute" datasource ="jasperserver">
			select attrName, attrValue from JIProfileAttribute
			where
						principalObjectClass =  <cf_queryparam value="#principalObjectClass.user#" CFSQLTYPE="CF_SQL_VARCHAR" >
						and principalObjectID =  <cf_queryparam value="#getJasperServerUserIDForCurrentUser()#" CFSQLTYPE="CF_SQL_INTEGER" >
						and attrName =  <cf_queryparam value="#attrName#" CFSQLTYPE="CF_SQL_VARCHAR" >

			</cfquery>
		<cfreturn getAttribute>
	</cffunction>


	<cffunction name="doesFolderExist">

		<cfscript>
			var jasperRepositoryObj = CreateObject( "java", "com.jaspersoft.ji.repository.metadata.common.service.RepositoryService") ;
		</cfscript>
		<cfset jasperRepositoryObj.init()>
		<cfdump var="#jasperRepositoryObj#">

	</cffunction>

	<cffunction name="updateUserFolderName" output="false">

		<cfset var username = getJasperServerUserNameForCurrentUser()>
		<cfset var tenantURI = application.com.jasperServer.getTenantRecord().tenantFolderUri>		<!--- 2012-10-01 PPB Case 430511 explicitly qualify the "path" to the function (ie application.com.jasperServer.) --->
		<cfset var updateUserFolder = "">

		<cfquery name="updateUserFolder" datasource="jasperServer">
			update jiResourceFolder set label= <cf_queryparam value="#request.relayCurrentUser.person.userName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			where name =  <cf_queryparam value="#username#_home" CFSQLTYPE="CF_SQL_VARCHAR" >
				and uri  like  <cf_queryparam value="#tenantURI#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

	</cffunction>

	<cffunction name="doesCurrentUserHaveAccessToObject" access="public" returnType="boolean">
		<cfargument name="uriString" type="string" required="true">
		<cfargument name="method" type="string" default="webservice">

		<cfset var userID = getJasperServerUserIDForCurrentUser()>

		<cfreturn doesUserHaveAccessToObject(uriString=arguments.uriString,userID=userID,method=arguments.method)>
	</cffunction>

	<cffunction name="doesUserHaveAccessToObject" access="public" returnType="boolean">
		<cfargument name="uriString" type="string" required="true">
		<cfargument name="userID" type="string" default="#application.com.jasperServer.getJasperServerUserNameForCurrentUser()#">
		<cfargument name="method" type="string" default="webservice">

		<cfset var tenantID = getTenantID()>
		<cfset var objectPermissions = "">
		<cfset var hasAccess = true>
		<cfset var getUserRoles = "">
		<cfset var role = "">
		<cfset var getPermissions = "">
		<cfset var username = "">

		<cfif arguments.method eq "webservice">
			<cfquery name="getUserRoles" datasource="jasperServer">
				select distinct r.rolename from jiuserrole ur
					inner join jiuser u on ur.userid =u.id
					inner join jitenant t on u.tenantid=t.id
					inner join jirole r on ur.roleid =r.id
				where u.id =  <cf_queryparam value="#arguments.userID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfset objectPermissions = application.com.jasperServerWebServiceCaller.getPermissionsForObject(uriString=arguments.uriString)>

			<cfloop list="#objectPermissions#" index="role">
				<cfif role contains "ROLE_MOD_" and not listFindNoCase(valueList(getUserRoles.rolename),role)>
					<cfset hasAccess = false>
					<cfbreak>
				</cfif>
			</cfloop>

		<cfelseif arguments.method eq "database">

			<cfset username = application.com.jasperServer.getJasperServerUserNameForCurrentUser()>

			<cfquery name="getPermissions" datasource="jasperServer">
				select * from vObjectPermissionAllowed
				where uri  like  <cf_queryparam value="%#arguments.uriString#" CFSQLTYPE="CF_SQL_VARCHAR" >
					and username =  <cf_queryparam value="#username#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfset hasAccess = getPermissions.recordCount>
		</cfif>

		<cfreturn hasAccess>
	</cffunction>

	<cffunction name="getDashboards" type="public" returntype="query" validvalues="true">
		<cfargument name="restrictByCurrentUserRights" type="numeric" default="true" hint="Get dashboards that user has access to">
		<cfargument name="showInternalExternal" type="string" default="" hint="Get internal or external dashboards">

		<cfset var tenantURI = application.com.jasperServer.getTenantRecord().tenantFolderUri>	<!--- 2012-10-01 PPB Case 430511 explicitly qualify the "path" to the function (ie application.com.jasperServer.) --->
		<cfset var getDashboardQry = "">
		<cfset var resourceTypes = "">
		<cfset var userID = application.com.jasperServer.getJasperServerUserIDForCurrentUser()>

		<cfif arguments.showInternalExternal eq "external">
			<cfset resourceTypes = "portal_resources">
		<cfelseif arguments.showInternalExternal eq "internal">
			<cfset resourceTypes = "internal_resources">
		</cfif>

		<cfquery name="getDashboardQry" datasource="jasperserver">
			select distinct replace(rf.uri,<cf_queryparam value="#tenantURI#" CFSQLTYPE="CF_SQL_VARCHAR" >,'')+'/'+r.name as dashboardURI, rf.uri
			from jidashboard d inner join jiresource r
				on r.id = d.id inner join jiresourceFolder rf
				on r.parent_folder = rf.id inner join jidashboardresource dr
				on dr.dashboard_id = d.id
				<cfif arguments.restrictByCurrentUserRights>
					inner join dbo.getFolderPermissions(<cf_queryparam value="#userID#" CFSQLTYPE="CF_SQL_INTEGER" >) p on p.uri = rf.uri
				</cfif>
			where rf.uri  like  <cf_queryparam value="#tenantURI#%/#resourceTypes#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				and rf.hidden = 0
			order by rf.uri
		</cfquery>

		<cfreturn getDashboardQry>
	</cffunction>


	<cffunction name="getBackUpResources" returntype="query">
		<cfargument name="id" type="numeric" required="true">

		<cfset var getBackupDomainsQry = "">

		<cfquery name="getBackupDomainsQry" datasource="jasperserver">
			select backup_id,created from jifileresourcebackup
			where id=#arguments.id#
		</cfquery>

		<cfreturn getBackupDomainsQry>
	</cffunction>

</cfcomponent>