<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

methods used when email merging withing communications

various variables are available in request scope

request.commitem is a structure holding all the columns of the current row of the getSelectionInfo query
request.thisComm

2008-02-21 AJC P-SNY041 encryption functionality extension
2008-09-04 AJC CR-TND561 Inactive 
2009/06     WAB added test parameter to calls to createseidquerystring
2009/10/02	NJH	LID 2725 - changed request.thisComm.test to request.commItem.test
2010/11/11 AJC P-LEN022 CR011 Add view online link function
WAB 2010/10/12 Removed references to application.internalUser Domains, replaced with application.com.relayCurrentSite.getReciprocalInternalDomain()
2010/12/21	NJH LID 4923 - replaced #request.currentSite.httpprotocol##request.thisComm.linkToSiteURL# with #request.thisComm.linkToSiteURLWithProtocol#
			as that will then include the external protocol, rather than the current site protocol.
 --->

<cfcomponent displayname="Functions for merging things into Communications" hint="">


	<cffunction access="public" name="URLParameterForUnrestrictedLinkToSecurePage" return="string" output="no">
		<cfargument name="elementid" type="string" required="true" hint="the elementid of the page to go to">
		<cfargument name="personid" type="numeric" default = 0>
		<cfargument name="commid" type="numeric" default = 0>
		<cfargument name="days" type="numeric" default="30">			
		
		
		<cfset var theString = application.com.relayelementTree.createSEIDQueryString (linktype = "unrestricted",elementid = elementid, personid = personid, commid = commid, days=days)>
		<cfreturn theString>

	</cffunction>
	
	<!--- 
	This creates a link to a page which usually requires a login.
	Clicking on this link allows the user to go to this page (and its parents) without logging in
	 --->
	 
	<cffunction access="public" name="SecurePageLink" return="string" output="no">
	
		<cfargument name="elementid" type="string" required="true">
		<cfargument name="days" type="numeric" default="30">			
		<cfargument name="additionalParameters" type="string" default="">			
	
		<cfset var result = "">
		<cfset var extraParams = "">
		
		<cfset var seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "unrestricted",elementid = elementid, personid = request.CommItem.personid, commid = request.thisComm.commid, days=days, test = request.commItem.test)>	<!--- WAB LID 2361 added test parameter ---><!--- NJH 2009/10/02 LID 2725 --->	

		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters> 
		</cfif>

		<!--- LID 4923 - NJH 2010/12/21 replaced with below
		<cfset result = "#request.currentSite.httpprotocol##request.thisComm.linkToSiteURL#?#seidQueryString##extraParams#"> --->
		<cfset result = "#request.thisComm.linkToSiteURLWithProtocol#?#seidQueryString##extraParams#">
	
		<cfreturn result>
	
	</cffunction>


	<cffunction access="public" name="RelayPageLink" return="string" output="no">
	
		<cfargument name="elementid" type="string" required="true">
		<cfargument name="additionalParameters" type="string" default="">			
	
		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">
		
		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters> 
		</cfif>

		<cfset theString = "&a=e&e=#elementID#">
		
		<cfset result = request.CommItem.LinkToRemoteCFM & theString  & extraParams>
	
		<cfreturn result>
	
	</cffunction>



	<!--- 2009-01-05 NYB CR-LEX585 - added function: --->
	<cffunction access="public" name="AutoLoginLink" return="string" output="no">
		<cfargument name="elementid" type="string" required="true">
		<cfargument name="additionalParameters" type="string" default="">			
	
		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">
		
		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters> 
		</cfif>

		<cfset seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "autologin",elementid = elementid, personid = request.CommItem.personid, commid = request.thisComm.commid, test = request.commItem.test)>	<!--- WAB LID 2361 added test parameter ---><!--- NJH 2009/10/02 LID 2725 --->	
		<!--- LID 4923 - NJH 2010/12/21 replaced with below
		<cfset result = "#request.currentSite.httpprotocol##request.thisComm.linkToSiteURL#?#seidQueryString##extraParams#"> --->
		<cfset result = "#request.thisComm.linkToSiteURLWithProtocol#?#seidQueryString##extraParams#">
		
		<cfreturn result>
	</cffunction>	


	
	<!--- this version by passes remote.cfm and goes straight to the portal--->
	<cffunction access="public" name="RelayPageLinkV2" return="string" output="no">
	
		<cfargument name="elementid" type="string" required="true">
		<cfargument name="additionalParameters" type="string" default="">			
	
		<cfset var result = "">
		<cfset var extraParams = "">
		<cfset var theString = "">
		
		<cfif additionalParameters is not "" and additionalParameters contains "=">
			<cfset extraParams = "&" & additionalParameters> 
		</cfif>

<!--- 		<cfset theString = "p=#request.CommItem.magicNumber#&cid=#request.thisComm.commid#&eid=#elementID#">
		<cfset result = "#request.currentSite.httpprotocol##request.thisComm.linkToSiteURL#?#theString##extraParams#">
 --->
		<cfset seidQueryString = application.com.relayelementTree.createSEIDQueryString (linktype = "tracked",elementid = elementid, personid = request.CommItem.personid, commid = request.thisComm.commid,test=request.commItem.test)>  <!--- WAB LID 2361 added test parameter --->	
		<!--- LID 4923 - NJH 2010/12/21 replaced with below
		<cfset result = "#request.currentSite.httpprotocol##request.thisComm.linkToSiteURL#?#seidQueryString##extraParams#"> --->
		<cfset result = "#request.thisComm.linkToSiteURLWithProtocol#?#seidQueryString##extraParams#">
		
		<cfreturn result>
	</cffunction>	


	<!--- output value of a flagGroup for person/location/org --->
	<cffunction access="public" name="showProfileValue" return="string" output="no">
		<cfargument name="profileID" type="string" required="true">

		<cfset var result = "">
		<cfset var flagGroup  = "">
		
		<cfif not application.com.flag.doesFlagGroupExist (profileID)>
			<cfthrow message = "Flag Group does not exist:  #htmleditformat(profileID)#" extendedInfo = "#htmleditformat(profileID)#"> 
		</cfif>
		
		<cfset flagGroup  = application.com.flag.getFlagGroupStructure(profileID)>
		<cfif listfind("0,1,2",flaggroup.entityType.entitytypeid) is 0 
			and (not structKeyExists(request,"CommItem") and structKeyExists(request.CommItem,"entityTypeID") and request.CommItem.entityTypeID eq flaggroup.entityType.entitytypeid)>
			<cfreturn "">
		</cfif>

		<cfif structKeyExists(request,"CommItem") and structKeyExists(request.CommItem,"entityTypeID") and structKeyExists(request.CommItem,"entityID")>
			<cfset result=application.com.flag.getBooleanFlagList(entityID=#request.CommItem.entityID#,flagGroupID=#flagGroup.flagGroupID#).names>
			<cfreturn result>
		<cfelse>
			<cfquery name = "result" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,0,2)#">  <!--- just caches for 2 seconds (ie this template!) --->
			select dbo.BooleanFlagList (#flagGroup.flagGroupID#,#flaggroup.entityType.uniquekey#,default) as flagList from person where personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>		
			<cfreturn result.flagList>
		</cfif>

	</cffunction>	

		<!--- output value of a flag or person/location/org --->
	<cffunction access="public" name="showProfileAttributeValue" return="string" output="no">
		<cfargument name="profileAttributeID" type="string" required="true">

		<cfset var result = "">
		<cfset var temp = "">
		<cfset var flag  = "">		
		
		<cfif not application.com.flag.doesFlagExist (profileAttributeID)>
			<cfthrow message = "Flag does not exist:  #htmleditformat(profileAttributeID)#" extendedInfo = "#htmleditformat(profileAttributeID)#"> 
		</cfif>


		<cfset  flag  = application.com.flag.getFlagStructure(profileAttributeID)>
		<cfif listfind("0,1,2",flag.entityType.entitytypeid) is 0
			and (not structKeyExists(request,"CommItem") and structKeyExists(request.CommItem,"entityTypeID") and request.CommItem.entityTypeID eq flaggroup.entityType.entitytypeid)>
			<cfreturn "">
		</cfif>
		<cfif structKeyExists(request,"CommItem") and structKeyExists(request.CommItem,"entityTypeID") and structKeyExists(request.CommItem,"entityID")>
			<cfset temp = application.com.flag.getFlagData (entityID = request.commItem.entityID, flagid = flag.flagid)>
		<cfelseif structKeyExists(request,"CommItem") and structKeyExists(request.CommItem,"personid")>
			<cfset temp = application.com.flag.getFlagDataForPerson (personid = request.CommItem.personid, flagid = flag.flagid)>
		<cfelseif structKeyExists(request,"relaycurrentuser") and structKeyExists(request.relaycurrentuser,"personid")>
			<cfset temp = application.com.flag.getFlagDataForPerson (personid = request.relaycurrentuser.personid, flagid = flag.flagid)>
		</cfif>
		<cfif isdefined("temp.data_name")>
			<cfset result = temp.data_name>
		<cfelseif isdefined("temp.data")>
			<cfset result = temp.data>
		<cfelseif temp.recordcount is not 0>			
			<cfset result = true>   <!--- not sure what best to bring back here - ideally for booleans should use showProfileValue --->
		<cfelse> 
			<cfset result = false>
		</cfif>

		<cfreturn result>	
		
	</cffunction>	
	
	<cffunction access="public" name="TrackedLink" return="string" output="no">
		<!--- adds correct stuff to end of a remote.cfm link to go to a url--->
		<cfargument name="URL" type="string" required="true">
		
	
		<cfset var theString = "&a=g&u=#urlencodedformat(url)#">
		<cfset var result = request.CommItem.LinkToRemoteCFM & theString>
		<cfreturn result>

	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             2006/03/16 - GCC - CR_SNY588 Set Password generatePasswordLink											
			Modified by WAB so that can be used in a comm (needs to look for request.CommItem.personid)
	  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->	
	<cffunction access="public" name="generatePasswordLink" hint="Creates an encrypted string to add to a link to enable a user to set/reset their password" output="no">
		<cfargument name="personID" type="numeric">
		<cfargument name="reset" type="boolean" default="true">

		<cfset var generateRandomPassword ="">
		<cfset var getFullPersonDetails ="">
		<cfset var link ="">

		<cfif isDefined("request.CommItem")>
			<cfset arguments.personid = request.CommItem.personid>
		<cfelseif arguments.personid is "">	
			<cfoutput>Function generatePasswordLink: PersonID required</cfoutput><CF_ABORT><!--- actuall this will never be outputted because output is OFF - which it must be --->
		</cfif>
		
		
		<cfif arguments.reset>		
			<cfset generateRandomPassword = application.com.login.createUserNamePassword(personID=arguments.personID)>
		</cfif>
		<cfset getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID=arguments.personID)>
		<cfset link = arguments.personID & "#application.delim1#" & now() & "#application.delim1#" & getFullPersonDetails.password>
		<!--- 2008-02-21 AJC P-SNY041 encryption functionality extension --->
		<cfset encryptedLink = application.com.encryption.encryptString(string=link)>
		<cfreturn encryptedLink>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            PKP: 2008-02-01 : REWORK GENERATE PASSWORD LINK FOR INTERNAL USERS
							adding site to link that is to be encrypted
	  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->	
	<cffunction access="public" name="generateInternalPasswordLink" hint="Creates an encrypted string to add to a link to enable a user to set/reset their password" output="no">
		<cfargument name="personID" type="numeric">
		<cfargument name="reset" type="boolean" default="true">

		<cfset var generateRandomPassword ="">
		<cfset var getFullPersonDetails ="">
		<cfset var link ="">

		<cfif isDefined("request.CommItem")>
			<cfset arguments.personid = request.CommItem.personid>
		<cfelseif arguments.personid is "">	
			<cfoutput>Function generatePasswordLink: PersonID required</cfoutput><CF_ABORT><!--- actuall this will never be outputted because output is OFF - which it must be --->
		</cfif>
		
		
		<cfif arguments.reset>		
			<cfset generateRandomPassword = application.com.login.createUserNamePassword(personID=arguments.personID)>
		</cfif>
		<cfset getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(personID=arguments.personID)>
		<cfset link = arguments.personID & "#application.delim1#" & now() & "#application.delim1#" & getFullPersonDetails.password& "#application.delim1#" & application.com.relayCurrentSite.getReciprocalExternalDomain()>
		<cfset encryptedLink = application.com.encryption.encryptString(string=link)>
		<cfreturn encryptedLink>
	</cffunction>


	<cffunction access="public" name="redirectURL" hint="URL setsup a redirect link for a email">
		<cfargument name="action" type="string" default="i">
		<cfargument name="personid" type="numeric" required="true">
		<cfargument name="templatepassed" type="string" default = "">
		<cfargument name="urlparams" Type="string" default = "">
		
		<cfset magicnumber = application.com.login.getMagicNumber(entityID=#personid#,entityTypeID=0)>
		<cfset encodedtargeturl = templatepassed & '?' & urlparams>
		
		<cfset urlpassed = request.currentSite.httpprotocol & request.currentsite.internaldomain & '/remote.cfm?' & 'a=' & action & '&p=' & magicnumber & '&relocateTo='&urlencodedformat(encodedtargeturl)>
		
		
		<cfreturn urlpassed>
	</cffunction>
	
	<cffunction name="if" return="string" output="no" access="public">
		<cfargument name="condition" required="yes" type="string">
		<cfargument name="trueResult" required="yes" type="string">
		<cfargument name="falseResult" required="yes" type="string">
		
		<cfset var result = "">

		<cfif condition>
			<cfset result=trueResult>
		<cfelse>
			<cfset result=falseResult>
		</cfif>

		<cfreturn result>
	
	</cffunction>
	
	<!--- START: 2010/11/11 AJC P-LEN022 CR011 Add view online link function --->
	<cffunction access="public" name="RelayCommunicationLink" return="string" output="no">
	
	
		<cfset theString = "&a=c">
		
		<cfset result = request.CommItem.LinkToRemoteCFM & theString >
	
		<cfreturn result>
	
	</cffunction>
	<!--- END: 2010/11/11 AJC P-LEN022 CR011 Add view online link function --->
	
</cfcomponent>	



