<!--- �Relayware. All Rights Reserved 2014 --->
<!---
AMENDMENT HISTORY

2008-07-25		NYF		Bug 803: created getValidMenuItemIDs function and added to getMenuItems in place of other code
2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Added getMenuXMLDoc().  Altered loadRelayMenu() to deal with client extensions
2013-02-19 		PPB 	Case 433500 remove the need for the nodes in RelayMenuItemsExtension.xml to be case-specific (WAB applied original 2012Summer fix applied to new code)
2013-08-14		YMA		Case 436224 Update getApps to run security task check.
2014-02-24		IH		Remove "content" from standardMenuItemModules
--->


<cfcomponent displayname="Common Menu Queries" hint="Contains a number of commonly used Menu Queries">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->

	<cffunction name="getSubMenuNavItems" access="public" hint="Returns a query of submenu navigation items within the menu XML structure that the user has access to">
		<cfargument name="module" required="yes" type="string">
		<cfargument name="subMenuNavItem" required="yes" type="string">
		<!--- NJH 2010/08/05 RW8.3 no longer needed <cfargument name="relayMenuXMLFile" required="no" type="string" default="#application.path#\xml\RelayMenuItemsv3.xml"> --->  <!--- NJH changed the default value filename to include the path as well --->
		<cfset myNavItemMenu = "">

		<!--- <cfset menuItemsInModule = XmlSearch(getMenuXMLDoc(),"/MenuData/MenuItem[translate(@Module,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.module)#']/MenuAction[contains(translate(@TabText,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'#lcase(arguments.subMenuNavItem)#')]/SubMenuAction/")> --->
		<cfset menuItemsInModule = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=getMenuXMLDoc(),xPathString="/MenuData/MenuItem[translate(@Module,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(arguments.module)#']/MenuAction[contains(translate(@TabText,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'#lcase(arguments.subMenuNavItem)#')]/SubMenuAction/",lockName="applicationMenu")>

		<cfset relayMenuXML = StructNew()>

		<cfset size = ArrayLen(menuItemsInModule)>

		<cfset subMenuNavItemsQuery = QueryNew("Name,URL,Grouping","varchar,varchar,varchar")>
		<cfif size gt 0>
			<cfset temp = QueryAddRow(subMenuNavItemsQuery,#size#)>
			<cfloop index="menuItemIdx" from="1" to="#size#">
				<!--- WAB 2007-04-11 needed a way of not showing some reports on particular sites so added the concept of a Condition (not required)
					ideally we would need to be able to read the ini file for a particular group, but for now will have to use existing variables or request variables
				--->
				<cfset showItem = true>
				<cfif structKeyExists (menuItemsInModule[menuItemIdx].XmlAttributes, "condition")>
					<cftry>
						<cfset showItem = evaluate(menuItemsInModule[menuItemIdx].XmlAttributes.condition)>
						<cfcatch>
							<cfset showItem =false><cfoutput> #menuItemsInModule[menuItemIdx].XmlAttributes.Name#</cfoutput>
						</cfcatch>
					</cftry>
				</cfif>

				<cfif showItem>
					<!--- NJH 2006/07/07 Do we have rights to see this reports? --->
					<cfset menuItemSecurityLevel = "dataTask:Level1">
					<cfif structKeyExists(menuItemsInModule[menuItemIdx].XmlAttributes,"Security")>
						<cfset menuItemSecurityLevel = menuItemsInModule[menuItemIdx].XmlAttributes.Security>
					</cfif>
					<cfif application.com.login.checkInternalPermissions(listfirst(menuItemSecurityLevel,":"),listlast(menuItemSecurityLevel,":"))>
						<cfset temp = QuerySetCell(subMenuNavItemsQuery,"Name",#menuItemsInModule[menuItemIdx].XmlAttributes.Name#,#menuItemIdx#)>
						<cfset temp = QuerySetCell(subMenuNavItemsQuery,"URL",#menuItemsInModule[menuItemIdx].XmlAttributes.URL#,#menuItemIdx#)>
						<cfset temp = QuerySetCell(subMenuNavItemsQuery,"Grouping",#menuItemsInModule[menuItemIdx].XmlAttributes.Group#,#menuItemIdx#)>
					</cfif>
				</cfif>
			</cfloop>
		</cfif>

		<cfquery name="myNavItemMenu" dbtype="query">
			select name, url, grouping from subMenuNavItemsQuery where name is not null
		</cfquery>

		<cfreturn myNavItemMenu>
	</cffunction>




	<cffunction name="getValidMenuItemIDs" access="public" hint="Returns a comma separated list of valid menu modules">
		<cfscript>
			//var standardMenuItemIDs = "60,602,73,6,63,66,631,661,662,604,664";
			var standardMenuItemModules = "Administer,Security,Admin,ContentManager,DataTools,Workflow,MyFavorites,Screens,Config";
			var activeModulesQry = getActiveModules();
			var validMenuItemModules = listAppend(valueList(activeModulesQry.moduleTextID),standardMenuItemModules);
		</cfscript>
		<cfreturn validMenuItemModules>
	</cffunction>

	<!--- WAB 2013-02-13 altered to merge in the client extensions, previously done on an adhoc basis in various places in the code --->
	<cffunction name="loadRelayMenu" access="public" hint="Loads the menu xml file into application scope">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="defaultMenuFile" type="string" default="#applicationscope.paths.relayware#\xml\RelayMenuItems.xml">



			<cfset menuFileName = "#defaultMenuFile#">
			<cfif fileExists("#applicationScope.paths.abovecode#\xml\relayMenuItemsOverride.xml")>
				<cfset menuFileName = "#applicationScope.paths.abovecode#\xml\relayMenuItemsOverride.xml">
			</cfif>


			<cfset coreMenuXMLDoc = application.com.xmlfunctions.readAndParseXMLFile(menuFileName).XML>

			<cfset clientExtensionFile = "#application.paths.abovecode#/xml/RelayMenuItemsExtension.xml">
			<cfif fileExists (clientExtensionFile)>
				<cfset clientMenuXMLDoc = application.com.xmlfunctions.readAndParseXMLFile("#application.paths.abovecode#/xml/RelayMenuItemsExtension.xml").xml>

				<cfset args = {coreMenuXMLDoc = coreMenuXMLDoc}>
				<cfset application.com.xmlfunctions.runFunctionAgainstNodes (xmlnodearray = clientMenuXMLDoc.MenuData.xmlchildren,function = mergeClientExtensions, additionalArgs = args)>
			</cfif>

			<cfset application.com.xmlfunctions.runfunctionagainstnodes(xmlnodearray = coreMenuXMLDoc.xmlroot,function = AddIDsToMenu)>

		  	<cfset applicationScope.menuXMLdoc = coreMenuXMLDoc>

	</cffunction>


	<!--- Function to merge ClientExtensions into the core Menu file
			Called by runfunctionagainstnodes
	 --->
	<cffunction name = "mergeClientExtensions">
		<cfargument name="xmlnode">
		<cfargument name="coreMenuXMLDoc">

		<!--- Search for node with same itemName --->
		<!--- <cfset searchResult = xmlSearch (arguments.coreMenuXMLDoc,"//MenuItem[translate(@ItemName,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(xmlnode.xmlattributes.itemName)#']")> --->
		<cfset searchResult = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=arguments.coreMenuXMLDoc,xPathString="//MenuItem[translate(@ItemName,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(xmlnode.xmlattributes.itemName)#']",lockName="applicationMenu")>

		<cfif arrayLen(searchResult)>
			<cfset var coreXMLNode = searchResult[1]>
			<!--- Loop through children of XMLnode (which are menuActions)
  				 See if there is a matching menuAction in the core Menus  (match on attribute Tabtext)
				If there is a matching menuAction, then we need to append the children to the core menuAction
				If there is no matching menuAction, we need to create a new one and copy the children
			 --->
			<cfloop array="#xmlNode.xmlChildren#" index="extensionMenuAction">
				<!--- Search for action --->
				<!--- <cfset var menuActionSearch = xmlSearch (coreXMLNode,"./MenuAction[translate(@TabText,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(extensionMenuAction.xmlattributes.tabText)#']")> --->
				<cfset var menuActionSearch = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=arguments.coreMenuXMLDoc,xPathString="./MenuAction[translate(@TabText,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(extensionMenuAction.xmlattributes.tabText)#']",lockName="applicationMenu")>


				<cfif arrayLen(menuActionSearch)>
					<!--- MenuAction already exists, add children to it --->
					<cfset XmlAppend (menuActionSearch[1],extensionMenuAction,false)>
				<cfelse>
					<!--- MenuAction does not already exists, add whole menuAction--->
					<cfset XmlAppend (coreXMLNode,extensionMenuAction,true)>
				</cfif>

			</cfloop>

			<!--- Copy children of node in extension to node in main Menu --->

		<cfelse>
			<!--- Need to add whole menuAction to end of the core doc --->
			<cfset XmlAppend (coreMenuXMLDoc.MenuData,xmlnode,true)>

		</cfif>

		<cfreturn false><!--- false because we do not want to contiue to the children --->
	</cffunction>

	<!--- Function to add unique ids to each menu item - used by the jQuery menus to keep track of the order
			Called by runfunctionagainstnodes
	 --->
	<cffunction name="AddIDsToMenu">

		<cfargument name="xmlnode">
		<cfargument name="mode" default="start">

		<cfset var fullID = "">

			<cfif mode is "start">
				<cfif xmlnode.xmlname is "SubMenuAction" and structKeyExists(xmlnode.xmlattributes,"id") and structKeyExists(xmlnode.xmlParent.xmlattributes,"id")>
					<cfset FULLID = "nav_" & xmlnode.xmlParent.xmlParent.xmlattributes.module & "_" & xmlnode.xmlParent.xmlattributes.id & xmlnode.xmlParent.xmlattributes.tabtext & "_" & xmlnode.xmlattributes.id>
				<cfelseif xmlnode.xmlname is "MenuAction" and structKeyExists(xmlnode.xmlattributes,"id")>
					<cfset FULLID = "nav_" & xmlnode.xmlParent.xmlattributes.module & "_" & xmlnode.xmlattributes.id>
				<cfelseif xmlnode.xmlname is "menuitem">
					<cfset FULLID = "nav_" & xmlnode.xmlattributes.module >
				</cfif>

				<cfset xmlnode.xmlattributes.fullID = lCase(fullid)>
			</cfif>

	</cffunction>


	<cffunction name="getMenuXMLDoc" access="public" hint="">

		<cfif not structKeyExists(application,"menuXMLDoc")>

			<cfset loadRelayMenu()>

		</cfif>

		<cfreturn application.menuXMLDoc>
	</cffunction>


	<cffunction name="isModuleActive" access="public" returntype="boolean" ouput="false">
		<cfargument name="moduleID" required="true">
		<cfargument name="applicationScope" default="#application#">

		<cfset var activeModules = getActiveModules(moduleID=arguments.moduleID,applicationScope = applicationScope)>

		<cfif activeModules.recordCount eq 0>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>

	</cffunction>

	<cffunction name="getActiveModules" access="public" returnType="query" output="false" validValues="true">
		<cfargument name="moduleID" required="false">
		<cfargument name="applicationScope" default="#application#">

		<cfset var activeModuleQry="">

		<cfquery name="activeModuleQry" datasource="#applicationScope.siteDataSource#">
			select rm.*, 'phr_sys_nav_'+rm.moduleTextId as translatedTitle from relayModule rm with (noLock)
			where rm.active=1
				<cfif structKeyExists(arguments,"moduleID")>
					and <cfif not isNumeric(arguments.moduleID)>rm.moduleTextID='#arguments.moduleID#'<cfelse>rm.moduleID=#arguments.moduleID#</cfif>
				</cfif>
			order by rm.name
		</cfquery>

		<cfreturn activeModuleQry>
	</cffunction>


	<cffunction name="getApps" access="public" returnType="query" output="true" validValues="true">
		<cfargument name="moduleTextID" required="false" default="">
		<cfargument name="excludeModuleTextID" default="" type="string">

		<!--- <cfset var menuXML = application.com.relayMenu.getMenuXMLDoc()>
		<cfset var selectedElements = XmlSearch(menuXML, "//MenuItem")> --->
		<cfset var selectedElements = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=getMenuXMLDoc(),xPathString="//MenuItem",lockName="applicationMenu")>
		<cfset var validMenuItemIDs = application.com.relayMenu.getValidMenuItemIDs()>
		<cfset var qModules = queryNew("")>

		<cfset var aModuleTextID = []>
		<cfset var aModuleName = []>

		<cfloop array="#selectedElements#" index="xmlNode">
			<cfif listfindNoCase(validMenuItemIDs,xmlNode.XmlAttributes.Module) and not listfindNoCase(arguments.excludeModuleTextID,xmlNode.XmlAttributes.Module)>
				<!--- 2013-08-14		YMA		Case 436224 Update getApps to run security task check. --->
				<cfif (structKeyExists(xmlNode.XmlAttributes,"security") and application.com.login.checkSecurityList(xmlNode.XmlAttributes.SECURITY)) or not structKeyExists(xmlNode.XmlAttributes,"security")>
					<cfif arguments.moduleTextID is "" or (arguments.moduleTextID is not "" and arguments.moduleTextID is xmlNode.XmlAttributes.Module)>
						<cfset arrayAppend(aModuleTextID,xmlNode.XmlAttributes.Module)>
						<cfset arrayAppend(aModuleName,application.com.relayTranslations.translateString('phr_sys_nav_#replace(LTrim(xmlNode.XmlAttributes.ItemName)," ","_","ALL")#'))>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>

		<cfset queryAddColumn(qModules,"moduleTextID","VarChar",aModuleTextID)>
		<cfset queryAddColumn(qModules,"moduleName","VarChar",aModuleName)>

		<cfquery dbtype="query" name="qModules">
			select *,upper(moduleName) as sort
			from qModules
			order by sort
		</cfquery>
		<!--- IH - had to add the extra "sort" column to make the sorting case insensitive --->

		<cfreturn qModules>
	</cffunction>

	<cffunction name="getModuleName" access="public" returntype="string">
		<cfargument name="moduleID" required="true">
		<cfargument name="applicationScope" default="#application#">

		<cfset var getModule="">

		<cfquery name="getModule" datasource="#applicationScope.siteDataSource#">
			select * from relayModule with (noLock)
				where <cfif not isNumeric(arguments.moduleID)>moduleTextID='#arguments.moduleID#'<cfelse>moduleID=#arguments.moduleID#</cfif>
		</cfquery>

		<cfreturn getModule.modulename>
	</cffunction>


	<!--- NJH 2015/11/13 Jira Prod-231 function to return the attributes for a given menu item as determined by the scriptname --->
	<cffunction name="getMenuItemByUrl" access="public" hint="Return the attributes for a given menu item as determined by the scriptname" returnType="struct">

		<cfset var menuStruct = {url="",tabText="",description="",security=""}>
		<cfset var menuArray = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuAction[translate(@URL,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(cgi.script_name)#']",lockName="applicationMenu")>
		<cfreturn arrayLen(menuArray)?menuArray[1].xmlAttributes:menuStruct>
	</cffunction>
</cfcomponent>

