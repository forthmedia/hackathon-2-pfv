<!---
  --- singleSignOnSiteControl
  --- --------------
  --- 
  --- author: Richard.Tingle
  --- date:   08/04/16
  --->
<cfcomponent  accessors="true" output="false" persistent="false">


	<cffunction name="getAllSitesSSOBehaviour" access="public" output="false" returntype="Query">
		<!--- siteDefID is an optional argument used by getSiteSSOBehaviour --->
		<cfargument name="siteDefID" type="numeric" required="false">	
		
		<cfquery name="siteBehaviour" datasource="#application.siteDatasource#">	
			select 
				Title, 
				SiteDef.siteDefId, 
				isInternal,
				isNull(defaultAllowRegularLogin,1) as defaultAllowRegularLogin, 
				isNull(defaultAllowSingleSignOn,0) as defaultAllowSingleSignOn,
				isNull(autostartSingleSignOn,0) as autostartSingleSignOn
			from SiteDef
			left join SingleSignOnSPSiteBehaviour on SiteDef.siteDefId=SingleSignOnSPSiteBehaviour.siteDefID
			where 1=1
			<cfif structKeyExists(arguments,"siteDefID")>
				and SiteDef.siteDefId = <cfqueryparam value = "#arguments.siteDefID#" CFSQLType ="CF_SQL_INTEGER">
			</cfif>
			
		</cfquery>
		<cfreturn siteBehaviour/>
	</cffunction>
	
	<cffunction name="getSiteSSOBehaviour" access="public" output="false" returntype="Query">
		<cfargument name="siteDefID" type="numeric" required="true">	
		
		<!---Copy reference in case cache cleared between statements --->
		<cfset var cacheCopy=getCache()>
		
		<cfif structKeyExists(cacheCopy,siteDefID)>
			<cfreturn cacheCopy[siteDefID]>
		<cfelse>
			<cfset siteBehaviour=getAllSitesSSOBehaviour(siteDefID)>
			<cfset this.cache[siteDefID]=siteBehaviour>
			<cfreturn siteBehaviour>
		</cfif>
	</cffunction>

	<cffunction name="getCache" access="private" output="false" returntype="any">
		<cfscript>
			if (not structKeyExists(this,"cache")){
				this.cache={};
			}
			return this.cache;
		</cfscript>
		
	</cffunction>

	<cffunction name="clearCache" access="private" output="false" returntype="void">
		<!--- Reloading cfcs also clears cache --->
		<cfset this.cache={}>
		
	</cffunction>
	
	<cffunction name="updateSiteSSOBehaviour" access="public" output="false" returntype="void">
		<cfargument name="siteDefID" type="numeric" required="true">	
		<cfargument name="defaultAllowRegularLogin" type="boolean" required="true" >
		<cfargument name="defaultAllowSingleSignOn" type="boolean" required="true" >
		<cfargument name="autostartSingleSignOn" type="boolean" required="true" >

		<cfquery name="updateSiteBehaviour" datasource="#application.siteDatasource#">	
		
			if exists (select 1 from SingleSignOnSPSiteBehaviour where siteDefID = <cfqueryparam value = "#arguments.siteDefID#" CFSQLType ="CF_SQL_INTEGER">)
			BEGIN
				update SingleSignOnSPSiteBehaviour
				set 
				defaultAllowRegularLogin = <cfqueryparam value = "#arguments.defaultAllowRegularLogin#" CFSQLType ="CF_SQL_BIT">, 
				defaultAllowSingleSignOn = <cfqueryparam value = "#arguments.defaultAllowSingleSignOn#" CFSQLType ="CF_SQL_BIT">, 
				autostartSingleSignOn = <cfqueryparam value = "#arguments.autostartSingleSignOn#" CFSQLType ="CF_SQL_BIT">
				where siteDefId = <cfqueryparam value = "#arguments.siteDefID#" CFSQLType ="CF_SQL_INTEGER">
			END
			ELSE
			BEGIN
				insert into SingleSignOnSPSiteBehaviour (siteDefId,defaultAllowRegularLogin,defaultAllowSingleSignOn,autostartSingleSignOn)
				values(
					<cfqueryparam value = "#arguments.siteDefID#" CFSQLType ="CF_SQL_INTEGER">,
					<cfqueryparam value = "#arguments.defaultAllowRegularLogin#" CFSQLType ="CF_SQL_BIT">,
					<cfqueryparam value = "#arguments.defaultAllowSingleSignOn#" CFSQLType ="CF_SQL_BIT">,
					<cfqueryparam value = "#arguments.autostartSingleSignOn#" CFSQLType ="CF_SQL_BIT">
				)
			END
			
		</cfquery>
		
		<cfset clearCache()>

		
	</cffunction>
	
	
	<cffunction name="getValidIdentityProvidersForASite" access="public" output="false" returntype="query" hint="Returns all identity providers for a site. Currently just returns saml service providers ">
		<cfargument name="siteDefID" type="numeric" required="true" default="#request.currentSite.siteDefID#">	
		<cfscript>
			allSamlIDPsQuery=new Query();
			allSamlIDPsQuery.setDatasource(application.siteDatasource);
			allSamlIDPsQuery.addParam(name="siteDefID",value=siteDefID,cfsqltype="CF_SQL_INTEGER"); 
			allSamlIDPs = allSamlIDPsQuery.execute(sql="select SAMLIdentityProvider.SAMLIdentityProviderID,SAMLIdentityProvider.identityProviderName  "&
														"from SAMLIdentityProvider  "&
														"left join SAMLIdentityProviderAuthorisedSite on SAMLIdentityProviderAuthorisedSite.SAMLIdentityProviderId=SAMLIdentityProvider.SAMLIdentityProviderId "&
														"where active=1 and siteDefID = :siteDefID").getResult();	
		
		</cfscript>

		<cfreturn allSamlIDPs>
	</cffunction>
	
</cfcomponent>