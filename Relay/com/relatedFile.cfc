<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Adam Reynolds
Date created:	2005-03-21

	Provides facilities for the management of related files.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2009/09/07 LID 2515 Removed #application. userfilespath#
WAB 2011/03/15		Added new functions to get file query and to delete files. Tied into a new webservices\relatedFileWS.cfc and changes to the custom tags
2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes
2015-10-26			ACPK		PROD2015-142 Encode filename in WebHandle
2015-05-06 PPB 	P-KAS054 added fileCategoryId and fileCategory as optional filters to getRelatedFileCategory()
2015-11-09 PPB 	P-KAS065 Opportunity Document Synching

Enhancement still to do:
Fully provide the set of functionality currently held within the relatedFilesv2 set of custom tag files.


--->
<cfcomponent displayname="relatedfile" hint="Provides facilities for accessing related file information.">
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns columnNames from a query
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getFileDetailsByFileID">
		<cfargument name="fileID" required="Yes" type="numeric">

		<cfreturn getFileDetails (argumentCollection = arguments)>
	</cffunction>

	<cffunction name="getFileDetailsByEntityID">
		<cfargument name="entityID" required="Yes" type="numeric">
		<cfargument name="entityType" required="Yes" type="string">
		<cfargument name="FileCategory" required="no" type="string" default = ""><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
		<cfargument name="uploadUUID" required="No" type="string">

		<cfreturn getFileDetails (argumentCollection = arguments)>

	</cffunction>

	<cffunction name="getFileDetails" access="private">
		<cfargument name="fileID" required="No" type="numeric">
		<cfargument name="entityID" required="No" type="numeric">
		<cfargument name="entityType" required="No" type="string">
		<cfargument name="FileCategory" required="no" type="string" default = "">
		<cfargument name="getRights" required="no" type="boolean" default = "false">
		<cfargument name="uploadUUID" required="No" type="string"><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->

		<cfset var fileDetailsQry = "">
		<cfset var qualifiedList = "">

		<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
		<cfquery name="fileDetailsQry" DATASOURCE="#application.SiteDataSource#">
				select
			rf.FileID,
			rf.EntityID,
			rf.FileCategoryID,
			rf.FileName,
			case when isNull(rfT.secure,0) = 1 then '#application.paths.secureContent#' else '#application.paths.content#' end + '/' + rft.FilePath + '\' + rf.FileName as FileHandle,
			<!--- '#startpath#' + rft.FilePath + '\' + rf.FileName as FileHandle,--->
			'#request.currentSite.protocolAndDomain#/content/' + rft.FilePath + '/' + dbo.fn_urlencode(rf.FileName) as WebHandle, <!--- 2015-10-26	ACPK	PROD2015-142 Encode filename in WebHandle --->
			case when PrefixFile = 1 then right(filename,len(filename) - CHARINDEX('_',filename)) else FileName end as originalFileName,
			rf.UploadedFileName,
			rf.Description,
			rf.UploadedBy,
			rf.Uploaded,
			rf.ReadLevel,
			rf.WriteLevel,
			rf.Expires,
			rf.FileSize,
			rf.Properties,
			isnull(l.Language,'None') as Language,
			rf.Language as LanguageID,
			rf.UploadUUID,
			rft.SingleFile,
			rft.PrefixFile,
			rft.FileCategoryEntity,
			rft.FileCategory,
			rft.FileCategoryDescription,
			rft.FilePath,
			rft.CreatedBy,
			rft.Created,
			rft.Active,
			rft.secure,
			<cfif getRights>
				<cfif application.com.login.checkInternalPermissions("adminTask","Level3")>
				1
				<cfelse>
				case when UploadedBy = #request.relayCurrentUser.personID# then 1 else 0 end
				</cfif>
				as hasDeleteRights,

			</cfif>
			isnull(p.firstname,'') + ' ' + isnull(p.lastname,'') as Loadedby

		from
			RelatedFile as rf
			inner join RelatedFileCategory as rft on rf.FileCategoryID = rft.FileCategoryID
			left join Language as L on rf.Language = l.LanguageID
			left outer join person p on p.personid = rf.uploadedby
		where
			<cfif structKeyExists (arguments,"fileID")>
				RF.FILEID = #arguments.FILEID#
			<cfelse>
				<cfif NOT arguments.EntityID AND IsDefined("arguments.uploadUUID") AND Len(arguments.uploadUUID) EQ 36>
					rf.UploadUUID = <cf_queryparam value="#arguments.uploadUUID#" CFSQLTYPE="CF_SQL_IDSTAMP" maxlength="36">
				<cfelse>
					rf.EntityID = #arguments.EntityID#
				</cfif>
				and rft.FileCategoryEntity =  <cf_queryparam value="#arguments.EntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
				<cfif FileCategory is not "">
					<cfset qualifiedList = listqualify(arguments.FileCategory,"'")>
					and rft.FileCategory  in ( <cf_queryparam value="#arguments.FileCategory#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
				</cfif>
			</cfif>
		</cfquery>
		<cfreturn fileDetailsQry>
	</cffunction>


	<cffunction name="deleteRelatedFileByEntityID">
		<cfargument name="entityID" required="Yes" type="numeric">
		<cfargument name="entityType" required="Yes" type="string">

		<cfset var fileDetails = getFileDetailsByEntityID (argumentCollection = arguments,getRights = true)>

		<cfreturn deleteRelatedFile (fileDetails = fileDetails)>

	</cffunction>

	<cffunction name="deleteRelatedFileByFileID">
		<cfargument name="fileID" required="Yes" type="numeric">

		<cfset var fileDetails = getFileDetailsByFileID (fileID = fileID, getRights = true)>

		<cfreturn deleteRelatedFile (fileDetails = fileDetails)>

	</cffunction>

	<cffunction name="deleteRelatedFile">
		<cfargument name="fileDetails" required="Yes" type="query">

		<cfset var result = {isOK = true,message=""}>
		<cfset var checkOne = "">

		<cfif request.relayCurrentUser.isLoggedIn>
			<cfif fileDetails.recordCount>

				<!--- NJH 2007/03/01 check the user's permission levels before displaying the delete link  --->
				<cfif fileDetails.hasDeleteRights>

					<!--- make sure that no other reference to this file exists --->

					<cfquery name="checkOne" datasource="#application.sitedatasource#">
						select
							rf.FileName
						from
							RelatedFile as rf,
							RelatedFileCategory as rft
						where
							rf.FileCategoryID = rft.FileCategoryID
							and rf.FileName =  <cf_queryparam value="#fileDetails.FileName#" CFSQLTYPE="CF_SQL_VARCHAR" >
							and rft.FilePath =  <cf_queryparam value="#fileDetails.FilePath#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>

					<!--- NJH 2009/07/08 - added the check for adminTask:level3 or if user was the one uploading the files.. a bit overkill, but makes it that much more secure.  --->
					<cfif checkOne.recordcount is 1 >
						<!--- perform the actual file deletion --->
						<!--- NJH 2010/08/06 RW8.3 - commented out for below code <cffile action="DELETE" file="#startpath##GetFileDetails.FilePath#\#getfiledetails.FileName#"> --->
						<cfif fileExists (fileDetails.FileHandle)>
							<cffile action="DELETE" file="#fileDetails.FileHandle#">
						</cfif>
					</cfif>

					<!--- delete the appropriate row from the table --->
					<!--- NJH 2016/03/10 JIRA PROD2016-472 Synching Attachments - track related file deletions --->
					<cfquery name="deletefiledetails" datasource="#application.sitedatasource#">
						update relatedFile set
							lastUpdated = getDate()
							,lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >
							,lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer" >
						where fileID =  <cf_queryparam value="#fileDetails.fileid#" CFSQLTYPE="CF_SQL_INTEGER" >

						delete
						from
							RelatedFile
						where fileID =  <cf_queryparam value="#fileDetails.fileid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

				<cfelse>
					<cfset result.isOK = false>
					<cfset result.Message = "You do not have rights to delete this file">
				</cfif>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.Message = "File does not Exist">
			</cfif>

		<cfelse>
			<cfset result.isOK = false>
			<cfset result.Message = "Please Login">


		</cfif>

		<cfreturn result>

	</cffunction>


	<!--- NJH 2012/11/02 Case 431417
			WAB 2014-05-20 added fileCategoryID to query
		 2015-04-13 PPB P-KAS054 added fileCategoryId and fileCategory as optional filters 
	 --->
	<cffunction name="getRelatedFileCategory" access="public" returntype="query">
		<cfargument name="fileCategoryId" type="numeric" required="false">					<!--- 2015-04-13 PPB P-KAS054 --->
		<cfargument name="fileCategoryEntity" type="string" required="false">
		<cfargument name="fileCategory" type="string" required="false">						<!--- 2015-04-13 PPB P-KAS054 --->

		<cfset var getFileCategory = "">

		<cfquery name="getFileCategory" datasource="#application.siteDataSource#">
			select fileCategoryID,fileCategoryEntity,fileCategory,fileCategoryDescription,filePath,secure,fileUploadedEmail from relatedFileCategory 												<!--- 2015-04-13 PPB P-KAS054 return * ---> 
			where 1=1 
			<cfif structKeyExists(arguments,"fileCategoryEntity")>
				and fileCategoryEntity=<cf_queryparam cfsqlType="cf_sql_varchar" value="#arguments.fileCategoryEntity#">
			</cfif>
			<!--- START: 2015-04-13 PPB P-KAS054 --->
			<cfif structKeyExists(arguments,"fileCategoryId")>
				and fileCategoryId=<cf_queryparam cfsqlType="cf_sql_integer" value="#arguments.fileCategoryId#">
			</cfif>
			<cfif structKeyExists(arguments,"fileCategory")>
				and fileCategory=<cf_queryparam cfsqlType="cf_sql_varchar" value="#arguments.fileCategory#">
			</cfif>
			<!--- END: 2015-04-13 PPB P-KAS054 --->
			order by fileCategory
		</cfquery>

		<cfreturn getFileCategory>
	</cffunction>

	<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
	<cfscript>
		public struct function reallocateRelatedFiles
		(
			required string uploadUUID,
			required numeric entityID
		)
		{
			var userFilePath = '';
			var rtnStruct = {};
			rtnStruct.success = true;
			rtnStruct.messages = [];

			//Get list of files for this uuid
			var qRelatedFiles = GetRelatedFilesByUUID(
				uploadUUID=arguments.uploadUUID
			);

			// clear out session secure file locations as it gets in a twist trying to serve wrong filename as it uses fileID which doesnt change
			session.securedFileLocations = structNew();

			for (var i=1;i <= qRelatedFiles.recordCount;i++) {

				// RT-250 use secure location when file is secure
				userFilePath = (qRelatedFiles.secure[i] ? application.paths.securecontent : application.paths.content);

				//rename the actual file
				destinationFileName = arguments.entityID & "_" & ListDeleteAt(qRelatedFiles.FileName[i],1,"_");

				sourceFilePath = userFilePath & "\" & qRelatedFiles.FilePath[i] & "\" & qRelatedFiles.FileName[i];
				destinationFilePath = userFilePath & "\" & qRelatedFiles.FilePath[i] & "\" & destinationFileName;

				try {
					FileMove(sourceFilePath,destinationFilePath);
				}
				catch(any excpt) {
					rtnStruct.success = false;
					ArrayAppend(rtnStruct.messages,excpt.message);
				}

				transaction {
					//update the file names in the database
					var result = RenameFileInDatabase(
						fileID = qRelatedFiles.FileID[i],
						destinationFileName = destinationFileName
					);
					if (!result.success) {
						ArrayAppend(rtnStruct.messages,result.message);
					}

					//remove the uuid from the database
					var result = SetFileUUID(
						fileID = qRelatedFiles.FileID[i]
					);
					if (!result.success) {
						ArrayAppend(rtnStruct.messages,result.message);
					}

					//update the file names in the database
					var result = SetRelatedFileEntityID(
						fileID = qRelatedFiles.FileID[i],
						entityID = arguments.entityID
					);
					if (!result.success) {
						ArrayAppend(rtnStruct.messages,result.message);
					}

					//Remove Uploaded file name from the database
					var result = SetUploadedFileName(
						fileID = qRelatedFiles.FileID[i]
					);
					if (!result.success) {
						ArrayAppend(rtnStruct.messages,result.message);
					}

				}

			}

			return rtnStruct;
		}

		private query function GetRelatedFilesByUUID
		(
			required string uploadUUID
		)
		{
			var sql = "
				SELECT
					RF.FileID,
					RF.EntityID,
					RF.FileCategoryID,
					RF.FileName,
					RFC.FilePath,
					RFC.secure
				FROM RelatedFile RF
				JOIN RelatedFileCategory RFC ON RF.FileCategoryID = RFC.FileCategoryID
				WHERE UploadUUID = :UploadUUID
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="UploadUUID",cfsqltype="CF_SQL_IDSTAMP",value=arguments.uploadUUID,maxlength=36);
			var qGetRelatedFiles = qObj.execute(sql=sql).getResult();

			return qGetRelatedFiles;
		}

		private struct function RenameFileInDatabase
		(
			required numeric fileID,
			required string destinationFileName
		)
		{
			var rtnStruct = {};
			rtnStruct.success = true;
			rtnStruct.message = "Record Updated Successfully";

			var sql = "
				Declare @outputTable Table(FileID int)
			
				UPDATE RelatedFile SET
				FileName = :FileName
				OUTPUT INSERTED.FileID into @outputTable
				WHERE FileID = :FileID
				
				select FileID from @outputTable
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="FileName",cfsqltype="CF_SQL_VARCHAR",value=arguments.destinationFileName);
			qObj.addParam(name="FileID",cfsqltype="CF_SQL_INTEGER",value=arguments.fileID);
			var updateResult = qObj.execute(sql=sql).getResult();

			if (!updateResult.recordCount) {
				rtnStruct.success = false;
				rtnStruct.message = "No Records Updated";
			}

			return rtnStruct;
		}

		private struct function SetFileUUID
		(
			required numeric fileID,
			string uploadUUID=""
		)
		{
			var rtnStruct = {};
			rtnStruct.success = true;
			rtnStruct.message = "Record Updated Successfully";

			var sql = "
				Declare @outputTable Table(FileID int)
			
				UPDATE RelatedFile SET
				uploadUUID = :uploadUUID
				OUTPUT INSERTED.FileID into @outputTable
				WHERE FileID = :FileID
				
				select FileID from @outputTable
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="uploadUUID",cfsqltype="CF_SQL_VARCHAR",value=arguments.uploadUUID,null=!Len(arguments.uploadUUID));
			qObj.addParam(name="FileID",cfsqltype="CF_SQL_INTEGER",value=arguments.fileID);
			var result = qObj.execute(sql=sql).getResult();

			if (!result.recordCount) {
				rtnStruct.success = false;
				rtnStruct.message = "No Records Updated";
			}

			return rtnStruct;
		}

		private struct function SetRelatedFileEntityID
		(
			required numeric fileID,
			required numeric entityID
		)
		{
			var rtnStruct = {};
			rtnStruct.success = true;
			rtnStruct.message = "Record Updated Successfully";

			var sql = "
				Declare @outputTable Table(FileID int)
			
				UPDATE RelatedFile SET
				EntityID = :entityID
				OUTPUT INSERTED.FileID into @outputTable
				WHERE FileID = :FileID
				
				select FileID from @outputTable
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="EntityID",cfsqltype="CF_SQL_INTEGER",value=arguments.entityID);
			qObj.addParam(name="FileID",cfsqltype="CF_SQL_INTEGER",value=arguments.fileID);
			var result = qObj.execute(sql=sql).getResult();

			if (!result.recordCount) {
				rtnStruct.success = false;
				rtnStruct.message = "No Records Updated";
			}

			return rtnStruct;
		}

		private struct function SetUploadedFileName
		(
			required numeric fileID,
			string uploadedFileName=""
		)
		{
			var rtnStruct = {};
			rtnStruct.success = true;
			rtnStruct.message = "Record Updated Successfully";

			var sql = "
				Declare @outputTable Table(FileID int)
			
				UPDATE RelatedFile SET
				UploadedFileName = :uploadedFileName
				OUTPUT INSERTED.FileID into @outputTable
				WHERE FileID = :FileID
				
				select FileID from @outputTable
			";
			var qObj = new query();
			qObj.setDatasource(application.siteDataSource);
			qObj.addParam(name="uploadedFileName",cfsqltype="CF_SQL_VARCHAR",value=arguments.uploadedFileName,null=!Len(arguments.uploadedFileName));
			qObj.addParam(name="FileID",cfsqltype="CF_SQL_INTEGER",value=arguments.fileID);
			var result = qObj.execute(sql=sql).getResult();

			if (!result.recordCount) {
				rtnStruct.success = false;
				rtnStruct.message = "No Records Updated";
			}

			return rtnStruct;
		}

	</cfscript>


</cfcomponent>