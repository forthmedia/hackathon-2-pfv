<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent displayname="Relay Active Content Manager" hint="Methods for managing Active Content">

	<cffunction name="renderActiveContent" hint="Writes Active Content.">
		<cfargument name="filePath" type="string" required="yes" hint="Path to file.">
		<cfargument name="fileAttributes" type="string" required="yes" hint="List of file attributes.">
		<cfargument name="suppressJSInclude" type="boolean" required="No" default="false"><!--- AJC 2007-12-04 P-SNY041 DealerNet Sony1 Integration | Was including above xml repsonse from webservice causing error ---> 
		<cfset var acType = listLast(listFirst(arguments.filePath,"?"),".")>

		<cfset pathForJS = listDeleteAt(listFirst(arguments.filePath,"?"),listLen(listFirst(arguments.filePath,"?"),"."),".")>
		
		<cfif listLen(arguments.filePath,"?") gt 1>
			<!--- GCC 2006/05/16 changed to permit '?' to be passed in the urlstring --->
			<cfset pathForJS = "#pathForJS#?#mid(arguments.filePath,len(listFirst(arguments.filePath,'?'))+2,len(arguments.filePath)-len(listFirst(arguments.filePath,'?')))#">
		</cfif>
		<!--- AJC 2007-12-04 P-SNY041 DealerNet Sony1 Integration | Was including above xml repsonse from webservice causing error ---> 
		<cfif not suppressJSInclude>
			<cf_includejavascriptonce template = "/javascript/AC_RunActiveContent.js">
		</cfif>

		
		<cfset fileAttrList_JS = '"movie","#pathForJS#","src","#pathForJS#"'>
		<cfset ArgfileAttrList = replaceNoCase(replaceNoCase(arguments.fileAttributes,"#chr(10)##chr(13)#","","all")," ","","all")>
		
		<cfloop list="#ArgfileAttrList#" index="attrib" delimiters="#application.delim1#">
			<cfif listLen(attrib,"=") gte 2>
				<cfset attribName = trim(listFirst(attrib,"="))>
				<cfset attribValue = trim(replaceNoCase(replaceNoCase(listDeleteAt(attrib,1,"="),"'","","all"),'"','','all'))>
				<cfset fileAttrList_JS = '#fileAttrList_JS#,"#attribName#","#jsStringFormat(attribValue)#"'>
			</cfif>
		</cfloop>
		
		
		
		<cfswitch expression="#acType#">
			<cfcase value="swf">
				<cfoutput>
					<SCRIPT type="text/javascript">
						AC_FL_RunContent(#fileAttrList_JS#);
					</script>
				</cfoutput>
			</cfcase>
			<cfdefaultcase>
				<cfreturn false>
			</cfdefaultcase>
		</cfswitch>
		
		<cfreturn>
	</cffunction>

</cfcomponent>

