/**
 * This is a CFC that can have functions placed in it that can be used to calculate a score for an activity.
 * (Score caps are enforces seperately from this and don't need to be worried about).
 * In all cases the function should return an integer. In the case of a problem an error should be thrown.
 * If such an error is thrown an entry will be made in the error logs under "ActivityScoringError" and 
 * processing will continue with the next scoring type.
 * 
 * Note this is run inside housekeeping so don't use anything that needs relayCurrentUser (such as RelayEntity)
 * 
 * @author Richard.Tingle
 * @date 10/03/16
 **/
component accessors=true output=false persistent=false {

	public numeric function scoreCommunicationReadBasedOnResponsiveness(
		required numeric entityID, 
		required numeric activityID,
	    //ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]
		required string entityTable,
		required numeric maximumPoints phrase="phr_score_commRead_maxiumumPointsPerActivity", 
		required numeric minimumPoints phrase="phr_score_commRead_minimumPointsPerActivity",
		required numeric pointsFallOffPerDay phrase="phr_score_commRead_dailyFalloff"
		) activityTypes="CommRead" phrase="phr_score_CommRead_basedOnResponsiveness"{
		
		//have to use queries as within housekeeping
		var entityDetails=new Query();
		entityDetails.setDatasource(application.siteDatasource);
		entityDetails.addParam(name="entityID",value=entityID,cfsqltype="CF_SQL_INTEGER"); 	
		var entityDetails_queryReturn=entityDetails.execute(sql="select sendDate, created from Communication where commID = :entityID" ).getResult();
		
		var readDetails=new Query();
		readDetails.setDatasource(application.siteDatasource);
		readDetails.addParam(name="activityID",value=activityID,cfsqltype="CF_SQL_INTEGER"); 	
		var readDetails_queryReturn=readDetails.execute(sql="select activityTime from Activity where activityID = :activityID" ).getResult();
		
		
		var sentDate=entityDetails_queryReturn.sendDate;
		if (sentDate EQ ""){
			sentDate=entityDetails_queryReturn.created; //sometimes (possibly for non scedualed comms there is no sent date)
		}
		
		var readDate=readDetails_queryReturn.activityTime;
		
		daysTillRead=DateCompare(sentDate, readDate,"d");
		
		
		var score=arguments.maximumPoints-(pointsFallOffPerDay)*daysTillRead;
		
		if (score<minimumPoints){
			score=minimumPoints;
		}

		return score;
	}

	//ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]
	public numeric function scoreCommunicationReadBasedOnEntity(
			required numeric entityID,
			required numeric activityID,
			required string entityTable,
			required string profileToUseScore phrase="phr_score_profile_for_score",
			required numeric pointsPer phrase="phr_score_points_per"
			) phrase="phr_score_awards_based_on_emtity_field"{

		var score=0;
		var qObj = new query();
		qObj.setDatasource=(application.siteDataSource);
		if(IsNumeric(profileToUseScore)) {
			
			var checkResult = application.com.flag.getFlagData (flagid = '#profileToUseScore#',entityid = "#entityID#").data;
			if (checkResult EQ ""){
				checkResult=0;
			}
			
			score = checkResult * pointsPer;
		} else {
			//11/10/2016 PROD2016-2415 ESZ Activity Scoring - updateActivities producing ActivityScoringError
			
			var entityTypeID=application.entitytypeid[entityTable];
			var uniqueIdentifier=application.entitytype[entityTypeID].UNIQUEKEY;

			var score=0;
			var qObj = new query();
			qObj.setDatasource=(application.siteDataSource);
	
			var sql = "select isNull(#application.com.security.queryObjectName(profileToUseScore)#,0)  as entityFieldForScoring from #application.com.security.queryObjectName(entityTable)#  where #application.com.security.queryObjectName(uniqueIdentifier)# = :entityID";
			qObj.addParam(name="entityID",value=entityID,cfsqltype="CF_SQL_INTEGER"); 
			qObj.setSql(sql);
			
			var qGetFieldActivityValue = qObj.execute().getResult().entityFieldForScoring;
			score = qGetFieldActivityValue * pointsPer;
	
	
			return Round(score);

			
		}
		return Round(score);
	}
}
