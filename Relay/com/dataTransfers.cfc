<!--- �Relayware. All Rights Reserved 2014 --->
<!--- dataTransfers.cfc

Author AJC - NJH

2006-11-29

Data Transfer Functions
2007/12/20 NJH moved Lenovo transfer functions to userFiles.
2008-08-28 NYB P-LEX014.  Made changes to FileToDb function: in QuerySim - as this wasn't working
2009-01-23 SSS Bug found The table in filetodb was not getting created if there was nothing in a field as the code was trying to create a nvarchar(0) which is not allowed the try catch around this is now redundant.
2009/01/28 SSS Have done some work so that fields craeted are at least 10 in size bigger then they need to be where getting problems if dataloads had to right to some
				Data in to the database.
2009-01-29 SSS Have added a option to delete the table and start again.
2009-01-29 SSS Have made the create function work so that the fields are all created as null
2009-01-29 SSS I have left a cfrethrow around the cfhttp call in the file to db function Error messages must now be added round the function calls
2009-06-09 SSS p-len008 For utf-8 files columns have to be extracted and then cleaned before they can be used as column names
2009-06-09 SSS p-len008 For kanji I have added the capital N so that kanji is not cruppted going into the database
2009/10/13 SSS p-lex038 Changed the filetodb function so you are now able to add a primary key and spcify a name for it
2009-11-04 NJH p-FNL079 SFDC integration. Added import and export opportunity functions.
2009-11-17 SSS P-lex039 - Added debug show no to a query so it would not bring down the site if debug was on
2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location
2010/11/08	WAB LID 4684 - deal with https issues when doing cfhttp requests to own site
2011/01/31  PPB	LID5510 - references to application.flag changed to use application.com.flag.getFlagStructure() (salesforce flags)
2011/05/04 WAB LID 6454 speed/memory issues with fileToDB.  Changed code so that didn't recurse, but used a loop instead
2011-07-25 	NYB  LHID6768 - issue with system not finding uploaded file - because it's uploaded to a different instance then the system is looking for
2011-09-09	STCR LID4952 - FileToDB: Pass delimiter parameter to use ListFix's support for delimiters other than the comma default.
2012-01-27	NYB	Case#424794 - change to FileToDB function: removed abort, failures added to message variable of struct returned instead
2012-05-17	STCR P-REL109 - Enable Unicode character support in fileToDB
2012-11-21	IH	Case 429346 fix encoding in cffile action=read
2012-11-22	IH	Case 429346 fix function putData to handle Integer and Date data types, fix function fileToDB to obey setCreated when sourceCollectionMethod is set to http
2012-12-06	IH	Case 432436 fix bug in function putData
2012-12-10	IH	Case 432249 fix the order dataType is populated in csvcomponent query
2013-01-31 	NYB Case 432626 added if to chartype designation to force system to make dataloads accept foreign characters
2013-03-15 PPB Case 434056 getDataTransferHistory - optional maxRows
2013-04-04 PPB Case 434056 added a large timeout
2013-04-04 PPB Case 434056 select the MOST RECENT maxRows by switching the order

--->


<cfcomponent displayname="Functions for various Data Transfers" hint="Data Transfer Component">

	<!--- Gets the details from the DataTransferDefinition table --->
	<cffunction access="public" name="fn_get_DataTransferDefinition" returnType="query">
		<cfargument name="dtdID" type="numeric" required="no" default="0">
		<cfargument name="directionTypeID" type="numeric" required="no" default="0">

 		<cfset var qry_get_DataTransferDefinitions="">

		<!--- get the DataTransferDefinitions --->
		<cfquery name="qry_get_DataTransferDefinitions" datasource="#application.siteDataSource#">
			select dtd.*, dtdt.description as direction from DataTransferDefinition dtd
			INNER JOIN DataTransferDirectionType dtdt ON dtd.directionTypeID = dtdt.directionTypeID
			where 1=1
			<cfif arguments.dtdID neq 0>
				and dtdID=#arguments.dtdID#
			</cfif>
			<cfif arguments.directionTypeID neq 0>
				and directionTypeID=#arguments.directionTypeID#
			</cfif>
		</cfquery>

		<cfreturn qry_get_DataTransferDefinitions>

	</cffunction>



	<!--- Gets the max ID from modRegister --->
	<cffunction access="public" name="fn_get_MaxModRegisterID" returnType="query">
 		<cfset var qry_get_MaxModRegisterID="">

		<!--- get the maximum modregester id --->
		<cfquery name="qry_get_MaxModRegisterID" datasource="#application.siteDataSource#">
			select max(ID) as ID from modRegister
		</cfquery>

		<cfreturn qry_get_MaxModRegisterID>

	</cffunction>



	<!--- Gets the last data transfer details --->
	<cffunction access="public" name="fn_get_LastDataTransfer" returnType="query">
		<cfargument name="directionTypeID" type="numeric" required="no" default="0">
		<cfargument name="dtdID" type="numeric" required="yes">

		<cfset var qry_get_LastDataTransfer="">

		<!--- get the last record in dataTransferHistory --->
		<cfquery name="qry_get_LastDataTransfer" datasource="#application.siteDataSource#" maxrows=1>
			select * from dataTransferHistory
			where MaxModRegisterID <> 0 and
			dtdID=#arguments.dtdID#
			<cfif arguments.directionTypeID neq 0>
				and dtdID IN (select dtdID from DataTransferDefinition where directionTypeID=#arguments.directionTypeID#)
			</cfif>
			Order By DTHID DESC
		</cfquery>

		<cfreturn qry_get_LastDataTransfer>

	</cffunction>


	<cffunction name="getDataTransferHistory" returntype="query">
		<cfargument name="dthID" type="numeric" required="no">
		<cfargument name="dtdID" type="numeric" required="no">
		<cfargument name="maxRows" type="numeric" required="no" default="500">		<!--- 2013-03-15 PPB Case 434056 optional maxRows --->

		<cfset var getDataTransferHistoryRecord="">
		<!--- 2013-04-04 PPB Case 434056 select the MOST RECENT maxRows by switching the order --->
		<cfquery name="getDataTransferHistoryRecord" datasource="#application.siteDataSource#" timeout="1800">			<!--- 2013-04-04 PPB Case 434056 added a large timeout in case the user asks for a lot of rows  --->
			select * from
			(
			select top #arguments.maxRows# * from dataTransferHistory                /* 2013-03-15 PPB Case 434056 optional maxRows */
			where 1=1
				<cfif structKeyExists(arguments,"dtdID")>and dtdID = #arguments.dtdID#</cfif>
				<cfif structKeyExists(arguments,"dthID")>and dthID = #arguments.dthID#</cfif>
			order by dthid desc
			) as dummy
			order by dthid
		</cfquery>

		<cfreturn getDataTransferHistoryRecord>
	</cffunction>


	<!--- Inserts Data Transfer History into the database --->
	<cffunction access="public" name="fn_ins_DataTransferHistory">

		<cfargument name="dtdID" type="numeric" required="yes">
		<cfargument name="dtStartTime" type="numeric" required="yes">
		<cfargument name="dtEndTime" type="numeric" required="yes">
		<cfargument name="rowCount" type="numeric" required="no" default="0">
		<cfargument name="maxModID" type="numeric" required="no" default="0">
		<cfargument name="fileGenerated" type="boolean" required="no" default="0">
		<cfargument name="filename" type="string" required="no"> <!--- NJH 2009/06/04 P-SNY063 --->
		<cfargument name="scheduledTaskLogID" type="numeric" required="no"> <!--- NJH 2009/06/04 P-SNY063 --->
		<cfargument name="isOK" type="boolean" default="true"> <!--- NJH 2009/06/04 P-SNY063 --->
		<cfargument name="traceStruct" type="struct" default="#structNew()#">

 		<cfset var qry_ins_DataTransferHistory="">
		<cfset var jsonTraceStruct = "">

		<cfset jsonTraceStruct = SerializeJSON(arguments.traceStruct,"true")>

		<cfquery name="qry_ins_DataTransferHistory" datasource="#application.siteDataSource#">
			insert into dataTransferHistory (dtdID,StartTime,EndTime,Rows,MaxModRegisterID,GenerateFile
			<cfif structKeyExists(arguments,"Filename")>,Filename</cfif>
			<cfif structKeyExists(arguments,"scheduledTaskLogID")>,scheduledTaskLogID</cfif>
			,isOK,trace
			) values(
				<cf_queryparam value="#arguments.dtdID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.dtStartTime#" CFSQLTYPE="CF_SQL_TIMESTAMP">,
				<cf_queryparam value="#arguments.dtEndTime#" CFSQLTYPE="CF_SQL_TIMESTAMP">,
				<cf_queryparam value="#arguments.rowCount#" CFSQLTYPE="CF_SQL_Integer" >,
				<cf_queryparam value="#arguments.maxModID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#arguments.fileGenerated#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cfif structKeyExists(arguments,"Filename")><cf_queryparam value="#arguments.fileName#" CFSQLTYPE="CF_SQL_VARCHAR" >,</cfif>
				<cfif structKeyExists(arguments,"scheduledTaskLogID")><cf_queryparam value="#scheduledTaskLogID#" CFSQLTYPE="CF_SQL_INTEGER" >,</cfif>
			<cfif arguments.isOK>1<cfelse>0</cfif>
			,<cf_queryparam value="#jsonTraceStruct#" CFSQLTYPE="CF_SQL_VARCHAR" >
			)
		</cfquery>

	</cffunction>


	<!--- NJH 2008/04/11 added for CR-LEN507
		WAB 2011/06/20 For NAS Problem with this function if more than one character to remove from an individual item
					 Went wrong because startQualifierPos did not take into account that the string was getting shorted
					 PS.  Seems an odd function, would have thought that you might replace these characters with something else which you can then change back afterwards!

	--->
	<cffunction name="fn_removeSpecialCharsFromTextQualifiedString" access="public" return="string" hint="Removes specified characters from a text qualified string">
		<cfargument name="textQualifiedString" type="string" required="yes">
		<cfargument name="textQualifier" type="string" default="#chr(34)#">
		<cfargument name="charToRemove" type="string" default=",">

		<cfset var startQualifierPos = 0>
		<cfset var replaceBlockLenBeforeCharRemoval = 0>
		<cfset var replaceBlockLenAfterCharRemoval = 0>
		<cfset var exitLoop = "false">
		<cfset var newTextQualifiedString = arguments.textQualifiedString>
		<cfset var stringLength = 0>
		<cfset var replaceBlock = "">
		<cfset var endQualifierPos = '' />
		<cfset var replaceBlockLength = '' />
		<cfset var leftString = '' />
		<cfset var tempString = '' />
		<cfset var tempStringLength = '' />
		<cfset var rightString = '' />
		<cfset var reductionInLength = 0 />


		<!--- Replace double textQualifiers with a temporary placeholder --->
		<cfset newTextQualifiedString = Replace(newTextQualifiedString,"#arguments.textQualifier##arguments.textQualifier#","#application.delim1#|#application.delim1#","ALL")>

		<cfset stringLength = Len(newTextQualifiedString)>

		<cfloop condition = "exitLoop eq false">
			<cfset endQualifierPos = startQualifierPos+1>
			<!--- find the first position of the textqualified --->
			<cfset startQualifierPos = find(arguments.textQualifier,newTextQualifiedString,endQualifierPos)>

			<!--- if a text qualifier exists --->
			<cfif startQualifierPos gt 0>
				<!--- get the next text qualifier --->
				<cfset endQualifierPos = find(arguments.textQualifier,newTextQualifiedString,startQualifierPos+1)>
				<!--- <cfoutput>#newTextQualifiedString#: #startQualifierPos# - #endQualifierPos#<br></cfoutput> --->
				<cfset replaceBlock = mid(newTextQualifiedString,startQualifierPos,endQualifierPos-startQualifierPos+1)>

				<cfset replaceBlockLenBeforeCharRemoval = len(replaceBlock)>
				<cfset replaceBlock = replace(replaceBlock,arguments.charToRemove,"","ALL")>
				<cfset replaceBlockLenAfterCharRemoval = len(replaceBlock)>
				<cfset reductionInLength = replaceBlockLenBeforeCharRemoval - replaceBlockLenAfterCharRemoval>
				<cfset stringLength = stringLength - reductionInLength>

				<!--- replace all occurrences of the content delimitor between quotes with a temp delimiter --->
				<!--- <cfset replaceBlock=Replace(replaceBlock,contentDelim,"#application.delim1#","ALL")> --->
				<!--- <cfset replaceBlockLength = Len(replaceBlock)> --->
				<!--- <cfif startQualifierPos GT 0>
					<cfset leftString = Left(arguments.textQualifiedString,startQualifierPos-i)>
				<cfelse> --->
				<cfset leftString = Left(newTextQualifiedString,startQualifierPos-1)>
				<cfset tempString = leftString & replaceBlock>
				<cfset tempStringLength = Len(tempString)>
				<cfset rightString = right(newTextQualifiedString,stringLength-tempStringLength)>
				<cfset tempString = tempString & rightString>
				<cfset newTextQualifiedString = tempString>
				<cfset replaceBlock = "">
				<cfset leftString = "">
				<cfset rightString = "">
				<cfset replaceBlockLenBeforeCharRemoval = 0>
				<cfset replaceBlockLenAfterCharRemoval = 0>
				<cfset startQualifierPos = endQualifierPos+1-reductionInLength>
			<cfelse>
				<cfset exitLoop = "true">
			</cfif>
		</cfloop>

		<cfset newTextQualifiedString = Replace(newTextQualifiedString,"#application.delim1#|#application.delim1#","#arguments.textQualifier##arguments.textQualifier#","ALL")>

		<cfreturn newTextQualifiedString>

	</cffunction>

	<!--- NJH 2008/04/11 added for CR-LEN507 --->
	<cffunction name="fn_ListFix" access="public" returntype="string" hint="Replaces empty list elements with a specified value">
		<cfargument name="list" type="string">
		<cfargument name="delimiter" default="," type="string">
		<cfargument name="nullValue" default="NULL" type="string" hint="The value to place in the empty list element">

		<cfscript>
		/**
		 * Fixes a list by replacing null entries.
		 * This is a modified version of the ListFix UDF
		 * written by Raymond Camden. It is significantly
		 * faster when parsing larger strings with nulls.
		 * Version 2 was by Patrick McElhaney (&#112;&#109;&#99;&#101;&#108;&#104;&#97;&#110;&#101;&#121;&#64;&#97;&#109;&#99;&#105;&#116;&#121;&#46;&#99;&#111;&#109;)
		 *
		 * @param list 	 The list to parse. (Required)
		 * @param delimiter 	 The delimiter to use. Defaults to a comma. (Optional)
		 * @param null 	 Null string to insert. Defaults to "NULL". (Optional)
		 * @return Returns a list.
		 * @author Steven Van Gemert (&#112;&#109;&#99;&#101;&#108;&#104;&#97;&#110;&#101;&#121;&#64;&#97;&#109;&#99;&#105;&#116;&#121;&#46;&#99;&#111;&#109;&#115;&#118;&#103;&#50;&#64;&#112;&#108;&#97;&#99;&#115;&#46;&#110;&#101;&#116;)
		 * @version 3, July 31, 2004
		 */
		//function listFix(list) {
			var delim = arguments.delimiter;
			var null = arguments.nullValue;
			var special_char_list      = "\,+,*,?,.,[,],^,$,(,),{,},|,-";
			var esc_special_char_list  = "\\,\+,\*,\?,\.,\[,\],\^,\$,\(,\),\{,\},\|,\-";
			var i = "";

			if(arrayLen(arguments) gt 1) delim = arguments[2];
			if(arrayLen(arguments) gt 2) null = arguments[3];


			if(findnocase(left(arguments.list, 1),delim)) arguments.list = null & arguments.list;
			if(findnocase(right(arguments.list,1),delim)) arguments.list = arguments.list & null;

			i = len(delim) - 1;
			while(i GTE 1){
			delim = mid(delim,1,i) & "_Separator_" & mid(delim,i+1,len(delim) - (i));
			i = i - 1;
			}

			delim = ReplaceList(delim, special_char_list, esc_special_char_list);
			delim = Replace(delim, "_Separator_", "|", "ALL");

			arguments.list = rereplace(arguments.list, "(" & delim & ")(" & delim & ")", "\1" & null & "\2", "ALL");
			arguments.list = rereplace(arguments.list, "(" & delim & ")(" & delim & ")", "\1" & null & "\2", "ALL");

			return arguments.list;
		//}
		</cfscript>

	</cffunction>

	<!--- NJH 2008/04/11 added for CR-LEN507 --->
	<cffunction name="fileToDB" access="public" returntype="struct" output="false">
		<cfargument name="dir" type="string">
		<cfargument name="filename" type="string">
		<cfargument name="endRowDelim" type="string" default="#chr(13)##chr(10)#">
		<cfargument name="delim" type="string" default="#application.delim1#">
		<cfargument name="tablename" type="string" required="true"> <!--- the table to insert the data into --->
		<cfargument name="tableOptions" type="string" default=""> <!--- truncate or delete any existing data; will append if this is an empty string --->
		<cfargument name="columnNames" type="boolean" default="true"> <!--- first row has column headings --->
		<cfargument name="fieldnames" type="array"> <!--- only needed if sourceCollectionMethod="absolutePath"  --->
		<cfargument name="validateFile" type="boolean" default="false">
		<cfargument name="textQualifier" type="string" default="#chr(34)#"> <!--- double quote --->
		<cfargument name="deleteQryWhereClause" type="string" default="">
		<cfargument name="setCreated" type="boolean" default="false">
		<cfargument name="numberElementsExpected" type="numeric" required="false">
		<cfargument name="sourceCollectionMethod" type="string" default="absolutePath">
		<cfargument name="nocheckloaddata" type="boolean" default="true">
		<cfargument name="defaultPrecisionSize" type="numeric" default="255">
		<cfargument name="increment" type="numeric" default="100">
		<cfargument name="primarykeyname" type="string" default=""> <!--- 2009/10/13 SSS p-lex038 if blank primary key not added --->
		<cfargument name="charset" type="string" default="utf-8"> <!---SSS 2009/12/09 P-LEX038 added to enable charset to be changed if necessary--->
		<cfargument name="sourceDirectory" type="string" default="" />
		<cfargument name="destinationDirectory" type="string" default="" />
		<cfargument name="rethrowOnError" type="boolean" default="false" /><!--- 2012-05-24 STCR P-REL109 - give the caller the option to handle exceptions more gracefully --->

		<!--- START: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location | Tidy up some var'ing --->
		<cfscript>
			var indexCounter = 0;
			var importProcess = "";
			var importLostQuery = QueryNew("rowNum,data","integer,varchar");
			var rowCount = 0;
			var returnStruct = structNew();
			var arrayLength = "";
			var importProcessQry = QueryNew(""); // NJH 2009/05/26 P-SNY063 var'd this variable
			var dataLoadFiles = QueryNew("");
			var columnNameList = "";
			var colcount = 0;
			var loopCount = 0;
			var deleteRecords = "";
			var TruncateRecords = "";
			var droptable = "";
			var x = "";
			var csvimport = "";
			var logRow = "";
			var logRowProcess = "";
			var rowArray = arraynew(1);
			var insertcsv = QueryNew("");
			var index = "";
			var tempValue = "";
			var csvcomponent = "";
			var filteredDataLoadFiles = "";
			var httpURL = "#request.currentSite.httpprotocol##request.currentsite.domain##arguments.dir#/#arguments.filename#";  // NJH 2010/08/17 RW8.3
			var TemporaryFolderDetails = structNew(); // NJH 2010/08/17 RW8.3
			var pathToFile = "#application.paths.userfiles##replace(arguments.dir,'/','\','all')#" ;
			var attributeStruct = '' ;
			var listIX = '' ;
			var arrayIdx = '' ;
		</cfscript>
		<!--- END: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location | Tidy up some var'ing' --->

		<!--- START: 2012-01-27	NYB	Case#424794 - added --->
		<cfset returnStruct.message = "">
		<cfset returnStruct.isOK = "true">
		<!--- END: 2012-01-27	NYB	Case#424794 --->

		<cfif structkeyexists(arguments,"fieldNames")>
			<cfset arrayLength = arrayLen(arguments.fieldnames)>
		</cfif>
		<cfset returnStruct.fileLoaded="false">
		<cfset returnStruct.numRowsInserted = 0>

		<!--- blindingly fast for very large files (> 2mB) but only works for perfectly formed files - will throw an error if any rows are incomplete or too long --->
		<!--- request.importHeader.dir needs to be the path from webroot to the file e.g. /code/cftemplates/--->
		<!---Start 2009-06-09 P-LEN008 GeoData it a file is utf8 you have to clean columns before the cfhttp will work --->
		<!--- START: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location --->
		<cfif arguments.sourceDirectory is not "">
			<cfset pathToFile = arguments.sourceDirectory>
		</cfif>
		<cfif arguments.sourceDirectory is not "" and arguments.destinationDirectory is not "" and arguments.sourceDirectory neq arguments.destinationDirectory>
			<!--- check that the source and destination directory exists --->
			<cfif directoryexists(arguments.sourceDirectory) and directoryexists(arguments.destinationDirectory)>
				<cftry>
					<cfset pathToFile = arguments.destinationDirectory>
				<!--- copy file to destination directory: Note this should be a public assessable directory--->
					<cffile action="copy" source="#arguments.sourceDirectory#\#filename#" destination="#arguments.destinationDirectory#\#filename#" />
					<cfcatch type="any">
						<cfthrow detail="Cannot copy #filename# from #arguments.sourceDirectory# to #arguments.destinationDirectory#">
					</cfcatch>
				</cftry>
			<cfelse>
				<cfthrow detail="Directory #arguments.sourceDirectory# or #arguments.destinationDirectory#  does not exist">
			</cfif>
		</cfif>

		<cftry>
		<!--- END: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location --->

			<cfif arguments.sourceCollectionMethod eq "http">
				<!--- NJH 2010/08/17 - RW8.3 if we're trying to read a file in dataTransfers, move the file into a temporary directory so that it can be read via http.
					8.3 will move the dataTransfers directory out of content into the root of userfiles, so if the reference to content exists in the path,
					remove it. Removing 'content' really is belt and braces as content should be replaced as part of the 8.3 release

					WAB 2011/01/12 make this more generic - any file which is not under content or code will need moved to an http accessible location
					Note that we may still run into http/https problems on dev sites with self signed certificates
					May need to use a temporary folder which does not require ssl

				 --->
				<cfset pathToFile = "#pathToFile#\#arguments.filename#">

				<cfif (arguments.dir DOES NOT CONTAIN "\content" and arguments.dir DOES NOT CONTAIN "\code") and fileExists(pathToFile)>
					<cfset TemporaryFolderdetails = application.com.fileManager.getTemporaryFolderDetails(notssl=true)>
					<!--- START: NYB Case 432626 - removed fix from here, put back copy: --->
					<cffile action="copy" source="#pathToFile#" destination="#TemporaryFolderdetails.path#">
					<cfset httpURL = TemporaryFolderdetails.webpath&arguments.filename>
				</cfif>

				<cfset attributeStruct = {url=httpURL,method="GET",name="importProcessQry",delimiter=arguments.Delim,textqualifier=arguments.textQualifier,firstRowAsHeaders="false",charset=arguments.charset,throwOnError="yes"}>

				<cftry>
					<cftry>
						<cfhttp attributecollection=#attributeStruct# />
						<!--- NYB Case 432626 added to provide correct result: --->
						<cfset returnStruct.fileLoaded="true">
						<cfcatch type="any">
							<cftry>
								<!--- WAB 2010/11/08 Deal with HTTPS where the certificate is dodgy!
										We think that this will only occur on our dev sites
										The above request via https will fail so we try via http instead
										On dev sites is may be necessry to allow non SSL requests to the given directory
								 --->
								<cfset attributeStruct.url = replaceNoCase (attributeStruct.url,"https","http","ONE")>
								<cfhttp
									url="#attributeStruct.url#"
									RESULT ="CFHTTPTest"
								>
								<cfif CFHTTPTest.statusCode is "403 Forbidden" or CFHTTPTest.statusCode is "500 Internal Server Error">
									<!---  WAB 2011/09/05 LID 6947,  just testing for 403 Forbidden doesn't always work.
											On some systems we get a "500 Internal Server Error", not so specific but we can be fairly sure that it is probably an https problem so I will add it
									--->
									<cfoutput>
										<div class="errorblock">
											Unable to read: <br/> #htmleditformat(httpURL)#<BR><BR>
											<cfif isdefined("CFHTTPTest.ErrorDetail") and len(CFHTTPTest.ErrorDetail) gt 0>
												Reason:  #htmleditformat(CFHTTPTest.ErrorDetail)#<BR><BR>
											<cfelse>
												<cfif isdefined("CFHTTPTest.Responseheader.Explanation") and len(CFHTTPTest.Responseheader.Explanation) gt 0>
													Reason:  #htmleditformat(CFHTTPTest.Responseheader.Explanation)#<BR><BR>
												</cfif>
											</cfif>
											Please contact your Network Administrator to allow non SSL requests to: <br/> #htmleditformat(TemporaryFolderdetails.webrootpath)#.  <!--- WAB 2011/09/05 was .webrootpath and neither were actually in the TemporaryFolderdetails structure, so change made to fileManager.getTemporaryFolderDetails() --->
										</div>
									</cfoutput>
									<CF_ABORT>
								</cfif>

								<cfhttp attributecollection=#attributeStruct# />
								<!--- NYB Case 432626 added to provide correct result: --->
								<cfset returnStruct.fileLoaded="true">
								<cfcatch type="any">
									<cfrethrow>
								</cfcatch>
							</cftry>
						</cfcatch>
					</cftry>
					<!---End 2009-06-09 P-LEN008 GeoData it a file is utf8 you have to clean columns before the cfhttp will work --->

					<cfif arguments.columnNames>
						 <cfset columnNameList = "">
						 <!--- 2009/08/11 GCC modifeid form looping over column list as column list is alphabetical so the order is broken if coluimn list has more than 9 columns in it --->
						 <cfset colcount = 1>
						 <cfloop from="1" to="#listlen(importProcessQry.columnList)#" index="loopCount">
							<cfset columnNameList = listappend(columnNameList,application.com.regExp.makeSafeColumnName(inputString=evaluate("importProcessQry.COLUMN_#loopCount#")))>
						 </cfloop>
						 <cfset attributeStruct.columns = columnNameList>
						 <cfset attributeStruct.firstrowasheaders = "true">
					</cfif>

					<cfhttp attributecollection=#attributeStruct# />
					<!--- NYB Case 432626 added to provide correct result: --->
					<cfset returnStruct.fileLoaded="true">

					<cfcatch type="any">
						<!--- SSS 2009-01-29 Have commented this out in order and just left a rethrow in order to catch errors put a try catch around
						your function call
						<cfif cfcatch.detail eq "Column names must be valid variable names. They must start with a letter and can only include letters, numbers, and underscores.">
							<cfoutput>#cfcatch.detail#</cfoutput> Please modify the column names in your csv file and upload again.
							<CF_ABORT>
						</cfif>
						<cfoutput>cfcatch.detail=  #cfcatch.detail#</cfoutput>
						<cfdump var="#cfcatch#">
						<cfrethrow>
						<!--- NJH 2008/10/06 CR-LEN503. Need to email client if file fails to load, so am rethrowing the error --->---->
						<!--- START: 2012-01-27	NYB	Case#424794 - took out the abort and assigned error values to variables instead --->
						<cfoutput>
						<cfif cfcatch.detail eq "Column names must be valid variable names. They must start with a letter and can only include letters, numbers, and underscores.">
							<cfset returnStruct.message = "#cfcatch.detail#.  Please modify the column names in your csv file and upload again.">
						<cfelse>
							<cfset returnStruct.message = "#cfcatch.message# #cfcatch.detail#">
						</cfif>
						</cfoutput>
						<cfset returnStruct.isOK = "false">
						<!--- NYB Case 432626 removed - superfluous:
						<cfset returnStruct.fileLoaded="false">
						--->
						<cfset returnStruct.numRowsInserted = 0>
						<!--- END: 2012-01-27	NYB	Case#424794 --->
					</cfcatch>
				</cftry>

				<!--- NYB Case 432626 removed - overwrites this being set to false within the catch, thereby returning an incorrect result:
				<cfset returnStruct.fileLoaded="true">
				--->
				<cfset returnStruct.numRowsInserted = importProcessQry.recordCount>
				<cfset returnStruct.importQuery = importProcessQry>

				<cfif arguments.nocheckloaddata>
					<!--- 2008/06/19 - GCC - if doing http we can get column names from the result set rather than 'predict' them
					work to enable dataload tables to be created from csv using this method
					handles stripping text qualifiers off and escaped qualifiers automatically --->

					<cfif arguments.setCreated>
						<cfset aCreated = arrayNew(1)>
						<cfset aCreatedBy = arrayNew(1)>
						<cfloop query="importProcessQry">
							<cfset arrayAppend(aCreated,createODBCDateTime(request.requestTime))>
							<cfset arrayAppend(aCreatedBy,request.relayCurrentUser.personID)>
						</cfloop>
						<cfset queryAddColumn(importProcessQry,"created","Date",aCreated)>
						<cfset queryAddColumn(importProcessQry,"createdBy","Integer",aCreatedBy)>
						<cfset arrayAppend(arguments.fieldnames,"created")>
						<cfset arrayAppend(arguments.fieldnames,"createdBy")>
					</cfif>

					<cfset stMetaData = structNew()>
					<cfset aMetaData = getMetaData(importProcessQry)>

					<cfloop array="#aMetaData#" index="i">
						<!--- START:  NYB Case 432626 2013-01-31 added if to chartype designation --->
						<cfif i.TypeName eq "varchar">			<!--- 2013-02-07 PPB Case 432626 removed arguments.charset eq "utf-16" and on Nicole's request --->
							<cfset stMetaData[i.Name] = "NVARCHAR">
						<cfelse>
						<cfset stMetaData[i.Name] = i.TypeName>
						</cfif>
						<!--- END:  NYB Case 432626 2013-01-31 --->
					</cfloop>

					<cf_querysim>
						csvcomponent
						tableColumnName,dataType,filePosition
						<cfloop from="1" to="#listLen(importProcessQry.columnlist)#" index="listIX"><CFOUTPUT>#gettoken(importProcessQry.columnlist,listIX,",")#<cfif listLen(gettoken(importProcessQry.columnlist,listIX,","),'|') lt 3>|#stMetaData[gettoken(importProcessQry.columnlist,listIX,",")]#|#htmleditformat(listIX)#</cfif>#htmleditformat(chr(13))##htmleditformat(chr(10))#</CFOUTPUT></cfloop>
					</cf_querysim>

					<cfif not isDefined("arguments.numberElementsExpected")>
						<cfset arguments.numberElementsExpected = listLen(importProcessQry.columnlist)>
					<!--- NJH 2008/10/09 CR-LEN503 SPMDataload --->
					<cfelseif isDefined("arguments.numberElementsExpected") and arguments.numberElementsExpected neq listLen(importProcessQry.columnlist)>
						<cfthrow type="Custom" message="The number of columns does not match the number of expected columns. Expected number of columns is #arguments.numberElementsExpected#">
					</cfif>

					<!--- NJH 2008/10/06 added the table options for a http method as well --->
					<cfif CompareNoCase("delete", arguments.tableOptions) EQ 0>
						<cfset deleteRecords = application.com.dbtools.DeleteRecords(tablename=arguments.tableName,whereClause=arguments.deleteQryWhereClause)>
					<cfelseif CompareNoCase("truncate", arguments.Tableoptions) EQ 0>
						<cfset TruncateRecords = application.com.dbtools.TruncateRecords(tablename=arguments.Tablename)>
					<cfelseif CompareNoCase("drop", arguments.Tableoptions) EQ 0>
						<cfset droptable = application.com.dbtools.DeleteTable(tablename=arguments.Tablename)>
					</cfif>
					<!---SSS p-lex038 2009/10/13 you can now define the name for a primary key--->
					<cfset x = putData(dataQuery=importProcessQry,increment=arguments.increment,csvComponentQuery=csvcomponent,NumberElementsExpected=arguments.numberElementsExpected,tablename=arguments.tablename,defaultPrecisionSize=arguments.defaultPrecisionSize,primarykeyname=arguments.primarykeyname)>

				<!--- NJH 2009/06/04 Bug Fix shouldn't be returning a query, but rather the structure which gets returned at the end of the function.
				<cfelse>
						<cfreturn importProcessQry>
					--->
				</cfif>

			<!--- Not doing an HTTP call, but rather reading the file using cffile --->
			<cfelse>
				<!--- 2008-08-28 NYB P-LEX014 - in cf_querysim replaced ->
				<cfloop from="1" to="#arrayLength#" index="arrayIdx"><CFOUTPUT>#arrayToList(arguments.fieldnames[arrayIdx],"|")##chr(13)##chr(10)#</CFOUTPUT></cfloop>
				!--- with: ---
				<cfloop from="1" to="#arrayLength#" index="arrayIdx"><CFOUTPUT>#arguments.fieldnames[arrayIdx]##chr(13)##chr(10)#</CFOUTPUT></cfloop>
				!--- <- P-LEX014 - as it throwing erroring --->
				<cf_querysim>
					csvcomponent
					tableColumnName,dataType,filePosition
					<cfloop from="1" to="#arrayLength#" index="arrayIdx"><CFOUTPUT>#arguments.fieldnames[arrayIdx]#<cfif listLen(arguments.fieldnames[arrayIdx],'|') lt 3>|text|#htmleditformat(arrayIdx)#</cfif>#htmleditformat(chr(13))##htmleditformat(chr(10))#</CFOUTPUT></cfloop><!--- 2011-09-09 STCR LID4952 Added check to prevent breaking fileToDB usage for code where the dataType and filePosition has been explicitly specified. --->
				</cf_querysim>
				<cfif not isDefined("arguments.numberElementsExpected")>
					<cfset arguments.numberElementsExpected = arrayLength>
				</cfif>

				<!--- Read the file --->
				<cffile action="READ" file="#arguments.dir#\#arguments.filename#" variable="csvimport" charset="#arguments.charset#">

				<!--- remove any occurrences of the end of row delimiter that is not an end of row delimiter; ie. that are text qualified --->
				<cfif (arguments.endRowDelim eq "#chr(13)##chr(10)#")>
					<!--- remove any newlines or carriage returns that are text qualified --->
					<cfset csvimport = fn_removeSpecialCharsFromTextQualifiedString(textQualifiedString=csvimport,charToRemove="#Chr(13)#")>
					<cfset csvimport = fn_removeSpecialCharsFromTextQualifiedString(textQualifiedString=csvimport,charToRemove="#Chr(10)#")>
				<cfelse>
					<!--- remove any occurrences of the end of row delimiter that are text qualified --->
					<cfset csvimport = fn_removeSpecialCharsFromTextQualifiedString(textQualifiedString=csvimport,charToRemove=arguments.endRowDelim)>
				</cfif>

				<!--- Build our query of good data; If the number of columns is less than that specified, dump it into the 'Bad Row Query' --->
				<cfloop index="logRow" list="#csvimport#" delimiters="#arguments.EndRowDelim#">
					<cfset rowCount = rowCount + 1>
					<cfset logRow = fn_listFix(list=logRow,delimiter=arguments.Delim)> <!--- replace empty elements with the null value ---><!--- 2011-09-01 STCR LID4952 use the listFix support for other delimiter characters besides comma --->
					<cfset logRow = fn_removeSpecialCharsFromTextQualifiedString(textQualifiedString=logRow,charToRemove=arguments.Delim)>

					<cfif listLen(logRow,arguments.Delim) eq csvcomponent.recordCount>
						<cfset importprocess = importprocess & logRow & arguments.EndRowDelim>
					<cfelse>
						<cfset QueryAddRow(importLostQuery)>
						<cfset QuerySetCell(importLostQuery, "rowNum",rowCount)>
						<cfset QuerySetCell(importLostQuery, "data",logRow)>
					</cfif>
				</cfloop>

				<!--- if we want to validate the file and some columns don't contain the correct number of columns, don't load the file --->
				<cfif arguments.validateFile and importLostQuery.recordCount neq 0>
					<cfset returnStruct.fileLoaded="false">
				<cfelse>
					<cftransaction action = "begin">
					<!--- 	<cftry> --->
							<cfif CompareNoCase("delete", arguments.tableOptions) EQ 0>
								<cfset deleteRecords = application.com.dbtools.DeleteRecords(tablename=arguments.tableName,whereClause=arguments.deleteQryWhereClause)>
							<cfelseif CompareNoCase("truncate", arguments.Tableoptions) EQ 0>
								<cfset TruncateRecords = application.com.dbtools.TruncateRecords(tablename=arguments.Tablename)>
							</cfif>
							<cfset rowCount = 0>
							<cfloop index="logRowProcess" list="#importprocess#" delimiters="#arguments.EndRowDelim#">
								<cfset rowCount = rowCount+1>
								<cfset rowArray = listToArray (logRowProcess,arguments.Delim)>
								<cfset indexCounter = indexCounter + 1>
									<cfif not (arguments.ColumnNames and indexCounter eq 1)>
									<!---SSS 2009-11-17  P-lex039 added debug=no--->
										<cfquery name="insertcsv" datasource="#application.siteDataSource#" debug="No">
											INSERT into  #arguments.Tablename# (
												<cfloop index="index" from="1" to="#csvcomponent.recordcount#" step="1">
													<cfif index NEQ 1>,</cfif>
														<cf_queryobjectname value = "#csvcomponent["tableColumnName"][index]#">
													<cfif index eq csvcomponent.recordcount and arguments.setCreated>
														,created,createdBy
													</cfif>
												</cfloop>)
											values (
											<cfloop index="index" from="1" to="#csvcomponent.recordcount#" step="1">
												<cfset tempValue = rowArray[csvcomponent["fileposition"][index]]>
												<!--- NJH 2008/05/15 no longer needed as empty elements now have the value of "null"
													which is done by the fn_listFix above
												<cfset tempValue = removeChars(tempValue,1,1)>
												<cfif len(tempValue) eq 0>
													<cfset tempValue = "null">
												</cfif> --->
												<cfif index NEQ 1>,</cfif>
												<cfif listFindNoCase("int,float",csvcomponent["dataType"][index])>
													<cf_queryparam value="#tempValue#" CFSQLTYPE="CF_SQL_NUMERIC" >
												<cfelseif listFindNoCase("text",csvcomponent["dataType"][index]) and tempValue neq "NULL">
													<cf_queryparam value="#tempValue#" CFSQLTYPE="CF_SQL_VARCHAR" > <!--- STCR 2012-05-17 P-REL109 Enable Unicode character support --->
												<cfelseif tempValue eq "NULL">
													null
												<cfelse>
													<cf_queryparam value="#tempValue#" CFSQLTYPE="CF_SQL_VARCHAR" >
												</cfif>
												<cfif index eq csvcomponent.recordcount and arguments.setCreated>
													,<cf_queryparam value="#createODBCDateTime(request.requestTime)#" CFSQLTYPE="cf_sql_timestamp" >
													,#request.relayCurrentUser.personID#
												</cfif>
											</cfloop>
											)
										</cfquery>
									</cfif>
							</cfloop>
							<cfset returnStruct.numRowsInserted = rowCount>
							<cfset returnStruct.fileLoaded="true">

							<!--- <cfcatch type="Any">
								<cfset returnStruct.fileLoaded="false">
								<cftransaction action="rollback"/>
							</cfcatch>

						</cftry> --->

					</cftransaction>

				</cfif>
				<cfset returnStruct.badRecords = importLostQuery>
			</cfif>
			<!--- START: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location --->
			<cfcatch type="any">
				<!--- BEGIN 2012-05-24 STCR P-REL109 - give the caller the option to handle exceptions more gracefully --->
				<cfif arguments.rethrowOnError and arguments.destinationDirectory eq "">
					<cfrethrow />
				</cfif>
				<!--- END 2012-05-24 STCR P-REL109 --->
				<cfoutput><p>filetoDb error: <br/><cfdump var="#cfcatch#" expand="false"></p></cfoutput>
				<!--- 2011/01/12 NYB added if - if destinationDirectory never passed then copy is never created to be deleted --->
				<cfif arguments.destinationDirectory is not "">
					<cfif fileexists("#arguments.destinationDirectory#\#filename#")>
						<cffile action="delete" file="#arguments.destinationDirectory#\#filename#" />
					</cfif>
					<cfthrow detail="Error Deleting #arguments.destinationDirectory#\#filename#">
				</cfif>
			</cfcatch>
		</cftry>

		<!--- START: 2011/01/12 NYB added if - if destinationDirectory never passed then copy is never created to be deleted --->
		<cfif arguments.destinationDirectory is not "" and arguments.sourceDirectory neq arguments.destinationDirectory>
			<cfif directoryexists(arguments.sourceDirectory) and directoryexists(arguments.destinationDirectory)>
				<!--- get all the files in the destination folder --->
				<cfdirectory name="dataLoadFiles" directory="#arguments.destinationDirectory#" />
				<!--- get the file that was moved plus any files that have been in the directory for more than 2 hours --->
				<cfquery name="filteredDataLoadFiles" dbtype="query">
					select * from dataLoadFiles
					where
						(dateLastModified < #dateadd('h',-2,now())#
							or name = '#arguments.filename#')

						and type = 'File'
				</cfquery>
				<!--- loop through the results and delete the files --->
				<cfoutput query="dataLoadFiles">
					<cffile action="delete" file="#arguments.destinationDirectory#\#name#" />
				</cfoutput>
			</cfif>
		</cfif>
		<!--- END: 2010-04-27 AJC LID3239 Copy dataload from secure location to publically accessible location --->

		<!--- NJH 2010/08/17 RW8.3 clean up!!  delete the temporary file if it exists --->
		<cfif structKeyExists(TemporaryFolderDetails,"path") and fileExists("#TemporaryFolderDetails.path#\#arguments.filename#")>
			<cffile action="delete" file="#TemporaryFolderDetails.path#\#arguments.filename#" />
		</cfif>

		<cfreturn returnStruct>

	</cffunction>

	<!--- NJH 2008/04/11 added for CR-LEN507 --->
	<!--- more efficient than single inserts but will get stack overflow if this number is too large
			- recursive loop to reduce the number of insert statements we fire at the database
		WAB 2011/05/05 problems with dataloads bot speed and bringing the system to a halt
			I reduced the default increment so that we inserted fewer rows at a time
			Also felt that the recursion might be causing memory problems.  Replaced it with an outer loop.

			--->
	<cffunction name="putData" access="private" output="false">
		<cfargument name="queryStartRow" default="1">
		<cfargument name="increment" required="true">
		<cfargument name="dataQuery" required="true">
		<cfargument name="csvComponentQuery" required="true">
		<cfargument name="NumberElementsExpected" required="true">
		<cfargument name="Tablename" required="true">
		<cfargument name="defaultPrecisionSize" required="true">
		<cfargument name="primarykeyname" default="" type="string"><!--- p-lex038 SSS 2009/10/13 you can now define the name for a primary key --->

		<cfscript>
			var insertcsv = "";
			var EndRowTmp = "";
			var StartRowTmp = "";
			 var columnSize = '' ;
			 var tempValue = '' ;
			 var thisCol = '' ;
			 var myColumnName = '' ;
			 var i = '' ;
			 var CreateDataloadTable = '' ;
			 var createtableprimarykey = '' ;
			var firstPass = true ;
		</cfscript>

		<cfif application.com.dbtools.TableExists(tablename=arguments.tablename) EQ 0>
			<cfset columnSize = structNew()>
			<cfloop list="#dataQuery.Columnlist#" index="thisCol">
				<cfset columnSize[thisCol] = 20> <!--- SSS 2009-01-23 changed this to 20 so that column can be created, then to 20 as a default size --->
			</cfloop>
			<cfloop query="dataQuery">
				<cfloop list="#dataQuery.Columnlist#" index="thisCol">
					<cfset columnSize[thisCol] = max(columnSize[thisCol] ,len(dataQuery[thisCol][currentrow]))>
				</cfloop>
			</cfloop>

			<!---SSS 2009/01/28 Adds to the column size count so that it leaves some leeway to add more to the table --->
			<cfloop collection=#columnsize# item="i">
				<cfset columnsize[i] = round(columnsize[i]/10+1) * 10>
			</cfloop>

			 <!--- 2008-08-28 NYB P-LEX014 - added try statement and retry with default precision -> --->
			 <!--- 2008-01-29 SSS Having fixed the precision problem the try catch is not longer needed --->
				<cfquery name="CreateDataloadTable" datasource="#application.siteDataSource#">
					 if not exists (SELECT * FROM sysobjects where type = 'u' and name =  <cf_queryparam value="#arguments.Tablename#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					CREATE TABLE [dbo].[#arguments.Tablename#](
						<CFLOOP QUERY ="arguments.csvComponentQuery">
							<cfif currentRow is not 1>,</cfif>
							[#TABLECOLUMNNAME#] #DATATYPE#(#columnSize[TABLECOLUMNNAME]#)
						</CFLOOP>)
				</cfquery>
				<!--- SSS lex038 2009/10/13 you can now define the name for a primary key --->
				<cfif arguments.primarykeyname NEQ "">
					<cfquery name="createtableprimarykey" datasource="#application.siteDataSource#">
						ALTER TABLE [dbo].[#arguments.Tablename#] ADD [#arguments.primarykeyname#] [int] IDENTITY (1, 1) NOT NULL primary key
					</cfquery>
				</cfif>
					</cfif>

		<cfloop index="StartRowTmp" from="#queryStartRow#" to="#dataQuery.recordCount#" step="#arguments.increment#">

			<!--- WAB 2011/04/26 getting time-outs on large dataloads,
				Lets say we need 1 second per row
				this function ensures that there is at least that much time left
				--->

			<cfset application.com.request.extendTimeOutIfNecessary(arguments.increment * 1)>


			<cfset EndRowTmp = min(StartRowTmp + arguments.increment - 1,dataQuery.recordCount)>

				<cfset stDataTypes = structNew()>
				<cfloop query="csvComponentQuery">
					<cfset stDataTypes[tableColumnName] = dataType>
				</cfloop>
				<cfquery name="insertcsv" datasource="#application.sitedatasource#">
				   INSERT INTO #arguments.Tablename# (#valuelist(csvComponentQuery.tableColumnName)#)
				   <cfloop query="dataQuery" startrow = "#StartRowTmp#" endrow = "#EndRowTmp#">
				      SELECT
						<cfset firstPass = true>
						<cfloop list="#valuelist(csvComponentQuery.tableColumnName)#" index="myColumnName"><cfset tempValue = evaluate(myColumnName)><cfif firstPass><cfset firstPass = false><cfelse>,</cfif><cfif listFindNoCase("integer,date",stDataTypes[myColumnName])>#tempValue#<cfelse>  <cf_queryparam value="#tempValue#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif></cfloop>
				      <cfif currentrow LT EndRowTmp>UNION ALL</cfif>
				   </cfloop>
				</cfquery>

				<!--- <cf_log file="dataload" text="Rows #StartRowTmp# - #EndRowTmp# done"> --->
		</cfloop>


<!--- 	<cfset newstartrow = queryEndRow+1>
		<cfif arguments.dataQuery.recordcount gt newstartrow>
			<cfreturn putData(queryStartRow=newstartrow,increment=arguments.increment,dataQuery=arguments.dataQuery,csvComponentQuery=arguments.csvComponentQuery,NumberElementsExpected=arguments.NumberElementsExpected,Tablename=arguments.tablename,defaultPrecisionSize=arguments.defaultPrecisionSize)>
		<cfelse>
			<cfreturn>
		</cfif>
--->
	</cffunction>


	<!--- NJH 2009/10/21 P-FNL079 - function to export to sales force --->
	<cffunction name="SalesForceExportOpportunity" access="public" hint="Exports relayware opportunities to SalesForce" returntype="query" output="false">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceExport(argumentCollection=arguments)>
	</cffunction>


	<!--- P-PAN004 - a wrapper import function... might need to revisit this.. --->
	<cffunction name="SalesForceImport" access="public" hint="Imports a salesForce object" returntype="struct">
		<cfargument name="importEntities" type="string" required="true">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfif listFindNoCase(arguments.importEntities,"asset")>
			<cfreturn SalesForceImportAsset(argumentCollection=arguments)>
		<cfelseif listFindNoCase(arguments.importEntities,"opportunity")>
			<cfreturn SalesForceImportOpportunity(argumentCollection=arguments)>
		</cfif>

	</cffunction>

	<!--- NJH 2009/10/21 P-FNL079 - function to import opportunities from sales force --->
	<cffunction name="SalesForceImportOpportunity" access="public" hint="Imports SalesForce opportunities into Relayware" returntype="struct">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceImport(argumentCollection=arguments)>
	</cffunction>

	<!--- NJH 2009/10/21 P-FNL079 - function to import opportunities from sales force --->
	<cffunction name="SalesForceImportAsset" access="public" hint="Imports SalesForce assets into Relayware" returntype="struct">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceImport(dtdID=arguments.dtdID,updateExisting=true,object="asset")>
	</cffunction>


	<cffunction name="SalesForcePOLSynch" access="public" hint="Synchs existing POL data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForcePOLSynch(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="SalesForceProcessPendingPolSynch" access="public" hint="Synchs existing POL data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">

		<cfreturn salesForcePOLSynch(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="SalesForceOppSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceOppSynch(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="SalesForceProcessPendingOppSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">

		<cfreturn salesForceOppSynch(argumentCollection=arguments)>
	</cffunction>

	<!--- START:	2014-03-21	MS	P-EVT003 New function SalesForceCustObjSynch to synch custom standalone objects form SFDC to RW for Evault --->
	<cffunction name="SalesForceCustObjSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceCustObjSynch(argumentCollection=arguments)>
	</cffunction>
	<!--- END:  	2014-03-21	MS	P-EVT003 New function SalesForceCustObjSynch to synch custom standalone objects form SFDC to RW for Evault --->

	<!--- START:	2014-06-30	MS	P-EVA003 New function SalesForceProcessPendingCustObjSynch to process all the records from custom standalone objects --->
	<cffunction name="SalesForceProcessPendingCustObjSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">

		<cfreturn application.com.salesForce.SalesForceCustObjSynch(argumentCollection=arguments)>
	</cffunction>
	<!--- END  :	2014-06-30	MS	P-EVA003 New function SalesForceProcessPendingCustObjSynch to process all the records from custom standalone objects --->


	<!--- START:	2013-08-21	MS	P-KAS001 New function SalesForceLeadSynch for the new RW Opps to SFDC Lead Synch functionality --->
	<cffunction name="SalesForceLeadSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="dtStartTime" type="date" required="true">

		<cfreturn application.com.salesForce.SalesForceLeadSynch(argumentCollection=arguments)>
	</cffunction>
	<!--- END:  	2013-08-21	MS	P-KAS001 New function SalesForceLeadSynch for the new RW Opps to SFDC Lead Synch functionality --->

	<!--- START: 2014-09-08 REH Case P-NEX001 Added where clause settings for Lead Synch --->
	<cffunction name="SalesForceProcessPendingLeadSynch" access="public" hint="Synchs existing Opportunity data with SalesForce" returnType="struct">
		<cfargument name="maxModRegisterID" type="numeric" required="yes">
		<cfargument name="dtdID" type="numeric" required="true">

		<cfreturn application.com.salesForce.SalesForceLeadSynch(argumentCollection=arguments)>
	</cffunction>
	<!--- END: 2014-09-08 REH Case P-NEX001 --->

	<cffunction name="getLastDataTransferRun" access="public" returnType="date">
		<cfargument name="dtdID" type="numeric" required="true">
		<cfargument name="lastSuccessfulRun" type="boolean" required="false">
		<cfargument name="convertToUTC" type="boolean" default="false">

		<cfscript>
			var getLastRunDate = "";
			var lastRunDate = "";
		</cfscript>

		<!--- '2010-05-13 13:15:00' --->
		<cfquery name="getLastRunDate" datasource="#application.siteDataSource#">
			select isNull(max(startTime),getDate()) as lastRunDate from dataTransferHistory
				where dtdID = #arguments.dtdID#
				<cfif structKeyExists(arguments,"lastSuccessfulRun")>
					and isOK =  <cf_queryparam value="#iif(arguments.lastSuccessfulRun,1,0)#" CFSQLTYPE="CF_SQL_bit" >
				</cfif>
		</cfquery>

		<cfset lastRunDate=getLastRunDate.lastRunDate>

		<cfif convertToUTC>
			<cfset lastRunDate=application.com.dateFunctions.convertServerDateToUTC(lastRunDate)>
		</cfif>

		<cfreturn lastRunDate>
	</cffunction>

</cfcomponent>

