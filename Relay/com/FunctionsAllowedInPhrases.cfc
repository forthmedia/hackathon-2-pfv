<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

These are all stub functions which call other functions in the application.com scope.
	This allows us to control which functions are available for use within phrases
	- any expression containing application.com is rejected, but we can alias this component as say fn.

	
	WAB 2008/06/19 added outputAddress function
	NYB	2009/02/19 Sophos Stargate 2
	WAB/NJH 2009/03/03 Lighthouse Issue All Sites 1739 - added function to get the url of an external site.
	NYB	2009/03/05 Sophos Stargate 2
	NYB 2009/06/11 removed IsFlagSet function - func in redirectorAllowedFunctions.cfc - which extends this file 
	NJH	2010/03/23	P-PAN002 - added encryptURL function
 --->

<cfcomponent displayname="functions which can be used within phrases etc." hint="">


	<cffunction name = "URLParamsForLinkToElement" return="string" output="no">
		
		<cfargument name="elementID" required="yes" type="string">
		<cfreturn application.COM.relayElementTree.createSEIDQueryString (linktype="tracked",elementid = elementID,personid = request.relayCurrentUser.personid)>
	
	</cffunction>


	<cffunction name = "if" return="string" output="no">
		<cfargument name="condition" required="yes" type="string">
		<cfargument name="trueResult" required="yes" type="string">
		<cfargument name="falseResult" required="yes" type="string">
		
		<cfif condition>
			<cfreturn trueResult>
		<cfelse>
			<cfreturn falseResult>
		</cfif>
	
	</cffunction>
	
	<cffunction name="SalesRepDetails" return="string" output="yes">
		<cfargument name="personID" required="yes" type="numeric">
		<cfargument name="flagTextID" required="yes" default = "AccSalesRep" type="string">
				
		<cfset var getSalesRep="">

		<cfif not application.com.flag.doesFlagExist(flagID='#arguments.flagTextID#')>
			<cfreturn "">		
		<cfelse>		
			<!--- 2007/03/01 GCC needs a lot fo work - hacked in for Lenovo - can work with integer and integer multiple but not a lot else I guess --->
			<cfquery name="getSalesRep" datasource="#application.sitedatasource#">		
				SELECT   top 1  person.firstname,person.lastname,person.email
				FROM         Person Person_1 INNER JOIN
		                      #application.com.flag.getFlagStructure(arguments.flagTextID).FlagType.DataTableFullName# fd ON Person_1.OrganisationID = fd.entityid INNER JOIN
		                      flag ON fd.flagid = flag.FlagID LEFT OUTER JOIN
		                      Person ON fd.data = Person.PersonID
				WHERE     (Flag.FlagTextID =  <cf_queryparam value="#arguments.flagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ) AND (Person_1.PersonID = #arguments.personID#)
				order by person.firstname,person.lastname,person.email,person.personid
			</cfquery>
			<cfif getSalesRep.recordcount eq 0>
				<cfreturn "">
			<cfelse>
				<cfreturn "#getSalesRep.firstname# #getSalesRep.lastname# #getSalesRep.email#">
			</cfif>
		</cfif>
	</cffunction>
	
	<cffunction name="BPDBID" return="string" output="yes">
		<cfargument name="personID" required="yes" type="numeric">
		
		<cfset var getBPDBID="">
		
		<!--- 2007/03/01 GCC hacked in for Lenovo - I know it shouldn't be here.... --->			
		<cfquery name="getBPDBID" datasource="#application.sitedatasource#">	
			IF exists (select 1 from sysobjects where name = 'lenovoAgreement' and xtype='u')
				BEGIN
				SELECT   top 1  lenovoAgreement.CABPDBID
			FROM         lenovoAgreement INNER JOIN
			                      Person ON lenovoAgreement.OrganisationID = Person.OrganisationID
			WHERE     (Person.PersonID = #arguments.personID#)
			ORDER BY AgreementNumber
				END
			ELSE
				select NULL as CABPDBID	
		</cfquery>
		<cfreturn getBPDBID.CABPDBID>
	</cffunction>

	<cffunction name="dump" return="string" output="yes">
		<cfargument name="var" required="yes" >	
		<cfset var resultHTML = "">
		<cfsavecontent variable = "resultHTML">
			<cfdump var="#var#">
		</cfsavecontent>
		<cfreturn resultHTML>
	</cffunction>

	<!--- 2008/06/19 WAB testing outputting of addresses in emails--->
	<cffunction name="outputAddress">
			<cfreturn application.com.screens.evaluateAddressFormat (argumentCollection = arguments)>
	</cffunction>

	<!--- 2009/02/19 NYB Sophos Stargate 2 --->
	<cffunction name="getFlagData">
		<cfargument name="FlagID" required="yes" >	
		<cfargument name="EntityID" required="yes" >	
		<cfreturn application.com.flag.getFlagData(flagId = FlagID,entityid=EntityID).data>
	</cffunction>

	<!--- 2009/06/11 NYB - removed IsFlagSet function - func in and included by redirectorAllowedFunctions.cfc --->
	
	<!--- WAB/NJH 2009/03/03 Lighthouse Issue All Sites 1739 --->
	<cffunction name="getExternalSiteURL">
		<cfargument name="site" default =  "">	
		<!--- <cfargument name="country" default =  "">	 --->
		
		<cfreturn application.com.relayCurrentSite.getExternalSiteURL(argumentCollection = arguments)>
		
	
	</cffunction>

	<!--- NJH P-PAN002 2010/03/23 --->
	<cffunction name="encryptURL">
		<cfreturn application.com.security.encryptURL(argumentcollection=arguments)>
	</cffunction>

</cfcomponent>

