<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			oppCustomers.cfc
Author:				SKP
Date started:		2010

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Date (20-MAY-2010)	SKP	new CFC started to extend opportunity.cfc
Date (25-May-2010)  New edit to commit with LHID
2016/12/12	NJH JIRA PROD2016-2937 - remove the reseller OrgID parameter. Now done in rights. Added rights sql to opportunities query
 --->
<cfcomponent hint="Get my customers for internal" displayname="Opportunity, customers component">
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             SET UP ONE PARAM PER DB COLUMN
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">

	<cffunction access="public" name="getCustomersWithDealsAndRenewals" hint="Returns the opportunities for a specified organisation for use in my customers" returntype="query">
		<!--- <cfargument name="reseller OrgID" required="yes" type="numeric">
		<cfargument name="organisationID" required="no" default="0"> --->
	 	<cfargument name="sortOrder" required="no" default="org.organisationID,  o.expectedCloseDate">

	 	<cfset var qryOpportunities = "" />
	 	<cfset var oppRightsQuerySnippet = application.com.rights.getRightsFilterQuerySnippet(entityType="opportunity",Alias="o")>
		<cfset var oppRightsJoin = oppRightsQuerySnippet.join>
		<cfset var oppRightsWhereClause = oppRightsQuerySnippet.whereClause>

		<cfquery name="qryOpportunities" datasource="#application.siteDataSource#">

			SELECT     o.opportunityID,
					   o.expectedCloseDate,
					   o.actualCloseDate,
					   o.detail AS opportunityDescription,
					   SUM(ISNULL(op.subtotal, 0)) AS TotalValue,
					   o.currency,
					   ot.oppTypeTextID,
					   org.organisationName,
					   org.organisationID,
					   month(o.expectedCloseDate) as closeMonth,
					   year(o.expectedCloseDate) as closeYear
			FROM       opportunity AS o
			INNER JOIN organisation org on o.entityid = org.organisationid
			INNER JOIN OppStage ON  OppStage.OpportunityStageID = o.stageID
			INNER JOIN OppType ot ON  ot.OppTypeID = o.OppTypeID
			LEFT OUTER JOIN  opportunityProduct AS op ON o.opportunityID = op.OpportunityID
			#preserveSingleQuotes(oppRightsJoin)#
			<!--- INNER JOIN location on location.locationID = o.partnerLocationID
				AND location.organisationID = #arguments.reseller OrgID# --->

			WHERE  (OppStage.stageTextID NOT IN ('oppStageClosed'))
			AND (OppStage.LiveStatus = 1)
			AND (ot.oppTypeTextID = 'Deals' OR ot.oppTypeTextID = 'Renewal')
			#preserveSingleQuotes(oppRightsWhereClause)#
			GROUP BY  org.organisationID, org.organisationName, o.opportunityID, o.expectedCloseDate, o.actualCloseDate, o.detail, o.currency, ot.oppTypeTextID
			ORDER BY #arguments.sortOrder#
		</cfquery>
		<!--- order by columns above very important do not remove --->
		<cfreturn qryOpportunities>
	</cffunction>

</cfcomponent>

