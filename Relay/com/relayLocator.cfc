<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2008/03/19   WAB fixed bug which occurred when page hit by IPCheck - so the browser did not have a country/language combination.
			one place where needed to put a check for isdefined arguments.countryid and also added test for not being 0.

2008/08/06  NJH CR-LEN547 Added 'with (noLock)' to the queries and cached queries to improve performance
2008/09/23 	NJH Bug Fix Lenovo Issue 1182 - dealing with apostrophes in the sitename -also updated some vars
2008-12-09 		NYB 		added param DefaultFlags and RequiredFlags
2009-03-27	NYB Demo Locator
2009-04-27 SSS Added 2 functions one to upload geodata and one to insert geodata
2009-06-10 GCC LID2344: Added session scope locking for MX7 backwards compatibility and additional function var scoping.
2009-06-10 SSS p-len008 Geodata: added N to the queries to enable kanji support
2011/01/13	NJH	LID 5188 - added an 'in' for the inner join on isocode as the UK has three country iso codes (ENG,SCO and WAL)
2011/05/25	NYB		REL106 various changes.  Moved -v3 functions to end.
2011/10/20	NYB		LHID7283 - changed locator to use CountryScope - making locatorDef.CountryGroupid redundant
2012/06/15	IH		Case 428703 allow multiple OrgApprovalFlagTextID
2013-03-13 	NYB 	Case 434064 made changes to getLocatorDefinitions query
2013-10-02 	NYB 	Case 435669 addedgetLocationsRequiringLatLongUpdate function (Case 434064 will need to be released with this if not already done)
2013-10-02 	NYB 	Case 435669-2 change to query in getResellersByTextSearch function - adding support for OrgApprovalFlagTextID being org, loc or country
2013-12-05 	NYB 	Case 436665 REMOVED if not len(url.allLocations) around limiting results by required flags in the locatordef in the getLocationsRequiringLatLongUpdate function
2014-01-28	YMA		Case 438652 Fixed map doesnt re-load after a new search.
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2014-03-05 	NYB 	Case 438911 moving bug fixes made on 2013 into 2014 and eVault Locator, and moving some 2014 changes into 2013 - esp limiting updates to countries the user has rights to
2014-03-28	NYB		Case 439188 Altered the locator so it doesn't fall over when a countryid is passed that there is no LocatorDefinition setup for, in this case the drop down will include a Please Select A Country option that it defaults to
2014-10-22  WAB 	CASE 442259 Large number of changes to add Area Search.  Note needs some SQL scripts run first and a CF script run afterwards to update existing data
/2014-11-12  WAB 	CASE 442259 continued.  Changed the names of the administrative areas fields to not have underscores in them - because updateData gets confused.
2014-11-24  WAB 	CASE 442259 continued.  Deal with state abbreviations.   Add code for doing release script to populate administrativeArea fields
2016-01-17	WAB		Global change of variable names replaceStatement and replaceValueStatement. Add _sql to end to pass codecop
2016-02-23	WAB/DCC Case 448017 new function jsStringFormat_ for renesas customisation revealed a scenario where the html_data.map is stored as a js variable and required the closing </script> tag needed to be escaped
			the infowindowtext in the markerArray infoWindowText:'#jsStringFormat_(marker.infoWindowText)#' in custom relayLocator.cfc deals with this and to persist on the pages
			<cfif prefilter>
				#display_html.map#
				<SCRIPT>/*persist the global js variable - other approach html5 not supported in older browsers localStorage.setItem could be used in future When older browsers no longer supported */
				 searchResultsMap = <cfoutput>'#application.com.relayLocator.jsStringFormat_(display_html.map)#'</cfoutput>
				</SCRIPT>
			</cfif>
2016-10-08	WAB		CASE 450971 Improve performance of getMarkers().  Don't use our queryRowToStruct, just do a cfscript loop over query

--->


<cfcomponent displayname="locator" hint="componentFunction" output="false">

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
             Returns columnNames from a query
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLocatorCounties" hint="Returns locator counties">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="place" type="string" required="no">
		<cfargument name="useFormVariables" default="true">
		<cfscript>
			var querydetail = "";
			var locatorRegionsQry = "";
			var getIsoCode = "";
			querydetail = createGeoDataSearchQueryFromFormVariables();
		</cfscript>

		<!--- this is used for performance reasons!!! --->
		<cfquery name="getIsoCode" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select isoCode3 from geoCountry gc with (noLock)inner join country c with (noLock)on c.isoCode = gc.isoCode2 where countryID=#arguments.countryID#
		</cfquery>
		<!--- GCC - this cannot be cached! It caches with criteria --->
		<cfquery name="locatorRegionsQry" datasource="#application.siteDataSource#" <!---cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#"--->>
			select distinct province, plainProvince from geoData d with (noLock)inner join geoCountry gc with (noLock)
				on d.isoCode = gc.isoCode3 and d.isoCode
					<cfif getIsoCode.recordCount gt 1>in ('#valueList(getIsoCode.isoCode3,"','")#')<cfelse>= '#getIsoCode.isoCode3#'</cfif> inner join country c with (noLock)
				on c.isoCode = gc.isoCode2
				<cfif arguments.useFormVariables>#queryDetail.additionaljoins_sql#</cfif>
			where c.countryID = #arguments.countryID#
				<cfif arguments.useFormVariables>#preserveSingleQuotes(queryDetail.whereStatement_sql)#</cfif>
				and len(province) > 0
			order by province
		</cfquery>

		<cfreturn locatorRegionsQry>
	</cffunction>


	<cffunction access="public" name="getLocatorTowns" hint="Returns locator towns">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="province" type="string" required="false">
		<cfargument name="useFormVariables" default="true">

		<cfscript>
			var getIsoCode = "";
			var geoQueryDetail ="";
			var flagQueryDetail = "";
			var locatorTownsQry = "";
			var replaceValueStatement_SQL = replaceSpecialCharactersForDBQuery().replaceValueStatement_SQL;
			var replaceStatement_SQL = replaceSpecialCharactersForDBQuery().replaceStatement_SQL;

			geoQueryDetail = createGeoDataSearchQueryFromFormVariables();
			flagQueryDetail = createFlagSearchQueryFromFormVariables();
		</cfscript>


		<!--- this is used for performance reasons!!! --->
		<cfquery name="getIsoCode" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select isoCode3 from geoCountry gc with (noLock) inner join country c with (noLock) on c.isoCode = gc.isoCode2 where countryID=#arguments.countryID#
		</cfquery>

			<!---
			NJH 2008/08/05 CR-LEN547 use plainPlace field that has all special characters already removed
			select distinct place, #replaceStatement_SQL#lower(place)#replace(replaceValueStatement_SQL,"**","'","ALL")# as ajaxPlace
			--->

		<cfquery name="locatorTownsQry" datasource="#application.siteDataSource#" <!--- cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#" --->>
			select distinct place, lower(plainPlace) as ajaxPlace
			from geoData d with (noLock) inner join geoCountry gc with (noLock)
				on d.isoCode = gc.isoCode3 and d.isoCode
					<cfif getIsoCode.recordCount gt 1>in ('#valueList(getIsoCode.isoCode3,"','")#')<cfelse>= '#getIsoCode.isoCode3#'</cfif> inner join country c with (noLock)
				on c.isoCode = gc.isoCode2
				<cfif arguments.useFormVariables>#geoQueryDetail.additionaljoins_sql#</cfif>
			where c.countryID = #arguments.countryID#
				<cfif arguments.useFormVariables>#preserveSingleQuotes(geoQueryDetail.whereStatement_sql)#</cfif>
				and len(place) > 0
			order by place
		</cfquery>

		<cfreturn locatorTownsQry>
	</cffunction>


	<cffunction access="public" name="getLocatorPostCodes" hint="Returns locator post codes">
		<cfargument name="countryID" type="numeric" required="yes">
		<cfargument name="province" type="string" required="false">
		<cfargument name="place" type="string" required="false">
		<cfargument name="postalCode" type="string" required="false">
		<cfargument name="useFormVariables" default="true">

		<cfscript>
			var querydetail = "";
			var locatorPostCodeQry = "";
			querydetail = createGeoDataSearchQueryFromFormVariables();
		</cfscript>

		<!--- this is used for performance reasons!!! --->
		<cfquery name="getIsoCode" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select isoCode3 from geoCountry gc with (noLock) inner join country c with (noLock) on c.isoCode = gc.isoCode2 where countryID=#arguments.countryID#
		</cfquery>

		<cfquery name="locatorPostCodeQry" datasource="#application.siteDataSource#" <!--- cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#" --->>
			select distinct d.postalCode from geoData d with (noLock) inner join geoCountry gc with (noLock)
				on d.isoCode = gc.isoCode3 and d.isoCode
					<cfif getIsoCode.recordCount gt 1>in ('#valueList(getIsoCode.isoCode3,"','")#')<cfelse>= '#getIsoCode.isoCode3#'</cfif> inner join country c with (noLock)
				on c.isoCode = gc.isoCode2
				<cfif arguments.useFormVariables>#queryDetail.additionaljoins_sql#</cfif>
			where c.countryID = #arguments.countryID#
				#preserveSingleQuotes(queryDetail.whereStatement_sql)#
				and len(d.postalCode) > 0
			order by d.postalCode
		</cfquery>
		<cfreturn locatorPostCodeQry>
	</cffunction>


	<cffunction access="public" name="getLocatorDefinition" hint="Returns locator definition">
		<cfargument name="locatorID" type="numeric" required="no">
		<cfargument name="countryID" type="numeric" required="no">
		<cfargument name="cache" type="boolean" default="true">
		<cfargument name="all" type="boolean" default="false">  <!--- 2013-10-02 NYB Case 435669 added --->
		<cfargument name="limitByCountryRights" type="boolean" default="false">  <!--- 2014-03-05 NYB Case 438911 added --->

		<cfset var getLocatorDefinition = "">
		<!--- <cfset var defaultCountry = application.com.settings.getSetting('settings.locator.defaultCountryGroup')> --->
		<cfset var maxrowsVar = 1>  <!--- 2013-10-02 NYB Case 435669 added --->

		<cfif cache>
			<cfset cacheTimeSpan = CreateTimeSpan(0, 0, 15, 0)>
		<cfelse>
			<cfset cacheTimeSpan = CreateTimeSpan(0, 0, 0, 0)>
		</cfif>

		<!--- START: 2013-10-02 NYB Case 435669 added --->
		<cfif all>
			<cfset maxrowsVar = -1>
		</cfif>
		<!--- END: 2013-10-02 NYB Case 435669 --->

		<!--- Get the locator definition for this country --->
		<!--- START 2011/10/20 NYB LHID7283 - rewrote entire query ---
		<cfquery name="getLocatorDefinition" datasource="#application.siteDataSource#" cachedwithin="#cacheTimeSpan#" maxrows="1">
			select	1 as SortOrder,ld1.*, 0 as countryDefExists
				from LocatorDef ld1 with (noLock)
				inner join Country c1 with (noLock) on ld1.countryGroupID = c1.countryID
					and c1.countryDescription='#defaultCountry#'
			union
			select	0 as SortOrder,ld.*,
				<cfif isDefined("arguments.countryID") and arguments.countryID is not 0><!--- WAB 2008/03/19 had to add this if in for the cases when arguments.countryid not passed in ---><!--- 2008/03/19 WAB added is not 0 --->
				case when ld.countryGroupID = #arguments.countryID# then 1 else 0 end
				<cfelse>
				0
				</cfif>
				as countryDefExists
			from LocatorDef ld with (noLock)
				inner join Country c with (noLock) on ld.countryGroupID = c.countryID
					and c.countryDescription!='#defaultCountry#'
				inner join CountryGroup cg with (noLock) on c.countryID = cg.countryGroupID
				<!--- NYB 2011-05-17 REL106: added if --->
				<cfif isDefined("arguments.countryID") and arguments.countryID is not 0><!--- 2008/03/19 WAB added is not 0 --->
					<cfif len(application.countryiso[arguments.countryID]) eq 0><!--- ie - is countrygroup --->
						and	 cg.CountryGroupID = #arguments.countryID#
					<cfelse>
						and	 cg.CountryMemberID = #arguments.countryID#
					</cfif>
				</cfif>
			 where ld.Live = 1
			 <cfif isDefined("arguments.locatorID")>
				and ld.LocatorID = #arguments.locatorID#
			</cfif>
			<!--- START: NYB 2009-03-27 Demo Locator - Removed: and GCC deleted locatorID = 1 from the Lenovo database to stop it causing a problem. Please consult the original developer in cases like this when you can to validate your assumptions about the validity of the change
				and locatorID > 1 <!--- NJH 2007/12/18 HACK!!! this is so that Belgium doesn't pick up the old locator def when using V2... --->
			<---   END: NYB 2009-03-27 Demo Locator --->
			 order by SortOrder asc,countryDefExists desc, locatorid asc
		</cfquery>
		--- 2011/10/20 NYB LHID7283 - rewrote entire query --->
		<!--- 2013-03-13 NYB Case 434064 made changes to query --->
		<cfquery name="getLocatorDefinition" datasource="#application.siteDataSource#"  maxrows=#maxrowsVar#>  <!--- 2013-10-02 NYB Case 435669 replaced maxrows="1" with #maxrowsVar# --->
			<!--- 2013-10-02 NYB Case 435669 replaced isDefined with StructKeyExists: --->
			select <cfif not isDefined("arguments.locatorID")>cs.countryid<cfelse><cfif structkeyexists(arguments,"countryID")>#arguments.countryID#<cfelse>0</cfif></cfif> as countryid,* from LocatorDef l with (noLock)
			<cfif not isDefined("arguments.locatorID")>
			inner join countryscope cs with (noLock)
				on l.locatorid=cs.entityid and cs.entity='LOCATORDEF'
			</cfif>
			where l.Live = 1
					and l.active = 1
			<cfif isDefined("arguments.countryID") and arguments.countryID is not 0 and not isDefined("arguments.locatorID")>
				and cs.countryid = #arguments.countryID#
			</cfif>
			<!--- START: 2014-03-05 NYB Case 438911 added --->
			<cfif arguments.limitByCountryRights>
				and cs.countryid in (#request.relayCurrentUser.countryList#)
			</cfif>
			<!--- END: 2014-03-05 NYB Case 438911 added --->
			<cfif isDefined("arguments.locatorID")>
				and l.LocatorID = #arguments.locatorID#
			</cfif>
		</cfquery>
		<!--- END 2011/10/20 NYB LHID7283 - rewrote entire query--->

		<cfreturn getLocatorDefinition>
	</cffunction>


	<cffunction access="public" name="GetResellerLocations" hint="Find reseller locations" return="query">
		<cfargument name="numResults" type="numeric" required="yes" default=5>
		<cfargument name="latitude" type="numeric" required="no">
		<cfargument name="longitude" type="numeric" required="no">
		<cfargument name="partnerFlag" type="numeric" required="no">
		<cfargument name="filterFlagList" type="string" required="no" default="">
		<cfargument name="countryGroupID" type="string" required="yes">
		<cfargument name="selectionOrder" type="numeric" required="No">
		<cfargument name="siteName" type="string" required="No">
		<cfargument name="orgApprovalFlagTextID" type="string" required="yes">
		<cfargument name="requiredFlags" type="string" default=""> <!--- NYB 2008-12-09 Lenovo Bug 1384 - added --->
		<cfargument name="resultRadius" type="string" required="no" default=""> <!--- p-lex039 SSS 2009-11-30 --->
		<cfargument name="distancemeasure" type="string" required="no" default="miles"> <!--- p-lex039 SSS 2009-11-30 --->

		<cfscript>
			var getResellerLocations = "";
			var orgFlagList = "";
			var locFlagList = "";
			var orgRqdFlagList = "";
			var locRqdFlagList = "";
			var getOrgFlags = "";
			var getLocFlags = "";
			var getResellerLocationsWithSiteName = "";
			var getAllResellerLocations = "";
		</cfscript>

		<cfif arguments.filterFlagList neq "">
			<cfquery name="getOrgFlags" datasource="#application.siteDataSource#">
				select flagID from vOrgFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.filterFlagList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
			<cfset orgFlagList = valueList(getOrgFlags.flagID)>
			<cfquery name="getLocFlags" datasource="#application.siteDataSource#">
				select flagID from vLocFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.filterFlagList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
			<cfset locFlagList = valueList(getLocFlags.flagID)>
		</cfif>

		<!--- NYB 2008-12-09 Lenovo Bug 1384 - added --->
		<cfif arguments.requiredFlags neq "">
			<cfquery name="getOrgFlags" datasource="#application.siteDataSource#">
				select flagID from vOrgFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.requiredFlags#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
			<cfset orgRqdFlagList = valueList(getOrgFlags.flagID)>
			<cfquery name="getLocFlags" datasource="#application.siteDataSource#">
				select flagID from vLocFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.requiredFlags#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>
			<cfset locRqdFlagList = valueList(getLocFlags.flagID)>
		</cfif>
		<!--- <- Lenovo Bug 1384 --->

		<!--- NYB 2008-12-09 Lenovo Bug 1384 - added ",@OrgRequiredList='#orgRqdFlagList#',@LocRequiredList='#locRqdFlagList#'" to query: --->
		<cfquery name="getResellerLocations" datasource="#application.siteDataSource#">
			exec sp_GetResellerLocations @NumResults='#arguments.numResults#', @Latitude='#arguments.latitude#', @Longitude='#arguments.longitude#', @OrgFilterList =  <cf_queryparam value="#orgFlagList#" CFSQLTYPE="CF_SQL_VARCHAR" > , @LocFilterList =  <cf_queryparam value="#locFlagList#" CFSQLTYPE="CF_SQL_VARCHAR" > , @CountryIDs =  <cf_queryparam value="#arguments.countryGroupID#" CFSQLTYPE="CF_SQL_VARCHAR" > , @SelectionOrder=#arguments.selectionOrder#, @orgApprovalFlagTextID = "#listQualify(arguments.orgApprovalFlagTextID,"'")#",@OrgRequiredList =  <cf_queryparam value="#orgRqdFlagList#" CFSQLTYPE="CF_SQL_VARCHAR" > ,@LocRequiredList =  <cf_queryparam value="#locRqdFlagList#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif isDefined("arguments.siteName") and arguments.siteName neq "">
			<!--- filter results by sitename --->
			<cfquery name="getResellerLocationsWithSiteName" dbType="query" maxrows="#arguments.numResults#">
				select * from getResellerLocations where lower(siteName) like '#lcase(arguments.siteName)#%'
			</cfquery>
			<cfset getResellerLocations = getResellerLocationsWithSiteName>
		<cfelse>
			<!--- show all the results --->
			<cfquery name="getAllResellerLocations" dbType="query" maxrows="#arguments.numResults#">
				select * from getResellerLocations
			</cfquery>
			<cfset getResellerLocations = getAllResellerLocations>
		</cfif>
		<!--- p-lex039 SSS 2009-11-30 --->
		<cfif arguments.resultRadius neq "">
		<!--- filter by distance here --->
			<cfif distancemeasure EQ "miles">
				<cfquery name="getResellerLocations" dbType="query" maxrows="#arguments.numResults#">
					select * from getResellerLocations where dist  < #arguments.resultRadius#
				</cfquery>
			<cfelse>
				<cfquery name="getResellerLocations" dbType="query" maxrows="#arguments.numResults#">
					select * from getResellerLocations where dist * 1.609344  < #arguments.resultRadius#
				</cfquery>
			</cfif>
		</cfif>

		<cfreturn getResellerLocations>
	</cffunction>


	<cffunction access="public" name="createGeoDataSearchQueryFromFormVariables" returns="struct">
		<cfset var replaceValues = "">
		<cfset var replaceValueStatement_SQL = "">
		<cfset var replaceStatement_SQL = "">
		<cfset var province = "">
		<cfset var town = "">
		<cfset var siteName = "">
		<cfset var getCountriesWithProvinces = "">
		<cfset var result = structNew()>

		<cfset result.hasGeoDataCriteria = false>
		<cfset result.hasOrganisationCriteria = false>
		<cfset result.hasLocationCriteria = false>
		<cfset result.whereStatement_sql = "">
		<cfset result.additionaljoins_sql = "">

		<cfset replaceValueStatement_SQL = replaceSpecialCharactersForDBQuery().replaceValueStatement_SQL>
		<cfset replaceStatement_SQL = replaceSpecialCharactersForDBQuery().replaceStatement_SQL>

		<cfquery name="getCountriesWithProvinces" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select distinct countryID from province with (noLock)
		</cfquery>

		<cfif isDefined("form.frmSearchRegion") and form.frmSearchRegion neq "">
			<cfset province = replace(form.frmSearchRegion,"'","''","ALL")>
		</cfif>
		<cfif isDefined("form.frmSearchTown") and form.frmSearchTown neq "">
			<cfset town = replace(form.frmSearchTown,"'","''","ALL")>
		</cfif>
		<!--- NJH 2009/09/23 Bug Fix Lenovo Issue 1182 - dealing with apostrophes in the sitename --->
		<cfif isDefined("form.frmSearchSiteName") and form.frmSearchSiteName neq "">
			<cfset siteName = replace(form.frmSearchSiteName,"'","''","ALL")>
		</cfif>

		<!--- NJH 2008/08/08 we were filtering the province/town drop down to show only those towns that had
		locations flagged with the flags selected. This was slowing down the locator considerably and it was decided to remove it from the query...
		<cfset flagQueryDetail = createFlagSearchQueryFromFormVariables()> --->

		<cfsavecontent variable = "result.whereStatement_sql">
			<cfoutput>
				<cfif isDefined("town") and town neq "">
					<!--- <cfset charReplacedTown = replaceSpecialCharacters(searchString=form.frmSearchTown)> --->
					<!--- NJH 2008/08/05 CR-LEN547 use plainPlace field that has all special characters already removed --->
					<!--- and (#replaceStatement_SQL#d.place#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#town#'#replace(replaceValueStatement_SQL,"**","'","ALL")#) --->
					and (d.plainPlace = #replaceStatement_SQL#N'#town#'#replace(replaceValueStatement_SQL,"**","'","ALL")#)
					<cfset result.hasGeoDataCriteria = true>
				</cfif>
				<cfif isDefined("province") and province neq "">
					<!--- <cfset charReplacedRegion = replaceSpecialCharacters(searchString=form.frmSearchRegion)> --->
					<!--- NJH 2008/08/05 CR-LEN547 use plainProvince field that has all special characters already removed --->
					<!--- and (#replaceStatement_SQL#d.province#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#province#'#replace(replaceValueStatement_SQL,"**","'","ALL")#) --->
					and (d.plainProvince =  <cf_queryparam value="#province#" CFSQLTYPE="CF_SQL_VARCHAR" > )
					<cfset result.hasGeoDataCriteria = true>
				</cfif>
				<cfif isDefined("form.frmSearchPostalCode") and form.frmSearchPostalCode neq "">
					<!--- <cfset charReplacedPostalCode = replaceSpecialCharacters(searchString=frmSearchPostalCode)> --->
					and d.postalCode  like  <cf_queryparam value="#form.frmSearchPostalCode#%" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfset result.hasGeoDataCriteria = true>
				</cfif>

				<!--- NJH 2009/02/23 Bug Fix Lenovo Issue 1182 - replaced form.frmSearchSiteName to use sitename --->
				<cfif isDefined("siteName") and siteName neq "">
					<!--- <cfset charReplacedSiteName = replaceSpecialCharacters(searchString=form.frmSearchSiteName)> --->
					and  (#replaceStatement_SQL#sitename#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#siteName#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						or #replaceStatement_SQL#organisationName#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#siteName#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						or #replaceStatement_SQL#aka#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#siteName#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						)
					<cfset result.hasOrganisationCriteria = true>
					<cfset result.hasLocationCriteria = true>
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<!--- creating the joins needed --->

		<cfsavecontent variable = "result.additionaljoins_sql">
			<cfoutput>
				<!--- <cfif result.hasOrganisationCriteria is true or flagQueryDetail.hasFlagOrganisationCriteria>
				inner join organisation o on o.countryID = c.countryID
				<!--- if the geodata doesn't have org criteria but the flags require an organisation table--->
				<cfelseif not result.hasOrganisationCriteria is true and (flagQueryDetail.hasFlagOrganisationCriteria or flagQueryDetail.hasFlagLocationCriteria)>
					inner join organisation o on o.countryID = c.countryID
				</cfif>
				<cfif result.hasLocationCriteria is true or flagQueryDetail.hasFlagLocationCriteria>
					inner join location l on o.organisationID = l.organisationID
					inner join booleanFlagData bfd on l.locationID = bfd.entityID and bfd.flagID = 1895
				</cfif> --->

				<cfif result.hasLocationCriteria is true <!--- or flagQueryDetail.hasFlagLocationCriteria --->>
					inner join location l with (noLock) on l.countryID = c.countryID and (d.place = l.address4 or d.place = l.address3)
					<cfif listFind(valueList(getCountriesWithProvinces.countryID),session.locatorCountryID)>
						inner join province p with (noLock) on p.countryid = c.countryid and p.name = d.province and p.abbreviation = l.address5
					<cfelse>
						and d.province = l.address5
					</cfif>
					inner join booleanFlagData bfd with (noLock) on l.locationID = bfd.entityID and bfd.flagID =  <cf_queryparam value="#request.locatorPartnerFlag#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- if the geodata doesn't have org criteria but the flags require an organisation table--->
				<!--- <cfelseif not result.hasLocationCriteria is true and (flagQueryDetail.hasFlagOrganisationCriteria or flagQueryDetail.hasFlagLocationCriteria)>
					inner join location l with (noLock) on l.countryID = c.countryID and (d.place = l.address4 or d.place = l.address3)
					<cfif listFind(valueList(getCountriesWithProvinces.countryID),session.locatorCountryID)>
						inner join province p with (noLock) on p.countryid = c.countryid and p.name = d.province and p.abbreviation = l.address5
					<cfelse>
						and d.province = l.address5
					</cfif>
					inner join booleanFlagData bfd with (noLock) on l.locationID = bfd.entityID and bfd.flagID = #request.locatorPartnerFlag#
				 ---></cfif>
				<cfif result.hasOrganisationCriteria is true <!--- or flagQueryDetail.hasFlagOrganisationCriteria --->>
				inner join organisation o with (noLock) on o.organisationID = l.organisationID
				</cfif>

			</cfoutput>
		</cfsavecontent>

		<cfset result.additionaljoins_sql = result.additionaljoins_sql <!--- & flagQueryDetail.flagAdditionaljoins --->>

		<cfreturn result>

	</cffunction>


	<cffunction access="public" name="createFlagSearchQueryFromFormVariables" returns="struct">

		<cfset var partnerTypeFlagIDs = "">
		<cfset var flagResult = structNew()>
		<cfset flagResult.hasFlagOrganisationCriteria = false>
		<cfset flagResult.hasFlagLocationCriteria = false>
		<cfset flagResult.hasFlagPersonCriteria = false>
		<cfset flagResult.flagAdditionaljoins_sql = "">



		<cfif isDefined("form.frmPartnerType") and form.frmPartnerType neq "">
			<cfset partnerTypeFlagIDs = listAppend(partnerTypeFlagIDs,form.frmPartnerType)>
		</cfif>

		<!--- <cfif isDefined("frmPartnerTypeFG") and frmPartnerTypeFG neq "">
			<cfquery name="getFlagIDsFromFlagGroup" datasource="#application.siteDataSource#">
				select flagID from flag where flagGroupID in (#frmPartnerTypeFG#)
			</cfquery>
			<cfset partnerTypeFlagIDs = listAppend(partnerTypeFlagIDs,valueList(getFlagIDsFromFlagGroup.flagID))>
		</cfif> --->

		<cfif partnerTypeFlagIDs neq "">
			<cfsavecontent variable = "flagResult.flagAdditionaljoins_sql">
				<cfoutput>
					<cfloop list="#partnerTypeFlagIDs#" index="flagID">
						<cfset flagStructure = application.com.flag.getFlagStructure(flagID)>
						inner join #flagStructure.flagType.dataTableFullName# #flagStructure.flagType.dataTable##flagID# with (noLock)
							on #flagStructure.flagType.dataTable##flagID#.entityID =
							<cfswitch expression="#flagStructure.entityTypeID#">
								<cfcase value="0">
									p.personID
									<cfset flagResult.hasFlagPersonCriteria = true>
								</cfcase>
								<cfcase value="1">
									l.locationID
									<cfset flagResult.hasFlagLocationCriteria = true>
								</cfcase>
								<cfcase value="2">
									o.organisationID
									<cfset flagResult.hasFlagOrganisationCriteria = true>
								</cfcase>
							</cfswitch>
							and  #flagStructure.flagType.dataTable##flagID#.flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfloop>
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<cfif isDefined("form.frmPartnerTypeFG") and form.frmPartnerTypeFG neq "">
			<cfsavecontent variable = "flagResult.flagGroupAdditionaljoins_sql">
				<cfoutput>
					<cfloop list="#form.frmPartnerTypeFG#" index="flagGroupID">
						<cfset flagGroupStructure = application.com.flag.getFlagGroupStructure(flagGroupID)>
						inner join #flagGroupStructure.flagType.dataTableFullName# #flagGroupStructure.flagType.dataTable##flagGroupID#FG with (noLock)
							on #flagGroupStructure.flagType.dataTable##flagGroupID#FG.entityID =
							<cfswitch expression="#flagGroupStructure.entityTypeID#">
								<cfcase value="0">
									p.personID
									<cfset flagResult.hasFlagPersonCriteria = true>
								</cfcase>
								<cfcase value="1">
									l.locationID
									<cfset flagResult.hasFlagLocationCriteria = true>
								</cfcase>
								<cfcase value="2">
									o.organisationID
									<cfset flagResult.hasFlagOrganisationCriteria = true>
								</cfcase>
							</cfswitch>
							and  #flagGroupStructure.flagType.dataTable##flagGroupID#FG.flagID in --(select flagID from flag with (noLock) where flagGroupID = #flagGroupID#)
							<cfset flagGroupQry =application.com.flag.getFlagGroupFlags(FlagGroupId=flagGroupID)>
							(#valueList(flagGroupQry.flagID)#)
					</cfloop>
				</cfoutput>
			</cfsavecontent>

			<cfset flagResult.flagAdditionaljoins_sql = flagResult.flagAdditionaljoins_sql & flagResult.flagGroupAdditionaljoins_sql>
		</cfif>

		<cfreturn flagResult>

	</cffunction>



	<cffunction name="replaceSpecialCharacters" access="public" returntype="string" hint="Replaces special characters in a string">
		<cfargument name="searchString" required="true" type="string">

		<cfscript>
			var getReplacementStrings = "";
			var replacedString = arguments.searchString;
		</cfscript>

		<cfquery name="getReplacementStrings" dataSource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select stringToReplace, replacementString from matchStringReplacement with (noLock) where isNull(matchType,'address') ='address'
		</cfquery>

		<cfloop query="getReplacementStrings">
			<cfset replacedString = replaceNoCase(replacedString,stringToReplace,replacementString,"ALL")>
		</cfloop>

		<cfreturn replacedString>
	</cffunction>


	<!--- NJH 2016/11/23  - why do we have this function?? This seems to be the exact one that we've got in dbtools/matching!!!! When someone has time, please refactor! --->
	<cffunction name="replaceSpecialCharactersForDBQuery" access="public" hint="Builds a replace statement for a db query for special characters">

		<!--- cfset var replaceStatement_SQL = "">
		<cfset var replaceValueStatement_SQL = "">
		<cfset var returnVar = structNew()>
		<cfset var getReplaceString = "">
		<cfset var localReplaceChar= "">

		<cfquery name="getReplaceString" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 15, 0)#">
			select distinct stringToReplace, replacementString from matchStringReplacement with (noLock) where isNull(matchType,'address') ='address'
		</cfquery>

		<cfloop query="getReplaceString">
			<cfset localReplaceChar = getReplaceString.stringToReplace>
			<cfset replaceStatement_SQL = replaceStatement_SQL & "REPLACE(">

			<!--- GCC Work arround to put trailing space on words but not special chars which we look for anywhere in the string
			We only want to find and replace "Gawain Sc" to "Gawain", not "Gawain School" to "Gawainhool"
			 --->
			<cfif len(localReplaceChar) gt 1>
				<cfset localReplaceChar = localReplaceChar & " ">
			</cfif>
			<cfset replaceValueStatement_SQL = replaceValueStatement_SQL & ",**" & localReplaceChar & "**,**" & replacementString &"**)">
		</cfloop>

		<cfset returnVar.replaceValueStatement_SQL = replaceValueStatement_SQL>
		<cfset returnVar.replaceStatement_SQL = replaceStatement_SQL>

		<cfreturn returnVar> --->
		<cfset var result = application.com.matching.getMatchFieldReplaceStrings(matchField="matchAddress")>
		<cfset result.replaceValueStatement_SQL = result.replaceValueStatement>
		<cfset result.replaceStatement_SQL = result.replaceStatement>
		<cfreturn result>

	</cffunction>



	<cffunction name="getResellersByTextSearch" access="public" returnType="query">
		<cfargument name="regionSearch" type="string" default="">
		<cfargument name="townSearch" type="string" default="">
		<cfargument name="siteNameSearch" type="string" default="">
		<cfargument name="locatorPartnerFlag" type="numeric" required="false">
		<cfargument name="partnerTypeFlagIDList" type="string" required="false">
		<cfargument name="orgApprovalFlagTextID" type="string" required="yes">
		<cfargument name="RequiredFlags" type="string" default=""><!--- comma separated list of integers --->

		<cfscript>
			var getResellerLocations="";
			var flagQueryDetail = createFlagSearchQueryFromFormVariables();
			var province = "";
			var town = "";
		</cfscript>

		<cfset var replaceValueStatement_SQL = replaceSpecialCharactersForDBQuery().replaceValueStatement_SQL>
		<cfset var replaceStatement_SQL = replaceSpecialCharactersForDBQuery().replaceStatement_SQL>
		<cfset var approvalflagStruct = structnew()><!--- 2013-10-02 NYB Case 435669 added --->

		<cfset flagQueryDetail.flagAdditionaljoins_sql = replace(flagQueryDetail.flagAdditionaljoins_sql,"o.organisationID", "organisationID","ALL")>
		<cfset flagQueryDetail.flagAdditionaljoins_sql = replace(flagQueryDetail.flagAdditionaljoins_sql,"l.locationID", "locationID","ALL")>
		<cfset flagQueryDetail.flagAdditionaljoins_sql = replace(flagQueryDetail.flagAdditionaljoins_sql,"p.personID", "personID","ALL")>

		<cfif arguments.regionSearch neq "">
			<cfset province = replace(arguments.regionSearch,"'","''","ALL")>
		</cfif>
		<cfif arguments.townSearch neq "">
			<cfset town = replace(arguments.townSearch,"'","''","ALL")>
		</cfif>

		<cfquery name="getOrgFlags" datasource="#application.siteDataSource#">
			select flagID from vOrgFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.requiredFlags#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<cfset orgRqdFlagList = valueList(getOrgFlags.flagID)>
		<cfquery name="getLocFlags" datasource="#application.siteDataSource#">
			select flagID from vLocFlag with (noLock) where flagID  in ( <cf_queryparam value="#arguments.requiredFlags#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		<cfset locRqdFlagList = valueList(getLocFlags.flagID)>

		<!--- 2013-10-02 NYB Case 435669 added: --->
		<cfset approvalflagStruct = application.com.flag.getFlagStructure(flagID=orgApprovalFlagTextID)>

		<!--- finding the locations --->
		<cfquery name="getResellerLocations" datasource="#application.siteDataSource#">
			select * from vLocatorResellers with (noLock)
				<!--- START: 2013-10-02 NYB Case 435669 - replaced ---
				inner join booleanFlagData bfd with (noLock)
					on bfd.entityID = organisationID
				inner join flag orgApp with (noLock)
					on orgApp.flagID = bfd.flagID and orgApp.flagTextID =  <cf_queryparam value="#arguments.orgApprovalFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >  <!--- 2009/11/20 SSS p-Lex039 --->
				!--- 2013-10-02 NYB Case 435669 with: --->
				inner join #approvalflagStruct.FLAGTYPE.DATATABLEFULLNAME# d with(nolock) on vLocatorResellers.#approvalflagStruct.FLAGGROUP.ENTITYTYPE.UNIQUEKEY# = d.entityID and d.flagid =  <cf_queryparam value="#approvalflagStruct.FlagID#" CFSQLTYPE="cf_sql_integer" >
				<!--- END: 2013-10-02 NYB Case 435669 --->
					#flagQueryDetail.flagAdditionaljoins_sql#
				<cfloop index="i" list="#orgRqdFlagList#">
					inner join booleanFlagData flag_#i# with (noLock)
						on flag_#i#.entityID = organisationID and flag_#i#.flagid=#flag_#i##
				</cfloop>
				<cfloop index="i" list="#locRqdFlagList#">
					inner join booleanFlagData flag_#i# with (noLock)
						on flag_#i#.entityID = locationid and flag_#i#.flagid=#flag_#i##
				</cfloop>
			where countryID =  <cf_queryparam value="#session.locatorCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and organisationID not in (select organisationID from vOrgsToSupressFromReports with (noLock))
				and locatorFlagID = #arguments.locatorPartnerFlag#
				<!--- #flagQueryDetail.flagWhereStatement# --->
				<cfif isDefined("province") and province neq "">
					<!--- <cfset charReplacedRegion = replaceSpecialCharacters(searchString=arguments.regionSearch)> --->
					and (#replaceStatement_SQL#address3#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#province#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						 or #replaceStatement_SQL#address4#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#province#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						 or #replaceStatement_SQL#address5#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#province#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						 or #replaceStatement_SQL#provinceName#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#province#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						)
				</cfif>
				<cfif isDefined("town") and town neq "">
					and (#replaceStatement_SQL#address2#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#town#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						 or #replaceStatement_SQL#address3#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#town#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						 or #replaceStatement_SQL#address4#replace(replaceValueStatement_SQL,"**","'","ALL")# = #replaceStatement_SQL#N'#town#'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						)
				</cfif>
				<cfif arguments.siteNameSearch neq "">
					and (#replaceStatement_SQL#sitename#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.siteNameSearch#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						or #replaceStatement_SQL#organisationName#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.siteNameSearch#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						or #replaceStatement_SQL#aka#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.siteNameSearch#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						)
				</cfif>
			<cfif arguments.regionSearch neq "">
			order by address4, partnerLevelOrder, siteName
			<cfelseif arguments.townSearch neq "">
			order by partnerLevelOrder, siteName
			</cfif>
		</cfquery>

		<cfreturn getResellerLocations>

	</cffunction>

	<cffunction name="GetProductsOfInterest" access="public" Hint="Gets products from the database">
		<cfargument name="countryid" type="numeric" require="true">
		<cfquery name="GetProducts" datasource="#application.siteDataSource#">
			select * from pcmProduct p with(nolock)
			inner join country c with(nolock) on p.countrycode=c.isocode and c.countryid=#arguments.countryid#
			and p.languagecode in
			(select top 1 p.languagecode from pcmProduct p with(nolock)
			inner join country c with(nolock) on p.countrycode=c.isocode and c.countryid=#arguments.countryid#
			order by productid)
			order by title
		</cfquery>
		<cfreturn GetProducts>
	</cffunction>

	<!--- NYB REL106: following functions are no longer used (for locatorV3) --->

	<!--- NYB not used anywhere, but could be: --->
	<cffunction access="public" name="SetActivityRecord" hint="Inserts a locator search audit record" return="numeric">
		<cfargument name="locatorID" type="numeric" required="yes">
		<cfargument name="criteriaString" type="string" required="yes">
		<cfargument name="islive" type="boolean" default="true" required="yes">
		<cfargument name="referrer" type="string" required="yes">
		<cfargument name="searchResult" type="string" required="yes">
		<cfargument name="hitID" type="numeric" required="yes">

		<cfset var setUserRecord = "">

		<cfquery name= "setUserRecord" datasource="#application.siteDataSource#">
			INSERT INTO
				LocatorSearchAudit
				(LocatorDef, SearchCriteriaString, LocatorLive, Referer, SearchTime, results, hitid)
			VALUES
				(<cf_queryparam value="#arguments.locatorID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<!--- 2013/02/28	YMA	2013 Roadmap 135 Company Locator Page --->
				<cfif #arguments.criteriaString# eq ""><cf_queryparam value="null" CFSQLTYPE="CF_SQL_VARCHAR" ><cfelse><cf_queryparam value="#arguments.criteriaString#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>
				<!--- <cf_queryparam value="#arguments.criteriaString#" CFSQLTYPE="CF_SQL_VARCHAR" >,  --->
				<cf_queryparam value="#arguments.islive#" CFSQLTYPE="CF_SQL_bit" >,
				<cf_queryparam value="#arguments.referrer#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#CreateODBCDateTime(now())#" CFSQLTYPE="cf_sql_timestamp" >,
				<cf_queryparam value="#arguments.searchResult#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#arguments.hitID#" CFSQLTYPE="CF_SQL_INTEGER" >)

			select scope_identity() as maxSearchID
		</cfquery>

		<cfreturn setUserRecord.maxSearchID>

	</cffunction>


	<cffunction access="public" name="findUserLocation" hint="" return="struct">
		<cfargument name="searchCriteriaType" type="string" default="town">
		<cfargument name="searchCriteria" type="string" default="abcdefghi">
		<cfargument name="countryGroupID" type="numeric" default=9>
		<cfargument name="locatorID" type="numeric" required="yes">

		<cfset var criteriaString = "">
		<cfset var breakLoop = false>
		<cfset var criteriaTypesSearched = "">
		<cfset var currentSearchCriteriaType = arguments.searchCriteriaType>
		<cfset var currentSearchCriteria = arguments.searchCriteria>
		<cfset var counter = 0>
		<cfset var town="">
		<cfset var county="">
		<cfset var postalCode="">
		<cfset var referrer="">
		<cfset var locatorDef="">
		<cfset var getLatLong="">
		<cfset var searchid="">
		<cfset var resultStruct="">

		<cfset arguments.searchCriteria = trim(arguments.searchCriteria)> <!--- WAB 2007-07-02 added trim --->
		<cfset criteriaString = "#searchCriteriaType# = #searchCriteria#">

		<!--- <cfquery name="getCountryMembers" datasource="#application.siteDataSource#">
			select countryMemberID from countryGroup where countryGroupID = #arguments.countryGroupID#
		</cfquery> --->

		<cfif parameterexists(HTTP_REFERER)>
			<cfset referrer=HTTP_REFERER>
		<cfelse>
			<cfset referrer="Not Known">
		</cfif>

		<cfset locatorDef = application.com.relayLocator.getLocatorDefinition(locatorID=arguments.locatorID)>

		<cfswitch expression="#arguments.searchCriteriaType#">
			<cfcase value="town">
				<cfset town = arguments.searchCriteria>
			</cfcase>
			<cfcase value="county">
				<cfset county = arguments.searchCriteria>
			</cfcase>
			<cfcase value="postalCode">
				<cfset postalCode = arguments.searchCriteria>
			</cfcase>
			<cfdefaultcase>
				<cfset town = arguments.searchCriteria>
			</cfdefaultcase>
		</cfswitch>

		<!--- loop through town,county and postal code until we've either got results or until we've searched all three location types --->
		<cfloop condition = "not breakLoop">

			<cfif criteriaTypesSearched neq "">

				<!--- If we've just searched the town field --->
				<cfif listLast(criteriaTypesSearched) eq "town">
					<cfset town = "">
					<cfif not listFind(criteriaTypesSearched,"county") and locatorDef.useCounty>
						<cfset county = arguments.searchCriteria>
						<cfset currentSearchCriteriaType = "county">
					<cfelseif not listFind(criteriaTypesSearched,"postalCode") and locatorDef.useZip>
						<cfset postalCode = arguments.searchCriteria>
						<cfset currentSearchCriteriaType = "postalCode">
					</cfif>

				<!--- if we've just searched the county field --->
				<cfelseif listLast(criteriaTypesSearched) eq "county">
					<cfset county = "">
					<cfif not listFind(criteriaTypesSearched,"town") and locatorDef.useTown>
						<cfset town = arguments.searchCriteria>
						<cfset currentSearchCriteriaType = "town">
					<cfelseif not listFind(criteriaTypesSearched,"postalCode") and locatorDef.useZip>
						<cfset postalCode = arguments.searchCriteria>
						<cfset currentSearchCriteriaType = "postalCode">
					</cfif>

				<!--- if we've just searched the postal code --->
				<cfelseif listLast(criteriaTypesSearched) eq "postalCode">
					<!--- if we've entered a postal code, trim the criteria by 1 and search for it. Keep looping until we've found something --->
					<cfif len(currentSearchCriteria) gt 1>
						<cfset currentSearchCriteria = left(currentSearchCriteria,(len(currentSearchCriteria)-1))>
						<cfset postalCode = currentSearchCriteria>
					<cfelseif len(currentSearchCriteria) eq 1>
						<cfset currentSearchCriteria = "">
						<cfset postalCode = currentSearchCriteria>
					<cfelse>
						<cfset postalCode = "">
						<cfif not listFind(criteriaTypesSearched,"town") and locatorDef.useTown>
							<cfset town = arguments.searchCriteria>
							<cfset currentSearchCriteriaType = "town">
						<cfelseif not listFind(criteriaTypesSearched,"county") and locatorDef.useCounty>
							<cfset county = arguments.searchCriteria>
							<cfset currentSearchCriteriaType = "county">
						</cfif>
					</cfif>
				</cfif>
			</cfif>

			<!---
				all fields will be null if we've searched through all of them
				if this is the case, don't search again.
			 --->

			<!--- <cfif not (town eq "" and county eq "" and postalCode eq "")> --->
				<cfquery name="getLatLong" datasource="#application.siteDataSource#">
					exec sp_getLatLong '#postalCode#','#town#','#county#','#arguments.countryGroupID#'
				</cfquery>

				<cfset criteriaString = "#currentSearchCriteriaType# = #currentSearchCriteria#">
				<cfif not getLatLong.recordcount>
					<cfset searchid = application.com.relayLocator.setActivityRecord(locatorID,criteriaString,locatorDef.live,referrer,"Bad Location",hitID)>
					<!--- append the last search to the list of searches we've performed. --->
					<cfset criteriaTypesSearched = listAppend(criteriaTypesSearched,currentSearchCriteriaType)>
					<!--- if we've reached the end of our searching and we still haven't found anything--->
					<cfif len(currentSearchCriteria) eq 0>
						<cfset breakLoop = true>
					</cfif>

				<cfelse>
					<!--- we've found a location. This exits the loop --->
					<cfset breakLoop = true>
					<cfif getLatLong.recordCount eq 1>
						<cfset searchid = application.com.relayLocator.setActivityRecord(locatorID,criteriaString,locatorDef.live,referrer,"Yes Result",hitID)>
					<cfelse>
						<cfset searchid = application.com.relayLocator.setActivityRecord(locatorID,criteriaString,locatorDef.live,referrer,"Yes Result Narrowed",hitID)>
					</cfif>
				</cfif>
			<!--- </cfif> --->
		</cfloop>

		<cfscript>
			resultStruct = StructNew();
			structInsert(resultStruct,"latLongQuery",getLatLong);
			structInsert(resultStruct,"searchID",searchID);
		</cfscript>
		<cfreturn resultStruct>
	</cffunction>


	<cffunction access="public" name="getLocatorRegions" hint="Returns locator regions">
		<cfargument name="locatorID" type="numeric" required="yes">

		<cfset var getLocatorRegions = "">

		<cfquery name="getLocatorRegions" datasource="#application.siteDataSource#">
			select RegionText,RegionMatchText from LocatorRegionSearchList with (noLock) where locatorID = <cf_queryparam value="#arguments.locatorID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfreturn getLocatorRegions>
	</cffunction>



	<cffunction name="EditGeodataRecord" access="public" Hint="SSS 27-04-2009 Saves a Geodata record that has been editted by someone">
		<cfargument name="frmGeodataID" type="numeric" required="Yes">
		<cfargument name="frmCity" type="string" default="">
		<cfargument name="frmProvince" type="string" default="">
		<cfargument name="frmLatitude" type="numeric" required="Yes">
		<cfargument name="frmLongitude" type="numeric" required="Yes">
		<cfargument name="frmPostcode" type="string" default="">

		<cfset var replaceStatement_SQL = replaceSpecialCharactersForDBQuery().replaceStatement_SQL>
		<cfset var replaceValueStatement_SQL = replaceSpecialCharactersForDBQuery().replaceValueStatement_SQL>
		<!--- plainProvince = '#replaceStatement_SQL##arguments.frmProvince##replace(replaceValueStatement_SQL,"**","'","ALL")#' --->
		<!---start p-len008 SSS 2009-06-10 Geodata kanji compitable --->
		<cfquery name= "UpdateGeodata" datasource="#application.siteDataSource#">
			update geoData
			set 	place =  <cf_queryparam value="#arguments.frmCity#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					Province =  <cf_queryparam value="#arguments.frmProvince#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					DecimalLatitude = <cf_queryparam value="#frmLatitude#" CFSQLTYPE="CF_SQL_NUMERIC" >,
					DecimalLongitude = <cf_queryparam value="#frmLongitude#" CFSQLTYPE="CF_SQL_NUMERIC" >,
					Postalcode =  <cf_queryparam value="#frmPostcode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					plainPlace = #replaceStatement_SQL#<cf_queryparam value="#arguments.frmCity#" CFSQLTYPE="CF_SQL_VARCHAR" >#replace(replaceValueStatement_SQL,"**","'","ALL")#,
					plainProvince = #replaceStatement_SQL#<cf_queryparam value="#arguments.frmProvince#" CFSQLTYPE="CF_SQL_VARCHAR" >#replace(replaceValueStatement_SQL,"**","'","ALL")#
			where GeodataID = <cf_queryparam value="#frmGeodataID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<!---End p-len008 SSS 2009-06-10 Geodata kanji compitable --->
	</cffunction>

	<cffunction name="InsertGeodataRecord" access="public" Hint="SSS 27-04-2009 checks to see if the record is already there if not inserts it">
		<cfargument name="frmCity" type="string" default="">
		<cfargument name="frmProvince" type="string" required="Yes">
		<cfargument name="frmLatitude" type="numeric" required="Yes">
		<cfargument name="frmLongitude" type="numeric" required="yes">
		<cfargument name="frmPostcode" type="string" default="">
		<!--- <cfargument name="frmPlainProvince" type="string" required="Yes">
		<cfargument name="frmPlainCity" type="string" required="Yes"> --->
		<cfargument name="frmisocode" type="string" required="Yes">

		<cfset var replaceStatement_SQL = replaceSpecialCharactersForDBQuery().replaceStatement_SQL>
		<cfset var replaceValueStatement_SQL = replaceSpecialCharactersForDBQuery().replaceValueStatement_SQL>
		<!---start p-len008 SSS 2009-06-10 Geodata kanji compitable --->
		<cfquery name="insertGeodata" datasource="#application.siteDataSource#">
			if not exists (select 1 from geodata where ISOCode =  <cf_queryparam value="#arguments.frmisocode#" CFSQLTYPE="CF_SQL_VARCHAR" >
												 and Place =  <cf_queryparam value="#arguments.frmCity#" CFSQLTYPE="CF_SQL_VARCHAR" >
												 and Province =  <cf_queryparam value="#arguments.frmProvince#" CFSQLTYPE="CF_SQL_VARCHAR" >
												 and DecimalLatitude = <cf_queryparam value="#arguments.frmLatitude#" CFSQLTYPE="CF_SQL_NUMERIC" >
												 and DecimalLongitude = <cf_queryparam value="#arguments.frmLongitude#" CFSQLTYPE="CF_SQL_NUMERIC" >
												 and Postalcode =  <cf_queryparam value="#arguments.frmPostcode#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			begin
				insert into geodata (geokey, ISOCode, Place, Province, DecimalLatitude, DecimalLongitude, Postalcode, plainPlace, plainProvince)
				values(1
					, <cf_queryparam value="#arguments.frmisocode#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, <cf_queryparam value="#arguments.frmCity#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, <cf_queryparam value="#arguments.frmProvince#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, <cf_queryparam value="#arguments.frmLatitude#" CFSQLTYPE="CF_SQL_NUMERIC" >
					, <cf_queryparam value="#arguments.frmLongitude#" CFSQLTYPE="CF_SQL_NUMERIC" >
					, <cf_queryparam value="#arguments.frmPostcode#" CFSQLTYPE="CF_SQL_VARCHAR" >
					, #replaceStatement_SQL#<cf_queryparam value="#arguments.frmCity#" CFSQLTYPE="CF_SQL_VARCHAR" >#replace(replaceValueStatement_SQL,"**","'","ALL")#
					, #replaceStatement_SQL#<cf_queryparam value="#arguments.frmProvince#" CFSQLTYPE="CF_SQL_VARCHAR" >#replace(replaceValueStatement_SQL,"**","'","ALL")#)
			end
		</cfquery>
		<!---End p-len008 SSS 2009-06-10 Geodata kanji compitable --->
	</cffunction>




	<cffunction access="public" name="getLocatorCountries" hint="Returns locator countries">
		<cfargument name="locatorID" type="numeric" required="no">
		<cfargument name="countryID" type="numeric" required="no">
		<cfargument name="cache" type="boolean" default="true">
		<cfargument name="showNullValue" type="boolean" default="false"><!--- 2014-03-28 NYB Case 439188 --->
		<cfargument name="nullText" type="string" default="phr_locator_PleaseSelectACountry"><!--- 2014-03-28 NYB Case 439188 --->

		<cfset var getLocatorCountries = "">

		<cfif cache>
			<cfset cacheTimeSpan = CreateTimeSpan(0, 0, 15, 0)>
		<cfelse>
			<cfset cacheTimeSpan = CreateTimeSpan(0, 0, 0, 0)>
		</cfif>

		<!--- START: 2014-03-28 NYB Case 439188 --->
		<cfif showNullValue>
			<cfset arguments.nullText = application.com.relayTranslations.translatePhrase(phrase=arguments.nullText)>
			<cfif arguments.nullText eq "locator_PleaseSelectACountry">
				<cfset arguments.nullText = "Please Select A Country">
			</cfif>
		</cfif>
		<!--- END: 2014-03-28 NYB Case 439188 --->

		<cfquery name="getLocatorCountries" datasource="#application.siteDataSource#" cachedwithin="#cacheTimeSpan#">
			<!--- 2014-03-28 NYB Case 439188 added line: --->
			<cfif showNullValue>select 0 as sortOrder,0 as countryID, <cfoutput>'#arguments.nullText#'</cfoutput> as country, '' as distanceMeasure union </cfif>
			<!--- 2014-03-28 NYB Case 439188 added sortorder: --->
			select distinct 1 as sortOrder,c.countryID, c.localCountryDescription as country, c.distanceMeasure
			from country c with (noLock)
			inner join countryscope cs with (noLock) on c.countryid=cs.countryid
				and entity='LocatorDef'
			<cfif isDefined("arguments.countryID") and arguments.countryID is not 0>
				and cs.countryid = #arguments.countryID#
			</cfif>
			<cfif isDefined("arguments.locatorID")>
				and cs.entityID = #arguments.locatorID#
			</cfif>
			<!--- 2014-03-28 NYB Case 439188 replaced localCountryDescription with sortOrder,country: --->
			order by sortOrder,country
		</cfquery>

		<cfreturn getLocatorCountries>
	</cffunction>

	<!--- NJH 2013Roadmap function --->
	<cffunction name="getFlagGroupsAndFlagsFromSearchCriteria" returntype="struct" output="false">
		<cfargument name="searchCriteriaList" type="string" required="true">

		<cfset var result = structNew()>
		<cfset var allFlagIDs = "">
		<cfset var profileDisplayStruct = structNew()>
		<cfset var currentRow = 0>
		<cfset var profile = "">
		<cfset var flagGroupID = 0>
		<cfset var flagsInFlagGroup = "">
		<cfset var flagID = 0>
		<cfset var structKey = "">
		<cfset var flagGroupIDKey = "">

		<cfloop list="#arguments.searchCriteriaList#" index="profile">
			<cfset currentRow++>
			<cfif listFirst(profile,".") eq "p">
				<cfset flagGroupID = listLast(profile,".")>
				<cfset flagsInFlagGroup = application.com.flag.getFlagGroupFlags(flagGroupID=flagGroupID,systemFlags=false)>
				<cfset profileDisplayStruct["#currentRow#|#flagGroupID#"] = valueList(flagsInFlagGroup.flagID)>
				<cfset allFlagIDs = listAppend(allFlagIDs,valueList(flagsInFlagGroup.flagID))>
			<cfelse>
				<cfset flagID = listLast(profile,".")>
				<cfset flagGroupID = application.com.flag.getFlagStructure(flagID=flagID).flagGroupID>
				<cfset flagGroupIDKey = "">
				<cfloop list="#structKeyList(profileDisplayStruct)#" index="structKey">
					<cfif listLast(structKey,"|") eq flagGroupId>
						<cfset flagGroupIDKey = structKey>
						<cfbreak>
					</cfif>
				</cfloop>
				<cfif flagGroupIDKey eq "">
					<cfset flagGroupIDKey = "#currentRow#|#flagGroupID#">
					<cfset profileDisplayStruct[flagGroupIDKey] = "">
				</cfif>
				<cfset profileDisplayStruct[flagGroupIDKey] = listAppend(profileDisplayStruct[flagGroupIDKey],flagID)>
				<cfset allFlagIDs = listAppend(allFlagIDs,flagID)>
			</cfif>

		</cfloop>

		<cfset result = profileDisplayStruct>
		<!--- Done so that we output the profiles in the order which they appear in the relay tag --->
		<cfset result.sortedFlagGroupAndFlagList =listSort(structKeyList(profileDisplayStruct),"textnocase")>
		<cfset result.flagIDList = allFlagIDs> <!--- return all the flags available in the search criteria in no particular order --->

		<cfreturn result>
	</cffunction>


	<!--- START: 2013-10-02 NYB Case 435669 added function: --->
	<cffunction access="public" name="getLocationsRequiringLatLongUpdate" hint="Returns locations requiring update of the latitude and longitude fields">
		<cfargument name="countryID" type="numeric" required="no">
		<cfargument name="allLocations" type="string" required="no" default="">
		<cfargument name="locatorDefQry" type="query" required="no">
		<cfargument name="filterByFlag" type="string" required="no">
		<cfargument name="getSummary" type="boolean" default="false">
		<cfargument name="getTopNumber" type="numeric" default="200">
		<cfargument name="rtnFailuresOnly" type="boolean" default="false"><!--- 2014-03-05 NYB Case 438911 added --->

		<cfset var getLocationsRqgUpdateQry = "">
		<cfset var flagStruct = structnew()>

		<cfif structkeyexists(arguments,"filterByFlag")>
			<cfset flagStruct = application.com.flag.getFlagStructure(flagID=arguments.filterByFlag)>
		</cfif>

		<cfquery name="getLocationsRqgUpdateQry" datasource="#application.siteDataSource#">
			SELECT
				<cfif getSummary>
					distinct c.countryID,c.countryDescription,count(*) as totalLeft
				<cfelse>
					top #getTopNumber#
					l.SiteName,
					l.locationID,
					l.address1,
					l.address2,
					l.address3,
					l.address4,
					l.address5,
					l.postalcode,
					c.countryid, <!--- WAB 2014-10-22  added for CASE 442259 Area search --->
					c.countryDescription,
					l.latitude,
					l.longitude
				</cfif>
			 FROM
			 	country c with(nolock)
			 	inner join location l with(nolock) on l.countryid = c.countryID
			 		and
					<!--- START: 2014-03-05 NYB Case 43891 include locations with no lat or long assigned and added the option of just bringing back failed locs --->
					<cfif arguments.rtnFailuresOnly>
					l.latlongsource ='FAIL' and l.Latitude is null and l.Longitude is null
					<cfelse>
					(l.latlongsource is null or l.latlongSource='GOOGLE UPDATE REQD' or ((l.Latitude is null or l.Longitude is null) and l.latlongsource !='FAIL'))
					</cfif>
					<!--- END: 2014-03-05 NYB Case 43891  --->
					AND
					(
					l.address1 is not null OR
					l.address2 is not null OR
					l.address3 is not null OR
					l.address4 is not null OR
					l.address5 is not null OR
					l.address1 is not null OR
					l.postalcode is not null
					)
				<cfif structkeyexists(arguments,"countryID") and arguments.countryid gt 0>
					and c.countryid=<cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.countryid#">
				</cfif>
				<cfif structkeyexists(arguments,"filterByFlag") and structkeyexists(flagStruct,"FLAGID")>
			 		inner join #flagStruct.FLAGTYPE.DATATABLEFULLNAME# d with(nolock) on l.#flagStruct.FLAGGROUP.ENTITYTYPE.UNIQUEKEY# = d.entityID and d.flagid =  <cf_queryparam value="#flagStruct.FlagID#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
			/* WHERE 2013-08-22 NYB Case 436665 removed WHERE */
				<!--- 2013-12-05 NYB Case 436665 REMOVED:
				<cfif (((not len(arguments.allLocations)) or arguments.allLocations eq "yes" or arguments.allLocations eq "true") or getSummary) and structkeyexists(arguments,"locatorDefQry") and isquery(arguments.locatorDefQry)>
				--->
					<cfset counter = 0>
					<cfset maxrow=0>
					<cfset outerFirstRun = true>
					<cfoutput query="locatorDefQry" group="RequiredFlagList">
						<cfset validFlags = StructNew()>
						<!--- construct valid flag list for this locatordef --->
						<cfloop index="i" list="#RequiredFlagList#">
							<cfset flagDetails = application.com.flag.getFlagStructure(flagid=i)>
							<cfif flagDetails.isOK>
								<cfif ListFindNoCase("organisation,location",flagDetails.ENTITYTYPE.TABLENAME) gt 0>
									<cfif not structkeyexists(validFlags,flagDetails.ENTITYTYPE.TABLENAME)>
										<cfset validFlags[flagDetails.ENTITYTYPE.TABLENAME] = StructNew()>
									</cfif>
									<cfset validFlags[flagDetails.ENTITYTYPE.TABLENAME][flagDetails.FLAGTEXTID] = flagDetails.FLAGID>
								</cfif>
							</cfif>
						</cfloop>
						<cfif not StructIsEmpty(validFlags) or len(rtrim(ltrim(RequiredFlagList))) eq 0><!--- ie, if a flag is put in RequiredFlagList that doesn't exist then return no results for those countries --->
							<cfif outerFirstRun>
								WHERE /* 2013-08-22 NYB Case 436665 added WHERE */
								<cfset outerFirstRun = false>
							<cfelse>
								or
							</cfif>
							(
								<cfset innerFirstRun = true>
								<cfset validFlagEntities = StructKeyList(validFlags)>
								<cfloop index="tableName" list="#validFlagEntities#">
									<cfset validFlagTextIDs = StructKeyList(validFlags[tableName])>
									<cfloop index="j" list="#validFlagTextIDs#">
										<cfif innerFirstRun><cfset innerFirstRun = false><cfelse>
											AND
										</cfif>
										<cfset counter++>
											l.#tableName#id in
											(
												select entityid from BooleanFlagData b#counter# with(nolock) where b#counter#.flagid =  <cf_queryparam value="#validFlags[tableName][j]#" CFSQLTYPE="CF_SQL_INTEGER" >
											)
									</cfloop>
								</cfloop>
								<cfif innerFirstRun><cfset innerFirstRun = false><cfelse>
									AND
								</cfif>
								l.countryid in
									(
									<cfoutput><cfset maxrow++></cfoutput>
									<cfoutput >  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" > <cfif currentrow lt maxrow>,</cfif></cfoutput>
									)
							)
						</cfif>
					</cfoutput>
				<!--- 2013-12-05 NYB Case 436665 REMOVED:
				</cfif>
				--->
				<cfif getSummary>
					group by c.countryID,c.countryDescription
					order by c.countryDescription
				<cfelse>
					order by l.SiteName,l.locationid
				</cfif>
		</cfquery>

		<cfreturn getLocationsRqgUpdateQry>
	</cffunction>
	<!--- END: 2013-10-02 NYB Case 435669 --->



	<cffunction name="getDistanceForCountry" returnType="query" access="public">
		<cfargument name="countryID" type="numeric" required="true">

		<cfset var distanceMeasure = application.com.relayCountries.getCountryDetails(countryID=arguments.countryID).distanceMeasure>
		<cfset var getDistanceQry = "">
		<cfset var distanceLengths = getLocatorDefinition(countryId=arguments.countryId,cache=false).distanceLengths>
		<cfset var lengths = 0>
		<cfset var counter = 0>

		<cfif distanceLengths eq "">
			<cfset distanceLengths = "10,20,50,100">
		</cfif>
		<cfset lengths = listLen(distanceLengths)>

		<!--- NJH Case 436189 re-instate distances from locator def --->
		<cfquery name="getDistanceQry" datasource="#application.siteDataSource#">
			<cfloop list="#distanceLengths#" index="distanceLength">
				<cfset counter++>
				select #distanceLength# as value, '#distanceLength# '+ lower(distanceMeasure) as display
				from country where countryID = <cf_queryparam value="#arguments.countryID#" cfsqltype="cf_sql_integer">
				<cfif counter lt lengths>union</cfif>
			</cfloop>
			order by value
		</cfquery>

		<cfreturn getDistanceQry>
	</cffunction>


	<cffunction name="getLocatorSearchCriteria" access="public" returntype="struct" output="false">
		<cfargument name="countryID" type="numeric" required="true">

		<cfset var result = {content="",contactReseller=false}>
		<cfset var getLocatorDef = getLocatorDefinition(countryId=arguments.countryId,cache=false)>
		<cfset var getCountries = ""><!--- 2014-03-28 NYB Case 439188 changed to blank string --->
		<cfset var distanceMeasure = getDistanceForCountry(countryID=arguments.countryID)>
		<cfset var profileDisplayStruct = getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList = getLocatorDef.searchCriteriaList)>
		<cfset var structKey = "">
		<cfset var flagGroupID = 0>
		<cfset var flagStruct = structNew()>
		<cfset var imgSrc = "">
		<cfset var altImageText = "">
		<cfset var imgFilePath = "">
		<cfset var productFlags = getLocatorProducts()>
		<cfset var i = "">
		<cfset var flagID = "">

		<!--- START: 2014-03-28 NYB Case 439188 added: --->
		<cfif getLocatorDef.recordCount gt 0>
		<cfset result.contactReseller = getLocatorDef.contactReseller>
			<cfset getCountries = getLocatorCountries(cache=false)>
		<cfelse>
			<cfset getCountries = getLocatorCountries(cache=false,showNullValue="true")>
			<cfset arguments.countryID = 0>
		</cfif>
		<!--- END: 2014-03-28 NYB Case 439188 --->

		<cfsavecontent variable="result.content">
			<cfoutput>
				<script type="text/javascript">
					jQuery('##advancedSearchLink a').click(function(){
						var myDiv = jQuery('##advancedFilterContainer');

						if(myDiv.css('display') == 'none'){

							jQuery(".lessAdvancedSearchLink").show();
							jQuery(".moreAdvancedSearchLink").hide();
							myDiv.slideDown();

						}else{

							jQuery(".moreAdvancedSearchLink").show();
							jQuery(".lessAdvancedSearchLink").hide();
							myDiv.slideUp();

						}
					});
				</script>
				<form id="searchForm" name="searchForm">
					<div id="searchCriteria">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 form-group">
								<cf_select name="countryID" label="phr_locator_selectCountry" selected="#arguments.countryID#" query="#getCountries#" display="Country" value="countryID" onchange="javascript:getSearchCriteria(this.value);" class="form-control">
						 	</div>
						<cfif getLocatorDef.recordCount>
							<cfif isBoolean(getLocatorDef.useCompany) and getLocatorDef.useCompany>
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 form-group">
									<cf_input type="text" id="company" name="company" label="phr_locator_enterACompanyName" currentValue="" placeholder="phr_locator_enterACompanyName" class="form-control">
								</div>
							</cfif>
							<cfif getLocatorDef.useGeo>
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 form-group">
									<cf_input type="text" id="location" name="location" label="phr_locator_enterYourCityStatePostCode" class="form-control" currentValue="" placeholder="phr_locator_enterYourCityStatePostCode">
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 form-group">
									<cf_relayFormElement relayFormElementType="select" fieldname="distance" label="phr_locator_radius" currentValue="" query=#distanceMeasure# display="display" value="value" class="form-control">
								</div>
							</cfif>

						</div>

							<cfif getLocatorDef.searchCriteriaList neq "">
								<div id="advancedSearchLink">
									<a href="javascript:void(0)" class="moreAdvancedSearchLink">phr_more_search_options</a>
									<a href="javascript:void(0)" class="lessAdvancedSearchLink">phr_less_search_options</a>
								</div>
										<div class="row" id="advancedFilterContainer">
										<!--- <h3 class="col-xs-12">phr_locator_advanced_filterheader:</h3> --->
								<cfloop list="#profileDisplayStruct.sortedFlagGroupAndFlagList#" index="structKey">
									<cfset flagGroupID = listLast(structKey,"|")>

											<div class="locatorFilter col-xs-12 col-sm-3">
												<h3><!--- <a onClick="toggleFilter('#flagGroupID#');" class="toggleFilterTD"><img src="/code/borders/images/screenBlueArrowDown.png" id="#flagGroupID#_toggle" class="toggleImg"/> --->#htmlEditFormat(application.com.flag.getFlagGroupStructure(flagGroupID=flagGroupID).name)#<!--- </a> ---></h3>
												<div id="#flagGroupId#" class="searchItemContainer" style="display:block;">
													<ul>
													<cfloop list="#profileDisplayStruct[structKey]#" index="flagID">
														<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagID)>
														<cfif flagStruct.flagactive eq 1>
															<cfset imgSrc = "/images/misc/blank.gif">
															<cfset altImageText = "">
															<cfloop list="jpg,gif,png" index="i">
																<cfset imgFilePath = "/content/linkImages/flag/#flagID#/Thumb_#flagID#.#i#">
																<cfif fileExists(expandPath(imgFilePath))>
																	<cfset imgSrc = imgFilePath>
																	<cfset altImageText = flagStruct.name>
																</cfif>
															</cfloop>
															<cfset var name = "flagIDList">
															<li class="searchItems">
																<cf_img src="#imgSrc#" width="16px" height="16px" alt="#htmlEditFormat(altImageText)#" title="#htmlEditFormat(altImageText)#">
																<cf_input type="checkbox" name="#name#" id="#flagID#" value="#flagID#"> &nbsp; <label for="#flagID#">#htmlEditFormat(flagStruct.name)#</label>
															</li>
														</cfif>
													</cfloop>
													</ul>
												</div>
											</div>

								</cfloop>
										</div>
							</cfif>
							<!--- 2014/01/28	YMA	Case 438652 Fixed map doesnt re-load after a new search. --->
							<div class="row buttonLocator">
								<div id="locatorClearBtn">
									<a href="##" onclick="clearFilters();return false;" id="clearLocator" class="btn btn-sm btn-primary">phr_locator_ClearFilters</a>
								</div>
								<div id="locatorSearchBtn">
									<input type="button" id="searchLocator" value="phr_locator_Search" class="btn btn-sm btn-primary" onclick="javascript:search();mapLoaded = 0;" />
								</div>
							</div>
						<cfelse>
							<!--- 2014-03-28 NYB Case 439188 added the if: --->
							<cfif arguments.countryid neq 0><tr><td>phr_locator_noDefinitionSupplied</td></tr></cfif>
						</cfif>
					</div>
				</form>

			</cfoutput>
		</cfsavecontent>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2013Roadmap function --->
	<cffunction name="getDisplayResults" access="public" returnType="struct" output="false">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="location" type="string" required="false">
		<cfargument name="distance" type="numeric" required="false">
		<cfargument name="company" type="string" required="false">
		<cfargument name="flagIDList" type="string" default=""> <!--- holds checkbox value; radio flags come through as flag_#flagGroupID# --->
		<cfargument name="viewType" type="string" default="list,map">
		<cfargument name="getResultsFrom" type="numeric" default="1" required="true">
		<cfargument name="getResultsStep" type="numeric" default="20" required="true">

		<cfset var locatorResults = getLocatorResults(argumentCollection=arguments)>
		<cfset var argStruct = {locatorResults=locatorResults,countryID=arguments.countryID, getResultsFrom = arguments.getResultsFrom}>
		<cfset var display = {hasResults=locatorResults.recordCount}>
		<cfset var viewAs = "list">

		<cfif structKeyExists(arguments,"distance")>
			<cfset argStruct.distance = arguments.distance>
		</cfif>

		<cfloop list="#arguments.viewType#" index="viewAs">
			<cfset display[viewAs] = getLocatorResultsByViewType(argumentCollection=argStruct,viewType=viewAs).content>
		</cfloop>

		<cfreturn display>
	</cffunction>


	<!--- NJH 2013Roadmap function
		WAB 2014-10-22  CASE 442259 Added Area search
		WAB 2014-10-22  Also added, in passing, a maxRows argument
	--->
	<cffunction name="getLocatorResults" access="public" returnType="query">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="location" type="string" required="false">
		<cfargument name="distance" type="numeric" required="false">
		<cfargument name="company" type="string" required="false">
		<cfargument name="flagIDList" type="string" default=""> <!--- holds checkbox value; radio flags come through as flag_#flagGroupID# --->
		<cfargument name="getResultsFrom" type="numeric" default="1" required="true">
		<cfargument name="getResultsStep" type="numeric" default="5" required="true"><!--- holds checkbox value; radio flags come through as flag_#flagGroupID# --->

		<cfset var flagID = "">
		<cfset var locatorResultsQry = "">
		<cfset var locationLatLong = {longitude="",latitude=""}>
		<cfset var useProximity = false>
		<cfset var getLocatorDef = getLocatorDefinition(countryId=arguments.countryId,cache=false)>
		<cfset var orderByIndex = listLen(getLocatorDef.orderbyFlagOverride)>
		<cfset var replaceSpecialChars = replaceSpecialCharactersForDBQuery()>
		<cfset var replaceValueStatement_SQL = replaceSpecialChars.replaceValueStatement_SQL>
		<cfset var replaceStatement_SQL = replaceSpecialChars.replaceStatement_SQL>
		<cfset var flagStruct = structNew()>
		<cfset var flagGroupStruct = structNew()>
		<cfset var profileDisplayStruct = getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList = getLocatorDef.searchCriteriaList)>
		<cfset var distanceMeasure = application.com.relayCountries.getCountryDetails(countryID=arguments.countryID).distanceMeasure>
		<cfset var flagGroupID = "">
		<cfset var flagTextID = "">

		<cfif structKeyExists(arguments,"location") and arguments.location neq "">
			<cfset useProximity = true>
                        <!--- WAB 2014-10-22  CASE 442259, moved function call from lower down --->
			<cfset locationLatLong = getLatLongFromAddress(countryID=arguments.countryID,location=arguments.location)>
                        <!--- WAB 2014-10-22  CASE 442259, proposal to make sure that the distance was always at least as large as size the bounding box, not implemented
			<cfif structKeyExists(arguments,"distance") and arguments.distance neq 0 and structKeyExists (locationLatLong,"viewport") and arguments.distance LT locationLatLong.viewport.diagonalDistance>
				<cfset arguments.distance = locationLatLong.viewport.diagonalDistance>
			</cfif>
			--->

		</cfif>

		<cfquery name="locatorResultsQry" datasource="#application.siteDataSource#">
			select ROW_NUMBER()
        OVER (order by orderByFlag desc,distance,sitename) AS Row, * from
			 (select distinct sitename, aka, locationID, case when len(isNull(specificURL,'')) > 0 then specificURL else orgUrl end as url,latitude,longitude,o.organisationID,
				case when len(isNull(pictureURL,'')) != 0 then pictureURL else '/images/social/icon_no_orgPhoto.png' end as pictureURL,
				<cfif useProximity>
					<!--- store this struct in request scope, so that we can access it when drawing our maps --->
					<cfset request.userLatLong = locationLatLong>
					<cfif locationLatLong.addressFound>
						<!--- WAB 2014-10-22  CASE 442259, implement a SQL function for calculating distance, NOTE - returns kilometers  --->
						dbo.getDistanceBetweenPoints (#locationLatLong.latitude#,#locationLatLong.longitude#,l.latitude,l.longitude)  <cfif distanceMeasure eq "miles">/ 1.6</cfif>

					<cfelse>
						0
					</cfif>
				<cfelse>
					0
				</cfif>
				as distance,

				<!--- WAB 2014-10-22  CASE 442259, add administrative areas --->
				administrativeAreaLevel1,
				administrativeAreaLevel2,
				<cfif orderByIndex neq 0>
					case
						<cfloop list="#getLocatorDef.orderbyFlagOverride#" index="flagTextID">
							when orderByFlag_#flagTextID#.flagID is not null then #orderByIndex#
							<cfset orderByIndex-->
						</cfloop>
						else 0 end
				<cfelse>
					1
				</cfif> as orderByFlag
				from organisation o
					inner join location l on o.organisationID = l.organisationID
					<!--- NJH 2013/07/23 Case 436190 - re-instate requiredFlagList --->
					<cfif listlen(getLocatorDef.requiredFlagList)>
						<cfloop list="#getLocatorDef.requiredFlagList#" index="flagTextID">
							<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagTextID)>
					inner join #flagStruct.flagType.dataTableFullName# requiredFlag_#flagTextID# on requiredFlag_#flagTextID#.entityID = #left(flagStruct.entityType.tablename,1)#.#flagStruct.entityType.uniqueKey# and requiredFlag_#flagTextID#.flagID=#flagStruct.flagID#
						</cfloop>
					</cfif>
			 		<cfif structKeyExists(arguments,"flagIDList")>
					<cfloop list="#arguments.flagIDList#" index="flagID">
						<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagID)>
					left outer join #flagStruct.flagType.dataTableFullName# f_#flagID# on f_#flagId#.entityID = #left(flagStruct.entityType.tablename,1)#.#flagStruct.entityType.uniqueKey# and f_#flagID#.flagID=#flagID#
					</cfloop>
					<cfif listlen(getLocatorDef.orderbyFlagOverride)>
						<cfloop list="#getLocatorDef.orderbyFlagOverride#" index="flagTextID">
							<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagTextID)>
							left outer join #flagStruct.flagType.dataTableFullName# orderByFlag_#flagTextID# on orderByFlag_#flagTextID#.entityID = #left(flagStruct.entityType.tablename,1)#.#flagStruct.entityType.uniqueKey# and orderByFlag_#flagTextID#.flagID=#flagStruct.flagID#
						</cfloop>
					</cfif>
				</cfif>
				where l.countryID = <cf_queryParam value="#arguments.countryID#" cfsqltype="cf_sql_integer">
				<cfif useProximity>
					<cfif not locationLatLong.addressFound>
						and 1=0
					</cfif>
					and l.latitude is not null and l.longitude is not null
				</cfif>

				<!--- NJH Case 436185 - when searching, display all flags as checkboxes.. but we 'and' any flags in different groups and 'or' flags within a group. For example, get me all resellers that are approved AND have 1-5 OR 25-50 employees--->
				<cfif structKeyExists(arguments,"flagIDList") and listLen(arguments.flagIDList)>
					<cfloop list="#arguments.flagIDList#" index="flagID">
						<cfset flagStruct = application.com.flag.getFlagStructure(flagID=flagID)>
						<cfif not structKeyExists(flagGroupStruct,flagStruct.flagGroupID)>
							<cfset flagGroupStruct[flagStruct.flagGroupID] = "">
						</cfif>
						<cfset flagGroupStruct[flagStruct.flagGroupID] = listAppend(flagGroupStruct[flagStruct.flagGroupID],flagID)>
					</cfloop>
					<cfloop collection="#flagGroupStruct#" item="flagGroupID">
						<cfset flagCount = 0>
						<cfset var numFlagsInList = listLen(flagGroupStruct[flagGroupID])>
						and (<cfloop list="#flagGroupStruct[flagGroupId]#" index="flagID">
								f_#flagID#.entityID is not null
								<cfset flagCount++>
								<cfif flagCount lt numFlagsInList>or</cfif>
							</cfloop>)
					</cfloop>
				</cfif>
				<cfif structKeyExists(arguments,"company") and arguments.company neq "">
					and (#replaceStatement_SQL#l.sitename#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.company#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
							or #replaceStatement_SQL#o.organisationName#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.company#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
							or #replaceStatement_SQL#o.aka#replace(replaceValueStatement_SQL,"**","'","ALL")# like #replaceStatement_SQL#N'#arguments.company#%'#replace(replaceValueStatement_SQL,"**","'","ALL")#
						)
				</cfif>
                <!--- WAB 2014-10-22  CASE 442259 added administrative area search
					NJH 2016/02/09 only check if address found.
					--->
				<cfif useProximity and locationLatLong.addressFound>
					<cfif locationLatLong.addressType is "administrative_area_level_1">
						<!--- If Address is administrative_area_level_1 then just search on that field --->
						and l.administrativeAreaLevel1  =  <cf_queryparam value="#LEFT(locationLatLong.administrativeAreaLevel1,200)#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelseif locationLatLong.addressType is "administrative_area_level_2">
						<!--- If Address is administrative_area_level_2 then search on that field
							and also on Level_1 if a level_1 has been returned by google
							but deal with fact that some addresses may have a Level2 but no Level1
						--->
						and l.administrativeAreaLevel2  =  <cf_queryparam value="#LEFT(locationLatLong.administrativeAreaLevel2,200)#" CFSQLTYPE="CF_SQL_VARCHAR" >
						<cfif structKeyExists (locationLatLong,"administrativeAreaLevel1")>
						and (l.administrativeAreaLevel1 =  <cf_queryparam value="#LEFT(locationLatLong.administrativeAreaLevel1,200)#" CFSQLTYPE="CF_SQL_VARCHAR" > or isNull(l.administrativeAreaLevel1,'') = '')
						</cfif>

					<cfelseif structKeyExists(arguments,"distance") and arguments.distance neq 0>
						and dbo.getDistanceBetweenPoints (#locationLatLong.latitude#,#locationLatLong.longitude#,l.latitude,l.longitude)<cfif distanceMeasure eq "miles">/ 1.6</cfif> <= <cf_queryParam value="#arguments.distance#" cfsqltype="cf_sql_integer">
					</cfif>

				</cfif>


			) as base
                        <!--- WAB 2014-10-22  CASE 442259 Moved where block into main query  --->
			order by orderByFlag desc,distance,sitename
		</cfquery>

		<cfreturn locatorResultsQry>

	</cffunction>


	<!--- NJH 2013Roadmap function --->
	<cffunction name="getLocatorResultsByViewType" access="private" returnType="struct" output="false">
		<cfargument name="locatorResults" type="query" required="true">
		<cfargument name="viewType" type="string" default="list" hint="Can be list or map">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="distance" type="numeric" required="false">
		<cfargument name="getResultsFrom" type="numeric" default="1" required="true">
		<cfargument name="getResultsStep" type="numeric" default="20" required="true">

		<cfset var result = {content="",viewType=arguments.viewType}>
		<cfset var resultID = "locatorResultsData">
		<cfset var resultsDisplay = "<div id='#resultID#'>phr_locator_noCompaniesFound.</div>">
		<cfset var useProximity = false>
		<cfset var getFlagGroupsForFlags = "">
		<cfset var countryDetails = application.com.relayCountries.getCountryDetails(countryID=arguments.countryID)>
		<cfset var distanceMeasure = countryDetails.distanceMeasure>
		<cfset var flagStruct = structNew()>
		<cfset var locatorDef = application.com.structureFunctions.queryRowToStruct(query=getLocatorDefinition(countryID=arguments.countryID,cache=false),row=1)>
		<cfset var profileDisplayStruct = getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList = locatorDef.searchCriteriaList)>
		<cfset var getPartnerFlags = "">
		<cfset var encryptedArgs = "">
		<cfset var imgFilePath = "">
		<cfset var flagGroupStruct = structNew()>
		<cfset var flagCount = 0>
		<cfset var numFlagsInList = 0>
		<cfset var resultRadius = 20>
		<cfset var zoomLevel = countryDetails.zoom>
		<cfset var latitude = countryDetails.latitude>
		<cfset var longitude = countryDetails.longitude>
		<cfset var markers = arrayNew(1)>
		<cfset var markerCount = 0>
		<cfset var locationInfo = {}>
		<cfset var marker = "">
		<cfset var nextStart = 0>
		<cfset var thisEnd = 0>
		<cfset var locatorResultsFromTo = ''>

		<cfprocessingdirective suppresswhitespace="yes">
			<cfsavecontent variable="resultsDisplay">

				<cfset nextStart = arguments.getResultsFrom + arguments.getResultsStep>
				<cfset thisEnd = arguments.getResultsFrom + arguments.getResultsStep - 1>

				<cfquery name="locatorResultsFromTo" dbtype="query">
				SELECT * FROM arguments.locatorResults WHERE
				ROW BETWEEN #arguments.getResultsFrom# AND #thisEnd#
				</cfquery>

				<cfoutput>
					<cfif arguments.viewType eq "list">
						<cfif locatorResultsFromTo.recordCount>
							<div id="#resultID#">
							<cfloop query="locatorResultsFromTo">
								<div id="companyProfileRow#currentRow#" class="companyProfileRow">
									<cfset locationInfo = application.com.structureFunctions.queryRowToStruct(query=locatorResultsFromTo,row=currentRow)>
									<cfset structAppend(locationInfo,locatorDef)>
									#setInfoText(argumentCollection=locationInfo,currentRow=currentRow,useProximity=useProximity,viewMode="listingScreen")#
								</div>
							</cfloop>

					<cfif nextStart LTE arguments.locatorResults.recordCount>

							<div id="showMeMore">
								<div class="companyInfoTD" colspan="2" id="showMeMoreTD">
<input type="button" class="btn btn-sm btn-primary" value="phr_locator_showMore" onclick="getmoreData();">
							<!---	<div class="showoption" onclick="getmoreData();">--->

								<input type="hidden" name="nextStart" id="nextStart" value="#nextStart#">
							<!---	</div> --->
								</div>
							</div>
						</cfif>

							</div>

						<cfelseif useProximity and not locationLatLong.addressFound>
							<div id='#resultID#' class="col-md-12 col-xs-12">phr_locator_couldNotFindAddress.</div>

						<cfelse>
							<div id='#resultID#' class="col-md-12 col-xs-12">phr_locator_noCompaniesFound.</div>
						</cfif>

					<cfelseif arguments.viewType eq "map">

						<div id="map_canvas"></div>

						<cfif locatorResultsFromTo.recordCount>
							<cfif structKeyExists(arguments,"distance") and arguments.distance gt 0>
								<cfset resultRadius = arguments.distance>
							</cfif>
							<cfset zoomLevel = 20 - round(((15*resultRadius)+300)/(14*(sqr(resultRadius))))>

							<cfif structKeyExists(request,"userLatLong")>
								<cfif request.userLatLong.addressFound>
									<cfset latitude = request.userLatLong.latitude>
									<cfset longitude = request.userLatLong.longitude>
								</cfif>
							</cfif>
						</cfif>

						<!--- Google Maps setup. --->
						<script type="text/javascript">

							var markerArray = [];
							<cfset markers = getMarkers(locatorResults = locatorResultsFromTo,locatorDef=locatorDef)>
							<cfset markerCount = 0>

							<cfloop array="#markers#" index="marker">
								markerArray[#markerCount#] = {title:'#jsStringFormat(marker.title)#',icon:'#jsStringFormat(marker.icon)#',latitude:'#jsStringFormat(marker.latitude)#',longitude:'#jsStringFormat(marker.longitude)#',infoWindowText:'#jsStringFormat(marker.infoWindowText)#'};
								<cfset markerCount++>
							</cfloop>
							displayGoogleMap('map_canvas',#jsStringFormat(latitude)#,#jsStringFormat(longitude)#,#jsStringFormat(zoomLevel)#,markerArray);
						</script>
					</cfif>
				</cfoutput>
			</cfsavecontent>
		</cfprocessingdirective>

		<cfset result.content = resultsDisplay>

		<cfreturn result>
	</cffunction>


	<!--- 	NJH 2013Roadmap function
		WAB 2014-10-22 CASE 442259 Area Search.  Bring back more information from the Google Result - including Status, AdminAreas, bounding box

	--->
	<cffunction name="getLatLongFromAddress" access="public" returntype="struct">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="location" type="string" required="true">

		<cfset var result = {latitude="",longitude="",addressFound=false, addressrequested = location, googlerequest="",addressType=''}>
		<cfset var latitudeArray = "">
		<cfset var longitudeArray = "">
		<cfset var fileContentXML = "">
		<cfset var mapXML = "">
		<cfset var countryDesc = application.com.relayCountries.getCountryDetails(countryID=arguments.countryID).countryDescription>

		<!--- 	2014-11-19 WAB
				Deal with regions entered by abbreviation.  States such as OR did not work
				New approach is to check in our provinces table for the abbreviation
				If the abbreviation is found (ie we know that we are dealing with an admin area) then we make a specific request to google
		--->
		<cfquery name="checkForRegionAbbreviation">
		select
			name, abbreviation
		from
			province p
				inner join
			country c on p.countryid = c.countryid
		where
			p.countryid = <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and p.abbreviation = <cf_queryparam value="#location#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfset var googleSearchSuccess = false>

		<!--- 	If we know that the user has entered the abbreviation for a region then ask google for details of that region using its full name
				If google gives us an answer then use it, otherwise fall back to a standard request
		 --->
		<cfif checkForRegionAbbreviation.recordCount>
			<!--- we have the option of doing a search by abbeviation or by full name here.  Both work for the US so I am using abbreviation, not sure whether best decision or not --->
			<cfset result.googlerequest = "administrative_area:#checkForRegionAbbreviation.abbreviation#|country:#countryDesc#">
			<cfhttp url="http://maps.googleapis.com/maps/api/geocode/xml?components=#result.googlerequest#&sensor=false" method="get" result="mapXML" timeout="120"></cfhttp>
			<cfset fileContentXML = xmlParse(mapXML.Filecontent)>
			<cfset var statusSearch = XmlSearch( fileContentXML, "/GeocodeResponse/status")>
			<cfif statusSearch[1].xmlText is "OK">
				<cfset googleSearchSuccess = true>
			<cfelse>
				<cfset result.googleRequestFailed = result.googlerequest>
			</cfif>
		</cfif>

		<!--- If we did not do an admin area request to google, or it failed, then we do a regular address search --->
		<cfif not googleSearchSuccess>
			<cfset result.googlerequest = arguments.location & "," & countryDesc>
			<cfhttp url="http://maps.googleapis.com/maps/api/geocode/xml?address=#urlEncodedFormat(result.googlerequest)#&sensor=false" method="get" result="mapXML" timeout="120"></cfhttp>
			<cfset fileContentXML = xmlParse(mapXML.Filecontent)>
		</cfif>


		<cfset var statusSearch = XmlSearch( fileContentXML, "/GeocodeResponse/status")>
		<cfset result.status = statusSearch[1].xmlText>

		<cfset var resultSearch = XmlSearch(fileContentXML,"/GeocodeResponse/result")>

		<cfif arrayLen(resultSearch)>

			<!--- Have a result so pull the details into the result structure --->
			<cfset var resultNode = resultSearch[1]>
			<cfset result.addressFound = true>

			<cfset result.latitude = XmlSearch(resultNode,"./geometry/location/lat")[1].xmlText>
			<cfset result.longitude = XmlSearch(resultNode,"./geometry/location/lng")[1].xmlText>
			<cfset result.address= XmlSearch(resultNode,"./formatted_address")[1].xmlText>


			<!--- find out the address type, bit of a stab in the dark, seem to have more than one type element sometimes but the one we want always seems to be first --->
			<cfset var TypeSearch = XmlSearch( resultNode, './type')>
			<cfif arrayLen (TypeSearch)>
				<cfset result.addressType = TypeSearch[1].xmltext>
			</cfif>




			<!--- return adminLevel names if they exist--->
			<cfset var adminLevel1Search = XmlSearch( resultNode, './address_component[type="administrative_area_level_1"]')>
			<cfif arrayLen (adminLevel1Search)>
				<cfset result.administrativeAreaLevel1 = adminLevel1Search[1].long_name.xmlText>
			</cfif>

			<cfset var adminLevel2Search = XmlSearch( resultNode, './address_component[type="administrative_area_level_2"]')>
			<cfif arrayLen (adminLevel2Search)>
				<cfset result.administrativeAreaLevel2 = adminLevel2Search[1].long_name.xmlText>
			</cfif>


			<!--- WAB 2014-09-03 Added this code to pull out the viewport bounding box and calculate diagonal
					Not actually used yet, but could be useful!
					Note requires new getDistanceBetweenPoints SQL function
			--->
			<cfset var viewPort = XmlSearch(fileContentXML,"/GeocodeResponse/result/geometry/viewport")>
			<cfif arrayLen(viewPort)>
				<cfset result.viewPort.southwest = {lat =  viewPort[1].southwest.lat.xmltext, long =  viewPort[1].southwest.lng.xmltext}>
				<cfset result.viewPort.northeast = {lat =  viewPort[1].northeast.lat.xmltext, long =  viewPort[1].northeast.lng.xmltext}>
				<cfset result.viewPort.diagonalDistance = getDistanceBetweenPoints(result.viewPort.southwest, result.viewPort.northeast)>
			</cfif>

		</cfif>

		<!--- START 2014-11-24 AHL Case 442668 geo-data population task not running --->
		<cfif structKeyExists(result, "status") AND result.status neq "OK">
			<cfset application.com.errorHandler.recordRelayError_Warning (severity="Information",Type="Failing getting Geocode From Address",Message="Failing getting Geocode From Address (#location#) with status #result.status# ",warningStructure=result,TTL=7 )>
		</cfif>
		<!--- END 2014-11-24 AHL Case 442668 geo-data population task not running --->

		<cfreturn result>
	</cffunction>


        <!--- WAB 2014-10-22 CASE 442259 New function to geocode a location
                brought from locator\updateLocationGoogle.cfm (and modified to deal with administrative areas
        --->
	<cffunction name="geocodeLocation" output="true">
		<cfargument name="locationID" type="numeric" required="true">
		<cfargument name="location" type="struct" required="false">

		<cfset var locationqry = "">

		<cfif not structKeyExists (arguments,"location") OR len(trim(arguments.location.countryID)) EQ 0>
			<cfquery name="locationqry">
			select locationid, address1, address2, address3, address4, address5, postalcode, countryid
			from location
			where locationid = <cf_queryparam value="#arguments.locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset arguments.location = application.com.structureFunctions.queryRowToStruct(locationQry,1)>
		</cfif>

		<!--- Build the address --->
		<cfset var address = "">
		<cfset var addressPart = "">
		<cfloop list = "address1,address2,address3,address4,address5,postalcode" index="addresspart">
			<cfif location[addressPart] is not "">
				<cfset address = listappend(address,location[addressPart])>
			</cfif>
		</cfloop>

		<cfset address = replace(address,"?"," ","all")>
		<cfset address = replace(address,"&"," ","all")>

		<!--- Get the google result. --->
		<cfset var getLatLong = application.com.relayLocator.getLatLongFromAddress (location.countryid,address)>

		<cfswitch expression="#getLatLong.status#">
			<cfcase value="OK">
				<cfif getLatLong.addressFound>
					<cfquery>
						UPDATE LOCATION SET
							latitude = <cfqueryparam cfsqltype="cf_sql_float" value="#getLatLong.Latitude#">,
							longitude = <cfqueryparam cfsqltype="cf_sql_float" value="#getLatLong.Longitude#">,
							<!--- I could leave the administrative areas null when not returned, but am updating to '' so  that I can tell which items have been processed when doing the initial update of this field --->
							administrativeAreaLevel1 = <cfif structKeyExists (getLatLong,"administrativeAreaLevel1")><cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(getLatLong.administrativeAreaLevel1,200)#"><cfelse>''</cfif>,
							administrativeAreaLevel2 = <cfif structKeyExists (getLatLong,"administrativeAreaLevel2")><cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(getLatLong.administrativeAreaLevel2,200)#"><cfelse>''</cfif>,
							latlongsource = 'GOOGLE',
							lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
							lastUpdated = getDate(),
							lastUpdatedByPerson = #request.relayCurrentUser.personID#
						WHERE
							locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
				<cfelse>
					<cfquery>
						UPDATE LOCATION SET
							latlongsource = 'FAIL',
							lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
							lastUpdated = getDate(),
							lastUpdatedByPerson = #request.relayCurrentUser.personID#
						WHERE
							locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>
				</cfif>

			</cfcase>
			<cfcase value="ZERO_RESULTS,INVALID_REQUEST">
				<cfquery >
					UPDATE LOCATION SET
						latlongsource = 'FAIL',
							lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
							lastUpdated = getDate(),
							lastUpdatedByPerson = #request.relayCurrentUser.personID#
					WHERE
						locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfcase>
			<!--- The assumption is that we have over grabbed the data --->
			<cfcase value="OVER_QUERY_LIMIT">
			</cfcase>
		</cfswitch>

		<cfreturn getLatLong>

	</cffunction>

	<!--- WAB 2014-10-22 CASE 442259  Added this function to access new getDistanceBetweenPoints SQL function --->
	<cffunction name="getDistanceBetweenPoints" hint="returns the distance between two points in Km">
		<cfargument name="LatLong1" type="struct" required="true">
		<cfargument name="LatLong2" type="struct" required="true">

		<cfset var getDistance = "">

		<cfquery name="getDistance">
			select dbo.getDistanceBetweenPoints (#latLong1.lat#,#latLong1.long#,#latLong2.lat#,#latLong2.long#) as distance
		</cfquery>

		<cfreturn getDistance.distance>

	</cffunction>

	<cffunction name="reverseGeocode" access="public" returntype="struct">
		<cfargument name="latitude" type="numeric" required="true">
		<cfargument name="longitude" type="string" required="true">

		<cfset var result = {addressFound=false}>
		<cfset var mapXML = "">

		<cfhttp url="http://maps.googleapis.com/maps/api/geocode/xml?latlng=#latitude#,#longitude#&sensor=false" method="get" result="mapXML" timeout="120"></cfhttp>

		<cfset var fileContentXML = xmlParse(mapXML.Filecontent)>

		<cfset var statusSearch = XmlSearch( fileContentXML, "/GeocodeResponse/status")>
		<cfset result.status = statusSearch[1].xmlText>

		<cfset var resultSearch = XmlSearch(fileContentXML,"/GeocodeResponse/result")>

		<cfif arrayLen(resultSearch)>
			<cfset var resultNode = resultSearch[1]>
			<cfset result.addressFound = true>

			<cfset result.address= XmlSearch(resultNode,"./formatted_address")[1].xmlText>

			<!--- return adminLevel names if they exist--->
			<cfset var adminLevel1Search = XmlSearch( resultNode, './address_component[type="administrative_area_level_1"]')>
			<cfif arrayLen (adminLevel1Search)>
				<cfset result.administrativeAreaLevel1 = adminLevel1Search[1].long_name.xmlText>
			</cfif>

			<cfset var adminLevel2Search = XmlSearch( resultNode, './address_component[type="administrative_area_level_2"]')>
			<cfif arrayLen (adminLevel2Search)>
				<cfset result.administrativeAreaLevel2 = adminLevel2Search[1].long_name.xmlText>
			</cfif>


		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2013Roadmap function --->
	<cffunction name="getLocatorProducts" access="public" output="false" returntype="query">

		<cfset var qryGetLocatorProducts = "">

		<cfquery name="qryGetLocatorProducts" dataSource="#application.siteDataSource#">
			select pg.flagTextID as productGroupValue,'phr_'+pg.NamePhraseTextID as productGroupDisplay,
				p.flagID as productValue, 'phr_'+p.namePhraseTextID as productDisplay
			from flag pg
				inner join flagGroup pgFlagGroup on pg.flagGroupID = pgFlagGroup.flagGroupID and pgFlagGroup.flagGroupTextID='LocatorProductGroups'
				inner join flagGroup pFlagGroup on pFlagGroup.flagGroupTextID = replace(pg.flagTextID,'locator','')
				inner join flag p on p.flagGroupID = pFlagGroup.flagGroupID
			where pg.active = 1 and p.active = 1
			order by pg.orderingIndex,p.orderingIndex
		</cfquery>

		<cfreturn qryGetLocatorProducts>
	</cffunction>

	<!--- NJH 2013Roadmap function
			2016-10-08	WAB	CASE 450971 Improve performance, don't use our queryRowToStruct, just do a cfscript loop over query
	--->
	<cffunction name="getMarkers" access="private" returnType="array" output="false">
		<cfargument name="locatorResults" type="query" required="true">
		<cfargument name="locatorDef" type="struct" required="true">

		<cfset var marker = structNew()>
		<cfset var markerArray =  arrayNew(1)>
		<cfset var markerCount = 1>
		<cfset var locationInfo = structNew()>
		<cfset var locatorResultsQry = arguments.locatorResults>

		<cfscript>
			for (locationInfo in locatorResultsQry) {

				structAppend(locationInfo,arguments.locatorDef);

				if (locationInfo.latitude neq "" and locationInfo.longitude neq "") {
					marker = {latitude=locationInfo.latitude, longitude=locationInfo.longitude, infoWindowText="<div>#locationInfo.sitename#</div>", title=locationInfo.sitename,icon=""};
					marker.infoWindowText = "<table class='infoWindowText'><tr>"&setInfoText(argumentCollection=locationInfo)&"</tr></table>";
					if (markerCount lte 99) {
						marker.icon = "/styles/locatorImages/markers/marker#markerCount#.png";
					}
					arrayAppend(markerArray,marker);
					markerCount++;
				}
			}
		</cfscript>
<!---
		<cfloop query="locatorResultsQry">
			<cfset locationInfo = application.com.structureFunctions.queryRowToStruct(query=locatorResultsQry,row=currentRow)>
			<cfset structAppend(locationInfo,arguments.locatorDef)>

			<cfif latitude neq "" and longitude neq "">
				<cfset marker = {latitude=latitude,longitude=longitude,infoWindowText="<div>#sitename#</div>",title=sitename,icon=""}>

				<cfset marker.infoWindowText = "<table class='infoWindowText'><tr>"&setInfoText(argumentCollection=locationInfo)&"</tr></table>">

				<cfif markerCount lte 99>
					<cfset marker.icon = "/styles/locatorImages/markers/marker#markerCount#.png">
				</cfif>
				<cfset arrayAppend(markerArray,marker)>
				<cfset markerCount++>
			</cfif>
		</cfloop>
--->

		<cfreturn markerArray>
	</cffunction>

	<!--- NJH 2013Roadmap function --->
	<cffunction name="setInfoText" access="private" returntype="string" output="false">
		<cfargument name="pictureURL" type="any" default="">
		<cfargument name="sitename" type="string" required="true">
		<cfargument name="url" type="string" default="">
		<cfargument name="distance" type="string" default="">
		<cfargument name="useProximity" type="boolean" default="false">
		<cfargument name="latitude" type="string" default="">
		<cfargument name="longitude" type="string" default="">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="locationID" type="numeric" required="true">
		<cfargument name="currentRow" type="numeric" default=0>
		<cfargument name="viewMode" type="string" default="markers">

		<cfset var profileHTML = "">
		<cfset var profileDisplayStruct = "">
		<cfset var getPartnerFlags = "">
		<cfset var flagStruct = "">
		<cfset var imgFilePath = "">
		<cfset var encryptedArgs = "">

		<cfprocessingdirective suppresswhitespace="yes">
			<cfsavecontent variable="profileHTML">
				<cfoutput>
					<div class="imageProfileRow">
						<div class="imageDiv">
							<cfif pictureURL neq ""><cf_img class="orgImage" src="#arguments.pictureURL#" width="80px" height="80px"></cfif>
						</div>
					</div>
					<div class="companyInfoTD">
						<div class="companyName">#htmlEditFormat(arguments.sitename)#</div>


						#htmlEditFormat(application.com.relayPLO.formatAddress(locationID=arguments.locationID,showAddressOnly=true))#

						<div class="profileImages">
							<cfset profileDisplayStruct = application.com.relayLocator.getFlagGroupsAndFlagsFromSearchCriteria(searchCriteriaList = arguments.searchCriteriaList)>
							<cfif profileDisplayStruct.flagIDList neq "">
								<cfquery name="getPartnerFlags" datasource="#application.siteDataSource#">
									select distinct flagID from vLocFlagData l where data is not null and entityID = #arguments.locationID# and flagId in (<cf_queryParam value="#profileDisplayStruct.flagIDList#" cfsqltype="cf_sql_integer" list="true">)
									union
									select distinct flagID from vOrgFlagData o where data is not null and entityID = #arguments.organisationID# and flagID in (<cf_queryParam value="#profileDisplayStruct.flagIDList#" cfsqltype="cf_sql_integer" list="true">)
								</cfquery>

								<cfloop query="getPartnerFlags">
									<cfset flagStruct = application.com.flag.getFlagStructure(flagID=getPartnerFlags.flagID)>
									<cfset imgFilePath = "/content/linkImages/flag/#getPartnerFlags.flagID#/Thumb_#getPartnerFlags.flagID#.jpg">
									<cfif fileExists(expandPath(imgFilePath))>
										<cf_img src="#imgFilePath#" width="16px" height="16px" alt="#htmlEditFormat(flagStruct.name)#" title="#htmlEditFormat(flagStruct.name)#">
									</cfif>
								</cfloop>
							</cfif>
						</div>
						<cfif arguments.url neq ""> <a href="<cfif left(arguments.url,4) neq "http">http://</cfif>#htmlEditFormat(arguments.url)#" target="_blank">#htmlEditFormat(arguments.url)#</a><br></cfif>
					</div>
					<div id="partnerLocatorContactReseller">
						<cfif arguments.viewMode eq "listingScreen" and arguments.contactReseller><div class="contactResellerDiv"><span class="contactResellerCheckbox"><cf_input type="checkbox" name="frmLocationIDCheck" value="#application.com.security.encryptVariableValue(name='fromLocationIDCheck', value=arguments.locationID)#"></span></div></cfif>
					</div>
					<div id="viewProfileTD">

							<cfset encryptedArgs = application.com.security.encryptQueryString("&forOrganisationID=#arguments.organisationID#&forLocationID=#arguments.locationID#")>
							<a href="/?eid=organisationPublicProfile#encryptedArgs#" class="viewProfileLink fancybox.iframe mapLink btn btn-primary">phr_locator_ViewProfile</a>
					</div>
					<cfif currentRow neq 0>
					<div class="map" id="partnerLocatorMap">
						<cfif useProximity>
							#round(arguments.distance)# #lcase(arguments.distanceMeasure)#
						</cfif>
						<cfif arguments.latitude neq "" and arguments.longitude neq "">
							<a href="##" class="mapLink btn btn-primary" onclick="viewMap('companyProfileRow#currentRow#',#arguments.latitude#,#arguments.longitude#);return false;">phr_locator_Map</a>
						</cfif>
					</div>
					</cfif>

				</cfoutput>
			</cfsavecontent>
		</cfprocessingdirective>

		<cfreturn profileHTML>
	</cffunction>

	<!--- 	2014-11-21 AHL Case 442259 Administrative Areas in Locator
			This code is designed to be run during the release/upgrade of case 442259
			It populates the location.administrativeAreaLevel1/2 fields for locations which already have a latitidude/longitude
			It takes into account the google API limits, so this function includes a sleep() and just a limited number of items per run
			Called from \releaseScripts\2014RoadMap_PartnerCloud_releaseScripts.cfc
	--->
	<cffunction name="reloadGoogleAdminAreas" access="public" output="true" returntype="struct">
		<cfargument name="numberOfItems" type="numeric" default="200">
		<cfargument name="sleepBetweenItems" type="numeric" default="100">

		<cfset var result = {isOK = true,message="", overQueryLimit = false}>
		<cfset var updatedCount = 0>
		<cfset var failedCount = 0>

		<cfquery name ="qryLocationsToUpdate">
			SELECT TOP #numberOfItems# locationid, latitude,longitude, address4, address5 FROM dbo.[Location] l
				WHERE latitude IS NOT NULL
				AND administrativeAreaLevel1 IS NULL
				ORDER BY l.Created DESC
		</cfquery>

		<cfloop query = "qryLocationsToUpdate">
			<cfset getLatLong = application.com.relayLocator.reverseGeocode (latitude =latitude , longitude = longitude  )>
			<cfif getLatLong.status is "OK">
				<cfquery >
				UPDATE location
				SET
					administrativeAreaLevel1 = <cfif structKeyExists (getLatLong,"administrativeAreaLevel1")><cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(getLatLong.administrativeAreaLevel1,200)#"><cfelse>''</cfif>,
					administrativeAreaLevel2 = <cfif structKeyExists (getLatLong,"administrativeAreaLevel2")><cfqueryparam cfsqltype="cf_sql_varchar" value="#LEFT(getLatLong.administrativeAreaLevel2,200)#"><cfelse>''</cfif>
				WHERE locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset updatedCount ++>

			<cfelseif getLatLong.status is "ZERO_RESULTS">
				<cfquery >
				UPDATE location
				SET
					administrativeAreaLevel1 = '',
					administrativeAreaLevel2 = ''
				WHERE locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset failedCount ++>

			<cfelseif getLatLong.status is "over_query_limit">
				<!--- 	If we are over the query limit then Google suggests that we wait 2 seconds and try again.
						If the next request is successful then the problem is just that we have sent too many in a second
						If the next request is not successful then we are over our daily limit and we are supposed to wait for 2 hours
						Note that this item will not get updated on this run (didn't want to repeat the code), but this code is designed to be run more than once until all items are done
				 --->
				<cfset sleep (3000)>
				<cfset getLatLong = application.com.relayLocator.reverseGeocode (latitude =latitude , longitude = longitude )>
				<cfif getLatLong.status is "over_query_limit">
					<cfset result.isok = false>
					<cfset result.overquerylimit = true>
					<cfset result.message &= "Over Query Limit<br/>">
					<cfbreak>
				</cfif>

			<cfelse>
				<cfset result.message &= "LocationID: #locationid# #getLatLong.status#<br/>">
				<cfset failedCount ++>
			</cfif>
			<cfset sleep (sleepBetweenItems)>
		</cfloop>

		<cfquery name ="qryItemsLeft">
			SELECT count(*) as [count]
			FROM dbo.[Location] l
			WHERE latitude IS NOT NULL
				AND administrativeAreaLevel1 IS NULL
		</cfquery>

		<cfset result.itemsLeft = qryItemsLeft.count>
		<cfset result.message &= "#updatedCount# locations updated.<BR>#failedCount# locations failed<BR>#qryItemsLeft.count# remaining">

		<cfreturn result>
	</cffunction>

	<!--- 2016-02-22 WAB/DCC Case 448017 replace end script tags with an escaped version --->
	<cffunction name="jsStringFormat_" access="public" returnType="string" output="false">
		<cfargument name="string" type="string" required="true">

		<cfreturn replace(jsStringFormat (application.com.regExp.removeWhiteSpace(string = string, allLineBreaks = true)),"</script>","<\/script>", "ALL")>
		<!--- non whitespace version
		<cfreturn replaceNoCase(jsStringFormat (string),"</script>","<\/script>", "ALL")>
		 --->
	 </cffunction>

	<!--- 2014-11-27 AHL Case 442668 geo-data population task not running --->
	<cffunction name="updateLocationGoogleCoordinates" hint="Updating Geo-data for modified locations">
		<cfset var result = {isOK = true,message=""}>

		<cfset locatorDefQry = application.com.relayLocator.getLocatorDefinition(cache = "false",all = "true",limitByCountryRights="false")>
		<cfset getLocationsQry = application.com.relayLocator.getLocationsRequiringLatLongUpdate(locatorDefQry = locatorDefQry)>

		<cfloop query="getLocationsQry">
			<cfset geocodeResult = application.com.relayLocator.geocodeLocation (locationid,application.com.structureFunctions.queryRowToStruct(getLocationsQry,currentRow))>
				<cfif geocodeResult.status is "OVER_QUERY_LIMIT">
					<cfset result.message = "OVER_QUERY_LIMIT">
					<cfbreak>
				</cfif>
		</cfloop>

		<cfreturn result>
	</cffunction>
	<!--- 2014-11-27 AHL Case 442668 geo-data population task not running --->
</cfcomponent>