<!---
  --- objectFactory
  --- -------------
  ---
  --- Object management
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   03/12/15
  --->
<cfcomponent hint="Object management" accessors="true" output="false" persistent="false">

	<cffunction name="getObject" access="public" output="false" returnType="component">
		<cfargument name="object" type="string" required="true" hint="The name of the object">

		<cfparam name="request.componentLoadedOnDev" default="#structNew()#">
		<cfparam name="application.componentLoaded" default="#structNew()#"> <!--- NJH 2015/12/22 this will cause a reload of all components when it gets cleared. No nice way to otherwise clear them --->

		<!--- TODO: discuss with Will... is settings special case??  --->
		<cfif arguments.object neq "">
			<cfif (not structKeyExists(application.componentLoaded,arguments.object) or not application.componentLoaded[arguments.object]) or not structKeyExists(application.com,arguments.object) or (application.testSite eq 2 and not structKeyExists(request.componentLoadedOnDev,arguments.object) and object neq "settings")>
				<!--- <cfset var componentPath = listLen(arguments.object,'.')?reverse(ListRest(reverse(arguments.object),"."))&".":"">
				<cfset var componentName = listLast(arguments.object,".")> --->
				<cfset var fullComponentPath = "#arguments.object#.cfc">

				<cfloop list="code,relay" index="path">
					<cfif fileExists(application.paths[path]&"\"&replace(arguments.object,".","\","ALL")&".cfc")>
						<cftry>
							<cfset application.com[arguments.object] = createObject("#path#.#arguments.object#")>
							<cfset application.componentLoaded[arguments.object] = true>
							<cfif application.testSite eq 2>
								<cfset request.componentLoadedOnDev[arguments.object] = true>
							</cfif>

							<cfcatch>
								<cfset application.com.errorHandler.recordRelayError_Warning(type="Object",Severity="error",catch=cfcatch,WarningStructure=arguments)>>
								<cfif application.testsite eq 2>
									<cfoutput>Failed trying to create object '#path#.#fullComponentPath#'</cfoutput>
									<Cfdump var="#cfcatch#">
									<Cf_abort>
								</cfif>
							</cfcatch>
						</cftry>
						<cfbreak>
					<cfelse>
						<!--- file doesn't exist...Throw an error??' --->
					</cfif>
				</cfloop>
			</cfif>
		<cfelse>
			<cfthrow message="The object name must be specified">
		</cfif>

		<cfreturn application.com[arguments.object]>
	</cffunction>

	<cfset application.getObject = this.getObject>
</cfcomponent>