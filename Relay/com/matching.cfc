<!---
  --- matching
  --- --------
  ---
  --- Component that holds matching functions
  ---
  --- author: Nathaniel.Hogeboom
  --- date:   16/08/16
  --->
<cfcomponent hint="Component that holds matching functions" accessors="true" output="false" persistent="false">

	<cffunction name="getEntityMatchFields" access="public" output="false" returnType="struct" hint="Returns a structure of the various match fields for a given entity">
		<cfargument name="entityType" type="string" required="true">

		<!--- 2014/06/03 NJH start of new matching functions
			We have two types of matching: lead and standard. lead was done primarily for the connector
				when SF deals with partnerRegistration leads. This means that we create a person and lead. The lead is sent across to SF, approved and converted
				and the contact then comes back to RW. We then want to match on this field rather than standard matching. This match method should be removed
				when we deal with partner registrations as proper leads - but not sure if that will ever happen.
		 --->
		<cfset var matchMethod = {}>
		<cfset matchMethod.person.lead = "convertedFromLeadID">
		<!--- <cfset matchMethod.person.standard = "firstname,lastname,email,locationID"> --->
		<cfset matchMethod.location.lead = "convertedFromLeadID">

		<cfset matchMethod.person.crmID = "crmPerId">
		<cfset matchMethod.location.crmID = "crmLocId">
		<cfset matchMethod.organisation.crmID = "crmOrgId">

		<!--- <cfset matchMethod.location.standard.1 = "matchSiteName,matchAddress,address4,postalCode,countryID,organisationID"> <!--- perhaps organisationID and matchSiteName are core fields that cannot be changed --->
		<cfset matchMethod.location.standard.2 = "matchSiteName,address4,postalCode,countryID,organisationID">
		<cfset matchMethod.location.standard.3 = "matchSiteName,postalCode,countryID,organisationID">
		<cfset matchMethod.organisation.standard.1 = "matchName,vatNumber">
		<cfset matchMethod.organisation.standard.2 = "matchName"> --->

		<cfset var matchEntityType = "">
		<cfset var matchFields = application.com.settings.getSetting("matching.matchFields")>
		<cfloop collection="#matchFields#" item="matchEntityType">
			<cfif not structKeyExists(matchMethod,matchEntityType)>
				<cfset matchMethod[matchEntityType] = {}>
			</cfif>
			<cfset matchMethod[matchEntityType].standard = matchFields[matchEntityType]>
		</cfloop>

		<cfif structKeyExists(matchMethod,arguments.entityType)>
			<cfreturn matchMethod[arguments.entityType]>
		</cfif>

		<cfreturn {}>
	</cffunction>


	<cffunction name="getFieldsToPopulateMatchField" access="public" output="false" returnType="string" hint="Returns the list of fields used to populate a match field">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="matchField" type="string" required="true">

		<cfset var fields = "">

		<cfset var matchFields = {}>
		<!---<cfset matchFields.location = {matchname="address1,address4,postalCode",matchSiteName="sitename",matchAddress="address1,address2,address3,address4"}>--->
		<cfset matchFields.location = {matchname="sitename",matchAddress="address1,address2,address3,address4"}>
		<cfset matchFields.organisation = {matchname="organisationName"}>
		<cfset matchFields.person = {matchname="firstname,lastname,email"}>

		<cfif structKeyExists(matchFields,arguments.entityType) and structKeyExists(matchFields[arguments.entityType],arguments.matchField)>
			<cfset fields = matchFields[arguments.entityType][arguments.matchField]>
		</cfif>

		<cfreturn fields>
	</cffunction>


	<cffunction name="getRequiredMatchFields" access="public" output="false" returnType="string" hint="Returns a list of columns needed for matching">
		<cfargument name="entityType" type="string" required="true">

		<cfset var theEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var requiredFields = "">
		<cfset var entityMatchFields = getEntityMatchFields(entityType=arguments.entityType)>

		<cfloop collection="#entityMatchFields#" item="matchMethodType">
			<cfset requiredFields = listAppend(requiredFields,entityMatchFields[matchMethodType])>
			<cfset requiredFields = listAppend(requiredFields,theEntityType.uniqueKey)> <!--- this is the unique key to be set when matching --->

			<!--- get the fields that make up the matchname fields --->
			<cfloop list="#requiredFields#" index="fieldName">
				<cfset requiredFields = listAppend(requiredFields,getFieldsToPopulateMatchField(entityType=arguments.entityType,matchField=fieldname))>
			</cfloop>
		</cfloop>

		<cfreturn application.com.globalFunctions.RemoveListDuplicates(list=requiredFields)>
	</cffunction>


	<cffunction name="createMatchFieldColumns" access="public" output="false" hint="Add the columns to the table necessary for matching if they don't already exist">
		<cfargument name="tablename" type="string" required="true" hint="The name of the table to add the matching columns to. This will be the table holding data to be loaded into RW.">
		<cfargument name="entityType" type="string" required="true" hint="The entityType">

		<cfset var requiredFields = getRequiredMatchFields(entityType=arguments.entityType)>
		<cfset var theEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var getExistingMatchFields = "">
		<cfset var tempTable = left(arguments.tablename,1) eq "##"?true:false>
		<cfset var tempDBPrefix = tempTable?"tempdb..":"">

		<cfquery name="getExistingMatchFields">
			declare @fieldname varchar(100)
			declare @fieldType varchar(100)
			declare @fieldLength varchar(100)
			declare @sqlStatement varchar(max)

			declare missingFieldName_cursor cursor for
				select d.column_name,d.data_type,d.character_maximum_length from information_schema.columns d
					left join (#tempDBPrefix#sysColumns sc inner join #tempDBPrefix#sysObjects so on sc.id = so.id) on sc.name = d.column_name and so.name= <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar">
				where
					d.table_name= <cf_queryparam value="#theEntityType.tablename#" cfsqltype="cf_sql_varchar">
					and d.column_name  in ( <cf_queryparam value="#requiredFields#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
					and sc.name is null

			open missingFieldName_cursor
			fetch next from missingFieldName_cursor into @fieldname,@fieldType,@fieldLength
			while @@fetch_status = 0
			begin
				set @sqlStatement = 'alter table #arguments.tablename# add '+@fieldname+' '+@fieldType
				if @fieldLength is not null
					set @sqlStatement = @sqlStatement +'('+@fieldLength+')'
				exec(@sqlStatement)
				fetch next from missingFieldName_cursor into @fieldname,@fieldType,@fieldLength
			end
			close missingFieldName_cursor
			deallocate missingFieldName_cursor
		</cfquery>
	</cffunction>


	<cffunction access="public" name="getMatchFieldReplaceStrings" output="false" hint="Creates the match field replace string to replace special words and characters" returnType="struct">
		<cfargument name="matchField" type="string" required="true">

		<cfset var replaceStatement = "">
		<cfset var replaceValueStatement = "">
		<cfset var returnVar = structNew()>
		<cfset var GetReplaceStrings = "">
		<cfset var localReplaceString= "">
		<cfset var localReplaceWithString = "">

		<!--- trailing spaces vital for substitution words rather than characters
		order important to replace special characters before substituting words
		fudged to add leading and trailing spaces for words (len > 2) rather than chars to ensure only complete words replaced--->

		<cfquery name="GetReplaceStrings">
			SELECT replaceString = replace(case when len(stringToReplace) <=2 then rtrim(ltrim(stringToReplace)) else ' ' + rtrim(ltrim(stringToReplace)) + ' ' end, '''',''''''),
			replaceWithString = case when len(stringToReplace) <=2 and len(replacementString)>0 then rtrim(ltrim(replacementString)) else ' ' + rtrim(ltrim(replacementString)) + ' ' end
			FROM matchStringReplacement
			where matchType is null
				or matchType = <cf_queryparam value="#(arguments.matchField eq 'matchAddress'?'address':'name')#" cfsqltype="cf_sql_varchar">
			order by len(stringToReplace) DESC,stringToReplace					<!--- 2013-12-20 PPB Case 438460 added DESC to make longer strings take priority --->
		</cfquery>

		<cfloop query="getReplaceStrings">
			<cfset localReplaceString = getReplaceStrings.replaceString>
			<cfset localReplaceWithString = getReplaceStrings.replaceWithString>
			<cfset replaceStatement = replaceStatement & "REPLACE(">
			<cfset replaceValueStatement = replaceValueStatement & ",**" & localReplaceString & "**,**" & localReplaceWithString & "**)">
		</cfloop>

		<cfset returnVar.replaceValueStatement = replaceValueStatement>
		<cfset returnVar.replaceStatement = replaceStatement>

		<cfreturn returnVar>
	</cffunction>


	<cffunction name="getSqlMatchNameUpdateInfo" access="private" output="false" returnType="struct" hint="Returns the update clause for a mtachname field for a given entity as well as the names of the matchname fields">
		<cfargument name="entityType" type="string" required="true">

		<cfset var entityMatchFields = getEntityMatchFields(entityType=arguments.entityType)>
		<cfset var updateClause = "">
		<cfset var field = "">
		<cfset var matchFieldList = "">
		<cfset var customMatchNameSql = application.com.settings.getSetting("matching.matchname.#entityType#")>
		<cfset var matchNameSql = "">

		<cfif structCount(entityMatchFields)>
			<cfsavecontent variable="updateClause">
				<cfoutput>
					<cfloop list="#entityMatchFields.standard#" index="field">
						<!--- we are only interested in updating matchSitename, matchAddress and also matchname --->
						<cfif listFindNoCase("matchname,matchAddress",field)>
							<cfif field eq "matchname" and customMatchNameSql neq "">
								<cfset matchNameSql = customMatchNameSql>
							<cfelse>
								<cfset var matchFieldReplaceString = getMatchFieldReplaceStrings(matchField=field)>
								<cfset var replaceValueStatement = replace(matchFieldReplaceString.replaceValueStatement,"**","'","ALL")>
								<cfset var matchNameFields = getFieldsToPopulateMatchField(entityType=arguments.entityType,matchField=field)>
								<cfset var matchnameString = "">
								<cfset var firstRun = true>

								<!--- matchnameString = "isNull(address1,'') +' #application.delim1# '+isNull(address2,'') +' #application.delim1# '+ isNull(address3,'')" --->
								<cfloop list="#matchNameFields#" index="fieldname">
									<cfif not firstRun>
										<cfset matchnameString = matchnameString & "+' #application.delim1# ' +">
									</cfif>
									<cfset matchnameString = matchnameString & "isNull(#fieldname#,'')">
									<cfset firstRun = false>
								</cfloop>

								<cfset matchNameSql = "substring(RTRIM(REPLACE(#matchFieldReplaceString.replaceStatement#(N' ' + REPLACE(#matchnameString#,'.',' ') + ' ')#preserveSingleQuotes(replaceValueStatement)#,' ','')),1,500)">
							</cfif>

							<cfset matchFieldList = listAppend(matchFieldList,field)>
							<cfif listLen(matchFieldList) neq 1>,</cfif>#field# = #preserveSingleQuotes(matchNameSql)#
						</cfif>
					</cfloop>
				</cfoutput>
			</cfsavecontent>
		</cfif>

		<cfset var result = {updateClause=updateClause,matchFieldList=matchFieldList}>
		<cfreturn result>
	</cffunction>


	<cffunction name="updateMatchFields" access="public" output="false" hint="Function to update the sanitised match fields used in matching on a given table">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="whereClause" type="string" default="" hint="A where clause to target specific records">
		<cfargument name="setNullMatchFieldsOnly" type="boolean" default="true" hint="Normally true, but in the case of matching criteria changing, we need to be able to reset all records">
		<cfargument name="entityID" type="numeric" required="false">

		<cfset var sqlMatchUpdate = getSqlMatchNameUpdateInfo(entityType=arguments.entityType)>
		<cfset var updateClause = sqlMatchUpdate.updateClause>

		<cfif trim(updateClause) neq "">
			<cfset var updateMatchFieldsForTable = "">
			<cfset var field = "">
			<cfset var firstRun = true>

			<cftry>
				<cfquery name="updateMatchFieldsForTable">
					<!--- if doing a large operation like resetting the matchfields, then do the update in batches --->
					<cfif not arguments.setNullMatchFieldsOnly>
					declare @x int = 1, @total int=0
					declare @message varchar(100)
					set rowCount 200000
					while (@x!=0)
					begin
					</cfif>

						update #arguments.tablename#
						set
							#preserveSingleQuotes(updateClause)#
						where
							<cfif arguments.whereClause neq "">#preserveSingleQuotes(arguments.whereClause)#<cfelse>1=1</cfif>
							<cfif structKeyExists(arguments,"entityID")>and #application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey# = <cf_queryparam value="#arguments.entityID#" cfsqltype="cf_sql_integer"></cfif>
							<cfif listLen(sqlMatchUpdate.matchFieldList) and arguments.setNullMatchFieldsOnly>
								and (
								<cfloop list="#sqlMatchUpdate.matchFieldList#" index="field">
									<cfif not firstRun>or</cfif> isNull(ltrim(rtrim(#field#)),'') = ''
									<cfset firstRun = false>
								</cfloop>
								)
							</cfif>

					<cfif not arguments.setNullMatchFieldsOnly>
						set @x = @@rowcount
						set @total = @total+@x
						set @message = cast(@total as varchar)+' rows updated in total ' + convert(varchar, getdate(),9)
						RAISERROR (@message, 10, 1) WITH NOWAIT
					end

					set rowCount 0
					</cfif>
				</cfquery>

				<cfcatch type="database">
					<cfquery name="local.resetRowCount">
						set rowCount 0
					</cfquery>

					<cfrethrow>
				</cfcatch>
			</cftry>
		</cfif>

	</cffunction>


	<cffunction name="getFirstRowOfData" access="private" output="false" hint="Returns top record of the incoming table so that we can determine some simple things about the incoming table" returnType="query">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true"> <!--- we're assuming that if this is a person record that a locationID exists in the incoming table --->

		<cfquery name="local.getTopRowQry">
			select top 1 d.* <cfif arguments.entityType eq "person">,l.accountTypeID</cfif> from #arguments.tablename# d
				<cfif arguments.entityType eq "person">inner join location l on l.locationID = d.locationID</cfif>
		</cfquery>

		<cfreturn local.getTopRowQry>
	</cffunction>


	<!--- <cffunction name="getAdminFieldStruct" access="public" output="false" hint="Returns a struct containing the fields which hold the accountType" returnType="struct">
		<cfreturn {person="accountTypeID",location="accountTypeId",organisation="organisationTypeID"}>
	</cffunction> --->


	<cffunction name="isDataAdminData" access="private" output="false" returnType="boolean" hint="Works out whether the incoming data is admin data or not as the matching rules slightly differ for admin companies.">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">

		<cfset var tableData = getFirstRowOfData(arguments.tablename,arguments.entityType)>
		<cfset var adminFieldStruct = {person="accountTypeID",location="accountTypeId",organisation="organisationTypeID"}>
		<!--- trying to work out whether the incoming data relates to an Admin company. Look at the top row. There is a BIG assumption that admin company data is not mixed in with other types of company data--->
		<cfreturn structKeyExists(adminFieldStruct,arguments.entityType) and listFindNoCase(tableData.columnList,adminFieldStruct[arguments.entityType]) and tableData[adminFieldStruct[arguments.entityType]][1] eq application.organisationType.AdminCompany.organisationTypeID?true:false>
	</cffunction>


	<cffunction name="getMatchingSql" access="private" output="false" hint="Returns the sql used to match records" returnType="string">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="uniqueIDColumnName" type="string" default="matchID" hint="The name of the column on the incoming table that holds the unique row identifier.">
		<cfargument name="matchCondition" type="struct" default="#structNew()#" hint="A structure that can hold a condition, which if met, excludes or includes a potential record from being matched. For example, remove records that already have a SFID from matching.">
		<cfargument name="additionalMatchColumns" type="string" default="" hint="Bit of a hack at the moment, but need to match by accountType/organisationType as well. This list is appended to standard match list. So, using this method until that is sorted. Used when matching POL records converted from leads. Only want to map end customers">

		<cfset var theEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var matchSql = "">
		<cfset var matchMethodType = "">
		<cfset var entityMatchFields = getEntityMatchFields(entityType=arguments.entityType)>
		<cfset var matchSortOrder = 0>
		<cfset var field = "">
		<cfset var isPOLTable = listFindNoCase("location,person,organisation",theEntityType.tablename)>
		<!--- <cfset var adminFieldStruct = getAdminFieldStruct()> --->
		<cfset var isAdminCompany = isPOLTable?isDataAdminData(tablename=arguments.tablename,entityType=theEntityType.tablename):false>

		<cfsavecontent variable="matchSql">
			<cfoutput>
				<!---
				loop over list to get the sort order correct....
				--->
				<cfloop list="lead,crmID,standard" index="matchMethodType">
					<cfif structKeyExists(entityMatchFields,matchMethodType)>
						<cfset matchSortOrder++>
						<cfif matchSortOrder gt 1>union</cfif>
						select t.#arguments.uniqueIDColumnName#, d.#theEntityType.uniqueKey#,#matchSortOrder# as matchMethodOrder
						from #arguments.tablename# t
							inner join #theEntityType.tablename# d
							on (
								<cfset var listCount = 0>
								<cfset var fieldsToMatchOn = entityMatchFields[matchMethodType]>
								<cfif arguments.additionalMatchColumns neq "">
									<cfset fieldsToMatchOn = listAppend(fieldsToMatchOn,arguments.additionalMatchColumns)>
								</cfif>
								<cfset var listLength = listLen(fieldsToMatchOn)>
								<cfloop list="#fieldsToMatchOn#" index="field">
									<cfset listCount++>
									<cfset var origNullValue="">
									<cfset var matchOnNullvalue = "">

									<!--- we don't want to match on null convertedFromLeadId/crmID values... so using neg numbers to ensure this doesn't happen --->
									<cfif listFindNoCase("lead,crmId",matchMethodType)>
										<cfset origNullValue="-1">
										<cfset matchOnNullvalue = "-2">
									</cfif>
									isNull(rtrim(ltrim(t.#field#)),'#origNullValue#') = isNull(rtrim(ltrim(d.#field#)),'#matchOnNullvalue#')
									<cfif listCount lt listLength> and </cfif>
								</cfloop>
								<!--- JIRA PROD2016-3310 - if the incoming record is an admin record and the client has NOT specified the accountType to match on, then ensure that we match only admin with admin. The assumption here
									is that Admin companies do not come in the same dataload/table as other companies but are only entered manually through the UI. So we look at the data in the first row to determine whether it's an admin record --->
								<!--- <cfif matchMethodType eq "standard" and isAdminCompany and not listFindNoCase(entityMatchFields.standard,adminFieldStruct[theEntityType.tablename])>
										and d.#adminFieldStruct[theEntityType.tablename]# = #(theEntitytype.tablename eq "person"?"l":"t")#.#adminFieldStruct[theEntityType.tablename]#
								</cfif> --->
							)
							<cfif theEntityType.tablename eq "person">inner join location l on l.locationID = d.locationID</cfif>
							<!--- NJH 2016/11/21 JIRA PROD2016-2745 don't match on deleted POL records --->
							<cfif isPOLTable>
								left join vbooleanFlagData del on del.entityID = d.#theEntityType.uniqueKey# and del.flagTextId = <cf_queryparam value="delete#theEntityType.tablename#" cfsqltype="cf_sql_varchar">
							</cfif>
							<cfif structKeyExists(arguments.matchCondition,"join")>
								#arguments.matchCondition.join#
							</cfif>
						where 1=1
						<cfif isPOLTable>
							<!--- don't match on admin companies ONLY if incoming data is not admin data. Otherwise we do want to match ONLY on admin data --->
							and isNull(
							<cfif theEntityType.tablename eq "organisation">d.organisationTypeID
							<cfelseif theEntityType.tablename eq "location">d.accountTypeID
							<cfelse>l.accountTypeID</cfif>
							,1) <cfif not isAdminCompany>!</cfif>= #application.organisationType.AdminCompany.organisationTypeID#

							and del.entityID is null /*don't match on deleted entities*/
							<cfif not application.com.settings.getSetting("matching.matchInactiveRecords")>
							and isNull(d.active,1) = 1
							</cfif>
						</cfif>
						<cfif structKeyExists(arguments.matchCondition,"whereClause") and arguments.matchCondition.whereClause neq "">
							and (#arguments.matchCondition.whereClause#)
						</cfif>
					</cfif>
				</cfloop>
				) as x
			</cfoutput>
		</cfsavecontent>

		<cfreturn matchSql>
	</cffunction>


	<cffunction name="matchRecords" access="public" output="false" hint="Matches records on the given table">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="uniqueIDColumnName" type="string" required="true" hint="The name of the column on the incoming table that holds the unique row identifier">
		<cfargument name="allowDuplicateMatchedRecords" type="boolean" default="false" hint="If we allow incoming records with the same match criteria to match to the same record. Would we ever want this?">
		<cfargument name="updateIDColumnName" type="string" default="#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey#" hint="We may want to update the table with a remote key, as in a salesforceID">
		<cfargument name="updateIDFromColumnName" type="string" default="#application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType).uniqueKey#" hint="We may want to update the table from a remote key, as in a salesforceID">
		<cfargument name="updateTableName" type="string" default="#arguments.tablename#" hint="The name of the table to update. May be different than arguments.tablename if we're updating a remote key on a RW entity table from another table">
		<cfargument name="additionalMatchColumns" type="string" default="" hint="Bit of a hack at the moment, but need to match by accountType/organisationType as well. So, using this method until that is sorted.">
		<cfargument name="matchCondition" type="struct" default="#structNew()#" hint="A structure that can hold a condition, which if met, excludes a potential record from being matched. For example, remove records that already have a SFID from matching.">


		<cfset var theEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>
		<cfset var setUniqueKey = "">
		<cfset var matchSqlString = getMatchingSql(argumentCollection=arguments)>

		<cfquery name="setUniqueKey">
			update #arguments.updateTableName#
				set #arguments.updateIDColumnName# = matchedRecords.#arguments.updateIDColumnName eq theEntityType.uniqueKey?theEntityType.uniqueKey:arguments.updateIDFromColumnName#
				<cfif arguments.updateTableName eq theEntityType.tablename>
					,lastUpdatedBy = #request.relayCurrentUser.userGroupID#
					,lastUpdatedByPerson = #request.relayCurrentUser.personID#
				</cfif>
			from
				#arguments.tablename# t inner join
					(
					select #theEntityType.uniqueKey#,
						#arguments.uniqueIDColumnName#,
						row_number() over (partition by #arguments.uniqueIDColumnName# order by matchMethodOrder) as rowNumber
					from
					(
						#preserveSingleQuotes(matchSqlString)#
					) as matchedRecords
				on matchedRecords.#arguments.uniqueIDColumnName# = t.#arguments.uniqueIDColumnName# and rowNumber = 1
				<cfif arguments.tablename neq theEntityType.tablename and arguments.updateTableName eq theEntityType.tablename>
				inner join #theEntityType.tablename# e on e.#theEntityType.uniqueKey# = matchedRecords.#theEntityType.uniqueKey#
				</cfif>
			where
				t.#theEntityType.uniqueKey# is null

			<!--- if we're setting the match key on a non-entity table and we don't want more than 1 record matching to the same RW record, set any subsequent matches back to null. Effectively, an incoming table could contain
			duplicates which would meant that they would/could match to the same RW record. But we would only want one to match to the existing record. The other record would be created as a duplicate in RW. This is currently
			the situation for incoming SF records. --->
			<cfif not arguments.allowDuplicateMatchedRecords and arguments.tablename eq arguments.updateTableName and arguments.tablename neq theEntityType.tablename>
				update #arguments.tablename#
					set #theEntityType.uniqueKey# = null
				from
					#arguments.tablename# t
					inner join (
						select #theEntityType.uniqueKey#, min(#arguments.uniqueIDColumnName#) as #arguments.uniqueIDColumnName#
						from #arguments.tablename#
						group by #theEntityType.uniqueKey#
						having count(#theEntityType.uniqueKey#) > 1
					) d on d.#theEntityType.uniqueKey# = t.#theEntityType.uniqueKey# and d.#arguments.uniqueIDColumnName# != t.#arguments.uniqueIDColumnName#
			</cfif>
		</cfquery>

	</cffunction>


	<cffunction name="getMatches" access="public" output="false" hint="Returns a query with the list of matches for the given input">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="matchCondition" type="struct" default="#structNew()#" hint="A structure that can hold a condition, which if met, excludes a potential record from being matched. For example, remove records that already have a SFID from matching.">

		<cfset var getMatchesQry = "">
		<cfset var matchSqlString = getMatchingSql(tablename=arguments.tablename,entityType=arguments.entityType,matchCondition=arguments.matchCondition)>
		<cfset var theEntityType = application.com.relayEntity.getEntityType(entityTypeID=arguments.entityType)>

		<cfquery name="getMatchesQry">
			select #theEntityType.uniqueKey# from
				(
					select #theEntityType.uniqueKey#,
					row_number() over (partition by #theEntityType.uniqueKey# order by matchMethodOrder) as rowNumber
				from
				(
					#preserveSingleQuotes(matchSqlString)#
				) as matchedRecords
		</cfquery>

		<cfreturn getMatchesQry>
	</cffunction>


	<cffunction name="getMatchesForEntityDetails" access="public" output="false" hint="Returns matches for a given record. Used for single record pre-inserts.">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="matchCondition" type="struct" default="#structNew()#" hint="A structure that can hold a condition, which if met, excludes a potential record from being matched. For example, remove records that already have a SFID from matching.">

		<cfset var tempTableName = "####TempMatchName_#replace(rand(),".","")#">
		<cfset var insertIntoTable = "">

		<!--- put data into temp table --->
		<cfquery name="insertIntoTable">
			select 1 as matchID <!--- just a unique ID value for this record. --->
				<cfloop collection = "#arguments#" item="field">
					<cfset value = arguments[field]>
					<cfif isdefined("value") AND isSimpleValue(value)>
						,<cf_queryparam value="#value#" cfsqltype="cf_sql_varchar"> as #field#
					</cfif>
				</cfloop>
			into
			#tempTableName#
		</cfquery>

		<cfset prepareDataForMatching(tablename=tempTableName,entityType=arguments.entityType)>
		<cfset var matches = getMatches(tablename = tempTableName,entityType=arguments.entityType,matchCondition=arguments.matchCondition)>

		<cfset var dropTempTable = "">
		<!--- <cfquery name="dropTempTable">
			drop table #tempTableName#
		</cfquery> --->

		<cfreturn matches>
	</cffunction>


	<cffunction name="prepareDataForMatching" access="public" output="false" hint="Calls some functions to get the data in a state where matching can be done and/or matches found">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">

		<!--- first add any matching columns on the incoming table --->
		<cfset createMatchFieldColumns(tablename=arguments.tableName,entityType=arguments.entityType)>

		<!--- update the match fields on the newly created columns --->
		<cfset updateMatchFields(tablename=arguments.tableName,entityType=arguments.entityType)>

		<!--- update the match fields on the base tables --->
		<cfset updateMatchFields(tablename=arguments.entityType,entityType=arguments.entityType)>
	</cffunction>


	<cffunction name="matchEntityRecords" access="public" output="false" hint="A wrapper function that handles all the steps necessary for matching">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="uniqueIDColumnName" type="string" required="true" hint="The name of the column on the incoming table that holds the unique row identifier">
		<cfargument name="additionalMatchColumns" type="string" default="" hint="Bit of a hack at the moment, but need to match by accountType/organisationType as well. So, using this method until that is sorted. Currently used in leads so that we only match end customers">
		<cfargument name="matchCondition" type="struct" default="#structNew()#">

		<cfset prepareDataForMatching(tablename=arguments.tableName,entityType=arguments.entityType)>

		<!--- perform the matching --->
		<cfset matchRecords(argumentCollection=arguments)>

	</cffunction>
</cfcomponent>