<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author PKP

file: relayQuiz.cfc

DATE : 2008-03-27

A place to put all quiz related functions

Amendments
2008-10-13	NYB		Sophos-1. removed DescriptionPhraseTextID from throughout code as this is no longer a column in the QuizDetail table
					Sophos-2. added QuizDetailID arg & where clause to getQuizzes
2008/10/21 NYB		Sophos for relaytags
2008/11/03 NYB		Sophos - added ModuleID to getQuizInfo query in getQuizInfo
2009/02/25  WAB		Sophos - Negative and Fractional Scores
2009-03-31 		NYB		Sophos bugzilla 2011 - changed updateQuiz to stop the system throwing an error when the quiz is changed between attempts
2009-04-21 		NYB 	Sophos bugzilla 2052 - stop Quiz throwing an error when a question has been removed from a quiz between a users attempts (failing on 2nd attempt)
2009-06-12		NYB		LHID 2350
2009-07-29 		NYB 	Sophos LHID2462 - disclude inactive profiles from being added to QuizDetail
added a function flagGroupUpdateTask that's run after profileManager/flagGroupUpdateTask.cfm if EntityType.table=QuizTaken
2009-08-03 		NYB 	SNY047  Translation of quiz names
2009-09-24 		NYB 	LHID2668
2009-09-27 		NYB 	LHID2667
2009/10/01		NJH		LID 2717
2009-10-08		NYB		LHID2739 - made a number of improvements to the code - updated for version control
2010/04/10 		WAB/SSS 	LID 3289 changes to updateQuizDetailOnProfileChange
2010-09-06		NYB		LHIDs: 3828 (FastHosts) & 3806 (Sony1) - changed quiztaken->Started to be based on db time not server time
2010/09/20		NAS		LID4110: eLearning - Bulk loading questions and attributes error
2010/09/30 		NAS 	LID4189: eLearning - Quiz question pool to display in alpha order - added SortOrder Param
2011-09-16 		NYB		P-SNY106 - introduction of stringencyMode - if of type Strict then various behaviours will be altered
							added functions updateQuestionsViewed & getUnansweredQuestions
2011/10/28		NJH		LID 8027 - when setting the started and completed time for a quiz, use 'getDate' so that we get an accurate time of how long the quiz took to complete. Having
							a mixture of request.request time, now() and getDate will result in inaccurate times.
2012/02/13 		PPB 	Case 426433 defend against questions being "" on quiztaken table
2012/04/30		IH		Case 427916 changed cfoutput to cfquery and scoped variables inside the loop
2012/07/12		IH		Case 428813 Add sorting capability to Question Pools
2012/07/17		Englex	P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR018 Elearning PDF Certificate
2012/07/24		IH		Case 429695	Check if arguments.sortOrder is not blank in getQuizQuestionPoolList
2012-08-07		STCR	P-REL109 Removed updateQuizDetailOnProfileChange (which related to LID3153 and LID2462), since QuizTaken entity Flags are disabled in the 2012 Relay Quiz structure
2012-08-13		STCR	P-REL109 convert getUnAnsweredQuestions() function to new data structure.
2012-08-15		STCR	P-REL109 convert getIncorrectQuestions() function to new data structure.
2012-08-22		STCR	CASE 430124 Matching Answer scoring; include matching answers within max possible score calculation.
2021-09-04		STCR	CASE 430416 Fix Answer scoring rounding issue due to cf_queryparam data type
2012-09-17		STCR	CASE 430523 SCORM able to populate QuizTaken.PassMark and QuizTaken.QuizStatus to help with reporting changes.
2012-09-17		STCR	Populate QuizTakenQuestion.QuestionTextGiven and QuizTakenAnswer.AnswerTextGiven with translations for current user.
2012-11-14 		WAB/NJH CASE 431977 added CRUD to updateQuizScore()
2013-02-22		PPB		Case 433209 show/edit AnswerRef against each answer
2013-03-21 		NYB 	Case 434096 added qd. to passmark
2013-04-12		STCR	CASE 432328 Question SortOrder
2013-04-23 		NYB 	Case 434166 replaced QuizDetail.Passmark with Quiztaken.passmark
2013-05-10		STCR	CASE 434166 Ensure PassMark is populated on QuizTaken record
2013-09-03 		PPB 	Case 436464 we no longer need to maintain the Questions column in QuizTaken because we now maintain QuizTakenQuestion table instead
2013-09-28		WAB		CASE 437228 Fix to getQuiz() - added passMark alias
2013-11-07 		NYB 	Case 437785 removed 'if passmark exists' and just look it up if not passed in
2013-11-19 		NYB 	Case 436793 added various lines
2014-01-31 		NYB 	Case 438543 changed getQuiz to update questions if quiz has been changed in between attempts
2014-02-07 		PPB 	Case 438828 simplified a QOQ to avoid error
2014-02-11 		NYB 	Case 438543 additional changes
2015-06-26      DCC     SCORM quizzes have no quizdetail record get course title and passdate new function getCourseSCORMbits and changes to getQuizCertificationMergeFields
2016-05-18		WAB		During BF-714 set validValues="true" on getAnswerList() 
2016-05-18		WAB 	PROD2016-876/PROD2016-1279 XSRF Form Tampering QuizSetup.cfm.  Require some arguments to be encrypted
2016-06-28      DAN     Case 449661 - include number of questions when getting question pools to account for the actual number requested

 --->

 <cfcomponent displayname="Various Quiz Related functions" hint="Date Component">

	<!--- get the information about this quiz --->
	<cffunction access="public" name="getQuizInfo" returnType="Query">
		<cfargument name="QuizDetailID" type="numeric" required="No">

		<cfscript>
			var getQuizInfo = "";
			var getQuizQuestions = "";
		</cfscript>
		<!--- NYB 2008/11/03 P-SOP009 - added ModuleID to query --->
		<CFQUERY NAME="getQuizInfo" datasource="#application.siteDataSource#">
			SELECT qd.QuizDetailID,
				'phr_title_TrngModule_' + convert(VarChar,tm.moduleID) AS AssociatedModule,
				'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) as QuizName,
				PossibleQuestions, NumToChoose, TimeAllowed, QuestionsPerPage,
				NumberOfAttemptsAllowed, Header_defaultTranslation, footer_defaultTranslation, qd.Created,
				qd.CreatedBy, QuizGroupID, qd.Passmark, isnull(tm.ModuleID,0) as [ModuleID], tm.passDecidedBy    <!--- 2013-03-21 NYB Case 434096 wrapped inNull around ModuleID: --->
			FROM QuizDetail qd
				left outer join trngModule tm on qd.quizDetailID = tm.quizDetailID
			WHERE 1=1
			<cfif structKeyExists(arguments,"QuizDetailID")>
				and qd.QuizDetailID = #QuizDetailID#
			</cfif>
				and qd.active=1
		</CFQUERY>

		<cfloop query="getQuizInfo">
			<cfif isNumeric(getQuizInfo.QuizDetailID)>
				<cfquery name="getQuizQuestions" datasource="#application.siteDataSource#">
					select qq.QuestionID from QuizDetailQuestionPool qdqp
					inner join QuizQuestion qq with(nolock) on qdqp.QuestionPoolID = qq.QuestionPoolID
					where QuizDetailID =  <cf_queryparam value="#getQuizInfo.QuizDetailID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset getQuizInfo.PossibleQuestions = ValueList(getQuizQuestions.QuestionID) />
			</cfif>
			<!--- STCR TODO If this is ever really called without arguments.QuizDetailID then we should handle PossibleQuestions more efficiently for multiple QuizDetail rows --->
		</cfloop>

		<cfreturn getQuizInfo>
	</cffunction>

	<!--- get the information about specifc quizzes --->
	<!--- 2008-10-13 NYB added QuizDetailID arg & where clause --->
	<cffunction access="public" name="getQuizzes" returnType="Query">
		<!--- 2008/10/21 NYB - Sophos Issue - changed QuizDetailID type from numeric to string to allow multiple entries --->
		<cfargument name="QuizDetailID" type="string" required="No">
		<!--- 2008/10/21 NYB - Sophos - added: --->
		<cfargument name="ModuleID" type="string" required="No">

		<cfscript>
			var getQuizzes = "";
			var getQuizQuestions = "";
		</cfscript>

		<CFQUERY NAME="getQuizzes" datasource="#application.siteDataSource#">
			SELECT qd.QuizDetailID,
				'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) as QuizName,
				PossibleQuestions, NumToChoose, TimeAllowed, QuestionsPerPage,
				NumberOfAttemptsAllowed, qd.Created, qd.CreatedBy, QuizGroupID, qd.Passmark
			FROM QuizDetail qd
			<cfif structKeyExists(arguments,"ModuleID")>
				left outer join trngModule tm on tm.quizDetailID = qd.quizDetailID
			</cfif>
			WHERE 1=1
			<cfif structKeyExists(arguments,"ModuleID")>
				AND tm.ModuleID  in ( <cf_queryparam value="#ModuleID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
			<cfif structKeyExists(arguments,"QuizDetailID")>
				AND qd.QuizDetailID  in ( <cf_queryparam value="#QuizDetailID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfif>
		</CFQUERY>
		<cfloop query="getQuizzes">
			<cfif isNumeric(getQuizzes.QuizDetailID)>
				<cfquery name="getQuizQuestions" datasource="#application.siteDataSource#">
					select qq.QuestionID from QuizDetailQuestionPool qdqp
					inner join QuizQuestion qq with(nolock) on qdqp.QuestionPoolID = qq.QuestionPoolID
					where QuizDetailID =  <cf_queryparam value="#getQuizzes.QuizDetailID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset getQuizzes.PossibleQuestions = ValueList(getQuizQuestions.QuestionID) />
			</cfif>
			<!--- STCR TODO If this is ever really called without arguments.QuizDetailID then we should handle PossibleQuestions more efficiently for multiple QuizDetail rows --->
		</cfloop>
		<cfreturn getQuizzes>
	</cffunction>

 	<!--- check to see if user has attempted the quiz already --->
	<cffunction access="public" name="checkQuiz" returnType="Query">
		<cfargument name="QuizDetailID" type="numeric" required="Yes">
		<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfargument name="userModuleProgressID" type="numeric" required="false">

		<cfscript>
			var checkQuiz = "";
			var getQuizQuestions = "";
		</cfscript>

		<!--- 2011-09-16 NYB P-SNY106 - added qt.forceCompletion, qt.Questions to query --->
		<!--- 2013-03-21 NYB Case 434096 added qd. to passmark: --->
		<!--- 2013-04-23 NYB Case 434166 replaced qd.passmark with qt.passmark: --->
		<CFQUERY NAME="checkQuiz" datasource="#application.siteDataSource#">
			SELECT QuizTakenID, TimeLeft, Completed, score, qt.passmark, qt.forceCompletion, qt.Questions
			FROM QuizTaken qt with (noLock) inner join quizDetail qd with (noLock)
				on qt.quizDetailID = qd.quizDetailID
			WHERE PersonID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#">
			AND qt.quizDetailID = #QuizDetailID#
			<cfif structKeyExists(arguments,"userModuleProgressID")>
				and userModuleProgressID = #arguments.userModuleProgressID#
			</cfif>
			ORDER BY qt.Created DESC
		</CFQUERY><!--- STCR replace checkQuiz.Questions --->
		<cfloop query="checkQuiz">
			<cfif structKeyExists(arguments,"QuizDetailID") and isNumeric(checkQuiz.QuizTakenID)>
				<cfquery name="getQuizQuestions" datasource="#application.siteDataSource#">
					select QuestionID from QuizTakenQuestion where QuizTakenID =  <cf_queryparam value="#checkQuiz.QuizTakenID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset checkQuiz.Questions = ValueList(getQuizQuestions.QuestionID) />
			</cfif>
			<!--- STCR TODO handle more efficiently when multiple QuizTaken rows --->
		</cfloop>
		<cfreturn checkQuiz>
	</cffunction>

 	<!--- check to see if user has attempted the quiz already and update record--->
	<cffunction access="public" name="updateQuiz" >
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes">
	 	<cfargument name="stringencyMode" default="Standard"><!--- 2011-09-16 NYB P-SNY106 - added --->
		<cfargument name="OrderMethod" type="string" required="false" default="Random" hint="Alpha|Random|SortOrder. Question order used by portal relaytags. Alpha not currently supported." />  <!--- 2014-02-11 NYB Case 438543 - added --->

		<!--- 2013-03-21 NYB Case 434096 replaced ActualNumToChoose="" with =0: --->
		<cfscript>
			var updateQuiz = "";
			var qryQuizTakenDetails="";
			var argumentsStruct = structNew();
			var QuestionsVar = "";
			var QuestionsViewed = "";
			var i=0;
			var ActualNumToChoose = 0;
			var j=0;
			var ActualQs = "";
			var PossQs = "";
			var RandomQ = "";
			var NumPossQs = "";
			var numToChoose = "";
		</cfscript>

		<!--- 2014-02-11 NYB Case 438543 replaced with argumentcollection=arguments --->
		<cfset qryQuizTakenDetails = getQuiz(argumentcollection=arguments)>
		<!--- START:  NYB 2009-03-31 Sophos bugzilla 2011 - added to stop the system throwing an error when the quiz is changed between attempts--->
		<!--- START:  NYB 2009-04-21 Sophos bugzilla 2052 - stop Quiz throwing an error when questions have changed - but not their amount --->
		<!--- this section deletes flag data of questions that no longer exists in this quiz - keeps those that do --->
		<!--- START: 2014-02-11 NYB Case 438543 removed - redundant:
		<cfset QuestionsVar = qryQuizTakenDetails.Questions>
		--- END: 2014-02-11 NYB Case 438543 removed - redundant --->
		<cfset QuestionsViewed = qryQuizTakenDetails.QuestionsViewed><!--- 2011-09-16 NYB P-SNY106 - added --->
		<CFLOOP index="i" list="#qryQuizTakenDetails.Questions#">
			<cfif ListFind(qryQuizTakenDetails.PossibleQuestions,i) eq 0>
				<!--- START: NYB 2011-09-16 P-SNY106 reset lastQuestionViewed and firstPageQuestion is that flag gets deleted before their second attempt --->
				<cfif ListFind(QuestionsViewed,i) gt 0>
					<cfset QuestionsViewed = ListDeleteAt(QuestionsViewed,ListFind(QuestionsViewed,i))>
					<cfset updateQuestionsViewed(QuizTakenID=QuizTakenID,QuestionsViewed=QuestionsViewed)>
				</cfif>
				<!--- END: NYB 2011-09-16 P-SNY106 --->
				<!--- START: 2014-02-11 NYB Case 438543 removed - redundant:
				<cfset QuestionsVar = ListDeleteAt(QuestionsVar,ListFind(QuestionsVar,i))>
				--- END: 2014-02-11 NYB Case 438543 removed - redundant --->
			</cfif>
		</CFLOOP>
		<!--- END:  NYB 2009-04-21 --->

		<!--- NYB 2009-04-21 - till END:  NYB 2009-03-31 replaced qryQuizTakenDetails.Questions with QuestionsVar--->

		<!--- START: 2014-02-11 NYB Case 438543 removed - redundant:

		<!--- START:  NYB 2009-10-08 LHID2739 --->
		<cfset ActualNumToChoose = qryQuizTakenDetails.NumToChoose>
		<cfif ActualNumToChoose gt ListLen(qryQuizTakenDetails.PossibleQuestions)>
			<cfset ActualNumToChoose = ListLen(qryQuizTakenDetails.PossibleQuestions)>
		</cfif>
		<cfset ActualQs = QuestionsVar>
		<!--- END:  NYB 2009-10-08 LHID2739 --->

		<!--- NYB 2009-10-08 LHID2739 - replaced qryQuizTakenDetails.NumToChoose with ActualNumToChoose: --->
		<cfif ListLen(QuestionsVar) gt ActualNumToChoose>
			<cfset ActualQs = "">
			<CFLOOP index="i" from="1" to="#ActualNumToChoose#">
				<cfset ActualQs = ListAppend(ActualQs, ListGetAt(QuestionsVar,i))>
			</CFLOOP>
		<cfelseif ListLen(QuestionsVar) lt ActualNumToChoose>
			<!--- NYB 2009-10-08 LHID2739 - removed cfset ActualQs --->
			<!--- make a random selection of the possible questions - result is stored as ActualQs --->
			<cfset PossQs = qryQuizTakenDetails.PossibleQuestions>
			<CFLOOP index="i" list="#QuestionsVar#">
				<CFSET PossQs = ListDeleteAt(PossQs, ListFind(PossQs, i))>
			</CFLOOP>
			<cfset NumPossQs = ListLen(PossQs)>
			<!--- START:  NYB 2009-09-24 LHID2668 - REPLACED: ---
			<cfset NumToChoose = ActualNumToChoose - ListLen(QuestionsVar)>
			<CFLOOP CONDITION="ListLen(ActualQs) LT NumToChoose">
			!--- WITH:  NYB 2009-09-24 LHID2668 --->

			<!--- 2014-01-31 NYB Case 438543 replaced CFLOOP with a function --->
			<cfset ActualQs = application.com.globalfunctions.RandomSelectFromList(PossQs,ActualNumToChoose)>

			<!--- START:  NYB 2009-06-12 LHID 2350 - remove *1 ---
			<cfset ActualQs = "#QuestionsVar#,#ActualQs#">
			!--- END:  NYB 2009-06-12 LHID 2350 - *1 because question list is being passed with a "," at the start --->
		</cfif>
		--- END: 2014-02-11 NYB Case 438543 removed - redundant --->

		<!--- END:  NYB 2009-03-31 --->
		<!--- NYB 2010-09-06 LHIDs: 3828 (FastHosts) & 3806 (Sony1) - in query
			replaced "#CreateODBCDateTime(Now())#" with #CreateODBCDateTime(request.requestTime)#
			as timer.cfm uses db time
		--->
		<CFQUERY NAME="updateQuiz" datasource="#application.siteDataSource#">
			UPDATE QuizTaken SET Started = getDate()
			<!--- NYB 2009-10-08 LHID2739 - removed cfif around ActualQs --->
				<!--- , Questions =  <cf_queryparam value="#ActualQs#" CFSQLTYPE="CF_SQL_VARCHAR" > ---> <!--- 2013-09-03 PPB Case 436464 we no longer need to maintain the Questions column in QuizTaken because we now maintain QuizTakenQuestion table instead --->
				<cfif isNumeric(qryQuizTakenDetails.PassMark)>, PassMark =  <cf_queryparam value="#qryQuizTakenDetails.PassMark#" CFSQLTYPE="cf_sql_float" ></cfif><!--- STCR 2013-05-10 CASE 434166 --->
				<cfif arguments.stringencyMode eq "Strict">
					,forceCompletion=1
				</cfif>
			WHERE QuizTakenID = #QuizTakenID#
			<!--- 2014-02-11 NYB Case 438543 added a select query --->
			select started from QuizTaken WHERE QuizTakenID = #QuizTakenID#
		</CFQUERY>

		<!--- START: 2014-02-11 NYB Case 438543 added --->
		<cfset QuerySetCell(qryQuizTakenDetails,"started",updateQuiz.started)>
		<cfif arguments.stringencyMode eq "Strict">
			<cfset QuerySetCell(qryQuizTakenDetails,"forceCompletion",1)>
		</cfif>
		<!--- END: 2014-02-11 NYB Case 438543 --->

		<!--- NJH 2008/09/16 update the lastVisitedDate on the user module progress table --->
		<cfscript>
			//NYB 2009-03-31 Sophos bugzilla 2011 - moved to the above:
			//qryQuizTakenDetails = getQuiz(quizTakenID=arguments.QuizTakenID);
			argumentsStruct.frmLastVisited = now();
			application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=qryQuizTakenDetails.userModuleProgressID,argumentsStruct=argumentsStruct);
		</cfscript>

		<!--- 2014-02-11 NYB Case 438543 added --->
		<cfreturn qryQuizTakenDetails>

	</cffunction>

 	<!--- SWJ 3-Sep-08 eLearning 8.1 update a quizTaken record and set the quizTaken date --->
 	<!--- GCC 2010-05-19 changed Completed date to request.requesttime so that started and completed dates are both sourced form the DB server
	to stop the issue of quizzes being finished before they are started on environments where the web server clock is slower than the DB server clock --->
	<cffunction access="public" name="setQuizTakenCompleted" hint="Update a quizTaken record and set the quizTaken date to now.">
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes">
	 	<cfargument name="sendEmail" type="boolean" default="false"> <!--- NJH 2009/02/05 P-SNY047 - send email on quiz completion --->
	 	<cfargument name="passMark" type="numeric" required="false">
	 	<cfargument name="quizStatus" type="string" required="false">

		<cfscript>
			var setQuizTakenCompletedQry = "";
			var qryQuizTakenDetails = "";
			var quizPassed = false;
			var emailMergeStruct = StructNew();
			var moduleCompleteResult = StructNew();
			var rwEmailTextID = "";
			var quizEmailTextID = "";
			var getPersonTiedToQuiz = "";
			var pdfCertificateRequired = "";
			var pdfCertifications = queryNew("path");
			var argumentStruct = StructNew();    // 2013-11-19 NYB Case 436793 added
		</cfscript>

		<!--- 2013-04-23 NYB Case 434166 replaced qd.passmark with qt.passmark: --->
		<cfquery name="setQuizTakenCompletedQry" datasource="#application.siteDataSource#">
			update QuizTaken set
				Completed = getDate(),
				lastUpdated = getDate(),
				lastUpdatedBy = <cf_queryparam value="#request.relayCurrentUser.userGroupID#" CFSQLTYPE="CF_SQL_INTEGER">,
				lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER">,
				quizStatus = <cfif not structKeyExists(arguments,"quizStatus") or not listFindNoCase("pass,fail",arguments.quizStatus)>case when qd.passmark <= isNull(qt.score,0) then 'Pass' else 'Fail' end<cfelse><cf_queryparam value="#arguments.quizStatus#" CFSQLTYPE="CF_SQL_VARCHAR"></cfif>,
				passmark = <cfif not structKeyExists(arguments,"passmark")>isNull(qd.passmark,qt.passmark)<cfelse><cf_queryparam value="#arguments.passmark#" CFSQLTYPE="CF_SQL_FLOAT"></cfif>
			from quizTaken qt with (noLock)
				left join quizDetail qd with (noLock) on qt.quizDetailID = qd.quizDetailID
			where qt.QuizTakenID = <cf_queryparam value="#arguments.QuizTakenID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>

		<!--- NJH 2012/11/19 CASE 432093 - replaced getQuiz function call with query below, as quiz may not always have quiz detail (sco quizzes)  --->
		<!--- 2013-03-21 NYB Case 434096 changed inner join to a left join: --->
		<cfquery name="qryQuizTakenDetails" datasource="#application.siteDataSource#">
			<!--- START: 2013-11-19 NYB Case 436793 added isnull around qt.complete, and added attempts --->
			select qt.quizTakenID, qt.passmark, qt.score, isnull(qt.completed,'') as completed, qt.quizStatus, 'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) as QuizName, qt.userModuleProgressID, m.passDecidedBy,
				isNull(qd.timeAllowed,0) as timeAllowed, isNull(qt.TimeLeft,0) as timeLeft, qt.personid,isnull(qd.NumberOfAttemptsAllowed,0) as NumberOfAttemptsAllowed,
				isnull(tump.quizAttempts,0) as quizAttempts
			<!--- END: 2013-11-19 NYB Case 436793 --->
			from QuizTaken qt
				left join trngUserModuleProgress tump on qt.userModuleProgressID = tump.userModuleProgressID
				left join trngModule m on m.moduleID = tump.moduleID
				left join quizDetail qd on qt.QuizDetailID = qd.QuizDetailID
			where qt.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#" />
		</cfquery>


		<cfquery name="pdfCertificateRequired" datasource="#application.siteDataSource#">
			select 1 from booleanFlagData with (noLock) where flagID=<cf_queryparam value="#application.com.flag.getFlagStructure(flagID='PDFCertificateRequired').flagID#" CFSQLTYPE="CF_SQL_INTEGER">
				and entityID = <cf_queryparam value="#request.relaycurrentuser.organisationID#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>

		<!--- NJH 2009/02/05 P-SNY047 --->
		<!--- 2013-03-21 NYB Case 434096 removed: ---
		<cfquery name="getPersonTiedToQuiz" datasource="#application.siteDataSource#">
			select personID from trngUserModuleProgress with (noLock) where userModuleProgressID =  <cf_queryparam value="#qryQuizTakenDetails.userModuleProgressID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		!--- 2013-03-21 NYB Case 434096 --->

		<!--- NJH 2009/02/10 P-SNY047 --->
		<cfscript>
			if (qryQuizTakenDetails.quizStatus eq "Pass") {
				quizPassed=true;
			}

			emailMergeStruct.quizTakenID = arguments.quizTakenID;
			emailMergeStruct.passMark = qryQuizTakenDetails.passMark;
			emailMergeStruct.score = qryQuizTakenDetails.score;
			emailMergeStruct.passDate = qryQuizTakenDetails.completed;
			emailMergeStruct.quizName=application.com.relayTranslations.translatePhrase(phrase=qryQuizTakenDetails.QuizName); //LID 5166 NJH 2010/12/22 - pretranslate quizname
		</cfscript>

		<!--- 2013-03-21 NYB Case 434096 replaced getPersonTiedToQuiz.personID with qryQuizTakenDetails.personID: --->
		<cfscript>
			//if (quizPassed) {
				if (arguments.sendEmail) {
					if (quizPassed) {
						rwEmailTextID = "elearningQuizPassedRewards"; // setting this variable will trigger a rewards email getting sent if the right conditions are met
						quizEmailTextID = "elearningQuizPassed";
					} else {
						quizEmailTextID = "elearningQuizFailed";
					}
				}

				if (pdfCertificateRequired.recordCount){
					structAppend(emailMergeStruct,getQuizCertificationMergeFields(quizTakenID=arguments.QuizTakenID,personID=qryQuizTakenDetails.personID),false);
					pdfCertifications = createModuleCertificate(mergeStruct=emailMergeStruct);
				}

				//2013-11-19 NYB Case 436793 added:
				argumentStruct = {userModuleProgressID=qryQuizTakenDetails.userModuleProgressID,rwNotificationEmailTextID=rwEmailTextID,rwNotificationEmailMergeStruct=emailMergeStruct,qryQuizTaken=qryQuizTakenDetails,notificationEmailTextID=quizEmailTextID,attachments=pdfCertifications,status="#qryQuizTakenDetails.quizStatus#ed"};

				//START: 2013-11-19 NYB Case 436793 added additional if's, replaced separate arguments to above struct
				if (qryQuizTakenDetails.userModuleProgressID neq 0 and (quizPassed or (qryQuizTakenDetails.completed neq "" and qryQuizTakenDetails.NumberOfAttemptsAllowed gte qryQuizTakenDetails.quizAttempts))) {
					moduleCompleteResult = application.com.relayElearning.setModuleComplete(argumentcollection=argumentStruct);
 				} else {
 					application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=qryQuizTakenDetails.userModuleProgressID,argumentsStruct=argumentStruct);
					if (arguments.sendEmail) {
						application.com.email.sendEmail(personID=qryQuizTakenDetails.personID,emailTextID=quizEmailTextID,mergeStruct=emailMergeStruct);
					}
					//END: 2013-11-19 NYB Case 436793
 				}

		</cfscript>

	</cffunction>

 	<!--- SWJ 3-Sep-08 eLearning 8.1 update a quizTaken record and set the quizTaken date --->
	<cffunction access="public" name="setQuizTakenTimeLeft" hint="Update a quizTaken record and set the timeleft.">
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes">
	 	<cfargument name="NewTimeLeft" type="numeric" required="Yes">

		<cfscript>
			var setQuizTakenTimeLeft = "";
		</cfscript>

		<CFQUERY NAME="setQuizTakenTimeLeft" datasource="#application.siteDataSource#">
			UPDATE QuizTaken SET
			TimeLeft = #arguments.NewTimeLeft#
			WHERE QuizTakenID = #QuizTakenID#
		</CFQUERY>
	</cffunction>

	 <!--- check to see if user has attempted the quiz already and get details --->
	<cffunction access="public" name="getQuiz" returnType="Query">
		<cfargument name="QuizTakenID" type="numeric" required="Yes" />
		<cfargument name="OrderMethod" type="string" required="false" default="Random" hint="Alpha|Random|SortOrder. Question order used by portal relaytags. Alpha not currently supported." />

		<cfscript>
			var getQuiz = "";
			var getQuizQuestions = "";
			var getQuizTakenQuestions = "";
			var getQuizTakenQuestionsViewed = "";
			var newQs = 0;  // 2014-01-31 NYB Case 438543 added
			var thisActualQ = "";
			var getQuizTakenQuestionsLimited = "";
		</cfscript>

		<!--- 2011-09-16 NYB P-SNY106 - changed to call in all quizTaken fields --->
		<!--- 2013-04-23 NYB Case 434166 replaced td.Passmark with tt.passmark: --->
		<CFQUERY NAME="getQuiz" datasource="#application.siteDataSource#">
			SELECT 	td.QuizDetailID,
				'phr_title_TrngModule_' + convert(VarChar,tm.moduleID) AS AssociatedModule,
					'phr_title_quizdetail_'+cast(td.quizDetailID as varchar(50)) as QuizName,
					td.PossibleQuestions, td.NumToChoose, td.TimeAllowed, td.QuestionsPerPage,
					td.NumberOfAttemptsAllowed, td.Created,
					td.CreatedBy,
					td.QuizGroupID,
					isNull(tt.Passmark,td.Passmark) as Passmark,  <!--- 2013-09-28 WAB CASE 437228, added alias to this field	--->
					isnull(tm.moduleID,0) as [moduleID],<!--- 2013-03-21 NYB Case 434096 wrapped tm.moduleid with isNull --->
					 tm.isSCORM, tm.passDecidedBy,tt.* <!--- STCR this was using QuizTaken.*, therefore we need to handle the QuizTaken.Questions and QuizTaken.QuestionsViewed columns in case they were being used by code calling this function. --->
			FROM QuizTaken AS tt INNER JOIN QuizDetail AS td ON tt.QuizDetailID = td.QuizDetailID
				<!--- 2013-03-21 NYB Case 434096 changed inner join to left join: --->
				left join trngModule tm on tm.quizDetailId = td.quizDetailID
			WHERE tt.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#" />
		</CFQUERY>

		<!--- STCR - needed to modify QuizTaken.Questions to use QuizDetailQuestionPool and QuizTakenQuestion, because questions.cfm uses this for the Question IDs. It would be nice to separate these out to avoid the ID lists, but there is a lot of sparsely-commented logic that depends upon these csv ID lists being passed around. --->
		<cfif getQuiz.recordCount gt 0 and isNumeric(getQuiz.QuizDetailID)>
			<cfquery name="getQuizQuestions" datasource="#application.siteDataSource#">
				select qq.QuestionID from QuizDetailQuestionPool qdqp
				inner join QuizQuestion qq with(nolock) on qdqp.QuestionPoolID = qq.QuestionPoolID
				where QuizDetailID =  <cf_queryparam value="#getQuiz.QuizDetailID#" CFSQLTYPE="cf_sql_integer" >
				<cfif arguments.OrderMethod eq "SortOrder">order by qq.sortOrder,qq.QuestionRef</cfif><!--- 2013-04-16 STCR CASE 432328 --->
			</cfquery>
			<cfset getQuiz.PossibleQuestions = ValueList(getQuizQuestions.QuestionID) />

			<cfquery name="getQuizTakenQuestions" datasource="#application.siteDataSource#">
				<!--- 2014-01-31 NYB Case 438543 added qtq.QuizTakenQuestionID and qtq.QuizTakenID --->
				select qtq.QuizTakenID,qtq.QuizTakenQuestionID,qtq.QuestionID, qq.sortOrder, qq.QuestionRef,qtq.Viewed
				from QuizTakenQuestion qtq
				left join QuizQuestion qq with(nolock) on qtq.QuestionID = qq.QuestionID
				where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
				<!--- START: 2014-02-11 NYB Case 438543 added an ELSE - it's lack of existance was nullifying the concept of Randomising questions --->
				<cfif arguments.OrderMethod eq "SortOrder">order by qq.sortOrder,qq.QuestionRef
				<cfelse>
				order by quizTakenQuestionID
				</cfif><!--- 2013-04-16 STCR CASE 432328 --->
				<!--- END: 2014-02-11 NYB Case 438543 --->
				<!--- 2013-04-16 STCR Default order is the random order specified in ActualQs at createQuiz() time. --->
				<!--- Alpha order not required so not implemented. Unsure what field - if any - would make sense to use for an alpha sort anyway - QuestionRef vs defaultTranslation vs currentuser translation?) --->
			</cfquery>

			<!--- START: 2014-01-31 NYB Case 438543 additions/changes --->
			<!--- 2014-02-11 NYB Case 438543 - changed if --->
			<CFIF getQuiz.TimeLeft gt 0 AND len(getQuiz.Completed) eq 0>
				<cfquery name="local.deleteQuizTakenQuestions" datasource="#application.siteDataSource#">
					<!--- 2014-02-11 NYB Case 438543 - delete question answers to - so we don't end up with floating data --->
					delete from QuizTakenAnswer where
						QuizTakenQuestionID in (select QuizTakenQuestionID from QuizTakenQuestion where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
							and questionid  not in ( <cf_queryparam value="#getQuiz.PossibleQuestions#" CFSQLTYPE="cf_sql_integer"  list="true"> ))
					delete from QuizTakenQuestion where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
						and questionid  not in ( <cf_queryparam value="#getQuiz.PossibleQuestions#" CFSQLTYPE="cf_sql_integer"  list="true"> )
				</cfquery>

				<cfquery name="getQuizTakenQuestions" dbtype="query">
					<!--- 2014-02-11 NYB Case 438543 - changed query contents--->
					select * from getQuizTakenQuestions where questionid in (#getQuiz.PossibleQuestions#)
					<!--- START: 2014-02-11 NYB Case 438543 added order by --->
					<cfif arguments.OrderMethod eq "SortOrder">order by sortOrder,QuestionRef
					<cfelse>
					order by quizTakenQuestionID
					</cfif>
					<!--- END: 2014-02-11 NYB Case 438543 --->
				</cfquery>

				<cfif getQuizTakenQuestions.recordcount gt getQuiz.NumToChoose>
					<cfquery name="getQuizTakenQuestionsLimited" dbtype="query" maxrows="#getQuiz.NumToChoose#">
						select * from getQuizTakenQuestions
						<!--- START: 2014-02-11 NYB Case 438543 added order by --->
						<cfif arguments.OrderMethod eq "SortOrder">order by sortOrder,QuestionRef
						<cfelse>
						order by quizTakenQuestionID
						</cfif>
						<!--- END: 2014-02-11 NYB Case 438543 --->
					</cfquery>
					<cfquery name="deleteQuizTakenQuestions" datasource="#application.siteDataSource#">
						<!--- 2014-02-11 NYB Case 438543 - delete question answers to - so we don't end up with floating data --->
						delete from QuizTakenAnswer where
							QuizTakenQuestionID in (select QuizTakenQuestionID from QuizTakenQuestion where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
								and questionid  not in ( <cf_queryparam value="#getQuizTakenQuestionsLimited.QuestionID#" CFSQLTYPE="cf_sql_integer"  list="true"> ))
						delete from QuizTakenQuestion where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
							and questionid  not in ( <cf_queryparam value="#getQuizTakenQuestionsLimited.QuestionID#" CFSQLTYPE="cf_sql_integer"  list="true"> )
					</cfquery>
					<cfquery name="getQuizTakenQuestions" dbtype="query">
						select * from getQuizTakenQuestionsLimited
						<!--- START: 2014-02-11 NYB Case 438543 added order by --->
						<cfif arguments.OrderMethod eq "SortOrder">order by sortOrder,QuestionRef
						<cfelse>
						order by quizTakenQuestionID
						</cfif>
						<!--- END: 2014-02-11 NYB Case 438543 --->
					</cfquery>
				</cfif>
				<cfif getQuizTakenQuestions.recordcount lt getQuiz.NumToChoose>
					<!--- START: 2014-02-11 NYB Case 438543 - remove existing question from possible list first --->
					<cfif getQuiz.NumToChoose gt listlen(getQuiz.POSSIBLEQUESTIONS)>
						<cfset var MaxRowsVar = listlen(getQuiz.POSSIBLEQUESTIONS)>
					<cfelse>
						<cfset MaxRowsVar = getQuiz.NumToChoose>
					</cfif>
					<cfset MaxRowsVar = MaxRowsVar - getQuizTakenQuestions.recordcount>
					<cfset var possNewQs = application.com.globalfunctions.ListMinusList(getQuiz.POSSIBLEQUESTIONS,valuelist(getQuizTakenQuestions.Questionid))>
					<cfset newQs = application.com.globalfunctions.RandomSelectFromList(possNewQs,MaxRowsVar)>
					<!--- END: 2014-02-11 NYB Case 438543 --->

					<cfloop list="#newQs#" index="thisActualQ">
						<cfset var thisQuestionTextGiven = application.com.relayTranslations.translatePhrase(phrase="Title_QuizQuestion_#val(thisActualQ)#") />
						<cfquery name="local.addQuizTakenQuestion" datasource="#application.siteDataSource#">
							INSERT INTO QuizTakenQuestion (QuizTakenID,QuestionID,QuestionTextGiven)
							VALUES (<cf_queryparam value="#arguments.QuizTakenID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#val(thisActualQ)#" CFSQLTYPE="CF_SQL_INTEGER" >,
								<cfif len(thisQuestionTextGiven) gt 4000>
									<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(thisQuestionTextGiven,4000)#">
								<cfelseif len(thisQuestionTextGiven) gt 0>
									<cf_queryparam cfsqltype="cf_sql_varchar" value="#thisQuestionTextGiven#">
								<cfelse>
									NULL
								</cfif>
								)
						</cfquery>
						<cfset thisQuestionTextGiven = "" />
					</cfloop>

					<cfquery name="getQuizTakenQuestions" datasource="#application.siteDataSource#">
						<!--- 2014-02-11 NYB Case 438543 - added quizTakenQuestionID and Viewed--->
						select quizTakenQuestionID,qtq.Viewed,qtq.QuestionID, qq.sortOrder, qq.QuestionRef
						from QuizTakenQuestion qtq
						left join QuizQuestion qq with(nolock) on qtq.QuestionID = qq.QuestionID
						where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
						<!--- START: 2014-02-11 NYB Case 438543 added order by --->
						<cfif arguments.OrderMethod eq "SortOrder">order by sortOrder,QuestionRef
						<cfelse>
						order by quizTakenQuestionID
						</cfif>
						<!--- END: 2014-02-11 NYB Case 438543 --->
					</cfquery>
				</cfif>
			</CFIF>

			<cfset getQuiz.Questions = ValueList(getQuizTakenQuestions.QuestionID) />

			<cfquery name="getQuizTakenQuestionsViewed" dbtype="query">
				<!--- 2014-02-11 NYB Case 438543 - added quizTakenQuestionID --->
				select QuestionID,quizTakenQuestionID,sortOrder,QuestionRef from getQuizTakenQuestions where Viewed=1
				<!--- START: 2014-02-11 NYB Case 438543 added order by --->
				<cfif arguments.OrderMethod eq "SortOrder">order by sortOrder,QuestionRef
				<cfelse>
				order by quizTakenQuestionID
				</cfif>
				<!--- END: 2014-02-11 NYB Case 438543 --->
			</cfquery>
			<!--- END: 2014-01-31 NYB Case 438543 additions/changes --->

			<cfset getQuiz.QuestionsViewed = ValueList(getQuizTakenQuestionsViewed.QuestionID) />

		</cfif>
	 	<cfreturn getQuiz />
	</cffunction>

	<!--- make a record for the person taking the quiz --->
	<cffunction access="public" name="createQuiz" returnType="Query">
		<cfargument name="EntityTypeID" type="numeric" required="Yes" default="0">
		<cfargument name="QuizDetailID" type="numeric" required="Yes">
		<cfargument name="PersonID" type="numeric" default="#request.relaycurrentuser.personid#">
		<cfargument name="ActualQs" type="string" required="Yes">
		<cfargument name="TimeAllowed" type="numeric" required="Yes">
		<cfargument name="PassMark" type="numeric" required="No"><!--- 2013-05-10 STCR CASE 434166 --->
		<cfargument name="FreezeDate" type="numeric" required="Yes">
		<cfargument name="CreatedBy" type="numeric" required="Yes" default="#request.relayCurrentUser.personid#">
		<cfargument name="userModuleProgressID" type="numeric" required="yes"> <!--- NJH 2008/09/03 elearning 8.1  - userModuleProgressID is now tied to a quizTaken --->

		<cfscript>
			var createQuiz = "";
			var getQuizdetails = "";
			var argumentsStruct = structNew();
			var thisActualQ = "";
			var createQuizResult = "";
			var newQuizTakenID = "";
			var getQuizDetailQuestions = "";
			var getQuizTakenQuestions = "";
			var thisQuestionTextGiven = "";
			var getQuizInfo = "";
		</cfscript>

		<!--- START 2013-11-07 NYB Case 437785 added to make sure values get into the right place --->
		<cfif structKeyExists(arguments,"PassMark")>
			<CFQUERY NAME="getQuizInfo" datasource="#application.siteDataSource#">
				SELECT Passmark FROM QuizDetail WHERE QuizDetailID = #arguments.QuizDetailID#
			</CFQUERY>
			<cfset arguments.PassMark = getQuizInfo.PassMark>
		</cfif>
		<!--- END 2013-11-07 NYB Case 437785 --->

		<!--- START 2013-09-03 PPB Case 436464 we no longer need to maintain the Questions column in QuizTaken because we now maintain QuizTakenQuestion table instead --->
		<CFQUERY NAME="createQuiz" datasource="#application.siteDataSource#" result="createQuizResult">
			INSERT INTO QuizTaken
			(EntityTypeID, QuizDetailID, PersonID, Started, TimeLeft, UserModuleProgressID, Created, CreatedBy, PassMark)
			VALUES
			(<cf_queryparam value="#EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#QuizDetailID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" >, getdate(), <cf_queryparam value="#TimeAllowed#" CFSQLTYPE="CF_SQL_Integer" >, <cf_queryparam value="#arguments.UserModuleProgressID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#CreatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >
			, <!--- START 2013-11-07 NYB Case 437785 removed 'if passmark exists' ---><cf_queryparam value="#arguments.PassMark#" CFSQLTYPE="CF_SQL_FLOAT"><!--- END 2013-11-07 NYB Case 437785 --->) <!--- 2013-05-10 STCR CASE 434166 --->
		</CFQUERY>
		<!--- END 2013-09-03 PPB Case 436464 --->

		<cfset newQuizTakenID = createQuizResult.identityCol />

		<cfloop list="#arguments.ActualQs#" index="thisActualQ">
			<cfset thisQuestionTextGiven = application.com.relayTranslations.translatePhrase(phrase="Title_QuizQuestion_#val(thisActualQ)#") />
			<cfquery name="local.addQuizTakenQuestion" datasource="#application.siteDataSource#">
				INSERT INTO QuizTakenQuestion (QuizTakenID,QuestionID,QuestionTextGiven)
				VALUES (<cf_queryparam value="#newQuizTakenID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#val(thisActualQ)#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cfif len(thisQuestionTextGiven) gt 4000>
						<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(thisQuestionTextGiven,4000)#">
					<cfelseif len(thisQuestionTextGiven) gt 0>
						<cf_queryparam cfsqltype="cf_sql_varchar" value="#thisQuestionTextGiven#">
					<cfelse>
						NULL
					</cfif>
					)
			</cfquery>
			<cfset thisQuestionTextGiven = "" />
		</cfloop>

		<!--- get details of inserted record--->
		<!--- 2013-04-23 NYB Case 434166 replaced td.Passmark with tt.passmark: --->
		<CFQUERY NAME="getQuizdetails" datasource="#application.siteDataSource#">
			SELECT 	td.QuizDetailID,
					'phr_title_quizdetail_'+cast(td.quizDetailID as varchar(50)) as QuizName,
					td.PossibleQuestions, td.NumToChoose, td.TimeAllowed, td.QuestionsPerPage,
					td.NumberOfAttemptsAllowed,td.Created,
					td.CreatedBy, td.QuizGroupID, tt.Passmark, tm.moduleID,
					tt.QuizTakenID, tt.EntityTypeID, tt.QuizDetailID, tt.Questions, tt.TimeLeft,
					tt.Score, tt.Completed, tt.Created, tt.CreatedBy, tt.Started, tt.PersonID, tt.userModuleProgressID
			FROM QuizTaken AS tt INNER JOIN QuizDetail AS td ON tt.QuizDetailID = td.QuizDetailID
				<!--- 2013-03-21 NYB Case 434096 replaced inner join with left join: --->
				left join trngModule tm on tm.quizDetailID = td.quizDetailID
			WHERE tt.Created = #FreezeDate#
			AND tt.PersonID = #PersonID#
			AND tt.CreatedBy = #CreatedBy#
		</CFQUERY>

		<cfloop query="getQuizDetails"><!--- STCR - there should only be one row, but I do not entirely like that the query above does not use the specific inserted identity for retrieving the inserted record, so will loop through just in case there are duplicate matches on the whereclause --->
			<cfif isNumeric(getQuizDetails.QuizDetailID)>
				<cfquery name="getQuizDetailQuestions" datasource="#application.siteDataSource#">
					select qq.QuestionID from QuizDetailQuestionPool qdqp
					inner join QuizQuestion qq with(nolock) on qdqp.QuestionPoolID = qq.QuestionPoolID
					where QuizDetailID =  <cf_queryparam value="#getQuizDetails.QuizDetailID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset getQuizDetails.PossibleQuestions = ValueList(getQuizDetailQuestions.QuestionID) />
			</cfif>
 			<cfif isNumeric(getQuizDetails.QuizTakenID)>
				<cfquery name="getQuizTakenQuestions" datasource="#application.siteDataSource#">
					select QuestionID from QuizTakenQuestion where QuizTakenID =  <cf_queryparam value="#getQuizDetails.QuizTakenID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset getQuizDetails.Questions = ValueList(getQuizTakenQuestions.QuestionID) />
			</cfif>
		</cfloop>

		<!--- NJH 2008/09/16 update the lastVisitedDate on the user module progress table --->
		<cfscript>
			argumentsStruct.frmLastVisited = FreezeDate;
			application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=arguments.userModuleProgressID,argumentsStruct=argumentsStruct);
		</cfscript>

		<cfreturn getQuizdetails>
	</cffunction>

	<!--- P-REL109 STCR - return the Questions for this Quiz. Replaces FlagGroup based method --->
	<cffunction access="public" name="getQuizQuestions" hint="Returns QuizQuestion information for Portal RelayTag. May factor this in with getQuizQuestionList() later" returnType="Query">
		<cfargument name="QuestionIDs" required="No" default="">
		<cfargument name="excludeIDs" required="No" type="Boolean" default="false"><!--- STCR - this appears to be used when populating the CF_TwoSelectsComboList for assigning questions to a quiz --->
		<cfargument name="useParents" required="No" type="Boolean" default="false"><!--- STCR - seems to be used for Question Groups, using parent FlagGroup  --->
		<!--- 2010/09/30 		NAS 	LID4189: eLearning - Quiz question pool to display in alpha order - added SortOrder Param --->
		<cfargument name="sortorder" required="No" >
		<cfargument name="QuizDetailID" required="false" type="numeric" default="0" />
		<cfargument name="QuizTakenID" required="false" type="numeric" default="0" />
		<cfargument name="QuestionID" required="false" type="numeric" default="0" />

		<cfscript>
			var getQuestions = "";
		</cfscript>

		<cfquery name="getQuestions" datasource="#application.siteDataSource#">
			select qq.QuestionID, qq.QuestionTypeID, qq.QuestionRef, qqt.QuestionTypeTextID <cfif arguments.QuizTakenID gt 0>,qtq.QuizTakenQuestionID</cfif>
			from QuizQuestion qq
			inner join QuizQuestionType qqt with(nolock) on qq.QuestionTypeID = qqt.QuestionTypeID
			<cfif arguments.QuizTakenID gt 0>
				inner join QuizTakenQuestion qtq with(nolock) on qq.QuestionID = qtq.QuestionID
				inner join QuizTaken qt with(nolock) on qtq.QuizTakenID = qt.QuizTakenID
				where 1=1
				AND qt.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
				<cfif listLen(arguments.QuestionIDs) gt 0>AND qq.QuestionID <cfif arguments.excludeIDs>NOT </cfif>
					in (<cf_queryparam list="true" cfsqltype="cf_sql_integer" value="#arguments.QuestionIDs#">)
				</cfif>
			<cfelseif listLen(arguments.QuestionIDs) gt 0>
				where qq.QuestionID <cfif arguments.excludeIDs>NOT </cfif>
					in (<cf_queryparam list="true" cfsqltype="cf_sql_integer" value="#arguments.QuestionIDs#">)
			<cfelseif arguments.QuestionIDs eq "" and arguments.excludeIDs eq false>
				where 1=0
			</cfif>
			<cfif isdefined("arguments.sortorder") and arguments.sortorder eq "QuestionRef">
				order by qq.QuestionRef
			</cfif>
		</cfquery>

	 	<cfreturn getQuestions />
	</cffunction>

	<!--- STCR P-REL109 --->
	<cffunction access="public" name="getQuizQuestionPools" hint="Returns QuizQuestionPool information for Question editor." returntype="Query">
		<cfargument name="QuizDetailID" type="numeric" required="false" default="0" />
		<cfargument name="invertSelection" type="boolean" required="false" default="false" />
		<cfargument name="sortByRef" type="boolean" required="false" default="true" />


		<cfscript>
			var getQuizQuestionPools = "";
		</cfscript>

		<cfquery name="getQuizQuestionPools" datasource="#application.siteDataSource#">
			select qqp.QuestionPoolID, qqp.QuestionPoolRef, qdqp.numOfQuestions
			from QuizQuestionPool qqp
			<cfif arguments.QuizDetailID neq 0 and not arguments.invertSelection>
				inner join QuizDetailQuestionPool qdqp with(nolock) on qqp.QuestionPoolID = qdqp.QuestionPoolID
				and qdqp.QuizDetailID =  <cf_queryparam value="#val(arguments.QuizDetailID)#" CFSQLTYPE="cf_sql_integer" >
			<cfelseif arguments.QuizDetailID neq 0 and arguments.invertSelection>
				where qqp.QuestionPoolID not in <!--- STCR TODO consider revising this to remove the subquery by using a left join and checking qdqp.QuizDetailQuestionPoolID is null ? --->
					(select QuestionPoolID from QuizDetailQuestionPool where QuizDetailID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizDetailID#"> )
			<!--- NJH 2013/01/07 - if on a new quiz and we don't want to invert a selection, we don't want to bring back any results --->
			<cfelseif not arguments.invertSelection>
				where 1=0
			</cfif>
			<cfif arguments.sortByRef>
				order by qqp.QuestionPoolRef
			</cfif>
		</cfquery>

		<cfreturn getQuizQuestionPools />
	</cffunction>

	<!--- STCR P-REL109 --->
	<cffunction access="public" name="getQuizQuestionList" hint="Returns QuizQuestion information for Question editor. May factor this in with getQuizQuestions() later" returntype="Query">
		<cfargument name="QuestionID" type="numeric" required="false" default="0" />
		<cfargument name="QuestionPoolID" type="numeric" required="false" default="0" />
		<cfargument name="forDropDown" type="boolean" required="false" default="0" />

		<cfscript>
			var getQuizQuestions = "";
			var QuizQuestionPhraseQuerySnippets = "";
		</cfscript>

		<cfset QuizQuestionPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "QuizQuestion", phraseTextID = "title")>

		<cfquery name="getQuizQuestions" datasource="#application.siteDataSource#">
			<cfif forDropDown and arguments.QuestionID eq 0> select 0 as value, 'phr_elearning_Question_none' as display UNION</cfif>
			select <cfif forDropDown>QuestionID as value, QuestionText as display<cfelse>*</cfif> from (
			select QuestionID, QuestionRef, QuizQuestion.QuestionTypeID, QuizQuestionType.QuestionTypeTextID, QuizQuestion.QuestionPoolID, 'Answers' AS Answers
			,QuizQuestion.sortOrder <!--- 2013-04-12 STCR CASE 432328 --->
			,#preservesinglequotes(QuizQuestionPhraseQuerySnippets.select)# as QuestionText
			from QuizQuestion
			#preservesinglequotes(QuizQuestionPhraseQuerySnippets.join)#
			inner join QuizQuestionType with(nolock) on QuizQuestion.QuestionTypeID = QuizQuestionType.QuestionTypeID
			where 1=1
			<cfif arguments.QuestionID neq 0>
				and QuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionID#">
			</cfif>
			<cfif arguments.QuestionPoolID neq 0>
				and QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionPoolID#">
			</cfif>
			) as baseqry
			where 1=1
		</cfquery>

		<cfreturn getQuizQuestions />
	</cffunction>

	<!--- STCR P-REL109 --->
	<cffunction access="public" name="getQuizQuestionPoolList" hint="Returns QuizQuestionPool information for Question Pool editor." returntype="Query">
		<cfargument name="QuestionPoolID" type="numeric" required="false" default="0" />
		<cfargument name="sortOrder" type="string" required="false">

		<cfscript>
			var getQuizQuestionPools = "";
		</cfscript>

		<cfquery name="getQuizQuestionPools" datasource="#application.siteDataSource#">
			select * from (
			select QuestionPoolID, QuestionPoolRef, 'phr_elearning_Manage_Questions' AS Manage_Questions
			from QuizQuestionPool
			where 1=1
			<cfif arguments.QuestionPoolID neq 0>
				and QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionPoolID#">
			</cfif>
			) as baseqry
			<cfif structKeyExists(arguments,"sortOrder") and arguments.sortOrder neq "">
			order by #arguments.sortOrder#
			</cfif>
		</cfquery>

		<cfreturn getQuizQuestionPools />
	</cffunction>

	<!--- STCR P-REL109 --->
	<cffunction access="public" name="getAnswerList" hint="Returns QuizAnswer information for Answer editor. " returntype="Query" validValues="true">
		<cfargument name="AnswerID" type="numeric" required="false" default="0" />
		<cfargument name="QuestionID" type="numeric" required="false" default="0" />
		<cfargument name="QuestionPoolID" type="numeric" required="false" default="0" />
		<cfargument name="forDropDown" type="boolean" required="false" default="0" />
		<cfargument name="forMatchingQuestionType" type="boolean" required="false" default="false" />
		<cfargument name="forAddForm" type="boolean" required="false" default="false" />
		<cfargument name="excludeAnswerID" type="numeric" required="false" default="0" />

		<cfscript>
			var getQuizAnswers = "";
			var QuizAnswerPhraseQuerySnippets = "";
		</cfscript>

		<cfset QuizAnswerPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "QuizAnswer", phraseTextID = "title")>

		<cfquery name="getQuizAnswers" datasource="#application.siteDataSource#">
			<cfif forDropDown and arguments.AnswerID eq 0> select 0 as value, <cfif arguments.forMatchingQuestionType and arguments.forAddForm>'phr_elearning_Select_RHS_Option_Or_Input_New'<cfelseif arguments.forMatchingQuestionType>'phr_elearning_Matching_Answer_none'<cfelse>'phr_elearning_Answer_none'</cfif> as display UNION</cfif>
			select <cfif forDropDown>AnswerID as value, AnswerText as display<cfelse>*</cfif> from (
			select AnswerID, QuizAnswer.QuestionID, MatchingAnswerID, Score, QuizAnswer.sortOrder, QuizQuestion.QuestionTypeID, QuizQuestionType.QuestionTypeTextID <!--- , QuizQuestion.QuestionPoolID --->
			,#preservesinglequotes(QuizAnswerPhraseQuerySnippets.select)# as AnswerText
			,AnswerRef						<!--- 2013-02-22 PPB Case 433209 --->
			from QuizAnswer
			#preservesinglequotes(QuizAnswerPhraseQuerySnippets.join)#
			inner join QuizQuestion with(nolock) on QuizQuestion.QuestionID = QuizAnswer.QuestionID
			inner join QuizQuestionType with(nolock) on QuizQuestion.QuestionTypeID = QuizQuestionType.QuestionTypeID
			where 1=1
			<cfif arguments.AnswerID neq 0>
				and AnswerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.AnswerID#">
			</cfif>
			<cfif arguments.excludeAnswerID neq 0>
				and AnswerID != #val(arguments.excludeAnswerID)# <!--- For matchingAnswer dropdown, we want to exclude from  --->
			</cfif>
			<cfif arguments.forDropDown and arguments.forMatchingQuestionType>
				and isNull(MatchingAnswerID,0) = 0 <!--- Do not offer already-assigned left hand side options as potential right hand side options --->
			</cfif>
			<cfif arguments.QuestionID neq 0><!--- STCR The cf_queryparam breaks using this query inside a RelayXMLEditor field --->
				and QuizAnswer.QuestionID = <cfif arguments.forDropDown>#val(arguments.QuestionID)#<cfelse><cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionID#"></cfif>
			</cfif>
			<!--- <cfif arguments.QuestionPoolID neq 0>
				and QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionPoolID#">
			</cfif> --->
			) as baseqry
			where 1=1
			<cfif not forDropDown>order by baseqry.sortOrder</cfif>
		</cfquery>

		<cfreturn getQuizAnswers />
	</cffunction>

	<cffunction access="public" name="getQuizAnswers" hint="Retrieve Answers for Quiz relaytag" returntype="query">
		<cfargument name="QuestionID" type="numeric" required="true" />
		<cfargument name="QuizTakenID" type="numeric" required="false" default="0" />

		<cfscript>
			var getQuizAnswers = "";
		</cfscript>

		<cfquery name="getQuizAnswers" datasource="#application.siteDataSource#">
			select qa.AnswerID, qa.QuestionID, qa.MatchingAnswerID, qa.Score, qa.sortOrder <cfif arguments.QuizTakenID neq 0>, qtq.QuizTakenQuestionID, qtq.QuizTakenID, qta.QuizTakenAnswerID, qta.ScoreGiven</cfif>
			from QuizAnswer qa
			<cfif arguments.QuizTakenID neq 0>inner join QuizTakenQuestion qtq on qtq.QuestionID = qa.QuestionID and qtq.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
			left join QuizTakenAnswer qta on qtq.QuizTakenQuestionID = qta.QuizTakenQuestionID and qa.AnswerID=qta.AnswerID </cfif>
			where qa.QuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionID#">
			order by sortOrder
		</cfquery>

		<cfreturn getQuizAnswers />
	</cffunction>


	<cffunction access="public" name="insertAnswer" hint="Inserts new QuizAnswer information. " returntype="numeric">
		<cfargument name="QuestionID" type="numeric" required="true" />
		<cfargument name="defaultTranslation" type="string" required="false" default="" />
		<cfargument name="MatchingAnswerID" type="numeric" required="false" default="0" />
		<cfargument name="Score" type="numeric" required="false" default="0" />
		<cfargument name="sortOrder" type="numeric" required="false" default="0" />
		<cfargument name="AnswerRef" type="string" required="false" default="" /><!--- Used for identifying unique Answer during Bulk Upload, to support multiple translations before the AnswerID is known --->
		<cfargument name="MatchingAnswerRef" type="string" required="false" default="" /><!--- Used for identifying unique Matching Answer during Bulk Upload ---><!--- 2012-08-24 STCR CASE 430084 --->

		<cfscript>
			var insertAnswer = "";
			var insertAnswerQueryResult = "";
			var newAnswerID = 0;
		</cfscript>

		<cfquery name="insertAnswer" datasource="#application.siteDataSource#" result="insertAnswerQueryResult">
			insert into QuizAnswer (QuestionID,AnswerRef,MatchingAnswerRef,MatchingAnswerID,Score,sortOrder)
			VALUES(
				<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionID#">,
				<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.AnswerRef#" maxlength="100">,
				<cfif len(arguments.MatchingAnswerRef) gt 0>
					<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.MatchingAnswerRef#" maxlength="100">, <!--- 2012-08-24 STCR CASE 430084 --->
				<cfelse>
					NULL,
				</cfif>
				<cfif arguments.MatchingAnswerID gt 0>
					<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.MatchingAnswerID#">,
				<cfelse>
					NULL,
				</cfif>
				<cf_queryparam cfsqltype="cf_sql_float" value="#arguments.Score#">,<!--- 2012-05-10 STCR P-REL109 - cf_sql_decimal and cf_sql_numeric were causing the value to be rounded to nearest integer. --->
				<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.sortOrder#">
				)
		</cfquery>

		<cfset newAnswerID = insertAnswerQueryResult.identityCol />

		<cfif isNumeric(newAnswerID) and newAnswerID gt 0 and arguments.defaultTranslation neq "">
			<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(entityTypeID=application.entityTypeID.QuizAnswer, entityID=newAnswerID, phrasetext=arguments.defaultTranslation, phraseTextID='Title') />
		</cfif>

		<cfreturn newAnswerID />
	</cffunction>

	<cffunction access="public" name="deleteAnswer" hint="Deletes QuizAnswer information.">
		<cfargument name="answerID" type="numeric" required="true" />

		<cfscript>
			var deleteAnswer = "";
		</cfscript>

		<cfquery name="deleteAnswer" datasource="#application.siteDataSource#">
			delete from QuizAnswer
			where answerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.answerID#">

			update quizAnswer
			set matchingAnswerID = null
			where matchingAnswerID =  <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.answerID#">
		</cfquery>

	</cffunction>

	<cffunction access="public" name="insertQuestion" hint="Inserts new QuizQuestion information. " returntype="numeric">
		<cfargument name="QuestionPoolID" type="numeric" required="true" />
		<cfargument name="QuestionRef" type="string" required="true" />
		<cfargument name="QuestionTypeID" type="numeric" required="true" />
		<cfargument name="defaultTranslation" type="string" required="false" default="" />
		<cfargument name="sortOrder" type="numeric" required="false" default="0" />

		<cfscript>
			var insertQuestion = "";
			var insertQuestionQueryResult = "";
			var newQuestionID = 0;
		</cfscript>

		<cfquery name="insertQuestion" datasource="#application.siteDataSource#" result="insertQuestionQueryResult">
			insert into QuizQuestion (QuestionPoolID,QuestionRef,QuestionTypeID,sortOrder,Created,CreatedBy,LastUpdated,LastUpdatedBy)
			VALUES(
				<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionPoolID#">,
				<cf_queryparam cfsqltype="cf_sql_varchar" value="#arguments.QuestionRef#" maxlength="100">,
				<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionTypeID#">,
				<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.sortOrder#">,
				getDate(),
				<cf_queryparam value="#request.relaycurrentuser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,
				getDate(),
				<cf_queryparam value="#request.relaycurrentuser.userGroupID#" CFSQLTYPE="cf_sql_integer" >)
		</cfquery>

		<cfset newQuestionID = insertQuestionQueryResult.identityCol />

		<cfif isNumeric(newQuestionID) and newQuestionID gt 0 and arguments.defaultTranslation neq "">
			<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(entityTypeID=application.entityTypeID.QuizQuestion, entityID=newQuestionID, phrasetext=arguments.defaultTranslation, phraseTextID='Title') />
		</cfif>

		<cfreturn newQuestionID />
	</cffunction>

	<cffunction access="public" name="deleteQuestion" hint="Deletes QuizQuestion information.">
		<cfargument name="QuestionID" type="numeric" required="true" />

		<cfscript>
			var deleteQuestion = "";
		</cfscript>

		<cfquery name="deleteQuestion" datasource="#application.siteDataSource#">
			delete from QuizAnswer
			where questionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.questionID#">

			delete from QuizQuestion
			where questionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.questionID#">
		</cfquery>

	</cffunction>

	<!--- 2012-09-04 STCR CASE 430474 --->
	<cffunction access="public" name="deleteQuestionPool" hint="Deletes QuizQuestionPool information.">
		<cfargument name="QuestionPoolID" type="numeric" required="true" />

		<cfscript>
			var getQuestions = "";
			var deleteQuestionPool = "";
		</cfscript>

		<!--- Remove QuizAnswer records, then QuizQuestion records, then the QuizQuestionPool --->
		<cfset getQuestions = getQuizQuestionList(QuestionPoolID=arguments.QuestionPoolID) />
		<cfloop query="getQuestions">
			<cfset deleteQuestion(QuestionID=getQuestions.QuestionID) />
		</cfloop>

		<cfquery name="deleteQuestionPool" datasource="#application.siteDataSource#">
			delete from QuizQuestionPool
			where questionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.questionPoolID#" />
		</cfquery>

	</cffunction>

	<!--- Takes over updates that used to be handled by flagTask.cfm --->
	<cffunction access="public" name="updateQuizTakenAnswers" hint="Inserts new QuizAnswer information. " returntype="numeric">
		<cfargument name="QuizTakenID" type="numeric" required="true" />
		<cfargument name="QuizTakenQuestionID" type="numeric" required="true" />
		<cfargument name="QuestionID" type="numeric" required="true" />
		<cfargument name="AnswerList" type="string" required="false" default="" />
		<cfargument name="MatchingAnswerList" type="string" required="false" /><!--- 2012-08-22 CASE 430124 --->
		<cfargument name="MatchingPairDelim" type="string" required="false" default="#application.delim1#" /><!--- 2012-08-22 CASE 430124 --->
		<!--- <cfargument name="AnsweredAt" type="Date" required="false" /> --->

		<cfscript>
			var returnValue = 0;
			var checkIDs = "";
			var removeUnselected = "";
			var insertNew = "";
			var thisOptionIDPair = "";
			var thisOptionIDLeft = "";
			var thisOptionIDRight = "";
			var listOptionIDLeft = "";
			var thisAnswerTextGiven = "";
			var thisMatchingAnswerTextGiven = "";
		</cfscript>

		<cfif arguments.QuizTakenID gt 0 and arguments.QuizTakenQuestionID gt 0 and arguments.QuestionID gt 0>
			<!--- <cftransaction> --->
				<cfquery name="checkIDs" datasource="#application.siteDataSource#">
					select qtq.QuizTakenQuestionID, qa.AnswerID, qa.MatchingAnswerID, qa.Score from QuizTakenQuestion qtq
					left join QuizAnswer qa with(nolock) on qtq.QuestionID = qa.QuestionID
					where qtq.QuizTakenQuestionID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
					and qtq.QuizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
					and qtq.QuestionID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuestionID#">
				</cfquery>
				<cfif checkIDs.recordCount gt 0>
					<!--- 2012-08-22 STCR CASE 430124 Matching Answer scoring --->
					<cfif not structKeyExists(arguments,"MatchingAnswerList")>
						<cfquery name="removeUnselected" datasource="#application.siteDataSource#">
							delete from QuizTakenAnswer
							where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
							AND AnswerID not in (<cf_queryparam list="true" cfsqltype="cf_sql_integer" value="#arguments.AnswerList#">)
						</cfquery>
						<cfloop query="checkIDs">
							<cfif isNumeric(checkIDs.AnswerID) and listFind(arguments.AnswerList,checkIDs.AnswerID)>
								<cfset thisAnswerTextGiven = application.com.relayTranslations.translatePhrase(phrase="Title_QuizAnswer_#checkIDs.AnswerID#") />
								<cfquery name="insertNew" datasource="#application.siteDataSource#">
									if not exists (
										select QuizTakenAnswerID from QuizTakenAnswer
										where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
										AND AnswerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#checkIDs.AnswerID#">
									)
									insert into QuizTakenAnswer (QuizTakenQuestionID, AnswerID, AnsweredAt, ScoreGiven, AnswerTextGiven) <!--- For Phase 2 also use AnswerTextGiven, MatchingAnswerID, MatchingAnswerTextGiven --->
									values (
										<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">,
										<cf_queryparam cfsqltype="cf_sql_integer" value="#checkIDs.AnswerID#">,
										getDate(),
										<cf_queryparam cfsqltype="cf_sql_float" value="#checkIDs.Score#"> <!--- 2012-09-04 STCR CASE 430416 - cf_sql_decimal and cf_sql_numeric were causing the value to be rounded to nearest integer. --->
										,<cfif len(thisAnswerTextGiven) gt 4000>
											<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(thisAnswerTextGiven,4000)#">
										<cfelseif len(thisAnswerTextGiven) gt 0>
											<cf_queryparam cfsqltype="cf_sql_varchar" value="#thisAnswerTextGiven#">
										<cfelse>
											NULL
										</cfif>
										)
								</cfquery>
								<cfset thisAnswerTextGiven = "" />
							</cfif>
						</cfloop>
					<cfelse>
						<cfloop list="#arguments.MatchingAnswerList#" index="thisOptionIDPair">
							<cfif listlen(thisOptionIDPair,arguments.MatchingPairDelim) eq 2>
								<cfset thisOptionIDLeft  = ListGetAt(thisOptionIDPair,1,arguments.MatchingPairDelim) />
								<cfset thisOptionIDRight = ListGetAt(thisOptionIDPair,2,arguments.MatchingPairDelim) />
								<cfset listOptionIDLeft = ListAppend(listOptionIDLeft,thisOptionIDLeft) />
								<cfif isNumeric(thisOptionIDLeft) and isNumeric(thisOptionIDRight)>
									<cfquery name="removeUnselected" datasource="#application.siteDataSource#">
										delete from QuizTakenAnswer
										where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
										AND AnswerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisOptionIDLeft#">
										AND MatchingAnswerID != <cf_queryparam cfsqltype="cf_sql_integer" value="#thisOptionIDRight#">
									</cfquery>
									<cfloop query="checkIDs">
										<cfif isNumeric(checkIDs.AnswerID) and thisOptionIDLeft eq checkIDs.AnswerID and thisOptionIDRight neq 0><!--- 0 is not a valid selection so do not insert a row --->
											<cfset thisAnswerTextGiven = application.com.relayTranslations.translatePhrase(phrase="Title_QuizAnswer_#checkIDs.AnswerID#") />
											<cfset thisMatchingAnswerTextGiven = application.com.relayTranslations.translatePhrase(phrase="Title_QuizAnswer_#thisOptionIDRight#") />
											<cfquery name="insertNew" datasource="#application.siteDataSource#">
												if not exists (
													select QuizTakenAnswerID from QuizTakenAnswer
													where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
													AND AnswerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#checkIDs.AnswerID#">
													AND MatchingAnswerID = <cf_queryparam cfsqltype="cf_sql_integer" value="#thisOptionIDRight#">
												)
												insert into QuizTakenAnswer (QuizTakenQuestionID, AnswerID, AnsweredAt, ScoreGiven, MatchingAnswerID, AnswerTextGiven, MatchingAnswerTextGiven) <!--- For Phase 2 also use AnswerTextGiven, MatchingAnswerID, MatchingAnswerTextGiven --->
												values (
													<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">,
													<cf_queryparam cfsqltype="cf_sql_integer" value="#checkIDs.AnswerID#">,
													getDate(),
													<!--- Use the Answer Score from the LHS option, only if the correct RHS option was chosen --->
													<cfif isNumeric(checkIDs.MatchingAnswerID) and checkIDs.MatchingAnswerID eq thisOptionIDRight and isNumeric(checkIDs.Score)>
														<cf_queryparam cfsqltype="cf_sql_float" value="#checkIDs.Score#">, <!--- 2012-09-04 STCR CASE 430416 - cf_sql_decimal and cf_sql_numeric were causing the value to be rounded to nearest integer. --->
													<cfelse>
														0,
													</cfif>
													<cf_queryparam cfsqltype="cf_sql_integer" value="#thisOptionIDRight#">
													,<cfif len(thisAnswerTextGiven) gt 4000>
														<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(thisAnswerTextGiven,4000)#">
													<cfelseif len(thisAnswerTextGiven) gt 0>
														<cf_queryparam cfsqltype="cf_sql_varchar" value="#thisAnswerTextGiven#">
													<cfelse>
														NULL
													</cfif>
													,<cfif len(thisMatchingAnswerTextGiven) gt 4000>
														<cf_queryparam cfsqltype="cf_sql_varchar" value="#left(thisMatchingAnswerTextGiven,4000)#">
													<cfelseif len(thisMatchingAnswerTextGiven) gt 0>
														<cf_queryparam cfsqltype="cf_sql_varchar" value="#thisMatchingAnswerTextGiven#">
													<cfelse>
														NULL
													</cfif>
													)
											</cfquery>
											<cfset thisAnswerTextGiven = "" />
											<cfset thisMatchingAnswerTextGiven = "" />
										</cfif>
									</cfloop>
								</cfif>
							</cfif>
							<cfset thisOptionIDPair = "" />
							<cfset thisOptionIDLeft = "" />
							<cfset thisOptionIDRight = "" />
						</cfloop>
						<cfquery name="removeUnselected" datasource="#application.siteDataSource#">
							delete from QuizTakenAnswer
							where QuizTakenQuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenQuestionID#">
							AND AnswerID not in (<cf_queryparam list="true" cfsqltype="cf_sql_integer" value="#listOptionIDLeft#">)
						</cfquery>
					</cfif>
				</cfif>
			<!--- </cftransaction> --->
		</cfif>
		<cfreturn returnValue />
	</cffunction>

	<!--- END STCR P-REL109 --->

	<!--- get quiz score

	WAB 2009/02/25	adjusted to allow negative and decimal scoring,
		needs a switch at quiz level to decide whether to allow a negative score for indvidual question
	--->
	<cffunction access="public" name="getScore" returnType="Query">
		<cfargument name="QuizTakenID" type="numeric" required="Yes">
		<cfargument name="Questions" required="Yes">
		<cfargument name="AllowNegativeScore" default = "false"> <!--- whether a negative result can be given for an individual question, if false the negative results become 0 --->

		<cfscript>
			var getScore = "";
		</cfscript>
		<!--- 2012/02/13 PPB Case 426433 defend against questions being "" on quiztaken table; subsequently I added a trap in finish.cfm to prevent us coming here in the scenario I was investigating (quiz already passed) but this may avoid an ugly error in another scenario --->
		<cfif Trim(Questions) eq "">
			<cfset arguments.Questions = "0">
		</cfif>

		<!--- NYB 2009-03-06 Removed a spare "select" in the query that was causing it to error --->
		<CFQUERY NAME="getScore" datasource="#application.siteDataSource#">
			<!--- WAB 2009/02/25 new query to deal with negative and fractional scores --->
			select ISNULL(sum(<cfif allowNegativeScore>RawScore<cfelse>RawScoreNoNeg</cfif> ),0) as TotalScore		<!--- 2012/02/13 PPB Case 426433 added ISNULL to defend against questions being "" on quiztaken table  --->
			FROM ( <!--- STCR P-REL109 score from new data structure --->
				select round(sum(scoreGiven),2) as RawScore, round(case when sum(scoreGiven) <0 then 0 else sum(scoreGiven) end,2) as RawScoreNoNeg
				from quiztakenanswer qta inner join quiztakenquestion qtq on qta.quiztakenquestionid=qtq.quiztakenquestionid
				where qtq.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
					) as dummy
		</CFQUERY>
		<cfreturn getScore>
	</cffunction>

	<!--- update record with total score--->
	<cffunction access="public" name="updateQuizScore">
		<cfargument name="QuizTakenID" type="numeric" required="Yes">
		<cfargument name="TotalScore" type="numeric" required="Yes">
		<cfargument name="PassMark" type="numeric" required="No">
		<cfargument name="QuizStatus" type="string" required="No"><!--- STCR 2012-09-17 CASE 430523 Enable SCORM functions to update PassMark and QuizStatus, without using SetQuizTakenCompleted() which relies on a real QuizDetail record. --->

		<cfset var updateQuiz = "">

		<!--- WAB/NJH 2012-11-14 CASE 431977 added CRUD --->
		<CFQUERY NAME="updateQuiz" datasource="#application.siteDataSource#">
			UPDATE QuizTaken SET
				Score = #TotalScore#,
				<cfif structKeyExists(arguments,"PassMark")>PassMark = <cf_queryparam cfsqltype="cf_sql_float" value="#arguments.PassMark#">,</cfif>
				<cfif structKeyExists(arguments,"QuizStatus") and listFindNoCase("Pass,Fail",arguments.QuizStatus)>QuizStatus = '#arguments.QuizStatus#',</cfif>
				lastupdated = getdate(),
				lastupdatedby = #request.relaycurrentuser.usergroupid#,
				lastupdatedbyPerson = #request.relaycurrentuser.personid#
			WHERE QuizTakenID = #QuizTakenID#
		</CFQUERY>

		<!--- NJH 2012/11/19 CASE 432093 - call setQuizTakenCompleted if status has been passed... this will set the module to complete and then update things like certifications, etc. --->
		<cfif structKeyExists(arguments,"QuizStatus") and listFindNoCase("Pass,Fail",arguments.QuizStatus)>
			<cfset setQuizTakenCompleted(argumentCollection=arguments)>
		</cfif>
	</cffunction>

	<cffunction access="public" name="quizlist" returnType="Query">
		<cfargument name="PersonID" type="numeric" default="#request.relayCurrentUser.personid#">
		<cfargument name="quizGroupID" type="numeric" default="0" required="no">

		<cfset var GetQuizes = "">

		<!--- 2008-10-13 NYB removed DescriptionPhraseTextID from query as it no longer exists in table --->
		<CFQUERY NAME="GetQuizes" datasource="#application.siteDataSource#">
			Select qd.quizDetailID,
				'phr_title_quizdetail_'+cast(qd.quizDetailID as varchar(50)) as QuizName,
			completed, NumToChoose,score, convert(decimal(6,1),(convert(decimal(6,2),score)/NumtoChoose*100)) as percentage
			from quizDetail qd left outer join
			(select qti.quizDetailID, qti.completed, qti.score from quizTaken qti
				where personid = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#">
				and completed = (select max(completed) from quiztaken
					where personid = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.PersonID#"> and quizdetailid  = qti.quizDetailID)) qt
			on qd.quizDetailID = qt.quizDetailID
			<CFIF quizGroupID neq "0">
				where qd.quizGroupID = #quizGroupID#
			</CFIF>
			order by quizName
		</CFQUERY>
		<cfreturn GetQuizes>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Returns a list of all liveQuizes
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getLiveQuizzes" hint="Returns a list of all liveQuizes">
		<cfargument name="sortOrder"  required="Yes">
		<cfargument name="active" type="boolean" required="false">

		<cfscript>
			var getLiveQuizzes = "";
		</cfscript>

		<cfquery name="getLiveQuizzes" datasource="#application.siteDataSource#">
			select * from vQuizDetail
			where 1=1
			<cfif structKeyExists(arguments,"active")>and active=<cf_queryparam cfsqltype="cf_sql_bit" value="#arguments.active#"></cfif>
			order by #arguments.sortOrder#
		</cfquery>
		<cfreturn getLiveQuizzes>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      Returns the Out Of value of a Quiz
	  WAB 2009/02/25 added a round function to deal with fractional scores  also added a group by to bottom union so that rounding done by question
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getHighestPossibleScore" returnType="numeric">
		<!--- 2012-05-08 STCR P-REL109 now deriving from QuizTakenID. Have updated call from finishV2.cfm --->
		<!--- <cfargument name="Questions" required="Yes"> --->
		<cfargument name="QuizTakenID" type="numeric" required="true" />

		<cfscript>
			var getScore = "";
		</cfscript>

		<!--- 2012-05-08 STCR P-REL109 new data structure --->
		<CFQUERY NAME="getScore" datasource="#application.siteDataSource#">
			select isNULL(sum(possibleScore),0) as HighestPossScore from
				(
				<!---  radios--->
				select max(qa.Score) as possibleScore from QuizTakenQuestion qtq
				inner join QuizAnswer qa with(nolock) on qtq.QuestionID=qa.QuestionID
				inner join QuizQuestion qq with(nolock) on qtq.QuestionID=qq.QuestionID
				inner join QuizQuestionType qqt with(nolock) on qq.QuestionTypeID=qqt.QuestionTypeID
				where qtq.QuizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#"> and qqt.QuestionTypeTextID='radio'
				group by qq.QuestionID
				union all
				<!---  not radios--->
				select round(sum(qa.Score),2) as possibleScore from QuizTakenQuestion qtq
				inner join QuizAnswer qa with(nolock) on qtq.QuestionID=qa.QuestionID
				inner join QuizQuestion qq with(nolock) on qtq.QuestionID=qq.QuestionID
				inner join QuizQuestionType qqt with(nolock) on qq.QuestionTypeID=qqt.QuestionTypeID
				where qtq.QuizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#"> and qqt.QuestionTypeTextID='checkbox' and qa.Score > 0
				group by qq.QuestionID
				<!--- STCR - For Phase 2 implement scoring for matching type questions --->
				union all
				select round(sum(qa.Score),2) as possibleScore from QuizTakenQuestion qtq
				inner join QuizAnswer qa with(nolock) on qtq.QuestionID=qa.QuestionID
				inner join QuizQuestion qq with(nolock) on qtq.QuestionID=qq.QuestionID
				inner join QuizQuestionType qqt with(nolock) on qq.QuestionTypeID=qqt.QuestionTypeID
				where qtq.QuizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#"> and qqt.QuestionTypeTextID='matching' and isNull(qa.MatchingAnswerID,0) <> 0 and qa.Score > 0 <!--- Should the answer editor even allow negative scored pairs for matching answers? --->
				group by qq.QuestionID
				) as scores
		</CFQUERY>

		<cfreturn getScore.HighestPossScore>
	</cffunction>

<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Update QuizTaken lastQuestionViewed value
		Created: 2011-09-16 NYB	P-SNY106
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="updateQuestionsViewed" hint="Update updateQuestionsViewed in a quizTaken record.">
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes">
	 	<cfargument name="QuestionsViewed" type="string" required="Yes">

		<cfscript>
			var updateQuestionsViewedQry = "";
		</cfscript>

		<!--- <CFQUERY NAME="updateQuestionsViewedQry" datasource="#application.siteDataSource#">
			UPDATE QuizTaken SET QuestionsViewed = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.QuestionsViewed#">
				WHERE QuizTakenID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.QuizTakenID#">
		</CFQUERY> --->
		<!--- STCR P-REL109 --->
		<cfif listLen(arguments.QuestionsViewed) gt 0>
			<CFQUERY NAME="updateQuestionsViewedQry" datasource="#application.siteDataSource#">
				UPDATE QuizTakenQuestion SET Viewed = 1, ViewedAt = getDate()
				WHERE QuestionID IN (<cf_queryparam list="true" cfsqltype="CF_SQL_INTEGER" value="#arguments.QuestionsViewed#">)
				AND QuizTakenID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.QuizTakenID#">
			</CFQUERY>
		</cfif>
	</cffunction>



<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Get Incorrect Questions query
		Created: 2011-09-16 NYB	P-SNY106
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getIncorrectQuestions" hint="Get Incorrectly answered Questions">
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes" />

		<cfscript>
			var getIncorrectQuestionsQry = "";
		</cfscript>

		<!--- 2012-08-15 STCR use new Quiz data structure. Currently called from finish.cfm under certain conditions. --->
		<cfquery name="getIncorrectQuestionsQry" datasource="#application.siteDataSource#">
			select distinct base.QuizTakenID,
				QuestionID, TotalScoreGiven
			FROM (
					select
					QuizTakenID,
					qtq.QuestionID, sum(isNull(qta.ScoreGiven,0)) AS TotalScoreGiven from QuizTakenQuestion qtq
					left join QuizTakenAnswer qta with(nolock) on qtq.QuizTakenQuestionID=qta.QuizTakenQuestionID
					where QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#" >
					group by qtq.QuestionID, QuizTakenID
				) base
			where TotalScoreGiven <= 0
		</cfquery>
		<cfreturn getIncorrectQuestionsQry />
	</cffunction>


<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Get Unanswered Questions query
		Created: 2011-09-16 NYB	P-SNY106
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction access="public" name="getUnansweredQuestions" hint="Get Unanswered Questions" returntype="query">
	 	<cfargument name="QuizTakenID" type="numeric" required="Yes" />

		<cfscript>
			var getUnansweredQuestionsQry = "";
		</cfscript>


		<cfquery NAME="getUnansweredQuestionsQry" datasource="#application.siteDataSource#">
			<!--- 2012-08-13 STCR use new Quiz data structure. In Phase 1 we converted the old Flag query found in continue.cfm. In Phase 2 we move the query here and changed continue.cfm to call this. Up to now it appears that this function was not being called. --->
			select qtq.QuestionID AS NotAnswered from QuizTakenQuestion qtq
			left join QuizTakenAnswer qta on qtq.QuizTakenQuestionID=qta.QuizTakenQuestionID
			where QuizTakenID = <cf_queryparam value="#arguments.QuizTakenID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and qta.QuizTakenAnswerID is null <!--- get questions where no answer was given, and if no answer was given then there will only be one row on the left of the join per qtq, so we do not need to select distinct QuestionID --->
		</cfquery>
		<cfreturn getUnansweredQuestionsQry />
	</cffunction>


	<cffunction name="getQuizCertificationMergeFields" access="public" returnType="struct">
		<cfargument name="QuizTakenID" type="Numeric" required="Yes">
		<cfargument name="personID" type="numeric" required="no">
        <cfargument name="courseID" type="numeric" required="no"><!--- Case 448830 / DCC added for scorm quizzes --->

		<cfset var mergeStruct = structNew()>
		<cfset var personDetails = "">
		<cfset var qryQuizTakenDetails = getQuiz(quizTakenID=arguments.quizTakenId)>

		<cfif structKeyExists(arguments,"personID")>
			<cfset personDetails = application.com.relayPLO.getPersonDetails(arguments.personID)>
			<cfset mergeStruct.firstName = personDetails.firstName>
			<cfset mergeStruct.lastName = personDetails.lastName>
			<cfset mergeStruct.orgName = personDetails.organisationName>
		</cfif>

		<!--- start: Case 448830 --->
        <!--- DCC 2015-06-26 SCORM quizzes have no quizdetail record --->
        <cfif qryQuizTakenDetails.recordcount GT 0>
    		<cfset mergeStruct.passdate = LSDateFormat(qryQuizTakenDetails.Completed, "dddd, mmmm dd, yyyy")>
    		<cfset mergeStruct.quizname = qryQuizTakenDetails.QuizName>
    		<cfset mergeStruct.courseName = getCourseNameForQuiz(arguments.quizTakenId)>
    		<cfset mergeStruct.moduleID = qryQuizTakenDetails.moduleId>
    		<cfset mergeStruct.moduleName = getModuleNameForQuiz(arguments.quizTakenId)>
    	<cfelse>
           <!--- DCC scorm quizzes don't have quizdetail records --->
           <cfset var gettheCourseSCORMbits = getCourseSCORMbits(arguments.quizTakenId)>
           <cfset mergeStruct.passdate = LSDateFormat(gettheCourseSCORMbits.passdate, "dddd, mmm dd,yyyy")>
           <cfset mergeStruct.moduleID = gettheCourseSCORMbits.moduleID>
           <cfset mergeStruct.moduleName = getCourseNameForQuiz(arguments.quizTakenId,arguments.courseID)><!--- DCC actually the course name --->       
        </cfif>
        <!--- end: Case 448830 --->

		<cfreturn mergeStruct>
	</cffunction>

    <!--- Case 448830 / DCC 2015-06-26 SCORM quizzes have no quizdetail record new function --->
    <cffunction name="getCourseSCORMbits" access="public">
        <cfargument name="QuizTakenID" type="Numeric" required="Yes">

        <cfscript>
           var qgetCourseSCORMbits = "";
        </cfscript>

        <cfquery name="qgetCourseSCORMbits" datasource="#application.siteDataSource#">
            SELECT qt.completed as passdate, tump.moduleID
            FROM
              dbo.QuizTaken qt
            INNER JOIN dbo.trngUserModuleProgress tump ON  tump.userModuleProgressID=qt.userModuleProgressID
            WHERE quiztakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
        </cfquery>
        <cfreturn qgetCourseSCORMbits />
    </cffunction>

	<!--- START: 2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR018 Elearning PDF Certificate --->
		<!--- Get Name of Course Based on Quiz ID passed --->
        <!--- Get Name of Course Based on Quiz ID passed
        DCC 2015/06/26 added bits for SCORM as no quizdetail record
        --->
	<cffunction access="public" name="getCourseNameForQuiz" returntype="String" >
		<cfargument name="QuizTakenID" type="Numeric" required="Yes">
        <cfargument name="courseID" type="numeric" required="no"><!--- Case 448830 / DCC added for scorm quizzes --->	

		<cfset var courseNameTitle = "" />
		<cfset var courseName = "" />

		<!--- Case 448830 --->
		<cfquery name="courseNameTitle" datasource="#application.siteDataSource#">
			select c.title_defaultTranslation
				from trngCourse c with (noLock)
                <cfif structKeyExists(arguments,"courseID")>
                where c.courseID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.courseID#">
                <cfelse>
				inner join trngModule m with (noLock) on m.courseID = c.courseID
				inner join quizDetail qd with (noLock) on m.quizDetailID = qd.quizDetailID
				inner join quizTaken qt with (noLock) on qd.quizDetailId = qt.quizDetailId
			where qt.quizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
                </cfif>
		</cfquery>

		<cfset courseName = courseNameTitle.title_defaultTranslation>

		<cfreturn courseName>

	</cffunction>

	<cffunction access="public" name="getModuleNameForQuiz" returntype="String" >
		<cfargument name="QuizTakenID" type="Numeric" required="Yes">

		<cfset var moduleName = "" />
		<cfset var moduleNameTitle = "" />

		<cfquery name="moduleNameTitle" datasource="#application.siteDataSource#">
			select m.title_defaultTranslation from trngModule m with (noLock)
				inner join quizDetail qd with (noLock) on m.quizDetailID = qd.quizDetailID
				inner join quizTaken qt with (noLock) on qd.quizDetailId = qt.quizDetailId
			where qt.quizTakenID=<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.QuizTakenID#">
		</cfquery>

		<cfset moduleName = moduleNameTitle.title_defaultTranslation />

		<cfreturn moduleName>

	</cffunction>


	<cffunction access="public" name="insertCertificatePrint" hint="Inserts new Certificate print information." >
		<cfargument name="personID" type="numeric" required="true" />
		<cfargument name="quiztakenid" type="numeric" required="true" />
		<cfargument name="visitID" type="numeric" />

		<cfscript>
			var insertCertificatePrints = "";
		</cfscript>

		<cfquery name="insertCertificatePrints" datasource="#application.siteDataSource#">
			insert into trngcertificatePrints (personid,quiztakenid,visitid)
			values (
			<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.personID#">,
			<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.quiztakenid#">,
			<cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.visitID#">
			)
		</cfquery>

	</cffunction>

	<!--- END:  2012/07/17 - Englex - P-REL109 Phase 2 - Code ported and upgraded from P-LEN030 CR018 Elearning PDF Certificate --->

	<cffunction name="getQuizQuestionAnswersGivenByPerson" access="public" returnType="any">
		<cfargument name="questionID" type="numeric" required="false" />
		<cfargument name="quizTakenID" type="numeric" required="true" />

		<cfset var getQuizAnswers = "">
		<cfset var QuizAnswerPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "QuizAnswer", phraseTextID = "title", baseTableAlias = "a")>
		<cfset var QuizMatchingAnswerPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "QuizAnswer", phraseTextID = "title", baseTableAlias = "ma",phraseTableAlias="matchingAnswer")>
		<cfset var QuizQuestionPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "QuizQuestion", phraseTextID = "title", baseTableAlias = "q")>

		<cfquery name="getQuizAnswers" datasource="#application.siteDataSource#">
			select #preservesinglequotes(QuizAnswerPhraseQuerySnippets.select)# as AnswerTextGiven,
				ta.quizTakenQuestionID,
				#preservesinglequotes(QuizQuestionPhraseQuerySnippets.select)# as questionTextGiven,
				ta.answerID,ta.matchingAnswerID,ta.scoreGiven,
				#preservesinglequotes(QuizMatchingAnswerPhraseQuerySnippets.select)# as matchingAnswerTextGiven
			from QuizAnswer as a
				inner join quizTakenAnswer as ta on a.AnswerID = ta.AnswerID
				inner join quizTakenQuestion as tq on tq.QuestionID = a.QuestionID and tq.QuizTakenQuestionID = ta.QuizTakenQuestionID
				inner join quizQuestion q on tq.questionID = q.questionID
				left join quizAnswer ma on ma.answerID = ta.matchingAnswerID
				#preservesinglequotes(QuizAnswerPhraseQuerySnippets.join)#
				#preservesinglequotes(QuizQuestionPhraseQuerySnippets.join)#
				#preservesinglequotes(QuizMatchingAnswerPhraseQuerySnippets.join)#
			where tq.QuizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.quizTakenID#">
				<cfif structKeyExists(arguments,"questionID")>
				and tq.QuestionID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.questionID#">
				</cfif>
		</cfquery>

		<cfreturn getQuizAnswers>

	</cffunction>


	<cffunction name="getQuizTakenMergeFields" access="public" returntype="struct">
		<cfargument name="quizTakenID" type="numeric" required="true">
		<cfargument name="quizTaken" type="struct" required="false">

		<cfif not structKeyExists(arguments,"quizTaken")>
			<cfset arguments.quizTaken = application.com.structureFunctions.queryRowToStruct(query=application.com.relayQuiz.getQuiz(quizTakenID=arguments.QuizTakenID),row=1)>
		</cfif>
		<cfif not structKeyExists(quizTaken,"quizTakenQuestionsAnswers")>
			<cfset quizTaken.QuestionsAnswers = getQuizQuestionAnswersGivenByPerson(quizTakenID=arguments.quizTakenID)>
		</cfif>

		<cfreturn quizTaken>
	</cffunction>


	<cffunction access="public" name="createModuleCertificate" returntype="query">
		<cfargument name="mergeStruct"  type="Struct" required="Yes">

		<cfset var qryPaths = queryNew("path")>
		<cfset var tempFolder = "">
		<cfset var getContent = "">
		<cfset var certificate = "">

		<!--- The Query that brings back the cretificate to be printed --->
		<cfquery name="getContent" datasource="#Application.SiteDataSource#">
			select id from Element where elementTextId = <cf_queryparam cfsqltype="cf_sql_varchar" value="#application.com.settings.getSetting('elearning.courseCertificateETID')#">
		</cfquery>

		<cfif getContent.recordCount>
			<!--- Creating the PDF Certificate --->
			<cfdocument name="certificate" format="PDF" orientation="portrait" overwrite="true">
				<cfoutput>#application.com.relayTranslations.translatePhrase(phrase="phr_Detail_Element_"&getContent.id,mergeStruct=arguments.mergeStruct)#</cfoutput>
			</cfdocument>

			<!--- Finding a temp folder and writing it there --->
			<cfset tempFolder=application.com.filemanager.getTemporaryFolderDetails()>
			<cffile action="write" file="#tempFolder.path#\certificate.pdf" output="#certificate#">

			<!--- returning the path and the file name created back as a query --->
			<cf_querysim>
				<cfoutput>
				   qryPaths
				   path
					#tempFolder.path#certificate.pdf
				</cfoutput>
			</cf_querysim>
		</cfif>

		<cfreturn qryPaths>

	</cffunction>

	<!--- CASE 435326 - NJH 2013/05/23 - create function to delete a quiz attempt..currently leaving trngCoursewareData table alone for scorm quizzes...it should be ok to leave it?? --->
	<cffunction name="deleteQuizTaken" returnType="struct">
		<cfargument name="quizTakenID" type="numeric" required="true">

		<cfset var result = {isOK=true,message="Quiz Taken Record successfully deleted."}>
		<cfset var getUserModuleProgress = "">

		<cfquery name="getUserModuleProgress" datasource="#application.siteDataSource#">
			select tump.moduleID, tump.personID, m.isScorm from trngUserModuleProgress tump
				inner join quizTaken qt on qt.userModuleProgressID = tump.userModuleProgressID
				inner join trngModule m on m.moduleID = tump.moduleID
			where quizTakenID = <cf_queryparam value="#arguments.quizTakenID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cftry>
			<!--- if a RW quiz, delete the actual quiz taken record... otherwise, if a scorm module, just delete the data in trngCourswareData to allow the user to take the quiz again --->
			<cfquery name="local.deleteQuizTakenRecord" datasource="#application.siteDataSource#">
				<cfif not getUserModuleProgress.isScorm[1]>
					delete quizTakenAnswer
						from
						quizTakenAnswer qta inner join quizTakenQuestion qtq on qta.quizTakenQuestionID = qtq.quizTakenQuestionID
					where qtq.quizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.quizTakenID#">

					delete from quizTakenQuestion where quizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.quizTakenID#">

					delete from quizTaken where quizTakenID = <cf_queryparam cfsqltype="cf_sql_integer" value="#arguments.quizTakenID#">
				<cfelse>
					delete from trngCoursewareData where moduleID =  <cf_queryparam value="#getUserModuleProgress.moduleID[1]#" CFSQLTYPE="cf_sql_integer" > and personId =  <cf_queryparam value="#getUserModuleProgress.personID[1]#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
			</cfquery>

			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message = "Error deleting Quiz Taken Record.">
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<!--- 2014-04-14	YMA	Case 434303 Test Feedback to users --->
	<cffunction name="getCorrectAnswers" returnType="query">
		<cfargument name="quizTakenID" type="numeric" required="true">

		<cfset var getCorrectAnswers = "">

		<cfquery name="getCorrectAnswers" datasource="#application.siteDataSource#">
			select tump.moduleID, tump.personID, m.isScorm from trngUserModuleProgress tump
				inner join quizTaken qt on qt.userModuleProgressID = tump.userModuleProgressID
				inner join trngModule m on m.moduleID = tump.moduleID
			where quizTakenID = <cf_queryparam value="#arguments.quizTakenID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn getCorrectAnswers>
	</cffunction>


	<cffunction name="getQuestionPoolsForQuizDetail" returnType="query" output="false">
		<cfargument name="quizDetailID" type="numeric" required="true">
		<cfargument name="assigned" type="boolean" required="false">

		<cfset var getQuizQuestionPools = "">

		<cfquery name="getQuizQuestionPools">
			select QuestionPoolRef, count(distinct questionID) as numOfQuestionsInPool,qdqp.quizDetailQuestionPoolId,qqp.questionPoolID,
				qdqp.numOfQuestions as numOfQuestions,
				isNull(NumOfQuestions,count(distinct questionID)) as numOfQuestionsToChooseFrom <!--- for backwards compatibility --->
			from quizQuestionPool qqp
				inner join quizQuestion qq on qq.questionPoolId = qqp.questionPoolID
				left join QuizDetailQuestionPool qdqp on qdqp.questionPoolId = qqp.questionPoolID and qdqp.quizDetailID = <cf_queryparam value="#arguments.quizDetailID#" cfsqltype="cf_sql_integer">
			<cfif structKeyExists(arguments,"assigned")>
			where
				qdqp.quizDetailQuestionPoolId is <cfif arguments.assigned>not</cfif> null
			</cfif>
			group by QuestionPoolRef,qdqp.quizDetailQuestionPoolId,qqp.questionPoolID,numOfQuestions
			order by questionPoolRef
		</cfquery>

		<cfreturn getQuizQuestionPools>
	</cffunction>


	<!--- NJH 2014/06/23 Task Core-81 - insert a quizDetailQuestionPool record --->
	<cffunction name="insertQuizDetailQuestionPool" access="public" output="false" returnType="struct">
		<cfargument name="quizDetailID" type="numeric" required="true">
		<cfargument name="questionPoolID" type="numeric" required="true">
		<cfargument name="numOfQuestions" type="numeric" required="true">

		<cfset var result = {isOk=true,message=""}>

		<cftry>
			<cfquery name="local.qInsertQuizDetailQuestionPool">
				if not exists (select 1 from QuizDetailQuestionPool where quizDetailId = <cf_queryparam value="#arguments.quizDetailID#" cfsqltype="cf_sql_integer"> and questionPoolID = <cf_queryparam value="#arguments.questionPoolID#" cfsqltype="cf_sql_integer">)
				insert into quizDetailQuestionPool (
					quizDetailID,
					questionPoolID,
					numOfQuestions)
				values (
					<cf_queryparam value="#arguments.quizDetailID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#arguments.questionPoolID#" cfsqltype="cf_sql_integer">,
					<cf_queryparam value="#arguments.numOfQuestions#" cfsqltype="cf_sql_integer">
					)
			</cfquery>


			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>

	<!--- NJH 2014/06/23 Task Core-81 - delete a quizDetailQuestionPool record --->
	<cffunction name="deleteQuizDetailQuestionPool" access="public" output="false" returnType="struct">
		<cfargument name="QuizDetailQuestionPoolID" type="numeric" required="true">

		<cfset var result = {isOk=true,message=""}>
		<cftry>
			<cfquery name="local.qDeleteQuizDetailQuestionPool">
				delete from quizDetailQuestionPool where quizDetailQuestionPoolID = <cf_queryparam value="#arguments.QuizDetailQuestionPoolID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


	<!--- NJH 2014/06/23 Task Core-81 - the display for the quizDetail - Question Pool editor --->
	<cffunction name="getQuizDetailQuestionPoolDisplay" access="public" output="false" returnType="struct">
		<cfargument name="quizDetailID" type="numeric" required="true">

		<cfset var qQuestionPoolsAssigned = getQuestionPoolsForQuizDetail(quizDetailID=arguments.quizDetailID,assigned="true")>
		<cfset var qQuestionPoolsNotAssigned = getQuestionPoolsForQuizDetail(quizDetailID=arguments.quizDetailID,assigned="false")>
		<cfset var quizDetailQuestionPoolDisplay = "">
		<cfset var result = {isOK=true,content=""}>

		<cftry>
			<cfquery name="qQuestionPoolsAssigned" dbtype="query">
				select questionPoolId, questionPoolRef+ ' ('+ cast(numOfQuestionsInPool as varchar) +')' as questionPoolRef, NumOfQuestions, quizDetailQuestionPoolId, numOfQuestionsInPool,
					numOfQuestionsToChooseFrom
				from qQuestionPoolsAssigned
				order by questionPoolRef
			</cfquery>

			<cfquery name="qQuestionPoolsNotAssigned" dbtype="query">
				select questionPoolId, questionPoolRef+ ' ('+ cast(numOfQuestionsInPool as varchar) +')' as questionPoolRef, quizDetailQuestionPoolId, numOfQuestionsInPool
				from qQuestionPoolsNotAssigned
				order by questionPoolRef
			</cfquery>

			<cfsavecontent variable="quizDetailQuestionPoolDisplay">
				<cfoutput>
					<!--- NJH 2016/01/22 - JIRA BF-126 - revert display back to table. Set the relayFormDisplayStyle attribute --->
					<cf_relayFormDisplay relayFormDisplayStyle="HTML-table">
						<tr>
							<th>Question Pool</th>
							<th>Number of Questions</th>
							<th>&nbsp;</th>
						</tr>
						<cfloop query="qQuestionPoolsAssigned">
							<cfoutput>
							<tr id="#quizDetailQuestionPoolId#">
								<td>#htmlEditformat(QuestionPoolRef)#</td>
								<td>#htmlEditFormat(NumOfQuestions)#</td>
								<td><cf_input type="button" value="phr_delete" onclick="deleteQuizDetailQuestionPool('#jsStringFormat(application.com.security.encryptVariableValue(value = quizDetailQuestionPoolId, name = 'quizDetailQuestionPoolId', singleSession="true"))#')"></td>
							</tr>
							</cfoutput>
						</cfloop>
						<cf_input type="hidden" value="#arraySum(qQuestionPoolsAssigned["numOfQuestionsToChooseFrom"])#" name="sumOfQuestions">
						<cfif qQuestionPoolsNotAssigned.recordCount>
							<tr id="addNewQuizDetailQuestionPool">
								<td><cf_select query="#qQuestionPoolsNotAssigned#" showNull="true" display="questionPoolRef" value="questionPoolId" name="questionPoolId" customAttribute="numOfQuestionsInPool" encrypt="true"></td>
								<td><cf_input type="text" value="" name="numOfQuestions" range="1,#qQuestionPoolsNotAssigned.numOfQuestionsInPool[1]#"></td>
								<td><cf_input type="button" value="phr_add" onclick="addQuizDetailQuestionPool();return false;" id="addQuizDetailQuestionPoolBtn"></td>
							</tr>
						</cfif>
					</cf_relayFormDisplay>
				</cfoutput>
			</cfsavecontent>

			<cfset result.content = quizDetailQuestionPoolDisplay>

			<cfcatch>
				<cfset result.isOK = true>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>


 </cfcomponent>

