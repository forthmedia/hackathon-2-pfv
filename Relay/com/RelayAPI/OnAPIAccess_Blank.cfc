/**
 * Used for consistency when no specific behaviour found; just has empty methods (avoids the requirements for null checks)
 *
 * @author Richard.Tingle
 * @date 17/02/15
 **/
component implements="interfaces.OnAPIAccessCustomBehaviour" accessors=true output=false persistent=false {


	public void function onSuccessfulUpdate(numeric entityID, struct entityDetails){};
	public void function onFailedUpdate(numeric entityID, struct entityDetails){};
	public void function onSuccessfulInsert(numeric entityID, struct entityDetails){};
	public void function onFailedInsert(numeric entityID, struct entityDetails){};
	public void function onSuccessfulDelete(numeric entityID){};
	public void function onFailedDelete(numeric entityID){};
}