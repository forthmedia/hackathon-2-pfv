/**
 * Handles the calling of the actual custom behaviour (which is hooked by CustomPostAPIAccessBehaviourFetcher from the cfTemplate directory in Code,
 * these are named OnAPIAccess_ENTITYTYPE and implement the interfaces.OnAPIAccessCustomBehaviour interface
 *
 * @author Richard.Tingle
 * @date 17/02/15
 **/
component accessors=true output=false persistent=false {


		/**
		 * customPostAPIAccessBehaviourFetcher is an optional argument to provide an object that provides CustomBehaviourObjects.
		 * If not passed one will be initialised
		 **/
		public CustomPostAPIBehaviourHandler function init(CustomPostAPIAccessBehaviourFetcher customPostAPIAccessBehaviourFetcher){
			//optional customPostAPIAccessBehaviourFetcher, if not passed one will be created

			if (structkeyexists(arguments, "customPostAPIAccessBehaviourFetcher" )){
				this.customPostAPIAccessBehaviourFetcher=arguments.customPostAPIAccessBehaviourFetcher;
			}else{
				this.customPostAPIAccessBehaviourFetcher=new CustomPostAPIAccessBehaviourFetcher();
			}
			return this;
		}

		/**
		 * entityType - Person, Location etc
		 * entityID
		 * success
		 * entityData - the data used when updating/inserting"
		 **/
		public void function handleCustomBehaviour_insert(
			required string entityType,
			required string entityID,
			required boolean success,
			required struct entityData
			){

				var customResponse=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject(entityType);
				if (success){
					customResponse.onSuccessfulInsert(entityID,entityData);
				}else{
					customResponse.onFailedInsert(entityID,entityData);
				}
			}


		/**
		 * entityType - Person, Location etc
		 * entityID
		 * success
		 * entityData - the data used when updating/inserting
		 **/
		public void function handleCustomBehaviour_update(
			required string entityType,
			required string entityID,
			required boolean success,
			required struct entityData
			){

				var customResponse=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject(entityType);

				if (success){
					customResponse.onSuccessfulUpdate(entityID,entityData);
				}else{
					customResponse.onFailedUpdate(entityID,entityData);
				}

			}

		/**
		 * entityType - Person, Location etc
		 * entityID
		 * success
		 **/
		public void function handleCustomBehaviour_delete(
			required string entityType,
			required string entityID,
			required boolean success
			){
				var customResponse=this.customPostAPIAccessBehaviourFetcher.getCustomBehaviourObject(entityType);

				if (success){
					customResponse.onSuccessfulDelete(entityID);
				}else{
					customResponse.onFailedDelete(entityID);
				}
			}




}