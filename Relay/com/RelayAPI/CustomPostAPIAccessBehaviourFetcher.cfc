/**
 * CustomPostAPIAccessBehaviourFetcher
 *
 * This class fetches (if it exists) classes of the form onAPIAccess_ENTITYTYPE from the CFTemplate directory (or anywhere application.com is loaded from)
 * and implement the interfaces.OnAPIAccessCustomBehaviour interface. This represents a hook for custom behaviour
 *
 * @author Richard.Tingle
 * @date 17/02/15
 **/
component accessors=true output=false persistent=false {


	public CustomPostAPIAccessBehaviourFetcher function init(struct structToSearch, interfaces.OnAPIAccessCustomBehaviour failureObject){
		if (structkeyexists(arguments, "structToSearch")){
			this.structToSearch=arguments.structToSearch;
		}else{
			this.structToSearch=application.com;
		}
		if (structkeyexists(arguments, "failureObject")){
			this.failureObject=arguments.failureObject;
		}else{
			this.failureObject=new OnAPIAccess_Blank();
		}
		return this;
	}

	/**
	 *  entityType - The entityType (Person, Location etc) to get the custom handler for
	 **/
	public interfaces.OnAPIAccessCustomBehaviour function getCustomBehaviourObject(required string entityType) output="false" {
		cfcToLookFor="onAPIAccess_" & entityType;

		if (structKeyExists(this.structToSearch,cfcToLookFor)){
			return this.structToSearch[cfcToLookFor];
		}else{
			return this.failureObject;
		}


	}
}