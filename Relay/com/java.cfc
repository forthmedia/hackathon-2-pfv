/**
 * java
 * 
 * @author Richard.Tingle
 * @date 23/02/16
 **/
component accessors=true output=true persistent=false {


	private any function getLibraryDirectorys(){
		return ["#application.paths.relay#\javaloader\support\cfcdynamicproxy\lib\","#application.paths.relay#\java\lib\"];
	}
	/**
	 * Returns all the known library directories, including the lib_CFIncluded, these are needed for the compiler but not the loader (as the loader 
	 * comes from CF so already has all the CFIncluded files) 
	 **/
	private any function getLibraryDirectorys_includingCFAware(){
		var libraries=["#application.paths.relay#\java\lib_CFIncluded\"];
		ArrayAppend(libraries,getLibraryDirectorys(), true);
		return libraries;
	}
	private String function getCompileDirectory(){
		return "#application.paths.aboveCode#\dynamicCompilation\core";
	}


	public Struct function compileAndLoad_forReload(applicationScope=application){
		 var resultStruct=
		 {
		  isOk="false",
		  message="" //this can be html that will be displayed on screen
		 };
		 
		 var results=compile(applicationScope);
		 
		 resultStruct.isOk=results.isOk;
		 
		 if (results.isOk){
		 	resultStruct.message="All java sources successfully compiled <BR/>"; 
			fullJavaLoadAndBoot();
		 }else{
		 	resultStruct.message="Failed to compile java sources <BR/>";
		 	
		 	for(var errorString in results.compileReport.getErrors()){
			 	resultStruct.message&="#errorString# <BR/>";
			 }
		 }
		 resultStruct.message&="<BR/><BR/>";
		 //append remarks to the message
		 
		 for(var remark in results.compileReport.getOtherRemarks()){
		 	resultStruct.message&="#remark# <BR/>";
		 }
		 
		return resultStruct;
		
	}


	public Struct function compile(applicationScope=application ) hint="Compiles java sources ready for them to be loaded" output="true" {
		var sourceDirectory="#applicationScope.paths.relay#/java/src/main";

		var compileJavaLoader=getOrCreateCompilationJavaLoader();
		
		var javaCompilationHandler=compileJavaLoader.create("com.relayware.javacompilation.JavaCompilationHandler");

		
		var response=javaCompilationHandler.compile(sourceDirectory,getAllJarsInDirectorys(getLibraryDirectorys_includingCFAware()), getCompileDirectory());
		
		if (!response.isOk()){
			//log the problem
			var warningStruct=
			{
				message="Error occured during java compilation",
				errors=response.getErrors(),
				remarks= response.getOtherRemarks()
			};
			applicationScope.com.errorHandler.recordRelayError_Warning(Severity="ERROR", Type="Java Compile", WarningStructure=warningStruct);
		}

		return {isok=response.isOk(), textResponse=response.toString(), compileReport=response}; //Todo: raise errors for compilation problems and return false
		
	}



	public Struct function fullJavaLoadAndBoot() hint="Loads all java classes and libraries and does any required injection" output="true" {
		/* TODO: Implement Method */
		var allLibraryJars=getAllJarsInDirectorys(getLibraryDirectorys());

		ArrayAppend(allLibraryJars,  getCompileDirectory());

		application.javaloader=createObject("component", "javaloader.JavaLoader").init(loadPaths=allLibraryJars, loadColdFusionClassPath=true);

		application.cfcDynamicProxy =application.javaloader.create("com.compoundtheory.coldfusion.cfc.CFCDynamicProxy");
					
		//wrapped versions of coldfusion CFCs
		var cfcDynamicProxyWrapped= application.CFCDynamicProxy.createInstance(new java.javabridge.wrappingService(application.cfcDynamicProxy), ["coldfusionbridge.interfaces.CFCDynamicProxy"]);
		var structAccessorWrapped= application.CFCDynamicProxy.createInstance(new java.javabridge.StructAccessor(), ["coldfusionbridge.interfaces.CFStructAccessor"]);
		var componentFactoryWrapped= application.CFCDynamicProxy.createInstance(new java.javabridge.ComponentFactory(application.cfcDynamicProxy), ["coldfusionbridge.interfaces.CFComponentFactory"]);
		
		//java methods which will have wrapped CFCs injected into them
		var injectPoint_cfcDynamicProxy=application.javaloader.create("coldfusionbridge.injection.CFCDynamicProxyInjectionPoint").init();
		var injectPoint_Stuct=application.javaloader.create("coldfusionbridge.injection.CFStructAccessorInjectionPoint").init();
		var injectPoint_Factory=application.javaloader.create("coldfusionbridge.injection.CFComponentFactoryInjectionPoint").init();

		//the actual injection
		injectPoint_cfcDynamicProxy.setProxy(cfcDynamicProxyWrapped);
		injectPoint_Stuct.setCFStructAccessor(structAccessorWrapped);
		injectPoint_Factory.setFactory(componentFactoryWrapped);
		


		return {isok=true};
	}

	public void function reloadComplilationBootstrap() hint="Very rarely required, reloads the code that compiles java classes on demand" output="false" {
		var sourcePaths=ArrayNew(1);
			
			
		ArrayAppend(sourcePaths, "#application.paths.relay#/javaCompilation/lib/ecj-4.5.1.jar"); //the actual compiler
		ArrayAppend(sourcePaths,"#application.paths.relay#/javaCompilation/lib/javaCompilation-1.0.jar"); //our face on the compiler (compiled from src)
			
		//create the 2 fundimental items, javaloader for loading java, cfcDynamicProxy for wrapping in java interfaces
		application.javaSupport.compileJavaLoader=createObject("component", "javaloader.JavaLoader").init(loadPaths=sourcePaths, loadColdFusionClassPath=false);
	}
	
	private any function getOrCreateCompilationJavaLoader(){
		if (!structKeyExists(application,"javaSupport")){
			application.javaSupport=structNew();
		}
		if (!structKeyExists(application.javaSupport,"compileJavaLoader")){

			reloadComplilationBootstrap();
		}
		return application.javaSupport.compileJavaLoader;
		
	}
	
	private any function getAllJarsInDirectorys(Array directorys){
		var containedJars=ArrayNew(1);
		
		for(i=1; i <= ArrayLen(directorys); i++){
			directory=directorys[i];
			ArrayAppend(containedJars, getAllJarsInDirectory(directory),true );
		}
		return containedJars;
	}
	
	private any function getAllJarsInDirectory(String directory){
		var directoryFile = createObject("java", "java.io.File").init(directory);
		
		var containedJars=ArrayNew(1);

		var subPaths=directoryFile.listFiles();

		for(i=1; i <= ArrayLen(subPaths); i++){
			var subPath=subPaths[i];
			if (!subPath.isDirectory() AND subPath.getPath().endsWith(".jar")){
				ArrayAppend(containedJars, subPath.getPath());
			}
			
		}

		return containedJars;
	}

}