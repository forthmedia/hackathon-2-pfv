<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History

Date		By 		Description
2017-02-22	SBW		JIRA RT-28 Added function to return existing account managers. This differentiates
					between Location Acccount manager and org account manager depending on setting PLO.USELOCATIONASPRIMARYPARTNERACCOUNT.

--->
<cfcomponent output="false">

	<cffunction name="getAccountManagers" output="false" hint="Gets potential Account Managers">
		<cfparam name="arguments.searchString" default="">

		<cfquery name="qAccountManager" datasource="#application.siteDataSource#">
			SELECT  personid as dataValue, firstName + ' ' + lastname as displayValue
			from country c
			inner join location l on c.countryid = l.countryid
			inner join person p on p.locationid = l.locationid
			inner join booleanflagdata bfd on p.personid = bfd.entityid
			inner join flag f on bfd.flagid = f.flagid
			where flagtextid = 'isAccountManager'
			and p.active = 1
			<cfif arguments.searchString is not "">
				and
				<cfif IsNumeric(form.frmSearchString)>
					p.personid  =  <cf_queryparam value="#arguments.searchString#" CFSQLTYPE="CF_SQL_INTEGER" >
				<cfelse>
					p.personid in (select EntityID from vPersonSearch where contains(searchableString,<cf_queryparam value="#replace(arguments.searchString,' ',' AND ')#" CFSQLTYPE="CF_SQL_VARCHAR">))
				</cfif>
			</cfif>
			order by displayvalue
		</cfquery>

		<cfreturn qAccountManager>
	</cffunction>

	<cffunction name="getExistingAccountManagers" output="false" hint="Gets existing account managers for Organisation or Location">


		<cfset flagTextID = (application.com.settings.getSetting('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') eq 1 ? 'locAccManager' : 'AccManager')>
		<cfset Table = (application.com.settings.getSetting('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') eq 1 ? 'location' : 'organisation')>
		<cfset flagJoin = application.com.flag.getJoinInfoForFlag (flagID = flagTextID, joinType = "INNER", entityTableAlias = "e") >

		<cfquery name="qAccountManager">
			select distinct #flagJoin.selectfield# as displayValue, #flagJoin.selectFieldRaw# as datavalue from #Table# e #flagJoin.join#
			where #flagJoin.tableAlias#_ent.active = 1
		</cfquery>

		<cfreturn qAccountManager>

	</cffunction>

</cfcomponent>